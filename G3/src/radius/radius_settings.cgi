#!/bin/sh
#/*==========================================================================*
# *  Copyright 2020 Vertiv Tech Co., Ltd
# *  PRODUCT  : NCU(Netsure Controller Unit)
# *  FILENAME : radius_settings.cgi
# *  CREATOR  : Sunil Singh                DATE: 2019-02-02 
# *  VERSION  : V1.00
# *  PURPOSE  : The shell file to make an ACU module project.
# *  HISTORY  : Add To Control Radius Settings.
# *==========================================================================*/

echo "Content-type: text/html"
echo ""
echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<title>Form Example</title>'
echo '</head>'
echo '<body>'


echo "<form method=POST action=\"${SCRIPT}\">"\
       '<table nowrap>'\
          '</tr></table>'
  
read POST_STRING

#echo $POST_STRING > ajax.txt


P_Server=`echo "$POST_STRING" | sed -n 's/^.*_pserver=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
echo $P_Server
P_Port=`echo "$POST_STRING" | sed -n 's/^.*_p1port=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
echo $P_Port

S_Server=`echo "$POST_STRING" | sed -n 's/^.*_sserver=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
echo $S_ServerIP
S_Port=`echo "$POST_STRING" | sed -n 's/^.*_p2port=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
echo $S_Port
SecretKey=`echo "$POST_STRING" | sed -n 's/^.*_passwordC=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
echo $SecretKey
      
NASKEY=`echo "$POST_STRING" | sed -n 's/^.*_nasidentifier=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
EnableRadius=`echo "$POST_STRING" | sed -n 's/^.*_radiusenable=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
     
update_radiusclient_P="authserver "$P_Server":"$P_Port
update_server_P=$P_Server" "$SecretKey
update_radiusclient_S="authserver "$S_Server":"$S_Port
update_server_S=$S_Server" "$SecretKey

echo "$EnableRadius","$P_Server","$P_Port","$S_Server","$S_Port","$NASKEY", > /app/radius/radius_config/rserver_param.txt
#upadte the radiusclient files with primary and secondary server IP and port

sed -i 34s/.*/"$update_radiusclient_P"/ /app/radius/radius_config/radiusclient.conf

sed -i 36s/.*/"$update_radiusclient_S"/ /app/radius/radius_config/radiusclient.conf
#sed -i "34s/.*/$update_radiusclient_P/" /app//radiusclient.conf
#sed -i "36s/.*/$update_radiusclient_S/" /app/config/radiusclient/radiusclient.conf
     
#update the  servers file with serverIP and shared secret key
echo "#ServerIP         shared secret key" >/app/radius/radius_config/servers

echo $update_server_P >>/app/radius/radius_config/servers
     if [ -z "$S_Server" ]
     then 
     	echo ########### >>/app/radius/radius_config/servers
     else
     	echo $update_server_S >>/app/radius/radius_config/servers
     fi
     
     
echo '</body>'
echo '</html>'

exit 0

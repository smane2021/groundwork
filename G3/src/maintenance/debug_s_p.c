/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : debug_s_p.c
 *  CREATOR  : LIXIDONG                 DATE: 2004-11-10 14:17
 *  VERSION  : V1.00
 *  PURPOSE  : Debug Service Program. It will run by itself.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include <signal.h>
#include "stdsys.h"
#include "public.h"

#include "../service/maintenance_intf/debug_maintn.h"
#include "../mainapp/main/main.h"
#include "debug_s_p.h"

#ifdef _DEBUG
//#define _DEBUG_MSP_	1
#endif //_DEBUG

//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO  g_SiteInfo;

//need to be defined in Miscellaneous Function Module
TIME_SRV_INFO g_sTimeSrvInfo;

//need to be defined in equip management Module

int Equip_Control(IN int nSenderType, IN char *pszSenderName,
				  IN int nSendDirectly,
				  IN int nEquipID, IN int nSigType, IN int nSigID, 
				  IN VAR_VALUE *pCtrlValue, IN DWORD dwTimeout)
{
	UNUSED(nSenderType);
	UNUSED(pszSenderName);
	UNUSED(nSendDirectly);
	UNUSED(nEquipID);
	UNUSED(nSigType);
	UNUSED(nSigID);
	UNUSED(pCtrlValue);
	UNUSED(dwTimeout);
	return 0;
}


//need to be defined in Miscellaneous Function Module
int UpdateSCUPTime (time_t* pTimeRet)
{
	TRACE("\n***UpdateSCUPTime in debug_s_p.c\n");
	return stime(pTimeRet);
}

//need to be defined in Miscellaneous Function Module
int UpdateNTPTime(void)
{
	return 1;
}
//STUB: used in libapp.so, but NOT need in this APP.mfh
int WriteRunConfigItem(RUN_CONFIG_ITEM* pRunConfigItem)
{
	UNUSED(pRunConfigItem);
	return 0;
}

char g_szACUConfigDir[MAX_FILE_PATH];
SERVICE_MANAGER		g_ServiceManager;

extern int	Dsp_ProcessCmdLine(IN int argc, IN char *argv[]);
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//DSP------Debug Service Program
///////////////////////////////////////////////////////////////////////////////
//Start debug service program main code

// the init and exit modules shall be called in main.


///////////////////////////////////////////////////////////////////////////////
static void DSP_ExitAbnormally(int iRunningState);


#define DSP_IS_INITIALIZING		0
#define DSP_IS_RUNNING			1
#define DSP_IS_EXITING			2

#define DSP_IS_INTERRUPTED		-1	// killed/stopped by user
#define DSP_IS_KILLED			-2	// 10 Ctrl-C will kill the system.
#define DSP_IS_ABNORMAL			-3	// abnormal for threads
#define DSP_IS_FAULT			-4

static int s_iDspSystemRunningState = DSP_IS_RUNNING;
static int s_iDspSystemExitingCode  = 0;

static SYS_INIT_EXIT_MODULE	s_DspInitExitModule[] =
{
#ifdef _HAS_LCD_UI
	// Added the display function. maofuhua, 2005-2-25
	DEF_INIT_EXIT_MODULE("Display Maintenace Info",FALSE,	ERR_INIT_DATA_MGMT,		
						DSP_InitMaintenaceScreen,	NULL,			NULL,
						DSP_DestroyMaintenaceScreen,NULL,			NULL),
#endif //_HAS_LCD_UI
						/*
    DEF_INIT_EXIT_MODULE("Service Manager",			TRUE,	ERR_CREATING_SERVICE,
						ServiceManager_Start,		&g_ServiceManager,	NULL,
						ServiceManager_Stop,		&g_ServiceManager,	10000),//10s
						*/
	
	DEF_INIT_EXIT_MODULE("Data Management Interface",TRUE,	ERR_INIT_DATA_MGMT,		
						DAT_IniMngHisData,			NULL,			NULL,
						DAT_ClearResource,			NULL,			NULL),
		
	DEF_INIT_EXIT_MODULE("Data Exchange Interface",	TRUE,	ERR_INIT_DXI,	
						InitDataExchangeModule,		NULL,			NULL,
						DestroyDataExchangeModule,	NULL,			NULL),
	

	DEF_INIT_EXIT_MODULE("Debug Service Program",   TRUE,	ERR_DBS_INIT_PRO,		
						DBS_IniDebugPro,			NULL,			NULL,
						DBS_ExitDebugService,		NULL,			NULL),

};

//moved to debug_s_p.h, maofuhua,2005-2-25
//#define DSP_MAIN "ACU_DSP"



/*==========================================================================*
 * FUNCTION : DSP_InitModules
 * PURPOSE  : init the modules by normal sequence.
 * CALLS    : 
 * CALLED BY: DSP_Init
 * ARGUMENTS:   void : 
 * RETURN   : static int : if any mandatory module fails on init, 
 *                         return its errcode, or return OK.
 * COMMENTS : 
 * CREATOR  : LIXIDONG         DATE: 2004-12-03 11:13
 *==========================================================================*/
static int DSP_InitModules(void)
{
	int						i;
	BOOL					bResult;
	SYS_INIT_EXIT_MODULE	*pModule;
	int						iResult;

	iResult = ERR_DBS_OK;
	AppLogOut(DSP_MAIN, APP_LOG_INFO, "Initializing system modules...\n");

	for (i = 1; i <= ITEM_OF(s_DspInitExitModule); i++)
	{
		pModule = &s_DspInitExitModule[i-1];

		if (pModule->pfnInit != NULL)
		{
			AppLogOut(DSP_MAIN, APP_LOG_INFO, "%d: Initializing %s...\n",
				i, pModule->pszModuleName);

			bResult = pModule->pfnInit(
				pModule->pInitArgs[0],
				pModule->pInitArgs[1]);

			AppLogOut(DSP_MAIN, 
				bResult ? APP_LOG_INFO : APP_LOG_ERROR, 
				"%d: %s is %s successfully initialized.\n",
				i, pModule->pszModuleName,
				bResult ? "now" : "NOT");

			if (!bResult && pModule->bMandatoryModule) //maofuhua added the last cond.
			{
				iResult = ERR_DBS_FAIL;
				break;
			}
		}
	}

	AppLogOut(DSP_MAIN, (iResult == ERR_DBS_OK) ? APP_LOG_INFO : APP_LOG_ERROR,
		"Debug service modules are %s successully initialized.\n",
		(iResult == ERR_DBS_OK) ? "now" : "NOT");

	return iResult;
}


/*==========================================================================*
 * FUNCTION : DSP_DestroyModules
 * PURPOSE  : Destroy the module by reverse sequence.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : always OK
 * COMMENTS : 
 * CREATOR  : LIXIDONG         DATE: 2004-12-03 11:15
 *==========================================================================*/
static int DSP_DestroyModules(void)
{
	int						i;
	SYS_INIT_EXIT_MODULE	*pModule;

	AppLogOut(DSP_MAIN, APP_LOG_UNUSED, "Destroying system modules...\n");

	// call destroy function by reverse sequence
	for (i = ITEM_OF(s_DspInitExitModule); i > 0; i--)
	{
		pModule = &s_DspInitExitModule[i-1];

		if (pModule->pfnExit != NULL)
		{
			AppLogOut(DSP_MAIN, APP_LOG_UNUSED, "%d: Destroying %s...\n",
				i, pModule->pszModuleName);

			pModule->pfnExit(
				pModule->pExitArgs[0],
				pModule->pExitArgs[1]);

			AppLogOut(DSP_MAIN, APP_LOG_UNUSED, "%d: %s is successfully destroyed.\n",
				i, pModule->pszModuleName);
		}
	}

	AppLogOut(DSP_MAIN, APP_LOG_INFO, "DSP modules are destroyed.\n");

	return ERR_DBS_OK;
}


/*==========================================================================*
 * FUNCTION : DSP_RunThreadEventHandler
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN DWORD      dwThreadEvent  : //	the thread event. below.
 *            IN HANDLE     hThread        : //	The thread id
 *            IN const char *pszThreadName : 
 * RETURN   : static DWORD : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG         DATE: 2004-11-17 20:18
 *==========================================================================*/
static DWORD DSP_RunThreadEventHandler(
	IN DWORD	dwThreadEvent,
	IN HANDLE	hThread,
	IN const char *pszThreadName)
{
	UNUSED(pszThreadName);

	WatchDog_Feed();

	if (THREAD_EVENT_NO_RESPONSE == dwThreadEvent)
	{
		AppLogOut( DSP_MAIN, APP_LOG_ERROR,
			"Thread %s(%p) is no response, it will be killed, "
			"and the debug service will be restarted!\n",
			pszThreadName, hThread);

		s_iDspSystemRunningState = DSP_IS_EXITING;
		s_iDspSystemExitingCode  = SYS_EXIT_BY_THREAD;

		//continue this thread, but we need quit system
		//return THREAD_CONTINUE_THIS;
	}
	/*
	else if (THREAD_EVENT_NO_RESPONSE == dwThreadEvent)
	{
		AppLogOut( DSP_MAIN, APP_LOG_ERROR,
			"Thread %s(%p) is no response, it will be killed, "
			"and the system will be restarted!\n",
			pszThreadName, hThread);

		s_iDspSystemRunningState = DSP_IS_ABNORMAL;

		//continue this thread, but we need quit system
		//return THREAD_CONTINUE_THIS;
	}
	*/

	return THREAD_CONTINUE_RUN;
}


/*==========================================================================*
 * FUNCTION : DSP_HandleStopSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG         DATE: 2004-11-18 13:32
 *==========================================================================*/
static void DSP_HandleStopSignals(void)
{
	static int		iKillCount = 0;
	static time_t	tmLastSignaled = 0;
	time_t			tmNow = time(NULL);

	if (tmNow - tmLastSignaled > 10)	// 10 seconds
	{
		iKillCount = 0;
	}

	tmLastSignaled = tmNow;

	iKillCount++;

	switch (iKillCount)
	{
	case 1:
#define SYS_PREQUIT_MSG	"Received a quit command, please send "\
			"command again to stop system.\n"
		AppLogOut(DSP_MAIN, APP_LOG_WARNING, "%s", SYS_PREQUIT_MSG);
		printf("%s", SYS_PREQUIT_MSG);

		break;

	case 2:
#define SYS_INTR_MSG	"Received two quit commands, system is now quiting...\n"
		AppLogOut(DSP_MAIN, APP_LOG_WARNING, "%s", SYS_INTR_MSG);

		printf("%s", SYS_INTR_MSG);
		s_iDspSystemRunningState = DSP_IS_EXITING;
		s_iDspSystemExitingCode  = SYS_EXIT_BY_USER;

		break;

	case 10:
#define SYS_KILL_MSG	"System aborted due to too many quit commands Ctrl-C.\n"
		printf("%s", SYS_KILL_MSG);
		AppLogOut(DSP_MAIN, APP_LOG_WARNING, "%s", SYS_KILL_MSG);

		// exit with no cleanup.
		s_iDspSystemRunningState = DSP_IS_KILLED;

		_exit(-1);
		break;

	default:
		if (iKillCount >= 15)
		{
			DSP_ExitAbnormally(s_iDspSystemRunningState);
		}

		printf( "System is quiting, please wait... \n" );
	}
}



/*==========================================================================*
 * FUNCTION : DSP_SignalHandler
 * PURPOSE  : 
 * CALLS    : DSP_ExitAbnormally,DSP_HandleStopSignals
 * CALLED BY: DSP_InstallSigHandler
 * ARGUMENTS: IN int  nSig : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-11 09:38
 *==========================================================================*/
static void DSP_SignalHandler(IN int iSig)
{
	TRACEX("Received a signal #%d.\n", iSig);

	switch (iSig)
	{
	case SIGSEGV:
		DSP_ExitAbnormally(s_iDspSystemRunningState);	// never returns
		s_iDspSystemRunningState = DSP_IS_FAULT;	
		break;

	case SIGTERM:	
	case SIGKILL:
	case SIGQUIT:
		DSP_HandleStopSignals();
		break;

	case SIGINT:	// CTRL+C, the number of signal will be sent to app 
					// when ctrl+c is pressed in ACU board, ignore it.
	case SIGPIPE:	// pipe broken, continue to run
	default:
		break;
	}
}

/*==========================================================================*
 * FUNCTION : DSP_InstallSigHandler
 * PURPOSE  : Define signal and install signal with signal process functions
 * CALLS    : DSP_SignalHandler
 * CALLED BY: 
 * ARGUMENTS: BOOL  bInstall : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG         DATE: 2004-12-06 10:37
 *==========================================================================*/
static BOOL DSP_InstallSigHandler(BOOL bInstall)
{	
	int	i;

	int pHandledSignals[] = 
	{	
		SIGTERM, SIGINT, SIGKILL, SIGQUIT, // Kill signal or Ctrl+C
		SIGPIPE,							// broken pipe.
		SIGSEGV,							// segment fault error.
	};

	UNUSED(bInstall);

	// 0. init msg handler.
	for (i = 0; i < ITEM_OF(pHandledSignals); i++)
	{
		signal(pHandledSignals[i], DSP_SignalHandler);
	}

	return TRUE;	
}

/*==========================================================================*
 * FUNCTION : DSP_CheckRootDir
 * PURPOSE  : Not used so far
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszExecName : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-12 11:44
 *==========================================================================*/
/*
static void DSP_CheckRootDir(char *pszExecName)
{
    char *pszConfigEnv = getenv(ENV_ACU_ROOT_DIR);

	if (pszConfigEnv == NULL)
	{
		static char	szEnvRootDir[MAX_FILE_PATH];	// must be static.
		char	szRootDir[MAX_FILE_PATH], *pExecPath;

		pExecPath = strrchr(pszExecName, '/');
		if (pExecPath != NULL)
		{
			// add one extra room for '\0'
			size_t		len = (pExecPath-pszExecName)+1;

			// copy the dir in the exec file. 
			strncpyz(szRootDir, pszExecName, (int)MIN(len, sizeof(szRootDir)));
		}
		else
		{
			strcpy(szRootDir, ".");
		}

		printf("Please set the environment variable '%s' of ACU root dir.!\n"
			"The default dir \"%s\" will be used.\n",
			ENV_ACU_ROOT_DIR,
			szRootDir);

		snprintf(szEnvRootDir, sizeof(szEnvRootDir), "%s=%s",
			ENV_ACU_ROOT_DIR, szRootDir);
	
		putenv(szEnvRootDir);	// set back the env.

		TRACEX("ENV is: %s. got env is %s\n",
			szEnvRootDir, getenv(ENV_ACU_ROOT_DIR));

		ASSERT(strcmp(szRootDir, getenv(ENV_ACU_ROOT_DIR)) == 0);
	}
}
*/

/*==========================================================================*
 * FUNCTION : DSP_Init
 * PURPOSE  : init services and everythings
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN MAIN_ARGS  *THIS : 
 * RETURN   : static int : ERR_OK for OK
 * COMMENTS : 
 * CREATOR  : LIXIDONG         DATE: 2004-11-17 20:22
 *==========================================================================*/
static int DSP_Init(void)
{
	int		iResult;

	s_iDspSystemRunningState = DSP_IS_INITIALIZING;

	//{{add the LCD displaying function. 128*64pix=16*4chars(8x16)
	//       ACU  Maintenance 
	//}}

	// 1. starting msg.
	AppLogOut(DSP_MAIN, APP_LOG_MILESTONE, "+-------------------------------------+\n");
	Sleep(1000); //Ensure log initiated normally
	AppLogOut(DSP_MAIN, APP_LOG_MILESTONE, "|   Debug Service now is starting...  |\n");
	AppLogOut(DSP_MAIN, APP_LOG_MILESTONE, "+-------------------------------------+\n");

	/*
	if (!WatchDog_Open())
	{
		AppLogOut(DSP_MAIN, APP_LOG_ERROR,
			"Fails on opening and enabling the watch-dog.\n");
	}
	*/   //it is moved to main function


	// 0. init msg handler.in order to process signal during running
	DSP_InstallSigHandler(TRUE);


	// 2. Init thread mamager
	iResult = RunThread_ManagerInit(DSP_RunThreadEventHandler);	// must be at 1st

	// 3. init sys modules.
	iResult = DSP_InitModules();

	return iResult;
}



/*==========================================================================*
 * FUNCTION : DSP_Run
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-10 10:58
 *==========================================================================*/
static int DSP_Run(void)
{

	Sleep(5000);	// sleep before log running msg

	// the program will receive a quit signal while system is starting.
	if (s_iDspSystemRunningState != DSP_IS_INITIALIZING)
	{
		return s_iDspSystemExitingCode;
	}

	s_iDspSystemRunningState = DSP_IS_RUNNING;

	AppLogOut(DSP_MAIN, APP_LOG_MILESTONE, "+-------------------------------------+\n");
	AppLogOut(DSP_MAIN, APP_LOG_MILESTONE, "|   Debug Service now is running...   |\n");
	AppLogOut(DSP_MAIN, APP_LOG_MILESTONE, "+-------------------------------------+\n");

	// running
	while (s_iDspSystemRunningState == DSP_IS_RUNNING)
	{
		WatchDog_Feed();

		Sleep(1000);		// sleep 1 second.
		
		/*
		if (!THREAD_IS_RUNNING(g_ServiceManager.hServiceManager))
		{
			AppLogOut(DSP_MAIN, APP_LOG_ERROR,
				"PANIC! The service manager exited abnormally, "
				"the system will quit and restart now.\n");
			s_iDspSystemRunningState = DSP_IS_EXITING;
			s_iDspSystemExitingCode  = SYS_EXIT_BY_SERVICE;
		}
		*/
		
	}

	return s_iDspSystemExitingCode;
}




/*==========================================================================*
 * FUNCTION : DSP_Exit
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  nCurrentStatus : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-10 11:03
 *==========================================================================*/
static int DSP_Exit(IN int iCurrentStatus)
{
	//s_iDspSystemRunningState = SYS_IS_EXITING;	// other value is already sett!
	AppLogOut(DSP_MAIN, APP_LOG_INFO, "+-------------------------------------+\n");
	AppLogOut(DSP_MAIN, APP_LOG_MILESTONE, "|   Debug Service now is quiting...   |\n");
	AppLogOut(DSP_MAIN, APP_LOG_INFO, "+-------------------------------------+\n");

	// destroy sys modules
	DSP_DestroyModules();

	// stop the  thread manager.
	RunThread_ManagerExit(10000, TRUE);	// must be at last

	AppLogOut(DSP_MAIN, APP_LOG_INFO, "+----------------------------------+\n");
	AppLogOut(DSP_MAIN, APP_LOG_MILESTONE, "|   Debug service system exited %s  |\n",
		(iCurrentStatus == SYS_EXIT_BY_USER) ? "normally.  " : "abnormally." );
	AppLogOut(DSP_MAIN, APP_LOG_INFO, "+----------------------------------+\n\n\n");

	
	return iCurrentStatus;
}

/*==========================================================================*
 * FUNCTION : DSP_ExitAbnormally
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nRunningState : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG         DATE: 2004-11-20 19:50
 *==========================================================================*/
static void DSP_ExitAbnormally(int iRunningState)
{
	char	*pStates[] = {"initializing", "running", "exiting"};

	if ((iRunningState < 0) || (iRunningState > ITEM_OF(pStates)))
	{
		iRunningState = DSP_IS_EXITING;
	}

	AppLogOut(DSP_MAIN, APP_LOG_ERROR, 
		"PANIC: System received SIGSEGV(segment fault) signal "
		"while %s, system exit.\n",
		pStates[iRunningState]);

	WatchDog_Close(FALSE);	// do NOT disable dog

	_exit(-1);	// do NOT cleanup if fails exit().
}

/*==========================================================================*
 * FUNCTION : main
 * PURPOSE  : This is the debug service program main function, it will be
 *			  used by itself.Use the debug_maintn.so
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-10 14:26
 *==========================================================================*/
int main(int argc, char *argv[])
{
	int iExitStatus;

	// 0. init msg handler.
	//DSP_InstallSigHandler(TRUE);

#ifdef _DEBUG_MSP_
	printf("Come here1, please waiting.\n");
#endif

	if (!WatchDog_Open())
	{
		AppLogOut(DSP_MAIN, APP_LOG_ERROR,
			"Debug Service starting. Fails on opening and enabling the watch-dog.\n");
	}

	//{{ parse the command line. lixidong added. 2005-3-8
	if (Dsp_ProcessCmdLine(argc, argv) == -1)
	{
		// return -1 when show help only.
		WatchDog_Close(TRUE);
		return 0;
	}
	//}}
	
	// 1. init
	iExitStatus = DSP_Init();

	if (iExitStatus != ERR_DBS_OK)
	{
		AppLogOut(DSP_MAIN, APP_LOG_ERROR,
			"Init system got error code %d(%x), system exit.\n",
			iExitStatus, iExitStatus);

		iExitStatus = DSP_IS_ABNORMAL;
	}

	// 2. If succeed to initiate, run clear watch_dog
	else 
	{
		iExitStatus = DSP_Run();
	}

	// 3. cleanup
	DSP_Exit(iExitStatus);

	// disable and close watch dog
	WatchDog_Close((iExitStatus == SYS_EXIT_BY_USER) 
		? TRUE		// for user interrupt, stop and disable dog
		: FALSE);	// for abnormally stop, do NOT disabel dog to make dog reset system

	return iExitStatus;
}
////////////////////
////////////////////

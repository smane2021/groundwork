/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : debug_lcd_msg.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-25 20:23
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"  
#include "public.h"  

#include "debug_s_p.h"

//#include "lcd_interface.h"
//#include "lcd_font_lib.h"

/*==========================================================================*
 * FUNCTION : DSP_GetLaunchingReason
 * PURPOSE  : get the luanch reason from environment var:ACU_EXIT_CODE
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static char *: 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-25 19:38
 *==========================================================================*/
static char *DSP_GetLaunchingReason(void)
{
	char	*pExitCodeOfACU = getenv("ACU_EXIT_CODE");
	char	*pExitReasonList[] =
	{
  	   //1234567890ABCDEF
		"   User Launch  ", //SYS_EXIT_BY_USER		/killed/stopped by user.
		"  PowerKit Call ", //SYS_EXIT_BY_SWAP		/for swap to other program. 
		"Init/Cfg Failure", //SYS_EXIT_BY_INIT		/for init error.
		" Thread Failure ", //SYS_EXIT_BY_THREAD	/for abnormal threads.
		" Service Failure", //SYS_EXIT_BY_SERVICE	/for abnormal service
		"  Running Fault ", //SYS_EXIT_BY_FAULT	/for fault errors, such as segment fault.
		"    Unknown     "	 	 
	};
	int		pExitCodesList[] = // must be matched with pExitReasonList
	{
		SYS_EXIT_BY_USER,		
		SYS_EXIT_BY_SWAP,		
		SYS_EXIT_BY_INIT,		
		SYS_EXIT_BY_THREAD,	
		SYS_EXIT_BY_SERVICE,	
		SYS_EXIT_BY_FAULT
	};

	int		i, nExitCodeOfACU = 0;

	if (pExitCodeOfACU != NULL)
	{
		nExitCodeOfACU = atoi(pExitCodeOfACU);
	}

	ASSERT(ITEM_OF(pExitCodesList) < ITEM_OF(pExitReasonList));

	for (i = 0; i < ITEM_OF(pExitCodesList); i++)
	{
		if (pExitCodesList[i] == nExitCodeOfACU)
		{
			break;
		}
	}

	// if the code is NOT found, the default text of "unknown" will be returned.
	return pExitReasonList[i];
}

//stub
BOOL SetBuzzerBeepThredID(HANDLE hThreadID)
{
	UNUSED(hThreadID);
	return TRUE;
}

static	char	*s_pszLangCode= "en";	// ascii language type, only support ENGLISH
static  int		s_nFontHeight= BASE_HEIGHT*2;
static  HANDLE	s_hMaintenanceWaitKeyThread = NULL;
static	char	*s_pMaintenanceReason = "";

static	char	s_pRebootPromptText1[] = "ENT to Reboot   ";
static	char	s_pRebootPromptText2[] = "ENT again Reboot";
static	char	s_pRebootPromptText3[] = "The application Rebooting  ";
static	char	*s_pRebootPromptText4  = "   MSP Exited   ";

static  char	*s_ppMaintenaceInfo[] =
{
   //1234567890ABCDEF
	"App Maintenance",
	"Maintn Reason:  ",
	"                ", // for s_pMaintenanceReason, PowerKit Call, etc.
//	"[ENT] to Reboot ",
	s_pRebootPromptText1,
};



#define MSP_LCD_TIME_FMT		" %y%m%d %H:%M:%S"	// YYMMDD hh:mm:ss
#define PERIOD_WAIT_2_ENT_KEY	10	// sec, time delay of waiting next ENT
#define PERIOD_SWAP_TITLE_TIME	3	// sec
#define KEY_BEEP_DELAY_TIME		100	// ms, delay time of the key sound

/*==========================================================================*
 * FUNCTION : DSP_MaintenanceWaitKeyThread
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN void  *lpParam : 
 * RETURN   : static DWORD : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-25 21:21
 *==========================================================================*/
static DWORD DSP_MaintenanceWaitKeyThread(IN void *pUnused)
{
	HANDLE	hSelf = RunThread_GetId(NULL);
	time_t	tmLastEnterPressed = 0, tmLastSwapTitle = 0;
	UCHAR	byKeyValue;
	time_t  tmNow;
	BOOL	bBeeping = TRUE;	// force to stop beep.
	char	szTimeNow[32];
	char	*pTitle = szTimeNow;// title text.

	UNUSED(pUnused);

	BuzzerBeep(TRUE);
	Sleep(2000);

	while (THREAD_IS_RUNNING(hSelf))
	{		
		RunThread_Heartbeat(hSelf);	//heartbeat

		tmNow = time(NULL);

		byKeyValue = GetKey();
		if(byKeyValue != VK_NO_KEY)
		{
			BuzzerBeep(TRUE);
			bBeeping = TRUE;

			switch (byKeyValue)
			{
			case VK_ENTER:
				if (tmLastEnterPressed == 0)
				{
					tmLastEnterPressed = tmNow;

					// display reboot msg reversed color
					DisString(s_pRebootPromptText2, 
						(int)strlen(s_pRebootPromptText2),
						0, 3*s_nFontHeight,	// the 4th line, y start from 0.
						s_pszLangCode, FLAG_DIS_REVERSE_TYPE);
					break;
				}
				else
				{
					if (tmNow - tmLastEnterPressed <= PERIOD_WAIT_2_ENT_KEY)
					{
						TRACEX("Rebooting system...\n");

						DisString(s_pRebootPromptText3, 
							(int)strlen(s_pRebootPromptText3),
							0, 3*s_nFontHeight,	// the 4th line, y start from 0.
							s_pszLangCode, FLAG_DIS_REVERSE_TYPE);

						s_pRebootPromptText4 = s_pRebootPromptText3;

						Sleep(2000);
						BuzzerBeep(FALSE);
						////////////////////////
						DSP_Reboot_System();
						Sleep(5000);
						////////////////////////

						return 0;
					}
				}
				//break;	// no-break here.

			default:
				if (tmLastEnterPressed != 0)
				{
					DisString(s_pRebootPromptText1, 
						(int)strlen(s_pRebootPromptText1),
						0, 3*s_nFontHeight,	// the 4th line, y start from 0.
						s_pszLangCode, FLAG_DIS_NORMAL_TYPE);
					tmLastEnterPressed = 0;	// discard the last pressed ENT.
				}
			}
		}
		else
		{
			Sleep(KEY_BEEP_DELAY_TIME);

			if (bBeeping)
			{
				bBeeping = FALSE;
				BuzzerBeep(FALSE);
			}

			// discard the first ENT key if timeout when waiting the next ENT.
			if (tmLastEnterPressed != 0)
			{
				if (tmNow - tmLastEnterPressed > PERIOD_WAIT_2_ENT_KEY)
				{
					DisString(s_pRebootPromptText1, 
						(int)strlen(s_pRebootPromptText1),
						0, 3*s_nFontHeight,	// the 4th line, y start from 0.
						s_pszLangCode, FLAG_DIS_NORMAL_TYPE);
					tmLastEnterPressed = 0;	// discard the last pressed ENT.
				}
			}

			//swap time and title
			if (tmLastSwapTitle != tmNow)
			{
				// swap title
				if ((tmNow % PERIOD_SWAP_TITLE_TIME) == 0)
				{
					pTitle = (pTitle == szTimeNow) 
						? s_ppMaintenaceInfo[0] : szTimeNow;
				}
				
				if (pTitle == szTimeNow)	// show time
				{
					TimeToString(tmNow, MSP_LCD_TIME_FMT,	// show time.	
							szTimeNow, sizeof(szTimeNow));
				}

				DisString(pTitle, 
					(int)strlen(pTitle),
					0, 0,				// the 1st line, y start from 0.
					s_pszLangCode, FLAG_DIS_NORMAL_TYPE);

				tmLastSwapTitle = tmNow;
			}
		}
	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : DSP_InitMaintenaceScreen
 * PURPOSE  : init the screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-25 19:30
 *==========================================================================*/
BOOL DSP_InitMaintenaceScreen(void)
{
	int		i;

	s_pMaintenanceReason = DSP_GetLaunchingReason();

	//1. init LCD and clear LCD
	if (InitLCD() != ERR_LCD_OK)
	{
		Sleep(5000);	// try again after 5 sec.
		if (InitLCD() != ERR_LCD_OK)
		{
			AppLogOut(DSP_MAIN, APP_LOG_WARNING,
				"Fails on initializing LCD.\n",
				s_pszLangCode);
			return FALSE;
		}
	}

	//Light(TRUE);
	ClearScreen();

	//2. load ASCII lib
	if (RegisterFontLib(1, &s_pszLangCode, FLAG_REGIST_FONT_LIB) != ERR_LCD_OK)
	{
		AppLogOut(DSP_MAIN, APP_LOG_WARNING,
			"Fails on loading font of lannguage code '%s'.\n",
			s_pszLangCode);

		return FALSE;
	}

	s_nFontHeight = GetFontHeight(s_pszLangCode);
	
	// display text
#define ENG_TEXT_HEIGHT		(BASE_HEIGHT*2)
	for (i = 0; i < ITEM_OF(s_ppMaintenaceInfo); i++)
	{
		DisString(s_ppMaintenaceInfo[i], (int)strlen(s_ppMaintenaceInfo[i]), 
			0, i*s_nFontHeight, 
			s_pszLangCode, FLAG_DIS_NORMAL_TYPE);
	}

	// display maintenace reason with reversed color
	DisString(s_pMaintenanceReason, (int)strlen(s_pMaintenanceReason),
		0, 2*s_nFontHeight,	// the 3rd line, y start from 0.
		s_pszLangCode, FLAG_DIS_REVERSE_TYPE);

	// start the key receive thread
	s_hMaintenanceWaitKeyThread = RunThread_Create("Maintn LCD Key",
				(RUN_THREAD_START_PROC)DSP_MaintenanceWaitKeyThread,
				(void *)NULL,
				NULL,
				0);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : DSP_DestroyMaintenaceScreen
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  void : 
 * RETURN   :  void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-25 19:30
 *==========================================================================*/
void DSP_DestroyMaintenaceScreen(void)
{
	//stop key thread
	if (s_hMaintenanceWaitKeyThread != NULL)
	{
		RunThread_Stop(s_hMaintenanceWaitKeyThread,	2000, TRUE);
		s_hMaintenanceWaitKeyThread = NULL;

		ClearScreen();
		DisString(s_pRebootPromptText4, (int)strlen(s_pRebootPromptText4),
			0, (SCREEN_HEIGHT-s_nFontHeight)/2, 
			s_pszLangCode, FLAG_DIS_NORMAL_TYPE);
	}

	// clear LCD driver and font
	//Do NOT clear screen.keep the display msg when rebooting.
	//ClearScreen();
	//Light(FALSE);
	RegisterFontLib(1, &s_pszLangCode, FLAG_UNREGIST_FONT_LIB);//2. load ASCII lib
	CloseLcdDriver();
}


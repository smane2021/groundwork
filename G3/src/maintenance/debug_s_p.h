/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : debug_maintn.h
 *  CREATOR  : LIXIDONG                 DATE: 2004-09-24 10:56
 *  VERSION  : V1.00
 *  PURPOSE  : This file will be used in communicating with powerkit 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef _DEBUG_S_P_H
#define _DEBUG_S_P_H

#define DSP_MAIN "SCUP_MSP"

struct tagDspSiteInfo
{
#define DSP_CONFIG_DIR					"/config/"
#define DSP_CONFIG_BAK_DIR				"/config_bak/"
#define DSP_CONFIG_DEFAULT_DIR			"/config_default/"

#define DSP_CFG_NORMAL_TYPE		0	// The being used config is normal
#define DSP_CFG_BACKUP_TYPE		1	// the being used config is backup
#define DSP_CFG_DEFAULT_TYPE	2	// the being used config is default.


	BOOL	bStatDataReported;			// TRUE for stat data is reported OK
};
typedef struct tagDspSiteInfo DSP_SITE_INFO;

extern void DSP_Reboot_System(void);
//LCD init/destroy function. maofuhua, 2005-2-25
extern BOOL DSP_InitMaintenaceScreen(void);
extern void DSP_DestroyMaintenaceScreen(void);

#endif //_DEBUG_MAINTN_H


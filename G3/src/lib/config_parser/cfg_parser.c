/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : cfg_parser.c
 *  CREATOR  : LinTao                      DATE: 2004-09-07
 *  VERSION  : V1.0
 *  PURPOSE  : Source file for Config Parser
 *
 *  HISTORY  : based on the EMNET project ( Author: maofuhua )
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

//#define FILE_END_WITH_0     1



#ifndef FILE_END_WITH_0
    #define END_OF_PROF(prof)   ( (prof)->pReadPtr == (prof)->pEndFile )
#else 
    #define END_OF_PROF(prof)   ( *(prof)->pReadPtr == 0 )  	/* FILE_END_WITH_0 */
#endif 

#define CR      (0x0D)
#define LF      (0x0A)
#define SPACE   (0x20)
#define TAB     (0x09)
#define COMMENT_FLAG        "#"

#define IS_WHITE_SPACE(c)         ( ((c) == SPACE) || ((c) == TAB) )
#define END_OF_LINE(c)      ( ((c) == CR) || ((c) == LF) )

#define SECT_PREFIX     '['
#define SECT_SUFFIX     ']'

/*==========================================================================*
 * FUNCTION : Cfg_ProfileOpen
 * PURPOSE  : Create SProfile structure
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *szFileContents (in): file contents buffer
 *            int   nFileSize       (in): file length
 * RETURN   : void * : point to the returned SProfile sturcture
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
void *Cfg_ProfileOpen(IN char *szFileContents, IN int nFileSize)
{
    SProfile    *pProf = NULL;

#ifndef FILE_END_WITH_0
    if ((szFileContents != NULL) && (nFileSize > 0))
#else
    if (szFileContents != NULL)
#endif
    {
        pProf = NEW(SProfile, 1);

		if (pProf == NULL)
		{
			return NULL;
		}

        pProf->pFileBuffer = szFileContents;
#ifndef FILE_END_WITH_0
        pProf->pEndFile    = szFileContents + nFileSize;
#endif 	
        pProf->pReadPtr    = pProf->pFileBuffer;
    }

    return (void *)pProf;
}

/*==========================================================================*
 * FUNCTION : Cfg_ProfileRewind
 * PURPOSE  : reset pReadPtr pointer
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pProf (in) : SProfile structure 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
void Cfg_ProfileRewind(IN void *pProf)
{
    ((SProfile *)pProf)->pReadPtr = ((SProfile *)pProf)->pFileBuffer;
}

/*==========================================================================*
 * FUNCTION : Cfg_ProfileTell
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN void  *pProf : 
 * RETURN   : long : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-24 11:04
 *==========================================================================*/
long Cfg_ProfileTell(IN void *pProf)
{
	SProfile *pFile;
	long     lCurPos;

	pFile = (SProfile *)pProf;

	lCurPos = pFile->pReadPtr - pFile->pFileBuffer;
	return lCurPos;
}

/*==========================================================================*
 * FUNCTION : Cfg_ProfileClose
 * PURPOSE  : close the SProfile structure 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pProf (in): the SProfile structure
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
void Cfg_ProfileClose(IN void *pProf)
{
	SProfile *p;

	p = (SProfile *)pProf;

    if (p != NULL)
    {
		if (p->pFileBuffer != NULL)
		{
			/* the memory is newed in Cfg_LoadConfigFile */
			DELETE(p->pFileBuffer); 
		}
        DELETE(p);
    }
}

/*==========================================================================*
 * FUNCTION : Cfg_SkipWhiteSpace
 * PURPOSE  : skip the CR/LF/SPACE/TAB before reading
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SProfile  *pProf (in): the SProfile structure
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
static void Cfg_SkipWhiteSpace(IN SProfile *pProf)
{
	if(pProf == NULL)
	{
		return;
	}
    while (!END_OF_PROF(pProf) &&               // not end of file
			(END_OF_LINE(*pProf->pReadPtr) ||      // is CR or LF
				IS_WHITE_SPACE(*pProf->pReadPtr)))  // or is TAB or SPACE
    {
        pProf->pReadPtr++;
    }
}

/*==========================================================================*
 * FUNCTION : Cfg_ProfileReadLine
 * PURPOSE  : read a line from file
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SProfile  *pProf  (in)  : the SProfile structure
 *            char      *pszBuf (out) : the buffer to load line data
 *            int       nBufSize(in)  : the buffer size
 * RETURN   : int : the line data length in the buffer,0 means end of file
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
int Cfg_ProfileReadLine(IN SProfile *pProf, OUT char *pszBuf, IN int nBufSize)
{
    int     i = 0;
    int     nWhitePos = -1;

    /* remove all following CR and LF */
    Cfg_SkipWhiteSpace(pProf);

    while (!END_OF_PROF(pProf))
    {
        if (END_OF_LINE(*pProf->pReadPtr))
        {
            break;
        }

        if (i < nBufSize-1)
        {
            if (IS_WHITE_SPACE(*pProf->pReadPtr))
            {
                if (nWhitePos < 0)
                    nWhitePos = i;
            }
            else
                nWhitePos = -1;

            pszBuf[i++] = *pProf->pReadPtr;
        }

        // if the buffer is full, but we do not encounter the CR or LF
        // we continue read till the CR or LF, or end of file
        pProf->pReadPtr++;
    }

    if (nWhitePos >= 0)    // remove the ending white space
    {
        i = nWhitePos;
    }

    pszBuf[i] = 0;        // now line data end with NULL

    return i;
}

/*==========================================================================*
 * FUNCTION : *Cfg_RemoveWhiteSpace
 * PURPOSE  : remove the front and rear space(SPACE or TAB) from the buf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszBuf (in) : the buffer to be dealed with
 * RETURN   : char * : the first non-space ptr  
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
char *Cfg_RemoveWhiteSpace(IN char *pszBuf)
{
    char    *p = pszBuf;
    char    *pFirstNonSpace = pszBuf, *pLastWhiteSpace = NULL;
	BOOL    bFound = FALSE;  /* first non-space character found flag */

    while (*p)
    {
        if (IS_WHITE_SPACE(*p))
        {
            if(pLastWhiteSpace == NULL)
                pLastWhiteSpace = p;
        }
        else 
        {
            if (!bFound)
			{
                pFirstNonSpace = p;
				bFound = TRUE;
			}

            pLastWhiteSpace = NULL;
        }

        p++;
    }

    if (pLastWhiteSpace != NULL)
        *pLastWhiteSpace = 0;

    return pFirstNonSpace;
}

/*==========================================================================*
 * FUNCTION : *Cfg_RemoveBoundChr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char               *pszBuf       : 
 *            IN IS_BOUND_CHR_PROC  pfnIsBoundChr : 
 * RETURN   : char *: 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-16 10:17
 *==========================================================================*/
char *Cfg_RemoveBoundChr(IN char *pszBuf, IN IS_BOUND_CHR_PROC pfnIsBoundChr)
{
    char    *p = pszBuf;
    char    *pFirstBoundChr = pszBuf, *pLastBoundChr = NULL;
	BOOL    bFound = FALSE;  /* first bound character found flag */
	
    while (*p)
    {
        if (pfnIsBoundChr((int)*p))
        {
            if(pLastBoundChr == NULL)
                pLastBoundChr = p;
        }
        else 
        {
            if (!bFound)
			{
                pFirstBoundChr = p;
				bFound = TRUE;
			}

            pLastBoundChr = NULL;
        }

        p++;
    }

    if (pLastBoundChr != NULL)
        *pLastBoundChr = 0;

    return pFirstBoundChr;
}
/*==========================================================================*
 * FUNCTION : Cfg_ProfileFindSectionRealize
 * PURPOSE  : to fullfill finding section task
 * CALLS    : 
 * CALLED BY: Cfg_ProfileFindSection
 * ARGUMENTS: void  *pProf        (in): the SProfile structure 
 *            const char  *pszKey (in): the section-key name 
 * RETURN   : int : 0 for not find
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
static int Cfg_ProfileFindSectionRealize(IN void *pProf, IN const char *pszKey)
{
	SProfile *p;
    int n, nCmp;
    char buf[MAX_LINE_SIZE];

	p = (SProfile *)pProf;

    /* find the key-section */
    nCmp = strlen(pszKey);

    do
    {
        n = Cfg_ProfileReadLine(p, buf, sizeof(buf));
    } while ((n > 0) && (strnicmp(buf, pszKey, (size_t)nCmp) != 0));

    return n;
}

/*==========================================================================*
 * FUNCTION : Cfg_ProfileFindSection
 * PURPOSE  : find a section according to the given section-key (move the pReadPtr   
 *            to the position of the relevant section-value) 
 * CALLS    : Cfg_ProfileFindSectionRealize
 * CALLED BY: 
 * ARGUMENTS: void  *pProf       (in): the SProfile structure
 *            const char  *pszKey (in): the section-key name
 * RETURN   : int : 0 means not find 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
int Cfg_ProfileFindSection(IN void *pProf, IN const char *pszKey)
{
    int n;
	n = Cfg_ProfileFindSectionRealize(pProf, pszKey);

	if( n == 0 )
	{
		//G3_OPT [debug info], by Lin.Tao.Thomas
		//detect un-efficient cfg loading
		//printf("\n\n\npanic! call rewinder. Keyname: %s\n\n\n", pszKey);
		Cfg_ProfileRewind(pProf);
		n = Cfg_ProfileFindSectionRealize(pProf, pszKey);
	}

	return n;
}

/*==========================================================================*
 * FUNCTION : Cfg_ProfileGetLine
 * PURPOSE  : get the first line of the section-value that is belong to the 
 *            given section-key
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pProf               (in) : the SProfile structure
 *            const char  *pszSectionKey (in) : the section-key name
 *            char  *pszBuf              (out): the buffer to load line data
 *            int   nBufSize             (in) : the buffer size
 * RETURN   : int : the filled length of the buffer, -1 means section does not
 *                  exit, 0 means no data. 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
int Cfg_ProfileGetLine(IN void *pProf, IN const char *pszSectionKey, 
					   OUT char *pszBuf, IN int nBufSize)
{
    int n;
    
    n = Cfg_ProfileFindSection(pProf, pszSectionKey);

	if (n == 0 )
	{
		return -1;
	}

	else
    {
        n = Cfg_ProfileGetNextLine(pProf, pszBuf, nBufSize);
    }

    return n;
}

/*==========================================================================*
 * FUNCTION : Cfg_ProfileGetNextLine
 * PURPOSE  : get the next line of a section-value  return 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pProf   (in) : the SProfile structure
 *            char  *pszBuf  (out): the buffer to load line data
 *            int   nBufSize (in) : the buffer size
 * RETURN   : int : the filled length of buffer, 0 means end of the section
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
int Cfg_ProfileGetNextLine(IN void *pProf, OUT char *pszBuf, IN int nBufSize)
{
    int     n;
    char    *p;
	char    *pBufferedReadPtr = NULL;       //buffer the ReadPtr of last line
	SProfile *pFile;

	pFile = (SProfile *)pProf;

    do
    {
        pBufferedReadPtr = pFile->pReadPtr;
		n = Cfg_ProfileReadLine(pFile, pszBuf, nBufSize);

        if (n > 0)
        {
            /* get rid of the comment line */
            p = strstr(pszBuf, COMMENT_FLAG);
            if (p != NULL)
            {
                *p = 0;
            }
        }
    } while ((n > 0) && (*pszBuf == 0));

    if (pszBuf[0] == SECT_PREFIX)// || n == 0)
	{
		pFile->pReadPtr = pBufferedReadPtr;
		n = 0;
	}

    return n;
}

/*==========================================================================*
 * FUNCTION : Cfg_SplitString
 * PURPOSE  : get a string from src, the string that splits from other by the splitter
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszSrc   (in) : src string
 *            char  *pszDst   (out): dst buffer
 *            int   nBufLen   (in) : dst buffer size
 *            int  cSplitter (in) : the splitter character
 * RETURN   : char * : point to the unsplited string of pszSrc
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
char *Cfg_SplitString(IN char *pszSrc, OUT char *pszDst, IN int nBufLen, IN int cSplitter)
{
    char    *p, *q;

    /* skip the first splitter and white space */
    p = pszSrc;
    while ((*p != 0) && ((*p == (char)cSplitter) || IS_WHITE_SPACE(*p)))
    {
        p++;
    }

    /* copy string to dest */
	q = pszDst;
    for ( ; (*p != 0) && (*p != (char)cSplitter); p++ )
    {
        if (nBufLen > 1)
        {
            *q++ = *p;
            nBufLen --;
        }
    }

    *q = 0;

    Cfg_RemoveWhiteSpace(pszDst); // remove the rear space

    return p;
}

/*==========================================================================*
 * FUNCTION : Cfg_SplitStringEx
 * PURPOSE  : ver2, we donot copy string to dest but set the ref addr of dest.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszSrc   (in) : src string
 *            char  **ppszDst (out): the dst pointer address
 *            int  cSplitter (in) : the splitter character
 * RETURN   : char * : unsplited string of pszSrc  
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
char *Cfg_SplitStringEx(IN char *pszSrc, OUT char **ppszDst, IN int cSplitter)
{
    char    *p;

    /* skip the first splitters and white space */
    p = pszSrc;
    while ((*p != 0) && ((*p == (char)cSplitter) || IS_WHITE_SPACE(*p)))
    {
        p++;
    }

    /* set dest addr ref */
    *ppszDst = p;

    for ( ; (*p != 0) && (*p != (char)cSplitter); p++ )
    {
        ;
    }

    if (*p == (char)cSplitter)
    {
        *p = 0; // set the end of string
        p++;   // goto next string
    }

    Cfg_RemoveWhiteSpace(*ppszDst); // remove the rear space

    return p;
}

//Frank Wu,20150525, for TL1
char *Cfg_SplitStringExForTL1(IN char *pszSrc, OUT char **ppszDst, IN int cSplitter, OUT BOOL *pbHasSplitter)
{
	char    *p;

	/* skip the first splitters and white space */
	p = pszSrc;
	//while ((*p != 0) && (/*(*p == (char)cSplitter) || */IS_WHITE_SPACE(*p)))
	//{
	//	p++;
	//}

	/* set dest addr ref */
	*ppszDst = p;

	for ( ; (*p != 0) && (*p != (char)cSplitter); p++ )
	{
		;
	}

	if(NULL != pbHasSplitter)
	{
		*pbHasSplitter = FALSE;
	}

	if (*p == (char)cSplitter)
	{
		*p = 0; // set the end of string
		p++;   // goto next string

		if(NULL != pbHasSplitter)
		{
			*pbHasSplitter = TRUE;
		}
	}

	//Cfg_RemoveWhiteSpace(*ppszDst); // remove the rear space

	return p;
}


/*==========================================================================*
 * FUNCTION : Cfg_SplitStringIndex
 * PURPOSE  : get the sub-string indentified by Index, for G3_OPT [loader]
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszSrc   (in) : src string
 *            char  **ppszDst (out): the dst pointer address
 *            int  cSplitter (in) : the splitter character
 * RETURN   : char * : unsplited string of pszSrc  
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2013-05-24 
 *==========================================================================*/
char *Cfg_SplitStringIndex(IN char *pszSrc, OUT char *pszDst, IN int nBufLen,
						   IN int cSplitter, IN int nIndex)
{
	char    *p, *q;
	int     i;

	/* skip the first splitters and white space */
	p = pszSrc;
	while ((*p != 0) && ((*p == (char)cSplitter) || IS_WHITE_SPACE(*p)))
	{
		p++;
	}

	/* find the Index position */
	for (i = 1; i < nIndex; i++)
	{
		for ( ; (*p != 0) && (*p != (char)cSplitter); p++ )
		{
			;
		}

		while (*p == (char)cSplitter)
		{
			//*p = 0; // set the end of string
			p++;   // goto next sub-string
		}
	}

	/* copy string to dest */
	q = pszDst;
	for ( ; (*p != 0) && (*p != (char)cSplitter); p++ )
	{
		if (nBufLen > 1)
		{
			*q++ = *p;
			nBufLen --;
		}
	}

	*q = 0;

	Cfg_RemoveWhiteSpace(pszDst); // remove the rear space

	return p;
}
/*==========================================================================*
 * FUNCTION : Cfg_GetSplitStringCount
 * PURPOSE  : get the splitter count
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *p        (in): the src string 
 *            int  cSplitter (in): the splitter character
 * RETURN   : int : the splitter count
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
int Cfg_GetSplitStringCount(IN char *p, IN int cSplitter)
{
    int  cnt = 0;

    while (*p)
    {
        // skip the first splitters and white space
        while ((*p != 0) && ((*p == (char)cSplitter) || IS_WHITE_SPACE(*p)))
        {
            p++;
        }
        
        // set dest addr ref.
        cnt++;
        
        // skip the non-white chars
        while ((*p != 0) && (*p != (char)cSplitter))
        {
             p++;
        }
    }

    return cnt;
}

/*==========================================================================*
 * FUNCTION : Cfg_ProfileGetInt
 * PURPOSE  : get 'int' type data of section-value according to the section-key 
 *            (Note: only when the section-value has one line data can use the 
 *            function)
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pCfg          (in) : the SPofile structure 
 *            const char  *pszSectionKey (in) : the section-key name
 *            int   *pNum          (out): to store the int data 
 * RETURN   : int : 1 for success, -1 for fail(such section does not exit or 
 *                  data format is error), 0 means no data.
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
int Cfg_ProfileGetInt(IN void *pCfg, IN const char *pszSectionKey, OUT int *pNum)
{
    char szBuf[32];
    int  i, ret;

	ret = Cfg_ProfileGetLine(pCfg, pszSectionKey, szBuf, sizeof(szBuf));
    if ( ret <= 0)
    {
        return ret;
    }

    // is +nnn or -nnn ?
    i = ((szBuf[0] == '-') || (szBuf[0] == '+')) ? 1 : 0;

    if ((szBuf[i] < '0') || (szBuf[i] > '9'))
        return -1;    // not num error

    *pNum = atoi(szBuf);

    return 1;
}


/*==========================================================================*
 * FUNCTION : Cfg_ProfileGetString
 * PURPOSE  : get 'string' type data of section-value according to the section-key 
 *            (Note: only when the section-value has one line data can use the 
 *            function)
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pCfg                (in) : the SPofile structure 
 *            const char  *pszSectionKey (in) : the section-key name: 
 *            char  *pszBuf              (out): the buffer to load the data
 *            int   nBufLen              (in) : the buffer size
 * RETURN   : int : 1 for success, -1 for fail(such section does not exit or 
 *                  data format is error), 0 means no data.
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-07 
 *==========================================================================*/
int Cfg_ProfileGetString(IN void *pCfg, IN const char *pszSectionKey, 
                     OUT char *pszBuf, IN int nBufLen)
{
    char szBuf[MAX_LINE_SIZE];
	int  ret;

    *pszBuf = 0;

	ret = Cfg_ProfileGetLine(pCfg, pszSectionKey, szBuf, sizeof(szBuf));
    if (ret <= 0)
    {
        return ret;
    }

    strncpy(pszBuf, szBuf, (size_t)nBufLen);
    pszBuf[nBufLen-1] = 0;

    return 1;
}

/*==========================================================================*
 * FUNCTION : Cfg_GetSectionLineCount
 * PURPOSE  : get section line count by section-key
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pProf              (in): the SProfile structure
 *            const char  *szSectionKey (in): section-key name
 * RETURN   : int : the count of the lines, -1 means such section not exit 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-08
 *==========================================================================*/
int Cfg_GetSectionLineCount(IN void *pProf, IN const char *szSectionKey)
{
	int ret, iLineCount = 0;
	char szBuf[MAX_LINE_SIZE];

	long lLastPos = Cfg_ProfileTell(pProf);  //added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4

	ret = Cfg_ProfileFindSection(pProf, szSectionKey);

	if (ret == 0)
	{
		return -1;
	}

	ret = Cfg_ProfileGetNextLine(pProf, szBuf, MAX_LINE_SIZE);
	while (ret != 0)
	{
		iLineCount++;
		ret = Cfg_ProfileGetNextLine(pProf, szBuf, MAX_LINE_SIZE);
	}

	Cfg_ProfileSeekSet(pProf, lLastPos);  //added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4

	return iLineCount;
}


/*==========================================================================*
 * FUNCTION : MoveBackOneLine
 * PURPOSE  : move back pReadPtr a line
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN void  *pProf : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-27 11:11
 *==========================================================================*/
static void MoveBackOneLine(IN void *pProf)
{
	SProfile *pFile = (SProfile *)pProf;
    char *p;

	p = pFile->pReadPtr;

	/* remove current LF or CRs */
	while (END_OF_LINE(*p))
	{
		p--;
	}
	
	/* move back one line */
	while ( p != pFile->pFileBuffer)
	{
		if (END_OF_LINE(*p))
		{
			break;
		}

		p--;
	}

	pFile->pReadPtr = p;
	return;
}


/*==========================================================================*
 * FUNCTION : MoveBackCommentLines
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN void  *pProf : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-27 11:30
 *==========================================================================*/
static void MoveBackCommentLines(IN void *pProf)
{
	SProfile *pFile = (SProfile *)pProf;
	char *p, *pBuf;
	int  iFoundCount = 0;

	do
	{
		pBuf = pFile->pReadPtr;
		MoveBackOneLine(pProf);
		p = pFile->pReadPtr + 1;
		iFoundCount++;
	}
	while (*p == '#');

	/* if found '#' */
	if (iFoundCount > 1)
	{
		/* remove dupplicated LF or CRs */
		while (END_OF_LINE(*pBuf))
		{
			pBuf--;
		}
		pBuf++;
	}

	pFile->pReadPtr = pBuf;
}


/*==========================================================================*
 * FUNCTION : Cfg_ProfileGetSectionPos
 * PURPOSE  : get a section position, excluding comment lines. Used by config
 *            file modification.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN void        *pProf      : 
 *            IN const char  *szKey      : section name
 *            OUT long       *pulStartPos : 
 *            OUT long       *pulEndPos   : 
 * RETURN   : int : 0 for not find such section
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-27 13:33
 *==========================================================================*/
int Cfg_ProfileGetSectionPos(IN void *pProf,
							 IN const char *szKey, 
							 OUT size_t *pulStartPos, 
							 OUT size_t *pulEndPos)
{
	SProfile *pFile;
	char szBuf[MAX_LINE_SIZE];
	int iRet;

	pFile = (SProfile *)pProf;

	/* find the section */
	iRet = Cfg_ProfileFindSection(pProf, szKey);
    if (iRet == 0)
	{
		return 0;
	}

	/* get first line of the section */
	iRet = Cfg_ProfileGetNextLine(pProf, szBuf, MAX_LINE_SIZE);

	MoveBackOneLine(pProf);
	*pulStartPos = pFile->pReadPtr - pFile->pFileBuffer;
	
	/* empty section */
	if (iRet == 0)
	{
		*pulEndPos = *pulStartPos;

		return 1;
	}

    /* not empty section */
	do
	{
		iRet = Cfg_ProfileGetNextLine(pProf, szBuf, MAX_LINE_SIZE);
	}
	while (iRet != 0);
	
	MoveBackCommentLines(pProf);
	//MoveBackOneLine(pProf);
	*pulEndPos = pFile->pReadPtr - pFile->pFileBuffer;

	return 1;
}


//empty field judging function
BOOL Cfg_IsEmptyField(IN const char *pField)
{
	/*	if (pField == NULL)
	{
	return FALSE;
	}

	if ((pField + 1 ) == NULL)
	{
	return FALSE;
	}
	*/

	if (*pField == 'N' && *(pField + 1) == 'A' &&
		*(pField + 2) == '\0')
	{
		return TRUE;
	}
	else 
	{
		return FALSE;
	}
}


/*==========================================================================*
 * FUNCTION : CFG_CheckBase
 * PURPOSE  : basic function for check a null-ended string, used by other 
 *            specifically realized check functions
 * CALLS    : 
 * CALLED BY: specifically realized check functions, such as ESR_CheckPhoneNumber
 * ARGUMENTS: const char  *szObject : 
 *            LEGAL_CHR   fnCmp     : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 11:18
 *==========================================================================*/
BOOL CFG_CheckBase(const char *szObject, LEGAL_CHR fnCmp)
{
	const char *p;

	if (szObject == NULL)
	{
		return FALSE;
	}

	p = szObject;
	for (; p != NULL; p++)
	{
		if (*p == '\0')
		{
			return TRUE;
		}

		if (!fnCmp(*p))
		{
			return FALSE;
		}

	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : CFG_CheckPNumber
 * PURPOSE  : for checking positive number null-ended string
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szNumber : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 10:36
 *==========================================================================*/
static BOOL IsPNumber(char c)
{
	return  (c >= '0' && c <= '9');
}

BOOL CFG_CheckPNumber(const char *szNumber)
{
	return CFG_CheckBase(szNumber, IsPNumber);
}
	
/*==========================================================================*
 * FUNCTION : CFG_CheckNumber
 * PURPOSE  : for checking number null-ended string
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szNumber : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 10:36
 *==========================================================================*/
BOOL CFG_CheckNumber(const char *szNumber)
{
	if (szNumber[0] == '-' && szNumber+1 != NULL)
	{
		return CFG_CheckBase(szNumber+1, IsPNumber);
	}
	else
	{
		return CFG_CheckBase(szNumber, IsPNumber);
	}
}

/*==========================================================================*
 * FUNCTION : CFG_CheckHexNumber
 * PURPOSE  : for checking hex number null-ended string
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szHexNumber : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 11:28
 *==========================================================================*/
static BOOL IsHexNumber(char c)
{
	return ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') ||
		(c == 'x') || (c == 'X'));
}


BOOL CFG_CheckHexNumber(const char *szHexNumber)
{
	return CFG_CheckBase(szHexNumber, IsHexNumber);
}

//Added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
//just support SEEK_SET
BOOL Cfg_ProfileSeekSet(IN void *pProf, IN long lPos)
{
	SProfile *pFile;
	pFile = (SProfile *)pProf;

	if (lPos >= (long)(pFile->pEndFile - pFile->pFileBuffer))
		return FALSE;

	pFile->pReadPtr = pFile->pFileBuffer + lPos;
	
	return TRUE;
}

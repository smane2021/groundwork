/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : cfg_reader.c
 *  CREATOR  : LinTao                   DATE: 2004-09-08
 *  VERSION  : V1.0
 *  PURPOSE  : Source file for Config Reader
 *
 *  HISTORY  : based on the EMNET project ( Author: maofuhua )
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"


/*==========================================================================*
 * FUNCTION : Cfg_LoadSingleTable
 * PURPOSE  : load one talbe
 * CALLS    : 
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: void            *pCfg             (in): the SProfile structure 
 *            CONFIG_TABLE_LOADER  *pLoader (in out): the Loader structure
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-08
 *==========================================================================*/
static int Cfg_LoadSingleTable(IN void *pCfg, IN OUT CONFIG_TABLE_LOADER *pLoader)
{
    int     iRowNum, i, nErrorItemIndex;
    char    szBuf[MAX_LINE_SIZE];
    char    *pRecord;

    RunThread_Heartbeat(RunThread_GetId(NULL));

	*pLoader->pRowNum = 0;   // init
	*pLoader->ppRecords  = NULL;

    /* 1. read the row count */
	if (pLoader->szNumKey == NULL) //no Num section, so we should count the lines
	{
		iRowNum = Cfg_GetSectionLineCount(pCfg, pLoader->szDataKey);
		if (iRowNum == -1)
		{
			AppLogOut("CFG_READER", APP_LOG_ERROR, "no %s section!\n",
				pLoader->szDataKey);
			TRACE("[%s]--%s: ERROR: no %s section!\n", __FILE__,
				__FUNCTION__, pLoader->szDataKey);

			return ERR_CFG_BADCONFIG;
		}
	}
	else
	{
		if (Cfg_ProfileGetInt(pCfg, pLoader->szNumKey, &iRowNum) == -1)
		{
			AppLogOut("CFG_READER", APP_LOG_ERROR, "no %s section!\n",
				__FILE__, pLoader->szNumKey);
			TRACE("[%s]--%s: ERROR: no %s section!\n", __FILE__,
				__FUNCTION__, pLoader->szNumKey);

			return ERR_CFG_BADCONFIG; 
		}

		if (iRowNum < 0) /* error num */
		{		
			AppLogOut("CFG_READER", APP_LOG_ERROR, "Rows of a table should"
				"be greater than 0!\n");
			TRACE("[%s]--%s: ERROR: rows of a table should be greater than 0!\n",
				 __FILE__, __FUNCTION__);

			return ERR_CFG_BADCONFIG; 
		}
	}

	/* 2.have get the row count, check it */
	if (iRowNum == 0)
	{
		return ERR_CFG_OK; /* empty section, just jump */
	}

    /* 3.have some rows, read them */
	if (Cfg_ProfileFindSection(pCfg, pLoader->szDataKey) == 0)
    {
		AppLogOut("CFG_READER", APP_LOG_ERROR, "no %s section!\n",
			pLoader->szDataKey);
		TRACE("[%s]--%s: ERROR: no %s section!\n",
				 __FILE__, __FUNCTION__, pLoader->szDataKey);

        return ERR_CFG_BADCONFIG;
    }

    pRecord = NEW(char, iRowNum * (pLoader->iRecordSize));
    if (pRecord == NULL)
    {
		AppLogOut("CFG_READER", APP_LOG_ERROR, "out of memory on loading %s!\n",
			pLoader->szDataKey );
		TRACE("[%s]--%s: ERROR: out of memory on loading %s!\n",
				 __FILE__, __FUNCTION__, pLoader->szDataKey);

        return ERR_CFG_NO_MEMORY;
    }

    /* clear data to 0 */
    memset(pRecord, 0, (size_t)(iRowNum * (pLoader->iRecordSize)));

    /* set the data ptr */
	*pLoader->ppRecords  = pRecord;
	*pLoader->pRowNum = iRowNum;

    /* load records one by one */
    for (i = 0; i < iRowNum; i++, pRecord += pLoader->iRecordSize)
    {
        RunThread_Heartbeat(RunThread_GetId(NULL));

        /* read a line from config file */
        if (Cfg_ProfileGetNextLine( pCfg, szBuf, sizeof(szBuf) ) == 0)
        {
			AppLogOut("CFG_READER", APP_LOG_ERROR, "end on loading #%d record" 
				" of %s!\n",i + 1, pLoader->szDataKey);
			TRACE("[%s]--%s: ERROR: end on loading #%d record of %s!\n",
				 __FILE__, __FUNCTION__, i + 1, pLoader->szDataKey);

            return ERR_CFG_BADCONFIG;
        }

        /* parse a record properties from the buf */
		nErrorItemIndex = pLoader->pfnParseRecord(szBuf, pRecord);

		//record maybe skippable by default, added for G3_OPT [loader], by Lin.Tao, 2013-5
		if (nErrorItemIndex == CFG_RECORD_SKIP)
		{
			(*pLoader->pRowNum)--;
			pRecord -= pLoader->iRecordSize;
			continue;
		}

        if (nErrorItemIndex != 0)
        {
            AppLogOut("CFG_READER", APP_LOG_ERROR, "End on loading %s: Record #%d,"
				 " Field #%d!\n", pLoader->szDataKey, i + 1, nErrorItemIndex);
			TRACE("[%s]--%s: ERROR: End on loading %s: Record #%d, Field #%d!\n",
				 __FILE__, __FUNCTION__, pLoader->szDataKey, 
				 i + 1, nErrorItemIndex);

            return ERR_CFG_FAIL;  // read end of this data section.
        }
    }

    return ERR_CFG_OK;
}



/*==========================================================================*
 * FUNCTION : Cfg_LoadTables
 * PURPOSE  : load tables
 * CALLS    : Cfg_LoadSingleTable
 * CALLED BY: 
 * ARGUMENTS: void            *pCfg   (in): the SProfile structure
 *            int             nTable  (in): the table number
 *            CONFIG_TABLE_LOADER  *loader (in out): Loaders stucture
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-09
 *==========================================================================*/
int Cfg_LoadTables( IN void *pCfg, IN int nTable, IN OUT CONFIG_TABLE_LOADER  *loader )
{
    int i;

    // load all tables
    for(i = 0; i < nTable; i++)
    {
        RunThread_Heartbeat( RunThread_GetId(NULL) );     

        if(Cfg_LoadSingleTable(pCfg, &loader[i]) != ERR_CFG_OK)
        {
            AppLogOut("CFG_READER", APP_LOG_ERROR, "fail on loading table: "
				"%s\n", loader[i].szDataKey);
			TRACE("[%s]--%s: ERROR: fail on loading table: %s\n", 
				__FILE__, __FUNCTION__, loader[i].szDataKey);

            return ERR_CFG_FAIL;
        }
    }

    return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : Cfg_UnloadTables
 * PURPOSE  : Unload tables(to free memory)
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                    nTable    (in): the number of teh tables
 *            CONFIG_TABLE_UNLOADER  *unloader (in): Unloader structure
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-09
 *==========================================================================*/
void Cfg_UnloadTables(IN int nTable, IN OUT CONFIG_TABLE_UNLOADER  *unloader)
{
    int i;

    for (i = 0; i < nTable; i++)
    {
        if (*unloader[i].ppRecords  != NULL)
        {
            if (unloader[i].pfnReleaseRecord != NULL)
            {
                int  j;
                char *p = (*unloader[i].ppRecords);

                for (j = 0; j < (*unloader[i].pRowNum); j++)
                {
                    unloader[i].pfnReleaseRecord(p);
                    p += unloader[i].iRecordSize;
                }
            }

            DELETE (*unloader[i].ppRecords);
            *unloader[i].ppRecords = NULL;
        }
    }
}

/*==========================================================================*
 * FUNCTION : Cfg_LoadCfgFirstTime
 * PURPOSE  : Load config file
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const  char     *pszConfigFile (in) : config file name
 *            LOAD_CONF_PROC  pfnLoader      (in) : Loader structure
 *            void            *pBuf          (out): the stucture to load config
 *													file data
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : Songxu                   DATE: 2016-08-03
 *==========================================================================*/
int Cfg_LoadCfgFirstTime(const char *pszConfigFile, 
                   LOAD_CONF_PROC pfnLoader, void *pBuf)
{
    void    *pCfg;
	char    *szFileContents;
	FILE    *pFile;
    long    ulFileLen;
    
    RunThread_Heartbeat(RunThread_GetId(NULL));


#ifdef _SHOW_LOADING_INFO
	TRACE("[%s]--Cfg_LoadConfigFile: loading config file %s.\n",
        __FILE__, pszConfigFile );
#endif //_SHOW_LOADING_INFO

    /* 1. read the file content to the buffer */
	pFile = fopen(pszConfigFile, "r");
	if (pFile == NULL)
	{
//		AppLogOut("CFG_READER", APP_LOG_WARNING, "Cannot open file: %s first time!\n",
//			pszConfigFile);
		TRACE("[%s]--%s: WARN: Cannot open file: %s first time!\n", __FILE__,
			__FUNCTION__, pszConfigFile);
		return ERR_CFG_FILE_OPEN;
	}
	
	fseek(pFile, 0, SEEK_END);
	ulFileLen = ftell(pFile);
	if (ulFileLen == -1)
	{
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Get Config file(%s) length error!\n",
			pszConfigFile);
		TRACE("[%s]--%s: ERROR: Get Config file(%s) length error!\n",
			 __FILE__, __FUNCTION__, pszConfigFile);	

		fclose(pFile);
		return ERR_CFG_FAIL;
	}
	fseek(pFile, 0, SEEK_SET);

	szFileContents = NEW(char, ulFileLen + 1);
	if (szFileContents == NULL)
	{
		AppLogOut("CFG_READER", APP_LOG_ERROR, "out of memory on loading "
				"file: %s!\n", pszConfigFile);
		TRACE("[%s]--%s: ERROR: out of memory on loading file: %s!\n",
			 __FILE__, __FUNCTION__, pszConfigFile);

		fclose(pFile);
		return ERR_CFG_NO_MEMORY;
	}

	ulFileLen = fread(szFileContents, sizeof(char), (size_t)ulFileLen, pFile);
	fclose(pFile);
	if (ulFileLen < 0) 
	{
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Read config file (%s) fail!\n",
			pszConfigFile);
		TRACE("[%s]--%s: ERROR: Read config file (%s) fail!\n",
			__FILE__, __FUNCTION__, pszConfigFile);

		DELETE(szFileContents);
		return ERR_CFG_FILE_READ;
	}
	szFileContents[ulFileLen] = 0;  // end with NULL


	/* 2. get SProfile structure */
    pCfg = Cfg_ProfileOpen( szFileContents, ulFileLen);
    if( pCfg == NULL )
    {
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Open config file: %s out"
			"of memery.\n", pszConfigFile);
		TRACE("[%s]--%s: ERROR: Open config file: %s out of memery.\n",
			__FILE__, __FUNCTION__, pszConfigFile);

		DELETE(szFileContents);
        return ERR_CFG_FAIL;
    }

    /* 3. load config file */
    if (pfnLoader( pCfg, pBuf ) != ERR_CFG_OK)
    { 
		Cfg_ProfileClose(pCfg);
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Load the Config file: %s "
			"failed!\n", pszConfigFile);
		TRACE("[%s]--%s: ERROR: CLoad the Config file: %s failed!\n", 
			__FILE__, __FUNCTION__, pszConfigFile);
        return ERR_CFG_FAIL;
    }

    if (pCfg != NULL)
    {
        Cfg_ProfileClose(pCfg);  //free the memery
    }

    return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : Cfg_LoadConfigFile
 * PURPOSE  : Load config file
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const  char     *pszConfigFile (in) : config file name
 *            LOAD_CONF_PROC  pfnLoader      (in) : Loader structure
 *            void            *pBuf          (out): the stucture to load config
 *													file data
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-09
 *==========================================================================*/
int Cfg_LoadConfigFile(const char *pszConfigFile, 
                   LOAD_CONF_PROC pfnLoader, void *pBuf)
{
    void    *pCfg;
	char    *szFileContents;
	FILE    *pFile;
    long    ulFileLen;
    
    RunThread_Heartbeat(RunThread_GetId(NULL));


#ifdef _SHOW_LOADING_INFO
	TRACE("[%s]--Cfg_LoadConfigFile: loading config file %s.\n",
        __FILE__, pszConfigFile );
#endif //_SHOW_LOADING_INFO

    /* 1. read the file content to the buffer */
	pFile = fopen(pszConfigFile, "r");
	if (pFile == NULL)
	{
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Can not open the file: %s!\n",
			pszConfigFile);
		TRACE("[%s]--%s: ERROR: Can not open the file: %s!\n", __FILE__,
			__FUNCTION__, pszConfigFile);
		return ERR_CFG_FILE_OPEN;
	}
	
	fseek(pFile, 0, SEEK_END);
	ulFileLen = ftell(pFile);
	if (ulFileLen == -1)
	{
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Get Config file(%s) length error!\n",
			pszConfigFile);
		TRACE("[%s]--%s: ERROR: Get Config file(%s) length error!\n",
			 __FILE__, __FUNCTION__, pszConfigFile);	

		fclose(pFile);
		return ERR_CFG_FAIL;
	}
	fseek(pFile, 0, SEEK_SET);

	szFileContents = NEW(char, ulFileLen + 1);
	if (szFileContents == NULL)
	{
		AppLogOut("CFG_READER", APP_LOG_ERROR, "out of memory on loading "
				"file: %s!\n", pszConfigFile);
		TRACE("[%s]--%s: ERROR: out of memory on loading file: %s!\n",
			 __FILE__, __FUNCTION__, pszConfigFile);

		fclose(pFile);
		return ERR_CFG_NO_MEMORY;
	}

	ulFileLen = fread(szFileContents, sizeof(char), (size_t)ulFileLen, pFile);
	fclose(pFile);
	if (ulFileLen < 0) 
	{
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Read config file (%s) fail!\n",
			pszConfigFile);
		TRACE("[%s]--%s: ERROR: Read config file (%s) fail!\n",
			__FILE__, __FUNCTION__, pszConfigFile);

		DELETE(szFileContents);
		return ERR_CFG_FILE_READ;
	}
	szFileContents[ulFileLen] = 0;  // end with NULL


	/* 2. get SProfile structure */
    pCfg = Cfg_ProfileOpen( szFileContents, ulFileLen);
    if( pCfg == NULL )
    {
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Open config file: %s out"
			"of memery.\n", pszConfigFile);
		TRACE("[%s]--%s: ERROR: Open config file: %s out of memery.\n",
			__FILE__, __FUNCTION__, pszConfigFile);

		DELETE(szFileContents);
        return ERR_CFG_FAIL;
    }

    /* 3. load config file */
    if (pfnLoader( pCfg, pBuf ) != ERR_CFG_OK)
    { 
		Cfg_ProfileClose(pCfg);
		AppLogOut("CFG_READER", APP_LOG_ERROR, "Load the Config file: %s "
			"failed!\n", pszConfigFile);
		TRACE("[%s]--%s: ERROR: CLoad the Config file: %s failed!\n", 
			__FILE__, __FUNCTION__, pszConfigFile);
        return ERR_CFG_FAIL;
    }

    if (pCfg != NULL)
    {
        Cfg_ProfileClose(pCfg);  //free the memery
    }

    return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : ModififySigleItem
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: 
 *            IN void                  *pProf     : 
 *            IN CONFIG_FILE_MODIFIER  pModifier  : 
 *			  IN OUT long          *pulLastReadPos :
 *            OUT char                 *szOutFile : 
 *            IN long                  ulBufLen    : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-27 17:31
 *==========================================================================*/
static int ModififySigleItem(IN void *pProf, 
							 IN CONFIG_FILE_MODIFIER *pModifier, 
							 IN OUT size_t *pulLastReadPos,
				             OUT char *szOutFile,
				             IN size_t ulBufLen)
{
	SProfile *pFile; 
	size_t ulStartPos, ulEndPos;
	size_t ulRetainLen;   /* length of retained content of config file */


	pFile = (SProfile *)pProf;
	if (Cfg_ProfileGetSectionPos(pProf, 
		pModifier->szSectionKey, &ulStartPos, &ulEndPos) == 0)
	{
		/* not found such section, input param is invalid */
		TRACE("[%s]--%s: ERROR: section name of Modifier structure is invalid"
			".\n", __FILE__, __FUNCTION__);
		return ERR_CFG_PARAM;
	}

	/* check buf length */
	ulRetainLen = ulStartPos - (*pulLastReadPos) + 1;
	if (ulBufLen - strlen(szOutFile) < 
				ulRetainLen + strlen(pModifier->szContent) + 1)
	{
		TRACE("[%s]--%s: ERROR: buffer for modified config file is too short.\n",
			__FILE__, __FUNCTION__);

		return ERR_CFG_PARAM;
	}

	/* now copy data */
	strncat(szOutFile, &pFile->pFileBuffer[*pulLastReadPos],
		ulStartPos - (*pulLastReadPos) + 1);
	strcat(szOutFile, pModifier->szContent);
	

	(*pulLastReadPos) = ulEndPos;

	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : Cfg_ModifyConfigFile
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const char            *szFileName : 
 *            IN int                   iModifiers  : 
 *            IN CONFIG_FILE_MODIFIER  *pModifier  : 
 *            OUT char                 **szOutFile  : 
 * RETURN   : int : err code defined in the errcode.h
 * COMMENTS : note: pModifier should be ordered !!!
 * CREATOR  : LinTao                   DATE: 2004-10-27 16:48
 *==========================================================================*/
int Cfg_ModifyConfigFile(IN const char *szFileName,
						 IN int iModifiers,
						 IN CONFIG_FILE_MODIFIER *pModifier,
						 OUT char **szOutFile)
{
	int ret, i;

	void *pProf;  
	FILE *pFile;
	char *szInFile;
	size_t ulFileLen, ulLastReadPos;

	/* open file */
	pFile = fopen(szFileName, "r");
	if (pFile == NULL)
	{
		TRACE("[%s]--%s: ERROR: Can not open the file: %s!\n",
			__FILE__, __FUNCTION__, szFileName);
		return ERR_CFG_FILE_OPEN;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		TRACE("[%s]--%s: ERROR: There is no memory!\n", 
			__FILE__, __FUNCTION__);
		fclose(pFile);
		return ERR_CFG_NO_MEMORY;
	}

	*szOutFile = NEW(char, ulFileLen + MAX_EXTRA_SIZE);
	if (*szOutFile == NULL)
	{
		TRACE("[%s]--%s: ERROR: There is no memory!\n", 
			__FILE__, __FUNCTION__);
		DELETE(szInFile);
		fclose(pFile);
		return ERR_CFG_NO_MEMORY;
	}
	(*szOutFile)[0] = '\0';

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		TRACE("[%s]--%s: ERROR: Read file (%s) fail!\n", __FILE__,
			__FUNCTION__, szFileName);

		/* clear the memory */
		DELETE(szInFile);
		DELETE(*szOutFile);

		return ERR_CFG_FILE_READ;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */


	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);
	
	if (pProf == NULL)
	{
		TRACE("[%s]--%s: ERROR: Create SProfile fail!\n", 
			__FILE__, __FUNCTION__);

		DELETE(szInFile);
		return ERR_CFG_FILE_READ;
	}

	/* do modification one by one. Note: pModifier should be ordered! */
	ulLastReadPos = 0;
	for (i = 0; i < iModifiers; i++, pModifier++)
	{
		ret = ModififySigleItem(pProf, pModifier, &ulLastReadPos, 
			 *szOutFile, ulFileLen + MAX_EXTRA_SIZE);

		if (ret != ERR_CFG_OK)
		{
			TRACE("[%s]--%s: ERROR: modifiy item #%d of the config file"
				" failed.\n", __FILE__, __FUNCTION__, i + 1);

			Cfg_ProfileClose(pProf);
			return ret;
		}
	}
	/* read left content */
	strcat(*szOutFile, ((SProfile *)pProf)->pFileBuffer + ulLastReadPos);

	Cfg_ProfileClose(pProf);
	return ERR_CFG_OK;
}

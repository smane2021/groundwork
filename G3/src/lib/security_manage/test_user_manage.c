/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : test_user_manage.c
*  CREATOR  : HULONGWEN                DATE: 2004-09-30 16:35
*  VERSION  : V1.00
*  PURPOSE  : To test user_manage.so
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>

#include "user_manage.h"


extern USER_INFO_STRU g_UserManage[];
extern int			  g_nUserNumber;


void TestUserManage(void)
{
	printf("filename: test_user_exchange.c [TestUserManage]\r\n");

	printf("User manage sub module self_text start!\r\n");
	printf("please press ENTER key!\r\n");

	getchar();

	//test GetUserNum

	if(GetUserNum(GET_MAXIMUM_USER_NUM) == USERS_MAX_NUMBER
		&& GetUserNum(GET_ACTUAL_USER_NUM) == 2)
	{
		printf("GetUserNum() success!\r\n");
	}
	else
	{
		printf("GetUserNum() failure!\r\n");
	}

	//Test AddUserInfo
	if(AddUserInfo("lintao", "Tomas", ENGINEER_LEVEL) == ERR_SEC_OK
		&& AddUserInfo("HUlw", "Tomas", ENGINEER_LEVEL) == ERR_SEC_USER_ALREADY_EXISTED)
	{
		printf("AddUserInfo() success!\r\n");
	}
	else
	{
		printf("AddUserInfo() failure!\r\n");
	}


	//Test ModifyUserInfo
	if(ModifyUserInfo("lintao", "Frank", -1) == ERR_SEC_OK
		&& ModifyUserInfo("AdmIn", "Tomas", ENGINEER_LEVEL) == ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL
		&& ModifyUserInfo("ajflalka", "Tomas", ENGINEER_LEVEL) == ERR_SEC_USER_NOT_EXISTED)
	{
		printf("ModifyUserInfo() success!\r\n");
	}
	else
	{
		printf("ModifyUserInfo() failure!\r\n");
	}

	//Test ModifyUserInfo
	if(DeleteUserInfo("hulw") == ERR_SEC_OK
		&& DeleteUserInfo("AdmIn") == ERR_SEC_CANNOT_DELETE_ADMIN
		&& DeleteUserInfo("ajflalka") == ERR_SEC_USER_NOT_EXISTED)
	{
		printf("DeleteUserInfo() success!\r\n");
	}
	else
	{
		printf("DeleteUserInfo() failure!\r\n");
	}

	//Test FindUserInfo
	USER_INFO_STRU stUserInfo;
	if(FindUserInfo("hulw", &stUserInfo) == ERR_SEC_USER_NOT_EXISTED
		&& FindUserInfo("AdmIn", &stUserInfo) == ERR_SEC_OK)
	{
		printf("FindUserInfo() success!\r\n");
	}
	else
	{
		printf("FindUserInfo() failure!\r\n");
	}

	//Test GetUserInfo
	USER_INFO_STRU* pUserInfo;
	if(GetUserInfo(&pUserInfo) == g_nUserNumber
		&& pUserInfo == g_UserManage) 
	{
		printf("GetUserInfo() success!\r\n");
	}
	else
	{
		printf("GetUserInfo() failure!\r\n");
	}
}

void InitUserManageInfo(void)
{
	printf("filename: test_user_exchange.c [InitUserManageInfo]\r\n");


	//user1, should be administrator 
	g_UserManage[0].byLevel = ADMIN_LEVEL;

	strncpyz(g_UserManage[0].szUserName, ADMIN_DEFAULT_NAME,
		sizeof(g_UserManage[0].szUserName));

	strncpyz(g_UserManage[0].szPassword, ADMIN_DEFAULT_PASSWORD,
		sizeof(g_UserManage[0].szPassword));


	//user2, hulw
	g_UserManage[1].byLevel = OPERATOR_LEVEL;

	strncpyz(g_UserManage[1].szUserName, "hulw",
		sizeof(g_UserManage[1].szUserName));

	strncpyz(g_UserManage[1].szPassword, "hello",
		sizeof(g_UserManage[1].szPassword));

	g_nUserNumber = 2;

}



/*==========================================================================*
* FUNCTION : main
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int    argc    : 
*            INT char  *argv[] : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-30 16:38
*==========================================================================*/
int main(IN int argc, IN char *argv[])
{
	UNUSED(argc);

	printf("Test start!\r\n");


	InitUserManage();

	InitUserManageInfo();

	if(argv[1])
	{
		if(strcmp(argv[1], "-user") == 0)
		{
			TestUserManage();
		}
	}

	else
	{
		TestUserManage();

	}

	return 0;
}

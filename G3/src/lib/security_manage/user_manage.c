/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : user_manage.c
*  CREATOR  : HULONGWEN                DATE: 2004-09-28 14:37
*  VERSION  : V1.00
*  PURPOSE  : The implementation file of security management
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  
#include "../data_mgmt/his_data.h"
#include <time.h>

#define IS_USER_INFO	1
#define IS_NMS_INFO		2
#define IS_V3NMS_INFO		3
#define IS_TRAP_INFO		4

typedef struct
{
	int				nInfoType;

	int		iTrapLevel;
	USER_INFO_STRU	stUserInfo;
	NMS_INFO		stNmsInfo;
	V3NMS_INFO		stV3NmsInfo;

} USER_AND_NMS_INFO_SAVE;

//The handle of the mutex for set time server info
static HANDLE g_hMutexUserManage = NULL;

//The handle of the mutex for write info to flash
static HANDLE g_hMutexUserInfoToFlash = NULL;

//The number of the actual users
static int	  g_nUserNumber = 0;

static USER_INFO_STRU g_UserManage[USERS_MAX_NUMBER];
static USER_INFO_STRU g_UserWebManage[USERS_MAX_NUMBER];

//The number of the NMS users
int	  g_nTrapLevel = 0;
int	  g_nNmsUserNumber = 0;
int	  g_V3nNmsUserNumber = 0;

NMS_INFO g_NmsUserManage[NMS_USERS_MAX_NUMBER];
V3NMS_INFO g_V3NmsUserManage[NMS_USERS_MAX_NUMBER];

//for G3_OPT [config]
static BOOL g_LoadedfromRunFile = FALSE;


#define USR_LOCK_WAIT_TIMEOUT		1000

static int UserExist(char* pszName);

//User/NMS info transfer, added for G3_OPT [config], by Thomas, 2013-5
BOOL GetUserInfoFromRunFile(FILE *pf)
{
	//for check sum
	char *pFrame; 
	int  iCheckSumRead, i, j, nCount, iUserNumber = -1;
	WORD dwSum = 0; 

	ASSERT(pf);

	/* 1. clear buffer first */
	g_nUserNumber = 0;
	g_nNmsUserNumber = 0;
	g_V3nNmsUserNumber = 0;

	ZERO_POBJS(g_UserManage,	USERS_MAX_NUMBER);
	ZERO_POBJS(g_NmsUserManage, NMS_USERS_MAX_NUMBER);
	ZERO_POBJS(g_V3NmsUserManage, V3_NMS_USERS_MAX_NUMBER);

	typedef struct tagUserInfoLoad
	{
		int  *pNumber;
		char *pData;
		int  nSize;
		int  nMaxNumber;
	} USER_INFO_LOADING;

	USER_INFO_LOADING szUserInfo[] =
	{
		{&g_nUserNumber,	  g_UserManage,      sizeof(USER_INFO_STRU), USERS_MAX_NUMBER},
		{&g_nNmsUserNumber,	  g_NmsUserManage,   sizeof(NMS_INFO),       NMS_USERS_MAX_NUMBER},
		{&g_V3nNmsUserNumber, g_V3NmsUserManage, sizeof(V3NMS_INFO),     V3_NMS_USERS_MAX_NUMBER},
	};


	/* 2. read user info, using check sum for validity checking */
	int iNumber = 0;
	for (i = 0; i < ITEM_OF(szUserInfo); i++)
	{
		if (!fread(&iNumber, sizeof(int), 1, pf))
		{
			return FALSE;
		}
		if (iNumber < 0 || iNumber > szUserInfo[i].nMaxNumber)
		{
			return FALSE;
		}
		if (iNumber >0 && !fread(szUserInfo[i].pData, szUserInfo[i].nSize, iNumber, pf))
		{
			return FALSE;
		}
		if(i == 0)//User info
		{
			iUserNumber = iNumber;
		}
		pFrame = szUserInfo[i].pData;
		nCount = iNumber * szUserInfo[i].nSize;
		for (j = 0; j < nCount; j++)
		{
			dwSum += *pFrame++;
		}
		*(szUserInfo[i].pNumber) = iNumber ;
	}

	/* 3. check sum checking */
	if (!fread(&iCheckSumRead, sizeof(int), 1, pf))
	{
		return FALSE;
	}
	if (iCheckSumRead != (WORD)-dwSum)
	{
		AppLogOut("EQUIP INIT", APP_LOG_ERROR, "Checksum error of user/NMS info from Param file!\n");
		TRACE("[%s]--%s: ERROR: Checksum error of user/NMS info from Param file!\n", __FILE__,__FUNCTION__);
		return FALSE;
	}
	
//changed by Fengel hu ,2016-12-24, for BT security(kill user of emersonadmin).   
#ifdef	NEED_EMERSONADMIN
	if(iUserNumber > 0 && UserExist(SUPER_ADMIN_NAME) < 0)//Fengel 2016-12-26,& to &&
	{
		g_nUserNumber = iUserNumber + 1;
		
		strncpyz(g_UserManage[iUserNumber].szUserName, SUPER_ADMIN_NAME,
			USERNAME_LEN);
		strncpyz(g_UserManage[iUserNumber].szPassword, SUPER_ADMIN_PASSWORD,
			PASSWORD_LEN);
		g_UserManage[iUserNumber].byLevel = ADMIN_LEVEL;
		
	}
#endif
	/* 4. set flag to avoid load again */
	if (UpdateUserInfoToFlash())
	{
		g_LoadedfromRunFile = TRUE;
		
		return TRUE;
	}	

	return FALSE;
}


static BOOL GetUserInfoFromFlash(void)
{
	HANDLE hUserInfoHandle = NULL;

	USER_AND_NMS_INFO_SAVE* pUserAndNmsInfo;

	g_nUserNumber = 0;
	g_nNmsUserNumber = 0;
	g_V3nNmsUserNumber = 0;

	ZERO_POBJS(g_UserManage,	USERS_MAX_NUMBER);
	ZERO_POBJS(g_NmsUserManage, NMS_USERS_MAX_NUMBER);
	ZERO_POBJS(g_V3NmsUserManage, V3_NMS_USERS_MAX_NUMBER);

	if (g_hMutexUserInfoToFlash != NULL && Mutex_Lock(g_hMutexUserInfoToFlash, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	hUserInfoHandle = DAT_StorageOpen(SYSTEM_USER_LOG);

	// read the alarm as historical alarm.
	if ((hUserInfoHandle != NULL) && (((RECORD_SET_TRACK *)hUserInfoHandle)->iRecordSize == sizeof(USER_AND_NMS_INFO_SAVE))
	    && (((RECORD_SET_TRACK *)hUserInfoHandle)->iMaxRecords == USERS_MAX_NUMBER + NMS_USERS_MAX_NUMBER + V3_NMS_USERS_MAX_NUMBER + 1))
	{
	    printf("No need to Re-Create the storage\n");
		int iStartPos = 1;

		int i;

		int nUserNum = USERS_MAX_NUMBER + NMS_USERS_MAX_NUMBER + V3_NMS_USERS_MAX_NUMBER + 1;// add the snmp trap level

		USER_AND_NMS_INFO_SAVE* pUserAndNmsInfoRead = NULL;

		pUserAndNmsInfo = NEW(USER_AND_NMS_INFO_SAVE, nUserNum);

		pUserAndNmsInfoRead = pUserAndNmsInfo;

		printf("Now read the user and nms info from flash!\n");

		if (!DAT_StorageReadRecords(hUserInfoHandle, &iStartPos,
			&nUserNum, pUserAndNmsInfo, 0, TRUE))
		{
		    printf("Reading user info data ... FAILED.\n");
			TRACEX("Reading user info data ... FAILED.\n");

			nUserNum = 0;
		}
		else
		{
		    printf("Reading user info data ... success[%d].\n", nUserNum);
		}

		DAT_StorageClose(hUserInfoHandle);
		
		for (i = 0; i < nUserNum; i++, pUserAndNmsInfo++)
		{
			if (pUserAndNmsInfo != NULL)
			{
				if (pUserAndNmsInfo->nInfoType == IS_USER_INFO)
				{
					memcpy(&(g_UserManage[g_nUserNumber]), 
						&(pUserAndNmsInfo->stUserInfo), 
						sizeof(USER_INFO_STRU));

					g_nUserNumber++;
				}
				else if (pUserAndNmsInfo->nInfoType == IS_NMS_INFO)
				{
					memcpy(&(g_NmsUserManage[g_nNmsUserNumber]), 
						&(pUserAndNmsInfo->stNmsInfo), 
						sizeof(NMS_INFO));

					g_nNmsUserNumber++;
				}
				else if (pUserAndNmsInfo->nInfoType == IS_V3NMS_INFO)
				{
				    memcpy(&(g_V3NmsUserManage[g_V3nNmsUserNumber]), 
					&(pUserAndNmsInfo->stV3NmsInfo), 
					sizeof(V3NMS_INFO));

				    g_V3nNmsUserNumber++;
				}
				else if(pUserAndNmsInfo->nInfoType == IS_TRAP_INFO)
				{
				    g_nTrapLevel = pUserAndNmsInfo->iTrapLevel;
				    if((g_nTrapLevel > 3) || (g_nTrapLevel < 0))
				    {
					g_nTrapLevel = 0;
				    }
				   // printf("g_nTrapLevel is %d\n", g_nTrapLevel);
				}
			}
		}

		DELETE(pUserAndNmsInfoRead);

		if (UserExist(ADMIN_DEFAULT_NAME) == -1)
		{
			AddUserInfo(ADMIN_DEFAULT_NAME, 
				ADMIN_DEFAULT_PASSWORD, 
				ADMIN_LEVEL);
		}
//changed by Fengel hu ,2016-12-24, for BT security(kill user of emersonadmin). 
#ifdef	NEED_EMERSONADMIN
		if (UserExist(SUPER_ADMIN_NAME) == -1)
		{
		    AddUserInfo(SUPER_ADMIN_NAME, 
			SUPER_ADMIN_PASSWORD, 
			ADMIN_LEVEL);
		}
#endif
#ifdef	NEED_ES_ADMIN		
		if (UserExist(ES_ADMIN_DEFAULT_NAME) == -1)
		{
			AddUserInfo(ES_ADMIN_DEFAULT_NAME, 
				ES_ADMIN_DEFAULT_PASSWORD, 
				ADMIN_LEVEL);
		}
#endif		
#ifdef	NEED_ES_ENGINEER
		if (UserExist(ES_ENGINEER_DEFAULT_NAME) == -1)
		{
		    AddUserInfo(ES_ENGINEER_DEFAULT_NAME, ES_ENGINEER_DEFAULT_PASSWORD, ADMIN_LEVEL);
		}
#endif
	}
	else //Create user info and user log
	{
		printf("Now create the user and nms info in flash!\n");
		hUserInfoHandle = DAT_StorageCreate(SYSTEM_USER_LOG,
			sizeof(USER_AND_NMS_INFO_SAVE), 
			USERS_MAX_NUMBER + NMS_USERS_MAX_NUMBER + V3_NMS_USERS_MAX_NUMBER + 1,
			FREQ_USER_INFO);

		// log after created...
		if (hUserInfoHandle)
		{
			AppLogOut(USER_MANAGE_LOG, APP_LOG_INFO, 
				"Creating user data file '%s'"
				"(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s\n", 
				SYSTEM_USER_LOG, sizeof(USER_INFO_STRU), 
				USERS_MAX_NUMBER, FREQ_USER_INFO, "OK");
		}
		else
		{
            AppLogOut(USER_MANAGE_LOG, APP_LOG_ERROR, 
				"Creating user data file '%s'"
				"(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s\n", 
				SYSTEM_USER_LOG, sizeof(USER_INFO_STRU), 
				USERS_MAX_NUMBER, FREQ_USER_INFO, "FAILED");
		}


		DAT_StorageClose(hUserInfoHandle);

		//user1, should be administrator 
		AddUserInfo(ADMIN_DEFAULT_NAME, ADMIN_DEFAULT_PASSWORD, ADMIN_LEVEL);
//changed by Fengel hu ,2016-12-24, for BT security(kill user of emersonadmin). 
#ifdef	NEED_EMERSONADMIN
		AddUserInfo(SUPER_ADMIN_NAME, SUPER_ADMIN_PASSWORD, ADMIN_LEVEL);
#endif
#ifdef	NEED_ES_ADMIN
		AddUserInfo(ES_ADMIN_DEFAULT_NAME, ES_ADMIN_DEFAULT_PASSWORD, ADMIN_LEVEL);
#endif
#ifdef	NEED_ES_ENGINEER
		AddUserInfo(ES_ENGINEER_DEFAULT_NAME, ES_ENGINEER_DEFAULT_PASSWORD, ADMIN_LEVEL);
#endif
	}
	if(g_hMutexUserInfoToFlash)
	{
		Mutex_Unlock(g_hMutexUserInfoToFlash);
	}

	return TRUE;
}

BOOL UpdateUserInfoToFlash(void)
{
	HANDLE hUserInfoHandleW = NULL;
	BOOL	bRst;

	if (g_hMutexUserInfoToFlash != NULL && Mutex_Lock(g_hMutexUserInfoToFlash, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	if (DAT_StorageDeleteRecord(SYSTEM_USER_LOG) != TRUE)
	{
		AppLogOut(USER_MANAGE_LOG, APP_LOG_ERROR, 
			"Delete user data file error");

		Mutex_Unlock(g_hMutexUserInfoToFlash);

		return FALSE;
	}

	hUserInfoHandleW = DAT_StorageOpenW(SYSTEM_USER_LOG);

	if (hUserInfoHandleW != NULL)
	{
		USER_AND_NMS_INFO_SAVE *pUserAndNmsInfo, *pWriteInfo;
		int i;

		pUserAndNmsInfo = NEW(USER_AND_NMS_INFO_SAVE, 
			g_nUserNumber + g_nNmsUserNumber + g_V3nNmsUserNumber + 1);	//Add the trap info

		if(pUserAndNmsInfo == NULL)
		{
			if(g_hMutexUserInfoToFlash)
			{
				Mutex_Unlock(g_hMutexUserInfoToFlash);
			}
			return FALSE;
		}
		printf("The total user number is %d\n", (g_nUserNumber + g_nNmsUserNumber + g_V3nNmsUserNumber + 1));
		printf("every user is %d bytes\n", sizeof(USER_AND_NMS_INFO_SAVE));

		pWriteInfo = pUserAndNmsInfo;

		memset(pUserAndNmsInfo, 0,
			sizeof(USER_AND_NMS_INFO_SAVE) * 
			(g_nUserNumber + g_nNmsUserNumber + g_V3nNmsUserNumber + 1));

		for (i = 0; i < g_nUserNumber; i++, pUserAndNmsInfo++)
		{
			pUserAndNmsInfo->nInfoType = IS_USER_INFO;

			memcpy(&(pUserAndNmsInfo->stUserInfo),
				&(g_UserManage[i]),
				sizeof(USER_INFO_STRU));
		}

		for (i = 0; i < g_nNmsUserNumber; i++, pUserAndNmsInfo++)
		{
			pUserAndNmsInfo->nInfoType = IS_NMS_INFO;
			memcpy(&(pUserAndNmsInfo->stNmsInfo), 
				&(g_NmsUserManage[i]),
				sizeof(NMS_INFO));
		}
		for (i = 0; i < g_V3nNmsUserNumber; i++, pUserAndNmsInfo++)
		{
		    pUserAndNmsInfo->nInfoType = IS_V3NMS_INFO;
		    memcpy(&(pUserAndNmsInfo->stV3NmsInfo), 
			&(g_V3NmsUserManage[i]),
			sizeof(V3NMS_INFO));
		}

		pUserAndNmsInfo->nInfoType = IS_TRAP_INFO;
		pUserAndNmsInfo->iTrapLevel = g_nTrapLevel;

		bRst = DAT_StorageWriteRecord(hUserInfoHandleW,
			sizeof(USER_AND_NMS_INFO_SAVE) * 
			(g_nUserNumber + g_nNmsUserNumber + g_V3nNmsUserNumber + 1),
			pWriteInfo);
		printf("The write flash result is %d\n", bRst);

		DAT_StorageClose(hUserInfoHandleW);

		if (pWriteInfo != NULL)
		{
			DELETE(pWriteInfo);
		}
	}
	if(g_hMutexUserInfoToFlash)
	{
		Mutex_Unlock(g_hMutexUserInfoToFlash);
	}

	return TRUE;
}


//Initialize user management module
BOOL InitUserManage(void)
{
	char	*pszResumeDefPwd;

	TRACE("filename: user_manage.c [InitUserManage]\r\n");

	//if (DXI_GetConfigStatus(NULL) == CONFIG_STATUS_DEFAULT_LOADED)
	//{
	//	//clear system user info
	//	DAT_StorageDeleteRecord(SYSTEM_USER_LOG);

	//}

	/*g_nUserNumber = 0;
	g_nNmsUserNumber = 0;
	g_V3nNmsUserNumber = 0;

	ZERO_POBJS(g_UserManage, USERS_MAX_NUMBER);
	ZERO_POBJS(g_NmsUserManage, NMS_USERS_MAX_NUMBER);
	ZERO_POBJS(g_V3NmsUserManage, V3_NMS_USERS_MAX_NUMBER);*/

	g_hMutexUserManage = Mutex_Create(TRUE);
	if (g_hMutexUserManage == NULL)
	{
		TRACE("\n\rfilename: user_manage.c [InitUserManage]\
			   user manage mutex create failed");

		AppLogOut(USER_MANAGE_LOG, APP_LOG_ERROR, 
			"[%s]--InitUserManage: ERROR: "
			"user manage mutex create failed!\n\r", __FILE__);

		return FALSE;
	}

	g_hMutexUserInfoToFlash = Mutex_Create(TRUE);
	if (g_hMutexUserInfoToFlash == NULL)
	{
		TRACE("\n\rfilename: user_manage.c [InitUserManage]\
			   write user info to flash mutex create failed");

		AppLogOut(USER_MANAGE_LOG, APP_LOG_ERROR, 
			"[%s]--InitUserManage: ERROR: "
			"write user info to flash mutex create failed!\n\r", __FILE__);

		return FALSE;
	}	

	if (!g_LoadedfromRunFile) //load from run file first, condition added for G3_OPT [config], by Lin.Tao, 2013-5
	{
		GetUserInfoFromFlash();
	}
        
        user_lockout_init(); // Added by Koustubh Mattikalli IA-01-04

	//2. get the default password switch status. 1: change is disabled.
	//pszResumeDefPwd = getenv(ENV_ACU_JUMP_DEFAULT_PASSWD);

	//if ((pszResumeDefPwd == NULL) ? FALSE
	//	: (atoi(pszResumeDefPwd) != 0) ? TRUE : FALSE)
	if(DAT_CurrentStatusIsRDONLY(ENV_ACU_JUMP_DEFAULT_PASSWD))
	{
		AppLogOut(USER_MANAGE_LOG, APP_LOG_WARNING, 
			"The switch for resumming default password of admin is ON, "
			"The password of admin was resummed to default.\n");  

		ModifyUserInfo(ADMIN_DEFAULT_NAME, ADMIN_DEFAULT_PASSWORD, -1);
#ifdef	NEED_ES_ADMIN
		ModifyUserInfo(ES_ADMIN_DEFAULT_NAME, ES_ADMIN_DEFAULT_PASSWORD, -1);
#endif
#ifdef	NEED_ES_ENGINEER
		ModifyUserInfo(ES_ENGINEER_DEFAULT_NAME, ES_ENGINEER_DEFAULT_PASSWORD, -1);
#endif
	}

	return TRUE;
}



void DestroyUserManager(void)
{
	if (g_hMutexUserManage != NULL)
	{
		Mutex_Destroy(g_hMutexUserManage);
		
		g_hMutexUserManage = NULL;
	}

	if (g_hMutexUserInfoToFlash != NULL)
	{
		Mutex_Destroy(g_hMutexUserInfoToFlash);

		g_hMutexUserInfoToFlash = NULL;
	}
}


/*==========================================================================*
* FUNCTION : GetUserNum
* PURPOSE  : Get user number
* CALLS    : 
* CALLED BY: To be called by the services
* ARGUMENTS: int nGetFlag : 0, Get maximum user number
1, Get actual  user number
* RETURN   : int : user number
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-28 17:30
*==========================================================================*/
int GetUserNum(int nGetFlag)
{
	if (nGetFlag == GET_MAXIMUM_USER_NUM)
	{
		return USERS_MAX_NUMBER;

	}
	else if (nGetFlag == GET_ACTUAL_USER_NUM)
	{
		return g_nUserNumber;

	}
	else
	{
		return -1;

	}
}


/*==========================================================================*
* FUNCTION : UserExist
* PURPOSE  : Get the user offset 
* CALLS    : 
* CALLED BY: AddUserInfo, DeleteUserInfo, ModifyUserInfo, FindUserInfo
* ARGUMENTS: char*  pszName : the wanted user name
* RETURN   : static int : the user offset, if -1, user not exist
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-28 17:33
*==========================================================================*/
static int UserExist(char* pszName)
{
	int i;

	for (i = 0; i < g_nUserNumber;i++)
	{
		if (strcasecmp(g_UserManage[i].szUserName, pszName) == 0)
		{
			return i;
		}
	}

	return -1;
}

/*==========================================================================*
* FUNCTION : AddUserInfo
* PURPOSE  : Add a user
* CALLS    : UserExist
* CALLED BY: To be called by the services
* ARGUMENTS: char  *pszName     : user name
*            char  *pszPassword : user password
*            int   iAuthority   : user authority
* RETURN   : int : error code
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-28 17:36
*==========================================================================*/
int AddUserInfo(char *pszName, char *pszPassword, int iAuthority)
{
	int nError;

	
	if (iAuthority <= ADMIN_LEVEL && iAuthority >= BROWSER_LEVEL)
	{		
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	if (g_nUserNumber < USERS_MAX_NUMBER)
	{
		if (UserExist(pszName) == -1)
		{
			strncpyz(g_UserManage[g_nUserNumber].szUserName, pszName,
				USERNAME_LEN);

			strncpyz(g_UserManage[g_nUserNumber].szPassword, pszPassword,
				PASSWORD_LEN);

			g_UserManage[g_nUserNumber].byLevel = iAuthority;

			g_nUserNumber += 1;

			UpdateUserInfoToFlash();
		}
		else
		{
			nError = ERR_SEC_USER_ALREADY_EXISTED;
		}
	}
	else
	{
		nError = ERR_SEC_TOO_MANY_USERS;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

//changed by Frank Wu,4/N/15,20140527, for e-mail
int AddUserInfo2(char *pszName, char *pszPassword, int iAuthority, char *pszLcdEnable)
{
	int nError;


	if (iAuthority <= ADMIN_LEVEL && iAuthority >= BROWSER_LEVEL)
	{		
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	if (g_nUserNumber < USERS_MAX_NUMBER)
	{
		if (UserExist(pszName) == -1)
		{
			strncpyz(g_UserManage[g_nUserNumber].szUserName, pszName,
				USERNAME_LEN);

			strncpyz(g_UserManage[g_nUserNumber].szPassword, pszPassword,
				PASSWORD_LEN);

			strncpyz(g_UserManage[g_nUserNumber].szLcdEnable, pszLcdEnable,
					LCD_ENABLE_LEN);

			g_UserManage[g_nUserNumber].byLevel = iAuthority;

			g_nUserNumber += 1;

			UpdateUserInfoToFlash();
		}
		else
		{
			nError = ERR_SEC_USER_ALREADY_EXISTED;
		}
	}
	else
	{
		nError = ERR_SEC_TOO_MANY_USERS;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

//changed by Frank Wu,4/N/15,20140527, for e-mail
int AddUserInfo3(char *pszName, char *pszPassword, int iAuthority, char *pszLcdEnable, char *pszAccountEnable, char *pszAccountLock, int iAccountCount, int iAccountStatus, char *pszStrongPass)
{
	int nError;


	if (iAuthority <= ADMIN_LEVEL && iAuthority >= BROWSER_LEVEL)
	{
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	if (g_nUserNumber < USERS_MAX_NUMBER)
	{
		if (UserExist(pszName) == -1)
		{
			strncpyz(g_UserManage[g_nUserNumber].szUserName, pszName,
				USERNAME_LEN);

			strncpyz(g_UserManage[g_nUserNumber].szPassword, pszPassword,
				PASSWORD_LEN);

			strncpyz(g_UserManage[g_nUserNumber].szLcdEnable, pszLcdEnable,
					LCD_ENABLE_LEN);

			strncpyz(g_UserManage[g_nUserNumber].szAccountEnable, pszAccountEnable,
					ACCOUNT_ENABLE_LEN);

			strncpyz(g_UserManage[g_nUserNumber].szAccountLock, pszAccountLock,
					ACCOUNT_ENABLE_LEN);

			strncpyz(g_UserManage[g_nUserNumber].szStrongPass, pszStrongPass,
					STRONG_PASS_ENABLE_LEN);

			g_UserManage[g_nUserNumber].iAccountCount = iAccountCount;

			g_UserManage[g_nUserNumber].iAccountStatus = iAccountStatus;

			g_UserManage[g_nUserNumber].byLevel = iAuthority;

			g_nUserNumber += 1;

			UpdateUserInfoToFlash();
		}
		else
		{
			nError = ERR_SEC_USER_ALREADY_EXISTED;
		}
	}
	else
	{
		nError = ERR_SEC_TOO_MANY_USERS;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

int AddUserInfoStru(USER_INFO_STRU* pUserInfo)
{
	int nError;
	char *pszName = NULL;
	int iAuthority = -1;

	if(NULL == pUserInfo)
	{
		return ERR_SEC_INVALID_USER_INFO;
	}
	pszName = pUserInfo->szUserName;
	iAuthority = pUserInfo->byLevel;

	if (iAuthority <= ADMIN_LEVEL && iAuthority >= BROWSER_LEVEL)
	{		
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	if (g_nUserNumber < USERS_MAX_NUMBER)
	{
		if (UserExist(pszName) == -1)
		{
			memcpy(&g_UserManage[g_nUserNumber],
					pUserInfo,
					sizeof(USER_INFO_STRU));

			g_nUserNumber += 1;

			UpdateUserInfoToFlash();
		}
		else
		{
			nError = ERR_SEC_USER_ALREADY_EXISTED;
		}
	}
	else
	{
		nError = ERR_SEC_TOO_MANY_USERS;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

/*==========================================================================*
* FUNCTION : ModifyUserInfo
* PURPOSE  : Modify user info
* CALLS    : UserExist
* CALLED BY: To be called by the services
* ARGUMENTS: char  *pszName        : user name
*            char  *pszNewPassword : new user password
*            int   iNewAuthority   : new authority
* RETURN   : int : error code
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-28 17:40
*==========================================================================*/
int ModifyUserInfo(char *pszName, char *pszNewPassword, int iNewAuthority)
{

	int nError;

	int nFind;

	if((iNewAuthority == -1)
		|| (iNewAuthority <= ADMIN_LEVEL && iNewAuthority >= BROWSER_LEVEL))
	{		
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (strcasecmp(pszName, ADMIN_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}

#ifdef	NEED_ES_ADMIN
	if (strcasecmp(pszName, ES_ADMIN_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}
#endif

#ifdef	NEED_ES_ENGINEER
	if (strcasecmp(pszName, ES_ENGINEER_DEFAULT_NAME) == 0
	    && (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
	    return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}
#endif

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	nFind = UserExist(pszName);

	if (nFind >= 0)
	{
		if (pszNewPassword)
		{
			strncpyz(g_UserManage[nFind].szPassword, pszNewPassword, 
				PASSWORD_LEN);
		}

		if (iNewAuthority != -1)
		{
			g_UserManage[nFind].byLevel = iNewAuthority;
		}

		UpdateUserInfoToFlash();
	}
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

//changed by Frank Wu,5/N/15,20140527, for e-mail
int ModifyUserInfo2(char *pszName, char *pszNewPassword, int iNewAuthority, char *pszLcdEnable)
{

	int nError;

	int nFind;

	if((iNewAuthority == -1)
		|| (iNewAuthority <= ADMIN_LEVEL && iNewAuthority >= BROWSER_LEVEL))
	{		
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (strcasecmp(pszName, ADMIN_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}

#ifdef	NEED_ES_ADMIN
	if (strcasecmp(pszName, ES_ADMIN_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}
#endif

#ifdef	NEED_ES_ENGINEER
	if (strcasecmp(pszName, ES_ENGINEER_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}
#endif

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	nFind = UserExist(pszName);

	if (nFind >= 0)
	{
		if (pszNewPassword)
		{
			strncpyz(g_UserManage[nFind].szPassword, pszNewPassword, 
				PASSWORD_LEN);
		}

		if(pszLcdEnable)
		{
			strncpyz(g_UserManage[nFind].szLcdEnable, pszLcdEnable,
					LCD_ENABLE_LEN);

		}

		if (iNewAuthority != -1)
		{
			g_UserManage[nFind].byLevel = iNewAuthority;
		}

		UpdateUserInfoToFlash();
	}
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

int ModifyUserInfo3(char *pszName, char *pszNewPassword, int iNewAuthority, char *pszLcdEnable, char *pszAccountEnable, char *pszAccountLock, int iAccountCount,int iAccountStatus, char *pszStrongPass)
{

	int nError;

	int nFind;

	if((iNewAuthority == -1)
		|| (iNewAuthority <= ADMIN_LEVEL && iNewAuthority >= BROWSER_LEVEL))
	{
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (strcasecmp(pszName, ADMIN_DEFAULT_NAME) == 0
		&& iNewAuthority == ADMIN_LEVEL && strcasecmp("Enabled",pszStrongPass) == 0)
	{
		return ERR_SEC_CANNOT_SET_STRONG_PASS;
	}

	if (strcasecmp(pszName, ADMIN_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}

#ifdef	NEED_ES_ADMIN
	if (strcasecmp(pszName, ES_ADMIN_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}
#endif

#ifdef	NEED_ES_ENGINEER
	if (strcasecmp(pszName, ES_ENGINEER_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}
#endif

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	nFind = UserExist(pszName);

	if (nFind >= 0)
	{
		if (pszNewPassword)
		{
			strncpyz(g_UserManage[nFind].szPassword, pszNewPassword,
				PASSWORD_LEN);
		}

		if(pszLcdEnable)
		{
			strncpyz(g_UserManage[nFind].szLcdEnable, pszLcdEnable,
					LCD_ENABLE_LEN);
		}

		if(pszAccountEnable)
		{
			strncpyz(g_UserManage[nFind].szAccountEnable, pszAccountEnable,
								LCD_ENABLE_LEN);
			g_UserManage[nFind].iAccountCount = iAccountCount;
			g_UserManage[nFind].iAccountStatus = iAccountStatus;

		}

		if(pszAccountLock)
		{
			strncpyz(g_UserManage[nFind].szAccountLock, pszAccountLock,
								LCD_ENABLE_LEN);
		}

		if(pszStrongPass)
		{
			strncpyz(g_UserManage[nFind].szStrongPass, pszStrongPass,
					STRONG_PASS_ENABLE_LEN);
		}

		if (iNewAuthority != -1)
		{
			g_UserManage[nFind].byLevel = iNewAuthority;
		}

		UpdateUserInfoToFlash();
	}
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}


int ModifyUserInfoStru(USER_INFO_STRU* pUserInfo)
{

	int nError;
	int nFind;
	char *pszName = NULL;
	int iNewAuthority = -1;

	if(NULL == pUserInfo)
	{
		return ERR_SEC_INVALID_USER_INFO;
	}
	pszName = pUserInfo->szUserName;
	iNewAuthority = pUserInfo->byLevel;

	if((iNewAuthority == -1)
		|| (iNewAuthority <= ADMIN_LEVEL && iNewAuthority >= BROWSER_LEVEL))
	{
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (strcasecmp(pszName, ADMIN_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}

#ifdef	NEED_ES_ADMIN
	if (strcasecmp(pszName, ES_ADMIN_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}
#endif

#ifdef	NEED_ES_ENGINEER
	if (strcasecmp(pszName, ES_ENGINEER_DEFAULT_NAME) == 0
		&& (iNewAuthority != -1 && iNewAuthority != ADMIN_LEVEL ))
	{
		return ERR_SEC_CANNOT_MODIFY_ADMIN_LEVEL;
	}
#endif

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	nFind = UserExist(pszName);

	if (nFind >= 0)
	{
		memcpy(&g_UserManage[nFind],
				pUserInfo,
				sizeof(USER_INFO_STRU));

		UpdateUserInfoToFlash();
	}
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

int ModifyUserInfoStru2(USER_INFO_STRU* pUserInfo)  // Added by Koustubh Mattikalli (IA-01-04)
{

	int nError;
	int nFind;
	char *pszName = NULL;
	int iNewAuthority = -1;

	if(NULL == pUserInfo)
	{
		return ERR_SEC_INVALID_USER_INFO;
	}
	pszName = pUserInfo->szUserName;
	iNewAuthority = pUserInfo->byLevel;

	if((iNewAuthority == -1)
		|| (iNewAuthority <= ADMIN_LEVEL && iNewAuthority >= BROWSER_LEVEL))
	{
	}
	else
	{
		return ERR_INVALID_USER_LEVEL;
	}

	nError = ERR_SEC_OK;

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	nFind = UserExist(pszName);

	if (nFind >= 0)
	{
		memcpy(&g_UserManage[nFind],
				pUserInfo,
				sizeof(USER_INFO_STRU));

		UpdateUserInfoToFlash();
	}
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

/*==========================================================================*
* FUNCTION : DeleteUserInfo
* PURPOSE  : Delete a user info
* CALLS    : UserExist
* CALLED BY: To be called by the services
* ARGUMENTS: char  *pszName : user name
* RETURN   : int : error code
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-28 17:43
*==========================================================================*/
int DeleteUserInfo(char *pszName)
{
	int nError;

	int nFind;

	//	ASSERT(strlen(pszName)		< USERNAME_LEN);

	nError = ERR_SEC_OK;

	if (strcasecmp(pszName, ADMIN_DEFAULT_NAME) == 0)
	{
		return ERR_SEC_CANNOT_DELETE_ADMIN;
	}
#ifdef	NEED_ES_ADMIN	
	if (strcasecmp(pszName, ES_ADMIN_DEFAULT_NAME) == 0)
	{
		return ERR_SEC_CANNOT_DELETE_ADMIN;
	}
#endif
#ifdef	NEED_ES_ENGINEER	
	if (strcasecmp(pszName, ES_ENGINEER_DEFAULT_NAME) == 0)
	{
		return ERR_SEC_CANNOT_DELETE_ENGINEER;
	}
#endif
	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	nFind = UserExist(pszName);
        
	if (nFind >= 0)
	{
		memmove(&(g_UserManage[nFind]), 
			&(g_UserManage[nFind + 1]), 
			sizeof(USER_INFO_STRU)* (g_nUserNumber - nFind - 1));

		memset(&(g_UserManage[g_nUserNumber - 1]), 0,
			sizeof(USER_INFO_STRU));

		g_nUserNumber--;

		UpdateUserInfoToFlash();
	}
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;

}

/*==========================================================================*
* FUNCTION : FindUserInfo
* PURPOSE  : Find a user info according to user name
* CALLS    : UserExist
* CALLED BY: To be called by the services
* ARGUMENTS: char            *pUserName : user name 
*            USER_INFO_STRU  *pUserInfo : the return user info
* RETURN   : int : error code
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-28 17:47
*==========================================================================*/
int FindUserInfo(char *pUserName, USER_INFO_STRU *pUserInfo)
{
	int nError;

	int nFind;

	//	ASSERT(strlen(pszName)		< USERNAME_LEN);

	nError = ERR_SEC_OK;

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	nFind = UserExist(pUserName);

	if (nFind >= 0)
	{
		if (pUserInfo != NULL)
		{
			memcpy(pUserInfo, &(g_UserManage[nFind]), sizeof(USER_INFO_STRU));
		}

	}
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;

}

/*==========================================================================*
* FUNCTION : FindUserInfo
* PURPOSE  : Find a user info according to user name
* CALLS    : UserExist
* CALLED BY: To be called by the services
* ARGUMENTS: char            *pUserName : user name
*            USER_INFO_STRU  *pUserInfo : the return user info
* RETURN   : int : error code
* COMMENTS :
* CREATOR  : HULONGWEN                DATE: 2004-09-28 17:47
*==========================================================================*/
int FindUpdateUserInfo(char *pUserName, USER_INFO_STRU *pUserInfo)
{
	int nError;

	int nFind;

	//	ASSERT(strlen(pszName)		< USERNAME_LEN);

	nError = ERR_SEC_OK;

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}

	nFind = UserExist(pUserName);

	if (nFind >= 0)
	{
		if (pUserInfo != NULL)
		{
			memcpy(&(g_UserManage[nFind]), pUserInfo, sizeof(USER_INFO_STRU));
		}

	}
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;

}

/*==========================================================================*
* FUNCTION : FindUser
* PURPOSE  : Find a user according to user name
* CALLS    : 
* CALLED BY: To be called by the web_user
* ARGUMENTS: char            *pUserName : user name 
* RETURN   : int : the user sequence -1 means no user can be found
* COMMENTS : 
* CREATOR  : Zicheng Zhao                DATE: 2013-07-13
*==========================================================================*/
int FindUser(char *pUserName)
{
    int nFind;

    nFind = UserExist(pUserName);

    return(nFind);
}

/*==========================================================================*
* FUNCTION : FindUser
* PURPOSE  : Find a user according to user name
* CALLS    : 
* CALLED BY: To be called by the web_user
* ARGUMENTS: char            *pUserName : user name 
* RETURN   : int : the user sequence -1 means no user can be found
* COMMENTS : 
* CREATOR  : Zicheng Zhao                DATE: 2013-07-13
*==========================================================================*/
char* FindUserAddr(int iFind)
{
    if(iFind >= 0)
    {
	return g_UserManage[iFind].szContactAddr;
    }
    else
    {
	return NULL;
    }
}

/*==========================================================================*
* FUNCTION : GetUserInfo
* PURPOSE  : Get user info
* CALLS    : 
* CALLED BY: To be called by the services
* ARGUMENTS: USER_INFO_STRU**  pUserInfo : the user info pointer
* RETURN   : int : the actual user number
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-28 17:49
*==========================================================================*/
int GetUserInfo(USER_INFO_STRU** pUserInfo)
{
	ASSERT(pUserInfo);

	*pUserInfo = g_UserManage;

	return g_nUserNumber;
}

int GetUserInfoForWeb(USER_INFO_STRU** pUserInfo)
{
	ASSERT(pUserInfo);
	memcpy(g_UserWebManage,g_UserManage, g_nUserNumber * sizeof(USER_INFO_STRU));
	//g_UserWebManage = g_UserManage;
	*pUserInfo = g_UserWebManage;

	return g_nUserNumber;
}
/*==========================================================================*
* FUNCTION : FindUserInfo_ByIndex
* PURPOSE  : Get user info by index number (IA-01-04)
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: pUserIdx: index number, USER_INFO_STRU**  pUserInfo : the user info pointer
* RETURN   : int : the actual user number
* COMMENTS : 
* CREATOR  : Koustubh Mattikalli                DATE: 
*==========================================================================*/
int FindUserInfo_ByIndex(int pUserIdx, USER_INFO_STRU *pUserInfo)
{
	int nError;

	nError = ERR_SEC_OK;

	if (Mutex_Lock(g_hMutexUserManage, USR_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_USR_MUTEX_WAIT_TIMEOUT;
	}
        if(g_nUserNumber >=0)
        {
		if (pUserInfo != NULL)
		{
                        //printf("\n g_UserManage[%d]: %s\n", pUserIdx, g_UserManage[pUserIdx].szUserName);
			memcpy(pUserInfo, &(g_UserManage[pUserIdx]), sizeof(USER_INFO_STRU));
		}
        }
	else
	{
		nError = ERR_SEC_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_hMutexUserManage);

	return nError;
}

void user_lockout_init(void)
{
	printf("\n\n%s\n",__FUNCTION__);
	int nError, nUserNum, i;
    time_t diff;
    time_t current_time = 0;
    USER_INFO_STRU *pUserInfo;

    nUserNum = GetUserNum(GET_ACTUAL_USER_NUM);
    if(nUserNum > 0)
    {
        pUserInfo = NEW(USER_INFO_STRU,1);
        if(pUserInfo == NULL)
        {
			return -1;
        }        
        
        for(i = 0; i < nUserNum; i++)
        {
            diff = 0;
            memset(pUserInfo,0x00,sizeof(USER_INFO_STRU));
            nError = FindUserInfo_ByIndex(i, pUserInfo);

            printf("\n\n User:%s\n iTime: %d\n count: %d\n isLock: %d\n interval: %d\n\n",
															   pUserInfo->szUserName,
                                                               pUserInfo->stLockout.iTime,
                                                               pUserInfo->stLockout.iCount,
                                                               pUserInfo->stLockout.isLock,
															   pUserInfo->stLockout.iInterval);

            if(nError == ERR_SEC_OK && (pUserInfo->stLockout.iTime) > 0)
            {
				user_lockout_enteredWrongPass(pUserInfo, 1);
			}
		}
	}
}

int user_lockout_enteredCorrectPass(USER_INFO_STRU *pUserInfo)
{
	printf("\n\n%s\t(%s)\n",__FUNCTION__,pUserInfo->szUserName);
	time_t now = 0;
	time(&now);
	printf("time diff: %d\n",now - pUserInfo->stLockout.iTime);
	printf("lock: %d\n",pUserInfo->stLockout.isLock);	
	time_print("now",now);	
	time_print("iTime",pUserInfo->stLockout.iTime);
	if((pUserInfo->stLockout.isLock == 0))
	{
		pUserInfo->stLockout.iTime = 0;
		pUserInfo->stLockout.iCount = 0;
		pUserInfo->stLockout.iInterval = INIT;		
	}
	else
	{
		if((now - pUserInfo->stLockout.iTime) >= UNLOCK_INTERVAL)
		{
			printf("Unlock interval\n");
			pUserInfo->stLockout.isLock = 0;
			pUserInfo->stLockout.iCount = 0;
			pUserInfo->stLockout.iInterval = INIT;
			pUserInfo->stLockout.iTime = 0;
		}
		else
		{
			printf("User is locked\n");
		}
		
	}
	return pUserInfo->stLockout.isLock;	
}

int user_lockout_enteredWrongPass(USER_INFO_STRU *pUserInfo, int validateOnly)
{
	printf("\n\n%s\t(%s)\n",__FUNCTION__,pUserInfo->szUserName);
	time_t now = 0;
	time(&now);
	// printf("interval: %d\n",pUserInfo->stLockout.iInterval);
	printf("time diff: %d unlock: %d lock:%d\n",now - pUserInfo->stLockout.iTime,UNLOCK_INTERVAL,LOCK_INTERVAL);	
	time_print("now",now);	
	time_print("iTime",pUserInfo->stLockout.iTime);
	
	if(pUserInfo->stLockout.iInterval == LOCK)
	{
		printf("Lock interval\n");		
		time_print("Lock Time",pUserInfo->stLockout.iTime + LOCK_INTERVAL);

		if((now - pUserInfo->stLockout.iTime) < LOCK_INTERVAL)
		{
			if(validateOnly == 0)
				pUserInfo->stLockout.iCount++;
		
			if(pUserInfo->stLockout.iCount >= MAX_WRNGPASS)
			{
				pUserInfo->stLockout.isLock = 1;
				pUserInfo->stLockout.iInterval = UNLOCK;
				pUserInfo->stLockout.iTime = now;
				printf("User locked\n");
			}
		}
		else 
		{	
			pUserInfo->stLockout.isLock = 0;
			pUserInfo->stLockout.iTime = now;
			pUserInfo->stLockout.iCount = 1;
			if(validateOnly == 1)
			{				
				pUserInfo->stLockout.iInterval = INIT;				
			}									
			printf("Reset lock\n");		
		}		
	}
	else if((pUserInfo->stLockout.iInterval == UNLOCK))
	{
			printf("Unlock interval\n");			
			time_print("Unlock Time",pUserInfo->stLockout.iTime + UNLOCK_INTERVAL);
			if(((now - pUserInfo->stLockout.iTime) >= UNLOCK_INTERVAL))
			{
			pUserInfo->stLockout.isLock = 0;
			pUserInfo->stLockout.iCount = 1;			
			if(validateOnly == 1)
			{				
				pUserInfo->stLockout.iInterval = INIT;				
			}
			else
			{
				pUserInfo->stLockout.iInterval = LOCK;
			}			
			pUserInfo->stLockout.iTime = now;
			}
			else if(pUserInfo->stLockout.iCount == MAX_WRNGPASS)
			{
				pUserInfo->stLockout.iCount = MAX_WRNGPASS + 1;
			}						
	}
	else if(pUserInfo->stLockout.iInterval == INIT && validateOnly == 0)
	{
		printf("Initial interval\n");
		pUserInfo->stLockout.isLock = 0;
		pUserInfo->stLockout.iInterval = LOCK;
		pUserInfo->stLockout.iTime = now;
		pUserInfo->stLockout.iCount = 1;
	}
	printf("count: %d\n",pUserInfo->stLockout.iCount);
	return pUserInfo->stLockout.isLock;
}

void time_print(char* str, time_t t)
{
	struct tm *time = gmtime(&t);
	printf("%s: %d:%d:%d\n",str,time->tm_hour,time->tm_min,time->tm_sec);
}

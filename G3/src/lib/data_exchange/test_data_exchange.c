/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : test_data_exchange.c
*  CREATOR  : HULONGWEN                DATE: 2004-09-20 21:32
*  VERSION  : V1.00
*  PURPOSE  : To test data_exchange.so
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>

#include "data_exchange.h"

//���ڴ�׮��data_exchange.c��Ҫ�õ���ȫ�ֱ����ͺ���

//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO  g_SiteInfo;

//need to be defined in Miscellaneous Function Module
TIME_SRV_INFO g_sTimeSrvInfo;

//need to be defined in equip management Module
int Equip_Control(int nEquipID, int nSigType, int nSigID, 
				  VAR_VALUE *pCtrlValue, DWORD dwTimeout)
{
	UNUSED(nEquipID);
	UNUSED(nSigType);
	UNUSED(nSigID);
	UNUSED(dwTimeout);

	return TRUE;
}

//need to be defined in Miscellaneous Function Module
int UpdateACUTime (time_t* pTimeRet)
{
	return stime(pTimeRet);
}

//Test call back function
struct tagsParamStru
{
	int nAItem;
	int nBItem;
}g_stParam;

int ServiceNotification(HANDLE hService,			//Service handle
						int	  nMsgType,			//Message type
						int	  nTrapDataLength,	//Message length
						void*  lpTrapData,		//Message buffer
						void*  lpParam,			//Parameter
						BOOL   bUrgent			//Urgent level
						)
{
	UNUSED(hService);

	if (bUrgent)
	{
		printf("���������\r\n");
	}
	else
	{
		printf("������������\r\n");
	}
	printf("filename: SelfTest.cpp [ServiceNotification]: 0x%08x 0x%08x 0x%08x 0x%08x\r\n", 
		nTrapDataLength,
		nMsgType, 
		(int)lpTrapData, 
		(int)lpParam);


	if ((struct tagsParamStru*)lpParam == &g_stParam)
	{
		printf("ServiceNotification() call back success!\r\n");
	}
	else
	{
		printf("ServiceNotification() call back failure!\r\n");

	}
	return 0;
}

//extern int GetEquipInfoByEquipID(int					nEquipID, 
//								 EQUIP_INFO**			pEquip,
//								 STDEQUIP_TYPE_INFO**	pStdEquipType);

//Test DxiGetData() and DxiSetData().It maybe a reference of the get and set use
void TestDataExchange(void)
{
	int		nInterfaceType;
	int		nVarID = 0;		//Data ID
	int		nVarSubID = 0;	//Data sub ID
	int		nBufLen;
	int		nTimeOut = 0;		//Time out

	int		nError = ERR_DXI_OK;


	printf("Data exchange module self_text start!\r\n");
	printf("please press ENTER key!\r\n");

	getchar();

	//Test GetEquipNum()
	{
		int nEquipNumRet;

		nInterfaceType = VAR_ACU_EQUIPS_NUM;

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nEquipNumRet,			
			nTimeOut);

		if (nError == ERR_DXI_OK && nEquipNumRet == g_SiteInfo.iEquipNum)
		{
			printf("Test GetEquipNum() success\r\n");
		}
		else
		{
			printf("Test GetEquipNum() failure nError = %d\r\n", nError);
		}
	}
	//Test GetEquipList()
	{
		EQUIP_INFO* pEquipInfo;

		nInterfaceType = VAR_ACU_EQUIPS_LIST;

		DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pEquipInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK && pEquipInfo == g_SiteInfo.pEquipInfo)
		{
			printf("Test GetEquipList() success\r\n");
		}
		else
		{
			printf("Test GetEquipList() failure nError = %d\r\n", nError);
		}
	}
	//Test GetEquipInfoByEquipID()--a static interface
	/*{
	int	nEquipID = 51; 
	EQUIP_INFO*			pEquip;
	STDEQUIP_TYPE_INFO*	pStdEquipType;

	nError = GetEquipInfoByEquipID(nEquipID,
	&pEquip,			
	&pStdEquipType);

	nEquipID = 8;

	nError = GetEquipInfoByEquipID(nEquipID,
	&pEquip,			
	&pStdEquipType);

	nEquipID = 2;

	nError = GetEquipInfoByEquipID(nEquipID,
	&pEquip,			
	&pStdEquipType);

	printf("Test GetEquipInfoByEquipID() success\r\n");
	}*/
	//Test GetSamSigNumofEquip()
	{
		int nSampleSigNumRet;

		nInterfaceType = VAR_SAM_SIG_NUM_OF_EQUIP;

		nVarID = 51; //�豸ID

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nSampleSigNumRet,			
			nTimeOut);

		nVarID = 40; //�豸ID

		if (nError == ERR_DXI_OK 
			&& nSampleSigNumRet == 
			g_SiteInfo.pEquipInfo[0].pStdEquip->iSampleSigNum)
		{
			printf("Test GetSamSigNumofEquip() success\r\n");
		}
		else
		{
			printf("Test GetSamSigNumofEquip() failure nError = %d\r\n", nError);
		}

		/*nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&nSampleSigNumRet,			
		nTimeOut);

		printf("Test GetSamSigNumofEquip() success\r\n");*/
	}
	//Test GetSamSigStruofEquip()
	{
		SAMPLE_SIG_INFO* pSampleSigInfo;

		nVarID = 51; //�豸ID
		nInterfaceType = VAR_SAM_SIG_STRU_OF_EQUIP;
		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pSampleSigInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& pSampleSigInfo == 
			g_SiteInfo.pEquipInfo[0].pStdEquip->pSampleSigInfo)
		{
			printf("Test GetSamSigStruofEquip() success\r\n");
		}
		else
		{
			printf("Test GetSamSigStruofEquip() failure nError = %d\r\n", nError);
		}

		/*nVarID = 40;
		nInterfaceType = VAR_SAM_SIG_STRU_OF_EQUIP;
		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pSampleSigInfo,			
		nTimeOut);

		printf("Test GetSamSigStruofEquip() success\r\n");*/
	}
	//Test GetSamSigValueofEquip()
	{
		SAMPLE_SIG_VALUE* pSampleSigValue;

		nInterfaceType = VAR_SAM_SIG_VALUE_OF_EQUIP;

		nVarID = 51; //�豸ID

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pSampleSigValue,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& pSampleSigValue == 
			g_SiteInfo.pEquipInfo[0].pSampleSigValue)
		{
			printf("Test GetSamSigValueofEquip() success\r\n");
		}
		else
		{
			printf("Test GetSamSigValueofEquip() failure nError = %d\r\n", nError);
		}

		/*nVarID = 30;

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pSampleSigValue,			
		nTimeOut);

		printf("Test GetSamSigValueofEquip() success\r\n");*/
	}

	//Test GetSetSigNumofEquip()
	{
		int nSetSigNumRet;
		nInterfaceType = VAR_SET_SIG_NUM_OF_EQUIP;

		nVarID = 58; //�豸ID

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nSetSigNumRet,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& nSetSigNumRet == 
			g_SiteInfo.pEquipInfo[2].pStdEquip->iSetSigNum)
		{
			printf("Test GetSetSigNumofEquip() success\r\n");
		}
		else
		{
			printf("Test GetSetSigNumofEquip() failure nError = %d\r\n", nError);
		}



		/*	nVarID = 40;

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&nSetSigNumRet,			
		nTimeOut);

		printf("Test GetSetSigNumofEquip() success\r\n");*/
	}
	//Test GetSetSigStruofEquip()
	{
		SET_SIG_INFO* pSetSigInfo;

		nVarID = 58; //�豸ID
		nInterfaceType = VAR_SET_SIG_STRU_OF_EQUIP;
		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pSetSigInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& pSetSigInfo == 
			g_SiteInfo.pEquipInfo[2].pStdEquip->pSetSigInfo)
		{
			printf("Test GetSetSigStruofEquip() success\r\n");
		}
		else
		{
			printf("Test GetSetSigStruofEquip() failure nError = %d\r\n", nError);
		}

		/*	nVarID = 51;
		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pSetSigInfo,			
		nTimeOut);

		printf("Test GetSetSigStruofEquip() success\r\n");*/
	}
	//Test GetSetSigValueofEquip()
	{
		SET_SIG_VALUE* pSetSigValue;

		nInterfaceType = VAR_SET_SIG_VALUE_OF_EQUIP;

		nVarID = 58; //�豸ID

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pSetSigValue,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& pSetSigValue == 
			g_SiteInfo.pEquipInfo[2].pSetSigValue)
		{
			printf("Test GetSetSigValueofEquip() success\r\n");
		}
		else
		{
			printf("Test GetSetSigValueofEquip() failure nError = %d\r\n", nError);
		}

		/*nVarID = 51;

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pSetSigValue,			
		nTimeOut);

		printf("Test GetSetSigValueofEquip() success\r\n");*/
	}

	//Test GetConSigNumofEquip()
	{
		int nCtrlSigNumRet;
		nInterfaceType = VAR_CON_SIG_NUM_OF_EQUIP;

		nVarID = 2; //�豸ID

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nCtrlSigNumRet,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& nCtrlSigNumRet == 
			g_SiteInfo.pEquipInfo[1].pStdEquip->iCtrlSigNum)
		{
			printf("Test GetConSigNumofEquip() success\r\n");
		}
		else
		{
			printf("Test GetConSigNumofEquip() failure nError = %d\r\n", nError);
		}


		/*nVarID = 51;

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&nCtrlSigNumRet,			
		nTimeOut);

		printf("Test GetConSigNumofEquip() success\r\n");*/
	}
	//Test GetConSigStruofEquip()
	{
		CTRL_SIG_INFO* pCtrlSigInfo;

		nVarID = 2; //�豸ID
		nInterfaceType = VAR_CON_SIG_STRU_OF_EQUIP;
		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pCtrlSigInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& pCtrlSigInfo == 
			g_SiteInfo.pEquipInfo[1].pStdEquip->pCtrlSigInfo)
		{
			printf("Test GetConSigStruofEquip() success\r\n");
		}
		else
		{
			printf("Test GetConSigStruofEquip() failure nError = %d\r\n", nError);
		}

		/*nVarID = 40;

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pCtrlSigInfo,			
		nTimeOut);

		printf("Test GetConSigStruofEquip() success\r\n");*/
	}
	//Test GetConSigValueofEquip()
	{
		CTRL_SIG_VALUE* pCtrlSigValue;

		nInterfaceType = VAR_CON_SIG_VALUE_OF_EQUIP;

		nVarID = 2; //�豸ID

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pCtrlSigValue,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& pCtrlSigValue == 
			g_SiteInfo.pEquipInfo[1].pCtrlSigValue)
		{
			printf("Test GetConSigValueofEquip() success\r\n");
		}
		else
		{
			printf("Test GetConSigValueofEquip() failure nError = %d\r\n", nError);
		}

		/*nVarID = 51;

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pCtrlSigValue,			
		nTimeOut);

		printf("Test GetConSigValueofEquip() success\r\n");*/
	}

	//Test GetAlarmSigNumofEquip()
	{
		int nAlarmSigNumRet;
		nInterfaceType = VAR_ALARM_SIG_NUM_OF_EQUIP;

		nVarID = 2; //�豸ID

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nAlarmSigNumRet,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& nAlarmSigNumRet == 
			g_SiteInfo.pEquipInfo[1].pStdEquip->iAlarmSigNum)
		{
			printf("Test GetAlarmSigNumofEquip() success\r\n");
		}
		else
		{
			printf("Test GetAlarmSigNumofEquip() failure nError = %d\r\n", nError);
		}

		/*nVarID = 51;

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&nAlarmSigNumRet,			
		nTimeOut);

		printf("Test GetAlarmSigNumofEquip() success\r\n");*/
	}
	//Test GetAlarmSigStruofEquip()
	{
		ALARM_SIG_INFO* pAlarmSigInfo;

		nVarID = 2; //�豸ID
		nInterfaceType = VAR_ALARM_SIG_STRU_OF_EQUIP;
		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pAlarmSigInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK 
			&& pAlarmSigInfo == 
			g_SiteInfo.pEquipInfo[1].pStdEquip->pAlarmSigInfo)
		{
			printf("Test GetAlarmSigStruofEquip() success\r\n");
		}
		else
		{
			printf("Test GetAlarmSigStruofEquip() failure nError = %d\r\n", nError);
		}

		/*	nVarID = 51;
		nInterfaceType = VAR_ALARM_SIG_STRU_OF_EQUIP;
		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pAlarmSigInfo,			
		nTimeOut);

		printf("Test GetAlarmSigStruofEquip() success\r\n");*/
	}
	//Test GetActiveAlarmNum()
	{
		int nAlarmNum;

		printf("Test GetActiveAlarmNum() start\r\n");

		nError = 0;
		nInterfaceType = VAR_ACTIVE_ALARM_NUM;

		nVarID = ALARM_LEVEL_NONE; //Alarm level

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nAlarmNum,			
			nTimeOut);

		printf("All alarm num = %d start\r\n",nAlarmNum);

		nVarID = ALARM_LEVEL_OBSERVATION;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nAlarmNum,			
			nTimeOut);

		printf("Observation alarm num = %d start\r\n",nAlarmNum);

		nVarID = ALARM_LEVEL_MAJOR;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nAlarmNum,			
			nTimeOut);

		printf("Major alarm num = %d start\r\n",nAlarmNum);

		nVarID = ALARM_LEVEL_CRITICAL;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nAlarmNum,			
			nTimeOut);

		printf("Critical alarm num = %d start\r\n",nAlarmNum);

		if (nError == ERR_DXI_OK)
		{
			printf("Test GetActiveAlarmNum() success\r\n");
		}
		else
		{
			printf("Test GetActiveAlarmNum() failure\r\n");
		}
	}

	//Test GetActiveAlarmInfo()
	{
#define GET_ALARM_NUM	4
		ALARM_SIG_VALUE stAlarmSigValue[GET_ALARM_NUM];

		printf("Test GetActiveAlarmInfo() start\r\n");

		nError = 0;

		memset(stAlarmSigValue, 0, sizeof(ALARM_SIG_VALUE) * GET_ALARM_NUM);

		nBufLen = sizeof(ALARM_SIG_VALUE) * GET_ALARM_NUM;//Should assign size

		nInterfaceType = VAR_ACTIVE_ALARM_INFO;

		nVarID = ALARM_LEVEL_NONE;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			stAlarmSigValue,			
			nTimeOut);

		nVarID = ALARM_LEVEL_OBSERVATION;

		memset(stAlarmSigValue, 0, sizeof(ALARM_SIG_VALUE) * GET_ALARM_NUM);

		nBufLen = sizeof(ALARM_SIG_VALUE) * GET_ALARM_NUM;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			stAlarmSigValue,			
			nTimeOut);

		nVarID = ALARM_LEVEL_MAJOR;

		memset(stAlarmSigValue, 0, sizeof(ALARM_SIG_VALUE) * GET_ALARM_NUM);

		nBufLen = sizeof(ALARM_SIG_VALUE) * GET_ALARM_NUM;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			stAlarmSigValue,			
			nTimeOut);

		nVarID = ALARM_LEVEL_CRITICAL;

		memset(stAlarmSigValue, 0, sizeof(ALARM_SIG_VALUE) * GET_ALARM_NUM);

		nBufLen = sizeof(ALARM_SIG_VALUE) * GET_ALARM_NUM;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			stAlarmSigValue,			
			nTimeOut);

		if (nError == ERR_DXI_OK)
		{
			printf("Test GetActiveAlarmInfo() success\r\n");
		}
		else
		{
			printf("Test GetActiveAlarmInfo() failure\r\n");
		}
	}

	//Test GetMultipleSignalsInfo()
	{
		GET_MULTIPAL_SIGNALS stGetMulSignals;

		printf("Test GetMultipleSignalsInfo() start\r\n");

		nError = 0;

		nBufLen = sizeof(GET_MULTIPAL_SIGNALS);

		nInterfaceType = VAR_MULTIPLE_SIGNALS_INFO;

		nVarID = MUL_SIGNALS_GET_VALUE;

		memset(&stGetMulSignals, 0, sizeof(GET_MULTIPAL_SIGNALS));

		stGetMulSignals.nGetNum = 4;
		stGetMulSignals.lData[0] = DXI_MERGE_UNIQUE_SIG_ID(2,  SIG_TYPE_CONTROL,  2);
		stGetMulSignals.lData[1] = DXI_MERGE_UNIQUE_SIG_ID(51, SIG_TYPE_SAMPLING, 2);
		stGetMulSignals.lData[2] = DXI_MERGE_UNIQUE_SIG_ID(58, SIG_TYPE_SETTING,  2);
		stGetMulSignals.lData[3] = DXI_MERGE_UNIQUE_SIG_ID(58, SIG_TYPE_SETTING,  7);

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&stGetMulSignals,			
			nTimeOut);

		nVarID = MUL_SIGNALS_GET_STRU;

		memset(&stGetMulSignals, 0, sizeof(GET_MULTIPAL_SIGNALS));

		stGetMulSignals.nGetNum = 4;

		stGetMulSignals.lData[0] = DXI_MERGE_UNIQUE_SIG_ID(51, SIG_TYPE_SAMPLING, 2);
		stGetMulSignals.lData[1] = DXI_MERGE_UNIQUE_SIG_ID(58, SIG_TYPE_SETTING,  2);
		stGetMulSignals.lData[2] = DXI_MERGE_UNIQUE_SIG_ID(2,  SIG_TYPE_CONTROL,  2);
		stGetMulSignals.lData[3] = DXI_MERGE_UNIQUE_SIG_ID(58, SIG_TYPE_SETTING,  7);

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&stGetMulSignals,			
			nTimeOut);


		if (nError == ERR_DXI_OK)
		{
			printf("Test GetMultipleSignalsInfo() success\r\n");
		}
		else
		{
			printf("Test GetMultipleSignalsInfo() failure\r\n");
		}
	}



	//Test GetAEquipInfo()
	{
		EQUIP_INFO* pEquipInfo;

		nInterfaceType = VAR_A_EQUIP_INFO;

		nVarID = 2;

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pEquipInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK
			&& pEquipInfo == &(g_SiteInfo.pEquipInfo[1]))
		{
			printf("Test GetAEquipInfo() success\r\n");
		}
		else
		{
			printf("Test GetAEquipInfo() failure\r\n");
		}

		/*nVarID = 5;

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pEquipInfo,			
		nTimeOut);

		printf("Test GetAEquipInfo() success\r\n");*/
	}
	//Test SetAEquipInfo()
	{
		char* pStrSet = "�����й��ˣ���";
		nInterfaceType = VAR_A_EQUIP_INFO;

		nError = 0;
		nBufLen = strlen(pStrSet);

		nVarID		= 58;
		nVarSubID	= MODIFY_SIGNAL_ENGLISH_FULL_NAME;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStrSet,			
			nTimeOut);

		nVarID		= 58;
		nVarSubID	= MODIFY_EQUIP_LOCAL_FULL_NAME;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStrSet,			
			nTimeOut);

		nVarID		= 58;
		nVarSubID	= MODIFY_EQUIP_ENGLISH_ABBR_NAME;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStrSet,			
			nTimeOut);

		nVarID		= 58;
		nVarSubID	= MODIFY_EQUIP_LOCAL_ABBR_NAME;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStrSet,			
			nTimeOut);

		if (nError == ERR_DXI_OK)
		{
			printf("Test SetAEquipInfo() success\r\n");
		}
		else
		{
			printf("Test SetAEquipInfo() failure\r\n");
		}
	}
	//Test GetASignalInfoStru()
	{
		SET_SIG_INFO*	pSetSigInfo;
		//ALARM_SIG_INFO* pAlarmSigInfo;

		nInterfaceType = VAR_A_SIGNAL_INFO_STRU;

		nVarID = 58;

		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 2);

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pSetSigInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK
			&& pSetSigInfo == &(g_SiteInfo.pEquipInfo[2].pStdEquip->pSetSigInfo[1]))
		{
			printf("Test GetASignalInfoStru() success\r\n");
		}
		else
		{
			printf("Test GetASignalInfoStru() failure\r\n");
		}

		/*nVarID = 2;

		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, 1);

		nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pAlarmSigInfo,			
		nTimeOut);

		printf("Test GetASignalInfoStru() success\r\n");*/
	}
	//Test SetASignalInfoStru()
	{
		SET_A_SIGNAL_INFO_STU stSetASignalInfo;

		nError = 0;
		nBufLen = sizeof(SET_A_SIGNAL_INFO_STU);

		nInterfaceType = VAR_A_SIGNAL_INFO_STRU;

		nVarID = 2;
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, 1);

		stSetASignalInfo.byModifyType = MODIFY_ALARM_LEVEL;
		stSetASignalInfo.bModifyAlarmLevel = 1;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stSetASignalInfo,			
			nTimeOut);

		nVarID = 51;

		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 2);

		strcpy(stSetASignalInfo.szModifyBuf, "�����й���");

		stSetASignalInfo.byModifyType = MODIFY_SIGNAL_ENGLISH_FULL_NAME;
		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stSetASignalInfo,			
			nTimeOut);

		stSetASignalInfo.byModifyType = MODIFY_SIGNAL_LOCAL_FULL_NAME;
		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stSetASignalInfo,			
			nTimeOut);

		stSetASignalInfo.byModifyType = MODIFY_SIGNAL_ENGLISH_ABBR_NAME;
		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stSetASignalInfo,			
			nTimeOut);

		stSetASignalInfo.byModifyType = MODIFY_SIGNAL_LOCAL_ABBR_NAME;
		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stSetASignalInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK)
		{
			printf("Test SetASignalInfoStru() success\r\n");
		}
		else
		{
			printf("Test SetASignalInfoStru() failure\r\n");
		}
	}
	//Test GetASignalValue()
	{
		SIG_BASIC_VALUE* pSigValue;

		nInterfaceType = VAR_A_SIGNAL_VALUE;

		nVarID = 51;

		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 7);

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pSigValue,			
			nTimeOut);

		if (nError == ERR_DXI_OK
			&& pSigValue == &((g_SiteInfo.pEquipInfo->pSampleSigValue + 2)->bv))
		{
			printf("Test GetASignalValue() success\r\n");
		}
		else
		{
			printf("Test GetASignalValue() failure\r\n");
		}
	}

	//Test SetASignalValue()
	{
		//VAR_VALUE value;

		//nError = 0;
		//nBufLen = sizeof(VAR_VALUE);

		//nInterfaceType = VAR_A_SIGNAL_VALUE;

		//nVarID = 2;
		//nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 2);

		//value.fValue = 54.7;

		//nError += DxiSetData(nInterfaceType,
		//	nVarID,			
		//	nVarSubID,		
		//	nBufLen,			
		//	&value,			
		//	nTimeOut);
		VAR_VALUE_EX value;

		nError = 0;
		nBufLen = sizeof(VAR_VALUE_EX);

		nInterfaceType = VAR_A_SIGNAL_VALUE;

		nVarID = 2;
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 2);

		value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;

		value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		value.pszSenderName = "LUI";

		value.varValue.enumValue = 1;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&value,			
			nTimeOut);

		//����Խ��
		nVarID = 58;
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 7);

		value.ulValue = 1000;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&value,			
			nTimeOut);


		if (nError == ERR_DXI_OK + ERR_DXI_INVALID_DATA_VALUE)
		{
			printf("Test SetASignalValue() success\r\n");
		}
		else
		{
			printf("Test SetASignalValue() failure\r\n");
		}
	}
	//Test GetACUPublicConfig()
	{
		SITE_INFO* pSiteInfo;
		LANG_TEXT* pLangInfo;

		PORT_INFO* pPortInfo;

		DWORD	   dwGetData;

		int		   nSiteID;

		char	   szGetStr[128];

		char*		pLocalLangCode;


		nError = 0;
		nInterfaceType = VAR_ACU_PUBLIC_CONFIG;

		nVarID = SITE_INFO_POINTER;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pSiteInfo,			
			nTimeOut);

		nVarID = SITE_PORT_INFO;
		nVarSubID = 1;
		// note: use nVarSubID as index (iPortID)

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pPortInfo,			
			nTimeOut);

		nVarID = SITE_NAME;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pLangInfo,			
			nTimeOut);

		nVarID = SITE_SW_VERSION;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&dwGetData,			
			nTimeOut);

		nVarID = SITE_ID;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nSiteID,			
			nTimeOut);

		nVarID = ACU_MANUFACTURER;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			szGetStr,			
			nTimeOut);

		nVarID = LOCAL_LANGUAGE_CODE;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pLocalLangCode,			
			nTimeOut);

		nVarID = SITE_LOCATION;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pLangInfo,			
			nTimeOut);


		nVarID = SITE_DESCRIPTION;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pLangInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK)
		{
			printf("Test GetACUPublicConfig() success\r\n");
		}
		else
		{
			printf("Test GetACUPublicConfig() failure\r\n");
		}

	}
	//Test SetACUPublicConfig()
	{
		char* pStr;

		DWORD dwVersion = 158;
		int   nSiteID   = 0x3210;

		nError = 0;

		nInterfaceType = VAR_ACU_PUBLIC_CONFIG;

		nVarID = SITE_NAME;
		nVarSubID = MODIFY_SITE_ENGLISH_FULL_NAME;

		pStr = "ACU��Ŀ��1";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_NAME;
		nVarSubID = MODIFY_SITE_LOCAL_FULL_NAME;

		pStr = "ACU��Ŀ��2";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_NAME;
		nVarSubID = MODIFY_SITE_ENGLISH_ABBR_NAME;

		pStr = "ACU��Ŀ��3";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_NAME;
		nVarSubID = MODIFY_SITE_LOCAL_ABBR_NAME;

		pStr = "ACU��Ŀ��4";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_SW_VERSION;

		nBufLen = sizeof(dwVersion);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&dwVersion,			
			nTimeOut);

		nVarID = ACU_MANUFACTURER;
		pStr = "ë������";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_ID;

		nBufLen = sizeof(nSiteID);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&nSiteID,			
			nTimeOut);

		nVarID = SITE_LOCATION;
		nVarSubID = MODIFY_LOCATION_ENGLISH_FULL;

		pStr = "ACU team group 1";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_LOCATION;
		nVarSubID = MODIFY_LOCATION_LOCAL_FULL;

		pStr = "ACU��Ŀ��1";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_LOCATION;
		nVarSubID = MODIFY_LOCATION_ENGLISH_ABBR;

		pStr = "ACU team group 2";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_LOCATION;
		nVarSubID = MODIFY_LOCATION_LOCAL_ABBR;

		pStr = "ACU��Ŀ��2";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_DESCRIPTION;
		nVarSubID = MODIFY_DESCRIPTION_ENGLISH_FULL;

		pStr = "In developing phase";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_DESCRIPTION;
		nVarSubID = MODIFY_DESCRIPTION_LOCAL_FULL;

		pStr = "���ڿ�������";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_DESCRIPTION;
		nVarSubID = MODIFY_DESCRIPTION_ENGLISH_ABBR;

		pStr = "In developing";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);

		nVarID = SITE_DESCRIPTION;
		nVarSubID = MODIFY_DESCRIPTION_LOCAL_ABBR;

		pStr = "���ڿ���";
		nBufLen = strlen(pStr);

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			pStr,			
			nTimeOut);


		if (nError == ERR_DXI_OK)
		{
			printf("Test SetACUPublicConfig() success\r\n");
		}
		else
		{
			printf("Test SetACUPublicConfig() failure\r\n");
		}

	}
	//Test GetACUNetInfo()
	{
		struct in_addr* pIn;

		ACU_NET_INFO stACUNetInfo;
		int nAddress;

		nError = ERR_DXI_OK;

		nInterfaceType = VAR_ACU_NET_INFO;
		nVarID = NET_INFO_ALL;
		nBufLen = sizeof(ACU_NET_INFO);

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&stACUNetInfo,			
			nTimeOut);

		printf("Call GetACUNetInfo()\r\n");

		pIn = (struct in_addr*)(&(stACUNetInfo.ulIp));
		printf("IP        = %s\r\n",inet_ntoa(*pIn));

		pIn = (struct in_addr*)(&(stACUNetInfo.ulMask));
		printf("MASK      = %s\r\n",inet_ntoa(*pIn));

		pIn = (struct in_addr*)(&(stACUNetInfo.ulGateway));
		printf("GateWay   = %s\r\n",inet_ntoa(*pIn));

		pIn = (struct in_addr*)(&(stACUNetInfo.ulBroardcast));
		printf("BroadCast = %s\r\n",inet_ntoa(*pIn));

		/*printf("IP        = %s\r\n",inet_ntoa(*(((struct in_addr)*)&(stACUNetInfo.ulIp))));
		printf("MASK      = %s\r\n",inet_ntoa(*(((struct in_addr)*)&stACUNetInfo.ulMask)));
		printf("GateWay   = %s\r\n",inet_ntoa(*(((struct in_addr)*)&stACUNetInfo.ulGateway)));
		printf("BroadCast = %s\r\n",inet_ntoa(*(((struct in_addr)*)&stACUNetInfo.ulBroardcast)));*/


		for(nVarID = NET_INFO_IP;nVarID <= NET_INFO_BROADCAST;nVarID++)
		{
			nError += DxiGetData(nInterfaceType,
				nVarID,			
				nVarSubID,		
				&nBufLen,			
				&nAddress,			
				nTimeOut);

			printf("Address = %d\r\n", nAddress);
		}

		if (nError == ERR_DXI_OK)
		{
			printf("Test GetACUNetInfo() success\r\n");
		}
		else
		{
			printf("Test GetACUNetInfo() failure\r\n");
		}

	}

	//Test SetACUNetInfo()
	{
		ACU_NET_INFO stACUNetInfo;

		printf("ACU network info set start!�����أ�\r\n");
		printf("please press ENTER key!\r\n");

		getchar();

		nInterfaceType = VAR_ACU_NET_INFO;
		nVarID = NET_INFO_ALL;
		nBufLen = sizeof(ACU_NET_INFO);

		stACUNetInfo.ulIp = inet_addr("142.100.6.41");
		stACUNetInfo.ulMask = inet_addr("255.255.254.0");;
		stACUNetInfo.ulGateway = inet_addr("142.100.6.1");
		stACUNetInfo.ulBroardcast = inet_addr("142.100.7.255");

		nError = DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stACUNetInfo,			
			nTimeOut);

		printf("Test SetACUNetInfo() success\r\n");
	}
	//Test GetTimeSrv()
	{
		TIME_SRV_INFO* pTimeSvrInfo;
		int nGetData;

		time_t* pTime;;

		nInterfaceType = VAR_TIME_SERVER_INFO;

		nVarID = TIME_SRV_CONFIG;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pTimeSvrInfo,			
			nTimeOut);

		if (nError == 0
			&& pTimeSvrInfo == &g_sTimeSrvInfo)
		{
			printf("Get time server config success\r\n\r\n");
		}
		else
		{
			printf("Get time server config failure \r\n");
		}

		nVarID = TIME_SRV_IP;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nGetData,			
			nTimeOut);

		nVarID = TIME_SRV_BACKUP_IP;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nGetData,			
			nTimeOut);

		nVarID = TIME_SYNC_INTERVAL;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nGetData,			
			nTimeOut);

		nVarID = SYSTEM_TIME;

		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nGetData,			
			nTimeOut);

		pTime = (time_t*)(&nGetData);
		printf("NOW is %s ---time = %d \r\n", ctime(pTime), nGetData);

		if (nError == ERR_DXI_OK)
		{
			printf("Test GetTimeSrv() success\r\n");
		}
		else
		{
			printf("Test GetTimeSrv() failure\r\n");
		}


	}
	//Test SetTimeSrv()
	{
		int nSetData;

		nError = ERR_DXI_OK;

		nInterfaceType = VAR_TIME_SERVER_INFO;

		nVarID = TIME_SRV_IP;

		nBufLen = sizeof(unsigned long);

		nSetData = 0x765;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&nSetData,			
			nTimeOut);

		nVarID = TIME_SRV_BACKUP_IP;

		nBufLen = sizeof(unsigned long);

		nSetData = 0x654;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&nSetData,			
			nTimeOut);

		nVarID = TIME_SYNC_INTERVAL;

		nBufLen = sizeof(WORD);

		nSetData = 0x543;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&nSetData,			
			nTimeOut);

		printf("ACU system time set start!�����أ�\r\n");
		printf("please press ENTER key!\r\n");

		getchar();

		nVarID = SYSTEM_TIME;

		nBufLen = sizeof(time_t);

		nSetData = 1096110544/*(int)time(NULL)*/;

		nError += DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&nSetData,			
			nTimeOut);

		if (nError == ERR_DXI_OK)
		{
			printf("Test SetTimeSrv() success\r\n");
		}
		else
		{
			printf("Test SetTimeSrv() failure\r\n");
		}

	}

	//Test GetSamplersNum()
	{
		int nSamplersNumRet;

		nInterfaceType = VAR_SAMPLERS_NUM;

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nSamplersNumRet,			
			nTimeOut);

		if (nError == ERR_DXI_OK && nSamplersNumRet == g_SiteInfo.iSamplerNum)
		{
			printf("Test GetSamplersNum() success\r\n");
		}
		else
		{
			printf("Test GetSamplersNum() failure nError = %d\r\n", nError);
		}
	}
	//Test GetSamplersList()
	{
		SAMPLER_INFO* pSamplersInfo;

		nInterfaceType = VAR_SAMPLERS_LIST;

		DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pSamplersInfo,			
			nTimeOut);

		if (nError == ERR_DXI_OK && pSamplersInfo == g_SiteInfo.pSamplerInfo)
		{
			printf("Test GetSamplersList() success\r\n");
		}
		else
		{
			printf("Test GetSamplersList() failure nError = %d\r\n", nError);
		}
	}
	
	//Test GetLCDKeyStatus
	{
		UCHAR byKeyValue;

		printf("Test GetLCDKeyStatus() start\r\n");

		nError = 0;
		nInterfaceType = VAR_LCDKEY_THRUYDN23;		
		
		nError += DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&byKeyValue,			
			nTimeOut);
		if (nError == ERR_DXI_OK)
		{
			printf("GetLCDKeyStatus = %c start\r\n",byKeyValue);
		}
		else
		{
			printf("Test GetLCDKeyStatus() failure nError = %d\r\n", nError);
		}
		
	}
	//Test SetLCDKeyStatus
	{
		
		printf("SetLCDKeyStatus start!\r\n");
		nInterfaceType = VAR_LCDKEY_THRUYDN23;
		nBufLen = sizeof(_KEY_INFO_);
		
		_KEY_INFO_   stTestKey
		stTestKey.byKeyValue = 0x01;
		stTestKey.nCount = 5;

		nError = DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stTestKey,			
			nTimeOut);

		if (nError == ERR_DXI_OK)
		{
			printf("SetLCDKeyStatus success\r\n");
		}
		else
		{
			printf("SetLCDKeyStatus fail\r\n");
		}
		
	}
	//Test GetAppDHCPInfo
	{
		int nDHCP;

		printf("Test GetAppDHCPInfo() start\r\n");

		nError = 0;
		nBufLen = sizeof(int);

		nError += DxiGetData(VAR_APP_DHCP_INFO,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&nDHCP,			
			nTimeOut);
		if (nError == ERR_DXI_OK)
		{
			printf("GetAppDHCPInfo = %d start\r\n",nDHCP);
		}
		else
		{
			printf("Test GetAppDHCPInfo() failure nError = %d\r\n", nError);
		}
		
	}
	//Test SetAppDHCPInfo
	{
		
		printf("SetAppDHCPInfo start!\r\n");
		
		nBufLen = sizeof(int);
		int nDHCP;
		
		nDHCP = APP_DHCP_ON;
 
		nError = DxiSetData(VAR_APP_DHCP_INFO,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&nDHCP,			
			nTimeOut);

		if (nError == ERR_DXI_OK)
		{
			printf("SetAppDHCPInfo success\r\n");
		}
		else
		{
			printf("SetAppDHCPInfo fail\r\n");
		}
		
	}

}

//Test trap call back
void TestTrapCallback(void)
{
	printf("Text trap call back function!");

	getchar();

	DxiRegisterNotification("Text thread", 
		ServiceNotification,
		&g_stParam, 							  
		(_ALARM_MASK | _CONFIG_MASK), 							  
		(HANDLE)0x10000010,							  
		_REGISTER_FLAG);

	getchar();

	printf("Register the same!");
	DxiRegisterNotification("Text thread", 
		ServiceNotification,
		&g_stParam, 							  
		_ALARM_MASK | _CONFIG_MASK, 							  
		(HANDLE)0x10000010,							  
		_REGISTER_FLAG);

	getchar();

	NotificationFunc(_ALARM_MASK,	
		6,				  
		"hello",					  
		TRUE);

	getchar();
	NotificationFunc(_CONFIG_MASK,	
		6,				  
		"hello",					  
		FALSE);

	getchar();
	NotificationFunc(_SET_MASK,	
		6,				  
		"hello",					  
		TRUE);

	getchar();
}


BOOL ConstructLangText(LANG_TEXT*	pLangText,
					   int			iMaxLenForFull,
					   int			iMaxLenForAbbr,
					   char*		pFullName0,
					   char*		pFullName1,
					   char*		pAbbrName0,
					   char*		pAbbrName1)


{
	char* pUseString;


	if (pLangText)
	{
		//	pLangText->iResourceID = 20;
		pLangText->iMaxLenForFull = iMaxLenForFull;	
		pLangText->iMaxLenForAbbr = iMaxLenForAbbr;	

		pUseString = NEW(char, pLangText->iMaxLenForFull);
		strncpyz(pUseString, pFullName0, pLangText->iMaxLenForFull);
		pLangText->pFullName[0] = pUseString;	

		pUseString = NEW(char, pLangText->iMaxLenForFull);
		strncpyz(pUseString, pFullName1, pLangText->iMaxLenForFull);
		pLangText->pFullName[1] = pUseString;

		pUseString = NEW(char, pLangText->iMaxLenForAbbr);
		strncpyz(pUseString, pAbbrName0, pLangText->iMaxLenForAbbr);
		pLangText->pAbbrName[0] = pUseString;	

		pUseString = NEW(char, pLangText->iMaxLenForAbbr);
		strncpyz(pUseString, pAbbrName1, pLangText->iMaxLenForAbbr);
		pLangText->pAbbrName[1] = pUseString;
	}

	return TRUE;
}

//Init site info
BOOL InitSiteInfo(void)
{
#ifdef _DEBUG
	printf("filename: test_data_exchange.c [InitSiteInfo]\n\r");
#endif 	/*_DEBUG	*/

	g_SiteInfo.LangInfo.szLocaleLangCode = "zh";

	g_SiteInfo.iSiteID = 15;

	ConstructLangText(&(g_SiteInfo.langSiteName),
		32,
		16,
		"Advanced controller unit team",
		"Advanced controller unit ��Ŀ��",
		"ACU team",
		"ACU ��Ŀ��");

	ConstructLangText(&(g_SiteInfo.langSiteLocation),
		64,
		32,
		"On the dept. of monitor",
		"����¥��ز�",
		"On the dept. of monitor",
		"����¥��ز�");

	ConstructLangText(&(g_SiteInfo.langDescription),
		64,
		32,
		"Has strong function",
		"ACU���ܺ�ǿ���",
		"Has strong function",
		"ACU���ܺ�ǿ���");


	{
		//four standard equip types

		STDEQUIP_TYPE_INFO*	pStdEquipTypeInfo;	

		SAMPLE_SIG_INFO	*pSampleSigInfo;
		CTRL_SIG_INFO	*pCtrlSigInfo;
		SET_SIG_INFO	*pSetSigInfo;
		ALARM_SIG_INFO	*pAlarmSigInfo;

		g_SiteInfo.iStdEquipTypeNum = 5;	

		pStdEquipTypeInfo = NEW(STDEQUIP_TYPE_INFO,g_SiteInfo.iStdEquipTypeNum);
		ZERO_POBJS(pStdEquipTypeInfo, g_SiteInfo.iStdEquipTypeNum);

		g_SiteInfo.pStdEquipTypeInfo = pStdEquipTypeInfo;

		//�����The first equip is battery group
		pStdEquipTypeInfo->iTypeID = 3;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"Battery group",
			"������",
			"Bat grp",
			"�����");


		//battery equip has three sample signals
		pStdEquipTypeInfo->iSampleSigNum = 3;

		pSampleSigInfo = NEW(SAMPLE_SIG_INFO,pStdEquipTypeInfo->iSampleSigNum);
		ZERO_POBJS(pSampleSigInfo, pStdEquipTypeInfo->iSampleSigNum);

		pStdEquipTypeInfo->pSampleSigInfo = pSampleSigInfo;
		pSampleSigInfo->iSigID = 1;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery total current",
			"�����ܵ���",
			"Bat cur",
			"����ܵ���");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "6.0";
		pSampleSigInfo->szSigUnit = "A";


		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 100;
		pSampleSigInfo->fMaxValidValue = 1000;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 2;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery average voltage",
			"����ƽ����ѹ",
			"Bat avr vol",
			"��ؾ�ѹ");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "4.1";
		pSampleSigInfo->szSigUnit = "V";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_NONE;

		pSampleSigInfo->fMinValidValue = 43;
		pSampleSigInfo->fMaxValidValue = 58;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 7;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery temparature",
			"�����¶�",
			"Bat temp",
			"�����");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "5.1";
		pSampleSigInfo->szSigUnit = "degC";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = -40;
		pSampleSigInfo->fMaxValidValue = 80;

		//Batt group has one alarm signals
		pStdEquipTypeInfo->iAlarmSigNum = 1;

		pAlarmSigInfo = NEW(ALARM_SIG_INFO,pStdEquipTypeInfo->iAlarmSigNum);

		ZERO_POBJS(pAlarmSigInfo, pStdEquipTypeInfo->iAlarmSigNum);

		pStdEquipTypeInfo->pAlarmSigInfo = pAlarmSigInfo;

		pAlarmSigInfo->iSigID = 1;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"Battery temperature hight",
			"��س���",
			"Hight bat temp",
			"��س���");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_MAJOR;

		//ģ��The second equip is rectifier
		pStdEquipTypeInfo++;

		pStdEquipTypeInfo->iTypeID = 2;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"Rectifier group",
			"����ģ��",
			"Rect grp",
			"ģ��");

		//Rectifier equip has two control signals
		pStdEquipTypeInfo->iCtrlSigNum = 2;

		pCtrlSigInfo = NEW(CTRL_SIG_INFO,pStdEquipTypeInfo->iCtrlSigNum);

		ZERO_POBJS(pCtrlSigInfo, pStdEquipTypeInfo->iCtrlSigNum);

		pStdEquipTypeInfo->pCtrlSigInfo = pCtrlSigInfo;

		pCtrlSigInfo->iSigID = 1;

		pCtrlSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pCtrlSigInfo->pSigName,
			32,
			16,
			"Rectifier current limit control",
			"����ģ�����������",
			"curr lmt ctrl",
			"ģ������");


		pCtrlSigInfo->fMinValidValue = 0;
		pCtrlSigInfo->fMaxValidValue = 110;

		pCtrlSigInfo->iSigValueType = VAR_UNSIGNED_LONG;
		pCtrlSigInfo->szValueDisplayFmt = "2";
		pCtrlSigInfo->szSigUnit = "%";

		pCtrlSigInfo->fControlStep = 2;

		pCtrlSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pCtrlSigInfo->byAuthorityLevel = ADMIN_LEVEL;

		pCtrlSigInfo++;

		pCtrlSigInfo->iSigID = 2;

		pCtrlSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pCtrlSigInfo->pSigName,
			32,
			16,
			"Rectifier voltage control",
			"����ģ���ѹ����",
			"Rect vol ctrl",
			"ģ���ѹ");

		pCtrlSigInfo->iSigValueType = VAR_FLOAT;

		pCtrlSigInfo->fMinValidValue = 45;
		pCtrlSigInfo->fMaxValidValue = 58;

		pCtrlSigInfo->szValueDisplayFmt = ".1";
		pCtrlSigInfo->szSigUnit = "V";

		pCtrlSigInfo->fControlStep = 0.1;

		pCtrlSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pCtrlSigInfo->byAuthorityLevel = OPERATOR_LEVEL;

		//alarm signals
		//Rectifier equip has two alarm signals
		pStdEquipTypeInfo->iAlarmSigNum = 2;

		pAlarmSigInfo = NEW(ALARM_SIG_INFO,pStdEquipTypeInfo->iAlarmSigNum);

		ZERO_POBJS(pAlarmSigInfo, pStdEquipTypeInfo->iAlarmSigNum);

		pStdEquipTypeInfo->pAlarmSigInfo = pAlarmSigInfo;

		pAlarmSigInfo->iSigID = 1;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"Rectifier AC failure",
			"ģ�齻��ͣ��",
			"AC failure",
			"����ͣ��");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_CRITICAL;

		pAlarmSigInfo++;

		pAlarmSigInfo->iSigID = 2;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"Multiple Rectifier Failure",
			"˫(��)ģ�����",
			"Rect Failure",
			"ģ�����");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_MAJOR;

		//����������The third equip is general controller

		pStdEquipTypeInfo++;

		pStdEquipTypeInfo->iTypeID = 7;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"ACU base controller",
			"ACU����������",
			"Base controller",
			"����������");

		//GC equip has three set signals
		pStdEquipTypeInfo->iSetSigNum = 3;

		pSetSigInfo = NEW(SET_SIG_INFO,pStdEquipTypeInfo->iSetSigNum);

		ZERO_POBJS(pSetSigInfo, pStdEquipTypeInfo->iSetSigNum);

		pStdEquipTypeInfo->pSetSigInfo = pSetSigInfo;
		pSetSigInfo->iSigID = 1;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Constant Current Test Enabled",
			"��غ�����������",
			"Constant curr",
			"������������");

		pSetSigInfo->iSigValueType = VAR_ENUM;

		pSetSigInfo->iStateNum = 2;
		pSetSigInfo->pStateText = NEW(LANG_TEXT*, 2);
		pSetSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[0],
			32,
			16,
			"Test Enabled",
			"��������",
			"Enabled",
			"����");
		pSetSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[1],
			32,
			16,
			"Test Disabled",
			"���Բ�����",
			"Disable",
			"������");

		pSetSigInfo->szValueDisplayFmt = "8";

		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;
		

		pSetSigInfo->fMinValidValue = 0;
		pSetSigInfo->fMaxValidValue = 1;

		pSetSigInfo++;

		pSetSigInfo->iSigID = 2;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Planned Test Enabled",
			"��ض�ʱ��������",
			"Planned Test",
			"��ʱ��������");

		pSetSigInfo->iSigValueType = VAR_ENUM;

		pSetSigInfo->iStateNum = 2;
		pSetSigInfo->pStateText = NEW(LANG_TEXT*, 2);
		pSetSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[0],
			32,
			16,
			"Test Enabled",
			"��������",
			"Enabled",
			"����");
		pSetSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[1],
			32,
			16,
			"Test Disabled",
			"���Բ�����",
			"Disable",
			"������");

		pSetSigInfo->szValueDisplayFmt = "8";

		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;


		pSetSigInfo->fMinValidValue = 0;
		pSetSigInfo->fMaxValidValue = 1;

		pSetSigInfo++;

		pSetSigInfo->iSigID = 7;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Battery Test End Time",
			"��ز�����ֹʱ��",
			"Test end time",
			"������ֹʱ��");

		pSetSigInfo->iSigValueType = VAR_UNSIGNED_LONG;

		pSetSigInfo->szValueDisplayFmt = "5";
		pSetSigInfo->szSigUnit = "����";
		pSetSigInfo->fSettingStep = 9;
		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSetSigInfo->fMinValidValue = 60;
		pSetSigInfo->fMaxValidValue = 360;		

		//Base controller has one alarm signals
		pStdEquipTypeInfo->iAlarmSigNum = 1;

		pAlarmSigInfo = NEW(ALARM_SIG_INFO,pStdEquipTypeInfo->iAlarmSigNum);

		ZERO_POBJS(pAlarmSigInfo, pStdEquipTypeInfo->iAlarmSigNum);

		pStdEquipTypeInfo->pAlarmSigInfo = pAlarmSigInfo;

		pAlarmSigInfo->iSigID = 1;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"Battery test failure",
			"��ز���ʧ��",
			"Battest fail",
			"��ز���ʧ��");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_OBSERVATION;

		//���The fourth standard equip is battery
		pStdEquipTypeInfo++;

		pStdEquipTypeInfo->iTypeID = 4;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"Battery ",
			"���",
			"Battery",
			"���");


		//battery equip has five sample signals
		pStdEquipTypeInfo->iSampleSigNum = 5;

		pSampleSigInfo = NEW(SAMPLE_SIG_INFO,pStdEquipTypeInfo->iSampleSigNum);
		ZERO_POBJS(pSampleSigInfo, pStdEquipTypeInfo->iSampleSigNum);

		pStdEquipTypeInfo->pSampleSigInfo = pSampleSigInfo;

		pSampleSigInfo->iSigID = 1;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery  current",
			"��ص���",
			"Bat cur",
			"��ص���");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "6.0";
		pSampleSigInfo->szSigUnit = "A";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 100;
		pSampleSigInfo->fMaxValidValue = 1000;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 2;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery average voltage",
			"���ƽ����ѹ",
			"Bat avr vol",
			"��ؾ�ѹ");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "4.1";
		pSampleSigInfo->szSigUnit = "V";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 43;
		pSampleSigInfo->fMaxValidValue = 58;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 7;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery temparature",
			"����¶�",
			"Bat temp",
			"�����");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "5.1";
		pSampleSigInfo->szSigUnit = "degC";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = -40;
		pSampleSigInfo->fMaxValidValue = 80;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 20;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery  current",
			"��ؽڵ���",
			"Bat cell cur",
			"��ؽڵ���");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "3.1";
		pSampleSigInfo->szSigUnit = "AN";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 100;
		pSampleSigInfo->fMaxValidValue = 1000;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 30;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery average��cell voltage",
			"��ؽڵ�ѹ",
			"Bat avr cell vol",
			"��ؽ�ѹ");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "4.1";
		pSampleSigInfo->szSigUnit = "VOL";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 43;
		pSampleSigInfo->fMaxValidValue = 58;

		//Battery has one alarm signals
		pStdEquipTypeInfo->iAlarmSigNum = 1;

		pAlarmSigInfo = NEW(ALARM_SIG_INFO,pStdEquipTypeInfo->iAlarmSigNum);

		ZERO_POBJS(pAlarmSigInfo, pStdEquipTypeInfo->iAlarmSigNum);

		pStdEquipTypeInfo->pAlarmSigInfo = pAlarmSigInfo;

		pAlarmSigInfo->iSigID = 1;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"No battery ballance",
			"��ز�����",
			"No bat ballance",
			"��ز�����");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_CRITICAL;

		//ϵͳThe fifth standard equip is system stdEquip
		pStdEquipTypeInfo++;

		pStdEquipTypeInfo->iTypeID = 100;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"System group",
			"ϵͳ��",
			"System group",
			"ϵͳ��");


		//battery equip has five sample signals
		pStdEquipTypeInfo->iSampleSigNum = 4;

		pSampleSigInfo = NEW(SAMPLE_SIG_INFO,pStdEquipTypeInfo->iSampleSigNum);
		ZERO_POBJS(pSampleSigInfo, pStdEquipTypeInfo->iSampleSigNum);

		pStdEquipTypeInfo->pSampleSigInfo = pSampleSigInfo;

		pSampleSigInfo->iSigID = 1;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"System Voltage",
			"ϵͳ��ѹ",
			"System Voltage",
			"ϵͳ��ѹ");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = ".1";
		pSampleSigInfo->szSigUnit = "V";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 42;
		pSampleSigInfo->fMaxValidValue = 60;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 2;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"System Load",
			"ϵͳ����",
			"System Load",
			"ϵͳ����");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = ".0";
		pSampleSigInfo->szSigUnit = "A";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 50;
		pSampleSigInfo->fMaxValidValue = 6000;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 3;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"System Power",
			"ϵͳ����",
			"System Power",
			"ϵͳ����");

		pSampleSigInfo->iSigValueType = VAR_UNSIGNED_LONG;
		pSampleSigInfo->szValueDisplayFmt = "3";
		pSampleSigInfo->szSigUnit = "%";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 0;
		pSampleSigInfo->fMaxValidValue = 100;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 4;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"System status",
			"ϵͳ״̬",
			"Bat cell cur",
			"ϵͳ״̬");

		pSampleSigInfo->iSigValueType = VAR_ENUM;
		pSampleSigInfo->szValueDisplayFmt = "3";

pSampleSigInfo->iStateNum = 2;
		pSampleSigInfo->pStateText = NEW(LANG_TEXT*, 2);
		pSampleSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSampleSigInfo->pStateText[0],
			32,
			16,
			"No Alarm",
			"����",
			"No Alarm",
			"����");
		pSampleSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSampleSigInfo->pStateText[1],
			32,
			16,
			"�澯",
			"���Բ�����",
			"Alarm",
			"�澯");

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 0;
		pSampleSigInfo->fMaxValidValue = 1;

		pStdEquipTypeInfo->iSetSigNum = 2;

		pSetSigInfo = NEW(SET_SIG_INFO,pStdEquipTypeInfo->iSetSigNum);

		ZERO_POBJS(pSetSigInfo, pStdEquipTypeInfo->iSetSigNum);

		pStdEquipTypeInfo->pSetSigInfo = pSetSigInfo;
		pSetSigInfo->iSigID = 5;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Auto/Man State",
			"���Զ�״̬",
			"Auto/Man State",
			"���Զ�״̬");

		pSetSigInfo->iSigValueType = VAR_ENUM;

		pSetSigInfo->iStateNum = 2;
		pSetSigInfo->pStateText = NEW(LANG_TEXT*, 2);
		pSetSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[0],
			32,
			16,
			"Auto",
			"�Զ�",
			"Auto",
			"�Զ�");
		pSetSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[1],
			32,
			16,
			"Manual",
			"�ֶ�",
			"Manual",
			"�ֶ�");

		pSetSigInfo->szValueDisplayFmt = "4";

		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;
		

		pSetSigInfo->fMinValidValue = 0;
		pSetSigInfo->fMaxValidValue = 1;

		pSetSigInfo++;

		pSetSigInfo->iSigID = 6;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Battery Status",
			"��ع���״̬",
			"Battery Status",
			"��ع���״̬");

		pSetSigInfo->iSigValueType = VAR_ENUM;

		pSetSigInfo->iStateNum = 3;
		pSetSigInfo->pStateText = NEW(LANG_TEXT*, pSetSigInfo->iStateNum);
		pSetSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[0],
			32,
			16,
			"Float",
			"����",
			"Float",
			"����");
		pSetSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[1],
			32,
			16,
			"BC",
			"����",
			"BC",
			"����");

		pSetSigInfo->pStateText[2] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[2],
			32,
			16,
			"Test",
			"����",
			"Test",
			"����");

		pSetSigInfo->szValueDisplayFmt = "4";

		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;


		pSetSigInfo->fMinValidValue = 0;
		pSetSigInfo->fMaxValidValue = 2;
		
	}

	//Has six equip:battery,rectifier and general controller, two Batt, system
	{
		EQUIP_INFO				*pEquipInfo;	

		SAMPLE_SIG_VALUE	*pSampleSigValue;	 
		CTRL_SIG_VALUE		*pCtrlSigValue;	 
		SET_SIG_VALUE		*pSetSigValue;	 
		ALARM_SIG_VALUE		*pAlarmSigValue;	
		ALARM_SIG_VALUE		*pAlarmSigValue1;	

		g_SiteInfo.iEquipNum = 6;	

		pEquipInfo = NEW(EQUIP_INFO,g_SiteInfo.iEquipNum);

		ZERO_POBJS(pEquipInfo, g_SiteInfo.iEquipNum);


		g_SiteInfo.pEquipInfo = pEquipInfo;

		//�����
		pEquipInfo->iEquipID = 51;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"Battery group",
			"������",
			"Bat grp",
			"�����");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 0;

		pSampleSigValue = NEW(SAMPLE_SIG_VALUE,pEquipInfo->pStdEquip->iSampleSigNum);
		ZERO_POBJS(pSampleSigValue, pEquipInfo->pStdEquip->iSampleSigNum);

		pEquipInfo->pSampleSigValue = pSampleSigValue;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 0;

		pSampleSigValue->bv.varValue.fValue = 500;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 1;

		pSampleSigValue->bv.varValue.fValue = 52.7;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 2;

		pSampleSigValue->bv.varValue.fValue = 25;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		//active alarm
		//major alarm
		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2003 - 1900;
			startTime.tm_mon = 9 - 1;
			startTime.tm_mday = 27;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_MAJOR)->next = pAlarmSigValue;
		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		pAlarmSigValue->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_MAJOR) ;


		//ģ��
		pEquipInfo++;

		pEquipInfo->iEquipID = 2;

		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"Rectifier group",
			"����ģ��",
			"Rect grp",
			"ģ��");


		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 1;

		pCtrlSigValue = NEW(CTRL_SIG_VALUE,pEquipInfo->pStdEquip->iCtrlSigNum);
		ZERO_POBJS(pCtrlSigValue, pEquipInfo->pStdEquip->iCtrlSigNum);

		pEquipInfo->pCtrlSigValue = pCtrlSigValue;

		pCtrlSigValue->pStdSig = pEquipInfo->pStdEquip->pCtrlSigInfo + 0;

		pCtrlSigValue->bv.varValue.lValue = 90;

		pCtrlSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pCtrlSigValue++;

		pCtrlSigValue->pStdSig = pEquipInfo->pStdEquip->pCtrlSigInfo + 1;

		pCtrlSigValue->bv.varValue.fValue = 53.5;

		pCtrlSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		//active alarm

		//major alarm
		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;


			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2004 - 1900;
			
			startTime.tm_mon = 5 - 1;
			startTime.tm_mday = 9;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_MAJOR)->next = pAlarmSigValue;
		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 1;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		pAlarmSigValue->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_MAJOR) ;

		//critical alarm
		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2004 - 1900;
			startTime.tm_mon = 5 - 1;
			startTime.tm_mday = 8;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_CRITICAL)->next = pAlarmSigValue;


		//Obervation alarm
		pAlarmSigValue1 = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2003 - 1900;
			startTime.tm_mon = 06 - 1;
			startTime.tm_mday = 21;

			pAlarmSigValue1->sv.tmStartTime = mktime(&startTime);
		}

		pAlarmSigValue1->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue1->pEquipInfo = pEquipInfo;

		pAlarmSigValue->next = pAlarmSigValue1;

		pAlarmSigValue1->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_CRITICAL);


		//����������
		pEquipInfo++;

		pEquipInfo->iEquipID = 58;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,		
			32,
			16,
			"ACU base controller",
			"ACU����������",
			"Base controller",
			"����������");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 2;

		pSetSigValue = NEW(SET_SIG_VALUE,pEquipInfo->pStdEquip->iSetSigNum);
		ZERO_POBJS(pSetSigValue, pEquipInfo->pStdEquip->iSetSigNum);

		pEquipInfo->pSetSigValue = pSetSigValue;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 0;

		pSetSigValue->bv.varValue.enumValue = 1;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSetSigValue++;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 1;

		pSetSigValue->bv.varValue.enumValue = 1;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSetSigValue++;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 2;

		pSetSigValue->bv.varValue.ulValue = 320;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		//active alarm
		//observation alarm
		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2003 - 1900;
			startTime.tm_mon = 06 - 1;
			startTime.tm_mday = 19;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_OBSERVATION)->next = pAlarmSigValue;
		pAlarmSigValue->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_OBSERVATION) ;

		
		pEquipInfo++;
		
		//���1
		pEquipInfo->iEquipID = 100;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"Battery 1",
			"���1",
			"Bat 1",
			"���1");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 3;

		pSampleSigValue = NEW(SAMPLE_SIG_VALUE,pEquipInfo->pStdEquip->iSampleSigNum);
		ZERO_POBJS(pSampleSigValue, pEquipInfo->pStdEquip->iSampleSigNum);

		pEquipInfo->pSampleSigValue = pSampleSigValue;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 0;

		pSampleSigValue->bv.varValue.fValue = 500;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 1;

		pSampleSigValue->bv.varValue.fValue = 52.7;

		//��Ч֮
	//	pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 2;

		pSampleSigValue->bv.varValue.fValue = 25;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 3;

		pSampleSigValue->bv.varValue.fValue = 150;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 4;

		pSampleSigValue->bv.varValue.fValue = 53.7;

		//��Ч֮
	//	pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		//critical alarm

		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2003 - 1900;
			startTime.tm_mon = 9 - 1;
			startTime.tm_mday = 28;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_CRITICAL)->next = pAlarmSigValue;
		pAlarmSigValue->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_CRITICAL) ;

		pEquipInfo++;


		//���2
		pEquipInfo->iEquipID = 103;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"Battery 2",
			"���2",
			"Bat 2",
			"���2");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 3;

		pSampleSigValue = NEW(SAMPLE_SIG_VALUE,pEquipInfo->pStdEquip->iSampleSigNum);
		ZERO_POBJS(pSampleSigValue, pEquipInfo->pStdEquip->iSampleSigNum);

		pEquipInfo->pSampleSigValue = pSampleSigValue;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 0;

		pSampleSigValue->bv.varValue.fValue = 400;

		//��Ч֮
	//	pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 1;

		pSampleSigValue->bv.varValue.fValue = 53.0;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 2;

		pSampleSigValue->bv.varValue.fValue = 28;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 3;

		pSampleSigValue->bv.varValue.fValue = 250;

		//��Ч֮
	//	pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 4;

		pSampleSigValue->bv.varValue.fValue = 55.7;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pEquipInfo++;
		//ϵͳ�豸
		pEquipInfo->iEquipID = 1;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"ACU Site",
			"ACUվ",
			"ACU Site",
			"ACUվ");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 4;

		pSampleSigValue = NEW(SAMPLE_SIG_VALUE,pEquipInfo->pStdEquip->iSampleSigNum);
		ZERO_POBJS(pSampleSigValue, pEquipInfo->pStdEquip->iSampleSigNum);

		pEquipInfo->pSampleSigValue = pSampleSigValue;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 0;

		pSampleSigValue->bv.varValue.fValue = 53.5;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 1;

		pSampleSigValue->bv.varValue.fValue = 500;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 2;

		pSampleSigValue->bv.varValue.ulValue = 70;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 3;

		pSampleSigValue->bv.varValue.enumValue = 0;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSetSigValue = NEW(SET_SIG_VALUE,pEquipInfo->pStdEquip->iSetSigNum);
		ZERO_POBJS(pSetSigValue, pEquipInfo->pStdEquip->iSetSigNum);

		pEquipInfo->pSetSigValue = pSetSigValue;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 0;

		pSetSigValue->bv.varValue.enumValue = 1;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSetSigValue++;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 1;

		pSetSigValue->bv.varValue.enumValue = 2;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

	}

	//Active alarm count of site
	g_SiteInfo.iAlarmCount[ALARM_LEVEL_NONE] = 6;
	g_SiteInfo.iAlarmCount[ALARM_LEVEL_OBSERVATION] = 1;
	g_SiteInfo.iAlarmCount[ALARM_LEVEL_MAJOR] = 2;
	g_SiteInfo.iAlarmCount[ALARM_LEVEL_CRITICAL] = 3;

	return TRUE;
}

BOOL DestroySiteInfo(void)
{
	//���ǲ��Գ��򣬹�Ϊ���ٹ�������������������ڴ����ɾ��
	return TRUE;
}

BOOL InitTimeSvrInfo(void)
{
#ifdef _DEBUG
	printf("\n\rfilename: test_data_exchange.c [InitTimeSvrInfo]\r\n");
#endif 	/*_DEBUG	*/
	g_sTimeSrvInfo.ulBakTmSrvAddr = 0x789;
	g_sTimeSrvInfo.ulMainTmSrvAddr = 0x678;
	g_sTimeSrvInfo.dwJustTimeInterval = 80;

	return TRUE;
}

/*==========================================================================*
* FUNCTION : main
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int    argc    : 
*            INT char  *argv[] : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-20 21:38
*==========================================================================*/
int main(IN int argc, IN char *argv[])
{
	UNUSED(argc);

	printf("Test start!\r\n");

	InitSiteInfo();
	InitTimeSvrInfo();

	//At first init data exchange module
	InitDataExchangeModule();

	if (argv[1])
	{
		if (strcmp(argv[1], "-data") == 0)
		{
			TestDataExchange();
		}

		else if (strcmp(argv[1], "-trap") == 0)
		{
			TestTrapCallback();
		}
	}

	else
	{
		TestDataExchange();

		TestTrapCallback();

	}

	DestroySiteInfo();

	return 0;
}

/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : system_net_info.c
*  CREATOR  : HULONGWEN                DATE: 2004-09-17 10:43
*  VERSION  : V1.00
*  PURPOSE  : Get and set linux system net info
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/resource.h>		/* setrlimit */
#include <sys/types.h>			/* socket, bind, accept */
#include <sys/socket.h>			/* socket, bind, accept, setsockopt, */
#include <syslog.h>
#include <netdb.h>

#include <net/route.h>			/*rtentry*/
#include <net/if.h>				/*ifreq */

#include "system_net_info.h"

#define NETWORK_CONFIG_FILE_ETH0 "/home/app_script/init_eth"
#define NETWORK_CONFIG_FILE_ETH1 "/home/app_script/init_eth1"

//#define __SET_IP_THRU_IFCONFIG_CMD
#define SIOCGIFROUTEADDR	1
/*==========================================================================*
* FUNCTION : getEth1Info
* PURPOSE  : getEth1Info from file 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int     nGetCmd     : 
*            int     sockAddr      : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : SONG Xu                DATE: 2016-11-1 11:42
*==========================================================================*/
BOOL getEth1Info(int nGetCmd , ULONG	*sockAddr)
{
	char cIpAddr[20], cLine[128],cCmd[64];
	FILE *fp;
	char *pFind;
//printf("nGetCmd%d \n",nGetCmd);
	if(nGetCmd == SIOCGIFADDR)
	{
		snprintf(cCmd,64,"%s","/sbin/ifconfig eth1");
	}
	else if(nGetCmd == SIOCGIFNETMASK)
	{
		snprintf(cCmd,64,"%s","/sbin/ifconfig eth1 netmask");
	}
	else if(nGetCmd == SIOCGIFROUTEADDR)
	{
		snprintf(cCmd,64,"%s","/sbin/route add default gw");
	}
	else if(nGetCmd == SIOCGIFBRDADDR)
	{
		snprintf(cCmd,64,"%s","/sbin/route add default gw");
	}
	else
	{
		return FALSE;
	}
	
	if (! (fp = fopen (NETWORK_CONFIG_FILE_ETH1, "r")))
	{
		return FALSE;
	}
	while (fgets (cLine, sizeof(cLine), fp))
	{
		pFind = strstr(cLine, cCmd);
		if (pFind != NULL) 
		{
//			printf("%s length%d \n",cCmd,strlen(cCmd));
			pFind = pFind + strlen(cCmd) + 1;
			break;
		}
	}
	fclose(fp);	
	if(pFind == NULL)
	{
		return FALSE;
	}
//printf("cmd %d pFind%s\n",nGetCmd,pFind);
	snprintf(cIpAddr,20,"%s",pFind);
	*sockAddr = inet_addr(cIpAddr);

	return TRUE;
}

/*==========================================================================*
* FUNCTION : GetLanInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int     nIfNum     : 
*            int     nSock      : 
*            int     nGetCmd    : 
*            ULONG*  pulNewAddr : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-17 11:42
*==========================================================================*/
static int GetLanInfo(int nIfNum, int nSock, int nGetCmd, ULONG* pulNewAddr)
{
	struct ifreq		ifr;
	struct sockaddr_in	*p = (struct sockaddr_in *)&(ifr.ifr_addr);

	memset(&ifr,0,sizeof(struct ifreq));

	p->sin_family = AF_INET;
	/*ifr.ifr_ifno = nIfNum; */
	if (nIfNum == 0)
	{
		strcpy(ifr.ifr_name, "eth0");
	}
	else if (nIfNum == 1)
	{
		strcpy(ifr.ifr_name, "eth1");
	}
	else if (nIfNum == 2)
	{
		strcpy(ifr.ifr_name, "eth2");
	}

	/* get lan info */
	if (ioctl(nSock, (UINT)nGetCmd, &ifr) == -1)
	{
		if (nIfNum == 1)
		{
			if(getEth1Info(nGetCmd, pulNewAddr))
				return 0;
		}
		else
			return errno;
	}
#ifdef _DEBUG_DHCP
	TRACEX("\n\r[GetLanInfo] -- set %08x, old %s,",
		nGetCmd, inet_ntoa(p->sin_addr.s_addr, NULL));
#endif 	/*_DEBUG_DHCP	*/

	*pulNewAddr = (ULONG)p->sin_addr.s_addr;	
	return 0;
}

/*==========================================================================*
* FUNCTION : GetIPV6Addr
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int				   nIfNum     : 
*            struct in6_ifreq*     pIPV6LocalAddr    : 
*            struct in6_ifreq*     pIPV6GlobalAddr : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Jimmy Wu                DATE: 2014 8 11
*==========================================================================*/
struct in6_procinfo 
{
	IN6_ADDR _ip6_addr;
	int		_ip6_if_index;
	int		_ip6_prefix_len;
	int		_ip6_scope_type;
	int		_ip6_if_flag;
	char	_str_dev_name[32];
};
static void RegularAIPV6Addr(IN6_ADDR* pRawIPaddr)
{
	union MyValue
	{
		UINT32 ulVal;
		UINT8  ul8[4];
	} ;

	IN6_ADDR ipDst;
	union MyValue ulTmp;
	UINT8 u8Tmp;
	int i = 0;

	for(i = 0; i < 4; i ++)
	{
		ulTmp.ulVal = pRawIPaddr->S6_addr32[i];
		u8Tmp = ulTmp.ul8[0];
		ulTmp.ul8[0] = ulTmp.ul8[3];
		ulTmp.ul8[3] = u8Tmp;

		u8Tmp = ulTmp.ul8[1];
		ulTmp.ul8[1] = ulTmp.ul8[2];
		ulTmp.ul8[2] = u8Tmp;
		pRawIPaddr->S6_addr32[i] = ulTmp.ulVal;

	}
}
static int GetIPV6Addr(int nWhichNetCard, OUT IN6_IFREQ* pIPV6LocalAddr, OUT IN6_IFREQ* pIPV6GlobalAddr)
{
	FILE *in;
	struct in6_procinfo myNetProcInfo;
	char line[256];

	if (! (in = fopen ("/proc/net/if_inet6", "r")))
	{
		TRACEX("cannot open /proc/net/if_inet6 - burps\n\r");
		return RET_OPEN_FILE_ERROR;
	}

	while (fgets (line, sizeof(line), in))
	{

		if (9 != sscanf (line, "%08x%08x%08x%08x %02x %02x %02x %02x %s\n",
			&(myNetProcInfo._ip6_addr.S6_addr32[0]),
			&(myNetProcInfo._ip6_addr.S6_addr32[1]),
			&(myNetProcInfo._ip6_addr.S6_addr32[2]),
			&(myNetProcInfo._ip6_addr.S6_addr32[3]),
			&(myNetProcInfo._ip6_if_index),
			&(myNetProcInfo._ip6_prefix_len),
			&(myNetProcInfo._ip6_scope_type),
			&(myNetProcInfo._ip6_if_flag),
			myNetProcInfo._str_dev_name))
		{
			continue;
		}

		RegularAIPV6Addr(&myNetProcInfo._ip6_addr);
		//At least support three net adapter
		if (((!strcmp (myNetProcInfo._str_dev_name, "eth0")) && nWhichNetCard == 0) 
			|| ((!strcmp (myNetProcInfo._str_dev_name, "eth1")) && nWhichNetCard == 1)
			/*|| ((!strcmp (myNetProcInfo._str_dev_name, "eth2")) && nWhichNetCard == 2)*/)
		{
			if (myNetProcInfo._ip6_scope_type == 0x20)//local addr
			{
				pIPV6LocalAddr->ifr6_ifindex = myNetProcInfo._ip6_if_index;
				pIPV6LocalAddr->ifr6_prefixlen = myNetProcInfo._ip6_prefix_len;
				memcpy(&(pIPV6LocalAddr->ifr6_addr),
					&(myNetProcInfo._ip6_addr),
					sizeof(struct in6_addr));
			}			
			else if(myNetProcInfo._ip6_scope_type != 0x10)//��������ͳһ���global//if (myNetProcInfo._ip6_scope_type == 0x00)//global addr
			{
				pIPV6GlobalAddr->ifr6_ifindex = myNetProcInfo._ip6_if_index;
				pIPV6GlobalAddr->ifr6_prefixlen = myNetProcInfo._ip6_prefix_len;
				memcpy(&(pIPV6GlobalAddr->ifr6_addr),
					&(myNetProcInfo._ip6_addr),
					sizeof(struct in6_addr));
			}
		}

	}
	fclose(in);
	return RET_SUCCESS;
}
/*==========================================================================*
* FUNCTION : GetIPV6GateWay
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int     nIfNum     : 
*            int     nSock      : 
*            int     nGetCmd    : 
*            ULONG*  pulNewAddr : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Jimmy Wu                DATE: 2014 8 12
*==========================================================================*/
static int GetIPV6GateWay(int nWhichNetCard, OUT IN6_ADDR* pIPV6GW)
{
	FILE *stream;
	char buf[256];
	char strIP[128];
	char szSysCmd[200];
	memset(buf,0,256);
	memset(strIP,0,128);
	
	if(nWhichNetCard == 0)
	{
		snprintf(szSysCmd, 200, "%s", "ip -6 route show dev eth0 | grep default | grep via");
	}
	else
	{
		snprintf(szSysCmd, 200, "%s", "ip -6 route show dev eth1 | grep default | grep via");
	}

	if (! (stream = popen (szSysCmd, "r")))
	{
		return RET_OPEN_FILE_ERROR;
	}

	int iLen = fread(buf, sizeof(char), sizeof(buf),stream);
	int i = 0;
	char *p = strstr(buf,"via");
	char *q = strstr(buf,"metric");

	if(p && (q - p > 5))
	{
		strncpyz(strIP, p + 4, sizeof(char)*(q - p - 5));
	}
	inet_pton(AF_INET6, strIP, pIPV6GW);

	pclose(stream);
	return RET_SUCCESS;
}

/*==========================================================================*
* FUNCTION : GetDefaultGateway
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int     nIfNum           : 
*            ULONG*  pulDefultGateway : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-17 20:57
*==========================================================================*/
static int GetDefaultGateway(int nIfNum, ULONG* pulDefultGateway)
{
	//by HULONGWEN add temporary
	FILE *in;
	struct rtentry rtent;

	char line [256];

	char name[16];

	int rt_flags;
	int refcnt;
	unsigned int use;
	int iGatewayIB4 = 0;
	if (! (in = fopen ("/proc/net/route", "r")))
	{
		TRACEX("cannot open /proc/net/route - burps\n\r");

		return RET_OPEN_FILE_ERROR;
	}

	while (fgets (line, sizeof(line), in))
	{

		/*
		* as with 1.99.14:
		* Iface Dest GW Flags RefCnt Use Metric Mask MTU Win IRTT
		* eth0 0A0A0A0A 00000000 05 0 0 0 FFFFFFFF 1500 0 0 
		*/
		if (8 != sscanf (line, "%s %x %x %x %u %d %d %x %*d %*d %*d\n",
			/*rt->rt_dev*/name,
			&(((struct sockaddr_in *) &(rtent.rt_dst))->sin_addr.s_addr),
			&(((struct sockaddr_in *) &(rtent.rt_gateway))->sin_addr.s_addr),
			/* XXX: fix type of the args */
			&(rt_flags), &refcnt, &use, ((int *)&(rtent.rt_metric)),
			&(((struct sockaddr_in *) &(rtent.rt_genmask))->sin_addr.s_addr)))
			continue;


		//At least support three net adapter
		if (((!strcmp (name, "eth0")) && nIfNum == 0) 
			|| ((!strcmp (name, "eth1")) && nIfNum == 1)
			|| ((!strcmp (name, "eth2")) && nIfNum == 2))
		{
			if ((rt_flags & RTF_GATEWAY) && (rt_flags & RTF_UP))
			{
				*pulDefultGateway = ((struct sockaddr_in *) 
					&(rtent.rt_gateway))->sin_addr.s_addr;

				fclose(in);
				iGatewayIB4 = 1;
				return RET_SUCCESS;
			}
		}

	}
	if(iGatewayIB4 == 0 && nIfNum == 1)
	{
		if(getEth1Info(SIOCGIFROUTEADDR, pulDefultGateway))
			return 0;
	}

	fclose(in);

	return /*RET_NOT_FIND*/RET_SUCCESS;
}

/*==========================================================================*
* FUNCTION : SetLanInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    nIfNum    : 
*            int    nSock     : 
*            int    nGetCmd   : 
*            int    nSetCmd   : 
*            ULONG  ulNewAddr : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-17 11:18
*==========================================================================*/
static int SetLanInfo(int nIfNum, int nSock, int nGetCmd, 
					  int nSetCmd, ULONG ulNewAddr)
{
	struct ifreq		ifr;
	struct sockaddr_in	*p = (struct sockaddr_in *)&(ifr.ifr_addr);

#ifdef __SET_IP_THRU_IFCONFIG_CMD
	char szCmd[128];
#endif

	memset(&ifr,0,sizeof(struct ifreq));

	p->sin_family = AF_INET;
	/*ifr.ifr_ifno = nIfNum; */
	if (nIfNum == 0)
	{
		strcpy(ifr.ifr_name, "eth0");
	}
	else if (nIfNum == 1)
	{
		strcpy(ifr.ifr_name, "eth1");
	}
	else if (nIfNum == 2)
	{
		strcpy(ifr.ifr_name, "eth2");
	}

	/* get lan info */
	if (ioctl(nSock, (UINT)nGetCmd, &ifr) == -1)
		return errno;

#ifdef _DEBUG_DHCP
	/*TRACEX("\n\r[SetLanInfo] -- set %08x, old %s,",
		nSetCmd, inet_ntoa(p->sin_addr.s_addr, NULL));*/
#endif 	/*_DEBUG_DHCP	*/

	if ((ULONG)p->sin_addr.s_addr == ulNewAddr)
	{
		/* not changed, not need set */
		return 0;
	}

#ifndef __SET_IP_THRU_IFCONFIG_CMD

	/* setting address */
	p->sin_addr.s_addr = ulNewAddr;
	if ((int)ioctl(nSock, (UINT)nSetCmd, &ifr) == -1)
	{
		return errno;
	}

#else

	if (SIOCSIFADDR == nSetCmd)
	{
		snprintf(szCmd, sizeof(szCmd), "/sbin/ifconfig %s %s",///sbin/ifconfig eth0 *.*.*.*
			ifr.ifr_name,
			inet_ntoa(*((struct in_addr*)(&ulNewAddr))));
	}
	else if (SIOCSIFNETMASK == nSetCmd)
	{
		snprintf(szCmd, sizeof(szCmd), "/sbin/ifconfig %s netmask %s",
			ifr.ifr_name,
			inet_ntoa(*((struct in_addr*)(&ulNewAddr))));
	}
	else
	{
		snprintf(szCmd, sizeof(szCmd), "");
	}

	//system(szCmd);
	_SYSTEM(szCmd);

#endif


#ifdef _DEBUG_DHCP
	/*TRACEX("set to %s.", inet_ntoa(p->sin_addr.s_addr, NULL));*/
	fflush(stdout);
#endif 	/*_DEBUG_DHCP	*/

	return 0;
}

/*=====================================================================*
* Function name: SetLocalRoute
* Description  : 
* Arguments    : int dhcpSocket	: 
*                int bAddOrDelete	: 
*                ULONG ulIp	: 
*                ULONG ulGateway	: 
*                ULONG ulMask	: 
* Return type  : int 
*
* Create       : Mao Fuhua    202001-6-29 10:29:04
* Comment(s)   : 
*--------------------------------------------------------------------*/
//static int SetLocalRoute(int nIfNum, int dhcpSocket, int bAddOrDelete,
//						 ULONG ulIp, ULONG ulGateway, ULONG ulMask)
//{
//	struct sockaddr_in	*p;
//	struct rtentry	rtent;
//
//	UNUSED(ulIp);
//	UNUSED(ulMask);
//
//	/* 4. setting local route - not needed on later kernels  */
//	memset(&rtent,0,sizeof(struct rtentry));
//	/*rtent.rt_ifno = nIfNum;*/
//	if (nIfNum == 0)
//	{
//		rtent.rt_dev = "eth0";
//	}
//	else if (nIfNum == 1)
//	{
//		rtent.rt_dev = "eth1";
//	}
//	else if (nIfNum == 2)
//	{
//		rtent.rt_dev = "eth2";
//	}
//	/*rtent.rt_flags     |= RTF_INTF;*/
//	rtent.rt_flags |= RTF_UP;
//
//
//	//by HULONGWEN maybe need use socket(AF_INET, SOCK_RAW, 0);
//
//#if 0
//	p			=	(struct sockaddr_in *)&rtent.rt_dst;
//	p->sin_family		=	AF_INET;
//	p->sin_addr.s_addr  =   *(ULONG*)pOptions->val[subnetMask];
//	p->sin_addr.s_addr	&=	ulIp;
//	rtent.rt_flags      |=	RTF_UP;
//#endif
//
//	p			=	(struct sockaddr_in *)&rtent.rt_gateway;
//	p->sin_family		=	AF_INET;
//	p->sin_addr.s_addr  =   ulGateway;//by HULONGWEN maybe need use htonl()
//	rtent.rt_flags     |= RTF_GATEWAY;
//
//#if 0
//	p			=	(struct sockaddr_in *)&rtent.rt_netmask;
//	p->sin_family		=	AF_INET;
//	p->sin_addr.s_addr  =   ulMask;
//	rtent.rt_flags     |= RTF_MASK;
//#endif
//
//	if ((ioctl(dhcpSocket, 
//		(UINT)((bAddOrDelete != 0) ?  SIOCADDRT: SIOCDELRT),
//		&rtent) != 0) && (errno != EEXIST))
//	{
//		TRACEX("\n\r[SetLocalRoute] -- ioctl %s fail %x",
//			(bAddOrDelete != 0) ? "SIOCADDRT" : "SIOCDELRT", errno);
//		return -1;
//	}
//
//	return 0;
//}



/*==========================================================================*
* FUNCTION : GetNetworkInfo
* PURPOSE  : Get network info to linux system
* CALLS    : GetLanInfo,GetDefaultGateway
* CALLED BY: GetACUNetInfo
* ARGUMENTS: int    nIfNum         : the index of net adapter
*            int    bGetIp         : Get ip?  --1 is get
*            ULONG  *pulIp         : to return ip address
*            int    bGetMask       : get sub net mask? --1 is get
*            ULONG  *pulMask       : to return net mask
*            int    bGetGateway    : get default gateway? --1 is get
*            ULONG  *pulGateway    : to return default gateway
*            int    bBroardcast    : get broardcast address? --1 is get
*            ULONG  *pulBroardcast : to return broardcast address
* RETURN   : int : -1(create socket fail)
*					0 (get successfully)
*					1 (get fail)
*					2 (no item to get)
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-17 11:34
*==========================================================================*/
int GetNetworkInfo(int nIfNum, 
				   int bGetIp,		ULONG *pulIp,
				   int bGetMask,	ULONG *pulMask,
				   int bGetGateway,	ULONG *pulGateway, 
				   int bBroardcast,	ULONG *pulBroardcast)
{
	int dhcpSocket;	/* socket */
	int rc = 0;
	int iGetGateway;

	if ((!bGetIp) && (!bGetMask) 
		&& (!bGetGateway) && (!bBroardcast))
	{
		return 2;
	}

	dhcpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (dhcpSocket == -1)
	{
		TRACEX("\n\r[SetNetworkInfo] -- create socket fail %x", errno);
		return -1;
	}


	if (bGetIp)
	{
		*pulIp = 0;//if null, return 0;

		/* 1, Getting IP address */
		if (GetLanInfo(nIfNum, dhcpSocket, SIOCGIFADDR, pulIp) != 0)
		{
			TRACEX("\n\r[GetNetworkInfo] -- ioctl SIOCGIFADDR fail %x",
				errno);
			rc ++;

			close(dhcpSocket);
			return rc;
		}
	}

	if (bGetMask)
	{
		*pulMask = 0;//if null, return 0;

		/* 2. Getting netmask */
		if (GetLanInfo(nIfNum, dhcpSocket, SIOCGIFNETMASK, pulMask) != 0)
		{
			TRACEX("\n\r[GetNetworkInfo] -- ioctl SIOCGIFNETMASK fail %x", 
				errno);
			rc ++;

			close(dhcpSocket);
			return rc;
		}
	}

	if (bBroardcast)
	{
		*pulBroardcast = 0;//if null, return 0;

		/* 3. Getting broadcast address */
		if (GetLanInfo(nIfNum, dhcpSocket, SIOCGIFBRDADDR, pulBroardcast) 
			!= 0)
		{
			TRACEX("\n\r[GetNetworkInfo] -- ioctl SIOCGIFBRDADDR fail %x", 
				errno);
			rc ++;

			close(dhcpSocket);
			return rc;
		}
	}


	if (bGetGateway)
	{
		*pulGateway = 0;//if null, return 0;

		/* 4. Getting default gateway */
		iGetGateway =  GetDefaultGateway(nIfNum, pulGateway);
//		printf("iGetGateway%d \n",iGetGateway);
		if (iGetGateway == RET_OPEN_FILE_ERROR)
		{
		//	TRACEX("\n\r[GetNetworkInfo] -- Get default gateway fail ");
			rc ++;

			close(dhcpSocket);
			return rc;
		}
	}

	close(dhcpSocket);

	return rc;
}


static void SaveToNetworkCfgFile(int nIfNum)
{
	ULONG ulIP, ulNetmask, ulGateway, ulBroadcast;

	char szCmd[128];
	char ifrname[16];
	char net_config_file[128];
	
	//20130805 lkf �����������������������ļ�һ��д
	/*
	if (nIfNum == 1)
	{
	    sprintf(ifrname, "%s",  "eth1");
	    sprintf(net_config_file, "%s", NETWORK_CONFIG_FILE_ETH1);
	}
	else
	{
	    sprintf(ifrname, "%s", "eth0");
	    sprintf(net_config_file, "%s",  NETWORK_CONFIG_FILE_ETH0);
	}*/
	sprintf(ifrname, "%s", "eth0");
	sprintf(net_config_file, "%s",  NETWORK_CONFIG_FILE_ETH0);


	//Do not judge the ret value is 0;
	GetNetworkInfo(nIfNum,
		TRUE, &ulIP,
		TRUE, &ulNetmask,
		TRUE, &ulGateway,
		FALSE, &ulBroadcast);//Do not get Broardcast

	//��1���ļ�

	//Clear the content of the file
	snprintf(szCmd, sizeof(szCmd), "> %s", net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//Write #!/bin/sh
	snprintf(szCmd, sizeof(szCmd), "echo \"#!/bin/sh\" >> %s", net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//Write /sbin/ifconfig eth0 xxx.xxx.xxx.xxx ->IP addr
	snprintf(szCmd, sizeof(szCmd), "echo \"/sbin/ifconfig %s %s\" >> %s", ifrname,
		inet_ntoa(*((struct in_addr*)(&ulIP))),
		net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//Write /sbin/ifconfig eth0 xxx.xxx.xxx.xxx ->Netmask
	snprintf(szCmd, sizeof(szCmd), "echo \"/sbin/ifconfig %s netmask %s\" >> %s", ifrname,
		inet_ntoa(*((struct in_addr*)(&ulNetmask))),
		net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//Write route add default gw xxx.xxx.xxx.xxx ->Netmask
	snprintf(szCmd, sizeof(szCmd), "echo \"/sbin/route add default gw %s %s\" >> %s", 
		inet_ntoa(*((struct in_addr*)(&ulGateway))), ifrname,
		net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//��2���ļ�

	sprintf(ifrname, "%s", "eth1");
	sprintf(net_config_file, "%s",  NETWORK_CONFIG_FILE_ETH1);

	//Clear the content of the file
	snprintf(szCmd, sizeof(szCmd), "> %s", net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//Write #!/bin/sh
	snprintf(szCmd, sizeof(szCmd), "echo \"#!/bin/sh\" >> %s", net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//Write /sbin/ifconfig eth0 xxx.xxx.xxx.xxx ->IP addr
	snprintf(szCmd, sizeof(szCmd), "echo \"/sbin/ifconfig %s %s\" >> %s", ifrname,
	    inet_ntoa(*((struct in_addr*)(&ulIP))),
	    net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//Write /sbin/ifconfig eth0 xxx.xxx.xxx.xxx ->Netmask
	snprintf(szCmd, sizeof(szCmd), "echo \"/sbin/ifconfig %s netmask %s\" >> %s", ifrname,
	    inet_ntoa(*((struct in_addr*)(&ulNetmask))),
	    net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);

	//Write route add default gw xxx.xxx.xxx.xxx ->Netmask
	snprintf(szCmd, sizeof(szCmd), "echo \"/sbin/route add default gw %s %s\" >> %s", 
	    inet_ntoa(*((struct in_addr*)(&ulGateway))), ifrname,
	    net_config_file);
	//system(szCmd);
	_SYSTEM(szCmd);
	

}

/*==========================================================================*
* FUNCTION : SetNetworkInfo
* PURPOSE  : Set network info to linux system
* CALLS    : SetLanInfo
* CALLED BY: SetACUNetInfo
* ARGUMENTS: int    nIfNum       : the index of net adapter
*            int    bSetIp       : set ip?  --1 is set
*            ULONG  ulIp         : ip address
*            int    bSetMask     : set sub net mask? --1 is set
*            ULONG  ulMask       : net mask
*            int    bSetGateway  : set default gateway? --1 is set
*            ULONG  ulGateway    : default gateway
*            int    bBroardcast  : set broardcast address? --1 is set
*            ULONG  ulBroardcast : broardcast address
* RETURN   : int : -1(create socket fail)
*					0 (set successfully)
*					1 (set fail)
*					2 (no item to set)
*
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-17 11:15
*==========================================================================*/
int SetNetworkInfo(int nIfNum, 
				   int bSetIp,		ULONG ulIp,
				   int bSetMask,	ULONG ulMask,
				   int bSetGateway,	ULONG ulGateway, 
				   int bBroardcast,	ULONG ulBroardcast)
{
	BOOL bOnlySetIp = (bSetIp && !bSetMask && !bSetGateway && !bBroardcast);
	BOOL bOnlySetMask = (!bSetIp && bSetMask && !bSetGateway && !bBroardcast);

	int dhcpSocket;	/* socket */
	int rc = 0;

	ULONG ulIPOld, ulNetmaskOld, ulGatewayOld, ulBroadcastOld;

	//no item to set
	if ((!bSetIp) && (!bSetMask) 
		&& (!bSetGateway) && (!bBroardcast))
	{
		return 2;
	}

	dhcpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (dhcpSocket == -1)
	{
	//	TRACEX("\n\r[SetNetworkInfo] -- create socket fail %x", errno);
		return -1;
	}

	//Because the modify of IP will infulence the netmask,
	//and the modify of netmask will inflence the default gateway
	//so in any case, will modify the three items
	GetNetworkInfo(nIfNum,
		TRUE, &ulIPOld,
		TRUE, &ulNetmaskOld,
		TRUE, &ulGatewayOld,
		FALSE, &ulBroadcastOld);//Do not get Broardcast

	if (!bSetIp)
	{
		bSetIp	= TRUE;
		ulIp	= ulIPOld;
	}

	if (!bSetMask)
	{
		bSetMask	= TRUE;
		ulMask		= ulNetmaskOld;
	}

	if (!bSetGateway)
	{
		bSetGateway	= TRUE;
		ulGateway	= ulGatewayOld;
	}

	if (bSetIp)
	{
		/* 1, setting IP address */
		if (SetLanInfo(nIfNum, dhcpSocket, 
			SIOCGIFADDR, SIOCSIFADDR, ulIp) != 0)
		{
		/*	TRACEX("\n\r[SetNetworkInfo] -- ioctl SIOCSIFADDR fail %x", 
				errno);*/
			rc ++;

			close(dhcpSocket);
			return rc;
		}
	}

	if (bSetMask)
	{
		/* 2. setting netmask */
		if (SetLanInfo(nIfNum, dhcpSocket, 
			SIOCGIFNETMASK, SIOCSIFNETMASK, ulMask) != 0)
		{
			/*TRACEX("\n\r[SetNetworkInfo] -- ioctl SIOCSIFNETMASK fail %x", 
				errno);*/
			rc ++;

			close(dhcpSocket);
			return rc;
		}
	}

	if (bBroardcast)
	{
		/* 3. setting broadcast address */
		if (SetLanInfo(nIfNum, dhcpSocket,
			SIOCGIFBRDADDR, SIOCSIFBRDADDR, ulBroardcast) != 0)
		{
			/*TRACEX("\n\r[SetNetworkInfo] -- ioctl SIOCSIFBRDADDR fail %x", 
				errno);*/
			rc ++;

			close(dhcpSocket);
			return rc;
		}
	}


	if (bSetGateway)
	{
		ULONG ulOldGateway = 0;

		char szCmd[128];

		if((ulIPOld & ulNetmaskOld) == (ulGateway & ulNetmaskOld))
		{
			if(GetDefaultGateway(nIfNum, &ulOldGateway) == RET_SUCCESS
				&& ulOldGateway != 0)
			{
				snprintf(szCmd, sizeof(szCmd), "/sbin/route del default gw %s", 
					inet_ntoa(*((struct in_addr*)(&ulOldGateway))));
				//system(szCmd);
				_SYSTEM(szCmd);
			}

			snprintf(szCmd, sizeof(szCmd), "/sbin/route add default gw %s",
				inet_ntoa(*((struct in_addr*)(&ulGateway))));

			//system(szCmd);
			_SYSTEM(szCmd);
		}
		else
		{
			rc++;
		}
	
	}

	close(dhcpSocket);

	//Save the neweast network modification to config file
	SaveToNetworkCfgFile(nIfNum);

	// if set ip address only, return success in any case.
	if(bOnlySetIp || bOnlySetMask)
	{
		rc = 0;
	}
	return rc;
}

int GetIPV6_NetworkInfo(int nIfNum, void* pOutInfo)
{
	ACU_V6_NET_INFO* pNetV6Info = (ACU_V6_NET_INFO*)pOutInfo;
	GetIPV6Addr(nIfNum,&pNetV6Info->stLocalAddr,&pNetV6Info->stGlobalAddr);
	GetIPV6GateWay(nIfNum,&pNetV6Info->stGateWay);
	return 0;
}
#define IS_IPV6_ADDR_NULL(ip) (((ip).S6_addr32[0] == 0) && ((ip).S6_addr32[1] == 0) && ((ip).S6_addr32[2] == 0) && ((ip).S6_addr32[3] == 0))
#define  MAX_IPV6_ADDR_STR_LEN	128
#define IPV6_NETWORK_CONFIG_FILE_ETH0 "/home/app_script/init6_eth0"
#define IPV6_NETWORK_CONFIG_FILE_ETH1 "/home/app_script/init6_eth1"

static void GetIPV6FormatAddr(IN6_IFREQ* pSrcAddr, OUT char* pOutformatStr)
{
	char strIPV6Addr[96];
	inet_ntop(AF_INET6, &pSrcAddr->ifr6_addr, strIPV6Addr, sizeof(strIPV6Addr));
	snprintf(pOutformatStr,MAX_IPV6_ADDR_STR_LEN,"%s/%d",strIPV6Addr,pSrcAddr->ifr6_prefixlen);
}

static BOOL SaveIPV6ToCfgFile(int nIfNum,ACU_V6_NET_INFO* pNetV6Info,int nChangeType)
{
	char strNetName[16];
	char strSysCmd[256];
	char strFormatedIPV6[MAX_IPV6_ADDR_STR_LEN];

	if(nIfNum == 0)
	{
		sprintf(strNetName,"%s","eth0");
	}
	else
	{
		sprintf(strNetName,"%s","eth1");
	}
	char net_config_file[128];

	//20130805 lkf �����������������������ļ�һ��д
	if (nIfNum == 0)
	{
		sprintf(net_config_file, "%s", IPV6_NETWORK_CONFIG_FILE_ETH0);
	}
	else
	{
		sprintf(net_config_file, "%s",  IPV6_NETWORK_CONFIG_FILE_ETH1);
	}

	//Clear the content of the file
	snprintf(strSysCmd, sizeof(strSysCmd), "> %s", net_config_file);
	//system(szCmd);
	_SYSTEM(strSysCmd);

	//Write #!/bin/sh
	snprintf(strSysCmd, sizeof(strSysCmd), "echo \"#!/bin/sh\" >> %s", net_config_file);
	//system(szCmd);
	_SYSTEM(strSysCmd);

	if(!IS_IPV6_ADDR_NULL(pNetV6Info->stGlobalAddr.ifr6_addr))
	{
		//Write /sbin/ifconfig eth0 xxx.xxx.xxx.xxx ->IPV6 addr
		GetIPV6FormatAddr(&pNetV6Info->stGlobalAddr,strFormatedIPV6);	
		snprintf(strSysCmd, sizeof(strSysCmd), "echo \"/sbin/ifconfig %s inet6 add %s\" >> %s", strNetName,
			strFormatedIPV6,
			net_config_file);
		//system(szCmd);
		_SYSTEM(strSysCmd);
	}

	if(!IS_IPV6_ADDR_NULL(pNetV6Info->stGateWay))
	{	
		memset(strFormatedIPV6,0,MAX_IPV6_ADDR_STR_LEN);
		inet_ntop(AF_INET6, &pNetV6Info->stGateWay, strFormatedIPV6, sizeof(strFormatedIPV6));
		//Write route add default gw xxx.xxx.xxx.xxx ->Netmask
		snprintf(strSysCmd, sizeof(strSysCmd), "echo \"/sbin/route -A inet6 add default gw %s %s\" >> %s", 
			strFormatedIPV6, 
			strNetName,
			net_config_file);
		//system(szCmd);
		_SYSTEM(strSysCmd);
	}

	sprintf(strSysCmd,"%s",net_config_file);
	_SYSTEM(strSysCmd); //ȫ��ִ��һ�飬��������������ã���ԭ��V4�ļ��һЩ
	if(nChangeType == -1)
	{
		return TRUE;
	}
	else if(nChangeType == IPV6_CHANGE_GW)
	{
		IN6_ADDR ipV6GwGet;
		memset(&ipV6GwGet,0,sizeof(IN6_ADDR));
		GetIPV6GateWay(nIfNum, &ipV6GwGet);
		//printf("\n=--------------Jimmy I got a cmd to set gw\n");
		if(IS_IPV6_ADDR_NULL(ipV6GwGet))
		{
			return FALSE;
		}
	}
	else if(nChangeType == IPV6_CHANGE_ALL)
	{
		IN6_ADDR ipV6GwGet;
		memset(&ipV6GwGet,0,sizeof(IN6_ADDR));
		GetIPV6GateWay(nIfNum, &ipV6GwGet);
		if(IS_IPV6_ADDR_NULL(ipV6GwGet))
		{
			return FALSE;
		}
		IN6_IFREQ ipV6Glb, ipV6Loc;
		memset(&ipV6Glb,0,sizeof(IN6_IFREQ));
		GetIPV6Addr(nIfNum,&ipV6Loc,&ipV6Glb);
		if(IS_IPV6_ADDR_NULL(ipV6Glb.ifr6_addr))
		{
			return FALSE;
		}
	}
	else
	{
		IN6_IFREQ ipV6Glb, ipV6Loc;
		memset(&ipV6Glb,0,sizeof(IN6_IFREQ));
		GetIPV6Addr(nIfNum,&ipV6Loc,&ipV6Glb);
		if(IS_IPV6_ADDR_NULL(ipV6Glb.ifr6_addr))
		{
			return FALSE;
		}
	}

	//��popenʧ�ܵ�ʱ��Ҫһֱ�ȣ����Բ������ַ�ʽ
	//FILE* stream;//���ݷ���ֵ�ж��Ƿ�ִ�гɹ�������֪���Ƿ����óɹ�
	//stream = popen (strSysCmd, "r");
	//char szRet = 0;
	//fread(szRet, sizeof(char), sizeof(szRet),stream);
	//if(szRet != 0)
	//{
	//	fclose(stream);
	//	return FALSE;
	//}
	//fclose(stream);
	return TRUE;
}

int SetIPV6_NetworkInfo(int nIfNum, int nChangeType, void* pInInfo)
{
	ACU_V6_NET_INFO* pNetV6Info = (ACU_V6_NET_INFO*)pInInfo;
	ACU_V6_NET_INFO  ToSetV6Info;
	ACU_V6_NET_INFO  BackUpSetV6Info;
	memset(&ToSetV6Info,0,sizeof(ToSetV6Info));//��ʼ������
	
	char strFormatedIPV6[MAX_IPV6_ADDR_STR_LEN];
	char strSysCmd[256];
	char strNetName[16];
	if(nIfNum == 0)
	{
		sprintf(strNetName,"%s","eth0");
	}
	else
	{
		sprintf(strNetName,"%s","eth1");
	}
	//��ɾ��ԭ������
	GetIPV6Addr(nIfNum,&ToSetV6Info.stLocalAddr,&ToSetV6Info.stGlobalAddr);
	if(!IS_IPV6_ADDR_NULL(ToSetV6Info.stGlobalAddr.ifr6_addr))
	{
		GetIPV6FormatAddr(&ToSetV6Info.stGlobalAddr,strFormatedIPV6);
		sprintf(strSysCmd,"/sbin/ifconfig %s inet6 del %s",strNetName,strFormatedIPV6);
		_SYSTEM(strSysCmd);
	}
	GetIPV6GateWay(nIfNum,&ToSetV6Info.stGateWay);
	if(!IS_IPV6_ADDR_NULL(ToSetV6Info.stGateWay))
	{
		inet_ntop(AF_INET6, &ToSetV6Info.stGateWay, strFormatedIPV6, sizeof(strFormatedIPV6));
		sprintf(strSysCmd,"/sbin/route -A inet6 del default gw %s %s",strFormatedIPV6,strNetName);
		_SYSTEM(strSysCmd);
	}
	memcpy(&BackUpSetV6Info,&ToSetV6Info,sizeof(ACU_V6_NET_INFO));//�ȱ���һ��

	switch(nChangeType)
	{
	case IPV6_CHANGE_ADDR:
		memcpy(&ToSetV6Info.stGlobalAddr.ifr6_addr,&pNetV6Info->stGlobalAddr.ifr6_addr,sizeof(IN6_ADDR));
		break;
	case IPV6_CHANGE_PREFIX:
		ToSetV6Info.stGlobalAddr.ifr6_prefixlen = pNetV6Info->stGlobalAddr.ifr6_prefixlen;
		break;
	case IPV6_CHANGE_ADDR_AND_PREFIX:
		memcpy(&ToSetV6Info.stGlobalAddr.ifr6_addr,&pNetV6Info->stGlobalAddr.ifr6_addr,sizeof(IN6_ADDR));
		ToSetV6Info.stGlobalAddr.ifr6_prefixlen = pNetV6Info->stGlobalAddr.ifr6_prefixlen;
		break;
	case IPV6_CHANGE_GW:
		memcpy(&ToSetV6Info.stGateWay,&pNetV6Info->stGateWay,sizeof(IN6_ADDR));
		break;
	case IPV6_CHANGE_ALL:
		memcpy(&ToSetV6Info.stGlobalAddr.ifr6_addr,&pNetV6Info->stGlobalAddr.ifr6_addr,sizeof(IN6_ADDR));
		ToSetV6Info.stGlobalAddr.ifr6_prefixlen = pNetV6Info->stGlobalAddr.ifr6_prefixlen;
		memcpy(&ToSetV6Info.stGateWay,&pNetV6Info->stGateWay,sizeof(IN6_ADDR));
		break;
	default:
		break;
	}
	//printf("\n-------------------HHEIEIRIRERERIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII\n");
	//��ʼд���ļ���ִ��
	if(!SaveIPV6ToCfgFile(nIfNum,&ToSetV6Info,nChangeType))
	{
		//���Է������ִ�в��ɹ�����Ҫ����ɾ��һ�Σ���Ȼ����ֶ��IP�����
		//GetIPV6Addr(nIfNum,&ToSetV6Info.stLocalAddr,&ToSetV6Info.stGlobalAddr);
		if(!IS_IPV6_ADDR_NULL(ToSetV6Info.stGlobalAddr.ifr6_addr))
		{
			GetIPV6FormatAddr(&ToSetV6Info.stGlobalAddr,strFormatedIPV6);
			sprintf(strSysCmd,"/sbin/ifconfig %s inet6 del %s",strNetName,strFormatedIPV6);
			_SYSTEM(strSysCmd);
		}
		//GetIPV6GateWay(nIfNum,&ToSetV6Info.stGateWay);
		if(!IS_IPV6_ADDR_NULL(ToSetV6Info.stGateWay))
		{
			inet_ntop(AF_INET6, &ToSetV6Info.stGateWay, strFormatedIPV6, sizeof(strFormatedIPV6));
			sprintf(strSysCmd,"/sbin/route -A inet6 del default gw %s %s",strFormatedIPV6,strNetName);
			_SYSTEM(strSysCmd);
		}
		//printf("\n------------------------Jimmy, Save IP err!!!!!!!!!!\n");
		SaveIPV6ToCfgFile(nIfNum,&BackUpSetV6Info,-1);//ִ��ʧ�ܣ�˵�������⣬���Իָ�Ĭ��,-1��ʾ���ж�
		return 1;
	}

	return 0;
}

/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : data_exchange.c
*  CREATOR  : HULONGWEN                DATE: 2004-09-14 14:48
*  VERSION  : V1.00
*  PURPOSE  : The implementation file of data exchange module
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include <signal.h>

//#include "data_exchange.h"

#include "system_net_info.h"		//SetNetworkInfo() and GetNetworkInfo()
#include "../../mainapp/equip_mon/equip_mon.h"
#include <unistd.h>
#include "err_code.h"
#include <arpa/inet.h>			/* inet_ntoa */

#ifndef PRODUCT_INFO_SUPPORT
#define PRODUCT_INFO_SUPPORT
#endif

#define _DATA_EXCHANGE_DEBUG	0

#ifdef _DEBUG
//#define	_DEBUG_TRAPCALL			1
#endif
//#define    Debug_ExchangeInterface_2007    1 

//If String has invalid char, it will not be set successfully
#define HAS_INVALID_CHAR(string) \
	(((string) == NULL) \
	|| (strchr((string), '\t') != NULL) \
	|| (strchr((string), '#') != NULL) \
	|| (strchr((string), '\r') != NULL) \
	|| (strchr((string), '\n') != NULL) \
	|| (strchr((string), '\"') != NULL) \
	|| (strchr((string), '\'') != NULL) \
	|| ((string)[0] == '\0'))//Don't support ""


//need to be defined in Miscellaneous Function Module
extern TIME_SRV_INFO g_sTimeSrvInfo;  //It's used to save the time server info
//Defined in config_mgmt module
extern int WriteRunConfigItem(RUN_CONFIG_ITEM* pRunConfigItem);

//need to be defined in Miscellaneous Function Module
extern int UpdateSCUPTime (time_t* pTimeRet);
extern int UpdateNTPTime (void);

//The software version of ACU
DWORD g_dwSWVersion = ACU_APP_VER;

//the manufacturer of ACU
char g_szManufacturerName[MAX_MANUFACTURER_NAME] = "Vertiv Tech Co.,Ltd.";

//for Set LCD Key through YDN23
static _KEY_INFO_   stKeyInfo;;

#define DXI_MODULE_TASK	"DXI" //"Data exchange module"

//#define PRODUCT_INFO_OF_ACU "M800D"
#define PRODUCT_INFO_OF_ACU "M830B"

/*Mutexes for data exchange module*/

#define DXI_LOCK_WAIT_TIMEOUT		1000

#define SEND_CTRL_COMMAND_TIMEOUT	6000

#define LOG_MODIFY_ALAEM_LEVEL    "Modify Alarm LVL"
#define LOG_MODIFY_ALAEM_RELAY    "Modify Alarm RLY"

//for com share service switch function
#ifdef COM_SHARE_SERVICE_SWITCH
static SERVICE_SWITCH_FLAG g_ServiceSwitchFlag;
static HANDLE g_hMutexServiceSwitch = NULL;
#endif //COM_SHARE_SERVICE_SWITCH


//The handle of the mutex for set time server info
static HANDLE  g_hMutexESRCommonCfg = NULL;  //for ESR common config file

//The handle of the mutex for set an equip enfo
static HANDLE g_hMutexSetAEquipInfo = NULL;

//The handle of the mutex for trap call back
static HANDLE g_hMutexTrapCallBack = NULL;

//The handle of the mutex for set a signal structure info
static HANDLE g_hMutexSetASignalStru = NULL;

//The handle of the mutex for set net info
static HANDLE g_hMutexSetNetInfo = NULL;

//The handle of the mutex for set public config
static HANDLE g_hMutexSetPublicConfig = NULL;

//The handle of the mutex for set time server info
static HANDLE g_hMutexSetTimeSrv = NULL;


//The number of registered trap call back functions
static int g_nTrapCallbacks = 0;

//The array of registered trap call back functions
static CALL_BACK_REGISTER g_sCallBackRegister[CALLBACK_REGISTER_NUMBER];


//Declaration of all the static functions of data exchange module

//Initialize trap call back routine 
static BOOL InitRegisterTrap(void);

static int GetEquipNum(int   nVarID,			
					   int   nVarSubID,		
					   int*  pBufLen,			
					   void* pDataBuf,			
					   int   nTimeOut);       

static int GetEquipList(int   nVarID,			
						int   nVarSubID,		
						int*  pBufLen,			
						void* pDataBuf,			
						int   nTimeOut);         

static int GetSamSigNumofEquip(int   nVarID,			
							   int   nVarSubID,		
							   int*  pBufLen,			
							   void* pDataBuf,			
							   int   nTimeOut);        

static int GetSamSigStruofEquip(int   nVarID,			
								int   nVarSubID,		
								int*  pBufLen,			
								void* pDataBuf,			
								int   nTimeOut);       

static int GetSamSigValueofEquip(int   nVarID,			
								 int   nVarSubID,		
								 int*  pBufLen,			
								 void* pDataBuf,			
								 int   nTimeOut);       

static int GetSetSigNumofEquip(int   nVarID,			
							   int   nVarSubID,		
							   int*  pBufLen,			
							   void* pDataBuf,			
							   int   nTimeOut);       

static int GetSetSigStruofEquip(int   nVarID,			
								int   nVarSubID,		
								int*  pBufLen,			
								void* pDataBuf,			
								int   nTimeOut);        

static int GetSetSigValueofEquip(int   nVarID,			
								 int   nVarSubID,		
								 int*  pBufLen,			
								 void* pDataBuf,			
								 int   nTimeOut);      

static int GetConSigNumofEquip(int   nVarID,			
							   int   nVarSubID,		
							   int*  pBufLen,			
							   void* pDataBuf,			
							   int   nTimeOut);       

static int GetConSigStruofEquip(int   nVarID,			
								int   nVarSubID,		
								int*  pBufLen,			
								void* pDataBuf,			
								int   nTimeOut);        

static int GetConSigValueofEquip(int   nVarID,			
								 int   nVarSubID,		
								 int*  pBufLen,			
								 void* pDataBuf,			
								 int   nTimeOut);    

static int GetAlarmSigNumofEquip(int   nVarID,			
								 int   nVarSubID,		
								 int*  pBufLen,			
								 void* pDataBuf,			
								 int   nTimeOut);       

static int GetAlarmSigStruofEquip(int   nVarID,			
								  int   nVarSubID,		
								  int*  pBufLen,			
								  void* pDataBuf,			
								  int   nTimeOut);    

static int GetActiveAlarmNum(int   nVarID,			
							 int   nVarSubID,		
							 int*  pBufLen,			
							 void* pDataBuf,			
							 int   nTimeOut);        

static int GetActiveAlarmInfo(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut);    

static int GetMultipleSignalsInfo(int   nVarID,			
								  int   nVarSubID,		
								  int*  pBufLen,			
								  void* pDataBuf,			
								  int   nTimeOut); 	

static int GetAEquipInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut); 	

static int SetAEquipInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut);   


static int GetASignalInfoStru(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut); 	

static int SetASignalInfoStru(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut);       

static int GetASignalValue(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut); 

static ALARM_SIG_VALUE* GetAAlarmValue(int   nEquipID,			
									   int   nSigType,		
									   int   nSigID);

static int SetASignalValue(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut);       

static int GetACUPublicConfig(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut); 	

static int SetACUPublicConfig(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut);         

static int GetACUNetInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut); 	

static int SetACUNetInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut);     

static int GetFrontIPInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut);

static int GetAppDHCPInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut); 	

static int SetAppDHCPInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut);   
static int GetRealDHCPInfo(BOOL bIsIPV6);						 						 

static int GetTimeSrv(int   nVarID,			
					  int   nVarSubID,		
					  int*  pBufLen,			
					  void* pDataBuf,			
					  int   nTimeOut); 	

static int SetTimeSrv(int   nVarID,			
					  int   nVarSubID,		
					  int*  pBufLen,			
					  void* pDataBuf,			
					  int   nTimeOut);   

static int GetSamplersNum(int   nVarID,			
						  int   nVarSubID,		
						  int*  pBufLen,			
						  void* pDataBuf,			
						  int   nTimeOut);       

static int GetSamplersList(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut);   

static int GetStdEquipsNum(int   nVarID,			
						  int   nVarSubID,		
						  int*  pBufLen,			
						  void* pDataBuf,			
						  int   nTimeOut);

static int GetStdEquipsList(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut);

static int GetASignalInfoViaStdEquip(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut);

static int SetASignalInfoViaStdEquip(int   nVarID,			
									 int   nVarSubID,		
									 int*  pBufLen,			
									 void* pDataBuf,			
									 int   nTimeOut);

static int GetAStdEquipInfo(int   nVarID,			
							int   nVarSubID,		
							int*  pBufLen,			
							void* pDataBuf,			
							int   nTimeOut);

//////////////////////////////////////////////////////////////////////////
//Added by wj for AlarmSupExp Config
static int GetStdEquipTypeMapInfo(int   nVarID,			
                                  int   nVarSubID,		
                                  int*  pBufLen,			
                                  void* pDataBuf,			
                                  int   nTimeOut);

static int GetStdEquipTypeMapNum(int   nVarID,			
                                 int   nVarSubID,		
                                 int*  pBufLen,			
                                 void* pDataBuf,			
                                 int   nTimeOut);

static int SetASignalValidity(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut);
//end////////////////////////////////////////////////////////////////////////

static int GetEquipAlarmRelayByID(int   nVarID,			
                                  int   nVarSubID,		
                                  int*  pBufLen,			
                                  void* pDataBuf,			
                                  int   nTimeOut);

static int SetSTDEquipAlarmRelayByID(int   nVarID,			
                                     int   nVarSubID,		
                                     int*  pBufLen,			
                                     void* pDataBuf,			
                                     int   nTimeOut);


//Set LCD Key through YDN23
static int GetLCDKeyStatus(int   nVarID,			
                                  int   nVarSubID,		
                                  int*  pBufLen,			
                                  void* pDataBuf,			
                                  int   nTimeOut);

static int SetLCDKeyStatus(int   nVarID,			
                                     int   nVarSubID,		
                                     int*  pBufLen,			
                                     void* pDataBuf,			
                                     int   nTimeOut);

//Get User Define Pages
static int GetUserDefPages(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut);

//Get the dhcp server info of ACU
static int GetDHCPServerInfo(int   nVarID,			
			     int   nVarSubID,		
			     int*  pBufLen,			
			     void* pDataBuf,			
			     int   nTimeOut);

//Set the DHCP server info of ACU
static int SetDHCPServerInfo(int   nVarID,			
			     int   nVarSubID,		
			     int*  pBufLen,			
			     void* pDataBuf,			
			     int   nTimeOut);

//Get the  g_SiteInfo.pLoadCurrentData
static int GetLoadCurrentData(int   nVarID,			
			      int   nVarSubID,		
			      int*  pBufLen,			
			      void* pDataBuf,			
			      int   nTimeOut);
//Set the  g_SiteInfo.pLoadCurrentData
static int SetLoadCurrentData(int   nVarID,			
			      int   nVarSubID,		
			      int*  pBufLen,			
			      void* pDataBuf,			
			      int   nTimeOut);

//Get the VPN info of ACU
static int GetVPNInfo(int   nVarID,			
		      int   nVarSubID,		
		      int*  pBufLen,			
		      void* pDataBuf,			
		      int   nTimeOut);
//Set the VPN info of ACU
static int SetVPNInfo(int   nVarID,			
		      int   nVarSubID,		
		      int*  pBufLen,			
		      void* pDataBuf,			
		      int   nTimeOut);

//Get the SMS info of ACU
static int GetSMSInfo(int   nVarID,			
		      int   nVarSubID,		
		      int*  pBufLen,			
		      void* pDataBuf,			
		      int   nTimeOut);
//Set the SMS info of ACU
static int SetSMSInfo(int   nVarID,			
		      int   nVarSubID,		
		      int*  pBufLen,			
		      void* pDataBuf,			
		      int   nTimeOut);

//Get the IPV6 info of ACU
static int GetIPV6Info(int   nVarID,			
		       int   nVarSubID,		
		       int*  pBufLen,			
		       void* pDataBuf,			
		       int   nTimeOut);

//Set the IPV6 info of ACU
static int SetIPV6Info(int   nVarID,			
		       int   nVarSubID,		
		       int*  pBufLen,			
		       void* pDataBuf,			
		       int   nTimeOut);

//Get the smtp info of ACU
static int GetSmtpInfo(int   nVarID,			
		       int   nVarSubID,		
		       int*  pBufLen,			
		       void* pDataBuf,			
		       int   nTimeOut);
//Set the smtp info of ACU
static int SetSmtpInfo(int   nVarID,			
		       int   nVarSubID,		
		       int*  pBufLen,			
		       void* pDataBuf,			
		       int   nTimeOut);

static int AutoCfg_BlockAlarm(IN int iFlage,IN int iState);
static int AutoCfg_Start(void);
static int Autocfg_EndNotWorkingEquipAlarm(EQUIP_INFO *pEquip);
static BOOL AutoCFG_UpdateActiveAlarmCnt(IN SITE_INFO *pSite);
static BOOL AutoCfg_ClearEquipAlarm();
static BOOL AutoCFG_UpdateActiveAlarmCntExe(IN EQUIP_INFO *pEquip, IN int *piSiteCnt);
static BOOL ChangeSMDUSamplerMode(VAR_VALUE_EX* pVarValueEx);
static BOOL ChangeWorkMode(VAR_VALUE_EX* pVarValueEx);
void DIX_AutoConfigMain_ForSlavePosition(void);

static int GetEthernetNum(void);
static int GetRealDHCPServerInfo(BOOL bIsIPV6);

//The array for get and set functions of data exchange module
static DATA_EXCHANGE_INTERFACE g_sDataExchangeInterface[] =
{
	//Get the number of equipments
	{VAR_ACU_EQUIPS_NUM, GetEquipNum, NULL},

	//Get the list of equipments
	{VAR_ACU_EQUIPS_LIST, GetEquipList, NULL},

	//Get the sample signals number of a equipment
	{VAR_SAM_SIG_NUM_OF_EQUIP, GetSamSigNumofEquip, NULL},

	//Get the sample signals structure list  of a equipment
	{VAR_SAM_SIG_STRU_OF_EQUIP, GetSamSigStruofEquip, NULL},

	//Get the sample signals value list  of a equipment
	{VAR_SAM_SIG_VALUE_OF_EQUIP, GetSamSigValueofEquip, NULL},

	//Get the set signals number of a equipment
	{VAR_SET_SIG_NUM_OF_EQUIP, GetSetSigNumofEquip, NULL},

	//Get the set signals structure list  of a equipment
	{VAR_SET_SIG_STRU_OF_EQUIP, GetSetSigStruofEquip, NULL},

	//Get the set signals value list  of a equipment
	{VAR_SET_SIG_VALUE_OF_EQUIP, GetSetSigValueofEquip, NULL},

	//Get the control signals number of a equipment
	{VAR_CON_SIG_NUM_OF_EQUIP, GetConSigNumofEquip, NULL},

	//Get the control signals structure list  of a equipment
	{VAR_CON_SIG_STRU_OF_EQUIP, GetConSigStruofEquip, NULL},

	//Get the control signals value list  of a equipment
	{VAR_CON_SIG_VALUE_OF_EQUIP, GetConSigValueofEquip, NULL},

	//Get the alarm signals number of a equipment
	{VAR_ALARM_SIG_NUM_OF_EQUIP, GetAlarmSigNumofEquip, NULL},

	//Get the alarm signals structure list  of a equipment
	{VAR_ALARM_SIG_STRU_OF_EQUIP, GetAlarmSigStruofEquip, NULL},

	//Get the active alarms number
	{VAR_ACTIVE_ALARM_NUM, GetActiveAlarmNum, NULL},

	//Get the active alarms info
	{VAR_ACTIVE_ALARM_INFO, GetActiveAlarmInfo, NULL},

	//Get multiple signals info
	{VAR_MULTIPLE_SIGNALS_INFO, GetMultipleSignalsInfo, NULL},

	//Get or set a equipment info of ACU
	{VAR_A_EQUIP_INFO, GetAEquipInfo, SetAEquipInfo},

	//Get or set a signal structure
	{VAR_A_SIGNAL_INFO_STRU, GetASignalInfoStru, SetASignalInfoStru},

	//Get or set a signal value
	{VAR_A_SIGNAL_VALUE, GetASignalValue, SetASignalValue},

	//Get or set the public configuration of ACU
	{VAR_ACU_PUBLIC_CONFIG, GetACUPublicConfig, SetACUPublicConfig},//changed by wj for three languages 2006.4.28

	//Get or set the network info of ACU
	{VAR_ACU_NET_INFO, GetACUNetInfo, SetACUNetInfo},
	
	//Get or set the time server info
	{ VAR_TIME_SERVER_INFO, GetTimeSrv, SetTimeSrv},

	//Get the number of samples	
	{VAR_SAMPLERS_NUM, GetSamplersNum, NULL},

	//Get the list of samples
	{VAR_SAMPLERS_LIST, GetSamplersList, NULL},

	//Get the number of standard equipments	
	{VAR_STD_EQUIPS_NUM, GetStdEquipsNum, NULL},

	//Get the list of standard equipments
	{VAR_STD_EQUIPS_LIST, GetStdEquipsList, NULL},

	//Get or set a signal structure var standard equip ID
	{VAR_A_SIGNAL_INFO_THRO_STD_EQUIP, GetASignalInfoViaStdEquip,
	SetASignalInfoViaStdEquip},

	//Get a standard equipment info of ACU
	{VAR_A_STD_EQUIP_INFO, GetAStdEquipInfo, NULL},

	//Get standard equipment map info of ACU
	{VAR_STD_EQUIPTYPE_MAP_INFO, GetStdEquipTypeMapInfo, NULL},

	{VAR_STD_EQUIPTYPE_MAP_NUM, GetStdEquipTypeMapNum, NULL},	

	//Set signal valid or invalid
	{VAR_A_SIGNAL_VALIDITY, NULL, SetASignalValidity},

	{VAR_STD_EQUIP_ALARM_RELAY,GetEquipAlarmRelayByID,SetSTDEquipAlarmRelayByID},
    
	{VAR_LCDKEY_THRUYDN23,GetLCDKeyStatus,SetLCDKeyStatus},

	{VAR_USER_DEF_PAGES, GetUserDefPages, NULL},
		
	//Get or Set the DHCP info of APP
	{VAR_APP_DHCP_INFO, GetAppDHCPInfo, SetAppDHCPInfo},

	//Get or Set the DHCP Server info of APP
	{VAR_DHCP_SERVER_INFO, GetDHCPServerInfo, SetDHCPServerInfo},

	//Get the point  of g_SiteInfo.pLoadCurrentData 
	{VAR_HIS_DATA_RECORD_LOAD, GetLoadCurrentData, SetLoadCurrentData},

	//Get the VPN info
	{VAR_VPN_INFO, GetVPNInfo, SetVPNInfo},

	//Get the SMS info
	{VAR_SMS_PHONE_INFO, GetSMSInfo, SetSMSInfo},

	//Get the IPV6 info
	{VAR_NET_IPV6_INFO, GetIPV6Info, SetIPV6Info},

	//Get the SMTP info
	{VAR_SMTP_INFO, GetSmtpInfo, SetSmtpInfo},
    
	//changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller��s IP address
    //Get the front port IP
	{VAR_NET_FRONT_IP_INFO, GetFrontIPInfo, NULL},
	//end -- changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller��s IP address
};


//Initialize data exchange module
BOOL InitDataExchangeModule()
{
	g_hMutexESRCommonCfg = Mutex_Create(TRUE);
	if (g_hMutexESRCommonCfg == NULL)
	{
		TRACE("Create Mutex for ESR common config file failed.\n");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitDataExchangeModule: ERROR: "
			"Create Mutex for ESR common config file failed!\n\r", __FILE__);


		return FALSE;
	}  


#ifdef COM_SHARE_SERVICE_SWITCH
	/* init flags */
	g_ServiceSwitchFlag.bCOMHaveClosed = TRUE;
	g_ServiceSwitchFlag.bHLMSUseCOM = FALSE;
	g_ServiceSwitchFlag.bMtnCOMHasClient = FALSE;

	/* init Mutex */
	g_hMutexServiceSwitch = Mutex_Create(TRUE);
	if (g_hMutexServiceSwitch == NULL)
	{
		TRACE("Create Mutex for com-share service switch failed.\n");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitDataExchangeModule: ERROR: "
			"Create Mutex for com-share service switch failed!\n\r", __FILE__);


		return FALSE;
	}
#endif //COM_SHARE_SERVICE_SWITCH



	TRACE("filename: data_exchange.c [InitDataExchangeModule]\r\n");


	g_hMutexSetAEquipInfo = Mutex_Create(TRUE);
	if (g_hMutexSetAEquipInfo == NULL)
	{
		TRACE("\n\rfilename: data_exchange.c [InitDataExchangeModule]\
			   Create Mutex for set a equip info failed");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitDataExchangeModule: ERROR: "
			"Create Mutex for set a equip info failed!\n\r", __FILE__);

		return FALSE;
	}

	g_hMutexSetASignalStru = Mutex_Create(TRUE);
	if (g_hMutexSetASignalStru == NULL)
	{
		TRACE("\n\rfilename: data_exchange.c [InitDataExchangeModule]\
			   Create Mutex for set a signal structure failed");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitDataExchangeModule: ERROR: "
			"Create Mutex for set a signal structure failed!\n\r", 
			__FILE__);

		return FALSE;
	}

	g_hMutexSetNetInfo = Mutex_Create(TRUE);

	if (g_hMutexSetNetInfo == NULL)
	{
		TRACE("\n\rfilename: data_exchange.c [InitDataExchangeModule]\
			   Create Mutex for set network info failed");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitDataExchangeModule: ERROR: "
			"Create Mutex for set network info failed!\n\r",
			__FILE__);

		return FALSE;
	}

	g_hMutexSetPublicConfig = Mutex_Create(TRUE);

	if (g_hMutexSetPublicConfig == NULL)
	{
		TRACE("\n\rfilename: data_exchange.c [InitDataExchangeModule]\
			   Create Mutex for set public config failed");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitDataExchangeModule: ERROR: "
			"Create Mutex for set public config failed!\n\r",
			__FILE__);

		return FALSE;
	}

	g_hMutexSetTimeSrv = Mutex_Create(TRUE);

	if (g_hMutexSetTimeSrv == NULL)
	{
		TRACE("\n\rfilename: data_exchange.c [InitDataExchangeModule]\
			   Create Mutex for set timesrv failed");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitDataExchangeModule: ERROR: "
			"Create Mutex for set timesrv failed!\n\r",
			__FILE__);

		return FALSE;
	}

	//Initialize trap call back routine 
	return InitRegisterTrap();
}


#define DXI_DESTROY_MUTEX(m)	\
	do { if ((m) != NULL) { Mutex_Destroy((m));}} while(0)


void DestroyDataExchangeModule(void)
{
	DXI_DESTROY_MUTEX(g_hMutexESRCommonCfg);

	DXI_DESTROY_MUTEX(g_hMutexSetAEquipInfo);

	DXI_DESTROY_MUTEX(g_hMutexTrapCallBack);

	DXI_DESTROY_MUTEX(g_hMutexSetASignalStru);

	DXI_DESTROY_MUTEX(g_hMutexSetNetInfo);

	DXI_DESTROY_MUTEX(g_hMutexSetPublicConfig);

	DXI_DESTROY_MUTEX(g_hMutexSetTimeSrv);

#ifdef COM_SHARE_SERVICE_SWITCH
	DXI_DESTROY_MUTEX(g_hMutexServiceSwitch);
#endif //COM_SHARE_SERVICE_SWITCH

}


/*==========================================================================*
* FUNCTION : DxiGetData
* PURPOSE  : Get data or config from the main module
* CALLS    : The following sub functions
* CALLED BY: To be called by the services
* ARGUMENTS: int    nInterfaceType : Interface type
*            int    nVarID         : Data ID
*            int    nVarSubID      : Data sub ID
*            int*   pBufLen        : The length of the buffer
*            void*  pDataBuf       : The data buffer
*            int    nTimeOut       : The time out for getting data
* RETURN   : int : The error code for getting data			  
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-14 17:08
*==========================================================================*/
int DxiGetData(int   nInterfaceType,		
			   int   nVarID,			
			   int   nVarSubID,		
			   int*  pBufLen,			
			   void* pDataBuf,			
			   int   nTimeOut)
{

	if (nInterfaceType < VAR_INTERFACE_TYPE_START 
		|| nInterfaceType > VAR_INTERFACE_TYPE_END)
	{
		TRACE("filename: data_exchange.c [DxiGetData] "
			"Has not nInterfaceType = %d!", nInterfaceType);

		return ERR_DXI_INVALID_VARIABLE_TYPE;
	}

	if (g_sDataExchangeInterface[nInterfaceType].fGet != NULL)
	{
		if (pBufLen == NULL)
		{
			return ERR_DXI_INVALID_LENGTH_BUF;
		}

		if (pDataBuf == NULL)
		{
			return ERR_DXI_INVALID_DATA_BUFFER;
		}

		return g_sDataExchangeInterface[nInterfaceType].fGet(
			nVarID,nVarSubID,pBufLen, pDataBuf, nTimeOut);
	}
	else
	{
		TRACE("filename: data_exchange.c [DxiGetData] \
			   Has not get function of nInterfaceType = %d!", nInterfaceType);

		return ERR_DXI_INVALID_VARIABLE_TYPE;
	}

}

/*==========================================================================*
* FUNCTION : DxiSetData
* PURPOSE  : Set data or config from the main module
* CALLS    : The following sub functions
* CALLED BY: To be called by the services
* ARGUMENTS: int    nInterfaceType : Interface type
*            int    nVarID         : Data ID
*            int    nVarSubID      : Data sub ID
*            int    nBufLen        : The length of the buffer
*            void*  pDataBuf       : The data buffer 
*            int    nTimeOut       : The time out for setting data
* RETURN   : int : The error code for setting data			  
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-14 16:48
*==========================================================================*/
int DxiSetData(int	 nInterfaceType,
			   int	 nVarID,			
			   int	 nVarSubID,		
			   int	 nBufLen,		
			   void* pDataBuf,		
			   int	 nTimeOut)
{
	int	iReturn;
	if (nInterfaceType < VAR_INTERFACE_TYPE_START 
		|| nInterfaceType > VAR_INTERFACE_TYPE_END)
	{
		TRACE("filename: data_exchange.c [DxiSetData] "
			"Has not nInterfaceType = %d!", nInterfaceType);

		return ERR_DXI_INVALID_VARIABLE_TYPE;
	}

	//disable ACU changes
	if (GetSiteInfo()->bSettingChangeDisabled 
		&& nInterfaceType != VAR_A_SIGNAL_VALUE)
	{
		return ERR_DXI_HARDWARE_SWITCH_STATUS;
	}

	if (g_sDataExchangeInterface[nInterfaceType].fSet != NULL)
	{
		if (pDataBuf == NULL)
		{
			return ERR_DXI_INVALID_DATA_BUFFER;
		}
		iReturn = g_sDataExchangeInterface[nInterfaceType].fSet(
			nVarID,nVarSubID,&nBufLen, pDataBuf, nTimeOut);
		
		//if(iReturn == ERR_DXI_OK)
		//{
		//	if(nInterfaceType == VAR_A_SIGNAL_INFO_STRU 
		//		||nInterfaceType == VAR_A_EQUIP_INFO)
		//	{
		//		//Just send the equipment ID information.
		//		NotificationFunc(_CONFIG_MASK,	// masks
		//			sizeof(int),				// the sizeof (EQUIP_INFO *)
		//			&pEquipInfo,							// the equipment address ref.
		//			FALSE);	
		//	}	
		//}
		return iReturn; 
	}
	else			
	{
		TRACE("filename: data_exchange.c [DxiSetData] \
			   Has not set function of nInterfaceType = %d!", nInterfaceType);

		return ERR_DXI_INVALID_VARIABLE_TYPE;
	}
}


/*==========================================================================*
* FUNCTION : DxiRegisterNotification
* PURPOSE  : Register or logout the trap call back routine
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char                 *pszProcName    : Register name for debug 
*            SERVICE_NOTIFY_PROC  proc            : Trap call back procedure
*            void*                lpParam         : Parameter for procedure
*            int                  nMsgType        : Call back message type
*            HANDLE               hService        : The handle of service
*            BOOL                 bRegisterHandle : Register or logout flag
* RETURN   : int : Error code for register or logout
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-14 15:48
*==========================================================================*/
int DxiRegisterNotification(char				*pszProcName, 
							SERVICE_NOTIFY_PROC proc,
							void*				lpParam, 							  
							int					nMsgType, 							  
							HANDLE				hService,							  
							BOOL				bRegisterHandle)
{
	int i, nError;
	int bFound;

	CALL_BACK_REGISTER *pItem;

	if (proc == NULL)
	{
		return ERR_DXI_INVAILID_CALLBACK;
	}

	nError = ERR_DXI_OK;
	bFound = FALSE;

	if (Mutex_Lock(g_hMutexTrapCallBack, DXI_LOCK_WAIT_TIMEOUT) 
		!= ERR_MUTEX_OK)
	{
		return ERR_DXI_MUTEX_WAIT_TIMEOUT;
	}

	/* check the TRAP proc existed or not */
	pItem = g_sCallBackRegister;

	for (i = 0; i < g_nTrapCallbacks; i++, pItem++)
	{
		if ((pItem->proc == proc) 
			&& (pItem->lpParam == lpParam)
			&& (pItem->nMsgType == nMsgType)
			&& (pItem->hService == hService))
		{
			bFound = TRUE;
			break;
		}
	}

	//Register the proc
	if (bRegisterHandle == _REGISTER_FLAG)
	{
		if (bFound == TRUE)
		{
			nError = ERR_DXI_CALLBACK_ALEADY_EXITS;
		}
		else if (g_nTrapCallbacks >= CALLBACK_REGISTER_NUMBER)
		{
			nError = ERR_DXI_TOO_MANY_CALLBACK;
		}
		else
		{
			//{{ get the service type info. maofuhua, 20050118
			APP_SERVICE *pService; 
			pService = ServiceManager_GetService(SERVICE_GET_BY_HANDLE,
				(void *)hService);

			// set the service type of the item.
			if ((pService != NULL) && 
				((pService->nServiceType == SERVICE_OF_LOGIC_CONTROL) || 
				(pService->nServiceType == SERVICE_OF_USER_INTERFACE)))
			{
				pItem->bAlarmBlockingAllowed = FALSE;
			}
			else
			{
				pItem->bAlarmBlockingAllowed = TRUE;
			}
			//}}

			// pItem is current empty item 
#ifdef _DEBUG
			strncpy(pItem->szName, pszProcName, sizeof(pItem->szName));
			pItem->szName[sizeof(pItem->szName)-1] = 0;
#endif 	/*_DEBUG	*/

			pItem->proc = proc;
			pItem->lpParam = lpParam;
			pItem->nMsgType = nMsgType; 
			pItem->hService = hService;

			g_nTrapCallbacks++;
			
#ifdef _DEBUG
			TRACE("\n\r>>>> Register Trap#%d callback: %s %p %d 0x%08x\n\r", 
							g_nTrapCallbacks,
							pItem->szName, pItem->proc, 
							pItem->nMsgType, (int)pItem->hService);
#endif
		}
	}
	else    // if unregister a TRAP proc
	{
		if (bFound != TRUE)
		{
			nError = ERR_DXI_CALLBACK_NOT_FOUND;
		}
		else
		{
			pItem->proc     = NULL;
			pItem->lpParam  = NULL;
			pItem->nMsgType   = 0; 
			pItem->hService   = 0;

			memmove(pItem, pItem+1,
				(g_nTrapCallbacks-i+1)*sizeof(CALL_BACK_REGISTER));

			g_nTrapCallbacks--;
		}
	}

	Mutex_Unlock(g_hMutexTrapCallBack);

	return nError;
}


/*==========================================================================*
* FUNCTION : NotificationFunc
* PURPOSE  : Call the trap call back function according to msg type
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    nMsgType        : The message type 
*            int    nTrapDataLength : The length of trap data
*            void*  lpTrapData      : The trap data buffer
*            BOOL   bUrgent         : The urgent level
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-14 15:59
*==========================================================================*/
void NotificationFunc(int	nMsgType,	
					  int	nTrapDataLength,				  
					  void* lpTrapData,					  
					  BOOL	bUrgent)
{	
	int i;
	CALL_BACK_REGISTER *pItem = g_sCallBackRegister;

#ifdef _DEBUG_TRAPCALL
	printf("\n\r\n\r ========== Traping mask=%d =========", nMsgType);
#endif 	/*_DEBUG_TRAPCALL	*/

	if (Mutex_Lock(g_hMutexTrapCallBack,DXI_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return;
	}

	for (i = 0; i < g_nTrapCallbacks; i++, pItem++)
	{
		if ((pItem->proc != NULL) 
			&& ((pItem->nMsgType & nMsgType) == nMsgType))
		{
			// do alarm out-going blocking.
			if (GetSiteInfo()->bAlarmOutgoingBlocked && 
				pItem->bAlarmBlockingAllowed && // for DATA_COMM service is always TRUE
				((nMsgType & (_ALARM_OCCUR_MASK|_ALARM_CLEAR_MASK)) != 0))// alarm notifying
			{
				continue;
			}

			//{{ Fix the bug that will cause system got BUS-ERROR if the service exited.
			//   maofuhua, 2005-6-18
			if ((pItem->hService != NULL) &&			// is a service or thread
				!THREAD_IS_RUNNING(pItem->hService))	// the service is not running
			{
				// remove this callback proc
				memmove(pItem, pItem+1,
					(g_nTrapCallbacks-i+1)*sizeof(CALL_BACK_REGISTER));

				g_nTrapCallbacks--;

				// The 'i' and 'pItem' will be increased in for(;;), 
				// so decrease them at first
				i--;
				pItem--;

				continue;	// to check the next
			}
			//}}

#ifdef _DEBUG_TRAPCALL
			printf("\n\rCalling Trap#%d callback: %s 0x%08x %d 0x%08x", 
				i, pItem->szName, (int)pItem->proc, 
				pItem->nMsgType, (int)pItem->hService);
#endif 	/*_DEBUG_TRAPCALL	*/
			if (pItem->hService != NULL)
			{
				if (pItem->proc(pItem->hService, nMsgType, nTrapDataLength, 
					lpTrapData, pItem->lpParam, bUrgent) != ERR_DXI_OK)
				{
#ifdef _DEBUG_TRAPCALL
					printf("\n\r Trap failed.");
#endif 	/*_DEBUG_TRAPCALL	*/
				}
			}
		}
	}

	Mutex_Unlock(g_hMutexTrapCallBack);

#ifdef _DEBUG_TRAPCALL
	printf("\n\r--------- Trap done-------");
#endif 	/*_DEBUG_TRAPCALL	*/

	return;
}

//Initialize trap call back routine 
static BOOL InitRegisterTrap(void)
{
	TRACE("filename: data_exchange.c [InitRegisterTrap]\r\n");

	g_hMutexTrapCallBack = Mutex_Create(TRUE);

	if (g_hMutexTrapCallBack == NULL)
	{
		TRACE("\n\rfilename: data_exchange.c [InitRegisterTrap]\
			  Trap call back mutex create failed");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitRegisterTrap: ERROR: "
			"Trap call back mutex create failed!\n\r",
			__FILE__);

		return FALSE;
	}

	g_nTrapCallbacks = 0;

	memset(g_sCallBackRegister, 0, 
		CALLBACK_REGISTER_NUMBER*sizeof(CALL_BACK_REGISTER));

	return TRUE;
}


//Get the number of equipments
static int GetEquipNum(int   nVarID,			
					   int   nVarSubID,		
					   int*  pBufLen,			
					   void* pDataBuf,			
					   int   nTimeOut)
{
	UNUSED(nVarID);
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	*pBufLen = sizeof(GetSiteInfo()->iEquipNum);

	*((int*)pDataBuf) = GetSiteInfo()->iEquipNum;

	return ERR_DXI_OK;
}       

//Get the list of equipments
static int GetEquipList(int   nVarID,			
						int   nVarSubID,		
						int*  pBufLen,			
						void* pDataBuf,			
						int   nTimeOut)
{
	UNUSED(nVarID);
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	*pBufLen = (GetSiteInfo()->iEquipNum) * sizeof(EQUIP_INFO);

	*((EQUIP_INFO**)pDataBuf) = GetSiteInfo()->pEquipInfo;

	return ERR_DXI_OK;
}  


/*==========================================================================*
* FUNCTION : GetEquipInfoByEquipID
* PURPOSE  : Get a equip info according to it's ID
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int                   nEquipID      : Equip ID
*            IN OUT EQUIP_INFO**          pEquip        : the ret equit info
*            IN OUT STDEQUIP_TYPE_INFO**  pStdEquipType : Ret standard equip info
* RETURN   : static int : error code
* COMMENTS : updated on 2013-7-16, after code review by David.
*  			 pEquip and pStdEquipType are set to NULL is there is an error
* CREATOR  : HULONGWEN                DATE: 2004-09-20 16:44
*==========================================================================*/
static int GetEquipInfoByEquipID(int					nEquipID, 
								 EQUIP_INFO**			pEquip,
								 STDEQUIP_TYPE_INFO**	pStdEquipType)
{
	int i;

	EQUIP_INFO*			pEquipInfo;

	//begin of G3_OPT [run time], by Lin.Tao.Thomas, 2013-5
	/*STDEQUIP_TYPE_INFO*	pStdEquip;

	pEquipInfo = GetSiteInfo()->pEquipInfo;

	for(i = 0;i < GetSiteInfo()->iEquipNum; i++, pEquipInfo++)
	{

		if (!pEquipInfo)
		{
			TRACE("\n\r filename: data_exchange.c [GetEquipInfoByEquipID]\
				  pEquipInfo is not valid ");

			*pEquip			= NULL;
			*pStdEquipType	= NULL;

			return ERR_DXI_INVALID_EQUIP_BUFFER;
		}

		if (pEquipInfo->iEquipID == nEquipID)
		{
			break;
		}
	}

	if (i >= GetSiteInfo()->iEquipNum)
	{
		*pEquip			= NULL;
		*pStdEquipType	= NULL;

		return ERR_DXI_INVALID_EQUIP_ID;
	}

	pStdEquip = pEquipInfo->pStdEquip;

	if (!pStdEquip)
	{
		{
			TRACE("\n\r filename: data_exchange.c [GetEquipInfoByEquipID]\
				   pEquipInfo is not valid ");

			*pEquip			= NULL;
			*pStdEquipType	= NULL;

			return ERR_DXI_INVALID_STD_EQUIP_BUFFER;
		}
	}

	*pEquip			= pEquipInfo;
	*pStdEquipType	= pStdEquip;

	return ERR_DXI_OK;*/

	SITE_INFO *pSite = GetSiteInfo();  
	int iEquipNum = pSite->iEquipNum;

	if (nEquipID <= 0)
	{
		return ERR_DXI_INVALID_EQUIP_ID;
	}

	// quikly search the ID. The ID start from 1.
	if (nEquipID <= iEquipNum)
	{
		pEquipInfo = &pSite->pEquipInfo[nEquipID-1];
		if( pEquipInfo->iEquipID == nEquipID)
		{
			*pEquip			= pEquipInfo;
			*pStdEquipType	= pEquipInfo->pStdEquip;

			return ERR_DXI_OK;
		}
	}

	// the ID is not equal to the INDEX, compare the ID one by one
	//pEquipInfo = pSite->pEquipInfo;
	//for (i = 0; i < pSite->iEquipNum; i++, pEquipInfo++)
	//{
	//	if (pEquipInfo->iEquipID == nEquipID)
	//	{
	//		*pEquip			= pEquipInfo;
	//		*pStdEquipType	= pEquipInfo->pStdEquip;

	//		return ERR_DXI_OK;
	//	}
	//}
	//
	//improve the serch efficiency, comments by David.
	int head = 0, tail = iEquipNum-1;
	pEquipInfo = pSite->pEquipInfo;
	do
	{
		i = (head + tail)/2;

		if (pEquipInfo[i].iEquipID > nEquipID)
		{
			tail = i - 1;
		}

		else
		{
			head = i + 1;
		}
	}
	while(pEquipInfo[i].iEquipID != nEquipID && head <= tail);

	if (pEquipInfo[i].iEquipID == nEquipID)
	{
		*pEquip			= (pEquipInfo+i);
		*pStdEquipType	= (pEquipInfo+i)->pStdEquip;

		return ERR_DXI_OK;
	}

	*pEquip			= NULL;
	*pStdEquipType	= NULL;

	return ERR_DXI_INVALID_EQUIP_ID;
	//end of G3_OPT [runt time]

}

//Get the sample signals number of a equipment
static int GetSamSigNumofEquip(int   nVarID,			
							   int   nVarSubID,		
							   int*  pBufLen,			
							   void* pDataBuf,			
							   int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = sizeof(pStdEquip->iSampleSigNum);

	*((int*)pDataBuf) = pStdEquip->iSampleSigNum;

	return ERR_DXI_OK;
}        

//Get the sample signals structure list  of a equipment
static int GetSamSigStruofEquip(int   nVarID,			
								int   nVarSubID,		
								int*  pBufLen,			
								void* pDataBuf,			
								int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}


	*pBufLen = (pStdEquip->iSampleSigNum) * sizeof(SAMPLE_SIG_INFO);

	*((SAMPLE_SIG_INFO**)pDataBuf) = pStdEquip->pSampleSigInfo;


	return ERR_DXI_OK;
}       

//Get the sample signals value list  of a equipment
static int GetSamSigValueofEquip(int   nVarID,			
								 int   nVarSubID,		
								 int*  pBufLen,			
								 void* pDataBuf,			
								 int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = (pStdEquip->iSampleSigNum) 
		* sizeof(SAMPLE_SIG_VALUE);

	*((SAMPLE_SIG_VALUE**)pDataBuf) = pEquipInfo->pSampleSigValue;


	return ERR_DXI_OK;
}       

//Get the set signals number of a equipment
static int GetSetSigNumofEquip(int   nVarID,			
							   int   nVarSubID,		
							   int*  pBufLen,			
							   void* pDataBuf,			
							   int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = sizeof(pStdEquip->iSetSigNum);

	*((int*)pDataBuf) = pStdEquip->iSetSigNum;

	return ERR_DXI_OK;
}       

//Get the set signals structure list  of a equipment
static int GetSetSigStruofEquip(int   nVarID,			
								int   nVarSubID,		
								int*  pBufLen,			
								void* pDataBuf,			
								int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = (pStdEquip->iSetSigNum) 
		* sizeof(SET_SIG_INFO);

	*((SET_SIG_INFO**)pDataBuf) = pStdEquip->pSetSigInfo;


	return ERR_DXI_OK;
}        

//Get the set signals value list  of a equipment
static int GetSetSigValueofEquip(int   nVarID,			
								 int   nVarSubID,		
								 int*  pBufLen,			
								 void* pDataBuf,			
								 int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = (pStdEquip->iSetSigNum) 
		* sizeof(SET_SIG_VALUE);

	*((SET_SIG_VALUE**)pDataBuf) = pEquipInfo->pSetSigValue;
	//printf("Run here3 pEquipInfo->pSetSigValue->pStdSig->iSigID=%d\n",pEquipInfo->pSetSigValue->pStdSig->iSigID);//zzc debug


	return ERR_DXI_OK;
}      

//Get the control signals number of a equipment
static int GetConSigNumofEquip(int   nVarID,			
							   int   nVarSubID,		
							   int*  pBufLen,			
							   void* pDataBuf,			
							   int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = sizeof(pStdEquip->iCtrlSigNum);

	*((int*)pDataBuf) = pStdEquip->iCtrlSigNum;

	return ERR_DXI_OK;
}       

//Get the control signals structure list  of a equipment
static int GetConSigStruofEquip(int   nVarID,			
								int   nVarSubID,		
								int*  pBufLen,			
								void* pDataBuf,			
								int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = (pStdEquip->iCtrlSigNum) 
		* sizeof(CTRL_SIG_INFO);

	*((CTRL_SIG_INFO**)pDataBuf) = pStdEquip->pCtrlSigInfo;


	return ERR_DXI_OK;
}        

//Get the control signals value list  of a equipment
static int GetConSigValueofEquip(int   nVarID,			
								 int   nVarSubID,		
								 int*  pBufLen,			
								 void* pDataBuf,			
								 int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = (pStdEquip->iCtrlSigNum) 
		* sizeof(CTRL_SIG_VALUE);

	*((CTRL_SIG_VALUE**)pDataBuf) = pEquipInfo->pCtrlSigValue;


	return ERR_DXI_OK;
}       

//Get the alarm signals number of a equipment
static int GetAlarmSigNumofEquip(int   nVarID,			
								 int   nVarSubID,		
								 int*  pBufLen,			
								 void* pDataBuf,			
								 int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = sizeof(pStdEquip->iAlarmSigNum);

	*((int*)pDataBuf) = pStdEquip->iAlarmSigNum;

	return ERR_DXI_OK;
}       

//Get the alarm signals structure list  of a equipment
static int GetAlarmSigStruofEquip(int   nVarID,			
								  int   nVarSubID,		
								  int*  pBufLen,			
								  void* pDataBuf,			
								  int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = (pStdEquip->iAlarmSigNum) 
		* sizeof(ALARM_SIG_INFO);

	*((ALARM_SIG_INFO**)pDataBuf) = pStdEquip->pAlarmSigInfo;


	return ERR_DXI_OK;
} 

//Get the active alarms number
static int GetActiveAlarmNum(int   nVarID,			
							 int   nVarSubID,		
							 int*  pBufLen,			
							 void* pDataBuf,			
							 int   nTimeOut)
{
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	*pBufLen = sizeof(GetSiteInfo()->iAlarmCount[0]);

	//by HULONGWEN nVarID = 0 :Get all active alarm

	/*if (nVarID == 0xFF)
	{
	*((int*)pDataBuf) = GetSiteInfo()->iAlarmCount[0];
	}
	else if (nVarID >= ALARM_LEVEL_OBSERVATION && nVarID <= ALARM_LEVEL_CRITICAL)
	*/

	if (nVarID >= ALARM_LEVEL_NONE && nVarID <= ALARM_LEVEL_CRITICAL)
	{
		*((int*)pDataBuf) = GetSiteInfo()->iAlarmCount[nVarID];
	}
	else
	{
		return ERR_DXI_INVALID_VARIABLE_DATA_ID;
	}

	return ERR_DXI_OK;
}        

//Get the active alarms info
static int GetActiveAlarmInfo(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut)
{
	int i, j;
	int iRetAlarmNum;

	EQUIP_INFO*			pEquipInfo;
	ALARM_SIG_VALUE*	pAlarmSigValue;
	ALARM_SIG_VALUE*	pRetAlarmSigValue;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	//by HULONGWEN nVarID = 0 :Get all active alarm

	/*if ((nVarID < ALARM_LEVEL_OBSERVATION)
	|| (nVarID > ALARM_LEVEL_CRITICAL && nVarID != 0xFF))*/
	if ((nVarID < ALARM_LEVEL_NONE) || nVarID > ALARM_LEVEL_CRITICAL)
	{
		return ERR_DXI_INVALID_VARIABLE_DATA_ID;
	}

	iRetAlarmNum = 0;
	memset(pDataBuf, 0, (size_t)*pBufLen);

	pRetAlarmSigValue = (ALARM_SIG_VALUE*)pDataBuf;

	pEquipInfo = GetSiteInfo()->pEquipInfo;

	for(i = 0;i < GetSiteInfo()->iEquipNum; i++, pEquipInfo++)
	{
		if (!pEquipInfo)
		{

			TRACE("\n\r filename: data_exchange.c [GetActiveAlarmInfo]\
				   pEquipInfo is not valid ");


			return ERR_DXI_INVALID_EQUIP_BUFFER;
		}

		Mutex_Lock(pEquipInfo->hSyncLock, MAX_TIME_WAITING_EQUIP_LOCK);

		for(j = ALARM_LEVEL_OBSERVATION;j <= ALARM_LEVEL_CRITICAL;j++)
		{
			pAlarmSigValue = &(pEquipInfo->pActiveAlarms[j]);

			pAlarmSigValue = pAlarmSigValue->next;

			while ((pAlarmSigValue != NULL)
				&& (pAlarmSigValue != &(pEquipInfo->pActiveAlarms[j])))
			{
				if (nVarID == ALARM_LEVEL_NONE || nVarID == j)
				{
					if (((iRetAlarmNum + 1) * sizeof(ALARM_SIG_VALUE)) > 
						(UINT)*pBufLen
						|| !pRetAlarmSigValue)
					{
						Mutex_Unlock(pEquipInfo->hSyncLock);
						return ERR_DXI_OK;
					}

					if (ALARM_IS_ACTIVING(pAlarmSigValue))
					{
						memcpy(pRetAlarmSigValue, pAlarmSigValue, 
							sizeof(ALARM_SIG_VALUE));

						pRetAlarmSigValue++;
						iRetAlarmNum++;
					}

					pAlarmSigValue = pAlarmSigValue->next;
				}
				else
				{
					break;
				}
			}

		}

		Mutex_Unlock(pEquipInfo->hSyncLock);
	}

	//Return actual AlarmNum 
	*pBufLen = iRetAlarmNum * sizeof(ALARM_SIG_VALUE);

	return ERR_DXI_OK;

}

//Get multiple signals info
static int GetMultipleSignalsInfo(int   nVarID,			
								  int   nVarSubID,		
								  int*  pBufLen,			
								  void* pDataBuf,			
								  int   nTimeOut)
{
	int		i;
	long	lTempVar;
	int		nBufLength;

	void*	pGetInfo;

	int		nError;

	GET_MULTIPAL_SIGNALS* pGetMulSignals;

	int nEquipID, nSignalID, nSignalType;

	UNUSED(nVarSubID);


	if (*pBufLen != sizeof(GET_MULTIPAL_SIGNALS))
	{
		return ERR_DXI_WRONG_BUFFER_LENGTH;
	}

	if (nVarID != MUL_SIGNALS_GET_VALUE && nVarID != MUL_SIGNALS_GET_STRU)
	{
		return ERR_DXI_INVALID_VARIABLE_DATA_ID;	
	}

	pGetMulSignals = (GET_MULTIPAL_SIGNALS*)pDataBuf;


	for(i = 0;i < pGetMulSignals->nGetNum;i++)
	{
		lTempVar = pGetMulSignals->lData[i];

		DXI_SPLIT_UNIQUE_SIG_ID(lTempVar, nEquipID, nSignalType, nSignalID);

		if (nVarID == MUL_SIGNALS_GET_VALUE)
		{
			nError = GetASignalValue(nEquipID,
				DXI_MERGE_SIG_ID(nSignalType, nSignalID),
				&nBufLength,
				&pGetInfo,
				nTimeOut);
		}

		else if (nVarID == MUL_SIGNALS_GET_STRU)
		{
			nError = GetASignalInfoStru(nEquipID,
				DXI_MERGE_SIG_ID(nSignalType, nSignalID),
				&nBufLength,
				&pGetInfo,
				nTimeOut);
		}
		else
		{		
			return ERR_DXI_INVALID_VARIABLE_DATA_ID;	
		}

		if (nError != ERR_DXI_OK)
		{
			//Do not quit, continue, only set the return pointer to NULL of the signal
			//return nError;
			pGetMulSignals->lData[i] = (long)NULL;
		}
		else
		{
			pGetMulSignals->lData[i] = (long)pGetInfo;
		}
	}

	return ERR_DXI_OK;
}

//Get a equipment info of ACU
static int GetAEquipInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	*pBufLen = sizeof(EQUIP_INFO);

	*((EQUIP_INFO**)pDataBuf) = pEquipInfo;

	return ERR_DXI_OK;
}

//Set a equipment info of ACU
static int SetAEquipInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut)
{
	int nError;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	LANG_TEXT*	pEquipName;
	char*		pModifyInfo; 


	UNUSED(nTimeOut);


	pModifyInfo = (char*)pDataBuf;


	if (strlen(pModifyInfo) != (size_t)*pBufLen)
	{
		return ERR_DXI_WRONG_BUFFER_LENGTH;
	}


	if (HAS_INVALID_CHAR(pModifyInfo))
	{
		return ERR_DXI_INVALID_MODIFY_NAME_BUF;
	}

	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	pEquipName = pEquipInfo->pEquipName;

	if (!pEquipName)
	{
		nError = ERR_DXI_INVALID_EQUIP_NAME_BUF;
		return nError;
	}

	//-----------------------moddfying by Jimmy 2012.08
	int nCopyLen = pEquipName->iMaxLenForFull + 1;
	//Jimmy Ôö¼Ó£¬ÎªÁËÖ§³Ö'#'×Ö·û×÷ÎªÃû³Æ,ÕâÀï×öÁË×ª»»
	char* pNoRepleacedInfo = NEW(char,nCopyLen);//Ô­À´µÄÎ´¸ü¸ÄµÄ

	strncpyz(pNoRepleacedInfo, 
		pModifyInfo,
		nCopyLen);

	int kk = 0;
	for(kk = 0; kk < nCopyLen;kk++)
	{
		if(*(pModifyInfo+kk) == '!')
		{
			*(pModifyInfo+kk) ='#'; //ÏÔÊ¾ÓÃÌæ»»¹ýµÄ£¬µ«´æ´¢ÔòÊ¹ÓÃÕý³£µÄ
		}
	}
	//printf("\n The Curr Name is %s",pModifyInfo);
	//------------------------end modifying

	if (Mutex_Lock(g_hMutexSetAEquipInfo,DXI_LOCK_WAIT_TIMEOUT) !=ERR_MUTEX_OK)
	{
		return ERR_DXI_MUTEX_WAIT_TIMEOUT;
	}

    //TRACE("| SetAEquipInfo:%d\n",nVarSubID);

	switch (nVarSubID)
	{
	case MODIFY_EQUIP_ENGLISH_FULL_NAME:
		{

			if ((pEquipName->pFullName[0]))
			{
				strncpyz((pEquipName->pFullName[0]), 
					pModifyInfo,
					pEquipName->iMaxLenForFull + 1);

			}
			else
			{
				nError = ERR_DXI_INVALID_EQUIP_NAME_BUF;
			}			
		}
		break;

	case MODIFY_EQUIP_LOCAL_FULL_NAME:
		{

			if ((pEquipName->pFullName[1]))
			{

				strncpyz((pEquipName->pFullName[1]), 
					pModifyInfo,
					pEquipName->iMaxLenForFull + 1);
			}
			else
			{
				nError = ERR_DXI_INVALID_EQUIP_NAME_BUF;
			}
		}
		break;

	case MODIFY_EQUIP_ENGLISH_ABBR_NAME:
		{

			if ((pEquipName->pAbbrName[0]))
			{

				strncpyz((pEquipName->pAbbrName[0]), 
					pModifyInfo,
					pEquipName->iMaxLenForAbbr + 1);
			}
			else
			{
				nError = ERR_DXI_INVALID_EQUIP_NAME_BUF;
			}
		}
		break;

	case MODIFY_EQUIP_LOCAL_ABBR_NAME:
		{

			if ((pEquipName->pAbbrName[1]))
			{

				strncpyz((pEquipName->pAbbrName[1]), 
					pModifyInfo,
					pEquipName->iMaxLenForAbbr + 1);
			}
			else
			{
				nError = ERR_DXI_INVALID_EQUIP_NAME_BUF;
			}
		}
		break;

//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.5.9
    case MODIFY_EQUIP_LOCAL2_FULL_NAME:
        {

            if ((pEquipName->pFullName[2]))
            {

                strncpyz((pEquipName->pFullName[2]), 
                    pModifyInfo,
                    pEquipName->iMaxLenForFull + 1);
            }
            else
            {
                nError = ERR_DXI_INVALID_EQUIP_NAME_BUF;
            }
        }
        break;

    case MODIFY_EQUIP_LOCAL2_ABBR_NAME:
        {

            if ((pEquipName->pAbbrName[2]))
            {

                strncpyz((pEquipName->pAbbrName[2]), 
                    pModifyInfo,
                    pEquipName->iMaxLenForAbbr + 1);
            }
            else
            {
                nError = ERR_DXI_INVALID_EQUIP_NAME_BUF;
            }
        }
        break;

//end////////////////////////////////////////////////////////////////////////

	default:
		{
			nError = ERR_DXI_INVALID_EQUIP_MODIFY_TYPE;
		}
		break;

	}

	if (nError == ERR_DXI_OK)
	{
		RUN_CONFIG_ITEM stRunConfigItem = DEF_RUN_CONFIG_ITEM(
			CONFIG_CHANGED_EQUIPNAME,
			nVarSubID,
			pNoRepleacedInfo,  //pModifyInfo,Modifying by Jimmy 2012.08
			nVarID,
			0,//Not be used
			0,//Not be used
			0);//Not be used

		WriteRunConfigItem(&stRunConfigItem);
	}

	DELETE(pNoRepleacedInfo); //¼ÇµÃÊÍ·ÅÄÚ´æ Jimmy 2012.08.07

	Mutex_Unlock(g_hMutexSetAEquipInfo);

	return ERR_DXI_OK;
}


//Get a signal structure
static int GetASignalInfoStru(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut)
{
	int	  nSigInfosLength;
	void* pSigInfo;

	int	  nSigID;
	int   nSigType;

	int   nSigPosition;

	int	  nError;

	UNUSED(nTimeOut);


	nSigInfosLength = 0;

	DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);

	switch (nSigType)
	{
	case SIG_TYPE_SAMPLING:
		{
			nError = GetSamSigStruofEquip(nVarID,			
				0,		
				&nSigInfosLength,			
				(void*)(&pSigInfo),			
				0);

			*pBufLen = sizeof(SAMPLE_SIG_INFO);
		}
		break;

	case SIG_TYPE_CONTROL:	
		{
			nError = GetConSigStruofEquip(nVarID,			
				0,		
				&nSigInfosLength,			
				(void*)(&pSigInfo),			
				0);

			*pBufLen = sizeof(CTRL_SIG_INFO);
		}
		break;

	case SIG_TYPE_SETTING:		
		{
			nError = GetSetSigStruofEquip(nVarID,			
				0,		
				&nSigInfosLength,			
				(void*)(&pSigInfo),			
				0);

			*pBufLen = sizeof(SET_SIG_INFO);
		}
		break;

	case SIG_TYPE_ALARM:		
		{			
			nError = GetAlarmSigStruofEquip(nVarID,			
				0,		
				&nSigInfosLength,			
				(void*)(&pSigInfo),			
				0);

			*pBufLen = sizeof(ALARM_SIG_INFO);
		}
		break;

	default:
		{
			nError = ERR_DXI_INVALID_SIG_TYPE;
		}
		break;

	}

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	nSigPosition = *pBufLen;

	while (1)
	{
		if ((nSigPosition > nSigInfosLength) || !pSigInfo)
		{
			return ERR_DXI_NOT_FIND_SIGNAL;
		}

		//because the first item of pSigInfo is iSigID
		if ((*((int*)pSigInfo) == nSigID))
		{
			break;
		}

		switch (nSigType)
		{
		case SIG_TYPE_SAMPLING:
			{
				pSigInfo = ((SAMPLE_SIG_INFO*)pSigInfo)+1;
			}
			break;

		case SIG_TYPE_CONTROL:	
			{
				pSigInfo = ((CTRL_SIG_INFO*)pSigInfo) + 1;
			}
			break;

		case SIG_TYPE_SETTING:		
			{
				pSigInfo = ((SET_SIG_INFO*)pSigInfo) + 1;
			}
			break;

		case SIG_TYPE_ALARM:		
			{			
				pSigInfo = ((ALARM_SIG_INFO*)pSigInfo) + 1;
			}
			break;

		default:
			{
				return ERR_DXI_INVALID_SIG_TYPE;
			}
			break;

		}

		nSigPosition += *pBufLen;

	}

	*((void**)pDataBuf) = pSigInfo;

	return ERR_DXI_OK;
}	

static int SetASignalModify(int		nStdEquipID,
						int		nSigType,
						void*	pSigInfo,
						SET_A_SIGNAL_INFO_STU* pModifyInfo)
{
	LANG_TEXT	*pSigName;

	char logText[256];
	int alarmLevel, alarmRleayNo;

	memset(logText, '\0', 256);	

	int nError = ERR_DXI_OK;

	if (pModifyInfo->byModifyType == MODIFY_ALARM_LEVEL 
		|| pModifyInfo->byModifyType == MODIFY_ALARM_RELAY)//added by ht,2006.11.02
	{
		char* pWriteConfigStr;

		if (Mutex_Lock(g_hMutexSetASignalStru,DXI_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
		{
			return ERR_DXI_MUTEX_WAIT_TIMEOUT;
		}

		if (nSigType == SIG_TYPE_ALARM)
		{
			if (pModifyInfo->byModifyType == MODIFY_ALARM_LEVEL) 
			{
				alarmLevel = ((ALARM_SIG_INFO*)pSigInfo)->iAlarmLevel;

				((ALARM_SIG_INFO*)pSigInfo)->iAlarmLevel 
					= pModifyInfo->bModifyAlarmLevel;

#ifdef _DEBUG
			TRACEX("Changing alarm level...\n");
#endif
			}
			else
			{
				alarmRleayNo = ((ALARM_SIG_INFO*)pSigInfo)->iAlarmRelayNo;

				((ALARM_SIG_INFO*)pSigInfo)->iAlarmRelayNo //added by ht,2006.11.3
					= pModifyInfo->bModifyAlarmRelay;

#ifdef _DEBUG
			TRACEX("Changing alarm relay...\n");
#endif
			}

			// notify the data process task when alarm level changed.
			// maofuhua, 2005-4-19
			//{
			EQUIP_PROCESS_CMD	cmd;

			// notify the equipment manager to process data
			if (pModifyInfo->byModifyType == MODIFY_ALARM_LEVEL) //added by ht,2006.11.02
			{
				cmd.nCmd = PROCESS_CMD_ALARM_LEVEL_CHANGED;
			}
			else
			{
				cmd.nCmd = PROCESS_CMD_ALARM_RELAY_CHANGED;
			}
			//	(int)cmd.pEquip = nStdEquipID;	// set the nStdEquipTypeID.
			Queue_Put(GetSiteInfo()->hqProcessCommand, &cmd, FALSE);

			//TRACE("DEBUG_PROCESS_EQUIP, 90 hantao put alarm change.\n");
			//}
		}
		else
		{
			nError = ERR_DXI_INVALID_VARIABLE_SUBID;
		}
		
		if (pModifyInfo->byModifyType == MODIFY_ALARM_LEVEL) //added by ht,2006.11.02
		{
			if (pModifyInfo->bModifyAlarmLevel == ALARM_LEVEL_NONE)
			{
				pWriteConfigStr = "NA";
			}
			else if (pModifyInfo->bModifyAlarmLevel == ALARM_LEVEL_OBSERVATION)
			{
				pWriteConfigStr = "OA";
			}
			else if (pModifyInfo->bModifyAlarmLevel == ALARM_LEVEL_MAJOR)
			{
				pWriteConfigStr = "MA";
			}
			else if (pModifyInfo->bModifyAlarmLevel == ALARM_LEVEL_CRITICAL)
			{
				pWriteConfigStr = "CA";
			}
			else
			{
				pWriteConfigStr = "NA";
			}
			if (nError == ERR_DXI_OK)
			{
				RUN_CONFIG_ITEM stRunConfigItem = DEF_RUN_CONFIG_ITEM(
					CONFIG_CHANGED_ALARMLEVEL,
					pModifyInfo->byModifyType,
					pWriteConfigStr,
					0,//Not be used
					nStdEquipID,
					nSigType,
					((ALARM_SIG_INFO*)pSigInfo)->iSigID);

				WriteRunConfigItem(&stRunConfigItem);
			}
		}
		else
		{
			if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_NONE)
			{
				pWriteConfigStr = "NA";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_FIRST)
			{
				pWriteConfigStr = "1";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_SECOND)
			{
				pWriteConfigStr = "2";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_THIRD)
			{
				pWriteConfigStr = "3";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_FOURTH)
			{
				pWriteConfigStr = "4";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_FIFTH)
			{
				pWriteConfigStr = "5";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_SIXTH)
			{
				pWriteConfigStr = "6";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_SEVENTH)
			{
				pWriteConfigStr = "7";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_EIGHTH)
			{
				pWriteConfigStr = "8";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_9TH)
			{
				pWriteConfigStr = "9";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_10TH)
			{
				pWriteConfigStr = "10";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_11TH)
			{
				pWriteConfigStr = "11";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_12TH)
			{
				pWriteConfigStr = "12";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_13TH)
			{
				pWriteConfigStr = "13";
			}			
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_14TH)
			{
				pWriteConfigStr = "14";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_15TH)
			{
				pWriteConfigStr = "15";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_16TH)
			{
				pWriteConfigStr = "16";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_17TH)
			{
				pWriteConfigStr = "17";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_18TH)
			{
				pWriteConfigStr = "18";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_19TH)
			{
				pWriteConfigStr = "19";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_20TH)
			{
				pWriteConfigStr = "20";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_21TH)
			{
				pWriteConfigStr = "21";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_22TH)
			{
				pWriteConfigStr = "22";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_23TH)
			{
				pWriteConfigStr = "23";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_24TH)
			{
				pWriteConfigStr = "24";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_25TH)
			{
				pWriteConfigStr = "25";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_26TH)
			{
				pWriteConfigStr = "26";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_27TH)
			{
				pWriteConfigStr = "27";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_28TH)
			{
				pWriteConfigStr = "28";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_29TH)
			{
				pWriteConfigStr = "29";
			}
			else if (pModifyInfo->bModifyAlarmRelay == ALARM_RELAY_30TH)
			{
				pWriteConfigStr = "30";
			}			
			else
			{
				pWriteConfigStr = "NA";
			}
			
			if (nError == ERR_DXI_OK)
			{
				RUN_CONFIG_ITEM stRunConfigItem = DEF_RUN_CONFIG_ITEM(
					CONFIG_CHANGED_ALARMRELAY,
					pModifyInfo->byModifyType,
					pWriteConfigStr,
					0,//Not be used
					nStdEquipID,
					nSigType,
					((ALARM_SIG_INFO*)pSigInfo)->iSigID);

				WriteRunConfigItem(&stRunConfigItem);
			}

		}

		Mutex_Unlock(g_hMutexSetASignalStru);

		if(pModifyInfo->byModifyType == MODIFY_ALARM_LEVEL)
		{
			if(pModifyInfo->bModifyAlarmLevel > ALARM_LEVEL_CRITICAL)
			{
				return nError;
			}

			char AlarmLevel[4][3] = {{"NA"}, {"OA"}, {"MA"}, {"CA"}};

			sprintf(logText, "Modify %d:%d from %s to %s by %s", nStdEquipID, ((ALARM_SIG_INFO*)pSigInfo)->iSigID, 
			AlarmLevel[alarmLevel], AlarmLevel[pModifyInfo->bModifyAlarmLevel], pModifyInfo->cModifyUser);
			AppLogOut(LOG_MODIFY_ALAEM_LEVEL, APP_LOG_INFO, "%s\n", logText);
		}
		else if(pModifyInfo->byModifyType == MODIFY_ALARM_RELAY)
		{
			if(pModifyInfo->bModifyAlarmRelay > ALARM_RELAY_30TH)
			{
				return nError;
			}

			sprintf(logText, "Modify %d:%d from %d to %d by %s", nStdEquipID, ((ALARM_SIG_INFO*)pSigInfo)->iSigID, 
				alarmRleayNo, pModifyInfo->bModifyAlarmRelay, pModifyInfo->cModifyUser);
			AppLogOut(LOG_MODIFY_ALAEM_RELAY, APP_LOG_INFO, "%s\n", logText);
		}

		return nError;
	}

	if (HAS_INVALID_CHAR(pModifyInfo->szModifyBuf))
	{
		return ERR_DXI_INVALID_MODIFY_NAME_BUF;
	}

	switch (nSigType)
	{
	case SIG_TYPE_SAMPLING:
		{
			pSigName = ((SAMPLE_SIG_INFO*)pSigInfo)->pSigName;
		}
		break;

	case SIG_TYPE_CONTROL:	
		{
			pSigName = ((CTRL_SIG_INFO*)pSigInfo)->pSigName;
		}
		break;


	case SIG_TYPE_SETTING:		
		{
			pSigName = ((SET_SIG_INFO*)pSigInfo)->pSigName;
		}
		break;

	case SIG_TYPE_ALARM:		
		{
			pSigName = ((ALARM_SIG_INFO*)pSigInfo)->pSigName;
		}
		break;

	default:
		{
			nError = ERR_DXI_INVALID_SIG_TYPE;
			return nError;
		}
		break;

	}

	if (!pSigName)
	{
		nError = ERR_DXI_INVALID_SIGNAL_NAME_BUF;
		return nError;
	}


	if (Mutex_Lock(g_hMutexSetASignalStru,DXI_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_DXI_MUTEX_WAIT_TIMEOUT;
	}

	switch (pModifyInfo->byModifyType)
	{
	case MODIFY_SIGNAL_ENGLISH_FULL_NAME:
		{
			if (pSigName->pFullName[0])
			{
				strncpyz((pSigName->pFullName[0]), 
					pModifyInfo->szModifyBuf,
					pSigName->iMaxLenForFull + 1);
			}
			else
			{
				nError = ERR_DXI_INVALID_SIGNAL_NAME_BUF;
			}
		}
		break;

	case MODIFY_SIGNAL_LOCAL_FULL_NAME:
		{
			if ((pSigName->pFullName[1]))
			{
				strncpyz((pSigName->pFullName[1]), 
					pModifyInfo->szModifyBuf,
					pSigName->iMaxLenForFull + 1);
			}
			else
			{
				nError = ERR_DXI_INVALID_SIGNAL_NAME_BUF;
			}
		}
		break;

	case MODIFY_SIGNAL_ENGLISH_ABBR_NAME:
		{
			if (pSigName->pAbbrName[0])
			{
				strncpyz((pSigName->pAbbrName[0]), 
					pModifyInfo->szModifyBuf,
					pSigName->iMaxLenForAbbr + 1);
			}
			else
			{
				nError = ERR_DXI_INVALID_SIGNAL_NAME_BUF;
			}
		}
		break;

	case MODIFY_SIGNAL_LOCAL_ABBR_NAME:
		{
			if ((pSigName->pAbbrName[1]))
			{
				strncpyz((pSigName->pAbbrName[1]), 
					pModifyInfo->szModifyBuf,
					pSigName->iMaxLenForAbbr + 1);

			}
			else
			{
				nError = ERR_DXI_INVALID_SIGNAL_NAME_BUF;
			}
		}
		break;

//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.5.9
    case MODIFY_SIGNAL_LOCAL2_FULL_NAME:
		{
			/*if ((pSigName->pFullName[2]))
			{
				strncpyz((pSigName->pFullName[2]), 
					pModifyInfo->szModifyBuf,
					pSigName->iMaxLenForFull + 1);
			}
			else*/
			{
				nError = ERR_DXI_INVALID_SIGNAL_NAME_BUF;
			}
		}
		break;

	case MODIFY_SIGNAL_LOCAL2_ABBR_NAME:
	{
		/*if ((pSigName->pAbbrName[2]))
		{
			strncpyz((pSigName->pAbbrName[2]), 
				pModifyInfo->szModifyBuf,
				pSigName->iMaxLenForAbbr + 1);

		}
		else*/
		{
			nError = ERR_DXI_INVALID_SIGNAL_NAME_BUF;
		}
	}
	break;
//end////////////////////////////////////////////////////////////////////////

	default:
		{
			nError = ERR_DXI_INVALID_SIGNAL_MODIFY_TYPE;
		}
		break;

	}

	if (nError == ERR_DXI_OK)
	{
		RUN_CONFIG_ITEM stRunConfigItem = DEF_RUN_CONFIG_ITEM(
			CONFIG_CHANGED_SIGNAME,
			pModifyInfo->byModifyType,
			pModifyInfo->szModifyBuf,
			0,//Not be used
			nStdEquipID,
			nSigType,
			((SAMPLE_SIG_INFO*)pSigInfo)->iSigID);//all iSigID is first item

		WriteRunConfigItem(&stRunConfigItem);

	}

	Mutex_Unlock(g_hMutexSetASignalStru);

	return nError;

}

//Set one virtual signal invalid or valid
static int SetASignalValidity(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut)
{
	SIG_BASIC_VALUE *pBasicVal;

	int	  nSigValuesLength;
	void* pSigValue;

	int	  nSigID;
	int   nSigType;

	int   nSigPosition;

	void* pStdSigInfo;

	int	  nError;

	UNUSED(nTimeOut);


	nSigValuesLength = 0;

	DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);

	if (nSigType == SIG_TYPE_SAMPLING)
	{
		nError = GetSamSigValueofEquip(nVarID,			
			0,		
			&nSigValuesLength,			
			(void*)(&pSigValue),			
			0);


		*pBufLen = sizeof(SAMPLE_SIG_VALUE);
	}
	else if (nSigType == SIG_TYPE_CONTROL)
	{
		nError = GetConSigValueofEquip(nVarID,			
			0,		
			&nSigValuesLength,			
			(void*)(&pSigValue),			
			0);

		*pBufLen = sizeof(CTRL_SIG_VALUE);

	}
	else if (nSigType == SIG_TYPE_SETTING)
	{
		nError = GetSetSigValueofEquip(nVarID,			
			0,		
			&nSigValuesLength,			
			(void*)(&pSigValue),			
			0);

		*pBufLen = sizeof(SET_SIG_VALUE);
	}
	else if (nSigType == SIG_TYPE_ALARM)
	{
		ALARM_SIG_VALUE* pAlarmSigValue;

		/*printf("Get Alarm start, nVarID = %d, nSigType = %d,nSigID = %d\r\n",
		nVarID,
		nSigType, nSigID);*/

		pAlarmSigValue = GetAAlarmValue(nVarID,			
			nSigType,		
			nSigID);

		/*printf("Get Alarm end");*/

		*pBufLen = sizeof(ALARM_SIG_VALUE);

		*(void**)pDataBuf = pAlarmSigValue;


		if(pAlarmSigValue == NULL)
		{
			return ERR_DXI_NOT_FIND_SIGNAL;
		}
		else
		{
			return ERR_DXI_OK;
		}
	}

	else
	{
		nError = ERR_DXI_INVALID_SIG_TYPE;
	}

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	nSigPosition = *pBufLen;

	while (1)
	{
		if ((nSigPosition > nSigValuesLength) || !pSigValue)
		{
			return ERR_DXI_NOT_FIND_SIGNAL; //Or invalid sigvalue buffer
		}

		if (nSigType == SIG_TYPE_SAMPLING)
		{
			pStdSigInfo = ((SAMPLE_SIG_VALUE*)pSigValue)->pStdSig;
			if (!pStdSigInfo)
			{
				return ERR_DXI_INVALID_STD_SIGNAL_BUFFER;
			}

			if (((SAMPLE_SIG_INFO*)pStdSigInfo)->iSigID  == nSigID)
			{
				break;
			}

			pSigValue = ((SAMPLE_SIG_VALUE*)pSigValue) + 1;
		}
		else if (nSigType == SIG_TYPE_CONTROL)
		{
			pStdSigInfo = ((CTRL_SIG_VALUE*)pSigValue)->pStdSig;
			if (!pStdSigInfo)
			{
				return ERR_DXI_INVALID_STD_SIGNAL_BUFFER;
			}

			if (((CTRL_SIG_INFO*)pStdSigInfo)->iSigID  == nSigID)
			{
				break;
			}

			pSigValue = ((CTRL_SIG_VALUE*)pSigValue) + 1;
		}
		else if (nSigType == SIG_TYPE_SETTING)
		{
			pStdSigInfo = ((SET_SIG_VALUE*)pSigValue)->pStdSig;
			if (!pStdSigInfo)
			{
				return ERR_DXI_INVALID_STD_SIGNAL_BUFFER;
			}

			if (((SET_SIG_INFO*)pStdSigInfo)->iSigID  == nSigID)
			{
				break;
			}

			pSigValue = ((SET_SIG_VALUE*)pSigValue) + 1;	

		}
		else
		{
			return ERR_DXI_INVALID_SIG_TYPE;
		}

		nSigPosition += *pBufLen;

	}

	if (nSigType == SIG_TYPE_SAMPLING)
	{
		pBasicVal= &(((SAMPLE_SIG_VALUE*)pSigValue)->bv);	        
	}
	else if (nSigType == SIG_TYPE_CONTROL)
	{
       pBasicVal= &(((SET_SIG_VALUE*)pSigValue)->bv); 
	}
	else if (nSigType == SIG_TYPE_SETTING)
	{
        pBasicVal= &(((CTRL_SIG_VALUE *)pSigValue)->bv);
	}
	else if(nSigType == SIG_TYPE_ALARM)
	{
		pBasicVal= &(((ALARM_SIG_VALUE *)pSigValue)->bv);
	}
	else
	{
		return ERR_DXI_INVALID_SIG_TYPE;
	}

	SIG_STATE_SET(pBasicVal, *((int *)pDataBuf));

	return ERR_DXI_OK;
}

//Set a signal structure
static int SetASignalInfoStru(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut)
{
	int	  nSigID;
	int   nSigType;

	int nSigInfoLen;
	void* pSigInfo;

	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	SET_A_SIGNAL_INFO_STU* pModifyInfo;

	int nError = ERR_DXI_OK;

	if (sizeof(SET_A_SIGNAL_INFO_STU) != *pBufLen)
	{
		return ERR_DXI_WRONG_BUFFER_LENGTH;
	}

	pModifyInfo = (SET_A_SIGNAL_INFO_STU*)pDataBuf;


	nError = GetASignalInfoStru(nVarID,			
		nVarSubID,		
		&nSigInfoLen,			
		&pSigInfo,
		nTimeOut);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);

	nError = GetEquipInfoByEquipID(nVarID, 
		&pEquipInfo,
		&pStdEquip);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}
    
	nError = SetASignalModify(pStdEquip->iTypeID,
		nSigType,
		pSigInfo,
		pModifyInfo);

	return nError;
}       

//Add the function temporarily because of Lintao's demand
static ALARM_SIG_VALUE* GetAAlarmValue(int   nEquipID,			
									   int   nSigType,		
									   int   nSigID)
{
	EQUIP_INFO*			pEquipInfo;
	STDEQUIP_TYPE_INFO*	pStdEquip;

	int					nAlarmSigNum;
	ALARM_SIG_VALUE		*pAlarmSigValue;

	int nError = ERR_LCD_OK;

	int i;

	/*printf("GetAAlarmValue\n");*/
	UNUSED(nSigType);

	nError = GetEquipInfoByEquipID(nEquipID, 
		&pEquipInfo,
		&pStdEquip);


	if (nError != ERR_LCD_OK)
	{
		return NULL;
	}


	nAlarmSigNum = pEquipInfo->pStdEquip->iAlarmSigNum;


	pAlarmSigValue = pEquipInfo->pAlarmSigValue;

	for(i = 0; i < nAlarmSigNum; i++, pAlarmSigValue++)
	{

		if((pEquipInfo->pStdEquip->pAlarmSigInfo + i)->iSigID == nSigID)
		{
			return pAlarmSigValue;
		}
	}

	return NULL;
}

//Get a signal value, the ret value is a poniter of SIG_BASIC_VALUE*,
//because in each value structure, SIG_BASIC_VALUE	bv is the first
static int GetASignalValue(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut)
{
	int	  nSigValuesLength;
	void* pSigValue;

	int	  nSigID;
	int   nSigType;

	int   nSigPosition;

	void* pStdSigInfo;

	int	  nError;

	UNUSED(nTimeOut);


	nSigValuesLength = 0;

	DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);	

	if (nSigType == SIG_TYPE_SAMPLING)
	{
		nError = GetSamSigValueofEquip(nVarID,			
			0,		
			&nSigValuesLength,			
			(void*)(&pSigValue),			
			0);


		*pBufLen = sizeof(SAMPLE_SIG_VALUE);
	}
	else if (nSigType == SIG_TYPE_CONTROL)
	{
		nError = GetConSigValueofEquip(nVarID,			
			0,		
			&nSigValuesLength,			
			(void*)(&pSigValue),			
			0);

		*pBufLen = sizeof(CTRL_SIG_VALUE);

	}
	else if (nSigType == SIG_TYPE_SETTING)
	{
		//printf("Run here2\n");  //zzc debug
		nError = GetSetSigValueofEquip(nVarID,			
			0,		
			&nSigValuesLength,			
			(void*)(&pSigValue),			
			0);

		*pBufLen = sizeof(SET_SIG_VALUE);
	}
	else if (nSigType == SIG_TYPE_ALARM)
	{
		ALARM_SIG_VALUE* pAlarmSigValue;

		/*printf("Get Alarm start, nVarID = %d, nSigType = %d,nSigID = %d\r\n",
		nVarID,
		nSigType, nSigID);*/

		pAlarmSigValue = GetAAlarmValue(nVarID,			
			nSigType,		
			nSigID);

		/*printf("Get Alarm end");*/

		*pBufLen = sizeof(ALARM_SIG_VALUE);

		*(void**)pDataBuf = pAlarmSigValue;


		if(pAlarmSigValue == NULL)
		{
			return ERR_DXI_NOT_FIND_SIGNAL;
		}
		else
		{
			return ERR_DXI_OK;
		}
	}

	else
	{
		nError = ERR_DXI_INVALID_SIG_TYPE;
	}

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	nSigPosition = *pBufLen;

	while (1)
	{
		if ((nSigPosition > nSigValuesLength) || !pSigValue)
		{
			return ERR_DXI_NOT_FIND_SIGNAL; //Or invalid sigvalue buffer
		}

		if (nSigType == SIG_TYPE_SAMPLING)
		{
			pStdSigInfo = ((SAMPLE_SIG_VALUE*)pSigValue)->pStdSig;
			if (!pStdSigInfo)
			{
				return ERR_DXI_INVALID_STD_SIGNAL_BUFFER;
			}

			if (((SAMPLE_SIG_INFO*)pStdSigInfo)->iSigID  == nSigID)
			{
				break;
			}

			pSigValue = ((SAMPLE_SIG_VALUE*)pSigValue) + 1;
		}
		else if (nSigType == SIG_TYPE_CONTROL)
		{
			pStdSigInfo = ((CTRL_SIG_VALUE*)pSigValue)->pStdSig;
			if (!pStdSigInfo)
			{
				return ERR_DXI_INVALID_STD_SIGNAL_BUFFER;
			}

			if (((CTRL_SIG_INFO*)pStdSigInfo)->iSigID  == nSigID)
			{
				break;
			}

			pSigValue = ((CTRL_SIG_VALUE*)pSigValue) + 1;
		}
		else if (nSigType == SIG_TYPE_SETTING)
		{
			pStdSigInfo = ((SET_SIG_VALUE*)pSigValue)->pStdSig;
			if (!pStdSigInfo)
			{
				return ERR_DXI_INVALID_STD_SIGNAL_BUFFER;
			}

	
			if (((SET_SIG_INFO*)pStdSigInfo)->iSigID  == nSigID)
			{
				break;
			}

			pSigValue = (void *)((SET_SIG_VALUE*)pSigValue + 1);
		}
		else
		{
			return ERR_DXI_INVALID_SIG_TYPE;
		}

		nSigPosition += *pBufLen;

	}

	//by HULONGWEN modify to ret pointer
	//memcpy(pDataBuf, pSigValue, *pBufLen);
	*(void**)pDataBuf = pSigValue;

	return ERR_DXI_OK;
}	

//Get set a signal value
static int SetASignalValue(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut)
{
	int nEquipID;
	int nSigType;
	int nSigID;
	int iBufLen;
	int iAutoCFGTemp = 0;

	int iControlResult;
	int	nSigValuesLength;
	SIG_BASIC_VALUE* pSigValue;
	int	nError;

	VAR_VALUE_EX* pVarValueEx = (VAR_VALUE_EX*)pDataBuf;

	if (*pBufLen != sizeof(VAR_VALUE_EX))
	{
		return ERR_DXI_WRONG_BUFFER_LENGTH;
	}

	ASSERT(pVarValueEx);

	//disable ACU changes if nSenderType != SERVICE_OF_LOGIC_CONTROL
	if (GetSiteInfo()->bSettingChangeDisabled 
		&& (pVarValueEx->nSenderType != SERVICE_OF_LOGIC_CONTROL))
	{
		return ERR_DXI_HARDWARE_SWITCH_STATUS;
	}

	nEquipID = nVarID;
	DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);

	if (nSigType != SIG_TYPE_SAMPLING 
		&& nSigType != SIG_TYPE_SETTING 
		&& nSigType != SIG_TYPE_CONTROL)
	{
		return ERR_DXI_INVALID_VARIABLE_SUBID;
	}

	/*if(nEquipID == 1 && nSigType == SIG_TYPE_SETTING && nSigID == 183)
	{
		ChangeSMDUSamplerMode(pVarValueEx);
	}*/

	if(nEquipID == 1 && nSigType == SIG_TYPE_SETTING && nSigID == 180)
	{
		
		//ChangeWorkMode(pVarValueEx);
		
		char szFullPath[MAX_FILE_PATH];
		char szCmdLine[128];

		Cfg_GetFullConfigPath(LOADALLEQUIP_FLAGFILE, szFullPath, MAX_FILE_PATH);
		snprintf(szCmdLine, sizeof(szCmdLine), "rm -rf %s",
			szFullPath);	
		_SYSTEM(szCmdLine);

	}

	static int sRelaySigID[12] = {13,14,15,16,17,18,19,20,22,23,24,25};
	//If support LVD 3, any control on relay for LVD3 will return TRUE;	
	if(((nEquipID == 1) ||(nEquipID == 202) ) && (nSigType == 1) )
	{
		nError = GetASignalValue(LVD_GROUP_EQUIP_ID,
			SIG_ID_LVD3_RELAY_NO,
			&nSigValuesLength,			
			(void*)(&pSigValue),			
			0);
		if(nError == ERR_DXI_OK)
		{		
			if(nEquipID == 1)
			{
				if( ((sRelaySigID[pSigValue->varValue.enumValue] >= 22) && (sRelaySigID[pSigValue->varValue.enumValue] <= 25)) 
					 || ((sRelaySigID[pSigValue->varValue.enumValue] >= 13) && (sRelaySigID[pSigValue->varValue.enumValue] <= 20))	)
				{
					if(nSigID == sRelaySigID[pSigValue->varValue.enumValue])
					{
						nError = GetASignalValue(LVD_GROUP_EQUIP_ID,
									SIG_ID_LVD3_ENABLE,
									&nSigValuesLength,			
									(void*)(&pSigValue),			
									0);
						if(nError == ERR_DXI_OK && pSigValue->varValue.enumValue)//Enable
						{
							if(strcmp(pVarValueEx->pszSenderName, LVD3_SENDER_NAME)!=0)
							{
								return ERR_DXI_OK;
							}
						}
					}
				}
			}
			else if(nEquipID == 202)
			{
				if( (sRelaySigID[pSigValue->varValue.enumValue] >= 13) && (sRelaySigID[pSigValue->varValue.enumValue] <= 20))
				{
					if(nSigID == ( (sRelaySigID[pSigValue->varValue.enumValue]) - 12))//12 is the offset of system control sig ID and IB2-1 control sig ID
					{
						nError = GetASignalValue(LVD_GROUP_EQUIP_ID,
									SIG_ID_LVD3_ENABLE,
									&nSigValuesLength,			
									(void*)(&pSigValue),			
									0);
						if(nError == ERR_DXI_OK && pSigValue->varValue.enumValue)//Enable
						{
							if(strcmp(pVarValueEx->pszSenderName, LVD3_SENDER_NAME)!=0)
							{
								return ERR_DXI_OK;
							}
						}
					}		
				}
			}
			else
			{
			}
		}
	}
	
	if(nEquipID == LVD3_EQUIP_ID && nSigType == 1 && nSigID == 1 )//For LVD 3 Control
	{
		nError = GetASignalValue(LVD_GROUP_EQUIP_ID,
			SIG_ID_LVD3_RELAY_NO,
			&nSigValuesLength,			
			(void*)(&pSigValue),			
			0);
		if(nError == ERR_DXI_OK)
		{
			nSigID = sRelaySigID[pSigValue->varValue.enumValue];	
			if( (nSigID >= 22) && (nSigID <= 25))
			{
				nEquipID = 1;
			}
			else if( (nSigID >= 13) && (nSigID <= 20))
			{
				nEquipID = 202;
				nSigID = nSigID -12;//12 is the offset of system control sig ID and IB2-1 control sig ID
			}
			else
			{
				return ERR_DXI_INVALID_SIG_ID;
			}
		}
		else
		{
			return ERR_DXI_INVALID_SIG_ID;
		}
	}
	

	////NA special requirement of MAX current limit point,
	//if(nEquipID == 2 && nSigType == SIG_TYPE_CONTROL && nSigID == 1)
	//{
	//	int	  nSigValuesLength;
	//	SIG_BASIC_VALUE* pSigValue;
	//	int	  nError;

	//	nError = GetASignalValue(1,
	//		DXI_MERGE_SIG_ID(2, 7),
	//		&nSigValuesLength,			
	//		(void*)(&pSigValue),			
	//		0);

	//	if(pSigValue->varValue.enumValue == 1)
	//	{

	//		nError = GetASignalValue(2,
	//			DXI_MERGE_SIG_ID(0, 32),
	//			&nSigValuesLength,			
	//			(void*)(&pSigValue),			
	//			0);

	//		if(pVarValueEx->varValue.fValue > pSigValue->varValue.fValue)
	//		{
	//			return ERR_EQP_INVALID_ARGS;
	//		}

	//	}

	//}

	iControlResult = Equip_Control(pVarValueEx->nSenderType, 
						pVarValueEx->pszSenderName,
						pVarValueEx->nSendDirectly,
						nEquipID, nSigType, nSigID, 
						&(pVarValueEx->varValue),  
						(DWORD)nTimeOut);
#ifdef _CODE_FOR_MINI
	if((nEquipID == 639 && nSigType == SIG_TYPE_CONTROL && nSigID == 11) ||
		(nEquipID == 640 && nSigType == SIG_TYPE_CONTROL && nSigID == 11) ||
		(nEquipID == 641 && nSigType == SIG_TYPE_CONTROL && nSigID == 11))
#else
	if((nEquipID == 639 && nSigType == SIG_TYPE_CONTROL && (nSigID == 11 || nSigID == 12)) ||
		(nEquipID == 640 && nSigType == SIG_TYPE_CONTROL && (nSigID == 11 || nSigID == 12)) ||
		(nEquipID == 641 && nSigType == SIG_TYPE_CONTROL && (nSigID == 11 || nSigID == 12)))
#endif
	{
		printf("\n into GET_AUTO_CONFIG!!!\n");
		RunThread_Heartbeat(RunThread_GetId(NULL));
		HANDLE hCmdThread = RunThread_Create("Auto Config",
			(RUN_THREAD_START_PROC)DIX_AutoConfigMain_ForSlavePosition_ForWeb,
			(void*)NULL,
			(DWORD*)NULL,
			0);

	}

	return iControlResult;
	
}

static BOOL ChangeWorkMode(VAR_VALUE_EX* pVarValueEx)
{
	int nEquipID = 1;
	int nSigType = SIG_TYPE_SETTING;
	int nSigID = 180;
	int iBufLen;
	SIG_BASIC_VALUE*		pSigValue;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		nEquipID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, nSigID),
		&iBufLen,			
		(void *)&pSigValue,			
		0);

	if(pSigValue->varValue.enumValue != pVarValueEx->varValue.enumValue)
	{
		TRACE("\n into GET_AUTO_CONFIG!!!\n");
		RunThread_Heartbeat(RunThread_GetId(NULL));
		HANDLE hCmdThread = RunThread_Create("Slave Position",
			(RUN_THREAD_START_PROC)DIX_AutoConfigMain,
			(void*)NULL,
			(DWORD*)NULL,
			0);
	}


}

static BOOL ChangeSMDUSamplerMode(VAR_VALUE_EX* pVarValueEx)
{
#define	SOLUTION_FOR_485_SMDU	"MonitoringSolution_SMDU485.cfg"
#define	SOLUTION_FOR_CAN_SMDU	"MonitoringSolution_SMDUCAN.cfg"
#define	SOLUTION		"MonitoringSolution.cfg"

	char szZip[256];


	TRACE("\n______Change SMDU Sampler Mode_______\n");

	memset(szZip, 0, sizeof(szZip));
	sprintf(szZip, "rm -f /app/config/solution/%s", SOLUTION);
	//system(szZip);
	_SYSTEM(szZip);

	TRACE("\n_____pVarValueEx->varValue.enumValue:%d____\n",pVarValueEx->varValue.enumValue);

	if(pVarValueEx->varValue.enumValue == 0)
	{
		memset(szZip, 0, sizeof(szZip));
		sprintf(szZip, "cp -f /app/config/solution/%s /app/config/solution/%s", SOLUTION_FOR_CAN_SMDU, SOLUTION);
		//system(szZip);
		_SYSTEM(szZip);
		memset(szZip, 0, sizeof(szZip));
		sprintf(szZip, "cp -f /app/config_bak/solution/%s /app/config_bak/solution/%s", SOLUTION_FOR_CAN_SMDU, SOLUTION);
		//system(szZip);
		_SYSTEM(szZip);

	}
	else if(pVarValueEx->varValue.enumValue == 1)
	{
		memset(szZip, 0, sizeof(szZip));
		sprintf(szZip, "cp -f /app/config/solution/%s /app/config/solution/%s", SOLUTION_FOR_485_SMDU, SOLUTION);
		//system(szZip);
		_SYSTEM(szZip);
		memset(szZip, 0, sizeof(szZip));
		sprintf(szZip, "cp -f /app/config_bak/solution/%s /app/config_bak/solution/%s", SOLUTION_FOR_485_SMDU, SOLUTION);
		//system(szZip);
		_SYSTEM(szZip);

	}

	return TRUE;


}


static BOOL GetAcuInfoViaFlag(int nStrLength, char* pString, char* GetStrFlag,
							  char* pFileName)
{
	FILE *pFile;
	long lFileLen;

	char* pFileContent;

	//read acu info file
	pFile = fopen(pFileName, "r");

	if (pFile == NULL)
	{
		TRACE("[%s]--GetAcuInfoViaFlag: ERROR: Can not open the "
			"file: %s!\n\r", __FILE__, pFileName);

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--GetAcuInfoViaFlag: ERROR: Can not open the "
			"file: %s!\n\r", __FILE__, pFileName);

		return FALSE;
	}

	lFileLen = GetFileLength(pFile);

	pFileContent = NEW(UCHAR, lFileLen + 1);
	if (pFileContent == NULL)
	{
		TRACE("[%s]--GetAcuInfoViaFlag: ERROR: There is no memory for "
			"clean %s file.\n\r", __FILE__, pFileName);

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--GetAcuInfoViaFlag: ERROR: There is no memory for "
			"clean %s file.\n\r", __FILE__, pFileName);

		fclose(pFile);

		return FALSE;
	}

	lFileLen = fread(pFileContent, sizeof(char), (size_t)lFileLen, pFile);

	if (lFileLen < 0) 
	{
		TRACE("[%s]--GetAcuInfoViaFlag: ERROR: Read %s file failed!\n\r",
			__FILE__, pFileName);

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--GetAcuInfoViaFlag: ERROR: Read %s file failed!\n\r",
			__FILE__, pFileName);

		/* clear the memory */
		DELETE(pFileContent);

		fclose(pFile);

		return FALSE;
	}


	//Serial number
	//strncpyz(pString, 
	//	strstr(pFileContent, GetStrFlag) + strlen(GetStrFlag), nStrLength);

	{
		char *pFoundVar = strstr(pFileContent, GetStrFlag);

		if (pFoundVar != NULL)
		{
			strncpyz(pString, 
				pFoundVar + strlen(GetStrFlag), nStrLength);
		}
		else
		{
			/* Modified by Thomas, keep the same, 2006-2-17 */
			/*strncpyz(pString, 
				"N/A", nStrLength);*/
			pString[0] = '\0';
		}
	}

	if (pString == NULL)
	{
		/* clear the memory */
		DELETE(pFileContent);

		fclose(pFile);

		return FALSE;
	}

	if (strchr(pString, '\r') != NULL)
	{
		pString[strlen(pString) - strlen(strchr(pString, '\r'))] = '\0';
	}
	else if (strchr(pString, '\n') != NULL)
	{
		pString[strlen(pString) - strlen(strchr(pString, '\n'))] = '\0';
	}

	DELETE(pFileContent);

	fclose(pFile);

	return TRUE;

}

static BOOL GetAppInfoViaFlag(int nStrLength, ACU_PRODUCT_INFO *pProductInfo,  char* pFileName)
{
	FILE *pFile;
	long lFileLen;

	char* pFileContent;
	char* pFind = NULL;

	//read acu info file
	pFile = fopen(pFileName, "r");

	if (pFile == NULL)
	{
		TRACE("[%s]--GetAppInfoViaFlag: ERROR: Can not open the "
			"file: %s!\n\r", __FILE__, pFileName);

		AppLogOut(DXI_MODULE_TASK, GetAppInfoViaFlag, 
			"[%s]--GetAcuInfoViaFlag: ERROR: Can not open the "
			"file: %s!\n\r", __FILE__, pFileName);

		return FALSE;
	}

	lFileLen = GetFileLength(pFile);

	pFileContent = NEW(UCHAR, lFileLen + 1);
	if (pFileContent == NULL)
	{
		TRACE("[%s]--GetAppInfoViaFlag: ERROR: There is no memory for "
			"clean %s file.\n\r", __FILE__, pFileName);

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--GetAppInfoViaFlag: ERROR: There is no memory for "
			"clean %s file.\n\r", __FILE__, pFileName);

		fclose(pFile);

		return FALSE;
	}

	lFileLen = fread(pFileContent, sizeof(char), (size_t)lFileLen, pFile);

	if (lFileLen <= 12) 
	{
		TRACE("[%s]--GetAppInfoViaFlag: ERROR: Read %s file failed!\n\r",
			__FILE__, pFileName);

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--GetAppInfoViaFlag: ERROR: Read %s file failed!\n\r",
			__FILE__, pFileName);

		/* clear the memory */
		DELETE(pFileContent);

		fclose(pFile);

		return FALSE;
	}

	//030704000341M810GA00;
	//Serial number  11bit
	nStrLength = 11;
	strncpyz(pProductInfo->szACUSerialNo,pFileContent, nStrLength + 1);
	//HW Version
	pFind = strchr(pFileContent, ';');
	if(pFind != NULL)
	{
		strncpyz(pProductInfo->szHWRevision,pFileContent + lFileLen - 4, 4);
	}
	else
	{
		strncpyz(pProductInfo->szHWRevision,"", 2);
	}
	//Productinfo
	strncpyz(pProductInfo->szProductNo,pFileContent + 12,lFileLen - 15);	
	strncpyz(pProductInfo->szPartNumber,pFileContent + 12,lFileLen - 15);



	DELETE(pFileContent);

	fclose(pFile);

	return TRUE;

}
//Get the public configuration of ACU
static int GetACUPublicConfig(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut)
{
	int i;
	//UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	switch (nVarID)
	{
	case SITE_INFO_POINTER:
		{
			*pBufLen = sizeof(SITE_INFO);
			*((SITE_INFO**)pDataBuf) = GetSiteInfo();
		}
		break;

	case SITE_PORT_INFO:
		{
			/**pBufLen = sizeof(PORT_INFO) * (GetSiteInfo()->iPortNum);
			*((PORT_INFO**)pDataBuf) = GetSiteInfo()->pPortInfo;*/

			// get by iPortID, Thomas, 11-22
			PORT_INFO *pPortInfo;

			pPortInfo = GetSiteInfo()->pPortInfo;
			for (i = 0; i < GetSiteInfo()->iPortNum; i++, pPortInfo++)
			{
				if (pPortInfo->iPortID == nVarSubID)
				{
					*pBufLen = sizeof(PORT_INFO);
					*((PORT_INFO**)pDataBuf) = pPortInfo;
					break;
				}
			}

			if (i == GetSiteInfo()->iPortNum)
			{
				return ERR_DXI_INVALID_VARIABLE_DATA_ID;
			}
		}
		break;

	case SITE_NAME:
		{
			*pBufLen = sizeof(LANG_TEXT);
			*((LANG_TEXT**)pDataBuf) = &(GetSiteInfo()->langSiteName);
		}
		break;

	case SITE_SW_VERSION:
		{
			*pBufLen = sizeof(g_dwSWVersion);
			*((DWORD*)pDataBuf) = g_dwSWVersion;
		}
		break;

	case ACU_MANUFACTURER:
		{
			*pBufLen = strlen(g_szManufacturerName);

			strncpyz((char*)pDataBuf,g_szManufacturerName, 
				sizeof(g_szManufacturerName));
		}
		break;

	case SITE_ID:
        {
			*pBufLen = sizeof(GetSiteInfo()->iSiteID);
			*((int*)pDataBuf) = GetSiteInfo()->iSiteID;
		}
		break;

	case LOCAL_LANGUAGE_CODE:
		{
			if (!(GetSiteInfo()->LangInfo.szLocaleLangCode))
			{
				return ERR_DXI_INVALID_SITE_LANG_CODE_BUF;
			}

			*pBufLen = strlen(GetSiteInfo()->LangInfo.szLocaleLangCode);

			*((char**)pDataBuf) = GetSiteInfo()->LangInfo.szLocaleLangCode;
			
		}
		break;

    case LOCAL2_LANGUAGE_CODE://Added by wj for three languages 2006.4.28
        {
            if (!(GetSiteInfo()->LangInfo.szLocale2LangCode))
            {
                return ERR_DXI_INVALID_SITE_LANG_CODE_BUF;
            }

            *pBufLen = strlen(GetSiteInfo()->LangInfo.szLocale2LangCode);

            *((char**)pDataBuf) = GetSiteInfo()->LangInfo.szLocale2LangCode;

        }
        break;

	case SITE_LOCATION:
		{
			*pBufLen = sizeof(LANG_TEXT);
			*((LANG_TEXT**)pDataBuf) = &(GetSiteInfo()->langSiteLocation);

		}
		break;

	case SITE_DESCRIPTION:
		{
			*pBufLen = sizeof(LANG_TEXT);
			*((LANG_TEXT**)pDataBuf) = &(GetSiteInfo()->langDescription);

		}
		break;

	case ACU_PRODUCT_INFO_GET:
		{
			ACU_PRODUCT_INFO* pAcuProductInfo = (ACU_PRODUCT_INFO*)pDataBuf;

			if (*pBufLen != sizeof(ACU_PRODUCT_INFO))
			{
				return ERR_DXI_WRONG_BUFFER_LENGTH;
			}

			if (pAcuProductInfo == NULL)
			{
				return ERR_DXI_INVALID_DATA_BUFFER;
			}


#define ACU_INFO_FILE_NAME		"/var/appinfo.log"
#define APP_INFO_FILE_NAME		"/home/SerialNum.log"
#define ACU_VERSION_FILE_NAME	"/var/version"

#define ACU_SERIAL_NUMBER_FLAG		"PRODUCT_SERIAL_NUMBER="
#define ACU_HARDWARE_VERSION_FLAG	"HARDWARE_VERSION="
#define ACU_ETHADDR_FLAG			"ethaddr="
#define ACU_PART_NUMBER_FLAG		"PART_NUMBER="

#define ACU_FILE_SYSTEM_VER_FLAG	"file sytem version: V"//"file sytem version:"


			memset(pAcuProductInfo, 0 ,sizeof(ACU_PRODUCT_INFO));

			STRING_APP_VER(pAcuProductInfo->szSWRevision, 
				sizeof(pAcuProductInfo->szSWRevision));

			strncpyz(pAcuProductInfo->szProductNo, 
				PRODUCT_INFO_OF_ACU, 
				sizeof(pAcuProductInfo->szProductNo));

#ifdef CR_CFGFILE_INFO			//CR:add config version to lcd	-caihao  2005.12.23
			strncpyz(pAcuProductInfo->szCfgFileVersion,
				GetSiteInfo()->pConfigInformation->szCfgVersion,
				sizeof(pAcuProductInfo->szCfgFileVersion));
#endif

			if ((!GetAppInfoViaFlag(sizeof(ACU_PRODUCT_INFO), 
				pAcuProductInfo, 
				APP_INFO_FILE_NAME))
				|| (!GetAcuInfoViaFlag(sizeof(pAcuProductInfo->szEthaddr), 
				pAcuProductInfo->szEthaddr, 
				ACU_ETHADDR_FLAG,
				ACU_INFO_FILE_NAME))
				|| (!GetAcuInfoViaFlag(sizeof(pAcuProductInfo->szFileSystemVersion), 
				pAcuProductInfo->szFileSystemVersion, 
				ACU_FILE_SYSTEM_VER_FLAG,
				ACU_VERSION_FILE_NAME)))
			{		
				return ERR_DXI_GET_ACU_PRODUCT_INFO;
			}	
//#ifdef PRODUCT_INFO_SUPPORT
//			if (!GetAcuInfoViaFlag(sizeof(pAcuProductInfo->szPartNumber), 
//				pAcuProductInfo->szPartNumber, 
//				ACU_PART_NUMBER_FLAG, 
//				ACU_INFO_FILE_NAME))
//			{
//				return ERR_DXI_GET_ACU_PRODUCT_INFO;
//			}
//#endif //PRODUCT_INFO_SUPPORT

		}
		break;
	default:
		{
			return ERR_DXI_INVALID_VARIABLE_DATA_ID;
		}
		break;
	}

	return ERR_DXI_OK;

}	

#ifdef PRODUCT_INFO_SUPPORT
/*==========================================================================*
 * FUNCTION : *DXI_GetDeviceProductInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : PRODUCT_INFO : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-09 18:53
 *==========================================================================*/
PRODUCT_INFO *DXI_GetDeviceProductInfo()
{
	static PRODUCT_INFO stProduct;  //generic product information structure
	ACU_PRODUCT_INFO sAcuProductInfo;

	int		iBufLen, iError;

	memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
	memset(&stProduct, 0, sizeof(stProduct));

	//call Dxi to get ACU product info
	iBufLen = sizeof(sAcuProductInfo);

	iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		ACU_PRODUCT_INFO_GET, 
		0, 
		&iBufLen,
		&(sAcuProductInfo),
		0);

	if (iError == ERR_DXI_OK)
	{
		strncpyz(stProduct.szPartNumber, sAcuProductInfo.szPartNumber,
			sizeof(stProduct.szPartNumber));
		strncpyz(stProduct.szSerialNumber, sAcuProductInfo.szACUSerialNo,
			sizeof(stProduct.szSerialNumber));
		strncpyz(stProduct.szHWVersion, sAcuProductInfo.szHWRevision,
			sizeof(stProduct.szHWVersion));
		strncpyz(stProduct.szSWVersion, sAcuProductInfo.szSWRevision,
			sizeof(stProduct.szSWVersion));

		//add device name for ACU, Thomas, 2006-2-14
#ifdef _CODE_FOR_MINI
#define MINI_NCU_DEV_NAME			"MiniNCU"

		strncpyz(stProduct.szDeviceName[0], MINI_NCU_DEV_NAME,
			sizeof(stProduct.szDeviceName[0]));
		strncpyz(stProduct.szDeviceName[1], MINI_NCU_DEV_NAME,
			sizeof(stProduct.szDeviceName[1]));
		strncpyz(stProduct.szDeviceAbbrName[0], MINI_NCU_DEV_NAME,
			sizeof(stProduct.szDeviceAbbrName[0]));
		strncpyz(stProduct.szDeviceAbbrName[1], MINI_NCU_DEV_NAME,
			sizeof(stProduct.szDeviceAbbrName[1]));
#else
		strncpyz(stProduct.szDeviceName[0], "NCU",
			sizeof(stProduct.szDeviceName[0]));
		strncpyz(stProduct.szDeviceName[1], "NCU",
			sizeof(stProduct.szDeviceName[1]));
		strncpyz(stProduct.szDeviceAbbrName[0], "NCU",
			sizeof(stProduct.szDeviceAbbrName[0]));
		strncpyz(stProduct.szDeviceAbbrName[1], "NCU",
			sizeof(stProduct.szDeviceAbbrName[1]));
#endif
	}

	return &stProduct;
}


/*==========================================================================*
 * FUNCTION : DXI_GetDeviceNum
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-11 16:48
 *==========================================================================*/
__INLINE int DXI_GetDeviceNum()
{
	return g_SiteInfo.iDeviceNum;
}


/*==========================================================================*
 * FUNCTION : DXI_GetDeviceByID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iDeviceID : 
 * RETURN   : DEVICE_INFO * : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-11 16:53
 *==========================================================================*/
DEVICE_INFO * DXI_GetDeviceByID(int iDeviceID)
{
	int	 i, iDeviceNum;
	DEVICE_INFO		*pDevice = NULL;

	iDeviceNum = g_SiteInfo.iDeviceNum;
	if (iDeviceID < 1 || iDeviceID > iDeviceNum)
	{
		return NULL;
	}

	//find the device quickly
	pDevice = g_SiteInfo.pDeviceInfo + (iDeviceID - 1);

	//not found, look through the array
	if (pDevice->iDeviceID != iDeviceID)
	{
		pDevice = g_SiteInfo.pDeviceInfo;
		for (i = 0; i < iDeviceNum; i++,pDevice++)
		{
			if (pDevice->iDeviceID == iDeviceID)
			{
				break;
			}
		}

		//still not found, bad device ID
		if (i == iDeviceNum)
		{
			return NULL;
		}
	}	

	return pDevice;
}


/*==========================================================================*
 * FUNCTION : RefreshItem
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: DEVICE_INFO  *pDevice : 
 *            int          iType    : 
 *            char         *szStr   : 
 *            int          iStrLen  : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-11 16:48
 *==========================================================================*/
static void RefreshItem(DEVICE_INFO	*pDevice, 
						int iType, 
						char *szStr, 
						int iStrLen)
{
	PI_CONVERTOR  sConvertor;
	SAMPLE_SIG_VALUE *pRefSignal;

	//should not come here!
	if (!pDevice->pConvertor)
	{
		return;
	}

	sConvertor = (pDevice->pConvertor)[iType];
	pRefSignal = sConvertor.pSigRef;

	if (pRefSignal)
	{
		if (SIG_VALUE_IS_VALID(pRefSignal))  //process when the sig is valid
		{

			if(iType == PI_CONVERTOR_PART_NUMBER)
			{
				
				sConvertor.pfnConverProc((char *)&pDevice->ppRelatedEquip[0]->iEquipID,
							sizeof(pRefSignal->bv.varValue.ulValue),
							szStr,
							iStrLen);
			}
			else
			{
				sConvertor.pfnConverProc((DWORD *)&pRefSignal->bv.varValue.ulValue,
							sizeof(pRefSignal->bv.varValue.ulValue),
							szStr,
							iStrLen);
			}

		}
		else if (!SIG_VALUE_IS_CONFIGURED(pRefSignal))
		{
			//not configured now, clear the Product info
			szStr[0] = 0;
		}
		else
		{
			sConvertor.pfnConverProc((DWORD *)&pRefSignal->bv.fRawData ,
							sizeof(pRefSignal->bv.varValue.ulValue),
							szStr,
							iStrLen);
		}
	}
	else
	{
		if(iType == PI_CONVERTOR_PART_NUMBER)
		{
			sConvertor.pfnConverProc((DWORD *)&pDevice->ppRelatedEquip[0]->iEquipID,
						sizeof(pRefSignal->bv.varValue.ulValue),
						szStr,
						iStrLen);
		}
		TRACEX("IN RefreshItem pRefSignal is %d!!\n", pRefSignal);
	}
}


/*==========================================================================*
 * FUNCTION : GetDeviceName
 * PURPOSE  : Get Device Name based on stdBasic config file
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: DEVICE_INFO  *pDevice : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2006-01-23 17:09
 *==========================================================================*/
//static void GetDeviceName(DEVICE_INFO *pDevice)
//{
//	int	 i, iDeviceTypeNum;
//	PRODUCT_INFO		*pPI = NULL;
//	DEVICE_TYPE_MAP		*pDeviceTypeMap = NULL;
//
//	pPI = pDevice->pProductInfo;
//	iDeviceTypeNum = g_SiteInfo.iDeviceTypes;
//	pDeviceTypeMap = g_SiteInfo.pDeviceTypeMap;
//
//	//init as zero!
//	pPI->szDeviceName[0] = 0;
//
//	for (i = 0; i < iDeviceTypeNum; i++, pDeviceTypeMap++)
//	{
//		if (strstr(pPI->szPartNumber, pDeviceTypeMap->szPartNumber))
//		{
//			strncpyz(pPI->szDeviceName, pDeviceTypeMap->szDeviceName,
//				sizeof(pPI->szDeviceName));
//			break;
//		}
//	}
//
//	return;
//}
/*==========================================================================*
 * FUNCTION : RefreshProductInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: DEVICE_INFO  *pDevice : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-11 16:53
 *==========================================================================*/
static void RefreshProductInfo(DEVICE_INFO *pDevice)
{
	char szBuf[64] = {0};

	//TRACE("Come to RefreshProductInfo, iDeviceID is %d\n",pDevice->iDeviceID);
	//TRACE("pDevice->iDeviceID is %d ,  \n pDevice->nRelatedEquip is %d, \n pDevice->pConvertor is %d, \n (*pDevice->ppRelatedEquip)->iEquipID is %d\n pDevice->pProductInfo is %d \n",
	//	pDevice->iDeviceID, pDevice->nRelatedEquip, pDevice->pConvertor, (*pDevice->ppRelatedEquip)->iEquipID, 
	//	pDevice->pProductInfo);

	//if (pDevice->pProductInfo->bSigModelUsed)
	{
		//high part of serial number
	//	printf("pDevice->ppRelatedEquip[0]->iEquipID = %d\n", pDevice->ppRelatedEquip[0]->iEquipID);
		RefreshItem(pDevice, PI_CONVERTOR_HI_SERIAL,
			szBuf,
			sizeof(szBuf));
        //TRACE("End Refresh PI_CONVERTOR_HI_SERIAL, pDevice is %s\n",pDevice->pProductInfo->szSerialNumber);
		//old rectifier format: 2  YYM  NNNNNN
		//new rectifier format: NN YYMM NNNNN
		if (szBuf[0])   //new rectifier used, should merge
		{
			//low part of serial number
			size_t stLen = strlen(szBuf);
			RefreshItem(pDevice, PI_CONVERTOR_LO_SERIAL,
				szBuf + stLen,
				sizeof(szBuf) - stLen);

			//merge them
			strncpyz(pDevice->pProductInfo->szSerialNumber, szBuf,
				sizeof(pDevice->pProductInfo->szSerialNumber));

		}
		else
		{
			//legacy serial number
			RefreshItem(pDevice, PI_CONVERTOR_SERIAL,
				pDevice->pProductInfo->szSerialNumber,
				sizeof(pDevice->pProductInfo->szSerialNumber));
		}
		//TRACE("End2 Refresh PI_CONVERTOR_LO_SERIAL, pDevice is %s\n",
		//	pDevice->pProductInfo->szSerialNumber);

		//part number
		RefreshItem(pDevice, PI_CONVERTOR_PART_NUMBER,
			pDevice->pProductInfo->szPartNumber,
			sizeof(pDevice->pProductInfo->szPartNumber));

		//TRACE("End3 Refresh PI_CONVERTOR_PART_NUMBER %s\n",pDevice->pProductInfo->szPartNumber);

		//HW version
		RefreshItem(pDevice, PI_CONVERTOR_HW_VER,
			pDevice->pProductInfo->szHWVersion,
			sizeof(pDevice->pProductInfo->szHWVersion));
		//TRACE("End4 Refresh PI_CONVERTOR_HW_VER %s\n",pDevice->pProductInfo->szHWVersion);

		//SW version
		RefreshItem(pDevice, PI_CONVERTOR_SW_VER,
			pDevice->pProductInfo->szSWVersion,
			sizeof(pDevice->pProductInfo->szSWVersion));
		//TRACE("End5 Refresh PI_CONVERTOR_SW_VER %s\n",pDevice->pProductInfo->szSWVersion);

		//Device Name, Thomas, 2006-2-14
		// for sig-based product info, use equip name as the device name


		strncpyz(pDevice->pProductInfo->szDeviceName[0],
			pDevice->ppRelatedEquip[0]->pEquipName->pFullName[0],
			sizeof(pDevice->pProductInfo->szDeviceName[0]));

		strncpyz(pDevice->pProductInfo->szDeviceName[1],
			pDevice->ppRelatedEquip[0]->pEquipName->pFullName[1],
			sizeof(pDevice->pProductInfo->szDeviceName[1]));

		strncpyz(pDevice->pProductInfo->szDeviceAbbrName[0],
			pDevice->ppRelatedEquip[0]->pEquipName->pAbbrName[0],
			sizeof(pDevice->pProductInfo->szDeviceAbbrName[0]));

		strncpyz(pDevice->pProductInfo->szDeviceAbbrName[1],
			pDevice->ppRelatedEquip[0]->pEquipName->pAbbrName[1],
			sizeof(pDevice->pProductInfo->szDeviceAbbrName[1]));



		//TRACE("End6 Refresh szDeviceName %s\n",pDevice->pProductInfo->szDeviceName);

	}
	//else
	//{
		//TRACE("pDevice->pProductInfo->bSigModelUsed is %d, Not used.\n",
		//	pDevice->pProductInfo->bSigModelUsed);
	//}

	//Get the device name based on the stdBasic config file
	//GetDeviceName(pDevice);
}
	

/*==========================================================================*
 * FUNCTION : DXI_GetPIByDeviceID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int           iDeviceID : 
 *            PRODUCT_INFO  *pPI      : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-11 16:53
 *==========================================================================*/
int DXI_GetPIByDeviceID(int iDeviceID, PRODUCT_INFO *pPI)
{
	DEVICE_INFO		*pDevice;
	PRODUCT_INFO	*pProductInfo;
	


	pDevice = DXI_GetDeviceByID(iDeviceID);
	
    if (!pDevice)
	{
		return ERR_DXI_INVALID_DEVICE_ID;
	}
    
	//refresh Product info
	RefreshProductInfo(pDevice);
	//TRACE("End RefreshProductInfo pDevice->pProductInfo is %d\n", pDevice->pProductInfo);

	//get the Product Info
	pProductInfo = pDevice->pProductInfo;
	strncpyz(pPI->szPartNumber, pProductInfo->szPartNumber,
		sizeof(pPI->szPartNumber));
	strncpyz(pPI->szSerialNumber, pProductInfo->szSerialNumber,
		sizeof(pPI->szSerialNumber));
	strncpyz(pPI->szHWVersion, pProductInfo->szHWVersion,
		sizeof(pPI->szHWVersion));
	strncpyz(pPI->szSWVersion, pProductInfo->szSWVersion,
		sizeof(pPI->szSWVersion));
	strncpyz(pPI->szDeviceName[0], pProductInfo->szDeviceName[0],
		sizeof(pPI->szDeviceName[0]));
	strncpyz(pPI->szDeviceName[1], pProductInfo->szDeviceName[1],
		sizeof(pPI->szDeviceName[1]));
	strncpyz(pPI->szDeviceAbbrName[0], pProductInfo->szDeviceAbbrName[0],
		sizeof(pPI->szDeviceAbbrName[0]));
	strncpyz(pPI->szDeviceAbbrName[1], pProductInfo->szDeviceAbbrName[1],
		sizeof(pPI->szDeviceAbbrName[1]));

	
	return ERR_DXI_OK;
}


/*==========================================================================*
 * FUNCTION : DXI_IsDeviceRelativeEquip
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iDeviceID : 
 *            int  iEquipID  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-11 16:53
 *==========================================================================*/
BOOL DXI_IsDeviceRelativeEquip(int iDeviceID, int iEquipID)
{
	int i;
	DEVICE_INFO	*pDevice;
	EQUIP_INFO	*pEquip;

	pDevice = DXI_GetDeviceByID(iDeviceID);
	if (!pDevice)
	{
		return FALSE;
	}

	for (i = 0; i < pDevice->nRelatedEquip; i++)
	{
		pEquip = pDevice->ppRelatedEquip[i];

		if (iEquipID == pEquip->iEquipID)
		{
			return TRUE;
		}
	}

	return FALSE;
}
BOOL DXI_IsEquipExist(int iEquipID)
{
 
	int		iBufLen = 0;
	int		iTimeOut = 0;		//Time out

	SIG_BASIC_VALUE* pSigValue;

	int iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 100);//Exist signal.

	int iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iEquipID,			
			iVarSubID,		
			&iBufLen,			
			&pSigValue,			
			iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		return (0 == pSigValue->varValue.enumValue) ? TRUE : FALSE;
	}
	else
	{
		return FALSE;
	}

}
int DXI_GetFirstRectDeviceID()
{
	int		iDevice = DXI_GetDeviceNum();
	int		i, iFirstRectID = 0, iEquipNum = 0, iBufLen;
		
	/*get equip information*/
	EQUIP_INFO		*pEquipInfo = NULL;

	int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
						0,			
						0,		
						&iBufLen,			
						&pEquipInfo,			
						0);
	
	/*get equip number*/
	int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
							0,			
							0,		
							&iBufLen,			
							(void *)&iEquipNum,			
							0);
	
	if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
	{
		return 0;
	}

	for(i = 0; i < iEquipNum && pEquipInfo; i++, pEquipInfo++ )
	{
		if(pEquipInfo->iEquipTypeID == RECT_STD_EQUIP_ID)
		{
			iFirstRectID = pEquipInfo->iEquipID;
			break;
		}
	}

	if(!iFirstRectID)
	{
		return 0;
	}

	for(i = 1; i <= iDevice; i++)
	{
		if (DXI_IsDeviceRelativeEquip(i, iFirstRectID))
		{
			return i;
		}
	}
	return 0;
}

int	DXI_GetRectRealNumber()
{
	//Get Rectifier number
	SIG_BASIC_VALUE* pSigValue;
	int		iVarRectID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8);
	int		iRectLen = 0;
	int		iRectGroupID = 0;
	EQUIP_INFO		*pEquipInfo = NULL;

	int iError = DxiGetData(VAR_ACU_EQUIPS_LIST,
						0,			
						0,		
						&iRectLen,			
						&pEquipInfo,			
						0);
	if(iError == ERR_DXI_OK)
	{
		while(pEquipInfo != NULL)
		{
			if(pEquipInfo->iEquipTypeID == RECT_GROUP_STD_EQUIP_ID)
			{
				iRectGroupID = pEquipInfo->iEquipID;
				break;
			}
			pEquipInfo++;
		}
	}

	if(!iRectGroupID)
	{
		return 0;
	}

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				iRectGroupID,	//Rectifier group ID		
				iVarRectID,		
				&iRectLen,			
				(void *)&pSigValue,			
				0);

	if (iError == ERR_DXI_OK)
	{
		return pSigValue->varValue.ulValue;
	}
	else
	{

		return 0;
	}
}


#endif //PRODUCT_INFO_SUPPORT

//Get the  g_SiteInfo.LoadCurrentData[]
static int GetLoadCurrentData(int   nVarID,			
				int   nVarSubID,		
				int*  pBufLen,			
				void* pDataBuf,			
				int   nTimeOut)
{

    UNUSED(nVarSubID);
    UNUSED(nTimeOut);


    *pBufLen = sizeof(int);

    *((HIS_DATA_RECORD_LOAD**)pDataBuf) = g_SiteInfo.pLoadCurrentData;

    return ERR_DXI_OK;
}   
//Set the  g_SiteInfo.LoadCurrentData[]
static int SetLoadCurrentData(int   nVarID,			
			      int   nVarSubID,		
			      int*  pBufLen,			
			      void* pDataBuf,			
			      int   nTimeOut)
{

    UNUSED(nVarSubID);
    UNUSED(nTimeOut);

    if (*pBufLen != sizeof(HIS_DATA_RECORD_LOAD))
    {
	return ERR_DXI_WRONG_BUFFER_LENGTH;
    }
    else
    {
	g_SiteInfo.pLoadCurrentData = (HIS_DATA_RECORD_LOAD*)pDataBuf;	
    }

    return ERR_DXI_OK;
}   

//Set the public configuration of ACU
static int SetACUPublicConfig(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut)
{
	int nError = ERR_DXI_OK;

	LANG_TEXT*	pLangName;
	char*		pModifyInfo;

	UNUSED(nTimeOut);

	pModifyInfo = (char*)pDataBuf;

	if (nVarID != SITE_SW_VERSION || nVarID != SITE_ID)
	{
		if (HAS_INVALID_CHAR(pModifyInfo))
		{
			return ERR_DXI_INVALID_MODIFY_NAME_BUF;
		}
	}

	if (Mutex_Lock(g_hMutexSetPublicConfig,DXI_LOCK_WAIT_TIMEOUT) 
		!= ERR_MUTEX_OK)
	{
		return ERR_DXI_MUTEX_WAIT_TIMEOUT;
	}	

	switch (nVarID)
	{

	case SITE_NAME:
		{
			pLangName = &(GetSiteInfo()->langSiteName);
			
			if (!(pLangName))
			{
				nError = ERR_DXI_INVALID_SITE_NAME_BUFFER;
				break;
			}

			switch (nVarSubID)
			{
			case MODIFY_SITE_ENGLISH_FULL_NAME:
				{

					if ((pLangName->pFullName[0]))
					{
						strncpyz((pLangName->pFullName[0]), 
							pModifyInfo,
							pLangName->iMaxLenForFull + 1);
						NotificationFunc(_WEB_SET_MASK,	// masks
							sizeof(int),				// the sizeof (EQUIP_INFO *)
							&nVarSubID,							// the equipment address ref.
							FALSE);		
						/*by HULONGWEN after modification, 
						should insert a record in the MainConfig.run*/
					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_NAME_BUF;
					}

					break;
				}
			case MODIFY_SITE_LOCAL_FULL_NAME:
				{
					if ((pLangName->pFullName[1]))
					{
						strncpyz((pLangName->pFullName[1]), 
							pModifyInfo,
							pLangName->iMaxLenForFull + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_NAME_BUF;
					}
				}
				break;

			case MODIFY_SITE_ENGLISH_ABBR_NAME:
				{

					if ((pLangName->pAbbrName[0]))
					{
						strncpyz((pLangName->pAbbrName[0]), 
							pModifyInfo,
							pLangName->iMaxLenForAbbr + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_NAME_BUF;
					}
				}
				break;

			case MODIFY_SITE_LOCAL_ABBR_NAME:
				{

					if ((pLangName->pAbbrName[1]))
					{
						strncpyz((pLangName->pAbbrName[1]), 
							pModifyInfo,
							pLangName->iMaxLenForAbbr + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_NAME_BUF;
					}
				}
				break;
                //////////////////////////////////////////////////////////////////////////
                //Added by wj for three languages 2006.5.11
            case MODIFY_SITE_LOCAL2_FULL_NAME:
                {
                   /* if ((pLangName->pFullName[2]))
                    {
                        strncpyz((pLangName->pFullName[2]), 
                            pModifyInfo,
                            pLangName->iMaxLenForFull + 1);


                    }
                    else*/
                    {
                        nError = ERR_DXI_INVALID_SITE_NAME_BUF;
                    }
                }
                break;

            case MODIFY_SITE_LOCAL2_ABBR_NAME:
                {

                    /*if ((pLangName->pAbbrName[2]))
                    {
                        strncpyz((pLangName->pAbbrName[2]), 
                            pModifyInfo,
                            pLangName->iMaxLenForAbbr + 1);


                    }
                    else*/
                    {
                        nError = ERR_DXI_INVALID_SITE_NAME_BUF;
                    }
                }
                break;

                //end////////////////////////////////////////////////////////////////////////
                

			default:
				{
					nError = ERR_DXI_INVALID_SITE_MODIFY_TYPE;
				}
				break;
			}


			if (nError == ERR_DXI_OK)
			{
				RUN_CONFIG_ITEM stRunConfigItem = DEF_RUN_CONFIG_ITEM(
					CONFIG_CHANGED_SITENAME,
					nVarSubID,
					pModifyInfo,
					0,//Not be used
					0,//Not be used
					0,//Not be used
					0);//Not be used

				WriteRunConfigItem(&stRunConfigItem);

			}

		}
		break;

	case SITE_SW_VERSION:
		{
			if (*pBufLen != sizeof(g_dwSWVersion))
			{
				nError = ERR_DXI_WRONG_BUFFER_LENGTH;
			}
			else
			{
				*pBufLen = sizeof(g_dwSWVersion);
				g_dwSWVersion = *((DWORD*)pDataBuf);
			}

		}
		break;

	case ACU_MANUFACTURER:
		{
			//because should include a '/0'
			if ((size_t)*pBufLen >= sizeof(g_szManufacturerName)
				|| strlen(pModifyInfo) != (size_t)*pBufLen)
			{
				nError = ERR_DXI_WRONG_BUFFER_LENGTH;
			}
			else
			{
				strncpyz(g_szManufacturerName, 
					(char*)pDataBuf,
					sizeof(g_szManufacturerName));

			}
		}
		break;

	case SITE_ID:
		{
			if (*pBufLen != sizeof(GetSiteInfo()->iSiteID))
			{
				nError = ERR_DXI_WRONG_BUFFER_LENGTH;
			}
			else
			{
				GetSiteInfo()->iSiteID = *((int*)pDataBuf);
			}

		}
		break;


	case SITE_LOCATION:
		{
			
			pLangName = &(GetSiteInfo()->langSiteLocation);
			
			if (!(pLangName))
			{
				nError = ERR_DXI_INVALID_SITE_LOCATION_BUF;
				break;
			}

			switch (nVarSubID)
			{
			case MODIFY_LOCATION_ENGLISH_FULL:
				{

					if ((pLangName->pFullName[0]))
					{
						strncpyz((pLangName->pFullName[0]), 
							pModifyInfo,
							pLangName->iMaxLenForFull + 1);

					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_LOCATION_BUF;
					}
					break;
				}
			case MODIFY_LOCATION_LOCAL_FULL:
				{
					if ((pLangName->pFullName[1]))
					{
						strncpyz((pLangName->pFullName[1]), 
							pModifyInfo,
							pLangName->iMaxLenForFull + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_LOCATION_BUF;
					}
				}
				break;

			case MODIFY_LOCATION_ENGLISH_ABBR:
				{

					if ((pLangName->pAbbrName[0]))
					{
						strncpyz((pLangName->pAbbrName[0]), 
							pModifyInfo,
							pLangName->iMaxLenForAbbr + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_LOCATION_BUF;
					}
				}
				break;

			case MODIFY_LOCATION_LOCAL_ABBR:
				{

					if ((pLangName->pAbbrName[1]))
					{
						strncpyz((pLangName->pAbbrName[1]), 
							pModifyInfo,
							pLangName->iMaxLenForAbbr + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_LOCATION_BUF;
					}
				}
				break;

                //////////////////////////////////////////////////////////////////////////
                //Added by wj for three languages 2006.5.11

            case MODIFY_LOCATION_LOCAL2_FULL:
                {
                    /*if ((pLangName->pFullName[2]))
                    {
                        strncpyz((pLangName->pFullName[2]), 
                            pModifyInfo,
                            pLangName->iMaxLenForFull + 1);


                    }
                    else*/
                    {
                        nError = ERR_DXI_INVALID_SITE_LOCATION_BUF;
                    }
                }
                break;

            case MODIFY_LOCATION_LOCAL2_ABBR:
                {

                    /*if ((pLangName->pAbbrName[2]))
                    {
                        strncpyz((pLangName->pAbbrName[2]), 
                            pModifyInfo,
                            pLangName->iMaxLenForAbbr + 1);


                    }
                    else*/
                    {
                        nError = ERR_DXI_INVALID_SITE_LOCATION_BUF;
                    }
                }
                break;


                //end////////////////////////////////////////////////////////////////////////
                

			default:
				{
					nError = ERR_DXI_INVALID_SITE_LOCATION_TYPE;
				}
				break;
			}

			if (nError == ERR_DXI_OK)
			{
				RUN_CONFIG_ITEM stRunConfigItem = DEF_RUN_CONFIG_ITEM(
					CONFIG_CHANGED_SITELOCATION,
					nVarSubID,
					pModifyInfo,
					0,//Not be used
					0,//Not be used
					0,//Not be used
					0);//Not be used

				WriteRunConfigItem(&stRunConfigItem);

			}

		}
		break;

	case SITE_DESCRIPTION:
		{
			pLangName = &(GetSiteInfo()->langDescription);
			
			if (!(pLangName))
			{
				nError = ERR_DXI_INVALID_SITE_DESCRIP_BUF;
				break;
			}
			switch (nVarSubID)
			{
			case MODIFY_DESCRIPTION_ENGLISH_FULL:
				{

					if ((pLangName->pFullName[0]))
					{
						strncpyz((pLangName->pFullName[0]), 
							pModifyInfo,
							pLangName->iMaxLenForFull + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_DESCRIP_BUF;
					}
					break;
				}
			case MODIFY_DESCRIPTION_LOCAL_FULL:
				{
					if ((pLangName->pFullName[1]))
					{
						strncpyz((pLangName->pFullName[1]), 
							pModifyInfo,
							pLangName->iMaxLenForFull + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_DESCRIP_BUF;
					}
				}
				break;

			case MODIFY_DESCRIPTION_ENGLISH_ABBR:
				{

					if ((pLangName->pAbbrName[0]))
					{
						strncpyz((pLangName->pAbbrName[0]), 
							pModifyInfo,
							pLangName->iMaxLenForAbbr + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_DESCRIP_BUF;
					}
				}
				break;

			case MODIFY_DESCRIPTION_LOCAL_ABBR:
				{

					if ((pLangName->pAbbrName[1]))
					{
						strncpyz((pLangName->pAbbrName[1]), 
							pModifyInfo,
							pLangName->iMaxLenForAbbr + 1);


					}
					else
					{
						nError = ERR_DXI_INVALID_SITE_DESCRIP_BUF;
					}
				}
				break;

                //////////////////////////////////////////////////////////////////////////
                //Added by wj for three languages 2006.5.11

            case MODIFY_DESCRIPTION_LOCAL2_FULL:
                {
                    if ((pLangName->pFullName[2]))
                    {
                        strncpyz((pLangName->pFullName[2]), 
                            pModifyInfo,
                            pLangName->iMaxLenForFull + 1);


                    }
                    else
                    {
                        nError = ERR_DXI_INVALID_SITE_DESCRIP_BUF;
                    }
                }
                break;

            case MODIFY_DESCRIPTION_LOCAL2_ABBR:
                {

                    /*if ((pLangName->pAbbrName[2]))
                    {
                        strncpyz((pLangName->pAbbrName[2]), 
                            pModifyInfo,
                            pLangName->iMaxLenForAbbr + 1);


                    }
                    else*/
                    {
                        nError = ERR_DXI_INVALID_SITE_DESCRIP_BUF;
                    }
                }
                break;


                //end////////////////////////////////////////////////////////////////////////
                

			default:
				{
					nError = ERR_DXI_INVALID_SITE_DESCRIP_TYPE;
				}
				break;
			}

			if (nError == ERR_DXI_OK)
			{
				RUN_CONFIG_ITEM stRunConfigItem = DEF_RUN_CONFIG_ITEM(
					CONFIG_CHANGED_SITEDESCRIPTION,
					nVarSubID,
					pModifyInfo,
					0,//Not be used
					0,//Not be used
					0,//Not be used
					0);//Not be used

				WriteRunConfigItem(&stRunConfigItem);

			}

		}
		break;

	default:
		{
			nError = ERR_DXI_INVALID_VARIABLE_DATA_ID;
		}
		break;

	}

	Mutex_Unlock(g_hMutexSetPublicConfig);

	return nError;
}             



//Get the ethernet number of ACU
#define ETHERNET_NUMBER_FILE	"/home/app_script/ethernet.log"

static int GetEthernetNum(void)
{
    FILE *in;

    char line [256];

    if (! (in = fopen (ETHERNET_NUMBER_FILE, "r")))
    {
	TRACEX("cannot open /home/app_script/ethernet.log - burps\n\r");
	return ETHERNET_NUMBER_1;
    }

    while (fgets (line, sizeof(line), in))
    {
	if (strstr(line, "eth:1")) 
	{
	    fclose(in);
	    return ETHERNET_NUMBER_1;
	}
	else if (strstr(line, "eth:2"))
	{
	    fclose(in);
	    return ETHERNET_NUMBER_2;
	}
	else
	{
	    fclose(in);
	    return ETHERNET_NUMBER_1;
	}
    }

    fclose(in);

    return ETHERNET_NUMBER_1;
}

//Get the network info of ACU
static int GetACUNetInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut)
{
	int nError = ERR_DXI_OK;
	int nIfNum = INDEX_ETH0;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	if (nVarID < NET_INFO_ALL || nVarID > NET_INFO_BROADCAST)
	{
		return ERR_DXI_INVALID_VARIABLE_DATA_ID;
	}

	//Two ethernet: 
	if(GetEthernetNum() == ETHERNET_NUMBER_2)
	{
	    nIfNum = INDEX_ETH1;
	}

	if (nVarID == NET_INFO_ALL)
	{
		if (*pBufLen != sizeof(ACU_NET_INFO))
		{
			return ERR_DXI_WRONG_BUFFER_LENGTH;
		}

		nError = GetNetworkInfo(nIfNum,
			(nVarID == NET_INFO_ALL),
			&(((ACU_NET_INFO*)pDataBuf)->ulIp),
			(nVarID == NET_INFO_ALL),
			&(((ACU_NET_INFO*)pDataBuf)->ulMask),
			(nVarID == NET_INFO_ALL),
			&(((ACU_NET_INFO*)pDataBuf)->ulGateway),
			(nVarID == NET_INFO_ALL),
			&(((ACU_NET_INFO*)pDataBuf)->ulBroardcast));
	}
	else
	{
		*pBufLen = sizeof(ULONG);

		nError = GetNetworkInfo(nIfNum,
			(nVarID == NET_INFO_IP) ? 1 : 0, (ULONG *)pDataBuf, 
			(nVarID == NET_INFO_NETMASK) ? 1 : 0, (ULONG *)pDataBuf, 
			(nVarID == NET_INFO_DEFAULT_GATEWAY) ? 1 : 0, (ULONG *)pDataBuf,
			(nVarID == NET_INFO_BROADCAST) ? 1 : 0, (ULONG *)pDataBuf);

	}

	return nError;
}	

//Set the network info of ACU
static int SetACUNetInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut)
{
	int nError = ERR_DXI_OK;
	int nIfNum = INDEX_ETH0;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);

	if (nVarID < NET_INFO_ALL || nVarID > NET_INFO_BROADCAST)
	{
		return ERR_DXI_INVALID_VARIABLE_DATA_ID;
	}

	if (Mutex_Lock(g_hMutexSetNetInfo,DXI_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_DXI_MUTEX_WAIT_TIMEOUT;
	}

	//Two ethernet: 
	if(GetEthernetNum() == ETHERNET_NUMBER_2)
	{
	    nIfNum = INDEX_ETH1;
	}

	if (nVarID == NET_INFO_ALL)
	{
		if (*pBufLen != sizeof(ACU_NET_INFO))
		{
			nError = ERR_DXI_WRONG_BUFFER_LENGTH;
		}
		else
		{
			nError = SetNetworkInfo(nIfNum,
				(nVarID == NET_INFO_ALL),
				((ACU_NET_INFO*)pDataBuf)->ulIp,
				(nVarID == NET_INFO_ALL),
				((ACU_NET_INFO*)pDataBuf)->ulMask,
				(nVarID == NET_INFO_ALL),
				((ACU_NET_INFO*)pDataBuf)->ulGateway,
				(nVarID == NET_INFO_ALL),
				((ACU_NET_INFO*)pDataBuf)->ulBroardcast);
			
			Sleep(250);

			nError = SetNetworkInfo(nIfNum,
				(nVarID == NET_INFO_ALL),
				((ACU_NET_INFO*)pDataBuf)->ulIp,
				(nVarID == NET_INFO_ALL),
				((ACU_NET_INFO*)pDataBuf)->ulMask,
				(nVarID == NET_INFO_ALL),
				((ACU_NET_INFO*)pDataBuf)->ulGateway,
				(nVarID == NET_INFO_ALL),
				((ACU_NET_INFO*)pDataBuf)->ulBroardcast);
		}
	}
	else
	{
		if (*pBufLen != sizeof(ULONG))
		{
			return ERR_DXI_WRONG_BUFFER_LENGTH;
		}
		else
		{
			nError = SetNetworkInfo(nIfNum,
				(nVarID == NET_INFO_IP) ? 1 : 0, 
				*(ULONG *)pDataBuf, 
				(nVarID == NET_INFO_NETMASK) ? 1 : 0, 
				*(ULONG *)pDataBuf, 
				(nVarID == NET_INFO_DEFAULT_GATEWAY) ? 1 : 0, 
				*(ULONG *)pDataBuf,
				(nVarID == NET_INFO_BROADCAST) ? 1 : 0, 
				*(ULONG *)pDataBuf);
		}

	}

	if (nError == ERR_DXI_OK)
	{
		//by HULONGWEN need to be restarted
	}

	Mutex_Unlock(g_hMutexSetNetInfo);

	return nError;
}     

/*==========================================================================*
* FUNCTION : GetFrontIPInfo
* PURPOSE  : Get the front port ip info of ACU
* CALLS    : GetNetworkInfo()
* CALLED BY:
* ARGUMENTS:
* RETURN   :
* COMMENTS : for CR - Improve the ability to determine a controller's IP address
* CREATOR  : Hrishikesh Oak
* DATE     : 2019/09/04
*==========================================================================*/
static int GetFrontIPInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut)
{
	int nError = ERR_DXI_OK;
	
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);
	
	//*pBufLen = 0;
	*pBufLen = sizeof(ULONG);
	//GetEthernetNum() returns the port for which IP has been assigned.
	//if ETHERNET_NUMBER_2 port returned then IB4 is present.
	//In this case, Front craft port has static IP. ELSE there will be NO static IP (return 0xffffffff to identify).
	//if(GetEthernetNum() == ETHERNET_NUMBER_2)
	//{
	    //*pBufLen = sizeof(ULONG);
	    
		nError = GetNetworkInfo(INDEX_ETH0,
			1, (ULONG *)pDataBuf, 
			0, (ULONG *)pDataBuf, 
			0, (ULONG *)pDataBuf,
			0, (ULONG *)pDataBuf);
	//}
	//else
	//{
	    //*((ULONG *)pDataBuf) = 0xffffffff;
	//}
	return nError;
}

/*==========================================================================*
* FUNCTION : GetVPNInfo
* PURPOSE  : Get the VPN info of ACU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   :  0-success  
* COMMENTS : 
* CREATOR  : 
* DATE     : 
*==========================================================================*/
#define NETWORK_VPN_FILE "/home/app_script/vpn_client.conf"

static int GetRealVPNInfo(int iDataType, ACU_VPN_INFO *pVPNInfo)
{
    FILE *in;
    BOOL bFoundFlag = FALSE;
    char line [256];
    char *pFind;
    int iNum, i;
    char ip_addr[20];

    /* 1. enable  */
    if (! (in = fopen ("/home/app_script/vpn.log", "r")))
    {
		TRACEX("cannot open /home/app_script/vpn.log - burps\n\r");
		pVPNInfo->iVPNEnable = APP_VPN_ERR;
		return APP_VPN_ERR;
    }

    pVPNInfo->iVPNEnable = APP_VPN_OFF;

    while (fgets (line, sizeof(line), in))
    {
	if (strstr(line, "vpn:no")) 
	{		
	    pVPNInfo->iVPNEnable = APP_VPN_OFF;
	    bFoundFlag = TRUE;
	    break;
	}
	else if (strstr(line, "vpn:yes"))
	{
	    pVPNInfo->iVPNEnable = APP_VPN_ON;
	    bFoundFlag = TRUE;
	    break;
	}	    
    }
    fclose(in);

    if(bFoundFlag == FALSE)
    {
	return APP_VPN_ERR;
    }

    /*2.vpn remote ip & port */
    if (! (in = fopen (NETWORK_VPN_FILE, "r")))
    {
	TRACEX("cannot open %s - burps\n\r", NETWORK_VPN_FILE);
	return APP_VPN_ERR;
    }	

    //seek the "remote" section
    bFoundFlag = FALSE;
    while((bFoundFlag == FALSE) && (fgets (line, sizeof(line), in)))
    {
	pFind = strstr(line, "remote");
	if (pFind != NULL) 
	{
	    bFoundFlag = TRUE;
	}
    }

    //found the "remote" section
    if(bFoundFlag == TRUE)
    {
	//2.1 seek ip-addr segment
	while((*pFind != 0x0D) && (*pFind != 0x0A) && (*pFind != 0) && (!((*pFind >= '0') && (*pFind <= '9'))))
	{
	    pFind++;
	}	    
	if((*pFind >= '0') && (*pFind <= '9')) //found ip-addr segment
	{
	    iNum = 0;
	    i = 0;

	    for(i = 0; i < sizeof(ip_addr); i++)
	    {
		if(((*pFind >= '0') && (*pFind <= '9')) || (*pFind == '.'))
		{
		    if(*pFind == '.')  
		    {
			iNum++;
		    }
		    ip_addr[i] = *pFind;
		    pFind++;
		}
		else
		{
		    break;	
		}
	    }
	    ip_addr[i] = 0;

	    if((iNum == 3) && (*(pFind -1) != '.'))  //the IP format is ok
	    {
		ip_addr[i] = 0;
		pVPNInfo->ulRemoteIp = inet_addr(ip_addr);

	    }
	    else
	    {
		fclose(in);   
		return APP_VPN_ERR;
	    }
	}  
	//END : 2.1 seek ip-addr segment

	//2.2 seek port segment
	while((*pFind != 0x0D) && (*pFind != 0x0A) && (*pFind != 0) && (!((*pFind >= '0') && (*pFind <= '9'))))
	{
	    pFind++;
	}
	if((*pFind >= '0') && (*pFind <= '9')) //found port segment
	{
	    pVPNInfo->iRemotePort = atoi(pFind);
	}
	else
	{
	    fclose(in);   
	    return APP_VPN_ERR;
	}
	////END : 2.2 seek port segment

	fclose(in);

	return APP_VPN_OFF;

    } // END: if(bFoundFlag == TRUE)
    else
    {
	fclose(in);
	return APP_VPN_ERR;
    }    
    //END: remote ip & port

}

static int GetVPNInfo(int   nVarID,			
			 int   nVarSubID,		
			 int*  pBufLen,			
			 void* pDataBuf,			
			 int   nTimeOut)
{
    int nError = ERR_DXI_OK;
    int nReturn;
    ACU_VPN_INFO szVPNinfo = {0};

    UNUSED(nVarSubID);
    UNUSED(nTimeOut);


    if (nVarID < VPN_INFO_ALL || nVarID > VPN_INFO_REMOTE_PORT)
    {
	return ERR_DXI_INVALID_VARIABLE_DATA_ID;
    }

    nReturn = GetRealVPNInfo(nVarID, &szVPNinfo);

    if(nReturn == APP_VPN_ERR)
    {
	//return APP_VPN_ERR;
	szVPNinfo.iVPNEnable = APP_VPN_OFF;
    }

    //vpn all
    if (nVarID == VPN_INFO_ALL)
    {	
	memcpy((char *)pDataBuf, (char *)(&szVPNinfo), sizeof(ACU_VPN_INFO));
	*pBufLen = sizeof(ACU_VPN_INFO);

    }
    //vpn enable
    else if (nVarID == VPN_INFO_ENABLE)
    {	
	*((int*)pDataBuf) = szVPNinfo.iVPNEnable;
	*pBufLen = sizeof(int);
	
    }
    //remote ip
    else if(nVarID == VPN_INFO_REMOTE_IP)
    {
	*((ULONG*)pDataBuf) = szVPNinfo.ulRemoteIp;
	*pBufLen = sizeof(ULONG);
    }
    //remote port
    else if(nVarID == VPN_INFO_REMOTE_PORT)
    {
	*((int*)pDataBuf) = szVPNinfo.iRemotePort;
	*pBufLen = sizeof(int);
    }

    return nError;
}	


/*==========================================================================*
* FUNCTION : SetVPNInfo
* PURPOSE  : Set the VPN info of ACU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   :  0-success  
* COMMENTS : 
* CREATOR  : 
* DATE     : 
*==========================================================================*/
//Set the VPN info (Remote IP & port ) of ACU
static int SetRealVPNInfo(ACU_VPN_INFO *pVPNInfo)
{
    FILE *in;

    char line [256];
    char *pFind, *pSrc, *pDst;
    long lFileLen;
    char szBuf[MAX_LINE_SIZE];
    long lCurPos, lPos_Remote;
    BOOL bFoundFlag = FALSE;
    struct in_addr	inIP;

    if (! (in = fopen (NETWORK_VPN_FILE, "r")))
    {
	TRACEX("cannot open %s - burps\n\r", NETWORK_VPN_FILE);
	return APP_VPN_ERR;
    }	

    //seek the "remote" section
    while((bFoundFlag == FALSE) && (fgets (line, sizeof(line), in)))
    {
	lCurPos = ftell( in );     // get cur pos
	pFind = strstr(line, "remote");
	if (pFind != NULL) 
	{
	    bFoundFlag = TRUE;
	}	
	else
	{
	    lPos_Remote = lCurPos;
	}
    }

    //COPY the contents befor "remote" to szBuf
    fseek( in, 0, SEEK_SET );       // move pointer to start
    fread(szBuf, sizeof(char), (size_t)lPos_Remote, in);

    //Write the new remote ip
    inIP.s_addr = pVPNInfo->ulRemoteIp;
    snprintf(line, sizeof(line), "remote %s %d\r\n", inet_ntoa(inIP),  pVPNInfo->iRemotePort);

    pSrc = line;
    pDst = &szBuf[lPos_Remote];
    while(*pSrc != '\0')
    {
	*pDst++ = *pSrc++;
    }


    //copy the remaining contents of the file to szBuf (Not include the "remote" line)
    fseek( in, lCurPos, SEEK_SET );       // move pointer to the remaining contents of the file
    lFileLen = GetFileLength(in);

    if(lFileLen >= (sizeof(szBuf) -1 - (pDst - szBuf) ))
    {
	lFileLen = (sizeof(szBuf) -1 - (pDst - szBuf));
    }

    lFileLen = fread(pDst, sizeof(char), (size_t)lFileLen, in);
    if (lFileLen < 0) 
    {
	fclose(in);   
	return APP_VPN_ERR;
    }

    *(pDst+lFileLen) = '\0';  // end with NULL

    fclose(in);   

    if (! (in = fopen (NETWORK_VPN_FILE, "w")))
    {
	TRACEX("cannot open %s - burps\n\r", NETWORK_VPN_FILE);
	return APP_VPN_ERR;
    }	

    //write the contents
    fwrite(szBuf, sizeof(char), ((pDst- szBuf) + lFileLen), in);

    fclose(in);  

    return ERR_DXI_OK;
}

static int SetVPNInfo(int   nVarID,			
			 int   nVarSubID,		
			 int*  pBufLen,			
			 void* pDataBuf,			
			 int   nTimeOut)
{
    int nError = ERR_DXI_OK;
    int nReturn;
    ACU_VPN_INFO szVPNinfo;

    UNUSED(nVarSubID);
    UNUSED(nTimeOut);

    //VPN all
    if (nVarID == VPN_INFO_ALL)  
    {
	if (*pBufLen != sizeof(ACU_VPN_INFO))
	{
	    return ERR_DXI_WRONG_BUFFER_LENGTH;
	}

	szVPNinfo.iVPNEnable = ((ACU_VPN_INFO *)pDataBuf)->iVPNEnable;
	szVPNinfo.iRemotePort = ((ACU_VPN_INFO *)pDataBuf)->iRemotePort;
	szVPNinfo.ulRemoteIp = ((ACU_VPN_INFO *)pDataBuf)->ulRemoteIp;

	//enable
	if( szVPNinfo.iVPNEnable == APP_VPN_OFF)//off
	{
		//system("/home/app_script/vpn.sh off");	    
		_SYSTEM("/home/app_script/vpn.sh off");
	}
	else if( szVPNinfo.iVPNEnable == APP_VPN_ON)//on
	{
		//system("/home/app_script/vpn.sh on");
		_SYSTEM("/home/app_script/vpn.sh on");
	    //remote ip & port
	    nReturn = SetRealVPNInfo(&szVPNinfo);

	    if(nReturn != ERR_DXI_OK)
	    {
		return ERR_DXI_INVALID_DATA_BUFFER;
	    }
	}
	else
	{
	    return ERR_DXI_INVALID_DATA_BUFFER;
	}

	

	return ERR_DXI_OK;
    }
    //VPN enable
    else if (nVarID == VPN_INFO_ENABLE)  
    {
	if (*pBufLen != sizeof(int))
	{
	    nError = ERR_DXI_WRONG_BUFFER_LENGTH;
	}
	else 
	{
	    if( *((int *)pDataBuf) == APP_VPN_OFF)//off
	    {
		//system("/home/app_script/vpn.sh off");
		_SYSTEM("/home/app_script/vpn.sh off");
		return ERR_DXI_OK;
	    }
	    else if( *((int *)pDataBuf) == APP_VPN_ON)//on
	    {
		//system("/home/app_script/vpn.sh on");
		_SYSTEM("/home/app_script/vpn.sh on");
		return ERR_DXI_OK;
	    }
	    else
	    {
		return ERR_DXI_INVALID_DATA_BUFFER;
	    }	

	}
    }
    //Remote IP & PORT
    else if((nVarID == VPN_INFO_REMOTE_IP) || (nVarID == VPN_INFO_REMOTE_PORT))
    {
	nReturn = GetRealVPNInfo(nVarID, &szVPNinfo);

	if(nReturn == APP_VPN_ERR)
	{
	    return APP_VPN_ERR;
	}

	if(nVarID == VPN_INFO_REMOTE_IP)
	{
	    if (*pBufLen != sizeof(ULONG))
	    {
		return ERR_DXI_WRONG_BUFFER_LENGTH;
	    }

	    szVPNinfo.ulRemoteIp = *(ULONG *)pDataBuf;
	}
	else
	{
	    if (*pBufLen != sizeof(int))
	    {
		return ERR_DXI_WRONG_BUFFER_LENGTH;
	    }

	    szVPNinfo.iRemotePort = *(int *)pDataBuf;
	}

	nReturn = SetRealVPNInfo(&szVPNinfo);

	if(nReturn == APP_VPN_ERR)
	{
	    return ERR_DXI_INVALID_DATA_BUFFER;
	}
    }
    
   
    return nError;
}  



//Get the IPV6 info of ACU
static int GetIPV6Info(int   nVarID,			
		      int   nVarSubID,		
		      int*  pBufLen,			
		      void* pDataBuf,			
		      int   nTimeOut)
{
    int nError = ERR_DXI_OK;
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);
	int iEthernetNum = GetEthernetNum();
	GetIPV6_NetworkInfo(iEthernetNum,pDataBuf);
    return nError;
}

//Set the IPV6 info of ACU
static int SetIPV6Info(int   nVarID,			
		      int   nVarSubID,		
		      int*  pBufLen,			
		      void* pDataBuf,			
		      int   nTimeOut)
{
    int nError = ERR_DXI_OK;
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);
	int iEthernetNum = GetEthernetNum();
	if (nVarID < IPV6_CHANGE_ADDR || nVarID > IPV6_CHANGE_ALL)
	{
		return ERR_DXI_INVALID_VARIABLE_DATA_ID;
	}

	if (Mutex_Lock(g_hMutexSetNetInfo,DXI_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_DXI_MUTEX_WAIT_TIMEOUT;
	}

	if(SetIPV6_NetworkInfo(iEthernetNum,nVarID,pDataBuf) != 0)
	{
		nError = ERR_DXI_INVALID_DATA_VALUE;
	}
	Mutex_Unlock(g_hMutexSetNetInfo);
    return nError;
}


/*==========================================================================*
* FUNCTION : GetSmtpConfig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:             
* RETURN   :
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
//#define SMTP_CONFIG_FILE   "/home/app_script/email.log"
#define SMTP_CONFIG_FILE   "private/email/mail.cfg"

static int GetSmtpConfig(ACU_SMTP_INFO * pSmtpCfgInfo)
{
    void    *pCfg;
    char    *szFileContents;
    FILE    *pFile;
    long    ulFileLen;

    char    szCfgFileName[MAX_FILE_PATH]; 


    Cfg_GetFullConfigPath(SMTP_CONFIG_FILE, 
	szCfgFileName, MAX_FILE_PATH);


    /* 1. read the file content to the buffer */
    pFile = fopen(szCfgFileName, "r");
    if (pFile == NULL)
    {
	/*AppLogOut("GetSmtpConfig", APP_LOG_WARNING, "Can not open the file: %s!\n",
	    SMTP_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: Can not open the file: %s!\n", __FILE__,
	    __FUNCTION__, SMTP_CONFIG_FILE);*/
	return ERR_CFG_FILE_OPEN;
    }

    fseek(pFile, 0, SEEK_END);
    ulFileLen = ftell(pFile);
    if (ulFileLen == -1)
    {
	/*AppLogOut("GetSmtpConfig", APP_LOG_WARNING, "Get Config file(%s) length error!\n",
	    SMTP_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: Get Config file(%s) length error!\n",
	    __FILE__, __FUNCTION__, SMTP_CONFIG_FILE);	*/

	fclose(pFile);
	return ERR_CFG_FAIL;
    }
    fseek(pFile, 0, SEEK_SET);


    szFileContents = NEW(char, ulFileLen + 1);
    if (szFileContents == NULL)
    { 
	/*AppLogOut("GetSmtpConfig", APP_LOG_WARNING, "out of memory on loading "
	    "file: %s!\n", SMTP_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: out of memory on loading file: %s!\n",
	    __FILE__, __FUNCTION__, SMTP_CONFIG_FILE);*/

	fclose(pFile);
	return ERR_CFG_NO_MEMORY;
    }

    ulFileLen = fread(szFileContents, sizeof(char), (size_t)ulFileLen, pFile);
    fclose(pFile);

    if (ulFileLen < 0) 
    {
	/*AppLogOut("GetSmtpConfig", APP_LOG_WARNING, "Read config file (%s) fail!\n",
	    SMTP_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: Read config file (%s) fail!\n",
	    __FILE__, __FUNCTION__, SMTP_CONFIG_FILE);*/

	DELETE(szFileContents);
	return ERR_CFG_FILE_READ;
    }
    szFileContents[ulFileLen] = 0;  // end with NULL

    /* 2. get SProfile structure */
    pCfg = Cfg_ProfileOpen( szFileContents, ulFileLen);
    if( pCfg == NULL )
    {
	/*AppLogOut("GetSmtpConfig", APP_LOG_WARNING, "Open config file: %s out"
	    "of memery.\n", SMTP_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: Open config file: %s out of memery.\n",
	    __FILE__, __FUNCTION__, SMTP_CONFIG_FILE);*/

	DELETE(szFileContents);
	return ERR_CFG_FAIL;
    }

    /* 3. load config file */
    char szLineData[MAX_LINE_SIZE];
    int  ret;

    //3.0   [EMAIL_SEND_TO]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SEND_TO]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/	
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else
    {
	sprintf(pSmtpCfgInfo->szEmailSendTo, "%s", szLineData);
    }	

    //3.1   [EMAIL_FROM]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_FROM]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else
    {
	sprintf(pSmtpCfgInfo->szEmailFrom, "%s", szLineData);
    }


    //3.2   [EMAIL_SERVER_IP]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SERVER_IP]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else
    {
	sprintf(pSmtpCfgInfo->szEmailSeverIP, "%s", szLineData);
    }

    //3.3   [EMAIL_SERVER_PORT]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SERVER_PORT]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR: EmailServerPort is "
	    "not a number!\n", __FILE__);*/
    }
    else
    {
	pSmtpCfgInfo->iEmailServerPort = atoi(szLineData);
    }

    //3.4   [EMAIL_ACCOUNT]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_ACCOUNT]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else 
    {
	sprintf(pSmtpCfgInfo->szEmailAccount, "%s", szLineData);
    }

    //3.5   [EMAIL_PASSWORD]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_PASSWORD]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else
    {
	sprintf(pSmtpCfgInfo->szEmailPasswd, "%s", szLineData);
    }

    //3.6   [EMAIL_AUTHEN]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_AUTHEN]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR: EMAIL_AUTHEN is "
	    "not a number!\n", __FILE__);*/
    }
    else 
    {
	pSmtpCfgInfo->iEmailAuthen = atoi(szLineData);
    }

    //3.7   [ALARM_LEVEL]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[ALARM_LEVEL]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR: ALARM_LEVEL is "
	    "not a number!\n", __FILE__);*/
    }
    else 
    {
	pSmtpCfgInfo->iAlarmLevel = atoi(szLineData);
    }


    if (pCfg != NULL)
    {
	Cfg_ProfileClose(pCfg);  //free the memery
    }    

   
    return ERR_CFG_OK;
}


/*==========================================================================*
* FUNCTION : GetSmtpInfo
* PURPOSE  : Get the Smtp info of ACU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   :  0-success  1-fail
* COMMENTS : 
* CREATOR  : 
* DATE     : 
*==========================================================================*/
static int GetSmtpInfo(int   nVarID,			
		      int   nVarSubID,		
		      int*  pBufLen,			
		      void* pDataBuf,			
		      int   nTimeOut)
{
    int nReturn;
    ACU_SMTP_INFO szSMTPinfo = {0};

    UNUSED(nVarSubID);
    UNUSED(nTimeOut);


    if (nVarID < SMTP_INFO_ALL || nVarID > SMTP_INFO_ALARM_LEVEL)
    {
	return ERR_DXI_INVALID_VARIABLE_DATA_ID;
    }

    nReturn = GetSmtpConfig(&szSMTPinfo);

    if(nReturn != ERR_CFG_OK)
    {
	return ERR_DXI_NOT_FIND_SIGNAL;
    }

    //all
    if(nVarID == SMTP_INFO_ALL)
    {
	memcpy((char *)pDataBuf, (char *)(&szSMTPinfo), sizeof(ACU_SMTP_INFO));
	*pBufLen = sizeof(ACU_SMTP_INFO);
    }
    //szEmailSendTo
    else if (nVarID == SMTP_INFO_SENDTO)
    {		
	strncpy((char *)pDataBuf, szSMTPinfo.szEmailSendTo, sizeof(szSMTPinfo.szEmailSendTo));
	*pBufLen = sizeof(szSMTPinfo.szEmailSendTo);

    }
    //szEmailFrom
    else if(nVarID == SMTP_INFO_FROM)
    {
	strncpy((char *)pDataBuf, szSMTPinfo.szEmailFrom, sizeof(szSMTPinfo.szEmailFrom));
	*pBufLen = sizeof(szSMTPinfo.szEmailFrom);
    }
    //szEmailSeverIP
    else if(nVarID == SMTP_INFO_SERVERIP)
    {
	strncpy((char *)pDataBuf, szSMTPinfo.szEmailSeverIP, sizeof(szSMTPinfo.szEmailSeverIP));
	*pBufLen = sizeof(szSMTPinfo.szEmailSeverIP);
    }
    //szEmailPasswd
    else if(nVarID == SMTP_INFO_PASSWORD)
    {
	strncpy((char *)pDataBuf, szSMTPinfo.szEmailPasswd, sizeof(szSMTPinfo.szEmailPasswd));
	*pBufLen = sizeof(szSMTPinfo.szEmailPasswd);
    }
    //szEmailAccount
    else if(nVarID == SMTP_INFO_ACCOUNT)
    {
	strncpy((char *)pDataBuf, szSMTPinfo.szEmailAccount, sizeof(szSMTPinfo.szEmailAccount));
	*pBufLen = sizeof(szSMTPinfo.szEmailAccount);
    }
    //iEmailServerPort
    else if(nVarID == SMTP_INFO_SERVERPORT)
    {
	*((int*)pDataBuf) = szSMTPinfo.iEmailServerPort;
	*pBufLen = sizeof(int);
    }
    //iEmailAuthen
    else if(nVarID == SMTP_INFO_AUTHEN)
    {
	*((int*)pDataBuf) = szSMTPinfo.iEmailAuthen;
	*pBufLen = sizeof(int);
    }
    //iAlarmLevel
    else if(nVarID == SMTP_INFO_ALARM_LEVEL)
    {
	*((int*)pDataBuf) = szSMTPinfo.iAlarmLevel;
	*pBufLen = sizeof(int);
    }

    return ERR_CFG_OK;
}

/*==========================================================================*
* FUNCTION : SetSmtpInfo
* PURPOSE  : Set the smtp info of ACU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   :  0-success 
* COMMENTS : 
* CREATOR  : 
* DATE     : 
*==========================================================================*/
static int SetRealSmtpInfo(const char *szDataKey,  char *WriteBuf)
{
    FILE *in;
    char line [256];
    char *pFind, *pSrc, *pDst;
    long lFileLen;
    char szBuf[MAX_LINE_SIZE] = {0};
    long lCurPos, lPos_Remote;
    BOOL bFoundFlag = FALSE;
    int i;
    char    szCfgFileName[MAX_FILE_PATH]; 


    Cfg_GetFullConfigPath(SMTP_CONFIG_FILE, 
	szCfgFileName, MAX_FILE_PATH);


    if (! (in = fopen (szCfgFileName, "r")))
    {	    
	return ERR_CFG_FAIL;
    }	

    //seek the section
    lPos_Remote = 0;
    while((bFoundFlag == FALSE) && (fgets (line, sizeof(line), in)))
    {
	lCurPos = ftell( in );     // get cur pos
	pFind = strstr(line, szDataKey);
	if (pFind != NULL) 
	{
	    bFoundFlag = TRUE;
	}		    
    }

    //copy the lines named "#" to the file
    for(i=0; i < 10; i++)
    {
	if(fgets (line, sizeof(line), in))
	{
	    pFind = strstr(line, "#");
	    if (pFind != NULL) 
	    {
		lCurPos = ftell( in );     // get cur pos
	    }   
	    else
	    {
		break;
	    }
	}
	else
	{
	    break;
	}
    }

    //COPY the contents befor key-section to szBuf
    fseek( in, 0, SEEK_SET );       // move pointer to start
    fread(szBuf, sizeof(char), (size_t)lCurPos, in);

    //Write the new content to the file
    snprintf(line, sizeof(line), "%s \r\n", WriteBuf);
    pSrc = line;
    pDst = &szBuf[lCurPos];
    while(*pSrc != '\0')
    {
	*pDst++ = *pSrc++;
    }

    //copy the remaining contents of the file to szBuf 
    bFoundFlag = FALSE;
    fseek( in, lCurPos, SEEK_SET );       // move pointer to the remaining contents of the file
    lPos_Remote = lCurPos;
    while((bFoundFlag == FALSE) && (fgets (line, sizeof(line), in)))
    {
	lCurPos = ftell( in );     // get cur pos
	pFind = strstr(line, "[");
	if (pFind != NULL) 
	{
	    bFoundFlag = TRUE;
	}	
	else
	{
	    lPos_Remote = lCurPos;
	}
    }

    fseek( in, lPos_Remote, SEEK_SET );       // move pointer to the remaining contents of the file
    lFileLen = GetFileLength(in);

    if(lFileLen >= (sizeof(szBuf) -1 - (pDst - szBuf) ))
    {
	lFileLen = (sizeof(szBuf) -1 - (pDst - szBuf));
    }

    lFileLen = fread(pDst, sizeof(char), (size_t)lFileLen, in);
    if (lFileLen < 0) 
    {
	fclose(in);   
	return SMS_INFOR_ERR;
    }

    *(pDst+lFileLen) = '\0';  // end with NULL

    fclose(in);   

    if (! (in = fopen (szCfgFileName, "w")))
    {
	TRACEX("cannot open %s - burps\n\r", SMTP_CONFIG_FILE);
	return ERR_CFG_FAIL;
    }	

    //write the contents
    fwrite(szBuf, sizeof(char), ((pDst- szBuf) + lFileLen), in);

    fclose(in);  
}

static int SetSmtpInfo(int   nVarID,			
		      int   nVarSubID,		
		      int*  pBufLen,			
		      void* pDataBuf,			
		      int   nTimeOut)
{ 
    FILE *in;    
    char WriteBuf[128] = {0};
    const char    *szDataKey;
    char    szCfgFileName[MAX_FILE_PATH]; 


    Cfg_GetFullConfigPath(SMTP_CONFIG_FILE, 
	szCfgFileName, MAX_FILE_PATH);


    // if the file is not exist, create it!
    if (! (in = fopen (szCfgFileName, "r")))
    {
	in = fopen (szCfgFileName, "w+");
	if(!in)
	{
		return;
	}
	fputs( "[EMAIL_SEND_TO]\r\n", in);
	fputs( "[EMAIL_FROM]\r\n", in);
	fputs( "[EMAIL_SERVER_IP]\r\n", in);
	fputs( "[EMAIL_SERVER_PORT]\r\n", in);
	fputs( "[EMAIL_ACCOUNT]\r\n", in);
	fputs( "[EMAIL_PASSWORD]\r\n", in);
	fputs( "[EMAIL_AUTHEN]\r\n", in);
	fputs( "[ALARM_LEVEL]\r\n", in);
	/* 20130708 lkf The below contents Reserve
	fputs( "[EMAIL_SMS_SEND_TYPE]\r\n", in);
	fputs( "#0-->ETH\r\n", in);
	fputs( "#1-->SMS GPRS\r\n", in);
	fputs( "[EMAIL_SMS_SMTP_AUTHEN]\r\n", in);
	fputs( "#0-->Do not need authen\r\n", in);
	fputs( "#1-->Need\r\n", in);
	fputs( "[EMAIL_SMS_SMTP_SERVER_IP]\r\n", in);
	fputs( "[EMAIL_SMS_SMTP_SERVER_PORT]\r\n", in);
	fputs( "[EMAIL_SMS_SMTP_FROM]\r\n", in);
	fputs( "[EMAIL_SMS_SMTP_USER]\r\n", in);
	fputs( "[EMAIL_SMS_SMTP_PASSWORD]\r\n", in);
	fputs( "[EMAIL_SMS_SMTP_RECIPIENT_FORMAT]\r\n", in);
	fputs( "#Example: 123456@123.com\r\n", in);
	fputs( "[EMAIL_GPRS_SETTING]\r\n", in);
	fputs( "#Example: 460900,n,8,1\r\n", in);
	*/

	fclose(in);   
    }	
	
    // save all the settings to the file
    if(nVarID == SMTP_INFO_ALL) 
    {

	strcpy(WriteBuf, ((ACU_SMTP_INFO *)pDataBuf)->szEmailSendTo); 
	SetRealSmtpInfo("[EMAIL_SEND_TO]", WriteBuf);

	memset(WriteBuf, 0, sizeof(WriteBuf));	
	strcpy(WriteBuf, ((ACU_SMTP_INFO *)pDataBuf)->szEmailFrom); 
	SetRealSmtpInfo("[EMAIL_FROM]", WriteBuf);

	memset(WriteBuf, 0, sizeof(WriteBuf));	
	strcpy(WriteBuf, ((ACU_SMTP_INFO *)pDataBuf)->szEmailSeverIP); 
	SetRealSmtpInfo("[EMAIL_SERVER_IP]", WriteBuf);

	memset(WriteBuf, 0, sizeof(WriteBuf));	
	snprintf(WriteBuf, sizeof(WriteBuf), "%d", ((ACU_SMTP_INFO *)pDataBuf)->iEmailServerPort);
	SetRealSmtpInfo("[EMAIL_SERVER_PORT]", WriteBuf);
	    
	memset(WriteBuf, 0, sizeof(WriteBuf));	
	strcpy(WriteBuf, ((ACU_SMTP_INFO *)pDataBuf)->szEmailAccount); 
	SetRealSmtpInfo("[EMAIL_ACCOUNT]", WriteBuf);

	memset(WriteBuf, 0, sizeof(WriteBuf));	
	strcpy(WriteBuf, ((ACU_SMTP_INFO *)pDataBuf)->szEmailPasswd); 
	SetRealSmtpInfo("[EMAIL_PASSWORD]", WriteBuf);

	memset(WriteBuf, 0, sizeof(WriteBuf));	
	snprintf(WriteBuf, sizeof(WriteBuf), "%d", ((ACU_SMTP_INFO *)pDataBuf)->iEmailAuthen);
	SetRealSmtpInfo("[EMAIL_AUTHEN]", WriteBuf);

	memset(WriteBuf, 0, sizeof(WriteBuf));	
	snprintf(WriteBuf, sizeof(WriteBuf), "%d", ((ACU_SMTP_INFO *)pDataBuf)->iAlarmLevel);
	SetRealSmtpInfo("[ALARM_LEVEL]", WriteBuf);
    }
    // save the alone settings to the file
    else if((nVarID >= SMTP_INFO_SENDTO) && (nVarID <= SMTP_INFO_ALARM_LEVEL))
    {
	if(nVarID == SMTP_INFO_SENDTO)
	{
	    szDataKey = "[EMAIL_SEND_TO]";	
	    strcpy(WriteBuf, (char *)pDataBuf); 
	}
	else if(nVarID == SMTP_INFO_FROM)
	{
	    szDataKey = "[EMAIL_FROM]";	
	    strcpy(WriteBuf, (char *)pDataBuf); 
	}
	else if(nVarID == SMTP_INFO_SERVERIP)
	{
	    szDataKey = "[EMAIL_SERVER_IP]";
	    strcpy(WriteBuf, (char *)pDataBuf); 
	}
	else if(nVarID == SMTP_INFO_SERVERPORT)
	{
	    szDataKey = "[EMAIL_SERVER_PORT]";	
	    snprintf(WriteBuf, sizeof(WriteBuf), "%d", *(int *)pDataBuf);
	}
	else if(nVarID == SMTP_INFO_ACCOUNT)
	{
	    szDataKey = "[EMAIL_ACCOUNT]";	
	    strcpy(WriteBuf, (char *)pDataBuf); 
	}
	else if(nVarID == SMTP_INFO_PASSWORD)
	{
	    szDataKey = "[EMAIL_PASSWORD]";
	    strcpy(WriteBuf, (char *)pDataBuf); 
	}
	else if(nVarID == SMTP_INFO_AUTHEN)
	{
	    szDataKey = "[EMAIL_AUTHEN]";	
	    snprintf(WriteBuf, sizeof(WriteBuf), "%d", *(int *)pDataBuf);
	}
	else if(nVarID == SMTP_INFO_ALARM_LEVEL)
	{
	    szDataKey = "[ALARM_LEVEL]";	
	    snprintf(WriteBuf, sizeof(WriteBuf), "%d", *(int *)pDataBuf);
	}

	SetRealSmtpInfo(szDataKey, WriteBuf);
	fclose(in);

	/* copy below content to SetRealSmtpInfo()
	if (! (in = fopen (SMTP_CONFIG_FILE, "r")))
	{	    
	    return ERR_CFG_FAIL;
	}	

	//seek the section
	lPos_Remote = 0;
	while((bFoundFlag == FALSE) && (fgets (line, sizeof(line), in)))
	{
	    lCurPos = ftell( in );     // get cur pos
	    pFind = strstr(line, szDataKey);
	    if (pFind != NULL) 
	    {
		bFoundFlag = TRUE;
	    }		    
	}
	
	//copy the lines named "#" to the file
	for(i=0; i < 10; i++)
	{
	    if(fgets (line, sizeof(line), in))
	    {
		pFind = strstr(line, "#");
		if (pFind != NULL) 
		{
		    lCurPos = ftell( in );     // get cur pos
		}   
		else
		{
		    break;
		}
	    }
	    else
	    {
		break;
	    }
	}

	//COPY the contents befor key-section to szBuf
	fseek( in, 0, SEEK_SET );       // move pointer to start
	fread(szBuf, sizeof(char), (size_t)lCurPos, in);

	//Write the new content to the file
	snprintf(line, sizeof(line), "%s \r\n", WriteBuf);
	pSrc = line;
	pDst = &szBuf[lCurPos];
	while(*pSrc != '\0')
	{
	    *pDst++ = *pSrc++;
	}

	//copy the remaining contents of the file to szBuf 
	bFoundFlag = FALSE;
	fseek( in, lCurPos, SEEK_SET );       // move pointer to the remaining contents of the file
	lPos_Remote = lCurPos;
	while((bFoundFlag == FALSE) && (fgets (line, sizeof(line), in)))
	{
	    lCurPos = ftell( in );     // get cur pos
	    pFind = strstr(line, "[");
	    if (pFind != NULL) 
	    {
		bFoundFlag = TRUE;
	    }	
	    else
	    {
		lPos_Remote = lCurPos;
	    }
	}

	fseek( in, lPos_Remote, SEEK_SET );       // move pointer to the remaining contents of the file
	lFileLen = GetFileLength(in);

	if(lFileLen >= (sizeof(szBuf) -1 - (pDst - szBuf) ))
	{
	    lFileLen = (sizeof(szBuf) -1 - (pDst - szBuf));
	}

	lFileLen = fread(pDst, sizeof(char), (size_t)lFileLen, in);
	if (lFileLen < 0) 
	{
	    fclose(in);   
	    return SMS_INFOR_ERR;
	}

	*(pDst+lFileLen) = '\0';  // end with NULL

	fclose(in);   

	if (! (in = fopen (SMTP_CONFIG_FILE, "w")))
	{
	    TRACEX("cannot open %s - burps\n\r", SMTP_CONFIG_FILE);
	    return ERR_CFG_FAIL;
	}	

	//write the contents
	fwrite(szBuf, sizeof(char), ((pDst- szBuf) + lFileLen), in);

	fclose(in);  
	*/
    }


    return ERR_CFG_OK;
}  
/*==========================================================================*
* FUNCTION : DXI_GetPhoneNumber
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int       iIndex        : ²Î¿¼enum eCfgPhoneNum
*            OUT char  *pPhoneNumber : ÊÖ»úºÅÂë
* RETURN   : BOOL:  0-success 1-fail
* COMMENTS : 
* CREATOR  : 
* DATE: 
*==========================================================================*/
//#define SMS_PHONE_NUM_FILE "/home/app_script/sms_phone.log"
#define SMS_PHONE_NUM_FILE "/private/phonems/sms.cfg"

static int GetSMSConfig(SMS_PHONE_INFO * pSMSCfgInfo)
{
    void    *pCfg;
    char    *szFileContents;
    FILE    *pFile;
    long    ulFileLen;
    char    szCfgFileName[MAX_FILE_PATH]; 

    Cfg_GetFullConfigPath(SMS_PHONE_NUM_FILE, 
	szCfgFileName, MAX_FILE_PATH);


    /* 1. read the file content to the buffer */
    pFile = fopen(szCfgFileName, "r");
    if (pFile == NULL)
    {
	/*
	AppLogOut("GetSMSConfig", APP_LOG_WARNING, "Can not open the file: %s!\n",
	    SMS_PHONE_NUM_FILE);
	TRACE("[%s]--%s: ERROR: Can not open the file: %s!\n", __FILE__,
	    __FUNCTION__, SMS_PHONE_NUM_FILE);
	*/
	return ERR_CFG_FILE_OPEN;
    }

    fseek(pFile, 0, SEEK_END);
    ulFileLen = ftell(pFile);
    if (ulFileLen == -1)
    {
	/*
	AppLogOut("GetSMSConfig", APP_LOG_WARNING, "Get Config file(%s) length error!\n",
	    SMS_PHONE_NUM_FILE);
	TRACE("[%s]--%s: ERROR: Get Config file(%s) length error!\n",
	    __FILE__, __FUNCTION__, SMS_PHONE_NUM_FILE);	
	*/
	fclose(pFile);
	return ERR_CFG_FAIL;
    }
    fseek(pFile, 0, SEEK_SET);

    szFileContents = NEW(char, ulFileLen + 1);
    if (szFileContents == NULL)
    {
	/*
	AppLogOut("GetSMSConfig", APP_LOG_WARNING, "out of memory on loading "
	    "file: %s!\n", SMS_PHONE_NUM_FILE);
	TRACE("[%s]--%s: ERROR: out of memory on loading file: %s!\n",
	    __FILE__, __FUNCTION__, SMS_PHONE_NUM_FILE);
	*/
	fclose(pFile);
	return ERR_CFG_NO_MEMORY;
    }

    ulFileLen = fread(szFileContents, sizeof(char), (size_t)ulFileLen, pFile);
    fclose(pFile);

    if (ulFileLen < 0) 
    {
	/*
	AppLogOut("GetSMSConfig", APP_LOG_WARNING, "Read config file (%s) fail!\n",
	    SMS_PHONE_NUM_FILE);
	TRACE("[%s]--%s: ERROR: Read config file (%s) fail!\n",
	    __FILE__, __FUNCTION__, SMS_PHONE_NUM_FILE);
	*/
	DELETE(szFileContents);
	return ERR_CFG_FILE_READ;
    }
    szFileContents[ulFileLen] = 0;  // end with NULL

    /* 2. get SProfile structure */
    pCfg = Cfg_ProfileOpen( szFileContents, ulFileLen);
    if( pCfg == NULL )
    {
	/*
	AppLogOut("GetSMSConfig", APP_LOG_WARNING, "Open config file: %s out"
	    "of memery.\n", SMS_PHONE_NUM_FILE);
	TRACE("[%s]--%s: ERROR: Open config file: %s out of memery.\n",
	    __FILE__, __FUNCTION__, SMS_PHONE_NUM_FILE);
	*/
	DELETE(szFileContents);
	return ERR_CFG_FAIL;
    }

    /* 3. load config file */
    char szLineData[MAX_LINE_SIZE];
    int  ret;

    //3.0   [PHONE1]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[PHONE1]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*
	AppLogOut("PHONE1 LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);	
	*/
    }
    else if (ret == 0)
    {
	/*
	AppLogOut("PHONE1 LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);
	*/
    }
    else
    {
	sprintf(pSMSCfgInfo->szPhoneNumber1, "%s", szLineData);
    }	

    //3.1   [PHONE2]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[PHONE2]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*
	AppLogOut("PHONE2 LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);
	*/
    }
    else if (ret == 0)
    {
	/*
	AppLogOut("PHONE2 LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);
	*/
    }
    else
    {
	sprintf(pSMSCfgInfo->szPhoneNumber2, "%s", szLineData);
    }

    //3.2   [PHONE3]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[PHONE3]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*
	AppLogOut("PHONE3 LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);
	    */
    }
    else if (ret == 0)
    {
	/*
	AppLogOut("PHONE3 LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);
	    */
    }
    else
    {
	sprintf(pSMSCfgInfo->szPhoneNumber3, "%s", szLineData);
    }


    //3.3   [ALARM_LEVEL]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[ALARM_LEVEL]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*
	AppLogOut("ALARM_LEVEL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);
	    */
    }
    else if (ret == 0)
    {
	/*
	AppLogOut("ALARM_LEVEL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);
	    */
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*
	AppLogOut("ALARM_LEVEL LDR", APP_LOG_WARNING, 
	    "[%s]--LoadSMSConfigProc: ERROR: ALARM_LEVEL is "
	    "not a number!\n", __FILE__);
	    */
    }
    else
    {
	pSMSCfgInfo->iAlarmLevel = atoi(szLineData);
    }

    if (pCfg != NULL)
    {
	Cfg_ProfileClose(pCfg);  //free the memery
    }    


    return ERR_CFG_OK;
}


/*==========================================================================*
* FUNCTION : GetSMSConfig
* PURPOSE  : Get the SMS info of ACU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   :  0-success  1-fail
* COMMENTS : 
* CREATOR  : 
* DATE     : 
*==========================================================================*/
static int GetSMSInfo(int   nVarID,			
		       int   nVarSubID,		
		       int*  pBufLen,			
		       void* pDataBuf,			
		       int   nTimeOut)
{
    int nReturn;
    SMS_PHONE_INFO szSMSinfo = {0};

    UNUSED(nVarSubID);
    UNUSED(nTimeOut);


    if (nVarID < SMS_INFO_PHONE_ALL || nVarID > SMS_INFO_ALARM_LEVEL)
    {
	return ERR_DXI_INVALID_VARIABLE_DATA_ID;
    }

    nReturn = GetSMSConfig(&szSMSinfo);

    if(nReturn != ERR_CFG_OK)
    {
	return ERR_DXI_NOT_FIND_SIGNAL;
    }

    if(nVarID == SMS_INFO_PHONE_ALL)
    {
	memcpy((char *)pDataBuf, (char *)(&szSMSinfo), sizeof(SMS_PHONE_INFO));
	*pBufLen = sizeof(SMS_PHONE_INFO);
    }
    else if(nVarID == SMS_INFO_PHONE1)
    {	
	strncpy((char *)pDataBuf, szSMSinfo.szPhoneNumber1, sizeof(szSMSinfo.szPhoneNumber1));
	*pBufLen = sizeof(szSMSinfo.szPhoneNumber1);
    }
    else if(nVarID == SMS_INFO_PHONE2)
    {	
	strncpy((char *)pDataBuf, szSMSinfo.szPhoneNumber2, sizeof(szSMSinfo.szPhoneNumber2));
	*pBufLen = sizeof(szSMSinfo.szPhoneNumber2);
    }
    else if(nVarID == SMS_INFO_PHONE3)
    {	
	strncpy((char *)pDataBuf, szSMSinfo.szPhoneNumber3, sizeof(szSMSinfo.szPhoneNumber3));
	*pBufLen = sizeof(szSMSinfo.szPhoneNumber3);
    }
    else if(nVarID == SMS_INFO_ALARM_LEVEL)
    {	
	*(int *)pDataBuf = szSMSinfo.iAlarmLevel;
	*pBufLen = sizeof(int);
    }

    return ERR_CFG_OK;
}

/*==========================================================================*
* FUNCTION : SetRealSMSInfo
* PURPOSE  : Set the SMS info of ACU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   :  0-success 
* COMMENTS : 
* CREATOR  : 
* DATE     : 
*==========================================================================*/
static int SetRealSMSInfo(const char *szDataKey,  char *WriteBuf)
{
    FILE *in;
    char line [256];
    char *pFind, *pSrc, *pDst;
    long lFileLen;
    char szBuf[MAX_LINE_SIZE] = {0};
    long lCurPos, lPos_Remote;
    BOOL bFoundFlag = FALSE;
    int i;
    char    szCfgFileName[MAX_FILE_PATH]; 

    Cfg_GetFullConfigPath(SMS_PHONE_NUM_FILE, 
	szCfgFileName, MAX_FILE_PATH);

    if (! (in = fopen (szCfgFileName, "r")))
    {	    
	return ERR_CFG_FAIL;
    }	

    //seek the section
    lPos_Remote = 0;
    while((bFoundFlag == FALSE) && (fgets (line, sizeof(line), in)))
    {
	lCurPos = ftell( in );     // get cur pos
	pFind = strstr(line, szDataKey);
	if (pFind != NULL) 
	{
	    bFoundFlag = TRUE;
	}		    
    }

    //copy the lines named "#" to the file
    for(i=0; i < 10; i++)
    {
	if(fgets (line, sizeof(line), in))
	{
	    pFind = strstr(line, "#");
	    if (pFind != NULL) 
	    {
		lCurPos = ftell( in );     // get cur pos
	    }   
	    else
	    {
		break;
	    }
	}
	else
	{
	    break;
	}
    }

    //COPY the contents befor key-section to szBuf
    fseek( in, 0, SEEK_SET );       // move pointer to start
    fread(szBuf, sizeof(char), (size_t)lCurPos, in);

    //Write the new content to the file
    snprintf(line, sizeof(line), "%s \r\n", WriteBuf);
    pSrc = line;
    pDst = &szBuf[lCurPos];
    while(*pSrc != '\0')
    {
	*pDst++ = *pSrc++;
    }

    //copy the remaining contents of the file to szBuf 
    bFoundFlag = FALSE;
    fseek( in, lCurPos, SEEK_SET );       // move pointer to the remaining contents of the file
    lPos_Remote = lCurPos;
    while((bFoundFlag == FALSE) && (fgets (line, sizeof(line), in)))
    {
	lCurPos = ftell( in );     // get cur pos
	pFind = strstr(line, "[");
	if (pFind != NULL) 
	{
	    bFoundFlag = TRUE;
	}	
	else
	{
	    lPos_Remote = lCurPos;
	}
    }

    fseek( in, lPos_Remote, SEEK_SET );       // move pointer to the remaining contents of the file
    lFileLen = GetFileLength(in);

    if(lFileLen >= (sizeof(szBuf) -1 - (pDst - szBuf) ))
    {
	lFileLen = (sizeof(szBuf) -1 - (pDst - szBuf));
    }

    lFileLen = fread(pDst, sizeof(char), (size_t)lFileLen, in);
    if (lFileLen < 0) 
    {
	fclose(in);   
	return SMS_INFOR_ERR;
    }

    *(pDst+lFileLen) = '\0';  // end with NULL

    fclose(in);   

    if (! (in = fopen (szCfgFileName, "w")))
    {
	TRACEX("cannot open %s - burps\n\r", SMS_PHONE_NUM_FILE);
	return ERR_CFG_FAIL;
    }	

    //write the contents
    fwrite(szBuf, sizeof(char), ((pDst- szBuf) + lFileLen), in);

    fclose(in);  
}

static int SetSMSInfo(int   nVarID,			
		       int   nVarSubID,		
		       int*  pBufLen,			
		       void* pDataBuf,			
		       int   nTimeOut)
{ 
    FILE *in;
    char    szCfgFileName[MAX_FILE_PATH]; 
    char WriteBuf[128] = {0};
    const char    *szDataKey;

    Cfg_GetFullConfigPath(SMS_PHONE_NUM_FILE, 
	szCfgFileName, MAX_FILE_PATH);


    // if the file is not exist, create it!
    if (! (in = fopen (szCfgFileName, "r")))
    {
	in = fopen (szCfgFileName, "w+");
	if(!in)
	{
		return;
	}
	fputs( "[PHONE1]\r\n", in);
	fputs( "[PHONE2]\r\n", in);
	fputs( "[PHONE3]\r\n", in);
	fputs( "[ALARM_LEVEL]\r\n", in);
	fclose(in);   
    }	


    // save all the settings to the file
    if(nVarID == SMS_INFO_PHONE_ALL) 
    {
	if(*pBufLen != sizeof(SMS_PHONE_INFO))
	{
	    return ERR_DXI_WRONG_BUFFER_LENGTH;
	}    

	strcpy(WriteBuf, ((SMS_PHONE_INFO *)pDataBuf)->szPhoneNumber1); 
	SetRealSMSInfo("[PHONE1]", WriteBuf);


	memset(WriteBuf, 0, sizeof(WriteBuf));	
	strcpy(WriteBuf, ((SMS_PHONE_INFO *)pDataBuf)->szPhoneNumber2); 
	SetRealSMSInfo("[PHONE2]", WriteBuf);

	memset(WriteBuf, 0, sizeof(WriteBuf));	
	strcpy(WriteBuf, ((SMS_PHONE_INFO *)pDataBuf)->szPhoneNumber3); 
	SetRealSMSInfo("[PHONE3]", WriteBuf);

	memset(WriteBuf, 0, sizeof(WriteBuf));	
	snprintf(WriteBuf, sizeof(WriteBuf), "%d", ((SMS_PHONE_INFO *)pDataBuf)->iAlarmLevel);
	SetRealSMSInfo("[ALARM_LEVEL]", WriteBuf);

	
    }
    // save the alone settings to the file
    else if((nVarID >= SMS_INFO_PHONE1) && (nVarID <= SMS_INFO_ALARM_LEVEL))
    {
	if(nVarID == SMS_INFO_PHONE1)
	{
	    szDataKey = "[PHONE1]";	
	    strcpy(WriteBuf, (char *)pDataBuf); 
	}
	else if(nVarID == SMS_INFO_PHONE2)
	{
	    szDataKey = "[PHONE2]";	
	    strcpy(WriteBuf, (char *)pDataBuf); 
	}
	else if(nVarID == SMS_INFO_PHONE3)
	{
	    szDataKey = "[PHONE3]";
	    strcpy(WriteBuf, (char *)pDataBuf); 
	}
	else if(nVarID == SMS_INFO_ALARM_LEVEL)
	{
	    szDataKey = "[ALARM_LEVEL]";	
	    snprintf(WriteBuf, sizeof(WriteBuf), "%d", *(int *)pDataBuf);
	}
	

	SetRealSMSInfo(szDataKey, WriteBuf);

    }


    return ERR_CFG_OK;
}  

//Get the dhcp info of ACU
static int GetAppDHCPInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut)
{
	int nError = ERR_DXI_OK;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);

	//Sleep(2000);

	BOOL bIsIPV6 = (nVarID == DXI_VAR_ID_IPV6) ? TRUE : FALSE;

	*((int*)pDataBuf) = GetRealDHCPInfo(bIsIPV6);
	*pBufLen = sizeof(int);

	return nError;
}	
static int GetRealDHCPInfo(BOOL bIsIPV6)
{
	//by HULONGWEN add temporary
	FILE *in;

	char line [256];

	//added by Jimmy to recognize IPV6
	if(bIsIPV6)
	{
		in = fopen ("/home/app_script/dhcpc6.log", "r");
	}
	else
	{
		in = fopen ("/home/app_script/dhcp.log", "r");
	}

	if (! in)
	{
		TRACEX("cannot open /home/app_script/dhcp(or 6).log - burps\n\r");
		return APP_DCHP_ERR;
	}

	while (fgets (line, sizeof(line), in))
	{
		if (strstr(line, "dhcp:off")) 
		{
			fclose(in);
			return APP_DHCP_OFF;
 		}
 		else if ((strstr(line, "dhcp:yes"))
		    || (strstr(line, "dhcp:on")))
 		{
 			fclose(in);
 			return APP_DHCP_ON;
 		}
 		else
 		{
 			fclose(in);
 			return APP_DCHP_ERR;
 		}
	}

	fclose(in);

	return APP_DCHP_ERR;
}
//Set the DHCP info of ACU
static int SetAppDHCPInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut)
{
	int nError = ERR_DXI_OK;
	int iEthernetNum = ETHERNET_NUMBER_1;
	char szCmd[128]={0};
	char ifrname[16]={0};

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);

	//added by Jimmy 2014.8.8, select whether IPV4 or IPV6
	BOOL bIsIPV6 = (nVarID == DXI_VAR_ID_IPV6) ? TRUE : FALSE;
	int nDHCPType = *(int *)pDataBuf;

	//check ethernet number
	iEthernetNum = GetEthernetNum();

	if((GetRealDHCPServerInfo(bIsIPV6) == APP_DHCP_SERVER_ON) 
	    && (iEthernetNum == ETHERNET_NUMBER_2))
	{
	    sprintf(ifrname, "%s", "eth1");	    
	}
	else
	{
	    sprintf(ifrname, "%s", "eth0");
	}


	if (*pBufLen != sizeof(int))
	{
		nError = ERR_DXI_WRONG_BUFFER_LENGTH;
	}
	else 
	{
		if(nDHCPType  == APP_DHCP_OFF)//off
		{
			if(bIsIPV6)
			{
				//system("/home/app_script/udhcp.sh off");
				snprintf(szCmd, sizeof(szCmd), "/home/app_script/dhcpc6.sh off %s", ifrname);			
			}
			else
			{
				//system("/home/app_script/udhcp.sh off");
				snprintf(szCmd, sizeof(szCmd), "/home/app_script/udhcp.sh off %s", ifrname);			
			}

			_SYSTEM(szCmd);
			return ERR_DXI_OK;
		}
		else if(nDHCPType == APP_DHCP_ON)//on
		{
			if(bIsIPV6)
			{
				//system("/home/app_script/udhcp.sh on");
				snprintf(szCmd, sizeof(szCmd), "/home/app_script/dhcpc6.sh on %s", ifrname);			
			}
			else
			{
				//system("/home/app_script/udhcp.sh on");
				snprintf(szCmd, sizeof(szCmd), "/home/app_script/udhcp.sh on %s", ifrname);			
			}
			
			_SYSTEM(szCmd);
			return ERR_DXI_OK;
		}
		else
		{
			return ERR_DXI_INVALID_DATA_BUFFER;
		}	

	}

	return ERR_DXI_SET_APP_DHCP_INFO;
}  

/*==========================================================================*
* FUNCTION :  SetDHCPServerInfo
* PURPOSE  :  Set the DHCP server info of ACU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  :        
* DATE: 
*==========================================================================*/
#define DHCP_SERVER_CFG_FILE	"/home/app_script/dhcp_server.log"
#define DHCP_SERVER_CFG_FILE_V6	"/home/app_script/dhcpd6_server.log"
static int SetDHCPServerInfo(int   nVarID,			
			  int   nVarSubID,		
			  int*  pBufLen,			
			  void* pDataBuf,			
			  int   nTimeOut)
{
    int nError = ERR_DXI_OK;

    //UNUSED(nVarSubID);
    UNUSED(nTimeOut);
	BOOL bIsIPV6 = (nVarSubID == DXI_VAR_ID_IPV6) ? TRUE : FALSE;//ÅÐ¶ÏÊÇ·ñÎªIPV6
    if (*pBufLen != sizeof(int))
    {
		nError = ERR_DXI_WRONG_BUFFER_LENGTH;
    }
    else 
    {
	if( *((int *)pDataBuf) == APP_DHCP_SERVER_OFF)//off
	{
		if(!bIsIPV6)
		{
			//system("/home/app_script/udhcp_server.sh off");
			_SYSTEM("/home/app_script/udhcp_server.sh off");
		}
		else
		{
			//system("/home/app_script/udhcp_server.sh off");
			_SYSTEM("/home/app_script/dhcpd6_server.sh off");
		}
		return ERR_DXI_OK;
	}
	else if( *((int *)pDataBuf) == APP_DHCP_SERVER_ON)//on
	{
		if(!bIsIPV6)
		{
			//system("/home/app_script/udhcp_server.sh on");
			_SYSTEM("/home/app_script/udhcp_server.sh on");
		}
		else
		{
			//system("/home/app_script/udhcp_server.sh on");
			_SYSTEM("/home/app_script/dhcpd6_server.sh on");
		}
		return ERR_DXI_OK;
	}
	else
	{
	    return ERR_DXI_INVALID_DATA_BUFFER;
	}	

    }

    return ERR_DXI_SET_APP_DHCP_INFO;
}  
//Get the dhcp server info of ACU
static int GetRealDHCPServerInfo(BOOL bIsIPV6)
{
    FILE *in;

    char line [256];

	char cfg_file[128];

	if(!bIsIPV6)
	{
		sprintf(cfg_file,"%s",DHCP_SERVER_CFG_FILE);
	}
	else
	{
		sprintf(cfg_file,"%s",DHCP_SERVER_CFG_FILE_V6);
	}

    if (! (in = fopen (cfg_file, "r")))
    {
		TRACEX("cannot open /home/app_script/dhcp_server(or V6).log - burps\n\r");
		return APP_DCHP_ERR;
    }

    while (fgets (line, sizeof(line), in))
    {
	if (strstr(line, "udhcpd:off")) 
	{
	    fclose(in);
	    return APP_DHCP_SERVER_OFF;
	}
	else if ((strstr(line, "udhcpd:yes"))
	    || (strstr(line, "udhcpd:on")))
	{
	    fclose(in);
	    return APP_DHCP_SERVER_ON;
	}
	else
	{
	    fclose(in);
	    return APP_DCHP_SERVER_ERR;
	}
    }

    fclose(in);

    return APP_DCHP_SERVER_ERR;
}
static int GetDHCPServerInfo(int   nVarID,			
			  int   nVarSubID,		
			  int*  pBufLen,			
			  void* pDataBuf,			
			  int   nTimeOut)
{
   

    int iEthernetNum = ETHERNET_NUMBER_1;
    
    
    int nError = ERR_DXI_OK;

    //UNUSED(nVarSubID);
    UNUSED(nTimeOut);
	BOOL bIsIPV6 = (nVarSubID == DXI_VAR_ID_IPV6) ? TRUE : FALSE;//ÅÐ¶ÏÊÇ·ñÎªIPV6

    if (nVarID < DHCP_SERVER_INFO_ALL || nVarID > DHCP_SERVER_INFO_IP)
    {
		return ERR_DXI_INVALID_VARIABLE_DATA_ID;
    }

    //check ethernet number
    iEthernetNum = GetEthernetNum();
	int iDHCStatus = GetRealDHCPServerInfo(bIsIPV6);
	if(!bIsIPV6)
	{
		DHCP_SERVER_NET_INFO DHCPServerNet;
		ULONG ulTemp;
		if((iDHCStatus == APP_DHCP_SERVER_ON) && (iEthernetNum == ETHERNET_NUMBER_2))
		{
			DHCPServerNet.iDHCPServerEnable = APP_DHCP_SERVER_ON;

			nError = GetNetworkInfo(INDEX_ETH0,
				1, &(DHCPServerNet.ulIp), 
				0, &ulTemp, 
				0, &ulTemp, 
				0, &ulTemp);		
		}
		else
		{
			DHCPServerNet.iDHCPServerEnable = APP_DHCP_SERVER_OFF;
			DHCPServerNet.ulIp = 0;
		}
		if (nVarID == DHCP_SERVER_INFO_ALL)
		{
			memcpy((char *)pDataBuf, (char *)(&DHCPServerNet), sizeof(DHCP_SERVER_NET_INFO));
			*pBufLen = sizeof(DHCP_SERVER_NET_INFO);
		}
		else if (nVarID == DHCP_SERVER_INFO_ENABLE)
		{
			*((int*)pDataBuf) = DHCPServerNet.iDHCPServerEnable;
			*pBufLen = sizeof(int);
		}
		else if (nVarID == DHCP_SERVER_INFO_IP)  
		{	
			*pBufLen = sizeof(ULONG);
			*(ULONG *)pDataBuf = DHCPServerNet.ulIp;  
		}
	}
	else
	{
		//printf("\n----------GGGGGGGGGGGGGGGGGGeting IPV6 DHCP ServIP\n");
		ACU_V6_NET_INFO	ServerIPInfo;
		DHCP_SERVER_NET_INFO_IPV6 DHCPServerIPV6;
		if((iDHCStatus == APP_DHCP_SERVER_ON) && (iEthernetNum == ETHERNET_NUMBER_2))
		{
			DHCPServerIPV6.iDHCPServerEnable = APP_DHCP_SERVER_ON;
			nError = GetIPV6_NetworkInfo(INDEX_ETH0,&ServerIPInfo);
			memcpy(&DHCPServerIPV6.stServerAddr,&ServerIPInfo.stGlobalAddr,sizeof(IN6_IFREQ));
		}
		else
		{
			DHCPServerIPV6.iDHCPServerEnable = APP_DHCP_SERVER_OFF;
			memset(&DHCPServerIPV6.stServerAddr,0,sizeof(IN6_IFREQ));
		}
		if (nVarID == DHCP_SERVER_INFO_ALL)
		{
			memcpy((char *)pDataBuf, (char *)(&DHCPServerIPV6), sizeof(DHCP_SERVER_NET_INFO_IPV6));
			*pBufLen = sizeof(DHCP_SERVER_NET_INFO_IPV6);
		}
		else if (nVarID == DHCP_SERVER_INFO_ENABLE)
		{
			*((int*)pDataBuf) = DHCPServerIPV6.iDHCPServerEnable;
			*pBufLen = sizeof(int);
		}
		else if (nVarID == DHCP_SERVER_INFO_IP)  
		{	
			*pBufLen = sizeof(IN6_IFREQ);
			memcpy((char *)pDataBuf, (char *)(&DHCPServerIPV6.stServerAddr), sizeof(IN6_IFREQ));
		}
	}

    return nError;
}	

//Get the time server info
static int GetTimeSrv(int   nVarID,			
					  int   nVarSubID,		
					  int*  pBufLen,			
					  void* pDataBuf,			
					  int   nTimeOut)
{
	time_t curTime;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	switch (nVarID)
	{
	case TIME_SRV_CONFIG:
		{
			*pBufLen = sizeof(TIME_SRV_INFO);
			*((TIME_SRV_INFO**)pDataBuf) = &g_sTimeSrvInfo;
		}
		break;

	case TIME_SRV_IP:
		{
			*pBufLen = sizeof(g_sTimeSrvInfo.ulMainTmSrvAddr);
			*((unsigned long*)pDataBuf) = g_sTimeSrvInfo.ulMainTmSrvAddr;
			break;
		}
		break;

	case TIME_SRV_BACKUP_IP:
		{
			*pBufLen = sizeof(g_sTimeSrvInfo.ulBakTmSrvAddr);
			*((unsigned long*)pDataBuf) = g_sTimeSrvInfo.ulBakTmSrvAddr;
		}
		break;

	case TIME_SYNC_INTERVAL:
		{
			*pBufLen = sizeof(g_sTimeSrvInfo.dwJustTimeInterval);
			*((DWORD*)pDataBuf) = g_sTimeSrvInfo.dwJustTimeInterval;
		}
		break;

	case SYSTEM_TIME:
		{
			time(&curTime);

			*pBufLen = sizeof(curTime);

			memcpy(pDataBuf, &curTime, (size_t)*pBufLen);
		}
		break;

	default:
		{
			return ERR_DXI_INVALID_VARIABLE_DATA_ID;
		}
		break;
	}


	return ERR_DXI_OK;
}	

//Set the time server info
static int SetTimeSrv(int   nVarID,			
					  int   nVarSubID,		
					  int*  pBufLen,			
					  void* pDataBuf,			
					  int   nTimeOut)
{
	int nError = ERR_DXI_OK;

	int nMergeSigID;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	if (Mutex_Lock(g_hMutexSetTimeSrv,DXI_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_DXI_MUTEX_WAIT_TIMEOUT;
	}

	switch (nVarID)
	{
	case TIME_SRV_IP:
		{
			if (*pBufLen != sizeof(g_sTimeSrvInfo.ulMainTmSrvAddr))
			{
				nError = ERR_DXI_WRONG_BUFFER_LENGTH;
			}
			else
			{
				g_sTimeSrvInfo.ulMainTmSrvAddr = *((unsigned long*)pDataBuf);

				nMergeSigID = SIG_ID_TIME_SYNC_SVR_IP;
			}

		}
		break;

	case TIME_SRV_BACKUP_IP:
		{
			if (*pBufLen != sizeof(g_sTimeSrvInfo.ulBakTmSrvAddr))
			{
				nError = ERR_DXI_WRONG_BUFFER_LENGTH;
			}
			else
			{
				g_sTimeSrvInfo.ulBakTmSrvAddr = *((unsigned long*)pDataBuf);

				nMergeSigID = SIG_ID_TIME_SYNC_BACKUP_IP;
			}

		}
		break;

	case TIME_SYNC_INTERVAL:
		{
			if (*pBufLen != sizeof(g_sTimeSrvInfo.dwJustTimeInterval))
			{
				nError = ERR_DXI_WRONG_BUFFER_LENGTH;
			}
			else
			{
				g_sTimeSrvInfo.dwJustTimeInterval = *((unsigned long*)pDataBuf);

				nMergeSigID = SIG_ID_TIME_SYNC_INTERVAL;
			}

		}
		break;

	case TIME_SYNC_TIMEZONE:
		{
			if (*pBufLen != sizeof(g_sTimeSrvInfo.lTimeZone))
			{
				nError = ERR_DXI_WRONG_BUFFER_LENGTH;
			}
			else
			{
				g_sTimeSrvInfo.lTimeZone = *((long*)pDataBuf);

				nMergeSigID = SIG_ID_TIME_SYNC_TIMEZONE;
			}

		}
		break;
	//Added to run NTP funcion immediately after setting by SongXu20180202.
	case TIME_SYNC_RUN_IMMEDIATELY:
		{
			nError = UpdateNTPTime( );
		}
		break;

	case SYSTEM_TIME:
		{

			if (*pBufLen != sizeof(time_t))
			{
				nError = ERR_DXI_WRONG_BUFFER_LENGTH;
			}
			else
			{
				//by HULONGWEN call UpdateSCUPTime() of Miscellaneous Module
				UpdateSCUPTime((time_t*)pDataBuf);
			}

		}
		break;

	default:
		{
			nError = ERR_DXI_INVALID_VARIABLE_DATA_ID;
		}
		break;
	}

//Save the time server config
	if (nVarID == TIME_SYNC_TIMEZONE && nError == ERR_DXI_OK)
	{
		VAR_VALUE_EX	varValueEx;

		memset(&varValueEx, 0, sizeof(varValueEx));

		varValueEx.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;

		varValueEx.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		varValueEx.pszSenderName = "";

		varValueEx.varValue.lValue = *((long*)pDataBuf);

		nError = DxiSetData(VAR_A_SIGNAL_VALUE,
			DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
			nMergeSigID,		
			sizeof(VAR_VALUE_EX),			
			&(varValueEx),			
			0);
	}
	//Save the time server config
	//TIME_SYNC_RUN_IMMEDIATELY unnecessary to change the signal
	else if (nVarID != SYSTEM_TIME && nError == ERR_DXI_OK && nVarID != TIME_SYNC_RUN_IMMEDIATELY)
	{
		VAR_VALUE_EX	varValueEx;

		memset(&varValueEx, 0, sizeof(varValueEx));

		varValueEx.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;

		varValueEx.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		varValueEx.pszSenderName = "";

		varValueEx.varValue.ulValue = *((unsigned long*)pDataBuf);

		nError = DxiSetData(VAR_A_SIGNAL_VALUE,
			DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
			nMergeSigID,		
			sizeof(VAR_VALUE_EX),			
			&(varValueEx),			
			0);
	}

	Mutex_Unlock(g_hMutexSetTimeSrv);

	return nError;
}   

//Get the number of samples
static int GetSamplersNum(int   nVarID,			
						  int   nVarSubID,		
						  int*  pBufLen,			
						  void* pDataBuf,			
						  int   nTimeOut)
{
	UNUSED(nVarID);
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	*pBufLen = sizeof(GetSiteInfo()->iSamplerNum);

	*((int*)pDataBuf) = GetSiteInfo()->iSamplerNum;

	return ERR_DXI_OK;
}       

//Get the list of samples
static int GetSamplersList(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut)
{
	UNUSED(nVarID);
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	*pBufLen = (GetSiteInfo()->iSamplerNum) * sizeof(SAMPLER_INFO);

	*((SAMPLER_INFO**)pDataBuf) = GetSiteInfo()->pSamplerInfo;

	return ERR_DXI_OK;
}  

//Get the number of standard equipments
static int GetStdEquipsNum(int   nVarID,			
						  int   nVarSubID,		
						  int*  pBufLen,			
						  void* pDataBuf,			
						  int   nTimeOut)
{
	UNUSED(nVarID);
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	*pBufLen = sizeof(GetSiteInfo()->iStdEquipTypeNum);

	*((int*)pDataBuf) = GetSiteInfo()->iStdEquipTypeNum;

	return ERR_DXI_OK;
}       

//Get the list of standard equipments
static int GetStdEquipsList(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut)
{
	UNUSED(nVarID);
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);


	*pBufLen = (GetSiteInfo()->iStdEquipTypeNum) * sizeof(STDEQUIP_TYPE_INFO);

	*((STDEQUIP_TYPE_INFO**)pDataBuf) = GetSiteInfo()->pStdEquipTypeInfo;

	return ERR_DXI_OK;
}  

//Get a signal structure via standard equipment ID
static int GetASignalInfoViaStdEquip(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut)
{
	int	  nSigInfosLength;
	void* pSigInfo;

	int	  nSigID;
	int   nSigType;

	int   nSigPosition;

	int	  nError = ERR_DXI_OK;

	int		i;

	int						iStdEquipTypeNum;	 
	STDEQUIP_TYPE_INFO 		*pStdEquipTypeInfo;

	UNUSED(nTimeOut);


	nSigInfosLength = 0;

	DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);

	iStdEquipTypeNum  = GetSiteInfo()->iStdEquipTypeNum;
	pStdEquipTypeInfo = GetSiteInfo()->pStdEquipTypeInfo;

	for (i = 0; i < iStdEquipTypeNum; i++, pStdEquipTypeInfo++)
	{
		if (pStdEquipTypeInfo->iTypeID == nVarID)
		{
			break;
		}
	}

	if (i < iStdEquipTypeNum) //Find needed stdanard equipment
	{
		
	}
	else
	{
		return ERR_DXI_INVALID_VARIABLE_DATA_ID;
	}

	switch (nSigType)
	{
	case SIG_TYPE_SAMPLING:
		{
			nSigInfosLength = sizeof(SAMPLE_SIG_INFO) * 
				pStdEquipTypeInfo->iSampleSigNum;

			pSigInfo = pStdEquipTypeInfo->pSampleSigInfo;

			*pBufLen = sizeof(SAMPLE_SIG_INFO);
		}
		break;

	case SIG_TYPE_CONTROL:	
		{
			nSigInfosLength = sizeof(CTRL_SIG_INFO) * 
				pStdEquipTypeInfo->iCtrlSigNum;

			pSigInfo = pStdEquipTypeInfo->pCtrlSigInfo;

			*pBufLen = sizeof(CTRL_SIG_INFO);
		}
		break;

	case SIG_TYPE_SETTING:		
		{
			nSigInfosLength = sizeof(SET_SIG_INFO) * 
				pStdEquipTypeInfo->iSetSigNum;

			pSigInfo = pStdEquipTypeInfo->pSetSigInfo;

			*pBufLen = sizeof(SET_SIG_INFO);
		}
		break;

	case SIG_TYPE_ALARM:		
		{			
			nSigInfosLength = sizeof(ALARM_SIG_INFO) * 
				pStdEquipTypeInfo->iAlarmSigNum;

			pSigInfo = pStdEquipTypeInfo->pAlarmSigInfo;

			*pBufLen = sizeof(ALARM_SIG_INFO);
		}
		break;

	default:
		{
			nError = ERR_DXI_INVALID_SIG_TYPE;
		}
		break;

	}

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	nSigPosition = *pBufLen;

	while (1)
	{
		if ((nSigPosition > nSigInfosLength) || !pSigInfo)
		{
			return ERR_DXI_NOT_FIND_SIGNAL;
		}

		//because the first item of pSigInfo is iSigID
		if ((*((int*)pSigInfo) == nSigID))
		{
			break;
		}

		switch (nSigType)
		{
		case SIG_TYPE_SAMPLING:
			{
				pSigInfo = ((SAMPLE_SIG_INFO*)pSigInfo)+1;
			}
			break;

		case SIG_TYPE_CONTROL:	
			{
				pSigInfo = ((CTRL_SIG_INFO*)pSigInfo) + 1;
			}
			break;

		case SIG_TYPE_SETTING:		
			{
				pSigInfo = ((SET_SIG_INFO*)pSigInfo) + 1;
			}
			break;

		case SIG_TYPE_ALARM:		
			{			
				pSigInfo = ((ALARM_SIG_INFO*)pSigInfo) + 1;
			}
			break;

		default:
			{
				return ERR_DXI_INVALID_SIG_TYPE;
			}
			break;

		}

		nSigPosition += *pBufLen;

	}

	*((void**)pDataBuf) = pSigInfo;

	return ERR_DXI_OK;
}	


//Set a signal structure var standard equipment ID
static int SetASignalInfoViaStdEquip(int   nVarID,			
							  int   nVarSubID,		
							  int*  pBufLen,			
							  void* pDataBuf,			
							  int   nTimeOut)
{
	int	  nSigID;
	int   nSigType;
	int	nEquipTypeID;
	int	i;

	int nSigInfoLen;
	void* pSigInfo;

	SET_A_SIGNAL_INFO_STU* pModifyInfo;
	int				iStdEquipTypeNum;	 
	STDEQUIP_TYPE_INFO 		*pStdEquipTypeInfo;
	

	int nError = ERR_DXI_OK;

	if (sizeof(SET_A_SIGNAL_INFO_STU) != *pBufLen)
	{
		return ERR_DXI_WRONG_BUFFER_LENGTH;
	}

	pModifyInfo = (SET_A_SIGNAL_INFO_STU*)pDataBuf;

	DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);

	if(nVarID > 300 && nVarID < 400 &&  pModifyInfo->bModifyAlarmLevel && nSigID <= 3) //If it is battery, it will change all battery unit's alarm level.
	{
	
		iStdEquipTypeNum  = GetSiteInfo()->iStdEquipTypeNum;
		pStdEquipTypeInfo = GetSiteInfo()->pStdEquipTypeInfo;

		for (i = 0; i < iStdEquipTypeNum; i++, pStdEquipTypeInfo++)
		{
			if ( pStdEquipTypeInfo->iTypeID > 300 && pStdEquipTypeInfo->iTypeID < 400)
			{
				nEquipTypeID = pStdEquipTypeInfo->iTypeID;
				nError = GetASignalInfoViaStdEquip(nEquipTypeID,			
						nVarSubID,		
						&nSigInfoLen,			
						&pSigInfo,
						nTimeOut);

				if (nError != ERR_DXI_OK)
				{
					return nError;
				}

				DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);

				nError += SetASignalModify(nEquipTypeID,
					nSigType,
					pSigInfo,
					pModifyInfo);
			}
		}
	
		return nError;
	}
	//else 
	nError = GetASignalInfoViaStdEquip(nVarID,			
		nVarSubID,		
		&nSigInfoLen,			
		&pSigInfo,
		nTimeOut);

	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	DXI_SPLIT_SIG_ID(nVarSubID, nSigType, nSigID);

	nError = SetASignalModify(nVarID,
		nSigType,
		pSigInfo,
		pModifyInfo);

	return nError;
}       


/*==========================================================================*
* FUNCTION : Cfg_GetStdPortByTypeId
* PURPOSE  : get the std port driver from config
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite       : 
*            IN int        nStdPortType : 
* RETURN   : STDPORT_INFO *: NULL for failure
* COMMENTS : 
* CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-02 14:18
*==========================================================================*/
STDPORT_INFO *Init_GetStdPortByTypeId(IN SITE_INFO *pSite, 
									  IN int nStdPortType)
{
	STDPORT_INFO	*pStdPort;
	int				n;

	if (nStdPortType <= 0)
	{
		return NULL;
	}

	// quikly search the ID. The ID start from 1.
	if (nStdPortType <= pSite->iStdPortNum)
	{
		pStdPort = &pSite->pStdPortInfo[nStdPortType-1];
		if (pStdPort->iPortTypeID == nStdPortType)
		{
			return pStdPort;
		}
	}

	// the ID is not equal to the INDEX, compare the ID one by one
	pStdPort = pSite->pStdPortInfo;
	for (n = 0; n < pSite->iStdPortNum; n++, pStdPort++)
	{
		if (pStdPort->iPortTypeID == nStdPortType)
		{
			return pStdPort;
		}
	}

	return NULL;	// not found.
}



/*==========================================================================*
* FUNCTION : *GetStandardPortDriverName
* PURPOSE  : 
* CALLS    : 
* CALLED BY: CommOpen.
* ARGUMENTS: IN int  nStdPortType : 
* RETURN   : char : 
* COMMENTS : 
* CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-13 13:03
*==========================================================================*/
char *GetStandardPortDriverName(IN int nStdPortType)
{
	STDPORT_INFO	*pStdPort;

	pStdPort = Init_GetStdPortByTypeId(
		GetSiteInfo(), nStdPortType);
	if (pStdPort == NULL)
	{
		return NULL;
	}


	return pStdPort->szPortAccessingDriver;
}

//Get a standard equipment info of ACU
static int GetAStdEquipInfo(int   nVarID,			
						 int   nVarSubID,		
						 int*  pBufLen,			
						 void* pDataBuf,			
						 int   nTimeOut)
{

	STDEQUIP_TYPE_INFO*	pStdEquip;

	int i;

	UNUSED(nVarSubID);
	UNUSED(nTimeOut);

	pStdEquip = GetSiteInfo()->pStdEquipTypeInfo;

	for(i = 0;i < GetSiteInfo()->iEquipNum; i++, pStdEquip++)
	{

		if (!pStdEquip)
		{

			TRACE("\n\r filename: data_exchange.c [GetEquipInfoByEquipID]\
				   pEquipInfo is not valid ");


			return ERR_DXI_INVALID_EQUIP_BUFFER;
		}
		if (pStdEquip->iTypeID == nVarID)
		{
			break;
		}
	}

	if (i >= GetSiteInfo()->iEquipNum)
	{
		return ERR_DXI_INVALID_STD_EQUIP_ID;
	}

	*pBufLen = sizeof(STDEQUIP_TYPE_INFO);

	*((STDEQUIP_TYPE_INFO**)pDataBuf) = pStdEquip;

	return ERR_DXI_OK;
}



/*==========================================================================*
* FUNCTION : *GetStandardPortDriverName
* PURPOSE  : 
* CALLS    : 
* CALLED BY: CommOpen.
* ARGUMENTS: IN int  nStdPortType : 
* RETURN   : char : 
* COMMENTS : 
* CREATOR  : WangJing         DATE: 2006-5-13 13:03
*==========================================================================*/
static int GetStdEquipTypeMapInfo(int   nVarID,			
                                  int   nVarSubID,		
                                  int*  pBufLen,			
                                  void* pDataBuf,			
                                  int   nTimeOut)
{

    UNUSED(nVarID);
    UNUSED(nVarSubID);
    UNUSED(nTimeOut);


    *pBufLen = GetSiteInfo()->iEquipTypeMapNum*sizeof(STDEQUIP_TYPEMAP_INFO);

    *((STDEQUIP_TYPEMAP_INFO**)pDataBuf) = GetSiteInfo()->pEquipTypeMap;

    return ERR_DXI_OK;
}



/*==========================================================================*
* FUNCTION : *GetStandardPortDriverName
* PURPOSE  : 
* CALLS    : 
* CALLED BY: CommOpen.
* ARGUMENTS: IN int  nStdPortType : 
* RETURN   : char : 
* COMMENTS : 
* CREATOR  : WangJing         DATE: 2006-5-13 13:03
*==========================================================================*/
static int GetStdEquipTypeMapNum(int   nVarID,			
                                  int   nVarSubID,		
                                  int*  pBufLen,			
                                  void* pDataBuf,			
                                  int   nTimeOut)
{
    UNUSED(nVarID);
    UNUSED(nVarSubID);
    UNUSED(nTimeOut);


    *pBufLen = sizeof(GetSiteInfo()->iEquipTypeMapNum);

    *((int*)pDataBuf) = GetSiteInfo()->iEquipTypeMapNum;

    return ERR_DXI_OK;
}

/*==========================================================================*
* FUNCTION : Compare_ALARM_SIG_VALUE
* PURPOSE  : Sort comare function
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: const ALARM_SIG_VALUE  *p1 : 
*            const ALARM_SIG_VALUE  *p2 : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:12
*==========================================================================*/
static int Compare_ALARM_SIG_VALUE(const ALARM_SIG_VALUE *p1,
								   const ALARM_SIG_VALUE *p2)
{
	if(p1->sv.tmStartTime < p2->sv.tmStartTime)
	{
		return 1;
	}

	if(p1->sv.tmStartTime > p2->sv.tmStartTime)
	{
		return -1;
	}

	if (p1->pEquipInfo->iEquipID < p2->pEquipInfo->iEquipID)
	{
		return 1;
	}

	if (p1->pEquipInfo->iEquipID > p2->pEquipInfo->iEquipID)
	{
		return -1;
	}

	if (p1->pStdSig->iSigID > p2->pStdSig->iSigID)
	{
		return 1;
	}

	if (p1->pStdSig->iSigID < p2->pStdSig->iSigID)
	{
		return -1;
	}

	return 0;
}



/*==========================================================================*
* FUNCTION : MakeAlarmSigValueSort
* PURPOSE  : Alarm signal value sort, use qsort
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: ALARM_SIG_VALUE*  pAlarmSigValue : 
*            int               nAlarmNum      : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:11
*==========================================================================*/
static void MakeAlarmSigValueSort(ALARM_SIG_VALUE* pAlarmSigValue, 
								  int nAlarmNum)
{
	qsort(pAlarmSigValue, (size_t)nAlarmNum, sizeof(*pAlarmSigValue),
		(int(*)(const void *, const void *))Compare_ALARM_SIG_VALUE);
}

void DXI_MakeAlarmSigValueSort(ALARM_SIG_VALUE* pAlarmSigValue, 
								  int nAlarmNum)
{
	MakeAlarmSigValueSort(pAlarmSigValue, nAlarmNum);
}

/*==========================================================================*
 * FUNCTION : DXI_GetActiveAlarmItems
 * PURPOSE  : Get active alarm items from equip-mon module
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int*               pAlarmNum             : Return alarm num
 *            ALARM_SIG_VALUE**  ppActiveAlarmSigValue : Return alarm buffer
 * RETURN   : int : 
 * COMMENTS : Need to delete the assigned memory
 * CREATOR  : HULONGWEN                DATE: 2005-03-09 10:41
 *==========================================================================*/
int DXI_GetActiveAlarmItems(int* pAlarmNum,
							ALARM_SIG_VALUE** ppActiveAlarmSigValue)
{
	int nError = ERR_SNP_OK;

	int					nSigNum = 0;
	ALARM_SIG_VALUE*	pActiveAlarmSigValue = NULL;

	int			nBufLen;

	*pAlarmNum = 0;
	*ppActiveAlarmSigValue = NULL;

	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
		ALARM_LEVEL_NONE,			
		0,		
		&nBufLen,			
		&nSigNum,			
		0);

	if(ERR_DXI_OK != nError)
	{
		return nError;
	}

//	TRACE("\n*** Active Alarm Num is %d.***\n", nSigNum);

	if(nSigNum)
	{
		pActiveAlarmSigValue = NEW(ALARM_SIG_VALUE, nSigNum);

		/*TRACE("\n\n[%s--%d--%s] - ALARM_SIG_VALUE size is %d. Total is %d.\n\n",
			__FILE__,
			__LINE__,
			__FUNCTION__,
			sizeof(ALARM_SIG_VALUE),
			nSigNum * sizeof(ALARM_SIG_VALUE));*/

		if (pActiveAlarmSigValue == NULL)
		{
			AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
				"[%s]--%s: "
				"nSigNum = %d, There is no enough memory! ", 
				__FILE__, __FUNCTION__, nSigNum);

			return ERR_NO_MEMORY;
		}

		nBufLen = sizeof(ALARM_SIG_VALUE) * nSigNum;

		nError = DxiGetData(VAR_ACTIVE_ALARM_INFO,
			ALARM_LEVEL_NONE,			
			0,		
			&nBufLen,			
			pActiveAlarmSigValue,			
			0);

		if(ERR_DXI_OK != nError)
		{

			DELETE(pActiveAlarmSigValue);

			pActiveAlarmSigValue = NULL;

			return nError;
		}

		//Get actual AlarmNum to nSigNum
		nSigNum = nBufLen/sizeof(ALARM_SIG_VALUE);

		if (nSigNum != 0)
		{

			MakeAlarmSigValueSort(pActiveAlarmSigValue, nSigNum);

			*pAlarmNum = nSigNum;
			*ppActiveAlarmSigValue = pActiveAlarmSigValue;
		}
		else
		{
			DELETE(pActiveAlarmSigValue);

			pActiveAlarmSigValue = NULL;
		}

	}
	else
	{

	}

	return nError;
}

/*==========================================================================*
 * FUNCTION : DXI_GetEquipIDFromStdEquipID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nStdEquipID : 
 * RETURN   : static int : the needed equip ID
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:48
 *==========================================================================*/
int DXI_GetEquipIDFromStdEquipID(int nStdEquipID)
{
	EQUIP_INFO* pEquipInfo;

	int nBufLen = 0;
	int nEquipNum, i;

	DxiGetData(VAR_ACU_EQUIPS_LIST,
			0,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0);

	nEquipNum = nBufLen/sizeof(EQUIP_INFO);

	for(i = 0; i < nEquipNum && pEquipInfo != NULL; i++, pEquipInfo++)
	{
		if(pEquipInfo->pStdEquip->iTypeID == nStdEquipID)
		{
			return pEquipInfo->iEquipID;
		}
	}

	return -1;
}
/*==========================================================================*
 * FUNCTION : DXI_GetStdEquipIDStatus
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  nStdEquipID : 
 * RETURN   : static BOOL : 1 exist, 0 not exist
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:48
 *==========================================================================*/
BOOL DXI_GetStdEquipIDStatus(int nStdEquipID)
{
	EQUIP_INFO* pEquipInfo;

	int nBufLen = 0;
	int nEquipNum, i;

	DxiGetData(VAR_ACU_EQUIPS_LIST,
			0,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0);

	nEquipNum = nBufLen/sizeof(EQUIP_INFO);

	for(i = 0; i < nEquipNum && pEquipInfo != NULL; i++, pEquipInfo++)
	{
		if(pEquipInfo->pStdEquip->iTypeID == nStdEquipID && pEquipInfo->bWorkStatus == TRUE)
		{
			return TRUE;
		}
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : DXI_RebootACUorSCU
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN BOOL  bResetSCU : 
 *            IN BOOL  bResetACU : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  :                  DATE: 2006-06-20 14:05
 *==========================================================================*/
BOOL DXI_RebootACUorSCU(IN BOOL bResetSCU, 
						IN BOOL bResetACU)
{
	BOOL bError = TRUE;
	UNUSED(bResetSCU);
	
	if (bResetACU)
	{
		//Tell main app to reboot ACU
		//printf("killall g3_lui\n");
		_SYSTEM("killall g3_lui");
		_SYSTEM("reboot");

		//raise(SIGUSR2);
		//raise(SIGUSR2);
		
	}

	return bError;
}



/*==========================================================================*
 * FUNCTION : DXI_CopyConfigFiles
 * PURPOSE  : copy configuration from folder to folder
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pszCopyFrom : 
 *            IN char  *pszCopyTo   : 
 *            IN BOOL  bCopyForce   : 
 * RETURN   : BOOL : TRUE is successful, otherwise FALSE
 * COMMENTS : 
 * CREATOR  : MAOFUHUA                DATE: 2005-03-21 14:36
 *==========================================================================*/
BOOL DXI_CopyConfigFiles(IN char *pszCopyFrom, 
						 IN char *pszCopyTo, 
						 IN BOOL bCopyForce)
{
	// copy config from folder to folder.
	char	szFromDir[MAX_FILE_PATH];
	char	szToDir[MAX_FILE_PATH];

	char	szCmdLine[MAX_FILE_PATH*2+64];
	char	*pszConfigRootDir = getenv(ENV_ACU_ROOT_DIR);
	
	//disable ACU changes
	if (GetSiteInfo()->bSettingChangeDisabled)
	{
		return TRUE;	// treat it is OK.
	}

	if (pszConfigRootDir == NULL)
	{
		pszConfigRootDir = "./";
	}	

	MakeFullFileName(szFromDir, pszConfigRootDir, pszCopyFrom, "");
	MakeFullFileName(szToDir, pszConfigRootDir, pszCopyTo, "");

// NOW: use the new cp cmd: /scup/bin/syncp. maofuhua, 2005-2-28
//#define _USE_SYSTEM_CP #do NOT define it!
#ifdef _USE_SYSTEM_CP
	// 1. mkdir back dir
	snprintf(szCmdLine, sizeof(szCmdLine), "mkdir -p %s",
		szToDir);

	//system(szCmdLine);
	_SYSTEM(szCmdLine);

	// copy current to backup
	snprintf(szCmdLine, sizeof(szCmdLine), "cp %s %s/* %s/",
		(bCopyForce ? "-Rf" : "-Ru"), szFromDir, szToDir);
#else
	snprintf(szCmdLine, sizeof(szCmdLine), "%s/bin/syncp %s %s %s",
		pszConfigRootDir,
		(bCopyForce ? "-Rf" : "-Ru"), szFromDir, szToDir);
#endif //_USE_SYSTEM_CP

	AppLogOut(DXI_MODULE_TASK, APP_LOG_MILESTONE,//APP_LOG_INFO,
		"Backuping files by executing: %s\n", 
		szCmdLine);
	//system(szCmdLine);
	_SYSTEM(szCmdLine);
	/*if (_SYSTEM(szCmdLine) != 0)
	{
		AppLogOut(DXI_MODULE_TASK, APP_LOG_WARNING,
			"Fails on executing: %s\n", 
			szCmdLine);
		return FALSE;
	}*/

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : DXI_ReloadDefaultConfig
 * PURPOSE  : Reload default configuration and reboot ACU
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-03-21 14:36
 *==========================================================================*/
int DXI_ReloadDefaultConfig(void)
{
	int nRet = ERR_DXI_OK;
	char *ppszCfgDirs[] = {CONFIG_DIR, CONFIG_BAK_DIR, CONFIG_DEFAULT_DIR};

	//disable ACU changes
	if (GetSiteInfo()->bSettingChangeDisabled)
	{
		return ERR_DXI_HARDWARE_SWITCH_STATUS;
	}

	//It is no efficiency to copy all files here, just remove the setting data in the flash.
	/*if (DXI_CopyConfigFiles(ppszCfgDirs[CONFIG_DEFAULT_TYPE], 
		ppszCfgDirs[CONFIG_NORMAL_TYPE], TRUE))
	{*/
		// remove the last solution run file will cause system remove all
		// historical data files after system restarted. maofuhua, 2005-3-24
		char szRunningSolution[MAX_FILE_PATH];
	
		MakeFullFileName(szRunningSolution, getenv(ENV_ACU_ROOT_DIR), 
			CONFIG_DIR, SCUP_RUNNING_SOLUTION);


		remove(szRunningSolution);

		DAT_StorageDeleteRecord(PERSISTENT_SIG_LOG);
		TRACE("\n Delete Setting DATA!!! \n");
		Sleep(2000);//Wait 2 seconds here.
		//Tell main app to reboot ACU and SCU
		DXI_RebootACUorSCU(TRUE, TRUE);
	/*}
	else
	{
		nRet = ERR_DXI_LOAD_DEFAULT_CONFIG;

		AppLogOut(DXI_MODULE_TASK, APP_LOG_WARNING,
			"Fails on loading default config\n");
	}*/

	return nRet;
}



/*==========================================================================*
 * FUNCTION : DXI_GetConfigStatus
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  *pConfigType : 
 *		#define CONFIG_NORMAL_TYPE	0	// The being used config is normal
 *		#define CONFIG_BACKUP_TYPE	1	// the being used config is backup
 *		#define CONFIG_DEFAULT_TYPE	2	// the being used config is default.
 * RETURN   : int : the config changed status
 *		#define CONFIG_STATUS_NOT_CHANGED		0	// the config is not changed.
 *		#define CONFIG_STATUS_NORMALLY_CHANGED	1	// the config just changed,
 *                                                  // historical data need be removed.
 *		#define CONFIG_STATUS_DEFAULT_LOADED	2	// the config just changed to default,
 *                                                  // all record files shall be removed.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-24 21:39
 *==========================================================================*/
int DXI_GetConfigStatus(int *pConfigType)
{
	if (pConfigType != NULL)
	{
		*pConfigType = GetSiteInfo()->nConfigType;
	}

	return GetSiteInfo()->nConfigStatus;
}

/*==========================================================================*
 * FUNCTION : DXI_GetUSBConfigFiles
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  :Marco         DATE: 2008-10-29 10:01
 *==========================================================================*/
 #define USB_MOUNT_DIR		"/usb"
BOOL DXI_USBGetConfigFiles(IN char *pszConfigFiles)
{
	if(access(pszConfigFiles, F_OK))//R_OK	//return 0 if success
	{
		return FALSE;
	}
	return TRUE;
}

BOOL DXI_USBMountDirect()
{
	char szCommand[128];
	sprintf(szCommand,"/bin/mount /dev/sda1 %s", USB_MOUNT_DIR);
	//if(system(szCommand) > 0)
	if(_SYSTEM(szCommand) > 0)
	{
		return TRUE;
	}
	return FALSE;
}
/*
BOOL DXI_GetDirectoryList(OUT FileList **stFileList,OUT int *iBufLen)   
{   

	DIR		*directory_pointer;   
	struct	dirent  *entry;
	FileList	*stTFileList; 
	
	stTFileList = NEW(char, 10*sizeof(FileList));
	if(stTFileList == NULL)
	{
		return FALSE;
	}
	if((directory_pointer = opendir(USB_MOUNT_DIR)) == NULL)
	{	   
		TRACE("Error to open %s\n",USB_MOUNT_DIR);   
		return FALSE;
	}
	else   
	{   

		while   ((entry = readdir(directory_pointer)) != NULL)   
		{   
          		if(strstr(entry->d_name,".tar.gz"))
          		{
          	  		if( (*iBufLen) > 10)
          	  		{          	  			
          	  			continue;   	  			
          	  		}
          	  		else
          	  		{
          	  			strncpyz(stTFileList->filename,entry->d_name,sizeof(stTFileList->filename));           	  			
          	  		}
          	  		stTFileList++;
          	  		(*iBufLen)++; 
          		}
		}   
		closedir(directory_pointer);
		*stFileList = stTFileList;
	        
	}   
	
	return TRUE;
}*/
/* for config file load, read and write */
/*==========================================================================*
* FUNCTION : *Cfg_GetFullConfigPath
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char  *pszPartPath : 
*			  OUT char  *szFullPath :
*			  IN int iFullPathLen   :
* RETURN   : char : 
* COMMENTS : 
* CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-04 19:56
*==========================================================================*/
char *Cfg_GetFullConfigPath(IN char *pszPartPath, 
							OUT char *szFullPath, 
							IN int iFullPathLen)
{
	//	static char szFullPath[256];
	////#warning "This function Cfg_GetFullPath is not Multi-Thread safe!"
	//#define ENV_ACU_ROOT_DIR	"ACU_ROOT_DIR"
	//
	//	char *pszConfigEnv = getenv(ENV_ACU_ROOT_DIR);	
	//
	//	if (pszConfigEnv == NULL)
	//	{
	//		printf("Please set the environment variable '%s' of ACU root dir!\n",
	//			ENV_ACU_ROOT_DIR);
	//		pszConfigEnv = ".";
	//	}
	//
	//	snprintf(szFullPath, sizeof(szFullPath),
	//		"%s/%s",
	//		pszConfigEnv,
	//		pszPartPath);

	snprintf(szFullPath, (size_t)iFullPathLen,
		"%s/%s",
		g_szACUConfigDir,
		pszPartPath);


	return szFullPath;
}

//ServiceManager_GetService() have been move to app_service.c file by lixidong 20050311

/*==========================================================================*
* FUNCTION : DXI_GetESRCommonCfgMutex
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : HANDLE : 
 * COMMENTS : 
 * CREATOR  : lintao                DATE: 2005-03-09 10:43
 *==========================================================================*/
HANDLE DXI_GetESRCommonCfgMutex()
{
	return g_hMutexESRCommonCfg;
}


/*==========================================================================*
* FUNCTION : SetFloatSigValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    nEquipId    : 
*            int    nSignalType : 
*            int    nSignalId   : 
*            DWORD  fValue     : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Copy  Frank Cao  add by IlockTeng     DATE: 2007-06-27 18:42
*==========================================================================*/
BOOL SetFloatSigValue(int nEquipId,
					  int nSignalType,
					  int nSignalId,
					  float fValue,
					  char*	pcUser)
{
	int					iRst;

	VAR_VALUE_EX		sigValue;

	//sigValue.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = pcUser;
	sigValue.varValue.fValue = fValue;

	iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
		nEquipId,
		DXI_MERGE_SIG_ID(nSignalType, nSignalId),
		sizeof(VAR_VALUE_EX),
		&sigValue,
		0);
	if(iRst != ERR_OK)
	{
		AppLogOut(pcUser, 
			APP_LOG_ERROR, 
			"SetFloatSigValue error, EquipId = %d, SigType = %d, SigId = %d iRst = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId, iRst); 
		return FALSE;
	}
	return TRUE;

}

/*==========================================================================*
* FUNCTION : SetEnumSigValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    nEquipId    : 
*            int    nSignalType : 
*            int    nSignalId   : 
*            DWORD  fValue     : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Copy  Frank Cao  add by IlockTeng               DATE: 2007-06-27 18:42
*==========================================================================*/
BOOL SetEnumSigValue(int nEquipId,
					  int nSignalType,
					  int nSignalId,
					  SIG_ENUM EnumValue,
					  char*	pcUser)
{
	int					iRst;

	VAR_VALUE_EX		sigValue;

	//sigValue.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = pcUser;
	sigValue.varValue.enumValue = EnumValue;

	iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
		nEquipId,
		DXI_MERGE_SIG_ID(nSignalType, nSignalId),
		sizeof(VAR_VALUE_EX),
		&sigValue,
		0);
	if(iRst != ERR_OK)
	{
		AppLogOut(pcUser, 
			APP_LOG_ERROR, 
			"SetEnumSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
		return FALSE;
	}
	return TRUE;

}

/*==========================================================================*
 * FUNCTION : SetDwordSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    nEquipId    : 
 *            int    nSignalType : 
 *            int    nSignalId   : 
 *            DWORD  dwValue     : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2007-06-27 18:42
 *==========================================================================*/
BOOL SetDwordSigValue(int nEquipId,
					  int nSignalType,
					  int nSignalId,
					  DWORD dwValue,
					  char*	pcUser)
{
	int					iRst;

	VAR_VALUE_EX		sigValue;

	sigValue.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = pcUser;
	sigValue.varValue.ulValue = dwValue;

	iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					sizeof(VAR_VALUE_EX),
					&sigValue,
					0);
	if(iRst != ERR_OK)
	{
		AppLogOut(pcUser, 
			APP_LOG_ERROR, 
			"SetDwordSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
		return FALSE;
	}
	return TRUE;

}


/*==========================================================================*
 * FUNCTION : GetDwordSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nEquipId    : 
 *            int  nSignalType : 
 *            int  nSignalId   : 
 * RETURN   : static DWORD : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2007-06-27 18:30
 *==========================================================================*/
DWORD GetDwordSigValue(int nEquipId,
					   int nSignalType,
					   int nSignalId,
					   char* pcUser)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		AppLogOut(pcUser, 
			APP_LOG_ERROR, 
			"GetDwordSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
		return 0;
	}	
	
	return pSigValue->varValue.ulValue;

}


/*==========================================================================*
 * FUNCTION : GetEnumSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nEquipId    : 
 *            int  nSignalType : 
 *            int  nSignalId   : 
 * RETURN   : SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-02 11:03
 *==========================================================================*/
SIG_ENUM GetEnumSigValue(int nEquipId, 
						 int nSignalType,
						 int nSignalId,
						 char* pcUser)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		AppLogOut(pcUser, 
			APP_LOG_ERROR, 
			"GetEnumSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
		return 0;
	}	
	
	return pSigValue->varValue.enumValue;

}


/*==========================================================================*
 * FUNCTION : GetFloatSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nEquipId    : 
 *            int  nSignalType : 
 *            int  nSignalId   : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-05-15 14:11
 *==========================================================================*/
float GetFloatSigValue(int nEquipId, 
					   int nSignalType,
					   int nSignalId,
					   char* pcUser)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		AppLogOut(pcUser, 
			APP_LOG_ERROR, 
			"GetFloatSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
		return 0.0;
	}
	
	return pSigValue->varValue.fValue;

}





#ifdef COM_SHARE_SERVICE_SWITCH

HANDLE DXI_GetServiceSwitchMutex()
{
	return g_hMutexServiceSwitch;
}

SERVICE_SWITCH_FLAG * DXI_GetServiceSwitchFlag()
{
	return &g_ServiceSwitchFlag;
}
#endif //COM_SHARE_SERVICE_SWITCH

/*==========================================================================*
* FUNCTION : GetEquipAlarmRelayByID
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: nVarID:STDEquipID
             nVarSubID:AlarmSigID
* RETURN   : pDataBuf
* COMMENTS : 
* CREATOR  : WangJing         DATE: 2006-10-24 13:03
*==========================================================================*/
static int GetEquipAlarmRelayByID(int   nVarID,			
                                  int   nVarSubID,		
                                  int*  pBufLen,			
                                  void* pDataBuf,			
                                  int   nTimeOut)
{
    TRACE("\n into GetEquipAlarmRelayByID \n");
    int  nSigPosition;
    int   nSigInfosLength;

    int	  nSigID = nVarSubID;

    ALARM_SIG_INFO *pSigInfo;

    int	  nError;

    UNUSED(nTimeOut);

    nError = GetASignalInfoStru(nVarID,			
                                DXI_MERGE_SIG_ID(3, nSigID),		
                                &nSigInfosLength,			
                                (void*)(&pSigInfo),			
                                0);
    

    //*pBufLen = sizeof(ALARM_SIG_INFO);

    if (nError != ERR_DXI_OK)
    {
        return nError;
    }

    TRACE(" \n GetASignalInfoStru ok!\n");

    

    TRACE(" \n GetASignalInfoStru ok iAlarmRelayNo = %d\n",(ALARM_SIG_INFO *)pSigInfo->iAlarmRelayNo);

    *(int *)pDataBuf = (ALARM_SIG_INFO *)pSigInfo->iAlarmRelayNo;

    return ERR_DXI_OK;

}

/*==========================================================================*
* FUNCTION : SetEquipAlarmRelayByID
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: nVarID:STDEquipTypeID
             nVarSubID:AlarmSigID
             
* RETURN   : pDataBuf
* COMMENTS : 
* CREATOR  : WangJing         DATE: 2006-10-24 13:03
*==========================================================================*/
static int SetSTDEquipAlarmRelayByID(int   nVarID,			
                                     int   nVarSubID,		
                                     int*  pBufLen,			
                                     void* pDataBuf,			
                                     int   nTimeOut)
{
    TRACE("\n into SetSTDEquipAlarmRelayByID \n");
    int   nSigInfosLength;
    int   iMergeID;


    *pBufLen = 4;



    ALARM_SIG_INFO *pSigInfo;

    int	  nError;

    UNUSED(nTimeOut);


    nSigInfosLength = 0;

    iMergeID = DXI_MERGE_SIG_ID(3, nVarSubID);

    nError = GetASignalInfoViaStdEquip(nVarID,			
        iMergeID,		
        &nSigInfosLength,			
        (void*)(&pSigInfo),			
        0);

    if (nError != ERR_DXI_OK)
    {
        return nError;
    }


    

//    (ALARM_SIG_INFO *)pSigInfo->iAlarmRelayNo = *(int *)pDataBuf;


    return ERR_DXI_OK;
}
/*==========================================================================*
* FUNCTION : GetLCDKeyStatus
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: nVarID: NULL
             nVarSubID: NULL
             
* RETURN   : pDataBuf
* COMMENTS : 
* CREATOR  : YangGuoxin         DATE: 2008-6-18 13:03
*==========================================================================*/

static int GetLCDKeyStatus(int   nVarID,			
                                  int   nVarSubID,		
                                  int*  pBufLen,			
                                  void* pDataBuf,			
                                  int   nTimeOut)
{
	UNUSED(nVarID);
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);

	*pBufLen = sizeof(UCHAR);

	if(stKeyInfo.nCount > 0)
	{
		stKeyInfo.nCount--;
	}
	else
	{
		stKeyInfo.byKeyValue = 0x00;
		stKeyInfo.nCount = 0;
	}

	*((UCHAR*)pDataBuf) = stKeyInfo.byKeyValue;
 

	return ERR_DXI_OK;
}

static int SetLCDKeyStatus(int   nVarID,			
				int   nVarSubID,		
				int*  pBufLen,			
				void* pDataBuf,			
				int   nTimeOut)
{
	
	int nError = ERR_DXI_OK;

	if (sizeof(_KEY_INFO_) != *pBufLen)
	{
		return ERR_DXI_WRONG_BUFFER_LENGTH;
	}
	
	
	if(nVarID == TRUE)
	{
		//clear key status
		stKeyInfo.byKeyValue = 0x00;
		stKeyInfo.nCount= 0;
	}
	else
	{
		//Set key status
  		stKeyInfo = *((_KEY_INFO_*)pDataBuf);
	}
	
	if (nError != ERR_DXI_OK)
	{
		return nError;
	}

	return nError;
}    

/*==========================================================================*
* FUNCTION : GetUserDefPages
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: nVarID: NULL
nVarSubID: NULL

* RETURN   : pDataBuf
* COMMENTS : 
* CREATOR  : wj         DATE: 2008-6-18 13:03
*==========================================================================*/

static int GetUserDefPages(int   nVarID,			
						   int   nVarSubID,		
						   int*  pBufLen,			
						   void* pDataBuf,			
						   int   nTimeOut)
{
	UNUSED(nVarID);
	UNUSED(nVarSubID);
	UNUSED(nTimeOut);

	*pBufLen = sizeof(GetSiteInfo()->pUserDefPages);

	*((USER_DEF_PAGES**)pDataBuf) = GetSiteInfo()->pUserDefPages;


	return ERR_DXI_OK;
}

static int AutoCfg_RefreshSlaveRectPositionSignal(int iSlaveID)
{
	int i;
	int iRectNumber, iStartRectID, iTempSigValue;
	int iError1,iError2;
	int iEquipID;//ACU System Equip ID
	int iSignalType = 0;
	int iSignalID = 2;//Slave Rectifier Number
	int iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	SIG_BASIC_VALUE* pSigValue;

	int iOkFlage = 0;

	int		iBufLen = sizeof(VAR_VALUE_EX);
	VAR_VALUE_EX	value;


	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;


	if(iSlaveID == 0)
	{
		iEquipID = 639;
		iStartRectID = 339;
	}
	else if(iSlaveID == 1)
	{
		iEquipID = 640;
		iStartRectID = 439;
	}
	else
	{
		iEquipID = 641;
		iStartRectID = 539;
	}

	iError1 = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);

	if(iError1 == ERR_DXI_OK)
	{
		iRectNumber = pSigValue->varValue.ulValue;//Rectifier Number
	}
	else
	{
		iRectNumber = 0;
	}



	TRACE("\n iStartRectID:%d;iRectNumber:%d \n",iStartRectID,iRectNumber);

	for(i = iStartRectID;i < iStartRectID + iRectNumber; i++)
	{

		iError1 = DxiGetData(VAR_A_SIGNAL_VALUE,
			i,			
			DXI_MERGE_SIG_ID(2, 1),		
			&iBufLen,			
			(void *)&pSigValue,			
			10000);

		if(iError1 != ERR_DXI_OK)
		{
			iOkFlage = 1;
			break;
		}

		iTempSigValue = pSigValue->varValue.ulValue;

		TRACE("/n PositionID:%d /n",iTempSigValue);

		value.varValue.ulValue  = iTempSigValue;
		iBufLen = sizeof(VAR_VALUE_EX);

		iError2 = DxiSetData(VAR_A_SIGNAL_VALUE,
			i,			
			DXI_MERGE_SIG_ID(0, 34),	
			iBufLen,			
			&value,			
			10000);

		TRACE("/n Error:%d /n",iError2);

		RunThread_Heartbeat(RunThread_GetId(NULL));
	}



	if(iOkFlage == 1)
	{

		Sleep(30000);

		for(i = iStartRectID;i < iStartRectID + iRectNumber; i++)
		{

			iError1 = DxiGetData(VAR_A_SIGNAL_VALUE,
				i,			
				DXI_MERGE_SIG_ID(2, 1),		
				&iBufLen,			
				(void *)&pSigValue,			
				10000);

			iTempSigValue = pSigValue->varValue.ulValue;

			TRACE("/n PositionID:%d /n",iTempSigValue);

			value.varValue.ulValue  = iTempSigValue;
			iBufLen = sizeof(VAR_VALUE_EX);

			iError2 = DxiSetData(VAR_A_SIGNAL_VALUE,
				i,			
				DXI_MERGE_SIG_ID(0, 34),	
				iBufLen,			
				&value,			
				10000);

			TRACE("/n Error:%d /n",iError2);

			RunThread_Heartbeat(RunThread_GetId(NULL));
		}

	}

	return TRUE;

}

static int AutoCfg_RefreshSlaveRectPosition()
{

	TRACE("\n Into AutoCfg_RefreshSlaveRectPosition\n");
	int i;
	int iBufLen;
	int iState,iSlaveWork[3];
	int iError;
	int iEquipID = 639;//ACU System Equip ID
	int iSignalType = 0;
	int iSignalID = 100;//Work Status
	int iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	SIG_BASIC_VALUE* pSigValue[3];

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue[0],			
		10000);
	iSlaveWork[0] = pSigValue[0]->varValue.enumValue;//Slave1 Work Status

	iEquipID = 640;//ACU System Equip ID
	iSignalType = 0;
	iSignalID = 100;//Work Status
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue[1],			
		10000);
	iSlaveWork[1] = pSigValue[1]->varValue.enumValue;//Slave2 Work Status

	iEquipID = 641;//ACU System Equip ID
	iSignalType = 0;
	iSignalID = 100;//Work Status
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue[2],			
		10000);
	iSlaveWork[2] = pSigValue[2]->varValue.enumValue;//Slave3 Work Status



	for(i = 0; i < 3; i++)
	{
		TRACE("\n %d \n",iSlaveWork[i]);
		if(iSlaveWork[i] == 0)
		{
			TRACE("\n Slave %d\n",i);
			AutoCfg_RefreshSlaveRectPositionSignal(i);
			RunThread_Heartbeat(RunThread_GetId(NULL));

		}

	}



	return TRUE;

}
/*==========================================================================*
* function : DXI_AutoConfigWithReboot
* purpose  : G3 loads typical equipments by default, need reboot first 
* calls    : 
* called by: 
* return   : handle : 
* comments : 
* creator  : LinTao                date: 2013-05-24 13:52
*==========================================================================*/
BOOL DXI_AutoConfigWithReboot(void)
{
	char szFullPath[MAX_FILE_PATH];
	char szCmdLine[128];
	printf("killall g3_lui\n");
	_SYSTEM("killall g3_lui");
	Cfg_GetFullConfigPath(LOADALLEQUIP_FLAGFILE, szFullPath, MAX_FILE_PATH);
	snprintf(szCmdLine, sizeof(szCmdLine), "rm -rf %s",
		szFullPath);	
	_SYSTEM(szCmdLine);

	//DXI_RebootACUorSCU(FALSE, TRUE);
	_SYSTEM("reboot");
	return TRUE;
}

/*==========================================================================*
* function : DIX_AutoConfigMain_ForSlavePosition_ForWeb
* purpose  : auto config main logic 
* calls    : 
* called by: 
* return   : handle : 
* comments : 
* creator  : wangjing                date: 2007-06-21 10:43
*==========================================================================*/
void DIX_AutoConfigMain_ForSlavePosition_ForWeb(void)
{

	printf("Start DIX_AutoConfigMain Thread_Web!!!\n");

	int iEquipID;
	int iSignalType;
	int iSignalID;
	int iVarSubID;
	int iBufLen;
	int iError;
	VAR_VALUE_EX value;
	SIG_BASIC_VALUE* pSigValue;
	int i;
	int iChangeStatusForWeb;

	iBufLen = sizeof(VAR_VALUE_EX);

	iEquipID = 106;//SMDU_GROUP_EQUIPID
	iVarSubID =DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 3);

	
	
	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	value.pszSenderName = "Slave_Position";


	RunThread_Heartbeat(RunThread_GetId(NULL));

	Sleep(150000);//20 seconds

	RunThread_Heartbeat(RunThread_GetId(NULL));


	AutoCfg_RefreshSlaveRectPosition();

	RunThread_Heartbeat(RunThread_GetId(NULL));

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);
	iChangeStatusForWeb = pSigValue->varValue.ulValue;//Slave1 Work Status

	iChangeStatusForWeb++;

	if(iChangeStatusForWeb >= 255)
	{
		iChangeStatusForWeb = 0;
	}

	value.varValue.ulValue  = iChangeStatusForWeb;

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		&value,			
		10000);

	printf("End DIX_AutoConfigMain Thread_Web!!!\n");


}

/*==========================================================================*
* function : DIX_AutoConfigMain_ForSlavePosition
* purpose  : auto config main logic 
* calls    : 
* called by: 
* return   : handle : 
* comments : 
* creator  : wangjing                date: 2007-06-21 10:43
*==========================================================================*/
void DIX_AutoConfigMain_ForSlavePosition(void)
{

	printf("Start DIX_AutoConfigMain Thread!!!\n");

	int iEquipID;
	int iSignalType;
	int iSignalID;
	int iVarSubID;
	int iBufLen;
	int iError;
	VAR_VALUE_EX value;
	SIG_BASIC_VALUE* pSigValue;
	int i;

	iBufLen = sizeof(VAR_VALUE_EX);

	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	value.varValue.enumValue  = 1;
	value.pszSenderName = "Slave_Position";

	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 90;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);


	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		&value,			
		10000);

	RunThread_Heartbeat(RunThread_GetId(NULL));
	for(i = 0;i < 3;i++)
	{
		sleep(20);//20 seconds
		RunThread_Heartbeat(RunThread_GetId(NULL));
	}

	RunThread_Heartbeat(RunThread_GetId(NULL));

	AutoCfg_RefreshSlaveRectPosition();

	printf("End DIX_AutoConfigMain Thread!!!\n");

}

/*==========================================================================*
* function : DIX_AutoConfigMain
* purpose  : auto config main logic 
* calls    : 
* called by: 
* return   : handle : 
* comments : 
* creator  : wangjing                date: 2007-06-21 10:43
*==========================================================================*/
void DIX_AutoConfigMain(void)
{
	TRACE("Start DIX_AutoConfigMain Thread!!!\n");
	AppLogOut(DXI_MODULE_TASK, APP_LOG_INFO, "Start AutoConfig.\n");		//Added by Victor Fan, 2007-11-21
	int i;
	int iBufLen;
	int iState;
	int iError;
	int iEquipID = 1;//ACU System Equip ID
	int iSignalType = 2;
	int iSignalID = 7;//AUTO/MAN Mode
	int iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	SIG_BASIC_VALUE* pSigValue;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);
	iState = pSigValue->varValue.enumValue;//AUTO/MAN Mode


	RUN_THREAD_MSG AutocfgStartmsg;
	RUN_THREAD_MSG AutocfgRefreshmsg;
	RUN_THREAD_MAKE_MSG(&AutocfgStartmsg,NULL,MSG_AUTOCONFIG,ID_AUTOCFG_START,0);//Defined in run_thread.h & data_exchange.h,for the AutoConfig Start msg
	RUN_THREAD_MAKE_MSG(&AutocfgRefreshmsg,NULL,MSG_AUTOCONFIG,ID_AUTOCFG_REFRESH_DATA,0);//For the AutoConfig Refresh msg

	RunThread_PostMessage(-1,&AutocfgStartmsg,FALSE);

	AutoCfg_BlockAlarm(TRUE,iState);

	AutoCfg_Start();


	RunThread_Heartbeat(RunThread_GetId(NULL));
	for(i = 0;i < 4;i++)
	{
		sleep(20);//20 seconds
		RunThread_Heartbeat(RunThread_GetId(NULL));
	}
	
	
	AutoCfg_ClearEquipAlarm();

	AutoCfg_BlockAlarm(FALSE,iState);

	AutoCFG_UpdateActiveAlarmCnt(GetSiteInfo());

	EQUIP_INFO *pEquip;
	SAMPLER_INFO *pSampler;
	int iSamplerNumber;

	pSampler = GetSiteInfo()->pSamplerInfo;
	iSamplerNumber = GetSiteInfo()->iSamplerNum;

	for(i = 0;i < iSamplerNumber;i++)
	{   
		if(pSampler[i].pStdSampler->iStdSamplerID == 1 || pSampler[i].pStdSampler->iStdSamplerID == 2 || pSampler[i].pStdSampler->iStdSamplerID > 3)
		{
			pSampler[i].bQueryPI = FALSE;//get the product info when finish auto config in process
			
		}
	}

	sleep(20);//40 seconds

//#ifdef SUPPORT_CSU
//
//	int iACUSysID = 1;
//	int iDCLCNum,iBatLCNum;
//	VAR_VALUE_EX value;
//	//setting the CSU working state
//	iVarSubID = SIG_ID_SYS_DCLCNUM;//CSU DCLC Number
//	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
//		iACUSysID,	//ACU System Equip ID		
//		iVarSubID,		
//		&iBufLen,			
//		(void *)&pSigValue,			
//		10000);
//	iDCLCNum = pSigValue->varValue.ulValue;//CSU DCLC Number
//
//	iVarSubID = SIG_ID_SYS_BATLCNUM;//CSU BatLC Number
//	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
//		iACUSysID,	//ACU System Equip ID		
//		iVarSubID,		
//		&iBufLen,			
//		(void *)&pSigValue,			
//		10000);
//	iBatLCNum = pSigValue->varValue.ulValue;//CSU BatLC Number
//
//	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
//	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
//	iVarSubID = SIG_ID_SYS_LCWORKINGSTATE;
//
//	for(iEquipID = 1169,i = 0;iEquipID <= 1222;iEquipID++,i++)
//	{
//		if(i < iDCLCNum)
//		{
//			
//			value.varValue.enumValue = 0;//Working  
//
//			iError = DxiSetData(VAR_A_SIGNAL_VALUE,
//				iEquipID,			
//				iVarSubID,		
//				iBufLen,			
//				&value,			
//				10000);
//
//		}
//		else
//		{
//			value.varValue.enumValue = 1;//not Working  
//
//			iError = DxiSetData(VAR_A_SIGNAL_VALUE,
//				iEquipID,			
//				iVarSubID,		
//				iBufLen,			
//				&value,			
//				10000);
//
//		}
//
//	}
//
//	for(iEquipID = 1223,i = 0;iEquipID <= 1253;iEquipID++,i++)
//	{
//		if(i < iBatLCNum)
//		{
//
//			value.varValue.enumValue = 0;//Working  
//
//			iError = DxiSetData(VAR_A_SIGNAL_VALUE,
//				iEquipID,			
//				iVarSubID,		
//				iBufLen,			
//				&value,			
//				10000);
//
//		}
//		else
//		{
//			value.varValue.enumValue = 1;//Working  
//
//			iError = DxiSetData(VAR_A_SIGNAL_VALUE,
//				iEquipID,			
//				iVarSubID,		
//				iBufLen,			
//				&value,			
//				10000);
//
//		}
//
//	}
//
//
//#endif

	RunThread_PostMessage(-1,&AutocfgRefreshmsg,FALSE);


	TRACE("DIX_AutoConfigMain Thread OK!!!\n");
	AppLogOut(DXI_MODULE_TASK, APP_LOG_INFO, "AutoConfig Done.\n");		//Added by Victor Fan, 2007-11-21

}

/*==========================================================================*
* function : AutoCfg_BlockAlarm
* purpose  : blocking the alarm;iflage = true(blocking)/false(normal)
* calls    : 
* called by: 
* return   : handle : 
* comments : 
* creator  : wangjing                date: 2007-06-21 10:43
*==========================================================================*/
static int AutoCfg_BlockAlarm(IN int iFlage,IN int iState)
{
	int iEquipID;
	int iSignalType;
	int iSignalID;
	int iVarSubID;
	int iBufLen;
	int iError;
	VAR_VALUE_EX value;
	SIG_BASIC_VALUE* pSigValue;

	iBufLen = sizeof(VAR_VALUE_EX);

	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	value.varValue.enumValue  = iFlage;


	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 8;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		&value,			
		10000);

	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 7;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);



	if(iState != 1 )
	{
		iError = DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,			
			iVarSubID,		
			iBufLen,			
			&value,			
			10000);
		TRACE("____Change State to:%d_____\n",iFlage);
	}



	return iError;
}
/*==========================================================================*
* function : AutoCfg_Start
* purpose  : 
* calls    : 
* called by: 
* return   : handle : 
* comments : 
* creator  : wangjing                date: 2007-06-21 10:43
*==========================================================================*/
static int AutoCfg_Start(void)
{
	int iEquipID;
	int iSignalType;
	int iSignalID;
	int iVarSubID;
	int iBufLen;
	int iError;
	VAR_VALUE_EX value;
	SIG_BASIC_VALUE* pSigValue;

	iBufLen = sizeof(VAR_VALUE_EX);

	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	value.varValue.enumValue  = 1;
	value.pszSenderName = "Auto_Config";


	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 70;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		&value,			
		10000);

	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 71;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);


	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		&value,			
		10000);

	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 90;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);


	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		&value,			
		10000);

	return iError;
}

/*==========================================================================*
* function : Autocfg_EndNotWorkingEquipAlarm
* purpose  :  
* calls    : 
* called by: 
* return   : handle : 
* comments : 
* creator  : wangjing                date: 2007-06-21 10:43
*==========================================================================*/
static int Autocfg_EndNotWorkingEquipAlarm(EQUIP_INFO *pEquip)
{

	int iAlarmSigNum;
	int i;
	ALARM_SIG_VALUE *pAlarm;
	time_t	tmNow = time(NULL);


	iAlarmSigNum = pEquip->pStdEquip->iAlarmSigNum;
	pAlarm = pEquip->pAlarmSigValue;

	for(i = 0;i < iAlarmSigNum;i++,pAlarm++ )
	{
		if((pAlarm->next != NULL))
		{
			REMOVE_NODE_SELF_FROM_LINK(pAlarm);
			SIG_STATE_SET(pAlarm, ALARM_STATE_IS_NEW_END);
			pAlarm->bv.varValue.lValue = 0;
			pAlarm->sv.tmEndTime = tmNow;
		}
	}


}
/*==========================================================================*
* FUNCTION : AutoCFG_UpdateActiveAlarmCntExe
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN EQUIP_INFO  *pEquip    : 
*            IN int         *piSiteCnt : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2006-10-20 16:16
*==========================================================================*/
static BOOL AutoCFG_UpdateActiveAlarmCntExe(IN EQUIP_INFO *pEquip, IN int *piSiteCnt)
{
	int					nLevel;
	int                 iAlarmCnt[ALARM_LEVEL_MAX] = {0};
	ALARM_SIG_VALUE		*pSig, *pHead;

	for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		pHead = &pEquip->pActiveAlarms[nLevel];
		pSig  = pHead->next;

		// the link is a bilink
		for (; pSig != pHead; pSig = pSig->next)
		{
			//only non-suppressed alarm could be counted
			if(!SIG_VALUS_IS_SUPPRESSED(pSig))  
			{
				iAlarmCnt[nLevel] ++;
			}
		}

		pEquip->iAlarmCount[nLevel] = iAlarmCnt[nLevel];
		piSiteCnt[nLevel] += iAlarmCnt[nLevel];
		iAlarmCnt[0] += iAlarmCnt[nLevel];
	}

	// increase the total alarm count
	pEquip->iAlarmCount[0] = iAlarmCnt[0];	
	piSiteCnt[0] += iAlarmCnt[0];

	return TRUE;
}
/*==========================================================================*
* FUNCTION : AutoCFG_UpdateActiveAlarmCnt
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2006-10-20 16:14
*==========================================================================*/
static BOOL AutoCFG_UpdateActiveAlarmCnt(IN SITE_INFO *pSite)
{
	int				i, nLevel;
	int             iSiteCnt[ALARM_LEVEL_MAX] = {0};
	EQUIP_INFO		*pEquip;


	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		AutoCFG_UpdateActiveAlarmCntExe(pEquip, iSiteCnt);
	}

	for (nLevel = ALARM_LEVEL_NONE; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		pSite->iAlarmCount[nLevel] = iSiteCnt[nLevel];	
	}

	return TRUE;
}


/*==========================================================================*
* FUNCTION : AutoCfg_ClearEquipAlarm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2006-10-20 16:14
*==========================================================================*/
static BOOL AutoCfg_ClearEquipAlarm()
{
	int	iVarSubID;
	int iBufLen;
	int iEquipNumber;
	int	iError = 0;
	int i,j,k;
	EQUIP_INFO *pEquip;
	pEquip = GetSiteInfo()->pEquipInfo;
	iEquipNumber = GetSiteInfo()->iEquipNum;

	for(i = 0;i < iEquipNumber;i++)
	{    
		if(pEquip[i].pStdEquip->iTypeID !=201)
		{
			/* general process */
			if (!pEquip[i].bWorkStatus)
			{
				Autocfg_EndNotWorkingEquipAlarm(&pEquip[i]);
			}
		}
	}

}


/*==========================================================================*
* FUNCTION : DXI_ChangeLanguage
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: language type:{"de","es","it","pt","ru","sp","tr","tw","zh"}
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Marco Yang                   DATE: 2013-07-09 16:14
*==========================================================================*/
void DXI_ChangeLanguage(IN char *szLangType)
{
#define MAX_SUPPORT_LANGUAGE		8
	static char szLangCode[MAX_SUPPORT_LANGUAGE][8] ={"de","es","fr","it","ru","tr","tw","zh"};
	static char szLRFileName[256], szLRToFileName[256]; /* the returned lang resource file name */
	int i;

	if (szLangType == NULL)
	{
		return;
	}

	for(i = 0; i <MAX_SUPPORT_LANGUAGE; i++ )
	{
		if(strstr(szLangType, szLangCode[i]))
		{
			break;
		}
	}
	if(i == MAX_SUPPORT_LANGUAGE)//Not supported language;
	{
		return;
	}
	/* init */
	szLRFileName[0] = '\0';
	szLRToFileName[0] = '\0';
	
	/* get the head: config/lang/xx/ */
	strcpy(szLRFileName, "/app/config_default/solution/MonitoringSolution_");
	strcat(szLRFileName, szLangType);
	strcat(szLRFileName, ".cfg");

	snprintf(szLRToFileName,sizeof(szLRToFileName), "cp %s /app/config/solution/MonitoringSolution.cfg -f",szLRFileName);
	
	//system(szLRToFileName);
	_SYSTEM(szLRToFileName);
	//Copy the language directory.
	/* init */
	szLRFileName[0] = '\0';
	szLRToFileName[0] = '\0';
	
	/* get the head: config/lang/xx/ */
	strcpy(szLRFileName, "/app/config_default/lang/");
	strcat(szLRFileName, szLangType);
	snprintf(szLRToFileName,sizeof(szLRToFileName), "cp %s /app/config/lang/ -rf",szLRFileName);
	//system(szLRToFileName);
	_SYSTEM(szLRToFileName);
	DXI_RebootACUorSCU(TRUE, TRUE);

	return;
}


int GetTempFormSwitchSignalPoint(SIG_BASIC_VALUE** pSigValue)
{
#define EQUIP_ID_SYSTEM			1
#define SIG_ID_TEMP_FORM		498

	int nError,nBufLength;

	nError = GetASignalValue(EQUIP_ID_SYSTEM,
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SIG_ID_TEMP_FORM),
		&nBufLength,
		pSigValue,
		0);

	return nError;
}
/*==========================================================================*
* FUNCTION : GetTempSwitchMode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : 0 £ºCelsius   1 £º Fahrenheit 
* COMMENTS : 
* CREATOR  : SongXu                   DATE: 2017-03-24
*==========================================================================*/
int GetTempSwitchMode()
{
	static int s_nError;
	static SIG_BASIC_VALUE * pSigValueTempForm = NULL;
	if(pSigValueTempForm == NULL)
	{
		s_nError = GetTempFormSwitchSignalPoint(&pSigValueTempForm);

	}
	if(s_nError != ERR_DXI_OK || pSigValueTempForm == NULL)
	{
		return CELSIUS;
	}

	return pSigValueTempForm->varValue.enumValue;
}

#define TEMP_C_UNIT		"deg.C"
#define TEMP_F_UNIT		"deg.F"
/*==========================================================================*
* FUNCTION : GetTempSwitch
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : 0 £ºCelsius   1 £º Fahrenheit 
* COMMENTS : 
* CREATOR  : SongXu                   DATE: 2017-03-24
*==========================================================================*/
int DXI_GetTempSwitch(IN int iFunType,		//0 for coeficient 1 for value
					IN float fTempDataOld,
					OUT float *fTempData,
					OUT char *cTempUnit)
{
	int iTempMode = 0;
	iTempMode = GetTempSwitchMode();
	//0 £ºCelsius   1 £º Fahrenheit 
	if(iTempMode == CELSIUS)
	{
		if(cTempUnit != NULL)
		{
			if(iFunType == TEMP_COE)
			{
				snprintf(cTempUnit,(4+sizeof(TEMP_C_UNIT)), "mV/%s",TEMP_C_UNIT);
			}
			else
			{
				snprintf(cTempUnit,sizeof(TEMP_C_UNIT), TEMP_C_UNIT);
			}
		}
		*fTempData = fTempDataOld;
		return CELSIUS;
	}
	else
	{
		if(cTempUnit != NULL)
		{
			if(iFunType == TEMP_COE)
			{
				snprintf(cTempUnit,(4+sizeof(TEMP_F_UNIT)), "mV/%s",TEMP_F_UNIT);
			}
			else
			{
				snprintf(cTempUnit,sizeof(TEMP_F_UNIT), TEMP_F_UNIT);
			}
		}
		if(iFunType == TEMP_COE)
		{
			*fTempData = fTempDataOld / 1.8;
		}
		else
		{
			*fTempData = 1.8 * fTempDataOld +32;
		}
		return FAHRENHEIT;
	}
}
/*==========================================================================*
* FUNCTION : DXI_SetTempSwitch
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : 0 £ºCelsius   1 £º Fahrenheit 
* COMMENTS : 
* CREATOR  : SongXu                   DATE: 2017-03-24
*==========================================================================*/
int DXI_SetTempSwitch(IN int iFunType,		//0 for coeficient 1 for value
					float fTempDataOld,
					float *fTempData)
{
	int iTempMode = 0;
	iTempMode = GetTempSwitchMode();
	//0 £ºCelsius   1 £º Fahrenheit 
	if(iTempMode == CELSIUS)
	{
		*fTempData = fTempDataOld;
		return CELSIUS;
	}
	else
	{
		if(iFunType == TEMP_COE)
		{
			*fTempData = fTempDataOld * 1.8;
		}
		else
		{
			*fTempData = (fTempDataOld - 32) / 1.8;
		}
		return FAHRENHEIT;
	}
}
/*==========================================================================*
* FUNCTION : DXI_GetTimeFormat
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : 0 £ºEMEA	1 £ºESNA	2 : CN
* COMMENTS : 
* CREATOR  : SongXu                   DATE: 2017-03-24
*==========================================================================*/
int DXI_GetTimeFormat()
{
#define SIGNAL_ID_TIME_FORMAT	456
static SIG_BASIC_VALUE* s_pSNMPTimeFmtSigValue = NULL;

	int iBufLen, iRetTimeFor = 2;
	if (s_pSNMPTimeFmtSigValue == NULL)
	{
		//get the time format signal value.
		DxiGetData(VAR_A_SIGNAL_VALUE,
			1,				
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SIGNAL_ID_TIME_FORMAT),
			&iBufLen,
			(void *)&s_pSNMPTimeFmtSigValue,
			0);
	}
	if (s_pSNMPTimeFmtSigValue != NULL)
	{
		iRetTimeFor = s_pSNMPTimeFmtSigValue->varValue.enumValue;
	}
	return iRetTimeFor;
}


/*==========================================================================*
* FUNCTION : DXI_isTempSigUnit
* PURPOSE  : 
* CALLS    : 
* CALLED BY: DXI_isTempSignal
* ARGUMENTS:  
* RETURN   : TRUE is temperature UNIT; FALSE is not a temperature UNIT
* COMMENTS : 
* CREATOR  : SongXu                   DATE: 2017-07-18
*==========================================================================*/
#define DEG_C	"deg.C"

BOOL DXI_isTempSigUnit(char *cSigUnit)
{
	if(cSigUnit != NULL)
	{
		if(strcmp(cSigUnit, DEG_C)==0)
		{
			return TRUE;
		}
	}
	
	return FALSE;
}

/*==========================================================================*
* FUNCTION : DXI_isTempSignal
* PURPOSE  : 
* CALLS    : DXI_isTempSigUnit
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : TRUE is temperature signal; FALSE is not a temperature signal
* COMMENTS : 
* CREATOR  : SongXu                   DATE: 2017-07-18
*==========================================================================*/
BOOL DXI_isTempSignal(SIG_BASIC_VALUE* pSigValue)
{
	char cSigUnit[8];
	if(pSigValue == NULL)
	{
		return FALSE;
	}

	if(pSigValue->ucSigType == SIG_TYPE_SAMPLING)
	{
		strncpyz(cSigUnit, ((SAMPLE_SIG_VALUE*)pSigValue)->pStdSig->szSigUnit, 8);
	}
	else if (pSigValue->ucSigType == SIG_TYPE_SETTING)
	{
		strncpyz(cSigUnit, ((SET_SIG_VALUE*)pSigValue)->pStdSig->szSigUnit, 8);
	}
	else if (pSigValue->ucSigType == SIG_TYPE_CONTROL)
	{
		strncpyz(cSigUnit, ((CTRL_SIG_VALUE*)pSigValue)->pStdSig->szSigUnit, 8);
	}
	else
	{
		return FALSE;
	}

	return DXI_isTempSigUnit(cSigUnit);

}

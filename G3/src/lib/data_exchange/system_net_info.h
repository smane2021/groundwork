/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : system_net_info.h
*  CREATOR  : HULONGWEN                DATE: 2004-09-17 10:04
*  VERSION  : V1.00
*  PURPOSE  : Get and set linux system net info
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __SYSTEM_NET_INFO_H__040917
#define __SYSTEM_NET_INFO_H__040917

enum RET_GET_DEFAULT_GATEWAY
{
	RET_SUCCESS = 0,
	RET_OPEN_FILE_ERROR,
	RET_NOT_FIND
};

/*==========================================================================*
* FUNCTION : GetNetworkInfo
* PURPOSE  : Get network info to linux system
* CALLS    : GetLanInfo,GetDefaultGateway
* CALLED BY: GetACUNetInfo
* ARGUMENTS: int    nIfNum         : the index of net adapter
*            int    bGetIp         : Get ip?  --1 is get
*            ULONG  *pulIp         : to return ip address
*            int    bGetMask       : get sub net mask? --1 is get
*            ULONG  *pulMask       : to return net mask
*            int    bGetGateway    : get default gateway? --1 is get
*            ULONG  *pulGateway    : to return default gateway
*            int    bBroardcast    : get broardcast address? --1 is get
*            ULONG  *pulBroardcast : to return broardcast address
* RETURN   : int : -1(create socket fail)
*			  	   0 (get successfully)
*			  	   1 (get fail)
*			  	   2 (no item to get)
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-17 11:34
*==========================================================================*/
int GetNetworkInfo(int nIfNum, 
				   int bGetIp,		ULONG *pulIp,
				   int bGetMask,	ULONG *pulMask,
				   int bGetGateway,	ULONG *pulGateway, 
				   int bBroardcast,	ULONG *pulBroardcast);

/*==========================================================================*
* FUNCTION : SetNetworkInfo
* PURPOSE  : Set network info to linux system
* CALLS    : SetLanInfo,SetLocalRoute
* CALLED BY: SetACUNetInfo
* ARGUMENTS: int    nIfNum       : the index of net adapter
*            int    bSetIp       : set ip?  --1 is set
*            ULONG  ulIp         : ip address
*            int    bSetMask     : set sub net mask? --1 is set
*            ULONG  ulMask       : net mask
*            int    bSetGateway  : set default gateway? --1 is set
*            ULONG  ulGateway    : default gateway
*            int    bBroardcast  : set broardcast address? --1 is set
*            ULONG  ulBroardcast : broardcast address
* RETURN   : int : -1(create socket fail)
*				   0 (set successfully)
*				   1 (set fail)
*				   2 (no item to set)
*
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-17 11:15
*==========================================================================*/
int SetNetworkInfo(int nIfNum, 
				   BOOL bSetIp,		ULONG ulIp,
				   int bSetMask,	ULONG ulMask,
				   int bSetGateway,	ULONG ulGateway, 
				   int bBroardcast,	ULONG ulBroardcast);


//added by Jimmy 2014.08.12
int GetIPV6_NetworkInfo(int nIfNum, void* pOutInfo);
int SetIPV6_NetworkInfo(int nIfNum, int nChangeType, void* pInInfo);

#endif //__SYSTEM_NET_INFO_H__040917

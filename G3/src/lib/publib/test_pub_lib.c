/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : test_pub_lib.c
 *  CREATOR  : Mao Fuhua                DATE: 2004-10-12 18:57
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

#include "stdsys.h"
#include "basetypes.h"
#include "new.h"
#include "pubfunc.h"
#include "err_code.h"
#include "run_mutex.h"
#include "run_queue.h"
#include "run_thread.h"
#include "run_timer.h"

static int ArgToInt( char *pszArg, int nMax, int nMin)
{
	int v = atoi(pszArg);

	if( v > nMax)
	{
		return nMax;
	}

	if(v < nMin)
	{
		return nMin;
	}

	return v;
}

typedef	int (*TEST_API_PROC)( int nTestArgs, char *pTestArgs[] ); 

struct STestAPI
{
	char			*strNameAPI;		// API name
	char			*strArgsRequired;	// args name
	int				nArgsRequired;	// No. of args
	TEST_API_PROC	pfnTest; 
};

#define ADD_TEST_API( name, pArgs, nArgs, test )	\
	{\
		extern int test( int nTestArgs, char *pTestArgs[] ); \
		\
		pTestAPI[nTestAPI].strNameAPI = (char *)(name);\
		pTestAPI[nTestAPI].strArgsRequired = (char *)(pArgs);\
		pTestAPI[nTestAPI].nArgsRequired = (int)(nArgs);\
		pTestAPI[nTestAPI].pfnTest = (TEST_API_PROC)(test);\
		nTestAPI ++;\
	}


#define MAX_TEST_API		20

// test mutex
static DWORD g_dwLockWaitTime	=	1000;
static DWORD LOCKED_SLEEP_TIME = 0;
static DWORD Test_MutexThread(HANDLE hm)
{
	int		rc;
	HANDLE	id = RunThread_GetId(NULL);
	double  tmStart, tmEnd;

	while (THREAD_IS_RUNNING(id))
	{
		RunThread_Heartbeat(id);

		tmStart = GetCurrentTime();
		printf("[Test_MutexThread] -- %p: Mutex_Lock(hm, %u) at %lf.\n", 
			id, (unsigned int)g_dwLockWaitTime, tmStart);
		fflush(stdout);

		rc = Mutex_Lock(hm, g_dwLockWaitTime);

		tmEnd = GetCurrentTime();
		printf("[Test_MutexThread] -- %p: Mutex_Lock(hm, %u) got %08x at %lf(%lf).\n",
			id, (unsigned int)g_dwLockWaitTime, rc, tmEnd, tmEnd-tmStart);
		fflush(stdout);

		if (rc == ERR_MUTEX_OK)
		{
			if (LOCKED_SLEEP_TIME > 0)
			{
				printf("[Test_MutexThread] -- %p: Lock done, sleep %d ms\n", 
					id, (int)LOCKED_SLEEP_TIME);
				fflush(stdout);

				Sleep(LOCKED_SLEEP_TIME);
			}

			printf("[Test_MutexThread] -- %p: Unlock()\n", id);
			fflush(stdout);

			Mutex_Unlock(hm);
		}
	}

	printf("[Test_MutexThread] -- %p: exiting.\n", id);
	fflush(stdout);

	return 0;
}


DWORD RunThreadEventHandler(		
	DWORD	dwThreadEvent,		//	the thread event. below.
	HANDLE	hThread,				//	The thread id.
	const char *pszThreadName);

int Test_Mutex(int nTestArgs, char *pTestArgs[])
{
	HANDLE hm;
#define _MUTEX_TEST_THREADS 100
	char	szName[20];
	HANDLE	phThread[_MUTEX_TEST_THREADS];
	DWORD	pdwExitCodes[_MUTEX_TEST_THREADS];
	int	i;
	BOOL bError = FALSE;
	BOOL    bOpenedLock = TRUE;
	int		nMutexThreadToCreate = atoi(pTestArgs[0]);

	UNUSED(nTestArgs);
	UNUSED(pTestArgs);

	nMutexThreadToCreate = ArgToInt(pTestArgs[0], _MUTEX_TEST_THREADS, 0);
	g_dwLockWaitTime		 = ArgToInt(pTestArgs[1], 1000000, 0);
	LOCKED_SLEEP_TIME    = ArgToInt(pTestArgs[2], 1000000, 0);

	printf("[Test_Mutex] --  Mutex_Create(FALSE).\n");
	hm = Mutex_Create(bOpenedLock);

	ZERO_POBJS(phThread, nMutexThreadToCreate);

	for (i = 0; i < nMutexThreadToCreate; i++)
	{
		sprintf(szName, "MutexThrd%d", i);

		phThread[i] = RunThread_Create(szName,
			(RUN_THREAD_START_PROC)Test_MutexThread,
			(void *)hm,
			&pdwExitCodes[i],
			0);

		if (phThread[i] == NULL)
		{
			printf("[Test_Mutex] -- Fails on creating thread %s, quit.\n",
				szName);
			bError = TRUE;
			break;
		}
	}


	printf("[Test_Mutex] --  Mutex_Unlock(hm).\n");
	if (!bOpenedLock)
	{
		Sleep(1000);
		Mutex_Unlock(hm);
	}

	printf( "[Test_Mutex] --  Wait thread quit...\n");
	while ((i = RunThread_GetThreadCount()) > 0) // there is a thread mgr
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));
//		printf( "[Test_Mutex] -- Current threads is %d.\n", i);
		Sleep(2000);
	}

	printf( "[Test_Mutex] --  quit...\n");

	printf("[Test_Mutex] --  Mutex_Unlock(hm).\n");
    Mutex_Destroy(hm);

	printf("[Test_Mutex] --  end test.\n");

	return 0;
}

// test queue
#define MAX_QUEUE_PRODUCERS		100
#define MAX_QUEUE_CONSUMERS		100
#define MAX_QUEUE_PRODUCTS		100

typedef struct __QUEUE_PRODUCT
{
	int		nId;		// product id
	int		nProductSerial;
	int		nProducer;
	time_t  tProductedTime;
}QUEUE_PRODUCT;

typedef struct _THREAD_INFO
{
	HANDLE		hThread;
	DWORD		dwExitCode;
}THREAD_INFO;


#define QUEUE_WAIT_TIME		2000

static DWORD Test_QueueProducer(HANDLE hQ)
{
	HANDLE			hSelf = RunThread_GetId(NULL);
	int				nTatol = 0;
	QUEUE_PRODUCT	prod;
	RUN_THREAD_MSG msg;

	printf("[Test_QueueProducer] -- Thread %p is starting.\n",
		hSelf);

	Timer_Set(hSelf, (int)hSelf, 1000, NULL, 500);
	Timer_Set(hSelf, (int)((int)hSelf<<1), 5000, NULL, 5000);

	while (THREAD_IS_RUNNING(hSelf))
	{
		RunThread_Heartbeat(hSelf);

		if (RunThread_GetMessage(hSelf, &msg, FALSE, 1000) == ERR_OK)
		{
			if(msg.dwMsg == MSG_TIMER)
			{
				printf( "[Test_QueueProducer] -- Thread %p recv timer %d.\n",
					hSelf, (int)msg.dwParam1);
				//Sleep(msg.dwParam2);
			}
		}
		else
		{
			Sleep(100);
			continue;
		}

		prod.nId			= (int)hSelf&0xff;
		prod.nProducer		= (int)hSelf;
		prod.nProductSerial = ++nTatol;
		prod.tProductedTime = time(NULL);

		printf( "[Test_QueueProducer] -- Thread %p has producted: %d,%d,%d,%d.\n",
			hSelf, prod.nProducer, (int)prod.tProductedTime, prod.nId, prod.nProductSerial);

		while (Queue_Put(hQ, (void*)&prod, FALSE) != ERR_QUEUE_OK)
		{
			if (!THREAD_IS_RUNNING(hSelf))
			{
				break;
			}

			Sleep(1000);
		}

		Sleep(100);
	}

	printf("[Test_QueueProducer] -- Thread %p exits, producted %d products.\n",
		hSelf, nTatol);

	return nTatol;
}

static DWORD Test_QueueConsumer(HANDLE hQ)
{
	HANDLE			hSelf = RunThread_GetId(NULL);
	int				nTatol = 0;
	QUEUE_PRODUCT	prod;

	printf("[Test_QueueConsumer] -- Thread %p is starting.\n",
		hSelf);

	while (THREAD_IS_RUNNING(hSelf))
	{
		RunThread_Heartbeat(hSelf);

		if(Queue_Get(hQ, (void*)&prod, FALSE,QUEUE_WAIT_TIME) == ERR_QUEUE_OK)
		{
			printf( "[Test_QueueConsumer] -- Thread %p has consumed(%d): %d,%d,%d,%d.\n",
				hSelf, nTatol, prod.nProducer, (int)prod.tProductedTime, prod.nId, prod.nProductSerial);
			nTatol ++;
		}
		else
		{
			printf( "[Test_QueueConsumer] -- Thread %p has consumed nothing.\n",
				hSelf);
		}
	}

	printf("[Test_QueueConsumer] -- Thread %p exits, consumed %d products.\n",
		hSelf, nTatol);

	return nTatol;
}

typedef struct _TEST_QUEUE_ARGS
{
	int nProductLine;
	int nProducers;
	int nConsumers;
	int nQueueSize;
}TEST_QUEUE_ARGS;

static int Test_QueueAfterFixBuf(int nQueueSize)
{
	HANDLE			hQ;
	int i;

	hQ = Queue_Create(nQueueSize, sizeof(QUEUE_PRODUCT),nQueueSize/2 );

	///////////
	for (i = 0; i < nQueueSize*4; i++)
	{
		QUEUE_PRODUCT	prod;

		prod.nId			= i;
		prod.nProducer		= 0;
		prod.nProductSerial = i/2+i%2;
		prod.tProductedTime = 0;//time(NULL);

		printf( "Put: %d: prod.nID = %d, serial=%d.\n",
			i, prod.nId, prod.nProductSerial);

		Queue_Put(hQ, &prod, i%2);

		if (i%5 == 0)
		{
			Queue_Get(hQ, &prod, FALSE, 0);
			printf( "            Get: %d: prod.nID = %d, serial=%d.\n",
				i, prod.nId, prod.nProductSerial);
		}
	}	

	printf( "To get all ater putting all.\n");

	for (i = 0; ; i++)
	{
		QUEUE_PRODUCT	prod;

		if (Queue_GetEx(hQ, i, &prod, TRUE) == ERR_QUEUE_OK)
		{
			printf( "GetEx: %d: prod.nID = %d, serial=%d.\n",
				i, prod.nId, prod.nProductSerial);
		}
		else
		{
			printf( "GetEx: %d: queue is empty, exit.\n",
				i);
			break;
		}
	}

	Queue_Empty(hQ);
	Queue_Destroy(hQ);

	getchar();

	return 0;
}

static int Test_QueueProductProc(TEST_QUEUE_ARGS *pArg)
{
	HANDLE			hSelf = RunThread_GetId(NULL);
	THREAD_INFO		pProducers[MAX_QUEUE_PRODUCERS];
	THREAD_INFO		pConsumers[MAX_QUEUE_PRODUCERS];
	int				i;
	int				nCreatedConsumers = 0;
	int				nCreatedProducers = 0;
	char			szName[32];
	HANDLE			hQ;
	int				nProductLine = pArg->nProductLine;
	int				nProducers   = pArg->nProducers;
	int				nConsumers   = pArg->nConsumers;
	int				nQueueSize   = pArg->nQueueSize;

	DELETE(pArg);

	printf("[Test_QueueProductProc] -- %p started and creating queue(%d,%d).\n",
		hSelf, nQueueSize, sizeof(QUEUE_PRODUCT));
	
	hQ = Queue_Create(nQueueSize, sizeof(QUEUE_PRODUCT),nQueueSize/2 );
	if (hQ == NULL)
	{
		printf("[Test_QueueProductProc] -- %p fails on creating queue(%d,%d).\n",
			hSelf, nQueueSize, sizeof(QUEUE_PRODUCT));
		return -1;
	}

	// create consumers
	printf("[Test_QueueProductProc] -- %p is creating %d consumer threads.\n",
		hSelf, nConsumers);

	for (i = 0; i < nConsumers; i++)
	{
		sprintf(szName, "Csmr%d.%d", nProductLine, i);

		pConsumers[i].hThread = RunThread_Create(szName, 
			(RUN_THREAD_START_PROC)Test_QueueConsumer,
			(void *)hQ,
			&pConsumers[i].dwExitCode,
			0);

		if (pConsumers[i].hThread != NULL)
		{
			printf("[Test_QueueProductProc] -- %p created #%d consumer %p.\n",
				hSelf, i, pConsumers[i].hThread);
		}
		else
		{
			printf("[Test_QueueProductProc] -- %p fails on creating #%d consumer.\n",
				hSelf, i);
			break;
		}
	}

	if (i == 0)	// no consumer.
	{
		printf("[Test_QueueProductProc] -- %p, no any consumer created, exits.\n",
			hSelf);
		goto __ExitProduct;
	}

	nCreatedConsumers = i;
	
	printf("[Test_QueueProductProc] -- %p creates %d consumers.\n",
		hSelf, nCreatedConsumers);

	// create producers
	printf("[Test_QueueProductProc] -- %p is creating %d producer threads.\n",
		hSelf, nProducers);

	for (i = 0; i < nProducers; i++)
	{
		sprintf(szName, "Prdr%d.%d", nProductLine, i);

		pProducers[i].hThread = RunThread_Create(szName, 
			(RUN_THREAD_START_PROC)Test_QueueProducer,
			(void *)hQ,
			&pProducers[i].dwExitCode,
			RUN_THREAD_FLAG_HAS_MSG);	// with msg queue.

		if (pProducers[i].hThread != NULL)
		{
			printf("[Test_QueueProductProc] -- %p created #%d producer %p.\n",
				hSelf, i, pProducers[i].hThread);
		}
		else
		{
			printf("[Test_QueueProductProc] -- %p fails on creating #%d producer.\n",
				hSelf, i);
			break;
		}
	}

	if (i == 0)	// no producer.
	{
		printf("[Test_QueueProductProc] -- %p, no any producer created, exits.\n",
			hSelf);
		goto __ExitProduct;
	}

	nCreatedProducers = i;

	printf("[Test_QueueProductProc] -- %p creates %d producers.\n",
		hSelf, nCreatedProducers);

	// 3. run
	while (THREAD_IS_RUNNING(hSelf))
	{
		RunThread_Heartbeat(hSelf);

		Sleep(1000);
	}
   

__ExitProduct:
	// stop producers
	printf("[Test_QueueProductProc] -- %p stops %d producers.\n",
		hSelf, nCreatedProducers);

	RunThread_Heartbeat(hSelf);
	for (i = 0; i < nCreatedProducers; i++)
	{
		printf("[Test_QueueProductProc] -- %p sends quit msg to producer %p.\n",
			hSelf, pProducers[i].hThread);

		RunThread_PostQuitMessage(pProducers[i].hThread);
	}

	RunThread_Heartbeat(hSelf);
	for (i = 0; i < nCreatedProducers; i++)
	{
		printf("[Test_QueueProductProc] -- %p waits for producer %p exiting.\n",
			hSelf, pProducers[i].hThread);

		while (RunThread_GetStatus(pProducers[i].hThread) != RUN_THREAD_IS_INVALID)
		{
			RunThread_Heartbeat(hSelf);
			Sleep(1000);
		}

		printf("[Test_QueueProductProc] -- %p, the producer %p exited with %d.\n",
			hSelf, pProducers[i].hThread, (int)pProducers[i].dwExitCode);
	}
	
	// stop consumers
	printf("[Test_QueueProductProc] -- %p stops %d consumers.\n",
		hSelf, nCreatedConsumers);

	RunThread_Heartbeat(hSelf);
	for (i = 0; i < nCreatedConsumers; i++)
	{
		printf("[Test_QueueProductProc] -- %p sends quit msg to consumer %p.\n",
			hSelf, pConsumers[i].hThread);

		RunThread_PostQuitMessage(pConsumers[i].hThread);
	}

	RunThread_Heartbeat(hSelf);
	for (i = 0; i < nCreatedConsumers; i++)
	{
		printf("[Test_QueueProductProc] -- %p waits for consumer %p exiting.\n",
			hSelf, pConsumers[i].hThread);

		while (RunThread_GetStatus(pConsumers[i].hThread) != RUN_THREAD_IS_INVALID)
		{
			RunThread_Heartbeat(hSelf);
			Sleep(1000);
		}

		printf("[Test_QueueProductProc] -- %p, the consumer %p exited with %d.\n",
			hSelf, pConsumers[i].hThread, (int)pConsumers[i].dwExitCode);
	}

	//destroy q
	printf("[Test_QueueProductProc] -- %p destroy the queue and exit.\n",
		hSelf);

	Queue_Destroy(hQ);

	return ((nCreatedProducers != 0)&&(nCreatedConsumers != 0)) 
		? (nCreatedProducers+nCreatedConsumers): -1;
}

int Test_Queue(int nTestArgs, char *pTestArgs[])
{
	int nProducts  = ArgToInt(pTestArgs[0], MAX_QUEUE_PRODUCTS, 1);
	int nProducers = ArgToInt(pTestArgs[1], MAX_QUEUE_PRODUCERS, 1);
	int nConsumers = ArgToInt(pTestArgs[2], MAX_QUEUE_CONSUMERS, 1);
	int nQueueSize = ArgToInt(pTestArgs[3], 100000, 1);
	THREAD_INFO		 pProducts[MAX_QUEUE_PRODUCERS];
	char			szName[32];
	TEST_QUEUE_ARGS *pArg;

	int i;
	int nCreatedProducts = 0;

	UNUSED(nTestArgs);

	printf("[Test_Queue] -- starting to create %d product lines.\n",
		nProducts);

///
	Test_QueueAfterFixBuf(nQueueSize);
///

	for (i = 0; i < nProducts; i++)
	{
		sprintf(szName, "Prln%d", i);

		pArg = NEW(TEST_QUEUE_ARGS, 1);
		ASSERT(pArg);

		pArg->nProductLine = i;
		pArg->nProducers   = nProducers;
		pArg->nConsumers   = nConsumers;
		pArg->nQueueSize   = nQueueSize;

		pProducts[i].hThread = RunThread_Create(szName, 
			(RUN_THREAD_START_PROC)Test_QueueProductProc,
			(void *)pArg,
			&pProducts[i].dwExitCode,
			0);

		if (pProducts[i].hThread != NULL)
		{
			printf("[Test_Queue] -- created #%d product line %p.\n",
				i, pProducts[i].hThread);
		}
		else
		{
			printf("[Test_Queue] -- fails on creating #%d product line.\n",
				i);
			break;
		}
	}

	if (i == 0)	// no producer.
	{
		printf("[Test_Queue] -- no any product line created, exits.\n");
		return -1;
	}

	nCreatedProducts = i;

	printf( "[Test_Queue] -- press Enter to quit...\n");
    getchar();

	// stop consumers
	printf("[Test_Queue] -- stops %d product lines.\n",
		nCreatedProducts);

	for (i = 0; i < nCreatedProducts; i++)
	{
		printf("[Test_Queue] -- sends quit msg to product lines %p.\n",
			pProducts[i].hThread);

		RunThread_PostQuitMessage(pProducts[i].hThread);
	}

	for (i = 0; i < nCreatedProducts; i++)
	{
		printf("[Test_Queue] -- waits for product line %p exiting.\n",
			pProducts[i].hThread);

		while (RunThread_GetStatus(pProducts[i].hThread) != RUN_THREAD_IS_INVALID)
		{
			Sleep(1000);
		}

		printf("[Test_Queue] -- the product line %p exited with %d.\n",
			pProducts[i].hThread, (int)pProducts[i].dwExitCode);
	}

	return 0;
}

// test timer

static int MY_ON_TIMER_PROC(HANDLE hself, int idTimer, void *par)
{
	UNUSED(hself);

	TRACEX("On timer %d, at %lf\n", idTimer, GetCurrentTime());
	if (par != 0)
	{
		(*(int *)par)++;
		if ((*(int *)par) > idTimer)
		{
			return TIMER_KILL_THIS;
		}
	}

	return 0;
}


int Test_Timer(int nTestArgs, char *pTestArgs[])
{
	int i;

	{
		int i;
		for (i=0; i < 1000; i++)
		{
			TRACEX("Sleep 1000ms.\n");
			Sleep(1000);

			if (i == 5)
			{
				TRACEX("Set timer.\n");
				// set the timer to process delaying alarms.
				Timer_Set( NULL, 1000, 1000, MY_ON_TIMER_PROC, (DWORD)NULL);
			}
		}
	}


	printf("Creating timer...\n");
	i = Timer_Set(NULL, 0, 1000, NULL, 0);
	printf("Creating timer...DONE, got %d(%x)..\n", i, i);

	for (i = 0; i < nTestArgs; i++)
	{
		int	nID, nInterval;
		sscanf(pTestArgs[i], "%d=%d", &nID, &nInterval);

		printf("Creating timer %d with interval %d, got %x.\n",
			nID, nInterval,

		Timer_Set( NULL, nID, nInterval, MY_ON_TIMER_PROC, (DWORD)NULL));
	}

	printf( "Press Enter to kill timer.\n");
	getchar();

	for (i = 0; i < nTestArgs; i++)
	{
		int	nID, nInterval;
		sscanf(pTestArgs[i], "%d=%d", &nID, &nInterval);

		printf("Killing timer %d.\n", nID);

		Timer_Kill( NULL, nID);
	}

	return 0;
}


int Test_TimerAuto(int nTestArgs, char *pTestArgs[])
{
	int i = 0;
	int n = 0;

	UNUSED(nTestArgs);
	UNUSED(pTestArgs);

	Timer_Set( NULL, 100, 200, MY_ON_TIMER_PROC, (DWORD)&i);
	Timer_Set( NULL, 200, 400, MY_ON_TIMER_PROC, (DWORD)&i);
	Timer_Set( NULL, 300, 600, MY_ON_TIMER_PROC, (DWORD)NULL);

	while(++n) 
	{
		if (n == 10)
		{
			i  = 0;
			Timer_Set( NULL, 100, 20, MY_ON_TIMER_PROC, (DWORD)&i);
			Timer_Set( NULL, 200, 400, MY_ON_TIMER_PROC, (DWORD)&i);
		}

		TRACEX("Now is %lf\n", GetCurrentTime());

		Sleep(1000);
	}

	return 0;
}

// test thread
#define MSG_COUNT MSG_USER+1
//static BOOL g_bQuit = FALSE;
//static BOOL g_bQuitConsumber = FALSE;

DWORD Test_ConsumerThreadProc(HANDLE hProducer)
{
//	BOOL	bQuit = FALSE;
	HANDLE	hSelf = RunThread_GetId(NULL);
	int		n = 0;
	RUN_THREAD_MSG msg;

#define GET_MSG_WAIT_TIME	10000

	printf("[Test_ConsumerThreadProc] -- Thread %p(%p) is starting.\n",
		hSelf, (HANDLE)pthread_self());

	Sleep(2000);

	while (RunThread_GetStatus(hSelf) == RUN_THREAD_IS_RUNNING)
//	while (!bQuit)
//	while (!g_bQuitConsumber)
	{
		RunThread_Heartbeat(hSelf);
		printf( "[Test_ConsumerThreadProc] -- Thread %p is running, %d.	\n",
			hSelf, ++n);

		if (RunThread_GetMessage(hProducer, &msg, FALSE, GET_MSG_WAIT_TIME) 
			== ERR_OK)
		{
			switch (msg.dwMsg)
			{
			case MSG_COUNT:
				printf( "[Test_ConsumerThreadProc] -- Thread %p received %d count %d-%d.\n",
					hSelf, ++n, (int)msg.dwParam1, (int)msg.dwParam2);
				break;
			case MSG_QUIT:
				printf( "[Test_ConsumerThreadProc] -- Thread %p received quit msg, quit.\n",
					hSelf);
//				bQuit = TRUE;
				break;
			default:
				printf( "[Test_ConsumerThreadProc] -- Thread %p received unknown msg %d, quit.\n",
					hSelf, (int)msg.dwMsg);
			}
		}

	}

	printf("[Test_ConsumerThreadProc] -- Thread %p end.\n",
		hSelf);

	return (DWORD)n;
}

#define MAX_CONSUMER_THREADS	10
DWORD Test_ThreadProc(int nConsumersToCreate)
{
	HANDLE	hSelf = RunThread_GetId(NULL);
	int		n = 0, nCount;
	RUN_THREAD_MSG msg;
	static int nConsumerThreads = 0;
	char	szConsumerName[200];
	HANDLE  hConsumer[MAX_CONSUMER_THREADS];
	DWORD	dwConsumerExitCode[MAX_CONSUMER_THREADS];
	int		nCreatedConsumers;

	printf("[Test_ThreadProc] -- Thread %p(%p) is starting. PID is %d\n",
		hSelf, (HANDLE)pthread_self(), getpid());

	//create the consumer thread
	for (nCreatedConsumers = 0; nCreatedConsumers < nConsumersToCreate; nCreatedConsumers++)
	{
		sprintf( szConsumerName, "Cnsmr%d", ++nConsumerThreads);

		hConsumer[nCreatedConsumers] = RunThread_Create(szConsumerName,
			Test_ConsumerThreadProc,
			(void *)hSelf,
			&dwConsumerExitCode[nCreatedConsumers],
			0);
		if (hConsumer[nCreatedConsumers] == NULL)
		{
			printf("[Test_ThreadProc] -- Fails on creating the sonsumer thread.\n");
			break;
		}

		printf("[Test_ThreadProc] -- Creating the consumer thread %d(%p)...\n", 
			nCreatedConsumers, hConsumer[nCreatedConsumers]);
	}

	if (nCreatedConsumers == 0)
	{
		return (DWORD)-1;
	}

	while (RunThread_GetStatus(hSelf) == RUN_THREAD_IS_RUNNING)
//	while (!g_bQuit)
	{
		RunThread_Heartbeat(hSelf);

		nCount = rand()%10000;
		printf( "[Test_ThreadProc] -- Thread %p is running, %d-%d.	\n",
			hSelf, ++n, nCount);

		RUN_THREAD_MAKE_MSG( &msg, hSelf, MSG_COUNT, n, nCount);
		while (RunThread_PostMessage(hSelf,	// send to itself
			&msg,
			FALSE) == ERR_THREAD_MSG_FULL)
		{
			if (RunThread_GetStatus(hSelf) != RUN_THREAD_IS_RUNNING)
			{
				break;
			}

			Sleep((DWORD)(rand()%1000));
		}

		Sleep((DWORD)(rand()%100));	// discard cpu
	}	

//	g_bQuitConsumber = TRUE;
//	Sleep(10);

	// to quit the consumers
	for (nCount = 0; nCount < nCreatedConsumers; nCount++)
	{
		RunThread_PostQuitMessage(hConsumer[nCount]);
	}

	for (nCount = 0; nCount < nCreatedConsumers; nCount++)
	{
		printf("[Test_ThreadProc] -- waiting the consumer thread %p to exit.\n",
			hConsumer[nCount]);

		// when the thread quit done, status will return RUN_THREAD_IS_INVALID
		while (RunThread_GetStatus(hConsumer[nCount]) != RUN_THREAD_IS_INVALID)
		{
			RunThread_Heartbeat(hSelf);
			Sleep(1000);
		}

		printf("[Test_ThreadProc] -- Consumer thread %p exited with %d.\n",
			hConsumer[nCount], (int)dwConsumerExitCode[nCount]);
	}

	printf("[Test_ThreadProc] -- Thread %p end.\n",
		hSelf);

	return (DWORD)n;
}

int Test_Thread(int nTestArgs, char *pTestArgs[])
{
#define _MAX_TEST_THREADS 500
	char	szName[200];
	HANDLE	phThread[_MAX_TEST_THREADS];
	DWORD	pdwExitCodes[_MAX_TEST_THREADS];
	int	i;
	BOOL bError = FALSE;
	int nThreadToCreate,  nThreadCreated;
	int nConsumersToCreate;

	UNUSED(nTestArgs);

	srand((unsigned int)time(NULL));

	//g_bQuit = FALSE;

	nThreadToCreate = ArgToInt(pTestArgs[0], _MAX_TEST_THREADS, 0);
	nConsumersToCreate= ArgToInt(pTestArgs[1], MAX_CONSUMER_THREADS, 1);

	printf("[Test_Thread] --  Creating %d threads per thread with %d consumers.\n",
		nThreadToCreate, nConsumersToCreate);

	for (i = 0; (i < nThreadToCreate); i++)
	{
		sprintf(szName, "RunThrd%d", i);

		phThread[i] = RunThread_Create(szName,
			(RUN_THREAD_START_PROC)Test_ThreadProc,
			(void *)nConsumersToCreate,
			&pdwExitCodes[i],
			RUN_THREAD_FLAG_HAS_MSG);

		if (phThread[i] == NULL)
		{
			printf("[Test_Thread] -- Fails on creating thread %s, quit.\n",
				szName);
			bError = TRUE;
			break;
		}
	}

	nThreadCreated = i;
	printf( "[Test_Thread] --  %d thread are created press Enter to quit...\n",
		nThreadCreated);
	getchar();

	for ( i = nThreadCreated-1; i >= 0; i--)
	{
		printf("[Test_Thread] -- Sending quit message to thread %p.\n",
			phThread[i]);

//		g_bQuit = TRUE;
		RunThread_PostQuitMessage(phThread[i]);
	}

	printf("[Test_Thread] -- Waiting for threads to quit.\n");
	for ( i = nThreadCreated-1; i >= 0; i--)
	{
		// when the thread quit done, status will return RUN_THREAD_IS_INVALID
		printf("[Test_Thread] -- waiting the producer thread %p to exit.\n",
			phThread[i]);

		while (RunThread_GetStatus(phThread[i]) != RUN_THREAD_IS_INVALID)
		{
			Sleep(1000);
		}

		printf("[Test_Thread] -- producer thread %p exited with %d.\n",
			phThread[i], (int)pdwExitCodes[i]);
	}


	while ((i = RunThread_GetThreadCount()) > 0) // there is a thread mgr
	{
		printf( "[Test_Thread] -- Current threads is %d.\n", i);
		Sleep(1000);
	}

	printf("[Test_Thread] -- Check for exit code of threads.\n");
	for ( i = nThreadCreated-1; i >= 0; i--)
	{
		printf("[Test_Thread] -- The thread %p exited with code %d.\n",
			phThread[i], (int)pdwExitCodes[i]);
	}

	printf( "[Test_Thread] --  quited.\n");

	return 0;
}


DWORD RunThreadEventHandler(		
	DWORD	dwThreadEvent,		//	the thread event. below.
	HANDLE	hThread,				//	The thread id.
	const char *pszThreadName)
{
	if (THREAD_EVENT_NO_RESPONSE == dwThreadEvent)
	{
		printf( "[RunThreadEventHandler] -- %p is no response, kill it!\n",
			hThread);
		return THREAD_CANCEL_THIS;
	}

	return THREAD_CONTINUE_RUN;
}


#if 0
static void Handler_SIGSEGV( int n, siginfo_t *pSigInfo, void *a )
{
	printf("[Handler_SIGSEGV] -- caught %d\n", n );

	printf( "si_signo = %d\n", pSigInfo->si_signo);  /* Signal number */
	printf( "si_errno = %d\n", pSigInfo->si_errno);	 /* An errno value */
	printf( "si_code  = %d\n", pSigInfo->si_code);	 /* Signal code */
	printf( "si_pid   = %d\n", pSigInfo->si_pid);	 /* Sending process ID */
	printf( "si_uid   = %d\n", pSigInfo->si_uid);	 /* Real user ID of sending process */
	printf( "si_status= %d\n", pSigInfo->si_status); /* Exit value or signal */
	printf( "si_utime = %d\n", (int)pSigInfo->si_utime);	 /* User time consumed */
	printf( "si_stime = %d\n", (int)pSigInfo->si_stime);	 /* System time consumed */
//	printf( "si_value = %d\n", *(int*)&pSigInfo->si_value);	 /* Signal value */
	printf( "si_int   = %d\n", (int)pSigInfo->si_int);	 /* POSIX.1b signal */
	printf( "si_ptr   = %p\n", pSigInfo->si_ptr);	 /* POSIX.1b signal */
	printf( "si_addr  = %p\n", pSigInfo->si_addr);	 /* Memory location which caused fault */
	printf( "si_band  = %ld\n", pSigInfo->si_band);	 /* Band event */
	printf( "si_fd    = %d\n", pSigInfo->si_fd);	 /* File descriptor */

	printf( "Exited....\n");
	fflush(stdout);
	exit(-1);
}

static int create_new_SIGSEGV(void)
{
  	struct sigaction new_action, old_action;

	/* Set up the structure to specify the new action. */
	(__sighandler_t)new_action.sa_handler = (__sighandler_t)Handler_SIGSEGV;
	sigemptyset (&new_action.sa_mask);
	new_action.sa_flags = SA_SIGINFO;

	sigaction (SIGSEGV, &new_action, &old_action);

	/* set up to catch the SIGSEGV signal */
//	signal( SIGSEGV, Handler_SIGSEGV );
	return 0;
}

/////////////////////
/* routine that is called when timer expires */
static void timer_intr(int sig, siginfo_t *extra, void *cruft)
{
	TRACEX("Intr: Now is %lf\n", GetCurrentTime());
	/* perform periodic processing and then exit */
}



 void t_mytimer_create(int num_secs, int num_nsecs)
{
//	struct sigaction sa;
	struct sigevent sig_spec;
	timer_t timer_h;
	struct itimerspec tmr_setting;

	/* setup signal to respond to timer */
//	sigemptyset(&sa.sa_mask);
//	sa.sa_flags = SA_SIGINFO;
//	sa.sa_sigaction = timer_intr;

//	if (sigaction(SIGRTMIN, &sa, NULL) < 0)
//		perror("sigaction");

//	sig_spec.sigev_notify = SIGEV_SIGNAL;
//	sig_spec.sigev_signo = SIGRTMIN;

	sig_spec.sigev_notify = SIGEV_THREAD;
	sig_spec.sigev_signo = 0;
	sig_spec.sigev_notify_function = (void (*)(sigval_t))timer_intr;
	sig_spec.sigev_value.sival_ptr = 0;

	/* create timer, which uses the REALTIME clock */
	if (timer_create(CLOCK_REALTIME, &sig_spec, &timer_h) < 0)
		perror("timer create");

	/* set the initial expiration and frequency of timer */
	tmr_setting.it_value.tv_sec = 1;
	tmr_setting.it_value.tv_nsec = 0;
	tmr_setting.it_interval.tv_sec = num_secs;
	tmr_setting.it_interval.tv_nsec = num_nsecs;
	if ( timer_settime(timer_h, 0, &tmr_setting,NULL) < 0) 
		perror("settimer");
}
#endif

static void Test_OverrideMem(char *p, int nSize)
{
	int n;

	printf( "Press non-zero to test override mem."); scanf("%d", &n);
	if (n > 0)
	{
		*((long *)&p[nSize]) = n;
	}
	else if(n<0)
	{
		*((long *)&p[n]) = n;
	}

	printf( "Press non-zero to show mem mgr info."); scanf("%d", &n);
	MEM_MGR_SHOW_MSG(n);
}

static int Test_Renew(int nTestArgs, char *pTestArgs[])
{
	int		nSize = 1, nNewSize;
	char	*pOld = 0, *pNew = 0;

	
	while(1)
	{
		printf( "Please input the size of NEW(0 will make RENEW to simulate NEW:");
		scanf("%d", &nSize);

		if (nSize < 0)
		{
			printf( "size must be >= 0. exit...\n");
			break;
		}

		if (nSize > 0)
		{
			pOld = NEW(char, nSize);
			
			Test_OverrideMem(pOld, nSize);
		}
		else
		{
			printf( "Simulate NEW.\n");
			pOld = NULL;
		}

		printf( "NEW %d bytes got addr %p %s\n",
			nSize, pOld, (pOld != NULL) ? "OK!" : "Failure!");


		printf( "Please input the size of RENEW(0 will make RENEW to simulate DELETE:");
		scanf("%d", &nNewSize);

		if (nNewSize < 0)
		{
			printf( "size must be >= 0. the old addr is being released.\n");
			DELETE(pOld);
			continue;
		}
		else if(nNewSize == 0)
		{
			printf( "Simulate DELETE.\n");
		}
		
		pNew = RENEW(char, pOld, nNewSize);
		printf( "RENEW %d bytes got addr %p %s. old size: %d, old addr: %p.\n",
			nNewSize, pNew, (pNew != NULL || nNewSize == 0) ? "OK!" : "Failure!",
			nSize, pOld);

		if (pNew == NULL)
		{
			if (nNewSize != 0)
			{
				DELETE(pOld);
			}
		}
		else
		{
			Test_OverrideMem(pNew, nNewSize);

			DELETE(pNew);
		}
	}

	return 0;
}



struct _SYS_TIME
{
   int nYear;		// actual year with century: 0~yyyy
   int nMonth;		// 1~12
   int nDay;		// 1~31
   int nHour;		// 0~23
   int nMinute;	// 0~59
   int nSecond;	// 0~59
};
typedef struct _SYS_TIME SYSTEM_TIME;


// return null for error.
SYSTEM_TIME *TimetSpilit(IN time_t tmNow,  OUT SYSTEM_TIME *pSysTime)
{
	struct tm gmTime;
    
	// conver time
	if (gmtime_r(tmNow, &gmTime) == NULL)
	{
		return NULL;
	}

	//tm_year: The number of years since 1900.
	pSysTime->nYear  = gmTime.tm_year + 1900;	
	//tm_mon: The number of months since January, in the range 0 to 11.
	pSysTime->nMonth = gmTime.tm_mon + 1;
	//tm_mday:  The day of the month, in the range 1 to 31.
	pSysTime->nDay   = gmTime.tm_mday;
	
	//tm_hour: The number of hours past midnight, in the range 0 to 23.
	pSysTime->nHour   = gmTime.tm_hour;	
	//tm_min: The number of minutes after the hour, in the range 0 to 59.
	pSysTime->nMinute = gmTime.tm_min;
	//tm_sec: The number of seconds after the minute, normally in the range  0
	//           to 59, but can be up to 61 to allow for leap seconds.
	pSysTime->nSecond = gmTime.tm_sec;

	return pSysTime;
}

// -1 for error,
time_t TimetMerge(IN SYSTEM_TIME *pSysTime)
{
	struct tm gmTime;

	//tm_year: The number of years since 1900.
	gmTime.tm_year = pSysTime->nYear - 1900;	
	//tm_mon: The number of months since January, in the range 0 to 11.
	gmTime.tm_mon  = pSysTime->nMonth - 1;
	//tm_mday:  The day of the month, in the range 1 to 31.
	gmTime.tm_mday = pSysTime->nDay;
	
	//tm_hour: The number of hours past midnight, in the range 0 to 23.
	gmTime.tm_hour = pSysTime->nHour;	
	//tm_min: The number of minutes after the hour, in the range 0 to 59.
	gmTime.tm_min  = pSysTime->nMinute;
	//tm_sec: The number of seconds after the minute, normally in the range  0
	//           to 59, but can be up to 61 to allow for leap seconds.
	gmTime.tm_sec  = pSysTime->nSecond;


	return mktime(&gmTime);
}

int main( int argc, char *argv[] )
{
	int		i = 0;
	int 	nResult = -1;
	int		bFound = 0;

	int		nTestAPI = 0;
	struct STestAPI pTestAPI[MAX_TEST_API];

	// add test functions at here
	ADD_TEST_API( "mutex", "num_threads g_dwLockWaitTime slee_after_locked", 3, Test_Mutex );
//	ADD_TEST_API( "queue", "max_elements element_size <dynamic|static>", 3, Test_Queue );
	ADD_TEST_API( "queue", "num_products num_producers num_consumers queue_size", 4, Test_Queue );
	ADD_TEST_API( "timer", "id1=tv1 id2=tv2 id3=tv3 ... idn=tvn", 1, Test_Timer );
	ADD_TEST_API( "thread", "num_producer_thread num_consumer", 2, Test_Thread );
	ADD_TEST_API( "renew",   "", 0, Test_Renew );
	
	printf( "Test Public Lib API.\n" );

	// to test an API
	if( argc > 1 )
	{
		for( i = 0; (i < nTestAPI) && !bFound; i ++ )
		{
			if( strcmp( argv[1], pTestAPI[i].strNameAPI ) == 0 )
			{
				bFound = 1;
				break;
			}
			else
			{
				int n = atoi( argv[1] );
				if( n > 0 && n <= nTestAPI )
				{
					// input a number!
					i = n-1; //found. 
					bFound = 1;
					break;
				}
			}
		}

		if( bFound )	
		{
			if( argc-2 >= pTestAPI[i].nArgsRequired )	// at least the args.
			{
				RunThread_ManagerInit(RunThreadEventHandler);


				printf( "Testing API \"%s\"...\n", pTestAPI[i].strNameAPI);
				nResult = pTestAPI[i].pfnTest(argc-2, &argv[2]);

				RunThread_ManagerExit(10000, TRUE);
			}
			else
			{
				printf( "Wrong number of args passed in, %d arguments are required. \n"
					"The usage of this API is:\n"
					"%s %s\n",
					pTestAPI[i].nArgsRequired, 
					pTestAPI[i].strNameAPI, 
					pTestAPI[i].strArgsRequired );
			}
		}
	}

	// Not found the API that want to be tested, or argc is 1
	if( !bFound )
	{
		// show help
		printf( "Usage: testapi API_name [arg1 arg2 ...]\n"
			"You can choice one of the following APIs:\n" );

		for( i = 0; i < nTestAPI; i ++ )
		{
			printf( "   %d: %s %s\n",
				i+1,
				pTestAPI[i].strNameAPI, 
				pTestAPI[i].strArgsRequired );
		}

		printf( "\nYou can input the API index instead of the name of API.\n" );
	}
    
	return nResult;
}

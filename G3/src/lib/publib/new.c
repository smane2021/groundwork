/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : new.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-12 10:10
 *  VERSION  : V1.00
 *  PURPOSE  : To manage the allocated memory in DEBUG version
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
/* memory management utility */

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>

#include "new.h"

//added for memory opt
//#define _TOTAL_MEMORY_BYTES_INCLUDE_FREED  1 //to check the memory peak

#ifdef _DEBUG
int _MEM_MGR_SHOW_NEW_MSG = 0;

//manage the allocated memory
#define _MANAGE_ALLOCATED_MEMORY	1

#define _MULTI_THREAD_SAFE_NEW		1

#if defined(_MANAGE_ALLOCATED_MEMORY) && !defined(_MULTI_THREAD_SAFE_NEW)
#define _MULTI_THREAD_SAFE_NEW		1
#endif

//#define MAX_FILE_LEN    80
typedef struct tagMemoryHead _MEMORY_NODE;

struct tagMemoryHead
{
    unsigned	long ulBeginFlag;   /* must be the first */

#ifdef	_MANAGE_ALLOCATED_MEMORY
	_MEMORY_NODE	*next;
	_MEMORY_NODE	*prev;
#endif

    int			nId;
    int			nLine;
    char		*pszFile;
	char		*pszFunction;
	size_t		ulSize;	
    unsigned	long ulFlag;   /* must locate before pBuffer */
    char		pBuffer[0];    /* must be last item    */
};

#define _MEMORY_FLAG        0x55aaaa55ul
#define _N_DBG_HEAD_OF_BUF  sizeof(_MEMORY_NODE)

//add end flag check, maofuhua, 2004-11-11. add end flag size
#define _N_NODE_EXTRA_SIZE	(_N_DBG_HEAD_OF_BUF + sizeof(unsigned long))

#define DEBUG_GET_NODE(p)	((_MEMORY_NODE *)((char *)(p)-_N_DBG_HEAD_OF_BUF))
#define DEBUG_IS_NODE(p)	(((p)->ulFlag == _MEMORY_FLAG) \
							&& ((p)->ulBeginFlag == _MEMORY_FLAG))

#define NODE_SET_END_FLAG(p)	\
	((((p)->pBuffer[(p)->ulSize   ]) = ((_MEMORY_FLAG    )&0xff)), \
        (((p)->pBuffer[(p)->ulSize + 1]) = ((_MEMORY_FLAG>>8 )&0xff)), \
        (((p)->pBuffer[(p)->ulSize + 2]) = ((_MEMORY_FLAG>>16)&0xff)), \
        (((p)->pBuffer[(p)->ulSize + 3]) = ((_MEMORY_FLAG>>24)&0xff)))

#define NODE_IS_OVERRIDED(p) \
        ((((p)->pBuffer[(p)->ulSize   ]) != ((_MEMORY_FLAG    )&0xff)) || \
        (((p)->pBuffer[(p)->ulSize + 1]) != ((_MEMORY_FLAG>>8 )&0xff)) || \
        (((p)->pBuffer[(p)->ulSize + 2]) != ((_MEMORY_FLAG>>16)&0xff)) || \
        (((p)->pBuffer[(p)->ulSize + 3]) != ((_MEMORY_FLAG>>24)&0xff)))

static int		g_nMemoryBlockCount  = 0;	// total unfreed mem in block
static int		g_nAllocatedMemoryId = 0;	// ID, always increased
static size_t	g_ulMemoryByteCount  = 0;	// total unfreed mem in byte

//added for memory opt
static size_t	g_ulRenewBytes  = 0;	// total unfreed mem in byte
static size_t	g_ulDeletedBytes  = 0;	// total unfreed mem in byte

//int MEM_GET_INFO(int *pnAllocatedMemoryId, size_t *pulMemoryByteCount)
int MEM_GET_INFO(int *pnAllocatedMemoryId, size_t *pulMemoryByteCount, size_t *pRenewed, size_t *pDel)
{
	*pnAllocatedMemoryId = g_nAllocatedMemoryId;
	*pulMemoryByteCount  = g_ulMemoryByteCount;
	
	//added for memory opt
	*pRenewed = g_ulRenewBytes;
	*pDel = g_ulDeletedBytes;

	return g_nMemoryBlockCount;
}

#ifdef _MULTI_THREAD_SAFE_NEW
#include <pthread.h>

static pthread_mutex_t	hMemLock = PTHREAD_MUTEX_INITIALIZER;

#define LOCK_MEM_INIT()		  pthread_mutex_init(&hMemLock, NULL)
#define LOCK_MEM_SYNC()       pthread_mutex_lock(&hMemLock)
#define UNLOCK_MEM_SYNC()     pthread_mutex_unlock(&hMemLock)
#define DESTROY_MEM_SYNC()    pthread_mutex_destroy(&hMemLock)
#endif


static jmp_buf s_JumpBuf_SIGSEGV;
static void DEBUG_MEMORY_MANAGER_SIGSEGV(int n)
{
	n = n;
	longjmp(s_JumpBuf_SIGSEGV, 1);
}

// check the pointer field of mem node. maofuhua, 2004-11-14
static int CHECK_STRING_FIELD_VALID(char *pszStrToChk)
{
	char	szTemp[256];

	strncpy( szTemp, pszStrToChk, sizeof(szTemp));

	return szTemp[0];
}

static _MEMORY_NODE *DEBUG_MEMORY_MANAGER_CHECK_NODE(char *__caller__,
									_MEMORY_NODE *pNode, 
									char *__file__, int __line__, 
									char * __function__)
{
	__sighandler_t	pfnOldHandlder_SIGSEGV = NULL;
	_MEMORY_NODE	*pMemChecked;
	char			*pNodeName;

	if (pNode == NULL)
	{
		return NULL;
	}

	// avoid the node p is invalid to cause SEGMENT fault.
	/* set up to catch the SIGSEGV signal */
	pfnOldHandlder_SIGSEGV = signal(SIGSEGV, DEBUG_MEMORY_MANAGER_SIGSEGV);

	pNodeName = "this";
	pMemChecked = pNode;

	// set the on error return position.
	// setjmp returns non-0 if longjmp is called, error has happened.
	if(setjmp(s_JumpBuf_SIGSEGV) == 0)	// return 0 after init jmp buffer
	{
		// if cause segment fault, trigger SIGSEGV
		if (DEBUG_IS_NODE(pNode))	// check 
		{
			// check the node is written exceed end bound.
			if (NODE_IS_OVERRIDED(pNode))
			{
				printf("[MemMgr] -- Panic! %s: the memory %p(%s:%d:%s) is "
					"end overrided.\n",
					__caller__,
					((char *)pNode) + _N_DBG_HEAD_OF_BUF,
					pNode->pszFile, pNode->nLine, pNode->pszFunction);
			}

			// check the pszFile field pointer
			pNodeName = "this->pszFile";
			CHECK_STRING_FIELD_VALID(pNode->pszFile);

			// check the pszFunction field pointer
			pNodeName = "this->pszFunction";
			CHECK_STRING_FIELD_VALID(pNode->pszFunction);

			// check the first item next. This is a loop link.
#ifdef _MANAGE_ALLOCATED_MEMORY
			pNodeName = "next";
			pMemChecked = pNode->next;
			(void)DEBUG_IS_NODE(pNode->next);

			pNodeName = "prev";
			pMemChecked = pNode->prev;
			(void)DEBUG_IS_NODE(pNode->prev);
#endif
		}
		else 
		{
			printf("[MemMgr] -- Panic! %s: the memory ID:%d:%p(%s:%d:%s) is crashed.\n",
				__caller__,
				g_nAllocatedMemoryId,
				((char *)pNode) + _N_DBG_HEAD_OF_BUF,
				__file__, __line__, __function__);

			pNode = NULL;
		}
	}
	else
	{
		printf("[MemMgr] -- Panic! %s: the %s memory(%p) of ID:%d:%p(%s:%d:%s) causes a "
			"segment fault.\n",
			__caller__,
			pNodeName,
			((char *)pMemChecked) + _N_DBG_HEAD_OF_BUF,
			g_nAllocatedMemoryId,
			((char *)pNode) + _N_DBG_HEAD_OF_BUF,
			__file__, __line__, __function__);

		if (pNode == pMemChecked)	// this node is invalid.
		{
			pNode = NULL;
		}
	}

	// restore old handler.
	signal( SIGSEGV, pfnOldHandlder_SIGSEGV );

	return pNode;
}

#define DEBUG_MEMORY_MANAGER_GET_NODE(__caller__, p, \
	__file__, __line__, __function__)	\
	DEBUG_MEMORY_MANAGER_CHECK_NODE((__caller__), DEBUG_GET_NODE(p),\
		(__file__), (__line__),	(__function__))

/////////////// log malloced memory
#ifdef _MANAGE_ALLOCATED_MEMORY
static int g_nMemManagerInited = 0;

// the mem head. must be 2. the item[1] will be overwritten.
static _MEMORY_NODE	g_pMemNodeHead[2];
#define g_MemNodeHead	(g_pMemNodeHead[0])

static void DEBUG_DESTROY_MEMORY_MANAGER(void);

/*==========================================================================*
 * FUNCTION : DEBUG_INIT_MEMORY_MANAGER
 * PURPOSE  : to init the mem head to a bi-link.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-12 10:10
 *==========================================================================*/
static void DEBUG_INIT_MEMORY_MANAGER(void)
{
	_MEMORY_NODE	*pHead = &g_MemNodeHead;

#ifdef _MULTI_THREAD_SAFE_NEW
	LOCK_MEM_INIT();
#endif

	memset(&g_pMemNodeHead, 0, sizeof(g_pMemNodeHead)); 

	pHead->pszFunction = __FUNCTION__;
	pHead->ulFlag  = pHead->ulBeginFlag = _MEMORY_FLAG;
	pHead->pszFile = __FILE__;
	pHead->nLine   = __LINE__;
	pHead->nId     = 0;
	pHead->ulSize  = 0;

	// this call will overwrite the g_pMemNodeHead[1]
	NODE_SET_END_FLAG(pHead);

	// construct a loop dual-link.
	pHead->next = pHead->prev = pHead;

	atexit(DEBUG_DESTROY_MEMORY_MANAGER);
}


static void DEBUG_DESTROY_MEMORY_MANAGER(void)
{
	__sighandler_t	pfnOldHandlder_SIGSEGV = NULL;
	int				nCurLine = __LINE__;
	char			*pCurRunning = "Nothing";
#define TRACE_RUNNING_INFO(info)		(nCurLine = __LINE__, pCurRunning = (info))

	// show leaked memory
	if (g_nMemoryBlockCount == 0)
	{
		return;
	}

#ifdef _MULTI_THREAD_SAFE_NEW
	LOCK_MEM_SYNC();
#endif
	// avoid the node p is invalid to cause SEGMENT fault.
	/* set up to catch the SIGSEGV signal */
	pfnOldHandlder_SIGSEGV = signal(SIGSEGV, DEBUG_MEMORY_MANAGER_SIGSEGV);

	if(setjmp(s_JumpBuf_SIGSEGV) == 0)	// return 0 after init jmp buffer
	{
		int				i;
		_MEMORY_NODE	*pNode;
		_MEMORY_NODE	*p, *pEndNode;
		int				nCheckByNext = 1; 

		printf( "\n\n[MemMgr] -- PANIC! There are %d memory leaks (%u bytes) "
			"after exit.\n",
			g_nMemoryBlockCount, g_ulMemoryByteCount);

		pNode    = g_MemNodeHead.next;
		pEndNode = &g_MemNodeHead;

		for (i = 1; pNode != pEndNode; )
		{
			// to check the node.
			p = DEBUG_MEMORY_MANAGER_CHECK_NODE("MANAGER", pNode, __FILE__,
				__LINE__, __FUNCTION__);

			if ( p != NULL)
			{
				TRACE_RUNNING_INFO("displaying leaked node");

				printf("[MemMgr] -- Leak %-3d: Mem=%p ID=%-6d L=%-6d "
					"NEW at %s:%d:%s.\n",
					i, 
					p->pBuffer,
					p->nId, (int)p->ulSize, p->pszFile, p->nLine,
					p->pszFunction);

				TRACE_RUNNING_INFO("going to next node");

				// go to next/prev node.
				pNode = (nCheckByNext) ? pNode->next : pNode->prev;

				TRACE_RUNNING_INFO("freeing leaked node");

				// free leaked memory
				new_free((void*)p);

				i++;
			}
			else
			{
				TRACE_RUNNING_INFO("displaying creashed node");

				printf("[MemMgr] -- Leak %4d: Mem=%p is crashed.\n",
					i, ((char *)pNode)+_N_DBG_HEAD_OF_BUF);

				if (nCheckByNext)
				{
					TRACE_RUNNING_INFO("setting reverse mode");

					printf("[MemMgr] -- Memory leak check to try reverse "
						"direction.\n");

					nCheckByNext = 0;
					pEndNode     = pNode;
					pNode        = g_MemNodeHead.prev;
				}
				else
				{
					printf("[MemMgr] -- Memory leak check fails in reverse "
						"direction, exit.\n");
					break;
				}
			}
		}

		printf( "[MemMgr] -- End of showing %d memory leaks, "
			"there are %d crashed memory blocks.\n",
			g_nMemoryBlockCount,
			g_nMemoryBlockCount-(i-1));
	}
	else
	{
		// causes a segment fault.
		printf("[MemMgr] -- PANIC! Memory manager %s:%d causes a "
			"segment fault while executing %s.\n",
			__FILE__, nCurLine, pCurRunning);
			// restore old handler.
	}

	// restore old handler of the signal
	signal( SIGSEGV, pfnOldHandlder_SIGSEGV );		

#ifdef _MULTI_THREAD_SAFE_NEW
	UNLOCK_MEM_SYNC();

	DESTROY_MEM_SYNC();
#endif

#undef TRACE_RUNNING_INFO
}

static void DEBUG_ADD_NODE(_MEMORY_NODE *p, char *__file__, int __line__,
						   char *__function__)
{
	if (DEBUG_MEMORY_MANAGER_CHECK_NODE("NEW", g_MemNodeHead.next, 
		__file__, __line__, __function__) != NULL)
	{
		// insert at head
		p->next			   = g_MemNodeHead.next;
		g_MemNodeHead.next = p;

		p->prev            = &g_MemNodeHead;
		p->next->prev      = p;
	}
	else
	{
		printf( "[MemMgr] -- Memory allocation at %s:%d:%s OK, but fails on "
			"inserting it to memory list for the previous allocated memory error.\n",
			__file__, __line__, __function__);
	}
}

static void DEBUG_DEL_NODE(_MEMORY_NODE *p)
{
	p->prev->next = p->next;
	p->next->prev = p->prev;
}

#endif
///////////////

void *DEBUG_NEW( size_t dwSize, char *__file__, int __line__,
				char * __function__)
{
    _MEMORY_NODE *p;

#ifdef _MANAGE_ALLOCATED_MEMORY
	if (!g_nMemManagerInited)
	{
		g_nMemManagerInited = 1;
		DEBUG_INIT_MEMORY_MANAGER();
	}
#endif
   
    p = (_MEMORY_NODE *)new_malloc(sizeof(char)*(dwSize + _N_NODE_EXTRA_SIZE));
    if(p != NULL )
    {
		// OK, to init mem node.
#ifdef _MULTI_THREAD_SAFE_NEW
		LOCK_MEM_SYNC();
#endif

		p->nId    = ++g_nAllocatedMemoryId;
		p->ulFlag = _MEMORY_FLAG;
		p->ulBeginFlag = _MEMORY_FLAG;
		p->ulSize = dwSize;
		p->nLine  = __line__;
		p->pszFile= __file__;
		p->pszFunction = __function__;

		NODE_SET_END_FLAG(p);	// add end overrided check flag.

		g_nMemoryBlockCount += 1;
		g_ulMemoryByteCount += dwSize;

		if (_MEM_MGR_SHOW_NEW_MSG)
		{
			printf( "[MemMgr] -- NEW %d(%p),ID=%d: File %s:%d:%s L=%d.\n",
				g_nMemoryBlockCount, 
				p->pBuffer,
				p->nId, __file__, __line__, __function__, (int)dwSize );
			fflush(stdout);
		}

#ifdef _MANAGE_ALLOCATED_MEMORY
		DEBUG_ADD_NODE(p, __file__, __line__, __function__);
#endif

#ifdef _MULTI_THREAD_SAFE_NEW
		UNLOCK_MEM_SYNC();
#endif
	}

	else
	{
		printf( "[MemMgr] -- NEW failed at File %s:%d:%s L=%d.\n",
			__file__, __line__, __function__, (int)dwSize );
		fflush(stdout);
	}

    return (p != NULL) ? (void *)(p->pBuffer) : NULL;
}


void *DEBUG_RENEW(void *pOldPtr, size_t dwNewSize, 
				  char *__file__, int __line__,
				  char * __function__)
{
	//added for memory opt
#ifdef RENEW_OPT_DEBUG
	static int iCountTotal = 0;
	static int iCountRealloc = 0;
	static int iCountMalloc = 0;
#endif

    _MEMORY_NODE *p, *pOldNode;

	if (dwNewSize == 0)
	{
		// same as free(), call DEBUG_DELETE()
		DEBUG_DELETE(pOldPtr, __file__, __line__, __function__);
		return NULL;	// freed
	}

	//added for memory opt
#ifdef RENEW_OPT_DEBUG
	iCountTotal ++;
#endif

	if (pOldPtr == NULL)
	{
		//added for memory opt
#ifdef RENEW_OPT_DEBUG
		iCountMalloc ++;
		printf("Inside renew: iCountTotal=%d, malloc:%d, Realloc=%d\n ", iCountTotal, iCountMalloc, iCountRealloc);
#endif

		// same as malloc(), call DEBUG_NEW()
		return DEBUG_NEW(dwNewSize, __file__, __line__, __function__);
	}

#ifdef _MULTI_THREAD_SAFE_NEW
	LOCK_MEM_SYNC();
#endif

	// it's real realloc.get the real malloc addr
	pOldNode = DEBUG_MEMORY_MANAGER_GET_NODE("RENEW", pOldPtr, __file__, __line__,
		__function__);
	if (pOldNode != NULL)
	{
#ifdef _MANAGE_ALLOCATED_MEMORY
		// remove the old node at first, it may be changed after realloc
		DEBUG_DEL_NODE(pOldNode);
#endif

		p = (_MEMORY_NODE *)new_realloc(pOldNode, 
			sizeof(char)*(dwNewSize + _N_NODE_EXTRA_SIZE) );

		//added for memory opt
#ifdef RENEW_OPT_DEBUG
		iCountRealloc++;
#endif

		if (p != NULL)
		{
			// the head info is copied to the new returned addr,
			// that need not set any information
			// the num of mem block is not changed. 
			// But, we need record the file and line info.

			//added for memory opt
#ifndef _TOTAL_MEMORY_BYTES_INCLUDE_FREED
			g_ulMemoryByteCount -= p->ulSize;
#endif
			
			g_ulMemoryByteCount += dwNewSize;

			//added for memory opt
			g_ulRenewBytes += p->ulSize;

			p->nId			= ++g_nAllocatedMemoryId;
			p->nLine		= __line__;
			p->pszFile		= __file__;
			p->pszFunction  = __function__;
			p->ulSize		= dwNewSize;
			
			NODE_SET_END_FLAG(p);	// add end overrided check flag.

			if (_MEM_MGR_SHOW_NEW_MSG)
			{
				printf( "[MemMgr] -- RENEW %d(%p),ID=%d:" \
					" NEW: %s:%d:%s, L=%d, RENEW: %s:%d:%s L=%d.\n",
					g_nMemoryBlockCount, p->pBuffer, p->nId,
					p->pszFile, p->nLine, p->pszFunction,
					p->ulSize,
					__file__, __line__, __function__,
					dwNewSize);
				fflush(stdout);
			}

#ifdef _MANAGE_ALLOCATED_MEMORY
			DEBUG_ADD_NODE(p, __file__, __line__, __function__);
#endif
		}
		else
		{
			// if fails, we shall insert the old node to list again.
			// the old ptr is not changed when realloc fails
#ifdef _MANAGE_ALLOCATED_MEMORY
			DEBUG_ADD_NODE(pOldNode, pOldNode->pszFile, pOldNode->nLine,
				pOldNode->pszFunction);
#endif

			printf( "[MemMgr] -- RENEW %p failed at File %s:%d:%s L=%d.\n",
				pOldNode, __file__, __line__, __function__, (int)dwNewSize);
			fflush(stdout);
		}
    }
	else
	{
		p = NULL;

		printf( "[MemMgr] -- ERROR: RENEW unknown(NEW?) memory %p "
			"at %s:%d:%s.\n", 
			pOldPtr, __file__, __line__, __function__);
		fflush(stdout);
	}

#ifdef _MULTI_THREAD_SAFE_NEW
	UNLOCK_MEM_SYNC();
#endif

    //added for memory opt
#ifdef RENEW_OPT_DEBUG
	printf("Inside renew: iCountTotal=%d, malloc:%d, Realloc=%d\n ", iCountTotal, iCountMalloc, iCountRealloc);
#endif

    return (p != NULL) ? (void *)(p->pBuffer) : NULL;
}


void DEBUG_DELETE( void *p1, char *__file__, int __line__,
				  char * __function__)
{
    if( p1 != NULL )
    {
        _MEMORY_NODE *p;
            
#ifdef _MULTI_THREAD_SAFE_NEW
		LOCK_MEM_SYNC();
#endif
		p = DEBUG_MEMORY_MANAGER_GET_NODE("DELETE", p1, __file__, __line__,
			__function__);
		if (p != NULL)
		{
			if (_MEM_MGR_SHOW_NEW_MSG)
			{
				printf("[MemMgr] -- DEL %d(%p),ID=%d:" \
					" NEW: %s:%d:%s, L=%d DEL: %s:%d:%s.\n",
					g_nMemoryBlockCount,
					p->pBuffer,	p->nId, 
					p->pszFile, p->nLine, p->pszFunction,
					(int)p->ulSize,
					__file__, __line__, __function__);
				fflush(stdout);
			}

			p->ulFlag      = 0;
			p->ulBeginFlag = 0;

			g_nMemoryBlockCount -= 1;
			g_ulMemoryByteCount -= p->ulSize;

#ifdef _MANAGE_ALLOCATED_MEMORY
			DEBUG_DEL_NODE(p);
#endif
			new_free( (void*)p );
        }
        else
        {
			printf("[MemMgr] -- WARNING: Unknown(NEW?) memory %p will be "
				"freed at %s:%d:%s.\n", 
                p1, __file__, __line__, __function__);
            fflush(stdout);

            new_free( p1 );
        }
    
#ifdef _MULTI_THREAD_SAFE_NEW
		UNLOCK_MEM_SYNC();
#endif	
	}
}

#endif	// ifdef _DEBUG

//by 23632, for memory opt
//simple pool management, not thread safe
SIMPLE_POOL_MANAGER g_PoolManager;
char * Pool_GetBlock(size_t size)
{
	if (size > POOL_BLOCK_SIZE)
	{
#ifdef MEM_POOL_DEBUG
		printf("Panic! The Memery Block size of the pool is not big enogh(required: %d)!\n", size);
#endif
		return NULL;
	}

	SIMPLE_POOL_MANAGER *pManager = &g_PoolManager;
	SIMPLE_POOL_BLOCK *pBlock = (SIMPLE_POOL_BLOCK *)pManager->pMem;

	int i;
	for (i = 0; i < pManager->iBlocks; i++, pBlock++)
	{
		if (!pBlock->bInUse)
		{

#ifdef MEM_POOL_DEBUG
			printf("Get free block: id = %d\n", pBlock->iID);
#endif
			
			pBlock->bInUse = TRUE;
			return pBlock->pBuff;
		}
	}

#ifdef MEM_POOL_DEBUG
		printf("PANIC! The Memery pool is overflow!\n");
#endif
	return NULL;
}

void Pool_ReleaseBlock(void *p)
{
	SIMPLE_POOL_BLOCK *pBlock;
	pBlock = (SIMPLE_POOL_BLOCK *)(p-sizeof(BOOL)-sizeof(int));

	if (pBlock)
	{
		pBlock->bInUse = FALSE;

#ifdef MEM_POOL_DEBUG
		printf("Release free block: id = %d\n", pBlock->iID);
		if ( pBlock->iID >= POOL_SIZE || pBlock->iID <0 )
		{
			printf("===============\n\n\n\n     PANIC!Wrong mem pool block id used!      \n\n\n\n===========================\n");
		}
#endif
	}
	return;
}

BOOL Pool_Manager_Initial()
{
#ifndef MEM_POOL_IMP
	return TRUE;
#endif

	g_PoolManager.iBlocks = POOL_SIZE;
	g_PoolManager.pMem = (char *)NEW(SIMPLE_POOL_BLOCK, POOL_SIZE);

#ifdef MEM_POOL_DEBUG
	int i;
	SIMPLE_POOL_BLOCK *pBlock;
#endif

	if (g_PoolManager.pMem != NULL)
	{
		memset(g_PoolManager.pMem, 0, POOL_SIZE*sizeof(SIMPLE_POOL_BLOCK));

#ifdef MEM_POOL_DEBUG
		pBlock = g_PoolManager.pMem;
		for (i = 0; i < POOL_SIZE; i++, pBlock++)
		{
			pBlock->iID = i;
		}
#endif

		return TRUE;
	}

	return FALSE;
}

void Pool_Manager_Release()
{
#ifndef MEM_POOL_IMP
	return;
#endif

#ifdef MEM_POOL_DEBUG
		SIMPLE_POOL_BLOCK *pBlock;
		int i;
		pBlock = g_PoolManager.pMem;
		for (i = 0; i < POOL_SIZE; i++, pBlock++)
		{
			if (pBlock->bInUse == TRUE)
			{
				printf("===============\n\n\n\n     PANIC! Memory leak of mem pool!      \n\n\n\n===========================\n");
			}
		}
#endif
	SAFELY_DELETE(g_PoolManager.pMem);
	g_PoolManager.iBlocks = 0;

	return;
}




//Frank Wu
#ifdef DEBUG_MEM_FOR_FIND_LEAK

#include "err_code.h"
#include "stdsys.h"  
#include "public.h"  



typedef struct tagDebugMemInfo {
	BOOL			bEnable;//enable or disable the function of debugging memory leak
	HANDLE			hMutexOperatorMem;//thread mutex lock

	time_t			tmSysStartTime;//the app start time
	time_t			tmMemDebugStartTime;//the start time of debugging memory leak
	time_t			tmMemDebugPtrCountReachMaxTime;//the time which the used memory reach the max value

	unsigned int	uiPtrCountUsedMax;//the max value of used memory
	unsigned int	uiPtrCount;//the current value of used memory
	unsigned int	auiPtrTable[DEBUG_MEM_MAX_PTR_COUNT][4];//0, New ptr; 1, New Size; 2, thread ID; 3, time of New space
	
	unsigned int	uiNewSize;//the total size of New memory space
	unsigned int	uiFreeSize;//the total size of Free memory space

} DEBUG_MEM_INFO;

static DEBUG_MEM_INFO s_stDebugMemInfo;


void DEBUG_print_mem_status(void)
{
#define DEBUG_UNIT_M		(1024*1024)
#define DEBUG_UNIT_K		(1024)

	static time_t sl_tmLastPrint;
	static int sl_iLastUsedSize = 0;
	static BOOL sl_bNeedPrintLeak = TRUE;
	time_t tmCur;
	int iUsedSize = 0;
	int iUsedChangeSize = 0;
	int i;

	if( s_stDebugMemInfo.bEnable )
	{
		//just display debugging info every one minute
		tmCur = time(NULL);
		if(( tmCur - sl_tmLastPrint <= 60)
			&& ( tmCur - sl_tmLastPrint >= -60))
		{
			return;
		}
		sl_tmLastPrint = tmCur;

		RUN_THREAD_HEARTBEAT();

		//make sure the thread mutex lock is OK
		if(NULL == s_stDebugMemInfo.hMutexOperatorMem)
		{
			s_stDebugMemInfo.hMutexOperatorMem = Mutex_Create_With_Malloc(TRUE);
		}

		if(NULL != s_stDebugMemInfo.hMutexOperatorMem)
		{
			if (Mutex_Lock(s_stDebugMemInfo.hMutexOperatorMem,1000) == ERR_MUTEX_OK)//lock
			{
				//compute the value of iUsedSize
				iUsedSize = 0;
				for(i = 0; i < s_stDebugMemInfo.uiPtrCount; i++)
				{
					iUsedSize += s_stDebugMemInfo.auiPtrTable[i][1];
				}
				//compute the value of iUsedChangeSize
				iUsedChangeSize = iUsedSize - sl_iLastUsedSize;

				printf("s_uiNewSize=%u, s_uiFreeSize=%u, iUsedSize=%d, s_uiPtrCount=%u, s_uiPtrCountUsedMax=%u, DEBUG_MEM_MAX_PTR_COUNT=%u, s_hMutexOperatorMem=%p\n",
					s_stDebugMemInfo.uiNewSize,
					s_stDebugMemInfo.uiFreeSize,
					iUsedSize,
					s_stDebugMemInfo.uiPtrCount,
					s_stDebugMemInfo.uiPtrCountUsedMax,
					DEBUG_MEM_MAX_PTR_COUNT,
					s_stDebugMemInfo.hMutexOperatorMem);

				printf("s_uiNewSize=(%uM, %uK, %uB)\n",
					s_stDebugMemInfo.uiNewSize/DEBUG_UNIT_M,
					(s_stDebugMemInfo.uiNewSize%DEBUG_UNIT_M)/DEBUG_UNIT_K,
					s_stDebugMemInfo.uiNewSize%DEBUG_UNIT_K);

				printf("s_uiFreeSize=(%uM, %uK, %uB)\n",
					s_stDebugMemInfo.uiFreeSize/DEBUG_UNIT_M,
					(s_stDebugMemInfo.uiFreeSize%DEBUG_UNIT_M)/DEBUG_UNIT_K,
					s_stDebugMemInfo.uiFreeSize%DEBUG_UNIT_K);

				printf("iUsedSize=(%dM, %dK, %dB)\n",
					iUsedSize/DEBUG_UNIT_M,
					(iUsedSize%DEBUG_UNIT_M)/DEBUG_UNIT_K,
					iUsedSize%DEBUG_UNIT_K);

				printf("iUsedChangeSize=(%dM, %dK, %dB)\n",
					iUsedChangeSize/DEBUG_UNIT_M,
					(iUsedChangeSize%DEBUG_UNIT_M)/DEBUG_UNIT_K,
					iUsedChangeSize%DEBUG_UNIT_K);

				printf("tmSysStartTime=%s",
					(0 == s_stDebugMemInfo.tmSysStartTime)? "None": ctime(&s_stDebugMemInfo.tmSysStartTime));
				printf("tmMemDebugStartTime=%s",
					(0 == s_stDebugMemInfo.tmMemDebugStartTime)? "None": ctime(&s_stDebugMemInfo.tmMemDebugStartTime));
				printf("tmMemDebugPtrCountReachMaxTime=%s\n",
					(0 == s_stDebugMemInfo.tmMemDebugPtrCountReachMaxTime)? "None": ctime(&s_stDebugMemInfo.tmMemDebugPtrCountReachMaxTime));

				//check whether the memory space is leak
				if(s_stDebugMemInfo.uiPtrCountUsedMax >= DEBUG_MEM_MAX_PTR_COUNT)
				{
					printf("uiPtrCountUsedMax==DEBUG_MEM_MAX_PTR_COUNT=%u, maybe has ***Memory leak***\n",
						s_stDebugMemInfo.uiPtrCountUsedMax);
				}

				//print the used memory space info
				if( sl_bNeedPrintLeak )
				{
					sl_bNeedPrintLeak = FALSE;

					for(i = 0; i < s_stDebugMemInfo.uiPtrCount; i++)
					{
						printf("%04d,Thread ID=%p(%u), Ptr=%p(%u), Size=%u, Time=%s",
							i,
							s_stDebugMemInfo.auiPtrTable[i][2],
							s_stDebugMemInfo.auiPtrTable[i][2],
							s_stDebugMemInfo.auiPtrTable[i][0],
							s_stDebugMemInfo.auiPtrTable[i][0],
							s_stDebugMemInfo.auiPtrTable[i][1],
							ctime(&s_stDebugMemInfo.auiPtrTable[i][3]));
					}
					printf("\n");
				}
				else
				{
					//if "/var/ShowLeak" is exist, print the used memory space info
					struct stat stFileStat;
					if(0 == stat(DEBUG_MEM_FILE_SHOW_LEAK, &stFileStat))//file exist
					{
						sl_bNeedPrintLeak = TRUE;
						
						unlink(DEBUG_MEM_FILE_SHOW_LEAK);
					}
				}
				
				//unlock
				Mutex_Unlock(s_stDebugMemInfo.hMutexOperatorMem);
			}
		}

		//store the size of last used memory space for computing the change of size
		sl_iLastUsedSize = iUsedSize;
	}
}




void *DEBUG_new_malloc(size_t dwSize)
{
	void		*pRet;
	time_t		tmCur;
	
	//log the system start time
	if(0 == s_stDebugMemInfo.tmSysStartTime)
	{
		s_stDebugMemInfo.tmSysStartTime = time(NULL);
	}
	else if( !s_stDebugMemInfo.bEnable )
	{
		//the function of debugging memory leak won't enable until DEBUG_MEM_START_DELAY_S later
		if(DEBUG_MEM_START_DELAY_S >= 0)
		{
			tmCur = time(NULL);
			if( (tmCur - s_stDebugMemInfo.tmSysStartTime > DEBUG_MEM_START_DELAY_S) )
			{
				s_stDebugMemInfo.bEnable = TRUE;//enable the function of debugging memory leak
				s_stDebugMemInfo.tmMemDebugStartTime = tmCur;
			}
		}
	}
	
	//correct some invalid malloc operator
	if(0 == dwSize)
	{
		dwSize = 1;
	}
	
	//use malloc to New memory space
	pRet = new_malloc(dwSize);
	if(s_stDebugMemInfo.bEnable && (NULL != pRet))
	{
		RUN_THREAD_HEARTBEAT();

		//make sure the thread mutex lock is OK
		if(NULL == s_stDebugMemInfo.hMutexOperatorMem)
		{
			s_stDebugMemInfo.hMutexOperatorMem = Mutex_Create_With_Malloc(TRUE);
		}
	
		if(NULL != s_stDebugMemInfo.hMutexOperatorMem)
		{
			if (Mutex_Lock(s_stDebugMemInfo.hMutexOperatorMem,1000) == ERR_MUTEX_OK)//lock
			{
				s_stDebugMemInfo.uiNewSize += dwSize;

				//log New operator to a table, include pointer address, size and thread ID of owner
				if(s_stDebugMemInfo.uiPtrCount < DEBUG_MEM_MAX_PTR_COUNT)
				{
					s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount][0] = (unsigned int)pRet;
					s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount][1] = dwSize;
					s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount][2] = RunThread_GetId(NULL);
					s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount][3] = time(NULL);

					s_stDebugMemInfo.uiPtrCount++;
					
					//compute the max value of used uiPtrCount
					if(s_stDebugMemInfo.uiPtrCountUsedMax < s_stDebugMemInfo.uiPtrCount)
					{
						s_stDebugMemInfo.uiPtrCountUsedMax = s_stDebugMemInfo.uiPtrCount;
					}
				}
				else
				{
					//the table is too small or the used memory space is too large
					//log the time
					if(0 == s_stDebugMemInfo.tmMemDebugPtrCountReachMaxTime)
					{
						s_stDebugMemInfo.tmMemDebugPtrCountReachMaxTime = time(NULL);
					}
				}
				
				//unlock
				Mutex_Unlock(s_stDebugMemInfo.hMutexOperatorMem);
			}
			else
			{
				printf("DEBUG_new_malloc Mutex_Lock ERROR\n");
			}
		}
		else
		{
			printf("DEBUG_new_malloc Mutex_Create ERROR\n");
		}

	}

	//print memory leak info
	DEBUG_print_mem_status();

	return pRet;
}

void *DEBUG_new_realloc(void *pOldPtr, size_t dwSize)
{
	void *pRet;
	unsigned int i;
	time_t		tmCur;

	//log the system start time
	if(0 == s_stDebugMemInfo.tmSysStartTime)
	{
		s_stDebugMemInfo.tmSysStartTime = time(NULL);
	}
	else if( !s_stDebugMemInfo.bEnable )
	{
		//the function of debugging memory leak won't enable until DEBUG_MEM_START_DELAY_S later
		if(DEBUG_MEM_START_DELAY_S >= 0)
		{
			tmCur = time(NULL);
			if( (tmCur - s_stDebugMemInfo.tmSysStartTime > DEBUG_MEM_START_DELAY_S) )
			{
				s_stDebugMemInfo.bEnable = TRUE;//enable the function of debugging memory leak
				s_stDebugMemInfo.tmMemDebugStartTime = tmCur;
			}
		}
	}

	//correct some invalid malloc operator
	if(0 == dwSize)
	{
		dwSize = 1;
	}

	//use malloc to New memory space
	pRet = realloc(pOldPtr, dwSize);
	if(s_stDebugMemInfo.bEnable && (NULL != pRet))
	{
		RUN_THREAD_HEARTBEAT();
	
		//make sure the thread mutex lock is OK
		if(NULL == s_stDebugMemInfo.hMutexOperatorMem)
		{
			s_stDebugMemInfo.hMutexOperatorMem = Mutex_Create_With_Malloc(TRUE);
		}
	
		if(NULL != s_stDebugMemInfo.hMutexOperatorMem)
		{
			if (Mutex_Lock(s_stDebugMemInfo.hMutexOperatorMem,1000) == ERR_MUTEX_OK)//lock
			{
				s_stDebugMemInfo.uiNewSize += dwSize;

				//log New operator to a table, include pointer address, size and thread ID of owner
				if(s_stDebugMemInfo.uiPtrCount < DEBUG_MEM_MAX_PTR_COUNT)
				{
					s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount][0] = (unsigned int)pRet;
					s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount][1] = dwSize;
					s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount][2] = RunThread_GetId(NULL);
					s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount][3] = time(NULL);

					s_stDebugMemInfo.uiPtrCount++;

					//compute the max value of used uiPtrCount
					if(s_stDebugMemInfo.uiPtrCountUsedMax < s_stDebugMemInfo.uiPtrCount)
					{
						s_stDebugMemInfo.uiPtrCountUsedMax = s_stDebugMemInfo.uiPtrCount;
					}
				}
				else
				{
					//the table is too small or the used memory space is too large
					//log the time
					if(0 == s_stDebugMemInfo.tmMemDebugPtrCountReachMaxTime)
					{
						s_stDebugMemInfo.tmMemDebugPtrCountReachMaxTime = time(NULL);
					}
				}

				//free the old memory space
				if(NULL != pOldPtr)
				{
					//find the size info of old memory space
					for(i = 0; i < s_stDebugMemInfo.uiPtrCount; i++)
					{
						if(pOldPtr == s_stDebugMemInfo.auiPtrTable[i][0])
						{
							break;
						}
					}

					//delete the old memory space info from table
					if(i < s_stDebugMemInfo.uiPtrCount)
					{
						s_stDebugMemInfo.uiFreeSize += s_stDebugMemInfo.auiPtrTable[i][1];

						s_stDebugMemInfo.auiPtrTable[i][0] = s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount - 1][0];
						s_stDebugMemInfo.auiPtrTable[i][1] = s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount - 1][1];
						s_stDebugMemInfo.auiPtrTable[i][2] = s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount - 1][2];
						s_stDebugMemInfo.auiPtrTable[i][3] = s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount - 1][3];
						
						s_stDebugMemInfo.uiPtrCount--;
					}
				}
				
				//unlock
				Mutex_Unlock(s_stDebugMemInfo.hMutexOperatorMem);
			}
			else
			{
				printf("DEBUG_new_realloc Mutex_Lock ERROR\n");
			}
		}
		else
		{
			printf("DEBUG_new_realloc Mutex_Create ERROR\n");
		}
	}

	//print memory leak info
	DEBUG_print_mem_status();

	return pRet;
}

void DEBUG_new_free(void *pOldPtr)
{
	unsigned int i;
	time_t		tmCur;

	//log the system start time
	if(0 == s_stDebugMemInfo.tmSysStartTime)
	{
		s_stDebugMemInfo.tmSysStartTime = time(NULL);
	}
	else if( !s_stDebugMemInfo.bEnable )
	{
		//the function of debugging memory leak won't enable until DEBUG_MEM_START_DELAY_S later
		if(DEBUG_MEM_START_DELAY_S >= 0)
		{
			tmCur = time(NULL);
			if( (tmCur - s_stDebugMemInfo.tmSysStartTime > DEBUG_MEM_START_DELAY_S) )
			{
				s_stDebugMemInfo.bEnable = TRUE;//enable the function of debugging memory leak
				s_stDebugMemInfo.tmMemDebugStartTime = tmCur;
			}
		}
	}
	
	if(s_stDebugMemInfo.bEnable && (NULL != pOldPtr))
	{
		RUN_THREAD_HEARTBEAT();

		//make sure the thread mutex lock is OK
		if(NULL == s_stDebugMemInfo.hMutexOperatorMem)
		{
			s_stDebugMemInfo.hMutexOperatorMem = Mutex_Create_With_Malloc(TRUE);
		}

		if(NULL != s_stDebugMemInfo.hMutexOperatorMem)
		{
			if (Mutex_Lock(s_stDebugMemInfo.hMutexOperatorMem,1000) == ERR_MUTEX_OK)//lock
			{
				//free the old memory space
				
				//find the size info of old memory space
				for(i = 0; i < s_stDebugMemInfo.uiPtrCount; i++)
				{
					if(pOldPtr == s_stDebugMemInfo.auiPtrTable[i][0])
					{
						break;
					}
				}

				//delete the old memory space info from table
				if(i < s_stDebugMemInfo.uiPtrCount)
				{
					s_stDebugMemInfo.uiFreeSize += s_stDebugMemInfo.auiPtrTable[i][1];

					s_stDebugMemInfo.auiPtrTable[i][0] = s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount - 1][0];
					s_stDebugMemInfo.auiPtrTable[i][1] = s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount - 1][1];
					s_stDebugMemInfo.auiPtrTable[i][2] = s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount - 1][2];
					s_stDebugMemInfo.auiPtrTable[i][3] = s_stDebugMemInfo.auiPtrTable[s_stDebugMemInfo.uiPtrCount - 1][3];

					s_stDebugMemInfo.uiPtrCount--;
				}

				Mutex_Unlock(s_stDebugMemInfo.hMutexOperatorMem);//unlock
			}
			else
			{
				printf("DEBUG_new_free Mutex_Lock ERROR\n");
			}
		}
		else
		{
			printf("DEBUG_new_free Mutex_Create ERROR\n");
		}
	}
	
	//print memory leak info
	DEBUG_print_mem_status();

	free(pOldPtr);
}

#endif




/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : pubfunc.c
 *  CREATOR  : ACU Team                 DATE: 2004-09-14 21:08
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

/* !!!!!!!!!!!!!!!!!=== NOTICE ===!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*
 ! There is a pubfunc.c in EMNET software, if you need something, you can  !
 ! copy it from that punfunc.c of EMNET.								   !
 ! Frank Mao. 2004-09-14												   !
 * !!!!!!!!!!!!!!!!!=== NOTICE ===!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

#include "stdsys.h"
#include <dlfcn.h>		// for dl*()
#include "pubfunc.h"
#include "new.h"
#include "run_thread.h"
#include "iconv.h"
//----------------------------Jimmy for system function
#include <sys/types.h> 
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <sys/sem.h>
#include <unistd.h>
#include <semaphore.h>
#include "run_mutex.h"
#include "applog.h"
#include "err_code.h"
#include "signal.h"

static HANDLE  g_hMutexSystemHandler = NULL;  //For SYSTEM cmd handler
#define  __MAX_CMD_CHARS	512 //Max 512 char
#define __CMD_MEM_SIZE		1024 // (sizeof(CMD)) //share memory
#define __SYSHAND_PID_FILE	"/var/app/syshand.pid" //Judge the process.


//#define ANSWER_MWM_SIZE 8 //response memory
struct _sCmdItem
{
	int iCmdType; //��������
	char szCmd[__MAX_CMD_CHARS]; //��������
	int iCmdRet; //����Ӧ��
};
typedef struct _sCmdItem CMD;

#define _SEM_CMDWAIT_PATH "/_semSysHandWait"
#define _SEM_ANSWER_PATH "/_semSysHandAnsw"
#define _KEY_T_SYSHAND  (ftok("/app/bin/mkpkg",15)) //syshand���̵�ID
#define IPCKEY 8765
//#define _KEY_T_APP  (ftok("/app/bin/app",0)) //app���̵�ID

static BOOL InitSysHandler(void);
static sem_t* psemWaitCmd = NULL; //�ȴ�����
static sem_t* psemAnswer = NULL; //����Ӧ�����
static int shmid_cmd = -1; //cmd������id,����app���� (��Ϊ����syshand����)

static BOOL InitSysHandler()
{
	//���¿����źŵ�
	psemWaitCmd =  sem_open(_SEM_CMDWAIT_PATH, O_CREAT,0666, 0);//�ź�����ʼֵΪ0
	if(psemWaitCmd == SEM_FAILED || psemWaitCmd == NULL)
	{
		printf( "[dxi-syshand]:sem_open waitCmd Failed!\n" );
		return FALSE;
	}
	psemAnswer =  sem_open(_SEM_ANSWER_PATH, O_CREAT,0666, 0);//�ź�����ʼֵΪ0
	if(psemAnswer == SEM_FAILED || psemAnswer == NULL)
	{
		printf( "[dxi-syshand]:sem_open semAnswer Failed!\n" );
		return FALSE;
	}
	//printf("\n\n[dxi-syshand]:init sem OK");
	//���´��������ڴ���
	key_t keySys = _KEY_T_SYSHAND;
	//key_t keySys = IPCKEY;
	//printf("\n\n[dxi-syshand]:keyt is [%d]",keySys);
	shmid_cmd = shmget(keySys,0,0666);
	if(shmid_cmd == -1)
	{
		printf("\n\n[dxi-syshand]:shmget ini ERRRRR!!!");
		return FALSE;
	}
	//add by Jimmy 20130502
	g_hMutexSystemHandler = Mutex_Create(TRUE);

	if (g_hMutexSystemHandler == NULL)
	{
		/*TRACE("\n\rfilename: data_exchange.c [InitDataExchangeModule]\
			  Create Mutex for System cmd failed");

		AppLogOut(DXI_MODULE_TASK, APP_LOG_ERROR, 
			"[%s]--InitDataExchangeModule: ERROR: "
			"Create Mutex for System cmd failed!\n\r",
			__FILE__);*/

		return FALSE;
	}

	//system("reboot");
	return TRUE;
}
/*==========================================================================*
 * FUNCTION : strncpyz
 * PURPOSE  : copy nDstLen-1 chars from pSrc to pDst, and append '\0' to pDst
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT char  *pDst   : The target string buffer
 *            IN char  *pSrc   : The source string ending with '\0'
 *            IN int   nDstLen : The size of target buffer.
 * RETURN   : char *: The target buffer pointer: pDst
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-11 19:54
 *==========================================================================*/
char *strncpyz(OUT char *pDst, IN const char *pSrc, IN int nDstLen)
{
    char *p = pDst;

    if (p == NULL)
	{
        return NULL;
	}

	if (pSrc != NULL)
	{
		for( nDstLen -= 1; *pSrc && (nDstLen > 0); nDstLen -- )
		{
			*p ++ = *pSrc ++;
		}
	}

    *p = '\0';   /* append a string end flag '\0'    */

    return pDst;
}

/*==========================================================================*
 * FUNCTION : strncpyz_f
 * PURPOSE  : add filter funtion of strncpyz(replace the illegal character
 *			  with '?')
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT char  *pDst   : The target string buffer
 *            IN char  *pSrc   : The source string ending with '\0'
 *            IN int   nDstLen : The size of target buffer.
 * RETURN   : char *: The target buffer pointer: pDst
 * COMMENTS : 
 * CREATOR  : LinTao                DATE: 2005-03-06 11:05
 *==========================================================================*/
char *strncpyz_f(OUT char *pDst, IN const char *pSrc, IN int nDstLen, 
				REPLACE_ILLEGAL_CHR_PROC pfn)
{
    char *p = pDst;

    if (p == NULL)
	{
        return NULL;
	}

	if (pSrc != NULL)
	{
		for( nDstLen -= 1; *pSrc && (nDstLen > 0); nDstLen -- )
		{
			//*p = *pSrc ++;
			*p ++ = pfn(*pSrc ++);
		}
	}

    *p = '\0';   /* append a string end flag '\0'    */

    return pDst;
}


/*=====================================================================*
 * Function name: GetFileLength
 * Description  : 
 * Argument     : FILE *fp	: uses std method to get the file length
 * Return type  : long
 *
 * Create       : Mao Fuhua    2002-12-24 16:44:38
 * Comment(s)   : 
 *--------------------------------------------------------------------*/
long GetFileLength( IN FILE *fp )
{
        long lCurPos = ftell( fp );     // get cur pos
        long lFileLen;

        fseek( fp, 0, SEEK_END );       // move pointer to end

        lFileLen = ftell( fp );         // get cur pos is the file length

        fseek( fp, lCurPos, SEEK_SET ); // restore the pos

        return lFileLen;
}


#define SELECT_MAX_WAIT_INTERVAL	MAX_WAIT_INTERVAL	// second

/*==========================================================================*
 * FUNCTION : Sleep
 * PURPOSE  : sleep with the interval dwMilliseconds
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN DWORD  dwMilliseconds : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-17 16:18
 *==========================================================================*/
#define _USES_SELECT_SLEEP	1

#ifdef _USES_SELECT_SLEEP
void Sleep( IN DWORD dwMilliseconds )
{
    struct timeval tv;

	if (dwMilliseconds == 0)
	{
		sleep(0);
		return;
	}

	while (dwMilliseconds > 0)
	{
		RUN_THREAD_HEARTBEAT();

		if (dwMilliseconds < (SELECT_MAX_WAIT_INTERVAL*1000))
		{
			tv.tv_usec = dwMilliseconds%1000*1000;		/* usec     */
			tv.tv_sec  = dwMilliseconds/1000;			/* seconds  */

			dwMilliseconds = 0;
		}
		else
		{
			tv.tv_usec = 0;							/* usec     */
			tv.tv_sec  = SELECT_MAX_WAIT_INTERVAL;  /* seconds  */

			dwMilliseconds -= (SELECT_MAX_WAIT_INTERVAL*1000);
		}
		
		select(0,NULL,NULL,NULL, &tv); 
	}
}

#else

void Sleep( IN DWORD dwMilliseconds )
{
	int	nSeconds		= (int)(dwMilliseconds/1000);
	int	nMicroSeconds  = (int)(dwMilliseconds%1000*1000);

	// if no us, don't sleep.
	if (nMicroSeconds > 0)
	{
		usleep( (unsigned int)nMicroSeconds );
	}

	// sometimes, we need to sleep(0) to give up the CPU. 
	while (nSeconds >= 0)
	{
		RUN_THREAD_HEARTBEAT();

		if (nSeconds <= MAX_WAIT_INTERVAL)
		{
			sleep((unsigned int)nSeconds);
			break;
		}
		else
		{
			sleep(MAX_WAIT_INTERVAL);
			nSeconds -= MAX_WAIT_INTERVAL;
		}
	}
}
#endif

/*==========================================================================*
 * FUNCTION : NEW_strdup
 * PURPOSE  : dup a string which can be free used DELETE macro
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szSrc : 
 * RETURN   : char *: the created string (end of '\0')
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-20 09:43
 *==========================================================================*/
char * NEW_strdup(IN const char *szSrc)
{
	char *p;
	size_t  iLen = strlen(szSrc);

	if (szSrc == NULL)
	{
		return NULL;
	}
	
	p = NEW(char, iLen + 1);
	if (p == NULL)
	{
		return NULL;
	}

	strcpy(p, szSrc);
	//p[iLen] = '\0';

	return p;
}

/*==========================================================================*
 * FUNCTION : NEW_strdup_f
 * PURPOSE  : add filter funtion of NEW_strdup(replace the illegal character
 *			  with '?')
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szSrc : 
 * RETURN   : char *: the created string (end of '\0')
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-20 09:43
 *==========================================================================*/
char * NEW_strdup_f(const char *szSrc,REPLACE_ILLEGAL_CHR_PROC pfn)
{
	char *p;
	size_t  iLen = strlen(szSrc);

	if (szSrc == NULL)
	{
		return NULL;
	}
	
	p = NEW(char, iLen + 1);
	if (p == NULL)
	{
		return NULL;
	}

	strncpyz_f(p, szSrc, iLen + 1, pfn);
	//strcpy(p, szSrc);
	//p[iLen] = '\0';

	return p;
}

/*==========================================================================*
 * FUNCTION : NEW_straddz
 * PURPOSE  : add two string, use NEW macro to get memory
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szSrc1 : 
 *            const char  *szSrc2 : 
 * RETURN   : char * : 
 * COMMENTS : the successfully created string will end of '\0'
 * CREATOR  : LinTao                   DATE: 2004-09-20 09:46
 *==========================================================================*/
char * NEW_straddz(const char *szSrc1, const char *szSrc2)
{
	char *szDst, *p;
	size_t iLen1, iLen2;

	if (szSrc1 == NULL || szSrc2 == NULL)
	{
		return NULL;
	}

	iLen1 = strlen(szSrc1);
	iLen2 = strlen(szSrc2);

	szDst = NEW(char, iLen1 + iLen2 +1);
	if (szDst == NULL)
	{
		return NULL;
	}
	
	strcpy(szDst, szSrc1);
	p = szDst + iLen1;
	strcpy(p, szSrc2);
	szDst[iLen1 + iLen2] = '\0';

	return szDst;
}

/*==========================================================================*
 * FUNCTION : NEW_strcpy
 * PURPOSE  : safe strcpy (use NEW macro to get memory if memory is not enough)
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char        **szDst : 
 *            const char  *szSrc  : 
 * RETURN   : char * : 
 * COMMENTS : szSrc need to be end with NULL
 * CREATOR  : LinTao                   DATE: 2004-09-20 09:49
 *==========================================================================*/
char * NEW_strcpy(char **szDst, const char *szSrc)
{
	size_t iDstLen, iSrcLen;
	char *p;

	if (szDst == NULL || szSrc == NULL)
	{
		return NULL;
	}

	if (*szDst == NULL)
	{
		return NULL;
	}

	iDstLen = strlen(*szDst);
	iSrcLen = strlen(szSrc);

	if (iDstLen >= iSrcLen)
	{
		strcpy(*szDst, szSrc);

		return *szDst;
	}
	else
	{
		p = NEW(char, iSrcLen + 1);

		if (p == NULL)
		{
			return NULL;
		}

		strcpy(p, szSrc);

		DELETE(*szDst);
		*szDst = p;
	
		return p;
	}
}

/*=====================================================================*
 * Function name: WaitFiledReadable
 * Description  : 
 * Arguments    : int fd	: 
 *                int nmsTimeOut: milliseconds 
 * Return type  : int : 1: data ready, 0: timeout, -1: wait error.
 *
 * Create       : Mao Fuhua    2000-10-30 17:06:54
 * Comment(s)   : Modified by maofuhua, 2004-09-22
 *--------------------------------------------------------------------*/
int WaitFiledReadable(int fd, int nmsTimeOut/* milliseconds*/)
{
    fd_set fdset, fderr;
    struct timeval tv;
    int rv = WAIT_DATA_TIMEOUT;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.

		if (nmsTimeOut < (SELECT_MAX_WAIT_INTERVAL*1000))
		{
			tv.tv_usec = nmsTimeOut%1000*1000;		/* usec     */
			tv.tv_sec  = nmsTimeOut/1000;			/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			tv.tv_usec = 0;							/* usec     */
			tv.tv_sec  = SELECT_MAX_WAIT_INTERVAL;  /* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (SELECT_MAX_WAIT_INTERVAL*1000);
		}

		FD_ZERO(&fdset);		/*  Initializes the set to the NULL set. */
		FD_SET(fd, &fdset);		/*  Adds descriptor s to set. */

		fderr = fdset;
		
		/* determines the status of one or more sockets, waiting if necessary, 
		 * to perform synchronous I/O.     */
#ifdef unix
		rv = select(fd+1, &fdset, NULL, &fderr, &tv);
#else
		rv = select(FD_SETSIZE, &fdset, NULL, &fderr, &tv);
#endif

		if (rv > 0)	// data ready or error.
		{
			rv = (FD_ISSET(fd, &fdset)) ? WAIT_DATA_READY	// OK
				: (FD_ISSET(fd, &fderr)) ? WAIT_DATA_EXCEPT	// except
				: WAIT_DATA_ERROR;							// error??
			break;
		}
		else if (rv < 0)
		{
			rv = WAIT_DATA_ERROR;							// error??
			break;
		}
	}

    return rv;  /* select error, or time out */
}



/*=====================================================================*
 * Function name: WaitFiledWritable
 * Description  : 
 * Arguments    : int fd	: 
 *                int nmsTimeOut: milliseconds
 * Return type  : int : 1: data ready, 0: timeout, -1: wait error.
 *
 * Create       : Mao Fuhua    2000-10-30 17:08:10
 * Comment(s)   : Modified by maofuhua, 2004-09-22
 *--------------------------------------------------------------------*/
int WaitFiledWritable(int fd, int nmsTimeOut/* milliseconds*/)
{
    fd_set fdset, fderr;
    struct timeval tv;
    int rv = WAIT_DATA_TIMEOUT;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (SELECT_MAX_WAIT_INTERVAL*1000))
		{
			tv.tv_usec = nmsTimeOut%1000*1000;		/* usec     */
			tv.tv_sec  = nmsTimeOut/1000;			/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			tv.tv_usec = 0;							/* usec     */
			tv.tv_sec  = SELECT_MAX_WAIT_INTERVAL;  /* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (SELECT_MAX_WAIT_INTERVAL*1000);
		}

		FD_ZERO(&fdset);		/*  Initializes the set to the NULL set. */
		FD_SET(fd, &fdset);		/*  Adds descriptor s to set. */

		fderr = fdset;
		
		/* determines the status of one or more sockets, waiting if necessary, 
		* to perform synchronous I/O.     */
#ifdef unix
		rv = select(fd+1, NULL, &fdset, &fderr, &tv);
#else
		rv = select(FD_SETSIZE, NULL, &fdset, &fderr, &tv);
#endif

		if (rv > 0)	// data ready or error.
		{
			rv = (FD_ISSET(fd, &fdset)) ? WAIT_DATA_READY	// OK
				: (FD_ISSET(fd, &fderr)) ? WAIT_DATA_EXCEPT	// except
				: WAIT_DATA_ERROR;							// error??
			break;
		}
		else if (rv < 0)
		{
			rv = WAIT_DATA_ERROR;							// error??
			break;
		}
	}

    return rv;  /* select error, or time out */
}

#ifdef unix
/***
*char *_strupr(string) - map lower-case characters in a string to upper-case
*
*Purpose:
*   _strupr() converts lower-case characters in a null-terminated string
*   to their upper-case equivalents.  Conversion is done in place and
*   characters other than lower-case letters are not modified.
*
*   In the C locale, this function modifies only 7-bit ASCII characters
*   in the range 0x61 through 0x7A ('a' through 'z').
*
*   If the locale is not the 'C' locale, MapStringW() is used to do
*   the work.  Assumes enough space in the string to hold result.
*
*Entry:
*   char *string - string to change to upper case
*
*Exit:
*   input string address
*
*Exceptions:
*   The original string is returned unchanged on any error.
*
*******************************************************************************/

char * strupr(char * string)
{
    char * cp;

    for (cp=string; *cp; ++cp)
    {
        if ('a' <= *cp && *cp <= 'z')
            *cp += 'A' - 'a';
    }

    return(string);
}

#endif


/*==========================================================================*
 * FUNCTION : GetCurrentTime
 * PURPOSE  : unit is seconds.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : double : the current time in seconds.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-13 16:50
 *==========================================================================*/
double GetCurrentTime(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	return (double)tv.tv_sec + (double)tv.tv_usec/(double)1000000;
}




/*==========================================================================*
 * FUNCTION : LoadDynamicLibrary
 * PURPOSE  : load a shared lib
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const char  *pszLibName    : 
 *            IN int         nSym           : 
 *            IN const char  *ppszSymName[] : array of symbols
 *            OUT HANDLE     *pfnProc[]     : array to save symbol addr
 *            IN BOOL	 	 bNeedAllSym    : TRUE: all symbols shall be loaded
 *                                          FALSE: at least one symbol be loaded
 * RETURN   : HANDLE : non-NULL for OK, NULL for error.
 * COMMENTS : if bNeedAllSym is FALSE, some sym may be not found. 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 17:28
 *==========================================================================*/
HANDLE LoadDynamicLibrary(IN const char *pszLibName,
					   IN int			nSym,
					   IN const char	*ppszSymName[],
					   OUT HANDLE		*pfnProc[],
					   IN BOOL			bNeedAllSym)
{
	int		i;
	HANDLE  hLib;

	//printf("Now load the lib of %s\n", pszLibName);
	//1. open lib
	hLib = (HANDLE)dlopen(pszLibName, RTLD_LAZY);
	if (hLib == NULL)
	{
		TRACE("[LoadDynamicLibrary] -- dlopen %s fails on error %s.\n",
			pszLibName, dlerror() );
		/*printf("[LoadDynamicLibrary] -- dlopen %s fails on error %s.\n",
		    pszLibName, dlerror() );*/

		return NULL;
	}

	//2. load all symbols
	for (i = 0; i < nSym; i++)
	{
        *pfnProc[i] = (HANDLE)dlsym(hLib, ppszSymName[i]);
		if (*pfnProc[i] == NULL)
		{
			TRACE("[LoadDynamicLibrary] -- Fails on getting the "
				"address of \"%s\".\n",
				ppszSymName[i]);
			/*printf("[LoadDynamicLibrary] -- Fails on getting the "
			    "address of \"%s\".\n",
			    ppszSymName[i]);*/

			if (bNeedAllSym)	// error. due to all sym shall be found
			{				
				dlclose(hLib);
				return NULL;
			}
		}
	}

	return hLib;	// OK
}



/*==========================================================================*
 * FUNCTION : UnloadDynamicLibrary
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hLib : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:59
 *==========================================================================*/
void UnloadDynamicLibrary(IN HANDLE hLib)
{
	if (hLib != NULL)
	{
		dlclose(hLib);
	}
}


//for memory opt
#ifdef RENEW_OPT_IMP
static BOOL ArrayMemory_GetBoundary(int iBase, int iVal, unsigned int *iOrder)
{
	int i, boundaryValue;
	int iTemp = 1;

	if (iVal == 0)
	{
		*iOrder = 0;
		return TRUE;
	}

	for ( i = 0; i < 15; i++)
	{
		boundaryValue = iBase * iTemp;
		if (boundaryValue >= iVal)
		{
			*iOrder = i+1;
			return (boundaryValue == iVal ? TRUE: FALSE);
		}

		iTemp = iTemp*2;
	}

	//overflow
	return FALSE;
}

static int ArrayMemory_IntPower(unsigned int iIndex)
{
	unsigned int i;
	int iRet = 1;

	for (i = 0; i < iIndex; i++)
	{
		iRet *= 2;
	}

	return iRet;
}


static void *AppendArrayItemEx(IN OUT void **ppArray, IN int nItemSize,
					  IN OUT int *pCurItems, IN void *pNew)
{
#define BATCH_BASE    5   //request N items at once

#ifdef RENEW_OPT_DEBUG
	static int iRenew = 0;
	static int iSkipRenew = 0;
#endif

	char	*pNewArray;
	size_t	pOldSize = *pCurItems * nItemSize;

	unsigned int iOrder;


	if (ArrayMemory_GetBoundary(BATCH_BASE, *pCurItems, &iOrder))  //need renew
	//if (*pCurItems % BATCH_BASE == 0)  
	{
#ifdef RENEW_OPT_DEBUG
		printf("Append Array traceing(before, request memory): CurrIndex=%d, iOrder=%d\n", *pCurItems, iOrder);

		iRenew++;
#endif

		pNewArray = RENEW(char, *ppArray, nItemSize*(BATCH_BASE)*ArrayMemory_IntPower(iOrder));
		//pNewArray = RENEW(char, *ppArray, nItemSize*(BATCH_BASE)*((*pCurItems) / BATCH_BASE+1));

		if (pNewArray != NULL)
		{
			memmove(&pNewArray[pOldSize], pNew, (size_t)nItemSize);
			(*pCurItems)++;
		}
		else
		{
			TRACEX("out of memory, delete the old.\n");

#ifdef RENEW_OPT_DEBUG
			printf("PANIC!!!!\n");
#endif

			if (*ppArray != NULL)
			{
				DELETE(*ppArray);
			}
		}

		*ppArray = pNewArray;

	}
	else  //only copy the new item to the extra space
	{

#ifdef RENEW_OPT_DEBUG
		printf("Append Array traceing(before, skip RENEW): CurrIndex=%d, iOrder=%d\n", *pCurItems, iOrder);
		iSkipRenew++;
#endif

		memmove(&((char *)(*ppArray))[pOldSize], pNew, (size_t)nItemSize);
		(*pCurItems)++;
	}

#ifdef RENEW_OPT_DEBUG	
	printf("Append Array traceing: CurrIndex=%d, iSkipMemRequset=%d, iMemRequest=%d\n\n", *pCurItems,iSkipRenew, iRenew);
#endif

	return *ppArray;
}
#endif


/*==========================================================================*
 * FUNCTION : AppendArrayItem
 * PURPOSE  : append a new item to ppArray, and increase the cur item num
 *            *pCurItems. The memory of *ppArray will be enlarged.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT void  **ppArray  : The item will be appened to array,
 *                                      IN the old ptr, OUT the enlarged ptr 
 *            IN int       nItemSize  : an item size of array
 *            IN OUT int   *pCurItems : current item size in, +1 when out if OK
 *            IN void      *pNew      : the new item
 * RETURN   : void *: NULL for out of memory, the old array is DELETED.
 *                    else for OK.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-31 20:25
 *==========================================================================*/
void *AppendArrayItem(IN OUT void **ppArray, IN int nItemSize,
					  IN OUT int *pCurItems, IN void *pNew)
{
#ifndef RENEW_OPT_IMP

	char	*pNewArray;
	size_t	pOldSize = *pCurItems * nItemSize;

	pNewArray = RENEW( char, *ppArray, (pOldSize+nItemSize));
	if (pNewArray != NULL)
	{
		memmove(&pNewArray[pOldSize], pNew, (size_t)nItemSize);
		(*pCurItems)++;
	}
	else
	{
		TRACEX("out of memory, delete the old.\n");

		if (*ppArray != NULL)
		{
			DELETE(*ppArray);
		}
	}

	*ppArray = pNewArray;

	return *ppArray;

#endif

#ifdef RENEW_OPT_DEBUG
	static int iTotal = 0;

	iTotal++;
	printf("Total call: %d \n", iTotal);
#endif

	return AppendArrayItemEx(ppArray, nItemSize, pCurItems, pNew);

}



/*==========================================================================*
 * FUNCTION : MakeFullFileName
 * PURPOSE  : to make a full path as pszRootDir/pszSubDir/pszFileName
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT char       *pFullName   : buffer to save full name, size must
 *                                          be greater than MAX_FILE_PATH(256)
 *            IN const char  *pszRootDir  : if NULL or "", treat it as "."
 *            IN const char  *pszSubDir   : if NULL or "", treat it as "."
 *            IN const char  *pszFileName : 
 * RETURN   : char *: return pFullName
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-10 09:47
 *==========================================================================*/
char *MakeFullFileName(OUT char *pFullName,
							  IN const char *pszRootDir,
							  IN const char *pszSubDir,
							  IN const char *pszFileName)
{
	if ((pszRootDir == NULL) || (*pszRootDir == 0))
	{
		pszRootDir = ".";
	}

	if ((pszSubDir == NULL) || (*pszSubDir == 0))
	{
		pszSubDir = ".";
	}

	snprintf(pFullName, MAX_FILE_PATH, "%s/%s/%s",
		pszRootDir, pszSubDir, pszFileName);

	return pFullName;
}


/*==========================================================================*
 * FUNCTION : FindIntItemIndex
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  nItemToFind : 
 *            IN int  *pIntArray  : 
 *            IN int  nItemNum    : 
 * RETURN   : int : -1: for not found, other is the index of the item
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-19 20:46
 *==========================================================================*/
int FindIntItemIndex(IN int nItemToFind, IN int *pIntArray, IN int nItemNum)
{
	int i;

	if (pIntArray != NULL)
	{
		for (i = 0; i < nItemNum; i++, pIntArray++)
		{
			if(nItemToFind == *pIntArray)
			{
				return i;
			}
		}
	}

	return -1;
}


/*==========================================================================*
 * FUNCTION : *TimeToString
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN time_t   tmTime      : 
 *            const char  *fmt        : see help of strftime
 *            OUT char    *strTime    : 
 *            IN int      nLenStrTime : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-28 14:53
 *==========================================================================*/
char *TimeToString(IN time_t tmTime, const char *fmt, 
				   OUT char *strTime, IN int nLenStrTime)
{
	struct tm gmTime;

	// conver time, maofuhua, 04-11-12
	gmtime_r(&tmTime, &gmTime);

	// convert time to yyyy-mm-dd hh:mm:ss
	strftime(strTime, (size_t)nLenStrTime, fmt, &gmTime);

	return strTime;
}

/*==========================================================================*
* FUNCTION : CalculateArctan
* PURPOSE  : calculate the value of arctan 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float x
* RETURN   : float : 
* COMMENTS : added for CR# 0357-10-HF NGC
*
* CREATOR  : Sean Hsu                   DATE: 2010-11-01 16:20
*==========================================================================*/
float CalculateArctan(float tan)   
{
	//atan(x)=x-x^3/3+x^5/5-x^7/7+.....(-1<x<1)  
	//return:[-pi/2,pi/2]   
	float mult,sum,xx;  
	float sum_last = 0.0;
	sum=0.0;   
	int i;
	int flag = 0;

	if(tan == 1)
	{   
		return PI/4;   
	}   

	if(tan == -1)
	{   
		return -PI/4;   
	}   

	if ((tan > 1) || (tan < -1))
	{
		mult = 1 / tan;
	}
	else
	{
		mult = tan;
	}
	xx = mult * mult;   

	for(i=1;i<200;i+=2)
	{   
		flag = (i + 1) % 4 == 0 ? -1:1;
		sum += (mult * flag / i);
		mult *= xx;  

		if (fabs(sum - sum_last) < 0.01)
		{
			break;
		}
		sum_last = sum;
	} 

	if(tan > 1)
	{   
		return PI/2 - sum;   
	}
	else if (tan < -1)
	{
		return -PI/2 - sum;
	}
	else
	{   
		return sum;   
	}   
} 

/*==========================================================================*
* FUNCTION : CalculateArccos
* PURPOSE  : calculate the value of arccos 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float x
* RETURN   : float : 
* COMMENTS : added for CR# 0357-10-HF NGC
*
* CREATOR  : Sean Hsu                   DATE: 2010-11-01 16:20
*==========================================================================*/
float CalculateArccos(float cos)
{
	float arc;
	float tan;

	//acos(x)=��/2 - asin(x) = ��/2 - atan(x/sqrt(1-x^2))

	//debug
	//struct timeval start,end;
	//int time_need;

	//gettimeofday(&start, NULL); 

	if (cos == 1)
	{
		arc = 0.0;
	}
	else if (cos == -1)
	{
		arc = PI;
	}
	else
	{
		tan = cos / sqrt(1 - cos * cos);
		arc = PI/2 - CalculateArctan(tan);
	}

	//gettimeofday(&end, NULL); 
	//time_need = (end.tv_sec-start.tv_sec)*1000000 + end.tv_usec - start.tv_usec;
	//printf("@@@xsf: arccos time need %ld usec!!!!\n", time_need);

	return arc;
}

/*==========================================================================*
* FUNCTION : code_convert
* PURPOSE  : Convert from code to another code 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : t : 
* COMMENTS : 
*
* CREATOR  :Marco                  DATE: 2013-8-07 16:20
*==========================================================================*/
//����ת��:��һ�ֱ���תΪ��һ�ֱ���
static int code_convert(char *from_charset,char *to_charset,char *inbuf,int inlen,char *outbuf,int outlen)
{
	iconv_t cd;
	int rc;
	char **pin = &inbuf;
	char **pout = &outbuf;

	cd = iconv_open(to_charset,from_charset);
	if (cd==0) return -1;
	memset(outbuf,0,outlen);
	//Frank Wu, 20160224, for fixing up memmory leak issue
	//if (iconv(cd,pin,&inlen,pout,&outlen)==-1) return -1;
	if (iconv(cd,pin,&inlen,pout,&outlen)==-1)
	{
		iconv_close(cd);
		return -1;
	}
	else
	{
		iconv_close(cd);
		return 0;
	}
}
/*==========================================================================*
* FUNCTION : UTF8toGB2312
* PURPOSE  : UNICODE�� TO GB2312 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : t : 
* COMMENTS : Should have preloadable_libiconv.so to get this function
*
* CREATOR  :Marco                  DATE: 2013-8-07 16:20
*==========================================================================*/

int UTF8toGB2312(char *inbuf,int inlen,char *outbuf,int outlen)
{
	return code_convert("utf-8","GBK",inbuf,inlen,outbuf,outlen);
}

int UTF8toKOI8(char *inbuf,int inlen,char *outbuf,int outlen)
{
	return code_convert("utf-8","KOI8-R",inbuf,inlen,outbuf,outlen);
}


/*==========================================================================*
* FUNCTION : UTF8toGB2312
* PURPOSE  : GB2312 to UNICODE��
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : t : 
* COMMENTS : Should have preloadable_libiconv.so to get this function
*
* CREATOR  :Marco                  DATE: 2013-8-07 16:20
*==========================================================================*/

int GBKtoUTF8(char *inbuf,size_t inlen,char *outbuf,size_t outlen)
{
	return code_convert("GBK","utf-8",inbuf,inlen,outbuf,outlen);
}

int KOI8toUTF8(char *inbuf,size_t inlen,char *outbuf,size_t outlen)
{
	return code_convert("KOI8-R","utf-8",inbuf,inlen,outbuf,outlen);
}
/*********************************************************************************
*  
*  FUNCTION NAME : _SYSTEM
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2013-5-2 14:22:53
*  DESCRIPTION   : handle the 'system' function by communicating with another process
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/

int _SYSTEM(char* pszCmd)
{
#define _SYSTEM_ERR_OK			0
#define _SYSTEM_ERR_CMD_EMPT		-5
#define _SYSTEM_ERR_EXE			-9
#define _SYSTEM_ERR_INI			-10
#define _SYSTEM_ERR_EXE_TIMEOUT	-11

	int nError = _SYSTEM_ERR_OK;
	CMD* pCmd;
	int iTimes = 0,iSemRet = 0;

	if(psemWaitCmd == NULL || psemAnswer == NULL || shmid_cmd == -1)
	{
		BOOL bIni = InitSysHandler();
		if(!bIni)
		{
			printf("\n------system handler initialize error!!!\n");
			return _SYSTEM_ERR_INI;
		}
	}
#define _LOCK_WAIT_TIMEOUT		30000
	if (Mutex_Lock(g_hMutexSystemHandler,_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	{
		return ERR_DXI_MUTEX_WAIT_TIMEOUT;
	}
	//while(Mutex_Lock(g_hMutexSystemHandler,_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
	//{
	//	//һֱ�ȵ������ɹ�;
	//}

	if(strcmp(Cfg_RemoveWhiteSpace(pszCmd),"") == 0)
	{
		nError = _SYSTEM_ERR_CMD_EMPT;
	}
	else
	{
		//printf("\n------[dxi-syshand]:Jimmy, I Got a system cmd: [%s] \n",pszCmd);

		pCmd = (CMD*)shmat(shmid_cmd,0,0);  
		if(pCmd == (CMD*)-1)
		{
			nError = _SYSTEM_ERR_EXE;
			printf("\n[dxi-syshand]:Write CMD is fail\n");
			Mutex_Unlock(g_hMutexSystemHandler);
			return nError;
		}
		snprintf(pCmd->szCmd,sizeof(pCmd->szCmd),pszCmd);
		pCmd->iCmdType = 0; //0 ��ʾsystem����
		pCmd->iCmdRet = -1; //��ʼ��
		if(shmdt((void*)pCmd) == -1)
		{
			printf("\n\n[dxi-syshand]:shmdt is fail\n");
		}
		sem_post(psemWaitCmd); //���������syshan���̶�ȡ
		//���¿�ʼ�ȴ�syshand��ִ��
		BOOL bAnswOk = FALSE;
		for(iTimes = 0; iTimes < 400; iTimes++) //����60s
		{
			iSemRet = sem_trywait(psemAnswer);
			if(iSemRet == 0)
			{
				bAnswOk = TRUE;
				pCmd = (CMD*)shmat(shmid_cmd,0,0);  
				if(pCmd == (CMD*)-1)
				{
					nError = _SYSTEM_ERR_EXE;
					printf("\n[dxi-syshand]:Write CMD is fail\n");
					break;
				}
				if(pCmd->iCmdRet == 0) //0��ʾ�ɹ�ִ��
				{
					nError = _SYSTEM_ERR_OK;
				}
				else
				{
					nError = _SYSTEM_ERR_EXE;
				}
				if(shmdt((void*)pCmd) == -1)
				{
					printf("\n\n[dxi-syshand]:shmdt is fail\n");
				}

				break;
			}
			Sleep(300);
		}
		if(!bAnswOk)
		{
			nError = _SYSTEM_ERR_EXE_TIMEOUT; //ûӦ�𣬿����Ǳ߽����Ѿ�������Ҫ����
			AppLogOut("PUB-SYSHAND", APP_LOG_ERROR, 
				"[%s]--[system] call error: "
				"Syshand process NO answer, need restart system!\n\r", __FILE__);
			printf("\n\n[dxi-syshand]:Syshand process NO answer, need restart system\n");
			//system("reboot");
			raise(SIGTERM); 
			raise(SIGTERM); 
		}		
	}
	RunThread_Heartbeat(RunThread_GetId(NULL));
	Mutex_Unlock(g_hMutexSystemHandler);
	return nError;

}
int _CloseSystemMutex(void)
{
	if(g_hMutexSystemHandler)
	{
		Mutex_Destroy(g_hMutexSystemHandler);
	}
	return;

}


#define  TEMP_FILTER_VAL 5
#define  MAX_TEMP_MAGIC_COUNT 5 
//moved from G4, used for smooth the temperature when EMC testing.
void  TmepSmoothingAlgorithm(float *pfOldVal,float *pfCurrVal,float *pfMagicSum,int iMagicCountCH)
{
	float fCurrVal = *pfCurrVal ;  //��ǰ�¶�
	float fOldVal = *pfOldVal ;    //ǰһ���¶�
	float fMagicSum;                  // ħ�����ֵ 
	static float fStepOn = 0.5 ;   // �ݽ����ɵ���  
	static int iMagicCountArr[3] ={0,0,0}; //3ͨ����������
	static float fMagicBuf[3][MAX_TEMP_MAGIC_COUNT] ={0}; //3=channal ,MAX_TEMP_MAGIC_COUNT ħ�����
	int  i; 
	int iMagicCount = 0;

	if(iMagicCountCH > 2 )
	{	
		return;//��������Խ�籣��
	}
	 
	//printf("\n Begin CH:%d |####:old:%0.2f,next:%0.2f\n",iMagicCountCH,fOldVal,fCurrVal);
	if(fCurrVal < -270.0 || fOldVal< -270.0)
	{
		//���� �������߲��������澯 
		fOldVal = fCurrVal ;
	}
	else
	{
		//���������¶ȣ�������ʼ����ִ��һ��; 
		if(fOldVal<-100 ) //����Curr�����¶��Ǹ����Ŀ���
		{ 
			*pfCurrVal = fCurrVal ; 
			*pfOldVal = fCurrVal ;  //���ɼ�¼�ȸ�ֵ 
			return;  
		}
		// �¶ȷ�Χ���ȶ��¶�ʱ���� ���ݿɲɼ���
		//  if(fOldVal>100 || fOldVal<-50)
		//  {
		//       fCurrVal = fOldVal;
		//         *pfCurrVal = fCurrVal ; 
		//         *pfOldVal = fOldVal ;
		//         return;
		//  }
		if(fOldVal>70.0)  //��ǰֵһֱ�������������µ�󣬲������˾�����ʱ��󣬹��¿�ȷ��Ӧ�ü�ʱ���ֳ���
		{
			//��ǰ�¶Ȼ���������һ������70�ȣ��Ͳ����������������������´�����ʱ
			fOldVal = fCurrVal ; 
			*pfCurrVal = fCurrVal ; 
			*pfOldVal = fOldVal ;
			return ; 
		}

		//LIN: if increase   ��С����TEMP_FILTER_VAL   ȱ�㣺�Ż�ͻ��ҲҪ����ƽ���Ÿ澯
		if( (fCurrVal-fOldVal<TEMP_FILTER_VAL) && (fCurrVal-fOldVal>-TEMP_FILTER_VAL))
		{
			//printf("11 ################\n");
			//��ǰħ���㷨���������  
			iMagicCount  = iMagicCountArr[iMagicCountCH]; 

			*pfMagicSum += fCurrVal  ; // ��β��
			
			//printf("Begin:%f\n",*pfMagicSum);
			if(iMagicCount<MAX_TEMP_MAGIC_COUNT)   
			{  

				//��ʼ��������5��
				fCurrVal = *pfMagicSum/(iMagicCount+1) ; 
				//ħ�����ۼӽ׶�
				fMagicBuf[iMagicCountCH][iMagicCount] = fCurrVal;
				iMagicCount++ ;
				iMagicCountArr[iMagicCountCH] = iMagicCount; 
			}
			else
			{        
				//����ħ���ֵ����      
				*pfMagicSum -= fMagicBuf[iMagicCountCH][0]; //ȥͷ       
				fCurrVal = *pfMagicSum /MAX_TEMP_MAGIC_COUNT  ; 
							   	//����ħ��ȥͷ��
				for(i=0;i<MAX_TEMP_MAGIC_COUNT-1;i++)
				{
					fMagicBuf[iMagicCountCH][i] = fMagicBuf[iMagicCountCH][i+1] ;
				}
				//������ֵ��������λ��
				fMagicBuf[iMagicCountCH][iMagicCount-1] = fCurrVal;  //�����쳣ħ�������
			}
				fOldVal = fCurrVal ;  //���µ�ǰ�¶ȣ���¼��һ���¶� 
				//DEBUG 
				//for(i=0;i<iMagicCount+1;i++)         //���»������� 
				//{
					//printf("%f\t",fMagicBuf[iMagicCountCH][i]) ;
				//}
				//printf("\n");
		}
		else
		{
			//printf("22 ################\n");
			//�쳣ʱ�¶ȣ�ͻȻ�¶ȴ���TEMP_FILTER_VAL��curval�ݽ���ʽ
			if(fCurrVal>fOldVal)
			{
				fOldVal += fStepOn ; 
			}
			else
			{
				fOldVal -= fStepOn ; 
			}
			fCurrVal = fOldVal ;  // �������¶ȣ�����¶��𽥵����ݼ�
			
			// Խ����������ħ�� 
			iMagicCountArr[iMagicCountCH] = 0;
			*pfMagicSum = 0 ; 
		}	
	}
	//printf("End:%f\n",*pfMagicSum);
	//printf("End CH:%d |####:old:%0.2f,next:%0.2f\n",iMagicCountCH,fOldVal,fCurrVal);
	//printf("\n #####################temp end ################\n");
	*pfCurrVal = fCurrVal;
	*pfOldVal = fOldVal;
}


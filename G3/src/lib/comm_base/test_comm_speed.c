/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : test_comm_speed.c
 *  CREATOR  : Frank Mao                DATE: 2005-08-16 10:16
 *  VERSION  : V1.00
 *  PURPOSE  : To test the communication speed
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <signal.h>
#include "stdsys.h"		/* all standard system head files			*/
#include "basetypes.h"
#include "pubfunc.h"
#include "halcomm.h"
#include "new.h"
#include "err_code.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


typedef struct _ERR_CODE_MSG
{
	int			nErrCode;
	char		*pErrMsg;
}ERR_CODE_MSG;

#define DEF_ERR_MSG(code, msg)		{(code), (msg)}

static ERR_CODE_MSG s_pErrMsg[] = 
{
	/* general errors */
	DEF_ERR_MSG(ERR_COMM_OK				, "OK"),
	DEF_ERR_MSG(ERR_COMM_NO_MEMORY		, "Out of memory"),
	DEF_ERR_MSG(ERR_COMM_TIMEOUT		, "Timeout"),
	DEF_ERR_MSG(ERR_COMM_OPENING_PORT	, "Opening port failure"),
	DEF_ERR_MSG(ERR_COMM_OPENING_PARAM	, "Invalid opening params"),
	DEF_ERR_MSG(ERR_COMM_SUPPORT_ACCEPT , "Client port doesn't support accept"),
	DEF_ERR_MSG(ERR_COMM_MANY_CLIENTS   , "Too many clients to accept"),
	DEF_ERR_MSG(ERR_COMM_ACCEPT_FAILURE , "Accept failure"),
	DEF_ERR_MSG(ERR_COMM_READ_DATA      , "Read data failure"),
	DEF_ERR_MSG(ERR_COMM_WRITE_DATA     , "Write data failure"),
	DEF_ERR_MSG(ERR_COMM_CONNECT_SERVER	, "Connect server failure"),
	DEF_ERR_MSG(ERR_COMM_CREATE_SERVER	, "Create server failure"),
	DEF_ERR_MSG(ERR_COMM_PORT_HANDLE	, "Invalid port handle, maybe closed"),
	DEF_ERR_MSG(ERR_COMM_CTRL_COMMAND	, "Invalid control command"),
	DEF_ERR_MSG(ERR_COMM_CTRL_PARAMS	, "Invalid control params"),
	DEF_ERR_MSG(ERR_COMM_CTRL_EXECUTION	, "Control failure"),
	DEF_ERR_MSG(ERR_COMM_BUFFER_SIZE	, "Invalid control buffer size"),
	DEF_ERR_MSG(ERR_COMM_CONNECTION_BROKEN,"Connection broken"),
	DEF_ERR_MSG(ERR_COMM_STD_PORT		, "Invalid standard port type"),
	DEF_ERR_MSG(ERR_COMM_LOADING_DRIVER , "Load standard port driver failure"),

	/* special for TCP/IP and UDP port */

	/* special for dialling serial port */
	DEF_ERR_MSG(ERR_COMM_CTRL_MODEM		, "Modem control failure"),
	DEF_ERR_MSG(ERR_COMM_DIAL_PHONE		, "Modem dail failure"),
	DEF_ERR_MSG(ERR_COMM_PHONE_BUSY		, "Phone busy"),
	DEF_ERR_MSG(ERR_COMM_HANGUP_PHONE	, "Hangup failure"),
	DEF_ERR_MSG(ERR_COMM_NO_CARRIER		, "No carrier"),
	DEF_ERR_MSG(ERR_COMM_NO_DIALTONE	, "No dial tone"),
};


char *CommGetErrorMessage(int nErrCode)
{
	int	i;
	ERR_CODE_MSG *p = s_pErrMsg;


	for (i = 0; i < ITEM_OF(s_pErrMsg); i++, p++)
	{
		if (p->nErrCode == nErrCode)
		{
			return p->pErrMsg;
		}
	}

	return "Unkown error";
}

#define TEST_DATA_LENGTH	32768

static char *GetStandardPortDriverName(IN int nStdPortType)
{
	const char *pszStdDriver[] =
	{
		"comm_std_serial.so",
		"comm_hayes_modem.so",
		"comm_net_tcpip.so",
		"comm_acu_485.so"
	};

	return (char *)pszStdDriver[nStdPortType];
}



static int s_nRunning = 1;
static void QuitSignalHandler( int a )
{
	UNUSED(a);

    if( s_nRunning > 0)
    {
        printf( "Receives a quit signal. Quiting ... \007\007\n" );
        s_nRunning = 0;
    }
    else
    {
        printf( "App is quiting, please wait ... \007\007\n" );
		s_nRunning--;
		if (s_nRunning < -5)
		{
			exit(-1);
		}
    }
}


static void dump_buf( char *buf, int n)
{
	while (n > 0)
	{
		printf( "%c", *buf ++);
		n --;
	}

	printf( "\n" );
}

static void ClearRxTx( HANDLE hPort)
{
	CommControl( hPort, COMM_PURGE_TXCLEAR, (void*)1, 0);
	CommControl( hPort, COMM_PURGE_RXCLEAR, (void*)1, 0);
}


static void ShowLastError( HANDLE hPort, char *pszMsg )
{
	int n;
	int		nErrCode;

	n = CommControl( hPort, 
		COMM_GET_LAST_ERROR, 
		(char *)&nErrCode,
		sizeof(nErrCode) );
	if (n != ERR_COMM_OK)
	{
		printf( "  ===>%s: error on getting error code got: %08x.\n",
			pszMsg, n );
	}
	else
	{
		printf( "  ===>%s: error code: %08x(%s).\n",
			pszMsg, nErrCode, CommGetErrorMessage(nErrCode) );
	}
}


//#pragma pack(1)
struct _TEST_DATA_FRAME
{
#define DATA_FRAME_FLAG			0x55aa0816
	DWORD		dwFrameFlag;	// always be 0x55aa0816
	DWORD		dwCmd;
	int			nDataLength;

	DWORD		dwHeadChecksum;
	DWORD		dwDataChecksum;

	char		pData[0];		// data
};

typedef struct _TEST_DATA_FRAME TEST_DATA_FRAME;
//#pragma pack()

static DWORD ChecksumDataFrame(char *pData, int nDataLength)
{
	int		i;
	DWORD	dwDataChecksum = 0;

	for (i = 0; i < nDataLength; i++)
	{
		dwDataChecksum += pData[i];
	}

	return dwDataChecksum;
}

static void ShowDataFrame(char *pszMsg, TEST_DATA_FRAME *pFrame)
{
	printf("%s: flag: 0x%08x, length:%d, headsum: 0x%08x, datasum: 0x%08x, cmd:%d.\n",
		pszMsg,
		(unsigned int)pFrame->dwFrameFlag,
		pFrame->nDataLength, 
		(unsigned int)pFrame->dwHeadChecksum,
		(unsigned int)pFrame->dwDataChecksum,
		(int)pFrame->dwCmd);
}


static char s_pTestData[TEST_DATA_LENGTH*100];
static void PrepareTestData(void)
{
	int		i;

	srand((unsigned int)time(NULL));

	for (i = 0; i < ITEM_OF(s_pTestData); i++)
	{
		s_pTestData[i] = rand()%256;
	}
}


static void	MakeDataFrame(TEST_DATA_FRAME *pFrame, DWORD dwCmd, int nPkgSize)
{
	int		i;
	static int nLastDataIdx = 0;

	pFrame->dwFrameFlag = DATA_FRAME_FLAG;
	pFrame->nDataLength = nPkgSize-sizeof(TEST_DATA_FRAME);
	pFrame->dwCmd       = dwCmd;

	for (i = 0; i < pFrame->nDataLength; i++)
	{
		pFrame->pData[i] = s_pTestData[nLastDataIdx++];

		if (nLastDataIdx >= ITEM_OF(s_pTestData))
		{
			nLastDataIdx = 0;
		}
	}

	pFrame->dwHeadChecksum = ChecksumDataFrame((char *)pFrame, 
		(int)&((TEST_DATA_FRAME *)NULL)->dwHeadChecksum);
	pFrame->dwDataChecksum = ChecksumDataFrame(pFrame->pData, 
		pFrame->nDataLength);
}



BOOL SendDataFrame(HANDLE hPort, TEST_DATA_FRAME *pSendFrame, int nPkgSize)
{
	int		nErrCode;
	int		n;

	//ShowDataFrame("Send", pSendFrame);

	n = CommWrite( hPort, (char *)pSendFrame, nPkgSize );
	if (n == nPkgSize)
	{
		return TRUE;

	}
	
	nErrCode = CommGetLastError(hPort);
	printf( "===> Write data to port failure, error code: 0x%08x(%s).\n",
		nErrCode, CommGetErrorMessage(nErrCode) );

	return FALSE;
}


BOOL RecvDataFrame(HANDLE hPort, TEST_DATA_FRAME *pRecvFrame)
{
	int		nErrCode;
	int		n;

	n = CommRead(hPort, (char *)pRecvFrame, sizeof(TEST_DATA_FRAME));
	if (n == sizeof(TEST_DATA_FRAME))
	{
		if ((pRecvFrame->dwFrameFlag == DATA_FRAME_FLAG))
		{
			DWORD dwHeadChecksum = ChecksumDataFrame((char *)pRecvFrame, 
				(int)&((TEST_DATA_FRAME *)NULL)->dwHeadChecksum);
			if (pRecvFrame->dwHeadChecksum != dwHeadChecksum)
			{
				printf("===> Error head checksum 0x%08x, re-check got 0x%08x.\n",
					(unsigned int)pRecvFrame->dwHeadChecksum, 
					(unsigned int)dwHeadChecksum);
				return FALSE;
			}

			n = CommRead(hPort, (char *)pRecvFrame->pData, pRecvFrame->nDataLength);
		
			//ShowDataFrame("Recv", pRecvFrame);

			if (n == pRecvFrame->nDataLength)
			{
				DWORD dwDataChecksum = ChecksumDataFrame(pRecvFrame->pData,
					pRecvFrame->nDataLength);
				if (pRecvFrame->dwDataChecksum == dwDataChecksum)
				{
					return TRUE;
				}
				else
				{
					printf("===> Error data checksum 0x%08x, re-check got 0x%08x.\n",
						(unsigned int)pRecvFrame->dwDataChecksum, 
						(unsigned int)dwDataChecksum);
					return FALSE;
				}
			}
		}
		else if (pRecvFrame->dwFrameFlag != DATA_FRAME_FLAG)
		{
			printf("===> Error frame flag 0x%08ux, it should be 0x%08ux.\n",
				(unsigned int)pRecvFrame->dwFrameFlag, 
				(unsigned int)DATA_FRAME_FLAG);
			return FALSE;
		}
	}

	nErrCode = CommGetLastError(hPort);
	printf( "===> Read data from port failure, error code: 0x%08x(%s).\n",
		nErrCode, CommGetErrorMessage(nErrCode) );

	return FALSE;
}


static void StatCommSpeed(char *pszType,/* client or server */
						  int nSentPkg, int nSentBytes, double fSentTime,
						  int nRecvPkg, int nRecvBytes, double fRecvTime,
						  double fElapsedTime)
{
#define KILO		1024
#define EPSILLON	0.00001
	printf("\n\n%s: Comm speed stat(1K=%dbytes).\n", pszType, KILO);

	if (fSentTime < EPSILLON || fElapsedTime < EPSILLON || fRecvTime < EPSILLON)
	{
		printf("Elapsed time(sent:%f, recv:%f, total:%f) is too little to stat.\n",
			fSentTime, fRecvTime, fElapsedTime);
		return;
	}

	printf("Stat\tPackages\tBytes      \tElapsed(s)\tSpeed(kb/s)\n");
	
	printf("Sent\t%8d\t%8d\t%f\t%f\n",
		nSentPkg, nSentBytes, fSentTime, 
		nSentBytes/fSentTime/KILO);

	printf("Recv\t%8d\t%8d\t%f\t%f\n",
		nRecvPkg, nRecvBytes, fRecvTime, 
		nRecvBytes/fRecvTime/KILO);

	printf("Total\t%8d\t%8d\t%f\t%f\n",
		(nRecvPkg+nSentPkg), (nRecvBytes+nSentBytes), fElapsedTime,
		(nRecvBytes+nSentBytes)/fElapsedTime/KILO);

	printf("\n");
}

enum TEST_COMM_COMMAND
{
	CMD_COMM_START = 0,
	CMD_COMM_DATA  = 1,	
	CMD_COMM_STOP  = 2,
};


static int SetCommIntervalCharTimeout(HANDLE hPort, int nIntervalMs)
{
	COMM_TIMEOUTS to;
	int nErrCode;

	if (CommControl(hPort, COMM_GET_TIMEOUTS, (char *)&to, sizeof(to)) != ERR_COMM_OK)
	{
		nErrCode = CommGetLastError(hPort);
		printf( "===> Port get timeout failure, error code: %08x(%s).\n", 
			nErrCode, CommGetErrorMessage(nErrCode) );

		return nErrCode;
	}
	else
	{
		INIT_TIMEOUTS_EX(to, to.nReadTimeout, to.nWriteTimeout, nIntervalMs);

		if (CommControl(hPort, COMM_SET_TIMEOUTS, (char *)&to, sizeof(to)) != ERR_COMM_OK)
		{
			nErrCode = CommGetLastError(hPort);
			printf( "===> Port set timeout failure, error code: %08x(%s).\n", 
				nErrCode, CommGetErrorMessage(nErrCode) );

			return nErrCode;
		}
	}

	return ERR_COMM_OK;
}


int TestClient(int nStdType, IN char *pszDevDescriptor, IN char *pszOpenParams,
			   IN int nTimeoutMS, IN int nTotalBytes, IN int nPkgSize, 
			   IN int nTotalTime) /*valid when nTotalBytes <= 0*/
{
	HANDLE	hPort;
	int		nErrCode = ERR_COMM_OK;
	int     nRecvBytes = 0, nSentBytes = 0;
	int		nSentPkg = 0, nRecvPkg = 0;
	char	szSendBuf[TEST_DATA_LENGTH];
	char	szRecvBuf[TEST_DATA_LENGTH];
	double  fCurrentTime=0, fLastTime = 0;
	double  fBeginTime=0, fEndTime=0, fElapsedTime, fSentTime = 0, fRecvTime = 0;
	DWORD	dwCmdState;
    
	TEST_DATA_FRAME	*pSendFrame = (TEST_DATA_FRAME *)szSendBuf;
	TEST_DATA_FRAME	*pRecvFrame = (TEST_DATA_FRAME *)szRecvBuf;

	printf("Communication Speed Test -- Client.\n"
		"Port type: %d, port name: %s, port param: %s, timeout:%d\n"
		"Total bytes: %d, package size: %d, total time: %ds\n", 
		nStdType,  pszDevDescriptor, pszOpenParams,
		nTimeoutMS, nTotalBytes, nPkgSize, nTotalTime);

	if ((0 >= nPkgSize) || 
		(nPkgSize > (int)(TEST_DATA_LENGTH-sizeof(TEST_DATA_FRAME))))
	{
		printf("Error package size %d, shall be 0 < Pack Size < %d.\n",
			nPkgSize, TEST_DATA_LENGTH-sizeof(TEST_DATA_FRAME));
		return -1;
	}

	hPort = CommOpen( GetStandardPortDriverName(nStdType),
		pszDevDescriptor, 
		pszOpenParams, 
		COMM_CLIENT_MODE,
		nTimeoutMS,
		&nErrCode);

	printf( "===> Port %s opened %s, error code: %08x(%s).\n", 
		pszOpenParams, (hPort != NULL) ? "OK" : "failure",
		nErrCode, CommGetErrorMessage(nErrCode) );

	if (hPort == NULL)
	{
		return nErrCode;
	}
    
	nErrCode = SetCommIntervalCharTimeout(hPort, 1000);
	if (nErrCode != ERR_COMM_OK)
	{
		CommClose(hPort);
		return nErrCode;
	}


	ClearRxTx( hPort );

	// start
	dwCmdState = CMD_COMM_START;

	printf( "===> Communication is start. \n");

	while(TRUE)
	{
		MakeDataFrame(pSendFrame, dwCmdState, nPkgSize);
	
		fCurrentTime = GetCurrentTime();
		if (SendDataFrame(hPort, pSendFrame, nPkgSize))
		{
			if (pSendFrame->dwCmd == CMD_COMM_DATA)
			{
				fEndTime  = GetCurrentTime();
				fSentTime += fEndTime-fCurrentTime;
				nSentPkg++;
				nSentBytes += nPkgSize;
			}
		}

		else
		{
			break;
		}
	
		// read response
		fCurrentTime = GetCurrentTime();
		if (RecvDataFrame(hPort, pRecvFrame))
		{
			if (pRecvFrame->dwCmd == CMD_COMM_DATA)
			{
				fEndTime  = GetCurrentTime();
				fRecvTime += fEndTime - fCurrentTime;
				nRecvPkg++;			
				nRecvBytes += sizeof(TEST_DATA_FRAME) + pRecvFrame->nDataLength;
			}
		}
		else
		{
			break;
		}

		if (pSendFrame->dwCmd == CMD_COMM_START)
		{
			dwCmdState = CMD_COMM_DATA;

			fBeginTime = GetCurrentTime();
			printf( "===> Communication is in progress(%f). Please wait...\n",
				fBeginTime);

			fLastTime = fBeginTime;
		}
		else if (pRecvFrame->dwCmd == CMD_COMM_STOP)
		{
			break;
		}
		else if (((nTotalBytes > 0) && (nSentBytes >= nTotalBytes)) ||
			((nTotalBytes <= 0) && (fEndTime > 0) && (fBeginTime > 0) && 
			(fEndTime-fBeginTime >= nTotalTime)) ||
			(s_nRunning <= 0))
		{
			dwCmdState = CMD_COMM_STOP;
		}

		if (fCurrentTime - fLastTime > 5)
		{
			printf( "%d ", (int)(fCurrentTime - fBeginTime)); 	fflush(stdout);
			
			fLastTime = fCurrentTime;
		}
	}

	fElapsedTime = fEndTime-fBeginTime;	
	printf( "===> Communication is end. \n");

	CommClose(hPort);

	// stat
	StatCommSpeed("Client", 
		nSentPkg, nSentBytes, fSentTime,
		nRecvPkg, nRecvBytes, fRecvTime,
		fElapsedTime);

	return nErrCode;
}


int TestIncomingClient( HANDLE hPort )
{
	int		nPkgSize = 0;
	int		nErrCode = ERR_COMM_OK;
	int     nRecvBytes = 0, nSentBytes = 0;
	int		nSentPkg = 0, nRecvPkg = 0;
	char	szSendBuf[TEST_DATA_LENGTH];
	char	szRecvBuf[TEST_DATA_LENGTH];
	TEST_DATA_FRAME	*pSendFrame = (TEST_DATA_FRAME *)szSendBuf;
	TEST_DATA_FRAME	*pRecvFrame = (TEST_DATA_FRAME *)szRecvBuf;
	int		nWaitData = 10;
	double  fCurrentTime=0, fLastTime = 0;
	double  fBeginTime=0, fEndTime=0, fElapsedTime, fSentTime = 0, fRecvTime = 0;

	printf( "Communication Speed Test -- Incoming Client...\n");

	nErrCode = SetCommIntervalCharTimeout(hPort, 1000);
	if (nErrCode != ERR_COMM_OK)
	{
		CommClose(hPort);
		return nErrCode;
	}


	while(TRUE)
	{
		// read response
		fCurrentTime = GetCurrentTime();
		if (RecvDataFrame(hPort, pRecvFrame))
		{
			nPkgSize   = sizeof(TEST_DATA_FRAME) + pRecvFrame->nDataLength;

			if (pRecvFrame->dwCmd == CMD_COMM_DATA)
			{
				fEndTime  = GetCurrentTime();
				fRecvTime += fEndTime - fCurrentTime;
				nRecvPkg++;		
				nRecvBytes += nPkgSize;
			}
		}
		else 
		{
			nWaitData--;
			if ((nWaitData > 0) && (int)fBeginTime == 0 && s_nRunning > 0)
			{
				continue;
			}
			else
			{
				printf("===> Timeout on waiting client data.\n");
				break;
			}
		}

		if (s_nRunning > 0)
		{
			memmove(pSendFrame, pRecvFrame, (size_t)nPkgSize);
		}
		else if (nPkgSize != 0)
		{
			MakeDataFrame(pSendFrame, CMD_COMM_STOP, nPkgSize);
		}
		else
		{
			break;
		}

		fCurrentTime = GetCurrentTime();
		if (SendDataFrame(hPort, pSendFrame, nPkgSize))
		{
			if (pSendFrame->dwCmd == CMD_COMM_DATA)
			{
				fEndTime  = GetCurrentTime();
				fSentTime += fEndTime-fCurrentTime;
				nSentPkg++;
				nSentBytes += nPkgSize;
			}
		}
		else
		{
			break;
		}

		if (pSendFrame->dwCmd == CMD_COMM_START)
		{
			fBeginTime = GetCurrentTime();

			printf( "===> Communication is in progress. \n"
				"Start at %1.6fs, Please wait...\n", fBeginTime);

			fLastTime = fBeginTime;
		}
		else if ((pRecvFrame->dwCmd == CMD_COMM_STOP) || s_nRunning<= 0)
		{
			break;
		}

		if (fCurrentTime - fLastTime > 5)
		{
			printf( "%d ", (int)(fCurrentTime - fBeginTime)); 	fflush(stdout);
			
			fLastTime = fCurrentTime;
		}
	}

	fElapsedTime = fEndTime-fBeginTime;	
	printf( "===> Communication is end. \n");

	nErrCode = CommClose(hPort);
	printf( "  ===> Close port %s, got error code %08x(%s).\n",
		(nErrCode == ERR_COMM_OK) ? "OK" : "failure",
		nErrCode, CommGetErrorMessage(nErrCode) );

	// stat
	StatCommSpeed("Server", 
		nSentPkg, nSentBytes, fSentTime,
		nRecvPkg, nRecvBytes, fRecvTime,
		fElapsedTime);

	return nErrCode;
}

int TestServer(int nStdType,
			   IN char *pszDevDescriptor, IN char *pszOpenParams,
			   IN int nTimeoutMS )
{
	HANDLE	hPort;
	int		nErrCode = ERR_COMM_OK, nLastErr = 0;
	int     n;
    
	printf( "0. Testing server port %s...\n", pszOpenParams );
	printf( "1. Opening port %s...\n", pszOpenParams );

	hPort = CommOpen(GetStandardPortDriverName(nStdType),
		pszDevDescriptor, 
		pszOpenParams, 
		COMM_SERVER_MODE,
		nTimeoutMS,
		&nErrCode);

	printf( "  ===> Port %s opened %s, error code: %08x(%s).\n", 
		pszOpenParams, (hPort != NULL) ? "OK" : "failure",
		nErrCode, CommGetErrorMessage(nErrCode) );

	if (hPort == NULL)
	{
		return nErrCode;
	}


	printf( "2. Testing accept...\n" );

	while (s_nRunning > 0)
	{
		HANDLE hClient = CommAccept( hPort );

		if (hClient != NULL) 
		{
			COMM_PEER_INFO cpi;
		    pthread_t hThread;

			n = CommControl(hClient, 
				COMM_GET_PEER_INFO, 
				(char *)&cpi,
				sizeof(COMM_PEER_INFO) );

			if (n == ERR_COMM_OK)
			{
				printf( "\n\n============>The accepted port is %d, IP is %s.\n",
					cpi.nPeerPort, inet_ntoa(*(struct in_addr *)cpi.szAddr));
			}

			pthread_create( &hThread, 
				NULL,
				(PTHREAD_START_ROUTINE)TestIncomingClient,
				(void *)hClient );
		}

		else 
		{
			nErrCode = CommGetLastError(hPort);

			if (nLastErr != nErrCode)
			{
				if (nErrCode == ERR_COMM_TIMEOUT)
				{
					printf( "  ===> Accept timeout\n" );
				}
				else if (nErrCode == ERR_COMM_MANY_CLIENTS)
				{
					printf( "  ===> Accept ERR_COMM_MANY_CLIENTS\n" );
				}
				else
				{
					printf( "  ===> Accept failure and error code is %08x(%s).\n",
						nErrCode, CommGetErrorMessage(nErrCode) );

					break;
				}
			}

			nLastErr = nErrCode;
		}

	}	

	printf( "\nWaiting for client quit..."); fflush(stdout);
	while((CommControl(hPort, COMM_GET_CUR_CLIENTS, (char *)&n, sizeof(n)) == ERR_OK) &&
		(n > 0))
	{
		Sleep(1000);
		printf( "."); fflush(stdout);
	}

	nErrCode = CommClose(hPort);
	printf( "===> Close port %s, got error code %08x(%s). Server exited.\n",
		(nErrCode == ERR_COMM_OK) ? "OK" : "failure",
		nErrCode, CommGetErrorMessage(nErrCode) );

	return nErrCode;
}


/*==========================================================================*
 * FUNCTION : main
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int    argc    : 
 *            INT char  *argv[] : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-15 17:31
 *==========================================================================*/
int main(IN int argc, IN char *argv[])
{
	signal(SIGTERM, QuitSignalHandler);
	signal(SIGINT, QuitSignalHandler);

	PrepareTestData();
	
	if((argc == 9) && (strcmp(argv[1], "-c") == 0))
	{
		TestClient( atoi(argv[2]), argv[3], argv[4], atoi(argv[5]),
			atoi(argv[6]), atoi(argv[7]), atoi(argv[8]));
		return 0;
	}

	else if((argc == 6) && (strcmp(argv[1], "-s") == 0))
	{
		TestServer( atoi(argv[2]), argv[3], argv[4], atoi(argv[5]) );
		return 0;
	}

	printf( 
		"Usage: test_comm_speed <-c|-s> std-port-id port-descr port-set timeout [total-size pkg-size total-time]\n"
		" purpose           std-port-id  port-descriptor   port-setting\n"
		" test std serial         0        COM1 or COM2      b,p,d,s\n"
		" test dial serial        1        COM1 or COM2      b,p,d,s:tel\n"
		" test TCPIP port         2        eth0              n1.n2.n3.n4:port\n"
		" test ACU485 serial      3        COM4              b,p,d,s\n"
		" total-size, pkg-size, total-time are required by client\n"
		"      if total-size <= 0, the total-time will be used.\n\n"
		);

	return -1;
}

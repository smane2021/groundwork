 /*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : HisData.h
 *  CREATOR  : Li Xidong                DATE: 2004-09-08 09:05:14
 *  VERSION  : D01
 *  PURPOSE  : It include two sides.One is the storage interface functions to 
 *			 : high level modules,the other is the initiate histhorical data
 *			 : management.The later serve to the storage interface function.
 *           : The interface function can't be used until historical data
 *			 : management is initiated.. 
 *==========================================================================*/

#ifndef __HIS_DATA_H
#define __HIS_DATA_H

#ifndef _KERNEL_
#define _KERNEL_
#endif //_KERNEL_

//Definition
#define LEN_DATA_NAME			20
#define DATA_START_SECTOR		3

#define EMERSON					"Vertivv"  

//FLASH Sector relevant reference definition
#define SIZE_VER_RELE_INFO		418  


// Max times whic every sector can be erased
#define MAX_ERASE_TIMES			100000  
// Bytes per sector 
#define SIZE_PER_SECTOR			(65536*2)//65536   
// Years which flash can be used
#define MIN_USED_YEARS			10  

#define MAX_HISDATA_SECTORS		80

// The first Sector in history data storage region-device:dev/mtd6
#define START_SECTOR			1  
//Hisdata's start postion
#define START_POSITION			0		

//Define CRC16 check relevant const
// CRC16 Check Bytes
#define CRC16_BYTES				2	
// Identify record index Bytes
#define RECORD_ID_BYTES			4			

// Data  Struct
//#pragma pack(1)
struct tagSECTORMAPTABLE
{
	BYTE			byLogSectorNo;					//Logic Sector Index
	BYTE			byPhySectorNo;					//Physical Sector Index

};
typedef struct tagSECTORMAPTABLE SECTOR_MAP_TABLE;


//Static historical data information
struct tagStaticStorageRecord
{
	char				DataName[LEN_DATA_NAME];	/* LEN_DATA_NAME -16*/
	int					iTotalSectors;				/* Total Sectors of Current Type Data */
    int					iMaxRecords;				/* Max records can be held */
    int					iRecordSize;				/* A record size in byte    */	    	 
 };
typedef struct tagStaticStorageRecord STATIC_STORAGE_RECORD;

//Version and other relevant information
struct tagVerReleInfo
{
	int				iVerNo;							//Define in app_conf.h file
	char			szBuff[408];					/*SIZE_VER_RELE_INFO - RECORD_ID_BYTES 
													- CRC16_BYTES - sizeof(iVerNo)
													 maybe will be used in the future*/
};
typedef struct tagVerReleInfo VER_RELEVANT_INFO;

//Historical Data Tracking Information
struct tagRecordSetTrack 
{
	char				DataName[LEN_DATA_NAME];	/* LEN_DATA_NAME -16*/
	int					iTotalSectors;				/* Total Sectors of Current Type Data */
    int					iMaxRecords;				/* Max records can be held */
    int					iRecordSize;				/* A record size in byte    */
	////////////////////////////////////////////////////////////////////////////
	//Add erasing sector pre-process function
	ERASE_SECTOR_PRE_PROC
						pfn_preProcess;				/* Before erasing sector,execute this function*/
	HANDLE				hPreProcess;				/* Process handle */
	void				*Params;					
	///////////////////////////////////////////////////////////////////////////
	int					iStartRecordNo;				/* Among latest iMaxRecords records index*/
	///////////////////////////////////////////////////////////////////////////
	BOOL				bSectorEffect;				/* It decide current data can be write in*/
	///////////////////////////////////////////////////////////////////////////
	int					iDataRecordIDIndex;			
	BYTE				byCurrSectorNo;				/* Current Sector Number*/
	BYTE				byNewSectorFlag;			/* 1 Current records < MaxRecords*/   
	int					iMaxRecordsPerSector;		/* Max records can be held in one Sector*/
	int					iWritableRecords;			/* Records that Current Sector can be written*/
	int					iCurrWritePos;				/* The current write record index  */
	int					iCurrReadPos;				/* The current read record index   */
	DWORD				dwCurrRecordID;				/* The Current Record ID*/
	SECTOR_MAP_TABLE	*pbyPhysicsSectorNo;		

	///////////////////////////////////////////////////////////////////////////
	//Used to recover read record tracking info
	BYTE				byOldCurrSectorNo;			/* Current Sector Number*/	    	 
 };
typedef struct tagRecordSetTrack RECORD_SET_TRACK;

//Temperory storage queue
struct tagHISDATAQUEUE
{
	int		iDataRecordIDIndex;			   /* Show different data record id in flash*/
	BYTE	bIsAlarm;                      /* Alarm record				  */
    short	iHead;                         /* Start position in queue*/
    int		iLength;                       /* Current Records Total length*/
	int		iRecordSize;                   /* A record size in byte */
    int		iMaxSize;                      /* Max Records which can hold */
    void	*pHisData;                     /* Point to data record array */
    
 };
typedef struct tagHISDATAQUEUE HISDATA_QUEUE;

//RECORD_SET_TRACK g_RECORD_SET_TRACK[];
//HISDATA_QUEUE	g_HISDATA_QUEUE[];

//#pragma pack()

#ifdef _DEBUG
#define  DAT_ASSERT(Rst, ErrCode, strTaskName, nLevel, OutString) \
	if ((Rst) != ERR_OK) {printf((OutString)); \
	printf("Error code: %d\n", ErrCode);abort();}
#else
#define  DAT_ASSERT(Rst, ErrCode, strTaskName, nLevel, OutString) \
	if ((Rst) != ERR_OK) {\
	AppLogOut(strTaskName, nLevel, "Error code: %d\n", ErrCode);} 
#endif

#endif




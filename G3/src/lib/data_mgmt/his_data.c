
/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : his_data.c
 *  CREATOR  : LIXIDONG               DATE: 2004-09-09 17:22
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"
#include "his_data.h" 


#include <sys/mount.h>
#include <linux/mtd/mtd.h>

#ifdef _DEBUG
//#define   _DEBUG_DAT_MGMT_		1
//#define   _INIT_DAT_MGMT_		1
#endif
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Max temporary records in hisdata queue
#define MAX_TEMP_RECORD_LENGTH	10	
// Temperory add some space to storage new records
#define TEMP_ADD_LENGTH			10	
//RecordID(4) + logic Sec(1) + phyisi Sec(1) + CRC16(2)
#define SECTOR_MAP_REC_LEN		8

///////////////////////////////////////////////////////////////////////////////
//Define write version info 
#define VER_WRITE_MAIN_AND_BAK   3
#define VER_WRITE_BAK_SECTOR     2
#define VER_WRITE_MAIN_SECTOR    1

///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//Read data start position
#define THE_LAST_RECORD			(-1)
#define THE_FIRST_READ			0

///////////////////////////////////////////////////////////////////////////////
//ReadRecordsControl
#define ERR_CTRL_INPUT          (-1)
#define ERR_END_READING         (-2)
//Mutex Lock definition
#define DM_LOCK_WAIT_TIME		0xffffffff
#define DM_LOCK_WAIT_TIME_15		15000

///////////////////////////////////////////////////////////////////////////////
#define		APP_RW_SET_FILE			"/var/jumpset.log"
#define		ACU_JUMP_RDONLY			"ACU_JUMP_APP_RDONLY"
#define		ACU_JUMP_NOT_RDONLY		"NOT SET"
#define		ACU_JUMP_SET_RDONLY		"SET"
#define		ACU_JUMP_NOT_SET		0
#define		ACU_JUMP_READONLY		1
#define		ACU_JUMP_OTHER_ERR		2
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Declare CRC16 checking functions  
static unsigned short CRC16(IN unsigned char *Msg, 
					 IN unsigned short len);


//Declare functions relevant sector operation
static int CalculateSectors(IN int iRecordSize, 
					 IN int iMaxRecords, 
					 IN float iWritefrequencyPerday);

//Check sector is empty or not
static BOOL DAT_SectorIsEmpty(IN int iPhySectorNo);

//Erase data sector in flash
//Used in x86 environment, debug
static int EraseSector_x86(IN int iStartSectorNo, IN int iSectors);
//Used in ppc acutual environment
static int EraseSector_ppc(IN int iStartSectorNo, IN int iSectors);

#ifdef _HAS_FLASH_MEM
#define EraseSector(a,b)       EraseSector_ppc(a,b)
#else
#define EraseSector(a,b)       EraseSector_x86(a,b)
#endif

///////////////////////////////////////////////////////////////////////////////
//Write his data main thread function
static void DAT_fnWriteDataThread(void);
//Write service thread function
static void DAT_fnWriteServiceThread(void);

//Move active alarm record to new sector
static void DAT_MoveAlarmRecord(IN RECORD_SET_TRACK * pTrack);

static BOOL DAT_GetStorageSpace(IN int iDataTrackIndex);


static BOOL RecordSpaceIsEmpty(IN int iRecordSize,
							   IN void *pRecordBuff);

static int GetMaxRecordsReadable(IN RECORD_SET_TRACK *pTrack);

///////////////////////////////////////////////////////////////////////////////
// Erase sector function declaraion 
static int region_erase(IN int Fd, 
						IN int start, 
						IN int count, 
						IN int unlock, 
						IN int regcount);

///////////////////////////////////////////////////////////////////////////////
//Declare data record reading functions 
static int ReadOneRecord(IN FILE *fp, 
				  IN long iOffset,
				  OUT void * pBuff, 
				  IN int iRecordSize);

//Declare data record writing functions 
static int WriteOneRecord(IN void * pRecordBuff, 
				   IN int iRecordSize, 
				   IN DWORD dwRecordID, 
				   IN long iOffset);

//Read more pieces of records from flash device
static int ReadMoreRecords(IN FILE *fp, 
					    IN long iOffset,
						OUT void *pRecordBuff, 
						IN int iRecordSize, 
						IN int iRecords,
						IN BOOL bActiveAlarm);

//Search more pieces of records from flash device
static int FindMoreRecords(IN FILE *fp, 
					IN long iOffset,
					OUT void *pRecordBuff,
					IN int	 iExpectRecs,
					IN int iRecordSize, 
					IN int iRecords, 
					IN COMPARE_PROC pfnCompare, 
					IN void *pCondition,
					IN BOOL bActiveAlarm);

//Search record
static int GetDataTrackIndex(IN RECORD_SET_TRACK *pTrack);

static int GetActualRecordSize(IN RECORD_SET_TRACK *pTrack, 
						IN BOOL bActiveAlarm,
						OUT int *piRecordSize, 
						OUT int *piActualRecordSize);

///////////////////////////////////////////////////////////////////////////////
//Read static parameters and historical data sectors from static sector
static BOOL ReadStaticParas(IN int iStartSector);
static BOOL ReadSectorMapTable(IN long iOffset,  
						IN int iTotalSectors, 
						IN SECTOR_MAP_TABLE *pSector);

static int DAT_ReadStaticParameter(void);
static BOOL DAT_ReadHisDataStatus(void);

///////////////////////////////////////////////////////////////////////////////
static BOOL WriteStaticParameter(IN void *pRecordBuff, 
								IN int iActualRecordSize, 
								OUT DWORD *dwStaticRecordID,
								IN int iMainOrBak);
static BOOL ReWriteStaticParameter(IN int iStaticSector);

static BOOL WriteSectorMapTable(IN SECTOR_MAP_TABLE *pBuff, 
								IN int  iTotalSectors, 
								IN long iOffset);

//Process information when fail to write static parameters
static void FailtoCreateStatPara(IN OUT RECORD_SET_TRACK *pTrack, 
								 IN OUT HISDATA_QUEUE *pQueue);

//Process information when fail to read static parameters
static void FailToReadStaticParas(IN OUT RECORD_SET_TRACK *pTrack, 
								  IN OUT HISDATA_QUEUE *pQueue);


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//g_hWriteThread:write data thread
static HANDLE g_hWriteThread;
static HANDLE g_hWriteServThread;

// Global variables definition:
static BYTE	g_byMaxDataIndex;
static BYTE	g_byCurrMaxSectorNo;

//Globally writing file pointer,it will be used in four conditions
static FILE * g_pFile;

//Global mutex luck variables definition
//The handle of the mutex for write record track,only writing by one user once
static HANDLE g_hMutexWriteRecTrack;
//The handle of the mutex for read record track
static HANDLE g_hMutexReadRecTrack[MAX_DATA_TYPES];
//The handle of the mutex for data queues
static HANDLE g_hMutexQueue[MAX_DATA_TYPES];
//Used to control global variable g_byMaxDataIndex,g_byCurrMaxSectorNo;
static HANDLE g_hMutexCreateHandle;

//Sectors management table
static BOOL g_bSectorMngTable[MAX_HISDATA_SECTORS + 1];

//Flash error state
static int	 g_iFlashRunningState = ERR_DAT_OK;
static int	 g_iFlashOpenState    = ERR_DAT_OK;


//Global variables definition:
static RECORD_SET_TRACK g_RECORD_SET_TRACK[MAX_DATA_TYPES];
static HISDATA_QUEUE	g_HISDATA_QUEUE[MAX_DATA_TYPES];
//added Mutex for flash read and write 20170418  
static BOOL g_bIsEraseSector[MAX_HISDATA_SECTORS + 1];
static HANDLE g_hMutexForbidEraseSector;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Start resource code
/*==========================================================================*
 * FUNCTION : DAT_GetFlashRunningState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   int : 0 :Flash is running normally
 *				       Others:Flash error, it can't write data to flash,
 *						     new history record will be lost
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-27 16:30
 *==========================================================================*/
int	DAT_GetFlashRunningState(void)
{
	return g_iFlashRunningState;
}


/*==========================================================================*
 * FUNCTION : DAT_GetFlashOpenState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   int : 0 :Flash is running normally
 *				      Others:Flash error, it can't write data to flash,
 *					  new history record will be lost,can't read out
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-15 09:17
 *==========================================================================*/
static int	DAT_GetFlashOpenState(void)
{
	return g_iFlashOpenState;
}
/*==========================================================================*
 * FUNCTION : WriteVerReleInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  iSectorToWrite : 1:main static sector 
 *									   2:bak static sector
 *									   3:main and bak static sector
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-03-02 20:23
 *==========================================================================*/
static BOOL WriteVerReleInfo(IN int iSectorToWrite)
{
	VER_RELEVANT_INFO  stVerInfo;
	long			   iOffset;
	int				   i, iResult;
	BOOL			   bWriteAll;

	iOffset = 0;
	bWriteAll = FALSE;
	//It must ensure static sector is empty
	//Write version info to flash static sector
	memset(&stVerInfo, 0, (unsigned)sizeof(VER_RELEVANT_INFO));
	stVerInfo.iVerNo = GET_APP_VER();

	if (iSectorToWrite == START_SECTOR + 2)
	{
		bWriteAll = TRUE;
	}
		
	for (i = 0; i < 2; i++)
	{
		//Historical data device file isn't opened;	
		if (bWriteAll)
		{
			iOffset = i * SIZE_PER_SECTOR;
		}
		else
		{
			iOffset = (iSectorToWrite - 1) * SIZE_PER_SECTOR;
		}
		//Write version information to  main static sector
		
		iResult = WriteOneRecord((void *)&stVerInfo, 
			sizeof(VER_RELEVANT_INFO), 1, iOffset);

		if (iResult == 1)
		{
			//Write again
			iResult = WriteOneRecord((void *)&stVerInfo, 
				sizeof(VER_RELEVANT_INFO), 1, iOffset);
		}
		else if (iResult == 2)  //Waste space
		{
			//Erase static main sector and write again
			EraseSector((i + 1), 1);
			iResult = WriteOneRecord((void *)&stVerInfo, 
			sizeof(VER_RELEVANT_INFO), 1, iOffset);
		}

		if (iResult != ERR_DAT_OK)
		{
			AppLogOut("DAT_READ_STPARA", APP_LOG_ERROR, 
				"Can't write version information to static sector.");
			if ((bWriteAll && (i == 1)) ||
				(!bWriteAll))
			{
				return FALSE;
			}
		}

		if (!bWriteAll)
		{
			break;
		}
	}
	
	//Succeed
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ReadVerReleInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-24 13:32
 *==========================================================================*/
static int ReadVerReleInfo(VER_RELEVANT_INFO *pVerInfo, int iStartNo)
{
	long			   iOffset;
	int				   iResult;

	iOffset = 0;
	iResult = 0;

	//Historical data device file isn't opened;	
	iOffset = (iStartNo - 1) * SIZE_PER_SECTOR;
	//Write version information to  main static sector
	iResult = ReadOneRecord(g_pFile, iOffset, (void *)pVerInfo,	sizeof(VER_RELEVANT_INFO));

	return iResult;
}

/*==========================================================================*
 * FUNCTION : CheckVerReleInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-24 13:32
 *==========================================================================*/
static BOOL CheckVerReleInfo(void)
{
	BOOL bMainEmpty, bBakEmpty;
	//Set original value
	g_byMaxDataIndex = 0; 
	bMainEmpty = FALSE;
	bBakEmpty = FALSE;
	VER_RELEVANT_INFO  stOldVerInfo, stNewVerInfo;

	memset(&stOldVerInfo, 0, (unsigned)sizeof(VER_RELEVANT_INFO));
	memset(&stNewVerInfo, 0, (unsigned)sizeof(VER_RELEVANT_INFO));

	bMainEmpty = DAT_SectorIsEmpty(START_SECTOR);
	bBakEmpty = DAT_SectorIsEmpty(START_SECTOR + 1);
	//printf("bMainEmpty = %d,bBakEmpty = %d\n",bMainEmpty,bBakEmpty );
	//No any historical data store in static Sectors
	if (bMainEmpty && bBakEmpty)   
	{
		//Erase 3-63th sector
		//Default is 0xFF, do not need to format, 2014-8-1.
		//EraseSector(3, (MAX_HISDATA_SECTORS - 2));

		if (!WriteVerReleInfo(VER_WRITE_MAIN_AND_BAK))
		{
			AppLogOut("DAT_READ_STPARA", APP_LOG_ERROR, 
					"Can't write version information to main and bak static sector.");
			return FALSE;
		}

		return TRUE;
	}
		

	//Read version info
	if (ERR_DAT_OK != ReadVerReleInfo(&stOldVerInfo, START_SECTOR))
	{
		memset(&stOldVerInfo, 0, (unsigned)sizeof(VER_RELEVANT_INFO));
        //Try to read bak Sector
		if (ERR_DAT_OK != ReadVerReleInfo(&stOldVerInfo, (START_SECTOR + 1)))
		{
			AppLogOut("DAT_CHK_VER", APP_LOG_ERROR, 
					"Fail to read ver info, it will erased all history data.Rewrite version information");

			if(EraseSector(START_SECTOR, MAX_HISDATA_SECTORS))
			{
				AppLogOut("DAT_CHK_VER", APP_LOG_UNUSED,//APP_LOG_UNUSED, 
					"System has erased all history records and rewrote version information.");
			}

			if(!WriteVerReleInfo(VER_WRITE_MAIN_AND_BAK))
			{
				AppLogOut("DAT_READ_STPARA", APP_LOG_ERROR, 
					"Can't write version information to main and bak static sector.");
				return FALSE;
			}

			return TRUE;
		}
	}		
	
	stNewVerInfo.iVerNo = GET_APP_VER();

	//If the version is different, will erase flash, now remove it.The version change, don't erase flash.
	//if (stNewVerInfo.iVerNo == stOldVerInfo.iVerNo)
	//{
	//	return TRUE;
	//}
	//else
	//{
	//	//Version has been upgrated, it should erase all history record
	//	// change the GET_APP_MINOR_VER protocol. maofuhua. 2005-3-13
	//	if ((GET_APP_MAJOR_VER(GET_APP_VER()) != GET_APP_MAJOR_VER(stOldVerInfo.iVerNo)) ||
	//		(GET_APP_MINOR_VER(GET_APP_VER()) != GET_APP_MINOR_VER(stOldVerInfo.iVerNo)))
	//	{
	//		if(EraseSector(START_SECTOR, MAX_HISDATA_SECTORS))
	//		{
	//			AppLogOut("DAT_CHK_VER", APP_LOG_INFO,//APP_LOG_UNUSED, 
	//				"System has erased all history records for version upgrade.");
	//		}
	//		else
	//		{
	//			AppLogOut("DAT_CHK_VER", APP_LOG_ERROR, 
	//				"Fail to execute erase all sectors when software version upgrade.");
	//		}

	//		if(!WriteVerReleInfo(VER_WRITE_MAIN_AND_BAK))
	//		{
	//			AppLogOut("DAT_READ_STPARA", APP_LOG_ERROR, 
	//				"Can't write version information to main and bak static sector.");
	//			return FALSE;
	//		}
	//	}
	//}	

	//Succeed
	return TRUE;
}
/*==========================================================================*
 * FUNCTION : DAT_IniMngHisData
 * PURPOSE  : When system starts,main module will call this function,
 *			: It will initiate all historical data infomation to 
 *			: read and write 
 * CALLS    : DAT_ReadStaticParameter
 *			: DAT_ReadHisDataStatus
 *			: DAT_fnWriteDataThread
 * CALLED BY: Main module 
 * RETURN   : BOOL : FALSE-Fail to initiate    
 *			:		 TRUE -Succeed to initiate historical management
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-14 16:50
 *==========================================================================*/
BOOL DAT_IniMngHisData(void)
{
	int i, iFileLen;
	BYTE   byData;

	RECORD_SET_TRACK	* pTrack;
	HISDATA_QUEUE	    * pQueue;

	iFileLen = 0;
	byData   = 0;
	//1.Initiate globle variables 
	g_byMaxDataIndex		= 0;
	g_iFlashRunningState	= ERR_DAT_OK;
	g_iFlashOpenState       = ERR_DAT_OK;

	for (i = 0; i < (MAX_HISDATA_SECTORS + 1); i++)
	{   
		//Two static sector must be used
		if (i < 3)
		{
			g_bSectorMngTable[i] = TRUE;
		}
		else
		{
			g_bSectorMngTable[i] = FALSE;
		}
	}
	

	//Start from 3,the first two are used to store static parameter
	g_byCurrMaxSectorNo = DATA_START_SECTOR;  

	//2.Open storage device
#ifndef _HAS_FLASH_MEM      //x86:   MTD_NAME	"mtd6.log"	

	g_pFile = fopen(MTD_NAME, "a+");
	if (g_pFile == NULL)
	{	
		g_iFlashRunningState = ERR_DAT_CANT_WRITE_FLASH;
		g_iFlashOpenState    = ERR_DAT_CANT_WRITE_FLASH;
		return FALSE;
	}

	rewind(g_pFile);
	fseek(g_pFile, 0, SEEK_END);
	iFileLen = ftell(g_pFile);

	if ( iFileLen != MAX_HISDATA_SECTORS * SIZE_PER_SECTOR)
	{
		rewind(g_pFile);
		fseek(g_pFile, 0, SEEK_SET);
		byData = 0xFF;
		//Default is 0xFF. 2014-8-1, it take too long time in the first run.
		/*for (i = 0; i < MAX_HISDATA_SECTORS * SIZE_PER_SECTOR; i++)
		{
			fwrite((void *)&byData, sizeof(BYTE), 1, g_pFile);	
		}*/			
	}

	fclose(g_pFile);
#endif

	g_pFile = fopen(MTD_NAME, "rb+");
	if (g_pFile == NULL)
	{	
		//Added to process error
		g_pFile = fopen(MTD_NAME, "rb+");

		if (g_pFile == NULL)
		{
			g_iFlashRunningState = ERR_DAT_CANT_WRITE_FLASH;
			g_iFlashOpenState    = ERR_DAT_CANT_WRITE_FLASH;
			AppLogOut("DAT_INI_MNG", APP_LOG_ERROR, 
				"Data management module can't open flash,"
				"all historical records will not be written to flash.");
				//return FALSE;    //Let it running normally
			return TRUE;
		}
		else
		{
			AppLogOut("DAT_INI_MNG", APP_LOG_UNUSED, 
				"Data management module opened flash,"
				"on the second attempt.");

		}
	}    

	//3.Create Only one writing lock
	g_hMutexWriteRecTrack = Mutex_Create(TRUE);	
	g_hMutexCreateHandle  = Mutex_Create(TRUE);
	g_hMutexForbidEraseSector = Mutex_Create(TRUE);
	for(i = 0; i<(MAX_HISDATA_SECTORS + 1); i++)
	{
		g_bIsEraseSector[i] = FALSE;
	}
	//4.Initiate tracking array
	for (i = 0; i < MAX_DATA_TYPES; i++)        
	{	
		pTrack = &(g_RECORD_SET_TRACK[i]);
		pQueue = &(g_HISDATA_QUEUE[i]);

		//4.1 Initiate array g_RECORD_SET_TRACK[i]
		memset(pTrack->DataName, 0, sizeof(pTrack->DataName));
		strcpy(pTrack->DataName, EMERSON);
		pTrack->iDataRecordIDIndex		= i;
		pTrack->iMaxRecords				= 0;
		pTrack->iRecordSize				= 0;
		///////////////////////////////////
		//Add erasing sector pre-process function
		pTrack->pfn_preProcess			= NULL;
		pTrack->hPreProcess             = NULL;
		pTrack->Params					= NULL;
		///////////////////////////////////
		pTrack->iStartRecordNo			= THE_FIRST_READ;  //Express first read data
		///////////////////////////////////
		pTrack->bSectorEffect			= TRUE;            //Current data can be write in flash
		///////////////////////////////////
		pTrack->iTotalSectors			= 0;
		pTrack->iMaxRecordsPerSector	= 1;
		pTrack->byCurrSectorNo			= 1;
		pTrack->byOldCurrSectorNo		= 1;
		pTrack->iWritableRecords		= 0;
		pTrack->iCurrWritePos			= 0;
        pTrack->iCurrReadPos			= 0;
		pTrack->dwCurrRecordID			= 1;
		pTrack->pbyPhysicsSectorNo		= NULL;

		//If current data change another sector, TRUE express
		//It will be used in writing service thread
		pTrack->byNewSectorFlag		= FALSE;

		////4.2 Initiate array g_RECORD_SET_TRACK[i]
		pQueue->iDataRecordIDIndex			= 0;
		pQueue->iHead					= 0;
		pQueue->iLength					= 0;
		pQueue->iMaxSize				= MAX_TEMP_RECORD_LENGTH;
		pQueue->bIsAlarm				= FALSE;
		pQueue->pHisData				= NULL;

		//5.Create reading and queue mutex lock
		g_hMutexReadRecTrack[i] = Mutex_Create(TRUE);
		g_hMutexQueue[i]	    = Mutex_Create(TRUE);

		if ((g_hMutexWriteRecTrack == NULL) 
			|| (g_hMutexQueue[i] == NULL) 
			|| (g_hMutexReadRecTrack[i] == NULL))
		{	
			DAT_ClearResource();
			g_iFlashRunningState = ERR_DAT_CANT_WRITE_FLASH;
			g_iFlashOpenState    = ERR_DAT_CANT_WRITE_FLASH;

			return FALSE;
		}

		
	}    //End initiation
	

	if (!CheckVerReleInfo())
	{
        AppLogOut("DAT_CHK_VER", APP_LOG_ERROR, 
			"Fail to check system version information!"
			"Flash has some bad static sectors.");
	}


	if (DAT_ReadStaticParameter() < 0)
	{
		DAT_ClearResource();
		g_iFlashRunningState = ERR_DAT_CANT_WRITE_FLASH;
		g_iFlashOpenState    = ERR_DAT_CANT_WRITE_FLASH;

		AppLogOut("DAT_INI_MNG", APP_LOG_ERROR, 
			"There is a serious mistake in reading static parameter!",
			"Fail to initiate historical storage!");
		return FALSE;
	}     	
	//printf("g_byMaxDataIndex = %d\n", g_byMaxDataIndex);
	if (g_byMaxDataIndex > MAX_DATA_TYPES)  
	{
		g_iFlashRunningState = ERR_DAT_CANT_WRITE_FLASH;
		g_iFlashOpenState    = ERR_DAT_CANT_WRITE_FLASH;

		AppLogOut("DAT_INI_MNG", APP_LOG_ERROR, 
			"There is a serious mistake in reading static parameter!",
			"Fail to initiate historical storage!");
		DAT_ClearResource();

		return FALSE;		 //Fail to read static parameter
	}
    
	if (!DAT_ReadHisDataStatus())
	{
		g_iFlashRunningState = ERR_DAT_CANT_WRITE_FLASH;
		g_iFlashOpenState    = ERR_DAT_CANT_WRITE_FLASH;

		AppLogOut("DAT_INI_MNG", APP_LOG_ERROR, 
			"Fail to read hisdata status, system will exit.");
		DAT_ClearResource();

		return FALSE;
	}



	g_hWriteThread = RunThread_Create("WRT_REC_THD", 
						(RUN_THREAD_START_PROC)DAT_fnWriteDataThread,
						NULL,
						NULL,
						0);	

	if (g_hWriteThread == NULL)
	{
		g_iFlashRunningState = ERR_DAT_CANT_WRITE_FLASH;
		g_iFlashOpenState    = ERR_DAT_CANT_WRITE_FLASH;

		DAT_ClearResource();
		AppLogOut("DAT_INI_MNG", APP_LOG_ERROR, 
			"Fail to create write history data thread.");		
		return FALSE;
	}

	g_hWriteServThread = RunThread_Create("WRI_SERV_THD", 
						(RUN_THREAD_START_PROC)DAT_fnWriteServiceThread,
						NULL,
						NULL,
						0);	
	
	if (g_hWriteServThread == NULL)
	{
		g_iFlashRunningState = ERR_DAT_CANT_WRITE_FLASH;
		g_iFlashOpenState    = ERR_DAT_CANT_WRITE_FLASH;

		DAT_ClearResource();
		AppLogOut("DAT_INI_MNG", APP_LOG_ERROR, 
			"Fail to create service thread.");		
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ReadStaticParas
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:    : int iStartSector    :1:main sector 2:bak sector 
 * RETURN   :	   BOOL : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-20 11:40
 *==========================================================================*/
static BOOL ReadStaticParas(IN int iStartSector)
{
	int		i, j, iReadReturn;
	int		iMaxStaticRecords;
	int		iSectorMapTabSize;
	int		iOneRecordSize;
	long    iOffset;

	BOOL	bReadMainSector;
	int     iExistData;

	STATIC_STORAGE_RECORD	stStaticRec;
	RECORD_SET_TRACK		*pTrack;
	HISDATA_QUEUE			*pQueue;
	SECTOR_MAP_TABLE		*pSector;

	bReadMainSector = TRUE;

	//give a temporary valid value
	iExistData = 100;    
	
	//Sector map table size
	iSectorMapTabSize = MAX_HISDATA_SECTORS * (sizeof(SECTOR_MAP_TABLE)
		+ RECORD_ID_BYTES + CRC16_BYTES);

	//One complete static record size
	iOneRecordSize = sizeof(STATIC_STORAGE_RECORD) + RECORD_ID_BYTES 
		+ CRC16_BYTES + iSectorMapTabSize;
    
	//Max static records which static Sector can keep 
	iMaxStaticRecords = SIZE_PER_SECTOR/iOneRecordSize;

	//Static sector start address
	iOffset = (iStartSector - 1) * SIZE_PER_SECTOR + SIZE_VER_RELE_INFO;	

	//Continuous reading static data records from Flash
	for (i = 0; i < iMaxStaticRecords; i++)  
	{	
		iReadReturn = ReadOneRecord(g_pFile, iOffset, 
			&stStaticRec, sizeof(STATIC_STORAGE_RECORD));

		//Current Space is empty
		if( iReadReturn == 1)			
		{
            break;			
		}
		else if(iReadReturn == 0) //Succeed:valid record	
		{	
			//pTrack = g_RECORD_SET_TRACK;
			pTrack = &(g_RECORD_SET_TRACK[0]);

			/* Check this data existed or not in current existing 
			  g_byMaxDataIndex Data types.*/
			for(j = 0; j < g_byMaxDataIndex; j++)    
			{
				iExistData = strcmp(stStaticRec.DataName, 
					(pTrack +j)->DataName);

				if(iExistData == 0)
				{
					break; 
				}
			}	
			
			//Read Next record
			if(iExistData == 0)      
			{
				iOffset = iOffset + iOneRecordSize;
				continue;          
			}

			//New record
			pTrack = &(g_RECORD_SET_TRACK[g_byMaxDataIndex]);
			pQueue = &(g_HISDATA_QUEUE[g_byMaxDataIndex]);

			//Update data index
			g_byMaxDataIndex++;  
			
			//Update g_RECORD_SET_TRACK[g_byMaxDataIndex] all variables
			strncpyz(pTrack->DataName, stStaticRec.DataName, 
				(size_t)LEN_DATA_NAME);
			pTrack->iTotalSectors = stStaticRec.iTotalSectors;
			pTrack->iMaxRecords = stStaticRec.iMaxRecords;
			pTrack->iRecordSize = stStaticRec.iRecordSize;
			pTrack->iDataRecordIDIndex = i + 1;
			
			//Update g_HISDATA_QUEUE[g_byMaxDataIndex] all variables
			pQueue->iDataRecordIDIndex = i + 1;
			pQueue->iRecordSize = stStaticRec.iRecordSize;

			if (strcmp(stStaticRec.DataName, HIS_ALARM_LOG) == 0)
			{
				pQueue->bIsAlarm = TRUE;
			}
			else
			{
				pQueue->bIsAlarm = FALSE;
			}

			//Allocate memory to store temporary records in queue
			pQueue->pHisData = 
				(void *)NEW(char, pQueue->iMaxSize 
				* (stStaticRec.iRecordSize + RECORD_ID_BYTES + CRC16_BYTES));
		
			//Allocate memory to store sector map table
			pTrack->pbyPhysicsSectorNo =
					NEW(SECTOR_MAP_TABLE, stStaticRec.iTotalSectors);

			pSector = pTrack->pbyPhysicsSectorNo;

			//Initiate Logic Sector Index
			for(j = 0; j < stStaticRec.iTotalSectors; j++)   
			{
				(pSector + j)->byLogSectorNo = j + 1;

				//Temperory set 0
				(pSector + j)->byPhySectorNo = 0;				
			}
			
			//Read sector map tale and update (pSector + i)->byPhySectorNo 
			//with actual physical sector number

			//pSector = pTrack->pbyPhysicsSectorNo;   //Repeated

			iOffset = iOffset + (iOneRecordSize - iSectorMapTabSize);

			if(!ReadSectorMapTable(iOffset, 
				stStaticRec.iTotalSectors, pSector))
			{
				//Recover data track to old condition
				FailToReadStaticParas(pTrack, pQueue);
				bReadMainSector = FALSE;
			}

			//Update offset,prepare to read next record
			iOffset = iOffset + iSectorMapTabSize;

		}
		else     //iReadReturn = 2;
		{
			//Update offset,prepare to read next record
			iOffset = iOffset + iOneRecordSize;
			bReadMainSector = FALSE;
		}//End if
 
	} //End for

	return bReadMainSector;
}


/*==========================================================================*
 * FUNCTION : ReadSectorMapTable
 * PURPOSE  : Read sector map table and update historical data's physical 
 *			  sector number
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: long				  iOffset, : 
 *            int                 iTotalSectors : 
 *            SECTOR_MAP_TABLE	  *pSector       : 
 * RETURN   : BOOL : TRUE:Succeed    FALSE:Current sector map table is invalid
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-20 14:38
 *==========================================================================*/
static BOOL ReadSectorMapTable(IN long iOffset, 
							   IN int iTotalSectors, 
							   IN SECTOR_MAP_TABLE *pSector)
{
	int iReadReturn, i, k;
	int iSucceedCounts;
	long iCurPos;
	int iSecMapRecSize;
	int iEmptCounts;
	int iSectorNo;

	SECTOR_MAP_TABLE stSctMapTab;
	
	iCurPos = iOffset;
	iSecMapRecSize = sizeof(SECTOR_MAP_TABLE) + RECORD_ID_BYTES + CRC16_BYTES;
	//Start to read sector map table in FLASH after read static parameter
	iSucceedCounts = 0;
	iEmptCounts = 0;

	for (k = 0; k < MAX_HISDATA_SECTORS; k++)
	{
		iReadReturn = ReadOneRecord(g_pFile, iCurPos, 
			&stSctMapTab, sizeof(SECTOR_MAP_TABLE));

		if( iReadReturn == 2)	       //Current record is invalid
		{            
            iCurPos = iCurPos + iSecMapRecSize;	
			iEmptCounts = 0;
			continue;
		}
		else if(iReadReturn == 0)      //Succeed to read valid record
		{
			iSucceedCounts++;
			iSectorNo = stSctMapTab.byPhySectorNo;
			for(i = 0; i < iTotalSectors; i++)
			{
				//Update physical sector number according logic sector index
				if((pSector + i)->byLogSectorNo == stSctMapTab.byLogSectorNo)
				{
                    (pSector + i)->byPhySectorNo = stSctMapTab.byPhySectorNo;

					if (((START_SECTOR + 1) < iSectorNo) &&
						(iSectorNo < (MAX_HISDATA_SECTORS + 1)))
					{
						g_bSectorMngTable[iSectorNo] = TRUE;
					}
				}

				//Update g_byCurrMaxSectorNo, it will be used in allocating sector 
				//or change old bad sector
				if(stSctMapTab.byPhySectorNo >= g_byCurrMaxSectorNo)
				{
					g_byCurrMaxSectorNo = stSctMapTab.byPhySectorNo + 1;
				}
			}
			
			iEmptCounts = 0;
			iCurPos = iCurPos + iSecMapRecSize;
		}
		else		//Current record is empty;
		{
			iCurPos = iCurPos + iSecMapRecSize;
			iEmptCounts++;
			if (iEmptCounts > 2)   //continuous three record is empty,exit
			{
				break;
			}
			
		}

	} //end for

	
	//Check all physical sector is right or not
	if(iSucceedCounts >= iTotalSectors)
	{
		for (k = 0; k < iTotalSectors; k++)
		{
			if ((pSector + k)->byPhySectorNo <= 1)
			{				
				return FALSE;
			}            
		}
		return TRUE;
	}
	else   //Something is wrong
	{
		return FALSE;
	}
	
}


/*==========================================================================*
 * FUNCTION : FailToReadStaticParas
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK *  pTrack : 
 *            HISDATA_QUEUE *     pQueue : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-20 17:14
 *==========================================================================*/
static void FailToReadStaticParas(IN OUT RECORD_SET_TRACK *pTrack, 
								  IN OUT HISDATA_QUEUE *pQueue)
{	
	//Set current data invalid
	strcpy(pTrack->DataName, EMERSON);  

	//free memory
	if (pTrack->pbyPhysicsSectorNo != NULL)
	{
		DELETE(pTrack->pbyPhysicsSectorNo);	
	}

	if (pQueue->pHisData != NULL)
	{
		DELETE(pQueue->pHisData);
	}

	//Recover current total valid data types numbers
	g_byMaxDataIndex--;            

	return;
}




/*==========================================================================*
 * FUNCTION : SearchCurrPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE  bySectorNo           : 
 *            int   iActualStoreRecordSize          : 
 *            int   iMaxRecordsPerSector : 
 * RETURN   : int : -1:file can't be opened.Other:current position
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-14 12:57
 *==========================================================================*/
 static int SearchCurrPos(IN int iSectorNo,
						  IN int iActualStoreRecordSize, 
						  IN int iMaxRecordsPerSector, 
					      OUT DWORD *pCurrentID,
						  IN FILE *fp)
{
	int		iCurrRecordPos;   //The position in current sector
	long	iOffset, iOffsetOld;
	int		iBottomPos, iTopPos, iMidPos;
	BOOL    bBotPosEmpty, bMidPosEmpty;
	void	*pRecordBuff;	

	//iRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;
	iOffsetOld = (iSectorNo - 1) * SIZE_PER_SECTOR;
	pRecordBuff = (void *)NEW(char, iActualStoreRecordSize);
	if(pRecordBuff == NULL)
	{
		return 0;
	}
	iBottomPos = 0;

	*pCurrentID = 0;    
	iTopPos = iMaxRecordsPerSector - 1;
	iCurrRecordPos = 0;
	bBotPosEmpty = FALSE;
	bMidPosEmpty = FALSE;
	
	do
	{
		iOffset = iOffsetOld + iBottomPos * iActualStoreRecordSize;
		rewind(fp);
		fseek(fp, iOffset, SEEK_SET);
		fread(pRecordBuff, (unsigned)iActualStoreRecordSize, 1, fp);
		bBotPosEmpty = RecordSpaceIsEmpty(iActualStoreRecordSize, pRecordBuff);

		if(bBotPosEmpty)
		{
			iCurrRecordPos = iBottomPos;
			break;
		}

		iMidPos = (iBottomPos + iTopPos)/2;
		iOffset = iOffsetOld + iMidPos * iActualStoreRecordSize;
		rewind(fp);
		fseek(fp, iOffset, SEEK_SET);
		fread(pRecordBuff, (unsigned)iActualStoreRecordSize, 1, fp);
		bMidPosEmpty = RecordSpaceIsEmpty(iActualStoreRecordSize, pRecordBuff);
		
		if(bMidPosEmpty)
		{
			iTopPos = iMidPos - 1;
		}
		else
		{
			iBottomPos = iMidPos + 1;
			*pCurrentID = *((DWORD *)pRecordBuff);
		}
	
	} while((iBottomPos <= iTopPos) && (iCurrRecordPos == 0));
	
	iCurrRecordPos = iBottomPos;

	if (pRecordBuff != NULL)
	{
		DELETE(pRecordBuff);
	}

	return iCurrRecordPos;
}
/*==========================================================================*
 * FUNCTION : DAT_ReadHisDataStatus
 * PURPOSE  : Read current different data's writing position in flash
 * CALLS    : SearchCurrPos
 * CALLED BY: DAT_IniMngHisData
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-14 11:48
 *==========================================================================*/
static BOOL DAT_ReadHisDataStatus(void)
{
	long ioffset;
	int i, j;
	int iRecords;
	DWORD dwCurrRecord;
	DWORD *pCurrRecordID = &dwCurrRecord;

	DWORD dwMaxRecordID;
	BYTE bySectorNo;
	int iRecordSize;
	int iAcutalRecordSize;
	int iMaxRecordsPerSector;

	SECTOR_MAP_TABLE * pSector;
	SECTOR_MAP_TABLE * pUsedSector;
	RECORD_SET_TRACK * pTrack;
	HISDATA_QUEUE    * pQueue;
	
	FILE * fp;

	fp = fopen(MTD_NAME, "rb");

	if (fp == NULL)
	{
		return FALSE;
	}

	//No any historical data store in Flash
	if (g_byMaxDataIndex == 0)   
	{
		fclose(fp);
		return TRUE;
	}

	
	//Start reading historical data status
	for (i = 0; i < g_byMaxDataIndex; i++)   //Every type of hisdata
	{
		dwMaxRecordID = 0;

		pTrack = &(g_RECORD_SET_TRACK[i]);
		pQueue = &(g_HISDATA_QUEUE[i]);

		pSector = pTrack->pbyPhysicsSectorNo;
		iRecordSize = pTrack->iRecordSize;

		iAcutalRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;

		//This is alarm data
		if (pQueue->bIsAlarm)
		{
			iAcutalRecordSize = iAcutalRecordSize + sizeof(DWORD) + CRC16_BYTES;
		}

		//Read current historical data's all sectors
		for (j = 0; j < pTrack->iTotalSectors; j++)
		{
			//The j Sector is empty
			if (DAT_SectorIsEmpty((int)(pSector + j)->byPhySectorNo))  
			{
				if (j == 0)
				{
					//Next Sector is empty too
					if(DAT_SectorIsEmpty((int)(pSector + j + 1)->byPhySectorNo)) 
					{	
						pTrack->byCurrSectorNo = 1;
					}
					else
					{
                        pTrack->byCurrSectorNo = pTrack->iTotalSectors;
					}					
				}
				else
				{   
					pTrack->byCurrSectorNo = j;					
				}
				break;				
			}
			else   //The j Sector isn't empty
			{
				ioffset = ((pSector + j)->byPhySectorNo - 1) * SIZE_PER_SECTOR;
				rewind(fp);
				fseek(fp, ioffset, SEEK_SET);					
				
				//First record iCurrRecordID of j sector				
				fread(pCurrRecordID, (unsigned)sizeof(DWORD), 1, fp);					
				
				if (*pCurrRecordID == 0xffffffff)
				{
					//It must be some errors
					DAT_StorageDeleteRecord(pTrack->DataName);
					break;
				}

				if( dwMaxRecordID < (*pCurrRecordID))
				{
					dwMaxRecordID = (*pCurrRecordID);					
				}
				else
				{
					pTrack->byCurrSectorNo = j;
					break;
				}				
			}

		}//End reading current historical data's all sectors
		
		//Current is the max sector number of current data
		if (j == pTrack->iTotalSectors)
		{
            pTrack->byCurrSectorNo = j;
		}

		//Search for current record position
		*pCurrRecordID = 0;
		pUsedSector = pTrack->pbyPhysicsSectorNo;
		bySectorNo = (pUsedSector + pTrack->byCurrSectorNo - 1)->byPhySectorNo;
				

		pTrack->iMaxRecordsPerSector = SIZE_PER_SECTOR/iAcutalRecordSize;

		iMaxRecordsPerSector = pTrack->iMaxRecordsPerSector;
		
		iRecords = SearchCurrPos((int)bySectorNo, iAcutalRecordSize, 
			iMaxRecordsPerSector, pCurrRecordID ,fp);

		//Update g_RECORD_SET_TRACK[i] relevant variables
		pTrack->iWritableRecords = iMaxRecordsPerSector - iRecords;

		//New ID should be added 1 based on old existed max recordID
		pTrack->dwCurrRecordID = (*pCurrRecordID) + 1;
		pTrack->iCurrWritePos = iRecords;	

		//Specially process pTrack->dwCurrRecordID prevent error 20050608
		if ((pTrack->dwCurrRecordID > 1) && 
			(pTrack->dwCurrRecordID <= (DWORD)(pTrack->iMaxRecordsPerSector)) &&
			(pTrack->iCurrWritePos == 0))
		{
			//It must be 1,otherwise the id must be wrong
            pTrack->dwCurrRecordID = 1;
		}

		//Set global variable
		pTrack->byNewSectorFlag = TRUE;		


	}//End reading all historical data status 

	fclose(fp);
 	return TRUE;
}



/*==========================================================================*
 * FUNCTION : WriteCurrentRecords
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int               iRecordsToWrite  : Record number to write
 *            int               iRecordSize      : Record size
 *            HISDATA_QUEUE *   pQueue           : 
 *            RECORD_SET_TRACK  *pTrack          : 
 *            FILE *            fp  
 *			  long				iWritePos: 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-19 09:05
 *==========================================================================*/
static BOOL WriteCurrentRecords(IN int iRecordsToWrite, 
						IN int iRecordSize, 
						IN HISDATA_QUEUE * pQueue, 
						IN RECORD_SET_TRACK *pTrack,
						IN long iWritePos)
{
	int iErrorWasteCounts, iErrorEmptyCounts, iWriteReturn, k;
	int iActualRecordSize;
	void * pData;
	BOOL bIsAlarm;
	int iAlarmAddedSize;
	long iPos;

	pData = NULL;	
	pData =(void *)((char *)pQueue->pHisData 
				+ (pQueue->iHead) * iRecordSize);

	bIsAlarm = pQueue->bIsAlarm;   //Alarm will be dealed with specially
	
	iActualRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;

	 //Endtime and iCheckNo2 space will be allocated in advance.
	iAlarmAddedSize = sizeof(DWORD) + CRC16_BYTES;	
	
	iErrorWasteCounts = 0;
	iErrorEmptyCounts = 0;
	iPos = iWritePos;
	for (k = 0; k < iRecordsToWrite; k++)
	{	
		//There is no enough space to write,exit recycle directly
		if (pTrack->iWritableRecords <= 0)
		{
			break;
		}

		iWriteReturn = WriteOneRecord(pData, iRecordSize, 
					pTrack->dwCurrRecordID, iPos);

		if(iWriteReturn == 0)  ////Succeed
		{	
			//Update g_HISDATA_QUEUE[i] and g_RECORD_SET_TRACK[i] variables
			pQueue->iHead = pQueue->iHead + 1;

			pQueue->iLength = pQueue->iLength - 1;

			if (pQueue->iLength == 0)
			{
				pQueue->iHead = 0;    //Recover to starting position
			}

			pTrack->iCurrWritePos = pTrack->iCurrWritePos + 1;

			pTrack->dwCurrRecordID = pTrack->dwCurrRecordID + 1;
			pTrack->iWritableRecords = pTrack->iWritableRecords - 1; 

			pData = (void *)((char *)pData + iRecordSize);	
			iErrorEmptyCounts = 0;					//Recover to 0;	
			iErrorWasteCounts = 0;
			
		}
		else if(iWriteReturn == 1 )   //Fail to write,Record space is empty
		{
			iErrorEmptyCounts++;
			iErrorWasteCounts = 0;

			if (iErrorEmptyCounts > 3)           //Try three record spaces
			{
				AppLogOut("DAT_WRI_RECS",APP_LOG_WARNING,
							"Record Space is empty,but can't write in,%s",
							"Please exit system,check flash.");//Warning
				return FALSE;    //This condition will not be permited;				
			}

			if (pTrack->iWritableRecords > 1)
			{
				iRecordsToWrite++;
				continue;
			}
       
		}				
		else   //iWriteReturn = 2 Fail to write,Record space is wasted
		{
			//Update g_HISDATA_QUEUE[i] and g_RECORD_SET_TRACK[i] variables
			pTrack->iCurrWritePos = pTrack->iCurrWritePos + 1;					
			pTrack->iWritableRecords = pTrack->iWritableRecords - 1;
			pTrack->dwCurrRecordID = pTrack->dwCurrRecordID + 1;

			iErrorWasteCounts++;
			iErrorEmptyCounts = 0;

			if (iErrorWasteCounts > 3)           //Try three record spaces
			{
				AppLogOut("DAT_WRI_RECS",APP_LOG_WARNING,
							"Record Space is wasted,can't write correctly,%s",
							"Please exit system,check flash.");//Warning
				return FALSE;    //This condition will not be permited;				
			}

			if (pTrack->iWritableRecords > 1)
			{
				//Allocate endtime and ichecknum's space of hisalarm
				if (pQueue->bIsAlarm)
				{
					iPos = iPos + iAlarmAddedSize;
				}	

				iPos = iPos + iActualRecordSize;
				iRecordsToWrite++;
				continue;
			}
		}	
		
		//Allocate endtime and ichecknum's space of hisalarm
		if (pQueue->bIsAlarm)
		{
			iPos = iPos + iAlarmAddedSize;
		}	

		iPos = iPos + iActualRecordSize;

	}//End for

#ifdef _DEBUG_DAT_MGMT_
		//printf("pTrack->iCurrWritePos is %d\n, pTrack->dwCurrRecordID is %d\n", 
			//pTrack->iCurrWritePos, pTrack->dwCurrRecordID);
#endif

	//Check Alarm Records, if Alarm records > iMaxRecords ,move active alarm  records in
	//next sector to erase
	//Delete following code ,it will be processed in DAT_WriteServiceThread
/*	if ((pQueue->bIsAlarm) && (pTrack->iCurrWritePos > pTrack->iMaxRecords) &&
		(!pTrack->byNewSectorFlag))
	{

#ifdef _DEBUG_DAT_MGMT_
		printf("pTrack->iCurrWritePos is %d, ACTIVE ALARM Alarm is MOVING\n", pTrack->iCurrWritePos);
#endif
		DAT_MoveAlarmRecord(pTrack);
		//Set current alarm sector can be erased
        pTrack->byNewSectorFlag = TRUE;		
	}	
*/
	
	return TRUE;	

}

/*==========================================================================*
 * FUNCTION : SearchAlarmIndex
 * PURPOSE  : Get alarm data position in hisdata_queue[i]
 * CALLS    : GetNextDataindex()
 * CALLED BY: in the module
 * RETURN   : int					 :   
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-18 19:23
 *==========================================================================*/

static int SearchAlarmIndex(void)
{
	int i;
	HISDATA_QUEUE * pQueue;
	for (i = 0; i < g_byMaxDataIndex; i++)
	{
		pQueue = &g_HISDATA_QUEUE[i];
		if (pQueue->bIsAlarm)      //Find the alarm
		{
			break;
		}

	}
	pQueue = NULL;
	return i;
}

/*==========================================================================*
 * FUNCTION : GetNextDataindex
 * PURPOSE  : Get next data position to write data record,
 *			: at the same time cycle counter(*iTrackPos);
 * CALLS    : 
 * CALLED BY: fnWriteDatathread()
 * ARGUMENTS: HISDATA_QUEUE * pQueue : 
 *            int * iWritePos		 : 
 *            int * iTrackPos        : 
 * RETURN   : int					 :   
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-18 19:23
 *==========================================================================*/
static int GetNextDataIndex(IN		HISDATA_QUEUE *pQueue, 
							OUT		int *iWritePos, 
							IN OUT	int *iTrackPos)
{
	int iAlarmPos;
	if (pQueue->bIsAlarm)						//Current data is alarm
	{
		(*iTrackPos)++;							//Cycle variable + 1;
		if ((*iTrackPos) >= g_byMaxDataIndex)
		{
			(*iTrackPos) = 0;					//recover to start position
		}
		(*iWritePos)= (*iTrackPos);				//Set i = j;
	}
	else										//Current data isnt Alarm
	{
		iAlarmPos = SearchAlarmIndex();			//find alarm position 
		if (iAlarmPos < g_byMaxDataIndex)			//No alarm data type so far
		{
			(*iWritePos) = iAlarmPos;          //Set i to alarm position
		}
		else
		{
			(*iTrackPos)++;							//Cycle variable + 1;
			if ((*iTrackPos) >= g_byMaxDataIndex)
			{
				(*iTrackPos) = 0;					//recover to start position
			}
			(*iWritePos)= (*iTrackPos);				//Set i = j;
		}
	}

	return (*iWritePos);
}

/*==========================================================================*
 * FUNCTION : DAT_fnWriteDataThread
 * PURPOSE  : Alarm data will be checked first after writing one type data
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-14 20:04
 *==========================================================================*/
 static void DAT_fnWriteDataThread(void)
{
   int i, j, iCounter;
   long iOffset;
   int bWriteResult;
   int iRecordsToWrite,iRecordSize;
   BOOL bExitFlag;

   SECTOR_MAP_TABLE  *pSector;
   HISDATA_QUEUE     *pQueue;
   RECORD_SET_TRACK  *pTrack;
   int iAcutalRecordSize;

   BYTE byCurrSector;
   HANDLE hCurrentThread;
   
   //for fixed the issue:WRT_REC_THD : PANIC in Creating and clearing historical and runtime data files in storage
   //wait for the finish of Init_CreateDataFiles
// Sleep(10000);
   
   hCurrentThread = RunThread_GetId(NULL);  
   RunThread_Heartbeat(hCurrentThread);

   if (g_pFile == NULL)
   {
      return;
   }

   i = 0;    //Used to track g_HISDATA_QUEUE[i]
   j = 0;     //Used to cycle from 0 -> g_byMaxDataIndex one by one
   iCounter = 0;
   bExitFlag = FALSE;
   
   while(!bExitFlag)
   {
      iCounter++;
      pQueue = &(g_HISDATA_QUEUE[i]);
      pTrack = &(g_RECORD_SET_TRACK[i]);

      //1.1 Some records to write to FLASH device 
      if (pQueue->iLength > 0 && pTrack->iWritableRecords > 0)  
      {
#ifdef _DEBUG_DAT_MGMT_
         if (iCounter%50 ==0)
         {
            printf("Come here pQueue->length is %d ,DataName is %s\n", 
               pQueue->iLength, pTrack->DataName);
         }
#endif
         pSector = pTrack->pbyPhysicsSectorNo;
         byCurrSector = pTrack->byCurrSectorNo;

         iAcutalRecordSize = pTrack->iRecordSize  + RECORD_ID_BYTES + CRC16_BYTES;

         //Alarm Record has different storage format
         if (pQueue->bIsAlarm)
         {
            iAcutalRecordSize = iAcutalRecordSize + sizeof(DWORD) + CRC16_BYTES;
         }

         iOffset = ((pSector + byCurrSector - 1)->byPhySectorNo - 1) * SIZE_PER_SECTOR
            + pTrack->iCurrWritePos * iAcutalRecordSize;      

         //Get the mutex lock      
         Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);
         Mutex_Lock(g_hMutexQueue[i], DM_LOCK_WAIT_TIME);
         
         if (pQueue->iLength >= pTrack->iWritableRecords) //2.1
         {
             //Space isn't enough to write pQueue->iLength pieces
            iRecordsToWrite = pTrack->iWritableRecords;
            iRecordSize = pTrack->iRecordSize;
            
            if (pTrack->bSectorEffect)
            {
               bWriteResult = WriteCurrentRecords(iRecordsToWrite, 
                                   iRecordSize, 
                                   pQueue, 
                                         pTrack,
                                   iOffset);
               if (!bWriteResult)
               {
                  AppLogOut("DAT_WRI_THD", APP_LOG_ERROR, 
                    "Fail to write %s in %d Sector record position is %d!"
                    "Following records will be lost.", 
                    pTrack->DataName, (iOffset/SIZE_PER_SECTOR + 1), 
                    pTrack->iCurrWritePos);

                  pTrack->bSectorEffect = FALSE; //Next time will not write again 
                  g_iFlashRunningState  = ERR_DAT_CANT_WRITE_FLASH;
               } 
            }
            else       //Current data will be lost for sector error
            {
               pQueue->iLength = 0;
               pQueue->iHead   = 0;
            }                 
         }
         else                               //2.2
         {
            iRecordsToWrite = pQueue->iLength;
            iRecordSize = pTrack->iRecordSize;
            
            if (pTrack->bSectorEffect)
            {
               bWriteResult = WriteCurrentRecords(iRecordsToWrite,           
                                      iRecordSize, 
                                      pQueue, 
                                      pTrack,
                                      iOffset);
               if (!bWriteResult)
               {
                  AppLogOut("DAT_WRI_THD", APP_LOG_ERROR, 
                    "Fail to write %s in %d Sector record position is %d!"
                    "Following records will be lost.", 
                    pTrack->DataName, (iOffset/SIZE_PER_SECTOR + 1), 
                    pTrack->iCurrWritePos);

                  pTrack->bSectorEffect = FALSE; //Next time will not write again 
                  g_iFlashRunningState  = ERR_DAT_CANT_WRITE_FLASH;
               }
            }
            else       //Current data will be lost for sector error
            {
               pQueue->iLength = 0;
               pQueue->iHead   = 0;
            }           
            
         }
         Mutex_Unlock(g_hMutexQueue[i]);
         Mutex_Unlock(g_hMutexWriteRecTrack);
#ifdef _DEBUG_DAT_MGMT_
         if (!bWriteResult)
         {
            printf("bWriteResult is %d ,DataName is %s\n", bWriteResult, pTrack->DataName);
         }     
#endif
      } //1.2 End writing records     

      //Free mutex lock OLD position is error
      
      Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);
      Mutex_Lock(g_hMutexQueue[i], DM_LOCK_WAIT_TIME);
      //Erase sectors
      if ((pTrack->iWritableRecords == 0) && 
         (strcmp(pTrack->DataName, EMERSON) != 0))
      {
         //Mutex_Lock(g_hMutexCreateHandle, DM_LOCK_WAIT_TIME);
         //printf("Index %d\n", i);
         //Sleep(1000);
		//printf("--------data %d \n",i);
         if (!(DAT_GetStorageSpace(i)))
         {           
            AppLogOut("DAT_GET_SPACE", APP_LOG_ERROR, 
               "No good sector to storage historical data",
            "Write data thread will exit!");
            g_iFlashRunningState  = ERR_DAT_CANT_ERASE_FLASH;
         }
         //Mutex_Unlock(g_hMutexCreateHandle);
         //Set global variable, show it is new sector
         pTrack->byNewSectorFlag = TRUE;
      }

      //Free mutex lock
      Mutex_Unlock(g_hMutexQueue[i]);
      Mutex_Unlock(g_hMutexWriteRecTrack);

      i = GetNextDataIndex(pQueue, &i, &j);   //Get next data position to write record

      Sleep(10);
      if (iCounter%200 == 0)
      {
#ifdef _DEBUG_DAT_MGMT_
            //printf("fnWriteDataThread is living.\n");
#endif
         RunThread_Heartbeat(hCurrentThread);
         iCounter = 0;
      }

      if (!THREAD_IS_RUNNING(hCurrentThread))
      {
         bExitFlag = TRUE;
      }

   }   //End while
}


/*==========================================================================*
 * FUNCTION : DAT_RegisterNotifiy
 * PURPOSE  : Register erasing sector pre-process function and parameter
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE                 hCurrData 
 *										: In fact it's &g_HISDATA_QUEUE[i]
 *            ERASE_SECTOR_PRE_PROC  pfn_PreProcess : 
 *            void                   *pParams       : 
 * RETURN   :  BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-30 12:38
 *==========================================================================*/
 BOOL DAT_RegisterNotifiy(IN HANDLE hCurrData, 
						  IN ERASE_SECTOR_PRE_PROC pfn_PreProcess, 
						  IN void *pParams)
  {
	  int iDataIndex;
	  HISDATA_QUEUE *pQueue;
	  RECORD_SET_TRACK *pTrack;

	  if ((hCurrData == NULL)
		  || (pfn_PreProcess == NULL))
	  {
		  return FALSE;
	  }

	  pQueue = (HISDATA_QUEUE *)hCurrData;

	  for (iDataIndex = 0; iDataIndex < MAX_DATA_TYPES; iDataIndex++)
	  {
		 pTrack = &(g_RECORD_SET_TRACK[iDataIndex]);
		 if (pTrack->iDataRecordIDIndex == pQueue->iDataRecordIDIndex)
		{
			break;
		}
	  }
	  //data error
	  if (iDataIndex == MAX_DATA_TYPES)
	  {
		  return FALSE;
	  }

	  //Get writing queque mutex lock
	  Mutex_Lock(g_hMutexQueue[iDataIndex], DM_LOCK_WAIT_TIME);

	  pTrack->hPreProcess = hCurrData;
	  pTrack->pfn_preProcess = pfn_PreProcess;
	  pTrack->Params = pParams;
		
	  //Free writing queque mutex lock
	  Mutex_Unlock(g_hMutexQueue[iDataIndex]);

	  return TRUE;
  }
/*==========================================================================*
 * FUNCTION : Pre_ProEraseSector
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *pTrack          : 
 *            int               iPhySecNoToErase : 
 * RETURN   :  void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-30 11:49
 *==========================================================================*/
 void Pre_ProEraseSector(IN RECORD_SET_TRACK *pTrack,
						 IN int iPhySecNoToErase)
 {
	 FILE *fp;
	 long iOffset;
	 void *pRecordBuff;
	 int iRec;
	 int iRlt;

	 fp = fopen(MTD_NAME, "rb");
	if (fp == NULL)
	{
		AppLogOut("DAT_PRE_ERAS", APP_LOG_ERROR, "Fail to open device.");
		return;
	}
	else
	{
		pRecordBuff = (void*)NEW(char, pTrack->iRecordSize);
		if(pRecordBuff == NULL)
		{
			fclose(fp);
			return;
		}
		iOffset = (iPhySecNoToErase - 1) * SIZE_PER_SECTOR;
		for (iRec = 0; iRec < pTrack->iMaxRecordsPerSector; iRec++)
		{
			memset(pRecordBuff, 0, (unsigned)(pTrack->iRecordSize));
			if (iRec%20 == 0)
			{
				RunThread_Heartbeat(g_hWriteServThread);
			}
			rewind(fp);
			fseek(fp, iOffset, SEEK_SET);
			if (ReadOneRecord(fp, iOffset, pRecordBuff, 
					pTrack->iRecordSize) == ERR_DAT_OK)
			{
				iRlt = pTrack->pfn_preProcess(pTrack->hPreProcess, 
							pRecordBuff, 1, pTrack->Params);

				if (iRlt == ERR_DAT_OK)
				{
					break;
				}									
			}
			iOffset = iOffset + pTrack->iRecordSize 
				+ CRC16_BYTES + RECORD_ID_BYTES;
		}

		DELETE(pRecordBuff);
		fclose(fp);					
	}
	return;
 }
 /*==========================================================================*
 * FUNCTION : DAT_PrepareSector
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIndex : 
 *			  RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-15 14:17
 *==========================================================================*/
 static BOOL DAT_PrepareSector(IN int iIndex)
{
	int iErrorCounts = 0;
	BYTE byCurLogiSecNo;
	int iCurrRecPos ,i, iWriteReturn;
	int iOnestatRecordSize;
	int iOneStatRecordAllSize;
	int iSectorTableSize;
	int iTotalSector;
	long iOffset;
	int iSectorIndex;

	int  iEraseFail;

	BYTE byLogiSecNoToErase;
	BYTE byPhySecNoToErase;

	void * pBuff;
	SECTOR_MAP_TABLE * pSector;
	SECTOR_MAP_TABLE   stSecMapTab;
	RECORD_SET_TRACK * pTrack;
	

	pTrack = &(g_RECORD_SET_TRACK[iIndex]);

	byCurLogiSecNo = pTrack->byCurrSectorNo;
	pSector = pTrack->pbyPhysicsSectorNo;	

#ifdef _DEBUG_DAT_MGMT_
	printf("Come here DAT_PrepareSector, current data is %s\n", pTrack->DataName);
#endif
	
	//1.Get physical sector index to erase
	if (byCurLogiSecNo == pTrack->iTotalSectors)
	{
		byLogiSecNoToErase = 1;
	}
	else
	{
		byLogiSecNoToErase = byCurLogiSecNo + 1;
	}

	///////////////////////////////////////////////////////////////////////////
	if (pSector == NULL)
	{
		return FALSE;
	}

	byPhySecNoToErase = (pSector + byLogiSecNoToErase - 1)->byPhySectorNo;
	
	//2.Execute callback function pre-process sector to be erased.
	if (pTrack->pfn_preProcess != NULL)
	{
		if (!DAT_SectorIsEmpty((int)byPhySecNoToErase))
		{
			Pre_ProEraseSector(pTrack,(int)byPhySecNoToErase);		
		}		
	}

	//3.Start erasing current sector
	iEraseFail = 0;
	RunThread_Heartbeat(g_hWriteServThread);
    do
	{
		//Only erase current one sector
		iEraseFail = EraseSector(byPhySecNoToErase, 1);
		iErrorCounts++;	
		
	} while((iEraseFail == 0) && (iErrorCounts < 3));	
	
	//4.Succeed to erase sector,update g_RECORD_SET_TRACK flag
	if(iErrorCounts < 3)    
	{
		return TRUE;
	}
	
	g_bSectorMngTable[byPhySecNoToErase] = TRUE;
	//5. After three times erase that sector,if erasing fails.
	//  New sector has to allocate to this historical data /	
	iErrorCounts = 0;
   	iEraseFail = 0;
	RunThread_Heartbeat(g_hWriteServThread);
    do
	{	
		//Before use it, it has to erase sector
		for (iSectorIndex = 3; iSectorIndex < MAX_HISDATA_SECTORS + 1; 
					iSectorIndex++)
		{
			if (g_bSectorMngTable[iSectorIndex] == FALSE)
			{
				pSector->byPhySectorNo = iSectorIndex;
				g_bSectorMngTable[iSectorIndex] = TRUE;
				break;
			}
		}

		if (iSectorIndex > MAX_HISDATA_SECTORS)
		{
			AppLogOut("DAT_PRE_SEC", APP_LOG_ERROR, 
				"Fail to find empty sector.");
			iErrorCounts = 3;
		}
		else
		{
			iEraseFail = EraseSector(iSectorIndex, 1);
			//Fail to erase,sector can't be used
			if (iEraseFail == 0)
			{
				g_bSectorMngTable[iSectorIndex] = TRUE;
			}
		}

		iErrorCounts++;	
		
	} while((iEraseFail == 0) && (iErrorCounts < 3));

	RunThread_Heartbeat(g_hWriteServThread);
	//If fail to erase current sector,return false
	if(iErrorCounts >= 3)	
	{
		return FALSE;
	}
	
	//6. After succeed to erase sector ,it has to be written to the tail 
	//  of current data's static sector /
	stSecMapTab.byLogSectorNo = byLogiSecNoToErase;
	stSecMapTab.byPhySectorNo = iSectorIndex;

	//7.1Get write static data's some necessary arguments
	iCurrRecPos = pTrack->iDataRecordIDIndex;
	iSectorTableSize = SECTOR_MAP_REC_LEN;

	iOnestatRecordSize = sizeof(STATIC_STORAGE_RECORD) 
		+ RECORD_ID_BYTES + CRC16_BYTES;

	iOneStatRecordAllSize = iOnestatRecordSize 
		+ MAX_HISDATA_SECTORS * iSectorTableSize;

	iTotalSector = pTrack->iTotalSectors;

	//7.2.Get offset in flash-writing starting address
	iOffset = iOneStatRecordAllSize * iCurrRecPos + iOnestatRecordSize 
		+ iTotalSector * iSectorTableSize + SIZE_VER_RELE_INFO; //Version info
	
	pBuff = (void *)NEW(char, iSectorTableSize);

	rewind(g_pFile);
	fseek(g_pFile, iOffset, SEEK_SET);
	//7.3.Find the free record space of sector map table
	for (i = 0; i < (MAX_HISDATA_SECTORS - iTotalSector); i++)
	{
		fread(pBuff, (unsigned)iSectorTableSize, 1, g_pFile);
		if(RecordSpaceIsEmpty(iSectorTableSize, pBuff))
		{
			//rewind(g_pFile);
			fseek(g_pFile, (-iSectorTableSize), SEEK_CUR);
			DELETE(pBuff);
			pBuff = NULL;
			break;
		}
	} 
	
	//Check it validity
	if ( i == (MAX_HISDATA_SECTORS - iTotalSector))
	{
		AppLogOut("DAT_GET_SPACE", APP_LOG_ERROR, 
			"There is no enough space to store sector map table!");
		DELETE(pBuff);
		return FALSE;
	}

	if (pBuff != NULL)
	{
		DELETE(pBuff);
	}

	RunThread_Heartbeat(g_hWriteServThread);
	//8.Current actually writing position
	iOffset = iOffset + i * iSectorTableSize;

	//Get mutex lock
	Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);

	iWriteReturn = WriteOneRecord(&stSecMapTab, sizeof(stSecMapTab), 
		(unsigned)(iTotalSector + i + 1), iOffset);

	//Writing result process
	if (iWriteReturn == 1)
	{
		////Write again
		iWriteReturn = WriteOneRecord(&stSecMapTab, sizeof(stSecMapTab), 
		(unsigned)(iTotalSector + i + 1), iOffset);
		if(iWriteReturn != 0)
		{
			Mutex_Unlock(g_hMutexWriteRecTrack);
			return FALSE;
		}
	}
	else if(iWriteReturn == 2)
	{
		iOffset = iOffset + iSectorTableSize;
		//Write again
		iWriteReturn = WriteOneRecord(&stSecMapTab, sizeof(stSecMapTab), 
		(unsigned)(iTotalSector + i + 1), iOffset);
		if(iWriteReturn != 0)
		{
			Mutex_Unlock(g_hMutexWriteRecTrack);
			return FALSE;
		}
	}

	RunThread_Heartbeat(g_hWriteServThread);
	//9.Succeed to update tracking information
	pSector = pTrack->pbyPhysicsSectorNo;
	(pSector + byLogiSecNoToErase - 1)->byPhySectorNo 
		= stSecMapTab.byPhySectorNo;
	//Update global variable
	//g_byCurrMaxSectorNo++;

	Mutex_Unlock(g_hMutexWriteRecTrack);

	return TRUE;

}
 /*==========================================================================*
 * FUNCTION : DAT_fnWriteServiceThread
 * PURPOSE  : This function will do erasing sector and move active alarm
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-12-8 14:08
 *==========================================================================*/
 static void DAT_fnWriteServiceThread(void)
 {

	int i, j, iCounter;
	HISDATA_QUEUE		*pQueue;
	RECORD_SET_TRACK	*pTrack;
	BOOL				bExitFlag;
	int	iEraseStdRecords;
	int iActualRecordSize;

	RunThread_Heartbeat(g_hWriteServThread);

	i = 0;    //Used to track g_HISDATA_QUEUE[i]
	j = 0;	  //Used to cycle from 0 -> g_byMaxDataIndex one by one
	iCounter = 0;
	bExitFlag = FALSE;

	while(!bExitFlag)
	{
		iCounter++;
		
		for (i = 0; i < g_byMaxDataIndex; i++)
		{
			pQueue = &(g_HISDATA_QUEUE[i]);
			pTrack = &(g_RECORD_SET_TRACK[i]);
 
			iEraseStdRecords = pTrack->iMaxRecords;

			if ((pTrack->iMaxRecords > pTrack->iMaxRecordsPerSector) ||
				(pTrack->iMaxRecords < 10))
			{
				iEraseStdRecords = pTrack->iMaxRecordsPerSector/2;

				if (pTrack->iMaxRecords < 10)
				{
					iActualRecordSize = pTrack->iRecordSize 
					 + RECORD_ID_BYTES + CRC16_BYTES;

					if ((iActualRecordSize * pTrack->iMaxRecords) 
						< (SIZE_PER_SECTOR/2))
					{
						iEraseStdRecords = pTrack->iMaxRecordsPerSector/2;
					}
					else
					{
						iEraseStdRecords = pTrack->iMaxRecords;
					}
				}
			}	
			

			if ((pTrack->byNewSectorFlag) && 
				(pTrack->iCurrWritePos > iEraseStdRecords))
			{
				RunThread_Heartbeat(g_hWriteServThread);
				//Move active alarm
				if (pQueue->bIsAlarm)
				{
					DAT_MoveAlarmRecord(pTrack);	
				}

				//Prepare Sector
				if (!DAT_PrepareSector(i))
				{
					AppLogOut("WRI_SERV_THD", APP_LOG_ERROR, 
						"Fail to erase sector.Please check flash.");
					g_iFlashRunningState  = ERR_DAT_CANT_ERASE_FLASH;					
				}

				//It will be set as TRUE in write data thread,when 
				//pTrack->iWritableRecords = 0;
				pTrack->byNewSectorFlag = FALSE;   
			}			
		}

		Sleep(100);
		//Clear watch dog per 3s
		if (iCounter == 20)
		{
#ifdef _DEBUG_DAT_MGMT_
				//printf("DAT_fnWriteServiceThread is living.\n");
#endif
			RunThread_Heartbeat(g_hWriteServThread);
			iCounter = 0;
		}

		if (!THREAD_IS_RUNNING(g_hWriteServThread))
		{
			bExitFlag = TRUE;
		}
	}
 }
/*==========================================================================*
 * FUNCTION : WriteOneRecord
 * PURPOSE  : Write a record to flash,
 *			: at the same time check the writing is correct or not;
 * CALLS    : 
 * CALLED BY: in the module
 * ARGUMENTS: void * pRecordBuff : 
 *            int    iRecordSize : Data record size not include dwRecordI and CheckNo
 *			  DWORD dwRecordID
 *            long iOffset          : 
 * RETURN   : BOOL : 0-Succeed  
 *				   : 1-Fail and but record space is free
 *				   : 2-Fail and record space is wasted.
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-18 19:23
 *==========================================================================*/
 static int WriteOneRecord(IN void *pRecordBuff, 
						   IN int iRecordSize, 
						   IN DWORD dwRecordID, 
						   IN long iOffset)
{
	int iActualRecordSize;
	unsigned short iCheckNo;
	int iResult;
	long iCurrPos;
	void * pWriteBuff;

	iActualRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;

	pWriteBuff = (void *)NEW(char, iActualRecordSize);    //start
	if(pWriteBuff == NULL)
	{
		return;
	}
	
	memmove(pWriteBuff, (const void *)&dwRecordID, RECORD_ID_BYTES);
	memmove((void *)((char *)pWriteBuff + RECORD_ID_BYTES),
		pRecordBuff, (unsigned)iRecordSize);

	//dwRecordID is checked too
	iCheckNo = CRC16((unsigned char *)pWriteBuff, 
		(unsigned short)(iRecordSize + RECORD_ID_BYTES));

	memmove((void*)((char *)pWriteBuff + iRecordSize +RECORD_ID_BYTES),
		(const void *)&iCheckNo, CRC16_BYTES);
	
	iCurrPos = iOffset;

	rewind(g_pFile);
	fseek(g_pFile, iCurrPos, SEEK_SET);	
    fwrite(pWriteBuff, (unsigned)iActualRecordSize, 1, g_pFile);		

   	//Read and check
	iResult = 0;
	
	//Set to origin position
	rewind(g_pFile);
	fseek(g_pFile, iCurrPos, SEEK_SET);
	iResult = ReadOneRecord(g_pFile, iCurrPos, pWriteBuff, iRecordSize);	

	if (iResult == 0) //succeed
	{
		DELETE(pWriteBuff);
		return 0;
	}
	else if (iResult == 1)  //Record is empty
	{
		DELETE(pWriteBuff);
		return 1;		
		
	}
	else   //iResult == 2
	{
		DELETE(pWriteBuff);		
		return 2;
	}

	
}

/*==========================================================================*
 * FUNCTION : DAT_GetStorageSpace
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iDataTrackIndex : 
 *			  RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-15 14:17
 *==========================================================================*/
 static BOOL DAT_GetStorageSpace(IN int iDataTrackIndex)
{
	BYTE byCurLogiSecNo;

	BYTE byLogiSecNoToGet;
	BYTE byPhySecNoToGet;

	SECTOR_MAP_TABLE * pSector;
	RECORD_SET_TRACK * pTrack;
	
	pTrack = &(g_RECORD_SET_TRACK[iDataTrackIndex]);
	byCurLogiSecNo = pTrack->byCurrSectorNo;
	pSector = pTrack->pbyPhysicsSectorNo;

	//Get new physical sector index
	if (byCurLogiSecNo == pTrack->iTotalSectors)
	{
		byLogiSecNoToGet = 1;
	}
	else
	{
		byLogiSecNoToGet = byCurLogiSecNo + 1;
	}

	byPhySecNoToGet = (pSector + byLogiSecNoToGet - 1)->byPhySectorNo;

	if(!DAT_SectorIsEmpty((int)byPhySecNoToGet))
	{
		//Ensure last time erased sector
		if (!EraseSector(byPhySecNoToGet, 1))
		{
			AppLogOut("DAT_GET_STOR", APP_LOG_ERROR, 
				"Can't erase physics secotr %d", byPhySecNoToGet);
			return FALSE;
		}
	}

	pTrack->byCurrSectorNo = byLogiSecNoToGet;
	pTrack->iCurrWritePos = 0;

	pTrack->iWritableRecords 
			= pTrack->iMaxRecordsPerSector;	
	pTrack->byNewSectorFlag = TRUE;

#ifdef _DEBUG_DAT_MGMT_
		//printf("Current Sector is %d\n", pTrack->byCurrSectorNo);
#endif	
	return TRUE;
	
}

/*==========================================================================*
 * FUNCTION : DAT_MoveAlarmRecord
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK *  pTrack : 
 *               : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-19 12:20
 *==========================================================================*/
static void DAT_MoveAlarmRecord(IN OUT RECORD_SET_TRACK *pTrack)
{
	int iLogicSector, iSectorToMove, iCurPhySector;
	SECTOR_MAP_TABLE * pSector;
	int i, iWriteReturn;
	int iEmptyCounts, iCounter;
	int iAlarmRecordSize,iActiveAlarmSize, iRecordSize;
	long  iOffset, iCurrSecOffset, iOfSetAlarm;
	int	iRecordsMoved;

	void * pBuff;
	DWORD dwTempData;
	unsigned short iCheckNo;

	RunThread_Heartbeat(g_hWriteServThread);

	dwTempData = 0;
	iRecordsMoved = 0;
	iCounter = 0;

	iRecordSize = pTrack->iRecordSize;
	iActiveAlarmSize = pTrack->iRecordSize + RECORD_ID_BYTES
		+ CRC16_BYTES;
	iAlarmRecordSize = iActiveAlarmSize + sizeof(DWORD) + CRC16_BYTES;

	iLogicSector = pTrack->byCurrSectorNo;
	pSector = pTrack->pbyPhysicsSectorNo;

	iCurPhySector = (pSector + iLogicSector - 1)->byPhySectorNo;

	if (iLogicSector == pTrack->iTotalSectors)
	{
		iLogicSector = 1;		 
	}
	else
	{
		iLogicSector = iLogicSector + 1;
	}
		 
	iSectorToMove = (pSector + iLogicSector - 1)->byPhySectorNo;

#ifdef _DEBUG_DAT_MGMT_	
	//printf("iSectorToMove is %d\n iCurPhySector is %d\n", 
		//iSectorToMove, iCurPhySector);
#endif

	iOffset = (iSectorToMove - 1) * SIZE_PER_SECTOR;
	pBuff = (void *)NEW(char, iAlarmRecordSize);
	if(pBuff == NULL)
	{
		return;
	}
	
	iEmptyCounts = 0;
    iOffset = iOffset - iAlarmRecordSize;
	for (i = 0; i < pTrack->iMaxRecordsPerSector; i++)
	{
		iCounter++;
		/* If active alarm are too many,only move
		   pTrack->iMaxRecordsPerSector - pTrack->iMaxRecords pieces.*/
		//Exit Condition 1:
		if (pTrack->iWritableRecords <= 0)
		{
			//DELETE(pBuff);
			break;
		}

		iOffset = iOffset + iAlarmRecordSize;
		rewind(g_pFile);
		fseek(g_pFile, iOffset, SEEK_SET);
		fread(pBuff, (unsigned)iAlarmRecordSize, 1, g_pFile);

		//Exit Condition 2:
		if (RecordSpaceIsEmpty(iAlarmRecordSize, pBuff))
		{
			//Read again,confirm it is empty
			rewind(g_pFile);
			fseek(g_pFile, iOffset, SEEK_SET);
			fread(pBuff, (unsigned)iAlarmRecordSize, 1, g_pFile);			
			if (RecordSpaceIsEmpty(iAlarmRecordSize, pBuff))
			{
				DELETE(pBuff);
				return;
			}			
		}

		//update
		iEmptyCounts = 0;

		//Read the endtime data
		dwTempData = *((DWORD*)((char *)pBuff + iActiveAlarmSize));
		iCheckNo = *(unsigned short *)((char *)pBuff + iActiveAlarmSize + sizeof(DWORD));

		//There is space to store active alarm
		if ((dwTempData == 0xffffffff) && iCheckNo == 0xffff)
		{
			//Get Mutex_Lock
			Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);

			iCurrSecOffset = (iCurPhySector - 1) * SIZE_PER_SECTOR
				+ pTrack->iCurrWritePos * iAlarmRecordSize;

			
			iWriteReturn = WriteOneRecord((void *)((char *)pBuff + RECORD_ID_BYTES),
				   pTrack->iRecordSize, pTrack->dwCurrRecordID, iCurrSecOffset);

			if(iWriteReturn == 0)  ////Succeed
			{	
				//Update g_HISDATA_QUEUE[i] and g_RECORD_SET_TRACK[i] variables			
				pTrack->iCurrWritePos = pTrack->iCurrWritePos + 1;

				pTrack->dwCurrRecordID = pTrack->dwCurrRecordID + 1;
				pTrack->iWritableRecords = pTrack->iWritableRecords - 1; 	

				//Set old position's active alarm as invalid record
				//Only write endtime and check no as 0
				dwTempData = 1;
                iCheckNo = 1;
				iOfSetAlarm = iOffset + iActiveAlarmSize;
				rewind(g_pFile);
				fseek(g_pFile, iOfSetAlarm, SEEK_SET);
				fwrite(&dwTempData, (unsigned)sizeof(DWORD), 1, g_pFile);
				fwrite(&iCheckNo, (unsigned)CRC16_BYTES, 1, g_pFile);

				//Ensure enough alarm records, so it can exit,because too many alarms
				//Maybe system is testing,exit should be OK.
				//Exit Condition 3:
				iRecordsMoved++;
				if (iRecordsMoved >= pTrack->iMaxRecords)
				{
					DELETE(pBuff);
					return;
				}
			
			}
			else if(iWriteReturn == 1 )   //Fail to write,Record space is empty
			{
				iWriteReturn = WriteOneRecord((void *)((char *)pBuff 
					+ RECORD_ID_BYTES), 
					iRecordSize, 
					pTrack->dwCurrRecordID, 
					iCurrSecOffset);       //Write again

				if(iWriteReturn == 1)
				{
					AppLogOut("DAT_MOV_ALM", APP_LOG_WARNING, 
						"Can't write alarm record to current"
						"sector.some hisalarm are lost."); 
					DELETE(pBuff);
					return;

					//Empty record won't be permitted.
				}
				else if (iWriteReturn == 0)
				{
					pTrack->iCurrWritePos = pTrack->iCurrWritePos + 1;
					pTrack->dwCurrRecordID = pTrack->dwCurrRecordID + 1;
					pTrack->iWritableRecords = pTrack->iWritableRecords - 1; 

					//end alarm
					dwTempData = 0;
					iCheckNo = 0;
					iOfSetAlarm = iOffset + iActiveAlarmSize;

					rewind(g_pFile);
					fseek(g_pFile, iOfSetAlarm, SEEK_SET);
					fwrite(&dwTempData, (unsigned)sizeof(DWORD), 1, g_pFile);
					fwrite(&iCheckNo, (unsigned)CRC16_BYTES, 1, g_pFile);
				}
       
			}

			//iWriteReturn = 2 Fail to write,Record space is wasted
			if (iWriteReturn == 2)   
			{
				//Update g_HISDATA_QUEUE[i] and g_RECORD_SET_TRACK[i] variables
				pTrack->dwCurrRecordID = pTrack->dwCurrRecordID + 1;
				pTrack->iCurrWritePos = pTrack->iCurrWritePos + 1;					
				pTrack->iWritableRecords = pTrack->iWritableRecords - 1; 			
				
				iCurrSecOffset = iCurrSecOffset + iAlarmRecordSize; 
				if (pTrack->iWritableRecords > 0)
				{
					iWriteReturn = WriteOneRecord((void *)((BYTE*)pBuff 
						+ RECORD_ID_BYTES), 
						iRecordSize,
						pTrack->dwCurrRecordID, 
						iCurrSecOffset);

					if (iWriteReturn != 1)
					{
						pTrack->dwCurrRecordID = pTrack->dwCurrRecordID + 1;
						pTrack->iCurrWritePos = pTrack->iCurrWritePos + 1;					
						pTrack->iWritableRecords = pTrack->iWritableRecords - 1;
					}
					else
					{
                        AppLogOut("DAT_MOV_ALM", APP_LOG_WARNING, 
						"Can't write alarm record to current"
						"sector.some hisalarm are lost.");
						DELETE(pBuff);
						return;
					}
				}
				
			}//End else iWriteReturn = 2.	

			Mutex_Unlock(g_hMutexWriteRecTrack);

			Sleep(10);
			if (iCounter%100 == 0)
			{
				RunThread_Heartbeat(g_hWriteServThread);
				iCounter = 0;
			}
            			
		}//End if(dwTempData == 0xffffffff)) Active record		
		
	} //End for

	DELETE(pBuff);
	return;
}


/*==========================================================================*
 * FUNCTION : DAT_SectorIsEmpty
 * PURPOSE  : Check sector
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iPhySectorNo : 
 * RETURN   : BOOL : TRUE:Current Sector has no data. 
 *					 FALSE:some data in current sector
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-18 17:29
 *==========================================================================*/
static BOOL DAT_SectorIsEmpty( int iPhySectorNo)
{
	FILE * fp;
	long iOffset;
	DWORD dwData;
	int i, iCounts;

	if(g_bIsEraseSector[iPhySectorNo] == TRUE)
	{
		printf("g_bIsEraseSector[%d]==TRUE\n",iPhySectorNo);
		return FALSE;
	}

	fp = fopen(MTD_NAME,"rb");
	if (fp == NULL)
	{
		return (-1);
	}

	iOffset = (iPhySectorNo - 1) * SIZE_PER_SECTOR;

	rewind(fp);
	fseek(fp, iOffset, SEEK_SET);
	iCounts = SIZE_PER_SECTOR/sizeof(DWORD);
	for (i = 0; i < iCounts; i++)
	{
		fread(&dwData, (unsigned)sizeof(DWORD), 1, fp);
		if (dwData != 0xffffffff)
		{
			fclose(fp);
			return FALSE;
		}
	}

	fclose(fp);
	return TRUE;

}


/*==========================================================================*
 * FUNCTION : RecordSpaceIsEmpy
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int     iRecordSize : 
 *            void *  pRecordBuff       : 
 * RETURN   : BOOL : TRUE:Current Record Space has no data. 
 *					 FALSE:some data in Record Space
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-18 17:52
 *==========================================================================*/
static BOOL RecordSpaceIsEmpty(int iRecordSize, void * pRecordBuff)
{
	BYTE byUnit;
	BYTE *pUnit;
	
	int i;
	pUnit = (BYTE*)pRecordBuff;
	
	for (i = 0; i < iRecordSize; i++)
	{
		byUnit = *(pUnit + i);
		if (byUnit != 0xff)
		{
			pUnit = NULL;
			return FALSE;
		}
	}

	pUnit = NULL;
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : EraseSector_ppc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iStartSectorNo : Start sector to erase
 *            int   iSectors        : Sector numbers to erase
 * RETURN   : int :					: SUCCEED:1  FAIL:0
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-01 10:22
 *==========================================================================*/
static int EraseSector_ppc(int iStartSectorNo, int iSectors)
{
	if( Mutex_Lock(g_hMutexForbidEraseSector, DM_LOCK_WAIT_TIME_15) != ERR_MUTEX_OK )
	{
		AppLogOut("ERASE DATA", APP_LOG_WARNING, 
			"Lock Failed iStartSectorNo%d iSectors%d\n", iStartSectorNo, iStartSectorNo);
	}
	int i;
	for(i = 0; i < iSectors; i++)
	{
		g_bIsEraseSector[iStartSectorNo + i] = TRUE;

		//printf(" set  g_bIsEraseSector[%d] = TRUE;\n",iStartSectorNo + i);
	}
	int Fd;
	int iStart;					//Start position
	int iCount;					
	int unlock;	
	int iEraseReturn;

	if (iSectors <= 0 || iStartSectorNo <= 0)
	{
		for(i = 0; i < iSectors; i++)
		{
			g_bIsEraseSector[iStartSectorNo + i] = FALSE;
		}
		Mutex_Unlock(g_hMutexForbidEraseSector);
		return 0;
	}
	
    Fd = open(MTD_NAME,O_RDWR);

	if (Fd == (-1))
	{
		for(i = 0; i < iSectors; i++)
		{
			g_bIsEraseSector[iStartSectorNo + i] = FALSE;
		}
		Mutex_Unlock(g_hMutexForbidEraseSector);
		return 0;
	}
    
	iStart = (iStartSectorNo - 1) * SIZE_PER_SECTOR;

	//if every block is 1024bites
	iCount = iSectors;      
	unlock = 0;       //if any block is unlocked.

	iEraseReturn = non_region_erase(Fd, iStart, iCount, unlock);

	if(iEraseReturn == 0)
	{
		for(i = 0; i < iSectors; i++)
		{
			g_bIsEraseSector[iStartSectorNo + i] = FALSE;
		}
		Mutex_Unlock(g_hMutexForbidEraseSector);
		close(Fd);
		return 1;
	}
	else
	{
		//Failed,try again
		iEraseReturn = non_region_erase(Fd, iStart, iCount, unlock);
		if(iEraseReturn != 0)
		{
			//Failed, try third time
			iEraseReturn = non_region_erase(Fd, iStart, iCount, unlock);
			if (iEraseReturn != 0)
			{
				for(i = 0; i < iSectors; i++)
				{
					g_bIsEraseSector[iStartSectorNo + i] = FALSE;
				}
				Mutex_Unlock(g_hMutexForbidEraseSector);
				close(Fd);
				return 0;
			}
		}
		for(i = 0; i < iSectors; i++)
		{
			g_bIsEraseSector[iStartSectorNo + i] = FALSE;
		}
		Mutex_Unlock(g_hMutexForbidEraseSector);
        close(Fd);
		return 1;
	}
	
}

/*==========================================================================*
 * FUNCTION : EraseSector_x86
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE  byStartSectorNo : Start sector to erase
 *            int   iSectors        : Sector numbers to erase
 * RETURN   : int :					: SUCCEED:1  FAIL:0
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-01 10:22
 *==========================================================================*/
static int EraseSector_x86(int iStartSectorNo, int iSectors)
{
	int iStart;					//Start position
	int i,iCount;					
	DWORD dwData;
    
	if (iSectors <= 0 || iStartSectorNo <= 0)
	{
		return 0;
	}
	
 	if (g_pFile == NULL)
	{
		return 0;
	}
    
	dwData = 0xffffffff;

	iStart = (iStartSectorNo - 1) * SIZE_PER_SECTOR;

	rewind(g_pFile);
	fseek(g_pFile, iStart, SEEK_SET);
	
	iCount = SIZE_PER_SECTOR/sizeof(DWORD);
	iCount = iCount * iSectors;

	for (i = 0; i < iCount; i++)
	{
		fwrite((void *)&dwData, sizeof(DWORD), 1, g_pFile);
	}

	i = 0;
	while (i < iSectors)
	{
		if (!DAT_SectorIsEmpty(iStartSectorNo + i))
		{
			return 0;
		}
		i++;
	}
	
    return 1;	
}

/*==========================================================================*
 * FUNCTION : DAT_ClearResource
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-15 20:05
 *==========================================================================*/
void DAT_ClearResource(void)
{
	int iDataTrackIndex, iExitResult;

	//Before stop thread,it should save all current records in queue
	if (g_hWriteThread != NULL)
	{
		iExitResult =  RunThread_Stop(g_hWriteThread, 1000, TRUE);
		g_hWriteThread = NULL;
	}
	
	//Before stop thread,it should save all current records in queue
	if (g_hWriteServThread != NULL)
	{
		iExitResult =  RunThread_Stop(g_hWriteServThread, 1000, TRUE);
		g_hWriteServThread = NULL;
	}

	//Free memory
	for (iDataTrackIndex = 0; iDataTrackIndex < MAX_DATA_TYPES; 
		iDataTrackIndex++)
	{
		if (g_RECORD_SET_TRACK[iDataTrackIndex].pbyPhysicsSectorNo != NULL)
		{
			DELETE(g_RECORD_SET_TRACK[iDataTrackIndex].pbyPhysicsSectorNo);
		}
		
		if ((g_HISDATA_QUEUE[iDataTrackIndex].pHisData) != NULL)
		{
			DELETE(g_HISDATA_QUEUE[iDataTrackIndex].pHisData);
		}
	}

	//Clear mutex lock
	if (g_hMutexWriteRecTrack != NULL)
	{
		//Only one writing lock
		Mutex_Destroy(g_hMutexWriteRecTrack);
	}
	  
	for (iDataTrackIndex = 0; iDataTrackIndex < MAX_DATA_TYPES; 
		iDataTrackIndex++)
	{
		if (g_hMutexQueue[iDataTrackIndex] != NULL)
		{
			 Mutex_Destroy(g_hMutexQueue[iDataTrackIndex]);
		}
       

		if (g_hMutexReadRecTrack[iDataTrackIndex] != NULL)
		{
			Mutex_Destroy(g_hMutexReadRecTrack[iDataTrackIndex]);	
		}			
	}

	if (g_hMutexCreateHandle != NULL)
	{
		//Only one create handle lock
		Mutex_Destroy(g_hMutexCreateHandle);
	}
	

	//Close global varible
	if (g_pFile != NULL)
	{
		fclose(g_pFile);
	}

	return;
}

/*==========================================================================*
 * FUNCTION : DAT_ReadStaticParameter
 * PURPOSE  : Read the data reading and writing's tracking parameter in Flash
 * CALLS    : EraseSector(),
 *			: RecordSpaceisEmpty(),
 * CALLED BY: IniMngHisData
 * RETURN   : int : 0:SUCCEED -1:FAIL
 * COMMENTS : 
 * CREATOR  : Li Xidong                DATE: 2004-09-07 14:22:10
 *--------------------------------------------------------------------------*/
static int DAT_ReadStaticParameter(void)
{
	BOOL bMainEmpty, bBakEmpty;
	//Set original value
	g_byMaxDataIndex = 0; 
	bMainEmpty = FALSE;
	bBakEmpty = FALSE;

	bMainEmpty = DAT_SectorIsEmpty(START_SECTOR);
	bBakEmpty = DAT_SectorIsEmpty(START_SECTOR + 1);

	//No any historical data store in static Sectors
	if (bMainEmpty && bBakEmpty)   
	{	
		return 0;
	}
		
	//Historical data device file isn't opened;	
	if (g_pFile == NULL)
	{
		AppLogOut("DAT_READ_STPARA", APP_LOG_WARNING, 
			"Hisdata file can't be opened,please check your flash device.");
		return (-1);
	}

	//Read main sector static parameters
	if(!bMainEmpty)
	{
		if (!(ReadStaticParas(START_SECTOR)))
		{
            //Try to read bak Sector
			ReadStaticParas(START_SECTOR + 1);
		}		
	}
	else
	{
		//Read data only by bak sector
		ReadStaticParas(START_SECTOR + 1);
	}

	//Succeed
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
//Following code will process create new same history file in flash, but new
//file's recordsize and max records are not the same as the old file
/*==========================================================================*
 * FUNCTION : UpdateHisdataStaticParas
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int    iIndex                : 
 *            IN int    iRecordSize           : 
 *            IN int    iMaxRecords           : 
 *            IN float  fWritefrequencyPerday : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-03-03 08:37
 *==========================================================================*/
static BOOL UpdateHisdataStaticParas (IN int	iIndex,  
									  IN int	iRecordSize,
									  IN int	iMaxRecords,
									  IN float	fWritefrequencyPerday)
{
	int iActualRecordSize;
	int iTotalSectors;
	int iCollbackNum, iOldPhySectorNo;
	int i, iSectorIndex, iNewSize;
	void * pTmpData;

	HISDATA_QUEUE		*pQueue;
	RECORD_SET_TRACK    *pTrack;
	SECTOR_MAP_TABLE	*pSector;
	SECTOR_MAP_TABLE	*pNewSector;
	SECTOR_MAP_TABLE	*pOldSector;

	//1.Calculate new file sectors
	iActualRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;
	iTotalSectors = CalculateSectors(iActualRecordSize, iMaxRecords,
		fWritefrequencyPerday); 

	//printf("iTotalSectors is %d\n", iTotalSectors);
	//2.Update
	pQueue = &g_HISDATA_QUEUE[iIndex];
	pTrack = &g_RECORD_SET_TRACK[iIndex];
	pTmpData = NULL;
	//2.1 Update g_HISDATA_QUEUE[iIndex]
	Mutex_Lock(g_hMutexQueue[iIndex], DM_LOCK_WAIT_TIME);

	pQueue->iRecordSize = iRecordSize;
	iNewSize = pQueue->iMaxSize * iRecordSize;

	pTmpData = (void *)NEW(char, iNewSize);

	//Succeed to NEW
	if (pTmpData != NULL)
	{
		DELETE(pQueue->pHisData);
		pQueue->pHisData = pTmpData;
	}
	else
	{
		AppLogOut("DAT_UPD_STATICE", APP_LOG_ERROR, 
			"Temp space isn't enough, fail to NEW pHisData!");
		Mutex_Unlock(g_hMutexQueue[iIndex]);
		return FALSE;
	}		
	Mutex_Unlock(g_hMutexQueue[iIndex]);

	//2.2 Update g_RECORD_SET_TRACK[iIndex]
	Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);
	pTrack->iRecordSize = iRecordSize;
	pTrack->iMaxRecords = iMaxRecords;
	pTrack->iMaxRecordsPerSector = SIZE_PER_SECTOR/iActualRecordSize;
	pTrack->iCurrWritePos = 0;
	pTrack->iWritableRecords = pTrack->iMaxRecordsPerSector;
	pTrack->byCurrSectorNo = 1;
	pTrack->dwCurrRecordID = 1;

	/*for(i = 0; i < 62; i++)
	{
	    printf("g_bSectorMngTable[%d] is %d\n", i, g_bSectorMngTable[i]);
	}*/

	if (pTrack->iTotalSectors != iTotalSectors)
	{
		pNewSector = NULL;
		pNewSector = NEW(SECTOR_MAP_TABLE, iTotalSectors);

		if (pNewSector == NULL)
		{
			AppLogOut("DAT_UPD_STATICE", APP_LOG_ERROR, 
				"Temp space isn't enough, fail to NEW SECTOR_MAP_TABLE!");
			Mutex_Unlock(g_hMutexWriteRecTrack);
			return FALSE;
		}	

		pOldSector = pTrack->pbyPhysicsSectorNo;
		pSector = pNewSector;
				
		for (i = 0; i < iTotalSectors; i++)
		{
			pSector->byLogSectorNo = i + 1;
			if ((pSector->byLogSectorNo) <= pTrack->iTotalSectors)
			{
				pSector->byPhySectorNo = pOldSector->byPhySectorNo;
				pOldSector++;
			}
			else
			{
				for (iSectorIndex = 3; iSectorIndex < MAX_HISDATA_SECTORS + 1; 
					iSectorIndex++)
				{
					if (g_bSectorMngTable[iSectorIndex] == FALSE)
					{
						pSector->byPhySectorNo = iSectorIndex;
						g_bSectorMngTable[iSectorIndex] = TRUE;
						break;
						//g_byCurrMaxSectorNo++;
					}
				}
				
				if (iSectorIndex > MAX_HISDATA_SECTORS)
				{
					AppLogOut("DAT_UPD_STATIC", APP_LOG_ERROR, 
						"Current history sector have been used up."
						"Fail to update new data file!");

					DELETE(pNewSector);
					Mutex_Unlock(g_hMutexWriteRecTrack);
					return FALSE;
				}				
			}//end else	
			pSector++;			
		}//end for

		//If iTotalSector < pTrack-iTotalSector will not be collected back	
		if (iTotalSectors < pTrack->iTotalSectors)
		{
			iCollbackNum = pTrack->iTotalSectors - iTotalSectors; 
			//Collect back
			while(iCollbackNum > 0)
			{
				iOldPhySectorNo = pOldSector->byPhySectorNo;
				if ((iOldPhySectorNo > 0) && 
					(iOldPhySectorNo < (MAX_HISDATA_SECTORS + 1)))
				{
					g_bSectorMngTable[iOldPhySectorNo] = FALSE;
				}

				pOldSector++;
				iCollbackNum--;
			}
		}

		DELETE(pTrack->pbyPhysicsSectorNo);
		pTrack->pbyPhysicsSectorNo = pNewSector;
	}	

	pTrack->iTotalSectors = iTotalSectors;   //The last step
	Mutex_Unlock(g_hMutexWriteRecTrack);
	//Sectors will be erased during writing new records	

//3. sector one
	EraseSector(START_SECTOR, 1);	
	Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);
	if (!WriteVerReleInfo(VER_WRITE_MAIN_SECTOR))
	{
		AppLogOut("DAT_UPD_STATIC", APP_LOG_ERROR, 
			"Can't write version information to main static sector.");
	}

	if (!ReWriteStaticParameter(START_SECTOR))
	{
		AppLogOut("DAT_UPD_STATIC", APP_LOG_ERROR, 
			"Fail to update static parameters to main static sector.");
		Mutex_Unlock(g_hMutexWriteRecTrack);
		return FALSE;
	}
	Mutex_Unlock(g_hMutexWriteRecTrack);

//4.sector one
	EraseSector((START_SECTOR + 1), 1);
	Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);
	if (!WriteVerReleInfo(VER_WRITE_BAK_SECTOR))
	{
		AppLogOut("DAT_UPD_STATIC", APP_LOG_ERROR, 
			"Can't write version information to bak static sector.");
	}

	if (!ReWriteStaticParameter(START_SECTOR + 1))
	{
		AppLogOut("DAT_UPD_STATIC", APP_LOG_ERROR, 
			"Fail to update static parameters to bak sector.");
	}
	Mutex_Unlock(g_hMutexWriteRecTrack);

	//5.End orperation
	return TRUE;
}
///////////////////////////////////////////////////////////////////////////////

/*==========================================================================*
 * FUNCTION : DAT_StorageCreate
 * PURPOSE  : Open one kind of data type storage device,if the data doest 
 *			: exist in FLASH.it will create in FLASH and return address to 
 *			: write data record
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: char		*pDataName            : Current data type name
 *            int		iRecordSize           : Record size per piece
 *            int		iMaxRecords           : Max records needed to store
 *            float		iWritefrequencyPerday : Max times to write per day
 *			  RASE_SECTOR_PRE_PROC pfn_preProcess
 *											  : Add erasing sector pre-process 
 * RETURN   : HANDLE :					  : handle to write data,in fact it's
 *											&(g_HISDATA_QUEUE[i])
 * COMMENTS : 
 * CREATOR  : Li Xidong                DATE: 2004-09-07 14:24:57
 *--------------------------------------------------------------------------*/
HANDLE DAT_StorageCreate (IN char	*pDataName, 
						  IN int	iRecordSize,
						  IN int	iMaxRecords, 
						  IN float fWritefrequencyPerday)
{
	int i, k, iSectorIndex;
	long ioffset;
	int	 iActualRecordSize;
	int  iOneRecAllSize;
	int  iOneRecForeSize;

	DWORD dwStaticRecordID;

	BYTE iTotalSectors;
	STATIC_STORAGE_RECORD tTmprecord;

	HISDATA_QUEUE		*pQueue;
	RECORD_SET_TRACK    *pTrack;
	SECTOR_MAP_TABLE	*pSector;

	if (DAT_GetFlashOpenState() != ERR_DAT_OK)
	{
		//return directly current records are lost.
		return &(g_HISDATA_QUEUE[0]);
	}

	//1.Check input arguments
	if(pDataName == NULL)
	{
		return NULL;
	}

	if (strlen(pDataName) > (LEN_DATA_NAME - 1))
	{
		AppLogOut("DAT_CREATE", APP_LOG_ERROR, 
			"File Name is too long, it's max length is %d.\n", 
			(LEN_DATA_NAME - 1));
		return NULL;
	}

	Mutex_Lock(g_hMutexCreateHandle, DM_LOCK_WAIT_TIME); 
	//2.Search 
	//printf("g_byMaxDataIndex is %d\n", g_byMaxDataIndex);
    if (g_byMaxDataIndex != 0)
	{
		//pTrack = g_RECORD_SET_TRACK;
		pTrack = &(g_RECORD_SET_TRACK[0]);

		for (i = 0;i < g_byMaxDataIndex; i++)
		{
			if (strcmp((pTrack + i)->DataName, pDataName) == 0)
			{
				if (((pTrack + i)->iRecordSize >= iRecordSize) &&
					((pTrack + i)->iMaxRecords >= iMaxRecords))
				{
					Mutex_Unlock(g_hMutexCreateHandle);
					return &(g_HISDATA_QUEUE[i]);
				}
				else
				{
					//User want to change old data record size and iMaxRecords
#ifdef _DEBUG_DAT_MGMT_
					printf("OLD pTrack->iRecordSize is %d, pTrack->iMaxRecords is %d.\n", 
						(pTrack + i)->iRecordSize,
						(pTrack + i)->iMaxRecords);
					printf("NEW iRecordSize is %d, iMaxRecords is %d.\n", 
						iRecordSize,
						iMaxRecords);
#endif
					AppLogOut("DAT_CREATE", APP_LOG_ERROR, 
						"A old %s existed, but record size or "
						"max records is different, it needs erase static sector.", 
						pDataName);
					///////////////////////////////////////////////////////////
					/*
					Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);
					EraseSector(START_SECTOR, 2);
					Mutex_Unlock(g_hMutexWriteRecTrack);

					AppLogOut("DAT_CREATE", APP_LOG_ERROR, 
						"System erase old history sector, restart system.\n");
					iResult = system("reboot");
					*/
					if (UpdateHisdataStaticParas (i, iRecordSize, iMaxRecords, 
						fWritefrequencyPerday))
					{
						AppLogOut("DAT_CREATE", APP_LOG_UNUSED, 
							"Succeed to update static sector parameters.");
					}
					else
					{
						(pTrack + i)->bSectorEffect = FALSE;
					}
					///////////////////////////////////////////////////////////
					Mutex_Unlock(g_hMutexCreateHandle);					
					return &(g_HISDATA_QUEUE[i]);
				}
				
			}	
		}
	}


	if(g_pFile == NULL)
	{
#ifdef _DEBUG_DAT_MGMT_
		printf("g_pFile is null, DAT_StorageCreate failed.\n");
#endif
		Mutex_Unlock(g_hMutexCreateHandle);
		return NULL;
	}

	printf("g_byMaxDataIndex = %d\n", g_byMaxDataIndex);
	if (g_byMaxDataIndex == 0)
	{
		
		if (!EraseSector(START_SECTOR, 2))
		{
			Mutex_Unlock(g_hMutexCreateHandle);
			return NULL;
		}
		else
		{				
			if (!WriteVerReleInfo(VER_WRITE_MAIN_AND_BAK))
			{
				AppLogOut("DAT_CREATE", APP_LOG_ERROR, 
					"Can't write version information after erase main and bak static sector.");
				Mutex_Unlock(g_hMutexCreateHandle);
				return NULL;
			}
		}		
	}

	
	pTrack = &(g_RECORD_SET_TRACK[g_byMaxDataIndex]);
	pQueue = &(g_HISDATA_QUEUE[g_byMaxDataIndex]);
    
	//Update global variable
	//g_byMaxDataIndex++;

	//strncpyz(pTrack->DataName, pDataName, (size_t)LEN_DATA_NAME);

	
	pTrack->iMaxRecords = iMaxRecords;
	pTrack->iRecordSize = iRecordSize;  
	pTrack->pfn_preProcess = NULL;

	iActualRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;

	if (strcmp(pDataName, HIS_ALARM_LOG) == 0)
	{
		//Add the endtime bytes and checkno2 bytes
		iActualRecordSize = iActualRecordSize + sizeof(DWORD) + CRC16_BYTES;
	}

	
	iTotalSectors = CalculateSectors(iActualRecordSize, iMaxRecords,
		fWritefrequencyPerday); 

	printf("iTotalSectors = %d\n",iTotalSectors);
	if((g_byCurrMaxSectorNo + iTotalSectors - 1) > MAX_HISDATA_SECTORS)
	{
		AppLogOut("DAT_CREAT", APP_LOG_WARNING, 
			"Fail to create,no enough Sectors !");

		//recover
		//g_byMaxDataIndex--;	//g_byMaxDataIndex++ has move to below.
        strcpy(pDataName, EMERSON);

		Mutex_Unlock(g_hMutexCreateHandle);
		return NULL;
	}

	
	pTrack->iTotalSectors = iTotalSectors;		
    
	pTrack->iMaxRecordsPerSector = SIZE_PER_SECTOR/iActualRecordSize;

	
	pTrack->iWritableRecords = SIZE_PER_SECTOR/iActualRecordSize;	

	pTrack->byCurrSectorNo = 1;       //Current Logic Sector index
	
	pTrack->iCurrWritePos = 0;
    pTrack->iCurrReadPos = 0;
	pTrack->dwCurrRecordID = 1;

	//Very important flag
	pTrack->byNewSectorFlag = TRUE;

	
	pTrack->pbyPhysicsSectorNo
		= NEW(SECTOR_MAP_TABLE, iTotalSectors); 
	//Update global variable
	//g_byMaxDataIndex++;
	


	if (pTrack->pbyPhysicsSectorNo == NULL)
	{
		AppLogOut("DAT_CREATE", APP_LOG_ERROR, 
				"No enough space to NEW SECTOR_MAP_TABLE.");

		FailtoCreateStatPara(pTrack, pQueue);
		Mutex_Unlock(g_hMutexCreateHandle);
		return NULL;
	}

	pSector = pTrack->pbyPhysicsSectorNo;
    
	
	for (k = 0; k < iTotalSectors; k++)
	{
		pSector->byLogSectorNo = k + 1;
		for (iSectorIndex = 3; iSectorIndex < MAX_HISDATA_SECTORS + 1; 
					iSectorIndex++)
		{
			if (g_bSectorMngTable[iSectorIndex] == FALSE)
			{
				pSector->byPhySectorNo = iSectorIndex;
				g_bSectorMngTable[iSectorIndex] = TRUE;
				break;
			}
		}

		if (iSectorIndex > MAX_HISDATA_SECTORS)
		{
			AppLogOut("DAT_CREATE", APP_LOG_ERROR, 
				"All sectors have been used up,fail to create.");

			FailtoCreateStatPara(pTrack, pQueue);
			Mutex_Unlock(g_hMutexCreateHandle);
			return NULL;
		}
		
		pSector++;
	}
	
	
	pSector = pTrack->pbyPhysicsSectorNo;
	if (!DAT_SectorIsEmpty(pSector->byPhySectorNo))
	{
		
		if (!EraseSector(pSector->byPhySectorNo, 1))
		{
			FailtoCreateStatPara(pTrack, pQueue);
			Mutex_Unlock(g_hMutexCreateHandle);
			return NULL;
		}
	}

	
	pQueue->iDataRecordIDIndex = 0;    //Set initial value
	pQueue->iHead = 0;
	pQueue->iLength = 0;
	pQueue->iRecordSize = iRecordSize;
	pQueue->iMaxSize = MAX_TEMP_RECORD_LENGTH;
    
	if (strcmp(pDataName, HIS_ALARM_LOG) == 0)
	{
		pQueue->bIsAlarm = TRUE;
	}
	else
	{
		pQueue->bIsAlarm = FALSE;
	}	

	
	pQueue->pHisData = 
		(void *)NEW(char, MAX_TEMP_RECORD_LENGTH * iRecordSize);
	
	if (pQueue->pHisData == NULL)
	{
		AppLogOut("DAT_CREATE", APP_LOG_ERROR, 
				"No enough space to NEW pHisData.");

		FailtoCreateStatPara(pTrack, pQueue);
		Mutex_Unlock(g_hMutexCreateHandle);
		return NULL;
	}
	//Update global variable
	g_byMaxDataIndex++;
	strncpyz(pTrack->DataName, pDataName, (size_t)LEN_DATA_NAME);
	
	memset(&tTmprecord, 0, sizeof(tTmprecord));	
	strncpyz(tTmprecord.DataName, pTrack->DataName, (size_t)LEN_DATA_NAME);

	
	tTmprecord.iMaxRecords = pTrack->iMaxRecords;
	tTmprecord.iRecordSize = pTrack->iRecordSize;
	tTmprecord.iTotalSectors = pTrack->iTotalSectors;
	//Get writing data mutex lock
	Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);

	
	iOneRecForeSize = sizeof(STATIC_STORAGE_RECORD) 
		+ RECORD_ID_BYTES + CRC16_BYTES;

	
	iOneRecAllSize = iOneRecForeSize + MAX_HISDATA_SECTORS * SECTOR_MAP_REC_LEN;

	
	if(WriteStaticParameter((void *)&tTmprecord, 
							sizeof(STATIC_STORAGE_RECORD), 
							&dwStaticRecordID, 0))
	{
		
		pTrack->iDataRecordIDIndex = dwStaticRecordID;
		pQueue->iDataRecordIDIndex = dwStaticRecordID;

		ioffset = (dwStaticRecordID - 1) * iOneRecAllSize 
			+ iOneRecForeSize + SIZE_VER_RELE_INFO;

		
		pSector = pTrack->pbyPhysicsSectorNo; 

		if(!WriteSectorMapTable(pSector, (int)(tTmprecord.iTotalSectors), ioffset))
		{
			AppLogOut("DAT_CREAT", APP_LOG_ERROR,
			"Write Sector Map Table to main sector is Failed.");

			FailtoCreateStatPara(pTrack, pQueue);
			Mutex_Unlock(g_hMutexWriteRecTrack);
			Mutex_Unlock(g_hMutexCreateHandle);
			return NULL;
		}
	} 
	else
	{
		AppLogOut("DAT_CREAT", APP_LOG_ERROR,
			"Write Static Parameter main sector is Failed.");

		FailtoCreateStatPara(pTrack, pQueue);
		Mutex_Unlock(g_hMutexWriteRecTrack);
		Mutex_Unlock(g_hMutexCreateHandle);
		return NULL;
	}

	
	pSector = (SECTOR_MAP_TABLE*)pTrack->pbyPhysicsSectorNo; //recover start position

	if(WriteStaticParameter((void *)&tTmprecord, sizeof(tTmprecord), &dwStaticRecordID, 1))
	{
		ioffset = SIZE_PER_SECTOR + 
			(dwStaticRecordID - 1) * iOneRecAllSize 
			+ iOneRecForeSize + SIZE_VER_RELE_INFO;

		pTrack->iDataRecordIDIndex = dwStaticRecordID;
		pQueue->iDataRecordIDIndex = dwStaticRecordID;

		//Write Sector Map Table to main Sector
		pSector = pTrack->pbyPhysicsSectorNo; //It must get the starting postion

		if(!WriteSectorMapTable(pSector, (int)(tTmprecord.iTotalSectors),
			ioffset))
		{
			AppLogOut("DAT_CREAT", APP_LOG_ERROR,
			"Write Sector Map table to bak sector is Failed.");

			FailtoCreateStatPara(pTrack, pQueue);
			Mutex_Unlock(g_hMutexWriteRecTrack);
			Mutex_Unlock(g_hMutexCreateHandle);
			return NULL;
		}
	} 
	else
	{
		AppLogOut("DAT_CREAT", APP_LOG_ERROR,
			"Write Static Parameter to bak sector is Failed.");

		FailtoCreateStatPara(pTrack, pQueue);
		Mutex_Unlock(g_hMutexWriteRecTrack);
		Mutex_Unlock(g_hMutexCreateHandle);
		return NULL;
	}
	
	
	Mutex_Unlock(g_hMutexWriteRecTrack);
	Mutex_Unlock(g_hMutexCreateHandle);

	AppLogOut("DAT_CREAT", APP_LOG_INFO, 
		"This %s file has been created in Flash successfully!", pDataName);
	

	//eturn &(g_HISDATA_QUEUE[g_byMaxDataIndex - 1]);
	return pQueue;
}


/*==========================================================================*
 * FUNCTION : BOOL ReWriteStaticParameter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  iStaticSector : 
 * RETURN   :  static : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-03-02 20:14
 *==========================================================================*/
static BOOL ReWriteStaticParameter(IN int iStaticSector)
{
	int i, iWriteReturn = 0;
	int iRecordSize, iActualRecordSize;

	int iMaxStaticRecords;
	int iOneRecordSize;
	DWORD dwCurrentID;
	long iPos, ioffset;
	STATIC_STORAGE_RECORD stStaticRecord;
	RECORD_SET_TRACK *pTrack;
	SECTOR_MAP_TABLE *pSector;

	memset(&stStaticRecord, 0, (unsigned)sizeof(stStaticRecord));

	iRecordSize = sizeof(STATIC_STORAGE_RECORD);
	iActualRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;
	iOneRecordSize = iActualRecordSize + MAX_HISDATA_SECTORS 
		* (sizeof(SECTOR_MAP_TABLE) 
		+ RECORD_ID_BYTES + CRC16_BYTES);

	iMaxStaticRecords = SIZE_PER_SECTOR/iOneRecordSize - 1;//SIZE_VER_RELE_INFO
	//pBuff = (void *)NEW(char, iOneRecordSize);
	
	iPos = (iStaticSector - 1) * SIZE_PER_SECTOR + SIZE_VER_RELE_INFO;

	dwCurrentID = 0;

	for (i = 0; i < g_byMaxDataIndex; i++)
	{
		pTrack = &g_RECORD_SET_TRACK[i];
		memset(&stStaticRecord, 0, (unsigned)sizeof(stStaticRecord));
		
		strncpyz(stStaticRecord.DataName, pTrack->DataName, (size_t)LEN_DATA_NAME);
		stStaticRecord.iMaxRecords = pTrack->iMaxRecords;
		stStaticRecord.iRecordSize = pTrack->iRecordSize;
		stStaticRecord.iTotalSectors = pTrack->iTotalSectors;

		dwCurrentID++;			
		
		iWriteReturn = 
			WriteOneRecord((void *)&stStaticRecord, 
			iRecordSize, dwCurrentID, iPos);

		if(iWriteReturn == 1 )   //1Fail to write,Record space is empty
		{
			iWriteReturn = WriteOneRecord((void *)&stStaticRecord, 
				iRecordSize, dwCurrentID, iPos); //write again

			if(iWriteReturn == 1)
			{
				AppLogOut("DAT_WRI_STPARA", APP_LOG_WARNING, 
						"Current Sector is free,but can't write in",
						"Please check flash");//Warning
				return FALSE;	
			}	
		}
		//:2:Fail to write,Record space is wasted ,write to next record space
		if (iWriteReturn == 2)
		{
			//Record number + 1
			dwCurrentID++;

			//Move forward by 64*sizeof(SECTOR_MAP_TABLE)
			iPos = iPos + iOneRecordSize;
			
			//Find current position and set it
			iWriteReturn = WriteOneRecord((void *)&stStaticRecord,
				iRecordSize, dwCurrentID, iPos); //write again

			if(iWriteReturn != 0)
			{
				AppLogOut("DAT_WRI_STPARA", APP_LOG_WARNING, 
						"Fail to write again,please check flash");//Warning
				return FALSE;	
			}
		}
		
		//Write Sector Map Table to Sector
		pSector = pTrack->pbyPhysicsSectorNo; 
		ioffset = iPos + iActualRecordSize;
		if(!WriteSectorMapTable(pSector, pTrack->iTotalSectors, ioffset))
		{
			AppLogOut("DAT_REW_STATIC", APP_LOG_ERROR,
			"Write Sector Map Table to sector is Failed.");
			return FALSE;
		}

		iPos = iPos + iOneRecordSize;

	}//end for

	return TRUE;	
}


/*==========================================================================*
 * FUNCTION : WriteStaticParameter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void *  pRecordBuff       : 
 *            int     iRecordSize :            
 *			: DWORD * dwStaticRecordID   OUT
 *			  int     iMainOrBak        : 0: main sector 1:bak sector
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-19 18:04
 *==========================================================================*/
 static BOOL WriteStaticParameter(IN void * pRecordBuff, 
								  IN int iRecordSize, 
								  IN DWORD * dwStaticRecordID,
								  IN int iMainOrBak)
{
	int i, iWriteReturn = 0;
	int iActualRecordSize;
	void * pBuff;
	int iMaxStaticRecords;
	int iOneRecordSize;
	DWORD dwCurrentID;
	long iPos;

	iActualRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;
	iOneRecordSize = iActualRecordSize + MAX_HISDATA_SECTORS 
		* (sizeof(SECTOR_MAP_TABLE) 
		+ RECORD_ID_BYTES + CRC16_BYTES);

	iMaxStaticRecords = SIZE_PER_SECTOR/iOneRecordSize - 1;//SIZE_VER_RELE_INFO
	pBuff = (void *)NEW(char, iOneRecordSize);
	if(pBuff == NULL)
	{
		return;
	}
	
	iPos = iMainOrBak * SIZE_PER_SECTOR + SIZE_VER_RELE_INFO;

	rewind(g_pFile);
	fseek(g_pFile, iPos, SEEK_SET);

	dwCurrentID = 0;
	for (i = 0; i < iMaxStaticRecords; i++)  //Ensure space is enough
	{
		fread(pBuff, (unsigned)iOneRecordSize, 1, g_pFile);
        if(RecordSpaceIsEmpty(iOneRecordSize, pBuff))
		{
			dwCurrentID = i + 1;
			SAFELY_DELETE(pBuff);
			break;
		}
	}

	if (i == iMaxStaticRecords)
	{
		AppLogOut("DAT_WRI_STPARA", APP_LOG_WARNING, 
			"Fail to write,Static sector records is overflow!");

		SAFELY_DELETE(pBuff);
		return FALSE;
	}

	*dwStaticRecordID = dwCurrentID;

	iPos = iPos + (dwCurrentID - 1) * iOneRecordSize;

	iWriteReturn = WriteOneRecord(pRecordBuff, iRecordSize, dwCurrentID, iPos);
	if(iWriteReturn == 1 )   //1Fail to write,Record space is empty
	{
		iWriteReturn = WriteOneRecord(pRecordBuff, 
			iRecordSize, dwCurrentID, iPos); //write again

		if(iWriteReturn == 1)
		{
			SAFELY_DELETE(pBuff);
			AppLogOut("DAT_WRI_STPARA", APP_LOG_WARNING, 
						"Current Sector is free,but can't write in",
						"Please check flash");//Warning
			return FALSE;						
		}				
             
	}

	//:2:Fail to write,Record space is wasted ,write to next record space
	if (iWriteReturn == 2)
	{
		//Record number + 1
		dwCurrentID++;

		//Move forward by 64*sizeof(SECTOR_MAP_TABLE)
		iPos = (dwCurrentID - 1) * iOneRecordSize;;

		//Find current position and set it
   		iWriteReturn = WriteOneRecord(pRecordBuff, 
			iRecordSize, dwCurrentID, iPos); //write again

		if(iWriteReturn != 0)
		{
			AppLogOut("DAT_WRI_STPARA", APP_LOG_WARNING, 
						"Fail to write again,please check flash");//Warning
			return FALSE;						
		}		
	}
	
	SAFELY_DELETE(pBuff);
	return TRUE;	
}

/*==========================================================================*
 * FUNCTION : WriteSectorMapTable
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SECTOR_MAP_TABLE *  pBuff          : 
 *            int     iTotalSectors : 
 *            long    iOffset    :acutual position in sector to write 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-19 18:31
 *==========================================================================*/
static BOOL WriteSectorMapTable(IN SECTOR_MAP_TABLE * pBuff, 
								IN int  iTotalSectors, 
								IN long iOffset)
{
	int iWriteReturn = 0;
	SECTOR_MAP_TABLE * pSector; 
	int i, iActualRecordSize;
	int iOneRecordSize;
	long iPos;

	iPos = iOffset;
	pSector = pBuff;
	iActualRecordSize = SECTOR_MAP_REC_LEN;
	
	iOneRecordSize = iActualRecordSize;

	for (i = 0; i < iTotalSectors; i++)
	{
		//find the position to write every time
		iWriteReturn = WriteOneRecord((void*)pSector, 
			sizeof(SECTOR_MAP_TABLE), (DWORD)(i+1), iPos);

		//1Fail to write,Record space is empty
		if(iWriteReturn == 1 )   
		{
			iPos = iPos - iActualRecordSize;
			iWriteReturn = WriteOneRecord((void*)pSector, 
				sizeof(SECTOR_MAP_TABLE), (DWORD)(i+1), iPos); //write again
			if(iWriteReturn == 1)
			{				
				return FALSE;						
			}				
             
		}

		//:2:Fail to write,Record space is wasted ,write to next record space
		//This condition will be prohibited
		if (iWriteReturn == 2)
		{
			return FALSE;		
					
		}
		//Move forward
		pSector++;
		iPos = iPos + iActualRecordSize;

	}    

	return TRUE;

}

/*==========================================================================*
 * FUNCTION : CalculateSectors
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: DAT_StorageCreate
 * ARGUMENTS: int    iRecordSize           : 
 *            int    iMaxRecords           : 
 *            float  iWritefrequencyPerday : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Li Xidong                DATE: 2004-09-07 14:24:05
 *--------------------------------------------------------------------------*/
static int CalculateSectors(IN int iRecordSize,
							IN int iMaxRecords, 
							IN float fWritefrequencyPerday)
{
	float fMaxEraseTimes, fSizePerSector, fMinUsedYears;
	float fMinSectors, fActMinSectors;
	int	  iMinSectors, iActMinSectors;
	int   iSectors; 
   
	fMaxEraseTimes = MAX_ERASE_TIMES;    
    fSizePerSector = SIZE_PER_SECTOR;     
    fMinUsedYears = MIN_USED_YEARS;        

	//According to sector used life
	fMinSectors = (iRecordSize * fWritefrequencyPerday * 365/fSizePerSector ) 
		/ (fMaxEraseTimes / fMinUsedYears);
	iMinSectors = (int)fMinSectors;
	if(fabs(fMinSectors - iMinSectors) > (1e-6))
	{
		iMinSectors++;
	}
	
	//According to storage max records need
    fActMinSectors = iRecordSize * iMaxRecords/fSizePerSector;
	iActMinSectors = (int)fActMinSectors;
	if(fabs(fActMinSectors - iActMinSectors) > (1e-6))
	{
		iActMinSectors++;
	}
	
	iActMinSectors++;    
						
    
	iSectors = MAX(iMinSectors, iActMinSectors);
	printf("iMinSectors = %d, iActMinSectors = %d iSectors =%d\n",iMinSectors, iActMinSectors, iSectors);

    return iSectors;
}

/*==========================================================================*
 * FUNCTION : FailtoCreateStatPara
 * PURPOSE  : Clear resource
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK *  pTrack : 
 *            HISDATA_QUEUE *     pQueue : 
 *            FILE *              fp     : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-20 14:04
 *==========================================================================*/
static void FailtoCreateStatPara(IN RECORD_SET_TRACK *pTrack, 
								 IN HISDATA_QUEUE	 *pQueue)
{
	strcpy(pTrack->DataName, EMERSON);

	if (pTrack->pbyPhysicsSectorNo != NULL)
	{
		DELETE(pTrack->pbyPhysicsSectorNo);
	}

	if (pQueue->pHisData)
	{
		DELETE(pQueue->pHisData);
	}
	g_byMaxDataIndex--;            //Recover current total data numbers

	return;
}

/*==========================================================================*
 * FUNCTION : DAT_StorageOpen
 * PURPOSE  : Open a historical data records storage device
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: char  DataName[16] : Current data type name
 * RETURN   : HANDLE :			   Read records pointer, in fact  it's a address
 *                                 of g_RECORD_SET_TRACK[i]'s copy;
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:34
 *==========================================================================*/
HANDLE DAT_StorageOpen (IN char *pDataName)
{
	int i;
	RECORD_SET_TRACK * hReadHandle; 

	if (DAT_GetFlashOpenState() != ERR_DAT_OK)
	{
		//return directly current records are lost.
		return &g_RECORD_SET_TRACK[0];
	}

	for(i = 0; i< g_byMaxDataIndex; i++ )
	{
		if( strcmp(pDataName, g_RECORD_SET_TRACK[i].DataName) == 0)
		{
			hReadHandle = NEW(RECORD_SET_TRACK, 1);
			if(hReadHandle == NULL)
			{
				return NULL;
			}
			memmove((void*)hReadHandle,&g_RECORD_SET_TRACK[i],
				(unsigned)sizeof(RECORD_SET_TRACK));
			hReadHandle->iCurrReadPos = 1;      //First value
			hReadHandle->byOldCurrSectorNo = hReadHandle->byCurrSectorNo;
			hReadHandle->iStartRecordNo	   = THE_FIRST_READ;  //Express first read data

#ifdef _DEBUG_DAT_MGMT_1
			if (pDataName != NULL)
			{
				printf("Succeed to open the data %s\n", pDataName);
			}
#endif
			return hReadHandle;
		}
	}

	return (NULL);	    //Current Hisdata does;t exist
}

/*==========================================================================*
 * FUNCTION : DAT_StorageOpenW
 * PURPOSE  : Open a historical data records storage device,used to write record
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: char  DataName[16] : Current data type name
 * RETURN   : HANDLE :			   Read records pointer, in fact  it's a address
 *                                 of g_RECORD_SET_TRACK[i]'s copy;
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2005-01-31 09:34
 *==========================================================================*/
HANDLE DAT_StorageOpenW (IN char *pDataName)
{
	int i;

	for(i = 0; i< g_byMaxDataIndex; i++ )
	{
		if( strcmp(pDataName, g_RECORD_SET_TRACK[i].DataName) == 0)
		{
			return &(g_HISDATA_QUEUE[i]);			 
		}
	}

	return (NULL);	    //Current Hisdata does;t exist
}

/*==========================================================================*
 * FUNCTION : DAT_StorageWriteRecord
 * PURPOSE  : Write historical data record to Storage Device
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: HANDLE        hCurrData : Current Data type writing handle
 *            unsigned int  ibytes    : Bytes to write
 *            void          *pBuffer  : Data buffer to write
 * RETURN   : BOOL : 
 * COMMENTS : TRUE:succeed   FALSE:failed
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:44
 *==========================================================================*/
BOOL DAT_StorageWriteRecord (IN HANDLE hCurrData,
							 IN unsigned int ibytes, 
							 IN void * pBuffer)
{
	int		iRecords, iRecordSize, iNewSize;
	int		iDataTrackIndex, iDataRecordIDIndex;
	int		iQueLen, iQueHead, iNewLen, iOldmaxLen;

	HISDATA_QUEUE	 *pQueue;
	void			 *pRecordData;
	void			 *pTmpData;
#ifdef _DEBUG_DAT_MGMT_
	//printf("Come to DAT_StorageWriteRecord\n");
#endif
	//Temp storage pointer
	pTmpData = NULL;

	//It must be added mutex lock
	if(hCurrData == NULL)
	{
		AppLogOut("DAT_WRI_REC", APP_LOG_WARNING, 
			"g_RECORD_SET_TRACK is NULL\n");
	   return FALSE;
	}	

	if (DAT_GetFlashOpenState() != ERR_DAT_OK)
	{
		//return directly current records are lost.
		AppLogOut("DAT_WRI_REC", APP_LOG_WARNING, 
			"DAT_GetFlashOpenState return is not ERR_DAT_OK\n");
		return TRUE;
	}

	pQueue = (HISDATA_QUEUE *)hCurrData;	
	iDataRecordIDIndex = pQueue->iDataRecordIDIndex;

	//find the data track index
	for(iDataTrackIndex = 0; iDataTrackIndex < g_byMaxDataIndex; 
		iDataTrackIndex++)
	{
		pQueue = &g_HISDATA_QUEUE[iDataTrackIndex];
		if (pQueue->iDataRecordIDIndex == iDataRecordIDIndex)
		{
			break;
		}
	}

	//Can't fint current Data's hisdata_queue
	if (iDataTrackIndex == g_byMaxDataIndex)   
	{
#ifdef _DEBUG_DAT_MGMT_
		printf("iDataTrackIndex == g_byMaxDataIndex\n");
#endif
		AppLogOut("DAT_WRI_REC", APP_LOG_WARNING, 
			"iDataTrackIndex == g_byMaxDataIndex == %d\n",iDataTrackIndex);
		return FALSE;
	}

	//Data information format is wrong
	if(ibytes%(pQueue->iRecordSize) != 0)
	{
#ifdef _DEBUG_DAT_MGMT_
		printf("%s DATA record is ERROR SIZE:ibytes is %d,pQueue->iRecordSize is %d\n",
			g_RECORD_SET_TRACK[iDataTrackIndex].DataName, ibytes, pQueue->iRecordSize);
#endif
		AppLogOut("DAT_WRI_REC", APP_LOG_ERROR, 
			"%s DATA is ERROR SIZE:ibytes to write is %d, but iRecordSize is %d."
			"ibytes%%iRecordSize != 0\n",
			g_RECORD_SET_TRACK[iDataTrackIndex].DataName, ibytes, pQueue->iRecordSize);

		return FALSE;
	}

	iRecords = ibytes/(pQueue->iRecordSize);

	//Get the queue lock
    
	Mutex_Lock(g_hMutexQueue[iDataTrackIndex], DM_LOCK_WAIT_TIME);

	iQueLen = pQueue->iLength;
	iQueHead = pQueue->iHead;
	iOldmaxLen = pQueue->iMaxSize;
	iRecordSize = pQueue->iRecordSize;


	if( (iRecords + iQueLen + iQueHead) > iOldmaxLen)
	{
		iNewLen = iRecords + iQueLen + iQueHead + TEMP_ADD_LENGTH + 1001;
		iNewSize = iNewLen * iRecordSize;

		pTmpData = (void *)RENEW(char, pQueue->pHisData, iNewSize);

		//Succeed to RENEW
		if (pTmpData != NULL)
		{
			pQueue->pHisData = pTmpData;
			pTmpData = NULL;
		}
		else
		{
			AppLogOut("DAT_WRI_QUE", APP_LOG_WARNING, "Fail to write "
				"current records!Temp space isn't enough, fail to RENEW!");

			return FALSE;
		}		

		pQueue->iMaxSize = iNewLen;
#ifdef _DEBUG_DAT_MGMT_
		printf("Exlarge pQueue->iMaxSize is %d\n", pQueue->iMaxSize);
#endif
	}



	if (((iRecords + iQueLen + iQueHead) < TEMP_ADD_LENGTH) &&
		iOldmaxLen > TEMP_ADD_LENGTH && (iQueLen == 0))
	{

		pTmpData = (void *)RENEW(char, pQueue->pHisData, 
			(TEMP_ADD_LENGTH * iRecordSize));
		//pQueue->pHisData = realloc(pQueue->pHisData,
			//(unsigned)(TEMP_ADD_LENGTH * iRecordSize));

		//Succeed to renew
		if (pTmpData != NULL)
		{
			pQueue->pHisData = pTmpData;
			pQueue->iMaxSize = MAX_TEMP_RECORD_LENGTH;
		}
		//If fail to renew keep old queue length
#ifdef _DEBUG_DAT_MGMT_
		printf("Minus pQueue->iMaxSize is %d\n", pQueue->iMaxSize);
#endif

	}
	
	pRecordData = pQueue->pHisData;

	//Current record position in HISDATA_QUEUE
	pRecordData = (void *)((char *)pRecordData 
		+ (iQueHead + iQueLen) * iRecordSize);    
 
	memmove(pRecordData, pBuffer, (unsigned)ibytes);

	pQueue->iLength = pQueue->iLength + iRecords;

	//Free the mutex lock

	Mutex_Unlock(g_hMutexQueue[iDataTrackIndex]);

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : DAT_RecoverReadHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT HANDLE  *hHandle : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-20 11:53
 *==========================================================================*/
BOOL DAT_RecoverReadHandle(IN OUT HANDLE *hHandle)
{
	RECORD_SET_TRACK *pTrack;
	
	pTrack = (RECORD_SET_TRACK *)(*hHandle);
	if (pTrack == NULL)
	{
		return FALSE;
	}
	pTrack->iCurrReadPos	= 1;      //Set it to First value
	pTrack->byCurrSectorNo  = pTrack->byOldCurrSectorNo;
	pTrack->iStartRecordNo	= THE_FIRST_READ;  //Express first read data

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ChangeCurrSectorPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *pTrack         : 
 *            int               iOffsetToOldPos : + ,Fore sectors
 *												  - ,Back sectors
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-19 20:03
 *==========================================================================*/
static void ChangeCurrSectorPos(IN OUT RECORD_SET_TRACK *pTrack, 
								IN int iOffsetToOldPos)
{
	BYTE byCurrLogiSector;

	byCurrLogiSector = pTrack->byCurrSectorNo;

    if (iOffsetToOldPos > 0)
	{
		if ((byCurrLogiSector + iOffsetToOldPos) > pTrack->iTotalSectors)
		{
			pTrack->byCurrSectorNo = 
				(byCurrLogiSector + iOffsetToOldPos)%(pTrack->iTotalSectors);
		}
		else
		{
			pTrack->byCurrSectorNo = byCurrLogiSector + iOffsetToOldPos;
		}
	}
	else
	{
		if ((byCurrLogiSector + iOffsetToOldPos) > 0)
		{
			pTrack->byCurrSectorNo = byCurrLogiSector + iOffsetToOldPos;
		}
		else
		{
			pTrack->byCurrSectorNo = 
				pTrack->iTotalSectors + byCurrLogiSector + iOffsetToOldPos;
		}
	}

	return;
}
/*==========================================================================*
 * FUNCTION : GetCurrentReadPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *ppTrack    : 
 *            BOOL              bAscending : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-19 19:26
 *==========================================================================*/
static BOOL GetCurrentReadPos(IN OUT RECORD_SET_TRACK *pTrack,
							  IN int iStartRecordNo,
							  IN BOOL bAscending)
{
	int iPosToCurrSector;
	int iBackRecords;
	int iMaxRecords;
	
#ifdef _DEBUG_DAT_MGMT_
		printf("Enter to GetCurrentReadPos.\n");
#endif
	iPosToCurrSector = 0;
	iBackRecords	= 0;
	iMaxRecords		= 0;
	if (pTrack == NULL)
	{
		return FALSE;
	}

	//It want to read all records
	if (iStartRecordNo == -1)
	{
		iStartRecordNo = 1;
	}


	//1 first read record
	if (pTrack->iStartRecordNo == THE_FIRST_READ)
	{
#ifdef _DEBUG_DAT_MGMT_
		printf("pTrack->iStartRecordNo == THE_FIRST_READ\n");
#endif
		if (bAscending)
		{
			if (pTrack->dwCurrRecordID > (unsigned)(pTrack->iMaxRecords))
			{
				iMaxRecords = pTrack->iMaxRecords;
				//It's ok
				pTrack->iStartRecordNo = iStartRecordNo;				
			}
			else
			{				
				iMaxRecords = pTrack->dwCurrRecordID - 1;
				//It has been modified
				if ((iMaxRecords + iStartRecordNo) > pTrack->iMaxRecords)
				{
					pTrack->iStartRecordNo = iStartRecordNo;
				}
				else
				{
					pTrack->iStartRecordNo = 
						iStartRecordNo + (pTrack->iMaxRecords - iMaxRecords);
						//Modify iStartRecordNo
						iStartRecordNo = pTrack->iStartRecordNo;
				}
			}
		}
		else
		{
			if (pTrack->dwCurrRecordID > (unsigned)(pTrack->iMaxRecords))
			{
				iMaxRecords = pTrack->iMaxRecords;
				pTrack->iStartRecordNo = iStartRecordNo;				
			}
			else
			{
				iMaxRecords = pTrack->dwCurrRecordID - 1;
				if (iStartRecordNo > iMaxRecords)
				{
					//It's an error current only iMaxRecords
					return FALSE;
				}
				else
				{
					pTrack->iStartRecordNo = iStartRecordNo;
				}
			}	
		}
	}


	//2 bAscending

	if (bAscending)
	{
		iBackRecords = pTrack->iCurrWritePos - 
			pTrack->iMaxRecords + pTrack->iStartRecordNo - 1; //-1 express include
		                                                      //the pTrack->iStartRecordNo record

#ifdef _DEBUG_DAT_MGMT_
		printf("iBackRecords is %d, pTrack->iStartRecordNo is %d\n", 
			iBackRecords, pTrack->iStartRecordNo);
#endif
	}
	else
	{
		iBackRecords = pTrack->iCurrWritePos - pTrack->iStartRecordNo;
#ifdef _DEBUG_DAT_MGMT_
		printf("pTrack->iCurrWritePos is %d, pTrack->iStartRecordNo is %d\n", 
			pTrack->iCurrWritePos, pTrack->iStartRecordNo);
#endif
	}

	if (iBackRecords >=0)  //Actuall starting from current sector
	{
		//pTrack->iCurrSectorNo no change
		pTrack->iCurrReadPos = iBackRecords;

	}
	else				   //Actually starting from his-sector
	{
		iPosToCurrSector = 
			((iBackRecords + 1)/(pTrack->iMaxRecordsPerSector)) - 1;
		ChangeCurrSectorPos(pTrack, iPosToCurrSector);
		
		if (((iBackRecords)%(pTrack->iMaxRecordsPerSector)) == 0)
		{
			pTrack->iCurrReadPos = 0;
		}
		else
		{
			pTrack->iCurrReadPos =
				pTrack->iMaxRecordsPerSector
				+ ((iBackRecords)%(pTrack->iMaxRecordsPerSector));
		}
	}	

	//ChangeCurrSectorPos(pTrack, iPosToCurrSector);

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ReadRecordsControl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *pTrack               : 
 *            long              *iIniOffset              : 
 *            int               *iRecordsToRead       : 
 *            int               *iRecordsInCurrSector : 
 *            BOOL              bAscending            : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-20 11:58
 *==========================================================================*/
static int ReadRecordsControl( IN OUT RECORD_SET_TRACK *pTrack, 
							   OUT	  long *iIniOffset,
							   IN OUT int  *iRecordsToRead,
							   OUT	  int  *iRecordsInCurrSector,
							   IN	  BOOL bAscending)
{
	SECTOR_MAP_TABLE  *pSector;
	BYTE byCurrPhySector, byCurrLogiSector;
	int iRecords;
	int iPosToChange;
	int iRecordsReadable;
	int iMaxValidRecords;



	//1.Check input arguments is right or not
	iRecords = *iRecordsToRead;
	if (iRecords <= 0)
	{
		*iRecordsToRead = 0;
		return ERR_END_READING; // All records has been read out
	}
	
	//2.Check it should end reading operation or not
	iMaxValidRecords = GetMaxRecordsReadable(pTrack);
	if (((pTrack->iStartRecordNo > iMaxValidRecords) && (!bAscending))
			|| (pTrack->iStartRecordNo > pTrack->iMaxRecords))
	{

		*iRecordsToRead = 0;
		return ERR_END_READING;  //It should end reading
	}
	
	//3.It need to change sector to read other records
	if ((bAscending && 
		(pTrack->iCurrReadPos == pTrack->iMaxRecordsPerSector))
		|| (!bAscending && 
		(pTrack->iCurrReadPos < 0))) //change from <= 0))) to  < 0))) 20050404
	{

		iPosToChange = -1;
		if (bAscending)
		{
			iPosToChange = 1;
		}

		ChangeCurrSectorPos(pTrack, iPosToChange);

		//Get current read position
		if (bAscending)
		{
			pTrack->iCurrReadPos = 0;
		}
		else
		{
			pTrack->iCurrReadPos = pTrack->iMaxRecordsPerSector - 1;
		}
	}
	
	//4.Get new iOffset
	pSector = pTrack->pbyPhysicsSectorNo;
	byCurrLogiSector = pTrack->byCurrSectorNo;
	byCurrPhySector = (pSector + byCurrLogiSector - 1)->byPhySectorNo;

	if ((pSector == NULL) && 
		(byCurrLogiSector < 1) &&
		(byCurrPhySector < 3))
	{
#ifdef _DEBUG_DAT_MGMT_
		printf("(pSector == NULL) && (byCurrLogiSector < 1) &&"
			"(byCurrPhySector < 3)\n");
#endif
		return ERR_CTRL_INPUT;
	}
	

	//It need to add pTrack->CurrReadPos * ActualRecordsize
	*iIniOffset = (byCurrPhySector - 1) * SIZE_PER_SECTOR;

	//4.Calculate records to read in current sector
	if (bAscending)
	{
		//If current sector is full or if current sector is not full
		iRecordsReadable = pTrack->iMaxRecordsPerSector - pTrack->iCurrReadPos;	
		if (iRecordsReadable <= 0)
		{
			AppLogOut("DAT_READ_CTL", APP_LOG_UNUSED, "There is no records to read.");
			*iRecordsToRead = 0;
			return ERR_END_READING; // All records has been read out
		}

		if (iRecordsReadable >= (pTrack->iMaxRecords - pTrack->iStartRecordNo))
		{
			*iRecordsInCurrSector = 
				(pTrack->iMaxRecords - pTrack->iStartRecordNo) + 1;
			//+1 express include the pTrack->iStartRecordNo record
		}
		else
		{
			*iRecordsInCurrSector = iRecordsReadable;
		}
	}
	else
	{

		iRecordsReadable = pTrack->iCurrReadPos + 1;
		if (iRecordsReadable <= 0)
		{
			AppLogOut("DAT_READ_CTL", APP_LOG_UNUSED, "There is no records to read.");
			*iRecordsToRead = 0;
			return ERR_END_READING; // All records has been read out
		}

		if (iRecordsReadable >= (iMaxValidRecords - pTrack->iStartRecordNo))
		{
			*iRecordsInCurrSector = 
				(iMaxValidRecords - pTrack->iStartRecordNo) + 1;   
			//+1 express include the pTrack->iStartRecordNo record
		}
		else
		{
			*iRecordsInCurrSector = iRecordsReadable;
		}
	}

	return ERR_DAT_OK;
}

/*==========================================================================*
* FUNCTION : ReadOneRecord_New
* PURPOSE  : Read one record and check it effectively or not, by using lseek,read
* CALLS    : 
* CALLED BY: DAT_StorageReadRecord
*			: DAT_fnWriteDataThread
* ARGUMENTS: FILE    *fp
*			  long    iOffset            : 
*            void    *pRecordBuff       : Output record data
*            int     iRecordSize : It is the data size 
*									Not include RECORD_ID_BYTES,CRC16_BYTES
* RETURN   : int :   0: Succeed
*					  1: Record is empty;
*					  2: Fail to read
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2011-08-20 17:32
*==========================================================================*/
static int ReadOneRecord_New(IN int iFp,
			     IN  long iOffset, 
			     OUT void *pRecordBuff,
			     IN  int  iRecordSize)
{
	unsigned short iOldCheckNo;
	unsigned short iCalCheckNo;
	int iActualRecordSize;
	void * pRdBuff;
	long iCurrPos;

	iActualRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;
	//printf("iActualRecordSize is %d\n", iActualRecordSize);
	pRdBuff = (void *)NEW(char, iActualRecordSize);
	if(pRdBuff == NULL)
	{
		return;
	}

	iCurrPos = iOffset;	

	//rewind(fp);
	lseek(iFp, iCurrPos, SEEK_SET);	

	//fread(pRdBuff, (unsigned)iActualRecordSize, 1, fp);      //fp has been moved iRecordSize
	read(iFp, pRdBuff, (unsigned)iActualRecordSize);

	//Current record is empty
	if(RecordSpaceIsEmpty(iActualRecordSize, pRdBuff))
	{				
		DELETE(pRdBuff);
		return 1;
	}

	//Output record data
	memmove(pRecordBuff, (void *)((BYTE *)pRdBuff + RECORD_ID_BYTES), 
		(unsigned)iRecordSize);

	iOldCheckNo = *((unsigned short *)((char *)pRdBuff 
		+ (iActualRecordSize - CRC16_BYTES)));


	//Check number is produced include RECORD_ID_BYTES should be checked
	//It is same as writing record
	iCalCheckNo = CRC16((unsigned char *)(pRdBuff), 
		(unsigned short)(iActualRecordSize - CRC16_BYTES));

	if (iOldCheckNo != iCalCheckNo)       //Error
	{
		//rewind(fp);
		//fseek(fp, iCurrPos, SEEK_SET);
		lseek(iFp, iCurrPos, SEEK_SET);
		//fread(pRdBuff, (unsigned)iActualRecordSize, 1, fp);     //Read again
		read(iFp, pRdBuff, (unsigned)iActualRecordSize);
		iOldCheckNo = *((unsigned short*)((char *)pRdBuff 
			+ iActualRecordSize - CRC16_BYTES));

		iCalCheckNo = CRC16((unsigned char *)(pRdBuff), 
			(unsigned short)(iActualRecordSize - CRC16_BYTES));

		//Output record data
		memmove(pRecordBuff, (void*)((char *)pRdBuff + RECORD_ID_BYTES), 
			(unsigned)iRecordSize);

		if (iOldCheckNo != iCalCheckNo) //Error again
		{			
			DELETE(pRdBuff);
			return 2;
		}
	}

	DELETE(pRdBuff);
	return 0;    //Succeed
}
/*==========================================================================*
 * FUNCTION : ReadMoreRecords_New
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *pTrack      : 
 *            void              *pRecordBuff : 
 *            int               *iRecords    : 
 *            BOOL              bActiveAlarm : 
 *            BOOL              bAscending   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-20 13:46
 *==========================================================================*/
static BOOL ReadMoreRecords_New(IN  RECORD_SET_TRACK *pTrack,
								OUT		void *pRecordBuff, 
								IN OUT  int  *iRecords,
								IN		BOOL bActiveAlarm,
								IN		BOOL bAscending)
{
	FILE *fp;
	long iOffset;
	int  iFp = (-1);

	long iIniOffset;
	int  iRecordsToRead;
	int  iRecordsInCurrSector;
	
	int iRecordsEffect;
	int iEmptyRecords, iMaxValidRecords;
	int iReadReturn;
	int iResult, k;
	long iCurrPos;
	int iRecordSize, iAcAlarmAddSize, iActualRecordSize;
	void *pRecord;	


	//fp = fopen(MTD_NAME,"rb");
	//if (fp == NULL)
	//{
	//	return FALSE;
	//}
	iFp =  open(MTD_NAME, O_RDONLY);
	if (iFp == (-1))
	{
		return FALSE;
	}

	iRecordsEffect = 0;
	iEmptyRecords = 0;
	iReadReturn = 0;

	//1.Get record size and storage record size
	iResult = GetActualRecordSize(pTrack, 
						bActiveAlarm,
						&iRecordSize, 
						&iActualRecordSize);
	if (iResult == (-1))
	{
		//fclose(fp);
		close(iFp);
		return FALSE;
	}
	iAcAlarmAddSize = sizeof(DWORD) + CRC16_BYTES;	

	pRecord = (void *)NEW(char, iRecordSize);
	if(pRecord == NULL)
	{
		close(iFp);
		return FALSE;
	}
	
	iResult = 0;
	iRecordsToRead = *iRecords;
	iIniOffset = 0;


	while (iRecordsEffect < *iRecords)
	{	
#ifdef _DEBUG_DAT_MGMT_
		printf("Recycle iRecordsEffect that there are %d  records is %d.\n", 
						iRecordsEffect, *iRecords);
#endif
		//1.Get iRecordsToRead in current sectors
		iRecordsToRead = *iRecords - iRecordsEffect;
		iResult = ReadRecordsControl(pTrack, 
							   &iIniOffset,
							   &iRecordsToRead,
							   &iRecordsInCurrSector,   //iRecordsInCurrSector is dead length
							   bAscending);


		if (iResult == ERR_CTRL_INPUT)
		{
			DELETE(pRecord);
			//fclose(fp);
			close(iFp);
			return FALSE;
		}
		else if (iResult == ERR_END_READING)
		{
            break;
		}
		iOffset = iIniOffset + pTrack->iCurrReadPos * iActualRecordSize;

		iCurrPos = iOffset;
		iEmptyRecords = 0;
		for (k = 0; k < iRecordsInCurrSector; k++)
		{
			//Process alarm specially
			if (bActiveAlarm && (k != 0))
			{
				;//iCurrPos = iCurrPos + iAcAlarmAddSize;
			}

			iReadReturn = ReadOneRecord_New(iFp, iCurrPos, pRecord, iRecordSize);
			//records are empty
			if ((iReadReturn == 1) )
			{
				iEmptyRecords++;
				if(iEmptyRecords >= 3)    //continuous three records are empty
				{
#ifdef _DEBUG_DAT_MGMT_
					//printf("It found that there are %d empty records.\n", 
					//	iEmptyRecords);
#endif
					pTrack->iStartRecordNo = pTrack->iStartRecordNo + 
						(iRecordsInCurrSector - k - 1);
					if (bAscending)
					{
						pTrack->iCurrReadPos = 
							pTrack->iCurrReadPos + (iRecordsInCurrSector - k - 1);
					}
					else
					{
						pTrack->iCurrReadPos = 
							pTrack->iCurrReadPos - (iRecordsInCurrSector - k - 1);
					}

					break;
				}
			}

			if( iReadReturn == 0)  //Effective records
			{	
				iEmptyRecords = 0;
				memmove((void*)((char *)pRecordBuff + iRecordsEffect * iRecordSize) , 
					pRecord, 
					(unsigned)iRecordSize);

				iRecordsEffect++;
#ifdef _DEBUG_DAT_MGMT_
				//printf("iRecordsEffect are %d records.\n", iRecordsEffect);
#endif
			}

			if (iReadReturn == 2)  //Invalid record
			{
				iEmptyRecords = 0;
			}
			
			if (bAscending)
			{
				iCurrPos = iCurrPos + iActualRecordSize;
				pTrack->iCurrReadPos++;
			}
			else
			{
				iCurrPos = iCurrPos - iActualRecordSize;
				pTrack->iCurrReadPos--;
			}
			
			pTrack->iStartRecordNo++;

			if ((iRecordsToRead == 0) || 
				(iRecordsEffect >= *iRecords))
			{
				break; //End reading record
			}
		}//End for
		
		
        if ((iRecordsToRead == 0) || 
				(iRecordsEffect >= *iRecords))
		{
			break; //End reading record
		}

		iMaxValidRecords = GetMaxRecordsReadable(pTrack);
		if (((pTrack->iStartRecordNo >= iMaxValidRecords) && (!bAscending))
			|| (pTrack->iStartRecordNo >= pTrack->iMaxRecords))
		{
			break;  //It should end reading
		}
		
	} //End while
	
	*iRecords = iRecordsEffect;
	//fclose(fp);
	close(iFp);
    DELETE(pRecord);
	return TRUE;
}


/*================================================================================*
 * FUNCTION : DAT_StorageReadRecords
 * PURPOSE  : Read current historical data iRecords pieces from iStartRecordNo.
 *			  It's not include active alarm reading
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: HANDLE  *hCurrData      : Data read Handle
 *            int     *pStartRecordNo : Start record number, if bAscending=TRUE,
 *										(-iMaxRecords) as first record; if 
 *										bAscending=FALSE,the latest record as 
 *										first record.
 *										if *pStartRecordNo=0, it express to continue
 *										read following records (IN)
 *										else if *pStartRecordNo=(-1),it will read 
 *										current Max records. (IN)
 *										else if *pStartRecordNo=(-2),Reading records 
 *										ended,it has compelted records (OUT)
 *            int     *iRecords       : Records to read
 *            void    *pBuff          : Buffer, it will be alloced by caller
 *			  BOOL    bActiveAlarm    : 1: to read the active alarm
 *									    0: to read ended alarm and other data
 *            BOOL    bAscending      : TRUE:direction is from -iMaxRecords
 *									        postion to current writing position	
 *									  : FALSE:From latest record position to oldest
 *									   record position
 * RETURN   : BOOL :				   TRUE:Succeed	   FALSE:Failed
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:47
 *================================================================================*/
BOOL DAT_StorageReadRecords (IN		HANDLE	hCurrData, 
							 IN OUT	int		*piStartRecordNo, 
							 IN OUT	int		*piRecords, 
							 OUT	void	*pBuff,
							 IN		BOOL	bActiveAlarm,
							 IN		BOOL	bAscending)
{
	int	iDataTrackIndex; 
	int iRecordsEffect;        //Actually effective records
	int iMaxRecords;

	int iStartRecordNo;
	int iRecords;
	
	//BOOL bFirstRead = FALSE; // added temporarily for resolve the BUG.  by maofuhua, 2005-2-16

	RECORD_SET_TRACK * pFind;
	RECORD_SET_TRACK * pTrack;
	SECTOR_MAP_TABLE * pSector;

#ifdef _DEBUG_DAT_MGMT_
	//printf("Come to DAT_StorageReadRecords, "
	//	"*piStartRecordNo is %d, *piRecords is %d\n", 
	//	*piStartRecordNo, *piRecords);
#endif
	if(hCurrData == NULL)
	{
#ifdef _DEBUG_DAT_MGMT_
		//printf("fail to arguments check.\n");
#endif
		return FALSE;
	}

	if (DAT_GetFlashOpenState() != ERR_DAT_OK)
	{
		//return directly current records are lost.
		*piStartRecordNo = -2;                //End reading record
		*piRecords = 0;
		return  TRUE;                         //The last time
	}

	pFind = &g_RECORD_SET_TRACK[0];
	pTrack = (RECORD_SET_TRACK *)(hCurrData); 
	pSector = pTrack->pbyPhysicsSectorNo;

	iStartRecordNo = *piStartRecordNo;
	iRecords	   = *piRecords;

	if (iRecords > pTrack->iMaxRecords)
	{
		iRecords = pTrack->iMaxRecords;
	}
	
	//1.Check input arguments
	if (((iStartRecordNo < 0) && (iStartRecordNo != (-1)) 
			&& (iStartRecordNo != (-2)))
		|| ((iStartRecordNo == 0) && 
			(pTrack->iStartRecordNo == THE_FIRST_READ))			//Input error
		|| (iStartRecordNo > pTrack->iMaxRecords)				//Input error
		|| ((iStartRecordNo != (-1)) && (iRecords < 1)))			//Input error
		//|| ((iRecords) > pTrack->iMaxRecordsPerSector))			//Max records once
	{
#ifdef _DEBUG_DAT_MGMT_
		//printf("fail to arguments check.\n");
#endif
		return FALSE;
	}

#ifdef _DEBUG_DAT_MGMT_
	printf("Pass to arguments check.\n");
#endif

	if (iStartRecordNo == (-1))
	{
#ifdef _DEBUG_DAT_MGMT_
		//printf("User want to read all records.\n");
#endif
		iStartRecordNo = 1;
		if (iRecords >= pTrack->iMaxRecords)
		{
			iRecords = pTrack->iMaxRecords;
		}
	}
	else if (iStartRecordNo == 0)
	{
		//Continue to read other records
		iStartRecordNo = pTrack->iStartRecordNo;
	}

	//2.Control reading data scope
	iMaxRecords = GetMaxRecordsReadable(pTrack);
	/*
	if((pTrack->iStartRecordNo >= iMaxRecords))
	*/
	if(((pTrack->iMaxRecords - pTrack->iStartRecordNo) < 0) ||
		((pTrack->iStartRecordNo >= iMaxRecords) && !bAscending))
	{
		*piStartRecordNo = -2;                //End reading record
		*piRecords = 0;
		return  TRUE;                         //The last time 											  
	}//END reading records 
			

	//3.Get
	if((pTrack->iCurrReadPos == 1) && 
		(pTrack->iStartRecordNo == THE_FIRST_READ)) //The first time
	{
#ifdef _DEBUG_DAT_MGMT_
		printf("This is first read record.iMaxRecords is %d\n", iMaxRecords);
#endif		
		if (!GetCurrentReadPos(pTrack, iStartRecordNo, bAscending))
		{
			*piRecords = 0;
			return FALSE;
		}
#ifdef _DEBUG_DAT_MGMT_
		printf("Pass to GetCurrentReadPos check.\n");
#endif

		
	}	

	//4.Find 
	iDataTrackIndex = GetDataTrackIndex(pTrack);
	if (iDataTrackIndex == MAX_DATA_TYPES)
	{
		*piRecords = 0;
		return FALSE;
	}

#ifdef _DEBUG_DAT_MGMT_
	TRACE("Pass to GetDataTrackIndex check iDataTrackIndex is %d.\n", 
			iDataTrackIndex);
#endif

	iRecordsEffect = 0;	
	

	Mutex_Lock(g_hMutexReadRecTrack[iDataTrackIndex], DM_LOCK_WAIT_TIME);

#ifdef _DEBUG_DAT_MGMT_
	TRACE("Pass to Mutex_Lock iDataTrackIndex is %d.\n", 
			iDataTrackIndex);
#endif

	if (!ReadMoreRecords_New(pTrack, 
							pBuff, 
					        &iRecords,
					        bActiveAlarm,
					        bAscending))
	{
		Mutex_Unlock(g_hMutexReadRecTrack[iDataTrackIndex]);
        *piRecords = 0;
		return FALSE;
	}

	//Free mutex lock
	Mutex_Unlock(g_hMutexReadRecTrack[iDataTrackIndex]);

#ifdef _DEBUG_DAT_MGMT_
		TRACE("Finished to ReadMoreRecords_New iRecords is %d."
			"pTrack->iStartRecordNo is %d\n", 
			iRecords, pTrack->iStartRecordNo);
#endif
	

	//Return actual records have been read
    *piRecords = iRecords;   
	*piStartRecordNo = pTrack->iStartRecordNo;


    return TRUE;
}

/*==========================================================================*
 * FUNCTION : FindMoreRecords_New
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *pTrack      : 
 *            void              *pRecordBuff : 
 *            int               *iRecords    : 
 *            BOOL              bActiveAlarm : 
 *            BOOL              bAscending   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-20 13:46
 *==========================================================================*/
static BOOL FindMoreRecords_New(IN		RECORD_SET_TRACK *pTrack,
								IN		COMPARE_PROC	pfnCompare, 
								IN		void *pCondition,
								OUT		void *pRecordBuff, 
								IN OUT  int  *iRecords,
								IN		BOOL bActiveAlarm,
								IN		BOOL bAscending)
{
	FILE *fp;
	int  iFp = (-1);
	long iOffset;

	long iIniOffset;
	int  iRecordsToRead;
	int  iRecordsInCurrSector;
	
	int iRecordsEffect;
	int iEmptyRecords, iMaxValidRecords;
	int iReadReturn;
	int iResult, k;
	long iCurrPos;
	int iRecordSize, iAcAlarmAddSize, iActualRecordSize;
	void *pRecord;	


	//fp = fopen(MTD_NAME,"rb");
	//if (fp == NULL)
	//{
	//	return FALSE;
	//}
	iFp =  open(MTD_NAME, O_RDONLY);
	if (iFp == (-1))
	{
		return FALSE;
	}

	iRecordsEffect = 0;
	iEmptyRecords = 0;
	iReadReturn = 0;

	//1.Get record size and storage record size
	iResult = GetActualRecordSize(pTrack, 
						bActiveAlarm,
						&iRecordSize, 
						&iActualRecordSize);
	if (iResult == (-1))
	{
		//fclose(fp);
		close(iFp);
		return FALSE;
	}
	iAcAlarmAddSize = sizeof(DWORD) + CRC16_BYTES;	

	pRecord = (void *)NEW(char, iRecordSize);
	if(pRecord == NULL)
	{
		close(iFp);
		return FALSE;
	}
	
	iResult = 0;
	iRecordsToRead = *iRecords;
	iIniOffset = 0;


	while (iRecordsEffect < *iRecords)
	{	
		//1.Get iRecordsToRead in current sectors
		iRecordsToRead = *iRecords - iRecordsEffect;
		iResult = ReadRecordsControl(pTrack, 
							   &iIniOffset,
							   &iRecordsToRead,
							   &iRecordsInCurrSector,   //iRecordsInCurrSector is dead length
							   bAscending);


		if (iResult == ERR_CTRL_INPUT)
		{
			DELETE(pRecord);
		//	fclose(fp);
			close(iFp);
			return FALSE;
		}
		else if (iResult == ERR_END_READING)
		{
            break;
		}
		iOffset = iIniOffset + pTrack->iCurrReadPos * iActualRecordSize;

		iCurrPos = iOffset;
		iEmptyRecords = 0;
		for (k = 0; k < iRecordsInCurrSector; k++)
		{
			//Process alarm specially
			if (bActiveAlarm && (k != 0))
			{
				;//iCurrPos = iCurrPos + iAcAlarmAddSize;
			}

			iReadReturn = ReadOneRecord_New(iFp, iCurrPos, pRecord, iRecordSize);
			//records are empty
			if ((iReadReturn == 1) )
			{
				iEmptyRecords++;
				if(iEmptyRecords >= 3)    //continuous three records are empty
				{
#ifdef _DEBUG_DAT_MGMT_
		//printf("It found that there are %d empty records.\n", iEmptyRecords);
#endif
					pTrack->iStartRecordNo = pTrack->iStartRecordNo + 
						(iRecordsInCurrSector - k - 1);
					if (bAscending)
					{
						pTrack->iCurrReadPos = 
							pTrack->iCurrReadPos + (iRecordsInCurrSector - k - 1);
					}
					else
					{
						pTrack->iCurrReadPos = 
							pTrack->iCurrReadPos - (iRecordsInCurrSector - k - 1);
					}

					break;
				}
			}

			if((iReadReturn == 0) && 
				(pfnCompare(pRecord, pCondition) == 1))  //Effective records
			{	
				iEmptyRecords = 0;
				memmove((void*)((char *)pRecordBuff + iRecordsEffect * iRecordSize) , 
					pRecord, 
					(unsigned)iRecordSize);

				iRecordsEffect++;
#ifdef _DEBUG_DAT_MGMT_
				//printf("iRecordsEffect are %d records.\n", iRecordsEffect);
#endif
			}

			if (iReadReturn == 2)  //Invalid record
			{
				iEmptyRecords = 0;
			}
			
			if (bAscending)
			{
				iCurrPos = iCurrPos + iActualRecordSize;
				pTrack->iCurrReadPos++;
			}
			else
			{
				iCurrPos = iCurrPos - iActualRecordSize;
				pTrack->iCurrReadPos--;
			}
			
			pTrack->iStartRecordNo++;

			if ((iRecordsToRead == 0) || 
				(iRecordsEffect >= *iRecords))
			{
				break; //End reading record
			}
		}//End for
		
		
        if ((iRecordsToRead == 0) || 
				(iRecordsEffect >= *iRecords))
		{
			break; //End reading record
		}

		iMaxValidRecords = GetMaxRecordsReadable(pTrack);
		if (((pTrack->iStartRecordNo >= iMaxValidRecords) && (!bAscending))
			|| (pTrack->iStartRecordNo >= pTrack->iMaxRecords))
		{
			break;  //It should end reading
		}
		
	} //End while
	
	*iRecords = iRecordsEffect;
	//fclose(fp);
	close(iFp);
    DELETE(pRecord);
	return TRUE;
}

/*================================================================================*
 * FUNCTION : DAT_StorageFindRecords
 * PURPOSE  : Find current historical data iRecords pieces from iStartRecordNo.
 *			  It's not include active alarm reading
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: HANDLE  *hCurrData      : Data read Handle
 *			  COMPARE_PROC		pfnCompare, 
 *			  void	  *pCondition
 *            int     *pStartRecordNo : Start record number, if bAscending=TRUE,
 *										(-iMaxRecords) as first record; if 
 *										bAscending=FALSE,the latest record as 
 *										first record.
 *										if *pStartRecordNo=0, it express to continue
 *										read following records (IN)
 *										else if *pStartRecordNo=(-1),it will read 
 *										current Max records. (IN)
 *										else if *pStartRecordNo=(-2),Reading records 
 *										ended,it has compelted records (OUT)
 *            int     *iRecords       : Records to read
 *            void    *pBuff          : Buffer, it will be alloced by caller
 *			  BOOL    bActiveAlarm    : 1: to read the active alarm
 *									    0: to read ended alarm and other data
 *            BOOL    bAscending      : TRUE:direction is from -iMaxRecords
 *									        postion to current writing position	
 *									  : FALSE:From latest record position to oldest
 *									   record position
 * RETURN   : BOOL :				   TRUE:Succeed	   FALSE:Failed
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:47
 *================================================================================*/
BOOL DAT_StorageFindRecords (IN			HANDLE				hCurrData, 
							 IN			COMPARE_PROC		pfnCompare, 
							 IN			void				*pCondition,
							 IN OUT		int				*piStartRecordNo, 
							 IN OUT		int				*piRecords, 
							 OUT		void			*pBuff,
							 IN			BOOL			bActiveAlarm,
							 IN			BOOL			bAscending)
{
	int	iDataTrackIndex; 
	int iRecordsEffect;        //Actually effective records
	int iMaxRecords;

	int iStartRecordNo;
	int iRecords;

	RECORD_SET_TRACK * pFind;
	RECORD_SET_TRACK * pTrack;
	SECTOR_MAP_TABLE * pSector;

#ifdef _DEBUG_DAT_MGMT_
	//printf("Come to DAT_StorageSearchRecords, "
	//	"*piStartRecordNo is %d, *piRecords is %d\n", 
	//	*piStartRecordNo, *piRecords);
#endif
	if(hCurrData == NULL)
	{
		return FALSE;
	}

	if (DAT_GetFlashOpenState() != ERR_DAT_OK)
	{
		//return directly current records are lost.
		*piStartRecordNo = -2;                //End reading record
		*piRecords = 0;
		return  TRUE;                         //The last time
	}
	pFind = &g_RECORD_SET_TRACK[0];
	pTrack = (RECORD_SET_TRACK *)(hCurrData); 
	pSector = pTrack->pbyPhysicsSectorNo;

	iStartRecordNo = *piStartRecordNo;
	iRecords	   = *piRecords;

	if (iRecords > pTrack->iMaxRecords)
	{
		iRecords = pTrack->iMaxRecords;
	}

	//1.Check input arguments
	if (((iStartRecordNo < 0) && (iStartRecordNo != (-1)) 
			&& (iStartRecordNo != (-2)))
		|| ((iStartRecordNo == 0) && 
			(pTrack->iStartRecordNo == THE_FIRST_READ))			//Input error
		|| (iStartRecordNo > pTrack->iMaxRecords)				//Input error
		|| ((iStartRecordNo != (-1)) && (iRecords < 1)))		//Input error
		//|| ((iRecords) > pTrack->iMaxRecordsPerSector))			//Max records once
	{
		return FALSE;
	}

#ifdef _DEBUG_DAT_MGMT_
	//printf("Pass to arguments check.\n");
#endif

	if (iStartRecordNo == (-1))
	{
#ifdef _DEBUG_DAT_MGMT_
		//printf("User want to read all records.\n");
#endif

		iStartRecordNo = 1;
		if (iRecords >= pTrack->iMaxRecords)
		{
			iRecords = pTrack->iMaxRecords;
		}
	}
	else if (iStartRecordNo == 0)
	{
		//Continue to read other records
		iStartRecordNo = pTrack->iStartRecordNo;
	}

	//2.Control reading data scope
	iMaxRecords = GetMaxRecordsReadable(pTrack);
	/*
	if((pTrack->iStartRecordNo >= iMaxRecords))
	*/
	if(((pTrack->iMaxRecords - pTrack->iStartRecordNo) < 0) ||
		((pTrack->iStartRecordNo >= iMaxRecords) && !bAscending))
	{
		*piStartRecordNo = -2;                //End reading record
		*piRecords = 0;
		return  TRUE;                         //The last time 											  
	}//END reading records 

	//3.Get current read logic sector No. and in current sector's position
	if((pTrack->iCurrReadPos == 1) && 
		(pTrack->iStartRecordNo == THE_FIRST_READ)) //The first time
	{
#ifdef _DEBUG_DAT_MGMT_
		//printf("This is first read record.\n");
#endif		
		if (!GetCurrentReadPos(pTrack, iStartRecordNo, bAscending))
		{
			*piRecords = 0;
			return FALSE;
		}
#ifdef _DEBUG_DAT_MGMT_
		//printf("Pass to GetCurrentReadPos check.\n");
#endif
	}	

	//4.Find current data tracking index
	iDataTrackIndex = GetDataTrackIndex(pTrack);
	if (iDataTrackIndex == MAX_DATA_TYPES)
	{
		*piRecords = 0;
		return FALSE;
	}

#ifdef _DEBUG_DAT_MGMT_
	//printf("Pass to GetDataTrackIndex check iDataTrackIndex is %d.\n", 
	//		iDataTrackIndex);
#endif

	iRecordsEffect = 0;	
	/*Add mutex lock,ensure only one user read one type of historical data
	  at the same time */
	Mutex_Lock(g_hMutexReadRecTrack[iDataTrackIndex], DM_LOCK_WAIT_TIME);

	if (!FindMoreRecords_New(pTrack,
							pfnCompare,
							pCondition,
							pBuff, 
					        &iRecords,
					        bActiveAlarm,
					        bAscending))
	{
		Mutex_Unlock(g_hMutexReadRecTrack[iDataTrackIndex]);
        *piRecords = 0;
		return FALSE;
	}

	//Free mutex lock
	Mutex_Unlock(g_hMutexReadRecTrack[iDataTrackIndex]);

#ifdef _DEBUG_DAT_MGMT_
		//printf("Finished to ReadMoreRecords_New iRecords is %d."
		//	"pTrack->iStartRecordNo is %d\n", 
		//	iRecords, pTrack->iStartRecordNo);
#endif
	//Return actual records have been read
    *piRecords = iRecords;   
	*piStartRecordNo = pTrack->iStartRecordNo;

    return TRUE;
}
/*==========================================================================*
 * FUNCTION : GetMaxRecordsReadable
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *pTrack : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-20 19:44
 *==========================================================================*/
static int GetMaxRecordsReadable(IN RECORD_SET_TRACK *pTrack)
{
	int iRecords;

	if (pTrack->dwCurrRecordID > (unsigned)(pTrack->iMaxRecords))
	{
		iRecords = pTrack->iMaxRecords;
	}
	else
	{
		iRecords = pTrack->dwCurrRecordID;
		if (pTrack->dwCurrRecordID == 1)
		{
			iRecords = 0;
		}
	}

    return iRecords;
}


/*==========================================================================*
 * FUNCTION : ReadOneRecord
 * PURPOSE  : Read one record and check it effectively or not.
 * CALLS    : 
 * CALLED BY: DAT_StorageReadRecord
 *			: DAT_fnWriteDataThread
 * ARGUMENTS: FILE    *fp
 *			  long    iOffset            : 
 *            void    *pRecordBuff       : Output record data
 *            int     iRecordSize : It is the data size 
 *									Not include RECORD_ID_BYTES,CRC16_BYTES
 * RETURN   : int :   0: Succeed
 *					  1: Record is empty;
 *					  2: Fail to read
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-17 17:32
 *==========================================================================*/
 static int ReadOneRecord(IN  FILE *fp, 
						  IN  long iOffset, 
				          OUT void *pRecordBuff,
				          IN  int  iRecordSize)
{
	unsigned short iOldCheckNo;
	unsigned short iCalCheckNo;
	int iActualRecordSize;
	void * pRdBuff;
	long iCurrPos;

	iActualRecordSize = iRecordSize + RECORD_ID_BYTES + CRC16_BYTES;
	pRdBuff = (void *)NEW(char, iActualRecordSize);
	if(pRdBuff == NULL)
	{
		return 1;
	}

	iCurrPos = iOffset;	

	rewind(fp);
	fseek(fp, iCurrPos, SEEK_SET);	

	fread(pRdBuff, (unsigned)iActualRecordSize, 1, fp);      //fp has been moved iRecordSize

	//Current record is empty
    if(RecordSpaceIsEmpty(iActualRecordSize, pRdBuff))
	{				
		DELETE(pRdBuff);
		return 1;
	}
	
	//Output record data
	memmove(pRecordBuff, (void *)((BYTE *)pRdBuff + RECORD_ID_BYTES), 
		(unsigned)iRecordSize);

	iOldCheckNo = *((unsigned short *)((char *)pRdBuff 
		+ (iActualRecordSize - CRC16_BYTES)));
	

	//Check number is produced include RECORD_ID_BYTES should be checked
	//It is same as writing record
	iCalCheckNo = CRC16((unsigned char *)(pRdBuff), 
		(unsigned short)(iActualRecordSize - CRC16_BYTES));

	if (iOldCheckNo != iCalCheckNo)       //Error
	{
		rewind(fp);
		fseek(fp, iCurrPos, SEEK_SET);
		fread(pRdBuff, (unsigned)iActualRecordSize, 1, fp);     //Read again

		iOldCheckNo = *((unsigned short*)((char *)pRdBuff 
			+ iActualRecordSize - CRC16_BYTES));

		iCalCheckNo = CRC16((unsigned char *)(pRdBuff), 
			(unsigned short)(iActualRecordSize - CRC16_BYTES));

		//Output record data
		memmove(pRecordBuff, (void*)((char *)pRdBuff + RECORD_ID_BYTES), 
			(unsigned)iRecordSize);

		if (iOldCheckNo != iCalCheckNo) //Error again
		{			
			DELETE(pRdBuff);
			return 2;
		}
	 }
	
	DELETE(pRdBuff);
	return 0;    //Succeed
}

/*==========================================================================*
 * FUNCTION : DAT_StorageUpdateAlarmRecord
 * PURPOSE  : Set one active alarm record's endtime,end one record
 * CALLS    : 
 * CALLED BY: Equipement Management Module
 * ARGUMENTS: HANDLE        hAlarmRecord	: Alarm data handle
 *            int			iAlarmID		: Alarm  ID
 *            time_t		tmEndTime		: Alarm ending information
 * RETURN   : BOOL : TRUE:succeed   FALSE:failed
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:47
 *==========================================================================*/
BOOL DAT_StorageUpdateAlarmRecord (IN HANDLE		hAlarmRecord, 
								   IN int			iAlarmID, 
								   IN time_t		tmEndTime)
{
    int		ioffset, i, j;
	int		iRecordSize;
	int		iActualRecordSize;
	BYTE	iTotalSectors;
	DWORD	dwStoreRecordID;
	RECORD_SET_TRACK * pTrack; 
	SECTOR_MAP_TABLE * pSector;
	
	STORAGE_HISALARM_RECORD		stStoreAlarmRecord;
	ACTIVE_ALARM_RECORD			stAlarmRecord;

	FILE	*fp = fopen(MTD_NAME,"rb");

	if (hAlarmRecord == NULL || fp == NULL)
	{
		return FALSE;
	}
    
	pTrack = (RECORD_SET_TRACK*)hAlarmRecord;
	pSector = pTrack->pbyPhysicsSectorNo;

	iTotalSectors = pTrack->iTotalSectors;
	iRecordSize = pTrack->iRecordSize;
	iActualRecordSize =iRecordSize + 2 * CRC16_BYTES + RECORD_ID_BYTES;
    
	//Cycling sector
	for (i = 0; i < pTrack->iTotalSectors; i++)
	{
		if ( !(DAT_SectorIsEmpty((int)(pSector+i)->byPhySectorNo)))
		{
            ioffset = ((pSector+i)->byPhySectorNo - 1) * SIZE_PER_SECTOR;
			
			ioffset = ioffset - iActualRecordSize;

			//Cycling record in one sector
			for ( j = 0; j < pTrack->iMaxRecordsPerSector; j++)
			{	
				dwStoreRecordID = 0;
				ioffset = ioffset + iActualRecordSize;
		

				//Succeed to read the effective record
				if (ReadOneRecord(fp, ioffset, &stAlarmRecord, iRecordSize) == 0)
				{
					//Found the record to update
					if (stAlarmRecord.iAlarmID == iAlarmID)
					{
						//Read StoreRecordID
						rewind(fp);
						fseek(fp, ioffset, SEEK_SET);;
						fread(&dwStoreRecordID, (unsigned)RECORD_ID_BYTES, 1, fp);

						stStoreAlarmRecord.iCurrRecordID = dwStoreRecordID;
						memmove((void*)((char *)(&stStoreAlarmRecord) + RECORD_ID_BYTES), 
							&stAlarmRecord, (unsigned)sizeof(stAlarmRecord));

						stStoreAlarmRecord.tmEndTime = tmEndTime;
						stStoreAlarmRecord.iCheckNo2 = 
							CRC16((unsigned char*)(&stStoreAlarmRecord),
							(sizeof(STORAGE_HISALARM_RECORD) - CRC16_BYTES));
						

						//Write endtime and iCheckNo2;						
						//Get writing lock global variable
						Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);

						ioffset = ioffset + (iActualRecordSize 
							- sizeof(DWORD) - CRC16_BYTES);

						rewind(g_pFile);
						fseek(g_pFile, ioffset, SEEK_SET);
						fwrite(&(stStoreAlarmRecord.tmEndTime),       
							sizeof(DWORD) + CRC16_BYTES, 1, g_pFile);						
						
						//Free mutex lock
						Mutex_Unlock(g_hMutexWriteRecTrack);

						fclose(fp);
						return TRUE;

					}//update successfully				         

				} //End succeed  				    

			} //End record cycling in one sector			

		} //End if Sector

	} //End sector cycling 
	
    fclose(fp);
	return FALSE;
}

/*==========================================================================*
 * FUNCTION : EraseOneDataType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *pTrack : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-18 09:37
 *==========================================================================*/
static BOOL EraseOneDataType(RECORD_SET_TRACK *pTrack)
{
	int i;
	SECTOR_MAP_TABLE * pSector;

	if (pTrack == NULL)
	{
		return FALSE;
	}

	pSector = pTrack->pbyPhysicsSectorNo;
	for (i = 0; i < pTrack->iTotalSectors; i++)
	{
		if (!EraseSector((pSector + i)->byPhySectorNo, 1))
		{
			AppLogOut("DAT_DEL_REC", APP_LOG_ERROR, 
				"Fail to erase current data %s sector No. %d,"
				"system need to change flash right now.", 
				pTrack->DataName, (int)((pSector + i)->byPhySectorNo));
			return FALSE;
		}
	}

	//Deal with failure to erase
	pTrack->dwCurrRecordID = 1;
	pTrack->iCurrWritePos = 0;
	pTrack->iWritableRecords = pTrack->iMaxRecordsPerSector;
	pTrack->byCurrSectorNo = 1;
	pTrack->byNewSectorFlag = FALSE;  
	
	//AppLogOut("DAT_DEL_REC", APP_LOG_UNUSED, 
	//	"Succeed to execute erasing %s.", pTrack->DataName);
	
	return TRUE;
}
/*==========================================================================*
 * FUNCTION : DAT_StorageDeleteRecord
 * PURPOSE  : Delete current data type all historical records
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: char *pDataName : 
 * RETURN   : BOOL : TRUE:succeed  FALSE:failed
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:48
 *==========================================================================*/
BOOL DAT_StorageDeleteRecord (IN char *pDataName)
{
	int i;
	RECORD_SET_TRACK * pTrack ;
	BOOL bIsAllData;
	BOOL bClearHisData = FALSE;

	//1.Check argument
	if (pDataName == NULL)
	{
#ifdef _DEBUG_DAT_MGMT_
		printf("Input error in DAT_StorageDeleteRecord.\n");
#endif
		return FALSE;
	}

	//Only clear running log	
	if (strcmp(pDataName, ACU_RUNNING_LOG) == 0)
	{
		LOG_DeleteLogRecords();
		return TRUE;
	}
	

	if (DAT_GetFlashOpenState() != ERR_DAT_OK)
	{
		//directly erase.
		//iRlt = system(ERASE_HISDATA);
		return  TRUE;                         //The last time
	}

	//2.Check it is all history data to delete or not
	if (strcmp(pDataName, ALL_HIS_DATAS) == 0)
	{
		bIsAllData = TRUE;
	}
	else
	{
		bIsAllData = FALSE;
		
		//Find the data in current data existed
		for(i = 0; i < g_byMaxDataIndex; i++)
		{	
			pTrack = &g_RECORD_SET_TRACK[i];

			if (strcmp(pTrack->DataName, pDataName) == 0)
			{
#ifdef _DEBUG_DAT_MGMT_
				//printf("Succeed find i is %d, pTrack->DataName is %s\n", i, pTrack->DataName);
#endif
				if(strcmp(HIS_DATA_LOG, pDataName) == 0)
				{
					bClearHisData = TRUE;
				}
				break;
			}
		}

		if (i >= g_byMaxDataIndex)
		{
#ifdef _DEBUG_DAT_MGMT_
			printf("Current data %s isn't created.\n", pDataName);
#endif		
			return TRUE;
		}
	}

	//Clear running log records firstly
	if (bIsAllData)
	{
		LOG_DeleteLogRecords();
	}	

	//3.Get writing data lock
	Mutex_Lock(g_hMutexWriteRecTrack, DM_LOCK_WAIT_TIME);
	//User needs to erase all data type
	if (bIsAllData)
	{
#ifdef _DEBUG_DAT_MGMT_
		//printf("System needs to clear all records in flash.\n");
#endif
		//User will to erase all hisdata records
		//Erase all historical data sectors except static 2 sectors and user info
	    //Update every historical data tracking information
		pTrack = &(g_RECORD_SET_TRACK[0]);
		for(i = 0; i < g_byMaxDataIndex; i++)
		{	
			if (strcmp(pTrack->DataName, SYSTEM_USER_LOG) != 0) //Let it be
			{
               EraseOneDataType(pTrack);
			}
			pTrack++;
		}

		AppLogOut("DAT_DEL_REC", APP_LOG_INFO, 
					"Succeed to execute erasing all historical data.");
	}
	else
	{
		pTrack = &g_RECORD_SET_TRACK[i];
		if (!EraseOneDataType(pTrack))
		{
			Mutex_Unlock(g_hMutexWriteRecTrack);	
			return FALSE;
		}
	}

	if(bClearHisData || bIsAllData)
	{
		NotificationFunc(_CLEAR_HISTORY_MASK,	// masks
				sizeof(int),				// the sizeof (EQUIP_INFO *)
				&bClearHisData,							// the equipment address ref.
				FALSE);	
	}

	//Free mutex lock
    Mutex_Unlock(g_hMutexWriteRecTrack);	
    
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//New searching method

/*==========================================================================*
 * FUNCTION : GetDataTrackIndex
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK *  pTrack : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-19 18:38
 *==========================================================================*/
static int GetDataTrackIndex(IN RECORD_SET_TRACK *pTrack)
{
	int iDataTrackIndex;
	RECORD_SET_TRACK *pFind;

	pFind = &g_RECORD_SET_TRACK[0];

	for (iDataTrackIndex = 0; iDataTrackIndex < MAX_DATA_TYPES; 
		iDataTrackIndex++)
	{
		if (pFind->iDataRecordIDIndex == pTrack->iDataRecordIDIndex)
		{
			break;
		}
		pFind++;
	}
	
	//data error
	if (iDataTrackIndex == MAX_DATA_TYPES)
	{
		return (-1);
	}

	return iDataTrackIndex;
}
/*==========================================================================*
 * FUNCTION : GetActualRecordSize
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK *  pTrack              : 
 *            BOOL                bActiveAlarm        : 
 *            int                 *piRecordSize       : 
 *            int                 *piActualRecordSize : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-22 11:24
 *==========================================================================*/
static int GetActualRecordSize(IN  RECORD_SET_TRACK * pTrack, 
							   IN  BOOL bActiveAlarm,
							   OUT int *piRecordSize, 
						       OUT int *piActualRecordSize)
{
	int iDataTrackIndex;
	int iRecordSize, iActualRecordSize;
	
	iRecordSize = pTrack->iRecordSize;
    iActualRecordSize = iRecordSize + CRC16_BYTES + RECORD_ID_BYTES;
	
	///////////////////////////////////////////////////////////////////////////
	//if pTrack->iMaxRecord > pTrack->iMaxRecordsPerSector,pTrack->iCurrReadPos
	//has new meaning, it express from -(pTrack->iMaxRecords) position start count.
	//System will read record in the recent iMaxRecords
		
	//data error
	iDataTrackIndex = GetDataTrackIndex(pTrack);

	if (iDataTrackIndex < 0)
	{
		return (-1);
	}

	//alarm record
	if (g_HISDATA_QUEUE[iDataTrackIndex].bIsAlarm)
	{
		//include endtime and checkno1
		if (!bActiveAlarm)  //to read ending alarm
		{
			//bActiveAlarm = FALSE to read ending alarm
			iRecordSize = iRecordSize + sizeof(DWORD) + CRC16_BYTES;
		}

		iActualRecordSize = iActualRecordSize + sizeof(DWORD) + CRC16_BYTES;
	}

	*piRecordSize = iRecordSize;
	*piActualRecordSize = iActualRecordSize;

	return 0;
}


/*==========================================================================*
 * FUNCTION : FindMoreRecords_New
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RECORD_SET_TRACK  *pTrack      : 
 *			  COMPARE_PROC		pfnCompare   : 
 *			  void				*pCondition  :
 *            void              *pRecordBuff : 
 *            int               *iRecords    : 
 *            BOOL              bActiveAlarm : 
 *            BOOL              bAscending   : 
 *            BOOL              bIsRecorNo  
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-20 13:46
 *==========================================================================*/
static BOOL SearchMoreRecords_New(IN OUT RECORD_SET_TRACK *pTrack,
								  IN	 COMPARE_PROC	  pfnCompare, 
								  IN	 void			  *pCondition,
								  OUT	 void			  *pRecordBuff, 
								  IN OUT int			  *iRecords,
								  IN	 BOOL			  bActiveAlarm,
								  IN	 BOOL			  bAscending,
								  IN	 BOOL			  bIsRecorNo)
{
	FILE *fp;
	int  iFp = (-1);
	long iOffset;
	int  iAscendRecordNo;

	long iIniOffset;
	int  iRecordsToRead;
	int  iRecordsInCurrSector;
	
	int iRecordsEffect;
	int iEmptyRecords, iMaxValidRecords;
	int iReadReturn;
	int iResult, k;
	long iCurrPos;
	int iRecordSize, iAcAlarmAddSize, iActualRecordSize;
	void *pRecord;	

#ifdef _DEBUG_DAT_MGMT_
	//printf("Enter to SearchMoreRecords_New.\n");
	//printf("*iRecords is %d, pTrack->iStartRecordNo is %d.\n", 
	//		*iRecords, pTrack->iStartRecordNo);
#endif
	//fp = fopen(MTD_NAME,"rb");
	//if (fp == NULL)
	//{
	//	return FALSE;
	//}
	iFp =  open(MTD_NAME, O_RDONLY);
	if (iFp == (-1))
	{
		return FALSE;
	}

	iRecordsEffect = 0;
	iEmptyRecords = 0;
	iReadReturn = 0;

	//1.Get record size and storage record size
	iResult = GetActualRecordSize(pTrack, 
						bActiveAlarm,
						&iRecordSize, 
						&iActualRecordSize);
	if (iResult == (-1))
	{
		//fclose(fp);
		close(iFp);
		return FALSE;
	}
	iAcAlarmAddSize = sizeof(DWORD) + CRC16_BYTES;	

	pRecord = (void *)NEW(char, iRecordSize);
	if(pRecord == NULL)
	{
		close(iFp);
		return FALSE;
	}
	
	iResult = 0;
	iRecordsToRead = *iRecords;
	iIniOffset = 0;


	while (iRecordsEffect < *iRecords)
	{	
		//1.Get iRecordsToRead in current sectors
		iRecordsToRead = *iRecords - iRecordsEffect;
		iResult = ReadRecordsControl(pTrack, 
							   &iIniOffset,
							   &iRecordsToRead,
							   &iRecordsInCurrSector,   //iRecordsInCurrSector is dead length
							   bAscending);


		if (iResult == ERR_CTRL_INPUT)
		{
			DELETE(pRecord);
			//fclose(fp);
			close(iFp);
			return FALSE;
		}
		else if (iResult == ERR_END_READING)
		{
            break;
		}
		iOffset = iIniOffset + pTrack->iCurrReadPos * iActualRecordSize;

		iCurrPos = iOffset;
		iEmptyRecords = 0;
		for (k = 0; k < iRecordsInCurrSector; k++)
		{
			//Process alarm specially
			if (bActiveAlarm && (k != 0))
			{
				;//iCurrPos = iCurrPos + iAcAlarmAddSize;
			}

			iReadReturn = ReadOneRecord_New(iFp, iCurrPos, pRecord, iRecordSize);
			//records are empty
			if ((iReadReturn == 1) )
			{
				iEmptyRecords++;
				if(iEmptyRecords >= 3)    //continuous three records are empty
				{
#ifdef _DEBUG_DAT_MGMT_
		//printf("It found that there are %d empty records.\n", iEmptyRecords);
#endif
					pTrack->iStartRecordNo = pTrack->iStartRecordNo + 
						(iRecordsInCurrSector - k - 1);
					if (bAscending)
					{
						pTrack->iCurrReadPos = 
							pTrack->iCurrReadPos + (iRecordsInCurrSector - k - 1);
					}
					else
					{
						pTrack->iCurrReadPos = 
							pTrack->iCurrReadPos - (iRecordsInCurrSector - k - 1);
					}

					break;
				}
			}

			if((iReadReturn == 0) && 
				(pfnCompare(pRecord, pCondition) == 1))  //Effective records
			{	
				iEmptyRecords = 0;
				if (bIsRecorNo)      //It is different
				{
					//All record no change to ascending index no for battery test record
					if (!bAscending)
					{
						iAscendRecordNo = pTrack->iMaxRecords 
							- pTrack->iStartRecordNo + 1;
					}
					else
					{
						iAscendRecordNo = pTrack->iStartRecordNo;
					}
#ifdef _DEBUG_DAT_MGMT_
					//printf("Current iAscendRecordNo is %d\n", iAscendRecordNo);
#endif
					memmove((void*)((char *)pRecordBuff + 
						iRecordsEffect * sizeof(int)),
						&(iAscendRecordNo), 
						(unsigned)sizeof(int));
				}
				else
				{
					memmove((void*)((char *)pRecordBuff + 
						iRecordsEffect * iRecordSize),
						pRecord, 
						(unsigned)iRecordSize);
				}

				iRecordsEffect++;
#ifdef _DEBUG_DAT_MGMT_
				//printf("iRecordsEffect are %d records.\n", iRecordsEffect);
#endif
			}

			if (iReadReturn == 2)  //Invalid record
			{
				iEmptyRecords = 0;
			}
			
			if (bAscending)
			{
				iCurrPos = iCurrPos + iActualRecordSize;
				pTrack->iCurrReadPos++;
			}
			else
			{
				iCurrPos = iCurrPos - iActualRecordSize;
				pTrack->iCurrReadPos--;
			}
			
			pTrack->iStartRecordNo++;

			if ((iRecordsToRead == 0) || 
				(iRecordsEffect >= *iRecords))
			{
				break; //End reading record
			}
		}//End for
		
		
        if ((iRecordsToRead == 0) || 
				(iRecordsEffect >= *iRecords))
		{
			break; //End reading record
		}

		iMaxValidRecords = GetMaxRecordsReadable(pTrack);
		if (((pTrack->iStartRecordNo >= iMaxValidRecords) && (!bAscending))
			|| (pTrack->iStartRecordNo >= pTrack->iMaxRecords))
		{
			break;  //It should end reading
		}
		
	} //End while
	
	*iRecords = iRecordsEffect;
	//fclose(fp);
	close(iFp);
    DELETE(pRecord);
	return TRUE;
}

/*======================================================================================*
 * FUNCTION : DAT_StorageSearchRecord_New
 * PURPOSE  : Search for records with contions 
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: HANDLE        hCurrData        : Data Handle
 *            COMPARE_PROC  pfnCompare       : Callback function
 *            void          *pCondition      : Compare conditions
 *            int           *piStartRecordNo : Start record number, if bAscending=TRUE,
 *										(-iMaxRecords) as first record; if 
 *										bAscending=FALSE,the latest record as 
 *										first record.
 *										if *pStartRecordNo=0, it express to continue
 *										read following records (IN)
 *										else if *pStartRecordNo=(-1),it will read 
 *										current Max records. (IN)
 *										else if *pStartRecordNo=(-2),Reading records 
 *										ended,it has compelted records (OUT)
 *                                      If bIsRecordNo=TRUE:returned record no.sequence is that 
 *									    (-iMaxRecords) as first record whether bAscending 
 *									    TRUE or FALSE
 *            int           *piRecords       : Valid Records expected to find  
 *            void          *pBuff           : Buffer to store valid records
 *            BOOL          bActiveAlarm     : TRUE:to search active alarm
 *            BOOL          bAscending       : TRUE:direction is from -iMaxRecords
 *										     : postion to current writing position
 *            BOOL          bIsRecordNo      : TRUE:express only get the valid 
 *										     : record's position No.(int) 
 *											 : such as (*pStartRecordNo)								 
 * RETURN   : BOOL :				           TRUE:Succeed	   FALSE:Failed
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-23 10:11
 *=====================================================================================*/
BOOL DAT_StorageSearchRecords(IN HANDLE		hCurrData, 
						   IN COMPARE_PROC		pfnCompare, 
						   IN		void		*pCondition,
						   IN OUT	int			*piStartRecordNo, 
						   IN OUT	int			*piRecords,
						   OUT		void		*pBuff, 
						   IN		BOOL		bActiveAlarm,
						   IN		BOOL		bAscending,
						   IN		BOOL		bIsRecordNo)
{
	int	iDataTrackIndex; 
	int iRecordsEffect;        //Actually effective records
	int iMaxRecords;

	int iStartRecordNo;
	int iRecords;

	RECORD_SET_TRACK * pFind;
	RECORD_SET_TRACK * pTrack;
	SECTOR_MAP_TABLE * pSector;


#ifdef _DEBUG_DAT_MGMT_
	//printf("Come to DAT_StorageSearchRecords, "
	//	"*piStartRecordNo is %d, *piRecords is %d\n", 
	//	*piStartRecordNo, *piRecords);
#endif
	if(hCurrData == NULL)
	{
	    //printf("return 1\n");
		return FALSE;
	}

	if (DAT_GetFlashOpenState() != ERR_DAT_OK)
	{
		//return directly current records are lost.
		*piStartRecordNo = -2;                //End reading record
		*piRecords = 0;
		//printf("return 2\n");
		return  TRUE;                         //The last time
	}

	pFind = &g_RECORD_SET_TRACK[0];
	pTrack = (RECORD_SET_TRACK *)(hCurrData); 
	pSector = pTrack->pbyPhysicsSectorNo;

	iStartRecordNo = *piStartRecordNo;
	iRecords	   = *piRecords;

	if (iRecords > pTrack->iMaxRecords)
	{
		iRecords = pTrack->iMaxRecords;
	}

	//1.Check input arguments
	if (((iStartRecordNo < 0) && (iStartRecordNo != (-1)) 
			&& (iStartRecordNo != (-2)))
		|| ((iStartRecordNo == 0) && 
			(pTrack->iStartRecordNo == THE_FIRST_READ))			//Input error
		|| (iStartRecordNo > pTrack->iMaxRecords)				//Input error
		|| ((iStartRecordNo != (-1)) && (iRecords < 1)))			//Input error
		//|| ((iRecords) > pTrack->iMaxRecordsPerSector))			//Max records once
	{
	    //printf("return 3\n");
		return FALSE;
	}

#ifdef _DEBUG_DAT_MGMT_
	//printf("Pass to arguments check.\n");
#endif

	if (iStartRecordNo == (-1))
	{
#ifdef _DEBUG_DAT_MGMT_
		printf("User want to read all records.\n");
#endif
		
		iStartRecordNo = 1;
		if (iRecords >= pTrack->iMaxRecords)
		{
			iRecords = pTrack->iMaxRecords;
		}
	}
	else if (iStartRecordNo == 0)
	{
		//Continue to read other records
		iStartRecordNo = pTrack->iStartRecordNo;
	}

	//2.Control reading data scope
	iMaxRecords = GetMaxRecordsReadable(pTrack);
	/*printf("iMaxRecords is %d, pTrack->iMaxRecords is %d, pTrack->iStartRecordNo is %d\n", 
	    iMaxRecords, pTrack->iMaxRecords, pTrack->iStartRecordNo);*/
	/*
	if((pTrack->iStartRecordNo >= iMaxRecords))
	*/
	if(((pTrack->iMaxRecords - pTrack->iStartRecordNo) < 0) ||
		((pTrack->iStartRecordNo >= iMaxRecords) && !bAscending))
	{
		*piStartRecordNo = -2;                //End reading record
		*piRecords = 0;
		//printf("return 4\n");
		return  TRUE;                         //The last time 											  
	}//END reading records 


	if((pTrack->iCurrReadPos == 1) && 
		(pTrack->iStartRecordNo == THE_FIRST_READ)) //The first time
	{
#ifdef _DEBUG_DAT_MGMT_
		//printf("This is first read record.\n");
#endif		
		if (!GetCurrentReadPos(pTrack, iStartRecordNo, bAscending))
		{
			*piRecords = 0;
			//printf("return 5\n");
			return FALSE;
		}
#ifdef _DEBUG_DAT_MGMT_
		//printf("Pass to GetCurrentReadPos check.\n");
#endif
	}	

	//4.Find current data tracking index
	iDataTrackIndex = GetDataTrackIndex(pTrack);
	if (iDataTrackIndex == MAX_DATA_TYPES)
	{
		*piRecords = 0;
		//printf("return 6\n");
		return FALSE;
	}

#ifdef _DEBUG_DAT_MGMT_
	//printf("Pass to GetDataTrackIndex check iDataTrackIndex is %d.\n", 
	//		iDataTrackIndex);
#endif

	iRecordsEffect = 0;	

	Mutex_Lock(g_hMutexReadRecTrack[iDataTrackIndex], DM_LOCK_WAIT_TIME);

	if (!SearchMoreRecords_New(pTrack,
							pfnCompare,
							pCondition,
							pBuff, 
					        &iRecords,
					        bActiveAlarm,
					        bAscending,
							bIsRecordNo))
	{
		Mutex_Unlock(g_hMutexReadRecTrack[iDataTrackIndex]);
        *piRecords = 0;
	//printf("return 7\n");
		return FALSE;
	}

	//Free mutex lock
	Mutex_Unlock(g_hMutexReadRecTrack[iDataTrackIndex]);

#ifdef _DEBUG_DAT_MGMT_
		//printf("Finished to ReadMoreRecords_New iRecords is %d."
		//	"pTrack->iStartRecordNo is %d\n", 
		//	iRecords, pTrack->iStartRecordNo);
#endif
	//Return actual records have been read
    *piRecords = iRecords;   
	*piStartRecordNo = pTrack->iStartRecordNo;
	//printf("return 8\n");

    return TRUE;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*==========================================================================*
 * FUNCTION : DAT_StorageClose
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: (HANDLE  hCurrData : Data Handle
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:50
 *==========================================================================*/
BOOL DAT_StorageClose (IN HANDLE hCurrData)
{
	HISDATA_QUEUE  *pQueue;

	if (hCurrData == NULL)
	{
		return TRUE;
	}

	pQueue = (HISDATA_QUEUE *)hCurrData;

	//It is write handle (It is &g_HISDATA_QUEUE[x])
    if ((pQueue->bIsAlarm == 0) 
		|| (pQueue->bIsAlarm == 1))
	{
		hCurrData = NULL;		
	}
	else  //Read record pointer, it is NEW(RECORD_SET_TRACK, 1)
	{
#ifdef _DEBUG_DAT_MGMT_1
		if (hCurrData != NULL)
		{
			//printf("Succeed to close the data %s\n", ((RECORD_SET_TRACK *)hCurrData)->DataName);
		}
#endif
		SAFELY_DELETE(hCurrData);		
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : CRC16 calculation
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: unsigned char   *Msg : 
 *            unsigned short  len  : 
 * RETURN   : unsigned short : 
 * COMMENTS : Copy
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 11:15
 *==========================================================================*/
static unsigned short CRC16(IN unsigned char *Msg, IN unsigned short len)
{
	unsigned short CRC;
    unsigned short i,temp;

	CRC = 0xFFFF;

    while(len--)
    {
        CRC = CRC^(*Msg++);
        for(i=0;i++<8;)
        {
            if(CRC&0x0001)
                CRC = (CRC>>1)^0xa001;
            else
                CRC>>=1;
        }
    }

    temp = CRC&0xFF;
    CRC = ((CRC>>8)&0xFF)+(temp<<8);  
	
    return(CRC);
}

/*==========================================================================*
 * FUNCTION : region_erase
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  Fd       : //Device file handle
 *            int  start    : //Start position
 *            int  count    : //current sector size divided by erase_info_t
 *            int  unlock   : //0:all is unlocked.1;some bites is locked
 *            int  regcount : //Sectors
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-09-23 15:35
 *==========================================================================*/
static int region_erase(IN int Fd, 
						IN int start, 
						IN int count, 
						IN int unlock, 
						IN int regcount)
{
	int i, j;
	region_info_t * reginfo;

	reginfo = (region_info_t *)calloc((unsigned)regcount, 
		(unsigned)sizeof(region_info_t));
	if(reginfo == NULL)
	{
		return 0;
	}
	for(i = 0; i < regcount; i++)
	{
		reginfo[i].regionindex = i;
		if(ioctl(Fd, MEMGETREGIONINFO, &(reginfo[i])) != 0)
		{
			return 8;
		}			
		else
		{
#ifdef _DEBUG_DAT_MGMT_
			//printf("Region %d is at %d of %d sector and with sector "
			//"size %x\n", i, reginfo[i].offset, reginfo[i].numblocks,
			//reginfo[i].erasesize);
#endif //_DEBUG_DAT_MGMT_
		}
	}

	// We have all the information about the chip we need.

	for(i = 0; i < regcount; i++)
	{ //Loop through the regions
		region_info_t * r = &(reginfo[i]);

		if(((unsigned int)start >= reginfo[i].offset) &&
				((unsigned int)start < (r->offset + r->numblocks*r->erasesize)))
			break;
	}

	if(i >= regcount)
	{
		
#ifdef _DEBUG_DAT_MGMT_
		//printf("Starting offset %x not within chip.\n", start);
#endif //_DEBUG_DAT_MGMT_

		return 8;
	}

	//We are now positioned within region i of the chip, so start erasing
	//count sectors from there.

	for(j = 0; (j < count)&&(i < regcount); j++)
	{
		erase_info_t erase;
		region_info_t * r = &(reginfo[i]);

		erase.start = start;
		erase.length = r->erasesize;

		if(unlock != 0)
		{ //Unlock the sector first.
			if(ioctl(Fd, MEMUNLOCK, &erase) != 0)
			{
				
#ifdef _DEBUG_DAT_MGMT_
				//perror("\nMTD Unlock failure");
#endif //_DEBUG_DAT_MGMT_

				close(Fd);
				return 8;
			}
		}
	
#ifdef _DEBUG_DAT_MGMT_
		//printf("\rPerforming Flash Erase of length %u at offset 0x%x\n",
		//	erase.length, erase.start);
#endif //_DEBUG_DAT_MGMT_

	
		fflush(stdout);
		if(ioctl(Fd, MEMERASE, &erase) != 0)
		{
			
#ifdef _DEBUG_DAT_MGMT_
			//perror("\nMTD Erase failure");
#endif //_DEBUG_DAT_MGMT_

			close(Fd);
			return 8;
		}


		start += erase.length;
		if((unsigned int)start >= (r->offset + r->numblocks*r->erasesize))
		{ //We finished region i so move to region i+1

#ifdef _DEBUG_DAT_MGMT_
			//printf("\nMoving to region %d\n", i+1);
#endif //_DEBUG_DAT_MGMT_
			
			i++;
		}
	}

#ifdef _DEBUG_DAT_MGMT_
	//printf(" done\n");
#endif

	return 0;
}

/*==========================================================================*
 * FUNCTION : non_region_erase
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  Fd       : //Device file handle
 *            int  start    : //Start position
 *            int  count    : //current sector size divided by erase_info_t.length
 *								erase_info_t.length = mtd_info_t.erasesize
 *            int  unlock   : //0:all is unlocked.1;some bites is locked
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-09-23 15:35
 *==========================================================================*/
int non_region_erase(IN int Fd, 
					IN int start, 
					IN int count, 
					IN int unlock)
{
	mtd_info_t meminfo;
	HANDLE hCurrentThread;

	hCurrentThread = RunThread_GetId(NULL);
	static int nFirst = 0;
	
	if (ioctl(Fd, MEMGETINFO, &meminfo) == 0)
	{
		erase_info_t erase;

		erase.start = start;

		erase.length = meminfo.erasesize;

#ifdef _DEBUG_DAT_MGMT_
		//printf("Erase Unit Size 0x%x, ", meminfo.erasesize);
#endif

		for (; count > 0; count--) 
		{
			RunThread_Heartbeat(hCurrentThread);

#ifdef _DEBUG_DAT_MGMT_
			printf("\rPerforming Flash Erase of length %u at offset 0x%x\n",
					erase.length, erase.start);
#endif
			fflush(stdout);

			if(unlock != 0)
			{

				//Unlock the sector first.
#ifdef _DEBUG_DAT_MGMT_
				//printf("\rPerforming Flash unlock at offset 0x%x",erase.start);
#endif

				if(ioctl(Fd, MEMUNLOCK, &erase) != 0)
				{
#ifdef _DEBUG_DAT_MGMT_
					perror("\nMTD Unlock failure");
#endif

					close(Fd);
					return 8;
				}
			}
			if(nFirst == 0)
			{
				Sleep(500);
				nFirst = 1;
				
			}
 			if (ioctl(Fd, MEMERASE, &erase) != 0)
			{   
#ifdef _DEBUG_DAT_MGMT_
				perror("\nMTD Erase failure");
#endif
				close(Fd);
				return 8;
			}
			erase.start += meminfo.erasesize;
 		}

	}

	return 0;
}


/*==========================================================================*
 * FUNCTION : DAT_CurrentStatusIsRDONLY
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   char *pszConst : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : lixidong                 DATE: 2006-12-12 09:59
 *==========================================================================*/
BOOL DAT_CurrentStatusIsRDONLY(char *pszConst)
{
	//char	*pszHardwareSetting;
	//BOOL	bACUIsReadOnly;
/*
	pszHardwareSetting = getenv(ENV_ACU_JUMP_APP_RDONLY);
	bACUIsReadOnly = (pszHardwareSetting == NULL) ? FALSE
		: (atoi(pszHardwareSetting) != 0) ? TRUE : FALSE;

	if (bACUIsReadOnly)
	{
#ifdef _DEBUG_MAINTN_
		printf("ACU is read-only.\n");
#endif
		return ACU_JUMP_READONLY;	
	}
	else
	{
#ifdef _DEBUG_MAINTN_
		printf("ACU is not read-only.\n");
#endif
		return ACU_JUMP_NOT_SET;
	}
*/
	FILE *fp;
	char *pszFileContents;
	char *pStep = ":";
	char *pDsnStr;
	char *pSetStr;
	long iFilelen;

	fp = fopen(APP_RW_SET_FILE, "r");

	if ((fp == NULL) || (!pszConst))
	{
		return FALSE;
	}
	
	fseek(fp, 0, SEEK_END);
	iFilelen = ftell(fp);

	if (iFilelen == -1)
	{
		fclose(fp);
		return FALSE;
	}
	fseek(fp, 0, SEEK_SET);

	pszFileContents = NEW(char, iFilelen + 1);
	if (pszFileContents == NULL)
	{
		fclose(fp);
		return FALSE;
	}
	
	iFilelen = fread(pszFileContents, sizeof(char), (size_t)iFilelen, fp);
	fclose(fp);

	if (iFilelen < 0) 
	{
		DELETE(pszFileContents);
		return FALSE;
	}

	pDsnStr = strstr(pszFileContents, pszConst);
	
	if (pDsnStr == NULL)
	{
		DELETE(pszFileContents);
		return FALSE;
	}
	else
	{
		TRACE("Find %s region is Read-Only.\n", pszConst);
		DELETE(pszFileContents);
		return TRUE;
	}	
}










/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : test_data_mng.c
 *  CREATOR  : LIXIDONG                 DATE: 2004-09-27 10:23
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <math.h>
#include "stdsys.h"
#include "public.h"
#include "his_data.h"

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO  g_SiteInfo;

//need to be defined in Miscellaneous Function Module
TIME_SRV_INFO g_sTimeSrvInfo;

//need to be defined in equip management Module
BOOL Equip_Control(int nEquipID, int nSigType, int nSigID, 
				   VAR_VALUE varCtrlValue)
{
	UNUSED(nEquipID);
	UNUSED(nSigType);
	UNUSED(nSigID);
	UNUSED(varCtrlValue);

	return TRUE;
}

//need to be defined in Miscellaneous Function Module
int UpdateACUTime (time_t* pTimeRet)
{
	return stime(pTimeRet);
}

char		g_szACUConfigDir[256];
///////////////////////////////////////////////////////////////////////////////

/*==========================================================================*
 * FUNCTION : test_data_mng     
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-19 12:57
 *==========================================================================*/
//int main(int argc, char *argv[])
int main(void)
{
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
//This thread is used in debug_maintn.c to test data management module,
//run 24hours,no any errors
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////	
//1.Create Thread 
hTestHisData = RunThread_Create("TestHisData", 
						(RUN_THREAD_START_PROC)TestHisData,
						NULL,
						NULL,
						0);	
				
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//2.Thread relevant functions
//Start test data management module
char *GetHisDataName(IN int iDataType)
{
	const char *pszHisDataName[] =
	{
		HIS_ALARM_LOG,
		HIS_DATA_LOG,
		STAT_DATA_LOG,		
		CTRL_CMD_LOG
	};

	return (char *)pszHisDataName[iDataType];
}

//Historical Signal Data Record
struct tagHisDataTestRecord
{
	int				iEquipID;						// Equipment ID 
	int				iSignalID;						// Signal ID
	VAR_VALUE		varSignalVal;					// Signal Value 
	time_t			tmSignalTime;					// Signal time 
    int				iActAlarmID;					// Actived Alarm ID   
	char			szBuff[108];
 };

typedef struct tagHisDataTestRecord HIS_DATA_TEST_RECORD;


int GetHisDataRecordSize(IN int iDataType)
{
	const int iSizeofHisData[] =
	{
		sizeof(ACTIVE_ALARM_RECORD),
		sizeof(HIS_DATA_TEST_RECORD),
		sizeof(HIS_STAT_RECORD),		
		sizeof(HIS_CONTROL_RECORD)
	};

	return (int)iSizeofHisData[iDataType];
}



static void TestHisData(void)
{
	int i, j;
	int iRecords;
	BOOL bExitFlag;
	int	iCounter;
	BOOL	bWriteFlag, bReadFlag;
	HANDLE hWrite[4], hRead[4];
	Sleep(2000);
	int iSize;

	ACTIVE_ALARM_RECORD stAlarm[10], stAlarmRead;
	HIS_DATA_TEST_RECORD	stData[10], stDataRead;
	HIS_STAT_RECORD		stStaRec[10], stStaRecRead;
	HIS_CONTROL_RECORD  stCtrl[10], stCtrlEead;
	
	HANDLE hCurrentThread;


	hCurrentThread = RunThread_GetId(NULL);	
	RunThread_Heartbeat(hCurrentThread);	
	

	for (i = 0; i < 4; i++)
	{
		if (i == 1)
		{
			iSize = 10000;
		}
		else
		{
			iSize = 500;
		}

		hWrite[i] = DAT_StorageCreate (
			GetHisDataName(i), 
			GetHisDataRecordSize(i),
			iSize, 
			1000);
		if (hWrite[i] != NULL)
		{
#ifdef _DEBUG
			printf("GetHisDataRecordSize(i) is %d\n", GetHisDataRecordSize(i));
			printf("Write handle %d has been created successful.\n", hWrite[i]);
#endif			
		}		
	}

	RunThread_Heartbeat(hCurrentThread);		
	
	iCounter = 0;

	for (i = 0; i < 9; i++)
	{	
		stAlarm[i].iAlarmID = iCounter;
		stAlarm[i].iEquipID = 100;
		stAlarm[i].tmStartTime = (time_t)400;
		stAlarm[i].varTrigValue.fValue = 999.9 + iCounter;

		stData[i].iActAlarmID = iCounter;
		stData[i].iEquipID    = iCounter;
		stData[i].iSignalID   = 1009;
		stData[i].tmSignalTime = (time_t)400;
		stData[i].varSignalVal.fValue = 999.9;
		memset(stData[i].szBuff ,0, (unsigned)108);

		stStaRec[i].iEquipID   = 100;
		stStaRec[i].iSignalID  = iCounter;
		stStaRec[i].tmMaxValue = (time_t)400;
		stStaRec[i].tmMinValue = (time_t)400;
		stStaRec[i].tmStatTime = (time_t)400;
		stStaRec[i].varAverageValue.fValue = 0.0;
		stStaRec[i].varCurrentVal.fValue	 = 999.9;
		stStaRec[i].varMaxValue.fValue	 = 999.9;
		stStaRec[i].varMinValue.fValue	 = -999.9;

		stCtrl[i].byControlResult	=	  1;
		stCtrl[i].bySenderType		=	  1;
		stCtrl[i].iEquipID			=	100;
		stCtrl[i].iSignalID		=	1009;
		strcpy(stCtrl[i].szCtrlSenderName, "CONTROL");
		stCtrl[i].tmControlTime =  (time_t)400;
		stCtrl[i].varControlVal.fValue = (float)iCounter;
	}

	Sleep(500);
	
	bExitFlag = FALSE;
	while (!bExitFlag)
	{
        iCounter++;
		Sleep(200);
		//Write data


		//1.Initiate data
		stAlarm[0].iAlarmID	= iCounter;
		stData[0].iEquipID = iCounter;
		stStaRec[0].iSignalID = iCounter;
		stCtrl[0].varControlVal.fValue = (float)iCounter;

		RunThread_Heartbeat(hCurrentThread);
		/*
		if (!THREAD_IS_RUNNING(hCurrentThread))
		{
			bExitFlag = TRUE;
#ifdef _DEBUG
			printf("Receive a exit info from main program.hCurrentThread is %d\n",
				hCurrentThread);
			printf("Receive a exit info from main program.hCurrentThread is %d\n",
				hCurrentThread);
			printf("Receive a exit info from main program.hCurrentThread is %d\n",
				hCurrentThread);
			printf("Receive a exit info from main program.hCurrentThread is %d\n",
				hCurrentThread);
			printf("Receive a exit info from main program.hCurrentThread is %d\n",
				hCurrentThread);
#endif
			break;
		}*/

		//2.Write data
		for (i = 0; i < 4; i++)
		{
			if (i == 0)
			{
				if (iCounter%10 == 0)
				{
					bWriteFlag = DAT_StorageWriteRecord (hWrite[i],
							 GetHisDataRecordSize(i), 
							 (void*)&stAlarm[0]);
					if (!bWriteFlag)
					{
#ifdef _DEBUG
						printf("Fail to write Alarm.\n");
#endif
					}
				}
			}
			else if(i == 1)
			{
				if (iCounter%10 == 0)
				{
					bWriteFlag = DAT_StorageWriteRecord (hWrite[i],
							GetHisDataRecordSize(i), 
							 (void*)&stData[0]);
					if (!bWriteFlag)
					{
#ifdef _DEBUG
						printf("Fail to write HisData.\n");
#endif

					}
					RunThread_Heartbeat(hCurrentThread);
				}
			}
			else if(i == 2)
			{
				if (iCounter%10 == 0)
				{
					bWriteFlag = DAT_StorageWriteRecord (hWrite[i],
							GetHisDataRecordSize(i), 
							 (void*)&stStaRec[0]);

					if (!bWriteFlag)
					{
#ifdef _DEBUG
						printf("Fail to write Statistic Data.\n");
#endif

					}
				}
			}
			else
			{
				bWriteFlag = DAT_StorageWriteRecord (hWrite[i],
							 GetHisDataRecordSize(i), 
							 (void*)&stCtrl[0]);
					if (!bWriteFlag)
					{
#ifdef _DEBUG
						printf("Fail to write Control Command.\n\n");
#endif
					}
			}			
		}

		RunThread_Heartbeat(hCurrentThread);


		//Read data and printf data
		//Write data
		
    	iRecords = 1;
		for (i = 0; i < 4; i++)
		{
			hRead[i] = DAT_StorageOpen (GetHisDataName(i));	

			if (i == 0)
			{
				memset((void *)&stAlarmRead, 0, 
					(unsigned)GetHisDataRecordSize(i));
				bReadFlag = DAT_StorageReadRecord (hRead[i],
							499, 
							&iRecords, 
							&stAlarmRead,
							1);

				if (!bReadFlag)
				{
#ifdef _DEBUG
					printf("Fail to Read Alarm.\n\n");
#endif
				}
#ifdef _DEBUG
				//printf("Alarm varTrigValue is %f\n", stAlarmRead.varTrigValue.fValue);
#endif
			}
			else if(i == 1)
			{
			
				iRecords = 1;
				memset((void *)&stDataRead, 0,(unsigned) GetHisDataRecordSize(i));
#ifdef _DEBUG
				printf("Start to read big data.\n");
#endif
				bReadFlag = DAT_StorageReadRecord (hRead[i],
							9999, 
							&iRecords, 
							&stDataRead,
							0);

				if (!bReadFlag)
				{
#ifdef _DEBUG
					printf("Fail to Read HisData.\n\n");
#endif
				}	
				
			}
			else if(i == 2)
			{
				memset((void *)&stStaRecRead, 0, (unsigned)GetHisDataRecordSize(i));
				bReadFlag = DAT_StorageReadRecord (hRead[i],
							499, 
							&iRecords, 
							&stStaRecRead,
							0); 

				if (!bReadFlag)
				{
#ifdef _DEBUG
					printf("Fail to Read Stastics Data.\n\n");
#endif					
				}
#ifdef _DEBUG
					//printf("Statistics data iSignalID is %d\n", stStaRecRead.iSignalID);
#endif				
			}
			else
			{
				memset((void *)&stCtrlEead, 0, (unsigned)GetHisDataRecordSize(i));
				bReadFlag = DAT_StorageReadRecord (hRead[i],
							499, 
							&iRecords, 
							&stCtrlEead,
							0);
				if (!bReadFlag)
				{
#ifdef _DEBUG
					printf("Fail to Read Control Command.\n\n");
#endif
				
				}
#ifdef _DEBUG
				if (iCounter%10 == 0)
				{
					printf("Current Control Command varControlVal.fValue is %f\n\n", 
						stCtrlEead.varControlVal.fValue);
				}
#endif
			}

			RunThread_Heartbeat(hCurrentThread);

			DAT_StorageClose(hRead[i]);
			hRead[i] = NULL;
		}
/*
		if (iCounter == 500)
		{
			break;
		}
		*/
	}	


	for (i = 0; i < 4; i++)
	{        
		DAT_StorageClose(hWrite[i]);
#ifdef _DEBUG
		printf("Closing hWrite[%d].\n", i);
#endif
	}
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//2004/12/23  Test DAT_StorageSearchRecord().Thread relevant functions
//Start test data management module
/*
char *GetHisDataName(IN int iDataType)
{
	const char *pszHisDataName[] =
	{
		HIS_ALARM_LOG,
		HIS_DATA_LOG,
		STAT_DATA_LOG,		
		CTRL_CMD_LOG
	};

	return (char *)pszHisDataName[iDataType];
}

//Historical Signal Data Record
struct tagHisDataTestRecord
{
	int				iEquipID;						// Equipment ID 
	int				iSignalID;						// Signal ID
	VAR_VALUE		varSignalVal;					// Signal Value 
	time_t			tmSignalTime;					// Signal time 
    int				iActAlarmID;					// Actived Alarm ID   
	char			szBuff[108];
 };

typedef struct tagHisDataTestRecord HIS_DATA_TEST_RECORD;


int GetHisDataRecordSize(IN int iDataType)
{
	const int iSizeofHisData[] =
	{
		sizeof(ACTIVE_ALARM_RECORD),
		sizeof(HIS_DATA_TEST_RECORD),
		sizeof(HIS_STAT_RECORD),		
		sizeof(HIS_CONTROL_RECORD)
	};

	return (int)iSizeofHisData[iDataType];
}

static int Pcm_proc(const void *pRecord, const void *pCondition)
{
	HIS_CONTROL_RECORD * pCtrlRec;
	pCtrlRec = (HIS_CONTROL_RECORD *)pRecord;

	if ((pCtrlRec->iEquipID)%50 == *((int *)pCondition))
	{
        return 1;
	}

	return 0;
}

static void TestHisData(void)
{
	int i;
	int iRecords;
	BOOL bExitFlag;
	int	iCounter;

	int iRltSearch;
	BOOL	bWriteFlag, bReadFlag;
	HANDLE hWrite[4], hRead[4];
	Sleep(2000);
	int iSize;

	/////////////////////////////////////////////////////////////
	//Search function arguments definition
	//COMPARE_PROC	pfnCompare; 
	int			    iCondition;
	int				iStartRecordNo; 
	int				iExpectedRecords;
	void			*pBuff; 
	int				iBuffLen;
	HIS_CONTROL_RECORD * pSearch;
	/////////////////////////////////////////////////////////////
	ACTIVE_ALARM_RECORD stAlarm[10], stAlarmRead;
	HIS_DATA_TEST_RECORD	stData[10], stDataRead;
	HIS_STAT_RECORD		stStaRec[10], stStaRecRead;
	HIS_CONTROL_RECORD  stCtrl[10], stCtrlEead;
	
	HANDLE hCurrentThread;


	hCurrentThread = RunThread_GetId(NULL);	
	RunThread_Heartbeat(hCurrentThread);	
	

	for (i = 0; i < 4; i++)
	{
		if (i == 1)
		{
			iSize = 10000;
		}
		else
		{
			iSize = 500;
		}

		hWrite[i] = DAT_StorageCreate (
			GetHisDataName(i), 
			GetHisDataRecordSize(i),
			iSize, 
			1000);
		if (hWrite[i] != NULL)
		{
#ifdef _DEBUG
			printf("GetHisDataRecordSize(i) is %d\n", GetHisDataRecordSize(i));
			printf("Write handle %d has been created successful.\n", hWrite[i]);
#endif			
		}		
	}

	RunThread_Heartbeat(hCurrentThread);		
	
	iCounter = 0;

	for (i = 0; i < 9; i++)
	{	
		stAlarm[i].iAlarmID = iCounter;
		stAlarm[i].iEquipID = 100;
		stAlarm[i].tmStartTime = (time_t)400;
		stAlarm[i].varTrigValue.fValue = 999.9 + iCounter;

		stData[i].iActAlarmID = iCounter;
		stData[i].iEquipID    = iCounter;
		stData[i].iSignalID   = 1009;
		stData[i].tmSignalTime = (time_t)400;
		stData[i].varSignalVal.fValue = 999.9;
		memset(stData[i].szBuff ,0, (unsigned)108);

		stStaRec[i].iEquipID   = 100;
		stStaRec[i].iSignalID  = iCounter;
		stStaRec[i].tmMaxValue = (time_t)400;
		stStaRec[i].tmMinValue = (time_t)400;
		stStaRec[i].tmStatTime = (time_t)400;
		stStaRec[i].varAverageValue.fValue = 0.0;
		stStaRec[i].varCurrentVal.fValue	 = 999.9;
		stStaRec[i].varMaxValue.fValue	 = 999.9;
		stStaRec[i].varMinValue.fValue	 = -999.9;

		stCtrl[i].byControlResult	=	  1;
		stCtrl[i].bySenderType		=	  1;
		stCtrl[i].iEquipID			=	100;
		stCtrl[i].iSignalID		=	1009;
		strcpy(stCtrl[i].szCtrlSenderName, "CONTROL");
		stCtrl[i].tmControlTime =  (time_t)400;
		stCtrl[i].varControlVal.fValue = (float)iCounter;
	}

	Sleep(500);
	
	bExitFlag = FALSE;
	while (!bExitFlag)
	{
        iCounter++;
		Sleep(200);
		//Write data


		//1.Initiate data
		stAlarm[0].iAlarmID	= iCounter;
		stData[0].iEquipID = iCounter;
		stStaRec[0].iSignalID = iCounter;
		stCtrl[0].varControlVal.fValue = (float)iCounter;
		stCtrl[0].iEquipID = iCounter;

		RunThread_Heartbeat(hCurrentThread);

		//2.Write data
		for (i = 0; i < 4; i++)
		{
			if (i == 0)
			{
				if (iCounter%10 == 0)
				{
					bWriteFlag = DAT_StorageWriteRecord (hWrite[i],
							 GetHisDataRecordSize(i), 
							 (void*)&stAlarm[0]);
					if (!bWriteFlag)
					{
#ifdef _DEBUG
						printf("Fail to write Alarm.\n");
#endif
					}
				}
			}
			else if(i == 1)
			{
				if (iCounter%10 == 0)
				{
					bWriteFlag = DAT_StorageWriteRecord (hWrite[i],
							GetHisDataRecordSize(i), 
							 (void*)&stData[0]);
					if (!bWriteFlag)
					{
#ifdef _DEBUG
						printf("Fail to write HisData.\n");
#endif

					}
					RunThread_Heartbeat(hCurrentThread);
				}
			}
			else if(i == 2)
			{
				if (iCounter%10 == 0)
				{
					bWriteFlag = DAT_StorageWriteRecord (hWrite[i],
							GetHisDataRecordSize(i), 
							 (void*)&stStaRec[0]);

					if (!bWriteFlag)
					{
#ifdef _DEBUG
						printf("Fail to write Statistic Data.\n");
#endif

					}
				}
			}
			else
			{
				bWriteFlag = DAT_StorageWriteRecord (hWrite[i],
							 GetHisDataRecordSize(i), 
							 (void*)&stCtrl[0]);
					if (!bWriteFlag)
					{
#ifdef _DEBUG
						printf("Fail to write Control Command.\n\n");
#endif
					}
			}			
		}

		RunThread_Heartbeat(hCurrentThread);


		//Read data and printf data
		//Write data
		
    	iRecords = 1;
		for (i = 0; i < 4; i++)
		{
			hRead[i] = DAT_StorageOpen (GetHisDataName(i));	

			if (i == 0)
			{
				memset((void *)&stAlarmRead, 0, 
					(unsigned)GetHisDataRecordSize(i));
				bReadFlag = DAT_StorageReadRecord (hRead[i],
							499, 
							&iRecords, 
							&stAlarmRead,
							1);

				if (!bReadFlag)
				{
#ifdef _DEBUG
					printf("Fail to Read Alarm.\n\n");
#endif
				}
#ifdef _DEBUG
				//printf("Alarm varTrigValue is %f\n", stAlarmRead.varTrigValue.fValue);
#endif
			}
			else if(i == 1)
			{
			
				iRecords = 1;
				memset((void *)&stDataRead, 0,(unsigned) GetHisDataRecordSize(i));

#ifdef _DEBUG
				if (iCounter%50 == 0)
				{
					printf("EnterDAT_StorageReadRecord iStartRecordNo is %d\n", 
						9999);
				}
#endif
				bReadFlag = DAT_StorageReadRecord (hRead[i],
							9999, 
							&iRecords, 
							&stDataRead,
							0);

				if (!bReadFlag)
				{
#ifdef _DEBUG
					printf("Fail to Read HisData.\n\n");
#endif
				}	
				
			}
			else if(i == 2)
			{
				memset((void *)&stStaRecRead, 0, (unsigned)GetHisDataRecordSize(i));
				bReadFlag = DAT_StorageReadRecord (hRead[i],
							499, 
							&iRecords, 
							&stStaRecRead,
							0); 

				if (!bReadFlag)
				{
#ifdef _DEBUG
					printf("Fail to Read Stastics Data.\n\n");
#endif					
				}			
			}
			else
			{
				////////////////////////////////////////
				//Control start 
				if (iCounter%50 == 0)
				{
				
				memset((void *)&stCtrlEead, 0, (unsigned)GetHisDataRecordSize(i));
				bReadFlag = DAT_StorageReadRecord (hRead[i],
							499, 
							&iRecords, 
							&stCtrlEead,
							0);
				
				iStartRecordNo = 100;
				iExpectedRecords = 5;
				iCondition = 1;
				pBuff = (void *)NEW(HIS_CONTROL_RECORD, iExpectedRecords);

				iRltSearch = DAT_StorageSearchRecord(hRead[i], 
						   (COMPARE_PROC)Pcm_proc, 
						   (void *)&iCondition,
						   &iStartRecordNo, 
						   iExpectedRecords,
						   pBuff, 
						   &iBuffLen,
						   0,
						   1,
						   0);

				pSearch = (HIS_CONTROL_RECORD *)pBuff;
#ifdef _DEBUG
				printf("Read data Control Command varControlVal.fValue is %f\n\n", 
						stCtrlEead.varControlVal.fValue);

				printf("End DAT_StorageSearchRecord.iRltSearch is %d\n"
					"iExpectedRecords is %d, Acutal iBuffLen is %d\n"
					"pSearch->iEquipID is %d\n", iRltSearch, 
					iExpectedRecords, iBuffLen,
					pSearch->iEquipID);

#endif
				DELETE(pBuff);

				if (!bReadFlag)
				{
#ifdef _DEBUG
					printf("Fail to Read Control Command.\n\n");
#endif				
				}		
					
				}
				//Control END
				/////////////////////////////////////////////
			}

			RunThread_Heartbeat(hCurrentThread);

			DAT_StorageClose(hRead[i]);
			hRead[i] = NULL;
		}
	}	


	for (i = 0; i < 4; i++)
	{        
		DAT_StorageClose(hWrite[i]);
#ifdef _DEBUG
		printf("Closing hWrite[%d].\n", i);
#endif
	}
}
*/
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
//2004/12/24
//Only used to test battery test data record
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//2.Thread relevant functions
//Start test data management module
/*
char *GetHisDataName(IN int iDataType)
{
	const char *pszHisDataName[] =
	{
		HIS_ALARM_LOG,
		HIS_DATA_LOG,
		STAT_DATA_LOG,		
		CTRL_CMD_LOG
	};

	return (char *)pszHisDataName[iDataType];
}

//Historical Signal Data Record
struct tagHisDataTestRecord
{
	int				iEquipID;						// Equipment ID 
	int				iSignalID;						// Signal ID
	VAR_VALUE		varSignalVal;					// Signal Value 
	time_t			tmSignalTime;					// Signal time 
    int				iActAlarmID;					// Actived Alarm ID   
	char			szBuff[108];
 };

typedef struct tagHisDataTestRecord HIS_DATA_TEST_RECORD;


int GetHisDataRecordSize(IN int iDataType)
{
	const int iSizeofHisData[] =
	{
		sizeof(ACTIVE_ALARM_RECORD),
		sizeof(HIS_DATA_TEST_RECORD),
		sizeof(HIS_STAT_RECORD),		
		sizeof(HIS_CONTROL_RECORD)
	};

	return (int)iSizeofHisData[iDataType];
}

static int Pcm_proc(const void *pRecord, const void *pCondition)
{
	HIS_DATA_TEST_RECORD * pBatTesRec;
	pBatTesRec = (HIS_DATA_TEST_RECORD *)pRecord;


	if ((pBatTesRec->iEquipID)%50 == *((int *)pCondition))
	{
#ifdef _DEBUG
		printf("pBatTesRec->iEquipID is %d\n", pBatTesRec->iEquipID);
#endif
        return 1;
	}

	return 0;
}

static void TestHisData(void)
{
	int i;
	int iRecords;
	BOOL bExitFlag;
	int	iCounter;

	int iRltSearch;
	BOOL	bWriteFlag, bReadFlag;
	HANDLE hWrite, hRead;
	Sleep(2000);
	int iSize;

	/////////////////////////////////////////////////////////////
	//Search function arguments definition
	//COMPARE_PROC	pfnCompare; 
	int			    iCondition;
	int				iStartRecordNo; 
	int				iExpectedRecords;
	void			*pBuff; 
	int				iBuffLen;
	HIS_DATA_TEST_RECORD * pSearch;
	/////////////////////////////////////////////////////////////

	HIS_DATA_TEST_RECORD  stData;
	
	HIS_DATA_TEST_RECORD stDataRead;

	HANDLE hCurrentThread;


	hCurrentThread = RunThread_GetId(NULL);	
	RunThread_Heartbeat(hCurrentThread);	
	
	iSize = 10000;	

	hWrite = DAT_StorageCreate (
			GetHisDataName(1), 
			GetHisDataRecordSize(1),
			iSize, 
			1000);
	if (hWrite != NULL)
	{
#ifdef _DEBUG
			printf("GetHisDataRecordSize(i) is %d\n", GetHisDataRecordSize(1));
			printf("Write handle %d has been created successful.\n", hWrite);
#endif			
	}		


	RunThread_Heartbeat(hCurrentThread);		
	
	iCounter = 0;

	stData.iActAlarmID = iCounter;
	stData.iEquipID    = iCounter;
	stData.iSignalID   = 1009;
	stData.tmSignalTime = (time_t)400;
	stData.varSignalVal.fValue = 999.9;
	memset(stData.szBuff ,0, (unsigned)108);


	Sleep(500);
	
	bExitFlag = FALSE;

	while (!bExitFlag)
	{
        iCounter++;
		Sleep(200);
		//Write data


		//1.Initiate data
		stData.iEquipID = iCounter;

		RunThread_Heartbeat(hCurrentThread);

		//2.Write data
		if (iCounter%10 == 0)
		{
			bWriteFlag = DAT_StorageWriteRecord (hWrite,
							GetHisDataRecordSize(1), 
							 (void*)&stData);
			if (!bWriteFlag)
			{
#ifdef _DEBUG
				printf("Fail to write HisData.\n");
#endif
			}
			RunThread_Heartbeat(hCurrentThread);
		}
			


		//RunThread_Heartbeat(hCurrentThread);

		//Read data and printf data
		
    	iRecords = 1;
		hRead = DAT_StorageOpen( GetHisDataName(1));
			

		memset((void *)&stDataRead, 0,(unsigned) GetHisDataRecordSize(1));

#ifdef _DEBUG
		if (iCounter%50 == 0)
		{
			printf("EnterDAT_StorageReadRecord iStartRecordNo is %d\n", 
						9999);

			bReadFlag = DAT_StorageReadRecord (hRead,
							9999, 
							&iRecords, 
							&stDataRead,
							0);

			iStartRecordNo = 9500;
			iExpectedRecords = 5;
			iCondition = 1;
			pBuff = (void *)NEW(HIS_DATA_TEST_RECORD, iExpectedRecords);

			iRltSearch = DAT_StorageSearchRecord(hRead, 
						   (COMPARE_PROC)Pcm_proc, 
						   (void *)&iCondition,
						   &iStartRecordNo, 
						   iExpectedRecords,
						   pBuff, 
						   &iBuffLen,
						   0,
						   0,
						   0);

			pSearch = (HIS_DATA_TEST_RECORD *)pBuff;

			printf("End DAT_StorageSearchRecord.iRltSearch is %d\n"
						"iExpectedRecords is %d, Acutal iBuffLen is %d\n"
						"pSearch->iEquipID is %d\n", iRltSearch, 
						iExpectedRecords, iBuffLen,
						pSearch->iEquipID);


			DELETE(pBuff);

			if (!bReadFlag)
			{
				printf("Fail to Read HisData.\n\n");

			}	
		}//icounter%50
#endif				

			RunThread_Heartbeat(hCurrentThread);

			DAT_StorageClose(hRead);
			hRead = NULL;
		
	}//End reading recycle	
     
	DAT_StorageClose(hWrite);
}
*/

/*
////////////////////////////////////////////////////////////////////////////
//2005/01/20
//Only used to test DAT_StorageReadRecords not DAT_StorageReadRecord
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//2.Thread relevant functions
//Start test data management module

static char *Test_GetHisDataName(IN int iDataType)
{
	const char *pszHisDataName[] =
	{
		HIS_ALARM_LOG,
		"TEST_HIDATA.LOG",
		STAT_DATA_LOG,		
		CTRL_CMD_LOG
	};

	return (char *)pszHisDataName[iDataType];
}

//Historical Signal Data Record
struct tagHisDataTestRecord
{
	int				iEquipID;						// Equipment ID 
	int				iSignalID;						// Signal ID
	VAR_VALUE		varSignalVal;					// Signal Value 
	time_t			tmSignalTime;					// Signal time 
    int				iActAlarmID;					// Actived Alarm ID   
	char			szBuff[108];
 };

typedef struct tagHisDataTestRecord TEST_HIS_DATA_TEST_RECORD;


static int Test_GetHisDataRecordSize(IN int iDataType)
{
	const int iSizeofHisData[] =
	{
		sizeof(ACTIVE_ALARM_RECORD),
		sizeof(TEST_HIS_DATA_TEST_RECORD),
		sizeof(HIS_STAT_RECORD),		
		sizeof(HIS_CONTROL_RECORD)
	};

	return (int)iSizeofHisData[iDataType];
}

static int Pcm_proc(const void *pRecord, const void *pCondition)
{
	TEST_HIS_DATA_TEST_RECORD * pBatTesRec;
	pBatTesRec = (TEST_HIS_DATA_TEST_RECORD *)pRecord;


	if ((pBatTesRec->iEquipID)%50 == *((int *)pCondition))
	{
#ifdef _DEBUG
		printf("pBatTesRec->iEquipID is %d\n", pBatTesRec->iEquipID);
#endif
        return 1;
	}

	return 0;
}

static void TestHisData(void)
{
	int i;
	int iRecords;
	BOOL bExitFlag;
	int	iCounter;


	BOOL	bWriteFlag, bReadFlag;
	HANDLE hWrite, hRead;
	Sleep(2000);
	int iSize;

	/////////////////////////////////////////////////////////////
	//Search function arguments definition
	//COMPARE_PROC	pfnCompare; 
	int			    iCondition;
	int				iStartRecordNo; 
	int				iExpectedRecords;


	
	TEST_HIS_DATA_TEST_RECORD			*pBuffer;
	TEST_HIS_DATA_TEST_RECORD			*pSearch;
	/////////////////////////////////////////////////////////////
	TEST_HIS_DATA_TEST_RECORD  stData;
	HANDLE hCurrentThread;

	pBuffer = (TEST_HIS_DATA_TEST_RECORD *)NEW(TEST_HIS_DATA_TEST_RECORD, 4);

	hCurrentThread = RunThread_GetId(NULL);	
	RunThread_Heartbeat(hCurrentThread);	
	
	iSize = 1;	

	hWrite = DAT_StorageCreate (
			Test_GetHisDataName(1), 
			Test_GetHisDataRecordSize(1),
			iSize, 
			1000);
	if (hWrite != NULL)
	{
#ifdef _DEBUG
			printf("GetHisDataRecordSize(i) is %d\n", Test_GetHisDataRecordSize(1));
			printf("Write handle %d has been created successful.\n", hWrite);
#endif			
	}		


	RunThread_Heartbeat(hCurrentThread);		
	
	iCounter = 0;

	stData.iActAlarmID = iCounter;
	stData.iEquipID    = iCounter;
	stData.iSignalID   = 1009;
	stData.tmSignalTime = (time_t)400;
	stData.varSignalVal.fValue = 999.9;
	memset(stData.szBuff ,0, (unsigned)108);


	Sleep(500);
	
	bExitFlag = FALSE;

	while (!bExitFlag)
	{
        iCounter++;
		Sleep(200);
		//Write data


		//1.Initiate data
		stData.iEquipID = iCounter;

		RunThread_Heartbeat(hCurrentThread);

		//2.Write data
		if (iCounter%2000000 == 0)
		{
			bWriteFlag = DAT_StorageWriteRecord (hWrite,
							Test_GetHisDataRecordSize(1), 
							 (void*)&stData);
			if (!bWriteFlag)
			{
#ifdef _DEBUG
				printf("Fail to write HisData.\n");
#endif
			}

			RunThread_Heartbeat(hCurrentThread);
		}
			


		//RunThread_Heartbeat(hCurrentThread);

		//Read data and printf data
		
    	iRecords = 1;
		hRead = DAT_StorageOpen( Test_GetHisDataName(1));
			

		memset((void *)pBuffer, 0,(unsigned)4*Test_GetHisDataRecordSize(1));

#ifdef _DEBUG
		if (iCounter%50 == 0)
		{
			iStartRecordNo = 1;
			iRecords = 1;
			bReadFlag = DAT_StorageReadRecords (hRead,
							&iStartRecordNo, 
							&iRecords, 
							pBuffer,
							0,
							0);

			iExpectedRecords = iRecords;

			printf("iStartRecordNo is %d,iRecords is %d\n", 
				iStartRecordNo, iRecords);
			iCondition = 1;

			pSearch = pBuffer;
			
			
			if (!bReadFlag)
			{
				printf("Fail to Read HisData.\n\n");

			}	
			else
			{
				for (i = 0; i < 4; i++)
				{
					printf("pBuffer[%d]->iEquipID is %d\n", i, pSearch->iEquipID);
					printf("pBuffer[%d]->iSignalID is %d\n", i, pSearch->iSignalID);
					pSearch++;
				}	
			}
		}//icounter%50
#endif				

			RunThread_Heartbeat(hCurrentThread);

			DAT_StorageClose(hRead);
			hRead = NULL;
		
	}//End reading recycle	
     
	DAT_StorageClose(hWrite);
}
*/
/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : app_service.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-10 19:50
 *  VERSION  : V1.00
 *  PURPOSE  : manage(load/unload) the application services.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "cfg_model.h"

extern SERVICE_MANAGER		g_ServiceManager;

#define LOCK_SERVICE_MGR()		Mutex_Lock(g_ServiceManager.hLock, WAIT_INFINITE)
#define UNLOCK_SERVICE_MGR()	Mutex_Unlock(g_ServiceManager.hLock)


/*==========================================================================*
 * FUNCTION : Service_Start
 * PURPOSE  : load and create an service.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT APP_SERVICE *pService : 
 * RETURN   : BOOL : FALSE for failure.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 18:34
 *==========================================================================*/
BOOL Service_Start(IN OUT APP_SERVICE *pService)
{
	const char *pszSymName[] =
	{
		SERVICE_INIT_PROC_NAME,		// "ServiceInit"
		SERVICE_MAIN_PROC_NAME,		// "ServiceMain"
		SERVICE_CONFIG_PROC_NAME,	// "ServiceConfig"
	};

	HANDLE	*pfnProc[ITEM_OF(pszSymName)] =
	{
		(HANDLE *)&pService->pfnServiceInit,
		(HANDLE *)&pService->pfnServiceMain,
		(HANDLE *)&pService->pfnServiceConfig,
	};

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"Starting service \"%s\"(%s)...\n",
		pService->szServiceName,
		pService->szServiceLib);

	// check the service is running or not.
	if (Service_IsRunning(pService))
	{
		AppLogOut(SERVICE_MGR, APP_LOG_ERROR,
			"Errors on trying to restart a running service %s(%s).\n",
			pService->szServiceName,
			pService->szServiceLib);
		return FALSE;
	}

	// lock at first.
	LOCK_SERVICE_MGR();

	// load service lib
	pService->hServiceLib = LoadDynamicLibrary(
		pService->szServiceLib,
		ITEM_OF(pszSymName),
		pszSymName,
		pfnProc,
		FALSE);

	if (pService->hServiceLib == NULL)
	{
		AppLogOut(SERVICE_MGR, APP_LOG_ERROR,
			"Fails on loading application service \"%s\"(%s).\n",
			pService->szServiceName,
			pService->szServiceLib);

		UNLOCK_SERVICE_MGR();
		return FALSE;
	}

	if (pService->pfnServiceMain == NULL)
	{
		AppLogOut(SERVICE_MGR, APP_LOG_ERROR,
			"Fails on finding ServiceMain in service \"%s\"(%s).\n",
			pService->szServiceName,
			pService->szServiceLib);

		UnloadDynamicLibrary(pService->hServiceLib);

		pService->hServiceLib    = NULL;
		pService->pfnServiceInit = NULL;
		pService->pfnServiceConfig = NULL;

		UNLOCK_SERVICE_MGR();
		return FALSE;
	}

	// set the running flag for all types of services
	pService->args.nQuitCommand = SERVICE_CONTINUE_RUN;

	// init the serice thread.
	if (pService->pfnServiceInit != NULL)
	{
		pService->args.pReserved = 
			pService->pfnServiceInit(pService->args.argc, pService->args.argv);
	}

	// the service need create a thread to run
	if (pService->nRunType == SERVICE_RUN_AS_TASK)
	{
		// create the run thread
		pService->hServiceThread = RunThread_Create(
			pService->szServiceName,
			(RUN_THREAD_START_PROC)pService->pfnServiceMain,
			(void *)&pService->args,
			&pService->args.dwExitCode,
			RUN_THREAD_FLAG_HAS_MSG);	// create with msg queue.

		if (pService->hServiceThread == NULL)
		{
			AppLogOut(SERVICE_MGR, APP_LOG_ERROR,
				"Fails on creating thread for service \"%s\"...\n",
				pService->szServiceName);
			// do NOT cleanup the  opened service.
			UNLOCK_SERVICE_MGR();
			return FALSE;
		}
	}

	//else the service is run by service manager periodically
	
	pService->bServiceRunning = TRUE;

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,//APP_LOG_INFO,
		"Service [%s] started as %s. tid=%p.\n",
		pService->szServiceName,
		(pService->nRunType == SERVICE_RUN_AS_TASK) ? "task" : "dummy",
		pService->hServiceThread);

	UNLOCK_SERVICE_MGR();

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Service_PostQuitMessage
 * PURPOSE  : post quit message to the service. 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT APP_SERVICE  *pService : 
 * RETURN   : BOOL :TRUE for successful. 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 17:22
 *==========================================================================*/
BOOL Service_PostQuitMessage(IN OUT APP_SERVICE *pService)
{
	// 1. set the exit flag for all types of services
	pService->args.nQuitCommand = SERVICE_STOP_RUNNING;
	
	if (pService->hServiceThread == NULL)
	{
		return TRUE;
	}

	//2.stop and wait for thread quit.
	RunThread_PostQuitMessage(pService->hServiceThread);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Service_Stop
 * PURPOSE  : stop a service and release the resource of the service
 * CALLED BY: 
 * ARGUMENTS: IN OUT APP_SERVICE *pService      : 
 *            IN int     nTimeToWaitServiceQuit : wait service quit in ms.
 *                       The thread will be killed if timeout
 * RETURN   : int : ERR_THREAD_OK for OK, 
 *                  ERR_THREAD_STILL_RUNNING for thread does not stopped,
 *                  ERR_THREAD_KILLED for thread is terminated by force.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 18:45
 *==========================================================================*/
int Service_Stop(IN APP_SERVICE *pService,
						  IN int nTimeToWaitServiceQuit) 
{
	int		rc = ERR_THREAD_OK;

	LOCK_SERVICE_MGR();

	if (pService->bServiceRunning)
	{
		// 1. set the exit flag for all types of services
		pService->args.nQuitCommand = SERVICE_STOP_RUNNING;

		// call the service main to make the service to quit.
		if (pService->nRunType == SERVICE_RUN_AS_DUMMY)
		{
			pService->args.dwExitCode = 
				pService->pfnServiceMain(&pService->args);
		}
		else if (pService->hServiceThread != NULL)
		{
			//2.stop and wait for thread quit.
			// kill service if the service is no response
			rc = RunThread_Stop(pService->hServiceThread, 
				nTimeToWaitServiceQuit, TRUE);

			pService->hServiceThread = NULL;
		}

		pService->bServiceRunning = FALSE;
	}

	// 3, cleanup
	if (pService->hServiceLib != NULL)
	{
#ifdef _DEBUG
		// DO NOT unload the service lib when in debugging mode.
		// if unload the lib, the mem-leaks of the service can NOT be shown,
		// and the running threads can NOT be found. maofuhua, 2005-1-29
#warning "The shared library is NOT released in debug version."
#else
		UnloadDynamicLibrary(pService->hServiceLib);
#endif
		pService->pfnServiceInit = NULL;
		pService->pfnServiceMain = NULL;
		pService->pfnServiceConfig = NULL;
	}
	
	UNLOCK_SERVICE_MGR();

    return rc;
}

/*==========================================================================*
 * FUNCTION : *ServiceManager_GetService
 * PURPOSE  : get the service infomation by 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int   nGetByFlag      :
 *            IN void  *pszServiceFlag : if nGetByFlag is SERVICE_GET_BY_NAME,
 *                                          pszServiceFlag is the Service Name(char *),
 *										 if nGetByFlag is SERVICE_GET_BY_LIB(char *),
 *                                          pszServiceFlag is the shared lib name,
 *										 if nGetByFlag is SERVICE_GET_BY_IDX,
 *                                          pszServiceFlag is index of the array(int)
 *								         if SERVICE_GET_BY_HANDLE, the pszServiceFlag
 *                                          is the service HANDLE(thread ID)
 * RETURN   : APP_SERVICE : NULL for not found, else the service address.
 * COMMENTS : do NOT change any field of the service!!!
 * CREATOR  : Mao Fuhua(Frnak)         DATE: 2005-01-07 15:29
 *==========================================================================*/
APP_SERVICE *ServiceManager_GetService(IN int nGetByFlag, IN void *pszServiceFlag)
{
	SERVICE_MANAGER *pServiceMgr = &g_ServiceManager;
	APP_SERVICE *pService;
	int			i;

	pService = pServiceMgr->pService;

	if (nGetByFlag == SERVICE_GET_BY_NAME)
	{
		for (i = 0; i < pServiceMgr->nService; i++, pService++)
		{
			if (strcmp(pService->szServiceName, 
				(const char *)pszServiceFlag) == 0)
			{
				return pService;
			}
		}
	}

	else if (nGetByFlag == SERVICE_GET_BY_LIB)
	{
		for (i = 0; i < pServiceMgr->nService; i++, pService++)
		{
			if (strcmp(pService->szServiceLib, 
				(const char *)pszServiceFlag) == 0)
			{
				return pService;
			}
		}
	}

	else if (nGetByFlag == SERVICE_GET_BY_IDX)
	{
		if ( ((int)pszServiceFlag >= 0) && 
			((int)pszServiceFlag < pServiceMgr->nService))
		{
			return &pService[(int)pszServiceFlag];
		}
	}

	else if (nGetByFlag == SERVICE_GET_BY_HANDLE)
	{
		for (i = 0; i < pServiceMgr->nService; i++, pService++)
		{
			if (pService->hServiceThread == (HANDLE)pszServiceFlag)
			{
				return pService;
			}
		}
	}

	return NULL;
}


/*==========================================================================*
 * FUNCTION : Service_IsRunning
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN APP_SERVICE  *pService : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-24 14:44
 *==========================================================================*/
BOOL Service_IsRunning(IN APP_SERVICE *pService)
{
	BOOL	bIsRunning = FALSE;

	if (pService != NULL)
	{
		LOCK_SERVICE_MGR();

		if (pService->nRunType == SERVICE_RUN_AS_TASK)
		{
			if ((pService->hServiceThread != NULL) &&
				THREAD_IS_RUNNING(pService->hServiceThread))
			{
				bIsRunning = TRUE;
			}
		}
		else if (pService->nRunType == SERVICE_RUN_AS_DUMMY)
		{
			bIsRunning = pService->bServiceRunning;
		}

		UNLOCK_SERVICE_MGR();
	}

	return bIsRunning;
}

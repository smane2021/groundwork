/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : test_applog.c
*  CREATOR  : LIXIDONG                 DATE: 2004-10-22 09:03
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#include "stdsys.h"
#include "public.h"


#define NUM 1000
HANDLE hWriting[NUM];

void WriteLogTest(char * szName)
{
	int kk;

	kk = 0;
	do
	{
		kk++;

		printf("%s:%d,This is undefined number.\n",
			szName, kk);
		AppLogOut("WriteLogTest1", kk%3, 
			"%s:%d,Log info.1EES is renamed Vertiv Tech Energy Systems.2EES is renamed Vertiv Tech Energy Systems."
			"3EES is renamed Vertiv Tech Energy Systems.4EES is renamed Vertiv Tech Energy Systems."
			"5EES is renamed Vertiv Tech Energy Systems.6EES is renamed Vertiv Tech Energy Systems."
			"7EES is renamed Vertiv Tech Energy Systems.8EES is renamed Vertiv Tech Energy Systems.",
			szName, kk );		

		Sleep(10);		
	} while (kk < 10000);

}


/*==========================================================================*
* FUNCTION : test_applog     
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2004-10-19 12:57
*==========================================================================*/

int main(int argc, char *argv[])
{
	int i;
	char szName[NUM][16];
	//WriteLogTest1();

	int nThreads;

	if( argc != 2)
	{
		printf("%s : threads\n", argv[0]);
		return 0;
	}

	nThreads = atoi(argv[1]);

	for (i = 0; i < nThreads; i++)
	{
		sprintf(szName[i], "Thread%03d",i);

		hWriting[i] = RunThread_Create(szName[i], 
			(RUN_THREAD_START_PROC)WriteLogTest,
			szName[i],
			NULL,
			0);

	}



	printf("Press Enter to EXIT\n");
	getchar();

	return 1;

}







/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : get_log.c
 *  CREATOR  : LIXIDONG                 DATE: 2005-01-31 19:01
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include <sys/types.h>
#include <stdarg.h>

#include "stdsys.h"
#include "public.h"
#include "../../log/apprunlog.h"

#include <sys/mount.h>
#include <linux/mtd/mtd.h>

#ifdef _DEBUG
//#define _RUN_LOG_WRITE_   1
#endif


//This flag is used to display all running system log
//#define		_DEBUG_SHOW_ALL_LOG		1

//Erase data sector in flash
//Used in x86 environment, debug
int EraseLogSector_x86(IN int iStartSectorNo, IN int iSectors);
//Used in ppc acutual environment
int EraseLogSector_ppc(IN int iStartSectorNo, IN int iSectors);

#ifdef _HAS_FLASH_MEM      //ppc:   MTD5_NAME	"/dev/mtd5"	
#define EraseLogSector(a,b)       EraseLogSector_ppc(a,b)
#else
#define EraseLogSector(a,b)       EraseLogSector_x86(a,b)
#endif


//File name definiton
#define   MAX_LOG_ERR_TIMES      3
//File name definiton
//old: #define APP_LOG_FILE			"/opt/ACU/log/ACU.log"
//old: #define APP_BAK_LOG_FILE      "/opt/ACU/log/ACUBAK.log"


//new log path and name, by maofuhua, 2004-11-12
#define BASIC_LOG_FILE			"ACU.log"
#define BASIC_BAK_LOG_FILE		"ACU.bak.log"

#define SUB_DIR_LOG_FILE		"log"

#ifndef ENV_ACU_ROOT_DIR
#define ENV_ACU_ROOT_DIR		"ACU_ROOT_DIR"	// to use shell env variable.
#endif

static char APP_LOG_FILE[MAX_FILE_PATH] = BASIC_LOG_FILE;

BOOL g_bEnableAppLogOut = TRUE;

#ifndef K_BYTES
#define K_BYTES 1024
#endif

#define APP_LOG_SIZE        (100*K_BYTES)

// commout the next line to use the Mutex_xxx defined in public module.
#define  __LOG_PTHREAD_LOCK	1  // use pthread_mutex.

#ifdef __LOG_PTHREAD_LOCK
//The handle of the mutex for write log,only writing by one user once
static pthread_mutex_t	hWriteFlashLogLock = PTHREAD_MUTEX_INITIALIZER;

#define LOCK_FLASH_LOG_INIT()		  pthread_mutex_init(&hWriteFlashLogLock, NULL)
#define LOCK_FLASH_LOG_SYNC()         pthread_mutex_lock(&hWriteFlashLogLock)
#define UNLOCK_FLASH_LOG_SYNC()       pthread_mutex_unlock(&hWriteFlashLogLock)
#define DESTROY_FLASH_LOG_SYNC()      pthread_mutex_destroy(&hWriteFlashLogLock)

#else	// use pub mutex lock
static HANDLE	hWriteFlashLogLock = NULL;

#define LOCK_FLASH_LOG_INIT()		  (hWriteFlashLogLock = Mutex_Create(TRUE))
#define LOCK_FLASH_LOG_SYNC()		   Mutex_Lock(hWriteFlashLogLock, (DWORD)-1)
#define UNLOCK_FLASH_LOG_SYNC()        Mutex_Unlock(hWriteFlashLogLock)
#define DESTROY_FLASH_LOG_SYNC()       Mutex_Destroy(hWriteFlashLogLock)
#endif

 static int LOG_WriteOneRecord(IN void *pRecordBuff, 
						   IN int iRecordSize, 
						   IN DWORD dwRecordID, 
						   IN long iOffset);

 static BOOL LOG_WritePosControl(void);
///////////////////////////////////////////////////////////////////////////////
//Only used to read log records
LOG_RECORD_SET_TRACK *pHisTrack = NULL;
//FILE				 *pHisxFile  = NULL;//Change to int
int				     g_iHisFile = (-1);

//Only used to write log records track
LOG_RECORD_SET_TRACK *pWriteTrack = NULL;
//FILE				 *pWritexFile  = NULL;//Change to int
int				     g_iWriteFile = (-1);
long				 g_iLogOffset = 0;

static int	 g_iLogFlashOpenState    = ERR_LOG_OK;

static void AppLog_Cleanup(void)
{
	DESTROY_FLASH_LOG_SYNC();
	LOG_ClearWriteFlashLog();
}


#define FULL_LOG_HEAD_MSG	\
    "ACU runtime log information. Log text format is:\n"\
    "TYPE(INFO/WARN/ERR) TIME(yy-mm-dd hh:mm:ss in GMT) SoureTaskName#TaskID: The log message.\n\n"

// write head msg of log file. maofuhua, 2004-11-12
static BOOL AppLog_WriteHeader(void)
{
	FILE *fp = fopen( APP_LOG_FILE, "r" );

	if (fp != NULL)	// the log file exists, return. do not write head.
	{
		fclose(fp);
		return FALSE;
	}

	// write head msg
	fp = fopen( APP_LOG_FILE, "a+" );
	if (fp == NULL)
	{
		return FALSE;
	}

	fprintf( fp, "%s", FULL_LOG_HEAD_MSG );
	
	fclose(fp);

	return TRUE;
}

// added by maofuhua, 2004-11-12
static void AppLog_Init_To_File(void)
{
#ifdef _DEBUG
	if (getenv(ENV_ACU_ROOT_DIR) == NULL)
	{
		TRACEX("The env variable \"%s\" for the ACU root dir "
			"is not set yet, will use current dir as default.\n",
			ENV_ACU_ROOT_DIR);
	}
#endif //_DEBUG

	// add initialization log name, maofuhua, 2004-11-12
	MakeFullFileName(APP_LOG_FILE, 
		getenv(ENV_ACU_ROOT_DIR),
		SUB_DIR_LOG_FILE, 
		BASIC_LOG_FILE);

#ifdef _DEBUG_APP_LOG
	TRACEX("The full name of log file is %s.\n",
		APP_LOG_FILE);
#endif //_DEBUG_APP_LOG
// end add name.

	LOCK_FLASH_LOG_INIT();

	atexit(AppLog_Cleanup);

	// write head msg to log file.
	AppLog_WriteHeader();
}


static void AppLog_Backup(void)
{
#define LINUX_mv_CMD_FORMAT		"mv -f \"%s\" \"%s\""

	char szBakLogFile[MAX_FILE_PATH];
	char szShellCmd[MAX_FILE_PATH*2 + sizeof(LINUX_mv_CMD_FORMAT)];

	// mv file.
	// new backup file method, maofuhua, 2004-11-12
	MakeFullFileName(szBakLogFile, 
		getenv(ENV_ACU_ROOT_DIR),
		SUB_DIR_LOG_FILE, 
		BASIC_BAK_LOG_FILE);

	//old: system( "mv -f " APP_LOG_FILE " " APP_BAK_LOG_FILE );

	// make cmd
	snprintf(szShellCmd, sizeof(szShellCmd), LINUX_mv_CMD_FORMAT,
		APP_LOG_FILE, szBakLogFile);


#ifdef _DEBUG_APP_LOG
	TRACEX("Backup log file %s to bak file %s...\n"
		"SHELL CMD: %s\n",
		APP_LOG_FILE, szBakLogFile, szShellCmd);
#endif //_DEBUG_APP_LOG

	// exec cmd
	//system(szShellCmd);
	_SYSTEM(szShellCmd);

	// trying to write head msg to log file.
	if (!AppLog_WriteHeader())	// head is not written, the log file exists.
	{
		printf("AppLogOut: Fails on backup log file by executing:\n%s\n"
			"The log file %s will be truncated.\n",
			szShellCmd, APP_LOG_FILE);

		truncate(APP_LOG_FILE, sizeof(FULL_LOG_HEAD_MSG)-1);	// keep head info.
	}
}
/*==========================================================================*
 * FUNCTION : AppLog_Init_New
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   BOOL : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-31 11:06
 *==========================================================================*/
static BOOL AppLog_Init_New(void)
{
	BOOL bIniSucceed;

	LOCK_FLASH_LOG_INIT();

	bIniSucceed = LOG_IniWriteFlashLog();
	//Clear handle
	atexit(AppLog_Cleanup);

	return bIniSucceed;
}
#define _DEBUG_SHOW_ALL_LOG			1
/*==========================================================================*
 * FUNCTION : AppLogOut
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *pszTaskName : 
 *            int         iLogLevel    : 
 *            const char  *pszFormat   : 
 *                        ...          : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-31 11:09
 *==========================================================================*/
void AppLogOut( const char *pszTaskName, int iLogLevel, 
			   const char *pszFormat, ...) 
{	
	static BOOL bLockInitialized = FALSE;	// added by maofuhua. 04/10/26

	va_list     arglist;
    char        szTaskBuf[128];// increase it
	char        szRecordBuf[256];// increase it
	char		szThreadName[20];
    char        szTime[32];
	char		*pszInfoLevel;
	int			nLen;
	int			iRecordSize;
	int			iResult;
	int			iEmptyCount, iWasteCount;
	DWORD		dwRecordID;
	char	   szDisplayTaskName[12];

	if (!g_bEnableAppLogOut)
	{
		return;
	}
	
	if (g_iLogFlashOpenState != ERR_LOG_OK)
	{
		return;   //directly return
	}

	iEmptyCount = 0;
	iWasteCount = 0;

	memset(szTaskBuf, 0, 128);
	memset(szRecordBuf, 0, 256);

	// register the at-exit function.
	if (!bLockInitialized)
	{
		AppLog_Init_New();	//Prevent sys failing initiate	
		bLockInitialized = TRUE;
	}

	//Too many running log, so not record info level log

#ifndef _DEBUG_SHOW_ALL_LOG	
	if (iLogLevel == APP_LOG_UNUSED)
	{
		return;
	}
#endif


	//Lock  
	LOCK_FLASH_LOG_SYNC();

    //ClearWDT(); 
	iRecordSize = sizeof(LOG_RECORD_SIZE);

  	switch(iLogLevel)
	{
	case APP_LOG_INFO:
		pszInfoLevel = "INFO";
		break;
	case APP_LOG_WARNING:
		pszInfoLevel = "WARN";
		break;
	case APP_LOG_ERROR:
		pszInfoLevel = "ERR ";
		break;
	case APP_LOG_MILESTONE:
		pszInfoLevel = "INFO";
		break;
	case APP_LOG_UNUSED:
		pszInfoLevel = "DEBUG";
		break;
	default:
		pszInfoLevel = "????";	// undefined
		break;	
	}

	// get current time and convert to YYYY-MM-DD hh:mm:ss.
	TimeToString(time(NULL), TIME_CHN_FMT, szTime, sizeof(szTime));

	va_start(arglist, pszFormat);
    nLen = vsnprintf(szTaskBuf, sizeof(szTaskBuf), pszFormat, arglist);
	va_end(arglist);

	// discard the end LF "\n"
	if(szTaskBuf[nLen-1] == '\n')	// added by maofuhua, 2004-11-04
	{
		szTaskBuf[nLen-1] = 0; 
	}

	// format log info as:		 //maofuhua 2004-11-12
	// see the macro: LOG_HEAD_MSG
	//"Type yy-mm-dd hh:mm:ss Source task#Task ID: Log message ..."
	RunThread_GetName(NULL, szThreadName, LEN_RUN_THREAD_NAME);
	if(!szThreadName[0])
	{
		strncpy(szThreadName, "MAIN_THREAD", 12);
	}
	strncpyz(szDisplayTaskName, pszTaskName, sizeof(szDisplayTaskName));
	snprintf(szRecordBuf, 
		255,
		"%-5s %s %-12s %08x %-12s: %s\n", 
		pszInfoLevel,
        &szTime[2],		// lose the centry of year. from yyyy to yy.
        szDisplayTaskName,
        (unsigned int)RunThread_GetId(NULL),
		szThreadName,
        szTaskBuf);

_ReWriteLog:

	dwRecordID = pWriteTrack->dwCurrRecordID;
	if (pWriteTrack->bSectorEffect)        
	{
		iResult = LOG_WriteOneRecord((void *)szRecordBuf, 
			iRecordSize, 
			dwRecordID, 
			g_iLogOffset);	
	}
	else    //Flash error,can't write log to flash
	{
		iResult = ERR_LOG_OK;
	}

	if (iResult == ERR_LOG_WRI_EMPTY)
	{
		iEmptyCount++;
		iWasteCount = 0;

		if (iEmptyCount < MAX_LOG_ERR_TIMES)
		{
			goto _ReWriteLog;
		}
		else
		{
			if (pWriteTrack->iCurrWritePos == 0)
			{
				EraseLogSector(pWriteTrack->byCurrSectorNo, 1);                
			}
			else
			{
				LOG_WritePosControl();
			}			
			//Let it be
		}
	}
	else if(iResult == ERR_LOG_WRI_WASTE)
	{
		LOG_WritePosControl();

		iEmptyCount = 0;
		iWasteCount++;
		if (iWasteCount < MAX_LOG_ERR_TIMES)
		{
			goto _ReWriteLog;
		}
	}
	else
	{
		LOG_WritePosControl();
		iEmptyCount = 0;
		iWasteCount = 0;
	}	

	//unlock
	UNLOCK_FLASH_LOG_SYNC();
  
	return ;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//From log_read.c
///////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : LOG_SectorIsEmpty
 * PURPOSE  : Check sector
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iPhySectorNo : 
 * RETURN   : BOOL : TRUE:Current Sector has no data. 
 *					 FALSE:some data in current sector
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-18 17:29
 *==========================================================================*/
static BOOL LOG_SectorIsEmpty( int iPhySectorNo)
{
	FILE * fp;
	long iOffset;
	DWORD dwData;
	int i, iCounts;

    fp = fopen(MTD5_NAME,"rb");

	if (fp == NULL)
	{
		return FALSE;
	}

	iOffset = (iPhySectorNo - 1) * SIZE_PER_LOG_SECTOR;

	fseek(fp, iOffset, SEEK_SET);
	iCounts = SIZE_PER_LOG_SECTOR/sizeof(DWORD);
	for (i = 0; i < iCounts; i++)
	{
		fread(&dwData, (unsigned)sizeof(DWORD), 1, fp);
		if (dwData != 0xffffffff)
		{
			fclose(fp);
			return FALSE;
		}
	}

	fclose(fp);
	return TRUE;

}

///////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : LOG_CRC16 calculation
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: unsigned char   *Msg : 
 *            unsigned short  len  : 
 * RETURN   : unsigned short : 
 * COMMENTS : Copy
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 11:15
 *==========================================================================*/
static unsigned short LOG_CRC16(IN unsigned char *Msg, IN unsigned short len)
{
	unsigned short CRC;
    unsigned short i,temp;

	CRC = 0xFFFF;

    while(len--)
    {
        CRC = CRC^(*Msg++);
        for(i=0;i++<8;)
        {
            if(CRC&0x0001)
                CRC = (CRC>>1)^0xa001;
            else
                CRC>>=1;
        }
    }

    temp = CRC&0xFF;
    CRC = ((CRC>>8)&0xFF)+(temp<<8);  
	
    return(CRC);
}

/*==========================================================================*
 * FUNCTION : LOG_RecordSpaceIsEmpty
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int     iRecordSize : 
 *            void *  pRecordBuff       : 
 * RETURN   : BOOL : TRUE:Current Record Space has no data. 
 *					 FALSE:some data in Record Space
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-18 17:52
 *==========================================================================*/
static BOOL LOG_RecordSpaceIsEmpty(int iRecordSize, void * pRecordBuff)
{
	BYTE byUnit;
	BYTE *pUnit;
	
	int i;
	pUnit = (BYTE*)pRecordBuff;
	
	for (i = 0; i < iRecordSize; i++)
	{
		byUnit = *(pUnit + i);
		if (byUnit != 0xff)
		{
			pUnit = NULL;
			return FALSE;
		}
	}

	pUnit = NULL;
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : LOG_SearchCurrPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE  bySectorNo           : 
 *            int   iActualStoreRecordSize          : 
 *            int   iMaxRecordsPerSector : 
 * RETURN   : int : -1:file can't be opened.Other:current position
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-14 12:57
 *==========================================================================*/
 static int LOG_SearchCurrPos(IN int iSectorNo,
						  IN int iActualStoreRecordSize, 
						  IN int iMaxRecordsPerSector, 
					      OUT DWORD *pCurrentID)

{
	int		iCurrRecordPos = 0;   //The position in current sector
	long	iOffset, iOffsetOld;
	int		iBottomPos, iTopPos, iMidPos;
	BOOL    bBotPosEmpty, bMidPosEmpty;
	void	*pRecordBuff;	

	//iRecordSize = iRecordSize + LOG_RECORD_ID_BYTES + LOG_CRC16_BYTES;
	iOffsetOld = (iSectorNo - 1) * SIZE_PER_LOG_SECTOR;

	pRecordBuff = (void *)NEW(char, iActualStoreRecordSize);
	if(pRecordBuff == NULL)
	{
		return iCurrRecordPos;
	}

	iBottomPos = 0;
	//*pCurrentID = 1;
	*pCurrentID = 0;
	iTopPos = iMaxRecordsPerSector - 1;
	iCurrRecordPos = 0;
	bBotPosEmpty = FALSE;
	bMidPosEmpty = FALSE;
	
	do
	{
		iOffset = iOffsetOld + iBottomPos * iActualStoreRecordSize;
		lseek(g_iHisFile, iOffset, SEEK_SET);
		read(g_iHisFile, (void *)pRecordBuff, (unsigned)iActualStoreRecordSize);
		bBotPosEmpty = LOG_RecordSpaceIsEmpty(iActualStoreRecordSize, pRecordBuff);

		if(bBotPosEmpty)
		{
			iCurrRecordPos = iBottomPos;
			break;
		}

		iMidPos = (iBottomPos + iTopPos)/2;
		iOffset = iOffsetOld + iMidPos * iActualStoreRecordSize;
		lseek(g_iHisFile, iOffset, SEEK_SET);
		read(g_iHisFile, (void *)pRecordBuff, (unsigned)iActualStoreRecordSize);
		bMidPosEmpty = LOG_RecordSpaceIsEmpty(iActualStoreRecordSize, pRecordBuff);
		
		if(bMidPosEmpty)
		{
			iTopPos = iMidPos - 1;
		}
		else
		{
			iBottomPos = iMidPos + 1;
			*pCurrentID = *((DWORD *)pRecordBuff);
		}
	
	} while((iBottomPos <= iTopPos) && (iCurrRecordPos == 0));
	
	iCurrRecordPos = iBottomPos;

	if (pRecordBuff != NULL)
	{
		DELETE(pRecordBuff);
	}

	return iCurrRecordPos;
}

/*==========================================================================*
 * FUNCTION : LOG_ReadOneRecord
 * PURPOSE  : Read one record and check it effectively or not.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int     iHisFile
 *			  long    iOffset            : 
 *            void    *pRecordBuff       : Output record data
 *            int     iRecordSize : It is the data size 
 *									Not include LOG_RECORD_ID_BYTES,CRC16_BYTES
 * RETURN   : int :   0: Succeed
 *					  1: Record is empty;
 *					  2: Fail to read
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-17 17:32
 *==========================================================================*/
static int LOG_ReadOneRecord(IN  int iHisFile, 
						  IN  long iOffset, 
				          OUT void *pRecordBuff,
				          IN  int  iRecordSize)
{
	unsigned short iOldCheckNo;
	unsigned short iCalCheckNo;
	int iActualRecordSize;
	void * pRdBuff;
	long iCurrPos;

	iActualRecordSize = iRecordSize + LOG_RECORD_ID_BYTES + LOG_CRC16_BYTES;
	pRdBuff = (void *)NEW(char, iActualRecordSize);
	if(pRdBuff == NULL)
	{
		return 1;
	}

	iCurrPos = iOffset;	

	lseek(iHisFile, iCurrPos, SEEK_SET);
	
	read(iHisFile, pRdBuff, (unsigned)iActualRecordSize);
	//Current record is empty
    if(LOG_RecordSpaceIsEmpty(iActualRecordSize, pRdBuff))
	{				
		DELETE(pRdBuff);
		return 1;
	}
	
	//Output record data
	memmove(pRecordBuff, (void *)((BYTE *)pRdBuff + LOG_RECORD_ID_BYTES), 
		(unsigned)iRecordSize);

	iOldCheckNo = *((unsigned short *)((char *)pRdBuff 
		+ (iActualRecordSize - LOG_CRC16_BYTES)));	

	//Check number is produced include LOG_RECORD_ID_BYTES should be checked
	//It is same as writing record
	iCalCheckNo = LOG_CRC16((unsigned char *)(pRdBuff), 
		(unsigned short)(iActualRecordSize - LOG_CRC16_BYTES));

	if (iOldCheckNo != iCalCheckNo)       //Error
	{
		lseek(iHisFile, iCurrPos, SEEK_SET);

		read(iHisFile, pRdBuff, (unsigned)iActualRecordSize);

		iOldCheckNo = *((unsigned short*)((char *)pRdBuff 
			+ iActualRecordSize - LOG_CRC16_BYTES));

		iCalCheckNo = LOG_CRC16((unsigned char *)(pRdBuff), 
			(unsigned short)(iActualRecordSize - LOG_CRC16_BYTES));

		//Output record data
		memmove(pRecordBuff, (void*)((char *)pRdBuff + LOG_RECORD_ID_BYTES), 
			(unsigned)iRecordSize);

		if (iOldCheckNo != iCalCheckNo) //Error again
		{	
			DELETE(pRdBuff);
			return 2;
		}
	 }

	DELETE(pRdBuff);
	return 0;    //Succeed
}

/*==========================================================================*
 * FUNCTION : LOG_ReadStaticParameter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-31 19:23
 *==========================================================================*/
static BOOL LOG_IniStaticParas(void)
{
	if (pHisTrack == NULL)
	{
		return FALSE;
	}

	strcpy(pHisTrack->DataName, ACU_RUNNING_LOG);
	pHisTrack->iDataRecordIDIndex		= 0;
	pHisTrack->iMaxRecords				= MAX_LOG_RECORD_COUNT;
	pHisTrack->iRecordSize				= sizeof(LOG_RECORD_SIZE);
	///////////////////////////////////
	//Add erasing sector pre-process function
	pHisTrack->pfn_preProcess			= NULL;
	pHisTrack->hPreProcess              = NULL;
	pHisTrack->Params					= NULL;
	///////////////////////////////////
	pHisTrack->iStartRecordNo			= THE_FIRST_READ;  //Express first read data
	///////////////////////////////////
	pHisTrack->bSectorEffect			= TRUE;            //Current data can be write in flash
	///////////////////////////////////
	pHisTrack->iTotalSectors			= MAX_LOG_SECTORS;
	pHisTrack->iMaxRecordsPerSector	    = 0;
	pHisTrack->byCurrSectorNo			= 1;
	pHisTrack->iWritableRecords			= 0;
	pHisTrack->iCurrWritePos			= 0;
    pHisTrack->iCurrReadPos				= 0;
	pHisTrack->dwCurrRecordID			= 1;
	pHisTrack->pbyPhysicsSectorNo		= NULL;

	//If current data change another sector, TRUE express
	//It will be used in writing service thread
	pHisTrack->byNewSectorFlag			= FALSE;

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : LOG_ReadLogStatus
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-31 19:59
 *==========================================================================*/
static BOOL LOG_ReadLogStatus(void)
{
	long ioffset;
	int j;
	int iRecords;
	DWORD dwCurrRecord;
	dwCurrRecord = 0;
	DWORD *pCurrRecordID = &dwCurrRecord;

	DWORD dwMaxRecordID;
	BYTE bySectorNo;
	int iRecordSize;
	int iAcutalRecordSize;
	int iMaxRecordsPerSector;

	//LOG_DeleteLogRecords();

	if (g_iHisFile == (-1))
	{
		return FALSE;
	}

	//No any historical data store in Flash
	if (pHisTrack == NULL)   
	{
		return FALSE;
	}

	
	//Start reading historical data status
	dwMaxRecordID = 0;
	iRecordSize = pHisTrack->iRecordSize;

	iAcutalRecordSize = 
		iRecordSize + LOG_RECORD_ID_BYTES + LOG_CRC16_BYTES;

	//Read current log data's all sectors
	for (j = 1; j <= pHisTrack->iTotalSectors; j++)
	{
			//The j Sector is empty
		if (LOG_SectorIsEmpty(j))  
		{

			if (j == 1)

			{
				//Next Sector is empty too
				if(LOG_SectorIsEmpty(j + 1)) 
				{	
					pHisTrack->byCurrSectorNo = 1;

				}
				else
				{
                      pHisTrack->byCurrSectorNo = pHisTrack->iTotalSectors;


				}					
			}
			else
			{   
				pHisTrack->byCurrSectorNo = j - 1;					
			}
			break;				
		}
		else   //The j Sector isn't empty
		{
			ioffset = (j - 1) * SIZE_PER_LOG_SECTOR;

			lseek(g_iHisFile, ioffset, SEEK_SET);					
				
			//First record iCurrRecordID of j sector				
			read(g_iHisFile, (void *)pCurrRecordID, (unsigned)sizeof(DWORD));	
			if (*pCurrRecordID == 0xffffffff)
			{
#ifdef _RUN_LOG_WRITE_
				//printf("j is %d, pCurrRecordID is %d, dwMaxRecordID is %d\n",
				//	j, *pCurrRecordID, dwMaxRecordID);
				//printf("pCurrRecordID < 0,It will clear all sector.\n");
#endif
				LOG_DeleteLogRecords();

				return TRUE;
			}

#ifdef _RUN_LOG_WRITE_
			//printf("j is %d, pCurrRecordID is %d, dwMaxRecordID is %d\n",
			//	j, *pCurrRecordID, dwMaxRecordID);
#endif
			if( dwMaxRecordID < (*pCurrRecordID))
			{
				dwMaxRecordID = (*pCurrRecordID);	
				//Current is the max sector number of current data
				if (j >= pHisTrack->iTotalSectors)
				{
					pHisTrack->byCurrSectorNo = j;
					break;
				}
			}
			else
			{
				pHisTrack->byCurrSectorNo = j - 1;

				break;
			}				
		}
	}//End reading current log data's all sectors
			

#ifdef _RUN_LOG_WRITE_
			//printf(" pHisTrack->byCurrSectorNo is %d\n",
			//	 pHisTrack->byCurrSectorNo);
#endif
	//Search for current record position
	*pCurrRecordID = 0;
	bySectorNo = pHisTrack->byCurrSectorNo;
				

	pHisTrack->iMaxRecordsPerSector = SIZE_PER_LOG_SECTOR/iAcutalRecordSize;

	iMaxRecordsPerSector = pHisTrack->iMaxRecordsPerSector;
		
	iRecords = LOG_SearchCurrPos((int)bySectorNo, iAcutalRecordSize, 
			iMaxRecordsPerSector, pCurrRecordID);

		//Update g_LOG_RECORD_SET_TRACK[i] relevant variables
	pHisTrack->iWritableRecords = iMaxRecordsPerSector - iRecords;

		//New ID should be added 1 based on old existed max recordID
	pHisTrack->dwCurrRecordID = (*pCurrRecordID) + 1;

#ifdef _RUN_LOG_WRITE_
	//printf(" pHisTrack->dwCurrRecordID is %d\n", pHisTrack->dwCurrRecordID);
#endif

	pHisTrack->iCurrWritePos = iRecords;	

		//Set global variable
	pHisTrack->byNewSectorFlag = TRUE;		

#ifdef _RUN_LOG_WRITE_
	//printf("Current Datatype is %s\n", pHisTrack->DataName);
	//printf("Current iRecords is %d\n", iRecords);
	//printf("Current Sector is %d\n", (int)bySectorNo);
#endif

 	return TRUE;
}

/*==========================================================================*
 * FUNCTION : LOG_IniReadFlashLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-31 19:04
 *==========================================================================*/
BOOL LOG_IniReadFlashLog(void)
{
	g_iHisFile = open(MTD5_NAME, O_RDONLY);

	if (g_iHisFile == (-1))
	{
		return FALSE;
	}

	pHisTrack = (LOG_RECORD_SET_TRACK *)NEW(LOG_RECORD_SET_TRACK, 1);

	if (pHisTrack == NULL)
	{
		return FALSE;
	}
	
	if (!LOG_IniStaticParas())
	{
		return FALSE;
	}

	if (!LOG_ReadLogStatus())
	{
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : GetMaxRecordsReadable
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: LOG_RECORD_SET_TRACK  *pTrack : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-20 19:44
 *==========================================================================*/
static int LOG_GetMaxRecordsReadable(IN LOG_RECORD_SET_TRACK *pTrack)
{
	int iRecords;

	if (pTrack->dwCurrRecordID > (unsigned)(pTrack->iMaxRecords))
	{
		iRecords = pTrack->iMaxRecords;
	}
	else
	{
		iRecords = pTrack->dwCurrRecordID;
		if (pTrack->dwCurrRecordID == 1)
		{
			iRecords = 0;
		}
	}

    return iRecords;
}

/*==========================================================================*
 * FUNCTION : LOG_ChangeCurrSectorPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: LOG_RECORD_SET_TRACK  *pTrack         : 
 *            int               iOffsetToOldPos : + ,Fore sectors
 *												  - ,Back sectors
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-19 20:03
 *==========================================================================*/
static void LOG_ChangeCurrSectorPos(IN OUT LOG_RECORD_SET_TRACK *pTrack, 
								IN int iOffsetToOldPos)
{
	BYTE byCurrLogiSector;

	byCurrLogiSector = pTrack->byCurrSectorNo;

    if (iOffsetToOldPos > 0)
	{
		if ((byCurrLogiSector + iOffsetToOldPos) > pTrack->iTotalSectors)
		{
			pTrack->byCurrSectorNo = 
				(byCurrLogiSector + iOffsetToOldPos)%(pTrack->iTotalSectors);
		}
		else
		{
			pTrack->byCurrSectorNo = byCurrLogiSector + iOffsetToOldPos;
		}
	}
	else
	{
		if ((byCurrLogiSector + iOffsetToOldPos) > 0)
		{
			pTrack->byCurrSectorNo = byCurrLogiSector + iOffsetToOldPos;
		}
		else
		{
			pTrack->byCurrSectorNo = 
				pTrack->iTotalSectors + byCurrLogiSector + iOffsetToOldPos;
		}
	}

	return;
}
/*==========================================================================*
 * FUNCTION : LOG_GetCurrentReadPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: LOG_RECORD_SET_TRACK  *ppTrack    : 
 *            BOOL              bAscending : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-19 19:26
 *==========================================================================*/
static BOOL LOG_GetCurrentReadPos(IN OUT LOG_RECORD_SET_TRACK *pTrack,
							  IN int iStartRecordNo,
							  IN BOOL bAscending)
{
	int iPosToCurrSector;
	int iBackRecords;
	int iMaxRecords;
	
#ifdef _RUN_LOG_WRITE_
	//printf("Enter to GetCurrentReadPos.\n");
#endif
	iPosToCurrSector = 0;
	iBackRecords	= 0;
	iMaxRecords		= 0;
	if (pTrack == NULL)
	{
		return FALSE;
	}

	//It want to read all records
	if (iStartRecordNo == -1)
	{
		iStartRecordNo = 1;
	}

	//Actual start sector,actual start read pos in actual start sector
	//1.Find the actual start sector according actual starting record no
	//Get current iMaxRecords
	//1 Get Acutal startrecord if the first read record
	if (pTrack->iStartRecordNo == THE_FIRST_READ)
	{
#ifdef _RUN_LOG_WRITE_
		//printf("pTrack->iStartRecordNo == THE_FIRST_READ\n");
#endif
		if (bAscending)
		{
			if (pTrack->dwCurrRecordID > (unsigned)(pTrack->iMaxRecords))
			{
				iMaxRecords = pTrack->iMaxRecords;
				//It's ok
				pTrack->iStartRecordNo = iStartRecordNo;				
			}
			else
			{
				iMaxRecords = pTrack->dwCurrRecordID - 1;
				pTrack->iStartRecordNo = 
					iStartRecordNo + (pTrack->iMaxRecords - iMaxRecords);
				//Modify iStartRecordNo
				iStartRecordNo = pTrack->iStartRecordNo;
			}
		}
		else
		{
			if (pTrack->dwCurrRecordID > (unsigned)(pTrack->iMaxRecords))
			{
				iMaxRecords = pTrack->iMaxRecords;
				pTrack->iStartRecordNo = iStartRecordNo;				
			}
			else
			{
				iMaxRecords = pTrack->dwCurrRecordID - 1;
			
				if (iStartRecordNo > iMaxRecords)
				{
					//It's an error current only iMaxRecords
					return FALSE;
				}
				else
				{
					pTrack->iStartRecordNo = iStartRecordNo;
				}				
			}	
		}
	}


	//2 Find the acutal starting sector no according to acutal start record no
	//  and find the actual reading pos in this sector
	if (bAscending)
	{
		iBackRecords = pTrack->iCurrWritePos - 
			pTrack->iMaxRecords + pTrack->iStartRecordNo - 1; //-1 express include
		                                                      //the pTrack->iStartRecordNo record

#ifdef _RUN_LOG_WRITE_
		//printf("iBackRecords is %d, pTrack->iStartRecordNo is %d\n", 
		//	iBackRecords, pTrack->iStartRecordNo);
#endif
	}
	else
	{
		iBackRecords = pTrack->iCurrWritePos - pTrack->iStartRecordNo;
#ifdef _RUN_LOG_WRITE_
		//printf("pTrack->iCurrWritePos is %d, pTrack->iStartRecordNo is %d\n", 
		//	pTrack->iCurrWritePos, pTrack->iStartRecordNo);
#endif
	}

	if (iBackRecords >=0)  //Actuall starting from current sector
	{
		//pTrack->iCurrSectorNo no change
		pTrack->iCurrReadPos = iBackRecords;
#ifdef _RUN_LOG_WRITE_
		//printf("pTrack->iCurrReadPos is %d, iBackRecords is %d\n", 
		//	pTrack->iCurrReadPos, iBackRecords);
#endif
	}
	else				   //Actually starting from his-sector
	{
		iPosToCurrSector = 
			((iBackRecords + 1)/(pTrack->iMaxRecordsPerSector)) - 1;
		LOG_ChangeCurrSectorPos(pTrack, iPosToCurrSector);
		
		if (((iBackRecords)%(pTrack->iMaxRecordsPerSector)) == 0)
		{
			pTrack->iCurrReadPos = 0;
		}
		else
		{
			pTrack->iCurrReadPos =
				pTrack->iMaxRecordsPerSector
				+ ((iBackRecords)%(pTrack->iMaxRecordsPerSector));
		}
	}	

	//ChangeCurrSectorPos(pTrack, iPosToCurrSector);
#ifdef _RUN_LOG_WRITE_
		//printf("pTrack->iCurrReadPos is %d, iPosToCurrSector is %d\n", 
		//	pTrack->iCurrReadPos, iPosToCurrSector);
		//printf("End Get CurrentRecordPos\n");
#endif
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : LOG_ReadRecordsControl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: LOG_RECORD_SET_TRACK  *pTrack               : 
 *            long              *iIniOffset              : 
 *            int               *iRecordsToRead       : 
 *            int               *iRecordsInCurrSector : 
 *            BOOL              bAscending            : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-20 11:58
 *==========================================================================*/
static int LOG_ReadRecordsControl( IN OUT LOG_RECORD_SET_TRACK *pTrack, 
							   OUT	  long *iIniOffset,
							   IN OUT int  *iRecordsToRead,
							   OUT	  int  *iRecordsInCurrSector,
							   IN	  BOOL bAscending)
{
	BYTE byCurrPhySector;
	int iRecords;
	int iPosToChange;
	int iRecordsReadable;
	int iMaxValidRecords;

#ifdef _RUN_LOG_WRITE_
		//printf("Enter to ReadRecordsControl.iRecordsToRead is %d\n", 
		//	*iRecordsToRead);
#endif

	//1.Check input arguments is right or not
	iRecords = *iRecordsToRead;
	if (iRecords <= 0)
	{
		*iRecordsToRead = 0;
		return ERR_END_READING; // All records has been read out
	}
	
	//2.Check it should end reading operation or not
	iMaxValidRecords = LOG_GetMaxRecordsReadable(pTrack);
	if (((pTrack->iStartRecordNo > iMaxValidRecords) && (!bAscending))
			|| (pTrack->iStartRecordNo > pTrack->iMaxRecords))
	{
#ifdef _RUN_LOG_WRITE_
		//printf("pTrack->iStartRecordNo is %d, iMaxValidRecords is %d\n", 
		//	pTrack->iStartRecordNo, iMaxValidRecords);
#endif
		*iRecordsToRead = 0;
		return ERR_END_READING;  //It should end reading
	}
	
	//3.It need to change sector to read other records
	if ((bAscending && 
		(pTrack->iCurrReadPos == pTrack->iMaxRecordsPerSector))
		|| (!bAscending && 
		(pTrack->iCurrReadPos < 0)))
	{
#ifdef _RUN_LOG_WRITE_
		//printf("It need to change sector to read other records.\n");
#endif
		iPosToChange = -1;
		if (bAscending)
		{
			iPosToChange = 1;
		}

		LOG_ChangeCurrSectorPos(pTrack, iPosToChange);

		//Get current read position
		if (bAscending)
		{
			pTrack->iCurrReadPos = 0;
		}
		else
		{
			pTrack->iCurrReadPos = pTrack->iMaxRecordsPerSector - 1;
		}
	}
	
	//4.Get new iOffset
	byCurrPhySector = pTrack->byCurrSectorNo;

	if ((byCurrPhySector < 1) || (byCurrPhySector > MAX_LOG_SECTORS))
	{
#ifdef _RUN_LOG_WRITE_
		printf("(byCurrPhySector < 1) &&"
			"(byCurrPhySector > 8)\n");
#endif
		return ERR_CTRL_INPUT;
	}
#ifdef _RUN_LOG_WRITE_
		//printf("byCurrPhySector is %d\n", byCurrPhySector);
#endif	

	//It need to add pTrack->CurrReadPos * ActualRecordsize



	*iIniOffset = (byCurrPhySector - 1) * SIZE_PER_LOG_SECTOR;


	//4.Calculate records to read in current sector
	if (bAscending)
	{
		//If current sector is full or if current sector is not full
		iRecordsReadable = pTrack->iMaxRecordsPerSector - pTrack->iCurrReadPos;	
		if (iRecordsReadable <= 0)
		{
			AppLogOut("DAT_READ_CTL", APP_LOG_INFO, "There is no records to read.");
			*iRecordsToRead = 0;
			return ERR_END_READING; // All records has been read out
		}

		if (iRecordsReadable >= (pTrack->iMaxRecords - pTrack->iStartRecordNo))
		{
			*iRecordsInCurrSector = 
				(pTrack->iMaxRecords - pTrack->iStartRecordNo) + 1;
			//+1 express include the pTrack->iStartRecordNo record
		}
		else
		{
			*iRecordsInCurrSector = iRecordsReadable;
		}
	}
	else
	{
		//iRecordsReadable = pTrack->iCurrReadPos;
		//Modified by lixidong 20050306,because icurrreadpos has move back 1 record
		//at GetCurrReadPos()
		iRecordsReadable = pTrack->iCurrReadPos + 1;
		if (iRecordsReadable <= 0)
		{
			AppLogOut("DAT_READ_CTL", APP_LOG_INFO, "There is no records to read.");
			*iRecordsToRead = 0;
			return ERR_END_READING; // All records has been read out
		}

		if (iRecordsReadable >= (iMaxValidRecords - pTrack->iStartRecordNo))
		{
			*iRecordsInCurrSector = 
				(iMaxValidRecords - pTrack->iStartRecordNo) + 1;   
			//+1 express include the pTrack->iStartRecordNo record
		}
		else
		{
			*iRecordsInCurrSector = iRecordsReadable;
		}
	}

	return ERR_DAT_OK;
}


/*==========================================================================*
 * FUNCTION : LOG_ReadMoreRecords
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: LOG_RECORD_SET_TRACK  *pTrack      : 
 *            void              *pRecordBuff : 
 *            int               *iRecords    : 
 *            BOOL              bPrintToScreen : 
 *            BOOL              bAscending   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-20 13:46
 *==========================================================================*/
static BOOL LOG_ReadMoreRecords(IN  LOG_RECORD_SET_TRACK *pTrack,
								OUT		void *pRecordBuff, 
								IN OUT  int  *iRecords,
								IN		BOOL bPrintToScreen,
								IN		BOOL bAscending)
{
	FILE *fp;
	long iOffset;

	long iIniOffset;
	int  iRecordsToRead;
	int  iRecordsInCurrSector;
	
	int iRecordsEffect;
	int iEmptyRecords, iMaxValidRecords;
	int iReadReturn;
	int iResult, k;
	long iCurrPos;
	int iRecordSize, iActualRecordSize;
	void *pRecord;	

#ifdef _RUN_LOG_WRITE_
	//printf("Enter to LOG_ReadMoreRecords.\n");
	//printf("*iRecords is %d, pTrack->iStartRecordNo is %d.\n", 
	//		*iRecords, pTrack->iStartRecordNo);
#endif

	if (g_iHisFile == (-1))
	{
		return FALSE;
	}

	iRecordsEffect = 0;
	iEmptyRecords = 0;
	iReadReturn = 0;

	//1.Get record size and storage record size
	iRecordSize = pTrack->iRecordSize;
    iActualRecordSize = iRecordSize + LOG_RECORD_ID_BYTES + LOG_CRC16_BYTES;

	pRecord = (void *)NEW(char, iRecordSize);
	if(pRecord == NULL)
	{
		return FALSE;
	}
	
	iResult = 0;
	iRecordsToRead = *iRecords;
	iIniOffset = 0;


	while (iRecordsEffect < *iRecords)
	{	
		//1.Get iRecordsToRead in current sectors
		iRecordsToRead = *iRecords - iRecordsEffect;
		iResult = LOG_ReadRecordsControl(pTrack, 
							   &iIniOffset,
							   &iRecordsToRead,
							   &iRecordsInCurrSector,   //iRecordsInCurrSector is dead length
							   bAscending);
#ifdef _RUN_LOG_WRITE_
		//printf("LOG_ReadRecordsControl iResult is %d.\n", iResult);
		//printf("LOG_ReadRecordsControl iIniOffset is %d.\n", (int)iIniOffset);
		//printf("LOG_ReadRecordsControl iRecordsToRead is %d.\n", iRecordsToRead);
		//printf("LOG_ReadRecordsControl iRecordsInCurrSector is %d.\n", iRecordsInCurrSector);
#endif

		if (iResult == ERR_CTRL_INPUT)
		{
			DELETE(pRecord);
			fclose(fp);
			return FALSE;
		}
		else if (iResult == ERR_END_READING)
		{
            break;
		}
		iOffset = iIniOffset + pTrack->iCurrReadPos * iActualRecordSize;

		iCurrPos = iOffset;
		iEmptyRecords = 0;
		for (k = 0; k < iRecordsInCurrSector; k++)
		{
			//Process alarm specially
			if ((k != 0))
			{
				;//iCurrPos = iCurrPos + iAcAlarmAddSize;
			}

			iReadReturn = LOG_ReadOneRecord(g_iHisFile, iCurrPos, 
				pRecord, iRecordSize);
			//records are empty
			if ((iReadReturn == 1) )
			{
				iEmptyRecords++;
				if(iEmptyRecords >= 3)    //continuous three records are empty
				{
#ifdef _RUN_LOG_WRITE_
					printf("It found that there are %d empty records.\n", 
						iEmptyRecords);
#endif
					pTrack->iStartRecordNo = pTrack->iStartRecordNo + 
						(iRecordsInCurrSector - k - 1);
					if (bAscending)
					{
						pTrack->iCurrReadPos = 
							pTrack->iCurrReadPos + (iRecordsInCurrSector - k - 1);
					}
					else
					{
						pTrack->iCurrReadPos = 
							pTrack->iCurrReadPos - (iRecordsInCurrSector - k - 1);
					}

					break;
				}
			}

			if( iReadReturn == 0)  //Effective records
			{	
				if (bPrintToScreen)
				{
					printf("%s", (char *)pRecord);
				}

				iEmptyRecords = 0;
				memmove((void*)((char *)pRecordBuff + iRecordsEffect * iRecordSize) , 
					pRecord, 
					(unsigned)iRecordSize);
				iRecordsEffect++;

#ifdef _RUN_LOG_WRITE_
				//printf("iRecordsEffect are %d records.\n", iRecordsEffect);
#endif
			}

			if (iReadReturn == 2)  //Invalid record
			{
				iEmptyRecords = 0;
			}
			
			if (bAscending)
			{
				iCurrPos = iCurrPos + iActualRecordSize;
				pTrack->iCurrReadPos++;
			}
			else
			{
				iCurrPos = iCurrPos - iActualRecordSize;
				pTrack->iCurrReadPos--;
			}
			
			pTrack->iStartRecordNo++;

			if ((iRecordsToRead == 0) || 
				(iRecordsEffect >= *iRecords))
			{
				break; //End reading record
			}
		}//End for
		
		
        if ((iRecordsToRead == 0) || 
				(iRecordsEffect >= *iRecords))
		{
			break; //End reading record
		}

		iMaxValidRecords = LOG_GetMaxRecordsReadable(pTrack);
		if (((pTrack->iStartRecordNo >= iMaxValidRecords) && (!bAscending))
			|| (pTrack->iStartRecordNo >= pTrack->iMaxRecords))
		{
			break;  //It should end reading
		}
		
	} //End while
	
	*iRecords = iRecordsEffect;
    DELETE(pRecord);
	return TRUE;
}


/*================================================================================*
 * FUNCTION : LOG_StorageReadRecords
 * PURPOSE  : Read current historical data iRecords pieces from iStartRecordNo.
 *			  It's not include active alarm reading
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: int     *pStartRecordNo : Start record number, if bAscending=TRUE,
 *										(-iMaxRecords) as first record; if 
 *										bAscending=FALSE,the latest record as 
 *										first record.
 *										if *pStartRecordNo=0, it express to continue
 *										read following records (IN)
 *										else if *pStartRecordNo=(-1),it will read 
 *										current Max records. (IN)
 *										else if *pStartRecordNo=(-2),Reading records 
 *										ended,it has compelted records (OUT)
 *            int     *iRecords       : Records to read
 *            void    *pBuff          : Buffer, it will be alloced by caller
 *			  BOOL    bPrintToScreen  : 1: to print to screen
 *									    0: Not print to screen
 *            BOOL    bAscending      : TRUE:direction is from -iMaxRecords
 *									        postion to current writing position	
 *									  : FALSE:From latest record position to oldest
 *									   record position
 * RETURN   : BOOL :				   TRUE:Succeed	   FALSE:Failed
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:47
 *================================================================================*/
BOOL LOG_StorageReadRecords (IN OUT	int		*piStartRecordNo, 
							 IN OUT	int		*piRecords, 
							 OUT	void	*pBuff,
							 IN		BOOL	bPrintToScreen,
							 IN		BOOL	bAscending)
{
	int iRecordsEffect;        //Actually effective records
	int iMaxRecords;

	int iStartRecordNo;
	int iRecords;

	LOG_RECORD_SET_TRACK * pTrack;

#ifdef _RUN_LOG_WRITE_
	//printf("Come to LOG_StorageReadRecords, "
	//	"*piStartRecordNo is %d, *piRecords is %d\n", 
	//	*piStartRecordNo, *piRecords);
#endif

	if (pHisTrack == NULL)
	{
		// 1. init log management module
		if (!LOG_IniReadFlashLog())
		{
			LOG_ClearReadFlashLog();
			return FALSE;
		}
	}

	if (pHisTrack != NULL)
	{
		if ((pHisTrack->byCurrSectorNo == 1) &&
			pHisTrack->dwCurrRecordID <= 1)
		{
			*piStartRecordNo = -2;                //End reading record
			*piRecords = 0;
			LOG_ClearReadFlashLog();
			return TRUE;
		}
	}

	pTrack = pHisTrack; 
	iStartRecordNo = *piStartRecordNo;
	iRecords	   = *piRecords;

	if (iRecords > pTrack->iMaxRecords)
	{
		iRecords = pTrack->iMaxRecords;
	}

#ifdef _RUN_LOG_WRITE_
	//printf("(iRecords) > pTrack->iMaxRecordsPerSector)\n.%d > %d\n",
	//	iRecords, pTrack->iMaxRecordsPerSector);
#endif

	
	//1.Check input arguments
	if (((iStartRecordNo < 0) && (iStartRecordNo != (-1)) 
			&& (iStartRecordNo != (-2)))
		|| ((iStartRecordNo == 0) && 
			(pTrack->iStartRecordNo == THE_FIRST_READ))			//Input error
		|| (iStartRecordNo > pTrack->iMaxRecords)				//Input error
		|| ((iStartRecordNo != (-1)) && (iRecords < 1)))		//Input error
	{
		LOG_ClearReadFlashLog();
		return FALSE;
	}

#ifdef _RUN_LOG_WRITE_
	//printf("Pass to arguments check.\n");
#endif

	if (iStartRecordNo == (-1))
	{
#ifdef _RUN_LOG_WRITE_
		//printf("User want to read all records.\n");
#endif

		iStartRecordNo = 1;
		if (iRecords >= pTrack->iMaxRecords)
		{
			iRecords = pTrack->iMaxRecords;
		}
	}
	else if (iStartRecordNo == 0)
	{
		//Continue to read other records
		iStartRecordNo = pTrack->iStartRecordNo;
	}

	//2.Control reading data scope
	iMaxRecords = LOG_GetMaxRecordsReadable(pTrack);

	//if((pTrack->iStartRecordNo >= iMaxRecords))
	if(((pTrack->iMaxRecords - pTrack->iStartRecordNo) < 0) ||
		((pTrack->iStartRecordNo >= iMaxRecords) && !bAscending))
	{
		*piStartRecordNo = -2;                //End reading record
		*piRecords = 0;
		LOG_ClearReadFlashLog();
        return  TRUE;                         //The last time 
											  //END reading records 
	}

	//3.Get current read logic sector No. and in current sector's position
	if((pTrack->iCurrReadPos == THE_FIRST_READ) && 
		(pTrack->iStartRecordNo == THE_FIRST_READ)) //The first time
	{
#ifdef _RUN_LOG_WRITE_
		//printf("This is first read record.iMaxRecords is %d\n", iMaxRecords);
#endif		
		if (!LOG_GetCurrentReadPos(pTrack, iStartRecordNo, bAscending))
		{
			LOG_ClearReadFlashLog();
			*piRecords = 0;
			return FALSE;
		}
#ifdef _RUN_LOG_WRITE_
		//printf("Pass to GetCurrentReadPos check.\n");
#endif
	}	

	//4.Find current data tracking index
	iRecordsEffect = 0;	
	/*Add mutex lock,ensure only one user read one type of historical data
	  at the same time */
	if (!LOG_ReadMoreRecords(pTrack, 
							pBuff, 
					        &iRecords,
					        bPrintToScreen,
					        bAscending))
	{
		LOG_ClearReadFlashLog();
        *piRecords = 0;
		return FALSE;
	}

#ifdef _RUN_LOG_WRITE_
		//printf("Finished to LOG_ReadMoreRecords iRecords is %d."
		//	"pTrack->iStartRecordNo is %d\n", 
		//	iRecords, pTrack->iStartRecordNo);
#endif
	//Return actual records have been read
    *piRecords = iRecords;   
	*piStartRecordNo = pTrack->iStartRecordNo;

	LOG_ClearReadFlashLog();
    return TRUE;
}

/*==========================================================================*
 * FUNCTION : LOG_ClearReadFlashLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-01 16:04
 *==========================================================================*/
void LOG_ClearReadFlashLog(void)
{
#ifdef _RUN_LOG_WRITE_
	//printf("g_iHisFile is %d\n",g_iHisFile);
#endif
	if (g_iHisFile != (-1))
	{
		close(g_iHisFile);
		g_iHisFile = (-1);
	}

		
	if (pHisTrack != NULL)
	{
		if (pHisTrack->pbyPhysicsSectorNo != NULL)
		{
			DELETE(pHisTrack->pbyPhysicsSectorNo);
			pHisTrack->pbyPhysicsSectorNo = NULL;
		}

		DELETE(pHisTrack);
		pHisTrack = NULL;
	}

	return;
}
///////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : LOG_IniWriteFlashLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-02 15:28
 *==========================================================================*/
BOOL LOG_IniWriteFlashLog(void)
{
	int iFileLen, i;
	BYTE byData;

	iFileLen		= 0;
	g_iLogOffset	= 0;
	byData			= 0xFF;
	i				= 0;

#ifdef _RUN_LOG_WRITE_
	//printf("Come to LOG_IniWriteFlashLog\n");
#endif

	g_iWriteFile = open(MTD5_NAME, O_RDWR);

	if (g_iWriteFile == (-1))
	{
		g_iLogFlashOpenState = ERR_LOG_CANT_OPEN_FLASH;
		return FALSE;
	}

	pWriteTrack = (LOG_RECORD_SET_TRACK *)NEW(LOG_RECORD_SET_TRACK, 1);

	if (pWriteTrack == NULL)
	{
		LOG_ClearWriteFlashLog();
		return FALSE;
	}

	if (!LOG_IniReadFlashLog())
	{
		LOG_ClearReadFlashLog();
		LOG_ClearWriteFlashLog();
		return FALSE;
	}

	if (pHisTrack == NULL)
	{
		LOG_ClearReadFlashLog();
		LOG_ClearWriteFlashLog();
		return FALSE;
	}
	else
	{
		memmove((void *)pWriteTrack, (void *)pHisTrack, 
			(unsigned)sizeof(LOG_RECORD_SET_TRACK));
		
		LOG_ClearReadFlashLog();
	}
#ifdef _RUN_LOG_WRITE_
	//printf("Come to LOG_IniWriteFlashLog6\n");
#endif
	//According to pWriteTrack get g_iLogOffset;

	g_iLogOffset = (pWriteTrack->byCurrSectorNo - 1) * SIZE_PER_LOG_SECTOR 
		+ pWriteTrack->iCurrWritePos * (pWriteTrack->iRecordSize 
		+ LOG_RECORD_ID_BYTES + LOG_CRC16_BYTES);

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : LOG_WritePosControl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-02 16:19
 *==========================================================================*/
static BOOL LOG_WritePosControl(void)
{
	//Sector control
	pWriteTrack->iCurrWritePos++;

	if (pWriteTrack->iCurrWritePos >= pWriteTrack->iMaxRecordsPerSector)
	{
		//It need change sector  (The 8th sector can't be used.)
		if (pWriteTrack->byCurrSectorNo >= MAX_LOG_SECTORS)
		{
			pWriteTrack->byCurrSectorNo = 1;
		}
		else
		{
			pWriteTrack->byCurrSectorNo++;
		}
		pWriteTrack->iCurrWritePos = 0;
		
		if (!EraseLogSector(pWriteTrack->byCurrSectorNo, 1))
		{
			AppLogOut("LOG_WRI_CTL", APP_LOG_ERROR, "Fail to erase sector.");
		}

		g_iLogOffset = (pWriteTrack->byCurrSectorNo - 1) * SIZE_PER_LOG_SECTOR; 

			//+ pWriteTrack->iCurrWritePos * (pWriteTrack->iRecordSize 
			//+ LOG_RECORD_ID_BYTES + LOG_CRC16_BYTES);

#ifdef _RUN_LOG_WRITE_
		//printf("Current Sector is %d\n", pWriteTrack->byCurrSectorNo);
#endif
	}
	else
	{
		g_iLogOffset = g_iLogOffset + (pWriteTrack->iRecordSize 
			+ LOG_RECORD_ID_BYTES + LOG_CRC16_BYTES);

#ifdef _RUN_LOG_WRITE_
		//printf("Current iCurrWritePos is %d\n", pWriteTrack->iCurrWritePos);
		//printf("Current dwCurrRecordID is %d\n", pWriteTrack->dwCurrRecordID);
#endif
	}

	return TRUE;
}
/*==========================================================================*
 * FUNCTION : LOG_ClearWriteFlashLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-02 15:29
 *==========================================================================*/
BOOL LOG_ClearWriteFlashLog(void)
{
	Sleep(2000); //Let all log record write to flash
	
	if (g_iWriteFile != (-1))
	{
		//fflush(pWritexFile);
		close(g_iWriteFile);
		g_iWriteFile = (-1);
	}
	
	if (pWriteTrack != NULL)
	{
		DELETE(pWriteTrack);
		pWriteTrack = NULL;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : WriteOneRecord
 * PURPOSE  : Write a record to flash,
 *			: at the same time check the writing is correct or not;
 * CALLS    : 
 * CALLED BY: in the module
 * ARGUMENTS: void * pRecordBuff : 
 *            int    iRecordSize : Data record size not include dwRecordI and CheckNo
 *			  DWORD dwRecordID
 *            long iOffset          : 
 * RETURN   : BOOL : 0-Succeed  
 *				   : 1-Fail and but record space is free
 *				   : 2-Fail and record space is wasted.
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-18 19:23
 *==========================================================================*/
 static int LOG_WriteOneRecord(IN void *pRecordBuff, 
						   IN int iRecordSize, 
						   IN DWORD dwRecordID, 
						   IN long iOffset)
{
	int iActualRecordSize;
	unsigned short iCheckNo;
	int iResult;
	long iCurrPos;
	void * pWriteBuff;

	iActualRecordSize = iRecordSize 
		+ LOG_RECORD_ID_BYTES + LOG_CRC16_BYTES;

	pWriteBuff = (void *)NEW(char, iActualRecordSize);    //start
	if(pWriteBuff == NULL)
	{
		return ERR_LOG_NO_MEMORY;
	}

	memmove(pWriteBuff, (const void *)&dwRecordID, LOG_RECORD_ID_BYTES);
	memmove((void *)((char *)pWriteBuff + LOG_RECORD_ID_BYTES),
		pRecordBuff, (unsigned)iRecordSize);

	//dwRecordID is checked too
	iCheckNo = LOG_CRC16((unsigned char *)pWriteBuff, 
		(unsigned short)(iRecordSize + LOG_RECORD_ID_BYTES));

	memmove((void*)((char *)pWriteBuff + iRecordSize + LOG_RECORD_ID_BYTES),
		(const void *)&iCheckNo, LOG_CRC16_BYTES);
	
	iCurrPos = iOffset;
	lseek(g_iWriteFile, iCurrPos, SEEK_SET);	

    write(g_iWriteFile, (void *)pWriteBuff, (unsigned)iActualRecordSize);	
	//fflush(pWritexFile);
   	//Read and check
	iResult = ERR_LOG_OK;
	
	//Set to origin position
	//fseek(pWritexFile, iCurrPos, SEEK_SET);
	iResult = LOG_ReadOneRecord(g_iWriteFile, iCurrPos, pWriteBuff, iRecordSize);	

	if (iResult == ERR_LOG_OK) //succeed
	{
		pWriteTrack->dwCurrRecordID++;
		DELETE(pWriteBuff);
		return ERR_LOG_OK;
	}
	else if (iResult == ERR_LOG_WRI_EMPTY)  //Record is empty
	{
		DELETE(pWriteBuff);
		return ERR_LOG_WRI_EMPTY;		
		
	}
	else   //iResult == 2
	{
		pWriteTrack->dwCurrRecordID++;
		DELETE(pWriteBuff);		
		return ERR_LOG_WRI_WASTE;
	}	
}



/*==========================================================================*
 * FUNCTION : LOG_non_region_erase
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  Fd       : //Device file handle
 *            int  start    : //Start position
 *            int  count    : //current sector size divided by erase_info_t.length
 *								erase_info_t.length = mtd_info_t.erasesize
 *            int  unlock   : //0:all is unlocked.1;some bites is locked
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-09-23 15:35
 *==========================================================================*/
static int LOG_non_region_erase(IN int Fd, 
					IN int start, 
					IN int count, 
					IN int unlock)
{
	mtd_info_t meminfo;
	HANDLE hCurrentThread;

	hCurrentThread = RunThread_GetId(NULL);
	
	if (ioctl(Fd, MEMGETINFO, &meminfo) == 0)
	{
		erase_info_t erase;

		erase.start = start;

		erase.length = meminfo.erasesize;

#ifdef _RUN_LOG_WRITE_
		//printf("Erase Unit Size 0x%x, ", meminfo.erasesize);
#endif

		for (; count > 0; count--) 
		{
			RunThread_Heartbeat(hCurrentThread);

#ifdef _RUN_LOG_WRITE_
			//printf("\rPerforming Flash Erase of length %u at offset 0x%x\n",
			//		erase.length, erase.start);
#endif
			fflush(stdout);

			if(unlock != 0)
			{

				//Unlock the sector first.
#ifdef _RUN_LOG_WRITE_
				//printf("\rPerforming Flash unlock at offset 0x%x",erase.start);
#endif

				if(ioctl(Fd, MEMUNLOCK, &erase) != 0)
				{
#ifdef _RUN_LOG_WRITE_
					perror("\nMTD Unlock failure");
#endif

					close(Fd);
					return 8;
				}
			}

			if (ioctl(Fd, MEMERASE, &erase) != 0)
			{   
#ifdef _RUN_LOG_WRITE_
				perror("\nMTD Erase failure");
#endif
				close(Fd);
				return 8;
			}
			erase.start += meminfo.erasesize;
		}

	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : EraseLogSector_x86
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE  byStartSectorNo : Start sector to erase
 *            int   iSectors        : Sector numbers to erase
 * RETURN   : int :					: SUCCEED:1  FAIL:0
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-01 10:22
 *==========================================================================*/
int EraseLogSector_x86(int iStartSectorNo, int iSectors)
{
	int iStart;					//Start position
	int i,iCount;					
	DWORD dwData;
    
	if (iSectors <= 0 || iStartSectorNo <= 0)
	{
		return 0;
	}
	
 	if (g_iWriteFile == (-1))
	{
		return 0;
	}
    
	dwData = 0xffffffff;

	iStart = (iStartSectorNo - 1) * SIZE_PER_LOG_SECTOR;

	lseek(g_iWriteFile, iStart, SEEK_SET);
	
	iCount = SIZE_PER_LOG_SECTOR/sizeof(DWORD);
	iCount = iCount * iSectors;

	for (i = 0; i < iCount; i++)
	{
		write(g_iWriteFile, (void *)&dwData, sizeof(DWORD));
	}

	i = 0;
	while (i < iSectors)
	{
		if (!LOG_SectorIsEmpty(iStartSectorNo + i))
		{
			return 0;
		}
		i++;
	}
	
    return 1;	
}

/*==========================================================================*
 * FUNCTION : EraseLogSector_ppc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iStartSectorNo : Start sector to erase
 *            int   iSectors        : Sector numbers to erase
 * RETURN   : int :					: SUCCEED:1  FAIL:0
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-01 10:22
 *==========================================================================*/
int EraseLogSector_ppc(int iStartSectorNo, int iSectors)
{
	int Fd;
	int iStart;					//Start position
	int iCount;					
	int unlock;	
	int iEraseReturn;

	if (iSectors <= 0 || iStartSectorNo <= 0)
	{
		return 0;
	}
	
    Fd = open(MTD5_NAME,O_RDWR);






	if (Fd == (-1))
	{
		return 0;
	}
    



	iStart = (iStartSectorNo - 1) * SIZE_PER_LOG_SECTOR;


	//if every block is 1024bites
	iCount = iSectors;      
	unlock = 0;       //if any block is unlocked.

	iEraseReturn = LOG_non_region_erase(Fd, iStart, iCount, unlock);

	if(iEraseReturn == 0)
	{
		close(Fd);
		return 1;
	}
	else
	{
		//Failed,try again
		iEraseReturn = LOG_non_region_erase(Fd, iStart, iCount, unlock);
		if(iEraseReturn != 0)
		{
			//Failed, try third time
			iEraseReturn = LOG_non_region_erase(Fd, iStart, iCount, unlock);
			if (iEraseReturn != 0)
			{
				close(Fd);
				return 0;
			}
		}

        close(Fd);
		return 1;
	}
}


/*==========================================================================*
 * FUNCTION : LOG_DeleteLogRecords
 * PURPOSE  : It can be called at any case.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-02 18:37
 *==========================================================================*/
BOOL LOG_DeleteLogRecords(void)
{
	int iResult;

	if (pWriteTrack != NULL)
	{
		//lock
		LOCK_FLASH_LOG_SYNC();
		



		//iResult = system(ERASE_RUN_LOG);   
		iResult = _SYSTEM(ERASE_RUN_LOG);   

		if (iResult == ERR_LOG_OK)
		{
#ifdef _RUN_LOG_WRITE_
			printf("Succeed to delete running log.\n");
#endif
			pWriteTrack->byCurrSectorNo = 1;
			pWriteTrack->iCurrWritePos  = 0;
		    pWriteTrack->iCurrReadPos	= 0;
			pWriteTrack->dwCurrRecordID	= 1;
			g_iLogOffset				= 0; //Add by lixidong 20050419

			//If current data change another sector, TRUE express
			//It will be used in writing service thread
			pWriteTrack->byNewSectorFlag	= TRUE;
#ifdef _RUN_LOG_WRITE_
		printf("Succeed to update tracking data.\n");
#endif
		}
		else
		{
			pWriteTrack->bSectorEffect  = FALSE;
		}

		//unlock
		UNLOCK_FLASH_LOG_SYNC();
#ifdef _RUN_LOG_WRITE_
		printf("Succeed to exit lock.\n");
#endif
	}
	else
	{
		//iResult = system(ERASE_RUN_LOG);
		iResult = _SYSTEM(ERASE_RUN_LOG);
	}

	return TRUE;
}


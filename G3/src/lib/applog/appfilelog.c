/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : applog.c
 *  CREATOR  : Li Xi Dong               DATE: 2004-09-11 14:50
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <sys/types.h> 
#include <stdarg.h>

#include "stdsys.h"
#include "public.h"

#ifdef _DEBUG
//#define _DEBUG_APP_LOG
#endif
//File name definiton
//old: #define APP_LOG_FILE			"/opt/ACU/log/ACU.log"
//old: #define APP_BAK_LOG_FILE      "/opt/ACU/log/ACUBAK.log"


//new log path and name, by maofuhua, 2004-11-12
#define BASIC_LOG_FILE			"ACU.log"
#define BASIC_BAK_LOG_FILE		"ACU.bak.log"

#define SUB_DIR_LOG_FILE		"log"

#ifndef ENV_ACU_ROOT_DIR
#define ENV_ACU_ROOT_DIR		"ACU_ROOT_DIR"	// to use shell env variable.
#endif

static char APP_LOG_FILE[MAX_FILE_PATH] = BASIC_LOG_FILE;

BOOL g_bEnableAppLogOut = TRUE;


#ifndef K_BYTES
#define K_BYTES 1024
#endif

#define APP_LOG_SIZE        (100*K_BYTES)

// commout the next line to use the Mutex_xxx defined in public module.
#define __USE_PTHREAD_LOCK	1  // use pthread_mutex.

#ifdef __USE_PTHREAD_LOCK
//The handle of the mutex for write log,only writing by one user once
static pthread_mutex_t	hWriteLogLock = PTHREAD_MUTEX_INITIALIZER;

#define LOCK_APP_LOG_INIT()		  pthread_mutex_init(&hWriteLogLock, NULL)
#define LOCK_APP_LOG_SYNC()       pthread_mutex_lock(&hWriteLogLock)
#define UNLOCK_APP_LOG_SYNC()     pthread_mutex_unlock(&hWriteLogLock)
#define DESTROY_APP_LOG_SYNC()    pthread_mutex_destroy(&hWriteLogLock)

#else	// use pub mutex lock
static HANDLE	hWriteLogLock = NULL;

#define LOCK_APP_LOG_INIT()		  (hWriteLogLock = Mutex_Create(TRUE))
#define LOCK_APP_LOG_SYNC()       Mutex_Lock(hWriteLogLock, (DWORD)-1)
#define UNLOCK_APP_LOG_SYNC()     Mutex_Unlock(hWriteLogLock)
#define DESTROY_APP_LOG_SYNC()    Mutex_Destroy(hWriteLogLock)
#endif


static void AppLog_Cleanup(void)
{
	DESTROY_APP_LOG_SYNC();
}


#define FULL_LOG_HEAD_MSG	\
    "ACU runtime log information. Log text format is:\n"\
    "TYPE(INFO/WARN/ERR) TIME(yy-mm-dd hh:mm:ss in GMT) SoureTaskName#TaskID: The log message.\n\n"

// write head msg of log file. maofuhua, 2004-11-12
static BOOL AppLog_WriteHeader(void)
{
	FILE *fp = fopen( APP_LOG_FILE, "r" );

	if (fp != NULL)	// the log file exists, return. do not write head.
	{
		fclose(fp);
		return FALSE;
	}

	// write head msg
	fp = fopen( APP_LOG_FILE, "a+" );
	if (fp == NULL)
	{
		return FALSE;
	}

	fprintf( fp, "%s", FULL_LOG_HEAD_MSG );
	
	fclose(fp);

	return TRUE;
}

// added by maofuhua, 2004-11-12
static void AppLog_Init(void)
{
#ifdef _DEBUG
	if (getenv(ENV_ACU_ROOT_DIR) == NULL)
	{
		TRACEX("The env variable \"%s\" for the ACU root dir "
			"is not set yet, will use current dir as default.\n",
			ENV_ACU_ROOT_DIR);
	}
#endif //_DEBUG

	// add initialization log name, maofuhua, 2004-11-12
	MakeFullFileName(APP_LOG_FILE, 
		getenv(ENV_ACU_ROOT_DIR),
		SUB_DIR_LOG_FILE, 
		BASIC_LOG_FILE);

#ifdef _DEBUG_APP_LOG
	TRACEX("The full name of log file is %s.\n",
		APP_LOG_FILE);
#endif //_DEBUG_APP_LOG
// end add name.

	LOCK_APP_LOG_INIT();

	atexit(AppLog_Cleanup);

	// write head msg to log file.
	AppLog_WriteHeader();
}


static void AppLog_Backup(void)
{
#define LINUX_mv_CMD_FORMAT		"mv -f \"%s\" \"%s\""

	char szBakLogFile[MAX_FILE_PATH];
	char szShellCmd[MAX_FILE_PATH*2 + sizeof(LINUX_mv_CMD_FORMAT)];

	// mv file.
	// new backup file method, maofuhua, 2004-11-12
	MakeFullFileName(szBakLogFile, 
		getenv(ENV_ACU_ROOT_DIR),
		SUB_DIR_LOG_FILE, 
		BASIC_BAK_LOG_FILE);

	//old: system( "mv -f " APP_LOG_FILE " " APP_BAK_LOG_FILE );

	// make cmd
	snprintf(szShellCmd, sizeof(szShellCmd), LINUX_mv_CMD_FORMAT,
		APP_LOG_FILE, szBakLogFile);


#ifdef _DEBUG_APP_LOG
	TRACEX("Backup log file %s to bak file %s...\n"
		"SHELL CMD: %s\n",
		APP_LOG_FILE, szBakLogFile, szShellCmd);
#endif //_DEBUG_APP_LOG

	// exec cmd
	//system(szShellCmd);
	_SYSTEM(szShellCmd);

	// trying to write head msg to log file.
	if (!AppLog_WriteHeader())	// head is not written, the log file exists.
	{
		printf("AppLogOut: Fails on backup log file by executing:\n%s\n"
			"The log file %s will be truncated.\n",
			szShellCmd, APP_LOG_FILE);

		truncate(APP_LOG_FILE, sizeof(FULL_LOG_HEAD_MSG)-1);	// keep head info.
	}
}

/*==========================================================================*
 * FUNCTION : AppLogOut
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char        *pszTaskName :
 *			  int		  iLogLevel
 *            const char  *pszFormat   : 
 *                        ...          : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-15 12:10
 *==========================================================================*/
void AppLogOut( const char *pszTaskName, int iLogLevel, 
			   const char *pszFormat, ...) 
{
	static BOOL bLockInitialized = FALSE;	// added by maofuhua. 04/10/26

	va_list     arglist;
    char        szBuf[512];// increase it from 256 to 512. maofuhua, 04-12-01
    char        szTime[32];
	char		*pszInfoLevel;
    long        nLogSize;
    FILE        *fp;
	int			nLen;

	if (!g_bEnableAppLogOut)
	{
		return;
	}

	// register the at-exit function.
	if (!bLockInitialized)
	{
		AppLog_Init();		
		bLockInitialized = TRUE;
	}

	//Lock  
	LOCK_APP_LOG_SYNC();

    //ClearWDT(); /* clear watch dog */

_OpenAgin:
    fp = fopen( APP_LOG_FILE, "a+" );
    if( fp == NULL )
    {
        fprintf( stderr, "Open log file %s fail.\n", APP_LOG_FILE );

		UNLOCK_APP_LOG_SYNC();
        return;
    }

	fseek(fp, 0l, SEEK_END);
    nLogSize = ftell(fp); // at the end of file, we use a+ open
    if(nLogSize >= APP_LOG_SIZE )
    {
        fflush( fp );
        fclose( fp );

		// back file.
		AppLog_Backup();

        goto _OpenAgin;
    }

	switch(iLogLevel)
	{
	case APP_LOG_INFO:
		pszInfoLevel = "INFO";
		break;
	case APP_LOG_WARNING:
		pszInfoLevel = "WARN";
		break;
	case APP_LOG_ERROR:
		pszInfoLevel = "ERR ";
		break;
	default:
		pszInfoLevel = "????";	// undefined
		break;	
	}

	// get current time and convert to YYYY-MM-DD hh:mm:ss.
	TimeToString(time(NULL), TIME_CHN_FMT, szTime, sizeof(szTime));

	va_start(arglist, pszFormat);
    nLen = vsnprintf(szBuf, sizeof(szBuf), pszFormat, arglist);
	va_end(arglist);

	// discard the end LF "\n"
	if(szBuf[nLen-1] == '\n')	// added by maofuhua, 2004-11-04
	{
		szBuf[nLen-1] = 0; 
	}

	// format log info as:		 //maofuhua 2004-11-12
	// see the macro: LOG_HEAD_MSG
	//"Type yy-mm-dd hh:mm:ss Source task#Task ID: Log message ..."
	fprintf( fp, "%s %s %-16s#%08x: %s\n", 
		pszInfoLevel,
        &szTime[2],		// lose the centry of year. from yyyy to yy.
        pszTaskName,
        (unsigned int)RunThread_GetId(NULL),   
        szBuf);

	fflush( fp );
    fclose( fp );


#ifdef _DEBUG		
	// display logging message in console. added by maofuhua, 04-11-12
	fprintf( stdout, "%s %s %-16s#%08x: %s\n", 
		pszInfoLevel,
        &szTime[2], 
        pszTaskName,
        (unsigned int)RunThread_GetId(NULL),   
        szBuf);
	fflush(stdout);
#endif

	//unlock
	UNLOCK_APP_LOG_SYNC();

	return ;
}

BOOL LOG_DeleteLogRecords(void)
{
	return TRUE;
}

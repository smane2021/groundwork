/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : applog.c
 *  CREATOR  : LIXIDONG                 DATE: 2005-02-03 18:32
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#define _APP_LOG_TO_FLASH	1    //They are defined at file applog.h,you should
                                 //Modify it in applog.h at the same time
#ifndef _HAS_FLASH_MEM			// add auto support FLASH, maofuhua, 2005-3-26
	#undef _APP_LOG_TO_FLASH	// when in x86 mode, use file log.
#endif

#ifndef _APP_LOG_TO_FLASH
	#define _APP_LOG_TO_FILE	1
#endif

//It will decide which comiling method
#ifdef		_APP_LOG_TO_FLASH     //It is defined in applog.h
#include	"appflashlog.c"
#else
#include	"appfilelog.c"
#endif




/*********************************************************************************
*  
*  FILE NAME     : smain.c
*  AUTHOR        : Jimmy Wu
*  CREATED DATE  : 2013-4-28 12:27:52
*  VERSION       : V1.0
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
#include "handler.h"
#include "smain.h"
int main(void)
{
	//建立这个文件的目的是为了app端可以通过脚本判断本进程已经启动起来了，避免重复启动
	FILE	*fp = fopen(_SYSHAND_PID_FILE,"w+t");
	if (fp == NULL)
	{
		return 0;
	}
	fprintf(fp, "%d\n", getpid());
	fclose(fp);
	chmod(_SYSHAND_PID_FILE,0777);
	//fprintf(fp, "%d\n", getpid());
	//fclose(fp);
	//close(fd);
	
	//printf("\n------Jimmy I start OK!!! \n");

	Main_Loop();

	return 0;
}


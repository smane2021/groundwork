/*********************************************************************************
*  
*  FILE NAME     : $handler.c
*  AUTHOR        : Jimmy Wu
*  CREATED DATE  : 2013-4-28 10:35:14
*  VERSION       : V1.0
*  DESCRIPTION   : This is a thread for communicating with app, 
*				   for excuting 'system' cmd
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
#include <sys/types.h> 
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <sys/sem.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/wait.h>  

#include "handler.h"
#include "pthread.h"

#define _EXIT_OK 1
#define _EXIT_NOOK 0
#define _SEM_CMDWAIT_PATH "/_semSysHandWait"
#define _SEM_ANSWER_PATH "/_semSysHandAnsw"
#define _KEY_T_SYSHAND  (ftok("/app/bin/mkpkg",15)) //syshand进程的ID
#define IPCKEY 8765
//#define _KEY_T_APP  (ftok("/app/bin/app",0)) //app进程的ID
#define BOOL int
#define TRUE 1
#define FALSE 0

static BOOL InitThis(void);
static int HandleCmd(CMD* pCmd);
static sem_t* psemWaitCmd = NULL; //等待命令
static sem_t* psemAnswer = NULL; //命令应答完毕
static int shmid_cmd = -1; //cmd共享区id,放在app进程 (改为放在syshand进程)
static pthread_mutex_t gMutex = PTHREAD_MUTEX_INITIALIZER; //互斥量
#define LOCK_THREAD_MUTEX()		pthread_mutex_lock(&gMutex)
#define UNLOCK_THREAD_MUTEX()	pthread_mutex_unlock(&gMutex)
#define DESTROY_THREAD_MUTEX()	pthread_mutex_destroy(&gMutex)

//static int shmid_answ = -1; //answer共享区id,放在syshand进程
//static int* ithread(void* pArg);
//
//static int* ithread(void* pArg)
//{
//	int iCmdRet = ERR_OK;
//	CMD* pCmd;
//	BOOL bStopRun = *(BOOL*)(pArg);
//	int iExitCode = _EXIT_NOOK;	
//	//while(!bStopRun)
//	do
//	{
//		printf("\n------Jimmy, MAINLOOP begins: %d",bStopRun);
//		//sem_wait(psemWaitCmd);//等待命令
//		int iRr = sem_trywait(psemWaitCmd);
//		if(iRr != 0)
//		{
//			printf("\n------Jimmy, NO SIGNAL !!!!");
//			sleep(2);
//			continue;
//		}
//		pCmd = (CMD*)shmat(shmid_cmd,0,0);  
//		if(pCmd == (CMD*)-1)
//		{
//			printf("[sysHandler]:Read CMD is fail\n");
//			continue;
//		}
//		iCmdRet = HandleCmd(pCmd);
//		//pIRet = (int*)shmat(shmid_answ,0,0);
//		//(*pIRet) = iCmdRet;
//		pCmd->iCmdRet = iCmdRet;
//		if(shmdt((void*)pCmd) == -1)
//		{
//			printf("\n\n[sysHandler]:shmdt is fail\n");
//		}
//		//释放信号量，让app读
//		sem_post(psemAnswer);
//	}
//	while(1);
//	return iExitCode;
//}
int Main_Loop(void)
{
	int iCmdRet = ERR_OK;
	CMD* pCmd;


	int iExitCode = _EXIT_NOOK;	
	BOOL bIsRun = TRUE;
	bIsRun = InitThis();
	
	//printf("\n\n-----[sysHandler]: Init OK!!!");
	//pthread_t gThread;
	//pthread_create(&gThread, NULL, ithread, (void*)&bStopRun);
	while(bIsRun)
	//do
	{
		LOCK_THREAD_MUTEX();
		//printf("\n------[sysHandler]Jimmy, MAINLOOP begins: %d",bIsRun);
		sem_wait(psemWaitCmd);//等待命令
		//int iRr = sem_trywait(psemWaitCmd);
		//if(iRr != 0)
		//{
		//	printf("\n------Jimmy, NO SIGNAL !!!!");
		//	sleep(1);
		//	UNLOCK_THREAD_MUTEX();
		//	continue;
		//}
		//printf("\n\n------[sysHandler]Jimmy, I received a cmd !!!!");
		pCmd = (CMD*)shmat(shmid_cmd,0,0);  
		if(pCmd == (CMD*)-1)
		{
			printf("[sysHandler]:Read CMD is fail\n");
			UNLOCK_THREAD_MUTEX();
			continue;
		}
		iCmdRet = HandleCmd(pCmd);
		//pIRet = (int*)shmat(shmid_answ,0,0);
		//(*pIRet) = iCmdRet;
		pCmd->iCmdRet = iCmdRet;
		if(shmdt((void*)pCmd) == -1)
		{
			printf("\n\n[sysHandler]:shmdt is fail\n");
		}
		//释放信号量，让app读
		sem_post(psemAnswer);
		//sleep(1); //给足够的时间让app读到
		UNLOCK_THREAD_MUTEX();
	}
	//while(0);
	//sleep(5);
	//pthread_join(gThread,&iExitCode);

	//进程退出，执行清理动作

	//Jimmy注释:system函数必须在while循环里面执行，这下面的语句会报段错误，故屏蔽
	//char pszCmd[128];
	//sprintf("rm %s -f",_SYSHAND_PID_FILE);
	////system("reboot");
	//system(pszCmd);
	////printf("\n------Jimmy I falied and exit!!! \n");
	//sleep(5);
	DESTROY_THREAD_MUTEX();
	return iExitCode;
}
static BOOL InitThis()
{
	//以下开辟信号灯
	psemWaitCmd =  sem_open(_SEM_CMDWAIT_PATH, O_CREAT,0666, 0);//信号量初始值为0
	if(psemWaitCmd == SEM_FAILED || psemWaitCmd == NULL)
	{
		printf( "[sysHandler]:sem_open waitCmd Failed!\n" );
		return FALSE;
	}
	psemAnswer =  sem_open(_SEM_ANSWER_PATH, O_CREAT,0666, 0);//信号量初始值为0
	if(psemAnswer == SEM_FAILED || psemAnswer == NULL)
	{
		printf( "[sysHandler]:sem_open semAnswer Failed!\n" );
		return FALSE;
	}
	//printf("\n\n[sysHandler]:init sem OK");
	//以下创建共享内存区
	key_t keySys = _KEY_T_SYSHAND;
	//key_t keySys = IPCKEY;
	//printf("\n\n[sysHandler]:keyt is [%d]",keySys);
	//sleep(5);
	shmid_cmd = shmget(keySys,CMD_MEM_SIZE,0666 | IPC_CREAT | IPC_EXCL);
	if(shmid_cmd == -1)
	{
		printf("\n\n[sysHandler]:shmget ini ERRRRR!!!");
		return FALSE;
	}
	//system("reboot");
	return TRUE;
}
#define _SYS_EXE_OK				0
#define  _SYS_EXE_NO_SCRIPT		1
#define  _SYS_EXE_FAIL			2

static int HandleCmd(CMD* pCmd)
{
	int iType = pCmd->iCmdType;
	int iRet = _SYS_EXE_OK;
	switch(iType)
	{
	case 0:
	default:
		{
			//printf("***iRet=%d start***\n",iRet);
			iRet = system(pCmd->szCmd);
			//printf("***iRet=%d end  ***\n",iRet);
		}
		break;
	}
	if (-1 == iRet)  
	{
		iRet = _SYS_EXE_NO_SCRIPT;
		printf("\n[sysHandler]:system script error!");  
	}
	else
	{
		//printf("\n[sysHandler]:exit status value = [0x%x]\n", iRet);  

		if(WIFEXITED(iRet))  
		{
			if(0 == WEXITSTATUS(iRet))  
			{
				iRet = _SYS_EXE_OK;
				printf("\n[sysHandler]:run shell script successfully.\n");  
			}
			else
			{
				printf("\n[sysHandler]:run shell script fail, script exit code: %d\n", WEXITSTATUS(iRet));  
				iRet = _SYS_EXE_FAIL;
			}
		}
		else
		{
			printf("\n[sysHandler]:exit status = [%d]\n", WEXITSTATUS(iRet));
			iRet = _SYS_EXE_FAIL;
		}
	}
	return iRet;
}


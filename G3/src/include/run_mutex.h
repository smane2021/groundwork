/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : run_mutex.h
 *  CREATOR  : Mao Fuhua                DATE: 2004-10-04 16:14
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __RUN_MUTEX_H__
#define __RUN_MUTEX_H__


/*==========================================================================*
 * FUNCTION : Mutex_Create
 * PURPOSE  : Create a mutex
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN BOOL  bOpenedLock : TRUE: the lock is opened, 
 *                                   FALSE: lock wiil be locked by the creator
 * RETURN   : HANDLE : NULL: failure, others: the handle of the mutex
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-04 15:13
 *==========================================================================*/
HANDLE Mutex_Create(IN BOOL bOpenedLock);


/*==========================================================================*
 * FUNCTION : Mutex_Lock
 * PURPOSE  : try to get a lock
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hm            : 
 *            IN DWORD   dwWaitTimeout : time for waiting lock in ms
 * RETURN   : int : ERR_OK, for OK, ERR_MUTEX_TIMEOUT for timeout.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-04 16:35
 *==========================================================================*/
int Mutex_Lock(IN HANDLE hm, IN DWORD dwWaitTimeout);


/*==========================================================================*
 * FUNCTION : Mutex_Unlock
 * PURPOSE  : Release the locked lock.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hm : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-04 20:13
 *==========================================================================*/
void Mutex_Unlock(HANDLE hm);


/*==========================================================================*
 * FUNCTION : Mutex_Destroy
 * PURPOSE  : To destroy a mutex, the mutext can not be used.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hm : The HANDLE of mutext will be destroyed
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-04 16:19
 *==========================================================================*/
void Mutex_Destroy(IN HANDLE hm);




////////////////////// Semaphore ////////////////////////////

/*==========================================================================*
 * FUNCTION : Sem_Create
 * PURPOSE  : Create a semaphore
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  nInitValue : the value of semaphore at init
 * RETURN   : HANDLE : NULL: failure, others: the handle of the semaphore
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-14 15:13
 *==========================================================================*/
HANDLE Sem_Create(IN int  nInitValue);


/*==========================================================================*
 * FUNCTION : Sem_Wait
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hSem          : 
 *            IN DWORD   dwWaitTimeout : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-14 16:48
 *==========================================================================*/
int Sem_Wait(IN HANDLE hSem, IN DWORD dwWaitTimeout);


/*==========================================================================*
 * FUNCTION : Sem_Post
 * PURPOSE  : Release the semaphore.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hSem : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-04 20:13
 *==========================================================================*/
void Sem_Post(HANDLE hSem, int nPostCount);


/*==========================================================================*
 * FUNCTION : Sem_Destroy
 * PURPOSE  : To destroy a semaphore, the semaphore can not be used.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hSem : The HANDLE of semaphore will be destroyed
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-10-14 16:19
 *==========================================================================*/
void Sem_Destroy(IN HANDLE hSem);

#endif /*__RUN_MUTEX_H__*/


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : user_manage.h
 *  CREATOR  : HULONGWEN                DATE: 2004-09-28 12:17
 *  VERSION  : V1.00
 *  PURPOSE  : The declaration file of user management
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __USER_MANAGE_H__040928
#define __USER_MANAGE_H__040928

//#define	NEED_ES_ADMIN
//#define		NEED_ES_ENGINEER
//#define		NEED_EMERSONADMIN   //changed by Fengel hu ,2016-12-24, for BT security(kill user of emersonadmin). 
#include <time.h>

enum USER_MANAGE_ENUM
{
	DELETE_USER_INFO = 0,
	ADD_USER_INFO,
	MODIFY_USER_INFO
};

enum USER_NUM_GET_ENUM
{
	GET_MAXIMUM_USER_NUM = 0,
	GET_ACTUAL_USER_NUM,
};

#define USER_MANAGE_LOG	"USER MANAGE"

#define USERS_MAX_NUMBER   	16

#define ADMIN_DEFAULT_NAME			"admin"		//The administrator user name
#define ADMIN_DEFAULT_PASSWORD		"640275"			//The administrator user password


#define SUPER_ADMIN_NAME		"vertivadmin"		//The super administrator user name 
//changed by Fengel hu ,2016-12-24, for BT security(kill user of emersonadmin). 
#ifdef	NEED_EMERSONADMIN
#define SUPER_ADMIN_PASSWORD		"Vrt645"		//The super administrator user password
#endif

#ifdef	NEED_ES_ADMIN
#define ES_ADMIN_DEFAULT_NAME		"es_admin"	//The user name to modify Redundancy Enabled
#define ES_ADMIN_DEFAULT_PASSWORD	 "640275"	//The user password
#endif

#ifdef	NEED_ES_ENGINEER
#define ES_ENGINEER_DEFAULT_NAME		"engineer"	//The user name to modify Redundancy Enabled
#define ES_ENGINEER_DEFAULT_PASSWORD	 "123456"	//The user password
#endif

#define USERNAME_LEN  		16
#define PASSWORD_LEN  		25
//changed by Frank Wu,2/N/15,20140527, for e-mail
#define CONTACT_ADDR_LEN	48
#define LCD_ENABLE_LEN	10
#define STRONG_PASS_ENABLE_LEN	10
#define ACCOUNT_ENABLE_LEN	10

enum{
	INIT,
	LOCK,
	UNLOCK
};
struct tagUserLockout   // Added by Koustubh Mattikalli (IA-01-04)
{
    //timer_t timerid;
    //struct  itimerspec trigger;
    time_t  iTime;
    int     iCount;
    int     isLock;
	int 	iInterval;    
};
typedef struct tagUserLockout USER_LOCKOUT;

struct tagUserInfo
{
	char szUserName[USERNAME_LEN];	//User name
	char szPassword[PASSWORD_LEN];	//User password
	BYTE byLevel;			//User level
	//changed by Frank Wu,3/N/15,20140527, for e-mail
	char szContactAddr[CONTACT_ADDR_LEN];	  //e-mail address
	char szLcdEnable[LCD_ENABLE_LEN];         //lcd Enable
	char szAccountEnable[ACCOUNT_ENABLE_LEN]; //Account Enable
	char szAccountLock[ACCOUNT_ENABLE_LEN];   //Account Lock/Unlock
	int  iAccountCount; 			  //Account Count
	int  iAccountStatus; 			  //Account Status
	char szStrongPass[STRONG_PASS_ENABLE_LEN];
    USER_LOCKOUT stLockout;                   //Added by Koustubh Mattikalli (IA-01-04)
};
typedef struct tagUserInfo USER_INFO_STRU;

// Added by Koustubh Mattikalli (IA-01-04)
#define LOCK_INTERVAL   1*60*60     // 1 hour
#define UNLOCK_INTERVAL 1*30*60     // 1/2   hour
#define MAX_WRNGPASS    10 + 1

void user_lockout_init(void);
int user_lockout_enteredCorrectPass(USER_INFO_STRU *pUserInfo);
int user_lockout_enteredWrongPass(USER_INFO_STRU *pUserInfo, int validateOnly);


//Initialize user management module
BOOL InitUserManage(void);
void DestroyUserManager(void);

/*==========================================================================*
 * FUNCTION : GetUserNum
 * PURPOSE  : Get user number
 * CALLS    : 
 * CALLED BY: To be called by the services
 * ARGUMENTS: int nGetFlag : 0, Get maximum user number
								1, Get actual  user number
 * RETURN   : int : user number
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-09-28 17:30
 *==========================================================================*/
int GetUserNum(int nGetFlag);

/*==========================================================================*
 * FUNCTION : AddUserInfo
 * PURPOSE  : Add a user
 * CALLS    : UserExist
 * CALLED BY: To be called by the services
 * ARGUMENTS: char  *pszName     : user name
 *            char  *pszPassword : user password
 *            int   iAuthority   : user authority
 * RETURN   : int : error code
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-09-28 17:36
 *==========================================================================*/
int AddUserInfo(char *pszName, char *pszPassword, int iAuthority);
int AddUserInfo3(char *pszName, char *pszPassword, int iAuthority, char *pszLcdEnable, char *pszAccountEnable, char *pszAccountLock, int iAccountCount, int iAccountStatus, char *pszStrongPass);
/*==========================================================================*
 * FUNCTION : ModifyUserInfo
 * PURPOSE  : Modify user info
 * CALLS    : UserExist
 * CALLED BY: To be called by the services
 * ARGUMENTS: char  *pszName        : user name
 *            char  *pszNewPassword : new user password
 *            int   iNewAuthority   : new authority
 * RETURN   : int : error code
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-09-28 17:40
 *==========================================================================*/
int ModifyUserInfo(char *pszName, char *pszNewPassword, int iNewAuthority);
int ModifyUserInfo3(char *pszName, char *pszNewPassword, int iNewAuthority, char *pszLcdEnable, char *pszAccountEnable, char *pszAccountLock, int iAccountCount,int iAccountStatus, char *pszStrongPass);
/*==========================================================================*
 * FUNCTION : DeleteUserInfo
 * PURPOSE  : Delete a user info
 * CALLS    : UserExist
 * CALLED BY: To be called by the services
 * ARGUMENTS: char  *pszName : user name
 * RETURN   : int : error code
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-09-28 17:43
 *==========================================================================*/
int DeleteUserInfo(char *pszName);

/*==========================================================================*
 * FUNCTION : FindUserInfo
 * PURPOSE  : Find a user info according to user name
 * CALLS    : UserExist
 * CALLED BY: To be called by the services
 * ARGUMENTS: char            *pUserName : user name 
 *            USER_INFO_STRU  *pUserInfo : the return user info
 * RETURN   : int : error code
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-09-28 17:47
 *==========================================================================*/
int FindUserInfo(char *pUserName, USER_INFO_STRU *pUserInfo);
int FindUpdateUserInfo(char *pUserName, USER_INFO_STRU *pUserInfo);
// Added by Zicheng Zhao
int FindUser(char *pUserName);
char* FindUserAddr(int iFind);

/*==========================================================================*
 * FUNCTION : GetUserInfo
 * PURPOSE  : Get user info
 * CALLS    : 
 * CALLED BY: To be called by the services
 * ARGUMENTS: USER_INFO_STRU**  pUserInfo : the user info pointer
 * RETURN   : int : the actual user number
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-09-28 17:49
 *==========================================================================*/
int GetUserInfo(USER_INFO_STRU** pUserInfo);
int GetUserInfoForWeb(USER_INFO_STRU** pUserInfo);

//added for G3_OPT [config]
BOOL GetUserInfoFromRunFile(FILE *pf);

#define NMS_USERS_MAX_NUMBER   	16
#define V3_NMS_USERS_MAX_NUMBER 5
#define LEN_NMS_COMMUNITY	32

//Nms to the snmp agent of ACU
struct tagSNmsInfo
{
    unsigned long    ulIpAddress;    // the IP address of NMS, the key words
    IN6_ADDR		unIPV6Address;
   int				nPrivilege;    // the privilege of NMS, not be used 
   char				szPublicCommunity[LEN_NMS_COMMUNITY];  // Query community
   char				szPrivateCommunity[LEN_NMS_COMMUNITY]; // Set community
   int				nTrapLevel;    // The receiving trap level of Nms
   int				nTrapEnable;
   BYTE	    bIPV6;

};
typedef struct tagSNmsInfo NMS_INFO;

struct tagSV3NmsInfo
{
    char				szUserName[LEN_NMS_COMMUNITY];
    char				szPrivPwd[LEN_NMS_COMMUNITY];
    char				szAuthPwd[LEN_NMS_COMMUNITY];
    char				szEngineID[LEN_NMS_COMMUNITY * 2];
    unsigned long			ulTrapIpAddress;
    IN6_ADDR				ulTrapIpV6Address;
    int					nTrapSecurityLevel;
    int					nV3TrapLevel;
    int					nV3TrapEnable;
    BYTE				bIPV6;
};
typedef struct tagSV3NmsInfo V3NMS_INFO;

// // The receiving trap level enum of Nms
enum TRAP_LEVEL_ENUM
{
	TRAP_LEVEL_ALL			= ALARM_LEVEL_NONE,			//Accept all trap
	TRAP_LEVEL_OBSERVATION	= ALARM_LEVEL_OBSERVATION,		/* OA && MA && CA*/
	TRAP_LEVEL_MAJOR		= ALARM_LEVEL_MAJOR,			/* MA && CA*/
	TRAP_LEVEL_CRITICAL		= ALARM_LEVEL_CRITICAL,			/* CA only*/

};

//The operation enum for NMS info
enum NMS_INFO_MANAGE_ENUM
{
	GET_NMS_USER_INFO = 0,
	ADD_NMS_USER_INFO,
	MODIFY_NMS_USER_INFO,
	DELETE_NMS_USER_INFO,
	MODIFY_NMS_TRAP_INFO,
	DELETE_ALL_NMS_USER_INFO,

	GET_NMS_V3_USER_INFO, 
	ADD_NMS_V3_USER_INFO, 
	MODIFY_NMS_V3_USER_INFO,
	DELETE_NMS_V3_USER_INFO,
	DELETE_ALL_NMS_V3_USER_INFO,
	GETTRAP_TYPE_OF_NMS,
};

//The nms user info, used in snmp module
extern int	  g_nNmsUserNumber;
extern int	  g_V3nNmsUserNumber;
extern NMS_INFO g_NmsUserManage[];
extern V3NMS_INFO g_V3NmsUserManage[];
extern int	  g_nTrapLevel;

extern BOOL UpdateUserInfoToFlash(void);

#endif //__USER_MANAGE_H__040928

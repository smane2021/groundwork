/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : cfg_model.h
 *  CREATOR  : LinTao                   DATE: 2004-09-09
 *  VERSION  : V1.00
 *  PURPOSE  : define the data structure for ACU Equipment-signal model
 *
 *  HISTORY  :
 *==========================================================================*/

#ifndef __CFG_MODEL_H_2004_09_09__
#define __CFG_MODEL_H_2004_09_09__

//#pragma pack(1)	// make the alignment all struct to 1 byte. maofuhua,2004-11-14

#define PRODUCT_INFO_SUPPORT    //supporting vital product information,Added by YangGuoxin,1/12/2006

#define MAX_ALLOWED_CTRL_CMDS	(64 * 2)	// the maximum allowed pending cmds of the site
#define MAX_PENDING_CTRL_CMD	MAX_ALLOWED_CTRL_CMDS	// max pending command in a port
/*For scu plus*/
#define MAX_SCUP_SIGS	1500
#define MAX_RELAY_NUM	30

#define MAX_RECTNUM_CAN				80//60

/* define max number of a expression */
#define EXP_MAX_ELEMENTS	30
#define EXP_MAX_OPERAND		5

//----------------added by Jimmy for IPV6
#define DXI_VAR_ID_IPV4          0
#define DXI_VAR_ID_IPV6          1
//�����ipv6.h����������ֱ�Ӱ������벻��
#include "./basetypes.h"
struct _in6_addr
{
	union 
	{
		UINT8		u6_addr8[16];
		UINT16		u6_addr16[8];
		UINT32		u6_addr32[4];
	} in6_u;
#define S6_addr			in6_u.u6_addr8
#define S6_addr16		in6_u.u6_addr16
#define S6_addr32		in6_u.u6_addr32
};
typedef struct _in6_addr IN6_ADDR;
struct _in6_ifreq {
	IN6_ADDR	ifr6_addr;
	ULONG	ifr6_prefixlen;
	int		ifr6_ifindex; 
};
typedef struct _in6_ifreq IN6_IFREQ;
struct _sockaddr_in6 {
	unsigned short int	sin6_family;    /* AF_INET6 */
	UINT16			sin6_port;      /* Transport layer port # */
	UINT32			sin6_flowinfo;  /* IPv6 flow information */
	IN6_ADDR		sin6_addr;      /* IPv6 address */
	UINT32			sin6_scope_id;  /* scope id (new in RFC2553) */
};
typedef struct _sockaddr_in6 SOCKADDR_IN6;
struct tagAcuV6NetInfo
{
	IN6_IFREQ stLocalAddr;
	IN6_IFREQ stGlobalAddr;
	IN6_ADDR stGateWay;
};
typedef struct tagAcuV6NetInfo ACU_V6_NET_INFO;

enum IPV6_CHANGE_TYPE
{
	IPV6_CHANGE_ADDR = 0,
	IPV6_CHANGE_PREFIX,
	IPV6_CHANGE_ADDR_AND_PREFIX,
	IPV6_CHANGE_GW,
	IPV6_CHANGE_ALL
};

enum CODE_CHARACTER_TYPE
{
	ENUM_UTF8 = 0,
	ENUM_GB2312,
	ENUM_KOI8R
};
//-----------------------end

/* define alarm level */
enum ALARM_LEVELS_ENUM              /* NOTE: not change their values! */
{
	ALARM_LEVEL_NONE = 0,			/* NA */
	ALARM_LEVEL_OBSERVATION,		/* OA */
	ALARM_LEVEL_MAJOR,				/* MA */
	ALARM_LEVEL_CRITICAL,			/* CA */
	ALARM_LEVEL_MAX
};

/* define alarm relay */
enum ALARM_RELAYS_ENUM              /* NOTE: not change their values! */
{
	ALARM_RELAY_NONE = 0,	
	ALARM_RELAY_FIRST,	// 1 to 8 are  IB1 relay
	ALARM_RELAY_SECOND,	
	ALARM_RELAY_THIRD,	
	ALARM_RELAY_FOURTH,
	ALARM_RELAY_FIFTH,
	ALARM_RELAY_SIXTH,
	ALARM_RELAY_SEVENTH,
	ALARM_RELAY_EIGHTH,
	ALARM_RELAY_9TH,	// 9 to 13 are EIB1 relay.
	ALARM_RELAY_10TH,
	ALARM_RELAY_11TH,
	ALARM_RELAY_12TH,
	ALARM_RELAY_13TH,

	ALARM_RELAY_14TH,	// 14 to 17 are new relay on mainboard
	ALARM_RELAY_15TH,
	ALARM_RELAY_16TH,
	ALARM_RELAY_17TH,

	ALARM_RELAY_18TH,	//18 to 25 are IB2 relay
	ALARM_RELAY_19TH,
	ALARM_RELAY_20TH,
	ALARM_RELAY_21TH,
	ALARM_RELAY_22TH,
	ALARM_RELAY_23TH,
	ALARM_RELAY_24TH,
	ALARM_RELAY_25TH,
	

	ALARM_RELAY_26TH,	//26 to 30 are EIB2 relay
	ALARM_RELAY_27TH,
	ALARM_RELAY_28TH,
	ALARM_RELAY_29TH,
	ALARM_RELAY_30TH
	
};

// support the alarm level dynamic changes. maofuhua, 2005-3-3
#define ALARM_LEVEL_DYNAMICALLY_CHANGE		1 


enum SIG_TYPE
{
	SIG_TYPE_SAMPLING = 0,	// MUST BE START FROM 0
	SIG_TYPE_CONTROL,	    // MUST BE BEFORE SIG_TYPE_ALARM
	SIG_TYPE_SETTING,		// MUST BE BEFORE SIG_TYPE_ALARM
	SIG_TYPE_ALARM,			// MUST BE AFTER OF SIG_TYPE_SETTING
	//SIG_TYPE_RELAY,
	SIG_TYPE_MAX			// MUST BE THE LAST!
};

enum AUTHORITY_LEVEL
{
	BROWSER_LEVEL = 1,
	OPERATOR_LEVEL,
	ENGINEER_LEVEL,
	ADMIN_LEVEL,
	AUTHORITY_LEVEL_MAX
};

enum PROTOCOL_TYPE
{
	PROTOCOL_EEM,
	PROTOCOL_YDN23,
	PROTOCOL_MODBUS,
	PROTOCOL_TL1//changed by Frank Wu,N/N/N,20150429, for supporting TL1

};

// virtual sample channel definition, used in iSamplerChannel of struct
//NOTE:
// The sampling channel of communication status of sampler is -1. this is
// compliant with PSMS system.
#define COMM_STATUS_CHANNEL		 (-1)	// channel of comm status of sampler.
#define VIRTUAL_SAMPLING_CHANNEL (-2)	// the virtual channel which value 
										// is cacluated from expression
#define VIRTUAL_INTERNAL_CHANNEL (-3)	// the internal channel whihc value 
										// is set by the General Controller service
// is set by the General Controller service
#define VIRTUAL_AUTORESET_CHANNEL    (-4)   //index to reset a signal after control, used by Control Sig, Added by Thomas, 2007-3-9


/* Define config changed types
 *(used by dwConfigMask of CONFIG_CHANGED_INFO structure) */
#define CONFIG_CHANGED_SITENAME				0x1
#define CONFIG_CHANGED_EQUIPNAME			0x2
#define CONFIG_CHANGED_SIGNAME				0x4
#define CONFIG_CHANGED_ALARMLEVEL			0x8
#define CONFIG_CHANGED_RESTORE				0x10

#define CONFIG_CHANGED_ALARMRELAY			0x80 //added by ht, 2006.11.2

#define CONFIG_CHANGED_SITELOCATION			0x20
#define CONFIG_CHANGED_SITEDESCRIPTION		0x40

/* Define display attribute */
#define DISPLAY_NONE	0
#define	DISPLAY_WEB		0x1
#define	DISPLAY_LCD		0x2

/* Define control attribute */
#define CONTROL_NONE	0
#define	CONTROL_WEB		0x1
#define	CONTROL_LCD		0x2

/* Define set attribute */
#define SET_NONE	0
#define	SET_WEB		0x1
#define	SET_LCD		0x2

#define FLAG_IS_SET(flgs, aflg)     ((((flgs)&(aflg))==(aflg)))
#define FLAG_IS_EQUAL(flg, aflg)     (((flg) == (aflg)))


#define STD_SIG_IS_DISPLAY_ON_UI(pStdSig, webOrLcdFlag)	\
	FLAG_IS_SET((pStdSig)->byValueDisplayAttr, (webOrLcdFlag))
#define CTRL_SIG_IS_CTRL_ON_UI(pCtrlSig, webOrLcdFlag)	\
	FLAG_IS_SET((pCtrlSig)->byValueControlAttr, (webOrLcdFlag))
#define SET_SIG_IS_SET_ON_UI(pSetSig, webOrLcdFlag)	\
	FLAG_IS_SET((pSetSig)->byValueSetAttr, (webOrLcdFlag))

/*==========================================================================*
 *  
 *    Std Config Structures
 *
 *==========================================================================*/

/* Element Definitions: */
/* variable element     */ 
struct tagExpVariable			
{
	int	iRelevantEquipIndex;	
	int	iSigType;     	
	int	iSigID;	
};
typedef struct tagExpVariable EXP_VARIABLE;

/* operator element */
struct tagExpOperator			
{
	BYTE	byOperatorType;	
	BYTE	byPriority;	
};
typedef struct tagExpOperator EXP_OPERATOR;

/* const element */
typedef float	EXP_CONST;		


/* General Expression Element discription */
struct tagExpElement	
{
	BYTE 	byElementType;	/* defined by TypesUsedByExp enum const */
	void	* pElement;	
};
typedef struct tagExpElement EXP_ELEMENT, *PEXP_ELEMENT;


/* used for control or setting signal, after control or set the signal, 
   assign the value to the variable */
struct tagCfgOnCtrlAction
{
	EXP_VARIABLE expVar;
	float        fValue;
};
typedef struct tagCfgOnCtrlAction CFG_ON_CTRL_ACTION;

#define CTRL_SIG_MAX_ON_CTRL_DEPTH	5	// at list 5 level on-ctrl deep.


//runtime struct see ON_CTRL_ACTION 

/* sig value definitions */
union tagVarValue	
{
	long			lValue;		
	float			fValue;		
	unsigned long	ulValue;		
	SIG_TIME		dtValue;		
	SIG_ENUM		enumValue;		
};
typedef union tagVarValue VAR_VALUE;

/*for G3*/
/*
struct SetInformation
{
	int EquipID;
	int SigID;
	int SigType;
	VAR_VALUE value;
};
typedef struct SetInformation SET_INFO;

struct CmdItem
{
	int iMapHandle;
	int CmdType;
	int ScreenID;
	SET_INFO setinfo;

};
typedef struct CmdItem CMD_INFO;

struct PackGetInfo
{
	int		iEquipID;
	int		iSigID;
	char		cSigName[20];
	int		iSigType;
	VAR_VALUE	vSigValue;

	char		cSigUnit[5];
	char		cEnumText[20];
};
typedef struct PackGetInfo PACK_INFO;

struct IcoInfo
{
	int		iEquipID;
	int		iSigID;
	char		cSigName[20];
	float		fSigValue;
	char		cSigUnit[5];
};
typedef struct IcoInfo ICO_INFO;

struct Pack_ACIco
{
	ICO_INFO	ACVolt[3];
	float		H1Volt;
	float		L1Volt;
	float		L2Volt;
};
typedef struct Pack_ACIco PACK_ACICOINFO;

struct ActAlmInfo
{
	char		AlmName[20];
	char		EquipName[20];
	int		AlmLevel;
	time_t		StartTime;
	char		EquipSN[32];
	char		AlmHelp[100];
};
typedef struct ActAlmInfo ACTALM_INFO;

struct PackActAlmInfo
{
	int		ActAlmNum;
	ACTALM_INFO	ActAlmItem[200];
};
typedef struct PackActAlmInfo PACK_ACTALM;

struct PackAlmNumInfo
{
	int		OANum;
	int		MANum;
	int		CANum;
};
typedef struct PackAlmNumInfo PACK_ALMNUM;

typedef struct HisCondition
{
	int 	iEquipID;
	int	iNum;			
}HIS_CONDITION;

struct HisAlmInfo
{
	char		EquipName[20];
	char		AlmName[20];
	BYTE		AlmLevel;
	time_t		StartTime;
	time_t		EndTime;
	char		EquipSN[32];
	char		AlmHelp[100];
};
typedef struct HisAlmInfo HISALM_INFO;

struct PackHisAlmInfo
{
	int		HisAlmNum;
	HISALM_INFO	HisAlmItem[500];
};
typedef struct PackHisAlmInfo PACK_HISALM;*/
/***/


/* Define ucType in SIG_BASIC_VALUE structure */
#define VAR_LONG			1
#define VAR_FLOAT			2
#define	VAR_UNSIGNED_LONG	3
#define	VAR_DATE_TIME		4
#define	VAR_ENUM			5

#define	VAR_STRING			6	//added by HULONGWEN
#define	VAR_MULTI_SECT		7	//added by HULONGWEN
#define	VAR_PHONE_NO		8	//added by Frank Cao

// to test the var value is equal or NOT.
#define VAR_IS_NOT_EQUAL(varType, pVar1, pVar2)		\
	(((varType) != VAR_FLOAT) ? ((pVar1)->lValue != (pVar2)->lValue) \
                 : FLOAT_NOT_EQUAL((pVar1)->fValue, (pVar2)->fValue))
#define VAR_IS_EQUAL(varType, pVar1, pVar2)		\
	(((varType) != VAR_FLOAT) ? ((pVar1)->lValue == (pVar2)->lValue) \
                 : FLOAT_EQUAL((pVar1)->fValue, (pVar2)->fValue))
#define VAR_SET_VALUE(pVar, varType, val)	\
	(((varType) != VAR_FLOAT) ? ((pVar)->lValue = (long)(val)) \
				 : ((pVar)->fValue = (float)(val)))


/* Define usStateMask in SIG_BASIC_VALUE structure*/
// about the bit definition of the 8bit usStateMask
// low  8bit are public bit for all signals
// high 8bit are shared for all signals

//1. LOW 8-bit definition. the HIGH byte must be 00
#define VALUE_STATE_IS_VALID	    0x0001
#define	VALUE_STATE_IS_SUPPRESSED	0x0002	// set when value of exp of suppression, settable, ctrlable is non-zero 
#define VALUE_STATE_IS_CONTROLLABLE VALUE_STATE_IS_SUPPRESSED	// share with suppression flag
#define VALUE_STATE_IS_SETTABLE		VALUE_STATE_IS_CONTROLLABLE
#define VALUE_STATE_IS_VALID_RAW	0x0004	// the sampled raw data is valid.
#define VALUE_STATE_IS_CONFIGURED	0x0008	// the signal is configured.
#define VALUE_STATE_IS_DEADLOCKED	0x0010	// the setting signal is in deadlock state. maofuhua, 2005-4-23
#define VALUE_STATE_NEED_DISPLAY	0x0020	// the signal need to be displayed on LCD/Web

//2. HIGH 8-bit definition. the LOW byte must be 00 
// alarm new beginning and new end flag. used by equip_data_process.c moudle.
#define	ALARM_STATE_IS_NEW_BEGIN				0x0100	// is new begin,not saved and reported
#define	ALARM_STATE_IS_NEW_END					0x0200	// is new end, not saved and reported
#define	ALARM_STATE_IS_NEW_BEGIN_THEN_SUPPRESSED 0x0400	// is new begin, and then suppressed
#define	ALARM_STATE_IS_NEW_END_WHILE_SUPPRESSED 0x0800	// is new ended by condition while suppressing, it is already ended by suppression 
#define ALARM_STATE_IS_NEW_LEVEL_CHANGED		0x1000	// alarm level is changed.

#define ALARM_STATE_OF_BEGIN_END_FLAG	(ALARM_STATE_IS_NEW_BEGIN|ALARM_STATE_IS_NEW_END|	\
		ALARM_STATE_IS_NEW_BEGIN_THEN_SUPPRESSED|ALARM_STATE_IS_NEW_END_WHILE_SUPPRESSED)

//3 the high 8-bit used by sampling signals.
#define SAMPLING_SIG_ALARM_STATE_IS_UPDATED		0x1000	// the alarm state of the sampling signal is updated.

// set a state bit of a signal vlaue
#define SIG_STATE_SET(pSig, aState)	 (((SIG_BASIC_VALUE*)(pSig))->usStateMask |= (aState))
// clear a state bit of a signal vlaue
#define SIG_STATE_CLR(pSig, aState)	 (((SIG_BASIC_VALUE*)(pSig))->usStateMask &= ~(aState))
// test a state bit of a signal vlaue
#define SIG_STATE_TEST(pSig, aState) \
				((((SIG_BASIC_VALUE*)(pSig))->usStateMask & (aState)) == (aState))
// get the masked state
#define SIG_STATE_GET(pSig, aState) \
				(((SIG_BASIC_VALUE*)(pSig))->usStateMask & (aState))

// to test a sig is valid or invalid
#define SIG_VALUE_IS_VALID(pSig)	\
	(SIG_STATE_TEST((pSig), (VALUE_STATE_IS_VALID|VALUE_STATE_IS_CONFIGURED)))

// to test a sig is configured or NOT
#define SIG_VALUE_IS_CONFIGURED(pSig)	\
	(SIG_STATE_TEST((pSig), VALUE_STATE_IS_CONFIGURED))

// to test a sig is suppressed or not
#define SIG_VALUS_IS_SUPPRESSED(pSig)	\
	(SIG_STATE_TEST((pSig), VALUE_STATE_IS_SUPPRESSED))

// to test a sig is settable/ctrlable or not
#define SIG_VALUE_IS_SETTABLE(pSig)	 \
	(SIG_STATE_TEST((pSig), VALUE_STATE_IS_SETTABLE))

#define SIG_VALUE_IS_CONTROLLABLE(pSig)	 	\
	(SIG_STATE_TEST((pSig), VALUE_STATE_IS_CONTROLLABLE))

#define SIG_VALUE_NEED_DISPLAY(pSig)	 	\
	(SIG_STATE_TEST((pSig), VALUE_STATE_NEED_DISPLAY))

// to test an alram is activing or not: value is non-zero, and not suppressed.
#define ALARM_IS_ACTIVING(pSig)	\
	((((SIG_BASIC_VALUE *)(pSig))->varValue.lValue != 0) &&	\
	(!SIG_STATE_TEST((pSig), VALUE_STATE_IS_SUPPRESSED)) ? TRUE : FALSE)

// to test the alarm changed state
#define ALARM_STATE_GET(pSig)	(SIG_STATE_GET((pSig), ALARM_STATE_OF_BEGIN_END_FLAG))
#define ALARM_STATE_CLR(pSig)	(SIG_STATE_CLR((pSig), ALARM_STATE_OF_BEGIN_END_FLAG))

// basic value of all types signal
struct tagSigBasicValue
{
	VAR_VALUE	    varValue;
	unsigned char	ucSigType;
	unsigned char	ucType;		/* sig value type, use macro */
	unsigned short	usStateMask;/* sig value state, use macro */
	time_t			tmCurrentSampled;
	VAR_VALUE		varLastValue;

	float			fRawData;	// sampled raw data. used by equipment mgr
};
typedef struct tagSigBasicValue	SIG_BASIC_VALUE;


struct tagSigStatValue		   /* stat. data in onr day, used by analogue sampling sig */
{	
	time_t		tmMinValue;	 
	VAR_VALUE	varMinValue;	 
	time_t		tmMaxValue;	 
	VAR_VALUE	varMaxValue;	 
	VAR_VALUE	varAverageValue;	
	int			iValueCount;	/* the number of values which is used to calculate varAverageValue */
};
typedef struct tagSigStatValue SIG_STAT_VALUE;


/* aheaded reference */
typedef struct tagSampleSigInfo	SAMPLE_SIG_INFO;
typedef struct tagCtrlSigInfo CTRL_SIG_INFO;
typedef struct tagSetSigInfo SET_SIG_INFO;
typedef struct tagAlarmSigInfo ALARM_SIG_INFO;
typedef struct tagEquipInfo EQUIP_INFO;

/* for alarm sig value */
typedef struct tagAlarmSigValue	ALARM_SIG_VALUE;
struct tagAlarmSigValue					 
{
	SIG_BASIC_VALUE	bv;					  /* NOTE: MUST BE THE FIRST  ITEM! basic value */
	ALARM_SIG_INFO	*pStdSig;			  /* NOTE: MUST BE THE SECOND ITEM! reference of related ALARM_SIG_INFO structure */

	// the application service needs when receives a reported alarm.
	EQUIP_INFO      *pEquipInfo;           /* reference of related EQUIP_INFO, used by application services */
				
	struct {					          /* special value of alarm sig */
#define INVALID_TIME		-1		// invalid time indicates the alarm not start
	time_t			tmStartTime;		  /* alarm start time */
	time_t			tmEndTime;			  /* alarm end time */
	int				nDelayingTime;		  // the time to the alarm rise. if 0, the alarm will be rised.
				
	int				iAlarmExpression;	  /* element count of Alarm Expression */
	EXP_ELEMENT		*pAlarmExpression;	  /* Note: the variable element now has been mapped to sig addr! */
	int				iSuppressExpression;  /* element count of Suppress Expression */
	EXP_ELEMENT		*pSuppressExpression; /* Note: the variable element now has been mapped to sig addr! */
	} sv;

	ALARM_SIG_VALUE	*prev;				  /* for active alarm bi-direction link */
	ALARM_SIG_VALUE	*next;	

#ifdef	ALARM_LEVEL_DYNAMICALLY_CHANGE
	int				iAlarmLevel;		  /* current alarm level which in alarming, use ALARM_LEVELS_ENUM enum const */
#endif
	char	szEquipSerial[32];
	int	iPosition;
};
//typedef struct tagAlarmSigValue	ALARM_SIG_VALUE;

struct tagSamplingSigValue				  /* for sampling sig value */
{
	SIG_BASIC_VALUE	bv;					  /* NOTE: MUST BE THE FIRST  ITEM! basic value */
	SAMPLE_SIG_INFO	*pStdSig;			  /* NOTE: MUST BE THE SECOND ITEM! reference of related SAMPLE_SIG_INFO structure */
	
	/* Special Value of sampling sig */
	struct 
	{
		int				iSamplerID;			  // NOTE: MUST BE	THE THRID ITEM!
		int				iSamplerChannel;	  // NOTE: MUST BE	THE FOURTH ITEM!
		
		int				iCalculateExpression; /* element count of Calculate Expression */
		EXP_ELEMENT		*pCalculateExpression; /* Note: the variable element now has been mapped to sig addr! */
	
		int				iRelatedAlarms;		  /* the number of related alarm sig */
		ALARM_SIG_VALUE	**ppRelatedAlarms;	  /* reference list of related alarm sig */
		char			iRelatedAlarmLevel;	  /* highest alarm level related to the sig */

		/* history data, only for the sig configed with StoringThreshold or StoringInterval */
		time_t			tmLastSaved;		  /* last time to save */
		VAR_VALUE		varLastSaved;		  /* last value */
		
		SIG_STAT_VALUE	*pStatValue;		  /* only for analogue sig, NULL for others */
		
		int				iDispExp;			/* element count of Disp Expression */
		EXP_ELEMENT		*pDispExp;			/* Note: the variable element now has been mapped to sig addr! */
	} sv;		 
};
typedef struct tagSamplingSigValue SAMPLE_SIG_VALUE;


struct tagOnCtrlAction	// runtime structure
{
	EQUIP_INFO		*pEquip;	// the owneer of the pSig.
	SIG_BASIC_VALUE *pSig;
	float			fValue;
};
typedef struct tagOnCtrlAction ON_CTRL_ACTION;



struct tagCtrlSigValue					  /* for control sig value */
{
	SIG_BASIC_VALUE	bv;					  /* NOTE: MUST BE THE FIRST  ITEM! basic value */
	CTRL_SIG_INFO	*pStdSig;			  /* NOTE: MUST BE THE SECOND ITEM! reference of related CTRL_SIG_INFO structure */
	
	/* special value of control sig */			
	struct 
	{
		int			iSamplerID;				  // NOTE: MUST BE	THE THRID ITEM!
		int			iSamplerChannel;		  // NOTE: MUST BE	THE FOURTH ITEM!
		int			iControlChannel;		  // NOTE: MUST BE	THE FIFTH ITEM!
		
		ON_CTRL_ACTION onCtrl;				  // NOTE: MUST BE THE  SIXTH ITEM!!!

		int			iControllableExpression;  /* element count of Controllable Expression */
		EXP_ELEMENT	*pControllableExpression; /* Note: the variable element now has been mapped to sig addr! */	

		int				iDispExp;			/* element count of Disp Expression */
		EXP_ELEMENT		*pDispExp;			/* Note: the variable element now has been mapped to sig addr! */

	} sv;
};
typedef struct tagCtrlSigValue CTRL_SIG_VALUE;

struct tagSetSigValue	          /* for setting sig value */
{
	SIG_BASIC_VALUE	bv;				      /* NOTE: MUST BE THE FIRST  ITEM! basic value */
	SET_SIG_INFO	*pStdSig;	          /* NOTE: MUST BE THE SECOND ITEM! reference of related SET_SIG_INFO structure */
	
	/* special value of setting sig */
	struct
	{						      
		int			iSamplerID;				  // NOTE: MUST BE	THE THRID ITEM!
		int			iSamplerChannel;		  // NOTE: MUST BE	THE FOURTH ITEM!
		int			iControlChannel;	      // NOTE: MUST BE	THE FIFTH ITEM!
		ON_CTRL_ACTION onCtrl;				  // NOTE: MUST BE THE  SIXTH ITEM!!!

		int			iSettableExpression;	  /* element count of Settable Expression */
		EXP_ELEMENT	*pSettableExpression;     /* Note: the variable element now has been mapped to sig addr! */

		int				iDispExp;			/* element count of Disp Expression */
		EXP_ELEMENT		*pDispExp;			/* Note: the variable element now has been mapped to sig addr! */

	} sv;								 
};
typedef struct tagSetSigValue SET_SIG_VALUE;



/* used to describe language resource file infomation (to support multi-language */
#define MAX_LANG_TYPE	2 //Modified by wj for three languages 2006.4.27
struct tagLangText
{
	int		iResourceID;	
	int		iMaxLenForFull;	
	int		iMaxLenForAbbr;	
	char	*pFullName[MAX_LANG_TYPE];		/* full name, element 0 is En */
	char	*pAbbrName[MAX_LANG_TYPE];		/* abbreviated name, element 0 is En */
};
typedef struct tagLangText LANG_TEXT;

struct tagLangFile							/* language resource file info */
{
	int			iLangTextNum;
	LANG_TEXT	*pLangText;
};
typedef struct tagLangFile LANG_FILE;

struct tagLangInfo							/* language info for solution */
{
    char		*szLocaleLangCode;			/* defined by  ISO-639 */
    char        *szLocale2LangCode;         /* Added by wj for three languages 2006.04.27 */
	int			iMaxFullNameLen;
	int			iMaxAbbrNameLen;
	
	int			iLangFileNum;
	int			iCodeType;	// if GB2312, should change from UTF-8 TO GB2312,
						//If KOI8-R, should change from UTF-8 to KOI8-R.
	LANG_FILE	*pLangFile;
	LANG_FILE   *pCurLangFile;             /* current lang file reference */
};
typedef struct tagLangInfo LANG_INFO;


/* for standard port */
#define BASIC_PORT_TCP		1
#define BASIC_PORT_UPD		2
#define BASIC_PORT_SERIAL	3
#define BASIC_PORT_DIAL		4
#define BASIC_485_PORT		5
#define BASIC_CAN			6
#define BASIC_I2C			7

struct tagStdPortInfo
{
	int	 iPortTypeID;	 // NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	char *szTypeName;	
	int	 iBasicType;      /* support: 1:TCP(Ethernet) 
						              2:UDP(Ethernet) 
									  3:Standard Serial 
						              4:Dialing Serial (PSTN, Wireless, ISDN Modern)
									  5:RS 485
									  6:CAN, but it's a virtual port, open, read and write functions are realized in sampler
									  7:I2C, but it's a virtual port, open, read and write functions are realized in sampler*/
	char *szPortAccessingDriver;	/* driver name */
};
typedef struct tagStdPortInfo STDPORT_INFO;


/* for standard sampler */
#define SAMPLER_READ_PROC_NAME	"Read"
typedef BOOL (*SAMPLER_READ_PROC)  /* read function prototype */
(
	HANDLE	hComm,			/* port handle */
	int	    iUnitNo,		/* sampler unit ID */
	void*	pData			/* buffer to load sampled data (float) */
); 

#define SAMPLER_WRITE_PROC_NAME	"Write"
typedef BOOL (*SAMPLER_WRITE_PROC)  /* read function prototype */
(
	HANDLE	hComm,			/* port handle */
	int	    iUnitNo,		/* sampler unit ID */
	void*	pData			/* buffer to load sampled data (float) */
); 

typedef BOOL (*SAMPLER_ENUM_PROC)  /* callback function prototype for channel signal */
(
	int	    iChanelNo,		/* channel number */
	float	fValue,			/* data of the channel */
	LPVOID	pParam	
);

#define SAMPLER_QUERY_PROC_NAME	"Query"
typedef BOOL (*SAMPLER_QUERY_PROC)  /* query function prototype */
(
	HANDLE	hComm,			/* port handle */
	int	    iUnitNo,		/* sampler unit ID */
	SAMPLER_ENUM_PROC	pfnEnumProc,	
	LPVOID	lpvoid			/* parameter can used by pfnEnumProc */
);

#define SAMPLER_CONTROL_PROC_NAME	"Control"
typedef BOOL (*SAMPLER_CONTROL_PROC)  /* control function prototype */
(
	HANDLE	hComm,			/* port handle */ 
	int	    iUnitNo,		/* sampler unit ID */ 
	char	*pCmdStr		/* contro parameter */
);

struct tagStdSamplerInfo    /* standard sampler structure */
{
	int	    iStdSamplerID;	// NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!	
	char	*szStdSamplerName;	
	char	*szStdSamplerDriver;	
	int	    iSamplerAttr;	
				
	/* runtime info */
	int					  iRef;			  /* refered count */
	HANDLE	              hSamplerLib;	 
	SAMPLER_READ_PROC	  pfnRead;	 
	SAMPLER_CONTROL_PROC  pfnControl;	 
	SAMPLER_QUERY_PROC	  pfnQuery;	
	SAMPLER_WRITE_PROC    pfnWrite;
};
typedef struct tagStdSamplerInfo STDSAMPLER_INFO;


/* for standard data communication protocol */
struct tagStdDCPInfo
{
	int	 iDCPID;	// NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	char *szDCPName;		
	char *szDCPDriver;	/* The Driver Name */
	int	 iMaxHLMSAllowed;		
	int	 iDCPAttr;  	
};
typedef struct tagStdDCPInfo STDDCP_INFO;


/* for sampling sig attribute definition */
struct tagSampleSigInfo	
{
	int			iSigID;					// NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	LANG_TEXT	*pSigName;				// NOTE: MUST BE THE SECOND ITEM! DO NOT MOVE!
	char		*szSigUnit;				// NOTE: MUST BE THE THIRD  ITEM! DO NOT MOVE!
	int			iSigValueType;			// NOTE: MUST BE THE FOURTH ITEM! DO NOT MOVE!
											/* 1: Long		     2: Float 
											   3: Unsigned Long  4: Date/Time
										       5: Enumeration    */	
	float		fMinValidValue;			// NOTE: MUST BE THE FIFTH ITEM! DO NOT MOVE!
	float		fMaxValidValue;			// NOTE: MUST BE THE SIXTH ITEM! DO NOT MOVE!
	int			iValueDisplayID;		// NOTE: MUST BE THE EVENTH ITEM! DO NOT MOVE!/* determine display order */

	int			iIndexofSamplerList;	/* index of pRelevantSamplerList of EQUIP_INFO */
	int			iSamplerChannel;	    /* relevant channel number,   
										   -1: sampler communication status 
										   -2: virtual singal from expression 
										   -3: virtual signal from control logic */
	float		fStoringThreshold;	
	int			iStoringInterval;	    /* Unit: S */
	int			iCalculateExpression;	/* element count of Calculat Expression */
	EXP_ELEMENT	*pCalculateExpression;	/* dynamic created pointer array, use Reverse Polish Notation */
	int         iStateNum;				/* state count for Enumeration sig value */
	LANG_TEXT	**pStateText;	    	/* text discription for enumeration sig value, pointer array */		
	BYTE		byValueDisplayAttr;	    /* display attribte, use macro */ 
	int			iValueDisplayLevel;		/* display sig only when sig alarm level is higher than it */
	char		*szValueDisplayFmt;		/* display format */ 
	int			iDispExp;				/* element count of Expression */
	EXP_ELEMENT	*pDispExp;				/* dynamic created pointer array, use Reverse Polish Notation */

};
//typedef struct tagSampleSigInfo	SAMPLE_SIG_INFO;

/* for control sig attribute definition */
struct tagCtrlExpression		/* for control tip message */
{
	float	   fThreshold;	/* when value is greater, show the tip */
	LANG_TEXT  *pText;		/* tip message */
};
typedef struct tagCtrlExpression CTRL_EXPRESSION;

struct tagCtrlSigInfo	/* control sig attr definition */
{
	int			iSigID;					// NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	LANG_TEXT	*pSigName;				// NOTE: MUST BE THE SECOND ITEM! DO NOT MOVE!
	char		*szSigUnit;				// NOTE: MUST BE THE THIRD  ITEM! DO NOT MOVE!
	int			iSigValueType;			// NOTE: MUST BE THE FOURTH ITEM! DO NOT MOVE!
											/* 1: Long		     2: Float 
											   3: Unsigned Long  4: Date/Time
										       5: Enumeration    */	
	float		fMinValidValue;			// NOTE: MUST BE THE FIFTH ITEM! DO NOT MOVE!
	float		fMaxValidValue;			// NOTE: MUST BE THE SIXTH ITEM! DO NOT MOVE!
	int			iValueDisplayID;		// NOTE: MUST BE THE EVENTH ITEM! DO NOT MOVE!/* determine display order */

	int			iIndexofSamplerList;	  /* index of pRelevantSamplerList of EQUIP_INFO */
	int				iSamplerChannel;		/* relevant channel number,   
											    -1: sampler communication status 
												-2: virtual singal from expression 
										        -3: virtual signal from control logic */

	VAR_VALUE	defaultValue;	
	int         iStateNum;				  /* state count for Enumeration sig value */
	LANG_TEXT	**pStateText;			  /* text discription for enumeration sig value, pointer array */
	BYTE		byValueDisplayAttr;		  /* display attribte, use macro */ 
	BYTE		byValueControlAttr;	      /* control attribte, use macro */ 
	int			iControllableExpression;  /* element count of Controllable Expression */
	EXP_ELEMENT	*pControllableExpression; /* dynamic created pointer array, use Reverse Polish Notation */
	BYTE		byAuthorityLevel;		  /* authority level needed to control current sig */
	char		*szValueDisplayFmt;		  /* display format */ 
	int			iControlChannel;	      /* relevant channel number */
	float		fControlStep;	
	char		*szCtrlParam;	
	int			iCtrlExpression;		  /* number of CTRL_EXPRESSIONs */ 
	CTRL_EXPRESSION	*pCtrlExpression;	  /* used to show tip message */
	CFG_ON_CTRL_ACTION	OnCtrl;			  /* used to assign the value to the certain sig */
	int			iDispExp;				/* element count of Expression */
	EXP_ELEMENT	*pDispExp;				/* dynamic created pointer array, use Reverse Polish Notation */

};
//typedef struct tagCtrlSigInfo CTRL_SIG_INFO;

/* for setting sig attribute definition */
struct tagSetSigInfo	
{
	int				iSigID;					// NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	LANG_TEXT		*pSigName;				// NOTE: MUST BE THE SECOND ITEM! DO NOT MOVE!
	char			*szSigUnit;				// NOTE: MUST BE THE THIRD  ITEM! DO NOT MOVE!
	int				iSigValueType;			// NOTE: MUST BE THE FOURTH ITEM! DO NOT MOVE!
											/* 1: Long		     2: Float 
											   3: Unsigned Long  4: Date/Time
										       5: Enumeration    */	
	float			fMinValidValue;			// NOTE: MUST BE THE FIFTH ITEM! DO NOT MOVE!
	float			fMaxValidValue;			// NOTE: MUST BE THE SIXTH ITEM! DO NOT MOVE!
	int				iValueDisplayID;		// NOTE: MUST BE THE EVENTH ITEM! DO NOT MOVE!/* determine display order */

	int				iIndexofSamplerList;	/* index of pRelevantSamplerList of EQUIP_INFO */
	int				iSamplerChannel;		/* relevant channel number,   
											    -1: sampler communication status 
												-2: virtual singal from expression 
										        -3: virtual signal from control logic */
	VAR_VALUE		defaultValue;	
	int             iStateNum;				/* state count for Enumeration sig value */
	LANG_TEXT		**pStateText;			/* text discription for enumeration sig value, pointer array */
	int				iSettableExpression;	/* element count of Settable Expression */
	EXP_ELEMENT		*pSettableExpression;	/* dynamic created pointer array, use Reverse Polish Notation, dynamic reated pointer array */
	BYTE			byValueDisplayAttr;		/* display attribte, use macro */ 
	BYTE			byValueSetAttr;	 		/* set attribte, use macro */ 
	BYTE			byAuthorityLevel;		/* authority level needed to set current sig */
	char			*szValueDisplayFmt;		/* display format */ 
	int				iControlChannel;	    /* relevant channel number */
	float			fSettingStep;	 
	int				iCtrlExpression;		/* number of CTRL_EXPRESSIONs */
	CTRL_EXPRESSION	*pCtrlExpression;		/* used to show tip message */ 
	BOOL			bPersistentFlag;		/* need to stored in the flash flag */
	CFG_ON_CTRL_ACTION	OnCtrl;			    /* used to assign the value to the certain sig */
	int			iDispExp;				/* element count of Expression */
	EXP_ELEMENT	*pDispExp;				/* dynamic created pointer array, use Reverse Polish Notation */

};
//typedef struct tagSetSigInfo SET_SIG_INFO;

/* for alarm sig attribute definition */
struct tagAlarmSigInfo
{
	int			iSigID;					// NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	LANG_TEXT	*pSigName;				// NOTE: MUST BE THE SECOND ITEM! DO NOT MOVE!

	int			iAlarmLevel;				/* alarm level, use ALARM_LEVELS_ENUM enum const */
	int			iAlarmExpression;			/* element count of Alarm Experssion */
	EXP_ELEMENT	*pAlarmExpression;			/* dynamic created pointer array, used to compute the alarm, adopt Reverse Polish Notation */
	int			iAlarmDelay;					/* Unit: S */
	int			iSuppressExpression;		/* element count of Suppress Experssion */
	EXP_ELEMENT	*pSuppressExpression;		/* dynamic created pointer array, use Reverse Polish Notation */
    int         iAlarmRelayNo;                //Added by wj for saving Alarm Relay No.Use ALARM_LEVELS_ENUM enum const

	LANG_TEXT	*pHelpInfo;				//added for G3 req., by Lin.Tao.Thomas, 2013-5

};
//typedef struct tagAlarmSigInfo ALARM_SIG_INFO;
struct tagBATT_NUM_INFO
{
    int					iCfgQtyOfComBAT;
    int					iCfgQtyOfEIBBAT1;
    int					iCfgQtyOfSmduBAT1;
    int					iCfgQtyOfEIBBAT2;
    int					iCfgQtyOfSmduBAT2;
    int					iCfgQtyOfLargeDUBAT;
    int					iCfgQtyOfSMBAT;//��¼SMBAT Battery����
    int					iCfgQtyOfSMBRC;//��¼SMBRC Battery����
	int					iCfgQtyOfSMDUEBAT;
};
typedef struct tagBATT_NUM_INFO BATT_NUM_INFO;
//Added by YangGuoxin,1/12/2006
// convert signal value to serial number string.
typedef char *(*EQUIP_SERIAL_CONVERT_PROC)(IN char *pRawSerial, 
										   IN int nRawLen,
										   OUT char *pStrSerial,
										   IN int nStrLen);
										   
typedef char *(*EQUIP_SERIAL_CONVERT_PROC2)(IN char *pRawSerial, IN char *pRawSerial2,
										   IN int nRawLen,
										   OUT char *pStrSerial,
										   IN int nStrLen);										   
#ifdef PRODUCT_INFO_SUPPORT
typedef char *(*EQUIP_PI_CONVERT_PROC)(IN char *pRawSig,
									   IN int nRawLen, 
									   OUT char *pStrInfo, 
									   IN int nStrLen);
struct tagPIConvertor
{
	int	  eConvertorType;   //use PI_CONVERTOR_TYPE constant
	int   iRelevantSigID;   //relevant PI info sampling sig id
	EQUIP_PI_CONVERT_PROC	pfnConverProc;

	//run-time info
	SAMPLE_SIG_VALUE	    *pSigRef;      
};
typedef struct tagPIConvertor PI_CONVERTOR;

enum PI_CONVERTOR_TYPE 
{
	PI_CONVERTOR_SERIAL,      //legacy serial number
	PI_CONVERTOR_HI_SERIAL,
	PI_CONVERTOR_LO_SERIAL,
	PI_CONVERTOR_PART_NUMBER,
	PI_CONVERTOR_HW_VER,
	PI_CONVERTOR_SW_VER,
	PI_CONVERTOR_NUM
};
#endif //PRODUCT_INFO_SUPPORT
//End  by YangGuoxin,1/12/2006

/* standard equip type definition */
struct tagStdEquipTypeInfo
{
	/* basic info */
	int				iTypeID;	 // NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!

	// is group type(eg. RectifierGroup type) flag
	BOOL			bGroupType;		
	LANG_TEXT		*pTypeName;	 

	// sampling sig ID for Serial Number, -1 for not configured
	int				iSerialRelatedSigID; 
	
	// sampling sig ID for communication status of the equipment(not sampler!)
	int				iCommStatusRelatedSigID;
	int				iWorkStatusRelatedSigID;

	/* sig info */
	int				iSampleSigNum;	 
	SAMPLE_SIG_INFO	*pSampleSigInfo;	 
	int 			iCtrlSigNum;	 
	CTRL_SIG_INFO	*pCtrlSigInfo;	 
	int				iSetSigNum;	 
	SET_SIG_INFO	*pSetSigInfo;	 
	int				iAlarmSigNum;	 
	ALARM_SIG_INFO	*pAlarmSigInfo; 

	// run time info
	EQUIP_SERIAL_CONVERT_PROC2	pfnSerialConvertProc;

#ifdef PRODUCT_INFO_SUPPORT        //added for ACU Barcode
	/* note: the order should be the same as PI_CONVERTOR_TYPE */
	PI_CONVERTOR  stConvertor[PI_CONVERTOR_NUM];
#endif //PRODUCT_INFO_SUPPORT


};
typedef struct tagStdEquipTypeInfo STDEQUIP_TYPE_INFO;



/*==========================================================================*
 *  
 *    Solution Config Structures
 *
 *==========================================================================*/
#ifdef PRODUCT_INFO_SUPPORT
struct tagDeviceTypeMap
{
	char    *szPartNumber;
	char	*szDeviceName;
};
typedef struct tagDeviceTypeMap DEVICE_TYPE_MAP;

struct tagProductInfo
{
	BOOL	bSigModelUsed;
	char 	szPartNumber[16];
	char	szHWVersion[16];
	char	szSWVersion[16];
	char	szSerialNumber[32];

	//configured in the stdBasic file, just for display
	char    szDeviceName[2][32];
	char    szDeviceAbbrName[2][16];
};
typedef struct tagProductInfo PRODUCT_INFO;
#endif //PRODUCT_INFO_SUPPORT


struct tagRelevantSamplerID
{
	int	iSamplerID;	 
	int	iStartSamplingChannel;	
	int	iStartControlChannel;	
};
typedef struct tagRelevantSamplerID	RELEVANT_SAMPLER;

/*The Solution Config version infomation,Should be display in LCD ,Web etc, caihao 2005.12.22*/
#ifdef CR_CFGFILE_INFO

struct tagConfigInformation
{
	char	*szCfgName;	//the name of solution ConfigFile.
	char	*szCfgVersion;	//the version of solution ConfigFile.
	char	*szCfgDate;	//the Date last changed.
	char	*szCfgEngineer;	//who last changed it.
	char	*szCfgDescription;	//the discription of the solution CfgFile.
};
typedef	struct	tagConfigInformation	CONFIG_INFORMATION;

#endif
/*The solution config version over*/

struct tagCustomChannel	 
{
	int	iEqipID;	 
	int	iSigType;	/* 0 = sampling sig, 1 = control sig, 2 = setting sig, 3 = alarm sig */
	int	iSigID; 
	int	iSamplerID;	 
	int	iSamplerChannel;	 
	int	iControlChannel;	  
};
typedef struct tagCustomChannel CUSTOM_CHANNEL;

/* equip info definition */
struct tagEquipInfo
{
	int					iEquipID;	// NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	int					iEquipTypeID;				/* defined by STDEQUIP_TYPE_INFO */
	LANG_TEXT			*pEquipName;	  
	LANG_TEXT			*pFactoryName;	
#define LEN_EQUIP_SERIAL_NUM		32	// including the end flag '\0'
	char				szSerialNumber[LEN_EQUIP_SERIAL_NUM];	//serial num. use static array. maofuhua, 2005-3-10
	
#define EQUIP_ATTR_SET(pEquip, aAttr) \
    ((pEquip)->iDefaultWorkState |= (aAttr))
#define EQUIP_ATTR_CLR(pEquip, aAttr) \
    ((pEquip)->iDefaultWorkState &= ~(aAttr))
#define EQUIP_ATTR_TEST(pEquip, aAttr) \
    (((pEquip)->iDefaultWorkState & (aAttr)) == (aAttr))
#define	EQUIP_WORKING_STATE		0x0001     //Added by Thomas, for Auto-config, 2007-6-21
	
	
	int				    iDefaultWorkState;			/* B00=1: not working, B01=1: working, B02=1: stand by */
	int					iAlarmFilterExpression;		/* element count of Alarm Filter Expression */
	EXP_ELEMENT			*pAlarmFilterExpressionCfg;	/* dynamic created pointer array, read from config file */
	EXP_ELEMENT			*pAlarmFilterExpression;	/* variable element now has been mapped to sig addr */
	int					iRelevantSamplerListLength;	 
	RELEVANT_SAMPLER	*pRelevantSamplerList;	
	int					iRelevantEquipListLength;	
	int					*pRelevantEquipList;		/* each element is equip ID */
	int					nCustomChannelInfo;			/* number of custom channel */
	CUSTOM_CHANNEL		*pCustomChannelInfo;	

	/* runtime info */
	STDEQUIP_TYPE_INFO	*pStdEquip;					/* reference of related STDEQUIP_TYPE_INFO */

#define EQP_STATE_PROCESS_IDLE			0			// valule of iProcessingState
#define EQP_STATE_PROCESS_PENDING		1
#define EQP_STATE_PROCESS_DOING			2
	int					iProcessingState;			/* use macro */
	SAMPLE_SIG_VALUE	*pSampleSigValue;	 
	CTRL_SIG_VALUE		*pCtrlSigValue;	 
	SET_SIG_VALUE		*pSetSigValue;	 
	ALARM_SIG_VALUE		*pAlarmSigValue;
	
	int					iAlarmCount[ALARM_LEVEL_MAX]; /* alarm count for each level */

    /* head of active alarm bi-drection link(each level has one, could not be deleted */
	ALARM_SIG_VALUE		pActiveAlarms[ALARM_LEVEL_MAX];

	int					nDelayingAlarms;
	ALARM_SIG_VALUE		pDelayingAlarms[1];	// only 1 dimension
													
	BOOL				bAlarmsSuppressed;	
	
	
	int					iEquipReferredThis;			/* length of reference list */

	/* reference list of equips who use the sig of cur equip ,usually there are a few*/
	EQUIP_INFO			**ppEquipReferredThis;	
	double				tImproveProcessTime;	    					

	// synclock for data processing task and the data processing data and
	// data exchanging interface. 
#define MAX_TIME_WAITING_EQUIP_LOCK		2000
	HANDLE				hSyncLock;
	HANDLE				hLcdSyncLock;

#ifdef PRODUCT_INFO_SUPPORT
	BOOL				bPISigUsed; //use sig model to get Product Infomation
#endif //PRODUCT_INFO_SUPPORT

	SAMPLE_SIG_VALUE	*pSerialSigRef;	// the serial num sig ref. 	 
	SAMPLE_SIG_VALUE	*pCommStatusSigRef;// the comm status signal of the equipment.
	SAMPLE_SIG_VALUE	*pWorkStatusSigRef;// the work status signal of the equipment.
	BOOL				bCommStatus;	// TRUE for OK.
	BOOL				bWorkStatus;	// TRUE for OK.
	//BOOL				bClearAlarmStatus; //USED to clear ACD/DCD/BAT alarm status signal.
	SAMPLE_SIG_VALUE	*pSerialSigRef2;	// the serial num sig ref.
	SAMPLE_SIG_VALUE	*pRectACStatus;
};
//typedef struct tagEquipInfo EQUIP_INFO;

typedef struct tagPortInfo PORT_INFO;

/* device info definition */
#ifdef PRODUCT_INFO_SUPPORT
struct tagDeviceInfo
{
	int			  iDeviceID;      //started from 1

	PRODUCT_INFO  *pProductInfo; 
	PI_CONVERTOR  *pConvertor;

	int           nRelatedEquip;
	EQUIP_INFO    **ppRelatedEquip;
};
typedef struct tagDeviceInfo DEVICE_INFO;
#endif //PRODUCT_INFO_SUPPORT


/* sampler info definition */
struct tagSamplerInfo
{
	int				iSamplerID;	 // NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	DWORD			*pMapTable;  //Used to ACD/DCD  //MUST BE THE SECOND ITEM
	LANG_TEXT		*pSamplerName;
	int				iSamplerAddr;	
	int				iSamplerPeriod; /* Unit: millisecond */
	int				iStdSamplerID;	/* defined by STDSAMPLER_INFO */
	int				iPollingPortID;	
	char			*szPortSetting;	

#define SAMPLER_ATTR_SET(pSampler, aAttr) \
				(((pSampler)->iSamplerAttr |= (aAttr))
#define SAMPLER_ATTR_TEST(pSampler, aAttr) \
				(((pSampler)->iSamplerAttr & (aAttr)) == (aAttr))

#define	SAMPLER_EXCLUSIVE_PORT		0x0001	
	int				iSamplerAttr;	/* B00=1:Exclusive polling port */
	
	/* runtime info */
	STDSAMPLER_INFO	*pStdSampler;	/* reference of standard sampler */
	PORT_INFO		*pPollingPort;	// the port of the sampler. use to send cmd.

	BOOL			bDataChanged;	/* the data in buffer is changed, need to process */
	BOOL			bCallingRead;	// using Read() proc of sampler to read data.
	float			*pChannelData;	/* get through pfnRead interface, pChannelData[0] is comm status data */
	double			tmSampleStarted;// time to start sample data.
	double			tmDataSampled;	// the time the last data sampled
	double			tmSampleUsed;	// the time used in one time of sampling data
	double			fElapsedTime;	// the elapsed time from the last sampling action.
									// NOTE: the time changing is NOT considered.
	BOOL			bJustControlled;// The sampler just sent a control, Need sample data now.
				
	int				nMaxUsedChannel;// the maximum channel used in std equipment cfg
	int				nMaxQueriedChannel;//the maximum channel queried by calling Query().

	int				nSigValues;		// total signal values in the ppSigValues. maofuhua, 2005-1-6
	SIG_BASIC_VALUE	**ppSigValues;	/* reference list of related Sig Values, ordered by channel no */

	BOOL			bCommStatus;
	int				nCommErrors;	// the comm read failure count
	
	int				nRelatedEquipment;	  /* the number of related equip */
	EQUIP_INFO		**ppRelatedEquipment; /* reference list of related equips */

#ifdef PRODUCT_INFO_SUPPORT
	int			   nQueryPIErrors;		//the comm failure count for query product info
	BOOL		   bQueryPI;	//when the flag is 0, PSC should query Product Info
	PRODUCT_INFO   stProductInfo;
#endif //PRODUCT_INFO_SUPPORT


};
typedef struct tagSamplerInfo SAMPLER_INFO;

/* port info definition */
struct tagPortInfo
{
	int	  iPortID;		// NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	char  *szPortName;	 
	int	  iStdPortTypeID;	    /* the type of the port, defined by STDPORT_INFO */
	char  *szDeviceDesp;	    /* used by the system to access the port,
							       eg:COM1(/dev/ttyS1), COM2(/dev/ttySA1), ¡­NET1(eth0) */
	
	int	  iDisconnDelay;	     /* -1:not disconnect, others: the delay time, counted by seconds*/
	int   iTimeout;				 /* Unit: millisecond */
	char  *szAccessingDriverSet;	 /* Parameter setting for the driver */
	
	/* runtime info */
	HANDLE			hPort;	
	STDPORT_INFO	*pStdPort;			  /* relevant standard port reference */
	int				nAttachedSamplers;	  /* the number of attached samplers */
	SAMPLER_INFO	**ppAttachedSamplers; /* reference list of attached samplers */

	// max pending command in a port, see MAX_PENDING_CTRL_CMD
	HANDLE			hqControlCommand;	  /* command queue of the port */

	//the sampling thread handle for this port
	HANDLE			hSampleThread;

#define MAX_TIME_WAITING_PORT_LOCK		10000
	HANDLE			hSyncLock;			/* the synchronization lock */

	SAMPLER_INFO	*pCurSampler;		// the current sampler using the port

	///////////////////////////////////////////////////////////////////////////
	/*For scu plus virtual port*/
	void			*pDataBuffer;
};


/* for each std equip type, gives its config file name */
struct tagStdEquipTypeMagInfo
{
	int		iEquipTypeID;	 // NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!
	BOOL	bUsed;				/* used in current solution flag */
	char	*szCfgFileName;	    /* std equip config file name */
};
typedef struct tagStdEquipTypeMagInfo STDEQUIP_TYPEMAP_INFO;


//  cmd struct of process equipment data. 
//  used by eqp mgmt thread and data sampling thread
struct _EQUIP_PROCESS_CMD
{
#define PROCESS_CMD_SIG_DATA_CHANGED		0	// the pointer of EQUIP_INFO.
#define PROCESS_CMD_ALARM_LEVEL_CHANGED		1   // the pEquip is the nStdEquipTypeId!
#define PROCESS_CMD_ALARM_RELAY_CHANGED		2
	int			nCmd;		// the cmd type. 
	EQUIP_INFO	*pEquip;	// the data of equipment need to be processed.
};
typedef struct _EQUIP_PROCESS_CMD	EQUIP_PROCESS_CMD;
/*For scu plus*/

struct tagSystemSigValInfo
{
	///////////////////////////////////////////////////////////////////////////
	SIG_ENUM diDISTR_FUSE1;	//8 digital input signal
	SIG_ENUM diDISTR_FUSE2;
	SIG_ENUM diAUX_INPUT1;
	SIG_ENUM diAUX_INPUT2;
	SIG_ENUM diAUX_INPUT3;
	SIG_ENUM diAUX_INPUT4;
	SIG_ENUM diCONTACT_IN1;
	SIG_ENUM diCONTACT_IN2;

	SIG_ENUM doCONTACT_OUT1; //10 digital outout signal
	SIG_ENUM doCONTACT_OUT2;
	SIG_ENUM doRELAY_OUT1;
	SIG_ENUM doRELAY_OUT2;
	SIG_ENUM doRELAY_OUT3;
	SIG_ENUM doRELAY_OUT4;
	SIG_ENUM doRELAY_OUT5;
	SIG_ENUM doRELAY_OUT6;
	SIG_ENUM doRELAY_OUT7;
	SIG_ENUM doRELAY_OUT8;

	float		fBatt_CURR1; //5 anolog input signal
	float		fBatt_CURR2;
	float		fTEMP;
	float		fDC_VOLT;
	float		fBATT_FUSE_Vol;
	float		fBATT_FUSE_Vol2; //Use for debug
	
};
typedef struct tagSystemSigValInfo SYS_SIG_VAL_INFO; 

struct _tagORIGINAL_SIG_VAL
{
	float fData[MAX_SCUP_SIGS];       //First 58 as system signals
	                                  //And 23 signals/per rectifier
};
typedef struct _tagORIGINAL_SIG_VAL ORIGIN_SIG_VAL;

struct tagSYS_SIG_CAL_COEFFI
{
	float fKvdc;	//DC voltage coefficent
	float fKbfuse;	//Battery Fuse1 Coefficient:
	float fKtemp;	//Temprature calc Coefficient
	float fKbf;		//Battery Fuse2 Coefficient
	float fKIb1;	//Battery current1 Coefficient
	float fKIb2;	//Battery current2 Coefficient
	float fKshunt1; //Shunt1 coeffient:
	float fKshunt2; //Shunt2 coeffient:
	float fVref;	//Reference voltage Coefficient
	float fIref;	//Reference current Coefficient

	float fContactType; //Contact type    
	float fPulseWidth;  //CMD_PULSE_WIDTH
};    //They will be get from flash and set to flash
typedef struct tagSYS_SIG_CAL_COEFFI SYS_SIG_CAL_COEFFI;

//////////////////////////////////////////////////////////////////////////
/*User define web page info definition*/
struct tagePageName
{
	int iPageID;
	int iMaxLenForFull;
	int iMaxLenForAbbr;
	char *pFullName[MAX_LANG_TYPE];
	char *pAbbrName[MAX_LANG_TYPE];
	char pFileNameEng[32];//Used to display the picture which defined by user
	char pFileNameLoc[32];//Used to display the picture which defined by user
	//Frank Wu,20160127, for MiniNCU
	int iResourceID;//for supporting multi languages
};
typedef struct tagePageName PAGE_NAME;

struct tagPageInfo
{
	int iPageInfoNum;
	PAGE_NAME *pPageName;
};
typedef struct tagPageInfo PAGE_INFO;

struct tagSigItem
{
	int iEquipID;
	int iSigType;
	int iSigID;
	int iPageID;
};
typedef struct tagSigItem SIG_ITEM;

struct tagSigItemInfo
{
	int iSigItemNum;
	SIG_ITEM *pSigItem;
};
typedef struct tagSigItemInfo SIG_ITEM_INFO;

struct tagUserDefPages
{
	PAGE_INFO *pPageInfo;
	SIG_ITEM_INFO *pSigItemInfo;
};
typedef struct tagUserDefPages USER_DEF_PAGES;
//////////////////////////////////////////////////////////////////////////
/* 20130531 LKF: Some signals sampled from Mainboard sampler need stuffer to the I2C sampler 
	Ϊ�˼���ACU+�������ļ�����Ҫ�Ѵ�����ɼ���OB���ź���䵽I2C�ɼ�����ͨ����		*/
enum MB_ROUGH_DATA_SHARE_FOR_I2C
{
    MB_DC_VOLT = 0,
    MB_LOAD_CURR,
    MB_BATT1_VOLT,
    MB_BATT2_VOLT,
    MB_BATT1_CURR,
    MB_BATT2_CURR,
    MB_TEMPERATURE1,
    MB_TEMPERATURE2,
    //MB_SPD,
    MB_LOAD_CURR_COM_VOLT,
    MB_BATT1_CURR_COM_VOLT,
    MB_BATT2_CURR_COM_VOLT,
    MB_BATT1_ST,
    MB_BATT2_ST,

    MB_PASSWORD_JUMPER,
    MB_OS_READONLY_JUMPER,
    MB_APP_READONLY_JUMPER,
    MB_5V_DC_POWER_JUMPER,

    MB_LOAD_FUSE1_ST ,			
    MB_LOAD_FUSE2_ST ,			
    MB_BATT_FUSE1_ST,			//OB BATT Fuse 1
    MB_BATT_FUSE2_ST ,	
#ifdef _CODE_FOR_MINI
    MB_BATT_FUSE3_ST ,			//add two batt fuse status for mini controller 	
    MB_BATT_FUSE4_ST ,	
#endif
    //IB01
    IB01_EXIST_ST,			//IB01 Exist Status 0-not exist     1-exist
    IB01_DI1_ST,
    IB01_DI2_ST,
    IB01_DI3_ST,
    IB01_DI4_ST,
    IB01_SPD_ST,

    //OB
    OB_TEMP1,
    OB_FUEL_SENSOR1,
    OB_FUEL_SENSOR2,
    OB_LOAD_FUSE1_ST,		//OB Load Fuse 1
    OB_LOAD_FUSE2_ST ,			
    OB_LOAD_FUSE3_ST ,			
    OB_LOAD_FUSE4_ST ,			
    OB_LOAD_FUSE5_ST ,			
    OB_LOAD_FUSE6_ST ,			
    OB_LOAD_FUSE7_ST ,			
    OB_LOAD_FUSE8_ST ,			
    OB_LOAD_FUSE9_ST ,			
    OB_LOAD_FUSE10_ST ,			
    OB_BATT_FUSE1_ST,			//OB BATT Fuse 1
    OB_BATT_FUSE2_ST ,	
    OB_BATT_FUSE3_ST,			
    OB_BATT_FUSE4_ST ,	
    OB_LVD_AUX1_ST,			//OB LVD AUX
    OB_LVD_AUX2_ST ,	

    OB_LVD1_STATE,
    OB_LVD2_STATE,

    OB_EXIST_ST,					//OB Exist Status 0- exist     1-not exist

    ABS_BATT_CURR1,
    ABS_BATT_CURR2,

    MB_ROUGH_SHARE_NUM,
};
union unSAMPLING_VALUE_MB_ROUGH
{
    float	fValue;
    int		iValue;
    UINT	uiValue;
    ULONG	ulValue;
    DWORD	dwValue;
};
typedef union unSAMPLING_VALUE_MB_ROUGH SAMPLING_VALUE_MB_ROUGH;

/*20130605 lkf: History Data Record For ui (Web / LCD)*/
struct tagTM1 {
    int tm_hour;    /* hours since midnight - [0,23] */
    int tm_mday;    /* day of the month - [1,31] */
    int tm_mon;     /* months since January - [0,11] */
    int tm_year;    /* years since 1900 */
};
typedef struct tagTM1 TM1;
struct tagHisDataRecordLoad
{
    TM1			tm1Date;
    float		floadCurrent;
    time_t		tmSignalTime;
    BOOL		bflag;
};
typedef struct tagHisDataRecordLoad HIS_DATA_RECORD_LOAD;

// changed by Frank Wu,20131213,1/8, for keep the same used rated current percent between WEB and LCD
struct tagWebLcdShareData {
	float fRatedCurrPercent;
	//changed by Frank Wu,20140213,1/9, for displaying history temperature curve more faster
	BOOL	bTrendTIsNeedUpdate;
};
typedef struct tagWebLcdShareData WEB_LCD_SHARE_DATA;


// �����ļ�ϵͳ����
#define OLD_FILE_SYS_TYPE 0
#define NEW_FILE_SYS_TYPE 1
// ����SSL����
#define NO_SSL		0
#define HAVE_SSL	1
//����snmpv3����
#define NO_SNMPV3	0
#define HAVE_SNMPV3	1
/* site info definition */
struct tagSiteInfo
{
	int 					iSiteID;	 // NOTE: MUST BE THE FIRST  ITEM! AND MUST BE int TYPE!!!

#define CONFIG_DIR					"/config/"
#define CONFIG_BAK_DIR				"/config_bak/"
#define CONFIG_DEFAULT_DIR			"/config_default/"

#define CONFIG_NORMAL_TYPE	0	// The being used config is normal
#define CONFIG_BACKUP_TYPE	1	// the being used config is backup
#define CONFIG_DEFAULT_TYPE	2	// the being used config is default.
	int						nConfigType;

#define CONFIG_STATUS_NOT_CHANGED		0	// the config is not changed.
#define CONFIG_STATUS_NORMALLY_CHANGED	1	// the config just changed, historical data need be removed.
#define CONFIG_STATUS_DEFAULT_LOADED	2	// the config just changed to default, all record files shall be removed.
	int						nConfigStatus;

#ifdef CR_CFGFILE_INFO
	CONFIG_INFORMATION		*pConfigInformation;	//add the version display in lcd ,caihao 2005.12.21
#endif
	/* language info */
	LANG_INFO				LangInfo;

	/* basic info */
	LANG_TEXT 				langSiteName;
	int						iSiteAttr;				/* reserved */
	LANG_TEXT				langSiteLocation; 
	LANG_TEXT				langDescription;	 
	
	/* std config info */
	int						iStdPortNum;	 
	STDPORT_INFO			*pStdPortInfo;	 
	int						iStdSamplerNum;	 
	STDSAMPLER_INFO			*pStdSamplerInfo;
	int						iStdDCPNum;	 
	STDDCP_INFO				*pStdDCPInfo;	 	
	int						iEquipTypeMapNum;	   //All equip type num from basic std file
	STDEQUIP_TYPEMAP_INFO	*pEquipTypeMap;	
	int						iStdEquipTypeNum;	   //Used by Current solution 
	STDEQUIP_TYPE_INFO 		*pStdEquipTypeInfo;	 

	/* solution config info */
	int						iPortNum;	 
	PORT_INFO 				*pPortInfo;	 
	int						iSamplerNum;		 
	SAMPLER_INFO 			*pSamplerInfo;		 
	int						iEquipNum;	 
	EQUIP_INFO				*pEquipInfo;		 
	int						iCustomChannelNum;
	CUSTOM_CHANNEL	        *pCustomChannel;

#ifdef PRODUCT_INFO_SUPPORT
	int						iDeviceNum;
	DEVICE_INFO             *pDeviceInfo;

	//configured in the stdBasic file, just for display
	int						iDeviceTypes;
	DEVICE_TYPE_MAP			*pDeviceTypeMap;
#endif //PRODUCT_INFO_SUPPORT


	// time to save or report stat data. 
	// it is the seconds count from middle night. 
	// if save time is hh:mm:ss, tmStatDataReportTime = hh*3600+mm*60+ss
	time_t	tmStatDataReportSchedule;

				
	/* runtime info */
	HANDLE	hqProcessCommand;	/* element of the queue is EQUIP_PROCESS_CMD which want to process data */

	BOOL	bAlarmOutgoingBlocked;	/* TRUE for alarm outgoing blocked. */
	BOOL	bSettingChangeDisabled;	/* TRUE: all settings are NOT allowed to change. 
									controlled by hardware switch */
	int		nSystemManagementMode;	/* 1=SYS_STATE_BY_MAN: The system is manually managed, 0: in Auto. */  

	int		iAlarmCount[ALARM_LEVEL_MAX];	/* active alarm count for each level */
	BOOL	bProcessDelayingAlarms;	// TRUE need to process the delaying alarms.
	//int		iAlarmSigsReleRelay[MAX_RELAY_NUM];//used track active alarm num 


	//the thread handle of equipment manager
	HANDLE	hEquipManager;

	// the maximum allowed pending cmds, see MAX_ALLOWED_CTRL_CMDS
	HANDLE	hCmdArray;					// the array of ctrl commands of equipment.

	BOOL	bStatDataSaved;			// TRUE for stat data is saved OK

	// handle of historical data.
	HANDLE	hHistoricalData;			// handle of historical sampling signal data
	HANDLE	hHistoricalAlarm;			// handle of alarm
	HANDLE	hStatData;					// the statistics data
	int		nMaxAllowedActiveAlarms;	// max allowed can be save active alarms.
	int		nCurrentSavedAlarms;		// current saved active alarm records.
	HANDLE	hActiveAlarm;				// handle of recording active alarm state.
	int		nPersistentSig;				// the num of signals need to be persistently saved.
	HANDLE	hPersistentSig;				// handle of save virtual setting signals which sampler chann is -3.
	int		nMaxAllowedPersistentSig;	// max set sig can be saved.
	int		nCurrentSavedPersistentSig;	// current saved setting sig records.
	HANDLE	hControlLog;				// user control log.
	/*For scu plus*/
	SYS_SIG_CAL_COEFFI *pCalCoe;		// Used calculate system signal value
	BOOL	bFirstRunning;
	HANDLE	hLcdMessageQueue;			// Used by float menu

	//User define page data
	USER_DEF_PAGES *pUserDefPages;		//Used for User Define Pages
	//USER_DEF_LCD	*pUserDefLCD;		//G3 for LCD

	BOOL bAutoConfigStatus;//TRUE : into Auto Config status

	PRODUCT_INFO stI2CProductInfo[8];
	//Frank Wu,20150318, for SoNick Battery
	//PRODUCT_INFO st485ProductInfo[66];
#define PRODUCT_INFO_485PRODUCTINFO_MAX		(66 + 32 + 16)//+ SoNick Battery(32) + Narada BMS(16)
	PRODUCT_INFO st485ProductInfo[PRODUCT_INFO_485PRODUCTINFO_MAX];
	PRODUCT_INFO stCANSMDUPProductInfo[20];
	PRODUCT_INFO stCANConverterProductInfo[60];//stCANConverterProductInfo[48];
	PRODUCT_INFO stCANSMTempProductInfo[8]; //added by Jimmy 2011/10/20 
	PRODUCT_INFO stCANLiBattProductInfo[60]; //added by Jimmy 2012/3/31 
	PRODUCT_INFO stCANLiBridgeCardProductInfo; //added by Jimmy 2012/6/25 北美要求“只”在web上显示与锂电池配合的BridgeCard的信�?

#ifdef GC_SUPPORT_MPPT
	PRODUCT_INFO stCANMpptProductInfo[30];
#endif
	PRODUCT_INFO stCANRectProductInfo[160];//160
	PRODUCT_INFO stCANSMDUProductInfo[8];
	PRODUCT_INFO stCANSMDUHProductInfo[8];
	PRODUCT_INFO stCANSMDUEProductInfo[8];
	PRODUCT_INFO stCANDCEMProductInfo[8];
	//int		ifileSysType;// �ļ�ϵͳ����
	int		iSSLFlag;
	int		iSNMPV3Flag;

	BATT_NUM_INFO	stBattNum;

	//only loading typical equipments by defaul, for G3_OPT [loader], by Thomas, 2013-5
	BOOL	bLoadAllEquip;  	   

	/* 20130531 LKF: Some signals sampled from Mainboard sampler need stuffer to the I2C sampler   
			Ϊ�˼���ACU+�������ļ�����Ҫ�Ѵ�����ɼ���OB���ź���䵽I2C�ɼ�����ͨ����     */
	SAMPLING_VALUE_MB_ROUGH		MbRoughDataShareForI2C[MB_ROUGH_SHARE_NUM];
	int			iOBHandle;  
	int			iIB01Handle;
	int			iMbHandle;

	/*20130605 lkf: History Data Record For ui (Web / LCD)*/
	HIS_DATA_RECORD_LOAD	*pLoadCurrentData;//��Ÿ��ص���ֵ;
	// changed by Frank Wu, 20131113, 8/9,for only displaying one data in each interval of 2 hours
	time_t					tmLatestUpdate;//store the latest updating time of ambient temperature
	// changed by Frank Wu,20131213,2/8, for keep the same used rated current percent between WEB and LCD
	WEB_LCD_SHARE_DATA *pstWebLcdShareData;
};
typedef struct tagSiteInfo SITE_INFO;

struct tagSigIdentity             
{
	int	iEquipType;	 
	int	iSigType;	/* 0 = sampling sig, 1 = control sig, 2 = setting sig, 3 = alarm sig */
	int	iSigID;	    /* defined by XXX_SIG_INFO */
};
typedef struct tagSigIdentity SIG_IDENTITY;

struct tagConfigChangedInfo
{
	DWORD			dwConfigMask;		/* use macro */
	int				iEquipNum;			/* name changed equip count */
	int				*pEquipIDs;			/* name changed equip id list */
	int				iSigNum;			/* name changed sig count */
	SIG_IDENTITY	*pSigIDs;	        /* name changed sig ID list */
	int				iAlarmNum;			/* level changed alarm sig count */
	SIG_IDENTITY    *pAlarmIDs;			/* level changed alarm sig ID list */
};
typedef struct tagConfigChangedInfo CONFIG_CHANGED_INFO;


/* global variable declare */
extern SITE_INFO	g_SiteInfo;


//#pragma pack()	// restore old aligment. maofuhua,2004-11-14

#endif //__CFG_MODEL_H_2004_09_09__

/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : data_mgmt.h
*  CREATOR  : LIXIDONG                 DATE: 2004-10-21 16:29
*  VERSION  : V1.00
*  PURPOSE  : It include two sides.One is the storage interface functions to 
*			 : high level modules,the other is the initiate histhorical data
*			 : management.The later serve to the storage interface function.
*           : The interface function can't be used until historical data
*			 : management is initiated..
*
*
*  HISTORY  :
*
*==========================================================================*/


#ifndef __DATA_MGMT_H
#define __DATA_MGMT_H

#define MTD5_NAME				"/dev/mtd6"     // memory device name  the mtd7 space is 14*64k

#ifdef _HAS_FLASH_MEM
#define MTD_NAME				"/dev/mtd5"     // memory device name 
#else

//#move the x86 log file to ./log/  maofuhua, 2005-1-12
static char s_szFull_MTD_NAME[MAX_FILE_PATH];
#ifndef ENV_ACU_ROOT_DIR
#define ENV_ACU_ROOT_DIR		"ACU_ROOT_DIR"	// to use shell env variable.
#endif
//#define MTD_NAME				"mtd6.log"
#define MTD_NAME		MakeFullFileName(s_szFull_MTD_NAME, \
	getenv(ENV_ACU_ROOT_DIR), "log", "mtd6.log") 

#endif

//Erase all hisdata and log
#define ERASE_HISDATA		"eraseall /dev/mtd5"
//#define ERASE_RUN_LOG		"eraseall /dev/mtd5"
#define ERASE_RUN_LOG		"eraseall /dev/mtd6"
#define ERASE_PRO_FLS		"eraseall /dev/mtd3"

// Define Max historical Data Type Number
#define MAX_DATA_TYPES			17			

// The log file name in the storage, - maofuhua, 2004-11-05
// The maximum length of name is 16-1=15.
#define HIS_ALARM_LOG			"his_alarm.log"			// historical alarm log //! obsoleted name, to use HIST_ALARM_LOG instead.
#define HIST_ALARM_LOG			"hist_alarm.log"		// historical alarm log
#define HIS_DATA_LOG			"his_data.log"			// historical sampling data log
#define STAT_DATA_LOG			"stat_data.log"			// the log of statistics signal
#define CTRL_CMD_LOG			"ctrl_cmd.log"			// the ctrl cmd and result log
#define ACTIVE_ALARM_LOG		"active_alrm.log"		// the active alarm info.
#define PERSISTENT_SIG_LOG		"persist_sig.log"		// Runtime persistent param log

#define SYSTEM_USER_LOG			"user_and_nms.log"		// system user info log
#define CONSUMPTION_LOG			"consumption.log"
#define BRANCH_LOG			"branch.log"

#define	BATT_TEST_LOG_FILE		"batt_test.log"			// battery test log file
#define	DSL_TEST_LOG			"dsl_test.log"			// diesel test log
#define	GEN_CTL_DATA			"gc_pri_data.log"		// General Controller Data
#define	CAN_SAMPLER_FLASH_DATA	"can_data.log"			// Can sampler data
#define I2C_SAMPLER_FLASH_DATA	"i2c_data.log"			// I2C sampler data
#define	ALL_HIS_DATAS			"*.*"					// Used in DAT_StorageDeleteRecord
#define ACU_RUNNING_LOG         "system_running.log"	// Acu running log

//#define RECT_ADDR_SERAIL_LOG	"rect_addr_sig.log"		// Rectifier serial to addr realtion

// the last status file always is at the config/run/
#define SCUP_RUNNING_SOLUTION	"run/AppLastSolution.run"// record the running solution info.

// the specification of the count of the logs
// ---- the following spec can be read from cfg file in the optimized version!
#define MAX_HIS_ALARM_COUNT		4000//3000		// see Design Specification.doc
#define MAX_HIS_DATA_COUNT		60000	// see Design Specification.doc
#define MAX_STAT_DATA_COUNT		1000	//	
#define MAX_ACTIVE_ALARM_COUNT	500		// the system have 500/2=250 alarms at the same time, it's impossible!
#define MAX_CTRL_CMD_COUNT		500		// the last 500 user ctrl logs
#define MAX_PERSISTENT_SIG		0			// auto calculate. see SITE_INFO->nPersistentSig
#define	MAX_LOG_RECORD_COUNT		5000			//change from 300 to 5000 2008-7-15 by YangGuoxin
#define	MAX_RECT_ADDR_INFO_COUNT	200
#define MAX_PERSIST_SIG_COUNT	1000


// the frequency of writing the historical data in records/day
#define FREQ_HIS_ALARM			(1.0*(60*60*24))		// in pcs/day. e.q. 1 pcs/sec.
#define FREQ_HIS_DATA			(1.0*(60*60*24))		// in pcs/day. e.q. 1 pcs/sec.
#define FREQ_STAT_DATA			(1.0*MAX_STAT_DATA_COUNT)// in pcs/day, 1 sig/day
#define FREQ_ACTIVE_ALARM		(2*FREQ_HIS_ALARM)
#define FREQ_CTRL_CMD			(1.0*60*24)				// in pcs/day, e.q. 1pcs/min
#define FREQ_PERSISTENT_SIG		(1.0*60*24)				// in pcs/day, e.q. 1pcs/min

#define FREQ_USER_INFO			(1.0)				// in pcs/day, e.q. 1pcs/day
#define FREQ_SYS_RUN_LOG		(1.0*(60*60*24))

#define FREQ_RECT_ADDR_INFO		(1.0)				// in pcs/day, e.q. 1pcs/day

//------------------------------------------------------------------
//Solve Manfacture flash (AMD Flash change to ST Flash)problem
//Added by YangGuoxin, 1/5/2006
#define SYS_LOG_STORE_STRATEGY_CHANGED			1
//------------------------------------------------------------------

/* Different History Data Struct Definition*/
//Historical Alarm Record Storage Format
//#pragma pack(1)
struct tagStoreHisAlarmRecord ///////// obsoleted data struct, to use HIS_ALARM_RECORD instead.
{
	DWORD				iCurrRecordID;					// Record ID in FLASH
	int					iEquipID;						// Equipment ID
	int					iAlarmID;						// Alarm ID 
	VAR_VALUE			varTrigValue;					// Alarm trigger value 
	time_t				tmStartTime;					// Alarm Start time 
	unsigned short		iCheckNo1;                      // CRC16 CheckNo not include dwEndtime
	time_t				tmEndTime;						// Alarm End time
	unsigned short		iCheckNo2;                      // CRC16 CheckNo include dwEndtime

};
typedef struct tagStoreHisAlarmRecord STORAGE_HISALARM_RECORD;

//Active Alarm Record
struct tagActiveAlarmRecord	///////// obsoleted data struct, to use ACT_ALARM_RECORD instead.
{
	int					iEquipID;						// Equipment ID
	int					iAlarmID;						// Alarm ID 
	VAR_VALUE			varTrigValue;					// Alarm trigger value 
	time_t				tmStartTime;					// Alarm Start time     	 
};
typedef struct tagActiveAlarmRecord ACTIVE_ALARM_RECORD;

//Alarm ending Information
struct tagEndAlarmInfo	///////// obsoleted data struct, do NOT use.
{
	int					iAlarmID;
	time_t				tmEndTime;						// Alarm End time    	 
};
typedef struct tagEndAlarmInfo END_ALARM_INFO; 

/////////// new method of saving alarm record /// maofuhua, 2005-1-9
//historical alarm record
//#define LEN_EQUIP_SERIAL_NUM		32	// including the end flag '\0'
struct tagHisAlarmRecord
{
	int					iEquipID;						// Equipment ID
	// serial num of equipment. for rect module, it is the formated rect ID
	// for others, it maybe '\0' if there is NO serial.
	char				szSerialNumber[LEN_EQUIP_SERIAL_NUM];
	int				iPosition;
	int					iAlarmID;						// Alarm ID 
	BYTE				byAlarmLevel;					// Alarm Level
	VAR_VALUE			varTrigValue;					// Alarm trigger value 
	time_t				tmStartTime;					// Alarm Start time 
	time_t				tmEndTime;						// Alarm End time
};
typedef struct tagHisAlarmRecord HIS_ALARM_RECORD;

// Save the active alarms to storage when it is active,
// and load the active alarms when system is restarted.
// do NOT save and load active alarms. maofuhua, 2005-2-14
#define SAVE_ACTIVE_ALARMS		1

//Active Alarm Record
struct tagActAlarmRecord
{
	int					iEquipID;						// Equipment ID
	int					iAlarmID;						// Alarm ID 

	//#define ALARM_BEGIN_NORMAL				1	// the alarm begin in normal
	//#define ALARM_BEGIN_SUPPRESSION_STOPPED 2	// the alarm re-begin for suppression stopped
	//#define ALARM_BEGIN_LEVEL_CHANGED		3	// the alarm level 
	//#define ALARM_END_NORMAL				4	// the alarm end in normal
	//#define ALARM_END_SUPPRESSION_STARTED	5	// the alarm end for the suppression starting
	//#define ALARM_END_WHILE_SUPPRESSED		6	// the alarm end in normal with suppression
	//#define ALARM_END_MANUALLY				7	// the alarm end manually, reserved.	
	//#define ALARM_END_LEVEL_CHANGED			8	// the alarm level changed
	unsigned short		usStateMask;					// the state of the alarm.
	BYTE				byAlarmFlag;					// 0: END, 1: Alarming
	time_t				tmStartTime;					// Alarm Start time     	 
	char				szSerial[32];
	int				iPosition;
};
typedef struct tagActAlarmRecord ACT_ALARM_RECORD;
///////////

//Ended Alarm Record
struct tagEndAlarmRecord
{
	int					iEquipID;						// Equipment ID
	int					iAlarmID;						// Alarm ID 
	BYTE				byAlarmLevel;					// Alarm Level
	VAR_VALUE			varTrigValue;					// Alarm trigger value 
	time_t				tmStartTime;					// Alarm Start time 
	time_t				tmEndTime;						// Alarm End time

};
typedef struct tagEndAlarmRecord END_ALARM_RECORD;


#ifdef _DEBUG
#define LOG_INTERNAL_CTRL_CMD			1		// log the cmd sent by Gen_ctrl service.
#endif

//0: for ONLY log the cmd is successfully executed.
#define LOG_CTRL_CMD_REGARDLESS_RESULT	0	

//Historical Control Command Record
struct tagHisContolRecord
{
	int				iEquipID;						// Equipment ID 
	int				iSignalID;						// Signal ID merged by DXI_MERGE_SIG_ID()
	time_t			tmControlTime;					// Control time 
#define MAX_CTRL_CMD_LEN			64
	char			szCtrlCmd[MAX_CTRL_CMD_LEN];	// Control Value
#define MAX_CTRL_SENDER_NAME	64
	char			szSenderName[MAX_CTRL_SENDER_NAME];	// Control Sender Name 
	BYTE			bySenderType;					// Sender Type:see app_service.h:APP_SERVICE_TYPE_ENUM 
	int				nControlResult;				// Control Result:see err_code.h:ERR_CODE_EQUIPMENT_MONITORING_ENUM
	int			iPositionID;

} ;
typedef struct tagHisContolRecord HIS_CONTROL_RECORD;		 


//Historical Statistic Record
struct tagHisStatRecord
{
	int					iEquipID;					// Equipment ID 
	int					iSignalID;					// Signal ID merged by DXI_MERGE_SIG_ID()
	time_t				tmStatTime;					// Statistic time 
	VAR_VALUE			varCurrentVal;				// Current Value
	time_t				tmMinValue;					// Time of Minimum Value
	VAR_VALUE			varMinValue;				// Minimum Value
	time_t				tmMaxValue;					// Time of Maximum Value
	VAR_VALUE			varMaxValue;				// Maximum Value
	VAR_VALUE			varAverageValue;				 

};
typedef struct tagHisStatRecord HIS_STAT_RECORD;


//Historical Signal Data Record
struct tagHisDataRecord
{
	int				iEquipID;						// Equipment ID 
	int				iSignalID;						// Signal ID merged by DXI_MERGE_SIG_ID()
	VAR_VALUE		varSignalVal;					// Signal Value 
	time_t			tmSignalTime;					// Signal time 
	int				iActAlarmID;					// Actived Alarm ID   
};
typedef struct tagHisDataRecord HIS_DATA_RECORD;

//defined in cfg_model.h   20130607 lkf
//struct tagTM1 {
//    int tm_mday;    /* day of the month - [1,31] */
//    int tm_mon;     /* months since January - [0,11] */
//    int tm_year;    /* years since 1900 */
//};
//typedef struct tagTM1 TM1;
//struct tagHisDataRecordLoad
//{
//    TM1			tm1Date;
//    float		floadCurrent;
//    time_t		tmSignalTime;
//    BOOL		bflag;
//};
//typedef struct tagHisDataRecordLoad HIS_DATA_RECORD_LOAD; 



//runtime persistent signal data record. maofuhua, 05-1-9
#define  PARAM_FILE_HEAD_INFO						"ACU_PARAM_RUN\0"
//Added by Thomas for TR# 62-ACU. if the format of PERSISTENT_SIG_RECORD has changed
//need to update the version number!
//#define  VER_PARAM_RECORD                         1   
#define  VER_PARAM_RECORD                           2   //add user/NMS info supporting, for G3_OPT [config], by Lin.Tao
#define  VER_PARAM_USERINFO_SUPPORT					2   //user/NMS info supporting from this version
#define  VER_PARAM_RECORD_INITIAL					1   //first version in market

struct tagPersistentSigRecord
{
	int				iEquipID;						// Equipment ID 
	int				iSignalID;						// Signal ID merged by DXI_MERGE_SIG_ID()
	VAR_VALUE		varSignalVal;					// Signal Value 				
	time_t			tmSignalTime;					// Signal time 
};
typedef struct tagPersistentSigRecord	PERSISTENT_SIG_RECORD;

struct tagRectAddrInfSigRecord
{
	DWORD			dwPosition;						// Rectifier Setting Address 
	DWORD			dwRectSerialNo;				// Rectifier Serail No
	DWORD			dwRectHiSerialNo;			// Rectifier High Serail No
};
typedef struct tagRectAddrInfSigRecord	RECT_ADDR_SIG_RECORD;

//System running log struct define length
struct tagRunLogSize 
{
	char szLogBuff[148];    	 
};
typedef struct tagRunLogSize LOG_RECORD_SIZE;

//#pragma pack()

//Callback function
typedef int (*COMPARE_PROC) (const void *pRecord, const void *pCondition);

//Add erasing sector pre-process function
/*==========================================================================*
* FUNCTION : int
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: *ERASE_SECTOR_PRE_PROC)(HANDLE  hWrite       : 
*            void *                          pRecordsBuff : 
*            int                             iRecords     : 
* RETURN   : typedef :						  1:continue; 0:end
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2004-12-30 11:07
*==========================================================================*/
typedef int (*ERASE_SECTOR_PRE_PROC)(HANDLE hWrite, 
				     void * pRecordsBuff, 
				     int iRecords, 
				     void *pParam);

/*==========================================================================*
* FUNCTION : DAT_ClearResource
* PURPOSE  : If system will exit,system will call this function to clear
*			  resource of data management module
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   :   static void : 
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2004-10-15 20:05
*==========================================================================*/
void DAT_ClearResource(void);

/////////////////////////////////////////////////////////////

//Data Management Interface Functions 

/*==========================================================================*
* FUNCTION : DAT_IniMngHisData
* PURPOSE  : When system starts,main module will call this function,
*			: It will initiate all historical data infomation to 
*			: read and write 
* CALLS    : DAT_ReadStaticParameter
*			: DAT_ReadHisDataStatus
*			: DAT_fnWriteDataThread
* CALLED BY: Main module 
* RETURN   : BOOL : FALSE-Fail to initiate    
*			:		 TRUE -Succeed to initiate historical management
* COMMENTS : 
* CREATOR  : Li Xi Dong               DATE: 2004-09-14 16:50
*==========================================================================*/
BOOL DAT_IniMngHisData(void);


//FLASH Interface Functions


/*==========================================================================*
* FUNCTION : DAT_StorageCreate
* PURPOSE  : Open one type of data storage device,if the data doesn't 
*			: exist in FLASH.it will create in FLASH and return address to 
*			: write data record
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: char  DataName[16]          : Current data type name
*            int   iRecordSize           : Record size per piece
*            int   iMaxRecords           : Max records needed to store
*            float iWritefrequencyPerday : Max times to write per day
* RETURN   : HANDLE :					  : handle to write data
* COMMENTS : 
* CREATOR  : Li Xidong                DATE: 2004-09-07 14:24:57
*--------------------------------------------------------------------------*/
HANDLE DAT_StorageCreate (IN char	DataName[16], 
			  IN int	iRecordSize,
			  IN int	iMaxRecords, 
			  IN float iWritefrequencyPerday);

/*==========================================================================*
* FUNCTION : DAT_StorageOpen
* PURPOSE  : Open a historical data records storage device
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: char  DataName[16] : Current data type name
* RETURN   : HANDLE :			   Point to data to read
* COMMENTS : 
* CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:34
*==========================================================================*/
HANDLE DAT_StorageOpen (IN char *pDataName);

/*==========================================================================*
* FUNCTION : DAT_StorageOpenW
* PURPOSE  : Open a historical data records storage device,used to write record
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: char  DataName[16] : Current data type name
* RETURN   : HANDLE :			   Read records pointer, in fact  it's a address
*                                 of g_RECORD_SET_TRACK[i]'s copy;
* COMMENTS : 
* CREATOR  : Li Xi Dong               DATE: 2005-01-31 09:34
*==========================================================================*/
HANDLE DAT_StorageOpenW (IN char *pDataName);

/*==========================================================================*
* FUNCTION : DAT_StorageWriteRecord
* PURPOSE  : Write historical data record to Storage Device
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: HANDLE        hCurrData : Current Data type writing handle
*            unsigned int  ibytes    : Bytes to write
*            void          *pBuffer  : Data buffer to write
* RETURN   : BOOL : 
* COMMENTS : TRUE:succeed   FALSE:failed
* CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:44
*==========================================================================*/
BOOL DAT_StorageWriteRecord (IN HANDLE hCurrData,
			     IN unsigned int ibytes, 
			     IN void * pBuffer);


/*==========================================================================*
* FUNCTION : DAT_StorageUpdateRecord
* PURPOSE  : Set one active alarm record's endtime,end one record
* CALLS    : 
* CALLED BY: Equipement Management Module
* ARGUMENTS: HANDLE        hAlarmRecord	: Alarm data handle
*            int			iRecordID		: Alarm record ID
*            time_t        tmEndTime		: Alarm ending time
* RETURN   : BOOL : TRUE:succeed   FALSE:failed
* COMMENTS : 
* CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:47
*==========================================================================*/
BOOL DAT_StorageUpdateAlarmRecord (IN HANDLE		hAlarmRecord, 
				   IN int			iRecordID, 
				   IN time_t		tmEndTime);

/*==========================================================================*
* FUNCTION : DAT_StorageDeleteRecord
* PURPOSE  : Delete current data type all historical records
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: char *pDataName : 
* RETURN   : BOOL : TRUE:succeed  FALSE:failed
* COMMENTS : 
* CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:48
*==========================================================================*/
BOOL DAT_StorageDeleteRecord (IN char *pDataName);


/*================================================================================*
* FUNCTION : DAT_StorageReadRecords
* PURPOSE  : Read current historical data iRecords pieces from iStartRecordNo.
*			  It's not include active alarm reading
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: HANDLE  *hCurrData      : Data read Handle
*            int     *piStartRecordNo: Start record number, if bAscending=TRUE,
*										(-iMaxRecords) as first record; if 
*										bAscending=FALSE,the latest record as 
*										first record.
*										if *pStartRecordNo=0, it express to continue
*										read following records (IN)
*										else if *pStartRecordNo=(-1),it will read 
*										current Max records. (IN)
*										else if *pStartRecordNo=(-2),Reading records 
*										ended,it has compelted records (OUT)
*            int     *iRecords       : Records to read
*            void    *pBuff          : Buffer, it will be alloced by caller
*			  BOOL    bActiveAlarm    : 1: to read the active alarm
*									    0: to read ended alarm and other data
*            BOOL    bAscending      : TRUE:direction is from -iMaxRecords
*									        postion to current writing position	
*									  : FALSE:From latest record position to oldest
*									   record position
* RETURN   : BOOL :				   TRUE:Succeed	   FALSE:Failed
* COMMENTS : 
* CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:47
*================================================================================*/
BOOL DAT_StorageReadRecords (IN HANDLE	hCurrData, 
			     IN OUT	int		*piStartRecordNo, 
			     IN OUT	int		*piRecords, 
			     OUT		void	*pBuff,
			     IN		BOOL	bActiveAlarm,
			     IN		BOOL	bAscending);

BOOL DAT_CurrentStatusIsRDONLY(char *pszConst);
/*================================================================================*
* FUNCTION : DAT_StorageFindRecords
* PURPOSE  : Find current historical data iRecords pieces from iStartRecordNo.
*			  It's not include active alarm reading
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: HANDLE  *hCurrData      : Data read Handle
*			  COMPARE_PROC		pfnCompare, 
*			  void	  *pCondition
*            int     *pStartRecordNo : Start record number, if bAscending=TRUE,
*										(-iMaxRecords) as first record; if 
*										bAscending=FALSE,the latest record as 
*										first record.
*										if *pStartRecordNo=0, it express to continue
*										read following records (IN)
*										else if *pStartRecordNo=(-1),it will read 
*										current Max records. (IN)
*										else if *pStartRecordNo=(-2),Reading records 
*										ended,it has compelted records (OUT)
*            int     *piRecords      : Records to read or get
*            void    *pBuff          : Buffer, it will be alloced by caller
*			  BOOL    bActiveAlarm    : 1: to read the active alarm
*									    0: to read ended alarm and other data
*            BOOL    bAscending      : TRUE:direction is from -iMaxRecords
*									        postion to current writing position	
*									  : FALSE:From latest record position to oldest
*									   record position
* RETURN   : BOOL :				   TRUE:Succeed	   FALSE:Failed
* COMMENTS : 
* CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:47
*================================================================================*/
BOOL DAT_StorageFindRecords (IN HANDLE				hCurrData, 
			     IN COMPARE_PROC		pfnCompare, 
			     IN void				*pCondition,
			     IN OUT	int				*piStartRecordNo, 
			     IN OUT	int				*piRecords, 
			     OUT	void			*pBuff,
			     IN		BOOL			bActiveAlarm,
			     IN		BOOL			bAscending);

/*======================================================================================*
* FUNCTION : DAT_StorageSearchRecords
* PURPOSE  : Search for records with contions 
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: HANDLE        hCurrData        : Data Handle
*            COMPARE_PROC  pfnCompare       : Callback function
*            void          *pCondition      : Compare conditions
*            int           *piStartRecordNo : Start record number, if bAscending=TRUE,
*										(-iMaxRecords) as first record; if 
*										bAscending=FALSE,the latest record as 
*										first record.
*										if *pStartRecordNo=0, it express to continue
*										read following records (IN)
*										else if *pStartRecordNo=(-1),it will read 
*										current Max records. (IN)
*										else if *pStartRecordNo=(-2),Reading records 
*										ended,it has compelted records (OUT)
*            int           *piRecords       : Valid Records expected to find  
*            void          *pBuff           : Buffer to store valid records
*            BOOL          bActiveAlarm     : TRUE:to search active alarm
*            BOOL          bAscending       : TRUE:direction is from -iMaxRecords
*										     : postion to current writing position
*            BOOL          bIsRecordNo      : TRUE:express only get the valid 
*										     : record's position No.(int) 
*											 : such as (*pStartRecordNo)
* RETURN   : BOOL		 :                   : FALSE: Fail to search; 
*                                             TRUE: Succeed-Actual valid records
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2004-12-23 10:11
*=====================================================================================*/
BOOL DAT_StorageSearchRecords(IN HANDLE		hCurrData, 
			      IN COMPARE_PROC		pfnCompare, 
			      IN		void		*pCondition,
			      IN OUT	int			*piStartRecordNo, 
			      IN OUT	int			*piRecords,
			      OUT		void		*pBuff, 
			      IN		BOOL		bActiveAlarm,
			      IN		BOOL		bAscending,
			      IN		BOOL		bIsRecordNo);

/*==========================================================================*
* FUNCTION : DAT_StorageClose
* PURPOSE  : Close current writing or reading handle
* CALLS    : 
* CALLED BY: High Level Modules
* ARGUMENTS: (HANDLE  hCurrData : Data Handle
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:50
*==========================================================================*/
BOOL DAT_StorageClose (IN HANDLE hCurrData);

/*==========================================================================*
* FUNCTION : DAT_GetFlashRunningState
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   :   BOOL : 0 :Flash is running normally
*				       Other:Flash error, it can't write data to flash,
*						     new history record will be lost
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2005-01-27 16:30
*==========================================================================*/
int	DAT_GetFlashRunningState(void);

/*==========================================================================*
* FUNCTION : DAT_RegisterNotifiy
* PURPOSE  : Register erasing sector pre-process function and parameter
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE                 hCurrData 
*										: In fact it's &g_HISDATA_QUEUE[i]
*            ERASE_SECTOR_PRE_PROC  pfn_PreProcess : 
*            void                   *pParams       : 
* RETURN   :  BOOL : 
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2004-12-30 12:38
*==========================================================================*/
BOOL DAT_RegisterNotifiy(IN HANDLE hCurrData, 
			 IN ERASE_SECTOR_PRE_PROC pfn_PreProcess, 
			 IN void *pParams);

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
* FUNCTION : non_region_erase
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int  Fd     : 
*            IN int  start  : 
*            IN int  count  : 
*            IN int  unlock : 
* RETURN   :  int : 
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2005-02-02 16:39
*==========================================================================*/
int non_region_erase(IN int Fd, 
		     IN int start, 
		     IN int count, 
		     IN int unlock);

/*==========================================================================*
* FUNCTION : DAT_RecoverReadHandle
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN OUT HANDLE  *hHandle : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2005-02-20 11:53
*==========================================================================*/
BOOL DAT_RecoverReadHandle(IN OUT HANDLE *hHandle);

#endif

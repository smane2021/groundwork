/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : applog.h
 *  CREATOR  : Li Xi Dong               DATE: 2004-09-15 12:09
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 * 
 *
 *==========================================================================*/
#ifndef _APP_LOG_OUT_H
#define _APP_LOG_OUT_H

//It will control sys running log write to where-FLASH or File
//Attention: It is set at applog.c,please modify it at applog.c at the same time
#define _APP_LOG_TO_FLASH	1
//#define _APP_LOG_TO_FILE	1
#ifndef _HAS_FLASH_MEM			// add auto support FLASH, maofuhua, 2005-3-26
	#undef _APP_LOG_TO_FLASH	// when in x86 mode, use file log.
#endif

#ifndef _APP_LOG_TO_FLASH
	#define _APP_LOG_TO_FILE	1
#endif
/*
#if defined(_APP_LOG_TO_FLASH) && defined(_APP_LOG_TO_FILE)
#error "The log file only can be written to _APP_LOG_TO_FLASH or _APP_LOG_TO_FILE!"
#elif !defined(_APP_LOG_TO_FLASH) && !defined(_APP_LOG_TO_FILE)
#error "The log file must be written to _APP_LOG_TO_FLASH or _APP_LOG_TO_FILE!"
#endif
*/
//Information level
#define APP_LOG_INFO		0	// Common information
#define APP_LOG_WARNING		1	// Not affect system running, but lose part functions
#define APP_LOG_ERROR		2	// System or service can't run
#define APP_LOG_MILESTONE	3	// Important step
#define APP_LOG_UNUSED		4	// Not Used temperary

/*==========================================================================*
 * FUNCTION : AppLogOut
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *pszTaskName : 
 *            int         iLogLevel    : 
 *            const char  *pszFormat   : 
 *                        ...          : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-03 09:54
 *==========================================================================*/
void AppLogOut( const char *pszTaskName, int iLogLevel, const char *pszFormat, ...);

/*==========================================================================*
 * FUNCTION : LOG_IniWriteFlashLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-02 15:28
 *==========================================================================*/
BOOL LOG_IniWriteFlashLog(void);

/*==========================================================================*
 * FUNCTION : LOG_ClearWriteLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-02 15:29
 *==========================================================================*/
BOOL LOG_ClearWriteFlashLog(void);

/*==========================================================================*
 * FUNCTION : LOG_IniReadFlashLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-31 19:04
 *==========================================================================*/
BOOL LOG_IniReadFlashLog(void);

/*================================================================================*
 * FUNCTION : LOG_StorageReadRecords
 * PURPOSE  : Read current historical data iRecords pieces from iStartRecordNo.
 *			  It's not include active alarm reading
 * CALLS    : 
 * CALLED BY: High Level Modules
 * ARGUMENTS: int     *pStartRecordNo : Start record number, if bAscending=TRUE,
 *										(-iMaxRecords) as first record; if 
 *										bAscending=FALSE,the latest record as 
 *										first record.
 *										if *pStartRecordNo=0, it express to continue
 *										read following records (IN)
 *										else if *pStartRecordNo=(-1),it will read 
 *										current Max records. (IN)
 *										else if *pStartRecordNo=(-2),Reading records 
 *										ended,it has compelted records (OUT)
 *            int     *iRecords       : Records to read
 *            void    *pBuff          : Buffer, it will be alloced by caller
 *			  BOOL    bPrintToScreen  : 1: to print to screen
 *									    0: Not print to screen
 *            BOOL    bAscending      : TRUE:direction is from -iMaxRecords
 *									        postion to current writing position	
 *									  : FALSE:From latest record position to oldest
 *									   record position
 * RETURN   : BOOL :				   TRUE:Succeed	   FALSE:Failed
 * COMMENTS : 
 * CREATOR  : Li Xi Dong               DATE: 2004-09-11 09:47
 *================================================================================*/
BOOL LOG_StorageReadRecords (IN OUT	int		*piStartRecordNo, 
							 IN OUT	int		*piRecords, 
							 OUT	void	*pBuff,
							 IN		BOOL	bPrintToScreen,
							 IN		BOOL	bAscending);

/*==========================================================================*
 * FUNCTION : LOG_ClearReadFlashLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-01 16:04
 *==========================================================================*/
void LOG_ClearReadFlashLog(void);


/*==========================================================================*
 * FUNCTION : LOG_DeleteLogRecords
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-02 18:37
 *==========================================================================*/
BOOL LOG_DeleteLogRecords(void);

#endif








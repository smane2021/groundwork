/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : app_service.h
 *  CREATOR  : ACU Team                   DATE: 2004-10-21 11:49
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __APP_SERVICE_H__
#define __APP_SERVICE_H__

#define SERVICE_MGR		"SERVICE_MGR"


/* used by ServiceMain interface */
struct SServiceArguments
{				
	int	    argc;	        //	the arg num for the service.
	char	**argv;	        //	the argments for the serice, including the 'argc'
							//  number of points containing argment, each argment
							//  is a string end with '\0'. like main(...).
	void	*pReserved;	    //	reserved pointer for the serice.
				
	int	    nQuitCommand;	//	quit command, see SERVICE_QUIT_CMD. the service shall
							// shall check the flag timely 
	DWORD	dwExitCode;	    //	to save the exit code. filled by service when quit.
};				
typedef struct SServiceArguments 	SERVICE_ARGUMENTS;

// each serive must have a SericeMain function with the following type.
typedef DWORD (*SERVICE_MAIN_PROC)
(				
	SERVICE_ARGUMENTS	*pArgs	//	the entrance args.
);				
//Return:	return the exit code. see SERVICE_EXIT_CODE. 
// must be equal to nQuitCommand
//0=ERR_OK	exit normally.


// each serive can have an optionan interface SericeInit function with 
// the following type.
typedef void *(*SERVICE_INIT_PROC)
(				
	IN int argc,		// arg num.
	IN char **argv		// args
);				
// the returned void * pointer will be saved to SERVICE_ARGUMENTS->pReserved,
// and then passed into ServiceMain.

// each service can provide the config proc if it has something need to
// be reconfigured by WEb. maofuhua, 2005-1-6.
typedef int (*SERVICE_CONFIG_PROC)(HANDLE hServiceThread,
				  BOOL bServiceIsRunning,
				  SERVICE_ARGUMENTS *pArgs, 
				  int nVarID, 
				  int *nBufLen, 
				  void *pDataBuf);

#define SERVICE_MAIN_PROC_NAME		"ServiceMain"
#define SERVICE_INIT_PROC_NAME		"ServiceInit"
#define SERVICE_CONFIG_PROC_NAME	"ServiceConfig"


// the basic serivce info 
struct _APP_SERVICE
{
#define LEN_SERVICE_NAME		32
#define LEN_SERVICE_LIB			64
	char	szServiceName [LEN_SERVICE_NAME];	//	the name of service
	char	szServiceLib [LEN_SERVICE_LIB];		//	the lib name of service
	int		nServiceType;						//  the type of the service: 
												//  see APP_SERVICE_TYPE_ENUM
	BOOL	bMandatorySerice;					//  TRUE:the service must be OK

	int		nRunType;							// see APP_SERVICE_RUN_TYPE_ENUM

	SERVICE_ARGUMENTS	args;					//	the args to run the serice

	// runtime info, can NOT modify them!!!!
	HANDLE				hServiceLib;			//	the opened lib handle of the *.so

	SERVICE_MAIN_PROC	pfnServiceMain;			//	the serice entry funcatoin
	SERVICE_INIT_PROC	pfnServiceInit;			//  init proc, optional.
	SERVICE_CONFIG_PROC pfnServiceConfig;		//  config proc, optional

				
	HANDLE	hServiceThread;						//	the handle of running service. 
	BOOL	bServiceRunning;					// TRUE for the service is running.
	BOOL	bReadyforNextService;				// the flag translate to the next service after init, 
};
typedef struct _APP_SERVICE APP_SERVICE;

#define DEF_APP_SERVICE(name, lib, type, bNeed, runAs)	\
	{ (name), (lib), (type), (bNeed), (runAs),			\
		{0, NULL, NULL, 0, 0}, NULL, NULL, NULL, NULL, NULL, FALSE, FALSE}

// the run type of the service.
enum APP_SERVICE_RUN_TYPE_ENUM
{
// do NOT change the value of the two macros.!!!
	SERVICE_RUN_AS_TASK		= 1,			// run as thread
	SERVICE_RUN_AS_DUMMY 	= 2,			// run as a callback function which is called by
											// the service manager periodically(about 1 sec).
	MAX_SERVICE_RUN_TYPE	= 3
};


// the type of service type
enum APP_SERVICE_TYPE_ENUM
{
	SERVICE_OF_LOGIC_CONTROL	= 0,// internal logical control,configured as 'lc'
	SERVICE_OF_USER_INTERFACE	= 1,// user interface,			configured as 'ui'
	SERVICE_OF_DATA_COMM		= 2,// data comm. protocol,		configured as 'dc'
	SERVICE_OF_MISC				= 3,// miscellaneous service.   configured as 'ms'
	SERVICE_OF_UNKNOWN          = 4
};


enum APP_SERVICE_GET_FLAG
{
	SERVICE_GET_BY_NAME			= 0,	// get by name
	SERVICE_GET_BY_LIB			= 1,	// get by the service lib
	SERVICE_GET_BY_IDX			= 2,	// get by the index from 0 to N-1.
	SERVICE_GET_BY_UUID			= 3,	// get by uuid, not implemented.
	SERVICE_GET_BY_HANDLE		= 4,	// get by the service HANDLE--thread ID.
};


/*==========================================================================*
 * FUNCTION : *ServiceManager_GetService
 * PURPOSE  : get the service infomation by 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int   nGetByFlag      :
 *            IN void  *pszServiceFlag : if nGetByFlag is SERVICE_GET_BY_NAME,
 *                                          pszServiceFlag is the Service Name(char *),
 *										 if nGetByFlag is SERVICE_GET_BY_LIB(char *),
 *                                          pszServiceFlag is the shared lib name,
 *										 if nGetByFlag is SERVICE_GET_BY_IDX,
 *                                          pszServiceFlag is index of the array(int)
 * RETURN   : APP_SERVICE : NULL for not found, else the service address.
 * COMMENTS : do NOT change any field of the service!!!
 * CREATOR  : Mao Fuhua(Frnak)         DATE: 2005-01-07 15:29
 *==========================================================================*/
extern APP_SERVICE *ServiceManager_GetService(IN int nGetByFlag, 
											  IN void *pszServiceFlag);

/*==========================================================================*
 * FUNCTION : Service_Start
 * PURPOSE  : load and create an service.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT APP_SERVICE *pService : 
 * RETURN   : BOOL : FALSE for failure.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 18:34
 *==========================================================================*/
BOOL Service_Start(IN OUT APP_SERVICE *pService);

/*==========================================================================*
 * FUNCTION : Service_Stop
 * PURPOSE  : stop a service and release the resource of the service
 * CALLED BY: 
 * ARGUMENTS: IN OUT APP_SERVICE *pService      : 
 *            IN int     nTimeToWaitServiceQuit : wait service quit in ms.
 *                       The thread will be killed if timeout
 * RETURN   : int : ERR_THREAD_OK for OK, 
 *                  ERR_THREAD_STILL_RUNNING for thread does not stopped,
 *                  ERR_THREAD_KILLED for thread is terminated by force.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 18:45
 *==========================================================================*/
int Service_Stop(IN APP_SERVICE *pService,
				IN int nTimeToWaitServiceQuit);

/*==========================================================================*
 * FUNCTION : Service_PostQuitMessage
 * PURPOSE  : post quit message to the service. 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT APP_SERVICE  *pService : 
 * RETURN   : BOOL :TRUE for successful. 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 17:22
 *==========================================================================*/
BOOL Service_PostQuitMessage(IN OUT APP_SERVICE *pService);

/*==========================================================================*
 * FUNCTION : Service_IsRunning
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN APP_SERVICE  *pService : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-24 14:44
 *==========================================================================*/
BOOL Service_IsRunning(IN APP_SERVICE *pService);

// service manager info
struct _SERVICE_MANAGER
{
	// the service info.
	int					nService;
	APP_SERVICE			*pService;

	// runtime info	
	HANDLE				hServiceManager;// handle of the service manager thread.

	//count of all type services. [0] is the total count.
	int					pRunningCount[MAX_SERVICE_RUN_TYPE];

	HANDLE				hLock;
};
typedef struct _SERVICE_MANAGER		SERVICE_MANAGER;



/* service exit code */
enum SERVICE_EXIT_CODE
{
	SERVICE_EXIT_OK	 = 0,		   /* exit successfully */	
	SERVICE_EXIT_CFG,              /* exit for bad config */
	SERVICE_EXIT_MEMORY,		   /* exit for no memory */
	SERVICE_EXIT_FAIL,             /* exit for undefined error */
	SERVICE_EXIT_STAR_DB,          /* exit, need start debug service */
	SERVICE_EXIT_STAR_ESR,         /* exti, need start esr service */
	ERR_SERVICE_CFG_ESR_YDN,	/*exit,*/
	SERVICE_EXIT_NONE              /* not need exit */
};

enum SERVICE_QUIT_CMD
{
	SERVICE_CONTINUE_RUN	= 0,	// continue run.
	SERVICE_STOP_RUNNING	= 1,	// stop all and exit service
};


/* service private config mask definition*/
/* ESR service */
#define ESR_CFG_W_MODE            0x1         /* 1 for update, 0 for read */
#define ESR_CFG_ALL               0x2
#define ESR_CFG_PROTOCOL_TYPE     0x4
#define ESR_CFG_CCID              0x8

#define ESR_CFG_SOCID             0x10
#define ESR_CFG_REPORT_IN_USE     0x20
#define ESR_CFG_CALLBACK_IN_USE   0x40
#define ESR_CFG_MEDIA_TYPE        0x80

#define ESR_CFG_MEDIA_PORT_PARAM  0x100
#define ESR_CFG_MAX_ATTEMPTS      0x200
#define ESR_CFG_ATTEMPT_ELAPSE    0x400
#define ESR_CFG_REPORT_NUMBER_1   0x800

#define ESR_CFG_REPORT_NUMBER_2   0x1000
#define ESR_CFG_CALLBACK_NUMBER   0x2000
#define ESR_CFG_IPADDR_1          0x4000
#define ESR_CFG_IPADDR_2          0x8000

#define ESR_CFG_SECURITY_IP_1     0x10000								  
#define ESR_CFG_SECURITY_IP_2     0x20000
#define ESR_CFG_SECURITY_LEVEL    0x40000
#define ESR_CFG_PRODCT_INFOM      0x80000

/* YDN service */
#define YDN_CFG_W_MODE            0x1         /* 1 for update, 0 for read */
#define YDN_CFG_ALL               0x2
#define YDN_CFG_PROTOCOL_TYPE     0x4
#define YDN_CFG_ADR               0x8

#define YDN_CFG_REPORT_IN_USE     0x10
#define YDN_CFG_MEDIA_TYPE        0x20

#define YDN_CFG_MEDIA_PORT_PARAM  0x100
#define YDN_CFG_MAX_ATTEMPTS      0x200
#define YDN_CFG_ATTEMPT_ELAPSE    0x400
#define YDN_CFG_REPORT_NUMBER_1   0x800
#define YDN_CFG_REPORT_NUMBER_2   0x1000
#define YDN_CFG_REPORT_NUMBER_3   0x2000

#define YDN_CFG_IPADDR_1          0x4000
#define YDN_CFG_IPADDR_2          0x8000


#define MODBUS_CFG_W_MODE            0x1         /* 1 for update, 0 for read */
#define MODBUS_CFG_ALL               0x2
#define MODBUS_CFG_PROTOCOL_TYPE     0x4
#define MODBUS_CFG_ADR               0x8

#define MODBUS_CFG_REPORT_IN_USE     0x10
#define MODBUS_CFG_MEDIA_TYPE        0x20

#define MODBUS_CFG_MEDIA_PORT_PARAM  0x100

//changed by Frank Wu,N/N/N,20150429, for supporting TL1
/* TL1 service */
#define TL1_CFG_W_MODE					0x1/* 1 for update, 0 for read */
#define TL1_CFG_ALL						0x2
#define TL1_CFG_PROTOCOL_TYPE			0x4
#define TL1_CFG_ADR						0x8
#define TL1_CFG_MEDIA_TYPE				0x10
#define TL1_CFG_MEDIA_PORT_PARAM		0x20

#define TL1_CFG_PORT_ACTIVATION			0x40
#define TL1_CFG_PORT_KEEP_ALIVE			0x80
#define TL1_CFG_SESSION_TIMEOUT			0x100
#define TL1_CFG_AUTO_LOGIN_USER			0x200
#define TL1_CFG_SYSTEM_IDENTIFIER		0x400
#define TL1_CFG_AID_GROUP_INFO			0x800


/* err code definition(may move to errcode.h later) */
/* used as returned code of ServiceConfig interface */
enum ERR_SERVICE_CFG
{
	ERR_SERVICE_CFG_OK = 0,
	ERR_SERVICE_CFG_FAIL,
	ERR_SERVICE_CFG_EXIT,        /* service is exited */
	ERR_SERVICE_CFG_VARID,       /* invalid nVarID param */
	ERR_SERVICE_CFG_DATA,        /* invalid pDataBuf param */
	ERR_SERVICE_CFG_FLASH,       /* write to flash failed */
	ERR_SERVICE_CFG_BUSY,		 /* service is busy, can not change config now */
	ERR_SERVUCE_CFG_ESR_COMUSED,  /* non-shared port has already been occupied */
	ERR_SERVICE_CFG_YDN_COMUSED,  /* non-shared port has already been occupied */
	ERR_SERVICE_CFG_MODBUS_COMUSED,
	//changed by Frank Wu,N/N/N,20150429, for supporting TL1
	ERR_SERVICE_CFG_TL1_COMUSED,  /* non-shared port has already been occupied */
	ERR_SERVICE_TL1_NOT_ENABLE	/*stCommonConfig.iModuleSwitch == 0*/
};

///* for service changing */
//enum SENDER_TYPE
//{
//	SENDER_ESR = 0,
//	SENDER_DEBUGER,
//	SENDER_NUM
//};
//
//void Main_FrameJudger(int iSenderType, 
//					  const unsigned char *sFrameData, 
//					  int iFrameDataLen);

#endif //__APP_SERVICE_H__

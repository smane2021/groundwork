/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : cfg_reader.h
 *  CREATOR  : LinTao                   DATE: 2004-09-08 
 *  VERSION  : V1.00
 *  PURPOSE  : Head file for Config Reader
 *
 *  HISTORY  : based on the EMNET project ( Author: maofuhua )
 *==========================================================================*/
#ifndef __CFG_READER_H_2004_09_08__
#define __CFG_READER_H_2004_09_08__

#ifndef MAX_PATH
#define MAX_PATH    256
#endif

/* for config file modification */
#ifndef MAX_EXTRA_SIZE
#define  MAX_EXTRA_SIZE		1024
#endif

/* parse talbe function type */
#define CFG_RECORD_SKIP		(-99)     //added for G3_OPT [loader]
typedef int (*PARSE_RECORD_PROC)(char *szBuf, char *pStructData);

/* release table function type */
typedef void (*RELEASE_RECORD_PROC)(char *pStructData);

/* load config file function type */
typedef int (*LOAD_CONF_PROC)(IN void *pCfg, OUT void *pLoadToBuf);

/* structures */
/* load a table */
struct tagConfigTableLoader
{
    const char    *szNumKey;
    int			  *pRowNum;

    const char    *szDataKey;
    char          **ppRecords;
    int           iRecordSize;

    PARSE_RECORD_PROC   pfnParseRecord;
};
typedef struct tagConfigTableLoader CONFIG_TABLE_LOADER;

/* used to initialize the CONFIG_TABLE_LOADER structure */
#define DEF_LOADER_ITEM(p, NumKey, RowNum, DataKey, Records, myLoadProc) \
   ((p)->szNumKey = (NumKey), \
    (p)->pRowNum = (RowNum), \
    (p)->szDataKey = (DataKey), \
    (p)->ppRecords = (char **)(Records), \
    (p)->iRecordSize = sizeof((Records)[0][0]), \
    (p)->pfnParseRecord = (PARSE_RECORD_PROC)(myLoadProc))



/* free a table memory(in memory, table is stored as structure) */
struct tagConfigTalbeUnloader
{
    int     *pRowNum;
    char    **ppRecords;
    int     iRecordSize;

    RELEASE_RECORD_PROC pfnReleaseRecord;
};
typedef struct tagConfigTalbeUnloader CONFIG_TABLE_UNLOADER;

/* used to initialize CONFIG_TABLE_UNLOADER structure */
#define DEF_UNLOADER_ITEM(p, RowNum, Records, myUnloadProc)    \
   ((p)->pRowNum = (RowNum), \
    (p)->ppRecords = (char **)(Records), \
    (p)->iRecordSize = sizeof((Records)[0][0]), \
    (p)->pfnReleaseRecord = (RELEASE_RECORD_PROC)(myUnloadProc))




/* modify config file */
struct tagConfigFileModifier
{
	const char  *szSectionKey;
	int         iRow;
	int         iCol;

	const char *szContent;
};
typedef struct tagConfigFileModifier CONFIG_FILE_MODIFIER;

/* used to init CONFIG_FILE_MODIFIER structure */
#define DEF_MODIFIER_ITEM(_p, _szSectionKey, _iRow, _iCol, _szContent)  \
	((_p)->szSectionKey = (_szSectionKey), \
	 (_p)->iRow = (_iRow),  \
	 (_p)->iCol = (_iCol),  \
	 (_p)->szContent = (_szContent))



/* interfaces */
int Cfg_LoadCfgFirstTime(const char *szConfigFile,
					   LOAD_CONF_PROC pfnLoader, 
					   void *pBuf );

int Cfg_LoadConfigFile(const char *szConfigFile,
					   LOAD_CONF_PROC pfnLoader, 
					   void *pBuf );

int Cfg_LoadTables(IN void *pCfg, 
				   IN int nTable, 
				   IN OUT CONFIG_TABLE_LOADER  *loader);

void Cfg_UnloadTables(IN int nTable, 
					  IN OUT CONFIG_TABLE_UNLOADER  *unloader);

int Cfg_ModifyConfigFile(IN const char *szFileName,
						 IN int iModifiers,
						 IN CONFIG_FILE_MODIFIER *pModifier,
						 OUT char **szOutFile);


#ifdef _DEBUG
	//#define _SHOW_LOADING_INFO
#endif //_DEBUG

#endif //_CFG_READER_H_2004_09_08

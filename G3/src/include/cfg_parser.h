/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : cfg_parser.h
 *  CREATOR  : LinTao                   DATE: 2004-09-07 
 *  VERSION  : V1.00
 *  PURPOSE  : Head file for Config Parser
 *
 *  HISTORY  : based on the EMNET project ( Author: maofuhua )
 *==========================================================================*/

#ifndef __CONFIG_PARSER_H_2004_09_06__
#define __CONFIG_PARSER_H_2004_09_06__

#ifndef MAX_LINE_SIZE
#define MAX_LINE_SIZE   (1048+512)		   //���������ļ����� �ַ���󳤶�  
#endif


/* file management structure, will be used by the Config Parser and Config Reader */
struct tagProfile
{
    char    *pFileBuffer;        /* the file contents buffer */

#ifndef FILE_END_WITH_0
    char    *pEndFile;           /* the end buffer ptr */
#endif 	                     

    char    *pReadPtr;           /* the current read position of buffer */
};
typedef struct tagProfile SProfile;


/* structures used by current file */
/* text type definition */
enum TextType
{
	ENGLISH_FULL		= 1,
	ENGLISH_ABBR,
	LOCALE_FULL,
	LOCALE_ABBR,

    LOCALE2_FULL,//Added by wj for three languages 2006.5.9
    LOCALE2_ABBR //Added by wj for three languages 2006.5.9

};

struct tagRunConfigItem
{
	/* Use CONFIG_CHANGED_XXX macro, defined in the cfg_model.h */
	DWORD	dwChangeInfoType;   

	/*0 for English, 1 for locale language ,2 for locale2 language//Added by wj for three languages 2006.5.9 */
	int		iTextType;		  /* use TextType enum */
	char	*szText;          /* new text */
	int		iEquipID;
	int		iEquipTypeID;
	int		iSigType;
	int		iSigID;
};
typedef struct tagRunConfigItem RUN_CONFIG_ITEM;

#define DEF_RUN_CONFIG_ITEM(dwChangeInfoType, iTextType, szText, iEquipID, iEquipTypeID, iSigType, iSigID) \
{ (dwChangeInfoType), (iTextType), (szText), (iEquipID), (iEquipTypeID), (iSigType), (iSigID) }



/* Profile struct operation functions */
void *Cfg_ProfileOpen(IN char *szFileContents, IN int nFileSize);
void Cfg_ProfileRewind(IN void *pProf);
long Cfg_ProfileTell(IN void *pProf);
BOOL Cfg_ProfileSeekSet(IN void *pProf, IN long lPos);  //G3_OPT [loader]
void Cfg_ProfileClose(IN void *pProf);
int Cfg_ProfileReadLine(IN SProfile *pProf, OUT char *pszBuf, IN int nBufSize);


/* directly read functions */
int Cfg_ProfileGetInt(IN void *pCfg, 
					  IN const char *pszSectionKey, 
					  OUT int *pNum); 

int Cfg_ProfileGetString(IN void *pCfg, 
						 IN const char *pszSectionKey, 
                         OUT char *pszBuf, 
						 IN int nBufLen);

int Cfg_ProfileGetLine(IN void *pProf, 
					   IN const char *pszSectionKey, 
					   OUT char *pszBuf, 
					   IN int nBufSize);

int Cfg_ProfileGetNextLine(IN void *pProfile, 
						   OUT char *buf, 
						   IN int nBufLen);


/* section functions */
int Cfg_ProfileFindSection(IN void *pProf, IN const char *pszSectionKey);
int Cfg_GetSectionLineCount(IN void *pProf, IN const char *szSectionKey);
int Cfg_ProfileGetSectionPos(IN void *pProf,
							 IN const char *szKey, 
							 OUT size_t *pulStartPos, 
							 OUT size_t *pulEndPos);


/* split string functions */
int Cfg_GetSplitStringCount(IN char *p, IN int cSplitter);

char *Cfg_SplitString(IN char *pszSrc, 
					  OUT char *pszDst, 
					  IN int nBufLen, 
					  IN int cSplitter);

char *Cfg_SplitStringEx(IN char *pszSrc, 
						OUT char **ppszDst, 
						IN  int cSplitter); // maofuhua change 'char' to 'int'.
//Frank Wu,20150525, for TL1
extern char *Cfg_SplitStringExForTL1(IN char *pszSrc, 
						OUT char **ppszDst, 
						IN  int cSplitter,
						OUT BOOL *pbHasSplitter); // maofuhua change 'char' to 'int'.

//added for G3_OPT [loader], by Thomas, 2013-5
char *Cfg_SplitStringIndex(IN char *pszSrc, OUT char *pszDst, IN int nBufLen,
						   IN int cSplitter, IN int nIndex);

/* used to remove certain chrs */
char *Cfg_RemoveWhiteSpace(IN OUT char *pszBuf);

typedef BOOL (*IS_BOUND_CHR_PROC)(int c);  
char *Cfg_RemoveBoundChr(IN char *pszBuf, IN IS_BOUND_CHR_PROC pfnIsBoundChr);

/* empty field judge */
BOOL Cfg_IsEmptyField(IN const char *pField);


/* field validity check function */
/* compare function prototype */
typedef BOOL (*LEGAL_CHR)(char c);
BOOL CFG_CheckBase(const char *szObject, LEGAL_CHR fnCmp);
BOOL CFG_CheckNumber(const char *szNumber);
BOOL CFG_CheckPNumber(const char *szNumber);
BOOL CFG_CheckHexNumber(const char *szHexNumber);

#endif

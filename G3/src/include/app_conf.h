 /*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : app_conf.h
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-19 09:50
 *  VERSION  : V1.00
 *  PURPOSE  : the configurations of ACU application.
 *			   the global environment about ACU app configuration
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __APP_CONF_H__
#define __APP_CONF_H__

/*-------------------------------------------------------------------------*/
/* ACU vesion info is combined by MAJOR.MINOR.BUILD. 
 * 
 * MAJOR version: start from 0 to N.	
 * MINOR version: start from 01 to 99.
 * BUILD version: start from 01 to 99.	#deleted. maofuhua, 2005-3-13
 */
//#define MK_VER(major, minor, build) ((unsigned long)((major)*10000 + (minor)*100 + (build)))


//#define MK_VAR_FOR_FB //special for FB version(X.XXX)

#ifdef MK_VAR_FOR_FB
#define MK_VER(major, minor) ((unsigned long)((major)*1000 + (minor)))
#else
#define MK_VER(major, minor) ((unsigned long)((major)*100 + (minor)))
#endif

/* the ACU version list and brief of the changes                           */


//#define ACU_APP_VER			MK_VER( 1,  00)			/* 0.01,  2013/01/21 10:41*/
//#define ACU_APP_VER			MK_VER( 1,  00)			/* 1.00,  2014/03/31 10:41*/
//#define ACU_APP_VER			MK_VER( 1,  01)			/* 1.01,  2014/04/10 10:41*/
//#define ACU_APP_VER			MK_VER( 1,  11)			/* 1.10,  2014/05/09 15:25*/
//#define ACU_APP_VER			MK_VER( 1,  20)			/* 1.20,  2014/08/11 16:47*/
//#define ACU_APP_VER			MK_VER( 1,  21)			/* 1.20,  2014/09/22 16:47*/
//#define ACU_APP_VER			MK_VER( 1,  22)			/* 1.20,  2014/10/14 09:02*/
//#define ACU_APP_VER			MK_VER( 1,  30)			/* 1.25,  2014/12/19 09:02*/
//#define ACU_APP_VER			MK_VER( 1,  42)			/* 1.40,  2014/03/20 09:02*/
//#define ACU_APP_VER			MK_VER( 1,  50)			/* 1.50,  2016/01/07 09:02*/
//#define ACU_APP_VER			MK_VER( 1,  70)			/* 1.50,  2016/05/31 14:49*/
#define ACU_APP_VER			MK_VER( 2,  20)			/* 1.80,  2017/04/19 14:49*/


#define ACU_APP_TEST_VER		""

#define	CR_CFGFILE_INFO						/*CR:add config version to lcd	-caihao  2005.12.23*/
#define	CR_KEYPAD_VOICE						/*CR:keypad voice enabled		-caihao	2005.12.26*/

#define	CR_BARCODE_LCDDISPLAY					/*CR:display barcode on lcd		-caihao  2006.01.19*/

#define AUTO_CONFIG

//Define the special version of ACU+
//#define GC_SUPPORT_HIGHTEMP_DISCONNECT			/*Support the OBLVD2 disconnect when the abs(battery temp, ambine temp ) > 10.0*/
//End:Define the special version of ACU+

#define GC_SUPPORT_RELAY_TEST					//Support Relay test function,2011-12-5
//#define SUPPORT_CSU //for support CSU

#define GC_SUPPORT_MPPT						//Support MPPT function,2012-3-31
#define GC_SUPPORT_FIAMM					//Support Fiamm battery, 2015-3-4
#define GC_SUPPORT_BMS						//Support Narada battery, 2016-1-5

//flag file to indicate loading all equipment, by default, G3 only load typical equipments.
//#define LOADALLEQUIP_FLAGFILE	"run/LoadAllEquip.run"
#define LOADALLEQUIP_FLAGFILE	"run/NotLoadRS485.run"  //For compatible with V4.03 config file ,
							//so the file is exist not load rs485, not exist, then load rs485

/*-------------------------------------------------------------------------*/
/* method to get current ver                                               */
extern DWORD		g_dwSWVersion;	// defined in main.c
#define GET_APP_VER()			g_dwSWVersion

#ifdef MK_VAR_FOR_FB
#define GET_APP_MAJOR_VER(ver)		((int)((ver) / 1000))
#define GET_APP_MINOR_VER(ver)		((int)((ver) % 1000))

#define STRING_APP_VER(szStrVer, nLenStrVer)			\
	snprintf((szStrVer), (nLenStrVer), "%d.%03d%s",	\
	GET_APP_MAJOR_VER(GET_APP_VER()), GET_APP_MINOR_VER(GET_APP_VER()),ACU_APP_TEST_VER)
#else
#define GET_APP_MAJOR_VER(ver)		((int)((ver) / 100))
#define GET_APP_MINOR_VER(ver)		((int)((ver) % 100))

#define STRING_APP_VER(szStrVer, nLenStrVer)			\
	snprintf((szStrVer), (nLenStrVer), "1.%d.%02d%s",	\
		GET_APP_MAJOR_VER(GET_APP_VER()), GET_APP_MINOR_VER(GET_APP_VER()),ACU_APP_TEST_VER)

#endif
/*-------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------*/
// Runtime environment varibales. these vars shall be set before ACU starting.
// In bash shell, the environment is set by the following syntax:
//		#export var=settings
// 
// The root dir of the ACU application, set by shell script startacu
#define ENV_ACU_ROOT_DIR			"ACU_ROOT_DIR"
// The hardware switch to disable ACU changes. set when ACU started.
//#define ENV_SYS_JUMP_APP_RDONLY		"SCUP_JUMP_APP_RDONLY:SET"
#define ENV_SYS_JUMP_APP_RDONLY		"JUMP_APP_RDONLY:SET"

//#define ENV_ACU_JUMP_APP_RDONLY		"ACU_JUMP_APP_RDONLY"
// The password switch to resume the password of "admin" to default password.
//#define ENV_ACU_JUMP_DEFAULT_PASSWD	"SCUP_JUMP_DEFAULT_PASSWD"
//#define ENV_ACU_JUMP_DEFAULT_PASSWD	"SCUP_JUMP_DEFAULT_PASSWD:SET"
#define ENV_ACU_JUMP_DEFAULT_PASSWD	"JUMP_DEFAULT_PASSWD:SET"
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
#define STD_ID_IS_GROUP(id)					(((id)%100) == 0)
#define STD_ID_IS_IN_GROUP(stdId, grpId)	(!STD_ID_IS_GROUP((stdId)) && \
											((((int)(stdId)/100)*100) == (int)(grpId))

// standard signal ID in ACU system(StdEquip_System.cfg)
// the signals need to be processed by system in specially
#define STD_ID_ACU_SYSTEM_EQUIP			100 // the ACU system equipment standard type
//Power sytem type value setting signal
//#define SIG_ID_SYSTEM_TYPE				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 41)	//System Type Value Setting
//#define	ERR_SYSTEM_TYPE_VALUE			0xFF

#define SYS_STATE_BY_AUTO				0
#define SYS_STATE_BY_MAN				1
#define SIG_ID_AUTO_MAN_STATE			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 7)	// Auto/Man State

#define SIG_ID_HW_SWITCH				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 7)	// Hardware Write-protect Switch
#define SIG_ID_SYSTEM_STATUS			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 20)	// System status
#define SIG_ID_ALARM_OUTGOING_BLOCKED	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 8)	// Outgoing Alarms Blocked
#define SIG_ID_A_ALARM_OUTGOING_BLOCKED	DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, 10)	// Outgoing Alarms Blocked

#define SIG_VAL_SYS_INTERNAL_FLASH_FAULT	1
#define SIG_ID_SYS_INTERNAL_STATUS		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 22)	// Supervision Unit Internal Status
#define SIG_ID_SYS_CONFIG_STATUS		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 24)	// Running Config Type

#define SIG_ID_SYSTEM_POWER				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 3)	// System Power
#define SIG_ID_POWER_PEAK_24HR			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 5)	// Power Peak In 24 Hours
#define SIG_ID_AVERAGE_POWER_24HR		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 6)	// Average Power in 24 Hours

#define SIG_ID_TOTAL_ALARM				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 28)	// Total Alarm Num Including Suppressed Alarms
#define SIG_ID_TOTAL_OA					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 29)	// Total O Alarm Num Including Suppressed Alarms
#define SIG_ID_TOTAL_MA					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 30)	// Total M Alarm Num Including Suppressed Alarms
#define SIG_ID_TOTAL_CA					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 31)	// Total C Alarm Num Including Suppressed Alarms

#define SIG_ID_MAINTN_TIME_RUN			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 24) // Maintenance time run
#define SIG_ID_MAINTN_DELAY				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 4)	// Maintenance time delay
#define SIG_ID_MAINTN_LIMIT				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 11)	// Maintenance time limit

#define SIG_ID_SYSTEM_TIME				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 21)	// System Time

#define SIG_ID_IB4_COMM_STATUS			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 216)	//IB4 Communication status 

#define SIG_ID_LANGUAGE_SUPPORT			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 22)	// Language
#define SIG_ID_ALARM_VOICE_SET			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 23)	// Alarm voice
#define SIG_ID_TIME_ZONE_SET			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 25)	// Time zone

#define SIG_ID_TIME_SYNC_SVR_IP			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 26)	// Time sync main server 
#define SIG_ID_TIME_SYNC_BACKUP_IP		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 27)	// Time sync backup server
#define SIG_ID_TIME_SYNC_INTERVAL		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 28)	// Time sync interval
#define SIG_ID_TIME_SYNC_TIMEZONE		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 448)	// Time Zone

#define SIG_ID_RESET_SCU_CTRL			DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 10)	// Reset SCU
#define SIG_ID_SYS_ALARM_LED_CTRL		DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 11)	// Control system alarm LED

#define SIG_ID_SMDU_SAMPLER_MODE_CTRL		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 183)	// Control system alarm LED

#ifdef CR_KEYPAD_VOICE			/*CR:keypad voice enabled		-caihao	2005.12.26*/
//#define	SIG_ID_KEYPAD_VOICE_SET			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 32)	// Set keypad voice
#define	SIG_ID_KEYPAD_VOICE_SET			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 35)	// Set keypad voice
//In scu plus ,it is set to 35 temperarily
#endif

#define SIG_ID_RECT_SN					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8)	// Rectifier SN

#ifndef PRODUCT_INFO_SUPPORT
#define PRODUCT_INFO_SUPPORT
#endif

//#ifdef PRODUCT_INFO_SUPPORT
	#define SIG_ID_RECT_HI_SN			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 26)	// High part of Rectifier SN
	#define SIG_ID_RECT_VER				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 27)	// Rectifier HW & SW version
	#define SIG_ID_RECT_PARTNUMBER		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 37)	// Rectifier partnumber
//#endif //PRODUCT_INFO_SUPPORT

#define SIG_ID_OTHER_SN				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 92)	// Rectifier SN
#define SIG_ID_OTHER_HI_SN			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 93)	// High part of Rectifier SN
#define SIG_ID_OTHER_VER				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 94)	// Rectifier HW & SW version
#define SIG_ID_OTHER_PARTNUMBER1			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 95)	// Rectifier partnumber
#define SIG_ID_OTHER_PARTNUMBER2			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 96)	// Rectifier partnumber
#define SIG_ID_OTHER_PARTNUMBER3			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 97)	// Rectifier partnumber
#define SIG_ID_OTHER_PARTNUMBER4			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 98)	// Rectifier partnumber



#define RECT_GROUP_STD_EQUIP_ID				200 // the Rectifier group equipment standard type
#define RECT_STD_EQUIP_ID					201 // the Rectifier equipment standard type
#define RECT_STD_EQUIP_ID_SLAVE1			1601 // the Rectifier equipment standard type
#define RECT_STD_EQUIP_ID_SLAVE2			1701 // the Rectifier equipment standard type
#define RECT_STD_EQUIP_ID_SLAVE3			1801 // the Rectifier equipment standard type
#define MPPT_STD_EQUIP_ID				2501

#define IB_STD_EQUIP_ID					801 // the IB equipment standard type
#define EIB_STD_EQUIP_ID				901 // the EIB equipment standard type
//folowing 8 SMDU are added by Jimmy acording to EMEA's TR
#define SMDU_STD_EQUIP_ID				1001 // the SMDU equipment standard type
#define SMDU1_STD_EQUIP_ID				1001 
#define SMDU2_STD_EQUIP_ID				1002
#define SMDU3_STD_EQUIP_ID				1003
#define SMDU4_STD_EQUIP_ID				1004
#define SMDU5_STD_EQUIP_ID				1005
#define SMDU6_STD_EQUIP_ID				1006
#define SMDU7_STD_EQUIP_ID				1007
#define SMDU8_STD_EQUIP_ID				1008
//end add by Jimmy 2011-09-01

//#define SMDUP_STD_EQUIP_ID				1901 // the SMDU equipment standard type
//#define SIG_ID_ACTUAL_SMDUP_NUM			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 1)	// actual smdup number	in SMDUPGroup.cfg
#define SIG_ID_ACTUAL_RECT_NUM			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8)	// actual rectifiers number	in RectGroup.cfg
#define SIG_ID_ACTUAL_MPPT_NUM			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8)	// actual Mppt number	in MpptGroup.cfg
#define SIG_ID_ACTUAL_SLAVE_RECT_NUM	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 2)	// actual slave rect num in SlaveSystem.cfg
#define SIG_ID_RECT_COMM_STATUS			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 10) // Communication Interrupt count
#define SIG_ID_ALL_RECT_TWINKLE			DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 5)	// All Rects twinkle
#define SIG_ID_ALL_CONV_TWINKLE			DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 5)	// All Convs twinkle
#define SIG_ID_RECT_TWINKLE				DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 3)	// Rect twinkle
#define SIG_ID_RECT_AC_STATUS				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 19) // AC Status
#define SIG_ID_RECT_CONV_POSITION			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 34)


#define SIG_ID_RECT_REALLOC_ADDR		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 37) // Check Rectifier address reallocate

#define SIG_ID_CONV_TWINKLE				DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 3)	// Conv twinkle

//used by auto config
#define SIG_ID_EQUIP_COMM_STATUS			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 99) // Communication Interrupt count
#define SIG_ID_EQUIP_WORK_STATUS			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 100) // Communication Interrupt count
#define IB_GROUP_EQUIP_ID				202
#define CONV_GROUP_STD_EQUIP_ID				1100
#define CONV_STD_EQUIP_ID				1101
#ifdef GC_SUPPORT_MPPT
#define MPPT_GROUP_STD_EQUIP_ID				2500
#define MPPT_STD_EQUIP_ID				2501
#define SIG_ID_MPPT_TWINKLE				DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 3)	// Conv twinkle
#define SIG_ID_ALL_MPPT_TWINKLE			DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 5)	// All Convs twinkle
#endif 

//Battery Group Std EquipID and Battery Unit StdEquipID
#define BAT_GROUP_STD_EQUIP_ID				300
#define SMDU_GROUP_STD_EQUIP_ID				1000
#define SMBRC_GROUP_STD_EQUIP_ID			2000
//#define SMDUP_GROUP_STD_EQUIP_ID			1900

#define SIG_ID_MB_BATT_NUM		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,64)
#define SIG_ID_EIB_BATT_NUM		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,7)


#define SIG_ID_SMDU_BATT_NUM		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,3)
#define SIG_ID_SMBRC_BATT_NUM		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,4)


#define DEISEL_GROUP_STD_EQUIP_ID			1500
#define SIG_ID_DEISEL_GROUP_EXIST_NUM		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,100)
/*#define BAT_UNIT_STD_EQUIP_ID				301

//AC/DC Distribute Equip Sig StdEquip_ACDistribution.cfg StdEquip_DCDistribution.cfg
#define ACD_STD_EQUIP_ID				401 // the AC Distribute equipment standard type
#define DCD_STD_EQUIP_ID				501 // the AC Distribute equipment standard type

#define	ACD_COMM_STARUS_SIGID			77
#define	DCD_COMM_STARUS_SIGID			89
#define ACD_GROUP_STD_EQUIP_ID			400 // the AC Distrib group equipment standard type
#define SIG_ID_ACD_COMM_STATUS			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 63) // Communication Interrupt count

//Added by hulw 06.09.03
#define SIG_ID_ACTUAL_AC_NUM			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1)	// actual AC number
#define SIG_ID_ACTUAL_DC_NUM			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1)	// actual DC number
//#define SIG_ID_ACTUAL_BATT_NUM		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 63)	// actual Batt number
#define SIG_ID_ACTUAL_BATT_NUM			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 40)	// actual Batt number

#define DCD_GROUP_STD_EQUIP_ID			500 // the DC Distrib group equipment standard type
#define SIG_ID_DCD_COMM_STATUS			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 84) // Communication Interrupt count

//Data Change Flag,used for YDN23 Protocol from file StdEquip_System.cfg
#define SIG_ID_DCD_DATA_FLAG			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 32)//BYTE type:0000'000X bit0	
#define SIG_ID_DCD_ALARM_FLAG			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 33)//BYTE type:000X'0000 bit4	
#define SIG_ID_ACD_DATA_FLAG			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 34)//BYTE type:0000'000X bit0	
#define SIG_ID_ACD_ALARM_FLAG			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 35)//BYTE type:000X'0000 bit4
#define SIG_ID_RECT_DATA_FLAG			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 36)//BYTE type:0000'000X bit0	
#define SIG_ID_RECT_ALARM_FLAG			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 37)//BYTE type:000X'0000 bit4	
#define SIG_ID_SYS_DATA_FLAG			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 39)//BYTE type:0000'000X bit0	
#define SIG_ID_SYS_ALARM_FLAG			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 40)//BYTE type:000X'0000 bit4
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//AC Distribute Group UpDnLimit Setting Info
#define ACDG_INPUT_OV_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 2)//From Cfg
#define ACDG_INPUT_UV_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 3)//
#define ACDG_PHASE_FAIL_VOLT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 4)//
#define ACDG_INPUT_OFREQ_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 5)//
#define ACDG_INPUT_UFREQ_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 6)//
//ACD Board UpDn Limit Setting Info
#define ACD_INPUT_OV_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1)//From Cfg
#define ACD_INPUT_UV_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 2)//
#define ACD_PHASE_FAIL_VOLT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 3)//
#define ACD_INPUT_OFREQ_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 4)//
#define ACD_INPUT_UFREQ_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 5)//
//ACD Board System Setting Info
#define ACD_TRANSFORM_COE_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 6)//From Cfg
#define ACD_AC_INPUT_TYPE_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 7)//
#define ACD_AC_INPUT_NUM_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 8)//
#define ACD_AC_MEASURE_MODE_SIGID 		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 9)//
#define ACD_AC_OUTPUT_NUM_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 10)//
#define ACD_AC_ADDR_INFO_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 11)//

//DCD Group Up/Down Limit Setting Info
#define DCDG_TEMP1_HIGH_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 2)//
#define DCDG_TEMP2_HIGH_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 3)//
#define DCDG_TEMP3_HIGH_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 4)//

#define DCDG_LVD1_LMT_SIGID				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 19)//
#define DCDG_LVD2_LMT_SIGID				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 26)//
#define DCDG_LVD3_LMT_SIGID				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 14)//

#define DCDG_LVD1_CONTROL_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 1)//
#define DCDG_LVD2_CONTROL_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 2)//
#define DCDG_LVD3_CONTROL_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 3)//

#define DCDG_DC_OV_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 15)//
#define DCDG_DC_UV_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 16)//
//DCD Board UpDn Limit Setting Info
#define DCD_TEMP1_HIGH_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1)//
#define DCD_TEMP2_HIGH_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 2)//
#define DCD_TEMP3_HIGH_LMT_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 3)//
#define DCD_DC_OV_LMT_SIGID				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 7)//
#define DCD_DC_UV_LMT_SIGID				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 8)//

//Batt Group UpDn Limit Setting Info
#define BATTG_OVCURR_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 45)//
#define BATTG_OVVOLT_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 61)//
#define BATTG_UVVOLT_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 62)//
//Batt Unit UpDn Limit Setting Info
#define BATT_OVCURR_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 45)//
#define BATT_OVVOLT_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 2)//
#define BATT_UVVOLT_LMT_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 3)//

//DCD Board Setting Info
#define DCD_TEMP_COE_SIGID				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 9)//From Cfg
#define DCD_LOAD_SENSOR_COE_SIGID		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 10)//
#define DCD_BATT_NUM_SIGID				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 11)//
#define DCD_TEMP_NUM_SIGID 				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 12)//
#define DCD_BRANCH_CURR_COE_SIGID 		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 13)//
#define DCD_OUTPUT_NUM_WITHCURR_SIGID 	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 15)//
#define DCD_OUTPUT_NUM_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 16)//
#define DCD_DC_ADDR_INFO_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 14)//

//Battery unit
#define BATT_SENSOR_COE_SIGID			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1)//


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// the ACU unit std type id
#define STD_ID_AC_UNIT_EQUIP_SMAC		801	// the StdEquip_SMACUnit.cfg
#define STD_ID_AC_UNIT_EQUIP_SCU1		802	// the StdEquip_SCUACUnit1.cfg with AC system
#define STD_ID_AC_UNIT_EQUIP_SCU2		803	// the StdEquip_SCUACUnit2.cfg without AC system

#define SIG_ID_A_MAINS_FAIL_CNT			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 41)//Phase A Mains Failure Counter	
#define SIG_ID_B_MAINS_FAIL_CNT			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 42)//Phase B Mains Failure Counter	
#define SIG_ID_C_MAINS_FAIL_CNT			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 43)//Phase C Mains Failure Counter	
#define SIG_ID_FREQ_FAIL_CNT			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 44)//Frequency Failure Counter	

#define SIG_ID_VOL_A					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 1)//Phase Voltage A
#define SIG_ID_VOL_B					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 2)//Phase Voltage B
#define SIG_ID_VOL_C					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 3)//Phase Voltage C
#define SIG_ID_VOL_FREQ					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 10)//Frequency

#define ALARM_ID_HIGH_VOL_A				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,14)	//	14	High Phase Voltage A		
#define ALARM_ID_LOW_VOL_A				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,16)	//	16	Low Phase Voltage A		
#define ALARM_ID_HIGH_VOL_B				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,18)	//	18	High Phase Voltage B		
#define ALARM_ID_LOW_VOL_B				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,20)	//	20	Low Phase Voltage B		
#define ALARM_ID_HIGH_VOL_C				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,22)	//	22	High Phase Voltage C		
#define ALARM_ID_LOW_VOL_C				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,24)	//	24	Low Phase Voltage C		
#define ALARM_ID_HIGH_FREQ				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,28)	//	28	High Frequency	
#define ALARM_ID_LOW_FREQ				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,29)	//	29	Low Frequency		

// the std equip type id definition -- SM Battery
//#define EQUIP_TYPE_ID_SMBAT4802			311
//#define EQUIP_TYPE_ID_SMBAT4806			312
//#define EQUIP_TYPE_ID_SMBAT4812			313
//#define EQUIP_TYPE_ID_SMBAT2402			314
//#define EQUIP_TYPE_ID_SMBAT2406			315
//#define EQUIP_TYPE_ID_SMBAT2412			316*/

//System
#define SIG_ID_ACU_SYS_POWER					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 3)//System_Power
#define SIG_ID_ACU_ALL_POWER_CONSUM		        DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 4)//Total_Power_Consumption
//Multi ACUnit
//SMAC	
#define SIG_ID_A_MAINS_FAIL_CNT			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 41)//Phase A Mains Failure Counter	
#define SIG_ID_B_MAINS_FAIL_CNT			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 42)//Phase B Mains Failure Counter	
#define SIG_ID_C_MAINS_FAIL_CNT			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 43)//Phase C Mains Failure Counter	
#define SIG_ID_FREQ_FAIL_CNT			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 44)//Frequency Failure Counter	

#define SIG_ID_VOL_A					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 1)//Phase Voltage A
#define SIG_ID_VOL_B					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 2)//Phase Voltage B
#define SIG_ID_VOL_C					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 3)//Phase Voltage C
#define SIG_ID_VOL_FREQ					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 10)//Frequency

#define ALARM_ID_HIGH_VOL_A				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,14)	//	14	High Phase Voltage A		
#define ALARM_ID_LOW_VOL_A				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,16)	//	16	Low Phase Voltage A		
#define ALARM_ID_HIGH_VOL_B				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,18)	//	18	High Phase Voltage B		
#define ALARM_ID_LOW_VOL_B				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,20)	//	20	Low Phase Voltage B		
#define ALARM_ID_HIGH_VOL_C				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,22)	//	22	High Phase Voltage C		
#define ALARM_ID_LOW_VOL_C				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,24)	//	24	Low Phase Voltage C		
#define ALARM_ID_HIGH_FREQ				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,28)	//	28	High Frequency	
#define ALARM_ID_LOW_FREQ				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM ,29)	//	29	Low Frequency		


#define SIG_ID_SYS_POWER					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 52)//System_Power
#define SIG_ID_ALL_POWER_CONSUM		        DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 53)//Total_Power_Consumption
#define SIG_ID_PHASE_A_CUR					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 7)//Max Voltage
#define SIG_ID_PHASE_B_CUR					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8)//Min Voltage
#define SIG_ID_PHASE_C_CUR					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 9)//Max Voltage
#define SIG_ID_PHASE_A_POWER				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 12)//Min Voltage
#define SIG_ID_PHASE_B_POWER				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 13)//Max Voltage
#define SIG_ID_PHASE_C_POWER				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 14)//Min Voltage

// the std sampler id definition -- scu
#define SAMPLER_TYPE_ID_SCU			3
#define SAMPLER_TYPE_ID_CAN			1
#define SAMPLER_TYPE_ID_I2C			2
//#define SAMPLER_TYIPE_ID_SCU_PLUS		1  //
#define SAMPLER_TYPE_ID_485			4

// Alarm Relay No Min and Max definition
//#define	MIN_ALARM_RELE_RELAY_NO			1
//#define	MAX_ALARM_RELE_RELAY_NO			8
//
//#define SIG_ID_BATT_COMM_STATUS			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 10) // Communication Interrupt count
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
*
*						Part 5    Fuel Management
*   		standard signal ID defined in StdEquip_FuelManagement.cfg
*
*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
#define STD_ID_FUEL_MANAGEMENT_GROUP_EQUIP 2100
#define STD_ID_FUEL_MANAGEMENT_EQUIP 2101	// the Fuel Management equipment standard type
#define STD_ID_OB_FUEL_MANAGEMENT_EQUIP 2102

#define FUEL_MGMT1_EQUIP_ID 701
#define FUEL_MGMT2_EQUIP_ID 702
#define FUEL_MGMT3_EQUIP_ID 703
#define FUEL_MGMT4_EQUIP_ID 704
#define FUEL_MGMT5_EQUIP_ID 705
#define FUEL_MGMT6_EQUIP_ID 1500
#define FUEL_MGMT7_EQUIP_ID 1501

#define MAX_NUM_FUEL_TANK	7
#define NUM_SIG_FUEL_MGMT	5

#define MAX_FULE_VALUE		99999

#define MAX_PARAM_NUM			3
#define SQUARE_PARAM_NUM		3
#define STANDING_CYLINDER_PARAM_NUM	2
#define LAYING_CYLINDER_PARAM_NUM	2

#define MIN_CALIB_POINTS_NUM    10
#define MAX_CALIB_POINTS_NUM    20

#define TIME_INTERVAL		30
#define MAX_TIME_INTERVAL	180
#define MILLILITER_PER_LITER	1000000
#define SECOND_PER_MINUTE	60
#define ERROR_FUEL_VALUE	-9999

#define SIG_ID_FUELMANG_FUEL_NUM			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1)
#define SIG_ID_FUELMANG_OB_FUEL_NUM			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 2)

#define SIG_ID_FUELMANAGEMENT_REMAINED_FUEL_HEIGHT	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,1)
#define SIG_ID_FUELMANAGEMENT_REMAINED_FUEL_VOLUME	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,2)
#define SIG_ID_FUELMANAGEMENT_REMAINED_FUEL_PERCENT	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,3)
#define SIG_ID_FUELMANAGEMENT_FUEL_THEFT_STATUS		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,4)
#define SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,5)
#define SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,6)

#define SIG_ID_FUELMANAGEMENT_RESET_THEFT_ALARM		DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL,1)
#define SIG_ID_FUELMANAGEMENT_FINISH_CONFIG		DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL,2)

#define SIG_ID_FUELMANAGEMENT_FULE_TANK_TYPE			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,1)
#define SIG_ID_FUELMANAGEMENT_SQUARE_TANK_LENGTH		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,2)
#define SIG_ID_FUELMANAGEMENT_SQUARE_TANK_WIDTH			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,3)
#define SIG_ID_FUELMANAGEMENT_SQUARE_TANK_HEIGHT		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,4)
#define SIG_ID_FUELMANAGEMENT_STANDING_CYLINDER_DIAMETER	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,5)
#define SIG_ID_FUELMANAGEMENT_STANDING_CYLINDER_HEIGHT		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,6)
#define SIG_ID_FUELMANAGEMENT_LAYING_CYLINDER_DIAMETER		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,7)
#define SIG_ID_FUELMANAGEMENT_LAYING_CYLINDER_LENGTH		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,8)

#define SIG_ID_FUELMANAGEMENT_CALIBRATION_POINTS_NUM	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,10)		//Calibration point number
#define MAX_CALIB_POINT_NUM				20
#define MIN_CALIB_POINT_NUM				10
#define SIG_ID_FUELMANAGEMENT_CALIBRATION_POINTS_HEIGHT_START	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,11)	//Calibration height start point
#define SIG_ID_FUELMANAGEMENT_CALIBRATION_POINTS_VOLUME_START	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,12)	//Calibration volume start point

#define SIG_ID_FUELMANAGEMENT_LOW_LEVER_LIMIT		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,56)
#define SIG_ID_FUELMANAGEMENT_HIGH_LEVER_LIMIT		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,57)
#define SIG_ID_FUELMANAGEMENT_MAX_CONSUMPTION_SPEED	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,58)

#define SIG_ID_SYS_DCLCNUM	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,94)
#define SIG_ID_SYS_BATLCNUM	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,95)
#define SIG_ID_SYS_LCWORKINGSTATE	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,1)

#define SIG_ID_LVD3_ENABLE		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,64)
#define SIG_ID_LVD3_RELAY_NO		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,65)
#define SIG_ID_LVD3_CONTROL_ID		DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL,1)
#define SIG_ID_LVD3_SAMPLE_ID		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,1)
#define LVD3_SENDER_NAME		"LVD3"
#define LVD_GROUP_EQUIP_ID		186
#define LVD3_EQUIP_ID			3510

#endif


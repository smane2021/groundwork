/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : public.h
 *  CREATOR  : ACU Team                 DATE: 2004-09-15 10:02
 *  VERSION  : V1.00
 *  PURPOSE  : All head files defined by us come here!
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __PUBLIC_H__
#define __PUBLIC_H__

/* public module function */
//Timer thread etc.

#include "basetypes.h"
#include "app_conf.h"	// the global environment about app configuration
#include "app_service.h"
#include "pubfunc.h"
#include "halcomm.h"
#include "new.h"
#include "err_code.h"
#include "cfg_model.h"
#include "data_mgmt.h"
#include "run_thread.h"
#include "run_queue.h"
#include "applog.h"
#include "stack.h"

#include "run_mutex.h"

#include "cfg_parser.h"
#include "cfg_reader.h"

#include "data_exchange.h"
#include "user_manage.h"  

#include "run_timer.h"
#include "batt_test_log.h"

//add by Yang Guoxin
//#include "../service/gen_ctl/gc_run_info.h"
//#include "../service/gen_ctl/gc_dsl_test.h"
//end by Yang Guoxin

#include "lcd_interface.h"
#include "lcd_font_lib.h"
#include "version_manage.h"  //add by lixidong

#include <linux/rtc.h>		//add by YangGuoxin
//#ifndef _SUPPORT_THREE_LANGUAGE
//
//    #define	_SUPPORT_THREE_LANGUAGE
//
//#endif
#endif /*__PUBLIC_H__*/

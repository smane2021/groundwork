/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *   PRODUCT  : NGMC-HF
 *
 *  FILENAME : rts.h
 *  CREATOR  : Tony(Han Tao)                DATE: 2007-11-07 14:03
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *==========================================================================*/

#ifndef __AT91SAM9263_RTS_H
#define __AT91SAM9263_RTS_H

//#define _DEBUG

#ifdef	_DEBUG
#define MSG(string, args...) printk(string, ##args)
#else
#define MSG(string, args...)
#endif

#ifndef LOWORD
#define LOWORD(l)           ((u_short)((u_long)(l) & 0xffff))
#endif //LOWORD

#ifndef HIWORD
#define HIWORD(l)           ((u_short)((u_long)(l) >> 16))
#endif //HIWORD


#define RTS_NAME			"rts"
#define RTS_MAJOR			248  

#define IOC_SET_SEND  	              _IOW(RTS_MAJOR, 1, unsigned long)
#define IOC_SET_RECEIVE   	       _IOW(RTS_MAJOR, 2, unsigned long)

int at91_rts_open(struct inode *inode, struct file *filp); 
int at91_rts_release(struct inode *inode, struct file *filp); 
int at91_rts_ioctl (struct inode *inode, struct file *file, unsigned int uiCmd, unsigned long arg); 
loff_t at91_rts_llseek(struct file *file,loff_t offset, int origin);
static void at91_rts_timer_handler( unsigned long data);

static int rts_hw_init(void);


#endif /* __AT91SAM9263_RTS_H */



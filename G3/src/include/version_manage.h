/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : XXX(XXX Controller Unit)
 *
 *  FILENAME : version_manage.h
 *  CREATOR  : LIXIDONG                 DATE: 2006-06-15 13:39
 *  VERSION  : V1.00
 *  PURPOSE  : This file is used to identify different software application.
 *			   For example,SCU Plus, ACU,DPU and so on.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef _SOFTWARE_VERSION_MANAGE_
#define _SOFTWARE_VERSION_MANAGE_

#define		_SCU_PLUS_APP_VER		1
//#define		_ACU_APP_VER			1
//#define		_DPU_APP_VER			1
//#define		_SUPPORT_THREE_LANGUAGE		1
#define		_SCU_PLUS_CHINA_VER		1

#endif

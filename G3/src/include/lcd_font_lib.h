/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_font_lib.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:55
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_FONT_LIB_H__041008
#define __LCD_FONT_LIB_H__041008

#ifdef  __cplusplus
extern "C" {
#endif

	//LCD_UI_TASK used to record log
#ifndef LCD_UI_TASK
#define LCD_UI_TASK "LCD_UI"
#endif //LCD_UI_TASK

	//Only support the font which height is multiple of BASE_HEIGHT
	//For example English:maybe 8 or 16, Chinese maybe 16 or 24
#define BASE_HEIGHT     8

	enum UI_BASE_FONT_CODE_ENUM
	{
		UI_BASE_FONT_CODE_ASCII = 0,
		UI_BASE_FONT_CODE_UTF8,
		UI_BASE_FONT_CODE_UTF16,
		UI_BASE_FONT_CODE_GBK,//GB2312
		UI_BASE_FONT_CODE_BIG5,
		UI_BASE_FONT_CODE_KOI8R,//Russian
		UI_BASE_FONT_CODE_LATIN1,//
		UI_BASE_FONT_CODE_LATIN5,//Turkish
		UI_BASE_FONT_CODE_LATIN9,//English, Spanish, German, Portuguese, Swedish, French, Italian
		UI_BASE_FONT_CODE_LATIN10,//
		UI_BASE_FONT_CODE_MAX,
	};

	//The struct of font size, the unit is pixel
	//For example Chinese:16(W),16(H),32(size = Width * Height / BASE_HEIGHT)
	struct tagFontSize
	{
		BYTE byFontWidth;
		BYTE byFontHEIGHT;
		BYTE byFontSize;

		//Frank Wu,20160127, for MiniNCU
		BYTE byFontCharCode;//UI_BASE_FONT_CODE_ENUM
	};
	typedef struct tagFontSize FONT_SIZE;

	//Function pointer for font operation
	typedef int (*INIT_FONT_LIB)(void);

	typedef int (*GET_FONT_SIZE)(FONT_SIZE* pFontSize);

	typedef int (*GET_FONT_MATRIX)(UCHAR *pText, UCHAR** ppRetMatrix, FONT_SIZE* pFontSize);

	typedef int (*CLOSE_FONT_LIB)(void);

	typedef int (*GET_FONT_HEIGHT)(void);

	//The struct includes the operation functions for a kind of font
	struct tagFontOperation
	{
		char* pLangCode;

		INIT_FONT_LIB	pfnInitFontLib;
		GET_FONT_SIZE	pfnGetFontSize;
		GET_FONT_MATRIX	pfnGetFontMatrix;
		CLOSE_FONT_LIB	pfnCloseFontLib;
		GET_FONT_HEIGHT	pfnGetFontHeight;
	};
	typedef struct tagFontOperation FONT_OPERATION;


	/*==========================================================================*
	* FUNCTION : InitFontLib
	* PURPOSE  : Init the supported languages
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: int     nLangNum   : languages num need to be initted
	*            char**  ppLangCode : languages codes, include nLangNum languages
	* RETURN   : int : The success of failure flag for initted result,
	*			  ERR_LCD_OK is success.
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 11:50
	*==========================================================================*/
	int InitFontLib(int nLangNum, char** ppLangCode);


	/*==========================================================================*
	* FUNCTION : GetFontSize
	* PURPOSE  : Get the font size of a language due to its language code
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: FONT_SIZE*  pFontSize : 
	*            char*       pLangCode : 
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 11:54
	*==========================================================================*/
	int GetFontSize(FONT_SIZE* pFontSize, char* pLangCode);

	/*==========================================================================*
	* FUNCTION : GetFontMatrix
	* PURPOSE  : Get a full unit font matrix of text
	*			  ��ȡһ�������ĵ�Ԫ�������������Ϊ1���ַ������ں�����Ϊ2���ַ�
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: UCHAR       *pText      : The input text buffer to be displayed
	*            UCHAR**     ppRetMatrix : Return font matrix of the input text
	*            FONT_SIZE*  pFontSize   : The font size of current font
	*			  char*		  pLangCode	  : 
	* RETURN   : static int : The char number which has been used
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 11:28
	*==========================================================================*/
	int GetFontMatrix(UCHAR *pText, 
		UCHAR** ppRetMatrix, 
		FONT_SIZE* pFontSize, 
		char* pLangCode);

	/*==========================================================================*
	* FUNCTION : CloseFontLib
	* PURPOSE  : Closed the initted languages
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: int     nLangNum   : languages num need to be Closed
	*            char**  ppLangCode : languages codes, include nLangNum languages
	* RETURN   : int : The success of failure flag for closed result,
	*			  ERR_LCD_OK is success.
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 11:50
	*==========================================================================*/
	int CloseFontLib(int nLangNum, char** ppLangCode);

	//Get font height due to language code
	int GetFontHeight(char* pLangCode);



#ifdef __cplusplus
}
#endif

#endif //__LCD_FONT_LIB_H__041008

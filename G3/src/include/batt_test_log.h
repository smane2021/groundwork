/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : batt_test_log.h
 *  CREATOR  : Frank Cao                DATE: 2004-12-28 15:44
 *  VERSION  : V1.00
 *  PURPOSE  : Battery test log 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __BATT_TEST_LOG_H__
#define __BATT_TEST_LOG_H__

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

//#define MAX_NUM_BATT_EQUIP			46 //�ظ����壬ȥ��By Jimmy Wu 20110714

enum	BATTERY_TEST_START_REASON
{
	START_PLANNED_TEST = 0,		//Start test for it is battery best plan time
	START_MANUAL_TEST,			//Start test manually 
	START_AC_FAIL_TEST,			//Start AC Fail test for AC fail
	START_MASTER_TEST,			//Start test for Master Power start test

	START_OTHER
};

enum	BATTERY_TEST_END_REASON
{
	END_MANUAL = 0,				//Stop test manually 
	END_ALARM,					//Stop test for alarm
	END_TIME,					//Stop test for test time-out
	END_CAPACITY,				//Stop test for capacity condition
	END_VOLTAGE,				//Stop test for voltage condition
	END_AC_FAIL,				//Stop test for AC fail
	END_AC_RESTORE,				//Stop AC fail test for AC restore
	END_AC_FAIL_TEST_DIS,		//Stop AC fail test for disabled
	END_MASTER_STOP,			//Stop test for Master Power stop test
	END_TURN_TO_MAN,			//Stop a PowerSplit BT for Auto/Man turn to Man
	END_TURN_TO_AUTO,			//Stop PowerSplit Man-BT for Auto/Man turn to Auto
	END_LOW_LOAD,

	END_OTHER
};

enum	BATTERY_TEST_RESULT
{
	RESULT_NONE = 0,			//No test result
	RESULT_GOOD,				//Battery is OK
	RESULT_BAD,					//Battery is bad
	RESULT_POWER_SPLIT,			//It's a Power Split Test
	RESULT_OTHER
};

/*Battery Test Log*/
////////////////////////////////////////////
//struct	tagBT_LOG_SUMMARY		
//{
//	time_t		tStartTime;					//Start Time
//	time_t		tEndTime;					//End Time
//	int			iStartReason;				//Start Reason
//	int			iEndReason;					//End Reason
//	int			iTestResult;				//Test Result
//
//	float		fBattRatedCap;				//Battery Rated Capacity
//	float		fStartVolt;					//System Voltage at start of test
//	float		fEndVolt;					//System Voltage at end of test
//	int			iBattNum;					//Quantity of Batteries
//    float		fTotalDisCap;				//Total discharge capacity
//#ifdef	GEN_CTL_FOR_CHN
//	float		afDisCap[MAX_NUM_BATT_EQUIP];	//All batteries' discharge capacity
//#ifdef	GEN_CTL_FOR_CHN
//
//};

struct	tagBT_LOG_SUMMARY		
{
	time_t		tStartTime;			//Start Time
	time_t		tEndTime;			//End Time
	int			iStartReason;		//Start Reason
	int			iEndReason;			//End Reason
	int			iTestResult;		//Test Result

	float		fBattRatedCap;		//Battery Rated Capacity
	int			iQtyBatt;			//Quantity of Battery String
	int			iQtyRecord;			//Quantity of record
	int			iSizeRecord;		//Size of per record
};

typedef struct tagBT_LOG_SUMMARY BT_LOG_SUMMARY;

struct	tagBT_LOG		
{
	BT_LOG_SUMMARY	Summary;		//Summary
	int				iRecordNo;		//Current Record No.
	float			fLastVolt;		//Last Voltage

	void*			pTestRecord;	//Record Data

	//following is prepared for parameter of DAT_StorageCreate
	int				iMaxRecords;	//maximum records number in storage
	float			fWritefrequencyPerday;
};
typedef struct tagBT_LOG TEST_LOG;

/*Battery Test Log Info*/
#define	SIZE_STORAGE_UNIT		256//128
#define	MAX_RECORD_PER_TEST		80
#define	MAX_TEST_LOG			10

/* used by services to pick up interested info */
#define BATT_CFGPROT_CUR		0x1
#define BATT_CFGPROT_VOL		0x2
#define BATT_CFGPROT_CAP		0x4
#define BATT_CFGPROT_TEMP		0x8

#define BATT_CFGRPOP_SET(_pBattCfg, _flag)  ((_pBattCfg)->iPropFlag |= (_flag))

#define BATT_CFGRPOP_TEST(_pBattCfg, _flag)  \
	(((_pBattCfg)->iPropFlag & (_flag)) == 0 ? FALSE : TRUE)

struct	tagBTLogBattCfg		
{
	int iPropFlag;

	int iOffsetCur;
	int iOffsetVol;
	int iOffsetCap;
	int iOffsetTemp;

	int iQtyBlockVol;
	int iOffsetBlockVol;

	int iOffsetTotal;
	int iTempNumber;
};
typedef struct tagBTLogBattCfg   BT_LOG_BATT_CFG;

void	GC_TestLogHandling(void);
void	GC_TestLogDestroy(void);
void	GC_TestLogStart(int iStartReason);
void	GC_TestLogEnd(int iEndReason, int iTestRrsult);
void	BattTestLogInfoInit(void);

#endif	/*__BATT_TEST_LOG_H__*/


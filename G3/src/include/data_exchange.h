/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : data_exchange.h
*  CREATOR  : HULONGWEN                DATE: 2004-09-14 12:49
*  VERSION  : V1.00
*  PURPOSE  : The declaration file of data exchange module
*
*
*  HISTORY  :
*
*==========================================================================*/

#ifndef __DATA_EXCHANGE_H__040914
#define __DATA_EXCHANGE_H__040914

//The site(ACU) info,include all equips data, signals data and configs
extern SITE_INFO  g_SiteInfo;
#define GetSiteInfo() (&g_SiteInfo)

//The list of varType

//Get the number of equipments			
#define VAR_ACU_EQUIPS_NUM				0		

//Get the list of equipments
#define VAR_ACU_EQUIPS_LIST				1		

//Get the sample signals number of a equipment
#define VAR_SAM_SIG_NUM_OF_EQUIP		2	

//Get the sample signals structure list  of a equipment
#define VAR_SAM_SIG_STRU_OF_EQUIP		3		

//Get the sample signals value list  of a equipment
#define VAR_SAM_SIG_VALUE_OF_EQUIP		4		

//Get the set signals number of a equipment
#define VAR_SET_SIG_NUM_OF_EQUIP		5		

//Get the set signals structure list  of a equipment
#define VAR_SET_SIG_STRU_OF_EQUIP		6	

//Get the set signals value list  of a equipment
#define VAR_SET_SIG_VALUE_OF_EQUIP		7		

//Get the control signals number of a equipment
#define VAR_CON_SIG_NUM_OF_EQUIP		8		

//Get the control signals structure list  of a equipment
#define VAR_CON_SIG_STRU_OF_EQUIP		9	

//Get the control signals value list  of a equipment
#define VAR_CON_SIG_VALUE_OF_EQUIP		10		

//Get the alarm signals number of a equipment
#define VAR_ALARM_SIG_NUM_OF_EQUIP		11

//Get the alarm signals structure list  of a equipment
#define VAR_ALARM_SIG_STRU_OF_EQUIP		12	

//Get the active alarms number
#define VAR_ACTIVE_ALARM_NUM			13		

//Get the active alarms info
#define VAR_ACTIVE_ALARM_INFO			14	

//Get multiple signals info
#define VAR_MULTIPLE_SIGNALS_INFO		15	

//Get or set a equipment info of ACU
#define VAR_A_EQUIP_INFO				16	

//Get or set a signal structure
#define VAR_A_SIGNAL_INFO_STRU			17		

//Get or set a signal value
#define VAR_A_SIGNAL_VALUE				18		

//Get or set the public configuration of ACU
#define VAR_ACU_PUBLIC_CONFIG			19		

//Get or set the network info of ACU
#define VAR_ACU_NET_INFO				20		

//Get or set the time server info
#define VAR_TIME_SERVER_INFO			21	

//Get the number of samples			
#define VAR_SAMPLERS_NUM				22		

//Get the list of samples
#define VAR_SAMPLERS_LIST				23	

//Get the number of standard equipments			
#define VAR_STD_EQUIPS_NUM				24		

//Get the list of standard equipments
#define VAR_STD_EQUIPS_LIST				25	

//Get or set a signal structure var standard equip ID
#define VAR_A_SIGNAL_INFO_THRO_STD_EQUIP	26		

#define VAR_A_STD_EQUIP_INFO			27

//Added by wj for AlarmSupExp Config
#define VAR_STD_EQUIPTYPE_MAP_INFO          28

#define VAR_STD_EQUIPTYPE_MAP_NUM           29

//Signal Validity
#define VAR_A_SIGNAL_VALIDITY				30

#define VAR_STD_EQUIP_ALARM_RELAY           31//add by wj for AlarmRelay


//Set LCD Key through YDN23
#define VAR_LCDKEY_THRUYDN23			32

//Get User define pages
#define VAR_USER_DEF_PAGES			33

//Get DHCP Client Info
#define VAR_APP_DHCP_INFO			34

//Get DHCP Server Info
#define VAR_DHCP_SERVER_INFO			35

//Get the point  of g_SiteInfo.LoadCurrentData[] 
#define VAR_HIS_DATA_RECORD_LOAD		36

//Get VPN Info
#define VAR_VPN_INFO				37

//Get SMS PHONE Info
#define VAR_SMS_PHONE_INFO			38

//Get NET IPV6 Info
#define VAR_NET_IPV6_INFO			39

//Get SMTP Info
#define VAR_SMTP_INFO				40

//changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller��s IP address
//Get front port IP Info
#define VAR_NET_FRONT_IP_INFO		41



#define VAR_INTERFACE_TYPE_START		VAR_ACU_EQUIPS_NUM
#define VAR_INTERFACE_TYPE_END			VAR_NET_FRONT_IP_INFO
//end -- changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller��s IP address

#define LOBYTEXX(w)           ((WORD)((DWORD_PTR)(w) & 0xfff))
#define HIBYTEXX(w)           ((BYTE)((DWORD_PTR)(w) >> 12))


#define DXI_MERGE_UNIQUE_SIG_ID(EquipID, SignalType, SignalID) \
	(MAKELONG(MAKEWORDX((SignalID),(SignalType)),(EquipID)))

#define DXI_MERGE_SIG_ID(SignalType, SignalID) \
	(MAKELONG((SignalID),(SignalType)))


#define DXI_SPLIT_UNIQUE_SIG_ID(UniqueSigID, EquipID, SignalType, SignalID) \
	((EquipID) = HIWORD((UniqueSigID)), \
	(SignalType) = HIBYTEXX(LOWORD((UniqueSigID))), \
	(SignalID) = LOBYTEXX(LOWORD((UniqueSigID))))


#define DXI_SPLIT_SIG_ID(MergeSigID, SignalType, SignalID) \
	((SignalType) = HIWORD((MergeSigID)), \
	(SignalID) = LOWORD((MergeSigID)))

//MID:Merged ID.
#define DXI_SPLIT_SIG_MID_TYPE(MergeSigID)	(HIWORD((MergeSigID)))
#define DXI_SPLIT_SIG_MID_ID(MergeSigID)	(LOWORD((MergeSigID)))


//The maximum length of manufacturer name
#define MAX_MANUFACTURER_NAME		64

//The maximum callback numbers
#define CALLBACK_REGISTER_NUMBER	32

#define _REGISTER_FLAG              1		//Register callback function
#define _CANCEL_FLAG				0		//Logout callback function

#define _SAMPLE_MASK				0X01	//Sample signal callback
#define _SET_MASK					0x02	//Set signal callback
#define _CONTROL_MASK				0x04	//Control signal callback

//replaced with _ALARM_OCCUR_MASK and _ALARM_CLEAR_MASK
#define _WEB_SET_MASK					0x08	//Web/LCD set signal callback
 
#define _CONFIG_MASK				0x10	//Config change callback

#define _ALARM_OCCUR_MASK			0x20	//Alarm signal callback
#define _ALARM_CLEAR_MASK			0x40	//Alarm signal callback
#define _LOAD_CURRENT_MASK			0x80	//Record load current point callback

#define _CLEAR_HISTORY_MASK			0x100	//Record load current point callback

//VAR_VALUE_EX.nSendDirectly Definition 
#define EQUIP_CTRL_SEND_CHECK_VALUE		0	// if current sig value is equal to the new value,
											// the ctrl will NOT be sent.
#define EQUIP_CTRL_SEND_DIRECTLY		1	// send out the ctrl regardless the ctrl value.
#define EQUIP_CTRL_SEND_URGENTLY		2	// send out the ctrl regardless the ctrl value urgently,
											// the cmd will be put to the head of the cmd queue.
//For DHCP Info
#define APP_DHCP_OFF				0
#define APP_DHCP_ON				1
#define APP_DCHP_ERR				2

//For DHCP SERVER Info : Enable
#define APP_DHCP_SERVER_OFF			0
#define APP_DHCP_SERVER_ON			1
#define APP_DCHP_SERVER_ERR			2

//For VPN Info - ENABLE
#define APP_VPN_OFF				0
#define APP_VPN_ON				1
#define APP_VPN_ERR				2

//For SMS Info - return
#define SMS_INFOR_OK				0
#define SMS_INFOR_ERR				1

//For Ethernet Number Info
#define ETHERNET_NUMBER_1			0     //ONE ethernet
#define ETHERNET_NUMBER_2			1     //TWO ehternet 


struct tagVarValueEx
{
	VAR_VALUE	varValue;

	//if the current value is equal to the value which will be sent, the call will return OK, 
	//and the value will NOT be sent to equipment actually.
	int		nSendDirectly;   //Look up EQUIP_CTRL_SEND_CHECK_VALUE

	//the type of sender
	int			nSenderType;  //Refer to APP_SERVICE_TYPE_ENUM definition in app_service.h
	//the sender name in str
	char		*pszSenderName;  //for example "GC_CTL"

};
typedef struct tagVarValueEx VAR_VALUE_EX;


//Enum for GetMultipleSignalsInfo
enum MUL_SIGNALS_GET_TYPE
{
	MUL_SIGNALS_GET_VALUE = 0,
	MUL_SIGNALS_GET_STRU	
};

#define MAX_GET_SIGNALS_NUM 512
struct tagGetMultipalSignals
{
	int  nGetNum;
	long lData[MAX_GET_SIGNALS_NUM];
	//long lData[1];
};
typedef struct tagGetMultipalSignals GET_MULTIPAL_SIGNALS;

/* now only set multi-signals of a certain equip */
struct tagSetMultipalSignals
{
	int  nSetNum;
	long lIDs[MAX_GET_SIGNALS_NUM];       //sig type, sig id info
	VAR_VALUE_EX vData[MAX_GET_SIGNALS_NUM];
};
typedef struct tagSetMultipalSignals SET_MULTIPAL_SIGNALS;


//Enum for SetASignalInfoStru
enum MODIFY_SIGNAL_INFO_TYPE
{
	MODIFY_SIGNAL_ENGLISH_FULL_NAME	= ENGLISH_FULL,
	MODIFY_SIGNAL_LOCAL_FULL_NAME	= LOCALE_FULL,
	MODIFY_SIGNAL_ENGLISH_ABBR_NAME	= ENGLISH_ABBR,
	MODIFY_SIGNAL_LOCAL_ABBR_NAME	= LOCALE_ABBR,
    MODIFY_SIGNAL_LOCAL2_FULL_NAME	= LOCALE2_FULL,//Add by wj for three languages 2006.5.9
    MODIFY_SIGNAL_LOCAL2_ABBR_NAME	= LOCALE2_ABBR,//Add by wj for three languages 2006.5.9



	MODIFY_ALARM_LEVEL,
	MODIFY_ALARM_RELAY  //added by ht,2006.11.02
	//others will add etc
};

#define MAX_LENGTH_OF_SIGNAL_NAME 64
//Structure for SetASignalInfoStru
struct tagSetASignalInfo
{
	BYTE byModifyType;

	char szModifyBuf[MAX_LENGTH_OF_SIGNAL_NAME];
	int	 bModifyAlarmLevel;
	int  bModifyAlarmRelay;  //added by ht,2006.11.02

	char cModifyUser[40];    //added by ht,2006.12.14, just for systerm log.

};
typedef struct tagSetASignalInfo SET_A_SIGNAL_INFO_STU;

//Enum for SetASignalInfoStru
enum SetAEquipInfo
{
	MODIFY_EQUIP_ENGLISH_FULL_NAME	= ENGLISH_FULL,
	MODIFY_EQUIP_LOCAL_FULL_NAME	= LOCALE_FULL,
	MODIFY_EQUIP_ENGLISH_ABBR_NAME	= ENGLISH_ABBR,
	MODIFY_EQUIP_LOCAL_ABBR_NAME	= LOCALE_ABBR,
    MODIFY_EQUIP_LOCAL2_FULL_NAME	= LOCALE2_FULL,
    MODIFY_EQUIP_LOCAL2_ABBR_NAME	= LOCALE2_ABBR,
	//others will add etc
};


//Enum for GetACUPublicConfig() and GetACUNetInfo()
enum ACU_PUBLIC_CONFIG_ID
{
	SITE_INFO_POINTER = 0,
	SITE_NAME,
	SITE_SW_VERSION,
	SITE_PORT_INFO,
	ACU_MANUFACTURER,
	SITE_ID,
	LOCAL_LANGUAGE_CODE,
    SITE_LOCATION,
	SITE_DESCRIPTION,
	ACU_SERIAL_ID,

	ACU_PRODUCT_INFO_GET,
    LOCAL2_LANGUAGE_CODE,
	APP_DHCP_INFO,
};


#define INDEX_ETH0				0
#define INDEX_ETH1				1

//Enum for GetACUNetInfo() and SetACUNetInfo()
enum ACU_NET_INFO_ID
{
	NET_INFO_ALL = 0,
	NET_INFO_IP,
	NET_INFO_NETMASK,
	NET_INFO_DEFAULT_GATEWAY,
	NET_INFO_BROADCAST,
};

//Enum for GetDHCPServerInfo() and SetDHCPServerInfo()
enum DHCP_SERVER_INFO_ID
{
    DHCP_SERVER_INFO_ALL = 0,
    DHCP_SERVER_INFO_ENABLE,
    DHCP_SERVER_INFO_IP,
};

//Enum for GetVPNInfo() and SetVPNInfo()
enum VPN_INFO_ID
{
    VPN_INFO_ALL = 0,
    VPN_INFO_ENABLE,
    VPN_INFO_REMOTE_IP,
    VPN_INFO_REMOTE_PORT,
};

//Enum for GetSMSPhoneInfo() and SetSMSPhoneInfo()
enum SMSPHONE_INFO_ID
{
    SMS_INFO_PHONE_ALL = 0,    
    SMS_INFO_PHONE1,
    SMS_INFO_PHONE2,
    SMS_INFO_PHONE3,
    SMS_INFO_ALARM_LEVEL,
};

//Enum for GetTimeSrv() and SetTimeSrv()
enum TIME_SRV_ID
{
	TIME_SRV_CONFIG = 0,
	TIME_SRV_IP,
	TIME_SRV_BACKUP_IP,
	TIME_SYNC_INTERVAL,
	TIME_SYNC_TIMEZONE,
	SYSTEM_TIME,
	//Added to run NTP funcion immediately after setting by SongXu20180202.
	TIME_SYNC_RUN_IMMEDIATELY,
};

//Enum for GetSmtpInfo() and SetSmtpInfo()
enum SMTP_INFO_ID
{

    SMTP_INFO_ALL = 0,
    SMTP_INFO_SENDTO,
    SMTP_INFO_FROM,
    SMTP_INFO_SERVERIP,
    SMTP_INFO_SERVERPORT,
    SMTP_INFO_ACCOUNT,
    SMTP_INFO_PASSWORD,
    SMTP_INFO_AUTHEN,
    SMTP_INFO_ALARM_LEVEL,
};


//Enum for SetACUPublicConfig(), when nVarID == SITE_NAME,
//nVarSubID will be put to the following value
enum ACU_PUBLIC_CONFIG_SUB_ID
{
	MODIFY_SITE_ENGLISH_FULL_NAME	= ENGLISH_FULL,
	MODIFY_SITE_LOCAL_FULL_NAME		= LOCALE_FULL,
	MODIFY_SITE_ENGLISH_ABBR_NAME	= ENGLISH_ABBR,
	MODIFY_SITE_LOCAL_ABBR_NAME		= LOCALE_ABBR,

	MODIFY_LOCATION_ENGLISH_FULL	= ENGLISH_FULL,
	MODIFY_LOCATION_LOCAL_FULL		= LOCALE_FULL,
	MODIFY_LOCATION_ENGLISH_ABBR	= ENGLISH_ABBR,
	MODIFY_LOCATION_LOCAL_ABBR		= LOCALE_ABBR,

	MODIFY_DESCRIPTION_ENGLISH_FULL = ENGLISH_FULL,
	MODIFY_DESCRIPTION_LOCAL_FULL	= LOCALE_FULL,
	MODIFY_DESCRIPTION_ENGLISH_ABBR	= ENGLISH_ABBR,
	MODIFY_DESCRIPTION_LOCAL_ABBR	= LOCALE_ABBR,

    //////////////////////////////////////////////////////////////////////////
    //Added by wj for three languages 2006.5.9

    MODIFY_SITE_LOCAL2_FULL_NAME	= LOCALE2_FULL,
    MODIFY_SITE_LOCAL2_ABBR_NAME	= LOCALE2_ABBR,
    MODIFY_LOCATION_LOCAL2_FULL		= LOCALE2_FULL,
    MODIFY_LOCATION_LOCAL2_ABBR		= LOCALE2_ABBR,
    MODIFY_DESCRIPTION_LOCAL2_FULL	= LOCALE2_FULL,
    MODIFY_DESCRIPTION_LOCAL2_ABBR	= LOCALE2_ABBR,

    //end////////////////////////////////////////////////////////////////////////
    
	//others will add etc
};

//Time server info structure
struct TimeSrvInfo	
{				
	unsigned long	ulMainTmSrvAddr;	//Main server ip
	unsigned long	ulBakTmSrvAddr;		//backup server ip
	unsigned long	ulSrvPort;			//port of server ip

	DWORD			dwJustTimeInterval;	//adjust interval from server or RTC
	LONG			lTimeZone;	//Time Zone

};				
typedef struct TimeSrvInfo	TIME_SRV_INFO;



typedef int (*DATA_EXCHANGE_INTERFACE_PROC)(int   nVarID,		//Data ID
											int   nVarSubID,	//Data sub ID
											int*  pBufLen,		//Data length
											void* pDataBuf,		//Data buffer
											int   nTimeOut		//Time out
											);

struct tagDataExchangeInterface
{
	BYTE						 byInterfaceID;		//Interface ID
	DATA_EXCHANGE_INTERFACE_PROC fGet;				//Get interface
	DATA_EXCHANGE_INTERFACE_PROC fSet;				//Set interface
};
typedef struct tagDataExchangeInterface DATA_EXCHANGE_INTERFACE;


typedef int (*SERVICE_NOTIFY_PROC)(HANDLE hService,			//Service handle
								   int	  nMsgType,			//Message type
								   int	  nTrapDataLength,	//Message length
								   void*  lpTrapData,		//Message buffer
								   void*  lpParam,			//Parameter
								   BOOL   bUrgent			//Urgent level
								   );

struct tagCallbackRegister
{
#ifdef _DEBUG
	char				szName[32];	//Display string for debug
#endif 	/*_DEBUG	*/

	SERVICE_NOTIFY_PROC proc;		//Trap call back proc been registered
	void*				lpParam;	//The parameter for trap call back proc
	int					nMsgType;	//Message type
	HANDLE				hService;	//Service handle of call back proc

	BOOL				bAlarmBlockingAllowed;// TRUE: do NOT notify me if alarm out-going blocked. 
};		
typedef struct tagCallbackRegister CALL_BACK_REGISTER;

struct tagAcuNetInfo
{
	ULONG ulIp;
	ULONG ulMask;
	ULONG ulGateway;
	ULONG ulBroardcast;
};
typedef struct tagAcuNetInfo ACU_NET_INFO;

struct tagDHCPServerInfo
{
    int	  iDHCPServerEnable;
    ULONG ulDHCPServerIp;
};
typedef struct tagDHCPServerInfo DHCP_SERVER_INFO;

struct tagVPNInfo
{
    int	  iVPNEnable;  //0-off 1-on
    ULONG ulRemoteIp;
    int	  iRemotePort;
};
typedef struct tagVPNInfo ACU_VPN_INFO;

struct tagDHCPServerNetInfo
{
    int iDHCPServerEnable;  //0-off  1-on
    ULONG ulIp;
};
typedef struct tagDHCPServerNetInfo DHCP_SERVER_NET_INFO;

struct tagDHCPServerNetInfoV6
{
	int iDHCPServerEnable;  //0-off  1-on
	IN6_IFREQ stServerAddr;
};
typedef struct tagDHCPServerNetInfoV6 DHCP_SERVER_NET_INFO_IPV6;

struct tagSMTPInfo
{
    char szEmailSendTo[128];    
    char szEmailFrom[128];
    char szEmailSeverIP[32];
    int iEmailServerPort;
    char szEmailAccount[64];
    char szEmailPasswd[64];
    int iEmailAuthen; /* 0 --> SMTP����������Ҫ��֤�� 1 --> SMTP��������Ҫ��֤�û������� */
    int	 iAlarmLevel;    //0-disable 1-OB 2-MA  3-CA
};
typedef struct tagSMTPInfo ACU_SMTP_INFO;

#define SMS_PHONE_NUMBER_LENTH	20
struct tagSMSPhoneInfo
{    
    char szPhoneNumber1[SMS_PHONE_NUMBER_LENTH];
    char szPhoneNumber2[SMS_PHONE_NUMBER_LENTH];
    char szPhoneNumber3[SMS_PHONE_NUMBER_LENTH];
    int	 iAlarmLevel;    //0-disable 1-OB 2-MA  3-CA
};
typedef struct tagSMSPhoneInfo SMS_PHONE_INFO;

struct tagAcuProductInfo
{
	char szACUSerialNo[32];
	char szHWRevision[16];
	char szSWRevision[16];

	char szEthaddr[32];
	char szFileSystemVersion[16];

#ifdef	CR_CFGFILE_INFO			//CR:add config version to lcd	-caihao  2005.12.23
	char szCfgFileVersion[16];
#endif
	char szProductNo[16];

#ifdef PRODUCT_INFO_SUPPORT
	char szPartNumber[16];
#endif //PRODUCT_INFO_SUPPORT

};
typedef struct tagAcuProductInfo ACU_PRODUCT_INFO;

struct tagKEY_INFO
{
	UCHAR			byKeyValue;					//Logic Sector Index
	int			nCount;					//Physical Sector Index

};
typedef struct tagKEY_INFO _KEY_INFO_;

enum	DIX_AUTOCFG_MSG_ID
{
	ID_AUTOCFG_START = 0,
	ID_AUTOCFG_REFRESH_DATA,

	MAX_AUTOCFG_MSG_NUM
};

//Initialize data exchange module
//it will be called when the main module initialize the system
BOOL InitDataExchangeModule(void);
void DestroyDataExchangeModule(void);


/*==========================================================================*
* FUNCTION : DxiGetData
* PURPOSE  : Get data or config from the main module
* CALLS    : The following sub functions
* CALLED BY: To be called by the services
* ARGUMENTS: int    nInterfaceType : Interface type
*            int    nVarID         : Data ID
*            int    nVarSubID      : Data sub ID
*            int*   pBufLen        : The length of the buffer
*            void*  pDataBuf       : The data buffer
*            int    nTimeOut       : The time out for getting data
* RETURN   : int : The error code for getting data			  
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-14 17:08
*==========================================================================*/
int DxiGetData(int   nInterfaceType,		
			   int   nVarID,			
			   int   nVarSubID,		
			   int*  pBufLen,			
			   void* pDataBuf,			
			   int   nTimeOut);



PRODUCT_INFO *DXI_GetDeviceProductInfo(void);

BOOL DXI_isTempSignal(SIG_BASIC_VALUE* pSigValue);
BOOL DXI_isTempSigUnit(char *SigcUnit);
#define IS_TEMP_SIGNAL(iEquipID, iSignalType, iSignalID) \
(\
	(iEquipID==1 && iSignalType == 0 && (iSignalID >= 43 && iSignalID <= 51)) || \
	(iEquipID==1 && iSignalType == 0 && (iSignalID == 53 || iSignalID == 69)) ||\
	(iEquipID==1 && iSignalType == 0 && (iSignalID >= 111 && iSignalID <= 181)) ||\
	(iEquipID==1 && iSignalType == 0 && (iSignalID >= 220 && iSignalID <= 227)) ||\
	(iEquipID==1 && iSignalType == 2 && (iSignalID == 209 || iSignalID == 210 || iSignalID == 219 )) ||\
	(iEquipID==1 && iSignalType == 2 && (iSignalID >= 231 && iSignalID <= 443)) ||\
	(iEquipID==1 && iSignalType == 2 && (iSignalID >= 486 && iSignalID <= 497)) ||\
	(iEquipID==115 && iSignalType == 0 && iSignalID == 36) ||\
	(iEquipID==115 && iSignalType == 2 && (iSignalID == 76 || iSignalID == 77 || iSignalID == 78 || iSignalID == 27)) ||\
	(iEquipID==186 && iSignalType == 2 && (iSignalID == 60 || iSignalID == 61)) ||\
	(((iEquipID>=211 && iEquipID<=258) || (iEquipID>=1400 && iEquipID<=1411) )&& iSignalType == 0 && iSignalID == 3) ||\
	((iEquipID>=709 && iEquipID <=716 ) && iSignalType == 0 && (iSignalID >= 1 && iSignalID <= 8)) ||\
	((iEquipID>=1351 && iEquipID<=1380 )&& iSignalType == 0 && iSignalID == 4) \
)

#define IS_TEMP_COE_SIGNAL(iEquipID, iSignalType, iSignalID) \
(\
	(iEquipID==115 && iSignalType == 2 && iSignalID == 28)  \
)
#define		TEMP_VALUE		1
#define		TEMP_COE		0

#define		CELSIUS			0
#define		FAHRENHEIT		1

int DXI_SetTempSwitch(IN int iFunType,		//0 for coeficient 1 for value
					float fTempDataOld,
					float *fTempData);

int DXI_GetTempSwitch(IN int iFunType,		//0 for coeficient 1 for value
					IN float fTempDataOld,
					OUT float *fTempData,
					OUT char *cTempUnit);

int DXI_GetTimeFormat(void);
/*==========================================================================*
* FUNCTION : DxiSetData
* PURPOSE  : Set data or config from the main module
* CALLS    : The following sub functions
* CALLED BY: To be called by the services
* ARGUMENTS: int    nInterfaceType : Interface type
*            int    nVarID         : Data ID
*            int    nVarSubID      : Data sub ID
*            int    nBufLen        : The length of the buffer
*            void*  pDataBuf       : The data buffer 
*            int    nTimeOut       : The time out for setting data
* RETURN   : int : The error code for setting data			  
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-14 16:48
*==========================================================================*/
int DxiSetData(int	 nInterfaceType,
			   int	 nVarID,			
			   int	 nVarSubID,		
			   int	 nBufLen,		
			   void* pDataBuf,		
			   int	 nTimeOut);  

/*==========================================================================*
* FUNCTION : DxiRegisterNotification
* PURPOSE  : Register or logout the trap call back routine
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char                 *pszProcName    : Register name for debug 
*            SERVICE_NOTIFY_PROC  proc            : Trap call back procedure
*            void*                lpParam         : Parameter for procedure
*            int                  nMsgType        : Call back message type
*            HANDLE               hService        : The handle of service
*            BOOL                 bRegisterHandle : Register or logout flag
* RETURN   : int : Error code for register or logout
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-14 15:48
*==========================================================================*/
int DxiRegisterNotification(char				*pszProcName, 
							SERVICE_NOTIFY_PROC proc,
							void*				lpParam, 							  
							int					nMsgType, 							  
							HANDLE				hService,							  
							BOOL				bRegisterHandle);

/*==========================================================================*
* FUNCTION : NotificationFunc
* PURPOSE  : Call the trap call back function according to msg type
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    nMsgType        : The message type 
*            int    nTrapDataLength : The length of trap data
*            void*  lpTrapData      : The trap data buffer
*            BOOL   bUrgent         : The urgent level
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-14 15:59
*==========================================================================*/
void NotificationFunc(int	nMsgType,	
					  int	nTrapDataLength,				  
					  void* lpTrapData,					  
					  BOOL	bUrgent);

/* get lib name */
char *GetStandardPortDriverName(IN int nStdPortType);


// the config base dir.
extern char		g_szACUConfigDir[MAX_FILE_PATH];	// defined in main.c

char *Cfg_GetFullConfigPath(IN char *pszPartPath, 
							OUT char *szFullPath, 
							IN int iFullPathLen);

HANDLE DXI_GetESRCommonCfgMutex(void);

int DXI_GetActiveAlarmItems(int* pAlarmNum,
						ALARM_SIG_VALUE** ppActiveAlarmSigValue);

int DXI_GetEquipIDFromStdEquipID(int nStdEquipID);

BOOL DXI_GetStdEquipIDStatus(int nStdEquipID);

void DXI_MakeAlarmSigValueSort(ALARM_SIG_VALUE* pAlarmSigValue, 
								  int nAlarmNum);

BOOL DXI_RebootACUorSCU(IN BOOL bResetSCU, 
						 IN BOOL bResetACU);

BOOL DXI_CopyConfigFiles(IN char *pszCopyFrom, 
						 IN char *pszCopyTo, 
						 IN BOOL bCopyForce);

int DXI_ReloadDefaultConfig(void);

BOOL DXI_USBGetConfigFiles(IN char *pszConfigFiles);
BOOL DXI_USBMountDirect(void);

/*==========================================================================*
 * FUNCTION : DXI_GetConfigStatus
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  *pConfigType : 
 *		#define CONFIG_NORMAL_TYPE	0	// The being used config is normal
 *		#define CONFIG_BACKUP_TYPE	1	// the being used config is backup
 *		#define CONFIG_DEFAULT_TYPE	2	// the being used config is default.
 * RETURN   : int : the config changed status
 *		#define CONFIG_STATUS_NOT_CHANGED		0	// the config is not changed.
 *		#define CONFIG_STATUS_NORMALLY_CHANGED	1	// the config just changed,
 *                                                  // historical data need be removed.
 *		#define CONFIG_STATUS_DEFAULT_LOADED	2	// the config just changed to default,
 *                                                  // all record files shall be removed.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-24 21:39
 *==========================================================================*/
int DXI_GetConfigStatus(int *pConfigType);


// do NOT use the Equip_Control and Equip_ControlEx directly, please use DxiSetData(...).

/*==========================================================================*
 * FUNCTION : Equip_Control
 * PURPOSE  : send a ctrl command to a equipment
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int nSenderType       : the type of sender, 
 *								see app_service.h:APP_SERVICE_TYPE_ENUM 
 *            IN char*pszSenderName    : the sender name in str
 *            IN int nSendDirectly     : see EQUP_CTRL_SEND...
 *            int         nEquipID     : the equipment cmd will be sent to
 *            int         nSigType     : the sig type
 *            int         nSigID       : 
 *            VAR_VALUE   *pCtrlValue  : 
 *            DWORD         dwTimeout     : timeout is ms
 * RETURN   : int : ERR_EQP_OK: ok, ERR_EQP_TIMEOUT: timeout on sending
 *                  ERR_EQP_COMM_FAILURE: comm failure on sending.
 *                  ERR_INVALID_ARGS: for err ctrl args
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-07 11:07
 *==========================================================================*/
int Equip_Control(IN int nSenderType, IN char *pszSenderName,
				  IN int nSendDirectly,
				  IN int nEquipID, IN int nSigType, IN int nSigID, 
				  IN VAR_VALUE *pCtrlValue, IN DWORD dwTimeout);

/*==========================================================================*
 * FUNCTION : Equip_ControlEx
 * PURPOSE  : ctrl by equip and sig addr
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int     nSenderType       : the type of sender, 
 *						          see app_service.h:APP_SERVICE_TYPE_ENUM 
 *            IN char    *pszSenderName    : the sender name in str
 *            IN int     nSendDirectly     : see EQUP_CTRL_SEND...
 *			  EQUIP_INFO       *pEquip     : 
 *            IN int           nSigType    : 
 *            SIG_BASIC_VALUE  *pSig       : 
 *            VAR_VALUE        *pCtrlValue : 
 *            DWORD            dwTimeout   : 
 * RETURN   : int : ERR_EQP_OK: ok, ERR_EQP_TIMEOUT: timeout on sending
 *                  ERR_EQP_COMM_FAILURE: comm failure on sending.
 *                  ERR_INVALID_ARGS: for err ctrl args
 *                  ERR_EQP_CTRL_SUPPRESSED: for suppressed by other signal
 *                  ERR_EQP_CTRL_DISABLED: for hardware switch disables control.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-24 14:54
 *==========================================================================*/
int Equip_ControlEx(IN int nSenderType, IN char *pszSenderName,
				  IN int nSendDirectly ,
				  IN EQUIP_INFO *pEquip, IN int nSigType,
                  IN OUT SIG_BASIC_VALUE *pSig, 
				  IN VAR_VALUE *pCtrlValue, IN DWORD dwTimeout);


/*==========================================================================*
* function : DIX_AutoConfigMain
* purpose  : auto config main logic 
* calls    : 
* called by: 
* return   : handle : 
* comments : 
* creator  : wangjing                date: 2007-06-21 10:43
*==========================================================================*/
void DIX_AutoConfigMain(void);

void DIX_AutoConfigMain_ForSlavePosition(void);

void DIX_AutoConfigMain_ForSlavePosition_ForWeb(void);

//G3 loads typical equipments by default, reboot first before auto-config
BOOL DXI_AutoConfigWithReboot(void);


/* for com share service switch function */
#define COM_SHARE_SERVICE_SWITCH

#ifdef COM_SHARE_SERVICE_SWITCH
struct tagServiceSwitchFlag
{
	volatile BOOL bCOMHaveClosed;
	volatile BOOL bHLMSUseCOM;
	volatile BOOL bMtnCOMHasClient;
};
typedef struct tagServiceSwitchFlag SERVICE_SWITCH_FLAG;

HANDLE DXI_GetServiceSwitchMutex(void);
SERVICE_SWITCH_FLAG * DXI_GetServiceSwitchFlag(void);
#endif //COM_SHARE_SERVICE_SWITCH


#ifdef PRODUCT_INFO_SUPPORT
PRODUCT_INFO *DXI_GetPSCDeviceProductInfo(void);

int DXI_GetDeviceNum(void);
int DXI_GetPIByDeviceID(int iDeviceID, PRODUCT_INFO *pPI);
BOOL DXI_IsDeviceRelativeEquip(int iDeviceID, int iEquipID);
DEVICE_INFO * DXI_GetDeviceByID(int iDeviceID);
int	DXI_GetRectRealNumber(void);
int DXI_GetFirstRectDeviceID(void);

#endif //PRODUCT_INFO_SUPPORT

BOOL SetFloatSigValue(int nEquipId,
					  int nSignalType,
					  int nSignalId,
					  float fValue,
					  char*	pcUser);

BOOL SetEnumSigValue(int nEquipId,
					 int nSignalType,
					 int nSignalId,
					 SIG_ENUM EnumValue,
					 char*	pcUser);

BOOL SetDwordSigValue(int nEquipId,
					  int nSignalType,
					  int nSignalId,
					  DWORD dwValue,
					  char* pcUser);
float GetFloatSigValue(int nEquipId,
					   int nSignalType,
					   int nSignalId,
					   char* pcUser);
DWORD GetDwordSigValue(int nEquipId, 
					   int nSignalType,
					   int nSignalId,
					   char* pcUser);
SIG_ENUM GetEnumSigValue(int nEquipId, 
					   int nSignalType,
					   int nSignalId,
					   char* pcUser);
BOOL DXI_IsEquipExist(int iEquipID);
void DXI_ChangeLanguage(IN char *szLangType);

#endif //__DATA_EXCHANGE_H__040914


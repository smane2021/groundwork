/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_interface.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:55
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_INTERFACE_H__041008
#define __LCD_INTERFACE_H__041008

#ifdef  __cplusplus
extern "C" {
#endif

//Used to record log
#ifndef LCD_UI_TASK
#define LCD_UI_TASK "LCD_UI"
#endif //LCD_UI_TASK

//LCD and key driver command code
/*
#define IOC_TEXT_MODE               0x6b01
#define IOC_GRAPHIC_MODE            0x6b02
#define IOC_BEEP_ON                 0x6b80
#define IOC_BEEP_OFF                0x6b90 
#define IOC_SET_BL					0x6b05
#define IOC_LCD_INIT                0x6b06
#define IOC_LCD_CLEAR               0x6b07
#define IOC_XOR_MODE				0x6b0c
#define IOC_OR_MODE					0x6b0d
#define IOC_AND_MODE                0x6b0e
#define IOC_NXOR_MODE               0x6b0f
#define IOC_FILL_SCREEN             0x6b10
#define IOC_COPY_MODE				0x6b11

#define IOC_KEY_DELAY				0x6b01
#define IOC_KEY_RATE				0x6b02
*/


//LCD driver
#define LCD_MAJOR			243  

#define IOC_TEXT_MODE				_IOW(LCD_MAJOR, 1, unsigned long)
#define IOC_GRAPHIC_MODE			_IOW(LCD_MAJOR, 2, unsigned long)
#define IOC_SET_CURSOR_DIR			_IOW(LCD_MAJOR, 3, unsigned long)
#define IOC_SET_CURSOR_SHAPE		_IOW(LCD_MAJOR, 4, unsigned long)
#define IOC_SET_BL					_IOW(LCD_MAJOR, 5, unsigned long)
#define IOC_LCD_INIT				_IOW(LCD_MAJOR, 6, unsigned long)
#define IOC_CLEAR_SCREEN			_IOW(LCD_MAJOR, 7, unsigned long)
#define IOC_DISP_OFF				_IOW(LCD_MAJOR, 8, unsigned long)
#define IOC_DISP_ON					_IOW(LCD_MAJOR, 9, unsigned long)
#define IOC_CURSOR_ON				_IOW(LCD_MAJOR, 10, unsigned long)
#define IOC_CURSOR_OFF				_IOW(LCD_MAJOR, 11, unsigned long)
#define IOC_BEEP_ON					_IOW(LCD_MAJOR, 12, unsigned long)
#define IOC_BEEP_OFF				_IOW(LCD_MAJOR, 13, unsigned long)

#define IOC_XOR_MODE				_IOW(LCD_MAJOR, 14, unsigned long)
#define IOC_OR_MODE					_IOW(LCD_MAJOR, 15, unsigned long)
#define IOC_AND_MODE				_IOW(LCD_MAJOR, 16, unsigned long)
#define IOC_NXOR_MODE				_IOW(LCD_MAJOR, 17, unsigned long)
#define IOC_FILL_SCREEN				_IOW(LCD_MAJOR, 18, unsigned long)
#define IOC_COPY_MODE				_IOW(LCD_MAJOR, 19, unsigned long)

#define IOC_SET_LCD_CURSOR			_IOW(LCD_MAJOR, 20, unsigned long)
#define IOC_SET_WRITE_MODE			_IOW(LCD_MAJOR, 21, unsigned long)
#define IOC_SET_ROLL_DEGREE			_IOW(LCD_MAJOR, 22, unsigned long)

#define IOC_LCD_CLEAR				_IOW(LCD_MAJOR, 7, unsigned long)



//Led driver
#define LED_MAJOR			251  

//#ifdef _CODE_FOR_MINI
#define IOCTL_SET_RLED				_IOW(LED_MAJOR, 1, unsigned long)
#define IOCTL_SET_GLED				_IOW(LED_MAJOR, 2, unsigned long)
#define IOCTL_SET_YLED				_IOW(LED_MAJOR, 3, unsigned long)
#define LED_ON_VAL					1
#define LED_OFF_VAL					0
//#endif

#ifdef _CODE_FOR_MINI
	//Keypad driver
	#define KEY_MAJOR			250
#else
	#define KEY_MAJOR			252
#endif

#define IOC_KEY_DELAY				_IOW(KEY_MAJOR, 1, unsigned long)
#define IOC_KEY_RATE				_IOW(KEY_MAJOR, 2, unsigned long)

//#ifdef _CODE_FOR_MINI
#define IOC_KEY_EN					_IOW(KEY_MAJOR, 3, unsigned long)
//#endif

//Specified key value
#define	NGMC_KEYBOARD_K1	0x01
#define	NGMC_KEYBOARD_K2	0x02
#define	NGMC_KEYBOARD_K3	0x04
#define	NGMC_KEYBOARD_K4	0x08

#define	NGMC_KEYBOARD_COMBO_BASE	0x40

	enum KEY_VALUE_ENUM
	{
		VK_NO_KEY	= 0x00,

		VK_ESCAPE	= NGMC_KEYBOARD_K1,
//		VK_LEFT		= 0x20,
		VK_UP		= NGMC_KEYBOARD_K3,
		VK_DOWN		= NGMC_KEYBOARD_K2,
//		VK_RIGHT	= 0x10,
		VK_ENTER	= NGMC_KEYBOARD_K4,
		

		VK_UP_DOWN		= NGMC_KEYBOARD_COMBO_BASE + VK_UP + VK_DOWN, 

		VK_ENTER_UP		= NGMC_KEYBOARD_COMBO_BASE + VK_ENTER + VK_UP,
		VK_ENTER_DOWN	= NGMC_KEYBOARD_COMBO_BASE + VK_ENTER + VK_DOWN,

		VK_ENTER_ESC	= NGMC_KEYBOARD_COMBO_BASE + VK_ENTER + VK_ESCAPE,

	};



	// LCD ��������
#define ENV_VAR_EXE_FILE			"/home/env_lcd/env_lcd"
#define ENV_VAR_READ_FLAG			0
#define ENV_VAR_WRITE_FLAG			1
#define ENV_VAR_LCD_ROTATION		"lcd_mode"
#define ENV_VAR_LCD_HEIGHT			"lcd_height"










//Used to write to LCD driver
typedef struct tagLcdDotStru
{
	unsigned char x;
	unsigned char y;
	unsigned char data;

}LCD_DOT_STRU;


#define LCD_DOT_STRU_ASSIGN(pLcdDot, nX, nY, byData)\
	(pLcdDot)->x = (nX),       \
	(pLcdDot)->y  = (nY),       \
	(pLcdDot)->data    = (byData)

//#define SCREEN_WIDTH		    128
//#define SCREEN_HEIGHT			128
#define SCREEN_WIDTH		    GetScreenWidth()
#define SCREEN_HEIGHT			GetScreenHeight()

#ifdef _CODE_FOR_MINI
//Frank Wu,20160127, for MiniNCU
//#define	TOP_BLANK_HEIGHT_OF_SCREEN	((SCREEN_HEIGHT - 64) / 2)
#define	TOP_BLANK_HEIGHT_OF_SCREEN	((((SCREEN_HEIGHT - 64) / 2) > 0)? ((SCREEN_HEIGHT - 64) / 2): 0)
#else
#define	TOP_BLANK_HEIGHT_OF_SCREEN	((SCREEN_HEIGHT - 64) / 2)
#endif


#define	MAX_CHAR_NUM_PER_LINE	16


#ifdef _CODE_FOR_MINI
#define MAX_CHARS_OF_LINE		64
#else
#define MAX_CHARS_OF_LINE		20
#endif

#define LCD_FIXED_LANGUAGE "en"

#ifdef _CODE_FOR_MINI
#define LCD_FIXED_LANGUAGE_EN	LCD_FIXED_LANGUAGE
#define LCD_FIXED_LANGUAGE_ZH	"zh"//Chinese 
#define LCD_FIXED_LANGUAGE_TW	"tw"//Traditional Chinese
#define LCD_FIXED_LANGUAGE_DE	"de"//Deutsch 
#define LCD_FIXED_LANGUAGE_ES	"es"//Spanish
#define LCD_FIXED_LANGUAGE_FR	"fr"//French
#define LCD_FIXED_LANGUAGE_IT	"it"//Italian
#define LCD_FIXED_LANGUAGE_RU	"ru"//Russian
#define LCD_FIXED_LANGUAGE_TR	"tr"//Turkish
#define LCD_FIXED_LANGUAGE_SV	"sv"//Swedish
#define LCD_FIXED_LANGUAGE_PT	"pt"//Portuguese
#endif

//A arrow width, used to many places to specified width
#define ARROW_WIDTH			8


#define FLAG_REGIST_FONT_LIB   0x01
#define FLAG_UNREGIST_FONT_LIB 0x00

#define FLAG_DIS_NORMAL_TYPE	(0x01)
#define FLAG_DIS_REVERSE_TYPE	(0x00)

	//The corresponing to ASCII character codes chart
	enum ASCII_LIB_INDEX_ENUM
	{
		ASCII_UP_ICON		= 0x1E,
		ASCII_DOWN_ICON		= 0x1F,
		ASCII_RIGHT_ICON	= 0x10,
		ASCII_LEFT_ICON		= 0x11,

		ASCII_RIGHT_ARROW	= 0x1A,
		ASCII_LEFT_ARROW	= 0x1B,
		ASCII_UP_ARROW		= 0x18,
		ASCII_DOWN_ARROW	= 0x19,

		ASCII_DEGREE_ICON	= 0x09,

	};


	/*==========================================================================*
	* FUNCTION : InitLCD
	* PURPOSE  : Open LCD and keyboard, and set the operate mode of LCD
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS:   void : 
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 15:32
	*==========================================================================*/
	int InitLCD(void);

	int CloseLcdDriver(void);

	/*==========================================================================*
	* FUNCTION : ChangeDrawMode
	* PURPOSE  : Change the LCD operation mode, it has OR/COPY/AND modes etc..
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: int  nMode : The needed mode
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 15:35
	*==========================================================================*/
	int ChangeDrawMode(int nMode);

	int ClearScreen(void);

	int Light(BOOL bLight);


#define LED_RUN 0x10
#define LED_YEL 0x20
#define LED_RED 0x40

	int AlarmLedCtrl(int nAlarmLed, BOOL bOpen);

	/*==========================================================================*
	* FUNCTION : Light
	* PURPOSE  : Open or close the back light
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: BOOL  bLight : TRUE, open, FALSE, close
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 15:37
	*==========================================================================*/
	int AdjustBright(BOOL bAddBright);

	/*==========================================================================*
	* FUNCTION : AdjustKeyParam
	* PURPOSE  : Adjust the key params(Rate and Delay), support two modes
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: BOOL  bAccelerateFlag : TRUE: short Delay, FALSE: long delay
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 15:41
	*==========================================================================*/
	int AdjustKeyParam(BOOL bAccelerateFlag);

	/*==========================================================================*
	* FUNCTION : BuzzerBeep
	* PURPOSE  : Open or close the buzzer beep
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: BOOL  bOpen : 
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 15:43
	*==========================================================================*/
	int BuzzerBeep(BOOL bOpen);

	int BuzzerBeepCtrl(BOOL bOpen, int nParam);

	/*==========================================================================*
	* FUNCTION : DisCursor
	* PURPOSE  : Display cursor on appointed position
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: int  x         : 
	*            int  y         : 
	*            int  nDispType : The positive or reverse display mode
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 15:44
	*==========================================================================*/
	int DisCursor(int x, int y, int	nDispType);


	/*==========================================================================*
	* FUNCTION : DisWelcomeScreen
	* PURPOSE  : Display the welcome screen
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: int  x : 
	*            int  y : 
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 16:02
	*==========================================================================*/
	int DisWelcomeScreen(int x,int y);

	/*==========================================================================*
	* FUNCTION : DisSpecifiedFlag
	* PURPOSE  : Display a specified flag on screen, the matrix in ascii lib
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: char  cFlag : 
	*            int   x     : 
	*            int   y     : 
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 16:55
	*==========================================================================*/
	int DisSpecifiedFlag(char cFlag, int x, int y);

	/*==========================================================================*
	* FUNCTION : ClearBar
	* PURPOSE  : Clear the appointed rect
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: int  x         : Rect start x position
	*            int  y         : Rect start y position
	*            int  nWidth    : Rect width
	*            int  nHeight   : Rect height
	*            int  nDispType : reverse mode
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 16:43
	*==========================================================================*/
	int ClearBar( int x,
		int y,
		int nWidth,
		int nHeight, 
		int  nDispType);

	/*==========================================================================*
	* FUNCTION : DisString
	* PURPOSE  : Display string on the screen
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: char*  pBuf      : The characters buffer
	*            int    nLength   : The characters number 
	*            int    x         : x pixel offset on the LCD screen
	*            int    y         : y pixel offset on the LCD screen
	*            char*  pLangCode : Language code
	*            int    nDispType : Display mode 
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-11-12 18:29
	*==========================================================================*/
	int DisString(char* pBuf,
		int		nLength,
		int		x,
		int		y,
		char*	pLangCode, 
		int		nDispType);

	/*==========================================================================*
	* FUNCTION : GetWidthOfString
	* PURPOSE  : Get the display screen width of a appointed buffer and its length 
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: char*  pBuf      : 
	*            int    nLength   : 
	*            char*  pLangCode : 
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 16:49
	*==========================================================================*/
	int GetWidthOfString(char*	pBuf, int nLength, char* pLangCode);


	/*==========================================================================*
	* FUNCTION : GetKey
	* PURPOSE  : Get the current pressed key
	*			  Considered key accelerated
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: void : 
	* RETURN   : UCHAR : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 16:50
	*==========================================================================*/
	UCHAR GetKey(void);

	/*==========================================================================*
	* FUNCTION : RegisterFontLib
	* PURPOSE  : Regist the supported languages
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: int     nLangNum   : languages num need to be initted
	*            char**  ppLangCode : languages codes, include nLangNum languages
	* RETURN   : int : The success of failure flag for initted result,
	*			  ERR_LCD_OK is success.
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 11:50
	*==========================================================================*/
	int RegisterFontLib(int nLangNum, char** ppLangCode, BOOL bRegFlag);

	int GetFontHeightFromCode(char* pLangCode);

	/*==========================================================================*
	* FUNCTION : DisplayProgressBar
	* PURPOSE  : Display the progress bar
	* CALLS    : 
	* CALLED BY: 
	* ARGUMENTS: int  nItems  : 
	*            int  nCur    : 
	*            int  nTop    : 
	*            int  nBottom : 
	* RETURN   : int : 
	* COMMENTS : 
	* CREATOR  : HULONGWEN                DATE: 2004-12-04 16:56
	*==========================================================================*/
	int DisplayProgressBar(int	nItems,
		int	nCur,
		int	nTop,
		int	nBottom);

	int DisplayProgressArrow(int	nItems,
					   int	nCur,
					   int	nTop,
					   int	nBottom);

/*==========================================================================*
 * FUNCTION : LCD_DisplayBmp
 * PURPOSE  : Display the a B&W bmp img screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  x : 
 *            int  y : 
 *            char *pBmpImg : the B&W color bmp img
 *            int  nBmpSize : the byte of the pBmpImg
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:02
 *==========================================================================*/
int LCD_DisplayBmp(IN int x, IN int y, IN char *pBmpImg, IN int nBmpSize);




int ReadEnvironmentVar(char *pEnvVarName);

int GetScreenWidth(void);
int GetScreenHeight(void);
void SetScreenWidth(int nScreenWidth);
void SetScreenHeight(int nScreenHeight);

int SetLCDRotation(int iRotationAngle);
int GetLCDRotation(void);

void SetLCDBright(int nBright);
int	GetLCDBright(void);

int DispBigSizeString(char* pBuf,
					  int		nLength,
					  int		x,
					  int		y,
					  char*		pLangCode, 
					  int		nDispType);


int SelfTestFunctionForDriver(void);




#ifdef __cplusplus
}
#endif


#endif //__LCD_INTERFACE_H__041008


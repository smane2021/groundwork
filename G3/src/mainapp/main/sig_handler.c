#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h> 

#include "stdsys.h"
#include "basetypes.h"

#include "sig_handler.h"

/*==========================================================================*
 * FUNCTION : Sig_InstallHandlers
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  bIgnoreOthers   : 
 *            void  *pfnSigHandler1 : 
 *            int   nSigNo1         : 
 *                  ...             : 
 * RETURN   : int : 
 * COMMENTS : Sig_InstallHandlers(handler1, sig1, sig2, ..., 0,
								  handle2, sig3, sig4, ..., 0,
								  0/NULL/end); 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-17 17:34
 *==========================================================================*/
int Sig_InstallHandlers(BOOL bIgnoreOthers, 
						void *pfnSigHandler1, int nSigNo1, ...)
{
	int				    nSigNo;
	__sighandler_t		pSighandler;

	va_list				sigList;
	int					nResult = 0;

  	struct sigaction	act;

	if (bIgnoreOthers)
	{
		// ignore all signal at first
		for (nSigNo = 1; nSigNo < _NSIG; nSigNo++)
		{
			sigemptyset(&act.sa_mask);
			act.sa_flags					 = SA_SIGINFO;
			//(__sighandler_t)act.sa_sigaction = (__sighandler_t)SIG_IGN;
			pSighandler = SIG_IGN;
			memcpy(&act.sa_sigaction, &pSighandler, sizeof(__sighandler_t));

			sigaction((int)nSigNo, &act, NULL);
		}
	}

	// set the signal handler we want to handle.
	va_start(sigList, nSigNo1);   /* Initialize variable arguments. */

	nSigNo      = nSigNo1;
	pSighandler = (__sighandler_t)pfnSigHandler1;

	while ((nSigNo > 0) && (pSighandler != NULL))
	{
		/* Set up the structure to specify the new action. */
		sigemptyset(&act.sa_mask);
		act.sa_flags		             = SA_SIGINFO;
		//(__sighandler_t)act.sa_sigaction = (__sighandler_t)pSighandler;
		memcpy(&act.sa_sigaction, &pSighandler, sizeof(__sighandler_t));

#ifdef _DEBUG
		//printf("Installing signal #%d handler %p...\n",
		//		nSigNo, pSighandler);
#endif //_DEBUG

		if (sigaction((int)nSigNo, &act, NULL) < 0)	// ignore old.
		{
#ifdef _DEBUG
			int		nErrNo = errno;
			printf("Installing signal #%d handler %p got error %d(%s).\n",
				nSigNo, pSighandler,
				nErrNo, strerror(nErrNo));
#endif //_DEBUG

			nResult = -1;
		}

		// get the next signal handle info.
		nSigNo		= va_arg(sigList, int);
		if (nSigNo == 0)// to get next handler.
		{
			pSighandler = va_arg(sigList, __sighandler_t);
			if (pSighandler != NULL)
			{
				nSigNo		= va_arg(sigList, int);
			}
		}
	}

	va_end(sigList);              /* Reset variable arguments.      */

	return nResult;
}





#if 0	// test code.
static void Main_SignalHandler(IN int nSig, IN siginfo_t *pSigInfo, 
							   IN void *pSigVal )
{
	printf("[Handler_SIGSEGV] -- caught %d\n", n );

	printf( "si_signo = %d\n", pSigInfo->si_signo);  /* Signal number */
	printf( "si_errno = %d\n", pSigInfo->si_errno);	 /* An errno value */
	printf( "si_code  = %d\n", pSigInfo->si_code);	 /* Signal code */

	printf( "si_pid   = %d\n", pSigInfo->si_pid);	 /* Sending process ID */
	printf( "si_uid   = %d\n", pSigInfo->si_uid);	 /* Real user ID of sending process */
	printf( "si_status= %d\n", pSigInfo->si_status); /* Exit value or signal */
	printf( "si_utime = %d\n", (int)pSigInfo->si_utime);	 /* User time consumed */
	printf( "si_stime = %d\n", (int)pSigInfo->si_stime);	 /* System time consumed */
//	printf( "si_value = %d\n", *(int*)&pSigInfo->si_value);	 /* Signal value */
	printf( "si_int   = %d\n", (int)pSigInfo->si_int);	 /* POSIX.1b signal */
	printf( "si_ptr   = %p\n", pSigInfo->si_ptr);	 /* POSIX.1b signal */
	printf( "si_addr  = %p\n", pSigInfo->si_addr);	 /* Memory location which caused fault */
	printf( "si_band  = %ld\n", pSigInfo->si_band);	 /* Band event */
	printf( "si_fd    = %d\n", pSigInfo->si_fd);	 /* File descriptor */

	printf( "Exited....\n");
	fflush(stdout);
	exit(-1);
}


#define SIG_TRACE(msg, nMsgLen)		write(STDOUT_FILENO, (msg), (nMsgLen))

/* SIGILL, SIGFPE, SIGSEGV, SIGBUS.  */
static void Sig_Handler_SIGSEGV(IN int nSig, IN siginfo_t *pSigInfo, 
							   IN void *pSigVal)
{
	char	szBuf[256];
	int		nLen;

	UNUSED(pSigDataFromSender);

	nLen = snprintf(szBuf, sizeof(szBuf), 
		"[%s] - si_signo = %d, si_errno = %d, si_code = %d, si_addr = %p.\n",
		__FUNCTION__, 
		pSigInfo->si_signo, pSigInfo->si_errno,  pSigInfo->si_code,
		pSigInfo->si_addr);

	SIG_TRACE(szBuf, nLen);
}

static int create_new_SIGSEGV(void)
{
  	struct sigaction new_action, old_action;

	/* Set up the structure to specify the new action. */
	(__sighandler_t)new_action.sa_handler = (__sighandler_t)Handler_SIGSEGV;
	sigemptyset (&new_action.sa_mask);
	new_action.sa_flags = SA_SIGINFO;

	sigaction (SIGSEGV, &new_action, &old_action);

	/* set up to catch the SIGSEGV signal */
//	signal( SIGSEGV, Handler_SIGSEGV );
	return 0;
}

static void set_sig_val(void)
{
	pid_t pid;
	int signum;
	union sigval mysigval;

	signum= The_sig_num;
	pid=(pid_t)pid_to_process_sig;
	mysigval.sival_int=a_int_value;	// 

	if(sigqueue(pid, signum, mysigval) < 0)
	{
		printf("send error\n");
	}
}
#endif // if 0.

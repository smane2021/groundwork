/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : main_args.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-27 15:04
 *  VERSION  : V1.00
 *  PURPOSE  : parse the cmd line
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "main.h"

// return: 0: OK, > 0: error Code, -1: stop running.
typedef int	(*CMD_OPTION_PROC)(char *pszOption);

struct _CMD_LINE_PARSER
{
	char				*pszOptName;
	CMD_OPTION_PROC		pfnParser;
	char				*pszOptHelp;
};

typedef struct _CMD_LINE_PARSER		CMD_LINE_PARSER;

#define _DEF_CMD_LINE_PARSER(op, parser, hlp)	\
	{ (op), (CMD_OPTION_PROC)(parser), (hlp)}

static char *s_pszMainPidFile = NULL;


// the functions to process cmd line args
/*==========================================================================*
 * FUNCTION : Main_CreatePidFile
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pszPidFile : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-27 19:08
 *==========================================================================*/
static int Main_CreatePidFile(IN char *pszPidFile)
{
	FILE	*fp = fopen(pszPidFile,"w+t");

	if (fp == NULL)
	{
		return 1; // continue.
	}

	fprintf(fp, "%d\n", getpid());
	fclose(fp);

	chmod(pszPidFile, 0777);

	s_pszMainPidFile = pszPidFile;

	return ERR_OK;
}



#define USER_CMD_FLAG		"ACU_CMD=20031015,QUIT."
#define ACU_RET_FLAG		"ACU_RET=20050501,ACK."

/*==========================================================================*
 * FUNCTION : Main_CheckStopCmd
 * PURPOSE  : check the stop cmd from user by the PID file.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : TRUE for quit, FALSE to continue.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-11 16:03
 *==========================================================================*/
BOOL Main_CheckStopCmd(void)
{
	FILE	*fp;
	BOOL	bQuit = FALSE;

	if (s_pszMainPidFile == NULL)
	{
		return bQuit;
	}

	fp = fopen(s_pszMainPidFile, "rt");
	if (fp != NULL)
	{
		int		pid;
		char	szLine[256];

		szLine[0] = 0;
		if (fgets(szLine, sizeof(szLine), fp) != NULL)
		{
			sscanf(szLine, "%d", &pid);
			if (pid == getpid())
			{
				szLine[0] = 0;
				if (fgets(szLine, sizeof(szLine), fp) != NULL)
				{
					if (strncmp(szLine, USER_CMD_FLAG, 
						(sizeof(USER_CMD_FLAG)-1)) == 0)	// OK
					{
						bQuit = TRUE;
					}
				}
			}	
		}

		fclose(fp);

		if (bQuit)
		{
			Main_AckStopCmd();
		}
	}


	return bQuit;
}


/*==========================================================================*
 * FUNCTION : Main_AckStopCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-13 20:08
 *==========================================================================*/
void Main_AckStopCmd(void)
{
	FILE	*fp;

	if (s_pszMainPidFile == NULL)
	{
		return;
	}

	fp = fopen(s_pszMainPidFile, "r+t");
	if (fp != NULL)
	{
		// append exiting flag.
		fseek(fp, 0L, SEEK_END);
		fprintf(fp, "\n%s\n", ACU_RET_FLAG);

		fclose(fp);

		//s_pszMainPidFile = NULL;	// avoid calling twice.
	}


	return;
}



/*==========================================================================*
 * FUNCTION : Main_ShowVersion
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pszContFlag : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-27 15:17
 *==========================================================================*/
static int Main_ShowVersion(IN char *pszContFlag)
{
	char	szStrVer[32];

	STRING_APP_VER(szStrVer, sizeof(szStrVer));

	printf("\nAPP_APPLICATION_VERSION=%s\n", szStrVer);
	printf("APP_APPLICATION_BUILT_DATE=%s %s\n", __DATE__, __TIME__);

	if (strcmp(pszContFlag, "=continue") == 0)
	{
		return 0;	// OK, continue run;
	}

	return -1;	// do NOT continue.
}



/*==========================================================================*
 * FUNCTION : Main_ProcessCmdLine
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int   argc    : 
 *            char  *argv[] : the format of args shall be:
*               -opt_name=opt_value
 * RETURN   : static int : -1: for show help or error, others: ERR_OK.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-27 14:53
 *==========================================================================*/
int	Main_ProcessCmdLine(IN int argc, IN char *argv[])
{
	int					i, n, iLenOpt;
	CMD_LINE_PARSER		cmdLine[] =
	{
		_DEF_CMD_LINE_PARSER("-pid=", Main_CreatePidFile, 
			"Set pid file used by stopacu. Format: -pid=pid_file_name"),
		_DEF_CMD_LINE_PARSER("-version", Main_ShowVersion, 
			"Show version of application. Format: -version[=continue]"),
	};

	// help
	if ((argc > 1) && (strcmp(argv[1], "-?") == 0))
	{
		printf("The supported cmd line options of ACU are:\n");

		for (n = 0; n < ITEM_OF(cmdLine); n++)
		{
			printf("\t%d: %s.\n", (n+1), cmdLine[n].pszOptHelp);
		}

		printf("\tNote: the args in [] are optional.\n");

		return -1;
	}

	// parse options
	for (i = 1; i < argc; i++)
	{
		for (n = 0; n < ITEM_OF(cmdLine); n++)
		{
			iLenOpt = strlen(cmdLine[n].pszOptName);
			ASSERT(iLenOpt > 0);

			if (strncmp(argv[i], cmdLine[n].pszOptName, (size_t)iLenOpt) ==0)
			{
				int nRet = cmdLine[n].pfnParser(&argv[i][iLenOpt]);

				if (nRet > 0)
				{
					printf("Error on processing command arg: %s.", argv[i]);
				}
				else if (nRet == -1)
				{
					return -1;	// do NOT continue;
				}

				break;
			}
		}

		if (n == ITEM_OF(cmdLine))
		{
			printf("Unknown command arg: %s.", argv[i]);
		}
	}

	return ERR_OK;
}


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : service_mgr.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 15:50
 *  VERSION  : V1.00
 *  PURPOSE  : manage(load/unload) the application services.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "cfg_model.h"
#include "../equip_mon/equip_mon.h"

// the ACU service info. 
///// this configuration will be moved to std config.
static int Service_GetProtocolType(IN SITE_INFO *pSite);

static APP_SERVICE	s_pAppService[] =
{
	/*DEF_APP_SERVICE("GEN_CTL",		"gen_ctl.so",		
		SERVICE_OF_LOGIC_CONTROL,	TRUE,  SERVICE_RUN_AS_TASK),*/

//#ifdef _HAS_LCD_UI
////#ifdef _HAS_LCD_UI
//DEF_APP_SERVICE("QT_FIFO",		"qt_fifo.so",	
//	SERVICE_OF_LOGIC_CONTROL,	TRUE, SERVICE_RUN_AS_TASK),
#ifdef _CODE_FOR_MINI
	DEF_APP_SERVICE("LCD_UI",		"lcd_module.so",	
					SERVICE_OF_USER_INTERFACE,	TRUE, SERVICE_RUN_AS_TASK),
#else
	DEF_APP_SERVICE("QT_FIFO",		"qt_fifo.so",			
		SERVICE_OF_LOGIC_CONTROL,	TRUE, SERVICE_RUN_AS_TASK), 
#endif

		//#endif //_HAS_LCD_UI
//#endif //_HAS_LCD_UI

    DEF_APP_SERVICE("WEB_UI",		"web_ui_comm.so",	
        SERVICE_OF_USER_INTERFACE,	FALSE, SERVICE_RUN_AS_TASK),

	DEF_APP_SERVICE("WEB_USER",		"web_user.so",	
		SERVICE_OF_USER_INTERFACE,	TRUE, SERVICE_RUN_AS_TASK),

	/*DEF_APP_SERVICE("CURL_APP",		"curl_app.so",	
		SERVICE_OF_USER_INTERFACE,	TRUE, SERVICE_RUN_AS_TASK),*/


	DEF_APP_SERVICE("YDN_MAIN",		"ydn23.so",		
		SERVICE_OF_DATA_COMM,		TRUE, SERVICE_RUN_AS_TASK),

	DEF_APP_SERVICE("ESR Protocol",		"eem_soc.so",			
		SERVICE_OF_DATA_COMM,		TRUE, SERVICE_RUN_AS_TASK),

	DEF_APP_SERVICE("SNMP_MAIN",			"snmp_agent.so",	
		SERVICE_OF_DATA_COMM,		FALSE, SERVICE_RUN_AS_TASK),

	
	DEF_APP_SERVICE("PLC",						"plc.so",			
		SERVICE_OF_LOGIC_CONTROL,	FALSE, SERVICE_RUN_AS_TASK),

	DEF_APP_SERVICE("SMS",			"sms.so",			
		SERVICE_OF_LOGIC_CONTROL,	TRUE, SERVICE_RUN_AS_TASK),

	DEF_APP_SERVICE("MAIL",			"mail.so",			
		SERVICE_OF_LOGIC_CONTROL,	TRUE, SERVICE_RUN_AS_TASK),

	DEF_APP_SERVICE("MODBUS_MAIN",		"modbus.so",		
		SERVICE_OF_DATA_COMM,		TRUE, SERVICE_RUN_AS_TASK),

	//changed by Frank Wu,N/N/N,20150429, for supporting TL1
	DEF_APP_SERVICE("TL1_MAIN",		"tl1.so",		
		SERVICE_OF_DATA_COMM,		TRUE, SERVICE_RUN_AS_TASK),

	/*DEF_APP_SERVICE("MAINTEN",		"libmaintenance_intf.so",
		SERVICE_OF_MISC,		FALSE, SERVICE_RUN_AS_TASK),*/

	DEF_APP_SERVICE("GEN_CTL",		"gen_ctl.so",		
		SERVICE_OF_LOGIC_CONTROL,	TRUE,  SERVICE_RUN_AS_TASK),
};
// macro to find a signal value pointer addr by ID
#define SERV_GET_SIG_VALUE_BY_ID( SIG_VAL_TYPE, P_SIG, N_SIG,	    \
							 nSigId, pFound)					\
{																\
	SIG_VAL_TYPE *pSig;											\
	int			  nSig, i;										\
																\
	pFound = NULL;												\
																\
	if (nSigId > 0) /* ID start from 1 */						\
	{															\
		nSig = (N_SIG);											\
																\
		/* quikly search the ID, the ID start from 1.*/			\
		if (nSigId <= nSig)										\
		{														\
			pSig = &(P_SIG)[nSigId-1]; 							\
			if(pSig->pStdSig->iSigID == nSigId)					\
			{													\
				pFound = (void *)pSig;							\
			}													\
		}														\
																\
		if (pFound == NULL)	/* fails on quick searching */		\
		{/* ID is not equal to the INDEX,compare ID one by one*/\
			pSig = (P_SIG);										\
			for (i = 0; i < nSig; i++, pSig++)					\
			{													\
				if(pSig->pStdSig->iSigID == nSigId)				\
				{												\
					pFound = (void *)pSig;						\
					break;										\
				}												\
			}													\
		}														\
	}															\
}																\
/* End of macro GET_SIG_VALUE_BY_ID */

#define APP_SERVICE_CFG		"app_service.cfg"



/*==========================================================================*
 * FUNCTION : ServiceManager_LoadServices
 * PURPOSE  : load services config from file, and open 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 * RETURN   : BOOL : TRUE for OK.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 15:45
 *==========================================================================*/
static BOOL ServiceManager_LoadServices(IN OUT SERVICE_MANAGER *pServiceMgr)
{
	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"Application service manager is loading the configuration of services...\n");

	pServiceMgr->pService = s_pAppService;
	pServiceMgr->nService = ITEM_OF(s_pAppService);
	pServiceMgr->hServiceManager = NULL;

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"The configuration of %d application services are loaded OK.\n",
		pServiceMgr->nService);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ServiceManager_UnloadServices
 * PURPOSE  : unload the service
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 16:03
 *==========================================================================*/
static BOOL ServiceManager_UnloadServices(IN OUT SERVICE_MANAGER *pServiceMgr)
{
	UNUSED(pServiceMgr);

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"Application service manager is unloading services...\n");

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"Application services are unloaded OK.\n");

	return TRUE;
}

int getFileSysVer(char * cVer)
{
#define OBJECT_STR  "NGC file sytem version: V"
#define VERSION_FILE					"/var/version"
    FILE    *fp = NULL;
    long    ulFileLen;
    char    *szFileContents;
    char    *szObject;
    int i;

    fp = fopen(VERSION_FILE, "r");
    if (fp == NULL)
    {
	return -1;
    }

    fseek(fp, 0, SEEK_END);
    ulFileLen = ftell(fp);
    if (ulFileLen == -1)
    {
	return -1;
    }
    fseek(fp, 0, SEEK_SET);
    szFileContents = NEW(char, ulFileLen + 1);
    if (szFileContents == NULL)
    {
	return -1;
    }
    ulFileLen = fread(szFileContents, sizeof(char), (size_t)ulFileLen, fp);
    fclose(fp);
    if (ulFileLen < 0) 
    {
	return -1;
    }
    szFileContents[ulFileLen] = 0;
    szObject = strstr((const char *)szFileContents, (const char *)OBJECT_STR);
    if(szObject == NULL)
    {
	return -1;
    }
    szObject += (sizeof(OBJECT_STR) - 1);
    //printf("%s\n", szObject);
    for(i = 0; i < 4; i++)
    {
	cVer[i] = *(szObject + i);
    }
    cVer[4] = 0;
    //printf("cVer = %s\n", cVer);
    return 1;
}

int getSNMPV3Flag(char * cVer)
{
#define OBJECT_STR  "#SNMPV3 Enable:"
#define VERSION_FILE	"/app/config/private/snmp/snmp_private.cfg"
    FILE    *fp = NULL;
    long    ulFileLen;
    char    *szFileContents;
    char    *szObject;
    int i;

    fp = fopen(VERSION_FILE, "r");
    if (fp == NULL)
    {
	return -1;
    }

    fseek(fp, 0, SEEK_END);
    ulFileLen = ftell(fp);
    if (ulFileLen == -1)
    {
	return -1;
    }
    fseek(fp, 0, SEEK_SET);
    szFileContents = NEW(char, ulFileLen + 1);
    if (szFileContents == NULL)
    {
	return -1;
    }
    ulFileLen = fread(szFileContents, sizeof(char), (size_t)ulFileLen, fp);
    fclose(fp);
    if (ulFileLen < 0) 
    {
	if(szFileContents != NULL)
	{
	    DELETE(szFileContents);
	    szFileContents = NULL;
	}
	return -1;
    }
    szFileContents[ulFileLen] = 0;
    szObject = strstr((const char *)szFileContents, (const char *)OBJECT_STR);
    if(szObject == NULL)
    {
	if(szFileContents != NULL)
	{
	    DELETE(szFileContents);
	    szFileContents = NULL;
	}
	return -1;
    }
    szObject += (sizeof(OBJECT_STR) - 1);
    //printf("%s\n", szObject);
    for(i = 0; i < 1; i++)
    {
	cVer[i] = *(szObject + i);
    }
    cVer[1] = 0;
   // printf("SNMPV3  cVer = %s\n", cVer);
    if(szFileContents != NULL)
    {
	DELETE(szFileContents);
	szFileContents = NULL;
    }
    return 1;
}

int getSSLFlag(char * cVer)
{
#define OBJECT_STR  "#SSL Enable:"
#define VERSION_FILE	"/app/config/private/web_user/cfg_ui_function.cfg"
    FILE    *fp = NULL;
    long    ulFileLen;
    char    *szFileContents;
    char    *szObject;
    int i;

    fp = fopen(VERSION_FILE, "r");
    if (fp == NULL)
    {
	return -1;
    }

    fseek(fp, 0, SEEK_END);
    ulFileLen = ftell(fp);
    if (ulFileLen == -1)
    {
	return -1;
    }
    fseek(fp, 0, SEEK_SET);
    szFileContents = NEW(char, ulFileLen + 1);
    if (szFileContents == NULL)
    {
	return -1;
    }
    ulFileLen = fread(szFileContents, sizeof(char), (size_t)ulFileLen, fp);
    fclose(fp);
    if (ulFileLen < 0) 
    {
	if(szFileContents != NULL)
	{
	    DELETE(szFileContents);
	    szFileContents = NULL;
	}
	return -1;
    }
    szFileContents[ulFileLen] = 0;
    szObject = strstr((const char *)szFileContents, (const char *)OBJECT_STR);
    if(szObject == NULL)
    {
	if(szFileContents != NULL)
	{
	    DELETE(szFileContents);
	    szFileContents = NULL;
	}
	return -1;
    }
    szObject += (sizeof(OBJECT_STR) - 1);
    //printf("%s\n", szObject);
    for(i = 0; i < 1; i++)
    {
	cVer[i] = *(szObject + i);
    }
    cVer[1] = 0;
    //printf("\nSSL	cVer = %s\n", cVer);
    if(szFileContents != NULL)
    {
	DELETE(szFileContents);
	szFileContents = NULL;
    }
    return 1;
}


/*==========================================================================*
 * FUNCTION : ServiceManager_StartServices
 * PURPOSE  : create all services
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 19:18
 *==========================================================================*/
static BOOL ServiceManager_StartServices(IN OUT SERVICE_MANAGER *pServiceMgr)
{
	int					i;
	APP_SERVICE			*pService;
	APP_SERVICE			*pLastService = NULL;
	BOOL				bResult;
	int iopResult;
	char cVer[5];
	int iCount;

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,//APP_LOG_INFO,
		"Starting %d application services...\n",
		pServiceMgr->nService);

	//�ж��ļ�ϵͳ����begin
	//iopResult = getFileSysVer(cVer);
	//if(iopResult == -1)
	//{//δ�����ļ�ϵͳ����
	//    printf("Get file system version error!\n");
	//    g_SiteInfo.ifileSysType = OLD_FILE_SYS_TYPE;
	//}
	//else if(iopResult == 1)
	//{
	//    if((strcmp((const char*)cVer, "1.00") == 0) || (strcmp((const char*)cVer, "1.01") == 0) ||
	//	(strcmp((const char*)cVer, "1.02") == 0) || (strcmp((const char*)cVer, "1.03") == 0))
	//    {/*�ɵ��ļ�ϵͳ*/
	//	printf("Old file system!\n");
	//	g_SiteInfo.ifileSysType = OLD_FILE_SYS_TYPE;
	//    }
	//    else
	//    {//�µ��ļ�ϵͳ
	//	printf("New file system!\n");
	//	g_SiteInfo.ifileSysType = NEW_FILE_SYS_TYPE;
	//    }
	//}
	//�ж��ļ�ϵͳ����end

	//�ж�SSL��־ begin
	iopResult = getSSLFlag(cVer);
	if(iopResult == -1)
	{//δ����SSL��־
	    printf("Get SSL Flag error!\n");
	    g_SiteInfo.iSSLFlag = NO_SSL;
	}
	else if(iopResult == 1)
	{
	    if((strcmp((const char*)cVer, "N") == 0) || (strcmp((const char*)cVer, "n") == 0))
	    {/*No SSL*/
		//printf("No SSL!\n");
		g_SiteInfo.iSSLFlag = NO_SSL;
	    }
	    else if((strcmp((const char*)cVer, "Y") == 0) || (strcmp((const char*)cVer, "y") == 0))
	    {//Have SSL
		//printf("Have SSL!\n");
		g_SiteInfo.iSSLFlag = HAVE_SSL;
	    }
	    else
	    {
		//printf("No SSL!\n");
		g_SiteInfo.iSSLFlag = NO_SSL;
	    }
	}
	//�ж�SSL��־end

	//�ж�SNMPV3��־ begin
	iopResult = getSNMPV3Flag(cVer);
	if(iopResult == -1)
	{//δ����snmpv3��־
	    printf("Get snmp Flag error!\n");
	    g_SiteInfo.iSNMPV3Flag = NO_SNMPV3;
	}
	else if(iopResult == 1)
	{
	    if((strcmp((const char*)cVer, "N") == 0) || (strcmp((const char*)cVer, "n") == 0))
	    {/*No snmpv3*/
		//printf("No snmpv3!\n");
		g_SiteInfo.iSNMPV3Flag = NO_SNMPV3;
	    }
	    else if((strcmp((const char*)cVer, "Y") == 0) || (strcmp((const char*)cVer, "y") == 0))
	    {//Have snmpv3
		//printf("Have snmpv3!\n");
		g_SiteInfo.iSNMPV3Flag = HAVE_SNMPV3;
	    }
	    else
	    {
		//printf("No snmpv3!\n");
		g_SiteInfo.iSNMPV3Flag = NO_SNMPV3;
	    }
	}
	//�ж�SNMPV3��־end

	pServiceMgr->pRunningCount[0] = 0;

	pService = pServiceMgr->pService;
	SIG_ENUM  enumProtocolType = Service_GetProtocolType(&g_SiteInfo);
	//printf("enumProtocolType = %d\n", enumProtocolType);	
	for (i = 0; i < pServiceMgr->nService; i++, pService++)
	{
		//�����ļ�ϵͳ�����;�������snmpv2_agent.so����snmpv3_agent.so begin
		if(strcmp((const char *)pService->szServiceName, "SNMP_MAIN") == 0)
		{
			if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
			{
				//printf("[Load .so] Use snmpv2_agent.so\n");
				//strcpy(pService->szServiceLib, "snmp_agent.so");
			}
			else
			{
				//printf("[Load .so] Use snmpv3_agent.so\n");
				strcpy(pService->szServiceLib, "snmpv3_agent.so");
			}
		}

		if(strcmp((const char *)pService->szServiceName, "ESR Protocol") == 0)
		{
			if(enumProtocolType != PROTOCOL_EEM)
			{
				continue;
			}
		}
		else if(strcmp((const char *)pService->szServiceName, "YDN_MAIN") == 0)
		{
			if(enumProtocolType != PROTOCOL_YDN23)
			{
				continue;
			}
		}
		else if(strcmp((const char *)pService->szServiceName, "MODBUS_MAIN") == 0)
		{
			if(enumProtocolType != PROTOCOL_MODBUS)
			{
				continue;
			}
		}
		//changed by Frank Wu,N/N/N,20150429, for supporting TL1
		else if(strcmp((const char *)pService->szServiceName, "TL1_MAIN") == 0)
		{
			if(enumProtocolType != PROTOCOL_TL1)
			{
				continue;
			}
		}

		AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
			"Starting #%d service \"%s\"(%s)...\n",
			i+1,
			pService->szServiceName,
			pService->szServiceLib);
		//wait for the former service ready.
		if(i > 0)
		{
			//sleep 20*300ms as max
			for(iCount = 0; iCount < 20; iCount++)
			{
				Sleep(300);
				if(pLastService != NULL)
				{
					if(pLastService->bReadyforNextService)
					{
						AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
								"Starting %s service after %s init finished\n",
								pService->szServiceName,
								pLastService->szServiceName);
						printf("Starting %s service after %s init finished\n",
								pService->szServiceName,
								pLastService->szServiceName);
						break;
					}
				}
			}
		}
		else
		{
			Sleep(2000);
		}
		printf("%s after waiting\n",pService->szServiceName);
		// load and create service
		bResult = Service_Start(pService);

		AppLogOut(SERVICE_MGR, 
			(bResult) ? APP_LOG_UNUSED : APP_LOG_ERROR,
			"#%d service \"%s\" is loaded %s. Lib=%p, Main=%p.\n",
			i+1,
			pService->szServiceName,
			(bResult) ? "OK" : "FAIL",
			pService->hServiceLib,
			pService->pfnServiceMain);

		// the serivce shall be loaded OK, return 
		if (bResult)
		{
			pLastService = pService;
			pServiceMgr->pRunningCount[0]++;
			pServiceMgr->pRunningCount[pService->nRunType]++;
		}
		else if (pService->bMandatorySerice)
		{
			TRACEX("Fails on creating an necessary service, exited.\n");
			return FALSE;
		}
	}
	AppLogOut(SERVICE_MGR, APP_LOG_MILESTONE,//APP_LOG_INFO,
		"Total %d application services have started.\n",
		pServiceMgr->pRunningCount[0]);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ServiceManager_StopServices
 * PURPOSE  : stop all services, kill the service if timeout
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr           : 
 *            IN int                  nTimeToWaitServiceQuit : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 19:37
 *==========================================================================*/
static BOOL ServiceManager_StopServices(IN OUT SERVICE_MANAGER *pServiceMgr,
						   IN int nTimeToWaitServiceQuit) 
{
	int					i, nStoppedTask = 0;
	int					rc;
	APP_SERVICE			*pService;

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,//APP_LOG_INFO,
		"Stopping %d application services...\n",
		pServiceMgr->pRunningCount[0]);

	// 1. send quit cmd to all services at first
	pService = pServiceMgr->pService;
	for (i = 0; i < pServiceMgr->nService; i++, pService++)
	{
		if (pService->bServiceRunning)
		{
			AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
				"Stopping #%d service \"%s\"...\n",
				i+1,
				pService->szServiceName);

			Service_PostQuitMessage(pService);
		}
	}
		
	// 2. wait the service to quit
	pService = pServiceMgr->pService;
	for (i = 0; i < pServiceMgr->nService; i++, pService++)
	{
		BOOL	bRunningThread = pService->bServiceRunning;

		// wait and stop the service. if the thread is NOT running,
		// call this function to cleanup.
		rc = Service_Stop(pService, nTimeToWaitServiceQuit);

		if (bRunningThread) // only log the running service info.
		{
			AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
				"The service \"%s\" %s with code %d.\n",
				pService->szServiceName,
				(rc == ERR_THREAD_OK) ? "exited" : "KILLED",
				pService->args.dwExitCode);

			pServiceMgr->pRunningCount[0]--;
			pServiceMgr->pRunningCount[pService->nRunType]--;
			nStoppedTask++;
		}
	}

	AppLogOut(SERVICE_MGR, APP_LOG_MILESTONE,//APP_LOG_INFO,
		"Total %d application services have been stopped.\n", nStoppedTask);

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : ServiceManager_CheckServices
 * PURPOSE  : check the running status of the services.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 * RETURN   : static int : num of the necessary services which stopped abnormally.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 16:19
 *==========================================================================*/
static int ServiceManager_CheckServices(IN OUT SERVICE_MANAGER *pServiceMgr)
{
	int					i, nStoppedNecessaryServices = 0;
	APP_SERVICE			*pService;

	if (pServiceMgr->pRunningCount[SERVICE_RUN_AS_TASK] == 0)
	{
		return 0;	// do NOT need check.
	}

	pService = pServiceMgr->pService;
	for (i = 0; i < pServiceMgr->nService; i++, pService++)
	{
		if ((pService->pfnServiceMain != NULL) && 
			!Service_IsRunning(pService))	// synchronized call.
		{
			AppLogOut(SERVICE_MGR, APP_LOG_WARNING,
				"The #%d service \"%s\"(%p) has exited "
				"with code %d(%08x).\n",
				i+1,
				pService->szServiceName,
				pService->hServiceThread,
				pService->args.dwExitCode, pService->args.dwExitCode);

			// cleanup.
			Service_Stop(pService, 0);		// synchronized call.			

			if (pService->bMandatorySerice)
			{
				nStoppedNecessaryServices++;
			}
		}
	}
	
	return nStoppedNecessaryServices;
}


/*==========================================================================*
 * FUNCTION : ServiceManager_RunDummies
 * PURPOSE  : execute the services which type is SERVICE_RUN_AS_DUMMY.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 * RETURN   : static BOOL : FALSE for failure on exectute any necessary service.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 16:26
 *==========================================================================*/
static BOOL ServiceManager_RunDummies(IN OUT SERVICE_MANAGER *pServiceMgr)
{
	int					i;
	APP_SERVICE			*pService;


	if (pServiceMgr->pRunningCount[SERVICE_RUN_AS_DUMMY] == 0)
	{
		return TRUE;	// there is no dummy service need to run.
	}


	pService = pServiceMgr->pService;
	for (i = 0; i < pServiceMgr->nService; i++, pService++)
	{
		if ((pService->nRunType == SERVICE_RUN_AS_DUMMY) &&
			(pService->pfnServiceMain != NULL))
		{
			pService->args.dwExitCode = 
				pService->pfnServiceMain(&pService->args);
		}
	}
	
	return TRUE;
}


#define RUN_DUMMY_SERVICE_INTERVAL		1000	// 1sec.

/*==========================================================================*
 * FUNCTION : Main_CheckRunningServices
 * PURPOSE  : 1)check the running service while running, if one or more of the 
 *            mandatory service stopped, exit and to indicate the system 
 *            shall be restart.
 *            2) execute the services which type is SERVICE_RUN_AS_DUMMY.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 13:42
 *==========================================================================*/
static DWORD ServiceManager_Main(IN OUT SERVICE_MANAGER *pServiceMgr)
{
	HANDLE	hSelf = RunThread_GetId(NULL);
	int		nStoppedThreads = 0;
	int		nRunningCount   = 0;

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"The main thread of service manager now started.\n");

	while (THREAD_IS_RUNNING(hSelf))
	{
		RunThread_Heartbeat(hSelf);

		Sleep(RUN_DUMMY_SERVICE_INTERVAL);

		// 1. run the dummy service
		RunThread_Heartbeat(hSelf);
		ServiceManager_RunDummies(pServiceMgr);

		// 2. check the thread service per 30 sec.
		nRunningCount += RUN_DUMMY_SERVICE_INTERVAL;
		if (nRunningCount >= (30*RUN_DUMMY_SERVICE_INTERVAL))
		{
			RunThread_Heartbeat(hSelf);
			nStoppedThreads = ServiceManager_CheckServices(pServiceMgr);
			if (nStoppedThreads != 0)
			{
				AppLogOut(SERVICE_MGR, APP_LOG_ERROR,
					"PANIC! There are %d necessary services have "
					"exited abnormally, service mgr exit now.\n",
					nStoppedThreads);
				break;
			}

			nRunningCount = 0;
		}
	}

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"The main thread of service manager now exited.\n");

	return nStoppedThreads;
}



/*==========================================================================*
 * FUNCTION : ServiceManager_Start
 * PURPOSE  : start the all services and the service manager.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 * RETURN   : BOOL : TRUE for OK
 * COMMENTS : regardless start OK or NOT, the  ServiceManager_Stop shall be
 *            called when exit system
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 20:25
 *==========================================================================*/
BOOL ServiceManager_Start(IN OUT SERVICE_MANAGER *pServiceMgr)
{
	BOOL	bResult = FALSE;

	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"Application manager now is starting...\n");

	ZERO_POBJS(pServiceMgr, 1);

	pServiceMgr->hLock = Mutex_Create(TRUE);
	if (pServiceMgr->hLock == NULL)
	{
		AppLogOut(SERVICE_MGR, APP_LOG_WARNING,
			"Failed to create synchronization lock for service mgr.\n");
	}

	else if (ServiceManager_LoadServices(pServiceMgr)
		&& ServiceManager_StartServices(pServiceMgr))
	{
		pServiceMgr->hServiceManager = RunThread_Create(SERVICE_MGR,
			(RUN_THREAD_START_PROC)ServiceManager_Main, pServiceMgr,
			NULL, 0);

		if (pServiceMgr->hServiceManager != NULL)
		{
			bResult = TRUE;
		}
		else
		{
			AppLogOut(SERVICE_MGR, APP_LOG_WARNING,
				"Application manager fails on creating the main thread.\n");
		}
	}

	AppLogOut(SERVICE_MGR, bResult?APP_LOG_UNUSED:APP_LOG_WARNING,
		"Application manager is %s successfully started.\n",
		bResult ? "now" : "NOT");

	return bResult;
}


/*==========================================================================*
 * FUNCTION : ServiceManager_Stop
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: called when exit or fails on ServiceManager_Start
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 *            IN int nTimeToWaitServiceQuit: unit ms.
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 20:27
 *==========================================================================*/
BOOL ServiceManager_Stop(IN OUT SERVICE_MANAGER *pServiceMgr, 
						 IN int nTimeToWaitServiceQuit)
{
	AppLogOut(SERVICE_MGR, APP_LOG_UNUSED,
		"Application manager now is quiting...\n");

	//1. stop the service manager.
	if (pServiceMgr->hServiceManager != NULL)
	{
		RunThread_Stop(pServiceMgr->hServiceManager,
			nTimeToWaitServiceQuit, TRUE);
	}

	// 2. stop all services
	ServiceManager_StopServices(pServiceMgr, nTimeToWaitServiceQuit);

	// 3.clean up
    ServiceManager_UnloadServices(pServiceMgr);

	if (pServiceMgr->hLock != NULL)
	{
		Mutex_Destroy(pServiceMgr->hLock);
		pServiceMgr->hLock = NULL;
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Main_FrameJudger
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                  iSenderType   : use SENDER_TYPE enum const
 *            const unsigned char  *sFrameData   : 
 *            int                  iFrameDataLen : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : ACUTeam                   DATE: 2004-11-20 15:21
 *==========================================================================*/
//void Main_FrameJudger(int iSenderType, 
//					  const unsigned char *sFrameData, 
//					  int iFrameDataLen)
//{
//	/* to do: Macro defined in each service may be removed */
//	UNUSED(iSenderType);
//	UNUSED(sFrameData);
//	UNUSED(iFrameDataLen);
//
//	//FRAME_TYPE iFrameType;
//	//int iEsrProtocolType;
//
//	//switch (iSenderType)
//	//{
//	//case SENDER_ESR:
//	//	{
//	//		/* to do: judge if a frame can handled by Debug module */
//	//	}
//	//	break;
//
//	//case SENDER_DEBUGER:
//	//	/* to do: get ESR common config: iProtocolType */
//	//	iFrameType = ESR_AnalyseFrame(sFrameData, iFrameDataLen);
//
//	//	if ((iEsrProtocolType == EEM && iFrameType == SELECT_OK) || 
//	//			(iEsrProtocolType != EEM && iFrameType == FRAME_ENQ))
//	//	{
//	//		/* to do: stop Debuger, start ESR service */
//	//	}
//	//	break;
//
//	//default:
//	//	break;
//	//}
//
//	return;
//}
/*==========================================================================*
 * FUNCTION : Service_GetProtocolType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: Site_Info 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Marco                    DATE: 2013-6-28 10:03
 *==========================================================================*/
/*==========================================================================*
 * FUNCTION : *Init_GetEquipRefById
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite   : 
 *            IN int            nEquipID : 
 * RETURN   : EQUIP_INFO : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-31 20:25
 *==========================================================================*/
EQUIP_INFO *Service_GetEquipRefById(IN OUT SITE_INFO *pSite, 
										  IN int nEquipID)
{
	EQUIP_INFO *pEquip;
	int		n;

	if (nEquipID <= 0)
	{
		return NULL;
	}
	// quikly search the ID. The ID start from 1.
	if (nEquipID <= pSite->iEquipNum)
	{
		pEquip = &pSite->pEquipInfo[nEquipID-1];
		if( pEquip->iEquipID == nEquipID)
		{
			return pEquip;
		}
	}

	// the ID is not equal to the INDEX, compare the ID one by one
	pEquip = pSite->pEquipInfo;
	for (n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		if (pEquip->iEquipID == nEquipID)
		{
			return pEquip;
		}
	}
	
	return NULL;	// not found.
}

static void *Service_GetEquipSigRefById(IN EQUIP_INFO *pEquip, 
								IN int nSigType, IN int nSigId)
{
	void	*pFoundSig = NULL;

	switch (nSigType)
	{
	case SIG_TYPE_SAMPLING:
		{
			SERV_GET_SIG_VALUE_BY_ID(SAMPLE_SIG_VALUE,
				pEquip->pSampleSigValue,
				pEquip->pStdEquip->iSampleSigNum,
				nSigId,
				pFoundSig);
		}
		break;

	case SIG_TYPE_CONTROL:
		{
			SERV_GET_SIG_VALUE_BY_ID(CTRL_SIG_VALUE,
				pEquip->pCtrlSigValue,
				pEquip->pStdEquip->iCtrlSigNum,
				nSigId,
				pFoundSig);
		}
		break;

	case SIG_TYPE_SETTING:
		{
			SERV_GET_SIG_VALUE_BY_ID(SET_SIG_VALUE,
				pEquip->pSetSigValue,
				pEquip->pStdEquip->iSetSigNum,
				nSigId,
				pFoundSig);
		}
		break;

	case SIG_TYPE_ALARM:
		{
			SERV_GET_SIG_VALUE_BY_ID(ALARM_SIG_VALUE,
				pEquip->pAlarmSigValue,
				pEquip->pStdEquip->iAlarmSigNum,
				nSigId,
				pFoundSig);
		}
		break;
	}

	if (pFoundSig == NULL)
	{
		/*AppLogOut(EQP_INIT, APP_LOG_UNUSED, "Failed to find signal[Type:%d,ID:%d] "
			"in equipment \"%s\"(%d).\n",
			nSigType, nSigId,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);*/
	}
    
	return pFoundSig;
}




static int Service_GetProtocolType(IN SITE_INFO *pSite)
{
#define SYSTEM_UNIT				1
#define SIG_ID_PROTOCOL_TYPE			457

	EQUIP_INFO	*pEquip = NULL;
	SIG_BASIC_VALUE *pSigProtocolType = NULL;
	
	pEquip = Service_GetEquipRefById(pSite, SYSTEM_UNIT);
	pSigProtocolType = (SIG_BASIC_VALUE *)Service_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,SIG_ID_PROTOCOL_TYPE);
	
	if(pSigProtocolType)
	{
		return pSigProtocolType->varValue.enumValue;
	}

	return PROTOCOL_EEM;
}
/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : sig_handler.h
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-17 16:32
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _SIG_HANDLER_H_DEF_
#define _SIG_HANDLER_H_DEF_

/*==========================================================================*
 * FUNCTION : Sig_InstallHandlers
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  bIgnoreOthers   : 
 *            void  *pfnSigHandler1 : 
 *            int   nSigNo1         : 
 *                  ...             : 
 * RETURN   : int : 
 * COMMENTS : Sig_InstallHandlers(handler1, sig1, sig2, ..., 0,
								  handle2, sig3, sig4, ..., 0,
								  0/NULL/end); 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-17 17:34
 *==========================================================================*/
int Sig_InstallHandlers(BOOL bIgnoreOthers, void *pfnSigHandler1, int nSigNo1, ...);

#endif //ifdef _SIG_HANDLER_H_DEF_

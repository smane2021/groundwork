/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : main.h
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 20:05
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __MAIN_H_
#define __MAIN_H_

#define SYS_MAIN	"SYS MAIN"

struct SMainArgs
{				
	SITE_INFO	*pSite;
};
typedef struct SMainArgs MAIN_ARGS;


typedef BOOL (*INIT_MODULE_PROC)(void *pArg1, void *pArg2);
typedef void (*EXIT_MODULE_PROC)(void *pArg1, void *pArg2);

#define MAX_INIT_EXIT_ARGS		2

// the init and exit modules shall be called in main.
struct _SYS_INIT_EXIT_MODULE
{
	char				*pszModuleName;
	BOOL				bMandatoryModule;	// must be init OK.
	DWORD				dwErrorCode;		// return errorcode on init error.

	INIT_MODULE_PROC	pfnInit;
	void				*pInitArgs[MAX_INIT_EXIT_ARGS];

	EXIT_MODULE_PROC	pfnExit;
	void				*pExitArgs[MAX_INIT_EXIT_ARGS];
};
typedef struct _SYS_INIT_EXIT_MODULE	SYS_INIT_EXIT_MODULE;


#define DEF_INIT_EXIT_MODULE(moduleName, bNecessary, errCode,				\
							 initProc, initArg1, initArg2,					\
	  						 exitProc, exitArg1, exitArg2)					\
{																			\
	(char *)(moduleName), (bNecessary),	(DWORD)(errCode),					\
	(INIT_MODULE_PROC)(initProc), {(void *)(initArg1), (void *)(initArg2)},	\
	(EXIT_MODULE_PROC)(exitProc), {(void *)(exitArg1), (void *)(exitArg2)},	\
}


/*==========================================================================*
 * FUNCTION : ServiceManager_Start
 * PURPOSE  : start the all services and the service manager.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 * RETURN   : BOOL : TRUE for OK
 * COMMENTS : regardless start OK or NOT, the  ServiceManager_Stop shall be
 *            called when exit system
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 20:25
 *==========================================================================*/
BOOL ServiceManager_Start(IN OUT SERVICE_MANAGER *pServiceMgr);

/*==========================================================================*
 * FUNCTION : ServiceManager_Stop
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: called when exit or fails on ServiceManager_Start
 * ARGUMENTS: IN OUT SERVICE_MANAGER  *pServiceMgr : 
 *            IN int nTimeToWaitServiceQuit: unit ms.
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-30 20:27
 *==========================================================================*/
BOOL ServiceManager_Stop(IN OUT SERVICE_MANAGER *pServiceMgr, 
						 IN int nTimeToWaitServiceQuit);

// watch dog.
BOOL WatchDog_Open(void);
void WatchDog_Feed(void);
void WatchDog_Close(BOOL bDisableDog);
int	Main_ProcessCmdLine(IN int argc, IN char *argv[]);
BOOL Main_CheckStopCmd(void);
void Main_AckStopCmd(void);

#endif	// __MAIN_H_


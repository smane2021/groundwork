/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : watch_dog.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 16:04
 *  VERSION  : V1.00
 *  PURPOSE  : function to use watch dog
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "basetypes.h"
#include "applog.h"

static int s_hWatchDog = -1;

#define WATCH_DOG_MAX_FEED_PERIOD		10	//10 seconds.
#define WATCH_DOG_NAME					"/dev/wd"

#ifdef _DEBUG
#warning ">>>>>>>>>>>>>>>>>> The watch dog is NOT enabled."
#undef _HAS_WATCH_DOG

#else
#define _HAS_WATCH_DOG		1
#endif

#define WATCH_DOG	"WATCH DOG"


//#define IOCTL_EN_WD		0x6b01
//#define IOCTL_DIS_WD		0x6b02
//#define IOCTL_FEED_WD		0x6b04
//#define IOCTL_SET_TIMER		0x6b10
#define WDOG_MAJOR			241 
#define IOCTL_EN_WD		_IOW(WDOG_MAJOR, 1, unsigned long)
#define IOCTL_DIS_WD		_IOW(WDOG_MAJOR, 2, unsigned long)
#define IOCTL_FEED_WD		_IOW(WDOG_MAJOR, 3, unsigned long)
#define IOCTL_SET_TIMER		_IOW(WDOG_MAJOR, 4, unsigned long)
void WatchDog_Feed(void);
/*==========================================================================*
 * FUNCTION : WatchDog_Open
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL: TRUE for OK : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 16:13
 *==========================================================================*/
BOOL WatchDog_Open(void)
{
	if (s_hWatchDog != -1)
	{
		return TRUE;	// is already opened.
	}

#ifdef _HAS_WATCH_DOG
	TRACEX("Opening watch dog...\n");

	s_hWatchDog = open(WATCH_DOG_NAME, O_RDWR);
	if(s_hWatchDog < 0)
	{
		return FALSE;
	}

	// init the maximum alllowed feed period in seconds.
	ioctl(s_hWatchDog, IOCTL_SET_TIMER, WATCH_DOG_MAX_FEED_PERIOD);

	ioctl(s_hWatchDog, IOCTL_EN_WD);
#else
	s_hWatchDog = 1;
#endif //_HAS_WATCH_DOG

	WatchDog_Feed();

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : WatchDog_Feed
 * PURPOSE  : feed the opeded dog. the interval of the two feeding time must
 *            be less than WATCH_DOG_MAX_FEED_PERIOD seconds.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 16:21
 *==========================================================================*/
void WatchDog_Feed(void)
{
	if (s_hWatchDog == -1)
	{
		return;
	}
#ifdef _HAS_WATCH_DOG
	
	ioctl(s_hWatchDog, IOCTL_EN_WD);
	ioctl(s_hWatchDog, IOCTL_FEED_WD);	
	
#else
	//TRACEX("Clear watch dog...\n");
	//printf(".");fflush(stdout);
#endif
	return;
}



/*==========================================================================*
 * FUNCTION : WatchDog_Close
 * PURPOSE  : close the watch dog.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  bDisableDog : TRUE to disable the dog. if FALSE, the dog
 *            will restart system due to being not feeded
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 16:23
 *==========================================================================*/
void WatchDog_Close(BOOL bDisableDog)
{
	if(s_hWatchDog == -1)
	{
		return;
	}

#ifdef _HAS_WATCH_DOG
	TRACEX("Closing watch dog with flag %d...\n", bDisableDog);

	if (bDisableDog)
	{
		ioctl(s_hWatchDog, IOCTL_DIS_WD);
	}

	close(s_hWatchDog);
#else
	UNUSED(bDisableDog);
#endif

	s_hWatchDog = -1;
	return;
}


//open to test wdt
//#define _TEST_WATCH_DOG

#ifdef _TEST_WATCH_DOG
#include <signal.h>

static int s_nRunning = 1;
static void QuitSignalHandler( int a )
{
    if( s_nRunning > 0)
    {
		printf( "WDT: Receives a quit signal. Quiting ... \007\007\n" );
        s_nRunning = 0;
    } 
}


int main(void)
{
	int i = 0;

	signal(SIGINT, QuitSignalHandler);

	printf("Open watch dog...\n");

	if (!WatchDog_Open())
	{
		printf("Fails on opening and enabling the watch-dog.\n");
	}

	printf( "Open Watch dog ok.  CTRL+C to quit...\n");

	while(s_nRunning)
	{
		i++;
		printf("%d: Feeding dog...\n", i);
		WatchDog_Feed();
		usleep(100000);
	}

	printf("Press '0' to enable watchdog before exiting, other disable.\n");
	scanf("%d", &i);
	if (i==0)
	{
		printf ("The WDT is enabled. wait for system restart...\n");
	}	

	WatchDog_Close(i ? 1 : 0);

	return 0;
}
#endif

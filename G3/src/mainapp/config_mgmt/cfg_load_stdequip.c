/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cfg_load_stdequip.c
 *  CREATOR  : LinTao                   DATE: 2004-09-23 17:11
 *  VERSION  : V1.00
 *  PURPOSE  : to load standard equip config files
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

/* for toupper function */
#include <ctype.h> 
#include <values.h>

#include "cfg_mgmt.h"
#include "cfg_helper.h"


/* assitant functions */
static BOOL IsBoundChr_S(int c)
{
	return ((char)c == '[' || (char)c == ']');
}

static BOOL IsBoundChr_ID(int c)
{
	return ((char)c == 'I' || (char)c == 'D');
}

/*==========================================================================*
 * FUNCTION : ParseSampleSigTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN  char                *szBuf     : 
 *            OUT SAMPLE_SIG_INFO *  pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-16 17:00
 *==========================================================================*/
static int ParseSampleSigTableProc(IN char *szBuf, OUT SAMPLE_SIG_INFO * pStructData)
{
	char *pField, *pSubField, *pSubSubField;
	int  iSubFieldCount, iSubSubFieldCount,iCurIndex;
	int  iResourceID;	/* language resource id */
	int  ret;

	LANG_FILE *pLangFile;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Sig ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSampleSigTableProc: ERROR: Sig ID is not a "
			"number!\n", __FILE__);
		return 1;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

    //TRACE("Sig ID field:%s | ",pField);

	/* 2.Sig Name field, just jump it */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

    //TRACE("Sig Name field:%s | ",pField);

	/* 3.Sig Name Resource ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSampleSigTableProc: ERROR: Sig Name Resource "
			"ID is not a number!\n", __FILE__);
		return 3;    /* not a num, error */
	}
	iResourceID = atoi(pField);
	pLangFile = g_SiteInfo.LangInfo.pCurLangFile;
	pStructData->pSigName = GetLangText(iResourceID, pLangFile->iLangTextNum, 
		pLangFile->pLangText);
	if (pStructData->pSigName == NULL)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSampleSigTableProc: ERROR: Parse Sig Name "
			"Resource ID failed!\n", __FILE__);
		return 3;
	}
    //TRACE("Resource ID:%s | ",pField);
	/* 4.Sig Unit field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->szSigUnit = NEW(char, 1);
		if (pStructData->szSigUnit == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			return 4;
		}
		*(pStructData->szSigUnit) = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Sig Unit is "
				"invalid!\n", __FILE__);
			return 4;
		}
		pStructData->szSigUnit = NEW_strdup_f(pField, Cfg_ReplaceIllegalChr);

		if (pStructData->szSigUnit == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: no memory to "
				"load Sig Unit info!\n", __FILE__);
			return 4;  
		}
	}
    //TRACE("Sig Unit field:%s | ",pField);
	/* 5.Index of Sampler List field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0' || *pField > '9') && 
		*pField != '-')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSampleSigTableProc: ERROR: Index of Sampler list"
			" is not an invalid number!\n", __FILE__);
		return 5;    /* not a num, error */
	}
	/* for negative number, only -1 is valid */
	if (*pField == '-' && *(pField + 1) != '1')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSampleSigTableProc: ERROR: Index of Sampler list"
			" is not an invalid number!\n", __FILE__);
		return 5;    /* not a num, error */
	}
	pStructData->iIndexofSamplerList = atoi(pField);

    //TRACE("Index of Sampler List field:%s | ",pField);

	/* 6.Sampler Chnanel ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '-')   /* -1,-2,-3 is valid */
	{
		if (*(pField+1) < '1' || *(pField+1) > '3') /* now pField+1 is not NULL */
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Sampler Chnanel"
				" ID is invalid!\n", __FILE__);
			return 6;    /* not a num, error */
		}
	}
	else 
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Sampler Chnanel"
				" ID is invalid!\n", __FILE__);
			return 6;    /* not a num, error */
		}
	}
	pStructData->iSamplerChannel = atoi(pField);

	if (pStructData->iIndexofSamplerList == -1 &&
		pStructData->iSamplerChannel > 0)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSampleSigTableProc: ERROR: index of sampler list "
			"and channel no. is inconsistent.\n", __FILE__);
		return 6;
	}

     //TRACE("Sampler Chnanel ID:%s | ",pField);

	/* 7.Sig Value Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	*pField = toupper(*pField);
	/* valid type: long(L), float(F), unsigned long(U), date/time(T), Enum(E) */
	if (*pField != 'L' && *pField != 'F' && *pField != 'U' &&
		*pField != 'T' && *pField != 'E' )
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSampleSigTableProc: ERROR: Sig Value Type "
			"is invalid!\n", __FILE__);
		return 7;
	}
	switch(*pField)
	{
	case 'L':
		pStructData->iSigValueType = VAR_LONG;
		break;
	case 'F':
		pStructData->iSigValueType = VAR_FLOAT;
		break;
	case 'U':
		pStructData->iSigValueType = VAR_UNSIGNED_LONG;
		break;
	case 'T':
		pStructData->iSigValueType = VAR_DATE_TIME;
		break;
	case 'E':
		pStructData->iSigValueType = VAR_ENUM;
		break;
	default:
		TRACE("[%s]--Line: %d : Should not go here!\n", __FILE__, __LINE__);
		ASSERT(0);
	}

    //TRACE("Sig Value Type field:%s | ",pField);

	/* 8.Storing Threshold field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->fStoringThreshold = 0;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Storing Threshold"
				" is not a number!\n", __FILE__);
			return 8;    /* not a num, error */
		}
		pStructData->fStoringThreshold = atof(pField);
	}

    //TRACE("Storing Threshold:%s | ",pField);

	/* 9.Storing Interval field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iStoringInterval = 0;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Storing Interval "
				"is not a number!\n", __FILE__);
			return 9;    /* not a num, error */
		}
#define STORING_INTERVEL_MINUTE			60		
		pStructData->iStoringInterval = atof(pField) * STORING_INTERVEL_MINUTE; //Store every minute
	}

    //TRACE("Storing Interval:%s | ",pField);

	/* 10.Evaluation Expression field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iCalculateExpression = 0;
	}
	else
	{
		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		
		if (ppExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: "
				"No memory!\n", __FILE__);
			return ERR_CFG_NO_MEMORY;
		}

		ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iCalculateExpression,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: "
				"Evaluation Expression is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 10;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 10;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: EXP_MAX_ELEMENTS "
				"macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 10;
		}

		pStructData->pCalculateExpression = NEW(EXP_ELEMENT,
			pStructData->iCalculateExpression);
		if (pStructData->pCalculateExpression == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: "
				"No memory!\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iCalculateExpression, ppExp);

			return ERR_CFG_NO_MEMORY;
		}

		Cfg_DumpExp(pStructData->pCalculateExpression,
			ppExp,
			pStructData->iCalculateExpression);		
	}
    //TRACE("Evaluation Expression:%s | ",pField);
	/* 11.Value Range field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		switch (pStructData->iSigValueType)
		{
			case VAR_ENUM:
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSampleSigTableProc: Value Range field "
					"should not be empty for Enum!\n", __FILE__);
				return 11;

			case VAR_LONG:     
				pStructData->fMinValidValue = MINLONG;
				pStructData->fMaxValidValue = MAXLONG;
				break;

			case VAR_FLOAT:
				pStructData->fMinValidValue = ACU_MINFLOAT;
				pStructData->fMaxValidValue = MAXFLOAT;
				break;

			case VAR_DATE_TIME:  //deal it as VAR_UNSIGNED_LONG
			case VAR_UNSIGNED_LONG:
				pStructData->fMinValidValue = 0;
				pStructData->fMaxValidValue = (unsigned long)-1;
				break;

			default:
				break;
		}
		 
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: Value Range "
				"field is NULL!\n", __FILE__);
			return 11; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');

		/* for enum value, use [ID1=... ]/[ID2=...]/../[IDn=...] format */
		if (pStructData->iSigValueType == VAR_ENUM) 
		{
			pStructData->iStateNum = iSubFieldCount;
			pStructData->pStateText = NEW(LANG_TEXT *, iSubFieldCount);
			if (pStructData->pStateText == NULL)
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSampleSigTableProc: ERROR: no memory for State Text"
					" pointer array.\n", __FILE__);
				pStructData->iStateNum = 0;
				return 11;
			}
			iCurIndex = 0;
			do
			{
				pField = Cfg_SplitStringEx(pField, &pSubField, '/');
				/* get rid of '[' and ']' */
				pSubField = Cfg_RemoveBoundChr(pSubField, IsBoundChr_S);

				iSubSubFieldCount = Cfg_GetSplitStringCount(pSubField, '=');
				if (iSubSubFieldCount < 2)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseSampleSigTableProc: ERROR: Value Range "
						"format for enum value is invalid(missing \'=\')!\n",
						__FILE__);
					return 11;
				}

				/* get 'IDn' string */
				Cfg_SplitStringEx(pSubField, &pSubSubField, '=');
				/* remove 'ID' character */
				pSubSubField = Cfg_RemoveBoundChr(pSubSubField, IsBoundChr_ID);
				pSubSubField = Cfg_RemoveWhiteSpace(pSubSubField);
				if (*pSubSubField < '0' || *pSubSubField > '9')
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseSampleSigTableProc: ERROR: Value "
						"Range format for enum value is invalid(resource id "
						"is not a number)!\n", __FILE__);
					return 11;
				}

				/* now resource id has been got */
				iResourceID = atoi(pSubSubField);
				pLangFile = g_SiteInfo.LangInfo.pCurLangFile;
				(pStructData->pStateText)[iCurIndex] = 
					GetLangText(iResourceID, pLangFile->iLangTextNum, 
					pLangFile->pLangText );

				if ((pStructData->pStateText)[iCurIndex] == NULL)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--%s: ERROR: Parse No.%d of Enum Value Name "
						"Resource IDs failed!\n", __FILE__, __FUNCTION__, 
						iCurIndex+1);
					return 11;
				}

			}
			while (++iCurIndex < iSubFieldCount);

			pStructData->fMinValidValue = 0;
			pStructData->fMaxValidValue = pStructData->iStateNum - 1;
		} /* end of enum value format */

		else  /* for non-enum value, use: minValue/maxValue format */
		{
			if (iSubFieldCount != 2)
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSampleSigTableProc: ERROR: Value "
					"Range field is invalid (for non-enum value, should be "
					"minValue/maxValue format)!\n", __FILE__);
				return 11;
			}

			/* get minValue of the range */
			pField = Cfg_SplitStringEx(pField, &pSubField, '/');
			if ((*pSubField < '0' || *pField > '9') && *pSubField != '-')
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSampleSigTableProc: ERROR: minValue "
					"of Value Range field is not a number!\n", __FILE__);
				return 11;
			}
			pStructData->fMinValidValue = atof(pSubField);

			/* get maxValue of the range */
			Cfg_SplitStringEx(pField, &pSubField, '/');
			if ((*pSubField < '0' || *pField > '9') && *pSubField != '-')
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSampleSigTableProc: ERROR: maxValue "
					"of Value Range field is not a number!\n", __FILE__);
				return 11;
			}
			pStructData->fMaxValidValue = atof(pSubField);
		} /* end of minValue/maxValue format process */
	} /* end of not empty field process */

    //TRACE("Value Range:%s | ",pField);

	/* 12.Value Display Attribute field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

    //TRACE("Value Display Attribute:%s\n",pField);

	if (Cfg_IsEmptyField(pField))
	{
		pStructData->byValueDisplayAttr = DISPLAY_NONE;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Value Display"
				" Attr is invalid!\n", __FILE__);
			return 12; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');
		if (iSubFieldCount > 2)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Value Display"
				" Attr is invalid(too many)!\n", __FILE__);
			return 12; 
		}

		pField = Cfg_SplitStringEx(pField, &pSubField, '/');
		toupper(*pSubField);
		if (*pSubField == 'W')
		{
			pStructData->byValueDisplayAttr = DISPLAY_WEB;
		}
		else if (*pSubField == 'L')
		{
			pStructData->byValueDisplayAttr = DISPLAY_LCD;
		}
		else
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Value Display"
				" Attr is invalid!\n", __FILE__);
			return 12; 
		}

		if (iSubFieldCount == 2)     /* may have W/L attibute */
		{
			Cfg_SplitStringEx(pField, &pSubField, '/');
			if (*pSubField == 'W')
			{
				pStructData->byValueDisplayAttr |= DISPLAY_WEB;
			}
			else if (*pSubField == 'L')
			{
				pStructData->byValueDisplayAttr |= DISPLAY_LCD;
			}
			else
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSampleSigTableProc: ERROR: Value Display"
					" Attr is invalid!\n", __FILE__);
				return 12; 
			}
		}
	}
    //TRACE("%d\n",pStructData->byValueDisplayAttr);
	/* 13.Value Display Level field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iValueDisplayLevel = ALARM_LEVEL_NONE;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Value Display"
				" Level is NULL!\n", __FILE__);
			return 13;   
		}

		if (*(pField + 1) != 'A')   /* (*pField) is not '\0', so pField+1 is not NULL */
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Value Display"
				" Level is invalid!\n", __FILE__);
			return 13;   
		}

		switch (*pField)
		{
		case 'N':			/* NA */
			pStructData->iValueDisplayLevel = ALARM_LEVEL_NONE;
			break;

		case 'O':			/* OA */
			pStructData->iValueDisplayLevel = ALARM_LEVEL_OBSERVATION;
			break;

		case 'M':			/* MA */
			pStructData->iValueDisplayLevel = ALARM_LEVEL_MAJOR;
			break;

		case 'C':			/* CA */
			pStructData->iValueDisplayLevel = ALARM_LEVEL_CRITICAL;
			break;

		default:
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Value Display"
				" Level is invalid!\n", __FILE__);
			return 13;
		}
	}

	/* 14.Value Display ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSampleSigTableProc: ERROR: Value Display ID"
			" is not a number!\n", __FILE__);
		return 14;    /* not a num, error */
	}
	pStructData->iValueDisplayID = atoi(pField);

	/* 15.Value Display format field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->szValueDisplayFmt = NEW(char, 1);
		if (pStructData->szValueDisplayFmt == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			return 15;
		}
		*pStructData->szValueDisplayFmt = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Value Display"
				" format is NULL!\n", __FILE__);
			return 15;     
		}
		pStructData->szValueDisplayFmt = NEW_strdup(pField);
		if (pStructData->szValueDisplayFmt == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: no memory to"
				" load Value Diaplsy Format info!\n", __FILE__);
			return 15;     
		}
	}
	
	/* 16.Display Expression field */
	//TRACE("***%s, %s************\n", pStructData->pSigName->pAbbrName[1], szBuf);

	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iDispExp = 0;
	}
	else
	{
		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
	
		if (ppExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: no memory for Display"
				" Expression pointer array.\n", __FILE__);
			return 12;
		}

	    ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iDispExp,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: Display Expression"
				" is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: EXP_MAX_ELEMENTS"
				" macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;
		}

		//
		pStructData->pDispExp = NEW(EXP_ELEMENT, pStructData->iDispExp);
		if (pStructData->pDispExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSampleSigTableProc: ERROR: no memory for Display"
				" Expression pointer array.\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iDispExp, ppExp);

			return 12;
		}

		Cfg_DumpExp(pStructData->pDispExp,
			ppExp,
			pStructData->iDispExp);		
	}
	return 0;
}

#define CLEAR_CFG_ON_CTRL(_pOnCtrl)  \
	((_pOnCtrl)->expVar.iRelevantEquipIndex = -1)


/*==========================================================================*
 * FUNCTION : Cmp_CtrlSigExp
 * PURPOSE  : compare the exp by descending order
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN CTRL_EXPRESSION  *pCtlExp1 : 
 *            IN CTRL_EXPRESSION  *pCtlExp2 : 
 * RETURN   : static int : 0: exp1==exp2, 1: exp1<exp2, -1: exp1>exp2
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-03 11:12
 *==========================================================================*/
static int Cmp_CtrlSigExp(IN CTRL_EXPRESSION  *pCtlExp1, 
						  IN CTRL_EXPRESSION  *pCtlExp2)
{
	return pCtlExp1->fThreshold < pCtlExp2->fThreshold ? 1  
		:  pCtlExp1->fThreshold > pCtlExp2->fThreshold ? -1 
		: 0;
}


/*==========================================================================*
 * FUNCTION : ParseControlSigTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char            *szBuf       : 
 *            OUT CTRL_SIG_INFO  *pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 10:14
 *==========================================================================*/
static int ParseControlSigTableProc(IN char *szBuf, OUT CTRL_SIG_INFO *pStructData)
{
	char *pField, *pSubField, *pSubSubField,*pResourceIDString;
	int  iSubFieldCount, iSubSubFieldCount,iCurIndex;
	int  iResourceID, iStrLen;	/* iResourceID:language resource id */
	int  ret;
	CTRL_EXPRESSION  *pCtlExp;

	LANG_FILE *pLangFile;
	pLangFile = g_SiteInfo.LangInfo.pCurLangFile;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Sig ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseControlSigTableProc: ERROR: Sig ID "
			"is not a number!\n", __FILE__);
		return 1;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 2.Sig Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 3.Sig Name Resource ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseControlSigTableProc: ERROR: Sig Name Resource"
			" ID is not a number!\n", __FILE__);
		return 3;    /* not a num, error */
	}
	iResourceID = atoi(pField);
	pStructData->pSigName = GetLangText(iResourceID, pLangFile->iLangTextNum, 
		pLangFile->pLangText);
	if (pStructData->pSigName == NULL)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseControlSigTableProc: ERROR: Parse Sig Name "
			"Resource ID failed!\n", __FILE__);
		return 3;
	}

	/* 4.Sig Unit field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->szSigUnit = NEW(char, 1);
		if (pStructData->szSigUnit == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory!"
				"\n", __FILE__);
			return 4;
		}
		*(pStructData->szSigUnit) = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Sig Unit "
				"is invalid!\n", __FILE__);
			return 4;
		}
		pStructData->szSigUnit = NEW_strdup_f(pField, Cfg_ReplaceIllegalChr);

		if (pStructData->szSigUnit == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory "
				"to load Sig Unit info!\n", __FILE__);
			return 4;  
		}
	}

	/* 5.Index of Sampler list field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0' || *pField > '9') && 
		*pField != '-')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseControlSigTableProc: ERROR: Index of Sampler"
			" list is not an invalid number!\n", __FILE__);
		return 5;    /* not a num, error */
	}
	/* for negative number, only -1 is valid */
	if (*pField == '-' && *(pField + 1) != '1')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseControlSigTableProc: ERROR: Index of Sampler"
			" list is not an invalid number!\n", __FILE__);
		return 5;    /* not a num, error */
	}
	pStructData->iIndexofSamplerList = atoi(pField);

	/* 6.Sampler Channel ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iSamplerChannel = -3;
	}
	else
	{
		if (*pField == '-')   /* -1,-2,-3 is valid */
		{
			if (*(pField+1) < '1' || *(pField+1) > '4') /* now pField+1 is not NULL */
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--%s: ERROR: Sampler Chnanel"
					" ID is invalid!\n", __FILE__, __FUNCTION__);
				return 6;    /* not a num, error */
			}
		}
		else 
		{
			if ((*pField < '0') || (*pField > '9'))
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--%s: ERROR: Sampler Chnanel"
					" ID is invalid!\n", __FILE__, __FUNCTION__);
				return 6;    /* not a num, error */
			}
		}
		pStructData->iSamplerChannel = atoi(pField);

		if (pStructData->iIndexofSamplerList == -1 &&
			pStructData->iSamplerChannel > 0)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: index of sampler list"
				" and channel no. is inconsistent.\n",
				__FILE__, __FUNCTION__);
			return 6;
		}
	}

	/* 7.Sig value type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	*pField = toupper(*pField);
	/* valid type: long(L), float(F), unsigned long(U), date/time(T), Enum(E) */
	if (*pField != 'L' && *pField != 'F' && *pField != 'U' &&
		*pField != 'T' && *pField != 'E' )
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseControlSigTableProc: ERROR: Sig Value Type"
			" is invalid!\n", __FILE__);
		return 7;
	}
	switch(*pField)
	{
	case 'L':
		pStructData->iSigValueType = VAR_LONG;
		break;
	case 'F':
		pStructData->iSigValueType = VAR_FLOAT;
		break;
	case 'U':
		pStructData->iSigValueType = VAR_UNSIGNED_LONG;
		break;
	case 'T':
		pStructData->iSigValueType = VAR_DATE_TIME;
		break;
	case 'E':
		pStructData->iSigValueType = VAR_ENUM;
		break;
	default:					/* can not go to here */
		TRACE("Should not go here!\n");
		ASSERT(0);
	}

	/* 8.Value range field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		switch (pStructData->iSigValueType)
		{
			case VAR_ENUM:
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: Value Range field "
					"should not be empty for Enum!\n", __FILE__);
				return 8;

			case VAR_LONG:     
				pStructData->fMinValidValue = MINLONG;
				pStructData->fMaxValidValue = MAXLONG;
				break;

			case VAR_FLOAT:
				pStructData->fMinValidValue = ACU_MINFLOAT;
				pStructData->fMaxValidValue = MAXFLOAT;
				break;

			case VAR_DATE_TIME:  //deal it as VAR_UNSIGNED_LONG
			case VAR_UNSIGNED_LONG:
				pStructData->fMinValidValue = 0;
				pStructData->fMaxValidValue = (unsigned long)-1;
				break;

			default:
				break;
		}
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Value Range "
				"field is NULL!\n", __FILE__);
			return 8; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');

		/* for enum value, use [ID1=... ]/[ID2=...]/../[IDn=...] format */
		if (pStructData->iSigValueType == VAR_ENUM) 
		{
			pStructData->iStateNum = iSubFieldCount;
			pStructData->pStateText = NEW(LANG_TEXT *, iSubFieldCount);
			if (pStructData->pStateText == NULL)
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: ERROR: no memory for "
					"State Text pointer array.\n", __FILE__);
				pStructData->iStateNum = 0;
				return 8;
			}
			iCurIndex = 0;
			do
			{
				pField = Cfg_SplitStringEx(pField, &pSubField, '/');
				/* get rid of '[' and ']' */
				pSubField = Cfg_RemoveBoundChr(pSubField, IsBoundChr_S);

				iSubSubFieldCount = Cfg_GetSplitStringCount(pSubField, '=');
				if (iSubSubFieldCount < 2)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseControlSigTableProc: ERROR: Value "
						"Range format for enum value is invalid"
						"(missing \'=\')!\n", __FILE__);
					return 8;
				}

				/* get 'IDn' string */
				Cfg_SplitStringEx(pSubField, &pSubSubField, '=');
				/* remove 'ID' character */
				pSubSubField = Cfg_RemoveBoundChr(pSubSubField, IsBoundChr_ID);
				pSubSubField = Cfg_RemoveWhiteSpace(pSubSubField);
				if (*pSubSubField < '0' || *pSubSubField > '9')
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseControlSigTableProc: ERROR: Value "
						"Range format for enum value is invalid"
						"(resource id is not a number)!\n", __FILE__);
					return 8;
				}

				/* now resource id has been got */
				iResourceID = atoi(pSubSubField);
				(pStructData->pStateText)[iCurIndex] = 
					GetLangText(iResourceID, pLangFile->iLangTextNum, 
					pLangFile->pLangText);

				if ((pStructData->pStateText)[iCurIndex] == NULL)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--%s: ERROR: Parse No.%d of Enum Value Name "
						"Resource IDs failed!\n", __FILE__, __FUNCTION__, 
						iCurIndex+1);
					return 8;
				}

			}
			while (++iCurIndex < iSubFieldCount);

			/* for single-value control signal, should always be "1" */
			if (pStructData->iStateNum == 1)
			{
				pStructData->fMinValidValue = 1;
				pStructData->fMaxValidValue = 1;
			}
			else
			{
				pStructData->fMinValidValue = 0;
				pStructData->fMaxValidValue = pStructData->iStateNum - 1;
			}
		} /* end of enum value format */

		else  /* for non-enum value, use: minValue/maxValue format */
		{
			if (iSubFieldCount != 2)
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: ERROR: Value Range "
					"field is invalid (for non-enum value, should be "
					"minValue/maxValue format)!\n", __FILE__);
				return 8;
			}

			/* get minValue of the range */
			pField = Cfg_SplitStringEx(pField, &pSubField, '/');
			if ((*pSubField < '0' || *pField > '9') && *pSubField != '-')
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: ERROR: minValue of"
					" Value Range field is not a number!\n", __FILE__);
				return 8;
			}
			pStructData->fMinValidValue = atof(pSubField);

			/* get maxValue of the range */
			Cfg_SplitStringEx(pField, &pSubField, '/');
			if ((*pSubField < '0' || *pField > '9') && *pSubField != '-')
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: ERROR: maxValue of"
					" Value Range field is not a number!\n", __FILE__);
				return 8;
			}
			pStructData->fMaxValidValue = atof(pSubField);
		} /* end of minValue/maxValue format process */
	} /* end of not empty field process */

	/* default value of control signal process */
	if (pStructData->iSigValueType == VAR_FLOAT)
	{
		pStructData->defaultValue.fValue = pStructData->fMinValidValue;
	}
	else
	{
		pStructData->defaultValue.lValue = pStructData->fMinValidValue;
	}

	/* 9.Value Display Attribute field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->byValueDisplayAttr = DISPLAY_NONE;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Value Display"
				" Attr is invalid!\n", __FILE__);
			return 9; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');
		if (iSubFieldCount > 2)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Value Display"
				" Attr is invalid(too many)!\n", __FILE__);
			return 9; 
		}

		pField = Cfg_SplitStringEx(pField, &pSubField, '/');
		toupper(*pSubField);
		if (*pSubField == 'W')
		{
			pStructData->byValueDisplayAttr = DISPLAY_WEB;
		}
		else if (*pSubField == 'L')
		{
			pStructData->byValueDisplayAttr = DISPLAY_LCD;
		}
		else
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Value Display"
				" Attr is invalid!\n", __FILE__);
			return 9; 
		}

		if (iSubFieldCount == 2)     /* may have W/L attibute */
		{
			Cfg_SplitStringEx(pField, &pSubField, '/');
			if (*pSubField == 'W')
			{
				pStructData->byValueDisplayAttr |= DISPLAY_WEB;
			}
			else if (*pSubField == 'L')
			{
				pStructData->byValueDisplayAttr |= DISPLAY_LCD;
			}
			else
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: ERROR: Value Display"
					" Attr is invalid!\n", __FILE__);
				return 9; 
			}
		}		
	}

	/* 10.Value Control Attribute field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->byValueControlAttr = CONTROL_NONE;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Value Control"
				" Attr is invalid!\n", __FILE__);
			return 10; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');
		if (iSubFieldCount > 2)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Value Control"
				" Attr is invalid(too many)!\n", __FILE__);
			return 10; 
		}

		pField = Cfg_SplitStringEx(pField, &pSubField, '/');
		toupper(*pSubField);
		if (*pSubField == 'W')
		{
			pStructData->byValueControlAttr = CONTROL_WEB;
		}
		else if (*pSubField == 'L')
		{
			pStructData->byValueControlAttr = CONTROL_LCD;
		}
		else
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Value Control"
				" Attr is invalid!\n", __FILE__);
			return 10; 
		}

		if (iSubFieldCount == 2)     /* may have W/L attibute */
		{
			Cfg_SplitStringEx(pField, &pSubField, '/');
			if (*pSubField == 'W')
			{
				pStructData->byValueControlAttr |= CONTROL_WEB;
			}
			else if (*pSubField == 'L')
			{
				pStructData->byValueControlAttr |= CONTROL_LCD;
			}
			else
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: ERROR: Value Control"
					" Attr is invalid!\n", __FILE__);
				return 10; 
			}
		}		
	}

	/* 11.Controllable Expression field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iControllableExpression = 0;
	}
	else
	{
		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);

		if (ppExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory for Controllable"
				" Expression pointer array.\n", __FILE__);
			return 11;
		}
		ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iControllableExpression,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Controlable"
				" Expression is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 11;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 11;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: EXP_MAX_ELEMENTS"
				" macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 11;
		}

		// parse OK.
		pStructData->pControllableExpression = NEW(EXP_ELEMENT,
			pStructData->iControllableExpression);

		if (pStructData->pControllableExpression == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory for Controllable"
				" Expression pointer array.\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iControllableExpression, ppExp);

			return 11;
		}

		Cfg_DumpExp(pStructData->pControllableExpression,
			ppExp,
			pStructData->iControllableExpression);
	}

	/* 12.Authority Level field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	*pField = toupper(*pField);
	/* valid level: B(browser) O(operator) E(Engineer) A(admin) */
	if (*pField != 'B' && *pField != 'O' && *pField != 'E' &&
		*pField != 'A')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseControlSigTableProc: ERROR: Authority "
			"Level Type is invalid!\n", __FILE__);
		return 12;
	}
	switch(*pField)
	{
	case 'B':
		pStructData->byAuthorityLevel = BROWSER_LEVEL;
		break;
	case 'O':
		pStructData->byAuthorityLevel = OPERATOR_LEVEL;
		break;
	case 'E':
		pStructData->byAuthorityLevel = ENGINEER_LEVEL;
		break;
	case 'A':
		pStructData->byAuthorityLevel = ADMIN_LEVEL;
		break;
	default:
		TRACE("Should not go here!\n");
		ASSERT(0);
	}

	/* 13.Value Display ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseControlSigTableProc: ERROR: Value Display"
			" ID is not a number!\n", __FILE__);
		return 13;    /* not a num, error */
	}
	pStructData->iValueDisplayID = atoi(pField);

	/* 14.Value Display Format field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->szValueDisplayFmt = NEW(char, 1);
		if (pStructData->szValueDisplayFmt == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: no memory.\n",__FILE__);
			return 14;
		}
		*pStructData->szValueDisplayFmt = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Value Display"
				" format is NULL!\n", __FILE__);
			return 14;     
		}
		pStructData->szValueDisplayFmt = NEW_strdup(pField);
		if (pStructData->szValueDisplayFmt == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory to"
				" load Value Display Format info!\n", __FILE__);
			return 14;     
		}
	}

	/* 15.Control Channel ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '-')   /* -1,-2,-3 is valid */
	{
		if (*(pField+1) < '1' || *(pField+1) > '3')  /* now pField+1 is not NULL */
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Control Chnanel"
				" ID is invalid!\n", __FILE__);
			return 15;    /* not a num, error */
		}
	}
	else 
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Control Chnanel"
				" ID is invalid!\n", __FILE__);
			return 15;    /* not a num, error */
		}
	}
	pStructData->iControlChannel = atoi(pField);

	/* 16.Control Step field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		if (pStructData->byValueControlAttr & CONTROL_LCD &&
			pStructData->iSigValueType != VAR_ENUM)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: for non-enum"
				" sig which can be controlled on LCD should config Control"
				" Step!\n", __FILE__);
			return 16;
		}
		pStructData->fControlStep = 0;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Control Step"
				" is not a number!\n", __FILE__);
			return 16;    /* not a num, error */
		}
		pStructData->fControlStep = atof(pField);
	}

	/* 17.Control Param field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->szCtrlParam = NEW(char, 1);
		if (pStructData->szCtrlParam == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTalbeProc: ERROR: "
				"no memory!\n", __FILE__);
			return 17;
		}
		*pStructData->szCtrlParam = '\0';
	}
	else
	{
		if ( *pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Control "
				"Param is NULL!\n", __FILE__);
			return 17;
		}

		pStructData->szCtrlParam = NEW_strdup(pField);
		if (pStructData->szCtrlParam == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory"
				" to load Ctrl Param info!\n", __FILE__);
			return 17;
		}
	}

	/* 18.Control Expression */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iCtrlExpression = 0;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Control "
				"Expression is NULL!\n", __FILE__);
			return 18; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');
		pStructData->iCtrlExpression = iSubFieldCount;
		pStructData->pCtrlExpression = NEW(CTRL_EXPRESSION, iSubFieldCount);
		if (pStructData->pCtrlExpression == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory for Ctrl"
				"Expression ", __FILE__);
			pStructData->iCtrlExpression = 0;
			return 18;
		}
		iCurIndex = 0;
		do
		{
			pField = Cfg_SplitStringEx(pField, &pSubField, '/');
			pSubField = Cfg_RemoveBoundChr(pSubField, IsBoundChr_S);

			iSubSubFieldCount = Cfg_GetSplitStringCount(pSubField, ',');
			if (iSubSubFieldCount < 2)
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: ERROR: Sub-express "
					"%d of Control Expression is invalid(lack of ',' as "
					"splitter)!\n", __FILE__, iCurIndex+1);
				return 18;
			}

			/* get threshold value of the sub-expression */
			pSubField = Cfg_SplitStringEx(pSubField, &pSubSubField, ',');
			if ((*pSubSubField < '0' || *pSubSubField >'9') && 
				*pSubSubField != '-')  
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseControlSigTableProc: ERROR: Threshold "
					"value in sub-expression %d for Control Expression"
					" is not a number!\n", __FILE__, iCurIndex+1);
				return 18;
			}
			pCtlExp = pStructData->pCtrlExpression + iCurIndex;
			pCtlExp->fThreshold = atof(pSubSubField);

			/* get resource id value of the sub-express */
			/* valid format for resource id sub-express should be: IDn=... */
			Cfg_SplitStringEx(pSubField, &pSubSubField, ',');	
			if (Cfg_IsEmptyField(pSubSubField))
			{
				/* not configed at for the certain range */
				pCtlExp->pText = NULL;
			}
			else
			{
				iStrLen = strlen(pSubSubField); 
				if (iStrLen < 4)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseControlSigTableProc: ERROR: the format "
						"of Resource ID value in sub-expression %d for Control"
						" expression is invalid!\n", __FILE__, iCurIndex+1);
					return 18;
				}
				/* now the pointer will not beyound */
				if (*pSubSubField != 'I' || *(pSubSubField+1) != 'D')
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseControlSigTableProc: ERROR: the format "
						"of Resource ID value in sub-expression %d for Control"
						" Expression is invalid!\n", __FILE__, iCurIndex+1);
					return 18;
				}
				/* throw string after '=' */
				if (Cfg_GetSplitStringCount(pSubField, '=') < 2)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseControlSigTableProc: ERROR: the format "
						"of Resource ID value in sub-expression %d for Control"
						" Expression is invalid!\n", __FILE__, iCurIndex+1);
					return 18;
				}
				Cfg_SplitStringEx(pSubSubField, &pResourceIDString, '=');
				pResourceIDString = Cfg_RemoveBoundChr(pResourceIDString,
					IsBoundChr_ID); /* remove 'ID' */
				pResourceIDString = Cfg_RemoveWhiteSpace(pResourceIDString);

				/* now get the resource id value */
				if ( *pResourceIDString <'0' || *pResourceIDString > '9')
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseControlSigTableProc: ERROR: the format"
						" of Resource ID value in sub-expression %d for Control"
						" Expression is invalid (not a number)!\n",
						__FILE__, iCurIndex+1);
					return 18;
				}
				iResourceID = atof(pResourceIDString);	
				pCtlExp->pText = GetLangText(iResourceID, pLangFile->iLangTextNum,
					pLangFile->pLangText);

				if (pCtlExp->pText == NULL)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--%s: ERROR: Parse No.%d of Control Expression "
						"Resource IDs failed!\n", __FILE__, __FUNCTION__, 
						iCurIndex+1);
					return 18;
				}
			}
		}
		while (++iCurIndex < iSubFieldCount);


		// added the sort function. maofuhua. 2005-3-3
		// sort by descending order.
		qsort((void *)pStructData->pCtrlExpression, 
			(size_t)pStructData->iCtrlExpression,
			(size_t)sizeof(pStructData->pCtrlExpression[0]),
			(int (*)(const void *, const void *))Cmp_CtrlSigExp);
	}




	/* 19.On Control field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		CLEAR_CFG_ON_CTRL(&pStructData->OnCtrl);
	}
	else
	{
		CFG_ON_CTRL_ACTION *pOnCtrl = &pStructData->OnCtrl;

		if (*pField != '[')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is"
				" invalid(lack of '[')!\n", __FILE__, __FUNCTION__);
			return 19;
		}

		/* get iRelevantEquipIndex value */
		pField = Cfg_RemoveBoundChr(pField, IsBoundChr_S);
		pField = Cfg_SplitStringEx(pField, &pSubField, ',');
		if (*pSubField < '0' || *pSubField > '9')			
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is invalid"
				" (invalid Relevant Equip Index of Variable Exp)!\n",
				__FILE__, __FUNCTION__);
			return 19;					/* expression is invalid */
		}
		sscanf(pSubField, "%d", &pOnCtrl->expVar.iRelevantEquipIndex);

		/* get iSigType value */
		pField = Cfg_SplitStringEx(pField, &pSubField, ',');
		if (*pSubField < '0' || *pSubField > '9')			
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is"
				" invalid(invalid Sig Type of Variable Exp)!\n",
				__FILE__, __FUNCTION__);
			return 19;		
		}
		sscanf(pSubField, "%d", &pOnCtrl->expVar.iSigType);

		/* get iSigID value */
		pField = Cfg_SplitStringEx(pField, &pSubField, ']');
		if (*pSubField < '0' || *pSubField > '9')			
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is invalid"
				" (invalid Sig ID of Variable Exp)!\n", 
				__FILE__, __FUNCTION__);
			return 19;		
		}
		sscanf(pSubField, "%d",  &pOnCtrl->expVar.iSigID);

		/* find '=' */
		pField = Cfg_RemoveWhiteSpace(pField); 
		if (*pField != '=')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is invalid"
				" (lack of '=')!\n", __FILE__, __FUNCTION__);
			return 19;		
		}

		/* get the value */
		pField++;
		pField = Cfg_RemoveWhiteSpace(pField); 
		if (!CFG_CheckNumber(pField))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is invalid"
				" (lack of a value)!\n", __FILE__, __FUNCTION__);
			return 19;		
		}
		pOnCtrl->fValue = atof(pField);
	}

	/* 20.Display Expression field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iDispExp = 0;
	}
	else
	{
		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
	
		if (ppExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory for Display"
				" Expression pointer array.\n", __FILE__);
			return 12;
		}

	    ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iDispExp,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: Display Expression"
				" is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: EXP_MAX_ELEMENTS"
				" macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;
		}

		//
		pStructData->pDispExp = NEW(EXP_ELEMENT,
			pStructData->iDispExp);
		if (pStructData->pDispExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseControlSigTableProc: ERROR: no memory for Display"
				" Expression pointer array.\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iDispExp, ppExp);

			return 12;
		}

		Cfg_DumpExp(pStructData->pDispExp,
			ppExp,
			pStructData->iDispExp);		
	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : ParseSetSigTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char             *szBuf      : 
 *            OUT SET_SIG_INFO *  pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 10:03
 *==========================================================================*/
static int ParseSetSigTableProc(IN char *szBuf, OUT SET_SIG_INFO *pStructData)
{
	char *pField,*pSubField, *pSubSubField, *pResourceIDString;
	int  iSubFieldCount, iSubSubFieldCount,iCurIndex;
	int  iStrLen, iResourceID;	/* language resource id */
	int  ret;
	CTRL_EXPRESSION *pCtlExp;

	LANG_FILE *pLangFile;
	pLangFile = g_SiteInfo.LangInfo.pCurLangFile;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Sig ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Sig ID is not"
			" a number!\n", __FILE__);
		return 1;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 2.Sig Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    //TRACE("%s\n",pField);
	/* 3.Sig Name Resource ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Sig Name Resource"
			" ID is not a number!\n", __FILE__);
		return 3;    /* not a num, error */
	}
	iResourceID = atoi(pField);
	pStructData->pSigName = GetLangText(iResourceID, pLangFile->iLangTextNum,
		pLangFile->pLangText);
	if (pStructData->pSigName == NULL)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Parse Sig Name "
			"Resource ID failed!\n", __FILE__);
		return 3;
	}

	/* 4.Sig Unit field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->szSigUnit = NEW(char, 1);
		if (pStructData->szSigUnit == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: "
				"no memory.\n", __FILE__);
			return 4;
		}
		*(pStructData->szSigUnit) = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Sig Unit"
				" is invalid!\n", __FILE__);
			return 4;
		}
		pStructData->szSigUnit = NEW_strdup_f(pField, Cfg_ReplaceIllegalChr);

		if (pStructData->szSigUnit == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: no memory to "
				"load Sig Unit info!\n", __FILE__);
			return 4;  
		}
	}

	/* 5.Index of Sampler list field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0' || *pField > '9') && 
		*pField != '-')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Index of Sampler "
			"list is not an invalid number!\n", __FILE__);
		return 5;    /* not a num, error */
	}
	/* for negative number, only -1 is valid */
	if (*pField == '-' && *(pField + 1) != '1')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Index of Sampler "
			"list is not an invalid number!\n", __FILE__);
		return 5;    /* not a num, error */
	}
	pStructData->iIndexofSamplerList = atoi(pField);

	/* 6.Sampler Channel ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iSamplerChannel = -3;
	}
	else
	{
		if (*pField == '-')   /* -1,-2,-3 is valid */
		{
			if (*(pField+1) < '1' || *(pField+1) > '3') /* now pField+1 is not NULL */
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: Sampler Chnanel"
					" ID is invalid!\n", __FILE__);
				return 6;    /* not a num, error */
			}
		}
		else 
		{
			if ((*pField < '0') || (*pField > '9'))
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: Sampler Chnanel"
					" ID is invalid!\n", __FILE__);
				return 6;    /* not a num, error */
			}
		}
		pStructData->iSamplerChannel = atoi(pField);

		if (pStructData->iIndexofSamplerList == -1 &&
			pStructData->iSamplerChannel > 0)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: index of sampler list"
				" and channel no. is inconsistent.\n", __FILE__);
			return 6;
		}
	}


	/* 7.Sig value type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	*pField = toupper(*pField);
	/* valid type: long(L), float(F), unsigned long(U), date/time(T), Enum(E) */
	if (*pField != 'L' && *pField != 'F' && *pField != 'U' &&
		*pField != 'T' && *pField != 'E' )
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Sig Value Type is "
			"invalid!\n", __FILE__);
		return 7;
	}
	switch(*pField)
	{
	case 'L':
		pStructData->iSigValueType = VAR_LONG;
		break;
	case 'F':
		pStructData->iSigValueType = VAR_FLOAT;
		break;
	case 'U':
		pStructData->iSigValueType = VAR_UNSIGNED_LONG;
		break;
	case 'T':
		pStructData->iSigValueType = VAR_DATE_TIME;
		break;
	case 'E':
		pStructData->iSigValueType = VAR_ENUM;
		break;
	default:					/* can not go to here */
		TRACE("Should not go here!\n");
		ASSERT(0);
	}

	/* 8.Default Value field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0' || *pField > '9') && *pField != '-')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Default Value "
			"is not a number!\n", __FILE__);
		return 8;    /* not a num, error */
	}
	if (pStructData->iSigValueType == VAR_FLOAT)
	{
		pStructData->defaultValue.fValue = atof(pField);
	}
	else
	{
		pStructData->defaultValue.lValue = atoi(pField);
	}

	/* 9.Value range field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		switch (pStructData->iSigValueType)
		{
			case VAR_ENUM:
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: Value Range field "
					"should not be empty for Enum!\n", __FILE__);
				return 9;

			case VAR_LONG:     
				pStructData->fMinValidValue = MINLONG;
				pStructData->fMaxValidValue = MAXLONG;
				break;

			case VAR_FLOAT:
				pStructData->fMinValidValue = ACU_MINFLOAT;
				pStructData->fMaxValidValue = MAXFLOAT;
				break;

			case VAR_DATE_TIME:  //deal it as VAR_UNSIGNED_LONG
			case VAR_UNSIGNED_LONG:
				pStructData->fMinValidValue = 0;
				pStructData->fMaxValidValue = (unsigned long)-1;
				break;

			default:
				break;
		}
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Value Range field "
				"is NULL!\n", __FILE__);
			return 9; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');

		/* for enum value, use [ID1=... ]/[ID2=...]/../[IDn=...] format */
		if (pStructData->iSigValueType == VAR_ENUM) 
		{
			pStructData->iStateNum = iSubFieldCount;
			pStructData->pStateText = NEW(LANG_TEXT *, iSubFieldCount);
			if (pStructData->pStateText == NULL)
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: no memory "
					"for State Text pointer array/\n", __FILE__);
				pStructData->iStateNum = 0;
				return 9;
			}
			iCurIndex = 0;
			do
			{
				pField = Cfg_SplitStringEx(pField, &pSubField, '/');
				/* get rid of '[' and ']' */
				pSubField = Cfg_RemoveBoundChr(pSubField, IsBoundChr_S);

				iSubSubFieldCount = Cfg_GetSplitStringCount(pSubField, '=');
				if (iSubSubFieldCount < 2)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseSetSigTableProc: ERROR: Value Range"
						" format for enum value is invalid"
						"(missing \'=\')!\n", __FILE__);
					return 9;
				}

				/* get 'IDn' string */
				Cfg_SplitStringEx(pSubField, &pSubSubField, '=');
				/* remove 'ID' character */
				pSubSubField = Cfg_RemoveBoundChr(pSubSubField, IsBoundChr_ID);
				pSubSubField = Cfg_RemoveWhiteSpace(pSubSubField);
				if (*pSubSubField < '0' || *pSubSubField > '9')
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseSetSigTableProc: ERROR: Value Range"
						" format for enum value is invalid(resource"
						" id is not a number)!\n", __FILE__);
					return 9;
				}

				/* now resource id has been got */
				iResourceID = atoi(pSubSubField);
				(pStructData->pStateText)[iCurIndex] = 
					GetLangText(iResourceID, pLangFile->iLangTextNum,
					pLangFile->pLangText);

				if ((pStructData->pStateText)[iCurIndex] == NULL)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--%s: ERROR: Parse No.%d of Enum Value Name "
						"Resource IDs failed!\n", __FILE__, __FUNCTION__, 
						iCurIndex+1);
					return 9;
				}
			}  
			while (++iCurIndex < iSubFieldCount);

			/* for single-value set signal, should always be "1" */
			if (pStructData->iStateNum == 1)
			{
				pStructData->fMinValidValue = 1;
				pStructData->fMaxValidValue = 1;
			}
			else
			{
				pStructData->fMinValidValue = 0;
				pStructData->fMaxValidValue = pStructData->iStateNum - 1;
			}
		} /* end of enum value format */

		else  /* for non-enum value, use: minValue/maxValue format */
		{
			if (iSubFieldCount != 2)
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: Value Range"
					" field is invalid (for non-enum value, should be"
					" minValue/maxValue format)!\n", __FILE__);
				return 9;
			}

			/* get minValue of the range */
			pField = Cfg_SplitStringEx(pField, &pSubField, '/');
			if ((*pSubField < '0' || *pField > '9') && *pSubField != '-')
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: minValue of "
					"Value Range field is not a number!\n", __FILE__);
				return 9;
			}
			pStructData->fMinValidValue = atof(pSubField);

			/* get maxValue of the range */
			Cfg_SplitStringEx(pField, &pSubField, '/');
			if ((*pSubField < '0' || *pField > '9') && *pSubField != '-')
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: maxValue of "
					"Value Range field is not a number!\n", __FILE__);
				return 9;
			}
			pStructData->fMaxValidValue = atof(pSubField);
		} /* end of minValue/maxValue format process */
	} /* end of not empty field process */

	/* 10.Value Display Attribute field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->byValueDisplayAttr = DISPLAY_NONE;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Value Display "
				"Attr is invalid!\n", __FILE__);
			return 10; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');
		if (iSubFieldCount > 2)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Value Display "
				"Attr is invalid(too many)!\n", __FILE__);
			return 10; 
		}

		pField = Cfg_SplitStringEx(pField, &pSubField, '/');
		toupper(*pSubField);
		if (*pSubField == 'W')
		{
			pStructData->byValueDisplayAttr = DISPLAY_WEB;
		}
		else if (*pSubField == 'L')
		{
			pStructData->byValueDisplayAttr = DISPLAY_LCD;
		}
		else
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Value Display"
				" Attr is invalid!\n", __FILE__);
			return 10; 
		}

		if (iSubFieldCount == 2)     /* may have W/L attibute */
		{
			Cfg_SplitStringEx(pField, &pSubField, '/');
			if (*pSubField == 'W')
			{
				pStructData->byValueDisplayAttr |= DISPLAY_WEB;
			}
			else if (*pSubField == 'L')
			{
				pStructData->byValueDisplayAttr |= DISPLAY_LCD;
			}
			else
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: Value Display"
					" Attr is invalid!\n", __FILE__);
				return 10; 
			}
		}
	}

	/* 11.Value Setting Attribute field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->byValueSetAttr = SET_NONE;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Value Control"
				" Attr is invalid!\n", __FILE__);
			return 11; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');
		if (iSubFieldCount > 2)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Value Control"
				" Attr is invalid(too many)!\n", __FILE__);
			return 11; 
		}

		pField = Cfg_SplitStringEx(pField, &pSubField, '/');
		toupper(*pSubField);
		if (*pSubField == 'W')
		{
			pStructData->byValueSetAttr = SET_WEB;
		}
		else if (*pSubField == 'L')
		{
			pStructData->byValueSetAttr = SET_LCD;
		}
		else
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Value Control "
				"Attr is invalid!\n", __FILE__);
			return 11; 
		}

		if (iSubFieldCount == 2)     /* may have W/L attibute */
		{
			Cfg_SplitStringEx(pField, &pSubField, '/');
			if (*pSubField == 'W')
			{
				pStructData->byValueSetAttr |= SET_WEB;
			}
			else if (*pSubField == 'L')
			{
				pStructData->byValueSetAttr |= SET_LCD;
			}
			else
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: Value Control"
					" Attr is invalid!\n", __FILE__);
				return 11; 
			}
		}		
	}

	/* 12.Settable Expression field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iSettableExpression = 0;
	}
	else
	{
		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
	
		if (ppExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: no memory for Settable"
				" Expression pointer array.\n", __FILE__);
			return 12;
		}

	    ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iSettableExpression,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Settable Expression"
				" is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: EXP_MAX_ELEMENTS"
				" macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;
		}

		//
		pStructData->pSettableExpression = NEW(EXP_ELEMENT,
			pStructData->iSettableExpression);
		if (pStructData->pSettableExpression == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: no memory for Settable"
				" Expression pointer array.\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iSettableExpression, ppExp);

			return 12;
		}

		Cfg_DumpExp(pStructData->pSettableExpression,
			ppExp,
			pStructData->iSettableExpression);		
	}

	/* 13.Authority Level field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	*pField = toupper(*pField);
	/* valid level: B(browser) O(operator) E(Engineer) A(admin) */
	if (*pField != 'B' && *pField != 'O' && *pField != 'E' &&
		*pField != 'A')
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Authority Level "
			"Type is invalid!\n", __FILE__);
		return 13;
	}
	switch(*pField)
	{
	case 'B':
		pStructData->byAuthorityLevel = BROWSER_LEVEL;
		break;
	case 'O':
		pStructData->byAuthorityLevel = OPERATOR_LEVEL;
		break;
	case 'E':
		pStructData->byAuthorityLevel = ENGINEER_LEVEL;
		break;
	case 'A':
		pStructData->byAuthorityLevel = ADMIN_LEVEL;
		break;
	default:
		TRACE("Should not go here!\n");
		ASSERT(0);
	}

	/* 14.Value Display ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Value Display ID"
			" is not a number!\n", __FILE__);
		return 14;    /* not a num, error */
	}
	pStructData->iValueDisplayID = atoi(pField);

	/* 15.Value Display Format field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->szValueDisplayFmt = NEW(char, 1);
		if (pStructData->szValueDisplayFmt == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			return 15;
		}
		*pStructData->szValueDisplayFmt = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Value Display "
				"format is NULL!\n", __FILE__);
			return 15;     
		}
		pStructData->szValueDisplayFmt = NEW_strdup(pField);
		if (pStructData->szValueDisplayFmt == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: no memory to "
				"load Value Display Format info!\n", __FILE__);
			return 15;     
		}
	}

	/* 16.Control Channel ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '-')   /* -1,-2,-3 is valid */
	{
		if (*(pField+1) < '1' || *(pField+1) > '3') /* now pField+1 is not NULL */
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Control Chnanel ID"
				" is invalid!\n", __FILE__);
			return 16;    /* not a num, error */
		}
	}
	else 
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Control Chnanel ID"
				" is invalid!\n", __FILE__);
			return 16;    /* not a num, error */
		}
	}
	pStructData->iControlChannel = atoi(pField);

	/* 17.Setting Step field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		if (pStructData->byValueSetAttr & CONTROL_LCD &&
			pStructData->iSigValueType != VAR_ENUM)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: for non-enum sig"
				" which can be set on LCD should config Setting "
				"Step!\n", __FILE__);
			return 17;
		}
		pStructData->fSettingStep = 0;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Setting Step is"
				" not a number!\n", __FILE__);
			return 17;    /* not a num, error */
		}
		pStructData->fSettingStep = atof(pField);
	}

	/* 18.Control Expression */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iCtrlExpression = 0;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Control Expression"
				" is NULL!\n", __FILE__);
			return 18; 
		}

		iSubFieldCount = Cfg_GetSplitStringCount(pField, '/');
		pStructData->iCtrlExpression = iSubFieldCount;
		pStructData->pCtrlExpression = NEW(CTRL_EXPRESSION, iSubFieldCount);
		if (pStructData->pCtrlExpression == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: no memory for Ctrl"
				"Expression \n", __FILE__);
			pStructData->iCtrlExpression = 0;
			return 18;
		}
		iCurIndex = 0;
		do
		{
			pField = Cfg_SplitStringEx(pField, &pSubField, '/');
			pSubField = Cfg_RemoveBoundChr(pSubField, IsBoundChr_S);

			iSubSubFieldCount = Cfg_GetSplitStringCount(pSubField, ',');
			if (iSubSubFieldCount < 2)
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: Sub-express %d "
					"of Control Expression is invalid(lack of ',' as "
					"splitter)!\n", __FILE__, iCurIndex+1);
				return 18;
			}

			/* get threshold value of the sub-expression */
			pSubField = Cfg_SplitStringEx(pSubField, &pSubSubField, ',');
			if ((*pSubSubField < '0' || *pSubSubField >'9') && 
				*pSubSubField != '-')  
			{
				AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
					"[%s]--ParseSetSigTableProc: ERROR: Threshold value "
					"in sub-expression %d for Control Expression is not"
					" a number!\n", __FILE__, iCurIndex+1);
				return 18;
			}
			pCtlExp = pStructData->pCtrlExpression + iCurIndex;
			pCtlExp->fThreshold = atof(pSubSubField);

			/* get resource id value of the sub-express */
			/* valid format for resource id sub-express should be: IDn=... */
			Cfg_SplitStringEx(pSubField, &pSubSubField, ',');	
			if (Cfg_IsEmptyField(pSubSubField))
			{
				/* not configed at for the certain range */
				pCtlExp->pText = NULL;
			}
			else
			{
				iStrLen = strlen(pSubSubField); 
				if (iStrLen < 4)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseSetSigTableProc: ERROR: the format of"
						" Resource ID value in sub-expression %d for Control "
						"Expression is invalid!\n", __FILE__, iCurIndex+1);
					return 18;
				}
				/* now the pointer will not beyound */
				if (*pSubSubField != 'I' || *(pSubSubField+1) != 'D')
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseSetSigTableProc: ERROR: the format of"
						" Resource ID value in sub-expression %d for Control "
						"Expression is invalid!\n", __FILE__, iCurIndex+1);
					return 18;
				}
				/* throw string after '=' */
				if (Cfg_GetSplitStringCount(pSubField, '=') < 2)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseSetSigTableProc: ERROR: the format of"
						" Resource ID value in sub-expression %d for Control "
						"Expression is invalid!\n", __FILE__, iCurIndex+1);
					return 18;
				}
				Cfg_SplitStringEx(pSubSubField, &pResourceIDString, '=');
				pResourceIDString = Cfg_RemoveBoundChr(pResourceIDString,
					IsBoundChr_ID); /* remove 'ID' */
				pResourceIDString = Cfg_RemoveWhiteSpace(pResourceIDString);

				/* now get the resource id value */
				if ( *pResourceIDString <'0' || *pResourceIDString > '9')
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--ParseSetSigTableProc: ERROR: the format of "
						"Resource ID value in sub-expression %d for Control "
						"Expression is invalid (not a number)!\n",
						__FILE__, iCurIndex+1);
					return 18;
				}
				iResourceID = atof(pResourceIDString);	
				pCtlExp->pText = GetLangText(iResourceID, pLangFile->iLangTextNum,
					pLangFile->pLangText);

				if (pCtlExp->pText == NULL)
				{
					AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
						"[%s]--%s: ERROR: Parse No.%d of Control Expression "
						"Resource IDs failed!\n", __FILE__, __FUNCTION__, 
						iCurIndex+1);
					return 18;
				}
			}

		}
		while (++iCurIndex < iSubFieldCount);

		// added the sort function. maofuhua. 2005-3-3
		// sort by descending order.
		qsort((void *)pStructData->pCtrlExpression, 
			(size_t)pStructData->iCtrlExpression,
			(size_t)sizeof(pStructData->pCtrlExpression[0]),
			(int (*)(const void *, const void *))Cmp_CtrlSigExp);
	}

	/* 19.Persistence flag field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	switch (*pField)
	{
	case '1':
		pStructData->bPersistentFlag = TRUE;
		break;

	case '0':
		pStructData->bPersistentFlag = FALSE;
		break;

	default:
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Persistence flag is "
			"invalid(should be \'1\' or \'0\')!\n", __FILE__);
		return 19;     
	}

	/* 20.On Control field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		CLEAR_CFG_ON_CTRL(&pStructData->OnCtrl);
	}
	else
	{
		CFG_ON_CTRL_ACTION *pOnCtrl = &pStructData->OnCtrl;

		if (*pField != '[')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is"
				" invalid(lack of '[')!\n", __FILE__, __FUNCTION__);
			return 20;
		}

		/* get iRelevantEquipIndex value */
		pField = Cfg_RemoveBoundChr(pField, IsBoundChr_S);
		pField = Cfg_SplitStringEx(pField, &pSubField, ',');
		if (*pSubField < '0' || *pSubField > '9')			
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is invalid"
				" (invalid Relevant Equip Index of Variable Exp)!\n",
				__FILE__, __FUNCTION__);
			return 20;					/* expression is invalid */
		}
		sscanf(pSubField, "%d", &pOnCtrl->expVar.iRelevantEquipIndex);

		/* get iSigType value */
		pField = Cfg_SplitStringEx(pField, &pSubField, ',');
		if (*pSubField < '0' || *pSubField > '9')			
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is"
				" invalid(invalid Sig Type of Variable Exp)!\n",
				__FILE__, __FUNCTION__);
			return 20;		
		}
		sscanf(pSubField, "%d", &pOnCtrl->expVar.iSigType);

		/* get iSigID value */
		pField = Cfg_SplitStringEx(pField, &pSubField, ']');
		if (*pSubField < '0' || *pSubField > '9')			
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is invalid"
				" (invalid Sig ID of Variable Exp)!\n", 
				__FILE__, __FUNCTION__);
			return 20;		
		}
		sscanf(pSubField, "%d",  &pOnCtrl->expVar.iSigID);

		/* find '=' */
		pField = Cfg_RemoveWhiteSpace(pField); 
		if (*pField != '=')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is invalid"
				" (lack of '=')!\n", __FILE__, __FUNCTION__);
			return 20;		
		}

		/* get the value */
		pField++;
		pField = Cfg_RemoveWhiteSpace(pField); 
		if (!CFG_CheckNumber(pField))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--%s: ERROR: On Control Field is invalid"
				" (lack of a value)!\n", __FILE__, __FUNCTION__);
			return 20;		
		}
		pOnCtrl->fValue = atof(pField);
	}

	/* 21.Display Expression field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iDispExp = 0;
	}
	else
	{
		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
	
		if (ppExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: no memory for Display"
				" Expression pointer array.\n", __FILE__);
			return 12;
		}

	    ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iDispExp,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: Display Expression"
				" is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: EXP_MAX_ELEMENTS"
				" macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 12;
		}

		//
		pStructData->pDispExp = NEW(EXP_ELEMENT,
			pStructData->iDispExp);
		if (pStructData->pDispExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseSetSigTableProc: ERROR: no memory for Display"
				" Expression pointer array.\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iDispExp, ppExp);

			return 12;
		}

		Cfg_DumpExp(pStructData->pDispExp,
			ppExp,
			pStructData->iDispExp);		
	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : ParseTypeInfoTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char                 *szBuf       : 
 *            OUT STDEQUIP_TYPE_INFO  *pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 11:22
 *==========================================================================*/
static int ParseTypeInfoTableProc(IN char *szBuf, OUT STDEQUIP_TYPE_INFO *pStructData)
{
	char *pField;
	int iResourceID;

	LANG_FILE *pLangFile;
	pLangFile = g_SiteInfo.LangInfo.pCurLangFile;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Type ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseTypeInfoTableProc: ERROR: StdEquip Type ID is "
			"not a number!\n", __FILE__);
		return 1;    /* not a num, error */
	}
	pStructData->iTypeID = atoi(pField);

	/* 2.Goup Type Flag field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	/* 1 for TURE, 0 for FALSE */
	if ((*pField != '0') && (*pField != '1'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseTypeInfoTableProc: ERROR: Group Type Flag is "
			"invalid(should be 1 or 0)!\n", __FILE__);
		return 2;    /* not a num, error */
	}
	pStructData->bGroupType = atoi(pField);

	/* 3.Type Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER); /* just jump it */

	/* 4.Type Name Resource ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseTypeInfoTableProc: ERROR: StdEquip Type Name "
			"Resource ID is not a number!\n", __FILE__);
		return 4;    /* not a num, error */
	}
	iResourceID = atoi(pField);
	pStructData->pTypeName = GetLangText(iResourceID, pLangFile->iLangTextNum, 
		pLangFile->pLangText);
	if (pStructData->pTypeName == NULL)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseTypeInfoTableProc: ERROR: Parse StdEquip Type "
			"Name Resource ID failed!\n", __FILE__);
		return 4;
	}

	/* 5.Sampling Signal ID for Serial Number field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField != '\0')
	{
		if ((*pField <= '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseTypeInfoTableProc: ERROR: Parse StdEquip Sampling "
				"Signal ID for Serial Number failed!\n", __FILE__);
			return 4;
		}

		pStructData->iSerialRelatedSigID = atoi(pField);
	}
	else  //not configured
	{
		pStructData->iSerialRelatedSigID = -1;
	}

	return 0;

}
/*==========================================================================*
 * FUNCTION : ParseAlarmSigTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char          *szBuf      : 
 *            ALARM_INFO *  pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 16:42
 *==========================================================================*/
static int ParseAlarmSigTableProc(IN char *szBuf, OUT ALARM_SIG_INFO *pStructData)
{
	char *pField;
	int iResourceID;
	int ret;

	LANG_FILE *pLangFile;
	pLangFile = g_SiteInfo.LangInfo.pCurLangFile;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Alarm Sig ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseAlarmSigTableProc: ERROR: Sig ID is not a "
			"number!\n", __FILE__);
		return 1;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 2.Alarm Sig Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER); /* just jump it */

	/* 3.Alarm Sig Name Resource ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseAlarmSigTableProc: ERROR: Sig Name Resource ID"
			" is not a number!\n", __FILE__);
		return 3;    /* not a num, error */
	}
	iResourceID = atoi(pField);
	pStructData->pSigName = GetLangText(iResourceID, pLangFile->iLangTextNum, 
		pLangFile->pLangText);
	if (pStructData->pSigName == NULL)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseAlarmSigTableProc: ERROR: Parse Sig Name "
			"Resource ID failed!\n", __FILE__);
		return 3;
	}

	/* 4.Alarm Level field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iAlarmLevel = ALARM_LEVEL_NONE;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: Alarm Level "
				"is NULL!\n", __FILE__);
			return 4;   
		}

		if (*(pField + 1) != 'A')   /* (*pField) is not '\0', so pField+1 is not NULL */
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: Alarm Level "
				"is invalid!\n", __FILE__);
			return 4;   
		}

		switch (*pField)
		{
		case 'N':			/* NA */
			pStructData->iAlarmLevel = ALARM_LEVEL_NONE;
			break;

		case 'O':			/* OA */
			pStructData->iAlarmLevel = ALARM_LEVEL_OBSERVATION;
			break;

		case 'M':			/* MA */
			pStructData->iAlarmLevel = ALARM_LEVEL_MAJOR;
			break;

		case 'C':			/* CA */
			pStructData->iAlarmLevel = ALARM_LEVEL_CRITICAL;
			break;

		default:
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: Alarm Level "
				"is NULL!\n", __FILE__);
			return 4;
		}
	}

	/* 5.Alarm Expression field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseAlarmSigTableProc: ERROR: Alarm Expression "
			"should be configured!\n", __FILE__);
		return 5;    
	}
	else
	{
		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);

		if (ppExp == NULL)

		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: no memory for Alarm"
				" Expression pointer array.\n", __FILE__);
			return 5;
		}

		ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iAlarmExpression,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: Alarm Expression"
				" is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 5;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 5;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: EXP_MAX_ELEMENTS"
				" macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 5;
		}

		// parse OK.
		pStructData->pAlarmExpression = NEW(EXP_ELEMENT,
			pStructData->iAlarmExpression);
		if (pStructData->pAlarmExpression == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: no memory for Alarm"
				" Expression pointer array.\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iAlarmExpression, ppExp);

			return 5;
		}

		Cfg_DumpExp(pStructData->pAlarmExpression,
			ppExp,
			pStructData->iAlarmExpression);	
	}

	/* 6.Alarm Delay field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iAlarmDelay = 0;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: Alarm Delay is"
				" not a number!\n", __FILE__);
			return 6;    /* not a num, error */
		}
		pStructData->iAlarmDelay = atoi(pField);
	}

	/* 7.Suppressing Expression field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iSuppressExpression = 0;
	}
	else
	{
        //pStructData->pOriginExpression = NEW_strdup(pField);//Added by wj for Alarm Suppressing Expression config

		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);

		if (ppExp == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: no memory for Suppress "
				"Expression Pointer Array!\n", __FILE__);
			return 7;
		}

		ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iSuppressExpression,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: Suppressing Expression"
				" is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 7;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 7;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: EXP_MAX_ELEMENTS"
				" macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 7;
		}

		//
		pStructData->pSuppressExpression = NEW(EXP_ELEMENT,
			pStructData->iSuppressExpression);

		if (pStructData->pSuppressExpression == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: no memory for Suppress "
				"Expression Pointer Array!\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iSuppressExpression, ppExp);

			return 7;
		}

		Cfg_DumpExp(pStructData->pSuppressExpression,
			ppExp,
			pStructData->iSuppressExpression);
	}

    /* 8.Alarm Relay No field */ //Added by wj for alarm relay No 2006.07.26
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (Cfg_IsEmptyField(pField))
    {
        pStructData->iAlarmRelayNo = 0;
    }
    else
    {
        if ((*pField < '1') || (*pField > '9'))
        {
            AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
                "[%s]--ParseAlarmSigTableProc: ERROR: Alarm RelayNo is"
                " not a number!\n", __FILE__);
            return 8;    /* not a num, error */
        }
        pStructData->iAlarmRelayNo = atoi(pField);
    }

	/* 9.Alarm help info Resource ID field, added for G3 by Lin.Tao.Thomas */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == NULL)   //not configured
	{
		pStructData->pHelpInfo = NULL;
	}
	else if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--ParseAlarmSigTableProc: ERROR: Alarm Info Resource ID"
			" is not a number!\n", __FILE__);
		return 9;    /* not a num, error */
	}
	else
	{
		iResourceID = atoi(pField);
		pStructData->pHelpInfo = GetLangText(iResourceID, pLangFile->iLangTextNum, 
			pLangFile->pLangText);
		if (pStructData->pHelpInfo == NULL)
		{
			AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
				"[%s]--ParseAlarmSigTableProc: ERROR: Parse Alarm Info "
				"Resource ID failed!\n", __FILE__);
			return 9;
		}
	}

	return 0;
}
/*==========================================================================*
 * FUNCTION : LoadStdEquipConfigProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN void        *pCfg       : 
 *            OUT STDEQUIP_TYPE_INFO  *pLoadToBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 17:02
 *==========================================================================*/
static int LoadStdEquipConfigProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	STDEQUIP_TYPE_INFO *pBuf;
	CONFIG_TABLE_LOADER loader[4];

	int  ret;
	char szLineData[MAX_LINE_SIZE];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);    

	pBuf = (STDEQUIP_TYPE_INFO *)pLoadToBuf;

	/* load std equip type info first */
	/*RunThread_Heartbeat(RunThread_GetId(NULL));*/
	memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
	ret = Cfg_ProfileGetLine(pCfg, STD_EQUIP_TYPE_INFO, 
		szLineData, MAX_LINE_SIZE);
	if (ret == -1)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--LoadStdEquipConfigProc: ERROR: STD_EQUIP_TYPE_INFO "
			"SECTION not found in the Solution Cfg file!\n", __FILE__);
		return ERR_CFG_BADCONFIG;
	}
	else if (ret == 0)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--LoadStdEquipConfigProc: ERROR: STD_EQUIP_TYPE_INFO "
			"SECTION should not be empty!\n", __FILE__);
		return ERR_CFG_BADCONFIG;
	}
	ret = ParseTypeInfoTableProc(szLineData, pBuf);
	if (ret != 0)
	{
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_ERROR, 
			"[%s]--LoadStdEquipConfigProc: ERROR: Get std equip type "
			"info failed!/n/r", __FILE__);
		return ERR_CFG_BADCONFIG;
	}

	/* then load tables */
	DEF_LOADER_ITEM(&loader[0], 
		NULL, &(pBuf->iSampleSigNum), 
		SAMPLING_SIGNAL_INFO,&(pBuf->pSampleSigInfo), 
		ParseSampleSigTableProc);

	DEF_LOADER_ITEM(&loader[1], 
		NULL, &(pBuf->iCtrlSigNum), 
		CTRL_SIGNAL_INFO,&(pBuf->pCtrlSigInfo), 
		ParseControlSigTableProc);

	DEF_LOADER_ITEM(&loader[2], 
		NULL, &(pBuf->iSetSigNum), 
		SETTING_SIGNAL_INFO,&(pBuf->pSetSigInfo), 
		ParseSetSigTableProc);

	DEF_LOADER_ITEM(&loader[3], 
		NULL, &(pBuf->iAlarmSigNum), 
		ALARM_SIGNAL_INFO, &(pBuf->pAlarmSigInfo), 
		ParseAlarmSigTableProc);


	if (Cfg_LoadTables(pCfg,4,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;	
}

/*==========================================================================*
 * FUNCTION : Cfg_LoadStdEquipConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char                 *szConfigFile : 
 *            OUT STDEQUIP_TYPE_INFO  *pLoadToBuf   : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 18:39
 *==========================================================================*/
int Cfg_LoadStdEquipConfig(IN char *szConfigFile, OUT STDEQUIP_TYPE_INFO *pLoadToBuf)
{
	int ret;
	char szConfigFileName[MAX_FILE_PATH] = "standard/";
    char szFullPath[MAX_FILE_PATH];
	
	strcat(szConfigFileName, szConfigFile);
	
	Cfg_GetFullConfigPath(szConfigFileName, szFullPath, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(szFullPath, LoadStdEquipConfigProc, pLoadToBuf);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

 /*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : cfg_helper.h
 *  CREATOR  : LinTao                   DATE: 2004-09-09
 *  VERSION  : V1.00
 *  PURPOSE  : common head file for Cfg Module(used by Cfg Module only)            
 *
 *  HISTORY  :
 *==========================================================================*/
#ifndef __CFG_HELPER_H_2004_09_09__
#define __CFG_HELPER_H_2004_09_09__

#define CFG_LOAD_BASIC		"STD CFG LDR"
#define CFG_LOAD_SOLUTION	"SOLUT LDR"
#define CFG_LOAD_EQUIP		"EQUIP LDR"
#define CFG_LOADER			"CFG LDR"
#define CFG_LOAD_RUN		"RUN CFG LDR"

/* define splitter charater */
#define SPLITTER		 ('\t')
#define SUB_SPLITTER	 (',')	   /* for relevant sampler list */
#define SUB_SUB_SPLITTER ('.')     /* for relevant sampler list */

/* define empty field value */
#define EMPTY_NUMVALUE	(-1)




/* config file dir and name definition */
//#define CONFIG_DIR					"/config/"
//#define CONFIG_BAK_DIR				"/config_bak/"
//#define CONFIG_DEFAULT_DIR			"/config_default/"

#define BASIC_STD_CFGFILE			"standard/BasicStandard.cfg"
#define SOLUTION_CFGFILE			"solution/MonitoringSolution.cfg"
#define USER_DEF_PAGES_CFGFILE			"private/cfg_ui/cfg_ui_private.cfg"
/*for G3*/
//#define USER_DEF_LCDCFG				"private/lcd/lcd_private.cfg"
/* for runtime changed config info */
#define RUN_CFGFILE					"run/MainConfig.run"
 

/* section name definition */
/* sections for lang resource files */
#define RES_ID_NUM					"[RES_ID_NUM]"
#define RES_INFO					"[RES_INFO]"
/*for G3*/
/*#define DISP_INFORMATION	"[DISPLAYINFO]"
#define ICO_INFO		"[ICOINFO]"
#define MODULE_INFO		"[MODULEINFO]"
#define SETTING_INFO		"[SETTINGINFO]"
#define DISTRIB_INFO		"[DISTRIBINFO]"
#define TEMP_INFO		"[TEMPINFO]"
#define BATT_INFO		"[BATTERYINFO]"*/
/***/

/* sections for BASIC_STD_CFGFILE */
#define STD_PORT_TYPE_NUM			"[STD_PORT_TYPE_NUM]"
#define STD_PORT_TYPE_INFO			"[STD_PORT_TYPE_INFO]"
#define STD_SAMPLER_TYPE_NUM		"[STD_SAMPLER_TYPE_NUM]"
#define STD_SAMPLER_TYPE_INFO		"[STD_SAMPLER_TYPE_INFO]"
#define STD_PROTOCOL_TYPE_NUM		"[STD_PROTOCOL_TYPE_NUM]"
#define STD_PROTOCOL_TYPE_INFO		"[STD_PROTOCOL_TYPE_INFO]"
#define STD_EQUIP_TYPE_NUM			"[STD_EQUIP_TYPE_NUM]"
#define STD_EQUIP_CFGFILE_MAP		"[STD_EQUIP_CFGFILE_MAP]"
#define STD_DEVICE_TYPE_MAP			"[STD_DEVICE_TYPE_INFO]"

/* sections for std equip config files */
#define STD_EQUIP_TYPE_INFO			"[EQUIPMENT_TYPE_INFO]"
#define SAMPLING_SIGNAL_NUM			"[SAMPLING_SIGNAL_NUM]"
#define SAMPLING_SIGNAL_INFO		"[SAMPLING_SIGNAL_INFO]"
#define CTRL_SIGNAL_NUM			    "[CTRL_SIGNAL_NUM]"
#define CTRL_SIGNAL_INFO			"[CTRL_SIGNAL_INFO]"
#define SETTING_SIGNAL_NUM			"[SETTING_SIGNAL_NUM]"
#define SETTING_SIGNAL_INFO			"[SETTING_SIGNAL_INFO]"
#define ALARM_SIGNAL_NUM			"[ALARM_NUM]"
#define ALARM_SIGNAL_INFO			"[ALARM_INFO]"

/* sections for SOLUTION_CFGFILE */
#define CONFIG_INFOR				"[CONFIG_INFORMATION]"
#define LANGUAGE_INFO				"[LANGUAGE_INFO]"
#define SITE_BASE_INFO				"[SITE_INFO]"
#define COMM_PORT_NUM				"[COMMUNICATION_PORT_NUM]"
#define COMM_PORT_INFO				"[COMMUNICATION_PORT_INFO]"
#define SAMPLER_UNIT_NUM			"[SAMPLER_UNIT_NUM]"
#define SAMPLER_UNIT_INFO			"[SAMPLER_UNIT_INFO]"
#define EQUIPMENT_NUM				"[EQUIPMENT_NUM]"
#define EQUIPMENT_INFO				"[EQUIPMENT_INFO]"
#define EQUIP_CUSTOM_CHANNEL_NUM	"[CUSTOMIZED_EQUIPMENT_SIGNAL_NUM]"
#define EQUIP_CUSTOM_CHANNEL_INFO	"[CUSTOMIZED_EQUIPMENT_SIGNAL_INFO]"

/* sections for USER DEF PAGES CONFIG FILE */
#define PAGE_INFORMATION	"[PAGE_INFORMATION]"
#define SETTING_SIGNAL		"[SETTING_SIGNAL]"

/* sections for RUN CONFIG FILE */
#define RUNTIME_CHANGED_INFO				"[CHANGED_INFO]"


/* common interfaces */
int ExpressionParser(IN char *szBuf, 
					 OUT int *piExpLen, 
					 OUT EXP_ELEMENT **pExprssion);

LANG_TEXT *GetLangText(IN int iResourceID, 
					   IN int iLen, 
					   IN LANG_TEXT *pLangTextList);

/* load config file interface */
int Cfg_LoadRunConfig(void);
int Cfg_LoadBasicStdConfig(char *szConfigFile);
int Cfg_LoadSolutionConfig(char *szConfigFile);
int Cfg_LoadStdEquipConfig(IN char *szConfigFile, 
						   OUT STDEQUIP_TYPE_INFO *pLoadToBuf);

void Cfg_DumpExp(OUT EXP_ELEMENT *pDst, IN OUT EXP_ELEMENT **ppExp, 
				 IN int nExpression);
void Cfg_FreeTempExpression(IN int iExpLenth, IN EXP_ELEMENT **pExpression);

char Cfg_ReplaceIllegalChr(int c);

#endif //__CFG_HELPER_H_2004_09_09__

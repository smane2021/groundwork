/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : test_cfg_load_stdbasic.c
 *  CREATOR  : LinTao                   DATE: 2004-09-24 14:40
 *  VERSION  : V1.00
 *  PURPOSE  : to test cfg mgmt module
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "cfg_mgmt.h"
#include "cfg_helper.h"

/* simulate app variables and functions used by libapp */
char g_szACUConfigDir[MAX_FILE_PATH] = "/home/l23632/ACU/src/config";

SITE_INFO g_SiteInfo;
SERVICE_MANAGER		g_ServiceManager;
TIME_SRV_INFO g_sTimeSrvInfo;  

int UpdateACUTime(time_t* pTimeRet)
{
	return stime(pTimeRet);;
}

int Equip_Control(IN int nSenderType, IN char *pszSenderName,
				  IN int nSendDirectly,
				  IN int nEquipID, IN int nSigType, IN int nSigID, 
				  IN VAR_VALUE *pCtrlValue, IN DWORD dwTimeout)
{
	UNUSED(nSenderType);
	UNUSED(pszSenderName);
	UNUSED(nSendDirectly);
	
	UNUSED(nEquipID);
	UNUSED(nSigType);
	UNUSED(nEquipID);

	UNUSED(pCtrlValue);
	UNUSED(dwTimeout);

	return TRUE;
}
/* end of simulation */


void Print_Exp(EXP_ELEMENT *pEle)
{
	EXP_VARIABLE *pVar;
	EXP_OPERATOR *pOperator;

	switch(pEle->byElementType)
	{
	case	EXP_ETYPE_VARIABLE:
		pVar = (EXP_VARIABLE *)pEle->pElement; 
		printf("Element Type: VARIABLE\n");
		printf("Element Value: [%d, %d, %d]\n", pVar->iRelevantEquipIndex,
			pVar->iSigType, pVar->iSigID);
		break;

	case EXP_ETYPE_OPERATOR:
		pOperator = (EXP_OPERATOR *)pEle->pElement;
		printf("Element Type: OPERATOR\n");
		switch(pOperator->byOperatorType)
		{
		case EXP_ADD:
			printf("Element Value: +\n");
			break;

		case EXP_SUB:
			printf("Element Value: -\n");
			break;

		case EXP_MUL:
			printf("Element Value: *\n");
			break;

		case EXP_DIV:
			printf("Element Value: \\\n");
			break;

		case EXP_GREATER:
			printf("Element Value: >\n");
			break;

		case EXP_LESS:
			printf("Element Value: <\n");
			break;

		case EXP_GREATER_EQUAL:
			printf("Element Value: >=\n");
			break;

		case EXP_LESS_EQUAL:
			printf("Element Value: <=\n");
			break;

		case EXP_EQUAL:
			printf("Element Value: =\n");
			break;

		case EXP_AND:
			printf("Element Value: &\n");
			break;

		case EXP_OR:
			printf("Element Value: |\n");
			break;

		case EXP_NOT:
			printf("Element Value: !\n");
			break;

		default:
			printf("Invalid Operator!\n");
		}
		break;

	case EXP_ETYPE_CONST:
		printf("Element Type: CONST\n");
		printf("Element Value: %6.4f\n", *((EXP_CONST *)pEle->pElement));
		break;

	case EXP_ETYPE_DUMMY:
		printf("Invalid element type: EXP_ETYPE_DUMMY\n");
		break;

	case EXP_ETYPE_INVALID:
		printf("Invalid element type: EXP_ETYPE_INVALID\n");
		break;

	default:
		printf("Invalid elemen type!\n");
	}
}

void Print_Solution()
{
	int ret;
	int i, j;

	EQUIP_INFO *pEquip;
	RELEVANT_SAMPLER *pRelevantSampler;

	STDSAMPLER_INFO *pStdSampler;
	EXP_ELEMENT *pEle;

	printf("\n------------------------------------------------------------\n");
	printf("******                   Solution Config              ******\n");
	printf("------------------------------------------------------------\n");

	printf("$Equip Info:\n");
	printf("Record Number: %d\n", g_SiteInfo.iEquipNum);


	pEquip = g_SiteInfo.pEquipInfo;
	for (i = 0; i < g_SiteInfo.iEquipNum; i++, pEquip++)
	{
		printf("********   Record #%d  started  *************\n", i + 1);
		printf("EquipID: %d\n", pEquip->iEquipID);
		printf("English Name: %s\n", pEquip->pEquipName->pFullName[0]);
		printf("Locale Name: %s\n", pEquip->pEquipName->pFullName[1]);
		printf("English Abbr Name: %s\n", pEquip->pEquipName->pAbbrName[0]);
		printf("Locale Abbr Name: %s\n", pEquip->pEquipName->pAbbrName[1]);
		printf("Equip Type ID: %d\n", pEquip->iEquipTypeID);
		printf("Serial Number: %s\n", pEquip->szSerialNumber);
		printf("Defualt workstate: %x\n", pEquip->iDefaultWorkState);

		printf("\n\rAlarm Filter Expression:\n");

		pEle = pEquip->pAlarmFilterExpressionCfg;
		for (j = 0; j < pEquip->iAlarmFilterExpression; j++,pEle++)
		{
			Print_Exp(pEle);
		}

		printf("\n\rRelevant Sampler List:\n");
		pRelevantSampler = pEquip->pRelevantSamplerList;
		for (j = 0; j < pEquip->iRelevantSamplerListLength; j++,pRelevantSampler++)
		{
			printf("Sampler ID: %d\n", pRelevantSampler->iSamplerID);
			printf("Channel No: %d\n", pRelevantSampler->iStartControlChannel);
		}

		printf("\n\rRelevant Equip List:\n");
		for (j = 0; j < pEquip->iRelevantEquipListLength; j++)
		{
			printf("Equip ID: %d\n", pEquip->pRelevantEquipList[j]);
		}

		printf("********   Record #%d  ended  ***************\n\n", i + 1);

	}
}


int main(IN int argc, OUT char *argv[])
{
	char szFullPath[MAX_FILE_PATH];  //full config file name
	int ret;
	int i, j;

	EQUIP_INFO *pEquip;
	RELEVANT_SAMPLER *pRelevantSampler;

	STDSAMPLER_INFO *pStdSampler;

	/*STDEQUIP_TYPE_INFO stdEquip;
	CTRL_SIG_INFO *pCtrlSig;*/

	EXP_ELEMENT *pEle;


	/* test cfg_load_solution */
	if (argc == 2 && strcmp(argv[1],"SL") == 0)
	{
        Cfg_GetFullConfigPath(SOLUTION_CFGFILE, szFullPath, MAX_FILE_PATH);
		
		ret = Cfg_LoadSolutionConfig(szFullPath);

		if (ret != ERR_CFG_OK)
		{
			printf("Failed to load Solution Config!\n");
			return -1;
		}

		else
		{
			Print_Solution();

			return 0;
		}
	}
	else if(argc == 2 && strcmp(argv[1], "SB") == 0)
	{
		ret = Cfg_LoadBasicStdConfig("BasicStandard.cfg");

		if (ret != ERR_CFG_OK)
		{
			printf("Failed to load Std Basic Config!\n");
			return -1;
		}
		else
		{
			printf("\n-----------------------------------------------\n");
			printf("******          Std basic Config         ******\n");
			printf("-----------------------------------------------\n");

			printf("$Std Sampler Info:\n");
			printf("Record Number: %d\n", g_SiteInfo.iStdSamplerNum);

			pStdSampler = g_SiteInfo.pStdSamplerInfo;
			for (i = 0; i < g_SiteInfo.iStdSamplerNum; i++, pStdSampler++)
			{
				printf("********   Record #%d  started  *************\n", i + 1);
				printf("Std Sampler ID: %d\n", pStdSampler->iStdSamplerID);
				printf("Std Sampler Name: %s\n", pStdSampler->szStdSamplerName);
				printf("Sampler Driver: %s\n", pStdSampler->szStdSamplerDriver);
				printf("Std Sampler Attribute: 0x%x\n", pStdSampler->iSamplerAttr);
				printf("********   Record #%d  ended  ***************\n\n", i + 1);
			}

			return 0;
		}
	}
	else if(argc == 2 && strcmp(argv[1], "ALL") == 0)
	{
		ret = Cfg_InitialConfig();
		if (ret != ERR_CFG_OK)
		{
			printf("Load All config failed!\n");

			Cfg_UnloadConfig();
			return -1;
		}

		//Print_Solution();

		Cfg_UnloadConfig();

	}
	else
	{
		printf("Usage: < ALL | SL | SB | SE (std equip filename) >");
		printf("ALL: loading the whole config.\n");
		printf("SL: loading solution config file.\n");
		printf("SB: loading basic std config file.\n");
		printf("SE: loading std equip config file.\n");

		return -1;
	}

}

/*==========================================================================*
 *     Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                      ALL RIGHTS RESERVED
 * 
 *   PRODUCT  : ACU(Advanced Controller Unit)
 * 
 *   FILENAME : cfg_load_stdbasic.c
 *   CREATOR  : LinTao                   DATE: 2004-09-11 17:09
 *   VERSION  : V1.00
 *   PURPOSE  : to load basic standard config file
 * 
 * 
 *   HISTORY  :
 * 
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"


#include "cfg_mgmt.h"
#include "cfg_helper.h"

/*==========================================================================*
 *  FUNCTION : ParseStdPortTableProc
 *  PURPOSE  : parse std Port table of BASIC_STD_CFGFILE
 *  CALLS    : 
 *  CALLED BY: 
 *  ARGUMENTS: IN  char          *szBuf       : line data to parse
 *             OUT STDPORT_INFO  *pStructData : 
 *  RETURN   : int : loaded failed field ID, zero for success
 *  COMMENTS : 
 *  CREATOR  : LinTao                   DATE: 2004-09-11
 *==========================================================================*/
static int ParseStdPortTableProc(IN char *szBuf, OUT STDPORT_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Type ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, (char)SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdPortTableProc: ERROR: Type ID is not a "
			"number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iPortTypeID = atoi(pField);

	/* 2.Type Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->szTypeName = NEW(char, 1);
		if (pStructData->szTypeName == NULL)
		{
			AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
				"[%s]--ParseStdPortTableProc: ERROR: "
				"no memory!\n", __FILE__);
			return 2;
		}
		*pStructData->szTypeName = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
				"[%s]--ParseStdPortTableProc: ERROR: Type Name is "
				"NULL!\n", __FILE__);

			return 2;
		}
		pStructData->szTypeName = NEW_strdup(pField);
		if (pStructData->szTypeName == NULL)
		{
			AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
				"[%s]--ParseStdPortTableProc: ERROR: no memory to load "
				"Type Name info!\n", __FILE__);

			return 2; 
		}
	}

	/* 3.Base Port Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdPortTableProc: ERROR: Base Port Type is not "
			"a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iBasicType = atoi(pField);

	/* 4.Port Driver field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdPortTableProc: ERROR: Port Driver is "
			"NULL!\n", __FILE__);

		return 4;
	}
	pStructData->szPortAccessingDriver = NEW_strdup(pField);
	if (pStructData->szPortAccessingDriver == NULL)
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdPortTableProc: ERROR: no memory to load "
			"Port Accessing Driver info!\n", __FILE__);

		return 4; 
	}

	return 0;
}

/*==========================================================================*
 *  FUNCTION : ParseStdSamplerTableProc
 *  PURPOSE  : parse Std Sampler table of BASIC_STD_CFGFILE
 *  CALLS    : 
 *  CALLED BY: 
 *  ARGUMENTS: IN char              *szBuf       : line data to parse
 *             OUT STDSAMPLER_INFO  *pStructData : 
 *  RETURN   : int : loaded failed field ID, zero for success
 *  COMMENTS : 
 *  CREATOR  : LinTao                   DATE: 2004-09-11
 *==========================================================================*/
static int ParseStdSamplerTableProc(IN char *szBuf, OUT STDSAMPLER_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Standard Sampler ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdSamplerTableProc: ERROR: Std Sampler ID is "
			"not a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iStdSamplerID = atoi(pField);

	/* 2.Standard Sampler Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdSamplerTableProc: ERROR: Std Sampler Name is"
			" NULL!\n", __FILE__);

		return 2;
	}
	pStructData->szStdSamplerName = NEW_strdup(pField);
	if (pStructData->szStdSamplerName == NULL)
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdSamplerTableProc: ERROR: no memory to load "
			"std Sampler Name info!\n", __FILE__);

		return 2; 
	}

	/* 3.Sampler Driver field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdSamplerTableProc: ERROR: Sampler Driver is "
			"NULL!\n", __FILE__);

		return 3;    
	}
	pStructData->szStdSamplerDriver = NEW_strdup(pField);
	if (pStructData->szStdSamplerDriver == NULL)
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdSamplerTableProc: ERROR: no memory to load "
			"std Sampler Driver info!\n", __FILE__);

		return 3; 
	}

	/* 4.Standard Sampler Attribute field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iSamplerAttr = 0;
	}
	else
	{
		if ((*pField != '0') || (*(pField + 1) != 'x' && *(pField + 1) != 'X'))
		{
			AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
				"[%s]--ParseStdSamplerTableProc: ERROR: Std Sampler "
				"Attribute is invalid!\n", __FILE__);

			return 4;
		}
		sscanf(pField, "%x", &pStructData->iSamplerAttr);
	}

	return 0;
}

/*==========================================================================*
 *  FUNCTION : ParseStdDCPTableProc
 *  PURPOSE  : parse std DCP table of BASIC_STD_CFGFILE
 *  CALLS    : 
 *  CALLED BY: 
 *  ARGUMENTS: IN  char         *szBuf       : lint data to parse
 *             OUT STDDCP_INFO  *pStructData : 
 *  RETURN   : int : loaded failed field ID, zero for success
 *  COMMENTS : 
 *  CREATOR  : LinTao                   DATE: 2004-09-11
 *==========================================================================*/
static int ParseStdDCPTableProc(IN char *szBuf, OUT STDDCP_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.DCP ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdDCPTableProc: ERROR: Std DCP ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iDCPID = atoi(pField);

	/* 2.DCP Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdDCPTableProc: ERROR: DCP Name is NULL!\n",
			__FILE__);

		return 2;    
	}
	pStructData->szDCPName = NEW_strdup(pField);
	if (pStructData->szDCPName == NULL)
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdDCPTableProc: ERROR: no memory to load "
			"DCP Name info!\n", __FILE__);

		return 2;  
	}

	/* 3.Maximum HLMS field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iMaxHLMSAllowed = 5;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
				"[%s]--ParseStdDCPTableProc: ERROR: Max HLMS is not a "
				"number!\n", __FILE__);

			return 3;    /* not a num, error */
		}
		pStructData->iMaxHLMSAllowed = atoi(pField);
	}

	/* 4.Attribute field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iDCPAttr = 0;
	}
	else
	{
		if ((*pField != '0') || (*(pField + 1) != 'x' && *(pField + 1) != 'X'))
		{
			AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
				"[%s]--ParseStdDCPTableProc: ERROR: Std DCP Attribute "
				"is invalid!\n", __FILE__);

			return 4;
		}
		sscanf(pField, "%x", &pStructData->iDCPAttr);
	}

	/* 5.DCP Driver filed */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdDCPTableProc: ERROR: DCP Driver is NULL!\n",
			__FILE__);

		return 5;    
	}
	pStructData->szDCPDriver = NEW_strdup(pField);
	if (pStructData->szDCPDriver == NULL)
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdDCPTableProc: ERROR: no memory to load "
			"DCP Driver info!\n", __FILE__);

		return 5;  
	}

	return 0;
}

/*==========================================================================*
 *  FUNCTION : ParseStdEquipTypeMapTableProc
 *  PURPOSE  : parse std equip type map table of BASIC_STD_CFGFILE
 *  CALLS    : 
 *  CALLED BY: 
 *  ARGUMENTS: IN  char                   *szBuf       : 
 *             OUT STDEQUIP_TYPEMAP_INFO  *pStructData : 
 *  RETURN   : int : loaded failed field ID, zero for success
 *  COMMENTS : 
 *  CREATOR  : LinTao                   DATE: 2004-09-11
 *==========================================================================*/
static int ParseStdEquipTypeMapTableProc(IN char *szBuf, OUT STDEQUIP_TYPEMAP_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.StdEquip Type ID filed */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdEquipTypeMapTableProc: ERROR: Equip type ID "
			"is not a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iEquipTypeID = atoi(pField);

	/* 2.Cfg File Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdEquipTypeMapTableProc: ERROR: Cfg File Name "
			"is NULL!\n", __FILE__);

		return 2;    
	}
	pStructData->szCfgFileName = NEW_strdup(pField);
	if (pStructData->szCfgFileName == NULL)
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseStdEquipTypeMapTableProc: ERROR: no memory to "
			"load Cfg File Name info!\n", __FILE__);

		return 2;  
	}

	return 0;
}


#ifdef PRODUCT_INFO_SUPPORT
static int ParseDeviceTypeMapTableProc(IN char *szBuf, OUT DEVICE_TYPE_MAP *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Device Part Number filed */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseDeviceTypeMapTableProc: ERROR: Device Part Number "
			"is NULL!\n", __FILE__);

		return 1;    
	}
	pStructData->szPartNumber = NEW_strdup(pField);
	if (pStructData->szPartNumber == NULL)
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseDeviceTypeMapTableProc: ERROR: no memory to "
			"load Device Part Number info!\n", __FILE__);

		return 1;  
	}


	/* 2.Device Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseDeviceTypeMapTableProc: ERROR: Device Name "
			"is NULL!\n", __FILE__);

		return 2;    
	}
	pStructData->szDeviceName = NEW_strdup(pField);
	if (pStructData->szDeviceName == NULL)
	{
		AppLogOut(CFG_LOAD_BASIC, APP_LOG_ERROR, 
			"[%s]--ParseDeviceTypeMapTableProc: ERROR: no memory to "
			"load Device Name info!\n", __FILE__);

		return 2;  
	}

	return 0;
}
#endif //PRODUCT_INFO_SUPPORT

/*==========================================================================*
 *  FUNCTION : LoadBasicStdConfigProc
 *  PURPOSE  : 
 *  CALLS    : 
 *  CALLED BY: Cfg_LoadConfigFile
 *  ARGUMENTS: void       *pCfg       : 
 *             SITE_INFO  *pLoadToBuf : 
 *  RETURN   : int : error code defined in the err_code.h , 0 for success
 *  COMMENTS : callback function
 *  CREATOR  : LinTao                   DATE: 2004-09-11
 *==========================================================================*/
int LoadBasicStdConfigProc(void *pCfg, void *pLoadToBuf)
{
	SITE_INFO *pBuf;
	CONFIG_TABLE_LOADER loader[4];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (SITE_INFO *)pLoadToBuf;


	DEF_LOADER_ITEM(&loader[0],
		STD_PORT_TYPE_NUM, &(pBuf->iStdPortNum), 
		STD_PORT_TYPE_INFO, &(pBuf->pStdPortInfo), 
		ParseStdPortTableProc);

	DEF_LOADER_ITEM(&loader[1], 
		STD_SAMPLER_TYPE_NUM, &(pBuf->iStdSamplerNum), 
		STD_SAMPLER_TYPE_INFO, &(pBuf->pStdSamplerInfo), 
		ParseStdSamplerTableProc);

	DEF_LOADER_ITEM(&loader[2],                      //Data Communication Protocol
		STD_PROTOCOL_TYPE_NUM, &(pBuf->iStdDCPNum), 
		STD_PROTOCOL_TYPE_INFO, &(pBuf->pStdDCPInfo), 
		ParseStdDCPTableProc);

    //STD_EQUIP_TYPE_NUM
	DEF_LOADER_ITEM(&loader[3], 
		NULL, &(pBuf->iEquipTypeMapNum), 
		STD_EQUIP_CFGFILE_MAP, &(pBuf->pEquipTypeMap), 
		ParseStdEquipTypeMapTableProc);

	
//#ifdef PRODUCT_INFO_SUPPORT
//	DEF_LOADER_ITEM(&loader[4], 
//		NULL, &(pBuf->iDeviceTypes), 
//		STD_DEVICE_TYPE_MAP, &(pBuf->pDeviceTypeMap), 
//		ParseDeviceTypeMapTableProc);
//
//	if (Cfg_LoadTables(pCfg,5,loader) != ERR_CFG_OK)
//	{
//		return ERR_CFG_FAIL; 
//	}
//
//#else

	if (Cfg_LoadTables(pCfg,4,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL; 
	}

//#endif //PRODUCT_INFO_SUPPORT


	return ERR_CFG_OK;	
}

/*==========================================================================*
 *  FUNCTION : Cfg_LoadBasicStdConfig
 *  PURPOSE  : load basic standard config file
 *  CALLS    : 
 *  CALLED BY: 
 *  ARGUMENTS: char  *szConfigFile : baisc standard config file name
 *  RETURN   : int : int : error code defined in the err_code.h , 0 for success
 *  COMMENTS : 
 *  CREATOR  : LinTao                   DATE: 2004-09-11
 *==========================================================================*/
int Cfg_LoadBasicStdConfig(char *szConfigFile)
{
	int ret;

	ret = Cfg_LoadConfigFile(
		szConfigFile, 
		LoadBasicStdConfigProc,
		&g_SiteInfo);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

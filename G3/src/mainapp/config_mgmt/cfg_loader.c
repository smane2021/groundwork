 /*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : cfg_loader.c
 *  CREATOR  : LinTao                   DATE: 2004-09-11
 *  VERSION  : V1.00
 *  PURPOSE  : Config Management main module
 *
 *  HISTORY  :
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "cfg_mgmt.h"
#include "cfg_helper.h"

HIS_DATA_RECORD_LOAD    g_stLoadCurrentData[ONE_MONTH_HOUR];//load current trend curve data

// changed by Frank Wu,20131213,3/8, for keep the same used rated current percent between WEB and LCD
WEB_LCD_SHARE_DATA g_stWebLcdShareData;

#ifdef _DEBUG
//#define		_DEBUG_CFG_LOADER		1
#endif

//#ifndef _SUPPORT_THREE_LANGUAGE
//
//    #define	_SUPPORT_THREE_LANGUAGE
//
//#endif


//moved to main.c, maofuhua, 2004-11-17
//SITE_INFO	g_SiteInfo;  /* to load config info */

//Used to load run config file
static HANDLE s_hMutexLoadRunConfig = NULL;

HANDLE GetMutexLoadRunConfig(void)
{
	return s_hMutexLoadRunConfig;
}


static BOOL IsBoundChr_S(int c)
{
	return ((char)c == '[' || (char)c == ']');
}

static BOOL IsBoundChr_BS(int c)
{
	return ((char)c == '{' || (char)c == '}');
}

char Cfg_ReplaceIllegalChr(int c)
{
	return (((char)c == '<' || (char)c == '>' ||  /* (char)c == '\'' || */
		(char)c == '"') ? '?' : (char)c);
}

//Load User define pages info
static int Cfg_LoadUserDefPageInfo(char *szConfigFile);
static int Cfg_LoadUserDefLCDInfo(char *szConfigFile);
/*==========================================================================*
 * FUNCTION : ParseLangResourseTableProc
 * PURPOSE  : parse res table of language resource files
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN  char       *szBuf       : lint data to parse
 *            OUT LANG_TEXT  *pStructData : 
 * RETURN   : int :  loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-13 12:18
 *==========================================================================*/
static int ParseLangResourseTableProc(IN char *szBuf, OUT LANG_TEXT *pStructData)
{
	char *pField;
	char xOut[64];
	int iCodeType = g_SiteInfo.LangInfo.iCodeType;
	int  iReturnChar = 0;

	/* used as buffer */
	int iMaxLenForFull, iMaxLenForAbbr;
	char *szText;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.RES ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: Res ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iResourceID = atoi(pField);

	/* 2.max length of full name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	iMaxLenForFull = atoi(pField);
	pStructData->iMaxLenForFull = iMaxLenForFull;	

	/* 3.max length of abbr name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: Max length(abbr) "
			"is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	iMaxLenForAbbr = atoi(pField);
	pStructData->iMaxLenForAbbr = iMaxLenForAbbr;

	/* 4.full english name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: full en name "
			"is NULL!\n", __FILE__);

		return 4;
	}
	pStructData->pFullName[0] = NEW(char, iMaxLenForFull+1);
	szText = pStructData->pFullName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: no memory to load "
			"English Full Name info!\n", __FILE__);

		return 4;
	}
	strncpyz_f(szText, pField, iMaxLenForFull+1, Cfg_ReplaceIllegalChr);

	/* 5.abbr english name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: abbr english name "
			"is NULL!\n", __FILE__);

		return 5;
	}
	pStructData->pAbbrName[0] = NEW(char, iMaxLenForAbbr+1);
	szText = pStructData->pAbbrName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: no memory to load "
			"Abbr English Name info!\n", __FILE__);

		return 5;
	}
	strncpyz_f(szText, pField, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);

	/* 6.full locale name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: full locale name"
			" is NULL!\n", __FILE__);

		return 6;
	}
	pStructData->pFullName[1] = NEW(char, iMaxLenForFull+1);
	szText = pStructData->pFullName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: no memory to load "
			"Locale Full Name info!\n", __FILE__);

		return 6;
	}

	//if(bGB2312)
	//{
	//	
	//	iReturnChar = UTF8toGB2312(pField,64,xOut,32);
	//	if(iReturnChar < 0)
	//	{
	//		strncpyz_f(szText, pField, iMaxLenForFull+1, Cfg_ReplaceIllegalChr);
	//	}
	//	else
	//	{
	//		strncpyz_f(szText, xOut, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);
	//	}
	//}
	//else
	//{
	//if(iCodeType == ENUM_KOI8R)
	//{		
	//	iReturnChar = UTF8toKOI8(pField,128,xOut,64);
	//	strncpyz_f(szText, xOut, iMaxLenForFull+1, Cfg_ReplaceIllegalChr);		
	//}
	//else
	//{
		strncpyz_f(szText, pField, iMaxLenForFull+1, Cfg_ReplaceIllegalChr);
	//}
	//}
	

	/* 7.abbr locale name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: abbr locale name "
			"is NULL!\n", __FILE__);

		return 7;
	}
	pStructData->pAbbrName[1] = NEW(char, iMaxLenForAbbr+1);
	szText = pStructData->pAbbrName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseLangResourseTableProc: ERROR: no memory to load "
			"Locale Abbr Name info!\n", __FILE__);

		return 7;
	}
	if(iCodeType == ENUM_GB2312)
	{
		iReturnChar = UTF8toGB2312(pField,64,xOut,32);
		
		//printf("iReturnChar = %d, xOut = %s\n", iReturnChar, xOut);
		/*if(iReturnChar < 0)
		{
			strncpyz_f(szText, pField, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);
		}
		else
		{*/
			strncpyz_f(szText, xOut, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);
		//}
	}
	else 	if(iCodeType == ENUM_KOI8R)
	{		
		iReturnChar = UTF8toKOI8(pField,64,xOut,32);
		strncpyz_f(szText, xOut, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);		
	}
	else
	{
		strncpyz_f(szText, pField, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);
	}	
	

	return 0;
}


/*==========================================================================*
* FUNCTION : ParseLangResourseTableProc2
* PURPOSE  : parse res table of language resource files
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN  char       *szBuf       : lint data to parse
*            OUT LANG_TEXT  *pStructData : 
* RETURN   : int :  loaded failed field ID, zero for success
* COMMENTS : 
* CREATOR  : wj                   DATE: 2006-04-27 12:18
*==========================================================================*/
static int ParseLangResourseTableProc2(IN char *szBuf, OUT LANG_TEXT *pStructData)
{
    char *pField;

    /* used as buffer */
    int iMaxLenForFull, iMaxLenForAbbr;
    char *szText;

    ASSERT(szBuf);
    ASSERT(pStructData);

    /* 1.RES ID field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if ((*pField < '0') || (*pField > '9'))
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: Res ID is not "
            "a number!\n", __FILE__);

        return 1;    /* not a num, error */
    }
    //pStructData->iResourceID = atoi(pField);

    /* 2.max length of full name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if ((*pField < '0') || (*pField > '9'))
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: Max length(full) "
            "is not a number!\n", __FILE__);

        return 2;    /* not a num, error */
    }
    iMaxLenForFull = atoi(pField);
    //pStructData->iMaxLenForFull = iMaxLenForFull;	

    /* 3.max length of abbr name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if ((*pField < '0') || (*pField > '9'))
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: Max length(abbr) "
            "is not a number!\n", __FILE__);

        return 3;    /* not a num, error */
    }
    iMaxLenForAbbr = atoi(pField);
    //pStructData->iMaxLenForAbbr = iMaxLenForAbbr;

    /* 4.full english name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: full en name "
            "is NULL!\n", __FILE__);

        return 4;
    }
    //pStructData->pFullName[0] = NEW(char, iMaxLenForFull+1);
    //szText = pStructData->pFullName[0];
    /*if (szText == NULL)
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc: ERROR: no memory to load "
            "English Full Name info!\n", __FILE__);

        return 4;
    }*/
    //strncpyz_f(szText, pField, iMaxLenForFull+1, Cfg_ReplaceIllegalChr);

    /* 5.abbr english name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: abbr english name "
            "is NULL!\n", __FILE__);

        return 5;
    }
    //pStructData->pAbbrName[0] = NEW(char, iMaxLenForAbbr+1);
    //szText = pStructData->pAbbrName[0];
    //if (szText == NULL)
    //{
    //    AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
    //        "[%s]--ParseLangResourseTableProc: ERROR: no memory to load "
    //        "Abbr English Name info!\n", __FILE__);

    //    return 5;
    //}
    //strncpyz_f(szText, pField, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);

    /* 6.full locale name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: full locale name"
            " is NULL!\n", __FILE__);

        return 6;
    }
    pStructData->pFullName[2] = NEW(char, iMaxLenForFull+1);
    szText = pStructData->pFullName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: no memory to load "
            "Locale Full Name info!\n", __FILE__);

        return 6;
    }
    strncpyz_f(szText, pField, iMaxLenForFull+1, Cfg_ReplaceIllegalChr);

    /* 7.abbr locale name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: abbr locale name "
            "is NULL!\n", __FILE__);

        return 7;
    }
    pStructData->pAbbrName[2] = NEW(char, iMaxLenForAbbr+1);
    szText = pStructData->pAbbrName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
            "[%s]--ParseLangResourseTableProc2: ERROR: no memory to load "
            "Locale Abbr Name info!\n", __FILE__);

        return 7;
    }
    strncpyz_f(szText, pField, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);

    return 0;
}
/*==========================================================================*
 * FUNCTION : LoadLangResourseConfigProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: IN char        *szBuf : 
 *            OUT LANG_FILE  *pLangFile     : 
 * RETURN   : int :  error code defined in the err_code.h , 0 for success
 * COMMENTS : callback function
 * CREATOR  : LinTao                   DATE: 2004-09-13 16:50
 *==========================================================================*/
static int LoadLangResourseConfigProc(IN void *pProf, OUT void *pLangFile)
{
	LANG_FILE *pBuf;
	CONFIG_TABLE_LOADER loader[1];

	ASSERT(pProf);
	ASSERT(pLangFile);

	pBuf = (LANG_FILE *)pLangFile;

	/* load table */
	DEF_LOADER_ITEM(loader, 
		NULL, &pBuf->iLangTextNum, 
		RES_INFO,&pBuf->pLangText,
		ParseLangResourseTableProc);

	if (Cfg_LoadTables(pProf,1,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	//TRACE("pBuf->iLangTextNum is %d\n", pBuf->iLangTextNum);

	return ERR_CFG_OK;	
}

/*==========================================================================*
* FUNCTION : LoadLangResourseConfigProc2
* PURPOSE  : 
* CALLS    : 
* CALLED BY: Cfg_LoadConfigFile
* ARGUMENTS: IN char        *szBuf : 
*            OUT LANG_FILE  *pLangFile     : 
* RETURN   : int :  error code defined in the err_code.h , 0 for success
* COMMENTS : callback function
* CREATOR  : wj                   DATE: 2006-04-27 16:50
*==========================================================================*/
static int LoadLangResourseConfigProc2(IN void *pProf, OUT void *pLangFile)
{
    LANG_FILE *pBuf;
    CONFIG_TABLE_LOADER loader[1];

    ASSERT(pProf);
    ASSERT(pLangFile);

    pBuf = (LANG_FILE *)pLangFile;

    /* load table */
    DEF_LOADER_ITEM(loader, 
        NULL, &pBuf->iLangTextNum, 
        RES_INFO,&pBuf->pLangText,
        ParseLangResourseTableProc2);

    if (Cfg_LoadTables(pProf,1,loader) != ERR_CFG_OK)
    {
        return ERR_CFG_FAIL;
    }


    return ERR_CFG_OK;	
}


/*==========================================================================*
 * FUNCTION : rmRcsEndNum
 * PURPOSE  : used to remove the number at the end of resource file name
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *szLangResourceFile : 
 * RETURN   : char *
 * COMMENTS : 
 * CREATOR  : Stone Song                   DATE: 2016-07-15 
 *==========================================================================*/
char *rmRcsEndNum(IN char *szLangResourceFile)
{
	int strLen=0;
	char rcsEndNum[3];
	const int rcsEndLen=8;
	strLen=strlen(szLangResourceFile);

	snprintf(rcsEndNum,sizeof(rcsEndNum),szLangResourceFile+strLen-9);
	if( rcsEndNum[1]>=48 && rcsEndNum[1]<=57)
	{
		if(rcsEndNum[0]>=48 && rcsEndNum[0]<=57)		//Two Num
		{
			snprintf(szLangResourceFile+strLen-9,rcsEndLen,szLangResourceFile+strLen-7);
		}
		else											//Only One Num
		{
			snprintf(szLangResourceFile+strLen-8,rcsEndLen,szLangResourceFile+strLen-7);
		}
	}
	return szLangResourceFile;
}

/*==========================================================================*
 * FUNCTION : LoadLangResourseConfig
 * PURPOSE  : used to load langugae resource file
 * CALLS    : Cfg_LoadConfigFile
 * CALLED BY: 
 * ARGUMENTS: IN char  *szConfigFile : 
 *            OUT LANG_FILE *        :
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-13 17:20
 *==========================================================================*/
int LoadLangResourseConfig(IN char *szLangResourceFile, LANG_FILE *pLangFile)
{
	int ret;

	ASSERT(szLangResourceFile);
	ASSERT(pLangFile);

	ret = Cfg_LoadCfgFirstTime(szLangResourceFile, LoadLangResourseConfigProc, 
		pLangFile);

	if (ret == ERR_CFG_FILE_OPEN)
	{
//added to make several standard configs share one resource file by Stone Song in 20160715
		szLangResourceFile=rmRcsEndNum(szLangResourceFile);
		ret = Cfg_LoadConfigFile(szLangResourceFile, LoadLangResourseConfigProc, 
			pLangFile);
		if(ret != ERR_CFG_OK)
		{
			return ERR_CFG_FAIL;
		}
	}

	return ERR_CFG_OK;
}

/*==========================================================================*
* FUNCTION : LoadLangResourseConfig2
* PURPOSE  : used to load langugae resource file
* CALLS    : Cfg_LoadConfigFile
* CALLED BY: 
* ARGUMENTS: IN char  *szConfigFile : 
*            OUT LANG_FILE *        :
* RETURN   : int : error code defined in the err_code.h , 0 for success
* COMMENTS : 
* CREATOR  : wj                  DATE: 2006-04-27
*==========================================================================*/
int LoadLangResourseConfig2(IN char *szLangResourceFile, LANG_FILE *pLangFile)
{
    int ret;

    ASSERT(szLangResourceFile);
    ASSERT(pLangFile);

    ret = Cfg_LoadConfigFile(szLangResourceFile, LoadLangResourseConfigProc2, 
        pLangFile);

    if (ret != ERR_CFG_OK)
    {
        return ERR_CFG_FAIL;
    }

    return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : GetLangText
 * PURPOSE  : get Lang Text structure by ResourceID
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int         iResourceID : key value
 *			  IN int		 iLen        : Lang Text list length
 *            IN LANG_TEXT  *pLangText   : Lang Text list to search
 * RETURN   : LANG_TEXT * : the searched element, NULL for fail
 * COMMENTS : we think the list has ascending order ..
 * CREATOR  : LinTao                   DATE: 2004-09-14 09:37
 *==========================================================================*/
LANG_TEXT *GetLangText(IN int iResourceID, IN int iLen, IN LANG_TEXT *pLangTextList)
{
	int i, head = 0, tail = iLen-1;

	/* Added by Thomas begin, 2006-3-9 
       for compatibility with the old version of resource files */	
#ifndef _DEBUG
	static char szNoConfiguredName[3] = "NA";
	static LANG_TEXT langText;
	LANG_TEXT   *langTestBox;
	for (i = 0; i < MAX_LANG_TYPE; i++)
	{
		langText.pAbbrName[i] = szNoConfiguredName;
		langText.pFullName[i] = szNoConfiguredName;
	}
#endif //_DEBUG
	/* Modified by Thomas end, 2006-3-9 */
	
	
	ASSERT(pLangTextList);
	langTestBox = pLangTextList;

	if (iLen <= 0)
	{
		return NULL;
	}

	do
	{
		i = (head + tail)/2;

		if (pLangTextList[i].iResourceID > iResourceID)
		{
			tail = i - 1;
		}

		else
		{
			head = i + 1;
		}
	}
	while(pLangTextList[i].iResourceID != iResourceID && head <= tail);

	if (pLangTextList[i].iResourceID == iResourceID)
	{
		return &pLangTextList[i];
	}

	else
	{
		/* Added by Thomas begin, 2006-3-9 
		   for compatibility with the old version of resource files */
#ifdef _DEBUG
		return NULL;
#else		
		AppLogOut(CFG_LOAD_EQUIP, APP_LOG_WARNING, 
			"Certain Resource ID (%d) not defined, please check"
			" %s\n", iResourceID,langTestBox->pAbbrName[0]);
		//return NULL;
		return &langText;
#endif //_DEBUG
		/* Modified by Thomas end, 2006-3-9 */
	}
}


/*==========================================================================*
 * FUNCTION : ReadElement
 * PURPOSE  : get element from expression string.
 * CALLS    : 
 * CALLED BY: ExpressionParser
 * ARGUMENTS: IN BOOL bLastOpe           : Last element is Operator, used for 
 *                                         support negative const
 *            IN OUT char  **szBuf       : the expression string to parse
 *			  OUT EXP_ELEMENT **pElement : the element got, if NULL,means 
 *								something is wrong or end of expression
 * RETURN   : int : err code defined in err_code.h file(ERR_CFG_OK, 
 *					ERR_CFG_NO_MEMORY or ERR_CFG_BADCONFIG)
 * COMMENTS : if returned dummy element, the next should be '!'
 * CREATOR  : LinTao                   DATE: 2004-09-15 12:20
 *==========================================================================*/
static int ReadElement(IN BOOL bLastOpe, IN OUT char **szBuf, 
					   OUT EXP_ELEMENT **pElement)
{
	EXP_OPERATOR *pExpOperator;
	EXP_VARIABLE *pExpVariable;
	EXP_CONST *pConstValue;

	char *pVariableString, *pField;  /* for variable element parse */
	char *pConstString;				 /* for const element parse */
	/* for const element check, '.' can be used only once */
	BOOL bDotUsed = FALSE;                 

	ASSERT(szBuf);
	ASSERT(*szBuf);

	*pElement = NULL;
	*szBuf = Cfg_RemoveWhiteSpace(*szBuf);

	if (**szBuf == '\0')	
	{
		return ERR_CFG_OK;
	}

	switch (**szBuf)
	{
	case '+':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_ADD;
		pExpOperator->byPriority = 3;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '*':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_MUL;
		pExpOperator->byPriority = 2;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '/':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_DIV;
		pExpOperator->byPriority = 2;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '&':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_AND;
		pExpOperator->byPriority = 6;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '|':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_OR;
		pExpOperator->byPriority = 7;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '!':
		(*szBuf)++;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_DUMMY;
		(*pElement)->pElement = NULL;
		break;

	case '>':
		(*szBuf)++;
		*szBuf = Cfg_RemoveWhiteSpace(*szBuf);
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byPriority = 4;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		if (**szBuf == '=')
		{
			(*szBuf)++;
			pExpOperator->byOperatorType = EXP_GREATER_EQUAL;				
			break;
		}					
		pExpOperator->byOperatorType = EXP_GREATER;
		break;

	case '<':
		(*szBuf)++;
		*szBuf = Cfg_RemoveWhiteSpace(*szBuf);
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byPriority = 4;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		if (**szBuf == '=')
		{
			(*szBuf)++;
			pExpOperator->byOperatorType = EXP_LESS_EQUAL;				
			break;
		}
		pExpOperator->byOperatorType = EXP_LESS;
		break;

	case '=':
		(*szBuf)++;
		*szBuf = Cfg_RemoveWhiteSpace(*szBuf);
		if (**szBuf == '=')
		{
			(*szBuf)++;
			pExpOperator = NEW(EXP_OPERATOR, 1);
			if (pExpOperator == NULL)
			{
				return ERR_CFG_NO_MEMORY;
			}
			pExpOperator->byOperatorType = EXP_EQUAL;
			pExpOperator->byPriority = 5;
			*pElement = NEW(EXP_ELEMENT, 1);
			if (*pElement == NULL)
			{
				SAFELY_DELETE(pExpOperator);
				return ERR_CFG_NO_MEMORY;
			}
			(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
			(*pElement)->pElement = pExpOperator;
			break;
		}
		return ERR_CFG_BADCONFIG;

	case '(':				 
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_LF_BRACKET;
		pExpOperator->byPriority = (BYTE)-1;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case ')':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_RT_BRACKET;
		pExpOperator->byPriority = (BYTE)-1;
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '[':
		pVariableString = *szBuf;
		while (*(*szBuf) != '\0' && *(*szBuf) != ']')
		{
			(*szBuf)++;
		}
		if (*(*szBuf) != ']')			/* expression is invalid */
		{
			return ERR_CFG_BADCONFIG;
		}

		**szBuf = '\0';
		(*szBuf)++;

		/* validation check, element should be 3 */
		if (3 != Cfg_GetSplitStringCount(pVariableString, SUB_SPLITTER))
		{
			TRACEX("count is: %d\n", Cfg_GetSplitStringCount(pVariableString, SUB_SPLITTER));
			return ERR_CFG_BADCONFIG;	
		}

		pExpVariable = NEW(EXP_VARIABLE, 1);
		if (pExpVariable == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}

		/* get iRelevantEquipIndex value */
		pVariableString = Cfg_RemoveBoundChr(pVariableString, IsBoundChr_S);
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iRelevantEquipIndex);

		/* get iSigType value */
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iSigType);

		/* get iSigID value */
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iSigID);
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpVariable);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_VARIABLE;
		(*pElement)->pElement = pExpVariable;

		break;

	case ']':
		return ERR_CFG_BADCONFIG;			/* bad expression */

	case '{':
		pVariableString = *szBuf;
		while (*(*szBuf) != '\0' && *(*szBuf) != '}')
		{
			(*szBuf)++;
		}
		if (*(*szBuf) != '}')			/* expression is invalid */
		{
			return ERR_CFG_BADCONFIG;
		}
		**szBuf = '\0';
		(*szBuf)++;
		pExpVariable = NEW(EXP_VARIABLE, 1);
		if (pExpVariable == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}

		/* get iRelevantEquipIndex value */
		pVariableString = Cfg_RemoveBoundChr(pVariableString, IsBoundChr_BS);
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iRelevantEquipIndex);

		/* get iSigType value */
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iSigType);

		/* get iSigID value */
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iSigID);
		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpVariable);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_VARIABLE_RAW;
		(*pElement)->pElement = pExpVariable;

		break;

	case '}':
		return ERR_CFG_BADCONFIG;			/* bad expression */

	case '-':
		if (!bLastOpe)  //if the last element is Operator, take it as const
		{
			(*szBuf)++;
			pExpOperator = NEW(EXP_OPERATOR, 1);
			if (pExpOperator == NULL)
			{
				return ERR_CFG_NO_MEMORY;
			}
			pExpOperator->byOperatorType = EXP_SUB;
			pExpOperator->byPriority = 3;
			*pElement = NEW(EXP_ELEMENT, 1);
			if (*pElement == NULL)
			{
				SAFELY_DELETE(pExpOperator);
				return ERR_CFG_NO_MEMORY;
			}
			(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
			(*pElement)->pElement = pExpOperator;
			break;
		}     // if bLastOpe == TRUE, go to const process

	default:					/* should be const */
		pConstString = *szBuf;

		if (!((**szBuf >= '0' && **szBuf <= '9') || **szBuf == '-'))
		{
			return ERR_CFG_BADCONFIG;
		}
		(*szBuf)++;
		while ((**szBuf >= '0' && **szBuf <= '9') || 
			(!bDotUsed && **szBuf == '.')) 
		{
			if (**szBuf == '.')
			{
				bDotUsed = TRUE;
			}

			(*szBuf)++;
		}
		pConstValue = NEW(EXP_CONST, 1);
		if (pConstValue == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		sscanf(pConstString, "%f", pConstValue);

		*pElement = NEW(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pConstValue);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_CONST;
		(*pElement)->pElement = pConstValue;
	}

	return ERR_CFG_OK;
}


//added for memory opt
static int ReadElement_Pool(IN BOOL bLastOpe, IN OUT char **szBuf, 
					   OUT EXP_ELEMENT **pElement)
{
	EXP_OPERATOR *pExpOperator;
	EXP_VARIABLE *pExpVariable;
	EXP_CONST *pConstValue;

	char *pVariableString, *pField;  /* for variable element parse */
	char *pConstString;				 /* for const element parse */
	/* for const element check, '.' can be used only once */
	BOOL bDotUsed = FALSE;                 

	ASSERT(szBuf);
	ASSERT(*szBuf);

	*pElement = NULL;
	*szBuf = Cfg_RemoveWhiteSpace(*szBuf);

	if (**szBuf == '\0')	
	{
		return ERR_CFG_OK;
	}

	switch (**szBuf)
	{
	case '+':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_ADD;
		pExpOperator->byPriority = 3;
		//*pElement = NEW(EXP_ELEMENT, 1);
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '*':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_MUL;
		pExpOperator->byPriority = 2;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '/':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_DIV;
		pExpOperator->byPriority = 2;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '&':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_AND;
		pExpOperator->byPriority = 6;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '|':
		(*szBuf)++;
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_OR;
		pExpOperator->byPriority = 7;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '!':
		(*szBuf)++;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_DUMMY;
		(*pElement)->pElement = NULL;
		break;

	case '>':
		(*szBuf)++;
		*szBuf = Cfg_RemoveWhiteSpace(*szBuf);
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byPriority = 4;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		if (**szBuf == '=')
		{
			(*szBuf)++;
			pExpOperator->byOperatorType = EXP_GREATER_EQUAL;				
			break;
		}					
		pExpOperator->byOperatorType = EXP_GREATER;
		break;

	case '<':
		(*szBuf)++;
		*szBuf = Cfg_RemoveWhiteSpace(*szBuf);
		pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byPriority = 4;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		if (**szBuf == '=')
		{
			(*szBuf)++;
			pExpOperator->byOperatorType = EXP_LESS_EQUAL;				
			break;
		}
		pExpOperator->byOperatorType = EXP_LESS;
		break;

	case '=':
		(*szBuf)++;
		*szBuf = Cfg_RemoveWhiteSpace(*szBuf);
		if (**szBuf == '=')
		{
			(*szBuf)++;
			pExpOperator = NEW(EXP_OPERATOR, 1);
			if (pExpOperator == NULL)
			{
				return ERR_CFG_NO_MEMORY;
			}
			pExpOperator->byOperatorType = EXP_EQUAL;
			pExpOperator->byPriority = 5;
			*pElement = NEW_POOL(EXP_ELEMENT, 1);
			if (*pElement == NULL)
			{
				SAFELY_DELETE(pExpOperator);
				return ERR_CFG_NO_MEMORY;
			}
			(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
			(*pElement)->pElement = pExpOperator;
			break;
		}
		return ERR_CFG_BADCONFIG;

	case '(':				 
		(*szBuf)++;
		//modified for memory opt, pool allocate for temperary existed "(" and ")" operator,
		pExpOperator = NEW_POOL(EXP_OPERATOR, 1);
		//pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_LF_BRACKET;
		pExpOperator->byPriority = (BYTE)-1;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case ')':
		(*szBuf)++;
		//modified for memory opt, pool allocate for temperary existed "(" and ")" operator,
		pExpOperator = NEW_POOL(EXP_OPERATOR, 1);
		//pExpOperator = NEW(EXP_OPERATOR, 1);
		if (pExpOperator == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		pExpOperator->byOperatorType = EXP_RT_BRACKET;
		pExpOperator->byPriority = (BYTE)-1;
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpOperator);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
		(*pElement)->pElement = pExpOperator;
		break;

	case '[':
		pVariableString = *szBuf;
		while (*(*szBuf) != '\0' && *(*szBuf) != ']')
		{
			(*szBuf)++;
		}
		if (*(*szBuf) != ']')			/* expression is invalid */
		{
			return ERR_CFG_BADCONFIG;
		}

		**szBuf = '\0';
		(*szBuf)++;

		/* validation check, element should be 3 */
		if (3 != Cfg_GetSplitStringCount(pVariableString, SUB_SPLITTER))
		{
			TRACEX("count is: %d\n", Cfg_GetSplitStringCount(pVariableString, SUB_SPLITTER));
			return ERR_CFG_BADCONFIG;	
		}

		pExpVariable = NEW(EXP_VARIABLE, 1);
		if (pExpVariable == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}

		/* get iRelevantEquipIndex value */
		pVariableString = Cfg_RemoveBoundChr(pVariableString, IsBoundChr_S);
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iRelevantEquipIndex);

		/* get iSigType value */
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iSigType);

		/* get iSigID value */
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iSigID);
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpVariable);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_VARIABLE;
		(*pElement)->pElement = pExpVariable;

		break;

	case ']':
		return ERR_CFG_BADCONFIG;			/* bad expression */

	case '{':
		pVariableString = *szBuf;
		while (*(*szBuf) != '\0' && *(*szBuf) != '}')
		{
			(*szBuf)++;
		}
		if (*(*szBuf) != '}')			/* expression is invalid */
		{
			return ERR_CFG_BADCONFIG;
		}
		**szBuf = '\0';
		(*szBuf)++;
		pExpVariable = NEW(EXP_VARIABLE, 1);
		if (pExpVariable == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}

		/* get iRelevantEquipIndex value */
		pVariableString = Cfg_RemoveBoundChr(pVariableString, IsBoundChr_BS);
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iRelevantEquipIndex);

		/* get iSigType value */
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iSigType);

		/* get iSigID value */
		pVariableString = Cfg_SplitStringEx(pVariableString, &pField, ',');
		if (pField >= *szBuf || *pField < '0' || *pField > '9')			
		{
			return ERR_CFG_BADCONFIG;					/* expression is invalid */
		}
		sscanf(pField, "%d", &pExpVariable->iSigID);
		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pExpVariable);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_VARIABLE_RAW;
		(*pElement)->pElement = pExpVariable;

		break;

	case '}':
		return ERR_CFG_BADCONFIG;			/* bad expression */

	case '-':
		if (!bLastOpe)  //if the last element is Operator, take it as const
		{
			(*szBuf)++;
			pExpOperator = NEW(EXP_OPERATOR, 1);
			if (pExpOperator == NULL)
			{
				return ERR_CFG_NO_MEMORY;
			}
			pExpOperator->byOperatorType = EXP_SUB;
			pExpOperator->byPriority = 3;
			*pElement = NEW_POOL(EXP_ELEMENT, 1);
			if (*pElement == NULL)
			{
				SAFELY_DELETE(pExpOperator);
				return ERR_CFG_NO_MEMORY;
			}
			(*pElement)->byElementType = EXP_ETYPE_OPERATOR;
			(*pElement)->pElement = pExpOperator;
			break;
		}     // if bLastOpe == TRUE, go to const process

	default:					/* should be const */
		pConstString = *szBuf;

		if (!((**szBuf >= '0' && **szBuf <= '9') || **szBuf == '-'))
		{
			return ERR_CFG_BADCONFIG;
		}
		(*szBuf)++;
		while ((**szBuf >= '0' && **szBuf <= '9') || 
			(!bDotUsed && **szBuf == '.')) 
		{
			if (**szBuf == '.')
			{
				bDotUsed = TRUE;
			}

			(*szBuf)++;
		}
		pConstValue = NEW(EXP_CONST, 1);
		if (pConstValue == NULL)
		{
			return ERR_CFG_NO_MEMORY;
		}
		sscanf(pConstString, "%f", pConstValue);

		*pElement = NEW_POOL(EXP_ELEMENT, 1);
		if (*pElement == NULL)
		{
			SAFELY_DELETE(pConstValue);
			return ERR_CFG_NO_MEMORY;
		}
		(*pElement)->byElementType = EXP_ETYPE_CONST;
		(*pElement)->pElement = pConstValue;
	}

	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : ExpressionParser
 * PURPOSE  : form Reverse Polish Notation
 * CALLS    : ReadElement
 * CALLED BY: 
 * ARGUMENTS: IN char          *szBuf       : expression string
 *            OUT int          *piExpLen    : element count
 *            OUT EXP_ELEMENT  **pExpression : point array for EXP_ELEMENT*
 * RETURN   : int : err code defined in err_code.h file(ERR_CFG_OK, 
 *					ERR_CFG_BADCONFIG, ERR_CFG_NO_MEMORY,
 *					ERR_CFG_FAIL(means EXP_MAX_ELEMENTS 
 *					define is not big enough))
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-15 17:09
 *==========================================================================*/
int ExpressionParser(IN char *szBuf, OUT int *piExpLen, OUT EXP_ELEMENT **pExpression) 
{
	int iLen,ret;				/* iLen: element count of the expression */
	EXP_ELEMENT *pEle = NULL;
	EXP_ELEMENT *pEleTmp = NULL;
	EXP_OPERATOR *pOperator = NULL;
	EXP_OPERATOR *pOperatorTmp = NULL; 
	EXP_ELEMENT *pOpEleStack[20];	/* stack of operator element */
	int iStackTop = 0;				/* top of operator element stack */

	BOOL bLastOpe = FALSE;  /* if the last element type is operator, set it to TRUE,
	                           for supporting negative const date */

	char *p = szBuf;
	iLen = 0;

	if (szBuf[0] == '\0')
	{
		return ERR_CFG_BADCONFIG;
	}

	//updated for memory opt
	//ret = ReadElement(bLastOpe, &p, &pEle);
	ret = ReadElement_Pool(bLastOpe, &p, &pEle);
	while (pEle != NULL)
	{
		bLastOpe = FALSE;  //reset to FALSE

		switch(pEle->byElementType)
		{
		case EXP_ETYPE_VARIABLE:		/* process by next section */
		case EXP_ETYPE_VARIABLE_RAW:
		case EXP_ETYPE_CONST:
			iLen++;
			if (iLen > EXP_MAX_ELEMENTS)
			{
				/* EXP_MAX_ELEMENTS define is not big enough! */
				return ERR_CFG_FAIL;
			}
			pExpression[iLen-1] = pEle;
			break;

		case EXP_ETYPE_DUMMY:			/* for dummy element, we should create '!' element*/
			iLen++;
			if (iLen > EXP_MAX_ELEMENTS)
			{
				/* EXP_MAX_ELEMENTS define is not big enough! */
				return ERR_CFG_FAIL;
			}
			pExpression[iLen-1] = pEle;
			//updated for memory opt
			//pEle = NEW(EXP_ELEMENT, 1);
			pEle = NEW_POOL(EXP_ELEMENT, 1); 
			if (pEle == NULL)
			{
				return ERR_CFG_NO_MEMORY;
			}
			pEle->byElementType = EXP_ETYPE_OPERATOR;
			pEle->pElement = NULL;
			pOperator = NEW(EXP_OPERATOR, 1);  
			if (pOperator == NULL)
			{
				return ERR_CFG_NO_MEMORY;
			}
			pOperator->byOperatorType = EXP_NOT;
			pOperator->byPriority = 1;
			pEle->pElement = pOperator; /* '!' is a operator, so go to next section */

		case EXP_ETYPE_OPERATOR:
			pOperator = (EXP_OPERATOR *)pEle->pElement;
			if (pOperator->byOperatorType == EXP_LF_BRACKET)
			{
				pOpEleStack[iStackTop] = pEle;
				iStackTop++;
				break;
			}
			else if (pOperator->byOperatorType == EXP_RT_BRACKET)
			{
				//updated for memory opt, pool allocate for temperary existed "(" and ")" operator,
		        DELETE_POOL(pOperator);
				DELETE_POOL(pEle);
				//SAFELY_DELETE(pOperator);
				//SAFELY_DELETE(pEle);				/* free memory for ')' element */
				if (iStackTop == 0)
				{
					return ERR_CFG_BADCONFIG;
				}
				pEleTmp = pOpEleStack[--iStackTop];
				pOperatorTmp = (EXP_OPERATOR *)(pEleTmp->pElement);
				while (pOperatorTmp->byOperatorType != EXP_LF_BRACKET && 
					iStackTop >0 )
				{
					iLen++;
					if (iLen > EXP_MAX_ELEMENTS)
					{
						/* EXP_MAX_ELEMENTS define is not big enough! */
						return ERR_CFG_FAIL;
					}
					pExpression[iLen-1] = pEleTmp;
					pEleTmp = pOpEleStack[--iStackTop];
					pOperatorTmp = (EXP_OPERATOR *)(pEleTmp->pElement);
				}
				if (pOperatorTmp->byOperatorType != EXP_LF_BRACKET )
				{
					return ERR_CFG_BADCONFIG;		/* should be '(' */
				}
				//modified for memory opt
				DELETE_POOL(pEleTmp->pElement);
				DELETE_POOL(pEleTmp);
				//SAFELY_DELETE(pEleTmp->pElement);
				//SAFELY_DELETE(pEleTmp);			/* free memory for '(' element */
				break;
			}
			else
			{
				bLastOpe = TRUE; 
				if (iStackTop != 0 )
				{
					pEleTmp = pOpEleStack[--iStackTop];
					pOperatorTmp = (EXP_OPERATOR *)pEleTmp->pElement;
					while ((pOperatorTmp->byPriority <= pOperator->byPriority) && 
						pOperatorTmp->byOperatorType != EXP_LF_BRACKET && 
						iStackTop > 0)
					{
						iLen++;
						if (iLen > EXP_MAX_ELEMENTS)
						{
							/* EXP_MAX_ELEMENTS define is not big enough! */
							return ERR_CFG_FAIL;
						}
						pExpression[iLen-1] = pEleTmp;
						pEleTmp = pOpEleStack[--iStackTop];
						pOperatorTmp = (EXP_OPERATOR *)pEleTmp->pElement;
					}
					if (pOperatorTmp->byPriority <= pOperator->byPriority && 
						pOperatorTmp->byOperatorType != EXP_LF_BRACKET)
					{						/* stop because iStackTop == 0 */
						iLen++;
						if (iLen > EXP_MAX_ELEMENTS)
						{
							/* EXP_MAX_ELEMENTS define is not big enough! */
							return ERR_CFG_FAIL;
						}
						pExpression[iLen-1] = pEleTmp;
						iStackTop--; 
					}
					pOpEleStack[++iStackTop] = pEle;
					iStackTop++;
				}
				else
				{
					pOpEleStack[iStackTop++] = pEle;
				}
			}
		}

		//updated for memory opt
		ret = ReadElement_Pool(bLastOpe, &p, &pEle);
		//ret = ReadElement(bLastOpe, &p, &pEle);
	}
	if (ret != ERR_CFG_OK)
	{
		return ret;
	}

	if (iStackTop != 0)
	{
		while ( --iStackTop >= 0 )
		{
			iLen++;
			if (iLen > EXP_MAX_ELEMENTS)
			{
				/*TRACE("EXP_MAX_ELEMENTS define is not big enough!");*/
				return ERR_CFG_FAIL;
			}
			pExpression[iLen-1] = pOpEleStack[iStackTop];
		}
	}

	*piExpLen = iLen;

	return ERR_CFG_OK;
}


static int CompareCustomChannel(CUSTOM_CHANNEL *pCh1, CUSTOM_CHANNEL *pCh2)
{
	return (pCh1->iEqipID - pCh2->iEqipID);
}
typedef int (*SORT_COMPARE_PROC)(const void *pEle1, const void *pEle2);


/*==========================================================================*
 * FUNCTION : DispatchCustomChannelInfo
 * PURPOSE  : dispatch global custom channel info to each equip
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-16 12:11
 *==========================================================================*/
static BOOL DispatchCustomChannelInfo(void)
{
	int index, iMaxEquipID = 0;			

	/* piEquipIDMap: use EquipID as map-key, index as map-value 
	piChannelNum: channelNum for each Equip */ 
	int *piEquipIDMap, *piChannelNum;     

	EQUIP_INFO *pEquip;
	CUSTOM_CHANNEL *pCustomChannel;


	ASSERT(g_SiteInfo.pEquipInfo);

	if (g_SiteInfo.iCustomChannelNum == 0)
	{
		return TRUE;
	}

	ASSERT(g_SiteInfo.pCustomChannel);

	RunThread_Heartbeat(RunThread_GetId(NULL));

	/* sort the global custom channels*/
	qsort(g_SiteInfo.pCustomChannel, 
		(size_t)g_SiteInfo.iCustomChannelNum,
		sizeof(CUSTOM_CHANNEL),
		(SORT_COMPARE_PROC)CompareCustomChannel);

	/* get max Equip ID */
	pEquip = g_SiteInfo.pEquipInfo;
	for (index = 0; index < g_SiteInfo.iEquipNum; index++, pEquip++)
	{
		if (pEquip->iEquipID > iMaxEquipID)
		{
			iMaxEquipID = pEquip->iEquipID;
		}
	}
	piEquipIDMap = NEW(int, iMaxEquipID + 1);
	if (piEquipIDMap == NULL) 
	{
		return FALSE;
	}
	piChannelNum = NEW(int ,iMaxEquipID + 1);

	if(piChannelNum == NULL)
	{
		DELETE(piEquipIDMap);
		return FALSE;
	}

	memset(piChannelNum, 0, sizeof(int)*(iMaxEquipID + 1));

	RunThread_Heartbeat(RunThread_GetId(NULL));

	/* init the map */
	pEquip = g_SiteInfo.pEquipInfo;
	for (index = 0; index < g_SiteInfo.iEquipNum; index++, pEquip++)
	{
		piEquipIDMap[pEquip->iEquipID] = index;
	}

	/* for each Equip, count custom channel number and get reference */
	pCustomChannel = g_SiteInfo.pCustomChannel;
	for (index = 0; index < g_SiteInfo.iCustomChannelNum;
		index++, pCustomChannel++)
	{
		(piChannelNum[pCustomChannel->iEqipID])++;

		pEquip = g_SiteInfo.pEquipInfo + piEquipIDMap[pCustomChannel->iEqipID];

		/* get reference only once for a equip */
		if (piChannelNum[pCustomChannel->iEqipID] == 1)
		{
			pEquip->pCustomChannelInfo = pCustomChannel;
		}
	}

	/* set custom channel number for each equip */
	for (index = 1; index <= iMaxEquipID; index++)
	{
		if (piChannelNum[index] == 0)
		{
			continue;
		}

		pEquip = g_SiteInfo.pEquipInfo + piEquipIDMap[index];
		pEquip->nCustomChannelInfo = piChannelNum[index];
	}

	/* free the memory */
	SAFELY_DELETE(piEquipIDMap);
	SAFELY_DELETE(piChannelNum);

	RunThread_Heartbeat(RunThread_GetId(NULL));

	return TRUE;
}

///* Used by get Config File name functions */
//enum CFG_FILE_TYPE
//{
//	CFG_STD_BASIC = 1,
//	CFG_STD_EQUIP,
//	CFG_SOLUTION,
//	CFG_RUN
//};
//
///* assitant functions for get Config File name */
//static char *GetConfigFileName(IN int iFileType, IN const char *szRawStdEquipFileName)
//{
//	switch (iFileType)
//	{
//	case CFG_STD_BASIC:
//		return CONFIG_DIR BASIC_STD_CFGFILE;
//
//	case CFG_SOLUTION:
//		return CONFIG_DIR SOLUTION_CFGFILE;
//
//	case CFG_RUN:
//		return CONFIG_DIR RUN_CFGFILE;
//
//	case CFG_STD_EQUIP:
//		return NEW_straddz(CONFIG_DIR, szRawStdEquipFileName); 
//
//	default:
//		return NULL;
//	}
//}
//
//static char *GetBakConfigFileName(IN int iFileType, IN char *szRawStdEquipFileName)
//{
//	switch (iFileType)
//	{
//	case CFG_STD_BASIC:
//		return CONFIG_BAK_DIR BASIC_STD_CFGFILE;
//
//	case CFG_SOLUTION:
//		return CONFIG_BAK_DIR SOLUTION_CFGFILE;
//
//	case CFG_RUN:
//		return CONFIG_BAK_DIR RUN_CFGFILE;
//
//	case CFG_STD_EQUIP:
//		return NEW_straddz(CONFIG_BAK_DIR, szRawStdEquipFileName); 
//
//	default:
//		return NULL;
//	}
//}
//
//static char *GetDefaultConfigFileName(IN int iFileType, IN char *szRawStdEquipFileName)
//{
//	switch (iFileType)
//	{
//	case CFG_STD_BASIC:
//		return CONFIG_DEFAULT_DIR BASIC_STD_CFGFILE;
//
//	case CFG_SOLUTION:
//		return CONFIG_DEFAULT_DIR SOLUTION_CFGFILE;
//
//	case CFG_RUN:
//		return CONFIG_DEFAULT_DIR RUN_CFGFILE;
//
//	case CFG_STD_EQUIP:
//		return NEW_straddz(CONFIG_DEFAULT_DIR, szRawStdEquipFileName); 
//
//	default:
//		return NULL;
//	}
//}


/*==========================================================================*
 * FUNCTION : *CreateLangResourceFileName
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szFileName : format: xxx.cfg
 *            const char  *szLangCode : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 20:03
 *==========================================================================*/
static char *CreateLangResourceFileName(const char *szFileName, const char *szLangCode)
{
	static char szLRFileName[256]; /* the returned lang resource file name */
	const char *p;   
	size_t iLen;

	if (szFileName == NULL || szLangCode == NULL)
	{
		return NULL;
	}

	/* init */
	szLRFileName[0] = '\0';

	/* get the head: config/lang/xx/ */
	strcpy(szLRFileName, "lang/");
	strcat(szLRFileName, szLangCode);
	strcat(szLRFileName, "/");

	/* get the name: stdxx_xx.res */
	p = szFileName;
	iLen = 0;
	while (*p != '\0' && *p != '.')
	{
		p++;
		iLen++;
	}

	if (*p == '\0')				/* lack of '.', invalid format */
	{
		return NULL;
	}

	strncat(szLRFileName, szFileName, iLen);
	strcat(szLRFileName, "_");
	strcat(szLRFileName, szLangCode);
	strcat(szLRFileName, ".res");

	return szLRFileName;
}


/*==========================================================================*
 * FUNCTION : Cfg_ChkSolutionChanges
 * PURPOSE  : always load the last solution from acu/config/run, and cmp the
 *            last and new solution by equipment type and ID.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : TRUE for solutions is Changed.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-01 20:25
 *==========================================================================*/
static BOOL Cfg_ChkSolutionChanges(IN SITE_INFO *pSite)
{
	BOOL			bSoltuionChanged = FALSE;
	BOOL			bLangChanged = FALSE;
	char			szRunningSolution[MAX_FILE_PATH];
	FILE			*fp;
	char			szBuf[256];
	int				n, nIdx, nEquipID, nEquipStdType;
	EQUIP_INFO		*pEquip;
	char			*pCurLangcode;

	int             nEquipFromFile; //Added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4

	char			*ppszFileFlags[] = 
	{
		// flag: each line shall has and only has a '\n'! 
		"# SCU+ running configuration solution.Copyright(c) Vertiv Tech.\n",
		"# This file is generated by SCU+ automatically,do NOT modify it.\n",
		"# CAUTION: \n",
		"#     All of the historical files will be erased if this file is changed.\n",
		"#\n"
	};

	char			*ppszSects[] =  
	{
		"[LANG_INFO]\n",
		"[EQUIP_NUM]\n", 
		"[EQUIP_INFO]\n"
	};

	pCurLangcode = pSite->LangInfo.szLocaleLangCode;
	pSite->bFirstRunning = FALSE;

	// the format of the file SCUP_RUNNING_SOLUTION
	// head msg:
	//[LANG_INFO]
	//local language code used in the solution
	// [EQUIP_NUM]
	// total equipment configured
	// [EQUIP_INFO]
	// 1 \t equip_id \t equiptype
	// 2 \t equip_id \t equiptype
	// .. .. ..
	// N \t equip_id \t equiptype

	MakeFullFileName(szRunningSolution, getenv(ENV_ACU_ROOT_DIR), 
		CONFIG_DIR, SCUP_RUNNING_SOLUTION);

	AppLogOut(CFG_LOADER, APP_LOG_UNUSED, 
		"Checking last sln flag file [%s]...\n", 
		szRunningSolution);

#ifdef _DEBUG_EQUIP_INIT
	TRACEX(">>>>>>>>>>>>>>>>>> The last solution file name is %s.\n",
		szRunningSolution);
#endif //_DEBUG_EQUIP_INIT

	fp = fopen(szRunningSolution, "rt");
	if (fp == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
			"Fails on opening last solution flag file.\n");
		bSoltuionChanged = TRUE;
		pSite->bFirstRunning = TRUE;
	}

	//1. read the head flags
	for (n = 0; (n < ITEM_OF(ppszFileFlags)) && !bSoltuionChanged; n++)
	{
		szBuf[0] = 0;
		fgets(szBuf, sizeof(szBuf), fp);
		
		if (strncmp(szBuf, ppszFileFlags[n], sizeof(szBuf)) != 0)
		{
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
				"The head flag of solution flag file does NOT match.\n");

			bSoltuionChanged = TRUE;
		}
	}

	//2. read the lang info
	if (!bSoltuionChanged)
	{
		szBuf[0] = 0;
		fgets(szBuf, sizeof(szBuf), fp);

		if (strncmp(szBuf, ppszSects[0], sizeof(szBuf)) != 0)
		{
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
				"The section %s is NOT found in solution flag file.\n",
				ppszSects[0]);

			bSoltuionChanged = TRUE;
		}
	}


	if (!bSoltuionChanged)
	{
		szBuf[0] = 0;
		fgets(szBuf, sizeof(szBuf), fp);
		szBuf[strlen(pCurLangcode)] = 0; // remove \n

		if (strcmp(szBuf, pCurLangcode) != 0)
		{
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
				"Local language code is changed to '%s', "
				"the resource files of last code '%s' will be deleted.\n",
				pCurLangcode, szBuf);

			bLangChanged = TRUE;	// set flag, the files will be deleted later
		}
	}
	

	//3. read the equipment num of last solution
	if (!bSoltuionChanged)
	{
		szBuf[0] = 0;
		fgets(szBuf, sizeof(szBuf), fp);

		if (strncmp(szBuf, ppszSects[1], sizeof(szBuf)) != 0)
		{
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
				"The section %s is NOT found in solution flag file.\n",
				ppszSects[1]);

			bSoltuionChanged = TRUE;
		}
	}
	
	if (!bSoltuionChanged)
	{
		szBuf[0] = 0;
		fgets(szBuf, sizeof(szBuf), fp);

		//updated for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
		//if just adding some equipments, NOT need to remove the history storage
		nEquipFromFile = atoi(szBuf);     
		if (nEquipFromFile > pSite->iEquipNum)  
		{
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
				"The last equipment num(%d) is bigger than the current num(%d).\n",
				nEquipFromFile, pSite->iEquipNum);

			bSoltuionChanged = TRUE;
		}

		/*n = atoi(szBuf);
		if (n != pSite->iEquipNum)
		{
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
				"The last equipment num(%d) is NOT equal to the current num(%d).\n",
				n, pSite->iEquipNum);

			bSoltuionChanged = TRUE;
		}*/
		//end of G3_OPT [loader]
	}

	// 4. read the IDX, EquipID, EquipType of each equipment. 
	if (!bSoltuionChanged)
	{
		szBuf[0] = 0;
		fgets(szBuf, sizeof(szBuf), fp);

		if (strncmp(szBuf, ppszSects[2], sizeof(szBuf)) != 0)
		{
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
				"The section %s is NOT found in solution flag file.\n",
				ppszSects[2]);

			bSoltuionChanged = TRUE;
		}
	}
	
	//updated for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	//if just adding some equipments, NOT need to remove the history storage
	for (n = 1; (n <= nEquipFromFile) && !bSoltuionChanged; n++)
	/*pEquip = pSite->pEquipInfo;
	for (n = 1; (n <= pSite->iEquipNum) && !bSoltuionChanged; n++, pEquip++)*/
	//end of G3_OPT [loader]
	{
		szBuf[0] = 0;
		fgets(szBuf, sizeof(szBuf), fp);

		// idx equipID EquipType
		nIdx = nEquipID = nEquipStdType = 0;
		sscanf(szBuf, "%d %d %d", 
			&nIdx, &nEquipID, &nEquipStdType);

		//updated for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
		pEquip = GetEquip(nEquipID); 
		if (pEquip == NULL || pEquip->iEquipTypeID != nEquipStdType)
		{			
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 		
				"The No.%d equipment last info(IDX=%d, ID=%d, TYPE=%d) is CHANGED.\n",			
				n, nIdx, nEquipID, nEquipStdType);	
			bSoltuionChanged = TRUE;	
		}

		/*if ((nIdx != n) || (pEquip->iEquipID != nEquipID) ||
			(pEquip->iEquipTypeID != nEquipStdType))
		{
			AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
				"The No.%d equipment last info(IDX=%d, ID=%d, TYPE=%d) is NOT "
				"(IDX=%d, ID=%d, TYPE=%d).\n",
				n, 
				nIdx,	nEquipID,			nEquipStdType, 
				n,		pEquip->iEquipID,	pEquip->iEquipTypeID);

			bSoltuionChanged = TRUE;
		}*/
		//end of G3_OPT [loader]
	}


	// the run flag file does not exist, treat the system is loaded to
	// default solution, need remove all historical data.
	// when the system is first running, or the default solution is justed
	// loaded, the file will not exist.
	pSite->nConfigStatus = (!bSoltuionChanged) ? CONFIG_STATUS_NOT_CHANGED
		: (fp != NULL) ? CONFIG_STATUS_NORMALLY_CHANGED
		: CONFIG_STATUS_DEFAULT_LOADED;

	// 4. read done
	if (fp != NULL)
	{
		fclose(fp);
	}


	AppLogOut(CFG_LOADER, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"The last solution flag file is checked done: the file is %s.\n",
		(bSoltuionChanged ? "Changed" : "NOT changed."));


	/* modified begin -- always delete rundance language resource file, 
	   Thomas, 2005-7-14 */
	{
		char	szCmdLine[MAX_FILE_PATH];

		/* delete redudant language resource files */
		snprintf(szCmdLine, sizeof(szCmdLine), "%s/bin/rmlang %s %s",
			getenv(ENV_ACU_ROOT_DIR), pCurLangcode, getenv(ENV_ACU_ROOT_DIR));
		//system(szCmdLine);
		_SYSTEM(szCmdLine); 
	}
	/* modified end */


	if (!bSoltuionChanged && !bLangChanged)
	{
		return bSoltuionChanged;
	}

	/* modified begin -- always delete rundance language resource file, 
	   Thomas, 2005-7-14 */

	/* ensure to delete run file */
	bSoltuionChanged = TRUE; 
	
	// the run file need be removed when solution changed or lang changed.
	//else
	//{
	//	char	szCmdLine[MAX_FILE_PATH];


	//	/* delete redudant language resource files */
	//	snprintf(szCmdLine, sizeof(szCmdLine), "%s/bin/rmlang %s %s",
	//		getenv(ENV_ACU_ROOT_DIR), pCurLangcode, getenv(ENV_ACU_ROOT_DIR));
	//	system(szCmdLine); 

	//	bSoltuionChanged = TRUE;  


	//	/* delete run file */
	//	Cfg_CleanMainRunFile(TRUE);
	//}
	/* modified end */
			
	//
	//
	//5. if solution is changed, recreate the file.
	//
	AppLogOut(CFG_LOADER, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"The solution flag file is being updated...\n");

	fp = fopen(szRunningSolution, "wt");
	if (fp == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_WARNING, 
			"Fails on creating last solution flag file.\n");
		return bSoltuionChanged;
	}

	//1. write the head flags
	for (n = 0; (n < ITEM_OF(ppszFileFlags)); n++)
	{
		fprintf(fp, "%s", ppszFileFlags[n]);
	}

	//2. write the lang info 
	fprintf(fp, "%s", ppszSects[0]);
	fprintf(fp, "%s\n", pCurLangcode);

	//3. write the equipment num of last solution
	fprintf(fp, "%s", ppszSects[1]);
	fprintf(fp, "%d\n", pSite->iEquipNum);

	// 4. write the IDX, EquipID, EquipType of each equipment. 
	fprintf(fp, "%s", ppszSects[2]);

	pEquip = pSite->pEquipInfo;
	for (n = 1; (n <= pSite->iEquipNum); n++, pEquip++)
	{
		fprintf(fp,"%d \t%d \t%d\n",
			n,	pEquip->iEquipID, pEquip->iEquipTypeID);
	}
	
	// 4. read done
	fclose(fp);

	AppLogOut(CFG_LOADER, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"The solution flag file is updated done.\n");

	return bSoltuionChanged;
}

/*==========================================================================*
 * FUNCTION : Cfg_InitialConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 18:42
 *==========================================================================*/
int Cfg_InitialConfig()
{
	int  i, j, ret;
	int  iEquipTypeID, iUsedTypeNum = 0;
	char szFullPath[MAX_FILE_PATH];  //full config file name
	char *szLangResourceFile;

	EQUIP_INFO				*pEquip;
	STDEQUIP_TYPEMAP_INFO	*pTypeMap;
	STDEQUIP_TYPE_INFO		*pStdEquip;
	LANG_INFO			    *pLangInfo;

    LANG_INFO			    *pLangInfoTemp;//add by wj for three languages 2006.4.27

	/* for checking Custom Channel Info of Solution Config */
	CUSTOM_CHANNEL    *pCustomChannel;

	/* 1.initial structure */
	memset(&g_SiteInfo, 0, sizeof(SITE_INFO));

	/* 2.load basic std config file */
	Cfg_GetFullConfigPath(BASIC_STD_CFGFILE, szFullPath, MAX_FILE_PATH);
	ret = Cfg_LoadBasicStdConfig(szFullPath);
	if (ret != ERR_CFG_OK)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: Load basic std config "
			"failed!\n", __FILE__);
		return ret;
	}

	/* 3.load solution config file */
	//Modified by wj for SMDU Sampler Mode Change
	//FILE *fp = NULL;
	//fp = fopen("/app/config/run/AppLastSolution.run", "rt");
	//if(fp == NULL)
	//{
	//	Cfg_GetFullConfigPath("solution/MonitoringSolution_SMDUCAN.cfg", szFullPath, MAX_FILE_PATH);
	//	ret = Cfg_LoadSolutionConfig(szFullPath);
	//	if (ret != ERR_CFG_OK)
	//	{
	//		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
	//			"[%s]--Cfg_InitialConfig: ERROR: Load solution config "
	//			"failed!\n", __FILE__);
	//		return ret;
	//	}

	//}
	//else
	//{
		Cfg_GetFullConfigPath(SOLUTION_CFGFILE, szFullPath, MAX_FILE_PATH);
		ret = Cfg_LoadSolutionConfig(szFullPath);
		if (ret != ERR_CFG_OK)
		{
			AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
				"[%s]--Cfg_InitialConfig: ERROR: Load solution config "
				"failed!\n", __FILE__);
			return ret;
		}

	//}
	

	/* check the solution Custom Channel Info exist or not */
	/* Equip ID of the Custom Channel Info Section should exist */
	pCustomChannel = g_SiteInfo.pCustomChannel;
	for (i = 0; i < g_SiteInfo.iCustomChannelNum; i++, pCustomChannel++)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));
		if (GetEquip(pCustomChannel->iEqipID) == NULL)
		{
			AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
				"[%s]--Cfg_InitialConfig: ERROR: Equip ID of Custom "
				"Channel Info Section in %s file doesn't exit.\n",
				__FILE__, SOLUTION_CFGFILE);
			return ERR_CFG_BADCONFIG;
		}
	}

	/* 4.get used std equip types num */
	for (i = 0; i < g_SiteInfo.iEquipNum; i++)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));
		pEquip = g_SiteInfo.pEquipInfo + i;
		iEquipTypeID = pEquip->iEquipTypeID;

		for (j = 0; j < g_SiteInfo.iEquipTypeMapNum; j++)  //g_SiteInfo.iEquipTypeMapNum it's from basic file
		{
			pTypeMap = g_SiteInfo.pEquipTypeMap + j;

			if (iEquipTypeID == pTypeMap->iEquipTypeID)
			{
				if (!pTypeMap->bUsed)  /* so the file has not loaded */
				{
					pTypeMap->bUsed = TRUE;
					iUsedTypeNum++;
				}
				break;
			}

		}
		/* not found in the type-map */
		if (j == g_SiteInfo.iEquipTypeMapNum)
		{
			AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
				"[%s]--Cfg_InitialConfig: ERROR: Solution config used "
				"non-defined stdEquip Type!\n", __FILE__);
			return ERR_CFG_BADCONFIG;
		}
	}

	//G3_OPT  [debuginfo]  , to delete
	//AppLogOut("EQUIP INIT", APP_LOG_UNUSED,//APP_LOG_UNUSED,
	//	"Got num...iUsedTypeNum=%d\n",iUsedTypeNum);


	//TRACE("Start get memory and load used std equip config.\n");
	/* 5.get memory and load used std equip config */
	pStdEquip = NEW(STDEQUIP_TYPE_INFO, iUsedTypeNum);
	if (pStdEquip == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: no memory to load std "
			"equip type info!\n", __FILE__);
		return ERR_CFG_NO_MEMORY;
	}

	ZERO_POBJS(pStdEquip, iUsedTypeNum);
	g_SiteInfo.iStdEquipTypeNum = iUsedTypeNum;
	g_SiteInfo.pStdEquipTypeInfo = pStdEquip;

	/* 6.init lang info of g_SiteIno */
	/* only used std equip config file has relevant lang resource file */
	pLangInfo = &g_SiteInfo.LangInfo;
	pLangInfo->pLangFile = NEW(LANG_FILE, iUsedTypeNum);


#ifdef _SUPPORT_THREE_LANGUAGE
    pLangInfoTemp=NEW(LANG_INFO,1);
    pLangInfoTemp->pLangFile = NEW(LANG_FILE, iUsedTypeNum);
#endif

	if (pLangInfo->pLangFile == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: no memory to load lang file "
			"info!n\r", __FILE__);
		return ERR_CFG_NO_MEMORY;
	}
	ZERO_POBJS(pLangInfo->pLangFile, iUsedTypeNum);
	pLangInfo->iLangFileNum = iUsedTypeNum; 
	pLangInfo->pCurLangFile = pLangInfo->pLangFile;

    
#ifdef _SUPPORT_THREE_LANGUAGE
	pLangInfoTemp->iLangFileNum = iUsedTypeNum; //add by wj for three languages 2006.4.27
    pLangInfoTemp->pCurLangFile = pLangInfoTemp->pLangFile;//add by wj for three languages 2006.4.27
#endif

    //7.Load resource file to gSite/
	//TRACE("Start Load resource file to gSite.\n");

	//G3_OPT  [debuginfo]		to delete
	//AppLogOut("EQUIP INIT", APP_LOG_UNUSED,//APP_LOG_UNUSED,
	//	"Start Load std file to gSite\n");

	pTypeMap = g_SiteInfo.pEquipTypeMap;
	for (i = 0; i < g_SiteInfo.iEquipTypeMapNum; i++, pTypeMap++)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));

		if (pTypeMap->bUsed)   /* so we should load */
		{
			/* 1.load relevant language resource file first *///load eng and first loc language file		
			szLangResourceFile = CreateLangResourceFileName(
				pTypeMap->szCfgFileName, pLangInfo->szLocaleLangCode);
			szLangResourceFile = Cfg_GetFullConfigPath(szLangResourceFile, 
				szFullPath, MAX_FILE_PATH);
			if (szLangResourceFile == NULL)
			{
				AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
					"[%s]--Cfg_InitialConfig: ERROR: get language "
					"resource file name for %s failed!\n",
					__FILE__, pTypeMap->szCfgFileName);
				return ERR_CFG_FAIL;
			}

			ret = LoadLangResourseConfig(szLangResourceFile,
				pLangInfo->pCurLangFile);
			if (ret != ERR_CFG_OK)
			{
				AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
					"[%s]--Cfg_InitialConfig: ERROR: load language "
					"resource file for %s failed!\n", 
					__FILE__, pTypeMap->szCfgFileName);
				return ret;
			}

			//TRACE("The %d Cfg_InitialConfig load eng&loc OK!!!\n", i);

///////////////////////////////////////////////////////////////////////////////////////
            /*add by wj load loc2 2006.4.27*/
			//2.Load the second local language file
#ifdef _SUPPORT_THREE_LANGUAGE

            szLangResourceFile = CreateLangResourceFileName(
                pTypeMap->szCfgFileName, pLangInfo->szLocale2LangCode);
            szLangResourceFile = Cfg_GetFullConfigPath(szLangResourceFile, 
                szFullPath, MAX_FILE_PATH);
            if (szLangResourceFile == NULL)
            {
                AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
                    "[%s]--Cfg_InitialConfig: ERROR: get language "
                    "resource file name for %s failed!\n",
                    __FILE__, pTypeMap->szCfgFileName);
                return ERR_CFG_FAIL;
            }

            ret = LoadLangResourseConfig2(szLangResourceFile,
                pLangInfoTemp->pCurLangFile);
            if (ret != ERR_CFG_OK)
            {
                AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
                    "[%s]--Cfg_InitialConfig: ERROR: load language "
                    "resource file for %s failed!\n", 
                    __FILE__, pTypeMap->szCfgFileName);
                return ret;
            }

 #endif

#ifdef	_DEBUG_CFG_LOADER
            TRACE("Cfg_InitialConfig load loc2 OK!!!\n");
#endif

//end///////////////////////////////////////////////////////////////////////////////////////
			//3.load standard equipment config for used std equipment
			ret = Cfg_LoadStdEquipConfig(pTypeMap->szCfgFileName, pStdEquip);
			if (ret != ERR_CFG_OK)
			{
				AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
					"[%s]--Cfg_InitialConfig: ERROR: Load std equip "
					"config failed!\n", __FILE__);
				return ret;
			}	

			/* now check std equip ID defined in the StdEqupXXX.cfg file */
			if (pStdEquip->iTypeID != pTypeMap->iEquipTypeID)
			{
				AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
					"[%s]--Cfg_InitialConfig: ERROR: Std Equip ID "
					"defined in the %s disaccords with %s.\n", __FILE__,
					pTypeMap->szCfgFileName, BASIC_STD_CFGFILE);
				return ERR_CFG_BADCONFIG;
			}
			
			//++ by equip,every equip has a lot of signals in pCurlangfile
			pStdEquip++;
			pLangInfo->pCurLangFile++;

#ifdef _SUPPORT_THREE_LANGUAGE
            pLangInfoTemp->pCurLangFile++;
#endif
		}
	}
		
	//G3_OPT  [debuginfo]   to delete
	//AppLogOut("EQUIP INIT", APP_LOG_UNUSED,//APP_LOG_UNUSED,
	//	"End of Load std file to gSite\n");

//////////////////////////////////////////////////////////////////////////
     /*add by wj load loc2 2006.4.27*/

#ifdef _SUPPORT_THREE_LANGUAGE
    pLangInfoTemp->pCurLangFile = pLangInfoTemp->pLangFile;
    pLangInfo->pCurLangFile = pLangInfo->pLangFile;

    LANG_TEXT *temp1=NULL;
    LANG_TEXT *temp2=NULL;
    LANG_FILE *temp3=NULL;


    int itemp  = 0;
    int itemp2 = 0;
    int NEW_count=0;
    int DEL_count=0;
	//9.Combine to one pointer[2]
	//TRACE("iUsedTypeNum is %d!!!\n",iUsedTypeNum);
    for(itemp=0;itemp<iUsedTypeNum;itemp++)
    {
        if((pLangInfoTemp->pCurLangFile != NULL) && (pLangInfo->pCurLangFile != NULL))
        {
            temp1=pLangInfo->pCurLangFile->pLangText;
            temp2=pLangInfoTemp->pCurLangFile->pLangText;
            if( temp1!=NULL && temp2!=NULL)
            {
				//TRACE("pLangInfo->pCurLangFile->iLangTextNum is %d!!!\n",pLangInfo->pCurLangFile->iLangTextNum);

                for(itemp2=0;itemp2<pLangInfo->pCurLangFile->iLangTextNum;temp1++,temp2++)
                {
					//TRACE("pLangInfo->pCurLangFile->iLangTextNum is %d!!!\n",itemp2);
                    temp1->pFullName[2]=NEW_strdup(temp2->pFullName[2]);
                    temp1->pAbbrName[2]=NEW_strdup(temp2->pAbbrName[2]);
                    itemp2++;
                    NEW_count++;
                }

				//TRACE("For Next Lang File!!!\n");
            }
        }
		
        pLangInfo->pCurLangFile++;
        pLangInfoTemp->pCurLangFile++;

    }

    pLangInfo->pCurLangFile = pLangInfo->pLangFile;
    
    /*for(itemp=0;itemp<iUsedTypeNum;itemp++)
    {
        if(pLangInfo->pCurLangFile != NULL)
        {
            temp1=pLangInfo->pCurLangFile->pLangText;
            
            if( temp1!=NULL)
            {
                for(itemp2=0;itemp2<pLangInfo->pCurLangFile->iLangTextNum;temp1++)
                {
                    printf("%s\n",temp1->pFullName[2]);
                    itemp2++;
                }
            }
        }

        pLangInfo->pCurLangFile++;
    }*/


    pLangInfoTemp->pCurLangFile = pLangInfoTemp->pLangFile;
   
	    for(itemp=0;itemp<iUsedTypeNum;itemp++)
    {
        if(pLangInfoTemp->pCurLangFile != NULL)
        {
            temp1=pLangInfoTemp->pCurLangFile->pLangText;

            if( temp1!=NULL)
            {
                for(itemp2=0;itemp2<pLangInfoTemp->pCurLangFile->iLangTextNum;itemp2++)
                {
                    SAFELY_DELETE(temp1->pFullName[2]);
                    SAFELY_DELETE(temp1->pAbbrName[2]);
                    temp1++;
                    DEL_count++;
                }
            }
            SAFELY_DELETE(pLangInfoTemp->pCurLangFile->pLangText);
        }

        pLangInfoTemp->pCurLangFile++;

    }

    SAFELY_DELETE(pLangInfoTemp->pLangFile);
    SAFELY_DELETE(pLangInfoTemp);

#endif

#ifdef	_DEBUG_CFG_LOADER
    TRACE("Cfg_InitialConfig OK!!!\n");
    TRACE("NEW_count=%d;DEL_count=%d\n",NEW_count,DEL_count);
#endif

    
//end////////////////////////////////////////////////////////////////////////

	//Load User Define Pages Info

#ifdef _CODE_FOR_MINI
	Cfg_GetFullConfigPath(USER_DEF_PAGES_CFGFILE, szFullPath, MAX_FILE_PATH);
	ret = Cfg_LoadUserDefPageInfo(szFullPath);
	if (ret != ERR_CFG_OK)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: Load solution config "
			"failed!\n", __FILE__);
		return ret;
	}
#endif

	//Load LCD Info for G3
	/*Cfg_GetFullConfigPath(USER_DEF_LCDCFG, szFullPath, MAX_FILE_PATH);
	ret = Cfg_LoadUserDefLCDInfo(szFullPath);
	if (ret != ERR_CFG_OK)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: Load LCD solution config "
			"failed!\n", __FILE__);
		return ret;
	}*/
	/***/

    //10.Custom Channel Info
	if (!DispatchCustomChannelInfo())
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: Dispatch cuntom channel to "
			"each equips failed!\n", __FILE__);
		return ERR_CFG_FAIL;
	}
	
	//11.Create Mutex 
	s_hMutexLoadRunConfig = Mutex_Create(TRUE);
	if (s_hMutexLoadRunConfig == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: Load run config mutex "
			"create error",  __FILE__);

			return ERR_CFG_FAIL;
	}

	/*11.Load runtime config changed info */
	if (Cfg_ChkSolutionChanges(&g_SiteInfo))
	{
		Cfg_CleanMainRunFile(TRUE);
	}

	//12.Load old run config
	ret = Cfg_LoadRunConfig();
	/* program which creates MainConfig.run may have bug, so return here */
	if (ret != ERR_CFG_OK)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: Load runtime config changed"
			" info failed!", __FILE__);
		return ret;
	}

	//G3_OPT  [debuginfo] , to delete
	//pEquip = g_SiteInfo.pEquipInfo;
	//printf("\n=====================Total Equips: %d==================\n", g_SiteInfo.iEquipNum);
	//for(i = 0; i < g_SiteInfo.iEquipNum; i++, pEquip++)
	//{
	//	printf("Equip ID: %d\tEquip Name:%s\t", 
	//		pEquip->iEquipID, pEquip->pEquipName->pFullName[0]);
	//	
	//	if (pEquip->iRelevantSamplerListLength > 0)
	//	{
	//		printf("First Sampler ID:%d\n", pEquip->pRelevantSamplerList->iSamplerID);
	//	}
	//	else
	//	{
	//		printf("\n");
	//	}
	//}
	//Here initial g_SiteInfo.pLoadCurrentData Zhao Zicheng 2013-7-29
	g_SiteInfo.pLoadCurrentData = g_stLoadCurrentData;
	// changed by Frank Wu, 20131113, 9/9,for only displaying one data in each interval of 2 hours
	g_SiteInfo.tmLatestUpdate = time(NULL);
	// changed by Frank Wu,20131213,4/8, for keep the same used rated current percent between WEB and LCD
	g_SiteInfo.pstWebLcdShareData = &g_stWebLcdShareData;
	//changed by Frank Wu,20140213,2/9, for displaying history temperature curve more faster
	g_SiteInfo.pstWebLcdShareData->bTrendTIsNeedUpdate = TRUE;

	return ERR_CFG_OK;
}
/*==========================================================================*
* FUNCTION : ParsePageInfoTableProc
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char          *szBuf      : 
*            OUT PORT_INFO *  pStructData : 
* RETURN   : int : loaded failed field ID, zero for success
* COMMENTS : 
* CREATOR  : WJ                   DATE: 2004-09-14 11:57
*==========================================================================*/
static int ParsePageInfoTableProc(IN char *szBuf, OUT PAGE_NAME * pStructData)
{
	char *pField;

	/* used as buffer */
	int iMaxLenForFull, iMaxLenForAbbr;
	char *szText;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Page ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Page ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iPageID = atoi(pField);

	/* 2.max length of full name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	iMaxLenForFull = atoi(pField);
	pStructData->iMaxLenForFull = iMaxLenForFull;	

	/* 3.max length of abbr name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(abbr) "
			"is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	iMaxLenForAbbr = atoi(pField);
	pStructData->iMaxLenForAbbr = iMaxLenForAbbr;

	/* 4.full english name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: full en name "
			"is NULL!\n", __FILE__);

		return 4;
	}
	pStructData->pFullName[0] = NEW(char, iMaxLenForFull+1);
	szText = pStructData->pFullName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: no memory to load "
			"English Full Name info!\n", __FILE__);

		return 4;
	}
	strncpyz_f(szText, pField, iMaxLenForFull+1, Cfg_ReplaceIllegalChr);

	/* 5.abbr english name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: abbr english name "
			"is NULL!\n", __FILE__);

		return 5;
	}
	pStructData->pAbbrName[0] = NEW(char, iMaxLenForAbbr+1);
	szText = pStructData->pAbbrName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: no memory to load "
			"Abbr English Name info!\n", __FILE__);

		return 5;
	}
	strncpyz_f(szText, pField, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);

	/* 6.full locale name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: full locale name"
			" is NULL!\n", __FILE__);

		return 6;
	}
	pStructData->pFullName[1] = NEW(char, iMaxLenForFull+1);
	szText = pStructData->pFullName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: no memory to load "
			"Locale Full Name info!\n", __FILE__);

		return 6;
	}
	strncpyz_f(szText, pField, iMaxLenForFull+1, Cfg_ReplaceIllegalChr);

	/* 7.abbr locale name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: abbr locale name "
			"is NULL!\n", __FILE__);

		return 7;
	}
	pStructData->pAbbrName[1] = NEW(char, iMaxLenForAbbr+1);
	szText = pStructData->pAbbrName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: no memory to load "
			"Locale Abbr Name info!\n", __FILE__);

		return 7;
	}
	strncpyz_f(szText, pField, iMaxLenForAbbr+1, Cfg_ReplaceIllegalChr);

	/* 8.picture eng name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: picture eng name "
			"is NULL!\n", __FILE__);

		return 8;
	}

	szText = pStructData->pFileNameEng;
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: no memory to load "
			"Locale Abbr Name info!\n", __FILE__);

		return 8;
	}
	strncpyz_f(szText, pField, 31, Cfg_ReplaceIllegalChr);

	/* 9.picture eng name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: picture loc name "
			"is NULL!\n", __FILE__);

		return 9;
	}

	szText = pStructData->pFileNameLoc;
	if (szText == NULL)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: no memory to load "
			"Locale Abbr Name info!\n", __FILE__);

		return 9;
	}
	strncpyz_f(szText, pField, 31, Cfg_ReplaceIllegalChr);


#ifdef _CODE_FOR_MINI
	//Frank Wu,20160127, for MiniNCU
	//it is a optional item
	/* 10.iResourceID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		pStructData->iResourceID = -1;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
				"[%s]--ParsePageInfoTableProc: ERROR: iResourceID is not "
				"a number!\n", __FILE__);

			return 10;    /* not a num, error */
		}
		pStructData->iResourceID = atoi(pField);
	}
#endif

	return 0;
}

/*==========================================================================*
* FUNCTION : ParseSigItemInfoTableProc
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char          *szBuf      : 
*            OUT PORT_INFO *  pStructData : 
* RETURN   : int : loaded failed field ID, zero for success
* COMMENTS : 
* CREATOR  : WJ                   DATE: 2004-09-14 11:57
*==========================================================================*/
static int ParseSigItemInfoTableProc(IN char *szBuf, OUT SIG_ITEM * pStructData)
{
	char *pField;

	/* used as buffer */
	char *szText;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Equip ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseSigItemInfoTableProc: ERROR: Equip ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iEquipID = atoi(pField);

	/* 2.Signal Type field */
	//szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	//if ((*pField < '0') || (*pField > '9'))
	//{
	//	AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
	//		"[%s]--ParseSigItemInfoTableProc: ERROR: Signal Type is not "
	//		"a number!\n", __FILE__);

	//	return 2;    /* not a num, error */
	//}
	//pStructData->iSigType = atoi(pField);
	pStructData->iSigType = 2;


	/* 1.Signal ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseSigItemInfoTableProc: ERROR: Signal ID is not "
			"a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 2.Page ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseSigItemInfoTableProc: ERROR: Page ID is not "
			"a number!\n", __FILE__);

		return 4;    /* not a num, error */
	}
	pStructData->iPageID = atoi(pField);


#ifdef _CODE_FOR_MINI
	/* 5.Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((pField != NULL) && (*pField != 0))//exist
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
				"[%s]--ParseSigItemInfoTableProc: ERROR: Signal Type is not "
				"a number!\n", __FILE__);

			return 5;    /* not a num, error */
		}
		
		pStructData->iSigType = atoi(pField);
	}
	else
	{
		pStructData->iSigType = SIG_TYPE_SETTING;
	}
	
	if((SIG_TYPE_CONTROL != pStructData->iSigType)
		&& (SIG_TYPE_SETTING != pStructData->iSigType))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseSigItemInfoTableProc: ERROR: Signal Type is not "
			"a valid number!\n", __FILE__);

		return 5;    /* not a num, error */
	}
#endif

	return 0;


}

/*for G3*/


/*static int LoadUserDefLCDInfoProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	SITE_INFO *pBuf;
	char szLineData[MAX_LINE_SIZE];
	int  ret;
	CONFIG_TABLE_LOADER loader[7];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (SITE_INFO *)pLoadToBuf;

	pBuf->pUserDefLCD = NEW(USER_DEF_LCD,1);
	pBuf->pUserDefLCD->pDispInfoLCD = NEW(DISPINFO_LCD,1);
	printf("NEW DISPINFO_LCD OK\n");
	
	//1.Read tables: Page Info,Signal Info
	DEF_LOADER_ITEM(&loader[0], 
		NULL, &(pBuf->pUserDefLCD->pDispInfoLCD->iDispInfoNum), 
		DISP_INFORMATION,&(pBuf->pUserDefLCD->pDispInfoLCD->pSigInfoLCD), 
		ParseDispInfoTableProc);

	DEF_LOADER_ITEM(&loader[1], 
		NULL, &(pBuf->pUserDefLCD->pDispInfoICO->iICOInfoNum), 
		ICO_INFO,&(pBuf->pUserDefLCD->pDispInfoICO->pSigInfoICO), 
		ParseIcoInfoTableProc);

	DEF_LOADER_ITEM(&loader[2], 
		NULL, &(pBuf->pUserDefLCD->pDispInfoMOD->iMODInfoNum), 
		MODULE_INFO,&(pBuf->pUserDefLCD->pDispInfoMOD->pSigInfoMOD), 
		ParseModuleInfoProc);
	DEF_LOADER_ITEM(&loader[3], 
		NULL, &(pBuf->pUserDefLCD->pDispInfoDistr->iDistrNum), 
		DISTRIB_INFO,&(pBuf->pUserDefLCD->pDispInfoDistr->pSigInfoDistr), 
		ParseDistribInfoProc);
	DEF_LOADER_ITEM(&loader[4], 
		NULL, &(pBuf->pUserDefLCD->pDispInfoTemp->iTempNum), 
		TEMP_INFO,&(pBuf->pUserDefLCD->pDispInfoTemp->pSigInfoTemp), 
		ParseTempInfoProc);
	DEF_LOADER_ITEM(&loader[5], 
		NULL, &(pBuf->pUserDefLCD->pDispInfoBatt->iBattInfoNum), 
		BATT_INFO,&(pBuf->pUserDefLCD->pDispInfoBatt->pSigInfoBatt), 
		ParseBattInfoProc);
	DEF_LOADER_ITEM(&loader[6], 
		NULL, &(pBuf->pUserDefLCD->pDispInfoSET->iSETInfoNum), 
		SETTING_INFO,&(pBuf->pUserDefLCD->pDispInfoSET->pSigInfoSET), 
		ParseSetInfoProc);
	
	
	
	printf("Parse OK\n");

	if (Cfg_LoadTables(pCfg,2,loader) != ERR_CFG_OK)
	{
		printf("CFG Fail\n");
		return ERR_CFG_FAIL;
	}

	printf("CFG OK\n");
	return ERR_CFG_OK;	

}*/

/*==========================================================================*
* FUNCTION : LoadUserDefPageInfoProc
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN void        *pCfg       : 
*            OUT SITE_INFO  *pLoadToBuf : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : WJ                   DATE: 2004-09-16 11:19
*==========================================================================*/
static int LoadUserDefPageInfoProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	SITE_INFO *pBuf;
	char szLineData[MAX_LINE_SIZE];
	int  ret;
	CONFIG_TABLE_LOADER loader[2];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (SITE_INFO *)pLoadToBuf;

	pBuf->pUserDefPages = NEW(USER_DEF_PAGES,1);
	if(pBuf->pUserDefPages == NULL)
	{
		return ERR_CFG_FAIL;
	}
	pBuf->pUserDefPages->pPageInfo = NEW(PAGE_INFO,1);
	if(pBuf->pUserDefPages->pPageInfo == NULL)
	{
		DELETE(pBuf->pUserDefPages);
		return ERR_CFG_FAIL;
	}
	pBuf->pUserDefPages->pSigItemInfo = NEW(SIG_ITEM_INFO,1);
	if(pBuf->pUserDefPages->pSigItemInfo == NULL)
	{
		DELETE(pBuf->pUserDefPages);
		DELETE(pBuf->pUserDefPages->pPageInfo);
		return ERR_CFG_FAIL;
	}

	//1.Read tables: Page Info,Signal Info
	DEF_LOADER_ITEM(&loader[0], 
		NULL, &(pBuf->pUserDefPages->pPageInfo->iPageInfoNum), 
		PAGE_INFORMATION,&(pBuf->pUserDefPages->pPageInfo->pPageName), 
		ParsePageInfoTableProc);

	DEF_LOADER_ITEM(&loader[1],
		NULL, &(pBuf->pUserDefPages->pSigItemInfo->iSigItemNum), 
		SETTING_SIGNAL, &(pBuf->pUserDefPages->pSigItemInfo->pSigItem), 
		ParseSigItemInfoTableProc);


	if (Cfg_LoadTables(pCfg,2,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;	

}

/*for G3*/
/*static int Cfg_LoadUserDefLCDInfo(char *szConfigFile)
{
	int ret;
	ret = Cfg_LoadConfigFile(szConfigFile, LoadUserDefLCDInfoProc, &g_SiteInfo);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}*/
/***/
/*==========================================================================*
* FUNCTION : Cfg_LoadUserDefPageInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char  *szConfigFile : solution file name
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : WJ                  DATE: 2004-9-16 12:07
*==========================================================================*/
static int Cfg_LoadUserDefPageInfo(char *szConfigFile)
{
	int ret;
	ret = Cfg_LoadConfigFile(szConfigFile, LoadUserDefPageInfoProc, &g_SiteInfo);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}
//////////////////////////////////////////////////////////////////////////

/*==========================================================================*
 * FUNCTION : FreeLangText
 * PURPOSE  : assitant fuctin for Cfg_UnloadConfig interface
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: LANG_TEXT  *pLangText : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-08 10:54
 *==========================================================================*/
static void FreeLangText(LANG_TEXT *pLangText)
{
	int i;

	if (pLangText == NULL)
	{
		return;
	}

	for (i = 0; i < MAX_LANG_TYPE; i++)
	{
		SAFELY_DELETE(pLangText->pFullName[i]);
		SAFELY_DELETE(pLangText->pAbbrName[i]);
	}

	return;
}
/*==========================================================================*
* FUNCTION : FreePageName
* PURPOSE  : assitant fuctin for Cfg_UnloadConfig interface
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: LANG_TEXT  *pLangText : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : WJ                   DATE: 2004-10-08 10:54
*==========================================================================*/
static void FreePageName(PAGE_NAME *pLangText)
{
	int i;

	if (pLangText == NULL)
	{
		return;
	}

	for (i = 0; i < MAX_LANG_TYPE; i++)
	{
		SAFELY_DELETE(pLangText->pFullName[i]);
		SAFELY_DELETE(pLangText->pAbbrName[i]);
	}

	return;
}

/*==========================================================================*
 * FUNCTION : Cfg_FreeTempExpression
 * PURPOSE  : assitant fuctin for Cfg_UnloadConfig interface
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int          iExpLenth     : number of elements in the expression
 *            EXP_ELEMENT  **pExpression : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-08 11:29
 *==========================================================================*/
void Cfg_FreeTempExpression(int iExpLenth, EXP_ELEMENT **pExpression)
{
	int i;
	EXP_ELEMENT *pEle;

	for (i = 0; i < iExpLenth; i++)
	{
		if (pExpression[i] != NULL)
		{
			pEle = pExpression[i];
			SAFELY_DELETE(pEle->pElement);

			SAFELY_DELETE(pEle);
		}
	}

	SAFELY_DELETE(pExpression);
}

static void FreeExpression(int iExpLenth, EXP_ELEMENT *pExpression)
{
	int i;
	EXP_ELEMENT *pEle = pExpression;

	for (i = 0; i < iExpLenth; i++, pEle++)
	{
		SAFELY_DELETE(pEle->pElement);	
	}

	SAFELY_DELETE(pExpression);
}

/*==========================================================================*
 * FUNCTION : Cfg_UnloadConfig
 * PURPOSE  : free memory managed by g_SiteInfo
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 20:36
 *==========================================================================*/
void Cfg_UnloadConfig()
{
	int i, j;

	/* reference pointer to free inner memory */
	STDEQUIP_TYPEMAP_INFO	*pTypeMap;
	STDPORT_INFO			*pStdPort;
	STDDCP_INFO				*pStdDCP;
	STDSAMPLER_INFO			*pStdSampler;
	
#ifdef PRODUCT_INFO_SUPPORT
	DEVICE_TYPE_MAP         *pDeviceTypeMap;
#endif //PRODUCT_INFO_SUPPORT


	STDEQUIP_TYPE_INFO		*pStdEquip;
	SAMPLE_SIG_INFO			*pSampleSig;
	CTRL_SIG_INFO			*pCtrlSig;
	SET_SIG_INFO			*pSetSig;
	ALARM_SIG_INFO			*pAlarmSig;

	PORT_INFO				*pPort;
	SAMPLER_INFO			*pSampler;
	EQUIP_INFO				*pEquip;

	LANG_INFO				*pLangInfo;
	LANG_FILE				*pLangFile;
	LANG_TEXT				*pLangText;

#ifdef CR_CFGFILE_INFO
	CONFIG_INFORMATION		*pConfigInfo; //used to display in lcd	--caihao  2005-12-22
#endif

	USER_DEF_PAGES			*pUserDefPages;

	RunThread_Heartbeat(RunThread_GetId(NULL));

	/* 1.free basic info */
	/* 1.free site name and so on */
	FreeLangText(&g_SiteInfo.langSiteName);
	FreeLangText(&g_SiteInfo.langDescription);
	FreeLangText(&g_SiteInfo.langSiteLocation);

	/* 1.2 free config file info     --caihao 2005.12.22*/
#ifdef CR_CFGFILE_INFO

	pConfigInfo = g_SiteInfo.pConfigInformation;
	
	if(pConfigInfo != NULL)
	{
		if(pConfigInfo->szCfgName != NULL)
		{
			SAFELY_DELETE(pConfigInfo->szCfgName);
		}
		if(pConfigInfo->szCfgVersion != NULL)
		{
			SAFELY_DELETE(pConfigInfo->szCfgVersion);
		}
		if(pConfigInfo->szCfgDate != NULL)
		{
			SAFELY_DELETE(pConfigInfo->szCfgDate);
		}

		if(pConfigInfo->szCfgEngineer != NULL)
		{
			SAFELY_DELETE(pConfigInfo->szCfgEngineer);
		}

		if(pConfigInfo->szCfgDescription != NULL)
		{
			SAFELY_DELETE(pConfigInfo->szCfgDescription);
		}

		SAFELY_DELETE(g_SiteInfo.pConfigInformation);	 
	}
#endif  
	/* 2.free std config info */
	/* 2.1 std port */
	for (i = 0; i < g_SiteInfo.iStdPortNum; i++)					
	{
		pStdPort = g_SiteInfo.pStdPortInfo + i;
		SAFELY_DELETE(pStdPort->szPortAccessingDriver);
		SAFELY_DELETE(pStdPort->szTypeName);
	}
	SAFELY_DELETE(g_SiteInfo.pStdPortInfo);

	/* 2.2 std sampler */
	for (i = 0; i < g_SiteInfo.iStdSamplerNum; i++)			
	{
		pStdSampler = g_SiteInfo.pStdSamplerInfo + i;
		SAFELY_DELETE(pStdSampler->szStdSamplerDriver);
		SAFELY_DELETE(pStdSampler->szStdSamplerName);
	}
	SAFELY_DELETE(g_SiteInfo.pStdSamplerInfo);

	/* 2.3 std data communication protocol */
	for (i = 0; i < g_SiteInfo.iStdDCPNum; i++)				
	{
		pStdDCP = g_SiteInfo.pStdDCPInfo + i;
		SAFELY_DELETE(pStdDCP->szDCPDriver);
		SAFELY_DELETE(pStdDCP->szDCPName);
	}
	SAFELY_DELETE(g_SiteInfo.pStdDCPInfo);

	/* 2.4 std equip type map */
	for (i = 0; i < g_SiteInfo.iEquipTypeMapNum; i++)		
	{
		pTypeMap = g_SiteInfo.pEquipTypeMap + i;
		SAFELY_DELETE(pTypeMap->szCfgFileName);
	}
	SAFELY_DELETE(g_SiteInfo.pEquipTypeMap);

    /* 2.5 Devive Type map, added by Thomas, 2006-01-24 */
#ifdef PRODUCT_INFO_SUPPORT
	for (i = 0; i < g_SiteInfo.iDeviceTypes; i++)
	{
		pDeviceTypeMap = g_SiteInfo.pDeviceTypeMap + i;
		SAFELY_DELETE(pDeviceTypeMap->szDeviceName);
		SAFELY_DELETE(pDeviceTypeMap->szPartNumber);
	}
	SAFELY_DELETE(g_SiteInfo.pDeviceTypeMap);
#endif //PRODUCT_INFO_SUPPORT

	/* 2.6 std equip */
	for (i = 0; i < g_SiteInfo.iStdEquipTypeNum; i++)		
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));

		pStdEquip = g_SiteInfo.pStdEquipTypeInfo + i;

		/* sampling sig */
		for (j = 0; j < pStdEquip->iSampleSigNum; j++)		
		{
			pSampleSig = pStdEquip->pSampleSigInfo + j;
			SAFELY_DELETE(pSampleSig->szSigUnit);
			SAFELY_DELETE(pSampleSig->szValueDisplayFmt);
			SAFELY_DELETE(pSampleSig->pStateText);
			FreeExpression(pSampleSig->iCalculateExpression,
				pSampleSig->pCalculateExpression);
			FreeExpression(pSampleSig->iDispExp,
				pSampleSig->pDispExp);

		}
		SAFELY_DELETE(pStdEquip->pSampleSigInfo);

		/* control sig */
		for (j = 0; j < pStdEquip->iCtrlSigNum; j++)
		{
			pCtrlSig = pStdEquip->pCtrlSigInfo + j;

			SAFELY_DELETE(pCtrlSig->szSigUnit);
			SAFELY_DELETE(pCtrlSig->szValueDisplayFmt);
			SAFELY_DELETE(pCtrlSig->szCtrlParam);  
			SAFELY_DELETE(pCtrlSig->pCtrlExpression);
			SAFELY_DELETE(pCtrlSig->pStateText);
			FreeExpression(pCtrlSig->iControllableExpression,
				pCtrlSig->pControllableExpression);
			FreeExpression(pCtrlSig->iDispExp,
				pCtrlSig->pDispExp);

		}
		SAFELY_DELETE(pStdEquip->pCtrlSigInfo);

		/* setting sig */
		for (j = 0; j < pStdEquip->iSetSigNum; j++)
		{
			pSetSig = pStdEquip->pSetSigInfo + j;

			SAFELY_DELETE(pSetSig->szSigUnit);
			SAFELY_DELETE(pSetSig->szValueDisplayFmt);
			SAFELY_DELETE(pSetSig->pCtrlExpression);
			SAFELY_DELETE(pSetSig->pStateText);
			FreeExpression(pSetSig->iSettableExpression,
				pSetSig->pSettableExpression);
			FreeExpression(pSetSig->iDispExp,
				pSetSig->pDispExp);

		}
		SAFELY_DELETE(pStdEquip->pSetSigInfo);

		/* alarm sig */
		for (j = 0; j < pStdEquip->iAlarmSigNum; j++)
		{
			pAlarmSig = pStdEquip->pAlarmSigInfo + j;

			FreeExpression( pAlarmSig->iAlarmExpression,
				pAlarmSig->pAlarmExpression);

			FreeExpression(pAlarmSig->iSuppressExpression,
				pAlarmSig->pSuppressExpression);

		}
		SAFELY_DELETE(pStdEquip->pAlarmSigInfo);
	}
	SAFELY_DELETE(g_SiteInfo.pStdEquipTypeInfo);

	RunThread_Heartbeat(RunThread_GetId(NULL));

	/* 3.free solution config info*/
	/* port */
	for (i = 0; i < g_SiteInfo.iPortNum; i++)				
	{
		pPort = g_SiteInfo.pPortInfo + i;

		SAFELY_DELETE(pPort->szPortName);
		SAFELY_DELETE(pPort->szDeviceDesp);
		SAFELY_DELETE(pPort->szAccessingDriverSet);
	}
	SAFELY_DELETE(g_SiteInfo.pPortInfo);

	/* sampler */
	for (i = 0; i < g_SiteInfo.iSamplerNum; i++)
	{
		pSampler = g_SiteInfo.pSamplerInfo + i;

		FreeLangText(pSampler->pSamplerName);
		SAFELY_DELETE(pSampler->pSamplerName);
		SAFELY_DELETE(pSampler->szPortSetting);
	}
	SAFELY_DELETE(g_SiteInfo.pSamplerInfo);

	/* equip */
	for (i = 0; i < g_SiteInfo.iEquipNum; i++)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));

		pEquip = g_SiteInfo.pEquipInfo + i;

		/* free names */
		FreeLangText(pEquip->pEquipName);
		FreeLangText(pEquip->pFactoryName);
		SAFELY_DELETE(pEquip->pEquipName);
		SAFELY_DELETE(pEquip->pFactoryName);

		// use static array. maofuhua, 2005-3-10
		//SAFELY_DELETE(pEquip->szSerialNumber);
		SAFELY_DELETE(pEquip->pRelevantSamplerList);
		SAFELY_DELETE(pEquip->pRelevantEquipList);

		FreeExpression(pEquip->iAlarmFilterExpression,
			pEquip->pAlarmFilterExpressionCfg);

	}
	SAFELY_DELETE(g_SiteInfo.pEquipInfo);

	/* Custom Channels */
	SAFELY_DELETE(g_SiteInfo.pCustomChannel);


	/* 4.free language info */
	pLangInfo = &(g_SiteInfo.LangInfo);
	for (i = 0; i < pLangInfo->iLangFileNum; i++)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));

		pLangFile = pLangInfo->pLangFile + i;

		/* free Lang Texts in each Lang File */
		pLangText = pLangFile->pLangText;
		for (j = 0; j < pLangFile->iLangTextNum; j++, pLangText++)
		{
			FreeLangText(pLangText);	
		}

		/* now free Lang File */
		SAFELY_DELETE(pLangFile->pLangText);
	}

	/* now free Lang Info */
	SAFELY_DELETE(pLangInfo->pLangFile);
	SAFELY_DELETE(pLangInfo->szLocaleLangCode);
	//SAFELY_DELETE(pLangInfo->szLocale2LangCode);

	/* 5.free user define page info */
	pUserDefPages = g_SiteInfo.pUserDefPages;
	PAGE_NAME *pTemp = NULL;
	if(pUserDefPages != NULL && pUserDefPages->pPageInfo != NULL)
	{

		for(i = 0; i < pUserDefPages->pPageInfo->iPageInfoNum; i++)
		{
			TRACE("\n___UNLOAD___\n");
			pTemp = pUserDefPages->pPageInfo->pPageName + i;
			FreePageName(pTemp);
			
		}
		TRACE("\n___UNLOAD2___\n");
		SAFELY_DELETE(pUserDefPages->pPageInfo->pPageName);
		SAFELY_DELETE(pUserDefPages->pPageInfo);
		SAFELY_DELETE(pUserDefPages->pSigItemInfo->pSigItem);
		SAFELY_DELETE(pUserDefPages->pSigItemInfo);
		
	}

	SAFELY_DELETE(pUserDefPages);

	if (s_hMutexLoadRunConfig != NULL) 
	{ 
		Mutex_Destroy(s_hMutexLoadRunConfig);

		s_hMutexLoadRunConfig = NULL;
	}


	TRACE("\n___UNLOAD3___\n");

	return;
}

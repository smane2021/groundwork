 /*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cfg_load_solution.c
 *  CREATOR  : LinTao                   DATE: 2004-09-14 17:04
 *  VERSION  : V1.00
 *  PURPOSE  : to load solution config file
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"


#include "cfg_mgmt.h"
#include "cfg_helper.h"


/* assitant function */
static BOOL IsBoundChr_B(int c)
{
	return ((char)c == '(' || (char)c == ')');
};


BOOL Cfg_CheckLangCode(IN const char *pField)
{
	int iLen;

	iLen = strlen(pField);
	if (iLen != 2)
	{
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
* FUNCTION : ParseConfigInfoTableProc
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char        *szBuf       : 
*            OUT CONFIG_INFORMATION  *pStructData : 
* RETURN   : int : loaded failed field ID, zero for success
* COMMENTS : 
* CREATOR  : CaiHao                   DATE: 2005-12-27 11:59
*==========================================================================*/
#ifdef CR_CFGFILE_INFO		//CR:add config version to lcd	-caihao  2005.12.23

static int ParseConfigInfoTableProc(IN char *szBuf,OUT SITE_INFO *pStructData)
{
	char *pField;
	char *szText;
	
	ASSERT(szBuf);
	ASSERT(pStructData);

	/*0.Initial for config info struct */
	CONFIG_INFORMATION	*pConfigInfo = NEW(CONFIG_INFORMATION,1);

	ZERO_POBJS(pConfigInfo,1);

	pStructData->pConfigInformation = pConfigInfo;
	if(pConfigInfo == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:no memory to create "
			"CONFIG_INFORMATION struct!\n",__FILE__);
		return 1;
	}
	
	/*1.solution config file name */
	szBuf = Cfg_SplitStringEx(szBuf,&pField,SPLITTER);
	if(*pField =='\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:Config File Name is "
			"NULL!\n",__FILE__);
		return	1;
	}
	pStructData->pConfigInformation->szCfgName = NEW_strdup(pField);
	szText = pStructData->pConfigInformation->szCfgName;

	if(szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:no memory to load "
			"Config Name info!\n",__FILE__);
		return 1;
	}

	/* 2.solution config file version */
	szBuf = Cfg_SplitStringEx(szBuf,&pField,SPLITTER);
	if(*pField =='\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:Config File Version is "
            "NULL!\n",__FILE__);
		return 2;
	}
	pStructData->pConfigInformation->szCfgVersion = NEW_strdup(pField);
	szText = pStructData->pConfigInformation->szCfgVersion;
	if(szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:no memory to load "
			"Config Version info!\n",__FILE__);
		return 2;
	}

	/* 3.solution config file date */
	szBuf = Cfg_SplitStringEx(szBuf,&pField,SPLITTER);
	if(*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:Config File Date is "
			"NULL!\n",__FILE__);
		return 3;
	}
	pStructData->pConfigInformation->szCfgDate = NEW_strdup(pField);
	szText= pStructData->pConfigInformation->szCfgDate;
	if(szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:no memery to load "
			"Config Date info!\n",__FILE__);
		return 3;
	}

	/*4.solution config file engineer*/
	szBuf = Cfg_SplitStringEx(szBuf,&pField,SPLITTER);
	if(*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:Config File Engineer is "
			"NULL!\n",__FILE__);
		return 4;
	}
	pStructData->pConfigInformation->szCfgEngineer = NEW_strdup(pField);
	szText= pStructData->pConfigInformation->szCfgEngineer;
	if(szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:no memery to load "
			"Config Engineer info!\n",__FILE__);
		return 4;
	}

	/*5.solution config file description*/
	szBuf = Cfg_SplitStringEx(szBuf,&pField,SPLITTER);
	if(*pField =='\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:Config File Description is "
			"NULL!\n",__FILE__);
		return 5;
	}
	pStructData->pConfigInformation->szCfgDescription = NEW_strdup(pField);
	szText = pStructData->pConfigInformation->szCfgDescription;
	if(szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--ParseConfigInfoTableProc:ERROR:no  memery to load "
			"Config Description info!\n",__FILE__);
		return 5;
	}

	AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_UNUSED,
		"[%s]--Config File Information "
		"Parse Successful!\n",__FILE__);
	return 0;

}			/* caihao 2005-12-22		09:47 */

#endif

/*==========================================================================*
 * FUNCTION : ParseLangInfoTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char        *szBuf       : 
 *            OUT LANG_INFO  *pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-14 16:49
 *==========================================================================*/
static int ParseLangInfoTableProc(IN char *szBuf, OUT LANG_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.locale language code field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (!Cfg_CheckLangCode(pField))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseLangInfoTableProc: ERROR: Language code is "
			"invalid!\n", __FILE__);

		return 1;    
	}
	pStructData->szLocaleLangCode = NEW_strdup(pField);
	if (pStructData->szLocaleLangCode == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseLangInfoTableProc: ERROR: no memory to load "
			"Locale Lang Code info!\n", __FILE__);

		return 1;  
	}
	
	if(strstr(g_SiteInfo.LangInfo.szLocaleLangCode,"zh") || strstr(g_SiteInfo.LangInfo.szLocaleLangCode,"tw"))
	{		
		pStructData->iCodeType = ENUM_GB2312;
	}
	else if(strstr(g_SiteInfo.LangInfo.szLocaleLangCode,"ru") )
	{
		pStructData->iCodeType = ENUM_KOI8R;
	}
	else
	{
		pStructData->iCodeType = ENUM_UTF8;
	}
		

	/* 2.max length for full name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseLangInfoTableProc: ERROR: max length for "
			"full name is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	pStructData->iMaxFullNameLen = atoi(pField) + 1; //leave place for '\0'

	/* 3.max length for abbreviated name */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseLangInfoTableProc: ERROR: max length for "
			"abbreviated name is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iMaxAbbrNameLen = atoi(pField) + 1; //leave place for '\0'

    /* 4.locale2 language code field */
    //////////////////////////////////////////////////////////////////////////
    //Add by wj for three languages 2006.4.27
#ifdef _SUPPORT_THREE_LANGUAGE
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (!Cfg_CheckLangCode(pField))
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseLangInfoTableProc: ERROR: Language code is "
            "invalid!\n", __FILE__);

        return 1;    
    }
    pStructData->szLocale2LangCode = NEW_strdup(pField);
    if (pStructData->szLocale2LangCode == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseLangInfoTableProc: ERROR: no memory to load "
            "Locale2 Lang Code info!\n", __FILE__);

        return 1;  
    }
#endif

    //end////////////////////////////////////////////////////////////////////////
    

	return 0;
}

/*==========================================================================*
 * FUNCTION : ParseSiteInfoTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char          *szBuf       : 
 *            OUT SITE_INFO    *pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-14 11:25
 *==========================================================================*/
static int ParseSiteInfoTableProc(IN char *szBuf, OUT SITE_INFO *pStructData)
{
	char *pField;

	/* to buffer the solution lang info */
	int  iMaxFullNameLen, iMaxAbbrNameLen;
	char *szText;  /* to buffer text string */

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* get the buffered value */
	iMaxFullNameLen = pStructData->LangInfo.iMaxFullNameLen;
	iMaxAbbrNameLen = pStructData->LangInfo.iMaxAbbrNameLen;

	/* 1.Site ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: Site ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iSiteID = atoi(pField);

	/* 2.Site Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: Site Name "
			"is NULL!\n", __FILE__);

		return 2;    
	}
	ZERO_POBJS(&pStructData->langSiteName, 1);

	pStructData->langSiteName.iMaxLenForFull = iMaxFullNameLen;
	pStructData->langSiteName.iMaxLenForAbbr = iMaxAbbrNameLen;

	pStructData->langSiteName.pFullName[0] = NEW(char, iMaxFullNameLen);
	szText = pStructData->langSiteName.pFullName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
			"Site Name info!\n", __FILE__);

		return 2;  
	}
	strncpyz(szText, pField, iMaxFullNameLen);

	/* 3.Site Abbrevated Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: Site Abbr Name is "
			"NULL!\n", __FILE__);

		return 3;    
	}
	pStructData->langSiteName.pAbbrName[0] = NEW(char, iMaxAbbrNameLen);
	szText = pStructData->langSiteName.pAbbrName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
			"Site Name info!\n", __FILE__);

		return 3;  
	}
	strncpyz(szText, pField, iMaxAbbrNameLen);

	/* 4.Site Locale Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: Site Locale Name is "
			"NULL!\n", __FILE__);

		return 4;    
	}
	pStructData->langSiteName.pFullName[1] = NEW(char, iMaxFullNameLen);
	szText = pStructData->langSiteName.pFullName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
			"Site Name info!\n", __FILE__);

		return 4;  
	}
	strncpyz(szText, pField, iMaxFullNameLen);

	/* 5.Site Abbrevated Locale Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: Site Abbr Locale Name "
			"is NULL!\n", __FILE__);

		return 5;    
	}
	pStructData->langSiteName.pAbbrName[1] = NEW(char, iMaxAbbrNameLen);
	szText = pStructData->langSiteName.pAbbrName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
			"Site Name info!\n", __FILE__);

		return 5;  
	}
	strncpyz(szText, pField, iMaxAbbrNameLen);

	/* 6.Attribute field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iSiteAttr = 0;
	}
	else
	{
		if ((*pField != '0') || (*(pField + 1) != 'x' && *(pField + 1) != 'X'))
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseSiteInfoTableProc: ERROR: Site Attribute is "
				"invalid!\n", __FILE__);

			return 6;
		}
		sscanf(pField, "%x", &pStructData->iSiteAttr);
	}

	/* 7.Location field(English) */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: Site Location(English)"
			" is NULL!\n", __FILE__);

		return 7;    
	}

	ZERO_POBJS(&pStructData->langSiteLocation, 1);

	pStructData->langSiteLocation.iMaxLenForFull = iMaxFullNameLen;
	pStructData->langSiteLocation.pFullName[0] = NEW(char, iMaxFullNameLen);
	szText = pStructData->langSiteLocation.pFullName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
			"Site Location info!\n", __FILE__);

		return 7;  
	}
	strncpyz(szText, pField, iMaxFullNameLen);

	/* 8.Location field(Locale language) */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: Site Location"
			"(Locale language) is NULL!\n", __FILE__);

		return 8;    
	}

	pStructData->langSiteLocation.pFullName[1] = NEW(char, iMaxFullNameLen);
	szText = pStructData->langSiteLocation.pFullName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
			"Site Location info!\n", __FILE__);

		return 8;  
	}
	strncpyz(szText, pField, iMaxFullNameLen);

	/* 9.Description field(English) */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	ZERO_POBJS(&pStructData->langDescription, 1);
	pStructData->langDescription.iMaxLenForFull = iMaxFullNameLen;
	if (Cfg_IsEmptyField(pField))
	{
		/* do nothing */
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseSiteInfoTableProc: ERROR: Site Description"
				"(English) is invalid!\n", __FILE__);

			return 9;    
		}
		pStructData->langDescription.pFullName[0] = NEW(char, iMaxFullNameLen);
		szText = pStructData->langDescription.pFullName[0];
		if (szText== NULL)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
				"Site Description info!\n", __FILE__);

			return 9;  
		}
		strncpyz(szText, pField, iMaxFullNameLen);
	}

	/* 10.Description field(Locale language) */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	if (Cfg_IsEmptyField(pField))
	{
		/* do nothing */
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseSiteInfoTableProc: ERROR: Site Description"
				"(Locale Language) is invalid!\n", __FILE__);

			return 10;    
		}
		pStructData->langDescription.pFullName[1] = NEW(char, iMaxFullNameLen);
		szText = pStructData->langDescription.pFullName[1];
		if (szText== NULL)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
				"Site Description info!\n", __FILE__);

			return 10;  
		}
		strncpyz(szText, pField, iMaxFullNameLen);
	}

//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.5.9
    /* 11.Site Locale2 Name */
#ifdef _SUPPORT_THREE_LANGUAGE
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSiteInfoTableProc: ERROR: Site Locale2 Name is "
            "NULL!\n", __FILE__);

        return 4;    
    }
    pStructData->langSiteName.pFullName[2] = NEW(char, iMaxFullNameLen);
    szText = pStructData->langSiteName.pFullName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
            "Site Name info!\n", __FILE__);

        return 4;  
    }
    strncpyz(szText, pField, iMaxFullNameLen);

    //TRACE("Site Locale2 Name OK!!!\n");

    /* 12.Site Abbrevated Locale2 Name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSiteInfoTableProc: ERROR: Site Abbr Locale2 Name "
            "is NULL!\n", __FILE__);

        return 5;    
    }
    pStructData->langSiteName.pAbbrName[2] = NEW(char, iMaxAbbrNameLen);
    szText = pStructData->langSiteName.pAbbrName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
            "Site Name info!\n", __FILE__);

        return 5;  
    }
    strncpyz(szText, pField, iMaxAbbrNameLen);

    //TRACE("Site Abbrevated Locale2 Name field OK!!!\n");

    /* 13.Location field(Locale2 language) */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSiteInfoTableProc: ERROR: Site Location"
            "(Locale language) is NULL!\n", __FILE__);

        return 8;    
    }

    pStructData->langSiteLocation.pFullName[2] = NEW(char, iMaxFullNameLen);
    szText = pStructData->langSiteLocation.pFullName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
            "Site Location info!\n", __FILE__);

        return 8;  
    }
    strncpyz(szText, pField, iMaxFullNameLen);

    //TRACE("Location field(Locale2 language) OK!!!\n");

    /* 14.Description field(Locale2 language) */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

    if (Cfg_IsEmptyField(pField))
    {
        /* do nothing */
    }
    else
    {
        if (*pField == '\0')
        {
            AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
                "[%s]--ParseSiteInfoTableProc: ERROR: Site Description"
                "(Locale2 Language) is invalid!\n", __FILE__);

            return 10;    
        }
        pStructData->langDescription.pFullName[2] = NEW(char, iMaxFullNameLen);
        szText = pStructData->langDescription.pFullName[2];
        if (szText== NULL)
        {
            AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
                "[%s]--ParseSiteInfoTableProc: ERROR: no memory to load "
                "Site Description info!\n", __FILE__);

            return 10;  
        }
        strncpyz(szText, pField, iMaxFullNameLen);
    }
#endif
    //TRACE("Description field(Locale2 language) OK!!!\n");

//end////////////////////////////////////////////////////////////////////////

	return 0;
}

/*==========================================================================*
 * FUNCTION : ParsePortInfoTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char          *szBuf      : 
 *            OUT PORT_INFO *  pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-14 11:57
 *==========================================================================*/
static int ParsePortInfoTableProc(IN char *szBuf, OUT PORT_INFO * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Port ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParsePortInfoTableProc: ERROR: Port ID is not "
			"a number!\n", __FILE__);
		return 1;    /* not a num, error */
	}
	pStructData->iPortID = atoi(pField);

	/* 2.Port Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParsePortInfoTableProc: ERROR: Port Name is "
			"NULL!\n", __FILE__);

		return 2;
	}
	pStructData->szPortName = NEW_strdup(pField);
	if (pStructData->szPortName == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParsePortInfoTableProc:ERROR: no memory to load "
			"Port Name info!\n", __FILE__);

		return 2;  
	}

	/* 3.Port Type ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParsePortInfoTableProc: ERROR: Port type ID is "
			"not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iStdPortTypeID = atoi(pField);

	/* 4.Device Descriptor field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParsePortInfoTableProc: ERROR: Device Descriptor is "
			"NULL!\n", __FILE__);

		return 4;    
	}
	pStructData->szDeviceDesp = NEW_strdup(pField);
	if (pStructData->szDeviceDesp == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParsePortInfoTableProc: ERROR: no memory to load "
			"Device Description info!\n", __FILE__);

		return 4;  
	}

	/* 5.Disconnect Delay field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField != '-' && *(pField+1) != '1')  /* -1 is valid */
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParsePortInfoTableProc: ERROR: Disconnect Delay "
				"is not a number!\n", __FILE__);

			return 5;    /* not a num, error */
		}
	}
	pStructData->iDisconnDelay = atoi(pField);

	/* 6.Timeout field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iTimeout = 5000;    /* unit: ms, default value = 5000 */
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParsePortInfoTableProc: ERROR: Timeout value "
				"is not a number!\n", __FILE__);

			return 6;    /* not a num, error */
		}
		pStructData->iTimeout = atoi(pField);
	}

	/* 7.Driver Settings field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParsePortInfoTableProc: ERROR: Driver Settings "
			"is NULL!\n", __FILE__);

		return 7;    
	}
	pStructData->szAccessingDriverSet = NEW_strdup(pField);
	if (pStructData->szAccessingDriverSet == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParsePortInfoTableProc: ERROR: no memory to load "
			"Accessing Driver Set info!\n", __FILE__);

		return 7;  
	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : ParseSamplerInfoTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char             *szBuf      : 
 *            OUT SAMPLER_INFO *  pStructData : 
 * RETURN   : int :  loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-14 14:13
 *==========================================================================*/
static int ParseSamplerInfoTableProc(IN char *szBuf, OUT SAMPLER_INFO * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Unit ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Unit ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iSamplerID = atoi(pField);

	/* 2.Sampler English Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Sampler "
			"English Name is NULL!\n", __FILE__);

		return 2;    
	}
	pStructData->pSamplerName = NEW(LANG_TEXT,1);
	if (pStructData->pSamplerName == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: no memory to load "
			"Sampler Name info!\n", __FILE__);

		return 2; 
	}
	ZERO_POBJS(pStructData->pSamplerName, 1);

	/* length info of g_SiteInfo has been initialized already */
	pStructData->pSamplerName->iMaxLenForFull = 
		g_SiteInfo.LangInfo.iMaxFullNameLen;

	pStructData->pSamplerName->iMaxLenForAbbr = 
		g_SiteInfo.LangInfo.iMaxAbbrNameLen;

	pStructData->pSamplerName->pFullName[0] = NEW_strdup(pField);

	if (pStructData->pSamplerName->pFullName[0] == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: no memory to load "
			"Sampler Name info!\n", __FILE__);

		return 2; 
	}

	/* 3.Sampler English Abbr Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Sampler "
			"English Abbr Name is NULL!\n", __FILE__);

		return 3;    
	}
	pStructData->pSamplerName->pAbbrName[0] = NEW_strdup(pField);
	if (pStructData->pSamplerName->pAbbrName[0] == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: no memory to load "
			"Sampler Name info!\n", __FILE__);

		return 3; 
	}

	/* 4.Sampler Locale Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Sampler "
			"Locale Name is NULL!\n", __FILE__);

		return 4;    
	}
	pStructData->pSamplerName->pFullName[1] = NEW_strdup(pField);
	if (pStructData->pSamplerName->pFullName[1] == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: no memory to load "
			"Smapler Name info!\n", __FILE__);

		return 4; 
	}

	/* 5.Sampler Locale Abbr Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Sampler "
			"Locale Abbr Name is NULL!\n", __FILE__);

		return 5;    
	}
	pStructData->pSamplerName->pAbbrName[1] = NEW_strdup(pField);
	if (pStructData->pSamplerName->pAbbrName[1] == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: no memory to load "
			"Sampler Name info!\n", __FILE__);

		return 5; 
	}

	/* 6.Std Sampler ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Std Sampler"
			"ID is not a number!\n", __FILE__);

		return 6;    /* not a num, error */
	}
	pStructData->iStdSamplerID = atoi(pField);

	/* 7.Polling Port ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Polling Port"
			"ID is not a number!\n", __FILE__);

		return 7;    /* not a num, error */
	}
	pStructData->iPollingPortID = atoi(pField);

	/* 8.Sampler Address field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Sampler Address"
			"is not a number!\n", __FILE__);

		return 8;    /* not a num, error */
	}
	pStructData->iSamplerAddr = atoi(pField);

	/* 9.Polling Port Setting field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Polling Port Setting "
			"is NULL!\n", __FILE__);

		return 9;    
	}
	pStructData->szPortSetting = NEW_strdup(pField);
	if (pStructData->szPortSetting == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: no memory to load "
			"Port Setting info!\n", __FILE__);

		return 9; 
	}

	/* 10.Sampling Period field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseSamplerInfoTableProc: ERROR: Sampling Period "
			"is not a number!\n", __FILE__);

		return 10;    /* not a num, error */
	}
	pStructData->iSamplerPeriod = atoi(pField);

	/* 11.Sampling Attribute */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iSamplerAttr = 0;
	}
	else
	{
		if ((*pField != '0') || (*(pField + 1) != 'x' && *(pField + 1) != 'X'))
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseSamplerInfoTableProc: ERROR: Sampling Attribute"
				"is invalid!\n", __FILE__);

			return 11;
		}
		sscanf(pField, "%x", &pStructData->iSamplerAttr);
	}

    //////////////////////////////////////////////////////////////////////////
    //Added by wj for three languages 2006.5.9

    /* 12.Sampler Locale2 Name field */
#ifdef _SUPPORT_THREE_LANGUAGE
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSamplerInfoTableProc: ERROR: Sampler "
            "Locale2 Name is NULL!\n", __FILE__);

        return 4;    
    }
    pStructData->pSamplerName->pFullName[2] = NEW_strdup(pField);
    if (pStructData->pSamplerName->pFullName[2] == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSamplerInfoTableProc: ERROR: no memory to load "
            "Smapler Name info!\n", __FILE__);

        return 4; 
    }

    TRACE("Sampler Locale2 Name field OK!!!\n");

    /* 13.Sampler Locale2 Abbr Name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSamplerInfoTableProc: ERROR: Sampler "
            "Locale2 Abbr Name is NULL!\n", __FILE__);

        return 5;    
    }
    pStructData->pSamplerName->pAbbrName[2] = NEW_strdup(pField);
    if (pStructData->pSamplerName->pAbbrName[2] == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseSamplerInfoTableProc: ERROR: no memory to load "
            "Sampler Name info!\n", __FILE__);

        return 5; 
    }

    TRACE("Sampler Locale2 Abbr Name field OK!!!\n");
#endif
    //end////////////////////////////////////////////////////////////////////////
    

	return 0;
}


/*==========================================================================*
 * FUNCTION : Cfg_DumpExp
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT EXP_ELEMENT     *pDst       : 
 *            IN OUT EXP_ELEMENT  **ppExp     : 
 *            IN int              nExpression : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-04 15:14
 *==========================================================================*/
void Cfg_DumpExp(OUT EXP_ELEMENT *pDst, 
				 IN OUT EXP_ELEMENT **ppExp, IN int nExpression)
{
	int i;
	for (i = 0; i < nExpression; i++)
	{
		pDst[i] = *(ppExp[i]);

		//updated for memory opt
		DELETE_POOL(ppExp[i]);
		//SAFELY_DELETE(ppExp[i]);	// Do NOT release the content--pElement,
		// which is still used by pDst[i];
	}

	//updated for memory opt
	DELETE_POOL(ppExp);
	//SAFELY_DELETE(ppExp);

	return;
}

/* not use now, and not considering err handling */
static BOOL CalStartChn(EQUIP_INFO * pEquipInfo)
{
#define SKIP_OF_STARTCHN_SMBAT  50
#define SKIP_OF_STARTCHN_RECT   23

	RELEVANT_SAMPLER *pSampler;
	int iChnOffset, iUnitOffset;

	/* only use the first relevant sampler info */
	pSampler = pEquipInfo->pRelevantSamplerList;
	iUnitOffset = pSampler->iStartSamplingChannel;

	switch (pEquipInfo->iEquipTypeID)
	{
	case RECT_STD_EQUIP_ID:
		iChnOffset = iUnitOffset * SKIP_OF_STARTCHN_RECT;
		break;

	/*case EQUIP_TYPE_ID_SMBAT4802:
		iChnOffset = iUnitOffset * SKIP_OF_STARTCHN_SMBAT;
		break;

	case EQUIP_TYPE_ID_SMBAT4806:
		iChnOffset = 100 + iUnitOffset * SKIP_OF_STARTCHN_SMBAT;
		break;

	case EQUIP_TYPE_ID_SMBAT4812:
		iChnOffset = 200 + iUnitOffset * SKIP_OF_STARTCHN_SMBAT;
		break;

	case EQUIP_TYPE_ID_SMBAT2402:
		iChnOffset = 300 + iUnitOffset * SKIP_OF_STARTCHN_SMBAT;
		break;

	case EQUIP_TYPE_ID_SMBAT2406:
		iChnOffset = 400 + iUnitOffset * SKIP_OF_STARTCHN_SMBAT;
		break;

	case EQUIP_TYPE_ID_SMBAT2412:
		iChnOffset = 600 + iUnitOffset * SKIP_OF_STARTCHN_SMBAT;
		break;*/

	default:
		iChnOffset = iUnitOffset;
	}

	pSampler->iStartControlChannel = iChnOffset;
	pSampler->iStartSamplingChannel = iChnOffset;

	return TRUE;
}

static BOOL NotLoadByDefault(char *szBuf)
{
#define SAMPLER_FIELD_INDEX 14
#define RS485_SAMPLER_ID	'4'
	int i;
	char szField[32];


	if (g_SiteInfo.bLoadAllEquip)
	{
		return FALSE;
	}

	Cfg_SplitStringIndex(szBuf, szField, sizeof(szField), SPLITTER, SAMPLER_FIELD_INDEX);
	

	//for G3, equipments connected to RS485 port will be not loaded by default
	if (szField[0] == RS485_SAMPLER_ID && 
		(szField[1] < '0' || szField[1] > '9'))  //simple but fast judge
	{
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : ParseEquipInfoTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char          *szBuf      : 
 *            EQUIP_INFO *  pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-15 09:38
 *==========================================================================*/
static int ParseEquipInfoTableProc(char *szBuf, EQUIP_INFO * pStructData)
{
	char *pField, *pSubField, *pSubSubField;
	int  ret;   /* for expression parse */
	int  iSubFieldCount, iCurIndex;

	/* to buffer the solution lang info */
	int  iMaxFullNameLen, iMaxAbbrNameLen;
	char *szText;  /* to buffer text string */
	int  iCodeType = g_SiteInfo.LangInfo.iCodeType;
	int	iReturnChar = 0;
	char xOut[32];

	ASSERT(szBuf);
	ASSERT(pStructData);

	//added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-5
	//not loading some equipments by default, to improve starting time
	if (NotLoadByDefault(szBuf))
	{
		return CFG_RECORD_SKIP;
	}

	
	/* get the buffered value */
	iMaxFullNameLen = g_SiteInfo.LangInfo.iMaxFullNameLen;
	iMaxAbbrNameLen = g_SiteInfo.LangInfo.iMaxAbbrNameLen;


	/* 1.Equip ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: Equip ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iEquipID = atoi(pField);
	//TRACE("EQUIPID is %d\n",pStructData->iEquipID);
	/* 2.Equip English Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: Equip "
			"English Name is NULL!\n", __FILE__);

		return 2;    
	}
	pStructData->pEquipName = NEW(LANG_TEXT,1);
	if (pStructData->pEquipName == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory to load "
			"Equip Name info!\n", __FILE__);

		return 2;    
	}
	ZERO_POBJS(pStructData->pEquipName, 1);

	pStructData->pEquipName->iMaxLenForFull = iMaxFullNameLen;
	pStructData->pEquipName->iMaxLenForAbbr = iMaxAbbrNameLen;

	pStructData->pEquipName->pFullName[0] = NEW(char, iMaxFullNameLen);
	szText = pStructData->pEquipName->pFullName[0];

	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[ParseEquipInfoTable] -- ERROR: no memory to load "
			"Equip Name info!\n", __FILE__);

		return 2;    
	}
	strncpyz(szText, pField, iMaxFullNameLen);

	/* 3.Equip Abbrevated English Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: Equip "
			"Abbr English Name is NULL!\n", __FILE__);

		return 3;    
	}
	pStructData->pEquipName->pAbbrName[0] = NEW(char, iMaxAbbrNameLen);
	szText = pStructData->pEquipName->pAbbrName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory to load "
			"Equip Name info!\n", __FILE__);

		return 3;    
	}
	strncpyz(szText, pField, iMaxAbbrNameLen);

	/* 4.Equip Locale Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: Equip "
			"Locale Name is NULL!\n", __FILE__);

		return 4;    
	}
	pStructData->pEquipName->pFullName[1] = NEW(char, iMaxFullNameLen);
	szText = pStructData->pEquipName->pFullName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory to load "
			"Equip Name info!\n", __FILE__);

		return 4;    
	}
	//printf("bGB2312 = %d\n", bGB2312);
	//if(bGB2312)
	//{
	//	iReturnChar = UTF8toGB2312(pField,64,xOut,32);
	//	if(iReturnChar < 0)
	//	{
	//		strncpyz(szText, pField, iMaxFullNameLen);
	//	}
	//	else
	//	{
	//		strncpyz(szText, xOut, iMaxFullNameLen);
	//	}
	//}
	//else
	//{
		strncpyz(szText, pField, iMaxFullNameLen);
	//}

	/* 5.Equip Abbrevated Locale Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: Equip "
			"Abbr Locale Name is NULL!\n", __FILE__);

		return 5;    
	}
	pStructData->pEquipName->pAbbrName[1] = NEW(char, iMaxAbbrNameLen);
	szText = pStructData->pEquipName->pAbbrName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory to load "
			"Equip Name info!\n", __FILE__);

		return 5;    
	}
	if(iCodeType == ENUM_GB2312)
	{
		iReturnChar = UTF8toGB2312(pField,64,xOut,32);		
		///*if(iReturnChar < 0)
		//{*/
		//	strncpyz(szText, pField, iMaxAbbrNameLen);
		///*}
		//else
		//{
			strncpyz(szText, xOut, iMaxAbbrNameLen);
		//}*/
	}
	else if(iCodeType == ENUM_KOI8R)
	{
		
		iReturnChar = UTF8toKOI8(pField,64,xOut,32);
		strncpyz(szText, xOut, iMaxAbbrNameLen);
		
	}
	else
	{
		strncpyz(szText, pField, iMaxAbbrNameLen);
	}

	/* 6.Equip Type ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: Equip Type ID "
			"is not a number!\n", __FILE__);

		return 6;    /* not a num, error */
	}
	pStructData->iEquipTypeID = atoi(pField);

	/* 7.Factory English Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pStructData->pFactoryName = NEW(LANG_TEXT,1);
	if (pStructData->pFactoryName == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory!\n",
			__FILE__);

		return 7;    
	}
	ZERO_POBJS(pStructData->pFactoryName, 1);

	/* length info of g_SiteInfo has been initialized already */
	pStructData->pFactoryName->iMaxLenForFull = iMaxFullNameLen;
	pStructData->pFactoryName->iMaxLenForAbbr = iMaxAbbrNameLen;
	pStructData->pFactoryName->pFullName[0] = NEW(char, iMaxFullNameLen);
	szText = pStructData->pFactoryName->pFullName[0];

	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory!\n",
			__FILE__);

		return 7;    
	}

	if (Cfg_IsEmptyField(pField))
	{
		szText[0] ='\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: Factory "
				"English Name is NULL!\n", __FILE__);

			return 7;    
		}
		strncpyz(szText, pField, iMaxFullNameLen);	
	}

	/* 8.Factory Abbrevated English Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pStructData->pFactoryName->pAbbrName[0] = NEW(char, iMaxAbbrNameLen);
	szText = pStructData->pFactoryName->pAbbrName[0];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory!\n",
			__FILE__);

		return 8;    
	}
	if (Cfg_IsEmptyField(pField))
	{
		szText[0] = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: Factory "
				"Abbr English Name is NULL!\n", __FILE__);

			return 8;    
		}
		strncpyz(szText, pField, iMaxAbbrNameLen);
	}

	/* 9.Factory Locale Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pStructData->pFactoryName->pFullName[1] = NEW(char, iMaxFullNameLen);
	szText = pStructData->pFactoryName->pFullName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory!\n",
			__FILE__);

		return 9;    
	}

	if (Cfg_IsEmptyField(pField))
	{
		szText[0] = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: Factory "
				"Locale Name is NULL!\n", __FILE__);

			return 9;    
		}
		
			//if(bGB2312)
			//{
			//	iReturnChar = UTF8toGB2312(pField,64,xOut,32);
			//	if(iReturnChar < 0)
			//	{
			//		strncpyz(szText, pField, iMaxFullNameLen);
			//	}
			//	else
			//	{
			//		strncpyz(szText, xOut, iMaxFullNameLen);
			//	}
			//}
			//else
			//{
				strncpyz(szText, pField, iMaxFullNameLen);
			//}
	}

	/* 10.Factory Abbrevated Locale Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pStructData->pFactoryName->pAbbrName[1] = NEW(char, iMaxAbbrNameLen);
	szText = pStructData->pFactoryName->pAbbrName[1];
	if (szText == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseEquipInfoTableProc: ERROR: no memory!\n",
			__FILE__);

		return 10;    
	}

	if (Cfg_IsEmptyField(pField))
	{
		szText[0] = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: Factory "
				"Abbr Locale Name is NULL!\n", __FILE__);

			return 10;    
		}
		if(iCodeType == ENUM_GB2312)
		{
			iReturnChar = UTF8toGB2312(pField,64,xOut,32);
			
			/*if(iReturnChar < 0)
			{
				strncpyz(szText, pField, iMaxAbbrNameLen);
			}
			else
			{*/
				strncpyz(szText, xOut, iMaxAbbrNameLen);
			//}
			
		}
		else if(iCodeType == ENUM_KOI8R)
		{		
			iReturnChar = UTF8toKOI8(pField,64,xOut,32);
			strncpyz(szText, xOut, iMaxAbbrNameLen);
			
		}
		else
		{
			strncpyz(szText, pField, iMaxAbbrNameLen);
		}
	}

	/* 11.Serial Number field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		*pStructData->szSerialNumber = '\0';
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: Serial Number "
				"is NULL!\n", __FILE__);

			return 11;    
		}

		strncpyz(pStructData->szSerialNumber, pField,
			sizeof(pStructData->szSerialNumber));
	}

	/* 12.Default Work State field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iDefaultWorkState = 0;
	}
	else
	{
		if ((*pField != '0') || (*(pField + 1) != 'x' && *(pField + 1) != 'X'))
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: Default Work State"
				" is invalid!\n", __FILE__);

			return 12;
		}
		sscanf(pField, "%x", &pStructData->iDefaultWorkState);
	}

	/* 13.Alarm Filter Expression field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iAlarmFilterExpression = 0;
	}
	else
	{
		//updated for memory opt
		EXP_ELEMENT **ppExp = NEW_POOL(EXP_ELEMENT *, EXP_MAX_ELEMENTS);
		//EXP_ELEMENT **ppExp = NEW(EXP_ELEMENT *, EXP_MAX_ELEMENTS);

		if (ppExp == NULL)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: "
				"no memory!\n", __FILE__);
			return 13;
		}

		ZERO_POBJS(ppExp, EXP_MAX_ELEMENTS);

		ret = ExpressionParser(pField, 
			&pStructData->iAlarmFilterExpression,
			ppExp);
		if (ret == ERR_CFG_BADCONFIG)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: "
				"Alarm Filter Expression is invalid!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 13;    
		}
		else if (ret == ERR_CFG_NO_MEMORY)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: "
				"no memory!\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 13;
		}
		else if (ret == ERR_CFG_FAIL)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: EXP_MAX_ELEMENTS "
				"macro definition is not big enough\n", __FILE__);
			Cfg_FreeTempExpression(EXP_MAX_ELEMENTS, ppExp);
			return 13;
		}

		// parse OK.
		pStructData->pAlarmFilterExpressionCfg = NEW(EXP_ELEMENT,
			pStructData->iAlarmFilterExpression);
		if (pStructData->pAlarmFilterExpressionCfg  == NULL)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: "
				"no memory on copying expression after parsing!\n", __FILE__);

			Cfg_FreeTempExpression(pStructData->iAlarmFilterExpression, ppExp);

			return 13;
		}

		Cfg_DumpExp(pStructData->pAlarmFilterExpressionCfg,
			ppExp,
			pStructData->iAlarmFilterExpression);		
	}

	/* 14.Relevant Sampler List field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pStructData->iRelevantSamplerListLength = 0;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: Relevant Sampler "
				"List is NULL!\n", __FILE__);

			return 14;    
		}
		iSubFieldCount = Cfg_GetSplitStringCount(pField, SUB_SPLITTER);
		pStructData->pRelevantSamplerList = 
			NEW(RELEVANT_SAMPLER, iSubFieldCount);
		if (pStructData->pRelevantSamplerList  == NULL)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: no memory!/n/r",__FILE__);
			return 14;
		}
		memset(pStructData->pRelevantSamplerList, 0, 
			sizeof(RELEVANT_SAMPLER)*iSubFieldCount);
		iCurIndex = 1;
		do
		{
			pField = Cfg_SplitStringEx(pField, &pSubField, SUB_SPLITTER);
			pSubField = Cfg_RemoveBoundChr(pSubField, IsBoundChr_B);

			/* if configured the relevant sampler */
			if (Cfg_IsEmptyField(pSubField))
			{
				/*AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_UNUSED, 
					"[%s]--ParseEquipInfoTableProc: No. %d of the Relevant "
					"Sampler List is not configured\n!", __FILE__, iCurIndex);*/
				pStructData->pRelevantSamplerList[iCurIndex-1].iSamplerID = -1;
			}
			else
			{
				pSubField = Cfg_SplitStringEx(pSubField, 
					&pSubSubField, SUB_SUB_SPLITTER);
				if (*pSubSubField == '\0')
				{
					AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
						"[%s]--ParseEquipInfoTableProc: ERROR: Relevant "
						"Sampler List is invalid\n!", __FILE__);
					return 14;
				}
				sscanf(pSubSubField, "%d", 
					&pStructData->pRelevantSamplerList[iCurIndex-1].iSamplerID);

				pSubField = Cfg_SplitStringEx(pSubField, 
					&pSubSubField, SUB_SUB_SPLITTER);
				if (*pSubSubField != '\0')
				{
					sscanf(pSubSubField, "%d", 
						&pStructData->pRelevantSamplerList[iCurIndex-1].iStartSamplingChannel);
				}

				// treat the Controlling Channal is equal to the Sampling Channel.
				pStructData->pRelevantSamplerList[iCurIndex-1].iStartControlChannel =
					pStructData->pRelevantSamplerList[iCurIndex-1].iStartSamplingChannel;
				//{{Parse the control channel.
				//pSubField = Cfg_SplitStringEx(pSubField, 
				//	&pSubSubField, SUB_SUB_SPLITTER);
				//if (*pSubSubField != '\0')
				//{
				//	sscanf(pSubSubField, "%d", 
				//		&pStructData->pRelevantSamplerList[iCurIndex-1].iStartControlChannel);
				//}
				//}}
			}
		}
		while (iCurIndex++ < iSubFieldCount);
		pStructData->iRelevantSamplerListLength = --iCurIndex;		
	}

	/* 15.Relevant Equip List field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		
		//pStructData->iRelevantEquipListLength = 0;

		/* always set to 1 for the requirement from equip_mon_init module */
		pStructData->iRelevantEquipListLength = 1;
		pStructData->pRelevantEquipList = NEW(int, 1);
		pStructData->pRelevantEquipList[0] = -1;
	}
	else
	{
		if (*pField == '\0')
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: Relevant Equip List "
				"is NULL!\n", __FILE__);

			return 15;    
		}
		iSubFieldCount = Cfg_GetSplitStringCount(pField, SUB_SPLITTER);
		pStructData->pRelevantEquipList = 
			NEW(int, iSubFieldCount);
		if (pStructData->pRelevantEquipList == NULL)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseEquipInfoTableProc: ERROR: no memory!/n/r",__FILE__);
			return 15;
		}
		pStructData->iRelevantEquipListLength = iSubFieldCount;
		memset(pStructData->pRelevantEquipList, 0, 
			sizeof(int)*iSubFieldCount);
		iCurIndex = 1;
		do
		{
			pField = Cfg_SplitStringEx(pField, &pSubField, SUB_SPLITTER);

			/* if configured the relevant sampler */
			if (Cfg_IsEmptyField(pSubField))
			{
				/*AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_UNUSED, 
					"[%s]--ParseEquipInfoTableProc: No. %d of the Relevant "
					"Equip List is not configured\n!", __FILE__, iCurIndex);*/
				pStructData->pRelevantEquipList[iCurIndex-1] = -1;
			}
			else
			{
				if (*pSubField == '\0')
				{
					AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
						"[%s]--ParseEquipInfoTableProc: ERROR: Relevant "
						"Sampler List is invalid\n!", __FILE__);
					return 15;
				} 
				sscanf(pSubField,"%d",&pStructData->pRelevantEquipList[iCurIndex-1]);
			}
		}
		while (iCurIndex++ < iSubFieldCount);
	}

    //////////////////////////////////////////////////////////////////////////
    //Added by wj for three languages 2006.5.9
#ifdef _SUPPORT_THREE_LANGUAGE
    /* 16.Equip Locale2 Name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseEquipInfoTableProc: ERROR: Equip "
            "Locale2 Name is NULL!\n", __FILE__);

        return 4;    
    }
    pStructData->pEquipName->pFullName[2] = NEW(char, iMaxFullNameLen);
    szText = pStructData->pEquipName->pFullName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseEquipInfoTableProc: ERROR: no memory to load "
            "Equip Name info!\n", __FILE__);

        return 4;    
    }
    strncpyz(szText, pField, iMaxFullNameLen);

    //TRACE("Equip Locale2 Name field OK!!!\n");

    /* 17.Equip Abbrevated Locale2 Name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (*pField == '\0')
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseEquipInfoTableProc: ERROR: Equip "
            "Abbr Locale2 Name is NULL!\n", __FILE__);

        return 5;    
    }
    pStructData->pEquipName->pAbbrName[2] = NEW(char, iMaxAbbrNameLen);
    szText = pStructData->pEquipName->pAbbrName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseEquipInfoTableProc: ERROR: no memory to load "
            "Equip Name info!\n", __FILE__);

        return 5;    
    }
    strncpyz(szText, pField, iMaxAbbrNameLen);

    //TRACE("Equip Abbrevated Locale2 Name field OK!!!\n");

    /* 18.Factory Locale2 Name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    pStructData->pFactoryName->pFullName[2] = NEW(char, iMaxFullNameLen);
    szText = pStructData->pFactoryName->pFullName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseEquipInfoTableProc: ERROR: no memory!\n",
            __FILE__);

        return 9;    
    }

    if (Cfg_IsEmptyField(pField))
    {
        szText[0] = '\0';
    }
    else
    {
        if (*pField == '\0')
        {
            AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
                "[%s]--ParseEquipInfoTableProc: ERROR: Factory "
                "Locale2 Name is NULL!\n", __FILE__);

            return 9;    
        }
        strncpyz(szText, pField, iMaxFullNameLen);
    }

    //TRACE("Factory Locale2 Name field OK!!!\n");

    /* 10.Factory Abbreviated Locale2 Name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    pStructData->pFactoryName->pAbbrName[2] = NEW(char, iMaxAbbrNameLen);
    szText = pStructData->pFactoryName->pAbbrName[2];
    if (szText == NULL)
    {
        AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
            "[%s]--ParseEquipInfoTableProc: ERROR: no memory!\n",
            __FILE__);

        return 10;    
    }

    if (Cfg_IsEmptyField(pField))
    {
        szText[0] = '\0';
    }
    else
    {
        if (*pField == '\0')
        {
            AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
                "[%s]--ParseEquipInfoTableProc: ERROR: Factory "
                "Abbr Locale2 Name is NULL!\n", __FILE__);

            return 10;    
        }
        strncpyz(szText, pField, iMaxAbbrNameLen);
    }
#endif
    //TRACE("Factory Abbreviated Locale2 Name field OK!!!\n");
    //end////////////////////////////////////////////////////////////////////////
    

	return 0;
}

/*==========================================================================*
 * FUNCTION : ParseCustomChannelTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char              *szBuf      : 
 *            CUSTOM_CHANNEL *  pStructData : 
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-16 11:07
 *==========================================================================*/
static int ParseCustomChannelTableProc(IN char *szBuf, OUT CUSTOM_CHANNEL *pStructData)
{
	char *pField;

	/* for Sampling channel and Control channel, only one can be empty */
	BOOL bEmptyUsed = FALSE; 

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.ID field, just jump it */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 2.Equip ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseCustomChannelTableProc: ERROR: Equip ID is not "
			"a number!\n", __FILE__);
		return 2;    /* not a num, error */
	}
	pStructData->iEqipID = atoi(pField);

	/* 3.Sig Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '3') || (*(pField+1) != '\0'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseCustomChannelTableProc: ERROR: "
			"Sig Type is invalid!\n", __FILE__);
		return 3;    
	}
	pStructData->iSigType = atoi(pField);

	/* 4.Sig ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--ParseCustomChannelTableProc: ERROR: "
			"Sig ID is not a number!\n", __FILE__);
		return 4;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 5.Sampler ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		/* means not configured */
		pStructData->iSamplerID = -1;

		/* not need read the left fields */
		return 0;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseCustomChannelTableProc: ERROR: "
				"Sampler ID is not a number!\n", __FILE__);
			return 5;    /* not a num, error */
		}
		pStructData->iSamplerID = atoi(pField);
	}

	/* 6.Sampling channel number field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		bEmptyUsed = TRUE;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseCustomChannelTableProc: ERROR: Sampling "
				"channel number is not a number!\n", __FILE__);
			return 6;    /* not a num, error */
		}
		pStructData->iSamplerChannel = atoi(pField);
	}

	/* 7.control channel number field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		if (bEmptyUsed)
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseCustomChannelTableProc: ERROR: Sampling "
				"channel and Control channel are both empty!\n", __FILE__);
			return 7;
		}
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
				"[%s]--ParseCustomChannelTableProc: ERROR: control "
				"channel number is not a number!\n", __FILE__);
			return 7;    /* not a num, error */
		}
		pStructData->iControlChannel = atoi(pField);
	}

	return 0;
}


/*==========================================================================*
 * FUNCTION : CustomizeSpecialSignals
 * PURPOSE  : customize channel for standard config file backup compatibitity
 *			  Note, try to use the new config file first, consider this solution 
 *			  at last	
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   : BOOL : 
 * COMMENTS : added for G3_OPT [config]
 * CREATOR  : LinTao                   DATE: 2013-06-05 11:19
 *==========================================================================*/
static BOOL CustomizeSpecialSignals(int nAddedChn, CUSTOM_CHANNEL *pAddedChn)
{
	int i;
	CUSTOM_CHANNEL *pOrigionChn;

	if (nAddedChn == 0 || pAddedChn == NULL)
	{
		return TRUE;
	}

	g_SiteInfo.pCustomChannel = RENEW(CUSTOM_CHANNEL,
		g_SiteInfo.pCustomChannel,
		g_SiteInfo.iCustomChannelNum + nAddedChn);

	if (g_SiteInfo.pCustomChannel == NULL)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"Out of memory on customizing special signals\n");

		return FALSE;
	}

	//overwrite the customized channel info
	pOrigionChn = g_SiteInfo.pCustomChannel + g_SiteInfo.iCustomChannelNum;
	for (i = 0; i < nAddedChn; i++)
	{
		memcpy(pOrigionChn++, pAddedChn++, sizeof(CUSTOM_CHANNEL));
	}
	g_SiteInfo.iCustomChannelNum += nAddedChn;

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : LoadSolutionConfigProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN void        *pCfg       : 
 *            OUT SITE_INFO  *pLoadToBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-16 11:19
 *==========================================================================*/
static int LoadSolutionConfigProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	SITE_INFO *pBuf;
	char szLineData[MAX_LINE_SIZE];
	int  ret;
	CONFIG_TABLE_LOADER loader[4];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (SITE_INFO *)pLoadToBuf;

	//added for G3_OPT [loader], by Thomas, 2013-5
	char szFullPath[MAX_FILE_PATH];
	Cfg_GetFullConfigPath(LOADALLEQUIP_FLAGFILE, szFullPath, MAX_FILE_PATH);
	//printf("access(szFullPath, F_OK) = %d\n",access(szFullPath, F_OK));
	if (!(access(szFullPath, F_OK) == 0)) //find the flag file
	{
		g_SiteInfo.bLoadAllEquip = TRUE;

		//to delete
		//printf("\nGot the flag file\n");
	}
	/* read solution config file information section*/
	/* Add it to be displayed in lcd and web---caihao 2005-12-27*/

#ifdef CR_CFGFILE_INFO

	memset(szLineData,0,sizeof(char)*MAX_LINE_SIZE);
	ret = Cfg_ProfileGetLine(pCfg,CONFIG_INFOR,szLineData,MAX_LINE_SIZE);
	if(ret == -1)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--LoadSolutionConfigProc: CONFIG INFORMATION SECTION not"
			" found in the Solution Cfg file!\n",__FILE__);
		return	ERR_CFG_BADCONFIG;
	}
	else if(ret ==0)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--LoadSolutionConfigProc:ERROR:CONFIG INFORMATION SECTION"
			"should not be empty!\n",__FILE__);
		return ERR_CFG_BADCONFIG;
	}
	ret = ParseConfigInfoTableProc(szLineData,pBuf);
	if(ret !=0)
	{
		AppLogOut(CFG_LOAD_SOLUTION,APP_LOG_ERROR,
			"[%s]--LoadSolutionConfigProc:ERROR:Get Config info of "
			"solution config file failed!\n\r",__FILE__);
		return ERR_CFG_BADCONFIG;
	}
#endif

	//1. read language info section /
	/*RunThread_Heartbeat(RunThread_GetId(NULL));*/
	memset(szLineData, 0, sizeof(char)*MAX_LINE_SIZE);
	ret = Cfg_ProfileGetLine(pCfg, LANGUAGE_INFO, szLineData, MAX_LINE_SIZE);
	if (ret == -1)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--LoadSolutionConfigProc: ERROR: LANGUAGE INFO SECTION not "
			"found in the Solution Cfg file!\n", __FILE__);
		return ERR_CFG_BADCONFIG;
	}
	else if (ret == 0)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--LoadSolutionConfigProc: ERROR: LANGUAGE INFO SECTION "
			"should not be empty!\n", __FILE__);
		return ERR_CFG_BADCONFIG;
	}
	ret = ParseLangInfoTableProc(szLineData, &pBuf->LangInfo);
	if (ret != 0)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--LoadSolutionConfigProc: ERROR: Get language info of"
			"solution config file failed!/n/r", __FILE__);
		return ERR_CFG_BADCONFIG;
	}

	//2.read base site info section */
	/*RunThread_Heartbeat(RunThread_GetId(NULL));*/
	memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
	ret = Cfg_ProfileGetLine(pCfg, SITE_BASE_INFO, szLineData, MAX_LINE_SIZE);
	if (ret == -1)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--LoadSolutionConfigProc: ERROR: Site Info Section not "
			"found in the Solution Cfg file!\n", __FILE__);
		return ERR_CFG_BADCONFIG;
	}
	else if (ret == 0)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--LoadSolutionConfigProc: ERROR: Site Info Section "
			"should not be empty!\n", __FILE__);
		return ERR_CFG_BADCONFIG;
	}
	ret = ParseSiteInfoTableProc(szLineData, pBuf);
	if (ret != 0)
	{
		AppLogOut(CFG_LOAD_SOLUTION, APP_LOG_ERROR, 
			"[%s]--LoadSolutionConfigProc: ERROR: Get base site info of "
			"teh solution config file failed!/n/r", __FILE__);
		return ERR_CFG_BADCONFIG;
	}


	//3.Read tables: Port infomaiton,Sampler Unit Info, Equip Info,Custom Signal Info/
	DEF_LOADER_ITEM(&loader[0], 
		NULL, &(pBuf->iPortNum), 
		COMM_PORT_INFO,&(pBuf->pPortInfo), 
		ParsePortInfoTableProc);

	DEF_LOADER_ITEM(&loader[1], 
		NULL, &(pBuf->iSamplerNum), 
		SAMPLER_UNIT_INFO, &(pBuf->pSamplerInfo), 
		ParseSamplerInfoTableProc);

	DEF_LOADER_ITEM(&loader[2], 
		NULL, &(pBuf->iEquipNum), 
		EQUIPMENT_INFO, &(pBuf->pEquipInfo), 
		ParseEquipInfoTableProc);

	DEF_LOADER_ITEM(&loader[3], 
		NULL, &(pBuf->iCustomChannelNum), 
		EQUIP_CUSTOM_CHANNEL_INFO, &(pBuf->pCustomChannel), 
		ParseCustomChannelTableProc);

	if (Cfg_LoadTables(pCfg,4,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	//customize channel for standard config file backup compatibitity.
	//added for G3_OPT [config], by Lin.Tao.Thomas, 2013-6
	if (!g_SiteInfo.bLoadAllEquip)
	{
#define SYSTEM_EQUIP_ID				1
#define RS485_COMMFAIL_SIG_ID		56

#define FUELTANK_QROUP_EQUIP_ID		700
#define COMM_COMMFAIL_SIG_ID		99
#define WORKSTATUS_SIG_ID			100

		CUSTOM_CHANNEL stSpecialSigCutomChn[] = {
			{ SYSTEM_EQUIP_ID, SIG_TYPE_SAMPLING, RS485_COMMFAIL_SIG_ID, -1, -3, 0 }, 
			{ FUELTANK_QROUP_EQUIP_ID, SIG_TYPE_SAMPLING, COMM_COMMFAIL_SIG_ID, 2, 2, 0 },
			{ FUELTANK_QROUP_EQUIP_ID, SIG_TYPE_SAMPLING, WORKSTATUS_SIG_ID, 2, 1, 0 },
		};

		if (!CustomizeSpecialSignals(ITEM_OF(stSpecialSigCutomChn), stSpecialSigCutomChn))		
		{
			return ERR_CFG_FAIL;
		}
	}

	return ERR_CFG_OK;	
}


/*==========================================================================*
 * FUNCTION : Cfg_LoadSolutionConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *szConfigFile : solution file name
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-9-16 12:07
 *==========================================================================*/
int Cfg_LoadSolutionConfig(char *szConfigFile)
{
	int ret;
	ret = Cfg_LoadConfigFile(szConfigFile, LoadSolutionConfigProc, &g_SiteInfo);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

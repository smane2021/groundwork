/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cfg_loadrun.c
 *  CREATOR  : LinTao                   DATE: 2004-09-22 09:42
 *  VERSION  : V1.00
 *  PURPOSE  : to load runtime changed config info
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"


#include "cfg_mgmt.h"
#include "cfg_helper.h"


#define SPLITTER_LINE		 "\r\n"


/* section for RUN_CFGFILE */
#define CHANGED_INFO				"[CHANGED_INFO]"



struct tagRunConfigInfo
{
	int					iChangedInfoNum;
	RUN_CONFIG_ITEM		*pChangedInfo;
};
typedef struct tagRunConfigInfo RUN_CONFIG_INFO;

//typedef struct tagProfile SProfile;
int Cfg_ProfileReadLine(IN SProfile *pProf, OUT char *pszBuf, IN int nBufSize);

extern HANDLE GetMutexLoadRunConfig(void);

#define MAX_TIME_WAITING_LOAD_RUN_CONFIG 1000


/*==========================================================================*
 * FUNCTION : ParseChangedInfoTableProc
 * PURPOSE  : parsing [CHANGED_INFO] table of MainConfig.run file
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char              *szBuf       : line data
 *            OUT RUN_CONFIG_ITEM  *pStructData : to store parsed data
 * RETURN   : int : loaded failed field ID, zero for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-22 09:49
 *==========================================================================*/
static int ParseChangedInfoTableProc(IN char *szBuf, OUT RUN_CONFIG_ITEM *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Time filed, just jump it */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 2.Changed info type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: Changed info type "
			"is NULL!\n", __FILE__);
		return 2;
	}

	/* if *pField != '\0', pFiled + 1 will not be NULL */
	switch(*pField)
	{
	case 'S':
		/* ST: Site Name */
		if (*(pField + 1) == 'T')
		{
			pStructData->dwChangeInfoType = CONFIG_CHANGED_SITENAME;
		}
		/* SL: Site location */
		else if (*(pField + 1) == 'L')
		{
			pStructData->dwChangeInfoType = CONFIG_CHANGED_SITELOCATION;
		}
		/* SD: Site description */
		else if (*(pField + 1) == 'D')
		{
			pStructData->dwChangeInfoType = CONFIG_CHANGED_SITEDESCRIPTION;
		}
		/* SG: Sig Name */
		else if (*(pField + 1) == 'G')
		{
			pStructData->dwChangeInfoType = CONFIG_CHANGED_SIGNAME;
		}
		else
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--ParseChangedInfoTable: ERROR: Changed info type "
				"is invalid!\n", __FILE__);
			return 2;
		}
		break;

	case 'E':
		/* EP: Equip Name */
		if (*(pField + 1) == 'P')
		{
			pStructData->dwChangeInfoType = CONFIG_CHANGED_EQUIPNAME;
		}
		else
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--ParseChangedInfoTable: ERROR: Changed info type "
				"is invalid!\n", __FILE__);
			return 2;
		}
		break;

	case 'A':
		/* AL: alarm level */
		if (*(pField + 1) == 'L')
		{
			pStructData->dwChangeInfoType = CONFIG_CHANGED_ALARMLEVEL;
		}
		else if(*(pField + 1) == 'R')//added by ht,2006.11.2
		{
			pStructData->dwChangeInfoType = CONFIG_CHANGED_ALARMRELAY;
		}
		else
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--ParseChangedInfoTable: ERROR: Changed info type "
				"is invalid!\n", __FILE__);
			return 2;
		}
		break;

	default:
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: Changed info type "
			"is invalid!\n", __FILE__);
		return 2;
	}

	/* 3.Text type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: Text Language type "
			"is NULL!\n", __FILE__);
		return 3;
	}

	switch(*pField)
	{
	case '-':
		break;

	case 'E':
		/* EF: english full name */
		if (*(pField + 1) == 'F')
		{
			pStructData->iTextType = ENGLISH_FULL;
		}
		/* EA: English Abbrevated Name */
		else if (*(pField + 1) == 'A')
		{
			pStructData->iTextType = ENGLISH_ABBR;
		}
		else
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--ParseChangedInfoTable: ERROR: Text type is "
				"invalide!\n", __FILE__);
			return 3;
		}
		break;

	case 'L':
		/* LF: Locale full name */
		if (*(pField + 1) == 'F')
		{
			pStructData->iTextType = LOCALE_FULL;
		}
		/* LA: Locale Abbrevated Name */
		else if (*(pField + 1) == 'A')
		{
			pStructData->iTextType = LOCALE_ABBR;
		}
        //////////////////////////////////////////////////////////////////////////
        //Added by wj for three languages 2006.5.11
        else if (*(pField + 1) == '2')
        {
            if(*(pField + 2) == 'F')
            {
                pStructData->iTextType = LOCALE2_FULL;
            }
            else if(*(pField + 2) == 'A')
            {
                pStructData->iTextType = LOCALE2_ABBR;
            }
            
        }

        //end////////////////////////////////////////////////////////////////////////
        
		else
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--ParseChangedInfoTable: ERROR: Text type is "
				"invalide!\n", __FILE__);
			return 3;
		}
		break;

	default:
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: Text type is NULL!\n",
			__FILE__);
		return 3;
	}

	/* 4.New text field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: New Text field "
			"is NULL!\n", __FILE__);
		return 4;    
	}
	pStructData->szText = NEW_strdup(pField);
	if (pStructData->szText == NULL)
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: not enough memory!\n",
			__FILE__);
		return 4;    
	}

	/* for site name(ST), SL(site location), SD(site description), only hase 4 field */
	if ((pStructData->dwChangeInfoType & CONFIG_CHANGED_SITENAME)
		|| (pStructData->dwChangeInfoType & CONFIG_CHANGED_SITELOCATION)
		|| (pStructData->dwChangeInfoType & CONFIG_CHANGED_SITEDESCRIPTION))
	{
		return 0;
	}

	/* 5.EquipID(for EP)/Equip Type ID(for SG,AL) field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: EquipID(for EP)/"
			"Equip Type ID(for SG,AL) is not a number!\n", __FILE__);
		return 5;    /* not a num, error */
	}

	/* for eqip name(EP), only has 5 field, so now should return */
	if (pStructData->dwChangeInfoType & CONFIG_CHANGED_EQUIPNAME)
	{
		pStructData->iEquipID = atoi(pField);
		return 0;
	}

	/* now is sig name(SG) or alarm level(AL) */	
	pStructData->iEquipTypeID = atoi(pField);


	/* 6.Sig type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: Sig type is NULL!\n",
			__FILE__);
		return 6;
	}

	/* if *pField != '\0', pFiled + 1 will not be NULL */
	switch(*pField)
	{
	case 'S':
		/* SP: Sampling sig type */
		if (*(pField + 1) == 'P')
		{
			pStructData->iSigType = SIG_TYPE_SAMPLING;
		}
		/* ST: Setting sig type */
		else if (*(pField + 1) == 'T')
		{
			pStructData->iSigType = SIG_TYPE_SETTING;
		}
		else
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--ParseChangedInfoTable: ERROR: Sig type "
				"is invalid!\n", __FILE__);
			return 6;
		}
		break;

	case 'C':
		/* C: Control sig type */
		pStructData->iSigType = SIG_TYPE_CONTROL;
		break;

	case 'A':
		/* AL: alarm sig type */
		pStructData->iSigType = SIG_TYPE_ALARM;
		break;

	default:
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: Sig type "
			"is invalid!\n", __FILE__);
		return 6;
	}

	/* 7.Sig ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--ParseChangedInfoTable: ERROR: Sig ID is not a number!\n",
			__FILE__);
		return 7;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	return 0;
}

/*==========================================================================*
 * FUNCTION : LoadRunConfigProc
 * PURPOSE  : call back function
 * CALLS    : 
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: IN void              *pCfg       : 
 *            OUT RUN_CONFIG_INFO  *pLoadToBuf : 
 * RETURN   : int : err code defined in the err_code.h file
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-22 10:03
 *==========================================================================*/
static int LoadRunConfigProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	RUN_CONFIG_INFO *pBuf;
	CONFIG_TABLE_LOADER loader;

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);    

	pBuf = (RUN_CONFIG_INFO *)pLoadToBuf;

	DEF_LOADER_ITEM(&loader, 
		NULL, &(pBuf->iChangedInfoNum), 
		CHANGED_INFO,&(pBuf->pChangedInfo), 
		ParseChangedInfoTableProc);

	if (Mutex_Lock(GetMutexLoadRunConfig(), MAX_TIME_WAITING_LOAD_RUN_CONFIG)
		== ERR_MUTEX_OK)
	{
		if (Cfg_LoadTables(pCfg,1,&loader) != ERR_CFG_OK)
		{
			Mutex_Unlock(GetMutexLoadRunConfig());
			return ERR_CFG_FAIL;
		}

		Mutex_Unlock(GetMutexLoadRunConfig());
	}
	else
	{
		return ERR_CFG_RUNINFO_MUTEX_TIMEOUT;
	}

	return ERR_CFG_OK;	
}

/*==========================================================================*
 * FUNCTION : GetCfgSig
 * PURPOSE  : assistant function, get Sig structure according ID
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iEquipTypeID : 
 *            int  iSigType     : 
 *            int  iSigID       : 
 * RETURN   : void * : NULL for failure, if succeeded, return 
 *				SAMPLE_SIG_INFO *, CTRL_SIG_INFO *, SET_SIG_INFO *
 *              or ALARM_SIG_INFO * according iSigType
 *                  
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-22 10:07
 *==========================================================================*/
void * GetCfgSig(IN int iEquipTypeID, IN int iSigType, IN int iSigID)
{
	int i;
	BOOL bFound;
	STDEQUIP_TYPE_INFO *pStdEquip;
	SAMPLE_SIG_INFO *pSampleSig;
	CTRL_SIG_INFO *pCtrlSig;
	SET_SIG_INFO *pSetSig;
	ALARM_SIG_INFO *pAlarmSig;

	bFound = FALSE;
	pStdEquip = g_SiteInfo.pStdEquipTypeInfo;
	for (i = 0; i < g_SiteInfo.iStdEquipTypeNum; i++, pStdEquip++)
	{
		if (pStdEquip->iTypeID == iEquipTypeID)
		{
			bFound = TRUE;
			break;
		}
	}

	/* not find the relevant stdEquip */
	if (!bFound)
	{
		return NULL;
	}

	switch(iSigType)
	{
	case SIG_TYPE_SAMPLING:
		for (i = 0; i < pStdEquip->iSampleSigNum; i++)
		{
			pSampleSig = pStdEquip->pSampleSigInfo + i;

			if (pSampleSig->iSigID == iSigID)
			{
				return pSampleSig;
			}
		}
		break;

	case SIG_TYPE_CONTROL:
		for (i = 0; i < pStdEquip->iCtrlSigNum; i++)
		{
			pCtrlSig = pStdEquip->pCtrlSigInfo + i;

			if (pCtrlSig->iSigID == iSigID)
			{
				return pCtrlSig;
			}
		}
		break;

	case SIG_TYPE_SETTING:
		for (i = 0; i < pStdEquip->iSetSigNum; i++)
		{
			pSetSig = pStdEquip->pSetSigInfo + i;

			if (pSetSig->iSigID == iSigID)
			{
				return pSetSig;
			}
		}
		break;

	case SIG_TYPE_ALARM:
		for (i = 0; i < pStdEquip->iAlarmSigNum; i++)
		{
			pAlarmSig = pStdEquip->pAlarmSigInfo + i;

			if (pAlarmSig->iSigID == iSigID)
			{
				return pAlarmSig;
			}
		}
		break;

	default:
		return NULL;
	}

	return NULL;
}

/*==========================================================================*
 * FUNCTION : GetEquip
 * PURPOSE  : asssitant function, for getting equip by equipID
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  iEquipID : 
 * RETURN   : EQUIP_INFO * : NULL for failure
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-22 10:23
 *==========================================================================*/
EQUIP_INFO * GetEquip(IN int iEquipID)
{
	int i;
	EQUIP_INFO *pEquip;

	pEquip = g_SiteInfo.pEquipInfo;
	for (i = 0; i < g_SiteInfo.iEquipNum; i++, pEquip++)
	{
		if (pEquip->iEquipID == iEquipID)
		{
			return pEquip;
		}
	}

	return NULL;
}

/*==========================================================================*
 * FUNCTION : ChangeTextName
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT LANG_TEXT  *pText    : 
 *            IN int            iTextType : 
 *            IN char *         szNewText : 
 * RETURN   : int : return ERR_CFG_OK, ERR_CFG_NO_MEMORY or ERR_CFG_BADRUNINFO
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-22 10:31
 *==========================================================================*/
static int ChangeTextName(IN OUT LANG_TEXT *pText, IN int iTextType, 
						  IN const char *szNewText)
{
	char *pOldName;
	int  iNameLen;

	ASSERT(pText);
	ASSERT(szNewText);

	switch(iTextType)
	{
	case ENGLISH_FULL:
		pOldName = pText->pFullName[0];
		iNameLen = pText->iMaxLenForFull;
		break;

	case ENGLISH_ABBR:
		pOldName = pText->pAbbrName[0];
		iNameLen = pText->iMaxLenForAbbr;
		break;

	case LOCALE_FULL:
		pOldName = pText->pFullName[1];
		iNameLen = pText->iMaxLenForFull;
		break;

	case LOCALE_ABBR:
		pOldName = pText->pAbbrName[1];
		iNameLen = pText->iMaxLenForAbbr;
		break;

//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.5.9
    case LOCALE2_FULL:
        pOldName = pText->pFullName[2];
        iNameLen = pText->iMaxLenForFull;
        break;

    case LOCALE2_ABBR:
        pOldName = pText->pAbbrName[2];
        iNameLen = pText->iMaxLenForAbbr;
        break;

//end////////////////////////////////////////////////////////////////////////


	default:
		pOldName = NULL;
	}

	if (pOldName != NULL)
	{
		strncpyz(pOldName, szNewText, iNameLen+1);
		//Modified by Jimmy :EMEAҪ����������ʾ��#��,�������"!"���洢'#'��Ȼ��ת��һ������ʾ.EEM �����WP�����'#'ת����'!'.
		//---------------begin modifying
		int ii;
		for(ii = 0; ii < iNameLen; ii++)
		{
			if(*(pOldName+ii) == '!')
			{
				*(pOldName+ii)  = '#';
			}
		}
		//---------------end modifying
		return ERR_CFG_OK;
	}

	return ERR_CFG_BADRUNINFO;
}

/*==========================================================================*
 * FUNCTION : ChangeAlarmLevel
 * PURPOSE  : change alarm level, check runtime changed info at the same time
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT ALARM_SIG_INFO  *pAlarmSig       : 
 *            IN const char       *szNewAlarmLevel : 
 * RETURN   : int : err code 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-22 11:49
 *==========================================================================*/
static int ChangeAlarmLevel(OUT ALARM_SIG_INFO *pAlarmSig, 
							IN const char *szNewAlarmLevel)
{
	size_t iLen;

	ASSERT(pAlarmSig);
	ASSERT(szNewAlarmLevel);

	iLen = strlen(szNewAlarmLevel);

	/* Valid Alarm Level string should be: NA OA MA CA */
	if (iLen != 2)
	{
		return ERR_CFG_BADRUNINFO;
	}

	if (*(szNewAlarmLevel + 1) != 'A')   
	{
		return ERR_CFG_BADRUNINFO;   
	}

	switch (*szNewAlarmLevel)
	{
	case 'N':			/* NA */
		pAlarmSig->iAlarmLevel = ALARM_LEVEL_NONE;
		break;

	case 'O':			/* OA */
		pAlarmSig->iAlarmLevel = ALARM_LEVEL_OBSERVATION;
		break;

	case 'M':			/* MA */
		pAlarmSig->iAlarmLevel = ALARM_LEVEL_MAJOR;
		break;

	case 'C':			/* CA */
		pAlarmSig->iAlarmLevel = ALARM_LEVEL_CRITICAL;
		break;

	default:
		return ERR_CFG_BADRUNINFO;
	}

	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : ChangeAlarmRelay
 * PURPOSE  : change alarm relay, check runtime changed info at the same time
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT ALARM_SIG_INFO  *pAlarmSig       : 
 *            IN const char       *szNewAlarmRelay : 
 * RETURN   : int : err code 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-03 11:49
 *==========================================================================*/
static int ChangeAlarmRelay(OUT ALARM_SIG_INFO *pAlarmSig, 
							IN const char *szNewAlarmRelay)
{
	size_t iLen;

	ASSERT(pAlarmSig);
	ASSERT(szNewAlarmRelay);

	iLen = strlen(szNewAlarmRelay);

	/*
	// Valid Alarm Level string should be: NA OA MA CA
	if (iLen != 2)
	{
		return ERR_CFG_BADRUNINFO;
	}

	if (*(szNewAlarmLevel + 1) != 'A')   
	{
		return ERR_CFG_BADRUNINFO;   
	}
  */
	if (*(szNewAlarmRelay) == 'N')
	{
		if(iLen != 2 || *(szNewAlarmRelay + 1) != 'A')
		  return ERR_CFG_BADRUNINFO;
	}
	else
	{
		if(iLen > 2)
			return ERR_CFG_BADRUNINFO;
	}


	switch (*szNewAlarmRelay)
	{
	case 'N':			/* NA */
		pAlarmSig->iAlarmRelayNo = ALARM_RELAY_NONE;
		break;

	case '1':
		/* Relay 1 */
		if(iLen == 1)
		{
			pAlarmSig->iAlarmRelayNo = ALARM_RELAY_FIRST;
		}
		else
		{
			switch (*(szNewAlarmRelay+1))
			{
			case '0':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_10TH;
				break;
			case '1':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_11TH;
				break;
			case '2':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_12TH;
				break;
			case '3':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_13TH;
				break;
			case '4':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_14TH;
				break;
			case '5':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_15TH;
				break;
			case '6':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_16TH;
				break;
			case '7':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_17TH;
				break;
			case '8':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_18TH;
				break;
			case '9':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_19TH;
				break;			
			default:
				return ERR_CFG_BADRUNINFO;
			}

		}
			
		break;

	case '2':			/* Relay 2 */
		//pAlarmSig->iAlarmRelayNo = ALARM_RELAY_SECOND;
		/* Relay 1 */
		if(iLen == 1)
		{
			pAlarmSig->iAlarmRelayNo = ALARM_RELAY_SECOND;
		}
		else
		{
			switch (*(szNewAlarmRelay+1))
			{
			case '0':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_20TH;
				break;
			case '1':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_21TH;
				break;
			case '2':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_22TH;
				break;
			case '3':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_23TH;
				break;
			case '4':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_24TH;
				break;
			case '5':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_25TH;
				break;
			case '6':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_26TH;
				break;
			case '7':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_27TH;
				break;
			case '8':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_28TH;
				break;
			case '9':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_29TH;
				break;
			
			default:
				return ERR_CFG_BADRUNINFO;
			}

		}
		break;

	case '3':			/* Relay 3 */
		//pAlarmSig->iAlarmRelayNo = ALARM_RELAY_THIRD;
		if(iLen == 1)
		{
			pAlarmSig->iAlarmRelayNo = ALARM_RELAY_THIRD;
		}
		else
		{
			switch (*(szNewAlarmRelay+1))
			{
			case '0':
				pAlarmSig->iAlarmRelayNo = ALARM_RELAY_30TH;
				break;
			default:
				return ERR_CFG_BADRUNINFO;
			}

		}		
		break;

	case '4':			/* Relay 4 */
		pAlarmSig->iAlarmRelayNo = ALARM_RELAY_FOURTH;
		break;

	case '5':			/* Relay 5 */
		pAlarmSig->iAlarmRelayNo = ALARM_RELAY_FIFTH;
		break;

	case '6':			/* Relay 6 */
		pAlarmSig->iAlarmRelayNo = ALARM_RELAY_SIXTH;
		break;

	case '7':			/* Relay 7 */
		pAlarmSig->iAlarmRelayNo = ALARM_RELAY_SEVENTH;
		break;

	case '8':			/* Relay 8 */
		pAlarmSig->iAlarmRelayNo = ALARM_RELAY_EIGHTH;
		break;

	case '9':			/* Relay 9 */
		pAlarmSig->iAlarmRelayNo = ALARM_RELAY_9TH;
		break;


	default:
		return ERR_CFG_BADRUNINFO;
	}

	return ERR_CFG_OK;
}


__INLINE static void FreeRunConfigInfo(RUN_CONFIG_INFO *pRunInfo)
{
	int i;
	RUN_CONFIG_ITEM *pItem;

	pItem = pRunInfo->pChangedInfo;
	for (i = 0; i < pRunInfo->iChangedInfoNum; i++, pItem++)
	{
		DELETE(pItem->szText);
	}

	DELETE(pRunInfo->pChangedInfo);
}

/*==========================================================================*
 * FUNCTION : Cfg_LoadRunConfig
 * PURPOSE  : main interface to load runtime changed config info
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : int : err code defined in the err_code.h
 * COMMENTS : runtime changed config file is produced by ACU
 * CREATOR  : LinTao                   DATE: 2004-09-22 10:59
 *==========================================================================*/
int Cfg_LoadRunConfig()
{
	RUN_CONFIG_INFO runConfigInfo;
	RUN_CONFIG_ITEM *pChangedInfo;

	/* tmp reference pointer */
	LANG_TEXT *pText;
	EQUIP_INFO *pEquip;
	void *pSig;

	int i, ret;
	char szRunFileName[MAX_FILE_PATH];  

	Cfg_GetFullConfigPath(RUN_CFGFILE, szRunFileName, MAX_FILE_PATH);

	/* 1.read MainConfig.run info to the RUN_CONFIG_INFO structure */
	ret = Cfg_LoadConfigFile(szRunFileName, LoadRunConfigProc, &runConfigInfo);

	if (ret != ERR_CFG_OK)
	{
		FreeRunConfigInfo(&runConfigInfo);
		return ERR_CFG_FAIL;
	}

	/* 2.rewrite relevent structure according runConfigInfo*/
	
	pChangedInfo = runConfigInfo.pChangedInfo;
	for (i = 0; i < runConfigInfo.iChangedInfoNum; i++, pChangedInfo++)
	{
		/*RunThread_Heartbeat(RunThread_GetId(NULL));*/
		
		/* 2.1 Alarm Level changed process */
		if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_ALARMLEVEL)
		{
			pSig = GetCfgSig(pChangedInfo->iEquipTypeID, 
				pChangedInfo->iSigType, pChangedInfo->iSigID);

			if (pSig != NULL)
			{
				ret = ChangeAlarmLevel(pSig, pChangedInfo->szText);

				if (ret == ERR_CFG_BADRUNINFO)
				{
					AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
						"[%s]--Cfg_LoadRunConfig: ERROR: Record: %d "
						"of %s is invalid: the new Alarm Level format is "
						"wrong!\n", __FILE__, i + 1, szRunFileName);
					FreeRunConfigInfo(&runConfigInfo);
					return ERR_CFG_BADRUNINFO;
				}
			}
			else
			{
				AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
					"[%s]--Cfg_LoadRunConfig: WARNING: Record: %d of "
					"%s is invalid: the Alarm Sig does not exist!\n", 
					__FILE__, i + 1, szRunFileName);	
			}
		}
		/* 2.2 Alarm Relay changed process, added by ht,2006.11.3*/
		else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_ALARMRELAY)
		{
			pSig = GetCfgSig(pChangedInfo->iEquipTypeID, 
				pChangedInfo->iSigType, pChangedInfo->iSigID);

			if (pSig != NULL)
			{
				ret = ChangeAlarmRelay(pSig, pChangedInfo->szText);

				if (ret == ERR_CFG_BADRUNINFO)
				{
					AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
						"[%s]--Cfg_LoadRunConfig: ERROR: Record: %d "
						"of %s is invalid: the new Alarm Relay format is "
						"wrong!\n", __FILE__, i + 1, szRunFileName);
					FreeRunConfigInfo(&runConfigInfo);
					return ERR_CFG_BADRUNINFO;
				}
			}
			else
			{
				AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
					"[%s]--Cfg_LoadRunConfig: WARNING: Record: %d of "
					"%s is invalid: the Alarm Sig does not exist!\n", 
					__FILE__, i + 1, szRunFileName);	
			}
		}
		/* 2.3 Name changed process */
		else
		{
			/* site name changed */
			if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SITENAME)
			{
				pText = &g_SiteInfo.langSiteName;

				ret = ChangeTextName(pText, pChangedInfo->iTextType, 
					pChangedInfo->szText);				
			}

			/* site location changed */
			else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SITELOCATION)
			{
				pText = &g_SiteInfo.langSiteLocation;

				ret = ChangeTextName(pText, pChangedInfo->iTextType, 
					pChangedInfo->szText);				
			}

			/* site description changed */
			else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SITEDESCRIPTION)
			{
				pText = &g_SiteInfo.langDescription;

				ret = ChangeTextName(pText, pChangedInfo->iTextType, 
					pChangedInfo->szText);				
			}

			/* equip name changed */
			else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_EQUIPNAME)
			{
				pEquip = GetEquip(pChangedInfo->iEquipID);

				if (pEquip != NULL)
				{
					pText = pEquip->pEquipName;
					ret = ChangeTextName(pText, pChangedInfo->iTextType, 
						pChangedInfo->szText);
				}
				else
				{
					AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
						"[%s]--Cfg_LoadRunConfig: WARNING: Record: "
						"%d of %s is invalid: Equip ID does not exist!\n",
						__FILE__, i + 1, szRunFileName);	
				}
			}

			/* sig name changed */
			else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SIGNAME)
			{
				pSig = GetCfgSig(pChangedInfo->iEquipTypeID,
					pChangedInfo->iSigType, pChangedInfo->iSigID);

				if (pSig != NULL)
				{
					switch(pChangedInfo->iSigType)
					{
					case SIG_TYPE_SAMPLING:
						pText = ((SAMPLE_SIG_INFO *)pSig)->pSigName;
						break;

					case SIG_TYPE_CONTROL:
						pText = ((CTRL_SIG_INFO * )pSig)->pSigName;
						break;

					case SIG_TYPE_SETTING:
						pText = ((SET_SIG_INFO *)pSig)->pSigName;
						break;

					case SIG_TYPE_ALARM:
						pText = ((ALARM_SIG_INFO *)pSig)->pSigName;
						break;

						/* will not be others */
					default:
						TRACE("[%s]--Cfg_LoadRunConfig: ERROR: iSigType of "
							"RUN_CONFIG_ITEM is invalid!\n", __FILE__);
						ASSERT(0);
					}

					ret = ChangeTextName(pText, pChangedInfo->iTextType, 
						pChangedInfo->szText);
				}
				else
				{
					AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
						"[%s]--Cfg_LoadRunConfig: WARNING: Record: "
						"%d of %s is invalid: the Sig does not exist!\n",
						__FILE__, i + 1, szRunFileName);
				}	
			}/* end of sig name changed */
			else
			{
				/* will not go here */
				TRACE("runConfigInfo.iChangedInfoNum is %d\n", runConfigInfo.iChangedInfoNum);
				TRACE("dwChangeInfoType of RUN_CONFIG_ITEM is invalid!\n");
				TRACE("pChangedInfo->dwChangeInfoType is : %d\n",pChangedInfo->dwChangeInfoType);
				ASSERT(0);
			}

			/* do the common process for name changed */
			/* should return */
			if (ret == ERR_CFG_NO_MEMORY)
			{
				AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
					"[%s]--Cfg_LoadRunConfig: ERROR: Memory is not "
					"enough!\n", __FILE__);
				FreeRunConfigInfo(&runConfigInfo);
				return ERR_CFG_NO_MEMORY;
			}

			/* just warning */
			if (ret == ERR_CFG_BADRUNINFO)
			{
				TRACE("Record: %d of %s is invalid: Text type "
					"is wrong!\n", i + 1, szRunFileName);
				ASSERT(0);
			}
		}/* end of Name changed process */
	}/* end of loop */

	FreeRunConfigInfo(&runConfigInfo);
	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : CheckDirty_ST
 * PURPOSE  : Check if current row of MainConfig.run is dirty
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int              iCurPos         : Checked item index
 *            RUN_CONFIG_INFO  *pRunConfigInfo : 
 * RETURN   : BOOL : 
 * COMMENTS : for Site Name changed type
 * CREATOR  : LinTao                   DATE: 2004-09-22 16:02
 *==========================================================================*/
static BOOL CheckDirty_ST(IN int iCurPos, IN RUN_CONFIG_INFO *pRunConfigInfo)
{
	int i;
	RUN_CONFIG_ITEM *pChangedInfo;

	DWORD	dwChangeInfoType;   
	int		iTextType;

	pChangedInfo = pRunConfigInfo->pChangedInfo + iCurPos;

	dwChangeInfoType = pChangedInfo->dwChangeInfoType;
	iTextType = pChangedInfo->iTextType;	

	for (i = iCurPos + 1; i < pRunConfigInfo->iChangedInfoNum; i++)
	{
		pChangedInfo = pRunConfigInfo->pChangedInfo + i;

		if ((pChangedInfo->dwChangeInfoType & dwChangeInfoType)
			&& (iTextType == pChangedInfo->iTextType))
		{
			return TRUE;
		}
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : CheckDirty_EP
 * PURPOSE  : Check if current row of MainConfig.run is dirty
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int              iCurPos         : 
 *            IN RUN_CONFIG_INFO  *pRunConfigInfo : 
 * RETURN   : BOOL : 
 * COMMENTS : for Equip Name changed type
 * CREATOR  : LinTao                   DATE: 2004-09-22 16:09
 *==========================================================================*/
static BOOL CheckDirty_EP(IN int iCurPos, IN RUN_CONFIG_INFO *pRunConfigInfo)
{
	int i;
	int iTextType, iEquipID;
	RUN_CONFIG_ITEM *pChangedInfo;

	pChangedInfo = pRunConfigInfo->pChangedInfo + iCurPos;

	iTextType = pChangedInfo->iTextType;
	iEquipID = pChangedInfo->iEquipID;

	for (i = iCurPos + 1; i < pRunConfigInfo->iChangedInfoNum; i++)
	{
		pChangedInfo++;

		if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_EQUIPNAME)
		{
			if (pChangedInfo->iTextType == iTextType && 
				pChangedInfo->iEquipID == iEquipID)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : CheckDirty_SG
 * PURPOSE  : Check if current row of MainConfig.run is dirty
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int              iCurPos         : 
 *            IN RUN_CONFIG_INFO  *pRunConfigInfo : 
 * RETURN   : BOOL : 
 * COMMENTS : for Sig Name changed type
 * CREATOR  : LinTao                   DATE: 2004-09-22 16:17
 *==========================================================================*/
static BOOL CheckDirty_SG(IN int iCurPos, IN RUN_CONFIG_INFO *pRunConfigInfo)
{
	int i;
	int iTextType, iEquipTypeID, iSigType, iSigID;
	RUN_CONFIG_ITEM *pChangedInfo;

	pChangedInfo = pRunConfigInfo->pChangedInfo + iCurPos;

	iTextType = pChangedInfo->iTextType;
	iEquipTypeID = pChangedInfo->iEquipTypeID;
	iSigType = pChangedInfo->iSigType;
	iSigID = pChangedInfo->iSigID;

	for (i = iCurPos + 1; i < pRunConfigInfo->iChangedInfoNum; i++)
	{
		pChangedInfo++;

		if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SIGNAME)
		{
			if (pChangedInfo->iTextType == iTextType && 
				pChangedInfo->iEquipTypeID == iEquipTypeID &&
				pChangedInfo->iSigType == iSigType &&
				pChangedInfo->iSigID == iSigID)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : CheckDirty_AL
 * PURPOSE  : Check if current row of MainConfig.run is dirty
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int              iCurPos         : 
 *            IN RUN_CONFIG_INFO  *pRunConfigInfo : 
 * RETURN   :  BOOL : 
 * COMMENTS : for Alarm Level changed type
 * CREATOR  : LinTao                   DATE: 2004-09-22 16:22
 *==========================================================================*/
static BOOL CheckDirty_AL(IN int iCurPos, IN RUN_CONFIG_INFO *pRunConfigInfo)
{
	int i;
	int iEquipTypeID, iSigID;
	RUN_CONFIG_ITEM *pChangedInfo;

	pChangedInfo = pRunConfigInfo->pChangedInfo + iCurPos;

	iEquipTypeID = pChangedInfo->iEquipTypeID;
	iSigID = pChangedInfo->iSigID;

	for (i = iCurPos + 1; i < pRunConfigInfo->iChangedInfoNum; i++)
	{
		pChangedInfo++;

		if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_ALARMLEVEL)
		{
			if (pChangedInfo->iEquipTypeID == iEquipTypeID && 
				pChangedInfo->iSigID == iSigID)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : CheckDirty_AR
 * PURPOSE  : Check if current row of MainConfig.run is dirty
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int              iCurPos         : 
 *            IN RUN_CONFIG_INFO  *pRunConfigInfo : 
 * RETURN   :  BOOL : 
 * COMMENTS : for Alarm Relay changed type
 * CREATOR  : HanTao                   DATE: 2006-11-03 16:22
 *==========================================================================*/
static BOOL CheckDirty_AR(IN int iCurPos, IN RUN_CONFIG_INFO *pRunConfigInfo)
{
	int i;
	int iEquipTypeID, iSigID;
	RUN_CONFIG_ITEM *pChangedInfo;

	pChangedInfo = pRunConfigInfo->pChangedInfo + iCurPos;

	iEquipTypeID = pChangedInfo->iEquipTypeID;
	iSigID = pChangedInfo->iSigID;

	for (i = iCurPos + 1; i < pRunConfigInfo->iChangedInfoNum; i++)
	{
		pChangedInfo++;

		if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_ALARMRELAY)
		{
			if (pChangedInfo->iEquipTypeID == iEquipTypeID && 
				pChangedInfo->iSigID == iSigID)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : CheckDirty_ID
 * PURPOSE  : check dirty caused by discarded IDs
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: RUN_CONFIG_ITEM  *pChangedInfo : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-22 18:32
 *==========================================================================*/
static BOOL CheckDirty_ID(RUN_CONFIG_ITEM *pChangedInfo)
{
	DWORD dwChangeInfoType;

	dwChangeInfoType = pChangedInfo->dwChangeInfoType;

	if (dwChangeInfoType & CONFIG_CHANGED_EQUIPNAME)
	{
		if (GetEquip(pChangedInfo->iEquipID) == NULL)
		{
			return TRUE;
		}
	}

	else if (dwChangeInfoType & CONFIG_CHANGED_SIGNAME)
	{
		if (GetCfgSig(pChangedInfo->iEquipTypeID, pChangedInfo->iSigType,
			pChangedInfo->iSigID) == NULL)
		{
			return TRUE;
		}
	}

	else if (dwChangeInfoType & CONFIG_CHANGED_ALARMLEVEL)
	{
		if (GetCfgSig(pChangedInfo->iEquipTypeID, SIG_TYPE_ALARM,
			pChangedInfo->iSigID) == NULL)
		{
			return TRUE;
		}
	}

	else if (dwChangeInfoType & CONFIG_CHANGED_ALARMRELAY)
	{
		if (GetCfgSig(pChangedInfo->iEquipTypeID, SIG_TYPE_ALARM,
			pChangedInfo->iSigID) == NULL)
		{
			return TRUE;
		}
	}

	/* for CONFIG_CHANGED_SITENAME */
	else
	{
		return FALSE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : Cfg_CleanMainRunFile
 * PURPOSE  : clean MainConfig.run file
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-22 15:17
 *==========================================================================*/
void Cfg_CleanMainRunFile(BOOL bClearALL)
{
	int  i, ret;
	BOOL bDirtyFlag = FALSE;
	char szRunFileName[MAX_PATH];  
	char *szInFile;
	long lLastPos, lCurPos;
	char *pCurOutPos;
	char szRowBuffer[MAX_LINE_SIZE];

	/* output file buffer */
	FILE *pFile;
	long lFileLen;
	char *szOutFile;
	void *pProf;

	RUN_CONFIG_INFO runConfigInfo;
	RUN_CONFIG_ITEM *pChangedInfo;

	Cfg_GetFullConfigPath(RUN_CFGFILE, szRunFileName, MAX_FILE_PATH);

	/* 1.read MainConfig.run info to the RUN_CONFIG_INFO structure */
	if (!bClearALL)
	{
		ret = Cfg_LoadConfigFile(szRunFileName, LoadRunConfigProc, &runConfigInfo);

		if (ret != ERR_CFG_OK)
		{
			return;
		}
	}

	/* 2.prepare IN & out file buffer */
	pFile = fopen(szRunFileName, "r");
	if (pFile == NULL)
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--Cfg_CleanMainRunFile: ERROR: Can not open the "
			"file: %s!\n", __FILE__, szRunFileName);
		return;
	}

	lFileLen = GetFileLength(pFile);

	szInFile = NEW(char, lFileLen + 1);
	if (szInFile == NULL)
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--Cfg_CleanMainRunFile: ERROR: There is no memory for "
			"clean %s file.\n", __FILE__, szRunFileName);
		fclose(pFile);
		return;
	}

	szOutFile = NEW(char, lFileLen + 1);
	if (szOutFile == NULL)
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--Cfg_CleanMainRunFile: ERROR: There is no memory for "
			"clean %s file.\n", __FILE__, szRunFileName);
		DELETE(szInFile);
		fclose(pFile);
		return;
	}


	lFileLen = fread(szInFile, sizeof(char), (size_t)lFileLen, pFile);
	fclose(pFile);

	if (lFileLen < 0) 
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--Cfg_CleanMainRunFile: ERROR: Read %s file failed!\n",
			__FILE__, szRunFileName);

		/* clear the memory */
		DELETE(szInFile);
		DELETE(szOutFile);

		return;
	}
	szInFile[lFileLen] = '\0';  // end with NULL
	lLastPos = 0;

	pProf = Cfg_ProfileOpen(szInFile, lFileLen + 1);

	/* 3.begin reading, to szOutFile */
	/* reading until meet runtime changed config data */
	if (Cfg_ProfileFindSection(pProf, CHANGED_INFO) == 0)
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--Cfg_CleanMainRunFile: ERROR: find %s section "
			"failed!\n", __FILE__, CHANGED_INFO);

		DELETE(szInFile);
		DELETE(szOutFile);

		return;
	}

	/* read the comment line #Time	Changed Info Type... */
	if (Cfg_ProfileReadLine(pProf, szRowBuffer, MAX_LINE_SIZE) == 0)
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--Cfg_CleanMainRunFile: ERROR: Comment line not "
			"found!\n", __FILE__);

		DELETE(szInFile);
		DELETE(szOutFile);

		return;
	}

	lCurPos = Cfg_ProfileTell(pProf);

	memcpy(szOutFile, szInFile + lLastPos, sizeof(char)*(lCurPos - lLastPos));
	pCurOutPos = szOutFile + (lCurPos - lLastPos);

	/* reading runtime changed config data */
	if (!bClearALL)
	{
		pChangedInfo = runConfigInfo.pChangedInfo;
		for (i = 0; i < runConfigInfo.iChangedInfoNum; i++, pChangedInfo++)
		{
			/* read next line data */
			lLastPos = lCurPos;
			Cfg_ProfileGetNextLine(pProf, szRowBuffer, MAX_LINE_SIZE);
			lCurPos = Cfg_ProfileTell(pProf);

			/* validity check */
			if (CheckDirty_ID(pChangedInfo))
			{
				bDirtyFlag = TRUE;
			}
			else
			{
				/* duplicating check */
				if ((pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SITENAME)
					|| (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SITELOCATION)
					|| (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SITEDESCRIPTION))
				{
					bDirtyFlag = CheckDirty_ST(i, &runConfigInfo);
				}
				else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_EQUIPNAME)
				{
					bDirtyFlag = CheckDirty_EP(i, &runConfigInfo);
				}
				else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_SIGNAME)
				{
					bDirtyFlag = CheckDirty_SG(i, &runConfigInfo);
				}
				else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_ALARMLEVEL)
				{
					bDirtyFlag = CheckDirty_AL(i, &runConfigInfo);
				}
				else if (pChangedInfo->dwChangeInfoType & CONFIG_CHANGED_ALARMRELAY)//added by ht,2006.11.3
				{
					bDirtyFlag = CheckDirty_AR(i, &runConfigInfo);
				}
				else
				{
					TRACE("pChangedInfo->dwChangeInfoType is : %d\n",pChangedInfo->dwChangeInfoType);
					AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
						"[%s]--Cfg_CleanMainRunFile: ERROR: dwChangeInfoType"
						"of RUN_CONFIG_ITEM is invalid!\n", __FILE__);
					ASSERT(0);
				}
			}

			if (!bDirtyFlag)
			{
				memcpy(pCurOutPos, szInFile + lLastPos, 
					(size_t)(lCurPos - lLastPos));
				pCurOutPos = pCurOutPos + (lCurPos - lLastPos);
			}
		}

		FreeRunConfigInfo(&runConfigInfo);
	}

	Cfg_ProfileClose(pProf);

	if (Mutex_Lock(GetMutexLoadRunConfig(),MAX_TIME_WAITING_LOAD_RUN_CONFIG) 
		== ERR_MUTEX_OK)
	{

		/* regenerate the MainConfig.run File */
		/* backup strategy will be added later, now just overwrite */
		pFile = fopen(szRunFileName, "w");

		if (pFile != NULL)
		{
			lFileLen = fwrite(szOutFile, sizeof(char), 
				(size_t)(pCurOutPos - szOutFile), pFile);
			if (lFileLen != pCurOutPos - szOutFile)
			{
				// to do: backup process
				AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
					"[%s]--Cfg_CleanMainRunFile: ERROR: Clean %s File failed",
					__FILE__, szRunFileName);
			}

			fclose(pFile);
		}
		
		Mutex_Unlock(GetMutexLoadRunConfig());
	}
	else
	{
		return;
	}

	/* clear memory */
	DELETE(szOutFile);

	return;
}

/*==========================================================================*
 * FUNCTION : AddStringToEndOfFile
 * PURPOSE  : Append a string to the end of file
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: FILE*  pFile     : FILE pointer 
 *            int    nWriteLen : string length
 *            char*  pString   : the written string
 * RETURN   : static BOOL : 0, append successful
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-20 09:06
 *==========================================================================*/
static BOOL AddStringToEndOfFile(FILE* pFile, int nWriteLen, char* pString)
{
	BOOL bRetFlag = TRUE;

	if (pFile != NULL)
	{
		fseek(pFile, 0, SEEK_END);

		fwrite(pString, sizeof(char), (size_t)nWriteLen, pFile);
	}
	else
	{
		bRetFlag = FALSE;
	}

	return bRetFlag;
}


/*==========================================================================*
 * FUNCTION : WriteRunConfigItem
 * PURPOSE  : Add a run config item, include the change of site name,equipname
			  signal name and alarm level
 * CALLS    : 
 * CALLED BY: DXI interface
 * ARGUMENTS: RUN_CONFIG_ITEM*  pRunConfigItem : change info
 * RETURN   : int : error code, 0 is successful
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-20 09:04
 *==========================================================================*/
int WriteRunConfigItem(RUN_CONFIG_ITEM* pRunConfigItem)
{
	int nError = ERR_CFG_OK;

	char szRunFileName[MAX_PATH];  
	char *szInFile;

	void *pProf;

	/* output file buffer */
	FILE *pFile;
	long lFileLen;

	char szWriteString[MAX_LINE_SIZE];

	time_t tCurTime;
	struct tm tmCurTime;

	char szChangeTime[64];
	char szChangeInfoType[8];
	char szChangeTextType[8];

	char szEquipID[8];
	char szSignalType[8];
	char szSignalID[8];

	Cfg_GetFullConfigPath(RUN_CFGFILE, szRunFileName, MAX_FILE_PATH);

	if (Mutex_Lock(GetMutexLoadRunConfig(), MAX_TIME_WAITING_LOAD_RUN_CONFIG) 
		== ERR_MUTEX_OK)
	{
		/* 2.prepare IN & out file buffer */
		pFile = fopen(szRunFileName, "a+");//read and write, if no file, create
	
		if (pFile == NULL)
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--WriteRunConfigItem: ERROR: Can not open the "
				"file: %s!\n", __FILE__, szRunFileName);

			Mutex_Unlock(GetMutexLoadRunConfig());

			return ERR_CFG_FILE_OPEN;
		}

		lFileLen = GetFileLength(pFile);

		szInFile = NEW(char, lFileLen + 1);

		if (szInFile == NULL)
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--Cfg_CleanMainRunFile: ERROR: There is no memory for "
				"clean %s file.\n", __FILE__, szRunFileName);

			fclose(pFile);

			Mutex_Unlock(GetMutexLoadRunConfig());

			return ERR_CFG_NO_MEMORY;
		}

		lFileLen = fread(szInFile, sizeof(char), (size_t)lFileLen, pFile);

		if (lFileLen < 0) 
		{
			AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
				"[%s]--Cfg_CleanMainRunFile: ERROR: Read %s file failed!\n",
				__FILE__, szRunFileName);

			fclose(pFile);

			/* clear the memory */
			DELETE(szInFile);

			Mutex_Unlock(GetMutexLoadRunConfig());

			return ERR_CFG_FILE_READ;
		}

		szInFile[lFileLen] = '\0';  // end with NULL

		pProf = Cfg_ProfileOpen(szInFile, lFileLen);

		//If no section of CHANGED_INFO, append it to end of file
		if (pProf == NULL
			|| Cfg_ProfileFindSection(pProf, CHANGED_INFO) == ERR_CFG_OK)
		{
			snprintf(szWriteString, sizeof(szWriteString), "%s%s",
				SPLITTER_LINE, CHANGED_INFO);

			AddStringToEndOfFile(pFile, (int)strlen(szWriteString), szWriteString);

			snprintf(szWriteString, sizeof(szWriteString), "%s%s",
				SPLITTER_LINE, "#Time	Changed Info Type	Text Type    New Text	EquipID(for EP)/Equip Type ID(for SG,AL)	Sig Type	Sig ID");
		
			AddStringToEndOfFile(pFile, (int)strlen(szWriteString), szWriteString);
		}

		DELETE(pProf);//Will not be used later, so DELETE it
		/* clear the memory */
		DELETE(szInFile);

		//1. change time
		time(&tCurTime);
		strftime(szChangeTime, sizeof(szChangeTime), "%Y-%m-%d %H:%M:%S", 
			gmtime_r(&(tCurTime), &tmCurTime));

		//2.Changed Info Type
		if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_SITENAME)
		{
			strncpyz(szChangeInfoType, "ST", sizeof(szChangeInfoType));
		}
		else if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_SITELOCATION)
		{
			strncpyz(szChangeInfoType, "SL", sizeof(szChangeInfoType));
		}
		else if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_SITEDESCRIPTION)
		{
			strncpyz(szChangeInfoType, "SD", sizeof(szChangeInfoType));
		}
		else if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_EQUIPNAME)
		{
			strncpyz(szChangeInfoType, "EP", sizeof(szChangeInfoType));
		}
		else if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_SIGNAME)
		{
			strncpyz(szChangeInfoType, "SG", sizeof(szChangeInfoType));
		}
		else if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMLEVEL)
		{
			strncpyz(szChangeInfoType, "AL", sizeof(szChangeInfoType));
		}
		else if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMRELAY)//added by ht,2006.11.02
		{
			strncpyz(szChangeInfoType, "AR", sizeof(szChangeInfoType));
		}
		else
		{
			strncpyz(szChangeInfoType, "-", sizeof(szChangeInfoType));
		}

		//3. Text Type
		if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMLEVEL
			|| pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMRELAY)//added by ht,2006.11.02
		{
			strncpyz(szChangeTextType, "-", sizeof(szChangeTextType));
		}
		else
		{
			if (pRunConfigItem->iTextType == ENGLISH_FULL)
			{
				strncpyz(szChangeTextType, "EF", sizeof(szChangeTextType));
			}
			else if (pRunConfigItem->iTextType == ENGLISH_ABBR)
			{
				strncpyz(szChangeTextType, "EA", sizeof(szChangeTextType));
			}
			else if (pRunConfigItem->iTextType == LOCALE_FULL)
			{
				strncpyz(szChangeTextType, "LF", sizeof(szChangeTextType));
			}
			else if (pRunConfigItem->iTextType == LOCALE_ABBR)
			{
				strncpyz(szChangeTextType, "LA", sizeof(szChangeTextType));
			}
            //////////////////////////////////////////////////////////////////////////
            //Added by wj for three languages 2006.5.11

            else if (pRunConfigItem->iTextType == LOCALE2_FULL)
            {
                strncpyz(szChangeTextType, "L2F", sizeof(szChangeTextType));
            }
            else if (pRunConfigItem->iTextType == LOCALE2_ABBR)
            {
                strncpyz(szChangeTextType, "L2A", sizeof(szChangeTextType));
            }
            //end////////////////////////////////////////////////////////////////////////
            
			else
			{
				strncpyz(szChangeTextType, "-", sizeof(szChangeTextType));
			}
		}

		//5. EquipID(for EP)/Equip Type ID(for SG,AL)
		if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_EQUIPNAME)
		{
			snprintf(szEquipID, sizeof(szEquipID), "%d", pRunConfigItem->iEquipID);
		}
		else if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_SIGNAME
			|| pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMLEVEL
			|| pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMRELAY)//added by ht,2006.11.02
		{
			snprintf(szEquipID, sizeof(szEquipID), "%d", pRunConfigItem->iEquipTypeID);
		}
		else
		{
			snprintf(szEquipID, sizeof(szEquipID), "-");
		}

		//6. Sig Type
		if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_SIGNAME
			|| pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMLEVEL
			|| pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMRELAY)//added by ht,2006.11.09
		{
			if (pRunConfigItem->iSigType == SIG_TYPE_SAMPLING)
			{
				snprintf(szSignalType, sizeof(szSignalType), "%s", "SP");
			}
			else if (pRunConfigItem->iSigType == SIG_TYPE_CONTROL)
			{
				snprintf(szSignalType, sizeof(szSignalType), "%s", "C");
			}
			else if (pRunConfigItem->iSigType == SIG_TYPE_SETTING)
			{
				snprintf(szSignalType, sizeof(szSignalType), "%s", "ST");
			}
			else if (pRunConfigItem->iSigType == SIG_TYPE_ALARM)
			{
				snprintf(szSignalType, sizeof(szSignalType), "%s", "A");
			}
			else
			{
				snprintf(szSignalType, sizeof(szSignalType), "%s", "-");
			}
		}
		else
		{
			snprintf(szSignalType, sizeof(szSignalType), "-");
		}

		//7. Sig ID
		if (pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_SIGNAME
			|| pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMLEVEL
			|| pRunConfigItem->dwChangeInfoType == CONFIG_CHANGED_ALARMRELAY)//added by ht,2006.11.02
		{
			snprintf(szSignalID, sizeof(szSignalID), "%d", pRunConfigItem->iSigID);
		}
		else
		{
			snprintf(szSignalID, sizeof(szSignalID), "-");
		}

		//A changed config info
		snprintf(szWriteString, sizeof(szWriteString),
			"%s%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s",
			SPLITTER_LINE,
			szChangeTime,
			SPLITTER, SPLITTER,
			szChangeInfoType,
			SPLITTER, SPLITTER,
			szChangeTextType,
			SPLITTER, SPLITTER,
			pRunConfigItem->szText,
			SPLITTER, SPLITTER,
			szEquipID,
			SPLITTER, SPLITTER,
			szSignalType,
			SPLITTER, SPLITTER,
			szSignalID);

		//append it to the file of end
		AddStringToEndOfFile(pFile, (int)strlen(szWriteString), szWriteString);		

		fclose(pFile);

		Mutex_Unlock(GetMutexLoadRunConfig());

	}
	else
	{
		return ERR_CFG_RUNINFO_MUTEX_TIMEOUT;
	}

	//If the file size is bigger than 64k, clean run file
#define _MAX_TO_CLEAN_DIRTY_FILE_SIZE (64 * 1000)

	pFile = fopen(szRunFileName, "r");//read file length
	if (pFile == NULL)
	{
		AppLogOut(CFG_LOAD_RUN, APP_LOG_ERROR, 
			"[%s]--WriteRunConfigItem: ERROR: Can not open the "
			"file: %s!\n", __FILE__, szRunFileName);

		return ERR_CFG_FILE_OPEN;
	}

	lFileLen = GetFileLength(pFile);

	fclose(pFile);

	if (lFileLen >= _MAX_TO_CLEAN_DIRTY_FILE_SIZE)
	{
		TRACEX("Cfg_CleanMainRunFile\n");

		Cfg_CleanMainRunFile(FALSE);
	}


	return nError;
}

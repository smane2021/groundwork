/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced controller unit)
 *
 *  FILENAME : cfg_loader.h
 *  CREATOR  : LinTao                   DATE: 2004-09-09
 *  VERSION  : V1.00
 *  PURPOSE  : head file for Config Management Module
 *
 *  HISTORY  :
 *==========================================================================*/
#ifndef __CFG_LOADER_H_2004_09_09__
#define __CFG_LOADER_H_2004_09_09__

/* used by expression analysis */
enum TypesUsedByExp
{
	/* expression operator type definition */
	// The operator must be start from 0, and less than EXP_MAX_OPERATOR.
	// The value of the operators must be continuous due to the value will be 
	// used as the array index in calculate the parsed expression in running.
	// if adds some new operator, the function Exp_Calculate() in 
	// equip_exp_eval.c must be modified at the same time. 
	// maofuhua,04-11-11
	EXP_NOT	  =   0     ,
	EXP_MUL				,
	EXP_DIV				,
	EXP_ADD				,
	EXP_SUB				,
	EXP_GREATER			,
	EXP_LESS			,
	EXP_GREATER_EQUAL	,
	EXP_LESS_EQUAL		,
	EXP_EQUAL			,
	EXP_AND				,
	EXP_OR				,
	EXP_MAX_OPERATOR	,	// end of all operator

	// middle op used in parsing expression
	EXP_LF_BRACKET		,			/* '(' */
	EXP_RT_BRACKET		,			/* ')' */

	/* element type definition */
	EXP_ETYPE_VARIABLE	,
	EXP_ETYPE_CONST		,
	EXP_ETYPE_OPERATOR	,
	EXP_ETYPE_DUMMY		,

	// run time type.
	EXP_ETYPE_VARIABLE_RAW,	// indicate get the raw value of this var. maofuhua. 2005-3-15
	EXP_ETYPE_INVALID	,	// the EXP value is invalid. used in calculation. maofuhua.
	EXP_ETYPE_ERROR		,	// the EXP expression is error when calculating. used in calculation
};


// macro to get the value from the value of EXP_ELEMENT which type is EXP_CONST 
#define EXP_SET_CONST_VALUE(pExpEle, v)		((*(EXP_CONST *)(pExpEle)->pElement) = (EXP_CONST)(v))

#define EXP_GET_CONST_VALUE(pExpEle)		((*(EXP_CONST *)(pExpEle)->pElement))
#define EXP_GET_CONST_INT(pExpEle)			((int)EXP_GET_CONST_VALUE(pExpEle))
#define EXP_GET_CONST_LONG(pExpEle)			((long)EXP_GET_CONST_VALUE(pExpEle))
#define EXP_GET_CONST_FLOAT(pExpEle)		((float)EXP_GET_CONST_VALUE(pExpEle))

#define EXP_IS_VAR(pVar)	(((pVar)->byElementType == EXP_ETYPE_VARIABLE) ||	\
			((pVar)->byElementType == EXP_ETYPE_VARIABLE_RAW))

/* constants for download config files */
enum FileTypeForDownload
{
	FILE_TYPE_SOLUTION = 0,

	FILE_TYPE_BASICSTD,
	FILE_TYPE_BASICSTD_RES,

	FILE_TYPE_STD_EQUIP,
	FILE_TYPE_STD_EQUIP_RES
};

//Added by Thomas for TR# 62-ACU, 2006-11-28
struct tagCfgParamRunInfo
{
	char   sFileHeadInfo[16];   //use Macro PARAM_FILE_HEAD_INFO
	int    iVersion;            //use Macro VER_PARAM_RECORD

	int    iParamNum;
	PERSISTENT_SIG_RECORD  *pParam;

	int	iUserNum;
	USER_INFO_STRU *pUserInfo;

	int	iNMSNum;
	NMS_INFO  *pNMSInfo;

	int	iNMS3Num;
	V3NMS_INFO *pNMS3Info;


	int	iCheckUserSum;	//For user sum.
	int    iCheckSum;          //simple check sum for pParam data, implemented as YDN23 protocol

	
};
typedef struct tagCfgParamRunInfo  CFG_PARAM_RUN_INFO;
//Added end, 2006-11-28

/* interface */
int Cfg_InitialConfig(void);
void Cfg_UnloadConfig(void);
void Cfg_CleanMainRunFile(BOOL bClearALL);


/* assistant functions */
EQUIP_INFO *GetEquip(IN int iEquipID);
void *GetCfgSig(IN int iEquipTypeID, IN int iSigType, IN int iSigID);

#define ONE_MONTH_HOUR	    720

extern HIS_DATA_RECORD_LOAD    g_stLoadCurrentData[ONE_MONTH_HOUR];

#endif //__CFG_LOADER_H_2004_09_09__

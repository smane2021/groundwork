/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_data_process.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 16:56
 *  VERSION  : V1.00
 *  PURPOSE  : equipmenet manager for data process.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#include "stdsys.h"
#include "public.h"

/* for toupper function */
#include <ctype.h> 
#include <values.h>
#include <time.h>
#include "../../sampler/sampler_base/local_linux.h"
#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <net/if.h>				/*ifreq */

#include "cfg_model.h"
#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "equip_mon.h"

#ifdef  _DEBUG_EQUIP_MON
//#define _DEBUG_DATA_PROCESS			1
/*
#define NotificationFunc(nMasks, uSize, pData, bUrgent)	\
		TRACEX("Calling NotificationFunc(mask=%x, size=%d, pData=%p, urgent=%d).\n",\
			nMasks, uSize, pData, bUrgent)
*/
#endif


/* 
 * the data process method please refer to the document:
 *  HLD for ACU Software - Equipment Monitoring Module.doc
 *	6.3.1	EQPMON-04-01 - Site_EquipmentManager
 *
 */
static BOOL Site_ProcessBasicSignals(IN SITE_INFO *pSite);
static BOOL Site_ProcessAlarmSignals(IN SITE_INFO *pSite);
static BOOL Site_ProcessDelayingAlarms(IN SITE_INFO *pSite);
static BOOL Site_ProcessAlarmSuppressions(IN SITE_INFO *pSite);
static BOOL Site_ProcessAlarmRelays(IN SITE_INFO *pSite);
static BOOL Site_SaveStatData(IN SITE_INFO *pSite);
static void Site_ResetStatData(IN OUT SITE_INFO *pSite);
static BOOL Site_SaveActiveAlarms(IN SITE_INFO *pSite);
static int  Site_OnTimer(HANDLE hself, int idTimer, SITE_INFO *pSite);
static BOOL Site_ProcessSpecialSignals(IN SITE_INFO *pSite);
static BOOL Site_ResetProcessingState(IN SITE_INFO *pSite);
static BOOL Equip_ProcessCommStatus(IN SITE_INFO *pSite,
									IN OUT EQUIP_INFO *pEquip);
static BOOL Equip_ProcessSamplingSignals(IN SITE_INFO *pSite,
										 IN OUT EQUIP_INFO *pEquip);
static BOOL Equip_ProcessSettingSignals(IN SITE_INFO *pSite,
										IN OUT EQUIP_INFO *pEquip);
static BOOL Equip_ProcessControlSignals(IN SITE_INFO *pSite,
										IN OUT EQUIP_INFO *pEquip);
static BOOL Equip_ProcessSerialNumber(IN SITE_INFO *pSite,
									  IN OUT EQUIP_INFO *pEquip);
static void RelayOutputCtrl(int iCtrlValue, int iRelayNo);
static SIG_ENUM GetRelayStatus(int iRelayNo);
static BOOL StdEquip_IBEIB_ProcessSpecialSignals(IN SITE_INFO *pSite);
static BOOL StdEquip_Source_ProcessSpecialSignals(IN SITE_INFO *pSite);
static BOOL DHCPSetFunc(IN SITE_INFO *pSite);
static BOOL Equip_UpdateActiveAlarmCnt(IN EQUIP_INFO *pEquip, IN int *piSiteCnt);
static BOOL Site_UpdateActiveAlarmCnt(IN SITE_INFO *pSite);
static BOOL StdEquip_DisplaySignalsValue(IN SITE_INFO *pSite);
static BOOL Equip_ProcessRectifierName(IN OUT EQUIP_INFO *pEquip, IN OUT int iPositionNumber);
static char* Equip_MakeNewRectName(IN OUT char *ptrEquipName, IN int iPositionNumber);
static int RemoveNumber(char *buf);
static SIG_BASIC_VALUE *Equip_GetSpecialSigByMergedId(IN EQUIP_INFO *pEquip, 
						      IN int nMergedId);
static BOOL StdEquip_ProcessLVD3Signals(IN SITE_INFO *pSite);
static BOOL DHCPEnableFunc(IN SITE_INFO *pSite);
// timer id and interval to process delaying alarms.
#define TIMER_ID_PROCESS_DELAYING_ALARMS		2000

// the unit of alarm delay is seconds.
#define TIMER_INTERVAL_PROCESS_DELAYING_ALARMS	1000	//ms. 

#define IB4_COMM_NORMAL		0
#define IB4_COMM_FAIL		1

#define SIG_ID_TEMP_AT_1	111
#define SIG_ID_TEMP_AT_2	224
#define SIG_ID_TEMP_AT_3	301
#define NUM_OF_TEMPS		95

#define SYSTEM_UNIT				1
/*==========================================================================*
 * FUNCTION : Equip_ProcessRectBasicSignals
 * PURPOSE  : 1. calculate the value for virtual signal.
 *            2. calculate the statistic data of sampling signals 
 *            3. calculate the settable/controllable expression
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : always return TRUE. due to we need to continue to
 *            process the alarm of the equipment. the conditions of the alarm 
 *            may be changed by other equipment or by operator/service.
 * COMMENTS : 
 * CREATOR  : LXD          DATE: 2007-02-06 15:44
 *==========================================================================*/
//static BOOL Equip_ProcessRectBasicSignals(IN SITE_INFO *pSite,
//										  IN OUT EQUIP_INFO *pEquip)
//{
//	int					nNotificationMasks = 0;
//	//BOOL				bNeedProcess = FALSE;
//	//int				iCurrRectNum;
//	//int				i;
//	EQUIP_PROCESS_CMD	cmd;
//
//	// 1.check the comm status of the equipment
//	Equip_ProcessCommStatus(pSite, pEquip);
//
//	// 2.process basic data
//	if (Equip_ProcessSamplingSignals(pSite, pEquip))
//	{
//		nNotificationMasks |= _SAMPLE_MASK;
//	}
//
//	// 3.convert serial num
//	Equip_ProcessSerialNumber(pSite, pEquip);
//
//	// process settings data
//	if (Equip_ProcessSettingSignals(pSite, pEquip))
//	{
//		nNotificationMasks |= _SET_MASK;
//	}
//
//	// 4.process control signal
//	if (Equip_ProcessControlSignals(pSite, pEquip))
//	{
//		nNotificationMasks |= _CONTROL_MASK;
//	}
//
//
//         // need process data to notify the data processor
//	cmd.nCmd   = PROCESS_CMD_SIG_DATA_CHANGED;
//	cmd.pEquip = pEquip;
//	Queue_Put(pSite->hqProcessCommand, &cmd, FALSE);
//	
//	return TRUE;
//}


/*==========================================================================*
 * FUNCTION : Site_EquipmentManager
 * PURPOSE  : data processor
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 16:56
 *==========================================================================*/
DWORD Site_EquipmentManager(IN OUT SITE_INFO *pSite)
{
	HANDLE				hSelf = RunThread_GetId(NULL);
	BOOL				bDataChanged;

	AppLogOut(EQP_MGR, APP_LOG_UNUSED,//APP_LOG_UNUSED,
		"Equipment manager task now is starting.\n");

	//1. set the timer to process delaying alarms.
	Timer_Set(hSelf, TIMER_ID_PROCESS_DELAYING_ALARMS, 
		TIMER_INTERVAL_PROCESS_DELAYING_ALARMS,
		(ON_TIMER_PROC)Site_OnTimer,
		(DWORD)pSite);

	//2. main loop of processing data
	while (THREAD_IS_RUNNING(hSelf))
	{		
		RunThread_Heartbeat(hSelf);	//heartbeat

		// 1. process the sampling data of all equipment
		bDataChanged = Site_ProcessBasicSignals(pSite);

		// 2. process alarm: if some equipment data is changed, 
		//    to calculate the alarm
		// For the event level can be changed on-line by LCD and Web
		//printf("bDataChanged = %d\n", bDataChanged);
		if (bDataChanged)
		{
			RunThread_Heartbeat(hSelf);	//heartbeat
			bDataChanged = Site_ProcessAlarmSignals(pSite);
		}

		// 3. always to process the delaying alarms
		RunThread_Heartbeat(hSelf);	//heartbeat
		if (pSite->bProcessDelayingAlarms)
		{
			bDataChanged |= Site_ProcessDelayingAlarms(pSite);
			pSite->bProcessDelayingAlarms = FALSE;
		}

		// 4. process alarm suppression
		if (bDataChanged)
		{
			RunThread_Heartbeat(hSelf);	//heartbeat
			bDataChanged |= Site_ProcessAlarmSuppressions(pSite);
		}

		// 5. process alarm relay relay error output two led union light changed 2007/02/06
		//if (bDataChanged)
		{
			RunThread_Heartbeat(hSelf);	//heartbeat
			bDataChanged |= Site_ProcessAlarmRelays(pSite);
		}

		//Count the alarm number every loop.
		Site_UpdateActiveAlarmCnt(pSite);

		//6. at last reset the processing state to IDLE.
		RunThread_Heartbeat(hSelf);	//heartbeat
		Site_ResetProcessingState(pSite);

		//7. to avoid there are the recursive signals in the evaluation exp,
		// which will cause the task enter to a continuously data processing.
		// sleep a while.
		Sleep(SLEEP_TIME_BEFORE_NEXT_LOOP);

		//8.USED to Clear ACD/DCD/BAT Alarm Status
	}

	// kill the timer to process delaying alarms.
	Timer_Kill(hSelf, TIMER_ID_PROCESS_DELAYING_ALARMS);

	
	AppLogOut(EQP_MGR, APP_LOG_UNUSED,//APP_LOG_UNUSED,
		"Equipment manager task now exited.\n");

	return 0;
}


/*==========================================================================*
 * FUNCTION : Site_OnTimer
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE     hself   : 
 *            int        idTimer : 
 *            SITE_INFO  *pSite  : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-29 18:36
 *==========================================================================*/
static int Site_OnTimer(HANDLE hself, int idTimer, SITE_INFO *pSite)
{
	UNUSED(hself);

	//TRACEX("On timer %d, at %lf\n", idTimer, GetCurrentTime());

	if (idTimer == TIMER_ID_PROCESS_DELAYING_ALARMS)
	{
		pSite->bProcessDelayingAlarms = TRUE;
	}

	return TIMER_CONTINUE_RUN;
}

static BOOL Equip_ProcessBasicSignals(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip);
static BOOL Equip_ProcessAlarmSignals(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip);
static BOOL Equip_ProcessDelayingAlarms(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip);

static BOOL Equip_ProcessAlarmSuppressions(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip);
static BOOL Equip_SaveActiveAlarms(IN SITE_INFO *pSite,
									   IN EQUIP_INFO *pEquip);

//#define			EQUIP_DATA_CHANGE_FLAG		1
//#define			EQUIP_ALARM_CHANGE_FLAG		0
/*==========================================================================*
 * FUNCTION : Equip_ProcessSignalsForHLMS
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO  *pEquip         : 
 *            IN int         iInfoChangeType : 1:DATA Change Flag  
 *                                             0:ALARM Change Flag
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2006-06-19 10:33
 *==========================================================================*/
//static BOOL Equip_ProcessSignalsForHLMS(IN EQUIP_INFO *pEquip, IN int iChangeType)
//{
//	VAR_VALUE_EX value_ex;
//	int nError,nBufLen,nTimeOut;
//	int nInterfaceType;
//	int nSubVarID;
//	int nEquipID;
//
//	if(!pEquip)
//	{
//		return FALSE;
//	}
//	
//	nTimeOut = 5000;
//	nError = 0;
//	nBufLen = sizeof(VAR_VALUE_EX);
//	nInterfaceType = VAR_A_SIGNAL_VALUE;
//	nEquipID = DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);
//	value_ex.nSenderType      = SERVICE_OF_LOGIC_CONTROL;
//	value_ex.nSendDirectly	  = EQUIP_CTRL_SEND_CHECK_VALUE;
//	value_ex.pszSenderName    = "EQUIP_PROC";
//	
//
//	if(iChangeType)
//	{
//		value_ex.varValue.ulValue = 0x10; //0001'0000
//	}
//	else  //Equip Alarm Changed
//	{
//		value_ex.varValue.ulValue = 0x01; //0000'0001
//	}
//
//	switch(pEquip->iEquipTypeID)
//	{
//		case STD_ID_ACU_SYSTEM_EQUIP:
//			nSubVarID = SIG_ID_SYS_DATA_FLAG;
//			break;
//		case RECT_STD_EQUIP_ID:
//			nSubVarID = SIG_ID_RECT_DATA_FLAG;
//			break;
//		case DCD_STD_EQUIP_ID:
//			nSubVarID = SIG_ID_DCD_DATA_FLAG;
//			break;
//		case ACD_STD_EQUIP_ID:
//			nSubVarID = SIG_ID_ACD_DATA_FLAG;
//			break;
//
//		default:
//			return FALSE;
//			//break;
//	}	
//
//	SIG_BASIC_VALUE  *pBv = NULL;
//
//	nError = DxiGetData(nInterfaceType,
//		nEquipID,	
//		nSubVarID,
//		&nBufLen,			
//		&pBv,			
//		nTimeOut);
//
//	if(pBv && (nError == ERR_OK))
//	{
//		value_ex.varValue.ulValue = (value_ex.varValue.ulValue | pBv->varValue.ulValue);
//	}
//
//	nBufLen = sizeof(VAR_VALUE_EX);
//	nError = DxiSetData(nInterfaceType,
//			nEquipID,			
//			nSubVarID,		
//			nBufLen,			
//			&value_ex,			
//			nTimeOut);
//
//	if (nError != ERR_OK)
//	{	
//		return FALSE;
//	}
//
//	return TRUE;
//}

#define			MAX_PROCESS_BASIC_SIG_TIMEOUT			5 //10s
/*==========================================================================*
 * FUNCTION : Site_ProcessBasicSignals
 * PURPOSE  : process the sampling data and virtual signal of all equipment
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : TRUE there are some equipment that data is changed.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-10 15:03
 *==========================================================================*/
static BOOL Site_ProcessBasicSignals(IN SITE_INFO *pSite)
{
	HANDLE				hSelf = pSite->hEquipManager;
	EQUIP_PROCESS_CMD	cmd;
	BOOL				bDataChanged = FALSE;
	time_t				tmNow, tmProcessSigTime;
	//EQUIP_INFO	*pEquip;
	//int		i;


	// 1. to Process basic sampled data
	tmProcessSigTime = GetCurrentTime();

	while (THREAD_IS_RUNNING(hSelf) 
		&& (Queue_Get(pSite->hqProcessCommand, 
					&cmd,
					FALSE,
					WAIT_TIME_CHECKING_PROCESS_CMD)
			== ERR_QUEUE_OK))
	{
		

		RunThread_Heartbeat(hSelf);	//heartbeat

		if (cmd.nCmd == PROCESS_CMD_SIG_DATA_CHANGED)
		{
			//G3_OPT [run time], by Lin.Tao.Thomas, 2013-4-3
			//skip already processed refered equip, to increase performace
			if (cmd.pEquip->iProcessingState == EQP_STATE_PROCESS_DOING)
			{
				continue;
			}

			//pEquip->iProcessingState = EQP_STATE_PROCESS_PENDING;
			bDataChanged |= Equip_ProcessBasicSignals(pSite, cmd.pEquip);

			// need process alarm even if the data of itself is not changed.
			// because the alarm expression may refer to other equipment which
			// data may be changed.
			cmd.pEquip->iProcessingState = EQP_STATE_PROCESS_DOING;

			//scu plus add for ydn23 
			 //Equip_ProcessSignalsForHLMS(cmd.pEquip, EQUIP_DATA_CHANGE_FLAG);
		}

		// set all equipment process state to EQP_STATE_PROCESS_DOING.
		// if the std alarm level is changed by UI.
		else if ((cmd.nCmd == PROCESS_CMD_ALARM_LEVEL_CHANGED) ||
			(cmd.nCmd == PROCESS_CMD_ALARM_RELAY_CHANGED))
		{
			int				i, nStdEquipId;
			EQUIP_INFO			*pEquip;

			// the cmd.pEquip is the StdEquipTypeID, not EQUIP_INFO!
			nStdEquipId = (int)cmd.pEquip;

			pEquip = pSite->pEquipInfo;
			for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
			{
				// force the equipment to process alarm.
				if ((int)pEquip->pStdEquip->iTypeID == nStdEquipId)
				{
					if(cmd.nCmd == PROCESS_CMD_ALARM_LEVEL_CHANGED)
					{
						pEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
					}
					else
					{
						pEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
						Site_ProcessAlarmRelays(pSite);
					}
					//scu plus add for ydn23
					//Equip_ProcessSignalsForHLMS(pEquip, EQUIP_ALARM_CHANGE_FLAG);
				}
			}

			bDataChanged = TRUE;
		}
		else 
		{
			;
		}

		double	dDiffTime = GetCurrentTime() - tmProcessSigTime;
		if((dDiffTime > MAX_PROCESS_BASIC_SIG_TIMEOUT)	//time out
			|| (dDiffTime < 0))							//time changed
		{
			tmProcessSigTime = GetCurrentTime();
			break;
		}		
	}

	// 1.1. Process the special signals every time
	RunThread_Heartbeat(hSelf);	//heartbeat
	bDataChanged |= Site_ProcessSpecialSignals(pSite);

	// 2. test it is the time to save stat data?
	tmNow = time(NULL)%SEC_PER_DAY;
	if (!pSite->bStatDataSaved)
	{
		if ((tmNow >= pSite->tmStatDataReportSchedule) 
			&& (tmNow < pSite->tmStatDataReportSchedule + STAT_DATA_REPORT_OVERTIME))
		{
			pSite->bStatDataSaved = Site_SaveStatData(pSite);
			if (pSite->bStatDataSaved)
			{
				Site_ResetStatData(pSite);	//saved OK, reset stat data.
			}
		}
	}
	else	// to set the reported flag to FALSE if the time is passed.
	{
		if (tmNow 
			>= pSite->tmStatDataReportSchedule + STAT_DATA_REPORT_OVERTIME)
		{
			if (!pSite->bStatDataSaved)
			{
				Site_ResetStatData(pSite);	// data still not saved? reset!
			}

			pSite->bStatDataSaved = FALSE;
		}
	}
	
	return bDataChanged;
}



/*==========================================================================*
 * FUNCTION : Equip_ResetStatData
 * PURPOSE  : reset the stat data after the data is saved.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-16 00:35
 *==========================================================================*/
static void Equip_ResetStatData(IN OUT SITE_INFO *pSite, 
								IN OUT EQUIP_INFO *pEquip)
{
	int		nSig;

	UNUSED(pSite);

	// 1. check the sampling signals
	if (pEquip->pSampleSigValue != NULL)
	{
		SAMPLE_SIG_VALUE *pSig;
		
		pSig = pEquip->pSampleSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iSampleSigNum; nSig++, pSig++)
		{
			if (pSig->sv.pStatValue != NULL)
			{
				// init the max and min value
				memset(pSig->sv.pStatValue, 0, sizeof(SIG_STAT_VALUE));
				pSig->sv.pStatValue->varMaxValue.fValue = MINFLOAT;
				pSig->sv.pStatValue->varMinValue.fValue = MAXFLOAT;
			}
		}
	}
}



/*==========================================================================*
 * FUNCTION : Site_ResetStatData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-16 00:35
 *==========================================================================*/
static void Site_ResetStatData(IN OUT SITE_INFO *pSite)
{
	int				n;
	EQUIP_INFO		*pEquip;

	for (pEquip = pSite->pEquipInfo, n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		Equip_ResetStatData(pSite, pEquip);
	}

	return;
}


/*==========================================================================*
 * FUNCTION : Equip_SaveStatData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite   : 
 *            IN OUT EQUIP_INFO  *pEquip  : 
 *            time_t             tmSaving : 
 * RETURN   : static BOOL :  
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-16 00:42
 *==========================================================================*/
static BOOL Equip_SaveStatData(IN OUT SITE_INFO *pSite, 
								IN OUT EQUIP_INFO *pEquip, time_t tmSaving)
{
	int		nSig;
	SAMPLE_SIG_VALUE *pSig;
	HIS_STAT_RECORD	data;
	SIG_STAT_VALUE		*pStatValue;

	// 1. check the sampling signals
	if (pEquip->pSampleSigValue == NULL)
	{
		return TRUE;	// nothing need to be saved,
	}

	pSig = pEquip->pSampleSigValue;
	for (nSig = 0; nSig < pEquip->pStdEquip->iSampleSigNum; nSig++, pSig++)
	{
		if (pSig->sv.pStatValue != NULL)
		{
			pStatValue = pSig->sv.pStatValue;

			data.iEquipID	= pEquip->iEquipID;
			data.iSignalID	= DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 
				pSig->pStdSig->iSigID);

			data.tmStatTime		 = tmSaving;

			data.varMaxValue	 = pStatValue->varMaxValue;
			data.tmMaxValue		 = pStatValue->tmMaxValue;
			
			data.varMinValue	 = pStatValue->varMinValue;
			data.tmMinValue		 = pStatValue->tmMinValue;
			
			data.varCurrentVal	 = pSig->bv.varValue;
			data.varAverageValue = pStatValue->varAverageValue;

			if (!DAT_StorageWriteRecord(pSite->hStatData,
				sizeof(data), (void *)&data))
			{
				TRACEX("Saving stat data for %s(%d)... FAILED.\n", 
					EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID);
				return FALSE;
			}			
			
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Site_SaveStatData
 * PURPOSE  : The stat data is required in Chinese Market. In here, the stat
 *            data is only saved. the real reporting action depends on the app
 *            service that is required to report data, the app service can 
 *            read the stat data from the storage and report it out. 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : always TRUE. due to we have no way to continue 
 *            process the case if fails on saving stat data.
 * COMMENTS : the data only saved.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-15 23:43
 *==========================================================================*/
static BOOL Site_SaveStatData(IN SITE_INFO *pSite)
{
	time_t	tmNow;
	int				n;
	EQUIP_INFO		*pEquip;

	// save stat data
	if ((pSite->hStatData == NULL) || 
		pSite->bAlarmOutgoingBlocked) // do NOT record on alarm outging blocked
	{
		return TRUE;
	}

	tmNow = time(NULL);

	for (pEquip = pSite->pEquipInfo, n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{		
		if (!Equip_SaveStatData(pSite, pEquip, tmNow))
		{
			// need log the event out?
			break;
		}
	}

	return TRUE;	// always return TRUE.
}


/*==========================================================================*
 * FUNCTION : Equip_ProcessSerialNumber
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-10 17:40
 *==========================================================================*/
static BOOL Equip_ProcessSerialNumber(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip)
{
	
	SIG_BASIC_VALUE		*pSig = (SIG_BASIC_VALUE *)pEquip->pSerialSigRef;
	SIG_BASIC_VALUE		*pSig2 = (SIG_BASIC_VALUE *)pEquip->pSerialSigRef2;

	UNUSED(pSite);

	//printf("\nEquip_ProcessSerialNumber  = %u\n", pSig->varValue.ulValue);
	/*if(pSig2 != NULL)	
	{
		TRACE("\nEquip_ProcessSerialNumber EquipID[%d] = [%u] [%u]  [%d][%s]\n", pEquip->iEquipID ,pSig2->varValue.ulValue, pSig2->varLastValue.ulValue, pEquip->iEquipID ,pEquip->szSerialNumber);	
		TRACE("\nEquip_ProcessSerialNumber EquipID[%d] = [%u][%u]\n",  pEquip->iEquipID ,pSig->varValue.ulValue, pSig->varLastValue.ulValue);	
	}*/
	if ( (pSig != NULL) && SIG_VALUE_IS_VALID((SAMPLE_SIG_VALUE *)pSig)  && ((pSig->varValue.ulValue != pSig->varLastValue.ulValue)) &&
		(pSig2 != NULL) && SIG_VALUE_IS_VALID((SAMPLE_SIG_VALUE *)pSig2)  && ((pSig2->varValue.ulValue != pSig2->varLastValue.ulValue)))
	{
		//changed by YangGuoxin.For rectifier id .
		
		pEquip->pStdEquip->pfnSerialConvertProc((char *)&pSig->varValue.ulValue,(char *)&pSig2->varValue.ulValue,
										sizeof(pSig->varValue.ulValue),
										pEquip->szSerialNumber, 
										sizeof(pEquip->szSerialNumber));
		TRACE("iEquipID = %d SerialNumber = %s\n",pEquip->iEquipID ,pEquip->szSerialNumber);
		/*pEquip->pStdEquip->pfnSerialConvertProc((char *)&pEquip->iEquipID,
										sizeof(pSig->varValue.ulValue),
										pEquip->szSerialNumber, 
										sizeof(pEquip->szSerialNumber));
	*/	return TRUE;
	}

	return FALSE;
}
static BOOL Equip_RefreshSerialNumber(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip)
{
	
	SIG_BASIC_VALUE		*pSig = (SIG_BASIC_VALUE *)pEquip->pSerialSigRef;
	SIG_BASIC_VALUE		*pSig2 = (SIG_BASIC_VALUE *)pEquip->pSerialSigRef2;

	UNUSED(pSite);
	
	if ( (pSig != NULL) && SIG_VALUE_IS_VALID((SAMPLE_SIG_VALUE *)pSig)   &&
		(pSig2 != NULL)  )
	{
		//changed by YangGuoxin.For rectifier id .
		
		pEquip->pStdEquip->pfnSerialConvertProc((char *)&pSig->varValue.ulValue,(char *)&pSig2->varValue.ulValue,
										sizeof(pSig->varValue.ulValue),
										pEquip->szSerialNumber, 
										sizeof(pEquip->szSerialNumber));
		return TRUE;
	}
	
	return FALSE;
}
#define			BATTERY_TYPE_ID			301

/*==========================================================================*
 * FUNCTION : Equip_ProcessCommStatus
 * PURPOSE  : get the comm status of the equipment if configured
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-05 20:18
 *==========================================================================*/
static BOOL Equip_ProcessCommStatus(IN SITE_INFO *pSite,
									IN OUT EQUIP_INFO *pEquip)
{
	
	//static int s_iWorkStatusLastStatus = 0;
	
	
	SIG_BASIC_VALUE		*pCommSig = (SIG_BASIC_VALUE *)pEquip->pCommStatusSigRef;

	SIG_BASIC_VALUE		*pWorkSig = (SIG_BASIC_VALUE *)pEquip->pWorkStatusSigRef;
	UNUSED(pSite);

	
	if (pCommSig)
	{		
		//All Equip
		pEquip->bCommStatus = (SIG_VALUE_IS_VALID((SAMPLE_SIG_VALUE *)pCommSig) 
							&& (pCommSig->varValue.ulValue == 0)) ? TRUE : FALSE;	
	}
	if (pWorkSig)
	{		
		//All Equip
		pEquip->bWorkStatus = (SIG_VALUE_IS_VALID((SAMPLE_SIG_VALUE *)pWorkSig) 
							&& (pWorkSig->varValue.ulValue == 0)) ? TRUE : FALSE;	

		/*
		if( 1801 == pEquip->iEquipID)
		{
			if( s_iWorkStatusLastStatus != pEquip->bWorkStatus)
			{
				printf("DataProcess: Equip_ID = %d,s_iWorkStatusLastStatus=%d, pEquip->bWorkStatus = %d,time(NULL)=%d~~\n", pEquip->iEquipID,s_iWorkStatusLastStatus ,pEquip->bWorkStatus,time(NULL));
				AppLogOut("Equip_ProcessCommStatus", APP_LOG_INFO, "DataProcess: Equip_ID = %d,s_iWorkStatusLastStatus=%d, pEquip->bWorkStatus = %d,time(NULL)=%d~~\n", pEquip->iEquipID,s_iWorkStatusLastStatus ,pEquip->bWorkStatus,time(NULL));
			}
			s_iWorkStatusLastStatus = pEquip->bWorkStatus;
		}*/
	}
	
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : Equip_ProcessBasicSignals
 * PURPOSE  : 1. calculate the value for virtual signal.
 *            2. calculate the statistic data of sampling signals 
 *            3. calculate the settable/controllable expression
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : always return TRUE. due to we need to continue to
 *            process the alarm of the equipment. the conditions of the alarm 
 *            may be changed by other equipment or by operator/service.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 15:44
 *==========================================================================*/
static BOOL Equip_ProcessBasicSignals(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip)
{
	int					nNotificationMasks = 0;
	//BOOL				bNeedProcess = FALSE;
	//int					iCurrRectNum;
	int					i;
	EQUIP_PROCESS_CMD	cmd;
	EQUIP_INFO			*pEquipReferredThis;

	// 1.check the comm status of the equipment
	Equip_ProcessCommStatus(pSite, pEquip);

	// 2.process basic data
	if (Equip_ProcessSamplingSignals(pSite, pEquip))
	{
		nNotificationMasks |= _SAMPLE_MASK;
	}

	// 3.convert serial num
	Equip_ProcessSerialNumber(pSite, pEquip);

	// process settings data
	if (Equip_ProcessSettingSignals(pSite, pEquip))
	{
		nNotificationMasks |= _SET_MASK;
	}

	// 4.process control signal
	if (Equip_ProcessControlSignals(pSite, pEquip))
	{
		nNotificationMasks |= _CONTROL_MASK;
	}


	// data not changed. not need notify the service and other equipment.
	if (nNotificationMasks == 0)
	{
		return TRUE;	// to forcibly process alarm.
	}


	// 5.post msg to nofity the related equipment to process data.
	// send the notification to the related equipment to process data

	/*if((pEquip->iEquipID == SYS_EQUIP_ID) 
		|| (pEquip->iEquipID == RECTG_EQUIP_ID)
		|| (pEquip->iEquipID == ACDG_EQUIP_ID) 
		|| (pEquip->iEquipID == DCDG_EQUIP_ID)
		|| (pEquip->iEquipID == BATG_EQUIP_ID))
	{
	      iCurrRectNum = RECT_GetTotalRectNum();
	      if(RECT_GetSamplingCommInfo()->iLastRectNum > iCurrRectNum)
	      {
		    iCurrRectNum = RECT_GetSamplingCommInfo()->iLastRectNum;
		    RECT_GetSamplingCommInfo()->iLastRectNum--;
	      }
	      else
	      {
		    RECT_GetSamplingCommInfo()->iLastRectNum = iCurrRectNum;
	      }

	      bNeedProcess = TRUE;
	}*/


	for (i = 0; i < pEquip->iEquipReferredThis; i++)
	{
		pEquipReferredThis = pEquip->ppEquipReferredThis[i];

		// send the msg to notify the equipment refers this equipment to
		// process data. but the notification will NOT be sent if the
		// peer equipment is processing data. also can avoid the recursive
		// reference in the equipment.
		if (pEquipReferredThis->iProcessingState == EQP_STATE_PROCESS_IDLE)
		{
			/*if(bNeedProcess && (pEquipReferredThis->iEquipTypeID == RECT_BASE_TYPE_ID) 
				&&(pEquipReferredThis->iEquipID > (iCurrRectNum + 2)))

			{
				continue;
			}*/
			// need process data to notify the data processor
			cmd.nCmd   = PROCESS_CMD_SIG_DATA_CHANGED;
			cmd.pEquip = pEquipReferredThis;
			Queue_Put(pSite->hqProcessCommand, &cmd, FALSE);

		}
	}

	// 6.send notification to application serivces
	// nNotificationMasks: combination of _SAMPLE_MASK, _SET_MASK,_CONTROL_MASK
	// send the EQUIP_INFO address.
	if ((nNotificationMasks & _SAMPLE_MASK) == _SAMPLE_MASK)
	{
		NotificationFunc(_SAMPLE_MASK,	// masks
			sizeof(EQUIP_INFO *),				// the sizeof (EQUIP_INFO *)
			&pEquip,							// the equipment address ref.
			FALSE);								// not urgent
	}

	if ((nNotificationMasks & _SET_MASK) == _SET_MASK)
	{
		NotificationFunc(_SET_MASK,	// masks
			sizeof(EQUIP_INFO *),				// the sizeof (EQUIP_INFO *)
			&pEquip,							// the equipment address ref.
			FALSE);								// not urgent
	}

	if ((nNotificationMasks & _CONTROL_MASK) == _CONTROL_MASK)
	{
		NotificationFunc(_CONTROL_MASK,	// masks
			sizeof(EQUIP_INFO *),				// the sizeof (EQUIP_INFO *)
			&pEquip,							// the equipment address ref.
			FALSE);								// not urgent
	}

	return TRUE;
}


__INLINE static BOOL Equip_ProcessRectifierName(IN OUT EQUIP_INFO *pEquip, IN OUT int iPositionNumber)
{
	char *ptrEquipName = NULL;	

	ptrEquipName = pEquip->pEquipName->pFullName[0];
	Equip_MakeNewRectName(ptrEquipName, iPositionNumber);
	//printf("pFullName ptrEquipName = %s\n", ptrEquipName);

	ptrEquipName =  pEquip->pEquipName->pFullName[1];
	Equip_MakeNewRectName(ptrEquipName, iPositionNumber);
	//printf("pFullName ptrEquipName = %s\n", ptrEquipName);

	ptrEquipName = pEquip->pEquipName->pAbbrName[0];
	Equip_MakeNewRectName(ptrEquipName, iPositionNumber);
	//printf("pAbbrName ptrEquipName = %s\n", ptrEquipName);

	ptrEquipName = pEquip->pEquipName->pAbbrName[1];
	Equip_MakeNewRectName(ptrEquipName, iPositionNumber);
	//printf("pAbbrName ptrEquipName = %s\n", ptrEquipName);

	return TRUE;
}

__INLINE static char* Equip_MakeNewRectName(IN OUT char *ptrEquipName, IN int iPositionNumber)
{
	int	i;
	char	*ptrTempEquipName = ptrEquipName;
	i = RemoveNumber(ptrTempEquipName);
	snprintf(ptrTempEquipName + i,5, "#%d", iPositionNumber);

	return ptrTempEquipName;
}

__INLINE static int Equip_GetRectID(IN OUT char *ptrEquipName)
{
#define IS_SHARP(c)			((c) == 0x23)
	int	i;
	char *p = ptrEquipName;
	while (*p)
	{
		if(IS_SHARP(*p))
		{
			p++;
			return atoi(p);
		}
		p++;

	}
	return 0;
}
__INLINE static int RemoveNumber(char *buf)
{
#define IS_NUMBER(c)								(((c)>=(0x30) && (c)<=(0x39)) || (c) == 0x23)
	char *p=buf;
	/*char pfinal[30]="";*/
	int i=0;
	while(*p)
	{
		if(IS_NUMBER(*p)) 
		{
			p=p++;
			break;
		}
		else
		{
			//pfinal[i]=*p;
			p++;
			i++;
		}
	}
	//p=strdup(pfinal);
	return i;
}
/*==========================================================================*
 * FUNCTION : Equip_SetSignalValue
 * PURPOSE  : Set the signal value from the raw data.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO           *pEquip   : 
 *            IN OUT SIG_BASIC_VALUE  *pSig     : 
 *			  IN float				  fNewValue : new value will be set to
 *            IN float                fMinValue : 
 *            IN float                fMaxValue : 
 * RETURN   : __INLINE static BOOL: data changed
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 18:33
 *==========================================================================*/
__INLINE static BOOL Equip_SetSignalValue(IN EQUIP_INFO *pEquip,
										  IN OUT SIG_BASIC_VALUE *pSig,
										  IN float fNewValue,
										  IN float fMinValue,
										  IN float fMaxValue)
{
	//Optimized by Thomas after code review with David, 2013-7-19

	//Two special logic: avoid precision losing, communication status judge rule

	BOOL bDataChanged = FALSE;
	BOOL bSpecial = FALSE;      //Special logic for rectifier inventory signals
	BOOL bExceedLimit = FALSE;

	//Minor performance improvement
	BOOL bEquipIsRect = FALSE;
	int iSigID = -1;  
	if ((pEquip->iEquipTypeID == RECT_STD_EQUIP_ID) 
		||(pEquip->iEquipTypeID == RECT_STD_EQUIP_ID_SLAVE1)
		||(pEquip->iEquipTypeID == RECT_STD_EQUIP_ID_SLAVE2)
		||(pEquip->iEquipTypeID == RECT_STD_EQUIP_ID_SLAVE3)) 
	{
		bEquipIsRect = TRUE;
	}

	//Specically process for Rectifier S/N, part number, version signals, always valid
	//since they are stored in the controller during AutoConfig.
	if (bEquipIsRect && (pSig->ucSigType == SIG_TYPE_SAMPLING))
	{
		iSigID = ((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID;

		if ((iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_SN)) ||
			(iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_PARTNUMBER)) ||
			(iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_VER)))
		{
			bSpecial = TRUE;
		}
	}

	//if (bEquipIsRect &&		// rect S/N
	//	(pSig->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_RECT_SN)) &&   //ensure sig type is sampling
	//	(((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_SN)))
	//{
	//	bRectSpecial = TRUE;
	//}
	//The sig removed in system already.
	//else if ((pEquip->iEquipTypeID == STD_ID_ACU_SYSTEM_EQUIP) &&		// system time
	//	((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID == 
	//	DXI_SPLIT_SIG_MID_ID(SIG_ID_SYSTEM_TIME) &&
	//	(pSig->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_SYSTEM_TIME)))
	//{
	//	bSpecial = TRUE;
	//}
//#ifdef PRODUCT_INFO_SUPPORT
//	//added by Thomas, 2005-12
//	else if (bEquipIsRect &&		// rect part number
//		(pSig->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_RECT_PARTNUMBER)) &&  //ensure sig type is sampling
//		(((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_PARTNUMBER)))
//	{
//		bSpecial = TRUE;
//	}
//	else if (bEquipIsRect &&		// rect version
//		(pSig->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_RECT_VER)) &&    //ensure sig type is sampling
//		(((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_VER)))
//	{
//		bSpecial = TRUE;
//	}
//#endif//PRODUCT_INFO_SUPPORT
//	//}}


	// add process communication status. maofuhua, 2005-4-5
	// Some signals should be valid.
	if ((!pEquip->bCommStatus )
		&& (pSig->ucSigType == SIG_TYPE_SAMPLING)
		&& (pSig != (SIG_BASIC_VALUE *)pEquip->pCommStatusSigRef)
		&& (pSig != (SIG_BASIC_VALUE *)pEquip->pWorkStatusSigRef))
	{
		//if(pEquip->pStdEquip->iTypeID == RECT_STD_EQUIP_ID && pSig == pEquip->pRectACStatus)
		if(bEquipIsRect && pSig == (SIG_BASIC_VALUE *)pEquip->pRectACStatus)  //for single-power-rectifer alarm process
		{
			;
		}
		else
		{
			SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID_RAW);
		}
	}


//#define USING_BOLD_METHOD
	// set the signal value from raw data if raw is valid
	if (SIG_STATE_TEST(pSig, VALUE_STATE_IS_VALID_RAW)
			|| bSpecial)  
	{
		// save old value.
		pSig->varLastValue = pSig->varValue;

#ifdef USING_BOLD_METHOD
		// always check the value range
/*		if((fMinValue > fNewValue)
					|| (fNewValue > fMaxValue))
		{
			SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID);
		}
		else
		{*/	
			if (pSig->ucType == VAR_FLOAT)
			{
				pSig->varValue.fValue = fNewValue;
			}
			else if (bSpecial)   //avoid precision losing
			{
				pSig->varValue.lValue = *(long *)&fNewValue;
			}
			else //VAR_ENUM etc should set directly
			{
				//note: currently in all sampler libs, the "int" value is used as "float" value in union directly,
				//after this bug fixed up, we could recover to the directly setting.
				//pSig->varValue.lValue = (long)fNewValue;  
				pSig->varValue.lValue = *(long *)&fNewValue;
			}

			// set the sig state to valid
			SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);

		/*}*/
#else

		//set float value
		if (pSig->ucType == VAR_FLOAT)
		{
			if((fMinValue <= fNewValue)	// validate range.
					&& (fNewValue <= fMaxValue))
			{
				pSig->varValue.fValue = fNewValue;
			}
			else
			{
				bExceedLimit = TRUE;
			}
		}
		else
		{
			long	lValue = *(long *)&fNewValue;
			

			if(bSpecial)
			{
				pSig->varValue.lValue = lValue;			
			}
			else if(pSig->ucType == VAR_UNSIGNED_LONG)//ULONG�������⴦��
			{
				unsigned long	ulValue = *(unsigned long *)&fNewValue;
				if((fMinValue <= (float)ulValue)	// validate range.
						&& ((float)ulValue <= fMaxValue))
				{
					pSig->varValue.lValue = lValue;
				}
				else
				{
					bExceedLimit = TRUE;
				}

			}
			else if ((pSig->ucType == VAR_LONG)
				|| (pSig->ucType == VAR_ENUM))
			{
				if((fMinValue <= (float)lValue)	// validate range.
						&& ((float)lValue <= fMaxValue))
				{
					pSig->varValue.lValue = lValue;
				}
				else
				{
					bExceedLimit = TRUE;
				}

			}
			else
			{
				pSig->varValue.lValue = lValue;
			}
		}

		if(bExceedLimit)
		{
			SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID);
		}
		else
		{
			// set the sig state to valid
			SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);
		}
#endif

		// 2. test data is changed or not.
		// we DO NOT consider the invalid/valid state of the signal
		// all data use the integer type in comparison
		if (pSig->varValue.lValue != pSig->varLastValue.lValue)
		{
			bDataChanged = TRUE;
		}
	}

	// clear the valid state of the signal if raw is invalid.
	else if (SIG_VALUE_IS_VALID(pSig))// last is valid, but now is invalid.
	{
		// do NOT change the last value.
		//pSig->varLastValue = pSig->varValue;
		SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID);
		bDataChanged = TRUE;
	}


	return bDataChanged;
}

/*==========================================================================*
 * FUNCTION : Equip_SetSignalValue_new
 * PURPOSE  : Set the signal value from the raw data.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO           *pEquip   : 
 *            IN OUT SIG_BASIC_VALUE  *pSig     : 
 *			  IN float				  fNewValue : new value will be set to
 *            IN float                fMinValue : 
 *            IN float                fMaxValue : 
 * RETURN   : __INLINE static BOOL: data changed
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 18:33
 *==========================================================================*/
#if  1		
__INLINE static BOOL Equip_SetSignalValue_new(IN EQUIP_INFO *pEquip,
										  IN OUT SIG_BASIC_VALUE *pSig,
										  IN float fNewValue,
										  IN float fMinValue,
										  IN float fMaxValue)
{
	BOOL bDataChanged = FALSE;
	BOOL bSpecial = FALSE;
	BOOL bExceedLimit = FALSE;
	//{{ specically process the time and Rect S/N due to avoid losing
	// the precision of these integer variable.
	if (((pEquip->iEquipTypeID == RECT_STD_EQUIP_ID) ||(pEquip->iEquipTypeID == 1601)||(pEquip->iEquipTypeID == 1701)||(pEquip->iEquipTypeID == 1801)) &&		// rect S/N
		(((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID == 
		DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_SN)) &&
		(pSig->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_RECT_SN)))
	{
		bSpecial = TRUE;
	}
	else if ((pEquip->iEquipTypeID == STD_ID_ACU_SYSTEM_EQUIP) &&		// system time
		((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID == 
		DXI_SPLIT_SIG_MID_ID(SIG_ID_SYSTEM_TIME) &&
		(pSig->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_SYSTEM_TIME)))
	{
		bSpecial = TRUE;
	}
#ifdef PRODUCT_INFO_SUPPORT
	//added by Thomas, 2005-12
	else if (((pEquip->iEquipTypeID == RECT_STD_EQUIP_ID) ||(pEquip->iEquipTypeID == 1601)||(pEquip->iEquipTypeID == 1701)||(pEquip->iEquipTypeID == 1801)) &&		// rect part number
		(((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID == 
		DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_PARTNUMBER)) &&
		(pSig->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_RECT_PARTNUMBER)))
	{
		bSpecial = TRUE;
	}
	else if (((pEquip->iEquipTypeID == RECT_STD_EQUIP_ID) ||(pEquip->iEquipTypeID == 1601)||(pEquip->iEquipTypeID == 1701)||(pEquip->iEquipTypeID == 1801)) &&		// rect version
		(((SAMPLE_SIG_VALUE *)pSig)->pStdSig->iSigID == 
		DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_VER)) &&
		(pSig->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_RECT_VER)))
	{
		bSpecial = TRUE;
	}
#endif//PRODUCT_INFO_SUPPORT
	//}}


	// add process communication status. maofuhua, 2005-4-5
	if ((!pEquip->bCommStatus )
		&& (pSig->ucSigType == SIG_TYPE_SAMPLING)
		&& (pSig != (SIG_BASIC_VALUE *)pEquip->pCommStatusSigRef)
		&& (pSig != (SIG_BASIC_VALUE *)pEquip->pWorkStatusSigRef))
	{
		if(pEquip->pStdEquip->iTypeID == RECT_STD_EQUIP_ID && pSig == pEquip->pRectACStatus)
		{
			;
		}
		else
		{
			SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID_RAW);
		}
		
	}


	// set the signal value from raw data if raw is valid
	if (SIG_STATE_TEST(pSig, VALUE_STATE_IS_VALID_RAW)
			|| bSpecial)
	{
		// save old value.
		pSig->varLastValue = pSig->varValue;

		// set float value
		if (pSig->ucType == VAR_FLOAT)
		{
			if((fMinValue <= fNewValue)	// validate range.
					&& (fNewValue <= fMaxValue))
			{
				pSig->varValue.fValue = fNewValue;
			}
			else
			{
				bExceedLimit = TRUE;
			}
		}
		else
		{
			//long	lValue = *(long *)&fNewValue;
			long	lValue = (long)fNewValue;

			if(bSpecial)
			{
				pSig->varValue.lValue = lValue;			
			}
			else if ((pSig->ucType == VAR_UNSIGNED_LONG) 
				|| (pSig->ucType == VAR_LONG)
				|| (pSig->ucType == VAR_ENUM))
			{
				if((fMinValue <= (float)lValue)	// validate range.
						&& ((float)lValue <= fMaxValue))
				{
					pSig->varValue.lValue = lValue;
				}
				else
				{
					bExceedLimit = TRUE;
				}
			}
			else
			{
				pSig->varValue.lValue = lValue;
			}
		}

		if(bExceedLimit)
		{
			SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID);
		}
		else
		{
			// set the sig state to valid
			SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);
		}

		// 2. test data is changed or not.
		// we DO NOT consider the invalid/valid state of the signal
		// all data use the integer type in comparison
		if (pSig->varValue.lValue != pSig->varLastValue.lValue)
		{
			bDataChanged = TRUE;
		}
	}

	// clear the valid state of the signal if raw is invalid.
	else if (SIG_VALUE_IS_VALID(pSig))// last is valid, but now is invalid.
	{
		// do NOT change the last value.
		//pSig->varLastValue = pSig->varValue;
		SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID);
		bDataChanged = TRUE;
	}


	return bDataChanged;
}
#endif

/*==========================================================================*
 * FUNCTION : Equip_CheckSavingData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SAMPLE_SIG_VALUE  *pSig             : 
 * RETURN   : __INLINE static BOOL: TRUE for data need to save
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 18:47
 *==========================================================================*/
__INLINE static BOOL Equip_CheckSavingData(IN SAMPLE_SIG_VALUE *pSig)
{
	BOOL	bNeedSave = FALSE;

	if (pSig->pStdSig->iStoringInterval != 0)
	{
		time_t nDelta = pSig->bv.tmCurrentSampled - pSig->sv.tmLastSaved;

		// test the sampling time
		if ((nDelta >= pSig->pStdSig->iStoringInterval) || // elapsed
			(nDelta < 0))	// time changed.
		{
			pSig->sv.tmLastSaved = pSig->bv.tmCurrentSampled;
			bNeedSave = TRUE;	// need save.
		}
	}

	// 3.2 if the saving method is by fStoringThreshold(NOT 0), 
	if (FLOAT_NOT_EQUAL0(pSig->pStdSig->fStoringThreshold))
	{
		float	fDelta;
#define ULONG_DIFF(u1, u2)	(((u1) > (u2)) ? ((u1) - (u2)) : ((u2) - (u1)))
		// get the delta value between last saved value
		fDelta = (pSig->bv.ucType == VAR_FLOAT) 
			? (pSig->bv.varValue.fValue - pSig->sv.varLastSaved.fValue)
			: (pSig->bv.ucType == VAR_UNSIGNED_LONG) 
			? (float)ULONG_DIFF(pSig->bv.varValue.ulValue, 
				pSig->sv.varLastSaved.ulValue)
			: (float)(pSig->bv.varValue.lValue -
				pSig->sv.varLastSaved.lValue);// treated as INT

		// test the changed data
		if (ABS(fDelta) >= pSig->pStdSig->fStoringThreshold)
		{
			pSig->sv.varLastSaved = pSig->bv.varValue;
			bNeedSave = TRUE;	// need save.
		}
	}

	return bNeedSave;
}



/*==========================================================================*
 * FUNCTION : void Equip_StatSamplingSignalData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SAMPLE_SIG_VALUE  *pSig : 
 * RETURN   : __INLINE static : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 18:54
 *==========================================================================*/
__INLINE static void Equip_StatSamplingSignalData(IN OUT SAMPLE_SIG_VALUE *pSig)
{
	SIG_STAT_VALUE		*pStatValue;

	pStatValue = pSig->sv.pStatValue;
	
	// the maximum value
	if (pStatValue->varMaxValue.fValue < pSig->bv.varValue.fValue)
	{
		pStatValue->varMaxValue.fValue = pSig->bv.varValue.fValue;
		pStatValue->tmMaxValue = pSig->bv.tmCurrentSampled;
	}

	// the minimum value
	if (pStatValue->varMinValue.fValue > pSig->bv.varValue.fValue)
	{
		pStatValue->varMinValue.fValue = pSig->bv.varValue.fValue;
		pStatValue->tmMinValue = pSig->bv.tmCurrentSampled;
	}

	// the average value
	// 1. PSMS formula
	//    Apsms = (N-1)*A+V)/N => (N-1)/N*A + V/N. // to avoid overlimit
	// 2. New formula. 
	//    Aacu = A + (V-A)/N  // N includes the new value.
#define FLOAT_SAFE_AVER(A, N, V) /* N includes new value*/	\
	((double)(A) + ((double)(V)-(double)(A))/(double)(N))

	pStatValue->iValueCount++;	// increase the point count at first

	pStatValue->varAverageValue.fValue = (float)FLOAT_SAFE_AVER(
		pStatValue->varAverageValue.fValue,
		pStatValue->iValueCount,
		pSig->bv.varValue.fValue);
}





/*==========================================================================*
 * FUNCTION : Equip_ProcessSamplingSignals
 * PURPOSE  : cal expression signal,
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : TRUE if any data is changed 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-14 17:23
 *==========================================================================*/
static BOOL Equip_ProcessSamplingSignals(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip)
{
#define SYSTEM_EQUIP_ID			1
#define SYSTEM_EQUIP_LOAD_ID		2	
#define	MIN_RECT_EQUIP_ID		3
#define	MAX_RECT_EQUIP_ID		102
#define	MIN_RECT_CAN2_EQUIP_ID		1601
#define	MAX_RECT_CAN2_EQUIP_ID		1660

#define	MIN_CONV_EQUIP_ID		211
#define	MAX_CONV_EQUIP_ID		258

#define	MIN_CONV_EQUIP_ID_EXTEND		1400
#define	MAX_CONV_EQUIP_ID_EXTEND		1411

#define	MIN_MPPT_EQUIP_ID		1351
#define	MAX_MPPT_EQUIP_ID		1380//1366

#define	MIN_1RECT_EQUIP_ID		339
#define	MAX_1RECT_EQUIP_ID		398

#define	MIN_2RECT_EQUIP_ID		439
#define	MAX_2RECT_EQUIP_ID		498

#define	MIN_3RECT_EQUIP_ID		539
#define	MAX_3RECT_EQUIP_ID		598

	time_t				tmNow = time(NULL);
	BOOL				bDataChanged = FALSE;
	int					i;

	int					nSig;
	SAMPLE_SIG_VALUE	*pSig;

	EXP_CONST			fResult;
	EXP_ELEMENT			result = {EXP_ETYPE_CONST, &fResult};

	float				fNewValue;
	BOOL				bDispFlagChange = FALSE;

	int				iRectifierNum, nBufLen, iWorkMode;
	SIG_BASIC_VALUE			*pSigValue;
	int j;//for debug

	//printf("EquipID=%d;",pEquip->iEquipID);

	static int iRefreashName = 4;

	static BOOL			bInited = FALSE;
	EQUIP_INFO	*pTempEquip = NULL;

	static SIG_BASIC_VALUE *pSigSYSWorkMode = NULL;

	static SIG_BASIC_VALUE *pSigRectNum = NULL;

	int	iPositionNumber;

	int iError;

	if (!bInited)
	{
		pTempEquip = Init_GetEquipRefById(pSite, 1);
		pSigSYSWorkMode = Equip_GetSpecialSigByMergedId(pTempEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 180));

		pTempEquip = Init_GetEquipRefById(pSite, 2);
		pSigRectNum = Equip_GetSpecialSigByMergedId(pTempEquip,SIG_ID_ACTUAL_RECT_NUM);

		bInited = TRUE;
	}

	if((pEquip->iEquipID >= MIN_RECT_EQUIP_ID && pEquip->iEquipID <= MAX_RECT_EQUIP_ID )
		|| ( pEquip->iEquipID >= MIN_RECT_CAN2_EQUIP_ID && pEquip->iEquipID <= MAX_RECT_CAN2_EQUIP_ID ) )
	{
		if (DxiGetData(VAR_A_SIGNAL_VALUE,
				DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID),	
				SIG_ID_ACTUAL_RECT_NUM,
				&nBufLen,			
				&pSigValue,			
				0) == ERR_DXI_OK)
		{
			iRectifierNum = pSigValue->varValue.ulValue;
		}
		else
		{
			iRectifierNum = 1000;  //has no limit
		}		
	}

	if(( pEquip->iEquipID >= MIN_RECT_EQUIP_ID && pEquip->iEquipID <= MAX_RECT_EQUIP_ID ) 
		|| ( pEquip->iEquipID >= MIN_RECT_CAN2_EQUIP_ID && pEquip->iEquipID <= MAX_RECT_CAN2_EQUIP_ID ) 
		||( pEquip->iEquipID >= MIN_CONV_EQUIP_ID && pEquip->iEquipID <= MAX_CONV_EQUIP_ID )
		||( pEquip->iEquipID >= MIN_MPPT_EQUIP_ID && pEquip->iEquipID <= MAX_MPPT_EQUIP_ID )
		|| ( pEquip->iEquipID >= MIN_CONV_EQUIP_ID_EXTEND && pEquip->iEquipID <= MAX_CONV_EQUIP_ID_EXTEND ))
	{	
		int	iPositionNumber;
		
		int iError;
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			pEquip->iEquipID,	
			SIG_ID_RECT_CONV_POSITION,
			&nBufLen,			
			&pSigValue,			
			0);
		if (iError == ERR_DXI_OK && pSigValue->varValue.ulValue > 0)
		{
			
			iPositionNumber = pSigValue->varValue.ulValue;
			Equip_ProcessRectifierName(pEquip, iPositionNumber);			
		}
		/*if(( pEquip->iEquipID >= MIN_RECT_EQUIP_ID && pEquip->iEquipID <= MAX_RECT_EQUIP_ID ) 
		|| ( pEquip->iEquipID >= MIN_RECT_CAN2_EQUIP_ID && pEquip->iEquipID <= MAX_RECT_CAN2_EQUIP_ID ) )

			printf("Eq %d/%d=%s;Er=%d/%d\n", pEquip->iEquipID, iPositionNumber, pEquip->pEquipName->pAbbrName[0],iError,pSigValue->varValue.ulValue);*/
		
	}

	iWorkMode = pSigSYSWorkMode->varValue.enumValue;

	if(iWorkMode == 1)
	{
		if(( pEquip->iEquipID >= MIN_1RECT_EQUIP_ID && pEquip->iEquipID <= MAX_1RECT_EQUIP_ID )
			|| ( pEquip->iEquipID >= MIN_2RECT_EQUIP_ID && pEquip->iEquipID <= MAX_2RECT_EQUIP_ID )
			|| ( pEquip->iEquipID >= MIN_3RECT_EQUIP_ID && pEquip->iEquipID <= MAX_3RECT_EQUIP_ID ))
		{

			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				pEquip->iEquipID,	
				SIG_ID_RECT_CONV_POSITION,
				&nBufLen,			
				&pSigValue,			
				0);
			if (iError == ERR_DXI_OK && pSigValue->varValue.ulValue > 0)
			{

				iPositionNumber = pSigValue->varValue.ulValue;
				Equip_ProcessRectifierName(pEquip, iPositionNumber);			
			}

		}

	}



	UNUSED(pSite);

	// 1.process virtual sampling signals
	pSig = pEquip->pSampleSigValue;
	nSig = pEquip->pStdEquip->iSampleSigNum;

	Mutex_Lock(pEquip->hLcdSyncLock, MAX_TIME_WAITING_EQUIP_LOCK);
	for (i = 0; i < nSig; i++, pSig++)
	{
		//1 calculate the virtual signal value.
		if (pSig->sv.pCalculateExpression != NULL)
		{
			// if the raw is valid, set the sig to valid first. 
			if ((pSig->sv.iSamplerChannel >= 0) &&
				SIG_STATE_TEST(pSig, VALUE_STATE_IS_VALID_RAW))
			{
				// will cause some services get wrong value while
				// calculating, if the result is invalid after calculated.
				SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);
			}

			// then, calc
			Exp_Calculate(EQP_GET_SIGV_NAME0(pSig),
				pSig->sv.pCalculateExpression,
				pSig->sv.iCalculateExpression, &result);

			// the result is valid, set the signal value
			if (result.byElementType == EXP_ETYPE_CONST)
			{
				fNewValue = EXP_GET_CONST_FLOAT(&result);

				if (pSig->sv.iSamplerChannel < 0)
				{
					pSig->bv.fRawData         = fNewValue;
					pSig->bv.tmCurrentSampled = tmNow;
				}

				// set raw is valid
				SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID_RAW);
				SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);
			}
			else	// set raw is invalid
			{
				// do NOT change current value, only set raw data state to invalid.				
				SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID_RAW);
			}
		}
		else
		{
			fNewValue = pSig->bv.fRawData;
		}

		//G3_OPT, the call for Equip_SetSignalValue_new could be removed after the bug(using int as float directly) 
		//in sampling lib is fixed up, but the effort is big, so keep it now.  Thomas, 2014-8

		// set the signal value from raw data if raw is valid
		// except the internal virtual signal that has no raw value.
		if (pSig->sv.iSamplerChannel == VIRTUAL_SAMPLING_CHANNEL)
		{
			bDataChanged |= Equip_SetSignalValue_new(pEquip, 
				&pSig->bv, 
				fNewValue,
				pSig->pStdSig->fMinValidValue,	// for ENUM sig, it's 0
				pSig->pStdSig->fMaxValidValue);	// for enum sig, it's the iStateNum-1.			
		}
		else if (pSig->sv.iSamplerChannel != VIRTUAL_INTERNAL_CHANNEL)
		//if (pSig->sv.iSamplerChannel != VIRTUAL_INTERNAL_CHANNEL)
		{
			bDataChanged |= Equip_SetSignalValue(pEquip, 
				&pSig->bv, 
				fNewValue,
				pSig->pStdSig->fMinValidValue,	// for ENUM sig, it's 0
				pSig->pStdSig->fMaxValidValue);	// for enum sig, it's the iStateNum-1.			
		}

		else
		{
			// add process communication status. maofuhua, 2005-4-5
			if (!pEquip->bCommStatus 
				&& (pSig->bv.ucSigType == SIG_TYPE_SAMPLING) 
				&& (pSig != pEquip->pCommStatusSigRef)
				&& (pSig != pEquip->pWorkStatusSigRef))
			{

				if((pEquip->iEquipID < MIN_RECT_EQUIP_ID) 
					|| ((pEquip->iEquipID > MAX_RECT_EQUIP_ID) &&(pEquip->iEquipID < 339)) || (pEquip->iEquipID > 639))
				{
					SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID);
				}
			}
		}

		// 5. calculate the display flag
		if (pSig->sv.pDispExp)
		{
			Exp_Calculate(EQP_GET_SIGV_NAME0(pSig),
				pSig->sv.pDispExp,
				pSig->sv.iDispExp,
				&result);

			if (result.byElementType == EXP_ETYPE_CONST)
			{
				if (EXP_GET_CONST_LONG(&result))	// enable set
				{
					if (!SIG_VALUE_NEED_DISPLAY(pSig))// last need NOT to be displayed
					{
						SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);
						bDispFlagChange = TRUE;
					}
				}
				else 
				{
					if (SIG_VALUE_NEED_DISPLAY(pSig))// last need to be displayed
					{
						SIG_STATE_CLR(pSig, VALUE_STATE_NEED_DISPLAY);
						bDispFlagChange = TRUE;
					}
				}
			}
			else	//Enable set, even if the exp result is invalid
			{
				if (!SIG_VALUE_NEED_DISPLAY(pSig))// last need NOT to be displayed
				{
					SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);
					bDispFlagChange = TRUE;
				}
			}
		}


		// if value is invalid, we need not to save or stat data.
		// continue to do next signal.
		if (!SIG_VALUE_IS_VALID(pSig))
		{
			continue;
		}

		//special for rectifier
		//if rectifier equipID  > rectifier number , continue to do next equipment.
		if(iRectifierNum <= 100)
		{
			if(pEquip->iEquipID > 2 + iRectifierNum &&  pEquip->iEquipID <= MAX_RECT_EQUIP_ID 
				|| (pEquip->iEquipID >= MIN_RECT_CAN2_EQUIP_ID  && pEquip->iEquipID <=MAX_RECT_CAN2_EQUIP_ID))
			{
				continue;
			}
		}
		else if(pEquip->iEquipID >= MIN_RECT_CAN2_EQUIP_ID + iRectifierNum -100 && pEquip->iEquipID <=MAX_RECT_CAN2_EQUIP_ID)
		{
			continue;
		}
		
		// 3. to chk the data need save or not if the value is data is valid
		if ((pSite->hHistoricalData != NULL) &&
			!pSite->bAlarmOutgoingBlocked &&
			Equip_CheckSavingData(pSig))
		{
			HIS_DATA_RECORD	data;

			//debug begin
			data.iEquipID	  = pEquip->iEquipID;
			data.iSignalID	  = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 
				pSig->pStdSig->iSigID); 
			data.varSignalVal = pSig->bv.varValue;
			data.tmSignalTime = pSig->bv.tmCurrentSampled;
			data.iActAlarmID  = 0;	// Not support, for compliant with PSMS.

			if (DAT_StorageWriteRecord(pSite->hHistoricalData,
				sizeof(data), (void *)&data))
			{
				pSig->sv.varLastSaved = data.varSignalVal;
				pSig->sv.tmLastSaved  = data.tmSignalTime;
				//If it is load signal, should send the message to Web
				if(pEquip->iEquipID == SYSTEM_EQUIP_ID && pSig->pStdSig->iSigID == SYSTEM_EQUIP_LOAD_ID)
				{
					HIS_DATA_RECORD_LOAD  stDataRecord;
					stDataRecord.floadCurrent = pSig->bv.varValue.fValue;
					stDataRecord.tmSignalTime = pSig->bv.tmCurrentSampled;

					NotificationFunc(_LOAD_CURRENT_MASK,	// masks
							sizeof(HIS_DATA_RECORD_LOAD),				// the sizeof (EQUIP_INFO *)
							&stDataRecord,							// the equipment address ref.
							FALSE);
					//printf("-----------Load Notification-------\n");
				}
			}
			else
			{
				TRACEX("Saving data for %s(%d)... FAILED.\n", 
					EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID);
			}
			//if((pEquip->iEquipID == 1) && ((pSig->pStdSig->iSigID == 2) || (pSig->pStdSig->iSigID == 182)))
			//{
			//    for(j = 0; j < 10; j++)
			//    {
			//	data.iEquipID	  = pEquip->iEquipID;
			//	data.iSignalID	  = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 
			//	    pSig->pStdSig->iSigID); 
			//	data.varSignalVal = pSig->bv.varValue;
			//	data.tmSignalTime = pSig->bv.tmCurrentSampled + j * 100;
			//	data.iActAlarmID  = 0;	// Not support, for compliant with PSMS.

			//	if (DAT_StorageWriteRecord(pSite->hHistoricalData,
			//	    sizeof(data), (void *)&data))
			//	{
			//	    pSig->sv.varLastSaved = data.varSignalVal;
			//	    pSig->sv.tmLastSaved  = data.tmSignalTime;
			//	}
			//	else
			//	{
			//	    TRACEX("Saving data for %s(%d)... FAILED.\n", 
			//		EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID);
			//	}
			//    }
			//}
			//else
			//{
			//    data.iEquipID	  = pEquip->iEquipID;
			//    data.iSignalID	  = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 
			//    	pSig->pStdSig->iSigID); 
			//    data.varSignalVal = pSig->bv.varValue;
			//    data.tmSignalTime = pSig->bv.tmCurrentSampled;
			//    data.iActAlarmID  = 0;	// Not support, for compliant with PSMS.

			//    if (DAT_StorageWriteRecord(pSite->hHistoricalData,
			//    	sizeof(data), (void *)&data))
			//    {
			//    	pSig->sv.varLastSaved = data.varSignalVal;
			//    	pSig->sv.tmLastSaved  = data.tmSignalTime;
			//    }
			//    else
			//    {
			//    	TRACEX("Saving data for %s(%d)... FAILED.\n", 
			//    		EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID);
			//    }
			//}
			//debug end
		}


		// 4. calculate the statistic data
		// NOTE: the stat data only is valid in float type signal
		if (pSig->sv.pStatValue != NULL)	// need calculate the stat data
		{
			Equip_StatSamplingSignalData(pSig);
		}

	}

	Mutex_Unlock(pEquip->hLcdSyncLock);
	if(bDispFlagChange)
	{
		MESSAGE_TO_UI	stMessageToLcd;

		stMessageToLcd.iEquipId = pEquip->iEquipID;
		stMessageToLcd.iSigType = SIG_TYPE_SAMPLING;
		if(ERR_OK != Queue_Put(pSite->hLcdMessageQueue, &stMessageToLcd, FALSE))
		{
			TRACEX("Failed to put a element in the queue hLcdMessageQueue.\n");
		}
	}	
	return bDataChanged;
}


#ifdef _CHK_SET_DEADLOCK
/*==========================================================================*
 * FUNCTION : Equip_CheckSettingDeadlock
 * PURPOSE  : check there is a deadlock of settable flag of the setting signal,
 *            when invalid default value of it is set.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SET_SIG_VALUE  *pSetSig : 
 * RETURN   : static BOOL : TRUE the setting sig has a deadlock!
 * COMMENTS : must be called before clearing the settable flag!
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-21 20:08
 *==========================================================================*/
static BOOL Equip_CheckSettingDeadlock(IN SET_SIG_VALUE *pSetSig)
{
	int					i;
	EXP_ELEMENT			*pExp	  = pSetSig->sv.pSettableExpression;
	int					nElements = pSetSig->sv.iSettableExpression;
	SIG_BASIC_VALUE		*pSig;
	BOOL				bDeadlocked = FALSE;

	for (i = 0; i < nElements; i++, pExp++)
	{
		if (pExp->byElementType == EXP_ETYPE_VARIABLE)
		{
			//1. find the related sampling signal
			pSig = (SIG_BASIC_VALUE *)pExp->pElement;

			// ONLY check the relevant setting signal is settable or NOT.
			if (pSig->ucSigType == SIG_TYPE_SETTING)				
			{
				if (((void *)pSetSig != (void *)pSig) &&	// NOT itself.
					SIG_VALUE_IS_SETTABLE(pSig))	// this sig is settable. 
				{
					bDeadlocked = FALSE;	// no deadlock.
					break;
				}

				bDeadlocked = TRUE;	// there may be a deadlock.
			}
		}
	}

#ifdef _DEBUG_DATA_PROCESS
	TRACEX("Sig %s(%d) has %d setting deadlock!\n", 
		EQP_GET_SIGV_NAME0(pSetSig),  pSetSig->pStdSig->iSigID,
		bDeadlocked);
#endif

	return bDeadlocked;
}






/*==========================================================================*
 * FUNCTION : Equip_CheckSettingDeadlock
 * PURPOSE  : check there is a deadlock of settable flag of the setting signal,
 *            when invalid default value of it is set.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SET_SIG_VALUE  *pSetSig : 
 * RETURN   : static BOOL : TRUE the setting sig has a deadlock!
 * COMMENTS : must be called before clearing the settable flag!
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-21 20:08
 * MODIFY   : Frank Cao 2007-3-28
 *==========================================================================*/
//static BOOL Equip_CheckSettingDeadlock(IN SET_SIG_VALUE *pSetSig)
//{
//	int					i, j;
//	EXP_ELEMENT			*pExpSelf = pSetSig->sv.pSettableExpression;
//	int					nElementsSelf = pSetSig->sv.iSettableExpression;
//	SIG_BASIC_VALUE		*pSigInSelfExp;
//	BOOL				bDeadlocked = FALSE;
//
//	for (i = 0; i < nElementsSelf; i++, pExpSelf++)
//	{
//		if (pExpSelf->byElementType == EXP_ETYPE_VARIABLE)
//		{
//			EXP_ELEMENT			*pExpThat;
//			int					nElementsThat;
//			SIG_BASIC_VALUE		*pSigInThatExp;
//
//			pSigInSelfExp = (SIG_BASIC_VALUE *)pExpSelf->pElement;
//
//			if ((pSigInSelfExp->ucSigType == SIG_TYPE_SETTING)	// Setting signal
//				&& ((void *)pSetSig != (void *)pSigInSelfExp))	// NOT itself.
//			{
//				BOOL	bIamInThat = FALSE;
//				
//				pExpThat = ((SET_SIG_VALUE*)pSigInSelfExp)->sv.pSettableExpression;
//				nElementsThat = ((SET_SIG_VALUE*)pSigInSelfExp)->sv.iSettableExpression;
//
//				for (j = 0; j < nElementsThat; j++, pExpThat++)
//				{
//					if (pExpThat->byElementType == EXP_ETYPE_VARIABLE)
//					{
//						pSigInThatExp = (SIG_BASIC_VALUE *)pExpThat->pElement;
//						if((void *)pSigInThatExp == (void *)pSetSig)
//						{
//							bIamInThat = TRUE;
//							break;
//						}
//					}
//				}
//
//				//This signal is in that signal's Settable Expression,
//				//and that signal is in this signal's Settable Expression
//				if(bIamInThat)
//				{
//					if (SIG_VALUE_IS_SETTABLE(pSigInSelfExp))	// this sig is settable. 
//					{
//						//bDeadlocked = FALSE;	// no deadlock.
//						//break;
//					}
//					else
//					{
//						bDeadlocked = TRUE;	// there may be a deadlock.
//						break;
//					}
//				}
//			}
//		}
//	}
//
//#ifdef _DEBUG_DATA_PROCESS
//	TRACEX("Sig %s(%d) has %d setting deadlock!\n", 
//		EQP_GET_SIGV_NAME0(pSetSig),  pSetSig->pStdSig->iSigID,
//		bDeadlocked);
//#endif
//
//	return bDeadlocked;
//}


/*==========================================================================*
 * FUNCTION : Equip_ModifySettingDeadlock
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SET_SIG_VALUE  *pSetSig : 
 *            IN BOOL           bSet     : TRUE to set deadlock state, and 
 *                                         enable set related setting signals.
 *                                         FALSE to clear.
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-23 19:01
 *==========================================================================*/
BOOL Equip_ModifySettingDeadlock(IN SET_SIG_VALUE *pSetSig, IN BOOL bSet)
{
	int					i;
	EXP_ELEMENT			*pExp	  = pSetSig->sv.pSettableExpression;
	int					nElements = pSetSig->sv.iSettableExpression;
	SIG_BASIC_VALUE		*pSig;

#ifdef _DEBUG_DATA_PROCESS
	TRACEX("%s VALUE_STATE_IS_DEADLOCKED flag for %s(%d).\n",
		bSet ? "Set" : "Clear", 
		EQP_GET_SIGV_NAME0(pSetSig),  pSetSig->pStdSig->iSigID);
#endif

	if (bSet)
	{
		SIG_STATE_SET(pSetSig, VALUE_STATE_IS_DEADLOCKED);
		SIG_STATE_SET(pSetSig, VALUE_STATE_IS_SETTABLE);
	}
	else
	{
		SIG_STATE_CLR(pSetSig, VALUE_STATE_IS_DEADLOCKED);
	}

	for (i = 0; i < nElements; i++, pExp++)
	{
		if (pExp->byElementType == EXP_ETYPE_VARIABLE)
		{
			//1. find the related sampling signal
			pSig = (SIG_BASIC_VALUE *)pExp->pElement;

			// ONLY check the relevant setting signal is settable or NOT.
			if ((pSig->ucSigType == SIG_TYPE_SETTING) &&
				((void *)pSetSig != (void *)pSig))		// NOT itself.
			{
#ifdef _DEBUG_DATA_PROCESS
				TRACEX("%s VALUE_STATE_IS_DEADLOCKED flag for %s(%d).\n",
					bSet ? "Set" : "Clear", 
					EQP_GET_SIGV_NAME0((SET_SIG_VALUE *)pSig), 
					((SET_SIG_VALUE *)pSig)->pStdSig->iSigID);
#endif
				if (bSet)
				{
					SIG_STATE_SET(pSig, VALUE_STATE_IS_DEADLOCKED);
					SIG_STATE_SET(pSig, VALUE_STATE_IS_SETTABLE);
				}
				else
				{
					SIG_STATE_CLR(pSig, VALUE_STATE_IS_DEADLOCKED);
				}
			}
		}
	}


	return TRUE;
}
#endif


/*==========================================================================*
 * FUNCTION : Equip_ProcessSettingSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:03
 *==========================================================================*/
static BOOL Equip_ProcessSettingSignals(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip)
{
	BOOL				bDataChanged = FALSE;
	int					i;

	int					nSig;
	SET_SIG_VALUE		*pSig;

	EXP_CONST			fResult;
	EXP_ELEMENT			result = {EXP_ETYPE_CONST, &fResult};

	unsigned short		usOldStateMask;
	BOOL				bDispFlagChange = FALSE;

	UNUSED(pSite);


//#ifdef _DEBUG_DATA_PROCESS
	//TRACE("Equip_ProcessSettingSignals %s(%d).......\n",
	//	EQP_GET_EQUIP_NAME0(pEquip),
	//	pEquip->iEquipID);
//#endif //_DEBUG_DATA_PROCESS

	pSig = pEquip->pSetSigValue;
	nSig = pEquip->pStdEquip->iSetSigNum;
	Mutex_Lock(pEquip->hLcdSyncLock, MAX_TIME_WAITING_EQUIP_LOCK);

	for (i = 0; i < nSig; i++, pSig++)
	{
		usOldStateMask = pSig->bv.usStateMask;

		// 1. calculate the display flag
		if (pSig->sv.pDispExp)
		{
			Exp_Calculate(EQP_GET_SIGV_NAME0(pSig),
				pSig->sv.pDispExp,
				pSig->sv.iDispExp,
				&result);

			if (result.byElementType == EXP_ETYPE_CONST)
			{
				if (EXP_GET_CONST_LONG(&result))	// enable set
				{
					if (!SIG_VALUE_NEED_DISPLAY(pSig))// last need NOT to be displayed
					{
						SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);
						bDispFlagChange = TRUE;
					}
				}
				else 
				{
					if (SIG_VALUE_NEED_DISPLAY(pSig))// last need to be displayed
					{
						SIG_STATE_CLR(pSig, VALUE_STATE_NEED_DISPLAY);
						bDispFlagChange = TRUE;
					}
				}
			}
			else	//Enable set, even if the exp result is invalid
			{
				if (!SIG_VALUE_NEED_DISPLAY(pSig))// last need NOT to be displayed
				{
					SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);
					bDispFlagChange = TRUE;
				}
			}
		}

		// 3. test the data is changed or not
		// we DO NOT consider the invalid/valid state of the signal
		// all data use the integer type in comparison
		// set the signal value from raw data if raw is valid
		if (pSig->sv.iSamplerChannel >= 0) // no virtual calc channel in set.
		{
			bDataChanged |= Equip_SetSignalValue(pEquip, &pSig->bv, 
				pSig->bv.fRawData,
				pSig->pStdSig->fMinValidValue,	// for ENUM sig, it's 0
				pSig->pStdSig->fMaxValidValue);	// for enum sig, it's the iStateNum-1.
		}
		

		if (usOldStateMask != pSig->bv.usStateMask)
		{
			bDataChanged = TRUE;
		}

	}

	Mutex_Unlock(pEquip->hLcdSyncLock);
	
	if(bDispFlagChange)
	{
		MESSAGE_TO_UI	stMessageToLcd;

		stMessageToLcd.iEquipId = pEquip->iEquipID;
		stMessageToLcd.iSigType = SIG_TYPE_SETTING;
		if(ERR_OK != Queue_Put(pSite->hLcdMessageQueue, &stMessageToLcd, FALSE))
		{
			TRACEX("Failed to put a element in the queue hLcdMessageQueue.\n");
		}
	}

	return bDataChanged;
}


/*==========================================================================*
 * FUNCTION : Equip_ProcessControlSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:03
 *==========================================================================*/
static BOOL Equip_ProcessControlSignals(IN SITE_INFO *pSite,
									   IN OUT EQUIP_INFO *pEquip)
{
	BOOL				bDataChanged = FALSE;
	int					i;

	int					nSig;
	CTRL_SIG_VALUE		*pSig;

	EXP_CONST			fResult;
	EXP_ELEMENT			result = {EXP_ETYPE_CONST, &fResult};

	unsigned short		usOldStateMask;
	BOOL				bDefaultCtrlable, bExpCtrlable;
	BOOL				bDispFlagChange = FALSE;

	UNUSED(pSite);

	//{{ if the system is in auto state, the default ctrl state of sig is FALSE,
	//   and in man state the default ctrlable state is TRUE(can control).
	//   BUT, the default ctrl state can be overrided by the controllable exp.
	//   maofuhua, 2005-3-1
	bDefaultCtrlable = 
		(pSite->nSystemManagementMode == SYS_STATE_BY_MAN) ? TRUE : FALSE; 
	//}}

	pSig = pEquip->pCtrlSigValue;
	nSig = pEquip->pStdEquip->iCtrlSigNum;
	
	Mutex_Lock(pEquip->hLcdSyncLock, MAX_TIME_WAITING_EQUIP_LOCK);
	for (i = 0; i < nSig; i++, pSig++)
	{
		bExpCtrlable   = bDefaultCtrlable;
		usOldStateMask = pSig->bv.usStateMask;

		// 1. calculate the controllable flag of the control signals
		if (pSig->sv.pControllableExpression != NULL)
		{
			Exp_Calculate(EQP_GET_SIGV_NAME0(pSig),
				pSig->sv.pControllableExpression,
				pSig->sv.iControllableExpression, &result);

			// the result is valid, set the control flag to pSig->usFlag
			bExpCtrlable = ((result.byElementType == EXP_ETYPE_CONST) &&
				(EXP_GET_CONST_LONG(&result) != 0))	
				? TRUE		// enable control
				: FALSE;	// disable control, even if the exp result is invalid
		}

		// 2. change the controllable state.
		if (bExpCtrlable)	// enable ctrl
		{
			SIG_STATE_SET(pSig, VALUE_STATE_IS_CONTROLLABLE);
		}
		else	// disable control
		{
			SIG_STATE_CLR(pSig, VALUE_STATE_IS_CONTROLLABLE);
		}

		if (usOldStateMask != pSig->bv.usStateMask)
		{
			bDataChanged = TRUE;
		}

		// 3. calculate the display flag
		if (pSig->sv.pDispExp)
		{
			Exp_Calculate(EQP_GET_SIGV_NAME0(pSig),
				pSig->sv.pDispExp,
				pSig->sv.iDispExp,
				&result);

			if (result.byElementType == EXP_ETYPE_CONST)
			{
				if (EXP_GET_CONST_LONG(&result) != 0)	// enable set
				{
					if (!SIG_VALUE_NEED_DISPLAY(pSig))// last need NOT to be displayed
					{
						SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);
						bDispFlagChange = TRUE;
					}
				}
				else
				{
					if (SIG_VALUE_NEED_DISPLAY(pSig))// last need to be displayde
					{
						SIG_STATE_CLR(pSig, VALUE_STATE_NEED_DISPLAY);
						bDispFlagChange = TRUE;
					}
				}
			}
			else	//Enable set, even if the exp result is invalid
			{
				if (!SIG_VALUE_NEED_DISPLAY(pSig))// last need NOT to be displayed
				{
					SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);
					bDispFlagChange = TRUE;
				}
			}
		}

		// 2. test the data is changed or not
		// set the signal value from raw data if raw is valid
		if (pSig->sv.iSamplerChannel >= 0) // no virtual calc channel in ctrl.
		{
			bDataChanged |= Equip_SetSignalValue(pEquip, &pSig->bv, 
				pSig->bv.fRawData,
				pSig->pStdSig->fMinValidValue,	// for ENUM sig, it's 0
				pSig->pStdSig->fMaxValidValue);	// for enum sig, it's the iStateNum-1.
		}
	}
	Mutex_Unlock(pEquip->hLcdSyncLock);

	if(bDispFlagChange)
	{
		MESSAGE_TO_UI	stMessageToLcd;

		stMessageToLcd.iEquipId = pEquip->iEquipID;
		stMessageToLcd.iSigType = SIG_TYPE_CONTROL;
		if(ERR_OK != Queue_Put(pSite->hLcdMessageQueue, &stMessageToLcd, FALSE))
		{
			TRACEX("Failed to put a element in the queue hLcdMessageQueue.\n");
		}
	}

	return bDataChanged;
}


/*==========================================================================*
 * FUNCTION : Site_ProcessAlarmSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:03
 *==========================================================================*/
static BOOL Site_ProcessAlarmSignals(IN SITE_INFO *pSite)
{
	BOOL			bDataChanged = FALSE;
	int				i;
	EQUIP_INFO		*pEquip;
	

	// proccess the equipment alarms if its data is changed
	pEquip = pSite->pEquipInfo;

	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{	   
		/*if(pEquip->bClearAlarmStatus)
		{
			pEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
		}*/

		if (pEquip->iProcessingState == EQP_STATE_PROCESS_DOING)
		{
			bDataChanged |= Equip_ProcessAlarmSignals(pSite, pEquip);
		}

		/*if(pEquip->bClearAlarmStatus)
		{
			pEquip->bClearAlarmStatus = FALSE;			
		}*/
	}

	return bDataChanged;
}


static BOOL Equip_UpdateSamplingSigAlarmState(ALARM_SIG_VALUE *pSigAlarm);
static BOOL Equip_AlarmSave(IN SITE_INFO *pSite, IN EQUIP_INFO *pEquip,
							  IN OUT ALARM_SIG_VALUE *pSig, IN int nActionFlag);
static BOOL Equip_AlarmReport(IN SITE_INFO *pSite, IN EQUIP_INFO *pEquip,
							  IN OUT ALARM_SIG_VALUE *pSig);


/*==========================================================================*
 * FUNCTION : AlarmRelayAction
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int   iCurrRelayNo : 
 *            IN BOOL  bAlarmStart  : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-06 11:30
 *==========================================================================*/
static void AlarmRelayAction(IN int iCurrRelayNo,
								   IN BOOL bAlarmStart)
{	
	int iCtrlValue;
	iCtrlValue = (bAlarmStart) ? 1 : 0;
	
	RelayOutputCtrl(iCtrlValue, iCurrRelayNo);

	return;
}


/*==========================================================================*
 * FUNCTION : IsAlmSigInList
 * PURPOSE  : If the alarm signal pSig is in the equipment list, return TRUE.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO       *pEquip : 
 *            IN ALARM_SIG_VALUE  *pSig   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao           DATE: 2007-03-13 15:27
 *==========================================================================*/
static BOOL IsAlmSigInList(IN EQUIP_INFO *pEquip,
						IN ALARM_SIG_VALUE *pSig)
{
	ALARM_SIG_VALUE*	pSigInList;
	ALARM_SIG_VALUE*	pHead;
	BOOL				bFind = FALSE;

	pHead = &pEquip->pActiveAlarms[pSig->iAlarmLevel];
	pSigInList = pHead->next;
     
	// the link is a bilink.
	    	
	for (; pSigInList != pHead; pSigInList = pSigInList->next)
	{
		if(pSigInList == pSig)
		{
			bFind = TRUE;
			break;
		}
		if(pSigInList==pHead)
		{
			break;
		}
	}
	return bFind;
}


/*==========================================================================*
 * FUNCTION : Equip_AlarmBegin
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO            *pSite        : 
 *            IN OUT EQUIP_INFO       *pEquip       : 
 *            IN OUT ALARM_SIG_VALUE  *pSig         : 
 *            IN time_t               tmNow         : 
 *            IN int				  nActionFlag   : combination of 
 *                                ALARM_ACTION_SAVE and ALARM_ACTION_REPORT
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:04
 *==========================================================================*/
#define    BATTERY_COMM_INTERRUPT_SIGID       8
 
BOOL Equip_AlarmBegin(IN SITE_INFO *pSite,
					  IN OUT EQUIP_INFO *pEquip,
					  IN OUT ALARM_SIG_VALUE *pSig,
					  IN time_t tmNow,
					  IN int nActionFlag)
{
	int				nAlarmLevel;

	nAlarmLevel = pSig->iAlarmLevel;

	pSig->bv.varLastValue = pSig->bv.varValue;
	pSig->bv.varValue.lValue = 1;
	
	pSig->sv.tmStartTime = tmNow;
	pSig->sv.tmEndTime   = INVALID_TIME;
	if(pSig->szEquipSerial[0] == 0)
	{
		Equip_RefreshSerialNumber(pSite, pEquip);
#ifdef DEBUG_HISTORY_ALARMSERIAL	
		TRACE("pEquip->iEquipID =%d , pEquip->szSerialNumber = %s\n",pEquip->iEquipID , pEquip->szSerialNumber);
#endif	
		strncpyz(pSig->szEquipSerial, pEquip->szSerialNumber,
				sizeof(pSig->szEquipSerial));
	}
//	else
//	{
//#ifdef DEBUG_HISTORY_ALARMSERIAL	
//		TRACE("pEquip->iEquipID =%d , pEquip->szSerialNumber = %s\n",pEquip->iEquipID , pSig->szEquipSerial);
//#endif		
//	}
	
		
	SIG_STATE_SET(pSig, ALARM_STATE_IS_NEW_BEGIN);
	
	// when an suppressed alarm becomes free, we do NOT added it to list,
	// because the alarm is already in the list -- the alarm is not removed
	// from the active alarm list when the alarm is suppressed.
	if (pSig->next == NULL)		// add it to link.
	{
		ALARM_SIG_VALUE		*pSigLink;
	
		//// put the alarm to head of the active alarm list
		pSigLink = &pEquip->pActiveAlarms[nAlarmLevel];
		//// lock the equipment, must get the lock.
		Mutex_Lock(pEquip->hSyncLock, WAIT_INFINITE);

		ASSERT(!IsAlmSigInList(pEquip, pSig));

		INSERT_NODE_TO_LINK_HEAD(pSigLink, pSig);
		// count all alarms including suppressed alarms.
		//pEquip->iAlarmCount[nAlarmLevel]++;	// increase the alarm count
		//pEquip->iAlarmCount[0]++;			// total alarms.
		//

		//pSite->iAlarmCount[nAlarmLevel]++;	//increase the alarm count
		//pSite->iAlarmCount[0]++;			//total alarms.
        
		ASSERT(pEquip->iAlarmCount[0] == (pEquip->iAlarmCount[1] + pEquip->iAlarmCount[2] + pEquip->iAlarmCount[3]));
		ASSERT(pSite->iAlarmCount[0] == (pSite->iAlarmCount[1] + pSite->iAlarmCount[2] + pSite->iAlarmCount[3]));
		ASSERT((pEquip->iAlarmCount[0] >= 0) && (pEquip->iAlarmCount[1] >= 0) && (pEquip->iAlarmCount[2] >= 0) && (pEquip->iAlarmCount[3] >= 0));
		ASSERT((pSite->iAlarmCount[0] >= 0) && (pSite->iAlarmCount[1] >= 0) && (pSite->iAlarmCount[2] >= 0) && (pSite->iAlarmCount[3] >= 0));


		//For ydn23
		//Equip_ProcessSignalsForHLMS(pEquip, EQUIP_ALARM_CHANGE_FLAG);
	
		Mutex_Unlock(pEquip->hSyncLock);
		
	}
		
	// set the sampling signals' related alarm level
	Equip_UpdateSamplingSigAlarmState(pSig);

	// save the alarm state.
	Equip_AlarmSave(pSite, pEquip, pSig, nActionFlag);

	// report to application services
	if (FLAG_IS_SET(nActionFlag, ALARM_ACTION_REPORT))
	{
		Equip_AlarmReport(pSite, pEquip, pSig);
	}	

	//added by Thomas for TR# 70-ACU, Thomas, 2006-10-20
	Site_UpdateActiveAlarmCnt(pSite);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : BOOL Equip_AlarmEnd
 * PURPOSE  : The end alarm node will be removed 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO            *pSite          : 
 *            IN OUT EQUIP_INFO       *pEquip         : 
 *            IN OUT ALARM_SIG_VALUE  *pSig           : 
 *            IN time_t               tmNow           : 
 *            IN int				  nNewValue		  : -1: alarm level change.
 *                                                       0: normal end.
 *                                                       1: suppressed.
 *            IN int				  nActionFlag   : combination of 
 *                                ALARM_ACTION_SAVE and ALARM_ACTION_REPORT
 * RETURN   :  BOOL: 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:04
 *==========================================================================*/
BOOL Equip_AlarmEnd(IN SITE_INFO *pSite, 
					IN OUT EQUIP_INFO *pEquip,
					IN OUT ALARM_SIG_VALUE	*pSig,
					IN time_t tmNow,
					IN int nNewValue,
					IN int nActionFlag)
{
	int		nAlarmLevel;

#ifdef _DEBUG_DATA_PROCESS
	TRACEX("EQUIP Alarm %s(%d, L=%d) of equipment %s(%d) is end...\n",
		EQP_GET_SIGV_NAME0(pSig),    pSig->pStdSig->iSigID,
		pSig->iAlarmLevel,
		EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
#endif //_DEBUG_DATA_PROCESS

    //date: 2007/07/13 NO EXIST ALARM in ALARMs LIST don't need ended 
    if(!IsAlmSigInList(pEquip, pSig))
    {
        return  FALSE;
    }  

	nAlarmLevel = pSig->iAlarmLevel;


	// end the alarm and remove it from active alarm link.
	pSig->bv.varLastValue    = pSig->bv.varValue;
	pSig->sv.tmEndTime		 = tmNow;
	pSig->bv.varValue.lValue = (nNewValue != 0) ? 1 : 0;

	SIG_STATE_SET(pSig, ALARM_STATE_IS_NEW_END);

	// if the alarm end by condtion, but it is still
	// suppressed(the alarm has aleady ended by alarm suppression),
	// do NOT send end msg to appservice
	if ((pSig->bv.varValue.lValue == 0) 	// the alarm is END.
		|| (nNewValue < 0))					// alarm level changed 
	{
		// lock the equipment, must get the lock.
		// remove ifself from the alarm list
		Mutex_Lock(pEquip->hSyncLock, WAIT_INFINITE);
	
			
		ASSERT(IsAlmSigInList(pEquip, pSig));		

		//pEquip->iAlarmCount[nAlarmLevel]--;	// descrease the alarm count
		//pEquip->iAlarmCount[0]--;			// total alarms.
		//
		//pSite->iAlarmCount[nAlarmLevel]--;	// descrease the alarm count
		//pSite->iAlarmCount[0]--;			// total alarms.
		  
		ASSERT(pEquip->iAlarmCount[0] == (pEquip->iAlarmCount[1] + pEquip->iAlarmCount[2] + pEquip->iAlarmCount[3]));
		ASSERT(pSite->iAlarmCount[0] == (pSite->iAlarmCount[1] + pSite->iAlarmCount[2] + pSite->iAlarmCount[3]));
		ASSERT((pEquip->iAlarmCount[0] >= 0) && (pEquip->iAlarmCount[1] >= 0) && (pEquip->iAlarmCount[2] >= 0) && (pEquip->iAlarmCount[3] >= 0));
		ASSERT((pSite->iAlarmCount[0] >= 0) && (pSite->iAlarmCount[1] >= 0) && (pSite->iAlarmCount[2] >= 0) && (pSite->iAlarmCount[3] >= 0));
	
		REMOVE_NODE_SELF_FROM_LINK(pSig);
	
		//Equip_ProcessSignalsForHLMS(pEquip, EQUIP_ALARM_CHANGE_FLAG);
	
		Mutex_Unlock(pEquip->hSyncLock);	
	}

	// set the sampling signals' related alarm level
	Equip_UpdateSamplingSigAlarmState(pSig);
	
	// save the alarm state.
	Equip_AlarmSave(pSite, pEquip, pSig, nActionFlag);

	// report to application services
	if (FLAG_IS_SET(nActionFlag, ALARM_ACTION_REPORT) &&
		!SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_BEGIN))
	{
		Equip_AlarmReport(pSite, pEquip, pSig);
	}
	
	// clear the NEW_BEGIN report flag to avoid reporting begin notification.
	SIG_STATE_CLR(pSig, ALARM_STATE_IS_NEW_BEGIN);
	SIG_STATE_CLR(pSig, ALARM_STATE_IS_NEW_END);

	//added by Thomas for TR# 70-ACU, Thomas, 2006-10-20
	Site_UpdateActiveAlarmCnt(pSite);


	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Equip_AlarmSave
 * PURPOSE  : 1. save the ended alarm as historical alarm.
 *            2. save the active alarm state.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO            *pSite      : 
 *            IN EQUIP_INFO           *pEquip     : 
 *            IN OUT ALARM_SIG_VALUE  *pSig       : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-06 21:37
 *==========================================================================*/
static BOOL Equip_AlarmSave(IN SITE_INFO *pSite,
							IN EQUIP_INFO *pEquip,
							IN OUT ALARM_SIG_VALUE *pSig,
							IN int nActionFlag)
{
	BOOL	bSavedOK;

	// do NOT save anything when alarm outgoing is blocked.
	if (pSite->bAlarmOutgoingBlocked)
	{
		return TRUE;
	}

#ifdef SAVE_ACTIVE_ALARMS
	if ((pSite->hActiveAlarm)
		&& FLAG_IS_SET(nActionFlag, ALARM_ACTION_SAVE_ACTIVE))
	{
		ACT_ALARM_RECORD	data;

		// save the alarm state to storage.
		// set the active alarm log info
		data.iEquipID	 = pEquip->iEquipID;
		data.iAlarmID	 = pSig->pStdSig->iSigID;
		data.usStateMask = SIG_STATE_GET(pSig, // only save begin/end/suppressed flag
			(VALUE_STATE_IS_SUPPRESSED|ALARM_STATE_IS_NEW_BEGIN|ALARM_STATE_IS_NEW_END));
		data.byAlarmFlag = pSig->bv.varValue.lValue ? 1 : 0;
		data.tmStartTime = SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_END) ?
			pSig->sv.tmEndTime : pSig->sv.tmStartTime;		
		strncpyz(data.szSerial, pSig->szEquipSerial,
			sizeof(data.szSerial));
		data.iPosition = (pSig->iPosition == 0) ? Equip_GetRectID(pEquip->pEquipName->pAbbrName[0]):pSig->iPosition;

#ifdef DEBUG_HISTORY_ALARMSERIAL			
		TRACE("Equip_AlarmSave[%d] data.szSerial = %s\n",data.iEquipID, data.szSerial);	
#endif
		bSavedOK = DAT_StorageWriteRecord(pSite->hActiveAlarm, 
			sizeof(data), (void *)&data);

		if (bSavedOK)
		{
			pSite->nCurrentSavedAlarms++;

			if (pSite->nCurrentSavedAlarms >= 
				pSite->nMaxAllowedActiveAlarms)
			{
				Site_SaveActiveAlarms(pSite);	// resave all signals.
			}
		}
		if(SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_BEGIN) && !SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_END)
					&& !SIG_STATE_TEST(pSig, VALUE_STATE_IS_SUPPRESSED))
		{
			AppLogOut(EQP_MON, APP_LOG_INFO,
				"Alarm start, signal info is: %s,%s.(EquipID,%d, SignalID:%d)\n", 
				pEquip->pEquipName->pAbbrName[0],pSig->pStdSig->pSigName->pAbbrName[0], pEquip->iEquipID,pSig->pStdSig->iSigID);
		}

#ifdef _DEBUG_DATA_PROCESS
		TRACEX("Saving active alarm data for %s(%d), flag %d... %s.\n", 
			EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID,
			data.byAlarmFlag,
			bSavedOK ? "OK" : "FAILED");
#endif //_DEBUG_DATA_PROCESS
	}
#endif //#ifdef SAVE_ACTIVE_ALARMS


	// write the alarm as historical alarm.
	if (SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_END) &&	// just end, and 
		!SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_BEGIN) &&	// not just beginning.
		(pSite->hHistoricalAlarm != NULL) && 
		FLAG_IS_SET(nActionFlag, ALARM_ACTION_SAVE_HISTORY))
	{
		HIS_ALARM_RECORD	hist;

		hist.iEquipID = pEquip->iEquipID;
		hist.iAlarmID = pSig->pStdSig->iSigID;
		hist.byAlarmLevel = pSig->iAlarmLevel;
		hist.tmStartTime = pSig->sv.tmStartTime;
		hist.tmEndTime   = pSig->sv.tmEndTime;
		hist.varTrigValue.lValue = 0;	// not supported yet
		hist.iPosition = (pSig->iPosition == 0) ? Equip_GetRectID(pEquip->pEquipName->pAbbrName[0]) : pSig->iPosition;

		// save the serial number of equip.
		/*strncpyz(hist.szSerialNumber, pEquip->szSerialNumber,
			sizeof(hist.szSerialNumber));*/
		strncpyz(hist.szSerialNumber, pSig->szEquipSerial,
			sizeof(hist.szSerialNumber));
#ifdef DEBUG_HISTORY_ALARMSERIAL			
		TRACE("Save to storeage[%d] hist.szSerialNumber = %s\n", hist.iEquipID, hist.szSerialNumber);	
#endif
		bSavedOK = DAT_StorageWriteRecord(pSite->hHistoricalAlarm, 
			sizeof(hist), (void *)&hist);
		AppLogOut(EQP_MON, APP_LOG_INFO,
			"Alarm end, signal info is: %s,%s.(EquipID,%d, SignalID:%d)\n", 
			pEquip->pEquipName->pAbbrName[0],pSig->pStdSig->pSigName->pAbbrName[0], pEquip->iEquipID,pSig->pStdSig->iSigID);

#ifdef _DEBUG_DATA_PROCESS
		TRACEX("Saving historical alarm data for %s(%d)... %s.\n", 
			EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID,
			bSavedOK ? "OK" : "FAILED");
#endif //_DEBUG_DATA_PROCESS
	}
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Equip_AlarmReport
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO            *pSite  : 
 *            IN EQUIP_INFO           *pEquip : 
 *            IN OUT ALARM_SIG_VALUE  *pSig   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-19 10:33
 *==========================================================================*/
static BOOL Equip_AlarmReport(IN SITE_INFO *pSite,
							  IN EQUIP_INFO *pEquip,
							  IN OUT ALARM_SIG_VALUE *pSig)
{
	int		nNotificationMasks = 0;
	int		nAlarmLevel;

	UNUSED(pSite);
	UNUSED(pEquip);

	// the alarm is new begin
	if (SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_BEGIN))
	{
		nNotificationMasks = _ALARM_OCCUR_MASK;
		nAlarmLevel        = pSig->iAlarmLevel;
	}

	// the alarm is new end
	else if (SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_END))
	{
		nNotificationMasks = _ALARM_CLEAR_MASK;
		nAlarmLevel        = ALARM_LEVEL_NONE;// to aviod ugrent sending.
	}

	// no new alarm.
	if (nNotificationMasks == 0)
	{
		return FALSE;
	}

	// report and notify the services...
	// send notification to application serivces
	// nNotificationMasks: combination of _ALARM_OCCUR_MASK, _ALARM_CLEAR_MASK
	// send ALARM_SIG_VALUE content. 
#ifdef _DEBUG_DATA_PROCESS
	TRACEX("Sending alarm %s(%d) %s traps...\n",
		EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID,
		(nNotificationMasks == _ALARM_OCCUR_MASK) ? "begin" : "end");
#endif

#define URGENT_ALARM_LEVEL		(ALARM_LEVEL_MAX-1)
//TRACE("\n nAlarmLevel is: %d\n", nAlarmLevel);
//TRACE("\n nNotificationMasks is: %d\n", nNotificationMasks);
	NotificationFunc(nNotificationMasks,// masks
		sizeof(ALARM_SIG_VALUE),		// the sizeof (ALARM_SIG_VALUE)
		pSig,							// the alarm sig address
		(nAlarmLevel >= URGENT_ALARM_LEVEL) ? TRUE : FALSE);//urgent?
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Equip_GetSpecialSigByMergedId
 * PURPOSE  : get the sig actual address in buffer, by merged id 
 *			  (DXI_MERGE_SIG_ID), if error, will
 *            log it out with the sig id.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO  *pEquip     : 
 *            IN int         nMergedId   : get from DXI_MERGE_SIG_ID 
 * RETURN   : static SIG_BASIC_VALUE *: 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-15 17:45
 *==========================================================================*/
static SIG_BASIC_VALUE *Equip_GetSpecialSigByMergedId(IN EQUIP_INFO *pEquip, 
	IN int nMergedId)
{
	SIG_BASIC_VALUE *pSig;

	pSig = Init_GetEquipSigRefById(pEquip, 
		DXI_SPLIT_SIG_MID_TYPE(nMergedId),
		DXI_SPLIT_SIG_MID_ID(nMergedId));

	/*if (pSig == NULL)
	{
		AppLogOut(EQP_MGR, APP_LOG_UNUSED,
			"The equipment %s(%d) has no such signal: type=%d, id=%d.\n",
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
			DXI_SPLIT_SIG_MID_TYPE(nMergedId), DXI_SPLIT_SIG_MID_ID(nMergedId));
	}*/
	
	return pSig;
}



/*==========================================================================*
 * FUNCTION : Equip_ProcessAlarmSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:04
 *==========================================================================*/
#define	THE_EQUIP_NOT_EXIST			1

static BOOL Equip_ProcessAlarmSignals(IN SITE_INFO *pSite,
									  IN OUT EQUIP_INFO *pEquip)
{

	static time_t       tmInit;
	static BOOL			bFlag = FALSE;
	static BOOL			bTimeFlage = FALSE;
	if(bFlag == FALSE)
	{
		tmInit = time(NULL);
		bFlag = TRUE;

	}
	

	time_t				tmNow = time(NULL);
	BOOL				bDataChanged = FALSE;
	int					i;
	//EQUIP_INFO			*pReleEquip;

	int					nSig;
	ALARM_SIG_VALUE		*pSig;
	
	EXP_CONST			fResult;
	EXP_ELEMENT			result = {EXP_ETYPE_CONST, &fResult};

	int					nNewState, nStdAlarmLevel;

	BOOL				bTheEquipExist = TRUE;
	SIG_BASIC_VALUE		*pSig_EquipExist;	
	
	pSig_EquipExist = Equip_GetSpecialSigByMergedId(pEquip,
							SIG_ID_EQUIP_WORK_STATUS);

	if(tmNow - tmInit < 15 && bTimeFlage == FALSE)
	{
		//TRACE("\n Equip_ProcessAlarmSignals Waiting!!! \n");
		return bDataChanged;
	}


	bTimeFlage = TRUE;

	if((pSig_EquipExist) 
		&& (THE_EQUIP_NOT_EXIST == pSig_EquipExist->varValue.enumValue))
	{
		bTheEquipExist = FALSE;
	}

	// add all alarms to the active alarm list if the alarm->nCurValue != 0.
	pSig = pEquip->pAlarmSigValue;
	nSig = pEquip->pStdEquip->iAlarmSigNum;

	for (i = 0; i < nSig; i++, pSig++)
	{
		// 1. calculate the alarm current state.    
		if (pSig->sv.pAlarmExpression == NULL)   
		{
			continue;// to do next. this alarm may be NOT configured.
		}
		//Added for the Alarm never show up when the alarm is not in the active alarm list but suppressed. Stone Song 20180307
		if(pSig->next == NULL && SIG_VALUS_IS_SUPPRESSED(pSig))
		{
			SIG_STATE_CLR(pSig, VALUE_STATE_IS_SUPPRESSED);
		}
		// get the current alarm level.
		nStdAlarmLevel = pSig->pStdSig->iAlarmLevel;
		if (bTheEquipExist && (nStdAlarmLevel != ALARM_LEVEL_NONE))
		{
			Exp_Calculate(EQP_GET_SIGV_NAME0(pSig),
				pSig->sv.pAlarmExpression,
				pSig->sv.iAlarmExpression, &result);
		    
			if (result.byElementType == EXP_ETYPE_CONST)
			{
				nNewState = EXP_GET_CONST_LONG(&result);
			}
			else
			{

#ifdef END_ALARM_IF_INVALID_RESULT	// end alarm by invalid result.
				nNewState = 0;	// end the alarm.
#else

				continue;			// to do next alarm.
#endif
			}
		}
		else	// treat the current state is 0 (not in alarm)
		{
			nNewState = 0;
		}


		// 1.1 set check the alarm to valid. 
		// the state is already set in equip_mon_init.c:Init_AlarmSig()
		//SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);

		// 1.2 set the new alarm state.
#define		ALARM_IS_TO_ACTIVE			0x01
#define		ALARM_IS_ALREADY_ACTIVE		0x02

		switch ((nNewState ? ALARM_IS_TO_ACTIVE : 0)  
			    |/*not ||*/ (pSig->next ? ALARM_IS_ALREADY_ACTIVE : 0))
		{		
			// 1.2.1 To check the delaying or active alarm level is changed or not.
		case (ALARM_IS_TO_ACTIVE | ALARM_IS_ALREADY_ACTIVE):
			// = if ((nNewState != 0) && (pSig->next != NULL))
			// this alarm is in alarm state, and it's alarming or delaying.
			// check the alarm level is changed or not.
			  
			if (nStdAlarmLevel == pSig->iAlarmLevel)
			{
				break;	//keep the alarm state.
			}

			// level is changed, to end the alarm at first.		
			nNewState = -1;	// -1: indicate the alarm level changed.
			// no break, to continue to end the alarm.
		
			// 1.2.2 To end the delaying or active alarm if new state <= 0.
		case ALARM_IS_ALREADY_ACTIVE:
			// = if ((nNewState <= 0) && (pSig->next != NULL))
			// this alarm is in end state, and is active or is delaying
			if (pSig->sv.nDelayingTime == 0)	// alarm that is active, end it.
			{	
#ifdef DEBUG_HISTORY_ALARMSERIAL			
				TRACE("ALARM_IS_ALREADY_ACTIVE : [%d][%s]\n",pEquip->iEquipID,pSig->szEquipSerial);
#endif				
				Equip_AlarmEnd(pSite,
							pEquip,
							pSig,
							tmNow,
							nNewState,
							(SIG_VALUS_IS_SUPPRESSED(pSig) ? ALARM_ACTION_SAVE_ACTIVE
								: (ALARM_ACTION_REPORT|ALARM_ACTION_SAVE_ALL)));
               
				// the alarm is end normally, remove the suppressing flag
				if (nNewState == 0)
				{
					SIG_STATE_CLR(pSig, VALUE_STATE_IS_SUPPRESSED);
				}
				bDataChanged = TRUE;
			}
			else	// this alarm is in delaying, set the delay time to 0.
			{
				pSig->sv.nDelayingTime = 0;
				pEquip->nDelayingAlarms--;

				// remove the alarm node from corresponding link.
				REMOVE_NODE_SELF_FROM_LINK(pSig);
			}

			// the alarm is end normally.
			if (nNewState == 0)
			{
				break;
			}

			// alarm level is changed, and the old level alarm is ended, and
			// no break; to continue to raise the new level alarm.
		
		
			// 1.2.3 To raise a new alarm with current std alarm level.
		case ALARM_IS_TO_ACTIVE:
			// =if((nNewState > 0) && (pSig->next == NULL))
			// this alarm now is in alarm state, and 
			// it's a new alarm. not in any link
			pSig->iAlarmLevel = nStdAlarmLevel;	// set new alarm level.

			
			if (pSig->pStdSig->iAlarmDelay == 0)	// no delay if alarmed
			{
				Equip_AlarmBegin(pSite, pEquip, pSig, tmNow,
					ALARM_ACTION_SAVE_ACTIVE);				

				bDataChanged = TRUE;
			}
			else 	// this alarm need delay a while.
			{
				ALARM_SIG_VALUE		*pSigLink;

				pSig->sv.nDelayingTime = pSig->pStdSig->iAlarmDelay;
				pSig->sv.tmStartTime   = tmNow;
				pSig->sv.tmEndTime     = INVALID_TIME;
				
				// put the alarm to head of the active alarm list
				pSigLink = &pEquip->pDelayingAlarms[0];
				INSERT_NODE_TO_LINK_HEAD(pSigLink, pSig);

				pEquip->nDelayingAlarms++;
				
			}

			//To do NOTHING.
		default:	// 0x00: // the alarm in last and now are not active.
			;
		}

	}//End for


	/*if(bDataChanged)
	{
		if((pEquip->iEquipID == SYS_EQUIP_ID) 
			|| (pEquip->iEquipID == RECTG_EQUIP_ID) 
			|| (pEquip->iEquipID == ACDG_EQUIP_ID) 
			|| (pEquip->iEquipID == DCDG_EQUIP_ID) 
			|| (pEquip->iEquipID == BATG_EQUIP_ID))
		{
			for(j = 0; j < pEquip->iEquipReferredThis; j++)
			{
				pReleEquip = pEquip->ppEquipReferredThis[j];
				if (pReleEquip->iProcessingState == EQP_STATE_PROCESS_IDLE)
				{
					pReleEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
				}
			}
		}		
	}	*/

	return bDataChanged;
}


/*==========================================================================*
 * FUNCTION : Site_ProcessDelayingAlarms
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:04
 *==========================================================================*/
static BOOL Site_ProcessDelayingAlarms(IN SITE_INFO *pSite)
{
	// if an alarm delayed timeout, the alarm is actived.
	BOOL			bDataChanged = FALSE;
	int				i;
	EQUIP_INFO		*pEquip;

	//TRACEX("Processing delaying alarm signals...\n");

	// proccess the equipment alarms if its data is changed
	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		if ((pEquip->nDelayingAlarms > 0) &&
			Equip_ProcessDelayingAlarms(pSite, pEquip))
		{
			bDataChanged = TRUE;
			pEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
		}
	}

	return bDataChanged;
}



/*==========================================================================*
 * FUNCTION : Equip_ProcessDelayingAlarms
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:04
 *==========================================================================*/
static BOOL Equip_ProcessDelayingAlarms(IN SITE_INFO *pSite,
										IN OUT EQUIP_INFO *pEquip)
{
	time_t				tmNow = time(NULL);
	BOOL				bDataChanged = FALSE;

	ALARM_SIG_VALUE		*pSig, *pHead, *pSaveNext;

	int					nElapsedTime;

	if (pEquip->nDelayingAlarms == 0)
	{
		return FALSE;
	}

	pHead = &pEquip->pDelayingAlarms[0];
	pSig  = pHead->next;
 
	// the link is a bilink.
	while (pSig != pHead)
	{
		ASSERT(pSig->pStdSig != NULL);
		
		// save the next, which would be removed this delaying link
		// if the alarm is active.
		pSaveNext = pSig->next;

		// get the elapsed time from last time.
		nElapsedTime = tmNow - pSig->sv.tmStartTime;

		// please do NOT use the following test condition:
		//if (nElapsedTime >= pSig->sv.nDelayingTime)
		//{
		//	// alarming....
		//}
		//Changed by Marco, 20160223, ��ĸ�澯��ʧʱ�����θ澯�����ϲ������������ز��Խ���ʱ������Ƿѹ��澯��
		//���澯������ʱ����ʱ���ټ��㡣
		if(SIG_VALUS_IS_SUPPRESSED(pSig))
		{
			pSig->sv.tmStartTime = tmNow;
			pSig = pSaveNext;			
			continue;
		}
#define MAX_ALLOWED_PROCESSING_TIME		10 /* seconds */
		// time changed to future or back, treat the elapsed time as 1s.
		if ((nElapsedTime >= MAX_ALLOWED_PROCESSING_TIME) ||   //old logic is &&, 
			(nElapsedTime < 0))		// time changed to back.   //new logic is ||, 
		{                                                      //changed by : zyl ,date : 2007/07/17
			nElapsedTime = 1;
		}
       
		if (nElapsedTime > 0)
		{
			pSig->sv.nDelayingTime -= nElapsedTime;
			pSig->sv.tmStartTime    = tmNow;

			// now this alarm will really alarming.
			if (pSig->sv.nDelayingTime <= 0)
			{
				// remove the alarm node from corresponding link at first.
				REMOVE_NODE_SELF_FROM_LINK(pSig);

				pEquip->nDelayingAlarms--;
				pSig->sv.nDelayingTime = 0;

				// the alarm level is already set.
				// to begin the alarm.
				Equip_AlarmBegin(pSite, pEquip, pSig, tmNow,
					ALARM_ACTION_SAVE_ACTIVE);

				bDataChanged = TRUE;
			}
		}

		pSig = pSaveNext;
	}

	return bDataChanged;
}

#define	RELAY_CTRL_TASK			"Relay"

#define RELAY_STATUS_OPEN		0
#define RELAY_STATUS_CLOSE		1

#define SIG_ID_FIXED_REPORT_WAY	48
#define	FIXED_REALY_OUTPUT_ENABLED		0

#define SIG_ID_OA_NUM			29
#define SIG_ID_MA_NUM			30
#define SIG_ID_CA_NUM			31

#define SIG_ID_OA_SUMMARY		150
#define SIG_ID_MA_SUMMARY		151
#define SIG_ID_CA_SUMMARY		152

#define	SIG_ID_FAIL_SAFE		195
#define	SIG_ID_RELAY_TEST		222

/*==========================================================================*
 * FUNCTION : Site_ProcessAlarmRelays
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2007-02-13 15:15
 *==========================================================================*/
//static BOOL  Is_IBEIBComFailResetRelays(void)
//{
//	static SIG_BASIC_VALUE *pSigIBComFail[2] = {NULL};
//	static SIG_BASIC_VALUE *pSigEIBComFail[2] ={ NULL};
//	
//	static BOOL s_bIBComFail[2]={FALSE};
//	static BOOL s_bEIBComFail[2]={FALSE};
//	static BOOL s_bFirstRun=TRUE;
//	BOOL bRelayReseted=FALSE;
//	BOOL bRelayNeedReset[MAX_RELAY_NUM]={FALSE};
//	BOOL bIBComFail[2]={FALSE};
//	BOOL bEIBComFail[2]={FALSE};
//
//	int i=0,j=0;
//	if(s_bFirstRun)
//	{
//		pSigIBComFail[0]=(SIG_BASIC_VALUE *)Init_GetEquipSigRefById(202,SIG_TYPE_SAMPLING,99);
//		pSigIBComFail[1]=(SIG_BASIC_VALUE *)Init_GetEquipSigRefById(203,SIG_TYPE_SAMPLING,99);
//
//		pSigEIBComFail[2]=(SIG_BASIC_VALUE *)Init_GetEquipSigRefById(197,SIG_TYPE_SAMPLING,99);  //EIB
//		pSigEIBComFail[3]=(SIG_BASIC_VALUE *)Init_GetEquipSigRefById(198,SIG_TYPE_SAMPLING,99);     //EIB2 
//		s_bFirstRun=FALSE;
//	}
//	if(pSigIBComFail[0]==NULL
//		||pSigIBComFail[1]==NULL
//		|| pSigEIBComFail[0]==NULL
//		|| pSigEIBComFail[1]==NULL)
//	{
//		return FALSE;	
//	}
//	for(i=0;i<2;i++)	   //IB
//	{
//		bIBComFail[i]=pSigIBComFail[i]->varValue.enumValue;
//		if(     bIBComFail[i]==FALSE &&
//			s_bIBComFail[i]==TRUE)
//		{
//			//1~8   18~25  IB Relay 
//			for(j=0;j<8;j++)
//			{
//				bRelayNeedReset[ALARM_RELAY_FIRST-1+j+17*i]=TRUE;
//			}
//		}
//	}
//	 s_bIBComFail[0]=bIBComFail[0];
//	  s_bIBComFail[1]=bIBComFail[1];
//	for(i=0;i<2;i++)	   //EIB
//	{
//		bEIBComFail[i]=pSigEIBComFail[i]->varValue.enumValue;	
//		if(     bEIBComFail[i]==FALSE &&
//			s_bEIBComFail[i]==TRUE)
//		{
//			//9~13    26~30 
//			for(j=0;j<4;j++)
//			{
//				bRelayNeedReset[ALARM_RELAY_9TH-1+j+i*17]=TRUE;
//			}
//		}
//	}
//        s_bEIBComFail[0]=bEIBComFail[0];
//	  s_bEIBComFail[1]=bEIBComFail[1];
//
//	for(i=0;i<   MAX_RELAY_NUM;i++)
//	{
//		if(bRelayNeedReset[i]==TRUE)
//		{
//			AlarmRelayAction(i, RELAY_STATUS_OPEN);
//			bRelayReseted=TRUE;
//			printf("IB/EIB Board comm Fail ,reset relay [%d] \n",i);
//		}
//	}
//	
//	return bRelayReseted ;	
//}
static BOOL Site_ProcessAlarmRelays(IN SITE_INFO *pSite)
{
	int					i, j;
	EQUIP_INFO			*pEquip;
	int					iRelayNo;
	BOOL				bRelayOutput[MAX_RELAY_NUM];
	BOOL				bRelayNeedControl[MAX_RELAY_NUM];
	static	BOOL		s_bLastRelayNeedControl[MAX_RELAY_NUM]={FALSE};
	int					iAlarmLevel = 0;
	ALARM_SIG_VALUE		*pHead, *pSig;
	SIG_BASIC_VALUE		*pSigTemp = NULL;
	EQUIP_INFO			*pSysEquip = NULL;
	STDEQUIP_TYPE_INFO 	*pStdEquip;
	ALARM_SIG_INFO		*pAlarmSig; 
	SIG_ENUM		enFailSafe;

	//SIG_BASIC_VALUE		*pSigOutgoingBlock = NULL;

#ifndef   GC_SUPPORT_HIGHTEMP_DISCONNECT
	// proccess the equipment alarms if its data is changed
	for(i = 0; i < MAX_RELAY_NUM; i++)
	{
		bRelayOutput[i] = FALSE;
		bRelayNeedControl[i] = FALSE;
	}

	pStdEquip = pSite->pStdEquipTypeInfo;
	for (i = 0; i < pSite->iStdEquipTypeNum; i++, pStdEquip++)
	{
		pAlarmSig = pStdEquip->pAlarmSigInfo; 
		for(j = 0; j < pStdEquip->iAlarmSigNum; j++, pAlarmSig++)
		{
			//fix the relay doesn't turn to INACTIVE from ACTIVE when the alarm level turn to NA by SongXu 20170527
			if(pAlarmSig->iAlarmRelayNo)
			//if(pAlarmSig->iAlarmLevel && pAlarmSig->iAlarmRelayNo)
			{
				bRelayNeedControl[pAlarmSig->iAlarmRelayNo - 1] = TRUE;
			}
		}
	}

	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		for(iAlarmLevel = 1; iAlarmLevel < ALARM_LEVEL_MAX; iAlarmLevel++)
		{			      
			pHead = &pEquip->pActiveAlarms[iAlarmLevel];
			pSig = pHead->next;

			for (; pSig != pHead; pSig = pSig->next)
			{
				//If alarm is suppressed, don't relay control.
				if(pSig->pStdSig->iAlarmRelayNo > 0 && !SIG_VALUS_IS_SUPPRESSED(pSig))
				{
					iRelayNo = pSig->pStdSig->iAlarmRelayNo;	
					bRelayOutput[iRelayNo - 1] = TRUE;
				}
			}		
		}
	}

//////////////////////////////////////////////////////////////////////////

	//Fix ��ʽ��gc_run_info.c��FixedRelayCtl()�д���

	DWORD	adAlarmNum[ALARM_LEVEL_MAX - 1] = {0};
	int		aiAlarmRelayNo[ALARM_LEVEL_MAX - 1] = {0};
	ALARM_SIG_INFO *pAlarmSummarySig;
	int iBufLen;

	enFailSafe = GetEnumSigValue(1, SIG_TYPE_SETTING, SIG_ID_FAIL_SAFE, RELAY_CTRL_TASK);

	for(i = 0; i < ALARM_LEVEL_MAX - 1; i++)	// 0 ~ 2
	{
		adAlarmNum[i] = GetDwordSigValue(1, SIG_TYPE_SAMPLING, SIG_ID_OA_NUM + i, RELAY_CTRL_TASK);
		int	iError = DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
			STD_ID_ACU_SYSTEM_EQUIP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, SIG_ID_OA_SUMMARY + i),
			&iBufLen,			
			(void *)&pAlarmSummarySig,			
			0);

		if(iError == ERR_DXI_OK)
		{
			aiAlarmRelayNo[i] = pAlarmSummarySig->iAlarmRelayNo - 1;

		}
		else
		{
			aiAlarmRelayNo[i] = -1;
		}

	}


	if(FIXED_REALY_OUTPUT_ENABLED 
		!= GetEnumSigValue(1, SIG_TYPE_SETTING, SIG_ID_FIXED_REPORT_WAY, RELAY_CTRL_TASK))
	{
		

		for(i = 0; i < ALARM_LEVEL_MAX - 1; i++)	// 0 ~ 2
		{

			if(aiAlarmRelayNo[i] >= 0)
			{
				bRelayNeedControl[aiAlarmRelayNo[i]] = TRUE;

				if(i < 2)	//OA or MA summary,������ļ�����ȡ"��"
				{
					if(adAlarmNum[i] > 0)
					{
						bRelayOutput[aiAlarmRelayNo[i]] = TRUE;
					}
				}
				else		//CA summary, �߼��Ƿ��ģ���ҪEnergise�������������ĳ�ͻ���Դ�Ϊ׼
				{
					/*Changed by YangGuoxin:Add function failsafe, for CA,�߼��Ƿ�ȡ�����������û�����,2010-12-22*/
					
					if(enFailSafe == 0)//Disable
					{
						bRelayOutput[aiAlarmRelayNo[i]] |= (adAlarmNum[i] > 0); //add OR in case clear the relay when it actived before.
					}
					else		//Enable
					{
						bRelayOutput[aiAlarmRelayNo[i]] = !(adAlarmNum[i] > 0);
					}
					/*End by Yangguoxin,2010-12-22*/
				}
			}
			

			//TRACE("i = %d, AlarmNum = %d, AlarmRelay = %d.\n", i, adAlarmNum[i], aiAlarmRelayNo[i]);
		}
	}

//////////////////////////////////////////////////////////////////////////
#ifdef GC_SUPPORT_RELAY_TEST	
	if(GetEnumSigValue(1, SIG_TYPE_SETTING, SIG_ID_RELAY_TEST, RELAY_CTRL_TASK) != 0)//If Relay test Enable, don't control relay 
	{
		return FALSE;
	}
	
#endif
	////IB EIB  ͨ���жϻָ����ȸ�λ IB EIB �Լ� ���Ӧ�Ŀ����ź�ֵ 
	//if(Is_IBEIBComFailResetRelays())
	//{
	//	return TRUE;	
	//}
	for(i = 0; i < MAX_RELAY_NUM; i++)
	{
		if(bRelayNeedControl[i])
		{	
			if(bRelayOutput[i] && (!pSite->bAlarmOutgoingBlocked))
			{
				//Check the relay status, if status is closed, do nothing, else close it
				if(RELAY_STATUS_OPEN == GetRelayStatus(i))
				{
					//Execute
					AlarmRelayAction(i, RELAY_STATUS_CLOSE);
				}
			}
			else
			{
				
				if(i == aiAlarmRelayNo[2] && enFailSafe != 0 && pSite->bAlarmOutgoingBlocked)
				{
					if( RELAY_STATUS_OPEN == GetRelayStatus(i))
					{
						//Execute
						AlarmRelayAction(i, RELAY_STATUS_CLOSE);
					}

				}
				else//Check the relay status, if status is open, do nothing, else open it
				{
					if(RELAY_STATUS_CLOSE == GetRelayStatus(i))
					{
						//Execute
						AlarmRelayAction(i, RELAY_STATUS_OPEN);
					}

				}
				
			}	
		}
		//else				 
		//{
		//	if( s_bLastRelayNeedControl[i]==TRUE)		 //	 �澯������ȡ�����ָ�DO��open
		//	{
		//		if(RELAY_STATUS_CLOSE == GetRelayStatus(i))
		//		{
		//			//Execute
		//			AlarmRelayAction(i, RELAY_STATUS_OPEN);
		//		}
		//		s_bLastRelayNeedControl[i]=FALSE;
		//		printf("Alarm Assign is removed ,Do [%d]open \n",i);
		//	}
		//}

	}
	/*for(i = 0; i < MAX_RELAY_NUM; i++)
	{
		 s_bLastRelayNeedControl[i]=bRelayNeedControl[i];
	}*/
#endif   GC_SUPPORT_HIGHTEMP_DISCONNECT

	return FALSE;	//FALSE means no data changed.
}



/*==========================================================================*
 * FUNCTION : Site_ProcessAlarmSuppressions
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:04
 *==========================================================================*/
static BOOL Site_ProcessAlarmSuppressions(IN SITE_INFO *pSite)
{
	BOOL			bDataChanged = FALSE;
	int				i;
	EQUIP_INFO		*pEquip;


#ifdef _DEBUG_DATA_PROCESS
	//TRACEX("Processing alarm suppressions...\n");
#endif //_DEBUG_DATA_PROCESS

	// proccess the equipment alarms if its data is changed
	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		if (pEquip->iProcessingState == EQP_STATE_PROCESS_DOING)
		{
			bDataChanged |= Equip_ProcessAlarmSuppressions(pSite, pEquip);
			pEquip->iProcessingState = EQP_STATE_PROCESS_IDLE;
		}
	}

	return bDataChanged;
}


/*==========================================================================*
 * FUNCTION : Site_ResetProcessingState
 * PURPOSE  : reset the process state of the equipment to IDLE.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-15 12:16
 *==========================================================================*/
static BOOL Site_ResetProcessingState(IN SITE_INFO *pSite)
{
	int				i;
	EQUIP_INFO		*pEquip;

	// proccess the equipment alarms if its data is changed
	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		pEquip->iProcessingState = EQP_STATE_PROCESS_IDLE;
	}

	return TRUE;
}

#ifdef SAVE_ACTIVE_ALARMS

/*==========================================================================*
 * FUNCTION : Equip_SaveActiveAlarms
 * PURPOSE  : save all active alarms.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO   *pSite  : 
 *            IN EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-19 10:13
 *==========================================================================*/
static BOOL Equip_SaveActiveAlarms(IN SITE_INFO *pSite,
									   IN EQUIP_INFO *pEquip)
{
	int					nLevel;
	ALARM_SIG_VALUE		*pSig, *pHead;

#ifdef _DEBUG_DATA_PROCESS
	//TRACEX("Equip_SaveActiveAlarms %s(%d)...\n",
	//	EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
#endif //_DEBUG_DATA_PROCESS

	// test the reporting state of the alarms of all levels.
	for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		pHead = &pEquip->pActiveAlarms[nLevel];
		pSig  = pHead->next;

		// the link is a bilink.
		for (; pSig != pHead; pSig = pSig->next)
		{
			Equip_AlarmSave(pSite, pEquip, pSig, ALARM_ACTION_SAVE_ACTIVE);
		}
	}

#ifdef _DEBUG_DATA_PROCESS
	//TRACEX("Equip_SaveActiveAlarms %s(%d) OK.\n",
	//	EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
#endif //_DEBUG_DATA_PROCESS

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Site_SaveActiveAlarms
 * PURPOSE  : to avoid conflict of saving active alarms.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:04
 *==========================================================================*/
static BOOL Site_SaveActiveAlarms(IN SITE_INFO *pSite)
{
	int				i;
	EQUIP_INFO		*pEquip;

	pSite->nCurrentSavedAlarms = 0;

	/*AppLogOut(EQP_MGR, APP_LOG_INFO,
		"Re-saving all active alarms to storage...\n");*/ 

	// report the changed alarm to the service.
	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		Equip_SaveActiveAlarms(pSite, pEquip);
	}

	/*AppLogOut(EQP_MGR, APP_LOG_INFO,
		"All active alarms are resaved to storage OK.\n");*/ 

	return TRUE;
}
#endif


/*==========================================================================*
 * FUNCTION : Equip_UpdateSamplingSigAlarmState
 * PURPOSE  : update the iRelatedAlarmLevel of sampling signal when 
 *            Alarm_Begin and Alarm_End
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN ALARM_SIG_VALUE  *pSigAlarm : 
 * RETURN   : static BOOL : always TRUE
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-19 16:55
 *==========================================================================*/
static BOOL Equip_UpdateSamplingSigAlarmState(ALARM_SIG_VALUE *pSigAlarm)
{
	int					i, n;
	EXP_ELEMENT			*pExp	  = pSigAlarm->sv.pAlarmExpression;
	int					nElements = pSigAlarm->sv.iAlarmExpression;
	SAMPLE_SIG_VALUE	*pSig;
	ALARM_SIG_VALUE		*pAlarm;
	int					nMaxAlarmLevel;

	if ((pExp == NULL) || (nElements == 0))
	{
		return TRUE;
	}

	for (i = 0; i < nElements; i++, pExp++)
	{
		if (pExp->byElementType == EXP_ETYPE_VARIABLE)
		{
			//1. find the related sampling signal
			pSig = (SAMPLE_SIG_VALUE *)pExp->pElement;
			if (pSig->bv.ucSigType != SIG_TYPE_SAMPLING)
			{
				continue;	// not a sampling signal to check the next var;
			}

			// at least has this alarm ref.
			ASSERT((pSig->sv.ppRelatedAlarms != NULL) &&
				(pSig->sv.iRelatedAlarms != 0));

			//2. get the max active alarm level.
			nMaxAlarmLevel = ALARM_LEVEL_NONE;
			for (n = 0; n < pSig->sv.iRelatedAlarms; n++)
			{
				pAlarm = pSig->sv.ppRelatedAlarms[n];
				ASSERT(pAlarm != NULL);

				if ((pAlarm->bv.varValue.lValue != 0) &&
					(nMaxAlarmLevel < pAlarm->iAlarmLevel))
				{
					nMaxAlarmLevel = pAlarm->iAlarmLevel;
				}
			}

			//3. set the max level to the sampling signal
			pSig->sv.iRelatedAlarmLevel = nMaxAlarmLevel;

#ifdef _DEBUG_DATA_PROCESS
			if (nMaxAlarmLevel != ALARM_LEVEL_NONE)
			{
				TRACEX("Set the sampling sig %s(%d) related alarm level to %d.\n",
					EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID,
					nMaxAlarmLevel);
			}
#endif //_DEBUG
		}
	}

	return TRUE;
}
static BOOL BatteryCommInterrupt(EQUIP_INFO *pBatEquip)
{
	SIG_BASIC_VALUE		*pSig = (SIG_BASIC_VALUE *)pBatEquip->pCommStatusSigRef;
	
	if (pSig != NULL)
	{	
		if(!SIG_VALUE_IS_VALID((SAMPLE_SIG_VALUE *)pSig))
		{
			SIG_STATE_SET(pSig, VALUE_STATE_IS_CONFIGURED);
			SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);
		}

		return	((pSig->varValue.ulValue == 1) ? TRUE : FALSE);	
	}
		
	return FALSE;
}

//process the suppression based on the active alarm list
/*==========================================================================*
 * FUNCTION : Equip_ProcessAlarmSuppressions
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 19:04
 *==========================================================================*/
static BOOL Equip_ProcessAlarmSuppressions(IN SITE_INFO *pSite,
										   IN OUT EQUIP_INFO *pEquip)
{
	time_t				tmNow = time(NULL);
	BOOL				bDataChanged = FALSE;

	ALARM_SIG_VALUE		*pSig, *pHead;

	EXP_CONST			fResult;
	EXP_ELEMENT			result = {EXP_ETYPE_CONST, &fResult};

	BOOL				bEquipSuppressed = FALSE;
	BOOL				bAlarmSuppressed;

	int					nLevel;

	// 1. calculate the suppression of the equipment, if the equipment is
	//    suppressed, all the active alarms of the equipment are suppressed.
	if (pEquip->pAlarmFilterExpression != NULL)
	{
		Exp_Calculate(EQP_GET_SIGV_NAME0(pSig),
			pEquip->pAlarmFilterExpression, 
			pEquip->iAlarmFilterExpression, &result);

		if ((result.byElementType == EXP_ETYPE_CONST) && 
			(EXP_GET_CONST_LONG(&result) != 0))
		{
			bEquipSuppressed = TRUE;
		}

		// set the equipment alarm suppressed flag.
		pEquip->bAlarmsSuppressed = bEquipSuppressed;
	}

	if(pEquip->iEquipTypeID == BATTERY_TYPE_ID)
	{
		if(BatteryCommInterrupt(pEquip))
		{
			bEquipSuppressed = TRUE;
		}
	}

	// test the suppression state of the alarms of all levels.
	for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		pHead = &pEquip->pActiveAlarms[nLevel];
		pSig  = pHead->next;
		
		// the link is a bilink.
		for (; pSig != pHead; pSig = pSig->next)
		{
			// it is NOT alarming, do NOT need calculate suppression state.
			if (pSig->bv.varValue.lValue == 0)
			{				
				ALARM_STATE_CLR(pSig);// clear alarm state flag
				continue;	// to process the next alarm.
			}

			// calc the suppression state of the alarm.
			if (bEquipSuppressed) // the equipment is suppressed
			{
				bAlarmSuppressed = TRUE;
			}
			else if (pSig->sv.pSuppressExpression != NULL)
			{
				Exp_Calculate(EQP_GET_SIGV_NAME0(pSig),
					pSig->sv.pSuppressExpression, 
					pSig->sv.iSuppressExpression, &result);

				// if the suppresson condition of ALARM is invalid,
				// do NOT do anything,  keep the old status of the ALARM.
				if (result.byElementType == EXP_ETYPE_CONST)
				{
					bAlarmSuppressed = 
						(EXP_GET_CONST_LONG(&result) != 0) ? TRUE : FALSE;
				}
				else
				{
					continue;// keep the old suppression status.
				}
			}

			else	// no suppression expression, the alarm so is not suppressed
			{
				bAlarmSuppressed = FALSE;
			}
		
			// if an alarm is suppressed, we only end the alarm, do NOT remove
			// it from the active alarm list. and set the status of the alarm 
			// to VALUE_STATE_IS_SUPPRESSED.
			if (bAlarmSuppressed)				// the alarm is suppressed
			{
				if (!SIG_VALUS_IS_SUPPRESSED(pSig))	// it has NOT been suppressed.
				{

					// the alarm is suppressed, set the flag at first.
					SIG_STATE_SET(pSig, VALUE_STATE_IS_SUPPRESSED);
					//pSig->bv.varValue.lValue = 0;
					// end the alarm but do NOT remove the alarm from the 
					// active alarm list.
					
					
					Equip_AlarmEnd(pSite, pEquip, pSig, tmNow,
						            1,
						            (ALARM_ACTION_REPORT|ALARM_ACTION_SAVE_ALL));

					bDataChanged = TRUE;
				}
			}

			// the alarm conditions are disappeared and it is being 
			// suppressed now, the alarm re-begins.
			else if (SIG_VALUS_IS_SUPPRESSED(pSig))
			{
				// the alarm now is normal, clear the suppressed flag and begin.
				SIG_STATE_CLR(pSig, VALUE_STATE_IS_SUPPRESSED);

				// begin the alarm but do NOT insert the alarm to the 
				// active alarm list.
				Equip_AlarmBegin(pSite, pEquip, pSig, tmNow, 
					(ALARM_ACTION_REPORT | ALARM_ACTION_SAVE_ACTIVE));
#ifdef _DEBUG_DATA_PROCESS
				if(pEquip->iEquipTypeID==301)
				{
					TRACE("___B____Alarm %s(%d) of equipment %s(%d) is suppressed ,alarm is %d ,suppress is %d\n",
					//	"and ended...\n",
						EQP_GET_SIGV_NAME0(pSig),    pSig->pStdSig->iSigID,
						EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
						pSig->bv.varValue.lValue,
						SIG_VALUS_IS_SUPPRESSED(pSig));
				}
#endif //_DEBUG_DATA_PROCESS
				bDataChanged = TRUE;

			}

			else if (SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_BEGIN))
			{
#ifdef _DEBUG_DATA_PROCESS
				if(pEquip->iEquipTypeID==301)
				{
					TRACE("___C____Alarm %s(%d) of equipment %s(%d) is suppressed ,alarm is %d ,suppress is %d \n",
					//	"and ended...\n",
						EQP_GET_SIGV_NAME0(pSig),    pSig->pStdSig->iSigID,
						EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
						pSig->bv.varValue.lValue,
						SIG_VALUS_IS_SUPPRESSED(pSig));
				}
#endif //_DEBUG_DATA_PROCESS
				Equip_AlarmReport(pSite, pEquip, pSig);
			}

			ALARM_STATE_CLR(pSig);// clear alarm state flag
		}
	}

	return bDataChanged;
}



///////////////////////////////////////////////////////////////////////////
//// special process //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
static BOOL StdEquip_ACU_ProcessSpecialSignals(IN SITE_INFO *pSite);
static BOOL StdEquip_AC_ProcessSpecialSignals(IN SITE_INFO *pSite);
static BOOL StdEquip_MultiSMAC_ProcessSpecialSignals(IN SITE_INFO *pSite);
static BOOL StdEquip_FuelManagement_ProcessSpecialSignals(IN SITE_INFO *pSite);
static BOOL StdEquip_TempSig_ProcessSpecialSignals(IN SITE_INFO *pSite);

/*==========================================================================*
 * FUNCTION : Site_ProcessSpecialSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)          DATE: 2005-01-27 16:29
 *==========================================================================*/
static BOOL Site_ProcessSpecialSignals(IN SITE_INFO *pSite)
{	
	static int s_ifirstFlage = 0;
	if(s_ifirstFlage == 0)
	{
		DHCPEnableFunc(pSite);
		s_ifirstFlage = 1;
	}
	return (StdEquip_ACU_ProcessSpecialSignals(pSite) | StdEquip_IBEIB_ProcessSpecialSignals(pSite) | DHCPSetFunc(pSite) |
		StdEquip_AC_ProcessSpecialSignals(pSite)  | StdEquip_MultiSMAC_ProcessSpecialSignals(pSite) | StdEquip_FuelManagement_ProcessSpecialSignals(pSite) |
		StdEquip_TempSig_ProcessSpecialSignals(pSite) | StdEquip_ProcessLVD3Signals(pSite) |StdEquip_Source_ProcessSpecialSignals(pSite));
}
/*==========================================================================*
 * FUNCTION : IsIB4Failed
 * PURPOSE  : check the IB4 communication 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  : 
 * RETURN   :  BOOL : 
 * COMMENTS : 
 * CREATOR  : Song Xu          DATE: 2016-11-1 16:29
 *==========================================================================*/
BOOL IsIB4Failed() 
{
	char cCmd[48]="ifconfig | grep eth1 > /var/appInitIB4";
	char cCmd1[25]="rm /var/appInitIB4";
	int iReturn;
	iReturn = _SYSTEM(cCmd);
//	printf("iReturniReturniReturn%d \n",iReturn);
	if (0 == iReturn) //_SYSTEM_ERR_OK
	{
		_SYSTEM(cCmd1);
		return TRUE;
	}
	else
	{
		_SYSTEM(cCmd1);
		return FALSE;
	}
}

//Get the ethernet number of ACU
#define ETHERNET_NUMBER_FILEForIB4	"/home/app_script/ethernet.log"

static int GetEthernetNumForIB4(void)
{
    FILE *in;

    char line [256];

    if (! (in = fopen (ETHERNET_NUMBER_FILEForIB4, "r")))
    {
	TRACEX("cannot open /home/app_script/ethernet.log - burps\n\r");
	return ETHERNET_NUMBER_1;
    }

    while (fgets (line, sizeof(line), in))
    {
	if (strstr(line, "eth:1")) 
	{
	    fclose(in);
	    return ETHERNET_NUMBER_1;
	}
	else if (strstr(line, "eth:2"))
	{
	    fclose(in);
	    return ETHERNET_NUMBER_2;
	}
	else
	{
	    fclose(in);
	    return ETHERNET_NUMBER_1;
	}
    }

    fclose(in);

    return ETHERNET_NUMBER_1;
}

/*==========================================================================*
 * FUNCTION : StdEquip_ACU_ProcessSpecialSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)          DATE: 2005-01-27 16:29
 *==========================================================================*/
static BOOL StdEquip_ACU_ProcessSpecialSignals(IN SITE_INFO *pSite)
{
	int		i;
	static BOOL			bInited = FALSE;
	static EQUIP_INFO	*pEquip = NULL;
	static int iEthNum = -1;
	static int iIntervalTimes = 0;
//	static SIG_BASIC_VALUE *pSig_HwSw = NULL;
	static SIG_BASIC_VALUE *pSig_SysStatus = NULL;
	static SIG_BASIC_VALUE *pSig_AlarmBlocked = NULL;
	static SIG_BASIC_VALUE *pSig_InternalStatus = NULL;
	static SIG_BASIC_VALUE *pSig_ConfigStatus = NULL;

	static SIG_BASIC_VALUE *pSig_MaintnRun = NULL;// Maintenance time run
	static SIG_BASIC_VALUE *pSig_MaintnDelay = NULL;// Maintenance time delay
	static SIG_BASIC_VALUE *pSig_MaintnLimit = NULL;// Maintenance time limi

	static SAMPLE_SIG_VALUE *pSig_SystemPower = NULL;
	static SIG_BASIC_VALUE *pSig_PowerPeak24Hr = NULL;
	static SIG_BASIC_VALUE *pSig_PowerAvg24Hr = NULL;

	static SIG_BASIC_VALUE *pSig_SystemManagementMode = NULL;//Auto/Man state
	static SIG_BASIC_VALUE *pSig_HwSwitch = NULL;// hardware switch

	static SIG_BASIC_VALUE *pSig_AlarmCount[ALARM_LEVEL_MAX];

	static SIG_BASIC_VALUE	*pSig_SMDUSampMode = NULL;

	static  SIG_BASIC_VALUE *pSig_IB4CommStatus = NULL;
	
	time_t	tmNow = time(NULL);
	BOOL	bChanged = FALSE;
	VAR_VALUE var;

	//1. init one time
	if (!bInited)
	{
		bInited = TRUE;
		//1.1.Get Power system equip address
		pEquip = Init_GetEquipRefByStdId(pSite, STD_ID_ACU_SYSTEM_EQUIP);
		if (pEquip == NULL)
		{
			AppLogOut(EQP_MGR, APP_LOG_WARNING,
				"Fails on get equip ref by SCU Plus Std equipment ID %d.\n",
				STD_ID_ACU_SYSTEM_EQUIP);
			return FALSE;
		}

		// init hd sw -- is already processed in OS with envirment var.

		//1.2. init get system status signal value address 
		pSig_SysStatus = Equip_GetSpecialSigByMergedId(pEquip,
			SIG_ID_SYSTEM_STATUS);
		//1.3.Add the different type alarm count signal value address,
		//    used to set count of different alarm
		if (pSig_SysStatus != NULL)
		{
			int		nSigID[ALARM_LEVEL_MAX] = {SIG_ID_TOTAL_ALARM,	
											   SIG_ID_TOTAL_OA,		
											   SIG_ID_TOTAL_MA,		
											   SIG_ID_TOTAL_CA};
			

			for (i = 0; i < ALARM_LEVEL_MAX; i++)
			{
				pSig_AlarmCount[i] = Equip_GetSpecialSigByMergedId(pEquip,
                    nSigID[i]);
			}
		}

		//1.4.Get alarm outgoing signal value address
		pSig_AlarmBlocked = Equip_GetSpecialSigByMergedId(pEquip, 
			SIG_ID_ALARM_OUTGOING_BLOCKED);
		if (pSig_AlarmBlocked != NULL)
		{
			pSig_AlarmBlocked->tmCurrentSampled = tmNow;
		}
		//1.5 Get system interal status signal value address
		pSig_InternalStatus = Equip_GetSpecialSigByMergedId(pEquip, 
			SIG_ID_SYS_INTERNAL_STATUS);
		//1.6 Get system maintenance time run signal value address
		pSig_MaintnRun = Equip_GetSpecialSigByMergedId(pEquip, 
			SIG_ID_MAINTN_TIME_RUN);
		if (pSig_MaintnRun != NULL)
		{
			if (pSig_MaintnRun->ucType == VAR_FLOAT)
			{
				pSig_MaintnRun->tmCurrentSampled = tmNow;
			}
			else	// the type must be float.
			{
				AppLogOut(EQP_MGR, APP_LOG_WARNING,
					"Error, the sig value type of %s(%d) is NOT float.\n",
					EQP_GET_SIGV_NAME0((SET_SIG_VALUE *)pSig_MaintnRun), 
					((SET_SIG_VALUE *)pSig_MaintnRun)->pStdSig->iSigID);
				pSig_MaintnRun = NULL;
			}
		}
		//1.7 Get system maintenance time delay signal value address
		pSig_MaintnDelay = Equip_GetSpecialSigByMergedId(pEquip, 
			SIG_ID_MAINTN_DELAY);

		//1.8 Get system maintenance time limit signal value address
		pSig_MaintnLimit = Equip_GetSpecialSigByMergedId(pEquip, 
			SIG_ID_MAINTN_LIMIT);

		// if the relevant signal not found, treat maintnrun is NULL.
		if ((pSig_MaintnDelay == NULL) || (pSig_MaintnLimit == NULL))
		{
			pSig_MaintnRun = NULL;
		}
		
		//1.9.Following three signal has been deleted in scuplus
		
		pSig_SystemPower = (SAMPLE_SIG_VALUE *)Equip_GetSpecialSigByMergedId(
			pEquip, SIG_ID_SYSTEM_POWER);

		pSig_PowerPeak24Hr = Equip_GetSpecialSigByMergedId(pEquip,
			SIG_ID_POWER_PEAK_24HR);

		pSig_PowerAvg24Hr = Equip_GetSpecialSigByMergedId(pEquip,
			SIG_ID_AVERAGE_POWER_24HR);

		// if the system power is NOT configured or has NO stat, 
		// set the related sig to non-configured.
		if ((pSig_SystemPower == NULL) ||					// no such signal
			(!SIG_VALUE_IS_CONFIGURED(pSig_SystemPower)) ||	// not configured
			(pSig_SystemPower->sv.pStatValue == NULL))		// no stat value
		{
			if (pSig_PowerPeak24Hr != NULL)
			{
				SIG_STATE_CLR(pSig_PowerPeak24Hr, VALUE_STATE_IS_CONFIGURED);
				pSig_PowerPeak24Hr = NULL;
			}

			if (pSig_PowerAvg24Hr != NULL)
			{
				SIG_STATE_CLR(pSig_PowerAvg24Hr, VALUE_STATE_IS_CONFIGURED);
				pSig_PowerAvg24Hr = NULL;
			}
		}

		// not need calc.
		if ((pSig_PowerPeak24Hr == NULL) && (pSig_PowerAvg24Hr == NULL))
		{
			pSig_SystemPower = NULL;
		}

		
		
		//1.10 Sys man/auto state
		pSig_SystemManagementMode = Equip_GetSpecialSigByMergedId(pEquip,
			SIG_ID_AUTO_MAN_STATE);

		//1.11.Sys hardware switch signal
		pSig_HwSwitch = Equip_GetSpecialSigByMergedId(pEquip,SIG_ID_HW_SWITCH);
		if (pSig_HwSwitch != NULL)	// set the hardware switch status
		{
			VAR_SET_VALUE(&var, pSig_HwSwitch->ucType, 
				(pSite->bSettingChangeDisabled ? 1 : 0));

			Equip_SetVirtualSignalValue(pSite, pEquip,
				DXI_SPLIT_SIG_MID_TYPE(SIG_ID_HW_SWITCH), pSig_HwSwitch,
				&var, tmNow, FALSE);
		}

		//1.12 added the config status signal. maofuhua, 2005-4-15 
		pSig_ConfigStatus = Equip_GetSpecialSigByMergedId(pEquip, 
			SIG_ID_SYS_CONFIG_STATUS);
		if (pSig_ConfigStatus != NULL)
		{
			VAR_SET_VALUE(&var, pSig_ConfigStatus->ucType, pSite->nConfigType);

			if (Equip_SetVirtualSignalValue(pSite, pEquip,
				DXI_SPLIT_SIG_MID_TYPE(SIG_ID_SYS_CONFIG_STATUS), pSig_ConfigStatus,
				&var, tmNow, FALSE) > 0)
			{
				bChanged = TRUE;
			}
		}

		//1.13 init all relay relevant active alarm num=0
		/*for(i = 0; i < MAX_RELAY_NUM; i++)
		{
			pSite->iAlarmSigsReleRelay[i] = 0;
		}*/

		//Added by wj for SMDU Sampler Mode Change
		if(pSite->nConfigStatus != CONFIG_STATUS_NOT_CHANGED)
		{
			TRACE("\n________Change Solution to SMDUCAN_______\n");
			VAR_VALUE_EX	value;
			
			int iEquipIDTemp = 1;
			int iSignalTypeTemp = 2;
			int iSignalIDTemp = 182;
			int iBufLen = sizeof(value);

			value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
			value.pszSenderName = "Internal Logic";
			value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
			value.varValue.enumValue = 0;

			/*pSig_SMDUSampMode = Equip_GetSpecialSigByMergedId(pEquip, 
				SIG_ID_SMDU_SAMPLER_MODE_CTRL);*/

			int iVarSubID = DXI_MERGE_SIG_ID(iSignalTypeTemp, iSignalIDTemp);
			int iError = DxiSetData(VAR_A_SIGNAL_VALUE,
				iEquipIDTemp,			
				iVarSubID,		
				&iBufLen,			
				&value,			
				1000);
			if (iError == ERR_DXI_OK )
			{
				TRACE("\n________Change Solution to SMDUCAN_______:%d\n",iError);

			}

			

		}
		//1.13 init and set the default value for the IB4 communication status 
		//get Ethernet Number
		iEthNum = GetEthernetNumForIB4();
		pSig_IB4CommStatus = Equip_GetSpecialSigByMergedId(pEquip, 
				SIG_ID_IB4_COMM_STATUS);
		//set default value 0 for IB4 communitcation status .
		VAR_SET_VALUE(&var, pSig_IB4CommStatus->ucType, IB4_COMM_NORMAL);
		if (Equip_SetVirtualSignalValue(pSite, pEquip,
			DXI_SPLIT_SIG_MID_TYPE(SIG_ID_IB4_COMM_STATUS), pSig_IB4CommStatus,
			&var, tmNow, FALSE) > 0)
		{
			bChanged = TRUE;
		}
	}

	//2.Process System status
	//2.1
	if (pSig_SysStatus != NULL)
	{
		int		i;

		for (i = 0; i < ALARM_LEVEL_MAX; i++)
		{
			if (pSig_AlarmCount[i] != NULL)
			{
				VAR_SET_VALUE(&var, pSig_AlarmCount[i]->ucType, 
					pSite->iAlarmCount[i]);

				Equip_SetVirtualSignalValue(pSite, pEquip,
					SIG_TYPE_SAMPLING, pSig_AlarmCount[i],
					&var, tmNow, FALSE);
			}
		}
		
				
		VAR_SET_VALUE(&var, pSig_SysStatus->ucType, 
			((pSite->iAlarmCount[ALARM_LEVEL_NONE] != 0) ? 1 : 0));
		
		Equip_SetVirtualSignalValue(pSite, pEquip,
			DXI_SPLIT_SIG_MID_TYPE(SIG_ID_SYSTEM_STATUS), pSig_SysStatus,
			&var, tmNow, FALSE);
	}

	//2.2. Alarm outgoing bloced	
	if (pSig_AlarmBlocked != NULL)
	{
		// The alarm outgoing block requirement:
		//		1. This alarm begin/end info shall be able to always be sent 
		//		   out to EEM/SNMP.
		//		2. Other alarms generated after this alarm shall be blocked.
		//
		// The  mechanism of set the bAlarmOutgoingBlocked:
		//		1. Do NOT change its value to TRUE when setting value is just
		//		   set to 1(blocked).
		//		2. Do change its value to FALSE when setting value is just 
		//         set to 0(un-blocked)
		if (pSite->bAlarmOutgoingBlocked)
		{
			// to clear outgoing blocked alarm after 24hrs.
			if (((tmNow - pSig_AlarmBlocked->tmCurrentSampled) >= (24*3600)) || // > 24hr
				(tmNow < pSig_AlarmBlocked->tmCurrentSampled)) // time modified late
			{
				VAR_SET_VALUE(&var, pSig_AlarmBlocked->ucType, 0);

				if (Equip_SetVirtualSignalValue(pSite, pEquip,
					DXI_SPLIT_SIG_MID_TYPE(SIG_ID_ALARM_OUTGOING_BLOCKED), pSig_AlarmBlocked,
					&var, tmNow, FALSE) > 0)
				{
					bChanged = TRUE;
				}
			}

			// set its state to FALSE if the setting value is un-blocked(0) now.
			// let the alarm resumed msg can be saved and sent out.
			if (pSig_AlarmBlocked->varValue.lValue == 0)	// un-block.
			{
				pSite->bAlarmOutgoingBlocked = FALSE;
			}
		}
		else if (pSig_AlarmBlocked->varValue.lValue != 0)
		{
			// the cur set is blocking.
			if (pSig_AlarmBlocked->varValue.lValue !=	// just blocked.
				pSig_AlarmBlocked->varLastValue.lValue)
			{
				// set the last value to current value. 
				// do NOT change the bAlarmOutgoingBlocked value.
				pSig_AlarmBlocked->varLastValue = pSig_AlarmBlocked->varValue;
			}
			else	
			{
				// OK. we can set its value to TRUE. 
				// the alarm outgoing blocked alarm is sent out.
				pSite->bAlarmOutgoingBlocked = TRUE;
			}
		}
	}

	//2.3 ACU internal status
	if (pSig_InternalStatus != NULL)
	{
		int nNew = (DAT_GetFlashRunningState() != ERR_OK) ? SIG_VAL_SYS_INTERNAL_FLASH_FAULT
			: 0;	// OK

		VAR_SET_VALUE(&var, pSig_InternalStatus->ucType, nNew);

		if (Equip_SetVirtualSignalValue(pSite, pEquip,
			DXI_SPLIT_SIG_MID_TYPE(SIG_ID_SYS_INTERNAL_STATUS), pSig_InternalStatus,
			&var, tmNow, FALSE) > 0)
		{
			bChanged = TRUE;
		}
	}

	//2.4. ACU maitenace time
	if (pSig_MaintnRun != NULL)
	{
		float fElapsedHrs = 
			(float)(tmNow - pSig_MaintnRun->tmCurrentSampled) / SEC_PER_HOUR;

#define MAINTN_TIME_RESOLUTION		0.01		// 0.01 hr. - do NOT change!
		if (fElapsedHrs >= MAINTN_TIME_RESOLUTION)
		{
			if (fElapsedHrs >= (2*MAINTN_TIME_RESOLUTION))
			{
				// the time may be changed to future.
				fElapsedHrs = MAINTN_TIME_RESOLUTION;
			}

			var.fValue = pSig_MaintnRun->varValue.fValue + fElapsedHrs;

			// need to clear? the unit of RUN is hr, the unit of the two others is day.
			if (var.fValue >= ((pSig_MaintnLimit->varValue.ulValue + 
					pSig_MaintnDelay->varValue.ulValue)*24))
			{
				var.fValue = 0;
			}

			// per hour to save the signal once, to avoid the FLASH failure.
			if ((var.fValue - (int)var.fValue) < (2*MAINTN_TIME_RESOLUTION))
			{
				// set it
				if (Equip_SetVirtualSignalValue(pSite, pEquip,
					DXI_SPLIT_SIG_MID_TYPE(SIG_ID_MAINTN_TIME_RUN), pSig_MaintnRun,
					&var, tmNow, FALSE) > 0)
				{
					bChanged = TRUE;
				}
			}
			else // change the current maintn run time directly.
			{
				// set the currnet value and time
				pSig_MaintnRun->varValue.fValue  = var.fValue;
				pSig_MaintnRun->tmCurrentSampled = tmNow;
			}
		}
		else if (fElapsedHrs < 0)
		{
			// the time changed back. set the last time.
			pSig_MaintnRun->tmCurrentSampled = tmNow;
		}
	}

	//2.5. system power peak in 24hr and average in 24hr
	if (pSig_SystemPower != NULL)
	{
		if (SIG_VALUE_IS_VALID(pSig_SystemPower))
		{
			SIG_STAT_VALUE  *pSig_SystemPowerStat;

			pSig_SystemPowerStat = pSig_SystemPower->sv.pStatValue;
			if (pSig_PowerPeak24Hr != NULL)
			{
				if (Equip_SetVirtualSignalValue(pSite, pEquip,
					pSig_PowerPeak24Hr->ucSigType, pSig_PowerPeak24Hr,
					&pSig_SystemPowerStat->varMaxValue, tmNow, FALSE) > 0)
				{
					bChanged = TRUE;
				}
			}

			if (pSig_PowerAvg24Hr != NULL)
			{
				if (Equip_SetVirtualSignalValue(pSite, pEquip,
					pSig_PowerAvg24Hr->ucSigType, pSig_PowerAvg24Hr,
					&pSig_SystemPowerStat->varAverageValue, tmNow, FALSE) > 0)
				{
					bChanged = TRUE;
				}
			}
		}
		else
		{
			if (pSig_PowerPeak24Hr != NULL)
			{
				SIG_STATE_CLR(pSig_PowerPeak24Hr, VALUE_STATE_IS_VALID);
			}

			if (pSig_PowerAvg24Hr != NULL)
			{
				SIG_STATE_CLR(pSig_PowerAvg24Hr, VALUE_STATE_IS_VALID);
			}
		}
	}


	//2.6. sys man-auto state
	if (pSig_SystemManagementMode != NULL)
	{
		pSite->nSystemManagementMode = pSig_SystemManagementMode->varValue.lValue; 
	}


	// do notify equipment to process data
	if (bChanged)
	{
		pEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
	}


	//set IB4 communication status
	if(iEthNum == ETHERNET_NUMBER_2)
	{
		iIntervalTimes++;
//		 printf("[%d  ",iIntervalTimes);
		if(iIntervalTimes >= 3)
		{
			iIntervalTimes = 0;
//			printf("IsIB4FailedIsIB4FailedIsIB4FailedIsIB4Failed %d \n ",iIntervalTimes);
			//set the value of IB4 Communication status
			if(!IsIB4Failed())
			{
				VAR_SET_VALUE(&var, pSig_IB4CommStatus->ucType, IB4_COMM_FAIL);
			}
			else
			{
				VAR_SET_VALUE(&var, pSig_IB4CommStatus->ucType, IB4_COMM_NORMAL);
			}
			Equip_SetVirtualSignalValue(pSite, pEquip,
					DXI_SPLIT_SIG_MID_TYPE(SIG_ID_IB4_COMM_STATUS), pSig_IB4CommStatus,
					&var, tmNow, FALSE);
		}
	}
	return bChanged;
}
/*==========================================================================*
* FUNCTION : StdEquip_MultiSMAC_ProcessSpecialSignals
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN OUT SITE_INFO  *pSite : 
* RETURN   : BOOL : 
* COMMENTS : CR# 0190-07-ACU
* CREATOR  : WangJing                   DATE: 2007-09-20 09:23
*==========================================================================*/
static BOOL StdEquip_MultiSMAC_ProcessSpecialSignals(IN SITE_INFO *pSite)
{
	int	iVarSubID;
	int iBufLen;

	int	iError = 0;
	SIG_BASIC_VALUE* pSigValue;
	static BOOL			bInited = FALSE;
	EQUIP_INFO	*pEquip = NULL;
	static EQUIP_INFO  *pSystemEquip = NULL;
	static EQUIP_INFO  *pACGroupEquip = NULL;
	int i,k;
	int j=0;
	VAR_VALUE var;
	time_t	tmNow = time(NULL);

#define NUM_SMACUNIT    4
#define NUM_SIG_SMAC    9
#define NUM_SIG_SYSTEM  2
#define NUM_SIG_ACGROUP 6


#define SYSTEM_EQUIPTYPE_ID         100
#define SMAC_EQUIPTYPE_ID           703
#define ACGROUP_EQUIPTYPE_ID          700

#define SYSTEM_EQUIP_ID             1
#define SMAC1_EQUIP_ID              259
#define SMAC2_EQUIP_ID              260
#define SMAC3_EQUIP_ID              706
#define SMAC4_EQUIP_ID              707

#define ACGROUP_EQUIP_ID            103


	static SIG_BASIC_VALUE 
		*pSigSMAC[NUM_SMACUNIT][NUM_SIG_SMAC] = {NULL};

	static SIG_BASIC_VALUE 
		*pSigSystem[NUM_SIG_SYSTEM] = {NULL};

	static SIG_BASIC_VALUE 
		*pSigACGroup[NUM_SIG_ACGROUP] = {NULL};

	int pACGroupSigID[] = { SIG_ID_PHASE_A_CUR,		
		SIG_ID_PHASE_B_CUR,		
		SIG_ID_PHASE_C_CUR,		
		SIG_ID_PHASE_A_POWER,	
		SIG_ID_PHASE_B_POWER,	
		SIG_ID_PHASE_C_POWER};

	int pEquipID[] = {SYSTEM_EQUIP_ID,
		SMAC1_EQUIP_ID,
		SMAC2_EQUIP_ID,
		SMAC3_EQUIP_ID,
		SMAC4_EQUIP_ID,
		ACGROUP_EQUIP_ID};

	int pSMACSigID[] = {SIG_ID_SYS_POWER,		
		SIG_ID_ALL_POWER_CONSUM,
		SIG_ID_PHASE_A_CUR,		
		SIG_ID_PHASE_B_CUR,		
		SIG_ID_PHASE_C_CUR,		
		SIG_ID_PHASE_A_POWER,	
		SIG_ID_PHASE_B_POWER,	
		SIG_ID_PHASE_C_POWER};



	int pSystemSigID[] = {SIG_ID_ACU_SYS_POWER,		
		SIG_ID_ACU_ALL_POWER_CONSUM};



	// init sig
	if (!bInited)
	{
		pSystemEquip = Init_GetEquipRefById(pSite, SYSTEM_EQUIP_ID);
		pACGroupEquip = Init_GetEquipRefById(pSite, ACGROUP_EQUIP_ID);

		for (k = 0; k < ITEM_OF(pEquipID); k++)
		{
			pEquip = Init_GetEquipRefById(pSite, pEquipID[k]);

			if (pEquip == NULL)
			{
				return FALSE;
			}
			if(pEquip->pStdEquip->iTypeID == SYSTEM_EQUIPTYPE_ID)
			{
				for (i = 0; i < NUM_SIG_SYSTEM; i++)
				{
					pSigSystem[i] = Equip_GetSpecialSigByMergedId(pEquip, 
						pSystemSigID[i]);
					if(pSigSystem[i] == NULL)
					{
						return FALSE;
					}
				}

			}
			
			else if(pEquip->pStdEquip->iTypeID == SMAC_EQUIPTYPE_ID)
			{
				for (i = 0; i < NUM_SIG_SMAC; i++)
				{
					pSigSMAC[j][i] = Equip_GetSpecialSigByMergedId(pEquip, 
						pSMACSigID[i]);
					if(pSigSMAC[j][i] == NULL)
					{
						return FALSE;
					}
				}
				j++;
			}
			else if(pEquip->pStdEquip->iTypeID == ACGROUP_EQUIPTYPE_ID)
			{
				for (i = 0; i < NUM_SIG_ACGROUP; i++)
				{
					pSigACGroup[i] = Equip_GetSpecialSigByMergedId(pEquip, 
						pACGroupSigID[i]);
					if(pSigACGroup[i] == NULL)
					{
						return FALSE;
					}
				}
			}
			
		}
		bInited = TRUE;
	}

	//TRACE("\n INIT OK !!!\n");
	//calculate
	
	for(i = 0; i < NUM_SIG_SYSTEM; i++)//calculate ACU System
	{
		var.fValue = 0.0;
		for(k = 0; k < NUM_SMACUNIT; k++)
		{
			if(SIG_STATE_TEST(pSigSMAC[k][i], VALUE_STATE_IS_VALID))
			{
				var.fValue += pSigSMAC[k][i]->varValue.fValue;
			}  
		}

		var.fValue = var.fValue ;//* 1000.0;
		if (Equip_SetVirtualSignalValue(pSite, pSystemEquip,
			pSigSystem[i]->ucSigType, pSigSystem[i],
			&var, tmNow, FALSE) > 0)
		{
			pSystemEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
		}
		SIG_STATE_SET(pSigSystem[i], VALUE_STATE_IS_CONFIGURED);
	}

	for(i = 0; i < NUM_SIG_ACGROUP; i++)//calculate ACGroup
	{
		var.fValue = 0.0;
		for(k = 0; k < NUM_SMACUNIT; k++)
		{
			if(SIG_STATE_TEST(pSigSMAC[k][i+2], VALUE_STATE_IS_VALID))
			{
				var.fValue += pSigSMAC[k][i+2]->varValue.fValue;
			}  
		}

		if (Equip_SetVirtualSignalValue(pSite, pACGroupEquip,
			pSigACGroup[i]->ucSigType, pSigACGroup[i],
			&var, tmNow, FALSE) > 0)
		{
			pACGroupEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
		}
		SIG_STATE_SET(pSigACGroup[i], VALUE_STATE_IS_CONFIGURED);
	}
	//TRACE("\n StdEquip_MultiSMAC_ProcessSpecialSignals!!!\n");
	return TRUE;

}

/*==========================================================================*
* FUNCTION : StdEquip_AC_ProcessSpecialSignals
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-02 00:19
*==========================================================================*/
static BOOL StdEquip_AC_ProcessSpecialSignals(IN SITE_INFO *pSite)
{
	static BOOL			bInited = FALSE;
	static EQUIP_INFO	*pEquip = NULL;

#define NUM_SIG_AC_FAIL_CNT		4
#define NUM_ALARM_AC_FAIL_A		2

#define NUM_ACUnit              4


#define SMACU_UNUT1             259
#define SMACU_UNUT2             260
#define SMACU_UNUT3             706
#define SMACU_UNUT4             707

	static SIG_BASIC_VALUE 
		*pSigAcFailCnt[NUM_ACUnit][NUM_SIG_AC_FAIL_CNT] = {NULL}; // A,B,C,F
	static SIG_BASIC_VALUE //A:0-1, B:2-3, C:4-5, F:6-7: the high and Low alarms
		*pSigAcAlarm[NUM_ACUnit][NUM_SIG_AC_FAIL_CNT*NUM_ALARM_AC_FAIL_A] = {NULL};
	static int pLastAlarmStates[NUM_ACUnit][NUM_SIG_AC_FAIL_CNT*NUM_ALARM_AC_FAIL_A]={0};

	time_t	tmNow = time(NULL);
	BOOL	bChanged[NUM_ACUnit] = {FALSE};
	VAR_VALUE var;
	int		i, j;
	int     k;
	SIG_BASIC_VALUE *pSigAlarm;
	int		pEquipIdOfAC[] = {SMACU_UNUT1,SMACU_UNUT2,SMACU_UNUT3,SMACU_UNUT4};


	// init
	if (!bInited)
	{
		int		pSigIdAcFailCnt[NUM_SIG_AC_FAIL_CNT] =
		{
			SIG_ID_A_MAINS_FAIL_CNT, SIG_ID_B_MAINS_FAIL_CNT, SIG_ID_C_MAINS_FAIL_CNT,
			SIG_ID_FREQ_FAIL_CNT
		};

		int		pSigIdAcFailDependent[NUM_SIG_AC_FAIL_CNT] = 
		{
			SIG_ID_VOL_A, SIG_ID_VOL_B, SIG_ID_VOL_C,
			SIG_ID_VOL_FREQ
		};

		int		pSigIdAlarmAcFail[NUM_SIG_AC_FAIL_CNT*NUM_ALARM_AC_FAIL_A] = 
		{
			ALARM_ID_HIGH_VOL_A, ALARM_ID_LOW_VOL_A,  
			ALARM_ID_HIGH_VOL_B, ALARM_ID_LOW_VOL_B,  
			ALARM_ID_HIGH_VOL_C, ALARM_ID_LOW_VOL_C,  
			ALARM_ID_HIGH_FREQ,  ALARM_ID_LOW_FREQ,
		};		



		bInited = TRUE;

		for (k = 0; k < ITEM_OF(pEquipIdOfAC); k++)
		{
			pEquip = Init_GetEquipRefById(pSite, pEquipIdOfAC[k]);

			if (pEquip == NULL)
			{
				return FALSE;
			}

			for (i = 0; i < NUM_SIG_AC_FAIL_CNT; i++)
			{
				SIG_BASIC_VALUE	*pSigDependent;

				pSigAcFailCnt[k][i] = Equip_GetSpecialSigByMergedId(pEquip, 
					pSigIdAcFailCnt[i]);

				if (pSigAcFailCnt[k][i] != NULL)
				{
					// get dependent signal
					pSigDependent = Equip_GetSpecialSigByMergedId(pEquip, 
						pSigIdAcFailDependent[i]);

					// no dependent signal or the dependent signal is NOT configured.
					// set the cnt signal to non-configured.
					if ((pSigDependent == NULL) || 
						!SIG_VALUE_IS_CONFIGURED(pSigDependent) || // dependent sig is not config
						!SIG_VALUE_IS_CONFIGURED(pSigAcFailCnt[k][i]))// this signal is not config
					{
						SIG_STATE_CLR(pSigAcFailCnt[k][i], VALUE_STATE_IS_CONFIGURED);	
						pSigAcFailCnt[k][i] = NULL;
					}
					else
					{
						SIG_STATE_SET(pSigAcFailCnt[k][i], VALUE_STATE_IS_VALID);
					}
				}

				// to find the related alarm sig
				if (pSigAcFailCnt[k][i] != NULL)
				{
					int		nFindSig = 0;// the related alarm signals.

					for (j = i*NUM_ALARM_AC_FAIL_A; j < (i+1)*NUM_ALARM_AC_FAIL_A; j++)
					{
						if (pSigIdAlarmAcFail[j] == 0)
						{
							continue;
						}

						pSigAcAlarm[k][j] = Equip_GetSpecialSigByMergedId(pEquip, 
							pSigIdAlarmAcFail[j]);
						if ((pSigAcAlarm[k][j] != NULL) && 
							SIG_VALUE_IS_CONFIGURED(pSigAcAlarm[k][j]))
						{
							nFindSig++;
						}
						else
						{
							pSigAcAlarm[k][j] = NULL;
						}
					}

					if (nFindSig == 0)
					{
						SIG_STATE_CLR(pSigAcFailCnt[k][i], VALUE_STATE_IS_VALID);
						pSigAcFailCnt[k][i] = NULL; // no conditions, do NOT calc it late. 
					}
				}
			}
		}
		// init sig


	}


	// do calculate the sig

	for (k = 0; k < ITEM_OF(pEquipIdOfAC); k++)
	{
		pEquip = Init_GetEquipRefById(pSite, pEquipIdOfAC[k]);

		if (pEquip == NULL)
		{
			return FALSE;
		}

		if(pEquip->bWorkStatus != TRUE)  //Auto-cfg Optimize, Thomas, 2007-10
		{
			continue;
		}

		for (i = 0; i < NUM_SIG_AC_FAIL_CNT; i++)
		{
			if (pSigAcFailCnt[k][i] != NULL)
			{

				if(!SIG_STATE_TEST(pSigAcFailCnt[k][i], VALUE_STATE_IS_VALID))
				{
					SIG_STATE_SET(pSigAcFailCnt[k][i], VALUE_STATE_IS_VALID);
				}

				for (j = i*NUM_ALARM_AC_FAIL_A; j < (i+1)*NUM_ALARM_AC_FAIL_A; j++)
				{
					pSigAlarm = pSigAcAlarm[k][j];
					if ((pSigAlarm != NULL) &&	// valid and if state changed
						(pLastAlarmStates[k][j] != pSigAlarm->varValue.lValue))
					{
						pLastAlarmStates[k][j] = pSigAlarm->varValue.lValue;

						if (pSigAlarm->varValue.lValue != 0)	// just alarm
						{
							// increase cnt
							var.lValue = pSigAcFailCnt[k][i]->varValue.lValue + 1;

							// set it
							if (Equip_SetVirtualSignalValue(pSite, pEquip,
								pSigAcFailCnt[k][i]->ucSigType, pSigAcFailCnt[k][i],
								&var, tmNow, FALSE) > 0)
							{
								bChanged[k] = TRUE;
							}
						}
					}
				}			
			}
		}
	}


	// do notify equipment to process data
	for (k = 0; k < ITEM_OF(pEquipIdOfAC); k++)
	{
		pEquip = Init_GetEquipRefById(pSite, pEquipIdOfAC[k]);

		if (pEquip == NULL)
		{
			return FALSE;
		}

		if(!EQUIP_ATTR_TEST(pEquip, EQUIP_WORKING_STATE))  //Auto-cfg Optimize, Thomas, 2007-10
		{
			continue;
		}

		if (bChanged[k])
		{
			pEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
		}

	}
	//TRACE("\n StdEquip_AC_ProcessSpecialSignals OK !!!\n");
	return TRUE;
}

#define SOURCE_VALUE_SIGID		240
#define SOURCE_NUM_SIGID		241
#define SOURCE_RATED_VALUE_SIGID		242

#define EIB1_UNIT				197
#define EIB_MAX_NUM				4
#define SMDU1_UNIT				107
#define SMDU_MAX_NUM				8

#define EIB_SHUNT_NUM			3
#define SMDU_SHUNT_NUM			5
enum SOURCE_SIGNAL
{
	SOURCE_VALUE_SEQ,
	SOURCE_NUM_SEQ,
	EIB_SHUNT_TYPE_SEQ,
	EIB_SHUNT_CURR_SEQ = 14,
	EIB_SHUNT_RATED_CURR_SEQ = 26,
	SMDU_SHUNT_TYPE_SEQ = 38,
	SMDU_SHUNT_CURR_SEQ = 78,
	SMDU_SHUNT_RATED_CURR_SEQ = 118,
	SOURCE_RATED_CURR_SEQ = 158,		//source rated current in system 
};	

static BOOL Init_Source_ProcessSpecialSignals(IN SITE_INFO *pSite,SIG_BASIC_VALUE **pSigSourceSig)
{
	int i,j;
	static EQUIP_INFO	*pEquip = NULL;
	int		iSigIdEIBShuntTpyeSetSig[EIB_SHUNT_NUM] =
	{
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,16),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,17),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,18)
	};
	int		iSigIdEIBShuntCurrValueSig[EIB_SHUNT_NUM] =
	{
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 64),
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 65),
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 66)
	};
	int		iSigIdEIBShuntRatedCurrSig[EIB_SHUNT_NUM] =
	{
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 3),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 5)
	};

	int		iSigIdSMDUShuntTpyeSetSig[SMDU_SHUNT_NUM] =
	{
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,16),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,17),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,18),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,19),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,20)
	};
	int		iSigIdSMDUShuntCurrValueSig[SMDU_SHUNT_NUM] =
	{
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,31),
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,32),
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,33),
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,34),
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,35)
	};
	int		iSigIdSMDUShuntRatedCurrSig[SMDU_SHUNT_NUM] =
	{
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,22),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,24),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,26),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,28),
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,30)
	};

	for(i = 0; i < EIB_MAX_NUM; i++)
	{
		pEquip = Init_GetEquipRefById(pSite, EIB1_UNIT + i);
		for (j = 0; j < EIB_SHUNT_NUM; j++)
		{
			pSigSourceSig[EIB_SHUNT_TYPE_SEQ + i * EIB_SHUNT_NUM + j] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdEIBShuntTpyeSetSig[j]);
			pSigSourceSig[EIB_SHUNT_CURR_SEQ + i * EIB_SHUNT_NUM + j] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdEIBShuntCurrValueSig[j]);
			pSigSourceSig[EIB_SHUNT_RATED_CURR_SEQ + i * EIB_SHUNT_NUM + j] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdEIBShuntRatedCurrSig[j]);
		}
	}
	for(i = 0; i < SMDU_MAX_NUM; i++)
	{
		pEquip = Init_GetEquipRefById(pSite, SMDU1_UNIT + i);
		for (j = 0; j < SMDU_SHUNT_NUM; j++)
		{
			pSigSourceSig[SMDU_SHUNT_TYPE_SEQ + i * SMDU_SHUNT_NUM + j] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdSMDUShuntTpyeSetSig[j]);
			pSigSourceSig[SMDU_SHUNT_CURR_SEQ + i * SMDU_SHUNT_NUM + j] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdSMDUShuntCurrValueSig[j]);
			pSigSourceSig[SMDU_SHUNT_RATED_CURR_SEQ + i * SMDU_SHUNT_NUM + j] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdSMDUShuntRatedCurrSig[j]);

		}
	}

	pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
	pSigSourceSig[SOURCE_VALUE_SEQ] = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SOURCE_VALUE_SIGID));
	pSigSourceSig[SOURCE_NUM_SEQ] = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SOURCE_NUM_SIGID));
	pSigSourceSig[SOURCE_RATED_CURR_SEQ] = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SOURCE_RATED_VALUE_SIGID));
return TRUE;
}
/*==========================================================================*
* FUNCTION : StdEquip_Source_ProcessSpecialSignals
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : wj         DATE: 20017-03-01 00:19
*==========================================================================*/
static BOOL StdEquip_Source_ProcessSpecialSignals(IN SITE_INFO *pSite)
{
	VAR_VALUE var;
	static BOOL			s_bInited = FALSE;
	BOOL bChanged = FALSE;
	static EQUIP_INFO	*pEquip = NULL;
	int					i, j;
	time_t				tmNow = time(NULL);
	int iSourceNum = 0;
	float fSourceCurrValue = 0;
	float fSourceRatedCurrValue = 0;

#define SOURCE_SIGNAL_NUM	((EIB_MAX_NUM * EIB_SHUNT_NUM * 3) + (SMDU_MAX_NUM * SMDU_SHUNT_NUM * 3) + 3)
	static SIG_BASIC_VALUE *pSigSourceSig[SOURCE_SIGNAL_NUM] = {NULL};
	//static SIG_BASIC_VALUE *pSigSMDUShuntTpyeSetSig[SMDU_MAX_NUM * SMDU_SHUNT_NUM] = {NULL};

	// init
	if (!s_bInited)
	{
		s_bInited = Init_Source_ProcessSpecialSignals(pSite, &pSigSourceSig);
	}

	//get source number and source current from EIB
	for(i = 0; i < EIB_MAX_NUM * EIB_SHUNT_NUM;i++)
	{
		if(pSigSourceSig[EIB_SHUNT_CURR_SEQ + i] != NULL && pSigSourceSig[EIB_SHUNT_TYPE_SEQ + i] != NULL)
		{
			if(SIG_VALUE_IS_VALID(pSigSourceSig[EIB_SHUNT_CURR_SEQ + i]) && pSigSourceSig[EIB_SHUNT_TYPE_SEQ + i]->varValue.enumValue == 4)
			{
				iSourceNum++;
				fSourceCurrValue += pSigSourceSig[EIB_SHUNT_CURR_SEQ + i]->varValue.fValue;
				fSourceRatedCurrValue += pSigSourceSig[EIB_SHUNT_RATED_CURR_SEQ + i]->varValue.fValue;
			}
		}
	}
	//get source number and source current from SMDU
	for(i = 0; i < SMDU_MAX_NUM * SMDU_SHUNT_NUM;i++)
	{
		if(pSigSourceSig[SMDU_SHUNT_CURR_SEQ + i] != NULL && pSigSourceSig[SMDU_SHUNT_TYPE_SEQ + i] != NULL)
		{
			if(SIG_VALUE_IS_VALID(pSigSourceSig[SMDU_SHUNT_CURR_SEQ + i]) && pSigSourceSig[SMDU_SHUNT_TYPE_SEQ + i]->varValue.enumValue == 4)
			{
				iSourceNum++;
				fSourceCurrValue +=pSigSourceSig[SMDU_SHUNT_CURR_SEQ + i]->varValue.fValue;
				fSourceRatedCurrValue += pSigSourceSig[SMDU_SHUNT_RATED_CURR_SEQ + i]->varValue.fValue;
			}
		}
	}

	pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
	//set source num signal 
	if(pSigSourceSig[SOURCE_NUM_SEQ] != NULL)
	{
		VAR_SET_VALUE(&var, pSigSourceSig[SOURCE_NUM_SEQ]->ucType, iSourceNum);
		if(Equip_SetVirtualSignalValue(pSite,
			pEquip, 
			SIG_TYPE_SAMPLING, 
			pSigSourceSig[SOURCE_NUM_SEQ],
			&var, 
			tmNow, 
			FALSE) > 0)
		{
			bChanged = TRUE;
		}
	}

	//set source current signal 
	if(pSigSourceSig[SOURCE_VALUE_SEQ] != NULL)
	{
		VAR_SET_VALUE(&var, pSigSourceSig[SOURCE_VALUE_SEQ]->ucType, fSourceCurrValue);
		if(Equip_SetVirtualSignalValue(pSite,
			pEquip, 
			SIG_TYPE_SAMPLING, 
			pSigSourceSig[SOURCE_VALUE_SEQ],
			&var, 
			tmNow, 
			FALSE) > 0)
		{
			bChanged = TRUE;
		}
	}
	//set source rated current signal 
	if(pSigSourceSig[SOURCE_RATED_CURR_SEQ] != NULL)
	{
		VAR_SET_VALUE(&var, pSigSourceSig[SOURCE_RATED_CURR_SEQ]->ucType, fSourceRatedCurrValue);
		if(Equip_SetVirtualSignalValue(pSite,
			pEquip, 
			SIG_TYPE_SAMPLING, 
			pSigSourceSig[SOURCE_RATED_CURR_SEQ],
			&var, 
			tmNow, 
			FALSE) > 0)
		{
			bChanged = TRUE;
		}
	}

	return bChanged;
}

/*==========================================================================*
* FUNCTION : StdEquip_IBEIB_ProcessSpecialSignals
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : wj         DATE: 2005-02-02 00:19
*==========================================================================*/
static BOOL StdEquip_IBEIB_ProcessSpecialSignals(IN SITE_INFO *pSite)
{
	static BOOL			bInited = FALSE;
	static EQUIP_INFO	*pEquip = NULL;
	int					i=0;
	time_t				tmNow = time(NULL);
	//static int			iWaitFlag;

#define IB1_UNIT				202
#define IB2_UNIT				203
#define EIB1_UNIT				197
#define EIB2_UNIT				198
//#define SYSTEM_UNIT				1
#define AC_UNIT					104
//#define NUM_TEMP_ALARM			4//10 Jimmy changed it to 4 ,according to CR temp Strategy
#define NUM_TEMP_ALARM			8
//#define NUM_TEMP				2
#define NUM_TEMP				4
#define NUM_AC_SIG				6
#define WAIT_PROCESS_TIMER		1

	static ALARM_SIG_VALUE *pSigSYSIBTempAlarm[NUM_TEMP_ALARM] = {NULL};

	static ALARM_SIG_VALUE *pSigSYSEIBTempAlarm[NUM_TEMP_ALARM] = {NULL};

	static SIG_BASIC_VALUE *pSigSYSIBTemp[NUM_TEMP] = {NULL};

	static SIG_BASIC_VALUE *pSigSYSEIBTemp[NUM_TEMP] = {NULL};

	static SIG_BASIC_VALUE *pSigACUnitSig[NUM_AC_SIG] = {NULL};

	static SIG_BASIC_VALUE *pSigEIBFail = NULL;

	static SIG_BASIC_VALUE *pSigEIBState = NULL;

	static SIG_BASIC_VALUE *pSigEIBFail2 = NULL;

	static SIG_BASIC_VALUE *pSigEIBState2 = NULL;

	static SIG_BASIC_VALUE *pSigIBFail = NULL;

	static SIG_BASIC_VALUE *pSigIBFail2 = NULL;

	static SIG_BASIC_VALUE *pSigIBState = NULL;

	static SIG_BASIC_VALUE *pSigACUnitState = NULL;

	// init
	if (!bInited)
	{

		//iWaitFlag = 0;

		int		iSigIdSYSIBTempAlarm[NUM_TEMP_ALARM] =
		{
				21,22,
				31,34,
				450,451,
				454,455,

				//38,39,
				//29,32,
				//30,33
		};

		int		iSigIdSYSEIBTempAlarm[NUM_TEMP_ALARM] =
		{
				42,43,
				48,49,
				452,453,
				456,457,
				//50,51,
				//44,45,
				//46,47
		};

		int		iSigIdACUnitSig[NUM_AC_SIG] =
		{
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 1),DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 2),
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 3),DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 4),
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 5),DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 6)

		};

		int		iSigIdSYSIBTemp[NUM_TEMP] =
		{
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 46),DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 47),
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 220),DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 221)

		};

		int		iSigIdSYSEIBTemp[NUM_TEMP] =
		{
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 52),DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 51),
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 222),DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 223)

		};


		bInited = TRUE;

		pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
		for (i = 0; i < NUM_TEMP_ALARM; i++)
		{
			pSigSYSIBTempAlarm[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,iSigIdSYSIBTempAlarm[i]);
			pSigSYSEIBTempAlarm[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,iSigIdSYSEIBTempAlarm[i]);

		}

		for (i = 0; i < NUM_TEMP; i++)
		{
			pSigSYSIBTemp[i] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdSYSIBTemp[i]);
			pSigSYSEIBTemp[i] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdSYSEIBTemp[i]);
		}

		pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
		pSigIBFail = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 96));
		pSigIBState = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 78));

		pEquip = Init_GetEquipRefById(pSite, IB2_UNIT);
		pSigIBFail2 = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 99));

		pEquip = Init_GetEquipRefById(pSite, EIB1_UNIT);
		pSigEIBFail = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 99));
		pSigEIBState = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 100));

		pEquip = Init_GetEquipRefById(pSite, EIB2_UNIT);
		pSigEIBFail2 = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 99));
		pSigEIBState2 = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 100));

		pEquip = Init_GetEquipRefById(pSite, AC_UNIT);
		for (i = 0; i < NUM_AC_SIG; i++)
		{
			pSigACUnitSig[i] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdACUnitSig[i]);
		}
		pSigACUnitState = Equip_GetSpecialSigByMergedId(pEquip,DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 11));


	}

	//if(iWaitFlag <= 20)
	//{
	//	iWaitFlag++;
	//	//TRACE("\n StdEquip_IBEIB_ProcessSpecialSignals Waiting!!!\n");
	//	return TRUE;
	//}

	//clear the valid of temp according to equip failure(sigID 99)
	if(pSigIBFail->varValue.enumValue == 1)
	{
		for (i = 0; i < NUM_TEMP/2; i++)
		{
			SIG_STATE_CLR(pSigSYSIBTemp[i], VALUE_STATE_IS_VALID);
		}
	}

	if(pSigIBFail2->varValue.enumValue == 1)
	{
		for (i = 2; i < NUM_TEMP; i++)
		{
			SIG_STATE_CLR(pSigSYSIBTemp[i], VALUE_STATE_IS_VALID);
		}
	}

	if(pSigEIBFail->varValue.enumValue == 1)
	{
		for (i = 0; i < NUM_TEMP/2; i++)
		{
			SIG_STATE_CLR(pSigSYSEIBTemp[i], VALUE_STATE_IS_VALID);
		}
	}
	if(pSigEIBFail2->varValue.enumValue == 1)
	{
		for (i = 2; i < NUM_TEMP; i++)
		{
			SIG_STATE_CLR(pSigSYSEIBTemp[i], VALUE_STATE_IS_VALID);
		}
	}

	if(pSigACUnitState->varValue.enumValue == 1)
	{
		for (i = 0; i < NUM_AC_SIG; i++)
		{
			SIG_STATE_CLR(pSigACUnitSig[i], VALUE_STATE_IS_VALID);
		}
	}

	//pSigIBState won't change if the the IB2-1/2 lost 
	if(pSigIBState->varValue.enumValue < 2)
	{
		for (i = NUM_TEMP_ALARM/2; i < NUM_TEMP_ALARM; i++)
		{
			
			if((pSigSYSIBTempAlarm[i]->next != NULL))
			{
				REMOVE_NODE_SELF_FROM_LINK(pSigSYSIBTempAlarm[i]);
				SIG_STATE_SET(pSigSYSIBTempAlarm[i], ALARM_STATE_IS_NEW_END);
				pSigSYSIBTempAlarm[i]->bv.varValue.lValue = 0;
				pSigSYSIBTempAlarm[i]->sv.tmEndTime = tmNow;
			}
		}
	}

	if(pSigIBState->varValue.enumValue < 1)
	{
		for (i = 0; i < NUM_TEMP_ALARM/2; i++)
		{
			
			if((pSigSYSIBTempAlarm[i]->next != NULL))
			{
				REMOVE_NODE_SELF_FROM_LINK(pSigSYSIBTempAlarm[i]);
				SIG_STATE_SET(pSigSYSIBTempAlarm[i], ALARM_STATE_IS_NEW_END);
				pSigSYSIBTempAlarm[i]->bv.varValue.lValue = 0;
				pSigSYSIBTempAlarm[i]->sv.tmEndTime = tmNow;
			}
		}
	}


	if(pSigEIBState->varValue.enumValue == 1)
	{
		for (i = 0; i < NUM_TEMP_ALARM/2; i++)
		{

			if((pSigSYSEIBTempAlarm[i]->next != NULL))
			{
				REMOVE_NODE_SELF_FROM_LINK(pSigSYSEIBTempAlarm[i]);
				SIG_STATE_SET(pSigSYSEIBTempAlarm[i], ALARM_STATE_IS_NEW_END);
				pSigSYSEIBTempAlarm[i]->bv.varValue.lValue = 0;
				pSigSYSEIBTempAlarm[i]->sv.tmEndTime = tmNow;
			}
		}
	}
	if(pSigEIBState2->varValue.enumValue == 1)
	{
		for (i = NUM_TEMP_ALARM/2; i < NUM_TEMP_ALARM; i++)
		{

			if((pSigSYSEIBTempAlarm[i]->next != NULL))
			{
				REMOVE_NODE_SELF_FROM_LINK(pSigSYSEIBTempAlarm[i]);
				SIG_STATE_SET(pSigSYSEIBTempAlarm[i], ALARM_STATE_IS_NEW_END);
				pSigSYSEIBTempAlarm[i]->bv.varValue.lValue = 0;
				pSigSYSEIBTempAlarm[i]->sv.tmEndTime = tmNow;
			}
		}
	}
	return TRUE;


}

static BOOL DHCPSetFunc(IN SITE_INFO *pSite)
{
	static int s_ifirstFlage = 0;

	if(s_ifirstFlage == 0)
	{
		int nDHCP;
		int nBufLen;
		VAR_VALUE_EX	varValueEx;

		memset(&varValueEx, 0, sizeof(varValueEx));

		varValueEx.nSendDirectly = EQUIP_CTRL_SEND_URGENTLY;
		varValueEx.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		varValueEx.pszSenderName = "";

		if(DxiGetData(VAR_APP_DHCP_INFO,
			0,
			0,
			&nBufLen,
			&nDHCP,
			0) == ERR_DXI_OK)
		{
			varValueEx.varValue.enumValue = (nDHCP == APP_DCHP_ERR);

			DxiSetData(VAR_A_SIGNAL_VALUE,
				1,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 54),//54	DHCP Failure		
				sizeof(VAR_VALUE_EX),			
				&(varValueEx),			
				0);
		}

		s_ifirstFlage = 1;
	}

	
	return TRUE;
}
static BOOL DHCPEnableFunc(IN SITE_INFO *pSite)
{
	static int s_ifirstFlage = 0;
	int  iBufLen, iError;
	int nDHCPType = 1;//Enable
	
	if(s_ifirstFlage == 0)
	{
		SIG_BASIC_VALUE* pSigValue = NULL;

		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			1,		
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 485),		
			&iBufLen,			
			(void *)&pSigValue,			
			10000);
		if (pSigValue == NULL || iError != ERR_DXI_OK)
		{
			return FALSE;
		}
		if (pSigValue->varValue.enumValue)//DHCP Enable
		{
			iBufLen = sizeof(int);
			DxiSetData(VAR_APP_DHCP_INFO,
					0,			
					0,		
					iBufLen,			
					&nDHCPType,			
					0);

			DxiSetData(VAR_APP_DHCP_INFO,
					DXI_VAR_ID_IPV6,			
					0,		
					iBufLen,			
					&nDHCPType,			
					0);
		}

		s_ifirstFlage = 1;
	}
	
	return TRUE;
}
static void RelayOutputCtrl(int iCtrlValue, int iRelayNo)
{
	int iError;
	int iBufLen;
	int iVarID, iVarSubID;
	int iTimeOut;
	VAR_VALUE_EX varValueEx;

	memset(&varValueEx, 0x0, sizeof(varValueEx));
	varValueEx.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	varValueEx.nSenderType	 = SERVICE_OF_LOGIC_CONTROL;
	varValueEx.pszSenderName = "Relay";
	varValueEx.varValue.enumValue = iCtrlValue;
	
	iError			= 0;
	iBufLen			= sizeof(VAR_VALUE_EX);

	iTimeOut		= 0;
//changed by SongXu for IB2-1 active 20160809
	if(iRelayNo < ALARM_RELAY_EIGHTH)
	{
		iVarID			= 202;//IB1
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
//			iRelayNo + 13);
			iRelayNo + 1);

	}
	else if(iRelayNo < ALARM_RELAY_13TH)
	{

		iVarID			= 197;//EIB1
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo - 8 + 1);
	}
	else if(iRelayNo < ALARM_RELAY_17TH)//IB01    14 TO 17
	{

		iVarID			= 1;
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo - 13 + 22);
	}
	else if(iRelayNo < ALARM_RELAY_25TH)//2nd IB1/IB2  18 TO 25
	{

		iVarID			= 203;//EIB1
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo - 17 + 1);
	}
	
	else if(iRelayNo < ALARM_RELAY_30TH)//2nd EIB  26 TO 30
	{

		iVarID			= 198;
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo - 25 + 1);
	}
	else 
	{
		return;
	}
	

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
			iVarID,			
			iVarSubID,		
			iBufLen,			
			(void *)&(varValueEx),			
			iTimeOut);

	if (iError == ERR_DXI_INVALID_DATA_VALUE)
	{
		return ;
	}

	return ;
}

static SIG_ENUM GetRelayStatus(int iRelayNo)
{

	int iError;
	int iBufLen;
	int iVarID, iVarSubID;
	int iTimeOut;

	iError			= 0;
	iBufLen			= sizeof(VAR_VALUE);
	iTimeOut		= 200;

	//1.Get data
	SIG_BASIC_VALUE *pSigVal;


	if(iRelayNo < ALARM_RELAY_EIGHTH)
	{
		iVarID			= 202;//IB1
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo + 1);
	}
	else if(iRelayNo < ALARM_RELAY_13TH)
	{

		iVarID			= 197;//EIB1
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo - 8 + 1);
	}
	else if(iRelayNo < ALARM_RELAY_17TH)
	{

		iVarID			= 1;//main board
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo - 13 + 22);
	}
	else if(iRelayNo < ALARM_RELAY_25TH)
	{

		iVarID			= 203;//IB2
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo - 17 + 1);
	}
	else if(iRelayNo < ALARM_RELAY_30TH)
	{

		iVarID			= 198;//EIB2
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, 
			iRelayNo - 25 + 1);
	}
	else
	{
		return FALSE;
	}


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iVarID,			
			iVarSubID,		
			&iBufLen,			
			&pSigVal,			
			iTimeOut);	

	if (iError != ERR_OK)
	{
		return FALSE;
	}
 
	if (!SIG_VALUE_IS_VALID(pSigVal))
	{
		return FALSE;
	}
	return pSigVal->varValue.enumValue;
 
}

//added as Lemon fixed in hybrid.
static BOOL  IsActiveAlarms_ContainSumAlarm(IN SITE_INFO *pSite,int iLevel)
{

#define OA_SUMMY_ID 150
#define MA_SUMMY_ID 151
#define CA_SUMMY_ID 152

	static BOOL bInited=FALSE;
	static EQUIP_INFO *pSysEquipInfo=NULL;
	EQUIP_INFO  *pEquip;
	pEquip = pSite->pEquipInfo;
	ALARM_SIG_VALUE  *pSig, *pHead;
	pEquip = pSite->pEquipInfo;
	int i=0;
	if(!bInited)
	{
		for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
		{
			if( pEquip->iEquipID==1)
			{
				pSysEquipInfo= pEquip;
			}
		}
		if( NULL!=pSysEquipInfo)
		{
			bInited=TRUE;
		}
	}
	if( NULL==pSysEquipInfo)
	{
		return FALSE; 
	}
	/*ͳ��OA �澯���ܸ澯����ʱ��OASummy�澯����Ҳ��OA�ģ��ᵼ����ѭ����һֱ��Ϊ
	��OA�澯���ּ����ж��߼�������OA����OA Summy���MA����MA Summy���C
	A����CA Summy  ��ֻ����һ���澯ʱ��ͳ�ƻ�澯ʱ��ȥ����*/
	//OA /MA/CA Active alarms have a OA/MA/A summmy alarm
	pHead = &pSysEquipInfo->pActiveAlarms[iLevel];
	pSig  = pHead->next;
	int iDstSumID=    OA_SUMMY_ID+iLevel-1;
	for (; pSig != pHead; pSig = pSig->next)
	{
		if(pSig->pStdSig->iSigID ==iDstSumID)
		{
			//printf("nLevel [%d]  SigID[%d] \n" ,iLevel,iDstSumID);
			return TRUE;
		}
	}
	return FALSE;
}

/*==========================================================================*
* FUNCTION : Site_UpdateActiveAlarmCnt
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2006-10-20 16:14
*==========================================================================*/
static BOOL Site_UpdateActiveAlarmCnt(IN SITE_INFO *pSite)
{
	int				i, nLevel;
	int             iSiteCnt[ALARM_LEVEL_MAX] = {0};
	EQUIP_INFO		*pEquip;


	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		Equip_UpdateActiveAlarmCnt(pEquip, iSiteCnt);
	}

	for (nLevel = ALARM_LEVEL_NONE; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		pSite->iAlarmCount[nLevel] = iSiteCnt[nLevel];	
	}

	//added as Lemon fixed in hybrid.
	for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		//if there is only one OA/CA/MA alarm , skip self if    OA/CA/MA Summy alarm   is set as  OA/CA/MA
		if( 1==pSite->iAlarmCount[nLevel]
		 &&IsActiveAlarms_ContainSumAlarm(pSite,nLevel))
		{
			//OA/MA/CA  alarms -1 (OA/MA/CA summy)
			//printf("OA/MA/CA Summy was skiped [%d]\n",nLevel);
			pSite->iAlarmCount[nLevel]-=1;
			//total alarm number -1
			pSite->iAlarmCount[0]-=1; 
		}
	//printf("After skip self nLevel [%d] iAlarmCount [%d] \n",nLevel,pSite->iAlarmCount[nLevel]);
	}
	return TRUE;
}


/*==========================================================================*
* FUNCTION : Equip_UpdateActiveAlarmCnt
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN EQUIP_INFO  *pEquip    : 
*            IN int         *piSiteCnt : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2006-10-20 16:16
*==========================================================================*/
static BOOL Equip_UpdateActiveAlarmCnt(IN EQUIP_INFO *pEquip, IN int *piSiteCnt)
{
	int					nLevel;
	int                 iAlarmCnt[ALARM_LEVEL_MAX] = {0};
	ALARM_SIG_VALUE		*pSig, *pHead;

	for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		pHead = &pEquip->pActiveAlarms[nLevel];
		pSig  = pHead->next;

		// the link is a bilink
		for (; pSig != pHead; pSig = pSig->next)
		{
			//only non-suppressed alarm could be counted
			if(!SIG_VALUS_IS_SUPPRESSED(pSig))  
			{
				iAlarmCnt[nLevel] ++;
			}
		}

		pEquip->iAlarmCount[nLevel] = iAlarmCnt[nLevel];
		piSiteCnt[nLevel] += iAlarmCnt[nLevel];
		iAlarmCnt[0] += iAlarmCnt[nLevel];
	}

	// increase the total alarm count
	pEquip->iAlarmCount[0] = iAlarmCnt[0];	
	piSiteCnt[0] += iAlarmCnt[0];

	return TRUE;
}

static BOOL StdEquip_DisplaySignalsValue(IN SITE_INFO *pSite)
{
	int i;
	static BOOL bInited = FALSE;
	
	EQUIP_INFO *pEquip;
	static SIG_BASIC_VALUE *pSigLargDULVD[8] = {NULL};
	int		iSigIdLargDULVD[4] =
	{
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 1),DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 2),
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 3),DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 8)
	};

	if (!bInited)
	{

		pEquip = Init_GetEquipRefById(pSite, 310);
		for (i = 0; i < 4; i++)
		{
			pSigLargDULVD[i] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdLargDULVD[i]);
		}

		pEquip = Init_GetEquipRefById(pSite, 315);
		for (i = 0; i < 4; i++)
		{
			pSigLargDULVD[i+4] = Equip_GetSpecialSigByMergedId(pEquip,iSigIdLargDULVD[i]);
		}

		bInited = TRUE;

	}

	TRACE("LVD22 Signal Value is Valid:%d\n",SIG_VALUE_IS_VALID(pSigLargDULVD[1]));

	TRACE("LVD22 Status:%d\n",pSigLargDULVD[1]->varValue.enumValue);

	TRACE("LVD22 Endable Status:%d\n",pSigLargDULVD[3]->varValue.enumValue);

	TRACE("LVD32 Signal Value is Valid:%d\n",SIG_VALUE_IS_VALID(pSigLargDULVD[5]));

	TRACE("LVD32 Status:%d\n",pSigLargDULVD[5]->varValue.enumValue);

	TRACE("LVD32 Endable Status:%d\n",pSigLargDULVD[7]->varValue.enumValue);

	return TRUE;


}
/*==========================================================================*
* FUNCTION : Equip_SetEnumSigByMergedId
* PURPOSE  :set and save the value of enum signal which sampling channal < 0
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN EQUIP_INFO  *pFuelManagementEquip,
IN	 SIG_ENUM   enumValue, 
IN  int iSigID
* RETURN   : BOOL : 
* COMMENTS : 
*
* CREATOR  : Sean Hsu                   DATE: 2010-09-27 11:03
*==========================================================================*/
static BOOL Equip_SetEnumSigByMergedId(IN OUT EQUIP_INFO  *pEquip,
				       IN  SIG_ENUM  enumValue, 
				       IN  int iMergeSigID )
{
	VAR_VALUE_EX     SigVal;
	int iError;

	if (pEquip == NULL)
	{
		return FALSE;
	}

	SigVal.varValue.enumValue = enumValue;
	SigVal.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	SigVal.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	SigVal.pszSenderName = "Set Enum Sig";

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		pEquip->iEquipID,
		iMergeSigID,
		sizeof(VAR_VALUE_EX),
		&SigVal,
		0);

	return iError;

}

/*==========================================================================*
* NAME	   : gSquareTankParam[MAX_NUM_FUEL_TANK][SQUARE_PARAM_NUM]
* PURPOSE  : Use for Fuel Management Calculate of Square Tank
* RANGE    : equip_data_process.c
* CALLED BY: FuelManagement_Calculate_SquareVolume

* CREATOR  : Sean Hsu                   DATE: 2010-11-22 11:00
*==========================================================================*/
static float gSquareTankParam[MAX_NUM_FUEL_TANK][SQUARE_PARAM_NUM] = {0.0};

/*==========================================================================*
* FUNCTION : FuelManagement_Calculate_SquareVolume
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN EQUIP_INFO  *pFuelManagementEquip,
IN VAR_VALUE *pRemainedFuelheight,
OUT VAR_VALUE *pRemainedFuelVolume, 
OUT VAR_VALUE *pRemainedFuelPercent,
IN BOOL bFinish_Cofig: 
* RETURN   : BOOL : 
* COMMENTS : added for CR# 0357-10-HF NGC
*
* CREATOR  : Sean Hsu                   DATE: 2010-09-27 11:03
*==========================================================================*/
static BOOL FuelManagement_Calculate_SquareVolume(IN EQUIP_INFO  *pFuelManagementEquip,
						  IN float *pRemainedFuelheight,
						  OUT float *pRemainedFuelVolume, 
						  OUT float *pRemainedFuelPercent,
						  IN BOOL bFinish_Cofig)
{
	SIG_BASIC_VALUE *pSig_SqureTank_Length = NULL;
	SIG_BASIC_VALUE *pSig_SqureTank_Width = NULL;
	SIG_BASIC_VALUE *pSig_SqureTank_Height = NULL;


	SIG_BASIC_VALUE *pSig_Config_Error_Status = NULL;

	float L,W,H,h;
	BOOL bConfig_Error = FALSE;
	int iError;
	int iTank_Seq = 0;
	int i;

	if (pFuelManagementEquip == NULL)
	{
		return FALSE;
	}

	pSig_Config_Error_Status = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
	if (pSig_Config_Error_Status == NULL)
	{
		return FALSE;
	}
	bConfig_Error = pSig_Config_Error_Status->varValue.enumValue;

	//get the sequence number of fuel tank
	if( pFuelManagementEquip->iEquipID <= FUEL_MGMT5_EQUIP_ID)
	{
	    iTank_Seq = pFuelManagementEquip->iEquipID - FUEL_MGMT1_EQUIP_ID; 
	}
	else
	{
	    iTank_Seq = pFuelManagementEquip->iEquipID - FUEL_MGMT6_EQUIP_ID + 5; 
	}

	if(iTank_Seq >= MAX_NUM_FUEL_TANK)
	{
	    return FALSE;
	}

	/*if config has been modified, load the config*/
	if (bFinish_Cofig == TRUE)
	{
		/*get the length,width,height of square tank*/
		pSig_SqureTank_Length = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_SQUARE_TANK_LENGTH);
		if (pSig_SqureTank_Length == NULL)
		{
			return FALSE;
		}
		gSquareTankParam[iTank_Seq][0] = pSig_SqureTank_Length->varValue.fValue;

		pSig_SqureTank_Width = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_SQUARE_TANK_WIDTH);
		if (pSig_SqureTank_Width == NULL)
		{
			return FALSE;
		}
		gSquareTankParam[iTank_Seq][1] = pSig_SqureTank_Width->varValue.fValue;

		pSig_SqureTank_Height = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_SQUARE_TANK_HEIGHT);
		if (pSig_SqureTank_Height == NULL)
		{
			return FALSE;
		}
		gSquareTankParam[iTank_Seq][2] = pSig_SqureTank_Height->varValue.fValue;

		/*judge whether the config is error*/
		for (i=0; i<SQUARE_PARAM_NUM; i++)
		{
			if (FLOAT_EQUAL0(gSquareTankParam[iTank_Seq][i]) || (gSquareTankParam[iTank_Seq][i] < 0) || (gSquareTankParam[iTank_Seq][i] > MAX_FULE_VALUE))
			{
				/*fuel tank config error alarm*/
				iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
				if (iError != ERR_DXI_OK)
				{
					return FALSE;
				}
				return FALSE;
			}
		}

		//close the fuel tank config error alarm
		iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, FALSE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
		if (iError != ERR_DXI_OK)
		{
			return FALSE;
		}

	}//end if(bFinish_Cofig == TRUE)
	else
	{
		//no process
	}

	/*when cofig not set, dont calculate*/
	if (bConfig_Error == TRUE)
	{
		return FALSE;
	}
	/*calculate*/
	else
	{
		L = gSquareTankParam[iTank_Seq][0];
		W = gSquareTankParam[iTank_Seq][1];
		H = gSquareTankParam[iTank_Seq][2];
		h = *pRemainedFuelheight;

		//height error
		if ((h > H) || (h < 0))
		{
			//set the multi-sharp height error alarm   
			iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
			if (iError != ERR_DXI_OK)
			{
				return FALSE;
			}

			return FALSE;
		}
		//v = L * W * h
		else
		{
			*pRemainedFuelVolume = (float)( L * W * h) / MILLILITER_PER_LITER;
			*pRemainedFuelPercent = 100 * ((float)(h / H));	
		}

		//close the multi-sharp height error alarm 
		iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, FALSE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
		if (iError != ERR_DXI_OK)
		{
			return FALSE;
		}

	}

	return TRUE;
}

/*==========================================================================*
* NAME	   : gStandingTankParam[MAX_NUM_FUEL_TANK][STANDING_CYLINDER_PARAM_NUM]
* PURPOSE  : Use for Fuel Management Calculate of Standing cylinder tank 
* RANGE    : equip_data_process.c
* CALLED BY: FuelManagement_Calculate_StandingCylinderVolume

* CREATOR  : Sean Hsu                   DATE: 2010-11-22 11:00
*==========================================================================*/
static float gStandingTankParam[MAX_NUM_FUEL_TANK][STANDING_CYLINDER_PARAM_NUM] = {0.0};

/*==========================================================================*
* FUNCTION : FuelManagement_Calculate_StandingCylinderVolume
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN EQUIP_INFO  *pFuelManagementEquip,
IN VAR_VALUE *pRemainedFuelheight,
OUT VAR_VALUE *pRemainedFuelVolume, 
OUT VAR_VALUE *pRemainedFuelPercent:
IN BOOL bFinish_Cofig: 
* RETURN   : BOOL : 
* COMMENTS : added for CR# 0357-10-HF NGC
*
* CREATOR  : Sean Hsu                   DATE: 2010-09-27 11:41
*==========================================================================*/
static BOOL FuelManagement_Calculate_StandingCylinderVolume(IN EQUIP_INFO  *pFuelManagementEquip,
							    IN float *pRemainedFuelheight,
							    OUT float *pRemainedFuelVolume, 
							    OUT float *pRemainedFuelPercent,
							    IN BOOL bFinish_Cofig)
{
	SIG_BASIC_VALUE *pSig_StandingCylinder_Diameter = NULL;
	SIG_BASIC_VALUE *pSig_StandingCylinder_Height = NULL; 


	SIG_BASIC_VALUE *pSig_Config_Error_Status = NULL;

	float R,H,h;
	BOOL bConfig_Error = FALSE;
	int iError;
	int iTank_Seq = 0;
	int i;

	if (pFuelManagementEquip == NULL)
	{
		return FALSE;
	}

	pSig_Config_Error_Status = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
	if (pSig_Config_Error_Status == NULL)
	{
		return FALSE;
	}
	bConfig_Error = pSig_Config_Error_Status->varValue.enumValue;

	//get the sequence number of fuel tank
	if( pFuelManagementEquip->iEquipID <= FUEL_MGMT5_EQUIP_ID)
	{
	    iTank_Seq = pFuelManagementEquip->iEquipID - FUEL_MGMT1_EQUIP_ID; 
	}
	else
	{
	    iTank_Seq = pFuelManagementEquip->iEquipID - FUEL_MGMT6_EQUIP_ID + 5; 
	}

	if(iTank_Seq >= MAX_NUM_FUEL_TANK)
	{
	    return FALSE;
	}


	/*if config has been modified, load the config*/
	if (bFinish_Cofig == TRUE)
	{	
		/*get the diameter,height of standing cylinder tank*/
		pSig_StandingCylinder_Diameter = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_STANDING_CYLINDER_DIAMETER);
		if (pSig_StandingCylinder_Diameter == NULL)
		{
			return FALSE;
		}
		gStandingTankParam[iTank_Seq][0] = pSig_StandingCylinder_Diameter->varValue.fValue;

		pSig_StandingCylinder_Height = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_STANDING_CYLINDER_HEIGHT);
		if (pSig_StandingCylinder_Height == NULL)
		{
			return FALSE;
		}
		gStandingTankParam[iTank_Seq][1] = pSig_StandingCylinder_Height->varValue.fValue;

		for (i=0; i<STANDING_CYLINDER_PARAM_NUM; i++)
		{
			if (FLOAT_EQUAL0(gStandingTankParam[iTank_Seq][i]) || (gStandingTankParam[iTank_Seq][i] < 0) || (gStandingTankParam[iTank_Seq][i] > MAX_FULE_VALUE))
			{
				/*fuel tank config error alarm*/
				iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
				if (iError != ERR_DXI_OK)
				{
					return FALSE;
				}
				return FALSE;
			}
		}

		//close the fuel tank config error alarm
		iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, FALSE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
		if (iError != ERR_DXI_OK)
		{
			return FALSE;
		}

	}//end if(bFinish_Cofig == TRUE)
	else
	{
		//no process
	}

	/*when cofig not set, dont calculate*/
	if (bConfig_Error == TRUE)
	{
		return FALSE;
	}
	/*calculate*/
	else
	{
		R = (float)gStandingTankParam[iTank_Seq][0] / 2;
		H = (float)gStandingTankParam[iTank_Seq][1];
		h = *pRemainedFuelheight;

		//height error
		if ((h > H) || (h < 0))
		{
			//set the multi-sharp height error alarm 
			iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
			if (iError != ERR_DXI_OK)
			{
				return FALSE;
			}

			return FALSE;
		}
		//calculate
		//v = ��* R^2 * h
		else
		{
			*pRemainedFuelVolume = PI * (float)(h * R * R) / MILLILITER_PER_LITER;
			*pRemainedFuelPercent = 100 * ((float)h / H);
		}

		//close the multi-sharp height error alarm 
		iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, FALSE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
		if (iError != ERR_DXI_OK)
		{
			return FALSE;
		}

	}

	return TRUE;
}

/*==========================================================================*
* NAME	   : gLayingTankParam[MAX_NUM_FUEL_TANK][LAYING_CYLINDER_PARAM_NUM]
* PURPOSE  : Use for Fuel Management Calculate of Laying cylinder tank 
* RANGE    : equip_data_process.c
* CALLED BY: FuelManagement_Calculate_LayingCylinderVolume

* CREATOR  : Sean Hsu                   DATE: 2010-11-22 11:00
*==========================================================================*/
static float gLayingTankParam[MAX_NUM_FUEL_TANK][LAYING_CYLINDER_PARAM_NUM] = {0.0};

/*==========================================================================*
* FUNCTION : FuelManagement_Calculate_LayingCylinderVolume
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN EQUIP_INFO  *pFuelManagementEquip,
IN VAR_VALUE *pRemainedFuelheight,
OUT VAR_VALUE *pRemainedFuelVolume, 
OUT VAR_VALUE *pRemainedFuelPercent:
IN BOOL bFinish_Cofig: 
* RETURN   : BOOL : 
* COMMENTS : added for CR# 0357-10-HF NGC
*
* CREATOR  : Sean Hsu                   DATE: 2010-09-27 13:45
*==========================================================================*/
static BOOL FuelManagement_Calculate_LayingCylinderVolume(IN EQUIP_INFO  *pFuelManagementEquip,
							  IN float *pRemainedFuelheight,
							  OUT float *pRemainedFuelVolume, 
							  OUT float *pRemainedFuelPercent,
							  IN BOOL bFinish_Cofig)

{
	SIG_BASIC_VALUE *pSig_LayingCylinder_Diameter = NULL;
	SIG_BASIC_VALUE *pSig_LayingCylinder_Length = NULL;

	SIG_BASIC_VALUE *pSig_Config_Error_Status = NULL;

	float L,R,h;

	BOOL bConfig_Error = FALSE;
	int iError;
	int iTank_Seq = 0;
	int i;

	if (pFuelManagementEquip == NULL)
	{
		return FALSE;
	}

	pSig_Config_Error_Status = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
	if (pSig_Config_Error_Status == NULL)
	{
		return FALSE;
	}
	bConfig_Error = pSig_Config_Error_Status->varValue.enumValue;

	//get the sequence number of fuel tank
	if( pFuelManagementEquip->iEquipID <= FUEL_MGMT5_EQUIP_ID)
	{
	    iTank_Seq = pFuelManagementEquip->iEquipID - FUEL_MGMT1_EQUIP_ID; 
	}
	else
	{
	    iTank_Seq = pFuelManagementEquip->iEquipID - FUEL_MGMT6_EQUIP_ID + 5; 
	}

	if(iTank_Seq >= MAX_NUM_FUEL_TANK)
	{
	    return FALSE;
	}

	/*if config has been modified, load the config*/
	if (bFinish_Cofig == TRUE)
	{
		/*get the diameter,length of laying cylinder tank*/
		pSig_LayingCylinder_Diameter = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_LAYING_CYLINDER_DIAMETER);
		if (pSig_LayingCylinder_Diameter == NULL)
		{
			return FALSE;
		}
		gLayingTankParam[iTank_Seq][0] = pSig_LayingCylinder_Diameter->varValue.fValue;

		pSig_LayingCylinder_Length = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_LAYING_CYLINDER_LENGTH);
		if (pSig_LayingCylinder_Length == NULL)
		{
			return FALSE;
		}
		gLayingTankParam[iTank_Seq][1] = pSig_LayingCylinder_Length->varValue.fValue;

		for (i=0; i<LAYING_CYLINDER_PARAM_NUM; i++)
		{
			if (FLOAT_EQUAL0(gLayingTankParam[iTank_Seq][i]) || (gLayingTankParam[iTank_Seq][i] < 0) || (gLayingTankParam[iTank_Seq][i] > MAX_FULE_VALUE))
			{
				/*fuel tank config error alarm*/
				iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
				if (iError != ERR_DXI_OK)
				{
					return FALSE;
				}
				return FALSE;
			}
		}

		//close the fuel tank config error alarm
		iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, FALSE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
		if (iError != ERR_DXI_OK)
		{
			return FALSE;
		}

	}//end if(bFinish_Cofig == TRUE)
	else
	{
		//no process
	}

	/*when cofig not set, dont calculate*/
	if (bConfig_Error == TRUE)
	{
		return FALSE;
	}
	/*calculate*/
	else
	{
		R = (float)gLayingTankParam[iTank_Seq][0] / 2;
		L = (float)gLayingTankParam[iTank_Seq][1];
		h = *pRemainedFuelheight;

		//height error
		if ((h > 2 * R) || (h < 0))
		{
			//set the multi-sharp height error alarm 
			iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
			if (iError != ERR_DXI_OK)
			{
				return FALSE;
			}

			return FALSE;
		}
		//calculate
		//v = R^2 * acos(1 - h/R) * L + (R - h) * sqrt(2Rh - h^2) * L
		else
		{
			//*pRemainedFuelVolume = (float)(L * (R * R * acosf(1 - h / R) - (R - h) * sqrt(2 * R * h - h * h))) / MILLILITER_PER_LITER;
			*pRemainedFuelVolume = (float)(R * R * CalculateArccos(1 - h / R) - (R - h) * sqrt(2 * R * h - h * h)) * L / MILLILITER_PER_LITER;
			*pRemainedFuelPercent = 100 * ((float)((*pRemainedFuelVolume) / (PI * R * R * L)) * MILLILITER_PER_LITER);
		}

		//close the multi-sharp height error alarm 
		iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, FALSE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
		if (iError != ERR_DXI_OK)
		{
			return FALSE;
		}

	}

	return TRUE;
}


/*==========================================================================*
* NAME	   : gCalibPoint[MAX_NUM_FUEL_TANK][MAX_CALIB_POINTS_NUM][2]
gCalibNum
* PURPOSE  : Use for Fuel Management Calculate of Multi-Sharp Tank
* RANGE    : equip_data_process.c
* CALLED BY: FuelManagement_Calculate_MultiSharpVolume

* CREATOR  : Sean Hsu                   DATE: 2010-11-20 10:10
*==========================================================================*/
static float gCalibPoint[MAX_NUM_FUEL_TANK][MAX_CALIB_POINTS_NUM][2] = {0.0};
static int gCalibNum[MAX_NUM_FUEL_TANK] = {0};

/*==========================================================================*
* FUNCTION : FuelManagement_Calculate_MultiSharpVolume
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN EQUIP_INFO  *pFuelManagementEquip,
IN int CalibNum,
IN VAR_VALUE *pRemainedFuelheight,
OUT VAR_VALUE *pRemainedFuelVolume, 
OUT VAR_VALUE *pRemainedFuelPercent:
IN BOOL bFinish_Cofig: 
* RETURN   : BOOL : 
* COMMENTS : added for CR# 0357-10-HF NGC
*
* CREATOR  : Sean Hsu                   DATE: 2010-09-27 14:33
*==========================================================================*/
static BOOL FuelManagement_Calculate_MultiSharpVolume(IN EQUIP_INFO  *pFuelManagementEquip,
						      IN float *pRemainedFuelheight,
						      OUT float *pRemainedFuelVolume, 
						      OUT float *pRemainedFuelPercent,
						      IN BOOL bFinish_Cofig)
{
	SIG_BASIC_VALUE *pSigCalibPointHeight = NULL;
	SIG_BASIC_VALUE *pSigCalibPointVolume = NULL;
	SIG_BASIC_VALUE *pSigCalibPointNum = NULL;

	SIG_BASIC_VALUE *pSig_Config_Error_Status = NULL;

	SIG_BASIC_VALUE pSigValue;

	int i,j;
	int iError;
	int iCalibNum = 0;
	float h = *pRemainedFuelheight; 
	BOOL bConfig_Error = FALSE;
	int iTank_Seq = 0;

	if (pFuelManagementEquip == NULL)
	{
		return FALSE;
	}

	pSig_Config_Error_Status = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
	if (pSig_Config_Error_Status == NULL)
	{
		return FALSE;
	}
	bConfig_Error = pSig_Config_Error_Status->varValue.enumValue;

	//get the sequence number of fuel tank
	if( pFuelManagementEquip->iEquipID <= FUEL_MGMT5_EQUIP_ID)
	{
	    iTank_Seq = pFuelManagementEquip->iEquipID - FUEL_MGMT1_EQUIP_ID; 
	}
	else
	{
	    iTank_Seq = pFuelManagementEquip->iEquipID - FUEL_MGMT6_EQUIP_ID + 5; 
	}

	if(iTank_Seq >= MAX_NUM_FUEL_TANK)
	{
	    return FALSE;
	}

	/*if config has been modified, load the config into gCalibPoint*/
	if (bFinish_Cofig == TRUE)
	{
		/*get the calibration points number of multi-sharp tank*/
		pSigCalibPointNum = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_CALIBRATION_POINTS_NUM);
		if (pSigCalibPointNum == NULL)
		{
			return FALSE;
		}

		gCalibNum[iTank_Seq] = pSigCalibPointNum->varValue.ulValue;
		if ((gCalibNum[iTank_Seq] < MIN_CALIB_POINTS_NUM) || (gCalibNum[iTank_Seq] > MAX_CALIB_POINTS_NUM))
		{
			/*fuel tank config error alarm*/
			iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
			if (iError != ERR_DXI_OK)
			{
				return FALSE;
			}

			return FALSE;
		}

		/*get the calibration points of multi-sharp tank*/
		for(i=0; i<gCalibNum[iTank_Seq]; i++)
		{
			/*load the height*/
			pSigCalibPointHeight = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_CALIBRATION_POINTS_HEIGHT_START+i*2);
			if (pSigCalibPointHeight == NULL)	//can't get calibration point height
			{
				return FALSE;
			}
			gCalibPoint[iTank_Seq][i][0] = pSigCalibPointHeight->varValue.fValue;
			if ((i>0) && (gCalibPoint[iTank_Seq][i][0] <= gCalibPoint[iTank_Seq][i-1][0])) //height order error
			{
				/*fuel tank config error alarm */
				iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
				if (iError != ERR_DXI_OK)
				{
					return FALSE;
				}
				return FALSE;
			}

			/*load the volume*/
			pSigCalibPointVolume = Equip_GetSpecialSigByMergedId(pFuelManagementEquip, SIG_ID_FUELMANAGEMENT_CALIBRATION_POINTS_VOLUME_START+i*2);
			if (pSigCalibPointVolume == NULL)	//can't get calibration point volume
			{
				return FALSE;
			}
			gCalibPoint[iTank_Seq][i][1] = pSigCalibPointVolume->varValue.fValue;
			if ((i>0) && (gCalibPoint[iTank_Seq][i][1] <= gCalibPoint[iTank_Seq][i-1][1])) //volume order error
			{
				/*fuel tank config error alarm*/
				iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
				if (iError != ERR_DXI_OK)
				{
					return FALSE;
				}
				return FALSE;
			}
		}


		//close the fuel tank config error alarm
		iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, FALSE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
		if (iError != ERR_DXI_OK)
		{
			return FALSE;
		}

	}//end if(bFinish_Cofig == TRUE)
	else
	{
		//no process
	}

	/*calculate*/
	if (bConfig_Error == TRUE)
	{
		return FALSE;
	}
	else
	{
		/*load iCalibNum from gCalibNum*/
		iCalibNum = gCalibNum[iTank_Seq];
		if (iCalibNum == 0)
		{
			return FALSE;
		}

		/*fCalibPoint[i][0]:calibration point of height , fCalibPoint[i][1]:calibration point of volume*/
		float fCalibPoint[iCalibNum][2] ; 

		/*load fCalibPoint from gCalibPoint*/
		for(i=0; i<iCalibNum; i++)
		{
			fCalibPoint[i][0] = gCalibPoint[iTank_Seq][i][0];
			fCalibPoint[i][1] = gCalibPoint[iTank_Seq][i][1];
		}
		/*the config has not set at the first time*/
		if ((fCalibPoint[0][0] == fCalibPoint[iCalibNum-1][0]) || (fCalibPoint[0][1] == fCalibPoint[iCalibNum-1][1]))
		{
			return FALSE;
		}

		//height error
		if ((h < 0) || (h > fCalibPoint[iCalibNum-1][0] ))
		{	
			//set the multi-sharp height error alarm 
			iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, TRUE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
			if (iError != ERR_DXI_OK)
			{
				return FALSE;
			}

			return FALSE;
		}
		else if (h <= fCalibPoint[0][0] )
		{
			if (FLOAT_EQUAL0(fCalibPoint[0][0]))
			{
				*pRemainedFuelVolume = 0;
				*pRemainedFuelPercent = 0;
			}
			//v = (V1/H1) * h
			else
			{
				*pRemainedFuelVolume = (float)((fCalibPoint[0][1]/fCalibPoint[0][0]) * h);
				*pRemainedFuelPercent = 100 * ((float)(*pRemainedFuelVolume) / fCalibPoint[iCalibNum-1][1]);
			}
		} 
		//v = ((V2 - V1) * h+(H2*V1 - H1*V2)) / (H2 - H1)
		else
		{
			j = 0;
			while (h > fCalibPoint[j][0])
			{
				j++;
				if (j == iCalibNum)
				{
					return FALSE;
				}

			}
			*pRemainedFuelVolume = (float)(((fCalibPoint[j][1] - fCalibPoint[j-1][1]) * h 
				+ (fCalibPoint[j][0]*fCalibPoint[j-1][1] - fCalibPoint[j-1][0]*fCalibPoint[j][1])) 
				/ (fCalibPoint[j][0] - fCalibPoint[j-1][0]));
			*pRemainedFuelPercent = 100 * ((float)(*pRemainedFuelVolume) / fCalibPoint[iCalibNum-1][1]);

		}

		//close the multi-sharp height error alarm 
		iError = Equip_SetEnumSigByMergedId(pFuelManagementEquip, FALSE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
		if (iError != ERR_DXI_OK)
		{
			return FALSE;
		}
	}

	return TRUE;
}


/*==========================================================================*
* FUNCTION : StdEquip_FuelManagement_ProcessSpecialSignals
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite : 
* RETURN   : BOOL : 
* COMMENTS : added for CR# 0357-10-HF NGC
*
* CREATOR  : Sean Hsu                   DATE: 2010-09-28 9:00
*==========================================================================*/
static BOOL StdEquip_FuelManagement_ProcessSpecialSignals(IN SITE_INFO *pSite)
{
	int iError = 0;

	int iBufLen;
	static BOOL	bInited = FALSE;
	BOOL		bChange = FALSE;
	BOOL		bFlag = FALSE;

	int i,j;
	EQUIP_INFO	*pEquip = NULL;
	SIG_BASIC_VALUE* pSigValue = NULL;
	time_t	tmNow = time(NULL);;
	static time_t tmLast[MAX_NUM_FUEL_TANK] = {0};

	static int iDelayCount = 0;
	static BOOL bDelayFlag = TRUE;

	VAR_VALUE varRemainedFuelHeight;
	VAR_VALUE varRemainedFuelVolume;
	VAR_VALUE varRemainedFuelPercent;
	VAR_VALUE varFuelTheftStatus;
	static VAR_VALUE varLastRemainedFuelVolume[MAX_NUM_FUEL_TANK] = {0};

	int iFuelTankType = 0;

	float fFuelConsumeSpeed = 0.0;

	//BOOL		bTheftStatus = FALSE;
	BOOL		bFinish_Cofig = FALSE;
	static  int		iLastTankType[MAX_NUM_FUEL_TANK] = {0};

	int pEquipID[7] = {FUEL_MGMT1_EQUIP_ID,
		FUEL_MGMT2_EQUIP_ID,
		FUEL_MGMT3_EQUIP_ID,
		FUEL_MGMT4_EQUIP_ID,
		FUEL_MGMT5_EQUIP_ID,
		FUEL_MGMT6_EQUIP_ID,
		FUEL_MGMT7_EQUIP_ID};

	int pFuelMgmtSigID[]={SIG_ID_FUELMANAGEMENT_REMAINED_FUEL_HEIGHT,
		SIG_ID_FUELMANAGEMENT_REMAINED_FUEL_VOLUME,
		SIG_ID_FUELMANAGEMENT_REMAINED_FUEL_PERCENT,
		SIG_ID_FUELMANAGEMENT_FUEL_THEFT_STATUS,
		SIG_ID_FUELMANAGEMENT_MAX_CONSUMPTION_SPEED} ;

	static SIG_BASIC_VALUE *pSigFuelMgmt[MAX_NUM_FUEL_TANK][NUM_SIG_FUEL_MGMT] = {{NULL}};

	/*init*/
	if (!bInited)
	{
		for(i=0; i< ITEM_OF(pEquipID); i++)
		{
			pEquip = Init_GetEquipRefById(pSite, pEquipID[i]);

			if (pEquip == NULL)
			{
				continue;
			}
			else
			{	
				if ((pEquip->pStdEquip->iTypeID != STD_ID_FUEL_MANAGEMENT_EQUIP)
				    && (pEquip->pStdEquip->iTypeID != STD_ID_OB_FUEL_MANAGEMENT_EQUIP))
				{
					return FALSE;
				}
				for (j=0; j<NUM_SIG_FUEL_MGMT; j++)
				{
					pSigFuelMgmt[i][j] = Equip_GetSpecialSigByMergedId(pEquip, pFuelMgmtSigID[j]);
					if (pSigFuelMgmt[i][j] == NULL)
					{
						return FALSE;
					}
				}

				if (!SIG_VALUE_IS_CONFIGURED(pSigFuelMgmt[i][0]))
				{
					SIG_STATE_CLR(pSigFuelMgmt[i][0], VALUE_STATE_IS_CONFIGURED);
					//should not be NULL, the signal will be used directly in the following process
					//pSigFuelMgmt[i][0] = NULL;  
				}
				else
				{
					SIG_STATE_SET(pSigFuelMgmt[i][0], VALUE_STATE_IS_VALID);
				}

				/*load default config at the first boot*/
				iError = Equip_SetEnumSigByMergedId(pEquip, TRUE, SIG_ID_FUELMANAGEMENT_FINISH_CONFIG);
				if (iError != ERR_DXI_OK)
				{
					return FALSE;
				}

				/*clear the height error status and config error status*/
				iError = Equip_SetEnumSigByMergedId(pEquip, FALSE, SIG_ID_FUELMANAGEMENT_HEIGHT_ERROR_STATUS);
				if (iError != ERR_DXI_OK)
				{
					return FALSE;
				}

				iError = Equip_SetEnumSigByMergedId(pEquip, FALSE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
				if (iError != ERR_DXI_OK)
				{
					return FALSE;
				}
			}

		}	

		bInited = TRUE;
	}

	/*don't calculate in the first 3 times cycle*/
	if(bDelayFlag == TRUE)
	{
		iDelayCount++;
		if (iDelayCount > 3)
		{
			bDelayFlag = FALSE;
		}
		return FALSE;
	}

	/*calculate remained volume and percent*/
	for(i=0; i<ITEM_OF(pEquipID); i++)
	{	
		pEquip = Init_GetEquipRefById(pSite, pEquipID[i]);

		//G3_OPT  [loader], by Lin.Tao.Thomas, 2013-4
		//if((pEquip == NULL) || pEquip->bWorkStatus == FALSE)
		if(pEquip == NULL)
		{
			continue;
		}
		if (pEquip->bWorkStatus == FALSE)
		{
			SIG_STATE_CLR(pSigFuelMgmt[i][1], VALUE_STATE_IS_VALID);
			SIG_STATE_CLR(pSigFuelMgmt[i][2], VALUE_STATE_IS_VALID);

			continue;
		}

		if (SIG_STATE_TEST(pSigFuelMgmt[i][0], VALUE_STATE_IS_VALID))
		{
			//get the fuel tank height
			varRemainedFuelHeight.fValue = pSigFuelMgmt[i][0]->varValue.fValue; 
		}
		else
		{
			/*cant get height, reverse display*/
			SIG_STATE_CLR(pSigFuelMgmt[i][1], VALUE_STATE_IS_VALID);
			SIG_STATE_CLR(pSigFuelMgmt[i][2], VALUE_STATE_IS_VALID);

			continue;
		}

		//get finish config status
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			pEquipID[i],		
			SIG_ID_FUELMANAGEMENT_FINISH_CONFIG,		
			&iBufLen,			
			(void *)&pSigValue,			
			10000);
		if (pSigValue == NULL || iError != ERR_DXI_OK)
		{
			return FALSE;
		}
		bFinish_Cofig = pSigValue->varValue.enumValue;

		//don't change fuel type until setting finish config status to 1;
		if (bFinish_Cofig == TRUE)
		{
			//get fuel tank type
			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				pEquipID[i],		
				SIG_ID_FUELMANAGEMENT_FULE_TANK_TYPE,		
				&iBufLen,			
				(void *)&pSigValue,			
				10000);
			if (pSigValue == NULL || iError != ERR_DXI_OK)
			{
				return FALSE;
			}
			iFuelTankType = pSigValue->varValue.enumValue;

			iLastTankType[i] = iFuelTankType;
		}

		//calculate volume and percent of remained fuel in kinds of tank case
		if (iLastTankType[i] == 0)//square tank
		{
			bFlag = FuelManagement_Calculate_SquareVolume(pEquip, &(varRemainedFuelHeight.fValue), 
				&(varRemainedFuelVolume.fValue), &(varRemainedFuelPercent.fValue),bFinish_Cofig);
		}
		else if (iLastTankType[i] == 1)//standing cylinder tank
		{
			bFlag = FuelManagement_Calculate_StandingCylinderVolume(pEquip,&(varRemainedFuelHeight.fValue), 
				&(varRemainedFuelVolume.fValue), &(varRemainedFuelPercent.fValue),bFinish_Cofig);
		}
		else if (iLastTankType[i] == 2)//laying cylinder tank
		{
			bFlag =  FuelManagement_Calculate_LayingCylinderVolume(pEquip,&(varRemainedFuelHeight.fValue), 
				&(varRemainedFuelVolume.fValue), &(varRemainedFuelPercent.fValue),bFinish_Cofig);
		}
		else if (iLastTankType[i] == 3)//mulit-sharp tank
		{
			bFlag =  FuelManagement_Calculate_MultiSharpVolume(pEquip, &(varRemainedFuelHeight.fValue),
				&(varRemainedFuelVolume.fValue), &(varRemainedFuelPercent.fValue),bFinish_Cofig);
		}
		else //none
		{
			/*close the cofig error alarm*/
			iError = Equip_SetEnumSigByMergedId(pEquip, FALSE, SIG_ID_FUELMANAGEMENT_CONFIG_ERROR_STATUS);
			if (iError != ERR_DXI_OK)
			{
				return FALSE;
			}

			bFlag = FALSE;
		}

		/*update the remained volume and percent*/
		if (bFlag == TRUE)
		{
			/*signal is invalid*/
			if ((varRemainedFuelVolume.fValue < 0.1) || (varRemainedFuelVolume.fValue > MAX_FULE_VALUE) || (varRemainedFuelPercent.fValue < 0.1))
			{
				varLastRemainedFuelVolume[i].fValue = 0;
				SIG_STATE_CLR(pSigFuelMgmt[i][1], VALUE_STATE_IS_VALID);
				SIG_STATE_CLR(pSigFuelMgmt[i][2], VALUE_STATE_IS_VALID);
			}
			else 
			{
				/*when calculate successfully,set signal value(volume and percent)*/
				SIG_STATE_SET(pSigFuelMgmt[i][1], VALUE_STATE_IS_VALID);
				SIG_STATE_SET(pSigFuelMgmt[i][2], VALUE_STATE_IS_VALID);
				Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigFuelMgmt[i][1], &varRemainedFuelVolume, tmNow, FALSE);
				Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigFuelMgmt[i][2], &varRemainedFuelPercent, tmNow, FALSE);
			}

		}
		else
		{
			/*when calculate unsuccessfully, reverse display*/
			varLastRemainedFuelVolume[i].fValue = 0;
			SIG_STATE_CLR(pSigFuelMgmt[i][1], VALUE_STATE_IS_VALID);
			SIG_STATE_CLR(pSigFuelMgmt[i][2], VALUE_STATE_IS_VALID);
		}

		//update last remained fuel volume,when resetting parameters of tank
		if (bFinish_Cofig == TRUE)
		{
			varLastRemainedFuelVolume[i].fValue = 0;

			/*set the finish config to false*/
			iError = Equip_SetEnumSigByMergedId(pEquip, FALSE, SIG_ID_FUELMANAGEMENT_FINISH_CONFIG);
			if (iError != ERR_DXI_OK)
			{
				return FALSE;
			}
		}

		//judge theft alarm status
		if (bFlag == TRUE)
		{
			varFuelTheftStatus.enumValue = 0;
			tmNow = time(NULL);
			if ((abs(tmNow - tmLast[i]) > TIME_INTERVAL) && (abs(tmNow - tmLast[i]) < MAX_TIME_INTERVAL))
			{
				fFuelConsumeSpeed = (varLastRemainedFuelVolume[i].fValue - varRemainedFuelVolume.fValue) / ((float)(tmNow - tmLast[i]) / SECOND_PER_MINUTE);

				//if theft appears, set the signal value
				if (fFuelConsumeSpeed > pSigFuelMgmt[i][4]->varValue.fValue)
				{
					varFuelTheftStatus.enumValue = 1;
					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigFuelMgmt[i][3], &varFuelTheftStatus, tmNow, FALSE);
				}

				/*if theft disappears, the theft alarm will not cancel, unless user set the Reset Theft Alarm on the web.*/
				else
				{
					//no process;
				}

				varLastRemainedFuelVolume[i].fValue = varRemainedFuelVolume.fValue;
				tmLast[i] = tmNow;
			}
			/*avoid long time waiting*/
			else if (abs(tmNow - tmLast[i]) > MAX_TIME_INTERVAL)
			{
				fFuelConsumeSpeed = (varLastRemainedFuelVolume[i].fValue - varRemainedFuelVolume.fValue) / ((float)(MAX_TIME_INTERVAL) / SECOND_PER_MINUTE);

				//if theft appears, set the signal value
				if (fFuelConsumeSpeed > pSigFuelMgmt[i][4]->varValue.fValue)
				{
					varFuelTheftStatus.enumValue = 1;
					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigFuelMgmt[i][3], &varFuelTheftStatus, tmNow, FALSE);
				}

				//if theft disappears, the theft alarm will not cancel, unless user set the Reset Theft Alarm on the web.
				else
				{
					//no process;
				}

				varLastRemainedFuelVolume[i].fValue = varRemainedFuelVolume.fValue;
				tmLast[i] = tmNow;
			}
			else
			{
				//no process;
			}
		}

		bChange |= bFlag;
		pEquip->iProcessingState = EQP_STATE_PROCESS_DOING;

	}

	return bChange;
}
/*==========================================================================*
* FUNCTION : StdEquip_TempSig_ProcessSpecialSignals
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN SITE_INFO  *pSite : 
* RETURN   : BOOL : 
* COMMENTS : New version is as below to replace this(Jimmy2011/12/06)
*
* CREATOR  : WJ                   DATE: 2011-02-13 9:00
*==========================================================================*/
//static BOOL StdEquip_TempSig_ProcessSpecialSignals(IN SITE_INFO *pSite)
//{
//
//	static BOOL			bInited = FALSE;
//	static EQUIP_INFO	*pEquip = NULL;
//	int					i=0;
//	time_t				tmNow = time(NULL);
//	//static int			iWaitFlag;
//	static BOOL bDelayFlag = TRUE;
//	static int iDelayCount = 0;
//
//	int j;
//	VAR_VALUE setValueTemp;
//
//#define NUM_BATT	46
//#define NUM_TEMP_SIG	3
//#define NUM_TEMP	7
////#define NUM_LARGEDU_TEMP	3
//#define SYSTEM_UNIT	1
//#define BATTERY_GROUP_UNIT	115
////#define LARGEDU_UNIT_1		299
//#define NUM_SYS_TEMP_SIG	2
//#define NUM_BATTGROUP_TEMP_SIG	2
//#define NUM_SMBRC_TEMP_SIG	12
//#define NUM_SMBRC	12
//
//	static SIG_BASIC_VALUE *pSigBattTempSig[NUM_BATT][NUM_TEMP_SIG] = {NULL};
//
//	static SIG_BASIC_VALUE *pSigEnvTempSig[NUM_TEMP_SIG] = {NULL};
//
//	static SIG_BASIC_VALUE *pSigSysTempSig[NUM_TEMP] = {NULL};
//
//	//static SIG_BASIC_VALUE *pSigLargeDUTempSig[NUM_LARGEDU_TEMP] = {NULL};
//
//
//	static SIG_BASIC_VALUE *pSigBattGroupTempSig[NUM_BATTGROUP_TEMP_SIG] = {NULL};
//	static SIG_BASIC_VALUE *pSigSMBRCTempSig[NUM_SMBRC_TEMP_SIG] = {NULL};
//
//
//	// init
//	if (!bInited)
//	{
//
//		//iWaitFlag = 0;
//
//
//		int		iSigIdBattTemp[NUM_TEMP_SIG] =
//		{
//			98,11,100
//
//		};
//
//		int		iSigIdEnvTemp[NUM_SYS_TEMP_SIG] =
//		{
//			69,208
//
//		};
//
//		int		iSigIdSYSTemp[NUM_TEMP] =
//		{
//			43,44,
//			45,46,
//			47,52,
//			51
//
//		};
//
//		bInited = TRUE;
//
//
//		pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
//		for (i = 0; i < NUM_TEMP; i++)
//		{
//			pSigSysTempSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,iSigIdSYSTemp[i]);
//
//		}
//
//
//
//		for (i = 0; i < NUM_SYS_TEMP_SIG; i++)
//		{
//			if(i == 1)
//			{
//				pSigEnvTempSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,iSigIdEnvTemp[i]);
//
//			}
//			else
//			{
//				pSigEnvTempSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,iSigIdEnvTemp[i]);
//			}
//
//		}
//
//
//		for(i = 0; i < NUM_BATT; i++)
//		{
//			if(i < 42)
//			{
//				pEquip = Init_GetEquipRefById(pSite, 116 + i);
//
//			}
//			else
//			{
//				pEquip = Init_GetEquipRefById(pSite, 164 + i);
//			}
//
//			
//			for(j = 0; j < NUM_TEMP_SIG; j++)
//			{
//				if(j == 1)
//				{
//					pSigBattTempSig[i][j] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,iSigIdBattTemp[j]);
//
//				}
//				else
//				{
//					pSigBattTempSig[i][j] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,iSigIdBattTemp[j]);
//				}
//				
//			}
//		}
//
//
//
//		for(i = 0; i < NUM_SMBRC; i++)
//		{
//			pEquip = Init_GetEquipRefById(pSite, 668 + i);
//
//			pSigSMBRCTempSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,13);
//
//		}
//
//
//
//		pEquip = Init_GetEquipRefById(pSite, BATTERY_GROUP_UNIT);
//
//		for (i = 0; i < NUM_BATTGROUP_TEMP_SIG; i++)
//		{
//			if(i == 1)
//			{
//				pSigBattGroupTempSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,63);
//
//			}
//			else
//			{
//				pSigBattGroupTempSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,36);
//			}
//
//		}
//
//
//		/*pEquip = Init_GetEquipRefById(pSite, LARGEDU_UNIT_1);
//
//		for(i = 0; i < NUM_LARGEDU_TEMP; i++)
//		{
//			pSigLargeDUTempSig[i] = (SET_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,i + 1);
//
//		}*/
//
//	}
//
//
//	if(bDelayFlag == TRUE)
//	{
//		iDelayCount++;
//		if (iDelayCount > 3)
//		{
//			bDelayFlag = FALSE;
//		}
//		return TRUE;
//	}
//
//	int iTempID;
//
//	for(i = 0; i < NUM_BATT; i++)
//	{
//		if(i < 42)
//		{
//			pEquip = Init_GetEquipRefById(pSite, 116 + i);
//			
//
//
//		}
//		else
//		{
//			pEquip = Init_GetEquipRefById(pSite, 164 + i);
//			
//		}
//
//
//		if(pSigBattTempSig[i][2]->varValue.enumValue == 0)
//		{
//			
//
//			if(pSigBattTempSig[i][1]->varValue.enumValue == 0)
//			{	
//				if(SIG_VALUE_IS_VALID(pSigBattTempSig[i][0]))
//				{
//					SIG_STATE_CLR(pSigBattTempSig[i][0], VALUE_STATE_IS_VALID);
//
//				}
//
//				if(SIG_VALUE_IS_CONFIGURED(pSigBattTempSig[i][0]))
//				{
//					SIG_STATE_CLR(pSigBattTempSig[i][0], VALUE_STATE_IS_CONFIGURED);
//
//				}
//				
//			}
//			else if(pSigBattTempSig[i][1]->varValue.enumValue > 0 && pSigBattTempSig[i][1]->varValue.enumValue < 8)
//			{	
//				iTempID = pSigBattTempSig[i][1]->varValue.enumValue;
//				if(SIG_VALUE_IS_VALID(pSigSysTempSig[iTempID - 1]) && SIG_VALUE_IS_CONFIGURED(pSigSysTempSig[iTempID - 1]))
//				{	
//					SIG_STATE_SET(pSigBattTempSig[i][0], VALUE_STATE_IS_VALID);
//					SIG_STATE_SET(pSigBattTempSig[i][0], VALUE_STATE_IS_CONFIGURED);
//					setValueTemp.fValue = pSigSysTempSig[iTempID - 1]->varValue.fValue;
//					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBattTempSig[i][0], &setValueTemp, tmNow, FALSE);
//
//				}
//				else
//				{	
//					SIG_STATE_CLR(pSigBattTempSig[i][0], VALUE_STATE_IS_VALID);
//				}
//				 
//
//			}
//			/*else if(pSigBattTempSig[i][1]->varValue.enumValue >=8 && pSigBattTempSig[i][1]->varValue.enumValue < 11)
//			{
//				iTempID = pSigBattTempSig[i][1]->varValue.enumValue;
//				if(SIG_VALUE_IS_VALID(pSigLargeDUTempSig[iTempID - 7 - 1]) && SIG_VALUE_IS_CONFIGURED(pSigSysTempSig[iTempID - 7 - 1]))
//				{
//					SIG_STATE_SET(pSigBattTempSig[i][0], VALUE_STATE_IS_VALID);
//					SIG_STATE_SET(pSigBattTempSig[i][0], VALUE_STATE_IS_CONFIGURED);
//					setValueTemp.fValue = pSigLargeDUTempSig[iTempID - 7 - 1]->varValue.fValue;
//					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBattTempSig[i][0], &setValueTemp, tmNow, FALSE);
//
//				}
//				else
//				{
//					SIG_STATE_CLR(pSigBattTempSig[i][0], VALUE_STATE_IS_VALID);
//				}
//
//			}*/
//		}
//		else
//		{
//			SIG_STATE_CLR(pSigBattTempSig[i][0], VALUE_STATE_IS_VALID);
//			SIG_STATE_CLR(pSigBattTempSig[i][0], VALUE_STATE_IS_CONFIGURED);
//		}
//
//
//	}
//
//
//	pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
//
//	if(pSigEnvTempSig[1]->varValue.enumValue == 0)
//	{
//
//		SIG_STATE_CLR(pSigEnvTempSig[0], VALUE_STATE_IS_VALID);
//		SIG_STATE_CLR(pSigEnvTempSig[0], VALUE_STATE_IS_CONFIGURED);
//	}
//	else
//	{	
//		iTempID = pSigEnvTempSig[1]->varValue.enumValue;
//		if(SIG_VALUE_IS_VALID(pSigSysTempSig[iTempID - 1]) && SIG_VALUE_IS_CONFIGURED(pSigSysTempSig[iTempID - 1]))
//		{
//			SIG_STATE_SET(pSigEnvTempSig[0], VALUE_STATE_IS_VALID);
//			SIG_STATE_SET(pSigEnvTempSig[0], VALUE_STATE_IS_CONFIGURED);
//			setValueTemp.fValue = pSigSysTempSig[iTempID - 1]->varValue.fValue;
//			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigEnvTempSig[0], &setValueTemp, tmNow, FALSE);
//
//		}
//		else
//		{
//			SIG_STATE_CLR(pSigEnvTempSig[0], VALUE_STATE_IS_VALID);
//		}
//
//
//	}
//
//	pEquip = Init_GetEquipRefById(pSite, BATTERY_GROUP_UNIT);
//
//	float fMaxTemp = -9999.0;
//
//	float fAllTemp = 0;
//
//	float fAllSMBRCTemp = 0;
//
//
//	if(pSigBattGroupTempSig[1]->varValue.enumValue == 0)
//	{
//
//		SIG_STATE_CLR(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//		SIG_STATE_CLR(pSigBattGroupTempSig[0], VALUE_STATE_IS_CONFIGURED);
//	}
//	else if(pSigBattGroupTempSig[1]->varValue.enumValue > 0 && pSigBattGroupTempSig[1]->varValue.enumValue < 8)
//	{
//		iTempID = pSigBattGroupTempSig[1]->varValue.enumValue;
//		if(SIG_VALUE_IS_VALID(pSigSysTempSig[iTempID - 1]) && SIG_VALUE_IS_CONFIGURED(pSigSysTempSig[iTempID - 1]))
//		{
//
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_CONFIGURED);
//			setValueTemp.fValue = pSigSysTempSig[iTempID - 1]->varValue.fValue;
//			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBattGroupTempSig[0], &setValueTemp, tmNow, FALSE);
//
//		}
//		else
//		{
//
//			SIG_STATE_CLR(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//		}
//
//	}
//	/*else if(pSigBattGroupTempSig[1]->varValue.enumValue >= 8 && pSigBattGroupTempSig[1]->varValue.enumValue < 11)
//	{
//		iTempID = pSigBattGroupTempSig[1]->varValue.enumValue;
//		if(SIG_VALUE_IS_VALID(pSigLargeDUTempSig[iTempID - 7 - 1]) && SIG_VALUE_IS_CONFIGURED(pSigLargeDUTempSig[iTempID - 7 - 1]))
//		{
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_CONFIGURED);
//			setValueTemp.fValue = pSigLargeDUTempSig[iTempID - 7 - 1]->varValue.fValue;
//			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBattGroupTempSig[0], &setValueTemp, tmNow, FALSE);
//
//		}
//		else
//		{
//			SIG_STATE_CLR(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//		}
//
//	}*/
//	else if(pSigBattGroupTempSig[1]->varValue.enumValue == 8)//MAX
//	{
//		for(i = 0;i < NUM_TEMP;i++)
//		{
//			if(SIG_VALUE_IS_VALID(pSigSysTempSig[i]) && SIG_VALUE_IS_CONFIGURED(pSigSysTempSig[i]))
//			{
//				if(pSigSysTempSig[i]->varValue.fValue > fMaxTemp && pSigSysTempSig[i]->varValue.fValue > -140 && pSigSysTempSig[i]->varValue.fValue < 150)
//				{
//					fMaxTemp = pSigSysTempSig[i]->varValue.fValue;
//
//				}
//				
//			}
//
//		}
//
//		if(fMaxTemp != -9999.0)
//		{
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_CONFIGURED);
//			setValueTemp.fValue = fMaxTemp;
//			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBattGroupTempSig[0], &setValueTemp, tmNow, FALSE);
//
//		}
//		else
//		{
//			SIG_STATE_CLR(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//		}
//
//	}
//	else if(pSigBattGroupTempSig[1]->varValue.enumValue == 9)//AVERAGE
//	{
//		j = 0;
//		for(i = 0;i < NUM_TEMP;i++)
//		{
//			if(SIG_VALUE_IS_VALID(pSigSysTempSig[i]) && SIG_VALUE_IS_CONFIGURED(pSigSysTempSig[i]))
//			{
//				if(pSigSysTempSig[i]->varValue.fValue > fMaxTemp && pSigSysTempSig[i]->varValue.fValue > -140 && pSigSysTempSig[i]->varValue.fValue < 150)
//				{
//					fAllTemp = fAllTemp + pSigSysTempSig[i]->varValue.fValue;
//					j++;
//
//				}
//
//			}
//
//		}
//
//		if(j > 0)
//		{
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_CONFIGURED);
//			setValueTemp.fValue = (fAllTemp/j);
//			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBattGroupTempSig[0], &setValueTemp, tmNow, FALSE);
//
//		}
//		else
//		{
//			SIG_STATE_CLR(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//		}
//
//
//	}
//	else if(pSigBattGroupTempSig[1]->varValue.enumValue == 10)//SMBRC AVERAGE
//	{
//		j = 0;
//		for(i = 0;i < NUM_SMBRC_TEMP_SIG;i++)
//		{
//			if(SIG_VALUE_IS_VALID(pSigSMBRCTempSig[i]) && SIG_VALUE_IS_CONFIGURED(pSigSMBRCTempSig[i]))
//			{
//				if(pSigSMBRCTempSig[i]->varValue.fValue > fMaxTemp && pSigSMBRCTempSig[i]->varValue.fValue > -140 && pSigSMBRCTempSig[i]->varValue.fValue < 150)
//				{
//					fAllSMBRCTemp = fAllSMBRCTemp + pSigSMBRCTempSig[i]->varValue.fValue;
//					j++;
//
//				}
//
//			}
//
//		}
//
//		if(j > 0)
//		{
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//			SIG_STATE_SET(pSigBattGroupTempSig[0], VALUE_STATE_IS_CONFIGURED);
//			setValueTemp.fValue = (fAllSMBRCTemp/j);
//			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBattGroupTempSig[0], &setValueTemp, tmNow, FALSE);
//
//		}
//		else
//		{
//			SIG_STATE_CLR(pSigBattGroupTempSig[0], VALUE_STATE_IS_VALID);
//		}
//	}
//
//	return TRUE;
//
//}

/////////   The Following are added for CR new temp strategy      ///////////
//-----------BINGIN INTRODUCED OF MODIFYING by Jimmy Wu----------------------
//* 
//*  DATE          : 2011-11-30 19:22:29
//*  CHANGE SOURCE : 
//*  REMARK        :  For CR <Tmp Strategy>
static int g_Sensor_Settings[MAX_TEMP_SENSORS3];
/*********************************************************************************
*  
*  FUNCTION NAME : GetAndJudge_AllSensorSettings
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-11-30 19:07:05
*  DESCRIPTION   : Get all the 71 sensors assigning signal and tell if each is valid

*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void GetAndJudge_AllSensorSettings(IN SITE_INFO *pSite)
{

#define SYSTEM_SENSOR_NUM	7
#define SYSTEM_SENSOR_NUM2	11
#define EACH_SMTEMP_SENSOR_NUM	8
#define SMTEMP_NUM		8


#define	SMDUE_NUM		2
#define	EACH_SMDUE_SENSOR_NUM	10

//#define SENSOR_SET_NUM		3 // each sensor has got 3 set signals

#define EQUIP_SYSTEM_UNIT	1
#define EQUIP_SMTEMP_UNIT	709
#define  EQUIP_SMDUE_UNIT		1801

#define EQUIP_COMMINTERRUP_ID	99
#define EQUIP_EXISTENCE_ID	100
//#define EQUIP_OB_UNIT		103
#define EQUIP_EIB_UNIT		197

//#define SYSTEM_SIG_SENSOR_SET_START_ID	151	//the 7 syetem sensor setting IDs are from 151
#define SMTEMP_SIG_SENSOR_SET_START_ID	1	//each SMTEMP, the 8 sensor setting IDs are from 1
#define SMDUE_SIG_SENSOR_SET_START_ID	71	//each SMDUE, the 10 sensor setting IDs are from 71

	static int iIndex_Sys_StartTempSet_ID[SYSTEM_SENSOR_NUM2] =
	{
		50,51,52,53,54,78,79,55,56,80,81
	};


	static SIG_BASIC_VALUE *pSigSMTempSensorSig[SMTEMP_NUM][EACH_SMTEMP_SENSOR_NUM] = { {NULL} };	
	static SIG_BASIC_VALUE *pSigSysSensorSig[SYSTEM_SENSOR_NUM2] = { NULL };

	//Add by raoliang 2019/03/08 for smdue 10 temp
	static SIG_BASIC_VALUE *pSigSMDUETempSensorSig[SMDUE_NUM][EACH_SMDUE_SENSOR_NUM] = { {NULL} };
	static SIG_BASIC_VALUE *pSigSMDUETempExistSig[SMDUE_NUM] = { NULL };
	static SIG_BASIC_VALUE *pSigSMDUETempInterruptSig[SMDUE_NUM] = { NULL };

	static SIG_BASIC_VALUE *pSigSMTempExistSig[SMTEMP_NUM] = { NULL };
	static SIG_BASIC_VALUE *pSigSMTempInterruptSig[SMTEMP_NUM] = { NULL };

	//SIG_BASIC_VALUE *pSigXBExistSig[3] = {NULL}; //OB/IB/EIB Board exist flag
	//static SIG_BASIC_VALUE *pSigSMTempExistSig[SMTEMP_NUM] = {NULL};

	EQUIP_INFO	*pEquip = NULL;
	static BOOL bIni = FALSE; 
	BOOL bEquipOK = FALSE;
	int i,k;
	if(!bIni)
	{
		bIni = TRUE;
		for(k = 0; k < SMTEMP_NUM; k++)
		{
			pEquip = Init_GetEquipRefById(pSite, EQUIP_SMTEMP_UNIT + k);

			if (pEquip)   //condition added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
			{
				pSigSMTempExistSig[k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,EQUIP_EXISTENCE_ID);
				pSigSMTempInterruptSig[k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,EQUIP_COMMINTERRUP_ID);
			}
		}

		//Add by raoliang 2019/03/08 for smdue 10 temp
		for(k = 0; k < SMDUE_NUM; k++)
		{
			pEquip = Init_GetEquipRefById(pSite, EQUIP_SMDUE_UNIT + k);

			if (pEquip)   
			{
				pSigSMDUETempExistSig[k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,EQUIP_EXISTENCE_ID);
				pSigSMDUETempInterruptSig[k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,EQUIP_COMMINTERRUP_ID);
			}
		}

		pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
		for (i = 0; i < SYSTEM_SENSOR_NUM2; i++)
		{
			pSigSysSensorSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,iIndex_Sys_StartTempSet_ID[i]);
		}
		for(k = 0; k < SMTEMP_NUM; k++)
		{
			pEquip = Init_GetEquipRefById(pSite, EQUIP_SMTEMP_UNIT + k);

			if (pEquip)   //condition added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
			{
				for (i = 0; i < EACH_SMTEMP_SENSOR_NUM; i++)
				{
					pSigSMTempSensorSig[k][i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,SMTEMP_SIG_SENSOR_SET_START_ID + i);
				}
			}
		}

		//Add by raoliang 2019/03/08 for smdue 10 temp
		 for(k = 0; k < SMDUE_NUM; k++)
		{
			pEquip = Init_GetEquipRefById(pSite, EQUIP_SMDUE_UNIT + k);

			if (pEquip)   //condition added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
			{
				for (i = 0; i < EACH_SMDUE_SENSOR_NUM; i++)
				{
					pSigSMDUETempSensorSig[k][i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,SMDUE_SIG_SENSOR_SET_START_ID + i);
				}
			}
		}
	}
	// Step 1: get rough settings from system and sm temp
	//pEquip = Init_GetEquipRefById(pSite, EQUIP_OB_UNIT); 
	//pSigXBExistSig[0] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,99);

	//pEquip = Init_GetEquipRefById(pSite, EQUIP_EIB_UNIT);
	//pSigXBExistSig[1] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,99);

	//pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
	//pSigXBExistSig[2] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,99);


	pEquip = Init_GetEquipRefById(pSite, SYSTEM_UNIT);
	for (i = 0; i < SYSTEM_SENSOR_NUM2; i++)
	{
		if(i < SYSTEM_SENSOR_NUM)
		{
			g_Sensor_Settings[i] = pSigSysSensorSig[i]->varValue.enumValue;
		}
		else
		{
			g_Sensor_Settings[i + MAX_TEMP_SENSORS - MAX_TEMP_SYS_SENSORS] = pSigSysSensorSig[i]->varValue.enumValue;
		}
		//if(g_Sensor_Settings[i] < SENSOR_SET_NONE || g_Sensor_Settings[i] > SENSOR_SET_BATTGROUP)
		//{
		//	g_Sensor_Settings[i] = SENSOR_SET_ERR;
		//}

	}
	

	for(k = 0; k < SMTEMP_NUM; k++)
	{
		bEquipOK = FALSE;

		if (pSigSMTempExistSig[k] &&	//condition added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
				pSigSMTempExistSig[k]->varValue.enumValue < 1 && pSigSMTempInterruptSig[k]->varValue.enumValue < 1)
		{
			bEquipOK = TRUE;
		}

		for (i = 0; i < EACH_SMTEMP_SENSOR_NUM; i++)
		{

			g_Sensor_Settings[MAX_TEMP_SYS_SENSORS + k*8+i] = bEquipOK? (pSigSMTempSensorSig[k][i])->varValue.enumValue : SENSOR_SET_NONE;
			//if(g_Sensor_Settings[7 + k*8+i] < SENSOR_SET_NONE || g_Sensor_Settings[7 + k*8+i] > SENSOR_SET_BATTGROUP)
			//{
			//	g_Sensor_Settings[7 + k*8+i] = SENSOR_SET_ERR;
			//}
			//printf("\n*******SM Temp: %d Exist: %d Settings: %d",k+1,bEquipExisted,g_Sensor_Settings[7 + k*8+i]);
		}
	}

	//Add by raoliang 2019/03/08 for smdue 10 temp
	for(k = 0; k < SMDUE_NUM; k++)
	{
		bEquipOK = FALSE;

		if (pSigSMDUETempExistSig[k] &&	//condition added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
				pSigSMDUETempExistSig[k]->varValue.enumValue < 1 && pSigSMDUETempInterruptSig[k]->varValue.enumValue < 1)
		{
			bEquipOK = TRUE;
		}

		for (i = 0; i < EACH_SMDUE_SENSOR_NUM; i++)
		{
			g_Sensor_Settings[MAX_TEMP_SENSORS2 + k*EACH_SMDUE_SENSOR_NUM+i] = bEquipOK? (pSigSMDUETempSensorSig[k][i])->varValue.enumValue : SENSOR_SET_NONE;
			//printf("Senor [%d]  Set as [%d]  \n",MAX_TEMP_SENSORS2 + k*EACH_SMDUE_SENSOR_NUM+i,(pSigSMDUETempSensorSig[k][i])->varValue.enumValue);
		}      
	}


}

/*********************************************************************************
*  
*  FUNCTION NAME : GetTempValue_FromASensor
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-12-12 10:48:25
*  DESCRIPTION   : Get the raw value of a sensor from its device
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/

static BOOL IsATempSensorExist(int iIdx,IN SITE_INFO *pSite)
{
#define EQUIP_SYSTEM_UNIT	1
#define EQUIP_EIB_UNIT		197
#define EQUIP_SMTEMP_UNIT	709

#define SIGID_SYS_IB_NUM	78
#define SIGID_SYS_IB_TYPE	79
#define SIGID_EQUIP_EXIST	100

	EQUIP_INFO* pEquip;
	SIG_BASIC_VALUE *pTempValue1 = NULL;
	SIG_BASIC_VALUE *pTempValue2 = NULL;

	if(iIdx >= 0 && iIdx < 3)//Mainboard and OB temperature
	{
		return TRUE;
	}
	else if(iIdx >= 3 && iIdx < 5)//IB2-1 temperature
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_SYSTEM_UNIT);
		pTempValue1 = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SYS_IB_NUM);
		pTempValue2 = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SYS_IB_TYPE);
		if(pTempValue1->varValue.ulValue > 0 && pTempValue2->varValue.ulValue == 1)
		{
			return TRUE;
		}
	}
	else if(iIdx >= 5 && iIdx < 7)//EIB-1 temperature
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_EIB_UNIT);
		pTempValue1 = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_EQUIP_EXIST);
		if(pTempValue1->varValue.enumValue == 0)
		{
			return TRUE;
		}
	}
	else if(iIdx >= 7 && iIdx < 71)//SMTEMP temperaure
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_SMTEMP_UNIT + (iIdx-7) / 8);
		pTempValue1 = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_EQUIP_EXIST);
		if(pTempValue1->varValue.enumValue == 0)
		{
			return TRUE;
		}
	}
	else if(iIdx >= 71 && iIdx < 73)//IB2-2 temperature
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_SYSTEM_UNIT);
		pTempValue1 = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SYS_IB_NUM);
		pTempValue2 = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SYS_IB_TYPE);
		if(pTempValue1->varValue.ulValue > 1 && pTempValue2->varValue.ulValue == 1)
		{
			return TRUE;
		}
	}
	else if(iIdx >= 73 && iIdx < 75)//EIB-2 temperature
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_EIB_UNIT +1);
		pTempValue1 = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_EQUIP_EXIST);
		if(pTempValue1->varValue.enumValue == 0)
		{
			return TRUE;
		}
	}
	else
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_SMDUE_UNIT +  (iIdx-75)/10);
		pTempValue1 = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_EQUIP_EXIST);
		if(pTempValue1->varValue.enumValue == 0)
		{
			return TRUE;
		}	
	}
	return FALSE;
}
static void *GetTempValue_FromASensor(int iIdx,IN SITE_INFO *pSite)
{
#define SYSTEM_SENSOR_NUM	7
#define SYSTEM_SENSOR_NUM2	11
#define EACH_SMTEMP_SENSOR_NUM	8
#define SMTEMP_NUM		8

#define SENSOR_SET_NUM		3 // each sensor has got 3 set signals

#define EQUIP_SYSTEM_UNIT	1
#define EQUIP_SMTEMP_UNIT	709
#define EQUIP_SMDUE_UNIT		1801

//#define SIGID_SYSTEMP_START	43
#define SIGID_SMTEMP_START	1
#define SIGID_SMDUE_START	31
	static int iIndex_Sys_StartTempSensor_ID[SYSTEM_SENSOR_NUM2] =
	{
		43,44,45,46,47,52,51,220,221,222,223,
	};

	SIG_BASIC_VALUE *pTempValue = NULL;
	float fTemp = 0.0;
	void *pValue = NULL;
	EQUIP_INFO* pEquip;	

	if(iIdx >= 0 && iIdx < 7)
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_SYSTEM_UNIT);
		pValue = Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,iIndex_Sys_StartTempSensor_ID[iIdx]);
	}
	else if(iIdx >= 71 && iIdx < 75)
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_SYSTEM_UNIT);
		pValue = Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,iIndex_Sys_StartTempSensor_ID[iIdx - 71 + 7]);
	}
	else if(iIdx >= 7 && iIdx < 71)
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_SMTEMP_UNIT + (iIdx-7) / 8);
		pValue = Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SMTEMP_START + (iIdx-7) % 8);		
	}
	else
	{
		pEquip = Init_GetEquipRefById(pSite, EQUIP_SMDUE_UNIT+(iIdx-75) / 10);
		pValue = Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SMDUE_START+(iIdx -75)%10);	
	}
	if(pValue != NULL)
	{
		pTempValue = (SIG_BASIC_VALUE*)pValue;
		fTemp = pTempValue->varValue.fValue;
		if(fTemp < -140.0 || fTemp > 150.0) //beyond the value range, so ignore it,�˴�Ϊ̽ͷFault�澯��Χ��fault�¶Ȳ����ϴ�
		{
			return NULL;
		}
	}
	

	return pValue;
}
static void Refresh_Temp_SNMP_AlmStatus(IN SITE_INFO* pSite)
{
	int i, k;
#define SIGID_SYS_TEMP_ALMSTAT_START	207
#define SIGID_SMTEMP_TEMP_ALMSTAT_START	21
#define SIGID_SMDUE_TEMP_ALMSTAT_START	81
#define SIG_ID_AMBIENT_ALAM_START	201
#define SIG_ID_AMBIENT_ALAM_START2	458
#define SIG_ID_AMBIENT_ALAM_START3	601

#define SIG_ID_BATTERY_ALAM_START	51
#define SIG_ID_BATTERY_ALAM_START2	280
#define SIG_ID_BATTERY_ALAM_START3	301

	static SIG_BASIC_VALUE *pAllTempSigAlmStat[NUM_OF_TEMPS];

	/*  �����¶� �и澯 ȡ���������¶ȸ澯+  1~7  71 ~75    δʹ�ø澯
	��� �¶� ��ȡ���������¶ȸ澯*/
	static SIG_BASIC_VALUE *pTwoTempSigAlmStat[2];
	static SIG_BASIC_VALUE *pAllAmbientAlms[3][NUM_OF_TEMPS];
	static SIG_BASIC_VALUE *pAllBatteryAlms[3][NUM_OF_TEMPS];

	static SIG_BASIC_VALUE *pSevenAmbientAlms[2][11];

	static BOOL bIni = FALSE;
	int		iSigIdSysAlarm[22] ={18,19,20,21,22,42,43,450,451,452,453,
		9,25,28,31,34,48,49,454,455,456,457};
	EQUIP_INFO* pEquip;	
	if(!bIni)
	{
		bIni = TRUE;
		pEquip = Init_GetEquipRefById(pSite, 1);
		pTwoTempSigAlmStat[0] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,215);
	
		for (i = 0; i < MAX_TEMP_SYS_SENSORS; i++)
		{
			pAllTempSigAlmStat[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SYS_TEMP_ALMSTAT_START + i);
		}
		for(i = MAX_TEMP_SENSORS; i < MAX_TEMP_SENSORS2; i++)
		{
			pAllTempSigAlmStat[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,230 + i - MAX_TEMP_SENSORS);
		}
		

		pEquip = Init_GetEquipRefById(pSite, 115);
		pTwoTempSigAlmStat[1] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,186);

		//SMTEMP
		for(k = 0; k < 8; k++)
		{
			pEquip = Init_GetEquipRefById(pSite, 709 + k);
			for (i = 0; i < 8; i++)
			{
				pAllTempSigAlmStat[MAX_TEMP_SYS_SENSORS+ k * 8 + i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SMTEMP_TEMP_ALMSTAT_START + i);
			}
		}
		 //for SMDUE  1,2  10temp alarm status 
		for(k = 0; k < 2; k++)
		{
			pEquip = Init_GetEquipRefById(pSite, 1801 + k);
			for (i = 0; i < 10; i++)
			{
				pAllTempSigAlmStat[MAX_TEMP_SENSORS2 + k * 10 + i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SMDUE_TEMP_ALMSTAT_START + i);
			}
		}
		

		//added by Jimmy 20140627
		pEquip = Init_GetEquipRefById(pSite, 1);
		for(i = 0; i < 3; i++)
		{
			for(k = 0; k < NUM_OF_TEMPS; k++)
			{
				if(k>=MAX_TEMP_SENSORS2)     //75 ~94
				{
					pAllAmbientAlms[i][k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,
						SIG_ID_AMBIENT_ALAM_START3 + 3*k + i-3*MAX_TEMP_SENSORS2);
				}
				else if(k < MAX_TEMP_SENSORS )    //1~70 
				{
					pAllAmbientAlms[i][k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,
						SIG_ID_AMBIENT_ALAM_START + 3*k + i);
				}
				else		    //72~74
				{
					pAllAmbientAlms[i][k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,
						SIG_ID_AMBIENT_ALAM_START2 + 3*k + i - 3*71);
				}
			}
		}
		for(i = 0; i < 2; i++)
		{
			for(k = 0; k < 11; k++)
			{
				pSevenAmbientAlms[i][k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,iSigIdSysAlarm[i*2 +k]);
			}
		}

		pEquip = Init_GetEquipRefById(pSite, 115);
		for(i = 0; i < 3; i++)
		{
			for(k = 0; k < NUM_OF_TEMPS; k++)
			{
				
				if(k >=MAX_TEMP_SENSORS2)
				{
					pAllBatteryAlms[i][k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,
						SIG_ID_BATTERY_ALAM_START3 + 3*k + i-3*MAX_TEMP_SENSORS2);	
				}
				else if(k < MAX_TEMP_SENSORS )
				{
					pAllBatteryAlms[i][k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,
						SIG_ID_BATTERY_ALAM_START + 3*k + i);
				}
				else			   //71 ~74
				{
					pAllBatteryAlms[i][k] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,
						SIG_ID_BATTERY_ALAM_START2 + 3*k + i - 3*71);
				}
			}
		}
	}

	time_t tmNow = time(NULL);
	SIG_BASIC_VALUE* pVal = NULL;
	VAR_VALUE var_Temp;
	for (i = 0; i < NUM_OF_TEMPS; i++)
	{
		if(i < MAX_TEMP_SYS_SENSORS || (i >=MAX_TEMP_SENSORS && i<MAX_TEMP_SENSORS2))
		{
			pEquip = Init_GetEquipRefById(pSite, 1);
		}
		else if(i>=MAX_TEMP_SYS_SENSORS && i<MAX_TEMP_SENSORS)
		{
			pEquip = Init_GetEquipRefById(pSite, 709 + (int)((i - 7) / 8));
		}
		else 
		{
			pEquip = Init_GetEquipRefById(pSite, 1801 + (int)((i - 75) / 10));	
		}

		pVal = (SIG_BASIC_VALUE *)GetTempValue_FromASensor(i,pSite);
		if(pVal == NULL)
		{
			var_Temp.ulValue = 2;//fail
			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSigAlmStat[i], &var_Temp, tmNow, FALSE);
		}
		else if(!SIG_VALUE_IS_VALID(pVal))
		{
			var_Temp.ulValue = 2;//fail
			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSigAlmStat[i], &var_Temp, tmNow, FALSE);
		}
		else
		{

			if(pAllAmbientAlms[0][i]->varValue.enumValue || pAllAmbientAlms[1][i]->varValue.enumValue ||
				pAllBatteryAlms[0][i]->varValue.enumValue || pAllBatteryAlms[1][i]->varValue.enumValue)
			{
				var_Temp.ulValue = 0;//high
			}
			else if(pAllAmbientAlms[2][i]->varValue.enumValue || pAllBatteryAlms[2][i]->varValue.enumValue )
			{
				var_Temp.ulValue = 1;//low
			}
			else
			{
				var_Temp.ulValue = 3;//normal/none
			}
			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSigAlmStat[i], &var_Temp, tmNow, FALSE);
		}

	}


	//11 ·ϵͳ  �¶� 1~7  71 ~75
	var_Temp.ulValue = 0;
	for (i = 0; i < MAX_TEMP_SENSORS3; i++)
	{
		if(i < MAX_TEMP_SYS_SENSORS || ( i>=MAX_TEMP_SENSORS&& i<MAX_TEMP_SENSORS2))
		{
			pEquip = Init_GetEquipRefById(pSite, 1);
		}
		else	if(     i>=	 MAX_TEMP_SYS_SENSORS && i<   MAX_TEMP_SENSORS)
		{
			pEquip = Init_GetEquipRefById(pSite, 709 + (int)((i - MAX_TEMP_SYS_SENSORS) / 8));
		}
		else		    //75 ~94 
		{
			pEquip = Init_GetEquipRefById(pSite, 1801 + (int)((i - MAX_TEMP_SENSORS2) / 10));
		}

		
		if(i < MAX_TEMP_SYS_SENSORS)
		{
			if(pSevenAmbientAlms[0][i]->varValue.enumValue || pSevenAmbientAlms[1][i]->varValue.enumValue)
			{
				var_Temp.ulValue = 1;//high					
				break;
			}
		}
		else if(i >= MAX_TEMP_SENSORS && i<MAX_TEMP_SENSORS2)
		{
			if(pSevenAmbientAlms[0][i - 71 + 7]->varValue.enumValue || pSevenAmbientAlms[1][i - 71 + 7]->varValue.enumValue)
			{
				var_Temp.ulValue = 1;//high					
				break;
			}
		}
		else
		{
			
		}


		if(pAllAmbientAlms[0][i]->varValue.enumValue || pAllAmbientAlms[1][i]->varValue.enumValue
			||pAllAmbientAlms[2][i]->varValue.enumValue)
		{
			var_Temp.ulValue = 1;//high				
			break;
		}									
		
	}
	pEquip = Init_GetEquipRefById(pSite, 1);
	Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pTwoTempSigAlmStat[0], &var_Temp, tmNow, FALSE);

	var_Temp.ulValue = 0;
	for (i = 0; i < NUM_OF_TEMPS; i++)
	{
		if(i < MAX_TEMP_SYS_SENSORS || (i >= MAX_TEMP_SENSORS && i<MAX_TEMP_SENSORS2))
		{
			pEquip = Init_GetEquipRefById(pSite, 1);
		}
		else	if(i>=MAX_TEMP_SYS_SENSORS && i<MAX_TEMP_SENSORS)
		{
			pEquip = Init_GetEquipRefById(pSite, 709 + (int)((i - MAX_TEMP_SYS_SENSORS) / 8));
		}
		else
		{
			pEquip = Init_GetEquipRefById(pSite, 1801 + (int)((i - MAX_TEMP_SENSORS2) / 10));	
		}
		
		if(pAllBatteryAlms[0][i]->varValue.enumValue || pAllBatteryAlms[1][i]->varValue.enumValue 
			||  pAllBatteryAlms[2][i]->varValue.enumValue)
		{
			var_Temp.ulValue = 1;//high
			break;
		}						

	}
	pEquip = Init_GetEquipRefById(pSite, 115);
	Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pTwoTempSigAlmStat[1], &var_Temp, tmNow, FALSE);

}
/*********************************************************************************
*  
*  FUNCTION NAME : Refresh_AllTemps
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-11-30 19:11:48
*  DESCRIPTION   : Refresh all temp values in battery/ambient, from the values of 
sensor devices. 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Refresh_AllTemps(IN SITE_INFO *pSite)
{
#define INVALID_TEMPVALUE		-273.0
#define TEMP_NUM_EACH_EQUIP	8

#define SIGID_SYSTEMP_START		111
#define SIGID_BATTGRPTEMP_START		111
#define SIGID_SYSTEMP_START2		224
#define SIGID_SYSTEMP_START3		301
#define SIGID_BATTGRPTEMP_START2		224
#define	 SIGID_BATTGRPTEMP_START3		301
	int i,j;
	EQUIP_INFO* pEquip;	

	time_t tmNow = time(NULL);
	
	SIG_BASIC_VALUE *pTempValue = NULL;
	
	VAR_VALUE var_Temp;

	static SIG_BASIC_VALUE *pAllTempSig[2][MAX_TEMP_SENSORS3];
	static BOOL bIni = FALSE;
	SAMPLE_SIG_VALUE *pSampleValue = NULL;
	EXP_CONST			fResult;
	EXP_ELEMENT			result = {EXP_ETYPE_CONST, &fResult};

	SIG_BASIC_VALUE *pSignalVal=NULL;

	if(!bIni)
	{
		bIni = TRUE;
		//System Ambient
		pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
		for(j = 0; j < MAX_TEMP_SENSORS3; j++)
		{
			if(j < MAX_TEMP_SENSORS)
			{
				pAllTempSig[0][j] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SYSTEMP_START + j);
			}
			else if(j>=MAX_TEMP_SENSORS && j<MAX_TEMP_SENSORS2)
			{
				pAllTempSig[0][j] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SYSTEMP_START2 + j - MAX_TEMP_SENSORS);
			}
			else
			{
				pAllTempSig[0][j] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_SYSTEMP_START3 + j - MAX_TEMP_SENSORS2);
			}
			SIG_STATE_CLR(pAllTempSig[0][j], VALUE_STATE_IS_VALID);
			SIG_STATE_CLR(pAllTempSig[0][j], VALUE_STATE_NEED_DISPLAY);
			
		}
		//Batt Group
		pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
		for(j = 0; j < MAX_TEMP_SENSORS3; j++)
		{
			if(j < MAX_TEMP_SENSORS)
			{
				pAllTempSig[1][j] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_BATTGRPTEMP_START + j);
			}
			else if(j>=MAX_TEMP_SENSORS && j<MAX_TEMP_SENSORS2)
			{
				pAllTempSig[1][j] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_BATTGRPTEMP_START2 + j - MAX_TEMP_SENSORS);
			}
			else
			{
				pAllTempSig[1][j] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIGID_BATTGRPTEMP_START3 + j - MAX_TEMP_SENSORS2);
			}
			SIG_STATE_CLR(pAllTempSig[1][j], VALUE_STATE_IS_VALID);
			SIG_STATE_CLR(pAllTempSig[1][j], VALUE_STATE_NEED_DISPLAY);
		}
		return;

	}
 
	for(i = 0;i < MAX_TEMP_SENSORS3;i++)
	{
		/*
		var_Temp.fValue = INVALID_TEMPVALUE;
		pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
		Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[0][i], &var_Temp, tmNow, FALSE);
		pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
		Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[1][i], &var_Temp, tmNow, FALSE);
		
		SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_IS_VALID);
		SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_NEED_DISPLAY);
		SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_IS_VALID);
		SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_NEED_DISPLAY);
		*/
		switch(g_Sensor_Settings[i])
		{
		case SENSOR_SET_AMBIENT:

			pTempValue = (SIG_BASIC_VALUE *)GetTempValue_FromASensor(i,pSite);

			//printf("Ambient  [%d]Value from [%f]  \n",i,pTempValue->varValue.fValue);

			if(IsATempSensorExist(i,pSite))
			{
				SIG_STATE_SET(pAllTempSig[0][i], VALUE_STATE_NEED_DISPLAY);
			}
			else
			{
				SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_NEED_DISPLAY);
			}


			if(NULL != pTempValue) // to test if it is valid
			{
				if(SIG_STATE_TEST(pTempValue, VALUE_STATE_IS_VALID_RAW) && SIG_STATE_TEST(pTempValue, VALUE_STATE_NEED_DISPLAY))
				{	
					pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
					var_Temp.fValue = pTempValue->varValue.fValue;
					if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[0][i], &var_Temp, tmNow, FALSE))
					{
						SIG_STATE_SET(pAllTempSig[0][i], VALUE_STATE_IS_VALID);	

						pSignalVal = pAllTempSig[0][i];

						if (((SET_SIG_VALUE *)pSignalVal)->sv.iSamplerChannel >= 0)
						{
							var_Temp.fValue = INVALID_TEMPVALUE;
							pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
							Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[0][i], &var_Temp, tmNow, FALSE);			
						}
					}
					else
					{
						pSignalVal = pAllTempSig[0][i];
						
						if (!pEquip->bCommStatus && (pSignalVal->ucSigType == SIG_TYPE_SAMPLING) &&
							(pSignalVal != (SIG_BASIC_VALUE *)pEquip->pCommStatusSigRef) &&
							(pSignalVal != (SIG_BASIC_VALUE *)pEquip->pWorkStatusSigRef))
						{
							var_Temp.fValue = INVALID_TEMPVALUE;
							pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
							Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[0][i], &var_Temp, tmNow, FALSE);			
							SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_IS_VALID);	
						}
					}
				}
				else
				{
					var_Temp.fValue = INVALID_TEMPVALUE;
					pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[0][i], &var_Temp, tmNow, FALSE);	
					SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_IS_VALID);
				}
			}
			else
			{
				var_Temp.fValue = INVALID_TEMPVALUE;
				pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
				Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[0][i], &var_Temp, tmNow, FALSE);	
				SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_IS_VALID);		
			}

			var_Temp.fValue = INVALID_TEMPVALUE;
			pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[1][i], &var_Temp, tmNow, FALSE);
			SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_IS_VALID);
			SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_NEED_DISPLAY);
			
			break;
			
		case SENSOR_SET_BATTGROUP:
			//Remove it, because when Brain try to hide the signal by the display expression, it is valid. 2015-1-20 by Yangguoxin
			//SIG_STATE_SET(pAllTempSig[1][i], VALUE_STATE_NEED_DISPLAY);// No matter if vallid, always display

			pTempValue = (SIG_BASIC_VALUE *)GetTempValue_FromASensor(i,pSite);
			if(IsATempSensorExist(i,pSite))
			{
				SIG_STATE_SET(pAllTempSig[1][i], VALUE_STATE_NEED_DISPLAY);
			}
			else
			{
				SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_NEED_DISPLAY);
			}

			if(NULL != pTempValue) // to test if it is valid
			{
				if(SIG_STATE_TEST(pTempValue, VALUE_STATE_IS_VALID_RAW) && SIG_STATE_TEST(pTempValue, VALUE_STATE_NEED_DISPLAY))
				{	
					pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
					var_Temp.fValue = pTempValue->varValue.fValue;
					if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[1][i], &var_Temp, tmNow, FALSE))
					{
						SIG_STATE_SET(pAllTempSig[1][i], VALUE_STATE_IS_VALID);	

						pSignalVal = pAllTempSig[1][i];

						if (((SET_SIG_VALUE *)pSignalVal)->sv.iSamplerChannel >= 0)
						{
							var_Temp.fValue = INVALID_TEMPVALUE;
							pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
							Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[1][i], &var_Temp, tmNow, FALSE);			
						}						
					}
					else
					{

						pSignalVal = pAllTempSig[1][i];
					
						if (!pEquip->bCommStatus && (pSignalVal->ucSigType == SIG_TYPE_SAMPLING) &&
							(pSignalVal != (SIG_BASIC_VALUE *)pEquip->pCommStatusSigRef) &&
							(pSignalVal != (SIG_BASIC_VALUE *)pEquip->pWorkStatusSigRef))
						{
							var_Temp.fValue = INVALID_TEMPVALUE;
							pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
							Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[1][i], &var_Temp, tmNow, FALSE);			
							SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_IS_VALID);	
						}
					}
				}
				else
				{
					var_Temp.fValue = INVALID_TEMPVALUE;
					pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[1][i], &var_Temp, tmNow, FALSE);			
					SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_IS_VALID);	
				}
			}
			else
			{
				var_Temp.fValue = INVALID_TEMPVALUE;
				pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
				Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[1][i], &var_Temp, tmNow, FALSE);			
				SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_IS_VALID);	
			}

			var_Temp.fValue = INVALID_TEMPVALUE;
			pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
			Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[0][i], &var_Temp, tmNow, FALSE);
			
			SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_IS_VALID);
			SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_NEED_DISPLAY);
			
			break;
			
		default:
				var_Temp.fValue = INVALID_TEMPVALUE;
				pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM); 
				Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[0][i], &var_Temp, tmNow, FALSE);
				pEquip = Init_GetEquipRefById(pSite, EQM_EQUIPID_BATTARY_GROUP); 
				Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAllTempSig[1][i], &var_Temp, tmNow, FALSE);
				
				SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_IS_VALID);
				SIG_STATE_CLR(pAllTempSig[0][i], VALUE_STATE_NEED_DISPLAY);

				SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_IS_VALID);
				SIG_STATE_CLR(pAllTempSig[1][i], VALUE_STATE_NEED_DISPLAY);
			break;
		}	
	}
}

/*********************************************************************************
*  
*  FUNCTION NAME : Refresh_Comp_Temp
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-12-12 11:40:08
*  DESCRIPTION   : Get and set comp temp
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Refresh_Comp_Temp(IN SITE_INFO *pSite)
{
#define MIN_TEMPCOMP_TEMP_NUM (NUM_OF_TEMPS + 4)//79

#define EQU_BATT_GROUP_ID	115
#define SIG_ID_COMP_SET		63
#define SIG_ID_COMP_TEMP_VALUE	36


#define INVALID_SIG_VALUE	-273.0
#define NUM_SMBRC		12
#define EQU_SMBRC_ID		668
#define SIG_ID_SMBRC_TEMP	13

#define SIG_ID_COMM_STATE	99
#define SIG_ID_EXIST_STATE	100
	EQUIP_INFO	*pEquip = NULL;


	static SIG_BASIC_VALUE *pSigCompSet = NULL;
	static SIG_BASIC_VALUE *pSigCompValue = NULL;

	SIG_BASIC_VALUE *pSigCompValue_Temp = NULL;
	static SIG_BASIC_VALUE *pSigTemps[NUM_OF_TEMPS]; //71 batt-grp temps

	static SIG_BASIC_VALUE *pSigSMBRCTempSig[NUM_SMBRC] = { NULL };
	static SIG_BASIC_VALUE *pSigSMBRCExistSig[NUM_SMBRC] = { NULL };
	static SIG_BASIC_VALUE *pSigSMBRCCommStateSig[NUM_SMBRC] = { NULL };

	static BOOL bIni = FALSE;
	//static int nDelay = 0;

	int nCount = 0;
	int i;
	float fMax,fMin,fAvg,fAvgofSMBRC;

	VAR_VALUE var_Temp; //Temporary Value for a temp
	BOOL bCompTempOK = FALSE;
	time_t tmNow = time(NULL);
	static nDelay = 0; //avoid the alarm of comptemp failure when ACU+ Start
	if(!bIni)
	{
		bIni = TRUE;
		pEquip = Init_GetEquipRefById(pSite, EQU_BATT_GROUP_ID);
		pSigCompSet = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,SIG_ID_COMP_SET);
		pSigCompValue = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_COMP_TEMP_VALUE);
		SIG_STATE_CLR(pSigCompValue, VALUE_STATE_IS_VALID);
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(i < MAX_TEMP_SENSORS)
			{
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_1 + i);
			}
			else if( i>=MAX_TEMP_SENSORS & i< MAX_TEMP_SENSORS2)
			{
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_2 + i - MAX_TEMP_SENSORS);
			}
			else
			{
				
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_3 + i - MAX_TEMP_SENSORS2);	
			}
		}
		for(i = 0; i < NUM_SMBRC; i++)
		{
			pEquip = Init_GetEquipRefById(pSite, EQU_SMBRC_ID + i);

			//G3_OPT, by Lin.Tao.Thomas, 2013-4
			if (pEquip)
			{
				pSigSMBRCCommStateSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_COMM_STATE);
				pSigSMBRCExistSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_EXIST_STATE);
				pSigSMBRCTempSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_SMBRC_TEMP);
			}
		}
	}


	if(NULL == pSigCompSet)
	{
		return;
	}
	bCompTempOK = FALSE;
	pEquip = Init_GetEquipRefById(pSite, EQU_BATT_GROUP_ID);
	SIG_STATE_CLR(pSigCompValue, VALUE_STATE_IS_VALID);
	if(pSigCompSet->varValue.enumValue <= 0) //None
	{
		bCompTempOK = TRUE;
	}
	else if(pSigCompSet->varValue.enumValue == 1)//Max
	{
		fMax  = -50.0;
		nCount = 0;
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(NULL == pSigTemps[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigTemps[i]) && SIG_VALUE_NEED_DISPLAY(pSigTemps[i]))
			{
				if(pSigTemps[i]->varValue.fValue > fMax && pSigTemps[i]->varValue.fValue > -140.0 && pSigTemps[i]->varValue.fValue < 150.0)
				{
					fMax = pSigTemps[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			var_Temp.fValue = fMax;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigCompValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigCompValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE;
			}
		}	

	}
	else if(pSigCompSet->varValue.enumValue == 2)//Average
	{
		nCount = 0;
		fAvg  = 0.0;
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(NULL == pSigTemps[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigTemps[i]) && SIG_VALUE_NEED_DISPLAY(pSigTemps[i]))
			{
				if(pSigTemps[i]->varValue.fValue > -140.0 && pSigTemps[i]->varValue.fValue < 150.0)
				{
					fAvg += pSigTemps[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			fAvg /= nCount; 
			var_Temp.fValue = fAvg;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigCompValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigCompValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE;
			}	
		}	
		//printf("\n%%Run Here for fAvg of temps as comp! fAvg=%f, pSigTemps[0]->varValue.fValue=%f,pSigTemps[1]->varValue.fValue=%f~~\n",fAvg,pSigTemps[0]->varValue.fValue,pSigTemps[1]->varValue.fValue);
	}
	else if(pSigCompSet->varValue.enumValue == 3)//Average of SMBRC
	{
		nCount = 0;
		fAvgofSMBRC  = 0.0;
		for(i = 0; i < NUM_SMBRC; i++)
		{	
		    if( !(pSigSMBRCExistSig[i]) ||     //G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
				pSigSMBRCCommStateSig[i]->varValue.enumValue > 0 || pSigSMBRCExistSig[i]->varValue.enumValue > 0)//first to tell if the equip works OK
			{
				continue;
			}
			if(NULL == pSigSMBRCTempSig[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigSMBRCTempSig[i]) && SIG_VALUE_NEED_DISPLAY(pSigSMBRCTempSig[i]))
			{
				if(pSigSMBRCTempSig[i]->varValue.fValue > -140.0 && pSigSMBRCTempSig[i]->varValue.fValue < 150.0)
				{
					fAvgofSMBRC += pSigSMBRCTempSig[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			fAvgofSMBRC /= nCount; 
			var_Temp.fValue = fAvgofSMBRC;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigCompValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigCompValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE;
			}	
		}
	}
	else if(pSigCompSet->varValue.enumValue >= 4 && pSigCompSet->varValue.enumValue < (NUM_OF_TEMPS + 4))// Temp1 ... Temp71
	{		
		pSigCompValue_Temp = pSigTemps[(int)(pSigCompSet->varValue.enumValue) -4];

		if(SIG_VALUE_IS_VALID(pSigCompValue_Temp) && SIG_VALUE_NEED_DISPLAY(pSigCompValue_Temp))
		{
			//printf("\n!!!!!!!!Unbelievable!!!!!!\n");
			var_Temp.fValue = pSigCompValue_Temp->varValue.fValue;			
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigCompValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigCompValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE;
				//SIG_STATE_SET(pSigCompValue, VALUE_STATE_NEED_DISPLAY);
			}
		}
	}
	else if( pSigCompSet->varValue.enumValue == MIN_TEMPCOMP_TEMP_NUM )
	{
		fMin  = 100.0;
		nCount = 0;
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(NULL == pSigTemps[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigTemps[i]) && SIG_VALUE_NEED_DISPLAY(pSigTemps[i]))
			{
				if(pSigTemps[i]->varValue.fValue < fMin && pSigTemps[i]->varValue.fValue > -140.0 && pSigTemps[i]->varValue.fValue < 150.0)
				{
					fMin = pSigTemps[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			var_Temp.fValue = fMin;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigCompValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigCompValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE;
			}
		}	
	
		//printf("\n%%Run Here for fMin of temps as comp! fMin=%f, pSigTemps[0]->varValue.fValue=%f,pSigTemps[1]->varValue.fValue=%f~~\n",fMin,pSigTemps[0]->varValue.fValue,pSigTemps[1]->varValue.fValue);
	}
	
	if(!bCompTempOK )
	{
		var_Temp.fValue = INVALID_SIG_VALUE;
		Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigCompValue, &var_Temp, tmNow, FALSE);
	}
}


/*********************************************************************************
*  FUNCTION NAME : Refresh_Comp_Enable_Use_Sensor_Fail
*  CREATOR       :
*  CREATED DATE  : 2018-08-25
*  DESCRIPTION   : 
*  CHANGE HISTORY: 
***********************************************************************************/
void  Refresh_Comp_Enable_Use_Sensor_Fail(IN SITE_INFO *pSite) 
{
#define SMTEMP_NUMBER				8
#define EACH_SMTEMP_SENSOR_NUMBER		8
#define ALL_SENSOR_NUMBER			75
#define SMTEMP_PROBE_NUMBER			64
#define SYSTEM_SENSOR_NUMBER			7
#define SYSTEM_SENSOR_NUMBER2			11
#define SMTEMP_FIRST_SIGNED_ID			11
#define SMTEMP_PROBE_1_OPEN_SIGNED_ID	21
#define TEMP_SERNSOR_NOT_FAIL 			0
#define TEMP_SERNSOR_FAIL 			1
#define COMMUNICATION_FAIL 			1
#define BATTERY_GROUP_EQUIP_ID 			115
#define SENSOR_DISABLE_SIGNAL_ID 		500
#define TMP_CMP_ENABLE_BY_SENSOR_FAIL		300
#define DISABLE_FUNCTION_USE_SENSOR_FAIL   		0	
#define SENSOR_FAIL_TYPE_ALL_TEMP   		0	
#define SENSOR_FAIL_TYPE_ANY_TEMP   		1
#define TEMP_COMP_DISABLE			0	
#define TEMP_COMP_ENABLE			1

#define SMDUE_NUM	2    
#define EACH_SMDUETEMP_SENSOR_NUMBER		10

	int i=0;
	int j=0;
	int iTemp_Num_Use_For_Bat=0;
	int iTemp_Num_Sensor_Fail=0;
	time_t tmNow = time(NULL);
	VAR_VALUE var_TmpCmp_Enable; 
	static BOOL bRunOnce = TRUE; 
	EQUIP_INFO* pEquip=NULL;	
	EQUIP_INFO* pEquipSys=NULL;	
	EQUIP_INFO* pEquipSMTemp[SMTEMP_NUMBER]={NULL};
	EQUIP_INFO* pEquipSMDUETemp[SMDUE_NUM]={NULL};
	static EQUIP_INFO* pEquipBatGrp=NULL;	
	static ALARM_SIG_VALUE *pSigALLTempAlarm[MAX_TEMP_SENSORS3] = {NULL};//use for statistics all Temp's not used alarm
	static ALARM_SIG_VALUE *pSigSmtempProbeOpenAlarm[SMTEMP_PROBE_NUMBER] = {NULL};
	static ALARM_SIG_VALUE *pSigSensorFaultAlarm[SYSTEM_SENSOR_NUMBER2] = {NULL};//Only use for statistics mainBoard/IB/EIB Temp Fault alarm
	static ALARM_SIG_VALUE *pSigSMtempComFailAlarm[SMTEMP_NUMBER] = {NULL};

	static ALARM_SIG_VALUE *pSigSMDUESensorFaultAlarm[20] = {NULL};//Only use for statistics SMUDE Fault alarm

	static ALARM_SIG_VALUE *pSigSMDUEtempComFailAlarm[SMDUE_NUM] = {NULL};
	static ALARM_SIG_VALUE *pSigCanComFailAlarm = NULL;
	static ALARM_SIG_VALUE *pSigIBComFailAlarm = NULL;
	static ALARM_SIG_VALUE *pSigEIBComFailAlarm = NULL;
	static ALARM_SIG_VALUE *pSigIB2ComFailAlarm = NULL;
	static ALARM_SIG_VALUE *pSigEIB2ComFailAlarm = NULL;


	static SIG_BASIC_VALUE *pSigCompEnable = NULL;
	static SIG_BASIC_VALUE *pSigFailType = NULL;
 	int	iSigIdSYSIBTempAlarm[SYSTEM_SENSOR_NUMBER2] ={18,19,20,21,22,42,43,450,451,452,453};
 	int	iSigIdSensorFaultAlarm[SYSTEM_SENSOR_NUMBER2] ={9,25,28,31,34,48,49,454,455,456,457};
 	unsigned char ucAllSensorsState[MAX_TEMP_SENSORS3]={0};


	//0, Get the pointer
	if(bRunOnce)
	{	
		bRunOnce = FALSE;
		pEquipSys = Init_GetEquipRefById(pSite, EQM_EQUIPID_SYSTEM);
		for(i=0; i<SMTEMP_NUMBER; i++)
		{
			pEquipSMTemp[i] = Init_GetEquipRefById(pSite, EQM_EQUIPID_SMTEMP_1 + i);
		}
		for(i=0; i<SMDUE_NUM; i++)
		{
			pEquipSMDUETemp[i] = Init_GetEquipRefById(pSite, EQM_EQUIPID_SMDUE_1 + i);
		}
		
		
		for(i = 0; i < SYSTEM_SENSOR_NUMBER2; i++)
		{
			if(i < SYSTEM_SENSOR_NUMBER)
			{
				pSigALLTempAlarm[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSys,SIG_TYPE_ALARM,iSigIdSYSIBTempAlarm[i]);
			}
			else
			{
				pSigALLTempAlarm[i + 71 - 7] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSys,SIG_TYPE_ALARM,iSigIdSYSIBTempAlarm[i]);
			}

			pSigSensorFaultAlarm[i] =  (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSys,SIG_TYPE_ALARM,iSigIdSensorFaultAlarm[i]);
			
		}

		pSigCanComFailAlarm = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSys,SIG_TYPE_ALARM,2);
		
		for(i = 0; i < SMTEMP_NUMBER; i++)
		{
			for (j = 0; j < EACH_SMTEMP_SENSOR_NUMBER; j++)
			{
				pSigALLTempAlarm[7 + i*8+j] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSMTemp[i],SIG_TYPE_ALARM,SMTEMP_FIRST_SIGNED_ID+j);
				pSigSmtempProbeOpenAlarm[i*8+j] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSMTemp[i],SIG_TYPE_ALARM,SMTEMP_PROBE_1_OPEN_SIGNED_ID+j);
			}
			pSigSMtempComFailAlarm[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSMTemp[i],SIG_TYPE_ALARM,1);
		}

		for(i = 0; i < SMDUE_NUM; i++)
		{
			for (j = 0; j < EACH_SMDUETEMP_SENSOR_NUMBER; j++)
			{	
				pSigALLTempAlarm[MAX_TEMP_SENSORS2+ i*10+j] =(ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSMDUETemp[i],SIG_TYPE_ALARM,111+j);
				pSigSMDUESensorFaultAlarm[i*10+j] =(ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSMDUETemp[i],SIG_TYPE_ALARM,121+j); 
			}
			pSigSMDUEtempComFailAlarm[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquipSMDUETemp[i],SIG_TYPE_ALARM,1);
		}


		pEquip = Init_GetEquipRefById(pSite, 202);//StdEquip_IB.cfg
		pSigIBComFailAlarm = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,1);
		pEquip = Init_GetEquipRefById(pSite, 197);//StdEquip_EIB.cfg
		pSigEIBComFailAlarm = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,1);
		pEquip = Init_GetEquipRefById(pSite, 203);//StdEquip_IB2.cfg
		pSigIB2ComFailAlarm = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,1);
		pEquip = Init_GetEquipRefById(pSite, 198);//StdEquip_EIB2.cfg
		pSigEIB2ComFailAlarm = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,1);

		pEquipBatGrp = Init_GetEquipRefById(pSite, BATTERY_GROUP_EQUIP_ID);
		pSigFailType = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquipBatGrp,SIG_TYPE_SETTING,SENSOR_DISABLE_SIGNAL_ID);
		pSigCompEnable = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquipBatGrp,SIG_TYPE_SAMPLING,TMP_CMP_ENABLE_BY_SENSOR_FAIL);
	}

	//1,Get the all Sensors States
	for(i=0; i< MAX_TEMP_SENSORS3; i++)
	{
		if(TEMP_SERNSOR_FAIL == pSigALLTempAlarm[i]->bv.varValue.lValue)
		{
			ucAllSensorsState[i] = TEMP_SERNSOR_FAIL;
		}
		else
		{
			ucAllSensorsState[i] = TEMP_SERNSOR_NOT_FAIL;
		}
	}

	for(i=0; i< SMTEMP_PROBE_NUMBER; i++)
	{
		if(TEMP_SERNSOR_FAIL == pSigSmtempProbeOpenAlarm[i]->bv.varValue.lValue)
		{
			ucAllSensorsState[i+7] = TEMP_SERNSOR_FAIL;
		}
	}
	
	for(i=0; i<SYSTEM_SENSOR_NUMBER2; i++)
	{
		if( i < 7 )
		{
			if(TEMP_SERNSOR_FAIL == pSigSensorFaultAlarm[i]->bv.varValue.lValue)
			{
				ucAllSensorsState[i] = TEMP_SERNSOR_FAIL;	
			}
		}
		else
		{
			if(TEMP_SERNSOR_FAIL == pSigSensorFaultAlarm[i]->bv.varValue.lValue)
			{
				ucAllSensorsState[i-7+71] = TEMP_SERNSOR_FAIL;	
			}
		}
	}

	for(i=0; i< 20; i++)
	{
		if(TEMP_SERNSOR_FAIL == pSigSMDUESensorFaultAlarm[i]->bv.varValue.lValue)
		{
			ucAllSensorsState[i+75] = TEMP_SERNSOR_FAIL;
		}
	}


	if( COMMUNICATION_FAIL ==  pSigIBComFailAlarm->bv.varValue.lValue )
	{
		ucAllSensorsState[3] = TEMP_SERNSOR_FAIL;
		ucAllSensorsState[4] = TEMP_SERNSOR_FAIL;
	}     

	if( COMMUNICATION_FAIL ==  pSigEIBComFailAlarm->bv.varValue.lValue )
	{
		ucAllSensorsState[5] = TEMP_SERNSOR_FAIL;
		ucAllSensorsState[6] = TEMP_SERNSOR_FAIL;
	}  	
	
	if( COMMUNICATION_FAIL ==  pSigIB2ComFailAlarm->bv.varValue.lValue )
	{
		ucAllSensorsState[71] = TEMP_SERNSOR_FAIL;
		ucAllSensorsState[72] = TEMP_SERNSOR_FAIL;
	}
	if( COMMUNICATION_FAIL ==  pSigEIB2ComFailAlarm->bv.varValue.lValue )
	{
		ucAllSensorsState[73] = TEMP_SERNSOR_FAIL;
		ucAllSensorsState[74] = TEMP_SERNSOR_FAIL;
	}
	if( COMMUNICATION_FAIL ==  pSigCanComFailAlarm->bv.varValue.lValue )
	{
		for(i=0; i< 64; i++)
		{
			ucAllSensorsState[7+i] = TEMP_SERNSOR_FAIL;
		}
		for(i=0;i<20;i++)
		{
			ucAllSensorsState[75+i] = TEMP_SERNSOR_FAIL;
		}
	}
	for(i = 0; i < SMTEMP_NUMBER; i++)
	{
		if(COMMUNICATION_FAIL == pSigSMtempComFailAlarm[i]->bv.varValue.lValue)
		{
			for (j = 0; j < EACH_SMTEMP_SENSOR_NUMBER; j++)
			{
				ucAllSensorsState[7+i*8+j] = TEMP_SERNSOR_FAIL;
			}
		}
	}
	for(i = 0; i < SMDUE_NUM; i++)
	{
		if(COMMUNICATION_FAIL == pSigSMDUEtempComFailAlarm[i]->bv.varValue.lValue)
		{
			for (j = 0; j < EACH_SMDUETEMP_SENSOR_NUMBER; j++)
			{
				ucAllSensorsState[75+i*10+j] = TEMP_SERNSOR_FAIL;
			}
		}
	}
	

	//2, Get  "iTemp_Num_Use_For_Bat"  and  "iTemp_Num_Sensor_Fail"
	for(i = 0; i < MAX_TEMP_SENSORS3; i++)
	{
		if( SENSOR_SET_BATTGROUP == g_Sensor_Settings[i] )
		{
			iTemp_Num_Use_For_Bat++;
			if(TEMP_SERNSOR_FAIL == ucAllSensorsState[i])
			{
				iTemp_Num_Sensor_Fail++;
			}
		}
		//printf("Refresh_Comp_Enable_Use_Sensor_Fail(): ucAllSensorsState[%d] =%d @@@@@@@@@@@@@@@@@@@\n",i,ucAllSensorsState[i]);
	}


	//3, Get  "var_TmpCmp_Enable.enumValue"  use  "iTemp_Num_Use_For_Bat"  and  "iTemp_Num_Sensor_Fail"
	if( 0 == iTemp_Num_Use_For_Bat )
	{
		//printf("Refresh_Comp_Enable_Use_Sensor_Fail():No Sensor use for Battery~~~~~~~~~~~~~~~~~~~~~~\n");
		var_TmpCmp_Enable.enumValue = TEMP_COMP_DISABLE;
	}
	else
	{
		if( 0 == iTemp_Num_Sensor_Fail )
		{
			 var_TmpCmp_Enable.enumValue = TEMP_COMP_ENABLE;
		}
		else
		{
			if( iTemp_Num_Use_For_Bat == iTemp_Num_Sensor_Fail )
			{
				 var_TmpCmp_Enable.enumValue = TEMP_COMP_DISABLE;//����������²��¶Ƚ����-273�ȣ���ʱ��ǰ���߼����н�ֹ�²�
			}
			else
			{
				if( SENSOR_FAIL_TYPE_ALL_TEMP == pSigFailType->varValue.enumValue )
				{
					   var_TmpCmp_Enable.enumValue = TEMP_COMP_ENABLE;
				}
				else if( SENSOR_FAIL_TYPE_ANY_TEMP == pSigFailType->varValue.enumValue )
				{
					  var_TmpCmp_Enable.enumValue = TEMP_COMP_DISABLE;
				}
				else
				{
					 var_TmpCmp_Enable.enumValue = TEMP_COMP_ENABLE;
				}	
			}
		}
	}

	//RunThread_Heartbeat(RunThread_GetId(NULL));

	//4, Set var_TmpCmp_Enable.enumValue
	if(Equip_SetVirtualSignalValue(pSite, pEquipBatGrp, SIG_TYPE_SAMPLING, pSigCompEnable, &var_TmpCmp_Enable, tmNow, FALSE))
	{
		SIG_STATE_SET(pSigCompEnable, VALUE_STATE_IS_VALID);
	}

	//printf("Refresh_Comp_Enable_Use_Sensor_Fail():iTemp_Num_Use_For_Bat=%d,iTemp_Num_Sensor_Fail=%d, var_TmpCmp_Enable.enumValue=%d~~~\n",iTemp_Num_Use_For_Bat,iTemp_Num_Sensor_Fail, var_TmpCmp_Enable.enumValue);
	
	return;
}


/*********************************************************************************
*  
*  FUNCTION NAME : Refresh_BTRM_Temp
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-02-04
*  DESCRIPTION   : Get and set BTRM temp
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Refresh_BTRM_Temp(IN SITE_INFO *pSite)
{
#define EQU_BATT_GROUP_ID	115
#define SIG_ID_BTRM_SET		414
#define SIG_ID_BTRM_TEMP_VALUE	182


#define INVALID_SIG_VALUE	-273.0
#define NUM_SMBRC		12
#define EQU_SMBRC_ID		668
#define SIG_ID_SMBRC_TEMP	13

#define SIG_ID_COMM_STATE	99
#define SIG_ID_EXIST_STATE	100
	EQUIP_INFO	*pEquip = NULL;


	static SIG_BASIC_VALUE *pSigBTRMSet = NULL;
	static SIG_BASIC_VALUE *pSigBTRMValue = NULL;

	SIG_BASIC_VALUE *pSigBTRMValue_Temp = NULL;
	static SIG_BASIC_VALUE *pSigTemps[NUM_OF_TEMPS]; //71 batt-grp temps

	static SIG_BASIC_VALUE *pSigSMBRCTempSig[NUM_SMBRC] = { NULL };
	static SIG_BASIC_VALUE *pSigSMBRCExistSig[NUM_SMBRC] = { NULL };
	static SIG_BASIC_VALUE *pSigSMBRCCommStateSig[NUM_SMBRC] = { NULL };

	static BOOL bIni = FALSE;
	//static int nDelay = 0;

	int nCount = 0;
	int i;
	float fMax,fAvg,fAvgofSMBRC;

	VAR_VALUE var_Temp; //Temporary Value for a temp
	BOOL bCompTempOK = FALSE;
	time_t tmNow = time(NULL);
	static nDelay = 0; //avoid the alarm of comptemp failure when ACU+ Start
	if(!bIni)
	{
		bIni = TRUE;
		//printf("\n*****************************First Time Init Comp Temp!!");
		pEquip = Init_GetEquipRefById(pSite, EQU_BATT_GROUP_ID);
		pSigBTRMSet = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,SIG_ID_BTRM_SET);
		pSigBTRMValue = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_BTRM_TEMP_VALUE);
		SIG_STATE_CLR(pSigBTRMValue, VALUE_STATE_IS_VALID);
		//SIG_STATE_CLR(pSigCompValue, VALUE_STATE_NEED_DISPLAY);
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(i < MAX_TEMP_SENSORS)
			{
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_1 + i);
			}
			else	  if(i>=MAX_TEMP_SENSORS && i<MAX_TEMP_SENSORS2)
			{
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_2 + i - MAX_TEMP_SENSORS);
			}
			else
			{
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_3 + i - MAX_TEMP_SENSORS2);	
			}
		}
		for(i = 0; i < NUM_SMBRC; i++)
		{
			pEquip = Init_GetEquipRefById(pSite, EQU_SMBRC_ID + i);

			//G3_OPT, by Lin.Tao.Thomas, 2013-4
			if (pEquip)
			{
				pSigSMBRCCommStateSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_COMM_STATE);
				pSigSMBRCExistSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_EXIST_STATE);
				pSigSMBRCTempSig[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_SMBRC_TEMP);
			}
		}
	}

	if(NULL == pSigBTRMSet)
	{
		return;
	}
	bCompTempOK = FALSE;
	pEquip = Init_GetEquipRefById(pSite, EQU_BATT_GROUP_ID);
	SIG_STATE_CLR(pSigBTRMValue, VALUE_STATE_IS_VALID);
	if(pSigBTRMSet->varValue.enumValue <= 0) //None
	{
		bCompTempOK = TRUE;
	}
	else if(pSigBTRMSet->varValue.enumValue == 1)//Max
	{
		fMax  = -50.0;
		nCount = 0;
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(NULL == pSigTemps[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigTemps[i]) && SIG_VALUE_NEED_DISPLAY(pSigTemps[i]))
			{
				if(pSigTemps[i]->varValue.fValue > fMax && pSigTemps[i]->varValue.fValue > -140.0 && pSigTemps[i]->varValue.fValue < 150.0)
				{
					fMax = pSigTemps[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			var_Temp.fValue = fMax;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBTRMValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigBTRMValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE;
			}
		}	
	}
	else if(pSigBTRMSet->varValue.enumValue == 2)//Average
	{
		nCount = 0;
		fAvg  = 0.0;
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(NULL == pSigTemps[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigTemps[i]) && SIG_VALUE_NEED_DISPLAY(pSigTemps[i]))
			{
				if(pSigTemps[i]->varValue.fValue > -140.0 && pSigTemps[i]->varValue.fValue < 150.0)
				{
					fAvg += pSigTemps[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			fAvg /= nCount; 
			var_Temp.fValue = fAvg;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBTRMValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigBTRMValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE;
			}	
		}		
	}
	else if(pSigBTRMSet->varValue.enumValue == 3)//Average of SMBRC
	{
		nCount = 0;
		fAvgofSMBRC  = 0.0;
		for(i = 0; i < NUM_SMBRC; i++)
		{	
			if( !(pSigSMBRCExistSig[i]) ||   //G3_OPT [loader], by Lin.Tao.Thoams, 2013-4
				pSigSMBRCCommStateSig[i]->varValue.enumValue > 0 || pSigSMBRCExistSig[i]->varValue.enumValue > 0)//first to tell if the equip works OK
			{
				continue;
			}
			if(NULL == pSigSMBRCTempSig[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigSMBRCTempSig[i]) && SIG_VALUE_NEED_DISPLAY(pSigSMBRCTempSig[i]))
			{
				if(pSigSMBRCTempSig[i]->varValue.fValue > -140.0 && pSigSMBRCTempSig[i]->varValue.fValue < 150.0)
				{
					fAvgofSMBRC += pSigSMBRCTempSig[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			fAvgofSMBRC /= nCount; 
			var_Temp.fValue = fAvgofSMBRC;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBTRMValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigBTRMValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE; 
			}	
		}
	}
	else if(pSigBTRMSet->varValue.enumValue >= 4 && pSigBTRMSet->varValue.enumValue < (NUM_OF_TEMPS+4))// Temp1 ... Temp71
	{	
		pSigBTRMValue_Temp = pSigTemps[(int)(pSigBTRMSet->varValue.enumValue) -4];
		if(SIG_VALUE_IS_VALID(pSigBTRMValue_Temp) && SIG_VALUE_NEED_DISPLAY(pSigBTRMValue_Temp))
		{ 
			var_Temp.fValue = pSigBTRMValue_Temp->varValue.fValue;			
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBTRMValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigBTRMValue, VALUE_STATE_IS_VALID);
				bCompTempOK = TRUE; 
			}
		}
	}

	if(!bCompTempOK ) 
	{
		var_Temp.fValue = INVALID_SIG_VALUE;
		Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigBTRMValue, &var_Temp, tmNow, FALSE);
	}
}

/*********************************************************************************
*  
*  FUNCTION NAME : Refresh_TempAlarm
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-12-12 14:49:21
*  DESCRIPTION   : Set special temp alarms
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
//static void Refresh_TempAlarm(IN SITE_INFO *pSite)
//{
//#define EQU_AMBIENT_UNIT	1
//#define EQU_BATT_GRP		115
//
//#define SIG_ID_ALM_AMBIENT_HI	181
//#define SIG_ID_ALM_AMBIENT_LOW	182
//
//#define SIG_ID_ALM_BATT_VHI	31
//#define SIG_ID_ALM_BATT_HI	32
//#define SIG_ID_ALM_BATT_LOW	33
//#define NUM_OF_ALARMS		8
//
//#define NUM_OF_SYS_SENSORS	7
//
//#define SIG_ID_BATTGRP_VHI	210
//#define SIG_ID_BATTGRP_HI	211
//#define SIG_ID_BATTGRP_LOW	212
//
//#define SIG_ID_AMBIENT_FAULT	209
//#define SIG_ID_AMBIENT_HI	210
//#define SIG_ID_AMBIENT_LOW	211
//
//
//	static int nIdx_of_SigIDSFault[NUM_OF_SYS_SENSORS] = {9,25,28,31,34,48,49};//sensor fault alarm ID 
//	static BOOL bIni = FALSE;
//	static ALARM_SIG_VALUE* pAmbient_Hi_Alarms[NUM_OF_ALARMS];
//	static ALARM_SIG_VALUE* pAmbient_Low_Alarms[NUM_OF_ALARMS];
//	static ALARM_SIG_VALUE* pAmbient_Sensor_Faults[NUM_OF_SYS_SENSORS];
//
//	static ALARM_SIG_VALUE* pBatt_VHi_Alarms[NUM_OF_ALARMS];
//	static ALARM_SIG_VALUE* pBatt_Hi_Alarms[NUM_OF_ALARMS];
//	static ALARM_SIG_VALUE* pBatt_Low_Alarms[NUM_OF_ALARMS];
//
//	static SIG_BASIC_VALUE* pAmbientSampStatus[3];
//	static SIG_BASIC_VALUE* pBAttGrpSampStatus[3];
//	VAR_VALUE setValueTemp;
//	int i,j;
//	EQUIP_INFO	*pEquip = NULL;
//	time_t tmNow = time(NULL);
//	if(!bIni)
//	{
//		printf("\n*****************************First Time Init Temp Alarm!!");
//		bIni = TRUE;
//		pEquip = Init_GetEquipRefById(pSite, EQU_AMBIENT_UNIT);
//		for(i = 0; i < NUM_OF_ALARMS; i++)
//		{
//			pAmbient_Hi_Alarms[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,SIG_ID_ALM_AMBIENT_HI + 2 * i);
//			pAmbient_Low_Alarms[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,SIG_ID_ALM_AMBIENT_LOW + 2 * i);
//		}
//		pAmbientSampStatus[0] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_AMBIENT_FAULT);
//		pAmbientSampStatus[1] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_AMBIENT_HI);
//		pAmbientSampStatus[2] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_AMBIENT_LOW);
//		for(i = 0; i < NUM_OF_SYS_SENSORS; i++)
//		{
//			pAmbient_Sensor_Faults[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,nIdx_of_SigIDSFault[i]);		
//		}
//
//		pEquip = Init_GetEquipRefById(pSite, EQU_BATT_GRP);
//		for(i = 0; i < NUM_OF_ALARMS; i++)
//		{
//			pBatt_VHi_Alarms[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,SIG_ID_ALM_BATT_VHI + 3 * i);
//			pBatt_Hi_Alarms[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,SIG_ID_ALM_BATT_HI + 3 * i);
//			pBatt_Low_Alarms[i] = (ALARM_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_ALARM,SIG_ID_ALM_BATT_LOW + 3 * i);
//		}
//		pBAttGrpSampStatus[0] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_BATTGRP_VHI);
//		pBAttGrpSampStatus[1] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_BATTGRP_HI);
//		pBAttGrpSampStatus[2] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_BATTGRP_LOW);
//		for(j = 0; j < 3; j++)
//		{
//			SIG_STATE_SET(pAmbientSampStatus[j], VALUE_STATE_IS_VALID); //intitial first
//			SIG_STATE_SET(pAmbientSampStatus[j], VALUE_STATE_IS_VALID);
//		}
//	}
//	BOOL bAlmFlag[2][3];
//	static int nDelay = 0;
//	if(nDelay < 20)
//	{
//		nDelay++;
//		return;
//	}
//	//First intial all values set to False
//	for(i = 0 ; i < 2; i++)
//	{
//		for(j = 0; j < 3; j++)
//		{
//			bAlmFlag[i][j] = FALSE;
//		}
//	}
//	//Update all values
//	for(i = 0; i < NUM_OF_ALARMS; i++)
//	{
//		if(pAmbient_Hi_Alarms[i]->bv.varValue.enumValue > 0)// has alarm
//		{
//			bAlmFlag[0][1] = TRUE;
//		}
//		if(pAmbient_Low_Alarms[i]->bv.varValue.enumValue > 0)// has alarm
//		{
//			bAlmFlag[0][2] = TRUE;
//		}
//		if(i < NUM_OF_SYS_SENSORS)
//		{
//			if(pAmbient_Sensor_Faults[i]->bv.varValue.enumValue > 0)// has alarm
//			{
//				bAlmFlag[0][0] = TRUE;
//			}
//		}
//		if(pBatt_VHi_Alarms[i]->bv.varValue.enumValue > 0)// has alarm
//		{
//			bAlmFlag[1][0] = TRUE;
//		}
//		if(pBatt_Hi_Alarms[i]->bv.varValue.enumValue > 0)// has alarm
//		{
//			bAlmFlag[1][1] = TRUE;
//		}
//		if(pBatt_Low_Alarms[i]->bv.varValue.enumValue > 0)// has alarm
//		{
//			bAlmFlag[1][2] = TRUE;
//		}
//	}
//	//Update status signal values
//	for (i = 0; i < 2;i++)
//	{
//		for (j = 0; j < 3;j++)
//		{
//			if(i == 0) //Ambient
//			{
//				pEquip = Init_GetEquipRefById(pSite, EQU_AMBIENT_UNIT);
//				if(bAlmFlag[i][j])
//				{
//					//SIG_STATE_SET(pAmbientSampStatus[j], VALUE_STATE_IS_VALID);
//					//SIG_STATE_SET(pAmbientSampStatus[j], VALUE_STATE_IS_CONFIGURED);
//					setValueTemp.enumValue = 1;
//					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAmbientSampStatus[j], &setValueTemp, tmNow, FALSE);
//				}
//				else
//				{
//					//SIG_STATE_CLR(pAmbientSampStatus[j], VALUE_STATE_IS_VALID);
//					//SIG_STATE_CLR(pAmbientSampStatus[j], VALUE_STATE_IS_CONFIGURED);
//					setValueTemp.enumValue = 0;
//					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pAmbientSampStatus[j], &setValueTemp, tmNow, FALSE);
//				}
//			}
//			else//BattGrp
//			{
//				pEquip = Init_GetEquipRefById(pSite, EQU_BATT_GRP);
//				if(bAlmFlag[i][j])
//				{
//					//SIG_STATE_SET(pBAttGrpSampStatus[j], VALUE_STATE_IS_VALID);
//					//SIG_STATE_SET(pBAttGrpSampStatus[j], VALUE_STATE_IS_CONFIGURED);
//					setValueTemp.enumValue = 1;
//					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pBAttGrpSampStatus[j], &setValueTemp, tmNow, FALSE);
//				}
//				else
//				{
//					//SIG_STATE_CLR(pBAttGrpSampStatus[j], VALUE_STATE_IS_VALID);
//					//SIG_STATE_CLR(pBAttGrpSampStatus[j], VALUE_STATE_IS_CONFIGURED);
//					setValueTemp.enumValue = 0;
//					Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pBAttGrpSampStatus[j], &setValueTemp, tmNow, FALSE);
//				}
//			}
//		}
//	}
//	//printf("\n&&&&&&&&Begin Print Alarm status&&&&&&&&&&&");
//	//for(i = 0; i < 3;i++)
//	//{
//	//	printf("\nThe current Sys alarm is %d",pAmbientSampStatus[i]->varValue.enumValue);
//	//}
//	//for(i = 0; i < 3;i++)
//	//{
//	//	printf("\nThe current Battgrp alarm is %d",pBAttGrpSampStatus[i]->varValue.enumValue);
//	//}
//}
/*********************************************************************************
*  
*  FUNCTION NAME : StdEquip_TempSig_ProcessSpecialSignals
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-12-7 15:00:11
*  DESCRIPTION   : This function is created by WJ, but I've re-written it here
For CR <Temp Strategy>
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/

/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-12-22 19:37:26
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Refresh_AAmbient_Temp(IN SITE_INFO *pSite)
{
#define EQU_SYSTEM_ID	1
#define SIG_ID_AMBIENT_SET		208
#define SIG_ID_AMBIENT_TEMP_VALUE	69
#define INVALID_SIG_VALUE	-273.0

	EQUIP_INFO	*pEquip = NULL;

	static SIG_BASIC_VALUE *pSigAmbientValue = NULL;
	static SIG_BASIC_VALUE *pSigAmbientSet = NULL;
	static SIG_BASIC_VALUE *pSigTemps[NUM_OF_TEMPS]; //71 ambient temps

	static BOOL bIni = FALSE;
	static int nDelay = 0;
	BOOL bAmbientOK = FALSE;
	int i;

	VAR_VALUE var_Temp; //Temporary Value for a temp
	time_t tmNow = time(NULL);
	if(!bIni)
	{
		bIni = TRUE;
		//printf("\n*****************************First Time Init Comp Temp!!");
		pEquip = Init_GetEquipRefById(pSite, EQU_SYSTEM_ID);
		pSigAmbientValue = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_AMBIENT_TEMP_VALUE);
		pSigAmbientSet = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SETTING,SIG_ID_AMBIENT_SET);
		SIG_STATE_CLR(pSigAmbientValue, VALUE_STATE_IS_VALID);
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(i < MAX_TEMP_SENSORS)
			{
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_1 + i);
			}
			else if(i>=MAX_TEMP_SENSORS && i<MAX_TEMP_SENSORS2)
			{
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_2 + i - MAX_TEMP_SENSORS);
			}
			else
			{
				pSigTemps[i] = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip,SIG_TYPE_SAMPLING,SIG_ID_TEMP_AT_3 + i - MAX_TEMP_SENSORS2);		
			}
		}
	}

	pEquip = Init_GetEquipRefById(pSite, EQU_SYSTEM_ID);

	SIG_STATE_CLR(pSigAmbientValue, VALUE_STATE_IS_VALID);

	bAmbientOK = FALSE;
	float fMax,fAvg;
	int nCount;
	if(pSigAmbientSet->varValue.enumValue == 0)
	{
		bAmbientOK = TRUE;
	}
	else if(pSigAmbientSet->varValue.enumValue == 1)//Max
	{ 
		fMax  = -50.0;
		nCount = 0;
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(NULL == pSigTemps[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigTemps[i]) && SIG_VALUE_NEED_DISPLAY(pSigTemps[i]))
			{
				if(pSigTemps[i]->varValue.fValue > fMax && pSigTemps[i]->varValue.fValue > -140.0 && pSigTemps[i]->varValue.fValue < 150.0)
				{
					fMax = pSigTemps[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			var_Temp.fValue = fMax;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigAmbientValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigAmbientValue, VALUE_STATE_IS_VALID);
				bAmbientOK = TRUE;
			}
		}	
	}
	else if(pSigAmbientSet->varValue.enumValue == 2)//Average
	{
		nCount = 0;
		fAvg  = 0.0;
		for(i = 0; i < NUM_OF_TEMPS; i++)
		{
			if(NULL == pSigTemps[i])
			{
				continue;
			}
			if(SIG_VALUE_IS_VALID(pSigTemps[i]) && SIG_VALUE_NEED_DISPLAY(pSigTemps[i]))
			{
				if(pSigTemps[i]->varValue.fValue > -140.0 && pSigTemps[i]->varValue.fValue < 150.0)
				{
					fAvg += pSigTemps[i]->varValue.fValue;
					nCount += 1;
				}
			}
		}
		if(nCount > 0)
		{
			fAvg /= nCount; 
			var_Temp.fValue = fAvg;
			if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigAmbientValue, &var_Temp, tmNow, FALSE))
			{
				SIG_STATE_SET(pSigAmbientValue, VALUE_STATE_IS_VALID);
				bAmbientOK = TRUE;
			}	
		}		
	}
	else if(pSigAmbientSet->varValue.enumValue >= 3 && pSigAmbientSet->varValue.enumValue < (NUM_OF_TEMPS+3))
	{
		i = (int)(pSigAmbientSet->varValue.enumValue) - 3;
 
		
		if(SIG_VALUE_IS_VALID(pSigTemps[i]) && SIG_VALUE_NEED_DISPLAY(pSigTemps[i]))
		{
			if(pSigTemps[i]->varValue.fValue > -140.0 && pSigTemps[i]->varValue.fValue < 150.0)
			{
				var_Temp.fValue = pSigTemps[i]->varValue.fValue;	
				if(Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigAmbientValue, &var_Temp, tmNow, FALSE))
				{
					SIG_STATE_SET(pSigAmbientValue, VALUE_STATE_IS_VALID);
					bAmbientOK = TRUE;
				}
			}
		}

	}
	if(!bAmbientOK )//&& nDelay >= 10)
	{
		var_Temp.fValue = INVALID_SIG_VALUE;
		Equip_SetVirtualSignalValue(pSite, pEquip, SIG_TYPE_SAMPLING, pSigAmbientValue, &var_Temp, tmNow, FALSE);
	}
}

static BOOL StdEquip_TempSig_ProcessSpecialSignals(IN SITE_INFO *pSite)
{
	static int nDelay = 0;
	if(nDelay < 10) //wait for 2 min after start
	{
		if(!nDelay)
		{
			Refresh_AllTemps(pSite);// Init all signals to not display.Only init once.
		}
		nDelay ++;		
		return FALSE;
	}	
	/*struct timeval* t_time;
	gettimeofday(t_time,NULL);
	printf("\nTemp_Sig_Process Start time: %ld",t_time->tv_usec);*/

	//Step 1: Refresh and update all sensor assigning settings


	GetAndJudge_AllSensorSettings(pSite);//��ȡ75·�¶���Щ������Ϊ�����ڻ����¶ȣ���Щ���ڵ���¶ȡ�

	Refresh_Temp_SNMP_AlmStatus(pSite);

	//Step 2: Get sensor and assign value to its speicified device
	Refresh_AllTemps(pSite);//����g_Sensor_Settings[75]���Ѳɼ�ͨ����Ӧ���¶�ֵ����-1/-3�źŶ�Ӧ�Ļ����¶Ȼ����¶�ֵ

	//Step 3: Calculate and update comp temp value(for GC) and ambient Temp(also for GC)
	Refresh_Comp_Temp(pSite);
	
	Refresh_Comp_Enable_Use_Sensor_Fail(pSite);//HPM CR

	//added by Jimmy 201202 as ESNA Brian suggests
	Refresh_BTRM_Temp(pSite);

	Refresh_AAmbient_Temp(pSite);

	//gettimeofday(t_time,NULL);
	//printf("\nTemp_Sig_Process End time: %ld",t_time->tv_usec);

	//Step 4: Update batt temp alarm status and ambient alarm status(also for GC)
	//Refresh_TempAlarm(pSite);

	return TRUE;
}

//-------------END OF MODIFYING by Jimmy Wu------------------------------------
/*==========================================================================*
 * FUNCTION : StdEquip_ProcessLVD3Signals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)          DATE: 2005-01-27 16:29
 *==========================================================================*/
static BOOL StdEquip_ProcessLVD3Signals(IN SITE_INFO *pSite)
{
	int		i;
	int 	iSigID=0;
	static EQUIP_INFO		*pIB2_1Equip = NULL;
	static BOOL			bInited = FALSE;
	static EQUIP_INFO		*pSystemEquip = NULL;
	static EQUIP_INFO		*pLVD3Equip = NULL;
	static EQUIP_INFO		*pLVDGroupEquip = NULL;

	static SIG_BASIC_VALUE *pSig_LVD3EnableStatus = NULL;
	static SIG_BASIC_VALUE *pSig_LVD3RelayNO = NULL;
	static SIG_BASIC_VALUE *pSig_LVD3Control = NULL;
	static SIG_BASIC_VALUE *pSig_LVD3Sample = NULL;
	static SIG_BASIC_VALUE *pSig_RelayStatus = NULL;

	time_t	tmNow = time(NULL);
	BOOL	bChanged = FALSE;
	VAR_VALUE var;
	
	static int sRelaySigID[12] = {13,14,15,16,17,18,19,20,22,23,24,25};
	//1. init one time
	if (!bInited)
	{
		bInited = TRUE;
		//1.1.Get Power system and IB2-1 equip address
		pSystemEquip = Init_GetEquipRefByStdId(pSite, STD_ID_ACU_SYSTEM_EQUIP);
		if (pSystemEquip  == NULL)
		{
			AppLogOut(EQP_MGR, APP_LOG_WARNING,
				"Fails on get equip ref by SCU Plus Std equipment ID %d.\n",
				STD_ID_ACU_SYSTEM_EQUIP);
			return FALSE;
		}

		pIB2_1Equip = Init_GetEquipRefByStdId(pSite, 801);
		if (pIB2_1Equip  == NULL)
		{
			AppLogOut(EQP_MGR, APP_LOG_WARNING,
				"Fails on get equip ref by SCU Plus Std equipment ID %d.\n",
				801);
			return FALSE;
		}		
		
		//1.2.Get LVD Group equip address
		pLVDGroupEquip = Init_GetEquipRefById(pSite, LVD_GROUP_EQUIP_ID);
		if (pLVDGroupEquip  == NULL)
		{
			AppLogOut(EQP_MGR, APP_LOG_WARNING,
				"Fails on get equip ref by SCU Plus Std equipment ID %d.\n",
				LVD_GROUP_EQUIP_ID);
			return FALSE;
		}
		//1.3.Get LVD3 equip address
		pLVD3Equip = Init_GetEquipRefById(pSite, LVD3_EQUIP_ID);
		if (pLVD3Equip  == NULL)
		{
			AppLogOut(EQP_MGR, APP_LOG_WARNING,
				"Fails on get equip ref by SCU Plus Std equipment ID %d.\n",
				LVD3_EQUIP_ID);
			return FALSE;
		}
		
		//1.4. init get LVD3 value address 
		pSig_LVD3EnableStatus = Equip_GetSpecialSigByMergedId(pLVDGroupEquip,
			SIG_ID_LVD3_ENABLE);
		
		//1.5 Get Relay Number signal value address
		pSig_LVD3RelayNO = Equip_GetSpecialSigByMergedId(pLVDGroupEquip, 
			SIG_ID_LVD3_RELAY_NO);
		
		//1.6 Get LVD3 Control signal value address
		pSig_LVD3Control = Equip_GetSpecialSigByMergedId(pLVD3Equip, 
			SIG_ID_LVD3_CONTROL_ID);

		pSig_LVD3Sample = Equip_GetSpecialSigByMergedId(pLVD3Equip, 
			SIG_ID_LVD3_SAMPLE_ID);
	}


	
	//2.3 
	if (pSig_LVD3EnableStatus != NULL && pSig_LVD3EnableStatus->varValue.enumValue)//Enable
	{
		//printf("pSig_LVD3EnableStatus->varValue.enumValue = %d %d\n",pSig_LVD3RelayNO->varValue.enumValue, pSig_LVD3EnableStatus->varValue.enumValue);
		//1.7 Get system interal status signal value address

		iSigID	= sRelaySigID[pSig_LVD3RelayNO->varValue.enumValue];

		if( (iSigID >= 22) && (iSigID <= 25))
		{		
			pSig_RelayStatus = Equip_GetSpecialSigByMergedId(pSystemEquip, DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, iSigID));
		}
		else if( (iSigID >= 13) && (iSigID <= 20))
		{
			iSigID = iSigID -12;//12 is the offset of system control sig ID and IB2-1 control sig ID
			pSig_RelayStatus = Equip_GetSpecialSigByMergedId(pIB2_1Equip, DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, iSigID));
		}
		else
		{
			pSig_RelayStatus = NULL;
		}
		

		if(pSig_RelayStatus != NULL)
		{

			VAR_SET_VALUE(&var, pSig_LVD3Control->ucType, pSig_RelayStatus->varValue.enumValue);

			if (Equip_SetVirtualSignalValue(pSite, pLVD3Equip,
					DXI_SPLIT_SIG_MID_TYPE(SIG_ID_LVD3_CONTROL_ID), pSig_LVD3Control,
					&var, tmNow, FALSE) > 0)
			{
				bChanged = TRUE;
			}
		
			if (Equip_SetVirtualSignalValue(pSite, pLVD3Equip,
					DXI_SPLIT_SIG_MID_TYPE(SIG_ID_LVD3_SAMPLE_ID), pSig_LVD3Sample,
					&var, tmNow, FALSE) > 0)
			{
				bChanged = TRUE;
			}

		}
		
	}

	


	// do notify equipment to process data
	if (bChanged)
	{
		pSystemEquip->iProcessingState = EQP_STATE_PROCESS_DOING;
	}

	return bChanged;
}

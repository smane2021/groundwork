/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_data_sample.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 16:52
 *  VERSION  : V1.00
 *  PURPOSE  : sample data from each port
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <dlfcn.h>

#include "stdsys.h"
#include "public.h"
#include "../../sampler/sampler_base/local_linux.h"

#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "cfg_model.h"
#include "equip_mon.h"
#include "data_mgmt.h"

typedef struct _PARAM_SAMPLING_TASK 
{
	SITE_INFO	*pSite;
	PORT_INFO	*pPort;
} PARAM_SAMPLING_TASK;

static DWORD Sample_DataSamplingTask(IN PARAM_SAMPLING_TASK *pParam);
static DWORD Sample_DataSamplingTask_VirtualPort(IN PARAM_SAMPLING_TASK *pParam);

static SIG_BASIC_VALUE *Sample_GetSpecialSigByMergedId(IN EQUIP_INFO *pEquip, 
                                                       IN int nMergedId);
//static BOOL Sample_ClearSigConfigflage(IN EQUIP_INFO *pEquip);


/*==========================================================================*
 * FUNCTION : Sample_CreateSamplingTasks
 * PURPOSE  : create the sampling task for each port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 16:59
 *==========================================================================*/
BOOL Sample_CreateSamplingTasks(IN SITE_INFO *pSite)
{
	int					i;
	BOOL				bExist485 = FALSE;
	PORT_INFO			*pPort;
	PARAM_SAMPLING_TASK	*pTaskParam;

	AppLogOut(EQP_SMPLR, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Sampling tasks are being created...\n");
	
	//1.Check Initial Info
	if(pSite->bFirstRunning)
	{	
		pSite->bFirstRunning = FALSE;
	}

	pPort = pSite->pPortInfo;
	for (i = 0; i < pSite->iPortNum; i++, pPort++)
	{
		//if(i == 3)	continue;
		
		// if no attached sampler, do NOT create sampling task for port
		if (pPort->nAttachedSamplers > 0)
		{
			AppLogOut(EQP_SMPLR, APP_LOG_UNUSED, 
				"Creating sampling task for port %s(%d)...\n",
				EQP_GET_PORT_NAME(pPort), pPort->iPortID);

			// init the creating params
			pTaskParam = NEW(PARAM_SAMPLING_TASK, 1);
			if (pTaskParam == NULL)
			{
				AppLogOut(EQP_SMPLR, APP_LOG_ERROR, 
					"Out of memory on allocating params for sampling task "
					"of port \"%s\"(%d).\n",
					EQP_GET_PORT_NAME(pPort),
					pPort->iPortID);

				return FALSE;
			}

			pTaskParam->pSite = pSite;
			pTaskParam->pPort = pPort;

			// create the task.
			pPort->hSampleThread = RunThread_Create(pPort->szPortName,
					(RUN_THREAD_START_PROC)Sample_DataSamplingTask,
					(void *)pTaskParam,
					NULL,
					0);				

			AppLogOut(EQP_SMPLR, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
				"Samp-task for P[%s:%d] is %s created.\n",
				EQP_GET_PORT_NAME(pPort), pPort->iPortID,
				(pPort->hSampleThread == NULL) ? "NOT" : "successfully");

			if (pPort->hSampleThread == NULL)
			{
				DELETE(pTaskParam);
				return FALSE;
			}
			
			if(strstr(EQP_GET_PORT_NAME(pPort), "RS485_PORT"))
			{
				bExist485 = TRUE;
			}
			Sleep(50);
		}
	}

	if(!bExist485)
	{
		RUN_THREAD_MSG msgFindEquip;
		RUN_THREAD_MAKE_MSG(&msgFindEquip, NULL, MSG_485_SAMPLER_FINISH_SCAN, 0, 0);
		RunThread_PostMessage(-1, &msgFindEquip, FALSE);
		//printf("MSG_485_SAMPLER_FINISH_SCAN: -----------Send Message thru sample Tasks.------------------\n");
	}

	AppLogOut(EQP_SMPLR, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Sampling tasks are created OK.\n");

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Sample_StopSamplingTasks
 * PURPOSE  : stop the sampling task of each sampling port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : optimized, copied from equip_mon_main.c:EquipMonitoring_Stop
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-14 15:28
 *==========================================================================*/
BOOL Sample_StopSamplingTasks(IN OUT SITE_INFO *pSite)
{
	int			i, rc;
	PORT_INFO	*pPort;

	AppLogOut(EQP_SMPLR, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Sampling tasks are being stopped...\n");

	// 2.1 post quit message to all port thread at first
	for (i = 0, pPort = pSite->pPortInfo; i < pSite->iPortNum; i++, pPort++)
	{
		if (pPort->hSampleThread != NULL)
		{
			AppLogOut(EQP_SMPLR, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
				"Sampling task of port %s(%d) is being stopped...\n",
				pPort->szPortName,
				pPort->iPortID);

			RunThread_PostQuitMessage(pPort->hSampleThread);
		}
	}

	// 2.2 wait for the sampling thread to quit.
	for (i = 0, pPort = pSite->pPortInfo; i < pSite->iPortNum; i++, pPort++)
	{
		if (pPort->hSampleThread != NULL)
		{
			rc = RunThread_Stop(pPort->hSampleThread,
				RUN_THREAD_HEARTBEAT_CHECK_INTERVAL,
				TRUE);

			AppLogOut(EQP_SMPLR,
				(rc != ERR_THREAD_KILLED) ? APP_LOG_UNUSED : APP_LOG_WARNING, 
				"Sampling task of port %s(%d) is stopped %s.\n",
				pPort->szPortName,
				pPort->iPortID,
				(rc != ERR_THREAD_KILLED) ? "normally" : "forcibly, killed");
		}
	}


	AppLogOut(EQP_SMPLR, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Sampling tasks have been stopped.\n");

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Sample_EnumChannelData
 * PURPOSE  : enum channel data one by one if out of memory on calling 
 *            Sample_InitSamplerDataBuffer to init sampling buffer in 
 *            Sample_ReadData().
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int               iChanelNo : 
 *            IN float             fValue    : 
 *            IN OUT SAMPLER_INFO  *pSampler : 
 * RETURN   : static BOOL : 
 * COMMENTS : I hope this function will only be called once.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-13 15:12
 *==========================================================================*/
static BOOL Sample_EnumChannelData(IN int iChanelNo,
	IN float	fValue,								 
	IN OUT SAMPLER_INFO *pSampler)
{
	//iChanelNo &= 0x7fff;	// compliance for psms  //Deleted on July 7,2006

	//TRACEX("Enuming channel %d, value %f.\n", iChanelNo, fValue);

	if (iChanelNo >= 0)
	{
		// to cacl the max channel num.
#if (COMM_STATUS_CHANNEL != 0)	// the communication status channel is always 0
		iChanelNo += 1;			// to keep chn 0 for comm. status
#endif
		if (iChanelNo < pSampler->nMaxUsedChannel)
		{
			// start from 1. 0 is comm status
			pSampler->pChannelData[iChanelNo] = fValue;

		}
	
		if (iChanelNo >= pSampler->nMaxQueriedChannel)
		{
			pSampler->nMaxQueriedChannel = iChanelNo+1;
		}

	}

	return TRUE;
}


#ifdef _USING_READ_METHOD
/*==========================================================================*
* FUNCTION : Sample_ReallocateChannelBuffer
* PURPOSE  : re-allocate memory for the sampler buffer
*            the buffer will be passed to pfnRead to read data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN PORT_INFO         *pPort    : 
*            IN OUT SAMPLER_INFO  *pSampler : 
* RETURN   : static BOOL : FALSE, failure on query, TRUE: for OK
* COMMENTS : 
* CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-05 20:04
*==========================================================================*/
static BOOL Sample_ReallocateChannelBuffer(IN OUT SAMPLER_INFO *pSampler)
{
	float *pNewBuffer;

	if (pSampler->nMaxQueriedChannel <= pSampler->nMaxUsedChannel)
	{
		return TRUE;
	}

	// some of channel is not used. but the sampling buffer
	// shall be large enough to save data. need allocate a new buffer.
	pNewBuffer = RENEW(float, pSampler->pChannelData,
		pSampler->nMaxQueriedChannel);	// do NOT add 1. 1 is already added to.
	if (pNewBuffer == NULL)
	{
		TRACEX("Out of memory on reallocating the channel data buffer(chn=%d) "
			"for sampler \"%s\"(%d).\n",
			pSampler->nMaxQueriedChannel,
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID);

		return FALSE;
	}

	memset(&pNewBuffer[pSampler->nMaxUsedChannel], 0xff, 
		(pSampler->nMaxQueriedChannel - pSampler->nMaxUsedChannel)*sizeof(float));

	// set new buffer.
	pSampler->pChannelData = pNewBuffer;

	return TRUE;
}
#endif //_USING_READ_METHOD
union unSAMPLING_VALUE
{
	float	fValue;
	int	iValue;
	UINT	uiValue;
	ULONG	ulValue;
	DWORD	dwValue;
};
typedef union unSAMPLING_VALUE SAMPLING_VALUE;
static SAMPLING_VALUE NoValid;
static SAMPLING_VALUE Digit_1;
static SAMPLING_VALUE Digit_0;

/*==========================================================================*
 * FUNCTION : Sample_SetCommStatus
 * PURPOSE  : set the comm status of the sampler.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PORT_INFO         *pPort      : 
 *            IN OUT SAMPLER_INFO  *pSampler   : 
 *            IN BOOL              bCommStatus : 
 * RETURN   : static BOOL : the current comm status after setting.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-19 15:34
 *==========================================================================*/
static BOOL Sample_SetCommStatus(IN PORT_INFO *pPort,
						  IN OUT SAMPLER_INFO *pSampler,
						  IN BOOL bCommStatus)
{
	Digit_1.iValue	= 1;
	Digit_0.iValue  = 0;
	NoValid.iValue  = 2;

	double		tmNow        = GetCurrentTime();	// the time data sampled
	
	// the time changing is NOT considered.
	pSampler->fElapsedTime = (tmNow - pSampler->tmDataSampled);
	pSampler->tmSampleUsed = (tmNow - pSampler->tmSampleStarted);

	pSampler->tmDataSampled = tmNow;

	// if comm. failure time exceed the maximum allowed time, 
	// set the comm status to failure.

	if (bCommStatus)
	{
		pSampler->bCommStatus = TRUE;	//OK
		pSampler->nCommErrors = 0;		// clear error count
	}
	else
	{
		pSampler->nCommErrors++;	// over max int? impossible! 
		
		if (pSampler->nCommErrors != COMM_READ_RETRY_TIME)
		{
			// the comm. is already failure. or
			// give another chance to sample data.
			return pSampler->bCommStatus;
		}
	
		pSampler->bCommStatus = FALSE;	// comm failure.
	}

	// set comm status and the time data sampled
	// 0 is normal and 1 is failure for comm. status in channel data buffer.
	pSampler->pChannelData[0] = (float)(pSampler->bCommStatus 
		? Digit_0.fValue								// 0: comm OK
		: (pPort->hPort != NULL) ? Digit_1.fValue	// 1: comm failure with sampler unit.
		: NoValid.fValue);

	pSampler->bDataChanged = TRUE;

	return pSampler->bCommStatus;
}



#define PORT_NEED_NOT_REOPEN(pPort, pSampler)	\
	(strcmp((pSampler)->szPortSetting, (pPort)->pCurSampler->szPortSetting) == 0)

#define   ADC_BOARD_SAMPLE_PORT_ID     2
#define   COMMUCATION_PORT1_ID         1   //COMMUNICATION_PORT_INFO
/*==========================================================================*
 * FUNCTION : Sample_ReopenPort
 * PURPOSE  : test need re-open port or not. if need, to open the port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT PORT_INFO  *pPort        : 
 *            IN SAMPLER_INFO   *pSampler     : the current sampler to sample
 * RETURN   : BOOL : TRUE for OK.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-08 11:23
 *==========================================================================*/
BOOL Sample_ReopenPort(IN OUT PORT_INFO *pPort, IN SAMPLER_INFO *pSampler)
{
	int		nErrCode = ERR_OK;

	RunThread_Heartbeat(pPort->hSampleThread);


	// the port has been opened.
	if (pPort->hPort != NULL)
	{
		//1.but the sampler setting is not changed.
		//2.Another condition:if the port is open for a long time and reset reopen
		 if (PORT_NEED_NOT_REOPEN(pPort, pSampler))
		{
			// set the current sampler.
			pPort->pCurSampler = pSampler;
			return TRUE;	// the port need not to be reopened.
		}

		// the port need to be reopened, close it at first
		 CommClose(pPort->hPort);
	 }
	
	// set the current sampler.
	pPort->pCurSampler = pSampler;

	// the port is not opened, open it now
	pPort->hPort = CommOpen(pPort->pStdPort->szPortAccessingDriver,
		pPort->szDeviceDesp,
		pSampler->szPortSetting,	// use the port setting of the sampler.
		COMM_CLIENT_MODE,
		pPort->iTimeout,
		&nErrCode);

	RunThread_Heartbeat(pPort->hSampleThread);

	if (pPort->hPort == NULL)
	{
		// log error msg at first failure.
		if (pSampler->nCommErrors == 0)
		{
			AppLogOut( EQP_SMPLR, APP_LOG_ERROR,
				"Fails on opening port \"%s\"(%d) for sampler \"%s\"(%d) with "
				"settings %s got error code %08x.\n",
				EQP_GET_PORT_NAME(pPort),
				pPort->iPortID,
				EQP_GET_SAMPLER_NAME0(pSampler),
				pSampler->iSamplerID,
				pSampler->szPortSetting,
				nErrCode);
		}

		// set the current comm. status.
		Sample_SetCommStatus(pPort, pSampler, FALSE);

		return FALSE;
	}
	
	return TRUE;	//OK.
}

#ifdef _DEBUG_DATA_SAMPLING

//#define _DEBUG_SAMPLER_ADDR	1

#ifdef _DEBUG_SAMPLER_ADDR
// code to use reading addr
#include <dlfcn.h>		// for dl*()

typedef BOOL (*SAMPLER_GET_ADDR_PROC)(HANDLE hComm, int CID1, int *ADR);

static BOOL Sample_GetSamplerAddr(IN PORT_INFO *pPort,
											 IN SAMPLER_INFO *pSampler)
{
	// we can get address from sampler using YDN32 protocol..
	SAMPLER_GET_ADDR_PROC pfnDebugGetSamplerAddr;
	int		ADDR, CID1;
	int		nStdSamplerID = pSampler->pStdSampler->iStdSamplerID;

	CID1 = (nStdSamplerID == 2) ? 0xE4 	// SCU
		 : (nStdSamplerID == 3) ? 0xD3 	// SM_AC
		 : (nStdSamplerID == 4) ? 0xD2 	// SM_BAT
		 : (nStdSamplerID == 5) ? 0xD1	// SM_IO
		 : 0;							// UNKOWN

	pfnDebugGetSamplerAddr = (SAMPLER_GET_ADDR_PROC)dlsym(
		pSampler->pStdSampler->hSamplerLib,
		"GetAddr");
	if (pfnDebugGetSamplerAddr == NULL)
	{
		return FALSE;
	}

	if (!pfnDebugGetSamplerAddr(pPort->hPort, CID1, &ADDR))
	{
		return FALSE;
	}

	TRACEX("Sampler %s(ID=%d, CID1=%02x): ADDR=%d.\n",
		EQP_GET_SAMPLER_NAME0(pSampler),
		pSampler->iSamplerID,
		CID1,
		ADDR);

	if (ADDR != pSampler->iSamplerAddr)
	{
		AppLogOut(EQP_SMPLR, APP_LOG_WARNING, 
			"The configured adddr %d of the sampler \"%s\"(%d) is wrong, "
			"the actual addr is %d.\n",
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID,
			pSampler->iSamplerAddr,
			ADDR);
	}

	return TRUE;
}

#endif	//_DEBUG_SAMPLER_ADDR

#endif //_DEBUG_DATA_SAMPLING

#ifdef PRODUCT_INFO_SUPPORT
/*==========================================================================*
 * FUNCTION : Sample_GetProductInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PORT_INFO     *pPort    : 
 *            IN SAMPLER_INFO  *pSampler : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-21
 *==========================================================================*/
typedef BOOL (*SAMPLER_GET_PI_PROC)(HANDLE hComm, int nUnitNo, void *pPI);

static BOOL Sample_GetProductInfo(IN PORT_INFO *pPort,
								  IN SAMPLER_INFO *pSampler)
{
	SAMPLER_GET_PI_PROC pfnGetPIProc;
	int		i=0;
	int		j=0;
	int		k=0;
	int		l=0;
	int		m=0;
	int		n=0;
	int		iConverterNo = 0;
	int		iSMTempNo = 0;
	int		iLiBattNo = 0;
	int		iRectNo = 0, iSMNo = 0, iSMDUHNo = 0, iDCEMNo = 0,iSMDUENo = 0;
#ifdef GC_SUPPORT_MPPT
	int		iMpptNo = 0;
#endif


	pfnGetPIProc = (SAMPLER_GET_PI_PROC)dlsym(
		pSampler->pStdSampler->hSamplerLib,
		"GetProductInfo");
	if (pfnGetPIProc == NULL)
	{
		AppLogOut( EQP_SMPLR, APP_LOG_WARNING, "Fails on load GetProductInfo "
			"symbol of the sampler lib: %s.",pSampler->pStdSampler->szStdSamplerDriver );

		return TRUE;
	}

	if(pSampler->iSamplerID == 1 )
	{
		for( i = 0; i < pSampler->nRelatedEquipment;i++)
		{
			if(pSampler->ppRelatedEquipment[i]->iEquipTypeID >= 1901 && pSampler->ppRelatedEquipment[i]->iEquipTypeID <= 1920)
			{
				strncpyz(g_SiteInfo.stCANSMDUPProductInfo[l].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANSMDUPProductInfo[l].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANSMDUPProductInfo[l].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANSMDUPProductInfo[l].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANSMDUPProductInfo[l].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANSMDUPProductInfo[l].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANSMDUPProductInfo[l].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANSMDUPProductInfo[l].szDeviceAbbrName[1]));

				// get the product info
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANSMDUPProductInfo[l]));
				l++;
			}
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID >= 2201 && pSampler->ppRelatedEquipment[i]->iEquipTypeID <= 2208) //SM Temp PI, added by Jimmy 2011/10/20 ,edited 2011/11/24 'cause divided by 8 config files
			{
				strncpyz(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo].szDeviceAbbrName[1]));

				// get the product info				
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANSMTempProductInfo[iSMTempNo]));
				iSMTempNo++;
			}
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1101) 
			{
				strncpyz(g_SiteInfo.stCANConverterProductInfo[iConverterNo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANConverterProductInfo[iConverterNo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANConverterProductInfo[iConverterNo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANConverterProductInfo[iConverterNo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANConverterProductInfo[iConverterNo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANConverterProductInfo[iConverterNo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANConverterProductInfo[iConverterNo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANConverterProductInfo[iConverterNo].szDeviceAbbrName[1]));

				// get the product info
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANConverterProductInfo[iConverterNo]));
				iConverterNo++;
			}
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID == 309) //added by Jimmy 2012/04/01 for Li Batt CR
			{
				strncpyz(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo].szDeviceAbbrName[1]));

				// get the product info
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANLiBattProductInfo[iLiBattNo]));
				iLiBattNo++;
			}
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID == 308) //added by Jimmy 2012/06/25 BridgeCard����Ϣ
			{
				strncpyz(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceAbbrName[1]));

				// get the product info
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANLiBridgeCardProductInfo));

			}
#ifdef GC_SUPPORT_MPPT
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID == 2501) //for Mppt
			{
				strncpyz(g_SiteInfo.stCANMpptProductInfo[iMpptNo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANMpptProductInfo[iMpptNo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANMpptProductInfo[iMpptNo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANMpptProductInfo[iMpptNo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANMpptProductInfo[iMpptNo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANMpptProductInfo[iMpptNo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANMpptProductInfo[iMpptNo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANMpptProductInfo[iMpptNo].szDeviceAbbrName[1]));

				// get the product info
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANMpptProductInfo[iMpptNo]));
				iMpptNo++;

			}
#endif
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID == 201)
			{
				strncpyz(g_SiteInfo.stCANRectProductInfo[iRectNo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANRectProductInfo[iRectNo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANRectProductInfo[iRectNo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANRectProductInfo[iRectNo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANRectProductInfo[iRectNo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANRectProductInfo[iRectNo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANRectProductInfo[iRectNo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANRectProductInfo[iRectNo].szDeviceAbbrName[1]));

				// get the product info
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANRectProductInfo[iRectNo]));
				iRectNo++;
			}
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID >= 1001 && pSampler->ppRelatedEquipment[i]->iEquipTypeID <= 1008) //SM DU
			{
				strncpyz(g_SiteInfo.stCANSMDUProductInfo[iSMNo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANSMDUProductInfo[iSMNo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANSMDUProductInfo[iSMNo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANSMDUProductInfo[iSMNo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANSMDUProductInfo[iSMNo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANSMDUProductInfo[iSMNo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANSMDUProductInfo[iSMNo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANSMDUProductInfo[iSMNo].szDeviceAbbrName[1]));

				// get the product info				
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANSMDUProductInfo[iSMNo]));
				iSMNo++;
			}
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID >= 2701 && pSampler->ppRelatedEquipment[i]->iEquipTypeID <= 2708) //SM DUH
			{
				strncpyz(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo].szDeviceAbbrName[1]));

				// get the product info				
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANSMDUHProductInfo[iSMDUHNo]));
				iSMDUHNo++;
			}
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID >= 3001 && pSampler->ppRelatedEquipment[i]->iEquipTypeID <= 3032) 
			{
				strncpyz(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo].szDeviceAbbrName[1]));
					
				// get the product info				
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANSMDUEProductInfo[iSMDUENo]));
					
				iSMDUENo++;
			}
			else if(pSampler->ppRelatedEquipment[i]->iEquipTypeID == 2401)
			{
				strncpyz(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo].szDeviceName[0]));

				strncpyz(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo].szDeviceName[1]));

				strncpyz(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo].szDeviceAbbrName[1]));

				// get the product info				
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.stCANDCEMProductInfo[iDCEMNo]));
				iDCEMNo++;
			}
		}

	}
	else if(pSampler->iSamplerID == 2 )
	{
		for( i = 0; i < pSampler->nRelatedEquipment;i++)
		{
			if((pSampler->ppRelatedEquipment[i]->iEquipTypeID >= 901 && pSampler->ppRelatedEquipment[i]->iEquipTypeID<=904 ) || 
				( pSampler->ppRelatedEquipment[i]->iEquipTypeID >= 801 && pSampler->ppRelatedEquipment[i]->iEquipTypeID<=804))
			{
				strncpyz(g_SiteInfo.stI2CProductInfo[j].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.stI2CProductInfo[j].szDeviceName[0]));

				strncpyz(g_SiteInfo.stI2CProductInfo[j].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.stI2CProductInfo[j].szDeviceName[1]));

				strncpyz(g_SiteInfo.stI2CProductInfo[j].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.stI2CProductInfo[j].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.stI2CProductInfo[j].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.stI2CProductInfo[j].szDeviceAbbrName[1]));

				// get the product info
				pfnGetPIProc(pPort->hPort,    
							pSampler->ppRelatedEquipment[i]->iEquipID,
							&(g_SiteInfo.stI2CProductInfo[j]));
				j++;
			}

		}

	}
	else if(pSampler->iSamplerID == 4 )
	{
		for( i = 0; i < pSampler->nRelatedEquipment;i++)
		{
			if(pSampler->ppRelatedEquipment[i]->iEquipTypeID == 703 
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1201
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1202
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1203
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1204
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1205
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1206
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1207
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1208
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 304
				//Frank Wu,20150318, for SoNick Battery
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 310
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1301
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1401
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1600
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1700
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 1800
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 2001
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 2100
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 2801 
				//Frank Wu,20150318, for Narada BMS
				|| pSampler->ppRelatedEquipment[i]->iEquipTypeID == 2901 
				)
			{
			    
			    strncpyz(g_SiteInfo.st485ProductInfo[k].szDeviceName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[0],
					sizeof(g_SiteInfo.st485ProductInfo[k].szDeviceName[0]));

				strncpyz(g_SiteInfo.st485ProductInfo[k].szDeviceName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pFullName[1],
					sizeof(g_SiteInfo.st485ProductInfo[k].szDeviceName[1]));

				strncpyz(g_SiteInfo.st485ProductInfo[k].szDeviceAbbrName[0], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[0],
					sizeof(g_SiteInfo.st485ProductInfo[k].szDeviceAbbrName[0]));

				strncpyz(g_SiteInfo.st485ProductInfo[k].szDeviceAbbrName[1], 
					pSampler->ppRelatedEquipment[i]->pEquipName->pAbbrName[1],
					sizeof(g_SiteInfo.st485ProductInfo[k].szDeviceAbbrName[1]));

				// get the product info
				pfnGetPIProc(pPort->hPort,    
					pSampler->ppRelatedEquipment[i]->iEquipID,
					&(g_SiteInfo.st485ProductInfo[k]));
				
				k++;
			}
		
		}
		return TRUE;

	}
	else
	{
		//Set the Device Name based on Sampler Name, Thomas, 2006-2-14
		strncpyz(pSampler->stProductInfo.szDeviceName[0], 
			pSampler->pSamplerName->pFullName[0],
			sizeof(pSampler->stProductInfo.szDeviceName[0]));
		strncpyz(pSampler->stProductInfo.szDeviceName[1], 
			pSampler->pSamplerName->pFullName[1],
			sizeof(pSampler->stProductInfo.szDeviceName[1]));

	
		// get the product info
		return pfnGetPIProc(pPort->hPort,    
			pSampler->iSamplerAddr, 
			&(pSampler->stProductInfo));

	}
	
}
#endif //PRODUCT_INFO_SUPPORT



/*==========================================================================*
 * FUNCTION : Sample_ReadData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PORT_INFO         *pPort    : 
 *            IN OUT SAMPLER_INFO  *pSampler : 
 * RETURN   : static BOOL : comm status
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-20 17:08
 *==========================================================================*/
static BOOL Sample_ReadData(IN PORT_INFO *pPort,
						  IN OUT SAMPLER_INFO *pSampler)
{
	static int iTimeFlage = 0;
	BOOL	bCommStatus;

	


#ifdef _USING_READ_METHOD
	if (pSampler->bCallingRead) 
	{
		// OK. let's sample data now
		bCommStatus = pSampler->pStdSampler->pfnRead(
			pPort->hPort,			/* port handle		*/
			pSampler->iSamplerAddr,	/* sampler unit ID	*/
			&pSampler->pChannelData[1]);	// the Data[0] is for comm. status
	}
	else
#endif //_USING_READ_METHOD
	{
		// use the query method to sampling data
		pSampler->nMaxQueriedChannel = 1;	// at least has the comm state!
		bCommStatus = pSampler->pStdSampler->pfnQuery(
			pPort->hPort,			/* port handle */
			pSampler->iSamplerAddr,	/* sampler unit ID */
			(SAMPLER_ENUM_PROC)Sample_EnumChannelData,
			(void *)pSampler);


#ifdef _USING_READ_METHOD
		if (bCommStatus)
		{
			if (pSampler->nMaxQueriedChannel < pSampler->nMaxUsedChannel)
			{
				pSampler->bCallingRead = TRUE;
			}
			else if (pSampler->nMaxQueriedChannel > pSampler->nMaxUsedChannel)
			{
				// trying to re-init pChannelData buffer. 
				// due the pre-allocated pChannelData is too small to hold all
				// channel data
				pSampler->bCallingRead =
					Sample_ReallocateChannelBuffer(pSampler);
			}
		}
#endif //_USING_READ_METHOD
	}

	RunThread_Heartbeat(pPort->hSampleThread);

#ifdef PRODUCT_INFO_SUPPORT
	//bQueryPI is set to 0 when the stru has been initialized

	if (!pSampler->bQueryPI)  
	{


		if(Sample_GetProductInfo(pPort, pSampler))
		{
			pSampler->nQueryPIErrors = 0;
			pSampler->bQueryPI = TRUE;
		}
		else
		{
			//using the err count in lest too long time cost when comm is lost	
			pSampler->nQueryPIErrors++;
			if (pSampler->nQueryPIErrors > COMM_READ_RETRY_TIME + 1)
			{
				pSampler->bQueryPI = TRUE;
			}
		}


		

	}

	if(pSampler->iSamplerID == 1)
	{
		pSampler->bQueryPI = FALSE;
	}



#endif //PRODUCT_INFO_SUPPORT


	RunThread_Heartbeat(pPort->hSampleThread);

#ifdef PRODUCT_INFO_SUPPORT
	/* when communication is recoved for first time, try query product info next time */
	if (bCommStatus && !pSampler->bCommStatus)
	{
		pSampler->bQueryPI = FALSE;
	}
#endif //PRODUCT_INFO_SUPPORT


	// if comm. failure time exceed the maximum allowed time, 
	// set the comm status to failure.
	Sample_SetCommStatus(pPort, pSampler, bCommStatus);
	
	return bCommStatus;
}





/*==========================================================================*
 * FUNCTION : Sample_DispatchData
 * PURPOSE  : dispacth the data just sampled to the related equipment.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO     *pSite    : 
 *            IN SAMPLER_INFO  *pSampler : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-07 18:54
 *==========================================================================*/
static BOOL Sample_DispatchData(IN SITE_INFO *pSite, IN SAMPLER_INFO *pSampler)
{
	EQUIP_PROCESS_CMD	cmd;
	int					i;
	SIG_BASIC_VALUE		*pSigVal;
	float				*pChnData;
	EQUIP_INFO			*pEquip;
	time_t				tmDataSampled;


	tmDataSampled = (time_t)pSampler->tmDataSampled;
	pChnData      = pSampler->pChannelData;

	// data read ok. distatch the data
	if (SAMPLER_COMM_OK(pSampler))
	{
		int	nChannel;
		for (i = 0; i < pSampler->nSigValues; i++)
		{
			// the sig val of this channel nChannel
			pSigVal = pSampler->ppSigValues[i];
			//multi signal use one channel, i != nChannel
			nChannel = ((SAMPLE_SIG_VALUE *)pSigVal)->sv.iSamplerChannel;

            if ((nChannel < pSampler->nMaxQueriedChannel) 
				&& (*(int *)&pChnData[nChannel] != SAMPLER_CHN_NOT_CFG_VAL))
				//&& (FLOAT_NOT_EQUAL(pChnData[nChannel], SAMPLER_CHN_NOT_CFG_VAL)))
			{
				if( *(int *)&pChnData[nChannel] == SAMPLER_INVALID_VAL )//Added by wj for Invalid Signal
				{
					// set raw is invalid
					SIG_STATE_CLR(pSigVal, VALUE_STATE_IS_VALID_RAW);
					
				}
				else 
				{
					// use copy method, to avoid type converting lost.
					*(int *)&pSigVal->fRawData = *(int *)&pChnData[nChannel];
					pSigVal->tmCurrentSampled  = tmDataSampled;

					// set raw is valid
					SIG_STATE_SET(pSigVal, VALUE_STATE_IS_VALID_RAW);

					//dynamically configure process, added by Thomas, 2005-12
					SIG_STATE_SET(pSigVal, VALUE_STATE_IS_CONFIGURED);

				}
				
			}
			else
			{
				// set raw is invalid
				SIG_STATE_CLR(pSigVal, VALUE_STATE_IS_VALID_RAW);

				//dynamically configure process, added by Thomas, 2005-12
				SIG_STATE_CLR(pSigVal, VALUE_STATE_IS_CONFIGURED);
			}
		}
	}

	else if (SAMPLER_JUST_FAILED(pSampler))	// comm just failed.		
	{
		// channel 0 is for comm status. that always is valid.
		for (i = 0; i < pSampler->nSigValues; i++)
		{
			// the sig val of this channel nChannel
			pSigVal = pSampler->ppSigValues[i];
			 // the comm status channel
			if (((SAMPLE_SIG_VALUE *)pSigVal)->sv.iSamplerChannel == 0)
			{
				pSigVal->fRawData		  = pChnData[0];
				pSigVal->tmCurrentSampled = tmDataSampled;

				// set raw is valid
				SIG_STATE_SET(pSigVal, VALUE_STATE_IS_VALID_RAW);
			}
			else
			{
				// set raw is invalid
				SIG_STATE_CLR(pSigVal, VALUE_STATE_IS_VALID_RAW);
			}
		}
	}

	else	// the comm status is still failure, need not to process data
	{
		return FALSE;
	}


	// send the notification to the related equipment to process data
	for (i = 0; i < pSampler->nRelatedEquipment; i++)
	{
		pEquip = pSampler->ppRelatedEquipment[i];

		// need process data - to notify the data processor
		cmd.nCmd   = PROCESS_CMD_SIG_DATA_CHANGED;
		cmd.pEquip = pEquip;
		Queue_Put(pSite->hqProcessCommand, &cmd, FALSE);

	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Sample_NeedQuery
 * PURPOSE  : test need query data or Not.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SAMPLER_INFO  *pSampler : 
 *            IN     double        tmNow     : 
 * RETURN   : static BOOL : TRUE need query data now.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-05-09 10:25
 *==========================================================================*/
static BOOL Sample_NeedQuery(IN OUT SAMPLER_INFO *pSampler, 
								 IN double tmNow)
{
	/*0. The bJustControlled flag is set by control cmd.	or		*/
	/*1. if the elapsed interval is greater than the period,		*/
	/*2. or the system time is changed to back, to sample data now.	*/

	//if (pSampler->bJustControlled || 
	if (
		((int)(tmNow - pSampler->tmSampleStarted) >= (pSampler->iSamplerPeriod/1000)) ||
		(tmNow < pSampler->tmSampleStarted))
	{
		pSampler->tmSampleStarted = tmNow;
		pSampler->bJustControlled = FALSE;	// reset need just control flag.

		return TRUE;					// Need query data now.
	}
	return FALSE;
}


// new, maofuhua, 2005-5-9
#define SAMPLER_NEED_QUERY(pSampler, tmNow)	 Sample_NeedQuery((pSampler), (tmNow))
/*==========================================================================*
 * FUNCTION : Sample_DataSamplingTask
 * PURPOSE  : the main sampling task for a port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PARAM_SAMPLING_TASK  *pParam : 
 * RETURN   : static DWORD : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-05 17:28
 *==========================================================================*/
static DWORD Sample_DataSamplingTask(IN PARAM_SAMPLING_TASK *pParam)
{
	HANDLE			hSelf = RunThread_GetId(NULL);
	SITE_INFO		*pSite;
	PORT_INFO		*pPort;
	SAMPLER_INFO	*pSampler;
	int				n = 0;
	double			tmNow;

	pSite = pParam->pSite;
	pPort = pParam->pPort;

	DELETE(pParam);// no use any more.

	AppLogOut(EQP_SMPLR, APP_LOG_UNUSED, 
		"Samp-task for P[%s:%d] starting, %d samplers attached.\n",
		EQP_GET_PORT_NAME(pPort),
		pPort->iPortID,
		pPort->nAttachedSamplers);

	g_bExitNotification = FALSE;

#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
				"Thread %s was created, Thread Id = %d.\n",
				EQP_GET_PORT_NAME(pPort), gettid());
#endif

	// main loop of sampling data
	while(!g_bExitNotification) //(THREAD_IS_RUNNING(hSelf))
	{
		if((!(THREAD_IS_RUNNING(hSelf))) 
			&& (pPort->iPortID == RS485_POLLING_SEVEN_ID))
		{
			g_bExitNotification = TRUE;
			continue;
		}

		// to sample data
		RunThread_Heartbeat(hSelf);	//heartbeat

		// to check control command.
		pSampler = Sample_CheckControl(pSite, pPort);

		RunThread_Heartbeat(hSelf);	//heartbeat

		// no sampler just controlled. to sample the all units one by one.
		if (pSampler == NULL)
		{
			pSampler = pPort->ppAttachedSamplers[n];

			// to read data from the next sampler
			n = (n+1 < pPort->nAttachedSamplers) ? n+1 : 0;
		}

		tmNow = GetCurrentTime();	// current time.

		//
		/*if (pSampler->iSamplerID == 4)
		{
			AppLogOut("Check", APP_LOG_ERROR, 
				"pSampler->nMaxUsedChannel=%d!\n\r", pSampler->nMaxUsedChannel);
		}*/
		//

		if (((pSampler->nMaxUsedChannel > 0) &&
			SAMPLER_NEED_QUERY(pSampler, tmNow))
			||(g_bExitNotification && (pPort->iPortID == RS485_POLLING_SEVEN_ID)))//Using for 485 sampler thread Exit
		{
			/*if (pSampler->iSamplerID == 4)
		{
			AppLogOut("Check", APP_LOG_ERROR, 
				"Should NOT go here!  pSampler->nMaxUsedChannel=%d!\n\r", pSampler->nMaxUsedChannel);
		}*/
			// test and open port
			if (Sample_ReopenPort(pPort, pSampler))
			{
				// OK, to read data from port using the sampler.
				Sample_ReadData(pPort, pSampler);
			}

			// to process data if need. // NOTE: pSampler->bDataChanged flag
			// can be set by Sample_ReadData() and Sample_ReopenPort().
			if (pSampler->bDataChanged)		// OK, to dispatch data.
			{
				RunThread_Heartbeat(hSelf);	//heartbeat

				// OK, to dispatch data and notify the related equipment.
				Sample_DispatchData(pSite, pSampler);

				pSampler->bDataChanged = FALSE;
			}
		}

		else
		{
			Sleep(100);	// sleep a while to check next sampler.
		}
	}

	Sleep(4000); //wait for son thread exiting

	AppLogOut(EQP_SMPLR, APP_LOG_UNUSED, 
		"Samp-task for port P[%s:%d:%s] exited.\n",
		EQP_GET_PORT_NAME(pPort),
		pPort->iPortID,
		pPort->szDeviceDesp);

	return 0;
}



/*==========================================================================*
* FUNCTION : Sample_GetSpecialSigByMergedId
* PURPOSE  : get the sig actual address in buffer, by merged id 
*			  (DXI_MERGE_SIG_ID), if error, will
*            log it out with the sig id.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN EQUIP_INFO  *pEquip     : 
*            IN int         nMergedId   : get from DXI_MERGE_SIG_ID 
* RETURN   : static SIG_BASIC_VALUE *: 
* COMMENTS : 
* CREATOR  : WangJing         DATE: 2006-06-01 17:45
*==========================================================================*/
static SIG_BASIC_VALUE *Sample_GetSpecialSigByMergedId(IN EQUIP_INFO *pEquip, 
                                                      IN int nMergedId)
{
    SIG_BASIC_VALUE *pSig;

    pSig = Init_GetEquipSigRefById(pEquip, 
        DXI_SPLIT_SIG_MID_TYPE(nMergedId),
        DXI_SPLIT_SIG_MID_ID(nMergedId));

    if (pSig == NULL)
    {
        AppLogOut(EQP_MGR, APP_LOG_WARNING,
            "The equipment %s(%d) has no such signal: type=%d, id=%d.\n",
            EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
            DXI_SPLIT_SIG_MID_TYPE(nMergedId), DXI_SPLIT_SIG_MID_ID(nMergedId));
    }

    return pSig;
}






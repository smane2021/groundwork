/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_exp_eval.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 16:37
 *  VERSION  : V1.00
 *  PURPOSE  : To evaluation the value of the expression
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <math.h>
#include <values.h>
#include "stdsys.h"
#include "public.h"


#include "cfg_model.h"
#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "equip_mon.h"


DEFINE_STACK(PEXP_ELEMENT);	// define the stack structure STACK_PEXP_ELEMENT

// return the pResult.
typedef PEXP_ELEMENT (*EXP_EVALUATION_PROC)(IN EXP_ELEMENT *pResult, 
							IN OUT STACK_PEXP_ELEMENT *pOpDataStack);

typedef struct _EXP_EVALUATION_PROC_LIST
{
#ifdef _DEBUG
	char					*name;
#endif
	int						op;		// operator
	EXP_EVALUATION_PROC		eval;	// evaluation proc
}EXP_EVALUATION_PROC_LIST;


#ifdef _DEBUG
#define DEF_EXP_EVAL_PROC_LIST(opr, proc)	\
	{ #opr, (opr), (EXP_EVALUATION_PROC)(proc)} 
// to check the stack status
#define POP_EXP_OPD(opd, pStack, pRes)		\
	do {																	\
		if (!STACK_SAFELY_POP((pStack), (opd)))								\
		{																	\
			TRACEX("Error, stack is empty when popup operand.\n");	\
			(pRes)->byElementType = EXP_ETYPE_ERROR; /*EXP_ETYPE_INVALID;*/	\
			return (pRes);													\
		}																	\
	} while(0)

#else	// release, it may not safe
#define DEF_EXP_EVAL_PROC_LIST(opr, proc)	\
		{ (opr), (EXP_EVALUATION_PROC)(proc)} 
#define POP_EXP_OPD(opd, pStack, pRes)		((opd)=STACK_POP(pStack))
#endif


static int Eval_GetElementValue(IN EXP_ELEMENT *pVal, OUT EXP_CONST *pValue)
{
	SIG_BASIC_VALUE *pSigVal;

	switch (pVal->byElementType)
	{
	case EXP_ETYPE_VARIABLE:
		pSigVal = (SIG_BASIC_VALUE *)pVal->pElement;

		if ( SIG_VALUE_IS_VALID(pSigVal))
		{
			*pValue = (pSigVal->ucType == VAR_FLOAT) 
				? (EXP_CONST)(pSigVal->varValue.fValue) 
				: (pSigVal->ucType == VAR_UNSIGNED_LONG)
				? (EXP_CONST)(pSigVal->varValue.ulValue)
				: (EXP_CONST)(pSigVal->varValue.lValue);
			return EXP_ETYPE_CONST;
		}

		return EXP_ETYPE_INVALID;

		// support sampled raw data as variable. maofuhua, 2005-3-15
	case EXP_ETYPE_VARIABLE_RAW:
		pSigVal = (SIG_BASIC_VALUE *)pVal->pElement;

		if (SIG_STATE_TEST(pSigVal, VALUE_STATE_IS_VALID_RAW))
		{
			*pValue = (EXP_CONST)(pSigVal->fRawData);
			return EXP_ETYPE_CONST;
		}

		return EXP_ETYPE_INVALID;

		
	case EXP_ETYPE_CONST:
		*pValue = *(EXP_CONST *)pVal->pElement;
		return EXP_ETYPE_CONST;
	}

	return EXP_ETYPE_INVALID;
}

/*==========================================================================*
 * FUNCTION : Eval_EXP_NOT
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if the opd is not zero, return 1, or 0
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 19:12
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_NOT(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		      val2;

	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	//no use

	pResult->byElementType = Eval_GetElementValue(opd2, &val2);

	// the value is ok
	if (pResult->byElementType == EXP_ETYPE_CONST)
	{
		*(EXP_CONST *)pResult->pElement = 
			(EXP_CONST)(FLOAT_EQUAL0((float)val2) ? 1 : 0);
	}	

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_MUL
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 19:51
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_MUL(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		*(EXP_CONST *)pResult->pElement =
			(EXP_CONST)((float)val1 * (float)(val2));
		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_DIV
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : return value=MAXFLOAT if divided by 0.
 *                                  return INVALID if one of opds is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:06
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_DIV(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		if (FLOAT_EQUAL0((float)val2))		// divided by zero?
		{
			if (FLOAT_EQUAL0((float)val1))	// 0/0 ? = 0
			{
				*(EXP_CONST *)pResult->pElement = (EXP_CONST)0;
			}
			else
			{
				TRACE("[Eval_EXP_DIV] -- %f divided by zero(%f).\n",
					(float)val1, (float)val2);

				*(EXP_CONST *)pResult->pElement = (EXP_CONST)MAXFLOAT;
			}
		}
		else
		{
			*(EXP_CONST *)pResult->pElement =
				(EXP_CONST)((float)val1 / (float)(val2));
		}

		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_ADD
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:13
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_ADD(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		*(EXP_CONST *)pResult->pElement =
			(EXP_CONST)((float)val1 + (float)(val2));
		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_SUB
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:13
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_SUB(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		*(EXP_CONST *)pResult->pElement =
			(EXP_CONST)((float)val1 - (float)(val2));
		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_GREATER
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:15
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_GREATER(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		*(EXP_CONST *)pResult->pElement = 
			(EXP_CONST)(((float)val1 > (float)(val2)) ? 1 : 0);
		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}



/*==========================================================================*
 * FUNCTION : Eval_EXP_LESS
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:17
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_LESS(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		*(EXP_CONST *)pResult->pElement =
			(EXP_CONST)(((float)val1 < (float)(val2)) ? 1 : 0);
		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_GREATER_EQUAL
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:18
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_GREATER_EQUAL(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		if (FLOAT_EQUAL((float)val1, (float)val2))
		{
			*(EXP_CONST *)pResult->pElement = (EXP_CONST)1;
		}
		else
		{
			*(EXP_CONST *)pResult->pElement = 
				(EXP_CONST)(((float)val1 > (float)(val2)) ? 1 : 0);
		}

		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_LESS_EQUAL
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:21
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_LESS_EQUAL(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		if (FLOAT_EQUAL((float)val1, (float)val2))
		{
			*(EXP_CONST *)pResult->pElement = (EXP_CONST)1;
		}
		else
		{
			*(EXP_CONST *)pResult->pElement = 
				(EXP_CONST)(((float)val1 < (float)(val2)) ? 1 : 0);
		}

		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_EQUAL
 * PURPOSE  : =.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:22
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_EQUAL(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		*(EXP_CONST *)pResult->pElement = 
			(EXP_CONST)((FLOAT_EQUAL((float)val1, (float)val2)) ? 1 : 0);
		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_AND
 * PURPOSE  : logic AND.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if one of two opds is invalid, 
 *            the result is invalid
 *            if one is zero, return zero. else is 1.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:22
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_AND(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	if ((Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST) &&
		(Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST))
	{
		*(EXP_CONST *)pResult->pElement = 
			(EXP_CONST)(FLOAT_NOT_EQUAL0((float)val1) &&
			FLOAT_NOT_EQUAL0((float)val2));

		pResult->byElementType = EXP_ETYPE_CONST;
	}
	else
	{
		pResult->byElementType = EXP_ETYPE_INVALID;
	}

	return pResult;
}


/*==========================================================================*
 * FUNCTION : Eval_EXP_OR
 * PURPOSE  : logic OR
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PEXP_ELEMENT            pResult       : 
 *            IN OUT STACK_PEXP_ELEMENT  *pOpDataStack : 
 * RETURN   : static PEXP_ELEMENT : if both of two opds are invalid, 
 *            the result is invalid
 *            if one is non-zero, return 1. else is 0.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:25
 *==========================================================================*/
static PEXP_ELEMENT Eval_EXP_OR(IN PEXP_ELEMENT pResult, 
								 IN OUT STACK_PEXP_ELEMENT *pOpDataStack)
{	
	PEXP_ELEMENT	opd1, opd2;
	EXP_CONST		val1, val2;
	int				nInvalidOpds = 0;
	
	POP_EXP_OPD(opd2, pOpDataStack, pResult);
	POP_EXP_OPD(opd1, pOpDataStack, pResult);	

	// get the opd value
	// for OR operator, to test the opd1 at first, if its value is TRUE, 
	// set TRUE to result, or to test the opd2.
	pResult->byElementType = EXP_ETYPE_CONST;

	if (Eval_GetElementValue(opd1, &val1) == EXP_ETYPE_CONST)
	{
		if (FLOAT_NOT_EQUAL0((float)val1))
		{
			*(EXP_CONST *)pResult->pElement = 1;	
			return pResult;
		}
	}
	else
	{
		nInvalidOpds++;
	}

	// to check opd2
	if (Eval_GetElementValue(opd2, &val2) == EXP_ETYPE_CONST)
	{
		if (FLOAT_NOT_EQUAL0((float)val2))
		{
			*(EXP_CONST *)pResult->pElement = 1;	
			return pResult;
		}
	}
	else
	{
		nInvalidOpds++;
	}

	// check both are invalid opds?
	if (nInvalidOpds != 2)	// not all, set 0 to result
	{
		*(EXP_CONST *)pResult->pElement = 0;		
		return pResult;
	}

	// invalid result.
	pResult->byElementType = EXP_ETYPE_INVALID;
	
	return pResult;
}


// The operator must be start from 0, and less than EXP_MAX_OPERATOR.
// The value of the operators must be continuous due to the value will be 
// used as the array index in calculate the parsed expression in running.
// if adds some new operator, the function Exp_Calculate() in 
// equip_exp_eval.c must be modified at the same time. 
// maofuhua,04-11-11

static EXP_EVALUATION_PROC_LIST	s_pExpEvalProc[] = 
{
	DEF_EXP_EVAL_PROC_LIST(EXP_NOT			, Eval_EXP_NOT			),
	DEF_EXP_EVAL_PROC_LIST(EXP_MUL			, Eval_EXP_MUL			),
	DEF_EXP_EVAL_PROC_LIST(EXP_DIV			, Eval_EXP_DIV			),
	DEF_EXP_EVAL_PROC_LIST(EXP_ADD			, Eval_EXP_ADD			),
	DEF_EXP_EVAL_PROC_LIST(EXP_SUB			, Eval_EXP_SUB			),
	DEF_EXP_EVAL_PROC_LIST(EXP_GREATER		, Eval_EXP_GREATER		),
	DEF_EXP_EVAL_PROC_LIST(EXP_LESS			, Eval_EXP_LESS			),
	DEF_EXP_EVAL_PROC_LIST(EXP_GREATER_EQUAL, Eval_EXP_GREATER_EQUAL),
	DEF_EXP_EVAL_PROC_LIST(EXP_LESS_EQUAL	, Eval_EXP_LESS_EQUAL	),
	DEF_EXP_EVAL_PROC_LIST(EXP_EQUAL		, Eval_EXP_EQUAL		),
	DEF_EXP_EVAL_PROC_LIST(EXP_AND			, Eval_EXP_AND			),
	DEF_EXP_EVAL_PROC_LIST(EXP_OR			, Eval_EXP_OR			),
	// end of all operator
};

#define MAX_IMPLEMENTED_OP		ITEM_OF(s_pExpEvalProc)

#define OPERATOR_IS_VALID(opr)	(((opr) < MAX_IMPLEMENTED_OP) &&		\
								(s_pExpEvalProc[(opr)].eval != NULL) &&	\
								(s_pExpEvalProc[(opr)].op == (opr)))



/*==========================================================================*
 * FUNCTION : Exp_StringElement
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: EXP_ELEMENT  *pEle : 
 *            BOOL	bMappedVar: TRUE the var is mapped to signal addr.
 *            OUT char        *pszEleBuf : 
 *            IN size_t       nEleLen    : 
 * RETURN   : static int : 
 * COMMENTS : // copied from Thomas Lin: Print_Exp(pEle)
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-21 13:35
 *==========================================================================*/
static int Exp_StringElement(IN EXP_ELEMENT *pEle, IN BOOL bMappedVar,
							  OUT char *pszEleBuf, IN size_t nEleLen)
{
	int			nLen;
	char		*pszOperator;
	char		cLeftBracket = '[', cRightBracket = ']'; 

	switch(pEle->byElementType)
	{
	case EXP_ETYPE_VARIABLE_RAW:
		cLeftBracket = '{', cRightBracket = '}';
	case EXP_ETYPE_VARIABLE:
		if (bMappedVar)
		{
			SAMPLE_SIG_VALUE *pVar;

			pVar = (SAMPLE_SIG_VALUE *)pEle->pElement; 
			if (pVar->bv.ucType == VAR_FLOAT) 
			{
				nLen = snprintf(pszEleBuf, nEleLen, "%c%s(%d)=%1.3f%c",
					cLeftBracket,
					EQP_GET_SIGV_NAME0(pVar), pVar->pStdSig->iSigID,
					pVar->bv.varValue.fValue,
					cRightBracket);
			}
			else
			{
				nLen = snprintf(pszEleBuf, nEleLen, "%c%s(%d)=%ld%c",
					cLeftBracket,
					EQP_GET_SIGV_NAME0(pVar), pVar->pStdSig->iSigID,
					pVar->bv.varValue.lValue,
					cRightBracket);
			}
		}

		else
		{
			EXP_VARIABLE	*pExpVar;

			pExpVar = (EXP_VARIABLE *)pEle->pElement;
			nLen = snprintf(pszEleBuf, nEleLen, "%c%d,%d,%d%c",
				cLeftBracket,
				pExpVar->iRelevantEquipIndex, 
				pExpVar->iSigType, pExpVar->iSigID,
				cRightBracket);
		}

		return nLen;

	case EXP_ETYPE_CONST:
		nLen = snprintf(pszEleBuf, nEleLen, "[C=%1.3f]",
			*((EXP_CONST *)pEle->pElement));
		return nLen;

	case EXP_ETYPE_OPERATOR:
		switch(((EXP_OPERATOR *)pEle->pElement)->byOperatorType)
		{
		case EXP_ADD:
			pszOperator = "[+]";
			break;

		case EXP_SUB:
			pszOperator = "[-]";
			break;

		case EXP_MUL:
			pszOperator = "[*]";
			break;

		case EXP_DIV:
			pszOperator = "[/]";
			break;

		case EXP_GREATER:
			pszOperator = "[>]";
			break;

		case EXP_LESS:
			pszOperator = "[<]";
			break;

		case EXP_GREATER_EQUAL:
			pszOperator = "[>=]";
			break;

		case EXP_LESS_EQUAL:
			pszOperator = "[<=]";
			break;

		case EXP_EQUAL:
			pszOperator = "[=]";
			break;

		case EXP_AND:
			pszOperator = "[&]";
			break;

		case EXP_OR:
			pszOperator = "[|]";
			break;

		case EXP_NOT:
			pszOperator = "[!]";
			break;

		default:
			pszOperator = "[E_OPERATOR]";
		}

		break;

	case EXP_ETYPE_DUMMY:
		pszOperator = "[E_DUMMY]";
		break;

	case EXP_ETYPE_INVALID:
		pszOperator = "[E_INVALID]";
		break;

	default:
		pszOperator = "[E_TYPE]";
	}


	nLen = snprintf(pszEleBuf, nEleLen, "%s", pszOperator);

	return nLen;
}


/*==========================================================================*
 * FUNCTION : Exp_DisplayExpression
 * PURPOSE  : write a exp to a string buffer.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EXP_ELEMENT  *pEvalExp : 
 *            IN int          nElements : 
 *            BOOL	bMappedVar: TRUE the var is mapped to signal addr.
 *            OUT char        *pszExpStr : 
 *            IN int          nExpStrLen    : 
 * RETURN   : char *: the pszExpStr
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-21 13:35
 *==========================================================================*/
char *Exp_StringExpression(IN EXP_ELEMENT *pEvalExp, IN int nElements,
						   IN BOOL bMappedVar, 
						   OUT char *pszExpStr, IN int nExpStrLen)
{
	int					i;
	EXP_ELEMENT			*pExp = pEvalExp;

	char				*pCurExpBuf   = pszExpStr;
	int					nCurExpStrLen = nExpStrLen;
	int					nLen;

	*pCurExpBuf = 0;

	for (i = 0; (i < nElements) && (nCurExpStrLen > 0); i++, pExp++)
	{
		nLen = Exp_StringElement(pExp, bMappedVar, 
			pCurExpBuf, (size_t)nCurExpStrLen);

		pCurExpBuf    += nLen;
		nCurExpStrLen -= nLen;

		if (nCurExpStrLen <= 0)	// < 0 impossible.
		{
			TRACEX("The buffer is too small on stringlizing the exp.\n");
			break;
		}
	}

	return pszExpStr;
}


#ifdef _DEBUG
/*==========================================================================*
 * FUNCTION : Exp_DisplayExpression
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EXP_ELEMENT  *pEvalExp : 
 *            IN int          nElements : 
 *            BOOL	bMappedVar: TRUE the var is mapped to signal addr.
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-21 13:35
 *==========================================================================*/
void Exp_DisplayExpression(IN EXP_ELEMENT *pEvalExp, IN int nElements,
						   IN BOOL bMappedVar)
{
	char	szBuf[512];

	Exp_StringExpression(pEvalExp, nElements, bMappedVar, 
		szBuf, sizeof(szBuf));

	printf("Exp(%p) %d elements: %s\n", 
		pEvalExp, nElements, szBuf);
}
#endif //_DEBUG


/*==========================================================================*
 * FUNCTION : Exp_Calculate
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT EXP_ELEMENT  *pExpResult : 
 *            IN EXP_ELEMENT   *pEvalExp       : 
 *            IN int          nElements    : 
 * RETURN   : BOOL : TRUE for OK, 
 *                   FALSE for the expression is error, the result is undefined.
 * COMMENTS : the byElementType of pExpResult shall be checked.
 *            if EXP_ETYPE_INVALID indicates the result is invalid.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:26
 *==========================================================================*/
#ifdef _DEBUG
BOOL Exp_CalculateD(IN char *pSigName, 
					IN EXP_ELEMENT *pEvalExp, IN int nElements,
					OUT EXP_ELEMENT *pResult,
					IN char *__function__, IN int __line__)
#else
BOOL Exp_CalculateR(IN EXP_ELEMENT *pEvalExp, IN int nElements,
				   OUT EXP_ELEMENT *pResult)
#endif //_DEBUG
{
	PEXP_ELEMENT		pStackBuffer[EXP_MAX_ELEMENTS];
	STACK_PEXP_ELEMENT	workStack;

	EXP_CONST			pMidResultValue[EXP_MAX_ELEMENTS];	// mid result.
	EXP_ELEMENT			pMidResultBuffer[EXP_MAX_ELEMENTS];
	PEXP_ELEMENT		pMidResult;

	int					op;
	int					i;
	EXP_ELEMENT			*pExp = pEvalExp;

#ifdef _DEBUG
	BOOL				bNoResult = FALSE;

	//TRACEX("Calculating exp=%p, n=%d...\n",
	//	pExp, nElements);
#endif //_DEBUG

	if ((pExp == NULL) || (pExp->byElementType == EXP_ETYPE_ERROR))
	{
		pResult->byElementType = EXP_ETYPE_ERROR;
		return FALSE;
	}

	STACK_INIT(&workStack, pStackBuffer, ITEM_OF(pStackBuffer));

	for (i = 0; i < nElements; i++, pExp++)
	{
		// push the operand to stack till reach a operator
		if (pExp->byElementType != EXP_ETYPE_OPERATOR)
		{
			STACK_PUSH(&workStack, pExp);
		}
		else // to eval
		{
			// make the middle result
			pMidResult			 = &pMidResultBuffer[i];
			pMidResult->pElement = &pMidResultValue[i];

			op = ((EXP_OPERATOR*)pExp->pElement)->byOperatorType;
			if (OPERATOR_IS_VALID(op))
			{
				// eval
				s_pExpEvalProc[op].eval(pMidResult, &workStack);
			}
			else
			{
				pMidResult->byElementType = EXP_ETYPE_INVALID;

#ifdef _DEBUG
				TRACEX("[%s:%d %s] Meet an invalid operator %d.\n", 
					__function__, __line__, pSigName, op);
#endif //_DEBUG
			}

			if (pMidResult->byElementType != EXP_ETYPE_ERROR)
			{
				// push the middle result to stack
				STACK_PUSH(&workStack, pMidResult);
			}
			else
			{
#ifdef _DEBUG
				TRACEX("Error on calc the No%d op=%s(%d) of exp(%p, %d ele).\n", 
					i, s_pExpEvalProc[op].name, op, 
					pEvalExp, nElements);
				Exp_DisplayExpression(pEvalExp, nElements, TRUE);
#endif //_DEBUG
				return FALSE;
			}
		}
	}

	// get the result.
	if (!STACK_IS_EMPTY(&workStack))
	{
		// popup result. copy the result. 
		pMidResult = STACK_POP(&workStack);

		// check the stack. if it now is empty, the exp is correct.
		if ((pMidResult->byElementType != EXP_ETYPE_OPERATOR) &&
			STACK_IS_EMPTY(&workStack))
		{
			pResult->byElementType = Eval_GetElementValue(pMidResult,
				(EXP_CONST *)pResult->pElement);
			
//#ifdef _DEBUG
//			TRACEX("The following exp(%p, %d ele) got: %1.3f.\n", 
//				pEvalExp, nElements, *(float *)(pResult->pElement));
//			Exp_DisplayExpression(pEvalExp, nElements, TRUE);
//#endif //_DEBUG

			return TRUE;
		}
	}

#ifdef _DEBUG
	else
	{
		bNoResult = TRUE;
	}

	// show error msg and the exp
	TRACE("[%s:%d %s] meets error on calc exp: ",
		__function__, __line__, pSigName);

	Exp_DisplayExpression(pEvalExp, nElements, TRUE);
	
	if (bNoResult)
	{
		TRACE("Invalid expression, no result in stack!\n");
	}

	else if (pMidResult->byElementType == EXP_ETYPE_OPERATOR)
	{
		TRACE("Invalid expression, the result type is an"
			"operator, not const or invalid!\n");
	}

	else if(!STACK_IS_EMPTY(&workStack))
	{
		TRACE("Invalid expression, the stack is not empty after "
			"popping result!\n");
	}

	TRACEX("The following exp(%p, %d ele) got: %1.3f.\n", 
		pEvalExp, nElements, *(float *)(pResult->pElement));
	Exp_DisplayExpression(pEvalExp, nElements, TRUE);

#endif	//_DEBUG

	// invalid and error result.
	pResult->byElementType = EXP_ETYPE_ERROR;
    
	return FALSE;
}



//#define DEBUG_CALE_EXP
/*==========================================================================*
 * FUNCTION : Exp_CalcDeadLockPossibleExp
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EXP_ELEMENT   *pEvalExp : 
 *            IN int           nElements : 
 *            OUT EXP_ELEMENT  *pResult  : 
 *            IN void          *pSigA    : 
 *            IN void          *pSigB    : 
 * RETURN   : BOOL : 
 * COMMENTS : Judge if need correct a middle result. The necessary condition is: 
			  a. A signal is in B signal's Settable Expression, vice versa;
			  b. b1. Operator is '<', '>', '<=' or '>=', and operand is A and B signal,
			  or b2. last operator is '+' or '-', and one operand is A (or B) signal and  
			  another operand is a constant, meanwhile this operator is '<', '>', '<=' or '>=',
			  and one operand is B (or A) signal.
 * CREATOR  : Frank Cao                DATE: 2007-03-30 16:43
 *==========================================================================*/
BOOL Exp_CalcDeadLockPossibleExp(IN EXP_ELEMENT *pEvalExp,
								 IN int nElements,
								 OUT EXP_ELEMENT *pResult,
								 IN void *pSigA,
								 IN void *pSigB)
{
	PEXP_ELEMENT		pStackBuffer[EXP_MAX_ELEMENTS];
	STACK_PEXP_ELEMENT	workStack;

	EXP_CONST			pMidResultValue[EXP_MAX_ELEMENTS];	// mid result.
	EXP_ELEMENT			pMidResultBuffer[EXP_MAX_ELEMENTS];
	PEXP_ELEMENT		pMidResult;

	int					op;
	int					i;
	EXP_ELEMENT			*pExp = pEvalExp;
	
	EXP_ELEMENT			*pElementA = NULL;
	EXP_ELEMENT			*pElementB = NULL;
	EXP_ELEMENT			*pElementC = NULL;
	EXP_ELEMENT			*pElementD = NULL;

	BOOL				bLastIsAddOrSub = FALSE;
	BOOL				bNeedModify = FALSE;
	BOOL				bNeedCheck = TRUE;

//#ifdef DEBUG_CALE_EXP
	/*int	iAsigId = ((SET_SIG_VALUE*)(pSigA))->pStdSig->iSigID;
	if(iAsigId == 17 || iAsigId == 19)
	TRACE("\n\n***A is %s, B is %s***\n", 
		((SET_SIG_VALUE*)(pSigA))->pStdSig->pSigName->pFullName[1],
		((SET_SIG_VALUE*)(pSigB))->pStdSig->pSigName->pFullName[1]);*/
//#endif
	if ((pExp == NULL) || (pExp->byElementType == EXP_ETYPE_ERROR))
	{
		pResult->byElementType = EXP_ETYPE_ERROR;
		return FALSE;
	}

	STACK_INIT(&workStack, pStackBuffer, ITEM_OF(pStackBuffer));

	for (i = 0; i < nElements; i++, pExp++)
	{
		// push the operand to stack till reach a operator
		if (pExp->byElementType != EXP_ETYPE_OPERATOR)
		{
			STACK_PUSH(&workStack, pExp);
		}
		else // to eval
		{
			// make the middle result
			pMidResult			 = &pMidResultBuffer[i];
			pMidResult->pElement = &pMidResultValue[i];

			op = ((EXP_OPERATOR*)pExp->pElement)->byOperatorType;

			if (OPERATOR_IS_VALID(op))
			{
				
				if(bNeedCheck)
				{
					if(op == EXP_ADD || op == EXP_SUB)
					{
						//get out 2 elements from stack
						if(!(STACK_SAFELY_POP(&workStack, pElementA)))
						{
							pElementA = NULL;
						}
						if(!(STACK_SAFELY_POP(&workStack, pElementB)))
						{
							pElementB = NULL;
						}

						if(pElementB)
						{
							STACK_PUSH(&workStack, pElementB);
						}
						if(pElementA)
						{
							STACK_PUSH(&workStack, pElementA);
						}

						//Get pElementC
						if(pElementB && pElementA)
						{
							if(pElementA->byElementType == EXP_ETYPE_VARIABLE 
								&& pElementB->byElementType == EXP_ETYPE_CONST)
							{
								pElementC = pElementA;
							}
							else if(pElementB->byElementType == EXP_ETYPE_VARIABLE 
								&& pElementA->byElementType == EXP_ETYPE_CONST)
							{
								pElementC = pElementB;
							}
							else
							{
								pElementC = NULL;
							}
						}
						else
						{
							pElementC = NULL;
						}
						bLastIsAddOrSub = TRUE;
					}
					else
					{
						if(op >= EXP_GREATER && op <= EXP_LESS_EQUAL)
						{	
							pElementA = STACK_POP(&workStack);
							pElementB = STACK_POP(&workStack);
							STACK_PUSH(&workStack, pElementB);
							STACK_PUSH(&workStack, pElementA);
					
							if(bLastIsAddOrSub && pElementC)
							{
								if(pElementA->byElementType == EXP_ETYPE_VARIABLE 
									&& pElementB->byElementType == EXP_ETYPE_CONST)
								{
									pElementD = pElementA;
								}
								else if(pElementB->byElementType == EXP_ETYPE_VARIABLE 
									&& pElementA->byElementType == EXP_ETYPE_CONST)
								{
									pElementD = pElementB;
								}
								else
								{
									pElementD = NULL;
								}
							}
							else
							{
								if(pElementA->byElementType == EXP_ETYPE_VARIABLE 
									&& pElementB->byElementType == EXP_ETYPE_VARIABLE)
								{
									pElementC = pElementA;
									pElementD = pElementB;
								}
								else
								{
									pElementC = NULL;
									pElementD = NULL;
								}
							}
			
							if(pElementC && pElementD)
							{
								bNeedModify = TRUE;
							}
						}
						bLastIsAddOrSub = FALSE;				
					}

				}
				/*if(iAsigId == 17 || iAsigId == 19)
					TRACE("***Before Calc: op = %d, ***\n", op);*/

				s_pExpEvalProc[op].eval(pMidResult, &workStack);

				if(bNeedModify)
				{
					/*if(iAsigId == 17 || iAsigId == 19)
						TRACE("***bNeedModify is TRUE!!! pSigA = %d, pSigB = %d, pElementC = %d, pElementD = %d***\n",
						pSigA,
						pSigB,
						pElementC->pElement,
						pElementD->pElement);*/ 

					if((pSigA == pElementC->pElement && pSigB == pElementD->pElement)
						|| (pSigA == pElementD->pElement && pSigB == pElementC->pElement))
					{
						pMidResult->byElementType = EXP_ETYPE_CONST;
						*((EXP_CONST *)(pMidResult->pElement)) = (EXP_CONST)(TRUE);

						bNeedCheck = FALSE;

						/*if(iAsigId == 17 || iAsigId == 19)
							TRACE("***Correct the result for deadlock***\n");*/ 
					}
					bNeedModify = FALSE;
				}
				/*if(iAsigId == 17 || iAsigId == 19)
				TRACE("***After Calc: pMidResult->byElementType = %d, pMidResult->pElement = %.3f.***\n", 
					pMidResult->byElementType, 
					*((float*)(pMidResult->pElement)));*/
			}
			else
			{
				pMidResult->byElementType = EXP_ETYPE_INVALID;

			}

			if (pMidResult->byElementType != EXP_ETYPE_ERROR)
			{
				// push the middle result to stack
				STACK_PUSH(&workStack, pMidResult);
			}
			else
			{
				return FALSE;
			}
		}
	}


	// get the result.
	if (!STACK_IS_EMPTY(&workStack))
	{
		// popup result. copy the result. 
		pMidResult = STACK_POP(&workStack);

		// check the stack. if it now is empty, the exp is correct.
		if ((pMidResult->byElementType != EXP_ETYPE_OPERATOR) &&
			STACK_IS_EMPTY(&workStack))
		{
			pResult->byElementType = Eval_GetElementValue(pMidResult,
				(EXP_CONST *)pResult->pElement);			
			return TRUE;
		}
	}

	// invalid and error result.
	pResult->byElementType = EXP_ETYPE_ERROR;
    
	return FALSE;
}



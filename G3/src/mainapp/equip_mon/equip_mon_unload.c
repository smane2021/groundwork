/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_mon_init.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-02 09:12
 *  VERSION  : V1.00
 *  PURPOSE  : To unload the site based on the loaded configuration information
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "cfg_model.h"
#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "equip_mon.h"

typedef BOOL	(*SITE_UNLOAD_SUB_PROC)(IN OUT SITE_INFO *pSite);
struct _SITE_UNLOAD_ITEM
{
	const char			*pszInitItem;	// description of being initialized
	SITE_UNLOAD_SUB_PROC	pfnInitProc;	// the init proc.
};
typedef struct _SITE_UNLOAD_ITEM	SITE_UNLOAD_ITEM;

#define DEF_SITE_UNLOAD_ITEM(pszItem, pfnInit)	\
	{(const char *)(pszItem), (SITE_UNLOAD_SUB_PROC)(pfnInit)}


static BOOL Unload_SiteInfo(IN OUT SITE_INFO *pSite);
static BOOL Unload_MonitoredEquipmentUnits(IN OUT SITE_INFO *pSite);
static BOOL Unload_Samplers(IN OUT SITE_INFO *pSite);
static BOOL Unload_StdSamplers(IN OUT SITE_INFO *pSite);
static BOOL Unload_SamplingPorts(IN OUT SITE_INFO *pSite);
static BOOL Unload_ActiveAlarms(IN OUT SITE_INFO *pSite);
static BOOL Unload_DataFiles(IN OUT SITE_INFO *pSite);
static BOOL Unload_CtrlCmdArray(IN OUT SITE_INFO *pSite);

#ifdef PRODUCT_INFO_SUPPORT
static BOOL Unload_Devices(IN OUT SITE_INFO *pSite);
#endif //PRODUCT_INFO_SUPPORT


#ifdef DEFER_BACKUP_CONFIG_FILES
extern	int EquipMonitoring_UnloadConfigBackupTask(IN SITE_INFO *pSite);
#endif

/*==========================================================================*
 * FUNCTION : Unload_MonitoringSite
 * PURPOSE  : cleanup the runtime data of the site created by the Init_xxxx()
 *            other configuration data shall be unloaded by Cfg_UnloadConfig();
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-29 17:02
 *==========================================================================*/
BOOL Unload_MonitoringSite(IN OUT SITE_INFO *pSite)
{
	SITE_UNLOAD_ITEM	inits[] =	// the init sequence can NOT be changed.
	{
		//5. unload active alarms
		DEF_SITE_UNLOAD_ITEM("Ctrl cmd array", Unload_CtrlCmdArray),

		//4. unload sampling port
		DEF_SITE_UNLOAD_ITEM("Sampling ports", Unload_SamplingPorts),

		//3. unload std-samplers
		DEF_SITE_UNLOAD_ITEM("Standard samplers", Unload_StdSamplers),

		//2.unload samplers
		DEF_SITE_UNLOAD_ITEM("Samplers", Unload_Samplers),

#ifdef PRODUCT_INFO_SUPPORT
		//unload devices
		DEF_SITE_UNLOAD_ITEM("Devices", Unload_Devices),
#endif //PRODUCT_INFO_SUPPORT

		//1. unload monitored equipment
		DEF_SITE_UNLOAD_ITEM("Monitored equipment", Unload_MonitoredEquipmentUnits),

		// 0. unload site self
		DEF_SITE_UNLOAD_ITEM("Site", Unload_SiteInfo),
	};

	int		n;

	AppLogOut(EQP_UNLDR, APP_LOG_UNUSED, "Unloading monitoring site...\n");

#ifdef DEFER_BACKUP_CONFIG_FILES
	EquipMonitoring_UnloadConfigBackupTask(pSite);
#endif

	// init the items defined in inits, stop if any init fails.
	for (n = 0; n < ITEM_OF(inits); n++ )
	{
		// start to init
		AppLogOut(EQP_UNLDR, APP_LOG_UNUSED, "%d: %s are being unloaded...\n",
			n+1, 
			inits[n].pszInitItem);
		
		// to do init
		inits[n].pfnInitProc(pSite);
		
		// log init result.
		AppLogOut(EQP_UNLDR, 
			APP_LOG_UNUSED, 
			"%d: %s have been successfully unloaded.\n",
			n+1, 
			inits[n].pszInitItem);
	}

	// if any init fails, whole initialization process is failure.
	AppLogOut(EQP_UNLDR, 
		APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"The monitoring site is successfully unloaded.\n");	
	
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_SiteInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-13 19:09
 *==========================================================================*/
static BOOL Unload_SiteInfo(IN OUT SITE_INFO *pSite)
{
	if(pSite->hqProcessCommand)
	{
		Queue_Destroy(pSite->hqProcessCommand);
	}

	if(pSite->hLcdMessageQueue)
	{
		Queue_Destroy(pSite->hLcdMessageQueue);	
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_EquipInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:55
 *==========================================================================*/
static BOOL Unload_EquipInfo(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip)
{
	int nSig;

	UNUSED(pSite);

	// 1. unload sampling signals
	if (pEquip->pSampleSigValue != NULL)
	{
		SAMPLE_SIG_VALUE *pSig;
		
		pSig = pEquip->pSampleSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iSampleSigNum; nSig++, pSig++)
		{
			// release the calc exp
			SAFELY_DELETE(pSig->sv.pCalculateExpression);
			pSig->sv.iCalculateExpression = 0;
			
			SAFELY_DELETE(pSig->sv.pDispExp);
			pSig->sv.iDispExp = 0;

			// release the related alarm table.
			SAFELY_DELETE(pSig->sv.ppRelatedAlarms);
			pSig->sv.iRelatedAlarms = 0;

			// release the statistics data buffer
			SAFELY_DELETE(pSig->sv.pStatValue);
		}

		// delete the all signals
		SAFELY_DELETE(pEquip->pSampleSigValue);
	}

	// 2. unload control signals
	if (pEquip->pCtrlSigValue != NULL)
	{
		CTRL_SIG_VALUE *pSig;
		
		pSig = pEquip->pCtrlSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iCtrlSigNum; nSig++, pSig++)
		{
			SAFELY_DELETE(pSig->sv.pControllableExpression);

			SAFELY_DELETE(pSig->sv.pDispExp);
			pSig->sv.iDispExp = 0;


		}

		// delete the all signals
		SAFELY_DELETE(pEquip->pCtrlSigValue);
	}


	//3. unload setting signals
	if (pEquip->pSetSigValue != NULL)
	{
		SET_SIG_VALUE *pSig;
		
		pSig = pEquip->pSetSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iSetSigNum; nSig++, pSig++)
		{
			SAFELY_DELETE(pSig->sv.pSettableExpression);
			SAFELY_DELETE(pSig->sv.pDispExp);
			pSig->sv.iDispExp = 0;

		}

		// delete the all signals
		SAFELY_DELETE(pEquip->pSetSigValue);
	}


	//4. unload alarm signals
	if (pEquip->pAlarmSigValue != NULL)
	{
		ALARM_SIG_VALUE *pSig;
		
		pSig = pEquip->pAlarmSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iAlarmSigNum; nSig++, pSig++)
		{
			SAFELY_DELETE(pSig->sv.pAlarmExpression);
			SAFELY_DELETE(pSig->sv.pSuppressExpression);
		}

		// delete the all signals
		SAFELY_DELETE(pEquip->pAlarmSigValue);
	}

	// delete the reference table
	SAFELY_DELETE(pEquip->ppEquipReferredThis);
	pEquip->iEquipReferredThis = 0;

	SAFELY_DELETE(pEquip->pAlarmFilterExpression);

	// destroy the synchronization lock
	if (pEquip->hSyncLock != NULL)
	{
		Mutex_Destroy(pEquip->hSyncLock);
		pEquip->hSyncLock = NULL;
	}
	
	if (pEquip->hLcdSyncLock != NULL)
	{
		Mutex_Destroy(pEquip->hLcdSyncLock);
		pEquip->hSyncLock = NULL;
	}	

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_MonitoredEquipmentUnits
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:55
 *==========================================================================*/
static BOOL Unload_MonitoredEquipmentUnits(IN OUT SITE_INFO *pSite)
{
	int				n;
	EQUIP_INFO		*pEquip;

	Unload_ActiveAlarms(pSite);
	Unload_DataFiles(pSite);

	for (pEquip = pSite->pEquipInfo, n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
//		TRACEX("Unloading %s(%d).\n", 
//			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		Unload_EquipInfo(pSite, pEquip);
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_SamplerA
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO         *pSite    : 
 *            IN OUT SAMPLER_INFO  *pSampler : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:55
 *==========================================================================*/
static BOOL Unload_SamplerA (IN SITE_INFO *pSite,
						   IN OUT SAMPLER_INFO *pSampler)
{
	UNUSED(pSite);

	// delete the sampling data buffer
	SAFELY_DELETE(pSampler->pChannelData);

	// delete all sig value ref ptr table
	SAFELY_DELETE(pSampler->ppSigValues);
	pSampler->nMaxUsedChannel = 0;

	// delete reference list of related equips
	SAFELY_DELETE(pSampler->ppRelatedEquipment);
	pSampler->nRelatedEquipment = 0;

	// delete the buffer to read data from sampler
	SAFELY_DELETE(pSampler->pChannelData);

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : Unload_Samplers
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:55
 *==========================================================================*/
static BOOL Unload_Samplers(IN OUT SITE_INFO *pSite)
{
	int			 n;
	SAMPLER_INFO *pSampler = pSite->pSamplerInfo;

	for (n = 0; n < pSite->iSamplerNum; n++, pSampler++)
	{
		Unload_SamplerA(pSite, pSampler);
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_StdSamplerA
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO            *pSite       : 
 *            IN OUT STDSAMPLER_INFO  *pStdSampler : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:55
 *==========================================================================*/
static BOOL Unload_StdSamplerA(IN SITE_INFO *pSite, 
							 IN OUT STDSAMPLER_INFO *pStdSampler)
{
	UNUSED(pSite);

	pStdSampler->iRef = 0;

	UnloadDynamicLibrary(pStdSampler->hSamplerLib);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_StdSamplers
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:55
 *==========================================================================*/
static BOOL Unload_StdSamplers(IN OUT SITE_INFO *pSite)
{
	int				n;
	STDSAMPLER_INFO *pStdSampler = pSite->pStdSamplerInfo;

	for (n = 0; n < pSite->iStdSamplerNum; n++, pStdSampler++)
	{
		Unload_StdSamplerA(pSite, pStdSampler);
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_SamplingPortA
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO      *pSite : 
 *            IN OUT PORT_INFO  *pPort : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:55
 *==========================================================================*/
static BOOL Unload_SamplingPortA(IN  SITE_INFO *pSite, IN OUT PORT_INFO *pPort)
{
	UNUSED(pSite);

	// delete the attached samplers list
	SAFELY_DELETE(pPort->ppAttachedSamplers);
	pPort->nAttachedSamplers = 0;

	// destroy the ctrl cmd queue
	if (pPort->hqControlCommand != NULL)
	{
		Queue_Destroy(pPort->hqControlCommand);
		pPort->hqControlCommand = NULL;
	}

	// close the opened port handle
	if (pPort->hPort != NULL)
	{
		CommClose(pPort->hPort);
		pPort->hPort = NULL;
	}

	// destroy the synchronization lock
	if (pPort->hSyncLock != NULL)
	{
		Mutex_Destroy(pPort->hSyncLock);
		pPort->hSyncLock = NULL;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_SamplingPorts
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 15:55
 *==========================================================================*/
static BOOL Unload_SamplingPorts(IN OUT SITE_INFO *pSite)
{
	int			n;
	PORT_INFO	*pPort = pSite->pPortInfo;

	for (n = 0; n < pSite->iPortNum; n++, pPort++)
	{
		Unload_SamplingPortA(pSite, pPort);
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_CtrlCmdArray
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-08 16:07
 *==========================================================================*/
static BOOL Unload_CtrlCmdArray(IN OUT SITE_INFO *pSite)
{
	EQUIP_CTRL_CMD_ARRAY *pCmdArray;

	if (pSite->hCmdArray != NULL)
	{
		int		i;
		EQUIP_CTRL_CMD *pCmd;

		pCmdArray = (EQUIP_CTRL_CMD_ARRAY *)pSite->hCmdArray;

		Mutex_Destroy(pCmdArray->hSyncLock);

		// clear the hSyncSem for each cmd item
		pCmd = pCmdArray->pCmds;
		for (i = 0; i < MAX_ALLOWED_CTRL_CMDS; i++, pCmd++)
		{
			if (pCmd->hSyncSem != NULL)
			{
				Sem_Destroy(pCmd->hSyncSem);
			}
		}

		DELETE(pCmdArray);

		pSite->hCmdArray = NULL;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : Unload_ActiveAlarms
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-08 16:07
 *==========================================================================*/
static BOOL Unload_ActiveAlarms(IN OUT SITE_INFO *pSite)
{
	UNUSED(pSite);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Unload_DataFiles
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-16 12:44
 *==========================================================================*/
static BOOL Unload_DataFiles(IN OUT SITE_INFO *pSite)
{
	HANDLE	pFileHandle[] = 
	{
		pSite->hHistoricalAlarm,// historical alarm		
		pSite->hHistoricalData,	// history data
		pSite->hStatData,		// stat data
		pSite->hActiveAlarm,	// active alarm
		pSite->hControlLog,		// ctrl log
		pSite->hPersistentSig,	// persistent signal
	};
	int		i;

	AppLogOut(EQP_UNLDR, APP_LOG_UNUSED, 
		"Closing historical and runtime data files...\n");

	// 1. create the data files.
	for (i = 0; i < ITEM_OF(pFileHandle); i++)
	{
		if (pFileHandle[i] != NULL)
		{
			DAT_StorageClose(pFileHandle[i]);
		}
	}

	AppLogOut(EQP_UNLDR, APP_LOG_UNUSED, 
		"Historical and runtime data files are closed.\n");

	return TRUE;
}


#ifdef PRODUCT_INFO_SUPPORT
#define Unload_DeviceA(_pDevice)  do { \
	SAFELY_DELETE((_pDevice)->ppRelatedEquip); \
	SAFELY_DELETE((_pDevice)->pConvertor);     \
    } \
	while(0)
	
static BOOL Unload_Devices(IN OUT SITE_INFO *pSite)
{
	int i;
	DEVICE_INFO *pDevice;

	pDevice = pSite->pDeviceInfo;
	for (i = 0; i < pSite->iDeviceNum; i++, pDevice++)
	{
		Unload_DeviceA(pDevice);
	}

	DELETE(pSite->pDeviceInfo);

	return TRUE;
}
#endif //PRODUCT_INFO_SUPPORT
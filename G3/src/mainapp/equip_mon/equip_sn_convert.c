/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_sn_convert.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-10 16:57
 *  VERSION  : V1.00
 *  PURPOSE  : convert equipment serial from the sampling signal.
 *             NOTE: these functions ought put to the sampler driver.
 *  
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "cfg_model.h"
#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "equip_mon.h"

#ifndef PRODUCT_INFO_SUPPORT
#define PRODUCT_INFO_SUPPORT
#endif

typedef struct _EQUIP_SER_CONVERTER_LIST
{
	int							nStdEquipTypeID;
	EQUIP_SERIAL_CONVERT_PROC	pfnConvertProc;
} EQUIP_SER_CONVERTER_LIST;

#ifdef PRODUCT_INFO_SUPPORT
typedef struct _PSC_PI_CONVERTER_CFGLIST
{
	int                         nConvertorType;
	int							nStdEquipTypeID;
	int                         nRelevantSigID;
	EQUIP_PI_CONVERT_PROC		pfnConvertProc;
} PSC_PI_CONVERTER_CFGLIST;
#endif //PRODUCT_INFO_SUPPORT


#define _DEF_EQUIP_SER_CONVERTER_LIST(type, proc)	\
	{ (type), (EQUIP_SERIAL_CONVERT_PROC)(proc) }


#ifdef PRODUCT_INFO_SUPPORT
#define _DEF_PI_CONVERTER_LIST(cType,eType, sigID, proc)	\
	{ (cType), (eType), (sigID), (EQUIP_SERIAL_CONVERT_PROC)(proc) }
#endif //PRODUCT_INFO_SUPPORT
static char *Rect_PartNumber_Converter(IN char *pRawPartNumber,
									   IN int nRawLen, 
									   OUT char *pStrPartNumber,
									   IN int nStrLen);
static char *SN_LoRectID_Converter_His(IN DWORD *pRawSerial,IN DWORD *pRawSerial2,
								   IN int nRawLen,
								   OUT char *pStrSerial,
								   IN int nStrLen);									   
static DWORD  SN_GetRectHighSNValue(IN int iEquipID)
{
	int iBufLen;
	SIG_BASIC_VALUE*		pSigValue = NULL;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECT_HI_SN),
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (pSigValue && (iError == ERR_DXI_OK))
	{
		return pSigValue->varValue.ulValue;
	}
	return 1;
}

static DWORD  *SN_GetRectBarcode(IN int iEquipID,int iIndex)
{
	int iBufLen;
	SIG_BASIC_VALUE*		pSigValue = NULL;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECT_PARTNUMBER + iIndex),
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (pSigValue && (iError == ERR_DXI_OK))
	{
		return &(pSigValue->varValue.ulValue);
	}
	return NULL;
}

static DWORD  *SN_GetSMDUBarcode(IN int iEquipID,int iIndex)
{
	int iBufLen;
	SIG_BASIC_VALUE*		pSigValue = NULL;
	int	iError;

	TRACE("iEquipID:%d\n",iEquipID);

		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iEquipID,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 95 + iIndex),
			&iBufLen,			
			(void *)&pSigValue,			
			0);

	if (pSigValue && (iError == ERR_DXI_OK))
	{
		return &(pSigValue->varValue.ulValue);
	}
	return NULL;
}

static int SN_GetRectHighSNIndex(void)
{
	int iBufLen;
	SAMPLE_SIG_INFO		*pSampleSigInfo;
	int	iError = DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
							RECT_STD_EQUIP_ID,			
							SIG_ID_RECT_HI_SN,		
							&iBufLen,			
							(void *)&pSampleSigInfo,			
							0);
	if (iError == ERR_DXI_OK)
	{
		 return (pSampleSigInfo->iValueDisplayID - 1);
	}
	
	return -1;
}

static char *SN_RectID_Converter(IN DWORD *pRawSerial,
								 IN int nRawLen,
								 OUT char *pStrSerial,
								 IN int nStrLen)
{

	ASSERT(nRawLen == sizeof(DWORD));

	// do NOT convert when the ID is 0 while reordering the CAN addr.
	if (*pRawSerial != 0)
	{
		snprintf(pStrSerial, (size_t)nStrLen, "2%02d%01X%06d", 
			(((*pRawSerial)>> 24) & 0x1F),
			(((*pRawSerial)>> 16) & 0x0F),
			LOWORD((DWORD)(*pRawSerial)));
	}

	return pStrSerial;

	//ASSERT(nRawLen == sizeof(DWORD));
	//static	int			iSN_RectHighIndex = -1;
	//int					k;

	//if(iSN_RectHighIndex == -1)
	//{
	//	iSN_RectHighIndex = SN_GetRectHighSNIndex();
	//}

	//// do NOT convert when the ID is 0 while reordering the CAN addr.
	//if (*pRawSerial != 0) // for 50A AC/DC rectifier module. by HULONGWEN
	//{
	//	if(!SIG_VALUE_IS_CONFIGURED(&pEquip->pSampleSigValue[iSN_RectHighIndex]))
	//	{
	//		snprintf(pStrSerial, (size_t)nStrLen, "2%02d%01X%06d", 
	//			(((BYTE*)pRawSerial)[0] & 0x1F),
	//			(((BYTE*)pRawSerial)[1] & 0x0F),
	//			LOWORD((DWORD)(*pRawSerial)));
	//	}
	//	else
	//	{
	//		snprintf(pStrSerial, (size_t)nStrLen, "%02u%02d%02d%05d", 
	//			SN_GetRectHighSNValue(pEquip->iEquipID),
	//			(((BYTE*)pRawSerial)[0] & 0x1F),
	//			(((BYTE*)pRawSerial)[1] & 0x0F),
	//			LOWORD((DWORD)(*pRawSerial)));
	//	}
	//}

	//return pStrSerial;
}


// default converter.
static char *SN_DefaultConverter(IN DWORD *pRawSerial,
								 IN int nRawLen,
								 OUT char *pStrSerial,
								 IN int nStrLen)
{

	if(nRawLen == sizeof(DWORD))
	{
		snprintf(pStrSerial, (size_t)nStrLen, "%08X",
			(unsigned int)*pRawSerial);
	}

	return pStrSerial;
}


/*==========================================================================*
 * FUNCTION : SN_GetEquipSerialConverter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  nStdEquipTypeID : 
 * RETURN   : EQUIP_SERIAL_CONVERT_PROC : return default converter if not found.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-10 17:17
 *==========================================================================*/
EQUIP_SERIAL_CONVERT_PROC2 SN_GetEquipSerialConverter(IN char *pTypeEngName,
													 IN int nStdEquipTypeID)
{
	int			i;
	EQUIP_SER_CONVERTER_LIST	list[] = 
	{
			//
		//_DEF_EQUIP_SER_CONVERTER_LIST(RECT_STD_EQUIP_ID, SN_RectID_Converter),
		_DEF_EQUIP_SER_CONVERTER_LIST(RECT_STD_EQUIP_ID, SN_LoRectID_Converter_His),		
	};

	UNUSED(pTypeEngName);
	
	for (i = 0; i < ITEM_OF(list); i++)
	{
		if (list[i].nStdEquipTypeID == nStdEquipTypeID)
		{
			return list[i].pfnConvertProc;
		}
	}
	
	return (EQUIP_SERIAL_CONVERT_PROC)SN_DefaultConverter;	// default.
}


#ifdef PRODUCT_INFO_SUPPORT
/*==========================================================================*
 * FUNCTION : *SN_HiRectID_Converter
 * PURPOSE  : for High part of Serial number process
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN DWORD  *pRawSerial : 
 *            IN int    nRawLen     : 
 *            OUT char  *pStrSerial : 
 *            IN int    nStrLen     : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-06 16:00
 *==========================================================================*/
static char *SN_HiRectID_Converter(IN DWORD *pRawSerial,
								   IN int nRawLen, 
								   OUT char *pStrSerial,
								   IN int nStrLen)
{
	ASSERT(nRawLen == sizeof(DWORD));

	//Old rect does not have Hi-serial
	if (*pRawSerial == 0xFF)
	{
		pStrSerial[0] = 0;
	}
	else
	{
		snprintf(pStrSerial, (size_t)nStrLen, "%02u", *pRawSerial);
	}

	return pStrSerial;
}

//new rectifier, format is different
static char *SN_LoRectID_Converter(IN DWORD *pRawSerial,
								   IN int nRawLen,
								   OUT char *pStrSerial,
								   IN int nStrLen)
{
	ASSERT(nRawLen == sizeof(DWORD));

	// do NOT convert when the ID is 0 while reordering the CAN addr.
	if (*pRawSerial != 0)
	{
		snprintf(pStrSerial, (size_t)nStrLen, "%02d%02d%05d", 
			(((*pRawSerial)>> 24) & 0x1F),
			(((*pRawSerial)>> 16) & 0x0F),
			LOWORD((DWORD)(*pRawSerial)));
	}

	return pStrSerial;
}

//new rectifier, format is different
static char *SN_LoRectID_Converter_His(IN DWORD *pRawSerial,IN DWORD *pRawSerial2,
								   IN int nRawLen,
								   OUT char *pStrSerial,
								   IN int nStrLen)
{
	ASSERT(nRawLen == sizeof(DWORD));

	
	
	

	// do NOT convert when the ID is 0 while reordering the CAN addr.
	if (*pRawSerial != 0 && *pRawSerial2 != 0)
	{
		snprintf(pStrSerial, (size_t)nStrLen, "%02u%02d%02d%05d", 
			(unsigned long)*pRawSerial2,
			(((*pRawSerial)>> 24) & 0x1F),
			(((*pRawSerial)>> 16) & 0x0F),
			LOWORD((DWORD)(*pRawSerial)));
	}

 

	
	return pStrSerial;
}

//#define			START_RECT_EQUIPID					3
///*==========================================================================*
// * FUNCTION : RECT_GetRectBarcode
// * PURPOSE  : 
// * CALLS    : 
// * CALLED BY: 
// * ARGUMENTS: IN int       iEquipID         : 
// *            IN OUT char  *pRectPartNumber : 
// * RETURN   : BOOL : 
// * COMMENTS : 
// * CREATOR  : LXD                      DATE: 2006-11-02 13:03
// *==========================================================================*/
//BOOL RECT_GetRectBarcode(IN int iEquipID, IN OUT char *pRectPartNumber)
//{
//	char *pBarcode;
//	BYTE  byRectNo;
//	byRectNo = iEquipID - START_RECT_EQUIPID;
//
//	ASSERT(byRectNo < MAX_RECTS_NUM);
//
//	pBarcode = g_RectCommData[byRectNo].szSerialNumer;
//	snprintf(pRectPartNumber, MAX_SERIAL_NO_LEN, "%s", pBarcode);
//
//	return TRUE;
//}



/*==========================================================================*
 * FUNCTION : *Rect_PartNumber_Converter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN DWORD  *pRawPartNumber : 
 *            IN int    nRawLen			: 
 *            OUT char  *pStrPartNumber : 
 *            IN int    nStrLen			: 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-06 16:02
 *==========================================================================*/
static char *Rect_PartNumber_Converter(IN char *pRawPartNumber,
									   IN int nRawLen, 
									   OUT char *pStrPartNumber,
									   IN int nStrLen)
{

	int nLen = nStrLen/4;
	int nReturn;
	
	int *pEquipID = (int *)(pRawPartNumber);
	int  iEquipID = *pEquipID;
	DWORD *dwPartNumber =  NULL;
	
	
#ifdef RECT_PARTNUMBER_HARDCODED
	//Part number of Rectifier is hard coded in PSC
	snprintf(pStrPartNumber, nStrLen, "%s", PSC_RECT_PARTNUMBER);
#else

	////get barcode1,barcode2,barcode3,barcode4
	
	dwPartNumber = SN_GetRectBarcode(iEquipID,0);
	if(dwPartNumber != NULL)
	{
		nReturn = sprintf(pStrPartNumber + 0, "%c%c", ((char *)dwPartNumber)[1],((char *)dwPartNumber)[0]);
		//printf("pStrPartNumber1 = %s\n", pStrPartNumber);
	}
	dwPartNumber = SN_GetRectBarcode(iEquipID,1);
	if(dwPartNumber != NULL)
	{
		nReturn += sprintf(pStrPartNumber + nReturn, "%c%c%c%c", ((char *)dwPartNumber)[3],((char *)dwPartNumber)[2], ((char *)dwPartNumber)[1],((char *)dwPartNumber)[0]);
		//printf("pStrPartNumber2 = %s\n", pStrPartNumber);
	}
	dwPartNumber = SN_GetRectBarcode(iEquipID,2);
	if(dwPartNumber != NULL)
	{
		nReturn += sprintf(pStrPartNumber + nReturn, "%c%c%c%c",  ((char *)dwPartNumber)[3],((char *)dwPartNumber)[2], ((char *)dwPartNumber)[1],((char *)dwPartNumber)[0]);
		//printf("pStrPartNumber3 = %s\n", pStrPartNumber);
	}
	dwPartNumber = SN_GetRectBarcode(iEquipID,3);
	if(dwPartNumber != NULL)
	{
		nReturn += sprintf(pStrPartNumber + nReturn, "%c%c%c%c",((char *)dwPartNumber)[3],((char *)dwPartNumber)[2], ((char *)dwPartNumber)[1],((char *)dwPartNumber)[0]);
		//printf("pStrPartNumber4 = %s\n", pStrPartNumber);
	}
#endif //RECT_PARTNUMBER_HARDCODED

	if (pStrPartNumber[nStrLen - 1])
	{
		pStrPartNumber[nStrLen - 1] = 0;
	}

	return pStrPartNumber;
}

static char *SMDU_PartNumber_Converter(IN char *pRawPartNumber,
									   IN int nRawLen, 
									   OUT char *pStrPartNumber,
									   IN int nStrLen)
{

	TRACE("Into SMDU_PartNumber_Converter!!!!!\n");
	int nLen = nStrLen/4;
	int nReturn;
	
	int iEquipID = (int *)(*pRawPartNumber);
	DWORD *dwPartNumber =  NULL;
	
	

	
	dwPartNumber = SN_GetSMDUBarcode(iEquipID,0);
	if(dwPartNumber != NULL)
	{
		nReturn = sprintf(pStrPartNumber + 0, "%c%c", ((char *)dwPartNumber)[1],((char *)dwPartNumber)[0]);
		//printf("pStrPartNumber1 = %s\n", pStrPartNumber);
	}
	dwPartNumber = SN_GetSMDUBarcode(iEquipID,1);
	if(dwPartNumber != NULL)
	{
		nReturn += sprintf(pStrPartNumber + nReturn, "%c%c%c%c", ((char *)dwPartNumber)[3],((char *)dwPartNumber)[2], ((char *)dwPartNumber)[1],((char *)dwPartNumber)[0]);
	
		//printf("pStrPartNumber2 = %s\n", pStrPartNumber);
	}
	dwPartNumber = SN_GetSMDUBarcode(iEquipID,2);
	if(dwPartNumber != NULL)
	{
		nReturn += sprintf(pStrPartNumber + nReturn, "%c%c%c%c",  ((char *)dwPartNumber)[3],((char *)dwPartNumber)[2], ((char *)dwPartNumber)[1],((char *)dwPartNumber)[0]);
	
		//printf("pStrPartNumber3 = %s\n", pStrPartNumber);
	}
	dwPartNumber = SN_GetSMDUBarcode(iEquipID,3);
	if(dwPartNumber != NULL)
	{
		nReturn += sprintf(pStrPartNumber + nReturn, "%c%c%c%c",((char *)dwPartNumber)[3],((char *)dwPartNumber)[2], ((char *)dwPartNumber)[1],((char *)dwPartNumber)[0]);
	
		//printf("pStrPartNumber4 = %s\n", pStrPartNumber);
	}

	//Jimmyע�ͣ���ΪSMDU��������FF���������һλ��������룬�����һ���жϣ���������⣬20120516
	for(nReturn = 0; nReturn < nStrLen; nReturn++)
	{
		if(pStrPartNumber[nReturn] == (char)0xFF || pStrPartNumber[nReturn] == (char)0x20)
		{
			pStrPartNumber[nReturn] = 0;//��ʾ����
			break;
		}
	}

	//if (pStrPartNumber[nStrLen - 1])
	//{
	//	pStrPartNumber[nStrLen - 1] = 0;
	//}

	return pStrPartNumber;
}

/*==========================================================================*
 * FUNCTION : *Rect_HWVer_Converter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN DWORD  *pRawVer : 
 *            IN int    nRawLen    : 
 *            OUT char  *pStrHWVer : 
 *            IN int    nStrLen    : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-06 16:27
 *==========================================================================*/
static char *Rect_HWVer_Converter(IN DWORD *pRawVer,
								  IN int nRawLen,
								  OUT char *pStrHWVer,
								  IN int nStrLen)
{
	ASSERT(nRawLen == sizeof(DWORD));
	BYTE byTempValue = 0;
	byTempValue = ((*pRawVer)>>16)& 0xff;

	//pStrHWVer[0] = ((BYTE*)pRawVer)[0] + 'A';
	pStrHWVer[0] = (BYTE*)(*pRawVer>>24) + 'A';
	//snprintf(pStrHWVer+1, (size_t)nStrLen-1, "%02d", ((BYTE*)pRawVer)[1]);
	snprintf(pStrHWVer+1, (size_t)nStrLen-1, "%02d", byTempValue);


	if (pStrHWVer[nStrLen-1])
	{
		pStrHWVer[nStrLen-1] = 0;
	}
	
	return pStrHWVer;
}


/*==========================================================================*
 * FUNCTION : *Rect_SWVer_Converter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN DWORD  *pRawVer   : 
 *            IN int    nRawLen    : 
 *            OUT char  *pStrSWVer : 
 *            IN int    nStrLen    : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-06 17:06
 *==========================================================================*/
static char *Rect_SWVer_Converter(IN DWORD *pRawVer,
								  IN int nRawLen,
								  OUT char *pStrSWVer,
								  IN int nStrLen)
{
	ASSERT(nRawLen == sizeof(DWORD));

	ZERO_POBJS(pStrSWVer,nStrLen);
	
	if( ((WORD*)pRawVer)[0] > 999)
	{
		pStrSWVer[0] = ((WORD*)pRawVer)[0]/1000 + '0';
		pStrSWVer[1] = '.';
		snprintf(pStrSWVer+2, (size_t)nStrLen-2, "%03d", ((WORD*)pRawVer)[0]%1000);
	}
	else
	{
		pStrSWVer[0] = ((WORD*)pRawVer)[0]/100 + '0';
		pStrSWVer[1] = '.';
		snprintf(pStrSWVer+2, (size_t)nStrLen-2, "%02d", ((WORD*)pRawVer)[0]%100);
	}
	if (pStrSWVer[nStrLen-1])
	{
		pStrSWVer[nStrLen-1] = 0;
	}

	return pStrSWVer;
}



static char *SMDU_HWVer_Converter(IN DWORD *pRawVer,
								  IN int nRawLen,
								  OUT char *pStrHWVer,
								  IN int nStrLen)
{
	ASSERT(nRawLen == sizeof(DWORD));

	WORD wdHWVer = (*pRawVer) >> 16;

	pStrHWVer[0] = wdHWVer / 100 + 'A';

	snprintf(pStrHWVer + 1, (size_t)nStrLen - 1, "%02d", wdHWVer % 100);

	pStrHWVer[nStrLen - 1] = NULL;

	return pStrHWVer;
}

static char *SMDU_SWVer_Converter(IN DWORD *pRawVer,
								  IN int nRawLen,
								  OUT char *pStrSWVer,
								  IN int nStrLen)
{
	ASSERT(nRawLen == sizeof(DWORD));

	WORD wdSWVer = (*pRawVer) & 0xFFFF;
	pStrSWVer[0] = wdSWVer / 100 + '0';
	pStrSWVer[1] = '.';
	snprintf(pStrSWVer + 2, (size_t)nStrLen - 2, "%02d", wdSWVer % 100);

	pStrSWVer[nStrLen - 1] = NULL;

	return pStrSWVer;
}



/*==========================================================================*
 * FUNCTION : PI_GetConverterCfg
 * PURPOSE  : This function only used to convert rectifier equip barcode
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT PI_CONVERTOR  *pConvertor : 
 *            IN                   nStdEquipID : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-08 10:07
 *==========================================================================*/
BOOL PI_GetConverterCfg(IN OUT PI_CONVERTOR *pConvertor,
						IN int nStdEquipID)
{
	int	 nConvertorType;
	int  i;

	/* note: 0 means ref sig is not configured */
	static PSC_PI_CONVERTER_CFGLIST	list[] = 
	{
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, RECT_STD_EQUIP_ID, 
			SIG_ID_RECT_SN, SN_RectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, RECT_STD_EQUIP_ID, 
			SIG_ID_RECT_HI_SN, SN_HiRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, RECT_STD_EQUIP_ID, 
			SIG_ID_RECT_SN, SN_LoRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, RECT_STD_EQUIP_ID,
			SIG_ID_RECT_PARTNUMBER, Rect_PartNumber_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, RECT_STD_EQUIP_ID,
			SIG_ID_RECT_VER, Rect_HWVer_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, RECT_STD_EQUIP_ID,
			SIG_ID_RECT_VER, Rect_SWVer_Converter),
	};

#define SIG_ID_SMDU_VER			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 92)
#define SIG_ID_SMDU_HI_SN		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 93)
#define SIG_ID_SMDU_LO_SN		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 94)

	static PSC_PI_CONVERTER_CFGLIST	list_smdu[] = 
	{
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU1_STD_EQUIP_ID, 
			SIG_ID_OTHER_SN, SN_RectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU1_STD_EQUIP_ID, 
			SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU1_STD_EQUIP_ID, 
			SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU1_STD_EQUIP_ID,
			SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU1_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU1_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_SWVer_Converter),

			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU2_STD_EQUIP_ID, 
			SIG_ID_OTHER_SN, SN_RectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU2_STD_EQUIP_ID, 
			SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU2_STD_EQUIP_ID, 
			SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU2_STD_EQUIP_ID,
			SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU2_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU2_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_SWVer_Converter),

			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU3_STD_EQUIP_ID, 
			SIG_ID_OTHER_SN, SN_RectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU3_STD_EQUIP_ID, 
			SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU3_STD_EQUIP_ID, 
			SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU3_STD_EQUIP_ID,
			SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU3_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU3_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_SWVer_Converter),

			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU4_STD_EQUIP_ID, 
			SIG_ID_OTHER_SN, SN_RectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU4_STD_EQUIP_ID, 
			SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU4_STD_EQUIP_ID, 
			SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU4_STD_EQUIP_ID,
			SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU4_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU4_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_SWVer_Converter),

			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU5_STD_EQUIP_ID, 
			SIG_ID_OTHER_SN, SN_RectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU5_STD_EQUIP_ID, 
			SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU5_STD_EQUIP_ID, 
			SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU5_STD_EQUIP_ID,
			SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU5_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU5_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_SWVer_Converter),

			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU6_STD_EQUIP_ID, 
			SIG_ID_OTHER_SN, SN_RectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU6_STD_EQUIP_ID, 
			SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU6_STD_EQUIP_ID, 
			SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU6_STD_EQUIP_ID,
			SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU6_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU6_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_SWVer_Converter),

			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU7_STD_EQUIP_ID, 
			SIG_ID_OTHER_SN, SN_RectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU7_STD_EQUIP_ID, 
			SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU7_STD_EQUIP_ID, 
			SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU7_STD_EQUIP_ID,
			SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU7_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU7_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_SWVer_Converter),

			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU8_STD_EQUIP_ID, 
			SIG_ID_OTHER_SN, SN_RectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU8_STD_EQUIP_ID, 
			SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU8_STD_EQUIP_ID, 
			SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU8_STD_EQUIP_ID,
			SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU8_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
			_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU8_STD_EQUIP_ID,
			SIG_ID_SMDU_VER, SMDU_SWVer_Converter),

			//_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDU_STD_EQUIP_ID, 
			//SIG_ID_OTHER_SN, SN_RectID_Converter),
			//_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDU_STD_EQUIP_ID, 
			//SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
			//_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDU_STD_EQUIP_ID, 
			//SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
			//_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDU_STD_EQUIP_ID,
			//SIG_ID_OTHER_PARTNUMBER1, SMDU_PartNumber_Converter),
			//_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDU_STD_EQUIP_ID,
			//SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
			//_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDU_STD_EQUIP_ID,
			//SIG_ID_SMDU_VER, SMDU_SWVer_Converter),
	};

	//static PSC_PI_CONVERTER_CFGLIST	list_smdup[] = 
	//{
	//	_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, SMDUP_STD_EQUIP_ID, 
	//	SIG_ID_OTHER_SN, SN_RectID_Converter),
	//	_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, SMDUP_STD_EQUIP_ID, 
	//	SIG_ID_SMDU_HI_SN, SN_HiRectID_Converter),
	//	_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, SMDUP_STD_EQUIP_ID, 
	//	SIG_ID_SMDU_LO_SN, SN_LoRectID_Converter),	//bug: only when Field of SMDU <= 3(2bits) is OK
	//	_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, SMDUP_STD_EQUIP_ID,
	//	SIG_ID_OTHER_PARTNUMBER1, SMDUP_PartNumber_Converter),
	//	_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, SMDUP_STD_EQUIP_ID,
	//	SIG_ID_SMDU_VER, SMDU_HWVer_Converter),
	//	_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, SMDUP_STD_EQUIP_ID,
	//	SIG_ID_SMDU_VER, SMDU_SWVer_Converter),
	//};

	static PSC_PI_CONVERTER_CFGLIST	list_SLAVE1[] = 
	{
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, RECT_STD_EQUIP_ID_SLAVE1, 
			SIG_ID_RECT_SN, SN_RectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, RECT_STD_EQUIP_ID_SLAVE1, 
			SIG_ID_RECT_HI_SN, SN_HiRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, RECT_STD_EQUIP_ID_SLAVE1, 
			SIG_ID_RECT_SN, SN_LoRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, RECT_STD_EQUIP_ID_SLAVE1,
			SIG_ID_RECT_PARTNUMBER, Rect_PartNumber_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, RECT_STD_EQUIP_ID_SLAVE1,
			SIG_ID_RECT_VER, Rect_HWVer_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, RECT_STD_EQUIP_ID_SLAVE1,
			SIG_ID_RECT_VER, Rect_SWVer_Converter),
	};
	
	static PSC_PI_CONVERTER_CFGLIST	list_SLAVE2[] = 
	{
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, RECT_STD_EQUIP_ID_SLAVE2, 
		SIG_ID_RECT_SN, SN_RectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, RECT_STD_EQUIP_ID_SLAVE2, 
		SIG_ID_RECT_HI_SN, SN_HiRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, RECT_STD_EQUIP_ID_SLAVE2, 
		SIG_ID_RECT_SN, SN_LoRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, RECT_STD_EQUIP_ID_SLAVE2,
		SIG_ID_RECT_PARTNUMBER, Rect_PartNumber_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, RECT_STD_EQUIP_ID_SLAVE2,
		SIG_ID_RECT_VER, Rect_HWVer_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, RECT_STD_EQUIP_ID_SLAVE2,
		SIG_ID_RECT_VER, Rect_SWVer_Converter),
	};
	static PSC_PI_CONVERTER_CFGLIST	list_SLAVE3[] = 
	{
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SERIAL, RECT_STD_EQUIP_ID_SLAVE3, 
		SIG_ID_RECT_SN, SN_RectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HI_SERIAL, RECT_STD_EQUIP_ID_SLAVE3, 
		SIG_ID_RECT_HI_SN, SN_HiRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_LO_SERIAL, RECT_STD_EQUIP_ID_SLAVE3, 
		SIG_ID_RECT_SN, SN_LoRectID_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_PART_NUMBER, RECT_STD_EQUIP_ID_SLAVE3,
		SIG_ID_RECT_PARTNUMBER, Rect_PartNumber_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_HW_VER, RECT_STD_EQUIP_ID_SLAVE3,
		SIG_ID_RECT_VER, Rect_HWVer_Converter),
		_DEF_PI_CONVERTER_LIST(PI_CONVERTOR_SW_VER, RECT_STD_EQUIP_ID_SLAVE3,
		SIG_ID_RECT_VER, Rect_SWVer_Converter),
	};
	nConvertorType = pConvertor->eConvertorType;
	for (i = 0; i < ITEM_OF(list); i++)
	{
		if (list[i].nConvertorType == nConvertorType &&
			list[i].nStdEquipTypeID == nStdEquipID) 
		{
			pConvertor->pfnConverProc = list[i].pfnConvertProc;
			pConvertor->iRelevantSigID = list[i].nRelevantSigID;

			return TRUE;
		}
	}
	
	for (i = 0; i < ITEM_OF(list_smdu); i++)
	{
		if (list_smdu[i].nConvertorType == nConvertorType &&
			list_smdu[i].nStdEquipTypeID == nStdEquipID) 
		{
			pConvertor->pfnConverProc = list_smdu[i].pfnConvertProc;
			pConvertor->iRelevantSigID = list_smdu[i].nRelevantSigID;

			return TRUE;
		}
	}
	/*for (i = 0; i < ITEM_OF(list_smdup); i++)
	{
		if (list_smdup[i].nConvertorType == nConvertorType &&
			list_smdup[i].nStdEquipTypeID == nStdEquipID) 
		{
			pConvertor->pfnConverProc = list_smdup[i].pfnConvertProc;
			pConvertor->iRelevantSigID = list_smdup[i].nRelevantSigID;

			return TRUE;
		}
	}*/
	for (i = 0; i < ITEM_OF(list_SLAVE1); i++)
	{
		if (list_SLAVE1[i].nConvertorType == nConvertorType &&
			list_SLAVE1[i].nStdEquipTypeID == nStdEquipID) 
		{
			pConvertor->pfnConverProc = list_SLAVE1[i].pfnConvertProc;
			pConvertor->iRelevantSigID = list_SLAVE1[i].nRelevantSigID;

			return TRUE;
		}
	}
	for (i = 0; i < ITEM_OF(list_SLAVE2); i++)
	{
		if (list_SLAVE2[i].nConvertorType == nConvertorType &&
			list_SLAVE2[i].nStdEquipTypeID == nStdEquipID) 
		{
			pConvertor->pfnConverProc = list_SLAVE2[i].pfnConvertProc;
			pConvertor->iRelevantSigID = list_SLAVE2[i].nRelevantSigID;

			return TRUE;
		}
	}
	for (i = 0; i < ITEM_OF(list_SLAVE3); i++)
	{
		if (list_SLAVE3[i].nConvertorType == nConvertorType &&
			list_SLAVE3[i].nStdEquipTypeID == nStdEquipID) 
		{
			pConvertor->pfnConverProc = list_SLAVE3[i].pfnConvertProc;
			pConvertor->iRelevantSigID = list_SLAVE3[i].nRelevantSigID;

			return TRUE;
		}
	}

	//Not rectifier
	pConvertor->pfnConverProc = 0;
	pConvertor->iRelevantSigID = 0;
	return FALSE;	// not defined
}
#endif //PRODUCT_INFO_SUPPORT
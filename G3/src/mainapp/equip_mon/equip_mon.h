/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_mon.h
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-21 11:20
 *  VERSION  : V1.00
 *  PURPOSE  : The internal functions and structs for equipment monitoring
 *             module.
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __EQUIP_MON_H__
#define __EQUIP_MON_H__

//#include "data_mgmt.h"
//#include "equip_rect_data.h"

//#define EQUIP_MON_DEBUG		1

// for debug to log detail info.
#ifdef _DEBUG_EQUIP_MON
#define _LOG_DETAIL_INFO	// log detailed init process.
#endif


#define ITEM_OF_ARRAY(array)		(int)(sizeof(array) / sizeof(array[0]))

//add support hysteresis for alarm. 2005-3-26
#define ALARM_SUPPORT_HYSTERESIS	1

// sort the standard equipment signals by its display ID. 2005-01-19
#define SORT_STD_SIGNALS_BY_DISPLAY_ID	1

// strictly check the expression variable. 
#define STRICT_CHECK_EXP_VAR		1

//#define DEBUG_HISTORY_ALARMSERIAL	1

// Support convert exp in the real sampling signal. maofuhua. 2005-3-15
#define REAL_SAMPLING_SIG_SUPPORT_EXP	1

#if defined(REAL_SAMPLING_SIG_SUPPORT_EXP) || defined(ALARM_SUPPORT_HYSTERESIS)
// Now allow a sample signal ref itself. maofuhua, 2005-3-15 
#undef STRICT_CHECK_EXP_VAR
#endif

// 1 sampler channel can be used by one or more equipment signal.
//#define SAMPLING_CHANNEL_MULTIPLE_MAPPED	1

// Change: The control signal also has value. Mao Fuhua. 2004-11-24
// see: CTRL_SIG_HAS_SAMPLING_CHANNEL in cfg_model.h. maofuhua, 2005-2-14
//#define CTRL_SIG_HAS_VALUE			1

// EXP_SIG_MUST_EXIST:
// if EXP_SIG_MUST_EXIST=1, if there is any variable[EquipID, SigType, SigID]
// in expression can not be found in solution, the system will failure.
// if EXP_SIG_MUST_EXIST=0, the invalid variablle will be treated as invalid
// and the system can continue run.
#define EXP_SIG_MUST_EXIST			1

// END_ALARM_IF_INVALID_RESULT:
// if there the alarm calculating expression return invalid result, 
// and the alarm is activing, the alarm will be ended.
// =0: only set the alarm state to invalid, do NOT end the alarm.
// =1: set the alarm state to invalid, and end the alarm.
#define END_ALARM_IF_INVALID_RESULT


// defer backup config file timer.
// due to initialization of application services private config maybe error 
// even if the solution config file is loaded OK.
#define DEFER_BACKUP_CONFIG_FILES
#ifdef DEFER_BACKUP_CONFIG_FILES
#define TIMER_INTERVAL_DEFER_BACKUP_CONFIG_FILES		(15*60*1000)	// 15min
#define TIMER_ID_DEFER_BACKUP_CONFIG_FILES				2005
#endif

extern SITE_INFO  g_SiteInfo;

#ifndef GetSiteInfo
#define GetSiteInfo() (&g_SiteInfo)
#endif

// log name to save the current value of setting signal that need persistently
#define	SET_SIG_VAL_LOG		"set_sig_val.log"

// task name used to log out
#define EQP_MON		"EQUIP MAIN"			// main of equip monitoring 
#define EQP_INIT	"EQUIP INIT"
#define EQP_MGR		"EQUIP MGR"
#define EQP_CTRL	"EQUIP CTRL"			// ctrl cmd of equip monitoring 
#define EQP_SMPLR	"EQUIP SAMP"			// data sampler
#define EQP_UNLDR	"EQUIP UNLDR"

#define GET_LANG_TEXT0(pTxt)			((pTxt)->pFullName[0])
#define EQP_GET_EQUIP_NAME0(pEquip)		GET_LANG_TEXT0((pEquip)->pEquipName)
#define EQP_GET_SIGV_NAME0(pSig)		GET_LANG_TEXT0((pSig)->pStdSig->pSigName)
#define EQP_GET_SIG_NAME0(pSig)			GET_LANG_TEXT0((pSig)->pSigName)
#define EQP_GET_SAMPLER_NAME0(pSampler)	GET_LANG_TEXT0((pSampler)->pSamplerName)

#define EQP_GET_PORT_NAME(pPort)		((pPort)->szPortName)
#define EQP_GET_STD_SAMPLER_NAME(pStdSampler)	\
										((pStdSampler)->szStdSamplerName)


// the max time can be used in sending ctrl cmd, if exceed this, the data 
// sample task shall return to sample data from control process.
#define MAX_ALLOWED_CONTROL_TIME		20		// unit is seconds.

#define COMM_READ_RETRY_TIME					2		// try time on reading data.
#define COMM_CTRL_RETRY_TIME					2		// try time on sending ctrl cmd

#define SAMPLER_COMM_OK(pSampler)			((pSampler)->bCommStatus)
#define SAMPLER_JUST_FAILED(pSampler)		\
					((pSampler)->nCommErrors == COMM_READ_RETRY_TIME)

//the value of the not configured sampling channle.
//#define SAMPLER_CHN_NOT_CFG_VAL		(-9999.0f)
#define SAMPLER_CHN_NOT_CFG_VAL			(-9999)
#define SAMPLER_INVALID_VAL			(-9998)//Added by wj for Invalid Signal

//wait time on checking equipment process cmd.
#define WAIT_TIME_CHECKING_PROCESS_CMD	500		// ms.
#define SLEEP_TIME_BEFORE_NEXT_LOOP		1000 

//wait time on checking control command for a port.
//#define WAIT_TIME_CHECKING_CTRL_CMD		100		// ms. 

#define WAIT_TIME_CHECKING_CTRL_CMD		20		// ms. In order to improve sampling speed.11.27

#define EQUIP_MAX_PENDING_CMD		10	// max pending cmd for each equipment

// the stat data will be reset if the data can NOT be reported in 2 hours
#define STAT_DATA_REPORT_OVERTIME	7200

//For B14C3U1
#define		MAX_ACDISTRIBUTE_NUM		5
#define		MAX_DCDISTRIBUTE_NUM		10
//Periodic Fresh CFG and Setting Signal Value
#define		FRESH_CFG_AND_SETTING_CYCLE_NUM						8

//#pragma pack(1)


struct	stssVIDTOSIDCID
{
	DWORD	dwMERGE_SIGID;
	DWORD	wSID;
	DWORD	byCID;
	DWORD	byDataType;
};
typedef struct stssVIDTOSIDCID  ST_VIDTOSIDCID;


// format of control command for a port
struct _EQUIP_CTRL_CMD
{
	DWORD		dwCmdSeq;	// the cmd sequence start from 0

	EQUIP_INFO		*pEquip;
	int				nSigType;
	SET_SIG_VALUE  *pSig;	// the ctrl value shall set to pSig 
							// if sampling channel of the pSig < 0 and ctrl OK.
	VAR_VALUE		varValue;	// the ctrl value.

    char		szSenderName[MAX_CTRL_SENDER_NAME];	// Control Sender Name 
	int			nSenderType;					// Sender Type:see app_service.h:APP_SERVICE_TYPE_ENUM 

	DWORD		dwTimeout;	// timeout in ms,0 for no wait.

// converted command info from the original command
	SAMPLER_INFO *pExecutor;	/* sampler unit */ 
	char	szCmdStr[MAX_CTRL_CMD_LEN];	/* contro parameter */

#define	CTRL_CMD_IS_IDLE		0		// the cmd slot is free to use
#define CTRL_CMD_IS_USED		1		// the cmd slot is used by others
#define CTRL_CMD_IS_PENDING		2		// there is a cmd pending, need to exec
#define CTRL_CMD_IS_EXECUTING	3		// the cmd is being executed
#define CTRL_CMD_IS_EXECUTED	4		// the cmd is executed done.
#define CTRL_CMD_IS_CANCELED	5		// the cmd is canceled by sender
#define CTRL_CMD_IS_END			6		// the cmd is end.
	int			nState;	// the cmd state

	HANDLE		hSyncSem;		// semaphore for waiting exec result

	int			nResult;		// see Equip_Control()
};
typedef struct _EQUIP_CTRL_CMD EQUIP_CTRL_CMD, *PEQUIP_CTRL_CMD;

struct _MESSAGE_TO_UI_
{
	int		iEquipId;
	int		iSigType;
};

typedef struct _MESSAGE_TO_UI_ MESSAGE_TO_UI;

// the cmd array of SITE_INFO. see hCmdArray.
struct _EQUIP_CTRL_CMD_ARRAY
{
	HANDLE			hSyncLock;
	EQUIP_CTRL_CMD	pCmds[MAX_ALLOWED_CTRL_CMDS];
	int				nPendingCmds;
	EQUIP_CTRL_CMD	*pLastEmpty;
};
typedef struct _EQUIP_CTRL_CMD_ARRAY	EQUIP_CTRL_CMD_ARRAY;

struct _stSCUP_CTRLCMD_ARRAY
{
	int		iCtrlChannel;
	int		iEquipID;
	int		iValueType;			//Byte,or Float
	BYTE	bySigType;			//1:control //2:setting
};
typedef struct _stSCUP_CTRLCMD_ARRAY	SCUP_CTRLCMD_ARR;


//#pragma pack()

#define GET_SITE_INFO()		(&g_SiteInfo)

/////////////////////////////////////////////////////////////////////////////
//Define ADC Distrib Read Data Type From Bus
#define		SAMPLING_SIG_DATA_1		0					//Sampling data Value not Include ...
#define		SAMPLING_SIG_DATA_2		1					//Include UpDn and System setting signal value

#define		SAMPLING_SIG_CFG_1		2					//Not Include UpDn and System Cfg
#define		SAMPLING_SIG_CFG_2		3					//Include UpDn and System Setting Cfg

/*-------------------------------------------------------------------------*/




/*==========================================================================*
 * FUNCTION : EquipMonitoring_Init
 * PURPOSE  : load cfg and init monitoring site.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-22 13:12
 *==========================================================================*/
BOOL EquipMonitoring_Init(IN OUT SITE_INFO *pSite);


/*==========================================================================*
 * FUNCTION : EquipMonitoring_Start
 * PURPOSE  : To init the internal data structures, and start the data
 *            sampling tasks and equipment management task.
 * CALLS    : 
 * CALLED BY: main()
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : the site info.
 * RETURN   : BOOL : TRUE for OK.
 * COMMENTS : The configuration of the site shall be loaded OK before calling
 *            this function.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-21 11:10
 *==========================================================================*/
BOOL EquipMonitoring_Start(IN OUT SITE_INFO *pSite);


/*==========================================================================*
 * FUNCTION : EquipMonitoring_Stop
 * PURPOSE  : to stop and cleanup equipment monitoring tasks
 * CALLS    : 
 * CALLED BY: main()
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-21 20:00
 *==========================================================================*/
BOOL EquipMonitoring_Stop( IN OUT SITE_INFO *pSite);



/*==========================================================================*
 * FUNCTION : EquipMonitoring_Unload
 * PURPOSE  : unload cfg and inited memory.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-22 16:22
 *==========================================================================*/
void EquipMonitoring_Unload(IN OUT SITE_INFO *pSite);


/*==========================================================================*
 * FUNCTION : Init_MonitoringSite
 * PURPOSE  : Init the running information of a monitoring site 
 * CALLS    : 
 * CALLED BY: EquipMonitoring_Start().
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : the site object
 * RETURN   : BOOL : TRUE for OK. FALSE for error
 * COMMENTS : The config information of the pSite object must be loaded OK
 *             before calling this function.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-22 13:08
 *==========================================================================*/
BOOL Init_MonitoringSite(IN OUT SITE_INFO *pSite );


/*==========================================================================*
 * FUNCTION : Site_EquipmentManager
 * PURPOSE  : data processor
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 16:56
 *==========================================================================*/
DWORD Site_EquipmentManager(IN OUT SITE_INFO *pSite);


/*==========================================================================*
 * FUNCTION : Sampler_CreateSamplingTasks
 * PURPOSE  : create the sampling task for each port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-03 16:59
 *==========================================================================*/
BOOL Sample_CreateSamplingTasks(IN SITE_INFO *pSite);

/*==========================================================================*
 * FUNCTION : Sample_StopSamplingTasks
 * PURPOSE  : stop the sampling task of each sampling port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : optimized, copied from equip_mon_main.c:EquipMonitoring_Stop
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-14 15:28
 *==========================================================================*/
BOOL Sample_StopSamplingTasks(IN OUT SITE_INFO *pSite);


/*==========================================================================*
 * FUNCTION : Unload_MonitoringSite
 * PURPOSE  : cleanup the runtime data of the site created by the Init_xxxx()
 *            other configuration data shall be unloaded by Cfg_UnloadConfig();
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-29 17:02
 *==========================================================================*/
BOOL Unload_MonitoringSite(IN OUT SITE_INFO *pSite );

/*==========================================================================*
 * FUNCTION : Sample_CheckControl
 * PURPOSE  : check and send the control commands to this port.
 *            if there are too many commands, will return in 10sec.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 *            IN PORT_INFO  *pPort : 
 * RETURN   : SAMPLER_INFO* : the first sampler the cmd is send to
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-05 21:04
 *==========================================================================*/
SAMPLER_INFO *Sample_CheckControl(IN SITE_INFO *pSite, IN PORT_INFO *pPort);


/*==========================================================================*
 * FUNCTION : Sample_ReopenPort
 * PURPOSE  : test need re-open port or not. if need, to open the port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT PORT_INFO  *pPort        : 
 *            IN SAMPLER_INFO   *pSampler     : the current sampler to sample
 * RETURN   : BOOL : TRUE for OK.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-08 11:23
 *==========================================================================*/
BOOL Sample_ReopenPort(IN OUT PORT_INFO *pPort, IN SAMPLER_INFO *pSampler);

//
EQUIP_INFO *Init_GetEquipRefById(IN OUT SITE_INFO *pSite, 
								 IN int nEquipID);
EQUIP_INFO *Init_GetEquipRefByStdId(IN SITE_INFO *pSite,
									IN int nStdTyeId);
										   
void *Init_GetEquipSigRefById(IN EQUIP_INFO *pEquip,
							  IN int nSigType,
							  IN int nSigId);
SAMPLER_INFO *Init_GetSamplerRefById(IN OUT SITE_INFO *pSite,
									 IN int nSamplerID);

#define ALARM_ACTION_SAVE_ACTIVE		0x01
#define ALARM_ACTION_SAVE_HISTORY		0x02
#define ALARM_ACTION_SAVE_ALL			(ALARM_ACTION_SAVE_ACTIVE|ALARM_ACTION_SAVE_HISTORY)
#define ALARM_ACTION_REPORT				0x04
BOOL Equip_AlarmBegin(IN SITE_INFO *pSite, 
							 IN OUT EQUIP_INFO *pEquip,
							 IN OUT ALARM_SIG_VALUE	*pSig,
							 IN time_t tmNow,
							 IN int nActionFlag);

BOOL Equip_AlarmEnd(IN SITE_INFO *pSite, 
							 IN OUT EQUIP_INFO *pEquip,
							 IN OUT ALARM_SIG_VALUE	*pSig,
							 IN time_t tmNow,
							 IN int nNewValue,
							 IN int nActionFlag);

BOOL Site_ResavePersistentSigs(IN OUT SITE_INFO *pSite);

int Equip_SetVirtualSignalValue(
			   IN SITE_INFO *pSite,
			   IN EQUIP_INFO *pEquip,
			   IN int nSigType,
			   IN OUT SIG_BASIC_VALUE *pSigVal,
			   IN VAR_VALUE *pCtrlValue,
			   IN time_t tmSampled,
			   IN BOOL bNofity);

// insert the alarm to the bilink head
#define INSERT_NODE_TO_LINK_HEAD(pSigLink, pSig)	\
		((pSig)->next       = (pSigLink)->next, 	\
		 (pSigLink)->next   = (pSig),				\
		 (pSig)->prev       = (pSigLink),			\
		 (pSig)->next->prev = (pSig))

// remove the node itself from bi-link.
#define REMOVE_NODE_SELF_FROM_LINK(pSig)			\
		((pSig)->prev->next = (pSig)->next,			\
		 (pSig)->next->prev = (pSig)->prev,			\
         (pSig)->prev = (pSig)->next = NULL)


BOOL Init_DupExp(OUT EXP_ELEMENT **ppEleDst,
						OUT int *pnEle,
						IN EXP_ELEMENT *pEleSrc, IN int nEle);

/*==========================================================================*
 * FUNCTION : Exp_Calculate
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT EXP_ELEMENT  *pResult : 
 *            IN EXP_ELEMENT   *pExp       : 
 *            IN int          nElements    : 
 * RETURN   : BOOL : TRUE for OK, 
 *                   FALSE for the expression is error, the result is undefined.
 * COMMENTS : the byElementType of pResult shall be checked.
 *            if EXP_ETYPE_INVALID indicates the result is invalid.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-11 20:26
 *==========================================================================*/
#ifdef _DEBUG
BOOL Exp_CalculateD(IN char *pSigName, IN EXP_ELEMENT *pEvalExp, IN int nElements,
				   OUT EXP_ELEMENT *pResult,
				   IN char *__function__, IN int __line__);
#define Exp_Calculate(pSigName, pExp, nElements, pResult)	\
	Exp_CalculateD((pSigName), (pExp), (nElements), (pResult), __FUNCTION__, __LINE__)

void Exp_DisplayExpression(IN EXP_ELEMENT *pEvalExp, IN int nElements,
						   IN BOOL bMappedVar);
#else	// release version
BOOL Exp_CalculateR(IN EXP_ELEMENT *pEvalExp, IN int nElements,
				   OUT EXP_ELEMENT *pResult);
#define Exp_Calculate(pSigName, pExp, nElements, pResult)	\
		Exp_CalculateR((pExp), (nElements), (pResult))
#endif //_DEBUG

BOOL Exp_CalcDeadLockPossibleExp(IN EXP_ELEMENT *pEvalExp,
								 IN int nElements,
								 OUT EXP_ELEMENT *pResult,
								 IN void *pSigA,
								 IN void *pSigB);
/*==========================================================================*
 * FUNCTION : Exp_DisplayExpression
 * PURPOSE  : write a exp to a string buffer.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EXP_ELEMENT  *pEvalExp : 
 *            IN int          nElements : 
 *            BOOL	bMappedVar: TRUE the var is mapped to signal addr.
 *            OUT char        *pszExpStr : 
 *            IN int          nExpStrLen    : 
 * RETURN   : char *: the pszExpStr
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-21 13:35
 *==========================================================================*/
char *Exp_StringExpression(IN EXP_ELEMENT *pEvalExp, IN int nElements,
						   IN BOOL bMappedVar, 
						   OUT char *pszExpStr, IN int nExpStrLen);


EQUIP_SERIAL_CONVERT_PROC2 SN_GetEquipSerialConverter(IN char *pTypeEngName,
													 IN int nStdEquipTypeID);

#ifdef _CHK_SET_DEADLOCK
BOOL Equip_ModifySettingDeadlock(IN SET_SIG_VALUE *pSetSig, IN BOOL bSet);
#endif


#ifdef PRODUCT_INFO_SUPPORT
BOOL PI_GetConverterCfg(IN OUT PI_CONVERTOR *pConvertor,
						IN int nStdEquipID);
#endif //PRODUCT_INFO_SUPPORT

/*
	Notification to  Matser/Slave RS485_SAMPLER Slave Thread exit
	g_bExitNotification Set TRUE And Soon Close the Rs485Comm 
	when Rs485Sampler(pPort->iPortID = 7) don't heartbeat 
*/
BOOL		g_bExitNotification;
#define		RS485_POLLING_SEVEN_ID		7

//void RECT_InitParameters(void);

//BOOL RECT_GetRectBarcode(IN int iEquipID, IN OUT char *pRectPartNumber);
/*==========================================================================*
 * FUNCTION : Io_Sampler_Comm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SAMPLING_COMM_INFO *  pSamplingCommInfo : 
 *            void                  *pDataBuffer      : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2006-04-09 08:49
 *==========================================================================*/
BOOL Io_Sampler_Comm(IN OUT void *pDataBuffer);


/*==========================================================================*
 * FUNCTION : RECT_CloseSamplingDevice
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT int  *piIoSampler : 
 *            IN OUT int  *piCanHandle : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2006-03-10 15:17
 *==========================================================================*/
BOOL RECT_CloseSamplingDevice(void);

/*==========================================================================*
 * FUNCTION : RECT_SendControl
 * PURPOSE  : Analysis command string,distribute to can and io_sampler
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO *pEquip,
 *			  IN char                *pszCmdStr     : 
 *            IN DWORD               dwTimeout      : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2006-04-06 18:52
 *==========================================================================*/
int RECT_SendControl(IN EQUIP_INFO *pEquip,IN char *pszCmdStr, IN DWORD dwTimeout);

/*==========================================================================*
 * FUNCTION : *RECT_GetRelayHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : SAMPLING_COMM_INFO : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2006-04-13 13:47
 *==========================================================================*/
int RECT_GetRelayHandle(void);


//int RECT_GetTotalRectNum(void);
/*==========================================================================*
 * FUNCTION : IO_SysDigitalOutput
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iCtrlValue : 
 *            int  iRelayNo   : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2006-08-25 17:16
 *==========================================================================*/
//BOOL IO_SysDigitalOutput(int iCtrlValue, int iRelayNo);
//BOOL IO_InitSysDigitalOutput(int iCtrlValue, int iRelayNo);

//-----------BINGIN INTRODUCED OF MODIFYING by Jimmy Wu----------------------
//* 
//*  DATE          : 2011-12-2 14:31:32
//*  CHANGE SOURCE : 
//*  REMARK        : The following are added for CR temp strategy
#define  MAX_TEMP_SYS_SENSORS	7
#define MAX_TEMP_SENSORS		71	//  7 system sensors + 8*8 SM Temp sensors
#define MAX_TEMP_SENSORS2		75	//  7 system sensors + 8*8 SM Temp sensors + IB2 +EIB2
#define MAX_TEMP_SENSORS3		95	//  7 system sensors + 8*8 SM Temp sensors + IB2 +EIB2+2*10 SMDUETemp
//#define SIG_ID_SENSOR_SET_START		221	//  Every sensor has a sample signal and this is the start ID of the 8: 
//  221~227 of system and 221~228 of SM Temp
//#define SIG_ID_SENSOR_SAMPLE_START	251	//  This is the start ID of all initial sensor sample values
//#define SIG_ID_TEMP_SAMPLE_START	301	//  Every temp value is displayed in its assigned equipment from this start ID
//
//#define SIG_ID_TEMP_SET_ERROR_ALARM	300	//  The alarm status for assigning error

#define EQM_SIG_INVALID_VALUE		-9999

enum em_EQM_Sensor_EquipID
{

	EQM_EQUIPID_SYSTEM = 1,
	EQM_EQUIPID_BATTARY_GROUP = 115,

	EQM_EQUIPID_SMTEMP_GROUP = 708,

	EQM_EQUIPID_SMTEMP_1 = 709,
	EQM_EQUIPID_SMTEMP_2,
	EQM_EQUIPID_SMTEMP_3,
	EQM_EQUIPID_SMTEMP_4,
	EQM_EQUIPID_SMTEMP_5,
	EQM_EQUIPID_SMTEMP_6,
	EQM_EQUIPID_SMTEMP_7,
	EQM_EQUIPID_SMTEMP_8,
	EQM_EQUIPID_SMDUE_1=1801,

	//MAX_SENSOR_EQUIP = 9, //MAX sensor equip num
};
enum en_Sensor_Set_Status
{
	SENSOR_SET_NONE,
	SENSOR_SET_AMBIENT,
	SENSOR_SET_BATTGROUP,
	SENSOR_SET_ERR = -1,
};

//added for renew opt
#define RENEW_OPT_IMP  1
//#define RENEW_OPT_DEBUG  1

#ifdef RENEW_OPT_DEBUG
	extern int g_nOvertimes_sig_alarms;
	extern int g_nOvertimes_equip_equips;
	extern int g_nOvertimes_sampler_equips;
	extern int g_nOvertimes_port_samplers;
	extern int g_nOvertimes_dev_equips;
#endif

#endif /*__EQUIP_MON_H__*/

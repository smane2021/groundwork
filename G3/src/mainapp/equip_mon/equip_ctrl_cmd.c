/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_ctrl_cmd.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-04 14:10
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#include "stdsys.h"
#include "public.h"

#include "cfg_model.h"
#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "equip_mon.h"

#ifdef _DEBUG
//#define _DEBUG_EQUIP_CTRL	1   
#endif


#define CTRL_NEED_LOG(nSenderType, nCtrlResult)		\
	((nSenderType != SERVICE_OF_LOGIC_CONTROL) &&	/* do NOT log cmd from logical ctrl */\
	 (LOG_CTRL_CMD_REGARDLESS_RESULT |				/*log all?*/ \
	 (nCtrlResult == ERR_EQP_OK)))


static EQUIP_CTRL_CMD *Equip_GetEmptyCmd(IN HANDLE hCmdArray);
static int Equip_SafelySetCmdState(IN HANDLE hCmdArray,
									IN OUT EQUIP_CTRL_CMD *pCmd,
									IN int nNewState);
static char *Equip_StringCtrlCmd(IN OUT char *pszStrCmd, IN int nStrLen,
		 IN int nSigType, IN SIG_BASIC_VALUE *pSig, IN VAR_VALUE *pCtrlValue);

static int Equip_PostCtrlCmd(
			   IN int nSenderType, IN char *pszSenderName,		   
			   IN SITE_INFO *pSite,
			   IN EQUIP_INFO *pEquip,
			   IN int nSigType,
			   IN SIG_BASIC_VALUE *pSig,
			   IN VAR_VALUE *pCtrlValue,
			   IN DWORD dwTimeout,
			   IN BOOL bUrgent);

static BOOL Equip_LogCtrlCmdResult(IN int nSenderType, IN char *pszSenderName,
							 IN SITE_INFO *pSite, IN int nEquipID,
							 IN int nSigType, IN int nSigID,IN int iPositionID,
 							 IN char *pszCtrlCmd, IN int nCtrResult,
							 IN time_t tmCtrlTime);
							
static BOOL Equip_LogCtrlCmdResult_System(IN int nSenderType,
										  IN char *pszSenderName,
										  IN SITE_INFO *pSite,
										  IN char *pszEquipName,
										  IN int nSigType,
										  IN char *pszSignalName,
										  IN char *pszCtrlCmd,
										  IN int nCtrResult,
										  IN time_t tmCtrlTime);							 

static BOOL Equip_TestControlValid(IN EQUIP_INFO *pEquip, IN int nSigType,
								   IN OUT SIG_BASIC_VALUE *pSig, 
								   IN VAR_VALUE *pCtrlValue);

#define GET_SIG_SAMPLER_ID(pSig) (((SAMPLE_SIG_VALUE *)(pSig))->sv.iSamplerID)

/*==========================================================================*
 * FUNCTION : Equip_Control
 * PURPOSE  : send a ctrl command to a equipment
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int nSenderType       : the type of sender, 
 *								see app_service.h:APP_SERVICE_TYPE_ENUM 
 *            IN char*pszSenderName    : the sender name in str
 *            IN int nSendDirectly     : see EQUP_CTRL_SEND...
 *            int         nEquipID     : the equipment cmd will be sent to
 *            int         nSigType     : the sig type
 *            int         nSigID       : 
 *            VAR_VALUE   *pCtrlValue  : 
 *            DWORD         dwTimeout     : timeout is ms
 * RETURN   : int : ERR_EQP_OK: ok, ERR_EQP_TIMEOUT: timeout on sending
 *                  ERR_EQP_COMM_FAILURE: comm failure on sending.
 *                  ERR_EQP_INVALID_ARGS: for err ctrl args
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-07 11:07
 *==========================================================================*/
int Equip_Control(IN int nSenderType, IN char *pszSenderName,
				  IN int nSendDirectly,
				  IN int nEquipID, IN int nSigType, IN int nSigID, 
				  IN VAR_VALUE *pCtrlValue, IN DWORD dwTimeout)
{
	SITE_INFO		*pSite = GetSiteInfo();
	EQUIP_INFO		*pEquip;
	SIG_BASIC_VALUE *pSig;
	pSig = NULL;
	int			iResult;
	char			szValue[32];

	pEquip = Init_GetEquipRefById(pSite, nEquipID);
	if (pEquip == NULL)
	{
		return ERR_EQP_INVALID_ARGS;
	}

	pSig = (SIG_BASIC_VALUE *)Init_GetEquipSigRefById(pEquip, nSigType, nSigID);

	if (pSig == NULL)
	{
		return ERR_EQP_INVALID_ARGS;
	}

	iResult =  Equip_ControlEx(nSenderType, pszSenderName, nSendDirectly ,
		pEquip, nSigType, pSig, pCtrlValue, dwTimeout);
		
	if(nSenderType == SERVICE_OF_LOGIC_CONTROL)
	{
		return iResult;
	}
 
	if(pSig->ucType == VAR_FLOAT)
	{
		sprintf(szValue,"%f",pCtrlValue->fValue);		
	}
	else if(pSig->ucType == VAR_LONG)
	{
		sprintf(szValue,"%ld",pCtrlValue->lValue);		
	}
	else if(pSig->ucType == VAR_UNSIGNED_LONG)
	{
		sprintf(szValue,"%ld",pCtrlValue->ulValue);
	}
	else if(pSig->ucType == VAR_ENUM)
	{		
		sprintf(szValue,"%d",pCtrlValue->enumValue);
	}
	else  if(pSig->ucType == VAR_DATE_TIME)
	{
		sprintf(szValue,"%ul",pCtrlValue->ulValue);
		TimeToString((time_t)pCtrlValue->dtValue, TIME_CHN_FMT,
			szValue, sizeof(szValue));
	}	
	else
	{
		sprintf(szValue,"%s","NULL");
	}	
	
	AppLogOut(EQP_MON, APP_LOG_INFO,
			"Control by %s,Send Info is: EquipID:%d, SigType:%d,SignalID:%d\n", 
			pszSenderName,pEquip->iEquipID,nSigType,nSigID);
	AppLogOut(EQP_MON, APP_LOG_INFO,
			"Control(Cont.): CtrlValue:%s.Result is %s\n", 
			szValue,(iResult == ERR_EQP_OK) ? "Successed" : "Failed");
			
			
	return iResult;			
}


/*==========================================================================*
 * FUNCTION : Equip_ControlEx
 * PURPOSE  : ctrl by equip and sig addr
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int     nSenderType       : the type of sender, 
 *						          see app_service.h:APP_SERVICE_TYPE_ENUM 
 *            IN char    *pszSenderName    : the sender name in str
 *            IN int     nSendDirectly     : see EQUP_CTRL_SEND...
 *			  EQUIP_INFO       *pEquip     : 
 *            IN int           nSigType    : 
 *            SIG_BASIC_VALUE  *pSig       : 
 *            VAR_VALUE        *pCtrlValue : 
 *            DWORD            dwTimeout   : 
 * RETURN   : int : ERR_EQP_OK: ok, ERR_EQP_TIMEOUT: timeout on sending
 *                  ERR_EQP_COMM_FAILURE: comm failure on sending.
 *                  ERR_EQP_INVALID_ARGS: for err ctrl args
 *                  ERR_EQP_CTRL_SUPPRESSED: for suppressed by other signal
 *                  ERR_EQP_CTRL_DISABLED: for hardware switch disables control.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-24 14:54
 *==========================================================================*/
__INLINE static int Equip_GetRectID(IN char *ptrEquipName)
{
#define IS_SHARP(c)			((c) == 0x23)
	int	i;
	char *p = ptrEquipName;
	while (*p)
	{
		if(IS_SHARP(*p))
		{
			p++;
			return atoi(p);
		}
		p++;

	}
	return 0;
}

int Equip_ControlEx(IN int nSenderType,
					IN char *pszSenderName,
					IN int nSendDirectly,
					IN EQUIP_INFO *pEquip,
					IN int nSigType,
					IN OUT SIG_BASIC_VALUE *pSig,
					IN VAR_VALUE *pCtrlValue,
					IN DWORD dwTimeout)
{
	SITE_INFO		*pSite = GetSiteInfo();
	int				nResult;
	BOOL			bLogResult = TRUE;
	float			fMin, fMax, fVal;
	//EQUIP_CTRL_CMD	cmd;

	if ((pEquip == NULL) || (pSig == NULL) || (pCtrlValue == NULL) ||
		((unsigned int)nSigType > SIG_TYPE_SETTING))
	{
		return ERR_EQP_INVALID_ARGS;
	}

	// there are two types control:
	// 1. the real ctrl, the ctrl-channel of setting and ctrl signal > 0.
	// 2. the virtual setting, the sampler-chn is less than 0 for all type sigs

	// check the value will be sent is in the valid ranges
	fMin = ((CTRL_SIG_VALUE *)pSig)->pStdSig->fMinValidValue;
	fMax = ((CTRL_SIG_VALUE *)pSig)->pStdSig->fMaxValidValue;
	fVal = (pSig->ucType == VAR_FLOAT)         ? pCtrlValue->fValue 
		: (pSig->ucType != VAR_UNSIGNED_LONG)  ? pCtrlValue->lValue
		: (float)pCtrlValue->ulValue;

	if ((fMin > fVal+EPSILON) || (fVal-EPSILON > fMax))
	{
		nResult = ERR_EQP_INVALID_ARGS;
	}

	// check the signal is configured or NOT.	
	else if (!SIG_VALUE_IS_CONFIGURED(pSig))
	{
		nResult = ERR_EQP_INVALID_ARGS;
	}
	
	// check the hardware switch status, if the HW SWITCH is ON, the control 
	// will be rejected.
	else if (pSite->bSettingChangeDisabled 
			&& (nSenderType != SERVICE_OF_LOGIC_CONTROL))
	{
		nResult = ERR_EQP_CTRL_DISABLED;
	}


	// chk the sig type and dispatch cmd
	else switch (nSigType)
	{
	case SIG_TYPE_SETTING:
	case SIG_TYPE_CONTROL:
		// to test the current value will be set whether satisfy the settable
		// expression or NOT
		// Allow LOGIC_CONTROL service to do any control even if the control
		// value is invalid. The service shall ensure the value is correct!
		// Maofuhua, 2005-6-30.
		if ((nSenderType != SERVICE_OF_LOGIC_CONTROL)
			&& (!Equip_TestControlValid(pEquip, nSigType, pSig, pCtrlValue)))
		{
			nResult = ERR_EQP_CTRL_SUPPRESSED;
			break;
		}
		
		// when the control is disabled, only the General Controller is 
		// allowed to send the control
		if (!SIG_VALUE_IS_CONTROLLABLE(pSig)
			&& (nSenderType != SERVICE_OF_LOGIC_CONTROL))
		{
			nResult = ERR_EQP_CTRL_SUPPRESSED;
			break;
		}

		// On control action.
		// check and process on-ctrl action before the sending ctrl.
		// forward the ctrl to other signal.
		if (((SET_SIG_VALUE *)pSig)->sv.onCtrl.pSig != NULL)
		{
			VAR_VALUE		var;
			ON_CTRL_ACTION	*pAction = &((SET_SIG_VALUE *)pSig)->sv.onCtrl;

			RUN_THREAD_HEARTBEAT();	// trigger heartbeat

			// the ctrl-value must be non-zero (for the request of EEM).
			//If send zero, just return OK.20120905,Marco, for EEM(ENEC).
			if (FLOAT_EQUAL0(fVal))	// is zero error.
			{
				nResult = ERR_EQP_OK;
				break;
			}

			// the value will be forward controlled.
            if (pAction->pSig->ucType == VAR_FLOAT)
			{
				var.fValue = pAction->fValue;
			}
			else
			{
				var.lValue = (long)pAction->fValue;
			}

			// do NOT return the result, continue do set virtual value on OK.
			nResult = Equip_ControlEx(nSenderType, pszSenderName, nSendDirectly,
				pAction->pEquip, pAction->pSig->ucSigType, pAction->pSig,
				&var, dwTimeout);

			if (nResult != ERR_EQP_OK)
			{
				break;	// do NOT continue if fails on-ctrl.
			}
		}

		// send real ctrl cmd.
		if (((SET_SIG_VALUE *)pSig)->sv.iControlChannel >= 0)
		{
			// if directly send or the current signal is sampling signal and
			// the current value is NOT equal to the sending value, 
			// send ctrl cmd out.
			if (nSendDirectly || 
				VAR_IS_NOT_EQUAL(pSig->ucType, &pSig->varValue, pCtrlValue))
			{
				// post ctrl cmd
				nResult = Equip_PostCtrlCmd(
					nSenderType, pszSenderName,
					pSite, pEquip, nSigType, 
					pSig, pCtrlValue, dwTimeout,
					((nSendDirectly==EQUIP_CTRL_SEND_URGENTLY) ? TRUE : FALSE));
					

			}
			// the value is same, do NOT send out. 
			else
			{
				nResult = ERR_EQP_OK;
			}

			// ctrl result need not log, it's already logged after sending done.
			bLogResult = FALSE;
		}

		// set virtual sig value.
		else if (((SET_SIG_VALUE *)pSig)->sv.iSamplerChannel < 0)
		{
#ifdef	NEED_ES_ADMIN
		    // Marketing requires that only es_admin user is allowed to 
			// modify RECT_REDUND_ENB_SIGID
			if((pEquip->iEquipID == RECT_GROUP_EQUIP_ID) 
				&& (((SET_SIG_VALUE *)pSig)->pStdSig->iSigID == RECT_REDUND_ENB_SIGID)
				&& (strcmp(pszSenderName + 5, ES_ADMIN_DEFAULT_NAME))	//not es_admin
				&& (nSenderType == SERVICE_OF_USER_INTERFACE))			//LCD or Web
			{
				nResult = ERR_EQP_INVALID_ARGS;
			}
			else
#endif
			{
				nResult = Equip_SetVirtualSignalValue(
					pSite, pEquip, nSigType,
					(SIG_BASIC_VALUE *)pSig, pCtrlValue, time(NULL), TRUE) >= 0 
						? ERR_EQP_OK : ERR_EQP_INVALID_ARGS;
			}
		}
		else
		{
			// maybe cfg error, ctrlChn<0 and SampleChn>=0, it should be sample sig
			nResult = ERR_EQP_INVALID_ARGS;
		}

#ifdef _CHK_SET_DEADLOCK
		if ((nResult == ERR_EQP_OK) && 
			SIG_STATE_TEST(pSig, VALUE_STATE_IS_DEADLOCKED))
		{
			Equip_ModifySettingDeadlock((SET_SIG_VALUE *)pSig, FALSE);
		}
#endif
		//
		// After control action...Not implemented yet.
		//
		break;

	case SIG_TYPE_SAMPLING:
		// the set cmd is valid if the sampling signal is a virtual signal
		if (((SAMPLE_SIG_VALUE *)pSig)->sv.iSamplerChannel < 0)
		{
			nResult = Equip_SetVirtualSignalValue(
				pSite, pEquip, nSigType,
				(SIG_BASIC_VALUE *)pSig, pCtrlValue, time(NULL), TRUE) >= 0 
					? ERR_EQP_OK : ERR_EQP_INVALID_ARGS;
		}
		else
		{
			nResult = ERR_EQP_INVALID_ARGS;
		}
		break;

	default:
		nResult = ERR_EQP_INVALID_ARGS;
	}

	// write ctrl log
	if (bLogResult && CTRL_NEED_LOG(nSenderType, nResult))
	{
		char	szCmdStr[MAX_CTRL_CMD_LEN];

		Equip_StringCtrlCmd(szCmdStr, sizeof(szCmdStr),
			nSigType, pSig, pCtrlValue);

		Equip_LogCtrlCmdResult(nSenderType, pszSenderName, pSite, 
			pEquip->iEquipID, nSigType,
			((CTRL_SIG_VALUE *)pSig)->pStdSig->iSigID,Equip_GetRectID(pEquip->pEquipName->pFullName[0]),
			szCmdStr, nResult, time(NULL));
			
/*		Equip_LogCtrlCmdResult_System(nSenderType, pszSenderName, pSite, 
			pEquip->pEquipName->pFullName[0], nSigType,
			((CTRL_SIG_VALUE *)pSig)->pStdSig->pSigName->pFullName[0],
			szCmdStr, nResult, time(NULL));	*/		
	}

	if(ERR_EQP_OK == nResult && (nSigType == SIG_TYPE_SETTING || nSigType == SIG_TYPE_CONTROL))//
	{
		if((pEquip->iEquipID == 1 && nSigType == SIG_TYPE_CONTROL && ((CTRL_SIG_VALUE *)pSig)->pStdSig->iSigID == 11)
			|| (pEquip->iEquipID == 2 && nSigType == SIG_TYPE_CONTROL  && ((CTRL_SIG_VALUE *)pSig)->pStdSig->iSigID == 1)
			|| (pEquip->iEquipID == 2 && nSigType == SIG_TYPE_CONTROL  && ((CTRL_SIG_VALUE *)pSig)->pStdSig->iSigID == 2)
			|| (pEquip->iEquipID == 2 && nSigType == SIG_TYPE_CONTROL  && ((CTRL_SIG_VALUE *)pSig)->pStdSig->iSigID == 12)
			|| (pEquip->iEquipID == 2 && nSigType == SIG_TYPE_SETTING  && ((SET_SIG_VALUE *)pSig)->pStdSig->iSigID == 37)
			|| (pEquip->iEquipID == 2 && nSigType == SIG_TYPE_SETTING  && ((SET_SIG_VALUE *)pSig)->pStdSig->iSigID ==49)
			|| (pEquip->iEquipID == 2 && nSigType == SIG_TYPE_SETTING  && ((SET_SIG_VALUE *)pSig)->pStdSig->iSigID == 50)
			|| (pEquip->iEquipID == 210 && nSigType == SIG_TYPE_SETTING  && ((SET_SIG_VALUE *)pSig)->pStdSig->iSigID == 49)
			|| (pEquip->iEquipID == 210 && nSigType == SIG_TYPE_SETTING  && ((SET_SIG_VALUE *)pSig)->pStdSig->iSigID == 50)
			|| (pEquip->iEquipID == 1350 && nSigType == SIG_TYPE_SETTING  && ((SET_SIG_VALUE *)pSig)->pStdSig->iSigID == 37))
		{
			//Do nothing.
		}
		else if(pEquip->iEquipTypeID  == 201 || pEquip->iEquipTypeID == 1601 
				|| pEquip->iEquipTypeID == 1701 || pEquip->iEquipTypeID == 1801 
				|| pEquip->iEquipTypeID == 1101 || pEquip->iEquipTypeID == 2501)
		{
			//Do nothing, only report rectifier group information to decrease the message number.
		}
		else if(nSigType == SIG_TYPE_CONTROL && pEquip->iEquipID != 2 && pEquip->iEquipID != 115  && pEquip->iEquipID != 1)
		{
			//Do nothing
		}
		else
		{
			
			//printf("Send Message Equip ID = %d, nSigType = %d, Signal ID = %d",pEquip->iEquipID, nSigType, ((CTRL_SIG_VALUE *)pSig)->pStdSig->iSigID);
			NotificationFunc(_WEB_SET_MASK,	// masks
				sizeof(EQUIP_INFO *),				// the sizeof (EQUIP_INFO *)
				&pEquip,							// the equipment address ref.
				FALSE);				
		}
	}
	return nResult;
}



/*==========================================================================*
 * FUNCTION : Equip_TestControlValid
 * PURPOSE  : test the value will be set satisfy the contrl/setable exp or NOT
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO           *pEquip     : 
 *            IN int                  nSigType    : 
 *            IN OUT SIG_BASIC_VALUE  *pSig       : 
 *            IN VAR_VALUE            *pCtrlValue : 
 * RETURN   : static BOOL : TRUE for valid.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-09 11:47
 *==========================================================================*/
static BOOL Equip_TestControlValid(IN EQUIP_INFO *pEquip, IN int nSigType,
								   IN OUT SIG_BASIC_VALUE *pSig, 
								   IN VAR_VALUE *pCtrlValue)
{
 	EXP_ELEMENT			*pTempExp = NULL;
	int					nTempExp  = 0, i;
	//BOOL				bNeedTestExp;
	BOOL				bTesResult;

	EXP_CONST			fResult;
	EXP_ELEMENT			result = {EXP_ETYPE_CONST, &fResult};

	SIG_BASIC_VALUE		sigTemp;

	UNUSED(pEquip);

	if ((nSigType != SIG_TYPE_SETTING) && (nSigType != SIG_TYPE_CONTROL))
	{
		return TRUE;	// NOT need check.
	}

	if(((SET_SIG_VALUE *)pSig)->sv.iSettableExpression == 0)
	{
		return TRUE;
	}

	if (!Init_DupExp(&pTempExp, &nTempExp,
		((SET_SIG_VALUE *)pSig)->sv.pSettableExpression,
		((SET_SIG_VALUE *)pSig)->sv.iSettableExpression))
	{
		return FALSE;
	}

	// find and replace the target signal in the new exp.
	for (/*bNeedTestExp = FALSE, */i = 0; i < nTempExp; i++)
	{
		if ((pTempExp[i].byElementType == EXP_ETYPE_VARIABLE) &&
			(pTempExp[i].pElement == pSig))
		{
			pTempExp[i].pElement = &sigTemp;
			//bNeedTestExp		 = TRUE;
		}
	}
	
	//if (bNeedTestExp)
	{
		// set the sigTemp value.
		sigTemp			 = *pSig;	// get the other private attr from src sig
		sigTemp.varValue = *pCtrlValue;	// the test value.

		// calculate the exp.
		Exp_Calculate(EQP_GET_SIGV_NAME0((SET_SIG_VALUE *)pSig), 
			pTempExp, nTempExp, &result);

		// the result is valid, set the settable flag to pSig->usFlag
		bTesResult = ((result.byElementType == EXP_ETYPE_CONST) &&
			(EXP_GET_CONST_LONG(&result) != 0)) 
			? TRUE		// enable set
			: FALSE;	// disable set, even if the exp result is invalid
	}
	//else	// treat it OK
	//{
	//	bTesResult = TRUE;
	//}

	//release tmp exp
	SAFELY_DELETE(pTempExp);

	return bTesResult;
}


/*==========================================================================*
 * FUNCTION : Site_ResavePersistentSigs
 * PURPOSE  : re-save all the setting signals when curren saved records
 *            exceeds the maximum allowed records.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO          *pSite    : 
 * RETURN   : TRUE for Ok.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-19 11:40
 *==========================================================================*/
BOOL Site_ResavePersistentSigs(IN OUT SITE_INFO *pSite)
{
	PERSISTENT_SIG_RECORD		data;

	int							i, n;
	EQUIP_INFO					*pEquip;
	int							nSig;
	SET_SIG_VALUE				*pSig;

	


	pSite->nCurrentSavedPersistentSig = 0;

	if (pSite->hPersistentSig == NULL)
	{
		TRACEX("Saving all setting signal values FAILED on opening file. \n");
		return FALSE;
	}

	AppLogOut(EQP_MON, APP_LOG_INFO,
		"Re-saving all persistent setting signal values...\n");

	pEquip = pSite->pEquipInfo;
	for (n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		data.iEquipID = pEquip->iEquipID;

		pSig = pEquip->pSetSigValue;
		nSig = pEquip->pStdEquip->iSetSigNum;
		for (i = 0; i < nSig; i++, pSig++)
		{
			// optimization, only save changed settings.
			if (pSig->pStdSig->bPersistentFlag &&
				(pSig->bv.tmCurrentSampled != 0))
			{
				data.iSignalID= DXI_MERGE_SIG_ID(pSig->bv.ucSigType, 
					pSig->pStdSig->iSigID);
				data.varSignalVal = pSig->bv.varValue;
				data.tmSignalTime = pSig->bv.tmCurrentSampled;

				if (!DAT_StorageWriteRecord(pSite->hPersistentSig,
					sizeof(data), (void *)&data))
				{
					AppLogOut(EQP_MON, APP_LOG_WARNING,
						"Saving setting data for %s(%d)... FAILED.\n", 
						EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID);
					return FALSE;	// fail, do NOT continue.
				}

				pSite->nCurrentSavedPersistentSig++;
			}
		}
	}

	AppLogOut(EQP_MON, APP_LOG_INFO,
		"Saving all setting signal values OK. \n");

	return TRUE;
}


#define			BATTERY_GROUP_EQUIP_ID			120
#define			BATTERY_GROUP_CAPACITY_SIGID	31
/*==========================================================================*
 * FUNCTION : Equip_SetVirtualSignalValue
 * PURPOSE  : set and save the signal value which sampling channal < 0,
 *            and save the setting signal value which needs be saved.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:
 *            IN SITE_INFO            *pSite         : 
 *            IN EQUIP_INFO           *pEquip        : 
 *            IN int                  nSigType       : 
 *            IN OUT SIG_BASIC_VALUE  *pSigVal       :the sampling channel must < 0.
 *            IN VAR_VALUE            *pCtrlValue    : 
 *			  IN time_t				   tmSampled     :
 *			  IN BOOL			      bNofity        : TRUE: nofity equipment mrg 
 *                                     to process data if data changed.
 * RETURN   :  int : 0: not changed, -1: invalid arg, 1: ok and changed.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-17 11:51
 *==========================================================================*/
int Equip_SetVirtualSignalValue(
			   IN SITE_INFO *pSite,
			   IN EQUIP_INFO *pEquip,
			   IN int nSigType,
			   IN OUT SIG_BASIC_VALUE *pSigVal,
			   IN VAR_VALUE *pCtrlValue,
			   IN time_t tmSampled,
			   IN BOOL bNofity)
{
	VAR_VALUE	varSetSignal;
	// the sampling channel has already be verified. re-check it.
	if (((SET_SIG_VALUE *)pSigVal)->sv.iSamplerChannel >= 0)
	{
		TRACEX("%s(%d) is not virtual sampling channle.\n",
			EQP_GET_SIGV_NAME0((SET_SIG_VALUE *)pSigVal), 
			((SET_SIG_VALUE *)pSigVal)->pStdSig->iSigID);

		return -1;
	}

	// add process communication status. maofuhua, 2005-4-5
	if (!pEquip->bCommStatus && (pSigVal->ucSigType == SIG_TYPE_SAMPLING) &&
		(pSigVal != (SIG_BASIC_VALUE *)pEquip->pCommStatusSigRef) &&
		(pSigVal != (SIG_BASIC_VALUE *)pEquip->pWorkStatusSigRef))
	{
		return 0;	// do NOT set it.
	}

	// always set the ctrl time. maofuhua, 2005-05-10
	pSigVal->tmCurrentSampled = tmSampled;

	varSetSignal = *(pCtrlValue);
	//modified by Thomas, add control signal auto-reset process, 2007-3-9
	{
		CTRL_SIG_VALUE *pSigValueTmp = (nSigType == SIG_TYPE_CONTROL ? (CTRL_SIG_VALUE *)pSigVal : NULL);

		if (pSigValueTmp != NULL &&
			pSigValueTmp->sv.iSamplerChannel == VIRTUAL_AUTORESET_CHANNEL &&
			pSigValueTmp->pStdSig->iSigValueType == VAR_ENUM &&
			FLOAT_EQUAL0(pSigValueTmp->pStdSig->fMinValidValue))
		{
			pCtrlValue->lValue = 0;
		}
	}
	//end of modified, Thomas, 2007-3-9

	// virtual sig // value changed or state change.
	if (VAR_IS_NOT_EQUAL(pSigVal->ucType, &pSigVal->varValue, pCtrlValue) ||
		!SIG_VALUE_IS_VALID(pSigVal))	// the valid state need change
	{
		// 1. set the value and state.
		pSigVal->varLastValue     = pSigVal->varValue;
		pSigVal->varValue         = varSetSignal;//*(pCtrlValue);
		SIG_STATE_SET(pSigVal, VALUE_STATE_IS_VALID);	// set it valid		
		
		// 2. save the setting signal to FLASH and relevant signal change
		if ((nSigType == SIG_TYPE_SETTING) &&	// setting signal
			((SET_SIG_VALUE *)pSigVal)->pStdSig->bPersistentFlag &&	// need save
			(pSite->hPersistentSig != NULL))
		{
			// record the setting even if when alarm outgoing is blocked
			PERSISTENT_SIG_RECORD data;

#ifdef _DEBUG_EQUIP_CTRL
			TRACEX("[SET_SIG] Saving setting signal value of %s(%d) of equipment %s(%d) "
				"to %1.3f...\n",
				EQP_GET_SIGV_NAME0((SET_SIG_VALUE *)pSigVal), 
				((SET_SIG_VALUE *)pSigVal)->pStdSig->iSigID,
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
				(pSigVal->ucType == VAR_FLOAT ? pCtrlValue->fValue 
				: pCtrlValue->lValue));
#endif //_DEBUG_EQUIP_CTRL

			data.iEquipID = pEquip->iEquipID;
			data.iSignalID= DXI_MERGE_SIG_ID(nSigType, 
				((SET_SIG_VALUE *)pSigVal)->pStdSig->iSigID);
			data.varSignalVal = *(pCtrlValue);
			data.tmSignalTime = pSigVal->tmCurrentSampled;

			if (DAT_StorageWriteRecord(pSite->hPersistentSig,
				sizeof(data), (void *)&data))
			{
				pSite->nCurrentSavedPersistentSig++;

				if (pSite->nCurrentSavedPersistentSig >= 
					pSite->nMaxAllowedPersistentSig)
				{
					Site_ResavePersistentSigs(pSite);	// resave all signals.
				}
			}
#ifdef _DEBUG
			else
			{
				TRACEX("Saving setting data for %s(%d)... FAILED.\n", 
					EQP_GET_SIGV_NAME0((SET_SIG_VALUE *)pSigVal), 
					((SET_SIG_VALUE *)pSigVal)->pStdSig->iSigID);
			}
#endif //_DEBUG
		}

		// 3. notify equipment manager to process data
		if (bNofity)
		{
			EQUIP_PROCESS_CMD	cmd;
			//TRACEX("Post msg to equipment %s(%d) to process data...\n",
			//	EQP_GET_EQUIP_NAME0(pEquip),
			//	pEquip->iEquipID);

			// to notify the data processor
			cmd.nCmd   = PROCESS_CMD_SIG_DATA_CHANGED;
			cmd.pEquip = pEquip;
			Queue_Put(pSite->hqProcessCommand, &cmd, FALSE);
		}

		return 1;
	}

	return 0;
}


/*==========================================================================*
 * FUNCTION : Equip_StringCtrlCmd
 * PURPOSE  : Convert the ctrl value and the ctrl channel info to the ctrl 
 *            string which will be passed to Control() of the sampler.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT char         *pszStrCmd  : 
 *            IN int              nStrLen     : 
 *            IN int              nSigType    : 
 *            IN SIG_BASIC_VALUE  *pSig       : 
 *            IN VAR_VALUE        *pCtrlValue : 
 * RETURN   : static char *: The ctrl string will be formated as
 *                           "CtrlChannelNo,CtrlValue,ExtraCtrlParam"
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-21 11:07
 *==========================================================================*/
static char *Equip_StringCtrlCmd(IN OUT char *pszStrCmd, IN int nStrLen,
			   IN int nSigType, IN SIG_BASIC_VALUE *pSig, IN VAR_VALUE *pCtrlValue)
{
	char	*pszCtrlParam; // the extra ctrl param for SM-IO relays.
	int		nCmdNo;

	pszCtrlParam = (nSigType == SIG_TYPE_CONTROL) 
		? ((CTRL_SIG_VALUE *)pSig)->pStdSig->szCtrlParam : "0";
	nCmdNo = (nSigType != SIG_TYPE_SAMPLING) 
		? ((CTRL_SIG_VALUE *)pSig)->sv.iControlChannel : -1;
	
	//TRACE("__A__ StrCmd is %s \n",pszStrCmd);

	if (pSig->ucType == VAR_FLOAT)
	{
		snprintf(pszStrCmd, (size_t)nStrLen, "%d,%1.4f,%s", 
			nCmdNo, pCtrlValue->fValue,	pszCtrlParam);
	}
	else if (pSig->ucType != VAR_UNSIGNED_LONG)
	{
		snprintf(pszStrCmd, (size_t)nStrLen, "%d,%ld,%s", 
			nCmdNo, pCtrlValue->lValue,	pszCtrlParam);
	}
	else // treated as VAR_LONG type
	{
		snprintf(pszStrCmd, (size_t)nStrLen, "%d,%lu,%s", 
			nCmdNo, pCtrlValue->ulValue, pszCtrlParam);
	}
 //  TRACE("__B__ StrCmd is %s \n",pszStrCmd);
	return pszStrCmd;
}

#define			BATTERY_GROUP_EQUIP_TYPE_ID		300
#define         DC_BOARD_MIN_ID					110 
#define         DC_BOARD_TYPE_ID                501
#define         DC_BOARD_BAT_NUM_CTL_CHANNEL    89
/*==========================================================================*
 * FUNCTION : Equip_PostCtrlCmd
 * PURPOSE  : post a real ctrl cmd and wait for the result. if timeout, cancel
 *            the cmd before return. 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO          *pSite      : 
 *            IN EQUIP_INFO         *pEquip     : 
 *            IN int                nSigType    : 
 *            IN SIG_BASIC_VALUE				*pSig       : 
 *            IN VAR_VALUE          *pCtrlValue : 
 *            IN DWORD              dwTimeout   : if 0, no wait the cmd.
 *            IN BOOL				bUrgent		: TRUE, the cmd is urgent.
 * RETURN   : statuc int : ERR_EQP_OK: ok, ERR_EQP_TIMEOUT: timeout on sending
 *                  ERR_EQP_COMM_FAILURE: comm failure on sending.
 *                  ERR_EQP_INVALID_ARGS: for err ctrl args
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-09 13:31
 *==========================================================================*/
static int Equip_PostCtrlCmd(
			   IN int nSenderType, IN char *pszSenderName,		   
			   IN SITE_INFO *pSite,
			   IN EQUIP_INFO *pEquip,
			   IN int nSigType,
			   IN SIG_BASIC_VALUE *pSig,
			   IN VAR_VALUE *pCtrlValue,
			   IN DWORD dwTimeout, 
			   IN BOOL bUrgent)
{
	EQUIP_CTRL_CMD *pCmd;
	SAMPLER_INFO   *pSampler;
	int				nResult;
	int				nEndState;// the end state of cmd

	
	pSampler = Init_GetSamplerRefById(pSite,
		((CTRL_SIG_VALUE *)pSig)->sv.iSamplerID);

	ASSERT(pSampler != NULL);	// The sampler ID has been verified in init_xxx()

	pCmd = Equip_GetEmptyCmd(pSite->hCmdArray);
	if (pCmd == NULL)
	{
		return ERR_EQP_COMM_FAILURE;
	}
	
	//if(pEquip)
	//{
	//	if(pEquip->iEquipTypeID >= BATTERY_GROUP_EQUIP_TYPE_ID && (pEquip->iEquipTypeID != 1101 || pEquip->iEquipTypeID != 1100))
	//	{
	//		dwTimeout = 20000; //20000
	//	}    
	//}

	// make cmd info
	pCmd->pExecutor = pSampler;
	pCmd->dwTimeout = dwTimeout;
	pCmd->nState    = CTRL_CMD_IS_PENDING;
	
	// the info used in logging ctrl cmd result.
	pCmd->pEquip    = pEquip;	

	pCmd->nSigType  = nSigType;
	pCmd->pSig      = (SET_SIG_VALUE *)pSig;
	pCmd->varValue  = *pCtrlValue;

	pCmd->nSenderType = nSenderType;
	strncpyz(pCmd->szSenderName, pszSenderName, 
		sizeof(pCmd->szSenderName));

	Equip_StringCtrlCmd(pCmd->szCmdStr, sizeof(pCmd->szCmdStr),
		nSigType, pSig, pCtrlValue);

	// post the cmd addr(not content!)to the cmd queue of the sampler port
	if (Queue_Put(pSampler->pPollingPort->hqControlCommand, &pCmd, bUrgent)
		== ERR_QUEUE_OK)
	{
		if (dwTimeout == 0)
		{
			// the sender do NOT wait the result, return OK directly
			// the cmd will be ended by executor.

			nResult = ERR_EQP_OK;

			// not need change state that will be set by Equip_SafelySetCmdState .
			nEndState = CTRL_CMD_IS_IDLE; 
		}

        // wait for the cmd to be executed.
		else if (Sem_Wait(pCmd->hSyncSem, dwTimeout) == ERR_MUTEX_OK)
		{
			nResult   = pCmd->nResult;
			nEndState = CTRL_CMD_IS_END;

		}
		else
		{
			// if timeout, try to cancel the cmd. if the cmd is being executins,
			// the cancel action may no effect, the cmd may still be sent to the 
			// sampler.
			nResult   = ERR_EQP_TIMEOUT;
			nEndState = CTRL_CMD_IS_CANCELED;
		}
	}
	else
	{		
		nResult = ERR_EQP_COMM_FAILURE;
		nEndState = CTRL_CMD_IS_END;
	}

	if (nEndState != CTRL_CMD_IS_IDLE)
	{
		// set the cmd new state, to release the cmd item if the state is 
		// CTRL_CMD_IS_END, else the sampler will release the item.
		Equip_SafelySetCmdState(
			pSite->hCmdArray,
			pCmd, 
			nEndState);
	}

	return nResult;
}


/*==========================================================================*
 * FUNCTION : Equip_GetEmptyCmd
 * PURPOSE  : get an empty cmd slot from cmd array, and set the cmd state to
 *            CTRL_CMD_IS_USED immediately. and the hSyncSem is also created.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE hCmdArray: 
 * RETURN   : static EQUIP_CTRL_CMD *: the allocated empty cmd ptr.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-08 16:19
 *==========================================================================*/
static EQUIP_CTRL_CMD *Equip_GetEmptyCmd(IN HANDLE hCmdArray)
{
	static DWORD dwCmdSeq = 0;
	EQUIP_CTRL_CMD_ARRAY *pCmdArray;
	EQUIP_CTRL_CMD	*pCmd;

	pCmdArray = (EQUIP_CTRL_CMD_ARRAY *)hCmdArray;

	Mutex_Lock(pCmdArray->hSyncLock, WAIT_INFINITE);

	if (pCmdArray->pLastEmpty != NULL)	// use the last used item
	{
		pCmd = pCmdArray->pLastEmpty;
		pCmdArray->pLastEmpty = NULL;	// last empty buffer is used.
	}

	else if (pCmdArray->nPendingCmds < MAX_ALLOWED_CTRL_CMDS)
	{
		// to search one empty cmd buffer. there must be at least a empty item!
		for (pCmd = pCmdArray->pCmds; pCmd->nState != CTRL_CMD_IS_IDLE; pCmd++)
		{
			;
		}
	}

	else	// too many cmds are pending to be executed.
	{
		pCmd = NULL;

#ifdef _DEBUG_EQUIP_CTRL
		TRACEX("%u: Too many cmds are waiting for being executed.\n",
			(unsigned int)dwCmdSeq);
#endif //_DEBUG_EQUIP_CTRL
	}

	if (pCmd != NULL)
	{
		// to prevent other from using
		pCmd->nState   = CTRL_CMD_IS_USED;
		pCmd->dwCmdSeq = dwCmdSeq++;
		pCmdArray->nPendingCmds++;
	}

	Mutex_Unlock(pCmdArray->hSyncLock);

	return pCmd;
}


/*==========================================================================*
 * FUNCTION : Equip_ReleaseCmdItem
 * PURPOSE  : to release a cmd item to idle state
 * CALLS    : 
 * CALLED BY: Equip_SafelySetCmdState
 * ARGUMENTS: IN OUT EQUIP_CTRL_CMD  *pCmd : 
 * RETURN   : static : 
 * COMMENTS : MUST BE CALLED BY Equip_SafelySetCmdState
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-09 11:14
 *==========================================================================*/
static void Equip_ReleaseCmdItem(IN OUT EQUIP_CTRL_CMD_ARRAY *pCmdArray,
								 IN OUT EQUIP_CTRL_CMD *pCmd)
{
	Sem_Wait(pCmd->hSyncSem, 0);	// make the sem to no-sig state.
	pCmd->nState          = CTRL_CMD_IS_IDLE;
	pCmdArray->pLastEmpty = pCmd;
	pCmdArray->nPendingCmds--;
}


/*==========================================================================*
 * FUNCTION : Equip_SafelySetCmdState
 * PURPOSE  : set the cmd state sately.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE hCmdArray: 
 *            IN OUT EQUIP_CTRL_CMD    *pCmd      : 
 *            IN int                   nNewState  : must be CTRL_CMD_IS_EXECUTED,
 *                                                  or CTRL_CMD_IS_CANCELED,
 *                                                  or CTRL_CMD_IS_END.
 * RETURN   : static int : the current state of the cmd, may be CTRL_CMD_IS_IDLE
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-08 16:25
 *==========================================================================*/
static int Equip_SafelySetCmdState(IN HANDLE hCmdArray,
									IN OUT EQUIP_CTRL_CMD *pCmd,
									IN int nNewState)
{
	int nCurState;
	EQUIP_CTRL_CMD_ARRAY *pCmdArray = (EQUIP_CTRL_CMD_ARRAY *)hCmdArray;

	Mutex_Lock(pCmdArray->hSyncLock, WAIT_INFINITE);

	if (pCmd->nState != CTRL_CMD_IS_IDLE)
	{
		switch (nNewState)
		{
		case CTRL_CMD_IS_CANCELED:// sender to cancel the cmd.	
			// test the current state of the cmd, if the cur state is
			//    CTRL_CMD_IS_EXECUTED,  set the cmd state to IDLE.
			if (pCmd->nState == CTRL_CMD_IS_EXECUTED)
			{
				Equip_ReleaseCmdItem(pCmdArray, pCmd);
			}
			else
			{
				pCmd->nState = nNewState;
			}
			break;

		case CTRL_CMD_IS_EXECUTED:	// by sampler 
			// test the current state of the cmd, if the cur state is
			//    CTRL_CMD_IS_CANCELED,  set the cmd state to IDLE.
			if ((pCmd->nState == CTRL_CMD_IS_CANCELED) ||
				(pCmd->dwTimeout == 0))	// the sender is not waiting for result.
			{
				Equip_ReleaseCmdItem(pCmdArray, pCmd);
			}
			else
			{
				pCmd->nState = nNewState;
				Sem_Post(pCmd->hSyncSem, 1);	// sync the sender.
			}
			break;

		case CTRL_CMD_IS_EXECUTING: // by sampler
			if (pCmd->nState == CTRL_CMD_IS_CANCELED)
			{
				Equip_ReleaseCmdItem(pCmdArray, pCmd);
			}
			else
			{
				pCmd->nState = nNewState;
			}
			break;


		case CTRL_CMD_IS_END:	// release the item
			{
				Equip_ReleaseCmdItem(pCmdArray, pCmd);
			}
			break;

		default:
			{
				pCmd->nState = nNewState;
			}
		}
	}

	nCurState = pCmd->nState;

	Mutex_Unlock(pCmdArray->hSyncLock);

	return nCurState;
}


/*==========================================================================*
 * FUNCTION : Sample_SendControl
 * PURPOSE  : send control to the specified sampler/
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PORT_INFO     *pPort     : 
 *            IN SAMPLER_INFO  *pSampler  : 
 *            IN  char         *pszCmdStr : 
 *            IN DWORD         dwTimeout  : 
 * RETURN   :static int : ERR_EQP_OK: ok, ERR_EQP_TIMEOUT: timeout on sending
 *                  ERR_EQP_COMM_FAILURE: comm failure on sending.
 *                  ERR_EQP_INVALID_ARGS: for err ctrl args
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-07 15:27
 *==========================================================================*/
static int Sample_SendControl(IN PORT_INFO *pPort, IN SAMPLER_INFO *pSampler,
					   IN char *pszCmdStr, IN DWORD dwTimeout)
{
	int				nRetry;

	UNUSED(dwTimeout);	// the timeout now is not used.

	// open port at first
	if (!Sample_ReopenPort(pPort, pSampler))
	{
		return ERR_EQP_COMM_FAILURE;
	}

	for(nRetry = COMM_CTRL_RETRY_TIME; nRetry > 0; nRetry--)
	{
		RunThread_Heartbeat(pPort->hSampleThread);

		// send out the cmd
		if (pSampler->pStdSampler->pfnControl(
			pPort->hPort,
			pSampler->iSamplerAddr,		/* sampler unit ID: address */ 
			pszCmdStr))					/* contro parameter */
		{
			return ERR_EQP_OK;
		}
	}	

	return ERR_EQP_COMM_FAILURE;
}

/*==========================================================================*
 * FUNCTION : Equip_LogCtrlCmdResult
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int        nSenderType    : 
 *            IN char       *pszSenderName : 
 *            IN SITE_INFO  *pSite         : 
 *            IN int        nEquipID       : 
 *            IN int        nSigType       : 
 *            IN int        nSigID         : 
 *            IN char       *pszCtrlCmd    : 
 *            IN int        nCtrResult     : see err_code.h:ERR_CODE_EQUIPMENT_MONITORING_ENUM
 *            IN time_t     tmCtrlTime     : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-17 11:50
 *==========================================================================*/
static BOOL Equip_LogCtrlCmdResult(IN int nSenderType, IN char *pszSenderName,
							 IN SITE_INFO *pSite, 
							 IN int nEquipID, IN int nSigType, IN int nSigID,IN int iPositionID, 
 							 IN char *pszCtrlCmd, IN int nCtrResult,
							 IN time_t tmCtrlTime)
{
	HIS_CONTROL_RECORD	data;

	//TRACEX("Sender %s(type:%d) sent ctrl cmd %s to signal %d:%d:%d got result %d.\n",
	//	pszSenderName, nSenderType,
	//	pszCtrlCmd,
	//	nEquipID, nSigType, nSigID,
	//	nCtrResult);

	if ((pSite->hControlLog == NULL) /* || // always log ctrl msg. maofuhua, 2005-4-4
		pSite->bAlarmOutgoingBlocked */) // do NOT record on alarm outging blocked
	{
		return FALSE;
	}

	data.bySenderType = (BYTE)nSenderType;

	strncpyz(data.szSenderName, pszSenderName, sizeof(data.szSenderName));

	data.iEquipID    = nEquipID;
	data.iSignalID   = DXI_MERGE_SIG_ID(nSigType, nSigID);
	data.iPositionID = iPositionID;

	strncpyz(data.szCtrlCmd, pszCtrlCmd, sizeof(data.szCtrlCmd));

	data.nControlResult = nCtrResult;
	data.tmControlTime  = tmCtrlTime;


	if (!DAT_StorageWriteRecord(pSite->hControlLog,
		sizeof(data), (void *)&data))
	{
		TRACEX("Saving ctrl log %s for %s... FAILED.\n", 
			pszCtrlCmd, pszSenderName);
		return FALSE;
	}

	return TRUE;
}


static BOOL Equip_LogCtrlCmdResult_System(IN int nSenderType,
										  IN char *pszSenderName,
										  IN SITE_INFO *pSite,
										  IN char *pszEquipName,
										  IN int nSigType,
										  IN char *pszSignalName,
										  IN char *pszCtrlCmd,
										  IN int nCtrResult,
										  IN time_t tmCtrlTime)
{
	if ((pSite->hControlLog == NULL) /* || // always log ctrl msg. maofuhua, 2005-4-4
		pSite->bAlarmOutgoingBlocked */) // do NOT record on alarm outging blocked
	{
		return FALSE;
	}	
	
	AppLogOut(EQP_MON, APP_LOG_INFO,
	"Send to %s %s %s through %s, value is %s , the result is %s\n", pszEquipName,
					(nSigType == 1) ? "Control signal" : "Setting signal", 
					pszSignalName, 
					pszSenderName,
					pszCtrlCmd,
					(nCtrResult == 0) ? "Successful" : "Failure");
					
	return TRUE;
}							 
/*==========================================================================*
 * FUNCTION : Sample_CheckControl
 * PURPOSE  : check and send the control commands to this port.
 *            if there are too many commands, will return in 10sec.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 *            IN PORT_INFO  *pPort : 
 * RETURN   : SAMPLER_INFO* : the first sampler the cmd is send to
 * COMMENTS : 
 * CREATOR  :LIXIDONG         DATE: 2005-11-21 21:04
 *==========================================================================*/
SAMPLER_INFO *Sample_CheckControl(IN SITE_INFO *pSite, IN PORT_INFO *pPort)
{
	time_t			tmStart = time(NULL), tmNow;
	EQUIP_CTRL_CMD	*pCmd;
	SAMPLER_INFO	*pFirstSampler = NULL;
	//int				iAddrInfo;
	//int				iCtrlValue = 0;;

	// NOT NEED TO LOCK, due to the port is used in the same thread.
	// get a ctrl command from command queue.
	while(THREAD_IS_RUNNING(pPort->hSampleThread) 
		&& (Queue_Get(pPort->hqControlCommand, 
					&pCmd,
					FALSE,
					WAIT_TIME_CHECKING_CTRL_CMD) == ERR_QUEUE_OK))
	{
		RunThread_Heartbeat(pPort->hSampleThread);	// heartbeat.
		if (pCmd->nState == CTRL_CMD_IS_CANCELED)
		{
			// the cmd is canceled by the sender. end the cmd simply
			Equip_SafelySetCmdState(
				pSite->hCmdArray,
				pCmd, 
				CTRL_CMD_IS_END);
		}

		// to safely set the state to avoid the sender cancel the cmd after
		// we chk and before we we new state directly.
		else if (Equip_SafelySetCmdState(pSite->hCmdArray,
				pCmd, 
				CTRL_CMD_IS_EXECUTING) == CTRL_CMD_IS_EXECUTING)
		{
			// is excuting... send the command to the sampler.

			//1.First Send cmd to low level equip
			/*if(pPort->iStdPortTypeID == BASIC_PORT_VIRSUAL)
			{
				pCmd->nResult = RECT_SendControl(pCmd->pEquip, pCmd->szCmdStr, pCmd->dwTimeout);
			}
			else if(pPort->ppAttachedSamplers[0]->iSamplerAddr 
				== ADC_DISTR_VIRTUAL_ADDR)
			{
				pCmd->nResult = ADC_SendControl(pPort,
						pCmd->pExecutor, pCmd->szCmdStr, 
						pCmd->dwTimeout, pCmd->pEquip->iEquipID,
						&iCtrlValue,
						USER_CONTROL_TYPE);

				if((pCmd->pSig->pStdSig->iSigValueType == VAR_UNSIGNED_LONG) ||
					(pCmd->pSig->pStdSig->iSigValueType == VAR_LONG))
				{
					pCmd->varValue.ulValue = iCtrlValue;
				}
				
				if((pCmd->nResult == ERR_OK) && 
					(pCmd->nSigType == SIG_TYPE_SETTING))
				{
					ADC_CheckCfgChanged(pCmd);
				}
			}
			else
			{*/
			pCmd->nResult = Sample_SendControl(pPort,
										pCmd->pExecutor,
										pCmd->szCmdStr,
										pCmd->dwTimeout);			
			/*}*/

			tmNow = time(NULL);	// get current time, will be used in log result.
		
			// log the result.
			if (CTRL_NEED_LOG(pCmd->nSenderType, pCmd->nResult))
			{
				// OK, log the ctrl info
				Equip_LogCtrlCmdResult(pCmd->nSenderType,
									pCmd->szSenderName,
									pSite, 
									pCmd->pEquip->iEquipID,
									pCmd->nSigType,
									pCmd->pSig->pStdSig->iSigID,
									Equip_GetRectID(pCmd->pEquip->pEquipName->pFullName[0]),
									pCmd->szCmdStr,
									pCmd->nResult,
									tmNow);
			}

			if (pCmd->nResult == ERR_EQP_OK)
			{
				//2.And then update Up level Value
				// set the current value if the sampling channle < 0 of the sig
				if (pCmd->pSig->sv.iSamplerChannel < 0)
				{
					Equip_SetVirtualSignalValue(pSite,
											pCmd->pEquip,
											pCmd->nSigType,
											(SIG_BASIC_VALUE *)pCmd->pSig,
											&pCmd->varValue,
											tmNow,
											TRUE);

				}

				if (pFirstSampler == NULL)
				{
					// we only save the first sampler who just has sent a cmd.
					pFirstSampler = pCmd->pExecutor;
				}

				// need to query data now, due to ctrl command.
				// to froce to sample data again.
				pCmd->pExecutor->bJustControlled = TRUE;
			}

			// OK, set the cmd state to CTRL_CMD_IS_EXECUTED.
			Equip_SafelySetCmdState(pSite->hCmdArray,
								pCmd, 
								CTRL_CMD_IS_EXECUTED);			
		}

		if (((tmNow - tmStart) >= MAX_ALLOWED_CONTROL_TIME) ||
			(tmNow < tmStart))	// time is changed to back.
		{
			// too many cmds need to be sent, but now we need to sample data
			// the left cmds will be processed in the next call.
#ifdef _DEBUG_EQUIP_CTRL
			TRACEX("It's time to sample data.\n");
#endif //_DEBUG_EQUIP_CTRL
			break;
		}
	}


	return pFirstSampler;
}




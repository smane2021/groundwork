/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_mon_main.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-21 10:04
 *  VERSION  : V1.00
 *  PURPOSE  : The start/stop function for equipment monitoring module
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "equip_mon.h"
#include "../main/main.h"

/*==========================================================================*
 * FUNCTION : EquipMonitoring_InitSiteInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-22 15:28
 *==========================================================================*/
static BOOL EquipMonitoring_InitSiteInfo(IN OUT SITE_INFO *pSite)
{
	int		nInitResult;

	AppLogOut(EQP_INIT, APP_LOG_UNUSED,//APP_LOG_UNUSED,
		"Solution cfg is loading from dir [%s]...\n",
		g_szACUConfigDir);

	// 2. load cfg.
	nInitResult = Cfg_InitialConfig();

	AppLogOut(EQP_INIT, (nInitResult == ERR_CFG_OK) ? APP_LOG_UNUSED : APP_LOG_ERROR,
		"Solution cfg is %s loaded "
		"from dir [%s].\n",
		(nInitResult == ERR_CFG_OK) ? "now" : "NOT",
		g_szACUConfigDir);

	if (nInitResult != ERR_CFG_OK)
	{		
		return FALSE;
	}

	// 3. init the cfg.
	if (!Init_MonitoringSite(pSite))
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, "Fails on init monitoring site "
			"at dir %s.\n", g_szACUConfigDir);

		return FALSE;
	}


	AppLogOut(EQP_INIT, APP_LOG_UNUSED,//APP_LOG_UNUSED,
		"Solution cfg is loaded OK from dir [%s]...\n",
		g_szACUConfigDir);

	return TRUE;
}


#ifdef DEFER_BACKUP_CONFIG_FILES
static pthread_t	s_hConfigBackupThread  = 0;
static int			s_nConfigBackupThreadState = 0;

/*==========================================================================*
 * FUNCTION : EquipMonitoring_BackupConfigTask
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static void *: 
 * COMMENTS : due to backup is a long time, can NOT use RunThread.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-29 11:54
 *==========================================================================*/
static void *EquipMonitoring_BackupConfigTask(IN OUT SITE_INFO *pSite)
{
	char	szRunningSolution[MAX_FILE_PATH];

    UNUSED(pSite);

	TRACEX("%1.3f: Config is being backuped.\n", GetCurrentTime());

	// set the thread is cancelable
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	//set the thread can be canceled immediately
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	/* update backup config */
	DXI_CopyConfigFiles(CONFIG_DIR, CONFIG_BAK_DIR, FALSE); 

	// remove the last solution run file at the backup dir
	// maofuhua, 2005-3-24
	MakeFullFileName(szRunningSolution, getenv(ENV_ACU_ROOT_DIR), 
		CONFIG_BAK_DIR, SCUP_RUNNING_SOLUTION);

	remove(szRunningSolution);

	Timer_Kill(0, TIMER_ID_DEFER_BACKUP_CONFIG_FILES);
	s_nConfigBackupThreadState++;	// done.

	TRACEX("%1.3f: Config has been backuped.\n", GetCurrentTime());

	return NULL;
}


/*==========================================================================*
 * FUNCTION : EquipMonitoring_BackupConfigTimer
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE     hself   : 
 *            int        idTimer : 
 *            SITE_INFO  *pSite  : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-29 12:17
 *==========================================================================*/
static int  EquipMonitoring_BackupConfigTimer(HANDLE hself, int idTimer,
											  SITE_INFO *pSite)
{
	TRACEX("On config backup timer.\n");

	ASSERT(idTimer == TIMER_ID_DEFER_BACKUP_CONFIG_FILES);

	// first call time to check the state of the backup thread.
	if (s_nConfigBackupThreadState == 0)	// not running
	{
		if (pthread_create(&s_hConfigBackupThread, 
			NULL,//&attr
			(PTHREAD_START_ROUTINE)EquipMonitoring_BackupConfigTask,
			(void *)pSite) != 0)
		{
			AppLogOut(EQP_MON, APP_LOG_WARNING, 
				"Fails on creating config backup task.\n");
			return TIMER_KILL_THIS;
		}

		// create the thread OK. detach the thread
		pthread_detach(s_hConfigBackupThread);
		s_nConfigBackupThreadState++;

		TRACEX("Creating backuping task...OK.\n");

		return TIMER_CONTINUE_RUN;
	}

	// next call time to check the state of the backup thread.
	if (s_nConfigBackupThreadState == 1)	// still running?
	{
		AppLogOut(EQP_MON, APP_LOG_WARNING, 
			"Fails on backuping config, backuping task will be canceled.\n");

		pthread_cancel(s_hConfigBackupThread);
		s_nConfigBackupThreadState++;

		return TIMER_KILL_THIS;
	}

	return TIMER_KILL_THIS;
}


/*==========================================================================*
 * FUNCTION : EquipMonitoring_UnloadConfigBackupTask
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-29 13:27
 *==========================================================================*/
int EquipMonitoring_UnloadConfigBackupTask(IN SITE_INFO *pSite)
{
	TRACEX("Unloading config backup timer and task...\n");

	Timer_Kill(0, TIMER_ID_DEFER_BACKUP_CONFIG_FILES);

	if (s_nConfigBackupThreadState == 1)	// still running?
	{
		AppLogOut(EQP_MON, APP_LOG_WARNING, 
			"Fails on backuping config, backuping task will be canceled.\n");

		pthread_cancel(s_hConfigBackupThread);
		s_nConfigBackupThreadState = 0;
	}	

	return 0;
}
#endif


/*==========================================================================*
 * FUNCTION : EquipMonitoring_Init
 * PURPOSE  : load cfg and init monitoring site.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-22 13:12
 *==========================================================================*/
BOOL EquipMonitoring_Init(IN OUT SITE_INFO *pSite)
{
	char *ppszCfgDirs[] = {CONFIG_DIR, CONFIG_BAK_DIR, CONFIG_DEFAULT_DIR};
	char *pszConfigRootDir = getenv(ENV_ACU_ROOT_DIR);
	int	 nConfigType;
	
	if (pszConfigRootDir == NULL)
	{
		pszConfigRootDir = "./";
	}	

	TRACEX("ENV is:%s\n", pszConfigRootDir);

#ifdef G3_LOAD_FROM_VAR
	//G3_OPT  [loader], by Lin.Tao.Thomas, 2013-4
	//0. load cfg from VAR (CONFIG_DIR).
	nConfigType = CONFIG_NORMAL_TYPE;	
	MakeFullFileName(g_szACUConfigDir, "/var/",
		ppszCfgDirs[nConfigType], "");

	if (EquipMonitoring_InitSiteInfo(pSite))
	{
#ifdef DEFER_BACKUP_CONFIG_FILES
		Timer_Set(0, TIMER_ID_DEFER_BACKUP_CONFIG_FILES, 
			TIMER_INTERVAL_DEFER_BACKUP_CONFIG_FILES,
			(ON_TIMER_PROC)EquipMonitoring_BackupConfigTimer,
			(DWORD)pSite);
#else
		char szRunningSolution[MAX_FILE_PATH];

		/* update backup config */
		DXI_CopyConfigFiles(CONFIG_DIR, CONFIG_BAK_DIR, FALSE); 

		// remove the last solution run file at the backup dir
		// maofuhua, 2005-3-24
		MakeFullFileName(szRunningSolution, getenv(ENV_ACU_ROOT_DIR), 
			CONFIG_BAK_DIR, SCUP_RUNNING_SOLUTION);

		remove(szRunningSolution);
#endif
		// EquipMonitoring_InitSiteInfo() will init all info of site to 0.
		pSite->nConfigType = nConfigType;	// set cur cfg here.

		// always true, regardless the backup result.
		return TRUE;
	}

	// unload the failure config.
	EquipMonitoring_Unload(pSite);
#endif

	// 1. load cfg from CONFIG_DIR.
	nConfigType = CONFIG_NORMAL_TYPE;	
	MakeFullFileName(g_szACUConfigDir, pszConfigRootDir,
		ppszCfgDirs[nConfigType], "");
	if (EquipMonitoring_InitSiteInfo(pSite))
	{
#ifdef DEFER_BACKUP_CONFIG_FILES
		Timer_Set(0, TIMER_ID_DEFER_BACKUP_CONFIG_FILES, 
			TIMER_INTERVAL_DEFER_BACKUP_CONFIG_FILES,
			(ON_TIMER_PROC)EquipMonitoring_BackupConfigTimer,
			(DWORD)pSite);
#else
		char szRunningSolution[MAX_FILE_PATH];

		/* update backup config */
		DXI_CopyConfigFiles(CONFIG_DIR, CONFIG_BAK_DIR, FALSE); 

		// remove the last solution run file at the backup dir
		// maofuhua, 2005-3-24
		MakeFullFileName(szRunningSolution, getenv(ENV_ACU_ROOT_DIR), 
			CONFIG_BAK_DIR, SCUP_RUNNING_SOLUTION);

		remove(szRunningSolution);
#endif
		// EquipMonitoring_InitSiteInfo() will init all info of site to 0.
		pSite->nConfigType = nConfigType;	// set cur cfg here.

		// always true, regardless the backup result.
		return TRUE;
	}

	// unload the failure config.
	EquipMonitoring_Unload(pSite);

	// 2. load cfg from CONFIG_BAK_DIR.
	nConfigType = CONFIG_BACKUP_TYPE;
	MakeFullFileName(g_szACUConfigDir, pszConfigRootDir,
		ppszCfgDirs[nConfigType], "");
	if (EquipMonitoring_InitSiteInfo(pSite))
	{
		// OK. check and backup the correct config file to back.
		DXI_CopyConfigFiles(CONFIG_BAK_DIR, CONFIG_DIR, TRUE); 

		// EquipMonitoring_InitSiteInfo() will init all info of site to 0.
		pSite->nConfigType = nConfigType;	// set cur cfg here.

		// the app serives will use/change the private cfg at the normal dir.
		MakeFullFileName(g_szACUConfigDir, pszConfigRootDir,
			ppszCfgDirs[CONFIG_NORMAL_TYPE], "");

		return TRUE;
	}

	// unload the failure config.
	EquipMonitoring_Unload(pSite);


	// 3. load cfg from CONFIG_DEFAULT_DIR.
	nConfigType = CONFIG_DEFAULT_TYPE;
	MakeFullFileName(g_szACUConfigDir, pszConfigRootDir,
		ppszCfgDirs[nConfigType], "");
	if (EquipMonitoring_InitSiteInfo(pSite))
	{
		// EquipMonitoring_InitSiteInfo() will init all info of site to 0.
		pSite->nConfigType = nConfigType;	// set cur cfg here.

		return TRUE;
	}

	// do NOT unload the failure config., it's will be unloaded at exit.
	// !EquipMonitoring_UnloadSiteInfo(pSite);
	return FALSE;
}


/*==========================================================================*
 * FUNCTION : EquipMonitoring_Start
 * PURPOSE  : To init the internal data structures, and start the data
 *            sampling tasks and equipment management task.
 * CALLS    : 
 * CALLED BY: main()
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : the site info.
 * RETURN   : BOOL : TRUE for OK.
 * COMMENTS : The configuration of the site shall be loaded OK before calling
 *            this function.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-21 11:10
 *==========================================================================*/
BOOL EquipMonitoring_Start(IN OUT SITE_INFO *pSite)
{
	//1. create the log message
	AppLogOut(EQP_MON, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Equipment monitoring task now is starting...\n");

	//2. create sampling task for each port
    if (!Sample_CreateSamplingTasks(pSite))
	{
		AppLogOut(EQP_MON, APP_LOG_ERROR, "Fails on creating sampling tasks.\n");
		return FALSE;
	}


	//3. create the equipment manager
	pSite->hEquipManager = RunThread_Create(
		(char *)"EQUIP_MGR",
		(RUN_THREAD_START_PROC)Site_EquipmentManager,
		(void *)pSite,
		NULL,
		RUN_THREAD_FLAG_HAS_MSG);	// create msg queue
	if (pSite->hEquipManager == NULL)
	{
		AppLogOut(EQP_MON, APP_LOG_ERROR,
			"Fails on creating equipment managment task.\n");
		return FALSE;
	}

	//4.create success log message
	AppLogOut(EQP_MON, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Equipment monitoring task has successfully started.\n");

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : EquipMonitoring_Unload
 * PURPOSE  : unload cfg and inited memory.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-22 16:22
 *==========================================================================*/
void EquipMonitoring_Unload(IN OUT SITE_INFO *pSite)
{
	// 3. clean up all memory allocated by the equipment monitoring task
	AppLogOut(EQP_MON, APP_LOG_UNUSED, "Unloading runtime equipment info...\n");
	Unload_MonitoringSite(pSite);

	// 3. unload cfg
	AppLogOut(EQP_MON, APP_LOG_UNUSED, "Unloading configuration...\n");
	Cfg_UnloadConfig();
}


/*==========================================================================*
 * FUNCTION : EquipMonitoring_Stop
 * PURPOSE  : to stop and cleanup equipment monitoring tasks
 * CALLS    : 
 * CALLED BY: main()
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-21 20:00
 *==========================================================================*/
BOOL EquipMonitoring_Stop( IN OUT SITE_INFO *pSite)
{
	int			rc;

	AppLogOut(EQP_MON, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Equipment monitoring task now is being stopped...\n");

	// 1. stop the equipment manager task.
	if (pSite->hEquipManager != NULL)
	{
		AppLogOut(EQP_MON, APP_LOG_UNUSED, 
			"Equipment manager is being stopped...\n");

		rc = RunThread_Stop(pSite->hEquipManager,
			RUN_THREAD_HEARTBEAT_CHECK_INTERVAL,
			TRUE);
		
		AppLogOut(EQP_MON, 
			(rc != ERR_THREAD_KILLED) ? APP_LOG_UNUSED : APP_LOG_WARNING, 
			"Equipment manager is stopped %s.\n",
			(rc != ERR_THREAD_KILLED) ? "normally" : "forcibly, killed");
	}

	// 2. stop the samping task of each port
	Sample_StopSamplingTasks(pSite);

	AppLogOut(EQP_MON, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Equipment monitoring task has stopped.\n");

	return TRUE;
}

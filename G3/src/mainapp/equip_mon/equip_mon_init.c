/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : equip_mon_init.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-22 09:12
 *  VERSION  : V1.00
 *  PURPOSE  : To init the site based on the loaded configuration information
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <values.h>	// for MAXFLOAT
#include "stdsys.h"
#include "public.h"

#include "cfg_model.h"
#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "equip_mon.h"

#ifdef _DEBUG
//#define _DEBUG_EQUIP_INIT	1
//#define _DEBUG_EQUIP_INIT_DEVICE 1
//#define _DEBUG_DEVICE_INIT_DETAIL  1 //show the device init process from sampler
#endif

#define VAR_NOT_CONFIGURED	NULL	// the var of expression is not configured.
#define	MAX_LCD_MESSAGE_NUM		300

static BOOL Init_SiteInfo(IN OUT SITE_INFO *pSite);
static BOOL Init_MonitoredEquipmentUnits(IN OUT SITE_INFO *pSite);
static BOOL Init_Samplers(IN OUT SITE_INFO *pSite);
static BOOL Init_StdSamplers(IN OUT SITE_INFO *pSite);
static BOOL Init_SamplingPorts(IN OUT SITE_INFO *pSite);
static BOOL Init_CheckReusedID(IN SITE_INFO *pSite);

#ifdef SAVE_ACTIVE_ALARMS
static BOOL Init_LoadActiveAlarms(IN OUT SITE_INFO *pSite);
#endif //SAVE_ACTIVE_ALARMS

static BOOL Init_LoadSetSigValue(IN OUT SITE_INFO *pSite);
static BOOL Init_CtrlCmdArray(IN OUT SITE_INFO *pSite);
static BOOL Init_DataFiles(IN OUT SITE_INFO *pSite);

#ifdef PRODUCT_INFO_SUPPORT
static BOOL Init_Devices(IN OUT SITE_INFO *pSite);
#endif //PRODUCT_INFO_SUPPORT

static int Init_Aux_ChkReadParam(CFG_PARAM_RUN_INFO *pRunInfo, FILE *pf, SITE_INFO *pSite);
static BOOL Init_Aux_ReadParamToBuff(CFG_PARAM_RUN_INFO *pRunInfo, FILE *pf);
static BOOL Init_LoadParamFile(IN OUT SITE_INFO *pSite);  //added for TR# 62-ACU, Thomas, 2006-11-29


typedef BOOL	(*SITE_INIT_SUB_PROC)(IN OUT SITE_INFO *pSite);
struct _SITE_INIT_ITEM
{
	const char			*pszInitItem;	// description of being initialized
	SITE_INIT_SUB_PROC	pfnInitProc;	// the init proc.
};
typedef struct _SITE_INIT_ITEM	SITE_INIT_ITEM;

#define DEF_SITE_INIT_ITEM(pszItem, pfnInit)	\
	{(const char *)(pszItem), (SITE_INIT_SUB_PROC)(pfnInit)}

/*==========================================================================*
 * FUNCTION : Init_MonitoringSite
 * PURPOSE  : Init the running information of a monitoring site 
 * CALLS    : 
 * CALLED BY: EquipMonitoring_Start().
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : the site object
 * RETURN   : BOOL : TRUE for OK. FALSE for error
 * COMMENTS : The config information of the pSite object must be loaded OK
 *             before calling this function.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-22 13:08
 *==========================================================================*/
BOOL Init_MonitoringSite(IN OUT SITE_INFO *pSite)
{
	SITE_INIT_ITEM	inits[] =	// the init sequence can NOT be changed.
	{
		// at first, check the IDs of each cfg object is used or not.
		DEF_SITE_INIT_ITEM("Checking reused ID", Init_CheckReusedID),

		//0. init site ifself.
		DEF_SITE_INIT_ITEM("Site", Init_SiteInfo),

		//1. init monitored equipment
		DEF_SITE_INIT_ITEM("Monitored equipment", Init_MonitoredEquipmentUnits),

		//2.init samplers
		DEF_SITE_INIT_ITEM("Samplers", Init_Samplers),

		//3. init std-samplers
		DEF_SITE_INIT_ITEM("Standard samplers", Init_StdSamplers),

		//4. init sampling port
		DEF_SITE_INIT_ITEM("Sampling ports", Init_SamplingPorts),

		//5. init ctrl cmd array
		DEF_SITE_INIT_ITEM("Ctrl cmd array", Init_CtrlCmdArray),

		//6. init data files.
		DEF_SITE_INIT_ITEM("Init data file", Init_DataFiles),

#ifdef PRODUCT_INFO_SUPPORT
		//init devices -- to support Product Infomation
		DEF_SITE_INIT_ITEM("Init devices", Init_Devices),
#endif //PRODUCT_INFO_SUPPORT

	};

	int		n;
	BOOL	bInitResult = TRUE;	
	
	AppLogOut(EQP_INIT, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"Initializating monitoring site...\n");

	// init the items defined in inits, stop if any init fails.
	for (n = 0; ((n < ITEM_OF(inits)) && bInitResult); n++ )
	{
		// start to init
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, "%d: %s are being initializated...\n",
			n+1, 
			inits[n].pszInitItem);
		
		// to do init
		bInitResult = inits[n].pfnInitProc(pSite);
		
		// log init result.
		AppLogOut(EQP_INIT, 
			(bInitResult == TRUE) ? APP_LOG_UNUSED : APP_LOG_ERROR, 
			"%d: %s have%s been successfully initializated.\n",
			n+1, 
			inits[n].pszInitItem,
			(bInitResult == TRUE) ? "" : " NOT");
	}

	// if any init fails, whole initialization process is failure.
	AppLogOut(EQP_INIT, 
		(bInitResult == TRUE) ? APP_LOG_UNUSED : APP_LOG_ERROR, 
		"The monitoring site is%s successfully initializated.\n",
		(bInitResult == TRUE) ? "" : " NOT");

	return bInitResult;
}



/*==========================================================================*
 * FUNCTION : Init_SiteInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-13 18:46
 *==========================================================================*/
static BOOL Init_SiteInfo(IN OUT SITE_INFO *pSite)
{
	//char	*pszHardwareSetting;

	// 1. init the process command queue
	pSite->hqProcessCommand = Queue_Create(
		pSite->iEquipNum * EQUIP_MAX_PENDING_CMD,	//10 cmds per equip
		sizeof(EQUIP_PROCESS_CMD),
		0);

	if (pSite->hqProcessCommand == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on creating the command queue of the site.\n");

		return FALSE;
	}

	//2. get the hardware switch status. 1: change is disabled.
	/*
	pszHardwareSetting = getenv(ENV_ACU_JUMP_APP_RDONLY);
	pSite->bSettingChangeDisabled = (pszHardwareSetting == NULL) ? FALSE
		: (atoi(pszHardwareSetting) != 0) ? TRUE : FALSE;
	*/
	pSite->bSettingChangeDisabled = DAT_CurrentStatusIsRDONLY(ENV_SYS_JUMP_APP_RDONLY);

	if (pSite->bSettingChangeDisabled)
	{
		AppLogOut(EQP_INIT, APP_LOG_WARNING, "The hardware protection switch is ON, "
			"any change of ACU settings is disabled.\n");  
	}
	
	pSite->hLcdMessageQueue
		= Queue_Create(MAX_LCD_MESSAGE_NUM, sizeof(MESSAGE_TO_UI), 0);
	if (!(pSite->hLcdMessageQueue))
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR,
			"Failed on creating LCD message queue.\n");

		return FALSE;
	}
	return TRUE;
}



struct _CHECK_REUSED_ID_OBJECT
{
	char		*pObjectName;
	int			nObjects;
	char		*pObjects;
	int			nElementSize;
};

typedef struct _CHECK_REUSED_ID_OBJECT CHECK_REUSED_ID_OBJECT;

#define _DEF_CHECK_REUSED_ID_OBJECT(name, nObjToCheck, pObjToCheck)	\
	{(name), (nObjToCheck),	 (char *)(pObjToCheck),	sizeof((pObjToCheck)[0])}


/*==========================================================================*
 * FUNCTION : Init_FindReusedID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN CHECK_REUSED_ID_OBJECT  *pCheckObj : 
 *            OUT int                    *pReusedID : the found reused id
 *            IN BOOL                    *pbFoundAll  : 
 *            IN char  *pLogFmt   : must have and only have fmt %s(%d)
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-06 22:09
 *==========================================================================*/
static BOOL Init_FindReusedID(IN CHECK_REUSED_ID_OBJECT *pCheckObj,
							 OUT int *pReusedID, IN OUT BOOL *pbFoundAll,
							 IN char *pLogFmt)
{
	char	*pObj1, *pObj2;
	int		i, j, nID1, nID2;
	int		nEleSize = pCheckObj->nElementSize, nObjs = pCheckObj->nObjects;
	BOOL	bFound = FALSE;

	pObj1 = pCheckObj->pObjects;
	for (i = 0; i < nObjs; i++, pObj1 += nEleSize)
	{
		nID1  = *(int *)pObj1;
	
		pObj2 = pObj1 + nEleSize;	// start from next object
		for (j = i+1; j < nObjs; j++, pObj2 += nEleSize)
		{
			nID2 = *(int *)pObj2;
			if (nID1 == nID2)
			{
				*pReusedID = nID1;
				bFound	   = TRUE;
				*pbFoundAll = FALSE;

				if (pLogFmt != NULL)
				{
					AppLogOut(EQP_INIT, APP_LOG_ERROR,
						pLogFmt,
						pCheckObj->pObjectName, nID1);
				}

				if (!(*pbFoundAll))
				{
					break;
				}
			}
		}
	}

	return bFound;
}


/*==========================================================================*
 * FUNCTION : Init_CheckReusedID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-06 20:55
 *==========================================================================*/
static BOOL Init_CheckReusedID(IN SITE_INFO *pSite)
{
	int		i, n, nReusedID;
	STDEQUIP_TYPE_INFO	*pStdEquip;
	char	szLogFmt[128];
	BOOL	bCheckAll = TRUE, bCheckOK = TRUE;

	//1.Define items to need checking
	CHECK_REUSED_ID_OBJECT	objs[] =
	{
		_DEF_CHECK_REUSED_ID_OBJECT("Equip ID", //1. check equip id
			pSite->iEquipNum, pSite->pEquipInfo),
		_DEF_CHECK_REUSED_ID_OBJECT("Port ID", //2. check port id
			pSite->iPortNum, pSite->pPortInfo),
		_DEF_CHECK_REUSED_ID_OBJECT("Sampler ID", //3. check sampler id
			pSite->iSamplerNum, pSite->pSamplerInfo),
		_DEF_CHECK_REUSED_ID_OBJECT("Std Port Type ID", //4. check std port id
			pSite->iStdPortNum, pSite->pStdPortInfo),
		_DEF_CHECK_REUSED_ID_OBJECT("Std Sampler Type ID", //5. check std sampler id
			pSite->iStdSamplerNum, pSite->pStdSamplerInfo),
		_DEF_CHECK_REUSED_ID_OBJECT("Std Equipment Type ID", //6. check std equip id
			pSite->iStdEquipTypeNum, pSite->pStdEquipTypeInfo)
	};


	snprintf(szLogFmt, sizeof(szLogFmt), 
		"Error, found a re-used %%s(%%d) in config.\n");

	//2.Check basic ID
	for (i = 0; i < ITEM_OF(objs); i++)
	{
		if (Init_FindReusedID(&objs[i], &nReusedID, &bCheckAll, szLogFmt))
		{
			bCheckOK = FALSE;

			//AppLogOut(EQP_INIT, APP_LOG_ERROR,
			//	"Error, there is a %s(%d) in configuration.\n",
			//	objs[i].pObjectName, nReusedID);
			if (!bCheckAll)
			{
				return bCheckOK;
			}
		}
	}

	//3.Check each signal id for std equip.
	pStdEquip = pSite->pStdEquipTypeInfo;
	for (n = 0; n < pSite->iStdEquipTypeNum; n++, pStdEquip++)
	{
		CHECK_REUSED_ID_OBJECT	objs[] =
		{
			_DEF_CHECK_REUSED_ID_OBJECT("Sampling Signal ID",
				pStdEquip->iSampleSigNum, pStdEquip->pSampleSigInfo),
			_DEF_CHECK_REUSED_ID_OBJECT("Ctrl Signal ID",
				pStdEquip->iCtrlSigNum, pStdEquip->pCtrlSigInfo),
			_DEF_CHECK_REUSED_ID_OBJECT("Setting Signal ID",
				pStdEquip->iSetSigNum, pStdEquip->pSetSigInfo),
			_DEF_CHECK_REUSED_ID_OBJECT("Alarm Signal ID",
				pStdEquip->iAlarmSigNum, pStdEquip->pAlarmSigInfo)
		};

		snprintf(szLogFmt, sizeof(szLogFmt), 
			"Error, found a re-used %%s(%%d) in std equip %s(%d).\n",
			GET_LANG_TEXT0(pStdEquip->pTypeName), 
			pStdEquip->iTypeID);

		for (i = 0; i < ITEM_OF(objs); i++)
		{
			if (Init_FindReusedID(&objs[i], &nReusedID, &bCheckAll, szLogFmt))
			{
				bCheckOK = FALSE;

				//AppLogOut(EQP_INIT, APP_LOG_ERROR,
				//	"Error, there is a %s(%d) in std equipment %s(%d).\n",
				//	objs[i].pObjectName, nReusedID,
				//	GET_LANG_TEXT0(pStdEquip->pTypeName), 
				//	pStdEquip->iTypeID);

				if (!bCheckAll)
				{
					return bCheckOK;
				}
			}
		}
	}

	return bCheckOK;
}



static BOOL Init_EquipBasicInfo(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip);
static BOOL Init_MapEquipExpressionVar(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip);
static BOOL Init_ChkSiteSignalConfigState(IN OUT SITE_INFO *pSite);
static BOOL Init_SortStdSignalDisplayID(IN OUT STDEQUIP_TYPE_INFO *pStdEquip);
static BOOL Init_ChkOnCtrlRelatedSig(IN SITE_INFO *pSite);
static BOOL Init_EquipSerialSigRef(IN OUT SITE_INFO *pSite);
static BOOL Init_EquipCommStatusSigRef(IN OUT SITE_INFO *pSite);


/*==========================================================================*
 * FUNCTION : Init_MonitoredEquipmentUnits
 * PURPOSE  : init the all units of monitored equipment
 * CALLS    : Init_EquipBasicInfo
 * CALLED BY: Init_MonitoringSite
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : TRUE for OK
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-26 14:18
 *==========================================================================*/
static BOOL Init_MonitoredEquipmentUnits(IN OUT SITE_INFO *pSite)
{
	int				n;
	EQUIP_INFO		*pEquip;

	//0.Sort the standard equipment signals by its display ID firstly. 
#ifdef SORT_STD_SIGNALS_BY_DISPLAY_ID	
	STDEQUIP_TYPE_INFO	*pStdEquip;

#ifdef _LOG_DETAIL_INFO
	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"The display order of the std equipment signals is sorting...\n");
#endif

	pStdEquip = pSite->pStdEquipTypeInfo;
	for (n = 0; n < pSite->iStdEquipTypeNum; n++, pStdEquip++)
	{
		Init_SortStdSignalDisplayID(pStdEquip);
	}

#ifdef _LOG_DETAIL_INFO
	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"The display order of the std equipment signals is sorted.\n");
#endif

#endif

	//1. Init the basic info of all equipment.
	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"The basic info of equipment is being initialized...\n");

	pEquip = pSite->pEquipInfo;
	for (n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"Equipment(%d-%d) \"%s\"(%d) basic info is being initialized...\n",
			pSite->iEquipNum, n+1,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
#endif //_LOG_DETAIL_INFO

		if (!Init_EquipBasicInfo(pSite, pEquip))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on initializing the basic info of equipment %s(#%d).\n",
				EQP_GET_EQUIP_NAME0(&pSite->pEquipInfo[n]),
				pSite->pEquipInfo[n].iEquipID);

			return FALSE;
		}

#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"Equipment(%d-%d) \"%s\"(%d) basic info is initialized OK.\n",
			pSite->iEquipNum, n+1,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
#endif //_LOG_DETAIL_INFO

	}

	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"The basic info of equipment is initialized OK.\n");

	//2. map the evaluation expression variables of equipment signals
	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"The expressions of equipment are being initialized...\n");

	pEquip = pSite->pEquipInfo;
	for (n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"Equipment(%d-%d) \"%s\"(%d) expression is being initialized...\n",
			pSite->iEquipNum, n+1,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
#endif //_LOG_DETAIL_INFO

		if (!Init_MapEquipExpressionVar(pSite, pEquip))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on mapping expression variables of the "
				"equipment %s(#%d).\n",
				EQP_GET_EQUIP_NAME0(&pSite->pEquipInfo[n]),
				pSite->pEquipInfo[n].iEquipID);

			return FALSE;
		}

#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"Equipment(%d-%d) \"%s\"(%d) expression is initialized OK.\n",
			pSite->iEquipNum, n+1,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
#endif //_LOG_DETAIL_INFO
	}

	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"The expressions of equipment are initialized OK.\n");
	
	// check the on-ctrl signal reference relationship.
	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Checking the on-ctrl of control/setting signals...\n");
	if (!Init_ChkOnCtrlRelatedSig(pSite))
	{
		return FALSE;
	}

	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Checking the on-ctrl of control/setting signals OK.\n");
	
	// check the not-configured signals of the site
	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Checking the cfg state of all signals...\n");

	if (Init_ChkSiteSignalConfigState(pSite))
	{
		// check again. due to the signal reference order.
		Init_ChkSiteSignalConfigState(pSite);
	}

	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Checking the configuration state of all signals OK.\n");

	//10. get the serial num sampling signal ref.
	Init_EquipSerialSigRef(pSite);

	//11. get the comm status sig ref.
	Init_EquipCommStatusSigRef(pSite);

	return TRUE;
}


typedef BOOL (*INIT_CHK_VAR_PROC)(IN int nSigType, IN void *pChkSig, 
					IN void *pCheckParam, IN OUT EXP_ELEMENT *pVar);

static BOOL Init_MapSamplerChannel(IN OUT SITE_INFO		*pSite,
								   IN OUT EQUIP_INFO	*pEquip,
								   IN int				nSigType,
								   IN int				nSigId,
								   IN OUT int			*pSamplerId,
								   IN OUT int			*pSamplingChannel,
								   IN OUT int			*pCtrlChannel,
								   IN SIG_BASIC_VALUE	*pChannelRelatedSig);

static int Init_MapExpVarA(IN SITE_INFO *pSite, 
						   IN EQUIP_INFO *pEquip,
						   IN EXP_VARIABLE	*pExpVar, 
						   OUT EQUIP_INFO **ppSigEquip,
						   IN OUT SIG_BASIC_VALUE **ppSig);

static BOOL Init_MapExpressionVar(IN SITE_INFO		*pSite,
								  IN EQUIP_INFO			*pEquip,
								  IN OUT EXP_ELEMENT	*pExp, 
								  IN int				iExp,
								  IN INIT_CHK_VAR_PROC	pfnVarChecker,
								  IN void				*pChkParam );

static BOOL Init_ChkVarOfVirtualSig(IN int nSigType, 
									IN void *pChkSig,
									IN SAMPLE_SIG_VALUE	*pSig,
									IN OUT EXP_ELEMENT	*pVar);
static BOOL Init_ChkVarOfCtrlSig(IN int nSigType, IN void *pChkSig, 
								 IN CTRL_SIG_VALUE *pSig );
static BOOL Init_ChkVarOfAlarmSig(IN int nSigType, IN void *pChkSig, 
								  IN ALARM_SIG_VALUE *pSig );

static BOOL Init_SamplingSig(IN OUT  SITE_INFO *pSite,
							 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex);
static BOOL Init_CtrlSig(IN OUT SITE_INFO *pSite, 
						 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex );
static BOOL Init_SetSig(IN OUT SITE_INFO *pSite, 
						 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex );
static BOOL Init_AlarmSig(IN OUT SITE_INFO *pSite, 
						 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex );
static STDEQUIP_TYPE_INFO *Init_GetStdEquipRefByTypeId(IN OUT SITE_INFO *pSite, 
										  IN int nEquipTypeID);
static BOOL Init_MakeEquipReference(IN OUT SITE_INFO *pSite,
									IN EQUIP_INFO *pEquip);
static const char *Init_EquipSignals(IN SITE_INFO *pSite,
							   IN OUT EQUIP_INFO *pEquip);
static BOOL Init_AddEquipToRelevantSampler(IN OUT SITE_INFO *pSite,
									IN EQUIP_INFO *pEquip);


#ifdef SORT_STD_SIGNALS_BY_DISPLAY_ID

static __INLINE int Init_CompareStdSigDisplayID(SAMPLE_SIG_INFO	*pSig1,
									   SAMPLE_SIG_INFO	*pSig2)
{
	return (pSig1->iValueDisplayID < pSig2->iValueDisplayID) ? -1
		: (pSig1->iValueDisplayID  > pSig2->iValueDisplayID) ? 1
		: 0;
}



/*==========================================================================*
 * FUNCTION : Init_EquipSerialSigRef
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-10 16:39
 *==========================================================================*/
static BOOL Init_EquipSerialSigRef(IN OUT SITE_INFO *pSite)
{
	char				*pErrMsg;
	SAMPLE_SIG_VALUE	*pSig,*pSig2;	
	EQUIP_INFO			*pEquip;
	int					n;

	STDEQUIP_TYPE_INFO	*pStdEquip;

	// init the std sampler's serial convert proc.
	pStdEquip = pSite->pStdEquipTypeInfo;
	for (n = 0; n < pSite->iStdEquipTypeNum; n++, pStdEquip++)
	{
		pStdEquip->pfnSerialConvertProc = 
			SN_GetEquipSerialConverter(GET_LANG_TEXT0(pStdEquip->pTypeName),
									pStdEquip->iTypeID);
	}


	// get the sig ref of the serial number of the equipment.
	pEquip = pSite->pEquipInfo;
	for (n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		if ((pEquip->pStdEquip->iSerialRelatedSigID < 0) 
			|| (pEquip->pStdEquip->pfnSerialConvertProc == NULL))
		{
			continue;
		}

		// get the serial num sig
		pSig = (SAMPLE_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,
			SIG_TYPE_SAMPLING, pEquip->pStdEquip->iSerialRelatedSigID);

		pErrMsg = (pSig == NULL) ? "no such sig"
			: !SIG_VALUE_IS_CONFIGURED(pSig) ? "not configured"
			: NULL;	
		if (pErrMsg == NULL)
		{
			pEquip->pSerialSigRef = pSig;
		}
		else
		{
			AppLogOut( EQP_INIT, APP_LOG_WARNING,
				"Fails on ref the sampling signal(ID=%d) of serial number of "
				"\"%s\"(%d): %s\n",
				pEquip->pStdEquip->iSerialRelatedSigID,
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
				pErrMsg);
			// it's no matter and continue. do NOT stop.
		}
		//Added by YangGuoxin
		if(pEquip->pStdEquip->iSerialRelatedSigID == 8)
		{
			// get the serial num sig
			//TRACE("\npEquip->pStdEquip->iSerialRelatedSigID = %d\n",pEquip->pStdEquip->iSerialRelatedSigID);
			pSig2 = (SAMPLE_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,
				SIG_TYPE_SAMPLING, 26);
			//TRACE("\npSig2->pStdSig->iSigID = %d\n",pSig2->pStdSig->iSigID);
			pErrMsg = (pSig2 == NULL) ? "no such sig"
				: !SIG_VALUE_IS_CONFIGURED(pSig2) ? "not configured"
				: NULL;	
			if (pErrMsg == NULL)
			{
				pEquip->pSerialSigRef2 = pSig2;
			}	
		}		
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_EquipCommStatusSigRef
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-04-05 20:17
 *==========================================================================*/
static BOOL Init_EquipCommStatusSigRef(IN OUT SITE_INFO *pSite)
{
	char				*pErrMsg;
	SAMPLE_SIG_VALUE	*pSig;	
	EQUIP_INFO			*pEquip;
	int					n;

	// get the sig ref of the communucation status of the equipment.
	pEquip = pSite->pEquipInfo;
	for (n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		//Set ACD/DCD/BAT Clear alarm Status flag
		//pEquip->bClearAlarmStatus = TRUE;   //changed by : zyl ,old value : FALSE ,NEW : TRUE ; DATE: 2007/06/18

//begin specially process.
#warning "WARNING: Specify the Comm Status signal ID of Rectifier directly."
		if (pEquip->pStdEquip->iTypeID == RECT_STD_EQUIP_ID || pEquip->pStdEquip->iTypeID == 1601 || pEquip->pStdEquip->iTypeID == 1701 || pEquip->pStdEquip->iTypeID == 1801)
		{
			pEquip->pStdEquip->iCommStatusRelatedSigID = 
				DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_COMM_STATUS);
			pEquip->pRectACStatus = (SAMPLE_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,
				SIG_TYPE_SAMPLING, DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_AC_STATUS));
				
		}
		else if(pEquip->pStdEquip->iTypeID != 100 && 
				pEquip->pStdEquip->iTypeID != 200 &&
				pEquip->pStdEquip->iTypeID != 300 &&
				pEquip->pStdEquip->iTypeID != 307 &&
				pEquip->pStdEquip->iTypeID != 400 &&
				pEquip->pStdEquip->iTypeID != 403 &&
				pEquip->pStdEquip->iTypeID != 500 &&
				pEquip->pStdEquip->iTypeID != 504 &&
				pEquip->pStdEquip->iTypeID != 600 &&
				pEquip->pStdEquip->iTypeID != 700 &&
				pEquip->pStdEquip->iTypeID != 701 &&
				pEquip->pStdEquip->iTypeID != 800 &&
				pEquip->pStdEquip->iTypeID != 900 &&
				pEquip->pStdEquip->iTypeID != 1000 &&
				pEquip->pStdEquip->iTypeID != 1100 &&
				pEquip->pStdEquip->iTypeID != 1200 &&
				pEquip->pStdEquip->iTypeID != 1300 &&
				pEquip->pStdEquip->iTypeID != 1400 &&
				pEquip->pStdEquip->iTypeID != 1500 &&
				pEquip->pStdEquip->iTypeID != 2800 &&
				pEquip->pStdEquip->iTypeID != 2700 &&
				pEquip->pStdEquip->iTypeID != 604)
		{
			pEquip->pStdEquip->iCommStatusRelatedSigID = DXI_SPLIT_SIG_MID_ID(SIG_ID_EQUIP_COMM_STATUS);
		}
		else
		{
			pEquip->pStdEquip->iCommStatusRelatedSigID = -1;
			

		}

		if(	pEquip->pStdEquip->iTypeID == 100 ||
			pEquip->pStdEquip->iTypeID == 300 ||
			pEquip->pStdEquip->iTypeID == 400 ||
			pEquip->pStdEquip->iTypeID == 500 ||
			pEquip->pStdEquip->iTypeID == 600 ||
			pEquip->pStdEquip->iTypeID == 700 ||
			pEquip->pStdEquip->iTypeID == 701 ||
			pEquip->pStdEquip->iTypeID == 307
			)
		{
			pEquip->pStdEquip->iWorkStatusRelatedSigID = -1;
			
		}
		else
		{
			pEquip->pStdEquip->iWorkStatusRelatedSigID = DXI_SPLIT_SIG_MID_ID(SIG_ID_EQUIP_WORK_STATUS);

		}
//end specially process

		if (pEquip->pStdEquip->iCommStatusRelatedSigID < 0)
		{
			pEquip->bCommStatus = TRUE;
		}
		else
		{
			// get the serial num sig
			pSig = (SAMPLE_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,
				SIG_TYPE_SAMPLING, pEquip->pStdEquip->iCommStatusRelatedSigID);
			pErrMsg = (pSig == NULL) ? "no such sig"
				: !SIG_VALUE_IS_CONFIGURED(pSig) ? "not configured"
				: (pSig->pStdSig->iSigValueType == VAR_FLOAT) ? "can NOT be float"
				: NULL;	
			if (pErrMsg == NULL)
			{
				pEquip->pCommStatusSigRef = pSig;
			}
			else
			{
				AppLogOut( EQP_INIT, APP_LOG_WARNING,
					"Fails on ref the sampling signal(ID=%d) of Comm Status of "
					"\"%s\"(%d): %s\n",
					pEquip->pStdEquip->iCommStatusRelatedSigID,
					EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
					pErrMsg);
				// it's no matter and continue. do NOT stop.
			}

		}

		
		

		if (pEquip->pStdEquip->iWorkStatusRelatedSigID < 0)
		{
			pEquip->bWorkStatus = TRUE;
		}
		else
		{
			pSig = (SAMPLE_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,
				SIG_TYPE_SAMPLING, pEquip->pStdEquip->iWorkStatusRelatedSigID);
			
			
			pErrMsg = (pSig == NULL) ? "no such sig"
				: !SIG_VALUE_IS_CONFIGURED(pSig) ? "not configured"
				: (pSig->pStdSig->iSigValueType == VAR_FLOAT) ? "can NOT be float"
				: NULL;	
			
			if (pErrMsg == NULL)
			{
				pEquip->pWorkStatusSigRef = pSig;
			}
			else
			{
				AppLogOut( EQP_INIT, APP_LOG_WARNING,
					"Fails on ref the sampling signal(ID=%d) of Work Status of "
					"\"%s\"(%d): %s\n",
					pEquip->pStdEquip->iWorkStatusRelatedSigID,
					EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
					pErrMsg);
				// it's no matter and continue. do NOT stop.
			}
			//printf("pEquip->iEquipID = %d,pSig->bv.varValue.ulValue = %d, pEquip->pWorkStatusSigRef->bv.varValue.ulValue = %d\n",pEquip->iEquipID,pSig->bv.varValue.ulValue, pEquip->pWorkStatusSigRef->bv.varValue.ulValue);

		}

		
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Init_SortStdSignalDisplayID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT STDEQUIP_TYPE_INFO  *pStdEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-19 16:07
 *==========================================================================*/
static BOOL Init_SortStdSignalDisplayID(IN OUT STDEQUIP_TYPE_INFO *pStdEquip)
{
	// sort std sampling signals
	qsort(pStdEquip->pSampleSigInfo, (size_t)pStdEquip->iSampleSigNum,
		sizeof(pStdEquip->pSampleSigInfo[0]),
		(int (*)(const void *, const void *))Init_CompareStdSigDisplayID);

	// sort std setting signals
	qsort(pStdEquip->pSetSigInfo, (size_t)pStdEquip->iSetSigNum,
		sizeof(pStdEquip->pSetSigInfo[0]),
		(int (*)(const void *, const void *))Init_CompareStdSigDisplayID);

	// sort std ctrl signals
	qsort(pStdEquip->pCtrlSigInfo, (size_t)pStdEquip->iCtrlSigNum,
		sizeof(pStdEquip->pCtrlSigInfo[0]),
		(int (*)(const void *, const void *))Init_CompareStdSigDisplayID);

	return TRUE;
}
#endif

/*==========================================================================*
 * FUNCTION : Init_EquipBasicInfo
 * PURPOSE  : To init a unit of monitored equipment
 * CALLS    : 
 * CALLED BY: Init_MonitoredEquipmentUnits
 * ARGUMENTS: IN OUT SITE_INFO  *pSite  : 
 *            IN OUT EQUIP_INFO     *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : Old protocol is Init_MonitoredEquipment
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-26 14:28
 *==========================================================================*/
static BOOL Init_EquipBasicInfo(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip)
{
	const char		*pErrorOnInitSigName;
	ALARM_SIG_VALUE	*pSig;
	int				i;

#ifdef _DEBUG_EQUIP_INIT
	TRACEX("Equipment %s(%d) has %d relevant samplers:",	// no CR.
		EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
		pEquip->iRelevantSamplerListLength);	

	for (i = 0; i < pEquip->iRelevantSamplerListLength; i++)
	{
		RELEVANT_SAMPLER *pRS = &pEquip->pRelevantSamplerList[i];

		// sample ID is -1 indicates the samplerID is not cfg'ed.
		TRACE("[%d,%d,%d] ",
			pRS->iSamplerID, 
			pRS->iStartSamplingChannel,
			pRS->iStartControlChannel);
	}

	TRACE("\n");
#endif
	
	// 1 Init the attributes of the equipment itself.
	// 1.1 Obtain the reference address of its standard equipment
	pEquip->pStdEquip = Init_GetStdEquipRefByTypeId(pSite,
		pEquip->iEquipTypeID);
	if (pEquip->pStdEquip == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on getting the reference of standard equipment ID %d of "
			"equipment \"%s\"(%d).\n",
			pEquip->iEquipTypeID,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;	// error has been logged.
	}

	// 1.2 The other attributes are already set to zero

	// 2 Accoring to the list of relevant equipments and alarm filter 
	//     expression of this equipment, add this equipment to the reference
	//     table(ppEquipReferredThis) of the other equipment that is referred
	//     by this equipment
	if (!Init_MakeEquipReference(pSite, pEquip))
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on making the reference relationship of the "
			"equipment \"%s\"(%d).\n",
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;	// error has been logged.
	}

	// 4 Init all types signals
	pErrorOnInitSigName = Init_EquipSignals(pSite, pEquip);
	if (pErrorOnInitSigName != NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on initializing %s of equipment \"%s\"(%d).\n",
			pErrorOnInitSigName,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}


	// 5 Check and map alarm filter expression
	// 5.1 Duplicate the expression
	if (!Init_DupExp( &pEquip->pAlarmFilterExpression,
		&pEquip->iAlarmFilterExpression,
		pEquip->pAlarmFilterExpressionCfg,
		pEquip->iAlarmFilterExpression))
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Out of memory on duplicating alarm filter expression "
			"of equipment \"%s\"(%d).\n",
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}

	// 6. Add this equipment to the referred equipment list of the samplers.
	if (!Init_AddEquipToRelevantSampler(pSite, pEquip))
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on adding the equipment \"%s\"(%d) to referred equipment "
			"list of the relevant samplers.\n",
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}

	// 7. Init the active and delaying alarm list as bi-link
	for (i = 0; i < ALARM_LEVEL_MAX; i++)
	{
		pSig = &pEquip->pActiveAlarms[i];
		pSig->pStdSig    = NULL;
		pSig->next = pSig->prev = pSig;
	}

	//Reset
	pSig = &pEquip->pDelayingAlarms[0];
	pSig->pStdSig    = NULL;
	pSig->next = pSig->prev = pSig;


	//8. create the synchronization lock for the synchronization between
	// the data processing data and data exchanging interface.
	pEquip->hSyncLock = Mutex_Create(TRUE);
	if (pEquip->hSyncLock == NULL)
	{
		AppLogOut( EQP_INIT, APP_LOG_ERROR,
			"Fails on creating synchronization lock for the equipment \"%s\"(%d).\n",
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}

	pEquip->hLcdSyncLock = Mutex_Create(TRUE);
	if (pEquip->hLcdSyncLock == NULL)
	{
		AppLogOut( EQP_INIT, APP_LOG_ERROR,
			"Fails on creating synchronization lock for the equipment \"%s\"(%d).\n",
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}

	return TRUE;
}


static BOOL Init_ChkVarOfDispExp(IN int nSigType, 
								 IN void *pChkSig,
								 IN SAMPLE_SIG_VALUE *pSig,
								 IN OUT EXP_ELEMENT	*pVar)
{
	UNUSED(pChkSig);
	UNUSED(pSig);
	UNUSED(pVar);

	if(/*nSigType == SIG_TYPE_CONTROL ||*/ nSigType == SIG_TYPE_ALARM)
	{
		return FALSE;
	}
	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Init_ChkVarOfVirtualSig
 * PURPOSE  : to check a variable of virtual sampling signal is valid or not
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int               nSigType : the type of the sig to be checked
 *            IN void              *pChkSig : the sig to be checked
 *            IN SAMPLE_SIG_VALUE  *pSig    : the sig owns the exp.
 *            IN OUT EXP_ELEMENT	*pVar   : the exp ele.
 * RETURN   : static BOOL : 
 * COMMENTS : Added the pVar. maofuhua, 2005-3-15
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 10:23
 *==========================================================================*/
static BOOL Init_ChkVarOfVirtualSig(IN int nSigType, 
									IN void *pChkSig,
									IN SAMPLE_SIG_VALUE	*pSig,
									IN OUT EXP_ELEMENT	*pVar)
{
#ifdef STRICT_CHECK_EXP_VAR
	if (pChkSig == pSig)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Error: The virtual sampling signal %s refers itself as variable.\n",
			EQP_GET_SIGV_NAME0(pSig));

		return FALSE;
	}
#else
	UNUSED(pVar);
#endif

	// only to check sampling signal
	switch (nSigType)
	{
#ifdef STRICT_CHECK_EXP_VAR
		// a virtual sample can NOT refer another  virtual signal.
	case SIG_TYPE_SAMPLING:
		if (((SAMPLE_SIG_VALUE*)pChkSig)->sv.iSamplerChannel == 
			VIRTUAL_SAMPLING_CHANNEL)
		{
			// a virtual signal can NOT be an input variable of another.
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Error: The virtual sampling signal %s refers another "
				"virtual signal %s.\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_SIGV_NAME0((SAMPLE_SIG_VALUE*)pChkSig));

			return FALSE;
		}
		break;
#endif //STRICT_CHECK_EXP_VAR

	case SIG_TYPE_ALARM:
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Error: The virtual sampling signal %s refers an alarm "
				"signal %s.\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_SIGV_NAME0((ALARM_SIG_VALUE*)pChkSig));

			return FALSE;
		}
		break;

	default:
        break;	// other type signal is allowed
	}

	// Removed: support the sampling channle value(RAW) with {EID, STYPE, SID} in
	// evaluation exp clearly. this type will be set while loading exp.
	// maofuhua, 2005-3-26
	//// if the chk sig and the owner sig are a real sig, the raw value of
	//// the chk sig will be used in evaluation.
	//if ((nSigType != SIG_TYPE_ALARM) &&		// is set/sample/ctrl sig, and
	//	(pSig->sv.iSamplerChannel >= 0) &&	// this is a real sample sig, and
	//	(((SAMPLE_SIG_VALUE*)pChkSig)->sv.iSamplerChannel >= 0))// ref a real sig.
	//{
	//	pVar->byElementType = EXP_ETYPE_VARIABLE_RAW;
	//}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_MapSamplingSigExpression
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 09:40
 *==========================================================================*/
static BOOL Init_MapSamplingSigExpression(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip)
{
	int					i, nSig;
	SAMPLE_SIG_VALUE	*pSig;

	nSig = pEquip->pStdEquip->iSampleSigNum;
	pSig = pEquip->pSampleSigValue;
	for (i = 0; i < nSig; i++, pSig++)
	{
		if ((pSig->sv.iCalculateExpression > 0) && 
			!Init_MapExpressionVar(pSite, pEquip, 
				pSig->sv.pCalculateExpression,
				pSig->sv.iCalculateExpression,
				(INIT_CHK_VAR_PROC)Init_ChkVarOfVirtualSig,
				pSig))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on checking and mapping the expression variales of "
				"the virtual sampling signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		if (pSig->sv.iDispExp > 0)
		{
			if(!Init_MapExpressionVar(pSite,
									pEquip, 
									pSig->sv.pDispExp,
									pSig->sv.iDispExp,
									(INIT_CHK_VAR_PROC)Init_ChkVarOfDispExp,
									pSig))
			{
				AppLogOut(EQP_INIT, APP_LOG_ERROR, 
					"Failed on checking and mapping the display expression variales of "
					"the signal %s of equipment \"%s\"(%d).\n",
					EQP_GET_SIGV_NAME0(pSig),
					EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
				return FALSE;
			}
		}

	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Init_ChkVarOfCtrlSig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int             nSigType : 
 *            IN void            *pChkSig : 
 *            IN CTRL_SIG_VALUE  *pSig    : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 10:53
 *==========================================================================*/
static BOOL Init_ChkVarOfCtrlSig(IN int nSigType, IN void *pChkSig, 
								 IN CTRL_SIG_VALUE *pSig)
{
	UNUSED(pChkSig);
	UNUSED(pSig);

#if 0	// Now let the ctrl signal ref it self. due to settable exp.
		// maofuhua, 2005-3-14
	if (pChkSig == pSig)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Error: The control signal %s refers itself as variable.\n",
			EQP_GET_SIGV_NAME0(pSig));

		return FALSE;
	}
#endif

	switch (nSigType)
	{
	// Change: The control signal also has current value. 2004-11-24


#if 0	// allows alarm signal in settable/ctrlable exp. maofuhua, 2005-4-3
	case SIG_TYPE_ALARM:
		{
			// an alarm signal can NOT be an input variable of another.
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Error: The control signal %s refers an alarm "
				"signal %s.\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_SIGV_NAME0((ALARM_SIG_VALUE*)pChkSig));

			return FALSE;
		}
		break;
#endif

		// the others is OK
	default:
		break;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : BOOL Init_MapControlSigExpression
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   :  static : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 10:51
 *==========================================================================*/
static BOOL Init_MapControlSigExpression(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip)
{
	int				i, nSig;
	CTRL_SIG_VALUE	*pSig;

	nSig = pEquip->pStdEquip->iCtrlSigNum;
	pSig = pEquip->pCtrlSigValue;
	for (i = 0; i < nSig; i++, pSig++)
	{
		if ((pSig->sv.iControllableExpression > 0) &&
			!Init_MapExpressionVar(pSite, pEquip, 
				pSig->sv.pControllableExpression,
				pSig->sv.iControllableExpression,
				(INIT_CHK_VAR_PROC)Init_ChkVarOfCtrlSig,
				pSig))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on checking and mapping the expression variales of "
				"the control signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}
		
		if ((pSig->sv.iDispExp > 0)
			&& !Init_MapExpressionVar(pSite,
									pEquip, 
									pSig->sv.pDispExp,
									pSig->sv.iDispExp,
									(INIT_CHK_VAR_PROC)Init_ChkVarOfDispExp,
									pSig))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Failed on checking and mapping the display expression variales of "
				"the signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// map the on-ctrl sig
		if ((pSig->pStdSig->OnCtrl.expVar.iRelevantEquipIndex >= 0) && // var is configured.
			(pSig->pStdSig->OnCtrl.expVar.iSigID > 0))	// ID start from 1.
		{
			if (Init_MapExpVarA(pSite, pEquip, &pSig->pStdSig->OnCtrl.expVar,
				&pSig->sv.onCtrl.pEquip, &pSig->sv.onCtrl.pSig) < 0)	// map error.
			{
				AppLogOut(EQP_INIT, APP_LOG_ERROR, 
					"Fails on checking and mapping the on-ctrl variales of "
					"the control signal %s of equipment \"%s\"(%d).\n",
					EQP_GET_SIGV_NAME0(pSig),
					EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

				return FALSE;
			}

			// set the on-ctrl value
			pSig->sv.onCtrl.fValue = pSig->pStdSig->OnCtrl.fValue;
		}
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Init_ChkVarOfSetSig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int            nSigType : 
 *            IN void           *pChkSig : 
 *            IN SET_SIG_VALUE  *pSig    : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 11:02
 *==========================================================================*/
static BOOL Init_ChkVarOfSetSig(IN int nSigType, IN void *pChkSig, 
								 IN SET_SIG_VALUE *pSig)
{
	UNUSED(pChkSig);
	UNUSED(pSig);

#if 0	// Now let the setting signal ref it self. due to settable exp.
		// maofuhua, 2005-3-14
	if (pChkSig == pSig)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Error: The setting signal %s refers itself as variable.\n",
			EQP_GET_SIGV_NAME0(pSig));

		return FALSE;
	}
#endif

	switch (nSigType)
	{

#if 0	// allows alarm signal in settable/ctrlable exp. maofuhua, 2005-4-3
	case SIG_TYPE_ALARM:
		{
			// an alarm signal can NOT be an input variable of another.
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Error: The setting signal %s refers an alarm "
				"signal %s.\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_SIGV_NAME0((ALARM_SIG_VALUE*)pChkSig));

			return FALSE;
		}
		break;
#endif

		// the others is OK
	default:
		break;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : BOOL Init_MapSettingSigExpression
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   :  static : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 10:51
 *==========================================================================*/
static BOOL Init_MapSettingSigExpression(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip)
{
	int				i, nSig;
	SET_SIG_VALUE	*pSig;

	nSig = pEquip->pStdEquip->iSetSigNum;
	pSig = pEquip->pSetSigValue;
	for (i = 0; i < nSig; i++, pSig++)
	{
		if ((pSig->sv.iSettableExpression > 0) &&
			!Init_MapExpressionVar(pSite, pEquip, 
				pSig->sv.pSettableExpression,
				pSig->sv.iSettableExpression,
				(INIT_CHK_VAR_PROC)Init_ChkVarOfSetSig,
				pSig))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on checking and mapping the expression variales of "
				"the setting signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}
		
		if ((pSig->sv.iDispExp > 0)
			&& !Init_MapExpressionVar(pSite,
									pEquip, 
									pSig->sv.pDispExp,
									pSig->sv.iDispExp,
									(INIT_CHK_VAR_PROC)Init_ChkVarOfDispExp,
									pSig))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Failed on checking and mapping the display expression variales of "
				"the signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// map the on-ctrl sig
		if ((pSig->pStdSig->OnCtrl.expVar.iRelevantEquipIndex >= 0) && // var is configured.
			(pSig->pStdSig->OnCtrl.expVar.iSigID > 0))	// ID start from 1.
		{
			if (Init_MapExpVarA(pSite, pEquip, &pSig->pStdSig->OnCtrl.expVar,
				&pSig->sv.onCtrl.pEquip, &pSig->sv.onCtrl.pSig) < 0)	// map error.
			{
				AppLogOut(EQP_INIT, APP_LOG_ERROR, 
					"Fails on checking and mapping the on-ctrl variales of "
					"the setting signal %s of equipment \"%s\"(%d).\n",
					EQP_GET_SIGV_NAME0(pSig),
					EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

				return FALSE;
			}

			// set the on-ctrl value
			pSig->sv.onCtrl.fValue = pSig->pStdSig->OnCtrl.fValue;
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_ChkVarOfAlarmSig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int              nSigType : 
 *            IN void             *pChkSig : 
 *            IN ALARM_SIG_VALUE  *pSig    : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 11:07
 *==========================================================================*/
static BOOL Init_ChkVarOfAlarmSig(IN int nSigType, IN void *pChkSig, 
								  IN ALARM_SIG_VALUE *pSig )
{
#ifndef ALARM_SUPPORT_HYSTERESIS	// if do NOT support alarm hysteresis
	if (pChkSig == pSig)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Error: The alarm signal %s refers itself as variable.\n",
			EQP_GET_SIGV_NAME0(pSig));

		return FALSE;
	}
#endif


	switch (nSigType)
	{
	case SIG_TYPE_SAMPLING:
		// add the alarm to the ppRelatedAlarms of the sampling sig
		{
			SAMPLE_SIG_VALUE *pSampleSig = (SAMPLE_SIG_VALUE *)pChkSig;

			if (AppendArrayItem((void **)&pSampleSig->sv.ppRelatedAlarms,
				sizeof(pSampleSig->sv.ppRelatedAlarms[0]),
				&pSampleSig->sv.iRelatedAlarms,
				&pSig) == NULL)
			{
				AppLogOut(EQP_INIT, APP_LOG_ERROR, 
					"Out of memory on add alarm \"%s\"(%d) to reference table of "
					"sampling signal \"%s\"(%d).\n",
					EQP_GET_SIGV_NAME0(pSig),
					pSig->pStdSig->iSigID,
					EQP_GET_SIGV_NAME0(pSampleSig),
					pSampleSig->pStdSig->iSigID);

				return FALSE;
			}
		}
		break;

#ifndef ALARM_SUPPORT_HYSTERESIS	// if do NOT support alarm hysteresis
	case SIG_TYPE_ALARM:
		{
			// an alarm signal can NOT be an input variable of another.
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Error: The setting signal %s refers an alarm "
				"signal %s.\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_SIGV_NAME0((ALARM_SIG_VALUE*)pChkSig));

			return FALSE;
		}
		break;
#endif

		// the others is OK
	default:
		break;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_ChkVarOfAlarmSuppression
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int              nSigType : 
 *            IN void             *pChkSig : 
 *            IN ALARM_SIG_VALUE  *pSig    : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 11:20
 *==========================================================================*/
static BOOL Init_ChkVarOfAlarmSuppression(IN int nSigType, IN void *pChkSig, 
								  IN ALARM_SIG_VALUE *pSig )
{
	if (pChkSig == pSig)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Error: The setting signal %s refers itself as variable.\n",
			EQP_GET_SIGV_NAME0(pSig));

		return FALSE;
	}

	switch (nSigType)
	{
	case SIG_TYPE_ALARM:
			// the supression var must be an alarm signal
		break;

	default:
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Error: The supression variable %s of %s is NOT an alarm "
				"signal.\n",				
				EQP_GET_SIGV_NAME0((ALARM_SIG_VALUE*)pChkSig),
				EQP_GET_SIGV_NAME0(pSig));

			return FALSE;
		}
		break;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_ChkVarOfEquipSuppression
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int         nSigType : 
 *            IN void        *pChkSig : 
 *            IN EQUIP_INFO  *pEquip  : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 11:31
 *==========================================================================*/
static BOOL Init_ChkVarOfEquipSuppression(IN int nSigType, IN void *pChkSig, 
								  IN EQUIP_INFO *pEquip )
{
	switch (nSigType)
	{
	case SIG_TYPE_ALARM:
			// the supression var must be an alarm signal
	case SIG_TYPE_SAMPLING:
			// the supression var must be an alarm signal
		break;

	default:
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Error: The supression variable %s of equip %s is NOT "
				"an alarm signal.\n",				
				EQP_GET_SIGV_NAME0((ALARM_SIG_VALUE*)pChkSig),
				EQP_GET_EQUIP_NAME0(pEquip));

			return FALSE;
		}
		break;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : Init_MapAlarmSigExpression
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 11:06
 *==========================================================================*/
static BOOL Init_MapAlarmSigExpression(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip)
{
	int				i, nSig;
	ALARM_SIG_VALUE	*pSig;

	nSig = pEquip->pStdEquip->iAlarmSigNum;
	pSig = pEquip->pAlarmSigValue;
	for (i = 0; i < nSig; i++, pSig++)
	{
		// check the alarm evaluation exp
		if ((pSig->sv.iAlarmExpression > 0) &&
			!Init_MapExpressionVar(pSite, pEquip, 
				pSig->sv.pAlarmExpression,
				pSig->sv.iAlarmExpression,
				(INIT_CHK_VAR_PROC)Init_ChkVarOfAlarmSig,
				pSig))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on checking and mapping the evaluation expression "
				"variales of the alarm signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// check the suppression exp
		if ((pSig->sv.iSuppressExpression > 0) &&
			!Init_MapExpressionVar(pSite, pEquip, 
				pSig->sv.pSuppressExpression,
				pSig->sv.iSuppressExpression,
				(INIT_CHK_VAR_PROC)Init_ChkVarOfAlarmSuppression,
				pSig))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on checking and mapping the suppression variales of "
				"the alarm signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIGV_NAME0(pSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : Init_MapEquipExpressionVar
 * PURPOSE  : map the variables from (EquipID, SigType, SigID) to the actual
 *            signal address(pointer)
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-31 20:39
 *==========================================================================*/
static BOOL Init_MapEquipExpressionVar(IN OUT SITE_INFO *pSite,
								IN OUT EQUIP_INFO *pEquip)
{
	//1. map and check the alarm filter
	if (!Init_MapExpressionVar(pSite, pEquip,
		pEquip->pAlarmFilterExpression,
		pEquip->iAlarmFilterExpression,
		(INIT_CHK_VAR_PROC)Init_ChkVarOfEquipSuppression,//  exp var checker
		pEquip))						// the param of the checking callback
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on checking and mapping the variales of the "
			"alarm filter expression of equipment \"%s\"(%d).\n",
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}

	//2. map the evaluation expression of the sampling signal
	if (!Init_MapSamplingSigExpression(pSite, pEquip))
	{
		return FALSE;
	}


	//3. map the controllable expression of the control signal 
	if (!Init_MapControlSigExpression(pSite, pEquip))
	{
		return FALSE;
	}
		
	//4. map the settable expression of the setting signal 
	if (!Init_MapSettingSigExpression(pSite, pEquip))
	{
		return FALSE;
	}

	//5. map the evaluation expression of the alarm signal and
	//   map the suppression expression of the alarm signal
	if (!Init_MapAlarmSigExpression(pSite, pEquip))
	{
		return FALSE;
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Init_ChkExpressVarConfigState
 * PURPOSE  : to check config state of the varibales of the exp.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT EXP_ELEMENT  *ppExp : 
 *            IN int              iExp  : 
 * RETURN   : static BOOL : TRUE: The exp is OK, 
 *                          FALSE: any var in the exp is NOT configured.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-14 14:51
 *==========================================================================*/
static BOOL Init_ChkExpressVarConfigState(IN OUT EXP_ELEMENT	*pExp, 
								  IN int iExp)
{
	int				n;
	EXP_ELEMENT		*pVar = pExp;

	if (pExp == NULL)		// not need to map var
	{
		return TRUE;
	}


	for (n = 0; n < iExp; n++, pVar++)
	{
		// map var to addr
		if (EXP_IS_VAR(pVar))
		{
			if ((pVar->pElement == VAR_NOT_CONFIGURED) ||
				!SIG_VALUE_IS_CONFIGURED(pVar->pElement))
			{
				return FALSE;
			}
		}
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Init_ChkEquipSignalConfigState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static BOOL : TRUE if found any signal is not configured.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-14 15:09
 *==========================================================================*/
static BOOL Init_ChkEquipSignalConfigState(IN OUT SITE_INFO *pSite, 
											 IN OUT EQUIP_INFO *pEquip)
{
	int		nSig;
	// the signal may be used as the var of another signal.
	BOOL	bFoundNotConfiguredSig = FALSE; 

	UNUSED(pSite);

	// 1. check the sampling signals
	if (pEquip->pSampleSigValue != NULL)
	{
		SAMPLE_SIG_VALUE *pSig;
		
		pSig = pEquip->pSampleSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iSampleSigNum; nSig++, pSig++)
		{
			// chk the config state of the virtual signal, if any of the var of
			// the exp is not configured, the exp will be deleted and the signal
			// configured state and valid state will be cleared. 
			if ((pSig->sv.pCalculateExpression != NULL) &&
				!Init_ChkExpressVarConfigState(pSig->sv.pCalculateExpression,
				pSig->sv.iCalculateExpression))
			{
				// release the calc exp
				SAFELY_DELETE(pSig->sv.pCalculateExpression);
				pSig->sv.iCalculateExpression = 0;

				SIG_STATE_CLR(pSig, VALUE_STATE_IS_CONFIGURED);
				SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID);
			}

			//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
			// if the related equip not configured in G3 solution, delete the
			// exp and display attr will be cleared
			if ((pSig->sv.pDispExp != NULL) &&
				!Init_ChkExpressVarConfigState(pSig->sv.pDispExp,
				pSig->sv.iDispExp))
			{
				// release the exp
				SAFELY_DELETE(pSig->sv.pDispExp);
				pSig->sv.iDispExp = 0;

				SIG_STATE_CLR(pSig, VALUE_STATE_NEED_DISPLAY);
			}
			//G3_OPT end

			if (!SIG_VALUE_IS_CONFIGURED(pSig))
			{
				bFoundNotConfiguredSig = TRUE;
			}
		}
	}


	// 2. check control signals
	if (pEquip->pCtrlSigValue != NULL)
	{
		CTRL_SIG_VALUE *pSig;
		
		pSig = pEquip->pCtrlSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iCtrlSigNum; nSig++, pSig++)
		{
			// if the any var of the controllable exp of the control signal,
			// the exp will be removed, and the signal will be set to controllable
			// be default. the exp will be deleted.
			if ((pSig->sv.pControllableExpression != NULL) &&
				!Init_ChkExpressVarConfigState(pSig->sv.pControllableExpression,
				pSig->sv.iControllableExpression))
			{
				// release the exp
				SAFELY_DELETE(pSig->sv.pControllableExpression);
				pSig->sv.iControllableExpression = 0;
			}

			//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
			// if the related equip not configured in G3 solution, delete the
			// exp and display attr will be cleared
			if ((pSig->sv.pDispExp != NULL) &&
				!Init_ChkExpressVarConfigState(pSig->sv.pDispExp,
				pSig->sv.iDispExp))
			{
				// release the exp
				SAFELY_DELETE(pSig->sv.pDispExp);
				pSig->sv.iDispExp = 0;

				SIG_STATE_CLR(pSig, VALUE_STATE_NEED_DISPLAY);
			}
			//G3_OPT end

			if (!SIG_VALUE_IS_CONFIGURED(pSig))
			{
				bFoundNotConfiguredSig = TRUE;
			}
		}
	}


	//3. check setting signals
	if (pEquip->pSetSigValue != NULL)
	{
		SET_SIG_VALUE *pSig;
		
		pSig = pEquip->pSetSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iSetSigNum; nSig++, pSig++)
		{
			// if the any var of the controllable exp of the control signal,
			// the exp will be removed, and the signal will be set to controllable
			// be default. the exp will be deleted.
			if ((pSig->sv.pSettableExpression != NULL) &&
				!Init_ChkExpressVarConfigState(pSig->sv.pSettableExpression,
				pSig->sv.iSettableExpression))
			{
				// release the exp
				SAFELY_DELETE(pSig->sv.pSettableExpression);
				pSig->sv.iSettableExpression = 0;
			}

			//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
			// if the related equip not configured in G3 solution, delete the
			// exp and display attr will be cleared
			if ((pSig->sv.pDispExp != NULL) &&
				!Init_ChkExpressVarConfigState(pSig->sv.pDispExp,
				pSig->sv.iDispExp))
			{
				// release the exp
				SAFELY_DELETE(pSig->sv.pDispExp);
				pSig->sv.iDispExp = 0;

				SIG_STATE_CLR(pSig, VALUE_STATE_NEED_DISPLAY);
			}
			//G3_OPT end

			if (!SIG_VALUE_IS_CONFIGURED(pSig))
			{
				bFoundNotConfiguredSig = TRUE;
			}
		}
	}


	//4. check alarm signals
	if (pEquip->pAlarmSigValue != NULL)
	{
		ALARM_SIG_VALUE *pSig;
		
		pSig = pEquip->pAlarmSigValue;
		for (nSig = 0; nSig < pEquip->pStdEquip->iAlarmSigNum; nSig++, pSig++)
		{
			// if any of the var of the alarm exp is not configured, the alarm 
			// signal will be set to invalid and not-configured.
			if ((pSig->sv.pAlarmExpression != NULL) &&
				!Init_ChkExpressVarConfigState(pSig->sv.pAlarmExpression,
				pSig->sv.iAlarmExpression))
			{
				SIG_STATE_CLR(pSig, VALUE_STATE_IS_CONFIGURED);
				SIG_STATE_CLR(pSig, VALUE_STATE_IS_VALID);

				// release the exp
				SAFELY_DELETE(pSig->sv.pAlarmExpression);
				pSig->sv.iAlarmExpression = 0;

				// also delete the suppression exp. -- it's no use.
				SAFELY_DELETE(pSig->sv.pSuppressExpression);
				pSig->sv.iSuppressExpression = 0;
			}

			// check the suppression expression
			else if ((pSig->sv.pSuppressExpression != NULL) &&
				!Init_ChkExpressVarConfigState(pSig->sv.pSuppressExpression,
				pSig->sv.iSuppressExpression))
			{
				// release the exp
				SAFELY_DELETE(pSig->sv.pSuppressExpression);
				pSig->sv.iSuppressExpression = 0;
			}

			if (!SIG_VALUE_IS_CONFIGURED(pSig))
			{
				bFoundNotConfiguredSig = TRUE;
			}
		}
	}

	// check the alarm filter exp.
	if ((pEquip->pAlarmFilterExpression != NULL) &&
		!Init_ChkExpressVarConfigState(pEquip->pAlarmFilterExpression,
		pEquip->iAlarmFilterExpression))
	{
        SAFELY_DELETE(pEquip->pAlarmFilterExpression);
	}

	return bFoundNotConfiguredSig;	
}



/*==========================================================================*
 * FUNCTION : Init_ChkSiteSignalConfigState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : TRUE if found any signal is not configured.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-14 16:18
 *==========================================================================*/
static BOOL Init_ChkSiteSignalConfigState(IN OUT SITE_INFO *pSite)
{
	int				n;
	EQUIP_INFO		*pEquip;
	BOOL	bFoundNotConfiguredSig = FALSE; 

	for (pEquip = pSite->pEquipInfo, n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		if (Init_ChkEquipSignalConfigState(pSite, pEquip))
		{
			bFoundNotConfiguredSig = TRUE;
		}
	}

	return bFoundNotConfiguredSig;
}



/*==========================================================================*
 * FUNCTION : Init_ChkOnCtrA
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO      *pEquip : 
 *            IN CTRL_SIG_VALUE  *pSig   : 
 * RETURN   : static BOOL : FALSE on recursive ref or too deep level ref
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-03 19:37
 *==========================================================================*/
static BOOL Init_ChkOnCtrA(IN EQUIP_INFO *pEquip, IN CTRL_SIG_VALUE *pSig)
{
	int				nDepth, nRefInfoLen;
	char			szRefInfo[256];
	ON_CTRL_ACTION  *pAct	 = &pSig->sv.onCtrl;
	CTRL_SIG_VALUE  *pRefSig = (CTRL_SIG_VALUE *)pAct->pSig;

	if (pRefSig == NULL)	// no on-ctrl
	{
		return TRUE;
	}

	nDepth	    = 0;
	nRefInfoLen = 0;

	// check. the reason of condition is '<=' for logging the signal info.
	while ((pRefSig != NULL) && (nDepth <= CTRL_SIG_MAX_ON_CTRL_DEPTH))
	{
		if (nRefInfoLen < (int)sizeof(szRefInfo))
		{
			nRefInfoLen += snprintf(&szRefInfo[nRefInfoLen], 
				(size_t)(sizeof(szRefInfo) - nRefInfoLen),
				"->[%s(%d), %d, %d]",
				EQP_GET_EQUIP_NAME0(pAct->pEquip), pAct->pEquip->iEquipID, 
				pRefSig->bv.ucSigType, pRefSig->pStdSig->iSigID);
		}

		nDepth++;

		if ((pRefSig == pSig) ||	// test not itself here for logging info.
			((pRefSig->bv.ucSigType != SIG_TYPE_CONTROL) &&	// NOT ctrl and
			(pRefSig->bv.ucSigType != SIG_TYPE_SETTING)))	// NOT set signal
		{
			break;
		}

		pAct	 = &pRefSig->sv.onCtrl;
		pRefSig  = (CTRL_SIG_VALUE *)pAct->pSig;
	}

	// logout error msg
	if ((pRefSig == pSig) || (nDepth >= CTRL_SIG_MAX_ON_CTRL_DEPTH))
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR,
			"Fails on checking the on-ctrl related sig of signal %s(%d) of "
			"equipment %s(%d) for %s.\n",
			EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
			((pRefSig == pSig) ? "recursive on-ctrl ref" 
			: "too deep nested on-ctrl."));

		AppLogOut(EQP_INIT, APP_LOG_ERROR,
			"Details of the on-ctrl related sigs of signal %s(%d) are: N=%d, %s\n",
			EQP_GET_SIGV_NAME0(pSig), pSig->pStdSig->iSigID,
			nDepth, szRefInfo);

		return FALSE;	// too much nested on-ctrl level
	}

	return TRUE;	// OK
}


/*==========================================================================*
 * FUNCTION : Init_ChkOnCtrlRelatedSig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-03 17:27
 *==========================================================================*/
static BOOL Init_ChkOnCtrlRelatedSig(IN SITE_INFO *pSite)
{
	int				n;
	EQUIP_INFO		*pEquip;
	int				i, nSig;

	for (pEquip = pSite->pEquipInfo, n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		// check the on-ctrl ref of the ctrl sigs
		if (pEquip->pStdEquip->iCtrlSigNum > 0)
		{
			CTRL_SIG_VALUE	*pSig;

			nSig = pEquip->pStdEquip->iCtrlSigNum;
			pSig = pEquip->pCtrlSigValue;
			for (i = 0; i < nSig; i++, pSig++)
			{
				if ((pSig->sv.onCtrl.pSig != NULL) &&
					!Init_ChkOnCtrA(pEquip, (CTRL_SIG_VALUE *)pSig))
				{
					return FALSE;
				}
			}
		}

		// check the on-ctrl ref of the setting sigs
		if (pEquip->pStdEquip->iSetSigNum > 0)
		{
			SET_SIG_VALUE	*pSig;

			nSig = pEquip->pStdEquip->iSetSigNum;
			pSig = pEquip->pSetSigValue;
			for (i = 0; i < nSig; i++, pSig++)
			{
				if ((pSig->sv.onCtrl.pSig != NULL) &&
					!Init_ChkOnCtrA(pEquip, (CTRL_SIG_VALUE *)pSig))
				{
					return FALSE;
				}
			}
		}

	}

	return TRUE;

}


/*==========================================================================*
 * FUNCTION : *Init_GetStdEquipRefByTypeId
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite       : 
 *            IN int            nEquipTypeID : 
 * RETURN   : static STDEQUIP_TYPE_INFO : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-31 20:25
 *==========================================================================*/
 static STDEQUIP_TYPE_INFO *Init_GetStdEquipRefByTypeId(
			IN OUT SITE_INFO *pSite, IN int nEquipTypeID)
{
	STDEQUIP_TYPE_INFO	*pStdEquip;
	int		n;

	if (nEquipTypeID <= 0)
	{
		return NULL;
	}

	// quikly search the ID. The ID start from 1. I think they are unused
	if (nEquipTypeID <= pSite->iStdEquipTypeNum)
	{
		pStdEquip = &pSite->pStdEquipTypeInfo[nEquipTypeID-1];
		if (pStdEquip->iTypeID == nEquipTypeID)
		{
			return pStdEquip;
		}		
	}

	// the ID is not equal to the INDEX, compare the ID one by one
	pStdEquip = pSite->pStdEquipTypeInfo;
	for (n = 0; n < pSite->iStdEquipTypeNum; n++, pStdEquip++)
	{
		if (pStdEquip->iTypeID == nEquipTypeID)
		{
			return pStdEquip;
		}		
	}

	return NULL;	// not found.
}


/*==========================================================================*
 * FUNCTION : *Init_GetEquipRefById
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite   : 
 *            IN int            nEquipID : 
 * RETURN   : EQUIP_INFO : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-31 20:25
 *==========================================================================*/
EQUIP_INFO *Init_GetEquipRefById(IN OUT SITE_INFO *pSite, 
										  IN int nEquipID)
{
	EQUIP_INFO *pEquip;
	int		n;

	if (nEquipID <= 0)
	{
		return NULL;
	}

	//G3_OPT [debug info], by Lin.Tao.Thomas
	/*struct timeval t_start,t_end; 
	long cost_time = 0; 
	gettimeofday(&t_start, NULL); 
	long start = ((long)t_start.tv_sec)*1000+(long)t_start.tv_usec/1000; */
	//printf("Start time: %ld ms\n", start); 

	
	// quikly search the ID. The ID start from 1.
	if (nEquipID <= pSite->iEquipNum)
	{
		pEquip = &pSite->pEquipInfo[nEquipID-1];
		if( pEquip->iEquipID == nEquipID)
		{
			return pEquip;
		}
	}

	// the ID is not equal to the INDEX, compare the ID one by one
	pEquip = pSite->pEquipInfo;
	for (n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		if (pEquip->iEquipID == nEquipID)
		{
			//G3_OPT [debug info], by Lin.Tao.Thomas
			//get end time 
			/*gettimeofday(&t_end, NULL); 
			long end = ((long)t_end.tv_sec)*1000+(long)t_end.tv_usec/1000; 
			cost_time = end - start; 
			printf("Cost time of equip (%d): %ld ms\n", nEquipID, cost_time);*/ 


			return pEquip;
		}
	}
	
	return NULL;	// not found.
}



/*==========================================================================*
 * FUNCTION : Init_GetEquipRefByStdId
 * PURPOSE  : get the first equipment which has the std type ID
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite    : 
 *            IN int        nStdTyeId : 
 * RETURN   : static EQUIP_INFO *: 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-01 23:23
 *==========================================================================*/
EQUIP_INFO *Init_GetEquipRefByStdId(IN SITE_INFO *pSite, 
										   IN int nStdTyeId)
{
	EQUIP_INFO *pEquip;
	int		n;

	
	// the ID is not equal to the INDEX, compare the ID one by one
	pEquip = pSite->pEquipInfo;
	for (n = 0; n < pSite->iEquipNum; n++, pEquip++)
	{
		if (pEquip->pStdEquip->iTypeID == nStdTyeId)
		{
			return pEquip;
		}
	}
	
	AppLogOut(EQP_INIT, APP_LOG_WARNING,
		"Fails on getting the first equip ref by Std equipment ID %d.\n",
		nStdTyeId);

	return NULL;	// not found.
}



// macro to find a signal value pointer addr by ID
#define GET_SIG_VALUE_BY_ID( SIG_VAL_TYPE, P_SIG, N_SIG,	    \
							 nSigId, pFound)					\
{																\
	SIG_VAL_TYPE *pSig;											\
	int			  nSig, i;										\
																\
	pFound = NULL;												\
																\
	if (nSigId > 0) /* ID start from 1 */						\
	{															\
		nSig = (N_SIG);											\
																\
		/* quikly search the ID, the ID start from 1.*/			\
		if (nSigId <= nSig)										\
		{														\
			pSig = &(P_SIG)[nSigId-1]; 							\
			if(pSig->pStdSig->iSigID == nSigId)					\
			{													\
				pFound = (void *)pSig;							\
			}													\
		}														\
																\
		if (pFound == NULL)	/* fails on quick searching */		\
		{/* ID is not equal to the INDEX,compare ID one by one*/\
			pSig = (P_SIG);										\
			for (i = 0; i < nSig; i++, pSig++)					\
			{													\
				if(pSig->pStdSig->iSigID == nSigId)				\
				{												\
					pFound = (void *)pSig;						\
					break;										\
				}												\
			}													\
		}														\
	}															\
}																\
/* End of macro GET_SIG_VALUE_BY_ID */




/*==========================================================================*
 * FUNCTION : *Init_GetEquipSigRefById
 * PURPOSE  : According signal id,signal type,and equip address,get current 
 *            signal actual address under pEquip->pxxValue
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT EQUIP_INFO  *pEquip  : 
 *            IN int             nSigType : 
 *            IN int             nSigId   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 11:50
 *==========================================================================*/
void *Init_GetEquipSigRefById(IN EQUIP_INFO *pEquip, 
								IN int nSigType, IN int nSigId)
{
	void	*pFoundSig = NULL;

	switch (nSigType)
	{
	case SIG_TYPE_SAMPLING:
		{
			GET_SIG_VALUE_BY_ID(SAMPLE_SIG_VALUE,
				pEquip->pSampleSigValue,
				pEquip->pStdEquip->iSampleSigNum,
				nSigId,
				pFoundSig);
		}
		break;

	case SIG_TYPE_CONTROL:
		{
			GET_SIG_VALUE_BY_ID(CTRL_SIG_VALUE,
				pEquip->pCtrlSigValue,
				pEquip->pStdEquip->iCtrlSigNum,
				nSigId,
				pFoundSig);
		}
		break;

	case SIG_TYPE_SETTING:
		{
			GET_SIG_VALUE_BY_ID(SET_SIG_VALUE,
				pEquip->pSetSigValue,
				pEquip->pStdEquip->iSetSigNum,
				nSigId,
				pFoundSig);
		}
		break;

	case SIG_TYPE_ALARM:
		{
			GET_SIG_VALUE_BY_ID(ALARM_SIG_VALUE,
				pEquip->pAlarmSigValue,
				pEquip->pStdEquip->iAlarmSigNum,
				nSigId,
				pFoundSig);
		}
		break;
	}

	if (pFoundSig == NULL)
	{
		/*AppLogOut(EQP_INIT, APP_LOG_UNUSED, "Failed to find signal[Type:%d,ID:%d] "
			"in equipment \"%s\"(%d).\n",
			nSigType, nSigId,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);*/
	}
    
	return pFoundSig;
}



/*==========================================================================*
 * FUNCTION : *Init_GetStdSamplerRefById
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite        : 
 *            IN int            nStdSamplerID : 
 * RETURN   : STDSAMPLER_INFO : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-12 17:34
 *==========================================================================*/
STDSAMPLER_INFO *Init_GetStdSamplerRefById(IN OUT SITE_INFO *pSite, 
										  IN int nStdSamplerID)
{
	int n;
	STDSAMPLER_INFO *pStdSampler;

	if (nStdSamplerID <= 0)
	{
		return NULL;
	}

	// quikly search the ID. The ID start from 1.
	if (nStdSamplerID <= pSite->iStdSamplerNum)
	{
		pStdSampler = &pSite->pStdSamplerInfo[nStdSamplerID-1];
		if (pStdSampler->iStdSamplerID == nStdSamplerID)
		{
			return pStdSampler;
		}
	}

	// the ID is not equal to the INDEX, compare the ID one by one
	pStdSampler = pSite->pStdSamplerInfo;
	for (n = 0; n < pSite->iStdSamplerNum; n++, pStdSampler++)
	{
		if (pStdSampler->iStdSamplerID == nStdSamplerID)
		{
			// find the std sampler, increase the ref num of the std sampler
			return pStdSampler;
		}
	}


	return NULL; // NOT found
}


/*==========================================================================*
 * FUNCTION : *Init_GetSamplerRefById
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite     : 
 *            IN int            nSamplerID : 
 * RETURN   : SAMPLER_INFO : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-31 20:25
 *==========================================================================*/
SAMPLER_INFO *Init_GetSamplerRefById(IN OUT SITE_INFO *pSite, 
										  IN int nSamplerID)
{
	SAMPLER_INFO *pSampler;
	int		n;

	if (nSamplerID <= 0)
	{
		return NULL;
	}

	// quikly search the ID. The ID start from 1.
	if (nSamplerID <= pSite->iSamplerNum)
	{
		pSampler = &pSite->pSamplerInfo[nSamplerID-1];
		if( pSampler->iSamplerID == nSamplerID)
		{
			return pSampler;
		}
	}

	// the ID is not equal to the INDEX, compare the ID one by one
	pSampler = pSite->pSamplerInfo;
	for (n = 0; n < pSite->iSamplerNum; n++, pSampler++)
	{
		if (pSampler->iSamplerID == nSamplerID)
		{
			return pSampler;
		}
	}
	
	return NULL;	// not found.
}

/*==========================================================================*
 * FUNCTION : Init_MakeEquipReference
 * PURPOSE  : Accoring to the list of relevant equipments and alarm filter 
 *			  expression of this equipment, add this equipment to the reference
 *			  table(ppEquipReferredThis) of the other equipment that is referred
 *			  by this equipment
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite  : 
 *            IN EQUIP_INFO     *pEquip : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-28 15:48
 *==========================================================================*/
static BOOL Init_MakeEquipReference(IN OUT SITE_INFO *pSite,
									IN EQUIP_INFO *pEquip)
{
	int				n;
	EXP_ELEMENT		*pVar;
	int				nEquipID;
	EQUIP_INFO		*pRefEquip;

	// 1.3.1 Accoring to the list of relevant equipments
	for (n = 0; n < pEquip->iRelevantEquipListLength; n++ )
	{
		nEquipID = pEquip->pRelevantEquipList[n];

		if (nEquipID == -1)	// this equipment is not configured. ignore it
		{
			continue;
		}

		// get the reference equipment address 	
		pRefEquip = Init_GetEquipRefById(pSite, nEquipID);

		if (pRefEquip == NULL)
		{
			//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
			//The equip maybe not configured in G3 solution, using default value of exp, just warning
			/*AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"The equipment ID %d in the relevant equipment list of "
				"equipment \"%s\"(%d) is invalid.\n",
				nEquipID, 
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
			return FALSE;*/
			AppLogOut(EQP_INIT, APP_LOG_WARNING, 
				"Equip ID %d in the equip list of "
				"\"%s\"(%d) is un-configured.\n",
				nEquipID, 
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
			
			pEquip->pRelevantEquipList[n] = -1;   //set as non-configured, map later

			continue;
			//G3_OPT end
		}

		// Add this equipment to the reference table of the referred equipment
		if ((FindIntItemIndex((int)pEquip, (int *)pRefEquip->ppEquipReferredThis,
			pRefEquip->iEquipReferredThis) < 0) &&	// not in the list
			(AppendArrayItem((void **)&pRefEquip->ppEquipReferredThis,
			sizeof(pRefEquip->ppEquipReferredThis[0]),
			&pRefEquip->iEquipReferredThis,
			&pEquip) == NULL))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on add equipment \"%s\"(%d) to reference table of "
				"equipment \"%s\"(%d).\n",
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
				EQP_GET_EQUIP_NAME0(pRefEquip), pRefEquip->iEquipID);

			return FALSE;
		}
	}
	
	// 1.3.2 Accoring to the alarm filter expression
	pVar = pEquip->pAlarmFilterExpressionCfg;
	for (n = 0; n < pEquip->iAlarmFilterExpression; n++, pVar++ )
	{
		if (EXP_IS_VAR(pVar))
		{
			// get the ID of the reffered equipment
			// iRelevantEquipIndex is an actual equipment ID 
			nEquipID = 
				((EXP_VARIABLE *)pVar->pElement)->iRelevantEquipIndex;
			
			pRefEquip = Init_GetEquipRefById(pSite, nEquipID);
			if (pRefEquip == NULL)
			{
				AppLogOut(EQP_INIT, APP_LOG_ERROR, 
					"No such equipment(ID=%d) in the #%d variable of the "
					"alarm filter expression of the equipment \"%s\"(%d).\n",
					nEquipID, n+1,
					EQP_GET_EQUIP_NAME0(pEquip),    pEquip->iEquipID);

				return FALSE;
			}
			
			// add this equipment to the pRefEquip->ppEquipReferredThis
			if ((FindIntItemIndex((int)pEquip, (int *)pRefEquip->iEquipReferredThis,
				pRefEquip->iEquipReferredThis) < 0) &&	// not in the list
				(AppendArrayItem((void **)&pRefEquip->ppEquipReferredThis,
				sizeof(pRefEquip->ppEquipReferredThis[0]),
				&pRefEquip->iEquipReferredThis,
				&pEquip) == NULL))
			{
				AppLogOut(EQP_INIT, APP_LOG_ERROR, 
					"Out of memory on add equipment \"%s\"(%d) to reference "
					"table of equipment \"%s\"(%d).\n",
					EQP_GET_EQUIP_NAME0(pEquip),    pEquip->iEquipID,
					EQP_GET_EQUIP_NAME0(pRefEquip),	pRefEquip->iEquipID);

				return FALSE;
			}
		}
	}
	
	return TRUE;
}

typedef BOOL (*INIT_A_SIGNAL_PROC)(IN OUT  SITE_INFO *pSite,
							 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex);


/*==========================================================================*
 * FUNCTION : Init_EquipValueSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO           *pSite          : 
 *            IN OUT EQUIP_INFO      *pEquip         : 
 *            IN const char          *pszSigTypeName : 
 *            IN int                 nSig            : signal number
 *            OUT char               **pSigBuf       : 
 *            IN int                 nSigItemSize    : every singal size
 *            IN INIT_A_SIGNAL_PROC  pfnInitSignal   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-27 18:57
 *==========================================================================*/
static BOOL Init_EquipValueSignals(
					IN SITE_INFO		*pSite,
					IN OUT EQUIP_INFO	*pEquip,
					IN const char		*pszSigTypeName,
					IN int				nSig,
					OUT char			**pSigBuf,
					IN int				nSigItemSize,
					IN INIT_A_SIGNAL_PROC pfnInitSignal)
{
	int		n;
	int		nSigBufSize = nSig*nSigItemSize;
	char	*pSig;

	if (nSig == 0)
	{
		// no such signals,return TRUE. it's OK
		return TRUE;
	}
	
	// 1. allocate memory for sampling value signals, NOT the original sig
	*pSigBuf = NEW(char, nSigBufSize);
	if (*pSigBuf == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Out of memory on allocating memory %s values of "
			" equipment \"%s\"(%d).\n",
			pszSigTypeName,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}

	ZERO_POBJS(*pSigBuf, nSigBufSize); // clear to 0

	// init each signal
	pSig = *pSigBuf;
	for (n = 0; n < nSig; n++ )
	{
		// set the default state of the signal is configured.
		SIG_STATE_SET(pSig, VALUE_STATE_IS_CONFIGURED);
	
		pSig += nSigItemSize;

		// to init each signal by its init-callback function.
		if (!pfnInitSignal(pSite, pEquip, n))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on initializing the #%d %s "
				"of equipment \"%s\"(%d)\n",
				n, pszSigTypeName,
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_EquipSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO       *pSite  : 
 *            IN OUT EQUIP_INFO  *pEquip : 
 * RETURN   : static const char *: The signal name of failure to init
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-28 18:48
 *==========================================================================*/
static const char *Init_EquipSignals(IN SITE_INFO *pSite,
							   IN OUT EQUIP_INFO *pEquip)
{
	const char *pszSigTypeName;


	// 1.  to init the sampling signal one by one.
	pszSigTypeName = "sampling signal";
	if (!Init_EquipValueSignals(pSite,
			pEquip, 
			pszSigTypeName,
			pEquip->pStdEquip->iSampleSigNum,
			(char **)&pEquip->pSampleSigValue,
			sizeof(pEquip->pSampleSigValue[0]),
			Init_SamplingSig))
	{
		return pszSigTypeName;
	}

	//2. to init the control signals
	pszSigTypeName = "control signal";
	if (!Init_EquipValueSignals(pSite,
			pEquip, 
			pszSigTypeName,
			pEquip->pStdEquip->iCtrlSigNum,
			(char **)&pEquip->pCtrlSigValue,
			sizeof(pEquip->pCtrlSigValue[0]),
			Init_CtrlSig))
	{
		return pszSigTypeName;
	}


	//3. to init the setting signals
	pszSigTypeName = "setting signal";
	if (!Init_EquipValueSignals(pSite,
			pEquip, 
			pszSigTypeName,
			pEquip->pStdEquip->iSetSigNum,
			(char **)&pEquip->pSetSigValue,
			sizeof(pEquip->pSetSigValue[0]),
			Init_SetSig))
	{
		return pszSigTypeName;
	}


	//4. to init the alarm signals
	pszSigTypeName = "alarm signal";
	if (!Init_EquipValueSignals(pSite,
			pEquip, 
			pszSigTypeName,
			pEquip->pStdEquip->iAlarmSigNum,
			(char **)&pEquip->pAlarmSigValue,
			sizeof(pEquip->pAlarmSigValue[0]),
			Init_AlarmSig))
	{
		return pszSigTypeName;
	}

	// OK
	return NULL;
}


/*==========================================================================*
 * FUNCTION : *Init_FindCustomizedChannel
 * PURPOSE  : according to the given sig type and signal id to find the 
 *            customized channel info.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO  *pEquip  : 
 *            IN int         iSigType : 
 *            IN int         iSigID   : 
 * RETURN   : static CUSTOM_CHANNEL : NULL for not customized channel for the
 *                                    signal.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-31 16:38
 *==========================================================================*/
static CUSTOM_CHANNEL *Init_FindCustomizedChannel(IN EQUIP_INFO	*pEquip,
	IN int iSigType, IN int iSigID)
{
	CUSTOM_CHANNEL *pCC = pEquip->pCustomChannelInfo;
	int				nCC;	// num of customized channels of the equipment

	if (pCC == NULL)
	{
		return NULL;
	}

	for( nCC = 0; nCC < pEquip->nCustomChannelInfo; nCC++, pCC++)
	{
		if ((pCC->iEqipID  == pEquip->iEquipID) &&
			(pCC->iSigType == iSigType) &&
			(pCC->iSigID   == iSigID))
		{
			return pCC;
		}
	}

	return NULL;
}





/*==========================================================================*
 * FUNCTION : Init_MapSamplerChannel
 * PURPOSE  : 1) apply the customized channels.
 *            2) or map the logical sample channel to the real channel.
 *            3) add the sig-value addr to related sampler.
 *            
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO    *pSite              : 
 *            IN OUT EQUIP_INFO   *pEquip             : 
 *            IN int              nSigType            : 
 *            IN int              nSigId              : 
 *            IN OUT int          *pSamplerId         : 
 *            IN OUT int          *pSamplingChannel   : 
 *            IN OUT int          *pCtrlChannel       : 
 *            IN SIG_BASIC_VALUE  *pChannelRelatedSig : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-29 17:03
 *==========================================================================*/
static BOOL Init_MapSamplerChannel(IN OUT SITE_INFO		*pSite,
								   IN OUT EQUIP_INFO	*pEquip,
								   IN int				nSigType,
								   IN int				nSigId,
								   IN OUT int			*pSamplerId,
								   IN OUT int			*pSamplingChannel,
								   IN OUT int			*pCtrlChannel,
								   IN SIG_BASIC_VALUE	*pChannelRelatedSig)
{
	CUSTOM_CHANNEL *pCC;


	// 1. Get the customized channel from the customized channel table
	pCC = Init_FindCustomizedChannel(pEquip, nSigType, nSigId);
	
	if (pCC != NULL)	// this signal channel is custmoized.
	{
		*pSamplerId = pCC->iSamplerID;

		// copy the customized sampling channel
		if (pSamplingChannel != NULL)
		{
			*pSamplingChannel = pCC->iSamplerChannel;

			if (*pSamplingChannel != COMM_STATUS_CHANNEL)
			{
#if (COMM_STATUS_CHANNEL != 0)	// to save the communication status to channel 0
				if (*pSamplingChannel >= 0)
				{
					*pSamplingChannel += 1;	// to keep 0 for comm. status
				}
#endif
			}
			else	// to save the communication status to channel 0
			{
				*pSamplingChannel = 0;	// to keep 0 for comm. status
			}
		}

		// copy the customized control channel
		if (pCtrlChannel != NULL)
		{
			*pCtrlChannel = pCC->iControlChannel;	
		}

#ifdef _DEBUG
		TRACEX("Found customized channel(SamplerID:%d,SID:%d, CID:%d) for the "
			"signal(type:%d,ID:%d) of equipment \"%s\"(%d).\n",
			pCC->iSamplerID, pCC->iSamplerChannel,pCC->iControlChannel,
			nSigType, nSigId,
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);
#endif //_DEBUG
	}

	// this signal channel is not custmoized.
	else if (pEquip->iRelevantSamplerListLength > 0)
	{
        RELEVANT_SAMPLER *pRelevantSampler;

		// get the actual sampler id
		if ((*pSamplerId < 1) ||	// sampler id start from 1
			(*pSamplerId > pEquip->iRelevantSamplerListLength)) // too big
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Mapping sampling channel: Invalid sampler index %d of the "
				"signal(type:%d,ID:%d) of equipment \"%s\"(%d).\n",
				*pSamplerId, nSigType, nSigId,
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// the old sampler id starts from 1, it's a index.
		pRelevantSampler = &pEquip->pRelevantSamplerList[*pSamplerId-1];

		// the actual sampler ID
        *pSamplerId      = pRelevantSampler->iSamplerID;

		// map the sampling channel
		// Actual Sampling Channel = SamplingChannelInStdCfg + 
		//				TheStartSamplingChannelInSolution
		if(pSamplingChannel != NULL) 
		{
			// the communication status channel need not map.
			//sometime *pSamplingChannel equal to -3 for setting signal,Undo!! 20060621lixidong
			if (*pSamplingChannel >= COMM_STATUS_CHANNEL)
			{
				if (*pSamplingChannel != COMM_STATUS_CHANNEL)
				{
					*pSamplingChannel +=
					pRelevantSampler->iStartSamplingChannel;

#if (COMM_STATUS_CHANNEL != 0)	// to save the communication status to channel 0
					*pSamplingChannel += 1;	// to keep 0 for comm. status
#endif
				}
				else	// to save the communication status to channel 0
				{
					*pSamplingChannel = 0;	// to keep 0 for comm. status
				}
			}
		}

		// map the control channel
		// Actual Control Channel = ControlChannelInStdCfg + 
		//				TheStartControlChannelInSolution
		if ((pCtrlChannel != NULL) && (*pCtrlChannel >= 0))
		{
			*pCtrlChannel += pRelevantSampler->iStartControlChannel;	
		}
	}

	else	// None of the relevant sampler is NOT configured.
	{
		*pSamplerId = -1;
	}


	// -1 indicates the samplerID is not cfg'ed, set the sampling signal to not
	// configured and not valid.
	if (*pSamplerId == -1)
	{
		SIG_STATE_CLR(pChannelRelatedSig, VALUE_STATE_IS_CONFIGURED);
		SIG_STATE_CLR(pChannelRelatedSig, VALUE_STATE_IS_VALID);
	}

	// save the data value ptr of this signal to the channel-signal
	// mapping table of the relevant sampler.
	else if ((pSamplingChannel != NULL) && (*pSamplingChannel >= 0)) 
	{
		SAMPLER_INFO	*pSampler;

		pSampler = Init_GetSamplerRefById(pSite, *pSamplerId);
		if (pSampler == NULL)
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Mapping sampling channel: Invalid sampler ID %d of the "
				"signal(type:%d,ID:%d) of equipment \"%s\"(%d).\n",
				*pSamplerId, nSigType, nSigId,
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// due to there may be two or more signal use one sampler channel,
		// change the map rule of signal value with sampling channel from 1 by 1
		// to n by 1. maofuhua, 2005-1-6
		// enlarge the sigval buffer directly, donot care the sampling channel.

//updated for memory opt
#ifdef RENEW_OPT_IMP
#define BATCH_BASE    1024 
		if (pSampler->nSigValues % BATCH_BASE == 0) 
		{
			pSampler->ppSigValues = RENEW( SIG_BASIC_VALUE *,
			pSampler->ppSigValues,
			(BATCH_BASE)*((pSampler->nSigValues / (BATCH_BASE)) + 1));
		}
		
#else
		pSampler->ppSigValues = RENEW( SIG_BASIC_VALUE *,
			pSampler->ppSigValues,
			pSampler->nSigValues + 1);
#endif

		if (pSampler->ppSigValues == NULL)
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on mapping sampler channel of the "
				"signal(type:%d,ID:%d) of equipment \"%s\"(%d).\n",
				nSigType, nSigId,
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// append the new signal value to the enlarged buffer.
		pSampler->ppSigValues[pSampler->nSigValues] = pChannelRelatedSig;
		pSampler->nSigValues++;	// increase the total count.

		// count the maximum used sampling channel number.
		if (pSampler->nMaxUsedChannel <= *pSamplingChannel)
		{
			pSampler->nMaxUsedChannel = (*pSamplingChannel) + 1;
		}

		// for customized channel.
		// add the equipment to the pSampler->pRelatedEquipment list if the
		// equipment is not in pSampler->pRelatedEquipment.
		//
		if ((pCC != NULL) &&	// this signal channel is custmoized.
			(FindIntItemIndex((int)pEquip, (int *)pSampler->ppRelatedEquipment,
			pSampler->nRelatedEquipment) < 0) &&	// not in the list
			(AppendArrayItem((void **)&pSampler->ppRelatedEquipment,			
			sizeof(pSampler->ppRelatedEquipment[0]), 			
			&pSampler->nRelatedEquipment,
			&pEquip ) == NULL))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on adding equipment \"%s\"(%d) to the reference "
				"table of the sampler \"%s\"(%d).\n",
				EQP_GET_EQUIP_NAME0(pEquip),     pEquip->iEquipID,
				EQP_GET_SAMPLER_NAME0(pSampler), pSampler->iSamplerID);

			return FALSE;
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_SamplingSig
 * PURPOSE  : Init a sampling signal
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO   *pSite    : 
 *            IN OUT EQUIP_INFO  *pEquip   : 
 *            IN int             nSigIndex : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-29 14:18
 *==========================================================================*/
static BOOL Init_SamplingSig(IN OUT  SITE_INFO *pSite,
							 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex)
{
	SAMPLE_SIG_VALUE *pSig    = &pEquip->pSampleSigValue[nSigIndex];
	SAMPLE_SIG_INFO  *pStdSig = &pEquip->pStdEquip->pSampleSigInfo[nSigIndex];

	pSig->bv.ucSigType = SIG_TYPE_SAMPLING;
	pSig->pStdSig = pStdSig; // to refer the standard signal info
	pSig->bv.ucType = (BYTE)pStdSig->iSigValueType;
	
	// 1. To map the sampling and control channel number
	// 1.1 Copy channel info from the standard signal
	pSig->sv.iSamplerID      = pStdSig->iIndexofSamplerList;// sampler index.
	pSig->sv.iSamplerChannel = pStdSig->iSamplerChannel;	// channel

	// 1.2 To map the cfg channel to actual channel, 
	//     except the virtual signal.
	if ((pSig->sv.iSamplerChannel >= 0) || 
		(pSig->sv.iSamplerChannel == COMM_STATUS_CHANNEL))
	{
		if (!Init_MapSamplerChannel(
			pSite, pEquip,			// site and equip the channel belongs to
			SIG_TYPE_SAMPLING, pStdSig->iSigID,
			&pSig->sv.iSamplerID,	// will return the actual sampler ID			
			&pSig->sv.iSamplerChannel, // will return the actual channel num
			NULL, 					// No control channel			
			&pSig->bv)) 			// The addr of basic signal value
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on mapping sampler channel of the sampling signal %s "
				"of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;	// 
		}
	}
	

	//
	// Allow a real sampling signal, which sampling channel > 0, has an exp.
	// in this case, the exp will be used to convert sampled raw data to real
	// signal data. 
	// such as SM-IO which DI returns an analog value (<4.0 indicates DI is 0,
	// >4.0 is 1) instead of 0/1. so, this exp will convert the raw to 0/1.
	// maofuhua, 2005-3-15
	// convert all eval exp.
#ifndef	REAL_SAMPLING_SIG_SUPPORT_EXP
	else // if virtual signal, duplicate the expression from the std signal
#endif
	{
		if (!Init_DupExp(&pSig->sv.pCalculateExpression,
			&pSig->sv.iCalculateExpression,
			pStdSig->pCalculateExpression,
			pStdSig->iCalculateExpression))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on duplicating evaluation expression "
				"of sampling signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// because othe signals may not be initialized, the map work can NOT
		// be done at now, it will be done later. 
	}

	// create the statistics data buffer
	// NOTE: the stat data only is valid in float type signal
	if ((pSig->bv.ucType == VAR_FLOAT) &&
		((pSig->pStdSig->iStoringInterval != 0) ||
		FLOAT_NOT_EQUAL0(pSig->pStdSig->fStoringThreshold)))
	{
		pSig->sv.pStatValue = NEW(SIG_STAT_VALUE,1);
		if (pSig->sv.pStatValue == NULL)
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on getting statistics data buffer for "
				"sampling signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;		
		}

		// init the max and min value
		memset(pSig->sv.pStatValue, 0, sizeof(SIG_STAT_VALUE));
		pSig->sv.pStatValue->varMaxValue.fValue = MINFLOAT;
		pSig->sv.pStatValue->varMinValue.fValue = MAXFLOAT;
	}

	// if having display expression, duplicate the exp from the std signal
	if (pStdSig->pDispExp)
	{
		//TRACE("***Std sig: %s, pDispExp = %d***\n", pStdSig->pSigName->pAbbrName[1], pStdSig->pDispExp);
		// Display exp
		if (!Init_DupExp(&pSig->sv.pDispExp,
			&pSig->sv.iDispExp,
			pStdSig->pDispExp,
			pStdSig->iDispExp))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on duplicating display expression "
				"of control signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// because other signals may not be initialized, the map work can NOT
		// be done at now, it will be done later. 
	}

	SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);

	if(pStdSig->iSigID == 100)// Work status
	{
		pSig->bv.varValue.ulValue = 1;//Set it default to not exist.
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_CtrlSig
 * PURPOSE  : init a control signal
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO    *pSite    : 
 *            IN OUT EQUIP_INFO  *pEquip   : 
 *            IN int              nSigIndex : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-29 16:15
 *==========================================================================*/
static BOOL Init_CtrlSig(IN OUT SITE_INFO *pSite, 
						 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex)
{
	CTRL_SIG_VALUE *pSig    = &pEquip->pCtrlSigValue[nSigIndex];
	CTRL_SIG_INFO  *pStdSig = &pEquip->pStdEquip->pCtrlSigInfo[nSigIndex];
	
	pSig->bv.ucSigType = SIG_TYPE_CONTROL;
	pSig->pStdSig = pStdSig; // to refer the standard signal info
	pSig->bv.ucType = (BYTE)pStdSig->iSigValueType;

	
	// 1. To map the sampling and control channel number
	// 1.1 Copy channel info from the standard signal
	pSig->sv.iSamplerID      = pStdSig->iIndexofSamplerList;// sampler index.
	pSig->sv.iSamplerChannel = pStdSig->iSamplerChannel;	// sampling channel
	pSig->sv.iControlChannel = pStdSig->iControlChannel;	// control channel

	// 1.2 To map the cfg channel to actual channel, 
	//     except the virtual signal.
	if (pSig->sv.iControlChannel >= 0)
	{
		if (!Init_MapSamplerChannel(
			pSite, pEquip,			// site and equip the channel belongs to
			SIG_TYPE_CONTROL, pStdSig->iSigID,
			&pSig->sv.iSamplerID,	// will return the actual sampler ID			
			((pSig->sv.iSamplerChannel == VIRTUAL_INTERNAL_CHANNEL) ? NULL 
				: &pSig->sv.iSamplerChannel),// sample channel
			&pSig->sv.iControlChannel,// control channel			
			&pSig->bv)) 			// The addr of basic signal value
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on mapping sampler channel of the control signal %s "
				"of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;	// 
		}
	}

	// if having controllable expression, duplicate the exp from the std signal
	if (pStdSig->pControllableExpression != NULL)
	{
		if (!Init_DupExp(&pSig->sv.pControllableExpression,
			&pSig->sv.iControllableExpression,
			pStdSig->pControllableExpression,
			pStdSig->iControllableExpression))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on duplicating controllable expression "
				"of control signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// because other signals may not be initialized, the map work can NOT
		// be done at now, it will be done later. 
	}
	
	// if having display expression, duplicate the exp from the std signal
	if (pStdSig->pDispExp)
	{
		// display exp
		if (!Init_DupExp(&pSig->sv.pDispExp,
			&pSig->sv.iDispExp,
			pStdSig->pDispExp,
			pStdSig->iDispExp))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on duplicating display expression "
				"of control signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// because other signals may not be initialized, the map work can NOT
		// be done at now, it will be done later. 
	}

	// get the default value from std sig, maofuhua, 2005-3-24 added.
	pSig->bv.varValue = pStdSig->defaultValue;
	SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);
	SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);
	SIG_STATE_SET(pSig, VALUE_STATE_IS_CONTROLLABLE);
		
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_SetSig
 * PURPOSE  : init a setting signal
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO    *pSite    : 
 *            IN OUT EQUIP_INFO  *pEquip   : 
 *            IN int              nSigIndex : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-29 16:15
 *==========================================================================*/
static BOOL Init_SetSig(IN OUT SITE_INFO *pSite, 
						 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex)
{
	SET_SIG_VALUE *pSig    = &pEquip->pSetSigValue[nSigIndex];
	SET_SIG_INFO  *pStdSig = &pEquip->pStdEquip->pSetSigInfo[nSigIndex];
	
	pSig->bv.ucSigType = SIG_TYPE_SETTING;
	pSig->pStdSig = pStdSig; // to refer the standard signal info
	pSig->bv.ucType = (BYTE)pStdSig->iSigValueType;
	
	// 1. To map the sampling and control channel number
	// 1.1 Copy channel info from the standard signal
	pSig->sv.iSamplerID      = pStdSig->iIndexofSamplerList;// sampler index.
	pSig->sv.iSamplerChannel = pStdSig->iSamplerChannel;	// sampling channel
	pSig->sv.iControlChannel = pStdSig->iControlChannel;	// control channel

	// 1.2 To map the cfg channel to actual channel, 
	//     except the virtual signal.
	if ((pSig->sv.iSamplerChannel >= 0) || 
		(pSig->sv.iSamplerChannel == COMM_STATUS_CHANNEL) ||
		(pSig->sv.iControlChannel >= 0))  //for setting signal must be added for -3signal20060621
	{
		if (!Init_MapSamplerChannel(
			pSite, pEquip,			// site and equip the channel belongs to
			SIG_TYPE_SETTING, pStdSig->iSigID,
			&pSig->sv.iSamplerID,	// will return the actual sampler ID			
			&pSig->sv.iSamplerChannel,// sample channel
			((pSig->sv.iControlChannel < 0) ? NULL 
				: &pSig->sv.iControlChannel), //  control channel			
			&pSig->bv)) 			// The addr of basic signal value
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on mapping sampler channel of the setting signal %s "
				"of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;	// 
		}
	}
	

	// if having settable expression, duplicate the exp from the std signal
	if (pStdSig->pSettableExpression != NULL)
	{
		// settable exp
		if (!Init_DupExp(&pSig->sv.pSettableExpression,
			&pSig->sv.iSettableExpression,
			pStdSig->pSettableExpression,
			pStdSig->iSettableExpression))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on duplicating settable expression "
				"of control signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// because othe signals may not be initialized, the map work can NOT
		// be done at now, it will be done later. 
	}


	// if having display expression, duplicate the exp from the std signal
	if (pStdSig->pDispExp)
	{
		// display exp
		if (!Init_DupExp(&pSig->sv.pDispExp,
			&pSig->sv.iDispExp,
			pStdSig->pDispExp,
			pStdSig->iDispExp))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on duplicating display expression "
				"of control signal %s of equipment \"%s\"(%d).\n",
				EQP_GET_SIG_NAME0(pStdSig),
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// because other signals may not be initialized, the map work can NOT
		// be done at now, it will be done later. 
	}


	// set the default value.
	pSig->bv.varValue = pStdSig->defaultValue;
    
	// if it is a virtual setting signal, set it to valid. maofuhua, 04-11-29
	// change: the init state of all types of setting/ctrl signal is valid
	// set all signal to valid and controllable by default
	SIG_STATE_SET(pSig, VALUE_STATE_NEED_DISPLAY);
	SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);
	SIG_STATE_SET(pSig, VALUE_STATE_IS_SETTABLE);

	// count the count of the persistent signals
	if (pSig->pStdSig->bPersistentFlag)
	{
		pSite->nPersistentSig++;
	}


	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_AlarmSig
 * PURPOSE  : init an alarm signal
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO    *pSite    : 
 *            IN OUT EQUIP_INFO  *pEquip   : 
 *            IN int              nSigIndex : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-29 16:16
 *==========================================================================*/
static BOOL Init_AlarmSig(IN OUT SITE_INFO *pSite, 
						 IN OUT EQUIP_INFO *pEquip, IN int nSigIndex)
{
	ALARM_SIG_VALUE *pSig    = &pEquip->pAlarmSigValue[nSigIndex];
	ALARM_SIG_INFO  *pStdSig = &pEquip->pStdEquip->pAlarmSigInfo[nSigIndex];

	UNUSED(pSite);

	pSig->bv.ucSigType = SIG_TYPE_ALARM;
	pSig->pStdSig = pStdSig; // to refer the standard signal info
	pSig->bv.ucType = (BYTE)VAR_ENUM;
	
	// 1. set the start time and end time with -1, 
	// -1 indicates the alarm does not start
	pSig->sv.tmStartTime = INVALID_TIME;	// -1
	pSig->sv.tmEndTime   = INVALID_TIME;	// -1
	pSig->pEquipInfo      = pEquip;

	// the alarm must have a alarm expression.
	if (pStdSig->pAlarmExpression == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_WARNING, 
			"WARNING, the alarm signal %s of equipment \"%s\"(%d) has "
			"no alarm calc expression.",
			EQP_GET_SIG_NAME0(pStdSig),
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		// OK. The alarm will never be generated.
	}

	// 2. to copy the alarm calc exp.
	else if (!Init_DupExp(&pSig->sv.pAlarmExpression,
		&pSig->sv.iAlarmExpression,
		pStdSig->pAlarmExpression,
		pStdSig->iAlarmExpression))
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Out of memory on duplicating calc expression "
			"of alarm signal %s of equipment \"%s\"(%d).\n",
			EQP_GET_SIG_NAME0(pStdSig),
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}


	// 3. to copy the alarm suppression exp
	else if (!Init_DupExp(&pSig->sv.pSuppressExpression,
		&pSig->sv.iSuppressExpression,
		pStdSig->pSuppressExpression,
		pStdSig->iSuppressExpression))
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Out of memory on duplicating suppression expression "
			"of alarm signal %s of equipment \"%s\"(%d).\n",
			EQP_GET_SIG_NAME0(pStdSig),
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

		return FALSE;
	}


	// the init state of alarm is valid. maofuhua, 2004-12-04
	SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);

	// because othe signals may not be initialized, the map work can NOT
	// be done at now, it will be done later. 

	return TRUE;

}


/*==========================================================================*
 * FUNCTION : Init_DupExp
 * PURPOSE  : duplicate an expression.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT EXP_ELEMENT **ppEleDst : the dest ptr to save allocated exp
 *			  OUT int		  *pnEle:   the save the exp ele num
 *		      IN EXP_ELEMENT  *pEleSrc : the src
 *            IN int            nEle  : 
 * RETURN   :  BOOL: TRUE for OK, else FALSE.
 * COMMENTS : if the pEleSrc is NULL, will return TRUE.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-28 19:19
 *==========================================================================*/
BOOL Init_DupExp(OUT EXP_ELEMENT **ppEleDst,
						OUT int *pnEle,
						IN EXP_ELEMENT *pEleSrc, IN int nEle)
{
	if ((pEleSrc == NULL) || (nEle == 0))
	{
		*ppEleDst = NULL;
		*pnEle    = 0;
		return TRUE;	// no exp need to copy, it's true!
	}

	*ppEleDst = NEW(EXP_ELEMENT, nEle);
	if (*ppEleDst == NULL)
	{
		*pnEle    = 0;
		return FALSE;	// out of memory
	}

	memmove(*ppEleDst, pEleSrc, nEle*sizeof(EXP_ELEMENT));
	*pnEle = nEle;

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_AddEquipToRelevantSampler
 * PURPOSE  : Add the equipment to the referred equipment list of its 
 *            relevant samplers
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite  : 
 *            IN EQUIP_INFO     *pEquip : 
 * RETURN   : static BOOL : TRUE for ok.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-28 19:41
 *==========================================================================*/
static BOOL Init_AddEquipToRelevantSampler(IN OUT SITE_INFO *pSite,
									IN EQUIP_INFO *pEquip)
{
	int				n;
	SAMPLER_INFO	*pSampler;
	
	for (n = 0; n < pEquip->iRelevantSamplerListLength; n++)
	{
		// -1 indicates the samplerID is not cfg'ed, continue do other.
		if (pEquip->pRelevantSamplerList[n].iSamplerID == -1)
		{
			continue;
		}

		pSampler = Init_GetSamplerRefById(pSite,
			pEquip->pRelevantSamplerList[n].iSamplerID);
		if (pSampler == NULL)
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Invalid relevant sampler ID %d of equipment \"%s\"(%d)\n",
				pEquip->pRelevantSamplerList[n].iSamplerID,
				EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID);

			return FALSE;
		}

		// add to the ref table	
		if (AppendArrayItem((void **)&pSampler->ppRelatedEquipment,			
			sizeof(pSampler->ppRelatedEquipment[0]), 			
			&pSampler->nRelatedEquipment,
			&pEquip ) == NULL)
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on adding equipment \"%s\"(%d) to the reference "
				"table of the sampler \"%s\"(%d).\n",
				EQP_GET_EQUIP_NAME0(pEquip),	 pEquip->iEquipID,
				EQP_GET_SAMPLER_NAME0(pSampler), pSampler->iSamplerID);

			return FALSE;
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_MapExpVarA
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO            *pSite   : 
 *            IN EQUIP_INFO           *pEquip  : 
 *            IN EXP_VARIABLE         *pExpVar : 
 *		      OUT EQUIP_INFO **ppSigEquip:     the equip owner this mapped sig
 *            IN OUT SIG_BASIC_VALUE  **ppSig  : 
 * RETURN   : static int : 1: for the var is successfully mapped.
 *                         0: the sig is not configured.
 *                        -1: the sig var is not found.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-26 16:59
 *==========================================================================*/
static int Init_MapExpVarA(IN SITE_INFO *pSite, IN EQUIP_INFO *pEquip,
						   IN EXP_VARIABLE	*pExpVar, 
						   OUT EQUIP_INFO **ppSigEquip,
						   IN OUT SIG_BASIC_VALUE **ppSig)
{
	int				nSigOwnerEquipIndex;
	EQUIP_INFO		*pOwnerEquip;

	nSigOwnerEquipIndex = pExpVar->iRelevantEquipIndex;
	if (nSigOwnerEquipIndex == 0) // the equipment itself
	{
		pOwnerEquip = pEquip;
	}
	else if ((nSigOwnerEquipIndex >= 1) &&
		(nSigOwnerEquipIndex <= pEquip->iRelevantEquipListLength))
	{
		// other referred eqp idx start from 1
		nSigOwnerEquipIndex = // the actual equipment ID.
			pEquip->pRelevantEquipList[nSigOwnerEquipIndex-1];

		if (nSigOwnerEquipIndex == -1)
		{
			// the actual equipment is not configured.
			// Set the not configured flag.
			*ppSig = NULL;
			return 0;	// the signal is NOT configured.
		}

		pOwnerEquip = Init_GetEquipRefById(pSite, nSigOwnerEquipIndex);
	}
	else
	{
		pOwnerEquip = NULL;
	}

	*ppSig = (pOwnerEquip != NULL) 
		? Init_GetEquipSigRefById(pOwnerEquip, 
			pExpVar->iSigType, pExpVar->iSigID)	
		: NULL;

	if (ppSigEquip != NULL)
	{
		*ppSigEquip = pOwnerEquip;
	}

	return (*ppSig != NULL) ? 1 : -1;
}


/*==========================================================================*
 * FUNCTION : Init_MapExpressionVar
 * PURPOSE  : map varible from V(EquipID, SigType, SigID) to the actual signal
 *            address, in order to accelerate the running speed in searching var.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO          *pSite        : 
 *            IN EQUIP_INFO         *pEquip       : 
 *            IN OUT EXP_ELEMENT    *pExp         : 
 *            IN int                iExp          : 
 *            IN INIT_CHK_VAR_PROC  pfnVarChecker : 
 *            IN void               *pChkParam    : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-10-31 20:51
 *==========================================================================*/
static BOOL Init_MapExpressionVar(IN SITE_INFO			*pSite,
								  IN EQUIP_INFO			*pEquip,
								  IN OUT EXP_ELEMENT	*pExp, 
								  IN int				iExp,
								  IN INIT_CHK_VAR_PROC	pfnVarChecker,
								  IN void				*pChkParam )
{
	int				n;
	EXP_ELEMENT		*pVar = pExp;
	EXP_VARIABLE	*pExpVar;
	SIG_BASIC_VALUE *pSig;

	EXP_CONST		fResult;
	EXP_ELEMENT		result = {EXP_ETYPE_CONST, &fResult};

	char			szBuf[512];
	
	if (pExp == NULL)		// not need to map var
	{
		return TRUE;
	}

#ifdef _DEBUG_EQUIP_INIT
	TRACE("Before mapping:");
	Exp_DisplayExpression(pExp, iExp, FALSE);
#endif

	// convert the exp to string at first. the string is used on error log.
	Exp_StringExpression(pExp, iExp, FALSE, szBuf, sizeof(szBuf));

	for (n = 0; n < iExp; n++, pVar++)
	{
		// map var to addr
		// added supporting sampling channel data by {EquiID, SigType, SigID}
		// in the evaluation exp in the config file. 2005-3-26
		if (EXP_IS_VAR(pVar))
		{
			char	*pErrMsg = NULL;

			pExpVar = (EXP_VARIABLE *)pVar->pElement;

			if (Init_MapExpVarA(pSite, pEquip, pExpVar, NULL, &pSig) == 0)
			{
				// the actual equipment is not configured.
				// Set the not configured flag.
				pVar->pElement = VAR_NOT_CONFIGURED;

				// the left vars need not mapped, this exp will be deleted in 
				// the function Init_ChkExpressVarConfigState.
				// return TRUE to make the init to continue.
				return TRUE;
			}				


			// check the mapped sig.
			if (pSig == NULL)
			{
				pErrMsg = "No such"; // no such signal error 
			}

			// check it
			else if (!pfnVarChecker(pExpVar->iSigType, pSig, pChkParam, pVar))
			{
				pErrMsg = "Invalid";	// check fail, invalid signal
			}

			// if the var is a raw var, it can NOT be an alarm, must be the 
			// sampling/ctrl/setting signal and which sampling channle >= 0.
			else if ((pVar->byElementType == EXP_ETYPE_VARIABLE_RAW) && 
				((pSig->ucSigType == SIG_TYPE_ALARM) ||
				((SAMPLE_SIG_VALUE *)pSig)->sv.iSamplerChannel < 0))
			{
				pErrMsg = "Not a raw"; // not a raw sig.
			}

			// OK. map signal.
			if (pErrMsg == NULL)	// error
			{
				pVar->pElement = pSig;	// OK, map it.
			}
			else	// erorr
			{
				// if there is an invalid variable, log warning msg and 
				// set this exp variable to EXP_ETYPE_INVALID.
				// I think, it's possible that some equipment may be not
				// always configured in engineer field.
				AppLogOut(EQP_INIT, 
					EXP_SIG_MUST_EXIST ? APP_LOG_ERROR : APP_LOG_WARNING, 
					"%s sig[%d,%d,%d] in exp of equip \"%s\"(%d)."
					"The exp is: %s.\n",
					pErrMsg,
					pExpVar->iRelevantEquipIndex, 
					pExpVar->iSigType, pExpVar->iSigID,
					EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
					szBuf);

#if !EXP_SIG_MUST_EXIST
				// Treat it as invalid, continue. maofuhua, 2004-11-15.
				pVar->byElementType = EXP_ETYPE_INVALID;
#else
				return FALSE;
#endif
			}
		}
	}

#ifdef _DEBUG_EQUIP_INIT
	TRACE("After mapping:");
	Exp_DisplayExpression(pExp, iExp, TRUE);
#endif

	// to test expression.
	if (!Exp_Calculate(EQP_GET_EQUIP_NAME0(pEquip),
		pExp, iExp, &result))
	{
		//Fails on trying to calculate the expression
		AppLogOut(EQP_INIT, 
			EXP_SIG_MUST_EXIST ? APP_LOG_ERROR : APP_LOG_WARNING, 
			"Fails on testing the following mapped exp of "
			"equip \"%s\"(%d).\n",
			EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
			pExp, szBuf);
		AppLogOut(EQP_INIT, 
			EXP_SIG_MUST_EXIST ? APP_LOG_ERROR : APP_LOG_WARNING, 
			"exp(%p): %s.\n",
			pExp, szBuf);

#if !EXP_SIG_MUST_EXIST
		// Treat it as error, continue. maofuhua, 2004-12-01.
		// the exp will not be calculated in data processing.
		pExp->byElementType = EXP_ETYPE_ERROR;
#else
		return FALSE;
#endif
	}

	return TRUE;
}



STDPORT_INFO *Init_GetStdPortByTypeId(IN SITE_INFO *pSite, 
									  IN int nStdPortType);
static PORT_INFO *Init_GetPortByTypeId(IN SITE_INFO *pSite, 
									  IN int nPortID);

/*==========================================================================*
 * FUNCTION : Init_SamplerA
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite    : 
 *            IN OUT SAMPLER_INFO   *pSampler : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 16:04
 *==========================================================================*/
static BOOL Init_SamplerA (IN SITE_INFO *pSite,
						   IN OUT SAMPLER_INFO *pSampler)
{
	// despite of the sampler is used or not, init its std sampler that will be
	// used by the application services. 2005-1-6
	if(pSampler == NULL)
	{
		return FALSE;
	}
	// 1. to find the std sampler of this sampler
	pSampler->pStdSampler = Init_GetStdSamplerRefById(pSite, 
		pSampler->iStdSamplerID);
	if (pSampler->pStdSampler == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on finding std sampler ID %d of the sampler \"%s\"(%d).\n",
			pSampler->iStdSamplerID,
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID);

		return FALSE;
	}

	// 2. check there is any related equipment or not
	if (pSampler->nRelatedEquipment == 0)
	{
		AppLogOut(EQP_INIT, APP_LOG_INFO, 
			"There is no any equipment related to the sampler \"%s\"(%d).\n",
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID);

		return TRUE;	// that's OK
	}

	// find the std sampler, increase the ref num of the std sampler
	pSampler->pStdSampler->iRef++;

	// 3. To find and make reference with the sampling port.
	pSampler->pPollingPort = Init_GetPortByTypeId(pSite, 
		pSampler->iPollingPortID);
	if (pSampler->pPollingPort == NULL)
	{
		// not found the port error
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"No such port ID %d of sampler \"%s\"(%d).\n",
			pSampler->iPollingPortID,
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID);

		return FALSE;
	}

	// add the sampler to the ref table of the port
	if (AppendArrayItem((void **)&pSampler->pPollingPort->ppAttachedSamplers,
		sizeof(pSampler->pPollingPort->ppAttachedSamplers[0]),
		&pSampler->pPollingPort->nAttachedSamplers,
		&pSampler) == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Out of memory on add sampler \"%s\"(%d) to reference "
			"table of port \"%s\"(%d).\n",
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID,
			EQP_GET_PORT_NAME(pSampler->pPollingPort),
			pSampler->pPollingPort->iPortID);

		return FALSE;
	}

	// to make the sampling data buffer. 
	// pSampler->nMaxUsedChannel is the maximum used sampling channel+1
	pSampler->pChannelData = NEW(float, pSampler->nMaxUsedChannel);
	if (pSampler->pChannelData == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Out of memory on allocating the buffer(chn=%d) to read data for "
			"sampler \"%s\"(%d).\n",
			pSampler->nMaxUsedChannel,
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID);

		return FALSE;
	}

	ZERO_POBJS(pSampler->pChannelData, pSampler->nMaxUsedChannel);

	// use the stub signal to fill the non-used sampling channel in ppSigValues
#ifdef _DEBUG_EQUIP_INIT
	TRACEX("Max channel is %d in sampler %s(%d).\n",
		pSampler->nMaxUsedChannel,
		EQP_GET_SAMPLER_NAME0(pSampler),
		pSampler->iSamplerID);
#endif //_DEBUG_EQUIP_INIT

	// OK
	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Init_Samplers
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 15:55
 *==========================================================================*/
static BOOL Init_Samplers(IN OUT SITE_INFO *pSite)
{
	int			 n;
	SAMPLER_INFO *pSampler = pSite->pSamplerInfo;

	for (n = 0; n < pSite->iSamplerNum; n++, pSampler++)
	{		
#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"Sampler(%d-%d) \"%s\"(%d) is being initialized...\n",
			pSite->iSamplerNum, n+1,
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID);
#endif //_LOG_DETAIL_INFO

		//Don't forget set init value ,or dead
		if(pSampler)   //in Sample_NeedQuery has ever used it before init it
		{
			pSampler->bJustControlled = FALSE; //It is used control20060616
		}

		if (!Init_SamplerA(pSite, pSampler))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on init sampler \"%s\"(%d).\n",
				EQP_GET_SAMPLER_NAME0(pSampler),
				pSampler->iSamplerID);

			return FALSE;
		}

#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"Sampler(%d-%d) \"%s\"(%d) is initialized OK.\n",
			pSite->iSamplerNum, (n+1), 
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID);
#endif //_LOG_DETAIL_INFO
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_StdSamplerA
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO        *pSite       : 
 *            IN OUT STDSAMPLER_INFO  *pStdSampler : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 16:43
 *==========================================================================*/
static BOOL Init_StdSamplerA(IN SITE_INFO *pSite, 
							 IN OUT STDSAMPLER_INFO *pStdSampler)
{
	const char *pszSymName[] =
	{
		//SAMPLER_WRITE_PROC_NAME, //Write
		//SAMPLER_READ_PROC_NAME,	//"Read"
		SAMPLER_QUERY_PROC_NAME,//"Query"
		SAMPLER_CONTROL_PROC_NAME//	"Control"
	};

	HANDLE	*pfnProc[] =
	{
		//(HANDLE *)&pStdSampler->pfnWrite,
		//(HANDLE *)&pStdSampler->pfnRead,
		(HANDLE *)&pStdSampler->pfnQuery,
		(HANDLE *)&pStdSampler->pfnControl
	};


	UNUSED(pSite);

	pStdSampler->hSamplerLib = LoadDynamicLibrary(
		pStdSampler->szStdSamplerDriver,
		ITEM_OF(pszSymName),
		pszSymName,
		pfnProc, 
		TRUE);

	if (pStdSampler->hSamplerLib == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on loading std sampler \"%s\"(%d) lib %s.\n",
			EQP_GET_STD_SAMPLER_NAME(pStdSampler),
			pStdSampler->iStdSamplerID,
			pStdSampler->szStdSamplerDriver);

		return FALSE;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_StdSamplers
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-01 16:43
 *==========================================================================*/
static BOOL Init_StdSamplers(IN OUT SITE_INFO *pSite)
{
	int				n;
	STDSAMPLER_INFO *pStdSampler = pSite->pStdSamplerInfo;

	for (n = 0; n < pSite->iStdSamplerNum; n++, pStdSampler++)
	{
		// no refered std sampler will not be init.
		if (pStdSampler->iRef == 0)	
		{
			continue;
		}

		if (!Init_StdSamplerA(pSite, pStdSampler))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on init standard sampler \"%s\"(%d).\n",
				EQP_GET_STD_SAMPLER_NAME(pStdSampler),
				pStdSampler->iStdSamplerID);

			return FALSE;
		}

#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"standard sampler(%d-%d) \"%s\"(%d) is initialized OK.\n",
			pSite->iStdSamplerNum, n+1,
			EQP_GET_STD_SAMPLER_NAME(pStdSampler),
			pStdSampler->iStdSamplerID);
#endif //_LOG_DETAIL_INFO
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : *Init_GetPortByTypeId
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite    : 
 *            IN int        nPortID : 
 * RETURN   : PORT_INFO : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-12 17:37
 *==========================================================================*/
static PORT_INFO *Init_GetPortByTypeId(IN SITE_INFO *pSite, 
									  IN int nPortID)
{
	PORT_INFO	*pPort;
	int				n;

	if (nPortID <= 0)
	{
		return NULL;
	}

	// quikly search the ID. The ID start from 1.
	if (nPortID <= pSite->iPortNum)
	{
		pPort = &pSite->pPortInfo[nPortID-1];
		if( pPort->iPortID == nPortID)
		{
			return pPort;
		}
	}

	// the ID is not equal to the INDEX, compare the ID one by one
	pPort = pSite->pPortInfo;
	for (n = 0; n < pSite->iPortNum; n++, pPort++)
	{
		if (pPort->iPortID == nPortID)
		{
			return pPort;
		}
	}
	
	return NULL;	// not found.
}



/*==========================================================================*
 * FUNCTION : Init_SamplingPortA
 * PURPOSE  : init a sampling port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO      *pSite : 
 *            IN OUT PORT_INFO  *pPort : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-02 09:41
 *==========================================================================*/
static BOOL Init_SamplingPortA(IN  SITE_INFO *pSite, IN OUT PORT_INFO *pPort)
{
	SAMPLER_INFO *pSampler, *pChkSampler;
	int			 n, i;

	// get the std port
	pPort->pStdPort = Init_GetStdPortByTypeId(pSite, pPort->iStdPortTypeID);
	if (pPort->pStdPort == NULL)
	{
		AppLogOut( EQP_INIT, APP_LOG_ERROR,
			"Fails on finding the standard port info of type %d "
			"for port \"%s\"(%d)\n",
			pPort->iStdPortTypeID,
			EQP_GET_PORT_NAME(pPort),
			pPort->iPortID);

		return FALSE;
	}

	// 1. to check the samplers num being attached on this port
	if (pPort->nAttachedSamplers == 0)
	{
#ifdef _DEBUG_EQUIP_INIT
		TRACEX("There is no sampler attached under the port \"%s\"(%d)\n",
			EQP_GET_PORT_NAME(pPort),
			pPort->iPortID);
#endif
		return TRUE;
	}

	// 2. check there is any exclusive sampler is attached on the port with
	//    other samplers.
	if (pPort->nAttachedSamplers > 1)
	{
		for (n = 0; n < pPort->nAttachedSamplers; n++)
		{
			pSampler = pPort->ppAttachedSamplers[n];

			// the sampler need a exclusive port, but the attached samplers
			// are more than one. return error.
			if (SAMPLER_ATTR_TEST(pSampler, SAMPLER_EXCLUSIVE_PORT))
			{
				AppLogOut( EQP_INIT, APP_LOG_ERROR,
					"The sampler \"%s\"(%d), attached under the port \"%s\"(%d) "
					"can NOT work with other sampler, need a exclusive port.\n",
					EQP_GET_SAMPLER_NAME0(pSampler),pSampler->iSamplerID,
					EQP_GET_PORT_NAME(pPort),		pPort->iPortID);

				return FALSE;
			}

			// check the sampler's address, this is forbidden if there are two
			// samplers have the same address.
			for (i = n+1; i < pPort->nAttachedSamplers; i++)
			{
				pChkSampler = pPort->ppAttachedSamplers[i];
				if (pChkSampler->iSamplerAddr == pSampler->iSamplerAddr)
				{
					AppLogOut( EQP_INIT, APP_LOG_ERROR,
						"The sampler \"%s\"(%d) and \"%s\"(%d) have the same "
						"address %d attached under the port \"%s\"(%d).\n",
						EQP_GET_SAMPLER_NAME0(pSampler),	pSampler->iSamplerID,
						EQP_GET_SAMPLER_NAME0(pChkSampler),	pChkSampler->iSamplerID,
						EQP_GET_PORT_NAME(pPort),			pPort->iPortID);

					return FALSE;
				}
			}
		}
	}
	
	// 2. To test open the port at once
	// has moved to equip_data_sample.c:Sample_OpenPort(). maofuhua, 04-11-08
	pPort->hPort = NULL;	// not opened.

	// create the command queue.
	pPort->hqControlCommand = Queue_Create(MAX_PENDING_CTRL_CMD,
		sizeof(PEQUIP_CTRL_CMD),	// EQUIP_CTRL_CMD *, not EQUIP_CTRL_CMD.
		0);	// can NOT be auto increased.
	if (pPort->hqControlCommand == NULL)
	{
		AppLogOut( EQP_INIT, APP_LOG_ERROR,
			"Fails on creating control command queue for the port \"%s\"(%d).\n",
			EQP_GET_PORT_NAME(pPort),	pPort->iPortID);

		return FALSE;
	}

	// create the synchronization lock
	pPort->hSyncLock = Mutex_Create(TRUE);
	if (pPort->hSyncLock == NULL)
	{
		AppLogOut( EQP_INIT, APP_LOG_ERROR,
			"Fails on creating synchronization lock for the port \"%s\"(%d).\n",
			EQP_GET_PORT_NAME(pPort),	pPort->iPortID);

		return FALSE;
	}

	return TRUE;	// that's all, ok.
}


/*==========================================================================*
 * FUNCTION : Init_SamplingPorts
 * PURPOSE  : init all sampling ports of a site
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-02 09:41
 *==========================================================================*/
static BOOL Init_SamplingPorts(IN OUT SITE_INFO *pSite)
{
	int			n;
	PORT_INFO	*pPort = pSite->pPortInfo;

	for (n = 0; n < pSite->iPortNum; n++, pPort++)
	{
#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"Port(%d-%d) \"%s\"(%d) is being initialized...\n",
			pSite->iPortNum, (n+1), 
			EQP_GET_PORT_NAME(pPort),	pPort->iPortID);
#endif //_LOG_DETAIL_INFO

		if (!Init_SamplingPortA(pSite, pPort))
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on init sampling port \"%s\"(%d).\n",
				EQP_GET_PORT_NAME(pPort),	pPort->iPortID);

			return FALSE;
		}

#ifdef _LOG_DETAIL_INFO
		AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
			"Port(%d-%d) \"%s\"(%d) is initialized OK.\n",
			pSite->iPortNum, (n+1), 
			EQP_GET_PORT_NAME(pPort),	pPort->iPortID);
#endif //_LOG_DETAIL_INFO
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Init_CtrlCmdArray
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-08 15:12
 *==========================================================================*/
static BOOL Init_CtrlCmdArray(IN OUT SITE_INFO *pSite)
{
	EQUIP_CTRL_CMD_ARRAY *pCmdArray;
	EQUIP_CTRL_CMD	*pCmd;
	int i;

	pCmdArray = NEW(EQUIP_CTRL_CMD_ARRAY, 1);
	if (pCmdArray == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Out of memory on initializing ctrl cmd array.\n");

		return FALSE;
	}

	ZERO_POBJS(pCmdArray, 1);	// reset all to 0

	// init the lock of the array
	pCmdArray->hSyncLock = Mutex_Create(TRUE);
	if (pCmdArray->hSyncLock == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
			"Fails on initializing the mutex of the ctrl cmd array.\n");

		DELETE(pCmdArray);

		return FALSE;
	}

	// set the last empty item
	pCmdArray->pLastEmpty = &pCmdArray->pCmds[0];

	// save the array to site
	pSite->hCmdArray = (HANDLE)pCmdArray;

	// init the semphore of the cmd.
	pCmd = pCmdArray->pCmds;
	for (i = 0; i < MAX_ALLOWED_CTRL_CMDS; i++, pCmd++)
	{
		pCmd->hSyncSem = Sem_Create(0);
		if (pCmd->hSyncSem == NULL)
		{
			AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Out of memory on creating semaphore for #%d ctrl cmd "
				"item of ctrl cmd array.\n",
				i);
			return FALSE;
		}
	}

	return TRUE;
}



#define NUM_REC_PER_READ	32

#ifdef SAVE_ACTIVE_ALARMS

/*==========================================================================*
 * FUNCTION : Init_RestoreActiveAlarmInfo
 * PURPOSE  : call AlarmBegin and AlarmEnd to init the alarm info from 
 *            the stored alarm info
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite           : 
 *            ACT_ALARM_RECORD  *pActiveAlarmRec : 
 *            int               nRec             : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-16 21:53
 *==========================================================================*/
static BOOL Init_RestoreActiveAlarmInfo(IN OUT SITE_INFO *pSite,
										ACT_ALARM_RECORD *pActiveAlarmRec, 
										int nRec)
{
	int					n;
	ACT_ALARM_RECORD	*p = pActiveAlarmRec;
	ALARM_SIG_VALUE		*pSig;
	EQUIP_INFO			*pEquip;

	for (n = 0; n < nRec; n++, p++)
	{		
		pEquip = Init_GetEquipRefById(pSite, p->iEquipID);
		if (pEquip != NULL)
		{
			pSig = Init_GetEquipSigRefById(pEquip, SIG_TYPE_ALARM, p->iAlarmID);

			if ((pSig != NULL)				// valid signal
				&& (pSig->pStdSig->iAlarmLevel != ALARM_LEVEL_NONE) 
				&& ((pSig->next != NULL) 	// the alarming sig, need to end it, or
				|| (p->byAlarmFlag != 0)))	// the sig need restore alarm.
			{
				
			
				pSig->iAlarmLevel = pSig->pStdSig->iAlarmLevel;
				//added by Song Xu 20160912 cause some active alarm which has been suppressed lost after reboot.
				//clear VALUE_STATE_IS_SUPPRESSED before getting every record 
				SIG_STATE_CLR(pSig, VALUE_STATE_IS_SUPPRESSED);

				// set the begin/end/suppression flag.
				SIG_STATE_SET(pSig, p->usStateMask);
				if (SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_END))
				{
				strncpyz(pSig->szEquipSerial, p->szSerial,sizeof(pSig->szEquipSerial));
				pSig->iPosition = p->iPosition;
#ifdef DEBUG_HISTORY_ALARMSERIAL			
				TRACE("Equip_AlarmEnd: p->szSerial = %s  Equip ID = [%d][%d] p->usStateMask = [%d]\n", p->szSerial,p->iEquipID, pSig->pEquipInfo->iEquipID, p->usStateMask);
#endif					    	
					Equip_AlarmEnd(pSite, pEquip, pSig, p->tmStartTime,
						p->byAlarmFlag,		// 1 or 0 
						0);					// do NOT save and report
					
				}
				// the BEGIN test must follow the END test.
				else //do NOT add!// if (SIG_STATE_TEST(pSig, ALARM_STATE_IS_NEW_BEGIN))
				{
				strncpyz(pSig->szEquipSerial, p->szSerial,sizeof(pSig->szEquipSerial));
				pSig->iPosition = p->iPosition;
#ifdef DEBUG_HISTORY_ALARMSERIAL			
				TRACE("Equip_AlarmBegin: p->szSerial = %s  Equip ID =[%d] [%d] p->usStateMask = [%d]\n", p->szSerial,p->iEquipID, pSig->pEquipInfo->iEquipID, p->usStateMask);
#endif					
					Equip_AlarmBegin(pSite, pEquip, pSig, p->tmStartTime,
						0);					// do NOT save and report
				}

				ALARM_STATE_CLR(pSig);	// clear begin/end flag
			}
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_LoadActiveAlarms
 * PURPOSE  : Load the last active alarms from flash storage.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-02 20:48
 *==========================================================================*/
static BOOL Init_LoadActiveAlarms(IN OUT SITE_INFO *pSite)
{
	ACT_ALARM_RECORD	data[NUM_REC_PER_READ];
	HANDLE				hActAlarm;
	int					iStart, nRecRead, nTotalRec = 0;
	HANDLE				hThread = RunThread_GetId(NULL);

	//1.Open device
	hActAlarm = DAT_StorageOpen(ACTIVE_ALARM_LOG);
	if (hActAlarm == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_WARNING,
			"Fails on open active alarm file: %s\n",
			ACTIVE_ALARM_LOG);

		// maybe this is the first time to run the system.
		return TRUE;
	}

	//2.Open device Used Check Alarm Relay Status
	//RECT_OpenSamplingDevice();//Why here???
	// 1. init the alarm values.
	for (iStart = 1; iStart != -2/*read end*/; iStart = 0/*continue*/)
	{
		nRecRead = NUM_REC_PER_READ;
		if (!DAT_StorageReadRecords(hActAlarm, &iStart, &nRecRead, (void *)data, 
			FALSE, TRUE))
		{
			AppLogOut(EQP_INIT, APP_LOG_WARNING,
				"Fails on reading active alarms: startRec=%d, nRec=%d.\n",
				iStart, nRecRead);
			break;	// do NOT return FALSE, due to the file maybe not created.
		}

		if (nRecRead <= 0)	// read end of records.
		{
			AppLogOut(EQP_INIT, APP_LOG_UNUSED,
				"End of reading active alarms: startRec=%d, nRec=%d. "
				"Total %d records.\n",
				iStart, nRecRead, nTotalRec);
			break;
		}

		RunThread_Heartbeat(hThread);	//heartbeat

		Init_RestoreActiveAlarmInfo(pSite, data, nRecRead);
		nTotalRec += nRecRead;
	}

	DAT_StorageClose(hActAlarm);
	
	return TRUE;
}

#endif //ifdef SAVE_ACTIVE_ALARMS


/*==========================================================================*
 * FUNCTION : Init_RestoreSettingValueInfo
 * PURPOSE  : dispatch the set value which is read from flash to the 
 *            corresponding setting signal.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO          *pSite   : 
 *            IN PERSISTENT_SIG_RECORD  *pSetRec : 
 *            IN int                    nRec     : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-18 21:04
 *==========================================================================*/
static 	BOOL Init_RestoreSettingValueInfo(IN OUT SITE_INFO *pSite, 
				IN PERSISTENT_SIG_RECORD *pSetRec, IN int nRec)
{
	int						i;
	PERSISTENT_SIG_RECORD	*p = pSetRec;
	SIG_BASIC_VALUE			*pSig;
	EQUIP_INFO				*pEquip;
	int						nSigType, nSigID;
	float			fMin, fMax, fVal;

	for (i = 0; i < nRec; i++, p++)
	{
		pEquip = Init_GetEquipRefById(pSite, p->iEquipID);

		if (pEquip != NULL)
		{
			DXI_SPLIT_SIG_ID(p->iSignalID, nSigType, nSigID);
			
			pSig = Init_GetEquipSigRefById(pEquip, nSigType, nSigID);
			if (pSig != NULL)
			{
#ifdef _DEBUG_EQUIP_INIT
				TRACEX("[SET_SIG] Restoring setting signal %s(%d) value to %1.3f...\n",
					EQP_GET_SIGV_NAME0((SET_SIG_VALUE *)pSig),
					((SET_SIG_VALUE *)pSig)->pStdSig->iSigID,
					(pSig->ucType == VAR_FLOAT ? p->varSignalVal.fValue 
					: p->varSignalVal.lValue));
#endif //_DEBUG_EQUIP_INIT
				fMin = ((SET_SIG_VALUE *)pSig)->pStdSig->fMinValidValue;
				fMax = ((SET_SIG_VALUE *)pSig)->pStdSig->fMaxValidValue;
				fVal = (pSig->ucType == VAR_FLOAT)         ? p->varSignalVal.fValue 
					: (pSig->ucType != VAR_UNSIGNED_LONG)  ? (float)p->varSignalVal.lValue
					: (float)p->varSignalVal.ulValue;

				//Only load the data when the data range is right. 20170810 by Stone Song
				if ((fMin <= (fVal+EPSILON)) && (fMax >= (fVal-EPSILON)))
				{
					pSig->varLastValue     = p->varSignalVal;
					pSig->varValue         = p->varSignalVal;
					pSig->tmCurrentSampled = p->tmSignalTime;

					SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);	// set it valid
				}
			}
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_LoadSetSigValue
 * PURPOSE  : load the last value stored in the flash/
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-05 10:21
 *==========================================================================*/
static BOOL Init_LoadSetSigValue(IN OUT SITE_INFO *pSite)
{
	PERSISTENT_SIG_RECORD	data[NUM_REC_PER_READ];
	HANDLE					hFile;
	int						iStart, nRecRead;

	hFile = DAT_StorageOpen(PERSISTENT_SIG_LOG);
	if (hFile == NULL)
	{
		AppLogOut(EQP_INIT, APP_LOG_WARNING,
			"Fails on open setting value file: %s\n",
			PERSISTENT_SIG_LOG);

		// maybe this is the first time to run the system.
		return TRUE;
	}

	for (iStart = 1; iStart != -2/*read end*/; iStart = 0/*continue*/)
	{
		nRecRead = NUM_REC_PER_READ;
		if (!DAT_StorageReadRecords(hFile, &iStart, &nRecRead, (void *)data, 
			FALSE, TRUE))
		{
			AppLogOut(EQP_INIT, APP_LOG_WARNING,
				"Fails on reading setting values: startRec=%d, nRec=%d.\n",
				iStart, nRecRead);
			break;	// do NOT return FALSE, due to the file maybe not created.
		}

		if (nRecRead <= 0)	// read end of records.
		{
#ifdef _LOG_DETAIL_INFO
			AppLogOut(EQP_INIT, APP_LOG_UNUSED,
				"End of reading setting values: startRec=%d, nRec=%d.\n",
				iStart, nRecRead);
#endif
			break;
		}

		Init_RestoreSettingValueInfo(pSite, data, nRecRead);
	}


	DAT_StorageClose(hFile);

	return TRUE;
}


typedef struct _DATA_FILE_CREATING_INFO
{
	char	*pszFileName;
	int		nRecordSize;
	int		nMaxRecords;
	float	fWriteFreq;	// in rec/year
	HANDLE	*pHandle;
	ERASE_SECTOR_PRE_PROC pfnOnErasingRecords;// called before some data need be erased.
} DATA_FILE_CREATING_INFO;

#define _DEF_FILE_CREATING_INFO(name, size, maxRec, freq, pHandle, onErasing)	\
	{(name), (size), (maxRec), (freq), (HANDLE *)(pHandle), \
	(ERASE_SECTOR_PRE_PROC)(onErasing)}

/*==========================================================================*
 * FUNCTION : Init_CreateDataFiles
 * PURPOSE  : create the FLASH files used to save historical data
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 *            IN BOOL     bClearContent: TRUE: clear the contents of all logs.
 * RETURN   : static BOOL : Always TRUE. 
 * COMMENTS : if fails on creating file, the error msg will be logged to log file
 *            To erase contents if solution is changed.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-09 19:06
 *==========================================================================*/
static BOOL Init_CreateDataFiles(IN OUT SITE_INFO *pSite, IN BOOL bClearContent)
{	
	DATA_FILE_CREATING_INFO	pFileInfo[] = 
	{
		// historical alarm
		_DEF_FILE_CREATING_INFO(HIST_ALARM_LOG, sizeof(HIS_ALARM_RECORD),
			MAX_HIS_ALARM_COUNT, FREQ_HIS_ALARM, &pSite->hHistoricalAlarm,
			NULL),
		// history data
		_DEF_FILE_CREATING_INFO(HIS_DATA_LOG, sizeof(HIS_DATA_RECORD),
			MAX_HIS_DATA_COUNT, FREQ_HIS_DATA, &pSite->hHistoricalData,
			NULL),
		// stat data
		_DEF_FILE_CREATING_INFO(STAT_DATA_LOG, sizeof(HIS_STAT_RECORD),
			MAX_STAT_DATA_COUNT, FREQ_STAT_DATA, &pSite->hStatData,
			NULL),
#ifdef SAVE_ACTIVE_ALARMS
		// active alarm
		_DEF_FILE_CREATING_INFO(ACTIVE_ALARM_LOG, sizeof(ACT_ALARM_RECORD),
			pSite->nMaxAllowedActiveAlarms, FREQ_ACTIVE_ALARM, &pSite->hActiveAlarm,
			NULL),
#endif
		// ctrl log
		_DEF_FILE_CREATING_INFO(CTRL_CMD_LOG, sizeof(HIS_CONTROL_RECORD),
			MAX_CTRL_CMD_COUNT, FREQ_CTRL_CMD, &pSite->hControlLog,
			NULL),
		// persistent signal
		_DEF_FILE_CREATING_INFO(PERSISTENT_SIG_LOG, sizeof(PERSISTENT_SIG_RECORD),
			pSite->nMaxAllowedPersistentSig, FREQ_PERSISTENT_SIG, &pSite->hPersistentSig,
			NULL),
	};
	int		i;

	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Creating%s historical and runtime data files in storage...\n",
		bClearContent ? " and clearing" : "");

	// 1. create the data files.
	for (i = 0; i < ITEM_OF(pFileInfo); i++)
	{
		// clear the contents if solution changed. maofuhua, 2005-3-1
		if (bClearContent)
		{
			BOOL	bOK;


			if(i != 5)//
			{
				TRACE("\n StorageDeleteRecord:%s \n",pFileInfo[i].pszFileName);
				bOK = DAT_StorageDeleteRecord(pFileInfo[i].pszFileName);

			}
			

#ifndef _LOG_DETAIL_INFO
			if (!bOK)
#endif
			{
				AppLogOut(EQP_INIT, APP_LOG_WARNING, 
					"Emptying file %s for solution changed...%s.\n",
					pFileInfo[i].pszFileName,
					bOK ? "OK" : "FAILED");
			}
		}

		*pFileInfo[i].pHandle = DAT_StorageCreate(pFileInfo[i].pszFileName,
			pFileInfo[i].nRecordSize, pFileInfo[i].nMaxRecords,
			pFileInfo[i].fWriteFreq);
		if ((*pFileInfo[i].pHandle != NULL) &&
			(pFileInfo[i].pfnOnErasingRecords != NULL))
		{
			// set the on-erasing callback function
			DAT_RegisterNotifiy(*pFileInfo[i].pHandle, 
				*pFileInfo[i].pfnOnErasingRecords, (void *)pSite);
		}

		// log after created...
#ifndef _LOG_DETAIL_INFO
		if (*pFileInfo[i].pHandle == NULL)
#endif
		{
			AppLogOut(EQP_INIT, APP_LOG_INFO, 
				"Creating %s(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s.\n", 
				pFileInfo[i].pszFileName, pFileInfo[i].nRecordSize, 
				pFileInfo[i].nMaxRecords, pFileInfo[i].fWriteFreq,
				((*pFileInfo[i].pHandle != NULL) ? "OK" : "FAILED"));
		}
	}

	AppLogOut(EQP_INIT, APP_LOG_INFO, 
		"Historical and runtime data files have been created OK in storage.\n");

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Init_DataFiles
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : static BOOL : always TRUE. the system can continue 
 *                          by using default settings
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-01-10 11:34
 *==========================================================================*/
static BOOL Init_DataFiles(IN OUT SITE_INFO *pSite)
{
	BOOL	bClearContents;

	//1. Chk the solution is changed or NOT. if the solution config is changed
	// all of the historical data will be erased. maofuhua, 2005-3-1
	bClearContents = (pSite->nConfigStatus != CONFIG_STATUS_NOT_CHANGED)
		? TRUE : FALSE;

	//2. create the data files.
	/*5 space to avoid frequently calling re-save all setting signals	*/
	pSite->nMaxAllowedPersistentSig = MAX_PERSIST_SIG_COUNT*5;
	pSite->nMaxAllowedActiveAlarms  = MAX_ACTIVE_ALARM_COUNT*4;
	Init_CreateDataFiles(pSite, bClearContents);

	// forcibly to resave all setting sig and active alarm again
	pSite->nCurrentSavedAlarms      = pSite->nMaxAllowedActiveAlarms;
	pSite->nCurrentSavedPersistentSig=pSite->nMaxAllowedPersistentSig;

	//3. Load the last save setting signal values
	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Loading value of setting signals from storage...\n");

	if (!Init_LoadSetSigValue(pSite))
	{
		AppLogOut(EQP_INIT, APP_LOG_WARNING, 
			"Fails on loading last setting signal values from storage.\n");

		// always TRUE. the system can continue by using default settings
		return TRUE;
	}

	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Value of setting signals are loaded from storage OK.\n");

	if (bClearContents)
	{
		return TRUE;	// NOT need to load old settings.
	}

	

	//Added by Thomas for TR# 62-ACU, 2006-11-28
	//4. Load setting signal values from Param.run file
	AppLogOut(EQP_INIT, APP_LOG_INFO, 
		"Loading value of setting signals from Param file...\n");

	if (!Init_LoadParamFile(pSite))
	{
		AppLogOut(EQP_INIT, APP_LOG_WARNING, 
			"Fails on loading value of setting signals from Param file.\n");

		// continue.
	}
	else
	{
		AppLogOut(EQP_INIT, APP_LOG_INFO, 
			"Value of setting signals are loaded from Param file OK.\n");
	}
	//Added end by Thomas, 2006-11-28

#ifdef SAVE_ACTIVE_ALARMS
	//5. Load the active alarms status from storage
	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Loading active alarms from storage...\n");

	if (!Init_LoadActiveAlarms(pSite))
	{
		AppLogOut(EQP_INIT, APP_LOG_WARNING, 
			"Fails on loading the active alarms status from storage.\n");

		return TRUE;
	}

	AppLogOut(EQP_INIT, APP_LOG_UNUSED, 
		"Active alarms are loaded from storage OK.\n");
#endif //SAVE_ACTIVE_ALARMS

	return TRUE;
}




//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
#ifdef PRODUCT_INFO_SUPPORT
//debug utilities
/*==========================================================================*
 * FUNCTION : Init_ShowProductInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PRODUCT_INFO  *pProductInfo : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : scu+ team                DATE: 2006-05-12 16:00
 *==========================================================================*/
static void Init_ShowProductInfo(IN PRODUCT_INFO *pProductInfo)
{
	printf("SigModelUsed Flag: %d\n", pProductInfo->bSigModelUsed);
	printf("Part Number: %s\n", pProductInfo->szPartNumber);
	printf("Serial Number: %s\n", pProductInfo->szSerialNumber);
	printf("HW Version: %s\n", pProductInfo->szHWVersion);
	printf("SW Version: %s\n", pProductInfo->szSWVersion);
}

/*==========================================================================*
 * FUNCTION : Init_ShowConvertorInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN PI_CONVERTOR  *pConvertor : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : scu+ team                DATE: 2006-05-12 16:00
 *==========================================================================*/
static void Init_ShowConvertorInfo(IN PI_CONVERTOR *pConvertor)
{
	printf("Convertor Type: %d\n", pConvertor->eConvertorType);
	printf("Ref Sig ID: %d\n", pConvertor->iRelevantSigID);
	printf("Ref Sig: %08x\n", pConvertor->pSigRef);
	if (pConvertor->pSigRef)
	{
		printf("Ref Sig Name: %s\n", 
			pConvertor->pSigRef->pStdSig->pSigName->pFullName[0]);
	}
	printf("Convertor Proc Address: %08x\n", pConvertor->pfnConverProc);	
}
	
/*==========================================================================*
 * FUNCTION : Init_ShowDeviceInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : scu+ team                DATE: 2006-05-12 16:00
 *==========================================================================*/
static void Init_ShowDeviceInfo(IN SITE_INFO *pSite)
{
	int   i, j, iDeviceNum;
	DEVICE_INFO		*pDevice;

	iDeviceNum = pSite->iDeviceNum;
	pDevice = pSite->pDeviceInfo;

	TRACE("===================================\n");
	TRACE("PSC Product Information\n");
	TRACE("Total Devices: %d\n", iDeviceNum);
	for (i = 0; i < iDeviceNum; i++, pDevice++)
	{
//#ifdef _DEBUG_DEVICE_INIT_DETAIL
		//TRACE("Device No: %d nRelatedEquip %d iEquipID %d\n", i + 1,pDevice->nRelatedEquip,pDevice->->ppRelatedEquip[0].iEquipID);
		Init_ShowProductInfo(pDevice->pProductInfo);
//#endif	
		
//#ifdef _DEBUG_CONVERTOR_DETAIL
		if (pDevice->pConvertor)
		{
			for (j = 0; j < PI_CONVERTOR_NUM; j++)
			{
				Init_ShowConvertorInfo(pDevice->pConvertor + j);
			}
		}
		else
		{
			TRACE("The Convertors have not been defined for the Device.\n");
		}
//#endif //_DEBUG_CONVERTOR_DETAIL


		TRACE("Relevant Equip number: %d\n", pDevice->nRelatedEquip);
		for (j = 0; j < pDevice->nRelatedEquip; j++)
		{
			TRACE("Equip Name(%d):%s\n", j,
				GET_LANG_TEXT0(pDevice->ppRelatedEquip[j]->pEquipName));
		}

		TRACE("\n\n");

	}

	TRACE("===================================\n");

}

/*==========================================================================*
 * FUNCTION : Init_EquipPISigRef
 * PURPOSE  : Only for rectifier
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-08 08:25
 *==========================================================================*/
static void Init_PIConvertors(IN OUT SITE_INFO *pSite)
{
	int					i, j, etype;

	STDEQUIP_TYPE_INFO	*pStdEquip;
	EQUIP_INFO      *pEquip;
	PI_CONVERTOR    *pConvertor;

	//1.init the std equip's convertors.
	pStdEquip = pSite->pStdEquipTypeInfo;
	for (i = 0; i < pSite->iStdEquipTypeNum; i++, pStdEquip++)
	{
		pConvertor = pStdEquip->stConvertor;

		for (j = 0; j < PI_CONVERTOR_NUM; j++, pConvertor++)
		{
			etype = j;
			pConvertor->eConvertorType = etype;
			PI_GetConverterCfg(pConvertor, pStdEquip->iTypeID);
		}
	}

	//2.set flag to equips
	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		pEquip->bPISigUsed = 
			(pEquip->pStdEquip->stConvertor[0].pfnConverProc == NULL) ?
			FALSE : TRUE;
	}
		

	return;
}


/*==========================================================================*
 * FUNCTION : Init_GetDeviceNum
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-08 11:37
 *==========================================================================*/
static int Init_GetDeviceNum(IN SITE_INFO *pSite)
{
	int		iDeviceNum, iExtendedNum;
	int		i, j;
	//BOOL    bExtended;   

	SAMPLER_INFO    *pSampler;
	EQUIP_INFO      *pEquip;

	iExtendedNum = 0; //ACU takes the place of SCU sampler, Thomas, 2006-2-14
	//iExtendedNum = 1;  //add product info of ACU itself

	//go through the samplers
	pSampler = pSite->pSamplerInfo;
	for (i = 0; i < pSite->iSamplerNum; i++, pSampler++)
	{
		//bExtended = FALSE;
		for (j = 0; j < pSampler->nRelatedEquipment; j++)
		{
			pEquip = pSampler->ppRelatedEquipment[j];
			if (pEquip->bPISigUsed)
			{
				//ref sig added
				iExtendedNum++;

				//Sampler always has the product info, even it has been extended!
				//Thomas, 2006-2-14
				/*
				 *if the sampler has been extended, its own Product Info 
				 *will be omitted
				 */
				//bExtended = TRUE;
			}
		}

		/*if (bExtended)
		{
			iExtendedNum--;
		}*/
	}

	iDeviceNum = pSite->iSamplerNum + iExtendedNum - 1;//delete can and i2c sampler,keep the MB sampler

	return iDeviceNum;
}


/*==========================================================================*
 * FUNCTION : *Init_ConvertorSigRef
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN EQUIP_INFO    *pEquip     : 
 *            IN PI_CONVERTOR  *pConvertor : 
 * RETURN   : int: 0 = OK
 *				   1 = not configured in the Convertor
 *                 2 = fail to get the sig ref
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-09 09:39
 *==========================================================================*/
static int Init_ConvertorSigRef(IN EQUIP_INFO *pEquip,
								IN PI_CONVERTOR *pConvertor)
{
	int   iMergedSigID;
	SAMPLE_SIG_VALUE	*pSig = NULL;	

	iMergedSigID = pConvertor->iRelevantSigID;

	/* 0 means the sig ref is not configured */
	if (!iMergedSigID)
	{
		return 1;
	}

	pSig = (SAMPLE_SIG_VALUE *)Init_GetEquipSigRefById(pEquip,
		DXI_SPLIT_SIG_MID_TYPE(iMergedSigID),
		DXI_SPLIT_SIG_MID_ID(iMergedSigID));


	pConvertor->pSigRef = pSig;
	
	return (pSig ? 0 : 2);
}


/*==========================================================================*
 * FUNCTION : Init_AttachConvertors
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-09 09:39
 *==========================================================================*/
static void Init_AttachConvertors(IN SITE_INFO *pSite)
{
	int    i, j, iDeviceNum, iRet;
	char   *pErrMsg;
	DEVICE_INFO			 *pDevice;
	EQUIP_INFO		     *pEquip;
	STDEQUIP_TYPE_INFO	 *pStdEquip;
	PI_CONVERTOR		 *pConvertor;

	iDeviceNum = pSite->iDeviceNum;
	pDevice = pSite->pDeviceInfo;
	for (i = 0; i < iDeviceNum; i++, pDevice++)
	{
		if (pDevice->nRelatedEquip == 1 &&
			pDevice->ppRelatedEquip[0]->bPISigUsed)  //Only for rectifier
		{
			pEquip = pDevice->ppRelatedEquip[0];
			pStdEquip = pEquip->pStdEquip;

			//pDevice->pConvertor = pStdEquip->stConvertor; -- dynamic creat!
			pDevice->pConvertor = NEW(PI_CONVERTOR, PI_CONVERTOR_NUM);
			if(pDevice->pConvertor == NULL)
			{
				return;
			}
			memcpy(pDevice->pConvertor, pStdEquip->stConvertor, 
				PI_CONVERTOR_NUM * sizeof(PI_CONVERTOR));

			//init Sig Ref
			for (j = 0; j < PI_CONVERTOR_NUM; j++)
			{
				pConvertor = pDevice->pConvertor+j;

				iRet = Init_ConvertorSigRef(pEquip, pConvertor);
				pErrMsg = (iRet == 2) ? "no such sig"
					: (iRet == 1) ? "not configured in the Convertor"
					: NULL;	
				if (pErrMsg)
				{			
					// it's no matter and continue. do NOT stop.
					AppLogOut( EQP_INIT, APP_LOG_WARNING,
						"Fails on ref the sampling signal(ID=%d) of Production Info of "
						"\"%s\"(%d): %s\n",
						DXI_SPLIT_SIG_MID_ID(pConvertor->iRelevantSigID),
						EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID,
						pErrMsg);
				}
			}
		}//end of Convertor process
	}//end of Device go through
}

/*==========================================================================*
 * FUNCTION : Init_Devices
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-10-08 14:10
 *==========================================================================*/
static BOOL Init_Devices(IN OUT SITE_INFO *pSite)
{
	int   i, j, iDeviceNum;
	//BOOL  bExtendedSampler;

	DEVICE_INFO		*pDevice;
	SAMPLER_INFO	*pSampler;
	EQUIP_INFO      *pEquip;


	//1.init convertors first
	Init_PIConvertors(pSite);

	//2.get sum of sampler num and rectifiers
	iDeviceNum = Init_GetDeviceNum(pSite);
	pDevice = NEW(DEVICE_INFO, iDeviceNum);
	if (!pDevice)
	{
		AppLogOut(EQP_INIT, APP_LOG_ERROR, 
				"Fails on init devices for there is no memory.\n");

		return FALSE;
	}

	ZERO_POBJS(pDevice, iDeviceNum);  //clear to zero
	pSite->iDeviceNum = iDeviceNum;
	pSite->pDeviceInfo = pDevice;


	//3.init structure
	/* init others (Rect, BBU, etc.) */
	pSampler = pSite->pSamplerInfo;
 
	for (i = 0; i < pSite->iSamplerNum; i++, pSampler++)
	{
		DEVICE_INFO  *pBufDevice;
	
		//bExtendedSampler = FALSE;
		//use ACU product info for scu sampler
		if (pSampler->iStdSamplerID == SAMPLER_TYPE_ID_SCU)
		{
			pDevice->pProductInfo = DXI_GetDeviceProductInfo();
		}
		else 	if ((pSampler->iStdSamplerID == SAMPLER_TYPE_ID_I2C))
		{
			continue;
		}
		else
		{
			//why not initiate pSampler->stProductInfo
			pDevice->pProductInfo = &pSampler->stProductInfo; 
		}

		pBufDevice = pDevice;  //buffer the device for the origine sampler

		for (j = 0; j < pSampler->nRelatedEquipment; j++)
		{
			DEVICE_INFO		*pCurDevice;

			pEquip = pSampler->ppRelatedEquipment[j];
	

			
#ifdef _DEBUG_DEVICE_INIT_DETAIL
			TRACE("j=%d\tRelevant Equip Name: %s\n",
				j, GET_LANG_TEXT0(pEquip->pEquipName));
#endif //_DEBUG_DEVICE_INIT_DETAIL


			if (pEquip->bPISigUsed)
			{
#ifdef _DEBUG_DEVICE_INIT_DETAIL
				TRACE("Extended Sampler!\n");
#endif //_DEBUG_DEVICE_INIT_DETAIL

				//In the case, the equip is taken as the Device
				pDevice++;

				pDevice->pProductInfo = &pSampler->stProductInfo;
				pCurDevice = pDevice;

				//bExtendedSampler = TRUE;
			}
			else
			{

				pCurDevice = pBufDevice;    //Unchange the start pos
			}
			
			//add pEquip pointor to pCurrDevice->ppRelatedEquip item to array
			AppendArrayItem((void **)&pCurDevice->ppRelatedEquip, 
				sizeof(pCurDevice->ppRelatedEquip[0]),
				&pCurDevice->nRelatedEquip,
				&pEquip);
		}
		pDevice++;
	}

	//4.get Device PI Convertors from std equip to used rectifiers equip
	Init_AttachConvertors(pSite);

	//5.set DeviceID field
	pDevice = pSite->pDeviceInfo;
	for (i = 0; i < pSite->iDeviceNum; i++, pDevice++)
	{
		pDevice->iDeviceID = i+1;
	}
    
#ifdef _DEBUG_EQUIP_INIT_DEVICE
	Init_ShowDeviceInfo(pSite);	
#endif //_DEBUG_EQUIP_INIT_DEVICE

	return TRUE;
}
#endif //PRODUCT_INFO_SUPPORT

/*==========================================================================*
* FUNCTION : Init_Aux_ReadParamToBuff
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: CFG_PARAM_RUN_INFO  *pRunInfo : 
*            FILE                *pf       : 
* RETURN   : int  !WARNING: You are missing the return type!: 
* COMMENTS : Auxiliary function, added for TR# 62-ACU
* CREATOR  : LinTao                   DATE: 2006-11-28 18:56
*==========================================================================*/
static BOOL Init_Aux_ReadParamToBuff(CFG_PARAM_RUN_INFO *pRunInfo, FILE *pf)
{
	// 1.head info section
	size_t  nCount = (size_t)ITEM_OF(pRunInfo->sFileHeadInfo);
	if (nCount != fread(pRunInfo->sFileHeadInfo, sizeof(char), nCount, pf))
	{
		return FALSE;
	}

	// 2.version section
	if (!fread(&pRunInfo->iVersion, sizeof(int), 1, pf))
	{
		return FALSE;
	}

	// 3.param num section
	if (!fread(&pRunInfo->iParamNum, sizeof(int), 1, pf))
	{
		return FALSE;
	}

	// 4.param data section will be read after checking validity - see Init_Aux_ChkReadParam function

	// 5.check sum section
	{
		long lLastPos = ftell(pf);
		long lPos = -sizeof(int);

		fseek(pf, lPos, SEEK_END);
		if (!fread(&pRunInfo->iCheckSum, sizeof(int), 1, pf))
		{
			DELETE(pRunInfo->pParam );
			return FALSE;
		}

		//move back 
		fseek(pf, lLastPos, SEEK_SET);
	}


	return TRUE;
}




/*==========================================================================*
* FUNCTION : Init_Aux_ChkReadParam
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: CFG_PARAM_RUN_INFO  *pRunInfo : 
*            SITE_INFO           *pSite    : 
* RETURN   : int : 0 for success!
* COMMENTS : auxiliary function, added for TR# 62-ACU
* CREATOR  : LinTao                   DATE: 2006-11-29 11:50
*==========================================================================*/
static int Init_Aux_ChkReadParam(CFG_PARAM_RUN_INFO *pRunInfo, FILE *pf, SITE_INFO *pSite)
{
	// 1. Head info
	if (strncmp(pRunInfo->sFileHeadInfo, PARAM_FILE_HEAD_INFO, sizeof(PARAM_FILE_HEAD_INFO)))
	{
		return 1;
	}

	// 2. Version 
	//if (pRunInfo->iVersion != VER_PARAM_RECORD)
	if (pRunInfo->iVersion > VER_PARAM_RECORD ||
		pRunInfo->iVersion < VER_PARAM_RECORD_INITIAL)  //need backward compatible, for G3_OPT [config]
	{
		return 2;
	}

	// 3. Param data
	{
		// 3.1 checking the param number
		int		iParamNum = pRunInfo->iParamNum;
		PERSISTENT_SIG_RECORD		*pRecord;
		//printf("\n iParamNum:%d \n",iParamNum);
		//printf("\n nMaxAllowedPersistentSig \n",pSite->nMaxAllowedPersistentSig);

		if (iParamNum < 0 || iParamNum > pSite->nMaxAllowedPersistentSig)
		{
			return 3;
		}

		// 3.2 allocate memory and load param data
		pRecord = NEW(PERSISTENT_SIG_RECORD, iParamNum);
		if (pRecord == NULL)
		{
			AppLogOut("EQP_INIT", APP_LOG_ERROR, "No enough memory for reading Param file!\n");
			TRACE("[%s]--%s: ERROR: No enough memory for reading Param file!\n", __FILE__,__FUNCTION__);

			return 3;
		}

		if ((size_t)iParamNum != fread(pRecord, sizeof(PERSISTENT_SIG_RECORD), (size_t)iParamNum, pf))
		{
			DELETE(pRecord);
			return 3;
		}

		//Note: need to DELETE outer if return value is TRUE!
		pRunInfo->pParam = pRecord;
	}

	// 4. User info, added for G3_OPT [config], 2013-5
	if (pRunInfo->iVersion == VER_PARAM_USERINFO_SUPPORT &&
		!GetUserInfoFromRunFile(pf))
	{
		AppLogOut("EQP_INIT", APP_LOG_ERROR, "Fails to load user/NMS info from Param file!\n");
		TRACE("[%s]--%s: ERROR: Fails to load user/NMS info from Param file!\n", __FILE__,__FUNCTION__);

		return 4;
	}

	// 5. Check sum
	{
		WORD dwSum = 0;
		int  i, nChrCount = pRunInfo->iParamNum * sizeof(PERSISTENT_SIG_RECORD);
		char *pFrame = (char *)pRunInfo->pParam;

		for (i = 0; i < nChrCount; i++)
		{
			dwSum += *pFrame++;
		}

		if (pRunInfo->iCheckSum != (WORD)-dwSum)
		{
			DELETE(pRunInfo->pParam);
			return 5;
		}
	}

	return 0;
}

/*==========================================================================*
* FUNCTION : Init_LoadParamFile
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN OUT SITE_INFO  *pSite : 
* RETURN   : BOOL : 
* COMMENTS : Added for TR# 62-ACU
* CREATOR  : LinTao                   DATE: 2006-11-28 14:27
*==========================================================================*/
static BOOL Init_LoadParamFile(IN OUT SITE_INFO *pSite)
{
	CFG_PARAM_RUN_INFO  stRunInfo;
	FILE *pFile;

#define RUN_PARAMFILE					"run/SettingParam.run"
	char szFullPath[MAX_FILE_PATH];  //full config file name
	Cfg_GetFullConfigPath(RUN_PARAMFILE, szFullPath, MAX_FILE_PATH);

	pFile = fopen(szFullPath, "rb");
	if (pFile == NULL)
	{
		AppLogOut("EQP_INIT", APP_LOG_WARNING, "Can not open the file: %s!\n",
			szFullPath);
		TRACE("[%s]--%s: ERROR: Can not open the file: %s!\n", __FILE__,
			__FUNCTION__, szFullPath);
		return FALSE;
	}

	//1. Load data to buffer
	if (!Init_Aux_ReadParamToBuff(&stRunInfo, pFile))
	{
		AppLogOut("EQP_INIT", APP_LOG_ERROR, "Read the file: %s error, not read enough info!\n",
			szFullPath);
		TRACE("[%s]--%s: ERROR: Read the file: %s error, not read enough info!\n", __FILE__,
			__FUNCTION__, szFullPath);

		fclose(pFile);
		return FALSE;
	}

	//2.Check validity - Name field, version info, check sum...
	{
		int iRet = Init_Aux_ChkReadParam(&stRunInfo, pFile, pSite);
		if (iRet)
		{
			AppLogOut("EQP_INIT", APP_LOG_ERROR, "Param file content is invalid(err code:%d)!\n", iRet);
			TRACE("[%s]--%s: ERROR: Param file content is invalid(err code:%d)!\n", __FILE__, __FUNCTION__, iRet);

			fclose(pFile);
			return FALSE;
		}
		else
		{
			fclose(pFile);
		}
	}


	//3. Update Site_info, flash will be updated during Init_RefreshU2Param process by sending cmd to U2 board
	Init_RestoreSettingValueInfo(pSite, stRunInfo.pParam, stRunInfo.iParamNum);

	

	Site_ResavePersistentSigs(pSite);

	//4. Remove .run file
	{
		char szCmdLine[56];

		snprintf(szCmdLine, sizeof(szCmdLine), "rm -fr %s", szFullPath);
		//system(szCmdLine); 
		_SYSTEM(szCmdLine); 
	}

	DELETE(stRunInfo.pParam);
	return TRUE;
}


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : test_equip_mon.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-02 09:12
 *  VERSION  : V1.00
 *  PURPOSE  : To unload the site based on the loaded configuration information
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"
#include <signal.h>

#include "cfg_model.h"
#include "../config_mgmt/cfg_mgmt.h"	// for loading the site config info
#include "equip_mon.h"


#define _HAS_MAIN  1

char				g_szACUConfigDir[MAX_FILE_PATH];
SITE_INFO			g_SiteInfo;
SERVICE_MANAGER		g_ServiceManager;

#ifndef _HAS_MAIN

//STUBS for TEST
TIME_SRV_INFO	g_sTimeSrvInfo;  

int UpdateSCUPTime(time_t* pTimeRet)
{
	return stime(pTimeRet);;
}

//END OF STUBS

static DWORD RunThreadEventHandler(		
	DWORD	dwThreadEvent,		//	the thread event. below.
	HANDLE	hThread,				//	The thread id.
	const char *pszThreadName)
{
	UNUSED(pszThreadName);

	if (THREAD_EVENT_NO_RESPONSE == dwThreadEvent)
	{
		printf( "[RunThreadEventHandler] -- %p is no response, kill it!\n",
			hThread);
		return THREAD_CANCEL_THIS;
	}

	return THREAD_CONTINUE_RUN;
}

#define SYS_MAIN	"SYS MAIN"

int main(int argc, char *argv[])
{
	int nErrors = 0;

#ifdef _DEBUG
	int		nBlock, nId;
	size_t  ulBytes;
#endif //_DEBUG

	AppLogOut(SYS_MAIN, APP_LOG_INFO, "+---------------------------------------+\n");
	AppLogOut(SYS_MAIN, APP_LOG_INFO, "|   SCU plus system now is starting...  |\n");
	AppLogOut(SYS_MAIN, APP_LOG_INFO, "+---------------------------------------+\n");

	// init thread mamager
	RunThread_ManagerInit(RunThreadEventHandler);	// must be at 1st

	if (Cfg_InitialConfig() != ERR_CFG_OK)
	{
		printf("[main] -- Cfg_InitialConfig failure.\n");
		nErrors++;
	}

	else if (!EquipMonitoring_Start(&g_SiteInfo))
	{
		printf("[main] -- EquipMonitoring_Start failure.\n");
		nErrors++;
	}

#ifdef _DEBUG
	nBlock = MEM_GET_INFO(&nId, &ulBytes);
	AppLogOut("MAIN", APP_LOG_INFO, "Total allocated %d mem blocks, %u bytes, "
		"%d time NEW/RENEW calls.\n",
		nBlock, ulBytes, nId);
#endif //_DEBUG
	
	printf("[main] -- press Enter to quit.\n");getchar();
	

	EquipMonitoring_Stop(&g_SiteInfo);

	Cfg_UnloadConfig();

	// stop the  thread manager.
	RunThread_ManagerExit(10000, TRUE);	// must be at last

	AppLogOut(SYS_MAIN, APP_LOG_INFO, "+---------------------------------------+\n");
	AppLogOut(SYS_MAIN, APP_LOG_INFO, "|   SCU plus system exited %s  |\n",
		(nErrors == 0) ? "normally.  " : "abnormally." );
	AppLogOut(SYS_MAIN, APP_LOG_INFO, "+---------------------------------------+\n\n");

	return nErrors;
}

#endif

#define GET_CHN(ch)		((((ch)>=0) && (COMM_STATUS_CHANNEL != 0)) ? (ch)-1 : (ch))

/////////////////////////////////////////////////////////////////////////////
// debug the equipment data
/////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG_EQUIP_TASK // defined in Makefile.

#define DISPLAY_SIG_A(i, pSig)											\
	if ((pSig)->bv.ucType == VAR_FLOAT)									\
	{																	\
		printf("[%4d]: ID=%4d(SID=%2d,CH=%2d) %-32s%c= %1.3f(Raw=%1.3f) %-8s\n",\
			(i)+1, (pSig)->pStdSig->iSigID,								\
			pSig->sv.iSamplerID, GET_CHN(pSig->sv.iSamplerChannel),				\
			EQP_GET_SIGV_NAME0((pSig)),									\
			SIG_VALUE_IS_VALID((pSig)) ? ' ' : SIG_VALUE_IS_CONFIGURED(pSig) ? 'x' : '-',\
			(pSig)->bv.varValue.fValue,									\
			(pSig)->bv.fRawData,										\
			(pSig)->pStdSig->szSigUnit);								\
	}																	\
	else																\
	{																	\
		printf("[%4d]: ID=%4d(SID=%2d,CH=%2d) %-32s%c= %ld(Raw=%1.3f) %-8s\n",\
			(i)+1, (pSig)->pStdSig->iSigID,								\
			pSig->sv.iSamplerID, GET_CHN(pSig->sv.iSamplerChannel),				\
			EQP_GET_SIGV_NAME0((pSig)),									\
			SIG_VALUE_IS_VALID((pSig)) ? ' ' : SIG_VALUE_IS_CONFIGURED(pSig) ? 'x' : '-',\
			(pSig)->bv.varValue.lValue,									\
			(pSig)->bv.fRawData,										\
			(pSig)->pStdSig->szSigUnit);								\
	}


#define DISPLAY_CTRL_SIG_A(i, pSig)							\
	if ((pSig)->bv.ucType == VAR_FLOAT)									\
	{																	\
		printf("[%4d]: ID=%4d(SID=%2d,CH=%2d,CMD=%c%d) %-32s%c= %1.3f(Raw=%1.3f) %-8s\n",\
			(i)+1, (pSig)->pStdSig->iSigID,								\
			pSig->sv.iSamplerID, GET_CHN(pSig->sv.iSamplerChannel),				\
			SIG_VALUE_IS_SETTABLE(pSig) ? ' ' : 'x', pSig->sv.iControlChannel,	\
			EQP_GET_SIGV_NAME0((pSig)),									\
			SIG_VALUE_IS_VALID((pSig)) ? ' ' : SIG_VALUE_IS_CONFIGURED(pSig) ? 'x' : '-',\
			(pSig)->bv.varValue.fValue,									\
			(pSig)->bv.fRawData,										\
			(pSig)->pStdSig->szSigUnit);								\
	}																	\
	else																\
	{																	\
		printf("[%4d]: ID=%4d(SID=%2d,CH=%2d,CMD=%c%d) %-32s%c= %ld(Raw=%1.3f) %-8s\n",\
			(i)+1, (pSig)->pStdSig->iSigID,								\
			pSig->sv.iSamplerID, GET_CHN(pSig->sv.iSamplerChannel),				\
			SIG_VALUE_IS_SETTABLE(pSig) ? ' ' : 'x', pSig->sv.iControlChannel,	\
			EQP_GET_SIGV_NAME0((pSig)),									\
			SIG_VALUE_IS_VALID((pSig)) ? ' ' : SIG_VALUE_IS_CONFIGURED(pSig) ? 'x' : '-',\
			(pSig)->bv.varValue.lValue,									\
			(pSig)->bv.fRawData,										\
			(pSig)->pStdSig->szSigUnit);								\
	}


#define DISPLAY_ALL_SIG(i, nSig, pSig)	\
	for ((i) = 0; (i) < (nSig); (i)++, (pSig)++)	\
	{												\
		DISPLAY_SIG_A((i), (pSig));	\
	}

#define DISPLAY_ALL_CTRL_SIG(i, nSig, pSig)	\
	for ((i) = 0; (i) < (nSig); (i)++, (pSig)++)	\
	{												\
		DISPLAY_CTRL_SIG_A((i), (pSig));	\
	}


static BOOL Equip_DisplaySamplingSignals(IN EQUIP_INFO *pEquip)
{
	int					i;
	int					nSig  = pEquip->pStdEquip->iSampleSigNum;
	SAMPLE_SIG_VALUE	*pSig = pEquip->pSampleSigValue;

	printf("\n\n%s(%d) has %d sampling signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		nSig);

	DISPLAY_ALL_SIG(i, nSig, pSig);

	return TRUE;
}


static BOOL Equip_DisplaySettingSignals(IN EQUIP_INFO *pEquip)
{
	int					i;
	int					nSig  = pEquip->pStdEquip->iSetSigNum;
	SET_SIG_VALUE		*pSig = pEquip->pSetSigValue;

	printf("\n\n%s(%d) has %d setting signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		nSig);

	DISPLAY_ALL_CTRL_SIG(i, nSig, pSig);

	return TRUE;
}


static BOOL Equip_DisplayControlSignals(IN EQUIP_INFO *pEquip)
{
	int					i;
	int					nSig  = pEquip->pStdEquip->iCtrlSigNum;
	CTRL_SIG_VALUE		*pSig = pEquip->pCtrlSigValue;

	printf("\n\n%s(%d) has %d control signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		nSig);

	DISPLAY_ALL_CTRL_SIG(i, nSig, pSig);

	return TRUE;
}


static BOOL Equip_DisplayAlarmSignals(IN EQUIP_INFO *pEquip, int nSigType)
{
	int nLevel, i;
	ALARM_SIG_VALUE		*pSig, *pHead;
	char	strTime[32];

	if (nSigType == SIG_TYPE_ALARM)
	{
		printf("\n\n%s(%d) detail alarm info:\n",
			EQP_GET_EQUIP_NAME0(pEquip),
			pEquip->iEquipID);

		pSig = pEquip->pAlarmSigValue;
		for (i = 0; i < pEquip->pStdEquip->iAlarmSigNum; i++, pSig++)
		{
			char			strTimeBegin[32]="-", strTimeEnd[32]="-";
	
			if (pSig->sv.tmStartTime > 0)
			{
				TimeToString(pSig->sv.tmStartTime, TIME_CHN_FMT,
					strTimeBegin, sizeof(strTimeBegin));
			}

			if (pSig->sv.tmEndTime > 0)
			{
				TimeToString(pSig->sv.tmEndTime, TIME_CHN_FMT, 
					strTimeEnd, sizeof(strTimeEnd));
			}

			// ID name x=Active/Disactive, Suppressed, start-end time delaying..
			printf("[%d] ID=%4d %-32s%c=%c %c, %-20s-%-20s delaying=%ds.\n",
				i+1,
				(pSig)->pStdSig->iSigID, 
				EQP_GET_SIGV_NAME0(pSig),
				SIG_VALUE_IS_VALID(pSig) ? ' ' : 'x',
				(pSig)->bv.varValue.lValue ? 'A' : 'D',
				SIG_VALUS_IS_SUPPRESSED(pSig) ? 'S' : ' ',
				strTimeBegin,
				strTimeEnd,
				pSig->sv.nDelayingTime);
		}

		printf("End of the %s(%d) detail alarm info.\n",
			EQP_GET_EQUIP_NAME0(pEquip),
			pEquip->iEquipID);
	}

	printf("\n\n%s(%d) has %d active alarms:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		pEquip->iAlarmCount[0]);
	
	// lock the equipment, must get the lock.
	if (Mutex_Lock(pEquip->hSyncLock, MAX_TIME_WAITING_EQUIP_LOCK) == ERR_MUTEX_OK)
	{			
		for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
		{
			pHead = &pEquip->pActiveAlarms[nLevel];
			pSig  = pHead->next;

			printf("\nLevel %d has %d active alarms:\n",
				nLevel, pEquip->iAlarmCount[nLevel]);

			// the link is a bilink.
			for (i = 1; pSig != pHead; pSig = pSig->next, i++)
			{
				printf("EQUIP %4d: %-32s start at %s, %s.\n",
					i, EQP_GET_SIGV_NAME0((pSig)),
					TimeToString(pSig->sv.tmStartTime, TIME_CHN_FMT,
						strTime, sizeof(strTime)),
					SIG_VALUS_IS_SUPPRESSED(pSig) ? "suppressed" : "active");

			}
		}

		Mutex_Unlock(pEquip->hSyncLock);
	}

	else
	{
		printf( "Fails on trying to lock equipment.\n");
	}

	printf("\n\n%s(%d) has %d delaying alarms:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		pEquip->nDelayingAlarms);

	pHead = &pEquip->pDelayingAlarms[0];
	pSig  = pHead->next;
	for (i = 1; pSig != pHead; pSig = pSig->next, i++)
	{
		printf("EQUIP %4d: %-32s is delaying, will active in %d seconds.\n",
			i, EQP_GET_SIGV_NAME0((pSig)),
			pSig->sv.nDelayingTime);
	}

	return TRUE;
}



static BOOL Equip_DisplaySignals(IN EQUIP_INFO *pEquip, IN int nSigType)
{
	printf("\n\nEquipment %s(%d) displaying signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID);

	if (nSigType >= 0)	// show all signal of the type
	{
		switch (nSigType)
		{
		case SIG_TYPE_SAMPLING:
			return Equip_DisplaySamplingSignals(pEquip);

		case SIG_TYPE_CONTROL:
			return Equip_DisplayControlSignals(pEquip);

		case SIG_TYPE_SETTING:
			return Equip_DisplaySettingSignals(pEquip);

		case SIG_TYPE_ALARM:
		case SIG_TYPE_ALARM+1:
			return Equip_DisplayAlarmSignals(pEquip, nSigType);
		}
		
		printf( "--->No such signal type %d.\n", nSigType);
		return FALSE;
	}

	Equip_DisplaySamplingSignals(pEquip);
	Equip_DisplaySettingSignals(pEquip);
	Equip_DisplayControlSignals(pEquip);
	Equip_DisplayAlarmSignals(pEquip, SIG_TYPE_ALARM);

	printf("Equipment %s(%d) end displaying signals.\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID);

	return TRUE;
}


static BOOL Site_DisplaySignals(IN SITE_INFO *pSite, IN int nSigType)
{
	int				i, nLevel;
	EQUIP_INFO		*pEquip;
	char			strTime[32];

	printf("\n\n\nSite has %d active alarms, %s:\n",
		pSite->iAlarmCount[0],
		TimeToString(time(NULL), TIME_CHN_FMT, strTime, sizeof(strTime)));
	
	for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		printf("EQUIP site Level %d has %d active alarms:\n",
			nLevel, pSite->iAlarmCount[nLevel]);
	}

	// proccess the equipment alarms if its data is changed
	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		Equip_DisplaySignals(pEquip, nSigType);
	}


	return TRUE;
}

// to query data
typedef BOOL (*EQUIP_SIG_QUERY_PROC)(IN SITE_INFO *pSite, IN char *pszArg);

typedef struct _EQUIP_SIG_QUERY_CMD
{
	int						nCmd;
	char					*pszPrompt;
	EQUIP_SIG_QUERY_PROC	pfnQuery;	
} QUERY_EQUIP_CMD;

#define DEF_EQUIP_SIG_QUERY_CMD(cmd, pmpt, query)	\
	{(int)(cmd), (pmpt), (EQUIP_SIG_QUERY_PROC)(query)}


static BOOL Site_QueryEquipSignal(IN SITE_INFO *pSite,
								  IN char *pArgs)
{
	EQUIP_INFO *pEquip;
	int			nEquipId = -1;
	int			nSigType = -1;
	int			nSigId   = -1;
	SAMPLE_SIG_VALUE *pSig;

	float		fNewValue = 0;
	int			nArgs;
	int			nSleep = 0;
	int			nRepeatCount = 1;
	float		fDeltaValue = 0;

	nArgs = sscanf(pArgs, "%d %d %d %f %d %d  %f", 
		&nEquipId, &nSigType, &nSigId, &fNewValue, &nSleep, 
		&nRepeatCount, &fDeltaValue);

	if (nArgs < 3)
	{
		printf( "--->Invalid args: %s\n", pArgs);
		return FALSE;
	}

	if (nEquipId < 0)	// show all equipment of the site
	{
		Site_DisplaySignals(pSite, nSigType);
		return TRUE;
	}

	pEquip = Init_GetEquipRefById(pSite, nEquipId);
	if (pEquip == NULL)
	{
		printf( "--->Invalid equipment ID: %d\n", nEquipId);
		return FALSE;
	}

	if (nSigId < 0)	// show all types of the equipment
	{
		Equip_DisplaySignals(pEquip, nSigType);
		return TRUE;
	}

	// show one signal
	pSig = (SAMPLE_SIG_VALUE*)Init_GetEquipSigRefById(pEquip, nSigType, nSigId);
	if (pSig == NULL)
	{
		printf( "--->No such signal type %d or ID %d in equipment %s(%d).\n",
			nSigType, nSigId,
			EQP_GET_EQUIP_NAME0(pEquip),
			pEquip->iEquipID);
		return FALSE;
	}

	printf("Equipment %s(%d) s/n: '%s', process-state: %d, displaying signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip), pEquip->iEquipID, pEquip->szSerialNumber,
		pEquip->iProcessingState);

	// alarm signal
	if (nSigType == SIG_TYPE_ALARM)
	{
		char			strTimeBegin[32]="-", strTimeEnd[32]="-";
		ALARM_SIG_VALUE	*pAlarmSig = (ALARM_SIG_VALUE *)pSig;

		if (pAlarmSig->sv.tmStartTime > 0)
		{
			TimeToString(pAlarmSig->sv.tmStartTime, TIME_CHN_FMT,
				strTimeBegin, sizeof(strTimeBegin));
		}

		if (pAlarmSig->sv.tmEndTime > 0)
		{
			TimeToString(pAlarmSig->sv.tmEndTime, TIME_CHN_FMT, 
				strTimeEnd, sizeof(strTimeEnd));
		}

		// ID name x=Active/Disactive, Suppressed, start-end time delaying..
		printf("ID=%4d %-32s%c=%c %c, %-20s-%-20s delaying=%ds.\n",
			(pAlarmSig)->pStdSig->iSigID, 
			EQP_GET_SIGV_NAME0(pAlarmSig),
			SIG_VALUE_IS_VALID(pAlarmSig) ? ' ' : 'x',
			(pAlarmSig)->bv.varValue.lValue ? 'A' : 'D',
			SIG_VALUS_IS_SUPPRESSED(pAlarmSig) ? 'S' : ' ',
			strTimeBegin,
			strTimeEnd,
			pAlarmSig->sv.nDelayingTime);

		if ((nArgs == 4) && ((int)fNewValue >= ALARM_LEVEL_NONE) && 
			((int)fNewValue < ALARM_LEVEL_MAX))// e 1 3 4 new level
		{
			EQUIP_PROCESS_CMD	cmd;

			printf("Set new alarm level from %d to %d.\n",
				pAlarmSig->pStdSig->iAlarmLevel, (int)fNewValue);

			pAlarmSig->pStdSig->iAlarmLevel = (int)fNewValue;

			// notify the equipment manager to process data
			cmd.nCmd		= PROCESS_CMD_ALARM_LEVEL_CHANGED;
			//(int)cmd.pEquip = pEquip->iEquipTypeID; // set the nStdEquipTypeID.
			
			Queue_Put(pSite->hqProcessCommand, &cmd, FALSE);
		}

		return TRUE;
	}


	// other signal
	if ((pSig)->bv.ucType == VAR_FLOAT)
	{
		printf("ID=%4d(SID=%2d,CH=%2d) %-32s%c= %1.3f(Raw=%1.3f) %-8s\n",
			(pSig)->pStdSig->iSigID, 
			pSig->sv.iSamplerID, GET_CHN(pSig->sv.iSamplerChannel),
			EQP_GET_SIGV_NAME0((pSig)),
			SIG_VALUE_IS_VALID(pSig) ? ' ' : 'x',
			(pSig)->bv.varValue.fValue,
			(pSig)->bv.fRawData,
			(pSig)->pStdSig->szSigUnit);
	}
	else
	{
		printf("ID=%4d(SID=%2d,CH=%2d) %-32s%c= %ld(Raw=%1.3f) %-8s\n",
			(pSig)->pStdSig->iSigID, 
			pSig->sv.iSamplerID, GET_CHN(pSig->sv.iSamplerChannel),
			EQP_GET_SIGV_NAME0((pSig)),
			SIG_VALUE_IS_VALID(pSig) ? ' ' : 'x',
			(pSig)->bv.varValue.lValue,
			(pSig)->bv.fRawData,
			(pSig)->pStdSig->szSigUnit);
	}

	while ((nArgs >= 4) && (nRepeatCount > 0))
	{
		int	nCtrlResult = -1;
		EQUIP_PROCESS_CMD	cmd;
		VAR_VALUE	var;

		if ((pSig)->bv.ucType == VAR_FLOAT)
		{
			var.fValue = fNewValue;
		}
		else
		{
			var.lValue = (int)fNewValue;
		}

		// send control cmd
		if ((nSigType != SIG_TYPE_SAMPLING) ||
			(pSig->sv.iSamplerChannel == VIRTUAL_INTERNAL_CHANNEL))
		{
			nCtrlResult = Equip_Control(
				SERVICE_OF_LOGIC_CONTROL, "Test", TRUE,
				nEquipId, nSigType,
				nSigId, &var, 5000);

			if (nCtrlResult == ERR_EQP_OK)
			{
				printf("Set new value %1.3f by sending ctrl cmd OK.\n",
					fNewValue);
			}
			else
			{
				printf("Fails on sending ctrl cmd(err:%d(%x)), to set the new "
					"value directly.\n", nCtrlResult, nCtrlResult);
			}
		}

		// set the value directly
		if (nCtrlResult != ERR_EQP_OK)
		{
			if (pSig->sv.iSamplerChannel >= 0)
			{
				pSig->bv.fRawData = fNewValue;
				SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID_RAW);
			}

			else if ((pSig)->bv.ucType == VAR_FLOAT)
			{
				(pSig)->bv.varValue.fValue = fNewValue;
			}
			else
			{
				(pSig)->bv.varValue.lValue = (int)fNewValue;
			}

			// set sig is valid
			SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID|VALUE_STATE_IS_CONFIGURED);

			printf( "Set signal value to %1.3f.\n",	fNewValue);

			// 2. notify equipment manager to process data
			// to notify the data processor
			cmd.nCmd   = PROCESS_CMD_SIG_DATA_CHANGED;
			cmd.pEquip = pEquip;
			Queue_Put(pSite->hqProcessCommand, &cmd, FALSE);
		}

		nRepeatCount--;	
		fNewValue += fDeltaValue;

		printf("Sleeping %d ms...%d time control left.\n", nSleep, nRepeatCount);
		Sleep((DWORD)nSleep);
	}

	return TRUE;
}


extern BOOL Sample_DisplayData(IN SAMPLER_INFO *pSampler);

/*==========================================================================*
 * FUNCTION : Sample_DisplayData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SAMPLER_INFO  *pSampler : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-19 13:39
 *==========================================================================*/
BOOL Sample_DisplayData(IN SAMPLER_INFO *pSampler)
{
	int		i;
	float	*p = pSampler->pChannelData;
	SAMPLE_SIG_VALUE	*pSig;
	char	strTime[32];

	printf("\n\n%s(%d) at %s, Queried Chn %d, Used Chn %d.\n"
		"Sampled Time(%s), elapsed %1.3fs, used %1.3fs. Comm Status %s(Err:%d). Channel Data:\n",
		EQP_GET_SAMPLER_NAME0(pSampler),
		pSampler->iSamplerID,
		pSampler->pPollingPort->szDeviceDesp,
		pSampler->nMaxQueriedChannel,
		pSampler->nMaxUsedChannel,
		TimeToString((time_t)pSampler->tmDataSampled, TIME_CHN_FMT,
			strTime, sizeof(strTime)),
		pSampler->fElapsedTime, pSampler->tmSampleUsed,
		(pSampler->bCommStatus ? "OK" : "FAILURE"),
		pSampler->nCommErrors);


#ifdef _USING_READ_METHOD
	for (i = 0; i < pSampler->nMaxQueriedChannel; i++, p++)
#else
	for (i = 0; i < pSampler->nMaxUsedChannel; i++, p++)
#endif //_USING_READ_METHOD
	{
		int n;
		for (n = 0; n < pSampler->nSigValues; n++)
		{
			pSig = (SAMPLE_SIG_VALUE *)pSampler->ppSigValues[n];
			if (pSig->sv.iSamplerChannel == i)
			{
				printf( "[%3d] %c= %9.3f [%3d]%-1s.\n",
					GET_CHN(pSig->sv.iSamplerChannel),
					SIG_STATE_TEST(pSig, VALUE_STATE_IS_VALID_RAW) ? ' ' : 'x',
					*p,
					pSig->pStdSig->iSigID,
					EQP_GET_SIGV_NAME0(pSig));
			}
		}
	}

	printf("%s(%d) end of displaying data.\n",
		EQP_GET_SAMPLER_NAME0(pSampler),
		pSampler->iSamplerID);

	return TRUE;

}


static BOOL Site_QuerySamplerSignal(IN SITE_INFO *pSite,
								  IN char *pArgs)
{
	SAMPLER_INFO	*pSampler;
	int				nSamplerID, nChannel;
	SAMPLE_SIG_VALUE	*pSig;
	char			strTime[32];
	int				nArgs, i;
	float			fNew = 0;

	nArgs = sscanf(pArgs, "%d %d %f", &nSamplerID, &nChannel, &fNew);

	if (nArgs < 2)
	{
		printf( "--->Invalid args: %s\n", pArgs);
		return FALSE;
	}

	pSampler = Init_GetSamplerRefById(pSite, nSamplerID);
	if (pSampler == NULL)
	{
		printf( "--->Invalid sampler ID: %d\n", nSamplerID);
		return FALSE;
	}


	if (nChannel < 0)	// to show all values
	{
		Sample_DisplayData(pSampler);
		return TRUE;
	}

	if (nChannel >= pSampler->nMaxUsedChannel)
	{
		printf( "--->Invalid sampler channel %d, the maximum allowed "
			"is %d in sampler %s(%d)\n",
			nChannel,
			pSampler->nMaxUsedChannel,
			EQP_GET_SAMPLER_NAME0(pSampler),
			pSampler->iSamplerID);

		return FALSE;
	}


	printf("\n\n%s(%d) at %s, Queried Chn %d, Used Chn %d.\n"
		"Sampled Time(%s), elapsed %1.3fs, used %1.3fs. Comm Status %s(Err:%d). Channel Data:\n",
		EQP_GET_SAMPLER_NAME0(pSampler),
		pSampler->iSamplerID,
		pSampler->pPollingPort->szDeviceDesp,
		pSampler->nMaxQueriedChannel,
		pSampler->nMaxUsedChannel,
		TimeToString((time_t)pSampler->tmDataSampled, TIME_CHN_FMT,
			strTime, sizeof(strTime)),
		pSampler->fElapsedTime, pSampler->tmSampleUsed,
		(pSampler->bCommStatus ? "OK" : "FAILURE"),
		pSampler->nCommErrors);

	for (i = 0; i < pSampler->nSigValues; i++)
	{
		pSig = (SAMPLE_SIG_VALUE *)pSampler->ppSigValues[i];
		if (GET_CHN(pSig->sv.iSamplerChannel) == nChannel)
		{
			printf( "--->Channel [%3d] %c= %9.3f   [%3d]%10s.\n",
				GET_CHN(pSig->sv.iSamplerChannel),
				SIG_STATE_TEST(pSig, VALUE_STATE_IS_VALID_RAW) ? ' ' : 'x',
				pSampler->pChannelData[nChannel],
				pSig->pStdSig->iSigID,
				EQP_GET_SIGV_NAME0(pSig));

			if (nArgs == 3)
			{
				pSig->bv.fRawData = fNew;
				SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID_RAW);
			}
		}
	}


	// set new value
	if (nArgs == 3)
	{
		printf("  Set to new channel value %1.3f.\n", fNew);
		pSampler->pChannelData[nChannel] = fNew;
	}

	// send the notification to the related equipment to process data
	for (i = 0; (i < pSampler->nRelatedEquipment) && (nArgs == 3); i++)
	{
		EQUIP_PROCESS_CMD	cmd;
		EQUIP_INFO			*pEquip;
	
		pEquip = pSampler->ppRelatedEquipment[i];

#ifdef _DEBUG_DATA_SAMPLING
		//TRACEX("Sampler %s(%d) is posting data processing msg to "
		//	"equipment %s(%d)...\n",
		//	EQP_GET_SAMPLER_NAME0(pSampler),
		//	pSampler->iSamplerID,
		//	EQP_GET_EQUIP_NAME0(pEquip),
		//	pEquip->iEquipID);
#endif //_DEBUG_DATA_SAMPLING

		// need process data - to notify the data processor
		cmd.nCmd   = PROCESS_CMD_SIG_DATA_CHANGED;
		cmd.pEquip = pEquip;
		Queue_Put(pSite->hqProcessCommand, &cmd, FALSE);
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Site_DisplayCtrlCmds
 * PURPOSE  : display the ctrl cmd state
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SITE_INFO  *pSite : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-12-05 11:06
 *==========================================================================*/
BOOL Site_DisplayCtrlCmds(IN SITE_INFO *pSite, IN char *pArgs)
{
	int						i;
	EQUIP_CTRL_CMD_ARRAY	*pCmdArray;
	EQUIP_CTRL_CMD			*pCmd;
	char					*pStateName[] =
	{
		"IDLE", "USED", "PENDING", "EXECUTING", "EXECUTED", "CANCELED", "END"
	};

	BOOL bShowAll = (pArgs[0] == 'a') ? TRUE : FALSE;

	pCmdArray = (EQUIP_CTRL_CMD_ARRAY *)pSite->hCmdArray;

	Mutex_Lock(pCmdArray->hSyncLock, WAIT_INFINITE);

	printf("\nThere are %d ctrl cmds are pending.\n",
		pCmdArray->nPendingCmds);

	pCmd = pCmdArray->pCmds;
	for (i = 0; i < MAX_ALLOWED_CTRL_CMDS; i++, pCmd++)
	{
		if (bShowAll || (pCmd->nState != CTRL_CMD_IS_IDLE))
		{
			printf("[%2d]: Seq:%u, Cmd:%s to %s(%2d), timeout:%dms, "
				"state: %s, result: %s.\n",
				i+1, (unsigned int)pCmd->dwCmdSeq, 
				pCmd->szCmdStr, 
				(pCmd->pExecutor != NULL) ? EQP_GET_SAMPLER_NAME0(pCmd->pExecutor): "NONE",
				(pCmd->pExecutor != NULL) ? pCmd->pExecutor->iSamplerID : -1,
				(int)pCmd->dwTimeout, 
				pStateName[pCmd->nState],
				pCmd->nResult == ERR_EQP_OK ? "OK" :  
				pCmd->nResult == ERR_EQP_TIMEOUT ? "TIMEOUT" :  
				pCmd->nResult == ERR_EQP_COMM_FAILURE ? "FAILURE" :  
				pCmd->nResult == ERR_EQP_INVALID_ARGS ? "INVALID ARGS" : "N/A"); 
		}
	}

	Mutex_Unlock(pCmdArray->hSyncLock);

	return TRUE;
}


static BOOL Site_DeleteStorage(IN SITE_INFO *pSite,
								  IN char *pArgs)
{
	BOOL	bOK;

	UNUSED(pSite);

	if (*pArgs == 0)
	{
		printf("The storage name is NOT specified.\n");
		return FALSE;
	}

	bOK = DAT_StorageDeleteRecord(pArgs);

	printf("The storage file %s is %s deleted.\n", 
		pArgs, (bOK ? "successfully" : "NOT"));

	return TRUE;
}


static BOOL Site_ListApplicationService(IN SITE_INFO *pSite,
								  IN char *pArgs)
{
	int				i = 0;
	APP_SERVICE		*pService;

	UNUSED(pSite);
	UNUSED(pArgs);

	printf("The application services info:\n");

	do
	{
		pService = ServiceManager_GetService(SERVICE_GET_BY_IDX, i);
		if (pService != NULL)
		{
			i++;
			printf("%d: %-30s : %-30s : %08x : %s\n",
				i, pService->szServiceName, 
				pService->szServiceLib,
				pService->hServiceThread,
				THREAD_IS_RUNNING(pService->hServiceThread) ? "Running" : "exited");
		}		
	} while (pService != NULL); 
	
	printf("Total %d application services listed.\n", i);

	return TRUE;
}


#ifdef _DEBUG
static BOOL Site_ShowMemoryUsage(IN SITE_INFO *pSite,
								  IN char *pArgs)
{
	int		nBlock, nId;
	size_t  ulBytes;

	UNUSED(pSite);
	UNUSED(pArgs);

	nBlock = MEM_GET_INFO(&nId, &ulBytes);
	printf("Time: %1.3f -- Total using %d mem blocks, %u bytes, "
		"%d NEW/RENEW calls.\n",
		GetCurrentTime(),
		nBlock, ulBytes, nId);

	return TRUE;
}
#endif


static void Site_QueryHelp(QUERY_EQUIP_CMD *pCmds, int nCmds)
{
	int i;

	printf( "\nQuery the equipment signal and sampler raw data.\n\n");

	for (i = 0; i < nCmds; i++)
	{
		printf("   %c: %s\n", pCmds[i].nCmd, pCmds[i].pszPrompt);
	}

	printf("\n");
}


// get rid of the blank char and CR in the str
static char *StripString(IN char *pStr)
{
	extern char *Cfg_RemoveWhiteSpace(IN char *pszBuf);
	char *p;
	char *pCR;

	p   = Cfg_RemoveWhiteSpace(pStr);
	pCR = strrchr(p, '\n');

	if (pCR != NULL)
	{
		*pCR = 0;
	}


	return p;
}


BOOL Site_DisplayCtrlCmds(IN SITE_INFO *pSite, IN char *pArgs);
void Main_HandleStopSignals(IN int nSig);

static void *Site_QueryEquip(IN SITE_INFO *pSite)
{
	int		i;
	char	szCmd[256], *pszCmd, szLastCmd[256];
	BOOL	bQuit = FALSE;

	QUERY_EQUIP_CMD		*pCmd, *pLastCmd;
	QUERY_EQUIP_CMD		cmds[] =
	{
		DEF_EQUIP_SIG_QUERY_CMD('e',
			"Get/set signal info. Format is: \n"
			"\t e equip_id sig_type sig_id new_value sleep_ms repeat_count delta_value.\n"
			"\t if equip_id = -1 to show all data of the site.\n"
			"\t if sig_type = -1 to show all data of the equipment.\n"
			"\t if sig_id   = -1 to show all data of the type.\n"
			"\t if is alarm, the new_value is the new alarm level.",
			Site_QueryEquipSignal),

		DEF_EQUIP_SIG_QUERY_CMD('s',
			"Get/set raw data of sampler. Format is:s sampler_id chn_id new_value.\n"
			"\t if chn_id = -1 to show all data of the sampler.",
			Site_QuerySamplerSignal),

		DEF_EQUIP_SIG_QUERY_CMD('c',
			"Display the info of the ctrl cmd queue. Format:c [all].",
			Site_DisplayCtrlCmds),

		DEF_EQUIP_SIG_QUERY_CMD('d',
			"Delete storage log data. Format is: d log_name(*.* for all).",
			Site_DeleteStorage),

		DEF_EQUIP_SIG_QUERY_CMD('l',
			"List the status of application services. Format is: l .",
			Site_ListApplicationService),

#ifdef _DEBUG
		DEF_EQUIP_SIG_QUERY_CMD('m',
			"Show mem usage. Format is: m .",
			Site_ShowMemoryUsage),
#endif

		DEF_EQUIP_SIG_QUERY_CMD('h',
			"Show this msg.",
			NULL),

		DEF_EQUIP_SIG_QUERY_CMD('x',
			"Exit the SCU plus system.",
			NULL)
	};

	// set the thread is cancelable
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	//set the thread can be canceled immediately
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	Site_QueryHelp(cmds, ITEM_OF(cmds));

	pLastCmd     = NULL;
	szLastCmd[0] = 0;

	while(!bQuit)
	{
		printf( ">");	
		fgets(szCmd, sizeof(szCmd), stdin);
		pszCmd = StripString(szCmd);

		pCmd = NULL;
		if (*pszCmd != 0)
		{
			for (i = 0; i < ITEM_OF(cmds); i++)
			{
				if (cmds[i].nCmd == pszCmd[0])
				{
					pCmd = &cmds[i];

					pLastCmd = pCmd;
					strncpyz(szLastCmd, pszCmd, sizeof(szLastCmd));
					break;
				}
			}
		}
		else
		{
			pCmd   = pLastCmd;
			pszCmd = szLastCmd;
		}

		if (pCmd != NULL)
		{
			if (pCmd->pfnQuery != NULL)
			{
				if (!pCmd->pfnQuery(pSite, &pszCmd[2]))
				{
					pLastCmd     = NULL;
					szLastCmd[0] = 0;
				}
			}
			else if (pCmd->nCmd == 'x')
			{
				printf("Quit...\n");
				bQuit = TRUE;
				Main_HandleStopSignals(SIGQUIT);	Sleep(500);
				Main_HandleStopSignals(SIGQUIT);
			}
			else if (pCmd->nCmd == 'h')
			{
				Site_QueryHelp(cmds, ITEM_OF(cmds));
			}
		}
		else if (*pszCmd != 0)
		{
			printf( "-->Invliad Command: %s\n", pszCmd);

			pLastCmd     = NULL;
			szLastCmd[0] = 0;
		}
		else 
		{
			//printf( "-->Press h for help.\n");
			Sleep(2000);
		}
	}

	return NULL;
}


static pthread_t	s_hQueryTask = 0;
BOOL	Site_CreateQueryEquipTask(IN SITE_INFO *pSite)
{
#ifndef _DEBUG	// always start this task in debug version.
	char	*pszEnable = getenv("ACU_DEBUG_EQUIP_TASK");

	if ((pszEnable == NULL) || (stricmp(pszEnable, "yes") != 0))
	{
		return TRUE;	// this function is disabled.
	}
#endif

	printf("============================================================\n");
	printf("=             Equip Data Debugger is Running               =\n");
	printf("= <Unset the environment SCUP_DEBUG_EQUIP_TASK to disable> =\n");
	printf("============================================================\n");

	if (pthread_create( &s_hQueryTask, 
		NULL,//&attr
		(PTHREAD_START_ROUTINE)Site_QueryEquip,
		(void *)pSite) != 0)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL	Site_StopQueryEquipTask(IN SITE_INFO *pSite)
{
	UNUSED(pSite);

	if (s_hQueryTask != 0)
	{
		pthread_cancel(s_hQueryTask);
		s_hQueryTask = 0;
	}

	return TRUE;
}
#endif

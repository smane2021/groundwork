#include "stdsys.h"
#include "public.h"


#include "splash_main.h"

static BOOL Splash_InitScreen(void);
static void Splash_DestroyScreen(void);
static BOOL Splash_ShowBmpScreen(char *pszBmpFileName);

static	char			*s_pszLangCode= "en";	// ascii language type, only support ENGLISH
static  int				s_nFontHeight= BASE_HEIGHT*2;
static  int				s_nFontWidth = BASE_HEIGHT;

// atfer 10 min, the splash shall auto-exit.
#define MAX_ALLOWED_RUN_TIME		(3*60*1000)	//ms. max 1min.
#define SPLASH_CHECK_PERIOD			100


#include <sys/time.h>
#include <sys/resource.h>
#include <sched.h>
//int setpriority(int which, int who, int prio); 
//int sched_setscheduler(pid_t pid, int policy, const struct sched_param *param);

/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SERVICE_ARGUMENTS *  pArgs : 
 * RETURN   : DWORD : service exit code, defined in app_service.h
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                   DATE: 2005-03-06 11:30
 *==========================================================================*/
DWORD App_ServiceMain(SERVICE_ARGUMENTS *pArgs)
{
	int			i, nRunningTime = 0;
	HANDLE		hSelf = RunThread_GetId(NULL);
	BOOL		bShowNormal = TRUE;
	UCHAR		byKeyValue;
	int			nKeydownTime = 0;

	DWORD		dwVer = GET_APP_VER();

	char		*ppBmpFiles[] = {"enp_logo.bmp"};
	int			nBmpFiles     = ITEM_OF(ppBmpFiles);
	
	int			iDispCount = 0;
	int			iDotStartX = 0;
	int			iDotStartY = 0;
	const char	szaDispDotList[][5] = {
		"  ",
		". ",
		"..",
	};
	
//Frank Wu,20160127, for MiniNCU
#if 0
	char		ppszSplashText[][17]= 
	{
  	  //1234567890ABCDEF
		"Advanced Control",
		"Unit Plus",
		//"Next Genaration",
		//"Controler",
		"V1.00",
		"Starting...",
	};

#define VER_LOC		2
#ifdef MK_VAR_FOR_FB
#define VER_FMT		"V%d.%03d"
#else
#define VER_FMT		"V%d.%02d"
#endif
#else
	char		ppszSplashText[][17]= 
	{
		//1234567890ABCDEF
		"MiniController",
		//"Unit Plus",
		//"Next Genaration",
		//"Controler",
		"V1.00",
		//"Starting...",
	};

#define VER_LOC		1
#ifdef MK_VAR_FOR_FB
#define VER_FMT		"V%d.%03d"
#else
#define VER_FMT		"V%s"
#endif

#endif
	UNUSED(pArgs);

	TRACEX("LCD splash is starting...\n");

	if (!Splash_InitScreen())
	{
		return SERVICE_EXIT_FAIL;
	}

	// string ver
//	snprintf(ppszSplashText[VER_LOC], sizeof(ppszSplashText[VER_LOC]), 
//		VER_FMT,
//		GET_APP_MAJOR_VER(dwVer), GET_APP_MINOR_VER(dwVer));
	char szTempBuf[30];
	
	memset(szTempBuf, 0, sizeof(szTempBuf));
	STRING_APP_VER(szTempBuf, sizeof(szTempBuf));
	snprintf(ppszSplashText[VER_LOC], sizeof(ppszSplashText[VER_LOC]), 
		VER_FMT,
		szTempBuf);
	
	//struct sched_param sp = {-20};
	//if(sched_setscheduler(0, SCHED_FIFO, &sp) < 0)
	//{
	//	printf("sched_setscheduler failed\n");
	//}
	
	//if(setpriority(PRIO_PROCESS, 0, -20) < 0)
	//{
	//	printf("setpriority failed\n");
	//}
	//ClearScreen();

	AlarmLedCtrl(LED_YEL, FALSE);
	AlarmLedCtrl(LED_RED, FALSE);
	BuzzerBeep(FALSE);

	for(i = 0; i < 5; i++)
	{
		GetKey();
		Sleep(200);
	}

	for (i = 0; i < ITEM_OF(ppszSplashText); i++)
	{
		int		nLen = strlen(ppszSplashText[i]);

		DisString(ppszSplashText[i], nLen,
			(SCREEN_WIDTH - nLen*s_nFontWidth) / 2,				// center text
			i*s_nFontHeight + TOP_BLANK_HEIGHT_OF_SCREEN,		// y .
			s_pszLangCode, 
			FLAG_DIS_NORMAL_TYPE);
	}

	
	while ((pArgs->nQuitCommand == SERVICE_CONTINUE_RUN) &&
		(nRunningTime < MAX_ALLOWED_RUN_TIME))
	{
		RunThread_Heartbeat(hSelf);
	
		if ((nRunningTime % 1500) == 0)
		{
#if 0
			if (bShowNormal)
			{
				ClearScreen();

				for (i = 0; i < ITEM_OF(ppszSplashText); i++)
				{
					int		nLen = strlen(ppszSplashText[i]);

					DisString(ppszSplashText[i], nLen,
						(SCREEN_WIDTH - nLen*s_nFontWidth) / 2,				// center text
						i*s_nFontHeight + TOP_BLANK_HEIGHT_OF_SCREEN,		// y .
						s_pszLangCode, 
						FLAG_DIS_NORMAL_TYPE);
				}
			}
			else if (nBmpFiles > 0)
			{
				Splash_ShowBmpScreen(ppBmpFiles[(nRunningTime/1000)%nBmpFiles]);
			}
#else
			//display progress dots
			iDispCount %= ITEM_OF(szaDispDotList);
			
			iDotStartX = (strlen(ppszSplashText[VER_LOC])*s_nFontWidth + SCREEN_WIDTH)/2 + 1;
			iDotStartY = 16;

			DisString(szaDispDotList[iDispCount], strlen(szaDispDotList[iDispCount]),
				iDotStartX,				// center text
				iDotStartY,		// y .
				s_pszLangCode, 
				FLAG_DIS_NORMAL_TYPE);
				
			iDispCount++;
#endif
			bShowNormal = !bShowNormal;
			
			AlarmLedCtrl(LED_YEL, bShowNormal);
			AlarmLedCtrl(LED_RED, !bShowNormal);
		}

		// check key
		byKeyValue = GetKey();
		if(byKeyValue != VK_NO_KEY)
		{
			BuzzerBeep(TRUE);
			nKeydownTime = nRunningTime;
		}
		else if ((nRunningTime - nKeydownTime) >= 200)
		{
			BuzzerBeep(FALSE);
			nKeydownTime = 0;
		}

		// mainloop,,,
		Sleep(SPLASH_CHECK_PERIOD);
		nRunningTime += SPLASH_CHECK_PERIOD;
	}

	AlarmLedCtrl(LED_YEL, FALSE);
	AlarmLedCtrl(LED_RED, FALSE);
	BuzzerBeep(FALSE);

	Splash_DestroyScreen();
	TRACEX("LCD splash  exited.\n");

	return SERVICE_EXIT_OK;
}


//stub. not need in splash, but needed by liblcd_ui_base.so
//BOOL SetBuzzerBeepThredID(HANDLE hThreadID)
//{
//	UNUSED(hThreadID);
//	return TRUE;
//}


/*==========================================================================*
 * FUNCTION : Splash_InitScreen
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-06 12:54
 *==========================================================================*/
static BOOL Splash_InitScreen(void)
{
	FONT_SIZE		font = {8, 16, 32};

	//1. init LCD and clear LCD
	if (InitLCD() != ERR_LCD_OK)
	{
		AppLogOut(LCD_SPLASH, APP_LOG_WARNING,
			"Fails on initializing LCD.\n",
			s_pszLangCode);
		return FALSE;
	}

	Light(TRUE);  

	//ClearScreen();

	//2. load ASCII lib
	if (RegisterFontLib(1, &s_pszLangCode, FLAG_REGIST_FONT_LIB) != ERR_LCD_OK)
	{
		AppLogOut(LCD_SPLASH, APP_LOG_WARNING,
			"Fails on loading font of lannguage code '%s'.\n",
			s_pszLangCode);

		return FALSE;
	}

	if (GetFontSize(&font, s_pszLangCode) == ERR_LCD_OK)
	{
		s_nFontHeight = font.byFontHEIGHT;//GetFontHeight(s_pszLangCode);
		s_nFontWidth  = font.byFontWidth;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Splash_DestroyScreen
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-06 12:54
 *==========================================================================*/
static void Splash_DestroyScreen(void)
{
	//ClearScreen(); -- Do NOT clear, keep the last screen.
	//Light(FALSE);
	RegisterFontLib(1, &s_pszLangCode, FLAG_UNREGIST_FONT_LIB);//2. load ASCII lib
	CloseLcdDriver();
}



/*==========================================================================*
 * FUNCTION : Splash_ShowBmpScreen
 * PURPOSE  : Display the welcome screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszBmpFileName : 
 * RETURN   : BOOL : true for OK
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:02
 *==========================================================================*/
static BOOL Splash_ShowBmpScreen(char *pszBmpFileName)
{
	UNUSED(pszBmpFileName);

	unsigned char pBmpImg[]=
	{
		/*------------------------------------------------------------------------------
		The bitmap displays the Vertivv icon
		------------------------------------------------------------------------------*/
#if 0
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0xB0,0xD8,0x98,0x20,0xC0,
		0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x80,0xC0,0xF8,0xFC,0xBF,0xBC,0x79,0xFB,0x75,0xFB,0xF6,
		0xFD,0xF3,0xE6,0xEC,0xD8,0xE0,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x38,0x7C,0xFF,0xBF,0x7B,0xFB,0x7F,0xFF,0xF7,0xFF,0xFF,0xEF,0xFF,
		0xDE,0xFF,0x7D,0x3F,0x0F,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x06,0x0C,0x19,0x36,0x4D,0x9E,0xFC,0x79,0x1F,
		0x05,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
#endif
#if 1
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFC,0xFC,0xFC,0xFC,0x9C,
		0x9C,0x9C,0x9C,0x9C,0x9C,0x00,0x00,0xFC,0xFC,0xFC,0xFC,0xF8,0xF0,0xC0,0x80,0x00,
		0x00,0xC0,0xE0,0xF8,0xFC,0xFC,0xFC,0xFC,0x00,0x00,0xFC,0xFC,0xFC,0xFC,0x9C,0x9C,
		0x9C,0x9C,0x9C,0x9C,0x00,0x00,0xFC,0xFC,0xFC,0xFC,0x1C,0x1C,0x9D,0x9D,0xFC,0xF8,
		0xF8,0xF0,0x00,0x00,0xF0,0xF8,0xF8,0xFC,0xDC,0x9C,0x9C,0x9C,0x1C,0x1C,0x00,0x00,
		0xC0,0xF0,0xF8,0xF8,0x7C,0x1C,0x1C,0x0C,0x1C,0x3C,0xF8,0xF8,0xF0,0xE0,0x00,0x00,
		0xFC,0xFC,0xFC,0xFC,0xF8,0xF0,0xC0,0x80,0x00,0x00,0xFC,0xFC,0xFC,0xFC,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7F,0x7F,0x7F,0x7F,0x73,
		0x73,0x73,0x73,0x73,0x73,0x00,0x00,0x7F,0x7F,0x7F,0x7F,0x01,0x03,0x0F,0x1F,0x7E,
		0x3F,0x0F,0x03,0x01,0x7F,0x7F,0x7F,0x7F,0x00,0x00,0x7F,0x7F,0x7F,0x7F,0x73,0x73,
		0x73,0x73,0x73,0x73,0x00,0x00,0x7F,0x7F,0x7F,0x7F,0x03,0x03,0x0F,0x1F,0x3F,0x7D,
		0x79,0x70,0x60,0x00,0x70,0x71,0x71,0x73,0x73,0x73,0x73,0x7F,0x7F,0x3F,0x1E,0x00,
		0x0F,0x1F,0x3F,0x3F,0x7C,0x70,0x60,0x60,0x70,0x78,0x7F,0x3F,0x1F,0x0F,0x00,0x00,
		0x7F,0x7F,0x7F,0x7F,0x01,0x03,0x07,0x0F,0x1F,0x7E,0x7F,0x7F,0x7F,0x7F,0x00,0x08,
		0x78,0x08,0x70,0x08,0x78,0x08,0x78,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0xF8,0xF8,0x38,0xF0,0xC0,0x00,0xF8,0xF8,0x00,0xE0,0x60,
		0x60,0xE0,0x80,0x60,0xF8,0xF8,0x60,0x20,0xE0,0x00,0xC0,0xE0,0xC0,0x00,0xE0,0x60,
		0x00,0xC0,0xE0,0x60,0xE0,0x80,0x00,0xE0,0xE0,0x60,0x40,0xFC,0xFC,0x80,0xE0,0x60,
		0x20,0x00,0x00,0xF8,0xF8,0x98,0x98,0xD8,0x70,0x00,0xC0,0x60,0x60,0xC0,0x00,0x60,
		0xE0,0x00,0xC0,0xE0,0xC0,0x00,0xE0,0x60,0x00,0xE0,0xE0,0xA0,0xE0,0x00,0xE0,0xE0,
		0x60,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x0F,0x0F,0x00,0x00,0x03,0x07,0x0F,0x0F,0x00,0x07,0x0D,
		0x0D,0x0D,0x01,0x00,0x0F,0x0F,0x0C,0x00,0x07,0x0F,0x0F,0x01,0x0F,0x0F,0x07,0x00,
		0x03,0x07,0x0E,0x0C,0x07,0x03,0x00,0x0F,0x0F,0x00,0x00,0x0F,0x0F,0x03,0x07,0x0E,
		0x0C,0x00,0x00,0x0F,0x0F,0x01,0x01,0x01,0x00,0x03,0x07,0x0C,0x0C,0x07,0x03,0x00,
		0x07,0x0F,0x0F,0x01,0x0F,0x0F,0x07,0x00,0x07,0x07,0x0D,0x0D,0x05,0x00,0x0F,0x0F,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
#endif
	};

	return (LCD_DisplayBmp(0, TOP_BLANK_HEIGHT_OF_SCREEN, pBmpImg, ITEM_OF(pBmpImg)) == ERR_LCD_OK);
}




/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : splash_main.h
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-11 15:04
 *  VERSION  : V1.00
 *  PURPOSE  : parse the cmd line
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _LCD_SPLASH_H_DEF
#define _LCD_SPLASH_H_DEF

#include "../main/main.h"

#define LCD_SPLASH "ACU_SPLASH"
DWORD App_ServiceMain(SERVICE_ARGUMENTS *pArgs);
#endif


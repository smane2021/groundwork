/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : debug_s_p.c
 *  CREATOR  : Mao Fuhua(Frank)                 DATE: 2004-11-10 14:17
 *  VERSION  : V1.00
 *  PURPOSE  : Debug Service Program. It will run by itself.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include <signal.h>
#include "stdsys.h"
#include "public.h"

#include "splash_main.h"

extern int	SPLASH_ProcessCmdLine(IN int argc, IN char *argv[]);

#ifdef _DEBUG
//#define _DEBUG_SPLASH	1
#endif //_DEBUG

//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO  g_SiteInfo;

//need to be defined in Miscellaneous Function Module
TIME_SRV_INFO g_sTimeSrvInfo;

//need to be defined in equip management Module

int Equip_Control(IN int nSenderType, IN char *pszSenderName,
				  IN int nSendDirectly,
				  IN int nEquipID, IN int nSigType, IN int nSigID, 
				  IN VAR_VALUE *pCtrlValue, IN DWORD dwTimeout)
{
	UNUSED(nSenderType);
	UNUSED(pszSenderName);
	UNUSED(nSendDirectly);
	UNUSED(nEquipID);
	UNUSED(nSigType);
	UNUSED(nSigID);
	UNUSED(pCtrlValue);
	UNUSED(dwTimeout);
	return 0;
}


//need to be defined in Miscellaneous Function Module
int UpdateSCUPTime(time_t* pTimeRet)
{
	ASSERT(pTimeRet);

	TRACE("\n***UpdateSCUPTime in splash_main\n");

	return stime(pTimeRet);		//2.Update OS time
}

int UpdateNTPTime(void)
{
	return 1;
}

//STUB: used in libapp.so, but NOT need in this APP.mfh
int WriteRunConfigItem(RUN_CONFIG_ITEM* pRunConfigItem)
{
	UNUSED(pRunConfigItem);
	return 0;
}

char g_szACUConfigDir[MAX_FILE_PATH];
SERVICE_MANAGER		g_ServiceManager;


//stub
BOOL SetBuzzerBeepThredID(HANDLE hThreadID)
{
	UNUSED(hThreadID);
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Splash------show the splash screen in lcd.
///////////////////////////////////////////////////////////////////////////////

// the init and exit modules shall be called in main.
///////////////////////////////////////////////////////////////////////////////
static void SPLASH_ExitAbnormally(int iRunningState);


#define SPLASH_IS_INITIALIZING		0
#define SPLASH_IS_RUNNING			1
#define SPLASH_IS_EXITING			2

#define SPLASH_IS_INTERRUPTED		-1	// killed/stopped by user
#define SPLASH_IS_KILLED			-2	// 10 Ctrl-C will kill the system.
#define SPLASH_IS_ABNORMAL			-3	// abnormal for threads
#define SPLASH_IS_FAULT			-4

static int s_iSystemRunningState = SPLASH_IS_RUNNING;
static int s_iSystemExitingCode  = 0;


// acu starting splash define. maofuhua, 2005-3-6
static 	APP_SERVICE s_AcuSplash = DEF_APP_SERVICE("ACU+ Splash",		"lib_acu_splash.so",		
		SERVICE_OF_MISC,	FALSE,  SERVICE_RUN_AS_TASK);

#ifdef _USES_APP_SERVICE_METHOD
#define STOP_SPLASH_TASK()
#else
#define STOP_SPLASH_TASK()	(s_AcuSplash.args.nQuitCommand = SERVICE_STOP_RUNNING)
#endif

#ifdef _USES_APP_SERVICE_METHOD
static SYS_INIT_EXIT_MODULE	s_InitExitModule[] =
{
	DEF_INIT_EXIT_MODULE("ACU+ Splash",				FALSE,	ERR_OK,		
						Service_Start,				&s_AcuSplash,	NULL,
						Service_Stop,				&s_AcuSplash,	500),
};


/*==========================================================================*
 * FUNCTION : SPLASH_InitModules
 * PURPOSE  : init the modules by normal sequence.
 * CALLS    : 
 * CALLED BY: SPLASH_Init
 * ARGUMENTS:   void : 
 * RETURN   : static int : if any mandatory module fails on init, 
 *                         return its errcode, or return OK.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-12-03 11:13
 *==========================================================================*/
static int SPLASH_InitModules(void)
{
	int						i;
	BOOL					bResult;
	SYS_INIT_EXIT_MODULE	*pModule;
	int						iResult;

	iResult = ERR_OK;

	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "Initializing system modules...\n");

	for (i = 1; i <= ITEM_OF(s_InitExitModule); i++)
	{
		pModule = &s_InitExitModule[i-1];

		if (pModule->pfnInit != NULL)
		{
			AppLogOut(LCD_SPLASH, APP_LOG_INFO, "%d: Initializing %s...\n",
				i, pModule->pszModuleName);

			bResult = pModule->pfnInit(
				pModule->pInitArgs[0],
				pModule->pInitArgs[1]);

			AppLogOut(LCD_SPLASH, 
				bResult ? APP_LOG_INFO : APP_LOG_ERROR, 
				"%d: %s is %s successfully initialized.\n",
				i, pModule->pszModuleName,
				bResult ? "now" : "NOT");

			if (!bResult && pModule->bMandatoryModule) //maofuhua added the last cond.
			{
				iResult = ERR_DBS_FAIL;
				break;
			}
		}
	}

	AppLogOut(LCD_SPLASH, (iResult == ERR_OK) ? APP_LOG_INFO : APP_LOG_ERROR,
		"Debug service modules are %s successully initialized.\n",
		(iResult == ERR_OK) ? "now" : "NOT");

	return iResult;
}


/*==========================================================================*
 * FUNCTION : SPLASH_DestroyModules
 * PURPOSE  : Destroy the module by reverse sequence.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : always OK
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-12-03 11:15
 *==========================================================================*/
static int SPLASH_DestroyModules(void)
{
	int						i;
	SYS_INIT_EXIT_MODULE	*pModule;

	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "Destroying system modules...\n");

	// call destroy function by reverse sequence
	for (i = ITEM_OF(s_InitExitModule); i > 0; i--)
	{
		pModule = &s_InitExitModule[i-1];

		if (pModule->pfnExit != NULL)
		{
			AppLogOut(LCD_SPLASH, APP_LOG_INFO, "%d: Destroying %s...\n",
				i, pModule->pszModuleName);

			pModule->pfnExit(
				pModule->pExitArgs[0],
				pModule->pExitArgs[1]);

			AppLogOut(LCD_SPLASH, APP_LOG_INFO, "%d: %s is successfully destroyed.\n",
				i, pModule->pszModuleName);
		}
	}

	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "Splash modules are destroyed.\n");

	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : SPLASH_RunThreadEventHandler
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN DWORD      dwThreadEvent  : //	the thread event. below.
 *            IN HANDLE     hThread        : //	The thread id
 *            IN const char *pszThreadName : 
 * RETURN   : static DWORD : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 20:18
 *==========================================================================*/
static DWORD SPLASH_RunThreadEventHandler(
	IN DWORD	dwThreadEvent,
	IN HANDLE	hThread,
	IN const char *pszThreadName)
{
	UNUSED(pszThreadName);

	WatchDog_Feed();

	if (THREAD_EVENT_NO_RESPONSE == dwThreadEvent)
	{
		AppLogOut( LCD_SPLASH, APP_LOG_ERROR,
			"Thread %s(%p) is no response, it will be killed, "
			"and the debug service will be restarted!\n",
			pszThreadName, hThread);

		s_iSystemRunningState = SPLASH_IS_EXITING;
		s_iSystemExitingCode  = SYS_EXIT_BY_THREAD;

		//continue this thread, but we need quit system
		//return THREAD_CONTINUE_THIS;
	}
	else if (THREAD_EVENT_NO_RESPONSE == dwThreadEvent)
	{
		AppLogOut( LCD_SPLASH, APP_LOG_ERROR,
			"Thread %s(%p) is no response, it will be killed, "
			"and the system will be restarted!\n",
			pszThreadName, hThread);

		s_iSystemRunningState = SPLASH_IS_ABNORMAL;

		//continue this thread, but we need quit system
		//return THREAD_CONTINUE_THIS;
	}

	return THREAD_CONTINUE_RUN;
}
#endif


/*==========================================================================*
 * FUNCTION : SPLASH_HandleStopSignals
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-18 13:32
 *==========================================================================*/
static void SPLASH_HandleStopSignals(void)
{
	static int		iKillCount = 0;
	static time_t	tmLastSignaled = 0;
	time_t			tmNow = time(NULL);

	if (tmNow - tmLastSignaled > 10)	// 10 seconds
	{
		iKillCount = 0;
	}

	tmLastSignaled = tmNow;

	iKillCount++;

	switch (iKillCount)
	{
	case 1:
#define SYS_PREQUIT_MSG	"Received a quit command, please send "\
			"command again to stop system.\n"
		AppLogOut(LCD_SPLASH, APP_LOG_WARNING, "%s", SYS_PREQUIT_MSG);
		//printf("%s", SYS_PREQUIT_MSG);

		break;

	case 2:
#define SYS_INTR_MSG	"Received two quit commands, system is now quiting...\n"
		AppLogOut(LCD_SPLASH, APP_LOG_WARNING, "%s", SYS_INTR_MSG);

		//printf("%s", SYS_INTR_MSG);
		STOP_SPLASH_TASK();
		s_iSystemRunningState = SPLASH_IS_EXITING;
		s_iSystemExitingCode  = SYS_EXIT_BY_USER;

		break;

	default:
		STOP_SPLASH_TASK();
		s_iSystemRunningState = SPLASH_IS_EXITING;
		s_iSystemExitingCode  = SYS_EXIT_BY_USER;
		printf( "ACU splash is quiting, please wait... \n" );
	}
}



/*==========================================================================*
 * FUNCTION : SPLASH_SignalHandler
 * PURPOSE  : 
 * CALLS    : SPLASH_ExitAbnormally,SPLASH_HandleStopSignals
 * CALLED BY: SPLASH_InstallSigHandler
 * ARGUMENTS: IN int  nSig : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)                 DATE: 2005-01-11 09:38
 *==========================================================================*/
static void SPLASH_SignalHandler(IN int iSig)
{
	TRACEX("Received a signal #%d.\n", iSig);

	switch (iSig)
	{
	case SIGSEGV:
		SPLASH_ExitAbnormally(s_iSystemRunningState);	// never returns
		s_iSystemRunningState = SPLASH_IS_FAULT;	
		break;

	case SIGTERM:	
	case SIGKILL:
	case SIGQUIT:
		SPLASH_HandleStopSignals();
		break;

	case SIGINT:	// CTRL+C, the number of signal will be sent to app 
					// when ctrl+c is pressed in ACU board, ignore it.
	case SIGPIPE:	// pipe broken, continue to run
	default:
		break;
	}
}

/*==========================================================================*
 * FUNCTION : SPLASH_InstallSigHandler
 * PURPOSE  : Define signal and install signal with signal process functions
 * CALLS    : SPLASH_SignalHandler
 * CALLED BY: 
 * ARGUMENTS: BOOL  bInstall : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-12-06 10:37
 *==========================================================================*/
static BOOL SPLASH_InstallSigHandler(BOOL bInstall)
{
	int	i;
	int pHandledSignals[] = 
	{	
		SIGTERM, SIGINT, SIGKILL, SIGQUIT, // Kill signal or Ctrl+C
		SIGPIPE,							// broken pipe.
		SIGSEGV,							// segment fault error.
	};

	//int pIgnoredSignals[] =
	//{
	//	SIGINT,
	//}

	UNUSED(bInstall);

	// 0. init msg handler.
	for (i = 0; i < ITEM_OF(pHandledSignals); i++)
	{
		signal(pHandledSignals[i], SPLASH_SignalHandler);
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : SPLASH_CheckRootDir
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszExecName : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)                 DATE: 2005-01-12 11:44
 *==========================================================================*/
static void SPLASH_CheckRootDir(char *pszExecName)
{
    char *pszConfigEnv = getenv(ENV_ACU_ROOT_DIR);

	if (pszConfigEnv == NULL)
	{
		static char	szEnvRootDir[MAX_FILE_PATH];	// must be static.
		char	szRootDir[MAX_FILE_PATH], *pExecPath;

		pExecPath = strrchr(pszExecName, '/');
		if (pExecPath != NULL)
		{
			// add one extra room for '\0'
			size_t		len = (pExecPath-pszExecName)+1;

			// copy the dir in the exec file. 
			strncpyz(szRootDir, pszExecName, (int)MIN(len, sizeof(szRootDir)));
		}
		else
		{
			strcpy(szRootDir, ".");
		}

		printf("Please set the environment variable '%s' of ACU root dir.!\n"
			"The default dir \"%s\" will be used.\n",
			ENV_ACU_ROOT_DIR,
			szRootDir);

		snprintf(szEnvRootDir, sizeof(szEnvRootDir), "%s=%s",
			ENV_ACU_ROOT_DIR, szRootDir);
	
		putenv(szEnvRootDir);	// set back the env.

		TRACEX("ENV is: %s. got env is %s\n",
			szEnvRootDir, getenv(ENV_ACU_ROOT_DIR));

		ASSERT(strcmp(szRootDir, getenv(ENV_ACU_ROOT_DIR)) == 0);
	}
}


/*==========================================================================*
 * FUNCTION : SPLASH_Init
 * PURPOSE  : init services and everythings
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN MAIN_ARGS  *THIS : 
 * RETURN   : static int : ERR_OK for OK
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 20:22
 *==========================================================================*/
static int SPLASH_Init(void)
{
	int		iResult = ERR_OK;

	s_iSystemRunningState = SPLASH_IS_INITIALIZING;

	//{{add the LCD displaying function. 128*64pix=16*4chars(8x16)
	//       ACU  Maintenance 
	//}}

	// 1. starting msg.
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "+-------------------------------------+\n");
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "|   Debug Service now is starting...  |\n");
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "+-------------------------------------+\n");

	/*
	if (!WatchDog_Open())
	{
		AppLogOut(LCD_SPLASH, APP_LOG_ERROR,
			"Fails on opening and enabling the watch-dog.\n");
	}
	*/   //it is moved to main function


	// 0. init msg handler.in order to process signal during running
	SPLASH_InstallSigHandler(TRUE);

#ifdef _USES_APP_SERVICE_METHOD
	// 2. Init thread mamager
	iResult = RunThread_ManagerInit(SPLASH_RunThreadEventHandler);	// must be at 1st

	// 3. init sys modules.
	iResult = SPLASH_InitModules();
#endif

	return iResult;
}



/*==========================================================================*
 * FUNCTION : SPLASH_Run
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   static int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)                 DATE: 2005-01-10 10:58
 *==========================================================================*/
static int SPLASH_Run(void)
{
	s_iSystemRunningState = SPLASH_IS_RUNNING;

	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "+-------------------------------------+\n");
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "|   Splash Program now is running...  |\n");
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "+-------------------------------------+\n");

	// running
#ifdef _USES_APP_SERVICE_METHOD
	while (s_iSystemRunningState == SPLASH_IS_RUNNING)
	{
		WatchDog_Feed();

		Sleep(50);		// sleep 1 second.
	}
#else
	{
		extern DWORD App_ServiceMain(SERVICE_ARGUMENTS *pArgs);

		s_AcuSplash.args.nQuitCommand = SERVICE_CONTINUE_RUN;
		App_ServiceMain(&s_AcuSplash.args);
	}
#endif

	return s_iSystemExitingCode;
}




/*==========================================================================*
 * FUNCTION : SPLASH_Exit
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  nCurrentStatus : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)                 DATE: 2005-01-10 11:03
 *==========================================================================*/
static int SPLASH_Exit(IN int iCurrentStatus)
{
	//s_iSystemRunningState = SYS_IS_EXITING;	// other value is already sett!
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "+-------------------------------------+\n");
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "|   Splash program now is quiting...  |\n");
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "+-------------------------------------+\n");

#ifdef _USES_APP_SERVICE_METHOD
	// destroy sys modules
	SPLASH_DestroyModules();

	// stop the  thread manager.
	RunThread_ManagerExit(10000, TRUE);	// must be at last
#endif

	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "+----------------------------------+\n");
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "|   Splash program exited %s  |\n",
		(iCurrentStatus == SPLASH_IS_INTERRUPTED) ? "normally.  " : "abnormally." );
	AppLogOut(LCD_SPLASH, APP_LOG_INFO, "+----------------------------------+\n\n\n");

	
	return iCurrentStatus;
}

/*==========================================================================*
 * FUNCTION : SPLASH_ExitAbnormally
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nRunningState : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-20 19:50
 *==========================================================================*/
static void SPLASH_ExitAbnormally(int iRunningState)
{
	char	*pStates[] = {"initializing", "running", "exiting"};

	if ((iRunningState < 0) || (iRunningState > ITEM_OF(pStates)))
	{
		iRunningState = SPLASH_IS_EXITING;
	}

	AppLogOut(LCD_SPLASH, APP_LOG_ERROR, 
		"PANIC: System received SIGSEGV(segment fault) signal "
		"while %s, system exit.\n",
		pStates[iRunningState]);

	WatchDog_Close(FALSE);	// do NOT disable dog

	exit(-1);	// do NOT cleanup if fails exit().
	_exit(-1);
}


/*==========================================================================*
 * FUNCTION : main
 * PURPOSE  : This is the debug service program main function, it will be
 *			  used by itself.Use the debug_maintn.so
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)                 DATE: 2004-11-10 14:26
 *==========================================================================*/
int main(int argc, char *argv[])
{
	int iExitStatus;
	extern BOOL g_bEnableAppLogOut;	// defined in applog.c

#ifndef _HAS_LCD_UI
	return 0;
#endif

	// to avoid conflicting appout with main app, disable logout in splash
#ifdef _DEBUG
	g_bEnableAppLogOut = FALSE;
#else
	g_bEnableAppLogOut = FALSE;
#endif

	//if (!WatchDog_Open())
	//{
	//	AppLogOut(LCD_SPLASH, APP_LOG_ERROR,
	//		"Debug Service starting. Fails on opening and enabling the watch-dog.\n");
	//}

	SPLASH_CheckRootDir(argv[0]);

	//{{ parse the command line. lixidong added. 2005-3-8
	if (SPLASH_ProcessCmdLine(argc, argv) == -1)
	{
		// return -1 when show help only.
		WatchDog_Close(TRUE);
		return 0;
	}
	//}}
	
	// 1. init
	iExitStatus = SPLASH_Init();

	if (iExitStatus != ERR_OK)
	{
		AppLogOut(LCD_SPLASH, APP_LOG_ERROR,
			"Init system got error code %d(%x), system exit.\n",
			iExitStatus, iExitStatus);

		iExitStatus = SPLASH_IS_ABNORMAL;
	}

	// 2. If succeed to initiate, run clear watch_dog
	else 
	{
		iExitStatus = SPLASH_Run();
	}

	// 3. cleanup
	SPLASH_Exit(iExitStatus);

	// disable and close watch dog
	WatchDog_Close(TRUE);	// always close WatchDog

	return iExitStatus;
}
////////////////////
////////////////////


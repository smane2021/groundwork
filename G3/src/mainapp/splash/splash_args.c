/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : splash_args.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-11 15:04
 *  VERSION  : V1.00
 *  PURPOSE  : parse the cmd line
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "splash_main.h"


typedef BOOL	(*CMD_OPTION_PROC)(char *pszOption);

struct _CMD_LINE_PARSER
{
	char				*pszOptName;
	CMD_OPTION_PROC		pfnParser;
	char				*pszOptHelp;
};

typedef struct _CMD_LINE_PARSER		CMD_LINE_PARSER;

#define _DEF_CMD_LINE_PARSER(op, parser, hlp)	\
	{ (op), (CMD_OPTION_PROC)(parser), (hlp)}
 

// the functions to process cmd line args
static BOOL Main_CreatePidFile(IN char *pszPidFile)
{
	FILE	*fp = fopen(pszPidFile,"w+t");

	if (fp == NULL)
	{
		return FALSE;
	}

	fprintf(fp, "%d\n", getpid());
	fclose(fp);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : SPLASH_ProcessCmdLine
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int   argc    : 
 *            char  *argv[] : the format of args shall be:
*               -opt_name=opt_value
 * RETURN   : static int : -1: for show help, others: ERR_OK.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-02-27 14:53
 *==========================================================================*/
int	SPLASH_ProcessCmdLine(IN int argc, IN char *argv[])
{
	int					i, n, iLenOpt;
	CMD_LINE_PARSER		cmdLine[] =
	{
		_DEF_CMD_LINE_PARSER("-pid=", Main_CreatePidFile, 
			"Set pid file used by stopacu. Format: -pid=pid_file_name"),
	};

	// help
	if ((argc > 1) && (strcmp(argv[1], "-?") == 0))
	{
		printf("The supported cmd line options of ACU are:\n");

		for (n = 0; n < ITEM_OF(cmdLine); n++)
		{
			printf("\t%d: %s.\n", (n+1), cmdLine[n].pszOptHelp);
		}

		return -1;
	}

	// parse options
	for (i = 1; i < argc; i++)
	{
		for (n = 0; n < ITEM_OF(cmdLine); n++)
		{
			iLenOpt = strlen(cmdLine[n].pszOptName);
			ASSERT(iLenOpt > 0);

			if (strncmp(argv[i], cmdLine[n].pszOptName, (size_t)iLenOpt) ==0)
			{
				if(!cmdLine[n].pfnParser(&argv[i][iLenOpt]))
				{
					AppLogOut(SYS_MAIN, APP_LOG_WARNING,
						"Error on processing command arg: %s.", argv[i]);
					return -1;	// can NOT continue;
				}

				break;
			}
		}

		if (n == ITEM_OF(cmdLine))
		{
			AppLogOut(SYS_MAIN, APP_LOG_WARNING,
				"Unknown command arg: %s.", argv[i]);
		}
	}

	return ERR_OK;
}


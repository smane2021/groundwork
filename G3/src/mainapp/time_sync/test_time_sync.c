/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : test_time_sync.c
*  CREATOR  : HULONGWEN                DATE: 2004-09-30 17:11
*  VERSION  : V1.00
*  PURPOSE  : To test time_sync.so
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>

#include "time_sync.h"


TIME_SRV_INFO g_sTimeSrvInfo;


void TestTimeSync(void)
{
	printf("filename: test_time_sync.c [TestTimeSync]\r\n");

	printf("Time sync sub module self_text start!\r\n");
	printf("please press ENTER key!\r\n");

	getchar();

	InitTimeSync();
	
}

void InitTimeSyncInfo(void)
{
	printf("filename: test_time_sync.c [InitTimeSyncInfo]\r\n");

	g_sTimeSrvInfo.ulMainTmSrvAddr = inet_addr("142.100.7.8");

	g_sTimeSrvInfo.ulBakTmSrvAddr = inet_addr("142.100.7.6");

	g_sTimeSrvInfo.dwJustTimeInterval = 1; //1 min	

}



/*==========================================================================*
* FUNCTION : main
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int    argc    : 
*            INT char  *argv[] : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-30 16:38
*==========================================================================*/
int main(IN int argc, IN char *argv[])
{
	UNUSED(argc);
	printf("Test start!\r\n");

	InitTimeSyncInfo();

	if(argv[1])
	{
		if(strcmp(argv[1], "-time") == 0)
		{
			TestTimeSync();
		}
	}

	else
	{
		TestTimeSync();

	}

	for(;;);

	return 0;
}

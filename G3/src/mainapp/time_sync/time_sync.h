/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : time_sync.h
*  CREATOR  : HULONGWEN                DATE: 2004-09-29 10:05
*  VERSION  : V1.00
*  PURPOSE  : The declaration file of time synchronization sub module
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __TIME_SYNC_H__040929
#define __TIME_SYNC_H__040929


#define TIME_SERVER_PORT	37

#define MSG_SEND_STR "what time is it?\0\n"

#define UNIXEPOCH 2208988800   //UNIX epoch, in UCT secs

//Sync interval from time server
#define _JUST_TIME_FROM_SERVER_DEFAULT_INTERVAL	(1*60)	//one hour

//Sync interval from SCU
#define _JUST_TIME_FROM_SCU_INTERVAL			(24*60)	//one day


//Init and destroy time sync sub module
BOOL InitTimeSync(void);
int DestroyTimeSync(void);

//Update ACU time, include linux sys time and SCU time
int UpdateSCUPTime(time_t* pTimeRet);
int UpdateNTPTime(void);
int my_system(const char* cmdstring);

#endif //__TIME_SYNC_H__040929



/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : time_sync.c
*  CREATOR  : HULONGWEN                DATE: 2004-09-29 10:05
*  VERSION  : V1.00
*  PURPOSE  : The implementation file of time synchronization sub module
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/resource.h>		/* setrlimit */
#include <sys/types.h>			/* socket, bind, accept */
#include <sys/socket.h>			/* socket, bind, accept, setsockopt, */
#include <syslog.h>
#include <netdb.h>
#include <sys/wait.h>

#include "time_sync.h"
//#include "ntp-4.2.2p1/ntpdate.h"

#define	RTC_DEV_NAME			"/dev/rtc0"
#define	READ_RTC_FAILED			-1
#define	READ_RTC_SUCCESSFUL		0


TIME_SRV_INFO g_sTimeSrvInfo;  //It's used to save the time server info

HANDLE g_hThreadTimeSync = NULL;

static DWORD TimeSyncFunc(void* pArgs);
static int AdjustTimeFromServer(int *pnAdjustTmFromServer);
static int SyncTimeFromServer(u_long ipTimeSvr);
static int SyncTimeFromServerNTP(u_long ipTimeSvr);
static int SyncTimeFromRTC(void);
static BOOL NeedRefreshSysTime(time_t* pSetTime);
static int GetRTCTime(time_t *piTime);
static int AdjustTimeFromServerAction(void);

int nAdjustTmFromServer = 0;

int my_system(const char* cmdstring)
{
    pid_t pid1;
    int istatus;
    if((pid1 = vfork()) < 0)
    {
	printf("vfork fail\n");
	istatus = -1;
    }
    else if(pid1 == 0)
    {
	printf("vfork success\n");
	execl("/bin/sh", "sh", "-c", cmdstring, (char *)0);
	_exit(127);
    }
    else
    {
	while(waitpid(pid1, &istatus, 0) < 0)
	{
	    if(errno != EINTR)
	    {
		istatus = -1;
		break;
	    }
	}
    }
    printf("istatus is %d\n", istatus);

    if(istatus == -1)
    {
	printf("Error is %s\n", strerror(errno));
    }
    return istatus;
}


static BOOL ReadTimeSyncConfig(void)
{
    int bRetFlag = TRUE;

    //by HULONGWEN read time sync config from FLASH
    g_sTimeSrvInfo.ulSrvPort = TIME_SERVER_PORT;//37	

    SIG_BASIC_VALUE* pSigValue;
    int nBufLen;

    if (DxiGetData(VAR_A_SIGNAL_VALUE,
	DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
	SIG_ID_TIME_SYNC_SVR_IP,
	&nBufLen,			
	&pSigValue,			
	0) == ERR_DXI_OK)
    {	
	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{
	    g_sTimeSrvInfo.ulMainTmSrvAddr = pSigValue->varValue.ulValue;
	}
	else
	{
	    g_sTimeSrvInfo.ulMainTmSrvAddr = 0;
	}

    }
    else
    {
	g_sTimeSrvInfo.ulMainTmSrvAddr = 0;

	bRetFlag = FALSE;
    }

    if (DxiGetData(VAR_A_SIGNAL_VALUE,
	DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
	SIG_ID_TIME_SYNC_BACKUP_IP,
	&nBufLen,			
	&pSigValue,			
	0) == ERR_DXI_OK)
    {	
	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{
	    g_sTimeSrvInfo.ulBakTmSrvAddr = pSigValue->varValue.ulValue;
	}
	else
	{
	    g_sTimeSrvInfo.ulBakTmSrvAddr = 0;
	}

    }
    else
    {
	g_sTimeSrvInfo.ulBakTmSrvAddr = 0;

	bRetFlag = FALSE;
    }

if (DxiGetData(VAR_A_SIGNAL_VALUE,
	DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
	SIG_ID_TIME_SYNC_TIMEZONE,
	&nBufLen,			
	&pSigValue,			
	0) == ERR_DXI_OK)
    {	
	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{
	    g_sTimeSrvInfo.lTimeZone = pSigValue->varValue.lValue;
	}
	else
	{
	    g_sTimeSrvInfo.lTimeZone = 0;
	}

    }
    else
    {
	g_sTimeSrvInfo.lTimeZone = 0;

	bRetFlag = FALSE;
    }

    if (DxiGetData(VAR_A_SIGNAL_VALUE,
	DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
	SIG_ID_TIME_SYNC_INTERVAL,
	&nBufLen,			
	&pSigValue,			
	0) == ERR_DXI_OK)
    {	
	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{
	    g_sTimeSrvInfo.dwJustTimeInterval = pSigValue->varValue.ulValue;
	}
	else
	{
	    g_sTimeSrvInfo.dwJustTimeInterval = 60; //60 min
	}

    }
    else
    {
	g_sTimeSrvInfo.dwJustTimeInterval = 60; //60 min
	bRetFlag = FALSE;
    }	


    return bRetFlag;
}

//Init time sync sub module
// changed the return type from int to BOOL, maofuhua, 2005-2-25
BOOL InitTimeSync(void)
{
    //	TRACE("filename: time_sync.c [InitTimeSync]\r\n");


    ReadTimeSyncConfig();

	//Delete for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	//This operation is time costly, call in main loop
    //At first, sync time from RTC
    //SyncTimeFromRTC();



    g_hThreadTimeSync = RunThread_Create("TIME_SYNC",
	TimeSyncFunc,
	(void*)NULL,
	(DWORD*)NULL,
	0);

    if (g_hThreadTimeSync == NULL)
    {

	TRACE("filename: time_sync.c [InitTimeSync]\
	      create time sync thread failed\n\r");

	AppLogOut(USER_MANAGE_LOG, APP_LOG_ERROR, 
	    "[%s]--InitUserManage: ERROR: "
	    "write user info to flash mutex create failed!\n\r", __FILE__);


	return FALSE;
    }

    //	TRACE("Create time sync thread sucessfully\r\n");

    return TRUE;			    
}

int DestroyTimeSync(void)
{
    //Stop the time sync thread
    RunThread_Stop(g_hThreadTimeSync,
	1000,//1s
	TRUE);

    return 0;
}


/*==========================================================================*
* FUNCTION : AdjustTimeFromRTC
* PURPOSE  : Adjust time from SCU
* CALLS    : SyncTimeFromRTC
* CALLED BY: TimeSyncFunc
* ARGUMENTS: int  *piAdjustTmFromRTC : The var for taking count
* RETURN   : int : int : 0, success 1, failure
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-30 11:35
*==========================================================================*/
static int AdjustTimeFromRTC()
{
    static	int		s_iAdjustTmFromRTC = 0;
    //TRACE("filename: time_sync.c [AdjustTimeFromRTC]\r\n");

    //ASSERT(piAdjustTmFromRTC);

    s_iAdjustTmFromRTC++;

    if (s_iAdjustTmFromRTC >= (int)(g_sTimeSrvInfo.dwJustTimeInterval))
    {
	s_iAdjustTmFromRTC = 0;

	if (SyncTimeFromRTC() == READ_RTC_SUCCESSFUL)
	{
	    return READ_RTC_SUCCESSFUL;
	}
	else
	{
	    TRACE("*****Failed to read RTC time!!!!\n");
	}
    }

    return 1;
}

/*==========================================================================*
* FUNCTION : UpdateNTPTime
* PURPOSE  : Adjust time from NTP
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : int : int : 0, success 2, failure
* COMMENTS : 
* CREATOR  : Song Xu                DATE: 2018-02-03 11:35
*==========================================================================*/
int UpdateNTPTime(void)
{
	int iNTPResult;
	//timer clear
	nAdjustTmFromServer = 0;

	iNTPResult = AdjustTimeFromServerAction();
	
	if (iNTPResult == 2)//sync time from server failure
	{
		AdjustTimeFromRTC(); 
	}

	return iNTPResult;
}

#define		NTP_FUN_ENABLE	1
#define		SYSTEM_EQUIP_ID	1
//The main time sync function
static DWORD TimeSyncFunc(void* pArgs)
{

    //int nAdjustTmFromSCU = 0;	
	int		iError = 0;
	int	iBufLen = 0;
	int	iTimeOut = 0;
	SIG_BASIC_VALUE* pNTPFunSigValue;
    HANDLE hSelf = NULL;

    BOOL bNeedSyncTimeFromRTC = TRUE;

    int nRetSyncTimeFromServer = 1;

    UNUSED(pArgs);
	
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				SYSTEM_EQUIP_ID, 	
				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 701),		
				&iBufLen,			
				(void *)&pNTPFunSigValue,			
				iTimeOut);
	if(iError != ERR_DXI_OK || pNTPFunSigValue == NULL)
	{
		
		AppLogOut("NTP SERVIVE", APP_LOG_ERROR,
			"Thread NTP Service was end because enable signal failed.\n");
		return 0;
	}

	hSelf = RunThread_GetId(NULL);

	nAdjustTmFromServer = g_sTimeSrvInfo.dwJustTimeInterval;//run the function at the first time.
	while (THREAD_IS_RUNNING(hSelf))
	{
		if(pNTPFunSigValue->varValue.enumValue == NTP_FUN_ENABLE)
		{
			if(nAdjustTmFromServer >= (int)(g_sTimeSrvInfo.dwJustTimeInterval))
			{
				UpdateNTPTime();
			}
			nAdjustTmFromServer++;
		}
		Sleep(1 * 60 * 1000); //one minute

		//Feed dog, To tell the thread manager I'm living.
		RunThread_Heartbeat(hSelf);	
    }

    return 0;
}


/*==========================================================================*
* FUNCTION : AdjustTimeFromServer
* PURPOSE  : Adjust time from time server
* CALLS    : SyncTimeFromServer
* CALLED BY: TimeSyncFunc
* ARGUMENTS: int  *pnAdjustTmFromServer : the var for taking count
* RETURN   : int : 0, success 1, failure
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-30 11:29
*==========================================================================*/
static int AdjustTimeFromServer(int *pnAdjustTmFromServer)
{
    static u_long ipTimeSvr1 = 0;
    static u_long ipTimeSvr2 = 0;
    int bTimeSvrChanged = 0;

    //	TRACE("filename: time_sync.c [AdjustTimeFromServer]\r\n");

    ASSERT(pnAdjustTmFromServer);

    /* test whether time svrs are changed */

    if (ipTimeSvr1 != (u_long)g_sTimeSrvInfo.ulMainTmSrvAddr)
    {
		ipTimeSvr1 = g_sTimeSrvInfo.ulMainTmSrvAddr;

		if (ipTimeSvr1 != 0)
		{
			bTimeSvrChanged = 1;
		}

    }

    if (ipTimeSvr2 != (u_long)g_sTimeSrvInfo.ulBakTmSrvAddr)
    {
		ipTimeSvr2 = g_sTimeSrvInfo.ulBakTmSrvAddr;


		if (ipTimeSvr2 != 0)
		{
			bTimeSvrChanged = 1;
		}
    }

    //	Mutex_Unlock(g_hMutexSetTimeSrv);


    (*pnAdjustTmFromServer) ++;

    if ((bTimeSvrChanged) ||   /* svr ip changed, or */
	(*pnAdjustTmFromServer >= (int)(g_sTimeSrvInfo.dwJustTimeInterval))) /* it's time to sync */
    {
	*pnAdjustTmFromServer = 0;

	/* sync time */
	if ((ipTimeSvr1 != 0) && (SyncTimeFromServerNTP(ipTimeSvr1) == 0))
	{
	    //    printf("ipTimeSvr1 = %x\n", ipTimeSvr1);
	    /*TRACE("Sync time from time server1 %s ok\r\n", 
	    inet_ntoa(*((struct in_addr*)(&ipTimeSvr1))));*/

	    return 0; /* ok */
	}

	else if ((ipTimeSvr2 != 0) && (SyncTimeFromServerNTP(ipTimeSvr2) == 0))
	{
	    //    printf("ipTimeSvr2 = %x\n", ipTimeSvr2);
	    /*TRACE("Sync time from time server2 %s ok\r\n", 
	    inet_ntoa(*((struct in_addr*)(&ipTimeSvr2))));*/

	    return 0; /* ok */
	}

	return 2;//sync time from server failure
    }

    return 1;
}

static int AdjustTimeFromServerAction(void)
{
	u_long ipTimeSvr1 = 0;
	u_long ipTimeSvr2 = 0;

	ipTimeSvr1 = g_sTimeSrvInfo.ulMainTmSrvAddr;
	ipTimeSvr2 = g_sTimeSrvInfo.ulBakTmSrvAddr;

	/* sync time */
	if ((ipTimeSvr1 != 0) && (SyncTimeFromServerNTP(ipTimeSvr1) == 0))
	{
		return 0; /* ok */
	}
	else if ((ipTimeSvr2 != 0) && (SyncTimeFromServerNTP(ipTimeSvr2) == 0))
	{
		return 0; /* ok */
	}

	return 2;//sync time from server failure
}

void ipToString(char *string, u_long ipTimeSvr)
{
    BYTE bipbyte[4];

    bipbyte[0] = ipTimeSvr & 0xFF;
    bipbyte[1] = (ipTimeSvr & 0xFF00) >> 8;
    bipbyte[2] = (ipTimeSvr & 0xFF0000) >> 16;
    bipbyte[3] = (ipTimeSvr & 0xFF000000) >> 24;

    sprintf(string, "%d.%d.%d.%d", bipbyte[0], bipbyte[1], bipbyte[2], bipbyte[3]);
    return;
}

//Use the NTP to set the controller time
//By Zhaozicheng
static int SyncTimeFromServerNTP(u_long ipTimeSvr)
{
	char szIP[32];
	char szCommand[42];
	int istatus = -1;
	time_t the_time;
	struct tm *local_Time;
	char szTime[64];
	char szCommand1[128];

	//    printf("ipTimeSvr = %x\n", ipTimeSvr);
	ipToString(szIP, ipTimeSvr);
	sprintf(szCommand, "/app/ntpdate -d %s", szIP);
	//printf("%s\n", szCommand);
	//istatus = system(szCommand);
	istatus = _SYSTEM(szCommand);
	//printf("istatus is %d,%d,%d\n", istatus, WIFEXITED(istatus), WEXITSTATUS(istatus));
	if((istatus != -1) && (WIFEXITED(istatus) == 1) && (0 == WEXITSTATUS(istatus)))
	{//synchronization success
		time(&the_time);
		//printf("the_time is %ld\n", the_time);
		//printf("lTimeZone is %ld\n", g_sTimeSrvInfo.lTimeZone);
		the_time = the_time - (time_t)g_sTimeSrvInfo.lTimeZone;
		//printf("the_time is %ld\n", the_time);
		local_Time = gmtime(&the_time);
		strftime(szTime, sizeof(szTime), TIME_CHN_FMT, local_Time);
		sprintf(szCommand1, "date \"%s\"", szTime);
		//printf("%s\n", szCommand1);
	//	system(szCommand1);
	//	system("hwclock -w");
		_SYSTEM(szCommand1);
		_SYSTEM("hwclock -w");
		return(0);
	}
	else
	{
		return(1);
	}
}


/*==========================================================================*
* FUNCTION : SyncTimeFromServer
* PURPOSE  : Sync time form time server
* CALLS    : UpdateSCUPTime
* CALLED BY: AdjustTimeFromServer
* ARGUMENTS: u_long  ipTimeSvr : The time server IP
* RETURN   : int : 0,sucess 1,failure
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-30 11:33
*==========================================================================*/
static int SyncTimeFromServer(u_long ipTimeSvr)
{
    int         s;
    int         rc = 1;
    time_t     tNow;

    int			sin_len;

    struct sockaddr_in address;
    int bNonBlock = 1;

    //TRACE("filename: time_sync.c [SyncTimeFromServer]\r\n");


    bzero(&address,sizeof(address));

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = ipTimeSvr;
    address.sin_port = htons(TIME_SERVER_PORT);

    s = socket(AF_INET, SOCK_DGRAM, 0);

    if (s == -1)
    {
	printf("[SyncTimeFromServer] -- Connect to time server fail.\r\n");
	return 1;
    }

    if (((!bNonBlock) ||
	(ioctl(s,FIONBIO,(char *)&bNonBlock) == 0)))
    { 
    }
    else
    {
	printf("ioctl() error: %d\r\n", errno);

	close(s);
	return 1;
    }


    if (sendto(s, MSG_SEND_STR, sizeof(MSG_SEND_STR), 0,
	(struct sockaddr*)&address , sizeof(address)) != sizeof(MSG_SEND_STR))
    {
	/*TRACE("[SyncTimeFromServer] -- send request to time server %s fail.\r\n",
	inet_ntoa(*(struct in_addr*)&ipTimeSvr));*/

	close(s);
	return 1;
    }

    sin_len = sizeof(address);

    // first package incoming timetout.
    rc = WaitFiledReadable(s, 5000);

    if (rc <= 0)
    {
	/* 0: wait time out, -1: socket error on waiting    */
	/*
	* NOTE!: if socket is closed by peer, rc will be 1, 
	*        and the next recv will return 0!
	*/
	close(s);
	return 1;
    }

    if (recvfrom(s, (char *)&tNow, sizeof(time_t), 0,
	(struct sockaddr*)&address , &sin_len) != sizeof(time_t))
    {
	/*	TRACE("[SyncTimeFromServer] -- recv response from time server %s fail.\r\n",
	inet_ntoa(*(struct in_addr*)&ipTimeSvr));*/

	close(s);
	return 1;
    }	

    /* ok, save time */

    tNow = ntohl(tNow);
    if (tNow > (time_t)UNIXEPOCH)
    {
	tNow -= UNIXEPOCH;  /*adjust time from 1900/1/1 to 1970/1/1 */
	if (tNow != 0)
	{
	    //		TRACE("The time is %s", ctime(&tNow));

	    if (NeedRefreshSysTime(&tNow))
	    {
		UpdateSCUPTime((time_t*)(&tNow));
	    }
	}		

    }


    close(s);

    return 0;
}


/*==========================================================================*
* FUNCTION : NeedRefreshSysTime
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: time_t*  pSetTime : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : LXDlxd1                  DATE: 2006-06-15 11:06
*==========================================================================*/
static BOOL NeedRefreshSysTime(time_t* pSetTime)
{
    time_t curTime;

    ASSERT(pSetTime);

    time(&curTime);

    if (abs(curTime - *pSetTime) > 10)// if bigger than 10s
    {
	return TRUE;
    }
    else
    {
	return FALSE;
    }
}


static int SetTimeThruDXI(time_t * pTimeSet)
{
    VAR_VALUE_EX	varValueEx;

    ASSERT(pTimeSet);

    memset(&varValueEx, 0, sizeof(varValueEx));

    varValueEx.nSendDirectly = EQUIP_CTRL_SEND_URGENTLY;

    varValueEx.nSenderType = SERVICE_OF_LOGIC_CONTROL;
    varValueEx.pszSenderName = "";

    varValueEx.varValue.dtValue = *pTimeSet;

    if (DxiSetData(VAR_A_SIGNAL_VALUE,
	DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
	SIG_ID_SYSTEM_TIME,		
	sizeof(VAR_VALUE_EX),			
	&(varValueEx),			
	0) == ERR_DXI_OK)
    {
	return 0;
    }
    else
    {	
	return 1;
    }
}

static int SyncTimeFromRTC(void)
{	
    time_t RTCTime1, RTCTime2;
    int i;

    for(i = 0; i < 10; i++)
    {
	//system("hwclock -s");
	_SYSTEM("hwclock -s");
	RTCTime1 = time(NULL);

	//system("hwclock -s");
	_SYSTEM("hwclock -s");
	RTCTime2 = time(NULL);


	//TRACE("RTCTime2 = %d, RTCTime2 = %d\n", RTCTime2, RTCTime2);
	if(abs(RTCTime2 - RTCTime1) <= 1)
	{
	    TRACE("---------------------Sync OK----------------------\n");
	    return 0;//sucessfullly
	}		
    }
    TRACE("---------------------Sync Fail----------------------\n");
    return 1;//Failure
}


int UpdateSCUPTime(time_t* pTimeRet)
{
	ASSERT(pTimeRet);
	char szTime[64];
	char szCommand[128];

	TimeToString(*pTimeRet, TIME_CHN_FMT, 
		szTime, sizeof(szTime));

	TRACE("\n***UpdateSCUPTime in time_sync\n");
	//SetTimeThruDXI(pTimeRet);	//1.Update to hardware


	sprintf(szCommand, "date \"%s\"", szTime);
	TRACE("\n***UpdateSCUPTime in time_sync %s\n", szCommand);
	//printf("%s\n", szCommand);
	//system(szCommand);
	//system("hwclock -w");
	_SYSTEM(szCommand);
	_SYSTEM("hwclock -w");

    return stime(pTimeRet);		//2.Update OS time

}

/*==========================================================================*
* FUNCTION : GetTimeFromRTC
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  time_t *piTime 
* RETURN   :  -1: failed, 0 Successful
* COMMENTS : 
* CREATOR  : Frank Cao                 DATE: 2007-06-21 10:16
*==========================================================================*/
static int GetRTCTime(time_t *piTime)
{
    int			iRTC, iRst;
    struct tm	rtime;

    TRACE("----------------GetRTCTime(time_t *piTime)----------------------------\n");
    iRTC = open(RTC_DEV_NAME, O_RDWR);	
    TRACE("----------------GetRTCTime(time_t *piTime)----------iRTC = %d------------------\n", iRTC);
    if (iRTC < 0)
    {
	TRACE("Can't open the RTC_DEV_NAME\n");
	return READ_RTC_FAILED;
    }

    //get time
    memset(&rtime, 0, sizeof(rtime));
    //read(iRTC, &rtime, sizeof(rtime));
    ioctl(iRTC, RTC_RD_TIME, &rtime);
    TRACE("--------------%d-%d-%d %d-%d-%d----------read------------------\n",rtime.tm_year,rtime.tm_mon,rtime.tm_mday);
    //In rtc, time format is different with struct tm.
    rtime.tm_year -= 1900;

    TRACE("----------------GetRTCTime(time_t *piTime)----------read2------------------\n");
    rtime.tm_mon -= 1;

    *piTime = mktime(&rtime);
    TRACE("----------------GetRTCTime(time_t *piTime)----------read----------%d--------\n", *piTime);
    if(*piTime >= 0)
    {
	iRst = READ_RTC_SUCCESSFUL;
    }
    else
    {
	iRst = READ_RTC_FAILED;			 		
    }

    close(iRTC);
    return iRst;
}          



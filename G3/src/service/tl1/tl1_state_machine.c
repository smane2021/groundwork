/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : tl1_state_machine.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "tl1.h"

/* timeout = READCOM_TIMEOUT * timeout counter */

#define RESEND_TIMES		4	//resend times(response and alarm frames)

/* some operations */

#define CLEAR_TIMEOUT_COUNTER(_pThis) \
	((_pThis)->iTimeoutCounter = 0)

#define NEED_RESEND(_pThis) \
	(++(_pThis)->iResendCounter <= RESEND_TIMES)

#define CLEAR_RESEND_COUNTER(_pThis) \
	((_pThis)->iResendCounter = 0)


/* log Macros (ESM = state machine) */
#define TL1_TASK_ESM		"TL1 State Machine"

/* error log */
#define LOG_TL1_ESM_E(_szLogText)  LOG_TL1_E(TL1_TASK_ESM, _szLogText)

/* warning log */
#define LOG_TL1_ESM_W(_szLogText)  LOG_TL1_W(TL1_TASK_ESM, _szLogText) 

/* info log */
#define LOG_TL1_ESM_I(_szLogText)  LOG_TL1_I(TL1_TASK_ESM, _szLogText)

/* assistant functional Macros */
/* create and send Dummy frame event */
#define ESM_SEND_DUMMY(_pThis, _pEvent)  \
	do {  \
	    (_pEvent) = &g_TL1DummyEvent; \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_TL1_ESM_E("Put frame event to Output Queue failed"); \
			return TL1_IDLE;    \
		}  \
	}  \
	while (0)

/* send static event to the OutputQueue */
#define ESM_SEND_SEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_TL1_ESM_E("Put frame event to Output Queue failed"); \
			return TL1_IDLE;    \
		}  \
	}  \
	while (0)

/* send dynamic event to the OutputQueue */
#define ESM_SEND_DEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_TL1_ESM_E("Put frame event to Output Queue failed"); \
			DELETE((_pEvent));  \
			return TL1_IDLE;    \
		}  \
	}  \
	while (0)




int TL1_SendPacket(IN OUT TL1_BASIC_ARGS *pThis, IN char *pszData, IN int iLen)
{
	TL1_EVENT					*pEventSend = NULL;


	if((NULL == pszData) || (iLen <= 0))
	{
		return TL1_ERR_OK;
	}

	pEventSend = NEW(TL1_EVENT, 1);
	if (NULL == pEventSend )
	{
		LOG_TL1_ESM_E("Memory is not enough");
		return TL1_ERR_FAIL;
	}

	TL1_INIT_FRAME_EVENT(pEventSend, iLen, pszData, FALSE, FALSE);

	if (ERR_OK != Queue_Put(pThis->hEventOutputQueue, &pEventSend, FALSE))
	{
		LOG_TL1_ESM_E("Put frame event to Output Queue failed");
		DELETE(pEventSend);
		return TL1_ERR_FAIL;
	}

	return TL1_ERR_OK;
}

int TL1_SendRespData(IN OUT TL1_BASIC_ARGS *pThis,
					 IN char *pszRespData,
					 IN int iMaxLen, 
					 IN BOOL bNeedPrompt, 
					 IN BOOL bForcePrompt)
{
	char						*pcChr = pszRespData;
	char						*pszPacket = pszRespData;
	int							iPacketLen = 0;
	int							i;
	int							iSendTotalLen = 0;

	//find out all packets and send them
	for(i = 0; i < iMaxLen; i++, pcChr++)
	{
		//find out a packet and then send it
		if(TL1_RESP_PACKET_SPLITTER == *pcChr)
		{
			if(TL1_ERR_OK != TL1_SendPacket(pThis, pszPacket, iPacketLen))
			{
				return TL1_ERR_FAIL;
			}

			iSendTotalLen += iPacketLen;

			//init packet info
			iPacketLen = 0;
			pszPacket = pcChr + 1;
		}
		else if(TL1_CHAR_NULL == *pcChr)
		{
			if(TL1_ERR_OK != TL1_SendPacket(pThis, pszPacket, iPacketLen))
			{
				return TL1_ERR_FAIL;
			}

			iSendTotalLen += iPacketLen;

			break;//for(i = 0; i < TL1_RESP_SIZE; i++)
		}
		else
		{
			iPacketLen++;
		}
	}

	if(bNeedPrompt)
	{
		if(bForcePrompt || iSendTotalLen > 0)
		{
			if( TL1_ERR_OK != TL1_SendPrompt(pThis))
			{
				return TL1_ERR_FAIL;
			}
		}
	}

	return TL1_ERR_OK;
}


int TL1_BreakConnection(IN OUT TL1_BASIC_ARGS *pThis)
{
	TL1_EVENT				*pEventSend = NULL;

	pEventSend = &g_TL1DiscEvent;

	if (ERR_OK != Queue_Put(pThis->hEventOutputQueue, &pEventSend, FALSE))
	{
		LOG_TL1_ESM_E("Put frame event to Output Queue failed");
		return TL1_ERR_FAIL;
	}

	return TL1_ERR_OK;
}


int TL1_SendPrompt(IN OUT TL1_BASIC_ARGS *pThis)
{
	char					szBuf[10];
	int						iLen = 0;

	iLen = snprintf(szBuf, sizeof(szBuf), "%s", TL1_INFO_PROMPT);

	return TL1_SendPacket(pThis, szBuf, iLen);
}

int TL1_BuildTelnetCommand(OUT char *pszData, IN int iAct, IN int iOption)
{
	int							iLen = 0;

	pszData[iLen++] = (char)TELNET_IAC;
	pszData[iLen++] = (char)iAct;
	pszData[iLen++] = (char)iOption;

	return iLen;
}

int TL1_SetupTelnetSessionMode(IN OUT TL1_BASIC_ARGS *pThis)
{
	char					szBuf[30];
	int						iLen = 0;

	iLen = TL1_BuildTelnetCommand(szBuf, TELNET_DONT, TELNET_Linemode);
	iLen += TL1_BuildTelnetCommand(szBuf + iLen, TELNET_DO, TELNET_Send_Location);
	//iLen += TL1_BuildTelnetCommand(szBuf + iLen, TELNET_WILL, TELNET_Echo);
	//iLen += TL1_BuildTelnetCommand(szBuf + iLen, TELNET_DO, TELNET_Echo);
	iLen += TL1_BuildTelnetCommand(szBuf + iLen, TELNET_DONT, TELNET_Suppress_Go_Ahead);

	return TL1_SendPacket(pThis, szBuf, iLen);
}


static unsigned char TL1LenCheckSum(IN int wLen)
{
	unsigned char byLenCheckSum = 0;

	//������Ϊ0������Ҫ����
	if (wLen == 0)
		return 0;

	byLenCheckSum = 0;
	byLenCheckSum += wLen & 0x000F;         //ȡ���4��BIT
	byLenCheckSum += (wLen >> 4) & 0x000F;  //ȡ��4~7��BIT
	byLenCheckSum += (wLen >> 8) & 0x000F;  //ȡ��8~11��BIT
	byLenCheckSum %= 16;                    //ģ16
	byLenCheckSum = (~byLenCheckSum) + 1;   //ȡ����1
	byLenCheckSum &= 0x0F;                  //ֻȡ4��BIT

	return byLenCheckSum;
}


/*==========================================================================*
 * FUNCTION : OnIdle_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: TL1_OnIdle
 * ARGUMENTS: TL1_BASIC_ARGS *pThis:
 *			  TL1_EVENT  *pEvent   : 
 * RETURN   : int : next YDN State, defined by TL1_STATES enum const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-17 20:02
 *==========================================================================*/
static int OnIdle_HandleFrameEvent(IN OUT TL1_BASIC_ARGS *pThis, IN TL1_EVENT *pEvent)
{
	TL1_FRAME_TYPE				frameType;
	BOOL						bNeedPrompt = TRUE;

	/* frame analyse */
	//double tTime1 = GetCurrentTime();
	TRACE("111 Time is: %.3f\n", GetCurrentTime());
	frameType = TL1_AnalyseFrame(pEvent->sData, pEvent->iDataLength, pThis);
	TRACE("\tFrame Type: %x\n", frameType);
	switch (frameType)
	{
		case TL1_E_FRAME_OK:
		{
			#ifdef _DEBUG_TL1_LINKLAYER
			   TRACE_TL1_TIPS("Received frame event: frame_OK");
			#endif //_DEBUG_TL1_LINKLAYER

			#ifdef _DEBUG_TL1_CMD_HANDLER
			   TL1_PrintEvent(pEvent);
			#endif //_DEBUG_TL1_CMD_HANDLER

			/* extract data field from select frame */
			//TL1_ExtractDataFromFrame(pEvent->sData, pEvent->iDataLength, &iCmdDataLen, pCmdData);

			/* decode and perform it */
			TRACE("\n Begin execute TL1 cmd!!!!\n");
			//result will be stored to pThis->szCmdRespBuff
			TL1_DecodeAndPerform(pThis, (unsigned char *)pThis->stSession.stCommand.pstCmdCode->szData, TRUE);
			bNeedPrompt = TRUE;

			DELETE(pEvent);
			break;//switch (frameType)
		}
		case TL1_E_ERR_FRAME_PARSE:
		case TL1_E_ERR_FRAME_CHECK:
		{
			TRACE_TL1_TIPS("OnIdle_HandleFrameEvent : TL1_E_ERR_FRAME_PARSE/TL1_E_ERR_FRAME_CHECK");
			//input err code is stored in pThis->stSession->stCommand.iErrCode;
			//result will be stored to pThis->szCmdRespBuff
			TL1_DecodeAndPerform(pThis, (unsigned char *)"RESP ERR CODE", FALSE);
			bNeedPrompt = TRUE;

			DELETE(pEvent);
			break;
		}
		case TL1_E_ERR_FRAME_JUST_PROMPT:
		{
			//just give a prompt char '>'
			TRACE_TL1_TIPS("OnIdle_HandleFrameEvent : TL1_E_ERR_FRAME_JUST_PROMPT");
			pThis->szCmdRespBuff[0] = TL1_CHAR_NULL;
			bNeedPrompt = TRUE;

			DELETE(pEvent);
			break;
		}
		case TL1_E_ERR_FRAME_IGNORE:
		{
			TRACE_TL1_TIPS("OnIdle_HandleFrameEvent : TL1_E_ERR_FRAME_IGNORE");

			pThis->szCmdRespBuff[0] = TL1_CHAR_NULL;
			bNeedPrompt = FALSE;

			DELETE(pEvent);
			break;
		}
		default:  //unexpected frame event
		{
			#ifdef _DEBUG_TL1_LINKLAYER
				TRACE_TL1_TIPS("OnIdle_HandleFrameEvent : received unexpected frame event");
				TRACE("\tFrame Type: %d\n", frameType);
			#endif //_DEBUG_TL1_LINKLAYER

			pThis->szCmdRespBuff[0] = TL1_CHAR_NULL;
			bNeedPrompt = TRUE;

			DELETE(pEvent);
			//DELETE_TL1_EVENT(pEvent);
			//ESM_SEND_DUMMY(pThis, pEvent);

			//return TL1_IDLE;
		    break;
		}
	}

	//send perform results
	if(TL1_ERR_OK != TL1_SendRespData(pThis, (char *)pThis->szCmdRespBuff, sizeof(pThis->szCmdRespBuff), bNeedPrompt, TRUE))
	{
		TL1_BreakConnection(pThis);
	}

	TRACE("222 Time is: %.3f\n", GetCurrentTime());
	return TL1_IDLE;
}




/*==========================================================================*
 * FUNCTION : TL1_OnIdle
 * PURPOSE  : 
 * CALLS    : TL1_SendAlarm
 *			  OnIdle_HandleFrameEvent
 * CALLED BY: 
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis : 
 * RETURN   : int : next State, defined by TL1_STATES enum const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-17 19:23
 *==========================================================================*/
int TL1_OnIdle(IN OUT TL1_BASIC_ARGS *pThis)
{
	int							iRet;
	TL1_EVENT					*pEvent;
	TL1_ALARM_HANDLER			*pAlarmHandler = &pThis->alarmHandler; 
	TL1_SESSION					*pstSess = &pThis->stSession;
	TL1_COMMON_CONFIG			*pstCfg = &g_TL1Globals.CommonConfig;
	BOOL						bStopCommForAlarm = FALSE;  //need to stop communication for alarm occurs


	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* alarm and callback process */
	if (pThis->bCommRunning && pAlarmHandler->bAlarmAtHand )
	{
		/* reset timeout counter */
		pThis->iTimeoutCounter = 0; 
		TL1_SendAlarm(pThis);
		//return TL1_SEND_ALARM;
		return TL1_IDLE;
	}

	/* get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	if (iRet == ERR_QUEUE_EMPTY)  
	{
		/* In Idel state, it means timeout, keep wait in next Idle state */
		return TL1_IDLE;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_TL1_ESM_E("Get from Input Queue failed(invalid args)");
		return TL1_IDLE;
	}

	/* reset timeout counter */
	if (pEvent->iEventType != TL1_TIMEOUT_EVENT)
	{
		CLEAR_TIMEOUT_COUNTER(pThis);
	}

	/* get the flag */
	bStopCommForAlarm = (g_TL1Globals.CommonConfig.iMediaType == TL1_MEDIA_TYPE_MODEM
	    && pAlarmHandler->bAlarmAtHand
		&& pThis->iOperationMode == TL1_MODE_SERVER) ? TRUE : FALSE;
	
  //   #ifdef _DEBUG_TL1_LINKLAYER
		// printf("\t TL1_OnIdle:pEvent->iEventType = %d\n", pEvent->iEventType);
	 //#endif
	/* now iRet is ERR_OK */
	switch (pEvent->iEventType)
	{
		case TL1_FRAME_EVENT:  //frame event process
		{
			pstSess->uiInPacketCount++;

			return OnIdle_HandleFrameEvent(pThis, pEvent);
		}
		case TL1_TIMEOUT_EVENT:  //timeout event process, send disconnect event
		{
				/*DELETE(pEvent);*/
			//		   if (++pThis->iTimeoutCounter > TL1_MAX_TIMEOUTCOUNT || 
			if ( 
				bStopCommForAlarm)
		   {
			   int iNextState = TL1_IDLE;

			   #ifdef _DEBUG_TL1_LINKLAYER
					TRACE_TL1_TIPS("keep Timeout too long or have alarm/callback to "
						"report, will dissconnect current communication actively");
				#endif //_DEBUG_TL1_LINKLAYER

				pThis->iTimeoutCounter = 0;  //reset first
				/* create Disconnect event */
				pEvent = &g_TL1DiscEvent;
				
				/*deleted by ht,2006.7.12
				if (g_TL1Globals.CommonConfig.iMediaType == 
					TL1_MEDIA_TYPE_LEASED_LINE && bStopCommForAlarm)
				{
					// not need callback when using Leased Line
					// set flag first, insure change to the client mode
					#ifdef _DEBUG_TL1_LINKLAYER
						TRACE("Enter TL1_SEND_ALARM!!!!!");
					#endif
					TL1_SendAlarm(pThis);
					iNextState = TL1_SEND_ALARM;
				}
			   */
				/* send it */
				ESM_SEND_SEVENT(pThis, pEvent);

				return iNextState;
		   }
		   else  //just send dummy
		   {
			   printf("ESM_SEND_DUMMY\n");
				ESM_SEND_DUMMY(pThis, pEvent);
				return TL1_IDLE;
		   }
		}
		case TL1_DISCONNECTED_EVENT:  //Disconnected Event
		{
			#ifdef _DEBUG_TL1_LINKLAYER
					TRACE_TL1_TIPS("Received Disconnected Event: TL1_OnIdle");
			#endif //_DEBUG_TL1_LINKLAYER

			pstSess->bIsValid = FALSE;
			pstSess->bIsLogin = FALSE;

			/*DELETE(pEvent);*/

			/* wait in another Idle state */
			return TL1_IDLE;
		}
		case TL1_CONNECTED_EVENT:  //Connected Event
		{
			#ifdef _DEBUG_TL1_LINKLAYER
			TRACE_TL1_TIPS("Received Connected Event: TL1_OnIdle");
			#endif //_DEBUG_TL1_LINKLAYER

			/*DELETE(pEvent);*/

			memset(pstSess, 0, sizeof(TL1_SESSION));

			//reset timeout
			pstSess->uiInPacketCount = 0;
			pstSess->uiTimerTimeoutCount = 0;
			pstSess->bIsAutoLogin = FALSE;
			pstSess->bIsLogin = FALSE;
			pstSess->bIsValid = TRUE;

			//Autonomous TL1 messages will be output through the TL1 port 
			//whenever one is pending and there is a valid port connection.
			pstSess->bNeedReportAllAlarm = TRUE;
			pAlarmHandler->bAlarmAtHand = TRUE;

			if(TL1_CHAR_NULL != pstCfg->szAutoLoginUser[0])
			{
				if(ERR_SEC_OK == FindUserInfo(pstCfg->szAutoLoginUser, &pstSess->stUser.stBaseInfo))
				{
					pstSess->bIsAutoLogin = TRUE;
					pstSess->bIsLogin = TRUE;
				}
			}

			//printf("ooooo1\n");
			if( TL1_ERR_OK != TL1_SetupTelnetSessionMode(pThis))
			{
				TL1_BreakConnection(pThis);
				return TL1_IDLE;
			}

			if( TL1_ERR_OK != TL1_SendPrompt(pThis))
			{
				TL1_BreakConnection(pThis);
				return TL1_IDLE;
			}
			//printf("ooooo2\n");

			/* wait in another Idle state */
			return TL1_IDLE;
		}
		default: //unexpected event
		{
			#ifdef _DEBUG_TL1_LINKLAYER
					TRACE_TL1_TIPS("Received unexpected event");
					TRACE("\tEvent Type: %d\n", pEvent->iEventType);
			#endif //_DEBUG_TL1_LINKLAYER

			DELETE_TL1_EVENT(pEvent);
			return TL1_IDLE;
		}
	}

	return TL1_IDLE;
}



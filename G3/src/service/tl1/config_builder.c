/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : config_builder.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "tl1.h"

/* define splitter charater used by config files */
#define SPLITTER		 ('\t')

/* config file names */
#define CONFIG_FILE_MODELMAP	"private/tl1/TL1ModelMap.cfg"
#define CONFIG_FILE_COMMON		"private/tl1/TL1CommonConfig.cfg"

/* section names of common config file */
#define  PROTOCOL_TYPE			"[Protocol Type]"
#define  TL1_ADR				"[ADR]"
#define  OPERATION_MEDIA		"[Operation media]"
#define  MEDIA_PORT_PARAM		"[Media Port Param]"
#define  PORT_ACTIVATION		"[Port Activation]"
#define  PORT_KEEP_ALIVE		"[Port Keep Alive]"
#define  SESSION_TIMEOUT		"[Session Timeout]"
#define  AUTO_LOGIN_USER		"[Auto Login User]"
#define  SYSTEM_IDENTIFIER		"[System Identifier]"
#define  MODULE_SWITCH			"[Module Switch]"

/* config items of common Config file */
#define  CFG_ITEM_NUM					9
/* max length of cfg itme */
#define  MAX_CFG_ITEM_LEN				100

/* config items of TL1ModelMap.cfg */
#define  CFG_MODEL_MAP_ITEM_NUM			2
/* max length of cfg itme */
#define  MAX_CFG_MODEL_MAP_ITEM_LEN		(50*1024)//max about 100 line


/* used to detect modified config item */
#define CFG_ITEM_TEST(_nVarID, _ItemMask) \
	(((_nVarID) & (_ItemMask)) == (_ItemMask) || \
	 ((_nVarID) & TL1_CFG_ALL) == TL1_CFG_ALL)


/* sections names of Model Map config file */
#define  AID_GROUP_INFO_SEC			"[AID_GROUP_INFO]"
//#define  TYPE_MAP_NUM				"[TYPE_MAP_NUM]"
#define  TYPE_MAP_INFO_SEC			"[TYPE_MAP_INFO]"
/* note: will be extended at runtime */
#define  MAPENTRIES_INFO_SEC		"[MAPENTRIES_INFO]"


/* max type maps of Model config file */
//#define  MODEL_MAX_MAPS       50


/* to simplify log action when read common config file */
#define LOG_COMMON_CFG(_section)  \
	(AppLogOut("Read TL1 Common Config", APP_LOG_ERROR, "Read %s failed, " \
			"not such section or format is invalid.\n", (_section)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s failed, not such section or format" \
             " is invalid.\n", __FILE__, __LINE__, __FUNCTION__, (_section)))

#define LOG_COMMON_CFG_2(_section)  \
	(AppLogOut("Read TL1 Common Config", APP_LOG_ERROR, "Read %s failed, " \
			"format is invalid.\n", (_section)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s failed, format is invalid.\n", \
            __FILE__, __LINE__, __FUNCTION__, (_section)))



/* simplify log function when read fields of a cfg table */
/* DT: data type error */
#define LOG_READ_TABLE_DT(_tableName, _fieldName)  \
	(AppLogOut("Load TL1 Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"data type of %s field is wrong.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, data type of %s "  \
        "field is wrong.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* DR: data range exceeds */ 
#define LOG_READ_TABLE_DR(_tableName, _fieldName)  \
	(AppLogOut("Load TL1 Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"%s field data exceeds the bound.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field data exceeds"  \
        "the bound.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* NO: not find the filed. may caused by bad format */
#define LOG_READ_TABLE_NO(_tableName, _fieldName) \
	(AppLogOut("Load TL1 Model Config", APP_LOG_ERROR, "Read %s table failed, %s " \
		"field not found(please use the correct EMPTY FIELD character).\n", \
		(_tableName), (_fieldName)), \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field not found"  \
        "(please use the correct EMPTY FIELD character).\n", \
		__FILE__, __LINE__, __FUNCTION__, (_tableName), (_fieldName)))

/* ET: field is empty */
#define LOG_READ_TABLE_ET(_tableName, _fieldName) \
	(AppLogOut("Load TL1 Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"%s field should not be empty.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field should not"  \
        "be empty.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))


/*==========================================================================*
 * FUNCTION : IsEmptyField
 * PURPOSE  : empty config item in common cfg file
 * CALLS    : 
 * CALLED BY: LoadCommonConfigProc
 * ARGUMENTS: const char  *pField : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-01 16:07
 *==========================================================================*/
static BOOL IsEmptyField(IN const char *pField)
{
	return (pField[0] == '-');
}


/*==========================================================================*
 * FUNCTION : LoadCommonConfigProc
 * PURPOSE  : callback function called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: void  *pCfg       : 
 *            void  *pLoadToBuf : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-10 16:38
 *==========================================================================*/
static int LoadCommonConfigProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	TL1_COMMON_CONFIG *pCommonCfg;
	int iRet, iBuf;

	pCommonCfg = (TL1_COMMON_CONFIG *)pLoadToBuf;

	/* 1.protocol type */
	//iRet = Cfg_ProfileGetInt(pCfg, PROTOCOL_TYPE, &iBuf);
	//if (iRet != 1)
	//{
	//	LOG_COMMON_CFG("protocol type");
	//	return ERR_CFG_FAIL;
	//}
	/*check protocol type */
	/* 1: YDN23 */
	//if (iBuf < 1 || iBuf > 4)
	//{
	//	LOG_COMMON_CFG_2("Protocol Type");
	//	return ERR_CFG_BADCONFIG;
	//}
	//pCommonCfg->iProtocolType = iBuf;
	pCommonCfg->iProtocolType = TL1;

	/* 2.ADR */
	//iRet = Cfg_ProfileGetInt(pCfg, TL1_ADR, &iBuf);
	//if (iRet != 1)
	//{
	//	LOG_COMMON_CFG("ADR");
	//	return ERR_CFG_FAIL;
	//}
 //   if (iBuf < 0 || iBuf > 255)
	//{
	//	LOG_COMMON_CFG_2("ADR");
	//	return ERR_CFG_BADCONFIG;
	//}
	pCommonCfg->byADR = (BYTE)1;//unused

	/* 3.operation media */
	iRet = Cfg_ProfileGetInt(pCfg, OPERATION_MEDIA, &iBuf);
	if (iRet != 1)
	{
		LOG_COMMON_CFG("Operation Media");
		return ERR_CFG_FAIL;
	}
	/* check value */
	if (iBuf < TL1_MEDIA_TYPE_TCPIP || iBuf >= TL1_MEDIA_TYPE_NUM)
	{
		LOG_COMMON_CFG_2("Operation Media");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->iMediaType = iBuf;


	/* 4.operation media port param */
	iRet = Cfg_ProfileGetString(pCfg, 
								MEDIA_PORT_PARAM, 
								pCommonCfg->szCommPortParam,
								COMM_PORT_PARAM_LEN);
	if (iRet != 1)
	{
		LOG_COMMON_CFG("Media Port Param");
		return ERR_CFG_FAIL;
	}
	switch (pCommonCfg->iMediaType)
	{
	case TL1_MEDIA_TYPE_LEASED_LINE:
		{
			if (!TL1_CheckBautrateCfg(pCommonCfg->szCommPortParam))
			{
				LOG_COMMON_CFG_2("Media Port Param");
				return ERR_CFG_BADCONFIG;
			}
			break;
		}
	case TL1_MEDIA_TYPE_MODEM:
		{
			//if (!TL1_CheckModemCfg(pCommonCfg->szCommPortParam))
			if (!TL1_CheckBautrateCfg(pCommonCfg->szCommPortParam))
			{
				LOG_COMMON_CFG_2("Media Port Param");
				return ERR_CFG_BADCONFIG;
			}
			break;
		}
	case TL1_MEDIA_TYPE_TCPIP:
		{
			if (!TL1_CheckIPAddress(pCommonCfg->szCommPortParam))
			{
				LOG_COMMON_CFG_2("Media Port Param");
				return ERR_CFG_BADCONFIG;
			}
			iBuf = atoi(pCommonCfg->szCommPortParam);
			if(iBuf < TL1_MIN_VAL_PORT_NUMBER || iBuf > TL1_MAX_VAL_PORT_NUMBER)
			{
				LOG_COMMON_CFG_2("Media Port Param");
				return ERR_CFG_BADCONFIG;
			}

			break;
		}
	case TL1_MEDIA_TYPE_TCPIPV6:
		{
			if (!TL1_CheckIPAddress(pCommonCfg->szCommPortParam))
			{
				LOG_COMMON_CFG_2("Media Port Param");
				return ERR_CFG_BADCONFIG;
			}
			iBuf = atoi(pCommonCfg->szCommPortParam);
			if(iBuf < TL1_MIN_VAL_PORT_NUMBER || iBuf > TL1_MAX_VAL_PORT_NUMBER)
			{
				LOG_COMMON_CFG_2("Media Port Param");
				return ERR_CFG_BADCONFIG;
			}

			break;
		}
	default:
		{
			LOG_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
	}
	
	/* 5.[Port Activation] */
	iRet = Cfg_ProfileGetInt(pCfg, PORT_ACTIVATION, &iBuf);
	if (iRet != 1)
	{
		LOG_COMMON_CFG(PORT_ACTIVATION);
		return ERR_CFG_FAIL;
	}
	if(TL1_DISABLE_VALUE != iBuf)
	{
		iBuf = TL1_ENABLE_VALUE;
	}
	pCommonCfg->iPortActivation = iBuf;

	/* 6.[Port Keep Alive] */
	iRet = Cfg_ProfileGetInt(pCfg, PORT_KEEP_ALIVE, &iBuf);
	if (iRet != 1)
	{
		LOG_COMMON_CFG(PORT_KEEP_ALIVE);
		return ERR_CFG_FAIL;
	}
	if(TL1_DISABLE_VALUE != iBuf)
	{
		iBuf = TL1_ENABLE_VALUE;
	}
	pCommonCfg->iPortKeepAlive = iBuf;

	/* 7.[Session Timeout] */
	iRet = Cfg_ProfileGetInt(pCfg, SESSION_TIMEOUT, &iBuf);
	if (iRet != 1)
	{
		LOG_COMMON_CFG(SESSION_TIMEOUT);
		return ERR_CFG_FAIL;
	}
	if (iBuf < TL1_MIN_VAL_SESSION_TIMEOUT || iBuf > TL1_MAX_VAL_SESSION_TIMEOUT)
	{
		LOG_COMMON_CFG(SESSION_TIMEOUT);
		return ERR_CFG_FAIL;
	}
	pCommonCfg->iSessionTimeout = iBuf;

	/* 8.[Auto Login User] */
	iRet = Cfg_ProfileGetString(pCfg, 
		AUTO_LOGIN_USER, 
		pCommonCfg->szAutoLoginUser,
		TL1_MAX_LEN_AUTO_LOGIN_USER);
	if (iRet != 1)
	{
		LOG_COMMON_CFG(AUTO_LOGIN_USER);
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szAutoLoginUser))
	{
		pCommonCfg->szAutoLoginUser[0] = '\0';
	}

	/* 9.[System Identifier] */
	iRet = Cfg_ProfileGetString(pCfg, 
		SYSTEM_IDENTIFIER, 
		pCommonCfg->szSystemIdentifier,
		TL1_MAX_LEN_SYSTEM_IDENTIFIER);
	if (iRet != 1)
	{
		LOG_COMMON_CFG(SYSTEM_IDENTIFIER);
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szSystemIdentifier))
	{
		pCommonCfg->szSystemIdentifier[0] = '\0';
	}

	/* 10.[Module Switch] */
	iRet = Cfg_ProfileGetInt(pCfg, MODULE_SWITCH, &iBuf);
	if (iRet != 1)
	{
		LOG_COMMON_CFG(MODULE_SWITCH);
		return ERR_CFG_FAIL;
	}
	if(TL1_DISABLE_VALUE != iBuf)
	{
		iBuf = TL1_ENABLE_VALUE;
	}
	pCommonCfg->iModuleSwitch = iBuf;

	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : LoadCommonConfig
 * PURPOSE  : to load common config file
 * CALLS    : 
 * CALLED BY: TL1_InitConfig
 * ARGUMENTS:  
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-18 20:56
 *==========================================================================*/
static int LoadCommonConfig(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_COMMON, szCfgFileName, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadCommonConfigProc,
		&g_TL1Globals.CommonConfig);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : ParseAIDGroupTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Cfg_LoadSingleTable
 * ARGUMENTS: char           *szBuf       : 
 *            TL1_AID_GROUP_INFO  *pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 09:28
 *==========================================================================*/
static int ParseAIDGroupTableProc(IN char *szBuf, OUT TL1_AID_GROUP_INFO *pStructData)
{
	char	*pField;
	int		iRet = 0;
	char	*pErrInfo = NULL;
	int		iAidNameMaxValidLen = 0;
	int		iAidGroupNameLen = 0;

	ASSERT(szBuf);
	ASSERT(pStructData);
	

	/* 1. Index */
	iRet++;
	pErrInfo = "Index";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iIndex = atoi(pField);
	if(pStructData->iIndex <= 0)
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 2. Description */
	iRet++;
	pErrInfo = "Description";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == TL1_CHAR_NULL)
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}
	if( TL1_CheckNA(pField) )
	{
		pStructData->szDescription[0] = TL1_CHAR_NULL;
	}
	else
	{
		strncpyz(pStructData->szDescription, pField, sizeof(pStructData->szDescription));
	}

	/* 3.AID Group Name */
	iRet++;
	pErrInfo = "AID Group Name";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField == TL1_CHAR_NULL)
		|| (!TL1_IsAlphaNum(pField))
		|| TL1_CheckNA(pField))
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}
	strncpyz(pStructData->szAidGroupName, pField, sizeof(pStructData->szAidGroupName));

	/* 4.AID SubGroup Name Prefix */
	iRet++;
	pErrInfo = "AIDSubGroupNamePrefix";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField == TL1_CHAR_NULL)
		|| (!TL1_IsAlphaNum(pField)))
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}

	if( TL1_CheckNA(pField) )
	{
		pStructData->szAidSubGroupNamePrefix[0] = TL1_CHAR_NULL;
	}
	else
	{
		strncpyz(pStructData->szAidSubGroupNamePrefix, pField, sizeof(pStructData->szAidSubGroupNamePrefix));
	}

	/* 5.AID Group Type */
	iRet++;
	pErrInfo = "AID Group Type";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if(0 == strcmp(pField,"EQPT"))
	{
		pStructData->iAidGroupType = 0;
	}
	else if (0 == strcmp(pField,"ENV"))
	{
		pStructData->iAidGroupType = 1;
	}
	else
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 6. Map Entries ID */
	iRet++;
	pErrInfo = "Map Entries ID";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iMapentriesID = atoi(pField);
	if(pStructData->iMapentriesID <= 0)
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 7. AID SubGroup Name Exist */
	iRet++;
	pErrInfo = "AID SubGroup Name Exist";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField == TL1_CHAR_NULL)
		|| (!TL1_CheckYesNo(pField)))
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}
	if(  ('y' == pField[0]) || ('Y' == pField[0])  )
	{
		pStructData->bAidSubGroupNameExist = TRUE;
	}
	else
	{
		pStructData->bAidSubGroupNameExist = FALSE;
	}

	/* 8. AID SubGroup Name Suffix Max Len */
	iRet++;
	pErrInfo = "AID SubGroup Name Suffix Max Len";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iAidSubGroupNameSuffixMaxLen = atoi(pField);
	if(pStructData->iAidSubGroupNameSuffixMaxLen < 0)
	{
		LOG_READ_TABLE_NO(AID_GROUP_INFO_SEC, pErrInfo);
		return iRet;
	}

	//check and fix AID SubGroup Name Suffix Max Len
	if( pStructData->bAidSubGroupNameExist )
	{
		//add the len of char '-' in AID Name (RECT-A1, RECT-A120,,,,Test-T11,,)
		pStructData->iAidSubGroupNameSuffixMaxLen += 1;

		//iAidSubGroupNameSuffixMaxLen is too long
		if(pStructData->iAidSubGroupNameSuffixMaxLen > TL1_CFG_AID_SUB_GROUP_NAME_SUFFIX_MAX_LEN)
		{
			pStructData->iAidSubGroupNameSuffixMaxLen = TL1_CFG_AID_SUB_GROUP_NAME_SUFFIX_MAX_LEN;
		}
	}
	else
	{
		pStructData->iAidSubGroupNameSuffixMaxLen = 0;
	}


	//9. limit AID Name total len
	if( pStructData->bAidSubGroupNameExist )
	{
		//AID Name format:RECT-A1, RECT-A120
		//total len is: (len of AID Group Name) + (len of char '-') + (len of AID Sub Group Name Prefix) + (len of AID Sub Group Name Suffix)
		//max total len is:TL1_FIELD_LIMIT_AID
		//for some reasons, (len of char '-') is included in iAidSubGroupNameSuffixMaxLen
		iAidNameMaxValidLen = TL1_FIELD_LIMIT_AID - pStructData->iAidSubGroupNameSuffixMaxLen;
		iAidGroupNameLen = strlen(pStructData->szAidGroupName);

		if(iAidGroupNameLen >= iAidNameMaxValidLen)//AID Group Name is too long
		{
			//cut Aid Group Name short to iAidNameMaxValidLen
			pStructData->szAidGroupName[iAidNameMaxValidLen] = TL1_CHAR_NULL;
			//cut Aid Sub Group Name short to 0
			pStructData->szAidSubGroupNamePrefix[0] = TL1_CHAR_NULL;
		}
		else
		{
			//cut Aid Group Name short to iAidGroupNameLen
			//pStructData->szAidGroupName[iAidGroupNameLen] = TL1_CHAR_NULL;
			//cut Aid Sub Group Name short to (iAidNameMaxValidLen - iAidGroupNameLen)
			pStructData->szAidSubGroupNamePrefix[iAidNameMaxValidLen - iAidGroupNameLen] = TL1_CHAR_NULL;
		}
	}
	else
	{
		//cut Aid Sub Group Name short to 0
		pStructData->szAidSubGroupNamePrefix[0] = TL1_CHAR_NULL;
	}


	return 0;
}




/*==========================================================================*
 * FUNCTION : ParseTypeMapTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Cfg_LoadSingleTable
 * ARGUMENTS: char           *szBuf       : 
 *            TL1_TYPE_MAP_INFO  *pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 09:28
 *==========================================================================*/
static int ParseTypeMapTableProc(IN char *szBuf, OUT TL1_TYPE_MAP_INFO *pStructData)
{
	char	*pField;
	int		iRet = 0;
	char	*pErrInfo = NULL;

	ASSERT(szBuf);
	ASSERT(pStructData);
	

	/* 1. Index */
	iRet++;
	pErrInfo = "Index";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iIndex = atoi(pField);
	if(pStructData->iIndex <= 0)
	{
		LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 2. Type Description */
	iRet++;
	pErrInfo = "Type Description";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 3.AID Group ID */
	iRet++;
	pErrInfo = "AID Group ID";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iAidGroupID = atoi(pField);
	if(pStructData->iAidGroupID <= 0)
	{
		LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 4. AID SubGroup Name Suffix Start */
	iRet++;
	pErrInfo = "AIDSubGroupNameSuffixStart";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if( TL1_CheckNA(pField) )
	{
		pStructData->iAidSubGroupSuffixStart = -1;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
		pStructData->iAidSubGroupSuffixStart = atoi(pField);
		if(pStructData->iAidSubGroupSuffixStart < 0)
		{
			LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
	}

	/* 5. SCU+ Equip Type ID */
	iRet++;
	pErrInfo = "SCU+ Equip Type ID";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if( TL1_CheckNA(pField) )
	{
		pStructData->iEquipTypeID = -1;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
		pStructData->iEquipTypeID = atoi(pField);
		if(pStructData->iEquipTypeID <= 0)
		{
			LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
	}


	/* 6. SCU+ Equip ID Start */
	iRet++;
	pErrInfo = "SCU+ Equip ID Start";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if( TL1_CheckNA(pField) )
	{
		pStructData->iEquipIDStart = -1;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
		pStructData->iEquipIDStart = atoi(pField);
		if(pStructData->iEquipIDStart <= 0)
		{
			LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
	}

	/* 7. SCU+ Equip ID End */
	iRet++;
	pErrInfo = "SCU+ Equip ID End";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if( TL1_CheckNA(pField) )
	{
		pStructData->iEquipIDEnd = -1;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
		pStructData->iEquipIDEnd = atoi(pField);
		if(pStructData->iEquipIDEnd <= 0)
		{
			LOG_READ_TABLE_NO(TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
	}

	return 0;
}


/*==========================================================================*
 * FUNCTION : ParseMapEntriesTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: LoadModelMapConfigProc
 * ARGUMENTS: char               *szBuf      : 
 *            MODBUS_MAPENTRIES_INFO *  pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 11:39
 *==========================================================================*/
static int ParseMapEntriesTableProc(IN char *szBuf, OUT TL1_MAPENTRIES_INFO *pStructData)
{
#define TL1_ACU_SIG_FORMAT_STR			"[%d,%d,%d]"

	char	*pField;
	int		iRet = 0;
	char	*pErrInfo = NULL;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1. Index */
	iRet++;
	pErrInfo = "Index";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	pStructData->iIndex = atoi(pField);
	if(pStructData->iIndex <= 0)
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}

	/* 2. Signal Description */
	iRet++;
	pErrInfo = "Description";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == TL1_CHAR_NULL)
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	if( TL1_CheckNA(pField) )
	{
		pStructData->szDescription[0] = TL1_CHAR_NULL;
	}
	else
	{
		strncpyz(pStructData->szDescription, pField, sizeof(pStructData->szDescription));
	}


	/* 3.TL1 Signal ID */
	iRet++;
	pErrInfo = "TL1 Signal ID";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	pStructData->iTL1SigID = atoi(pField);
	if(pStructData->iTL1SigID <= 0)
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}

	/* 4.Signal Active */
	iRet++;
	pErrInfo = "Signal Active";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField == TL1_CHAR_NULL)
		|| (!TL1_CheckYesNo(pField)))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	if(  ('y' == pField[0]) || ('Y' == pField[0])  )
	{
		pStructData->bSigActive = TRUE;
	}
	else
	{
		pStructData->bSigActive = FALSE;
	}

	/* 5.Condition Type */
	iRet++;
	pErrInfo = "Condition Type";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField == TL1_CHAR_NULL)
		|| (!TL1_IsAlphaNumUnderscoreA(pField)))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	if( TL1_CheckNA(pField) )
	{
		pStructData->szCondType[0] = TL1_CHAR_NULL;
	}
	else
	{
		strncpyz(pStructData->szCondType, pField, sizeof(pStructData->szCondType));
	}

	/* 6.Condition Description */
	iRet++;
	pErrInfo = "Condition Description";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == TL1_CHAR_NULL)
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	if( TL1_CheckNA(pField) )
	{
		pStructData->szCondDesc[0] = TL1_CHAR_NULL;
	}
	else
	{
		strncpyz(pStructData->szCondDesc, pField, sizeof(pStructData->szCondDesc));
	}

	/* 7.Service Affect Code */
	iRet++;
	pErrInfo = "Service Affect Code";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	pStructData->iServiceAffectCode = atoi(pField);
	if((pStructData->iServiceAffectCode < 0)
		|| (pStructData->iServiceAffectCode > 1))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}

	/* 8.Monitor Format */
	iRet++;
	pErrInfo = "Monitor Format";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	pStructData->iMonitorFormat = atoi(pField);
	if((pStructData->iMonitorFormat < 0)
		|| (pStructData->iMonitorFormat > 1))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}

	/* 9.Monitor Type */
	iRet++;
	pErrInfo = "Monitor Type";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField == TL1_CHAR_NULL)
		|| (!TL1_IsAlpha(pField)))
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	if( TL1_CheckNA(pField) )
	{
		pStructData->szMonitorType[0] = TL1_CHAR_NULL;
	}
	else
	{
		strncpyz(pStructData->szMonitorType, pField, sizeof(pStructData->szMonitorType));
	}

	/* 10.SCU+ Alarm Signal */
	iRet++;
	pErrInfo = "SCU+ Alarm Signal";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == TL1_CHAR_NULL)
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}

	pStructData->stAlarmSig.iRelevantEquipListIndex = -1;
	pStructData->stAlarmSig.iSigTypeID = SIG_TYPE_ALARM;
	pStructData->stAlarmSig.iSigID = -1;

	if( TL1_CheckNA(pField) )
	{
		;
	}
	else if((*pField >= '0') && (*pField <= '9'))//just a sig ID
	{
		pStructData->stAlarmSig.iSigID = atoi(pField);
		if(pStructData->stAlarmSig.iSigID <= 0)
		{
			LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
			return iRet;
		}
	}
	else
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}
	//else
	//{
	//	if(3 != sscanf(pField,
	//		TL1_ACU_SIG_FORMAT_STR,
	//		&pStructData->stAlarmSig.iRelevantEquipListIndex,
	//		&pStructData->stAlarmSig.iSigTypeID,
	//		&pStructData->stAlarmSig.iSigID));
	//	{
	//		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
	//		return iRet;
	//	}

	//	if((pStructData->stAlarmSig.iRelevantEquipListIndex < 0)
	//		|| (pStructData->stAlarmSig.iSigTypeID != SIG_TYPE_ALARM)
	//		|| (pStructData->stAlarmSig.iSigID <= 0))
	//	{
	//		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
	//		return iRet;
	//	}
	//}
	
	
	/* 11.SCU+ Sample Signal */
	iRet++;
	pErrInfo = "SCU+ Sample Signal";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == TL1_CHAR_NULL)
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}

	pStructData->stSampleSig.iRelevantEquipListIndex = -1;
	pStructData->stSampleSig.iSigTypeID = SIG_TYPE_SAMPLING;
	pStructData->stSampleSig.iSigID = -1;

	if( TL1_CheckNA(pField) )
	{
		;
	}
	else if((*pField >= '0') && (*pField <= '9'))//just a sig ID
	{
		pStructData->stSampleSig.iSigID = atoi(pField);
		if(pStructData->stSampleSig.iSigID <= 0)
		{
			LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
			return iRet;
		}
	}
	else
	{
		if(3 != sscanf(pField,
			TL1_ACU_SIG_FORMAT_STR,
			&pStructData->stSampleSig.iRelevantEquipListIndex,
			&pStructData->stSampleSig.iSigTypeID,
			&pStructData->stSampleSig.iSigID))
		{
			LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
			return iRet;
		}

		if((pStructData->stSampleSig.iRelevantEquipListIndex < 0)
			|| (pStructData->stSampleSig.iSigTypeID < 0)
			|| (pStructData->stSampleSig.iSigTypeID > 3)
			|| (pStructData->stSampleSig.iSigID <= 0))
		{
			LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
			return iRet;
		}
	}

	/* 12.SCU+ Setting Signal */
	iRet++;
	pErrInfo = "SCU+ Setting Signal";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == TL1_CHAR_NULL)
	{
		LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
		return iRet;
	}

	pStructData->stSettingSig.iRelevantEquipListIndex = -1;
	pStructData->stSettingSig.iSigTypeID = SIG_TYPE_SETTING;
	pStructData->stSettingSig.iSigID = -1;

	if( TL1_CheckNA(pField) )
	{
		;
	}
	else if((*pField >= '0') && (*pField <= '9'))//just a sig ID
	{
		pStructData->stSettingSig.iSigID = atoi(pField);
		if(pStructData->stSettingSig.iSigID <= 0)
		{
			LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
			return iRet;
		}
	}
	else
	{
		if(3 != sscanf(pField,
			TL1_ACU_SIG_FORMAT_STR,
			&pStructData->stSettingSig.iRelevantEquipListIndex,
			&pStructData->stSettingSig.iSigTypeID,
			&pStructData->stSettingSig.iSigID))
		{
			LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
			return iRet;
		}

		if((pStructData->stSettingSig.iRelevantEquipListIndex < 0)
			|| (pStructData->stSettingSig.iSigTypeID < 0)
			|| (pStructData->stSettingSig.iSigTypeID > 3)
			|| (pStructData->stSettingSig.iSigID <= 0))
		{
			LOG_READ_TABLE_NO("Model Map Entries", pErrInfo);
			return iRet;
		}
	}

	return 0;
}


/*==========================================================================*
 * FUNCTION : GetMapEntriesSecName
 * PURPOSE  : assistant function to get Map Entries Section Name
 * CALLS    : 
 * CALLED BY: LoadModelMapConfigProc
 * ARGUMENTS: IN const char  *szBaseName       : 
 *            IN int         iMapIndex         : 
 *            OUT char       *szEntriesSecName : 
 *            IN int         iLen              : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : 
 *==========================================================================*/
static void GetMapEntriesSecName(IN const char *szBaseName, 
								 IN int iMapIndex,
								 OUT char *szEntriesSecName,
								 IN int iLen)
{
	char szFix[10], *p;
	int i;
    
	sprintf(szFix, "_MAP%d]%c", iMapIndex, '\0');
	strncpy(szEntriesSecName, szBaseName, (size_t)iLen);

	p = szEntriesSecName;
	for (i = iLen; *p != ']' && --i > 0; p++)
	{
		;
	}

	*p = '\0';

    i = strlen(szEntriesSecName);
	strncat(szEntriesSecName, szFix, (size_t)(iLen - i -1));

	return;
}

/*==========================================================================*
 * FUNCTION : LoadModelMapConfigProc
 * PURPOSE  : callback funtion called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: LoadModelMapConfig
 * ARGUMENTS: void         *pCfg       : 
 *            void         *pLoadToBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao
 *==========================================================================*/
static int LoadModelMapConfigProc(IN void *pCfg, OUT void *pLoadToBuf)
{
    TL1MODEL_CONFIG_INFO *pBuf;

	CONFIG_TABLE_LOADER loader[1];
	//char szMapEntriesSecName[MODEL_MAX_MAPS][30];
	int iMaps, i, j;
	TL1_TYPE_MAP_INFO *pMap;  /* used as buf */

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (TL1MODEL_CONFIG_INFO *)pLoadToBuf;


	//1. load [AID_GROUP_INFO] at first
	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pBuf->iAIDGroupNum),
		AID_GROUP_INFO_SEC,
		&(pBuf->pstAIDGroupInfo), 
		ParseAIDGroupTableProc);

	if (Cfg_LoadTables(pCfg,1,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	if(pBuf->iAIDGroupNum <= 0)
	{
		return ERR_CFG_FAIL;
	}


	/* 2. load type map table */
	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pBuf->iTypeMapNum),
		TYPE_MAP_INFO_SEC,
		&(pBuf->pTypeMapInfo), 
		ParseTypeMapTableProc);

	if (Cfg_LoadTables(pCfg,1,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	/* then load map entries */
	iMaps = pBuf->iTypeMapNum;

	if(iMaps <= 0)
	{
		return ERR_CFG_OK;
	}

	//3. process AID Group ID between [TYPE_MAP_INFO] and [AID_GROUP_INFO]
	//AID Group ID of each [TYPE_MAP_INFO] item must be valid in [AID_GROUP_INFO]
	for(i = 0; i < iMaps; i++)
	{
		pBuf->pTypeMapInfo[i].pstAidGroupInfo = NULL;//init pointer

		//find AID Group ID in [AID_GROUP_INFO] for each item of [TYPE_MAP_INFO]
		for(j = 0; j < pBuf->iAIDGroupNum; j++)
		{
			if(pBuf->pTypeMapInfo[i].iAidGroupID ==  pBuf->pstAIDGroupInfo[j].iIndex)
			{
				//exist the same  AID Group ID
				//save pointer
				pBuf->pTypeMapInfo[i].pstAidGroupInfo = &pBuf->pstAIDGroupInfo[j];

				break;
			}
		}
	}

	//check whether AID Group ID of each [TYPE_MAP_INFO] is exist
	for(i = 0; i < iMaps; i++)
	{
		if(NULL == pBuf->pTypeMapInfo[i].pstAidGroupInfo)
		{
			return ERR_CFG_FAIL;
		}
	}

	//4. load all [MAPENTRIES_INFO_MAPXXX] to pstAIDGroupInfo
	CONFIG_TABLE_LOADER loader1[pBuf->iAIDGroupNum];
	char szMapEntriesSecName[pBuf->iAIDGroupNum][30];

	for (i = 0; i < pBuf->iAIDGroupNum; i++)
	{
		GetMapEntriesSecName(
			MAPENTRIES_INFO_SEC,
			pBuf->pstAIDGroupInfo[i].iMapentriesID,
			szMapEntriesSecName[i],
			30);

		DEF_LOADER_ITEM(&loader1[i],
			NULL,
			&(pBuf->pstAIDGroupInfo[i].iMapEntriesNum),
			szMapEntriesSecName[i],
			&(pBuf->pstAIDGroupInfo[i].pMapEntriesInfo),
			ParseMapEntriesTableProc);
	}

	if (Cfg_LoadTables(pCfg,pBuf->iAIDGroupNum,loader1) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : LoadModelMapConfig
 * PURPOSE  : to load Model Map config file
 * CALLS    : Cfg_LoadConfigFile
 * CALLED BY: TL1_InitConfig
 * ARGUMENTS: 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-19 15:43
 *==========================================================================*/
static int LoadModelMapConfig(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_MODELMAP, szCfgFileName, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadModelMapConfigProc,
		&g_TL1Globals.TL1ModelConfig);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}



/* Added by Thomas begin, support product info. 2006-2-9 */
#ifdef PRODUCT_INFO_SUPPORT

#endif //PRODUCT_INFO_SUPPORT
/* Added by Thomas end, support product info. 2006-2-9 */

/*==========================================================================*
 * FUNCTION : OrderEquipID
 * PURPOSE  : 
 * CALLS    : GetUnitNum
 *			  SetUnitNum
 * CALLED BY: CreateBlocks
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 19:45
 *==========================================================================*/
 static void OrderEquipID(IN int EquipNum, OUT EQUIP_INFO* pEquipInfo)
{
   int i, j, k, iError, iBufLen;
   int iEquipTypeID;
   int iStdEquipTypeNum;
   STDEQUIP_TYPE_INFO *pStdEquiPTypeInfo;
   EQUIP_INFO *pCurEquip;

   iError = DxiGetData(VAR_STD_EQUIPS_LIST, 0, 0, &iBufLen, &pStdEquiPTypeInfo, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Order EquipID",APP_LOG_ERROR, 
			"Get StdEquip List through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		//return ERR_CFG_FAIL;
		return;
	}

   iError = DxiGetData(VAR_STD_EQUIPS_NUM, 0, 0, &iBufLen, &iStdEquipTypeNum, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Order EquipID",APP_LOG_ERROR, 
			"Get StdEquip Num through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		//return ERR_CFG_FAIL;
		return;
	}

	k = 0;
   for(i = 0; i < iStdEquipTypeNum; i++, pStdEquiPTypeInfo++)
   {
	   pCurEquip = pEquipInfo;
       for(j = 0; j < EquipNum; j++, pCurEquip++)
       {
		   iEquipTypeID = pCurEquip->iEquipTypeID;
		   if(iEquipTypeID == pStdEquiPTypeInfo->iTypeID)
		   {
			   g_TL1Globals.iEquipIDOrder[k][0] = pCurEquip->iEquipID;
			   g_TL1Globals.iEquipIDOrder[k][1] = pCurEquip->iEquipTypeID;
			   k++;
		   }
       }
   }
}
/*==========================================================================*
 * FUNCTION : CreateBlocks
 * PURPOSE  : 
 * CALLS    : GetUnitNum
 *			  SetUnitNum
 * CALLED BY: TL1_InitConfig
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-18 19:45
 *==========================================================================*/
static int CreateBlocks(void)
{
	TL1MODEL_CONFIG_INFO	*pstModelCfgInfo = &g_TL1Globals.TL1ModelConfig;
	TL1_EQUIP_INFO			*pstCurTL1Equip = NULL;
    EQUIP_INFO				*pTotalEquipInfo = NULL, *pCurEquipInfo = NULL;
	TL1_TYPE_MAP_INFO		*pCurTypeMap = NULL;
	TL1_TYPE_MAP_INFO		*pTypeMap = NULL;
	TL1_BLOCK_INFO			*pBlockInfo = NULL;
	TL1_BLOCK_INFO			*pCurBlockInfo = NULL;
	TL1_MAPENTRIES_INFO		*pCurMapEntry = NULL;
	ALARM_SIG_VALUE			*pstAlarmSigVal = NULL;
	TL1_AID_GROUP_INFO		*pstCurAidGroupInfo = NULL;
	int						iTypeMapNum, iTotalEquipNum, iEquipNum;
	int						iBufLen, iError;
	int						iBlockNum = 0;
	int						i, j, k, iTemp;
	BOOL					bIsFetalFail = FALSE, bIsValidEquipTypeID = TRUE;
	int						iEquipID, iSigTypeID, iSigID;
	SIG_BASIC_VALUE  *pSigValue; 


	//1.get equip list 
	iError = DxiGetData(VAR_ACU_EQUIPS_LIST, 0, 0, &iBufLen, &pTotalEquipInfo, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Create Blocks",APP_LOG_ERROR, 
			"Get Equip List through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		return ERR_CFG_FAIL;
	}
	iTotalEquipNum = iBufLen/sizeof(EQUIP_INFO);
	g_TL1Globals.iEquipNum = iTotalEquipNum;
	//TRACE("\n g_TL1Globals.iEquipNum is: %d\n", g_TL1Globals.iEquipNum);
	g_TL1Globals.pEquipInfo = pTotalEquipInfo;


	//2. init and malloc

	iTypeMapNum = pstModelCfgInfo->iTypeMapNum;
	pTypeMap = pstModelCfgInfo->pTypeMapInfo;

	//init TL1ModelInfo
	g_TL1Globals.TL1ModelInfo.iBlocksNum = 0;
	g_TL1Globals.TL1ModelInfo.pBlockInfo = NULL;

	if(iTypeMapNum <= 0)
	{
		return ERR_CFG_OK;
	}

	//malloc pBlockInfo
	iBlockNum = iTypeMapNum;
	pBlockInfo = NEW(TL1_BLOCK_INFO, iBlockNum);
	if(NULL == pBlockInfo)
	{
		AppLogOut("Create Blocks",APP_LOG_ERROR, 
			"NEW pBlockInfo 1 failed.\n");
		TRACE("[%s]--%s: ERROR: NEW pBlockInfo 1 failed.\n",
			__FILE__, __FUNCTION__);
		return ERR_CFG_FAIL;
	}
	memset(pBlockInfo, 0, sizeof(TL1_BLOCK_INFO)*iBlockNum);

	g_TL1Globals.TL1ModelInfo.iBlocksNum = iBlockNum;
	g_TL1Globals.TL1ModelInfo.pBlockInfo = pBlockInfo;

	//reset pBlockInfo
	pCurBlockInfo = pBlockInfo;
	for(i = 0; i < iTypeMapNum; i++, pCurBlockInfo++)
	{
		pCurBlockInfo->bIsValid = FALSE;
		pCurBlockInfo->pszMainAidName = NULL;
		pCurBlockInfo->iTL1EquipNum = 0;
		pCurBlockInfo->pstTL1EquipList = NULL;
		pCurBlockInfo->pTypeInfo = NULL;
	}

	//init pBlockInfo
	bIsFetalFail = FALSE;
	pCurBlockInfo = pBlockInfo;
	pCurTypeMap = pTypeMap;
	for(i = 0; i < iTypeMapNum; i++, pCurTypeMap++, pCurBlockInfo++)
	{
		pCurBlockInfo->pTypeInfo = pCurTypeMap;

		//check whether MapEntries have one item at least
		if(pCurTypeMap->pstAidGroupInfo->iMapEntriesNum <= 0)
		{
			continue;
		}

		//check whether iEquipTypeID iEquipIDStart and iEquipIDEnd are valid.
		//then compute iEquipNum
		bIsValidEquipTypeID = TRUE;
		iEquipNum = 0;
		if((pCurTypeMap->iEquipIDStart > 0)
			&& (pCurTypeMap->iEquipIDEnd >= pCurTypeMap->iEquipIDStart))
		{
			//equip ID is valid
			iEquipNum = pCurTypeMap->iEquipIDEnd - pCurTypeMap->iEquipIDStart + 1;
			bIsValidEquipTypeID = FALSE;
		}
		else if(pCurTypeMap->iEquipTypeID > 0)
		{
			//equip type ID is valid
			//find all equipments with the same equip type ID
			for(j = 0, pCurEquipInfo = pTotalEquipInfo; j < iTotalEquipNum; j++, pCurEquipInfo++)
			{
				if(pCurEquipInfo->iEquipTypeID == pCurTypeMap->iEquipTypeID)
				{
					iEquipNum++;
				}
			}
			bIsValidEquipTypeID = TRUE;
		}
		else
		{
			//both equip ID and equip type ID are invalid
			continue;
		}

		if(iEquipNum <= 0)//no equipment is valid
		{
			continue;
		}


		pCurBlockInfo->pstTL1EquipList = NEW(TL1_EQUIP_INFO, iEquipNum);
		if(NULL == pCurBlockInfo->pstTL1EquipList)
		{
			bIsFetalFail = TRUE;
			break;
		}
		memset(pCurBlockInfo->pstTL1EquipList, 0, sizeof(TL1_EQUIP_INFO)*iEquipNum);

		//init pstTL1EquipList
		iTemp = 0;
		//scan all valid equipments in MonitoringSolution.cfg
		//get equipment pointers
		for(j = 0, pCurEquipInfo = pTotalEquipInfo; j < iTotalEquipNum; j++, pCurEquipInfo++)
		{
			if(iTemp >= iEquipNum)
			{
				break;
			}

			if(bIsValidEquipTypeID)
			{
				//scan equipment by iEquipTypeID 
				if(pCurEquipInfo->iEquipTypeID == pCurTypeMap->iEquipTypeID)
				{
					//store equipment pointer
					pCurBlockInfo->pstTL1EquipList[iTemp].pstEquip = pCurEquipInfo;
					iTemp++;
				}
			}
			else
			{
				//scan equipment by iEquipIDStart and iEquipIDEnd
				if((pCurEquipInfo->iEquipID >= pCurTypeMap->iEquipIDStart)
					&& (pCurEquipInfo->iEquipID <= pCurTypeMap->iEquipIDEnd))
				{
					//store equipment pointer
					pCurBlockInfo->pstTL1EquipList[iTemp].pstEquip = pCurEquipInfo;
					iTemp++;
				}
			}
		}

		pCurBlockInfo->bIsValid = TRUE;
		pCurBlockInfo->iTL1EquipNum = iTemp;
		pCurBlockInfo->pszMainAidName = pCurTypeMap->pstAidGroupInfo->szAidGroupName;

		//init each item in pstTL1EquipList
		for(j = 0; j < pCurBlockInfo->iTL1EquipNum; j++)
		{
			pstCurTL1Equip = &pCurBlockInfo->pstTL1EquipList[j];


			if( !pCurTypeMap->pstAidGroupInfo->bAidSubGroupNameExist ||
					TL1_CHAR_NULL == pCurTypeMap->pstAidGroupInfo->szAidSubGroupNamePrefix[0] )
			{
				pstCurTL1Equip->iSubAidNameSuffix = 0;

				snprintf(pstCurTL1Equip->szSubAidName,
					sizeof(pstCurTL1Equip->szSubAidName),
					"");
			}
			else if ( pCurTypeMap->iAidSubGroupSuffixStart > 0 )
			{
				
				pstCurTL1Equip->iSubAidNameSuffix = j + pCurTypeMap->iAidSubGroupSuffixStart;
				
				switch ( pstCurTL1Equip->pstEquip->iEquipTypeID )
				{
					case RECT_STD_EQUIP_ID:
					case CONV_STD_EQUIP_ID:
					case MPPT_STD_EQUIP_ID:
					{
						iError = DxiGetData(VAR_A_SIGNAL_VALUE,
									pstCurTL1Equip->pstEquip->iEquipID,
									SIG_ID_RECT_CONV_POSITION,
									&iBufLen,
									&pSigValue,
									0);
						if ( iError == ERR_DXI_OK && pSigValue->varValue.ulValue > 0 )
						{
							pstCurTL1Equip->iSubAidNameSuffix = pSigValue->varValue.ulValue;
						}	
						break;				
					}
				}		
							
				snprintf(pstCurTL1Equip->szSubAidName,
					sizeof(pstCurTL1Equip->szSubAidName),
					"%s%d",
					pCurTypeMap->pstAidGroupInfo->szAidSubGroupNamePrefix,
					pstCurTL1Equip->iSubAidNameSuffix);
			}
			else
			{
				snprintf(pstCurTL1Equip->szSubAidName,
					sizeof(pstCurTL1Equip->szSubAidName),
					"%s",
					pCurTypeMap->pstAidGroupInfo->szAidSubGroupNamePrefix);
			}		

			pstCurTL1Equip->ppAlmSigValList = NEW(SIG_BASIC_VALUE*, pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);
			pstCurTL1Equip->ppSampSigValList = NEW(SIG_BASIC_VALUE*, pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);
			pstCurTL1Equip->ppSetSigValList = NEW(SIG_BASIC_VALUE*, pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);
			pstCurTL1Equip->piSampSigInfoTypeList = NEW(int, pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);
			pstCurTL1Equip->piSetSigInfoTypeList = NEW(int, pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);

			if((NULL == pstCurTL1Equip->ppAlmSigValList)
				|| (NULL == pstCurTL1Equip->ppSampSigValList)
				|| (NULL == pstCurTL1Equip->ppSetSigValList)
				|| (NULL == pstCurTL1Equip->piSampSigInfoTypeList)
				|| (NULL == pstCurTL1Equip->piSetSigInfoTypeList)
				)
			{
				bIsFetalFail = TRUE;//need exit
				break;
			}

			memset(pstCurTL1Equip->ppAlmSigValList,
				0,
				sizeof(SIG_BASIC_VALUE*)*pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);

			memset(pstCurTL1Equip->ppSampSigValList,
				0,
				sizeof(SIG_BASIC_VALUE*)*pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);

			memset(pstCurTL1Equip->ppSetSigValList,
				0,
				sizeof(SIG_BASIC_VALUE*)*pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);

			memset(pstCurTL1Equip->piSampSigInfoTypeList,
				0,
				sizeof(int)*pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);

			memset(pstCurTL1Equip->piSetSigInfoTypeList,
				0,
				sizeof(int)*pCurTypeMap->pstAidGroupInfo->iMapEntriesNum);


			pstCurTL1Equip->iSigCount = pCurTypeMap->pstAidGroupInfo->iMapEntriesNum;

			//init each item of pMapEntriesInfo
			pCurMapEntry = pCurTypeMap->pstAidGroupInfo->pMapEntriesInfo;
			for(k = 0; k < pCurTypeMap->pstAidGroupInfo->iMapEntriesNum; k++, pCurMapEntry++)
			{
				//get alarm signal pointer
				iEquipID = pstCurTL1Equip->pstEquip->iEquipID;
				iSigTypeID = pCurMapEntry->stAlarmSig.iSigTypeID;
				iSigID = pCurMapEntry->stAlarmSig.iSigID;

				if((iEquipID <= 0)
					||(iSigTypeID < 0)
					||(iSigID <= 0))
				{
					pstCurTL1Equip->ppAlmSigValList[k] = NULL;
				}
				else
				{
					iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipID,
						DXI_MERGE_SIG_ID(iSigTypeID, iSigID),
						&iBufLen,
						(void *)&pstCurTL1Equip->ppAlmSigValList[k],
						0);
				}

				//get sample signal pointer
				if(pCurMapEntry->stSampleSig.iRelevantEquipListIndex <= 0)
				{
					iEquipID = pstCurTL1Equip->pstEquip->iEquipID;
				}
				else
				{
					if(pCurMapEntry->stSampleSig.iRelevantEquipListIndex <= pstCurTL1Equip->pstEquip->iRelevantEquipListLength)
					{
						iEquipID = pstCurTL1Equip->pstEquip->pRelevantEquipList[pCurMapEntry->stSampleSig.iRelevantEquipListIndex - 1];
					}
					else
					{
						iEquipID = -1;
					}
				}
				iSigTypeID = pCurMapEntry->stSampleSig.iSigTypeID;
				iSigID = pCurMapEntry->stSampleSig.iSigID;


				if((iEquipID <= 0)
					||(iSigTypeID < 0)
					||(iSigID <= 0))
				{
					pstCurTL1Equip->ppSampSigValList[k] = NULL;
				}
				else
				{
					iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipID,
						DXI_MERGE_SIG_ID(iSigTypeID, iSigID),
						&iBufLen,
						(void *)&pstCurTL1Equip->ppSampSigValList[k],
						0);
				}
				pstCurTL1Equip->piSampSigInfoTypeList[k] = iSigTypeID;

				//get setting signal pointer
				if(pCurMapEntry->stSettingSig.iRelevantEquipListIndex <= 0)
				{
					iEquipID = pstCurTL1Equip->pstEquip->iEquipID;
				}
				else
				{
					if(pCurMapEntry->stSettingSig.iRelevantEquipListIndex <= pstCurTL1Equip->pstEquip->iRelevantEquipListLength)
					{
						iEquipID = pstCurTL1Equip->pstEquip->pRelevantEquipList[pCurMapEntry->stSettingSig.iRelevantEquipListIndex - 1];
					}
					else
					{
						iEquipID = -1;
					}
				}
				iSigTypeID = pCurMapEntry->stSettingSig.iSigTypeID;
				iSigID = pCurMapEntry->stSettingSig.iSigID;


				if((iEquipID <= 0)
					||(iSigTypeID < 0)
					||(iSigID <= 0))
				{
					pstCurTL1Equip->ppSetSigValList[k] = NULL;
				}
				else
				{
					iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipID,
						DXI_MERGE_SIG_ID(iSigTypeID, iSigID),
						&iBufLen,
						(void *)&pstCurTL1Equip->ppSetSigValList[k],
						0);
				}
				pstCurTL1Equip->piSetSigInfoTypeList[k] = iSigTypeID;
			}

		}

		if(j < pCurBlockInfo->iTL1EquipNum)//happen some error, need exit
		{
			bIsFetalFail = TRUE;
			break;
		}

	}

	if(bIsFetalFail)
	{
		AppLogOut("Create Blocks",APP_LOG_ERROR, 
			"NEW pBlockInfo 2 failed.\n");
		TRACE("[%s]--%s: ERROR: init pBlockInfo 2 failed.\n",
			__FILE__, __FUNCTION__);
		return ERR_CFG_FAIL;
	}


	//build data for web display
	//process each Aid Group Info
	for(i = 0, pstCurAidGroupInfo = pstModelCfgInfo->pstAIDGroupInfo; 
		i < pstModelCfgInfo->iAIDGroupNum;
		i++, pstCurAidGroupInfo++)
	{
		pstCurAidGroupInfo->iTL1EquipCount = 0;

		//find all BlockInfo with the same iAidGroupID
		for(j = 0, pCurBlockInfo = pBlockInfo; j < iBlockNum; j++, pCurBlockInfo++)
		{
			if(pstCurAidGroupInfo->iTL1EquipCount >= TL1_AID_GROUP_OUTPUT_EQUIP_MAX)
			{
				break;
			}

			if(!pCurBlockInfo->bIsValid)
			{
				continue;
			}

			if(pCurBlockInfo->pTypeInfo->iAidGroupID == pstCurAidGroupInfo->iIndex)
			{
				for(k = 0; k < pCurBlockInfo->iTL1EquipNum; k++)
				{
					pstCurAidGroupInfo->pstTL1EquipList[pstCurAidGroupInfo->iTL1EquipCount] = 
						&pCurBlockInfo->pstTL1EquipList[k];
					pstCurAidGroupInfo->iTL1EquipCount++;
				}
			}
		}
	}


	//3. OrderEquipID
	//OrderEquipID(iTotalEquipNum, pTotalEquipInfo);

	return ERR_CFG_OK;
}




int TL1_UpdateBlocksData(void)
{
	TL1_EQUIP_INFO			*pstCurTL1Equip = NULL;
	TL1_BLOCK_INFO			*pBlockInfo = NULL;
	TL1_BLOCK_INFO			*pCurBlockInfo = NULL;
	int						iBlockNum = 0;
	int						i, j;


	iBlockNum = g_TL1Globals.TL1ModelInfo.iBlocksNum;
	pBlockInfo = g_TL1Globals.TL1ModelInfo.pBlockInfo;

	for(i = 0, pCurBlockInfo = pBlockInfo; i < iBlockNum; i++, pCurBlockInfo++)
	{
		if( !pCurBlockInfo->bIsValid )
		{
			continue;
		}

		for(j = 0; j < pCurBlockInfo->iTL1EquipNum; j++)
		{
			pstCurTL1Equip = &pCurBlockInfo->pstTL1EquipList[j];

			if( !pCurBlockInfo->pTypeInfo->pstAidGroupInfo->bAidSubGroupNameExist ||
					TL1_CHAR_NULL == pCurBlockInfo->pTypeInfo->pstAidGroupInfo->szAidSubGroupNamePrefix[0] )
			{
				snprintf(pstCurTL1Equip->szSubAidName,
					sizeof(pstCurTL1Equip->szSubAidName),
					"");
			}
			
			else if ( pCurBlockInfo->pTypeInfo->iAidSubGroupSuffixStart > 0 )
			{
				snprintf(pstCurTL1Equip->szSubAidName,
					sizeof(pstCurTL1Equip->szSubAidName),
					"%s%d",
					pCurBlockInfo->pTypeInfo->pstAidGroupInfo->szAidSubGroupNamePrefix,
					pstCurTL1Equip->iSubAidNameSuffix);
					 
			}
			else
			{
				snprintf(pstCurTL1Equip->szSubAidName,
					sizeof(pstCurTL1Equip->szSubAidName),
					"%s",
					pCurBlockInfo->pTypeInfo->pstAidGroupInfo->szAidSubGroupNamePrefix);
			}
		}
	}

	return ERR_DXI_OK;
}



static void PrintModel(void)
{
	//TL1MODEL_CONFIG_INFO		*pstModelConfig = &g_TL1Globals.TL1ModelConfig;
	TL1_MODEL_INFO				*pstModelInfo = &g_TL1Globals.TL1ModelInfo;
	TL1_BLOCK_INFO 				*pstCurBlockInfo = NULL;
	//TL1_COMMON_CONFIG			*pstCommonConfig = &g_TL1Globals.CommonConfig;
	TL1_EQUIP_INFO				*pstTL1Equip = NULL;
	int							i, j;

	printf("---------- TL1ModelInfo -------\n");
	printf("iBlocksNum=%d\n", pstModelInfo->iBlocksNum);

	for(i = 0; i < pstModelInfo->iBlocksNum; i++)
	{
		pstCurBlockInfo = &pstModelInfo->pBlockInfo[i];

		printf("[%d],bIsValid=%d, iTL1EquipNum=%d, iMapEntriesNum=%d\n", 
			i,
			pstCurBlockInfo->bIsValid, 
			pstCurBlockInfo->iTL1EquipNum,
			pstCurBlockInfo->pTypeInfo->pstAidGroupInfo->iMapEntriesNum);

		if( !pstCurBlockInfo->bIsValid )
		{
			continue;
		}

		printf("[%d],iIndex=%d,iAidGroupID=%d,iMapentriesID=%d,szAidGroupName=%s,szAidSubGroupNamePrefix=%s,iAidSubGroupSuffixStart=%d,"
			"iEquipTypeID=%d, iEquipIDStart=%d, iEquipIDEnd=%d\n", 
			i,
			pstCurBlockInfo->pTypeInfo->iIndex,
			pstCurBlockInfo->pTypeInfo->iAidGroupID,
			pstCurBlockInfo->pTypeInfo->pstAidGroupInfo->iMapentriesID,
			pstCurBlockInfo->pTypeInfo->pstAidGroupInfo->szAidGroupName,
			pstCurBlockInfo->pTypeInfo->pstAidGroupInfo->szAidSubGroupNamePrefix,
			pstCurBlockInfo->pTypeInfo->iAidSubGroupSuffixStart,
			pstCurBlockInfo->pTypeInfo->iEquipTypeID,
			pstCurBlockInfo->pTypeInfo->iEquipIDStart,
			pstCurBlockInfo->pTypeInfo->iEquipIDEnd
			);

		for(j = 0; j < pstCurBlockInfo->iTL1EquipNum; j++)
		{
			pstTL1Equip = &pstCurBlockInfo->pstTL1EquipList[j];
			printf("----[%d,%d]szSubAidName=%s, iSigCount=%d, pstEquip=%p", i, j,
				pstTL1Equip->szSubAidName,
				pstTL1Equip->iSigCount,
				pstTL1Equip->pstEquip);

			if(NULL != pstTL1Equip->pstEquip)
			{
				printf(",pEquipName=%s", pstTL1Equip->pstEquip->pEquipName->pFullName[0]);
			}
			printf("\n");
		}
		

	}
	printf("--------------------------------\n");


}


/*==========================================================================*
 * FUNCTION : TL1_InitConfig
 * PURPOSE  : 
 * CALLS    : LoadCommonConfig  
 *			  LoadModelMapConfig
 *			  SortMapEntries
 *			  CreateBlocks
 * CALLED BY: InitTL1Globals (service_provider.c)
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 20:40
 *==========================================================================*/
int TL1_InitConfig(void)
{
	int iMapNum;
	TL1_TYPE_MAP_INFO *pTypeInfo;  /* for sorting mapentries */

	/* read Common Config */
	if (LoadCommonConfig() != ERR_CFG_OK )
	{
		return ERR_CFG_FAIL;
	}

	/* read Model Config */
	if (LoadModelMapConfig() != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	//print model config info
	iMapNum = g_TL1Globals.TL1ModelConfig.iTypeMapNum;
	pTypeInfo = g_TL1Globals.TL1ModelConfig.pTypeMapInfo;

	/* init Model */
    if (CreateBlocks() != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

#ifdef _SHOW_TL1_CONFIG_INFO
	PrintModel();
#endif //_SHOW_TL1_CONFIG_INFO

	return ERR_CFG_OK;
}


/* function prototype for common config file item modification */
typedef BOOL (*HANDLE_CFG_ITEM) (int nVarID, 
						TL1_COMMON_CONFIG *pUserCfg, 
						CONFIG_FILE_MODIFIER *pModififier,
						char *szContent);

/*==========================================================================*
 * FUNCTION : HandleProtocolType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:16
 *==========================================================================*/
static BOOL HandleProtocolType(IN int nVarID, 
						IN TL1_COMMON_CONFIG *pUserCfg, 
						OUT CONFIG_FILE_MODIFIER *pModifier,
						OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_PROTOCOL_TYPE))
	{
		sprintf(szContent, "%d%c", pUserCfg->iProtocolType, '\0');

		DEF_MODIFIER_ITEM(pModifier, 
			PROTOCOL_TYPE, 
			0, 
			0, 
			szContent);
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleADR
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModifier, char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 10:16
 *==========================================================================*/
static BOOL HandleADR(IN int nVarID, 
					   IN TL1_COMMON_CONFIG *pUserCfg, 
					   OUT CONFIG_FILE_MODIFIER *pModifier,
					   OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_ADR))
	{
		sprintf(szContent, "%d%c", pUserCfg->byADR, '\0');

		DEF_MODIFIER_ITEM(pModifier, 
			TL1_ADR, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleMediaType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:31
 *==========================================================================*/
static BOOL HandleMediaType(IN int nVarID, 
							IN TL1_COMMON_CONFIG *pUserCfg, 
							OUT CONFIG_FILE_MODIFIER *pModififier,
							OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_MEDIA_TYPE))
	{
		sprintf(szContent, "%d%c", pUserCfg->iMediaType, '\0');
	
		DEF_MODIFIER_ITEM(pModififier, 
			OPERATION_MEDIA, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


static BOOL HandleCommPortParam(IN int nVarID, 
								IN TL1_COMMON_CONFIG *pUserCfg, 
								OUT CONFIG_FILE_MODIFIER *pModififier,
								OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_MEDIA_PORT_PARAM))
	{
		strcpy(szContent, pUserCfg->szCommPortParam);
	
		DEF_MODIFIER_ITEM(pModififier, 
			MEDIA_PORT_PARAM, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandlePortActivation
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:33
 *==========================================================================*/
static BOOL HandlePortActivation(IN int nVarID, 
						   IN TL1_COMMON_CONFIG *pUserCfg, 
						   OUT CONFIG_FILE_MODIFIER *pModififier,
						   OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_PORT_ACTIVATION))
	{
		pUserCfg->iPortActivation != 0 ?
			(void)strcpy(szContent, "1"): (void)strcpy(szContent, "0");

		DEF_MODIFIER_ITEM(pModififier, 
			PORT_ACTIVATION, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandlePortKeepAlive
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:33
 *==========================================================================*/
static BOOL HandlePortKeepAlive(IN int nVarID, 
						   IN TL1_COMMON_CONFIG *pUserCfg, 
						   OUT CONFIG_FILE_MODIFIER *pModififier,
						   OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_PORT_KEEP_ALIVE))
	{
		pUserCfg->iPortKeepAlive != 0 ?
			(void)strcpy(szContent, "1"): (void)strcpy(szContent, "0");

		DEF_MODIFIER_ITEM(pModififier, 
			PORT_KEEP_ALIVE, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleSessionTimeout
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:31
 *==========================================================================*/
static BOOL HandleSessionTimeout(IN int nVarID, 
							IN TL1_COMMON_CONFIG *pUserCfg, 
							OUT CONFIG_FILE_MODIFIER *pModififier,
							OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_SESSION_TIMEOUT))
	{
		sprintf(szContent, "%d%c", pUserCfg->iSessionTimeout, '\0');
	
		DEF_MODIFIER_ITEM(pModififier, 
			SESSION_TIMEOUT, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleAutoLoginUser
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:31
 *==========================================================================*/
static BOOL HandleAutoLoginUser(IN int nVarID, 
								IN TL1_COMMON_CONFIG *pUserCfg, 
								OUT CONFIG_FILE_MODIFIER *pModififier,
								OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_AUTO_LOGIN_USER))
	{
		//for empty string type config item, fill '-'
		if (pUserCfg->szAutoLoginUser[0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szAutoLoginUser);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			AUTO_LOGIN_USER, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleSystemIdentifier
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:31
 *==========================================================================*/
static BOOL HandleSystemIdentifier(IN int nVarID, 
								IN TL1_COMMON_CONFIG *pUserCfg, 
								OUT CONFIG_FILE_MODIFIER *pModififier,
								OUT char *szContent)
{
	if (CFG_ITEM_TEST(nVarID, TL1_CFG_SYSTEM_IDENTIFIER))
	{
		//for empty string type config item, fill '-'
		if (pUserCfg->szSystemIdentifier[0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szSystemIdentifier);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			SYSTEM_IDENTIFIER, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

#define TL1_LINE_MAX_LEN					1024
#define TL1_GET_STR_WITH_NA(pArgStr)		((TL1_CHAR_NULL == pArgStr[0])? "NA":pArgStr)


/*==========================================================================*
 * FUNCTION : HandleAidGroupInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:16
 *==========================================================================*/
static BOOL HandleAidGroupInfo(IN int nVarID, 
							IN TL1_COMMON_CONFIG *pUserCfg, 
							OUT CONFIG_FILE_MODIFIER *pModifier,
							OUT char *szContent)
{
	TL1MODEL_CONFIG_INFO		*pModelCfg = &g_TL1Globals.TL1ModelConfig;
	TL1_AID_GROUP_INFO_W		*pstAidGroupInfoForWrite = &pUserCfg->stAidGroupInfoForWrite;
	TL1_AID_GROUP_INFO			*pstAidGroupInfoFromUserCfg = &pstAidGroupInfoForWrite->stAIDGroupInfo;
	TL1_AID_GROUP_INFO			*pstCurAidGroupInfoFromOriCfg = NULL;
	int							i;
	int							iLen;


	if (CFG_ITEM_TEST(nVarID, TL1_CFG_AID_GROUP_INFO))
	{
		//check modify type for deciding which data need to be modified
		if((TL1_CFG_MODIFY_TYPE_ONLY_AID_GROUP == pstAidGroupInfoForWrite->iModifyType)
			|| (TL1_CFG_MODIFY_TYPE_AID_GROUP_AND_MAPENTRIES == pstAidGroupInfoForWrite->iModifyType))
		{
			//find and modify data
			for(i = 0, pstCurAidGroupInfoFromOriCfg = pModelCfg->pstAIDGroupInfo;
				i < pModelCfg->iAIDGroupNum;
				i++, pstCurAidGroupInfoFromOriCfg++)
			{
				if(pstCurAidGroupInfoFromOriCfg->iIndex == pstAidGroupInfoFromUserCfg->iIndex)
				{
					pstCurAidGroupInfoFromOriCfg->iAidGroupType = pstAidGroupInfoFromUserCfg->iAidGroupType;

					strncpyz(pstCurAidGroupInfoFromOriCfg->szAidGroupName,
						pstAidGroupInfoFromUserCfg->szAidGroupName,
						sizeof(pstCurAidGroupInfoFromOriCfg->szAidGroupName));

					strncpyz(pstCurAidGroupInfoFromOriCfg->szAidSubGroupNamePrefix,
						pstAidGroupInfoFromUserCfg->szAidSubGroupNamePrefix,
						sizeof(pstCurAidGroupInfoFromOriCfg->szAidSubGroupNamePrefix));

					break;
				}
			}
			
			//if data changed, output data to file
			if(i < pModelCfg->iAIDGroupNum)
			{
				iLen = 0;

				for(i = 0, pstCurAidGroupInfoFromOriCfg = pModelCfg->pstAIDGroupInfo;
					i < pModelCfg->iAIDGroupNum;
					i++, pstCurAidGroupInfoFromOriCfg++)
				{
					iLen += snprintf(szContent + iLen,
						TL1_LINE_MAX_LEN,
						"%d"				//1.Index
						"\t\t%s"			//2.Description
						"\t\t\t\t%s"		//3.AID Group Name
						"\t\t\t%s"			//4.AID SubGroup Name Prefix
						"\t\t\t\t%s"		//5.AID Group Type
						"\t\t\t%d"			//6.Map Entries ID
						"\t\t\t%s"			//7.AID SubGroup Name Exist
						"\t\t\t\t%d"		//8.AID SubGroup Name Suffix Max Len
						"\r\n",				//line end
						pstCurAidGroupInfoFromOriCfg->iIndex,
						TL1_GET_STR_WITH_NA(pstCurAidGroupInfoFromOriCfg->szDescription),
						TL1_GET_STR_WITH_NA(pstCurAidGroupInfoFromOriCfg->szAidGroupName),
						TL1_GET_STR_WITH_NA(pstCurAidGroupInfoFromOriCfg->szAidSubGroupNamePrefix),
						(0 == pstCurAidGroupInfoFromOriCfg->iAidGroupType)? "EQPT": "ENV",
						pstCurAidGroupInfoFromOriCfg->iMapentriesID,
						(pstCurAidGroupInfoFromOriCfg->bAidSubGroupNameExist)? "Y": "N",
						pstCurAidGroupInfoFromOriCfg->iAidSubGroupNameSuffixMaxLen
						);
				}


				DEF_MODIFIER_ITEM(pModifier, 
					AID_GROUP_INFO_SEC, 
					0, 
					0, 
					szContent);

				return TRUE;
			}

		}
	}

	return FALSE;
}


inline void TL1ConverSignalIDToString(IN TL1_ACU_SIG *pSigID, IN int iMaxLen, OUT char *szOut)
{
	szOut[0] = TL1_CHAR_NULL;

	if(pSigID->iRelevantEquipListIndex < 0)
	{
		if(pSigID->iSigID <= 0)
		{
			snprintf(szOut, iMaxLen, "NA");
		}
		else
		{
			snprintf(szOut, iMaxLen, "%d", pSigID->iSigID);
		}
	}
	else
	{
		snprintf(szOut,
			iMaxLen,
			"[%d,%d,%d]",
			pSigID->iRelevantEquipListIndex,
			pSigID->iSigTypeID,
			pSigID->iSigID);
	}
}

/*==========================================================================*
 * FUNCTION : HandleMapentriesInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            TL1_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:16
 *==========================================================================*/
static BOOL HandleMapentriesInfo(IN int nVarID, 
								IN TL1_COMMON_CONFIG *pUserCfg, 
								OUT CONFIG_FILE_MODIFIER *pModifier,
								OUT char *szContent)
{
	TL1MODEL_CONFIG_INFO		*pModelCfg = &g_TL1Globals.TL1ModelConfig;
	TL1_AID_GROUP_INFO_W		*pstAidGroupInfoForWrite = &pUserCfg->stAidGroupInfoForWrite;
	TL1_AID_GROUP_INFO			*pstAidGroupInfoFromUserCfg = &pstAidGroupInfoForWrite->stAIDGroupInfo;
	TL1_AID_GROUP_INFO			*pstCurAidGroupInfoFromOriCfg = NULL;
	TL1_MAPENTRIES_INFO			*pCurMapEntriesInfoFromUserCfg = NULL;
	TL1_MAPENTRIES_INFO			*pCurMapEntriesInfoFromOriCfg = NULL;
	int							i, j;
	int							iLen;
	char						szAlarmIDStr[25], szSampleIDStr[25], szSettingIDStr[25];
	BOOL						bIsDataChanged = FALSE;
	static char					sl_szMapEntriesSecName[30];



	if (CFG_ITEM_TEST(nVarID, TL1_CFG_AID_GROUP_INFO))
	{
		//check modify type for deciding which data need to be modified
		if((TL1_CFG_MODIFY_TYPE_ONLY_MAPENTRIES == pstAidGroupInfoForWrite->iModifyType)
			|| (TL1_CFG_MODIFY_TYPE_AID_GROUP_AND_MAPENTRIES == pstAidGroupInfoForWrite->iModifyType))
		{
			//find the right AID Group
			for(i = 0, pstCurAidGroupInfoFromOriCfg = pModelCfg->pstAIDGroupInfo;
				i < pModelCfg->iAIDGroupNum;
				i++, pstCurAidGroupInfoFromOriCfg++)
			{
				if(pstCurAidGroupInfoFromOriCfg->iIndex == pstAidGroupInfoFromUserCfg->iIndex)
				{
					break;
				}
			}
			printf("HandleMapentriesInfo i=%d, pstAidGroupInfoFromUserCfg->iIndex=%d\n", i, pstAidGroupInfoFromUserCfg->iIndex);

			//exist the right AID Group in OriCfg
			if(i < pModelCfg->iAIDGroupNum)
			{
				//exist MapEntriesInfo
				if((pstAidGroupInfoFromUserCfg->iMapEntriesNum > 0)
					&& (NULL != pstAidGroupInfoFromUserCfg->pMapEntriesInfo))
				{
					bIsDataChanged = FALSE;

					//find and modify data
					//!!!just support a item, because Cfg_ModifyConfigFile just support a extra size MAX_EXTRA_SIZE
					//the value is 1024, it is about 3 items size, but we just use a item.
					//for(i = 0, pCurMapEntriesInfoFromUserCfg = pstAidGroupInfoFromUserCfg->pMapEntriesInfo; i < pstAidGroupInfoFromUserCfg->iMapEntriesNum; i++, pCurMapEntriesInfoFromUserCfg++)
					for(i = 0, pCurMapEntriesInfoFromUserCfg = pstAidGroupInfoFromUserCfg->pMapEntriesInfo;
						i < 1;
						i++, pCurMapEntriesInfoFromUserCfg++)
					{
						//find the right MapEntriesInfo
						for(j = 0, pCurMapEntriesInfoFromOriCfg = pstCurAidGroupInfoFromOriCfg->pMapEntriesInfo; j < pstCurAidGroupInfoFromOriCfg->iMapEntriesNum; j++, pCurMapEntriesInfoFromOriCfg++)
						{
							if(pCurMapEntriesInfoFromOriCfg->iIndex == pCurMapEntriesInfoFromUserCfg->iIndex)
							{
								break;
							}
						}
						printf("HandleMapentriesInfo j=%d, pstCurAidGroupInfoFromOriCfg->iMapEntriesNum=%d\n", j, pstCurAidGroupInfoFromOriCfg->iMapEntriesNum);
						printf("HandleMapentriesInfo pCurMapEntriesInfoFromUserCfg->iIndex=%d\n", pCurMapEntriesInfoFromUserCfg->iIndex);

						if(j < pstCurAidGroupInfoFromOriCfg->iMapEntriesNum)
						{
							bIsDataChanged = TRUE;

							pCurMapEntriesInfoFromOriCfg->bSigActive = pCurMapEntriesInfoFromUserCfg->bSigActive;

							strncpyz(pCurMapEntriesInfoFromOriCfg->szCondType,
								pCurMapEntriesInfoFromUserCfg->szCondType,
								sizeof(pCurMapEntriesInfoFromOriCfg->szCondType));

							strncpyz(pCurMapEntriesInfoFromOriCfg->szCondDesc,
								pCurMapEntriesInfoFromUserCfg->szCondDesc,
								sizeof(pCurMapEntriesInfoFromOriCfg->szCondDesc));

							pCurMapEntriesInfoFromOriCfg->iServiceAffectCode = pCurMapEntriesInfoFromUserCfg->iServiceAffectCode;
							pCurMapEntriesInfoFromOriCfg->iMonitorFormat = pCurMapEntriesInfoFromUserCfg->iMonitorFormat;

							strncpyz(pCurMapEntriesInfoFromOriCfg->szMonitorType,
								pCurMapEntriesInfoFromUserCfg->szMonitorType,
								sizeof(pCurMapEntriesInfoFromOriCfg->szMonitorType));
						}
					}

					printf("bIsDataChanged=%d\n", bIsDataChanged);

					//output data to file
					if(bIsDataChanged)
					{
						iLen = 0;
						for(i = 0, pCurMapEntriesInfoFromOriCfg = pstCurAidGroupInfoFromOriCfg->pMapEntriesInfo;
							i < pstCurAidGroupInfoFromOriCfg->iMapEntriesNum;
							i++, pCurMapEntriesInfoFromOriCfg++)
						{
							TL1ConverSignalIDToString(&pCurMapEntriesInfoFromOriCfg->stAlarmSig, sizeof(szAlarmIDStr), szAlarmIDStr);
							TL1ConverSignalIDToString(&pCurMapEntriesInfoFromOriCfg->stSampleSig, sizeof(szSampleIDStr), szSampleIDStr);
							TL1ConverSignalIDToString(&pCurMapEntriesInfoFromOriCfg->stSettingSig, sizeof(szSettingIDStr), szSettingIDStr);

							iLen += snprintf(szContent + iLen,
								TL1_LINE_MAX_LEN,
								"%d"				//1.Index
								"\t%s"				//2.Description
								"\t\t\t\t\t\t%d"	//3.TL1 Signal ID
								"\t\t%s"			//4.Signal Active
								"\t\t\t%s"			//5.Condition Type
								"\t\t\t%s"			//6.Condition Description
								"\t\t\t\t%d"		//7.Service Affect Code
								"\t\t\t\t%d"		//8.Monitor Format
								"\t\t%s"			//9.Monitor Type
								"\t\t\t%s"			//10.SCU+ Alarm Signal
								"\t\t\t%s"			//11.SCU+ Sample Signal
								"\t\t\t%s"			//12.SCU+ Setting Signal
								"\r\n",				//line end
								pCurMapEntriesInfoFromOriCfg->iIndex,
								TL1_GET_STR_WITH_NA(pCurMapEntriesInfoFromOriCfg->szDescription),
								pCurMapEntriesInfoFromOriCfg->iTL1SigID,
								(pCurMapEntriesInfoFromOriCfg->bSigActive)? "Y": "N",
								TL1_GET_STR_WITH_NA(pCurMapEntriesInfoFromOriCfg->szCondType),
								TL1_GET_STR_WITH_NA(pCurMapEntriesInfoFromOriCfg->szCondDesc),
								pCurMapEntriesInfoFromOriCfg->iServiceAffectCode,
								pCurMapEntriesInfoFromOriCfg->iMonitorFormat,
								TL1_GET_STR_WITH_NA(pCurMapEntriesInfoFromOriCfg->szMonitorType),
								TL1_GET_STR_WITH_NA(szAlarmIDStr),
								TL1_GET_STR_WITH_NA(szSampleIDStr),
								TL1_GET_STR_WITH_NA(szSettingIDStr));
						}

						printf("szContent=%s\n", szContent);
						

						GetMapEntriesSecName(
							MAPENTRIES_INFO_SEC,
							pstCurAidGroupInfoFromOriCfg->iMapentriesID,
							sl_szMapEntriesSecName,
							sizeof(sl_szMapEntriesSecName));
						
						printf("szMapEntriesSecName=%s\n", sl_szMapEntriesSecName);

						DEF_MODIFIER_ITEM(pModifier, 
							sl_szMapEntriesSecName, 
							0, 
							0, 
							szContent);

						return TRUE;
					}//if(bIsDataChanged)
				}
			}//if(i < pModelCfg->iAIDGroupNum)
		}
	}//if (CFG_ITEM_TEST(nVarID, TL1_CFG_AID_GROUP_INFO))

	return FALSE;
}



/*==========================================================================*
 * FUNCTION : TL1_GetModifiedFileBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID       : 
 *            TL1_COMMON_CONFIG  *pUserCfg    : 
 *            char           **pszOutFile : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 14:03
 *==========================================================================*/
BOOL TL1_GetModifiedFileBuf(IN int nVarID, 
							IN TL1_COMMON_CONFIG *pUserCfg,
							OUT char **pszOutFile)
{
	CONFIG_FILE_MODIFIER Modififier[CFG_ITEM_NUM];

	/* note: MAX_CFG_ITEM_LEN should be long enough! */
	char szCfgFileName[MAX_FILE_PATH]; 
	char szContent[CFG_ITEM_NUM][MAX_CFG_ITEM_LEN];
	int iRet, iIndex, iModifiers;
	BOOL bModified;

	const char *pLogText;  //for log

	HANDLE_CFG_ITEM fnHandlers[CFG_ITEM_NUM];


	/* init the handlers */
	fnHandlers[0] = HandleProtocolType;
	fnHandlers[1] = HandleADR;
	fnHandlers[2] = HandleMediaType;
	fnHandlers[3] = HandleCommPortParam;
	fnHandlers[4] = HandlePortActivation;
	fnHandlers[5] = HandlePortKeepAlive;
	fnHandlers[6] = HandleSessionTimeout;
	fnHandlers[7] = HandleAutoLoginUser;
    fnHandlers[8] = HandleSystemIdentifier;
	

	
	/* fill Modififier one by one */
	iModifiers = 0;
	for (iIndex = 0; iIndex < CFG_ITEM_NUM; iIndex++)
	{
		bModified = fnHandlers[iIndex](nVarID, 
			pUserCfg,
			&Modififier[iModifiers],
			szContent[iModifiers]);

		if (bModified)
		{
			iModifiers++;
		}
	}

	/* get the file buf */
	Cfg_GetFullConfigPath(CONFIG_FILE_COMMON, szCfgFileName, MAX_FILE_PATH);
	iRet = Cfg_ModifyConfigFile(szCfgFileName,
						 iModifiers,
						 Modififier,
						 pszOutFile);


	/* to simplify log action */
#define TASK_NAME        "Modifify TL1 common config file"
#define LOG_MODIFY_FILE(_pLogText)  \
	(AppLogOut(TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	switch (iRet)
	{
	case ERR_CFG_OK:
		break;

	case ERR_CFG_FILE_OPEN:
		pLogText = "Open TL1 Common Config file for modification failed.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_NO_MEMORY:
		pLogText = "No memory.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_FILE_READ:
		pLogText = "Read TL1 Common Config file for modification failed.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_PARAM:
		pLogText = "Something is wrong with CONFIG_FILE_MODIFIER param for "
			"modification.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	default:
		pLogText = "Unhandled error when create new TL1 Common Config file"
			" in memory.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;
	}

	return TRUE;
}
		

/*==========================================================================*
 * FUNCTION : TL1_UpdateCommonConfigFile
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 14:20
 *==========================================================================*/
BOOL TL1_UpdateCommonConfigFile(IN const char *szContent)
{
	FILE *pFile;
	char szCfgFileName[MAX_FILE_PATH]; 
	size_t count;

	Cfg_GetFullConfigPath(CONFIG_FILE_COMMON, szCfgFileName, MAX_FILE_PATH);
	pFile = fopen(szCfgFileName, "w");
	if (pFile == NULL)
	{
		return FALSE;
	}

	count = strlen(szContent);
	if (fwrite(szContent, 1, count, pFile) != count)
	{
		fclose(pFile);
		return FALSE;
	}

	fclose(pFile);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : TL1_GetModifiedFileBufForModelMap
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID       : 
 *            TL1_COMMON_CONFIG  *pUserCfg    : 
 *            char           **pszOutFile : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 14:03
 *==========================================================================*/
BOOL TL1_GetModifiedFileBufForModelMap(IN int nVarID, 
							IN TL1_COMMON_CONFIG *pUserCfg,
							OUT char **pszOutFile)
{
	CONFIG_FILE_MODIFIER Modififier[CFG_MODEL_MAP_ITEM_NUM];

	/* note: MAX_CFG_ITEM_LEN should be long enough! */
	char szCfgFileName[MAX_FILE_PATH]; 
	char szContent[CFG_MODEL_MAP_ITEM_NUM][MAX_CFG_MODEL_MAP_ITEM_LEN];
	int iRet, iIndex, iModifiers;
	BOOL bModified;

	const char *pLogText;  //for log

	HANDLE_CFG_ITEM fnHandlers[CFG_MODEL_MAP_ITEM_NUM];


	/* init the handlers */
	fnHandlers[0] = HandleAidGroupInfo;
	fnHandlers[1] = HandleMapentriesInfo;
	
	/* fill Modififier one by one */
	iModifiers = 0;
	for (iIndex = 0; iIndex < CFG_MODEL_MAP_ITEM_NUM; iIndex++)
	{
		bModified = fnHandlers[iIndex](nVarID, 
			pUserCfg,
			&Modififier[iModifiers],
			szContent[iModifiers]);

		if (bModified)
		{
			iModifiers++;
		}
	}

	/* get the file buf */
	Cfg_GetFullConfigPath(CONFIG_FILE_MODELMAP, szCfgFileName, MAX_FILE_PATH);
	iRet = Cfg_ModifyConfigFile(szCfgFileName,
						 iModifiers,
						 Modififier,
						 pszOutFile);


	/* to simplify log action */
#define MODIFY_MODEL_MAP_TASK_NAME			"Modifify TL1 model map file"
#define LOG_MODIFY_FILE_MODEL_MAP(_pLogText)  \
	(AppLogOut(MODIFY_MODEL_MAP_TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	switch (iRet)
	{
	case ERR_CFG_OK:
		break;

	case ERR_CFG_FILE_OPEN:
		pLogText = "Open TL1 model map file for modification failed.";
		LOG_MODIFY_FILE_MODEL_MAP(pLogText);
		return FALSE;

	case ERR_CFG_NO_MEMORY:
		pLogText = "No memory.";
		LOG_MODIFY_FILE_MODEL_MAP(pLogText);
		return FALSE;

	case ERR_CFG_FILE_READ:
		pLogText = "Read TL1 model map file for modification failed.";
		LOG_MODIFY_FILE_MODEL_MAP(pLogText);
		return FALSE;

	case ERR_CFG_PARAM:
		pLogText = "Something is wrong with CONFIG_FILE_MODIFIER param for "
			"modification.";
		LOG_MODIFY_FILE_MODEL_MAP(pLogText);
		return FALSE;

	default:
		pLogText = "Unhandled error when create new TL1 model map file"
			" in memory.";
		LOG_MODIFY_FILE_MODEL_MAP(pLogText);
		return FALSE;
	}

	return TRUE;
}
		

/*==========================================================================*
 * FUNCTION : TL1_UpdateModelMapFile
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 14:20
 *==========================================================================*/
BOOL TL1_UpdateModelMapFile(IN const char *szContent)
{
	FILE *pFile;
	char szCfgFileName[MAX_FILE_PATH]; 
	size_t count;

	Cfg_GetFullConfigPath(CONFIG_FILE_MODELMAP, szCfgFileName, MAX_FILE_PATH);
	pFile = fopen(szCfgFileName, "w");
	if (pFile == NULL)
	{
		return FALSE;
	}

	count = strlen(szContent);
	if (fwrite(szContent, 1, count, pFile) != count)
	{
		fclose(pFile);
		return FALSE;
	}

	fclose(pFile);

	return TRUE;
}




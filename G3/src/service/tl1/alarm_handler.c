/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : alarm_handler.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "tl1.h"

/* log Macros (AH = Alarm Handler) */
#define TL1_TASK_AH		"TL1_AH"

/* error log */
#define LOG_TL1_AH_E(_szLogText)  LOG_TL1_E(TL1_TASK_AH, _szLogText)

/* warning log */
#define LOG_TL1_AH_W(_szLogText)  LOG_TL1_W(TL1_TASK_AH, _szLogText) 

/* info log */
#define LOG_TL1_AH_I(_szLogText)  LOG_TL1_I(TL1_TASK_AH, _szLogText)


/* do in the config builder sub-module */
//static BOOL CheckNumbers (TL1_BASIC_ARGS *pThis, BYTE byRouteType);

//static void ConnectToRemote (TL1_BASIC_ARGS *pThis, BYTE byRouteType)

/*==========================================================================*
 * FUNCTION : TL1_RegisterAlarm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: AlarmProc
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis        : 
 *            BOOL            bDealyedAlarm : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 16:11
 *==========================================================================*/
void TL1_RegisterAlarm(IN OUT TL1_BASIC_ARGS *pThis, IN BOOL bDelayedAlarm)
{
	TL1_ALARM_HANDLER *pAlarmHandler;
	TL1_COMMON_CONFIG *pCommonCfg;


	pCommonCfg = &g_TL1Globals.CommonConfig;

	//TRACE("\nEnter TL1_SEND_ALARM!!!!:TL1_RegisterAlarm\n");

	/* alarm report function is disabled */
	if (!pCommonCfg->bReportInUse || pCommonCfg->iMaxAttempts == 0)
	{
		return;
	}

	pAlarmHandler = &pThis->alarmHandler;

	/* if is delayed alarm or last report is running, do not reset retry counter */
	if (!bDelayedAlarm && !pAlarmHandler->bAlarmAtHand)
	{
		pAlarmHandler->byAlarmRetryCounter = 0;
	}


	/* if last report is not running (means not using Route2,...) */
	if (!pAlarmHandler->bAlarmAtHand) 
	{
		pAlarmHandler->byCurReportRouteType = TL1_ROUTE_ALARM_1;
	}
	pAlarmHandler->bAlarmAtHand = TRUE;

	return;
}

/*==========================================================================*
 * FUNCTION : TL1_SendAlarm
 * PURPOSE  : set link-layer operation mode change flag and link-layer manager
 *            will send alarm later
 * CALLS    : 
 * CALLED BY: TL1_OnIdle
 *			
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 11:18
 *==========================================================================*/
void TL1_SendAlarm(IN OUT TL1_BASIC_ARGS *pThis)
{
	TL1_ALARM_HANDLER *pAlarmHandler;
	pAlarmHandler = &pThis->alarmHandler;

	/* set server mode exit flag */
	//pThis->bServerModeNeedExit = TRUE;

	TRACE("Enter TL1_SendAlarm!!\n");

	/* set client mode */
	//pThis->iOperationMode = TL1_MODE_CLIENT;
	TL1_DecodeAndPerform(pThis, (unsigned char *)"REPT ALM EQPT", FALSE);
	if(TL1_ERR_OK != TL1_SendRespData(pThis, (char *)pThis->szCmdRespBuff, sizeof(pThis->szCmdRespBuff), TRUE, FALSE))
	{
		TL1_BreakConnection(pThis);
	}

	pAlarmHandler->bAlarmAtHand = FALSE;


	return;
}

/*==========================================================================*
 * FUNCTION : TL1_AlarmReported
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: (TL1_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 16:25
 *==========================================================================*/
void TL1_AlarmReported (IN OUT TL1_BASIC_ARGS *pThis)
{
	TL1_ALARM_HANDLER *pAlarmHandler;

	pAlarmHandler = &pThis->alarmHandler;

	pAlarmHandler->bAlarmAtHand = FALSE;
	pAlarmHandler->byAlarmRetryCounter = 0;

	/* set link-layer flags */
	pThis->bServerModeNeedExit = FALSE;
	pThis->iOperationMode = TL1_MODE_SERVER;

	return;
}

/*==========================================================================*
 * FUNCTION : TL1_AlarmReportTimerProc
 * PURPOSE  : timer call back function
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hTimerOwner : The thread ID of timer owner
 *            int     idTimer     : The ID of the timer
 *            void    *pArgs      : The args passed by timer owner, 
 *                                  here is TL1_BASIC_ARGS *pThis
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 14:12
 *==========================================================================*/
static int TL1_ReportTimerProc(IN HANDLE  hTimerOwner,
							IN int     idTimer,
							IN OUT void    *pArgs)
{
	TL1_BASIC_ARGS *pThis;
    UNUSED(hTimerOwner);

	pThis = (TL1_BASIC_ARGS *)pArgs;

	if (idTimer == TL1_ALARM_TIMER)
	{
#ifdef _DEBUG_ESR_REPORT
		TRACE_ESR_TIPS("Register Alarm by Timer");
#endif //_DEBUG_ESR_REPORT

		TL1_RegisterAlarm(pThis, TRUE);
	}
	
	return TIMER_KILL_THIS;
}


/*==========================================================================*
 * FUNCTION : TL1_ClientReportFailed
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: OnSend
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis          : 
 *            BOOL            bConnetedFailed : 
 * RETURN   :  void : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 14:39
 *==========================================================================*/
void TL1_ClientReportFailed(IN OUT TL1_BASIC_ARGS *pThis, 
							IN BOOL bConnetedFailed)
{
	TL1_COMMON_CONFIG *pConfig;
	TL1_ALARM_HANDLER *pAlarmHandler;
	int iCurRouteType, iTimerSetRet;
	BOOL bLeasedLine;    //for lease line media type

	char *pLog;
	char szLogText[TL1_LOG_TEXT_LEN];  //for log

	pConfig = &g_TL1Globals.CommonConfig;
	pAlarmHandler = &pThis->alarmHandler;
	iCurRouteType = pAlarmHandler->byCurReportRouteType;

	bLeasedLine = (pConfig->iMediaType == TL1_MEDIA_TYPE_LEASED_LINE) ? TRUE
		: FALSE;
	switch (iCurRouteType)
	{
	    case TL1_ROUTE_ALARM_1:
		   /* for leased line, only has one route */
		   if (!bLeasedLine)
		   {
				pAlarmHandler->byCurReportRouteType = TL1_ROUTE_ALARM_2;

				/* log and trace */
				if (bConnetedFailed)
				{
					LOG_TL1_AH_W("Alarm report using Route 1 failed(can not connect"
						" to MC), try to use Route 2");
				}
				else
				{
					LOG_TL1_AH_W("Alarm report using Route 1 failed, try to use "
						"Route 2");
				}
				break;
		   }

		 case TL1_ROUTE_ALARM_2:
		   /* for leased line, only has one route */
		   if (!bLeasedLine)
		   {
				pAlarmHandler->byCurReportRouteType = TL1_ROUTE_ALARM_3;

				/* log and trace */
				if (bConnetedFailed)
				{
					LOG_TL1_AH_W("Alarm report using Route 2 failed(can not connect"
						" to MC), try to use Route 3");
				}
				else
				{
					LOG_TL1_AH_W("Alarm report using Route 2 failed, try to use "
						"Route 3");
				}
				break;
		   }

	    case TL1_ROUTE_ALARM_3:
			if (++(pAlarmHandler->byAlarmRetryCounter) <
				pConfig->iMaxAttempts)
			{
				/* reset flags */
				pAlarmHandler->bAlarmAtHand = FALSE;
				pThis->bServerModeNeedExit = FALSE;
				pThis->iOperationMode = TL1_MODE_SERVER;

				/* log and trace */
				pLog = bLeasedLine ? "Lease Line" : "Route 3";
				
					
				if (bConnetedFailed)
				{
					sprintf(szLogText, "Alarm Report using %s failed(can not "
						"connect to MC). Try later", pLog);
					LOG_TL1_AH_W(szLogText);
				}
				else
				{
					sprintf(szLogText, "Alarm Report using %s failed. Try later",
						pLog);
					LOG_TL1_AH_W(szLogText);
				}
				

				/* statr timer, retry later */
				iTimerSetRet = Timer_Set(RunThread_GetId(NULL),
											TL1_ALARM_TIMER,
											pConfig->iAttemptElapse,
											TL1_ReportTimerProc,
											(DWORD)pThis);
				if (iTimerSetRet == ERR_TIMER_EXISTS)
				{
					TRACE_TL1_TIPS("Alarm Report Timer has existed");
				}
				if (iTimerSetRet == ERR_TIMER_SET_FAIL)
				{
					LOG_TL1_AH_E("Alarm Report failed(set re-alarmReport "
						"timer failed)");
				}
			}
			else
			{
				/* not try to report */
				pAlarmHandler->bAlarmAtHand = FALSE;
				pThis->bServerModeNeedExit = FALSE;
				pThis->iOperationMode = TL1_MODE_SERVER;

				sprintf(szLogText, "Alarm report failed after %d retries",
					pConfig->iMaxAttempts);
				LOG_TL1_AH_W(szLogText);	
			}
			break;

	   default: /* not be others */
		   TRACE_TL1_TIPS("Report route setting is error!!!");
		   break;
	}

	return;
}

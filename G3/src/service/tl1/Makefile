#/*==========================================================================*
# *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
# *                     ALL RIGHTS RESERVED
# *
# *  PRODUCT  : SCU+(Standard Controller Unit Plus)
# *
# *  FILENAME : Makefile
# *  CREATOR  : Frank Mao                DATE: 2004-09-10 19:29
# *  VERSION  : V1.00
# *  PURPOSE  : The make file for the TL1 application service.
# *
# *  HISTORY  : Modified by Han Tao
# *
# *==========================================================================*/

include $(ACU_SOURCE_DIR)/config_vars.mk

#stub objects dirs
CFG_DIR = $(ACU_SOURCE_DIR)/mainapp/config_mgmt
EQP_DIR = $(ACU_SOURCE_DIR)/mainapp/equip_mon

#the target output file
EXEC = tl1.so

#the head files the object files depend on
INCS = tl1.h 

#the object files to generate the target
OBJS = service_provider.o config_builder.o linklayer_manager.o frame_analysis.o  
OBJS += alarm_handler.o command_handler.o tl1_state_machine.o tl1_utility.o

#for self test
TEST_EXEC = test_tl1
TEST_OBJS = test_tl1.o  

#for init SCU+ model
TEST_OBJS += $(CFG_DIR)/cfg_load_stdbasic.o 
TEST_OBJS += $(CFG_DIR)/cfg_load_solution.o $(CFG_DIR)/cfg_load_stdequip.o   
TEST_OBJS += $(CFG_DIR)/cfg_loadrun.o $(CFG_DIR)/cfg_loader.o 

#for init SCU+ site
TEST_OBJS += $(EQP_DIR)/equip_mon_main.o $(EQP_DIR)/equip_mon_init.o 
TEST_OBJS += $(EQP_DIR)/equip_data_sample.o $(EQP_DIR)/equip_ctrl_cmd.o
TEST_OBJS += $(EQP_DIR)/equip_data_process.o $(EQP_DIR)/equip_exp_eval.o
TEST_OBJS += $(EQP_DIR)/equip_mon_unload.o

TEST_LIBS = $(EXEC) -ldl


#private shared libraries
LIBS = -ldl

all: $(EXEC)

cleanall:   
	-cd $(ACU_SOURCE_DIR); -ls; ./mkmodule clean; echo $?; -ls
	
$(PUBLIBS): 
	make -C $(ACU_SOURCE_DIR)/lib all || exit $?
	
$(EXEC): $(INCS) $(OBJS) $(LIBS) $(PUBINCS) $(PUBLIBS) 
	$(CC) -shared -fPIC $(CFLAGS) $(LDFLAGS) -o $@ $(OBJS) $(LIBS) $(PUBLIBS)
	ln -fs `pwd`/$(EXEC) $(ACU_SOURCE_DIR)/service/$(EXEC)
ifeq ($(ACU_MAKE_VERSION_N), debug)
install: $(EXEC)
	-mkdir -p $(ACU_TARGET_DIR)/service/
	#-cp $(EXEC) $(ACU_TARGET_DIR)/service/
	
	cp -fr $(EXEC) $(ACU_TARGET_DIR)/service/$(EXEC)
		
	#$(STRIP) -s -o $(ACU_TARGET_DIR)/service/$(EXEC) $(EXEC)
	chmod +x $(ACU_TARGET_DIR)/service/$(EXEC)	
else
install: $(EXEC)
	-mkdir -p $(ACU_TARGET_DIR)/service/
	#-cp $(EXEC) $(ACU_TARGET_DIR)/service/
	$(STRIP) -s -o $(ACU_TARGET_DIR)/service/$(EXEC) $(EXEC)
	#$(STRIP) -s -o $(ACU_TARGET_DIR)/service/$(EXEC) $(EXEC)
	chmod +x $(ACU_TARGET_DIR)/service/$(EXEC)
endif
		
#$(TEST_EXEC): $(EXEC) $(TEST_OBJS) $(PUBLIBS)
#	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(TEST_OBJS) $(TEST_LIBS) $(PUBLIBS)
	
test: $(TEST_EXEC) 
	-echo Test making ...

clean:
	-rm -f $(EXEC) *.gdb *.elf *.o $(OBJS) core $(TEST_EXEC)
	-rm -f make_*.txt *.map *.so
	-rm -f $(ACU_SOURCE_DIR)/service/$(EXEC)

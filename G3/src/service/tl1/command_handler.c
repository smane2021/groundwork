/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : command_handler.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "tl1.h"
#include <signal.h>



/* log Macros for Command Handler sub-module */
#define TL1_TASK_CMD		"TL1 Command process"

/* error log */
#define LOG_TL1_CMD_E(_szLogText)  LOG_TL1_E(TL1_TASK_CMD, _szLogText)

/* extended error log (logged with error code) */
#define LOG_TL1_CMD_E_EX(_szLogText, _errCode)  \
	LOG_TL1_E_EX(TL1_TASK_CMD, _szLogText, _errCode)

/* warning log */
#define LOG_TL1_CMD_W(_szLogText)  LOG_TL1_W(TL1_TASK_CMD, _szLogText)

/* info log */
#define LOG_TL1_CMD_I(_szLogText)  LOG_TL1_I(TL1_TASK_CMD, _szLogText)
	
/* used to init command handlers */
#define DEF_TL1_CMD_HANDLER(_p, _szCmdType, _bWriteFlag, _bIsSpecialCommand, __iLevel, _funExcute) \
          ((_p)->szCmdType = (_szCmdType), \
		  (_p)->bIsWriteCommand = (_bWriteFlag), \
		  (_p)->bIsSpecialCommand = (_bIsSpecialCommand), \
		  (_p)->iLevel = (int)(__iLevel), \
		   (_p)->funExecute = _funExcute)


#define IS_ALARMBLOCKED_ALARM(_pAlarmSigValue)  \
	((_pAlarmSigValue)->pEquipInfo->iEquipTypeID == STD_ID_ACU_SYSTEM_EQUIP &&  \
			(_pAlarmSigValue)->pStdSig->iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_A_ALARM_OUTGOING_BLOCKED))



/*==========================================================================*
 * FUNCTION : Power
 * PURPOSE  : simple assistant function
 * CALLS    : 
 * CALLED BY: GetDigitalStr
 * ARGUMENTS: int  x : 
 *            int  y : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 10:46
 *==========================================================================*/
static int Power(IN int x, IN int y)
{
	int i;
	int value = 1;

	for (i = 0; i < y; i++)
	{
		value *= x;
	}

	return value;
}

/*==========================================================================*
 * FUNCTION : ConvertTime
 * PURPOSE  : conver ydn23  time to UTC or convert UTC time to ydn23 time, ydn23 time zone is 8
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: time_t* pTime    : 
 *            BOOL bUTCToLocal  : TRUE, UTC time to ydn23 time , that is to add the zone8 , else
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-11-10 15:00
 *==========================================================================*/
BOOL ConvertTime(IN time_t* pTime, IN BOOL bUTCToYDN)
{
	SIG_BASIC_VALUE* pSigValue;

	int nBufLen;

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
		SIG_ID_TIME_ZONE_SET,
		&nBufLen,			
		&pSigValue,			
		0) != ERR_DXI_OK)
	{	
		return FALSE;
	}

	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{	
		int nTimeOffset = (pSigValue->varValue.lValue) * 3600 / 2;

		if (bUTCToYDN)
		{
			*pTime += nTimeOffset;
		}
		else//ydn23 time to UTC time
		{
			*pTime -= nTimeOffset;
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : *GetDigitalStr
 * PURPOSE  : change BOOL value to YDN digital data format
 * CALLS    : Power
 * CALLED BY: GetSigValues
 * ARGUMENTS: SIG_ENUM  *peValue : the number of the array is 4.
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 10:30
 *==========================================================================*/
static unsigned char GetDigitalStr(IN SIG_ENUM *peValue)
{
	unsigned char cValue;
	int i;

	cValue = 0;

	/* get the nibble value */
	for (i = 0; i < 4; i++)
	{
		if (peValue[3-i] == 1)    //reverse order
		{
			cValue |= Power(2, i);
		}
	}

	/* get the hex character */
	if (cValue <= 9)
	{
		cValue += '0';
	}
	else  
	{
		cValue += ('A' - 10);
	}

	return cValue;
}


/*==========================================================================*
 * FUNCTION : GetDigitalData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SetSigValues
 * ARGUMENTS: IN unsigned char  chr      : 
 *            OUT SIG_ENUM      *peValue : 4 elements array
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-20 19:49
 *==========================================================================*/
static void GetDigitalData(IN unsigned char *chr, OUT SIG_ENUM *peValue)
{
	//int i;
	//unsigned char c;
	int iData;

	//*peValue = (int)TL1_AsciiHexToChar(&chr);

    iData = (int)TL1_AsciiHexToChar(chr);
	peValue[0] = iData;
	//c = chr;
/*
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}

	//convert
	for (i = 0; i < 4; i++)
	{
		peValue[i] = (c & Power(2, 3-i)) == 0 ? 0 : 1;
	}
*/
	return;
}

/*==========================================================================*
 * FUNCTION : GetSubStringCount
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char*  szStr : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 16:03
 *==========================================================================*/
static int GetSubStringCount(IN const unsigned char* szStr)
{
	int iCount;
	const unsigned char *p;

	iCount = 0;
	p = szStr;
	while (*p != '\0')
	{
		if (*p == '!')
		{
			iCount++;
		}

		p++;
	}

	return iCount + 1;
}


/*==========================================================================*
 * FUNCTION : *GetSubString
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int                   iIndex : start from 1
 *            IN const unsigned char*  szStr  : 
 *			  OUT int *piSubStrLen            :
 * RETURN   : const unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 16:06
 *==========================================================================*/
static const unsigned char *GetSubString(IN int iIndex, 
										 IN const unsigned char* szStr,
										 OUT int *piSubStrLen)
{
	int iCount;
	const unsigned char *p, *pSubStr;

	iCount = 0;
	p = szStr;

	/* special case */
	if (iIndex == 1)
	{
		pSubStr = p;
	}
	else
	{
		/* 1.move to the start pos */
		while (*p != '\0')
		{
			if (*p == '!')
			{
				iCount++;
			}

			if (iCount == iIndex - 1)
			{
				break;
			}

			p++;
		}

		/* 2.pick the sub-string */
		pSubStr = ++p;
	}

	*piSubStrLen = 0;
	while (*p != '!' && *p != '*' && *p != '\0')
	{
		(*piSubStrLen)++;
		p++;
	}

	if (*piSubStrLen == 0)
	{
		pSubStr = NULL;
	}

	return pSubStr;
}




/*==========================================================================*
 * FUNCTION : GetHisAlarms
 * PURPOSE  : Get one History alarm info to fill ALARM_INFO_BUFFER
 * CALLS    : 
 * CALLED BY: ExecuteRH
 * ARGUMENTS: void  *pAlarmBuf : 
 *			int	iAlarmNo; history alarm no, 0 ~ (MAX_HIS_ALARM_COUNT-1)
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-09-27 13:11
 *==========================================================================*/
static BOOL GetHisAlarms(OUT void *pAlarmBuf, IN  int iAlarmNo)
{
	HANDLE hHisAlarmLog;
	BOOL bRet;
	int i, /*iRet,*/  iRecords;
	struct tm *tTime;
	int iStartPos;
	time_t	tCurTime;

	//HIS_ALARM_RECORD *pAlarmRecord;
	HIS_ALARM_RECORD HisAlarmRecord;
	UCHAR byTemp;
	UCHAR	*pbyBuf;

	if ( iAlarmNo >= MAX_HIS_ALARM_COUNT )
	{
		iAlarmNo = 0;
	}
	
	/* 1.Read history alarm info from the Storage */
	hHisAlarmLog = DAT_StorageOpen(HIST_ALARM_LOG);
	if (hHisAlarmLog == NULL)
	{
		LOG_TL1_CMD_E("Open History Alarm Log storage failed");
		TRACE("Open History Alarm Log storage failed");
		return FALSE;
	}

	iStartPos = iAlarmNo;	//Start record number,must 
	iRecords = 1;	//Only read 1 record
	bRet = DAT_StorageReadRecords(hHisAlarmLog, &iStartPos,
		&iRecords, (void *)&HisAlarmRecord, FALSE, FALSE);
	DAT_StorageClose(hHisAlarmLog);
	if (!bRet)
	{
		LOG_TL1_CMD_E("Read History Alarm Log storage failed");
		TRACE("Read History Alarm Log storage failed");
		return FALSE;
	}
	

	//alarm process for test ydn23, from HisAlarmRecord
	pbyBuf = (UCHAR  *)pAlarmBuf;
	byTemp = (UCHAR)iAlarmNo;
	HexToTwoAsciiData(byTemp, pbyBuf);
	pbyBuf += 2;
	
	HexToTwoAsciiData((UCHAR)0xff, pbyBuf);
	pbyBuf += 2;
	
	byTemp = (UCHAR)HisAlarmRecord.iEquipID;
	HexToTwoAsciiData(byTemp, pbyBuf);
	pbyBuf += 2;

	HexToTwoAsciiData((UCHAR)HisAlarmRecord.iAlarmID, pbyBuf);
	pbyBuf += 2;

	tCurTime = HisAlarmRecord.tmStartTime;
	ConvertTime(&tCurTime, TRUE);
	tTime = localtime(&tCurTime);
	HexToTwoAsciiData((UCHAR)(tTime->tm_year -100), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mon + 1), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mday), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_hour), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_min), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_sec), pbyBuf);
	pbyBuf += 2;
	
	tCurTime = HisAlarmRecord.tmEndTime;
	ConvertTime(&tCurTime, TRUE);
	tTime = localtime(&tCurTime);
	HexToTwoAsciiData((UCHAR)(tTime->tm_year -100), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mon + 1), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mday), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_hour), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_min), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_sec), pbyBuf);
	pbyBuf += 2;
#if 1
	for ( i=0; i<8; i++ )
	{
		if ( (HisAlarmRecord.szSerialNumber[i]>='0') 
			&& (HisAlarmRecord.szSerialNumber[i]<='9'))
		{
			*pbyBuf = (UCHAR)HisAlarmRecord.szSerialNumber[i];
		}
		else
		{
			*pbyBuf = 0x30;
		}
		
		pbyBuf++;
	}
#else
	printf("the serial is %s\n", HisAlarmRecord.szSerialNumber);
	for ( i=0; i<8; i++ )
	{
		*pbyBuf++ = 0x30;
	}
#endif


	//printf("ydn23ydn23ydn23ydn23ydn23ydn23ydn23ydn23\n");
	//for ( i=0; i<40; i++ )
	//{
	//	printf("%x\t", *((char *)pAlarmBuf+i));
	//}
	//printf("\nydn23ydn23ydn23ydn23ydn23ydn23ydn23ydn23\n");
	return TRUE;
}




#ifdef PRODUCT_INFO_SUPPORT


#endif //PRODUCT_INFO_SUPPORT


int TL1_PackPacketHeader(IN char *pszData,
						IN int iLen,
						IN const char *pszIdentifier,
						IN const char *pszCtag,
						IN const char *pszCompletionCode)
{
	char	szTime[32];

	TimeToString(time(NULL), TL1_TIME_CHN_FMT, szTime, sizeof(szTime));

	return snprintf(pszData, (size_t)iLen, "\r\n\n   %s %s\r\nM  %s %s",
		pszIdentifier,
		szTime,
		pszCompletionCode,
		pszCtag);

}

int TL1_PackPacketAutonomousHeader(IN char *pszData,
								IN int iMaxLen,
								IN const char *pszIdentifier,
								IN int iAlmCode,
								IN int iAtag,
								IN const char *pszCmdCode,
								OUT char **ppszAlarmCode)
{
	char	szTime[32];
	int		iLen = 0;

	TimeToString(time(NULL), TL1_TIME_CHN_FMT, szTime, sizeof(szTime));

	iLen = snprintf(pszData, (size_t)iMaxLen, "\r\n\n   %s %s\r\n", pszIdentifier, szTime);

	if(NULL != ppszAlarmCode)
	{
		*ppszAlarmCode = (char *)(pszData + iLen);
	}

	iLen += snprintf(pszData + iLen, (size_t)(iMaxLen - iLen), "%s %d %s", TL1_GetAlmCode(iAlmCode), iAtag, pszCmdCode);

	return iLen;
}

inline void TL1_ModifyPacketFieldAlarmCode(IN char *pszAlarmCodeAddr, IN int iAlmCode)
{
	const char	*pszAlarmCodeStr = TL1_GetAlmCode(iAlmCode);

	memcpy(pszAlarmCodeAddr, pszAlarmCodeStr, strlen(pszAlarmCodeStr));
}



int TL1_GetAtag(IN TL1_BASIC_ARGS *pThis)
{
	pThis->iAtag++;
	
	if(pThis->iAtag > 65535)
	{
		pThis->iAtag = 1;
	}

	return pThis->iAtag;
}


int TL1_PackPacketTerminatorAll(IN char *pszData, IN int iMaxLen)
{
	return snprintf(pszData, (size_t)iMaxLen, "\r\n;");
}

int TL1_PackPacketTerminatorOne(IN char *pszData, IN int iMaxLen)
{
	return snprintf(pszData, (size_t)iMaxLen, "\r\n>%c", TL1_RESP_PACKET_SPLITTER);
}

int TL1_PackOneErrPacket(IN char *pszData,
						 IN int iMaxLen,
						 IN const char *pszIdentifier,
						 IN const char *pszCtag,
						 IN const char *pszContent)
{
	int				iLen = 0;

	iLen += TL1_PackPacketHeader(pszData + iLen,
								iMaxLen - iLen,
								pszIdentifier,
								TL1_RESP_STAT_DENY,
								pszCtag);
	iLen += snprintf(pszData + iLen, (size_t)(iMaxLen - iLen), "%s", pszContent);
	iLen += TL1_PackPacketTerminatorAll(pszData + iLen, iMaxLen - iLen);

	return iLen;
}



static int ExecuteActUser(IN TL1_BASIC_ARGS *pThis,
							IN unsigned char *szInData,
							OUT unsigned char *szOutData)
{
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMAND				*pstCmd = &pstSess->stCommand;
	TL1_CMD_FIELD			*pFieldUserName = NULL;
	TL1_CMD_FIELD			*pFieldPassword = NULL;
	TL1_CMD_FIELD			*pstSeventhBlk = NULL;
	USER_INFO_STRU			stUserInfo;
	HANDLE					hLocalThread;
	int						/*i, */iOutLen;
	char					/**pcChr = NULL, */*pszOut = (char *)szOutData;
	//int						iNonAlphaCount, iSymbolCount;
	char					szLogInfo[128];

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	//UNUSED(szInData);


#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteActUser!!!!\n");
	TRACE("\n ExecuteActUser:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';
	//If a second user attempts a login that fails the user session is closed 
	//for the user that was previously logged on. A new user session 
	//must then be established if necessary. The telnet connection must remain open 
	//and autonomous messages will still be sent.


	//auto login won't execute this command
	if( pstSess->bIsAutoLogin )
	{
		iOutLen = TL1_PackOneErrPacket(pszOut,
			TL1_RESP_SIZE,
			pCfg->szSystemIdentifier,
			pstCmd->pstCtag->szData,
			TL1_GetRespErrStr(TL1_E_RESP_ERR_ICNV));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteActUser has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_IS_AUTOLOGIN;
	}


	//2. check each field of input message 
	//Input Format: ACT-USER:[<tid>]:<aid>:<ctag>::<pid>;

	//2.1 check user name
	//The maximum permissible length for a <uid> is ten alphanumeric characters
	pFieldUserName = pstCmd->pstAid;
	if(( !pFieldUserName->bIsExist )
		|| ( pFieldUserName->iLen < 1 )
		|| ( pFieldUserName->iLen > 8 )
		|| !TL1_IsAlphaNumUnderscore(pFieldUserName->szData))
	{
		if( pstSess->bIsLogin )
		{
			iOutLen = TL1_PackOneErrPacket(pszOut,
				TL1_RESP_SIZE,
				pCfg->szSystemIdentifier,
				pstCmd->pstCtag->szData,
				TL1_GetRespErrStr(TL1_E_RESP_ERR_IIAC));

#ifdef _DEBUG_TL1_CMD_HANDLER
			TRACE_TL1_TIPS("ExecuteActUser has been executed ok!");
			TRACE("Reply Data:\n");
			TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER
		}

		return TL1_ERR_EXEC_ARGS_INVALID;
	}

	//2.2 check password
	//It has a maximum length of ten characters. At least two of the characters 
	//must be non-alpha and at least one character must be a symbol 
	//(i.e non-alphanumeric and not a space). Valid symbols that can be used 
	//in this application are the ��#�� , ��%�� and ��+�� characters. 
	//All other symbols are invalid.
	pFieldPassword = pstCmd->pstDataBlk;
	if(( !pFieldPassword->bIsExist )
		|| ( pFieldPassword->iLen < 1 )
		|| ( pFieldPassword->iLen > 13 )
		|| !TL1_IsAlphaNumUnderscore(pFieldPassword->szData))
	{
		if( pstSess->bIsLogin )
		{
			iOutLen = TL1_PackOneErrPacket(pszOut,
				TL1_RESP_SIZE,
				pCfg->szSystemIdentifier,
				pstCmd->pstCtag->szData,
				TL1_GetRespErrStr(TL1_E_RESP_ERR_IDNV));

#ifdef _DEBUG_TL1_CMD_HANDLER
			TRACE_TL1_TIPS("ExecuteActUser has been executed ok!");
			TRACE("Reply Data:\n");
			TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER
		}

		return TL1_ERR_EXEC_ARGS_INVALID;
	}

	//pcChr = pFieldPassword->szData;
	//iNonAlphaCount = 0;
	//iSymbolCount = 0;

	//for(i = 0; i < pFieldPassword->iLen; i++, pcChr++)
	//{
	//	if( !isalpha(*pcChr) )
	//	{
	//		iNonAlphaCount++;
	//	}

	//	if( ('#' == *pcChr) || ('%' == *pcChr) || ('+' == *pcChr))
	//	{
	//		iSymbolCount++;
	//	}
	//}

	//if((iNonAlphaCount < 2)
	//	|| (iSymbolCount < 1))
	//{
	//	return TL1_ERR_EXEC_ARGS_INVALID;
	//}

	//2.3 too many fields, for example:<SeventhBlk> in 
	//ACT-USER:[<tid>]:<aid>:<ctag>::<pid>:<SeventhBlk>;
	pstSeventhBlk = pstCmd->pstSeventhBlk;
	if( pstSeventhBlk->bIsExist )
	{
		if( pstSess->bIsLogin )
		{
			iOutLen = TL1_PackOneErrPacket(pszOut,
				TL1_RESP_SIZE,
				pCfg->szSystemIdentifier,
				pstCmd->pstCtag->szData,
				TL1_GetRespErrStr(TL1_E_RESP_ERR_IISP));

#ifdef _DEBUG_TL1_CMD_HANDLER
			TRACE_TL1_TIPS("ExecuteActUser has been executed ok!");
			TRACE("Reply Data:\n");
			TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER
		}

		return TL1_ERR_EXEC_ARGS_INVALID;
	}


	pstSess->bIsLogin = FALSE;


#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE("user name=%s, password=%s\n",
		pFieldUserName->szData,
		pFieldPassword->szData);
#endif //_DEBUG_TL1_CMD_HANDLER

	/* 3.check user and password */
	if(ERR_SEC_OK != FindUserInfo(pFieldUserName->szData, &stUserInfo))
	{
		//log all Events about ACT-USER
		snprintf(szLogInfo,
			sizeof(szLogInfo),
			"Execute: %s Failed, UserName=%s does not exist",
			szInData,
			pFieldUserName->szData);

		LOG_TL1_CMD_I(szLogInfo);

		return TL1_ERR_EXEC_USER_NAME;
	}

	if( 0 != strcmp(pFieldPassword->szData, stUserInfo.szPassword) )
	{
		//log all Events about ACT-USER
		snprintf(szLogInfo,
			sizeof(szLogInfo),
			"Execute: %s Failed, UserName=%s is OK, Password is wrong",
			szInData,
			pFieldUserName->szData);

		LOG_TL1_CMD_I(szLogInfo);

		return TL1_ERR_EXEC_USER_PASSWORD;
	}

	//log all Events about ACT-USER
	snprintf(szLogInfo,
		sizeof(szLogInfo),
		"Execute: %s Successful, UserName=%s",
		szInData,
		pFieldUserName->szData);

	LOG_TL1_CMD_I(szLogInfo);

	//4. login successfully
	pstSess->bIsLogin = TRUE;
	memcpy(&pstSess->stUser, &stUserInfo, sizeof(pstSess->stUser));


	//5. build response message
	iOutLen += TL1_PackPacketHeader(pszOut + iOutLen,
									TL1_RESP_SIZE - iOutLen,
									pCfg->szSystemIdentifier,
									TL1_RESP_STAT_COMPLD,
									pstCmd->pstCtag->szData);

	iOutLen += snprintf(pszOut + iOutLen,
						(size_t)(TL1_RESP_SIZE - iOutLen),
						"\r\n   /* This is a private computer system.\r\n"
						"Unauthorized access may result in criminal prosecution */");

	iOutLen += TL1_PackPacketTerminatorAll(pszOut + iOutLen, TL1_RESP_SIZE - iOutLen);

#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteActUser has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}


static int ExecuteCancUser(IN TL1_BASIC_ARGS *pThis,
							IN unsigned char *szInData,
							OUT unsigned char *szOutData)
{
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMAND				*pstCmd = &pstSess->stCommand;
	TL1_CMD_FIELD			*pFieldUserName = NULL;
	TL1_CMD_FIELD			*pFieldGeneralBlk = NULL;
	HANDLE					hLocalThread;
	int						iOutLen;
	char					*pszOut = (char *)szOutData;

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	UNUSED(szInData);

#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteCancUser!!!!\n");
	TRACE("\n ExecuteCancUser:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';

	//For this command the access identifier is the user identifier(<uid>). 
	//The maximum permissible length for a <uid> is ten characters.
	//The <uid> is the user name configuration parameter of the user configuration of the controller.
	//Invalid values received for this parameter return an error response with the error code IIAC.

	//No response is to be returned if a valid user session has not been established
	if( pstSess->bIsAutoLogin )
	{
		iOutLen = TL1_PackOneErrPacket(pszOut,
			TL1_RESP_SIZE,
			pCfg->szSystemIdentifier,
			pstCmd->pstCtag->szData,
			TL1_GetRespErrStr(TL1_E_RESP_ERR_ICNV));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteCancUser has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_IS_AUTOLOGIN;
	}

	if( !pstSess->bIsLogin )
	{
		return TL1_ERR_OK;
	}

	//2. check each field of input message 
	//Input Format: CANC-USER:[<tid>]:<aid>:<ctag>;
	
	//2.1 check user name
	//The maximum permissible length for a <uid> is ten alphanumeric characters
	pFieldUserName = pstCmd->pstAid;
	if(( !pFieldUserName->bIsExist )
		|| ( pFieldUserName->iLen < 1 )
		|| ( pFieldUserName->iLen > 8 )
		|| !TL1_IsAlphaNumUnderscore(pFieldUserName->szData)
		|| (0 != strcmp(pFieldUserName->szData, pstSess->stUser.stBaseInfo.szUserName)))
	{
		iOutLen = TL1_PackOneErrPacket(pszOut,
			TL1_RESP_SIZE,
			pCfg->szSystemIdentifier,
			pstCmd->pstCtag->szData,
			TL1_GetRespErrStr(TL1_E_RESP_ERR_IIAC));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteCancUser has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_ARGS_INVALID;
	}

	//2.2 too many fields, for example:<general block> in 
	// CANC-USER:[<tid>]:<aid>:<ctag>:<general block>;
	pFieldGeneralBlk = pstCmd->pstGeneralBlk;
	if(pFieldGeneralBlk->bIsExist)
	{
		iOutLen = TL1_PackOneErrPacket(pszOut,
			TL1_RESP_SIZE,
			pCfg->szSystemIdentifier,
			pstCmd->pstCtag->szData,
			TL1_GetRespErrStr(TL1_E_RESP_ERR_IISP));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteCancUser has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_ARGS_INVALID;
	}


#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE("user name=%s\n", pFieldUserName->szData);
#endif //_DEBUG_TL1_CMD_HANDLER

	//3. logout successfully
	pstSess->bIsLogin = FALSE;


	//4. build response message
	iOutLen += TL1_PackPacketHeader(pszOut + iOutLen,
									TL1_RESP_SIZE - iOutLen,
									pCfg->szSystemIdentifier,
									TL1_RESP_STAT_COMPLD,
									pstCmd->pstCtag->szData);

	iOutLen += TL1_PackPacketTerminatorAll(pszOut + iOutLen, TL1_RESP_SIZE - iOutLen);

#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteCancUser has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}

static int CheckAid(IN TL1_CMD_FIELD *pstAid,
					IN TL1_CMD_FIELD *pstModifier2,
					IN TL1_CMD_FIELD *pstMainAid,
					IN TL1_CMD_FIELD *pstSubAid)
{
#define TL1_L_CHECK_AID_INVALID					-1
#define TL1_L_CHECK_AID_VALID_ALL				0
#define TL1_L_CHECK_AID_VALID_MAIN				1
#define TL1_L_CHECK_AID_VALID_MAIN_AND_SUB		2

	TL1_MODEL_INFO				*pModelInfo = &g_TL1Globals.TL1ModelInfo;
	//TL1_COMMON_CONFIG			*pCfg = &g_TL1Globals.CommonConfig;
	TL1_BLOCK_INFO 				*pCurBlockInfo = NULL;
	TL1_EQUIP_INFO				*pCurTL1Equip = NULL;
	int							i, j;

	//A NULL value is not permitted for this parameter.
	if(pstAid->bIsExist && pstAid->iLen > 0)
	{
		//The AID parameter is ALL.
		if(0 == strcmp(pstAid->szData, "ALL"))
		{
			return TL1_L_CHECK_AID_VALID_ALL;
		}

		//If the command code modifier is ALL, the AID parameter must be ALL.
		if(0 == strcmp(pstModifier2->szData, "ALL"))
		{
			return TL1_L_CHECK_AID_INVALID;
		}

		//The main AID parameter exists.
		if(pstMainAid->bIsExist && pstMainAid->iLen > 0)
		{
			for (i = 0; i < pModelInfo->iBlocksNum; i++)
			{
				pCurBlockInfo = &pModelInfo->pBlockInfo[i];
				if(!pCurBlockInfo->bIsValid)
				{
					continue;
				}

				//The main AID parameter matches a main AID element of the controller.
				if(0 == strcmp(pstMainAid->szData, pCurBlockInfo->pszMainAidName))
				{
					//The sub-AID parameter exists.
					if(pstSubAid->bIsExist && pstSubAid->iLen > 0)
					{
						for(j = 0; j < pCurBlockInfo->iTL1EquipNum; j++)
						{
							pCurTL1Equip = &pCurBlockInfo->pstTL1EquipList[j];
							if(NULL == pCurTL1Equip->pstEquip)
							{
								continue;
							}

							//The sub-AID parameter matches a sub-AID element of the controller.
							if(0 == strcmp(pstSubAid->szData, pCurTL1Equip->szSubAidName))
							{
								return TL1_L_CHECK_AID_VALID_MAIN_AND_SUB;
							}
						}
					}
					//The sub-AID parameter does not exist.
					else
					{
						return TL1_L_CHECK_AID_VALID_MAIN;
					}
				}
			}
		}
	}

	return TL1_L_CHECK_AID_INVALID;
}




static int CheckNtfcncde(IN TL1_CMD_FIELD *pFieldNtfcncde)
{
#define TL1_L_CHECK_NTFCNCDE_INVALID					-1
#define TL1_L_CHECK_NTFCNCDE_ALL						ALARM_LEVEL_NONE

	if(( pFieldNtfcncde->bIsExist )
		&& (pFieldNtfcncde->iLen > 0))
	{
		if(0 == strcmp(pFieldNtfcncde->szData, "CR"))
		{
			return ALARM_LEVEL_CRITICAL;
		}
		else if(0 == strcmp(pFieldNtfcncde->szData, "MJ"))
		{
			return ALARM_LEVEL_MAJOR;
		}
		else if(0 == strcmp(pFieldNtfcncde->szData, "MN"))
		{
			return ALARM_LEVEL_OBSERVATION;
		}
		else
		{
			return TL1_L_CHECK_NTFCNCDE_INVALID;
		}
	}

	return TL1_L_CHECK_NTFCNCDE_ALL;
}

static int CheckSrveff(IN TL1_CMD_FIELD *pFieldSrveff)
{
#define TL1_L_CHECK_SRVEFF_INVALID					-1
#define TL1_L_CHECK_SRVEFF_ALL						TL1_E_SRVEFF_MAX

	if(( pFieldSrveff->bIsExist )
		&& (pFieldSrveff->iLen > 0))
	{
		if(0 == strcmp(pFieldSrveff->szData, "SA"))
		{
			return TL1_E_SRVEFF_SA;
		}
		else if(0 == strcmp(pFieldSrveff->szData, "NSA"))
		{
			return TL1_E_SRVEFF_NSA;
		}
		else
		{
			return TL1_L_CHECK_SRVEFF_INVALID;
		}
	}

	return TL1_L_CHECK_SRVEFF_ALL;
}



static int BuildRtrvAlmData(OUT char *pszData,
							IN int iMaxLen,
							IN TL1_BASIC_ARGS *pThis,
							IN int iQueryAidType,
							IN int iQueryNtfcncdeType,
							IN char *pszQueryCondtype,
							IN int iQuerySrveffType)
{
	TL1_MODEL_INFO				*pModelInfo = &g_TL1Globals.TL1ModelInfo;
	TL1_BLOCK_INFO 				*pCurBlockInfo = NULL;
	TL1_EQUIP_INFO				*pCurTL1Equip = NULL;
	TL1_MAPENTRIES_INFO			*pMapEntriesInfo = NULL;
	TL1_MAPENTRIES_INFO			*pCurMapEntryInfo = NULL;
	TL1_COMMON_CONFIG			*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION					*pstSess = &pThis->stSession;
	TL1_COMMAND					*pstCmd = &pstSess->stCommand;
	int							i, j, k, iTemp;
	ALARM_SIG_VALUE*			pCurAlarmItem = NULL;
	int							iOutLen = 0;
	char						szAid[20];
	char						szDate[20];
	char						szTime[20];
	char						szActivePeriod[20];
	time_t						tmAlmStartTime;
	int							iValidEquipCount = 0, iValidSigCount = 0, iValidAlmCount = 0;
	char						*pszCondDesc = NULL;


	//process equip group
	for(i = 0; i < pModelInfo->iBlocksNum; i++)
	{
		pCurBlockInfo = &pModelInfo->pBlockInfo[i];
		pMapEntriesInfo = pCurBlockInfo->pTypeInfo->pstAidGroupInfo->pMapEntriesInfo;

		if( !pCurBlockInfo->bIsValid )
		{
			continue;
		}

		//check aid 
		//check main aid
		if(TL1_L_CHECK_AID_VALID_ALL == iQueryAidType)
		{
			;
		}
		else if( (TL1_L_CHECK_AID_VALID_MAIN == iQueryAidType)
			&& (0 == strcmp(pstCmd->pstMainAid->szData, pCurBlockInfo->pszMainAidName)) )
		{
			;
		}
		else if( (TL1_L_CHECK_AID_VALID_MAIN_AND_SUB == iQueryAidType)
			&& (0 == strcmp(pstCmd->pstMainAid->szData, pCurBlockInfo->pszMainAidName)) )
		{
			;
		}
		else
		{
			continue;
		}

		//process equip
		for(j = 0; j < pCurBlockInfo->iTL1EquipNum; j++)
		{
			pCurTL1Equip = &pCurBlockInfo->pstTL1EquipList[j];

			if(NULL == pCurTL1Equip->pstEquip)
			{
				continue;
			}

			if( !pCurTL1Equip->pstEquip->bWorkStatus )
			{
				continue;
			}

			//check aid
			//check sub aid
			if(TL1_L_CHECK_AID_VALID_ALL == iQueryAidType)
			{
				;
			}
			else if(TL1_L_CHECK_AID_VALID_MAIN == iQueryAidType)
			{
				;
			}
			else if( (TL1_L_CHECK_AID_VALID_MAIN_AND_SUB == iQueryAidType)
				&& (0 == strcmp(pstCmd->pstSubAid->szData, pCurTL1Equip->szSubAidName)) )
			{
				;
			}
			else
			{
				continue;
			}

			//record valid equipment number
			iValidEquipCount++;

			//process signal
			for(k = 0; k < pCurTL1Equip->iSigCount; k++)
			{
				pCurAlarmItem = (ALARM_SIG_VALUE*)pCurTL1Equip->ppAlmSigValList[k];
				pCurMapEntryInfo = &pMapEntriesInfo[k];

				if( !pCurMapEntryInfo->bSigActive )
				{
					continue;
				}
				
				if(NULL == pCurAlarmItem)
				{
					continue;
				}

				//query condition:<ntfcncde>
				if( (TL1_L_CHECK_NTFCNCDE_ALL != iQueryNtfcncdeType)
					&& (pCurAlarmItem->pStdSig->iAlarmLevel != iQueryNtfcncdeType) )
				{
					continue;
				}

				//query condition:<condtype>
				if((NULL != pszQueryCondtype)
					&& (0 != strcmp(pCurMapEntryInfo->szCondType, pszQueryCondtype)))
				{
					continue;
				}

				//query condition:<srveff>
				if( (TL1_L_CHECK_SRVEFF_ALL != iQuerySrveffType)
					&& (pCurMapEntryInfo->iServiceAffectCode != iQuerySrveffType) )
				{
					continue;
				}

				//record valid signal number
				iValidSigCount++;

				if( !ALARM_IS_ACTIVING(pCurAlarmItem) )
				{
					continue;
				}


				//response data format:
				//"[<aid>][,<aidtype>]:<ntfcncde>,<condtype>,<srveff>, [<ocrdat>],[<ocrtm>],[<locn>],[<dirn>][,<tmper>] [:[<conddescr>],[<aiddet>][,<obsdbhvr>[,<exptdbhvr>]] [:[<dgntype>][,<tblistl>]]]"
				//<aidtype>Valid values for this application are:EQPT - equipment
				//<ntfcncde>This is the notification code associated with a single alarm condition given in this block.
				//<condtype>This is the condition type of the alarm condition being given in this block.
				//<srveff>This is the effect on service caused by the single alarm condition given in this block.
				//<ocrdat>This is the date when the triggering event occurred. 
				//The format for this parameter is MOY-DOM(Month-Of-Year - Day-Of-Month).
				//<ocrtm>This is the time when the triggering event occurred. 
				//The format for this parameter is HOD-MOH-SOM(Hour-Of-Day - Minute-Of-Hour - Second-Of-Minute)
				//<locn> - location of alarm condition.A NULL value is inserted for them
				//<dirn> - direction of alarm condition.A NULL value is inserted for them
				//<tmper>This is the accumulation time period for PM parameters. 
				//In this application it represents the length of time the alarm condition has been active.
				if(iOutLen >= iMaxLen)
				{
					break;
				}
				else if(iValidAlmCount >= TL1_ALM_MAX_NUM)
				{
					break;
				}

				if(0 == iValidAlmCount)
				{
					iOutLen += TL1_PackPacketHeader(pszData + iOutLen,
						iMaxLen - iOutLen,
						pCfg->szSystemIdentifier,
						TL1_RESP_STAT_COMPLD,
						pstSess->stCommand.pstCtag->szData);
				}
				else if(0 == iValidAlmCount % TL1_ALM_SPLIT_NUM)
				{
					iOutLen += TL1_PackPacketTerminatorOne(pszData + iOutLen, iMaxLen - iOutLen);
					iOutLen += TL1_PackPacketHeader(pszData + iOutLen,
						iMaxLen - iOutLen,
						pCfg->szSystemIdentifier,
						TL1_RESP_STAT_COMPLD,
						pstSess->stCommand.pstCtag->szData);
				}

				//<aid>
				if(TL1_CHAR_NULL == pCurTL1Equip->szSubAidName[0])
				{
					snprintf(szAid, sizeof(szAid), "%s", pCurBlockInfo->pszMainAidName);
				}
				else if ( pCfg->iAidDelimeter )
				{
					snprintf(szAid, sizeof(szAid), "%s-%s", pCurBlockInfo->pszMainAidName, pCurTL1Equip->szSubAidName);
				}
				else
				{
					snprintf(szAid, sizeof(szAid), "%s%s", pCurBlockInfo->pszMainAidName, pCurTL1Equip->szSubAidName);
				}	

				//<ocrdat>
				tmAlmStartTime = pCurAlarmItem->sv.tmStartTime;
				TimeToString(tmAlmStartTime, TL1_TIME_CHN_FMT_DATE_2, szDate, sizeof(szDate));
				//<ocrtm>
				TimeToString(tmAlmStartTime, TL1_TIME_CHN_FMT_TIME, szTime, sizeof(szTime));

				//<tmper>
				iTemp = time(NULL) - tmAlmStartTime;
				if((iTemp >= TL1_C_DAY) || (iTemp <= -1*TL1_C_DAY))
				{
					//DAY (days) - used if alarm is active longer than 24 hours
					snprintf(szActivePeriod, sizeof(szActivePeriod), "%d-DAY", (int)(iTemp/TL1_C_DAY));
				}
				else
				{
					//MIN (minutes) - used if alarm is active less than 24 hours.
					snprintf(szActivePeriod, sizeof(szActivePeriod), "%d-MIN", (int)(iTemp/TL1_C_MIN));
				}

				if(TL1_CHAR_NULL == pCurMapEntryInfo->szCondDesc[0])
				{
					pszCondDesc = pCurAlarmItem->pStdSig->pSigName->pFullName[0];
				}
				else
				{
					pszCondDesc = pCurMapEntryInfo->szCondDesc;
				}

				iOutLen += snprintf(pszData + iOutLen,
					(size_t)(iMaxLen - iOutLen),
					"\r\n   \"%s,%s:%s,%s,%s,%s,%s,,,%s:\\\"%s\\\",\"",
					szAid,
					TL1_GetAidTypeStr(pCurBlockInfo->pTypeInfo->pstAidGroupInfo->iAidGroupType),
					TL1_GetNtfcncdeStr(pCurAlarmItem->pStdSig->iAlarmLevel),
					pCurMapEntryInfo->szCondType,
					TL1_GetSrveffStr(pCurMapEntryInfo->iServiceAffectCode),
					szDate,
					szTime,
					szActivePeriod,
					pszCondDesc);

				iValidAlmCount++;
			}
		}
	}


	//output tail
	if(iValidAlmCount > 0)
	{
		iOutLen += TL1_PackPacketTerminatorAll(pszData + iOutLen, iMaxLen - iOutLen);
	}
	else
	{
		//If no equipment exists in the system with the specified AID.
		//then it is an error. The error code ��ENEQ�� is returned
		if(iValidEquipCount <= 0)
		{
			iOutLen += TL1_PackOneErrPacket(pszData + iOutLen,
				iMaxLen - iOutLen,
				pCfg->szSystemIdentifier,
				pstCmd->pstCtag->szData,
				TL1_GetRespErrStr(TL1_E_RESP_ERR_ENEQ));
		}
		//If no signal exists in the system with the specified condition type 
		//then it is an error. The error code ��IDNV�� is returned
		else if(iValidSigCount <= 0)
		{
			iOutLen += TL1_PackOneErrPacket(pszData + iOutLen,
				iMaxLen - iOutLen,
				pCfg->szSystemIdentifier,
				pstCmd->pstCtag->szData,
				TL1_GetRespErrStr(TL1_E_RESP_ERR_IDNV));
		}
		else
		{
			iOutLen += TL1_PackPacketHeader(pszData + iOutLen,
				iMaxLen - iOutLen,
				pCfg->szSystemIdentifier,
				TL1_RESP_STAT_COMPLD,
				pstSess->stCommand.pstCtag->szData);
			iOutLen += TL1_PackPacketTerminatorAll(pszData + iOutLen, iMaxLen - iOutLen);
		}
	}

	return iOutLen;
}




static int ExecuteRtrvAlm(IN TL1_BASIC_ARGS *pThis,
							IN unsigned char *szInData,
							OUT unsigned char *szOutData)
{
	TL1_COMMON_CONFIG			*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION					*pstSess = &pThis->stSession;
	TL1_COMMAND					*pstCmd = &pstSess->stCommand;
	TL1_CMD_FIELD				*pFieldModifier2 = NULL;
	TL1_CMD_FIELD				*pFieldAid = NULL;
	TL1_CMD_FIELD				*pFieldMainAid = NULL;
	TL1_CMD_FIELD				*pFieldSubAid = NULL;
	TL1_CMD_FIELD				*pFieldNtfcncde = NULL;
	TL1_CMD_FIELD				*pFieldCondtype = NULL;
	TL1_CMD_FIELD				*pFieldSrveff = NULL;
	TL1_CMD_FIELD				*pFieldLocn = NULL;
	TL1_CMD_FIELD				*pFieldDirn = NULL;
	TL1_CMD_FIELD				*pFieldTmper = NULL;
	TL1_CMD_FIELD				*pstSeventhBlk = NULL;
	TL1_CMD_FIELD				*pstSeventhParam = NULL;
	TL1_CMD_FIELD				*pstDataBlk = NULL;
	HANDLE						hLocalThread;
	int							iOutLen;
	char						/**pcChr = NULL, */*pszOut = (char *)szOutData;
	//int							iNonAlphaCount, iSymbolCount;
	enum TL1_E_TYPE_RESP_ERR	eErrType;
	int							iQueryAidType;
	int							iQueryNtfcncdeType;
	char						*pszCondtype = NULL;
	int							iSrveffType;


	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	UNUSED(szInData);


#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteRtrvAlm!!!!\n");
	TRACE("\n ExecuteRtrvAlm:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';
	eErrType = TL1_E_RESP_ERR_OK;

	//2. check each field of input message 
	//Input Format: RTRV-ALM-{EQPT|ALL}:[<tid>]:<aid>:<ctag>::[<ntfcncde>],[<condtype>], [<srveff>],[<locn>],[<dirn>][,<tmper>];

	//2.1 check <aid>
	//Invalid values received for this parameter return an error response with the error code IIAC
	pFieldAid = pstCmd->pstAid;
	pFieldModifier2 = pstCmd->pstModifier2;
	pFieldMainAid = pstCmd->pstMainAid;
	pFieldSubAid = pstCmd->pstSubAid;

	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		iQueryAidType = CheckAid(pFieldAid, pFieldModifier2, pFieldMainAid, pFieldSubAid);
		if( TL1_L_CHECK_AID_INVALID ==  iQueryAidType)
		{
			eErrType = TL1_E_RESP_ERR_IIAC;
		}
	}

	//2.2 too few fields, for example:<DataBlk> in 
	//RTRV-ALM-{EQPT|ALL}:[<tid>]:<aid>:<ctag>::<DataBlk>;
	pstDataBlk = pstCmd->pstDataBlk;
	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		if( !pstDataBlk->bIsExist )
		{
			//Frank Wu,20151023, for CenturyTel pointed out that the trailing colons on RTRV-ALM-ALL 
			//and RTRV-ALM-EQPT should not be required.  For example, RTRV-ALM-ALL:Lorain:ALL:24::; 
			//and RTRV-ALM-ALL:Lorain:ALL:24; should be accepted.
			//eErrType = TL1_E_RESP_ERR_IISP;
		}
	}

	//2.3 check <ntfcncde>
	//This is the notification code associated with the alarm conditions being retrieved
	//A NULL value will cause all conditions classified as alarm conditions to be retrieved.
	//Invalid values for this parameter return an error response with the error code IDNV.
	pFieldNtfcncde = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_DATA_BLK][0];

	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		iQueryNtfcncdeType = CheckNtfcncde(pFieldNtfcncde);
		if(TL1_L_CHECK_NTFCNCDE_INVALID == iQueryNtfcncdeType)
		{
			eErrType = TL1_E_RESP_ERR_IDNV;
		}
	}

	//2.4 check <condtype>
	//This is the condition type of alarm conditions to be retrieved.
	//A NULL value will cause all conditions classified as alarm conditions to be retrieved.
	//Invalid values for this parameter return an error response with the error code IDNV.
	pFieldCondtype = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_DATA_BLK][1];

	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		if(( pFieldCondtype->bIsExist )
			&& (pFieldCondtype->iLen > 0))
		{
			pszCondtype = pFieldCondtype->szData;
		}
		else
		{
			//it means to search all signals
			pszCondtype = NULL;
		}
	}

	//2.5 check <srveff>
	//This is the condition type of alarm conditions to be retrieved.
	//A NULL value will cause all conditions classified as alarm conditions to be retrieved.
	//Invalid values for this parameter return an error response with the error code IDNV.
	pFieldSrveff = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_DATA_BLK][2];

	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		iSrveffType = CheckSrveff(pFieldSrveff);
		if(TL1_L_CHECK_SRVEFF_INVALID == iSrveffType)
		{
			eErrType = TL1_E_RESP_ERR_IDNV;
		}
	}

	//2.6 check <locn>
	//A NULL value is the only value which will be accepted for this parameter. 
	//Invalid values for this parameter return an error response with the error code IDNV.
	pFieldLocn = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_DATA_BLK][3];

	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		if( pFieldLocn->bIsExist )
		{
			if(pFieldLocn->iLen > 0)
			{
				eErrType = TL1_E_RESP_ERR_INUP;
			}
		}
	}

	//2.7 check <dirn>
	//A NULL value is the only value which will be accepted for this parameter. 
	//Invalid values for this parameter return an error response with the error code IDNV.
	pFieldDirn = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_DATA_BLK][4];

	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		if( pFieldDirn->bIsExist )
		{
			if(pFieldDirn->iLen > 0)
			{
				eErrType = TL1_E_RESP_ERR_INUP;
			}
		}
	}

	//2.8 check <tmper>
	//A NULL value is the only value which will be accepted for this parameter. 
	//Invalid values for this parameter return an error response with the error code IDNV.
	pFieldTmper = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_DATA_BLK][5];

	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		if( pFieldTmper->bIsExist )
		{
			if(pFieldTmper->iLen > 0)
			{
				eErrType = TL1_E_RESP_ERR_INUP;
			}
		}
	}

	//2.9 too many fields, for example:<SeventhParam> and <SeventhBlk> in 
	//RTRV-ALM-{EQPT|ALL}:[<tid>]:<aid>:<ctag>::[<ntfcncde>],[<condtype>],[<srveff>],[<locn>],[<dirn>][,<tmper>][,<SeventhParam>]:<SeventhBlk>;
	pstSeventhParam = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_DATA_BLK][6];
	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		if( pstSeventhParam->bIsExist )
		{
			eErrType = TL1_E_RESP_ERR_IISP;
		}
	}

	pstSeventhBlk = pstCmd->pstSeventhBlk;
	if(TL1_E_RESP_ERR_OK == eErrType)
	{
		if( pstSeventhBlk->bIsExist )
		{
			eErrType = TL1_E_RESP_ERR_IISP;
		}
	}


	if(TL1_E_RESP_ERR_OK != eErrType)
	{

		iOutLen = TL1_PackOneErrPacket(pszOut,
									TL1_RESP_SIZE,
									pCfg->szSystemIdentifier,
									pstCmd->pstCtag->szData,
									TL1_GetRespErrStr((int)eErrType));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteRtrvAlm has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_ARGS_INVALID;
	}


#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE("ExecuteRtrvAlm arguments check is OK\n");
#endif //_DEBUG_TL1_CMD_HANDLER

	//3. build response message
	iOutLen += BuildRtrvAlmData(pszOut + iOutLen,
								TL1_RESP_SIZE - iOutLen,
								pThis,
								iQueryAidType,
								iQueryNtfcncdeType,
								pszCondtype,
								iSrveffType);


#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteRtrvAlm has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}


static int BuildRtrvEqptData(OUT char *pszData, IN int iMaxLen, IN TL1_BASIC_ARGS *pThis)
{
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	ACU_PRODUCT_INFO		stProductInfo;
	int						iError, iBufLen;
	int						iOutLen = 0;


	memset(&stProductInfo, 0, sizeof(stProductInfo));
	iBufLen = sizeof(stProductInfo);

	iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
						ACU_PRODUCT_INFO_GET, 
						0, 
						&iBufLen,
						&(stProductInfo),
						0);
	if(ERR_DXI_OK != iError)
	{
		//return TL1_ERR_EXEC_ARGS_INVALID;
	}

	iOutLen += TL1_PackPacketHeader(pszData + iOutLen,
									iMaxLen - iOutLen,
									pCfg->szSystemIdentifier,
									TL1_RESP_STAT_COMPLD,
									pstSess->stCommand.pstCtag->szData);
	//response data format:
	//^^^"<aid>:[<eqpt_type>]:[<cur_eqpt>]:[SWVER=<swver>],[CURSWVER=<curswver>],[DSPSWVER=<dspswver>]:<state>,[<sst>]"
	//<aid> This value will always be ��SYSTEM�� indicating that it is the system processor
	//<eqpt_type> Valid values for this application will be the controller part number
	//<cur_eqpt> Valid values for this application will be NULL
	//<swver>This is an ASCII string from one to thirty-two characters. The value of this parameter 
	//will be the controller��s software configuration part number and revision (e.g., ��552365AB��).
	//<curswver>The value of this parameter will be the controller��s software version (e.g., ��1.30��)
	//<dspswver>A value of NULL will always be output for this parameter.
	//<state>A value of "IS-NR" should always be returned 
	//<sst>A value of NULL will always be output for this parameter.
	iOutLen += snprintf(pszData + iOutLen,
						(size_t)(iMaxLen - iOutLen),
						"\r\n   \"SYSTEM:%s::SWVER=%s,CURSWVER=%s,:IS-NR,\"",
						stProductInfo.szPartNumber,
						stProductInfo.szCfgFileVersion,
						stProductInfo.szSWRevision);

	iOutLen += TL1_PackPacketTerminatorAll(pszData + iOutLen, iMaxLen - iOutLen);

	return iOutLen;
}


static int ExecuteRtrvEqpt(IN TL1_BASIC_ARGS *pThis,
						  IN unsigned char *szInData,
						  OUT unsigned char *szOutData)
{
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMAND				*pstCmd = &pstSess->stCommand;
	TL1_CMD_FIELD			*pFieldAid = NULL;
	TL1_CMD_FIELD			*pFieldGeneralBlk = NULL;
	HANDLE					hLocalThread;
	int						iOutLen;
	char					*pszOut = (char *)szOutData;

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	UNUSED(szInData);

#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteRtrvEqpt!!!!\n");
	TRACE("\n ExecuteRtrvEqpt:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//This command message retrieves the software version of the specified equipment. 
	//For this application it will retrieve the controller software version 
	//and the controller software configuration part number and revision.

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';


	//2. check each field of input message 
	//Input Format: RTRV-EQPT:[<tid>]:<aid>:<ctag>;

	//2.1 check aid
	//Invalid values received for this parameter return an error response 
	//with the error code IIAC. Invalid values are any value other than ��SYSTEM�� or ��ALL��
	pFieldAid = pstCmd->pstAid;
	if(( !pFieldAid->bIsExist )
		|| ( pFieldAid->iLen < 1 )
		|| ( (0 != strcmp(pFieldAid->szData, "SYSTEM")) && (0 != strcmp(pFieldAid->szData, "ALL")) )
		)
	{
		iOutLen = TL1_PackOneErrPacket(pszOut,
			TL1_RESP_SIZE,
			pCfg->szSystemIdentifier,
			pstCmd->pstCtag->szData,
			TL1_GetRespErrStr(TL1_E_RESP_ERR_IIAC));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteRtrvEqpt has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_ARGS_INVALID;
	}

	//2.2 too many fields, for example:<general block> in 
	//RTRV-EQPT:[<tid>]:<aid>:<ctag>:<general block>;
	pFieldGeneralBlk = pstCmd->pstGeneralBlk;
	if(pFieldGeneralBlk->bIsExist)
	{
		iOutLen = TL1_PackOneErrPacket(pszOut,
			TL1_RESP_SIZE,
			pCfg->szSystemIdentifier,
			pstCmd->pstCtag->szData,
			TL1_GetRespErrStr(TL1_E_RESP_ERR_IISP));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteRtrvEqpt has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_ARGS_INVALID;
	}


	//3. build response message
	iOutLen += BuildRtrvEqptData(pszOut + iOutLen, TL1_RESP_SIZE - iOutLen, pThis);

#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteRtrvEqpt has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}

static int BuildRtrvHdrData(OUT char *pszData, IN int iMaxLen, IN TL1_BASIC_ARGS *pThis)
{
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	int						iOutLen = 0;


	iOutLen += TL1_PackPacketHeader(pszData + iOutLen,
									iMaxLen - iOutLen,
									pCfg->szSystemIdentifier,
									TL1_RESP_STAT_COMPLD,
									pstSess->stCommand.pstCtag->szData);
	iOutLen += TL1_PackPacketTerminatorAll(pszData + iOutLen, iMaxLen - iOutLen);

	return iOutLen;
}


static int ExecuteRtrvHdr(IN TL1_BASIC_ARGS *pThis,
						   IN unsigned char *szInData,
						   OUT unsigned char *szOutData)
{
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMAND				*pstCmd = &pstSess->stCommand;
	TL1_CMD_FIELD			*pFieldAid = NULL;
	TL1_CMD_FIELD			*pFieldGeneralBlk = NULL;
	HANDLE					hLocalThread;
	int						iOutLen;
	char					*pszOut = (char *)szOutData;

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	UNUSED(szInData);

#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteRtrvHdr!!!!\n");
	TRACE("\n ExecuteRtrvHdr:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';


	//2. check each field of input message 
	//Input Format: RTRV-HDR:[<tid>]:<aid>:<ctag>;

	//2.1 check aid
	//The <aid> in this command must be NULL since it does not apply. 
	//Invalid values received for this parameter return an error response with the error code IIAC.
	pFieldAid = pstCmd->pstAid;
	if(( !pFieldAid->bIsExist )
		|| ( pFieldAid->iLen > 0 ))
	{
		iOutLen = TL1_PackOneErrPacket(pszOut,
										TL1_RESP_SIZE,
										pCfg->szSystemIdentifier,
										pstCmd->pstCtag->szData,
										TL1_GetRespErrStr(TL1_E_RESP_ERR_IIAC));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteRtrvHdr has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_ARGS_INVALID;
	}

	//2.2 too many fields, for example:<general block> in 
	//RTRV-HDR:[<tid>]:<aid>:<ctag>:<general block>;
	pFieldGeneralBlk = pstCmd->pstGeneralBlk;
	if(pFieldGeneralBlk->bIsExist)
	{
		iOutLen = TL1_PackOneErrPacket(pszOut,
			TL1_RESP_SIZE,
			pCfg->szSystemIdentifier,
			pstCmd->pstCtag->szData,
			TL1_GetRespErrStr(TL1_E_RESP_ERR_IISP));

#ifdef _DEBUG_TL1_CMD_HANDLER
		TRACE_TL1_TIPS("ExecuteRtrvHdr has been executed ok!");
		TRACE("Reply Data:\n");
		TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

		return TL1_ERR_EXEC_ARGS_INVALID;
	}


	//3. build response message
	iOutLen += BuildRtrvHdrData(pszOut + iOutLen, TL1_RESP_SIZE - iOutLen, pThis);

#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteRtrvHdr has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}



static int ExecuteCmdNotSupport(IN TL1_BASIC_ARGS *pThis,
							IN unsigned char *szInData,
							OUT unsigned char *szOutData)
{
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMAND				*pstCmd = &pstSess->stCommand;
	//TL1_CMD_FIELD			*pFieldAid = NULL;
	HANDLE					hLocalThread;
	int						iOutLen;
	char					*pszOut = (char *)szOutData;

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	UNUSED(szInData);

#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteCmdNotSupport!!!!\n");
	TRACE("\n ExecuteCmdNotSupport:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';


	iOutLen = TL1_PackOneErrPacket(pszOut,
									TL1_RESP_SIZE,
									pCfg->szSystemIdentifier,
									pstCmd->pstCtag->szData,
									TL1_GetRespErrStr(TL1_E_RESP_ERR_ICNV));

#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteCmdNotSupport has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}



static int ExecuteCancSession(IN TL1_BASIC_ARGS *pThis,
								IN unsigned char *szInData,
								OUT unsigned char *szOutData)
{
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION				*pstSess = &pThis->stSession;
	//TL1_COMMAND				*pstCmd = &pstSess->stCommand;
	//TL1_CMD_FIELD			*pFieldAid = NULL;
	HANDLE					hLocalThread;
	int						iOutLen;
	char					*pszOut = (char *)szOutData;
	char					*pszCmdCode = (char *)szInData;

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	//UNUSED(szInData);

#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteCancSession!!!!\n");
	TRACE("\n ExecuteCancSession:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';

	iOutLen += TL1_PackPacketAutonomousHeader(pszOut + iOutLen,
											TL1_RESP_SIZE - iOutLen,
											pCfg->szSystemIdentifier,
											TL1_E_ALM_AUTO_MSG,
											TL1_GetAtag(pThis),
											pszCmdCode,
											NULL);
	if( pstSess->bIsLogin )
	{
		iOutLen += snprintf(pszOut + iOutLen,
			(size_t)(TL1_RESP_SIZE - iOutLen),
			"\r\n%s",
			pstSess->stUser.stBaseInfo.szUserName);
	}

	iOutLen += TL1_PackPacketTerminatorAll(pszOut + iOutLen, TL1_RESP_SIZE - iOutLen);


	pstSess->bIsLogin = FALSE;
	memset(&pstSess->stUser, 0, sizeof(pstSess->stUser));


#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteCancSession has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}

inline void OutMonitorSigVal(IN int iMonitorFormat,
							 IN int iSigType,
							IN SIG_BASIC_VALUE *pSigVal,
							OUT char *pszMonval,
							IN int iMaxLen)
{
	char					szFormatStr[10] = "%.0f";

	pszMonval[0] = TL1_CHAR_NULL;

	//decimal
	if(TL1_E_MONITOR_FORMAT_DECIMAL == iMonitorFormat)
	{
		if(NULL != pSigVal)
		{
			if(VAR_LONG == pSigVal->ucType)
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%ld", pSigVal->varValue.lValue);
			}
			//if it is a float, we display it with precision in pSigVal->pStdSig->szValueDisplayFmt
			else if(VAR_FLOAT == pSigVal->ucType)
			{
				if(SIG_TYPE_SAMPLING == iSigType)
				{
					if(TL1_CHAR_NULL != ((SAMPLE_SIG_VALUE *)pSigVal)->pStdSig->szValueDisplayFmt[0])
					{
						snprintf(szFormatStr, sizeof(szFormatStr), "%%%sf", ((SAMPLE_SIG_VALUE *)pSigVal)->pStdSig->szValueDisplayFmt);
					}
				}
				else if(SIG_TYPE_CONTROL == iSigType)
				{
					if(TL1_CHAR_NULL != ((CTRL_SIG_VALUE *)pSigVal)->pStdSig->szValueDisplayFmt[0])
					{
						snprintf(szFormatStr, sizeof(szFormatStr), "%%%sf", ((CTRL_SIG_VALUE *)pSigVal)->pStdSig->szValueDisplayFmt);
					}
				}
				else if(SIG_TYPE_SETTING == iSigType)
				{
					if(TL1_CHAR_NULL != ((SET_SIG_VALUE *)pSigVal)->pStdSig->szValueDisplayFmt[0])
					{
						snprintf(szFormatStr, sizeof(szFormatStr), "%%%sf", ((SET_SIG_VALUE *)pSigVal)->pStdSig->szValueDisplayFmt);
					}
				}

				snprintf(pszMonval, (size_t)iMaxLen, szFormatStr, pSigVal->varValue.fValue);
			}
			else if(VAR_UNSIGNED_LONG == pSigVal->ucType)
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%lu", pSigVal->varValue.ulValue);
			}
			else if(VAR_ENUM == pSigVal->ucType)
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%ld", pSigVal->varValue.enumValue);
			}
			else
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%ld", pSigVal->varValue.lValue);
			}
		}
	}
	//integer
	else if(TL1_E_MONITOR_FORMAT_INTEGER == iMonitorFormat)
	{
		if(NULL != pSigVal)
		{
			if(VAR_LONG == pSigVal->ucType)
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%ld", pSigVal->varValue.lValue);
			}
			else if(VAR_FLOAT == pSigVal->ucType)
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%.0f", pSigVal->varValue.fValue);
			}
			else if(VAR_UNSIGNED_LONG == pSigVal->ucType)
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%lu", pSigVal->varValue.ulValue);
			}
			else if(VAR_ENUM == pSigVal->ucType)
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%ld", pSigVal->varValue.enumValue);
			}
			else
			{
				snprintf(pszMonval, (size_t)iMaxLen, "%ld", pSigVal->varValue.lValue);
			}
		}
	}
}


static int BuildReptAlmEqptData(OUT char *pszData, IN int iMaxLen, IN TL1_BASIC_ARGS *pThis, IN char *pszCmdCode)
{
	TL1_MODEL_INFO				*pModelInfo = &g_TL1Globals.TL1ModelInfo;
	TL1_BLOCK_INFO 				*pCurBlockInfo = NULL;
	TL1_EQUIP_INFO				*pCurTL1Equip = NULL;
	TL1_MAPENTRIES_INFO			*pMapEntriesInfo = NULL;
	TL1_MAPENTRIES_INFO			*pCurMapEntryInfo = NULL;
	TL1_COMMON_CONFIG			*pCfg = &g_TL1Globals.CommonConfig;
	//TL1_SESSION					*pstSess = &pThis->stSession;
	//TL1_COMMAND					*pstCmd = &pstSess->stCommand;
	int							i, j, k, iTemp;
	ALARM_SIG_VALUE				*pCurAlarmItem = NULL;
	SIG_BASIC_VALUE				*pCurSampleItem = NULL;
	SIG_BASIC_VALUE				*pCurSettingItem = NULL;
	int							iCurSampleItemType;
	int							iCurSettingItemType;
	int							iOutLen = 0;
	char						szDate[20];
	char						szTime[20];
	char						szActivePeriod[20];
	char						szAid[20];
	char						szMonval[20];
	char						szThlev[20];
	time_t						tmAlmStartTime;
	int							iCount = 0;
	char						*pszAlarmCodeAddr = NULL;
	char						*pszCondDesc = NULL;
	int							iMaxAlarmLevel;


	//process equip group
	for(i = 0; i < pModelInfo->iBlocksNum; i++)
	{
		pCurBlockInfo = &pModelInfo->pBlockInfo[i];
		pMapEntriesInfo = pCurBlockInfo->pTypeInfo->pstAidGroupInfo->pMapEntriesInfo;

		if( !pCurBlockInfo->bIsValid )
		{
			continue;
		}

		//process equip
		for(j = 0; j < pCurBlockInfo->iTL1EquipNum; j++)
		{
			pCurTL1Equip = &pCurBlockInfo->pstTL1EquipList[j];

			if(NULL == pCurTL1Equip->pstEquip)
			{
				continue;
			}

			if( !pCurTL1Equip->pstEquip->bWorkStatus )
			{
				continue;
			}

			//process signal
			for(k = 0; k < pCurTL1Equip->iSigCount; k++)
			{
				pCurAlarmItem = (ALARM_SIG_VALUE*)pCurTL1Equip->ppAlmSigValList[k];
				pCurSampleItem = pCurTL1Equip->ppSampSigValList[k];
				pCurSettingItem = pCurTL1Equip->ppSetSigValList[k];
				iCurSampleItemType = pCurTL1Equip->piSampSigInfoTypeList[k];
				iCurSettingItemType = pCurTL1Equip->piSetSigInfoTypeList[k];
				pCurMapEntryInfo = &pMapEntriesInfo[k];

				if( !pCurMapEntryInfo->bSigActive )
				{
					continue;
				}
				
				if(NULL == pCurAlarmItem)
				{
					continue;
				}

				if(pCurAlarmItem->pStdSig->iAlarmLevel <= ALARM_LEVEL_NONE)
				{
					continue;
				}
				
				if( !ALARM_IS_ACTIVING(pCurAlarmItem) )
				{
					continue;
				}

				//response data format:
				//^^^"<aid>:<ntfcncde>,<condtype>,<srveff>, [<ocrdat>],[<ocrtm>],[<locn>],[<dirn>], [<monval>],[<thlev>][,<tmper>] 
				//[:[<conddescr>],[<aiddet>][,<obsdbhvr>[,<exptdbhvr>]] [:[<dgntype>][,<tblistl>]]]"
				//<aidtype>Valid values for this application are:EQPT - equipment
				//<ntfcncde>This is the notification code associated with a single alarm condition given in this block.
				//<condtype>This is the condition type of the alarm condition being given in this block.
				//<srveff>This is the effect on service caused by the single alarm condition given in this block.
				//<ocrdat>This is the date when the triggering event occurred. 
				//The format for this parameter is MOY-DOM(Month-Of-Year - Day-Of-Month).
				//<ocrtm>This is the time when the triggering event occurred. 
				//The format for this parameter is HOD-MOH-SOM(Hour-Of-Day - Minute-Of-Hour - Second-Of-Minute)
				//<locn> - location of alarm condition.A NULL value is inserted for them
				//<dirn> - direction of alarm condition.A NULL value is inserted for them
				//<tmper>This is the accumulation time period for PM parameters. 
				//In this application it represents the length of time the alarm condition has been active.

				if(iOutLen >= iMaxLen)
				{
					break;
				}
				else if(iCount >= TL1_ALM_MAX_NUM)
				{
					break;
				}

				if(0 == iCount)
				{
					iMaxAlarmLevel = ALARM_LEVEL_NONE;
					iOutLen += TL1_PackPacketAutonomousHeader(pszData + iOutLen,
						iMaxLen - iOutLen,
						pCfg->szSystemIdentifier,
						TL1_E_ALM_AUTO_MSG,
						TL1_GetAtag(pThis),
						(const char *)pszCmdCode,
						&pszAlarmCodeAddr);
				}
				else if(0 == iCount % TL1_ALM_SPLIT_NUM)
				{
					iOutLen += TL1_PackPacketTerminatorOne(pszData + iOutLen, iMaxLen - iOutLen);
					TL1_ModifyPacketFieldAlarmCode(pszAlarmCodeAddr, iMaxAlarmLevel);

					iMaxAlarmLevel = ALARM_LEVEL_NONE;
					iOutLen += TL1_PackPacketAutonomousHeader(pszData + iOutLen,
						iMaxLen - iOutLen,
						pCfg->szSystemIdentifier,
						TL1_E_ALM_AUTO_MSG,
						TL1_GetAtag(pThis),
						(const char *)pszCmdCode,
						&pszAlarmCodeAddr);
				}

				if(iMaxAlarmLevel < pCurAlarmItem->pStdSig->iAlarmLevel)
				{
					iMaxAlarmLevel = pCurAlarmItem->pStdSig->iAlarmLevel;
				}

				//<aid>
				if(TL1_CHAR_NULL == pCurTL1Equip->szSubAidName[0])
				{
					snprintf(szAid, sizeof(szAid), "%s", pCurBlockInfo->pszMainAidName);
				}
				else if ( pCfg->iAidDelimeter )
				{
					snprintf(szAid, sizeof(szAid), "%s-%s", pCurBlockInfo->pszMainAidName, pCurTL1Equip->szSubAidName);
				}
				else
				{
					snprintf(szAid, sizeof(szAid), "%s%s", pCurBlockInfo->pszMainAidName, pCurTL1Equip->szSubAidName);
				}	

				tmAlmStartTime = pCurAlarmItem->sv.tmStartTime;
				//<ocrdat>
				TimeToString(tmAlmStartTime, TL1_TIME_CHN_FMT_DATE_2, szDate, sizeof(szDate));
				//<ocrtm>
				TimeToString(tmAlmStartTime, TL1_TIME_CHN_FMT_TIME, szTime, sizeof(szTime));

				//<tmper>
				iTemp = time(NULL) - tmAlmStartTime;
				if((iTemp >= TL1_C_DAY) || (iTemp <= -1*TL1_C_DAY))
				{
					//DAY (days) - used if alarm is active longer than 24 hours
					snprintf(szActivePeriod, sizeof(szActivePeriod), "%d-DAY", (int)(iTemp/TL1_C_DAY));
				}
				else
				{
					//MIN (minutes) - used if alarm is active less than 24 hours.
					snprintf(szActivePeriod, sizeof(szActivePeriod), "%d-MIN", (int)(iTemp/TL1_C_MIN));
				}

				OutMonitorSigVal(pCurMapEntryInfo->iMonitorFormat, iCurSampleItemType, pCurSampleItem, szMonval, sizeof(szMonval));
				OutMonitorSigVal(pCurMapEntryInfo->iMonitorFormat, iCurSettingItemType, pCurSettingItem, szThlev, sizeof(szThlev));

				if(TL1_CHAR_NULL == pCurMapEntryInfo->szCondDesc[0])
				{
					pszCondDesc = pCurAlarmItem->pStdSig->pSigName->pFullName[0];
				}
				else
				{
					pszCondDesc = pCurMapEntryInfo->szCondDesc;
				}

				iOutLen += snprintf(pszData + iOutLen,
					(size_t)(iMaxLen - iOutLen),
					"\r\n   \"%s:%s,%s,%s,%s,%s,,,%s,%s,%s:\\\"%s\\\",\"",
					szAid,
					TL1_GetNtfcncdeStr(pCurAlarmItem->pStdSig->iAlarmLevel),
					pCurMapEntryInfo->szCondType,
					TL1_GetSrveffStr(pCurMapEntryInfo->iServiceAffectCode),
					szDate,
					szTime,
					szMonval,
					szThlev,
					szActivePeriod,
					pszCondDesc);

				iCount++;
			}
		}
	}

	//output tail
	if(iCount > 0)
	{
		iOutLen += TL1_PackPacketTerminatorAll(pszData + iOutLen, iMaxLen - iOutLen);
		TL1_ModifyPacketFieldAlarmCode(pszAlarmCodeAddr, iMaxAlarmLevel);
	}

	return iOutLen;
}



static int BuildReptAlmEqptEventData(OUT char *pszData, IN int iMaxLen, IN TL1_BASIC_ARGS *pThis, IN char *pszCmdCode)
{
	TL1_MODEL_INFO				*pModelInfo = &g_TL1Globals.TL1ModelInfo;
	TL1_BLOCK_INFO 				*pCurBlockInfo = NULL;
	TL1_EQUIP_INFO				*pCurTL1Equip = NULL;
	TL1_MAPENTRIES_INFO			*pMapEntriesInfo = NULL;
	TL1_MAPENTRIES_INFO			*pCurMapEntryInfo = NULL;
	TL1_COMMON_CONFIG			*pCfg = &g_TL1Globals.CommonConfig;
	//TL1_SESSION					*pstSess = &pThis->stSession;
	ALARM_SIG_VALUE				*pCurAlarmItem = NULL;
	SIG_BASIC_VALUE				*pCurSampleItem = NULL;
	SIG_BASIC_VALUE				*pCurSettingItem = NULL;
	int							iCurSampleItemType;
	int							iCurSettingItemType;
	int							i, j, iTemp;
	int							iCount = 0;
	int							iOutLen = 0;
	char						szAid[20];
	char						szDate[20];
	char						szTime[20];
	char						szActivePeriod[20];
	char						szMonval[20];
	char						szThlev[20];
	time_t						tmAlmEventTime, tmAlmStartTime, tmAlmEndTime;
	TL1_ALARM_EVENT				stAlarmEvent;
	int							iRet;
	int							iAlarmEquipID, iAlarmSigID;
	int							iMsgType, iAlarmLevel;
	int							iNtfcncde;
	char						*pszAlarmCodeAddr = NULL;
	char						*pszCondDesc = NULL;
	int							iMaxAlarmLevel;

	while(1)
	{
		iRet = Queue_Get(pThis->hEventAlarmQueue, &stAlarmEvent, FALSE, WAIT_FOR_A_QUEUE);
		if(ERR_OK != iRet)
		{
			break;
		}

		iAlarmEquipID = stAlarmEvent.pAlarmSigVal->pEquipInfo->iEquipID;
		iAlarmSigID = stAlarmEvent.pAlarmSigVal->pStdSig->iSigID;
		iMsgType = stAlarmEvent.iMsgType;
		tmAlmStartTime = stAlarmEvent.tmStartTime;
		tmAlmEndTime = stAlarmEvent.tmEndTime;
		iAlarmLevel = stAlarmEvent.iAlarmLevel;

		TRACE("iAlarmEquipID=%d, iAlarmSigID=%d, iMsgType=%x\n", iAlarmEquipID, iAlarmSigID, iMsgType);

		//find alarm signal in TL1 signal table
		for(i = 0; i < pModelInfo->iBlocksNum; i++)
		{
			pCurBlockInfo = &pModelInfo->pBlockInfo[i];
			pMapEntriesInfo = pCurBlockInfo->pTypeInfo->pstAidGroupInfo->pMapEntriesInfo;

			if( !pCurBlockInfo->bIsValid )
			{
				continue;
			}

			//process equip
			//find the same equipment in pCurBlockInfo
			for(j = 0; j < pCurBlockInfo->iTL1EquipNum; j++)
			{
				pCurTL1Equip = &pCurBlockInfo->pstTL1EquipList[j];

				if(NULL == pCurTL1Equip->pstEquip)
				{
					continue;
				}

				if( !pCurTL1Equip->pstEquip->bWorkStatus )
				{
					continue;
				}

				if(pCurTL1Equip->pstEquip->iEquipID == iAlarmEquipID)
				{
					break;//find the equipment
				}
			}

			//find none
			if(j >= pCurBlockInfo->iTL1EquipNum)
			{
				continue;
			}

			//find alarm signal in MapEntries
			for(j = 0; j < pCurTL1Equip->iSigCount; j++)
			{
				pCurAlarmItem = (ALARM_SIG_VALUE*)pCurTL1Equip->ppAlmSigValList[j];
				pCurSampleItem = pCurTL1Equip->ppSampSigValList[j];
				pCurSettingItem = pCurTL1Equip->ppSetSigValList[j];
				iCurSampleItemType = pCurTL1Equip->piSampSigInfoTypeList[j];
				iCurSettingItemType = pCurTL1Equip->piSetSigInfoTypeList[j];
				pCurMapEntryInfo = &pMapEntriesInfo[j];

				if( !pCurMapEntryInfo->bSigActive )
				{
					continue;
				}

				if(NULL == pCurAlarmItem)
				{
					continue;
				}

				if(iAlarmSigID == pCurAlarmItem->pStdSig->iSigID)
				{
					break;
				}
			}

			//find the right signal
			if(j < pCurTL1Equip->iSigCount)
			{
				break;//finish finding
			}
		}

		//can't find the right signal in TL1 signal table
		if(i >= pModelInfo->iBlocksNum)
		{
			continue;
		}

		//response data format:
		//^^^"<aid>:<ntfcncde>,<condtype>,<srveff>, [<ocrdat>],[<ocrtm>],[<locn>],[<dirn>], [<monval>],[<thlev>][,<tmper>] 
		//[:[<conddescr>],[<aiddet>][,<obsdbhvr>[,<exptdbhvr>]] [:[<dgntype>][,<tblistl>]]]"
		//<aidtype>Valid values for this application are:EQPT - equipment
		//<ntfcncde>This is the notification code associated with a single alarm condition given in this block.
		//<condtype>This is the condition type of the alarm condition being given in this block.
		//<srveff>This is the effect on service caused by the single alarm condition given in this block.
		//<ocrdat>This is the date when the triggering event occurred. 
		//The format for this parameter is MOY-DOM(Month-Of-Year - Day-Of-Month).
		//<ocrtm>This is the time when the triggering event occurred. 
		//The format for this parameter is HOD-MOH-SOM(Hour-Of-Day - Minute-Of-Hour - Second-Of-Minute)
		//<locn> - location of alarm condition.A NULL value is inserted for them
		//<dirn> - direction of alarm condition.A NULL value is inserted for them
		//<tmper>This is the accumulation time period for PM parameters. 
		//In this application it represents the length of time the alarm condition has been active.

		if(iOutLen >= iMaxLen)
		{
			break;
		}
		else if(iCount >= TL1_ALM_MAX_NUM)
		{
			break;
		}

		if(0 == iCount)
		{
			iMaxAlarmLevel = ALARM_LEVEL_NONE;
			iOutLen += TL1_PackPacketAutonomousHeader(pszData + iOutLen,
				iMaxLen - iOutLen,
				pCfg->szSystemIdentifier,
				TL1_E_ALM_AUTO_MSG,
				TL1_GetAtag(pThis),
				pszCmdCode,
				&pszAlarmCodeAddr);
		}
		else if(0 == iCount % TL1_ALM_SPLIT_NUM)
		{
			iOutLen += TL1_PackPacketTerminatorOne(pszData + iOutLen, iMaxLen - iOutLen);
			TL1_ModifyPacketFieldAlarmCode(pszAlarmCodeAddr, iMaxAlarmLevel);

			iMaxAlarmLevel = ALARM_LEVEL_NONE;
			iOutLen += TL1_PackPacketAutonomousHeader(pszData + iOutLen,
				iMaxLen - iOutLen,
				pCfg->szSystemIdentifier,
				TL1_E_ALM_AUTO_MSG,
				TL1_GetAtag(pThis),
				pszCmdCode,
				&pszAlarmCodeAddr);
		}

		if(_ALARM_OCCUR_MASK == iMsgType)
		{
			if(iMaxAlarmLevel < iAlarmLevel)
			{
				iMaxAlarmLevel = iAlarmLevel;
			}

			iNtfcncde = iAlarmLevel;
		}
		else
		{
			if(iMaxAlarmLevel < ALARM_LEVEL_NONE)
			{
				iMaxAlarmLevel = ALARM_LEVEL_NONE;
			}

			iNtfcncde = TL1_E_NTFCNCDE_CL;
		}

		//<aid>
		if(TL1_CHAR_NULL == pCurTL1Equip->szSubAidName[0])
		{
			snprintf(szAid, sizeof(szAid), "%s", pCurBlockInfo->pszMainAidName);
		}
		else if ( pCfg->iAidDelimeter )
		{
			snprintf(szAid, sizeof(szAid), "%s-%s", pCurBlockInfo->pszMainAidName, pCurTL1Equip->szSubAidName);
		}
		else
		{
			snprintf(szAid, sizeof(szAid), "%s%s", pCurBlockInfo->pszMainAidName, pCurTL1Equip->szSubAidName);
		}	

		if(_ALARM_OCCUR_MASK == iMsgType)
		{
			tmAlmEventTime = tmAlmStartTime;
		}
		else
		{
			tmAlmEventTime = tmAlmEndTime;
		}

		//<ocrdat>
		TimeToString(tmAlmEventTime, TL1_TIME_CHN_FMT_DATE_2, szDate, sizeof(szDate));
		//<ocrtm>
		TimeToString(tmAlmEventTime, TL1_TIME_CHN_FMT_TIME, szTime, sizeof(szTime));

		//<tmper>
		iTemp = time(NULL) - tmAlmEventTime;
		if((iTemp >= TL1_C_DAY) || (iTemp <= -1*TL1_C_DAY))
		{
			//DAY (days) - used if alarm is active longer than 24 hours
			snprintf(szActivePeriod, sizeof(szActivePeriod), "%d-DAY", (int)(iTemp/TL1_C_DAY));
		}
		else
		{
			//MIN (minutes) - used if alarm is active less than 24 hours.
			snprintf(szActivePeriod, sizeof(szActivePeriod), "%d-MIN", (int)(iTemp/TL1_C_MIN));
		}

		OutMonitorSigVal(pCurMapEntryInfo->iMonitorFormat, iCurSampleItemType, pCurSampleItem, szMonval, sizeof(szMonval));
		OutMonitorSigVal(pCurMapEntryInfo->iMonitorFormat, iCurSettingItemType, pCurSettingItem, szThlev, sizeof(szThlev));

		if(TL1_CHAR_NULL == pCurMapEntryInfo->szCondDesc[0])
		{
			pszCondDesc = pCurAlarmItem->pStdSig->pSigName->pFullName[0];
		}
		else
		{
			pszCondDesc = pCurMapEntryInfo->szCondDesc;
		}

		iOutLen += snprintf(pszData + iOutLen,
			(size_t)(iMaxLen - iOutLen),
			"\r\n   \"%s:%s,%s,%s,%s,%s,,,%s,%s,%s:\\\"%s\\\",\"",
			szAid,
			TL1_GetNtfcncdeStr(iNtfcncde),
			pCurMapEntryInfo->szCondType,
			TL1_GetSrveffStr(pCurMapEntryInfo->iServiceAffectCode),
			szDate,
			szTime,
			szMonval,
			szThlev,
			szActivePeriod,
			pszCondDesc);

		iCount++;
	}

	//output tail
	if(iCount > 0)
	{
		iOutLen += TL1_PackPacketTerminatorAll(pszData + iOutLen, iMaxLen - iOutLen);
		TL1_ModifyPacketFieldAlarmCode(pszAlarmCodeAddr, iMaxAlarmLevel);
	}

	return iOutLen;
}


static int ExecuteReptAlmEqpt(IN TL1_BASIC_ARGS *pThis,
							  IN unsigned char *szInData,
							  OUT unsigned char *szOutData)
{
	TL1_SESSION				*pstSess = &pThis->stSession;
	//TL1_COMMAND				*pstCmd = &pstSess->stCommand;
	//TL1_CMD_FIELD			*pFieldAid = NULL;
	HANDLE					hLocalThread;
	int						iOutLen;
	char					*pszOut = (char *)szOutData;

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	//UNUSED(szInData);

#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteReptAlmEqpt!!!!\n");
	TRACE("\n ExecuteReptAlmEqpt:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';

	//2. build response message
	if( pstSess->bNeedReportAllAlarm )
	{
		pstSess->bNeedReportAllAlarm = FALSE;

		TL1_ClearAlarmEventQueue(pThis->hEventAlarmQueue, FALSE);
		iOutLen += BuildReptAlmEqptData(pszOut + iOutLen, TL1_RESP_SIZE - iOutLen, pThis, (char *)szInData);
	}
	else
	{
		iOutLen += BuildReptAlmEqptEventData(pszOut + iOutLen, TL1_RESP_SIZE - iOutLen, pThis, (char *)szInData);
	}

#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteReptAlmEqpt has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}


static int ExecuteRespErrCode(IN TL1_BASIC_ARGS *pThis,
							  IN unsigned char *szInData,
							  OUT unsigned char *szOutData)
{
	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMAND				*pstCmd = &pstSess->stCommand;
	//TL1_CMD_FIELD			*pFieldAid = NULL;
	HANDLE					hLocalThread;
	int						iOutLen;
	char					*pszOut = (char *)szOutData;
	char					*pszCtag = NULL;

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	/* not used the param */
	UNUSED(szInData);

#ifdef _DEBUG_TL1_LINKLAYER
	TRACE("\nBegin ExecuteRespErrCode!!!!\n");
	TRACE("\n RespErrCode:szInData is: %s\n", szInData);
#endif

	hLocalThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hLocalThread);

	//1. init response message
	iOutLen = 0;
	pszOut[0] = '\0';


	if(pstSess->bIsValid && pstSess->bIsLogin)
	{
		if((NULL != pstCmd->pstCtag)
			&&(pstCmd->pstCtag->bIsExist)
			&& (pstCmd->pstCtag->iLen > 1))
		{
			pszCtag = pstCmd->pstCtag->szData;
		}
		else
		{
			pszCtag = "0";
		}


		iOutLen = TL1_PackOneErrPacket(pszOut,
			TL1_RESP_SIZE,
			pCfg->szSystemIdentifier,
			pszCtag,
			TL1_GetRespErrStr(pstCmd->iErrCode));
	}

#ifdef _DEBUG_TL1_CMD_HANDLER
	TRACE_TL1_TIPS("ExecuteRespErrCode has been executed ok!");
	TRACE("Reply Data:\n");
	TRACE("\tiLen=%d, data=%s\n", iOutLen, pszOut);
#endif //_DEBUG_TL1_CMD_HANDLER

	return TL1_ERR_OK;
}


static int RespErrCode(IN TL1_BASIC_ARGS *pThis,
					   IN char *szInData,
					   OUT char *szOutData,
					   IN int iErrCode)
{
	//TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_SESSION				*pstSess = &pThis->stSession;
	TL1_COMMAND				*pstCmd = &pstSess->stCommand;

	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	pstCmd->iErrCode = iErrCode;

	return ExecuteRespErrCode(pThis, (unsigned char *)szInData, (unsigned char *)szOutData);
}


static BOOL VerifyExecutable(TL1_BASIC_ARGS *pThis, TL1_CMD_HANDLER *pCmdHandler)
{
	TL1_SESSION		*pstSess = &pThis->stSession;
	int				iRequireLevel = pCmdHandler->iLevel;
	int				iCurLevel;

	if(pstSess->bIsValid && pstSess->bIsLogin)
	{
		iCurLevel = pstSess->stUser.stBaseInfo.byLevel;
	}
	else
	{
		iCurLevel = -1;
	}

	if(iCurLevel >= iRequireLevel)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
 * FUNCTION : TL1_InitCmdHander
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: InitTL1Globals
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 14:39
 *==========================================================================*/
void TL1_InitCmdHander(void)
{
	TL1_CMD_HANDLER *pCmdHandlers;

	pCmdHandlers = g_TL1Globals.CmdHandlers;

	//struct tagTL1CmdHandler
	//{
	//	const char				*szCmdType;
	//	BOOL					bIsWriteCommand;//unused
	//	BOOL					bIsSpecialCommand;//just can be called by local app
	//	//  Security access level of command, defined in enum AUTHORITY_LEVEL 
	//	int						iLevel;
	//	TL1_CMD_EXECUTE_FUN		funExecute;
	//};
	//typedef struct tagTL1CmdHandler TL1_CMD_HANDLER;

	DEF_TL1_CMD_HANDLER(&pCmdHandlers[0], "CANC", FALSE, TRUE, -1, ExecuteCancSession);//special cmd for automatic message
	DEF_TL1_CMD_HANDLER(&pCmdHandlers[1], "REPT ALM EQPT", FALSE, TRUE, -1, ExecuteReptAlmEqpt);//special cmd for automatic message
	DEF_TL1_CMD_HANDLER(&pCmdHandlers[2], "RESP ERR CODE", FALSE, TRUE, -1, ExecuteRespErrCode);//special cmd for automatic message
	DEF_TL1_CMD_HANDLER(&pCmdHandlers[3], "ACT-USER", FALSE, FALSE, -1, ExecuteActUser);
	DEF_TL1_CMD_HANDLER(&pCmdHandlers[4], "CANC-USER", FALSE, FALSE, -1, ExecuteCancUser);
	DEF_TL1_CMD_HANDLER(&pCmdHandlers[5], "RTRV-ALM-ALL", FALSE, FALSE, BROWSER_LEVEL, ExecuteRtrvAlm);
	DEF_TL1_CMD_HANDLER(&pCmdHandlers[6], "RTRV-ALM-EQPT", FALSE, FALSE, BROWSER_LEVEL, ExecuteRtrvAlm);
	DEF_TL1_CMD_HANDLER(&pCmdHandlers[7], "RTRV-EQPT", FALSE, FALSE, BROWSER_LEVEL, ExecuteRtrvEqpt);
	DEF_TL1_CMD_HANDLER(&pCmdHandlers[8], "RTRV-HDR", FALSE, FALSE, BROWSER_LEVEL, ExecuteRtrvHdr);

	//return;
}


/*==========================================================================*
 * FUNCTION : *GetCmdHandler
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: TL1_DecodeAndPerform
 * ARGUMENTS: const unsigned char *  szCmdData : 
 * RETURN   : CMD_HANDLER : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-11-14 17:33
 *==========================================================================*/
static TL1_CMD_HANDLER *GetCmdHandler(IN const unsigned char * szCmdData, IN BOOL bIsNormalMode)
{
	int i;
	//char szCmdType[5];
	TL1_CMD_HANDLER *pCmdHandler;

	//strncpyz(szCmdType, szCmdData, 5);
	pCmdHandler = g_TL1Globals.CmdHandlers;
	for (i = 0; i < TL1_CMD_NUM; i++, pCmdHandler++)
	{
		if( bIsNormalMode )
		{
			if( pCmdHandler->bIsSpecialCommand )
			{
				continue;
			}
		}

		if (strcmp((const char *)szCmdData, pCmdHandler->szCmdType) == 0)
		{
			return pCmdHandler;
		}
	}
    //TRACE("\ni is: %d\n", i);
	return NULL;
}

/*==========================================================================*
 * FUNCTION : TL1_DecodeAndPerform
 * PURPOSE  : 
 * CALLS    : GetCmdHandler
 *			  VerifyExecutable
 * CALLED BY: Static Machine functions
 * ARGUMENTS: TL1_BASIC_ARGS         *pThis       : 
 *            const unsigned char   *szCmdData    : 
 * RETURN   : void : 
 * COMMENTS : length of szCmdRespBuff is TL1_RESP_SIZE
 * CREATOR  : HANTao                   DATE: 2006-05-13 17:04
 *==========================================================================*/
int TL1_DecodeAndPerform(IN OUT TL1_BASIC_ARGS *pThis,
						IN unsigned char *szCmdData,
						IN BOOL bIsNormalMode)
{
	//TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_CMD_HANDLER			*pCmdHandler;
	char					*pszCmdRespBuff = (char *)pThis->szCmdRespBuff;
	int						iRTN/*, i*/;

	pszCmdRespBuff[0] = TL1_CHAR_NULL;

	/* 1.Get Cmd Handler */
	
	pCmdHandler = GetCmdHandler(szCmdData, bIsNormalMode);
	
	if (NULL == pCmdHandler)
	{
        #ifdef _DEBUG_TL1_CMD_HANDLER
		    TRACE_TL1_TIPS("Get Command Handler failed");
        #endif //_DEBUG_TL1_CMD_HANDLER

		RespErrCode(pThis, "ICNV", pszCmdRespBuff, TL1_E_RESP_ERR_ICNV);

		return TL1_ERR_EXEC_NO_CMD;//never run to here, always will find one
	}

	
#ifdef _DEBUG_TL1_CMD_HANDLER
	{
		char *szFlagText;
		if (pCmdHandler->bIsWriteCommand)
		{
			szFlagText = "TRUE";
		}
		else
		{
			szFlagText = "FALSE";
		}

		TRACE_TL1_TIPS("Get Cmd Handler");
		TRACE("Cmd Handler info:\n");
		TRACE("\tCmd Type: %s\n", pCmdHandler->szCmdType);
		TRACE("\tIs Write Cmd: %s\n", szFlagText);
	}
#endif //_DEBUG_TL1_CMD_HANDLER

	/* 2.judge executable */
	if ( !VerifyExecutable(pThis, pCmdHandler) )
	{
		RespErrCode(pThis, "PICC", pszCmdRespBuff, TL1_E_RESP_ERR_PICC);

		return TL1_ERR_EXEC_NO_AUTHORITY;
	}

	/* 3.excute it */
	//memset(pszCmdRespBuff, '\0', sizeof(char)*TL1_RESP_SIZE);
	iRTN = pCmdHandler->funExecute(pThis, szCmdData, pszCmdRespBuff);

	return iRTN;
}

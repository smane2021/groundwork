/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : ydn_utility.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "tl1.h"

#include <ctype.h>


/*==========================================================================*
 * FUNCTION : TL1_GetADR
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BYTE : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-02 14:43
 *==========================================================================*/
__INLINE BYTE TL1_GetADR(void)
{
	TL1_COMMON_CONFIG *pCommonCfg;

	pCommonCfg = &g_TL1Globals.CommonConfig;

	return pCommonCfg->byADR;
}


/*==========================================================================*
 * FUNCTION : TL1_CFG_CheckBase
 * PURPOSE  : basic function for check a null-ended string, used by other 
 *            specifically realized check functions
 * CALLS    : 
 * CALLED BY: specifically realized check functions, such as TL1_CheckPhoneNumber
 * ARGUMENTS: const char  *szObject : 
 *            LEGAL_CHR   fnCmp     : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 11:18
 *==========================================================================*/
BOOL TL1_CFG_CheckBase(IN char *szObject, IN LEGAL_CHR fnCmp)
{
	char *p;

	if (szObject == NULL)
	{
		return FALSE;
	}

	p = szObject;
	for (; p != NULL; p++)
	{
		if (*p == '\0')
		{
			return TRUE;
		}

		if (!fnCmp(*p))
		{
			return FALSE;
		}

	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : TL1_CFG_CheckBase
 * PURPOSE  : basic function for check a null-ended string, used by other 
 *            specifically realized check functions
 * CALLS    : 
 * CALLED BY: specifically realized check functions, such as TL1_CheckPhoneNumber
 * ARGUMENTS: const char  *szObject : 
 *            LEGAL_CHR   fnCmp     : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 11:18
 *==========================================================================*/
BOOL TL1_CFG_Check(IN char *szObject, IN LEGAL_CHR fnCmp)
{
	char *p;

	if (szObject == NULL || *szObject == '\0')
	//if (szObject == NULL)
	{
		return FALSE;
	}

	p = szObject;
	for (; p != NULL; p++)
	{
		if (*p == '\0')
		{
			return TRUE;
		}

		if (!fnCmp(*p))
		{
			return FALSE;
		}

	}

	return FALSE;
}



/*==========================================================================*
 * FUNCTION : TL1_CheckPhoneNumber
 * PURPOSE  : for checking phone number null-ended string
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szPhoneNumber : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 20:25
 *==========================================================================*/
static BOOL IsPhoneNumber(IN char c)
{
	return  ((c >= '0' && c <= '9') ||
		 c == ',' || c == '-');
}
	
BOOL TL1_CheckPhoneNumber(IN char *szPhoneNumber)
{
	//return TL1_CFG_Check(szPhoneNumber, IsPhoneNumber);
	return TL1_CFG_CheckBase(szPhoneNumber, IsPhoneNumber);
}
	

/*==========================================================================*
 * FUNCTION : TL1_CheckIPAddress
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szIPAddress : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 20:49
 *==========================================================================*/
static BOOL IsIPAddress(IN char c)
{
	return  ((c >= '0' && c <= '9') ||
		c == '.' || c == ':');
}

BOOL TL1_CheckIPAddress(IN const char *szIPAddress)
{
	return TL1_CFG_CheckBase((char *)szIPAddress, IsIPAddress);
}


/*==========================================================================*
 * FUNCTION : TL1_CheckBautrateCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szBautrateCfg : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 21:49
 *==========================================================================*/
static BOOL IsBautrateCfg(IN char c)
{
	return  ((c >= '0' && c <= '9') ||
		c == ',' || c == 'n' || c == 'N');
}

BOOL TL1_CheckBautrateCfg(IN const char *szBautrateCfg)
{
	int i, iLen, count = 0;

	if (!TL1_CFG_CheckBase((char *)szBautrateCfg, IsBautrateCfg))
	{
		return FALSE;
	}

	iLen = (int)strlen(szBautrateCfg);
	for (i = 0; i < iLen; i++)
	{
		if (szBautrateCfg[i] == ',')
		{
			count++;
		}
	}

	if (count != 3)
	{
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : TL1_CheckModemCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szBautrateCfg : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 21:55
 *==========================================================================*/
BOOL TL1_CheckModemCfg(IN const char *szModemCfg)
{
	char szSubField[30];

	szModemCfg = Cfg_SplitString((char *)szModemCfg, szSubField, sizeof(szSubField), ':');
	if (!TL1_CheckBautrateCfg(szSubField))
	{
		return FALSE;
	}

	Cfg_SplitString((char *)szModemCfg, szSubField, sizeof(szSubField), ':');
	if (!TL1_CheckPhoneNumber(szSubField))
	{
		return FALSE;
	}
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : TL1_AsciiHexToChar
 * PURPOSE  : get value from ascii hex. eg: "1A" = 1*16+10 =26
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-06-12 19:24
 *==========================================================================*/
BYTE TL1_AsciiHexToChar(IN const unsigned char *pStr)
{
	BYTE value;
	unsigned char c;

	ASSERT(pStr);

	c = pStr[0];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = c * 0x10;

	c = pStr[1];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value += c;

	return value;
}

/*==========================================================================*
 * FUNCTION : TL1_ThreeAsciiHexToChar
 * PURPOSE  : get value from ascii hex. eg: "11A" = 1*16*16 + 1*16+10 =282
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-06-12 19:24
 *==========================================================================*/
BYTE TL1_ThreeAsciiHexToChar(IN const unsigned char *pStr)
{
	BYTE value;
	unsigned char c;

	ASSERT(pStr);

	c = pStr[0];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = c * 0x100;

	c = pStr[1];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = value + c * 0x10;

	c = pStr[2];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value += c;

	return value;
}

__INLINE unsigned char	HexToAscii(IN DWORD Input)
{
	if(Input > 9)
	{
		return Input + 0x37;					/*A - F*/
	}
	else
	{
		return Input + 0x30;					/*0 - 9*/
	}
}

__INLINE void	HexToTwoAsciiData(IN DWORD Input, IN unsigned char *szOutData)
{
	*szOutData = HexToAscii(Input >> 4);
	*(szOutData + 1) = HexToAscii(Input & 0x0f);
	return;
}

void	HexToFourAsciiData(IN DWORD Input, IN unsigned char *szOutData)
{
	*szOutData = HexToAscii(Input >> 12);
	*(szOutData + 1) = HexToAscii((Input & 0x0f00) >> 8);
	*(szOutData + 2) = HexToAscii((Input & 0x00f0) >> 4);
	*(szOutData + 3) = HexToAscii(Input & 0x000f);
	
}
/*==========================================================================*
 * FUNCTION : TL1_IntToAscii
 * PURPOSE  : get value from ascii hex. eg: "1A" = 1*16+10 =26
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-06-12 19:24
 *==========================================================================*/
void TL1_IntToAscii(IN DWORD dValue, OUT unsigned char *cStr)
{
	DWORD cValue[2];

	ASSERT(cStr);

    cValue[1] = dValue % 100;
    cValue[0] = dValue / 100;

    *cStr = HexToAscii(cValue[0] >> 4);
    *(cStr + 1) = HexToAscii(cValue[0] & 0x0f);

    *(cStr + 2)  = HexToAscii(cValue[1] >> 4);
    *(cStr + 3)  = HexToAscii(cValue[1] & 0x0f);
    
    *(cStr + 4)  = '\0';
}

/*==========================================================================*
 * FUNCTION : TL1_CharToInt
 * PURPOSE  : get value from char. eg: "11" = 1*10+1 =11
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-09-20 09:24
 *==========================================================================*/
unsigned int TL1_CharToInt(IN const unsigned char *pStr, IN int iCNum)
{
	ASSERT(pStr);

	unsigned int iValue = 0;
	int iPower10 = 1;
	int i, j;
	unsigned int pValue[10]; 
	
	for(i = 0; i < iCNum; i++)
		pValue[i] = *(pStr + i) - '0';

	for(i = 0; i < iCNum; i++)
	{
		for(j = 1; j < iCNum - i; j++ )
			iPower10 *= 10;

		pValue[i] *= iPower10;
		iValue += pValue[i]; 
		iPower10 = 1;
	}

	return iValue;

}



/*==========================================================================*
 * FUNCTION : TL1_IsStrAsciiHex
 * PURPOSE  : assistant functions
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const unsigned char  *pStr : 
 *            int                  iLen  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-12 17:24
 *==========================================================================*/
BOOL TL1_IsStrAsciiHex(IN const unsigned char *pStr, IN int iLen)
{
	int i;
	
	for (i = 0; i < iLen; i++)
	{
		if ((pStr[i] >= '0' && pStr[i] <= '9') ||
			(pStr[i] >= 'A' && pStr[i] <= 'F'))
		{
			continue;
		}
		
		return FALSE;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : TL1_GetUnprintableChr
 * PURPOSE  : for trace
 * CALLS    : 
 * CALLED BY: TL1_PrintEvent
 * ARGUMENTS: IN unsigned char  chr   : 
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-25 14:30
 *==========================================================================*/
char *TL1_GetUnprintableChr(IN unsigned char chr)
{
	switch (chr)
	{
	case 0x06:
		return "<ACK>";

	case 0x15:
		return "<NAK>";

	case 0x04:
		return "<EOT>";

	case 0x05:
		return "<ENQ>";

	case 0x01:
		return "<SOH>";

	case 0x02:
		return "<STX>";

	case 0x03:
		return "<ETX>";

	case 0x0D:
		return "<EOI>";

	default:
		return "<NOT EXIST>";
	}
}


/*==========================================================================*
 * FUNCTION : TL1_PrintEvent
 * PURPOSE  : print YDN Event info(used for debug)
 * CALLS    : TL1_GetUnprintableChr
 * CALLED BY: 
 * ARGUMENTS: TL1_EVENT  *pEvent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-24 10:33
 *==========================================================================*/
void TL1_PrintEvent(IN TL1_EVENT *pEvent)
{
#ifdef _SHOW_TL1_EVENT_INFO
	int i;
	unsigned char chr;
	char szEventType[50];

	switch (pEvent->iEventType)
	{
	case TL1_FRAME_EVENT:
		strncpy(szEventType, "TL1_FRAME_EVENT", 50);
		break;

	case TL1_CONNECTED_EVENT:
		strncpy(szEventType, "TL1_CONNECTED_EVENT", 50);
		break;

	case TL1_CONNECT_FAILED_EVENT:
		strncpy(szEventType, "TL1_CONNECT_FAILED_EVENT", 50);
		break;

	case TL1_DISCONNECTED_EVENT:
		strncpy(szEventType, "TL1_DISCONNECTED_EVENT", 50);
		break;

	case TL1_TIMEOUT_EVENT:
		strncpy(szEventType, "TL1_TIMEOUT_EVENT", 50);
		break;

	default:
		strncpy(szEventType, "ERROR: not defined event type!", 50);
	}

	TRACE("TL1 Event Info\n");
	TRACE("Event Type: %s\n", szEventType);
	TRACE("Event Data length: %d\n", pEvent->iDataLength);

	TRACE("Event Data(Hex format):");
	for (i = 0; i < pEvent->iDataLength; i++)
	{
		TRACE("%02X ", pEvent->sData[i]);
	}
	TRACE("\n");

	TRACE("Event Data(Text format):");
	for (i = 0; i < pEvent->iDataLength; i++)
	{
		chr = pEvent->sData[i];
		if (chr < 0x21 || chr > 0X7E)
		{
			TRACE("%s", TL1_GetUnprintableChr(chr));
		}
		else
		{
			TRACE("%c", chr);
		}
	}
	TRACE("\n");
		
	TRACE("Event Flags: \nSkip Falg: %d	Dummy Flag: %d\n", 
		pEvent->bSkipFlag, pEvent->bDummyFlag);

#endif //_DEBUG_TL1_LINKLAYER
    UNUSED(pEvent);
	return;
}


/*==========================================================================*
 * FUNCTION : TL1_PrintState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iState : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-27 09:45
 *==========================================================================*/
void TL1_PrintState(IN int iState)
{
#ifdef _SHOW_TL1_CUR_STATE
	char *szStateName;

	if (g_TL1Globals.CommonConfig.iProtocolType == TL1)  //YDN State machine
	{
		switch (iState)
		{
		case TL1_IDLE:
			szStateName = "TL1_IDLE";
			break;
/*
		case TL1_WAIT_FOR_POLL:
			szStateName = "TL1_WAIT_FOR_POLL";
			break;

		case TL1_WAIT_FOR_RESP_ACK:
			szStateName = "TL1_WAIT_FOR_RESP_ACK";
			break;

		case TL1_SEND_CALLBACK:
			szStateName = "TL1_SEND_CALLBACK";
			break;

		case TL1_WAIT_FOR_CALLBACK_ACK:
			szStateName = "TL1_WAIT_FOR_CALLBACK_ACK";
			break;
*/
		//case TL1_SEND_ALARM:
		//	szStateName = "TL1_SEND_ALARM";
		//	break;
/*
		case TL1_WAIT_FOR_ALARM_ACK:
			szStateName = "TL1_WAIT_FOR_ALARM_ACK";
			break;
*/
		case TL1_STATE_MACHINE_QUIT:
			szStateName = "STATE_MACHINE_QUIT";
			break;

		default:
			szStateName = "Error State Name";
		}
	}
/*
	else
	{
		switch (iState)
		{
		case SOC_OFF:
			szStateName = "SOC_OFF";
			break;

		case SOC_SEND_ENQ:
			szStateName = "SOC_SEND_ENQ";
			break;

		case SOC_WAIT_DLE0:
			szStateName = "SOC_WAIT_DLE0";
			break;

		case SOC_ALARM_REPORT:
			szStateName = "SOC_ALARM_REPORT";
			break;

		case SOC_WAIT_CMD:
			szStateName = "SOC_WAIT_CMD";
			break;

		case SOC_WAIT_ACK:
			szStateName = "SOC_WAIT_ACK";
			break;

		case STATE_MACHINE_QUIT:
			szStateName = "STATE_MACHINE_QUIT";
			break;

		default:
			szStateName = "Error SOC State Name";

		}
	}
*/
	TRACE("++++++++++++++++++++++++++++++++++++++++++++++\n");
	TRACE("+  Current State: %-25s  +\n", szStateName);
	TRACE("++++++++++++++++++++++++++++++++++++++++++++++\n");
	
	return;
#endif //_SHOW_TL1_CUR_STATE

	UNUSED(iState);
	return;
}

/*==========================================================================*
 * FUNCTION : TL1_GetReportMsg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN BOOL      bAlarm		: TRUE  for SendAlarm 
 *										  FALSE for SendCallback
 *		      IN OUT char  *szReportMsg : 
 *            IN int       iBufLen      : 
 * RETURN   : int : 0 for success, 
 *					1 for call DXI to get site name failed,
 *					2 for site name is too long
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-06 19:18
 *==========================================================================*/
int TL1_GetReportMsg(IN BOOL bAlarm, 
					 IN OUT char *szReportMsg, 
					 IN int iBufLen)
{
	UNUSED(iBufLen);
	size_t iLen;
	int nError, nInterfaceType, nVarID, nVarSubID, nBufLen;
	LANG_TEXT *pLangInfo;
	char szSiteName[64];

	BYTE  byADR;

	ASSERT(iBufLen > 70);

	/* 1.get site name */
	nInterfaceType = VAR_ACU_PUBLIC_CONFIG;
	nVarID = SITE_NAME;
	nVarSubID = 0;
	pLangInfo = NULL;
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pLangInfo,			
		0);

	if (nError != ERR_DXI_OK)
	{
		szReportMsg[0] = '\0';
		return 1;
	}

	strncpyz(szSiteName, pLangInfo->pFullName[0], sizeof(szSiteName));

	/* 2.get CCID */
	byADR = g_TL1Globals.CommonConfig.byADR;

	/* 3.now build the string */
	if (bAlarm)  //called by OnSendAlarm state
	{
		sprintf(szReportMsg, "%s!%02X!E*", szSiteName, byADR);
	}
	else  //called by OnSendCallback state
	{
		sprintf(szReportMsg, "%s!%02X!C*", szSiteName, byADR);
	}

	/* 4.check buffer length */
	iLen = strlen(pLangInfo->pFullName[0]);
	if (iLen >= 64)
	{
		return 2;
	}

	return 0;

}

/*==========================================================================*
 * FUNCTION : PrintCommonCfg
 * PURPOSE  : Print YDN Common Config info
 * CALLS    : 
 * CALLED BY: PrintYDNConfigInfo
 * ARGUMENTS: TL1_COMMON_CONFIG *pConfig : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-24 10:39
 *==========================================================================*/
void TL1_PrintCommonCfg(IN TL1_COMMON_CONFIG *pConfig)
{
	char *pText;
	int iTmp;

	printf("************************************************\n");
	printf("*****          TL1 Common Config         *******\n");
	printf("************************************************\n");

	/* protocol type */
	iTmp = pConfig->iProtocolType;
	switch (iTmp)
	{
	case EEM_TL1:
		pText = "EEM_TL1";
		break;
	case RSOC_TL1:
		pText = "RSOC_TL1";
		break;
	case SOC_TPE_TL1:
		pText = "SOC_TPE_TL1";
		break;
	case YDN_TL1:
		pText = "YDN_TL1";
		break;
	case MODBUS_TL1:
		pText = "MODBUS_TL1";
		break;
	case TL1:
		pText = "TL1";
		break;
	default:
		pText = "ERROR! not such protocol type!";
		break;
	}
	TRACE("Protocol Type: %s\n", pText);

	/* ccid and socid */
	iTmp = pConfig->byADR;
	TRACE("byADR is: %d\n", iTmp);

	/* media */
	switch (pConfig->iMediaType)
	{
	case 0:
		pText = "Leased Line";
		break;

	case 1:
		pText = "Modem";
		break;

	case 2:
		pText = "TCPIP";
		break;

	default:
		pText = "ERROR, not such media.\n";
		break;
	}

	TRACE("Operation media is: %s\n", pText);

	TRACE("szCommPortParam=%s\n", pConfig->szCommPortParam);
	TRACE("iPortActivation=%d\n", pConfig->iPortActivation);
	TRACE("iPortKeepAlive=%d\n", pConfig->iPortKeepAlive);
	TRACE("iSessionTimeout=%d\n", pConfig->iSessionTimeout);
	TRACE("szAutoLoginUser=%s\n", pConfig->szAutoLoginUser);
	TRACE("szSystemIdentifier=%s\n", pConfig->szSystemIdentifier);


	return;
}

/*==========================================================================*
 * FUNCTION : TL1_ClearEventQueue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hq       : 
 *            BOOL    bDestroy : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-21 15:11
 *==========================================================================*/
void TL1_ClearEventQueue(IN HANDLE hq, IN BOOL bDestroy)
{
	int i, iCount, iRet;
	TL1_EVENT *pEvent;

	if (hq)
	{
		iCount = Queue_GetCount(hq, NULL);

		/* clear memory */
		for (i = 0; i < iCount; i++)
		{
			iRet = Queue_Get(hq, &pEvent, FALSE, 1000);

			if (iRet == ERR_OK)
			{
				DELETE_TL1_EVENT(pEvent);
			}
		}

		/* destroy queue */
		if (bDestroy)
		{
			Queue_Destroy(hq);
		}
	}
}

void TL1_ClearAlarmEventQueue(IN HANDLE hq, IN BOOL bDestroy)
{
	if (hq)
	{
		Queue_Empty(hq);

		/* destroy queue */
		if (bDestroy)
		{
			Queue_Destroy(hq);
		}
	}
}


/*==========================================================================*
 * FUNCTION : TL1_PrintfStr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hq       : 
 *            BOOL    bDestroy : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-21 15:11
 *==========================================================================*/
void TL1_PrintfStr(IN int nLen, IN char *pStr)
{
	int i;
	for(i = 0; i < nLen; i++, pStr++)
	{
		TRACE("%02x",*pStr);
	}
	if(nLen > 0)
	{
		TRACE("\n");
	}
}


/*==========================================================================*
 * FUNCTION : GetEquipID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iEquipTypeID : 
 * RETURN   : pCurEquip->iEquipID : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-19 11:29
 *==========================================================================*/
int GetEquipIDOrder(IN int iEquipTypeID)
{
	int i, iEquipID;
	EQUIP_INFO *pCurEquip;
	pCurEquip = g_TL1Globals.pEquipInfo;

	for (i = 0; i <  g_TL1Globals.iEquipNum; i++, pCurEquip++)
	{
		if(iEquipTypeID == g_TL1Globals.iEquipIDOrder[i][1])
		{
		    iEquipID = pCurEquip->iEquipID;
			break;
		}
	}

	/* not found */
	if (i == g_TL1Globals.iEquipNum)
	{
		i = 0;
		//TRACE("\n i is: %d\n", i);
	}

	return i;
}

/*==========================================================================*
* FUNCTION : GetEquipID
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iEquipTypeID : 
* RETURN   : pCurEquip->iEquipID : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2006-05-19 11:29
*==========================================================================*/
BOOL TL1_GetEquipIDOrder(IN int iEquipTypeID, IN OUT int* iEquipNum)
{

	int i, iEquipID;
	EQUIP_INFO *pCurEquip;
	pCurEquip = g_TL1Globals.pEquipInfo;
	*iEquipNum = MAX_EQUIP_NUM;

	for (i = 0; i <  g_TL1Globals.iEquipNum; i++, pCurEquip++)
	{
		if(iEquipTypeID == g_TL1Globals.iEquipIDOrder[i][1])
		{
			iEquipID = pCurEquip->iEquipID;
			break;
		}
	}

	/* not found */
	if (i == g_TL1Globals.iEquipNum)
	{
		return FALSE;

	}

	*iEquipNum =i;
	return TRUE;
}

/*==========================================================================*
* FUNCTION : GetEquipID
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iEquipTypeID : 
* RETURN   : pCurEquip->iEquipID : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2006-05-19 11:29
*==========================================================================*/
BOOL TL1_GetEquiplistIDOrder(IN int iEquipID, IN OUT int* iEquipNum)
{
	int i;

	for (i = 0; i < g_TL1Globals.iEquipNum; i++)
	{
		if(iEquipID == g_TL1Globals.iEquipIDOrder[i][0])
		{
			break;
		}
	}

	/* not found */
	if (i == g_TL1Globals.iEquipNum)
	{
		return FALSE;
	}

	*iEquipNum =i;
	return TRUE;
}



/*==========================================================================*
* FUNCTION : TL1_CheckSPortParam
* PURPOSE  : Check the SPort Param
* CALLS    : 
*			  
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Wang Jing                   DATE: 2006-11-09 16:02
*==========================================================================*/
BOOL TL1_CheckSPortParam(IN char *pSPortParam)
{
    char STDSPortParam[][32] = {"1200,n,8,1",
                                "2400,n,8,1",
                                "4800,n,8,1",
                                "9600,n,8,1",
                                "19200,n,8,1",
                                "38400,n,8,1"};
    char *pTempSPortParam = Cfg_RemoveWhiteSpace(pSPortParam);
    int i = 0;
    int iResult = 0;
    for(i = 0;i < 6;i++)
    {
        if(strcmp(pTempSPortParam,STDSPortParam[i]) == 0)
        {
            iResult = TRUE;
            break;
        }
        else
        {
            iResult = FALSE;
        }
    }

    return iResult;
}



BOOL TL1_IsNumber(IN const char *pszData)
{
	char	*pChr = (char *)pszData;

	while(*pChr)
	{
		if( !isdigit(*pChr) )
		{
			return FALSE;
		}

		pChr++;
	}

	return (TL1_CHAR_NULL == *pszData)? FALSE: TRUE;
}

BOOL TL1_IsAlpha(IN const char *pszData)
{
	char	*pChr = (char *)pszData;

	while(*pChr)
	{
		if( !isalpha(*pChr) )
		{
			return FALSE;
		}

		pChr++;
	}

	return (TL1_CHAR_NULL == *pszData)? FALSE: TRUE;
}

BOOL TL1_IsAlphaNum(IN const char *pszData)
{
	char	*pChr = (char *)pszData;

	while(*pChr)
	{
		if(!isalnum(*pChr))
		{
			return FALSE;
		}

		pChr++;
	}

	return (TL1_CHAR_NULL == *pszData)? FALSE: TRUE;
}

BOOL TL1_IsAlphaNumHyphen(IN const char *pszData)
{
	char	*pChr = (char *)pszData;

	while(*pChr)
	{
		if(!isalnum(*pChr)
			&& ('-' != *pChr))
		{
			return FALSE;
		}

		pChr++;
	}

	return (TL1_CHAR_NULL == *pszData)? FALSE: TRUE;
}

BOOL TL1_IsAlphaNumUnderscoreA(IN const char *pszData)
{
	char	*pChr = (char *)pszData;

	while(*pChr)
	{
		if(!isalnum(*pChr)
			&& ('_' != *pChr)
			&& ('/' != *pChr))
		{
			return FALSE;
		}

		pChr++;
	}

	return (TL1_CHAR_NULL == *pszData)? FALSE: TRUE;
}

BOOL TL1_IsAlphaNumUnderscore(IN const char *pszData)
{
	char	*pChr = (char *)pszData;

	while(*pChr)
	{
		if(!isalnum(*pChr)
			&& ('_' != *pChr))
		{
			return FALSE;
		}

		pChr++;
	}

	return (TL1_CHAR_NULL == *pszData)? FALSE: TRUE;
}

BOOL TL1_IsAlphaNumHyphenUnderscore(IN const char *pszData)
{
	char	*pChr = (char *)pszData;

	while(*pChr)
	{
		if(!isalnum(*pChr)
			&& ('-' != *pChr)
			&& ('_' != *pChr))
		{
			return FALSE;
		}

		pChr++;
	}

	return (TL1_CHAR_NULL == *pszData)? FALSE: TRUE;
}

BOOL TL1_IsIdentifier(IN const char *pszData)
{
	char	*pChr = (char *)pszData;

	if(isalpha(*pChr))//starts with a letter
	{
		pChr++;

		while(*pChr)//followed by any number of letters or digits
		{
			if( !isalnum(*pChr))
			{
				return FALSE;
			}
			pChr++;
		}

		return TRUE;
	}

	return FALSE;
}


BOOL TL1_IsIntInRange(IN const char *pszData, IN int iMin, IN int iMax)
{
	char	*pChr = (char *)pszData;
	int		iTemp;

	if(TL1_CHAR_NULL == *pChr)
	{
		return FALSE;
	}

	if('-' == *pChr)
	{
		pChr++;
	}

	while(*pChr)
	{
		if( !isdigit(*pChr) )
		{
			return FALSE;
		}

		pChr++;
	}

	iTemp = atoi(pszData);
	if((iTemp >= iMin) && (iTemp <= iMax))
	{
		return TRUE;
	}

	return FALSE;
}

const char* TL1_GetRespErrStr(IN int iIndex)
{
	static const char sl_szStrTable[][100] = 
	{
		"OK",//0
		TL1_RESP_ERR_ENAC,//1
		TL1_RESP_ERR_ENEQ,//2
		TL1_RESP_ERR_ENPM,//3
		TL1_RESP_ERR_ENRI,//4
		TL1_RESP_ERR_ENSI,//5
		TL1_RESP_ERR_ICNV,//6
		TL1_RESP_ERR_IDNV,//7
		TL1_RESP_ERR_IDRG,//8
		TL1_RESP_ERR_IIAC,//9
		TL1_RESP_ERR_IICT,//10
		TL1_RESP_ERR_IIFM,//11
		TL1_RESP_ERR_IIPG,//12
		TL1_RESP_ERR_IISP,//13
		TL1_RESP_ERR_IITA,//14
		TL1_RESP_ERR_INUP,//15
		TL1_RESP_ERR_IORD,//16
		TL1_RESP_ERR_PICC,//17
		TL1_RESP_ERR_PIFC,//18
		TL1_RESP_ERR_PIUC,//19
		TL1_RESP_ERR_PIUI,//20
		TL1_RESP_ERR_SAOP,//21
		TL1_RESP_ERR_SARB,//22
		TL1_RESP_ERR_SCNF,//23
		TL1_RESP_ERR_SDNR,//24
		TL1_RESP_ERR_SPNF,//25
		TL1_RESP_ERR_SROF,//26
		"",
	};

	if((iIndex >= 0) && (iIndex < TL1_E_RESP_ERR_MAX))
	{
		return sl_szStrTable[iIndex];
	}
	else
	{
		return sl_szStrTable[TL1_E_RESP_ERR_MAX];
	}
}

const char* TL1_GetAlmCode(IN int iIndex)
{
	static const char sl_szStrTable[][5] = 
	{
		"A ",/* automatic message */
		"* ",/* minor alarm */
		"**",/* major alarm */
		"*C",/* critical alarm */
		"  "
	};

	if((iIndex >= 0) && (iIndex < TL1_E_ALM_MAX))
	{
		return sl_szStrTable[iIndex];
	}
	else
	{
		return sl_szStrTable[TL1_E_ALM_MAX];
	}
}

const char* TL1_GetAidTypeStr(IN int iIndex)
{
	static const char sl_szStrTable[][5] = 
	{
		"EQPT",//0
		"ENV",//1
		""
	};

	if((iIndex >= 0) && (iIndex < TL1_E_AID_TYPE_MAX))
	{
		return sl_szStrTable[iIndex];
	}
	else
	{
		return sl_szStrTable[TL1_E_AID_TYPE_MAX];
	}
}

const char* TL1_GetNtfcncdeStr(IN int iIndex)
{
	static const char sl_szStrTable[][5] = 
	{
		"NA",//0
		"MN",//1
		"MJ",//2
		"CR",//3
		"NR",//4
		"CL",//5
		""
	};

	if((iIndex >= 0) && (iIndex < TL1_E_NTFCNCDE_MAX))
	{
		return sl_szStrTable[iIndex];
	}
	else
	{
		return sl_szStrTable[TL1_E_NTFCNCDE_MAX];
	}
}

const char* TL1_GetSrveffStr(IN int iIndex)
{
	static const char sl_szStrTable[][5] = 
	{
		"SA",//0
		"NSA",//1
		""
	};

	if((iIndex >= 0) && (iIndex < TL1_E_SRVEFF_MAX))
	{
		return sl_szStrTable[iIndex];
	}
	else
	{
		return sl_szStrTable[TL1_E_SRVEFF_MAX];
	}
}



static BOOL IsYesNo(IN char c)
{
	return  c == 'Y' || c == 'y' || c == 'N' || c == 'n';
}

BOOL TL1_CheckYesNo(IN const char *szData)
{
	if(TL1_CFG_CheckBase((char *)szData, IsYesNo))
	{
		if(1 == strlen(szData))
		{
			return TRUE;
		}
	}

	return FALSE;
}


static BOOL IsSig(IN char c)
{
	return  ((c >= '0' && c <= '9') ||
		c == '[' ||
		c == ']' ||
		c == ',');
}

BOOL TL1_CheckSig(IN const char *szData)
{
	if(TL1_CFG_CheckBase((char *)szData, IsSig))
	{
		return TRUE;
	}

	return FALSE;
}

BOOL TL1_CheckNA(IN const char *szData)
{
#define TL1_CFG_SPEC_VAL_NA						"NA"

	if(0 == stricmp(szData, TL1_CFG_SPEC_VAL_NA))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}






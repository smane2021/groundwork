/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : tl1.h
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __TL1_H_2015_04_29__
#define __TL1_H_2015_04_29__



enum TL1_ERR_CODE
{
	TL1_ERR_OK = 0,
	TL1_ERR_FAIL,
	TL1_ERR_EXEC_DONT_REPLY,
	TL1_ERR_EXEC_NO_CMD,
	TL1_ERR_EXEC_NO_AUTHORITY,
	TL1_ERR_EXEC_ARGS_INVALID,
	TL1_ERR_EXEC_USER_NAME,
	TL1_ERR_EXEC_USER_PASSWORD,
	TL1_ERR_EXEC_IS_AUTOLOGIN,
	TL1_ERR_MAX
};


/* const definitions */
/* max len of receive packet in linklayer_manager thread */
#define MAX_TL1FRAME_LEN					4096
#define TL1_CMD_IN_DATA_LEN					4096
/* command reply buf size, can include multi-packets */
#define TL1_RESP_SIZE						(4096*8)

/* used when call Queue_Get interface to get TL1 Event, Unit: ms */
#define WAIT_FOR_I_QUEUE					500	//wait for Event Input Queue
#define WAIT_FOR_O_QUEUE					100	//wait for Event Output Queue
#define WAIT_FOR_A_QUEUE					50	//wait for Event Alarm Queue

/* const definition for State Machine */
#define TL1_STATE_NUM						1

/* command number supported */
#define TL1_CMD_NUM							9

/* input event queue and output event queue size */
#define TL1_EVENT_QUEUE_SIZE				20

//MAX EquipNum
#define MAX_EQUIP_NUM						800

// compare with TL1_BASIC_ARGS.iTimeoutCounter. When ACU is on TL1_IDLE or
// SOC_OFF state, if MC has no action within READCOM_TIMEOUT*TL1_MAX_TIMEOUTCOUNT,
// ACU will disconnect the communication actively.
//#define TL1_MAX_TIMEOUTCOUNT				60

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * basic structures 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* define protocol types */
enum TL1_PROTOCOL
{
//    TL1    =6,
	EEM_TL1 = 1,
	RSOC_TL1,
	SOC_TPE_TL1,
	YDN_TL1,
	MODBUS_TL1,
	TL1,
	TL1_PROTOCOL_NUM
};


/* define transmit media types */
enum TL1_MEDIA_TYPE
{		
    TL1_MEDIA_TYPE_LEASED_LINE			= 0,//unused in TL1
    TL1_MEDIA_TYPE_MODEM,//unused in TL1
	TL1_MEDIA_TYPE_TCPIP,
	TL1_MEDIA_TYPE_TCPIPV6,
    TL1_MEDIA_TYPE_NUM
};

/* define timers */
enum TL1_TIMER
{
	TL1_ALARM_TIMER		= 1,//unused in TL1
	TL1_SESSION_TIMEOUT_TIMER,
	TL1_TIMER_NUM
};

/* define Event types */
enum TL1_EVENT_TYPE
{
	TL1_FRAME_EVENT				= 1,

	TL1_CONNECTED_EVENT,
	TL1_CONNECT_FAILED_EVENT,
	TL1_DISCONNECTED_EVENT,

	TL1_TIMEOUT_EVENT
};



/* Event definition */
struct tagTL1Event
{
	int	iEventType;			/* event type */

	/* data length of the event(only for FrameEvent) */
	int		iDataLength;	
	/* data of the event(only for FrameEvent) */
	unsigned char sData[MAX_TL1FRAME_LEN];	

	BOOL	bDummyFlag;		/* dummy flag for FrameEvent */
	BOOL	bSkipFlag;		/* skip flag for FrameEvent  */
};
typedef struct tagTL1Event TL1_EVENT;

struct tagTL1AlarmEvent
{
	int					iMsgType;//_ALARM_OCCUR_MASK, _ALARM_CLEAR_MASK
	time_t				tmStartTime;
	time_t				tmEndTime;
	int					iAlarmLevel;
	ALARM_SIG_VALUE		*pAlarmSigVal;
};
typedef struct tagTL1AlarmEvent TL1_ALARM_EVENT;



#define TL1_INIT_FRAME_EVENT(_pEvent, _iLen, _szData, _bDummyFlag, _bSkipFlag)   \
			((_pEvent)->iEventType = TL1_FRAME_EVENT, \
			(_pEvent)->iDataLength = (_iLen), \
			((_iLen) == 0 ? (_pEvent)->sData[0] = '\0' : \
				(void)memcpy((_pEvent)->sData, (_szData), (size_t)(_iLen))), \
			(_pEvent)->bDummyFlag = (_bDummyFlag), \
			(_pEvent)->bSkipFlag = (_bSkipFlag))

#define TL1_INIT_SIMPLE_FRAME_EVENT(_pEvent, _chr) \
			((_pEvent)->iEventType = TL1_FRAME_EVENT, \
			(_pEvent)->iDataLength = 1, \
			(_pEvent)->sData[0] = (_chr), \
			(_pEvent)->bDummyFlag = FALSE, \
			(_pEvent)->bSkipFlag = FALSE)

#define TL1_INIT_NONEFRAME_EVENT(_pEvent, _EventType)    \
					((_pEvent)->iEventType = (_EventType), \
					 (_pEvent)->iDataLength = 0, \
					 (_pEvent)->sData[0] = '\0', \
					 (_pEvent)->bDummyFlag = FALSE, \
					 (_pEvent)->bSkipFlag = FALSE)

/* use it after init the Event already */
#define SET_EVENT_SKIP_FLAG(_pEvent)	((_pEvent)->bSkipFlag = TRUE)


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Link-layer Manager Sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* link-layer operation mode definition */
enum TL1_OPERATION_MODE 
{
	TL1_MODE_SERVER						= 0,
	TL1_MODE_CLIENT
};


/* communication status between MC and EventQueues */
enum TL1_COMM_STATUS                  
{
	TL1_COMM_STATUS_DISCONNECT		= 0,
	TL1_COMM_STATUS_NEXT,
	TL1_COMM_STATUS_SKIP
};


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Alarm Handler sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
enum TL1_ROUTE_TYPE
{
	TL1_ROUTE_ALARM_1   = 0,
	TL1_ROUTE_ALARM_2,
	TL1_ROUTE_ALARM_3
};

struct tagTL1AlarmHandler
{
	BYTE	byCurReportRouteType;	/* route type defined by TL1_ROUTE_TYPE */

	BOOL	bAlarmAtHand;	
	BYTE	byAlarmRetryCounter;	/* alarm report retried times       */
};
typedef struct tagTL1AlarmHandler TL1_ALARM_HANDLER;



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Config Builder sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* 1. Model config info */
/* mapping info from ACU Model to Model */


// define SPECIAL VALUEs, returned by RB* command
#define	TL1_RESERVED_VALUE		1
#define TL1_ENABLE_VALUE		1
#define TL1_DISABLE_VALUE		0
/* Added end by Thomas, 2006-2-28 */


#define TL1_CFG_DESCRIPTION_MAX_LEN					(48 + 1)//end with '\0'
#define TL1_CFG_AID_GROUP_NAME_MAX_LEN				(20 + 1)
#define TL1_CFG_AID_SUB_GROUP_NAME_PREFIX_MAX_LEN	(20 + 1)
#define TL1_CFG_COND_TYPE_MAX_LEN					(20 + 1)
#define TL1_CFG_COND_DESC_MAX_LEN					(32 + 1)
#define TL1_CFG_MONITOR_TYPE_MAX_LEN				(10 + 1)

#define TL1_CFG_AID_SUB_GROUP_NAME_SUFFIX_MAX_LEN	(5)	//for example: Rectifier-1,,,Rectifier-120, the suffix number max len is 3



typedef struct tagTL1EquipInfo	
{
	char					szSubAidName[TL1_CFG_AID_GROUP_NAME_MAX_LEN];
	int						iSubAidNameSuffix;
	EQUIP_INFO				*pstEquip;
	int						iSigCount;
	SIG_BASIC_VALUE			**ppAlmSigValList;//convert to (ALARM_SIG_VALUE*) for getting iAlarmLevel to NotificationCode
	SIG_BASIC_VALUE			**ppSampSigValList;
	SIG_BASIC_VALUE			**ppSetSigValList;
	int						*piSampSigInfoTypeList;
	int						*piSetSigInfoTypeList;
} TL1_EQUIP_INFO;


typedef struct tagTL1ACUSigID
{
	int						iRelevantEquipListIndex;
	int						iSigTypeID;
	int						iSigID;
}TL1_ACU_SIG;

struct tagTL1MapEntriesInfo	/* sig mapping */
{
	int						iIndex;
	char					szDescription[TL1_CFG_DESCRIPTION_MAX_LEN];
	int						iTL1SigID;
	BOOL					bSigActive;
	char					szCondType[TL1_CFG_COND_TYPE_MAX_LEN];
	char					szCondDesc[TL1_CFG_COND_DESC_MAX_LEN];
	int						iServiceAffectCode;
	int						iMonitorFormat;
	char					szMonitorType[TL1_CFG_MONITOR_TYPE_MAX_LEN];
	TL1_ACU_SIG				stAlarmSig;
	TL1_ACU_SIG				stSampleSig;
	TL1_ACU_SIG				stSettingSig;

	//just for web pages writing
	int						iEquipTypeID;
	int						iNotificationCode;
};
typedef struct tagTL1MapEntriesInfo TL1_MAPENTRIES_INFO;


struct tagTL1AIDGroupInfo	/* AID Group Info */
{
	int						iIndex;
	char					szDescription[TL1_CFG_DESCRIPTION_MAX_LEN];
	char					szAidGroupName[TL1_CFG_AID_GROUP_NAME_MAX_LEN];
	char					szAidSubGroupNamePrefix[TL1_CFG_AID_SUB_GROUP_NAME_PREFIX_MAX_LEN];
	int						iAidGroupType;//0,EQPT; 1, ENV
	int						iMapentriesID;

	int						iMapEntriesNum;				/* the number of current model type */ 
	TL1_MAPENTRIES_INFO		*pMapEntriesInfo;			/* sig mapping info */

	BOOL					bAidSubGroupNameExist;
	int						iAidSubGroupNameSuffixMaxLen;//max len is TL1_CFG_AID_SUB_GROUP_NAME_SUFFIX_MAX_LEN==5

	//run info
#define TL1_AID_GROUP_OUTPUT_EQUIP_MAX		500	//just for web page display

	//just for web page reading
	int						iTL1EquipCount;//how many equipments in one AID Group Info
	TL1_EQUIP_INFO			*pstTL1EquipList[TL1_AID_GROUP_OUTPUT_EQUIP_MAX];
};
typedef struct tagTL1AIDGroupInfo TL1_AID_GROUP_INFO;


struct tagTL1TypeMapInfo				  /* type mapping */
{
	int						iIndex;
	//char					szDescription[TL1_CFG_DESCRIPTION_MAX_LEN];
	int						iAidGroupID;
	int						iAidSubGroupSuffixStart;
	int						iEquipTypeID;
	int						iEquipIDStart;
	int						iEquipIDEnd;

	TL1_AID_GROUP_INFO		*pstAidGroupInfo;//point to the same Aid Group ID item in [AID_GROUP_INFO]

};
typedef struct tagTL1TypeMapInfo TL1_TYPE_MAP_INFO;


struct tagTL1ModelConfigInfo          /* model config file info */
{
	int						iAIDGroupNum;
	TL1_AID_GROUP_INFO		*pstAIDGroupInfo;	  /* AID Group info */

	int						iTypeMapNum;
	TL1_TYPE_MAP_INFO		*pTypeMapInfo;	  /* type mapping info */
};
typedef struct tagTL1ModelConfigInfo TL1MODEL_CONFIG_INFO;


/* 2. model info */


struct tagTL1BlockInfo	
{
	BOOL					bIsValid;
	char					*pszMainAidName;
	int						iTL1EquipNum;
	TL1_EQUIP_INFO			*pstTL1EquipList;

	TL1_TYPE_MAP_INFO		*pTypeInfo;	 /* reference of relevant type info */
};
typedef struct tagTL1BlockInfo TL1_BLOCK_INFO;

struct tagTL1ModelInfo	 
{
	int						iBlocksNum;	    /* the number of blocks */
	TL1_BLOCK_INFO			*pBlockInfo;	 
};
typedef struct tagTL1ModelInfo TL1_MODEL_INFO;



/* 3.TL1 common config info */
/* const definitions */
#define COMM_PORT_PARAM_LEN					64

/* define the number of settable alarm phone number/network address */
#define TL1_ALARM_REPORT_NUM				3

/* define the number of settable callback phone number */
//#define TL1_CALLBACK_NUM					1

/* define the number of security ip address */
//#define TL1_SECURITY_IP_NUM					2

/* define max phone number length */
#define PHONENUMBER_LEN						20


#define TL1_MAX_LEN_AUTO_LOGIN_USER			(32 + 1)//last char is \0
#define TL1_MAX_LEN_SYSTEM_IDENTIFIER		(32 + 1)//last char is \0

#define TL1_MIN_VAL_SESSION_TIMEOUT			0
#define TL1_MAX_VAL_SESSION_TIMEOUT			1440
#define TL1_MIN_VAL_PORT_NUMBER				1024
#define TL1_MAX_VAL_PORT_NUMBER				65534

/////////////////////////   just for interface about AIDGroupInfo   ///////////
struct tagTL1AIDGroupInfoR	/* AID Group Info */
{
	int						iAIDGroupNum;
	TL1_AID_GROUP_INFO		*pstAIDGroupInfo;/* AID Group info */
};
typedef struct tagTL1AIDGroupInfoR TL1_AID_GROUP_INFO_R;

struct tagTL1AIDGroupInfoW	/* AID Group Info */
{
	//be used in TL1_AID_GROUP_INFO_W
#define TL1_CFG_MODIFY_TYPE_ONLY_AID_GROUP					0
#define TL1_CFG_MODIFY_TYPE_ONLY_MAPENTRIES					1
#define TL1_CFG_MODIFY_TYPE_AID_GROUP_AND_MAPENTRIES		2
	//iModifyType value list:
	//0, just modify AID Group info;
	//1, just modify MAPENTRIES_INFO in AID Group info;
	//2, modify both  AID Group info and MAPENTRIES_INFO
	int						iModifyType;
	TL1_AID_GROUP_INFO		stAIDGroupInfo;/* AID Group info */
};
typedef struct tagTL1AIDGroupInfoW TL1_AID_GROUP_INFO_W;
///////////////////////////////////////////////////////////////////////


/* common config info */
struct tagTL1CommonConfig
{
	int						iModuleSwitch;//0, Disable;1, Enable
	unsigned int			byADR;
	unsigned int			iProtocolType;	/* see enum TL1_PROTOCOL definition */
	int						iMediaType;	    /* see enum TL1_MEDIA_TYPE definition */
	char					szCommPortParam[COMM_PORT_PARAM_LEN];

	int						iPortActivation;//0, Disable;1, Enable
	int						iPortKeepAlive;//0, Disable;1, Enable
	int						iSessionTimeout;//in minutes, 0 is disabled
	char					szAutoLoginUser[TL1_MAX_LEN_AUTO_LOGIN_USER];
	char					szSystemIdentifier[TL1_MAX_LEN_SYSTEM_IDENTIFIER];
	int						iAidDelimeter;//0, Disable;1, Enable
	
	BOOL					bReportInUse;

	/* max attemps to report alarms */
	int						iMaxAttempts;//unused in TL1
	/* elaps time between each attemps (unit: second) */
	int						iAttemptElapse;	//unused in TL1

	/* phone number for alarm report*/
	char					szAlarmReportPhoneNumber[TL1_ALARM_REPORT_NUM][PHONENUMBER_LEN];//unused in TL1

	char					cModifyUser[40];//added by ht,2006.12.13, no use in common config file,just for system log.

	//just be used for modifying AID GROUP
	TL1_AID_GROUP_INFO_R	stAidGroupInfoForRead;
	TL1_AID_GROUP_INFO_W	stAidGroupInfoForWrite;
};
typedef struct tagTL1CommonConfig TL1_COMMON_CONFIG;


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * structures for Frame Analysis sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
enum tagTL1FrameType
{
	TL1_E_FRAME_OK			= 0,/* correct frame start */
	TL1_E_ERR_FRAME			= 0xE000, /* incorrect frame start */
	TL1_E_ERR_FRAME_IGNORE,//give no response
	TL1_E_ERR_FRAME_JUST_PROMPT,//give a prompt char '>'
	TL1_E_ERR_FRAME_PARSE,
	TL1_E_ERR_FRAME_CHECK,
	TL1_E_ERR_MAX /* incorrect frame end */
};
typedef enum tagTL1FrameType TL1_FRAME_TYPE;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Command Handler sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* command execution function prototype definition */
typedef struct tagTL1BasicArgs TL1_BASIC_ARGS;
typedef int (*TL1_CMD_EXECUTE_FUN) (TL1_BASIC_ARGS *pThis,
								 unsigned char *szInData,
								 unsigned char *szOutData);

struct tagTL1CmdHandler
{
	const char				*szCmdType;//command name
	BOOL					bIsWriteCommand;//unused in TL1
	BOOL					bIsSpecialCommand;//just can be called by local app
	//  Security access level of command, defined in enum AUTHORITY_LEVEL 
	int						iLevel;
	TL1_CMD_EXECUTE_FUN		funExecute;
};
typedef struct tagTL1CmdHandler TL1_CMD_HANDLER;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for State Machine sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* states definition */
/* states of State Machine */
enum TL1_STATES
{
	TL1_STATE_MACHINE_QUIT  = -1,
	TL1_IDLE
};

/* on-state function prototype */
typedef int (*TL1_ON_STATE_PROC) (TL1_BASIC_ARGS *pThis);


struct tagTL1StateInfo
{
	int						iStateID;	                /* defined by TL1_STATES enum */
	TL1_ON_STATE_PROC		fnStateProc;	
}; 
typedef struct tagTL1StateInfo TL1_STATE_INFO;


///* alarm info structure */
//struct tagTL1AlarmInfo
//{
//	char					szStartTime[13];	/* alarm start time (YYMMDDHHMMSS) */
//	char					szEndTime[13];		/* alarm end time (YYMMDDHHMMSS)   */
//
//	int						iGroupID;	        /* Group ID */
//	int						iSubGroupID;	    /* Subgroup ID */
//	int						iUnitID;	        /* Unit ID */
//	char					cSection;			/* I for DI signal, O for DO signal */
//	int						iCno;				/* sig no of YDN model */
//
//	/* 	alarm level of YDN Model, currently support :A1=0, A2=1, O1=3;
//	 *  they are mapped to CA=3, MA=2, OA=1 in ACU model. */
//	int						iAlarmLevel;	
//};
//typedef struct tagTL1AlarmInfo TL1_ALARM_INFO;


#define	TL1_MAX_FIELD_LEN					128
#define	TL1_MAX_FIELD_COUNT_L1				15
#define	TL1_MAX_FIELD_COUNT_L2				15
#define	TL1_MAX_FIELD_COUNT_L3				1

typedef struct tagTL1CmdFieldCfg
{
	BOOL					bNeedExist;
	int						iLimitMinLen;
	int						iLimitMaxLen;
	char					cSubFieldSplitChar;
	int						iErrCode;

	BOOL					(* pfnCheckBasic)(const char *) ;

} TL1_CMD_FIELD_CFG;


typedef struct tagTL1CmdField
{
	//configuration options
	TL1_CMD_FIELD_CFG		stCfg;

	//run info
	BOOL					bIsExist;
	int						iLen;
	char					szData[TL1_MAX_FIELD_LEN];

} TL1_CMD_FIELD;


typedef struct tagTL1Command
{
	int						iErrCode;

	TL1_CMD_FIELD			*pstCmdCode;
	TL1_CMD_FIELD			*pstVerb;
	TL1_CMD_FIELD			*pstModifier1;
	TL1_CMD_FIELD			*pstModifier2;
	TL1_CMD_FIELD			*pstTid;
	TL1_CMD_FIELD			*pstAid;
	TL1_CMD_FIELD			*pstMainAid;
	TL1_CMD_FIELD			*pstSubAid;
	TL1_CMD_FIELD			*pstCtag;
	TL1_CMD_FIELD			*pstGeneralBlk;
	TL1_CMD_FIELD			*pstDataBlk;
	TL1_CMD_FIELD			*pstSeventhBlk;

	char					szInData[TL1_CMD_IN_DATA_LEN];

	TL1_CMD_FIELD			stFieldTabL1[TL1_MAX_FIELD_COUNT_L1];
	TL1_CMD_FIELD			stFieldTabL2[TL1_MAX_FIELD_COUNT_L1][TL1_MAX_FIELD_COUNT_L2];
	TL1_CMD_FIELD			stFieldTabL3[TL1_MAX_FIELD_COUNT_L1][TL1_MAX_FIELD_COUNT_L2][TL1_MAX_FIELD_COUNT_L3];

} TL1_COMMAND;


#define TL1_MAX_LEN_SESSION_NAME					(32 + 1)

typedef struct tagTL1User
{
	USER_INFO_STRU			stBaseInfo;
} TL1_USER;


typedef struct tagTL1Session
{
	char					szSessionName[TL1_MAX_LEN_SESSION_NAME];//unused
	int						iSessionID;//unused

	BOOL					bIsValid;//whether exist a valid session connection
	BOOL					bIsAutoLogin;//whether exist a valid auto login
	BOOL					bIsLogin;//whether exist a valid user
	BOOL					bNeedReportAllAlarm;

	unsigned int			uiInPacketCount;//count of receiving data from remote client 
	unsigned int			uiTimerTimeoutCount;//timeout count of session timeout timer

	TL1_USER				stUser;// login user info  , Structure containing TL1 user

	TL1_COMMAND				stCommand;//current command

} TL1_SESSION;



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * globals structures 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* basic arg structure */
struct tagTL1BasicArgs
{
	/* event queues */
	HANDLE						hEventInputQueue;
	HANDLE						hEventOutputQueue;
	HANDLE						hEventAlarmQueue;

	/* data buffers */
	int							iTimeoutCounter; //used by TL1_IDEL and TL1_OFF state//unused in TL1
	int							iResendCounter;  /* used when timeout *///unused in TL1
	unsigned char				sReceivedBuff[MAX_TL1FRAME_LEN]; /* used for CHAE-based frame recognize */
	int							iCurPos;                        //for Received Buff
	int							iLen;						    //for Received Buff
	unsigned char				szCmdRespBuff[TL1_RESP_SIZE];	 // command reply data buf 	
	TL1_ALARM_HANDLER			alarmHandler;

	/* link-layer current operation mode */
	int							iOperationMode; /* use TL1_OPERATION_MODE const */

	/* communication handle */
	HANDLE						hComm;	

	//The value of the ATAG must start at ��1�� for the first message. 
	//With each succeeding message sent the value must be incremented 
	//by one from the value in the previous message sent. When the value 
	//reaches the maximum value of 65535, the next message sent will 
	//use a value of ��1�� and restart the sequence.
	int							iAtag;
	TL1_SESSION					stSession;

	/* hThreadID[0]: Service thread id;
	 * hThreadID[1]: linklayer_manager thread id*/
	HANDLE						hThreadID[2];

	/* flags */
	int							iMachineState;    /* state machine current state */ 
	int							*pOutQuitCmd;	  /* service exit command, from Main Module */
	int							iInnerQuitCmd;               /* use SERVICE_EXIT_CODE const     */
	int 						iLinkLayerThreadExitCmd;	 /* use THREAD_EXIT_CODE const      */
	BOOL						bServerModeNeedExit;	     /* server operation mode exit flag */
	BOOL						bProtocolChangedFlag;	     /* protocol type changed flag      */
	BOOL						bCommRunning;	             /* communication busy flag         */
	BOOL						bSecureLine;	             /* security connection flag        *///unused in TL1
	BOOL						bFrameDone;                  /* used by frame split function    *///unused in TL1
	BOOL						bADRChangedFlag;	     /* protocol type changed flag      *///unused in TL1
};



/* used to wait for Muxtex */
#define TIME_WAIT_MUTEX_COMCFG  1000
#define TIME_WAIT_MUTEX_SWITCH  3000

struct tagTL1Globals
{
	TL1MODEL_CONFIG_INFO		TL1ModelConfig;
	TL1_MODEL_INFO				TL1ModelInfo;
	TL1_COMMON_CONFIG			CommonConfig;

	TL1_CMD_HANDLER				CmdHandlers[TL1_CMD_NUM];
	TL1_STATE_INFO				staStates[TL1_STATE_NUM];

	EQUIP_INFO					*pEquipInfo;
	int							iEquipNum;
	int							iEquipIDOrder[MAX_EQUIP_NUM][2];//unused in TL1
	//HANDLE  hMutexCommonCfg;  //for TL1 common config file
};
typedef struct tagTL1Globals TL1_GLOBALS;

/* global variable declare */
extern TL1_GLOBALS  g_TL1Globals;

/* Simple Events declare */
extern TL1_EVENT g_TL1DummyEvent, g_TL1TimeoutEvent;
extern TL1_EVENT g_TL1DiscEvent, g_TL1ConnFailEvent, g_TL1ConnEvent;


/* interface */
/* Config Builder sub-module */
extern int TL1_InitConfig(void);
/* linklayer_manager sub-module */
extern int TL1_LinkLayerManager(IN TL1_BASIC_ARGS *pThis);

/* Alarm Handler sub-module */
//extern void TL1_RegisterAlarm(TL1_BASIC_ARGS *pThis, BOOL bDelayedAlarm);
//extern void TL1_SendAlarm(TL1_BASIC_ARGS *pThis);
//extern void TL1_AlarmReported (TL1_BASIC_ARGS *pThis);

//extern void TL1_RegisterCallback(TL1_BASIC_ARGS *pThis, BOOL bDealyedCallback);
//extern void TL1_SendCallback(TL1_BASIC_ARGS *pThis);
//extern void TL1_CallbackReported(TL1_BASIC_ARGS *pThis);

//extern void TL1_ClientReportFailed(TL1_BASIC_ARGS *pThis, 
//							BOOL bConnetedFailed);

/* Frame Analyse sub-module */
extern TL1_FRAME_TYPE TL1_AnalyseFrame(IN const unsigned char *pFrame,
										IN int iLen,
										IN OUT TL1_BASIC_ARGS *pThis);
/* Command Handler sub-module */
extern void TL1_InitCmdHander(void);

extern int TL1_DecodeAndPerform(IN OUT TL1_BASIC_ARGS *pThis,
								IN unsigned char * szCmdData,
								IN BOOL bIsNormalMode);
		
/* State Machine sub-module */
/* for state machine */
extern int TL1_OnIdle(IN OUT TL1_BASIC_ARGS *pThis);


/* Utilities used by Module */
extern BYTE TL1_AsciiHexToChar(IN const unsigned char *pStr);
extern unsigned int TL1_CharToInt(IN const unsigned char *pStr, IN int iCNum);
extern BOOL TL1_IsStrAsciiHex(IN const unsigned char *pStr, IN int iLen);
extern BOOL TL1_CheckPhoneNumber(IN char *szPhoneNumber);
extern BOOL TL1_CheckIPAddress(IN const char *szIPAddress);
extern BOOL TL1_CheckBautrateCfg(IN const char *szBautrateCfg);
extern BOOL TL1_CheckModemCfg(IN const char *szModemCfg);
extern BOOL TL1_CheckSPortParam(IN char *pSPortParam);
extern char *TL1_GetUnprintableChr(IN unsigned char chr);
extern void TL1_IntToAscii(IN DWORD dValue, OUT unsigned char *cStr);
extern void TL1CheckSum( IN const unsigned char   *pFrame ,	// ҪУ��Ĵ��봮
						IN int		nLen ,		// ��ҪУ��Ĵ�����
						OUT unsigned char *pChkBytes);	// ��У��Ľ��д��ô�

extern void TL1_PrintEvent(IN TL1_EVENT *pEvent);
extern void TL1_PrintState(IN int iState);
extern void TL1_PrintCommonCfg(IN TL1_COMMON_CONFIG *pConfig);
extern void TL1_PrintfStr(IN int nLen, IN char *pStr);

extern void TL1_ClearEventQueue(IN HANDLE hq, IN BOOL bDestroy);
extern void TL1_ClearAlarmEventQueue(IN HANDLE hq, IN BOOL bDestroy);

extern int TL1_ModifyCommonCfg(IN BOOL bServiceIsRunning,
						IN BOOL bByForce,
						IN OUT TL1_BASIC_ARGS *pThis,
						IN int iItemID,
						IN TL1_COMMON_CONFIG *pUserCfg);



extern BOOL TL1_GetEquipIDOrder(IN int iEquipTypeID,IN OUT int* iEquipNum);
extern BOOL TL1_GetEquiplistIDOrder(IN int iEquipID,IN OUT int* iEquipNum);


						
/* safe delete event */
#define DELETE_TL1_EVENT(_pEvent) \
	do {                          \
	if ((_pEvent)->iEventType == TL1_FRAME_EVENT && !(_pEvent)->bDummyFlag) \
	{     \
		DELETE(_pEvent);  \
	}   \
	} while(0)

/* log utilities */
#define TL1_LOG_TEXT_LEN	256

/* error log */
#define LOG_TL1_E(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_ERROR, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: ERROR: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* extended error log(logged with error code) */
#define LOG_TL1_E_EX(_task, _szLogText, _errCode)  \
	(AppLogOut((_task), APP_LOG_ERROR, "%s(Error Code: %X).\n", \
	(_szLogText), (_errCode)),  \
	TRACE("[%s:%d]--%s: ERROR: %s(Error Code: %X).\n", __FILE__, __LINE__, \
	__FUNCTION__, (_szLogText), (_errCode)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* warning log */
#define LOG_TL1_W(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_WARNING, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: WARNING: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* info log */
#define LOG_TL1_I(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_INFO, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: MESSAGE: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime())) 


/* debug utilities */
#ifdef _DEBUG
	//#define _DEBUG_TL1_SERVICE       //for TL1 Module debug 
	//#define _TEST_TL1_SERVICE        //for TL1 Module test version

#endif

//#define _DEBUG_TL1_SERVICE
#ifdef _DEBUG_TL1_SERVICE
	#define	_DEBUG_TL1

	#define _SHOW_TL1_EVENT_INFO
	//#define _SHOW_TL1_CONFIG_INFO

    /* show the current state name of the State Machine */
	#define _SHOW_TL1_CUR_STATE

	#define _DEBUG_TL1_LINKLAYER
	#define _DEBUG_TL1_CMD_HANDLER
	#define _DEBUG_TL1_REPORT
	//#define _TEST_TL1_MODIFYCONFIG
#endif //_DEBUG_TL1_SERVICE

//#define _DEBUG_TL1_SERVICE
#ifdef _DEBUG_TL1_SERVICE
	#define TRACE_TL1_TIPS(_tipInfo)   \
		(TRACE("[%s:%d]--%s: Message: %s.\n", __FILE__, __LINE__, \
		 __FUNCTION__, (_tipInfo)), \
		 TRACE("Time is: %.3f\n", GetCurrentTime()))

	#define TL1_GO_HERE  TRACE("[%s:%d]--%s: Go here!.\n", __FILE__, \
					__LINE__, __FUNCTION__)
#else
	#define TL1_GO_HERE  1 ? (void)0 : (void)0
	#define TRACE_TL1_TIPS(_tipInfo)   1 ? (void)0 : (void)0
#endif //_DEBUG_TL1_SERVICE


/* switch with Maintenance service */
#define COM_SHARE_SERVICE_SWITCH

#ifdef COM_SHARE_SERVICE_SWITCH
#define TL1_IS_SHARE_COM(_iMediaType)  \
	((_iMediaType) != TL1_MEDIA_TYPE_TCPIP ? TRUE : FALSE)

extern BOOL TL1_IsMtnFrame(IN const unsigned char *pFrame, IN int iLen);
extern void TL1_BuildMtnResponseFrame(OUT unsigned char *pFrameData, OUT int *piFrameDataLen);

#endif //COM_SHARE_SERVICE_SWITCH

//telnet command code
#define TELNET_Binary_Transmission						 0
#define TELNET_Echo										 1
#define TELNET_Reconnection								 2
#define TELNET_Suppress_Go_Ahead						 3
#define TELNET_Approx_Message_Size_Negotiation			 4
#define TELNET_Status									 5
#define TELNET_Timing_Mark								 6
#define TELNET_Remote_Controlled_Trans_and_Echo			 7
#define TELNET_Output_Line_Width						 8
#define TELNET_Output_Page_Size							 9
#define TELNET_Output_Carriage_Return_Disposition		10
#define TELNET_Output_Horizontal_Tab_Stops				11
#define TELNET_Output_Horizontal_Tab_Disposition		12
#define TELNET_Output_Formfeed_Disposition				13
#define TELNET_Output_Vertical_Tabstops					14
#define TELNET_Output_Vertical_Tab_Disposition			15
#define TELNET_Output_Linefeed_Disposition				16
#define TELNET_Extended_ASCII							17
#define TELNET_Logout									18
#define TELNET_Byte_Macro								19
#define TELNET_Data_Entry_Terminal						20
#define TELNET_SUPDUP									22
#define TELNET_SUPDUP_Output							22
#define TELNET_Send_Location							23
#define TELNET_Terminal_Type							24
#define TELNET_End_of_Record							25
#define TELNET_TACACS_User_Identification				26
#define TELNET_Output_Marking							27
#define TELNET_Terminal_Location_Number					28
#define TELNET_Telnet_3270_Regime						29
#define TELNET_X_3_PAD									30
#define TELNET_Negotiate_About_Window_Size				31
#define TELNET_Terminal_Speed							32
#define TELNET_Remote_Flow_Control						33
#define TELNET_Linemode									34
#define TELNET_X_Display_Location						35
#define TELNET_Environment_Option						36
#define TELNET_Authentication_Option					37
#define TELNET_Encryption_Option						38
#define TELNET_New_Environment_Option					39
#define TELNET_TN3270E									40


#define TELNET_SUBNEGOTIATION							0xFA
#define TELNET_END_SUBNEGOTIATION						0xF0
#define TELNET_BREAK									0xF3
#define TELNET_INTERUPT									0xF4
#define TELNET_ABORT_OUTPUT								0xF5
#define TELNET_ARE_YOU_THERE							0xF6
#define TELNET_ERASE_CHARACTER							0xF7
#define TELNET_ERASE_LINE								0xF8

#define TELNET_WILL										0xFB
#define TELNET_WONT										0xFC
#define TELNET_DO										0xFD
#define TELNET_DONT										0xFE
#define TELNET_IAC										0xFF



//TL1 response status string
#define TL1_RESP_STAT_COMPLD				"COMPLD"	/* Command completed */
#define TL1_RESP_STAT_RTRV					"RTRV"		/* Data requested are retrieved */
#define TL1_RESP_STAT_DELAY					"DELAY"		/* Delayed activation of execution */
#define TL1_RESP_STAT_PRTL					"PRTL"		/* Partially executed */
#define TL1_RESP_STAT_DENY					"DENY"		/* Execution denied, see error code */

//TL1 response error code string
/* Equipage, Not equipped with Alarm Cutoff */
#define TL1_RESP_ERR_ENAC					"\r\n   ENAC\r\n   /* Not equipped with Alarm Cutoff */"
/* Equipage, Not EQuipped */
#define TL1_RESP_ERR_ENEQ					"\r\n   ENEQ\r\n   /* Not equipped with specified equipment */"
/* Equipage, Not equipped for Performance Monitoring */
#define TL1_RESP_ERR_ENPM					"\r\n   ENPM\r\n   /* Not equipped for Performance Monitoring */"
/* Equipage, Not equipped for Retrieving specified Information */
#define TL1_RESP_ERR_ENRI					"\r\n   ENRI\r\n   /* Not equipped for retrieving specified information */"
/* Equipage, Not equipped for Setting specified Information */
#define TL1_RESP_ERR_ENSI					"\r\n   ENSI\r\n   /* Not equipped for retrieving specified information */"
/* Input, Command Not Valid */
#define TL1_RESP_ERR_ICNV					"\r\n   ICNV\r\n   /* Command not valid */"
/* Input, Data Not Valid */
#define TL1_RESP_ERR_IDNV					"\r\n   IDNV\r\n   /* Input data not valid */"
/* Input, Data RanGe */
#define TL1_RESP_ERR_IDRG					"\r\n   IDRG\r\n   /* Input data out of range */"
/* Input, Invalid Access Identifier */
#define TL1_RESP_ERR_IIAC					"\r\n   IIAC\r\n   /* Invalid Access Identifier */"
/* Input, Invalid Correlation Tag */
#define TL1_RESP_ERR_IICT					"\r\n   IICT\r\n   /* Invalid Correlation Tag */"
/* Input, Invalid Data Format */
#define TL1_RESP_ERR_IIFM					"\r\n   IIFM\r\n   /* Invalid Data Format */"
/* Input, Invalid Parameter Grouping */
#define TL1_RESP_ERR_IIPG					"\r\n   IIPG\r\n   /* Invalid Parameter Group */"
/* Input, Invalid Syntax or Punctuation */
#define TL1_RESP_ERR_IISP					"\r\n   IISP\r\n   /* Invalid syntax or punctuation */"
/* Input, Invalid TArget identifier */
#define TL1_RESP_ERR_IITA					"\r\n   IITA\r\n   /* Invalid Target Identifier */"
/* Input, Non-null Unimplemented Parameter */
#define TL1_RESP_ERR_INUP					"\r\n   INUP\r\n   /* Non-null unimplemented parameter */"
/* Input, Out of Range Data */
#define TL1_RESP_ERR_IORD					"\r\n   IORD\r\n   /* Input data out of range */"
/* Privilege, Illegal Command Code */
#define TL1_RESP_ERR_PICC					"\r\n   PICC\r\n   /* Illegal command code */"
/* Privilege, Illegal Field Code */
#define TL1_RESP_ERR_PIFC					"\r\n   PIFC\r\n   /* Command can not be executed on specified field */"
/* Privilege, Illegal User Code*/
#define TL1_RESP_ERR_PIUC					"\r\n   PIUC\r\n   /* Invalid user identifier or user access code */"
/* Privilege, Illegal User Identity */
#define TL1_RESP_ERR_PIUI					"\r\n   PIUI\r\n   /* Illegal user identity */"
/* Status, Already OPerated */
#define TL1_RESP_ERR_SAOP					"\r\n   SAOP\r\n   /* Status, equipment already operated */"
/* Status, All Resources Busy */
#define TL1_RESP_ERR_SARB					"\r\n   SARB\r\n   /* Cannot process command, resources busy */"
/* Status, Command Not Found */
#define TL1_RESP_ERR_SCNF					"\r\n   SCNF\r\n   /* Command not found */"
/* Status, Data Not Ready */
#define TL1_RESP_ERR_SDNR					"\r\n   SDNR\r\n   /* Status, data not available/ready */"
/* Status, Process Not Found */
#define TL1_RESP_ERR_SPNF					"\r\n   SPNF\r\n   /* Process not found */"
/* Status, Requested Operation Failed */
#define TL1_RESP_ERR_SROF					"\r\n   SROF\r\n   /* Requested operation failed */"

//server config
#define TL1_SERVER_MAX_CLIENTS							1
#define TL1_PORT_KEEP_ALIVE_COUNT						4	//the number of keepalive probe packets
#define TL1_PORT_KEEP_ALIVE_INTERVAL					5	//the keepalive interval
#define TL1_PORT_KEEP_ALIVE_IDLE						60	//the keepalive idle time

//command config of "RTRV_ALM"
//each alarm is about 160 bytes, need buffer 200*160 = 32 000 < TL1_RESP_SIZE=4096*8
#define TL1_ALM_MAX_NUM									200
//each alarm is about 160 bytes, need buffer 25*160 = 4 000 < 4096(a packet size)
#define TL1_ALM_SPLIT_NUM								25
//command config of "session timeout"
#define TL1_SESSION_TIMEOUT_TIMER_INTERVAL				15//seconds



//special char and string
#define TL1_CHAR_NULL									('\0')
#define TL1_INFO_PROMPT									"\r\n<"
#define TL1_RESP_PACKET_SPLITTER						('\255')

//split char of command parsing
#define TL1_CMD_SPLITTER_L0								(';')
#define TL1_CMD_SPLITTER_L1								(':')
#define TL1_CMD_SPLITTER_CMD_CODE						('-')
#define TL1_CMD_SPLITTER_AID							('-')
#define TL1_CMD_SPLITTER_DATA_BLK						(',')

//command field len limit
#define TL1_FIELD_LIMIT_CMD_CODE						20
#define TL1_FIELD_LIMIT_TID								20
#define TL1_FIELD_LIMIT_AID								20
#define TL1_FIELD_LIMIT_CTAG							6

//index define for referencing TL1_CMD_FIELD in TL1_COMMAND
#define TL1_FIELD_ID_TAB_L1								0
#define TL1_FIELD_ID_TAB_L2								1
#define TL1_FIELD_ID_TAB_L3								2

#define TL1_FIELD_ID_ITEM_CMD_CODE						0
#define TL1_FIELD_ID_ITEM_TID							1
#define TL1_FIELD_ID_ITEM_AID							2
#define TL1_FIELD_ID_ITEM_CTAG							3
#define TL1_FIELD_ID_ITEM_GENERAL_BLK					4
#define TL1_FIELD_ID_ITEM_DATA_BLK						5
#define TL1_FIELD_ID_ITEM_SEVENTH_BLK					6


enum TL1_E_TYPE_RESP_ERR
{
	TL1_E_RESP_ERR_OK = 0,
	TL1_E_RESP_ERR_ENAC,
	TL1_E_RESP_ERR_ENEQ,
	TL1_E_RESP_ERR_ENPM,
	TL1_E_RESP_ERR_ENRI,
	TL1_E_RESP_ERR_ENSI,
	TL1_E_RESP_ERR_ICNV,
	TL1_E_RESP_ERR_IDNV,
	TL1_E_RESP_ERR_IDRG,
	TL1_E_RESP_ERR_IIAC,
	TL1_E_RESP_ERR_IICT,
	TL1_E_RESP_ERR_IIFM,
	TL1_E_RESP_ERR_IIPG,
	TL1_E_RESP_ERR_IISP,
	TL1_E_RESP_ERR_IITA,
	TL1_E_RESP_ERR_INUP,
	TL1_E_RESP_ERR_IORD,
	TL1_E_RESP_ERR_PICC,
	TL1_E_RESP_ERR_PIFC,
	TL1_E_RESP_ERR_PIUC,
	TL1_E_RESP_ERR_PIUI,
	TL1_E_RESP_ERR_SAOP,
	TL1_E_RESP_ERR_SARB,
	TL1_E_RESP_ERR_SCNF,
	TL1_E_RESP_ERR_SDNR,
	TL1_E_RESP_ERR_SPNF,
	TL1_E_RESP_ERR_SROF,
	TL1_E_RESP_ERR_MAX
};

enum TL1_E_TYPE_ALM_CODE
{
	TL1_E_ALM_AUTO_MSG = 0,
	TL1_E_ALM_MINOR,
	TL1_E_ALM_MAJOR,
	TL1_E_ALM_CRITICAL,
	TL1_E_ALM_MAX
};

enum TL1_E_TYPE_SRVEFF
{
	TL1_E_SRVEFF_SA = 0,
	TL1_E_SRVEFF_NSA,
	TL1_E_SRVEFF_MAX
};

enum TL1_E_TYPE_AID_TYPE
{
	TL1_E_AID_TYPE_EQPT = 0,
	TL1_E_AID_TYPE_ENV,
	TL1_E_AID_TYPE_MAX
};

enum TL1_E_TYPE_NTFCNCDE
{
	TL1_E_NTFCNCDE_NA = 0,
	TL1_E_NTFCNCDE_MN,
	TL1_E_NTFCNCDE_MJ,
	TL1_E_NTFCNCDE_CR,
	TL1_E_NTFCNCDE_NR,
	TL1_E_NTFCNCDE_CL,
	TL1_E_NTFCNCDE_MAX
};

enum TL1_E_TYPE_MONITOR_TYPE
{
	TL1_E_MONITOR_FORMAT_DECIMAL = 0,
	TL1_E_MONITOR_FORMAT_INTEGER,
	TL1_E_MONITOR_FORMAT_MAX
};


#define TL1_C_DAY								(24*60*60)//seconds
#define TL1_C_MIN								(60)//seconds
#define TL1_TIME_CHN_FMT						"%y-%m-%d %H:%M:%S"
#define TL1_TIME_CHN_FMT_DATE_1					"%y-%m-%d"
#define TL1_TIME_CHN_FMT_DATE_2					"%m-%d"
#define TL1_TIME_CHN_FMT_TIME					"%H-%M-%S"


//LCD setting signals for TL1 common parameters
#define TL1_CFG_SIG_EQUIP_ID						1
#define TL1_CFG_SIG_SIGTYPE_ID						2
#define TL1_CFG_SIG_SIGID_START						TL1_CFG_SIG_SIGID_PORT_ACTIVATION
#define TL1_CFG_SIG_SIGID_END						TL1_CFG_SIG_SIGID_SESSION_TIMEOUT
#define TL1_CFG_SIG_SIGID_PORT_ACTIVATION			480
#define TL1_CFG_SIG_SIGID_MEDIA_TYPE				481
#define TL1_CFG_SIG_SIGID_PORT_NUMBER				482
#define TL1_CFG_SIG_SIGID_KEEP_ALIVE				483
#define TL1_CFG_SIG_SIGID_SESSION_TIMEOUT			484
#define TL1_CFG_SIG_SIGID_AID_DELIMETER				479


//interface declare
extern BOOL TL1_IsIntInRange(IN const char *pszData, IN int iMin, IN int iMax);
extern BOOL TL1_IsIdentifier(IN const char *pszData);
extern BOOL TL1_IsAlphaNumHyphen(IN const char *pszData);
extern BOOL TL1_IsAlphaNumUnderscore(IN const char *pszData);
extern BOOL TL1_IsAlphaNumHyphenUnderscore(IN const char *pszData);
extern BOOL TL1_IsAlphaNum(IN const char *pszData);
extern BOOL TL1_IsAlpha(IN const char *pszData);
extern BOOL TL1_IsNumber(IN const char *pszData);

extern BOOL TL1_CheckYesNo(IN const char *szData);
extern BOOL TL1_CheckNA(IN const char *szData);
extern BOOL TL1_CheckSig(IN const char *szData);

extern const char* TL1_GetRespErrStr(IN int iIndex);
extern const char* TL1_GetAlmCode(IN int iIndex);
extern const char* TL1_GetAidTypeStr(IN int iIndex);
extern const char* TL1_GetSrveffStr(IN int iIndex);
extern const char* TL1_GetNtfcncdeStr(IN int iIndex);


extern int TL1_SendPacket(IN OUT TL1_BASIC_ARGS *pThis, IN char *pszData, IN int iLen);
extern int TL1_BreakConnection(IN OUT TL1_BASIC_ARGS *pThis);
extern int TL1_SendPrompt(IN OUT TL1_BASIC_ARGS *pThis);
extern int TL1_SetupTelnetSessionMode(IN OUT TL1_BASIC_ARGS *pThis);
extern int TL1_SendRespData(IN OUT TL1_BASIC_ARGS *pThis,
							IN char *pszRespData,
							IN int iMaxLen,
							IN BOOL bNeedPrompt,
							IN BOOL bForcePrompt);
extern void TL1_SendAlarm(IN OUT TL1_BASIC_ARGS *pThis);

extern int TL1_UpdateBlocksData(void);

#endif //__TL1_H_2015_04_29__

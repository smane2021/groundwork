/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : frame_analysis.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "tl1.h"


#include <ctype.h>


static BOOL CheckFieldCtag(IN const char *pszData);
static BOOL CheckFieldTid(IN const char *pszData);
static BOOL CheckFieldAid(IN const char *pszData);
static BOOL CheckFieldCmdCode(IN const char *pszData);
//static BOOL TL1_CheckFieldGeneralBlk(IN const char *pszData);


typedef struct tagTL1CmdFieldCfgDef
{
	int					iTableID;
	int					iTableL1Offset;
	int					iTableL2Offset;
	int					iTableL3Offset;

	TL1_CMD_FIELD_CFG	stCfg;

}TL1_CMD_FIELD_CFG_DEF;

//typedef struct tagTL1CmdFieldCfg
//{
//	BOOL			bNeedExist;
//	int				iLimitMinLen;
//	int				iLimitMaxLen;
//	char			cSubFieldSplitChar;
//	int				iErrCode;
//
//	BOOL			(* pfnCheckBasic)(const char *) ;
//
//} TL1_CMD_FIELD_CFG;

static TL1_CMD_FIELD_CFG_DEF sg_stCmdFieldCfgTab[] = 
{
	{
		TL1_FIELD_ID_TAB_L1, TL1_FIELD_ID_ITEM_CMD_CODE, -1, -1,
		{TRUE, 1, TL1_FIELD_LIMIT_CMD_CODE, TL1_CMD_SPLITTER_CMD_CODE, TL1_E_RESP_ERR_ICNV, CheckFieldCmdCode}
	},
	{
		TL1_FIELD_ID_TAB_L1, TL1_FIELD_ID_ITEM_TID, -1, -1,
		{TRUE, -1, TL1_FIELD_LIMIT_TID, TL1_CHAR_NULL, TL1_E_RESP_ERR_IITA, CheckFieldTid}
	},
	{
		TL1_FIELD_ID_TAB_L1, TL1_FIELD_ID_ITEM_AID, -1, -1,
		{TRUE, -1, TL1_FIELD_LIMIT_AID, TL1_CMD_SPLITTER_AID, TL1_E_RESP_ERR_IIAC, CheckFieldAid}
	},
	{
		TL1_FIELD_ID_TAB_L1, TL1_FIELD_ID_ITEM_CTAG, -1, -1,
		{TRUE, 1, TL1_FIELD_LIMIT_CTAG, TL1_CHAR_NULL, TL1_E_RESP_ERR_IICT, CheckFieldCtag}
	},
	{
		TL1_FIELD_ID_TAB_L1, TL1_FIELD_ID_ITEM_GENERAL_BLK, -1, -1,	
		{FALSE, -1, 0, TL1_CHAR_NULL, TL1_E_RESP_ERR_INUP, NULL}
	},
	{
		TL1_FIELD_ID_TAB_L1, TL1_FIELD_ID_ITEM_DATA_BLK, -1, -1,		
		{FALSE, -1, -1, TL1_CMD_SPLITTER_DATA_BLK, TL1_E_RESP_ERR_OK, NULL}
	},
};




/*==========================================================================*
 * FUNCTION : IsCharPrintable
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CaclBCC (BCC is between 0x20-0x7f)
 * ARGUMENTS: IN const unsigned char  c : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 20:20
 *==========================================================================*/
static BOOL IsCharPrintable(IN const unsigned char c)
{
	if(c >= 0x20 && c <= 0x7f)
	{
      return TRUE;
	}

    return FALSE;
}

/*==========================================================================*
 *	FUNCTION:�Գ���ΪnLen�������ݰ�У�飬��У���ֵд��pChkBytes��
 *   PURPOSE :���У���ֵֻ��1���ֽڣ���д��pChkBytes[0]��
 *	Input   :
 *			  pFrame  Ҫ���ݰ��ĵ�ַ.
 *			  nLen	��У������ݰ�����
 *			  pChkBytes װУ��ֵ�Ĵ�
 *	Output  :	pChkBytes У��ֵ
 *	RETURN  :��
 *	HISTORY :
 *==========================================================================*/

void TL1CheckSum (
                IN const unsigned char   *pFrame ,	// ҪУ��Ĵ��봮
                IN int		nLen ,		// ��ҪУ��Ĵ�����
				OUT unsigned char *pChkBytes	// ��У��Ľ��д��ô�
               )
{
    WORD	wChkValue	= 0 ;
    int i;
    for ( i = 0 ; i< nLen ; i++ )
    {
        wChkValue +=  pFrame[ i ] ;
    }
	
	wChkValue = ~wChkValue + 1 ; 
	//sprintf( ( char * )pChkBytes , "%04X" , wChkValue );
	HexToFourAsciiData(wChkValue, pChkBytes);
}

/*--------------------------------------------*/
/*  ChgAToHex: Convert array to hexa value. */
/* example: array="12345678af",num=5
            after calling  a_to_hexa(array,hexa,num)
            hexa={0x12,0x34,0x56,0x78,0xaf}
*/
/*--------------------------------------------*/
void ChgAToHex( IN const unsigned char *array, OUT unsigned int *hexa, IN int num)
{
	int  i;
	BYTE byTemp;

	for(i=0; i< (num*2); i++) 
	{
		if( *(array + i) > '9' )
			byTemp = *(array + i) - 'A' + 10;
		else
			byTemp = *(array + i) - '0';
		if (i % 2)
			hexa[i/2] |= byTemp;
		else
			hexa[i/2] = byTemp << 4;
	}
}





static BOOL CheckFieldCtag(IN const char *pszData)
{
	
	//The value of the CTAG is an identifier or a decimal numeral, 
	//and is limited to a maximum of six characters. 
	//A TL1 identifier starts with a letter and is followed 
	//by any number of letters or digits(e.g., B, Ajrk, ur245). 
	//This application must also accept an unsigned integer value 
	//from 1 through 65535 as a valid value.

	if(TL1_CHAR_NULL == pszData[0])
	{
		return TRUE;
	}

	if(TL1_IsIdentifier(pszData) || TL1_IsIntInRange(pszData, 1, 65535))
	{
		return TRUE;
	}

	return FALSE;
}

static BOOL CheckFieldTid(IN const char *pszData)
{
	//limited to letters, digits, and hyphens.For all input command messages 
	//the value of the TID must match the value of the system identifier parameter 
	//or else be a NULL.

	if(TL1_CHAR_NULL == pszData[0])
	{
		return TRUE;
	}

	return TL1_IsAlphaNumUnderscore(pszData);
}

static BOOL CheckFieldAid(IN const char *pszData)
{
	//limited to letters, digits, and hyphens.For all input command messages 
	//the value of the TID must match the value of the system identifier parameter 
	//or else be a NULL.

	if(TL1_CHAR_NULL == pszData[0])
	{
		return TRUE;
	}

	return TL1_IsAlphaNumHyphenUnderscore(pszData);
}

static BOOL CheckFieldCmdCode(IN const char *pszData)
{
	//limited to letters, digits, and hyphens.For all input command messages 
	//the value of the TID must match the value of the system identifier parameter 
	//or else be a NULL.

	if(TL1_CHAR_NULL == pszData[0])
	{
		return TRUE;
	}

	return TL1_IsAlphaNumHyphenUnderscore(pszData);
}


//static BOOL TL1_CheckFieldGeneralBlk(IN const char *pszData)
//{
//	//unused, must be NULL
//
//	//if(TL1_CHAR_NULL == pszData[0])
//	//{
//	//	return TRUE;
//	//}
//
//	return TRUE;
//}



static void ConfigFields(IN TL1_COMMAND *pstCmd, IN TL1_CMD_FIELD_CFG_DEF *pstCfgDef, IN int iCfgNum)
{
	TL1_CMD_FIELD_CFG_DEF	*pstItem = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL1 = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL2 = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL3 = NULL;
	int						i;


	for(i = 0; i < iCfgNum; i++)
	{
		pstItem = &pstCfgDef[i];

		if(TL1_FIELD_ID_TAB_L1 == pstItem->iTableID)
		{
			pstCmdFieldL1 = &pstCmd->stFieldTabL1[pstItem->iTableL1Offset];
			memcpy(&pstCmdFieldL1->stCfg, &pstItem->stCfg, sizeof(pstCmdFieldL1->stCfg));
		}
		else if(TL1_FIELD_ID_TAB_L2 == pstItem->iTableID)
		{
			pstCmdFieldL2 = &pstCmd->stFieldTabL2[pstItem->iTableL1Offset][pstItem->iTableL2Offset];
			memcpy(&pstCmdFieldL2->stCfg, &pstItem->stCfg, sizeof(pstCmdFieldL2->stCfg));
		}
		else if(TL1_FIELD_ID_TAB_L3 == pstItem->iTableID)
		{
			pstCmdFieldL3 = &pstCmd->stFieldTabL3[pstItem->iTableL1Offset][pstItem->iTableL2Offset][pstItem->iTableL3Offset];
			memcpy(&pstCmdFieldL3->stCfg, &pstItem->stCfg, sizeof(pstCmdFieldL3->stCfg));
		}
	}

}


static void ResetAllFields(IN TL1_COMMAND *pstCmd, IN BOOL bResetRunInfo, IN BOOL bResetCfg)
{
	TL1_CMD_FIELD			*pstCmdFieldL1 = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL2 = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL3 = NULL;
	int						i, j, k;


	for(i = 0; i < TL1_MAX_FIELD_COUNT_L1; i++)
	{
		pstCmdFieldL1 = &pstCmd->stFieldTabL1[i];

		//check level 1 fields
		if(bResetCfg)
		{
			pstCmdFieldL1->stCfg.bNeedExist = FALSE;
			pstCmdFieldL1->stCfg.iLimitMinLen = -1;
			pstCmdFieldL1->stCfg.iLimitMaxLen = -1;
			pstCmdFieldL1->stCfg.cSubFieldSplitChar = TL1_CHAR_NULL;
			pstCmdFieldL1->stCfg.pfnCheckBasic = NULL;
		}

		if(bResetRunInfo)
		{
			pstCmdFieldL1->bIsExist = FALSE;
			pstCmdFieldL1->iLen = 0;
			pstCmdFieldL1->szData[0] = TL1_CHAR_NULL;
		}

		for(j = 0; j < TL1_MAX_FIELD_COUNT_L2; j++)
		{
			pstCmdFieldL2 = &pstCmd->stFieldTabL2[i][j];

			//check level 2 fields
			if(bResetCfg)
			{
				pstCmdFieldL2->stCfg.bNeedExist = FALSE;
				pstCmdFieldL2->stCfg.iLimitMinLen = -1;
				pstCmdFieldL2->stCfg.iLimitMaxLen = -1;
				pstCmdFieldL2->stCfg.cSubFieldSplitChar = TL1_CHAR_NULL;
				pstCmdFieldL2->stCfg.pfnCheckBasic = NULL;
			}

			if(bResetRunInfo)
			{
				pstCmdFieldL2->bIsExist = FALSE;
				pstCmdFieldL2->iLen = 0;
				pstCmdFieldL2->szData[0] = TL1_CHAR_NULL;
			}

			for(k = 0; k < TL1_MAX_FIELD_COUNT_L3; k++)
			{
				pstCmdFieldL3 = &pstCmd->stFieldTabL3[i][j][k];

				if(bResetCfg)
				{
					pstCmdFieldL3->stCfg.bNeedExist = FALSE;
					pstCmdFieldL3->stCfg.iLimitMinLen = -1;
					pstCmdFieldL3->stCfg.iLimitMaxLen = -1;
					pstCmdFieldL3->stCfg.cSubFieldSplitChar = TL1_CHAR_NULL;
					pstCmdFieldL3->stCfg.pfnCheckBasic = NULL;
				}

				if(bResetRunInfo)
				{
					pstCmdFieldL3->bIsExist = FALSE;
					pstCmdFieldL3->iLen = 0;
					pstCmdFieldL3->szData[0] = TL1_CHAR_NULL;
				}
			}
		}
	}

}

//be careful!!! the frame data will be broken after parsing field
static int ParseAllFields(IN TL1_COMMAND *pstCmd, IN OUT char *pFrame, IN int iLen)
{
	TL1_CMD_FIELD			*pstCmdFieldL1 = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL2 = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL3 = NULL;
	char					*pszBuf = pFrame;
	char					*pszFieldL0 = NULL;
	char					*pszFieldL1 = NULL;
	char					*pszFieldL2 = NULL;
	char					*pszFieldL3 = NULL;
	int						i, j, k;
	char					szTempBuf[3];
	BOOL					bFieldL0HasSplitter;
	BOOL					bFieldL1HasSplitter;
	BOOL					bFieldL2HasSplitter;
	BOOL					bFieldL3HasSplitter;
	BOOL					bLastFieldL1HasSplitter;
	BOOL					bLastFieldL2HasSplitter;
	BOOL					bLastFieldL3HasSplitter;


	//2. get the whole string of command
	//a command string example:
	//RTRV-ALM-{EQPT|ALL}:[<tid>]:<aid>:<ctag>::[<ntfcncde>],[<condtype>],[<srveff>],[<locn>],[<dirn>][,<tmper>];

	//parse command
	pszBuf = Cfg_SplitStringExForTL1(pszBuf, &pszFieldL0, TL1_CMD_SPLITTER_L0, &bFieldL0HasSplitter);
	if((pszBuf == pszFieldL0) || !bFieldL0HasSplitter)
	{
		return TL1_E_ERR_FRAME_PARSE;
	}
	strncpyz(pstCmd->szInData, pszFieldL0, sizeof(pstCmd->szInData));

	//append a ":" to the end of command for parsing the last field correctly
	//snprintf(szTempBuf, sizeof(szTempBuf), "%c", TL1_CMD_SPLITTER_L1);
	//strcat(pszFieldL0, szTempBuf);

	for(i = 0, bFieldL1HasSplitter = TRUE; i < TL1_MAX_FIELD_COUNT_L1; i++)
	{
		//process all level 1 fields
		pstCmdFieldL1 = &pstCmd->stFieldTabL1[i];

		//a level 1 field is such as: <tid>
		bLastFieldL1HasSplitter = bFieldL1HasSplitter;
		pszFieldL0 = Cfg_SplitStringExForTL1(pszFieldL0, &pszFieldL1, TL1_CMD_SPLITTER_L1, &bFieldL1HasSplitter);
		//printf("pszFieldL1=%s, i=%d, Exist=%d\n", pszFieldL1, i, (pszFieldL0 != pszFieldL1));
		if(pszFieldL0 == pszFieldL1)
		{
			if(bLastFieldL1HasSplitter)//special process for the last splitter
			{
				pstCmdFieldL1->bIsExist = TRUE;

				pstCmdFieldL1->szData[0] = TL1_CHAR_NULL;
				pstCmdFieldL1->iLen = 0;
			}
			else
			{
				pstCmdFieldL1->bIsExist = FALSE;

				pstCmdFieldL1->szData[0] = TL1_CHAR_NULL;
				pstCmdFieldL1->iLen = 0;
			}
		}
		else
		{
			pstCmdFieldL1->bIsExist = TRUE;

			strncpyz(pstCmdFieldL1->szData, pszFieldL1, sizeof(pstCmdFieldL1->szData));
			pstCmdFieldL1->iLen = strlen(pstCmdFieldL1->szData);

			//process all level 2 fields
			if(TL1_CHAR_NULL != pstCmdFieldL1->stCfg.cSubFieldSplitChar)
			{
				for(j = 0, bFieldL2HasSplitter = TRUE; j < TL1_MAX_FIELD_COUNT_L2; j++)
				{
					pstCmdFieldL2 = &pstCmd->stFieldTabL2[i][j];

					//a level 2 field is such as: <ntfcncde>
					bLastFieldL2HasSplitter = bFieldL2HasSplitter;
					pszFieldL1 = Cfg_SplitStringExForTL1(pszFieldL1, &pszFieldL2, pstCmdFieldL1->stCfg.cSubFieldSplitChar, &bFieldL2HasSplitter);
					//printf("pszFieldL2=%s, j=%d, Exist=%d\n", pszFieldL2, j, (pszFieldL1 != pszFieldL2));
					if(pszFieldL1 == pszFieldL2)
					{
						if(bLastFieldL2HasSplitter)//special process for the last splitter
						{
							pstCmdFieldL2->bIsExist = TRUE;

							pstCmdFieldL2->szData[0] = TL1_CHAR_NULL;
							pstCmdFieldL2->iLen = 0;
						}
						else
						{
							pstCmdFieldL2->bIsExist = FALSE;

							pstCmdFieldL2->szData[0] = TL1_CHAR_NULL;
							pstCmdFieldL2->iLen = 0;
						}
					}
					else
					{
						pstCmdFieldL2->bIsExist = TRUE;

						strncpyz(pstCmdFieldL2->szData, pszFieldL2, sizeof(pstCmdFieldL2->szData));
						pstCmdFieldL2->iLen = strlen(pstCmdFieldL2->szData);

						//process all level 3 fields
						if(TL1_CHAR_NULL != pstCmdFieldL2->stCfg.cSubFieldSplitChar)
						{
							for(k = 0, bFieldL3HasSplitter = TRUE; k < TL1_MAX_FIELD_COUNT_L3; k++)
							{
								pstCmdFieldL3 = &pstCmd->stFieldTabL3[i][j][k];

								//a level 3 field is such as: 
								//not be used currently
								bLastFieldL3HasSplitter = bFieldL3HasSplitter;
								pszFieldL2 = Cfg_SplitStringExForTL1(pszFieldL2, &pszFieldL3, pstCmdFieldL2->stCfg.cSubFieldSplitChar, &bFieldL3HasSplitter);
								//printf("pszFieldL3=%s\n", pszFieldL3);
								if(pszFieldL2 == pszFieldL3)
								{
									if(bLastFieldL3HasSplitter)//special process for the last splitter
									{
										pstCmdFieldL3->bIsExist = TRUE;

										pstCmdFieldL3->szData[0] = TL1_CHAR_NULL;
										pstCmdFieldL3->iLen = 0;
									}
									else
									{
										pstCmdFieldL3->bIsExist = FALSE;

										pstCmdFieldL3->szData[0] = TL1_CHAR_NULL;
										pstCmdFieldL3->iLen = 0;
									}
								}
								else
								{
									pstCmdFieldL3->bIsExist = TRUE;

									strncpyz(pstCmdFieldL3->szData, pszFieldL3, sizeof(pstCmdFieldL3->szData));
									pstCmdFieldL3->iLen = strlen(pstCmdFieldL3->szData);
								}
							}//for(k = 0; k < TL1_MAX_FIELD_COUNT_L3; k++)
						}
						else
						{
							for(k = 0; k < TL1_MAX_FIELD_COUNT_L3; k++)
							{
								pstCmdFieldL3 = &pstCmd->stFieldTabL3[i][j][k];
								pstCmdFieldL3->bIsExist = FALSE;

								pstCmdFieldL3->szData[0] = TL1_CHAR_NULL;
								pstCmdFieldL3->iLen = 0;
							}
						}

					}
				}//for(j = 0; j < TL1_MAX_FIELD_COUNT_L2; j++)
			}
			else
			{
				for(j = 0; j < TL1_MAX_FIELD_COUNT_L2; j++)
				{
					pstCmdFieldL2 = &pstCmd->stFieldTabL2[i][j];
					pstCmdFieldL2->bIsExist = FALSE;

					pstCmdFieldL2->szData[0] = TL1_CHAR_NULL;
					pstCmdFieldL2->iLen = 0;
				}
			}
		}
	}//for(i = 0; i < TL1_MAX_FIELD_COUNT_L1; i++)

	return TL1_E_FRAME_OK;
}

static TL1_CMD_FIELD* CheckAllFields(IN TL1_COMMAND *pstCmd)
{
	TL1_CMD_FIELD			*pstCmdFieldL1 = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL2 = NULL;
	TL1_CMD_FIELD			*pstCmdFieldL3 = NULL;
	int						i, j, k;

	for(i = 0; i < TL1_MAX_FIELD_COUNT_L1; i++)
	{
		pstCmdFieldL1 = &pstCmd->stFieldTabL1[i];

		//check level 1 fields
		if( !pstCmdFieldL1->bIsExist )
		{
			if(pstCmdFieldL1->stCfg.bNeedExist)
			{

				return pstCmdFieldL1;
			}
			else
			{
				continue;
			}
		}

		if(pstCmdFieldL1->stCfg.iLimitMinLen >= 0)
		{
			if(pstCmdFieldL1->iLen < pstCmdFieldL1->stCfg.iLimitMinLen)
			{
				return pstCmdFieldL1;
			}
		}

		if(pstCmdFieldL1->stCfg.iLimitMaxLen >= 0)
		{
			if(pstCmdFieldL1->iLen > pstCmdFieldL1->stCfg.iLimitMaxLen)
			{
				return pstCmdFieldL1;
			}
		}

		if(NULL != pstCmdFieldL1->stCfg.pfnCheckBasic)
		{
			if( !(pstCmdFieldL1->stCfg.pfnCheckBasic(pstCmdFieldL1->szData)) )
			{
				return pstCmdFieldL1;
			}
		}


		for(j = 0; j < TL1_MAX_FIELD_COUNT_L2; j++)
		{
			pstCmdFieldL2 = &pstCmd->stFieldTabL2[i][j];

			//check level 2 fields
			if( !pstCmdFieldL2->bIsExist )
			{
				if(pstCmdFieldL2->stCfg.bNeedExist)
				{
					return pstCmdFieldL2;
				}
				else
				{
					continue;
				}
			}

			if(pstCmdFieldL2->stCfg.iLimitMinLen >= 0)
			{
				if(pstCmdFieldL2->iLen < pstCmdFieldL2->stCfg.iLimitMinLen)
				{
					return pstCmdFieldL2;
				}
			}

			if(pstCmdFieldL2->stCfg.iLimitMaxLen >= 0)
			{
				if(pstCmdFieldL2->iLen > pstCmdFieldL2->stCfg.iLimitMaxLen)
				{
					return pstCmdFieldL2;
				}
			}

			if(NULL != pstCmdFieldL2->stCfg.pfnCheckBasic)
			{
				if( !(pstCmdFieldL2->stCfg.pfnCheckBasic(pstCmdFieldL2->szData)) )
				{
					return pstCmdFieldL2;
				}
			}


			for(k = 0; k < TL1_MAX_FIELD_COUNT_L3; k++)
			{
				pstCmdFieldL3 = &pstCmd->stFieldTabL3[i][j][k];

				if( !pstCmdFieldL3->bIsExist )
				{
					//check level 3 fields
					if(pstCmdFieldL3->stCfg.bNeedExist)
					{
						return pstCmdFieldL3;
					}
					else
					{
						continue;
					}
				}

				if(pstCmdFieldL3->stCfg.iLimitMinLen >= 0)
				{
					if(pstCmdFieldL3->iLen < pstCmdFieldL3->stCfg.iLimitMinLen)
					{
						return pstCmdFieldL3;
					}
				}

				if(pstCmdFieldL3->stCfg.iLimitMaxLen >= 0)
				{
					if(pstCmdFieldL3->iLen > pstCmdFieldL3->stCfg.iLimitMaxLen)
					{
						return pstCmdFieldL3;
					}
				}

				if(NULL != pstCmdFieldL3->stCfg.pfnCheckBasic)
				{
					if( !(pstCmdFieldL3->stCfg.pfnCheckBasic(pstCmdFieldL3->szData)) )
					{
						return pstCmdFieldL3;
					}
				}
			}
		}
	}

	return NULL;
}

void TL1_PrintField(IN TL1_CMD_FIELD *pstField)
{
	printf("---- Field ----\n");
	printf("bIsExist=%d, iLen=%d, szData=%s, bNeedExist=%d, "
		"cSubFieldSplitChar=%c, iErrCode=%d, iLimitMaxLen=%d, iLimitMinLen=%d\n",
		pstField->bIsExist,
		pstField->iLen,
		pstField->szData,
		pstField->stCfg.bNeedExist,
		pstField->stCfg.cSubFieldSplitChar,
		pstField->stCfg.iErrCode,
		pstField->stCfg.iLimitMaxLen,
		pstField->stCfg.iLimitMinLen);
	printf("--------\n");
}

/*==========================================================================*
 * FUNCTION : TL1_AnalyseFrame
 * PURPOSE  : 
 * CALLS    : SOC_AnalyseSTXFrame
 *			  RSOC_AnalyseSTXFrame
 * CALLED BY: 
 * ARGUMENTS: unsigned unsigned char *  pFrame : 
 *            int                       iLen   : 
 * RETURN   : FRAME_TYPE : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 16:32
 *==========================================================================*/
TL1_FRAME_TYPE TL1_AnalyseFrame(IN OUT const unsigned char *pFrame, 
							IN int iLen,
							IN OUT TL1_BASIC_ARGS *pThis)
{

	TL1_COMMON_CONFIG		*pCfg = &g_TL1Globals.CommonConfig;
	TL1_COMMAND				*pstCmd = &pThis->stSession.stCommand;
	TL1_CMD_FIELD			*pstField = NULL;
	int						iRet, i;
	char					*pBuf = (char *)pFrame;
	int						iValidCharCount = 0;
	

	pBuf[iLen] = TL1_CHAR_NULL;
	memset(pstCmd, 0, sizeof(TL1_COMMAND));

	//1.check telnet session setup frame
	if(0 == iLen % 3)//a telnet setup param resp frame len is 3 bytes
	{
		for(i = 1; i < iLen; i += 3)
		{
			//FF FC 17 FF FD 01 FF FC 01 
			if((0xFF != pBuf[i - 1]) || (0xFF == pBuf[i]))
			{
				//not a telnet setup param resp frame
				break;
			}
		}

		if(i > iLen)//is a telnet setup param resp frame
		{
			return TL1_E_ERR_FRAME_IGNORE;
		}
	}

	//2. check whether each char is ASCII char
	//Only ASCII characters are accepted. Backspace and other control characters and 
	//extended characters must be ignored.
	iValidCharCount = 0;
	for(i = 0; i < iLen; i++)
	{
		if( isprint(pBuf[i]) )
		{
			pBuf[iValidCharCount] = pBuf[i];
			iValidCharCount++;
		}
	}
	pBuf[iValidCharCount] = TL1_CHAR_NULL;
	
	if(iValidCharCount <= 0)
	{
		pstCmd->iErrCode = TL1_E_RESP_ERR_IISP;
		return TL1_E_ERR_FRAME_PARSE;
	}

	//3. init all fields in command
	ResetAllFields(pstCmd, TRUE, TRUE);

	//4. load field config info
	ConfigFields(pstCmd, sg_stCmdFieldCfgTab, sizeof(sg_stCmdFieldCfgTab)/sizeof(sg_stCmdFieldCfgTab[0]));

	//5. parse command to fields
	iRet = ParseAllFields(pstCmd, pBuf, iValidCharCount);
	if(TL1_E_FRAME_OK != iRet)
	{
		pstCmd->iErrCode = TL1_E_RESP_ERR_IISP;
		return TL1_E_ERR_FRAME_PARSE;
	}

	//6. check fields
	pstField = CheckAllFields(pstCmd);
	if(NULL != pstField)//exist invalid field
	{
		TL1_PrintField(pstField);
		pstCmd->iErrCode = pstField->stCfg.iErrCode;
		return TL1_E_ERR_FRAME_CHECK;
	}


	//7. get special fields
	pstCmd->pstCmdCode = &pstCmd->stFieldTabL1[TL1_FIELD_ID_ITEM_CMD_CODE];

	pstCmd->pstVerb = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_CMD_CODE][0];
	pstCmd->pstModifier1 = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_CMD_CODE][1];
	pstCmd->pstModifier2 = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_CMD_CODE][2];

	pstCmd->pstTid = &pstCmd->stFieldTabL1[TL1_FIELD_ID_ITEM_TID];

	pstCmd->pstAid = &pstCmd->stFieldTabL1[TL1_FIELD_ID_ITEM_AID];
	pstCmd->pstMainAid = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_AID][0];
	pstCmd->pstSubAid = &pstCmd->stFieldTabL2[TL1_FIELD_ID_ITEM_AID][1];

	pstCmd->pstCtag = &pstCmd->stFieldTabL1[TL1_FIELD_ID_ITEM_CTAG];
	pstCmd->pstGeneralBlk = &pstCmd->stFieldTabL1[TL1_FIELD_ID_ITEM_GENERAL_BLK];
	pstCmd->pstDataBlk = &pstCmd->stFieldTabL1[TL1_FIELD_ID_ITEM_DATA_BLK];
	pstCmd->pstSeventhBlk = &pstCmd->stFieldTabL1[TL1_FIELD_ID_ITEM_SEVENTH_BLK];

	printf("pstCmdCode=%s, pstVerb=%s,pstModifier1=%s,pstModifier2=%s,pstTid=%s,"
		"pstAid=%s,pstMainAid=%s,pstSubAid=%s,pstCtag=%s,pstGeneralBlk=%s,pstDataBlk=%s,"
		"pstSeventhBlk=%s\n",
		pstCmd->pstCmdCode->szData,
		pstCmd->pstVerb->szData,
		pstCmd->pstModifier1->szData,
		pstCmd->pstModifier2->szData,
		pstCmd->pstTid->szData,
		pstCmd->pstAid->szData,
		pstCmd->pstMainAid->szData,
		pstCmd->pstSubAid->szData,
		pstCmd->pstCtag->szData,
		pstCmd->pstGeneralBlk->szData,
		pstCmd->pstDataBlk->szData,
		pstCmd->pstSeventhBlk->szData
		);
	printf("szInData=%s\n", pstCmd->szInData);

	//8 check TID
	if((TL1_CHAR_NULL != pstCmd->pstTid->szData[0])
		&& (0 != strcmp(pstCmd->pstTid->szData, pCfg->szSystemIdentifier)))
	{
		//3.2.3.1.3 If the target identifier doesn't match the controller system identifier.. 
		//No response is made and the command prompt is output.
		//pstCmd->iErrCode = TL1_E_RESP_ERR_IITA;
		//return TL1_E_ERR_FRAME_CHECK;
		return TL1_E_ERR_FRAME_JUST_PROMPT;
	}

	return TL1_E_FRAME_OK;

}

/*==========================================================================*
 * FUNCTION : TL1_ExtractDataFromFrame
 * PURPOSE  : extract cmd data form frame
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN FRAME_TYPE frameType  : 
 *			  IN const unsigned char  *pFrame   : 
 *            IN int         iFrameLen : 
 *            OUT int        *piDataLen: 
 * RETURN   : const unsigned char *: 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-10-12 20:46
 *==========================================================================*/
void TL1_ExtractDataFromFrame(IN const unsigned char *pFrame,
						       IN int iFrameLen, 
						       OUT int *piDataLen,
						       OUT unsigned char *pCmdData)
{

	UNUSED(pFrame);
	UNUSED(iFrameLen);
	UNUSED(piDataLen);
	UNUSED(pCmdData);

}



#ifdef COM_SHARE_SERVICE_SWITCH

#define CRC32_DEFAULT    0x04C10DB7
static void BuildTable32(unsigned long aPoly , unsigned long *Table_CRC)
{
    unsigned long i, j;
    unsigned long iData;
    unsigned long iAccum;

    for (i = 0; i < 256; i++)
    {
        iData = (unsigned long )( i << 24);
        iAccum = 0;
        for (j = 0; j < 8; j++)
        {
            if (( iData ^ iAccum ) & 0x80000000)
			{
                iAccum = ( iAccum << 1 ) ^ aPoly;
			}
            else
			{
                iAccum <<= 1;
			}
            iData <<= 1;
        }
        Table_CRC[i] = iAccum;
    }
}

static unsigned long RunCRC32(const unsigned char *aData, 
							  unsigned long aSize, 
							  unsigned long aPoly)
{
    unsigned long Table_CRC[256]; // CRC table
    unsigned long i;
    unsigned long iAccum = 0;

    BuildTable32(aPoly, Table_CRC);
    
    for (i = 0; i < aSize; i++)
	{
        iAccum = ( iAccum << 8 ) ^ Table_CRC[( iAccum >> 24 ) ^ *aData++];
	}

    return iAccum;
}

BOOL TL1_IsMtnFrame(const unsigned char *pFrame, int iLen)
{
	DWORD dwCalCRC32;

	if (iLen < 13)
	{ 
		return FALSE;
	}

	if (pFrame[0] != 0x7e)
	{
		return FALSE;
	}

	if (pFrame[3] != 0xd5)
	{
		return FALSE;
	}

	if (pFrame[4] != 0xcc)
	{
		return FALSE;
	}
	
	if (pFrame[6] != 0 || pFrame[7] != 0)
	{
		return FALSE;
	}

	if (pFrame[12] != 0x0d)
	{
		return FALSE;
	}

	dwCalCRC32 =  *((DWORD *)(pFrame + 8));
	if (dwCalCRC32 != RunCRC32(pFrame + 1, 7, (unsigned long)CRC32_DEFAULT))
	{
		return FALSE;
	}

	return TRUE;
}

	

void TL1_BuildMtnResponseFrame(OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen)
{
	pFrameData[0] = 0x7e;
	pFrameData[1] = 1;
	pFrameData[2] = 0xff;
	pFrameData[3] = 0xd5;

	/* com is using */
	pFrameData[4] = 0x11;    

	pFrameData[5] = 0;
	pFrameData[6] = 0;
	pFrameData[7] = 0;

	/* get CRC */
	*((DWORD *)(pFrameData + 8)) = 
		RunCRC32(pFrameData + 1, 7, (unsigned long)CRC32_DEFAULT);

	pFrameData[12] = 0x0d;

	*piFrameDataLen = 13;

	return;
}

#endif //COM_SHARE_SERVICE_SWITCH

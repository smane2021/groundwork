/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : service_provider.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "tl1.h"
#include "addtl1.h"
/* define log tasks */
#define LOG_INIT_SERVICE				"Init TL1 service"
#define LOG_SERVICE						"TL1 service"

#define LOG_MODIFY_COMMON_CONFIG		"TL1 PARAM"


 
/* used to init state machines */
#define DEF_STATE_MACHINE(_p, _iStateID, _fnProc)  \
	      ((_p)->iStateID = (_iStateID), \
		   (_p)->fnStateProc = (_fnProc))



/* define the global variable of service */
TL1_GLOBALS				g_TL1Globals;
TL1_BASIC_ARGS			g_stBasicArgs;


/* define the share-used simple events */
TL1_EVENT g_TL1DummyEvent, g_TL1TimeoutEvent;
TL1_EVENT g_TL1DiscEvent, g_TL1ConnFailEvent, g_TL1ConnEvent;


__INLINE static void InitStaticEvents(void)
{
	TL1_INIT_FRAME_EVENT(&g_TL1DummyEvent, 0, 0, TRUE, FALSE);
	TL1_INIT_NONEFRAME_EVENT(&g_TL1TimeoutEvent, TL1_TIMEOUT_EVENT);
	TL1_INIT_NONEFRAME_EVENT(&g_TL1DiscEvent, TL1_DISCONNECTED_EVENT);
	TL1_INIT_NONEFRAME_EVENT(&g_TL1ConnFailEvent, TL1_CONNECT_FAILED_EVENT);
	TL1_INIT_NONEFRAME_EVENT(&g_TL1ConnEvent, TL1_CONNECTED_EVENT);
}
/*==========================================================================*
 * FUNCTION : AlarmProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ServiceNotification
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 19:26
 *==========================================================================*/
static int AlarmProc(IN OUT TL1_BASIC_ARGS *pThis, IN ALARM_SIG_VALUE *pAlarmSig, IN int iMsgType)
{
	int						iRet = 0;
	TL1_ALARM_EVENT			stAlarmEvent;
	TL1_ALARM_HANDLER		*pAlarmHandler = &pThis->alarmHandler;

#ifdef _DEBUG_TL1_REPORT

	TRACE_TL1_TIPS("Alarm captured by TL1 module");

#endif //_DEBUG_TL1_REPORT

	//just process current active during valid session
	if( !pThis->stSession.bIsValid )
	{
		return 0;
	}


	stAlarmEvent.iMsgType = iMsgType;
	stAlarmEvent.tmStartTime = pAlarmSig->sv.tmStartTime;
	stAlarmEvent.tmEndTime = pAlarmSig->sv.tmEndTime;
	stAlarmEvent.iAlarmLevel = pAlarmSig->iAlarmLevel;
	stAlarmEvent.pAlarmSigVal = pAlarmSig;

	TRACE("AlarmProc::iMsgType=%d,AlarmSigId=%d\n", iMsgType, pAlarmSig->pStdSig->iSigID);

	iRet = Queue_Put(pThis->hEventAlarmQueue, &stAlarmEvent, FALSE);
	if (iRet != ERR_OK)
	{
		AppLogOut(LOG_SERVICE, APP_LOG_ERROR, "Queue_Put pstAlarmEvent failed.\n");
		TRACE("[%s]--%s: ERROR:Queue_Put pstAlarmEvent failed.\n", __FILE__, __FUNCTION__);
	}

	pAlarmHandler->bAlarmAtHand = TRUE;

	//TL1_RegisterAlarm(pThis, FALSE);

	return 0;
}


/*==========================================================================*
 * FUNCTION : ServiceNotification
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService        : 
 *            int     nMsgType        : 
 *            int     nTrapDataLength : 
 *            void*   lpTrapData      : 
 *            void*   lpParam         :  
 *            BOOL    bUrgent         : 
 * RETURN   : int : 0 for success, 1 for error(defined in DXI module)
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 17:23
 *==========================================================================*/
int ServiceNotification(IN HANDLE hService, 
						IN int nMsgType, 
						IN int nTrapDataLength, 
						IN void *lpTrapData,	
						IN void *lpParam, 
						IN BOOL bUrgent)
{
	TL1_BASIC_ARGS *pBasicArgs = (TL1_BASIC_ARGS *)lpParam;
	ALARM_SIG_VALUE *pAlarmSig = (ALARM_SIG_VALUE *)lpTrapData;

	UNUSED(hService);
	UNUSED(nTrapDataLength);
	//UNUSED(lpTrapData);
	UNUSED(bUrgent);
	

	//TRACE("pAlarmSig->iAlarmLevel is : %d\n", pAlarmSig->iAlarmLevel);

	//if(g_TL1Globals.CommonConfig.iMediaType != TL1_MEDIA_TYPE_MODEM)
		//return 1;
	

	if (pAlarmSig->iAlarmLevel >= ALARM_LEVEL_OBSERVATION && 
		(nMsgType == _ALARM_OCCUR_MASK || nMsgType == _ALARM_CLEAR_MASK))
	{
		TRACE(" TL1::ServiceNotification pSigName=%s\n", pAlarmSig->pStdSig->pSigName->pFullName[0]);
		TRACE("pAlarmSig EquipId=%d, iSigID=%d, nMsgType=%x\n",
			pAlarmSig->pEquipInfo->iEquipID,
			pAlarmSig->pStdSig->iSigID, nMsgType);

		return AlarmProc(pBasicArgs, pAlarmSig, nMsgType);
	}



	return 1;
}

/*==========================================================================*
 * FUNCTION : InitTL1Globals
 * PURPOSE  : assistant function to init g_TL1Globals
 * CALLS    : 
 * CALLED BY: ServiceMain
 * RETURN   : DWORD : use SERVICE_EXIT_CODE const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 20:08
 *==========================================================================*/
static DWORD InitTL1Globals(void)
{
	TL1_STATE_INFO  *pStateInfo;

	/* 1.init config */
	if (TL1_InitConfig() != ERR_CFG_OK)
	{
		return SERVICE_EXIT_CFG;
	}

	/* create mutex for YDN Common Config file */
	/*g_TL1Globals.hMutexCommonCfg = Mutex_Create(TRUE);
	if (g_TL1Globals.hMutexCommonCfg == NULL)
	{
		AppLogOut(LOG_INIT_SERVICE, APP_LOG_ERROR, 
			"Create Mutex for YDN Common Config file failed\n");
		TRACE("[%s]--%s: ERROR: Create Mutex for YDN Common Config file"
			" failed.\n", __FILE__, __FUNCTION__);

			return SERVICE_EXIT_FAIL;
	}*/

	/* 2.init command handler */
	TL1_InitCmdHander();
	
	/* 3.init state machine */
	pStateInfo = g_TL1Globals.staStates;

	DEF_STATE_MACHINE(&pStateInfo[0], 
		TL1_IDLE, 
		TL1_OnIdle);

	//DEF_STATE_MACHINE(&pStateInfo[1], 
	//	TL1_WAIT_FOR_CMD, 
	//	TL1_OnWaitForCMD);

	//DEF_STATE_MACHINE(&pStateInfo[2], 
	//	TL1_SEND_ALARM, 
	//	TL1_OnSendAlarm);

	/* 4. init some flags*/


	return SERVICE_EXIT_NONE;
}


/*==========================================================================*
 * FUNCTION : StateMachineLoop
 * PURPOSE  : for main loop of state machine
 * CALLS    : 
 * CALLED BY: ServiceMain
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis : 
 * RETURN   : DWORD : use SERVICE_EXIT_CODE const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 15:01
 *==========================================================================*/
static DWORD StateMachineLoop(IN OUT TL1_BASIC_ARGS *pThis)
{
	int iLastState;
	int *piState;
	TL1_STATE_INFO *pCurStateMachine;

	piState = &(pThis->iMachineState);
	
	while (*(pThis->pOutQuitCmd) == SERVICE_CONTINUE_RUN && 
		pThis->iInnerQuitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		
		RunThread_Heartbeat(pThis->hThreadID[0]);
		//printf("Begin the state machine loop!\n");


			pCurStateMachine = g_TL1Globals.staStates;
			*piState = TL1_IDLE;

			TRACE_TL1_TIPS("Begin TL1 state machine");

		//init session
		memset(&pThis->stSession, 0, sizeof(pThis->stSession));

		while (*piState != TL1_STATE_MACHINE_QUIT)
		{
			/* for TRACE */
			iLastState = *piState;

			/* process one state */
			*piState = pCurStateMachine[*piState].fnStateProc(pThis);

			/* check quit conditions */
			if (*(pThis->pOutQuitCmd) == SERVICE_STOP_RUNNING ||
				pThis->iInnerQuitCmd != SERVICE_EXIT_NONE ||
				pThis->bProtocolChangedFlag)
			{
				TRACE("Exit state machine loop");
				TRACE("OutQuitCmd: %d\tInnerQuitCmd: %d\tProtocol Changed "
					"Flag: %d\n", *(pThis->pOutQuitCmd), pThis->iInnerQuitCmd,
					pThis->bProtocolChangedFlag);

				*piState = TL1_STATE_MACHINE_QUIT;

				/* reset flag */
				if (pThis->bProtocolChangedFlag)
				{
					pThis->bProtocolChangedFlag = FALSE;
					pThis->bFrameDone = TRUE;
				}
				//wait for config update
				Sleep(2000);
			}
			/* check quit conditions */
			if (*(pThis->pOutQuitCmd) == SERVICE_STOP_RUNNING ||
				pThis->iInnerQuitCmd != SERVICE_EXIT_NONE ||
				pThis->bADRChangedFlag)
			{
				TRACE_TL1_TIPS("Exit state machine loop");
				TRACE("OutQuitCmd: %d\tInnerQuitCmd: %d\tProtocol Changed "
					"Flag: %d\n", *(pThis->pOutQuitCmd), pThis->iInnerQuitCmd,
					pThis->bADRChangedFlag);

				*piState = TL1_STATE_MACHINE_QUIT;

				/* reset flag */
				if (pThis->bADRChangedFlag)
				{
					pThis->bADRChangedFlag = FALSE;
					pThis->bFrameDone = TRUE;
				}
			}
		  #ifdef _SHOW_TL1_CUR_STATE
             // TL1_PrintState(*piState);
          #endif //_SHOW_TL1_CUR_STATE
          #ifdef _SHOW_TL1_CUR_STATE
			if (*piState != iLastState)
			{
				TRACE_TL1_TIPS("Change to another State");
				TL1_PrintState(*piState);
			}
          #endif //_SHOW_TL1_CUR_STATE
		}
	}

	if (pThis->iInnerQuitCmd != SERVICE_EXIT_NONE)
	{
		return pThis->iInnerQuitCmd;
	}

	return SERVICE_EXIT_OK;
}


/*==========================================================================*
 * FUNCTION : RoutineBeforeExitService
 * PURPOSE  : to do clean work before service exit
 * CALLS    : TL1_ClearEventQueue
 * CALLED BY: ServiceMain
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 15:53
 *==========================================================================*/
static void RoutineBeforeExitService(IN OUT TL1_BASIC_ARGS *pThis)
{
	int							i, j;
	/*HANDLE hMutex;*/

	TL1MODEL_CONFIG_INFO		*pModelConfig;
	//TL1_TYPE_MAP_INFO			*pTypeMap;
	//TL1_ALARM_INFO				*pAlarmBuf;
	TL1_BLOCK_INFO				*pCurBlockInfo = NULL;
	TL1_AID_GROUP_INFO			*pstCurAidGroupInfo = NULL;

	/* 1.unregister in DXI module(it is safe even when not registered) */
	DxiRegisterNotification("TL1 Service", 
							ServiceNotification,
							pThis, 							  
							_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 							  
							pThis->hThreadID[0],							  
							_CANCEL_FLAG);

	/* 2.kill all the timers */
	for (i = 1; i < TL1_TIMER_NUM; i++)
	{
		Timer_Kill(pThis->hThreadID[0], i);
	}

	/* 4.stop child thread(it is safe even when child thread is not running) */
	if (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;
	}
	/* wait 60s at most*/
	TRACE("to stop iLinkLayerThreadExitCmd!!\n");
	int iTemp = RunThread_Stop(pThis->hThreadID[1], 5000, TRUE);
	pThis->hThreadID[1] = NULL;
	TRACE("iTemp1 is %d\n", iTemp);
	TRACE("stop iLinkLayerThreadExitCmd end!!\n");

		/* 5.destroy event queues */
	TL1_ClearEventQueue(pThis->hEventInputQueue, TRUE);
	TL1_ClearEventQueue(pThis->hEventOutputQueue, TRUE);
	TL1_ClearAlarmEventQueue(pThis->hEventAlarmQueue, TRUE);

	/* 6.Free memories */
	/* free memory of TL1ModelConfig */
	pModelConfig = &g_TL1Globals.TL1ModelConfig;
	//pTypeMap = pModelConfig->pTypeMapInfo;
	//for (i = 0; i < pModelConfig->iTypeMapNum; i++, pTypeMap++)
	//{
	//	SAFELY_DELETE(pTypeMap->pMapEntriesInfo);
	//}
	SAFELY_DELETE(pModelConfig->pTypeMapInfo);
	pModelConfig->iTypeMapNum = 0;

	pstCurAidGroupInfo = pModelConfig->pstAIDGroupInfo;
	for (i = 0; i < pModelConfig->iAIDGroupNum; i++, pstCurAidGroupInfo++)
	{
		SAFELY_DELETE(pstCurAidGroupInfo->pMapEntriesInfo);
	}
	SAFELY_DELETE(pModelConfig->pstAIDGroupInfo);
	pModelConfig->iAIDGroupNum = 0;


	/* free memory of TL1ModelInfo */
	pCurBlockInfo = g_TL1Globals.TL1ModelInfo.pBlockInfo;
	for(i = 0; i < g_TL1Globals.TL1ModelInfo.iBlocksNum; i++, pCurBlockInfo++)
	{
		for(j = 0; j < pCurBlockInfo->iTL1EquipNum; j++)
		{
			SAFELY_DELETE(pCurBlockInfo->pstTL1EquipList[j].ppAlmSigValList);
			SAFELY_DELETE(pCurBlockInfo->pstTL1EquipList[j].ppSampSigValList);
			SAFELY_DELETE(pCurBlockInfo->pstTL1EquipList[j].ppSetSigValList);
			SAFELY_DELETE(pCurBlockInfo->pstTL1EquipList[j].piSampSigInfoTypeList);
			SAFELY_DELETE(pCurBlockInfo->pstTL1EquipList[j].piSetSigInfoTypeList);
		}
		SAFELY_DELETE(pCurBlockInfo->pstTL1EquipList);
	}
	SAFELY_DELETE(g_TL1Globals.TL1ModelInfo.pBlockInfo);
	g_TL1Globals.TL1ModelInfo.iBlocksNum = 0;

	/* free memory of TL1_BASIC_ARGS */
	//pAlarmBuf = pThis->AlarmBuffer.pCurAlarmInfo;
	//if (pAlarmBuf != NULL)
	//{
	//	DELETE(pAlarmBuf);
	//}

	//pAlarmBuf = pThis->AlarmBuffer.pHisAlarmInfo;
	//if (pAlarmBuf != NULL)
	//{
	//	DELETE(pAlarmBuf);
	//}

	/* 6.free the Mutex -- Delet for Mutex now is allocated in DXI module */ 
	/*hMutex = g_TL1Globals.hMutexCommonCfg;
	if (hMutex != NULL) 
	{ 
		Mutex_Destroy(hMutex);
		hMutex = NULL;
	}*/

	return;
}


static int TimerProcSessionTimeout(IN HANDLE hTimerOwner,
								   IN int idTimer,
								   IN void *pArgs)
{
	TL1_COMMON_CONFIG		*pstCfg = &g_TL1Globals.CommonConfig;
	TL1_BASIC_ARGS			*pThis = (TL1_BASIC_ARGS *)pArgs;
	TL1_SESSION				*pstSess = &pThis->stSession;

	UNUSED(hTimerOwner);

	if (TL1_SESSION_TIMEOUT_TIMER == idTimer)
	{
		if(pstCfg->iSessionTimeout > 0)//enable session timeout
		{
			if( pstSess->bIsValid )
			{
				if(pstSess->uiInPacketCount > 0)//if we received data, reset timeout
				{
					pstSess->uiTimerTimeoutCount = 0;
				}
				
				if((int)(pstSess->uiTimerTimeoutCount*TL1_SESSION_TIMEOUT_TIMER_INTERVAL)
					<= pstCfg->iSessionTimeout*TL1_C_MIN)
				{
					pstSess->uiTimerTimeoutCount++;
				}
				else
				{
					//printf("TimerProcSessionTimeout\n");
					TL1_DecodeAndPerform(pThis, (unsigned char *)"CANC", FALSE);

					if(TL1_ERR_OK != TL1_SendRespData(pThis, (char *)pThis->szCmdRespBuff, sizeof(pThis->szCmdRespBuff), FALSE, FALSE))
					{
						//TL1_BreakConnection(pThis);
					}

					pstSess->uiTimerTimeoutCount = 0;
					//A user session will be terminated if no input has been received for a period of time equal to 
					//the 'user session timeout' setting. The telnet connection will be terminated 
					//when a user session timeout occurs
					TL1_BreakConnection(pThis);
				}
			}
			else
			{
				pstSess->uiTimerTimeoutCount = 0;
			}

			//reset uiInPacketCount
			pstSess->uiInPacketCount = 0;
		}

		return TIMER_CONTINUE_RUN;
	}

	return TIMER_KILL_THIS;
}



static BOOL SyncCommonCfgToSignals(void)
{
	TL1_COMMON_CONFIG		*pstCfg = &g_TL1Globals.CommonConfig;

	SetEnumSigValue(TL1_CFG_SIG_EQUIP_ID,
		TL1_CFG_SIG_SIGTYPE_ID,
		TL1_CFG_SIG_SIGID_PORT_ACTIVATION,
		pstCfg->iPortActivation,
		LOG_MODIFY_COMMON_CONFIG);

	SetEnumSigValue(TL1_CFG_SIG_EQUIP_ID,
		TL1_CFG_SIG_SIGTYPE_ID,
		TL1_CFG_SIG_SIGID_MEDIA_TYPE,
		pstCfg->iMediaType - TL1_MEDIA_TYPE_TCPIP,
		LOG_MODIFY_COMMON_CONFIG);

	SetDwordSigValue(TL1_CFG_SIG_EQUIP_ID,
		TL1_CFG_SIG_SIGTYPE_ID,
		TL1_CFG_SIG_SIGID_PORT_NUMBER,
		(DWORD )atoi(pstCfg->szCommPortParam),
		LOG_MODIFY_COMMON_CONFIG);

	SetEnumSigValue(TL1_CFG_SIG_EQUIP_ID,
		TL1_CFG_SIG_SIGTYPE_ID,
		TL1_CFG_SIG_SIGID_KEEP_ALIVE,
		pstCfg->iPortKeepAlive,
		LOG_MODIFY_COMMON_CONFIG);

	SetDwordSigValue(TL1_CFG_SIG_EQUIP_ID,
		TL1_CFG_SIG_SIGTYPE_ID,
		TL1_CFG_SIG_SIGID_SESSION_TIMEOUT,
		(DWORD )pstCfg->iSessionTimeout,
		LOG_MODIFY_COMMON_CONFIG);

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SERVICE_ARGUMENTS *  pArgs : 
 * RETURN   : DWORD : service exit code, defined in app_service.h
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-21 19:30
 *==========================================================================*/
#define		GET_SERVICE_OF_TL1_NAME		"tl1.so"

DWORD ServiceMain(IN OUT SERVICE_ARGUMENTS *pArgs)
{
	DWORD					dwExitCode;
	int						iRet;
	const char				*pLogText;
	HANDLE					hInputQueue, hOutputQueue, hAlarmQueue;
	HANDLE					hServiceThread, hLinklayerThread;
	APP_SERVICE		*TL1_AppService = NULL;
    SIG_BASIC_VALUE* pSigValue;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iError;

	TL1_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_TL1_NAME);
	/* clear to zero */
	memset(&g_TL1Globals, 0, sizeof(TL1_GLOBALS));
	memset(&g_stBasicArgs, 0, sizeof(TL1_BASIC_ARGS));

	/* get the thread handle, used to feed watch dog */
	hServiceThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hServiceThread);

	/* 1.initialize g_TL1Globals */
//#ifdef _DEBUG_TL1_SERVICE
	TRACE("enter TL1 ServiceMain\n");
//#endif
	
	dwExitCode = InitTL1Globals();

	if (dwExitCode != SERVICE_EXIT_NONE)
	{
		RoutineBeforeExitService(&g_stBasicArgs);
		return dwExitCode;
	}

	SyncCommonCfgToSignals();

//--- begin CR0201-2020 ---
	// Get signal from System equipment config file
    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, TL1_CFG_SIG_SIGID_AID_DELIMETER);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						1,
						iVarSubID,
						&iBufLen,
						(void *)&pSigValue,
						0);
    if(iError == ERR_DXI_OK)
    {
		g_TL1Globals.CommonConfig.iAidDelimeter = pSigValue->varValue.enumValue;
    }
    else
    {
		g_TL1Globals.CommonConfig.iAidDelimeter = 1;
    }
//--- end CR0201-2020 ---    
    
	/* init the static events */
    InitStaticEvents();

	/* feed watch dog */
	RunThread_Heartbeat(hServiceThread);

	/* 2.init TL1_BASIC_ARGS */
	/* event queues */
	hInputQueue = Queue_Create(TL1_EVENT_QUEUE_SIZE,
		sizeof(TL1_EVENT *), 0);
	hOutputQueue = Queue_Create(TL1_EVENT_QUEUE_SIZE,
		sizeof(TL1_EVENT *), 0);
	hAlarmQueue = Queue_Create(TL1_EVENT_QUEUE_SIZE,
		sizeof(TL1_ALARM_EVENT), 0);

	if (hInputQueue == NULL || hOutputQueue == NULL || hAlarmQueue == NULL)
	{
		AppLogOut(LOG_INIT_SERVICE, APP_LOG_ERROR, "Create Event Queue "
			"failed.\n");
		TRACE("[%s]--%s: ERROR: Create Event Queue failed.\n", __FILE__,
			__FUNCTION__);

		RoutineBeforeExitService(&g_stBasicArgs);
		return SERVICE_EXIT_FAIL;
	}

	g_stBasicArgs.hEventInputQueue = hInputQueue;
	g_stBasicArgs.hEventOutputQueue = hOutputQueue;
	g_stBasicArgs.hEventAlarmQueue = hAlarmQueue;


	/* AlarmBuffer: iCurAlarmNum and iHisAlarmNum cleared to zero already */
	/* alarmHandler: all members cleared to zero already */

	/* get Service thread id */
	g_stBasicArgs.hThreadID[0] = hServiceThread;

	/* flags */
	g_stBasicArgs.iOperationMode = TL1_MODE_SERVER;
	g_stBasicArgs.pOutQuitCmd = &pArgs->nQuitCommand;
	g_stBasicArgs.iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;
	g_stBasicArgs.iInnerQuitCmd = SERVICE_EXIT_NONE;
	g_stBasicArgs.bFrameDone = TRUE;

	/* other flags cleared to zero already */
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread TL1 Service was created, Thread Id = %d.\n", gettid());
#endif	

	/* register the basic agrs */
    pArgs->pReserved = &g_stBasicArgs;

	/* 3.register in the DXI module to receive Alarms */
	iRet = DxiRegisterNotification("TL1 Service", 
							ServiceNotification,
							&g_stBasicArgs, 							  
							_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 							  
							g_stBasicArgs.hThreadID[0],							  
							_REGISTER_FLAG);

	if (iRet != 0)
	{
		pLogText = "Register alarm callback function in DXI module failed.";

		AppLogOut(LOG_INIT_SERVICE, APP_LOG_ERROR, "%s.\n", pLogText);
		TRACE("[%s]--%s: ERROR: %s Call DxiRegisterNotification, returned"
			" %d.\n", __FILE__, __FUNCTION__, pLogText, iRet);

		RoutineBeforeExitService(&g_stBasicArgs);
		return SERVICE_EXIT_FAIL;
	}

	/* 5.create LinkLayer thread */
	if(g_TL1Globals.CommonConfig.iProtocolType != TL1)
	{
		g_stBasicArgs.hThreadID[1] = NULL;
	}
	else
	{
		hLinklayerThread = RunThread_Create("TL1_LINK",
			(RUN_THREAD_START_PROC)TL1_LinkLayerManager,
			&g_stBasicArgs,
			NULL,
			0);

		/* assign the reference out of the Thread to insure valid */
		g_stBasicArgs.hThreadID[1] = hLinklayerThread;

		if (hLinklayerThread == NULL)
		{
			pLogText = "Create link-layer thread failed.";

			AppLogOut(LOG_INIT_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
			TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);

			RoutineBeforeExitService(&g_stBasicArgs);
			return SERVICE_EXIT_FAIL;
		}
	}
#ifdef _TEST_TL1_MODIFYCONFIG
	{

		int iRet, nVarID;
		BOOL bServiceIsRunning = TRUE;
		TL1_COMMON_CONFIG UserCfg;

		//Sleep(5000);
		UserCfg.byADR = 66;
		UserCfg.iProtocolType = TL1;
		UserCfg.iMediaType = TL1_MEDIA_TYPE_TCPIP;
		strcpy(UserCfg.szCommPortParam, "100.200.1.1:12001");
		UserCfg.iPortActivation = 88;
		UserCfg.iPortKeepAlive = 99;
		UserCfg.iSessionTimeout = 999;
		strcpy(UserCfg.szAutoLoginUser, "tl1 user");
		strcpy(UserCfg.szSystemIdentifier, "test TL1 system");
		strcpy(UserCfg.cModifyUser, "debug");


		nVarID = TL1_CFG_W_MODE | TL1_CFG_ALL;
		iRet = TL1_ModifyCommonCfg(bServiceIsRunning,
						FALSE,
						&g_stBasicArgs,
						nVarID,
						&UserCfg);

		if (iRet != ERR_SERVICE_CFG_OK)
		{
			TRACE("Change Config file failed\n");
			TRACE("Return Value is: %d\n", iRet);
		}

		TL1_PrintCommonCfg(&UserCfg);
		TL1_PrintCommonCfg(&g_TL1Globals.CommonConfig);
	}
#endif //_TEST_TL1_MODIFYCONFIG

	iRet = Timer_Set(g_stBasicArgs.hThreadID[0],
		TL1_SESSION_TIMEOUT_TIMER,
		TL1_SESSION_TIMEOUT_TIMER_INTERVAL*1000,
		TimerProcSessionTimeout,
		(DWORD)&g_stBasicArgs);
	if (iRet == ERR_TIMER_EXISTS)
	{
		TRACE_TL1_TIPS("SESSION_TIMEOUT_TIMER has existed");
	}
	else if (iRet == ERR_TIMER_SET_FAIL)
	{
		AppLogOut(LOG_SERVICE, APP_LOG_ERROR, "Timer_Set SESSION_TIMEOUT_TIMER failed.\n");
		TRACE("[%s]--%s: ERROR:Timer_Set SESSION_TIMEOUT_TIMER failed.\n", __FILE__, __FUNCTION__);
		return SERVICE_EXIT_FAIL;
	}
	if(TL1_AppService != NULL)
	{
		TL1_AppService->bReadyforNextService = TRUE;
	}
	/* 5.begin main state machine loop */
	TRACE_TL1_TIPS("Begin state machine");
	dwExitCode = StateMachineLoop(&g_stBasicArgs);


	/* 6.do the clean work */
	RoutineBeforeExitService(&g_stBasicArgs);

	return dwExitCode;
}


/*==========================================================================*
 * FUNCTION : CheckUserConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID    : 
 *            TL1_COMMON_CONFIG  *pUserCfg : 
 *            TL1_COMMON_CONFIG  *pOriCfg  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 16:39
 *==========================================================================*/
static BOOL CheckUserConfig(IN int nVarID, 
							IN OUT TL1_COMMON_CONFIG *pUserCfg,
							IN TL1_COMMON_CONFIG *pOriCfg)
{
	int						iTemp;

	/* prepare for check accordance between config items */
	pUserCfg->iModuleSwitch = pOriCfg->iModuleSwitch;
	pUserCfg->iProtocolType = pOriCfg->iProtocolType;
	pUserCfg->byADR = pOriCfg->byADR;

	if( !(nVarID & TL1_CFG_ALL) && !(nVarID & TL1_CFG_MEDIA_TYPE))
	{
		pUserCfg->iMediaType = pOriCfg->iMediaType;
	}

	if( !(nVarID & TL1_CFG_ALL) && !(nVarID & TL1_CFG_MEDIA_PORT_PARAM))
	{
		strncpyz(pUserCfg->szCommPortParam, pOriCfg->szCommPortParam, sizeof(pUserCfg->szCommPortParam));
	}

	if( !(nVarID & TL1_CFG_ALL) && !(nVarID & TL1_CFG_PORT_ACTIVATION))
	{
		pUserCfg->iPortActivation = pOriCfg->iPortActivation;
	}

	if( !(nVarID & TL1_CFG_ALL) && !(nVarID & TL1_CFG_PORT_KEEP_ALIVE))
	{
		pUserCfg->iPortKeepAlive = pOriCfg->iPortKeepAlive;
	}

	if( !(nVarID & TL1_CFG_ALL) && !(nVarID & TL1_CFG_SESSION_TIMEOUT))
	{
		pUserCfg->iSessionTimeout = pOriCfg->iSessionTimeout;
	}

	if( !(nVarID & TL1_CFG_ALL) && !(nVarID & TL1_CFG_AUTO_LOGIN_USER))
	{
		strncpyz(pUserCfg->szAutoLoginUser, pOriCfg->szAutoLoginUser, sizeof(pUserCfg->szAutoLoginUser));
	}

	if( !(nVarID & TL1_CFG_ALL) && !(nVarID & TL1_CFG_SYSTEM_IDENTIFIER))
	{
		strncpyz(pUserCfg->szSystemIdentifier, pOriCfg->szSystemIdentifier, sizeof(pUserCfg->szSystemIdentifier));
	}
	
	/* 1.check protocol type and the accordance */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_PROTOCOL_TYPE)
	{
		if (pUserCfg->iProtocolType < EEM_TL1 || 
			pUserCfg->iProtocolType >= TL1_PROTOCOL_NUM)
		{
			return FALSE;
		}
	}

   //TRACE("\npUserCfg4->byADR is: %d\n", pUserCfg->byADR);
  // TRACE("\nnVarID is: %d\n", nVarID);

	/* 2.check ADR -- always true due to data type */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_ADR)
	{
		if (pUserCfg->byADR < 1 || pUserCfg->byADR > 255)
		{
			return FALSE;
		}
	}

	/* 4.check operation media and comm port param */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_MEDIA_TYPE || 
		  nVarID & TL1_CFG_MEDIA_PORT_PARAM)
	{
		if ((pUserCfg->iMediaType < TL1_MEDIA_TYPE_TCPIP) 
			|| (pUserCfg->iMediaType >= TL1_MEDIA_TYPE_NUM))
		{
			return FALSE;
		}

		switch (pUserCfg->iMediaType)
		{
		case TL1_MEDIA_TYPE_LEASED_LINE:
			if ((!TL1_CheckBautrateCfg(pUserCfg->szCommPortParam))||(!TL1_CheckSPortParam(pUserCfg->szCommPortParam)))
			{
				return FALSE;
			}
			break;

		case TL1_MEDIA_TYPE_MODEM:
			//if (!TL1_CheckModemCfg(pUserCfg->szCommPortParam))
		    if ((!TL1_CheckBautrateCfg(pUserCfg->szCommPortParam))||(!TL1_CheckSPortParam(pUserCfg->szCommPortParam)))
			{
				return FALSE;
			}
			break;

		case TL1_MEDIA_TYPE_TCPIP:
			if (!TL1_CheckIPAddress(pUserCfg->szCommPortParam))
			{
				return FALSE;
			}
			iTemp = atoi(pUserCfg->szCommPortParam);
			if(iTemp < TL1_MIN_VAL_PORT_NUMBER || iTemp > TL1_MAX_VAL_PORT_NUMBER)
			{
				return FALSE;
			}

			break;

		case TL1_MEDIA_TYPE_TCPIPV6:
			if (!TL1_CheckIPAddress(pUserCfg->szCommPortParam))
			{
				return FALSE;
			}
			iTemp = atoi(pUserCfg->szCommPortParam);
			if(iTemp < TL1_MIN_VAL_PORT_NUMBER || iTemp > TL1_MAX_VAL_PORT_NUMBER)
			{
				return FALSE;
			}

			break;
		}
	}

	/* 5.check [Port Activation] */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_PORT_ACTIVATION)
	{
		if (pUserCfg->iPortActivation != TL1_DISABLE_VALUE)
		{
			pUserCfg->iPortActivation = TL1_ENABLE_VALUE;
		}
	}

	/* 6.check [Port keep alive] */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_PORT_KEEP_ALIVE)
	{
		if (pUserCfg->iPortKeepAlive != TL1_DISABLE_VALUE)
		{
			pUserCfg->iPortKeepAlive = TL1_ENABLE_VALUE;
		}
	}

	/* 7.check [Session timeout] */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_SESSION_TIMEOUT)
	{
		if (pUserCfg->iSessionTimeout < TL1_MIN_VAL_SESSION_TIMEOUT
			|| pUserCfg->iSessionTimeout > TL1_MAX_VAL_SESSION_TIMEOUT)
		{
			return FALSE;
		}
	}

	/* 8.check [Auto login user] */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_AUTO_LOGIN_USER)
	{
		if (NULL != strchr(pUserCfg->szAutoLoginUser, '#'))//include char '#'
		{
			return FALSE;
		}
	}

	/* 9.check [System identifier] */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_SYSTEM_IDENTIFIER)
	{
		if (NULL != strchr(pUserCfg->szSystemIdentifier, '#'))//include char '#'
		{
			return FALSE;
		}
	}

	/* 10.check TL1_CFG_AID_GROUP_INFO */
	if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_AID_GROUP_INFO)
	{
		if(pUserCfg->stAidGroupInfoForWrite.stAIDGroupInfo.iIndex <= 0)
		{
			return FALSE;
		}

		if(TL1_CFG_MODIFY_TYPE_ONLY_AID_GROUP == pUserCfg->stAidGroupInfoForWrite.iModifyType)
		{
			if(TL1_CHAR_NULL == pUserCfg->stAidGroupInfoForWrite.stAIDGroupInfo.szAidGroupName[0])
			{
				return FALSE;
			}
		}
		else if(TL1_CFG_MODIFY_TYPE_ONLY_MAPENTRIES == pUserCfg->stAidGroupInfoForWrite.iModifyType)
		{

		}
		else if(TL1_CFG_MODIFY_TYPE_AID_GROUP_AND_MAPENTRIES == pUserCfg->stAidGroupInfoForWrite.iModifyType)
		{
			if(TL1_CHAR_NULL == pUserCfg->stAidGroupInfoForWrite.stAIDGroupInfo.szAidGroupName[0])
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}

	}

	return TRUE;
}


/* for copy TL1_COMMON_CONFIG structure, used as inline function */
#define TL1_COPY_COMMON_CONFIG(_src, _dst) \
   { \
   (_dst).iModuleSwitch = (_src).iModuleSwitch;  \
   (_dst).byADR = (_src).byADR;  \
   (_dst).iProtocolType = (_src).iProtocolType;  \
   (_dst).iMediaType = (_src).iMediaType;  \
   strcpy((_dst).szCommPortParam, (_src).szCommPortParam);  \
   (_dst).iPortActivation = (_src).iPortActivation;  \
   (_dst).iPortKeepAlive = (_src).iPortKeepAlive;  \
   (_dst).iSessionTimeout = (_src).iSessionTimeout;  \
   strcpy((_dst).szAutoLoginUser, (_src).szAutoLoginUser);  \
   strcpy((_dst).szSystemIdentifier, (_src).szSystemIdentifier);  \
   }

/*==========================================================================*
 * FUNCTION : UpdateNormalCfgItems
 * PURPOSE  : assistant function for ServiceConfig
 * CALLS    : 
 * CALLED BY: ServiceConfig
 * ARGUMENTS: int            nVarID    : 
 *            TL1_COMMON_CONFIG  *pOriCfg  : 
 *            TL1_COMMON_CONFIG  *pUserCfg : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 16:27
 *==========================================================================*/
static void UpdateNormalCfgItems(IN int nVarID,
								OUT TL1_COMMON_CONFIG *pOriCfg, 
								IN  TL1_COMMON_CONFIG *pUserCfg)
{
	if (nVarID & TL1_CFG_ALL)
	{
		TL1_COPY_COMMON_CONFIG(*pUserCfg, *pOriCfg);
		return;
	}

	if (nVarID & TL1_CFG_ADR)
	{
		pOriCfg->byADR = pUserCfg->byADR;
	}

	if (nVarID & TL1_CFG_PROTOCOL_TYPE)
	{
		pOriCfg->iProtocolType = pUserCfg->iProtocolType;
	}

	if (nVarID & TL1_CFG_MEDIA_TYPE)
	{
		pOriCfg->iMediaType = pUserCfg->iMediaType;
	}

	if (nVarID & TL1_CFG_MEDIA_PORT_PARAM)
	{
		strncpyz(pOriCfg->szCommPortParam, pUserCfg->szCommPortParam, sizeof(pOriCfg->szCommPortParam));
	}

	if (nVarID & TL1_CFG_PORT_ACTIVATION)
	{
		pOriCfg->iPortActivation = pUserCfg->iPortActivation;
	}

	if (nVarID & TL1_CFG_PORT_KEEP_ALIVE)
	{
		pOriCfg->iPortKeepAlive = pUserCfg->iPortKeepAlive;
	}

	if (nVarID & TL1_CFG_SESSION_TIMEOUT)
	{
		pOriCfg->iSessionTimeout = pUserCfg->iSessionTimeout;
	}

	if (nVarID & TL1_CFG_AUTO_LOGIN_USER)
	{
		strncpyz(pOriCfg->szAutoLoginUser, pUserCfg->szAutoLoginUser, sizeof(pOriCfg->szAutoLoginUser));
	}

	if (nVarID & TL1_CFG_SYSTEM_IDENTIFIER)
	{
		strncpyz(pOriCfg->szSystemIdentifier, pUserCfg->szSystemIdentifier, sizeof(pOriCfg->szSystemIdentifier));
	}

	return;
}


/*==========================================================================*
* FUNCTION :  TL1_ModifySignalAlarmLevel
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int equipID:
IN int signalType:
IN int signalID:
IN int alarmLevel:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
*==========================================================================*/

static int TL1_ModifySignalAlarmLevel(IN int iEquipTypeID,
									IN int iSignalType,
									IN int iSignalID,
									IN int iAlarmLevel,
									IN char *szWriteUserName)
{
	SET_A_SIGNAL_INFO_STU		stSetASignalInfo;
	int							iError	= 0;
	int							iBufLen = 0;
	int							iVarSubID	= DXI_MERGE_SIG_ID(iSignalType,iSignalID);


	strncpyz(stSetASignalInfo.cModifyUser, szWriteUserName, strlen(szWriteUserName) + 1);
	stSetASignalInfo.byModifyType		= MODIFY_ALARM_LEVEL;
	stSetASignalInfo.bModifyAlarmLevel	= iAlarmLevel;
	iBufLen = sizeof(SET_A_SIGNAL_INFO_STU);

	iError = DxiSetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
						iEquipTypeID,
						iVarSubID,
						iBufLen,
						(void *)&stSetASignalInfo,
						0);

	//if (ERR_DXI_OK != iError)
	//{
	//	AppLogOut(LOG_SERVICE, APP_LOG_WARNING, "Modify signal alarm level fail\n");
	//}

	return iError;
}


static BOOL TL1_UpdateAlarmLevel(IN TL1_COMMON_CONFIG *pUserCfg)
{
	TL1MODEL_CONFIG_INFO		*pModelCfg = &g_TL1Globals.TL1ModelConfig;
	TL1_AID_GROUP_INFO_W		*pstAidGroupInfoForWrite = &pUserCfg->stAidGroupInfoForWrite;
	TL1_MAPENTRIES_INFO			*pCurMapEntriesInfoFromUserCfg = NULL;
	TL1_MAPENTRIES_INFO			*pCurMapEntriesInfoFromOriCfg = NULL;
	int							i, j, iRet;
	char						szLogText[100];


	if((TL1_CFG_MODIFY_TYPE_ONLY_MAPENTRIES == pstAidGroupInfoForWrite->iModifyType)
		|| (TL1_CFG_MODIFY_TYPE_AID_GROUP_AND_MAPENTRIES == pstAidGroupInfoForWrite->iModifyType))
	{
		//input data is valid
		if((pstAidGroupInfoForWrite->stAIDGroupInfo.iMapEntriesNum > 0)
			&& (NULL != pstAidGroupInfoForWrite->stAIDGroupInfo.pMapEntriesInfo))
		{
			pCurMapEntriesInfoFromUserCfg = &pstAidGroupInfoForWrite->stAIDGroupInfo.pMapEntriesInfo[0];

			if(pCurMapEntriesInfoFromUserCfg->iEquipTypeID <= 0)
			{
				printf("pCurMapEntriesInfoFromUserCfg->iEquipTypeID=%d\n", pCurMapEntriesInfoFromUserCfg->iEquipTypeID);
				return TRUE;
			}

			//find the item MapEntriesInfoFromOriCfg which will be modified
			pCurMapEntriesInfoFromOriCfg = NULL;
			for(i = 0; i < pModelCfg->iAIDGroupNum; i++)
			{
				if(pModelCfg->pstAIDGroupInfo[i].iIndex == pstAidGroupInfoForWrite->stAIDGroupInfo.iIndex)
				{
					for(j = 0; j < pModelCfg->pstAIDGroupInfo[i].iMapEntriesNum; j++)
					{
						if(pModelCfg->pstAIDGroupInfo[i].pMapEntriesInfo[j].iIndex == pCurMapEntriesInfoFromUserCfg->iIndex)
						{
							pCurMapEntriesInfoFromOriCfg = &pModelCfg->pstAIDGroupInfo[i].pMapEntriesInfo[j];
							break;
						}
					}

					if(NULL != pCurMapEntriesInfoFromOriCfg)
					{
						break;
					}
				}
			}

			//find the right item, and start to modify it
			if(NULL != pCurMapEntriesInfoFromOriCfg)
			{
				iRet = TL1_ModifySignalAlarmLevel(pCurMapEntriesInfoFromUserCfg->iEquipTypeID,
					SIG_TYPE_ALARM,
					pCurMapEntriesInfoFromOriCfg->stAlarmSig.iSigID,
					pCurMapEntriesInfoFromUserCfg->iNotificationCode,
					LOG_SERVICE);


				if(ERR_DXI_OK == iRet)
				{
					return TRUE;
				}

				snprintf(szLogText,
					sizeof(szLogText),
					"Write alarm level failed iEquipTypeID=%d, iSigID=%d, level=%d",
					pCurMapEntriesInfoFromOriCfg->iEquipTypeID,
					pCurMapEntriesInfoFromOriCfg->stAlarmSig.iSigID,
					pCurMapEntriesInfoFromOriCfg->iNotificationCode);
				AppLogOut(LOG_SERVICE, APP_LOG_ERROR, "%s\n", szLogText);
			}
		}

		return FALSE;

	}//for setting alarm level
	else
	{
		return TRUE;
	}
}

/*==========================================================================*
 * FUNCTION : WriteTL1CfgToFlash
 * PURPOSE  : 
 * CALLS    : TL1_GetModifiedFileBuf
 * CALLED BY: 
 * ARGUMENTS: int            nVarID : 
 *            TL1_COMMON_CONFIG  *pCfg  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 17:23
 *==========================================================================*/
/* used interface defined in config_builder.c */
BOOL TL1_GetModifiedFileBuf(IN int nVarID, 
							IN TL1_COMMON_CONFIG *pUserCfg,
							OUT char **pszOutFile);

BOOL TL1_GetModifiedFileBufForModelMap(IN int nVarID, 
									IN TL1_COMMON_CONFIG *pUserCfg,
									OUT char **pszOutFile);

BOOL TL1_UpdateCommonConfigFile(IN const char *szContent);
BOOL TL1_UpdateModelMapFile(IN const char *szContent);


static BOOL WriteTL1CfgToFlash(IN int nVarID, IN TL1_COMMON_CONFIG *pUserCfg)
{
	HANDLE						hMutex;
	char						*szOutFile;
	char						*pLogText;

	/* to simplify log action */
#define TASK_NAME        "Modify TL1 Config File"
#define LOG_MODIFY_FILE(_pLogText)  \
	(AppLogOut(TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	hMutex = DXI_GetESRCommonCfgMutex();

	if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_COMCFG) == ERR_MUTEX_OK)
	{
		//TL1CommonConfig.cfg
		/* create new file in memory first */
		if (!TL1_GetModifiedFileBuf(nVarID, pUserCfg, &szOutFile))
		{
			pLogText = "Failed to create new TL1 common config file in memory.";
			LOG_MODIFY_FILE(pLogText);

			DELETE(szOutFile);
			Mutex_Unlock(hMutex);
			return FALSE;
		}

		/* write to flash */
		if (!TL1_UpdateCommonConfigFile(szOutFile))
		{
			pLogText = "Write the new TL1 common config file to flash failed.";
			LOG_MODIFY_FILE(pLogText);

			DELETE(szOutFile);
			Mutex_Unlock(hMutex);
			return FALSE;
		}

		//free buffer
		SAFELY_DELETE(szOutFile);


		//for TL1ModelMap.cfg
		if (nVarID & TL1_CFG_ALL || nVarID & TL1_CFG_AID_GROUP_INFO)
		{
			/* create new file in memory first */
			if (!TL1_GetModifiedFileBufForModelMap(nVarID, pUserCfg, &szOutFile))
			{
				pLogText = "Failed to create new TL1 model map file in memory.";
				LOG_MODIFY_FILE(pLogText);

				DELETE(szOutFile);
				Mutex_Unlock(hMutex);
				return FALSE;
			}

			/* write to flash */
			if (!TL1_UpdateModelMapFile(szOutFile))
			{
				pLogText = "Write the new TL1 model map file to flash failed.";
				LOG_MODIFY_FILE(pLogText);

				DELETE(szOutFile);
				Mutex_Unlock(hMutex);
				return FALSE;
			}

			//free buffer
			SAFELY_DELETE(szOutFile);

			//setting alarm level(notification code) after setting config file successfully
			if( !TL1_UpdateAlarmLevel(pUserCfg) )
			{
				pLogText = "Failed to update alarm level.";
				LOG_MODIFY_FILE(pLogText);

				Mutex_Unlock(hMutex);
				return FALSE;
			}
		}
	}
	else
	{
		pLogText = "Wait for Mutex of Common Config file timeout.";
		LOG_MODIFY_FILE(pLogText);

		return FALSE;
	}

	Mutex_Unlock(hMutex);
	return TRUE;
}

__INLINE static BOOL TL1_IsCommBusy(IN OUT TL1_BASIC_ARGS *pThis)
{
	BOOL bRet = pThis->bCommRunning;

	/* for leased line, when stay in TL1_IDLE or SOC_OFF as Server Mode, we 
	 * think communication is not running. 
	 */
	/*TRACE("g_TL1Globals.CommonConfig.iMediaType is %d\n", g_TL1Globals.CommonConfig.iMediaType);
	TRACE("pThis->iMachineState is %d\n", pThis->iMachineState);
	TRACE("pThis->iOperationMode is %d\n", pThis->iOperationMode);*/
	if (g_TL1Globals.CommonConfig.iMediaType == TL1_MEDIA_TYPE_LEASED_LINE)
		//|| g_TL1Globals.CommonConfig.iMediaType == TL1_MEDIA_TYPE_MODEM)
	{
		if (!pThis->iMachineState && pThis->iOperationMode == TL1_MODE_SERVER)
		{
			bRet = FALSE;
		}
	}
	else if(g_TL1Globals.CommonConfig.iMediaType == TL1_MEDIA_TYPE_MODEM)
	{
		if(pThis->iOperationMode == TL1_MODE_CLIENT)
			bRet = TRUE;
	}
    TRACE("pThis->bCommRunning is %d\n", pThis->bCommRunning);
	return bRet;
}


/*==========================================================================*
 * FUNCTION : TL1_ModifyCommonCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ServiceConfig
 * ARGUMENTS: BOOL bServiceIsRunning    :
 *			  BOOL bByForce				: ignore CommBusy restriction
 *			  TL1_BASIC_ARGS  *pThis    : 
 *            int             iItemID   : 
 *            TL1_COMMON_CONFIG   *pUserCfg : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-10 15:53
 *==========================================================================*/
int TL1_ModifyCommonCfg(IN BOOL bServiceIsRunning,
						IN BOOL bByForce,
						IN OUT TL1_BASIC_ARGS *pThis,
						IN int iItemID,
						IN OUT TL1_COMMON_CONFIG *pUserCfg)
{
	TL1_COMMON_CONFIG		*pOriCfg = NULL;
	HANDLE					hLinklayerThread;
	TL1_COMMON_CONFIG		stTempOriComCfg;
	char					*pLogText = NULL;
	TL1MODEL_CONFIG_INFO	*pstModelConfig;

	/* original common config */
	pOriCfg = &g_TL1Globals.CommonConfig;
	pstModelConfig = &g_TL1Globals.TL1ModelConfig;

	//store original common config for recording config change to log
	memcpy(&stTempOriComCfg, pOriCfg, sizeof(stTempOriComCfg));

	
	/* read config */
	if (!(iItemID & TL1_CFG_W_MODE))
	{
		TL1_COPY_COMMON_CONFIG(*pOriCfg, *pUserCfg);

		pUserCfg->stAidGroupInfoForRead.iAIDGroupNum = pstModelConfig->iAIDGroupNum;
		pUserCfg->stAidGroupInfoForRead.pstAIDGroupInfo = pstModelConfig->pstAIDGroupInfo;

		return ERR_SERVICE_CFG_OK;
	}

	//TRACE("pUserCfg->iAttemptElapse is %d\n", pUserCfg->iAttemptElapse);
	/* update config */
	if (!bByForce && TL1_IsCommBusy(pThis))
	{
		return ERR_SERVICE_CFG_BUSY;
	}

	if((iItemID & TL1_CFG_ALL))
	{
		iItemID = 0xFFFFFFFF;
	}
	/* check data first */
	if (!CheckUserConfig(iItemID, pUserCfg, pOriCfg))
	{
		return ERR_SERVICE_CFG_DATA;
	}
	/* check if the non-shared com has been occupied already */
#ifdef COM_SHARE_SERVICE_SWITCH
	{
		HANDLE hMutex;
		SERVICE_SWITCH_FLAG *pFlag;

		/* note: MediaType value of pUserCfg has been updated 
		in CheckUserConfig function */
		if (iItemID & TL1_CFG_MEDIA_TYPE || iItemID & TL1_CFG_ALL ||
			iItemID & TL1_CFG_MEDIA_PORT_PARAM)
		{
			pFlag = DXI_GetServiceSwitchFlag();

			if (TL1_IS_SHARE_COM(pUserCfg->iMediaType))
			{
				hMutex = DXI_GetServiceSwitchMutex();
				if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_SWITCH) == ERR_MUTEX_OK)
				{
					if (pFlag->bMtnCOMHasClient)
					{
						Mutex_Unlock(hMutex);
						return ERR_SERVICE_CFG_TL1_COMUSED;
					}
					else
					{
						pFlag->bHLMSUseCOM = TRUE;
						Mutex_Unlock(hMutex);
					}
				}
				else
				{
					return ERR_SERVICE_CFG_TL1_COMUSED;
				}
			}
			else
			{
				pFlag->bHLMSUseCOM = FALSE;
			}
		}

	}
#endif //COM_SHARE_SERVICE_SWITCH
	/* write to flash then */
	/*
	TRACE("pUserCfg->byADR is %d\n", pUserCfg->byADR);
	TRACE("pUserCfg->iProtocolType is %d\n", pUserCfg->iProtocolType);
	TRACE("pUserCfg->iMediaType is %d\n", pUserCfg->iMediaType);
	TRACE("pUserCfg->szCommPortParam is %s\n", pUserCfg->szCommPortParam);
	*/

	if (!WriteTL1CfgToFlash(iItemID, pUserCfg))
	{
		return ERR_SERVICE_CFG_FLASH;
	}

	/* now process in the memory */
	if (bServiceIsRunning)
	{
  //      /* Address updated */
		//if ((iItemID & TL1_CFG_ADR) ||
		//	(iItemID & TL1_CFG_ALL))
		//{
		//	/* changed */
		//	if (pOriCfg->byADR != pUserCfg->byADR)
		//	{
		//		pOriCfg->byADR = pUserCfg->byADR;
		//		pThis->bADRChangedFlag = TRUE;
		//	}
		//	
		//}

		///* protocol type updated */
		//if ((iItemID & TL1_CFG_PROTOCOL_TYPE) ||
		//	(iItemID & TL1_CFG_ALL))
		//{
		//	/* changed */
		//	if (pOriCfg->iProtocolType != pUserCfg->iProtocolType)
		//	{
		//		pOriCfg->iProtocolType = pUserCfg->iProtocolType;
		//		pThis->bProtocolChangedFlag = TRUE;
		//	}
		//}


		/* link-layer media updated */
		//if ((iItemID & TL1_CFG_MEDIA_TYPE) || (iItemID & TL1_CFG_ALL) ||
		//	(iItemID & TL1_CFG_MEDIA_PORT_PARAM))
			
		{
			/* changed */
			//if (pOriCfg->iMediaType != pUserCfg->iMediaType || 
			//	strcmp(pOriCfg->szCommPortParam, 
			//	pUserCfg->szCommPortParam) != 0)
			//TRACE("----------------------------Into create link-layer---------------------\n");
			//{		

				//a. stop LinkLayerThread
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;
				//Sleep(3000);
				/* wait 20s */
				if(pThis->hThreadID[1] != NULL)
				{
					RunThread_Stop(pThis->hThreadID[1], 20000, TRUE);
				}

				//b. stop state machine
				pThis->bProtocolChangedFlag = TRUE;

				//wait for state machine stop running
				while(TRUE == pThis->bProtocolChangedFlag)
				{
					Sleep(300);
				}

				//c. update config
				UpdateNormalCfgItems(iItemID, pOriCfg, pUserCfg);

				//update block data in TL1_MODEL_INFO
				if (iItemID & TL1_CFG_ALL || iItemID & TL1_CFG_AID_GROUP_INFO)
				{
					TL1_UpdateBlocksData();
				}

				//recreate

				//d. start state machine
				//state machine will auto start after the pThis->bProtocolChangedFlag become FALSE

				//f. start LinkLayerThread
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;
				/* restart link-layer thread */
				//if(pOriCfg->iProtocolType == TL1)
				//{
					hLinklayerThread = RunThread_Create("TL1_LINK",
						(RUN_THREAD_START_PROC)TL1_LinkLayerManager,
						pThis,
						NULL,
						0);
					if (hLinklayerThread == NULL)
					{
						pLogText = "Create link-layer thread failed.";
	
						AppLogOut(LOG_INIT_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
						TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);
	
						pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
						return ERR_SERVICE_CFG_EXIT;
					}
				

					pThis->hThreadID[1] = hLinklayerThread;
				//}
				//else				
				//{

				//	if(pThis->hThreadID[1])
				//	{
				//		RunThread_Stop(pThis->hThreadID[1], 
				//						10000, TRUE);
				//		
				//	}
				//	pThis->hThreadID[1] = NULL;
				//	
				//}
			//}/* end of changed */
		}/* end of link-layer media updated */
		/* for other config item, just modify values */
		//UpdateNormalCfgItems(iItemID, pOriCfg, pUserCfg);
	}

    char logText[TL1_LOG_TEXT_LEN];

    TRACE("iItemID is %d [%d]\n", iItemID, bServiceIsRunning);

    if(iItemID & TL1_CFG_ADR)
    {
        sprintf(logText, "Modify %s from %d to %d%s%s",
			"Address",
			stTempOriComCfg.byADR,
			pUserCfg->byADR,
			" by ",
			pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', TL1_LOG_TEXT_LEN);
    }

    if(iItemID & TL1_CFG_PROTOCOL_TYPE)
    {
	    int iProtocolNum,iUserProtocolNum;
	    if((stTempOriComCfg.iProtocolType>0)&&(stTempOriComCfg.iProtocolType<7))
	    {
		iProtocolNum = stTempOriComCfg.iProtocolType - 1;
	    }
	    else
	    {
		iProtocolNum = TL1-1;
	    }
	    if((pUserCfg->iProtocolType>0)&&(pUserCfg->iProtocolType<7))
	    {
		    iUserProtocolNum =pUserCfg->iProtocolType-1;
	    }
	    else
	    {
		    iUserProtocolNum = TL1-1;
	    }

		char protocolType[][20] = { {"EEM"}, {"RSOC"}, {"SOC_TPE"}, {"YDN23"}, {"MODBUS"}, {"TL1"}};
	    
        sprintf(logText, "Modify %s from %s to %s%s%s",
			"Protocol Type",
			protocolType[iProtocolNum],
        	protocolType[iUserProtocolNum],
			" by ",
			pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', TL1_LOG_TEXT_LEN);
    }

    if(iItemID & TL1_CFG_MEDIA_TYPE)
    {
	    char mediaType[][20] = {{"LEASED_LINE"}, {"MODEM"}, {"TCPIP"}, {"TCPIP V6"}};
	    
        sprintf(logText, "Modify %s from %s to %s%s%s",
			"Media Type",
			mediaType[stTempOriComCfg.iMediaType],
        	mediaType[pUserCfg->iMediaType],
			" by ",
			pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', TL1_LOG_TEXT_LEN);
    }

    if(iItemID & TL1_CFG_MEDIA_PORT_PARAM)
    {
        sprintf(logText, "Modify %s from %s to %s%s%s",
			"Media Port Parameter",
			stTempOriComCfg.szCommPortParam,
        	pUserCfg->szCommPortParam,
			" by ",
			pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', TL1_LOG_TEXT_LEN);
    }

	if(iItemID & TL1_CFG_PORT_ACTIVATION)
	{
		char szaEnum1[2][20] = {{"Disabled"}, {"Enabled"}};

		sprintf(logText, "Modify %s from %s to %s%s%s",
			"[Port Activation]",
			szaEnum1[stTempOriComCfg.iPortActivation], 
			szaEnum1[pUserCfg->iPortActivation],
			" by ",
			pUserCfg->cModifyUser);

		AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
		memset(logText, '\0', TL1_LOG_TEXT_LEN);
	}

	if(iItemID & TL1_CFG_PORT_KEEP_ALIVE)
	{
		char szaEnum1[2][20] = {{"Disabled"}, {"Enabled"}};

		sprintf(logText, "Modify %s from %s to %s%s%s",
			"[Port keep alive]",
			szaEnum1[stTempOriComCfg.iPortKeepAlive], 
			szaEnum1[pUserCfg->iPortKeepAlive],
			" by ",
			pUserCfg->cModifyUser);

		AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
		memset(logText, '\0', TL1_LOG_TEXT_LEN);
	}

	if(iItemID & TL1_CFG_SESSION_TIMEOUT)
	{
		sprintf(logText, "Modify %s from %d to %d%s%s",
			"[Session timeout]",
			stTempOriComCfg.iSessionTimeout, 
			pUserCfg->iSessionTimeout,
			" by ",
			pUserCfg->cModifyUser);

		AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
		memset(logText, '\0', TL1_LOG_TEXT_LEN);
	}

	if(iItemID & TL1_CFG_AUTO_LOGIN_USER)
	{
		sprintf(logText, "Modify %s from %s to %s%s%s",
			"[Auto login user]",
			(TL1_CHAR_NULL == stTempOriComCfg.szAutoLoginUser[0]) ? "None": stTempOriComCfg.szAutoLoginUser,
			(TL1_CHAR_NULL == pUserCfg->szAutoLoginUser[0]) ? "None": pUserCfg->szAutoLoginUser,
			" by ",
			pUserCfg->cModifyUser);

		AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
		memset(logText, '\0', TL1_LOG_TEXT_LEN);
	}


	if(iItemID & TL1_CFG_SYSTEM_IDENTIFIER)
	{
		sprintf(logText, "Modify %s from %s to %s%s%s",
			"[System identifier]",
			stTempOriComCfg.szSystemIdentifier, 
			pUserCfg->szSystemIdentifier,
			" by ",
			pUserCfg->cModifyUser);

		AppLogOut(LOG_MODIFY_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
		memset(logText, '\0', TL1_LOG_TEXT_LEN);
	}

	return ERR_SERVICE_CFG_OK;
}




/*==========================================================================*
 * FUNCTION : ServiceConfig
 * PURPOSE  : for common config info access
 * CALLS    : TL1_ModifyCommonCfg
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService : 
 *            SERVICE_ARGUMENTS * pArgs:
 *            int     nVarID   : 
 *            int    *nBufLen  : 
 *            void   *pDataBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 16:37
 *==========================================================================*/
int ServiceConfig(HANDLE hService, 
				  BOOL bServiceIsRunning,
				  SERVICE_ARGUMENTS *pArgs, 
				  int nVarID,
				  int *nBufLen, 
				  void *pDataBuf)
{

	UNUSED(hService);
	UNUSED(nBufLen);

	int iRtn = 0;
	iRtn = TL1_ModifyCommonCfg(bServiceIsRunning,
		   TRUE,
		   (TL1_BASIC_ARGS *)pArgs->pReserved,
		   nVarID, (TL1_COMMON_CONFIG *)pDataBuf);
	
	if(ERR_SERVICE_CFG_OK == iRtn)
	{
		SyncCommonCfgToSignals();
	}


	return iRtn;
}

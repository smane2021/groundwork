/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : linklayer_manager.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "../../haldriver/comm_net_tcpip/commsock.h"
#include "../../haldriver/comm_net_tcpip6/commsock6.h"

#include "tl1.h"
#include <arpa/inet.h>			/* inet_ntoa */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/tcp.h>

/* timeout uses in communication, unit: ms */
#define WAITFORCONN_TIMEOUT			3000/* wait for connected timeout */
#define READCOM_TIMEOUT				200
#define WRITECOM_TIMEOUT			2000


/* log Macros for Link-layer Manager sub-module(LM = linklayer manager) */
#define TL1_TASK_LM					"TL1 Linklayer Manager"

/* error log */
#define LOG_TL1_LM_E(_szLogText)  LOG_TL1_E(TL1_TASK_LM, _szLogText)
	
/* extended error log(logged with error code) */
#define LOG_TL1_LM_E_EX(_szLogText, _errCode)  \
	LOG_TL1_E_EX(TL1_TASK_LM, _szLogText, _errCode)

/* warning log */
#define LOG_TL1_LM_W(_szLogText)  LOG_TL1_W(TL1_TASK_LM, _szLogText)

/* info log */
#define LOG_TL1_LM_I(_szLogText)  LOG_TL1_I(TL1_TASK_LM, _szLogText)


#define TL1_STDPORT_TCP				1
#define TL1_STDPORT_MODEM			4
#define TL1_STDPORT_TCP_IPV6		7
#define	TL1_STDPORT_LEASEDLINE		3


/* wait for comm port has been closed */
__INLINE static void SafeOpenComm(IN OUT TL1_BASIC_ARGS *pThis)
{
#ifdef COM_SHARE_SERVICE_SWITCH
	int iMediaType;
	//HANDLE hMutex;  /* not need to use Mutex */
	SERVICE_SWITCH_FLAG *pFlag;

	HANDLE hLocalThread;

	iMediaType = g_TL1Globals.CommonConfig.iMediaType;
	pFlag = DXI_GetServiceSwitchFlag();
	hLocalThread = pThis->hThreadID[1];

	/* wait until Serial Port has closed by Maintenace Service */
	if (TL1_IS_SHARE_COM(iMediaType))
	{
		//TRACE("pFlag->bCOMHaveClosed is %d\n", pFlag->bCOMHaveClosed);
	    //TRACE("iMediaType is %d\n", iMediaType);
		while (1)
		{
			if (pFlag->bCOMHaveClosed)
			{
				pFlag->bCOMHaveClosed = FALSE;
				pFlag->bHLMSUseCOM = TRUE;

				break;
			}
			else
			{	
#ifdef _DEBUG_TL1_LINKLAYER
				//TRACE_TL1_TIPS("Serial Port has not been Closed");
				TRACE("Serial Port has not been closed!!\n");
#endif //_DEBUG_TL1_LINKLAYER

				Sleep(3000);

				/* feed watch dog */
				RunThread_Heartbeat(hLocalThread);
			}
		}
	}

#endif //COM_SHARE_SERVICE_SWITCH

	return;
}

void LeaveCommUsing(void)
{
#ifdef COM_SHARE_SERVICE_SWITCH
	int iMediaType;
	//HANDLE hMutex;  /* not need to use Mutex */
	SERVICE_SWITCH_FLAG *pFlag;

	iMediaType = g_TL1Globals.CommonConfig.iMediaType;
	pFlag = DXI_GetServiceSwitchFlag();

	//if (TL1_IS_SHARE_COM(iMediaType))
	{
	    TRACE("iMediaType:LeaveCommUsing is %d\n", iMediaType);
		pFlag->bCOMHaveClosed = TRUE;
	}

#endif //COM_SHARE_SERVICE_SWITCH

	return;
}

/*==========================================================================*
 * FUNCTION : ComQueue
 * PURPOSE  : communicate with Event Queues
 * CALLS    : Queue_Get
 *			  Queue_Put
 *			  CommWrite
 * CALLED BY: ComMC
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis       : 
 *            BOOL            bSendTimeOut : 
 * RETURN   : int : TL1_COMM_STATUS 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 14:20
 *==========================================================================*/
static int ComQueue(IN OUT TL1_BASIC_ARGS *pThis, IN BOOL bSendTimeOut)
{
	TL1_EVENT *pEvent;
	int iRet, iCommWriteRet;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[1]);

	if (bSendTimeOut) /* send timeout event to EventInputQueue */
	{
		pEvent = &g_TL1TimeoutEvent;

		/* send it to the EventInputQueue */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);

		if (iRet != ERR_OK)
		{
			LOG_TL1_LM_E_EX("send event to Event InputQueue failed", iRet);

			/* exit YDN service */
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

			return TL1_COMM_STATUS_DISCONNECT;
		}
	}
	iRet = Queue_Get(pThis->hEventOutputQueue, &pEvent, FALSE, WAIT_FOR_O_QUEUE);
	//TRACE("\n iRet:ComQueue is %d\n", iRet);

#ifdef _TEST_TL1_QUEUE_SYNCHRONIZATION
	{
		int iCount;
		TL1_EVENT *pLocalEvent;
		iCount = Queue_GetCount(pThis->hEventOutputQueue, NULL);
		if (iCount > 0)
		{
			TRACEX("Output Queue: Count is %d\n", iCount);
			TRACE("Time is: %.3f\n", GetCurrentTime());
			Queue_Get(pThis->hEventOutputQueue, &pLocalEvent, TRUE, 1000);
			TL1_PrintEvent(pLocalEvent);
		}

		iCount = Queue_GetCount(pThis->hEventInputQueue, NULL);
		if (iCount > 0)
		{
			TRACEX("Input Queue: Count is %d\n", iCount);
			TRACE("Time is: %.3f\n", GetCurrentTime());
			Queue_Get(pThis->hEventInputQueue, &pLocalEvent, TRUE, 1000);
			TL1_PrintEvent(pLocalEvent);
		}
	}
#endif //_TEST_TL1_QUEUE_SYNCHRONIZATION

	switch (iRet)
	{
		case ERR_OK:
		{
			switch (pEvent->iEventType)
			{
			case TL1_FRAME_EVENT:
				if (pEvent->bDummyFlag)
				{
					/* sleep 100 ms in lest two frame are sent together */
					/* normally, byte-read-delay is 50 ms */
					Sleep(100);
					return TL1_COMM_STATUS_NEXT;
				}
				else
				{
	#ifdef _DEBUG_TL1_LINKLAYER
					{
						int i, iLen;
						unsigned char chr;
						TRACE_TL1_TIPS("received frame event from OutputQueue, "
							"write the Data to Comm Port");
						iLen = pEvent->iDataLength;
						TRACE("\tData Length: %d\n", iLen);

						TRACE("\tData Content(Hex format):");
						for (i = 0; i < iLen; i++)
						{
							printf("%02X ", pEvent->sData[i]);
						}
						printf("\n");

						TRACE("\tData Content(Text format):");
						for (i = 0; i < iLen; i++)
						{
							chr = (unsigned char)pEvent->sData[i];
							//if (chr < 0x21 || chr > 0X7E)
							//{
							//	printf("%s", TL1_GetUnprintableChr(chr));
							//}
							//else
							{
								printf("%c", chr);
							}
						}
						printf("\n");

					}
	#endif //_DEBUG_TL1_LINKLAYER
			
					int		iRst;

					CommWrite(pThis->hComm, 
						(char *)pEvent->sData, 
						pEvent->iDataLength);
					//DELETE(pEvent);

					/* get the error code */
					iCommWriteRet = CommGetLastError(pThis->hComm);
					if (iCommWriteRet != ERR_OK)
					{
						LOG_TL1_LM_E_EX("Write to Comm Port failed", 
							iCommWriteRet);

						/* When timeout, not exit. */
						if (iCommWriteRet != ERR_TIMEOUT)
						{
							/* exit service */
							pThis->bServerModeNeedExit = TRUE;
							pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
						}

						iRst = TL1_COMM_STATUS_DISCONNECT;
					}


					else if (pEvent->bSkipFlag)
					{
						iRst = TL1_COMM_STATUS_SKIP;
					}
					else
					{
						iRst = TL1_COMM_STATUS_NEXT;
					}

					DELETE(pEvent);
					return iRst;
				}

			case TL1_DISCONNECTED_EVENT:
				TRACE_TL1_TIPS("Disconnected event received from "
					"EventOutputQueue");
				/*DELETE(pEvent);*/
				return TL1_COMM_STATUS_DISCONNECT;

			default:
				TRACE_TL1_TIPS("received none-expected event from EventOutputQueue");

				DELETE_TL1_EVENT(pEvent);
				return TL1_COMM_STATUS_NEXT;
			}
		}
		case ERR_QUEUE_EMPTY: //means timeout
		{
			//TRACE_TL1_TIPS("get event from EventOutputQueue timeout");
			return TL1_COMM_STATUS_NEXT;
		}
		default:         /* is ERR_INVALID_ARGS */ 
		{
				LOG_TL1_LM_E_EX("Get event from Event OutputQueue failed", iRet);

			/* exit service */
			pThis->bServerModeNeedExit = TRUE;
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

			return TL1_COMM_STATUS_DISCONNECT;
		}
	}
}


/*==========================================================================*
 * FUNCTION : PickupAFrame
 * PURPOSE  : Pickup a frame from input data buffer
 * CALLS    : 
 * CALLED BY: CommReadFrame
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis        : 
 *            int             iProtocolType : 
 *            char            *pDstBuf      : 
 *            int             *piLen        : 
 * RETURN   : BOOL : TRUE means get a frame from the received buf
 *					 FALSE means received buf is empty
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-12 09:55
 *==========================================================================*/
static BOOL PickupAFrame(IN OUT TL1_BASIC_ARGS *pThis, IN int iProtocolType, 
						 OUT char *pDstBuf, OUT int *piLen)
{
	int iRet = FALSE;
	int iSrcPos, iSrcLen;
	char *pSrcBuf;

	iSrcPos = pThis->iCurPos;
	iSrcLen = pThis->iLen;
	//iSrcLen = strlen(pThis->sReceivedBuff);
	pSrcBuf = (char *)pThis->sReceivedBuff;
	*piLen = 0;

	if (iSrcPos < iSrcLen)
	{
		iRet = TRUE;
		switch (iProtocolType)
		{
		    //need extra code
		    case TL1:

#ifdef	_DEBUG_TL1
	    		if((iSrcLen) > MAX_TL1FRAME_LEN)
				{
					AppLogOut("TL1 SRV", APP_LOG_ERROR, "***Error: PickupAFrame iSrcLen greater than MAX_TL1FRAME_LEN.\n");
				}
#endif

			   memcpy(pDstBuf, pSrcBuf + iSrcPos, (size_t)iSrcLen);
			   *piLen = iSrcLen;
			   iSrcPos = iSrcLen;
			   TL1_PrintfStr(iSrcLen, (char *)pThis->sReceivedBuff);
		}
		/* to delete */
		if (iSrcPos < iSrcLen)
		{
			TRACE("Split a frame!\n");
		}
		/* end */
	}

	pThis->iCurPos = iSrcPos;
	return iRet;
}


/*==========================================================================*
 * FUNCTION : CommReadFrame
 * PURPOSE  : 
 * CALLS    : PickupAFrame
 * CALLED BY: ComMC
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis   : 
 *            char            *pBuffer : 
 *            int             *piLen   : 
 *            int             iTimeOut : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-12 09:56
 *==========================================================================*/
static int CommReadFrame(IN OUT TL1_BASIC_ARGS *pThis, OUT char *pBuffer, 
						 OUT int *piLen, IN int iTimeOut)
{
	HANDLE hComm;
	int    iProtocolType, iErrCode;
	COMM_TIMEOUTS timeOut;

	hComm = pThis->hComm;
	iErrCode = ERR_COMM_OK;
	iProtocolType = g_TL1Globals.CommonConfig.iProtocolType;

	if (!PickupAFrame(pThis, iProtocolType, pBuffer, piLen))  /* received buff is empty */
	{
		/* set timeout */
		INIT_TIMEOUTS(timeOut, iTimeOut, WRITECOM_TIMEOUT);
		CommControl(hComm, 
			COMM_SET_TIMEOUTS,
			(char *)&timeOut,
			sizeof(COMM_TIMEOUTS)); 

		pThis->iCurPos = 0;
	 	pThis->iLen = CommRead(hComm, (char *)pThis->sReceivedBuff, MAX_TL1FRAME_LEN);
		//pThis->iLen = CommRead(hComm, pThis->sReceivedBuff, 512);
		//TRACE("pThis->iLen:CommRead is %d\n", pThis->iLen);
		TL1_PrintfStr(pThis->iLen, (char *)pThis->sReceivedBuff);
		/* get last err code */
		CommControl(hComm, 
			COMM_GET_LAST_ERROR, 
			(char *)&iErrCode, 
			sizeof(int));

		PickupAFrame(pThis, iProtocolType, pBuffer, piLen);
	}
	return iErrCode;
}


/*==========================================================================*
 * FUNCTION : ComMC
 * PURPOSE  : communicate with MC
 * CALLS    : CommRead 
 *			  Queue_Put  
 *			  ComQueue
 * CALLED BY: DataExchange
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis    : 
 *            int             iTimeOut  : 
 *            BOOL            bSkipFlag : 
 * RETURN   : int : TL1_COMM_STATUS
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 13:05
 *==========================================================================*/
static int ComMC(IN OUT TL1_BASIC_ARGS *pThis, IN int iTimeOut, IN BOOL bSkipFlag)
{
	HANDLE hComm;
	//char cFrame[128] = {'\0'};
	//BOOL bHaveEOI = FALSE;
	//BOOL bHaveSOI = FALSE;
	char sBuffer[MAX_TL1FRAME_LEN] = {'\0'};
	//char sBuffer[512];
	int  iLen, iErrCode, iRet, i, j, k;
	TL1_EVENT *pEvent;

	hComm = pThis->hComm;

	i = 0;
	j = 0;
	k = 0;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[1]);

	/* skip the step to keep synchronization */
	if (bSkipFlag)
	{
		return ComQueue(pThis, FALSE);
	}

	iErrCode = CommReadFrame(pThis, sBuffer, &iLen, iTimeOut);
	//TRACE("iErrCode is %d\n", iErrCode);
	//TL1_PrintfStr(iLen, sBuffer);
    //#ifdef _DEBUG_TL1_LINKLAYER
		  //TRACE("\t CommReadFrame:iErrcode = %d\n", iErrCode);
    //#endif //_DEBUG_TL1_LINKLAYER
	/* check the error code */
	switch (iErrCode) 
	{
	    case ERR_COMM_OK:
		    break;

	    case ERR_COMM_TIMEOUT:
		    if (iLen > 0)  //  is not timeout
		    {
				break;
			}

            /* send timeout event to EventQueue */
   //         #ifdef _DEBUG_TL1_LINKLAYER
		 //        TRACE_TL1_TIPS("read from MC timeout, send Timeout Event to "
			//"the InputQueue");
   //         #endif //_DEBUG_TL1_LINKLAYER

		    return ComQueue(pThis, FALSE);

	    case ERR_COMM_CONNECTION_BROKEN:

            #ifdef _DEBUG_TL1_LINKLAYER
		        TRACE_TL1_TIPS("connection closed by MC");
            #endif //_DEBUG_TL1_LINKLAYER

		    return TL1_COMM_STATUS_DISCONNECT;

	    default:
			TRACE("The received string is : %s\n", sBuffer);
		    LOG_TL1_LM_E_EX("Read Comm Port failed", iErrCode);

		    /* exit service */
		    pThis->bServerModeNeedExit = TRUE;
		    pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

		    return TL1_COMM_STATUS_DISCONNECT;
	}

    #ifdef _DEBUG_TL1_LINKLAYER
	{
		int i;
		unsigned char chr;

		TRACE_TL1_TIPS("Comm Port received data");
		TRACE("\tData Length: %d\n", iLen);

		TRACE("\tData Content(Hex format):");
		for (i = 0; i < iLen; i++)
		{
			printf("%02X ", sBuffer[i]);
		}
		printf("\n");

		TRACE("\tData Content(Text format):");
		for (i = 0; i < iLen; i++)
		{
			chr = (unsigned char)sBuffer[i];
			if (chr < 0x21 || chr > 0X7E)
			{
				printf("%s", TL1_GetUnprintableChr(chr));
			}
			else
			{
				printf("%c", chr);
			}
		}
		printf("\n");

	}
    #endif //_DEBUG_TL1_LINKLAYER

	pEvent = NEW(TL1_EVENT, 1);
	if (pEvent == NULL)
	{
		LOG_TL1_LM_E("no memory to create Frame Event");
		
		/* exit service */
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_MEMORY;

		return TL1_COMM_STATUS_DISCONNECT;
	}
	TL1_INIT_FRAME_EVENT(pEvent, iLen, sBuffer + j, 0, 0);


	//TL1_INIT_FRAME_EVENT(pEvent, iLen, cFrame, 0, 0);

    
    #ifdef _DEBUG_TL1_LINKLAYER
	    TRACE("The received data will sent to InputQueue as Frame Event.\n");
		TL1_PrintEvent(pEvent);
    #endif //_DEBUG_TL1_LINKLAYER
    //TRACE("the address of pEvent(CMD) is %p\n", pEvent);
	/* send it to the EventInputQueue */
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_TL1_LM_E_EX("Send Frame event to InputQueue failed", iErrCode);

		pThis->bServerModeNeedExit = TRUE;
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
	
		DELETE(pEvent);
		pEvent = NULL;
		return TL1_COMM_STATUS_DISCONNECT;
	}
	return ComQueue(pThis, FALSE);


}
			

/*==========================================================================*
 * FUNCTION : DataExchange
 * PURPOSE  : data exchange between MC and Event Queues
 * CALLS    : ComMC
 * CALLED BY: RunAsServer
 *			  RunAsClient
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 11:50
 *==========================================================================*/
static void DataExchange(IN OUT TL1_BASIC_ARGS *pThis)
{
	int iRet;
	BOOL bSkipFlag;

	bSkipFlag = FALSE;

	while (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		iRet = ComMC(pThis, READCOM_TIMEOUT, bSkipFlag);
     //   #ifdef _DEBUG_TL1_LINKLAYER
		   //TRACE("DataExchange->ComMC: %d\n", iRet);
     //   #endif //_DEBUG_TL1_LINKLAYER
		switch (iRet)
		{
		    case TL1_COMM_STATUS_DISCONNECT:
			    return;

		    case TL1_COMM_STATUS_SKIP:
                #ifdef _DEBUG_TL1_LINKLAYER
					TRACE_TL1_TIPS("skip ComMC");
					TRACE("returned value: %d\n", iRet);
                #endif //_DEBUG_TL1_LINKLAYER
			    bSkipFlag = TRUE;
			break;

		    case TL1_COMM_STATUS_NEXT:
				bSkipFlag = FALSE;
				break;
		}
	}		
}


/*==========================================================================*
 * FUNCTION : RunAsServer
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: TL1_LinkLayerManager
 * ARGUMENTS: TL1_BASIC_ARGS *  pThis      : 
 *            PORT_INFO         *pCommPort : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-10 21:27
 *==========================================================================*/
static void RunAsServer(IN OUT TL1_BASIC_ARGS *pThis, IN PORT_INFO *pCommPort)
{
	HANDLE hOpenComm, hDataComm;
	int iErrCode, iTryCount;

	/* to get common config info */
	TL1_COMMON_CONFIG *pCommonCfg;
	int iMediaType;   

	/* to send disconnected event */
	int iRet;
	TL1_EVENT *pEvent;

    /* to get client IP */
	COMM_PEER_INFO cpi;   
	DWORD dwPeerIP = 0;
	char *szClientIP;

	HANDLE hLocalThread;
	COMM_TIMEOUTS timeOut;
	int				iMaxClients = TL1_SERVER_MAX_CLIENTS;

	char *szLibName;
	char szOpenParam[64];

	hLocalThread = pThis->hThreadID[1];
	pCommonCfg = &g_TL1Globals.CommonConfig;
	iMediaType = pCommonCfg->iMediaType;

	/* reset flag */
	pThis->bServerModeNeedExit = FALSE;
	
	if (iMediaType == TL1_MEDIA_TYPE_TCPIP)
	{
		pCommPort->iStdPortTypeID = TL1_STDPORT_TCP;
		//strcpy(szOpenParam, "142.100.4.35:");
		strcpy(szOpenParam, "127.0.0.1:");
	}
	else if (iMediaType == TL1_MEDIA_TYPE_LEASED_LINE)
	{
		pCommPort->iStdPortTypeID = TL1_STDPORT_LEASEDLINE;
	}
	else if(iMediaType == TL1_MEDIA_TYPE_TCPIPV6)
	{
		pCommPort->iStdPortTypeID = TL1_STDPORT_TCP_IPV6;
		strcpy(szOpenParam, "[::1]:");
	}
	else
	{
		pCommPort->iStdPortTypeID = TL1_STDPORT_MODEM;
	}
    
	/* get lib name and port param */
	szLibName = GetStandardPortDriverName(pCommPort->iStdPortTypeID);
	/* get comm port param */
	if (iMediaType == TL1_MEDIA_TYPE_TCPIP
		|| iMediaType == TL1_MEDIA_TYPE_TCPIPV6)
	{
		strncat(szOpenParam, pCommonCfg->szCommPortParam,
			sizeof(szOpenParam));
	}
	else
	{
		strncpyz(szOpenParam, pCommonCfg->szCommPortParam,
				sizeof(szOpenParam));
	}

    #ifdef _DEBUG_TL1_LINKLAYER
		TRACE_TL1_TIPS("CC Comm Port Info:");
		TRACE("\tComm Port ID: %d\n", pCommPort->iPortID);
		TRACE("\tStdPort ID: %d\n", pCommPort->iStdPortTypeID);
		TRACE("\tStdPort accessing libname: %s\n", szLibName);
		TRACE("\tOpen Param: %s\n", szOpenParam);
		TRACE("\tpiTimeout: %d\n", pCommPort->iTimeout);
		TRACE("\tpszDeviceDesp: %s\n", pCommPort->szDeviceDesp);
		TRACE("\tszPortName: %s\n", pCommPort->szPortName);
	#endif //_DEBUG_TL1_LINKLAYER

	SafeOpenComm(pThis);
    //TRACE("Get out of SafeOpenComm!!\n");
	//printf("szLibName is %s\n", szLibName);
	//printf("pCommPort->szDeviceDesp is %s\n", pCommPort->szDeviceDesp);
	/* open the comm, retry 3 times */
	for (iTryCount = 0; iTryCount < 3; iTryCount++)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);
		
		hOpenComm = CommOpen(szLibName,
			pCommPort->szDeviceDesp,
			szOpenParam,
			COMM_SERVER_MODE,
			pCommPort->iTimeout,
			&iErrCode);
        TRACE("\niErrCode is: servermode %u\n", iErrCode);
		switch (iErrCode)
		{
		    case ERR_COMM_STD_PORT:
			    LOG_TL1_LM_E("Open Comm Port failed for StdPort ID is invalid");

			    /* exit the service */
			    pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
			    return;

		    case ERR_COMM_OPENING_PARAM:
				TRACE("\tiErrCode: %d\n", iErrCode);
				LOG_TL1_LM_E("Open Comm Port failed for Opening Param is invalid");
	            
				/* exit programme */
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_CFG;
				return;

		   case ERR_OK:
				iTryCount = 3;  /* break while loop */
				break;

		   default:  /* do nothing, just retry */
				Sleep(1000); 
				break;
		}
	}


	if (iErrCode != ERR_OK)
	{
		LOG_TL1_LM_E_EX("Open failed after 3 times retry", iErrCode);

		/* exit programme */
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
		return;
	}

	#ifdef _DEBUG_TL1_LINKLAYER
		TRACE_TL1_TIPS("Open comm port OK, now begin to wait for connected");
	#endif //_DEBUG_TL1_LINKLAYER

	/* main loop */
	INIT_TIMEOUTS(timeOut, WAITFORCONN_TIMEOUT, WRITECOM_TIMEOUT);
	while (!pThis->bServerModeNeedExit &&
		     pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);

		//Sleep(50);

        //pThis->bSecureLine = TRUE;
		
		/* set accept timeout */
		CommControl(hOpenComm, 
			COMM_SET_TIMEOUTS,
			(char *)&timeOut,
			sizeof(COMM_TIMEOUTS)); 

		/* set max connect count */
		CommControl(hOpenComm, 
			COMM_SET_MAX_CLIENTS,
			(char *)&iMaxClients,
			sizeof(iMaxClients)); 

		hDataComm = CommAccept(hOpenComm);

		if (hDataComm == NULL)
		{
			/* get last err code */
			CommControl(hOpenComm, 
				        COMM_GET_LAST_ERROR,
						(char *)&iErrCode, 
						sizeof(int));
			//TRACE("\niErrCode1 is: %d", iErrCode);

			if (iErrCode == ERR_COMM_TIMEOUT)
			{
				continue;
			}
			else  /* for other err, exit programme */
			{
				LOG_TL1_LM_E_EX("Accept failed", iErrCode);
				
				if(NULL != hOpenComm)
				{
					CommClose(hOpenComm);
					hOpenComm = NULL;
				}
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
                TRACE("\nErr1!!\n");
                 
				return;
			}
		} /* end of if (hDataComm == NULL) */

		/* successfully got hDataComm */
		/* check mode, if has something to report, return */ 
		/* Note: mode can be changed when bCommRunning is FALSE */
		if (pThis->iOperationMode != TL1_MODE_SERVER)
		{
		    TRACE("\n Run Serial_CommClose1!!\n");
			CommClose(hDataComm);
			if(NULL != hOpenComm)
			{
				CommClose(hOpenComm);
				hOpenComm = NULL;
			}
			return;
		}
		else
		{
			pThis->hComm = hDataComm;
		}

		TRACE_TL1_TIPS("Connected");

		/* set Running flag first to inhibit report */
		pThis->bCommRunning = TRUE;

		//we won't accept any other connections during processing this connection
		if(NULL != hOpenComm)
		{
			CommClose(hOpenComm);
			hOpenComm = NULL;
		}

        // set security flag
		switch (iMediaType)
		{
		case TL1_MEDIA_TYPE_LEASED_LINE:  //alaways secure
			pThis->bSecureLine = TRUE;
			break;

		case TL1_MEDIA_TYPE_MODEM:  //need callback
			pThis->bSecureLine = TRUE; 
			break;

		case TL1_MEDIA_TYPE_TCPIP:  //check secure IP list
		{
			pThis->bSecureLine = TRUE;

			iErrCode = CommControl(hDataComm, 
				COMM_GET_PEER_INFO, (char *)&cpi,
				sizeof(COMM_PEER_INFO));
			//TRACE("\niErrCode2 is: %d", iErrCode);
			if (iErrCode == ERR_COMM_OK)
			{
				// szAddr is in network format.
				dwPeerIP = *(DWORD *)cpi.szAddr;
				szClientIP = inet_ntoa(*(struct in_addr *)&dwPeerIP);
	
				//TRACE("\nszClientIP is: %s\n",szClientIP);
#ifdef _DEBUG_TL1_LINKLAYER
				{
					char szLogText[TL1_LOG_TEXT_LEN];
					sprintf(szLogText, "Client %s is accepted", szClientIP);
					TRACE_TL1_TIPS(szLogText);
				}
#endif //_DEBUG_TL1_LINKLAYER

/*
				for (i = 0; i < ESR_SECURITY_IP_NUM; i++)
				{
					if (strcmp(szClientIP, szSecurityIP[i]) == 0)
					{
						pThis->bSecureLine = TRUE;
						break;
					}
				}
*/	
			}

			break;
		}
		case TL1_MEDIA_TYPE_TCPIPV6:  //check secure IP list
		{
			pThis->bSecureLine = TRUE;

			iErrCode = CommControl(hDataComm, 
				COMM_GET_PEER_INFO, (char *)&cpi,
				sizeof(COMM_PEER_INFO));
			if (iErrCode == ERR_COMM_OK)
			{
				// szAddr is in network format.
				IN6_ADDR ip6Addr = *(IN6_ADDR *)(DWORD *)cpi.szAddr;
				char szClinetIP1[64];
				inet_ntop(AF_INET6,&ip6Addr,szClinetIP1,64);

#ifdef _DEBUG_TL1_LINKLAYER
				{
					char szLogText[TL1_LOG_TEXT_LEN];
					sprintf(szLogText, "Client %s is accepted", szClientIP);
					TRACE_TL1_TIPS(szLogText);
				}
#endif //_DEBUG_TL1_LINKLAYER


				//for (i = 0; i < ESR_SECURITY_IP_NUM; i++)
				//{
				//	if (strcmp(szClinetIP1, szSecurityIP[i]) == 0)
				//	{
				//		pThis->bSecureLine = TRUE;
				//		break;
				//	}
				//}
			}
			break;
		}
		default:  //do nothing
			break;
		}

		if((TL1_DISABLE_VALUE != pCommonCfg->iPortKeepAlive)//enable function Port Keep Alive
			&& ((TL1_MEDIA_TYPE_TCPIP == iMediaType || TL1_MEDIA_TYPE_TCPIPV6 == iMediaType)))
		{
			int iDataSockFd = -1;
			int socket_option_value;
			int rvalue;

			if(TL1_MEDIA_TYPE_TCPIPV6 == iMediaType)
			{
				//get socket fd from handle
				//hDataComm is a struct pointer of STD_PORT_DRV
				//a field:HANDLE				hInstance;		//	NOTE: MUST BE THE FIRST FIELD! 
				//hInstance is a struct pointer of TCPIP6_PORT_DRV
				iDataSockFd = ((TCPIP6_PORT_DRV *)(*(unsigned int *)hDataComm))->nSocket;
			}
			else
			{
				//get socket fd from handle
				//hDataComm is a struct pointer of STD_PORT_DRV
				//a field:HANDLE				hInstance;		//	NOTE: MUST BE THE FIRST FIELD! 
				//hInstance is a struct pointer of TCPIP_PORT_DRV
				iDataSockFd = ((TCPIP_PORT_DRV *)(*(unsigned int *)hDataComm))->nSocket;
			}

			// Activate the keepalive socket option
			socket_option_value = 1;
			rvalue = setsockopt(iDataSockFd,
				SOL_SOCKET,
				SO_KEEPALIVE,
				(char *)&socket_option_value,
				sizeof(socket_option_value));

			// Set the number of keepalive strobes to 4
			socket_option_value = TL1_PORT_KEEP_ALIVE_COUNT;
			rvalue = setsockopt(iDataSockFd, 
				IPPROTO_TCP, 
				TCP_KEEPCNT,
				(char *)&socket_option_value, 
				sizeof(socket_option_value));

			// Set the keepalive interval to five seconds
			socket_option_value = TL1_PORT_KEEP_ALIVE_INTERVAL;
			rvalue = setsockopt(iDataSockFd, 
				IPPROTO_TCP, 
				TCP_KEEPINTVL,
				(char *)&socket_option_value, 
				sizeof(socket_option_value));

			// Set the keepalive idle time to 60 seconds
			socket_option_value = TL1_PORT_KEEP_ALIVE_IDLE;
			rvalue = setsockopt(iDataSockFd, 
				IPPROTO_TCP, 
				TCP_KEEPIDLE,
				(char *)&socket_option_value, 
				sizeof(socket_option_value));
		}



		/* clear InputQueue */
		TL1_ClearEventQueue(pThis->hEventInputQueue, FALSE);

		pEvent = &g_TL1ConnEvent;
		/* send it */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
		if (iRet != ERR_OK)
		{
			LOG_TL1_LM_E("Put event to the Input Queue failed");
			/*DELETE(pEvent);*/
		}

		/* communication begin */
		#ifdef _DEBUG_TL1_LINKLAYER
	        TRACE("RunAsServer->DataExchange");
        #endif //_DEBUG_TL1_LINKLAYER

		DataExchange(pThis);
		pThis->bCommRunning = FALSE;
		/* communication end */

		/* clear OutputQueue */
		TL1_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

		pEvent = &g_TL1DiscEvent;

		/* send it */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
		if (iRet != ERR_OK)
		{
			LOG_TL1_LM_E("Put event to the Input Queue failed");
			/*DELETE(pEvent);*/
		}
		/* close Accept Comm Handle */
		//TRACE("\n Run Serial_CommClose2!!\n");
		CommClose(hDataComm);

		//just process a connection
		break;
	}

	/* close Open Comm Handle */
	TRACE("\n Run Serial_CommClose3!!\n");
	if(NULL != hOpenComm)
	{
		CommClose(hOpenComm);
		hOpenComm = NULL;
	}

	return;
}


/*==========================================================================*
 * FUNCTION : GetCommOpenParam
 * PURPOSE  : assistant function to get open params for CommOpen interface 
 *			  when in Client Mode
 * CALLS    : 
 * CALLED BY: RunAsClient
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis       : 
 *            PORT_INFO       *pCommPort   : 
 *            char            *szOpenParam : 
 *            int             iBufLen      : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 18:41
 *==========================================================================*/
static void GetCommOpenParam(IN OUT TL1_BASIC_ARGS *pThis,
							 IN PORT_INFO *pCommPort,
							 IN char *szOpenParam,
							 IN int iBufLen)
{
	TL1_COMMON_CONFIG *pCommonCfg;
	int iMediaType;
	BYTE byCurReportType;

	/* init local variable */
	pCommonCfg = &g_TL1Globals.CommonConfig;
	iMediaType = pCommonCfg->iMediaType;
	byCurReportType = pThis->alarmHandler.byCurReportRouteType;

	switch (iMediaType)
	{
	case TL1_MEDIA_TYPE_MODEM:
		/* get baud rate (szAccessingDriverSet: 9600,n,8,1:86011382)*/
		Cfg_SplitString(pCommPort->szAccessingDriverSet,
			szOpenParam, iBufLen, ':');
		/* now szOpenParam ended with null */
		strncat(szOpenParam, 
			    ":",
				iBufLen - strlen(szOpenParam) - 1); /* ensure end with null */

		switch (byCurReportType)
		{
		case TL1_ROUTE_ALARM_1:
			strncat(szOpenParam,
				    pCommonCfg->szAlarmReportPhoneNumber[0],
					iBufLen - strlen(szOpenParam) - 1);
			break;

		case TL1_ROUTE_ALARM_2:
			strncat(szOpenParam,
				    pCommonCfg->szAlarmReportPhoneNumber[1],
					iBufLen - strlen(szOpenParam) - 1);
			break;

		case TL1_ROUTE_ALARM_3:
			strncat(szOpenParam,
				    pCommonCfg->szAlarmReportPhoneNumber[2],
					iBufLen - strlen(szOpenParam) - 1);
			break;

		default:
			/* error */
			szOpenParam[0] = '\0';
		}
		break;

	case TL1_MEDIA_TYPE_TCPIP:
		/* TCPIP param format: 100.100.100.1:23 */
		switch (byCurReportType)
		{
			//deleted by ht,2006.5.12
		/*case TL1_ROUTE_ALARM_1:
			strncpyz(szOpenParam, pCommonCfg->szReportIP[0], iBufLen);
			break;

		case TL1_ROUTE_ALARM_2:
			strncpyz(szOpenParam, pCommonCfg->szReportIP[1], iBufLen);
			break;*/

		default:
			/* error */
			szOpenParam[0] = '\0';
		}
		break;
		
	case TL1_MEDIA_TYPE_LEASED_LINE:
		/* Leased line open format:   9600,n,8,1 */
		strncpyz(szOpenParam, pCommonCfg->szCommPortParam, iBufLen);
		break;

	default:  /* error */
		szOpenParam[0] = '\0';
	}

	return;
}


/*==========================================================================*
 * FUNCTION : RunAsClient
 * PURPOSE  : 
 * CALLS    : GetCommOpenParam
 * CALLED BY: TL1_LinkLayerManager
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis     : 
 *            PORT_INFO       *pCommPort : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 19:55
 *==========================================================================*/
static void RunAsClient(IN OUT TL1_BASIC_ARGS *pThis, IN PORT_INFO *pCommPort)
{
	char szOpenParam[64];  /* note: the buffer size should be enough */
	int  iErrCode, iRet;
	HANDLE hLocalThread, hDataComm;
	char *szLibName;

	char szLogText[TL1_LOG_TEXT_LEN];

	TL1_EVENT *pEvent;

	/* get Linklayer thread id */
	hLocalThread = pThis->hThreadID[1];

	/* prepare port open param */
	//TRACE("enter GetCommOpenParam!!");
	GetCommOpenParam(pThis, pCommPort, szOpenParam, 64);

	/* feed watch dog */
	RunThread_Heartbeat(hLocalThread);

    //TRACE("enter SafeOpenComm!!\n");
	SafeOpenComm(pThis);

	/* connect to remote */
	//TRACE("enter GetStandardPortDriverName!!\n");
	szLibName = GetStandardPortDriverName(pCommPort->iStdPortTypeID);
	TRACE("enter CommOpen!!\n");
	TRACE("szLibName is %s\n", szLibName);
	//TRACE("szOpenParam is %s\n", szOpenParam);
	//TRACE("pCommPort->iTimeout is %d\n", pCommPort->iTimeout);

	hDataComm = NULL;
	
	hDataComm = CommOpen(szLibName,
		            pCommPort->szDeviceDesp,
					szOpenParam,
					COMM_CLIENT_MODE,
					pCommPort->iTimeout,
					&iErrCode);

#ifdef _DEBUG_TL1_LINKLAYER
	TRACE_TL1_TIPS("MC Comm Port Info:");
	TRACE("\tComm Port ID: %d\n", pCommPort->iPortID);
	TRACE("\tStdPort ID: %d\n", pCommPort->iStdPortTypeID);
	TRACE("\tStdPort accessing libname: %s\n", szLibName);
	TRACE("\tOpen Param: %s\n", szOpenParam);
#endif //_DEBUG_TL1_LINKLAYER

	/* connect to remote takes time, feed watch dog again */
	RunThread_Heartbeat(hLocalThread);

	if (iErrCode != ERR_OK)  // init connect failed event
	{
		snprintf(szLogText, TL1_LOG_TEXT_LEN, "Connect to remote failed"
			"(Error Code: %X)", iErrCode);
		TRACE_TL1_TIPS(szLogText);
		TRACE("CommOpen at client is failed!!\n");
		/*TL1_INIT_NONEFRAME_EVENT(pEvent, TL1_CONNECT_FAILED_EVENT);*/
		pEvent = &g_TL1ConnFailEvent;
	}

	else  //init connected event
	{
		/* clear InputQueue */
		TL1_ClearEventQueue(pThis->hEventInputQueue, FALSE);
		pEvent = &g_TL1ConnEvent;
	}

	/* send it to the input queue */
	TRACE("enter Queue_Put1!!\n");
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_TL1_LM_E("Put event to the Input Queue failed");
	}

	/* if not connected, return */
	if (iErrCode != ERR_OK)
	{
		/* keep sychrnization */
		Sleep(50);
		return;
	}


	pThis->hComm = hDataComm;
	
	/* communication begin */
	pThis->bCommRunning = TRUE;

	/* now is security connect */

	/* send "CallBack and Report Alarms" frame from output queue */
	TRACE("enter ComQueue!!\n");
	ComQueue(pThis, FALSE);

    #ifdef _DEBUG_TL1_LINKLAYER
	        TRACE("RunAsClient->DataExchange");
    #endif //_DEBUG_TL1_LINKLAYER
    TRACE("enter DataExchange!!\n");
	DataExchange(pThis);

	/* communication end */
	/*pThis->bSecureLine = FALSE;*/
	pThis->bCommRunning = FALSE;

	/* clear OutputQueue */
	TRACE("enter TL1_ClearEventQueue!!\n");
	TL1_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

	pEvent = &g_TL1DiscEvent;
	/* send it */
	TRACE("enter Queue_Put2!!\n");
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_TL1_LM_E("Put event to the Input Queue failed");
		/*DELETE(pEvent);*/
	}

   // TRACE("\n Run Serial_CommClose:RunAsClient!!\n");
	CommClose(hDataComm);
	
	return;
}


/*==========================================================================*
 * FUNCTION : TL1_LinkLayerManager
 * PURPOSE  : Linklayer thread entry function
 * CALLS    : RunAsServer
 *			  RunAsClient
 * CALLED BY: ServiceMain
 * ARGUMENTS: TL1_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 16:02
 *==========================================================================*/
int TL1_LinkLayerManager(IN OUT TL1_BASIC_ARGS *pThis)
{
	int iCommPortID;
	PORT_INFO *pCommPort = NULL;
	int iMediaType;

	/* to get comm port info through DixGetData interface */
	int iError, iBufLen, iTryCount;

	HANDLE hLocalThread;

	/* get Local thread id */	
	hLocalThread = RunThread_GetId(NULL);
	/* assign the reference in the Thread to insure valid */
	pThis->hThreadID[1] = hLocalThread;
	/* feed watch dog */
	RunThread_Heartbeat(hLocalThread);

	/* get current Comm Port ID */
	if (g_TL1Globals.CommonConfig.iMediaType == TL1_MEDIA_TYPE_TCPIP
		|| g_TL1Globals.CommonConfig.iMediaType == TL1_MEDIA_TYPE_TCPIPV6)
	{
		/* for net data port, port id is 3 */
		iCommPortID = 2;
	}
	else /* Modem or Leased Line */
	{
		iCommPortID = 3;
	}

	/* get Comm Port config Info by port id */
	for (iTryCount = 0; iTryCount < 3; iTryCount++)
	{
		iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_PORT_INFO,			
			iCommPortID,		
			&iBufLen,			
			&pCommPort,
			0);

		if (iError != ERR_DXI_OK) 
		{
			if (iError == ERR_DXI_TIME_OUT)
			{
				/* try 3 times */
				Sleep(1000);
				continue;
			}
			else
			{
				LOG_TL1_LM_E_EX("Get Comm Port config info failed", iError);

				/* notify service to exit */
				pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
				return SERVICE_EXIT_FAIL;
			}
		}
		else
		{
			break;
		}
	}
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread TL1 Linker Service was created, Thread Id = %d.\n", gettid());
#endif	
	/* begin main loop */
	iMediaType = g_TL1Globals.CommonConfig.iMediaType;
	while (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);

		if((TL1_DISABLE_VALUE == g_TL1Globals.CommonConfig.iModuleSwitch)
			|| (TL1_DISABLE_VALUE == g_TL1Globals.CommonConfig.iPortActivation))//disable server port
		{
			Sleep(1000);
			continue;
		}

		/* run in the Server mode */
		if (pThis->iOperationMode == TL1_MODE_SERVER) 
		{   
		    TRACE("\nEnter the Server Mode!!\n");
			TRACE_TL1_TIPS("Enter the Server Mode");
			RunAsServer(pThis, pCommPort);
			LeaveCommUsing();
			TRACE_TL1_TIPS("Exit the server mode");
		}
		else			/* run in the Client mode */
		{
		    TRACE("\nEnter the Client Mode!!\n");
			TRACE_TL1_TIPS("Enter the Client Mode");
			//RunAsClient(pThis, pCommPort);
			//LeaveCommUsing();
			Sleep(1000);
			TRACE_TL1_TIPS("Exit the Client Mode");
		}
	}

	TRACE_TL1_TIPS("Exit the Link-layer sub-thread");

	/* notify service to exit(only for abnormal case) */
	if (pThis->iLinkLayerThreadExitCmd != SERVICE_EXIT_OK)
	{
		TRACE("\nExit code: %d\n", pThis->iLinkLayerThreadExitCmd);
		pThis->iInnerQuitCmd = pThis->iLinkLayerThreadExitCmd;
	}

	return pThis->iLinkLayerThreadExitCmd;
	
}


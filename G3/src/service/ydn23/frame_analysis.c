/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : frame_analysis.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "ydn.h"


/*==========================================================================*
 * FUNCTION : IsAddressOk
 * PURPOSE  : assistant funtion, check CCID
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char  *pStr : 2 ascii char
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 19:38
 *==========================================================================*/
static BOOL IsAddressOk(IN const unsigned char *pStr)
{
	if (pStr == NULL || pStr + 1 == NULL)
	{
		return FALSE;
	}

	if (YDN_AsciiHexToChar(pStr) == YDN_GetADR())
	{
		return TRUE;
	}

	else
	{
		return FALSE;
	}
}


/*==========================================================================*
 * FUNCTION : IsCharPrintable
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CaclBCC (BCC is between 0x20-0x7f)
 * ARGUMENTS: IN const unsigned char  c : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 20:20
 *==========================================================================*/
static BOOL IsCharPrintable(IN const unsigned char c)
{
	if(c >= 0x20 && c <= 0x7f)
	{
      return TRUE;
	}

    return FALSE;
}

/*==========================================================================*
 *	FUNCTION:�Գ���ΪnLen�������ݰ�У�飬��У���ֵд��pChkBytes��
 *   PURPOSE :���У���ֵֻ��1���ֽڣ���д��pChkBytes[0]��
 *	Input   :
 *			  pFrame  Ҫ���ݰ��ĵ�ַ.
 *			  nLen	��У������ݰ�����
 *			  pChkBytes װУ��ֵ�Ĵ�
 *	Output  :	pChkBytes У��ֵ
 *	RETURN  :��
 *	HISTORY :
 *==========================================================================*/

void YDNCheckSum (
                IN const unsigned char   *pFrame ,	// ҪУ��Ĵ��봮
                IN int		nLen ,		// ��ҪУ��Ĵ�����
				OUT unsigned char *pChkBytes	// ��У��Ľ��д��ô�
               )
{
    WORD	wChkValue	= 0 ;
    int i;
    for ( i = 0 ; i< nLen ; i++ )
    {
        wChkValue +=  pFrame[ i ] ;
    }
	
	wChkValue = ~wChkValue + 1 ; 
	//sprintf( ( char * )pChkBytes , "%04X" , wChkValue );
	HexToFourAsciiData(wChkValue, pChkBytes);
}

/*--------------------------------------------*/
/*  ChgAToHex: Convert array to hexa value. */
/* example: array="12345678af",num=5
            after calling  a_to_hexa(array,hexa,num)
            hexa={0x12,0x34,0x56,0x78,0xaf}
*/
/*--------------------------------------------*/
void ChgAToHex( const unsigned char *array, unsigned int *hexa, int num)
{
	int  i;
	BYTE byTemp;

	for(i=0; i< (num*2); i++) 
	{
		if( *(array + i) > '9' )
			byTemp = *(array + i) - 'A' + 10;
		else
			byTemp = *(array + i) - '0';
		if (i % 2)
			hexa[i/2] |= byTemp;
		else
			hexa[i/2] = byTemp << 4;
	}
}

/*==========================================================================*
 * FUNCTION : YDN_AnalyseFrame
 * PURPOSE  : 
 * CALLS    : SOC_AnalyseSTXFrame
 *			  RSOC_AnalyseSTXFrame
 * CALLED BY: 
 * ARGUMENTS: unsigned unsigned char *  pFrame : 
 *            int                       iLen   : 
 * RETURN   : FRAME_TYPE : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 16:32
 *==========================================================================*/
YDN_FRAME_TYPE YDN_AnalyseFrame(const unsigned char *pFrame, 
							int iLen,
							YDN_BASIC_ARGS *pThis)
{
    UNUSED(pThis);

    unsigned char pAdr[2];

	//TRACE("YDN23 pFrame is %s!\n", pFrame);

	if(*pFrame == 0x40)
	{
		if(*(pFrame + 1) == EOI)
		{
			TRACE("Get SCU+ address after alarm report!\n");
			return RADDR;
		}
		else 
			return FRAME_ERR;
	}
	else if (pFrame == NULL || *pFrame != SOI || *(pFrame + iLen -1) != EOI)
	{
		TRACE("[%s]--AnalysepFrame: ERROR: Get NULL pFrame or not a YDN23 pFrame!\n", __FILE__);
		return FRAME_ERR;
	}

	if (iLen < 1)
	{
		return FRAME_ERR;
	}

	//place the checksum
    unsigned char  iChkBytes[ 5 ]  = { 0 };
	unsigned int     iLenChk = 0;
	unsigned int     iProcotolVer;
	unsigned int     iControllerAdr;
	unsigned int     iCID2;
	unsigned int     wLengthInfo[ 2 ] = { 0 } ;
	//1.check the chksum
	YDNCheckSum( pFrame + 1, iLen - CHKSUM_OFFSET_ADDR - 1, iChkBytes ) ;
	if(	pFrame[ iLen - CHKSUM_OFFSET_ADDR ] != iChkBytes[ 0 ] 
		|| pFrame[ iLen - CHKSUM_OFFSET_ADDR + 1 ] != iChkBytes[ 1 ] 
		|| pFrame[ iLen - CHKSUM_OFFSET_ADDR + 2 ] != iChkBytes[ 2 ] 
		|| pFrame[ iLen - CHKSUM_OFFSET_ADDR + 3 ] != iChkBytes[ 3 ] )

	{
	    printf("\n YDN_AnalyseFrame:CHKSUM is %s\n", iChkBytes);
		return CHKERR;
	}

	//2.check the lchksum
	ChgAToHex( (pFrame + 9), wLengthInfo, 2 );

	BYTE	cLenChk = 0 ;
	cLenChk += wLengthInfo[ 0 ] & 0x0F ;
	cLenChk += ( wLengthInfo[ 0 ] >> 4 ) & 0x0F ;
	cLenChk += wLengthInfo[ 1 ] & 0x0F ;
	cLenChk += ( wLengthInfo[ 1 ] >> 4 ) & 0x0F ;

	cLenChk = iLenChk & 0x0F ;
	if( cLenChk != 0x00 )
	{
	    printf("\n YDN_AnalyseFrame:LCHKSUM is %s\n", wLengthInfo);
		return LCHKERR;
	}

	//check the first char
	if( pFrame[ 0 ] != SOI)
	{
	    printf("\n YDN_AnalyseFrame:SOI is %c\n", pFrame[ 0 ]);
		return CMDERR;
	}
    
	//check CID2
	//zwh:����READTIME, WRITETIME, WRITEKEYVLAUE, CLEARSOMEALARM
	iCID2 = (int)YDN_AsciiHexToChar(pFrame + 7);
	if( iCID2 != ANALOGSIG && iCID2  != STATUSSIG 
	 && iCID2 != ALARMSIG && iCID2  != REMOTECTL
	 && iCID2 != GETCHARPARA && iCID2  != GETFLOATPARA
	 && iCID2 != GETPROTOCOLVER && iCID2  != REMOTESET
	 && iCID2 != GETRECTID && iCID2 != SETTIME
	 && iCID2 != GETSMINFO && iCID2 !=  GETSMADR
	 && iCID2 != SYSSTATUSSP && iCID2 != SYSSTATUSST
	 && iCID2 != GETRECTBAECODE && iCID2 != GETSCUPBARCODE
	 && iCID2 != RECTREDUNDSP && iCID2 != RECTREDUNDST
	 && iCID2 != GETSIGNALVALUE && iCID2 != SETSIGNALVALUE
	 && iCID2 != READTIME && iCID2 != WRITETIME
	 && iCID2 != WRITEKEYVALUE && iCID2 != READHISTORYALARM
	 && iCID2 != CLEARSOMEALARM
	 && iCID2 != GZ_SET_A_SIG_VALUE			//���ӹ�װ����������
	 && iCID2 != GZ_GET_A_SIG_VALUE)
	{
	    printf("\n YDN_AnalyseFrame:CID2 is %d\n", iCID2);
		printf("\npFrame is %s\n", pFrame);
		return CID2ERR;
	}
	
	 //check the protocol version
	if(iCID2 != GETPROTOCOLVER)
	{
	     iProcotolVer = (int)YDN_AsciiHexToChar(pFrame + 1);
	     if( iProcotolVer != PROTOCOLVER )
	     {
	          printf("\n YDN_AnalyseFrame:PROTOCOLVER is %d\n", iProcotolVer);
		      return PERR;
	     }
	}

   // GetSCUPSingleSigValues(FALSE, 2, 30, 1, &pAdr);
	//iAdr = (int)YDN_AsciiHexToChar(pAdr);
   // g_YDNGlobals.SMAddr = iAdr;
    
    if(iCID2 != GETSMADR)
    {
		//check the address of controller
		
		iControllerAdr = (int)YDN_AsciiHexToChar(pFrame + 3);
		/*
		if( iControllerAdr != g_YDNGlobals.CommonConfig.byADR)
		{
			//������Ч���ݴ���
			printf("\n YDN_AnalyseFrame:byADR is %d\n", iControllerAdr);
			return DTERR;
		}
		*/
		
		if( iControllerAdr != g_YDNGlobals.CommonConfig.byADR)
		{
			//������Ч���ݴ���
			TRACE("\n YDN_AnalyseFrame:byADR is %d\n", iControllerAdr);
			TRACE("\n g_YDNGlobals.CommonConfig.byADR is %d\n", g_YDNGlobals.CommonConfig.byADR);
			if(g_YDNGlobals.CommonConfig.iMediaType == YDN_MEDIA_TYPE_TCPIP)
				return NOR;
			//return DTERR;
			return FRAME_ERR;//added at 2006.9.1��not send anything if address is wrong.
		}
    }
  
	return NOR;
}

/*==========================================================================*
 * FUNCTION : YDN_ExtractDataFromFrame
 * PURPOSE  : extract cmd data form frame
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN FRAME_TYPE frameType  : 
 *			  IN const unsigned char  *pFrame   : 
 *            IN int         iFrameLen : 
 *            OUT int        *piDataLen: 
 * RETURN   : const unsigned char *: 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-10-12 20:46
 *==========================================================================*/
void YDN_ExtractDataFromFrame(IN const unsigned char *pFrame,
						       IN int iFrameLen, 
						       OUT int *piDataLen,
						       OUT unsigned char *pCmdData)
{
	int i, j, iDataLen;

	iDataLen = YDN_ThreeAsciiHexToChar(pFrame + 10);

	*piDataLen = iFrameLen - 18;

    for(i = 0; i < 4; i++)
    	pCmdData[i] = *(pFrame + 5 + i);
	for(j=0; j < iDataLen && i < 32; j++, i++)	//��Ϊ����pCmdData��32���ռ䣬���ԼӸ��ݴ�
		pCmdData[i] = *(pFrame + 13 + j);
	pCmdData[i] = '\0';
	
	//return pRTN;
}


/*==========================================================================*
 * FUNCTION : YDN_BuildYDNResponseFrame
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char  *szResData    : 
 *            OUT unsigned char        *pFrameData  :
 *			  OUT int      *piFrameDataLen :
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-10-13 09:37
 *==========================================================================*/
void YDN_BuildYDNResponseFrame(IN const unsigned char *szResData,
                               IN  int iCID1,
                               IN  YDN_FRAME_TYPE RTN,
							   IN  int iAddr,//use at TCP/IP
							   OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen)
{
	//YDN_COMMON_CONFIG *pConfig;
	unsigned char *pData;

	unsigned char cAddr;

    int i;
	unsigned char  szChkBytes[ 5 ]  = { 0 } ;			//���У��ֵ

	pData = pFrameData;
	/* 1.build frame head, including SOI, PROTOCOLVER, byAdr, CID1, RTN*/
	//sprintf(pData, "%c%02X%02X%02X%02X", SOI, PROTOCOLVER, g_YDNGlobals.CommonConfig.byADR,
		  //iCID1, RTN);

	if(g_YDNGlobals.CommonConfig.iMediaType == YDN_MEDIA_TYPE_TCPIP)
		cAddr = iAddr;
	else
		cAddr = g_YDNGlobals.CommonConfig.byADR;
	
	*pData = SOI;
	HexToTwoAsciiData(PROTOCOLVER, pData + 1);
	HexToTwoAsciiData(cAddr, pData + 3);
	HexToTwoAsciiData(iCID1, pData + 5);
	HexToTwoAsciiData(RTN, pData + 7);
	
	*piFrameDataLen = 9;

	/* 2.builder DATA */
	pData += 9;
	while (*szResData != '\0' && (*piFrameDataLen) <= MAX_YDNFRAME_LEN - 14)
	{
		*pData++ = *szResData++;
		(*piFrameDataLen)++;
	}
    
	/* 2.build frame tail */
	YDNCheckSum(pFrameData + 1 , (*piFrameDataLen) - 1, szChkBytes);
	for(i = 0; i < 4; i++, pData++)
		*pData = szChkBytes[i];
	*pData = EOI;
	(*piFrameDataLen) += 5;

	return;
}
	
#ifdef COM_SHARE_SERVICE_SWITCH

#define CRC32_DEFAULT    0x04C10DB7
static void BuildTable32(unsigned long aPoly , unsigned long *Table_CRC)
{
    unsigned long i, j;
    unsigned long iData;
    unsigned long iAccum;

    for (i = 0; i < 256; i++)
    {
        iData = (unsigned long )( i << 24);
        iAccum = 0;
        for (j = 0; j < 8; j++)
        {
            if (( iData ^ iAccum ) & 0x80000000)
			{
                iAccum = ( iAccum << 1 ) ^ aPoly;
			}
            else
			{
                iAccum <<= 1;
			}
            iData <<= 1;
        }
        Table_CRC[i] = iAccum;
    }
}

static unsigned long RunCRC32(const unsigned char *aData, 
							  unsigned long aSize, 
							  unsigned long aPoly)
{
    unsigned long Table_CRC[256]; // CRC table
    unsigned long i;
    unsigned long iAccum = 0;

    BuildTable32(aPoly, Table_CRC);
    
    for (i = 0; i < aSize; i++)
	{
        iAccum = ( iAccum << 8 ) ^ Table_CRC[( iAccum >> 24 ) ^ *aData++];
	}

    return iAccum;
}

BOOL YDN_IsMtnFrame(const unsigned char *pFrame, int iLen)
{
	DWORD dwCalCRC32;

	if (iLen < 13)
	{ 
		return FALSE;
	}

	if (pFrame[0] != 0x7e)
	{
		return FALSE;
	}

	if (pFrame[3] != 0xd5)
	{
		return FALSE;
	}

	if (pFrame[4] != 0xcc)
	{
		return FALSE;
	}
	
	if (pFrame[6] != 0 || pFrame[7] != 0)
	{
		return FALSE;
	}

	if (pFrame[12] != 0x0d)
	{
		return FALSE;
	}

	dwCalCRC32 =  *((DWORD *)(pFrame + 8));
	if (dwCalCRC32 != RunCRC32(pFrame + 1, 7, (unsigned long)CRC32_DEFAULT))
	{
		return FALSE;
	}

	return TRUE;
}

	

void YDN_BuildMtnResponseFrame(OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen)
{
	pFrameData[0] = 0x7e;
	pFrameData[1] = 1;
	pFrameData[2] = 0xff;
	pFrameData[3] = 0xd5;

	/* com is using */
	pFrameData[4] = 0x11;    

	pFrameData[5] = 0;
	pFrameData[6] = 0;
	pFrameData[7] = 0;

	/* get CRC */
	*((DWORD *)(pFrameData + 8)) = 
		RunCRC32(pFrameData + 1, 7, (unsigned long)CRC32_DEFAULT);

	pFrameData[12] = 0x0d;

	*piFrameDataLen = 13;

	return;
}

#endif //COM_SHARE_SERVICE_SWITCH

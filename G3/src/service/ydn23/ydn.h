/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : ydn.h
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __YDN_H_2006_05_09__
#define __YDN_H_2006_05_09__


#define	_DEBUG_YDN


/* basic ascii code(used by frame-analysis and command-handler) */

#define  DATAFLAG       0x11


#define	 NOR		0x00
#define  PERR		0x01
#define  CHKERR		0x02
#define  LCHKERR	0x03
#define  CID2ERR	0x04
#define  CMDERR		0x05
#define  DTERR      0x06
#define  RERR		0xE0
#define  CTLFAIL	0xE1
#define  DEVFAIL    0xE2
#define  UNWRITE    0xE3
#define  REV1       0xE4
#define  REV2       0xE5
#define  REV3       0xE6
#define  REV4       0xE7
#define  REV5       0xE8
#define  REV6       0xE9
#define  REV7       0xEA
#define  REV8       0xEB
#define  REV9       0xEC
#define  REV10      0xED
#define  REV11      0xEE
#define  REV12      0xEF
#define  RADDR      0xF0

#define	 SOI		  '~' 		//first char of a YDN23 frame
#define	 EOI		  0x0D		//last char of a YDN23 frame
#define	 PROTOCOLVER  0x20 	  //protocol version,now is 1.00
#define   SOI_RADDR  0x40

//CID2 table
#define   ANALOGSIG  0x41
#define   STATUSSIG  0x43
#define   ALARMSIG   0x44
#define   REMOTECTL  0x45
#define   GETCHARPARA 0x46
#define   GETFLOATPARA 0x48
//zwh:����ydn23���Ӷ�дʱ���cid2
#define   READTIME	0x4D
#define   WRITETIME	0x4E

#define   GETPROTOCOLVER 0x4F
#define   GETSMINFO   0x51
#define   GETSMADR   0x50
#define   REMOTESET  0x80
#define   GETRECTID  0xE1
#define   GETRECTBAECODE 0xE3
#define   SETTIME    0xE2
#define   SYSSTATUSSP 0x81
#define   SYSSTATUSST 0x80
#define   GETSCUPBARCODE 0xFA

#define   RECTREDUNDSP  0x90
#define   RECTREDUNDST  0x91

#define   GETSIGNALVALUE 0xF0
#define   SETSIGNALVALUE 0xF1
//zwh:����д���̵�cid2, 0xE4
#define   WRITEKEYVALUE  0xE4
//zwh:���Ӷ���ʷ�澯e1ea
#define	  READHISTORYALARM 0xEA
//zwh:���������ʷ�澯e1f0
#define	  CLEARSOMEALARM 0xF0


//���ӹ�װ���� ��д�����ļ� ���� 0xEC 0xEB
#define	GZ_SET_A_SIG_VALUE	0xEC
#define	GZ_GET_A_SIG_VALUE	0xEB


#define  CHKSUM_OFFSET_ADDR	5
/* const definitions */
#define MAX_YDNFRAME_LEN		4114
#define YDN_CMD_DATA_LEN  4096
//#define YDN_CMD_DATA_LEN  512

/* used when call Queue_Get interface to get YDN Event, Unit: ms */
//#define WAIT_FOR_I_QUEUE		3000	//wait for Event Input Queue
#define WAIT_FOR_I_QUEUE		3000	//wait for Event Input Queue
#define WAIT_FOR_O_QUEUE		6000	//wait for Event Output Queue

/* const definition for State Machine */
#define YDN_STATE_NUM           3

/* YDN command number supported */
//#define YDN_CMD_NUM                 44
//zwh:������������ngc_lf���ݵ�Э��404D,404E
//zwh:������д���̵�Э��e1e4
//zwh:���Ӷ���ʷ�澯e1ea
#define YDN_CMD_NUM                 50
#define YDN_READ_CMD_NUM           11
/* maximum length of [Application Data] in YDN23 protocol */
#define YDN_MAX_APPDATA_LEN		4096

/* max alarm number reported in a frame */
#define YDN_MAX_ALARMS_IN_FRAME		10

/* command reply buf size */
#define YDN_RESP_SIZE				4114

/* input event queue and output event queue size */
#define YDN_EVENT_QUEUE_SIZE		20

/* define buffer size */
/* alarm buf */
#define YDN_ALARM_BUF_SIZE	    50

/* battery test log buf */
#define YDN_BATT_TESTLOG_SIZE   (2*1024)  //only report Capacity now

/* max battery test log quentity */
#define YDN_BATT_TESTLOG_NUM	10

/* battery test log segment size(transmint in a frame) */
#define	YDN_BATT_TESTLOG_SEG	127  //128 - 1, excluding '*'

//MAX EquipNum
#define MAX_EQUIP_NUM         800
/* define YDN service net port */
#define YDN_NETSERVICE_PORT    "2000"

// compare with YDN_BASIC_ARGS.iTimeoutCounter. When ACU is on YDN_IDLE or
// SOC_OFF state, if MC has no action within READCOM_TIMEOUT*YDN_MAX_TIMEOUTCOUNT,
// ACU will disconnect the communication actively.
#define YDN_MAX_TIMEOUTCOUNT    60
//#define YDN_MAX_TIMEOUTCOUNT    180

// for SMIO dynamic signal names
//#define SMIO_TYPE				"900"  //mapped to 901-908
#define MAX_SMIO_UNIT			8
#define SMIO_REF_TEXTS_NUM		18
#define MAX_REF_TEXTS			(MAX_SMIO_UNIT * SMIO_REF_TEXTS_NUM)

//#define SMBAT_TYPE				"310"  //mapped to 311-316
//#define ACUNIT_TYPE				"801"  //mapped to 801-803

#define ACD_MAX_NUM				5
#define ACD_MIN_NUM           0
#define AC_MAX_INPUT_ROUTE		3
#define AC_MAX_FUSE_NUM      0
#define AC_MAX_OUTPUT_BREAKER		8
#define DCD_MAX_NUM				10
#define DCD_MIN_NUM           0
#define ALL_AC_OR_DC              0xFF
#define RECT_MAX_NUM			120
#define DCD_BATT_NUM			20
#define MAX_RECT_ID            120
#define RECT_STDEQUIP_ID      201
#define BC_CTRL                 16
#define FC_CTRL                 31
#define START_TEST_CTRL        17
#define RECT_DCON_CTRL         32
#define RECT_DCOFF_CTRL        47
#define STOP_TEST_CTRL         228
#define RECT_ACON_CTRL         229
#define RECT_ACOFF_CTRL        230
#define RECT_RESET_CTRL        231

#define DCD_BATT_MAX_NUM      4// MAX number of each DCD
#define DCD_OUTPUT_MAX_NUM    9
#define DC_MAX_FUSE_NUM       64
#define YDN_MAX_RECT_NUM      50
//#define MAX_EQUIP_NUM       140
#define READ51TO100RECT_A_DATA   0xE2
#define READ101TO150RECT_A_DATA   0xE4

#define RECT_ID_SECOND_SECT  100 //attention: The rectifier's ID is not continuous.
#define RECT_NUM101_ID   1601

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * basic structures 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* define protocol types */
enum YDN_PROTOCOL
{
//    YDN    =1,
	EEM_YDN = 1,
	RSOC_YDN,
	SOC_TPE_YDN,
	YDN,
	YDN_PROTOCOL_NUM
};


/* define transmit media types */
enum YDN_MEDIA_TYPE
{		
    YDN_MEDIA_TYPE_LEASED_LINE			= 0,
    YDN_MEDIA_TYPE_MODEM,
    YDN_MEDIA_TYPE_TCPIP,
    YDN_MEDIA_TYPE_NUM
};

/* define timers */
enum YDN_TIMER
{
	YDN_ALARM_TIMER		= 1,
	YDN_CALLBACK_TIMER,

	YDN_SOC_WAIT_DLE0_TIMER,
	YDN_SOC_WAIT_CMD_TIMER,
	YDN_SOC_WAIT_ACK_TIMER,

	YDN_TIMER_NUM
};

/* define YDN Event types */
enum YDN_EVENT_TYPE
{
	YDN_FRAME_EVENT				= 1,

	YDN_CONNECTED_EVENT,
	YDN_CONNECT_FAILED_EVENT,
	YDN_DISCONNECTED_EVENT,

	YDN_TIMEOUT_EVENT
};



/* YDN Event definition */
struct tagYDNEvent
{
	int	iEventType;			/* event type */

	/* data length of the event(only for FrameEvent) */
	int		iDataLength;	
	/* data of the event(only for FrameEvent) */
	unsigned char sData[MAX_YDNFRAME_LEN];	

	BOOL	bDummyFlag;		/* dummy flag for FrameEvent */
	BOOL	bSkipFlag;		/* skip flag for FrameEvent  */
};
typedef struct tagYDNEvent YDN_EVENT;

#define YDN_INIT_FRAME_EVENT(_pEvent, _iLen, _szData, _bDummyFlag, _bSkipFlag)   \
			((_pEvent)->iEventType = YDN_FRAME_EVENT, \
			(_pEvent)->iDataLength = (_iLen), \
			((_iLen) == 0 ? (_pEvent)->sData[0] = '\0' : \
				(void)memcpy((_pEvent)->sData, (_szData), (size_t)(_iLen))), \
			(_pEvent)->bDummyFlag = (_bDummyFlag), \
			(_pEvent)->bSkipFlag = (_bSkipFlag))

#define YDN_INIT_SIMPLE_FRAME_EVENT(_pEvent, _chr) \
			((_pEvent)->iEventType = YDN_FRAME_EVENT, \
			(_pEvent)->iDataLength = 1, \
			(_pEvent)->sData[0] = (_chr), \
			(_pEvent)->bDummyFlag = FALSE, \
			(_pEvent)->bSkipFlag = FALSE)

#define YDN_INIT_NONEFRAME_EVENT(_pEvent, _EventType)    \
					((_pEvent)->iEventType = (_EventType), \
					 (_pEvent)->iDataLength = 0, \
					 (_pEvent)->sData[0] = '\0', \
					 (_pEvent)->bDummyFlag = FALSE, \
					 (_pEvent)->bSkipFlag = FALSE)

/* use it after init the Event already */
#define SET_EVENT_SKIP_FLAG(_pEvent)	((_pEvent)->bSkipFlag = TRUE)


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Link-layer Manager Sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* link-layer operation mode definition */
enum YDN_OPERATION_MODE 
{
	YDN_MODE_SERVER						= 0,
	YDN_MODE_CLIENT
};


/* communication status between MC and EventQueues */
enum YDN_COMM_STATUS                  
{
	YDN_COMM_STATUS_DISCONNECT		= 0,
	YDN_COMM_STATUS_NEXT,
	YDN_COMM_STATUS_SKIP
};


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Alarm Handler sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
enum YDN_ROUTE_TYPE
{
	YDN_ROUTE_ALARM_1   = 0,
	YDN_ROUTE_ALARM_2,
	YDN_ROUTE_ALARM_3
};

struct tagYDNAlarmHandler
{
	BYTE	byCurReportRouteType;	/* route type defined by YDN_ROUTE_TYPE */

	BOOL	bAlarmAtHand;	
	BYTE	byAlarmRetryCounter;	/* alarm report retried times       */
};
typedef struct tagYDNAlarmHandler YDN_ALARM_HANDLER;



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Config Builder sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* 1.YDN Model config info */
/* mapping info from ACU Model to YDN Model */
enum YDNSigType             /* Note: not change their values! */
{
	YDN_SIGTYPE_AI  = 0,
	YDN_SIGTYPE_AO,
	YDN_SIGTYPE_DI,
	YDN_SIGTYPE_DO
};

enum YDNAlarmLevel
{
	YDN_ALARM_LEVEL_A1    =0,
	YDN_ALARM_LEVEL_A2,
	YDN_ALARM_LEVEL_A3,
	YDN_ALARM_LEVEL_O1,
	YDN_ALARM_LEVEL_O2,
	YDN_ALARM_LEVEL_NUM
};


/* Added by Thomas for CR to distinguage the enable bit of YDN DI table,
   2006-2-28 */
// extend basic sig typed
enum SIG_TYPE_YDN_EX
{
	YDN_RESERVED = -1,
	YDN_ENABLE_BIT_OF_DI = -2,
	YDN_DISABLE_BIT_OF_DI = -3
};

// define YDN SPECIAL VALUEs, returned by RB* command
#define	YDN_RESERVED_VALUE		1
#define YDN_ENABLE_VALUE		1
#define YDN_DISABLE_VALUE		0
/* Added end by Thomas, 2006-2-28 */


struct tagYDNMapEntriesInfo	/* sig mapping */
{
	int	iYDNPort;			/* sig serial no. or command type*/
	int  iEquipType;         /* equip type ID*/
	int	iSCUPSigType;     	/* defined by SIG_TYPE enum */
	int	iSCUPSigID;	        /* signal ID in equip*/
	int  iFlag0;
	int  iFlag1;
	int  iLen;
	int  iStatusValue[5];
	float fPeakValue[5];
};
typedef struct tagYDNMapEntriesInfo YDN_MAPENTRIES_INFO;

struct tagYDNTypeMapInfo				  /* type mapping */
{
	int	 iCID1;	                  /* YDN23 command CID1 */
	int	 iCID2;			          /* YDN23 command CID2 */
	int	 iMapEntriesNum;	          /* the number of current YDN model type */ 
	int  iUnitNum;					  /* the number of units for current type */
	YDN_MAPENTRIES_INFO	*pMapEntriesInfo; /* sig mapping info */							
};
typedef struct tagYDNTypeMapInfo YDN_TYPE_MAP_INFO;


struct tagYDNModelConfigInfo          /* YDN model config file info */
{
	int	iTypeMapNum;	 
	YDN_TYPE_MAP_INFO	*pTypeMapInfo;	  /* type mapping info */
};
typedef struct tagYDNModelConfigInfo YDNMODEL_CONFIG_INFO;


/* 2.YDN model info */
#define YDN_MAX_DEVICE_NUM  80
#define YDN_MAX_RELATIVE_DEVICE_NUM	20
struct tagYDNBlockInfo	
{
	int	    iCID1;	
	int	    iCID2;	
    int	    iEquipID;	         /* relevant equip id of ACU model */
				
	YDN_TYPE_MAP_INFO  *pTypeInfo;	 /* reference of relevant type info */

};
typedef struct tagYDNBlockInfo YDN_BLOCK_INFO;

struct tagYDNModelInfo	 
{
	int				iBlocksNum;	    /* the number of blocks */
	YDN_BLOCK_INFO	*pYDNBlockInfo;	 
};
typedef struct tagYDNModelInfo YDN_MODEL_INFO;



/* 3.YDN common config info */
/* const definitions */
#define COMM_PORT_PARAM_LEN	   64

/* define the number of settable alarm phone number/network address */
#define YDN_ALARM_REPORT_NUM    3   

/* define the number of settable callback phone number */
#define YDN_CALLBACK_NUM		1   

/* define the number of security ip address */
#define YDN_SECURITY_IP_NUM		2

/* define max phone number length */
#define PHONENUMBER_LEN     20

#define IPADDRESS_LEN       30

/* YDN common config info */
struct tagYDNCommonConfig
{
	unsigned int	    byADR;                
	unsigned int		iProtocolType;	/* see enum YDN_PROTOCOL definition */
	unsigned int		iMediaType;	    /* see enum YDN_MEDIA_TYPE definition */
	char				szCommPortParam[COMM_PORT_PARAM_LEN];

	BOOL				bReportInUse;		
	
	/* max attemps to report alarms */
	int					iMaxAttempts;	
	/* elaps time between each attemps (unit: second) */
	int					iAttemptElapse;	

	/* phone number for alarm report*/
	char				szAlarmReportPhoneNumber[YDN_ALARM_REPORT_NUM][PHONENUMBER_LEN];

	char                 cModifyUser[40];//added by ht,2006.12.13, no use in common config file,just for system log.
};
typedef struct tagYDNCommonConfig YDN_COMMON_CONFIG;


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * structures for Frame Analysis sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
enum tagYDNFrameType
{
    ENOR = 0,		
    EPERR,		
    ECHKERR,		
    ELCHKERR,	
    ECID2ERR,	
    ECMDERR,		
    EDTERR,     
    ERERR = 0xE0,		
    ECTLFAIL,	
    EDEVFAIL,  
    EUNWRITE,   
    EREV1,       
    EREV2,       
    EREV3,      
    EREV4,      
    EREV5,      
    EREV6,      
    EREV7,      
    EREV8,       
    EREV9,       
    EREV10,      
    EREV11,      
    EREV12, 
	ERADDR,
	/* public frames */
	FRAME_SOI,        /*SOI*/
	FRAME_EOI,         /*EOT*/

	FRAME_ERR   /* incorrect frame */
};
typedef enum tagYDNFrameType YDN_FRAME_TYPE;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Command Handler sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* command execution function prototype definition */
typedef struct tagYDNBasicArgs YDN_BASIC_ARGS;
typedef int (*YDN_CMD_EXECUTE_FUN) (YDN_BASIC_ARGS *pThis,
								 unsigned char *szInData,
								 unsigned char *szOutData);

struct tagYDNCmdHandler
{
	const char        *szCmdType;
	BOOL              bIsWriteCommand;
	YDN_CMD_EXECUTE_FUN   funExecute;
};
typedef struct tagYDNCmdHandler YDN_CMD_HANDLER;

struct tagYDNPackDataHandler
{
	const char        *szCmdType;
	unsigned char     *pPackData;
};
typedef struct tagYDNPackDataHandler YDN_PACKDATA_HANDLER;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for State Machine sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* states definition */
/* states of YDN State Machine */
enum YDN_STATES
{
	YDN_STATE_MACHINE_QUIT  = -1,

	YDN_IDLE,
	YDN_WAIT_FOR_CMD,
	YDN_SEND_ALARM,
	YDN_WAIT_FOR_ALARM_ACK
};

/* on-state function prototype */
typedef int (*YDN_ON_STATE_PROC) (YDN_BASIC_ARGS *pThis);


struct tagYDNStateInfo
{
	int	iStateID;	                /* defined by YDN_STATES enum */
	YDN_ON_STATE_PROC	fnStateProc;	
}; 
typedef struct tagYDNStateInfo YDN_STATE_INFO;


/* alarm info structure */
struct tagYDNAlarmInfo
{
	char szStartTime[13];	/* alarm start time (YYMMDDHHMMSS) */
	char szEndTime[13];		/* alarm end time (YYMMDDHHMMSS)   */

	int	 iGroupID;	        /* Group ID */
	int	 iSubGroupID;	    /* Subgroup ID */
	int	 iUnitID;	        /* Unit ID */
	char cSection;			/* I for DI signal, O for DO signal */
	int	 iCno;				/* sig no of YDN model */

	/* 	alarm level of YDN Model, currently support :A1=0, A2=1, O1=3;
	 *  they are mapped to CA=3, MA=2, OA=1 in ACU model. */
	int	iAlarmLevel;	
};
typedef struct tagYDNAlarmInfo YDN_ALARM_INFO;



/* alarm info buffer */
struct tagYDNAlarmInfoBuffer
{
	/* alarm number for each level */
	int			iA1Num;
	int			iA2Num;
	int			iO1Num;

	int			iCurAlarmNum;	/* number of active alarms of the buf */
	YDN_ALARM_INFO  sCurAlarmInfo[YDN_ALARM_BUF_SIZE];      /* static buf */
	YDN_ALARM_INFO	*pCurAlarmInfo;	/* active alarm info, dynamic buf */

	int			iHisAlarmNum;	/* number of history alarms of the buf */
	YDN_ALARM_INFO	*pHisAlarmInfo;	/* history alarm info */

};
typedef struct tagYDNAlarmInfoBuffer YDN_ALARM_INFO_BUFFER;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * globals structures 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* basic arg structure */
struct tagYDNBasicArgs
{
	/* event queues */
	HANDLE	hEventInputQueue;	
	HANDLE	hEventOutputQueue;	

	/* data buffers */
	int					iTimeoutCounter; //used by YDN_IDEL and SOC_OFF state
	int					iResendCounter;  /*used when timeout
										   (by YDN_WAIT_FOR_RESP_ACK,
											   YDN_WAIT_FOR_ALARM_ACK,
											   YDN_WAIT_FOR_CALLBACK_ACK,
											   SOC_WAIT_DLE0, 
											   SOC_ALARM_REPORT ) */
	YDN_ALARM_INFO_BUFFER	AlarmBuffer;	
#define YDN_R_BUFF_SIZE			4114
	unsigned char       sReceivedBuff[YDN_R_BUFF_SIZE]; /* used for CHAE-based frame recognize */
	int					iCurPos;                        //for Received Buff
	int                 iLen;						    //for Received Buff
	unsigned char	    szCmdRespBuff[YDN_RESP_SIZE];	 // command reply data buf 	
	YDN_ALARM_HANDLER	    alarmHandler;	
	char                *szBattLog;  //battery log buf, size is YDN_BATTLOG_SIZE

	/* link-layer current operation mode */
	int					iOperationMode; /* use YDN_OPERATION_MODE const */

	/* communication handle */
	HANDLE	hComm;	

	
	/* hThreadID[0]: YDN Service thread id;
	 * hThreadID[1]: Linklayer thread id.
	 * hThreadID[2]: PackData thread id*/
	HANDLE  hThreadID[3];

	/* flags */
	int     iMachineState;    /* state machine current state */ 
	int		*pOutQuitCmd;	  /* YDN service exit command, from Main Module */
	int     iInnerQuitCmd;               /* use SERVICE_EXIT_CODE const     */
	int 	iLinkLayerThreadExitCmd;	 /* use THREAD_EXIT_CODE const      */
	int 	iPackDataThreadExitCmd;	 /* use THREAD_EXIT_CODE const      */
	BOOL	bServerModeNeedExit;	     /* server operation mode exit flag */
	BOOL	bProtocolChangedFlag;	     /* protocol type changed flag      */
	BOOL	bCommRunning;	             /* communication busy flag         */
	BOOL	bSecureLine;	             /* security connection flag        */
	BOOL    bFrameDone;                  /* used by frame split function    */
	BOOL	bADRChangedFlag;	     /* protocol type changed flag      */
};

struct tagRespDataBuffer
{
    unsigned char szRespData1[YDN_READ_CMD_NUM][MAX_YDNFRAME_LEN];// derectly send buffer
    unsigned char szRespData2[YDN_READ_CMD_NUM][MAX_YDNFRAME_LEN];// szRespData1's buffer
};
typedef struct tagRespDataBuffer YDN_RESPDATA_BUFF;
extern YDN_RESPDATA_BUFF g_RespData;

/* used by RX* command */
struct tagYDNRefText
{
	int iModuleNo;  //No. of SMIO module, range from 1 to 8
	int iRefNo;     //fixed Ref. Text No. for each SMIO module, from 1 to 18

	char *szText1;  //reference of SMIO signal name 
	char *szText2;  //reference of Unit for AIs

	int *piAlarmLevel;  //reference of High or Low AI alarm level 
};
typedef struct tagYDNRefText YDN_REF_TEXT;

/* used to wait for Muxtex */
#define TIME_WAIT_MUTEX_COMCFG  1000
#define TIME_WAIT_MUTEX_SWITCH  3000
struct tagYDNGlobals
{
	YDNMODEL_CONFIG_INFO YDNModelConfig;
	YDN_MODEL_INFO	YDNModelInfo;
	YDN_COMMON_CONFIG	CommonConfig;

	/* reference texts info of the solution (used for RX* command) */
	int iRefTexts;
	YDN_REF_TEXT	RefTexts[MAX_REF_TEXTS];

	YDN_CMD_HANDLER		CmdHandlers[YDN_CMD_NUM];
	YDN_PACKDATA_HANDLER PackDataHandlers[YDN_READ_CMD_NUM];
	YDN_STATE_INFO		YDNStates[YDN_STATE_NUM];
	EQUIP_INFO* pEquipInfo;
	int SMAddr;
	int iEquipNum;
	int iEquipIDOrder[MAX_EQUIP_NUM][2];
	//HANDLE  hMutexCommonCfg;  //for YDN common config file
};
typedef struct tagYDNGlobals YDN_GLOBALS;

/* global variable declare */
extern YDN_GLOBALS  g_YDNGlobals;

/* Simple Events declare */
extern YDN_EVENT g_YDNDummyEvent, g_YDNTimeoutEvent;
extern YDN_EVENT g_YDNDiscEvent, g_YDNConnFailEvent, g_YDNConnEvent;


/* interface */
/* Config Builder sub-module */
int YDN_InitConfig(void);

/* Linklayer Manager sub-module */
int YDN_LinkLayerManager(YDN_BASIC_ARGS *pThis);

/* pack response data sub-module*/
int YDN_PackData(YDN_BASIC_ARGS *pThis);

/* Alarm Handler sub-module */
void YDN_RegisterAlarm(YDN_BASIC_ARGS *pThis, BOOL bDelayedAlarm);
void YDN_SendAlarm(YDN_BASIC_ARGS *pThis);
void YDN_AlarmReported (YDN_BASIC_ARGS *pThis);

void YDN_RegisterCallback(YDN_BASIC_ARGS *pThis, BOOL bDealyedCallback);
void YDN_SendCallback(YDN_BASIC_ARGS *pThis);
void YDN_CallbackReported(YDN_BASIC_ARGS *pThis);

void YDN_ClientReportFailed(YDN_BASIC_ARGS *pThis, 
							BOOL bConnetedFailed);

/* Frame Analyse sub-module */
YDN_FRAME_TYPE YDN_AnalyseFrame(const unsigned char *pFrame, int iLen, 
							YDN_BASIC_ARGS *pThis);
void YDN_ExtractDataFromFrame(IN const unsigned char *pFrame,
						       IN int iFrameLen, 
						       OUT int *piDataLen,
						       OUT unsigned char *pCmdData);

void YDN_BuildYDNResponseFrame(IN const unsigned char *szResData,
							   IN  int iCID1,
	                           IN  YDN_FRAME_TYPE RTN,
							   IN  int iAddr,
							   OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen);

void YDN_BuildSOCResponseFrame(IN BOOL bSoc,
							   IN const unsigned char *szResData, 
						       IN BOOL bInsertCCID, 
						       IN const unsigned char *szSoc_proc, 
						       OUT unsigned char *pFrameData,
						       OUT int *piFrameDataLen);

/* Command Handler sub-module */
void YDN_InitCmdHander(void);

int YDN_DecodeAndPerform(YDN_BASIC_ARGS *pThis,
					        unsigned char * szCmdData);
void	SendAddress(unsigned char *szResData, OUT int *pDataLen);

		
/* State Machine sub-module */
/* for YDN state machine */
int YDN_OnIdle(YDN_BASIC_ARGS *pThis);
int YDN_OnWaitForCMD(YDN_BASIC_ARGS *pThis);
int YDN_OnWaitForRespACK(YDN_BASIC_ARGS *pThis);
int YDN_OnSendCallback(YDN_BASIC_ARGS *pThis);
int YDN_OnWaitForCallbackACK(YDN_BASIC_ARGS *pThis);
int YDN_OnSendAlarm(YDN_BASIC_ARGS *pThis);
int YDN_OnWaitForAlarmACK(YDN_BASIC_ARGS *pThis);
/*
// for SOC state machine
int SOC_OnOff(YDN_BASIC_ARGS *pThis);
int SOC_OnSendENQ(YDN_BASIC_ARGS *pThis);
int SOC_OnWaitDLE0(YDN_BASIC_ARGS *pThis);
int SOC_OnAlarmReport(YDN_BASIC_ARGS *pThis);
int SOC_OnWaitCmd(YDN_BASIC_ARGS *pThis);
int SOC_OnWaitACK(YDN_BASIC_ARGS *pThis);
*/

/* Utilities used by YDN Module */
BYTE YDN_GetADR(void);
int YDN_GetReportMsg(IN BOOL bAlarm, 
					 IN OUT char *szReportMsg, 
					 IN int iBufLen);
BYTE YDN_AsciiHexToChar(IN const unsigned char *pStr);
unsigned int YDN_CharToInt(IN const unsigned char *pStr, int iCNum);
BOOL YDN_IsStrAsciiHex(const unsigned char *pStr, int iLen);
BOOL YDN_CheckPhoneNumber(const char *szPhoneNumber);
BOOL YDN_CheckIPAddress(const char *szIPAddress);
BOOL YDN_CheckBautrateCfg(const char *szBautrateCfg);
BOOL YDN_CheckModemCfg(const char *szModemCfg);
unsigned char *YDN_GetUnprintableChr(IN unsigned char chr);
void YDN_PrintEvent(YDN_EVENT *pEvent);
void YDN_PrintState(int iState);
void YDN_PrintCommonCfg(YDN_COMMON_CONFIG *pConfig);
void YDN_ClearEventQueue(HANDLE hq, BOOL bDestroy);
void YDNCheckSum( IN const unsigned char   *pFrame ,	// ҪУ��Ĵ��봮
                  IN int		nLen ,		// ��ҪУ��Ĵ�����
				 OUT unsigned char *pChkBytes);	// ��У��Ľ��д��ô�

YDN_TYPE_MAP_INFO *GetTypeMap(int iCID1, int iCID2);
BOOL GetSCUPSigValues(BOOL bAnalogue,
						 YDN_MAPENTRIES_INFO *pEntriesInfo,
						 int iEquipID,
						 unsigned char *szOutData,
						 BOOL ucInvalidFlag,
						 unsigned char ucErrorRTN,
						 float fErrorRTN);
void YDN_IntToAscii(IN DWORD dValue, OUT unsigned char *cStr);
int YDN_ModifyCommonCfg(BOOL bServiceIsRunning,
						BOOL bByForce,
						YDN_BASIC_ARGS *pThis,
						int iItemID,
						YDN_COMMON_CONFIG *pUserCfg);

BOOL GetSCUPSingleSigValues(BOOL bAnalogue,
						 int iSCUPSigType,
						 int iSCUPSigID,
						 int iEquipID,
						 unsigned char *szOutData);
BOOL SetYDNSigValues(BOOL bAnalogue,
						 int iEquipID,
						 int iSigNum, 
						 YDN_MAPENTRIES_INFO *pEntriesInfo,
						 unsigned char *sInData);
unsigned char *ExchangeYDNSigValue(IN unsigned char *szchar, 
										   IN YDN_MAPENTRIES_INFO *pMapEntry);
BOOL YDN_CheckSPortParam(IN char *pSPortParam);

int IfValidWACP(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData);

int IfValidWDCP(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData);

int IfValidWSysStatus(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData);

int IfValidCRect(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData);
BOOL YDN_GetEquipIDOrder(int iEquipTypeID,int* iEquipNum);

BOOL YDN_GetEquiplistIDOrder(int iEquipID,int* iEquipNum);

BOOL bACDNoRespFlag;
BOOL bDCDNoRespFlag;
						
/* safe delete YDN event */
#define DELETE_YDN_EVENT(_pEvent) \
	do {                          \
	if ((_pEvent)->iEventType == YDN_FRAME_EVENT && !(_pEvent)->bDummyFlag) \
	{     \
		DELETE(_pEvent);  \
	}   \
	} while(0)

/* log utilities */
#define YDN_LOG_TEXT_LEN	256

/* error log */
#define LOG_YDN_E(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_ERROR, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: ERROR: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* extended error log(logged with error code) */
#define LOG_YDN_E_EX(_task, _szLogText, _errCode)  \
	(AppLogOut((_task), APP_LOG_ERROR, "%s(Error Code: %X).\n", \
	(_szLogText), (_errCode)),  \
	TRACE("[%s:%d]--%s: ERROR: %s(Error Code: %X).\n", __FILE__, __LINE__, \
	__FUNCTION__, (_szLogText), (_errCode)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* warning log */
#define LOG_YDN_W(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_WARNING, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: WARNING: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* info log */
#define LOG_YDN_I(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_INFO, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: MESSAGE: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime())) 


/* debug utilities */
#ifdef _DEBUG
	//#define _DEBUG_YDN_SERVICE       //for YDN Module debug 
	//#define _TEST_YDN_SERVICE        //for YDN Module test version
#endif

#ifdef _DEBUG_YDN_SERVICE
	#define _SHOW_YDN_EVENT_INFO
	//#define _SHOW_YDN_CONFIG_INFO

    /* show the current state name of the State Machine */
	#define _SHOW_YDN_CUR_STATE

	#define _DEBUG_YDN_LINKLAYER
	#define _DEBUG_YDN_CMD_HANDLER
	#define _DEBUG_YDN_REPORT
#endif //_DEBUG_YDN_SERVICE

#ifdef _TEST_YDN_SERVICE
	#define _TEST_YDN_BATTLOG
	//#define	_TEST_YDN_MODIFYCONFIG
	#define _TEST_DATA_PRECISION
	#define _TEST_YDN_QUEUE_SYNCHRONIZATION
#endif //_TEST_YDN_SERVICE


#ifdef _DEBUG_YDN_SERVICE
	#define TRACE_YDN_TIPS(_tipInfo)   \
		(TRACE("[%s:%d]--%s: Message: %s.\n", __FILE__, __LINE__, \
		 __FUNCTION__, (_tipInfo)), \
		 TRACE("Time is: %.3f\n", GetCurrentTime()))

	#define YDN_GO_HERE  TRACE("[%s:%d]--%s: Go here!.\n", __FILE__, \
					__LINE__, __FUNCTION__)
#else
	#define YDN_GO_HERE  1 ? (void)0 : (void)0
	#define TRACE_YDN_TIPS(_tipInfo)   1 ? (void)0 : (void)0
#endif //_DEBUG_YDN_SERVICE


/* switch with Maintenance service */
#define COM_SHARE_SERVICE_SWITCH

#ifdef COM_SHARE_SERVICE_SWITCH
#define YDN_IS_SHARE_COM(_iMediaType)  \
	((_iMediaType) != YDN_MEDIA_TYPE_TCPIP ? TRUE : FALSE)

BOOL YDN_IsMtnFrame(const unsigned char *pFrame, int iLen);
void YDN_BuildMtnResponseFrame(OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen);
#endif //COM_SHARE_SERVICE_SWITCH


#endif //__YDN_H_2004_10_09__

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : config_builder.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "ydn.h"

/* define splitter charater used by YDN config files */
#define SPLITTER		 ('\t')

/* config file names */
#define CONFIG_FILE_YDNMODELMAP	  "private/ydn23/YDNModelMap.cfg"
#define CONFIG_FILE_YDNCOMMON	  "private/ydn23/YDNCommonConfig.cfg"

/* section names of YDN common config file */
#define  PROTOCOL_TYPE			 "[Protocol Type]"
#define  YDN_ADR                "[ADR]"
#define  YDN_SOCID				 "[SOCID]"
#define  REPORT_IN_USE			 "[Report in use]"
#define  CALLBACK_IN_USE		 "[callback in use]"
#define  OPERATION_MEDIA		 "[Operation media]"
#define  MEDIA_PORT_PARAM		 "[Media Port Param]"
#define  MAX_REPORT_ATTAMPS		 "[Max alarm report attempts]"
#define  ELAPS_TIME				 "[Call elapse time]"

#define  REPORT_NUMBER_1		 "[Primary report number]"
#define  REPORT_NUMBER_2		 "[Secondary report number]"
#define  REPORT_NUMBER_3		 "[Third report number]"

#define  REPORT_IP_1			 "[Primary report IP-address]"
#define  REPORT_IP_2			 "[Secondary report IP-address]"

#define  SECURITYI_IP_1			 "[IP-address1]"
#define  SECURITYI_IP_2			 "[IP-address2]"

#define  SECURITY_LEVEL			 "[Security Level]"

/* config items of YDN common Config file */
#define  CFG_ITEM_NUM			 10     

/* max length of cfg itme */
#define  MAX_CFG_ITEM_LEN        100

/* used to detect modified config item */
#define YDN_CFG_ITEM_TEST(_nVarID, _ItemMask) \
	(((_nVarID) & (_ItemMask)) == (_ItemMask) || \
	 ((_nVarID) & YDN_CFG_ALL) == YDN_CFG_ALL)


/* sections names of YDN Model Map config file */
#define  TYPE_MAP_NUM			 "[TYPE_MAP_NUM]"
#define  YDN_TYPE_MAP_INFO_SEC       "[TYPE_MAP_INFO]"
/* note: will be extended at runtime */
#define  YDN_MAPENTRIES_INFO_SEC		 "[MAPENTRIES_INFO]"


/* max type maps of YDN Model config file */
#define  YDNMODEL_MAX_MAPS       50


/* to simplify log action when read YDN common config file */
#define LOG_YDN_COMMON_CFG(_section)  \
	(AppLogOut("Read YDN Common Config", APP_LOG_ERROR, "Read %s failed, " \
			"not such section or format is invalid.\n", (_section)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s failed, not such section or format" \
             " is invalid.\n", __FILE__, __LINE__, __FUNCTION__, (_section)))

#define LOG_YDN_COMMON_CFG_2(_section)  \
	(AppLogOut("Read YDN Common Config", APP_LOG_ERROR, "Read %s failed, " \
			"format is invalid.\n", (_section)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s failed, format is invalid.\n", \
            __FILE__, __LINE__, __FUNCTION__, (_section)))



/* simplify log function when read fields of a cfg table */
/* DT: data type error */
#define LOG_READ_TABLE_DT(_tableName, _fieldName)  \
	(AppLogOut("Load YDN Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"data type of %s field is wrong.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, data type of %s "  \
        "field is wrong.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* DR: data range exceeds */ 
#define LOG_READ_TABLE_DR(_tableName, _fieldName)  \
	(AppLogOut("Load YDN Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"%s field data exceeds the bound.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field data exceeds"  \
        "the bound.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* NO: not find the filed. may caused by bad format */
#define LOG_READ_TABLE_NO(_tableName, _fieldName) \
	(AppLogOut("Load YDN Model Config", APP_LOG_ERROR, "Read %s table failed, %s " \
		"field not found(please use the correct EMPTY FIELD character).\n", \
		(_tableName), (_fieldName)), \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field not found"  \
        "(please use the correct EMPTY FIELD character).\n", \
		__FILE__, __LINE__, __FUNCTION__, (_tableName), (_fieldName)))

/* ET: field is empty */
#define LOG_READ_TABLE_ET(_tableName, _fieldName) \
	(AppLogOut("Load YDN Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"%s field should not be empty.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field should not"  \
        "be empty.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))


/*==========================================================================*
 * FUNCTION : IsEmptyField
 * PURPOSE  : empty config item in YDN common cfg file
 * CALLS    : 
 * CALLED BY: LoadYDNCommonConfigProc
 * ARGUMENTS: const char  *pField : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-01 16:07
 *==========================================================================*/
static BOOL IsEmptyField(const char *pField)
{
	return (pField[0] == '-');
}


/*==========================================================================*
 * FUNCTION : LoadYDNCommonConfigProc
 * PURPOSE  : callback function called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: void  *pCfg       : 
 *            void  *pLoadToBuf : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-10 16:38
 *==========================================================================*/
static int LoadYDNCommonConfigProc(void *pCfg, void *pLoadToBuf)
{
	YDN_COMMON_CONFIG *pCommonCfg;
	int iRet, iBuf;

	pCommonCfg = (YDN_COMMON_CONFIG *)pLoadToBuf;

	/* 1.protocol type */
	iRet = Cfg_ProfileGetInt(pCfg, PROTOCOL_TYPE, &iBuf);
	//if (iRet != 1)
	//{
	//	LOG_YDN_COMMON_CFG("protocol type");
	//	return ERR_CFG_FAIL;
	//}
	/*check protocol type */
	/* 1: YDN23 */
	//if (iBuf < 1 || iBuf > 4)
	//{
	//	LOG_YDN_COMMON_CFG_2("Protocol Type");
	//	return ERR_CFG_BADCONFIG;
	//}
	//pCommonCfg->iProtocolType = iBuf;
	pCommonCfg->iProtocolType = YDN;

	/* 2.ADR */
	iRet = Cfg_ProfileGetInt(pCfg, YDN_ADR, &iBuf);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("ADR");
		return ERR_CFG_FAIL;
	}
    if (iBuf < 0 || iBuf > 255)
	{
		LOG_YDN_COMMON_CFG_2("ADR");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->byADR = (BYTE)iBuf;

	/* 3.report in use */
	iRet = Cfg_ProfileGetInt(pCfg, REPORT_IN_USE, &iBuf);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("Report In Use flag");
		return ERR_CFG_FAIL;
	}
	/* check value */
	switch (iBuf)
	{
	case 0:
		pCommonCfg->bReportInUse = FALSE;
		break;

	case 1:
		pCommonCfg->bReportInUse = TRUE;
		break;

	default:
		LOG_YDN_COMMON_CFG_2("Report In Use flag");
		return ERR_CFG_BADCONFIG;
	}

	/* 4.operation media */
	iRet = Cfg_ProfileGetInt(pCfg, OPERATION_MEDIA, &iBuf);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("Operation Media");
		return ERR_CFG_FAIL;
	}
	/* check value */
	if (iBuf < 0 || iBuf > 2)
	{
		LOG_YDN_COMMON_CFG_2("Operation Media");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->iMediaType = iBuf;


	/* 5.operation media port param */
	iRet = Cfg_ProfileGetString(pCfg, 
								MEDIA_PORT_PARAM, 
								pCommonCfg->szCommPortParam,
								COMM_PORT_PARAM_LEN);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("Media Port Param");
		return ERR_CFG_FAIL;
	}
	switch (pCommonCfg->iMediaType)
	{
	case YDN_MEDIA_TYPE_LEASED_LINE:
		if (!YDN_CheckBautrateCfg(pCommonCfg->szCommPortParam))
		{
			LOG_YDN_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;

	case YDN_MEDIA_TYPE_MODEM:
		//if (!YDN_CheckModemCfg(pCommonCfg->szCommPortParam))
		if (!YDN_CheckBautrateCfg(pCommonCfg->szCommPortParam))
		{
			LOG_YDN_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;

	case YDN_MEDIA_TYPE_TCPIP:
		if (!YDN_CheckIPAddress(pCommonCfg->szCommPortParam))
		{
			LOG_YDN_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;

	default:
		LOG_YDN_COMMON_CFG_2("Media Port Param");
		return ERR_CFG_BADCONFIG;
	}
	

	/* 6.max report attempts */
	iRet = Cfg_ProfileGetInt(pCfg, MAX_REPORT_ATTAMPS, &iBuf);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("Max Report Attempts");
		return ERR_CFG_FAIL;
	}
	pCommonCfg->iMaxAttempts = iBuf;

	/* 7.call elapse time */
	iRet = Cfg_ProfileGetInt(pCfg, ELAPS_TIME, &iBuf);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("Elapse Time");
		return ERR_CFG_FAIL;
	}
	pCommonCfg->iAttemptElapse = 1000 * iBuf; //SCU+ timer based on ms

	/* 8.Primary report number */
	iRet = Cfg_ProfileGetString(pCfg, 
								REPORT_NUMBER_1, 
								pCommonCfg->szAlarmReportPhoneNumber[0],
								PHONENUMBER_LEN);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("Primary report number");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szAlarmReportPhoneNumber[0]))
	{
		pCommonCfg->szAlarmReportPhoneNumber[0][0] = '\0';
	}
	else
	{
		if (!YDN_CheckPhoneNumber(pCommonCfg->szAlarmReportPhoneNumber[0]))
		{
			LOG_YDN_COMMON_CFG_2("Primary report number");
			return ERR_CFG_BADCONFIG;
		}
	}

	/* 9.Secondary report number */
	iRet = Cfg_ProfileGetString(pCfg, 
								REPORT_NUMBER_2, 
								pCommonCfg->szAlarmReportPhoneNumber[1],
								PHONENUMBER_LEN);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("Secondary report number");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szAlarmReportPhoneNumber[1]))
	{
		pCommonCfg->szAlarmReportPhoneNumber[1][0] = '\0';
	}
	else
	{
		if (!YDN_CheckPhoneNumber(pCommonCfg->szAlarmReportPhoneNumber[1]))
		{
			LOG_YDN_COMMON_CFG_2("Secondary report number");
			return ERR_CFG_BADCONFIG;
		}
	}

    /* 10.Third report number */
	iRet = Cfg_ProfileGetString(pCfg, 
								REPORT_NUMBER_3, 
								pCommonCfg->szAlarmReportPhoneNumber[2],
								PHONENUMBER_LEN);
	if (iRet != 1)
	{
		LOG_YDN_COMMON_CFG("Third report number");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szAlarmReportPhoneNumber[2]))
	{
		pCommonCfg->szAlarmReportPhoneNumber[2][0] = '\0';
	}
	else
	{
		if (!YDN_CheckPhoneNumber(pCommonCfg->szAlarmReportPhoneNumber[2]))
		{
			LOG_YDN_COMMON_CFG_2("Third report number");
			return ERR_CFG_BADCONFIG;
		}
	}
    
	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : LoadYDNCommonConfig
 * PURPOSE  : to load YDN common config file
 * CALLS    : 
 * CALLED BY: YDN_InitConfig
 * ARGUMENTS:  
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-18 20:56
 *==========================================================================*/
static int LoadYDNCommonConfig(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_YDNCOMMON, szCfgFileName, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadYDNCommonConfigProc,
		&g_YDNGlobals.CommonConfig);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : ParseTypeMapTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Cfg_LoadSingleTable
 * ARGUMENTS: char           *szBuf       : 
 *            YDN_TYPE_MAP_INFO  *pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 09:28
 *==========================================================================*/
static int ParseTypeMapTableProc(char *szBuf, YDN_TYPE_MAP_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);
	
	/* 1.jump Sequence id field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 2.jump Type Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 3.YDN23 command CID1 field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Types Map", "YDN23 command CID1");
		return 4;
	}
    if (!CFG_CheckHexNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Types Map", "YDN23 command CID1");
        return 4;    /* not a num, error */
    }
	sscanf(pField, "%x", &pStructData->iCID1);

	/* 4.YDN23 command CID2 field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Types Map", "YDN23 command CID2");
		return 5;
	}
    if (!CFG_CheckHexNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Types Map", "YDN23 command CID2");
        return 5;    /* not a num, error */
    }
	sscanf(pField, "%x", &pStructData->iCID2);

	return 0;
}

/* assistant function for judge enable bit of YDN DI table.
 * modified by Thomas for CR, 2005-2-28 */
BOOL YDN_IsEnableBit(IN const char *pField)
{
	if (*pField == 'E' && *(pField + 1) == 'N' &&
		*(pField + 2) == '\0')
	{
		return TRUE;
	}
	else 
	{
		return FALSE;
	}
}

BOOL YDN_IsDisableBit(IN const char *pField)
{
	if (*pField == 'D' && *(pField + 1) == 'N' &&
		*(pField + 2) == '\0')
	{
		return TRUE;
	}
	else 
	{
		return FALSE;
	}
}
/* Modified end, Thoams, 2006-2-28 */


/*==========================================================================*
 * FUNCTION : ParseMapEntriesTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: LoadYDNModelMapConfigProc
 * ARGUMENTS: char               *szBuf      : 
 *            YDN_MAPENTRIES_INFO *  pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 11:39
 *==========================================================================*/
static int ParseMapEntriesTableProc(char *szBuf, YDN_MAPENTRIES_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	char *ptempBuf;
	int i = 0;
	int iSplitCount = 0;

	/* 1.Port ID  */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Map Entries", "Port ID");
		return 2;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Map Entries", "Port ID");
		return 2;
	}
	pStructData->iYDNPort = atoi(pField);

    /* 2.jump Signal Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

    /* 3.M810G StdEquipType ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Types Map", "StdEquipType ID");
		return 3;
	}
	pStructData->iEquipType = atoi(pField);
	
	/* 4.M810G model signal type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))  // means no relevant sig type in M810G model
	{
		pStructData->iSCUPSigType = YDN_RESERVED;
		pStructData->iSCUPSigID = YDN_RESERVED;   /* just finish */
		return 0;
	}

	/* Added by Thomas for CR: using enable bit in DI table to distinguage the availability 
	 * of the front value, 2006-2-28. */
	else if (YDN_IsEnableBit(pField))
	{
		pStructData->iSCUPSigType = YDN_ENABLE_BIT_OF_DI;
		pStructData->iSCUPSigID = YDN_RESERVED;   /* just finish */
		return 0;
	}
	else if (YDN_IsDisableBit(pField))
	{
		pStructData->iSCUPSigType = YDN_DISABLE_BIT_OF_DI;
		pStructData->iSCUPSigID = YDN_RESERVED;   /* just finish */
		return 0;
	}
	/* modified end, Thomas, 2006-2-28 */

	else
	{
		if (*pField == '\0')
		{
			LOG_READ_TABLE_NO("Model Map Entries", "Model signal type");
			return 4;
		}

		/* check and assign(valid value: SP(sampling) ST(setting) C(control) A(alarm)) */
		switch (*pField)
		{
		case 'S':
			switch (*(pField + 1))
			{
			case 'P':   /* SP = sampling sig type */
				pStructData->iSCUPSigType = SIG_TYPE_SAMPLING;
				break;

			case 'T':   /* ST = setting sig type */
				pStructData->iSCUPSigType = SIG_TYPE_SETTING;
				break;

			default:    /* S? : invalid */
				LOG_READ_TABLE_DR("Model Map Entries", "Model signal type");
				return 4;
			}
			break;

		case 'C':       /* C = control sig type */
			pStructData->iSCUPSigType = SIG_TYPE_CONTROL;
			break;

		case 'A':		/* A = alarm sig type */
			pStructData->iSCUPSigType = SIG_TYPE_ALARM;
			break;

        case 'N':
        	break;
        	
		default:
			LOG_READ_TABLE_DR("Model Map Entries", "Model signal type");
			return 4;
		}
	}

	/* 5.Signal ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Map Entries", "Signal ID");
		return 5;
	}
	if (*pField == '*')
	{
		pStructData->iSCUPSigID = -1;
		return 0;    /* succeed return */
	}

	if (!CFG_CheckPNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Map Entries", "Signal ID");
		return 5;
	}
	pStructData->iSCUPSigID = atoi(pField);

	/* 6.Flag0 */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Map Entries", "Flag0");
		return 6;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Map Entries", "Flag0");
		return 6;
	}
	pStructData->iFlag0 = atoi(pField);

	/* 7.Flag1 */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Map Entries", "Flag1");
		return 7;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Map Entries", "Flag1");
		return 7;
	}
	pStructData->iFlag1 = atoi(pField);

	/*8.iStatusValue*/

	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	iSplitCount = Cfg_GetSplitStringCount(pField, '/');
	if(*pField != 'N')
	{
		while(i < iSplitCount)
		{
		    if(i != iSplitCount -1)
		    	{
		        pField = Cfg_SplitStringEx(pField, &ptempBuf, 0x2F);
		        //TRACE("\nptempBuf is %s\n", ptempBuf);
		        pStructData->iStatusValue[i] = atoi(ptempBuf);
				pStructData->fPeakValue[i] = atof(ptempBuf);
		    	}
		    
		    else
		    	{
		        //pField = Cfg_SplitStringEx(pField, &ptempBuf, SPLITTER);
		        //TRACE("\nptempBuf is %s\n", ptempBuf);
		        pStructData->iStatusValue[i] = atoi(pField);
				pStructData->fPeakValue[i] = atof(pField);
		    	}
		    	
		   // pStructData->iStatusValue[i] = atoi(ptempBuf);
		    //TRACE("\npStructData->iStatusValue[%d] is %d\n", i, pStructData->iStatusValue[i]);
		    i++;
		}
		pStructData->iLen = iSplitCount + 1;
	}
	else
		pStructData->iLen = -1;

	return 0;
}

/*==========================================================================*
 * FUNCTION : GetMapEntriesSecName
 * PURPOSE  : assistant function to get Map Entries Section Name
 * CALLS    : 
 * CALLED BY: LoadYDNModelMapConfigProc
 * ARGUMENTS: IN const char  *szBaseName       : 
 *            IN int         iMapIndex         : 
 *            OUT char       *szEntriesSecName : 
 *            IN int         iLen              : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : 
 *==========================================================================*/
static void GetMapEntriesSecName(IN const char *szBaseName, 
								 IN int iMapIndex,
								 OUT char *szEntriesSecName,
								 IN int iLen)
{
	char szFix[10], *p;
	int i;
    
	sprintf(szFix, "_MAP%d]%c", iMapIndex, '\0');
	strncpy(szEntriesSecName, szBaseName, (size_t)iLen);

	p = szEntriesSecName;
	for (i = iLen; *p != ']' && --i > 0; p++)
	{
		;
	}

	*p = '\0';

    i = strlen(szEntriesSecName);
	strncat(szEntriesSecName, szFix, (size_t)(iLen - i -1));

	return;
}

/*==========================================================================*
 * FUNCTION : LoadYDNModelMapConfigProc
 * PURPOSE  : callback funtion called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: LoadYDNModelMapConfig
 * ARGUMENTS: void         *pCfg       : 
 *            void         *pLoadToBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao
 *==========================================================================*/
static int LoadYDNModelMapConfigProc(void *pCfg, void *pLoadToBuf)
{
    YDNMODEL_CONFIG_INFO *pBuf;

	CONFIG_TABLE_LOADER loader[YDNMODEL_MAX_MAPS + 1];
	char szMapEntriesSecName[YDNMODEL_MAX_MAPS][30];
	int iMaps, i;
	YDN_TYPE_MAP_INFO *pMap;  /* used as buf */

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (YDNMODEL_CONFIG_INFO *)pLoadToBuf;


	/* load type map table first */
	DEF_LOADER_ITEM(&loader[0],
			TYPE_MAP_NUM, &(pBuf->iTypeMapNum), 
			YDN_TYPE_MAP_INFO_SEC, &(pBuf->pTypeMapInfo), 
			ParseTypeMapTableProc);

	if (Cfg_LoadTables(pCfg,1,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	/* then load map entries */
	iMaps = pBuf->iTypeMapNum;
	for (i = 0; i < iMaps; i++)
	{
		GetMapEntriesSecName(YDN_MAPENTRIES_INFO_SEC,
							 i + 1, 
			                 szMapEntriesSecName[i],
			                 30);

		pMap = pBuf->pTypeMapInfo + i;
		DEF_LOADER_ITEM(&loader[i + 1],
			NULL, &(pMap->iMapEntriesNum), 
			szMapEntriesSecName[i], &(pMap->pMapEntriesInfo), 
			ParseMapEntriesTableProc);
	}

	if (Cfg_LoadTables(pCfg,iMaps,loader + 1) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;

}

/*==========================================================================*
 * FUNCTION : LoadYDNModelMapConfig
 * PURPOSE  : to load YDN Model Map config file
 * CALLS    : Cfg_LoadConfigFile
 * CALLED BY: YDN_InitConfig
 * ARGUMENTS: 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-19 15:43
 *==========================================================================*/
static int LoadYDNModelMapConfig(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_YDNMODELMAP, szCfgFileName, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadYDNModelMapConfigProc,
		&g_YDNGlobals.YDNModelConfig);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

/* use SWAP	macro */
/* used to exchange member data of YDN_MAPENTRIES_INFO */
#define EXCHANGE_ENTRY(_entry1, _entry2, _buf)  \
	(_buf).iYDNPort = (_entry1).iYDNPort, \
	(_buf).iEquipType = (_entry1).iEquipType,\
	(_buf).iSCUPSigType = (_entry1).iSCUPSigType, \
	(_buf).iSCUPSigID = (_entry1).iSCUPSigID, \
	(_buf).iFlag0 = (_entry1).iFlag0, \
	(_buf).iFlag1 = (_entry1).iFlag1, \
	(_entry1).iYDNPort = (_entry2).iYDNPort, \
	(_entry1).iEquipType = (_entry2).iEquipType,\
	(_entry1).iSCUPSigType = (_entry2).iSCUPSigType, \
	(_entry1).iSCUPSigID = (_entry2).iSCUPSigID, \
	(_entry1).iFlag0 = (_entry2).iFlag0, \
	(_entry1).iFlag1 = (_entry2).iFlag1, \
	(_entry2).iYDNPort = (_buf).iYDNPort, \
	(_entry2).iEquipType = (_buf).iEquipType,\
	(_entry2).iSCUPSigType = (_buf).iSCUPSigType, \
	(_entry2).iSCUPSigID = (_buf).iSCUPSigID), \
	(_entry2).iFlag0 = (_buf).iFlag0, \
	(_entry2).iFlag1 = (_buf).iFlag1, \


/*==========================================================================*
 * FUNCTION : GetTypeMap
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CreateYDNBlocks
 * ARGUMENTS: int  iStdEquipID : 
 * RETURN   : YDN_TYPE_MAP_INFO : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 19:39
 *==========================================================================*/
YDN_TYPE_MAP_INFO *GetTypeMap(int iCID1, int iCID2)
{
	int i, iTypeMapNum;
	YDN_TYPE_MAP_INFO *pTypeMap;

	pTypeMap = g_YDNGlobals.YDNModelConfig.pTypeMapInfo;
	iTypeMapNum = g_YDNGlobals.YDNModelConfig.iTypeMapNum;

	for (i = 0; i < iTypeMapNum; i++, pTypeMap++)
	{
		/* normal process */
		if (pTypeMap->iCID1 == iCID1 && pTypeMap->iCID2 == iCID2)
		{
			return pTypeMap;
		}
	}

	return NULL;
}

/*==========================================================================*
 * FUNCTION : GetUnitNum
 * PURPOSE  : get created Unit numbers of a group at present
 * CALLS    : 
 * CALLED BY: CreateYDNBlocks
 * ARGUMENTS: int  iGroupID : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 10:21
 *==========================================================================*/
static int GetUnitNum(int iCID1)
{
	int i, iTypeMapNum, iUnitNum;
	YDNMODEL_CONFIG_INFO *pModelConfig;
	YDN_TYPE_MAP_INFO *pCurTypeMap;

	pModelConfig = &g_YDNGlobals.YDNModelConfig;
	iTypeMapNum = pModelConfig->iTypeMapNum;
	pCurTypeMap = pModelConfig->pTypeMapInfo;
	for (i = 0, iUnitNum = 0; i < iTypeMapNum; i++, pCurTypeMap++)
	{
		if (pCurTypeMap->iCID1 == iCID1 && 
					pCurTypeMap->iUnitNum > iUnitNum)
		{
			iUnitNum = pCurTypeMap->iUnitNum;		 
		}
	}

	return iUnitNum;
}

/*==========================================================================*
 * FUNCTION : SetUnitNum
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CreateYDNBlocks
 * ARGUMENTS: int  iGroupID : 
 *            int  iUnitNum : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 10:31
 *==========================================================================*/
static void SetUnitNum(int iCID1, int iUnitNum)
{
	int i, iTypeMapNum;
	YDNMODEL_CONFIG_INFO *pModelConfig;
	YDN_TYPE_MAP_INFO *pCurTypeMap;

	pModelConfig = &g_YDNGlobals.YDNModelConfig;
	iTypeMapNum = pModelConfig->iTypeMapNum;
	pCurTypeMap = pModelConfig->pTypeMapInfo;
	for (i = 0; i < iTypeMapNum; i++, pCurTypeMap++)
	{
		if (pCurTypeMap->iCID1 == iCID1)
		{
			pCurTypeMap->iUnitNum = iUnitNum;		 
		}
	}

	return;
}

/* Added by Thomas begin, support product info. 2006-2-9 */
#ifdef PRODUCT_INFO_SUPPORT
/*==========================================================================*
 * FUNCTION : AttachDevice
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-2-9 14:49
 *==========================================================================*/
//static BOOL AttachDevice(void)
//{
//	int i, j, iEquipID, iBlockNum, iDeviceNum, iIndex;
//	YDN_BLOCK_INFO *pBlock;
//
//	pBlock = g_YDNGlobals.YDNModelInfo.pYDNBlockInfo;
//	iBlockNum = g_YDNGlobals.YDNModelInfo.iBlocksNum;
//
//	iDeviceNum = DXI_GetDeviceNum();
//	if (iDeviceNum > YDN_MAX_DEVICE_NUM)
//	{
//		AppLogOut("Creat YDN Model", APP_LOG_ERROR,
//			"YDN_MAX_DEVICE_NUM is not big enough");
//		return FALSE;
//	}
//
//	for (i = 0; i < iBlockNum; i++, pBlock++)
//	{
//		iEquipID = pBlock->iEquipID;
//
//		pBlock->iRelatedDeviceNum = 0;
//
//		//Device ID started from 1
//		for (j = 1; j <= iDeviceNum; j++)
//		{
//			if (DXI_IsDeviceRelativeEquip(j, iEquipID))
//			{
//				pBlock->iRelatedDeviceNum++;
//				iIndex = pBlock->iRelatedDeviceNum - 1;
//
//				if (iIndex > YDN_MAX_DEVICE_NUM)
//				{
//					AppLogOut("Creat YDN Model", APP_LOG_ERROR,
//						"YDN_MAX_DEVICE_NUM is not big enough");
//					return FALSE;
//				}
//
//				pBlock->piRelatedDeviceIndex[iIndex] = j;
//
//			}
//		}
//
//	}
//
//	return TRUE; 
//}
#endif //PRODUCT_INFO_SUPPORT
/* Added by Thomas end, support product info. 2006-2-9 */

/*==========================================================================*
 * FUNCTION : OrderEquipID
 * PURPOSE  : 
 * CALLS    : GetUnitNum
 *			  SetUnitNum
 * CALLED BY: CreateYDNBlocks
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 19:45
 *==========================================================================*/
 static void OrderEquipID(int EquipNum, EQUIP_INFO* pEquipInfo)
{
   int i, j, k, iError, iBufLen;
   int iEquipTypeID;
   int iStdEquipTypeNum;
   STDEQUIP_TYPE_INFO *pStdEquiPTypeInfo;
   EQUIP_INFO *pCurEquip;

   iError = DxiGetData(VAR_STD_EQUIPS_LIST, 0, 0, &iBufLen, &pStdEquiPTypeInfo, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Order EquipID",APP_LOG_ERROR, 
			"Get StdEquip List through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		//return ERR_CFG_FAIL;
		return;
	}

   iError = DxiGetData(VAR_STD_EQUIPS_NUM, 0, 0, &iBufLen, &iStdEquipTypeNum, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Order EquipID",APP_LOG_ERROR, 
			"Get StdEquip Num through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		//return ERR_CFG_FAIL;
		return;
	}

	k = 0;
   for(i = 0; i < iStdEquipTypeNum; i++, pStdEquiPTypeInfo++)
   {
	   pCurEquip = pEquipInfo;
       for(j = 0; j < EquipNum; j++, pCurEquip++)
       {
		   iEquipTypeID = pCurEquip->iEquipTypeID;
		   if(iEquipTypeID == pStdEquiPTypeInfo->iTypeID)
		   {
			   g_YDNGlobals.iEquipIDOrder[k][0] = pCurEquip->iEquipID;
			   g_YDNGlobals.iEquipIDOrder[k][1] = pCurEquip->iEquipTypeID;
			   k++;
		   }
       }
   }
}
/*==========================================================================*
 * FUNCTION : CreateYDNBlocks
 * PURPOSE  : 
 * CALLS    : GetUnitNum
 *			  SetUnitNum
 * CALLED BY: YDN_InitConfig
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-18 19:45
 *==========================================================================*/
static int CreateYDNBlocks(void)
{
    EQUIP_INFO* pEquipInfo;//, *pCurEquip;
	YDN_TYPE_MAP_INFO *pCurTypeMap;
	int iTypeMapNum, iEquipNum;
	int iInterfaceType, iBufLen, iError;


	//YDN_BLOCK_INFO *pBlocks;
	//YDN_TYPE_MAP_INFO *pTypeMap;
	// 1.get equip list 
	iInterfaceType = VAR_ACU_EQUIPS_LIST;

	iError = DxiGetData(iInterfaceType, 0, 0, &iBufLen, &pEquipInfo, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Create YDN Blocks",APP_LOG_ERROR, 
			"Get Equip List through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		return ERR_CFG_FAIL;
	}
    iEquipNum = iBufLen/sizeof(EQUIP_INFO);
    g_YDNGlobals.iEquipNum = iEquipNum;
    //TRACE("\n g_YDNGlobals.iEquipNum is: %d\n", g_YDNGlobals.iEquipNum);
    g_YDNGlobals.pEquipInfo = pEquipInfo;
    
 	pCurTypeMap = g_YDNGlobals.YDNModelConfig.pTypeMapInfo;
	iTypeMapNum = g_YDNGlobals.YDNModelConfig.iTypeMapNum;

    OrderEquipID(iEquipNum, pEquipInfo);

	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : *GetAlarmLevelRef
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CreateRefTexts
 * ARGUMENTS: BOOL            bHigh          : 
 *            int             iAlarmSigNum   : 
 *            ALARM_SIG_INFO  *pAlarmSigInfo : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-19 12:08
 *==========================================================================*/
__INLINE static int *GetAlarmLevelRef(BOOL bHigh,
									  int iAlarmSigNum,
									  ALARM_SIG_INFO *pAlarmSigInfo)
{
	int i;
	int iSigID;

	for (i = 0; i < iAlarmSigNum; i++, pAlarmSigInfo++)
	{
		iSigID = pAlarmSigInfo->iSigID;
		if (bHigh && iSigID % 2 == 1 && iSigID <= 11)
		{
			return &pAlarmSigInfo->iAlarmLevel;
		}

		if (!bHigh && iSigID % 2 == 0 && iSigID <= 12)
		{
			return &pAlarmSigInfo->iAlarmLevel;
		}
	}

	return NULL;
}


/*==========================================================================*
 * FUNCTION : YDN_InitConfig
 * PURPOSE  : 
 * CALLS    : LoadYDNCommonConfig  
 *			  LoadYDNModelMapConfig
 *			  SortMapEntries
 *			  CreateYDNBlocks
 * CALLED BY: InitYDNGlobals (service_provider.c)
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 20:40
 *==========================================================================*/
int YDN_InitConfig()
{
	int iMapNum;
	YDN_TYPE_MAP_INFO *pTypeInfo;  /* for sorting mapentries */

	/* read YDN Common Config */
	if (LoadYDNCommonConfig() != ERR_CFG_OK )
	{
		return ERR_CFG_FAIL;
	}

	/* read YDN Model Config */
	if (LoadYDNModelMapConfig() != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	//print model config info
	iMapNum = g_YDNGlobals.YDNModelConfig.iTypeMapNum;
	pTypeInfo = g_YDNGlobals.YDNModelConfig.pTypeMapInfo;
	
#ifdef _SHOW_YDN_CONFIG_INFO
	{
	    //BLOCK_MAPENTRIES *pBlockMapEntries;
	    int j;
		YDN_MAPENTRIES_INFO  *pMapEntries;
		iMapNum = g_YDNGlobals.YDNModelConfig.iTypeMapNum;
		pTypeInfo = g_YDNGlobals.YDNModelConfig.pTypeMapInfo;

		printf("After sorting...\n");

		for (i = 0; i < iMapNum; i++, pTypeInfo++)
		{
			printf("Now is Map %d(after sorting)\n", i + 1);

			//AIs
			pMapEntries = pTypeInfo->pMapEntriesInfo;
			printf("the number of signals: %d\n", pTypeInfo->iUnitNum);
			for (j = 0; j < pTypeInfo->iMapEntriesNum; j++, pMapEntries++)
			{
				printf("YDN Port: %d\t", pMapEntries->iYDNPort);
				printf("Equip Type: %d\t", pMapEntries->iEquipType);
				printf("Signal Type: %d\t", pMapEntries->iSCUPSigType);
				printf("Signal ID: %d\t", pMapEntries->iSCUPSigID);
				printf("Signal Flag0: %d\t", pMapEntries->iFlag0);
				printf("Signal Flag1: %d\n", pMapEntries->iFlag1);
			}
		}
	}
#endif //_SHOW_YDN_CONFIG_INFO
   
	/* init YDN Model */
    if (CreateYDNBlocks() != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}


/* function prototype for YDN common config file item modification */
typedef BOOL (*HANDLE_CFG_ITEM) (int nVarID, 
						YDN_COMMON_CONFIG *pUserCfg, 
						CONFIG_FILE_MODIFIER *pModififier,
						char *szContent);

/*==========================================================================*
 * FUNCTION : HandleProtocolType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:16
 *==========================================================================*/
static BOOL HandleProtocolType(int nVarID, 
						YDN_COMMON_CONFIG *pUserCfg, 
						CONFIG_FILE_MODIFIER *pModifier,
						char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_PROTOCOL_TYPE))
	{
		sprintf(szContent, "%d%c", pUserCfg->iProtocolType, '\0');

		DEF_MODIFIER_ITEM(pModifier, 
			PROTOCOL_TYPE, 
			0, 
			0, 
			szContent);
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleADR
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModifier, char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 10:16
 *==========================================================================*/
static BOOL HandleADR(int nVarID, 
					   YDN_COMMON_CONFIG *pUserCfg, 
					   CONFIG_FILE_MODIFIER *pModifier,
					   char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_ADR))
	{
		sprintf(szContent, "%d%c", pUserCfg->byADR, '\0');

		DEF_MODIFIER_ITEM(pModifier, 
			YDN_ADR, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleReportInUse
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:25
 *==========================================================================*/
static BOOL HandleReportInUse(int nVarID, 
							  YDN_COMMON_CONFIG *pUserCfg, 
							  CONFIG_FILE_MODIFIER *pModififier,
							  char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_REPORT_IN_USE))
	{
		pUserCfg->bReportInUse == TRUE ?  (void)strcpy(szContent, "1") :
				(void)strcpy(szContent, "0");

		DEF_MODIFIER_ITEM(pModififier, 
			REPORT_IN_USE, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleMediaType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:31
 *==========================================================================*/
static BOOL HandleMediaType(int nVarID, 
							YDN_COMMON_CONFIG *pUserCfg, 
							CONFIG_FILE_MODIFIER *pModififier,
							char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_MEDIA_TYPE))
	{
		sprintf(szContent, "%d%c", pUserCfg->iMediaType, '\0');
	
		DEF_MODIFIER_ITEM(pModififier, 
			OPERATION_MEDIA, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


static BOOL HandleCommPortParam(int nVarID, 
								YDN_COMMON_CONFIG *pUserCfg, 
								CONFIG_FILE_MODIFIER *pModififier,
								char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_MEDIA_PORT_PARAM) ||
		YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_MEDIA_TYPE))
	{
		strcpy(szContent, pUserCfg->szCommPortParam);
	
		DEF_MODIFIER_ITEM(pModififier, 
			MEDIA_PORT_PARAM, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleAttempts
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:33
 *==========================================================================*/
static BOOL HandleAttempts(int nVarID, 
						   YDN_COMMON_CONFIG *pUserCfg, 
						   CONFIG_FILE_MODIFIER *pModififier,
						   char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_MAX_ATTEMPTS))
	{
		sprintf(szContent, "%d%c", pUserCfg->iMaxAttempts, '\0');

		DEF_MODIFIER_ITEM(pModififier, 
			MAX_REPORT_ATTAMPS, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleElapseTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:38
 *==========================================================================*/
static BOOL HandleElapseTime(int nVarID, 
							 YDN_COMMON_CONFIG *pUserCfg, 
							 CONFIG_FILE_MODIFIER *pModififier,
							 char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_ATTEMPT_ELAPSE))
	{
		/* note: ACU based on ms */
		sprintf(szContent, "%d%c", pUserCfg->iAttemptElapse/1000, '\0');

		DEF_MODIFIER_ITEM(pModififier, 
			ELAPS_TIME, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleRP_Number1
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:40
 *==========================================================================*/
static BOOL HandleRP_Number1(int nVarID, 
							 YDN_COMMON_CONFIG *pUserCfg, 
							 CONFIG_FILE_MODIFIER *pModifier,
							 char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_REPORT_NUMBER_1))
	{
		//for empty string type config item, fill '-'
		if (pUserCfg->szAlarmReportPhoneNumber[0][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szAlarmReportPhoneNumber[0]);
		}

		DEF_MODIFIER_ITEM(pModifier, 
			REPORT_NUMBER_1, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleRP_Number2
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModifier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:42
 *==========================================================================*/
static BOOL HandleRP_Number2(int nVarID, 
							 YDN_COMMON_CONFIG *pUserCfg, 
							 CONFIG_FILE_MODIFIER *pModifier,
							 char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_REPORT_NUMBER_2))
	{
		if (pUserCfg->szAlarmReportPhoneNumber[1][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szAlarmReportPhoneNumber[1]);
		}

		DEF_MODIFIER_ITEM(pModifier, 
			REPORT_NUMBER_2, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleRP_Number3
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            YDN_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModifier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 10:42
 *==========================================================================*/
static BOOL HandleRP_Number3(int nVarID, 
							 YDN_COMMON_CONFIG *pUserCfg, 
							 CONFIG_FILE_MODIFIER *pModifier,
							 char *szContent)
{
	if (YDN_CFG_ITEM_TEST(nVarID, YDN_CFG_REPORT_NUMBER_3))
	{
		if (pUserCfg->szAlarmReportPhoneNumber[2][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szAlarmReportPhoneNumber[2]);
		}

		DEF_MODIFIER_ITEM(pModifier, 
			REPORT_NUMBER_3, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : YDN_GetModifiedFileBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID       : 
 *            YDN_COMMON_CONFIG  *pUserCfg    : 
 *            char           **pszOutFile : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 14:03
 *==========================================================================*/
BOOL YDN_GetModifiedFileBuf(int nVarID, 
							YDN_COMMON_CONFIG *pUserCfg,
							char **pszOutFile)
{
	CONFIG_FILE_MODIFIER Modififier[CFG_ITEM_NUM];

	/* note: MAX_CFG_ITEM_LEN should be long enough! */
	char szCfgFileName[MAX_FILE_PATH]; 
	char szContent[CFG_ITEM_NUM][MAX_CFG_ITEM_LEN];
	int iRet, iIndex, iModifiers;
	BOOL bModified;

	const char *pLogText;  //for log

	HANDLE_CFG_ITEM fnHandlers[CFG_ITEM_NUM];


	/* init the handlers */
	fnHandlers[0] = HandleProtocolType;
	fnHandlers[1] = HandleADR;
	fnHandlers[2] = HandleReportInUse;
	

	fnHandlers[3] = HandleMediaType;
	fnHandlers[4] = HandleCommPortParam;

	fnHandlers[5] = HandleAttempts;
	fnHandlers[6] = HandleElapseTime;
	fnHandlers[7] = HandleRP_Number1;
	fnHandlers[8] = HandleRP_Number2;
    fnHandlers[9] = HandleRP_Number3;
	

	
	/* fill Modififier one by one */
	iModifiers = 0;
	for (iIndex = 0; iIndex < CFG_ITEM_NUM; iIndex++)
	{
		bModified = fnHandlers[iIndex](nVarID, 
			pUserCfg,
			&Modififier[iModifiers],
			szContent[iModifiers]);

		if (bModified)
		{
			iModifiers++;
		}
	}

	/* get the file buf */
	Cfg_GetFullConfigPath(CONFIG_FILE_YDNCOMMON, szCfgFileName, MAX_FILE_PATH);
	iRet = Cfg_ModifyConfigFile(szCfgFileName,
						 iModifiers,
						 Modififier,
						 pszOutFile);


	/* to simplify log action */
#define TASK_NAME        "Modifify YDN common config file"
#define LOG_MODIFY_FILE(_pLogText)  \
	(AppLogOut(TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	switch (iRet)
	{
	case ERR_CFG_OK:
		break;

	case ERR_CFG_FILE_OPEN:
		pLogText = "Open YDN Common Config file for modification failed.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_NO_MEMORY:
		pLogText = "No memory.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_FILE_READ:
		pLogText = "Read YDN Common Config file for modification failed.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_PARAM:
		pLogText = "Something is wrong with CONFIG_FILE_MODIFIER param for "
			"modification.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	default:
		pLogText = "Unhandled error when create new YDN Common Config file"
			" in memory.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;
	}

	return TRUE;
}
		

/*==========================================================================*
 * FUNCTION : YDN_UpdateCommonConfigFile
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 14:20
 *==========================================================================*/
BOOL YDN_UpdateCommonConfigFile(const char *szContent)
{
	FILE *pFile;
	char szCfgFileName[MAX_FILE_PATH]; 
	size_t count;

	Cfg_GetFullConfigPath(CONFIG_FILE_YDNCOMMON, szCfgFileName, MAX_FILE_PATH);
	pFile = fopen(szCfgFileName, "w");
	if (pFile == NULL)
	{
		return FALSE;
	}

	count = strlen(szContent);
	if (fwrite(szContent, 1, count, pFile) != count)
	{
		fclose(pFile);
		return FALSE;
	}

	fclose(pFile);

	return TRUE;
}

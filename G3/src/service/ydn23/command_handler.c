/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : command_handler.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "ydn.h"
#include <signal.h>

/* buff size for one type sig.  AI(0-63) AO(0-63) DI(0-255) DO(0-255) */
#define YDN_SIGTYPE_SIZE   512

/* YDN time format */
#define YDN_TIME_FORMAT    "%y%m%d%H%M%S"
//#define YDN_TIME_FORMAT    "%X%X%X%X%X%X"


/* float value format definition */
struct tagIEEEFloat
{
	unsigned a:1;
	unsigned b:8;
	unsigned c:22;
	unsigned d:1;	 
};
typedef struct tagIEEEFloat IEEE_FLOAT;

struct tagYDNFloat
{
	unsigned a:1;
	unsigned b:1;
	unsigned c:22;
	unsigned d:8;	
};
typedef struct tagYDNFloat YDN_FLOAT;

union IEEE_FVALUE
{
	float fValue;
	unsigned char   cValue[4];
	IEEE_FLOAT fIEEE;
};

union YDN_FVALUE
{
	float fValue;
	unsigned char   cValue[4];
	YDN_FLOAT fYDN;
};


/* log Macros for Command Handler sub-module */
#define YDN_TASK_CMD		"YDN Command process"

/* error log */
#define LOG_YDN_CMD_E(_szLogText)  LOG_YDN_E(YDN_TASK_CMD, _szLogText)

/* extended error log (logged with error code) */
#define LOG_YDN_CMD_E_EX(_szLogText, _errCode)  \
	LOG_YDN_E_EX(YDN_TASK_CMD, _szLogText, _errCode)

/* warning log */
#define LOG_YDN_CMD_W(_szLogText)  LOG_YDN_W(YDN_TASK_CMD, _szLogText)

/* info log */
#define LOG_YDN_CMD_I(_szLogText)  LOG_YDN_I(YDN_TASK_CMD, _szLogText)
	
/* used to init command handlers */
#define DEF_YDN_CMD_HANDLER(_p, _szCmdType, _bWriteFlag, _funExcute) \
          ((_p)->szCmdType = (_szCmdType), \
		   (_p)->bIsWriteCommand = (_bWriteFlag), \
		   (_p)->funExecute = _funExcute)


#define IS_ALARMBLOCKED_ALARM(_pAlarmSigValue)  \
	((_pAlarmSigValue)->pEquipInfo->iEquipTypeID == STD_ID_ACU_SYSTEM_EQUIP &&  \
			(_pAlarmSigValue)->pStdSig->iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_A_ALARM_OUTGOING_BLOCKED))

/* test battery log */
#ifdef _TEST_YDN_BATTLOG
  int iBattLogID;
#endif //_TEST_YDN_BATTLOG

__INLINE static float GetAnalogFromSBV(SIG_BASIC_VALUE* pBv)
{
	VAR_VALUE *pVarValue;
	float fRetValue = 0;

	if (pBv != NULL)
	{
		pVarValue = &(pBv->varValue);
		switch (pBv->ucType)
		{
		case VAR_LONG:
			fRetValue = (float)pVarValue->lValue;
			break;

		case VAR_FLOAT:
			fRetValue = pVarValue->fValue;
			break;

		case VAR_UNSIGNED_LONG:
			fRetValue = (float)pVarValue->ulValue;
			break;

		case VAR_DATE_TIME:
			fRetValue = (float)pVarValue->dtValue;

#ifdef _TEST_DATA_PRECISION
			if (pVarValue->dtValue)
			{
				TRACEX("Actual time:      %ld\n", pVarValue->dtValue);
				TRACEX("After convertion: %f\n", fRetValue);
			}
#endif //_TEST_DATA_PRECISION

			break;

		case VAR_ENUM:
			fRetValue = (float)pVarValue->enumValue;
		}
	}

	return fRetValue;
}

__INLINE static SIG_ENUM GetDigitalFromSBV(SIG_BASIC_VALUE* pBv)
{
	/* for multi ACU equip type mapping to one YDN equip type, 
	   some signal may have not been defined in ACU model,
	   so pBv is NULL at the case. eg. Phase B current is not defined 
	   in SCUACUnit2 (803).
     */
	return (pBv == NULL ? 0 : pBv->varValue.enumValue);	
}


/*==========================================================================*
 * FUNCTION : *GetAnalogueStr
 * PURPOSE  : change float value to YDN analogue data format
 * CALLS    : 
 * CALLED BY: GetSigValues
 * ARGUMENTS: BOOL bReserved:
 *			  float  fValue : 
 *			  unsigned char *strValue: length of the buf is 9
 * RETURN   : unsigned char *: 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 10:21
 *==========================================================================*/
static unsigned char *GetAnalogueStr(BOOL bReserved, float fValue, 
									 unsigned char *strValue)
{
	union IEEE_FVALUE ieee_uf;
	union YDN_FVALUE YDN_uf;
	union YDN_FVALUE tmpYNDFValue;

	int i;

	/* for reserved values */
	if (!bReserved)
	{
		strcpy(strValue, "7FFFFF80");
		return strValue;
	}

	/* normal value */
	/* 1.endian process */
	YDN_uf.fValue = fValue;
	tmpYNDFValue.fValue = fValue;
    #ifdef  _CPU_BIG_ENDIAN  //for ppc
		ieee_uf.fValue = fValue;

    #else  //for x86
	{
	   union IEEE_FVALUE tmpFValue;
	   tmpFValue.fValue = fValue;
		for (i = 0; i < 4; i++)
		{
			ieee_uf.cValue[i] = tmpFValue.cValue[3-i];
		}
		
	}
    #endif 
    
	for (i = 0; i < 4; i++)
	{
	    //YDN_uf.cValue[i] = tmpYNDFValue.cValue[3-i];
		YDN_uf.cValue[i] = tmpYNDFValue.cValue[i];
		HexToTwoAsciiData(YDN_uf.cValue[i], strValue  + 2*i);
	}
	
	return strValue;
}

/*==========================================================================*
 * FUNCTION : Power
 * PURPOSE  : simple assistant function
 * CALLS    : 
 * CALLED BY: GetDigitalStr
 * ARGUMENTS: int  x : 
 *            int  y : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 10:46
 *==========================================================================*/
static int Power(int x, int y)
{
	int i;
	int value = 1;

	for (i = 0; i < y; i++)
	{
		value *= x;
	}

	return value;
}

/*==========================================================================*
 * FUNCTION : ConvertTime
 * PURPOSE  : conver ydn23  time to UTC or convert UTC time to ydn23 time, ydn23 time zone is 8
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: time_t* pTime    : 
 *            BOOL bUTCToLocal  : TRUE, UTC time to ydn23 time , that is to add the zone8 , else
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-11-10 15:00
 *==========================================================================*/
BOOL ConvertTime(time_t* pTime, BOOL bUTCToYDN)
{
	SIG_BASIC_VALUE* pSigValue;

	int nBufLen;

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
		SIG_ID_TIME_ZONE_SET,
		&nBufLen,			
		&pSigValue,			
		0) != ERR_DXI_OK)
	{	
		return FALSE;
	}

	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{	
		int nTimeOffset = (pSigValue->varValue.lValue) * 3600 / 2;

		if (bUTCToYDN)
		{
			*pTime += nTimeOffset;
		}
		else//ydn23 time to UTC time
		{
			*pTime -= nTimeOffset;
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : *GetDigitalStr
 * PURPOSE  : change BOOL value to YDN digital data format
 * CALLS    : Power
 * CALLED BY: GetSigValues
 * ARGUMENTS: SIG_ENUM  *peValue : the number of the array is 4.
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 10:30
 *==========================================================================*/
static unsigned char GetDigitalStr(SIG_ENUM *peValue)
{
	unsigned char cValue;
	int i;

	cValue = 0;

	/* get the nibble value */
	for (i = 0; i < 4; i++)
	{
		if (peValue[3-i] == 1)    //reverse order
		{
			cValue |= Power(2, i);
		}
	}

	/* get the hex character */
	if (cValue <= 9)
	{
		cValue += '0';
	}
	else  
	{
		cValue += ('A' - 10);
	}

	return cValue;
}
/*==========================================================================*
 * FUNCTION : GetSigValues
 * PURPOSE  : get sig values by batch and convert to YDN value format
 * CALLS    : DxiGetData
 *			  GetAnalogueStr
 *			  GetDigitalStr
 * CALLED BY: GetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : TRUE for Analogue sig, 
 *											   FALSE for Digital sig
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            YDN_MAPENTRIES_INFO  *pEntriesInfo : 
 *            unsigned char    *szOutData    : length is YDN_SIGTYPE_SIZE, 
 *										cleared to zero already
 * RETURN   : BOOL : FALSE for call DXI interface to get sig values failed 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 15:26
 *==========================================================================*/
BOOL GetSCUPSigValues(BOOL bAnalogue,
						 YDN_MAPENTRIES_INFO *pEntriesInfo,
						 int iEquipID,
						 unsigned char *szOutData,
						 BOOL ucInvalidFlag,
						 unsigned char ucErrorRTN,
						 float fErrorRTN)
{
	BOOL	bRTN;
	char	ctemp[9],num,i;

     bRTN = GetSCUPSingleSigValues(bAnalogue, pEntriesInfo->iSCUPSigType,
						 pEntriesInfo->iSCUPSigID,
						 iEquipID,
						 szOutData);
     if(bRTN == FALSE)
     {
	if(ucInvalidFlag == TRUE)
	{
		if(bAnalogue)
		{
			num = 8;	
		}
		else
		{
			num = 2;
		}
		for(i=0;i<num;i++)
		{
			*(szOutData+i) = 0x20;
		}
	}
	else
	{
		if(bAnalogue)
		{
			memcpy(szOutData,GetAnalogueStr(TRUE, fErrorRTN, ctemp),8);
		}
		else
		{
			HexToTwoAsciiData(ucErrorRTN, szOutData);
		}
	}

     }
     return bRTN;
    
}

/*==========================================================================*
 * FUNCTION : GetAnalogueData
 * PURPOSE  : get float value from YDN analogue data format
 * CALLS    : 
 * CALLED BY: SetSigValues
 * ARGUMENTS: IN const unsigned char *sValue : char array containing 8 elements
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-20 19:34
 *==========================================================================*/
static float GetAnalogueData(IN const unsigned char *sValue)
{
    ASSERT(sValue);
    
	union YDN_FVALUE YDN_uf;

	int i, j;
	float fResult;

	/* 1.format convert */
	#ifdef  _CPU_BIG_ENDIAN  //for ppc
	   for (i = 3, j = 0; i >= 0 ; i--, j++)
	   {
		   YDN_uf.cValue[i] = YDN_AsciiHexToChar(sValue + 2 * j);
	   }
	#else
	   for (i = 0; i < 4; i++)
	   {
		   YDN_uf.cValue[i] = YDN_AsciiHexToChar(sValue + 2 * i);
	   }
	#endif 
	   
	/* 2.endian process */
	/*
#ifdef  _CPU_BIG_ENDIAN  //for ppc
		fResult = YDN_uf.fValue;
#else  //for x86
	{
		union IEEE_FVALUE tmpFValue;
		for (i = 0; i < 4; i++)
		{
			tmpFValue.cValue[i] = ieee_uf.cValue[3-i];
		}
		fResult = tmpFValue.fValue;
	}
#endif 
*/

	fResult = YDN_uf.fValue;
    //TRACE("\nfResult is: %f\n", fResult);
	return fResult;
}


/*==========================================================================*
 * FUNCTION : GetDigitalData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SetSigValues
 * ARGUMENTS: IN unsigned char  chr      : 
 *            OUT SIG_ENUM      *peValue : 4 elements array
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-20 19:49
 *==========================================================================*/
static void GetDigitalData(IN unsigned char *chr, OUT SIG_ENUM *peValue)
{
	//int i;
	//unsigned char c;
	int iData;

	//*peValue = (int)YDN_AsciiHexToChar(&chr);

    iData = (int)YDN_AsciiHexToChar(chr);
	peValue[0] = iData;
	//c = chr;
/*
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}

	//convert
	for (i = 0; i < 4; i++)
	{
		peValue[i] = (c & Power(2, 3-i)) == 0 ? 0 : 1;
	}
*/
	return;
}

/*==========================================================================*
 * FUNCTION : GetSubStringCount
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char*  szStr : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 16:03
 *==========================================================================*/
static int GetSubStringCount(IN const unsigned char* szStr)
{
	int iCount;
	const unsigned char *p;

	iCount = 0;
	p = szStr;
	while (*p != '\0')
	{
		if (*p == '!')
		{
			iCount++;
		}

		p++;
	}

	return iCount + 1;
}


/*==========================================================================*
 * FUNCTION : *GetSubString
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int                   iIndex : start from 1
 *            IN const unsigned char*  szStr  : 
 *			  OUT int *piSubStrLen            :
 * RETURN   : const unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 16:06
 *==========================================================================*/
static const unsigned char *GetSubString(IN int iIndex, 
										 IN const unsigned char* szStr,
										 OUT int *piSubStrLen)
{
	int iCount;
	const unsigned char *p, *pSubStr;

	iCount = 0;
	p = szStr;

	/* special case */
	if (iIndex == 1)
	{
		pSubStr = p;
	}
	else
	{
		/* 1.move to the start pos */
		while (*p != '\0')
		{
			if (*p == '!')
			{
				iCount++;
			}

			if (iCount == iIndex - 1)
			{
				break;
			}

			p++;
		}

		/* 2.pick the sub-string */
		pSubStr = ++p;
	}

	*piSubStrLen = 0;
	while (*p != '!' && *p != '*' && *p != '\0')
	{
		(*piSubStrLen)++;
		p++;
	}

	if (*piSubStrLen == 0)
	{
		pSubStr = NULL;
	}

	return pSubStr;
}

__INLINE static BOOL SetAnalogToVarValue(int iEquipID, long lSigID,
										 VAR_VALUE_EX *pVarValue, double fData)
{
	int nBufLen;
	BOOL bRet;
	SIG_BASIC_VALUE  *pBv = NULL;

	bRet = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,	
		lSigID,
		&nBufLen,			
		&pBv,			
		0);
	//TRACE("\npBv->ucType is: %d\n", pBv->ucType);
	if (bRet == ERR_DXI_OK && pBv)
	{
		switch (pBv->ucType)
		{
		case VAR_FLOAT:
			pVarValue->varValue.fValue = fData;
			break;

		case VAR_LONG:
			pVarValue->varValue.lValue = fData;
			break;

		case VAR_UNSIGNED_LONG:
			pVarValue->varValue.ulValue = fData;
			break;

		case VAR_DATE_TIME:
			pVarValue->varValue.dtValue = fData;
			break;

		case VAR_ENUM:
			pVarValue->varValue.enumValue = fData;
			break;
		}

		return TRUE;

	}

	return FALSE;
}


__INLINE static BOOL SetDigitalToVarValue(int iEquipID, long lSigID,
								 VAR_VALUE_EX *pVarValue, SIG_ENUM eValue)
{
	int nBufLen;
	BOOL bRet = FALSE;
	SIG_BASIC_VALUE  *pBv;

	bRet = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,	
		lSigID,
		&nBufLen,			
		&pBv,			
		0);

	if ( bRet == ERR_DXI_OK && pBv)
	{
		pVarValue->varValue.enumValue = eValue;
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : YDN_BuildDataInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char *sValue : char array containing 8 elements
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-14 19:34
 *==========================================================================*/

//static unsigned char *YDN_BuildDataInfo(int iDataLen, IN OUT unsigned char *szOutData)
//{
//    WORD wLength, Sum;
//    unsigned char *p;
//
//    p = szOutData;
//    wLength = iDataLen * 2;
//	Sum = (-(wLength + (wLength>>4) + (wLength>>8) )) << 12;
//	wLength = wLength & 0xFFF | Sum;
//	//TRACE("\nwLength is %ld", wLength);
//	HexToFourAsciiData(wLength, p);
//	//sprintf(p, "%04X%s", wLength, p + 4);
//	memcpy(p + 4,szOutData + 4, iDataLen * 2);
//
//	*(p + 4 + iDataLen * 2) = '\0';
//	
//	return p;
//   
//}
/*==========================================================================*
 * FUNCTION : SetSigValues
 * PURPOSE  : convert from YDN value format and set sig values by batch
 * CALLS    : GetAnalogueData
 *			  GetDigitalData
 * CALLED BY: SetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : 
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            YDN_MAPENTRIES_INFO  *pEntriesInfo : 
 *            const unsigned char    *szInData  : 
 * RETURN   : BOOL : 
 * COMMENTS : ensure the length of szInData is legal before call it!
 * CREATOR  : HanTao                   DATE: 2006-06-10 19:01
 *==========================================================================*/
static BOOL SetYDNSingleSigValues(BOOL bAnalogue,
                         int iSCUPSigType,
						 int iSCUPSigID,
						 int iEquipID,
						 int iSigNum, 
						 unsigned char *sInData)
{
	UNUSED(iSigNum);
	int i;
	BOOL bRet = TRUE;
	SIG_ENUM digitals[4];

	/* need add in DXI module */
	int nError, nBufLen;
	int nInterfaceType, nVarID, nVarSubID;
	SET_MULTIPAL_SIGNALS stSetMulSignals;// = NEW(SET_MULTIPAL_SIGNALS, 1);
  
	memset(&stSetMulSignals, 0, sizeof(SET_MULTIPAL_SIGNALS));
	nBufLen = sizeof(SET_MULTIPAL_SIGNALS);

    //TRACE("\npCMDType is: %s\n", sInData);
    //TRACE("\tiEquipID is %d\n", iEquipID);
  
#ifdef _DEBUG_YDN_CMD_HANDLER
	int j, k,iCurPos = 0; 
	TRACE_YDN_TIPS("Received a batch of Sig Values, will call DXI to set them");
	if (bAnalogue)
	{
		TRACE("Sigs value(Analogue) info -- before format converting:\n");
		for (i = 0, k = 0; i < iSigNum; i++)
		{
			TRACE("\tSig Value #%-4d", i + 1);
			for (j = 0; j < 8; j++)
			{
				TRACE("%c", sInData[k++]);
			}
			TRACE("\n");
		}
	}
	else
	{   TRACE("Sigs value(Digital) info -- before format converting:\n");
		TRACE("\t");
		for (i = 0 ; i < (iSigNum + 3)/4; i++)
		{
			TRACE("%c", sInData[i]);
		}
		TRACE("\n");
	}
#endif //_DEBUG_YDN_CMD_HANDLER


	/* note: check the length of sInData out */
	/* 1.format process */
	
	if (bAnalogue) //for Analogue sig
	{
		if (iSCUPSigType != -1) //not reserved pos
		{
			stSetMulSignals.lIDs[0] = DXI_MERGE_SIG_ID(
				iSCUPSigType, iSCUPSigID);
			if (!SetAnalogToVarValue(iEquipID, 
				stSetMulSignals.lIDs[0],
				&(stSetMulSignals.vData[0]),
				GetAnalogueData(sInData)))
			{
				//j--;
			}
		}
	}  //end of Analogue sig process

	else  //for Digital sig
	{
		//TRACE("\ndigitals = %d\n", digitals[0]);
		GetDigitalData(sInData, digitals);
		//TRACE("\ndigitals = %d\n", digitals[0]);
		//iCurPos += 1;

		if (iSCUPSigType != -1) //not reserved pos
		{
			stSetMulSignals.lIDs[0] = DXI_MERGE_SIG_ID(
				iSCUPSigType, iSCUPSigID);

			if (!SetDigitalToVarValue(iEquipID, 
				stSetMulSignals.lIDs[0],
				&(stSetMulSignals.vData[0]),
				digitals[0]))
			{
				//j--;
			}
			//stSetMulSignals.vData[j++].varValue.enumValue = digitals[k];	
		}
	}  //end of Digital sig process

	//}
	stSetMulSignals.nSetNum = 1;
    //TRACE("\nstSetMulSignals.vData[0] is: %f\n", stSetMulSignals.vData[0].varValue.enumValue);
	/* add extra info */
	for (i = 0; i < 1; i++)
	{
		stSetMulSignals.vData[i].nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
		if(iSCUPSigType == SIG_TYPE_SAMPLING)
			stSetMulSignals.vData[i].nSenderType = SERVICE_OF_LOGIC_CONTROL;
		else
		    stSetMulSignals.vData[i].nSenderType = SERVICE_OF_DATA_COMM;
		stSetMulSignals.vData[i].pszSenderName = "YDN23 Protocol";
	}

	/* 2.set data by DXI interface */
#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will call DXI to set the batch of sig values");
	TRACE("Sigs ID info:\n");
	TRACE("\tTotal num: %d\n", stSetMulSignals.nSetNum);
	for (i = 0; i < stSetMulSignals.nSetNum; i++)
	{
		TRACE("\tEquip ID: %d", iEquipID);
		TRACE("\t\tMerged ID: %08X", stSetMulSignals.lIDs[i]);
		if (bAnalogue)
		{
			TRACE("\t\tNew values: %f\n", 
				stSetMulSignals.vData[i].varValue.fValue);
		}
		else // for Digital
		{
			TRACE("\t\tNew values: %ld\n", 
				stSetMulSignals.vData[i].varValue.enumValue);
		}
	}
#endif //_DEBUG_YDN_CMD_HANDLER

	nInterfaceType = VAR_A_SIGNAL_VALUE;
	nVarID = iEquipID;  //  equip id
	nBufLen = sizeof(VAR_VALUE_EX);

	for (i = 0; i < stSetMulSignals.nSetNum; i++)
	{
		nVarSubID = stSetMulSignals.lIDs[i];

		//TRACE("stSetMulSignals.vData[i].");
		//TRACE("stSetMulSignals.vData[i].varValue.ulValue is %d\n", stSetMulSignals.vData[i].varValue.ulValue);

		nError = DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stSetMulSignals.vData[i],			
			0);

	    if (nError != ERR_DXI_OK)
		{
            //#ifdef _DEBUG_YDN_CMD_HANDLER
			    TRACEX("Set #%d sig failed. Error code from DXI: %08X iEquipID =%d\n", i+1, nError,iEquipID);
				TRACE("Set #%d sig failed. Error code from DXI: %08X\n", i+1, nError);
			    /*TRACEX("Detail Info:\n nInterfaceType: %d\tnVarID: %d\t"
				"nVarSubID: %08X\tnBufLen: %d\tNew Data: %f\n",
				nInterfaceType, nVarID, nVarSubID, nBufLen, 
				stSetMulSignals.vData[i].varValue.fValue);*/
            //#endif //_DEBUG_YDN_CMD_HANDLER

			bRet = FALSE;
		}
	}
	
	return bRet;
}

/*==========================================================================*
 * FUNCTION : SetSigValues
 * PURPOSE  : convert from YDN value format and set sig values by batch
 * CALLS    : GetAnalogueData
 *			  GetDigitalData
 * CALLED BY: SetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : 
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            YDN_MAPENTRIES_INFO  *pEntriesInfo : 
 *            const unsigned char    *szInData  : 
 * RETURN   : BOOL : 
 * COMMENTS : ensure the length of szInData is legal before call it!
 * CREATOR  : HanTao                   DATE: 2006-06-10 19:01
 *==========================================================================*/
BOOL SetYDNSigValues(BOOL bAnalogue,
						 int iEquipID,
						 int iSigNum, 
						 YDN_MAPENTRIES_INFO *pEntriesInfo,
						 unsigned char *sInData)
{

    BOOL bRet = TRUE;


    bRet = SetYDNSingleSigValues(bAnalogue,
                         pEntriesInfo->iSCUPSigType,
						 pEntriesInfo->iSCUPSigID,
						 iEquipID,
						 iSigNum, 
						 sInData);
   return bRet;
}


/*==========================================================================*
 * FUNCTION : IsAlarmBlocked
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-18 16:35
 *==========================================================================*/
static BOOL IsAlarmBlocked(void)
{
	int nEquipID, nBufLen;
	int nError;
	SIG_BASIC_VALUE* pSigValue;
	BOOL bBlock = FALSE;

	/* get Outgoing alarm blocked sig value: (1, 2, 8) */
	nEquipID = DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);
	nError = DxiGetData(VAR_A_SIGNAL_VALUE,
		nEquipID,			
		SIG_ID_ALARM_OUTGOING_BLOCKED,		
		&nBufLen,			
		&pSigValue,			
		0);

	if (nError != ERR_DXI_OK)
	{
		LOG_YDN_CMD_E("Call DXI interface to get Outgoing alarm blocked "
			"signal value failed");
	}
	else
	{
		/* 0: false   1: true */
		if (pSigValue->varValue.enumValue == 1)
		{
			bBlock = TRUE;
		}
	}

	return bBlock;
}

static int CompareBlockIDs(char *szID1, char *szID2)
{
	return strcmp(szID1, szID2);
}

/* Added by Thomas, support product info, 2006-2-10 */
static int CompareDeviceIDs(int *pID1, int *pID2)
{
	return (*pID1 > *pID2);
}

/*==========================================================================*
 * FUNCTION : ExchangeYDNSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int                   iIndex : start from 1
 *            IN const unsigned char*  szStr  : 
 *			  OUT int *piSubStrLen            :
 * RETURN   : const unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-01 16:06
 *==========================================================================*/
unsigned char *ExchangeYDNSigValue(IN unsigned char *szchar,
								   IN YDN_MAPENTRIES_INFO *pMapEntry)
{
    ASSERT(pMapEntry);
    ASSERT(szchar);
    
    int iIndex = 0;
    YDN_MAPENTRIES_INFO *pCurEntry;
    unsigned char *p;

    pCurEntry = pMapEntry;
    p = szchar;
    if(pCurEntry->iLen == -1)
	{
    	return p;
	}
   
	iIndex = (int)YDN_AsciiHexToChar(p);
	
	if(iIndex < 0 || iIndex > (pCurEntry->iLen - 1))
	{
	   iIndex = 0;
	}

    if(pCurEntry->iLen > 0)
	{
	   //sprintf(p, "%02X", pCurEntry->iStatusValue[iIndex]);
	   HexToTwoAsciiData(pCurEntry->iStatusValue[iIndex], p);
	}

    return p;
}

/*==========================================================================*
 * FUNCTION : GetSCUPSingleSigValues
 * PURPOSE  : get some signal values by batch and convert to YDN value format
 * CALLS    : DxiGetData
 *			  GetAnalogueStr
 *			  GetDigitalStr
 * CALLED BY: GetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : TRUE for Analogue sig, 
 *											   FALSE for Digital sig
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            YDN_MAPENTRIES_INFO  *pEntriesInfo : 
 *            unsigned char    *szOutData    : length is YDN_SIGTYPE_SIZE, 
 *										cleared to zero already
 * RETURN   : BOOL : FALSE for call DXI interface to get sig values failed 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-31 15:26
 *==========================================================================*/
BOOL GetSCUPSingleSigValues(BOOL bAnalogue,
						 int iSCUPSigType,
						 int iSCUPSigID,
						 int iEquipID,
						 unsigned char *szOutData)
{
    //ASSERT(szOutData);
	//float	fBuf;

	char	      szStrValue[9];      //for IEEE format analogue string value

	//unsigned char			digital;

	/* to call DXI interface to get sig values */
	int		nError, nBufLen;
	int		nInterfaceType, nVarID, nVarSubID;
	SIG_BASIC_VALUE			*pBv;

	GET_MULTIPAL_SIGNALS	stGetMulSignals;


	/* 1.get data by DXI interface */
	nError = 0;
	nInterfaceType = VAR_MULTIPLE_SIGNALS_INFO;
	nVarID = MUL_SIGNALS_GET_VALUE;
	nVarSubID = 0;

	memset(&stGetMulSignals, 0, sizeof(GET_MULTIPAL_SIGNALS));
	nBufLen = sizeof(GET_MULTIPAL_SIGNALS);

	
    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("Will call DXI to get a batch of sig values");
	   TRACE("Sigs ID info:\n");
   #endif //_DEBUG_YDN_CMD_HANDLER
	

	/* prepare GET_MULTIPAL_SIGNALS structure */
	/* note: buffer length of stGetMulSignals is MAX_GET_SIGNALS_NUM(256)! */

    #ifdef _DEBUG_YDN_CMD_HANDLER
			TRACE("\tSCU+ Equip ID: %d", iEquipID);
			TRACE("\t\tSCU+ Sig Type: %d", iSCUPSigType);
			TRACE("\t\tSCU+ Sig ID: %d\n", iSCUPSigID);
    #endif //_DEBUG_YDN_CMD_HANDLER

	stGetMulSignals.lData[0] = DXI_MERGE_UNIQUE_SIG_ID(iEquipID, 
				iSCUPSigType, iSCUPSigID);

	stGetMulSignals.nGetNum = 1;
	/* call DXI interface */
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&stGetMulSignals,			
		0);
	if ((nError != ERR_DXI_OK)||(stGetMulSignals.lData[0] ==NULL))
	{
		szOutData[0] = '\0';
		return FALSE;
	}

	
    #ifdef _DEBUG_YDN_CMD_HANDLER
	    int		i;
	    TRACE("\tTotal Sig Num: %d\n", stGetMulSignals.nGetNum);
	    TRACE_YDN_TIPS("Get Sig Values from DXI successfully");
	    TRACE("Sig Values:\n");
	    TRACE("\tSig Num: %d\n", stGetMulSignals.nGetNum);
	    for (i = 0; i < stGetMulSignals.nGetNum; i++)
	    {
		    pBv = (SIG_BASIC_VALUE*)(stGetMulSignals.lData[i]);
		    if (bAnalogue)
		    {
			   TRACE("\tSig Value: #%-4d%.4f\n", i+1, GetAnalogFromSBV(pBv));
		    }
		    else
		    {
			   TRACE("\tSig Value: #%-4d%ld\n", i+1, GetDigitalFromSBV(pBv));
		    }
	    }
    #endif //_DEBUG_YDN_CMD_HANDLER


	/* 2.format process */
    pBv = (SIG_BASIC_VALUE*)(stGetMulSignals.lData[0]);

	//TRACE("pBv->varValue.enumValue is %d",pBv->varValue.enumValue);

	if (bAnalogue) //for Analogue sig
	{
		//fBuf = GetAnalogFromSBV(pBv);
		//sprintf(szOutData, "%s", 
		//GetAnalogueStr(TRUE, fBuf, szStrValue));
		//memcpy(szOutData,GetAnalogueStr(TRUE, GetAnalogFromSBV(pBv), szStrValue),8);
		float fValue = GetAnalogFromSBV(pBv);
		if ( (SIG_TYPE_ALARM == iSCUPSigType )
			&& (!ALARM_IS_ACTIVING(pBv)) )
		{
			fValue = 0;
		}
		memcpy(szOutData,GetAnalogueStr(TRUE, fValue, szStrValue),8);
	}  //end of Analogue Sig
	else  //for Digital sig
	{
		//digital  = (unsigned char)GetDigitalFromSBV(pBv);
		HexToTwoAsciiData((unsigned char)GetDigitalFromSBV(pBv), szOutData);
		if(iSCUPSigType == 3)//added at 2007.3.8
	    {
			//TRACE("Enter active alarm judge1!!\n");
		    if(!ALARM_IS_ACTIVING(stGetMulSignals.lData[0]))
			{
				HexToTwoAsciiData(0x00, szOutData);
				//TRACE("Enter active alarm judge2!!\n");
				//TRACE("iEquipID is %d\n", iEquipID);
				//TRACE("iSCUPSigID is %d\n", iSCUPSigID);
			}
	    }
		//sprintf(szOutData, "%02X", GetDigitalStr(digital));
	}  //end of Digital sig
    //end of loop
    
    #ifdef _DEBUG_YDN_CMD_HANDLER
        int j, k;
	    TRACE_YDN_TIPS("Convert the format of Sig values");
	    if (bAnalogue)
	    {
		   TRACE("After format converting(Analogue):\n");
		   for (i = 0, k = 0; szOutData[i] != '\0'; k++)
		   {
			   TRACE("\tSig Value #%-4d", k + 1);
			   for (j = 0; j < 8; i++, j++)
			   {
				   TRACE("%c", szOutData[i]);
			   }
			   TRACE("\n");
		   }
	    }
	    else
	    {   
	       TRACE("After format converting(Digital):\n\t");
		   for (i = 0, j = 0; szOutData[i] != '\0'; i++, j++)
		   {
			   TRACE("%c", szOutData[i]);
			   if (j == 4)
			   {
				 TRACE("\t");
			   }
		   }
		   TRACE("\nTotal characters: %d\n", i);
	    }
    #endif //_DEBUG_YDN_CMD_HANDLER

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ExecuteRACA
 * PURPOSE  : Read AC Analog signals
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : length of szOutData is YDN_RESP_SIZE
 * CREATOR  : HanTao                   DATE: 2006-05-13 19:00
 *==========================================================================*/
static int ExecuteRACA(YDN_BASIC_ARGS *pThis,
				   unsigned char *szInData,
				   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRACA!!!!\n");
        TRACE("\nExecuteRACA:szInData is: %s\n", szInData);
	#endif
	
	int i, j, k, iDataLen;
	int iCID1,iCID2, iData[4] = {0};
	int iCurrMeasMeth = 0;
	unsigned char *pData, pCurrMeasMeth[2];
	unsigned char DataFlag[2] = {'1', '0'};
	BOOL bRTN = FALSE;
	//unsigned char *pCurrMeasMeth = 0;
	char logText[YDN_LOG_TEXT_LEN];
	//WORD wLength, Sum;
	BOOL bDxiRTN;
	int  iEquipIDOrder, iEquipID, iCommGrp;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;

    HANDLE hLocalThread;
	/* not used the param */
	UNUSED(pThis);

    hLocalThread = RunThread_GetId(NULL);
    
	/* 1.check validity */
	if (strncmp(szInData, "4041", 4) != 0)
	{
		//sprintf(szOutData, "%x", CMDERR);
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	iCommGrp = (int)YDN_AsciiHexToChar(szInData + 4);

	if(iCommGrp == ALL_AC_OR_DC)
	{
		iDataLen = strlen(g_RespData.szRespData1[0]);

#ifdef	_DEBUG_YDN
		if((iDataLen) > MAX_YDNFRAME_LEN)
		{
			AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRACA - iDataLen greater than 4114.\n");
		
		}
#endif

		if(iDataLen == 0)
			return CMDERR;
		memcpy(szOutData, g_RespData.szRespData1[0],iDataLen);


		pTypeMap = GetTypeMap(iCID1, iCID2);

		if (pTypeMap == NULL)
		{
			sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
				iCID1, iCID2);
			LOG_YDN_CMD_I(logText);

			//strcpy(szOutData, CMDERR);
			return CMDERR;
		}

		pCurEntry = pTypeMap->pMapEntriesInfo;
		iEquipIDOrder = 0;
		
		if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder))
		{
			iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
			bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, DataFlag);
			//TRACE("bRTN is %d\n", bRTN);
			if(bRTN)
			{
#ifdef _DEBUG_YDN_CMD_HANDLER
				TRACE_YDN_TIPS("clear dataflag has been excuted ok!");
				TRACE("Reply Data:\n");
				TRACE("\t%s\n", DataFlag);
#endif //_DEBUG_YDN_CMD_HANDLER
			}
		}

		return NOR;
	}

	if((iCommGrp > ACD_MAX_NUM || iCommGrp < ACD_MIN_NUM) && iCommGrp != ALL_AC_OR_DC)
		return CMDERR;
	
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		//strcpy(szOutData, CMDERR);
		return CMDERR;
	}
    
    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    // 2.Package the DataFlag
     if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
     {
	    return CMDERR;
     }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,1,0);
	bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, DataFlag);
	//TRACE("bRTN is %d\n", bRTN);
	if(bRTN)
	{
#ifdef _DEBUG_YDN_CMD_HANDLER
		TRACE_YDN_TIPS("clear dataflag has been excuted ok!");
		TRACE("Reply Data:\n");
		TRACE("\t%s\n", DataFlag);
#endif //_DEBUG_YDN_CMD_HANDLER
	}

	//HexToTwoAsciiData(0x11, szOutData);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    //iEquipTypeID = pCurEntry->iEquipType;
     if(!YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)) 
     {
	 return CMDERR;
     }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	bDxiRTN = GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
	if(bDxiRTN == FALSE)
	{
		 return CMDERR;
	}
	iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
	if(iData[0] == 0)
		return CMDERR;
	if(iCommGrp == ALL_AC_OR_DC)
	{
        //GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData);
        //iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
        iDataLen++;
        szOutData += 2;
	}
	pCurEntry++;//point to ACInputNum

	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder )== FALSE)
	{
		return CMDERR;
	}
	pFixedPos = pCurEntry;
    
    if(iCommGrp != ALL_AC_OR_DC)
    {
        if(iCommGrp < 0)
        	return CMDERR;
        else if(iCommGrp == 0)
        {
            if(iData[0] > 1)
            	return CMDERR;
        }
        else
        {
            if(iCommGrp > iData[0])
            	return CMDERR;
            iEquipIDOrder = iEquipIDOrder + iCommGrp - 1;//pack some ACD data;
        }
        iData[0] = 1;
    }
    
    for(i = 0; i < iData[0]; i++, iEquipIDOrder++)
    {
	   RunThread_Heartbeat(hLocalThread);
    
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];//find out the equipID orderly

	   bDxiRTN = GetSCUPSingleSigValues(FALSE, 2, 9, iEquipID, pCurrMeasMeth);
	   if(bDxiRTN == FALSE)							//for equipment is not exist by ZTZ
	   {
		iCurrMeasMeth = 0;
	   }
	   else
	   {
		iCurrMeasMeth = (int)YDN_AsciiHexToChar(pCurrMeasMeth);
	   }
	   
	   TRACE("\niCurrMeasMeth = %d\n", iCurrMeasMeth);
	  
        GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
       //pCurEntry++;
       iDataLen++;
       iData[1] = (int)YDN_AsciiHexToChar(szOutData);//ACInputNum 
       szOutData += 2;
	   iData[2] = pCurEntry->iFlag1;//fixed Num

       for(j=0; j< iData[1]; j++)
       {
          for(k = 0; k < iData[2]; k++)
          {
	      pCurEntry++;
              GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
              szOutData += 8;
              iDataLen += 4;
          }
		  pCurEntry++;
		 
          iData[3] = pCurEntry->iFlag1;//Defined Num
        
	  HexToTwoAsciiData(iData[3], szOutData);

          iDataLen++;
          szOutData += 2;
          for(k = 0; k < iData[3]; k++)
          {
			 pCurEntry++;
             GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
             szOutData += 8;
             iDataLen += 4;
          }
       }
	   pCurEntry = pCurEntry + (AC_MAX_INPUT_ROUTE - iData[1])*(iData[2] + iData[3] + 1) + 1;

	   //TRACE("pCurEntry->iYDNPort is: %d\n",pCurEntry->iYDNPort);

       if(iCurrMeasMeth == 0)
       {
           for(k = 0; k < 24; k++, szOutData++)
		   {
           	  *szOutData = 0x20;
		   }
		   iDataLen += 12;
       }
       else if(iCurrMeasMeth == 1)
       {
           //TRACE("\nOK12 + %d!!!!\n", i);
	
		   for(k = 0; k < 8; k++, szOutData++)
		   {
           	  *szOutData = 0x20;
		   }
		   iDataLen += 4;
		   pCurEntry++;

		   GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           szOutData += 8;
           iDataLen += 4;
		   //pCurEntry++;
           
		   for(k = 0; k < 8; k++, szOutData++)
		   {
           	  *szOutData = 0x20;
		   }
		   iDataLen += 4;
       }
       else
       {
           for(k = 0; k < 3; k++, pCurEntry++)
           {
              TRACE("\nOK13 + %d!!!!\n", i);
              GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
              szOutData += 8;
              iDataLen += 4;
           }
       }

	   pCurEntry = pFixedPos;
    }

	//TRACE("\npData2:ExecuteRACA is: %s\n",pData);
    // 4.calculate LCHKSUM and package it 
	szOutData = pData;
    //szOutData = YDN_BuildDataInfo(iDataLen, szOutData);
	YDN_BuildDataInfo(iDataLen, szOutData);
	
    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RACA has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER
    
	return NOR;
}


/*==========================================================================*
 * FUNCTION : GetBlock
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ExecuteRB
 * ARGUMENTS: int  iGroupID : 
 *            int  iUnitID  : 
 * RETURN   : YDN_BLOCK_INFO* : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 11:29
 *==========================================================================*/
static YDN_BLOCK_INFO* GetBlock(int iCID1, int iCID2)
{
	int i;
	YDN_MODEL_INFO *pModel;
	YDN_BLOCK_INFO *pBlock = NULL;

	//int iRectifierNum, nBufLen;
	//SIG_BASIC_VALUE  *pSigValue;

	pModel = &g_YDNGlobals.YDNModelInfo;
	pBlock = pModel->pYDNBlockInfo;


	/* to get the actual rectifier number */
	//if (DxiGetData(VAR_A_SIGNAL_VALUE,
	//	DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID),	
	//	SIG_ID_ACTUAL_RECT_NUM,
	//	&nBufLen,			
	//	&pSigValue,			
	//	0) == ERR_DXI_OK)
	//{
	//	iRectifierNum = pSigValue->varValue.ulValue;
	//}
	//else
	//{
	//	iRectifierNum = 1000;  //has no limit
	//}
	


	for (i = 0; i < pModel->iBlocksNum; i++, pBlock++)
	{
		if (pBlock->iCID1 == iCID1 && pBlock->iCID2 == iCID2)
		{
			break;
		}
	}

	/* not found */
	if (i == pModel->iBlocksNum)
	{
		pBlock = NULL;
	}
	//else
	//{
	//	/* check the actual rectifer number */
	//	if (pBlock->iGroupID == 0x02 && pBlock->iUnitID > iRectifierNum)
	//	{
	//		pBlock = NULL;
	//	}
	//}

	return pBlock;
}

/*==========================================================================*
 * FUNCTION : ExecuteRACS
 * PURPOSE  : Read AC Status Signals
 * CALLS    : 
 * CALLED BY: YDN_DecodeAndPerform
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : length of szOutData is YDN_RESP_SIZE
 * CREATOR  : HanTao                   DATE: 2006-05-23 19:15
 *==========================================================================*/
static int ExecuteRACS(YDN_BASIC_ARGS *pThis,
				   unsigned char *szInData,
				   unsigned char *szOutData)
{

    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRACS!!!!\n");
        TRACE("\nExecuteRACS:szInData is: %s\n", szInData);
	#endif
    
	int i, j, iDataLen;
	int iCID1,iCID2, iData[3] = {0};
	unsigned char *pData;
	//unsigned char DataFlag[2] = {'0', '0'};
	char logText[YDN_LOG_TEXT_LEN];
    //BOOL bRTN;
    
	int  iEquipIDOrder, iEquipID, iCommGrp;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;
    HANDLE hLocalThread;
	/* not used the param */
	UNUSED(pThis);

    hLocalThread = RunThread_GetId(NULL);

	/* 1.check validity */
	if (strncmp(szInData, "4043", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	iCommGrp = (int)YDN_AsciiHexToChar(szInData + 4);

	if(iCommGrp == ALL_AC_OR_DC)
	{
		iDataLen = strlen(g_RespData.szRespData1[1]);

#ifdef	_DEBUG_YDN
		if((iDataLen) > MAX_YDNFRAME_LEN)
		{
			AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRACS - iDataLen greater than 4114.\n");
		
		}
#endif

	    if(iDataLen == 0)
		   return CMDERR;
	    memcpy(szOutData, g_RespData.szRespData1[1],iDataLen);
		return NOR;
	}

	if((iCommGrp > ACD_MAX_NUM || iCommGrp < ACD_MIN_NUM) && iCommGrp != ALL_AC_OR_DC)
		return CMDERR;
	
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
     for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
     
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    // 2.Package the DataFlag
     if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
     {
	return CMDERR;
     }
    iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
    if(iData[0] == 0)
		return CMDERR;
	if(iCommGrp == ALL_AC_OR_DC)
	{
       //GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData);
       //iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
       iDataLen++;
       szOutData += 2;
	}
	pCurEntry++;
   
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CMDERR;
	}
	pFixedPos = pCurEntry;

    if(iCommGrp != ALL_AC_OR_DC)
    {
        if(iCommGrp < 0)
        	return CMDERR;
        else if(iCommGrp == 0)
        {
            if(iData[0] > 1)
            	return CMDERR;
        }
        else
        {
            if(iCommGrp > iData[0])
            	return CMDERR;
            iEquipIDOrder = iEquipIDOrder + iCommGrp - 1;//pack some ACD data;
        }
        iData[0] = 1;
    }
    
    for(i = 0; i < iData[0]; i++, iEquipIDOrder++)
    {
       RunThread_Heartbeat(hLocalThread);
       
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];//find out the equipID orderly

       GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
       pCurEntry++;
       iDataLen++;
       iData[1] = (int)YDN_AsciiHexToChar(szOutData);//OutputBreakerNum
       szOutData += 2;
       for(j = 0; j < iData[1]; j++, pCurEntry++)
       {
           GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           szOutData = ExchangeYDNSigValue(szOutData, pCurEntry);
           szOutData += 2;
           iDataLen++;
	   }
	   pCurEntry = pCurEntry + (AC_MAX_OUTPUT_BREAKER - iData[1]);//full config minus real config

       iData[2] = pCurEntry->iFlag1;//Defined Num
       //sprintf(szOutData, "%02X", iData[2]);
       HexToTwoAsciiData(iData[2], szOutData);
       pCurEntry++;
       iDataLen++;
	   szOutData += 2;
       for(j = 0; j < iData[2]; j++, pCurEntry++)
       {
           GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           szOutData = ExchangeYDNSigValue(szOutData, pCurEntry);
           szOutData += 2;
           iDataLen++;
       }

	   pCurEntry = pFixedPos;
    }
    // 4.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);
    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RACS has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : CheckWBParam
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int iAONum                      :
 *			  int iDONum                      :
 *			  const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-11-21 15:52
 *==========================================================================*/
static BOOL CheckWBParam(int iAONum,
						 int iDONum,
						 const unsigned char *szInData,
						 unsigned char *szOutData)
{
	const unsigned char *pAOStr, *pDOStr;
	int iStrLen;

	if (GetSubStringCount(szInData) != 3)
	{
		TRACE_YDN_TIPS("format of WB* command is incorrect");

		strcpy(szOutData, "ERR-CMD*");
		return FALSE;
	}

	/* check AO info */
	pAOStr = GetSubString(2, szInData, &iStrLen);
	if ((iStrLen != 0) && (pAOStr[0] != '$')) //leave out all AOs is acceptable
	{
		if (iStrLen != iAONum * 8)  
		{
			TRACE_YDN_TIPS("AOs length is incorrect in WB* command");

			strcpy(szOutData, "ERR1-WB*");
			return FALSE;
		}

		if (!YDN_IsStrAsciiHex(pAOStr, iStrLen))
		{
			TRACE_YDN_TIPS("AOs format is incorrect in WB* command"
				"(should be Hex characters)");

			strcpy(szOutData, "ERR1-WB*");
			return FALSE;
		}
	}

	 
	/* check DO info */
	pDOStr = GetSubString(3, szInData, &iStrLen);
	if ((iStrLen != 0) && (pDOStr[0] != '$'))  //leave out all DOs is acceptable
	{
		if (iStrLen != (iDONum + 3)/4)  
		{
			TRACE_YDN_TIPS("DOs length is incorrect in WB* command");

			strcpy(szOutData, "ERR1-WB*");
			return FALSE;
		}

		if (!YDN_IsStrAsciiHex(pDOStr, iStrLen))
		{
			TRACE_YDN_TIPS("DOs format is incorrect in WB* command"
				"(should be Hex characters)");

			strcpy(szOutData, "ERR1-WB*");
			return FALSE;
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ExecuteSTRE
 * PURPOSE  : Remote set rectifier
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 16:21
 *==========================================================================*/
static int ExecuteSTRE(YDN_BASIC_ARGS *pThis,
				   unsigned char *szInData,
				   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    //#ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteSTRE!!!!\n");
        TRACE("\nExecuteSTRE:szInData is: %s\n", szInData);
	//#endif
    
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	BOOL bRTN;

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "4180", 4) != 0)
	{
		//sprintf(szOutData, "%x", CMDERR);
		return CMDERR;
	}
 
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		//strcpy(szOutData, CMDERR);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will send command to recitifiers");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
		if(iCMDType == pCurEntry->iYDNPort)
		{
			//pCurEntry = pTypeMap->pMapEntriesInfo;
			break;
		}
		pCurEntry++;
	}
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CTLFAIL;
	}
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    
	bRTN = SetYDNSigValues(TRUE, iEquipID, 1, pCurEntry, szInData + 8);

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
 
	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("STRE has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", szOutData);
        #endif //_DEBUG_YDN_CMD_HANDLER
       
		return NOR;
	}
	else
		return CTLFAIL;
}


/*==========================================================================*
 * FUNCTION : ExecuteRRES
 * PURPOSE  : Read rectifiers status signals
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-27 10:53
 *==========================================================================*/
static int ExecuteRRES(YDN_BASIC_ARGS *pThis,
			          unsigned char *szInData,
			          unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

	UNUSED(pThis);

	int iDataLen = 0;

	if (strncmp(szInData, "4143", 4) != 0)
	{
		return CMDERR;
	}

	iDataLen = strlen(g_RespData.szRespData1[6]);

#ifdef	_DEBUG_YDN
	if((iDataLen) > MAX_YDNFRAME_LEN)
	{
		AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRRES - iDataLen greater than 4114.\n");
	
	}
#endif

	if(iDataLen == 0)
		return CMDERR;
	memcpy(szOutData, g_RespData.szRespData1[6],iDataLen);
	
	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteRREAL
 * PURPOSE  : Read rectifiers alarm signals
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-27 11:34
 *==========================================================================*/
static int ExecuteRREAL(YDN_BASIC_ARGS *pThis,
				      unsigned char *szInData,
				      unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

	UNUSED(pThis);

	int iDataLen = 0;

	/* 1.check validity */
	if (strncmp(szInData, "4144", 4) != 0)
	{
		return CMDERR;
	}

	iDataLen = strlen(g_RespData.szRespData1[7]);

#ifdef	_DEBUG_YDN
	if((iDataLen) > MAX_YDNFRAME_LEN)
	{
		AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRREAL - iDataLen greater than 4114.\n");
	
	}
#endif


	if(iDataLen == 0)
		return CMDERR;
	memcpy(szOutData, g_RespData.szRespData1[7],iDataLen);

	return NOR;
	
}

/*==========================================================================*
 * FUNCTION : ExecuteRT
 * PURPOSE  : RT, read time
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-05 11:34
 *==========================================================================*/
static int ExecuteRT(YDN_BASIC_ARGS *pThis,
				      unsigned char *szInData,
				      unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        printf("\nBegin ExecuteRT!!!!\n");
        printf("\nExecuteRT:szInData is: %s\n", szInData);
	#endif
    
	int nError, nBufLen, iDataLen, i;
	time_t	tCurtime;
	unsigned char *pData;
	unsigned char Year = 20;
	unsigned char Date[6] = {0};
	char szTime[13];

	UNUSED(pThis);

	/* 1.check the command */
	//zwh:����404D�Ķ�ȡʱ��Э�飬��ngc_lf����
	//if (strncmp(szInData, "E1E1", 4) != 0)
	if ( (strncmp(szInData, "E1E1", 4) != 0)
		&& (strncmp(szInData, "404D", 4) != 0) )
	{
		return CMDERR;
	}

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++)//reserved for LENGTH
    {
    	*szOutData = '0';
    	 szOutData++;
    }

	/* 2.read time */
	nError = DxiGetData(VAR_TIME_SERVER_INFO,
		SYSTEM_TIME,			
		0,		
		&nBufLen,			
		&tCurtime,			
		0);
	ConvertTime(&tCurtime, TRUE);
	if (nError != ERR_DXI_OK)
	{
		LOG_YDN_CMD_E("Call DXI interface to get System time failed");
		//sprintf(szOutData, "*");
		return CMDERR;
	}

    sprintf(szOutData, "%02X", Year);
    szOutData += 2;
	//sprintf(szOutData, "%s", 
		//TimeToString(tCurtime, YDN_TIME_FORMAT, szTime, sizeof (szTime)));
	TimeToString(tCurtime, YDN_TIME_FORMAT, szTime, sizeof (szTime));
	TRACE("\nszTime is: %s\n", szTime);
	for(i = 0; i < 6; i++)
	{
		Date[i] = YDN_CharToInt(szTime + (i * 2), 2);
		//sprintf(szOutData, "%02X", Date[i]);
		HexToTwoAsciiData(Date[i], szOutData);
        szOutData += 2;
	}
    iDataLen = 7;
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("RT has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return NOR;
	
}
/*==========================================================================*
 * FUNCTION : ExecuteST
 * PURPOSE  : ST, set time
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-01 11:43
 *==========================================================================*/
static int ExecuteST(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    // #ifdef _DEBUG_YDN_LINKLAYER
        printf("\nBegin ExecuteST!!!!\n");
        printf("\nExecuteST:szInData is: %s\n", szInData);
	//#endif

	int i, j, nError, nLen;
	size_t stLength;
	struct tm  stTime; 
	time_t tTime;
	unsigned char szBuf[6][3];
	unsigned char *pData;

	UNUSED(pThis);

    nLen = 0;
    nLen = strlen(szInData);
	/* 1.check the command */
    stLength = strlen(szInData);
	if (stLength != 18)
	{
		return CMDERR;
	}
	
	if (strncmp(szInData, "E1E2", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get and check param */
	for (i = 4; i < nLen; i++)
	{
		if (szInData[i] < '0')// || szInData[i] > '9')
		{
			return CMDERR;
		}
	}
			
	for (i = 0; i < 6; i++)
	{
		j = 2*(i+1);
		szBuf[i][0] = szInData[j + 4];
		szBuf[i][1] = szInData[j + 1 + 4];
		szBuf[i][2] = '\0';

	}

    stTime.tm_year = (int)YDN_AsciiHexToChar(szBuf[0]) + 100;
	stTime.tm_mon  = (int)YDN_AsciiHexToChar(szBuf[1]) - 1;
	stTime.tm_mday = (int)YDN_AsciiHexToChar(szBuf[2]);

	stTime.tm_hour = (int)YDN_AsciiHexToChar(szBuf[3]);
	stTime.tm_min  = (int)YDN_AsciiHexToChar(szBuf[4]);
	stTime.tm_sec  = (int)YDN_AsciiHexToChar(szBuf[5]);

	if (stTime.tm_mon < 0 || stTime.tm_mon > 11 ||
		stTime.tm_mday < 1 || stTime.tm_mday > 31 ||
		stTime.tm_hour < 0 || stTime.tm_hour > 24 ||
		stTime.tm_min < 0 || stTime.tm_min > 60 ||
		stTime.tm_sec < 0 || stTime.tm_sec > 60)
	{
		TRACE("\nERR4!!\n");
		return CMDERR;
	}

	tTime = mktime(&stTime);
	
	/* 3.set time */
	nError = DxiSetData(VAR_TIME_SERVER_INFO,
		SYSTEM_TIME,			
		0,		
		sizeof(tTime),			
		&tTime,			
		0);
	pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
	if (nError != ERR_DXI_OK)
	{
		LOG_YDN_CMD_E("Call DXI interface to set System time failed");
		return CTLFAIL;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("ST has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteRP
 * PURPOSE  : RP, read product information
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-01 15:01
 *==========================================================================*/
static int ExecuteRP(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
	ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRP!!!!\n");
        TRACE("\nExecuteRT:szInData is: %s\n", szInData);
	#endif
    
	int nError, nBufLen, iDataLen, i;
	//time_t	tCurtime;
	unsigned char *pData;
	//LANG_TEXT *pSCUPName;
	unsigned char pSCUPName[] = {'M', '8', '3', '0', 0, 0, 0, 0, 0, 0};
    
	unsigned char pEmersonName[] = {'V', 'E', 'R', 'T', 'I', 'V'};

	DWORD SCUPSWVer;
    unsigned char cSCUPSWVer[4];
    unsigned char cSCUPSiteName[20];
    
	UNUSED(pThis);

	/* 1.check the command */
	if (strncmp(szInData + 2, "51", 2) != 0)
	{
		return CMDERR;
	}

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++)//reserved for LENGTH
    {
    	*szOutData = '0';
    	 szOutData++;
    }

	nError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_SW_VERSION,			
			0,		
			&nBufLen,			
			&SCUPSWVer,			
			0);
	if (nError != ERR_DXI_OK)
	{
		LOG_YDN_CMD_E("Call DXI interface to get Equip Info failed2");
		return CMDERR;
	}
	
    /*
    iDataLen = strlen(pSCUPName);
    for(i = 0; i < 10; i++)
    	cSCUPSiteName[i] = '0';
    cSCUPSiteName[10] = '\0';
    if(iDataLen > 10)
    	iDataLen = 10;
    for(i = 0; i < iDataLen; i++)
    	cSCUPSiteName[i] = *(pSCUPName + i);
    	
	iDataLen = strlen(pEmersonName);
	if(iDataLen > 20)
		pEmersonName[20] = '\0';
	else
	{
		for(i = iDataLen; i < 20; i ++)
			 pEmersonName[i] = '0';
	    pEmersonName[i] = '\0';
	}
	
    YDN_IntToAscii(SCUPSWVer, cSCUPSWVer);
    
    TRACE("cSCUPSiteName is %s\n",cSCUPSiteName);
    TRACE("cSCUPSWVer is %s\n",cSCUPSWVer);
    TRACE("pEmersonName is %s\n",pEmersonName);
    
	sprintf(szOutData, "%s%s%s", cSCUPSiteName, cSCUPSWVer, pEmersonName);	
     */

    for(i = 0; i < 4; i++)
    {
		HexToTwoAsciiData(pSCUPName[i], szOutData);
		szOutData += 2;
    }
	
	for(i = 0; i < 6; i++)
	{
		HexToTwoAsciiData(0, szOutData);	
		szOutData += 2;
	}
	
	YDN_IntToAscii(SCUPSWVer, cSCUPSWVer);
	sprintf(szOutData, "%s", cSCUPSWVer);
	szOutData += 4;
	
//	for(i = 0; i < 7; i++)
	for(i = 0; i < 6; i++)
	{
		HexToTwoAsciiData(pEmersonName[i], szOutData);
		szOutData += 2;
	}
	
//	for(i = 0; i < 13; i++)
	for(i = 0; i < 14; i++)
	{
		HexToTwoAsciiData(0, szOutData);	
		szOutData += 2;
	}

    szOutData = pData;
    TRACE("szOutData is %s\n",szOutData);
	szOutData = YDN_BuildDataInfo(32, szOutData);

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("RP has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;

}

/*==========================================================================*
 * FUNCTION : ExecuteRP
 * PURPOSE  : RP, read product information
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-01 15:01
 *==========================================================================*/
static int ExecuteRSCUPBarCode(YDN_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRSCUPBarCode!!!!\n");
        TRACE("\nExecuteRSCUPBarCode:szInData is: %s\n", szInData);
	#endif
    
	int nError, nBufLen, iDataLen, i;
	unsigned char *pData;
   
    //LANG_TEXT SCUPBarCode;
  
	UNUSED(pThis);

	/* 1.check the command */
	if (strncmp(szInData, "E1FA", 4) != 0)
	{
		return CMDERR;
	}

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++)//reserved for LENGTH
    {
    	*szOutData = '0';
    	 szOutData++;
    }

	ACU_PRODUCT_INFO sSCUPProductInfo;

	memset(&sSCUPProductInfo, 0, sizeof(sSCUPProductInfo));

	nBufLen = sizeof(sSCUPProductInfo);
	nError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
						ACU_PRODUCT_INFO_GET, 
						0, 
						&nBufLen,
						&(sSCUPProductInfo),
						0);
	
	//sprintf(szOutData, "%s", sSCUPProductInfo.szACUSerialNo);	
	sprintf(szOutData, "%s%s%s", sSCUPProductInfo.szACUSerialNo, sSCUPProductInfo.szPartNumber, sSCUPProductInfo.szHWRevision);
	TRACE("\nsSCUPProductInfo.szPartNumber is: %s\n", sSCUPProductInfo.szPartNumber);
	//TRACE("\nsSCUPProductInfo.szACUSerialNo is: %s\n", sSCUPProductInfo.szACUSerialNo);
    
    //iDataLen = strlen(sSCUPProductInfo.szACUSerialNo);
	iDataLen = strlen(sSCUPProductInfo.szACUSerialNo)
		       + strlen(sSCUPProductInfo.szPartNumber)
			   + strlen(sSCUPProductInfo.szHWRevision);

    szOutData = pData;
	//szOutData = YDN_BuildDataInfo(iDataLen, szOutData);
	WORD wLength, Sum;
 
    wLength = iDataLen;
	Sum = (-(wLength + (wLength>>4) + (wLength>>8) )) << 12;
	wLength = wLength & 0xFFF | Sum;
	//sprintf(szOutData, "%04X%s", wLength, szOutData + 4);
	HexToFourAsciiData(wLength, szOutData);

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("RSCUPBarCode has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return NOR;

}

/*==========================================================================*
 * FUNCTION : GetBlockByEquipID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CurAlarmFormatProcess
 * ARGUMENTS: int  iEquipID : 
 * RETURN   : YDN_BLOCK_INFO* : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-11-30 13:20
 *==========================================================================*/
static YDN_BLOCK_INFO* GetBlockByEquipID(int iEquipID)
{
	int i;
	YDN_MODEL_INFO *pModel;
	YDN_BLOCK_INFO *pBlock;

	pModel = &g_YDNGlobals.YDNModelInfo;
	pBlock = pModel->pYDNBlockInfo;

	for (i = 0; i < pModel->iBlocksNum; i++, pBlock++)
	{
		if (pBlock->iEquipID == iEquipID)
		{
			return pBlock;
		}
	}

	return NULL;
}

///*==========================================================================*
// * FUNCTION : CurAlarmFormatProcess
// * PURPOSE  : 
// * CALLS    : GetBlockByEquipID
// *			  GetYDNSigIdentity
// * CALLED BY: GetCurAlarms
// * ARGUMENTS: BOOL                 bAlarmBlocked:
// *            time_t               tmBlockedTime:
// *			  ALARM_INFO_BUFFER       *pAlarmBuf:		
// *			  ALARM_INFO        *pCurAlarmInfo	:
// *            ALARM_SIG_VALUE   *pAlarmSigValue :             
// * RETURN   : int : error code defined in err_code.h, including:
// *                  ERR_YDN_CMD_OK
// *					ERR_YDN_CMD_NEXT(jump current process)
// *					ERR_YDN_CMD_FAIL
// * COMMENTS : 
// * CREATOR  : HanTao                   DATE: 2004-11-30 11:17
// *==========================================================================*/
//static int CurAlarmFormatProcess(BOOL bAlarmBlocked,
//								 time_t tmBlockedTime,
//								 ALARM_INFO_BUFFER *pAlarmBuf,
//								 ALARM_SIG_VALUE *pAlarmSigValue,
//								 ALARM_INFO *pCurAlarmInfo)
//{
//	YDN_BLOCK_INFO *pBlock;
//	BOOL bRet;
//	int  iEquipID, iACUSigID;
//	int  iYDNSigType, iYDNSigID, iYDNAlarmLevel;
//
//	iEquipID = pAlarmSigValue->pEquipInfo->iEquipID;
//
//	/* outgoing alarm blocked process */
//	if (bAlarmBlocked)
//	{
//		/* not need report */
//		if (!IS_ALARMBLOCKED_ALARM(pAlarmSigValue) && 
//			pAlarmSigValue->sv.tmStartTime > tmBlockedTime)
//		{
//			return ERR_YDN_CMD_NEXT;
//		}
//	}
//
//	/* 1.time format process */
//	TimeToString(pAlarmSigValue->sv.tmStartTime,
//				 YDN_TIME_FORMAT,
//		         pCurAlarmInfo->szStartTime, 
//				 sizeof (pCurAlarmInfo->szStartTime));
//
//	TimeToString(pAlarmSigValue->sv.tmEndTime,
//				 YDN_TIME_FORMAT,
//		         pCurAlarmInfo->szEndTime, 
//				 sizeof (pCurAlarmInfo->szEndTime));
//
//
//	/* 2.Identity format process */
//	/* get relevant block */
//	pBlock = GetBlockByEquipID(iEquipID);
//	if (pBlock == NULL)
//	{
//#ifdef _DEBUG_YDN_CMD_HANDLER
//		TRACE_YDN_TIPS("Find an equip which is not mapped to YDN model");
//		TRACE("Equip ID: %d\n", iEquipID);
//#endif //_DEBUG_YDN_CMD_HANDLER
//
//		return ERR_YDN_CMD_NEXT;
//	}
//	pCurAlarmInfo->iGroupID = pBlock->iGroupID;
//	pCurAlarmInfo->iSubGroupID = pBlock->iSubGroupID;
//	pCurAlarmInfo->iUnitID = pBlock->iUnitID;	
//
//	/* get YDN sig identity */
//	iACUSigID = pAlarmSigValue->pStdSig->iSigID;
//	bRet = GetYDNSigIdentity(pBlock, SIG_TYPE_ALARM, iACUSigID,
//		&iYDNSigType, &iYDNSigID);
//	if (!bRet)
//	{
//#ifdef _DEBUG_YDN_CMD_HANDLER
//		TRACE_YDN_TIPS("Find an sig which is not mapped to YDN model");
//		TRACE("ACU Sig Type: Alarm\t\tACU Sig ID: %d\n", iACUSigID);
//#endif //_DEBUG_YDN_CMD_HANDLER
//
//		return ERR_YDN_CMD_NEXT;
//	}
//
//	/* assign YDN sig identity */
//	switch (iYDNSigType)
//	{
//	case YDN_SIGTYPE_DI:
//		pCurAlarmInfo->cSection = 'I';
//		break;
//
//	case YDN_SIGTYPE_DO:
//		pCurAlarmInfo->cSection = 'O';
//		break;
//
//	default:  //error!
//		LOG_YDN_CMD_E("YDN sig type should be DI or DO for alarms");
//
//		return ERR_YDN_CMD_FAIL;
//	}
//
//	pCurAlarmInfo->iCno = iYDNSigID;
//
//
//
//	/* 3.alarm level format process */
//	switch (pAlarmSigValue->pStdSig->iAlarmLevel)
//	{
//	case ALARM_LEVEL_CRITICAL:
//		iYDNAlarmLevel = YDN_ALARM_LEVEL_A1;		/* A1 */
//		pAlarmBuf->iA1Num++;
//		break;
//
//	case ALARM_LEVEL_MAJOR:
//		iYDNAlarmLevel = YDN_ALARM_LEVEL_A2;		/* A2 */
//		pAlarmBuf->iA2Num++;
//		break;
//
//	case ALARM_LEVEL_OBSERVATION:
//		iYDNAlarmLevel = YDN_ALARM_LEVEL_O1;		/* O1 */
//		pAlarmBuf->iO1Num++;
//		break;
//
//	default:  //error
//		LOG_YDN_CMD_E("New ACU alarm level used, which can not be handled");
//
//		return ERR_YDN_CMD_FAIL;
//	}
//
//	pCurAlarmInfo->iAlarmLevel = iYDNAlarmLevel;
//	return ERR_YDN_CMD_OK;
//
//}

/*==========================================================================*
 * FUNCTION : SortAlarmsByTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: GetCurAlarms
 * ARGUMENTS: YDN_ALARM_INFO  *pAlarmInfo : 
 *            int         iNum        : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-11-30 19:34
 *==========================================================================*/
static void SortAlarmsByTime(YDN_ALARM_INFO *pAlarmInfo, int iNum)
{
	BOOL bDone;
	int i, j;

	i = 0;
	bDone = FALSE;
	while (i < iNum && (!bDone))
	{
		bDone = TRUE;
		for (j = 1; j < iNum - i; j++)
		{
			if (strcmp(pAlarmInfo[j].szStartTime, 
					pAlarmInfo[j - 1].szStartTime) > 0)
			{
				bDone = FALSE;
				SWAP(YDN_ALARM_INFO, pAlarmInfo[j], pAlarmInfo[j - 1]);
			}
		}

		i++;
	}

}


/*==========================================================================*
 * FUNCTION : SortAlarmsByLevel
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: GetCurAlarms
 * ARGUMENTS: YDN_ALARM_INFO  *pAlarmInfo : 
 *            int         iNum        : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-11-30 19:17
 *==========================================================================*/
static void SortAlarmsByLevel(YDN_ALARM_INFO *pAlarmInfo, int iNum)
{
	BOOL bDone;
	int i, j;
	int iLevel, iStartPos, nAlarms;
	
	/* sort by Level */
	i = 0;
	bDone = FALSE;
	while (i < iNum && (!bDone))
	{
		bDone = TRUE;
		for (j = 1; j < iNum - i; j++)
		{
			if (pAlarmInfo[j].iAlarmLevel < pAlarmInfo[j - 1].iAlarmLevel)
			{
				bDone = FALSE;
				SWAP(YDN_ALARM_INFO, pAlarmInfo[j], pAlarmInfo[j - 1]);
			}
		}

		i++;
	}

	/* sort by time for each level */
	iStartPos = 0;
	iLevel = pAlarmInfo[iStartPos].iAlarmLevel;
	nAlarms = 0;
	for (i = 0; i < iNum; i++, nAlarms++)
	{
		if (pAlarmInfo[i].iAlarmLevel != iLevel)
		{
			SortAlarmsByTime(&pAlarmInfo[iStartPos], nAlarms);
			iStartPos = i;
			nAlarms = 0;

			iLevel = pAlarmInfo[i].iAlarmLevel;
		}
	}

	/* sort the last level */
	SortAlarmsByTime(&pAlarmInfo[iStartPos], nAlarms);

}

/*==========================================================================*
 * FUNCTION : ClearAlarmInfoBuf
 * PURPOSE  : used to clear alarm info buffer
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL               bCurrent   : TRUE for Active Alarms
 *											  FALSE for History Alarms
 *            YDN_ALARM_INFO_BUFFER  *pAlarmBuf : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-02 15:28
 *==========================================================================*/
static void ClearAlarmInfoBuf(BOOL bCurrent, YDN_ALARM_INFO_BUFFER *pAlarmBuf)
{
	int iAlarmNum;
	
	/* clear statistic data */
	pAlarmBuf->iA1Num = 0;
	pAlarmBuf->iA2Num = 0;
	pAlarmBuf->iO1Num = 0;

	if (bCurrent)  //for active alarm info
	{
		iAlarmNum = pAlarmBuf->iCurAlarmNum;
		pAlarmBuf->iCurAlarmNum = 0;

		if (iAlarmNum > YDN_ALARM_BUF_SIZE)  //use dynamic buf
		{
			DELETE(pAlarmBuf->pCurAlarmInfo);
			pAlarmBuf->pCurAlarmInfo = NULL;
		}
		else		//use static buf
		{
			memset(pAlarmBuf->sCurAlarmInfo, 0, 
				sizeof(pAlarmBuf->sCurAlarmInfo));
		}
	}
	else  //for history alarm info
	{
		pAlarmBuf->iHisAlarmNum = 0;

		DELETE(pAlarmBuf->pHisAlarmInfo);
		pAlarmBuf->pHisAlarmInfo = NULL;
	}

	return;
}



/*==========================================================================*
 * FUNCTION : GetAlarmBlockedTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ALARM_SIG_VALUE  *pAlarmSigValueBuf : 
 *            int              nSigNum            : 
 * RETURN   : time_t : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2005-01-18 20:05
 *==========================================================================*/
static time_t GetAlarmBlockedTime(ALARM_SIG_VALUE *pAlarmSigValue,
								  int nSigNum)
{
	int i;

	for (i = 0; i < nSigNum; i++, pAlarmSigValue++)
	{
		if (IS_ALARMBLOCKED_ALARM(pAlarmSigValue))
		{
			return pAlarmSigValue->sv.tmStartTime;
		}
	}

	LOG_YDN_CMD_E("Alarm blocked flag is inconsistent with current"
		"alarm info");
	return 0;
}

/*==========================================================================*
 * FUNCTION : ExecuteWACP
 * PURPOSE  : Write AC parameters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 15:37
 *==========================================================================*/
static int ExecuteWACP(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteWACP!!!!\n");
        TRACE("\nExecuteWACP:szInData is: %s\n", szInData);
	#endif
    
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	BOOL bRTN;

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "4048", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will write data to AC");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
    
	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
	    //TRACE("\npCurEntry->iYDNPort: %d\n", pCurEntry->iYDNPort);
		if(iCMDType == pCurEntry->iYDNPort)
		{
			//pCurEntry = pTypeMap->pMapEntriesInfo;
			break;
		}
		pCurEntry++;
	}
    if(i == pTypeMap->iMapEntriesNum)
    {
		return CMDERR;
    }
	
	
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CTLFAIL;
	}
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	
	bRTN = SetYDNSigValues(TRUE, iEquipID, 1, pCurEntry, szInData + 6);
	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("WACP has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", szOutData);
        #endif //_DEBUG_YDN_CMD_HANDLER
       
		return NOR;
	}
	else
		return CTLFAIL;
}

/*==========================================================================*
 * FUNCTION : IfValidWACP
 * PURPOSE  : judge if the value is valid
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2007-02-28 15:37
 *==========================================================================*/
int IfValidWACP(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
	ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteWACP!!!!\n");
        TRACE("\nExecuteWACP:szInData is: %s\n", szInData);
	#endif
    
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	union YDN_FVALUE YDN_uf;
	union YDN_FVALUE tmpYNDFValue;
	//BOOL bRTN;

	//int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "4048", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will write data to AC");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

	for(i = 0; i < 4; i++)
	{
		tmpYNDFValue.cValue[i] = (int)YDN_AsciiHexToChar(szInData + 6 + i * 2);
	}

	for (i = 0; i < 4; i++)
	{
	    YDN_uf.cValue[i] = tmpYNDFValue.cValue[3-i];
	}
    
	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
	    //TRACE("\npCurEntry->iYDNPort: %d\n", pCurEntry->iYDNPort);
		if(iCMDType == pCurEntry->iYDNPort)
		{
			//pCurEntry = pTypeMap->pMapEntriesInfo;
			break;
		}
		pCurEntry++;
	}
    if(i == pTypeMap->iMapEntriesNum)
    {
		return CMDERR;
    }
/*
	TRACE("pCurEntry->iYDNPort is %d\n", pCurEntry->iYDNPort);
	TRACE("pCurEntry->iLen is %d\n", pCurEntry->iLen);
	TRACE("YDN_uf.fValue is %f\n", YDN_uf.fValue);
	TRACE("pCurEntry->fPeakValue[0] is %f\n", pCurEntry->fPeakValue[0]);
	TRACE("pCurEntry->fPeakValue[1] is %f\n", pCurEntry->fPeakValue[1]);
	*/

	if(YDN_uf.fValue < pCurEntry->fPeakValue[0] || YDN_uf.fValue > pCurEntry->fPeakValue[1])
		return CMDERR;

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ReadAlarmFromBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ExecuteRC
 * ARGUMENTS: BOOL          bHisAlarm     :
 *			  int           iSequenceID	  : 
 *            YDN_ALARM_INFO    *pAlarmInfo   : 
 *            int            iAlarms      : total alarms in the buff
 *            unsigned char  *szOutData   : 
 *			  BOOL *pbReadAll			  : output flag
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-11-30 20:20
 *==========================================================================*/
static void ReadAlarmFromBuf(BOOL bHisAlarm,
							 IN int iSequenceID, 
							 IN YDN_ALARM_INFO *pAlarmInfo, 
							 IN int iAlarms, 
							 OUT BOOL *pbReadAll,
							 OUT unsigned char *szOutData)
{
	int i, iPos, iOffSet;
	YDN_ALARM_INFO *pCurAlarmInfo;

	*pbReadAll = FALSE;

	/* 1.for head */
	sprintf(szOutData, "%02X#", iSequenceID);
	iOffSet = 3;

	/* 2.for alarm info one by one */
	iPos = 10 * iSequenceID;
	pCurAlarmInfo = pAlarmInfo + iPos;
	for (i = 0; i < 10 && iPos < iAlarms; i++, iPos++, pCurAlarmInfo++)
	{
		if (bHisAlarm)
		{
			sprintf(szOutData + iOffSet, "%s!", pCurAlarmInfo->szEndTime);
			iOffSet += 13;
		}

		sprintf(szOutData + iOffSet, "%s!%02X%02X%1X!%c%02X!%1X#",
			pCurAlarmInfo->szStartTime, pCurAlarmInfo->iGroupID,
			pCurAlarmInfo->iUnitID, pCurAlarmInfo->iSubGroupID,
			pCurAlarmInfo->cSection, pCurAlarmInfo->iCno, 
			pCurAlarmInfo->iAlarmLevel);

		iOffSet += 25;
	}

	/* have read all the alarms */
	if (i < 10)
	{
		*pbReadAll = TRUE;
	}

	
	/* change the last '#' to '*' */
	if (iOffSet != 3)
	{
		szOutData[--iOffSet] = '*';
	}
	else	// means empty, reply: xx*
	{
		szOutData[2] = '*';
		szOutData[3] = '\0';
	}

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteACAL
 * PURPOSE  : Read AC Alarms Signals
 * CALLS    : 
 *			  
 *			  
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 16:19
 *==========================================================================*/
static int ExecuteRACAL(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRACAL!!!!\n");
        TRACE("\nExecuteRACAL:szInData is: %s\n", szInData);
	#endif
  
	int i, j, k, m, iDataLen;
	int iCID1,iCID2, iData[5] = {0};
	int iMainInputNo = 0;
	unsigned char *pData, pMainInputNo[2];
	unsigned char **pAnalogAlarm;
	unsigned char *pTemp;
	//unsigned char DataFlag[2] = {'0', '0'};
	char logText[YDN_LOG_TEXT_LEN];
	BOOL bDxiRTN;
    //WORD wLength, Sum;
    
	int  iEquipIDOrder, iEquipID, iCommGrp;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	HANDLE hLocalThread;
	
	UNUSED(pThis);

    hLocalThread = RunThread_GetId(NULL);

	/* 1.check validity */
	if (strncmp(szInData, "4044", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	iCommGrp = (int)YDN_AsciiHexToChar(szInData + 4);

	if(iCommGrp == ALL_AC_OR_DC)
	{
		iDataLen = strlen(g_RespData.szRespData1[2]);

#ifdef	_DEBUG_YDN
		if((iDataLen) > MAX_YDNFRAME_LEN)
		{
			AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRACAL - iDataLen greater than 4114.\n");
		
		}
#endif

	    if(iDataLen == 0)
		   return CMDERR;
	    memcpy(szOutData, g_RespData.szRespData1[2],iDataLen);
		return NOR;
	}

	if((iCommGrp > ACD_MAX_NUM || iCommGrp < ACD_MIN_NUM) && iCommGrp != ALL_AC_OR_DC)
		return CMDERR;
	
	pTypeMap = GetTypeMap(iCID1, iCID2);
	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER
    
    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    // 2.Package the DataFlag

    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	  return CMDERR;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,DATAFLAG,0);
	
	pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    //iEquipTypeID = pCurEntry->iEquipType;
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
    if(iData[0] == 0)
		return CMDERR;
	if(iCommGrp == ALL_AC_OR_DC)
	{
        //GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData);
        //iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
        iDataLen++;
        szOutData += 2;
	}
	pCurEntry++;

	//find out the first place of the equiptype
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CMDERR;
	}
	pFixedPos = pCurEntry;

    if(iCommGrp != ALL_AC_OR_DC)
    {
        if(iCommGrp < 0)
        	return CMDERR;
        else if(iCommGrp == 0)
        {
            if(iData[0] > 1)
            	return CMDERR;
        }
        else
        {
            if(iCommGrp > iData[0])
            	return CMDERR;
            iEquipIDOrder = iEquipIDOrder + iCommGrp - 1;//pack some ACD data;
        }
        iData[0] = 1;
    }
    
    for(i = 0; i < iData[0]; i++, iEquipIDOrder++)
    {
       RunThread_Heartbeat(hLocalThread);
       
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];//find out the equipID orderly

	   //TRACE("iEquipID is %d", iEquipID);

      bDxiRTN= GetSCUPSingleSigValues(FALSE, 0, 62, iEquipID, pMainInputNo);
	if(bDxiRTN == FALSE)							//for equipment is not exist by ZTZ 
	{
		iMainInputNo = 0;//mains input No.
	}
	else
	{
		iMainInputNo = (int)YDN_AsciiHexToChar(pMainInputNo);//mains input No.
	}
       
       TRACE("\nMainInputNo is %d\n", iMainInputNo);
       GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
       iData[1] = (int)YDN_AsciiHexToChar(szOutData);//ACInputNum
	   iData[2] = pCurEntry->iFlag1;//fixed Num
	   pCurEntry++;
       iDataLen++;
       szOutData += 2;
       //for(j=0; j < iData[1]; j++)
	   for(j=1; j <= iData[1]; j++)
       {
		   pTemp = szOutData;
		   pAnalogAlarm = &pTemp;
          //if(j == iMainInputNo)
          //{
              for(k = 0; k < iData[2]-1; k++)
              {
			    // if(k == 0)
				    //pAnalogAlarm = &szOutData;

                 GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
				 int jj = (int)YDN_AsciiHexToChar(szOutData);
				 if(pCurEntry->iYDNPort == 4)
					 TRACE("jj = %d\n", jj);
                 ExchangeYDNSigValue(szOutData, pCurEntry);
                 pCurEntry++;
                 szOutData += 2;
                 iDataLen++;
              }
          //}
        //  //else
        //  {
        //      for(k = 0; k < iData[2]; k++)
        //      {
			     //pCurEntry++;
        //         sprintf(szOutData, "%02X", 0x00);//send 0x00 wen no use main input No.
        //         szOutData += 2;
        //         iDataLen++;
        //      }
        //  }
	GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
	pCurEntry++;
	szOutData += 2;
	iDataLen++;

          iData[3] = (int)YDN_AsciiHexToChar(szOutData - 2);//FuseNum
         
		  for(k = 0; k < iData[3]; k++)
          {
             GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
             ExchangeYDNSigValue(szOutData, pCurEntry);
             pCurEntry++;
             szOutData += 2;
             iDataLen++;
          }
		  pCurEntry = pCurEntry + (AC_MAX_FUSE_NUM - iData[3]);

          iData[4] = pCurEntry->iFlag1;//Defined Num
          sprintf(szOutData, "%02X", iData[4]);
          pCurEntry++;
          iDataLen++;
          szOutData += 2;
          for(k = 0; k < iData[4]; k++)
          {
             GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);

			 if(k == 6)
			 {
			    bACDNoRespFlag = (int)YDN_AsciiHexToChar(szOutData);
				TRACE("bACDNoRespFlag is %d\n",bACDNoRespFlag);
				if(bACDNoRespFlag)
				{
					for(m = 0; m < 8; m++)
						//HexToTwoAsciiData(0x00, pAnalogAlarm + m * 2);
						*(*pAnalogAlarm + m) = 0x30;
					bACDNoRespFlag = 0;
				}
			 }

             ExchangeYDNSigValue(szOutData, pCurEntry);
             pCurEntry++;
             szOutData += 2;
             iDataLen++;
          }
       }
       pCurEntry = pCurEntry + (AC_MAX_INPUT_ROUTE - iData[1]) * (iData[2] + iData[3] + iData[4] + 1);

       /*
       for(k = 0; k < 3; k++, pCurEntry++)
       {
          GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData);
          ExchangeYDNSigValue(szOutData, pCurEntry);
          szOutData += 2;
          iDataLen++;
       }
       */
        for(k = 0; k < 6; k++, szOutData++)
        	*szOutData = 0x20;
		iDataLen += 3;

	   pCurEntry = pFixedPos;
    }
    // 4.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);
    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RACA has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteRACP
 * PURPOSE  : Read AC Parameters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 14:42
 *==========================================================================*/
static int ExecuteRACP(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRACP!!!!\n");
        TRACE("\nExecuteRACP:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen, k;
	int iCID1,iCID2, iData[2];
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "4046", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

    // 2.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	iData[0] = pCurEntry->iFlag1;//fixed Num

    for(i=0; i< iData[0]; i++, pCurEntry++)
    {
		if(pCurEntry->iFlag0 == 0)
		{
			for(k = 0; k < 8; k++, szOutData++)
		   {
           	  *szOutData = 0x20;
		   }
		}
		else
		{
           GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           szOutData += 8;
		}
        iDataLen += 4;
	}
    iData[1] = pCurEntry->iFlag1;//Defined Num
    sprintf(szOutData, "%02X", iData[1]);
    pCurEntry++;
    iDataLen++;
	szOutData += 2;
    for(i = 0; i < iData[1]; i++, pCurEntry++)
    {
       GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
       szOutData += 8;
       iDataLen += 4;
	}

    // 3.calculate LCHKSUM and package it 
	szOutData = pData; 
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RACA has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}



/*==========================================================================*
 * FUNCTION : CompareProc
 * PURPOSE  : realization of COMPARE_PROC, used by DAT_StorageSearchRecords
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const void  *pRecord    : 
 *            const void  *pCondition : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-28 15:00
 *==========================================================================*/
static int YDN_CompareProc(const void *pRecord, const void *pCondition)
{
	UNUSED(pCondition);
	return (stricmp(pRecord, "Head of a Battery Test") == 0 ? TRUE : FALSE);
}

/*==========================================================================*
 * FUNCTION : *GetBTResult
 * PURPOSE  : assisitant function
 * CALLS    : 
 * CALLED BY: BattTestLogFormatProcess
 * ARGUMENTS: int  iResultCode : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 11:45
 *==========================================================================*/
__INLINE static char *GetBTResult(int iResultCode)
{
	switch (iResultCode)
	{
	case RESULT_NONE:
		return "No test result";

	case RESULT_GOOD:
		return "Battery is OK";

	case RESULT_BAD:
		return "Battery is bad";

	case RESULT_POWER_SPLIT:
		return "It's a Power Split Test";

	default:
		return "Not defined test result";
	}
}


/*==========================================================================*
 * FUNCTION : *GetBTStartReason
 * PURPOSE  : assisitant function
 * CALLS    : 
 * CALLED BY: BattTestLogFormatProcess
 * ARGUMENTS: int  iReasonCode : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 11:45
 *==========================================================================*/
__INLINE static char *GetBTStartReason(int iReasonCode)
{
	switch (iReasonCode)
	{
	case START_PLANNED_TEST:
		return "Planned Test";

	case START_MANUAL_TEST:
		return "Manual Test";

	case START_AC_FAIL_TEST:
		return "AC Failure Test";

	case START_MASTER_TEST:
		return "Power Splitter Test";

	default:
		return "Undefined Test";
	}
}


/*==========================================================================*
 * FUNCTION : *GetBTEndReason
 * PURPOSE  : assisitant function
 * CALLS    : 
 * CALLED BY: BattTestLogFormatProcess
 * ARGUMENTS: int  iReasonCode : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 11:45
 *==========================================================================*/
__INLINE static char *GetBTEndReason(int iReasonCode)
{
	switch (iReasonCode)
	{
	case END_MANUAL:
		return "Stop test manually";

	case END_ALARM:			
		return "Stop test for alarm";

	case END_TIME:			
		return "Stop test for test time-out";

	case END_CAPACITY:		
		return "Stop test for capacity condition";

	case END_VOLTAGE:		
		return "Stop test for voltage condition";

	case END_AC_FAIL:		
		return "Stop test for AC fail";

	case END_AC_RESTORE:		
		return "Stop AC fail test for AC restore";

	case END_AC_FAIL_TEST_DIS:
		return "Stop AC fail test for disabled";

	case END_MASTER_STOP:	
		return "Stop test for Master Power stop test";

	case END_TURN_TO_MAN:	
		return "Stop a PowerSplit BT for Auto/Man turn to Man";

	case END_TURN_TO_AUTO:	
		return "Stop PowerSplit Man-BT for Auto/Man turn to Auto";

	default:
		return "Undefined Reason";
	}
}


/*==========================================================================*
 * FUNCTION : BattTestLogFormatProcess
 * PURPOSE  : 
 * CALLS    : GetBTResult
 *			  GetBTStartReason
 *			  GetBTEndReason
 * CALLED BY: ReadBattTestLogToBuf
 * ARGUMENTS: int              iBattLogID   : 
 *            BT_LOG_SUMMARY   *pSummary    : 
 *            BT_LOG_BATT_CFG  *pLogBattCfg : 
 *            char             *sLogDetail  : 
 *            char             **pszBattLog : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 11:44
 *==========================================================================*/
static BOOL BattTestLogFormatProcess(int iBattLogID,
									 BT_LOG_SUMMARY *pSummary,
									 BT_LOG_BATT_CFG *pLogBattCfg,
									 char *sLogDetail,
									 char **pszBattLog)
{
	UNUSED(sLogDetail);
	LANG_TEXT *pLangInfo;
	int nBufLen;

    int iDstOffset, nError;
	float fRatedCap;
	BT_LOG_BATT_CFG *pCurLogBattCfg;

	char strStartTime[20], strEndTime[20];
#define BT_TIME_FORMAT  "%Y-%m-%d %H:%M:%S"

    /* the memory will be deleted after report */
	*pszBattLog = NEW(char, YDN_BATT_TESTLOG_SIZE);
	if (*pszBattLog == NULL)
	{
		LOG_YDN_CMD_E("no memory for read battery test log");
		return FALSE;
	}
	*pszBattLog[0] = '\0';

	/* 1.write site name and ccid */
	pLangInfo = NULL;
	nError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		SITE_NAME,			
		0,		
		&nBufLen,			
		&pLangInfo,			
		0);

	if (nError != ERR_DXI_OK)
	{
		DELETE(*pszBattLog);
		return FALSE;
	}

	iDstOffset = sprintf(*pszBattLog, "Site: %s\tCCID: %d\n", 
		pLangInfo->pFullName[0], YDN_GetADR());

	/* 2.write log summary info */
	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Battery Test Log NO: %d\t"
		"Test Result: %s\n", iBattLogID, GetBTResult(pSummary->iTestResult));

	TimeToString(pSummary->tStartTime, BT_TIME_FORMAT, 
		strStartTime, sizeof(strStartTime));
	TimeToString(pSummary->tEndTime, BT_TIME_FORMAT, 
		strEndTime, sizeof(strEndTime));
	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Test Start Time: %s\n"
		"Test End Time: %s\n", strStartTime, strEndTime);

	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Test Start Reason: %s\n", 
			GetBTStartReason(pSummary->iStartReason));
	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Test End Reason: %s\n", 
			GetBTEndReason(pSummary->iEndReason));

	//iDstOffset += sprintf(*pszBattLog + iDstOffset, "Quantity of Battery "
			//"Strings: %d\n", pSummary->iQtyBatt);

	/* 3.write log detail data(only write capacity) */
	iDstOffset += sprintf(*pszBattLog + iDstOffset, "\nRemained Capacity:\n");
	fRatedCap = pSummary->fBattRatedCap;
	pCurLogBattCfg = pLogBattCfg;
/*
	for (i = 0, iSrcOffset = 8; i < pSummary->iQtyBatt; i++, pCurLogBattCfg++)
	{
        #ifdef _TEST_YDN_BATTLOG
		   TRACE("\n\n\n\n\nCapcity configured is %d\n", BATT_CFGRPOP_TEST(pCurLogBattCfg, BATT_CFGPROT_CAP));
        #endif //_TEST_YDN_BATTLOG

		if (BATT_CFGRPOP_TEST(pCurLogBattCfg, BATT_CFGPROT_CAP))
		{
			fCapacity = *(float *)(sLogDetail + iSrcOffset +
				pCurLogBattCfg->iOffsetCap);
			iDstOffset += sprintf(*pszBattLog + iDstOffset, "\tBattery String"
				"%d: %.1fAh (%.1f%c)\n", i+1, fCapacity, 
				fCapacity/fRatedCap*100, '\x25');
		}

		iSrcOffset += pCurLogBattCfg->iOffsetTotal;
	}
*/
	return TRUE;
}



/*==========================================================================*
 * FUNCTION : ReadBattTestLogToBuf
 * PURPOSE  : 
 * CALLS    : BattTestLogFormatProcess
 * CALLED BY: 
 * ARGUMENTS: IN int    iBattLogID   : 
 *            OUT char  **pszBattLog : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 11:44
 *==========================================================================*/
static BOOL ReadBattTestLogToBuf(IN int iLogID, OUT char **pszBattLog)
{
	HANDLE	hTestLog;
	int sRecordNoBuff[YDN_BATT_TESTLOG_NUM];
	int  iBattLogID, iStartNo, iRet, iRecords;
	BOOL bRet;

	BT_LOG_SUMMARY *pLogSummary;
	BT_LOG_BATT_CFG *pLogBattCfg, *pCurLogBattCfg; 
	int i, j, k, iQtyBatt;
	char SummaryRecordBuf[128], CfgRecordBuf[128];
	char *sBTLogDetail;

	iBattLogID = iLogID;

	/* 1.open the storage */
	hTestLog = DAT_StorageOpen(BATT_TEST_LOG_FILE);
	if (hTestLog == NULL)
	{
		LOG_YDN_CMD_E("Open Battery Test Log storage failed");
		return FALSE;
	}

	/* 2.search the record no */
	iStartNo = -1;
	iRet = DAT_StorageSearchRecords(hTestLog, 
		YDN_CompareProc, 
		NULL,
		&iStartNo,           //search from the last record
		&iBattLogID, 
		sRecordNoBuff, 
		FALSE,
		FALSE,
		TRUE);

	if (!iRet || iBattLogID != iLogID || iBattLogID <= 1)
	{
		DAT_StorageClose(hTestLog);

#ifdef _DEBUG_YDN_CMD_HANDLER
		TRACE_YDN_TIPS("Seach Battery Log failed");
		TRACE("Log No. is: %d\n", iBattLogID);
		TRACE("iRet value is: %d\n", iRet);
#endif //_DEBUG_YDN_CMD_HANDLER

		return FALSE;
	}


#ifdef _TEST_YDN_BATTLOG
	{
		int ii;
		TRACE("\n\n\n\n\n\nBattLog Head Record No. Info:\n");
		for (ii = 0; ii < iBattLogID; ii++)
		{
			TRACE("Record No. of Battery Log %d is: %d\n", 
				ii + 1, sRecordNoBuff[ii]);
		}
	}
#endif //_TEST_YDN_BATTLOG


	/* 3.read the test log summary info */
	iRecords = 1;  
	iStartNo = sRecordNoBuff[iBattLogID-1];
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	iRet = DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, (void *)SummaryRecordBuf, FALSE, TRUE);
	pLogSummary = (BT_LOG_SUMMARY *)SummaryRecordBuf + 64;
	if (iRet == FALSE)
	{
		DAT_StorageClose(hTestLog);

        #ifdef _DEBUG_YDN_CMD_HANDLER
		    TRACE_YDN_TIPS("Read Battery Test Log Summary Info failed");
		    TRACE("Log No. is: %d\n", iBattLogID);
        #endif //_DEBUG_YDN_CMD_HANDLER

		return FALSE;
	}
	//iQtyBatt = pLogSummary->iQtyBatt;

#ifdef _TEST_YDN_BATTLOG
	{
		char strStartTime[20], strEndTime[20];
#define BT_TIME_FORMAT  "%Y-%m-%d %H:%M:%S"
		TimeToString(pLogSummary->tStartTime, BT_TIME_FORMAT, 
			strStartTime, sizeof(strStartTime));
		TimeToString(pLogSummary->tEndTime, BT_TIME_FORMAT, 
			strEndTime, sizeof(strEndTime));
		TRACE_YDN_TIPS("Read Battery Test Log Summary Info OK");
		TRACE("Summary Info:\n");
		TRACE("Test Start Time:%s\nTest End Time:%s\n",  strStartTime, strEndTime);
		TRACE("Test Start Reason: %d\nTest End Reason: %d\n", 
			pLogSummary->iStartReason, pLogSummary->iEndReason);
		TRACE("Test Result: %d\n", pLogSummary->iTestResult);
		TRACE("Rated Capacity: %f\n", pLogSummary->fBattRatedCap);
		TRACE("Battery String quentity: %d\n", pLogSummary->iQtyBatt);
		TRACE("Battery Test Record quentity: %d\n", pLogSummary->iQtyRecord);
		TRACE("Record Size: %d\n", pLogSummary->iSizeRecord);
		
	}
#endif //_TEST_YDN_BATTLOG



	/* 4.read battery string config info */
	iRecords = 1;
	iStartNo = sRecordNoBuff[iBattLogID-1] + 1;
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	iRet = DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, CfgRecordBuf, FALSE, TRUE);
	if (iRet == FALSE)
	{
		DAT_StorageClose(hTestLog);

#ifdef _DEBUG_YDN_CMD_HANDLER
		TRACE_YDN_TIPS("Read Battery Test Log Config Info failed");
		TRACE("Log No. is: %d\n", iBattLogID);
#endif //_DEBUG_YDN_CMD_HANDLER

		return FALSE;
	}
	pLogBattCfg = NEW(BT_LOG_BATT_CFG, iQtyBatt);

#ifdef _TEST_YDN_BATTLOG
	TRACE("\n\n\n\n\nBatt Test Config Info:\n String 1 Capa %d\n "
		"String 2 capa: %d\n", CfgRecordBuf[2], CfgRecordBuf[7]);
#endif //_TEST_YDN_BATTLOG

	if (pLogBattCfg == NULL)
	{
		LOG_YDN_CMD_E("No memory for read battery test log");
		DAT_StorageClose(hTestLog);
		return FALSE;
	}
	ZERO_POBJS(pLogBattCfg, iQtyBatt);


	pCurLogBattCfg = pLogBattCfg;
	for (i = 0, j = 0, k = 0; i < iQtyBatt; i++, pCurLogBattCfg++)
	{
		if (CfgRecordBuf[j] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_CUR);
			pCurLogBattCfg->iOffsetCur = 0;
			k++;
		}
		if (CfgRecordBuf[j + 1] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_VOL);
			pCurLogBattCfg->iOffsetVol = 4 * k;
			k++;
		}
		if (CfgRecordBuf[j + 2] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_CAP);
			pCurLogBattCfg->iOffsetCap = 4 * k;
			k++;
		}
		if (CfgRecordBuf[j + 3] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_TEMP);
			pCurLogBattCfg->iOffsetTemp = 4 * k;
			k++;
		}

		pCurLogBattCfg->iQtyBlockVol = CfgRecordBuf[j + 4];
		pCurLogBattCfg->iOffsetBlockVol = 4 * k;

		k = 0;
		j += 5;
	}

	/* 5.read data */
	//iRecords = (pLogSummary->iQtyRecord * pLogSummary->iSizeRecord - 1)/128 + 1;
	iStartNo = sRecordNoBuff[iBattLogID - 1] + 2;
	sBTLogDetail = NEW(char, iRecords*128);


#ifdef _TEST_YDN_BATTLOG
	TRACE("\n\n\n\n\n\niRecords value is: %d\n", iRecords);
	TRACE("Detail Data buffer is: %d\n\n\n\n", iRecords*128);
#endif //_TEST_YDN_BATTLOG



	if (sBTLogDetail == NULL)
	{
		LOG_YDN_CMD_E("no memory for read battery test log");
		DELETE(pLogBattCfg);
		DAT_StorageClose(hTestLog);
		return FALSE;
	}
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	if (DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, sBTLogDetail, FALSE, TRUE) == FALSE)
	{
		DELETE(pLogBattCfg);
		DELETE(sBTLogDetail);

#ifdef _DEBUG_YDN_CMD_HANDLER
		TRACE_YDN_TIPS("Read Battery Test Log Detail Info failed");
		TRACE("Log No. is: %d\n", iBattLogID);
#endif //_DEBUG_YDN_CMD_HANDLER

		DAT_StorageClose(hTestLog);
		return FALSE;
	}

	/* 6.convert to the YDN format */
	bRet = BattTestLogFormatProcess(iBattLogID, pLogSummary, 
			pLogBattCfg, sBTLogDetail, pszBattLog);

	/* clear */
	DELETE(pLogBattCfg);
	DELETE(sBTLogDetail);
	DAT_StorageClose(hTestLog);

	if (!bRet)
	{
		return FALSE;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : GetBattTestLogFromBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ExecuteRL
 * ARGUMENTS: int   iSequenceID : 
 *            char  *szOut      : the length of the array is
*								  YDN_BATT_TESTLOG_SEG + 2
 *            char  *szBTLog    : battery test log buffer
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 14:05
 *==========================================================================*/
static int GetBattTestLogFromBuf(int iSequenceID, char *szOut, char *szBTLog)
{
	int iLogSize;

	/* buffer has not perpared */
	if (szBTLog == NULL)
	{
		strcpy(szOut, "*");
		return 0;
	}

	iLogSize = (int)strlen(szBTLog);


	if (iSequenceID > (iLogSize-1)/YDN_BATT_TESTLOG_SEG || iLogSize == 0)
	{
		strcpy(szOut, "*");
		return 0;
	}

	strncpy(szOut, szBTLog + iSequenceID * YDN_BATT_TESTLOG_SEG, 
		YDN_BATT_TESTLOG_SEG);
	strcat(szOut, "*");

	return (int)strlen(szOut) - 1;
}


/*==========================================================================*
 * FUNCTION : ExecuteRREA
 * PURPOSE  : Read rectifiers analog signals
 * CALLS    : 
 *			  
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-27 11:00
 *==========================================================================*/
static int ExecuteRREA(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

	UNUSED(pThis);

	int iCID2;
	int iDataLen = 0;

	/* 1.check validity */
	if ((strncmp(szInData, "4141", 4) != 0) 
	     && (strncmp(szInData, "41E2", 4) != 0)
	     && (strncmp(szInData, "41E4", 4) != 0))
	{
		return CMDERR;
	}

	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);

	if(iCID2 == READ101TO150RECT_A_DATA)
	{
		iDataLen = strlen(g_RespData.szRespData1[10]);

#ifdef	_DEBUG_YDN
		if((iDataLen) > MAX_YDNFRAME_LEN)
		{
			AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRREA if - iDataLen greater than 4114.\n");

		}
#endif


		if(iDataLen == 0)
			return CMDERR;
		memcpy(szOutData, g_RespData.szRespData1[10],iDataLen);
	}
	else if(iCID2 == READ51TO100RECT_A_DATA)
	{
		iDataLen = strlen(g_RespData.szRespData1[8]);

#ifdef	_DEBUG_YDN
		if((iDataLen) > MAX_YDNFRAME_LEN)
		{
			AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRREA if - iDataLen greater than 4114.\n");
		
		}
#endif

		
		if(iDataLen == 0)
		   return CMDERR;
	    memcpy(szOutData, g_RespData.szRespData1[8],iDataLen);
	}
	else
	{
	    
		iDataLen = strlen(g_RespData.szRespData1[5]);

#ifdef	_DEBUG_YDN
		if((iDataLen) > MAX_YDNFRAME_LEN)
		{
			AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRREA else - iDataLen greater than 4114.\n");
		
		}
#endif


	    if(iDataLen == 0)
		   return CMDERR;
	    memcpy(szOutData, g_RespData.szRespData1[5],iDataLen);
	}
	
	
	//int i, j;
	int iCID1, iCID3;
	//unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	unsigned char DataFlag[2] = {'1', '0'};
	BOOL bRTN = FALSE;

	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if ((strncmp(szInData, "4141", 4) != 0) 
	     && (strncmp(szInData, "41E2", 4) != 0)
	     && (strncmp(szInData, "41E4", 4) != 0))
	{
		return CMDERR;
	}

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	iCID3 = iCID2;
	if((iCID3 == 0xE2)||(iCID3 == 0xE4))
		iCID2 = 0x41;
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}


	pCurEntry = pTypeMap->pMapEntriesInfo;
	iEquipIDOrder = 0;

	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return NOR;
	}
	if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    		iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, DataFlag);
	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("clear dataflag has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", DataFlag);
        #endif //_DEBUG_YDN_CMD_HANDLER
	}
	return NOR;
}


/*==========================================================================*
 * FUNCTION : *GetYDNAlarmLevelText
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  *piACUAlarmLevel : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2005-01-19 14:57
 *==========================================================================*/
__INLINE static char *GetYDNAlarmLevelText(int *piACUAlarmLevel)
{
	if (piACUAlarmLevel != NULL)
	{
		switch (*piACUAlarmLevel)
		{
		case ALARM_LEVEL_CRITICAL:
			return "A1";		/* A1 */

		case ALARM_LEVEL_MAJOR:
			return "A2";		/* A2 */

		case ALARM_LEVEL_OBSERVATION:
			return "O1";		/* O1 */

		default:  //error
			LOG_YDN_CMD_E("New ACU alarm level used, which can not be handled");
		}
	}

	return "Undefined";

}



/*==========================================================================*
 * FUNCTION : ReadADTexts
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int             iSequenceID : 
 *            OUT unsigned char  *szOutData  : 
 * RETURN   :  void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2005-01-19 14:55
 *==========================================================================*/
static void ReadADTexts(IN int iSequenceID, 
						OUT unsigned char *szOutData)
{
	int i, iPos, iRefTexts;
	YDN_REF_TEXT *pRefText;
	int iOffSet;
	time_t tmTime;
	char szTime[13];
	
	/* reply format: "RX00 #00!text1!text2 #01!text1!text2...#<timestamp>*" */
    iPos = 10 * iSequenceID;
	iRefTexts = g_YDNGlobals.iRefTexts;
	pRefText = g_YDNGlobals.RefTexts + iPos;

	/* 1.for head, eg: RX01 */
	sprintf(szOutData, "RX%02X", iSequenceID);
	iOffSet = 4;

	/* 2.read texts one by one */
	for (i = 0; i < 10 && iPos < iRefTexts; i++, iPos++, pRefText++)
	{
		/* alarm level text handle */
		if (pRefText->iRefNo == 17 || pRefText->iRefNo == 18)
		{
			sprintf(szOutData + iOffSet, "#%02X!%s!", 
				iPos, GetYDNAlarmLevelText(pRefText->piAlarmLevel));
		}
		else
		{
			sprintf(szOutData + iOffSet, "#%02X!%s!%s", 
				iPos, pRefText->szText1, pRefText->szText2);
		}

		iOffSet = strlen(szOutData);
	}

	/* 3.add <Time stamp> */
	if (i != 0)
	{
		tmTime = time(&tmTime);
		sprintf(szOutData + iOffSet, "#%s*",
			TimeToString(tmTime, YDN_TIME_FORMAT, szTime, sizeof (szTime)));
	}
	else
	{
		sprintf(szOutData + iOffSet, "*");
	}


	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteCRE
 * PURPOSE  : Remonte control rectifiers
 * CALLS    : ReadRefTexts
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-27 13:38
 *==========================================================================*/
static int ExecuteCRE(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	BOOL bRTN;

	int  iEquipIDOrder, iEquipID, iRectID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);
	//UNUSED(szOutData);

	/* 1.check validity */
	if (strncmp(szInData, "4145", 4) != 0)
	{
		return CMDERR;
	}
    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
		if(iCMDType == pCurEntry->iYDNPort)
		{
			break;
		}
		pCurEntry++;
	}
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CTLFAIL;
	}
	
	
    if(pCurEntry->iEquipType == RECT_STDEQUIP_ID)//find out rectifier NO.
    {
        iRectID = (int)YDN_AsciiHexToChar(szInData + 6);
        if((iRectID > 0) && (iRectID < (MAX_RECT_ID + 1)))
    	   iEquipIDOrder = iEquipIDOrder + (iRectID - 1);
        else
    	   return CMDERR;
    }
	
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

    if(iCMDType == BC_CTRL || iCMDType == START_TEST_CTRL
	   || iCMDType == RECT_DCOFF_CTRL
       || iCMDType == RECT_ACOFF_CTRL
	   || iCMDType == RECT_RESET_CTRL)
    {
        sprintf(szInData + 4, "%02X", 0x01);
    }
    else if(iCMDType == FC_CTRL || iCMDType == STOP_TEST_CTRL
      || iCMDType == RECT_DCON_CTRL
      || iCMDType == RECT_ACON_CTRL)
      //|| iCMDType == RECT_RESET_CTRL)
    {
        sprintf(szInData + 4, "%02X", 0x00);
    }
    else
    {
        return CMDERR;
    }
    *(szInData + 6) = '\0';
	bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, szInData + 4);

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;

	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("CRE has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", szOutData);
        #endif //_DEBUG_YDN_CMD_HANDLER
       
		return NOR;
	}
	else
		return CTLFAIL;
}

/*==========================================================================*
 * FUNCTION : IfValidCRect
 * PURPOSE  : judge if the value is valid
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2007-02-28 15:37
 *==========================================================================*/
int IfValidCRect(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
	ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	//BOOL bRTN;

	int  iEquipIDOrder, iEquipID, iRectID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);
	//UNUSED(szOutData);

	/* 1.check validity */
	if (strncmp(szInData, "4145", 4) != 0)
	{
		return CMDERR;
	}
    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
		if(iCMDType == pCurEntry->iYDNPort)
		{
			break;
		}
		pCurEntry++;
	}
	
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CMDERR;
	}
	
    if(pCurEntry->iEquipType == RECT_STDEQUIP_ID)//find out rectifier NO.
    {
        iRectID = (int)YDN_AsciiHexToChar(szInData + 6);
        if((iRectID > 0) && (iRectID < (MAX_RECT_ID + 1)))
    	   iEquipIDOrder = iEquipIDOrder + (iRectID - 1);
        else
    	   return CMDERR;
    }
	
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

    if(iCMDType == BC_CTRL || iCMDType == START_TEST_CTRL
	   || iCMDType == RECT_DCOFF_CTRL
       || iCMDType == RECT_ACOFF_CTRL
	   || iCMDType == RECT_RESET_CTRL)
    {
        sprintf(szInData + 4, "%02X", 0x01);
    }
    else if(iCMDType == FC_CTRL || iCMDType == STOP_TEST_CTRL
      || iCMDType == RECT_DCON_CTRL
      || iCMDType == RECT_ACON_CTRL)
      //|| iCMDType == RECT_RESET_CTRL)
    {
        sprintf(szInData + 4, "%02X", 0x00);
    }
    else
    {
        return CMDERR;
    }

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;


	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteREID
 * PURPOSE  : Read rectifiers ID
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 14:42
 *==========================================================================*/
static int ExecuteREID(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteREID!!!!\n");
        TRACE("\nExecuteREID:szInData is: %s\n", szInData);
	#endif
   
	int i, iDataLen, iPos = 0;
	int iCID1,iCID2, iData;
	unsigned char *pData;
	PRODUCT_INFO sProductInfo;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;

	int		iRectNumber = DXI_GetRectRealNumber();
	int		iRectStartDeviceID =  DXI_GetFirstRectDeviceID();

    //EQUIP_INFO* pEquipInfo;

	HANDLE hLocalThread;
	/* not used the param */
	UNUSED(pThis);

	hLocalThread = RunThread_GetId(NULL);

	/* 1.check validity */
	if (strncmp(szInData, "41E1", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read rectifiers ID");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

     // 2.Package the DataFlag
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }

    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,DATAFLAG,0);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
	
    iData = (int)YDN_AsciiHexToChar(szOutData);//Rectifier Num
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    
	iDataLen *= 2;

    if(iData > RECT_MAX_NUM)
    	return CMDERR;

    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
   /*
	for(i = 0; i < iData; i++, iEquipIDOrder++)
	 {
		 iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
		 iEquipID -= 1;
		 TRACE("\niEquipID is %d\n", iEquipID);
	*/
	int iRectDeviceId;

	if(iRectNumber > RECT_ID_SECOND_SECT)
	{
	    for(iRectDeviceId = 1; iRectDeviceId <= g_SiteInfo.iDeviceNum; iRectDeviceId++)
	    {
		    if (DXI_IsDeviceRelativeEquip(iRectDeviceId, RECT_NUM101_ID))
		    {
			    break;
		    }
	    }
	}

	 for(i = 0; i <iRectNumber ; i++)
	 {
		 if(i % 20 == 0)
			 RunThread_Heartbeat(hLocalThread);

		 if(i>=RECT_ID_SECOND_SECT)
		 {
			if(iRectDeviceId>g_SiteInfo.iDeviceNum)
			{
				strncpyz(sProductInfo.szSerialNumber,g_SiteInfo.stCANRectProductInfo[i].szSerialNumber,
				 sizeof(sProductInfo.szSerialNumber));
			}
			else
			{	
				 if (DXI_GetPIByDeviceID(iRectDeviceId+i-RECT_ID_SECOND_SECT , &sProductInfo) != ERR_DXI_OK)
				 {
					 LOG_YDN_CMD_E("Call DXI interface to get Product "
						 "Info failed");
				 }

			}
		 }
		 else
		 {
			 if (DXI_GetPIByDeviceID(iRectStartDeviceID+i, &sProductInfo) != ERR_DXI_OK)
			 {
				 //simply log it
				 LOG_YDN_CMD_E("Call DXI interface to get Product "
					 "Info failed");
			 }
		 }
		 //serial number
		 //sprintf(szOutData, "%s", sProductInfo.szSerialNumber);
		 memcpy(szOutData,sProductInfo.szSerialNumber, strlen(sProductInfo.szSerialNumber));
		 iPos = strlen(sProductInfo.szSerialNumber); 
		 szOutData += iPos;
		 iDataLen += iPos;
		// TRACE("\nszSerialNumber is %s\n", sProductInfo.szSerialNumber);
	 }
	 //TRACE("\niDataLen is %d\n", iDataLen);

    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
    //szOutData = YDN_BuildDataInfo(iDataLen, szOutData);
	WORD wLength, Sum;
 
    wLength = iDataLen;
	Sum = (-(wLength + (wLength>>4) + (wLength>>8) )) << 12;
	wLength = wLength & 0xFFF | Sum;
	//sprintf(szOutData, "%04X%s", wLength, szOutData + 4);
	HexToFourAsciiData(wLength, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("REID has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteRREBarCode
 * PURPOSE  : Read rectifiers BarCode
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-08-03 14:42
 *==========================================================================*/
static int ExecuteRREBarCode(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRREBarCode!!!!\n");
        TRACE("\nExecuteRREBarCode:szInData is: %s\n", szInData);
	#endif
   
	int i,iDataLen;
	int iCID1,iCID2, iData;
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];
	

	HANDLE hLocalThread;

	int  iEquipIDOrder, iEquipID, iPos = 0;
	YDN_MAPENTRIES_INFO	 *pCurEntry;
	YDN_TYPE_MAP_INFO *pTypeMap;
	PRODUCT_INFO sProductInfo;
	
	int		iRectNumber = DXI_GetRectRealNumber();
	int		iRectStartDeviceID =  DXI_GetFirstRectDeviceID();

	/* not used the param */
	UNUSED(pThis);

	hLocalThread = RunThread_GetId(NULL);

	/* 1.check validity */
	if (strncmp(szInData, "41E3", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get the block */

	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	//iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	iCID2 = 0xE1;
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read rectifiers ID");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

     // 2.Package the DataFlag
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,DATAFLAG,0);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
	
    iData = (int)YDN_AsciiHexToChar(szOutData);//Rectifier Num
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    
    if(iData > RECT_MAX_NUM)
    	return CMDERR;

	iDataLen *= 2;

    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;

	int iRectDeviceID;

	if(iRectNumber > RECT_ID_SECOND_SECT)
	{
		for(iRectDeviceID = 1;iRectDeviceID <= g_SiteInfo.iDeviceNum; iRectDeviceID++)
		{
			if (DXI_IsDeviceRelativeEquip(iRectDeviceID, RECT_NUM101_ID))
			{
				break;
			}
		}
	}
	for(i = 0; i < iRectNumber ; i++)
	 {
		 if(i % 20 == 0)
			 RunThread_Heartbeat(hLocalThread);
		 if(i>=RECT_ID_SECOND_SECT)
		 {
			 if(iRectDeviceID> g_SiteInfo.iDeviceNum)
			 {
				strncpyz(sProductInfo.szSerialNumber,g_SiteInfo.stCANRectProductInfo[i].szSerialNumber,
					sizeof(sProductInfo.szSerialNumber));
				strncpyz(sProductInfo.szPartNumber, g_SiteInfo.stCANRectProductInfo[i].szPartNumber,
					sizeof(sProductInfo.szPartNumber));
				strncpyz(sProductInfo.szHWVersion, g_SiteInfo.stCANRectProductInfo[i].szHWVersion,
					sizeof(sProductInfo.szHWVersion));
			 }
			 else
			 {	
				 if (DXI_GetPIByDeviceID(iRectDeviceID+i-RECT_ID_SECOND_SECT , &sProductInfo) != ERR_DXI_OK)
				 {
					 //simply log it
					 LOG_YDN_CMD_E("Call DXI interface to get Product "
						 "Info failed");
				 }

			 }
		 }
		 else
		 {
			 if (DXI_GetPIByDeviceID(iRectStartDeviceID+i , &sProductInfo) != ERR_DXI_OK)
			{
				 //simply log it
				LOG_YDN_CMD_E("Call DXI interface to get Product "
				"Info failed");
			}
		 }

		//serial number
		//sprintf(szOutData, "%s", sProductInfo.szSerialNumber);
		memcpy(szOutData,sProductInfo.szSerialNumber, strlen(sProductInfo.szSerialNumber));
		iPos = strlen(sProductInfo.szSerialNumber); 
		szOutData += iPos;
		iDataLen += iPos;
		
		memcpy(szOutData,sProductInfo.szPartNumber, strlen(sProductInfo.szPartNumber));
		iPos = strlen(sProductInfo.szPartNumber); 
		szOutData += iPos;
		iDataLen += iPos;  

		//HW version
		memcpy(szOutData,sProductInfo.szHWVersion, strlen(sProductInfo.szHWVersion));
		iPos = strlen(sProductInfo.szHWVersion);
		szOutData += iPos;
		iDataLen += iPos;
		

		
	 }
	
    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
	WORD wLength, Sum;
 
	wLength = iDataLen;
	Sum = (-(wLength + (wLength>>4) + (wLength>>8) )) << 12;
	wLength = wLength & 0xFFF | Sum;
	HexToFourAsciiData(wLength, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RREBarCode has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteRDCA
 * PURPOSE  : Read DC Analog signals
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : length of szOutData is YDN_RESP_SIZE
 * CREATOR  : HanTao                   DATE: 2006-05-29 19:00
 *==========================================================================*/
static int ExecuteRDCA(YDN_BASIC_ARGS *pThis,
				   unsigned char *szInData,
				   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

	UNUSED(pThis);

	int iDataLen = 0;

	iDataLen = strlen(g_RespData.szRespData1[3]);

#ifdef	_DEBUG_YDN
	if((iDataLen) > MAX_YDNFRAME_LEN)
	{
		AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRDCA iDataLen greater than 4114.\n");
	
	}
#endif

	if(iDataLen == 0)
		return CMDERR;
	memcpy(szOutData, g_RespData.szRespData1[3],iDataLen);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRDCA!!!!\n");
        TRACE("\nExecuteRDCA:szInData is: %s\n", szInData);
	#endif
    
	int iCID1,iCID2;
	//unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;//, *pFixedPos, *pFixedBattPos;
    YDN_TYPE_MAP_INFO *pTypeMap;
	unsigned char DataFlag[2] = {'1', '0'};
	BOOL bRTN = FALSE;
	/* not used the param */
	
	UNUSED(pThis);
    
	/* 1.check validity */
	if (strncmp(szInData, "4241", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	//iCommGrp = (int)YDN_AsciiHexToChar(szInData + 4);
	//if((iCommGrp > DCD_MAX_NUM && iCommGrp < DCD_MIN_NUM) && iCommGrp != ALL_AC_OR_DC)
		//return CMDERR;
	
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		//strcpy(szOutData, CMDERR);
		return CMDERR;
	}

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    // 2.Package the DataFlag
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
    
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, DataFlag);
	//TRACE("iEquipID is %d\n", iEquipID);
	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       //TRACE_YDN_TIPS("clear dataflag has been excuted ok!");
		   TRACE("clear dataflag has been excuted ok!\n");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", DataFlag);
        #endif //_DEBUG_YDN_CMD_HANDLER
	}
	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteRDCAL
 * PURPOSE  : Read DC Alarms Signals
 * CALLS    : 
 *			  
 *			  
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 16:19
 *==========================================================================*/
static int ExecuteRDCAL(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRDCAL!!!!\n");
        TRACE("\nExecuteRDCAL:szInData is: %s\n", szInData);
	#endif

	UNUSED(pThis);

	int iDataLen = 0;

	iDataLen = strlen(g_RespData.szRespData1[4]);

#ifdef	_DEBUG_YDN
	if((iDataLen) > MAX_YDNFRAME_LEN)
	{
		AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: ExecuteRDCAL iDataLen greater than 4114.\n");
	
	}
#endif
	if(iDataLen == 0)
		return CMDERR;
	memcpy(szOutData, g_RespData.szRespData1[4],iDataLen);
    
//	int i, j, k, iDataLen;
//	int iCID1,iCID2, iData[2];
//	unsigned char *pData;
//	unsigned char DataFlag[2] = {'0', '0'};
//	char logText[YDN_LOG_TEXT_LEN];
//    BOOL bRTN;
//
//	int  iEquipIDOrder[2], iEquipID[2];
//    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos, *pFixedBattPos;
//    YDN_TYPE_MAP_INFO *pTypeMap;
//	/* not used the param */
//	HANDLE hLocalThread;
//	
//	UNUSED(pThis);
//
//    hLocalThread = RunThread_GetId(NULL);
//
//
//	/* 1.check validity */
//	if (strncmp(szInData, "4244", 4) != 0)
//	{
//		return CMDERR;
//	}
//
//    
//	/* 2.get the block */
//	iCID1 = (int)YDN_AsciiHexToChar(szInData);
//	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
//	//iCommGrp = (int)YDN_AsciiHexToChar(szInData + 4);
//	//if((iCommGrp > DCD_MAX_NUM && iCommGrp < DCD_MIN_NUM) && iCommGrp != ALL_AC_OR_DC)
//		//return CMDERR;
//	
//	pTypeMap = GetTypeMap(iCID1, iCID2);
//
//	if (pTypeMap == NULL)
//	{
//		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
//			iCID1, iCID2);
//		LOG_YDN_CMD_I(logText);
//
//		return CMDERR;
//	}
//
//    #ifdef _DEBUG_YDN_CMD_HANDLER
//	    TRACE_YDN_TIPS("Will read data from a Block");
//	    TRACE("the Block Info\n");
//	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
//	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
//    #endif //_DEBUG_YDN_CMD_HANDLER
//
//    iDataLen = 0;
//    pData = szOutData;
//    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
//    	*szOutData = '0';
//    pCurEntry = pTypeMap->pMapEntriesInfo;
//    iEquipIDOrder[0] = 0;
//    iEquipIDOrder[1] = 0;
//    // 2.Package the DataFlag
//    iEquipIDOrder[0] = GetEquipIDOrder(pCurEntry->iEquipType);
//     if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
//    	iEquipIDOrder[0] = 0;
//     
//	iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];
//    GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData);
//    
//    bRTN = SetYDNSigValues(FALSE, iEquipID[0], 1, pCurEntry, DataFlag);
//	TRACE("\nbRTN is: %d", bRTN);
//	if(bRTN)
//	{
//        #ifdef _DEBUG_YDN_CMD_HANDLER
//	       TRACE_YDN_TIPS("clear dataflag has been excuted ok!");
//	       TRACE("Reply Data:\n");
//	       TRACE("\t%s\n", DataFlag);
//        #endif //_DEBUG_YDN_CMD_HANDLER
//	}
//	
//    pCurEntry++;
//    iDataLen++;
//    szOutData += 2;
//    // 3.Package data
//    iEquipIDOrder[0] = GetEquipIDOrder(pCurEntry->iEquipType);
//     if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
//    	iEquipIDOrder[0] = 0;
//	iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];
//    GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData);
//    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//DCDNum
//    pCurEntry++;
//    iDataLen++;
//    szOutData += 2;
//
//	iEquipIDOrder[0] = GetEquipIDOrder(pCurEntry->iEquipType);//find out the first place of the equiptype
//    if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
//    	iEquipIDOrder[0] = 0;
//    
//	pFixedPos = pCurEntry;
///*
//    if(iCommGrp != ALL_AC_OR_DC)
//    {
//        iData[0] = 1;
//        iEquipIDOrder[0] = iEquipIDOrder[0] + iCommGrp - 1;//pack some DCD data;
//    }
//*/
//    for(i = 0; i < iData[0]; i++, iEquipIDOrder[0]++)
//    {
//       RunThread_Heartbeat(hLocalThread);
//       
//	   iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];//find out the equipID orderly
//
//       GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData);//DCVoltAlarm
//       ExchangeYDNSigValue(szOutData, pCurEntry);
//       szOutData += 2;
//       iDataLen++;
//       pCurEntry++;
//
//       GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData);
//       iData[1] = (int)YDN_AsciiHexToChar(szOutData);//DCFuseNum
//	   TRACE("\nDCFuseNum is: %d", iData[1]);
//	   pCurEntry++;
//       iDataLen++;
//       szOutData += 2;
//       for(j=0; j< iData[1]; j++)
//       {
//            GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData);
//            ExchangeYDNSigValue(szOutData, pCurEntry);
//            szOutData += 2;
//            iDataLen++;
//			pCurEntry++;
//       }
//
//	   pCurEntry = pCurEntry + (DC_MAX_FUSE_NUM - iData[1]);
//
//       iData[1] = pCurEntry->iFlag1;//Defined Num
//       //sprintf(szOutData, "%02X", iData[1]);
//       HexToTwoAsciiData(iData[1], szOutData);
//	   pCurEntry++;
//       iDataLen++;
//       szOutData += 2;
//
//       iData[1] = pCurEntry->iFlag1;
//       for(j = 0; j < iData[1]; j++)
//       {
//          GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData);
//          ExchangeYDNSigValue(szOutData, pCurEntry);
//          szOutData += 2;
//          iDataLen++;
//          pCurEntry++;
//       }
//
//       iEquipIDOrder[1] = GetEquipIDOrder(pCurEntry->iEquipType);
//       if(iEquipIDOrder[1] > MAX_EQUIP_NUM - 1)
//    	    iEquipIDOrder[1] = 0;
//       pFixedBattPos = pCurEntry;
//	   for(k = 0; k < DCD_BATT_MAX_NUM; k++)
//       {
//          iEquipID[1] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[1] + i * 2][0];
//          for(j = 0; j < pFixedBattPos->iFlag1; j++)
//          {
//                GetSCUPSigValues(FALSE, pCurEntry, iEquipID[1], szOutData);
//                ExchangeYDNSigValue(szOutData, pCurEntry);
//                pCurEntry++;
//                szOutData += 2;
//                iDataLen++;
//          }
//          if(k < DCD_BATT_MAX_NUM -1)
//          {
//                iEquipIDOrder[1]++;
//                pCurEntry = pFixedBattPos;
//          }
//       }
//
//	   iEquipIDOrder[1] = GetEquipIDOrder(pCurEntry->iEquipType);
//       if(iEquipIDOrder[1] > MAX_EQUIP_NUM - 1)
//    	    iEquipIDOrder[1] = 0;
//       iEquipID[1] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[1]][0];
//       pFixedBattPos = pCurEntry;
//       for(k = 0; k < pFixedBattPos->iFlag1; k++)
//       {
//           GetSCUPSigValues(FALSE, pCurEntry, iEquipID[1], szOutData);
//           ExchangeYDNSigValue(szOutData, pCurEntry);
//           pCurEntry++;
//           szOutData += 2;
//           iDataLen++;
//       }
//	   /*
//	   for(k = 0; k < DCD_BATT_MAX_NUM - iBattNum; k++)//
//	   {
//	   	   for(j = 0; j < (pFixedBattPos->iFlag1 * 8); j++)
//	   	   {
//           	  *szOutData = 0x20;
//           	  szOutData++;
//	   	   }
//	   }
//       */
//       pCurEntry = pFixedPos;
//    }
//    // 4.calculate LCHKSUM and package it 
//	szOutData = pData;
//    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);
//
//    #ifdef _DEBUG_YDN_CMD_HANDLER
//	   TRACE_YDN_TIPS("RDCAL has been excuted ok!");
//	   TRACE("Reply Data:\n");
//	   TRACE("\t%s\n", szOutData);
//    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteRDCP
 * PURPOSE  : Read DC Parameters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 14:42
 *==========================================================================*/
static int ExecuteRDCP(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRDCP!!!!\n");
        TRACE("\nExecuteRDCP:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen;
	int iCID1,iCID2, iData;
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "4246", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

    // 2.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
	TRACE("pCurEntry->iEquipType is %d\n", pCurEntry->iEquipType);

    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	iData = pCurEntry->iFlag1;//fixed Num

    for(i=0; i< iData; i++, pCurEntry++)
    {
	    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	    {
		    return CMDERR;
	    }
	    iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
        GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
        szOutData += 8;
        iDataLen += 4;
	}
    iData = pCurEntry->iFlag1;//Defined Num
    //sprintf(szOutData, "%02X", iData);
	HexToTwoAsciiData(iData, szOutData);
    pCurEntry++;
    iDataLen++;
	szOutData += 2;
	
	pFixedPos = pCurEntry;
    for(i = 0; i < pFixedPos->iFlag1; i++)
    {
       if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
       {
	       return CMDERR;
       }
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
       GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
       szOutData += 8;
       iDataLen += 4;
       pCurEntry++;
	}

    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	pFixedPos = pCurEntry;
    for(i = 0; i < pFixedPos->iFlag1; i++)
    {
       if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
       {
	       return CMDERR;
       }
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
       GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
       szOutData += 8;
       iDataLen += 4;
       pCurEntry++;
	}
    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
    
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RACA has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteWDCP
 * PURPOSE  : Write DC parameters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 15:37
 *==========================================================================*/
static int ExecuteWDCP(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteWDCP!!!!\n");
        TRACE("\nExecuteWDCP:szInData is: %s\n", szInData);
	#endif
    
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	BOOL bRTN;

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "4248", 4) != 0)
	{
		return CMDERR;
	}
 
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will write data to DC");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
    
	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
		if(iCMDType == pCurEntry->iYDNPort)
		{
			break;
		}
		pCurEntry++;
	}
	if(i == pTypeMap->iMapEntriesNum)
		return CMDERR;
	
	
	
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CTLFAIL;
	}
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	bRTN = SetYDNSigValues(TRUE, iEquipID, 1, pCurEntry, szInData + 6);
	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("WDCP has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", szOutData);
        #endif //_DEBUG_YDN_CMD_HANDLER
       
		return NOR;
	}
	else
		return CTLFAIL;
}


/*==========================================================================*
 * FUNCTION : IfValidWDCP
 * PURPOSE  : judge if the value is valid
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2007-02-28 15:37
 *==========================================================================*/
int IfValidWDCP(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
	 ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteWDCP!!!!\n");
        TRACE("\nExecuteWDCP:szInData is: %s\n", szInData);
	#endif
    
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	union YDN_FVALUE YDN_uf;
	union YDN_FVALUE tmpYNDFValue;
	//BOOL bRTN;

	//int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "4248", 4) != 0)
	{
		return CMDERR;
	}
 
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will write data to DC");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);
	TRACE("szInData = %s\n", szInData);
	for(i = 0; i < 4; i++)
	{
		tmpYNDFValue.cValue[i] = (int)YDN_AsciiHexToChar(szInData + 6 + i * 2);
	}

	for (i = 0; i < 4; i++)
	{
	    YDN_uf.cValue[i] = tmpYNDFValue.cValue[i];
	}
    
	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
		if(iCMDType == pCurEntry->iYDNPort)
		{
			break;
		}
		pCurEntry++;
	}
	if(i == pTypeMap->iMapEntriesNum)
		return CMDERR;

	TRACE("pCurEntry->iYDNPort is %d\n", pCurEntry->iYDNPort);
	TRACE("pCurEntry->iLen is %d  %d\n", pCurEntry->iLen, pCurEntry->iEquipType);
	TRACE("%c%c%c%c\n",tmpYNDFValue.cValue[0], tmpYNDFValue.cValue[1], tmpYNDFValue.cValue[2], tmpYNDFValue.cValue[3]);
	TRACE("YDN_uf.fValue is %f\n", YDN_uf.fValue);
	TRACE("pCurEntry->fPeakValue[0] is %f\n", pCurEntry->fPeakValue[0]);
	TRACE("pCurEntry->fPeakValue[1] is %f\n", pCurEntry->fPeakValue[1]);


	/*
	if(pCurEntry->iYDNPort == 238)
	{
		if(YDN_uf.fValue < 0.10 && YDN_uf.fValue > 0.25)
		return CMDERR;
	}
	else if(pCurEntry->iYDNPort == 242)
	{
		if(YDN_uf.fValue < 43.2 && YDN_uf.fValue > 50.2)
		return CMDERR;
	}
	else if(pCurEntry->iYDNPort == 246)
	{
		if(YDN_uf.fValue < 0.005 && YDN_uf.fValue > 0.05)
		return CMDERR;
	}
	else if(pCurEntry->iYDNPort == 248)
	{
		if(YDN_uf.fValue < 0.05 && YDN_uf.fValue > 0.08)
		return CMDERR;
	}
	*/
	if(YDN_uf.fValue < pCurEntry->fPeakValue[0] || YDN_uf.fValue > pCurEntry->fPeakValue[1])
		return CMDERR;

	pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;

	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteRSYSA
 * PURPOSE  : Read system analog signals
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 14:42
 *==========================================================================*/
static int ExecuteRSYSA(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRSYSA!!!!\n");
        TRACE("\nExecuteRSYSA:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen;
	int iCID1,iCID2, iData;
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];
	unsigned char DataFlag[2] = {'1', '0'};
	BOOL bRTN = FALSE;

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E141", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from system");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

    // 2.Package the DataFlag
	//HexToTwoAsciiData(0x11, szOutData);

    // 2.Package the DataFlag
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,DATAFLAG,0);

	bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, DataFlag);
	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("clear dataflag has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", DataFlag);
        #endif //_DEBUG_YDN_CMD_HANDLER
	}

    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
     
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	iData = pCurEntry->iFlag1;//fixed Num

    for(i=0; i< iData; i++, pCurEntry++)
    {
        GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
        szOutData += 8;
        iDataLen += 4;
	}
    iData = pCurEntry->iFlag1;//Defined Num
    //sprintf(szOutData, "%02X", iData);
	HexToTwoAsciiData(iData, szOutData);
    pCurEntry++;
    iDataLen++;
	szOutData += 2;
    for(i = 0; i < iData; i++, pCurEntry++)
    {
	   if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	   {
		   return CMDERR;
	   }
       if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	   iEquipIDOrder = 0;
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

       GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
       szOutData += 8;
       iDataLen += 4;
	}

    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RSYSA has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteRSYSS
 * PURPOSE  : Read system status signals
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 14:42
 *==========================================================================*/
static int ExecuteRSYSS(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRSYSS!!!!\n");
        TRACE("\nExecuteRSYSS:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen;
	int iCID1,iCID2, iData, iBattState;
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E143", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

    // 2.Package the DataFlag
    //sprintf(szOutData, "%02X", 0x10);
	HexToTwoAsciiData(0x10, szOutData);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    /*iEquipIDOrder = GetEquipIDOrder(pCurEntry->iEquipType);
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
     
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];*/

	iData = pCurEntry->iFlag1;//fixed Num

    for(i=0; i< iData; i++, pCurEntry++)
    {
		
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CMDERR;
	}
        if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	    iEquipIDOrder = 0;
     
	    iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];


        GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
		 ExchangeYDNSigValue(szOutData, pCurEntry);

        if(pCurEntry->iFlag1 == 99)
        {
        	iBattState = (int)YDN_AsciiHexToChar(szOutData);
        	//TRACE("iBattState is: %d\n",iBattState);
        	if(iBattState ==1 || iBattState ==3 || iBattState == 4 || iBattState == 11)
        	{
        	 	iBattState = 2;
        	 	HexToTwoAsciiData(iBattState, szOutData);
        	}
        	else if(iBattState == 5 || iBattState == 6)
        	{
        		iBattState = 3;
        	 	HexToTwoAsciiData(iBattState, szOutData);
        	}
        	else if((iBattState >=7 && iBattState <= 10) || iBattState == 2)
        	{
        		iBattState = 1;
        	 	HexToTwoAsciiData(iBattState, szOutData);
        	}
        	else
        	{
        		iBattState = 0;
        	 	HexToTwoAsciiData(iBattState, szOutData);
        	}
        }
        	 
        szOutData += 2;
        iDataLen++;
	}
    iData = pCurEntry->iFlag1;//Defined Num
    //sprintf(szOutData, "%02X", iData);
	HexToTwoAsciiData(iData, szOutData);
    pCurEntry++;
    iDataLen++;
	szOutData += 2;
    for(i = 0; i < iData; i++, pCurEntry++)
    {
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CMDERR;
	}
        if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	    iEquipIDOrder = 0;
     
	    iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

        GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
		 ExchangeYDNSigValue(szOutData, pCurEntry);

        szOutData += 2;
        iDataLen++;
	}

    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RSYSS has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteRSYSAL
 * PURPOSE  : Read system alarms signals
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 14:42
 *==========================================================================*/
static int ExecuteRSYSAL(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRSYSAL!!!!\n");
        TRACE("\nExecuteRSYSAL:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen;
	int iCID1,iCID2, iData;
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E144", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from system");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

    // 2.Package the DataFlag
    //sprintf(szOutData, "%02X", 0x01);
	HexToTwoAsciiData(0x01, szOutData);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
     
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	iData = pCurEntry->iFlag1;//fixed Num

    for(i=0; i< iData; i++, pCurEntry++)
    {
        GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
		 ExchangeYDNSigValue(szOutData, pCurEntry);
        szOutData += 2;
        iDataLen++;
	}
    iData = pCurEntry->iFlag1;//Defined Num
    //sprintf(szOutData, "%02X", iData);
	HexToTwoAsciiData(iData, szOutData);
    pCurEntry++;
    iDataLen++;
	szOutData += 2;
    for(i = 0; i < iData; i++, pCurEntry++)
    {
	   
	   if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	   {
		   *szOutData = 0x20;
		   *(szOutData+1) = 0x20;
		   szOutData += 2;
		   iDataLen++;
		   continue;
	   }
       if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	  iEquipIDOrder = 0;
     
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
	    ExchangeYDNSigValue(szOutData, pCurEntry);
	    szOutData += 2;
            iDataLen++;
	}

    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RSYSAL has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteRSYSP
 * PURPOSE  : Read system parameters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 14:42
 *==========================================================================*/
static int ExecuteRSYSP(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRSYSP!!!!\n");
        TRACE("\nExecuteRSYSP:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen;
	int iCID1,iCID2, iData;
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E146", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from system");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

    // 2.Package data
   
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
    
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	iData = pCurEntry->iFlag1;//fixed Num

    for(i=0; i< iData; i++, pCurEntry++)
    {
        GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
        szOutData += 8;
        iDataLen += 4;
	}
    iData = pCurEntry->iFlag1;//Defined Num
    //sprintf(szOutData, "%02X", iData);
	HexToTwoAsciiData(iData, szOutData);
    pCurEntry++;
    iDataLen++;
	szOutData += 2;
    for(i = 0; i < iData; i++, pCurEntry++)
    {
       GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
       szOutData += 8;
        iDataLen += 4;
	}

    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RSYSP has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteWSYSP
 * PURPOSE  : write system paremeters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 14:42
 *==========================================================================*/
static int ExecuteWSYSP(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteWSYSP!!!!\n");
        TRACE("\nExecuteWSYSP:szInData is: %s\n", szInData);
	#endif
    
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	BOOL bRTN;
	unsigned char *pData;

	int iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E148", 4) != 0)
	{
		TRACE("\nErr1!!!\n");
		return CMDERR;
	}
   
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		TRACE("\nErr2!!!\n");
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will write data to DC");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
    
	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
		if(iCMDType == pCurEntry->iYDNPort)
		{
			//pCurEntry = pTypeMap->pMapEntriesInfo;
			break;
		}
		pCurEntry++;
	}
	if(i == pTypeMap->iMapEntriesNum)
	{
		TRACE("\nErr3!!!\n");
		//TRACE("\npCurEntry->iYDNPort is: %d\n",pCurEntry->iYDNPort);
		//TRACE("\niCMDType is: %d\n",iCMDType);
		//TRACE("\npTypeMap->iMapEntriesNum is: %d\n",pTypeMap->iMapEntriesNum);
		return CMDERR;
	}
	
	
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CTLFAIL;
	}
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    
	bRTN = SetYDNSigValues(TRUE, iEquipID, 1, pCurEntry, szInData + 6);

	TRACE("\nbRTN is: %d\n", bRTN);
	TRACE("\nszInData is : %s\n", szInData);


	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("WSYSP has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", szOutData);
        #endif //_DEBUG_YDN_CMD_HANDLER
       
		return NOR;
	}
	else
		return CTLFAIL;
}


/*==========================================================================*
 * FUNCTION : ExecuteREXAL
 * PURPOSE  : Read rectifiers alarm signals
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 11:34
 *==========================================================================*/
static int ExecuteREXAL(YDN_BASIC_ARGS *pThis,
				      unsigned char *szInData,
				      unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteREXAL!!!!\n");
        TRACE("\nExecuteREXAL:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen;
	int iCID1, iCID2, iData;
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E1F7", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read extend alarms");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    // 2.Package the DataFlag
    
    // 3.Package data
	//sprintf(szOutData, "%02X", pCurEntry->iFlag1);//device number
	HexToTwoAsciiData(pCurEntry->iFlag1, szOutData);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;

	iData = pCurEntry->iFlag1;//Fixed Num
	for(i = 0; i < iData; i++, pCurEntry++)
	{
	   
	    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	    {
		    return CMDERR;
	    }
	    iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
        GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
        iDataLen++;
        szOutData += 2;
	}
	
    iData = pCurEntry->iFlag1;//Defined Num
    //sprintf(szOutData, "%02X", iData);
    HexToTwoAsciiData(iData, szOutData);
	pCurEntry++;
    iDataLen++;
	szOutData += 2;
    for(i = 0; i < iData; i++, pCurEntry++)
    {
        GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
        iDataLen++;
        szOutData += 2;
    }

    // 4.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("REXAL has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
	
}

/*==========================================================================*
 * FUNCTION : ExecuteRSysStatus
 * PURPOSE  : Read or set System status
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-27 10:53
 *==========================================================================*/
static int ExecuteRWSysStatus(YDN_BASIC_ARGS *pThis,
			          unsigned char *szInData,
			          unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRSysStatus!!!!\n");
        TRACE("\nExecuteRRES:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen, iCMDInfo;
	int iCID1,iCID2;
	unsigned char *pData, pCMDType[2];
	char logText[YDN_LOG_TEXT_LEN];
    BOOL bRTN;
    
	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if ((strncmp(szInData, "E181", 4) != 0) && (strncmp(szInData, "E180", 4) != 0))
	{
		return CMDERR;
	}

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
			*szOutData = '0';
	pCurEntry = pTypeMap->pMapEntriesInfo;
	iEquipIDOrder = 0;

	
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CMDERR;
	}
	if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
		iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
     
    if(iCID2 == 0x81)
    {
		iDataLen = 0;
		// 2.Package data
		GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
		ExchangeYDNSigValue(szOutData, pCurEntry);
		iDataLen++;

		// 3.calculate LCHKSUM and package it 
		szOutData = pData;
		szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

		#ifdef _DEBUG_YDN_CMD_HANDLER
			TRACE_YDN_TIPS("ExecuteRSysStatus has been excuted ok!");
			TRACE("Reply Data:\n");
			TRACE("\t%s\n", szOutData);
		#endif //_DEBUG_YDN_CMD_HANDLER

		return NOR;
    }
    else if(iCID2 == 0x80)
    {
        iCMDInfo = (int)YDN_AsciiHexToChar(szInData + 4);
        if(iCMDInfo == 0xE1)
        	//sprintf(pCMDType, "%02x", 0x01);
			HexToTwoAsciiData(0x01, pCMDType);
        else
        	//sprintf(pCMDType, "%02x", 0x00);
			HexToTwoAsciiData(0x00, pCMDType);

		bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, pCMDType);
		
		*szOutData = '\0';
		szOutData = pData;

		if(bRTN)
		{
			#ifdef _DEBUG_YDN_CMD_HANDLER
			TRACE_YDN_TIPS("ExecuteRSysStatus has been excuted ok!");
			TRACE("Reply Data:\n");
			TRACE("\t%s\n", szOutData);
			#endif //_DEBUG_YDN_CMD_HANDLER
	       
			return NOR;
		}
		else
			return CTLFAIL;
	}
}

/*==========================================================================*
 * FUNCTION : IfValidWSysStatus
 * PURPOSE  : judge if the value is valid
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2007-02-28 15:37
 *==========================================================================*/
int IfValidWSysStatus(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
	ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteWACP!!!!\n");
        TRACE("\nExecuteWACP:szInData is: %s\n", szInData);
	#endif
    
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	//BOOL bRTN;

	//int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E180", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will write data to AC");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

    if(iCMDType != 0xE1 && iCMDType != 0xE0)
    	return CMDERR;
    
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
    
    /*
	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
	    //TRACE("\npCurEntry->iYDNPort: %d\n", pCurEntry->iYDNPort);
		if(iCMDType == pCurEntry->iYDNPort)
		{
			//pCurEntry = pTypeMap->pMapEntriesInfo;
			break;
		}
		pCurEntry++;
	}
    if(i == pTypeMap->iMapEntriesNum)
    {
		return CMDERR;
    }
    */

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteRADR
 * PURPOSE  : RP, read SCU+ address
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-01 15:01
 *==========================================================================*/
static int ExecuteRADR(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
	ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRADR!!!!\n");
        TRACE("\nExecuteRT:szInData is: %s\n", szInData);
	#endif
    
	int iDataLen, i;
	unsigned char *pData;
    
	UNUSED(pThis);

	/* 1.check the command */
	if (strncmp(szInData + 2, "50", 2) != 0)
	{
		 return CMDERR;
	}

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++)//reserved for LENGTH
    {
    	*szOutData = '0';
    	 szOutData++;
    }
    *szOutData = '\0';
    
    szOutData = pData;
	szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("RADR has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return NOR;

}

/*==========================================================================*
 * FUNCTION : ExecuteRProtocolVer
 * PURPOSE  : RP, read protocol version
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-01 15:01
 *==========================================================================*/
	static int ExecuteRProtocolVer(YDN_BASIC_ARGS *pThis,
						unsigned char *szInData,
						unsigned char *szOutData)
{
	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	#ifdef _DEBUG_YDN_LINKLAYER
		TRACE("\nBegin ExecuteRProtocolVer!!!!\n");
		TRACE("\nExecuteRT:szInData is: %s\n", szInData);
	#endif

	int iDataLen, i;
	unsigned char *pData;

	UNUSED(pThis);

	/* 1.check the command */
	if (strncmp(szInData + 2, "4F", 2) != 0)
	{
		TRACE("\nERR1!!\n");
		return CMDERR;
	}

	iDataLen = 0;
	pData = szOutData;
	for(i = 0; i < 4; i++)//reserved for LENGTH
	{
		*szOutData = '0';
    		szOutData++;
	}
	*szOutData = '\0';
	szOutData = pData;
	szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

	#ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RProtocolVer has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
	#endif //_DEBUG_ESR_CMD_HANDLER

	return NOR;

}

/*==========================================================================*
 * FUNCTION : ExecuteRWPara
 * PURPOSE  : Read system parameters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 14:42
 *==========================================================================*/
 static int ExecuteRWPara(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRWPara!!!!\n");
        TRACE("\nExecuteRWPara:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen, iCID2,j;
	int iEquipID, iSigType, iSigID;
	unsigned char *pData;
    BOOL isFloat, bRet;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if ((strncmp(szInData, "E1EA", 4) != 0) && (strncmp(szInData, "E1EB", 4) != 0))
	{
		return CMDERR;
	}

    iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);

    isFloat  = (int)YDN_AsciiHexToChar(szInData + 4);
    iEquipID = (int)YDN_AsciiHexToChar(szInData + 6);
    iSigType = (int)YDN_AsciiHexToChar(szInData + 8);
    iSigID = (int)YDN_AsciiHexToChar(szInData + 10);

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    if(iCID2 == 0xEA)
    {
       iDataLen = (isFloat == 0) ? 1 : 4;
       bRet = GetSCUPSingleSigValues(isFloat, iSigType, iSigID, iEquipID, szOutData);
       if(bRet == FALSE)
       {
	       if(isFloat)
	       {
		       for(j= 0;j<8;j++)
		       {
				*(szOutData+j) = 0x20;
		       }
	       }
	       else
	       {
		       for(j= 0;j<2;j++)
		       {
			       *(szOutData+j) = 0x20;
		       }
	       }

       }
       // 3.calculate LCHKSUM and package it 
	   szOutData = pData;
       szOutData = YDN_BuildDataInfo(iDataLen, szOutData);
    }
    else if(iCID2 == 0xEB)
    {
       bRet = SetYDNSingleSigValues(isFloat, iSigType, iSigID, iEquipID, 1, szInData + 12);
       *szOutData = '\0';
       szOutData = pData;
    }
    
    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("RSYSP has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}


/*==========================================================================*
 * FUNCTION : ExecuteRSysStatus
 * PURPOSE  : Read or set System status
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-27 10:53
 *==========================================================================*/
static int ExecuteACErrLight(YDN_BASIC_ARGS *pThis,
			          unsigned char *szInData,
			          unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteACErrLight!!!!\n");
        TRACE("\nExecuteACErrLight:szInData is: %s\n", szInData);
	#endif
    
	int i, iCMDInfo;
	int iCID1,iCID2;
	unsigned char *pData, pCMDType[2];
	char logText[YDN_LOG_TEXT_LEN];
    BOOL bRTN;
    
	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "4080", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);

		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
			*szOutData = '0';
	pCurEntry = pTypeMap->pMapEntriesInfo;
	iEquipIDOrder = 0;

	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CTLFAIL;
	}
	if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
		iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
     
    iCMDInfo = (int)YDN_AsciiHexToChar(szInData + 4);
    if(iCMDInfo == 0xE1)
        sprintf(pCMDType, "%02X", 0x01);
    else
        sprintf(pCMDType, "%02X", 0x00);
        
	bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, pCMDType);
		
	*szOutData = '\0';
	szOutData = pData;

	if(bRTN)
	{
		#ifdef _DEBUG_YDN_CMD_HANDLER
		TRACE_YDN_TIPS("ExecuteACErrLight has been excuted ok!");
		TRACE("Reply Data:\n");
		TRACE("\t%s\n", szOutData);
		#endif //_DEBUG_YDN_CMD_HANDLER
	       
		return NOR;
	}
	else
		return CTLFAIL;
}

/*==========================================================================*
 * FUNCTION : ExecuteRSysStatus
 * PURPOSE  : Read or set System status
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-27 10:53
 *==========================================================================*/
static int ExecuteErrSound(YDN_BASIC_ARGS *pThis,
			          unsigned char *szInData,
			          unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteErrSound!!!!\n");
        TRACE("\nExecuteErrSound:szInData is: %s\n", szInData);
	#endif
    
	int i, iCMDInfo;
	int iCID1,iCID2;
	unsigned char *pData, pCMDType[2];
	char logText[YDN_LOG_TEXT_LEN];
    BOOL bRTN;
    
	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E184", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from a Block");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
			*szOutData = '0';
	pCurEntry = pTypeMap->pMapEntriesInfo;
	iEquipIDOrder = 0;

	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CTLFAIL;
	}
	if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
		iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
     
    iCMDInfo = (int)YDN_AsciiHexToChar(szInData + 4);
    /*
    if(iCMDInfo == 0xE1)
        sprintf(pCMDType, "%02x", 0x01);
    else
        sprintf(pCMDType, "%02x", 0x00);
    */
        
	bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, pCMDType);
		
	*szOutData = '\0';
	szOutData = pData;

	if(bRTN)
	{
		#ifdef _DEBUG_YDN_CMD_HANDLER
		TRACE_YDN_TIPS("ExecuteErrSound has been excuted ok!");
		TRACE("Reply Data:\n");
		TRACE("\t%s\n", szOutData);
		#endif //_DEBUG_YDN_CMD_HANDLER
	       
		return NOR;
	}
	else
		return CTLFAIL;
}


/*==========================================================================*
 * FUNCTION : ExecuteRRectRedund
 * PURPOSE  : Read parameters of rectifier redundancy
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2008-05-15 14:42
 *==========================================================================*/
 static int ExecuteRRectRedund(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRRectRedund!!!!\n");
        TRACE("\nExecuteRRectRedund:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen;
	int iCID1,iCID2, iData;
	unsigned char *pData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E190", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will read data from system");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;

    // 2.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
    {
	    return CMDERR;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
    
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	iData = pCurEntry->iFlag1;//byte Num

    for(i=0; i< iData; i++, pCurEntry++)
    {
        GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
        szOutData += 2;
        iDataLen++;
	}

    iData = pCurEntry->iFlag1;//float Num
   
    for(i = 0; i < iData; i++, pCurEntry++)
    {
       GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
       szOutData += 8;
        iDataLen += 4;
	}

    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("ExecuteRRectRedund has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteWRectRedund
 * PURPOSE  : Write rectifier redundancy parameters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2008-05-15 15:37
 *==========================================================================*/
static int ExecuteWRectRedund(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteWRectRedund!!!!\n");
        TRACE("\nExecuteWRectRedund:szInData is: %s\n", szInData);
	#endif
    
	int i;
	int iCID1,iCID2, iCMDType;
	char logText[YDN_LOG_TEXT_LEN];
	BOOL bRTN;

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO		*pCurEntry;
    YDN_TYPE_MAP_INFO *pTypeMap;
    unsigned char *pData;
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "E191", 4) != 0)
	{
		return CMDERR;
	}

    
	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_CMD_I(logText);
		return CMDERR;
	}

    #ifdef _DEBUG_YDN_CMD_HANDLER
	    TRACE_YDN_TIPS("Will write data to AC");
	    TRACE("the Block Info\n");
	    TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	    TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
    #endif //_DEBUG_YDN_CMD_HANDLER

	pCurEntry = pTypeMap->pMapEntriesInfo;
    iCMDType = (int)YDN_AsciiHexToChar(szInData + 4);

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
    
	for(i = 0; i < pTypeMap->iMapEntriesNum; i++)
	{
		if(iCMDType == pCurEntry->iYDNPort)
		{
			break;
		}
		pCurEntry++;
	}
    if(i == pTypeMap->iMapEntriesNum)
    {
		return CMDERR;
    }
	
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder)== FALSE)
	{
		return CTLFAIL;
	}
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	if(iCMDType == 0xE1)
		bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, szInData + 6);
	else
	    bRTN = SetYDNSigValues(TRUE, iEquipID, 1, pCurEntry, szInData + 6);

	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("ExecuteWRectRedund has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", szOutData);
        #endif //_DEBUG_YDN_CMD_HANDLER
       
		return NOR;
	}
	else
		return CTLFAIL;
}


/*==========================================================================*
 * FUNCTION : ExecuteRSigValue
 * PURPOSE  : Read any signal value
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2008-08-19 14:42
 *==========================================================================*/
 static int ExecuteRSigValue(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{ 
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteRSigValue!!!!\n");
        TRACE("\nExecuteRSigValue:szInData is: %s\n", szInData);
	#endif
    
	int i, iDataLen;
	unsigned char *pData;
	BOOL bDxiRTN;

	int  iEquipID, iSigType, iSigID;
  
	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strncmp(szInData, "F1F0", 4) != 0)
	{
		return CMDERR;
	}

    iDataLen = 0;
    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
      
	iEquipID = (int)YDN_AsciiHexToChar(szInData + 4);
	iSigType = (int)YDN_AsciiHexToChar(szInData + 6);
	iSigID   = (int)YDN_AsciiHexToChar(szInData + 8);

	bDxiRTN = GetSCUPSingleSigValues(TRUE, iSigType,
						 iSigID,
						 iEquipID,
						 szOutData);
	if(bDxiRTN == FALSE)
	{
		 for(i = 0; i < 8; i++)//reserved for LENGTH
		 {
			 *(szOutData+i) = 0x20;
		 }
	}
    szOutData += 8;
    iDataLen += 4;

    // 3.calculate LCHKSUM and package it 
	szOutData = pData;
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

    #ifdef _DEBUG_YDN_CMD_HANDLER
	   TRACE_YDN_TIPS("ExecuteRRectRedund has been excuted ok!");
	   TRACE("Reply Data:\n");
	   TRACE("\t%s\n", szOutData);
    #endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteWSigValue
 * PURPOSE  : Write rectifier redundancy parameters
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2008-05-15 15:37
 *==========================================================================*/
static int ExecuteWSigValue(YDN_BASIC_ARGS *pThis,
					   unsigned char *szInData,
					   unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    #ifdef _DEBUG_YDN_LINKLAYER
        TRACE("\nBegin ExecuteWSigValue!!!!\n");
        TRACE("\nExecuteWSigValue:szInData is: %s\n", szInData);
	#endif

	BOOL bRTN, bRet;
	SIG_BASIC_VALUE  *pBv = NULL;
	int  i, iEquipID, iSigType, iSigID, nBufLen;
    unsigned char *pData;
	long lSigID;
	/* not used the param */
	UNUSED(pThis);

	bRTN = FALSE;

	/* 1.check validity */
	if (strncmp(szInData, "F1F1", 4) != 0)
	{
		return CMDERR;
	}

    pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
    
	iEquipID = (int)YDN_AsciiHexToChar(szInData + 4);
	iSigType = (int)YDN_AsciiHexToChar(szInData + 6);
	iSigID   = (int)YDN_AsciiHexToChar(szInData + 8);

	lSigID = DXI_MERGE_SIG_ID(iSigType, iSigID);

	
	bRet = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,	
		lSigID,
		&nBufLen,			
		&pBv,			
		0);
	printf("\nbRet is: %d\n", bRet);
	if (bRet == ERR_DXI_OK && pBv)
	{
		printf("\npBv->ucType is: %d\n", pBv->ucType);
		switch (pBv->ucType)
		{
		case VAR_FLOAT:
			bRTN = SetYDNSingleSigValues(TRUE, iSigType, iSigID, iEquipID, 1, szInData + 10);
			break;

		case VAR_LONG:
			bRTN = SetYDNSingleSigValues(TRUE, iSigType, iSigID, iEquipID, 1, szInData + 10);
			break;

		case VAR_UNSIGNED_LONG:
			bRTN = SetYDNSingleSigValues(TRUE, iSigType, iSigID, iEquipID, 1, szInData + 10);
			break;

		case VAR_DATE_TIME:
			bRTN = SetYDNSingleSigValues(TRUE, iSigType, iSigID, iEquipID, 1, szInData + 10);
			//pVarValue->varValue.dtValue = fData;
			break;

		case VAR_ENUM:
			bRTN = SetYDNSingleSigValues(TRUE, iSigType, iSigID, iEquipID, 1, szInData + 10);
			break;
		}
	}
	
	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("ExecuteWSigValue has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", szOutData);
        #endif //_DEBUG_YDN_CMD_HANDLER
       
		return NOR;
	}
	else
		return CTLFAIL;
}

/*==========================================================================*
 * FUNCTION : ExecuteWT
 * PURPOSE  : add write time for compaible with ngc_lf ydn23 protocol
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-09-26 15:00
 *==========================================================================*/
static int ExecuteWT(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
    ASSERT(pThis);
    ASSERT(szInData);
    ASSERT(szOutData);

    // #ifdef _DEBUG_YDN_LINKLAYER
        printf("\nBegin ExecuteST!!!!\n");
        printf("\nExecuteST:szInData is: %s\n", szInData);
	//#endif

	int i, j, nError, nLen;
	size_t stLength;
	struct tm  stTime; 
	time_t tTime;
	unsigned char szBuf[7][3];
	unsigned char *pData;

	UNUSED(pThis);

    nLen = 0;
    nLen = strlen(szInData);
	/* 1.check the command */
    stLength = strlen(szInData);
	if (stLength != 18)
	{
		return CMDERR;
	}
	
	if (strncmp(szInData, "404E", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get and check param */
	for (i = 4; i < nLen; i++)
	{
		if (szInData[i] < '0')// || szInData[i] > '9')
		{
			return CMDERR;
		}
	}
			
	for (i = 0; i < 7; i++)
	{
		j = 2*i;
		szBuf[i][0] = szInData[j + 4];
		szBuf[i][1] = szInData[j + 1 + 4];
		szBuf[i][2] = '\0';

	}

    stTime.tm_year = (int)YDN_AsciiHexToChar(szBuf[0]) * 100 + 
						(int)YDN_AsciiHexToChar(szBuf[1]) - 1900;
	stTime.tm_mon  = (int)YDN_AsciiHexToChar(szBuf[2]) - 1;
	stTime.tm_mday = (int)YDN_AsciiHexToChar(szBuf[3]);

	stTime.tm_hour = (int)YDN_AsciiHexToChar(szBuf[4]);
	stTime.tm_min  = (int)YDN_AsciiHexToChar(szBuf[5]);
	stTime.tm_sec  = (int)YDN_AsciiHexToChar(szBuf[6]);

	if (stTime.tm_mon < 0 || stTime.tm_mon > 11 ||
		stTime.tm_mday < 1 || stTime.tm_mday > 31 ||
		stTime.tm_hour < 0 || stTime.tm_hour > 24 ||
		stTime.tm_min < 0 || stTime.tm_min > 60 ||
		stTime.tm_sec < 0 || stTime.tm_sec > 60)
	{
		TRACE("\nERR4!!\n");
		return CMDERR;
	}

	tTime = mktime(&stTime);

	/* 3.set time */
	ConvertTime(&tTime, FALSE);
	nError = DxiSetData(VAR_TIME_SERVER_INFO,
		SYSTEM_TIME,			
		0,		
		sizeof(tTime),			
		&tTime,			
		0);
	pData = szOutData;
    for(i = 0; i < 4; i++, szOutData++)
    	*szOutData = '0';
    *szOutData = '\0';
    szOutData = pData;
	if (nError != ERR_DXI_OK)
	{
		LOG_YDN_CMD_E("Call DXI interface to set System time failed");
		return CTLFAIL;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("ST has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}
/*==========================================================================*
 * FUNCTION : ExecuteWKey
 * PURPOSE  : Write Key value
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-09-26 14:43
 *==========================================================================*/
static int ExecuteWKey(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
#define YDN23_KEY_ESC	46
#define YDN23_KEY_ENT	39
#define YDN23_KEY_UP	43
#define YDN23_KEY_DOWN	45
#define YDN23_KEY_NONE	47
#define YDN23_KEY_RESET	38
#define YDN23_KEY_BLUP	35
#define YDN23_KEY_BLDN	37
	
	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	// #ifdef _DEBUG_YDN_LINKLAYER
		printf("\nBegin ExecuteST!!!!\n");
		printf("\nExecuteST:szInData is: %s\n", szInData);
	//#endif

	int i, j, nError, nLen;
	size_t stLength;
	unsigned char szBuf[3];
	unsigned char *pData;
	unsigned char ucCmd[5];
	_KEY_INFO_   stTestKey;
	unsigned int	uiTemp;

	UNUSED(pThis);

	nLen = 0;
	nLen = strlen(szInData);
	/* 1.check the command */
	stLength = strlen(szInData);
	//if (stLength != 14)
	if (stLength != 6)
	{
		return CMDERR;
	}
	
	//if (strncmp(szInData, "F1F2", 4) != 0)
	if (strncmp(szInData, "E1E4", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get and check param */
	for (i = 4; i < nLen; i++)
	{
		if (szInData[i] < '0')// || szInData[i] > '9')
		{
			return CMDERR;
		}
	}
			
	//for (i = 0; i < 5; i++)
	for (i = 0; i < 1; i++)
	{
		j = 2*i;
		szBuf[0] = szInData[j + 4];
		szBuf[1] = szInData[j + 1 + 4];
		szBuf[2] = '\0';
		ucCmd[i] = YDN_AsciiHexToChar(szBuf);		
	}

	//stTestKey.byKeyValue = ucCmd[0];
	switch(ucCmd[0])
	{
		case YDN23_KEY_ESC:
			stTestKey.byKeyValue = VK_ESCAPE;
			break;
		case YDN23_KEY_ENT:
			stTestKey.byKeyValue = VK_ENTER;
			break;
		case YDN23_KEY_UP:
			stTestKey.byKeyValue = VK_UP;
			break;
		case YDN23_KEY_DOWN:
			stTestKey.byKeyValue = VK_DOWN;
			break;
		case YDN23_KEY_NONE:
			stTestKey.byKeyValue = VK_NO_KEY;
			break;
		default:
			stTestKey.byKeyValue = VK_NO_KEY;
			break;
	}
	//uiTemp = 0;
	//for ( i=1; i<5; i++ )
	//{
	//	uiTemp = (uiTemp<<8) + ucCmd[i];
	//}
	uiTemp = 1;
	stTestKey.nCount = (int)uiTemp;

	/* 3.set key value */
	nError = DxiSetData(VAR_LCDKEY_THRUYDN23,
		FALSE,			
		0,		
		sizeof(_KEY_INFO_),			
		&stTestKey, 		
		0);
	pData = szOutData;
	for(i = 0; i < 4; i++, szOutData++)
		*szOutData = '0';
	*szOutData = '\0';
	szOutData = pData;
	if (nError != ERR_DXI_OK)
	{
		LOG_YDN_CMD_E("Call DXI interface to set Key Value failed");
		return CTLFAIL;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("ST has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : GetHisAlarms
 * PURPOSE  : Get one History alarm info to fill ALARM_INFO_BUFFER
 * CALLS    : 
 * CALLED BY: ExecuteRH
 * ARGUMENTS: void  *pAlarmBuf : 
 *			int	iAlarmNo; history alarm no, 0 ~ (MAX_HIS_ALARM_COUNT-1)
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-09-27 13:11
 *==========================================================================*/
static BOOL GetHisAlarms(void *pAlarmBuf, int iAlarmNo)
{
	HANDLE hHisAlarmLog;
	BOOL bRet;
	int i, iRet,  iRecords;
	struct tm *tTime;
	int iStartPos;
	time_t	tCurTime;

	HIS_ALARM_RECORD *pAlarmRecord;
	HIS_ALARM_RECORD HisAlarmRecord;
	UCHAR byTemp;
	UCHAR	*pbyBuf;

	if ( iAlarmNo >= MAX_HIS_ALARM_COUNT )
	{
		iAlarmNo = 0;
	}
	
	/* 1.Read history alarm info from the Storage */
	hHisAlarmLog = DAT_StorageOpen(HIST_ALARM_LOG);
	if (hHisAlarmLog == NULL)
	{
		LOG_YDN_CMD_E("Open History Alarm Log storage failed");
		TRACE("Open History Alarm Log storage failed");
		return FALSE;
	}

	iStartPos = iAlarmNo;	//Start record number,must 
	iRecords = 1;	//Only read 1 record
	bRet = DAT_StorageReadRecords(hHisAlarmLog, &iStartPos,
		&iRecords, (void *)&HisAlarmRecord, FALSE, FALSE);
	DAT_StorageClose(hHisAlarmLog);
	if (!bRet)
	{
		LOG_YDN_CMD_E("Read History Alarm Log storage failed");
		TRACE("Read History Alarm Log storage failed");
		return FALSE;
	}
	

	//alarm process for test ydn23, from HisAlarmRecord
	pbyBuf = (UCHAR  *)pAlarmBuf;
	byTemp = (UCHAR)iAlarmNo;
	HexToTwoAsciiData(byTemp, pbyBuf);
	pbyBuf += 2;
	
	HexToTwoAsciiData((UCHAR)0xff, pbyBuf);
	pbyBuf += 2;
	
	byTemp = (UCHAR)HisAlarmRecord.iEquipID;
	HexToTwoAsciiData(byTemp, pbyBuf);
	pbyBuf += 2;

	HexToTwoAsciiData((UCHAR)HisAlarmRecord.iAlarmID, pbyBuf);
	pbyBuf += 2;

	tCurTime = HisAlarmRecord.tmStartTime;
	ConvertTime(&tCurTime, TRUE);
	tTime = localtime(&tCurTime);
	HexToTwoAsciiData((UCHAR)(tTime->tm_year -100), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mon + 1), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mday), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_hour), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_min), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_sec), pbyBuf);
	pbyBuf += 2;
	
	tCurTime = HisAlarmRecord.tmEndTime;
	ConvertTime(&tCurTime, TRUE);
	tTime = localtime(&tCurTime);
	HexToTwoAsciiData((UCHAR)(tTime->tm_year -100), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mon + 1), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mday), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_hour), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_min), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_sec), pbyBuf);
	pbyBuf += 2;
#if 1
	for ( i=0; i<8; i++ )
	{
		if ( (HisAlarmRecord.szSerialNumber[i]>='0') 
			&& (HisAlarmRecord.szSerialNumber[i]<='9'))
		{
			*pbyBuf = (UCHAR)HisAlarmRecord.szSerialNumber[i];
		}
		else
		{
			*pbyBuf = 0x30;
		}
		
		pbyBuf++;
	}
#else
	printf("the serial is %s\n", HisAlarmRecord.szSerialNumber);
	for ( i=0; i<8; i++ )
	{
		*pbyBuf++ = 0x30;
	}
#endif


	//printf("ydn23ydn23ydn23ydn23ydn23ydn23ydn23ydn23\n");
	//for ( i=0; i<40; i++ )
	//{
	//	printf("%x\t", *((char *)pAlarmBuf+i));
	//}
	//printf("\nydn23ydn23ydn23ydn23ydn23ydn23ydn23ydn23\n");
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : GetBattTestLog
 * PURPOSE  : Get one History alarm info to fill ALARM_INFO_BUFFER
 * CALLS    : 
 * CALLED BY: ExecuteRH
 * ARGUMENTS: void  *pBattTestBuf : 
 *			int	iBattTestNo; history alarm no, 0 ~ (MAX_TEST_LOG-1)
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-10-19 19:11
 *==========================================================================*/
static int YND23_CompareProc(const void *pRecord, const void *pCondition)
{
	UNUSED(pCondition);
	return (stricmp(pRecord, "Head of a Battery Test") == 0 ? TRUE : FALSE);
}

static BOOL GetBattTestLog(IN int iBattTestNo, OUT UCHAR *pBattTestBuf)
{
	HANDLE	hTestLog;
	int sRecordNoBuff[MAX_TEST_LOG];
	int  iBattLogID, iStartNo, iRet, iRecords;
	BOOL bRet;

	BT_LOG_SUMMARY *pLogSummary;
	BT_LOG_BATT_CFG *pLogBattCfg, *pCurLogBattCfg; 
	int i, j, k, iQtyBatt;
	char SummaryRecordBuf[256], CfgRecordBuf[1024];//CfgRecordBuf�Ĵ�С��256��Ϊ1024
	char *sBTLogDetail;
	
	struct tm *tTime;
	float	fTemp;
	UCHAR	*pbyBuf;
	int		iBattLogNum;
	float	fStartVolt, fEndVolt;
	
	//iBattLogID = iBattTestNo;
	iBattLogNum = 10;
	/* 1.open the storage */
	hTestLog = DAT_StorageOpen(BATT_TEST_LOG_FILE);
	if (hTestLog == NULL)
	{
		LOG_YDN_CMD_E("Open Battery Test Log storage failed");
		TRACE("Open Battery Test Log storage failed");
		return FALSE;
	}

	/* 2.search the record no */
	iStartNo = -1;
	iRet = DAT_StorageSearchRecords(hTestLog, 
		YND23_CompareProc, 
		NULL,
		&iStartNo,           //search from the last record
		&iBattLogNum, 
		sRecordNoBuff, 
		FALSE,
		FALSE,
		TRUE);

	if (!iRet || iBattLogNum == 0)
	{
		DAT_StorageClose(hTestLog);

#ifdef _DEBUG_YDN_CMD_HANDLER
		TRACE_YDN_TIPS("Seach Battery Log failed");
		TRACE("Log No. is: %d\n", iBattLogID);
		TRACE("iRet value is: %d\n", iRet);
#endif //_DEBUG_YDN_CMD_HANDLER

		return FALSE;
	}

#ifdef _TEST_YDN_BATTLOG
	int ii;
	TRACE("\nBattLog Head Record No. Info:\n");
	for (ii = 0; ii < iBattLogID; ii++)
	{
		TRACE("Record No. of Battery Log %d is: %d\n", 
			ii + 1, sRecordNoBuff[ii]);
	}
#endif //_TEST_YDN_BATTLOG

	/* 3.read the test log summary info */
	iBattLogID = iBattTestNo;
	iRecords = 1;  
	iStartNo = sRecordNoBuff[iBattLogID];
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	iRet = DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, SummaryRecordBuf, FALSE, TRUE);
	pLogSummary = (BT_LOG_SUMMARY *)(SummaryRecordBuf + 64);
	if (iRet == FALSE)
	{
		DAT_StorageClose(hTestLog);
		return FALSE;
	}
	iQtyBatt = pLogSummary->iQtyBatt;
	if ( pLogSummary->iQtyRecord <= 0)
	{
		return FALSE;
	}
	
#ifdef _TEST_YDN_BATTLOG
	TRACE("\nBattLog Head Record No. Info:\n");
	for (ii = 0; ii < MAX_TEST_LOG; ii++)
	{
		TRACE("Record No. of Battery Log %d is: %d\n", 
			ii + 1, sRecordNoBuff[ii]);
	}
#endif //_TEST_YDN_BATTLOG


#ifdef _TEST_YDN_BATTLOG
	{
		char strStartTime[20], strEndTime[20];
#define BT_TIME_FORMAT  "%Y-%m-%d %H:%M:%S"
		TimeToString(pLogSummary->tStartTime, BT_TIME_FORMAT, 
			strStartTime, sizeof(strStartTime));
		TimeToString(pLogSummary->tEndTime, BT_TIME_FORMAT, 
			strEndTime, sizeof(strEndTime));
		TRACE_ESR_TIPS("Read Battery Test Log Summary Info OK");
		TRACE("Summary Info:\n");
		TRACE("Test Start Time:%s\nTest End Time:%s\n",  strStartTime, strEndTime);
		TRACE("Test Start Reason: %d\nTest End Reason: %d\n", 
			pLogSummary->iStartReason, pLogSummary->iEndReason);
		TRACE("Test Result: %d\n", pLogSummary->iTestResult);
		TRACE("Rated Capacity: %f\n", pLogSummary->fBattRatedCap);
		TRACE("Battery String quentity: %d\n", pLogSummary->iQtyBatt);
		TRACE("Battery Test Record quentity: %d\n", pLogSummary->iQtyRecord);
		TRACE("Record Size: %d\n", pLogSummary->iSizeRecord);
		
	}
#endif //_TEST_ESR_BATTLOG
	
	/* 4.read battery string config info */
	iRecords = 4;
	iStartNo = sRecordNoBuff[iBattLogID] + 1;
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	iRet = DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, CfgRecordBuf, FALSE, TRUE);
	if (iRet == FALSE)
	{
		DAT_StorageClose(hTestLog);

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Read Battery Test Log Config Info failed");
		TRACE("Log No. is: %d\n", iBattLogID);
#endif //_DEBUG_ESR_CMD_HANDLER

		return FALSE;
	}
	pLogBattCfg = NEW(BT_LOG_BATT_CFG, iQtyBatt);
	
#ifdef _TEST_ESR_BATTLOG
	TRACE("\n\n\n\n\nBatt Test Config Info:\n String 1 Capa %d\n "
		"String 2 capa: %d\n", CfgRecordBuf[2], CfgRecordBuf[7]);
#endif //_TEST_ESR_BATTLOG

	if (pLogBattCfg == NULL)
	{
		LOG_YDN_CMD_E("No memory for read battery test log");
		DAT_StorageClose(hTestLog);
		return FALSE;
	}
	ZERO_POBJS(pLogBattCfg, iQtyBatt);


	pCurLogBattCfg = pLogBattCfg;
	for (i = 0, j = 0, k = 0; i < iQtyBatt; i++, pCurLogBattCfg++)
	{
		if (CfgRecordBuf[j] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_CUR);
			pCurLogBattCfg->iOffsetCur = 0;
			k++;
		}
		if (CfgRecordBuf[j + 1] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_VOL);
			pCurLogBattCfg->iOffsetVol = 4 * k;
			k++;
		}
		if (CfgRecordBuf[j + 2] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_CAP);
			pCurLogBattCfg->iOffsetCap = 4 * k;
			k++;
		}
		if (CfgRecordBuf[j + 3] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_TEMP);
			pCurLogBattCfg->iOffsetTemp = 4 * k;
			k++;
		}

		pCurLogBattCfg->iQtyBlockVol = CfgRecordBuf[j + 4];
		pCurLogBattCfg->iOffsetBlockVol = 4 * k;

		k = 0;
		j += 5;
	}
	
	/* 5.read data */
	iRecords = (pLogSummary->iQtyRecord * pLogSummary->iSizeRecord - 1)/SIZE_STORAGE_UNIT + 1;
	iStartNo = sRecordNoBuff[iBattLogID] + 5;
	sBTLogDetail = NEW(char, iRecords*SIZE_STORAGE_UNIT);
	
	
#ifdef _TEST_ESR_BATTLOG
	TRACE("\n\n\n\n\n\niRecords value is: %d\n", iRecords);
	TRACE("Detail Data buffer is: %d\n\n\n\n", iRecords*SIZE_STORAGE_UNIT);
#endif //_TEST_ESR_BATTLOG
	
	if (sBTLogDetail == NULL)
	{
		LOG_YDN_CMD_E("no memory for read battery test log");
		DELETE(pLogBattCfg);
		DAT_StorageClose(hTestLog);
		return FALSE;
	}
	
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	if (DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, sBTLogDetail, FALSE, TRUE) == FALSE)
	{
		DELETE(pLogBattCfg);
		DELETE(sBTLogDetail);

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_YDN_TIPS("Read Battery Test Log Detail Info failed");
		TRACE("Log No. is: %d\n", iBattLogID);
#endif //_DEBUG_ESR_CMD_HANDLER

		DAT_StorageClose(hTestLog);
		return FALSE;
	}
	//get batt test start volt
	printf("\nthe record num is %d, the record size is %d  bytes!\n",pLogSummary->iQtyRecord,pLogSummary->iSizeRecord);
	fStartVolt = *((float *)(sBTLogDetail+4));
	printf("\nThe Batt test start volt is : %7.3f\n", fStartVolt);
	//get batt test end volt
	printf("iQtyRecord is %d iSizeRecord is %d\n", pLogSummary->iQtyRecord, pLogSummary->iSizeRecord);
	fEndVolt = *((float *)(sBTLogDetail
						+(pLogSummary->iQtyRecord-1)*(pLogSummary->iSizeRecord)
						+4));
	printf("\nThe Batt test end  volt is : %7.3f\n", fEndVolt);
	
	/*4 process to ydn23 format*/
	/*
	total 32 bytes
	test num,		
	start mode, start time(6 byte), start volt(float, 4 byte), 
	end mode, end time(6 bytes), end volt(float, 4 byte)
	batt1 test cap(float,4byte), batt2 test cap( float, 4bytes), temp (1bytes)
	*/
	pbyBuf = (UCHAR *)pBattTestBuf;
	HexToTwoAsciiData((UCHAR)iBattTestNo, pbyBuf);
	pbyBuf += 2;
	
	HexToTwoAsciiData((UCHAR)pLogSummary->iStartReason, pbyBuf);
	pbyBuf += 2;
	
	time_t tCurTime;
	tCurTime = pLogSummary->tStartTime;
	ConvertTime(&tCurTime, TRUE);
	tTime = localtime(&tCurTime);
	HexToTwoAsciiData((UCHAR)(tTime->tm_year -100), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mon + 1), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mday), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_hour), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_min), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_sec), pbyBuf);
	pbyBuf += 2;

	GetAnalogueStr(TRUE, fStartVolt, pbyBuf);	//start volt
	pbyBuf += 8;
	
	HexToTwoAsciiData((UCHAR)pLogSummary->iEndReason, pbyBuf);
	pbyBuf += 2;
	
	tCurTime = pLogSummary->tEndTime;
	ConvertTime(&tCurTime, TRUE);
	tTime = localtime(&tCurTime);
	HexToTwoAsciiData((UCHAR)(tTime->tm_year -100), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mon + 1), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_mday), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_hour), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_min), pbyBuf);
	pbyBuf += 2;
	HexToTwoAsciiData((UCHAR)(tTime->tm_sec), pbyBuf);
	pbyBuf += 2;

	GetAnalogueStr(TRUE, fEndVolt, pbyBuf);	//end volt
	pbyBuf += 8;
	
	GetAnalogueStr(TRUE, (float)-1, pbyBuf);	//batt1 cap
	pbyBuf += 8;
	
	GetAnalogueStr(TRUE, (float)-2, pbyBuf);	//batt2 cap
	pbyBuf += 8;
	
	HexToTwoAsciiData((UCHAR)0, pbyBuf);		//temp
	
	DELETE(pLogBattCfg);
	DELETE(sBTLogDetail);
	
	return TRUE;
	
}

/*==========================================================================*
 * FUNCTION : ExecuteRHisEvent
 * PURPOSE  : ReadHistoryAlarm, reat test log
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-09-27 10:43
 *==========================================================================*/
static int ExecuteRHisEvent(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	// #ifdef _DEBUG_YDN_LINKLAYER
		TRACE("\nBegin ExecuteReadHistoryEvent!!!!\n");
		TRACE("\nExecuteReadHistoryAlarm:szInData is: %s\n", szInData);
	//#endif

	int i, j, nError, nLen;
	size_t stLength;
	unsigned char szBuf[3];
	unsigned char *pData;
	unsigned char ucCmd[3];
	_KEY_INFO_   stTestKey;
	int		iEventType;		//0 history alarm, 1 history event, 2 batt test log 3 history data
							//4 clear batt test log,  5 smdu batt test log
	int		iEventNo;		//event no.
	UCHAR pbyEventRecord[64];

	UNUSED(pThis);

	nLen = 0;
	nLen = strlen(szInData);
	/* 1.check the command */
	
	if (strncmp(szInData, "E1EA", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get and check param */
	for (i = 4; i < nLen; i++)
	{
		if (szInData[i] < '0')// || szInData[i] > '9')
		{
			return CMDERR;
		}
	}
	for (i = 0; i < 3; i++)
	{
		j = 2*i;
		szBuf[0] = szInData[j + 4];
		szBuf[1] = szInData[j + 1 + 4];
		szBuf[2] = '\0';
		ucCmd[i] = YDN_AsciiHexToChar(szBuf);		
	}
	iEventType = ucCmd[0];
	if ( iEventType > 5 )
	{
		return CMDERR;
	}
	iEventNo = ucCmd[1] * 256 + ucCmd[2];
	
	/* 3.Get HistoryAlarm */
	pData = szOutData;
    for(i = 0; i < 4; i++)//reserved for LENGTH
    {
    	*szOutData = '0';
    	 szOutData++;
    }
	if ( 0 == iEventType )
	{
		if ( iEventNo > (MAX_HIS_ALARM_COUNT-1) )
		{
			return CMDERR;
		}
		//get history alarm
		if ( GetHisAlarms(pbyEventRecord, iEventNo+1))
		{
			for (i=0; i<6; i++, szOutData++)
			{
				*szOutData = szInData[i+4];
			}
			for( i=0; i<40; i++, szOutData++)
			{
				*szOutData = pbyEventRecord[i];
			}
			*szOutData = '\0';
			szOutData = pData;
			szOutData = YDN_BuildDataInfo(23, szOutData);
			nError = ERR_DXI_OK;
		}
		else
		{
			return CTLFAIL;
		}
	}
	else if ( 1 == iEventType )
	{
		//get history event
		;
	}
	else if ( 2 == iEventType )
	{
		//get batt test log
		if ( iEventNo > (MAX_TEST_LOG-1) )
		{
			return CMDERR;
		}
		
		//get batt test log
		if ( GetBattTestLog(iEventNo, pbyEventRecord))
		{
			for (i=0; i<6; i++, szOutData++)
			{
				*szOutData = szInData[i+4];
			}
			for( i=0; i<64; i++, szOutData++)
			{
				*szOutData = pbyEventRecord[i];
			}
			*szOutData = '\0';
			szOutData = pData;
			szOutData = YDN_BuildDataInfo(35, szOutData);
			nError = ERR_DXI_OK;
		}
		else
		{
			return CTLFAIL;
		}
	}
	else if ( 3 == iEventType )
	{
		//get history data
	}
	else if ( 4 == iEventType )
	{
		//clear batt test log
	}
	else if ( 5 == iEventType )
	{
		//get smdu batt test log
	}
	else
	{
		return CMDERR;
	}
//	pData = szOutData;
//	for(i = 0; i < 4; i++, szOutData++)
//		*szOutData = '0';
//	*szOutData = '\0';
//	szOutData = pData;
	if (nError != ERR_DXI_OK)
	{
		pData = szOutData;
		for(i = 0; i < 4; i++, szOutData++)
			*szOutData = '0';
		*szOutData = '\0';
		szOutData = pData;
		LOG_YDN_CMD_E("Call DXI interface to set Key Value failed");
		return CTLFAIL;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("ST has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}

/*==========================================================================*
 * FUNCTION : ExecuteCSomeAlarm
 * PURPOSE  : ClearHistoryAlarm, batt test alarm, short batt test alarm
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-10-19 18:43
 *==========================================================================*/
static int ExecuteCSomeAlarm(YDN_BASIC_ARGS *pThis,
					  unsigned char *szInData,
					  unsigned char *szOutData)
{
	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);

	// #ifdef _DEBUG_YDN_LINKLAYER
		printf("\nBegin ExecuteReadHistoryAlarm!!!!\n");
		printf("\nExecuteReadHistoryAlarm:szInData is: %s\n", szInData);
	//#endif

	int i, j, nError, nLen;
	size_t stLength;
	unsigned char szBuf[3];
	unsigned char *pData;
	unsigned char ucCmd[3];
	_KEY_INFO_   stTestKey;
	int		iEventType;		//0 history alarm, 1 history event, 2 batt test log 3 history data
							//4 clear batt test log,  5 smdu batt test log
	int		iEventNo;		//event no.
	UCHAR pbyHisAlarmRecord[20];

	UNUSED(pThis);

	nLen = 0;
	nLen = strlen(szInData);
	/* 1.check the command */
	
	if (strncmp(szInData, "E1F0", 4) != 0)
	{
		return CMDERR;
	}

	/* 2.get and check param */
	for (i = 4; i < nLen; i++)
	{
		if (szInData[i] < '0')// || szInData[i] > '9')
		{
			return CMDERR;
		}
	}
	for (i = 0; i < 1; i++)
	{
		j = 2*i;
		szBuf[0] = szInData[j + 4];
		szBuf[1] = szInData[j + 1 + 4];
		szBuf[2] = '\0';
		ucCmd[i] = YDN_AsciiHexToChar(szBuf);		
	}
	iEventType = ucCmd[0];
	if (iEventType > 4)
	{
		return CMDERR;
	}
	
	/* 3.Get HistoryAlarm */
	pData = szOutData;
    for(i = 0; i < 4; i++)//reserved for LENGTH
    {
    	*szOutData = '0';
    	 szOutData++;
    }
	if ( 0 == iEventType )
	{
		//Clear history alarm
		/* perform the command */
		DAT_StorageDeleteRecord(HIST_ALARM_LOG);
		*szOutData = '\0';
		szOutData = pData;
		//szOutData = YDN_BuildDataInfo(0, szOutData);
		nError = ERR_DXI_OK;
	}
	else if ( 1 == iEventType )
	{
		//clear batt test alarm
		/* perform the command */
		*szOutData = '\0';
		szOutData = pData;
		//szOutData = YDN_BuildDataInfo(0, szOutData);
		nError = ERR_DXI_OK;
	}
	else if ( 2 == iEventType )
	{
		//clear batt short test alarm
		/* perform the command */
		*szOutData = '\0';
		szOutData = pData;
		//szOutData = YDN_BuildDataInfo(0, szOutData);
		nError = ERR_DXI_OK;
	}
	else if ( 3 == iEventType )
	{
		//clear rect lost alarm
		/* perform the command */
		*szOutData = '\0';
		szOutData = pData;
		//szOutData = YDN_BuildDataInfo(0, szOutData);
		nError = ERR_DXI_OK;
	}
	else if ( 4 == iEventType )
	{
		//clear sys weihua
		/* perform the command */
		*szOutData = '\0';
		szOutData = pData;
		//szOutData = YDN_BuildDataInfo(0, szOutData);
		nError = ERR_DXI_OK;
	}
	else if ( 5 == iEventType )
	{
		//clear energy save abm alarm
		/* perform the command */
		*szOutData = '\0';
		szOutData = pData;
		//szOutData = YDN_BuildDataInfo(0, szOutData);
		nError = ERR_DXI_OK;
	}
	else
	{
		return CMDERR;
	}
//	pData = szOutData;
//	for(i = 0; i < 4; i++, szOutData++)
//		*szOutData = '0';
//	*szOutData = '\0';
//	szOutData = pData;
	if (nError != ERR_DXI_OK)
	{
		LOG_YDN_CMD_E("Call DXI interface to set Key Value failed");
		return CTLFAIL;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("ST has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_YDN_CMD_HANDLER

	return NOR;
}


/*==========================================================================*
 * FUNCTION : SendAddress
 * PURPOSE  : pack SCU+ address
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 *            OUT unsigned char  *szResData  : 
 *          
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2007-05-09 10:53
 *==========================================================================*/
void	SendAddress(unsigned char *szResData, OUT int *pDataLen)
{
	unsigned char	temp, xrl;
	unsigned char   *pData;

	pData = szResData;
	
	///*pTXD++ = 0x21;								/*'!':��ͷ*/
	//HexToTwoAsciiData(0x00, szOutData);
	*pData = 0x21;
	pData++;
	
	xrl = (g_YDNGlobals.CommonConfig.byADR >> 4) + 0x30;/*High byte*/
	
	*pData = xrl;
	pData++;
	
	temp = (g_YDNGlobals.CommonConfig.byADR & 0x0f) + 0x30;/*Low byte*/
	
	*pData = temp;
	pData++;
	
	xrl = xrl ^ temp;							/*���У��*/
	
	*pData = (xrl >> 4) + 0x30;
	pData++;
	*pData = (xrl & 0x0f) + 0x30;
	pData++;
	
	*pData = 0x0d;

	pData++;

	*pData = '\0';
	 
	*pDataLen = strlen(szResData);
}

/*==========================================================================*
 * FUNCTION : HasLinkedToRefText
 * PURPOSE  : ExecuteRR
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iModuleNo : 
 *            int  iRefNo    : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2005-01-19 15:37
 *==========================================================================*/
__INLINE static BOOL HasLinkedToRefText(int iModuleNo, int iRefNo)
{
	int i, iRefTexts;
	YDN_REF_TEXT *pRefTexts;

	iRefTexts = g_YDNGlobals.iRefTexts;
	pRefTexts = g_YDNGlobals.RefTexts;

	for (i = 0; i < iRefTexts; i++, pRefTexts++)
	{
		if (pRefTexts->iModuleNo == iModuleNo &&
			pRefTexts->iRefNo == iRefNo)
		{
			return TRUE;
		}
	}

	return FALSE;
}

/* Added by Thomas begin, support product info, 2006-2-9 */
#ifdef PRODUCT_INFO_SUPPORT
///*==========================================================================*
// * FUNCTION : GetUnitNumByGroupID
// * PURPOSE  : 
// * CALLS    : 
// * CALLED BY: 
// * ARGUMENTS: int  iGroupID : 
// * RETURN   : int : 
// * COMMENTS : 
// * CREATOR  : HanTao                   DATE: 2006-2-9 16:20
// *==========================================================================*/
//static int GetUnitNumByGroupID(int iGroupID)
//{
//	int i, iTypeMapNum;
//	YDN_TYPE_MAP_INFO *pTypeMap;
//
//	pTypeMap = g_YDNGlobals.YDNModelConfig.pTypeMapInfo;
//	iTypeMapNum = g_YDNGlobals.YDNModelConfig.iTypeMapNum;
//
//	for (i = 0; i < iTypeMapNum; i++, pTypeMap++)
//	{
//		if (pTypeMap->iGroupID == iGroupID)
//		{
//			return pTypeMap->iUnitNum;
//		}
//	}
//
//	return 0;
//}

///*==========================================================================*
// * FUNCTION : IsBlockRelativeDevice
// * PURPOSE  : Aided function, judge if a device relative to a block
// * CALLS    : 
// * CALLED BY: 
// * ARGUMENTS: YDN_BLOCK_INFO  *pBlock   : 
// *            int             iDeviceID : 
// * RETURN   : BOOL : 
// * COMMENTS : 
// * CREATOR  : HanTao                   DATE: 2006-2-9 15:40
// *==========================================================================*/
//static BOOL IsBlockRelativeDevice(YDN_BLOCK_INFO *pBlock, int iDeviceID)
//{
//	int i;
//
//	if (pBlock)
//	{
//		for (i = 0; i < pBlock->iRelatedDeviceNum; i++)
//		{
//			if (iDeviceID == pBlock->piRelatedDeviceIndex[i])
//			{
//				return TRUE;
//			}
//		}
//	}
//
//	return FALSE;
//}


///*==========================================================================*
// * FUNCTION : AllocateDeviceToGroup
// * PURPOSE  : 
// * CALLS    : 
// * CALLED BY: 
// * ARGUMENTS:   void : 
// * RETURN   : void : 
// * COMMENTS : 
// * CREATOR  : HanTao                   DATE: 2006-2-9 10:17
// *==========================================================================*/
//static void AllocateDeviceToGroup(void)
//{
//	int i, j, k, iGroupID, iUnitID;
//	int iBlockNum, iDeviceNum, iUnitNum;
//	YDN_BLOCK_INFO *pBlock, *pUnitBlock, *pSysBlock;
//
//	iDeviceNum = DXI_GetDeviceNum();
//
//	//get System level block
//	pSysBlock = GetBlock(0, 0);
//	/* scu board has been taken as the ACU device, not like PSC.
//	 * so can delete it here. Thomas, 2006-2-16 */
//	//pSysBlock->iRelatedDeviceNum = 1;
//	//pSysBlock->piRelatedDeviceIndex[0] = 1;  //PSC device
//
//	//go through the Group level block
//	pBlock = g_YDNGlobals.YDNModelInfo.pYDNBlockInfo;
//	iBlockNum = g_YDNGlobals.YDNModelInfo.iBlocksNum;
//	for (i = 0; i < iBlockNum; i++, pBlock++)
//	{
//		iGroupID = pBlock->iGroupID;
//		iUnitID = pBlock->iUnitID;
//
//		//for Group Level block, reply the devices related to its units
//		if (iGroupID != 0 && iUnitID == 0)
//		{
//			pBlock->iRelatedDeviceNum = 0;
//			iUnitNum = GetUnitNumByGroupID(iGroupID);
//
//			for (j = 1; j <= iDeviceNum; j++)  //Device ID started from 1
//			{
//				for (k = 1; k <= iUnitNum; k++) //Unit ID started from 1
//				{
//				    /* Rect number dynamically recognization,
//				     * if Rect block is not pushed, ruturn NULL
//					 */
//					pUnitBlock = GetBlock(iGroupID, k);
//					if (IsBlockRelativeDevice(pUnitBlock, j))
//					{
//						//find!
//						/* check if has been added already, Thomas, 2006-2-16 */
//						if (!IsBlockRelativeDevice(pBlock, j))
//						{
//							pBlock->piRelatedDeviceIndex[pBlock->iRelatedDeviceNum] = j;
//							pBlock->iRelatedDeviceNum++;
//						}
//
//						//attach to System level block
//						/* check if has been added already, Thomas, 2006-2-16 */
//						if (!IsBlockRelativeDevice(pSysBlock, j))
//						{
//							pSysBlock->piRelatedDeviceIndex[pSysBlock->iRelatedDeviceNum] = j;
//							pSysBlock->iRelatedDeviceNum++;
//						}
//					}
//				}
//			}
//
//		}
//	}
//
//	qsort(pSysBlock->piRelatedDeviceIndex,
//		(size_t)pSysBlock->iRelatedDeviceNum,
//		sizeof(int), 
//		(int (*)(const void *, const void *))CompareDeviceIDs); 
//
//	return;
//}
#endif

#ifdef PRODUCT_INFO_SUPPORT
/*==========================================================================*
 * FUNCTION : GetDeviceIDFromStr
 * PURPOSE  : get value from 4 ascii hex. eg: "121A" = 1*16*16*16 + 2*16*16
 *            + 1*16+10 = 4634
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: *pStr : four char buffer, shall be checked out of the func
 * RETURN   : int : 
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-2-10 9:02
 *==========================================================================*/
static int GetDeviceIDFromStr(const unsigned char *pStr)
{
	int  iRet;
	BYTE byHiValue;

	//YDN_AsciiHexToChar function will process 2 char Hex.
	byHiValue = YDN_AsciiHexToChar(pStr);
	iRet = byHiValue*0x100 + YDN_AsciiHexToChar(pStr + 2);

	return iRet;
}
#endif //PRODUCT_INFO_SUPPORT

/*==========================================================================*
 * FUNCTION : YDN_InitCmdHander
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: InitYDNGlobals
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 14:39
 *==========================================================================*/
void YDN_InitCmdHander()
{
	YDN_CMD_HANDLER *pCmdHandlers;

	pCmdHandlers = g_YDNGlobals.CmdHandlers;

	DEF_YDN_CMD_HANDLER(&pCmdHandlers[0], "4041", FALSE, ExecuteRACA);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[1], "4043", FALSE, ExecuteRACS);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[2], "4044", FALSE, ExecuteRACAL);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[3], "4046", FALSE, ExecuteRACP);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[4], "4048", TRUE,  ExecuteWACP);

	DEF_YDN_CMD_HANDLER(&pCmdHandlers[5], "4141", FALSE, ExecuteRREA);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[6], "4143", FALSE, ExecuteRRES);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[7], "4144", FALSE, ExecuteRREAL);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[8], "4145", TRUE, ExecuteCRE);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[9], "4180", TRUE,  ExecuteSTRE);
    DEF_YDN_CMD_HANDLER(&pCmdHandlers[10], "41E1", FALSE, ExecuteREID);
    
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[11], "4241", FALSE, ExecuteRDCA);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[12], "4244", FALSE, ExecuteRDCAL);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[13], "4246", FALSE, ExecuteRDCP);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[14], "4248", TRUE,  ExecuteWDCP);
	
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[15], "E141", FALSE,  ExecuteRSYSA);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[16], "E143", FALSE,  ExecuteRSYSS);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[17], "E144", FALSE,  ExecuteRSYSAL);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[18], "E146", FALSE,  ExecuteRSYSP);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[19], "E148", TRUE,  ExecuteWSYSP);

	DEF_YDN_CMD_HANDLER(&pCmdHandlers[20], "E1F7", FALSE, ExecuteREXAL);

	DEF_YDN_CMD_HANDLER(&pCmdHandlers[21], "E1E1", FALSE,  ExecuteRT);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[22], "E1E2", TRUE,  ExecuteST);

	
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[23], "4051", FALSE,  ExecuteRP);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[24], "4151", FALSE,  ExecuteRP);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[25], "4251", FALSE,  ExecuteRP);

	DEF_YDN_CMD_HANDLER(&pCmdHandlers[26], "E181", FALSE,  ExecuteRWSysStatus);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[27], "E180", FALSE,  ExecuteRWSysStatus);
	
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[28], "4050", FALSE,  ExecuteRADR);
    DEF_YDN_CMD_HANDLER(&pCmdHandlers[29], "4150", FALSE,  ExecuteRADR);
    DEF_YDN_CMD_HANDLER(&pCmdHandlers[30], "4250", FALSE,  ExecuteRADR);
    
    DEF_YDN_CMD_HANDLER(&pCmdHandlers[31], "404F", FALSE,  ExecuteRProtocolVer);
    DEF_YDN_CMD_HANDLER(&pCmdHandlers[32], "414F", FALSE,  ExecuteRProtocolVer);
    DEF_YDN_CMD_HANDLER(&pCmdHandlers[33], "424F", FALSE,  ExecuteRProtocolVer);

    DEF_YDN_CMD_HANDLER(&pCmdHandlers[34], "E1EB", FALSE,  ExecuteRWPara);

    DEF_YDN_CMD_HANDLER(&pCmdHandlers[35], "4080", FALSE,  ExecuteACErrLight);
    DEF_YDN_CMD_HANDLER(&pCmdHandlers[36], "E184", FALSE,  ExecuteErrSound);

    DEF_YDN_CMD_HANDLER(&pCmdHandlers[37], "41E2", FALSE, ExecuteRREA);
    DEF_YDN_CMD_HANDLER(&pCmdHandlers[38], "41E3", FALSE, ExecuteRREBarCode);

    DEF_YDN_CMD_HANDLER(&pCmdHandlers[39], "E1FA", FALSE,  ExecuteRSCUPBarCode);

	DEF_YDN_CMD_HANDLER(&pCmdHandlers[40], "E190", FALSE,  ExecuteRRectRedund);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[41], "E191", TRUE,   ExecuteWRectRedund);

	DEF_YDN_CMD_HANDLER(&pCmdHandlers[42], "F1F0", FALSE,  ExecuteRSigValue);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[43], "F1F1", TRUE,   ExecuteWSigValue);

	//zwh:���Ӳ���ʹ�õ�ydn23Э��
	//404D,404E,��ȡ������ʱ��
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[44], "404D", FALSE,  ExecuteRT);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[45], "404E", TRUE,  ExecuteWT);
	//���ü���
	//DEF_YDN_CMD_HANDLER(&pCmdHandlers[46], "F1F2", TRUE,  ExecuteWKey);
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[46], "E1E4", TRUE,  ExecuteWKey);
	//����ʷ�澯
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[47], "E1EA", TRUE,  ExecuteRHisEvent);
	//�����ʷ�澯
	DEF_YDN_CMD_HANDLER(&pCmdHandlers[48], "E1F0", TRUE,  ExecuteCSomeAlarm);

	DEF_YDN_CMD_HANDLER(&pCmdHandlers[49], "41E4", FALSE, ExecuteRREA);

	return;
}


/*==========================================================================*
 * FUNCTION : *GetCmdHandler
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: YDN_DecodeAndPerform
 * ARGUMENTS: const unsigned char *  szCmdData : 
 * RETURN   : CMD_HANDLER : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-11-14 17:33
 *==========================================================================*/
static YDN_CMD_HANDLER *GetCmdHandler(const unsigned char * szCmdData)
{
	int i;
	char szCmdType[5];
	YDN_CMD_HANDLER *pCmdHandler;

	strncpyz(szCmdType, szCmdData, 5);
	pCmdHandler = g_YDNGlobals.CmdHandlers;
	for (i = 0; i < YDN_CMD_NUM; i++, pCmdHandler++)
	{
		if (strcmp(szCmdType, pCmdHandler->szCmdType) == 0)
		{
			return pCmdHandler;
		}
	}
    //TRACE("\ni is: %d\n", i);
	return NULL;
}

/*==========================================================================*
 * FUNCTION : YDN_DecodeAndPerform
 * PURPOSE  : 
 * CALLS    : GetCmdHandler
 *			  VerifyExecutable
 * CALLED BY: Static Machine functions
 * ARGUMENTS: YDN_BASIC_ARGS         *pThis       : 
 *            const unsigned char   *szCmdData    : 
 * RETURN   : void : 
 * COMMENTS : length of szCmdRespBuff is YDN_RESP_SIZE
 * CREATOR  : HANTao                   DATE: 2006-05-13 17:04
 *==========================================================================*/
int YDN_DecodeAndPerform(YDN_BASIC_ARGS *pThis,
						      unsigned char *szCmdData)
{
	YDN_CMD_HANDLER *pCmdHandler;
	//unsigned char *szCmdRespBuff;
	int iRTN, i;

	/* 1.Get Cmd Handler */
	
	pCmdHandler = GetCmdHandler(szCmdData);
	
	if (pCmdHandler == NULL)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
		    TRACE_YDN_TIPS("Get Command Handler failed");
        #endif //_DEBUG_YDN_CMD_HANDLER
        for(i = 0; i < 4; i++)
            *(pThis->szCmdRespBuff + i) = '0';

		return CMDERR;
	}

	
#ifdef _DEBUG_YDN_CMD_HANDLER
	{
		char *szFlagText;
		if (pCmdHandler->bIsWriteCommand)
		{
			szFlagText = "TRUE";
		}
		else
		{
			szFlagText = "FALSE";
		}

		TRACE_YDN_TIPS("Get Cmd Handler");
		TRACE("Cmd Handler info:\n");
		TRACE("\tCmd Type: %s\n", pCmdHandler->szCmdType);
		TRACE("\tIs Write Cmd: %s\n", szFlagText);
	}
#endif //_DEBUG_YDN_CMD_HANDLER

	/* 3.excute it */
	memset(pThis->szCmdRespBuff, '\0', sizeof(char)*YDN_RESP_SIZE);
	iRTN = pCmdHandler->funExecute(pThis, szCmdData, pThis->szCmdRespBuff);

	return iRTN;
}

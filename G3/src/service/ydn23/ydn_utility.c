/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : ydn_utility.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "ydn.h"

/* used to init command handlers */
#define DEF_YDN_PACKDATA_HANDLER(_p, _szCmdType, _pRespDataAddr) \
          ((_p)->szCmdType = (_szCmdType), \
		   (_p)->pPackData = _pRespDataAddr)
#define YDN_TASK_PACKDATA "YDN PackData Process"		   
#define LOG_YDN_PACKDATA_I(_szLogText)  LOG_YDN_I(YDN_TASK_PACKDATA, _szLogText)
/*==========================================================================*
 * FUNCTION : YDN_GetADR
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BYTE : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-02 14:43
 *==========================================================================*/
__INLINE BYTE YDN_GetADR(void)
{
	YDN_COMMON_CONFIG *pCommonCfg;

	pCommonCfg = &g_YDNGlobals.CommonConfig;

	return pCommonCfg->byADR;
}


/*==========================================================================*
 * FUNCTION : YDN_CFG_CheckBase
 * PURPOSE  : basic function for check a null-ended string, used by other 
 *            specifically realized check functions
 * CALLS    : 
 * CALLED BY: specifically realized check functions, such as YDN_CheckPhoneNumber
 * ARGUMENTS: const char  *szObject : 
 *            LEGAL_CHR   fnCmp     : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 11:18
 *==========================================================================*/
BOOL YDN_CFG_CheckBase(const char *szObject, LEGAL_CHR fnCmp)
{
	const char *p;

	if (szObject == NULL)
	{
		return FALSE;
	}

	p = szObject;
	for (; p != NULL; p++)
	{
		if (*p == '\0')
		{
			return TRUE;
		}

		if (!fnCmp(*p))
		{
			return FALSE;
		}

	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : YDN_CFG_CheckBase
 * PURPOSE  : basic function for check a null-ended string, used by other 
 *            specifically realized check functions
 * CALLS    : 
 * CALLED BY: specifically realized check functions, such as YDN_CheckPhoneNumber
 * ARGUMENTS: const char  *szObject : 
 *            LEGAL_CHR   fnCmp     : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 11:18
 *==========================================================================*/
BOOL YDN_CFG_Check(const char *szObject, LEGAL_CHR fnCmp)
{
	const char *p;

	if (szObject == NULL || *szObject == '\0')
	//if (szObject == NULL)
	{
		return FALSE;
	}

	p = szObject;
	for (; p != NULL; p++)
	{
		if (*p == '\0')
		{
			return TRUE;
		}

		if (!fnCmp(*p))
		{
			return FALSE;
		}

	}

	return FALSE;
}



/*==========================================================================*
 * FUNCTION : YDN_CheckPhoneNumber
 * PURPOSE  : for checking phone number null-ended string
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szPhoneNumber : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 20:25
 *==========================================================================*/
static BOOL IsPhoneNumber(char c)
{
	return  ((c >= '0' && c <= '9') ||
		 c == ',' || c == '-');
}
	
BOOL YDN_CheckPhoneNumber(const char *szPhoneNumber)
{
	//return YDN_CFG_Check(szPhoneNumber, IsPhoneNumber);
	return YDN_CFG_CheckBase(szPhoneNumber, IsPhoneNumber);
}
	

/*==========================================================================*
 * FUNCTION : YDN_CheckIPAddress
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szIPAddress : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 20:49
 *==========================================================================*/
static BOOL IsIPAddress(char c)
{
	return  ((c >= '0' && c <= '9') ||
		c == '.' || c == ':');
}

BOOL YDN_CheckIPAddress(const char *szIPAddress)
{
	return YDN_CFG_CheckBase(szIPAddress, IsIPAddress);
}


/*==========================================================================*
 * FUNCTION : YDN_CheckBautrateCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szBautrateCfg : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 21:49
 *==========================================================================*/
static BOOL IsBautrateCfg(char c)
{
	return  ((c >= '0' && c <= '9') ||
		c == ',' || c == 'n' || c == 'N');
}

BOOL YDN_CheckBautrateCfg(const char *szBautrateCfg)
{
	int i, iLen, count = 0;

	if (!YDN_CFG_CheckBase(szBautrateCfg, IsBautrateCfg))
	{
		return FALSE;
	}

	iLen = (int)strlen(szBautrateCfg);
	for (i = 0; i < iLen; i++)
	{
		if (szBautrateCfg[i] == ',')
		{
			count++;
		}
	}

	if (count != 3)
	{
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : YDN_CheckModemCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szBautrateCfg : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 21:55
 *==========================================================================*/
BOOL YDN_CheckModemCfg(const char *szModemCfg)
{
	char szSubField[30];

	szModemCfg = Cfg_SplitString((char *)szModemCfg, szSubField, sizeof(szSubField), ':');
	if (!YDN_CheckBautrateCfg(szSubField))
	{
		return FALSE;
	}

	Cfg_SplitString((char *)szModemCfg, szSubField, sizeof(szSubField), ':');
	if (!YDN_CheckPhoneNumber(szSubField))
	{
		return FALSE;
	}
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : YDN_AsciiHexToChar
 * PURPOSE  : get value from ascii hex. eg: "1A" = 1*16+10 =26
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-06-12 19:24
 *==========================================================================*/
BYTE YDN_AsciiHexToChar(IN const unsigned char *pStr)
{
	BYTE value;
	unsigned char c;

	ASSERT(pStr);

	c = pStr[0];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = c * 0x10;

	c = pStr[1];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value += c;

	return value;
}

/*==========================================================================*
 * FUNCTION : YDN_ThreeAsciiHexToChar
 * PURPOSE  : get value from ascii hex. eg: "11A" = 1*16*16 + 1*16+10 =282
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-06-12 19:24
 *==========================================================================*/
BYTE YDN_ThreeAsciiHexToChar(IN const unsigned char *pStr)
{
	BYTE value;
	unsigned char c;

	ASSERT(pStr);

	c = pStr[0];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = c * 0x100;

	c = pStr[1];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = value + c * 0x10;

	c = pStr[2];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value += c;

	return value;
}

__INLINE unsigned char	HexToAscii(DWORD Input)
{
	if(Input > 9)
	{
		return Input + 0x37;					/*A - F*/
	}
	else
	{
		return Input + 0x30;					/*0 - 9*/
	}
}

__INLINE void	HexToTwoAsciiData(DWORD Input, unsigned char *szOutData)
{
	*szOutData = HexToAscii(Input >> 4);
	*(szOutData + 1) = HexToAscii(Input & 0x0f);
	return;
}

void	HexToFourAsciiData(DWORD Input, unsigned char *szOutData)
{
	*szOutData = HexToAscii(Input >> 12);
	*(szOutData + 1) = HexToAscii((Input & 0x0f00) >> 8);
	*(szOutData + 2) = HexToAscii((Input & 0x00f0) >> 4);
	*(szOutData + 3) = HexToAscii(Input & 0x000f);
	
}
/*==========================================================================*
 * FUNCTION : YDN_IntToAscii
 * PURPOSE  : get value from ascii hex. eg: "1A" = 1*16+10 =26
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-06-12 19:24
 *==========================================================================*/
void YDN_IntToAscii(IN DWORD dValue, OUT unsigned char *cStr)
{
	DWORD cValue[2];

	ASSERT(cStr);

    cValue[1] = dValue % 100;
    cValue[0] = dValue / 100;

    *cStr = HexToAscii(cValue[0] >> 4);
    *(cStr + 1) = HexToAscii(cValue[0] & 0x0f);

    *(cStr + 2)  = HexToAscii(cValue[1] >> 4);
    *(cStr + 3)  = HexToAscii(cValue[1] & 0x0f);
    
    *(cStr + 4)  = '\0';
}

/*==========================================================================*
 * FUNCTION : YDN_CharToInt
 * PURPOSE  : get value from char. eg: "11" = 1*10+1 =11
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-09-20 09:24
 *==========================================================================*/
unsigned int YDN_CharToInt(IN const unsigned char *pStr, int iCNum)
{
	ASSERT(pStr);

	unsigned int iValue = 0;
	int iPower10 = 1;
	int i, j;
	unsigned int pValue[10]; 
	
	for(i = 0; i < iCNum; i++)
		pValue[i] = *(pStr + i) - '0';

	for(i = 0; i < iCNum; i++)
	{
		for(j = 1; j < iCNum - i; j++ )
			iPower10 *= 10;

		pValue[i] *= iPower10;
		iValue += pValue[i]; 
		iPower10 = 1;
	}

	return iValue;

}



/*==========================================================================*
 * FUNCTION : YDN_IsStrAsciiHex
 * PURPOSE  : assistant functions
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const unsigned char  *pStr : 
 *            int                  iLen  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-12 17:24
 *==========================================================================*/
BOOL YDN_IsStrAsciiHex(const unsigned char *pStr, int iLen)
{
	int i;
	
	for (i = 0; i < iLen; i++)
	{
		if ((pStr[i] >= '0' && pStr[i] <= '9') ||
			(pStr[i] >= 'A' && pStr[i] <= 'F'))
		{
			continue;
		}
		
		return FALSE;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : YDN_GetUnprintableChr
 * PURPOSE  : for trace
 * CALLS    : 
 * CALLED BY: YDN_PrintEvent
 * ARGUMENTS: IN unsigned char  chr   : 
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-25 14:30
 *==========================================================================*/
unsigned char *YDN_GetUnprintableChr(IN unsigned char chr)
{
	switch (chr)
	{
	case 0x06:
		return "<ACK>";

	case 0x15:
		return "<NAK>";

	case 0x04:
		return "<EOT>";

	case 0x05:
		return "<ENQ>";

	case 0x01:
		return "<SOH>";

	case 0x02:
		return "<STX>";

	case 0x03:
		return "<ETX>";

	case 0x0D:
		return "<EOI>";

	default:
		return "<NOT EXIST>";
	}
}


/*==========================================================================*
 * FUNCTION : YDN_PrintEvent
 * PURPOSE  : print YDN Event info(used for debug)
 * CALLS    : YDN_GetUnprintableChr
 * CALLED BY: 
 * ARGUMENTS: YDN_EVENT  *pEvent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-24 10:33
 *==========================================================================*/
void YDN_PrintEvent(YDN_EVENT *pEvent)
{
#ifdef _SHOW_YDN_EVENT_INFO
	int i;
	unsigned char chr;
	char szEventType[50];

	switch (pEvent->iEventType)
	{
	case YDN_FRAME_EVENT:
		strncpy(szEventType, "YDN_FRAME_EVENT", 50);
		break;

	case YDN_CONNECTED_EVENT:
		strncpy(szEventType, "YDN_CONNECTED_EVENT", 50);
		break;

	case YDN_CONNECT_FAILED_EVENT:
		strncpy(szEventType, "YDN_CONNECT_FAILED_EVENT", 50);
		break;

	case YDN_DISCONNECTED_EVENT:
		strncpy(szEventType, "YDN_DISCONNECTED_EVENT", 50);
		break;

	case YDN_TIMEOUT_EVENT:
		strncpy(szEventType, "YDN_TIMEOUT_EVENT", 50);
		break;

	default:
		strncpy(szEventType, "ERROR: not defined event type!", 50);
	}

	TRACE("YDN Event Info\n");
	TRACE("Event Type: %s\n", szEventType);
	TRACE("Event Data length: %d\n", pEvent->iDataLength);

	TRACE("Event Data(Hex format):");
	for (i = 0; i < pEvent->iDataLength; i++)
	{
		TRACE("%02X ", pEvent->sData[i]);
	}
	TRACE("\n");

	TRACE("Event Data(Text format):");
	for (i = 0; i < pEvent->iDataLength; i++)
	{
		chr = pEvent->sData[i];
		if (chr < 0x21 || chr > 0X7E)
		{
			TRACE("%s", YDN_GetUnprintableChr(chr));
		}
		else
		{
			TRACE("%c", chr);
		}
	}
	TRACE("\n");
		
	TRACE("Event Flags: \nSkip Falg: %d	Dummy Flag: %d\n", 
		pEvent->bSkipFlag, pEvent->bDummyFlag);

#endif //_DEBUG_YDN_LINKLAYER
    UNUSED(pEvent);
	return;
}


/*==========================================================================*
 * FUNCTION : YDN_PrintState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iState : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-27 09:45
 *==========================================================================*/
void YDN_PrintState(int iState)
{
#ifdef _SHOW_YDN_CUR_STATE
	char *szStateName;

	if (g_YDNGlobals.CommonConfig.iProtocolType == YDN)  //YDN State machine
	{
		switch (iState)
		{
		case YDN_IDLE:
			szStateName = "YDN_IDLE";
			break;
/*
		case YDN_WAIT_FOR_POLL:
			szStateName = "YDN_WAIT_FOR_POLL";
			break;

		case YDN_WAIT_FOR_RESP_ACK:
			szStateName = "YDN_WAIT_FOR_RESP_ACK";
			break;

		case YDN_SEND_CALLBACK:
			szStateName = "YDN_SEND_CALLBACK";
			break;

		case YDN_WAIT_FOR_CALLBACK_ACK:
			szStateName = "YDN_WAIT_FOR_CALLBACK_ACK";
			break;
*/
		case YDN_SEND_ALARM:
			szStateName = "YDN_SEND_ALARM";
			break;
/*
		case YDN_WAIT_FOR_ALARM_ACK:
			szStateName = "YDN_WAIT_FOR_ALARM_ACK";
			break;
*/
		case YDN_STATE_MACHINE_QUIT:
			szStateName = "STATE_MACHINE_QUIT";
			break;

		default:
			szStateName = "Error YDN State Name";
		}
	}
/*
	else
	{
		switch (iState)
		{
		case SOC_OFF:
			szStateName = "SOC_OFF";
			break;

		case SOC_SEND_ENQ:
			szStateName = "SOC_SEND_ENQ";
			break;

		case SOC_WAIT_DLE0:
			szStateName = "SOC_WAIT_DLE0";
			break;

		case SOC_ALARM_REPORT:
			szStateName = "SOC_ALARM_REPORT";
			break;

		case SOC_WAIT_CMD:
			szStateName = "SOC_WAIT_CMD";
			break;

		case SOC_WAIT_ACK:
			szStateName = "SOC_WAIT_ACK";
			break;

		case STATE_MACHINE_QUIT:
			szStateName = "STATE_MACHINE_QUIT";
			break;

		default:
			szStateName = "Error SOC State Name";

		}
	}
*/
	TRACE("++++++++++++++++++++++++++++++++++++++++++++++\n");
	TRACE("+  Current State: %-25s  +\n", szStateName);
	TRACE("++++++++++++++++++++++++++++++++++++++++++++++\n");
	
	return;
#endif //_SHOW_YDN_CUR_STATE

	UNUSED(iState);
	return;
}

/*==========================================================================*
 * FUNCTION : YDN_GetReportMsg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN BOOL      bAlarm		: TRUE  for SendAlarm 
 *										  FALSE for SendCallback
 *		      IN OUT char  *szReportMsg : 
 *            IN int       iBufLen      : 
 * RETURN   : int : 0 for success, 
 *					1 for call DXI to get site name failed,
 *					2 for site name is too long
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-06 19:18
 *==========================================================================*/
int YDN_GetReportMsg(IN BOOL bAlarm, 
					 IN OUT char *szReportMsg, 
					 IN int iBufLen)
{
	UNUSED(iBufLen);
	size_t iLen;
	int nError, nInterfaceType, nVarID, nVarSubID, nBufLen;
	LANG_TEXT *pLangInfo;
	char szSiteName[64];

	BYTE  byADR;

	ASSERT(iBufLen > 70);

	/* 1.get site name */
	nInterfaceType = VAR_ACU_PUBLIC_CONFIG;
	nVarID = SITE_NAME;
	nVarSubID = 0;
	pLangInfo = NULL;
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pLangInfo,			
		0);

	if (nError != ERR_DXI_OK)
	{
		szReportMsg[0] = '\0';
		return 1;
	}

	strncpyz(szSiteName, pLangInfo->pFullName[0], sizeof(szSiteName));

	/* 2.get CCID */
	byADR = g_YDNGlobals.CommonConfig.byADR;

	/* 3.now build the string */
	if (bAlarm)  //called by OnSendAlarm state
	{
		sprintf(szReportMsg, "%s!%02X!E*", szSiteName, byADR);
	}
	else  //called by OnSendCallback state
	{
		sprintf(szReportMsg, "%s!%02X!C*", szSiteName, byADR);
	}

	/* 4.check buffer length */
	iLen = strlen(pLangInfo->pFullName[0]);
	if (iLen >= 64)
	{
		return 2;
	}

	return 0;

}

/*==========================================================================*
 * FUNCTION : PrintCommonCfg
 * PURPOSE  : Print YDN Common Config info
 * CALLS    : 
 * CALLED BY: PrintYDNConfigInfo
 * ARGUMENTS: YDN_COMMON_CONFIG *pConfig : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-24 10:39
 *==========================================================================*/
void YDN_PrintCommonCfg(YDN_COMMON_CONFIG *pConfig)
{
	char *pText;
	int iTmp, i;

	printf("************************************************\n");
	printf("*****          YDN Common Config         *******\n");
	printf("************************************************\n");

	/* protocol type */
	iTmp = pConfig->iProtocolType;
	switch (iTmp)
	{
	case 1:
		pText = "YDN23";
		break;
		
	case 2:
		pText = "RSOC";
		break;

	case 3:
		pText = "";
		break;

	default:
		pText = "ERROR! not such protocol type!";
	}
	TRACE("Protocol Type: %s\n", pText);

	/* ccid and socid */
	iTmp = pConfig->byADR;
	TRACE("byADR is: %d\n", iTmp);

	/* media */
	switch (pConfig->iMediaType)
	{
	case 0:
		pText = "Leased Line";
		break;

	case 1:
		pText = "Modem";
		break;

	case 2:
		pText = "TCPIP";
		break;

	default:
		pText = "ERROR, not such media.\n";
	}

	TRACE("Operation media is: %s\n", pText);

	/* attemps and elaps */
	iTmp = pConfig->iMaxAttempts;
	TRACE("Max attemps: %d\n", iTmp);
	iTmp = pConfig->iAttemptElapse;
	TRACE("Elapse time is: %d\n", iTmp);

	/* phone numbers */
	for (i = 0; i < YDN_ALARM_REPORT_NUM; i++)
	{
		TRACE("Alarm report number %d is: %s\n", 
			i + 1, pConfig->szAlarmReportPhoneNumber[i]);
	}

	return;
}

/*==========================================================================*
 * FUNCTION : YDN_ClearEventQueue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hq       : 
 *            BOOL    bDestroy : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-21 15:11
 *==========================================================================*/
void YDN_ClearEventQueue(HANDLE hq, BOOL bDestroy)
{
	int i, iCount, iRet;
	YDN_EVENT *pEvent;

	if (hq)
	{
		iCount = Queue_GetCount(hq, NULL);

		/* clear memory */
		for (i = 0; i < iCount; i++)
		{
			iRet = Queue_Get(hq, &pEvent, FALSE, 1000);

			if (iRet == ERR_OK)
			{
				DELETE_YDN_EVENT(pEvent);
			}
		}

		/* destroy queue */
		if (bDestroy)
		{
			Queue_Destroy(hq);
		}
	}
}

/*==========================================================================*
 * FUNCTION : YDN_PrintfStr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hq       : 
 *            BOOL    bDestroy : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-21 15:11
 *==========================================================================*/
void YDN_PrintfStr(int nLen, char *pStr)
{
	int i;
	for(i = 0; i < nLen; i++, pStr++)
	{
		TRACE("%02x",*pStr);
	}
	TRACE("\n");
}

/*==========================================================================*
 * FUNCTION : PackReps
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : char * : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-8 10:43
 *==========================================================================*/
void PackReps(unsigned char *pStr)
{
	int nLen;	
	unsigned char *pnStr = "0"; 

	nLen = strlen(pnStr);
	
	memcpy(pStr,pnStr, (size_t)nLen);
}

unsigned char *YDN_BuildDataInfo(int iDataLen, 
								 IN OUT unsigned char *szOutData)
{
    WORD wLength, Sum;

    wLength = iDataLen * 2;
	Sum = (-(wLength + (wLength >> 4) + (wLength >> 8))) << 12;
	wLength = (wLength & 0xFFF) | Sum;
	HexToFourAsciiData(wLength, szOutData);

	*(szOutData + 4 + iDataLen * 2) = '\0';
	
	return szOutData;
   
}
/*==========================================================================*
 * FUNCTION : GetEquipID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iEquipTypeID : 
 * RETURN   : pCurEquip->iEquipID : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-19 11:29
 *==========================================================================*/
int GetEquipIDOrder(int iEquipTypeID)
{
	int i, iEquipID;
	EQUIP_INFO *pCurEquip;
	pCurEquip = g_YDNGlobals.pEquipInfo;

	for (i = 0; i <  g_YDNGlobals.iEquipNum; i++, pCurEquip++)
	{
		if(iEquipTypeID == g_YDNGlobals.iEquipIDOrder[i][1])
		{
		    iEquipID = pCurEquip->iEquipID;
			break;
		}
	}

	/* not found */
	if (i == g_YDNGlobals.iEquipNum)
	{
		i = 0;
		//TRACE("\n i is: %d\n", i);
	}

	return i;
}

/*==========================================================================*
* FUNCTION : GetEquipID
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iEquipTypeID : 
* RETURN   : pCurEquip->iEquipID : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2006-05-19 11:29
*==========================================================================*/
BOOL YDN_GetEquipIDOrder(int iEquipTypeID,int* iEquipNum)
{

	int i, iEquipID;
	EQUIP_INFO *pCurEquip;
	pCurEquip = g_YDNGlobals.pEquipInfo;
	*iEquipNum = MAX_EQUIP_NUM;

	for (i = 0; i <  g_YDNGlobals.iEquipNum; i++, pCurEquip++)
	{
		if(iEquipTypeID == g_YDNGlobals.iEquipIDOrder[i][1])
		{
			iEquipID = pCurEquip->iEquipID;
			break;
		}
	}

	/* not found */
	if (i == g_YDNGlobals.iEquipNum)
	{
		return FALSE;

	}

	*iEquipNum =i;
	return TRUE;
}

/*==========================================================================*
* FUNCTION : GetEquipID
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iEquipTypeID : 
* RETURN   : pCurEquip->iEquipID : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2006-05-19 11:29
*==========================================================================*/
BOOL YDN_GetEquiplistIDOrder(int iEquipID,int* iEquipNum)
{
	int i;

	for (i = 0; i < g_YDNGlobals.iEquipNum; i++)
	{
		if(iEquipID == g_YDNGlobals.iEquipIDOrder[i][0])
		{
			break;
		}
	}

	/* not found */
	if (i == g_YDNGlobals.iEquipNum)
	{
		return FALSE;
	}

	*iEquipNum =i;
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : YDN_InitPackDataHander
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: InitYDNGlobals
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2004-12-29 14:39
 *==========================================================================*/
void YDN_InitPackDataHander(void)
{
	YDN_PACKDATA_HANDLER *pPackDataHandlers;

	pPackDataHandlers = g_YDNGlobals.PackDataHandlers;

	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[0], "4041", g_RespData.szRespData1[0]);
	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[1], "4043", g_RespData.szRespData1[1]);
	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[2], "4044", g_RespData.szRespData1[2]);

	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[3], "4241", g_RespData.szRespData1[3]);
	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[4], "4244", g_RespData.szRespData1[4]);

	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[5], "4141", g_RespData.szRespData1[5]);
	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[6], "4143", g_RespData.szRespData1[6]);
	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[7], "4144", g_RespData.szRespData1[7]);
	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[8], "41E2", g_RespData.szRespData1[8]);
	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[9], "4180",  g_RespData.szRespData1[9]);
	DEF_YDN_PACKDATA_HANDLER(&pPackDataHandlers[10], "41E4",  g_RespData.szRespData1[10]);
	
	return;
}
/*==========================================================================*
 * FUNCTION : PackRACA
 * PURPOSE  : Package AC distribution analog data 
 * CALLS    : 
 *			  
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-12 16:02
 *==========================================================================*/
static void PackRACA(int iCID1, int iCID2)
{
	int i, j, k, iDataLen;
	int iData[4] = {0,0,0,0};
	int iCurrMeasMeth = 0;
	unsigned char *szOutData, pCurrMeasMeth[2];
	//unsigned char *pCurrMeasMeth = 0;
	char logText[YDN_LOG_TEXT_LEN];
	//WORD wLength, Sum;

	int  iEquipIDOrder, iEquipID, iCommGrp;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;

    HANDLE hLocalThread;
	/* not used the param */
    BOOL bDxiRTN;

    hLocalThread = RunThread_GetId(NULL);

	memset(g_RespData.szRespData2[0], '\0', MAX_YDNFRAME_LEN);

	/* 2.get the block */
	//iCommGrp = (int)YDN_AsciiHexToChar(szInData + 4);
	iCommGrp = ALL_AC_OR_DC;
	
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_PACKDATA_I(logText);

		memset(g_RespData.szRespData1[0], '\0', MAX_YDNFRAME_LEN);
		return;
	}
    
#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will read data from a Block");
	TRACE("the Block Info\n");
	TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
#endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    szOutData = g_RespData.szRespData2[0];
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    // 2.Package the DataFlag
    
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
    {
	    memset(g_RespData.szRespData1[0], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,DATAFLAG,0);
	//HexToTwoAsciiData(0x11, szOutData);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    //iEquipTypeID = pCurEntry->iEquipType;
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
    {
	    memset(g_RespData.szRespData1[0], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
	if(iData[0] == 0)
	{
	    memset(g_RespData.szRespData1[0], '\0', MAX_YDNFRAME_LEN);
		return;
	}
    iDataLen++;
    szOutData += 2;

	pCurEntry++;//point to ACInputNum

	//find out the first place of the equiptype
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
	{
		memset(g_RespData.szRespData1[0], '\0', MAX_YDNFRAME_LEN);
		return;
	}
	pFixedPos = pCurEntry;
        
    for(i = 0; i < iData[0]; i++, iEquipIDOrder++)
    {
	   RunThread_Heartbeat(hLocalThread);
    
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];//find out the equipID orderly

	   bDxiRTN = GetSCUPSingleSigValues(FALSE, 2, 9, iEquipID, pCurrMeasMeth);
	   if(bDxiRTN == FALSE)							//for equipment is not exist by ZTZ
	   {
		   iCurrMeasMeth = 0;
	   }
	   else
	   {
		    iCurrMeasMeth = (int)YDN_AsciiHexToChar(pCurrMeasMeth);
	   }
	  // TRACE("\niCurrMeasMeth = %d\n", iCurrMeasMeth);
	  
       GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
       //pCurEntry++;
       iDataLen++;
       iData[1] = (int)YDN_AsciiHexToChar(szOutData);//ACInputNum 
       szOutData += 2;
	   iData[2] = pCurEntry->iFlag1;//fixed Num

       for(j=0; j< iData[1]; j++)
       {
          for(k = 0; k < iData[2]; k++)
          {
			  pCurEntry++;
              GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
              szOutData += 8;
              iDataLen += 4;
          }
		  pCurEntry++;
          iData[3] = pCurEntry->iFlag1;//Defined Num
          //sprintf(szOutData, "%02X", iData[3]);
		  HexToTwoAsciiData((unsigned char)iData[3], szOutData);
          //pCurEntry++;
          iDataLen++;
          szOutData += 2;
          for(k = 0; k < iData[3]; k++)
          {
			  pCurEntry++;
              GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
              szOutData += 8;
              iDataLen += 4;
          }
       }
	   pCurEntry = pCurEntry + (AC_MAX_INPUT_ROUTE - iData[1])*(iData[2] + iData[3] + 1) + 1;

       if(iCurrMeasMeth == 0)
       {
           for(k = 0; k < 24; k++, szOutData++)
		   {
           	  *szOutData = 0x20;
		   }
		   iDataLen += 12;
       }
       else if(iCurrMeasMeth == 1)
       {
		   /*
           GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData);
           szOutData += 8;
           iDataLen += 4;
           for(k = 0; k < 16; k++, szOutData++)
		   {
           	  *szOutData = 0x20;
		   }
		   iDataLen += 8;
		   */
		   for(k = 0; k < 8; k++, szOutData++)
		   {
           	  *szOutData = 0x20;
		   }
		   iDataLen += 4;
		   pCurEntry++;

		   GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           szOutData += 8;
           iDataLen += 4;
		   //pCurEntry++;
           
		   for(k = 0; k < 8; k++, szOutData++)
		   {
           	  *szOutData = 0x20;
		   }
		   iDataLen += 4;
       }
       else
       {
           for(k = 0; k < 3; k++, pCurEntry++)
           {
              GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
              szOutData += 8;
              iDataLen += 4;
           }
       }

	   pCurEntry = pFixedPos;
    }

    // 4.calculate LCHKSUM and package it 
	szOutData = g_RespData.szRespData2[0];
	YDN_BuildDataInfo(iDataLen, szOutData);

	memcpy(g_RespData.szRespData1[0],g_RespData.szRespData2[0], MAX_YDNFRAME_LEN);

	Sleep(50);
}

/*==========================================================================*
 * FUNCTION : PackRACS
 * PURPOSE  : Package AC distribution digital data  
 * CALLS    : 
 *			  
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-12 16:02
 *==========================================================================*/
static void PackRACS(int iCID1, int iCID2)
{
	int i, j, iDataLen;
	int iData[3] = {0};
	unsigned char *szOutData;
	//unsigned char DataFlag[2] = {'0', '0'};
	char logText[YDN_LOG_TEXT_LEN];
    //BOOL bRTN;
    
	int  iEquipIDOrder, iEquipID, iCommGrp;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;
    HANDLE hLocalThread;

    hLocalThread = RunThread_GetId(NULL);

	memset(g_RespData.szRespData2[1], '\0', MAX_YDNFRAME_LEN);

	/* 2.get the block */
	iCommGrp = ALL_AC_OR_DC;
	
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_PACKDATA_I(logText);

		memset(g_RespData.szRespData1[1], '\0', MAX_YDNFRAME_LEN);
		return;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will read data from a Block");
	TRACE("the Block Info\n");
	TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
#endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    szOutData = g_RespData.szRespData2[1];
     for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
     
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    // 2.Package the DataFlag
    
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
    {
	    memset(g_RespData.szRespData1[1], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,DATAFLAG,0);
    /*
    bRTN = SetYDNSigValues(FALSE, iEquipID, 1, pCurEntry, DataFlag);
	if(bRTN)
	{
        #ifdef _DEBUG_YDN_CMD_HANDLER
	       TRACE_YDN_TIPS("clear dataflag has been excuted ok!");
	       TRACE("Reply Data:\n");
	       TRACE("\t%s\n", DataFlag);
        #endif //_DEBUG_YDN_CMD_HANDLER
	}
	*/
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
    {
	    memset(g_RespData.szRespData1[1], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
	if(iData[0] == 0)
	{
	    memset(g_RespData.szRespData1[1], '\0', MAX_YDNFRAME_LEN);
		return;
	}
    iDataLen++;
    szOutData += 2;

	pCurEntry++;
   
	//find out the first place of the equiptype
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
	{
		memset(g_RespData.szRespData1[1], '\0', MAX_YDNFRAME_LEN);
		return;
	}
	pFixedPos = pCurEntry;
    
    for(i = 0; i < iData[0]; i++, iEquipIDOrder++)
    {
       RunThread_Heartbeat(hLocalThread);
       
	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];//find out the equipID orderly

       GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
       pCurEntry++;
       iDataLen++;
       iData[1] = (int)YDN_AsciiHexToChar(szOutData);//OutputBreakerNum
       szOutData += 2;
       for(j = 0; j < iData[1]; j++, pCurEntry++)
       {
           GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           szOutData = ExchangeYDNSigValue(szOutData, pCurEntry);
           szOutData += 2;
           iDataLen++;
	   }
	   pCurEntry = pCurEntry + (AC_MAX_OUTPUT_BREAKER - iData[1]);//full config minus real config

       iData[2] = pCurEntry->iFlag1;//Defined Num
       sprintf(szOutData, "%02X", iData[2]);
       pCurEntry++;
       iDataLen++;
	   szOutData += 2;
       for(j = 0; j < iData[2]; j++, pCurEntry++)
       {
           GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           szOutData = ExchangeYDNSigValue(szOutData, pCurEntry);
           szOutData += 2;
           iDataLen++;
       }

	   pCurEntry = pFixedPos;
    }
    // 4.calculate LCHKSUM and package it 
	szOutData = g_RespData.szRespData2[1];
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

	memcpy(g_RespData.szRespData1[1],g_RespData.szRespData2[1], MAX_YDNFRAME_LEN);

	Sleep(50);
}

/*==========================================================================*
 * FUNCTION : PackRACAL
 * PURPOSE  : Package AC distribution alarm data  
 * CALLS    : 
 *			  
 * CALLED BY: PackData
 * ARGUMENTS:  
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-12 16:02
 *==========================================================================*/
static void PackRACAL(int iCID1, int iCID2)
{
	int i, j, k, m, iDataLen;
	int iData[5] = {0};
	int iMainInputNo = 0;
	unsigned char *szOutData, pMainInputNo[2];
	//unsigned char DataFlag[2] = {'0', '0'};
	char logText[YDN_LOG_TEXT_LEN];

	unsigned char **pAnalogAlarm;
	unsigned char *pTemp;
	//BOOL bRTN;
    //WORD wLength, Sum;
    
	int  iEquipIDOrder, iEquipID, iCommGrp;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;
	/* not used the param */
	HANDLE hLocalThread;

	BOOL bDxiRTN;

    hLocalThread = RunThread_GetId(NULL);

	memset(g_RespData.szRespData2[2], '\0', MAX_YDNFRAME_LEN);

	/* 2.get the block */
	iCommGrp = ALL_AC_OR_DC;
	
	pTypeMap = GetTypeMap(iCID1, iCID2);
	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_PACKDATA_I(logText);

		memset(g_RespData.szRespData1[2], '\0', MAX_YDNFRAME_LEN);
		return;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will read data from a Block");
	TRACE("the Block Info\n");
	TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
#endif //_DEBUG_YDN_CMD_HANDLER
    
    iDataLen = 0;
    szOutData = g_RespData.szRespData2[2];
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    // 2.Package the DataFlag
    
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
    {
	    memset(g_RespData.szRespData1[2], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE, DATAFLAG,0);

	pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
    {
	    memset(g_RespData.szRespData1[2], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];

	
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//ACDNum
	if(iData[0] == 0)
	{
	    memset(g_RespData.szRespData1[2], '\0', MAX_YDNFRAME_LEN);
		return;
	}
    iDataLen++;
    szOutData += 2;

	pCurEntry++;

	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
	{
		memset(g_RespData.szRespData1[2], '\0', MAX_YDNFRAME_LEN);
		return;
	}
	pFixedPos = pCurEntry;
    
    for(i = 0; i < iData[0]; i++, iEquipIDOrder++)
    {
		RunThread_Heartbeat(hLocalThread);

		iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];//find out the equipID orderly

		bDxiRTN = GetSCUPSingleSigValues(FALSE, 0, 62, iEquipID, pMainInputNo);
		if(bDxiRTN == FALSE)							//for equipment is not exist by ZTZ 
		{
			iMainInputNo = 0;//mains input No.
		}
		else
		{
			iMainInputNo = (int)YDN_AsciiHexToChar(pMainInputNo);//mains input No.
		}
		
		GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
		iData[1] = (int)YDN_AsciiHexToChar(szOutData);//ACInputNum
		iData[2] = pCurEntry->iFlag1;//fixed Num
		pCurEntry++;
		iDataLen++;
		szOutData += 2;

		for(j = 1; j <= iData[1]; j++)
		{
			pTemp = szOutData;
			pAnalogAlarm = &pTemp;

			for(k = 0; k < iData[2]; k++)
			{
				GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
				ExchangeYDNSigValue(szOutData, pCurEntry);
				pCurEntry++;
				szOutData += 2;
				iDataLen++;
			}

			iData[3] = (int)YDN_AsciiHexToChar(szOutData - 2);//FuseNum

			for(k = 0; k < iData[3]; k++)
			{
				GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
				ExchangeYDNSigValue(szOutData, pCurEntry);
				pCurEntry++;
				szOutData += 2;
				iDataLen++;
			}

			pCurEntry = pCurEntry + (AC_MAX_FUSE_NUM - iData[3]);

			iData[4] = pCurEntry->iFlag1;//Defined Num
			sprintf(szOutData, "%02X", iData[4]);
			pCurEntry++;
			iDataLen++;
			szOutData += 2;
			
			for(k = 0; k < iData[4]; k++)
			{
				GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);

				if(k == 6)
				{
					bACDNoRespFlag = (int)YDN_AsciiHexToChar(szOutData);

					if(bACDNoRespFlag)
					{
						for(m = 0; m < 8; m++)
						{
							*(*pAnalogAlarm + m) = 0x30;
						}
						bACDNoRespFlag = 0;
					}
				}

				ExchangeYDNSigValue(szOutData, pCurEntry);
				pCurEntry++;
				szOutData += 2;
				iDataLen++;
			}
		}
		pCurEntry = pCurEntry + (AC_MAX_INPUT_ROUTE - iData[1]) 
					* (iData[2] + iData[3] + iData[4] + 1);

        for(k = 0; k < 6; k++, szOutData++)
		{
        	*szOutData = 0x20;
		}
		iDataLen += 3;
		
		pCurEntry = pFixedPos;
	}

	// 4.calculate LCHKSUM and package it 
	szOutData = g_RespData.szRespData2[2];
	szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

	memcpy(g_RespData.szRespData1[2],g_RespData.szRespData2[2], MAX_YDNFRAME_LEN);

	Sleep(50);
}

#define	TEMP_NUM_SIG_ID		12
#define	MAX_TOTAL_BATT_NUM	20
/*==========================================================================*
 * FUNCTION : PackRDCA
 * PURPOSE  : Package data for the command of reading DC distribution analog. 
 * CALLS    : 
 *			  
 * CALLED BY: PackData
 * ARGUMENTS:  
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-12 16:02
 *==========================================================================*/
static void PackRDCA(int iCID1, int iCID2)
{
	int						i, k, j, l, iDataLen;
	int						iData[3], iBattNum[10];
	int						iTotalBattNum1 = 0;
	int						iTotalBattNum2 = 0;
	unsigned char*			szOutData, pTempNum[2];
	char					logText[YDN_LOG_TEXT_LEN];

	int						iEquipIDOrder[2], iEquipID[2], iTempNum;
    YDN_MAPENTRIES_INFO*	pCurEntry, *pFixedPos, *pFixedBattPos;
    YDN_TYPE_MAP_INFO*		pTypeMap;
	HANDLE					hLocalThread;
	BOOL			bDxiRTN;

    hLocalThread = RunThread_GetId(NULL);
	
	memset(g_RespData.szRespData2[3], '\0', MAX_YDNFRAME_LEN);
	
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_PACKDATA_I(logText);

		memset(g_RespData.szRespData1[3], '\0', MAX_YDNFRAME_LEN);
		return;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will read DC data");
	TRACE("the Block Info\n");
	TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
#endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    szOutData = g_RespData.szRespData2[3];
    for(i = 0; i < 4; i++, szOutData++)		//reserved for LENGTH
	{
    	*szOutData = '0';
	}
    
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder[0] = 0;
    iEquipIDOrder[1] = 0;
    
	// 2.Package the DataFlag
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
    {
	    memset(g_RespData.szRespData1[3], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
    if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
	{
    	iEquipIDOrder[0] = 0;
	}
    
	iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,DATAFLAG,0);
	//HexToTwoAsciiData(0x01, szOutData);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;

    // 3.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
    {
	    memset(g_RespData.szRespData1[3], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
    if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
	{
    	iEquipIDOrder[0] = 0;
	}

	iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//DCDNum
    pCurEntry++;
    iDataLen++;
    szOutData += 2;

	//find out the first place of the equiptype
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
	{
		memset(g_RespData.szRespData1[3], '\0', MAX_YDNFRAME_LEN);
		return;
	}
	if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
	{
    	iEquipIDOrder[0] = 0;
	}
	pFixedPos = pCurEntry;

    for(i = 0; i < iData[0]; i++, iEquipIDOrder[0]++)
    {
		RunThread_Heartbeat(hLocalThread);
	       
		iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];//find out the equipID orderly

		bDxiRTN=GetSCUPSingleSigValues(FALSE, 
							SIG_TYPE_SETTING, 
							TEMP_NUM_SIG_ID, 
							iEquipID[0], 
							pTempNum);
		if(bDxiRTN ==  FALSE)
		{
			iTempNum = 0;
		}
		else
		{
			iTempNum = (int)YDN_AsciiHexToChar(pTempNum);
		}
		

		iData[2] = pCurEntry->iFlag1;//fixed Num
	      
		for(k = 0; k < iData[2]; k++)//DCVolt, LoadCurr
		{
			GetSCUPSigValues(TRUE, pCurEntry, iEquipID[0], szOutData,TRUE,0,0);
			szOutData += 8;
			iDataLen += 4;
			pCurEntry++;
		}

		GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,0,0);
		iBattNum[i] = (int)YDN_AsciiHexToChar(szOutData);//BattNum
		if(iBattNum[i] > DCD_BATT_MAX_NUM)
		{
       		iBattNum[i] = 0;
		}
		pCurEntry++;
		iDataLen++;
		szOutData += 2;

		if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[1])) == FALSE)
		{
			memset(g_RespData.szRespData1[3], '\0', MAX_YDNFRAME_LEN);
			return;
		}
		if(iEquipIDOrder[1] > (MAX_EQUIP_NUM - 1))
		{
    		iEquipIDOrder[1] = 0;
		}
	       
		for(k = 0; k < i; k++)
		{
			iEquipIDOrder[1] += iBattNum[k];
		}

		for(k = 0; k < iBattNum[i]; k++)
		{
			iTotalBattNum1++;
			if(iTotalBattNum1 <= MAX_TOTAL_BATT_NUM)
			{
				iEquipID[1] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[1]][0];
				GetSCUPSigValues(TRUE, pCurEntry, iEquipID[1], szOutData,TRUE,0,0);
			}
			else
			{
				for(j = 0; j < 8; j++)
				{
					szOutData[j] = 0x20;
				}
			}
			pCurEntry++;
			szOutData += 8;
			iDataLen += 4;
			iEquipIDOrder[1]++;
		}

		pCurEntry = pCurEntry + (DCD_BATT_MAX_NUM - iBattNum[i]);

		GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,0,0);
		iData[1] = (int)YDN_AsciiHexToChar(szOutData);//OutputNum
		pCurEntry++;
		iDataLen++;
		szOutData += 2;
		for(k = 0; k < iData[1]; k++)
		{
			GetSCUPSigValues(TRUE, pCurEntry, iEquipID[0], szOutData,TRUE,0,0);
			pCurEntry++;
			szOutData += 8;
			iDataLen += 4;
		}

		pCurEntry = pCurEntry + (DCD_OUTPUT_MAX_NUM - iData[1]);
	       
		iData[1] = pCurEntry->iFlag1;//Defined Num
		//sprintf(szOutData, "%02X", iData[1]);
		HexToTwoAsciiData((unsigned char)iData[1], szOutData);
		pCurEntry++;
		iDataLen++;
		szOutData += 2;

		iData[1] = pCurEntry->iFlag1;

		for(k = 0; k < iTempNum; k++)
		{
			GetSCUPSigValues(TRUE, pCurEntry, iEquipID[0], szOutData,TRUE,0,0);
			pCurEntry++;
			szOutData += 8;
			iDataLen += 4;
		}
		 
		for(k = 0; k < (iData[1] - iTempNum) * 8; k++)
		{
			*szOutData = 0x20;
			szOutData++;
		}
		iDataLen = iDataLen + (iData[1] - iTempNum) * 4;
		pCurEntry = pCurEntry + (iData[1] - iTempNum);

		if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[1])) == FALSE)
		{
			memset(g_RespData.szRespData1[3], '\0', MAX_YDNFRAME_LEN);
			return;
		}
		if(iEquipIDOrder[1] > MAX_EQUIP_NUM - 1)
		{
    		iEquipIDOrder[1] = 0;
		}

		for(k = 0; k < i; k++)
		{
			iEquipIDOrder[1] += iBattNum[k];
		}

		pFixedBattPos = pCurEntry;
		for(k = 0; k < iBattNum[i]; k++)
		{
			iTotalBattNum2++;
			if(iTotalBattNum2 <= MAX_TOTAL_BATT_NUM)
			{
				iEquipID[1] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[1]][0];
				for(j = 0; j < pFixedBattPos->iFlag1; j++)
				{
					GetSCUPSigValues(TRUE, pCurEntry, iEquipID[1], szOutData,TRUE,0,0);
					pCurEntry++;
					szOutData += 8;
					iDataLen += 4;
				}
			}
			else
			{
				for(j = 0; j < pFixedBattPos->iFlag1; j++)
				{
					for(l = 0; l < 8; l++)
					{
						szOutData[l] = 0x20;
					}
					pCurEntry++;
					szOutData += 8;
					iDataLen += 4;
				}			
			}
			iEquipIDOrder[1]++;
			pCurEntry = pFixedBattPos;
		}

		for(k = 0; k < DCD_BATT_MAX_NUM - iBattNum[i]; k++)
		{
	   		for(j = 0; j < (pFixedBattPos->iFlag1 * 8); j++)
	   		{
           		*szOutData = 0x20;
           		szOutData++;
	   		}
			iDataLen += (4 * pFixedBattPos->iFlag1);
		}
		pCurEntry = pFixedPos;
		
    }
    
	// 4.calculate LCHKSUM and package it 
	szOutData = g_RespData.szRespData2[3];
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

	memcpy(g_RespData.szRespData1[3], g_RespData.szRespData2[3], MAX_YDNFRAME_LEN);

	Sleep(50);

}

/*==========================================================================*
 * FUNCTION : PackRDCAL
 * PURPOSE  : Package data for the command of reading DC distribution alarm. 
 * CALLS    : 
 *			  
 * CALLED BY: PackData
 * ARGUMENTS:  
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-12 16:02
 *==========================================================================*/
static void PackRDCAL(int iCID1, int iCID2)
{
	int						i, j, k, m, iDataLen;
	int						iData[2], iBattNum[10];
	int						iTotalBattNum = 0;
	unsigned char*			szOutData, pBattNum[2];
	char					logText[YDN_LOG_TEXT_LEN];
	unsigned char**			pDCVoltAlarm;
	unsigned char**			pAnalogAlarm;
	unsigned char*			pTemp;

	int						iEquipIDOrder[2], iEquipID[2];
	YDN_MAPENTRIES_INFO*	pCurEntry; 
	YDN_MAPENTRIES_INFO*	pFixedPos;
	YDN_MAPENTRIES_INFO*	pFixedBattPos;
    YDN_TYPE_MAP_INFO*		pTypeMap;

	/* not used the param */
	HANDLE hLocalThread;

    hLocalThread = RunThread_GetId(NULL);

	memset(g_RespData.szRespData2[4], '\0', MAX_YDNFRAME_LEN);
    
	/* 2.get the block */
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_PACKDATA_I(logText);

		memset(g_RespData.szRespData1[4], '\0', MAX_YDNFRAME_LEN);
		return;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will read data from a Block");
	TRACE("the Block Info\n");
	TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
#endif //_DEBUG_YDN_CMD_HANDLER


    iDataLen = 0;
    szOutData = g_RespData.szRespData2[4];
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
	{
    	*szOutData = '0';
	}

    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder[0] = 0;
    iEquipIDOrder[1] = 0;
    
	// 2.Package the DataFlag
    //printf("Go to Package dataflag\n");
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
    {
	    memset(g_RespData.szRespData1[4], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
	if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
	{
		iEquipIDOrder[0] = 0;
	}
     
	iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,DATAFLAG,0);
 
    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    //printf("Go to Package dataflag:%d, %d, %s\n",iEquipIDOrder[0], iEquipID[0], szOutData);
	// 3.Package data
    
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
    {
	    memset(g_RespData.szRespData1[4], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
	if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
	{
		iEquipIDOrder[0] = 0;
	}

	iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,0,0);
    //printf("Go to Package dataflag --2:%d, %d, %s\n",iEquipIDOrder[0], iEquipID[0], szOutData);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//DCDNum
    //printf("Go to Package dataflag --3:%d\n",iData[0] );
    pCurEntry++;

    iDataLen++;
    szOutData += 2;

	//find out the first place of the equiptype
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
	{
		memset(g_RespData.szRespData1[4], '\0', MAX_YDNFRAME_LEN);
		return;
	}
	if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
	{
		iEquipIDOrder[0] = 0;
	}
    
	pFixedPos = pCurEntry;

    for(i = 0; i < iData[0]; i++, iEquipIDOrder[0]++)
    {
		
		RunThread_Heartbeat(hLocalThread);
        
		iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];//find out the equipID orderly

		pTemp = szOutData;
		pDCVoltAlarm = &pTemp;

		GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,TRUE,0,0);//DCVoltAlarm
		ExchangeYDNSigValue(szOutData, pCurEntry);
		szOutData += 2;
		iDataLen++;
		pCurEntry++;

		GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,0,0);
		iData[1] = (int)YDN_AsciiHexToChar(szOutData);//DCFuseNum
		pCurEntry++;
		iDataLen++;
		szOutData += 2;
		for(j = 0; j< iData[1]; j++)
		{
			GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,TRUE,0,0);
			ExchangeYDNSigValue(szOutData, pCurEntry);
			szOutData += 2;
			iDataLen++;
			pCurEntry++;
		}

		pCurEntry = pCurEntry + (DC_MAX_FUSE_NUM - iData[1]);

		iData[1] = pCurEntry->iFlag1;//Defined Num

		HexToTwoAsciiData((unsigned char)iData[1], szOutData);
		pCurEntry++;
		iDataLen++;
		szOutData += 2;

		iData[1] = pCurEntry->iFlag1;
		for(j = 0; j < iData[1]; j++, pCurEntry++)
		{
			pTemp = szOutData;
			pAnalogAlarm = &pTemp;
			
	
			GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,TRUE,0,0);

			if(j == 3)
			{
				bDCDNoRespFlag = (int)YDN_AsciiHexToChar(szOutData);
				if(bDCDNoRespFlag)
				{
					for(m = 0; m < 2; m++)
					{
						*(*pDCVoltAlarm + m) = 0x30;
					}
					for(m = 0; m < 6; m++)
					{
						*(*pAnalogAlarm + m) = 0x30;
					}
					bDCDNoRespFlag = 0;
				}
			}

			ExchangeYDNSigValue(szOutData, pCurEntry);
			szOutData += 2;
			iDataLen++;
		}

		GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], pBattNum,FALSE,0,0);
		iBattNum[i] = (int)YDN_AsciiHexToChar(pBattNum);//BattNum
		if(iBattNum[i] > DCD_BATT_MAX_NUM)
		{
       		iBattNum[i] = 0; 
		}
		pCurEntry++;

		if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[1])) == FALSE)
		{
			memset(g_RespData.szRespData1[4], '\0', MAX_YDNFRAME_LEN);
			return;
		}
		if(iEquipIDOrder[1] > MAX_EQUIP_NUM - 1)
		{
    		iEquipIDOrder[1] = 0;
		}
		for(k = 0; k < i; k++)
		{
			iEquipIDOrder[1] += iBattNum[k];
		}

		pFixedBattPos = pCurEntry;
		for(k = 0; k < iBattNum[i]; k++)
		{
			iTotalBattNum++;
			if(iTotalBattNum <= MAX_TOTAL_BATT_NUM)
			{
				iEquipID[1] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[1]][0];
				for(j = 0; j < pFixedBattPos->iFlag1; j++)
				{
					GetSCUPSigValues(FALSE, pCurEntry, iEquipID[1], szOutData,TRUE,0,0);
					ExchangeYDNSigValue(szOutData, pCurEntry);
					pCurEntry++;
					szOutData += 2;
					iDataLen++;
				}
			}
			else
			{
				for(j = 0; j < pFixedBattPos->iFlag1; j++)
				{
					szOutData[0] = 0x30;
					szOutData[1] = 0x30;
					pCurEntry++;
					szOutData += 2;
					iDataLen++;
				}			
			}
			iEquipIDOrder[1]++;
			pCurEntry = pFixedBattPos;
		}

		for(k = iBattNum[i]; k < DCD_BATT_MAX_NUM; k++)
		{
			for(j = 0; j < 4; j++, szOutData++)
			{
				*szOutData = 0x30;
			}
	        iDataLen += 2;
		}
		//pCurEntry += 2;
	    pCurEntry = pFixedPos;
	}

    // 4.calculate LCHKSUM and package it 
	szOutData = g_RespData.szRespData2[4];
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

	memcpy(g_RespData.szRespData1[4],g_RespData.szRespData2[4], MAX_YDNFRAME_LEN);

	Sleep(50);
}

/*==========================================================================*
 * FUNCTION : PackRREA
 * PURPOSE  : PackData 
 * CALLS    : 
 *			  
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-12 16:02
 *==========================================================================*/
static void PackRREA(int iCID1, int iCID2)
{
	int i, j, iDataLen;
	int iCID3, iData[2];
	unsigned char *szOutData;
	char logText[YDN_LOG_TEXT_LEN];

	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;

	HANDLE hLocalThread;

	hLocalThread = RunThread_GetId(NULL);

	memset(g_RespData.szRespData2[5], '\0', MAX_YDNFRAME_LEN);
	memset(g_RespData.szRespData2[8], '\0', MAX_YDNFRAME_LEN);
	memset(g_RespData.szRespData2[10], '\0', MAX_YDNFRAME_LEN);

	/* 2.get the block */
	iCID3 = iCID2;
	if((iCID3 == READ51TO100RECT_A_DATA)||(iCID3 == READ101TO150RECT_A_DATA))
		iCID2 = 0x41;
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_PACKDATA_I(logText);

		memset(g_RespData.szRespData1[5], '\0', MAX_YDNFRAME_LEN);
		memset(g_RespData.szRespData1[8], '\0', MAX_YDNFRAME_LEN);
		memset(g_RespData.szRespData1[10], '\0', MAX_YDNFRAME_LEN);
		return;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will read data from a Block");
	TRACE("the Block Info\n");
	TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
#endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    if(iCID3 == READ101TO150RECT_A_DATA)
    {
	szOutData = g_RespData.szRespData2[10];
    }
    else if(iCID3 == READ51TO100RECT_A_DATA)
        szOutData = g_RespData.szRespData2[8];
    else
    	szOutData = g_RespData.szRespData2[5];

    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    
	// 2.Package the DataFlag
    
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
    {
	    if(iCID3 == READ101TO150RECT_A_DATA)
		memset(g_RespData.szRespData1[10], '\0', MAX_YDNFRAME_LEN);
	    else if(iCID3 == READ51TO100RECT_A_DATA)
		memset(g_RespData.szRespData1[8], '\0', MAX_YDNFRAME_LEN);
	    else
		memset(g_RespData.szRespData1[5], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,DATAFLAG,0);
    pCurEntry++;
    iDataLen++;
    szOutData += 2;

	// 3.Package data
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
    {
	    if(iCID3 == READ101TO150RECT_A_DATA)
		    memset(g_RespData.szRespData1[10], '\0', MAX_YDNFRAME_LEN);
	    else if(iCID3 == READ51TO100RECT_A_DATA)
		    memset(g_RespData.szRespData1[8], '\0', MAX_YDNFRAME_LEN);
	    else
		    memset(g_RespData.szRespData1[5], '\0', MAX_YDNFRAME_LEN);
	    return;
    }
    if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);//Rectifier output voltage
	pCurEntry++;
    iDataLen += 4;
    szOutData += 8;

	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
	{
		if(iCID3 == READ101TO150RECT_A_DATA)
			memset(g_RespData.szRespData1[10], '\0', MAX_YDNFRAME_LEN);
		else if(iCID3 == READ51TO100RECT_A_DATA)
			memset(g_RespData.szRespData1[8], '\0', MAX_YDNFRAME_LEN);
		else
			memset(g_RespData.szRespData1[5], '\0', MAX_YDNFRAME_LEN);
		return;
	}
	if(iEquipIDOrder > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder = 0;
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//Rectifier Num

	 if(iCID3 == READ101TO150RECT_A_DATA && iData[0] <= (YDN_MAX_RECT_NUM*2))
	{
	    memset(g_RespData.szRespData2[10], '\0', MAX_YDNFRAME_LEN);
	    memset(g_RespData.szRespData1[10], '\0', MAX_YDNFRAME_LEN);

	    return;
	}

	 if(iCID3 == READ101TO150RECT_A_DATA && iData[0] > (YDN_MAX_RECT_NUM*2))
	 {
		 iData[0] = iData[0] -(YDN_MAX_RECT_NUM*2);
		 HexToTwoAsciiData((unsigned char)iData[0], szOutData);
	 }

	if(iCID3 == READ51TO100RECT_A_DATA && iData[0] <= YDN_MAX_RECT_NUM)
	{
		memset(g_RespData.szRespData2[8], '\0', MAX_YDNFRAME_LEN);
		memset(g_RespData.szRespData1[8], '\0', MAX_YDNFRAME_LEN);
		
		return;
	}
    if(iCID3 == READ51TO100RECT_A_DATA && iData[0] > YDN_MAX_RECT_NUM)
	{
    		iData[0] = iData[0] - YDN_MAX_RECT_NUM;
		HexToTwoAsciiData((unsigned char)iData[0], szOutData);
		if(iData[0]  >YDN_MAX_RECT_NUM)
			iData[0] = YDN_MAX_RECT_NUM;
	}
  
    if(iData[0] > YDN_MAX_RECT_NUM && iCID3 == 0x41)
    	iData[0] = YDN_MAX_RECT_NUM;
    pCurEntry++;
    iDataLen++;
    szOutData += 2;

	//find out the first place of the equiptype
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&iEquipIDOrder) == FALSE)
	{
		if(iCID3 == READ101TO150RECT_A_DATA)
			memset(g_RespData.szRespData1[10], '\0', MAX_YDNFRAME_LEN);
		else if(iCID3 == READ51TO100RECT_A_DATA)
			memset(g_RespData.szRespData1[8], '\0', MAX_YDNFRAME_LEN);
		else
			memset(g_RespData.szRespData1[5], '\0', MAX_YDNFRAME_LEN);
		return;
	}
	if(iCID3 == READ51TO100RECT_A_DATA)
		iEquipIDOrder += YDN_MAX_RECT_NUM;
	else if(iCID3 == READ101TO150RECT_A_DATA)
	{
		if(YDN_GetEquiplistIDOrder(RECT_NUM101_ID,&iEquipIDOrder) == FALSE)
		{
			memset(g_RespData.szRespData1[10], '\0', MAX_YDNFRAME_LEN);
			return;
		}	
	}
	
	pFixedPos = pCurEntry;

    for(i = 0; i < iData[0]; i++, iEquipIDOrder++)
    {
		if(i % 20 == 0)
		   RunThread_Heartbeat(hLocalThread);

	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];//find out the equipID orderly

	   iData[1] = pCurEntry->iFlag1;//Fixed Num
	   for(j = 0; j < iData[1]; j++, pCurEntry++)
	   {
           GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);//rectifier output current
           iDataLen += 4;
           szOutData += 8;
	   }
       iData[1] = pCurEntry->iFlag1;//Defined Num
	   HexToTwoAsciiData((unsigned char)iData[1], szOutData);
       pCurEntry++;
       iDataLen++;
	   szOutData += 2;
       for(j = 0; j < iData[1]; j++, pCurEntry++)
       {
           GetSCUPSigValues(TRUE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           iDataLen += 4;
           szOutData += 8;
       }

	   pCurEntry = pFixedPos;
    }

    // 4.calculate LCHKSUM and package it 
    if(iCID3 == READ101TO150RECT_A_DATA)
	    szOutData = g_RespData.szRespData2[10];
    else if(iCID3 == READ51TO100RECT_A_DATA)
    	szOutData = g_RespData.szRespData2[8];
    else
	    szOutData = g_RespData.szRespData2[5];
    YDN_BuildDataInfo(iDataLen, szOutData);

	if(iCID3 == READ101TO150RECT_A_DATA)
	    memcpy(g_RespData.szRespData1[10],g_RespData.szRespData2[10], MAX_YDNFRAME_LEN);
	else if(iCID3 == READ51TO100RECT_A_DATA)
		memcpy(g_RespData.szRespData1[8],g_RespData.szRespData2[8], MAX_YDNFRAME_LEN);
	else
	    memcpy(g_RespData.szRespData1[5],g_RespData.szRespData2[5], MAX_YDNFRAME_LEN);

	Sleep(50);
}

/*==========================================================================*
 * FUNCTION : PackRRES
 * PURPOSE  : PackData 
 * CALLS    : 
 *			  
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-12 16:02
 *==========================================================================*/
static void PackRRES(int iCID1, int iCID2)
{
	int i, j, iDataLen, iBattState;
	unsigned char iData[2];
	unsigned char *szOutData;
	char logText[YDN_LOG_TEXT_LEN];
    
	int  iEquipIDOrder[2], iEquipID[2];
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;

	HANDLE hLocalThread;

	hLocalThread = RunThread_GetId(NULL);

	memset(g_RespData.szRespData2[6], '\0', MAX_YDNFRAME_LEN);
	/* 2.get the block */
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_PACKDATA_I(logText);
		TRACE("TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		memset(g_RespData.szRespData1[6], '\0', MAX_YDNFRAME_LEN);
		return;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will read data from a Block");
	TRACE("the Block Info\n");
	TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
#endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    szOutData = g_RespData.szRespData2[6];
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder[0] = 0;
    // 2.Package the DataFlag
    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
    {
	memset(g_RespData.szRespData1[6], '\0', MAX_YDNFRAME_LEN);
	return;
    }
    if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder[0] = 0;
	iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,DATAFLAG,0);

    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    // 3.Package data
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
	{
		memset(g_RespData.szRespData1[6], '\0', MAX_YDNFRAME_LEN);
		return;
	}
    if(iEquipIDOrder[0] > MAX_EQUIP_NUM - 1)
    	iEquipIDOrder[0] = 0;
	iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];
	GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//Rectifier Num
    pCurEntry++;
    iDataLen++;
    szOutData += 2;

    //TRACE("ExecuteRRES!!\n");

	//find out the first place of the equiptype
	if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[0])) == FALSE)
	{
		memset(g_RespData.szRespData1[6], '\0', MAX_YDNFRAME_LEN);
		return;
	}
	pFixedPos = pCurEntry;

    for(i = 0; i < iData[0]; i++, iEquipIDOrder[0]++)
    {
		if(i % 20 == 0)
		   RunThread_Heartbeat(hLocalThread);

	   iEquipID[0] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[0]][0];//find out the equipID orderly

	   iData[1] = pCurEntry->iFlag1;//Fixed Num
	   for(j = 0; j < iData[1]; j++)
	   {
           GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,TRUE,0,0);
           ExchangeYDNSigValue(szOutData, pCurEntry);
		   if(pCurEntry->iFlag1 == 99)
           {
			    if(YDN_GetEquipIDOrder(pCurEntry->iEquipType,&(iEquipIDOrder[1])) == FALSE)
			    {
				    memset(g_RespData.szRespData1[6], '\0', MAX_YDNFRAME_LEN);
				    return;
			    }	
			    if(iEquipIDOrder[1] > MAX_EQUIP_NUM - 1)
    				iEquipIDOrder[1] = 0;
				iEquipID[1] = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder[1]][0];

				GetSCUPSigValues(FALSE, pCurEntry, iEquipID[1], szOutData,TRUE,0,0);
				//if(pCurEntry->iLen != -1)
				ExchangeYDNSigValue(szOutData, pCurEntry);

        		iBattState = (int)YDN_AsciiHexToChar(szOutData);
        		//TRACE("iBattState is: %d\n",iBattState);
        		if(iBattState ==1 || iBattState ==3 || iBattState == 4 || iBattState == 11)
        		{
        	 		iBattState = 2;
        	 		HexToTwoAsciiData((unsigned char)iBattState, szOutData);
        		}
        		else if(iBattState == 5 || iBattState == 6)
        		{
        			iBattState = 3;
        	 		HexToTwoAsciiData((unsigned char)iBattState, szOutData);
        		}
        		else if((iBattState >=7 && iBattState <= 10) || iBattState == 2)
        		{
        			iBattState = 1;
        	 		HexToTwoAsciiData((unsigned char)iBattState, szOutData);
        		}
        		else
        		{
        			iBattState = 0;
        	 		HexToTwoAsciiData((unsigned char)iBattState, szOutData);
        		}
           }
           iDataLen++;
           szOutData += 2;
           pCurEntry++;
	   }
	   
       iData[1] = pCurEntry->iFlag1;//Defined Num
	   HexToTwoAsciiData((unsigned char)iData[1], szOutData);
       pCurEntry++;
       iDataLen++;
	   szOutData += 2;
       for(j = 0; j < iData[1]; j++)
       {
          GetSCUPSigValues(FALSE, pCurEntry, iEquipID[0], szOutData,TRUE,0,0);
          if(pCurEntry->iLen != -1)
             ExchangeYDNSigValue(szOutData, pCurEntry);
          iDataLen++;
          szOutData += 2;
          pCurEntry++;
       }

	   pCurEntry = pFixedPos;
    }
    // 4.calculate LCHKSUM and package it 
	szOutData = g_RespData.szRespData2[6];
    YDN_BuildDataInfo(iDataLen, szOutData);

	memcpy(g_RespData.szRespData1[6],g_RespData.szRespData2[6], MAX_YDNFRAME_LEN);

	Sleep(50);
}

/*==========================================================================*
 * FUNCTION : PackRREAL
 * PURPOSE  : PackData 
 * CALLS    : 
 *			  
 * CALLED BY: 
 * ARGUMENTS: 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-12 16:02
 *==========================================================================*/
static void PackRREAL(int iCID1, int iCID2)
{
	int i, j, iDataLen;
	int iData[2];
	unsigned char *szOutData;
	char logText[YDN_LOG_TEXT_LEN];
    
	int  iEquipIDOrder, iEquipID;
    YDN_MAPENTRIES_INFO	 *pCurEntry, *pFixedPos;
    YDN_TYPE_MAP_INFO *pTypeMap;

	HANDLE hLocalThread;

	hLocalThread = RunThread_GetId(NULL);

	memset(g_RespData.szRespData2[7], '\0', MAX_YDNFRAME_LEN);

	/* 2.get the block */
	pTypeMap = GetTypeMap(iCID1, iCID2);

	if (pTypeMap == NULL)
	{
		sprintf(logText, "TypeMap(iCID1:%d, iCID2:%d) not exits in system",
			iCID1, iCID2);
		LOG_YDN_PACKDATA_I(logText);

		memset(g_RespData.szRespData1[7], '\0', MAX_YDNFRAME_LEN);
		return;
	}

#ifdef _DEBUG_YDN_CMD_HANDLER
	TRACE_YDN_TIPS("Will read data from a Block");
	TRACE("the Block Info\n");
	TRACE("\tiCID1: %d\n", pTypeMap->iCID1);
	TRACE("\tiCID2: %d\n", pTypeMap->iCID2);
#endif //_DEBUG_YDN_CMD_HANDLER

    iDataLen = 0;
    szOutData = g_RespData.szRespData2[7];
    for(i = 0; i < 4; i++, szOutData++)//reserved for LENGTH
    	*szOutData = '0';
    pCurEntry = pTypeMap->pMapEntriesInfo;
    iEquipIDOrder = 0;
    
	// 2.Package the DataFlag
    iEquipIDOrder = GetEquipIDOrder(pCurEntry->iEquipType);
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
    GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,DATAFLAG,0);

    pCurEntry++;
    iDataLen++;
    szOutData += 2;
    
	// 3.Package data
	iEquipIDOrder = GetEquipIDOrder(pCurEntry->iEquipType);
	iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];
	GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,FALSE,0,0);
    iData[0] = (int)YDN_AsciiHexToChar(szOutData);//Rectifier Num
    pCurEntry++;
    iDataLen++;
    szOutData += 2;

	iEquipIDOrder = GetEquipIDOrder(pCurEntry->iEquipType);//find out the first place of the equiptype
    pFixedPos = pCurEntry;

    for(i = 0; i < iData[0]; i++, iEquipIDOrder++)
    {
		if(i % 20 == 0)
		   RunThread_Heartbeat(hLocalThread);

	   iEquipID = g_YDNGlobals.iEquipIDOrder[iEquipIDOrder][0];//find out the equipID orderly

	   iData[1] = pCurEntry->iFlag1;//Fixed Num
	   for(j = 0; j < iData[1]; j++)
	   {
           GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
           ExchangeYDNSigValue(szOutData, pCurEntry);
           iDataLen++;
           szOutData += 2;
           pCurEntry++;
	   }
	   
       iData[1] = pCurEntry->iFlag1;//Defined Num
       //sprintf(szOutData, "%02X", iData[1]);
	   HexToTwoAsciiData((DWORD)(iData[1]), szOutData);
       pCurEntry++;
       iDataLen++;
	   szOutData += 2;
       for(j = 0; j < iData[1]; j++)
       {
          GetSCUPSigValues(FALSE, pCurEntry, iEquipID, szOutData,TRUE,0,0);
          ExchangeYDNSigValue(szOutData, pCurEntry);
          iDataLen++;
          szOutData += 2;
          pCurEntry++;
       }

	   pCurEntry = pFixedPos;
    }
    // 4.calculate LCHKSUM and package it 
	szOutData = g_RespData.szRespData2[7];
    szOutData = YDN_BuildDataInfo(iDataLen, szOutData);

	memcpy(g_RespData.szRespData1[7],g_RespData.szRespData2[7], MAX_YDNFRAME_LEN);

	Sleep(50);
}

/*==========================================================================*
 * FUNCTION : YDN_PackData
 * PURPOSE  : PackData thread entry function
 * CALLS    : 
 *			  
 * CALLED BY: ServiceMain
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-09 16:02
 *==========================================================================*/
int YDN_PackData(YDN_BASIC_ARGS *pThis)
{
	HANDLE hLocalThread;

	/* get Local thread id */	
	hLocalThread = RunThread_GetId(NULL);
	/* assign the reference in the Thread to insure valid */
	pThis->hThreadID[2] = hLocalThread;
	/* feed watch dog */
	RunThread_Heartbeat(hLocalThread);
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread YDN Pack Service was created, Thread Id = %d.\n", gettid());
#endif	
	/* begin pack data loop */
	while (pThis->iPackDataThreadExitCmd == SERVICE_EXIT_NONE)
	{				/* feed watch dog */

		RunThread_Heartbeat(hLocalThread);

		PackRACA(0x40, 0x41);

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}

		PackRACS(0x40, 0x43);

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}

		PackRACAL(0x40, 0x44);

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}

		PackRDCA(0x42, 0x41);

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}
		
		PackRDCAL(0x42, 0x44);

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}
	
		PackRREA(0x41, 0x41);

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}
		
		PackRRES(0x41, 0x43);

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}

		
		PackRREAL(0x41, 0x44);

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}

		PackRREA(0x41, READ51TO100RECT_A_DATA);//read 51-100

		if(pThis->iPackDataThreadExitCmd == SERVICE_EXIT_OK)
		{
			break;
		}

		PackRREA(0x41, READ101TO150RECT_A_DATA);//read 101-100
		
		
		RunThread_Heartbeat(hLocalThread);
		
		Sleep(5000);
	}
	return 0;
}
/*==========================================================================*
* FUNCTION : YDN_CheckSPortParam
* PURPOSE  : Check the SPort Param
* CALLS    : 
*			  
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Wang Jing                   DATE: 2006-11-09 16:02
*==========================================================================*/
BOOL YDN_CheckSPortParam(IN char *pSPortParam)
{
    char STDSPortParam[][32] = {"1200,n,8,1",
                                "2400,n,8,1",
                                "4800,n,8,1",
                                "9600,n,8,1",
                                "19200,n,8,1",
                                "38400,n,8,1"};
    char *pTempSPortParam = Cfg_RemoveWhiteSpace(pSPortParam);
    int i = 0;
    int iResult = 0;
    for(i = 0;i < 6;i++)
    {
        if(strcmp(pTempSPortParam,STDSPortParam[i]) == 0)
        {
            iResult = TRUE;
            break;
        }
        else
        {
            iResult = FALSE;
        }
    }

    return iResult;
}


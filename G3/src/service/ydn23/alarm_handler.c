/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : alarm_handler.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "ydn.h"

/* log Macros (AH = Alarm Handler) */
#define YDN_TASK_AH		"YDN_AH"

/* error log */
#define LOG_YDN_AH_E(_szLogText)  LOG_YDN_E(YDN_TASK_AH, _szLogText)

/* warning log */
#define LOG_YDN_AH_W(_szLogText)  LOG_YDN_W(YDN_TASK_AH, _szLogText) 

/* info log */
#define LOG_YDN_AH_I(_szLogText)  LOG_YDN_I(YDN_TASK_AH, _szLogText)


/* do in the config builder sub-module */
//static BOOL CheckNumbers (YDN_BASIC_ARGS *pThis, BYTE byRouteType);

//static void ConnectToRemote (YDN_BASIC_ARGS *pThis, BYTE byRouteType)

/*==========================================================================*
 * FUNCTION : YDN_RegisterAlarm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: AlarmProc
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis        : 
 *            BOOL            bDealyedAlarm : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 16:11
 *==========================================================================*/
void YDN_RegisterAlarm(YDN_BASIC_ARGS *pThis, BOOL bDelayedAlarm)
{
	YDN_ALARM_HANDLER *pAlarmHandler;
	YDN_COMMON_CONFIG *pCommonCfg;

	pCommonCfg = &g_YDNGlobals.CommonConfig;

	//TRACE("\nEnter YDN_SEND_ALARM!!!!:YDN_RegisterAlarm\n");

	/* alarm report function is disabled */
	if (!pCommonCfg->bReportInUse || pCommonCfg->iMaxAttempts == 0)
	{
		return;
	}

	pAlarmHandler = &pThis->alarmHandler;

	/* if is delayed alarm or last report is running, do not reset retry counter */
	if (!bDelayedAlarm && !pAlarmHandler->bAlarmAtHand)
	{
		pAlarmHandler->byAlarmRetryCounter = 0;
	}


	/* if last report is not running (means not using Route2,...) */
	if (!pAlarmHandler->bAlarmAtHand) 
	{
		pAlarmHandler->byCurReportRouteType = YDN_ROUTE_ALARM_1;
	}
	pAlarmHandler->bAlarmAtHand = TRUE;

	return;
}

/*==========================================================================*
 * FUNCTION : YDN_SendAlarm
 * PURPOSE  : set link-layer operation mode change flag and link-layer manager
 *            will send alarm later
 * CALLS    : 
 * CALLED BY: YDN_OnIdle
 *			
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 11:18
 *==========================================================================*/
void YDN_SendAlarm(YDN_BASIC_ARGS *pThis)
{
	YDN_ALARM_HANDLER *pAlarmHandler;
	pAlarmHandler = &pThis->alarmHandler;

	/* set server mode exit flag */
	pThis->bServerModeNeedExit = TRUE;

	TRACE("Enter YDN_SendAlarm!!\n");

	/* set client mode */
	pThis->iOperationMode = YDN_MODE_CLIENT;

	return;
}

/*==========================================================================*
 * FUNCTION : YDN_AlarmReported
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: (YDN_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 16:25
 *==========================================================================*/
void YDN_AlarmReported (YDN_BASIC_ARGS *pThis)
{
	YDN_ALARM_HANDLER *pAlarmHandler;

	pAlarmHandler = &pThis->alarmHandler;

	pAlarmHandler->bAlarmAtHand = FALSE;
	pAlarmHandler->byAlarmRetryCounter = 0;

	/* set link-layer flags */
	pThis->bServerModeNeedExit = FALSE;
	pThis->iOperationMode = YDN_MODE_SERVER;

	return;
}

/*==========================================================================*
 * FUNCTION : YDN_AlarmReportTimerProc
 * PURPOSE  : timer call back function
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hTimerOwner : The thread ID of timer owner
 *            int     idTimer     : The ID of the timer
 *            void    *pArgs      : The args passed by timer owner, 
 *                                  here is YDN_BASIC_ARGS *pThis
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 14:12
 *==========================================================================*/
static int YDN_ReportTimerProc(HANDLE  hTimerOwner,
							   int     idTimer,
							   void    *pArgs)
{
	YDN_BASIC_ARGS *pThis;
    UNUSED(hTimerOwner);

	pThis = (YDN_BASIC_ARGS *)pArgs;

	if (idTimer == YDN_ALARM_TIMER)
	{
#ifdef _DEBUG_ESR_REPORT
		TRACE_ESR_TIPS("Register Alarm by Timer");
#endif //_DEBUG_ESR_REPORT

		YDN_RegisterAlarm(pThis, TRUE);
	}
	
	return TIMER_KILL_THIS;
}


/*==========================================================================*
 * FUNCTION : YDN_ClientReportFailed
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: OnSend
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis          : 
 *            BOOL            bConnetedFailed : 
 * RETURN   :  void : 
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-12 14:39
 *==========================================================================*/
void YDN_ClientReportFailed(YDN_BASIC_ARGS *pThis, 
							BOOL bConnetedFailed)
{
	YDN_COMMON_CONFIG *pConfig;
	YDN_ALARM_HANDLER *pAlarmHandler;
	int iCurRouteType, iTimerSetRet;
	BOOL bLeasedLine;    //for lease line media type

	char *pLog;
	char szLogText[YDN_LOG_TEXT_LEN];  //for log

	pConfig = &g_YDNGlobals.CommonConfig;
	pAlarmHandler = &pThis->alarmHandler;
	iCurRouteType = pAlarmHandler->byCurReportRouteType;

	bLeasedLine = (pConfig->iMediaType == YDN_MEDIA_TYPE_LEASED_LINE) ? TRUE
		: FALSE;
	switch (iCurRouteType)
	{
	    case YDN_ROUTE_ALARM_1:
		   /* for leased line, only has one route */
		   if (!bLeasedLine)
		   {
				pAlarmHandler->byCurReportRouteType = YDN_ROUTE_ALARM_2;

				/* log and trace */
				if (bConnetedFailed)
				{
					LOG_YDN_AH_W("Alarm report using Route 1 failed(can not connect"
						" to MC), try to use Route 2");
				}
				else
				{
					LOG_YDN_AH_W("Alarm report using Route 1 failed, try to use "
						"Route 2");
				}
				break;
		   }

		 case YDN_ROUTE_ALARM_2:
		   /* for leased line, only has one route */
		   if (!bLeasedLine)
		   {
				pAlarmHandler->byCurReportRouteType = YDN_ROUTE_ALARM_3;

				/* log and trace */
				if (bConnetedFailed)
				{
					LOG_YDN_AH_W("Alarm report using Route 2 failed(can not connect"
						" to MC), try to use Route 3");
				}
				else
				{
					LOG_YDN_AH_W("Alarm report using Route 2 failed, try to use "
						"Route 3");
				}
				break;
		   }

	    case YDN_ROUTE_ALARM_3:
			if (++(pAlarmHandler->byAlarmRetryCounter) <
				pConfig->iMaxAttempts)
			{
				/* reset flags */
				pAlarmHandler->bAlarmAtHand = FALSE;
				pThis->bServerModeNeedExit = FALSE;
				pThis->iOperationMode = YDN_MODE_SERVER;

				/* log and trace */
				pLog = bLeasedLine ? "Lease Line" : "Route 3";
				
					
				if (bConnetedFailed)
				{
					sprintf(szLogText, "Alarm Report using %s failed(can not "
						"connect to MC). Try later", pLog);
					LOG_YDN_AH_W(szLogText);
				}
				else
				{
					sprintf(szLogText, "Alarm Report using %s failed. Try later",
						pLog);
					LOG_YDN_AH_W(szLogText);
				}
				

				/* statr timer, retry later */
				iTimerSetRet = Timer_Set(RunThread_GetId(NULL),
											YDN_ALARM_TIMER,
											pConfig->iAttemptElapse,
											YDN_ReportTimerProc,
											(DWORD)pThis);
				if (iTimerSetRet == ERR_TIMER_EXISTS)
				{
					TRACE_YDN_TIPS("Alarm Report Timer has existed");
				}
				if (iTimerSetRet == ERR_TIMER_SET_FAIL)
				{
					LOG_YDN_AH_E("Alarm Report failed(set re-alarmReport "
						"timer failed)");
				}
			}
			else
			{
				/* not try to report */
				pAlarmHandler->bAlarmAtHand = FALSE;
				pThis->bServerModeNeedExit = FALSE;
				pThis->iOperationMode = YDN_MODE_SERVER;

				sprintf(szLogText, "Alarm report failed after %d retries",
					pConfig->iMaxAttempts);
				LOG_YDN_AH_W(szLogText);	
			}
			break;

	   default: /* not be others */
		   TRACE_YDN_TIPS("Report route setting is error!!!");
		   break;
	}

	return;
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : ydn_state_machine.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "ydn.h"

/* timeout = READCOM_TIMEOUT * timeout counter */

#define YDN_RESEND_TIMES		4	//resend times(response and alarm frames)

/* some operations */

#define CLEAR_TIMEOUT_COUNTER(_pThis) \
	((_pThis)->iTimeoutCounter = 0)

#define NEED_RESEND(_pThis) \
	(++(_pThis)->iResendCounter <= YDN_RESEND_TIMES)

#define CLEAR_RESEND_COUNTER(_pThis) \
	((_pThis)->iResendCounter = 0)


/* log Macros (ESM = YDN state machine) */
#define YDN_TASK_ESM		"YDN State Machine"

/* error log */
#define LOG_YDN_ESM_E(_szLogText)  LOG_YDN_E(YDN_TASK_ESM, _szLogText)

/* warning log */
#define LOG_YDN_ESM_W(_szLogText)  LOG_YDN_W(YDN_TASK_ESM, _szLogText) 

/* info log */
#define LOG_YDN_ESM_I(_szLogText)  LOG_YDN_I(YDN_TASK_ESM, _szLogText)

/* assistant functional Macros */
/* create and send Dummy frame event */
#define ESM_SEND_DUMMY(_pThis, _pEvent)  \
	do {  \
	    (_pEvent) = &g_YDNDummyEvent; \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_YDN_ESM_E("Put frame event to Output Queue failed"); \
			return YDN_IDLE;    \
		}  \
	}  \
	while (0)

/* send static YDN event to the OutputQueue */
#define ESM_SEND_SEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_YDN_ESM_E("Put frame event to Output Queue failed"); \
			return YDN_IDLE;    \
		}  \
	}  \
	while (0)

/* send dynamic YDN event to the OutputQueue */
#define ESM_SEND_DEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_YDN_ESM_E("Put frame event to Output Queue failed"); \
			DELETE((_pEvent));  \
			return YDN_IDLE;    \
		}  \
	}  \
	while (0)

__INLINE static void YDN_OnDisconnectedEvent(YDN_BASIC_ARGS *pThis)
{
	/* clear the flags and the buf */
	pThis->iTimeoutCounter = 0;
	pThis->iResendCounter = 0;

	pThis->szCmdRespBuff[0] = '\0';

}

static unsigned char YDN23LenCheckSum(int wLen)
{
	unsigned char byLenCheckSum = 0;

	//������Ϊ0������Ҫ����
	if (wLen == 0)
		return 0;

	byLenCheckSum = 0;
	byLenCheckSum += wLen & 0x000F;         //ȡ���4��BIT
	byLenCheckSum += (wLen >> 4) & 0x000F;  //ȡ��4~7��BIT
	byLenCheckSum += (wLen >> 8) & 0x000F;  //ȡ��8~11��BIT
	byLenCheckSum %= 16;                    //ģ16
	byLenCheckSum = (~byLenCheckSum) + 1;   //ȡ����1
	byLenCheckSum &= 0x0F;                  //ֻȡ4��BIT

	return byLenCheckSum;
}


int iSetAndGetCfg_CMD_Proc(YDN_BASIC_ARGS *pThis,
			   unsigned char *szInData,
			   unsigned char *szOutData)
{						
	ASSERT(pThis);
	ASSERT(szInData);
	ASSERT(szOutData);
	UNUSED(pThis);

	union _YDN23Str2Float
	{
		float f_value;					// float
		BYTE  by_value[4];				// byte
	};
	typedef union _YDN23Str2Float	YDN23Str2Float;

	int RcvCMDFlag = 0;		//1������	2д����
	if ((strncmp(szInData, "E1EB", 4) == 0))
	{
		RcvCMDFlag = 1;					//1 �������ļ�����
	}
	else if ((strncmp(szInData, "E1EC", 4) == 0))
	{
		RcvCMDFlag = 2;					//2 д�����ļ�����
	}
	else
	{
		return CMDERR;
	}

	//printf("\n	RcvCMDFlag=%d	szInData=%s	\n",RcvCMDFlag,szInData);

	int iCID1,iCID2;
	iCID1 = 0;
	iCID2 = 0;
	int iTempValue = 0;
	int iRcvEquipId = 0;
	int iRcvSigType = 0;
	int iRcvValueType = 0;
	int iRcvSigID = 0;
	ULONG uTempValue = 0;
	LONG  lTempValue = 0;
	int iRst = FALSE;
	unsigned char *pData = NULL;
	pData = szOutData;
	int iLenChk = 0;
	int iDataLength = 0;

	int iSentValueType = 0;
	int iSentValue = 0;
	float fSentValue = 0;

	/* 2.get the block */
	iCID1 = (int)YDN_AsciiHexToChar(szInData);
	iCID2 = (int)YDN_AsciiHexToChar(szInData + 2);

	/**************************************************
	�Ӵ�ӡ�Ͽ���szInData ���ݰ���
	CID1 CID2 DataInfo   ����û����LENGTH
	**************************************************/
	//printf("\n	This is szInData=%s	\n",szInData);

	//�豸Id
	iTempValue = 0;
	iTempValue = (int)YDN_AsciiHexToChar(szInData + 4);
	iRcvEquipId = (iTempValue << 8) + (int)YDN_AsciiHexToChar(szInData + 6);

	EQUIP_INFO*		pEquip;
	int nBufLen;

	int nError = DxiGetData(VAR_A_EQUIP_INFO,
		iRcvEquipId,			
		0,		
		&nBufLen,			
		&pEquip,			
		0);

	if(nError != ERR_DXI_OK)
	{
		return CMDERR;	//�豸�������ڣ����ö�����д��
	}

	//if (0 != GetEnumSigValue(iRcvEquipId, 0, 100, "Get Exist Stat of the Equip"))
	//{
	//	return CMDERR;	//�豸�������ڣ����ö�����д��
	//}

	//�ź�TYPE
	iRcvSigType = (int)YDN_AsciiHexToChar(szInData + 8);

	//�ź�Id
	iTempValue = 0;
	iTempValue = (int)YDN_AsciiHexToChar(szInData + 10);
	iRcvSigID = (iTempValue << 8) + (int)YDN_AsciiHexToChar(szInData + 12);

	YDN23Str2Float	StrValue;

	//д�����ļ�����
	if (2 == RcvCMDFlag)
	{
		//����Type
		iRcvValueType = (int)YDN_AsciiHexToChar(szInData + 14);

		//����Value
		StrValue.by_value[0] = YDN_AsciiHexToChar(szInData + 16);
		StrValue.by_value[1] = YDN_AsciiHexToChar(szInData + 18);
		StrValue.by_value[2] = YDN_AsciiHexToChar(szInData + 20);
		StrValue.by_value[3] = YDN_AsciiHexToChar(szInData + 22);	

		//printf("\n	Rcv Fvalue=%f		\n",StrValue.f_value);

		//1:  long
		//2:  Float
		//3:  unsigned long
		//4:  time   
		//5:  Enum
		//���������ļ���ʵ��1 2 5 ��ã�
		if (3 == iRcvValueType)
		{
			uTempValue = (ULONG)(StrValue.f_value);
			iRst = SetDwordSigValue(iRcvEquipId, iRcvSigType, iRcvSigID, uTempValue, "YDN23 SetCFG ULONG ");
		}
		else if (1 == iRcvValueType)
		{
			lTempValue = (LONG)(StrValue.f_value);
			iRst = SetDwordSigValue(iRcvEquipId, iRcvSigType, iRcvSigID, lTempValue, "YDN23 SetCFG long ");
		}
		else if (5 == iRcvValueType)
		{
			iTempValue = (int)(StrValue.f_value);
			iRst = SetEnumSigValue(iRcvEquipId, iRcvSigType, iRcvSigID, iTempValue, "YDN23 SetCFG ENUM ");
		}
		else
		{
			iRst = SetFloatSigValue(iRcvEquipId, iRcvSigType, iRcvSigID, StrValue.f_value, "YDN23 SetCFG Float ");
		}

		iDataLength = 0;
		//iLenChk = YDN23LenCheckSum(iDataLength) << 4;
		pData[0] = '0';
		pData[1] = '0';
		pData[2] = '0';
		pData[3] = '0';
		pData[4] = '\0';

		if (TRUE == iRst)
		{
			return NOR;
		}
		else
		{
			return CMDERR;
		}
	}
	else //if (1 == RcvCMDFlag)//�����ô���
	{
		int			nBufLen;
		VOID*			pSigStru;
		SET_SIG_INFO*		pSetSigInfo = NULL;
		SAMPLE_SIG_INFO*	pSmplingSigInfo = NULL;
		CTRL_SIG_INFO*		pCtrlSigInfo = NULL;
		ALARM_SIG_INFO*		pAlarmSigInfo =NULL;

		//printf("\n	####	iRcvEquipId=%d	iRcvSigType=%d	iRcvSigID=%d	\n",iRcvEquipId,iRcvSigType,iRcvSigID);

		if(ERR_DXI_OK != DxiGetData(VAR_A_SIGNAL_INFO_STRU,
			iRcvEquipId,	
			DXI_MERGE_SIG_ID(iRcvSigType, iRcvSigID),
			&nBufLen,
			&pSigStru,
			0))
		{
			iRst = FALSE;
			//printf("\n	DXI GET IS ERROR \n");
			return CMDERR;
		}
		else
		{
			if (SIG_TYPE_SAMPLING == iRcvSigType)
			{
				pSmplingSigInfo = (SAMPLE_SIG_INFO*)pSigStru;
				iSentValueType = pSmplingSigInfo->iSigValueType;
			}
			else if (SIG_TYPE_CONTROL == iRcvSigType)
			{
				pCtrlSigInfo = (CTRL_SIG_INFO*)pSigStru;
				iSentValueType = pCtrlSigInfo->iSigValueType;
			}
			else if (SIG_TYPE_SETTING == iRcvSigType)
			{
				pSetSigInfo = (SET_SIG_INFO*)pSigStru;
				iSentValueType = pSetSigInfo->iSigValueType;
			}
			else
			{
				pAlarmSigInfo = (ALARM_SIG_INFO*)pSigStru;
				iSentValueType = 3; //Ulong ���Ͱ�
			}

			if (VAR_LONG == iSentValueType || VAR_UNSIGNED_LONG == iSentValueType)
			{
				iSentValue = GetDwordSigValue(iRcvEquipId, iRcvSigType, iRcvSigID, "YDN23 get LONG ");
				fSentValue = (float)iSentValue;
			}
			else if (VAR_FLOAT == iSentValueType)
			{
				fSentValue =  GetFloatSigValue(iRcvEquipId, iRcvSigType, iRcvSigID, "YDN23 get FLOAT ");
				//printf("\n The to-be-sent value (Eid%d , %d, %d ) is: %.3f\n",iRcvEquipId,iRcvSigType,iRcvSigID,fSentValue);
			}
			else if (VAR_ENUM == iSentValueType)
			{
				iSentValue = GetEnumSigValue(iRcvEquipId, iRcvSigType, iRcvSigID, "YDN23 get LONG ");
				fSentValue = (float)iSentValue;
			}

			StrValue.f_value = fSentValue;

			//����У��
			iDataLength = 10;
			iLenChk = YDN23LenCheckSum(iDataLength) << 4;
			pData[0] = HexToAscii((iLenChk & 0xf0)>>4);
			pData[1] = HexToAscii((iDataLength >> 8) & 0x0f);
			pData[2] = HexToAscii((iDataLength & 0xf0)>>4);
			pData[3] = HexToAscii(iDataLength & 0x0f);
			sprintf((char *)(pData + 4),"%02X\0", iSentValueType);				//��ֵ����
			sprintf((char *)(pData + 6),"%02X\0", StrValue.by_value[0]);			//��ֵ
			sprintf((char *)(pData + 8),"%02X\0", StrValue.by_value[1]);
			sprintf((char *)(pData + 10),"%02X\0", StrValue.by_value[2]);
			sprintf((char *)(pData + 12),"%02X\0", StrValue.by_value[3]);
			*(pData + 14) =  '\0';
			//printf("\n	Send Data =%s	\n",pData);
			return NOR;
		}
	}
	//else
	//{
	//	//��ʵ�ӵ��ñ������ĵط������˴��ǽ�������
	//	return CMDERR;
	//}

	return NOR;
}

/*==========================================================================*
 * FUNCTION : OnIdle_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: YDN_OnIdle
 * ARGUMENTS: YDN_BASIC_ARGS *pThis:
 *			  YDN_EVENT  *pEvent   : 
 * RETURN   : int : next YDN State, defined by YDN_STATES enum const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-17 20:02
 *==========================================================================*/
static int OnIdle_HandleFrameEvent(YDN_BASIC_ARGS *pThis, YDN_EVENT *pEvent)
{
	YDN_FRAME_TYPE frameType;
	//const unsigned char *sFrameData;
	unsigned char pCmdData[32];
	
	int  iCmdDataLen, iRTN, iCID1, iAddr,i;

	/* frame analyse */
	//double tTime1 = GetCurrentTime();
	frameType = YDN_AnalyseFrame(pEvent->sData, pEvent->iDataLength, pThis);

	iAddr = (int)YDN_AsciiHexToChar(pEvent->sData + 3);

	if(frameType != RADDR)
	    iCID1 = (int)YDN_AsciiHexToChar(pEvent->sData + 5);

	switch (frameType)
	{
	     case ENOR:  
/*
            #ifdef _DEBUG_YDN_LINKLAYER
		       TRACE_YDN_TIPS("Received frame event: frame_OK");
            #endif //_DEBUG_YDN_LINKLAYER

            #ifdef _DEBUG_YDN_CMD_HANDLER
		       YDN_PrintEvent(pEvent);
            #endif //_DEBUG_YDN_CMD_HANDLER
*/
			/* extract data field from select frame */
			YDN_ExtractDataFromFrame(pEvent->sData,
						pEvent->iDataLength, &iCmdDataLen, pCmdData);
    
			/* should not happen! */
			if (iCmdDataLen > YDN_CMD_DATA_LEN - 1)
			{
				LOG_YDN_ESM_E("YDN_CMD_DATA_LEN(4096) is not big enough");

				DELETE(pEvent);
				return YDN_IDLE;
			}

			/* decode and perform it */
			//TRACE("\n Begin execute YDN23 cmd!!!!\n");
			
			if(strncmp(pCmdData, "4048", 4) == 0)
			{
				iRTN = IfValidWACP(pThis, pCmdData, pThis->szCmdRespBuff);
				TRACE("iRTN(4048) is %d\n", iRTN);
				if(iRTN != NOR)
				{
					#ifdef _DEBUG_YDN_CMD_HANDLER
						TRACE("\nReturn value of execute fun is: %d\n", iRTN);
					#endif //_DEBUG_YDN_CMD_HANDLER
	   				for(i = 0; i < 4; i++)
	   	        		pThis->szCmdRespBuff[i] = '0';
	   				pThis->szCmdRespBuff[i] = '\0';
	   				frameType = (YDN_FRAME_TYPE)iRTN;
				}
				DELETE(pEvent);

				if(frameType == FRAME_ERR)
				return YDN_IDLE;

				pEvent = NEW(YDN_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_YDN_ESM_E("Memory is not enough");
					return YDN_IDLE;
				}
				
				YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);
				YDN_BuildYDNResponseFrame(pThis->szCmdRespBuff, iCID1, frameType, iAddr,
					pEvent->sData, &pEvent->iDataLength);

				ESM_SEND_DEVENT(pThis, pEvent);

				//pThis->szCmdRespBuff[0] = '\0';

				YDN_DecodeAndPerform(pThis, pCmdData);
				TRACE("Set OK!(4048)%d\n");

				return YDN_IDLE;
			}
			else if(strncmp(pCmdData, "4248", 4) == 0)
			{
				iRTN = IfValidWDCP(pThis, pCmdData, pThis->szCmdRespBuff);
				TRACE("iRTN(4248) is %d\n", iRTN);
				if(iRTN != NOR)
				{
					#ifdef _DEBUG_YDN_CMD_HANDLER
						TRACE("\nReturn value of execute fun is: %d\n", iRTN);
					#endif //_DEBUG_YDN_CMD_HANDLER
	   				for(i = 0; i < 4; i++)
	   	        		pThis->szCmdRespBuff[i] = '0';
	   				pThis->szCmdRespBuff[i] = '\0';
	   				frameType = (YDN_FRAME_TYPE)iRTN;
				}
				DELETE(pEvent);

				if(frameType == FRAME_ERR)
				return YDN_IDLE;

				pEvent = NEW(YDN_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_YDN_ESM_E("Memory is not enough");
					return YDN_IDLE;
				}
				
				YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);
				YDN_BuildYDNResponseFrame(pThis->szCmdRespBuff, iCID1, frameType, iAddr,
					pEvent->sData, &pEvent->iDataLength);

				ESM_SEND_DEVENT(pThis, pEvent);

				//pThis->szCmdRespBuff[0] = '\0';

				YDN_DecodeAndPerform(pThis, pCmdData);
				TRACE("Set OK!(4248)%d\n");

				return YDN_IDLE;
			}
			else if(strncmp(pCmdData, "E1EC", 4) == 0)//SET
			{
				//printf("\n	Enter the EC Command	\n");

				iRTN = iSetAndGetCfg_CMD_Proc(pThis, pCmdData, pThis->szCmdRespBuff);

				//printf("\n	EC  pThis->szCmdRespBuff=%s iRTN=%d	\n",pThis->szCmdRespBuff,iRTN);

				if(iRTN != NOR)
				{
					for(i = 0; i < 4; i++)
						pThis->szCmdRespBuff[i] = '0';
					pThis->szCmdRespBuff[i] = '\0';
					frameType = (YDN_FRAME_TYPE)iRTN;
				}
				DELETE(pEvent);

				if(frameType == FRAME_ERR)
					return YDN_IDLE;

				pEvent = NEW(YDN_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_YDN_ESM_E("Memory is not enough");
					return YDN_IDLE;
				}

				YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);
				YDN_BuildYDNResponseFrame(pThis->szCmdRespBuff, iCID1, frameType, iAddr,
					pEvent->sData, &pEvent->iDataLength);

				ESM_SEND_DEVENT(pThis, pEvent);

				return YDN_IDLE;
			}
			else if (strncmp(pCmdData, "E1EB", 4) == 0)//Get
			{
				//printf("\n	Enter the EB Command	\n");

				iRTN = iSetAndGetCfg_CMD_Proc(pThis, pCmdData, pThis->szCmdRespBuff);


				//printf("\n	EB  pThis->szCmdRespBuff=%s iRTN=%d	\n",pThis->szCmdRespBuff,iRTN);

				if(iRTN != NOR)
				{
					for(i = 0; i < 4; i++)
						pThis->szCmdRespBuff[i] = '0';
					pThis->szCmdRespBuff[i] = '\0';
					frameType = (YDN_FRAME_TYPE)iRTN;
				}
				DELETE(pEvent);

				if(frameType == FRAME_ERR)
					return YDN_IDLE;

				pEvent = NEW(YDN_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_YDN_ESM_E("Memory is not enough");
					return YDN_IDLE;
				}

				YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

				YDN_BuildYDNResponseFrame(pThis->szCmdRespBuff, iCID1, frameType, iAddr,
					pEvent->sData, &pEvent->iDataLength);

				ESM_SEND_DEVENT(pThis, pEvent);

				return YDN_IDLE;
			}
			/*
			else if(strncmp(pCmdData, "4145", 4) == 0)
			{
				iRTN = IfValidCRect(pThis, pCmdData, pThis->szCmdRespBuff);
				TRACE("iRTN(4145) is %d\n", iRTN);
				if(iRTN != NOR)
				{
					#ifdef _DEBUG_YDN_CMD_HANDLER
						TRACE("\nReturn value of execute fun is: %d\n", iRTN);
					#endif //_DEBUG_YDN_CMD_HANDLER
	   				for(i = 0; i < 4; i++)
	   	        		pThis->szCmdRespBuff[i] = '0';
	   				pThis->szCmdRespBuff[i] = '\0';
	   				frameType = (YDN_FRAME_TYPE)iRTN;
				}
				DELETE(pEvent);

				if(frameType == FRAME_ERR)
				return YDN_IDLE;

				pEvent = NEW(YDN_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_YDN_ESM_E("Memory is not enough");
					return YDN_IDLE;
				}
				
				YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);
				YDN_BuildYDNResponseFrame(pThis->szCmdRespBuff, iCID1, frameType,
					pEvent->sData, &pEvent->iDataLength);

				ESM_SEND_DEVENT(pThis, pEvent);

				//pThis->szCmdRespBuff[0] = '\0';

				YDN_DecodeAndPerform(pThis, pCmdData);
				TRACE("Set OK!(4145)%d\n");

				return YDN_IDLE;
			}
			*/
			/*
			else if(strncmp(pCmdData, "E180", 4) == 0)
			{
				iRTN = IfValidWSysStatus(pThis, pCmdData, pThis->szCmdRespBuff);
				TRACE("iRTN(E180) is %d\n", iRTN);
				if(iRTN != NOR)
				{
					#ifdef _DEBUG_YDN_CMD_HANDLER
						TRACE("\nReturn value of execute fun is: %d\n", iRTN);
					#endif //_DEBUG_YDN_CMD_HANDLER
	   				for(i = 0; i < 4; i++)
	   	        		pThis->szCmdRespBuff[i] = '0';
	   				pThis->szCmdRespBuff[i] = '\0';
	   				frameType = (YDN_FRAME_TYPE)iRTN;
				}
				DELETE(pEvent);

				if(frameType == FRAME_ERR)
				return YDN_IDLE;

				pEvent = NEW(YDN_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_YDN_ESM_E("Memory is not enough");
					return YDN_IDLE;
				}
				
				YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);
				YDN_BuildYDNResponseFrame(pThis->szCmdRespBuff, iCID1, frameType,
					pEvent->sData, &pEvent->iDataLength);

				ESM_SEND_DEVENT(pThis, pEvent);

				//pThis->szCmdRespBuff[0] = '\0';

				YDN_DecodeAndPerform(pThis, pCmdData);
				TRACE("Set OK!(E180)%d\n");

				return YDN_IDLE;
			}
			*/
			else
			{
				iRTN = YDN_DecodeAndPerform(pThis, pCmdData);
				if(iRTN != NOR)
				{
					#ifdef _DEBUG_YDN_CMD_HANDLER
							TRACE("\nReturn value of execute fun is: %d\n", iRTN);
					#endif //_DEBUG_YDN_CMD_HANDLER
	   				for(i = 0; i < 4; i++)
	   					pThis->szCmdRespBuff[i] = '0';
	   				pThis->szCmdRespBuff[i] = '\0';
	   				frameType = (YDN_FRAME_TYPE)iRTN;
				}
				DELETE(pEvent);
			}
			//DELETE_YDN_EVENT(pEvent);

			//return YDN_IDLE;
			break;

		case RADDR:
				pEvent = NEW(YDN_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_YDN_ESM_E("Memory is not enough");
					return YDN_IDLE;
				}
				
				YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);
				SendAddress(pEvent->sData, &pEvent->iDataLength);
				TRACE("pEvent->sData is %s\n", pEvent->sData);

				//pEvent->iDataLength = 6;

				ESM_SEND_DEVENT(pThis, pEvent);

				//pThis->szCmdRespBuff[0] = '\0';

				return YDN_IDLE;

		default:  //unexpected frame event
		    #ifdef _DEBUG_YDN_LINKLAYER
				TRACE_YDN_TIPS("OnIdle_HandleFrameEvent : received unexpected frame event");
				TRACE("\tFrame Type: %d\n", frameType);
		    #endif //_DEBUG_YDN_LINKLAYER
		     for(i = 0; i < 4; i++)
	   	        	pThis->szCmdRespBuff[i] = '0';
	   	      pThis->szCmdRespBuff[i] = '\0';
	   	      DELETE(pEvent);
		    //DELETE_YDN_EVENT(pEvent);
		    //ESM_SEND_DUMMY(pThis, pEvent);

		    //return YDN_IDLE;
		    break;
	}

	if(frameType == FRAME_ERR)//added at 2006.9.1
		return YDN_IDLE;

	pEvent = NEW(YDN_EVENT, 1);
	if (pEvent == NULL)
	{
		LOG_YDN_ESM_E("Memory is not enough");
		return YDN_IDLE;
	}
	
	YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);
	YDN_BuildYDNResponseFrame(pThis->szCmdRespBuff, iCID1, frameType, iAddr,
		pEvent->sData, &pEvent->iDataLength);

    
	ESM_SEND_DEVENT(pThis, pEvent);

	//DELETE_YDN_EVENT(pEvent);

	return YDN_IDLE;
	
}

/*==========================================================================*
 * FUNCTION : YDN_OnIdle
 * PURPOSE  : 
 * CALLS    : YDN_SendAlarm
 *			  OnIdle_HandleFrameEvent
 * CALLED BY: 
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : next YDN State, defined by YDN_STATES enum const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-17 19:23
 *==========================================================================*/
int YDN_OnIdle(YDN_BASIC_ARGS *pThis)
{
	int iRet;
	YDN_EVENT  *pEvent;

	YDN_ALARM_HANDLER *pAlarmHandler;
	pAlarmHandler = &pThis->alarmHandler; 

	BOOL bStopCommForAlarm = FALSE;  //need to stop communication for alarm occurs

	/* feed watch dog */
	
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* alarm and callback process */
	if (!pThis->bCommRunning && pAlarmHandler->bAlarmAtHand 
		&& g_YDNGlobals.CommonConfig.iMediaType == YDN_MEDIA_TYPE_MODEM)//changed at 2006.9.5
	//if (!pThis->bCommRunning && pAlarmHandler->bAlarmAtHand )
	{
		/* reset timeout counter */
		pThis->iTimeoutCounter = 0; 
		YDN_SendAlarm(pThis);
		return YDN_SEND_ALARM;
	}

	/* get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	if (iRet == ERR_QUEUE_EMPTY)  
	{
		/* In Idel state, it means timeout, keep wait in next Idle state */
		return YDN_IDLE;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_YDN_ESM_E("Get from Input Queue failed(invalid args)");
		return YDN_IDLE;
	}

	/* reset timeout counter */
	if (pEvent->iEventType != YDN_TIMEOUT_EVENT)
	{
		CLEAR_TIMEOUT_COUNTER(pThis);
	}

	/* get the flag */
	bStopCommForAlarm = (g_YDNGlobals.CommonConfig.iMediaType == YDN_MEDIA_TYPE_MODEM
	    && pAlarmHandler->bAlarmAtHand
		&& pThis->iOperationMode == YDN_MODE_SERVER) ? TRUE : FALSE;
	
     #ifdef _DEBUG_YDN_LINKLAYER
		 printf("\t YDN_OnIdle:pEvent->iEventType = %d\n", pEvent->iEventType);
	 #endif
	/* now iRet is ERR_OK */
	switch (pEvent->iEventType)
	{
	    case YDN_FRAME_EVENT:  //frame event process
			return OnIdle_HandleFrameEvent(pThis, pEvent);
	
	    case YDN_TIMEOUT_EVENT:  //timeout event process, send disconnect event
		   /*DELETE(pEvent);*/

		   if (++pThis->iTimeoutCounter > YDN_MAX_TIMEOUTCOUNT || 
			     bStopCommForAlarm)
		   {
			   int iNextState = YDN_IDLE;

               #ifdef _DEBUG_YDN_LINKLAYER
			        TRACE_YDN_TIPS("keep Timeout too long or have alarm/callback to "
				        "report, will dissconnect current communication actively");
                #endif //_DEBUG_YDN_LINKLAYER

			    pThis->iTimeoutCounter = 0;  //reset first

				/* create Disconnect event */
				pEvent = &g_YDNDiscEvent;
				
                /*deleted by ht,2006.7.12
				if (g_YDNGlobals.CommonConfig.iMediaType == 
					YDN_MEDIA_TYPE_LEASED_LINE && bStopCommForAlarm)
				{
					// not need callback when using Leased Line
					// set flag first, insure change to the client mode
					#ifdef _DEBUG_YDN_LINKLAYER
					    TRACE("Enter YDN_SEND_ALARM!!!!!");
					#endif
					YDN_SendAlarm(pThis);
					iNextState = YDN_SEND_ALARM;
				}
               */
				/* send it */
				ESM_SEND_SEVENT(pThis, pEvent);

				return iNextState;
		   }
		   else  //just send dummy
		   {
				ESM_SEND_DUMMY(pThis, pEvent);
				return YDN_IDLE;
		   }

	   case YDN_DISCONNECTED_EVENT:  //Disconnected Event
			#ifdef _DEBUG_YDN_LINKLAYER
					TRACE_YDN_TIPS("Received Disconnected Event: YDN_OnIdle");
			#endif //_DEBUG_YDN_LINKLAYER

			/*DELETE(pEvent);*/

			/* wait in another Idle state */
			return YDN_IDLE;

	  default: //unexpected event
			#ifdef _DEBUG_YDN_LINKLAYER
					TRACE_YDN_TIPS("Received unexpected event");
					TRACE("\tEvent Type: %d\n", pEvent->iEventType);
			#endif //_DEBUG_YDN_LINKLAYER

			DELETE_YDN_EVENT(pEvent);
			return YDN_IDLE;
	}	

}


/*==========================================================================*
 * FUNCTION : OnWaitForCMD_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: YDN_OnWaitForCMD
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis  : 
 *            YDN_EVENT       *pEvent : 
 * RETURN   : int : next YDN State, defined by YDN_STATES enum const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-16 13:53
 *==========================================================================*/
static int OnWaitForCMD_HandleFrameEvent(YDN_BASIC_ARGS *pThis, 
										  YDN_EVENT *pEvent)
{
	YDN_FRAME_TYPE frameType;
	unsigned char *sFrameData, *pCmdData;
	int iFrameDataLen;
	int iCMDLen, iRTN, iCID1, iAddr;

    iCMDLen = 0;
	sFrameData = pEvent->sData;
	iFrameDataLen = pEvent->iDataLength;
	iCMDLen = (int)YDN_AsciiHexToChar(sFrameData + 9);
	pCmdData = pEvent->sData + 5;
    /*
	sprintf(pCmdData, "%c%c%c%c", sFrameData[5], sFrameData[6],
		sFrameData[7], sFrameData[8]);*/

	/* frame analyse */
	frameType = YDN_AnalyseFrame(sFrameData, iFrameDataLen, pThis);
    iCID1 = (int)YDN_AsciiHexToChar(sFrameData + 5);
	iAddr = (int)YDN_AsciiHexToChar(sFrameData + 3);
    if(frameType >= REV1)
	{
        #ifdef _DEBUG_YDN_LINKLAYER
		   TRACE_YDN_TIPS("OnWaitForCMD_HandleFrameEvent : received unexpected frame event");
		   TRACE("\tFrame Type: %d\n", frameType);
        #endif //_DEBUG_YDN_LINKLAYER

	    DELETE_YDN_EVENT(pEvent);

	    ESM_SEND_DUMMY(pThis, pEvent);
	    return YDN_WAIT_FOR_CMD;
	}
    DELETE(pEvent);
	/* extract data field from select frame */
	/*iCmdDataLen = 13;
	pCmdData = YDN_ExtractDataFromFrame(sFrameData, 
		iFrameDataLen, &iCmdDataLen);*/
		
	/* create send frame event */
	pEvent = NEW(YDN_EVENT, 1);
	if (pEvent == NULL)
	{
		LOG_YDN_ESM_E("Memory is not enough");
		return YDN_IDLE;
	}

	YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

	/* decode and perform it to build the data info*/
	if(frameType == NOR)
	{
	   iRTN = YDN_DecodeAndPerform(pThis, pCmdData);
	   if(iRTN != NOR)
	   {
	   	   memset(pThis->szCmdRespBuff, '\0', sizeof(char)*YDN_RESP_SIZE);
	   	   frameType = (YDN_FRAME_TYPE)iRTN;
	   }
	}
	else
		memset(pThis->szCmdRespBuff, '\0', sizeof(char)*YDN_RESP_SIZE);

	/* build the whole response frame */
	YDN_BuildYDNResponseFrame(pThis->szCmdRespBuff, iCID1, frameType, iAddr,
		pEvent->sData, &pEvent->iDataLength);
	
		/* send it */
	ESM_SEND_DEVENT(pThis, pEvent);

	return YDN_IDLE;
}


/*==========================================================================*
 * FUNCTION : YDN_OnWaitForCMD
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: InitYDNGlobals
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : next YDN State, defined by YDN_STATES enum const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-13 11:45
 *==========================================================================*/
int YDN_OnWaitForCMD(YDN_BASIC_ARGS *pThis)
{
	int iRet;
	YDN_EVENT  *pEvent;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
		/* timeout, keep on waiting */
        #ifdef _DEBUG_YDN_LINKLAYER
		   TRACE_YDN_TIPS("Event Input Queue is empty");
        #endif //_DEBUG_YDN_LINKLAYER

		return YDN_IDLE;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_YDN_ESM_E("Get from Input Queue failed(invalid args)");
		return YDN_IDLE;
	}
	
	/* reset timeout counter */
	if (pEvent->iEventType != YDN_TIMEOUT_EVENT)
	{
		CLEAR_TIMEOUT_COUNTER(pThis);
	}

	/* now iRet is OK */
	switch (pEvent->iEventType)
	{
	    case YDN_FRAME_EVENT:  //frame event process
		    return OnWaitForCMD_HandleFrameEvent(pThis, pEvent);

	    case YDN_TIMEOUT_EVENT:  //keep on waiting
            #ifdef _DEBUG_YDN_LINKLAYER
		        TRACE_YDN_TIPS("received Timeout Event");
            #endif //_DEBUG_YDN_LINKLAYER

		/*DELETE(pEvent);*/
		/*
		//avoid locked in YDN_WAIT_FOR_POLL state
		if (++pThis->iTimeoutCounter > YDN_WAITPOLL_TIMEOUT_C)
		{
			pThis->iTimeoutCounter = 0;  //reset first
			ESM_SEND_DUMMY(pThis, pEvent);
			return YDN_IDLE;
		}
        */
		/* keep on waiting */
		    ESM_SEND_DUMMY(pThis, pEvent);
		    return YDN_WAIT_FOR_CMD;

	    case YDN_DISCONNECTED_EVENT:  //return to Idle state
            #ifdef _DEBUG_YDN_LINKLAYER
		       TRACE_YDN_TIPS("Received Disconnected Event: YDN_OnWaitForCMD");
            #endif //_DEBUG_YDN_LINKLAYER

		/*DELETE(pEvent);*/

		    return YDN_IDLE;

	    default:  //ignore others
            #ifdef _DEBUG_YDN_LINKLAYER
		        TRACE_YDN_TIPS("Received unexpected event");
		        TRACE("\tEvent Type: %d\n", pEvent->iEventType);
            #endif //_DEBUG_YDN_LINKLAYER

		    DELETE_YDN_EVENT(pEvent);
		    return YDN_IDLE;
	  }	
}

/*==========================================================================*
 * FUNCTION : OnSend
 * PURPOSE  : assistant function
 * CALLS    : 
 * CALLED BY: YDN_OnSendCallback
 *			  YDN_OnSendAlarm	
 * ARGUMENTS: BOOL            bAlarm : TRUE for send alarm,
 *									   FALSE for send callback
 *            YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 10:50
 *==========================================================================*/
static int OnSend(BOOL bAlarm, YDN_BASIC_ARGS *pThis)
{
	int iRet, iCurState;
	char szLogText[YDN_LOG_TEXT_LEN];
	char *pSendTypeMsg;

	YDN_EVENT  *pEvent;
	YDN_ALARM_HANDLER *pAlarmHandler;

	/* to load "C*" or "E*" string */
	char szSendMsg[128];

    //TRACE("\n ready to send alarm!!!\n");

	/* 1.get send type messgae for trace/log */
	if (bAlarm)
	{
		pSendTypeMsg = "YDN_OnSendAlarm";
		iCurState = YDN_SEND_ALARM;
	}
	/*
	else
	{
		pSendTypeMsg = "YDN_OnSendCallback";
		iCurState = YDN_SEND_CALLBACK;
	}
    */
	/* 2.get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
#ifdef _DEBUG_YDN_LINKLAYER
		TRACE_YDN_TIPS("InputQueue is empty");
#endif //_DEBUG_YDN_LINKLAYER

		return iCurState;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		sprintf(szLogText, "Get from InputQueue failed for invalid args(%s)",
			pSendTypeMsg);
		LOG_YDN_ESM_E(szLogText);
		return YDN_IDLE;
	}

	pAlarmHandler = &pThis->alarmHandler;

	/* now iRet is OK */
	switch (pEvent->iEventType)
	{
	case YDN_CONNECTED_EVENT:  //send C* or E*
		/* now is secure connection */
		//pThis->bSecureLine = TRUE;

#ifdef _DEBUG_YDN_LINKLAYER
		sprintf(szLogText, "received Connected Event(%s)", pSendTypeMsg);
		TRACE_YDN_TIPS(szLogText);
#endif //_DEBUG_YDN_LINKLAYER

		/*DELETE(pEvent);*/

		/* create frame event to hold report msg */
		/*
		pEvent = NEW(YDN_EVENT, 1);
		if (pEvent == NULL)
		{
			sprintf(szLogText, "Memory is not enough(%s)", pSendTypeMsg);
			LOG_YDN_ESM_E(szLogText);
			return YDN_IDLE;
		}
		YDN_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

		// get the string
		iRet = YDN_GetReportMsg(bAlarm, szSendMsg, 128);
		
		if (iRet == 1)  //call DXI interface failed
		{
			sprintf(szLogText, "Get Site Name through DXI interface failed(%s)",
				pSendTypeMsg);
			LOG_YDN_ESM_E(szLogText);
			DELETE(pEvent);

			return YDN_IDLE;
		}

		if (iRet == 2)  //buffer is too small, just give tip
		{
			sprintf(szLogText, "Site Name length is bigger than 64bytes(%s)",
				pSendTypeMsg);
			LOG_YDN_ESM_I(szLogText);
		}
		 
 		// build frame and send it
		YDN_BuildYDNResponseFrame(szSendMsg, 1, NOR, 
			pEvent->sData, &pEvent->iDataLength);

		ESM_SEND_DEVENT(pThis, pEvent);
		*/
/*deleted by ht
		if (bAlarm)  //called by OnSendAlarm state
		{
			return YDN_WAIT_FOR_ALARM_ACK;
		}
*/
		//called by OnSendCallback state
		//return YDN_WAIT_FOR_CALLBACK_ACK;
		//YDN_AlarmReported(pThis);
		pAlarmHandler->bAlarmAtHand = FALSE;
		pThis->bServerModeNeedExit = FALSE;
		pAlarmHandler->byAlarmRetryCounter = 0;
		TRACE("\nDial success.Ready to enter server mode!\n");
		pThis->iOperationMode = YDN_MODE_SERVER;

        return YDN_IDLE;

	case YDN_CONNECT_FAILED_EVENT:
#ifdef _DEBUG_YDN_LINKLAYER
		sprintf(szLogText, "received Connect Failed Event(%s)", 
			pSendTypeMsg);
		TRACE_YDN_TIPS(szLogText);
#endif //_DEBUG_YDN_LINKLAYER
		
		/*DELETE(pEvent);*/

		YDN_ClientReportFailed(pThis, TRUE);
		return YDN_IDLE;


	case YDN_DISCONNECTED_EVENT:  //drop it
#ifdef _DEBUG_YDN_LINKLAYER
		TRACE_YDN_TIPS("Received Disconnected Event: OnSend");
#endif //_DEBUG_YDN_LINKLAYER

		/*DELETE(pEvent);*/

		return iCurState;

	default:  //unexpected event
#ifdef _DEBUG_YDN_LINKLAYER
		TRACE_YDN_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_YDN_LINKLAYER

		DELETE_YDN_EVENT(pEvent);

		return iCurState;
	}  //end of switch	
}


/*==========================================================================*
 * FUNCTION : YDN_OnSendAlarm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: InitYDNGlobals
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : next YDN State, defined by YDN_STATES enum const
 * COMMENTS : 
 * CREATOR  : Hantao                   DATE: 2006-05-19 10:43
 *==========================================================================*/
int YDN_OnSendAlarm(YDN_BASIC_ARGS *pThis)
{
	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	return OnSend(TRUE, pThis);	
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : test_ydn.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "ydn.h"
#include <arpa/inet.h>			/* inet_ntoa */
/* interface defined by other modules */
BOOL EquipMonitoring_Init(IN OUT SITE_INFO *pSite);
void EquipMonitoring_Unload(IN OUT SITE_INFO *pSite);


/* for dynamic lib loading */
#include <dlfcn.h>

/* for test */
DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs);


/* eem_soc service lib name */
#define YDN_LIB  "/home/h23920/scup/src/service/ydn23/ydn23.so"


/* basic ascii code */
#define	 ACK		(0x06)
#define  NAK		(0x15)
#define  EOT		(0x04)
#define  ENQ		(0x05)
#define  SOH		(0x01)
#define  STX		(0x02)
#define  ETX        (0x03)


/* simulate app variables and functions used by libapp */
SITE_INFO	g_SiteInfo;
SERVICE_MANAGER		g_ServiceManager;
char g_szACUConfigDir[MAX_FILE_PATH] = "/home/l23632/ACU/src/config";
TIME_SRV_INFO g_sTimYDNvInfo;  

int UpdateACUTime(time_t* pTimeRet)
{
	return stime(pTimeRet);;
}
/* end of simulation */



/* print ulities */
/* use by PrintACUErrCode */
struct tagACUErrCode
{
	char *pStr;
	int  iCode;
};
typedef struct tagACUErrCode ACU_ERROR_CODE;

#define INIT_ERROR_CODE(_Struct, _pStr, _iCode)  \
		((_Struct).pStr = (_pStr), (_Struct).iCode = (_iCode))

/*==========================================================================*
 * FUNCTION : PrintACUErrCode
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-24 10:39
 *==========================================================================*/
void PrintACUErrCode(void)
{
	int i;
	ACU_ERROR_CODE errCode[20];

	/* init struct */
	INIT_ERROR_CODE(errCode[0], "ERR_COMM_OK", ERR_COMM_OK);
	INIT_ERROR_CODE(errCode[1], "ERR_COMM_NO_MEMORY", ERR_COMM_NO_MEMORY);
	INIT_ERROR_CODE(errCode[2], "ERR_COMM_TIMEOUT", ERR_COMM_TIMEOUT);
	INIT_ERROR_CODE(errCode[3], "ERR_COMM_OPENING_PORT", ERR_COMM_OPENING_PORT);
	INIT_ERROR_CODE(errCode[4], "ERR_COMM_OPENING_PARAM", ERR_COMM_OPENING_PARAM);
	INIT_ERROR_CODE(errCode[5], "ERR_COMM_SUPPORT_ACCEPT", ERR_COMM_SUPPORT_ACCEPT);
	INIT_ERROR_CODE(errCode[6], "ERR_COMM_MANY_CLIENTS", ERR_COMM_MANY_CLIENTS);
	INIT_ERROR_CODE(errCode[7], "ERR_COMM_ACCEPT_FAILURE", ERR_COMM_ACCEPT_FAILURE);
	INIT_ERROR_CODE(errCode[8], "ERR_COMM_READ_DATA", ERR_COMM_READ_DATA);
	INIT_ERROR_CODE(errCode[9], "ERR_COMM_WRITE_DATA", ERR_COMM_WRITE_DATA);
	INIT_ERROR_CODE(errCode[10], "ERR_COMM_CONNECT_SERVER", ERR_COMM_CONNECT_SERVER);
	INIT_ERROR_CODE(errCode[11], "ERR_COMM_CREATE_SERVER", ERR_COMM_CREATE_SERVER);
	INIT_ERROR_CODE(errCode[12], "ERR_COMM_PORT_HANDLE", ERR_COMM_PORT_HANDLE);
	INIT_ERROR_CODE(errCode[13], "ERR_COMM_CTRL_COMMAND", ERR_COMM_CTRL_COMMAND);
	INIT_ERROR_CODE(errCode[14], "ERR_COMM_CTRL_PARAMS", ERR_COMM_CTRL_PARAMS);
	INIT_ERROR_CODE(errCode[15], "ERR_COMM_CTRL_EXECUTION", ERR_COMM_CTRL_EXECUTION);
	INIT_ERROR_CODE(errCode[16], "ERR_COMM_BUFFER_SIZE", ERR_COMM_BUFFER_SIZE);
	INIT_ERROR_CODE(errCode[17], "ERR_COMM_CONNECTION_BROKEN", ERR_COMM_CONNECTION_BROKEN);
	INIT_ERROR_CODE(errCode[18], "ERR_COMM_STD_PORT", ERR_COMM_STD_PORT);
	INIT_ERROR_CODE(errCode[19], "ERR_COMM_LOADING_DRIVER", ERR_COMM_LOADING_DRIVER);

	/* print info */
	printf("Error Code used by Hal Comm Module:\n");
	for (i = 0; i < 20; i++)
	{
		printf("%-30s:  %08X\n", errCode[i].pStr, errCode[i].iCode);
	}

}


/*==========================================================================*
 * FUNCTION : PrintModelCfg
 * PURPOSE  : Print YDN Model Config Info
 * CALLS    : 
 * CALLED BY: PrintYDNConfigInfo
 * ARGUMENTS: void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-24 10:40
 *==========================================================================*/
void PrintModelCfg(void)
{
	/* model info has been traced in DEBUG version */
	return;

	//printf("************************************************\n");
	//printf("*****          EEM Model Config          *******\n");
	//printf("************************************************\n");
}


/*==========================================================================*
 * FUNCTION : PrintYDNConfigInfo
 * PURPOSE  : Print YDN config info
 * CALLS    : PrintCommonCfg
 *			  PrintModelCfg
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-24 10:40
 *==========================================================================*/
void PrintYDNConfigInfo(void)
{
	YDN_COMMON_CONFIG *pConfig;
	pConfig = &g_YDNGlobals.CommonConfig;

	YDN_PrintCommonCfg(pConfig);

	PrintModelCfg();
}


void TestFrameAnalysis(const char *szResData)
{
	YDN_EVENT event;
	YDN_INIT_FRAME_EVENT(&event, 0, 0, 0, 0);

	YDN_BuildEEMResponseFrame(szResData, event.sData, &event.iDataLength);

	printf("The Response Frame contents:\n");
	YDN_PrintEvent(&event);
}

/* test ulities */

/* used for TestLib */
struct tagServiceManager
{
	HANDLE hServiceLib;
	HANDLE hServiceThread;
	void   *fnServiceMain;

	SERVICE_ARGUMENTS  serviceArg;
	DWORD  dwExitCode;
};
typedef struct tagServiceManager YDN_SERVICE_MANAGER;


void RoutineBeforeExit(YDN_SERVICE_MANAGER *pManager)
{
	dlclose(pManager->hServiceLib);
	RunThread_Stop(pManager->hServiceThread, 3000, TRUE);
	printf("Service Exit Code: %d\n", (int)pManager->dwExitCode);

	return;
}
/*==========================================================================*
 * FUNCTION : TestLib
 * PURPOSE  : test YDN service as a lib
 * CALLS    : 
 * CALLED BY: RoutineBeforeExit
 * ARGUMENTS:   void : 
 * RETURN   :    int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-24 10:41
 *==========================================================================*/
int TestLib(void)
{
	/*const char *szResData = "everything is ok!";
	TestFrameAnalysis(szResData);*/


	YDN_SERVICE_MANAGER  serviceManager;
	HANDLE hLib, hFn, hThread;
	char *err;

	/* clear to zero */
	ZERO_POBJS(&serviceManager, 1);

	/* get the lib handle */
	hLib = dlopen(YDN_LIB, RTLD_LAZY);
	if (hLib == NULL)
	{
		printf("Open eem_soc.so lib failed.\n");
		return 1;
	}
	serviceManager.hServiceLib = hLib;

	/* get ServiceMain symbol */
	hFn = dlsym(serviceManager.hServiceLib,
		"ServiceMain");
	err = dlerror();
	if (err == NULL)
	{
		dlclose(hFn);
		printf("Get ServiceMain symbol from eem_soc.so failed.\n");

		return 1;
	}
	serviceManager.hServiceLib = hFn;

	/* create ServiceMain thread */
	hThread = RunThread_Create("eem-soc service",
		serviceManager.fnServiceMain,
		&(serviceManager.serviceArg),
		&(serviceManager.dwExitCode),
		0);
	if (hThread == NULL)
	{
		dlclose(hLib);
		printf("Create eem-soc service thread failed.\n");

		return 1;
	};
	serviceManager.hServiceThread = hThread;


	/* now thread has created, if exit, use RoutineBeforeExit */
    RoutineBeforeExit(&serviceManager);
 
	return 0;
}




/*==========================================================================*
 * FUNCTION : TestServiceMain
 * PURPOSE  : test YDN service directly
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-24 10:42
 *==========================================================================*/
void TestServiceMain(void)
{
	SERVICE_ARGUMENTS  serviceArg;

	/* clear to zero */
	ZERO_POBJS(&serviceArg, 1);

	serviceArg.dwExitCode = ServiceMain(&serviceArg);


#ifdef _SHOW_YDN_CONFIG_INFO
	/* print config info */
	PrintYDNConfigInfo();
#endif //_SHOW_YDN_CONFIG_INFO

	
	printf("Service now exited.\n");
	printf("Service Exit code is: %ld\n", serviceArg.dwExitCode);

	return;
}


static void TestDXI(BOOL bGet)
{
	int i;
	int nError, nBufLen;
	int nInterfaceType, nVarID, nVarSubID;
	SIG_BASIC_VALUE *pBv;
	GET_MULTIPAL_SIGNALS stGetMulSignals;

	/* test DXI get interface */
	if (bGet)
	{
		nError = 0;
		nInterfaceType = VAR_MULTIPLE_SIGNALS_INFO;
		nVarID = MUL_SIGNALS_GET_VALUE;
		nVarSubID = 0;

		memset(&stGetMulSignals, 0, sizeof(GET_MULTIPAL_SIGNALS));
		nBufLen = sizeof(GET_MULTIPAL_SIGNALS);

		stGetMulSignals.nGetNum = 7;
		stGetMulSignals.lData[0] = 0x00010302;
		stGetMulSignals.lData[1] = 0x00010303;
		stGetMulSignals.lData[2] = 0x00010304;
		stGetMulSignals.lData[3] = 0x00010305;
		stGetMulSignals.lData[4] = 0x00010306;
		stGetMulSignals.lData[5] = 0x00010307;
		stGetMulSignals.lData[6] = 0x00010009;

		printf("Before call DXI\n");
		for (i = 0; i < 7; i++)
		{
			printf("lData %d: %ld\tHex format: %08X\n", i + 1,
				stGetMulSignals.lData[i], stGetMulSignals.lData[i]);
		}
		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&stGetMulSignals,			
			0);

		if (nError == ERR_DXI_OK)
		{
			printf("Call DXI OK!\n");
		}

		for (i = 0; i < 7; i++)
		{
			printf("lData %d: %ld", i + 1, stGetMulSignals.lData[i]);
			pBv = (SIG_BASIC_VALUE*)(stGetMulSignals.lData[i]);
			printf("\t\tSig Value: %f\n",pBv->varValue.fValue);
		}
	}  //end of test DXI get interface


	/* test DXI set interface */
	else
	{
		//nInterfaceType = VAR_A_SIGNAL_VALUE;
		//nVarID = 1;  //  equip id

		//nVarSubID = 0x00010201;

		//nError = DxiSetData(nInterfaceType,
		//	nVarID,			
		//	nVarSubID,		
		//	nBufLen,			
		//	&stSetMulSignals.vData[i],			
		//	0);


	}  //end of test DXI set interface

	
	return;

}


static void TestDataExchange(HANDLE hComm)
{
	
	char sBuffer[YDN_MAX_FRAME_LEN];
	int  iLen, iErrCode;
	COMM_TIMEOUTS timeOut;


	while (TRUE)
	{
		/* set timeout */
		INIT_TIMEOUTS(timeOut, 5000, 2000);
		CommControl(hComm, 
			COMM_SET_TIMEOUTS,
			(char *)&timeOut,
			sizeof(COMM_TIMEOUTS)); 

		iLen = CommRead(hComm, sBuffer, YDN_MAX_FRAME_LEN);

		/* get last err code */
		CommControl(hComm, 
			COMM_GET_LAST_ERROR, 
			(char *)&iErrCode, 
			sizeof(int));

		/* check the error code */
		switch (iErrCode) 
		{
		case ERR_COMM_OK:
			break;

		case ERR_COMM_TIMEOUT:
			if (iLen > 0)  //  is not timeout
			{
				break;
			}

			/* send timeout event to EventQueue */
			TRACE_YDN_TIPS("Read timeout, will disconnected");
			return;

		case ERR_COMM_CONNECTION_BROKEN:
			TRACE_YDN_TIPS("connection broken!!!!!!");
			return;

		default:
			TRACE_YDN_TIPS("Read Comm Port failed");
			break ;
		}
	}
}



static void TestServer(void)
{
	HANDLE hOpenComm, hDataComm;
	int iErrCode;

	/* to get client IP */
	COMM_PEER_INFO cpi;   
	DWORD dwPeerIP = 0;
	char *szClientIP;

	BOOL bExitFlag;
	COMM_TIMEOUTS timeOut;

	char *szLibName;
	char *szOpenParam;

	szLibName = "comm_net_tcpip.so";
	szOpenParam = "127.0.0.1:10000";


	while (1)
	{
		bExitFlag = FALSE;

		/* open the comm */
		hOpenComm = CommOpen(szLibName,
			"NET0",
			szOpenParam,
			COMM_SERVER_MODE,
			5000,
			&iErrCode);

		if (iErrCode != ERR_OK)
		{
			TRACE_YDN_TIPS("Open failed!");
			TRACE("Error code: %08X\n", iErrCode);
			return;
		}
		TRACE_YDN_TIPS("Open comm port OK!!!!!"); 
		while (!bExitFlag)
		{
			/* set accept timeout */
			INIT_TIMEOUTS(timeOut, 5000, 2000);
			CommControl(hOpenComm, 
				COMM_SET_TIMEOUTS,
				(char *)&timeOut,
				sizeof(COMM_TIMEOUTS)); 

			hDataComm = CommAccept(hOpenComm);

			if (hDataComm == NULL)
			{
				/* get last err code */
				CommControl(hOpenComm, 
					COMM_GET_LAST_ERROR,
					(char *)&iErrCode, 
					sizeof(int));

				if (iErrCode == ERR_COMM_TIMEOUT)
				{
					continue;
				}
				else  /* for other err, exit programme */
				{
					TRACE_YDN_TIPS("Accept failed");
					CommClose(hOpenComm);
					return;
				}
			} /* end of if (hDataComm == NULL) */


			TRACE_YDN_TIPS("Connected");
			iErrCode = CommControl(hDataComm, 
				COMM_GET_PEER_INFO, (char *)&cpi,
				sizeof(COMM_PEER_INFO));
			if (iErrCode == ERR_COMM_OK)
			{
				// szAddr is in network format.
				dwPeerIP = *(DWORD *)cpi.szAddr;
				szClientIP = inet_ntoa(*(struct in_addr *)&dwPeerIP);
				{
					char szLogText[YDN_LOG_TEXT_LEN];
					sprintf(szLogText, "Client %s is accepted", szClientIP);
					TRACE_YDN_TIPS(szLogText);
				}
			}

			TestDataExchange(hDataComm);
			TRACE_YDN_TIPS("exit data exchange");
			bExitFlag = TRUE;

			/* close Accept Comm Handle */
			CommClose(hDataComm);
		}

	/* close Open Comm Handle */
	CommClose(hOpenComm);
	}
	
	return;
}


void TestComm(void)
{
	TestServer();
}

	
/*==========================================================================*
 * FUNCTION : main
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-24 10:43
 *==========================================================================*/
int main(void)
{
	/* open new management trace function */
	//MEM_MGR_SHOW_MSG(1);
   
	/* init ACU model */
	//if (Cfg_InitialConfig() != ERR_CFG_OK)
	//{
	//	printf("Init ACU model failed!/n");
	//	Cfg_UnloadConfig();
	//	return -1;
	//}

	/* test HAL function */
	//TestComm();
	//return;


	/* init ACU site info */
	if (!EquipMonitoring_Init(&g_SiteInfo))
	{
		printf("Init ACU Site failed.\n");
		EquipMonitoring_Unload(&g_SiteInfo);
		return -1;
	}

	/* test DXI interface */
	//TestDXI();

	/* test YDN serviceMain directly */
	TestServiceMain();


	///* unload ACU site info */
	EquipMonitoring_Unload(&g_SiteInfo);
	//Cfg_UnloadConfig();


	return 0;

}


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : linklayer_manager.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "ydn.h"
#include <arpa/inet.h>			/* inet_ntoa */


/* timeout uses in communication, unit: ms */
#define WAITFORCONN_TIMEOUT    2000  /* wait for connected timeout */
#define READCOM_TIMEOUT		   1000  
#define WRITECOM_TIMEOUT	   2000
#define CONN_TIMEOUT           5000 /* connect to remote timeout */
//#define CONN_TIMEOUT           2000

/* log Macros for Link-layer Manager sub-module(LM = linklayer manager) */
#define YDN_TASK_LM		"YDN Linklayer Manager"

/* error log */
#define LOG_YDN_LM_E(_szLogText)  LOG_YDN_E(YDN_TASK_LM, _szLogText)
	
/* extended error log(logged with error code) */
#define LOG_YDN_LM_E_EX(_szLogText, _errCode)  \
	LOG_YDN_E_EX(YDN_TASK_LM, _szLogText, _errCode)

/* warning log */
#define LOG_YDN_LM_W(_szLogText)  LOG_YDN_W(YDN_TASK_LM, _szLogText)

/* info log */
#define LOG_YDN_LM_I(_szLogText)  LOG_YDN_I(YDN_TASK_LM, _szLogText)


#define YDN_STDPORT_TCP		1
#define YDN_STDPORT_MODEM	4
#define	YDN_STDPORT_LEASEDLINE		3


/* wait for comm port has been closed */
__INLINE static void SafeOpenComm(YDN_BASIC_ARGS *pThis)
{
#ifdef COM_SHARE_SERVICE_SWITCH
	int iMediaType;
	//HANDLE hMutex;  /* not need to use Mutex */
	SERVICE_SWITCH_FLAG *pFlag;

	HANDLE hLocalThread;

	iMediaType = g_YDNGlobals.CommonConfig.iMediaType;
	pFlag = DXI_GetServiceSwitchFlag();
	hLocalThread = pThis->hThreadID[1];

	/* wait until Serial Port has closed by Maintenace Service */
	if (YDN_IS_SHARE_COM(iMediaType))
	{
		//TRACE("pFlag->bCOMHaveClosed is %d\n", pFlag->bCOMHaveClosed);
	    //TRACE("iMediaType is %d\n", iMediaType);
		while (1)
		{
			if (pFlag->bCOMHaveClosed)
			{
				pFlag->bCOMHaveClosed = FALSE;
				pFlag->bHLMSUseCOM = TRUE;

				break;
			}
			else
			{	
#ifdef _DEBUG_YDN_LINKLAYER
				//TRACE_YDN_TIPS("Serial Port has not been Closed");
				TRACE("Serial Port has not been closed!!\n");
#endif //_DEBUG_YDN_LINKLAYER

				Sleep(3000);

				/* feed watch dog */
				RunThread_Heartbeat(hLocalThread);
			}
		}
	}

#endif //COM_SHARE_SERVICE_SWITCH

	return;
}

void LeaveCommUsing(void)
{
#ifdef COM_SHARE_SERVICE_SWITCH
	int iMediaType;
	//HANDLE hMutex;  /* not need to use Mutex */
	SERVICE_SWITCH_FLAG *pFlag;

	iMediaType = g_YDNGlobals.CommonConfig.iMediaType;
	pFlag = DXI_GetServiceSwitchFlag();

	//if (YDN_IS_SHARE_COM(iMediaType))
	{
	    TRACE("iMediaType:LeaveCommUsing is %d\n", iMediaType);
		pFlag->bCOMHaveClosed = TRUE;
	}

#endif //COM_SHARE_SERVICE_SWITCH

	return;
}

/*==========================================================================*
 * FUNCTION : ComQueue
 * PURPOSE  : communicate with Event Queues
 * CALLS    : Queue_Get
 *			  Queue_Put
 *			  CommWrite
 * CALLED BY: ComMC
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis       : 
 *            BOOL            bSendTimeOut : 
 * RETURN   : int : YDN_COMM_STATUS 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 14:20
 *==========================================================================*/
static int ComQueue(YDN_BASIC_ARGS *pThis, BOOL bSendTimeOut)
{
	YDN_EVENT *pEvent;
	int iRet, iCommWriteRet;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[1]);

	if (bSendTimeOut) /* send timeout event to EventInputQueue */
	{
		pEvent = &g_YDNTimeoutEvent;

		/* send it to the EventInputQueue */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);

		if (iRet != ERR_OK)
		{
			LOG_YDN_LM_E_EX("send event to Event InputQueue failed", iRet);

			/* exit YDN service */
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

			return YDN_COMM_STATUS_DISCONNECT;
		}
	}

	iRet = Queue_Get(pThis->hEventOutputQueue, &pEvent, FALSE, WAIT_FOR_O_QUEUE);
	//TRACE("\n iRet:ComQueue is %d\n", iRet);

#ifdef _TEST_YDN_QUEUE_SYNCHRONIZATION
	{
		int iCount;
		YDN_EVENT *pLocalEvent;
		iCount = Queue_GetCount(pThis->hEventOutputQueue, NULL);
		if (iCount > 0)
		{
			TRACEX("Output Queue: Count is %d\n", iCount);
			TRACE("Time is: %.3f\n", GetCurrentTime());
			Queue_Get(pThis->hEventOutputQueue, &pLocalEvent, TRUE, 1000);
			YDN_PrintEvent(pLocalEvent);
		}

		iCount = Queue_GetCount(pThis->hEventInputQueue, NULL);
		if (iCount > 0)
		{
			TRACEX("Input Queue: Count is %d\n", iCount);
			TRACE("Time is: %.3f\n", GetCurrentTime());
			Queue_Get(pThis->hEventInputQueue, &pLocalEvent, TRUE, 1000);
			YDN_PrintEvent(pLocalEvent);
		}
	}
#endif //_TEST_YDN_QUEUE_SYNCHRONIZATION

	switch (iRet)
	{
	case ERR_OK:
		switch (pEvent->iEventType)
		{
		case YDN_FRAME_EVENT:
			if (pEvent->bDummyFlag)
			{
				/* sleep 100 ms in lest two frame are sent together */
				/* normally, byte-read-delay is 50 ms */
				Sleep(100);
				return YDN_COMM_STATUS_NEXT;
			}
			else
			{
#ifdef _DEBUG_YDN_LINKLAYER
				{
					int i, iLen;
					unsigned char chr;
					TRACE_YDN_TIPS("received frame event from OutputQueue, "
						"write the Data to Comm Port");
					iLen = pEvent->iDataLength;
					TRACE("\tData Length: %d\n", iLen);

					TRACE("\tData Content(Hex format):");
					for (i = 0; i < iLen; i++)
					{
						printf("%02X ", pEvent->sData[i]);
					}
					printf("\n");

					TRACE("\tData Content(Text format):");
					for (i = 0; i < iLen; i++)
					{
						chr = (unsigned char)pEvent->sData[i];
						if (chr < 0x21 || chr > 0X7E)
						{
							printf("%s", YDN_GetUnprintableChr(chr));
						}
						else
						{
							printf("%c", chr);
						}
					}
					printf("\n");

				}					
#endif //_DEBUG_YDN_LINKLAYER
		
				int		iRst;

				CommWrite(pThis->hComm, 
					pEvent->sData, 
					pEvent->iDataLength);
				//DELETE(pEvent);

				/* get the error code */
				iCommWriteRet = CommGetLastError(pThis->hComm);
				if (iCommWriteRet != ERR_OK)
				{
					LOG_YDN_LM_E_EX("Write to Comm Port failed", 
						iCommWriteRet);

					/* When timeout, not exit. */
					if (iCommWriteRet != ERR_TIMEOUT)
					{
						/* exit YDN service */
						pThis->bServerModeNeedExit = TRUE;
						pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
					}

					iRst = YDN_COMM_STATUS_DISCONNECT;
				}


				else if (pEvent->bSkipFlag)
				{
					iRst = YDN_COMM_STATUS_SKIP;
				}
				else
				{
					iRst = YDN_COMM_STATUS_NEXT;
				}

				DELETE(pEvent);
				return iRst;
			}

		case YDN_DISCONNECTED_EVENT:
			TRACE_YDN_TIPS("Disconnected event received from "
				"EventOutputQueue");
			/*DELETE(pEvent);*/
			return YDN_COMM_STATUS_DISCONNECT;

		default:
			TRACE_YDN_TIPS("received none-expected event from EventOutputQueue");

			DELETE_YDN_EVENT(pEvent);
			return YDN_COMM_STATUS_NEXT;
		}

	case ERR_QUEUE_EMPTY: //means timeout
		TRACE_YDN_TIPS("get event from EventOutputQueue timeout");

		return YDN_COMM_STATUS_NEXT;

	default:         /* is ERR_INVALID_ARGS */ 
		LOG_YDN_LM_E_EX("Get event from Event OutputQueue failed", iRet);

		/* exit YDN service */
		pThis->bServerModeNeedExit = TRUE;
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

		return YDN_COMM_STATUS_DISCONNECT;
	}
}


/*==========================================================================*
 * FUNCTION : PickupAFrame
 * PURPOSE  : Pickup a frame from input data buffer
 * CALLS    : 
 * CALLED BY: CommReadFrame
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis        : 
 *            int             iProtocolType : 
 *            char            *pDstBuf      : 
 *            int             *piLen        : 
 * RETURN   : BOOL : TRUE means get a frame from the received buf
 *					 FALSE means received buf is empty
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-12 09:55
 *==========================================================================*/
static BOOL PickupAFrame(YDN_BASIC_ARGS *pThis, int iProtocolType, 
						 char *pDstBuf, int *piLen)
{
	int iRet = FALSE;
	int iSrcPos, iSrcLen;
	char *pSrcBuf;

	iSrcPos = pThis->iCurPos;
	iSrcLen = pThis->iLen;
	//iSrcLen = strlen(pThis->sReceivedBuff);
	pSrcBuf = pThis->sReceivedBuff;
	*piLen = 0;

	if (iSrcPos < iSrcLen)
	{
		iRet = TRUE;
		switch (iProtocolType)
		{
		    //need extra code
		    case YDN:

#ifdef	_DEBUG_YDN
	    		if((iSrcLen) > MAX_YDNFRAME_LEN)
				{
					AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: PickupAFrame iSrcLen greater than MAX_YDNFRAME_LEN.\n");
				}
#endif

			   memcpy(pDstBuf, pSrcBuf + iSrcPos, (size_t)iSrcLen);
			   *piLen = iSrcLen;
			   iSrcPos = iSrcLen;
			   //YDN_PrintfStr(iSrcLen, pThis->sReceivedBuff);
		}
		/* to delete */
		if (iSrcPos < iSrcLen)
		{
			TRACE("Split a frame!\n");
		}
		/* end */
	}

	pThis->iCurPos = iSrcPos;
	return iRet;
}


/*==========================================================================*
 * FUNCTION : CommReadFrame
 * PURPOSE  : 
 * CALLS    : PickupAFrame
 * CALLED BY: ComMC
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis   : 
 *            char            *pBuffer : 
 *            int             *piLen   : 
 *            int             iTimeOut : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-12 09:56
 *==========================================================================*/
static int CommReadFrame(YDN_BASIC_ARGS *pThis, char *pBuffer, 
						 int *piLen, int iTimeOut)
{
	HANDLE hComm;
	int    iProtocolType, iErrCode;
	COMM_TIMEOUTS timeOut;

	hComm = pThis->hComm;
	iErrCode = ERR_COMM_OK;
	iProtocolType = g_YDNGlobals.CommonConfig.iProtocolType;

	if (!PickupAFrame(pThis, iProtocolType, pBuffer, piLen))  /* received buff is empty */
	{
		/* set timeout */
		INIT_TIMEOUTS(timeOut, iTimeOut, WRITECOM_TIMEOUT);
		CommControl(hComm, 
			COMM_SET_TIMEOUTS,
			(char *)&timeOut,
			sizeof(COMM_TIMEOUTS)); 

		pThis->iCurPos = 0;
	 	pThis->iLen = CommRead(hComm, pThis->sReceivedBuff, MAX_YDNFRAME_LEN);
		//pThis->iLen = CommRead(hComm, pThis->sReceivedBuff, 512);
		//TRACE("pThis->iLen:CommRead is %d\n", pThis->iLen);
		//YDN_PrintfStr(pThis->iLen, pThis->sReceivedBuff);
		/* get last err code */
		CommControl(hComm, 
			COMM_GET_LAST_ERROR, 
			(char *)&iErrCode, 
			sizeof(int));

		PickupAFrame(pThis, iProtocolType, pBuffer, piLen);
	}
	return iErrCode;
}


/*==========================================================================*
 * FUNCTION : ComMC
 * PURPOSE  : communicate with MC
 * CALLS    : CommRead 
 *			  Queue_Put  
 *			  ComQueue
 * CALLED BY: DataExchange
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis    : 
 *            int             iTimeOut  : 
 *            BOOL            bSkipFlag : 
 * RETURN   : int : YDN_COMM_STATUS
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 13:05
 *==========================================================================*/
static int ComMC(YDN_BASIC_ARGS *pThis, int iTimeOut, BOOL bSkipFlag)
{
	HANDLE hComm;
	//char cFrame[128] = {'\0'};
	BOOL bHaveEOI = FALSE;
	BOOL bHaveSOI = FALSE;
	char sBuffer[MAX_YDNFRAME_LEN] = {'\0'};
	//char sBuffer[512];
	int  iLen, iErrCode, iRet, i, j, k;
	YDN_EVENT *pEvent;

	hComm = pThis->hComm;

	i = 0;
	j = 0;
	k = 0;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[1]);

	/* skip the step to keep synchronization */
	if (bSkipFlag)
	{
		return ComQueue(pThis, FALSE);
	}
	iErrCode = CommReadFrame(pThis, sBuffer, &iLen, iTimeOut);
	//TRACE("iErrCode is %d\n", iErrCode);
	//YDN_PrintfStr(iLen, sBuffer);
    #ifdef _DEBUG_YDN_LINKLAYER
		  TRACE("\t CommReadFrame:iErrcode = %d\n", iErrCode);
    #endif //_DEBUG_YDN_LINKLAYER
	/* check the error code */
	switch (iErrCode) 
	{
	    case ERR_COMM_OK:
		    break;

	    case ERR_COMM_TIMEOUT:
		    if (iLen > 0)  //  is not timeout
		    {
	    	    for(i = 0; i < MAX_YDNFRAME_LEN; i++)
	    	    {
	    	       if(sBuffer[i] == SOI)
	    	       {
				       bHaveSOI = TRUE;
	    	   	      j = i;
	    	          for(k = j + 1; k < MAX_YDNFRAME_LEN; k++)
	    	          {
	    	             if(sBuffer[k] == EOI)
	    	             {
	    	       	        bHaveEOI = TRUE;
	    	       	        break;
	    	             }
	    	         }
	    	       }
	    	       else if(sBuffer[i] == SOI_RADDR)
	    	       {
					   if(i >= MAX_YDNFRAME_LEN - 1)
					   {
						   break;
					   }
	    	           j = i;
	    	           if(sBuffer[i + 1] == EOI)
	    	           {
	    	       	        bHaveEOI = TRUE;
	    	       	        k = i + 1;
	    	       	        break;
	    	           }
	    	       }
	    	       if(bHaveEOI)
	    	   	   break;
	    	    }
	    	    if(((bHaveSOI) && (MAX_YDNFRAME_LEN - j < 18)) || (!bHaveEOI))
	    		   return ComQueue(pThis, TRUE);
	    	    else
	    	    {

//#ifdef	_DEBUG_YDN
//	    		   if((k - j + 1) > 128)
//				   {
//					   AppLogOut("YDN SRV", APP_LOG_ERROR, "***Error: k - j + 1 greater than 128.\n");
//					
//				   }
//#endif
					//memcpy(cFrame, sBuffer + j, k - j + 1);
	    		   iLen = k - j + 1;
	    	    }
			    break;
		    }

            /* send timeout event to EventQueue */
            #ifdef _DEBUG_YDN_LINKLAYER
		         TRACE_YDN_TIPS("read from MC timeout, send Timeout Event to "
			"the InputQueue");
            #endif //_DEBUG_YDN_LINKLAYER

		    return ComQueue(pThis, TRUE);

	    case ERR_COMM_CONNECTION_BROKEN:

            #ifdef _DEBUG_YDN_LINKLAYER
		        TRACE_YDN_TIPS("connection closed by MC");
            #endif //_DEBUG_YDN_LINKLAYER

		    return YDN_COMM_STATUS_DISCONNECT;

	    default:
			TRACE("The received string is : %s\n", sBuffer);
		    LOG_YDN_LM_E_EX("Read Comm Port failed", iErrCode);

		    /* exit YDN service */
		    pThis->bServerModeNeedExit = TRUE;
		    pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

		    return YDN_COMM_STATUS_DISCONNECT;
	}

    #ifdef _DEBUG_YDN_LINKLAYER
	{
		int i;
		unsigned char chr;

		TRACE_YDN_TIPS("Comm Port received data");
		TRACE("\tData Length: %d\n", iLen);

		TRACE("\tData Content(Hex format):");
		for (i = 0; i < iLen; i++)
		{
			printf("%02X ", sBuffer[i]);
		}
		printf("\n");

		TRACE("\tData Content(Text format):");
		for (i = 0; i < iLen; i++)
		{
			chr = (unsigned char)sBuffer[i];
			if (chr < 0x21 || chr > 0X7E)
			{
				printf("%s", YDN_GetUnprintableChr(chr));
			}
			else
			{
				printf("%c", chr);
			}
		}
		printf("\n");

	}
    #endif //_DEBUG_YDN_LINKLAYER

	pEvent = NEW(YDN_EVENT, 1);
	if (pEvent == NULL)
	{
		LOG_YDN_LM_E("no memory to create Frame Event");
		
		/* exit YDN service */
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_MEMORY;

		return YDN_COMM_STATUS_DISCONNECT;
	}
	YDN_INIT_FRAME_EVENT(pEvent, iLen, sBuffer + j, 0, 0);


	//YDN_INIT_FRAME_EVENT(pEvent, iLen, cFrame, 0, 0);

    
    #ifdef _DEBUG_YDN_LINKLAYER
	    TRACE("The received data will sent to InputQueue as Frame Event.\n");
		YDN_PrintEvent(pEvent);
    #endif //_DEBUG_YDN_LINKLAYER
    //TRACE("the address of pEvent(CMD) is %p\n", pEvent);
	/* send it to the EventInputQueue */
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_YDN_LM_E_EX("Send Frame event to InputQueue failed", iErrCode);

		pThis->bServerModeNeedExit = TRUE;
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
	
		DELETE(pEvent);
		pEvent = NULL;
		return YDN_COMM_STATUS_DISCONNECT;
	}
	return ComQueue(pThis, FALSE);


}
			

/*==========================================================================*
 * FUNCTION : DataExchange
 * PURPOSE  : data exchange between MC and Event Queues
 * CALLS    : ComMC
 * CALLED BY: RunAsServer
 *			  RunAsClient
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 11:50
 *==========================================================================*/
static void DataExchange(YDN_BASIC_ARGS *pThis)
{
	int iRet;
	BOOL bSkipFlag;

	bSkipFlag = FALSE;

	while (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		iRet = ComMC(pThis, READCOM_TIMEOUT, bSkipFlag);
        #ifdef _DEBUG_YDN_LINKLAYER
		   TRACE("DataExchange->ComMC: %d\n", iRet);
        #endif //_DEBUG_YDN_LINKLAYER
		switch (iRet)
		{
		    case YDN_COMM_STATUS_DISCONNECT:
			    return;

		    case YDN_COMM_STATUS_SKIP:
                #ifdef _DEBUG_YDN_LINKLAYER
					TRACE_YDN_TIPS("skip ComMC");
					TRACE("returned value: %d\n", iRet);
                #endif //_DEBUG_YDN_LINKLAYER
			    bSkipFlag = TRUE;
			break;

		    case YDN_COMM_STATUS_NEXT:
				bSkipFlag = FALSE;
				break;
		}
	}		
}


/*==========================================================================*
 * FUNCTION : RunAsServer
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: YDN_LinkLayerManager
 * ARGUMENTS: YDN_BASIC_ARGS *  pThis      : 
 *            PORT_INFO         *pCommPort : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-10 21:27
 *==========================================================================*/
static void RunAsServer(YDN_BASIC_ARGS *pThis, PORT_INFO *pCommPort)
{
	HANDLE hOpenComm, hDataComm;
	int iErrCode, iTryCount;

	/* to get YDN common config info */
	YDN_COMMON_CONFIG *pCommonCfg;
	int iMediaType;   

	/* to send disconnected event */
	int iRet;
	YDN_EVENT *pEvent;

    /* to get client IP */
	COMM_PEER_INFO cpi;   
	DWORD dwPeerIP = 0;
	char *szClientIP;

	HANDLE hLocalThread;
	COMM_TIMEOUTS timeOut;

	char *szLibName;
	char szOpenParam[64];

	hLocalThread = pThis->hThreadID[1];
	pCommonCfg = &g_YDNGlobals.CommonConfig;
	iMediaType = pCommonCfg->iMediaType;

	/* reset flag */
	pThis->bServerModeNeedExit = FALSE;
	
	if (iMediaType == YDN_MEDIA_TYPE_TCPIP)
	{
		pCommPort->iStdPortTypeID = YDN_STDPORT_TCP;
		//strcpy(szOpenParam, "142.100.4.35:");
		strcpy(szOpenParam, "127.0.0.1:");
	}
	else if (iMediaType == YDN_MEDIA_TYPE_LEASED_LINE)
	{
		pCommPort->iStdPortTypeID = YDN_STDPORT_LEASEDLINE;
	}
	else
	{
		pCommPort->iStdPortTypeID = YDN_STDPORT_MODEM;
	}
    
	/* get lib name and port param */
	szLibName = GetStandardPortDriverName(pCommPort->iStdPortTypeID);
	/* get comm port param */
	if (iMediaType == YDN_MEDIA_TYPE_TCPIP)
	{
		strncat(szOpenParam, pCommonCfg->szCommPortParam,
			sizeof(szOpenParam));
	}
	else
	{
		strncpyz(szOpenParam, pCommonCfg->szCommPortParam,
				sizeof(szOpenParam));
	}

    #ifdef _DEBUG_YDN_LINKLAYER
		TRACE_YDN_TIPS("CC Comm Port Info:");
		TRACE("\tComm Port ID: %d\n", pCommPort->iPortID);
		TRACE("\tStdPort ID: %d\n", pCommPort->iStdPortTypeID);
		TRACE("\tStdPort accessing libname: %s\n", szLibName);
		TRACE("\tOpen Param: %s\n", szOpenParam);
		TRACE("\tpiTimeout: %d\n", pCommPort->iTimeout);
		TRACE("\tpszDeviceDesp: %s\n", pCommPort->szDeviceDesp);
		TRACE("\szPortName: %s\n", pCommPort->szPortName);
	#endif //_DEBUG_YDN_LINKLAYER

	SafeOpenComm(pThis);
    //TRACE("Get out of SafeOpenComm!!\n");
	//printf("szLibName is %s\n", szLibName);
	//printf("pCommPort->szDeviceDesp is %s\n", pCommPort->szDeviceDesp);
	/* open the comm, retry 3 times */
	for (iTryCount = 0; iTryCount < 3; iTryCount++)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);
		
		hOpenComm = CommOpen(szLibName,
			pCommPort->szDeviceDesp,
			szOpenParam,
			COMM_SERVER_MODE,
			pCommPort->iTimeout,
			&iErrCode);
        TRACE("\niErrCode is: servermode %u\n", iErrCode);
		switch (iErrCode)
		{
		    case ERR_COMM_STD_PORT:
			    LOG_YDN_LM_E("Open Comm Port failed for StdPort ID is invalid");

			    /* exit the service */
			    pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
			    return;

		    case ERR_COMM_OPENING_PARAM:
				TRACE("\tiErrCode: %d\n", iErrCode);
				LOG_YDN_LM_E("Open Comm Port failed for Opening Param is invalid");
	            
				/* exit programme */
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_CFG;
				return;

		   case ERR_OK:
				iTryCount = 3;  /* break while loop */
				break;

		   default:  /* do nothing, just retry */
				Sleep(1000); 
				break;
		}
	}


	if (iErrCode != ERR_OK)
	{
		LOG_YDN_LM_E_EX("Open failed after 3 times retry", iErrCode);

		/* exit programme */
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
		return;
	}

	#ifdef _DEBUG_YDN_LINKLAYER
		TRACE_YDN_TIPS("Open comm port OK, now begin to wait for connected");
	#endif //_DEBUG_YDN_LINKLAYER

	/* main loop */
	INIT_TIMEOUTS(timeOut, WAITFORCONN_TIMEOUT, WRITECOM_TIMEOUT);
	while (!pThis->bServerModeNeedExit &&
		     pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);

		Sleep(50);

        //pThis->bSecureLine = TRUE;
		
		/* set accept timeout */
		CommControl(hOpenComm, 
			COMM_SET_TIMEOUTS,
			(char *)&timeOut,
			sizeof(COMM_TIMEOUTS)); 

		hDataComm = CommAccept(hOpenComm);

		if (hDataComm == NULL)
		{
			/* get last err code */
			CommControl(hOpenComm, 
				        COMM_GET_LAST_ERROR,
						(char *)&iErrCode, 
						sizeof(int));
			//TRACE("\niErrCode1 is: %d", iErrCode);

			if (iErrCode == ERR_COMM_TIMEOUT)
			{
				continue;
			}
			else  /* for other err, exit programme */
			{
				LOG_YDN_LM_E_EX("Accept failed", iErrCode);
				
				CommClose(hOpenComm);
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
                TRACE("\nErr1!!\n");
                 
				return;
			}
		} /* end of if (hDataComm == NULL) */

		/* successfully got hDataComm */
		/* check mode, if has something to report, return */ 
		/* Note: mode can be changed when bCommRunning is FALSE */
		if (pThis->iOperationMode != YDN_MODE_SERVER)
		{
		    TRACE("\n Run Serial_CommClose1!!\n");
			CommClose(hDataComm);
			CommClose(hOpenComm);
			return;
		}
		else
		{
			pThis->hComm = hDataComm;
		}

		TRACE_YDN_TIPS("Connected");

		/* set Running flag first to inhibit report */
		pThis->bCommRunning = TRUE;


        // set security flag
		switch (iMediaType)
		{
		case YDN_MEDIA_TYPE_LEASED_LINE:  //alaways secure
			pThis->bSecureLine = TRUE;
			break;

		case YDN_MEDIA_TYPE_MODEM:  //need callback
			pThis->bSecureLine = TRUE; 
			break;

		case YDN_MEDIA_TYPE_TCPIP:  //check secure IP list
			pThis->bSecureLine = TRUE;

			iErrCode = CommControl(hDataComm, 
				COMM_GET_PEER_INFO, (char *)&cpi,
				sizeof(COMM_PEER_INFO));
			//TRACE("\niErrCode2 is: %d", iErrCode);
			if (iErrCode == ERR_COMM_OK)
			{
				// szAddr is in network format.
				dwPeerIP = *(DWORD *)cpi.szAddr;
				szClientIP = inet_ntoa(*(struct in_addr *)&dwPeerIP);
	
				//TRACE("\nszClientIP is: %s\n",szClientIP);
#ifdef _DEBUG_YDN_LINKLAYER
				{
					char szLogText[YDN_LOG_TEXT_LEN];
					sprintf(szLogText, "Client %s is accepted", szClientIP);
					TRACE_YDN_TIPS(szLogText);
				}
#endif //_DEBUG_YDN_LINKLAYER

/*
				for (i = 0; i < ESR_SECURITY_IP_NUM; i++)
				{
					if (strcmp(szClientIP, szSecurityIP[i]) == 0)
					{
						pThis->bSecureLine = TRUE;
						break;
					}
				}
*/	
			}

			break;

		default:  //do nothing
			break;
		}

		/* clear InputQueue */
		YDN_ClearEventQueue(pThis->hEventInputQueue, FALSE);

		/* communication begin */
		#ifdef _DEBUG_YDN_LINKLAYER
	        TRACE("RunAsServer->DataExchange");
        #endif //_DEBUG_YDN_LINKLAYER

		DataExchange(pThis);
		pThis->bCommRunning = FALSE;
		/* communication end */

		/* clear OutputQueue */
		YDN_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

		pEvent = &g_YDNDiscEvent;

		/* send it */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
		if (iRet != ERR_OK)
		{
			LOG_YDN_LM_E("Put event to the Input Queue failed");
			/*DELETE(pEvent);*/
		}
		/* close Accept Comm Handle */
		//TRACE("\n Run Serial_CommClose2!!\n");
		CommClose(hDataComm);
	}

	/* close Open Comm Handle */
	TRACE("\n Run Serial_CommClose3!!\n");
	CommClose(hOpenComm);

	return;
}


/*==========================================================================*
 * FUNCTION : GetCommOpenParam
 * PURPOSE  : assistant function to get open params for CommOpen interface 
 *			  when in Client Mode
 * CALLS    : 
 * CALLED BY: RunAsClient
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis       : 
 *            PORT_INFO       *pCommPort   : 
 *            char            *szOpenParam : 
 *            int             iBufLen      : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 18:41
 *==========================================================================*/
static void GetCommOpenParam(YDN_BASIC_ARGS *pThis,
							 PORT_INFO *pCommPort,
							 char *szOpenParam,
							 int iBufLen)
{
	YDN_COMMON_CONFIG *pCommonCfg;
	int iMediaType;
	BYTE byCurReportType;

	/* init local variable */
	pCommonCfg = &g_YDNGlobals.CommonConfig;
	iMediaType = pCommonCfg->iMediaType;
	byCurReportType = pThis->alarmHandler.byCurReportRouteType;

	switch (iMediaType)
	{
	case YDN_MEDIA_TYPE_MODEM:
		/* get baud rate (szAccessingDriverSet: 9600,n,8,1:86011382)*/
		Cfg_SplitString(pCommPort->szAccessingDriverSet,
			szOpenParam, iBufLen, ':');
		/* now szOpenParam ended with null */
		strncat(szOpenParam, 
			    ":",
				iBufLen - strlen(szOpenParam) - 1); /* ensure end with null */

		switch (byCurReportType)
		{
		case YDN_ROUTE_ALARM_1:
			strncat(szOpenParam,
				    pCommonCfg->szAlarmReportPhoneNumber[0],
					iBufLen - strlen(szOpenParam) - 1);
			break;

		case YDN_ROUTE_ALARM_2:
			strncat(szOpenParam,
				    pCommonCfg->szAlarmReportPhoneNumber[1],
					iBufLen - strlen(szOpenParam) - 1);
			break;

		case YDN_ROUTE_ALARM_3:
			strncat(szOpenParam,
				    pCommonCfg->szAlarmReportPhoneNumber[2],
					iBufLen - strlen(szOpenParam) - 1);
			break;

		default:
			/* error */
			szOpenParam[0] = '\0';
		}
		break;

	case YDN_MEDIA_TYPE_TCPIP:
		/* TCPIP param format: 100.100.100.1:23 */
		switch (byCurReportType)
		{
			//deleted by ht,2006.5.12
		/*case YDN_ROUTE_ALARM_1:
			strncpyz(szOpenParam, pCommonCfg->szReportIP[0], iBufLen);
			break;

		case YDN_ROUTE_ALARM_2:
			strncpyz(szOpenParam, pCommonCfg->szReportIP[1], iBufLen);
			break;*/

		default:
			/* error */
			szOpenParam[0] = '\0';
		}
		break;
		
	case YDN_MEDIA_TYPE_LEASED_LINE:
		/* Leased line open format:   9600,n,8,1 */
		strncpyz(szOpenParam, pCommonCfg->szCommPortParam, iBufLen);
		break;

	default:  /* error */
		szOpenParam[0] = '\0';
	}

	return;
}


/*==========================================================================*
 * FUNCTION : RunAsClient
 * PURPOSE  : 
 * CALLS    : GetCommOpenParam
 * CALLED BY: YDN_LinkLayerManager
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis     : 
 *            PORT_INFO       *pCommPort : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 19:55
 *==========================================================================*/
static void RunAsClient(YDN_BASIC_ARGS *pThis, PORT_INFO *pCommPort)
{
	char szOpenParam[64];  /* note: the buffer size should be enough */
	int  iErrCode, iRet;
	HANDLE hLocalThread, hDataComm;
	char *szLibName;

	char szLogText[YDN_LOG_TEXT_LEN];

	YDN_EVENT *pEvent;

	/* get Linklayer thread id */
	hLocalThread = pThis->hThreadID[1];

	/* prepare port open param */
	//TRACE("enter GetCommOpenParam!!");
	GetCommOpenParam(pThis, pCommPort, szOpenParam, 64);

	/* feed watch dog */
	RunThread_Heartbeat(hLocalThread);

    //TRACE("enter SafeOpenComm!!\n");
	SafeOpenComm(pThis);

	/* connect to remote */
	//TRACE("enter GetStandardPortDriverName!!\n");
	szLibName = GetStandardPortDriverName(pCommPort->iStdPortTypeID);
	TRACE("enter CommOpen!!\n");
	TRACE("szLibName is %s\n", szLibName);
	//TRACE("szOpenParam is %s\n", szOpenParam);
	//TRACE("pCommPort->iTimeout is %d\n", pCommPort->iTimeout);

	hDataComm = NULL;
	
	hDataComm = CommOpen(szLibName,
		            pCommPort->szDeviceDesp,
					szOpenParam,
					COMM_CLIENT_MODE,
					pCommPort->iTimeout,
					&iErrCode);

#ifdef _DEBUG_YDN_LINKLAYER
	TRACE_YDN_TIPS("MC Comm Port Info:");
	TRACE("\tComm Port ID: %d\n", pCommPort->iPortID);
	TRACE("\tStdPort ID: %d\n", pCommPort->iStdPortTypeID);
	TRACE("\tStdPort accessing libname: %s\n", szLibName);
	TRACE("\tOpen Param: %s\n", szOpenParam);
#endif //_DEBUG_YDN_LINKLAYER

	/* connect to remote takes time, feed watch dog again */
	RunThread_Heartbeat(hLocalThread);

	if (iErrCode != ERR_OK)  // init connect failed event
	{
		snprintf(szLogText, YDN_LOG_TEXT_LEN, "Connect to remote failed"
			"(Error Code: %X)", iErrCode);
		TRACE_YDN_TIPS(szLogText);
		TRACE("CommOpen at client is failed!!\n");
		/*YDN_INIT_NONEFRAME_EVENT(pEvent, YDN_CONNECT_FAILED_EVENT);*/
		pEvent = &g_YDNConnFailEvent;
	}

	else  //init connected event
	{
		/* clear InputQueue */
		YDN_ClearEventQueue(pThis->hEventInputQueue, FALSE);
		pEvent = &g_YDNConnEvent;
	}

	/* send it to the input queue */
	TRACE("enter Queue_Put1!!\n");
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_YDN_LM_E("Put event to the Input Queue failed");
	}

	/* if not connected, return */
	if (iErrCode != ERR_OK)
	{
		/* keep sychrnization */
		Sleep(50);
		return;
	}


	pThis->hComm = hDataComm;
	
	/* communication begin */
	pThis->bCommRunning = TRUE;

	/* now is security connect */

	/* send "CallBack and Report Alarms" frame from output queue */
	TRACE("enter ComQueue!!\n");
	ComQueue(pThis, FALSE);

    #ifdef _DEBUG_YDN_LINKLAYER
	        TRACE("RunAsClient->DataExchange");
    #endif //_DEBUG_YDN_LINKLAYER
    TRACE("enter DataExchange!!\n");
	DataExchange(pThis);

	/* communication end */
	/*pThis->bSecureLine = FALSE;*/
	pThis->bCommRunning = FALSE;

	/* clear OutputQueue */
	TRACE("enter YDN_ClearEventQueue!!\n");
	YDN_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

	pEvent = &g_YDNDiscEvent;
	/* send it */
	TRACE("enter Queue_Put2!!\n");
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_YDN_LM_E("Put event to the Input Queue failed");
		/*DELETE(pEvent);*/
	}

   // TRACE("\n Run Serial_CommClose:RunAsClient!!\n");
	CommClose(hDataComm);
	
	return;
}


/*==========================================================================*
 * FUNCTION : YDN_LinkLayerManager
 * PURPOSE  : Linklayer thread entry function
 * CALLS    : RunAsServer
 *			  RunAsClient
 * CALLED BY: ServiceMain
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 16:02
 *==========================================================================*/
int YDN_LinkLayerManager(YDN_BASIC_ARGS *pThis)
{
	int iCommPortID;
	PORT_INFO *pCommPort = NULL;
	int iMediaType;

	/* to get comm port info through DixGetData interface */
	int iError, iBufLen, iTryCount;

	HANDLE hLocalThread;

	/* get Local thread id */	
	hLocalThread = RunThread_GetId(NULL);
	/* assign the reference in the Thread to insure valid */
	pThis->hThreadID[1] = hLocalThread;
	/* feed watch dog */
	RunThread_Heartbeat(hLocalThread);

	/* get current Comm Port ID */
	if (g_YDNGlobals.CommonConfig.iMediaType == YDN_MEDIA_TYPE_TCPIP)
	{
		/* for net data port, port id is 3 */
		iCommPortID = 2;
	}
	else /* Modem or Leased Line */
	{
		iCommPortID = 3;
	}

	/* get Comm Port config Info by port id */
	for (iTryCount = 0; iTryCount < 3; iTryCount++)
	{
		iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_PORT_INFO,			
			iCommPortID,		
			&iBufLen,			
			&pCommPort,
			0);

		if (iError != ERR_DXI_OK) 
		{
			if (iError == ERR_DXI_TIME_OUT)
			{
				/* try 3 times */
				Sleep(1000);
				continue;
			}
			else
			{
				LOG_YDN_LM_E_EX("Get Comm Port config info failed", iError);

				/* notify YDN service to exit */
				pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
				return SERVICE_EXIT_FAIL;
			}
		}
		else
		{
			break;
		}
	}
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread YDN23 Linker Service was created, Thread Id = %d.\n", gettid());
#endif	
	/* begin main loop */
	iMediaType = g_YDNGlobals.CommonConfig.iMediaType;
	while (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);
		/* run in the Server mode */
		if (pThis->iOperationMode == YDN_MODE_SERVER) 
		{   
		    TRACE("\nEnter the Server Mode!!\n");
			TRACE_YDN_TIPS("Enter the Server Mode");
			RunAsServer(pThis, pCommPort);
			LeaveCommUsing();
			TRACE_YDN_TIPS("Exit the server mode");
		}
		else			/* run in the Client mode */
		{
		    TRACE("\nEnter the Client Mode!!\n");
			TRACE_YDN_TIPS("Enter the Client Mode");
			RunAsClient(pThis, pCommPort);
			LeaveCommUsing();
			TRACE_YDN_TIPS("Exit the Client Mode");
		}
	}

	TRACE_YDN_TIPS("Exit the Link-layer sub-thread");

	/* notify YDN service to exit(only for abnormal case) */
	if (pThis->iLinkLayerThreadExitCmd != SERVICE_EXIT_OK)
	{
		TRACE("\nExit code: %d\n", pThis->iLinkLayerThreadExitCmd);
		pThis->iInnerQuitCmd = pThis->iLinkLayerThreadExitCmd;
	}

	return pThis->iLinkLayerThreadExitCmd;
	
}


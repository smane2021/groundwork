/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : service_provider.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "ydn.h"
#include "addydn.h"
/* define log tasks */
#define LOG_INIT_YDN_SERVICE    "Init YDN service"

#define LOG_MODIFY_YDN_COMMON_CONFIG    "YDN PARAM"


 
/* used to init state machines */
#define DEF_STATE_MACHINE(_p, _iStateID, _fnProc)  \
	      ((_p)->iStateID = (_iStateID), \
		   (_p)->fnStateProc = (_fnProc))



/* define the global variable of YDN service */
YDN_GLOBALS  g_YDNGlobals;

/* define the share-used simple events */
YDN_EVENT g_YDNDummyEvent, g_YDNTimeoutEvent;
YDN_EVENT g_YDNDiscEvent, g_YDNConnFailEvent, g_YDNConnEvent;

YDN_RESPDATA_BUFF g_RespData;

__INLINE static void InitStaticEvents()
{
	YDN_INIT_FRAME_EVENT(&g_YDNDummyEvent, 0, 0, TRUE, FALSE);
	YDN_INIT_NONEFRAME_EVENT(&g_YDNTimeoutEvent, YDN_TIMEOUT_EVENT);
	YDN_INIT_NONEFRAME_EVENT(&g_YDNDiscEvent, YDN_DISCONNECTED_EVENT);
	YDN_INIT_NONEFRAME_EVENT(&g_YDNConnFailEvent, YDN_CONNECT_FAILED_EVENT);
	YDN_INIT_NONEFRAME_EVENT(&g_YDNConnEvent, YDN_CONNECTED_EVENT);
}
/*==========================================================================*
 * FUNCTION : AlarmProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ServiceNotification
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 19:26
 *==========================================================================*/
static int AlarmProc(YDN_BASIC_ARGS *pThis)
{
#ifdef _DEBUG_YDN_REPORT

	TRACE_YDN_TIPS("Alarm captured by YDN module");

	if (g_YDNGlobals.CommonConfig.bReportInUse)
	{
		TRACE("Alarm Report function is on, will report to MC.\n");
	}
#endif //_DEBUG_YDN_REPORT

	YDN_RegisterAlarm(pThis, FALSE);

	return 0;
}


/*==========================================================================*
 * FUNCTION : ServiceNotification
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService        : 
 *            int     nMsgType        : 
 *            int     nTrapDataLength : 
 *            void*   lpTrapData      : 
 *            void*   lpParam         :  
 *            BOOL    bUrgent         : 
 * RETURN   : int : 0 for success, 1 for error(defined in DXI module)
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 17:23
 *==========================================================================*/
int ServiceNotification(HANDLE hService, 
						int nMsgType, 
						int nTrapDataLength, 
						void *lpTrapData,	
						void *lpParam, 
						BOOL bUrgent)
{
	YDN_BASIC_ARGS *pBasicArgs = (YDN_BASIC_ARGS *)lpParam;
	ALARM_SIG_VALUE *pAlarmSig = (ALARM_SIG_VALUE *)lpTrapData;

	UNUSED(hService);
	UNUSED(nTrapDataLength);
	//UNUSED(lpTrapData);
	UNUSED(bUrgent);
	

	//TRACE("pAlarmSig->iAlarmLevel is : %d\n", pAlarmSig->iAlarmLevel);

	//if(g_YDNGlobals.CommonConfig.iMediaType != YDN_MEDIA_TYPE_MODEM)
		//return 1;
	
	//if (nMsgType == _ALARM_OCCUR_MASK || nMsgType == _ALARM_CLEAR_MASK)
	if (nMsgType == _ALARM_OCCUR_MASK && pAlarmSig->iAlarmLevel >= ALARM_LEVEL_MAJOR)
	{
		return AlarmProc(pBasicArgs);
	}
	return 1;
}

/*==========================================================================*
 * FUNCTION : InitYDNGlobals
 * PURPOSE  : assistant function to init g_YDNGlobals
 * CALLS    : 
 * CALLED BY: ServiceMain
 * RETURN   : DWORD : use SERVICE_EXIT_CODE const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 20:08
 *==========================================================================*/
static DWORD InitYDNGlobals(void)
{
	YDN_STATE_INFO  *pStateInfo;

	/* 1.init config */
	if (YDN_InitConfig() != ERR_CFG_OK)
	{
		return SERVICE_EXIT_CFG;
	}

	/* create mutex for YDN Common Config file */
	/*g_YDNGlobals.hMutexCommonCfg = Mutex_Create(TRUE);
	if (g_YDNGlobals.hMutexCommonCfg == NULL)
	{
		AppLogOut(LOG_INIT_YDN_SERVICE, APP_LOG_ERROR, 
			"Create Mutex for YDN Common Config file failed\n");
		TRACE("[%s]--%s: ERROR: Create Mutex for YDN Common Config file"
			" failed.\n", __FILE__, __FUNCTION__);

			return SERVICE_EXIT_FAIL;
	}*/

	/* 2.init command handler */
	YDN_InitCmdHander();
	
	/* 3.init YDN state machine */
	pStateInfo = g_YDNGlobals.YDNStates;

	DEF_STATE_MACHINE(&pStateInfo[0], 
		YDN_IDLE, 
		YDN_OnIdle);

	DEF_STATE_MACHINE(&pStateInfo[1], 
		YDN_WAIT_FOR_CMD, 
		YDN_OnWaitForCMD);

	DEF_STATE_MACHINE(&pStateInfo[2], 
		YDN_SEND_ALARM, 
		YDN_OnSendAlarm);

	/* 4. init some flags*/
	bACDNoRespFlag = 0;
	bDCDNoRespFlag = 0;

	return SERVICE_EXIT_NONE;
}


/*==========================================================================*
 * FUNCTION : StateMachineLoop
 * PURPOSE  : for main loop of state machine
 * CALLS    : 
 * CALLED BY: ServiceMain
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : DWORD : use SERVICE_EXIT_CODE const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 15:01
 *==========================================================================*/
static DWORD StateMachineLoop(YDN_BASIC_ARGS *pThis)
{
	int iLastState;
	int *piState;
	YDN_STATE_INFO *pCurStateMachine;

	piState = &(pThis->iMachineState);
	
	while (*(pThis->pOutQuitCmd) == SERVICE_CONTINUE_RUN && 
		pThis->iInnerQuitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		
		RunThread_Heartbeat(pThis->hThreadID[0]);
		//printf("Begin the state machine loop!\n");


			pCurStateMachine = g_YDNGlobals.YDNStates;
			*piState = YDN_IDLE;

			TRACE_YDN_TIPS("Begin YDN23 state machine");




		while (*piState != YDN_STATE_MACHINE_QUIT)
		{
			/* for TRACE */
			iLastState = *piState;

			/* process one state */
			*piState = pCurStateMachine[*piState].fnStateProc(pThis);

			/* check quit conditions */
			if (*(pThis->pOutQuitCmd) == SERVICE_STOP_RUNNING ||
				pThis->iInnerQuitCmd != SERVICE_EXIT_NONE ||
				pThis->bProtocolChangedFlag)
			{
				TRACE("Exit state machine loop");
				TRACE("OutQuitCmd: %d\tInnerQuitCmd: %d\tProtocol Changed "
					"Flag: %d\n", *(pThis->pOutQuitCmd), pThis->iInnerQuitCmd,
					pThis->bProtocolChangedFlag);

				*piState = YDN_STATE_MACHINE_QUIT;

				/* reset flag */
				if (pThis->bProtocolChangedFlag)
				{
					pThis->bProtocolChangedFlag = FALSE;
					pThis->bFrameDone = TRUE;
				}
			}
			/* check quit conditions */
			if (*(pThis->pOutQuitCmd) == SERVICE_STOP_RUNNING ||
				pThis->iInnerQuitCmd != SERVICE_EXIT_NONE ||
				pThis->bADRChangedFlag)
			{
				TRACE_YDN_TIPS("Exit state machine loop");
				TRACE("OutQuitCmd: %d\tInnerQuitCmd: %d\tProtocol Changed "
					"Flag: %d\n", *(pThis->pOutQuitCmd), pThis->iInnerQuitCmd,
					pThis->bADRChangedFlag);

				*piState = YDN_STATE_MACHINE_QUIT;

				/* reset flag */
				if (pThis->bADRChangedFlag)
				{
					pThis->bADRChangedFlag = FALSE;
					pThis->bFrameDone = TRUE;
				}
			}
		  #ifdef _SHOW_YDN_CUR_STATE
             // YDN_PrintState(*piState);
          #endif //_SHOW_YDN_CUR_STATE
          #ifdef _SHOW_YDN_CUR_STATE
			if (*piState != iLastState)
			{
				TRACE_YDN_TIPS("Change to another State");
				YDN_PrintState(*piState);
			}
          #endif //_SHOW_YDN_CUR_STATE
		}
	}

	if (pThis->iInnerQuitCmd != SERVICE_EXIT_NONE)
	{
		return pThis->iInnerQuitCmd;
	}

	return SERVICE_EXIT_OK;
}


/*==========================================================================*
 * FUNCTION : RoutineBeforeExitService
 * PURPOSE  : to do clean work before service exit
 * CALLS    : YDN_ClearEventQueue
 * CALLED BY: ServiceMain
 * ARGUMENTS: YDN_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 15:53
 *==========================================================================*/
static void RoutineBeforeExitService(YDN_BASIC_ARGS *pThis)
{
	int i;
	/*HANDLE hMutex;*/

	YDNMODEL_CONFIG_INFO *pModelConfig;
	YDN_TYPE_MAP_INFO *pTypeMap;
	YDN_ALARM_INFO *pAlarmBuf;

	/* 1.unregister in DXI module(it is safe even when not registered) */
	DxiRegisterNotification("YDN Service", 
							ServiceNotification,
							pThis, 							  
							_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 							  
							pThis->hThreadID[0],							  
							_CANCEL_FLAG);

	/* 2.kill all the timers */
	for (i = 1; i < YDN_TIMER_NUM; i++)
	{
		Timer_Kill(pThis->hThreadID[0], i);
	}

	/* 4.stop child thread(it is safe even when child thread is not running) */
	if (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;
	}
	/* wait 60s at most*/
	TRACE("to stop iLinkLayerThreadExitCmd!!\n");
	int iTemp = RunThread_Stop(pThis->hThreadID[1], 5000, TRUE);
	pThis->hThreadID[1] = NULL;
	TRACE("iTemp1 is %d\n", iTemp);
	TRACE("stop iLinkLayerThreadExitCmd end!!\n");


    /* 3.stop child thread(it is safe even when child thread is not running) */
	if (pThis->iPackDataThreadExitCmd == SERVICE_EXIT_NONE)
	{
		pThis->iPackDataThreadExitCmd = SERVICE_EXIT_OK;
	}
	/* wait 60s most*/
	TRACE("to stop iPackDataThreadExitCmd!!\n");
	iTemp = RunThread_Stop(pThis->hThreadID[2], 5000, TRUE);
	pThis->hThreadID[2] = NULL;
	TRACE("iTemp2 is %d\n", iTemp);
	TRACE("stop iPackDataThreadExitCmd end!!\n");

		/* 5.destroy event queues */
	YDN_ClearEventQueue(pThis->hEventInputQueue, TRUE);
	YDN_ClearEventQueue(pThis->hEventOutputQueue, TRUE);
		

	/* 6.Free memories */
	/* free memory of YDNModelConfig */
	pModelConfig = &g_YDNGlobals.YDNModelConfig;
	pTypeMap = pModelConfig->pTypeMapInfo;
	for (i = 0; i < pModelConfig->iTypeMapNum; i++, pTypeMap++)
	{
		DELETE(pTypeMap->pMapEntriesInfo);
	}
	DELETE(pModelConfig->pTypeMapInfo);

	/* free memory of YDNModelInfo */
	DELETE(g_YDNGlobals.YDNModelInfo.pYDNBlockInfo);

	/* free memory of YDN_BASIC_ARGS */
	pAlarmBuf = pThis->AlarmBuffer.pCurAlarmInfo;
	if (pAlarmBuf != NULL)
	{
		DELETE(pAlarmBuf);
	}

	pAlarmBuf = pThis->AlarmBuffer.pHisAlarmInfo;
	if (pAlarmBuf != NULL)
	{
		DELETE(pAlarmBuf);
	}

	/* 6.free the Mutex -- Delet for Mutex now is allocated in DXI module */ 
	/*hMutex = g_YDNGlobals.hMutexCommonCfg;
	if (hMutex != NULL) 
	{ 
		Mutex_Destroy(hMutex);
		hMutex = NULL;
	}*/

	return;
}

#define		GET_SERVICE_OF_YDN23_NAME	"ydn23.so"
/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SERVICE_ARGUMENTS *  pArgs : 
 * RETURN   : DWORD : service exit code, defined in app_service.h
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-21 19:30
 *==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{
	DWORD dwExitCode;
	int iRet;
	const char *pLogText;
	APP_SERVICE		*YDN23_AppService = NULL;
	YDN23_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN23_NAME);

	YDN_BASIC_ARGS l_YDNBasicArgs;
	HANDLE hInputQueue, hOutputQueue;
	HANDLE hServiceThread, hLinklayerThread, hPackDataThread;

	/* clear to zero */
	memset(&g_YDNGlobals, 0, sizeof(YDN_GLOBALS));
	memset(&l_YDNBasicArgs, 0, sizeof(YDN_BASIC_ARGS));

	/* get the thread handle, used to feed watch dog */
	hServiceThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hServiceThread);

	/* 1.initialize g_YDNGlobals */
//#ifdef _DEBUG_YDN_SERVICE
	TRACE("enter YDN23 ServiceMain\n");
//#endif
	
	dwExitCode = InitYDNGlobals();

	if (dwExitCode != SERVICE_EXIT_NONE)
	{
		RoutineBeforeExitService(&l_YDNBasicArgs);
		return dwExitCode;
	}

	/* init the static events */
    InitStaticEvents();

	/* feed watch dog */
	RunThread_Heartbeat(hServiceThread);

	/* 2.init YDN_BASIC_ARGS */
	/* event queues */
	hInputQueue = Queue_Create(YDN_EVENT_QUEUE_SIZE,
		sizeof(YDN_EVENT *), 0);
	hOutputQueue = Queue_Create(YDN_EVENT_QUEUE_SIZE,
		sizeof(YDN_EVENT *), 0);

	if (hInputQueue == NULL || hOutputQueue == NULL)
	{
		AppLogOut(LOG_INIT_YDN_SERVICE, APP_LOG_ERROR, "Create Event Queue "
			"failed.\n");
		TRACE("[%s]--%s: ERROR: Create Event Queue failed.\n", __FILE__,
			__FUNCTION__);

		RoutineBeforeExitService(&l_YDNBasicArgs);
		return SERVICE_EXIT_FAIL;
	}

	l_YDNBasicArgs.hEventInputQueue = hInputQueue;
	l_YDNBasicArgs.hEventOutputQueue = hOutputQueue;


	/* AlarmBuffer: iCurAlarmNum and iHisAlarmNum cleared to zero already */
	/* alarmHandler: all members cleared to zero already */

	/* get YDN Service thread id */
	l_YDNBasicArgs.hThreadID[0] = hServiceThread;

	/* flags */
	l_YDNBasicArgs.iOperationMode = YDN_MODE_SERVER;
	l_YDNBasicArgs.pOutQuitCmd = &pArgs->nQuitCommand;
	l_YDNBasicArgs.iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;
	l_YDNBasicArgs.iPackDataThreadExitCmd = SERVICE_EXIT_NONE;
	l_YDNBasicArgs.iInnerQuitCmd = SERVICE_EXIT_NONE;
	l_YDNBasicArgs.bFrameDone = TRUE;

	/* other flags cleared to zero already */
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread YDN23 Service was created, Thread Id = %d.\n", gettid());
#endif	

	/* register the YDN basic agrs */
    pArgs->pReserved = &l_YDNBasicArgs;

	/* 3.register in the DXI module to receive Alarms */
	iRet = DxiRegisterNotification("YDN Service", 
							ServiceNotification,
							&l_YDNBasicArgs, 							  
							_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 							  
							l_YDNBasicArgs.hThreadID[0],							  
							_REGISTER_FLAG);

	if (iRet != 0)
	{
		pLogText = "Register alarm callback function in DXI module failed.";

		AppLogOut(LOG_INIT_YDN_SERVICE, APP_LOG_ERROR, "%s.\n", pLogText);
		TRACE("[%s]--%s: ERROR: %s Call DxiRegisterNotification, returned"
			" %d.\n", __FILE__, __FUNCTION__, pLogText, iRet);

		RoutineBeforeExitService(&l_YDNBasicArgs);
		return SERVICE_EXIT_FAIL;
	}

    // 4.create PackData thread 

	hPackDataThread = RunThread_Create("YDN_PACK",
		(RUN_THREAD_START_PROC)YDN_PackData,
		&l_YDNBasicArgs,
		NULL,
		0);

	l_YDNBasicArgs.hThreadID[2] = hPackDataThread;

	if (hPackDataThread == NULL)
	{
		pLogText = "Create PackData thread failed.";

		AppLogOut(LOG_INIT_YDN_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
		TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);

		RoutineBeforeExitService(&l_YDNBasicArgs);
		return SERVICE_EXIT_FAIL;
	}
	else
	{
		TRACE("Created PackData thread succesfully!!");
	}

	Sleep(50);

	/* 5.create LinkLayer thread */
	if(g_YDNGlobals.CommonConfig.iProtocolType != YDN)
	{
		l_YDNBasicArgs.hThreadID[1] = NULL;
	}
	else
	{
		hLinklayerThread = RunThread_Create("YDN_LINK",
			(RUN_THREAD_START_PROC)YDN_LinkLayerManager,
			&l_YDNBasicArgs,
			NULL,
			0);

		/* assign the reference out of the Thread to insure valid */
		l_YDNBasicArgs.hThreadID[1] = hLinklayerThread;

		if (hLinklayerThread == NULL)
		{
			pLogText = "Create link-layer thread failed.";

			AppLogOut(LOG_INIT_YDN_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
			TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);

			RoutineBeforeExitService(&l_YDNBasicArgs);
			return SERVICE_EXIT_FAIL;
		}
	}
#ifdef _TEST_YDN_MODIFYCONFIG
	{

		int iRet, nVarID;
		BOOL bServiceIsRunning = TRUE;
		YDN_COMMON_CONFIG UserCfg;

		//Sleep(5000);
		UserCfg.iProtocolType = 1;
		UserCfg.byADR = 66;
		UserCfg.bReportInUse = TRUE;
		UserCfg.iAttemptElapse = 199;
		strcpy(UserCfg.szAlarmReportPhoneNumber[0], "1111111");
		UserCfg.iMediaType = 2;
		strcpy(UserCfg.szCommPortParam, "100.200.1.1:12001");

		nVarID = YDN_CFG_W_MODE | YDN_CFG_ATTEMPT_ELAPSE;
		iRet = YDN_ModifyCommonCfg(bServiceIsRunning,
						FALSE,
						&l_YDNBasicArgs,
						nVarID,
						&UserCfg);

		if (iRet != ERR_SERVICE_CFG_OK)
		{
			TRACE("Change Config file failed\n");
			TRACE("Return Value is: %d\n", iRet);
		}

		YDN_PrintCommonCfg(&UserCfg);
		YDN_PrintCommonCfg(&g_YDNGlobals.CommonConfig);
	}
#endif //_TEST_YDN_MODIFYCONFIG
	if(YDN23_AppService != NULL)
	{
		YDN23_AppService->bReadyforNextService = TRUE;
	}
	/* 5.begin main state machine loop */
	TRACE_YDN_TIPS("Begin state machine");
	dwExitCode = StateMachineLoop(&l_YDNBasicArgs);


	/* 6.do the clean work */
	RoutineBeforeExitService(&l_YDNBasicArgs);

	return dwExitCode;
}


/*==========================================================================*
 * FUNCTION : CheckUserConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID    : 
 *            YDN_COMMON_CONFIG  *pUserCfg : 
 *            YDN_COMMON_CONFIG  *pOriCfg  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 16:39
 *==========================================================================*/
static BOOL CheckUserConfig(int nVarID, YDN_COMMON_CONFIG *pUserCfg,
							YDN_COMMON_CONFIG *pOriCfg)
{
	/* prepare for check accordance between config items */
	if (!(nVarID & YDN_CFG_PROTOCOL_TYPE))
	{
		pUserCfg->iProtocolType = pOriCfg->iProtocolType;
	}

	if (!(nVarID & YDN_CFG_ADR))
	{
		pUserCfg->byADR = pOriCfg->byADR;
	}

	if (!(nVarID & YDN_CFG_MEDIA_TYPE))
	{
		pUserCfg->iMediaType = pOriCfg->iMediaType;
	}

	if (!(nVarID & YDN_CFG_MEDIA_PORT_PARAM))
	{
		strcpy(pUserCfg->szCommPortParam, pOriCfg->szCommPortParam);
	}

	
	/* 1.check protocol type and the accordance */
	//if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_PROTOCOL_TYPE ||
		//nVarID & YDN_CFG_ADR)
	if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_PROTOCOL_TYPE)
	{
		if (pUserCfg->iProtocolType < EEM_YDN || 
			pUserCfg->iProtocolType >= YDN_PROTOCOL_NUM)
		{
			return FALSE;
		}
	}

   //TRACE("\npUserCfg4->byADR is: %d\n", pUserCfg->byADR);
  // TRACE("\nnVarID is: %d\n", nVarID);

	/* 2.check ADR -- always true due to data type */
	if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_ADR)
	{
		if (pUserCfg->byADR < 1 || pUserCfg->byADR > 255)
		{
			return FALSE;
		}

	}

	/* 4.chech operation media and comm port param */
	if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_MEDIA_TYPE || 
		  nVarID & YDN_CFG_MEDIA_PORT_PARAM)
	{
		if (pUserCfg->iMediaType >= YDN_MEDIA_TYPE_NUM)
		{
			return FALSE;
		}

		switch (pUserCfg->iMediaType)
		{
		case YDN_MEDIA_TYPE_LEASED_LINE:
			if ((!YDN_CheckBautrateCfg(pUserCfg->szCommPortParam))||(!YDN_CheckSPortParam(pUserCfg->szCommPortParam)))
			{
				return FALSE;
			}
			break;

		case YDN_MEDIA_TYPE_MODEM:
			//if (!YDN_CheckModemCfg(pUserCfg->szCommPortParam))
		    if ((!YDN_CheckBautrateCfg(pUserCfg->szCommPortParam))||(!YDN_CheckSPortParam(pUserCfg->szCommPortParam)))
			{
				return FALSE;
			}
			break;

		case YDN_MEDIA_TYPE_TCPIP:
			if (!YDN_CheckIPAddress(pUserCfg->szCommPortParam))
			{
				return FALSE;
			}
		}
	}

	/* 5.check Max alarm report attempts */
	if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_MAX_ATTEMPTS)
	{
		if (pUserCfg->iMaxAttempts < 0 || pUserCfg->iMaxAttempts > 5)
		{
			return FALSE;
		}
	}

	/* 6.check Call elapse time */
	if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_ATTEMPT_ELAPSE)
	{
		if (pUserCfg->iAttemptElapse < 0 || pUserCfg->iAttemptElapse > 1000 * 300)
		{
			return FALSE;
		}
	}

	/* 7.check report phone number 1 */
	if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_REPORT_NUMBER_1)
	{
		if (!YDN_CheckPhoneNumber(pUserCfg->szAlarmReportPhoneNumber[0]))
		{
			return FALSE;
		}
	}

	/* 8.check report phone number 2 */
	if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_REPORT_NUMBER_2)
	{
		if (!YDN_CheckPhoneNumber(pUserCfg->szAlarmReportPhoneNumber[1]))
		{
			return FALSE;
		}
	}

	/* 8.check report phone number 3 */
	if (nVarID & YDN_CFG_ALL || nVarID & YDN_CFG_REPORT_NUMBER_3)
	{
		if (!YDN_CheckPhoneNumber(pUserCfg->szAlarmReportPhoneNumber[2]))
		{
			return FALSE;
		}
	}

	return TRUE;
}


/* for copy YDN_COMMON_CONFIG structure, used as inline function */
#define YDN_COPY_COMMON_CONFIG(_src, _dst) \
   { \
   int i;  \
   (_dst).byADR = (_src).byADR;  \
   (_dst).iProtocolType = (_src).iProtocolType;  \
   (_dst).iMediaType = (_src).iMediaType;  \
   strcpy((_dst).szCommPortParam, (_src).szCommPortParam);  \
   (_dst).bReportInUse = (_src).bReportInUse;  \
   (_dst).iMaxAttempts = (_src).iMaxAttempts;  \
   (_dst).iAttemptElapse = (_src).iAttemptElapse;  \
   for (i = 0; i < YDN_ALARM_REPORT_NUM; i++)  \
   {  \
	 strcpy((_dst).szAlarmReportPhoneNumber[i], (_src).szAlarmReportPhoneNumber[i]);  \
   } \
   }

/*==========================================================================*
 * FUNCTION : UpdateNormalCfgItems
 * PURPOSE  : assistant function for ServiceConfig
 * CALLS    : 
 * CALLED BY: ServiceConfig
 * ARGUMENTS: int            nVarID    : 
 *            YDN_COMMON_CONFIG  *pOriCfg  : 
 *            YDN_COMMON_CONFIG  *pUserCfg : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 16:27
 *==========================================================================*/
static void UpdateNormalCfgItems(int nVarID,
								 YDN_COMMON_CONFIG *pOriCfg, 
								 YDN_COMMON_CONFIG *pUserCfg)
{
	if (nVarID & YDN_CFG_ALL)
	{
		YDN_COPY_COMMON_CONFIG(*pUserCfg, *pOriCfg);
		return;
	}

	if (nVarID & YDN_CFG_ADR)
	{
		pOriCfg->byADR = pUserCfg->byADR;
	}

	if (nVarID & YDN_CFG_REPORT_IN_USE)
	{
		pOriCfg->bReportInUse = pUserCfg->bReportInUse;
	}

	if (nVarID & YDN_CFG_MAX_ATTEMPTS)
	{
		pOriCfg->iMaxAttempts = pUserCfg->iMaxAttempts;
	}

	if (nVarID & YDN_CFG_ATTEMPT_ELAPSE)
	{
		pOriCfg->iAttemptElapse = pUserCfg->iAttemptElapse;
	}

	if (nVarID & YDN_CFG_REPORT_NUMBER_1)
	{
		strcpy(pOriCfg->szAlarmReportPhoneNumber[0],
			pUserCfg->szAlarmReportPhoneNumber[0]);
	}

	if (nVarID & YDN_CFG_REPORT_NUMBER_2)
	{
		strcpy(pOriCfg->szAlarmReportPhoneNumber[1],
			pUserCfg->szAlarmReportPhoneNumber[1]);
	}

	if (nVarID & YDN_CFG_REPORT_NUMBER_3)
	{
		strcpy(pOriCfg->szAlarmReportPhoneNumber[2],
			pUserCfg->szAlarmReportPhoneNumber[2]);
	}

	return;
}


/*==========================================================================*
 * FUNCTION : WriteYDNCfgToFlash
 * PURPOSE  : 
 * CALLS    : YDN_GetModifiedFileBuf
 * CALLED BY: 
 * ARGUMENTS: int            nVarID : 
 *            YDN_COMMON_CONFIG  *pCfg  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 17:23
 *==========================================================================*/
/* used interface defined in config_builder.c */
BOOL YDN_GetModifiedFileBuf(int nVarID, 
							YDN_COMMON_CONFIG *pUserCfg,
							char **pszOutFile);

BOOL YDN_UpdateCommonConfigFile(const char *szContent);

static BOOL WriteYDNCfgToFlash(int nVarID, YDN_COMMON_CONFIG *pUserCfg)
{
	HANDLE hMutex;
	char *szOutFile;
	char *pLogText;

	/* to simplify log action */
#define TASK_NAME        "Modify YDN Config File"
#define LOG_MODIFY_FILE(_pLogText)  \
	(AppLogOut(TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	hMutex = DXI_GetESRCommonCfgMutex();

	if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_COMCFG) == ERR_MUTEX_OK)
	{
		/* create new file in memory first */
		if (!YDN_GetModifiedFileBuf(nVarID, pUserCfg, &szOutFile))
		{
			pLogText = "Failed to create new YDN common config file in memory.";
			LOG_MODIFY_FILE(pLogText);

			DELETE(szOutFile);
			Mutex_Unlock(hMutex);
			return FALSE;
		}

		/* write to flash */
		if (!YDN_UpdateCommonConfigFile(szOutFile))
		{
			pLogText = "Write the new YDN common config file to flash failed.";
			LOG_MODIFY_FILE(pLogText);

			DELETE(szOutFile);
			Mutex_Unlock(hMutex);
			return FALSE;
		}
	}
	else
	{
		pLogText = "Wait for Mutex of Common Config file timeout.";
		LOG_MODIFY_FILE(pLogText);

		return FALSE;
	}


	DELETE(szOutFile);
	Mutex_Unlock(hMutex);
	return TRUE;
}

__INLINE static BOOL YDN_IsCommBusy(YDN_BASIC_ARGS *pThis)
{
	BOOL bRet = pThis->bCommRunning;

	/* for leased line, when stay in YDN_IDLE or SOC_OFF as Server Mode, we 
	 * think communication is not running. 
	 */
	/*TRACE("g_YDNGlobals.CommonConfig.iMediaType is %d\n", g_YDNGlobals.CommonConfig.iMediaType);
	TRACE("pThis->iMachineState is %d\n", pThis->iMachineState);
	TRACE("pThis->iOperationMode is %d\n", pThis->iOperationMode);*/
	if (g_YDNGlobals.CommonConfig.iMediaType == YDN_MEDIA_TYPE_LEASED_LINE)
		//|| g_YDNGlobals.CommonConfig.iMediaType == YDN_MEDIA_TYPE_MODEM)
	{
		if (!pThis->iMachineState && pThis->iOperationMode == YDN_MODE_SERVER)
		{
			bRet = FALSE;
		}
	}
	else if(g_YDNGlobals.CommonConfig.iMediaType == YDN_MEDIA_TYPE_MODEM)
	{
		if(pThis->iOperationMode == YDN_MODE_CLIENT)
			bRet = TRUE;
	}
    TRACE("pThis->bCommRunning is %d\n", pThis->bCommRunning);
	return bRet;
}


/*==========================================================================*
 * FUNCTION : YDN_ModifyCommonCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ServiceConfig
 * ARGUMENTS: BOOL bServiceIsRunning    :
 *			  BOOL bByForce				: ignore CommBusy restriction
 *			  YDN_BASIC_ARGS  *pThis    : 
 *            int             iItemID   : 
 *            YDN_COMMON_CONFIG   *pUserCfg : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-10 15:53
 *==========================================================================*/
int YDN_ModifyCommonCfg(BOOL bServiceIsRunning,
						BOOL bByForce,
						YDN_BASIC_ARGS *pThis,
						int iItemID,
						YDN_COMMON_CONFIG *pUserCfg)
{
	YDN_COMMON_CONFIG *pOriCfg;
	HANDLE hLinklayerThread;

	unsigned int	    byADR;                
	unsigned int		iProtocolType;	/* see enum YDN_PROTOCOL definition */
	unsigned int		iMediaType;	    /* see enum YDN_MEDIA_TYPE definition */
	char				szCommPortParam[COMM_PORT_PARAM_LEN];

	BOOL				bReportInUse;		
	
	/* max attemps to report alarms */
	int					iMaxAttempts;	
	/* elaps time between each attemps (unit: second) */
	int					iAttemptElapse;	

	/* phone number for alarm report*/
	char				szAlarmReportPhoneNumber[YDN_ALARM_REPORT_NUM][PHONENUMBER_LEN];

	char *pLogText;

	/* original common config */
	pOriCfg = &g_YDNGlobals.CommonConfig;

    byADR = pOriCfg->byADR;
    iProtocolType = pOriCfg->iProtocolType;
    iMediaType = pOriCfg->iMediaType;
    memcpy(szCommPortParam, pOriCfg->szCommPortParam, COMM_PORT_PARAM_LEN);
    TRACE("pOriCfg->iMediaType = %d, szCommPortParam = %s\n",  pOriCfg->iMediaType, szCommPortParam);
    TRACE("-------iItemID = %d-----------\n", iItemID);
    bReportInUse = pOriCfg->bReportInUse;
    iMaxAttempts = pOriCfg->iMaxAttempts;
    iAttemptElapse = pOriCfg->iAttemptElapse;
    memcpy(szAlarmReportPhoneNumber[0], pOriCfg->szAlarmReportPhoneNumber[0], PHONENUMBER_LEN);
    memcpy(szAlarmReportPhoneNumber[1], pOriCfg->szAlarmReportPhoneNumber[1], PHONENUMBER_LEN);
    memcpy(szAlarmReportPhoneNumber[2], pOriCfg->szAlarmReportPhoneNumber[2], PHONENUMBER_LEN);

	
	/* read config */
	if (!(iItemID & YDN_CFG_W_MODE))  
	{
		YDN_COPY_COMMON_CONFIG(*pOriCfg, *pUserCfg);
		return ERR_SERVICE_CFG_OK;
	}

	//TRACE("pUserCfg->iAttemptElapse is %d\n", pUserCfg->iAttemptElapse);
	/* update config */
	if (!bByForce && YDN_IsCommBusy(pThis))
	{
		return ERR_SERVICE_CFG_BUSY;
	}
  
	/* check data first */
	if (!CheckUserConfig(iItemID, pUserCfg, pOriCfg))
	{
		return ERR_SERVICE_CFG_DATA;
	}
	
	/* check if the non-shared com has been occupied already */
#ifdef COM_SHARE_SERVICE_SWITCH
	{
		HANDLE hMutex;
		SERVICE_SWITCH_FLAG *pFlag;

		/* note: MediaType value of pUserCfg has been updated 
		in CheckUserConfig function */
		if (iItemID & YDN_CFG_MEDIA_TYPE || iItemID & YDN_CFG_ALL ||
			iItemID & YDN_CFG_MEDIA_PORT_PARAM)
		{
			pFlag = DXI_GetServiceSwitchFlag();

			if (YDN_IS_SHARE_COM(pUserCfg->iMediaType))
			{
				hMutex = DXI_GetServiceSwitchMutex();
				if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_SWITCH) == ERR_MUTEX_OK)
				{
					if (pFlag->bMtnCOMHasClient)
					{
						Mutex_Unlock(hMutex);
						return ERR_SERVICE_CFG_YDN_COMUSED;
					}
					else
					{
						pFlag->bHLMSUseCOM = TRUE;
						Mutex_Unlock(hMutex);
					}
				}
				else
				{
					return ERR_SERVICE_CFG_YDN_COMUSED;
				}
			}
			else
			{
				pFlag->bHLMSUseCOM = FALSE;
			}
		}

	}
#endif //COM_SHARE_SERVICE_SWITCH
	/* write to flash then */
	/*
	TRACE("pUserCfg->byADR is %d\n", pUserCfg->byADR);
	TRACE("pUserCfg->iProtocolType is %d\n", pUserCfg->iProtocolType);
	TRACE("pUserCfg->iMediaType is %d\n", pUserCfg->iMediaType);
	TRACE("pUserCfg->szCommPortParam is %s\n", pUserCfg->szCommPortParam);
   */
	if (!WriteYDNCfgToFlash(iItemID, pUserCfg))
	{
		return ERR_SERVICE_CFG_FLASH;
	}
	/* now process in the memory */
	if (bServiceIsRunning)
	{
        /* Address updated */
		if ((iItemID & YDN_CFG_ADR) ||
			(iItemID & YDN_CFG_ALL))
		{
			/* changed */
			if (pOriCfg->byADR != pUserCfg->byADR)
			{
				pOriCfg->byADR = pUserCfg->byADR;
				pThis->bADRChangedFlag = TRUE;
			}
			
		}

		/* protocol type updated */
		if ((iItemID & YDN_CFG_PROTOCOL_TYPE) ||
			(iItemID & YDN_CFG_ALL))
		{
			/* changed */
			if (pOriCfg->iProtocolType != pUserCfg->iProtocolType)
			{
				pOriCfg->iProtocolType = pUserCfg->iProtocolType;
				pThis->bProtocolChangedFlag = TRUE;
			}
		}

		/* link-layer media updated */
		//if ((iItemID & YDN_CFG_MEDIA_TYPE) || (iItemID & YDN_CFG_ALL) ||
		//	(iItemID & YDN_CFG_MEDIA_PORT_PARAM))
			
		{
			/* changed */
			//if (pOriCfg->iMediaType != pUserCfg->iMediaType || 
			//	strcmp(pOriCfg->szCommPortParam, 
			//	pUserCfg->szCommPortParam) != 0)
			//TRACE("----------------------------Into create link-layer---------------------\n");
			//{		
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;

				//Sleep(3000);
				/* wait 20s */
				if(pThis->hThreadID[1] != NULL)
				{
					RunThread_Stop(pThis->hThreadID[1], 20000, TRUE);
				}
				/* update the value */
				if (iItemID & YDN_CFG_MEDIA_TYPE)
				{
					pOriCfg->iMediaType = pUserCfg->iMediaType;
				}
				if (iItemID & YDN_CFG_MEDIA_PORT_PARAM)
				{
					strcpy(pOriCfg->szCommPortParam, pUserCfg->szCommPortParam);
				}
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;
				/* restart link-layer thread */
				//if(pOriCfg->iProtocolType == YDN)
				//{
					hLinklayerThread = RunThread_Create("YDN_LINK",
						(RUN_THREAD_START_PROC)YDN_LinkLayerManager,
						pThis,
						NULL,
						0);
					if (hLinklayerThread == NULL)
					{
						pLogText = "Create link-layer thread failed.";
	
						AppLogOut(LOG_INIT_YDN_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
						TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);
	
						pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
						return ERR_SERVICE_CFG_EXIT;
					}
				

					pThis->hThreadID[1] = hLinklayerThread;
				//}
				//else				
				//{

				//	if(pThis->hThreadID[1])
				//	{
				//		RunThread_Stop(pThis->hThreadID[1], 
				//						10000, TRUE);
				//		
				//	}
				//	pThis->hThreadID[1] = NULL;
				//	
				//}
			//}/* end of changed */
		}/* end of link-layer media updated */
		/* for other config item, just modify values */
		UpdateNormalCfgItems(iItemID, pOriCfg, pUserCfg);
	}

    char logText[YDN_LOG_TEXT_LEN];

    TRACE("iItemID is %d [%d]\n", iItemID, bServiceIsRunning);

    if(iItemID & YDN_CFG_ADR)
    {
        sprintf(logText, "Modify YDN23 Address from %d to %d%s%s", byADR, pUserCfg->byADR, " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_PROTOCOL_TYPE)
    {
    /*
        char *pProtocolType;
	    switch(pUserCfg->iProtocolType)
	    {
		    case YDN:
			    pProtocolType = "YDN23";
			    break;

			case EEM:
			    pProtocolType = "EEM";
			    break;
			case RSOC:
			    pProtocolType = "RSOC";
			    break;
			case SOC_TPE:
			    pProtocolType = "SOC_TPE";
			    break;

			default:
				pProtocolType = "YDN23";
			    break;
	    }
	    */
	    int iProtocolNum,iUserProtocolNum;
	    if((iProtocolType>0)&&(iProtocolType<6))
	    {
		iProtocolNum =iProtocolType-1;
	    }
	    else
	    {
		iProtocolNum = YDN-1;
	    }
	    if((pUserCfg->iProtocolType>0)&&(pUserCfg->iProtocolType<6))
	    {
		    iUserProtocolNum =pUserCfg->iProtocolType-1;
	    }
	    else
	    {
		    iUserProtocolNum = YDN-1;
	    }

	    char protocolType[5][20] = { {"EEM"}, {"RSOC"}, {"SOC_TPE"}, {"YDN23"}, {"MODBUS"}};
	    
        sprintf(logText, "Modify Protocol Type from %s to %s%s%s", protocolType[iProtocolNum], 
        	protocolType[iUserProtocolNum], " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_MEDIA_TYPE)
    {
    /*
        char *pMediaType;
	    switch(pUserCfg->iMediaType)
	    {
		    case YDN_MEDIA_TYPE_LEASED_LINE:
			    pMediaType = "LEASED_LINE";
			    break;

			case YDN_MEDIA_TYPE_MODEM:
			    pMediaType = "MODEM";
			    break;
			    
			case YDN_MEDIA_TYPE_TCPIP:
			    pMediaType = "Ethernet";
			    break;

			default:
				pMediaType = "LEASED_LINE";
			    break;
	    }
	    */
	    char mediaType[3][20] = {{"LEASED_LINE"}, {"MODEM"}, {"Ethernet"}};
	    
        sprintf(logText, "Modify Media Type from %s to %s%s%s", mediaType[iMediaType],
        	mediaType[pUserCfg->iMediaType], " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_MEDIA_PORT_PARAM)
    {
        sprintf(logText, "Modify Media Port Parameter from %s to %s%s%s", szCommPortParam, 
        	pUserCfg->szCommPortParam, " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_REPORT_IN_USE)
    {
    /*
        char *pReportInUse;
        if(pUserCfg->bReportInUse)
        	pReportInUse = "Enabled";
        else
        	pReportInUse = "Disabled";
        	*/

        char reportInUse[2][20] = {{"Disabled"}, {"Enabled"}};
	    
        sprintf(logText, "Modify Alarm Report Enabled from %s to %s%s%s", reportInUse[bReportInUse], 
        	reportInUse[pUserCfg->bReportInUse], " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_MAX_ATTEMPTS)
    {
    	sprintf(logText, "Modify Times of Dialing Attempt from %d to %d%s%s", iMaxAttempts, 
    		pUserCfg->iMaxAttempts, " by ", pUserCfg->cModifyUser);
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_ATTEMPT_ELAPSE)
    {
    	sprintf(logText, "Modify Interval between Two Dialings from %dS to %dS%s%s", iAttemptElapse/1000,
    		pUserCfg->iAttemptElapse/1000, " by ", pUserCfg->cModifyUser);
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_REPORT_NUMBER_1)
    {
    	sprintf(logText, "Modify The First Report Phone Number from %s to %s%s%s", szAlarmReportPhoneNumber[0], 
    		pUserCfg->szAlarmReportPhoneNumber[0], " by ", pUserCfg->cModifyUser);
    	
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_REPORT_NUMBER_2)
    {
        sprintf(logText, "Modify The Second Report Phone Number from %s to %s%s%s", szAlarmReportPhoneNumber[1], 
    		pUserCfg->szAlarmReportPhoneNumber[1], " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }
    if(iItemID & YDN_CFG_REPORT_NUMBER_3)
    {
        sprintf(logText, "Modify The Third Report Phone Number from %s to %s%s%s", szAlarmReportPhoneNumber[2], 
    		pUserCfg->szAlarmReportPhoneNumber[2], " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_YDN_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', YDN_LOG_TEXT_LEN);
    }


	return ERR_SERVICE_CFG_OK;

}


/*==========================================================================*
 * FUNCTION : ServiceConfig
 * PURPOSE  : for YDN common config info access
 * CALLS    : YDN_ModifyCommonCfg
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService : 
 *            SERVICE_ARGUMENTS * pArgs:
 *            int     nVarID   : 
 *            int    *nBufLen  : 
 *            void   *pDataBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 16:37
 *==========================================================================*/
int ServiceConfig(HANDLE hService, 
				  BOOL bServiceIsRunning,
				  SERVICE_ARGUMENTS *pArgs, 
				  int nVarID,
				  int *nBufLen, 
				  void *pDataBuf)
{
	UNUSED(hService);
	UNUSED(nBufLen);

	int iRtn = 0;
	iRtn = YDN_ModifyCommonCfg(bServiceIsRunning,
		   FALSE,
		   (YDN_BASIC_ARGS *)pArgs->pReserved,
		   nVarID, (YDN_COMMON_CONFIG *)pDataBuf);
	
	return iRtn;
}

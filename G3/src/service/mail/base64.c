/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : RDU(Rack Data Unit)
 *
 *  FILENAME : base64.c
 *  CREATOR  : ����ǿ                 DATE: 2008-08-18
 *  VERSION  : V1.00
 *  PURPOSE  : base64 encode & decode
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
 
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <locale.h>

#include "public.h"


/*
** encode ת���� based onRFC1113
*/
static const char cb64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/*
** decode ת����
*/
static const char cd64[]="|$$$}rstuvwxyz{$$$$$$$>?@ABCDEFGHIJKLMNOPQRSTUVW$$$$$$XYZ[\\]^_`abcdefghijklmnopq";

/*
** encodeblock
**
** encode 3 8-bit binary bytes as 4 '6-bit' characters
*/
void encodeblock( unsigned char szIn[3], unsigned char szOut[4], int nLen )
{
    szOut[0] = cb64[ szIn[0] >> 2 ];
    szOut[1] = cb64[ ((szIn[0] & 0x03) << 4) | ((szIn[1] & 0xf0) >> 4) ];
    szOut[2] = (unsigned char) (nLen > 1 ? cb64[ ((szIn[1] & 0x0f) << 2) | ((szIn[2] & 0xc0) >> 6) ] : '=');
    szOut[3] = (unsigned char) (nLen > 2 ? cb64[ szIn[2] & 0x3f ] : '=');
}


/*==========================================================================*
 * FUNCTION : encode
 * PURPOSE  :  base64 encode
 * CALLS    :
 * CALLED BY:
 * ARGUMENTS:  in OrgStr
 *             out DesStr
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : ����ǿ         DATE: 2008-08-18
 *==========================================================================*/
int encode( const char *OrgStr, char *DesStr )
{
    unsigned char szIn [3];
    unsigned char szOut[4];

    //int i = 0;
    int j = 0;
    int nLen = 0;
    int nUnitCount = 0;
    int nOrgLen = 0 ;
    
    
    if(0 == OrgStr || 0 == DesStr)
    {
        return FALSE;
    }
     
    nOrgLen = strlen(OrgStr);


    for(j=0; j<nOrgLen; j+=3)
    {

        nLen = 3;
        memset(szIn,0,3);
        memset(szOut,0,4);
        
        if( (j+3) <nOrgLen)
        {
            memcpy(szIn,OrgStr+(nUnitCount*3),3);
        }
        else
        {
            //nLen = (j+3+1)-nOrgLen;
	    nLen = nOrgLen-j;
            memcpy(szIn,OrgStr+(nUnitCount*3),(size_t)nLen);
        }
        
        encodeblock( szIn, szOut, nLen );
        memcpy(DesStr+nUnitCount*4,szOut,4);
        
        nUnitCount ++;

    }
    
    return TRUE;
}

/*
** decodeblock
**
** decode 3 '6-bit' characters into 4 8-bit binary bytes
*/
void decodeblock( unsigned char szIn[4], unsigned char szOut[3] )
{   
    szOut[ 0 ] = (unsigned char ) (szIn[0] << 2 | szIn[1] >> 4);
    szOut[ 1 ] = (unsigned char ) (szIn[1] << 4 | szIn[2] >> 2);
    szOut[ 2 ] = (unsigned char ) (((szIn[2] << 6) & 0xc0) | szIn[3]);
}

/*==========================================================================*
 * FUNCTION : decode
 * PURPOSE  : base64 decode
 * CALLS    :
 * CALLED BY:
 * ARGUMENTS:  in OrgStr
 *             out DesStr
 * RETURN   : 
 * COMMENTS : 
 *            
 * CREATOR  : ����ǿ         DATE: 2008-08-18
 *==========================================================================*/
int decode( const char *OrgStr, char * DesStr )
{
    unsigned char szIn[4], szOut[3], cCharacter;
    int i = 0;
    int j = 0;
    
    int nLen=0;
    int nOrgLen = 0;
    int nCount =0;
    
    if(0 == OrgStr || 0 == DesStr)
    {
        return FALSE;
    }
    
    nOrgLen = strlen(OrgStr);
    
    //��Ч��base64�ַ�
    if( 0 != ( nOrgLen%4 ) )
    { 
        //AppLogOut(MAIL_SERVICE, APP_LOG_ERROR, "decode error!\n"); 
        return FALSE;
    }

    for(i = 0; i< nOrgLen; i++) 
    {
        
       for(nLen=0, j = 0; j < 4 ; j++ ) 
        {
           
            cCharacter = (unsigned char) OrgStr[i+j];
            cCharacter = (unsigned char) ((cCharacter < 43 || cCharacter > 122) ? 0 : cd64[ cCharacter - 43 ]);
        
            if( cCharacter ) 
            {
                cCharacter = (unsigned char) ((cCharacter == '$') ? 0 : cCharacter - 61);
            }
         
            if(0 == cCharacter)
            {
                continue;
            }
            nLen ++;
            szIn[j] = (unsigned char) (cCharacter - 1);
        
        }
        
        decodeblock( szIn, szOut );
        memcpy(DesStr + (nCount*3),szOut,(size_t)nLen-1);
        
        nCount++; 
        i+=(j-1);
    }
    
    return TRUE;
}

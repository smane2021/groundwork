/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : RDU(Rack Data Unit)
 *
 *  FILENAME : base64.h
 *  CREATOR  : ����ǿ
 *       DATE: 2008-08-17 20:05
 *  VERSION  : V1.00
 *  PURPOSE  : ����base64����ͽ���
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef  ___BASE64_H___
#define ___BASE64_H___

/*==========================================================================*
 * FUNCTION : decode
 * PURPOSE  : base64 decode
 * CALLS    :
 * CALLED BY:
 * ARGUMENTS:  in OrgStr
 *             out DesStr
 * RETURN   : 
 * COMMENTS : 
 *            
 * CREATOR  : ����ǿ         DATE: 2008-08-18
 *==========================================================================*/
int decode( const char *OrgStr, char * DesStr );

/*==========================================================================*
 * FUNCTION : encode
 * PURPOSE  :  base64 encode
 * CALLS    :
 * CALLED BY:
 * ARGUMENTS:  in OrgStr
 *             out DesStr
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : ����ǿ         DATE: 2008-08-18
 *==========================================================================*/
int encode( const char *OrgStr, char *DesStr );

#endif//___BASE64_H___


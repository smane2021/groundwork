/*-----------------------------------------------------------------------------
 * 	Copyright(c) 2020, Vertiv Tech Co., Ltd.
 * 					ALL RIGHTS RESERVED
 *
 * 	PRODUCT	: RDU(Rack Data Unit)
 * 	FILENAME: mail_agent.c
 * 	CREATOR : 
 * 	DATE	: 
 * 	PURPOSE	: 
 *
 * 	HISTORY	:
 *----------------------------------------------------------------------------*/

#define MAIL_SERVICE_DEBUG_SWITCH 0   //���Կ���

#if MAIL_SERVICE_DEBUG_SWITCH
#define TRACEX printf
#endif

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <locale.h>

#include "public.h"
#include "stdsys.h"
#include "mail.h"
#include "base64.h"
#include "halcomm.h"
//#include "CM_GlobalStructure.h"

#include "../include/cfg_model.h"
#include "../include/basetypes.h"



#define MAIL_QUEUE_CAPACITY	100

#define SPLIT_TAG "--====MajorSplitTag===="

typedef struct tag_queue_elem 
{
	char *pInformMsg;
	char *pInformAddr;
	char *pHeader;
}queue_elem;


//ע�⣬������è��baud���ˣ�����ҲҪ��
char g_szGprsSetting[MAX_GPRS_COMM_PARAM_SIZE] = {"460800,n,8,1"}; //gprs

//static pthread_mutex_t	g_hEmailSmsMutex = PTHREAD_MUTEX_INITIALIZER;
//#define INIT_EMAIL_SERVICE()	   pthread_mutex_init(&g_hEmailSmsMutex, NULL)
//#define LOCK_EMAIL_SERVICE()       pthread_mutex_lock(&g_hEmailSmsMutex)
//#define UNLOCK_EMAIL_SERVICE()     pthread_mutex_unlock(&g_hEmailSmsMutex)
//#define DESTROY_EMAIL_SERVICE()    pthread_mutex_destroy(&g_hEmailSmsMutex)

#define MAX_PENDING_EMAIL_SEND	    64

#define DELETE_ITEM(p) if ((p) != NULL) \
    { DELETE(p);  (p) = NULL; }

//The extend alarm signal value
//Include the active or deactive status of alarm
struct tagAlarmSigValueExt
{
    int				nAlarmMsgType;
    ALARM_SIG_VALUE	stAlarmSigValue;

    ULONG				ulAlarmSequenceID;
};
typedef struct tagAlarmSigValueExt ALARM_SIG_VALUE_EXT;


/* ���ýӿ� configuration info */
tagStruMailAgentConf g_struMailAgentConf;

/*Email task manage*/
typedef struct tagEmailManage
{
	//Handle of This Service thread
	HANDLE		hThreadSelf;


	int		iMailAgentConfSuccess;    //1-success 0-fail

	HANDLE		hTrapSendQueue;	  /* alarm trap send queue*/
	ULONG		ulAlarmSequenceID;

	//���ڻ�ȡ���澯��Ϣ��ķ�����Ϣ
	HANDLE		hOtherInforSendQueue;	  /* Other Information (exclude alarms) send queue*/

}EMAIL_MANAGE;
EMAIL_MANAGE g_sEmailManage;


static int Email_LoadConfigFile(void);
static BOOL send(queue_elem *elem);
BOOL DriverInit(LPVOID lpReserve);

/*=====================================================================*
* Function name: EncodingBase64
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void EncodingBase64(const char* src, char* des)
{
	if (src == NULL || des == NULL) {
		return;
	}

	char sixbit_encoding[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	while(*src!=(char)NULL) {
		*des++=sixbit_encoding[(unsigned char)*src++>>2]; //6
		if(*src==(char)NULL)
		{
			*des++=sixbit_encoding[(*(src-1)&0x3)<<4];//2+4
			*des++='=';
			*des++='=';
			break;
		}
		else
		{
			*des++=sixbit_encoding[(*(src-1)&0x3)<<4 | (unsigned char)(*src)>>4];//2+4
			src++;
		}
		if(*src==(char)NULL)
		{
			*des++=sixbit_encoding[(*(src-1)&0xF)<<2];//4+2
			*des++='=';
		}
		else
		{
			*des++=sixbit_encoding[(*(src-1)&0xF)<<2 | (unsigned char)(*src)>>6];//4+2
			*des++=sixbit_encoding[*src&0x3F];//6
			src++;
		}
	}
	*des=(char)NULL;
}
/*=====================================================================*
* Function name: EncodingBase64Adv
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
void EncodingBase64Adv(const char* src, int length,char* des)
{
	int i=0;
	char sixbit_encoding[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	if (des == NULL) {
		return;
	}
	while(i<length)
	{
		*des++=sixbit_encoding[(unsigned char)src[i++]>>2]; //6
		if(i==length)
		{
			*des++=sixbit_encoding[(src[i-1]&0x3)<<4];//2+4
			*des++='=';
			*des++='=';
			break;
		}
		else
		{
			*des++=sixbit_encoding[(src[i-1]&0x3)<<4 | (unsigned char)(src[i])>>4];//2+4
			i++;
		}
		if(i==length)
		{
			*des++=sixbit_encoding[(src[i-1]&0xF)<<2];//4+2
			*des++='=';
		}
		else
		{
			*des++=sixbit_encoding[(src[i-1]&0xF)<<2 | (unsigned char)(src[i])>>6];//4+2
			*des++=sixbit_encoding[src[i]&0x3F];//6
			i++;
		}
	}
	*des=(char)NULL;
}

/*========================================
 *��������rtrim
 *���룺szSource
 *�����szSource
 *���ã�
 *�����ã�
 *��������ȥ���Ҳ�Ŀո�
 *========================================*/
static char* rtrim(char* szSource)
{
	if( 0==szSource)
		return szSource;

	char* pszEOS;
	pszEOS = szSource + strlen(szSource) - 1;
	while(pszEOS >= szSource && *pszEOS == ' ')
		*pszEOS-- = '\0';
	return szSource;
}


/*========================================
 *��������ltrim
 *���룺szSource
 *�����szSource
 *���ã�
 *�����ã�
 *��������ȥ�����Ŀո�
 *========================================*/
static char* ltrim(char* szSource)
{
	char* pszBOS;
	pszBOS = szSource;
	while(*pszBOS == ' ')
		pszBOS++;
	strcpy(szSource,pszBOS);
	return szSource;
}


/*========================================
 *��������trim
 *���룺szSource
 *�����szSource
 *���ã�
 *�����ã�rtrim   ltrim
 *��������ȥ�����ҵĿո�
 *========================================*/
static char* trim(char* szSource)
{
	rtrim(szSource);
	ltrim(szSource);
	return szSource;
}

/*========================================
 *��������isEmail
 *��  �룺s
 *��  ����0 ��ЧEmail   -1 ��ЧEmail
 *��  �ã�
 *�����ã�
 *��  �������eamil�Ƿ���Ч
 *========================================*/
int isEmail(char *s)
{
	int nat = 0;

	for(; *s; s++)
	{
		if(*s == '@')
			nat++;
		else if(!isalpha(*s) && !isdigit(*s) && !strchr("_.-+/", *s))
			return 0;
	}
	return nat==1;
}


/*==========================================================================*
 * FUNCTION : isDigitalStr
 * PURPOSE  : �ж��ַ����Ƿ�������
 * CALLS    :
 * CALLED BY:
 * ARGUMENTS:
 *       in  strBuf ���жϵ��ַ���
 * RETURN   : int
 * COMMENTS :
 * CREATOR  : ����ǿ                DATE: 2008-08-18
 *==========================================================================*/
int isDigitalStr(const char * strBuf)
{

	const char * pBuf = strBuf;
	int i=0;
	int iSize = 0 ;

	if(0 == strBuf)
		return FALSE;

	iSize = strlen(strBuf);
	if (0 == iSize)
	{
		return FALSE;
	}

	for (;( i<iSize && (  (('0' <= (*pBuf)) && ('9' >= (*pBuf))  )? 1 : 0));i++, pBuf++);
	//printf("i=%d,iSize=%d",i,iSize);

	if (i == iSize)
		return TRUE;
	else
		return FALSE;

}

/*==========================================================================*
 * FUNCTION : strscan
 * PURPOSE  : ��鷵�ص���Ӧ�����Ƿ���ȷ
 * CALLS    :
 * CALLED BY:
 * ARGUMENTS: MailHeader *oMailHeader
 * RETURN   : TRUE �ɹ�, FALSE ʧ��.
 * COMMENTS :
 * CREATOR  : Zhang Rong Qiang                   DATE: 2008-08-21
 *==========================================================================*/

int strscan(char *line, char *str, int len)
{

	int i;
	int nLength = 0;


	if((0 == line) || (0 == str))
	{
		return FALSE;
	}

	nLength = strlen(line) ;

	if(nLength < len)
	{
		return FALSE;
	}

	for (i = 0 ; i < len ; i++)
	{
		if (line[i] == str[0])
		{
			if (!strncmp(line, str, 3))
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : dialog
 * PURPOSE  : ͨ��TCP/IPͨ�ţ�ִ��һ��SMTPЭ��Ự
 * CALLS    : CommWrite,CommRead
 * CALLED BY:
 * ARGUMENTS: MailHeader *oMailHeader
 * RETURN   : TRUE �ɹ�, FALSE ʧ��.
 * COMMENTS :
 * CREATOR  : Zhang Rong Qiang                   DATE: 2008-08-21
 *==========================================================================*/
int dialog(HANDLE hComm, char *command, char *szRsp, int nRecvLen)
{
#define MAX_DLG_RECV_LEN      512   //���Ự����buf����
#define MAX_DLG_RETRY_TIMES   4     //���Ự���Խ��մ���

	int ret, len;
	int nRepeatTimes = MAX_DLG_RETRY_TIMES;
	int nLen  = 0;

	char line[MAX_DLG_RECV_LEN+1];

	if(MAX_DLG_RECV_LEN < nRecvLen) {
#if MAIL_SERVICE_DEBUG_SWITCH
		printf("[EMail Service] required size is too long\n");
#endif
		return FALSE;
	}

	memset(line, 0, MAX_DLG_RECV_LEN + 1);

	if (command != NULL) {
		len = strlen(command);

#if MAIL_SERVICE_DEBUG_SWITCH
		printf("[EMail Service] send message:[%s]!\n",command);
#endif

		if (CommWrite(hComm, command, len) != len) {
#if MAIL_SERVICE_DEBUG_SWITCH
			printf("[EMail Service] send message to server error ,sended data length[%d]!\n",len);
#endif

			return FALSE;
		}
	}

	if (szRsp != NULL) {
		if (0 == nRecvLen) {
			/* just read one time in fixed waiting time */
			ret = CommRead(hComm, line, MAX_DLG_RECV_LEN);

			if (0 == ret) {
#if MAIL_SERVICE_DEBUG_SWITCH
				printf("[EMail Service] nothing to be read!\n");
#endif
				return FALSE;
			} else if (0 > ret) {
#if MAIL_SERVICE_DEBUG_SWITCH
				printf("[EMail Service] read data failue!\n");
#endif
				return FALSE;
			}

#if MAIL_SERVICE_DEBUG_SWITCH
			printf("[EMail Service] read message:[%s]!\n",line);
#endif

			line[ret] = 0;

			if ( 0 == strstr(line, szRsp)) {
				return FALSE;
			}

		} else if (0 < nRecvLen) {
			/* wait to receive object length of data, if get the object reply, do return */
			nLen = nRecvLen;

			ret = 0;
			nRepeatTimes = MAX_DLG_RETRY_TIMES;
			memset(line, 0, sizeof(line));

			do {
				int nRead = CommRead(hComm, line+ret, nLen-ret);
				ret += nRead;

				if( 0 == nRead) {
					//printf("[EMail Service] nothing to be read!\n");
					//return FALSE;
				} else if (0 > nRead) {
#if MAIL_SERVICE_DEBUG_SWITCH					
					printf("[EMail Service] read data failue!\n");
#endif
					return FALSE;
				}

				if (ret == nRecvLen) {
					break;
				}

				if (strstr(line, szRsp) != 0) {
#if MAIL_SERVICE_DEBUG_SWITCH
					printf("[EMail Service] get the object response message:[%s]\n", line);
#endif
					return TRUE;
				}

				if (strstr(line, "ERROR") != 0) {
					break;
				}

				nRepeatTimes --;

			} while (nRepeatTimes > 0);

			line[ret] = 0;

#if MAIL_SERVICE_DEBUG_SWITCH
			printf("[EMail Service] read message:[%s]! and the required length:[%d]\n"
					, line, nRecvLen);
#endif

			if (szRsp != NULL && 0 < nRecvLen) {
				if (0 == strstr(line, szRsp)) {
					return FALSE;
				}
			}
		}
	}

	return TRUE;
}

HANDLE initComm(int nMsgType, void *pCfg)
{
	HANDLE hComm = NULL;
	int nErrCode = 0;
	char CommSetting[100];

	memset(CommSetting, 0, sizeof(CommSetting));

	//�����ʼ�����
	if (OUTPUT_EMAIL == nMsgType)
	{
		if (NULL == pCfg)
		{
			//no config
			if (0 == strlen(g_struMailAgentConf.szEmailSeverIP)
					|| 0 == g_struMailAgentConf.iEmailServerPort)
			{
#if MAIL_SERVICE_DEBUG_SWITCH
				printf("[EMail Service] no config ,the  server address [%s] and port[%s]\n",
						g_struMailAgentConf.szEmailSeverIP, g_struMailAgentConf.iEmailServerPort);
#endif

				return NULL;
			}
			sprintf(CommSetting, "%s:%d", g_struMailAgentConf.szEmailSeverIP, g_struMailAgentConf.iEmailServerPort);

		}
		//������������Բ���
		else
		{
			//no config
			if (0 == strlen(((stMailSevCfg *)pCfg)->m_szEmailServer)
					|| 0 == strlen(((stMailSevCfg *)pCfg)->m_szEmailPort))
			{

#if MAIL_SERVICE_DEBUG_SWITCH
				printf("[EMail Service] invalid config ,the  server address [%s] and port[%s]\n",
						((stMailSevCfg *)pCfg)->m_szEmailServer,((stMailSevCfg *)pCfg)->m_szEmailPort);
#endif

				return NULL;
			}

			sprintf(CommSetting, "%s:%s",
					((stMailSevCfg *)pCfg)->m_szEmailServer,
					((stMailSevCfg *)pCfg)->m_szEmailPort);
		}
	}
	//�������Ϣ����
	else if (OUTPUT_SMSEMAIL == nMsgType)
	{
		if (0 == pCfg)
		{
			//no config
			if (0 == strlen(g_struMailAgentConf.szSMSSMTPServerIP)
					|| 0 == g_struMailAgentConf.iSMSSMTPServerPort)
			{
#if MAIL_SERVICE_DEBUG_SWITCH
				printf("[EMail Service] no config ,the  server address [%s] and port[%d]\n",
						g_struMailAgentConf.szSMSSMTPServerIP, g_struMailAgentConf.iSMSSMTPServerPort);
#endif
				return NULL;
			}
			sprintf(CommSetting, "%s:%d", g_struMailAgentConf.szSMSSMTPServerIP, g_struMailAgentConf.iSMSSMTPServerPort);

		}//������������Բ���
		else
		{
			//Begin modify by likun for compile warning 20090215
#if MAIL_SERVICE_DEBUG_SWITCH
			stMailSevCfg * ostMailSevCfg = (stMailSevCfg *)pCfg;
			//End modify by likun for compile warning 20090215

			printf("assemble the comm setting ... \n");
			printf("g_szSmsServer  =[%s] and ostMailSevCfg->m_szSmsServer [%s]\n", g_struMailAgentConf.szSMSSMTPServerIP, ostMailSevCfg->m_szEmailServer);
			printf("g_szSmsPort    =[%d] and ostMailSevCfg->m_szSmsPort [%s]\n", g_struMailAgentConf.iSMSSMTPServerPort, ostMailSevCfg->m_szEmailPort);
			printf("g_szSmsUser    =[%s] and ostMailSevCfg->m_szSmsUser [%s]\n", g_struMailAgentConf.szSMSSMTPUser, ostMailSevCfg->m_szEmailUser);
			printf("g_szSmsPassword=[%s] and ostMailSevCfg->m_szSmsPassword [%s]\n", g_struMailAgentConf.szSMSSMTPPasswd, ostMailSevCfg->m_szEmailPassword);
			printf("((stMailSevCfg *)pCfg)->m_szSmsServer  [%s]\n",((stMailSevCfg *)pCfg)->m_szEmailServer);
			printf("((stMailSevCfg *)pCfg)->m_szSmsPort    [%s]\n",((stMailSevCfg *)pCfg)->m_szEmailPort);

#endif
			//no config
			if (0 == strlen(((stMailSevCfg *)pCfg)->m_szEmailServer)
					|| 0 == strlen(((stMailSevCfg *)pCfg)->m_szEmailPort))
			{
#if MAIL_SERVICE_DEBUG_SWITCH
				printf("[EMail Service] invalid config ,the  server address [%s] and port[%d]\n",
						((stMailSevCfg *)pCfg)->m_szEmailServer,((stMailSevCfg *)pCfg)->m_szEmailPort);
#endif
				return NULL;
			}

			sprintf(CommSetting, "%s:%s",
					((stMailSevCfg *)pCfg)->m_szEmailServer,
					((stMailSevCfg *)pCfg)->m_szEmailPort);
		}
	}
	//GPRS����Ϣ����
	else if (OUTPUT_SMSGPRS == nMsgType)
	{
		//no config
		if (0 == strlen(g_szGprsSetting))
		{
#if MAIL_SERVICE_DEBUG_SWITCH
			printf("[EMail Service] invalid config ,the GPRS setting [%s]\n",
					g_szGprsSetting);
#endif
			return NULL;
		}
		sprintf(CommSetting, "%s", g_szGprsSetting);
	}


#if  MAIL_SERVICE_DEBUG_SWITCH
	TRACEX("CommSetting is [%s] ......\n",CommSetting);
#endif
	//sprintf(CommSetting, "%s", "460800,n,8,1");

	//����Ϣ��ʽ
	if (OUTPUT_SMSGPRS == nMsgType)
	{
		//���豸,������"AT+SET" then "AT+CONNECT"
		sprintf(CommSetting, "%s", "460800,n,8,1");
		hComm = CommOpen(EDGE_COMM_LIB,
				EDGE_COMM_DEV,
				CommSetting,    // use the port setting of the sampler.
				COMM_CLIENT_MODE,
				30000,  //Timeout Unit: millisecond
				&nErrCode);

		if (0 == hComm)
		{
			printf("[EMail Service] Open USB connecting error with setting[%s],dev=[%s]!\n",
					CommSetting,EDGE_COMM_DEV);
			return NULL;
		}

	}
	//�ʼ���ʽ
	else if (OUTPUT_EMAIL == nMsgType || OUTPUT_SMSEMAIL == nMsgType)
	{
		hComm = CommOpen(SMTP_COMM_LIB,
				SMTP_COMM_DEV,
				CommSetting,    // use the port setting of the sampler.
				COMM_CLIENT_MODE,
				30000,  //Timeout Unit: millisecond
				&nErrCode);
		if (0 == hComm)
		{
			printf("[EMail Service] Open tcp connecting error with setting[%s]!\n",
					CommSetting);
			return NULL;
		}
	}

	return hComm;
}

int uninitComm(HANDLE hComm)
{
    if (hComm != 0)
    {
        CommClose(hComm);
    }
    hComm = NULL;

    return TRUE;
}

/*==========================================================================*
* FUNCTION : sendSms
* PURPOSE  : ����һ���ʼ�ͷ��ʽ������������email���ʼ�������
* CALLS    : CommOpen,dialog
* CALLED BY:
* ARGUMENTS: SmsHeader *oSmsHeader
*          : int nMsgType ��Ϣ����  0:EMail , 1:Sms
*          : HANDLE hComm ���
* RETURN   : TRUE �ɹ�, FALSE ʧ��.
* COMMENTS :
* CREATOR  : Zhang Rong Qiang                   DATE: 2008-08-21
*==========================================================================*/
int sendSms(SmsHeader *oSmsHeader, HANDLE hComm, BOOL bNeedSetting)
{
#if MAIL_SERVICE_DEBUG_SWITCH
	TRACEX("enter sendSms() ......\n");
#endif

	int nResult  = TRUE;
	char line[MAIL_MAX_LINE_SIZE];

	if (hComm == NULL) {
		printf("[EMail Service] sendSms open comm handle is NULL!\n");
		return FALSE;
	}

	do
	{
		if (bNeedSetting)
		{
#if MAIL_SERVICE_DEBUG_SWITCH
			TRACEX("setting the content type of the short message to be sended\n");
#endif
			//���ö���Ϣ���ݸ�ʽΪ"�ı�"
			if (FALSE == dialog(hComm, "AT+CMGF=1\r", "OK" , 0))
			{
				nResult = FALSE;
				break;
			}

#if  MAIL_SERVICE_DEBUG_SWITCH
			TRACEX("setting the character-collection\n");
#endif
			//���ö���Ϣ�ַ���
			if (FALSE == dialog(hComm, "AT+CSCS=\"GSM\"\r", "OK", 0 ))
			{
				nResult = FALSE;
				break;
			}

#if MAIL_SERVICE_DEBUG_SWITCH
			TRACEX("setting the content-type's parameters \n");
#endif
			//���ö���Ϣ�ı�ģʽ����
			/*
			if (FALSE == dialog(hComm, "AT+CSMP=17,168,0,0\r", "OK", sizeof("AT+CSMP=17,168,0,0\r")+strlen("OK")+20))
			{
				nResult = FALSE;
				break;
			}
			*/
		}

		usleep(1000000);
#if  MAIL_SERVICE_DEBUG_SWITCH
		TRACEX("send the subscriber description\n");
#endif
		memset(line, 0, sizeof(line));
		sprintf(line, "AT+CMGS=\"%s\"\r\r",
				oSmsHeader->recipient);

		//���ͽ�����
		if (FALSE == dialog(hComm, line, ">", 0))
		{
			nResult = FALSE;
			break;
		}

#if MAIL_SERVICE_DEBUG_SWITCH
		TRACEX("send short message[%s] to a special subscriber\n"
				,oSmsHeader->subject);
#endif
		memset(line, 0, sizeof(line));
		sprintf(line, "%s %s\r%c",
				oSmsHeader->subject,
				oSmsHeader->contents,
				0x1a);

		//���Ͷ���Ϣ�ı�
		if (FALSE == dialog(hComm, line, "OK", (int)(strlen(line)+strlen("OK")+20)))
		{
			nResult = FALSE;
			break;
		}

	} while (0);

#if  MAIL_SERVICE_DEBUG_SWITCH
	TRACEX("exit sendSms()........\n");
#endif

	return(nResult);
}

/*==========================================================================*
* FUNCTION : sendMail
* PURPOSE  : ����һ���ʼ�ͷ��ʽ������������email���ʼ�������
* CALLS    : CommOpen,dialog
* CALLED BY:
* ARGUMENTS: MailHeader *oMailHeader
*          : int nMsgType ��Ϣ����  0:EMail , 1:Sms by Email
* RETURN   : TRUE �ɹ�, FALSE ʧ��.
* COMMENTS :
* CREATOR  : Zhang Rong Qiang                   DATE: 2008-08-21
*==========================================================================*/
int sendMail(MailHeader *oMailHeader, stMailSevCfg *ostMailSevCfg, HANDLE hComm)
{
	char szResult[LOGIN_MAX_BASE64_SIZE] ;
	int nResult  = TRUE;
	char line[MAIL_MAX_LINE_SIZE];
	int i;

	if (0 == hComm) {
		printf("[EMail Service] open comm handle is NULL!\n");
		return FALSE;
	}


	do {
		//��ȡ���ӽ��������Ӧ��Ϣ
		if (FALSE == dialog(hComm, NULL, "220" , 0)) {
			printf("[EMail Service] building connection error!\n");
			nResult = FALSE;
			break;
		}

		//say hello to  get greeting and await reponse authentication
		if (FALSE == dialog( hComm, "ehlo VerrtivNetwork.com.cn\r\n", "250", 0)) {
			printf("[EMail Service] get authentication-method error!\n");
			nResult = FALSE;
			break;
		}


		if (((stMailSevCfg*)ostMailSevCfg)->iNeedAuthen == 1) {
			//authentication login
			if (FALSE == dialog(hComm, "auth login\r\n", "334", 0)) {
				printf("[EMail Service] send login error!\n");
				nResult = FALSE;
				break;
			}

			memset(szResult, 0, LOGIN_MAX_BASE64_SIZE);
			if (FALSE == encode(((stMailSevCfg*)ostMailSevCfg)->m_szEmailUser, szResult)) {
				printf("[EMail Service] user encode error!\n");
				nResult = FALSE;
				break;
			}

			sprintf(line, "%s\r\n", szResult);
			//authentication user info
			if (FALSE == dialog(hComm, line, "334", 0)) {
				printf("[EMail Service] send user infomation error!\n");
				nResult = FALSE;
				break;
			}

			memset(szResult, 0 ,LOGIN_MAX_BASE64_SIZE);
			if (FALSE == encode(((stMailSevCfg*)ostMailSevCfg)->m_szEmailPassword, szResult)) {
				printf("[EMail Service] password encode error!\n");
				nResult = FALSE;
				break;
			}

			sprintf(line, "%s\r\n", szResult);
			//authentication password
			if (FALSE == dialog(hComm, line, "235", 0)) {
				printf("[EMail Service] The user or password information error!\n");
				nResult = FALSE;
				break;
			}
		}


		// Send MAIL FROM and await response
		sprintf(line, "MAIL FROM:<%s>\r\n", oMailHeader->sender);
		if (FALSE == dialog(hComm, line, "250", 0)) {
			printf("[EMail Service] send the original email-address error!\n");
			nResult = FALSE;
			break;
		}

		// Send RCPT TO and await response //
		sprintf(line, "RCPT TO:<%s>\r\n", oMailHeader->recipient);
		if (FALSE == dialog(hComm, line, "250", 0)) {
			printf("[EMail Service] send the destination email-address error!\n");
			nResult = FALSE;
			break;
		}

		// Send DATA and await response //
		if (FALSE == dialog(hComm, "DATA\r\n", "354", 0)) {
			printf("[EMail Service] send Data error!\n");
			nResult = FALSE;
			break;
		}

		char buff_head[1024];
		memset(buff_head, 0, sizeof(buff_head));

		time_t tm;
		time(&tm);
		char szTime[128];
		memset(szTime, 0, sizeof(szTime));
		ctime_r(&tm, szTime);
		szTime[strlen(szTime)-1] = '\0'; /* remove last '\n' */
		sprintf(buff_head, "Date:%s\r\n", szTime);

		memset(szResult, 0, sizeof(szResult));
		strcat(buff_head, "From: <");
		strcat(buff_head, oMailHeader->sender);
		strcat(buff_head, ">\r\n");

		memset(szResult, 0, sizeof(szResult));
		strcat(buff_head, "To: <");
		strcat(buff_head, oMailHeader->recipient);
		strcat(buff_head, ">\r\n");

		memset(szResult, 0, sizeof(szResult));
		strcat(buff_head, "Subject: ");
		strcat(buff_head, oMailHeader->subject);
		strcat(buff_head, "\r\n");

		strcat(buff_head, "Mime-Version: 1.0\r\n");

		if (oMailHeader->iAppendFileNum == 0) {
			/* do not have append file to send */
			strcat(buff_head, "Content-Type: text/plain;\r\n\tcharset=\"gb2312\"\r\n");
			strcat(buff_head, "Content-Transfer-Encoding: base64\r\n");
		} else {
			/* have append file to send */
			strcat(buff_head, "Content-Type: multipart/mixed;\r\n\tboundary=\"");
			strcat(buff_head, "====MajorSplitTag====");
			strcat(buff_head, "\"\r\n");
		}

		if (dialog(hComm, buff_head, NULL, 0) == FALSE) {
			printf("[Email Service] send head failed\n");
			nResult = FALSE;
			break;
		}

		char buff_body[1024*128];
		memset(buff_body, 0, sizeof(buff_body));

		if (oMailHeader->iAppendFileNum == 0) {
			/* have no append file, just sent email content */
			memset(buff_head, 0, sizeof(buff_head));
			EncodingBase64(oMailHeader->contents, buff_head);
			strcat(buff_body, "\r\n");
			strcat(buff_body, buff_head);
			strcat(buff_body, "\r\n\r\n.\r\n");
			if (FALSE == dialog(hComm, buff_body, "250", 0)) {
				printf("[EMail Service] send the Emal-content error!\n");
				nResult = FALSE;
				break;
			}
		} else {
			strcat(buff_body, "This is a multi-part message in MIME format.\r\n\r\n");
			strcat(buff_body, SPLIT_TAG);
			strcat(buff_body, "\r\nContent-Type: text/plain;\r\n\tcharset=\"gb2312\"\r\n");
			strcat(buff_body, "Content-Transfer-Encoding: base64\r\n\r\n");
			memset(buff_head, 0, sizeof(buff_head));
			EncodingBase64(oMailHeader->contents, buff_head);
			strcat(buff_body, buff_head);
			strcat(buff_body, "\r\n");
			if (FALSE == dialog(hComm, buff_body, NULL, 0)) {
				printf("[EMail Service] send the Emal-content error!\n");
				nResult = FALSE;
				break;
			}

			for (i = 0; i < oMailHeader->iAppendFileNum; ++i) {
				FILE *fp = fopen(oMailHeader->szAppendFileName[i], "r");
				if (fp == NULL) {
					printf("[Email Service] open file %s failed\n", oMailHeader->szAppendFileName[i]);
					continue;
				}

				/* get file size */
				long file_size = 0;
				if (fseek(fp, 0, SEEK_END) == 0) {
					file_size = ftell(fp);
				}
				rewind(fp);

				if (file_size > 1024*125) {
					printf("[Email Service] file size > 125K, will not send!\n");
					fclose(fp);
					continue;
				}

				/* read file content to buff */
				char file_content[1024*128];
				memset(file_content, 0, sizeof(file_content));

				fread(file_content, 1, file_size, fp);
			 	if (feof(fp) > 0) {
					printf("[Email Service] read %s content to buffer failed!\n", 
							oMailHeader->szAppendFileName[i]);
					fclose(fp);
					continue;
				}

				fclose(fp);
				
				memset(buff_body, 0, sizeof(buff_body));
				strcat(buff_body, SPLIT_TAG);
				strcat(buff_body, "\r\nContent-Type: application/octet-stream;\r\n\tname=\"");
				char file_name[128];
				memset(file_name, 0, sizeof(file_name));
				EncodingBase64(oMailHeader->szAppendFileName[i], file_name);
				strcat(buff_body, "=?gb2312?B?");
				strcat(buff_body, file_name);
				strcat(buff_body, "?=\"\r\n");
				strcat(buff_body, "Content-Transfer-Encoding: base64\r\n");
				strcat(buff_body, "Content-Disposition: attachment;\r\n\t");
				strcat(buff_body, "filename=\"");
				strcat(buff_body, "=?gb2312?B?");
				strcat(buff_body, file_name);
				strcat(buff_body, "?=\"\r\n\r\n");

				if (FALSE == dialog(hComm, buff_body, NULL, 0)) {
					printf("[Email Service] send %s content head to SMTP server failed!\n", 
							oMailHeader->szAppendFileName[i]);
					continue;
				}
				
				memset(buff_body, 0, sizeof(buff_body));
				
				char buffer[3073], des[4097];
				int length = file_size;
				int curLen = 0;
				int pos = 0;
				while (length > 0) {
					if (length < 3072) {
						curLen = length;
					} else {
						curLen = 3072;
					}

					memset(buffer, 0, sizeof(buffer));
					memcpy(buffer, file_content+pos, curLen);
					pos += curLen;
					memset(des, 0, sizeof(des));
					EncodingBase64Adv(buffer, curLen, des);
					strcat(buff_body, des);
					length -= 3072;
				}
				strcat(buff_body, "\r\n");
				
				
				if (FALSE == dialog(hComm, buff_body, NULL, 0)) {
					printf("[Email Service] send %s content body to SMTP server failed!\n", 
							oMailHeader->szAppendFileName[i]);
					continue;
				}
			}

			memset(buff_body, 0, sizeof(buff_body));
			strcat(buff_body, SPLIT_TAG);
			strcat(buff_body, "\r\n\r\n.\r\n");

			if (FALSE == dialog(hComm, buff_body, "250", 0)) {
				printf("[Email Service] send attach file end sign to SMTP server failed!\n");
				nResult = FALSE;
				break;
			}
		}

		if (FALSE == dialog(hComm, "QUIT\r\n", "221", 0)) {
			printf("[EMail Service] destroy the connection error!\n");
			nResult = FALSE;
			break;
		}
	} while (0);



#if MAIL_SERVICE_DEBUG_SWITCH
	TRACEX("exit sendMail()........\n");
#endif

	return nResult;
}

/*=============================================================
 * ���ܣ����ַ���S��cSepΪ�ָ����ֶΡ�
 * ��ڣ�S-Դ�ַ�����D-��һ���ַ�����cSep-�ָ�����
 * ���أ���һ���ַ����ĸ���
 * ===========================================================*/
int StrToK(char *S, char *D, char cSep)
{
	int i=0;

	while (S[i] && S[i] != cSep)
	{
		D[i] = S[i];
		i++;
	}
	D[i] = 0;

	if (S[i])
	{
		return i + 1;
	}

	return 0;
}




/*==========================================================================*
* FUNCTION : ServiceNotification
* PURPOSE  : Get the report alarm info and put it into queue
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hService        : 
*            int     nMsgType        : 
*            int     nTrapDataLength : 
*            void*   lpTrapData      : 
*            void*   lpParam         :  
*            BOOL    bUrgent         : 
* RETURN   : int : 0 for success, 1 for error(defined in DXI module)
* COMMENTS : 
* CREATOR  : HULONGWEN                   DATE: 2004-10-22 17:23
*==========================================================================*/
int ServiceNotification(HANDLE hService, 
			int nMsgType, 
			int nTrapDataLength, 
			void *lpTrapData,	
			void *lpParam, 
			BOOL bUrgent)
{
    UNUSED(hService);
    UNUSED(bUrgent);
    //int i = 0;

    if ((nMsgType & (_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK)) != 0)
    {
	//Because the trap send info include active or deactive status,
	//so extend the alarm signal value structure
	ALARM_SIG_VALUE_EXT	stAlarmSigValue;
	EMAIL_MANAGE* pSnmpInfoStru = (EMAIL_MANAGE*)lpParam;

	ASSERT(pSnmpInfoStru);

	if (nTrapDataLength != sizeof(ALARM_SIG_VALUE))
	{
	    return 0;
	}

	memcpy(&(stAlarmSigValue.stAlarmSigValue), lpTrapData, 
	    sizeof(ALARM_SIG_VALUE));

	stAlarmSigValue.nAlarmMsgType = nMsgType;

	pSnmpInfoStru->ulAlarmSequenceID += 1;

	stAlarmSigValue.ulAlarmSequenceID = pSnmpInfoStru->ulAlarmSequenceID;

	//Put the report info to alarm queue
	Queue_Put(pSnmpInfoStru->hTrapSendQueue, &stAlarmSigValue, FALSE);

	//i++;
	//printf("\nSMS Notification %d : %s \n", i, stAlarmSigValue.stAlarmSigValue.pStdSig->pSigName->pFullName[0]);
    }

    return 0;
}
/*=====================================================================*
* Function name: AddCurrentAlarmsToQueue
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static BOOL AddCurrentAlarmsToQueue(HANDLE hTrapSendQueue)
{
    int				nActiveAlarmNum = 0;
    ALARM_SIG_VALUE* pActiveAlarmSigValue = NULL;
    ALARM_SIG_VALUE* pAlarmSigValue = NULL;

    if (DXI_GetActiveAlarmItems(&nActiveAlarmNum, 
	&pActiveAlarmSigValue)
	== ERR_DXI_OK)
    {
	int i;

	ALARM_SIG_VALUE_EXT	stAlarmSigValue;

	pAlarmSigValue = pActiveAlarmSigValue;

	for (i = 0; i < nActiveAlarmNum; i++, pActiveAlarmSigValue++)
	{
	    memcpy(&(stAlarmSigValue.stAlarmSigValue),
		pActiveAlarmSigValue, 
		sizeof(ALARM_SIG_VALUE));

	    stAlarmSigValue.nAlarmMsgType = _ALARM_OCCUR_MASK;

	    g_sEmailManage.ulAlarmSequenceID += 1;

	    stAlarmSigValue.ulAlarmSequenceID = 
		g_sEmailManage.ulAlarmSequenceID;

	    //Put the report info to alarm queue
	    Queue_Put(hTrapSendQueue, &stAlarmSigValue, FALSE);

	}

	DELETE_ITEM(pAlarmSigValue);

	return TRUE;
    }

    return FALSE;
}
/*=====================================================================*
* Function name: AddCurrentAlarmsToQueue
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void InitMAIL(void)
{
    int iRet;

    iRet = Email_LoadConfigFile();

    if(iRet == ERR_CFG_OK)
    {			
	g_sEmailManage.iMailAgentConfSuccess = TRUE;
    }	
    else
    {
	g_sEmailManage.iMailAgentConfSuccess = FALSE;
    }

    if(g_sEmailManage.iMailAgentConfSuccess == TRUE)
    {	
	/* register in the DXI module to receive Alarms */
	iRet = DxiRegisterNotification("EMAIL", 
	    ServiceNotification,
	    &g_sEmailManage, // it shall be changed to the actual arg
	    _ALARM_OCCUR_MASK|_ALARM_CLEAR_MASK/*|_SAMPLE_MASK|_SET_MASK|_CONTROL_MASK*/, 
	    g_sEmailManage.hThreadSelf,
	    _REGISTER_FLAG);

	if (iRet != 0)
	{
	    AppLogOut(EMAIL_TASK, APP_LOG_ERROR,
		"Fails on calling DxiRegisterNotification.\n");
	}

	// init the alarm trap send queue
	g_sEmailManage.hTrapSendQueue = Queue_Create(
	    MAX_PENDING_EMAIL_SEND,
	    sizeof(ALARM_SIG_VALUE_EXT),
	    0);

	if (g_sEmailManage.hTrapSendQueue == NULL)
	{
	    AppLogOut(EMAIL_TASK, APP_LOG_ERROR, 
		"Fails on creating EMAIL Alarm  Queue.\n");

	}

	if(g_sEmailManage.hTrapSendQueue != NULL)
	{
	    AddCurrentAlarmsToQueue(g_sEmailManage.hTrapSendQueue);
	}

	//init the other information (exclude alarms) send queue
	//INIT_EMAIL_SERVICE();
	g_sEmailManage.hOtherInforSendQueue = Queue_Create(MAIL_QUEUE_CAPACITY, sizeof(queue_elem), 0);
	if ( g_sEmailManage.hOtherInforSendQueue == NULL)
	{
	    AppLogOut(EMAIL_TASK, APP_LOG_ERROR, 
		"Fails on creating EMAIL Information Queue.\n");
	}

    }
}
/*==========================================================================*
* FUNCTION : GetTimeString
* PURPOSE  : Get a DateAndTime string according to time_t struct 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: time_t  tCurtime : 
*            char*   string   : return the needed string
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 11:40
*==========================================================================*/
static int GetTimeString(time_t tCurtime, char* string)
{
    struct tm		tmCurtime;
    //char			tmp;
#define TM_YEAR_START	1900

    gmtime_r(&(tCurtime), &tmCurtime);

    sprintf(string, "%04d%02d%02d %02d:%02d:%02d", 
	(tmCurtime.tm_year + TM_YEAR_START),
	(tmCurtime.tm_mon + 1),
	tmCurtime.tm_mday,
	tmCurtime.tm_hour,
	tmCurtime.tm_min,
	tmCurtime.tm_sec
	);

    return ERR_SNP_OK;
}
/*==========================================================================*
* FUNCTION : GetAlarmDescription
* PURPOSE  : Get alarm description: alarm name + its owner
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*            
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
#define EMAIL_ALARM_DISCRIPTION_LEN	    80
int GetAlarmDescription(ALARM_SIG_VALUE_EXT* pAlarmSigValue, char* string)
{
    char cAlarmLevel[32]= {0};
    //char powerMIB_timestr[15];

    switch(pAlarmSigValue->stAlarmSigValue.pStdSig->iAlarmLevel)
    {
    case ALARM_LEVEL_OBSERVATION:		/* OA */
	sprintf(cAlarmLevel, g_struMailAgentConf.szDesOA);
	break;

    case ALARM_LEVEL_MAJOR:			/* MA */
	sprintf(cAlarmLevel, g_struMailAgentConf.szDesMA);
	break;

    case ALARM_LEVEL_CRITICAL:			/* CA */
	sprintf(cAlarmLevel, g_struMailAgentConf.szDesCA);
	break;

    default:
	break;
    }

    //GetTimeString(pAlarmSigValue->stAlarmSigValue.sv.tmStartTime, powerMIB_timestr);

    snprintf(string, EMAIL_ALARM_DISCRIPTION_LEN, "%s, its owner: %s, %s", 
	pAlarmSigValue->stAlarmSigValue.pStdSig->pSigName->pFullName[0],
	pAlarmSigValue->stAlarmSigValue.pEquipInfo->pEquipName->pAbbrName[0],
	cAlarmLevel
	);
    
    

    return ERR_SNP_OK;
}
/*==========================================================================*
* FUNCTION : EMAIL_Trip
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* RETURN   :  FALSE-sent none    TRUE-Has sent one message
* COMMENTS : 
* CREATOR  :
*==========================================================================*/
//The status of alarm
enum STATUS_CHANGE_ENUM
{
    ALARM_STATUS_INVALIDATE = 0,
    ALARM_STATUS_ACTIVATED,
    ALARM_STATUS_DEACTIVATED,
};
//wait time on get from EMAIL trap queue.
#define WAIT_TIME_EMAIL_TRAP_QUEUE_GET	1000		// ms. 
#define WAIT_TIME_EMAIL_INFO_QUEUE_GET	500		// ms. 
#define EQUIP_EMAIL_SYSTEM				    1
#define SYSTEM_SETTING_EMAIL_ALARM_TRAP_LEVEL		    459

#define EMAIL_INFORMSG_NUM  128
#define EMAIL_HEADER_NUM  128
#define EMAIL_MAXLEN_SITENAME		20

static int EMAIL_Trip(void)
{
    ALARM_SIG_VALUE_EXT stAlarmSigValue;
    time_t	tCurTime;
    unsigned long nalarmTrapNo = 0;	
    int nalarmStatusChange = 0;

    //int	nAlarmLevel;
    //int i;

    char powerMIB_timestr[20] = {0};
    char cAlarmDiscription[EMAIL_ALARM_DISCRIPTION_LEN]= {0};
    char cAlarmTimeType[10]= {0};
    char acAlarmEmailInfo[EMAIL_INFORMSG_NUM] = {0};
    char acAlarmEmailAddr[EMAIL_INFORMSG_NUM] = {0};
    char acAlarmEmailHeader[EMAIL_HEADER_NUM] = {0};

    //int					nVarID = 0;				// Equipment's id
    //int					nVarSubID = 0;			// setting signal id
    //SIG_BASIC_VALUE			*pSigValue;
    //int					iInterfaceType = VAR_A_SIGNAL_VALUE;
    //int					iError = ERR_DXI_OK;
    //int					nTimeOut = 0;
    //int					nBufLen;

    queue_elem				elem;

    int nBuflen = 0;
    LANG_TEXT*	pLangInfo;
    char acSiteName[EMAIL_MAXLEN_SITENAME] = {0};


    //iError = DxiGetData(VAR_A_SIGNAL_VALUE, EQUIP_EMAIL_SYSTEM, 
//	DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SYSTEM_SETTING_EMAIL_ALARM_TRAP_LEVEL), 
//	&nBufLen, &pSigValue, nTimeOut);

    /*iError = DxiGetData(VAR_SMTP_INFO, SMTP_INFO_ALARM_LEVEL, 0, &nBufLen, &nAlarmLevel, 0);
    if(iError == ERR_DXI_OK )
    {
	g_sEmailManage.nTrapLevel = nAlarmLevel;
    }
    else
    {
	g_sEmailManage.nTrapLevel = ALARM_LEVEL_NONE;
    }
    */   

    //Get the alarm info from queue
    if (Queue_Get(g_sEmailManage.hTrapSendQueue, &stAlarmSigValue, FALSE, 
	WAIT_TIME_EMAIL_TRAP_QUEUE_GET) == ERR_QUEUE_OK)
    {
	if ((g_struMailAgentConf.iAlarmLevel != ALARM_LEVEL_NONE)
	    &&(g_struMailAgentConf.iAlarmLevel <= stAlarmSigValue.stAlarmSigValue.iAlarmLevel))
	{
	    //Send message 
	    nalarmTrapNo = stAlarmSigValue.ulAlarmSequenceID;

	    if ((stAlarmSigValue.nAlarmMsgType & _ALARM_OCCUR_MASK)
		== _ALARM_OCCUR_MASK)
	    {

		tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmStartTime;

		nalarmStatusChange = ALARM_STATUS_ACTIVATED;
		sprintf(cAlarmTimeType, "%s", "Start");
	    }
	    else if ((stAlarmSigValue.nAlarmMsgType & _ALARM_CLEAR_MASK)
		== _ALARM_CLEAR_MASK)
	    {
		tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmEndTime;

		nalarmStatusChange = ALARM_STATUS_DEACTIVATED;
		sprintf(cAlarmTimeType,"%s", "End");
	    }
	    else
	    {
		tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmStartTime;

		nalarmStatusChange = ALARM_STATUS_INVALIDATE;
		sprintf(cAlarmTimeType, "%s", "Start");
	    }

	    GetTimeString(tCurTime, powerMIB_timestr);

	    GetAlarmDescription(&stAlarmSigValue, cAlarmDiscription);

	    DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		SITE_NAME,
		0,
		&nBuflen,
		&pLangInfo,
		0);

	    snprintf(acSiteName, sizeof(acSiteName), "%s", pLangInfo->pFullName[0]);

	    //header
	    if(nalarmStatusChange == ALARM_STATUS_DEACTIVATED)
	    {
		snprintf(acAlarmEmailHeader, sizeof(acAlarmEmailHeader), "Alarm ceased on site %s", acSiteName);
	    }
	    else if(nalarmStatusChange == ALARM_STATUS_ACTIVATED)
	    {
		snprintf(acAlarmEmailHeader, sizeof(acAlarmEmailHeader), "Alarm activated on site %s", acSiteName);
	    }
	    
	    if(g_sEmailManage.iMailAgentConfSuccess == TRUE)	
	    {

		snprintf(acAlarmEmailInfo, sizeof(acAlarmEmailInfo), "%s, %s at: %s, From:%s", 
		    cAlarmDiscription,
		    cAlarmTimeType,
		    powerMIB_timestr,
		    acSiteName
		    );	
#if MAIL_SERVICE_DEBUG_SWITCH
    sprintf(acAlarmEmailAddr, "%s", "lian.kaifeng.kane@vertiv.com"); 
#else
    sprintf(acAlarmEmailAddr, "%s", g_struMailAgentConf.szEmailSendTo); 
#endif
		    elem.pInformMsg = acAlarmEmailInfo;
		    elem.pInformAddr = acAlarmEmailAddr;
		    elem.pHeader = acAlarmEmailHeader;
		
		    send(&elem);
		    //free(elem.pInformMsg);
		    //free(elem.pInformAddr);

		return TRUE;
	    }
	    

	}
	
    }
    
    return FALSE;
 
}

/*==========================================================================*
* FUNCTION : EMAIL_Information
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* RETURN   :  FALSE-sent none    TRUE-Has sent one message
* COMMENTS : 
* CREATOR  :
*==========================================================================*/
static int EMAIL_Information(void)
{
    queue_elem elem;

    if (Queue_Get(g_sEmailManage.hOtherInforSendQueue, &elem, FALSE, WAIT_TIME_EMAIL_INFO_QUEUE_GET) == ERR_OK) 
    {
	/* have something to send */
	send(&elem);
	free(elem.pInformMsg);
	free(elem.pInformAddr);
	free(elem.pHeader);
    }
    return FALSE;
}
/*=====================================================================*
* Function name: ExitEmail
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void	ExitEmail(void)
{


    if (g_sEmailManage.hTrapSendQueue != NULL)
    {
	Queue_Destroy(g_sEmailManage.hTrapSendQueue);
    }

    if (g_sEmailManage.hOtherInforSendQueue != NULL)
    {
	Queue_Destroy(g_sEmailManage.hOtherInforSendQueue);
    }

    return;
}
/*=====================================================================*
* Function name: GetUserInfo
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void GetUserInfoMail(int nUserNo)
{
    USER_INFO_STRU		*pUserInfo = NULL;
    int				iUserNum = 0;
    queue_elem			elem;

    int nBuflen = 0;
    LANG_TEXT*	pLangInfo;
    char acSiteName[EMAIL_MAXLEN_SITENAME] = {0};
    
    if (g_sEmailManage.hOtherInforSendQueue == NULL)
    {
	return;
    }	

    iUserNum = GetUserInfo(&pUserInfo);
    if((nUserNo < iUserNum) && (pUserInfo != NULL))
    {
	pUserInfo += nUserNo;

	// Message
	elem.pInformMsg = (char *)malloc(EMAIL_INFORMSG_NUM);
	if (elem.pInformMsg == NULL) 
	{
	    return;
	}
	memset(elem.pInformMsg, 0, EMAIL_INFORMSG_NUM);
	

	DxiGetData(VAR_ACU_PUBLIC_CONFIG,
	    SITE_NAME,
	    0,
	    &nBuflen,
	    &pLangInfo,
	    0);

	snprintf(acSiteName, sizeof(acSiteName), "%s", pLangInfo->pFullName[0]);

	snprintf(elem.pInformMsg, EMAIL_INFORMSG_NUM, "UserName: %s, Password: %s, SiteName: %s", 
	    pUserInfo->szUserName,
	    pUserInfo->szPassword,
	    acSiteName
	    );	
	

	// Address    
	elem.pInformAddr = (char *)malloc(EMAIL_INFORMSG_NUM);
	if (elem.pInformAddr == NULL) 
	{
	    free(elem.pInformMsg);
	    return;
	}
	memset(elem.pInformAddr, 0, EMAIL_INFORMSG_NUM);
	
	if(pUserInfo->szContactAddr[0] != 0)
	{
	    sprintf(elem.pInformAddr, "%s", pUserInfo->szContactAddr); 
	}
	else
	{
	    sprintf(elem.pInformAddr, "%s", g_struMailAgentConf.szEmailSendTo);
	}

	// Header    
	elem.pHeader = (char *)malloc(EMAIL_HEADER_NUM);
	if (elem.pHeader == NULL) 
	{
	    free(elem.pInformMsg);
	    free(elem.pInformAddr);
	    return;
	}
	memset(elem.pHeader, 0, EMAIL_HEADER_NUM);
	
	snprintf(elem.pHeader, EMAIL_HEADER_NUM, "Forgotten password from site %s", acSiteName); 
	
	
	Queue_Put(g_sEmailManage.hOtherInforSendQueue, &elem, FALSE);

    }    
}
/*=====================================================================*
* Function name: ServiceMain
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
#define		GET_SERVICE_OF_MAIL_NAME		"mail.so"

DWORD  ServiceMain(SERVICE_ARGUMENTS	*pArgs)
{
	int iRet;
	BOOL	bExitFlag = FALSE;

	int iBufLength;
	ACU_SMTP_INFO szSMTPInfo = {0};
	APP_SERVICE		*MAIL_AppService = NULL;
	MAIL_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_MAIL_NAME);

	
	UNUSED(pArgs);
	
	g_sEmailManage.hThreadSelf = RunThread_GetId(NULL);	

	InitMAIL();	

	
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread Mail Service was created, Thread Id = %d.\n", gettid());
#endif
	while (!bExitFlag)
	{
	    //1. Check if need exit 
	    RUN_THREAD_MSG	msg;
	    iRet = RunThread_GetMessage(g_sEmailManage.hThreadSelf, &msg, FALSE, 0);
	    if(iRet == ERR_OK)
	    {
		//  printf("not execute the SMS!\n");
		switch(msg.dwMsg)
		{
		case MSG_QUIT:
		    bExitFlag = TRUE;
		    break;	

		case MSG_TIMER:	
		    break;

		case MSG_SEND_PASSWORD:   //Send password
		    
		    GetUserInfoMail((int)msg.dwParam1);		    
		    break;

		default:
		    break;
		}//switch(msg.dwMsg)

	    } //END: if(iRet == ERR_OK)

	    // 2. Send message
	    else if(g_sEmailManage.iMailAgentConfSuccess == TRUE)
	    {
		//2.1 renew the settings 
		
		iBufLength = sizeof(ACU_SMTP_INFO);
		if(DxiGetData(VAR_SMTP_INFO, SMTP_INFO_ALL, 0,  &iBufLength,  &szSMTPInfo, 0) == ERR_DXI_OK) 
		{
		    strncpy(g_struMailAgentConf.szEmailSendTo, szSMTPInfo.szEmailSendTo, sizeof(g_struMailAgentConf.szEmailSendTo));
		    strncpy(g_struMailAgentConf.szEmailFrom, szSMTPInfo.szEmailFrom, sizeof(g_struMailAgentConf.szEmailFrom));
		    strncpy(g_struMailAgentConf.szEmailSeverIP, szSMTPInfo.szEmailSeverIP, sizeof(g_struMailAgentConf.szEmailSeverIP));
		    strncpy(g_struMailAgentConf.szEmailAccount, szSMTPInfo.szEmailAccount, sizeof(g_struMailAgentConf.szEmailAccount));
		    strncpy(g_struMailAgentConf.szEmailPasswd, szSMTPInfo.szEmailPasswd, sizeof(g_struMailAgentConf.szEmailPasswd));
		    g_struMailAgentConf.iEmailServerPort = szSMTPInfo.iEmailServerPort;
		    g_struMailAgentConf.iEmailAuthen = szSMTPInfo.iEmailAuthen;
		    g_struMailAgentConf.iAlarmLevel = szSMTPInfo.iAlarmLevel;
		}
		else
		{
		    g_struMailAgentConf.iAlarmLevel = ALARM_LEVEL_NONE;
		}

		iRet = FALSE;  
		
	
		//2.2 Send Alarms    
		iRet = EMAIL_Trip(); //Send alarms
		
		//2.3  Send other information  
		iRet |= EMAIL_Information(); //Send other information

		// 2.3 have nothing to send, wait 1 second 
		if(iRet == FALSE)
		{			
		    Sleep(1000);//yield
		}
		
	    }
	    else
	    {
		Sleep(2000);//yield
	    }
		if(MAIL_AppService != NULL)
		{
			MAIL_AppService->bReadyforNextService = TRUE;
		}
	    //To tell the thread manager I'm living.
	    RunThread_Heartbeat(g_sEmailManage.hThreadSelf);	
	}

	ExitEmail();

	return 0;
}
/*=====================================================================*
* Function name: DriverInit
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
//extern "C" 
BOOL DriverInit(LPVOID lpReserve)
{
	UNUSED(lpReserve);
#if MAIL_SERVICE_DEBUG_SWITCH	
	printf("mail.so, call DriverInit\n");
	printf("[%s %s LINE %d] arguments:\n", __FILE__, __FUNCTION__, __LINE__);

#endif
	memset(&g_struMailAgentConf, 0, sizeof(g_struMailAgentConf));
	tagStruMailAgentConf *p = (tagStruMailAgentConf *)lpReserve;
	memcpy(&g_struMailAgentConf, p, sizeof(g_struMailAgentConf));

/*
#ifdef MAIL_SERVICE_DEBUG_SWITCH
	printf("%s\n", p->szEmailSendTo);
	printf("%s\n", p->szEmailFrom);
	printf("%s\n", p->szEmailSeverIP);
	printf("%d\n", p->iEmailServerPort);
	printf("%s\n", p->szEmailAccount);
	printf("%s\n", p->szEmailPasswd);
	printf("%d\n", p->iEmailAuthen);
	printf("%d\n", p->iSMSSendType);
	printf("%s\n", p->szSMSSMTPServerIP);
	printf("%d\n", p->iSMSSMTPServerPort);
	printf("%s\n", p->szSMSSMTPFrom);
	printf("%s\n", p->szSMSSMTPUser);
	printf("%s\n", p->szSMSSMTPPasswd);
	printf("%s\n", p->szSMSSMTPRecipientFormat);
	printf("%s\n", p->szGPRSSetting);
	printf("%d\n", p->iAlarmLevel);
#endif	
*/	
	if (g_struMailAgentConf.iEmailServerPort == 0) 
	{
		g_struMailAgentConf.iEmailServerPort = 25;
	}

	if (strlen(g_struMailAgentConf.szSMSSMTPRecipientFormat) == 0) {
		strcpy(g_struMailAgentConf.szSMSSMTPRecipientFormat, "%s@139.com");
	}

#if MAIL_SERVICE_DEBUG_SWITCH
	printf("\n\n[%s %s LINE %d]\n", __FILE__, __FUNCTION__, __LINE__);
	printf("%s\n", g_struMailAgentConf.szEmailSendTo);
	printf("%s\n", g_struMailAgentConf.szEmailFrom);
	printf("%s\n", g_struMailAgentConf.szEmailSeverIP);
	printf("%d\n", g_struMailAgentConf.iEmailServerPort);
	printf("%s\n", g_struMailAgentConf.szEmailAccount);
	printf("%s\n", g_struMailAgentConf.szEmailPasswd);
	printf("%d\n", g_struMailAgentConf.iEmailAuthen);
	printf("%d\n", g_struMailAgentConf.iSMSSendType);
	printf("%s\n", g_struMailAgentConf.szSMSSMTPServerIP);
	printf("%d\n", g_struMailAgentConf.iSMSSMTPServerPort);
	printf("%s\n", g_struMailAgentConf.szSMSSMTPFrom);
	printf("%s\n", g_struMailAgentConf.szSMSSMTPUser);
	printf("%s\n", g_struMailAgentConf.szSMSSMTPPasswd);
	printf("%s\n", g_struMailAgentConf.szSMSSMTPRecipientFormat);
	printf("%s\n", g_struMailAgentConf.szGPRSSetting);
	printf("%d\n", g_struMailAgentConf.iAlarmLevel);
#endif

	return TRUE;
}
/*=====================================================================*
* Function name: DriverSend
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
//extern "C" 
BOOL DriverSend(char* pInformMsg, char* pInformAddr)
{
#if MAIL_SERVICE_DEBUG_SWITCH
	printf("email_agent.so, call DriverSend\n");
	printf("pInformMsg[%s] pInformAddr[%s]\n", pInformMsg, pInformAddr);
#endif
	queue_elem elem;

	elem.pInformMsg = (char *)malloc(strlen(pInformMsg)+1);

	if (elem.pInformMsg == NULL) 
	{
		return FALSE;
	}
	memcpy(elem.pInformMsg, pInformMsg, strlen(pInformMsg));
	elem.pInformMsg[strlen(pInformMsg)] = '\0';

	elem.pInformAddr = (char *)malloc(strlen(pInformAddr)+1);
	
	if (elem.pInformAddr == NULL) 
	{
		free(elem.pInformMsg);
		return FALSE;
	}
	memcpy(elem.pInformAddr, pInformAddr, strlen(pInformAddr));
	elem.pInformAddr[strlen(pInformAddr)] = '\0';

	if (Queue_Put(g_sEmailManage.hOtherInforSendQueue, &elem, FALSE) == ERR_QUEUE_OK) 
	{
		return TRUE;
	} 
	else 
	{
		return FALSE;
	}
}
/*=====================================================================*
* Function name: send
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static BOOL send(queue_elem *elem)
{
	BOOL bRet = FALSE;
	char *pInformMsg = elem->pInformMsg;
	char *pInformAddr = elem->pInformAddr;
	char *pInformHeader = elem->pHeader;

	if (pInformMsg != NULL) {
#if MAIL_SERVICE_DEBUG_SWITCH		
		printf("pInformMsg = [%s]\n", pInformMsg);
#endif
	}

	if (pInformAddr != NULL) {
#if MAIL_SERVICE_DEBUG_SWITCH
		printf("pInformAdd = [%s]\n", pInformAddr);
#endif
	}

	//LOCK_EMAIL_SERVICE();
	
	//1.
	if (isEmail(pInformAddr)) 
	{
		int nMsgType = OUTPUT_EMAIL;
		stMailSevCfg oMailSevCfg;
		memset(&oMailSevCfg, 0, sizeof(oMailSevCfg));
		strcpy(oMailSevCfg.m_szEmailServer, g_struMailAgentConf.szEmailSeverIP);
		sprintf(oMailSevCfg.m_szEmailPort, "%d", g_struMailAgentConf.iEmailServerPort);
		strcpy(oMailSevCfg.m_szEmailUser, g_struMailAgentConf.szEmailAccount);
		strcpy(oMailSevCfg.m_szEmailPassword, g_struMailAgentConf.szEmailPasswd);
		strcpy(oMailSevCfg.m_szEmailSendFrom, g_struMailAgentConf.szEmailFrom);
		oMailSevCfg.iNeedAuthen = g_struMailAgentConf.iEmailAuthen;

		HANDLE hComm = initComm(nMsgType, &oMailSevCfg);

		MailHeader oMailHeader;
		memset(&oMailHeader, 0, sizeof(oMailHeader));
		// consider there are append files to be sent 
		oMailHeader.iAppendFileNum = 0;
		char *p = pInformMsg;
		int i = 0;
		for (i = 0; i < MAX_APPEND_FILE_NUM; ++ i) 
		{
			p = strchr(p, '|');
			if (p == NULL) {
				break;
			}

			p ++;
			char *q = p;
			q = strchr(p, '|');
			if (q == NULL) 
			{
				strncpy(oMailHeader.szAppendFileName[i], p, sizeof(oMailHeader.szAppendFileName[i]));
				i ++;
				break;
			}
			memcpy(oMailHeader.szAppendFileName[i], p, q - p);
			i ++;
		}
		oMailHeader.iAppendFileNum = i;

		p = strchr(pInformMsg, '|');
		if (p == NULL) 
		{
			// no append file to be sent 
			strncpy(oMailHeader.contents, pInformMsg, sizeof(oMailHeader.contents));
		} 
		else 
		{
			//there are append file information in pInformMsg, need to be omitted 
			memcpy(oMailHeader.contents, pInformMsg, p - pInformMsg);
		}

		//strncpy(oMailHeader.subject, "Warning!", sizeof(oMailHeader.subject));
		strncpy(oMailHeader.subject, pInformHeader, sizeof(oMailHeader.subject));		
		strncpy(oMailHeader.sender, g_struMailAgentConf.szEmailFrom, sizeof(oMailHeader.sender));
		strncpy(oMailHeader.recipient, pInformAddr, sizeof(oMailHeader.recipient));
		strncpy(oMailHeader.specialHeaders, "X-header: Boing\r\n", sizeof(oMailHeader.specialHeaders));
		strncpy(oMailHeader.contentType, "text/plain", sizeof(oMailHeader.contentType));

		bRet = sendMail(&oMailHeader, &oMailSevCfg, hComm);
		uninitComm(hComm);
	} 
	//2.
	else if (isDigitalStr(pInformAddr)) 
	{
		int nMsgType;
		HANDLE hComm = NULL;

		if (g_struMailAgentConf.iSMSSendType == SMS_COMM_TYPE_GPRS) 
		{
			nMsgType = OUTPUT_SMSGPRS;
		} else 
		{
			nMsgType = OUTPUT_SMSEMAIL;
		}

		if (nMsgType == OUTPUT_SMSEMAIL)
		{
			stMailSevCfg oMailSevCfg;
			memset(&oMailSevCfg, 0, sizeof(oMailSevCfg));

			strcpy(oMailSevCfg.m_szEmailServer, g_struMailAgentConf.szSMSSMTPServerIP);
			sprintf(oMailSevCfg.m_szEmailPort, "%d", g_struMailAgentConf.iSMSSMTPServerPort);
			strcpy(oMailSevCfg.m_szEmailUser, g_struMailAgentConf.szSMSSMTPUser);
			strcpy(oMailSevCfg.m_szEmailPassword, g_struMailAgentConf.szSMSSMTPPasswd);
			strcpy(oMailSevCfg.m_szEmailSendFrom, g_struMailAgentConf.szSMSSMTPFrom);
			oMailSevCfg.iNeedAuthen = g_struMailAgentConf.iSMSSMTPAuthen;

			hComm = initComm(nMsgType, &oMailSevCfg);

			MailHeader oMailHeader;
			memset(&oMailHeader, 0, sizeof(oMailHeader));
			strncpy(oMailHeader.contents, pInformMsg, sizeof(oMailHeader.contents));
			//strncpy(oMailHeader.subject, "Warning!", sizeof(oMailHeader.subject));
			strncpy(oMailHeader.subject, pInformHeader, sizeof(oMailHeader.subject));	
			strncpy(oMailHeader.sender, g_struMailAgentConf.szEmailFrom, sizeof(oMailHeader.sender));
			char szLine[128];
			memset(szLine, 0, sizeof(szLine));
			sprintf(szLine, g_struMailAgentConf.szSMSSMTPRecipientFormat, pInformAddr);
			strncpy(oMailHeader.recipient, szLine, sizeof(oMailHeader.recipient));
			strncpy(oMailHeader.specialHeaders, "X-header: Boing\r\n", sizeof(oMailHeader.specialHeaders));
			strncpy(oMailHeader.contentType, "text/plain", sizeof(oMailHeader.contentType));

			bRet = sendMail(&oMailHeader, &oMailSevCfg, hComm);
			uninitComm(hComm);
		} 
		else 
		{
			if (strlen(g_struMailAgentConf.szGPRSSetting) == 0) {
				hComm = initComm(nMsgType, NULL); // use default setting "460800,n,8,1" 
			} else {
				hComm = initComm(nMsgType, g_struMailAgentConf.szGPRSSetting);
			}

			SmsHeader oSmsHeader;
			strncpy(oSmsHeader.contents, pInformMsg, sizeof(oSmsHeader.contents));
			//strncpy(oSmsHeader.subject, "Waring!", sizeof(oSmsHeader.subject));
			strncpy(oSmsHeader.subject, pInformHeader, sizeof(oSmsHeader.subject));	
			strncpy(oSmsHeader.recipient, pInformAddr, sizeof(oSmsHeader.recipient));

			static BOOL iFirst = TRUE;
			if (iFirst == TRUE) 
			{
				bRet = sendSms(&oSmsHeader, hComm, TRUE);
				if (bRet == TRUE) 
				{
					iFirst = FALSE;
				}
			} 
			else 
			{
				bRet = sendSms(&oSmsHeader, hComm, FALSE);
			}

			uninitComm(hComm);
		}
		
	} 
	else 
	{
		bRet =  FALSE;
	}

	//UNLOCK_EMAIL_SERVICE();

	return bRet;
}

/*=====================================================================*
* Function name: Email_LoadConfigFile
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
//#define EMAIL_CONFIG_FILE   "/home/app_script/email.log"
#define EMAIL_CONFIG_FILE     "private/email/mail.cfg"

static int Email_LoadConfigFile(void)
{
    void    *pCfg;
    char    *szFileContents;
    FILE    *pFile;
    long    ulFileLen;
    char    szCfgFileName[MAX_FILE_PATH]; 

    tagStruMailAgentConf struMailConfTemp = {0};

    Cfg_GetFullConfigPath(EMAIL_CONFIG_FILE, 
	szCfgFileName, MAX_FILE_PATH);

    /* 1. read the file content to the buffer */
    pFile = fopen(szCfgFileName, "r");
    if (pFile == NULL)
    {
	/*AppLogOut("CFG_READER", APP_LOG_ERROR, "Can not open the file: %s!\n",
	    EMAIL_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: Can not open the file: %s!\n", __FILE__,
	    __FUNCTION__, EMAIL_CONFIG_FILE);*/
	return ERR_CFG_FILE_OPEN;
    }

    fseek(pFile, 0, SEEK_END);
    ulFileLen = ftell(pFile);
    if (ulFileLen == -1)
    {
	/*AppLogOut("CFG_READER", APP_LOG_ERROR, "Get Config file(%s) length error!\n",
	    EMAIL_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: Get Config file(%s) length error!\n",
	    __FILE__, __FUNCTION__, EMAIL_CONFIG_FILE);	*/

	fclose(pFile);
	return ERR_CFG_FAIL;
    }
    fseek(pFile, 0, SEEK_SET);

    szFileContents = NEW(char, ulFileLen + 1);
    if (szFileContents == NULL)
    {
	/*AppLogOut("CFG_READER", APP_LOG_ERROR, "out of memory on loading "
	    "file: %s!\n", EMAIL_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: out of memory on loading file: %s!\n",
	    __FILE__, __FUNCTION__, EMAIL_CONFIG_FILE);*/

	fclose(pFile);
	return ERR_CFG_NO_MEMORY;
    }

    ulFileLen = fread(szFileContents, sizeof(char), (size_t)ulFileLen, pFile);
    fclose(pFile);

    if (ulFileLen < 0) 
    {
	/*AppLogOut("CFG_READER", APP_LOG_ERROR, "Read config file (%s) fail!\n",
	    EMAIL_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: Read config file (%s) fail!\n",
	    __FILE__, __FUNCTION__, EMAIL_CONFIG_FILE);*/

	DELETE(szFileContents);
	return ERR_CFG_FILE_READ;
    }
    szFileContents[ulFileLen] = 0;  // end with NULL

    /* 2. get SProfile structure */
    pCfg = Cfg_ProfileOpen( szFileContents, ulFileLen);
    if( pCfg == NULL )
    {
	/*AppLogOut("CFG_READER", APP_LOG_ERROR, "Open config file: %s out"
	    "of memery.\n", EMAIL_CONFIG_FILE);
	TRACE("[%s]--%s: ERROR: Open config file: %s out of memery.\n",
	    __FILE__, __FUNCTION__, EMAIL_CONFIG_FILE);*/

	DELETE(szFileContents);
	return ERR_CFG_FAIL;
    }

    /* 3. load config file */
    char szLineData[MAX_LINE_SIZE];
    int  ret;

    //3.0   [EMAIL_SEND_TO]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SEND_TO]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
    }
    else
    {
	sprintf(struMailConfTemp.szEmailSendTo, "%s", szLineData);
    }	

    //3.1   [EMAIL_FROM]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_FROM]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szEmailFrom, "%s", szLineData);
    }


    //3.2   [EMAIL_SERVER_IP]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SERVER_IP]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szEmailSeverIP, "%s", szLineData);
    }

    //3.3   [EMAIL_SERVER_PORT]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SERVER_PORT]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR: EmailServerPort is "
	    "not a number!\n", __FILE__);*/

	//return ERR_CFG_BADCONFIG;    
    }
    else
    {
	struMailConfTemp.iEmailServerPort = atoi(szLineData);
    }

    //3.4   [EMAIL_ACCOUNT]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_ACCOUNT]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else 
    {
	sprintf(struMailConfTemp.szEmailAccount, "%s", szLineData);
    }

    //3.5   [EMAIL_PASSWORD]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_PASSWORD]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szEmailPasswd, "%s", szLineData);
    }

    //3.6   [EMAIL_AUTHEN]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_AUTHEN]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR: EMAIL_AUTHEN is "
	    "not a number!\n", __FILE__);*/

	//return ERR_CFG_BADCONFIG;    
    }
    else 
    {
	struMailConfTemp.iEmailAuthen = atoi(szLineData);
    }


    //3.7   [EMAIL_SMS_SEND_TYPE]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SMS_SEND_TYPE]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR: EMAIL_SMS_SEND_TYPE is "
	    "not a number!\n", __FILE__);*/

	//return ERR_CFG_BADCONFIG;    
    }
    else
    {
	struMailConfTemp.iSMSSendType = atoi(szLineData);
    }

    //3.8   [EMAIL_SMS_SMTP_AUTHEN]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SMS_SMTP_AUTHEN]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR: EMAIL_SMS_SMTP_AUTHEN is "
	    "not a number!\n", __FILE__);*/

	//return ERR_CFG_BADCONFIG;    
    }
    else
    {
	struMailConfTemp.iSMSSMTPAuthen = atoi(szLineData);
    }

    //3.9   [EMAIL_SMS_SMTP_SERVER_IP]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SMS_SMTP_SERVER_IP]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szSMSSMTPServerIP, "%s", szLineData);
    }

    //3.10   [EMAIL_SMS_SMTP_SERVER_PORT]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SMS_SMTP_SERVER_PORT]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR: EMAIL_SMS_SMTP_SERVER_PORT is "
	    "not a number!\n", __FILE__);*/

	//return ERR_CFG_BADCONFIG;    
    }
    else
    {
	struMailConfTemp.iSMSSMTPServerPort = atoi(szLineData);
    }

    //3.11   [EMAIL_SMS_SMTP_FROM]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SMS_SMTP_FROM]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szSMSSMTPFrom, "%s", szLineData);
    }

    //3.12   [EMAIL_SMS_SMTP_USER]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SMS_SMTP_USER]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szSMSSMTPUser, "%s", szLineData);
    }

    //3.13   [EMAIL_SMS_SMTP_PASSWORD]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SMS_SMTP_PASSWORD]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szSMSSMTPPasswd, "%s", szLineData);
    }

    //3.14   [EMAIL_SMS_SMTP_RECIPIENT_FORMAT]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_SMS_SMTP_RECIPIENT_FORMAT]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szSMSSMTPRecipientFormat, "%s", szLineData);
    }

    //3.15   [EMAIL_GPRS_SETTING]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[EMAIL_GPRS_SETTING]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else
    {
	sprintf(struMailConfTemp.szGPRSSetting, "%s", szLineData);
    }

    //3.16   [ALARM_LEVEL]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[ALARM_LEVEL]", szLineData, MAX_LINE_SIZE);
    if (ret == -1)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION not found in the Cfg file!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if (ret == 0)
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR"
	    "SECTION should not be empty!\n", __FILE__);*/
	//return ERR_CFG_BADCONFIG;
    }
    else if ((szLineData[0] < '0') || (szLineData[0] > '9'))
    {
	/*AppLogOut("EMAIL LDR", APP_LOG_ERROR, 
	    "[%s]--LoadEmailConfigProc: ERROR: ALARM_LEVEL is "
	    "not a number!\n", __FILE__);*/

	//return ERR_CFG_BADCONFIG;    
    }
    else 
    {
	struMailConfTemp.iAlarmLevel = atoi(szLineData);
    }

    //3.17   [ALARM_DESCRIPTION_OA]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[ALARM_DESCRIPTION_OA]", szLineData, MAX_LINE_SIZE);
    if (ret == -1 || ret == 0)
    {
	sprintf(struMailConfTemp.szDesOA, "%s", "OA");
    }
    else
    {
	sprintf(struMailConfTemp.szDesOA, "%s", szLineData);
    }

    //3.18   [ALARM_DESCRIPTION_MA]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[ALARM_DESCRIPTION_MA]", szLineData, MAX_LINE_SIZE);
    if (ret == -1 || ret == 0)
    {
	sprintf(struMailConfTemp.szDesMA, "%s", "MA");
    }
    else
    {
	sprintf(struMailConfTemp.szDesMA, "%s", szLineData);
    }
	
    //3.19   [ALARM_DESCRIPTION_CA]
    memset(szLineData, 0, sizeof(char) * MAX_LINE_SIZE);
    ret = Cfg_ProfileGetLine(pCfg, "[ALARM_DESCRIPTION_CA]", szLineData, MAX_LINE_SIZE);
    if (ret == -1 || ret == 0)
    {
	sprintf(struMailConfTemp.szDesCA, "%s", "CA");
    }
    else
    {
	sprintf(struMailConfTemp.szDesCA, "%s", szLineData);
    }
   

    if (pCfg != NULL)
    {
	Cfg_ProfileClose(pCfg);  //free the memery
    }    

    if(DriverInit(&struMailConfTemp) == FALSE)
    {
	return ERR_CFG_BADCONFIG;
    }

    return ERR_CFG_OK;
}

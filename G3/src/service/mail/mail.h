/*-----------------------------------------------------------------------------
 * 	Copyright(c) 2020, Vertiv Tech Co., Ltd.
 * 					ALL RIGHTS RESERVED
 *
 * 	PRODUCT	: RDU(Rack Data Unit)
 * 	FILENAME: mail_agent.h
 * 	CREATOR : 
 * 	DATE	: 
 * 	PURPOSE	: 
 *
 * 	HISTORY	:
 *----------------------------------------------------------------------------*/

#ifndef _MAIL_AGENT_H__
#define _MAIL_AGENT_H__

#include	"basetypes.h"

#include	"stdlib.h"
#include	"stdio.h"
#include	"stdsys.h"
#include	"new.h"
#include	"public.h"

//Used to record log
#ifndef EMAIL_TASK
#define EMAIL_TASK		"EMAIL"
#endif //EMAIL_TASK

//�澯�������
#define OUTPUT_EMAIL		1		//Email
#define OUTPUT_SMSEMAIL		2		//SMSEMail
#define OUTPUT_SMSGPRS		3		//SMS

#define SMS_COMM_TYPE_SMTP	0		//SMS �ʼ�ͨ�ŷ�ʽ
#define SMS_COMM_TYPE_GPRS	1		//SMS Edgeͨ�ŷ�ʽ

#define MAX_EMAIL_NOTIFY_NUM	20		//���EMail֪ͨ����
#define MAX_SMS_NOTIFY_NUM		20		//���SMS֪ͨ����
#define MAX_MAIL_BUF_SIZE		1024	//���EMAIL�����ֽ���
#define MAX_SMS_BUF_SIZE		1024	//���SMS�����ֽ���
#define PAIR_MIN_SIZE			8		//email��sms�ַ�����С�ֽ���
#define MAIL_MAX_LINE_SIZE		1256	//���email�Ự���ֽ���
#define LOGIN_MAX_BASE64_SIZE	44		//����½�û��������base64�ֽ���
#define MAX_SUBJECT_SIZE		200		//��������ֽ���
#define MAX_SNDR_SIZE			80		//������߱�ʶ�ֽ���
#define MAX_RCPT_SIZE			80		//�������߱�ʶ�ֽ���
#define MAX_CONTENT_SIZE		300		//��������ֽ���
#define MAX_CONTENT_TYPE_SIZE	80		//��������ֽ���
#define MAX_SPEC_CONTENT_SIZE	32		//������������ֽ���
#define MAX_GPRS_COMM_PARAM_SIZE	32	//���GPRSͨ�Ų���

#define SMTP_COMM_LIB  "/app/haldriver/comm_net_tcpip.so"	//SMTP ͨ�ſⶨ��
#define SMTP_COMM_DEV  "/dev/eth0"			//SMTP ͨ���豸�ڵ�
#define EDGE_COMM_LIB  "/home/idu/SO/comm_std_usb.so"	//USB ͨ�ſⶨ��
#define EDGE_COMM_DEV  "/dev/ttyUSB0"		//USB  ͨ���豸�ڵ�

#define SMTP_DEFAULT_PORT		25				// SMTP������Ĭ�϶˿�

#define EMAIL_SERVER_ADDR_LEN	22				//  "142.100.16.45:25" mail server ip & port
#define EMAIL_SERVER_PORT_LEN	5				//  "25" mail server port
#define EMAIL_USER_LEN			32				//  "TOM"         user identifier
#define EMAIL_PSWD_LEN			32				//  "security"    password
#define EMAIL_SENDER_LEN		MAX_SNDR_SIZE	//  "notifier@vertiv.com" sender mail address
#define SMS_COMM_TYPE_LEN		3				//  "security"    password
#define MAX_APPEND_FILE_NUM		10

//�ʼ�����������
struct _MailSevCfg
{
	char m_szEmailServer[EMAIL_SERVER_ADDR_LEN+1];	//Email server ip & port
	char m_szEmailPort[EMAIL_SERVER_PORT_LEN+1];	//Email server port
	char m_szEmailUser[EMAIL_USER_LEN+1];			//Email user identifier
	char m_szEmailPassword[EMAIL_PSWD_LEN+1];		//Email password
	char m_szEmailSendFrom[EMAIL_SENDER_LEN+1];		//Email sender identifier
	int iNeedAuthen;								//��Ҫ�����û��������������֤, 1 --> ��Ҫ
};
typedef struct _MailSevCfg stMailSevCfg;

//�ʼ�ͷ�ṹ
struct _MailHeader
{
	char contents[MAX_CONTENT_SIZE+1];
	char subject[MAX_SUBJECT_SIZE+1];
	char sender[MAX_SNDR_SIZE+1];
	char recipient[MAX_RCPT_SIZE+1];
	char specialHeaders[MAX_SPEC_CONTENT_SIZE+1];
	char contentType[MAX_CONTENT_TYPE_SIZE+1];
	char szAppendFileName[MAX_APPEND_FILE_NUM][128];
	int iAppendFileNum;
};
typedef struct _MailHeader MailHeader;

//����Ϣ�ṹ
struct _SmsHeader
{
	char contents[MAX_CONTENT_SIZE+1];
	char subject[MAX_SUBJECT_SIZE+1];
	char recipient[MAX_RCPT_SIZE+1];
};
typedef struct _SmsHeader SmsHeader ;

//���ýӿڽṹ
struct _struMailAgentConf {
	char szEmailSendTo[128];    
	char szEmailFrom[128];
	char szEmailSeverIP[32];
	int iEmailServerPort;
	char szEmailAccount[64];
	char szEmailPasswd[64];
	int iEmailAuthen; /* 0 --> SMTP����������Ҫ��֤�� 1 --> SMTP��������Ҫ��֤�û������� */
	int iAlarmLevel; /* 0 --> ����Ҫ���ʼ��� 1/2/3  --> �ⷢ�ʼ��ĸ澯���� */
	char szDesOA[32];
	char szDesMA[32];
	char szDesCA[32];

	//20130708 lkf ��������ΪԤ�����ݣ�Ŀǰ��֧��
	int iSMSSendType; /* 0 -->SMS �ʼ�SMTP֪ͨ��ʽ(����̫��), 1 --> SMS GPRSͨ�ŷ�ʽ */
	int iSMSSMTPAuthen; /* 0 --> SMTP����������Ҫ��֤�� 1 --> SMTP��������Ҫ��֤�û������� */
	char szSMSSMTPServerIP[32];
	int iSMSSMTPServerPort;
	char szSMSSMTPFrom[64];
	char szSMSSMTPUser[64];
	char szSMSSMTPPasswd[64];
	char szSMSSMTPRecipientFormat[64]; /* "13477889090@139.com", "%s@139.com" */
	char szGPRSSetting[32]; /* default is "460800,n,8,1" */

};
typedef struct _struMailAgentConf tagStruMailAgentConf ;

/* SO�����ṹ���� */

//extern "C" 
BOOL DriverSend(char* pInformMsg, char* pInformAddr);

#endif

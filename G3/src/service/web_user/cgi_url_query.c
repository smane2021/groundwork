/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NCU for ESNA
*
*  FILENAME : cgi_url_query.c
*  CREATOR  : Zhao Zicheng              DATE: 2015-09-25 16:47
*  VERSION  : V1.00
*  PURPOSE  : To deal with CR HTTP DATA INTERFACE
*
*  HISTORY  :
*
*==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

typedef struct tagInterfaceParamInfo
{
    int			iControl;           /*it is control command*/
    char		szFromTime[32];
    char		szToTime[32];
    char		szUkey1[17];
    char		szUkey2[17];
    char		szUkey3[17];
}InterfaceParamInfo;

struct tm	p_tm;

char *strptime(const char *s, const char *format, struct tm *tm);

static int checkTime(struct tm *p_tm)
{
    if((p_tm->tm_year > 200) || (p_tm->tm_year < 100))//year
    {
	return 0;
    }
    if((p_tm->tm_mon > 11) || (p_tm->tm_mon < 0))//month
    {
	return 0;
    }
    if((p_tm->tm_mday > 31) || (p_tm->tm_mday < 1))//day
    {
	return 0;
    }
    if((p_tm->tm_hour > 23) || (p_tm->tm_hour < 0))//hour
    {
	return 0;
    }
    if((p_tm->tm_min > 59) || (p_tm->tm_min < 0))//minute
    {
	return 0;
    }
    if((p_tm->tm_sec > 59) || (p_tm->tm_sec < 0))//second
    {
	return 0;
    }
    return 1;
}

/*==========================================================================*
* FUNCTION :  Web_GET_InterfaceParam
* PURPOSE  :  Get interface param from CGI
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  OUT InterfaceParamInfo *pstInterfaceParam:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Zhao Zicheng               DATE: 2015-09-21 11:20
*==========================================================================*/
static int Web_GET_InterfaceParam(OUT InterfaceParamInfo *pstInterfaceParam)
{
#define START_TIME	"stime"
#define END_TIME	"etime"
#define UKEY1		"ukey1"
#define UKEY2		"ukey2"
#define UKEY3		"ukey3"
#define START_TIME1	"starttime"
#define END_TIME1	"endtime"

    int		form_method; /* POST = 1, GET = 0 */
    char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    char	*val;
    time_t	tmTime;

    form_method = getRequestMethod();
    if(form_method == POST) 
    {
	getvars = getGETvars();
	postvars = getPOSTvars();
	if(postvars == NULL )
	{
	    return FALSE;
	}
    }
    else if(form_method == GET) 
    {
	getvars = getGETvars();
    }
    else 
    {
	return FALSE;
    }

    pstInterfaceParam->iControl = HTTP_INTERFACE;

    /*FILE *pf102=NULL;
    pf102=fopen("/var/file102.txt","a+");*/

    if((val = getValue( getvars, postvars, START_TIME )) != NULL)
    {
	strncpyz(pstInterfaceParam->szFromTime, val, sizeof(pstInterfaceParam->szFromTime));
    }
    else if((val = getValue( getvars, postvars, START_TIME1 )) != NULL)
    {
	strptime(val, "%Y-%m-%d %H:%M:%S", &p_tm);
	if(checkTime(&p_tm) == 0)
	{
	    snprintf(pstInterfaceParam->szFromTime, sizeof(pstInterfaceParam->szFromTime), "%ld", -1);
	}
	else
	{
	    tmTime = mktime(&p_tm);
	    snprintf(pstInterfaceParam->szFromTime, sizeof(pstInterfaceParam->szFromTime), "%ld", tmTime);
	}


	/*FILE *pf101=NULL;
	pf101=fopen("/var/file101.txt","a+");
	fwrite(val,strlen(val),1,pf101);
	char szbuf[100];
	snprintf(szbuf, sizeof(szbuf), " %d %d %d %d %d %d ", p_tm.tm_year, p_tm.tm_mon, p_tm.tm_mday, p_tm.tm_hour, p_tm.tm_min, p_tm.tm_sec);
	fwrite(szbuf,strlen(szbuf),1,pf101);
	fwrite(pstInterfaceParam->szFromTime,strlen(pstInterfaceParam->szFromTime),1,pf101);
	fclose(pf101);*/
    }
    else
    {
	snprintf(pstInterfaceParam->szFromTime, sizeof(pstInterfaceParam->szFromTime), "%ld", 0);
    }

    if((val = getValue( getvars, postvars, END_TIME )) != NULL)
    {
	strncpyz(pstInterfaceParam->szToTime, val, sizeof(pstInterfaceParam->szToTime));
    }
    else if((val = getValue( getvars, postvars, END_TIME1 )) != NULL)
    {
	strptime(val, "%Y-%m-%d %H:%M:%S", &p_tm);
	if(checkTime(&p_tm) == 0)
	{
	    snprintf(pstInterfaceParam->szToTime, sizeof(pstInterfaceParam->szToTime), "%ld", -1);
	}
	else
	{
	    tmTime = mktime(&p_tm);
	    snprintf(pstInterfaceParam->szToTime, sizeof(pstInterfaceParam->szToTime), "%ld", tmTime);
	}
    }
    else
    {
	snprintf(pstInterfaceParam->szToTime, sizeof(pstInterfaceParam->szToTime), "%ld", 0);
    }

    if((val = getValue( getvars, postvars, UKEY1 )) != NULL)
    {
	strncpyz(pstInterfaceParam->szUkey1, val, sizeof(pstInterfaceParam->szUkey1));
    }
    else
    {
	memset(pstInterfaceParam->szUkey1, 0, sizeof(pstInterfaceParam->szUkey1));
    }

    if((val = getValue( getvars, postvars, UKEY2 )) != NULL)
    {
	strncpyz(pstInterfaceParam->szUkey2, val, sizeof(pstInterfaceParam->szUkey2));
    }
    else
    {
	memset(pstInterfaceParam->szUkey2, 0, sizeof(pstInterfaceParam->szUkey2));
    }

    if((val = getValue( getvars, postvars, UKEY3 )) != NULL)
    {
	strncpyz(pstInterfaceParam->szUkey3, val, sizeof(pstInterfaceParam->szUkey3));
    }
    else
    {
	memset(pstInterfaceParam->szUkey3, 0, sizeof(pstInterfaceParam->szUkey3));
    }

    cleanUp(getvars, postvars);
    return TRUE;
}

static int Web_GetInterfaceData(IN InterfaceParamInfo *pstInterfaceParam, OUT char **ppBuf)
{
#define CGI_QUERY_BUFFER_LEN			4

    int	fd,fd2;
    char	buf[256], buf2[PIPE_BUF], fifoname[FIFO_NAME_LEN];
    mode_t	mode = 0666;
    int		iLen;
    char	*pBuf;
    int		iBufCount = 0;

    if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
    {
	return FALSE;
    }
    sprintf(fifoname, "%s/fifo.%ld", CGI_CLIENT_FIFO_PATH, (long)getpid());
    if((mkfifo(fifoname, mode)) < 0)
    {
	return FALSE;
    }
    iLen = sprintf(buf, "%10ld%2d%32s%32s%16s%16s%16s", (long)getpid(),
	pstInterfaceParam->iControl,
	pstInterfaceParam->szFromTime,
	pstInterfaceParam->szToTime,
	pstInterfaceParam->szUkey1,
	pstInterfaceParam->szUkey2,
	pstInterfaceParam->szUkey3);

    if((write(fd, buf, (unsigned int)(iLen + 1))) < 0)
    {
	close(fd);
	return FALSE;
    }

    if((fd2 = open(fifoname, O_RDONLY))<0)
    {
	AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Fail to open FIFO fail [%s]", MAIN_FIFO_NAME);
	return FALSE;
    }
    pBuf = NEW(char, CGI_QUERY_BUFFER_LEN * PIPE_BUF);

    while((iLen = read(fd2, buf2, PIPE_BUF - 1)) > 0)
    {
	if(iBufCount >= CGI_QUERY_BUFFER_LEN)
	{
	    pBuf = RENEW(char, pBuf, (iBufCount + 1) * PIPE_BUF);
	}
	strcat(pBuf, buf2);
	iBufCount++;
    }

    *ppBuf = pBuf;

    close(fd2);
    close(fd);
    unlink(fifoname);

    return TRUE;
}

int main(void)
{
    InterfaceParamInfo	stInterfaceParam;
    char	*pQueryReturnData = NULL;

    printf("Content-type:text/html\n\n");
    printf("\n\n");

    SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
	(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
	(void *)"clear param");

    if(Web_GET_InterfaceParam(&stInterfaceParam) == FALSE)
    {
	return FALSE;
    }

    if(Web_GetInterfaceData(&stInterfaceParam, &pQueryReturnData) == FALSE)
    {
	return FALSE;
    }

    PostPage(pQueryReturnData);
    DELETE(pQueryReturnData);
    pQueryReturnData = NULL;

    return TRUE;
}


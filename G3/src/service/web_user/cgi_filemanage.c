/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : cgi_filemanage.c
*  CREATOR  : Yang Guoxin              DATE: 2004-12-07 14:47
*  VERSION  : V1.00
*  PURPOSE  :
*
*  HISTORY  :
*
*==========================================================================*/
#include "stdsys.h"  
#include "public.h"
#include "pubfunc.h"
#include <sys/dir.h> 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "cgi_pub.h"
#include "cgivars.h"

#define WDOG_MAJOR			241 
#define IOCTL_EN_WD	     _IOW(WDOG_MAJOR, 1, unsigned long)
#define IOCTL_DIS_WD      _IOW(WDOG_MAJOR, 2, unsigned long)
#define IOCTL_FEED_WD    _IOW(WDOG_MAJOR, 3, unsigned long)
#define IOCTL_SET_TIMER    _IOW(WDOG_MAJOR, 4, unsigned long)

/*for upload file*/
#define CGI_CRLF					"\r\n"
#define HTTP_GET					"GET"
#define HTTP_POST					"POST"
#define X_CONTENT_TYPE				"application/x-www-form-urlencoded"
#define M_CONTENT_TYPE				"multipart/form-data"

#define CGI_CTL_RETURN_RESULT				"CONTROL_RESULT"
#define CGI_CTL_CONTENT_RESULT				"CONTENT_RESULT"

/*for cgi parameter*/
#define	WEB_CGI_GETFILELIST			"_getfilelist"
#define	WEB_CGI_CLOSE_ACU			"_close_acu"
#define WEB_CGI_START_ACU			"_start_acu"
#define WEB_CGI_REPLACE_FILE		"_replace_file"		//Replace file
#define REPLACE_FILE_TIPS			"replace_file_tips"
#define WEB_CGI_REPLACED_FILE_NAME	"_replaced_file_name"    //Replaced file name
#define WEB_CGI_REPLACE_FILE_NAME	"_replace_file_name"
#define	WEB_CGI_UPLOAD_FILE			"_upload_file"
#define WEB_CGI_CFG_FILE            "_upload_cfg"
#define WEB_CGI_LANG_FILE           "_upload_lang"
#define WEB_CGI_ALL_LOGS		"_upload_logs"

#define CLIENT_CLOSE_ACU			1
#define CLIENT_START_ACU			2
#define CLIENT_GETFILELIST			3
#define CLIENT_UPLOAD_FILE			4
#define CLIENT_REPLACE_FILE			5
#define UPLOAD_CFG_FILE             6
#define UPLOAD_LANG_FILE            7
#define APPEND_TO_TAR				8

//added for app identification by SongXu 20170407
#define APP_MODEL_INFO_STR			"[APP_MODEL_INFO]"
#define APP_MODEL_NUM_STR				"[APP_MODEL_NUM]"
#define APP_MODEL_ALL				"ALL_MODEL"
#define MAX_MODEL_LINE_LEN	64
#define MAX_MODEL_NUM		32
#define MAX_SERIAL_LEN		64
#define CR					(0x0D)
#define LF					(0x0A)
#define SPACE				(0x20)
#define TAB					(0x09)
#define COMMENT_FLAG		"#"
#define IS_WHITE_SPACE(c)	( ((c) == SPACE) || ((c) == TAB) )
#define END_OF_LINE(c)		( ((c) == CR) || ((c) == LF) || ((c) == 0))
#define SECT_PREFIX			'['
#define CONFIG_APP_MODEL_DATA		"config/run/AppDownloadFlag.cfg"
#define CONFIG_SERIAL_NUM_DATA		"/home/SerialNum.log"

#define WEB_UPLOAD_CONFIG_TAR		0
#define WEB_UPLOAD_LANG_TAR			1
#define WEB_UPLOAD_PROGRAM_TAR		2

#define WEB_UPLOAD_PROGRAM_PATH		"/app/service"
#define WEB_UPLOAD_LANG_PATH		"/app/config/lang"
#define WEB_UPLOAD_CONFIG_PATH		"/app/config"

#define LOC_FILE_PATH				"/app/www/html/cgi-bin/loc"
#define ENG_FILE_PATH				"/app/www/html/cgi-bin/eng"
#define VAR_PATH					"/var"
#define OBJECT_PATH					"/app"
#define CLOSE_ACU_SYSTEM_FILE		"p31_close_system.htm"
#define START_ACU_SYSTEM_FILE		"p33_replace_file.htm"		
#define REPLACE_ACU_FILE			"p33_replace_file.htm"

#define WEB_SETTINGPARAM_FILE_NAME			"SettingParam.run"

//#define ACU_DOWNLOAD_FILE			"p17_filemanage_download.htm"
#define ACU_UPLOAD_FILE				"p18_filemanage_upload.htm"
#define ACU_FILE_LIST				"file_list"

#define ACU_RETURN_RESULT			"CONTROL_RESULT"

#define WEB_ENG_FILE_PATH			"/app/www/html/cgi-bin/eng"
#define WEB_LOC_FILE_PATH			"/app/www/html/cgi-bin/loc"

#define WEB_ZIP_CFG_LANG			"app_lang.tar"//.gz
#define WEB_ZIP_CFG					"app_cfg.tar"//.gz

/*General*/
#define	FILE_NO_AUTHORITY				"3"
#define FAIL_TO_COMM_WITH_ACU		"4"

/*Used to close ACU*/
#define SUCCESS_TO_CLOSE_ACU		"1"
#define FAIL_TO_CLOSE_ACU			"2"

/*Used to start ACU*/
#define SUCCESS_TO_START_ACU		"1"
#define FAIL_TO_START_ACU			"2"

#define MAX_VAR_DIR_SIZE			9437184//6144000

#define TABSPACES					8 

#define PREFIX_DOWNLOAD_FILE		"download_"

//Move to new directory,because the software package is too large(> 6M)
#define NEW_DOWNLOAD_DIRECTORY		"/usb"

#define WEB_PLC_PAGE    "p40_cfg_plc_Popup.htm"////Added by wj 20070208
#define WEB_CFG_PATH				"config/solution/ config/standard/ config/private/ config/run/"
#define WEB_LANG_PATH				"config/lang"
#define WEB_TAR_GZ  "*.tar.gz"
#define WEB_TAR     "*.tar"

static unsigned char		*HTTPBuffer = NULL;
static unsigned char		*CONTENT_TYPE = NULL;
static unsigned char		*CONTENT_LENGTH = NULL;

static int Web_FLE_getCGIParam(OUT int *iReturnControlType, OUT char **szReturnSessionID, 
							   OUT int *iReturnLanguage, OUT char **szOutSrcFile, 
							   OUT char **szOutDesFile, OUT char **szOutFileName, OUT off_t *file_size);

//static char *Web_FLE_MakeFilesListBuffer(void);
static off_t GetListName(char *name);
static off_t list(char *name);
static char *Web_FLE_GetFileName(IN char *szHttp, IN const char *szFindName);
static int Web_Replace_File(IN char *szFileName);
static void Web_Remove_File(IN char *szFileName);
static void Web_Zip_File(void);
static void Web_Del_Zip_File(void);////Added by wj 20070208

static int Web_FLE_ModifyTime(IN char *szFileName);

#define WEB_RETURN_NO_AUTHORITY				"3"

/***************queue start***************/
typedef struct tagUploadQueue
{
	unsigned char type;
	unsigned char name[255];
	unsigned char *value;
	unsigned char extend[255];
	unsigned long size;
	struct tagUploadQueue *next;
}UploadQueue;

void push(UploadQueue **pQueue, unsigned char type, unsigned char *name, 
		  unsigned char *value, unsigned char *extend, unsigned long size);
void pop(UploadQueue **pQueue);

int		IsEmpty(UploadQueue *pQueue);
static off_t PrintQueue(UploadQueue *pQueue);


UploadQueue		*pUploadQueue = NULL;
static int		serial = 0;
/***************queue end***************/
//void InitHTTPEnv();
char *Request(IN char *key,OUT char *szOut);
/***********http upload start**********/

int			Web_FLE_GetBoundary(char *boundary);
int			Web_FLE_GetFormType(char *szIn);
void		Web_FLE_GetFormName(char *szIn, char *szOut);
void		Web_FLE_GetFileExtend(char *szIn, char *szOut);
int			Web_FLE_UImport(IN char *szIn, IN unsigned long ulS, IN char  *key);
UploadQueue *Web_FLE_UExport(UploadQueue **pQueue, char *name);
int			Web_FLE_SaveToFile(char *pszPath, unsigned char *szIn, unsigned int uiLen);
/***********http upload end**********/
void Web_FLE_BinToHex(unsigned char *szIn, char *szOut, int sl);
unsigned long Web_FLE_substring(size_t iPos, char *szIn, unsigned int iDataLen, char *szKey);
static int Web_FLE_CloseACU(void);
static int Web_Get_SwichState(void);
static int Web_FLE_StartACU(void);
static char *Web_FLE_GetFileFromPath(char *szSrcFile);
//static int Web_FLE_replaceFile(IN  char *szDstFile, IN  char *szSrcFile);
static char **getPOSTvarsA(unsigned char **HTTPBuffer) ;

char	*szMatchFileName = NULL;
/*for download file*/
#define	WORK_DIR_PATH				"/app/"
#define GET_DIR_PATH_SUCCESS		0

char	strResult[100] = {"{\"status\":/*[CONTROL_RESULT]*/,\"content\":[/*[CONTENT_RESULT]*/]}"};
char	strResult1[500] = {"<!DOCTYPE html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>\n\
<script type=\"text/javascript\">\n\
parent.Pages.UploadDownloadCallback('{\"status\":/*[CONTROL_RESULT]*/,\"content\":[/*[CONTENT_RESULT]*/]}');\n\
</script>\n\
</body></html>"};

void Web_FLE_BinToHex(unsigned char *szIn, char *szOut, int sl)
{
	int i;
	*szOut = 0x0;
	for (i = 0; i < sl; i++)
	{
		sprintf(szOut+strlen(szOut), "%2.2X", szIn[i]);
	}
}



unsigned long Web_FLE_substring(size_t iPos, char *szIn, unsigned int iDataLen, char *szKey)
{
	unsigned long i;
	int iKey = (int)strlen(szKey);

	for (i = iPos; i < iDataLen; i++)
	{
		if (!strncmp(&szIn[i], szKey, (size_t)iKey))
		{
			return i;
		}
	}
	return (unsigned long)-1L;
}

void Web_ReplaceSpaceInName(char *cFileName)
{
	int i, j, iLength;

	for(i = 0; i < (int)strlen(cFileName); i++)
	{
		if(cFileName[i] == ' ')
		{
			iLength = (int)strlen(cFileName);
			cFileName[iLength + 1] = '\0';
			for(j = iLength; j > i; j--)
			{
				cFileName[j] = cFileName[j - 1];
			}
			cFileName[i] = '\\';
			i++;
		}
	}
}


char szPathList[10240];

int main(int argc, char *argv[])
{
	UNUSED(argc);
	UNUSED(argv);

	int		iReturnControlType = 0, iReturnLanguage = 0;
	char		*szSrcFile = NULL, *szDesFile = NULL;
	char		*szReturnSessionID = NULL;
	//char		*szAuthority = NULL;
	//BOOL		bReturn = FALSE;
	char		*szFileName = NULL;
	off_t		file_size = 0;
	int		iReturn = 0;
	int		iAuthority = 0;
	//char		szVARFile[64];

	char	*pHtml = NEW(char, 100);
	if(pHtml == NULL)
	{
	    return FALSE;
	}
	char	*pHtml1 = NEW(char, 500);
	if(pHtml1 == NULL)
	{
	    DELETE(pHtml);
	    pHtml = NULL;
	    return FALSE;
	}
	sprintf(pHtml,"%s",strResult);
	sprintf(pHtml1,"%s",strResult1);

	printf("Content-type:text/html\n\n");

	/*FILE *fp16;
	if((fp16 = fopen("/var/file16.html","wb")) != NULL)
	{
	    fclose(fp16);
	}*/

	if((iReturn = Web_FLE_getCGIParam(&iReturnControlType, 
		&szReturnSessionID, 
		&iReturnLanguage,
		&szSrcFile, 
		&szDesFile, 
		&szFileName, 
		&file_size)) == TRUE)
	{
		/*language type*/
		switch(iReturnControlType)
		{
		case CLIENT_CLOSE_ACU:
			{
			    /*FILE *fp1;
			    if((fp1 = fopen("/var/file1.html","wb")) != NULL)
			    {
				fclose(fp1);
			    }*/
				iAuthority = start_session(Web_RemoveWhiteSpace(szReturnSessionID));
				if(iAuthority < 0)			//iAuthority < 0 , it is the session fault,
					//not the authority fault
				{
				    ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "98");
				    ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
				    PostPage(pHtml);
				    DELETE(pHtml);
				    pHtml = NULL;
				    return FALSE;
				}
				else if(iAuthority < 3)
				{
				    ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), WEB_RETURN_NO_AUTHORITY);
				    ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
				    PostPage(pHtml);
				    DELETE(pHtml);
				    pHtml = NULL;
				    return FALSE;
				}
				if(Web_FLE_CloseACU() == TRUE)
				{
					//Judge authority
					sleep(5);
					//printf("Content-type:text/html\n\n");
					ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "1");
					ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
					PostPage(pHtml);
					//Web_Zip_File();//removed by wj 20070209
				}
				else
				{
				    ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "2");
				    ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
				    PostPage(pHtml);
				}
				break;
			}
		case CLIENT_START_ACU:
			{
			    /*FILE *fp3;
			    if((fp3 = fopen("/var/file3.html","wb")) != NULL)
			    {
				fclose(fp3);
			    }*/
				Web_FLE_StartACU();
				ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "1");
				ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
				PostPage(pHtml);
				break;
			}
		case CLIENT_UPLOAD_FILE:
			{
			    /*FILE *fp4;
			    if((fp4 = fopen("/var/file4.html","wb")) != NULL)
			    {
				fclose(fp4);
			    }*/
				
					if(Web_Get_SwichState() == 3)
					{
					    /*FILE *fp23;
					    if((fp23 = fopen("/var/file23.html","wb")) != NULL)
					    {
						fclose(fp23);
					    }*/
					    ReplaceString(&pHtml1, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "12");
					    ReplaceString(&pHtml1, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
					    PostPage(pHtml1);
					}
					else if(Web_Get_SwichState() == 2)
					{
					    /*FILE *fp24;
					    if((fp24 = fopen("/var/file24.html","wb")) != NULL)
					    {
						fclose(fp24);
					    }*/
					    ReplaceString(&pHtml1, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "2");
					    ReplaceString(&pHtml1, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
					    PostPage(pHtml1);
					}
					else
					{
						char *szFileNameTemp = Web_FLE_GetFileFromPath(szMatchFileName);

						char szFileNameTotal[256];
						sprintf(szFileNameTotal, "%s%s", PREFIX_DOWNLOAD_FILE, szFileNameTemp);

						/*FILE *fp27;
						if((fp27 = fopen("/var/file27.html","wb")) != NULL)
						{
						    fwrite(szFileNameTotal,strlen(szFileNameTotal), 1, fp27);
						    fclose(fp27);
						}*/
						Web_ReplaceSpaceInName(szFileNameTotal);
						int iReturnValue = Web_Replace_File(szFileNameTotal);
						//delete
						Web_Remove_File(szFileNameTotal);

						if(iReturnValue == TRUE)
						{
						    /*FILE *fp25;
						    if((fp25 = fopen("/var/file25.html","wb")) != NULL)
						    {
							fclose(fp25);
						    }*/
						    ReplaceString(&pHtml1, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "1");
						    ReplaceString(&pHtml1, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
						    PostPage(pHtml1);
						}
						else
						{
						    /*FILE *fp26;
						    if((fp26 = fopen("/var/file26.html","wb")) != NULL)
						    {
							fclose(fp26);
						    }*/
						    ReplaceString(&pHtml1, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "2");
						    ReplaceString(&pHtml1, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
						    PostPage(pHtml1);
						}
					}
					if(szFileName != NULL)
					{
					    DELETE(szFileName);
					    szFileName = NULL;
					}
				break;
			}
		case UPLOAD_CFG_FILE:  //Added by wj 20070209
			{
			    /*FILE *fp5;
			    if((fp5 = fopen("/var/file5.html","wb")) != NULL)
			    {
				fclose(fp5);
			    }*/

				Web_Del_Zip_File();

				char szDelRun[256];
#define CFG_RUN_FILE    "AcuLastSolution.run"

				char szZip[256];

				memset(szDelRun, 0, sizeof(szDelRun));
				sprintf(szDelRun, "mv -f /app/config/run/%s /var/%s", CFG_RUN_FILE,CFG_RUN_FILE);
				system(szDelRun);

				memset(szZip, 0, sizeof(szZip));
				//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_CFG_PATH,WEB_ZIP_CFG_LANG);
				sprintf(szZip, "cd /app;tar -cf /var/download/%s %s", WEB_ZIP_CFG, WEB_CFG_PATH);
				system(szZip);

				memset(szDelRun, 0, sizeof(szDelRun));
				sprintf(szDelRun, "mv -f /var/%s /app/config/run/%s", CFG_RUN_FILE,CFG_RUN_FILE);
				system(szDelRun);

				ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "10");
				ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
				PostPage(pHtml);

				break;
			}

		case UPLOAD_LANG_FILE:  //Added by wj 20070209
			{
			    /*FILE *fp6;
			    if((fp6 = fopen("/var/file6.html","wb")) != NULL)
			    {
				fclose(fp6);
			    }*/

				Web_Del_Zip_File();
				char szZip[256];

				memset(szZip, 0, sizeof(szZip));
				//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
				sprintf(szZip, "cd /app;tar -cf /var/download/%s %s", WEB_ZIP_CFG_LANG, WEB_LANG_PATH);
				system(szZip);

				ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "11");
				ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
				PostPage(pHtml);
				break;

			}

		case APPEND_TO_TAR:	//Added by Hrishikesh Oak for CR - Retrieving Diagnostic Package
		    {
			  //Following definitions taken from web_user.h
			  #define WEB_LOG_DIR_HA	"/var/download/download_alarmHistory.html"
			  #define WEB_LOG_DIR_BT	"/var/download/BatteryTestLog"
			  #define WEB_LOG_DIR_EL	"/var/download/download_eventLog.html"
			  #define WEB_LOG_DIR_SL	"/var/download/download_systemLog.html"
			  #define WEB_LOG_DIR_DL	"/var/download/download_dataLog.html"
			  #define WEB_EXPORTED_SETTINGS "/var/download/settings.txt"
			  #define WEB_DATA_LOGIN_VAR	"/var/datas/data.login.html"
			  #define WEB_DATA_INVENTORY_VAR"/var/datas/data.system_inventory.html"
			  #define WEB_DATA_ALARM_VAR	"/var/datas/data.alarms.html"
			  #define CONTROLLER_INFO_FILE	"/var/download/controller_info.txt"
			  #define INVENTORY_FILE	"/var/download/inventory.txt"
			  #define CURRENT_ALARMS_FILE	"/var/download/current_alarms.txt"
			  #define WEB_SETTINGPARAM_TAR	"/var/download/SettingParam.tar"
			  #define WEB_SETTINGPARAM_TEXT	"/var/download/SettingParam.txt"

			  char szZip[2048];

			  // create Inventory file
			  FILE *fp = NULL;
			  fp = fopen(INVENTORY_FILE, "w");
			  if(fp != NULL)
			  {
			      fprintf(fp, "%16s%16s%16s%16s%32s%16s\n\n",
				      "Equipment", "EquipLocal", "Product Model",
				      "Hw Revision", "Serial Number", "Sw Revision");
			      fclose(fp);
			      fp = NULL;
			  }
			  memset(szZip, 0, sizeof(szZip));
			  sprintf(szZip, "sed -n 's/    \"inventory\":\\[\\[//p' %s | sed 's/\\],\\[/\\n/g' | sed 's/[][\",]*//g' >> %s", WEB_DATA_INVENTORY_VAR, INVENTORY_FILE);
			  system(szZip);

			  // create tar file
			  memset(szZip, 0, sizeof(szZip));
			  if(szMatchFileName)
			  {
				sprintf(szZip, "rm -f /var/download/%s", szMatchFileName);
				system(szZip);
				sprintf(szZip, "tar -czf /var/download/%s %s %s %s %s %s %s %s %s %s %s"
					" %s1.html %s2.html %s3.html %s4.html %s5.html %s6.html %s7.html %s8.html %s9.html %s10.html",
					szMatchFileName, WEB_LOG_DIR_HA,
					WEB_LOG_DIR_EL, WEB_LOG_DIR_SL, WEB_LOG_DIR_DL,
					WEB_EXPORTED_SETTINGS, CONTROLLER_INFO_FILE, INVENTORY_FILE,
					CURRENT_ALARMS_FILE, WEB_SETTINGPARAM_TAR, WEB_SETTINGPARAM_TEXT
					,WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT
					, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT
				);
			  }
			  else
			  {
				//sprintf(szZip, "tar -czf /var/download/log.tar %s %s %s %s %s", WEB_LOG_DIR_HA, WEB_LOG_DIR_EL, WEB_LOG_DIR_SL, WEB_LOG_DIR_DL, WEB_EXPORTED_SETTINGS);
			  }
			  system(szZip);
			  sprintf(szZip, "rm -f  %s %s %s %s %s %s %s %s %s %s"
				  " %s1.html %s2.html %s3.html %s4.html %s5.html %s6.html %s7.html %s8.html %s9.html %s10.html",
				  WEB_LOG_DIR_HA, WEB_LOG_DIR_EL,
				  WEB_LOG_DIR_SL, WEB_LOG_DIR_DL, WEB_EXPORTED_SETTINGS,
				  CONTROLLER_INFO_FILE, INVENTORY_FILE, CURRENT_ALARMS_FILE,
				  WEB_SETTINGPARAM_TAR, WEB_SETTINGPARAM_TEXT
				  ,WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT
				  , WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT, WEB_LOG_DIR_BT
			  );
			  system(szZip);

			  ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "20");
			  ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTENT_RESULT), "");
			  PostPage(pHtml);
			  break;
		    }
		}

	}
	else if(iReturn == ERR_FILE_NO_SPACE)
	{
		printf("<html><body topmargin=\"100\" leftmargin=\"0\" bgcolor=\"#ffffff\" text=\"#000000\">");
		printf("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"><tr bgcolor=\"#6699cc\">");
		printf("<td height=\"31\"><div align=\"left\"><font style=\"FONT-SIZE:14pt\" face=\"PrimaSans BT, Verdana, sans-serif\" color=\"white\">");
		printf("<b>Over space tips</b></font></div></td></tr><tr align=\"center\" valign=\"middle\"><td colspan=\"3\" bgcolor=\"#cccccc\">");
		printf("<table ><font style=\"FONT-SIZE:14pt\" face=\"PrimaSans BT, Verdana, sans-serif\" >Your upload files is over the space, please upload the correct files to Controller.");
		printf("Use Back button in IE to go back!</font></table></td></tr></table></body></html>");
	}


	if(szSrcFile != NULL)
	{
		DELETE(szSrcFile);
		szSrcFile = NULL;
	}

	if(szDesFile != NULL)
	{
		DELETE(szDesFile);
		szDesFile = NULL;
	}

	if(szFileName != NULL)
	{
		DELETE(szFileName);
		szFileName = NULL;
	}

	if(szMatchFileName != NULL)
	{
		DELETE(szMatchFileName);
		szMatchFileName = NULL;
	}
	if(pHtml != NULL)
	{
	    DELETE(pHtml);
	    pHtml = NULL;
	}
	if(pHtml1 != NULL)
	{
	    DELETE(pHtml1);
	    pHtml1 = NULL;
	}
	return TRUE; 
}

static int Web_Get_SwichState(void)
{
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;
	char    *pSysState = NULL;
	//char    *pAppState = NULL;
	char	*pTemp = NULL;

#define SWICH_FILE_NAME		"/var/appinfo.log"
#define	SYS_SWICH_STATE		"JUMP_SYSTEM_RDONLY:"
#define	APP_SWICH_STATE		"JUMP_APP_RDONLY:"

	pFile = fopen(SWICH_FILE_NAME, "r");
	if (pFile == NULL)
	{	
		return 2;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return 2;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	pTemp = strstr(szInFile,SYS_SWICH_STATE);
	pSysState = pTemp+19;

	/*pTemp = strstr(szInFile,APP_SWICH_STATE);
	pAppState = pTemp+16;*/

	/*if(*pAppState == 'S')
	{
	    FILE *fp20 = fopen("/var/20.htm","w");
	    fclose(fp20);
		return 3;
	}*/

	SAFELY_DELETE(szInFile);
	szInFile = NULL;

	return TRUE;

}

static int Web_FLE_CloseACU(void)
{
	/*���͹ر�ACU �ź�,���ACU �Ƿ�ر�*/
	//system("killall -9 acu");
	int		fd;    //fifo handle
	int		iLen = 0;
	char	szBuf[1024];


	/*create FIFO with our PID as part of name*/
	if((fd = open(MAIN_FIFO_NAME,O_WRONLY))<0)
	{
		return FALSE;
	}

	//time(&tp);
	iLen = sprintf(szBuf,"%10ld%2d%8d%2d%2d",(long)getpid(), FILE_MANAGE,0,0,0);

	if((write(fd,szBuf,strlen(szBuf) + 1)) < 0)
	{
		close(fd);
		return FALSE;
	}

	close(fd);
	return TRUE;
	//#define WEB_CLOSE_ACU  "/acu/stopacu"
	//	if(system(WEB_CLOSE_ACU) > 0)
	//	{
	//		return TRUE;
	//	}
	//	return FALSE;
}


#define _O_RDWR         0x0002  /* open for reading and writing */

static int Web_FLE_StartACU(void)
{
	/*��������ACU �ź�,���ACU �Ƿ�ر�*/
	//char		szSystemCommand[64];
	//sprintf(szSystemCommand, "%s", "reboot");

	//system(szSystemCommand);
	//printf("\n___/sbin/reboot OK!___\n");
	//system("/sbin/reboot");
	int s_hWatchDog;
	s_hWatchDog = open("/dev/wd", _O_RDWR);
	if(s_hWatchDog < 0)
	{
		//printf("error to open %d\n", s_hWatchDog);
		return ;
	}

	// init the maximum alllowed feed period in seconds.
	ioctl(s_hWatchDog, IOCTL_SET_TIMER, 0x03);

	ioctl(s_hWatchDog, IOCTL_EN_WD);

	sleep(20);

	return TRUE;

}

char *Request(IN char *key,OUT char *szOut)
{
	UNUSED(key);
	return szOut;
}



//��ȡ�����Ʒָ��
int Web_FLE_GetBoundary(IN char *boundary)
{
	int		iContentType = (int)strlen(M_CONTENT_TYPE);
	int		iLen = 0;
	int		i = 0;
	int		iPosition = iContentType + 11;//11 byte length just '; boundary='
	*boundary = 0x0;

	if (CONTENT_LENGTH == NULL)
		return 1;

	if (CONTENT_TYPE == NULL) 
		return 2;

	if (!strncmp(CONTENT_TYPE, M_CONTENT_TYPE, (size_t)iContentType))
	{
		*boundary++ = '-';
		*boundary++ = '-';
		iLen = (int)strlen(CONTENT_TYPE);

		for (i = iPosition; i < iLen; i++)
		{
			*boundary++ = CONTENT_TYPE[i];
		}

		*boundary = 0x0;
		return 0;
	}
	return 3;
}



/*
��ȡ������
=0������
=1���ļ���
*/
int Web_FLE_GetFormType(char *szIn)
{
	char key[255]={0};
	char *p;

	sprintf(key, "filename=%c", 34);
	p = strstr(szIn, key);

	return p != NULL;
}



//��ȡ����
void Web_FLE_GetFormName(char *szIn, char *szOut)
{
	char	key[255]={0};
	char	*p;

	sprintf(key, "name=%c", 34);
	p = strstr(szIn, key);

	*szOut = 0x0;
	if (p)
	{
		p += (int)strlen(key);

		while(*p&&*p!='"') 
			*szOut++ = *p++;
	}
}



//��Content-Type��ȡ�ļ���
void Web_FLE_GetFileExtend(IN char *szIn, OUT char *szOut)
{

	char	*szKey	= ".";
	const char	*szKey1 = "filename=\"";
	char *p;
	p= strrchr(szIn,92);		//��ȡhttpͷ�����һ��"\"

	*szOut = 0x0;
	if (p)
	{
		p += (int)strlen(szKey);

		while( *p && *p != '"')
			*szOut++ = *p++;
	}
	else
	{
	    p = strstr((const char*)(szIn), szKey1);
	    if(p)// for firefox browser
	    {
		p += (int)strlen(szKey1);
		while(*p && *p != '"')
		    *szOut++ = *p++;
	    }
	}
}



//�������ݣ����浽����
/*sPos : ��ʼλ�� s1:���ݳ���.�м�����Ϊ��������ݣ��޷ָ���*/
void DBParse(unsigned char *szIn, unsigned long ulStartPos, unsigned long ulDataLen)
{
	unsigned char	type = 0, name[255]={0}, *value, extend[255]={0};
	unsigned long	size;
	int				iCrlf = (int)strlen(CGI_CRLF);
	unsigned		long ulHeadLength;//head length
	unsigned		long ulPos1, ulPos2;
	char			*buf = NULL;

	buf = NEW(char, (size_t)ulDataLen + 1);//(char *)malloc(ulDataLen + 1);//

	char			*tmp = NULL;

	UNUSED(size);
	memset(buf, 0x0, (size_t)(ulDataLen + 1));
	memcpy(buf, szIn + ulStartPos, (size_t)ulDataLen);

	/*get head length*/
	ulHeadLength = Web_FLE_substring((size_t)0, buf, ulDataLen, "\r\n\r\n");
	tmp = NEW(char, ulHeadLength + 1);//(char *)malloc(ulHeadLength + 1);

	memset(tmp, 0x0, ulHeadLength + 1);
	memcpy(tmp, buf, ulHeadLength);

	// for debug begin
	if(ulHeadLength > 1)
	{
	    /*FILE *fp30;
	    if((fp30 = fopen("/var/file30.html","a+")) != NULL)
	    {
		fwrite(tmp, strlen(tmp), 1, fp30);
		fclose(fp30);
	    }*/
	}
	// for debug end

	type = (unsigned char)Web_FLE_GetFormType(tmp);
	Web_FLE_GetFormName(tmp, (char *)name);

	ulPos1 = ulHeadLength + iCrlf + iCrlf;  /*����"\r\n"*/
	ulPos2 = ulDataLen - ulPos1;

	value = NEW(unsigned char, ulPos2 + 1);//(unsigned char *)malloc(ulPos2 + 1);
	memset(value, 0x0, ulPos2+1);
	memcpy(value, buf + ulPos1, ulPos2);

	if (type == (unsigned char)1  && ulPos2 > 0)
	{
		/*get file extend*/
		Web_FLE_GetFileExtend(tmp, (char *)extend);
	}

	//TRACE("name:%s", name);
	push(&pUploadQueue, (unsigned char)type, name, 
		(unsigned char *)value, extend, 
		(unsigned long)ulPos2);
	serial++;

	DELETE(value);
	DELETE(tmp);
	DELETE(buf);//free(buf);
}


/*
�ϴ����
szIn = stdin���������
sl = CONTENT_LENGTH
key = �����Ʒָ��boundary
*/
/*IN unsigned long ulS :data length*/
int Web_FLE_UImport(IN char *szIn, IN unsigned long ulS, IN char  *key)
{
	int				iKey =	(int)strlen(key);
	int				iCrlf = (int)strlen(CGI_CRLF);
	unsigned long	ulPos1,  ulPos2;

	/*��ȡ��һ�������Ʒָ�����λ��*/
	ulPos1 = Web_FLE_substring((size_t)0, szIn, (unsigned int)ulS, key);
	while (ulPos1 != (unsigned long)-1)
	{
		ulPos2 = Web_FLE_substring((size_t)(ulPos1 + (unsigned long)iKey), szIn, (unsigned int)ulS, key);//��һ�������Ʒָ����Ŀ�ʼλ��

		if (ulPos2 ==(unsigned long) -1) 
			break;
		/*�ѷָ���֮������ݽ��н���*/
		//printf("Content-type:text/html\n\n");
		//TRACE("[%ld][%ld][%d][%d][%d]!\n",ulPos1,ulPos2, iKey,iCrlf, ulPos2 - ulPos1 - iKey - iCrlf * 2);
		DBParse(szIn, ulPos1 + iKey + iCrlf, ulPos2 - ulPos1 - iKey - iCrlf * 2);//���������Ʒָ���е�����

		ulPos1 = Web_FLE_substring((size_t)ulPos2, szIn, (unsigned int)ulS, key);


	}

	return 1;
}

/*
�ϴ��ļ�����
����PUploadQueue�ṹ
name = ����
*/
UploadQueue *Web_FLE_UExport(UploadQueue **pQueue, char *name)
{
	UploadQueue *PQ = *pQueue;
	UploadQueue *R = NULL;

	while(PQ != NULL)
	{
		if (!strcmp(PQ->name, name))
		{
			R = PQ;
			break;
		}
		PQ= PQ->next;
	}
	return R;
}

/*
�����ļ�
xPath = ·��
xIn = ����
xl = ���ݳ���
*/
int Web_FLE_SaveToFile(IN char *pszPath, IN unsigned char *szIn, IN unsigned int uiLen)
{
	FILE *fp = fopen(pszPath, "w");
	if (fp != NULL)
	{
		fwrite(szIn, uiLen, 1, fp);
		fclose(fp);
		return 1;
	}
	else
	{
		return 0;
	}
}



/**************queue*****************/

void push(UploadQueue **pQueue, unsigned char type, unsigned char *name, 
		  unsigned char *value, unsigned char *extend, unsigned long size)
{
	//UploadQueue *tmp = (UploadQueue *)malloc(sizeof(UploadQueue));
	UploadQueue *tmp = NEW(UploadQueue, 1);
	UploadQueue *PQ = *pQueue;
	if (tmp!=NULL)
	{
		tmp->type = type;
		strlcpy(tmp->name, name, sizeof(tmp->name));
		//tmp->value = (unsigned char *)malloc(size+1);
		tmp->value = NEW(unsigned char, size + 1);

		memset(tmp->value, 0x0, size+1);
		memcpy(tmp->value, value, size);

		strlcpy(tmp->extend, extend, sizeof(tmp->name));

		tmp->size = size;
		tmp->next = NULL;

		if (*pQueue == NULL)
		{
			*pQueue = tmp;
		}
		else
		{
			while(PQ != NULL && PQ->next != NULL)
			{
				PQ = PQ->next;
			}
			PQ->next = tmp;
		}
	}
	else
	{
		TRACE("No memory available.\n");
	}
}

void pop(UploadQueue **ppQueue)
{
	UploadQueue *tmp;
	if (IsEmpty(*ppQueue)) 
	{
		return;
	}
	tmp = *ppQueue;
	*ppQueue = (*ppQueue)->next;
	DELETE(tmp->value);
	DELETE(tmp);
}

int IsEmpty(UploadQueue *pQueue)
{
	return pQueue == NULL;
}

static off_t PrintQueue(UploadQueue *pQueue)
{
	char		filePath[255]={0};
	off_t		dir_size = 0, file_size = 0;

	dir_size = list(VAR_PATH);

	UploadQueue		*pTempQueue = pQueue;
	while(pTempQueue != NULL)
	{
		if(pTempQueue->type == (unsigned char)1 && pTempQueue->size > 0)
		{
			file_size += pTempQueue->size;
		}
		pTempQueue = pTempQueue->next;
	}


	//printf("dir_size : %ld[%ld]\n", dir_size, file_size);
	if(file_size < MAX_VAR_DIR_SIZE - dir_size)
	{
		while(pQueue != NULL)
		{
			if (pQueue->type == (unsigned char)1 && pQueue->size > 0)
			{
				sprintf(filePath, "%s/%s%s", NEW_DOWNLOAD_DIRECTORY, PREFIX_DOWNLOAD_FILE,pQueue->extend);
				Web_FLE_SaveToFile(filePath, pQueue->value, pQueue->size);
			}
			pQueue = pQueue->next;
		}
	}
	else
	{
		return ERR_FILE_NO_SPACE;
	}
	return file_size; 
}


static int Web_FLE_getCGIParam(OUT int *iReturnControlType, 
							   OUT char **szReturnSessionID, 
							   OUT int *iReturnLanguage, 
							   OUT char **szOutSrcFile,
							   OUT char **szOutDesFile, 
							   OUT char **szOutFileName,
							   OUT off_t *file_size)
{
	char			**postvars = NULL; /* POST request data repository */
	char			**getvars = NULL; /* GET request data repository */
	int				form_method; /* POST = 1, GET = 0 */  
	char			*val = NULL;
	unsigned long	ulLen = 0;
	int				iControlType = 0 , iLanguageType = 0;
	int				i;
	char			boundary[255]={0};
	char			*szSessionID = NULL;
	UploadQueue		*pUQ = NULL;
	char			*szSrcFile = NULL, *szDesFile = NULL;

	form_method		= getRequestMethod();

	UNUSED(ulLen);
	UNUSED(szSrcFile);
	UNUSED(szDesFile);
	UNUSED(szOutSrcFile);
	UNUSED(szOutDesFile);


	/*FILE *fp15;
	if((fp15 = fopen("/var/file15.html","wb")) != NULL)
	{
	    fclose(fp15);
	}*/

	if(form_method == POST) 
	{
	    /*FILE *fp17;
	    if((fp17 = fopen("/var/file17.html","wb")) != NULL)
	    {
		fclose(fp17);
	    }*/
		getvars		= getGETvars();
		//postvars	= getPOSTvars();


		postvars = getPOSTvarsA(&HTTPBuffer);
		if(postvars == NULL )
		{
		    /*FILE *fp18;
		    if((fp18 = fopen("/var/file18.html","wb")) != NULL)
		    {
			fclose(fp18);
		    }*/
			return FALSE;
		}
		/*FILE *fp19;
		if((fp19 = fopen("/var/file19.html","wb")) != NULL)
		{
		    fclose(fp19);
		}*/
	} 
	else if(form_method == GET) 
	{
	    /*FILE *fp20;
	    if((fp20 = fopen("/var/file20.html","wb")) != NULL)
	    {
		fclose(fp20);
	    }*/
		getvars = getGETvars();
	}
	else 
	{
	    /*FILE *fp21;
	    if((fp21 = fopen("/var/file21.html","wb")) != NULL)
	    {
		fclose(fp21);
	    }*/
		return FALSE;
	}



	if((val = getValue( getvars, postvars, SESSION_ID)) != NULL) // sessionId
	{
		//szSessionID = atoi(val);
		szSessionID = NEW(char, 32);
		sprintf(szSessionID, "%s", val);
		//printf("szSessionID[%s]", szSessionID);
	}

	if((val = getValue(getvars,postvars,LANGUAGE_TYPE)) != NULL)
	{
		iLanguageType = atoi(val);
	}



	//if((val = getValue(getvars,postvars,"file_name")) != NULL)
	//{
	//	TRACE("val : %s\n", val);
	//	sprintf(szMatchFileName, "%s", val);

	//}


	if((val = getValue(getvars,postvars,WEB_CGI_CLOSE_ACU)) != NULL && atoi(val) > 0)
	{
	    /*FILE *fp2;
	    if((fp2 = fopen("/var/file2.html","wb")) != NULL)
	    {
	        fclose(fp2);
	    }*/
		iControlType = CLIENT_CLOSE_ACU;
	}
	else 
	{
		if((val =Web_FLE_GetFileName(HTTPBuffer, LANGUAGE_TYPE)) != NULL )
		{
			iLanguageType = atoi(val);
		}

		if((val =Web_FLE_GetFileName(HTTPBuffer, WEB_CGI_START_ACU)) != NULL&& atoi(val) > 0)
		{
		    /*FILE *fp7;
		    if((fp7 = fopen("/var/file7.html","wb")) != NULL)
		    {
			fclose(fp7);
		    }*/
			iControlType = CLIENT_START_ACU;
		}
		else if((val = getValue(getvars,postvars,WEB_CGI_START_ACU)) != NULL && atoi(val) > 0)	
		{
		    /*FILE *fp8;
		    if((fp8 = fopen("/var/file8.html","wb")) != NULL)
		    {
			fclose(fp8);
		    }*/
			//TRACE("val : %s\n", val);
			iControlType = CLIENT_START_ACU;
		}
		else if((val =Web_FLE_GetFileName(HTTPBuffer, WEB_CGI_CFG_FILE)) != NULL&& atoi(val) > 0)
		{
		    /*FILE *fp9;
		    if((fp9 = fopen("/var/file9.html","wb")) != NULL)
		    {
			fclose(fp9);
		    }*/
			iControlType = UPLOAD_CFG_FILE;
		}
		else if((val = getValue(getvars,postvars,WEB_CGI_CFG_FILE)) != NULL && atoi(val) > 0)	
		{
		    /*FILE *fp10;
		    if((fp10 = fopen("/var/file10.html","wb")) != NULL)
		    {
			fclose(fp10);
		    }*/
			//TRACE("val : %s\n", val);
			iControlType = UPLOAD_CFG_FILE;
		}
		else if((val =Web_FLE_GetFileName(HTTPBuffer, WEB_CGI_LANG_FILE)) != NULL&& atoi(val) > 0)
		{
		    /*FILE *fp11;
		    if((fp11 = fopen("/var/file11.html","wb")) != NULL)
		    {
			fclose(fp11);
		    }*/
			iControlType = UPLOAD_LANG_FILE;
		}
		else if((val = getValue(getvars,postvars,WEB_CGI_LANG_FILE)) != NULL && atoi(val) > 0)	
		{
		    /*FILE *fp12;
		    if((fp12 = fopen("/var/file12.html","wb")) != NULL)
		    {
			fclose(fp12);
		    }*/
			//TRACE("val : %s\n", val);
			iControlType = UPLOAD_LANG_FILE;
		}
		else if((val = getValue(getvars,postvars,WEB_CGI_ALL_LOGS)) != NULL && atoi(val) > 0)
		{
			iControlType = APPEND_TO_TAR;
			szMatchFileName = getValue(getvars,postvars, "file_name");
		}
		else//((val = getValue(getvars,postvars,WEB_CGI_UPLOAD_FILE)) != NULL)
		{
		    /*FILE *fp13;
		    if((fp13 = fopen("/var/file13.html","wb")) != NULL)
		    {
			fclose(fp13);
		    }*/
			off_t			tmp_file_size = 0;
			//printf("Content-type:text/html\n\n");
			Web_Del_Zip_File();//Added by wj 20070208

			CONTENT_TYPE	= (unsigned char *)getenv("CONTENT_TYPE");
			CONTENT_LENGTH	= (unsigned char *)getenv("CONTENT_LENGTH");
			iControlType = CLIENT_UPLOAD_FILE;

			szMatchFileName = Web_FLE_GetFileName(HTTPBuffer,"file_name");

			/*Upload file*/

			if (!Web_FLE_GetBoundary(boundary))
			{
				/*get data*/
				Web_FLE_UImport((char *)HTTPBuffer, (unsigned long)atol(CONTENT_LENGTH), boundary);
			}

			//FILE *fp22;
			//if((fp22 = fopen("/var/file22.html","a+")) != NULL)
			//{
			//    //fwrite(boundary,strlen(boundary), 1,fp22);
			//    //fwrite(CONTENT_TYPE,strlen(CONTENT_TYPE), 1,fp22);
			//    //fwrite(CONTENT_LENGTH,strlen(CONTENT_LENGTH), 1,fp22);
			//    fwrite(szMatchFileName,strlen(szMatchFileName), 1,fp22);
			//    fclose(fp22);
			//}

			pUQ = Web_FLE_UExport(&pUploadQueue, "filedata0");//����ʾ��; filedata0�Ǳ���


			if (pUQ != NULL)
			{

				tmp_file_size = PrintQueue(pUploadQueue);

				if(tmp_file_size == ERR_FILE_NO_SPACE)
				{
					/*			DELETE(szFileNameMatch);
					szFileNameMatch = NULL;*/
					return ERR_FILE_NO_SPACE;
				}
				else
				{
					*file_size = tmp_file_size;
				}

			}

			for (i = 0; i < serial; i++) 
				pop(&pUploadQueue);//��ն���
			fflush(stdout);
		}
	}
	//printf("szFilePath : iReturnControlType :%d[%s]\n",iControlType, HTTPBuffer);

	*iReturnControlType	= iControlType;
	*szReturnSessionID	= szSessionID;
	*iReturnLanguage	= iLanguageType;

	if (HTTPBuffer != NULL) 
		DELETE(HTTPBuffer);
	cleanUp( getvars, postvars);


	/*FILE *fp14;
	if((fp14 = fopen("/var/file14.html","wb")) != NULL)
	{
	    fclose(fp14);
	}*/
	return TRUE;  
}

static char *Web_FLE_GetFileName(IN char *szHttp, IN const char *szFindName)
{
	ASSERT(szHttp);
	ASSERT(szFindName);

	//TRACE("szFindName : %s\n", szFindName);
	//TRACE("szHttp : %256s\n", szHttp);
	char	*pSearchValue1 = NULL, *pSearchValue2 = NULL;
	int		iPosition = 0;

	/*FILE *fp7 = fopen("/var/7.htm","w");
	fwrite(szHttp,strlen(szHttp), 1,fp7);
	fclose(fp7);*/

	pSearchValue1 = strstr(szHttp,szFindName);
	//TRACE("pSearchValue1 : %s", pSearchValue1);
	if(pSearchValue1 != NULL)
	{
		pSearchValue1 = pSearchValue1 + strlen(szFindName) + 2;
		//TRACE("pSearchValue1 : %64s", pSearchValue1);
		if((pSearchValue2 = strstr(pSearchValue1, "--")) != NULL)
		{
			//TRACE("pSearchValue2 : %s", pSearchValue2);
			iPosition = pSearchValue2 - pSearchValue1;
			char	*szOutName = NEW(char, iPosition + 1);
			strncpyz(szOutName, pSearchValue1, iPosition + 1);
			return szOutName;
		}
	}
	return NULL;
}
static char *Web_FLE_GetFileFromPath(char *szSrcFile)
{
	char		*szDesFile = NULL;
	char		*szSrcCopyFile = szSrcFile;
	char		*pSearchValue = NULL;
	int			iPosition = 0;
#define MAX_FILE_LEN			128
#define GET_REVERSE_SLASH		92//'\'

	/*FILE *fp6 = fopen("/var/6.htm","w");
	fwrite(szSrcFile,strlen(szSrcFile), 1,fp6);
	fclose(fp6);*/
	szDesFile = NEW(char, MAX_FILE_LEN);
	//printf("[1]szSrcCopyFile[%s]\n", szSrcCopyFile);
	if(szDesFile != NULL && szSrcCopyFile != NULL)
	{

		pSearchValue = strrchr(szSrcCopyFile, GET_REVERSE_SLASH);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - szSrcCopyFile;
			if(iPosition > 0)
			{
				szSrcCopyFile = szSrcCopyFile + iPosition + 1;
				//printf("[2]szSrcCopyFile[%s]\n", szSrcCopyFile);
				strncpyz(szDesFile, szSrcCopyFile, MAX_FILE_LEN  );

				//printf("[3]szDesFile[%s]\n", szDesFile);
				return szDesFile;
			}
		}
		else// for firefox with no absolute path
		{
		    DELETE(szDesFile);
		    szDesFile = NULL;
		    return (szSrcCopyFile + 3);// skip the "\n\r\n"
		}
	}

	DELETE(szDesFile);
	szDesFile = NULL;
	return NULL;
}


static char **getPOSTvarsA(unsigned char **HTTPBuffer) 
{
	int			i;
	int			content_length;
	char		**postvars;
	char		*postinput;
	char		**pairlist;
	int			paircount = 0;
	char		*nvpair;
	char		*eqpos;
	unsigned char		*pHTTPBuffer = NULL;

	postinput = getenv("CONTENT_LENGTH");

	if (!postinput)
		return NULL;
	if(!(content_length = atoi(postinput)))
		return NULL;
	if(!(pHTTPBuffer = NEW(char, (size_t)content_length + 1)))
	{
		return NULL;
	}
	if(!(postinput = NEW(unsigned char,(size_t)content_length + 1)))
		return NULL;
	if(!fread(pHTTPBuffer, (size_t)content_length, (size_t)1, stdin))
		return NULL;
	*HTTPBuffer = pHTTPBuffer;
	strncpyz(postinput, pHTTPBuffer, content_length + 1);
	//printf("postinput[%s]\n", postinput);
	//if((pHTTPBuffer = NEW(unsigned char, (size_t)content_length + 1)) != NULL)
	//{
	//	//sprintf(pHTTPBuffer, "%s[%d]", postinput,content_length);


	//	//printf("%us", postinput);
	//	strncpyz(pHTTPBuffer, (unsigned char *)postinput, content_length + 1);
	//	*HTTPBuffer = pHTTPBuffer;

	//}


	for(i=0;postinput[i];i++)
		if(postinput[i] == '+')
			postinput[i] = ' ';

	//pairlist = (char **) malloc(256*sizeof(char **));
	pairlist = NEW(char *,256);
	paircount = 0;
	nvpair = strtok(postinput, "&");
	//strtok_r(postinput, "&",&nvpair);
	while (nvpair) 
	{
		pairlist[paircount++] = strdup(nvpair);
		if(!(paircount%256))
			//pairlist = (char **) realloc(pairlist, (paircount+256)*sizeof(char **));
			pairlist = RENEW(char *, pairlist, (paircount+256)*sizeof(char *));
		nvpair = strtok(NULL, "&");
		//strtok_r(NULL, "&", &nvpair);
	}

	pairlist[paircount] = 0;
	//postvars = (char **) malloc((paircount*2+1)*sizeof(char **));
	postvars = NEW(char *,paircount*2 +1);
	for(i = 0;i<paircount;i++) 
	{
		if((eqpos = strchr(pairlist[i], '='))) 
		{
			*eqpos= '\0';
			unescape_url(postvars[i*2+1] = strdup(eqpos+1));//the value
		} 
		else 
		{
			unescape_url(postvars[i*2+1] = strdup(""));//no value
		}
		unescape_url(postvars[i*2]= strdup(pairlist[i]));// the key
	}
	postvars[paircount*2] = 0;

	//for(i=0;pairlist[i];i++)
	//	DELETE(pairlist[i]);
	DELETE(pairlist);
	DELETE(postinput);

	return postvars;
}

//��ȡ/var Ŀ¼�ռ��С
static off_t list(char *name) 
{ 
	char		pn[255]; 
	DIR			*dp; 
	off_t		f_size,d_size; 
	int			i; 
	struct stat sbuf; 
	struct direct *dir;

	f_size = 0; 

	for (i = 0; i <= 1; i++) 
	{ 
		if ((dp = opendir(name))== NULL) 
		{ 
			return -1; 
		} 
		while ((dir = readdir(dp)) != NULL) 
		{ 
			if(dir->d_ino == 0) 
				continue; 
			strlcpy(pn, name,sizeof(pn)); 
			strlcat(pn, "/",sizeof(pn)); 
			strlcat(pn, dir->d_name,sizeof(pn)); 
			if (lstat(pn, &sbuf) < 0) 
			{ 
				return -1; 
			} 
			//�ж϶������ļ��Ƿ���һ��Ŀ¼��������������(sbuf.st_mode&S_IFMT)==S_IFDIRʵ�֡�
			//�����������������˳��������ӡ�Ŀ¼�������һ��Ŀ¼���Ա��������ѭ���� 
			if (((sbuf.st_mode & S_IFMT) != S_IFLNK) &&
				((sbuf.st_mode & S_IFMT) == S_IFDIR) &&
				(strcmp(dir->d_name,".") != 0) &&
				(strcmp(dir->d_name,"..") != 0)) 
			{ 
				//����ǵ�2��forѭ����ݹ����list�������г���Ӧ����Ŀ¼��
				//ͬʱ�ۼ�Ŀ¼��ռ���̿ռ�Ĵ�С�� 
				if (i==1) 
				{ 
					d_size=list(pn); 
					f_size=f_size + d_size; 
				} 
			} 
			else 
			{ 
				if (i==0) 
				{ 
					f_size=f_size + sbuf.st_size; 
					if((strcmp(dir->d_name,".") != 0) &&
						(strcmp(dir->d_name,"..") != 0))
					{
						//printf("*********%s\n",pn); 
					}
				} 

			} 

		} 

		closedir(dp); 
	} 
	return f_size; 
}

static off_t GetListName(char *name) 
{ 
	char		pn[255]; 
	DIR			*dp; 
	off_t		f_size,d_size; 
	int			i; 
	struct stat sbuf; 
	struct direct *dir;

	f_size = 0; 

	for (i = 0; i <= 1; i++) 
	{ 
		if ((dp = opendir(name))== NULL) 
		{ 
			return -1; 
		} 
		while ((dir = readdir(dp)) != NULL) 
		{ 
			if(dir->d_ino == 0) 
				continue; 
			strlcpy(pn, name,sizeof(pn)); 
			strlcat(pn, "/",sizeof(pn)); 
			strlcat(pn, dir->d_name,sizeof(pn)); 
			if (lstat(pn, &sbuf) < 0) 
			{ 
				return -1; 
			} 
			//�ж϶������ļ��Ƿ���һ��Ŀ¼��������������(sbuf.st_mode&S_IFMT)==S_IFDIRʵ�֡�
			//�����������������˳��������ӡ�Ŀ¼�������һ��Ŀ¼���Ա��������ѭ���� 
			if (((sbuf.st_mode & S_IFMT) != S_IFLNK) &&
				((sbuf.st_mode & S_IFMT) == S_IFDIR) &&
				(strcmp(dir->d_name,".") != 0) &&
				(strcmp(dir->d_name,"..") != 0)) 
			{ 
				//����ǵ�2��forѭ����ݹ����list�������г���Ӧ����Ŀ¼��
				//ͬʱ�ۼ�Ŀ¼��ռ���̿ռ�Ĵ�С�� 
				if (i==1) 
				{ 
					d_size=GetListName(pn); 
					f_size=f_size + d_size; 
				} 
			} 
			else 
			{ 
				if (i==0) 
				{ 
					f_size=f_size + sbuf.st_size; 
					if((strcmp(dir->d_name,".") != 0) &&
						(strcmp(dir->d_name,"..") != 0))
					{
						//printf("************%s\n",pn); 
						strlcat(szPathList, pn,sizeof(szPathList));
						strlcat(szPathList,";\n",sizeof(szPathList));
					}
				} 

			} 

		} 

		closedir(dp); 
	} 
	return f_size; 
}


char *removeModelWhiteSpace(IN char *pszBuf)
{
    char    *p = pszBuf;
    char    *pFirstNonSpace = pszBuf, *pLastWhiteSpace = NULL;
	BOOL    bFound = FALSE;  /* first non-space character found flag */

    while (*p)
    {
        if (IS_WHITE_SPACE(*p))
        {
            if(pLastWhiteSpace == NULL)
                pLastWhiteSpace = p;
        }
        else 
        {
            if (!bFound)
			{
                pFirstNonSpace = p;
				bFound = TRUE;
			}

            pLastWhiteSpace = NULL;
        }

        p++;
    }

    if (pLastWhiteSpace != NULL)
        *pLastWhiteSpace = 0;

    return pFirstNonSpace;
}

/*
//get model number
static int getModelNum()
{

	int ret = -1;
	char strLine[MAX_MODEL_LINE_LEN];//Max num of each line is 1024
	char *p,*p1;
	FILE *fp = fopen(CONFIG_APP_MODEL_DATA,"r");
	if (fp == NULL)
	{
		printf("open %s failed\n",CONFIG_APP_MODEL_DATA);
		return ret;
	}

	while (!feof(fp)) 
	{
		fgets(strLine,MAX_MODEL_LINE_LEN,fp);
		if(strnicmp(strLine , APP_MODEL_NUM_STR, (size_t)(strlen(APP_MODEL_NUM_STR))) == 0)//match the modelNum title
		{
			break;
		}
	}
	if(feof(fp))
	{
		fclose(fp);
		return ret;
	}

	fgets(strLine,MAX_MODEL_LINE_LEN,fp);
	p = strLine;
	while((!feof(fp)) && p[0] != SECT_PREFIX)
	{
		if(END_OF_LINE(*p) )			// is CR or LF 
		{
			fgets(strLine,MAX_MODEL_LINE_LEN,fp);
			p = strLine;
		}
		else if((p1 = strstr(p, COMMENT_FLAG)) != NULL)// is comment
		{
			*p1 = 0;
		}
	//though atoi can process the tab and space, except the line 
		else if(IS_WHITE_SPACE(*p))		// is TAB or SPACE
		{
			p++;
		}
		else 
		{
			ret = atoi(p);
			break;
		}
	}
	fclose(fp);
	return ret; 
}
*/
static int getModelValue( IN OUT char **modelValue)
{
	int i;
	int  ModelRealNum = 0;
	char strLine[MAX_MODEL_LINE_LEN];//Max num of each line is 64

	char *p, *p1=NULL;
	FILE *fp = fopen(CONFIG_APP_MODEL_DATA,"r");
	if (fp == NULL)
	{
		printf("open %s failed\n",CONFIG_APP_MODEL_DATA);
		return -1;
	}
	
	while (!feof(fp)) 
	{
		fgets(strLine,MAX_MODEL_LINE_LEN,fp);
		if(strnicmp(strLine , APP_MODEL_INFO_STR, (size_t)(strlen(APP_MODEL_INFO_STR))) == 0)//match the modelNum title
		{
			break;
		}
	}
	if(feof(fp))
	{
		fclose(fp);
		return -1;
	}


	while(!feof(fp))
	{
		fgets(strLine,MAX_MODEL_LINE_LEN,fp);
		p = strLine;
		if(p[0] == SECT_PREFIX)
		{
			break;
		}
		if((p1 = strstr(p, COMMENT_FLAG)) != NULL)// is comment
		{
			*p1 = 0;
		}
		
		if(IS_WHITE_SPACE(*p))		// is TAB or SPACE
		{
			p = removeModelWhiteSpace(p);
		}

		if(END_OF_LINE(*p) )			// is CR or LF 
		{
			continue;
		}

	//though atoi can process the tab and space, except the line 

		for(i = strlen(p) - 1; i > 0; i--)
		{
			if(p[i] == CR || p[i] == LF || p[i] == SPACE)
			{
				p[i]='\0';
			}
			else 
				break;
		}
		modelValue[ModelRealNum] = NEW(char, strlen(p));
		snprintf(modelValue[ModelRealNum], MAX_MODEL_LINE_LEN, "%s",p);

		ModelRealNum++;
		if(ModelRealNum>1)
		{
			if(strnicmp(modelValue[ModelRealNum-2] , modelValue[ModelRealNum-1], (size_t)(MAX_MODEL_NUM)) == 0)
				ModelRealNum--;
		}
	}

return ModelRealNum;
}

static int verifySerialNum(int modelNum, char **modelValue)
{
	char strLine[MAX_MODEL_LINE_LEN];
	char *p = NULL;
	int i;
	FILE *fp = fopen(CONFIG_SERIAL_NUM_DATA,"r");
	if (fp == NULL)
	{
		printf("open %s failed\n",CONFIG_SERIAL_NUM_DATA);
		return 0;
	}

	while (!feof(fp)) 
	{
		fgets(strLine,MAX_MODEL_LINE_LEN,fp);
		p = strstr(strLine, COMMENT_FLAG);
		if(p != NULL)
		{
			*p = 0;
		}
		for(i = 0; i < modelNum; i++)
		{
			if(strstr(strLine , modelValue[i])!= NULL)//match the modelNum title
			{
				return 1;
				break;
			}
		}
	}
	return 0;
}


BOOL CGI_find_File(char * cDireBuff, char * cFileName)
{
	DIR *		dir;
	struct		dirent * ptr;
	char		*cFileTemp;
	dir = opendir(cDireBuff);
	if(dir!=NULL)
	{
		while((ptr = readdir(dir)) != NULL)
		{
			cFileTemp = ptr->d_name; 

			if(strncmp(cFileName,cFileTemp,strlen(cFileName))==0)
			{
				closedir(dir);
				return TRUE;
			}
		}
		closedir(dir);
	}
	return FALSE;
}

/*==========================================================================*
* FUNCTION :  ParsedAPPModelTableProc
* PURPOSE  :  Parse every setting signal in <AppDownloadFlag.cfg>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  szBuf: the string in one line describe one setting signal
pStructData:the struct describe one setting signal
* RETURN   :  int: success:1 or not:0
* COMMENTS :  
* CREATOR  :  SongXu               DATE: 2016-08-30
*==========================================================================*/


static int ParsedAPPModelToSerial( char * szFileName)
{
	char szUnzip[128];
	char *pField = NULL;
	int i,ret = 0;
	char *appPackModel[MAX_MODEL_NUM];
	char serialNum[MAX_SERIAL_LEN];
	int paraNumber = 0, paraRealNumber = 0 ;
//get the number of models
/*
	paraNumber = getModelNum();
	if(paraNumber <= 0 )
	{
		return -1;
	}
*/
	//get model value in package
	if(szFileName)
	{
		//remove the config first avoid the original file existed
		//sprintf(szUnzip, "rm -f %s", CONFIG_APP_MODEL_DATA);//, szFileName);
		//system(szUnzip);
		//sprintf(szUnzip, "tar zxf %s/%s %s", NEW_DOWNLOAD_DIRECTORY, szFileName,CONFIG_APP_MODEL_DATA);

		//if(system(szUnzip) == -1)
		//{
		//	return 0;
		//}
		paraRealNumber= getModelValue( &appPackModel[0]);
		
		if(paraRealNumber <= 0 )
		{
			return 0;
		}

		for(i = 0; i < paraRealNumber; i++)
		{
			if(strnicmp(appPackModel[i] , APP_MODEL_ALL, (size_t)(strlen(APP_MODEL_ALL))) == 0) //All model can upload
			{
				return 1;
			}
		}
		//get serial message in /home/SerialNum.log
		ret = verifySerialNum(paraRealNumber, appPackModel);
	}

	return ret;
}

/*==========================================================================*
* FUNCTION :  ParsedAPPPackageMode
* PURPOSE  :  judge the app package whether is apply for NCU.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  BOOL: success:TRUE or not:FALSE
* COMMENTS :  
* CREATOR  :  SongXu               DATE: 2017-01-20
*==========================================================================*/

/*
static BOOL ParsedAPPPackageMode( char * szFileName)
{
#define CONFIG_APP_MODEL_DIR_UP		"config/"
#define CONFIG_APP_MODEL_DIR		"config/run/"
#define CONFIG_APP_MODEL_NAME		"AppDownloadFlag.cfg"

#define WEBUSER_APP_MODEL_DIR		"config/private/"
#define WEBUSER_APP_MODEL_NAME		"web_user"
	char szUnzip[128];
	char *pField = NULL;
	int i,ret = 0;

	BOOL bCondition1 = FALSE;
	BOOL bCondition2 = FALSE;


	//get model value in package
	if(szFileName)
	{
		sprintf(szUnzip, "tar zxf %s/%s %s%s", NEW_DOWNLOAD_DIRECTORY, szFileName,CONFIG_APP_MODEL_DIR,CONFIG_APP_MODEL_NAME);
		system(szUnzip);



		if(!CGI_find_File(CONFIG_APP_MODEL_DIR,CONFIG_APP_MODEL_NAME))
		{
			bCondition1 = TRUE;
		}
		else
		{
			if(ParsedAPPModelToSerial(szFileName) == TRUE)
			{
				bCondition1 = TRUE;
			}
		}

		if(bCondition1 == TRUE)
		{
			sprintf(szUnzip, "tar zxf %s/%s %s%s", NEW_DOWNLOAD_DIRECTORY, szFileName,WEBUSER_APP_MODEL_DIR,WEBUSER_APP_MODEL_NAME);
			system(szUnzip);
			if(CGI_find_File(WEBUSER_APP_MODEL_DIR,WEBUSER_APP_MODEL_NAME))
			{
				bCondition2 = TRUE;
			}
		}
		sprintf(szUnzip, "rm -rf %s", CONFIG_APP_MODEL_DIR_UP);
		system(szUnzip);	
	}


	return ( bCondition1 && bCondition2 );
}
*/


/*  Fengel, mini and ESNA can not download EMEA SW
static BOOL ParsedAPPPackageMode( char * szFileName)
{
#define CONFIG_APP_MODEL_DIR_UP		"config/"
#define CONFIG_APP_MODEL_DIR		"config/run/"
#define CONFIG_APP_MODEL_NAME		"AppDownloadFlag.cfg"

#define WEBUSER_APP_MODEL_DIR		"service/"
#define WEBUSER_APP_MODEL_NAME		"web_user.so"
	char szUnzip[128];
	char *pField = NULL;
	int i,ret = 0;

	BOOL bCondition1 = FALSE;
	BOOL bCondition2 = FALSE;


	//get model value in package
	if(szFileName)
	{
		sprintf(szUnzip, "tar zxf %s/%s %s%s", NEW_DOWNLOAD_DIRECTORY, szFileName,CONFIG_APP_MODEL_DIR,CONFIG_APP_MODEL_NAME);
		system(szUnzip);

		sprintf(szUnzip, "tar zxf %s/%s %s%s", NEW_DOWNLOAD_DIRECTORY, szFileName,WEBUSER_APP_MODEL_DIR,WEBUSER_APP_MODEL_NAME);
		system(szUnzip);

		if(CGI_find_File(CONFIG_APP_MODEL_DIR,CONFIG_APP_MODEL_NAME))
		{
			bCondition1 = TRUE;
		}

		if(CGI_find_File(WEBUSER_APP_MODEL_DIR,WEBUSER_APP_MODEL_NAME))
		{
			bCondition2 = TRUE;
		}

		sprintf(szUnzip, "rm -rf %s", CONFIG_APP_MODEL_DIR_UP);
		system(szUnzip);	
		sprintf(szUnzip, "rm -rf %s", WEBUSER_APP_MODEL_DIR);
		system(szUnzip);
	}


	return ( bCondition1 && bCondition2 );
}
*/

//Fengel, MiniNCU can not upload NCU SW and NCU can not upload MiniNCU SW
static BOOL ParsedAPPPackageMode( char * szFileName)
{
#define CONFIG_APP_MODEL_DIR_UP		"config/"
#define CONFIG_APP_MODEL_DIR		"config/run/"
#define CONFIG_APP_MODEL_NAME		"AppDownloadFlag.cfg"

#define WEBUSER_APP_MODEL_DIR		"config/private/"
#define WEBUSER_APP_MODEL_NAME		"web_user"

#define APP_PACKAGE_SERVICE_DIR		"service/"
#define DIF_MININCU_AND_NCU			"qt_fifo.so"

	char szUnzip[128];
	char *pField = NULL;
	int i,ret = 0;

	BOOL bCondition1 = FALSE;
	BOOL bCondition2 = FALSE;
	BOOL bCondition3 = FALSE;
	BOOL bIsNCU_SW = FALSE;

	//get model value in package
	if(szFileName)
	{
		sprintf(szUnzip, "tar zxf %s/%s %s%s", NEW_DOWNLOAD_DIRECTORY, szFileName,CONFIG_APP_MODEL_DIR,CONFIG_APP_MODEL_NAME);
		system(szUnzip);



		if(!CGI_find_File(CONFIG_APP_MODEL_DIR,CONFIG_APP_MODEL_NAME))
		{
			bCondition1 = TRUE;
		}
		else
		{
			if(ParsedAPPModelToSerial(szFileName) == TRUE)
			{
				bCondition1 = TRUE;
			}
		}

		if(bCondition1 == TRUE)
		{
			sprintf(szUnzip, "tar zxf %s/%s %s%s", NEW_DOWNLOAD_DIRECTORY, szFileName,WEBUSER_APP_MODEL_DIR,WEBUSER_APP_MODEL_NAME);
			system(szUnzip);
			if(CGI_find_File(WEBUSER_APP_MODEL_DIR,WEBUSER_APP_MODEL_NAME))
			{
				bCondition2 = TRUE;
			}
		}
		sprintf(szUnzip, "rm -rf %s", CONFIG_APP_MODEL_DIR_UP);
		system(szUnzip);	

		if(bCondition2 == TRUE)
		{
			sprintf(szUnzip, "tar zxf %s/%s %s%s", NEW_DOWNLOAD_DIRECTORY, szFileName,APP_PACKAGE_SERVICE_DIR,DIF_MININCU_AND_NCU);
			system(szUnzip);

			if(CGI_find_File(APP_PACKAGE_SERVICE_DIR,DIF_MININCU_AND_NCU))
			{
				bIsNCU_SW = TRUE;
			}
			else
			{
				bIsNCU_SW = FALSE;
			}
		}
		
		sprintf(szUnzip, "rm -rf %s", APP_PACKAGE_SERVICE_DIR);
		system(szUnzip);
		
	}

#ifdef _CODE_FOR_MINI
				if( bIsNCU_SW == TRUE )
				{
					bCondition3 = FALSE;
				}
				else
				{
					bCondition3 = TRUE;
				}
#else
				if( bIsNCU_SW == TRUE )
				{
					bCondition3 = TRUE;
				}
				else
				{
					bCondition3 = FALSE;
				}
#endif

	return ( bCondition1 && bCondition2 && bCondition3 );
}

/*
//Fengel, MiniNCU can not upload NCU SW and NCU can not upload MiniNCU SW
static BOOL ParsedAPPPackageMode( char * szFileName)
{
#define APP_PACKAGE_SERVICE_DIR		"service/"
#define DIF_MININCU_AND_NCU			"qt_fifo.so"
	char szUnzip[128];
	char *pField = NULL;
	int i,ret = 0;

	BOOL bIsNCU_SW = FALSE;


	if(szFileName)
	{
		sprintf(szUnzip, "tar zxf %s/%s %s%s", NEW_DOWNLOAD_DIRECTORY, szFileName,APP_PACKAGE_SERVICE_DIR,DIF_MININCU_AND_NCU);
		system(szUnzip);

		if(CGI_find_File(APP_PACKAGE_SERVICE_DIR,DIF_MININCU_AND_NCU))
		{
			bIsNCU_SW = TRUE;
		}
		else
		{
			bIsNCU_SW = FALSE;
		}

		sprintf(szUnzip, "rm -rf %s", APP_PACKAGE_SERVICE_DIR);
		system(szUnzip);
	}


#ifdef _CODE_FOR_MINI
				if( bIsNCU_SW == TRUE )
				{
					return FALSE;
				}
				else
				{
					return TRUE;
				}
#else
				if( bIsNCU_SW == TRUE )
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
#endif
}
*/

static int Web_Replace_File(IN char *szFileName)
{
	char szUnzip[128];
	char	*pSearchValue = NULL;
	char	szFileModifyTime[128];
	char	szTarDetect[128];

#define WEB_SOLUTION_FILE_NAME			"MonitoringSolution.cfg"
#define	WEB_CONFIG_FILE_NAME			"download_app_cfg.tar"
#define WEB_MAIN_CONFIG_FILE			"config/run/MainConfig.run"

	/*FILE *fp4 = fopen("/var/4.htm","w");
	fwrite(szFileName,strlen(szFileName), 1, fp4);
	fclose(fp4);*/


	if(szFileName)
	{

		sprintf(szFileModifyTime, "/usb/%s", szFileName);
		Web_FLE_ModifyTime(szFileModifyTime);


		while((pSearchValue = strrchr(szFileName, '\n')) != NULL )
		{
			*pSearchValue = 32;
		}

		while((pSearchValue = strrchr(szFileName, '\r')) != NULL )
		{
			*pSearchValue = 32;
		}

		/*
		char szTarDetect1[128];
		FILE *fp5 = fopen("/var/321.htm","w");
		sprintf(szTarDetect1, "%s--1",szFileName);
		fwrite(szTarDetect1,strlen(szTarDetect1), 1,fp5);
		fclose(fp5);
		*/
		
		if((pSearchValue = strstr(szFileName, WEB_CONFIG_FILE_NAME)) != NULL)
		{
			//specilize for cfg package in 1.1.70, extract the mainconfig.run only
			sprintf(szTarDetect, "tar -xvf %s/%s %s", NEW_DOWNLOAD_DIRECTORY, szFileName,WEB_MAIN_CONFIG_FILE);
			if(system(szTarDetect) != -1)
			{
				sprintf(szUnzip, "mv ./%s /app/%s", WEB_MAIN_CONFIG_FILE, WEB_MAIN_CONFIG_FILE);//, szFileName);
			}
			else
			{
				return FALSE;
			}
		}
		else if((pSearchValue = strstr(szFileName, ".tar.gz")) != NULL)
		//if((pSearchValue = strstr(szFileName, ".tar.gz")) != NULL)
		{
			//judge the app package whether is for NCU
			if(ParsedAPPPackageMode(szFileName) != TRUE)
			{
				return FALSE;
			}
			sprintf(szUnzip, "tar -xzf %s/%s -C /app", NEW_DOWNLOAD_DIRECTORY, szFileName);//, szFileName);
		}
		else if((pSearchValue = strstr(szFileName, ".tar")) != NULL)
		{
			sprintf(szUnzip, "tar -xf %s/%s -C /app", NEW_DOWNLOAD_DIRECTORY, szFileName);//, szFileName);
		}
		else if((pSearchValue = strstr(szFileName, WEB_SOLUTION_FILE_NAME)) != NULL)
		{
			sprintf(szUnzip, "mv -f %s/%s /app/config/solution/%s", NEW_DOWNLOAD_DIRECTORY,szFileName, WEB_SOLUTION_FILE_NAME);
		}
		else if((pSearchValue = strstr(szFileName, WEB_SETTINGPARAM_FILE_NAME)) != NULL)
		{
			sprintf(szUnzip, "mv -f %s/%s /app/config/run/%s",NEW_DOWNLOAD_DIRECTORY, szFileName, WEB_SETTINGPARAM_FILE_NAME);
		}
		else
		{
			return FALSE;
		}

		if(system(szUnzip) != -1)
		{
		    /*FILE *fp29;
		    if((fp29 = fopen("/var/file29.html","wb")) != NULL)
		    {
			fwrite(szFileName,strlen(szFileName), 1,fp29);
			fclose(fp29);
		    }*/
			return TRUE;
		}
	}

	return FALSE;
}


static void Web_Remove_File(IN char *szFileName)
{
	char szUnzip[128];
	char *pSearchValue = NULL;

	if(szFileName && (strcmp(szFileName, WEB_ZIP_CFG_LANG) != 0 || strcmp(szFileName, WEB_ZIP_CFG) != 0))// || strcmp(szFileName, WEB_SETTINGPARAM_FILE_NAME) != 0
	{

		while((pSearchValue = strrchr(szFileName, '\n')) != NULL )
		{
			*pSearchValue = 32;
		}

		while((pSearchValue = strrchr(szFileName, '\r')) != NULL )
		{
			*pSearchValue = 32;
		}
		sprintf(szUnzip, "rm -rf %s/%s", NEW_DOWNLOAD_DIRECTORY, szFileName);
		/*FILE *fp31;
		if((fp31 = fopen("/var/file31.html","a+")) != NULL)
		{
		    fwrite(szUnzip, strlen(szUnzip), 1, fp31);
		    fclose(fp31);
		}*/
		system(szUnzip);
	}
}


static void Web_Zip_File(void)
{

#define WEB_CFG_PATH				"config/solution/ config/standard/ config/private/ config/run/"
#define WEB_LANG_PATH				"config/lang"
	//#define WEB_SOLUTION_FILE_PATH		"cp -rf /app/config/solution/MonitoringSolution.cfg /var/"

	char szZip[256];

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_CFG_PATH,WEB_ZIP_CFG_LANG);
	sprintf(szZip, "cd /app;tar -cf /var/download/%s %s", WEB_ZIP_CFG, WEB_CFG_PATH);
	system(szZip);


	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "cd /app;tar -cf /var/download/%s %s", WEB_ZIP_CFG_LANG, WEB_LANG_PATH);
	system(szZip);

	//system(WEB_SOLUTION_FILE_PATH);

}

static int Web_FLE_ModifyTime(IN char *szFileName)
{
	time_t				now;
	struct utimbuf		 FileTime;


	time(&now);
	FileTime.actime = now - 100;
	FileTime.modtime = now -100;

	if(szFileName != NULL)
	{
		utime(szFileName, &FileTime);
	}
}

static void Web_Del_Zip_File(void)
{


	char szZip[256];


	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_CFG_PATH,WEB_ZIP_CFG_LANG);
	sprintf(szZip, "rm -f /var/download/%s", WEB_ZIP_CFG);
	system(szZip);


	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/download/%s", WEB_ZIP_CFG_LANG);
	system(szZip);

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/eng/%s", WEB_PLC_PAGE);
	system(szZip);

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/loc/%s", WEB_PLC_PAGE);
	system(szZip);

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/download/%s", WEB_TAR_GZ);
	system(szZip);

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/download/%s", WEB_TAR);
	system(szZip);


	//system(WEB_SOLUTION_FILE_PATH);

}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_pub.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-10-11 14:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "cgi_pub.h"
#include "dirent.h"

 
 
 
#define SESSION_LIVE_TIME		60*60
#define SESSION_CLEAN_TIME		90//60*60*24

#define REMOTE_ADDR				getenv("REMOTE_ADDR")
#define HTTP_COOKIE				getenv("HTTP_COOKIE")

#define REMOTE_USER				getenv("REMOTE_USER")




#define MAX_COOKIE_LEN			256

char *sess_user_name;
char *sess_user_pwd;
static int GetCookie(IN char *szCookie, IN char *szName, OUT char *szHashKey);
static char *GetLocalIP(void);

//static char pCommandType[3][32] = {"Realtime","Config","Query"};

/*==========================================================================*
 * FUNCTION :  PostPage
 * PURPOSE  :  output the html to the client
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN char * pbuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
 void PostPage(IN char * pbuf)
 {
	 fprintf(stdout, pbuf );

 }
/*==========================================================================*
 * FUNCTION :  ReplaceString
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN OUT char *ppsrc
				IN char *strOld
				IN char *strNew
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
int ReplaceString( char **ppsrc, const char *strOld, char *strNew )
{
    char *src = *ppsrc;
    char *p = strstr( src, strOld );
    int nSrc;
    int nNew, nOld, nHeadLen, nTailLen;
    int nNewStrLen;
    char *pNewBuffer = NULL;
    
    if( p == NULL  )
    {
        return -1;
    }

    nNew = strlen( strNew );       /* the length of new string */
    nOld = strlen( strOld );
    nSrc = strlen(src);
    nHeadLen = p - src;
    nNewStrLen = nSrc + nNew - nOld;
    
    if( nSrc < nNewStrLen )
    {
        /* need allocate a new memory   */
        pNewBuffer = NEW( char, (nNewStrLen + 1) );
        if( pNewBuffer == NULL )
        {
            //TRACE("[String_ Replace] -- allocate memory for new string fail." );
            return -1;
        }
    }

    /* the dst addr to copy or to modify*/
    p = (pNewBuffer != NULL) ? pNewBuffer : src;

    /* copy fore string to dest    */
    if( p != src )
        memmove( p, src, (size_t)nHeadLen );

    /* copy the new string strNew to dest to replace strOld */
    p += nHeadLen;
    memmove( p, strNew, (size_t)nNew );

    /* copy the tail string to dest */
    p   += nNew;
    nTailLen = nSrc - nOld - nHeadLen + 1; // include '\0'
    memmove( p, src+nHeadLen+nOld, (size_t)nTailLen );

    if( pNewBuffer != NULL )
    {
		DELETE( src );      /* release the old memory   */
         
		*ppsrc  = pNewBuffer;
    }

    return nHeadLen+nNew;   /* the next pos use to do replace*/

}


/*==========================================================================*
 * FUNCTION :  MakeModiTime
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  OUT char * strModiTime
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
/*
void MakeModiTime(OUT char * strModiTime,int nLen)
{
	time_t  ltime;
    struct tm *gmt; 

    time( &ltime );
    gmt = gmtime( &ltime );
    strftime( strModiTime, 
        (size_t)nLen, 
        "%a, %d %b %Y %H:%M:%S GMT", //Fri, 10 May 2002 07:27:38 GMT
        gmt );
}
*/	
/*==========================================================================*
 * FUNCTION :  LoadHtmlFile
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
int LoadHtmlFile(IN const char *szFile, OUT char **pbuf)
{
	FILE *fp = fopen( szFile, "rb" );
    int len;
    
    if( fp == NULL )
    {
        printf( "File %s not found\n", szFile );
        return 0;
    }

    len = (int)GetFileLength(fp);
	//len = (int)FileLength(fp);
    //*pbuf = (char *)malloc( (len+1) * sizeof(char));
	*pbuf = NEW(char, len + 1);
	
    if( *pbuf == NULL )
    {
        printf( "Out of memory on loading file %s", szFile );
        fclose( fp );
        return 0;
    }

    fread( *pbuf, (size_t)1, (size_t)len, fp );

    (*pbuf)[len] = 0; // end flag
    fclose( fp );

    return len;
}


/*==========================================================================*
 * FUNCTION :  set_session
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *name
				IN char *pwd
 * RETURN   :	char *
 * COMMENTS :   create the session file when the client connected to the server first time.
				the content include :
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 16:20
 *==========================================================================*/
char *set_session(IN char *name,IN char *pwd,IN char *strAuthority)
{
	char			str_now[16];
	char			hash_key[17];
	char			*session_id = NULL;
	time_t			now;
	char			*pszReturnCookie = NULL;

	FILE			*sf = NULL;
	char			sfp[32];

	int				i,temp,r;

	time(&now);
	/***  clean time out session file*/
	clean_session_file();

	/*** get str_now*/
	sprintf(str_now,"%15ld",now);
	
	/*** get random hash_key*/
	srand((unsigned int)now);  
	r = rand();
	for(i = 0; i < 16; i++)
	{
		srand((unsigned int)r);
		r = rand();
		hash_key[i] = r%26 + 'a';
	}
	hash_key[16] = '\0';
	  
	/*** get more random session_id;*/
	temp = rand();
	srand((unsigned int)temp);
	r = rand();

	session_id = NEW(char, 17);

	if(session_id == NULL)
	{
		return NULL;
	}

	for(i = 0; i < 16; i++)
	{
		r = urandom();
		session_id[i] = r%26 + 'A';
	} 
	session_id[16] = '\0';
	 

	/*** create session file*/
	strlcpy(sfp,SESSION_DEFAULT_DIR,sizeof(sfp));
	strlcat(sfp,session_id,sizeof(sfp));

	sf = fopen(sfp,"w"); 
	chmod(sfp,06777);

	if( sf == NULL )
	{
		AppLogOut(CGI_APP_LOG_APPPUB_NAME,APP_LOG_WARNING,"can't creat session file"); 
		return NULL;
	}
 

	/*** fputs session file */
	fputs(str_now,sf);
	fputs("\n",sf);
	
	fputs(hash_key,sf);
	fputs("\n",sf);
	if(REMOTE_ADDR != NULL)
	{
		fputs(REMOTE_ADDR,sf);
	}
	fputs("\n",sf);
	fputs(name,sf);    //sess_user_name
	fputs("\n",sf);
	
	fputs(strAuthority,sf);	//
	fputs("\n",sf);
	fputs(pwd,sf);     // sess_user_pwd_
	fputs("\n",sf);  
	fclose(sf);

	/***  set cookie */ 

	pszReturnCookie = NEW(char, MAX_COOKIE_LEN);
	if(pszReturnCookie == NULL)
	{
		return NULL;
	}
	else
	{
		//sprintf(pszReturnCookie, "Set-cookie:hash_key=%s\nSet-cookie:session_ID=%s\nSet-cookie:username=%s\nSet-cookie:password=%s\n",hash_key,session_id,name,pwd);
		//char   *szLocalIP = GetLocalIP();
	    sprintf(pszReturnCookie, "hash_key=%s,sessionId=%s,nAuthority=%s,clientIP=%s,name=%s",hash_key,session_id,strAuthority,REMOTE_ADDR,name);//GetLocalIP());
		//printf("Set-Cookie:hash_key=%s;session_ID=%s;username=%s;password=%s\n;",hash_key,session_id,name,pwd);
	}

	if(session_id != NULL)
	{
		DELETE(session_id);
		session_id = NULL;
	}
	return pszReturnCookie;
}
/*==========================================================================*
 * FUNCTION :  start_session
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : called when the client was operated  
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 16:20
 *==========================================================================*/
//const char pReturn[4][8] ={SESSION_NO_THIS_FILE,SESSION_OVER_TIME,SESSION_NO_HASHKEY,SESSION_FAULT_IP};
#define SESSION_NO_THIS_FILE			-1
#define SESSION_OVER_TIME				-2
#define SESSION_NO_HASHKEY				-3	
#define SESSION_FAULT_IP				-4

int start_session(IN const char *sessionId)
{
	int nI,j,k;

	const char *session_id = sessionId;
	FILE *sf;
	char sfp[32];
	time_t now;
	//int    r;

	char buffer[512];
	char temp[64];
	char str_time[16];
	char str_hash_key[20];
 
	//char str_client_ip[20];
	char str_client_ip[64];
	
	char *sess_user_authority;

	char *str_array[7];
	 
	//sess_user_name = (char*)malloc(32*sizeof(char));
	//sess_user_pwd  = (char*)malloc(32*sizeof(char));
	sess_user_name = NEW(char, 32);
	if(sess_user_name == NULL)
	{
		return 0;
	}
	sess_user_pwd = NEW(char, 32);
	
	if(sess_user_pwd == NULL)
	{
		DELETE(sess_user_name);
		sess_user_name = NULL;
		return 0;
	}

	sess_user_authority = NEW(char,10);
	
	if(sess_user_authority == NULL)
	{
		DELETE(sess_user_name);
		sess_user_name = NULL;

		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;
		return 0;
	}

	str_array[0] = str_time;
	str_array[1] = str_hash_key;
	str_array[2] = str_client_ip;
	str_array[3] = sess_user_name;
	str_array[4] = sess_user_authority;
	str_array[5] = sess_user_pwd;
	
	//session_id = sessionId;
	/*** open session file */
	strlcpy(sfp,SESSION_DEFAULT_DIR,sizeof(sfp)); 
	strlcat(sfp,session_id,sizeof(sfp));

	sf = fopen(sfp,"rb+");
	if(  sf == NULL )
            /** can't open session file,maybe session has time out **/ 
	{
		//print_session_error("1");
		DELETE(sess_user_name);
		sess_user_name = NULL;

		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return SESSION_NO_THIS_FILE;
		
	}
	
	/*** read session var*/
	memset(buffer,0,512);
	fread(buffer,1,512,sf);
	for(nI=0,j=0,k=0;k<6 && nI<(int)strlen(buffer);nI++)
	{
		if( buffer[nI] == '\n'  )
		{
			temp[j] = '\0';
			strlcpy(str_array[k],temp,64);
			j = 0;
			k ++;
		}
		else
		{
			if(j<63)
			{
				temp[j++] = buffer[nI];
			}
		}
	}

	/*** check active time*/ 
	time(&now);
	if( now - atol(str_time) > (long)SESSION_LIVE_TIME )
	{
		//print_session_error("2"); 
		DELETE(sess_user_name);
		sess_user_name = NULL;

	
		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return SESSION_OVER_TIME;
	} 

	/*** compare client hash_key to session hash_key*/
	char		*pszCookie = NULL;
	pszCookie = NEW(char, 32);
	if(pszCookie == NULL)
	{
		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return 0;
	}
	/*get cookie of hash key*/
	//char	*pStartValue = HTTP_COOKIE + 20;
	//char	*pSearchValue = strchr(HTTP_COOKIE,59);  /*get ';'*/
	//int nLen = pSearchValue - pStartValue;
	//strncpyz(pszCookie,pStartValue, nLen + 1 );
	
	GetCookie(HTTP_COOKIE,"hash_key", pszCookie);
	
	//TRACE("pszCookie : %s\n", pszCookie);
	if( HTTP_COOKIE == "" || strcmp( Web_RemoveWhiteSpace(pszCookie) , str_hash_key ) != 0 )
	{
		//print_session_error("3");
		//return SESSION_NO_HASHKEY;
		printf("\n");
		TRACE("HTTP_COOKIE : %s\n", HTTP_COOKIE);
		TRACE("pszCookie :%s\n", pszCookie);
		TRACE("str_hash_key :%s", str_hash_key);
		DELETE(pszCookie);
		pszCookie = NULL;
		return SESSION_NO_HASHKEY;
	} 
	DELETE(pszCookie);
	pszCookie = NULL;
	
 
	/*return the authority type  return NO_AUTHORITY*/
 
	if( strcmp( REMOTE_ADDR, str_client_ip ) != 0 )
	{
		//print_session_error("4");
		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return SESSION_FAULT_IP;
	}
			
	
	
	/******************/

	/*** refresh session active time */ 
	time(&now);
	sprintf(str_time,"%15ld\n",(long)now);
	fseek (sf,(long)0,SEEK_SET);
	fputs(str_time,sf);  

	/*** get new hash_key*/
	//srand((unsigned int)now);
	//r = (int)rand();
	//for(nI=0;nI<16;nI++)
	//{
	//	srand((unsigned int)r);
	//	r = (int)rand();
	//	str_hash_key[nI] = r % 26 + 'a';
	//}
	//str_hash_key[16] = '\n';
	//str_hash_key[17] = '\0';
 

	/*** refresh session hash_key*/
	//fseek(sf,11,SEEK_SET);
	//fputs(str_hash_key,sf);

	fclose(sf);
	
	int		iAuthority = atoi(sess_user_authority);

	DELETE(sess_user_authority);
	sess_user_authority = NULL;
	
	DELETE(sess_user_name);
	sess_user_name = NULL;
	
	DELETE(sess_user_pwd);
	sess_user_pwd = NULL;
	
	return  iAuthority;

}

char *getUserName(IN char *sessionId)
{
	int nI,j,k;
	const char *session_id = sessionId;
	FILE *sf;
	char sfp[32];

	char buffer[512];
	char temp[64];
	char str_time[16];
	char str_hash_key[20];
 
	char str_client_ip[64];
	
	char sess_user_authority[10];

	char *str_array[7];
	char *user_name, user_password[65];
	user_name = NEW(char, 65);
	if(user_name == NULL)
	{
		return NULL;
	}

	str_array[0] = str_time;
	str_array[1] = str_hash_key;
	str_array[2] = str_client_ip;
	str_array[3] = user_name;
	str_array[4] = sess_user_authority;
	str_array[5] = user_password;
	
	/*** open session file */
	strlcpy(sfp,SESSION_DEFAULT_DIR,sizeof(sfp)); 
	strlcat(sfp,session_id,sizeof(sfp));

	sf = fopen(sfp,"rb+");
	if(  sf == NULL )
            /** can't open session file,maybe session has time out **/ 
	{
		//print_session_error("1");
		DELETE(user_name);
		user_name = NULL;

		return NULL;
		
	}
	/*** read session var*/
	memset(buffer,0,512);
	fread(buffer,1,512,sf);
	for(nI=0,j=0,k=0;k<6 && nI<(int)strlen(buffer);nI++)
	{
		if( buffer[nI] == '\n'  )
		{
			temp[j] = '\0';
			strlcpy(str_array[k],temp,64);
			j = 0;
			k ++;
		}
		else
		{
			if(j<63)
			{
				temp[j++] = buffer[nI];
			}
		}
	}
	return user_name;
}

int start_sessionA(IN const char *sessionId, IN char **szUserName)
{
	int nI,j,k;

	const char *session_id = sessionId;
	FILE *sf;
	char sfp[32];
	time_t now;
	//int    r;

	char buffer[512];
	char temp[64];
	char str_time[16];
	char str_hash_key[20];
 
	//char str_client_ip[20];
	char str_client_ip[64];//support ipv4 and ipv6
	
	char *sess_user_authority;

	char *str_array[7];
	 
	sess_user_name = NEW(char, 32);
	if(sess_user_name == NULL)
	{
		return 0;
	}
	sess_user_pwd = NEW(char, 64);
	
	if(sess_user_pwd == NULL)
	{
		DELETE(sess_user_name);
		sess_user_name = NULL;
		return 0;
	}

	sess_user_authority = NEW(char,10);
	
	if(sess_user_authority == NULL)
	{
		DELETE(sess_user_name);
		sess_user_name = NULL;

		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;
		return 0;
	}

	str_array[0] = str_time;
	str_array[1] = str_hash_key;
	str_array[2] = str_client_ip;
	str_array[3] = sess_user_name;
	str_array[4] = sess_user_authority;
	str_array[5] = sess_user_pwd;
	
	/*** open session file */
	strlcpy(sfp,SESSION_DEFAULT_DIR,sizeof(sfp)); 
	strlcat(sfp,session_id,sizeof(sfp));

	sf = fopen(sfp,"rb+");
	if(  sf == NULL )
            /** can't open session file,maybe session has time out **/ 
	{
		DELETE(sess_user_name);
		sess_user_name = NULL;

		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return SESSION_NO_THIS_FILE;
		
	}
	
	/*** read session var*/
	memset(buffer,0,512);
	fread(buffer,1,512,sf);
	for(nI=0,j=0,k=0;k<6 && nI<(int)strlen(buffer);nI++)
	{
		if( buffer[nI] == '\n'  )
		{
			temp[j] = '\0';
			strlcpy(str_array[k],temp,64);
			j = 0;
			k ++;
		}
		else
		{
			if(j<63)
			{
				temp[j++] = buffer[nI];
			}
		}
	}
	/*** check active time*/ 
	time(&now);
	//if( now - atoi(str_time) > atoi(parse_config_file("session_live_time")) )
	if( now - atol(str_time) > (long)SESSION_LIVE_TIME )
	{
		//print_session_error("2"); 
		DELETE(sess_user_name);
		sess_user_name = NULL;

	
		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return SESSION_OVER_TIME;
	} 

	/*** compare client hash_key to session hash_key*/
	char		*pszCookie = NULL;
	pszCookie = NEW(char, 32);
	if(pszCookie == NULL)
	{
		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return 0;
	}
	/*get cookie of hash key*/
	
	GetCookie(HTTP_COOKIE,"hash_key", pszCookie);
	
	//TRACE("pszCookie : %s\n", pszCookie);
	if( HTTP_COOKIE == "" || strcmp( Web_RemoveWhiteSpace(pszCookie) , str_hash_key ) != 0 )
	{
		printf("\n");
		TRACE("HTTP_COOKIE : %s\n", HTTP_COOKIE);
		TRACE("pszCookie :%s\n", pszCookie);
		TRACE("str_hash_key :%s", str_hash_key);
		DELETE(pszCookie);
		pszCookie = NULL;
		return SESSION_NO_HASHKEY;
	} 
	DELETE(pszCookie);
	pszCookie = NULL;
	
 
	/*return the authority type  return NO_AUTHORITY*/
	//FILE *fp1;
	//if((fp1 = fopen("/var/file1.html","wb")) != NULL)
	//{
	//    fwrite(REMOTE_ADDR,strlen(REMOTE_ADDR), 1, fp1);
	//    fclose(fp1);
	//}

	//FILE *fp2;
	//if((fp2 = fopen("/var/file2.html","wb")) != NULL)
	//{
	//    fwrite(str_client_ip,strlen(str_client_ip), 1, fp2);
	//    fclose(fp2);
	//}
 
	if( strcmp( REMOTE_ADDR, str_client_ip ) != 0 )
	{
		//print_session_error("4");
		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return SESSION_FAULT_IP;
	}
	/*** refresh session active time */ 
	time(&now);
	sprintf(str_time,"%15ld\n",(long)now);
	fseek (sf,(long)0,SEEK_SET);
	fputs(str_time,sf);  

	fclose(sf);
	
	int		iAuthority = atoi(sess_user_authority);

	DELETE(sess_user_authority);
	sess_user_authority = NULL;
	
	//DELETE(sess_user_name);
	//sess_user_name = NULL;
	*szUserName = sess_user_name;

	DELETE(sess_user_pwd);
	sess_user_pwd = NULL;
	
	return  iAuthority;

}

int ResetTime(IN const char *sessionId)
{
	int nI,j,k;

	const char *session_id = sessionId;
	FILE *sf;
	char sfp[32];
	time_t now;
	//int    r;

	char buffer[512];
	char temp[64];
	char str_time[16];
	char str_hash_key[20];
 
	//char str_client_ip[20];
	char str_client_ip[64];
	char *sess_user_authority;
	char *str_array[7];
	 
	//refresFileTime();

	sess_user_name = NEW(char, 32);
	if(sess_user_name == NULL)
	{
		return 0;
	}
	sess_user_pwd = NEW(char, 32);
	
	if(sess_user_pwd == NULL)
	{
		DELETE(sess_user_name);
		sess_user_name = NULL;
		return FALSE;
	}

	sess_user_authority = NEW(char,10);
	
	if(sess_user_authority == NULL)
	{
		DELETE(sess_user_name);
		sess_user_name = NULL;

		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;
		return FALSE;
	}

	str_array[0] = str_time;
	str_array[1] = str_hash_key;
	str_array[2] = str_client_ip;
	str_array[3] = sess_user_name;
	str_array[4] = sess_user_authority;
	str_array[5] = sess_user_pwd;
	
	/*** open session file */
#define SESSION_VAR				"/var/"	

	strlcpy(sfp,SESSION_VAR,sizeof(sfp)); 
	strlcat(sfp,session_id,sizeof(sfp));
	
	
	sf = fopen(sfp,"rb+");
	if(  sf == NULL )
            /** can't open session file,maybe session has time out **/ 
	{
		//print_session_error("1");
		DELETE(sess_user_name);
		sess_user_name = NULL;

		DELETE(sess_user_pwd);
		sess_user_pwd = NULL;

		DELETE(sess_user_authority);
		sess_user_authority = NULL;

		return FALSE;
		
	}
	
	/*** read session var*/
	memset(buffer,0,512);
	fread(buffer,1,512,sf);
	for(nI=0,j=0,k=0;k<6 && nI<(int)strlen(buffer);nI++)
	{
		if( buffer[nI] == '\n'  )
		{
			temp[j] = '\0';
			strlcpy(str_array[k],temp,64);
			j = 0;
			k ++;
		}
		else
		{
			if(j<63)
			{
				temp[j++] = buffer[nI];
			}
		}
	}
	/*return the authority type  return NO_AUTHORITY*/
 	time(&now);
	sprintf(str_time,"%15ld\n",(long)now);
	fseek (sf,(long)0,SEEK_SET);
	fputs(str_time,sf);  

	fclose(sf);

	DELETE(sess_user_authority);
	sess_user_authority = NULL;
	
	DELETE(sess_user_name);
	sess_user_name = NULL;
	
	DELETE(sess_user_pwd);
	sess_user_pwd = NULL;
	
	return  TRUE;
	
}
void refresFileTime(void)
{
	DIR					*pdir;
	struct dirent		*ent;
	char				path[32];
	char				*filename = NULL;
	char				filepath[64];
	//int					fd;
	time_t				now;
	struct utimbuf		 FileTime;

	time(&now);
	FileTime.actime = now;
	FileTime.modtime = now;

	sprintf(path,"%s","/var");
	pdir = opendir(path);
	if(pdir != NULL)
	{
		while( (ent = readdir(pdir)) != NULL )
		{
			filename = ent->d_name; 
			if( strncmp(filename,"sess_",5) ==0 )
			{
				strlcpy(filepath,path,sizeof(filepath));
				strlcat(filepath,"/",sizeof(filepath));
				strlcat(filepath,filename,sizeof(filepath));
				
				ResetTime(filename);
				utime(filepath, &FileTime);
				

		}
    } 
  }
  closedir(pdir);

  /*FILE *pfile3=NULL;
  pfile3=fopen("/var/wj4.txt","w");
  char *pp="refresFileTime OK!!";
  fwrite(pp,sizeof(pp),1,pfile3);
  fclose(pfile3);*/

}



static int GetCookie(IN char *szCookie, IN char *szName, OUT char *szHashKey)
{
	char	*pSearchValue =  NULL, *pLastSearchValue = NULL;//,*pFirstSearchValue = NULL;
	//int		iPosition  = 0;
	//char	*szHashKey = NULL;
	ASSERT(szCookie);
	ASSERT(szName);
	ASSERT(szHashKey);

	if(szCookie != NULL && szName != NULL )
	{
		pSearchValue = szCookie;
	}
	else
	{
		return FALSE;
	}
	while((pSearchValue = strstr(pSearchValue, "hash_key")) != NULL)
	{
		//TRACE("pSearchValue : %s\n", pSearchValue);
		pLastSearchValue = pSearchValue;
		pSearchValue = pSearchValue + 5;
	}
	//TRACE("szCookie : %s\n", szCookie);
	//TRACE("pLastSearchValue: %s", pLastSearchValue);

	if(pLastSearchValue != NULL)
	{
		pLastSearchValue = pLastSearchValue + strlen(szName) + 1;
		//szHashKey = NEW(char, 17);
		if(szHashKey != NULL)
		{
			strncpyz(szHashKey, pLastSearchValue, 17);
			//TRACE("szHashKey : %s\n", szHashKey);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	return FALSE;
}
/*==========================================================================*
 * FUNCTION :  kill_session
 * PURPOSE  :  delete the session file and disconnect 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 16:20
 *==========================================================================*/
void kill_session(IN char *sessionId)
{
  char *session_id;
  //char *session_path;
  char sfp[128];

  session_id   = sessionId;

  strlcpy(sfp,"/tmp",sizeof(sfp)); 
  strlcat(sfp,"/sess_",sizeof(sfp));
  strlcat(sfp,session_id,sizeof(sfp));

  remove(sfp);
}
/*==========================================================================*
 * FUNCTION :  clean_session_file
 * PURPOSE  :  clear the content of the session file
 * CALLS    : 
 * CALLED BY:  start_session
 * ARGUMENTS:  
 * RETURN   :  void
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 16:20
 *==========================================================================*/
void clean_session_file(void)
{
  DIR				*pdir;
  struct dirent		*ent;
  char				path[32];
  char				*filename;
  char				filepath[64];
  int				fd;
  char				str_time[11];
  time_t			now;
  struct			stat sbuf; 
  //path = "/tmp";
  
  struct utimbuf   FileTime;

  sprintf(path,"%s","/var");
  pdir = opendir(path);
  if(pdir != NULL)
  {
    while( (ent = readdir(pdir)) != NULL )
    {
		filename = ent->d_name; 
		if( strncmp(filename,"sess_",5) ==0 )
		{
			strlcpy(filepath,path,sizeof(filepath));
			strlcat(filepath,"/",sizeof(filepath));
			strlcat(filepath,filename,sizeof(filepath));

			fd = open(filepath,O_RDONLY);
			read(fd,str_time,10);
			time(&now);
			close(fd);
			 

			if (lstat(filepath, &sbuf) >= 0) 
			{ 
				if(now - sbuf.st_mtime  < 0 )
				{
					FileTime.actime = now;
					FileTime.modtime = now;

					utime(filepath, &FileTime);
				}
				else if(now - sbuf.st_mtime  > (long)SESSION_CLEAN_TIME )
				{
					remove(filepath);
				}


			} 

		/*	if( now - atol(str_time) > (long)SESSION_CLEAN_TIME ) 
			{
				remove(filepath);
			} */
			
		}
    } 
  }
  closedir(pdir);

}
/*==========================================================================*
 * FUNCTION :  find_session_file
 * PURPOSE  :  find the content of the session file
 * CALLS    : 
 * CALLED BY:  Web_MakeDataFile
 * ARGUMENTS:  
 * RETURN   :  void
 * COMMENTS : 
 * CREATOR  : John               DATE: 2016-10-17 10:40
 *==========================================================================*/
int find_session_file(void)
{
    #define SESS_FILE_HEAD		"sess_"
    DIR *		dir;
    struct		dirent * ptr;
    char		*filename;
    int			i=0;
    int			iLogFlag = 0;
    dir = opendir("/var");
    if(dir!=NULL)
    {
	    while((ptr = readdir(dir)) != NULL)
	    {
		//printf("d_name : %s\n", ptr->d_name);
		filename = ptr->d_name; 
		if(strncmp(SESS_FILE_HEAD,filename,5)==0)
		{
		 //printf("first 5 characters are same\n");
		 iLogFlag = 1;
		 break;
		}
	    }
    closedir(dir);
    }
    
    return iLogFlag;
}

 
/*==========================================================================*
 * FUNCTION :  print_session_error
 * PURPOSE  :  print the error to the client
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN char *n
 * RETURN   :  void
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 16:20
 *==========================================================================*/
void print_session_error(IN const int n, IN int iLanguage)
{
   //printf("Content-type:text/html\n\n");

	char		szFilePath[64];
	char		*pHtml = NULL;
	if(iLanguage >= 1)
	{
		sprintf(szFilePath, "%s/%s", HTML_SAVE_REAL_PATH, CGI_APP_OVERTIME_PAGE);
	}
	else
	{
		sprintf(szFilePath, "%s/%s", HTML_SAVE_ENG_PATH, CGI_APP_OVERTIME_PAGE);
	}

	if(LoadHtmlFile(szFilePath, &pHtml ) > 0 )
	{

		PostPage(pHtml);
		DELETE(pHtml);
		pHtml = NULL;
	}
	else
	{
		printf("<html><head>");
		printf("<title>Please login !</title>");
		printf("</head>\n");

		printf("<body>\n");
		printf("Sorry login please!<p>\n");
		printf("Login is overtime or system is running error<p>\n");
		printf("please connect with admin\n");
		printf("Error number [%d]\n",n);
		printf("</body>\n");
		printf("</html>\n");
	}
}
void print_no_authority_error(IN const int n)
{
  // printf("Content-type:text/html\n\n");
   printf("<html><head>");
   printf("<title>Log in again please!</title>");
   printf("</head>\n");

   printf("<body>\n");
   if(n == -1)
   {
	   printf("Incorrect User name<p>\n");
   }
   else
   { 
	   printf("Incorrect password<p>\n");
   }
 
   printf("<!--%d-->",n);
   printf("</body>");
   printf("</html>\n");
}
long FileLength(IN FILE *fp )
{
	long oldPos, len;
	
	oldPos = ftell(fp);
	fseek( fp, 0, SEEK_END );
	len = ftell(fp);
	fseek( fp, oldPos, SEEK_SET );
	return len;
}
/*debug*/
/*
int self_log(const char *pszObject, const int pszC, const char *pszInfo)
{
#define LOG_FILE "/var/tmp/realtime.log"


	FILE		*fp;
	if((fp = fopen(LOG_FILE,"a+")) ==   NULL)
	{
		return 1;
	}
	//system("chmod 777 /var/tmp/realtime.log");
	time_t t = time(NULL);
	fprintf(fp,"%s %s %d %s\n",ctime(&t), pszObject, pszC, pszInfo);
	fclose(fp);
	 
}
 */
char *CGI_PUB_GetData(IN char *ptr, IN char cSplit)
{
	ASSERT(ptr);

	int		iPosition = 0;
	char	*pSearchValue = NULL;
	char	*szPtr = NULL;

	pSearchValue = strchr(ptr, cSplit);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition >= 1)
		{
			szPtr = NEW(char, iPosition + 1);
			if(szPtr == NULL)
			{
				return NULL;
			}
			strncpyz(szPtr, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;
		return szPtr;
	}
	return NULL;
}



char *Web_RemoveWhiteSpace(IN char *pszBuf)
{
    char    *p = pszBuf;
    char    *pFirstNonSpace = pszBuf, *pLastWhiteSpace = NULL;
	BOOL    bFound = FALSE;  /* first non-space character found flag */

    while (*p)
    {
        if (WEB_IS_WHITE_SPACE(*p))
        {
            if(pLastWhiteSpace == NULL)
                pLastWhiteSpace = p;
        }
        else 
        {
            if (!bFound)
			{
                pFirstNonSpace = p;
				bFound = TRUE;
			}

            pLastWhiteSpace = NULL;
        }

        p++;
    }

    if (pLastWhiteSpace != NULL)
        *pLastWhiteSpace = 0;

    return pFirstNonSpace;
}
char *Web_SET_MakeConfigurePagePath(IN const char *szPrePath, IN int iLanguage)
{
#define HTML_SAVE_LOC_PATH				"/app/www/html/cgi-bin/loc"
#define HTML_SAVE_LOC2_PATH				"/app/www/html/cgi-bin/loc2" //Added by wj for three languages 2006.4.29
#define HTML_SAVE_ENG_PATH				"/app/www/html/cgi-bin/eng" 
	char		*szGetPath = NULL;

	if((szGetPath = NEW(char, 128)) != NULL && szPrePath != NULL)
	{
		if(iLanguage == ENGLISH_LANGUAGE_NAME)
		{
			sprintf(szGetPath, "%s/%s", HTML_SAVE_ENG_PATH, szPrePath);
		}
		else if(iLanguage == LOCAL_LANGUAGE_NAME)
		{
			sprintf(szGetPath, "%s/%s", HTML_SAVE_LOC_PATH, szPrePath);
		}
        else
        {
            sprintf(szGetPath, "%s/%s", HTML_SAVE_LOC2_PATH, szPrePath);
        }

		return szGetPath;
	}
	return NULL;
}


static char *GetLocalIP(void)
{
	//get ip
	/*char *char_ip;
	struct hostent *host = NULL; 
	char szHostName[256];
	struct in_addr addr;
	
	gethostname(szHostName, sizeof(szHostName));
	host = gethostbyname(szHostName);
	memcpy(&addr, host->h_addr_list[0], (size_t)host->h_length);
	
	char_ip = inet_ntoa(addr);
	return char_ip;*/

	return NULL;
}

int CGI_ClearProc(char *lpMsg)
{
	/*printf("%s: Cleaning....\n",
		lpMsg);*/
	 
	char	szDeleteFile[128];
	sprintf(szDeleteFile, "/var/fifo.%ld", (long)getpid());
	
	if (remove(szDeleteFile) != 0)
	{
		perror(szDeleteFile);
	}
	return 0;
}

void CGI_ClearSessionFile(IN char *szSessionID)
{
	char   sfp[64];
	time_t now;
	//char	*p;
	struct utimbuf   FileTime;
	
	if(szSessionID != NULL)
	{
		time(&now);
		FileTime.actime = now;
		FileTime.modtime = now;
		
		strlcpy(sfp,SESSION_DEFAULT_DIR,sizeof(sfp)); 
		strlcat(sfp,szSessionID,sizeof(sfp));
				

		//p = Web_RemoveWhiteSpace(sfp);
		utime(sfp, &FileTime);
	}
	clean_session_file();


}

BOOL IsValidWORD(const char *pField)
{
#ifndef GC_MAX_NUM_PER_FIELD
#define	GC_MAX_NUM_PER_FIELD	50
#endif
	int	i;

	if(!(*pField))
	{
		return FALSE;
	}

	if((*pField) == '-')
	{
		return FALSE;
	}
	else if((*pField == '+')
		|| (*pField == ' '))
	{
		i = 1;
	}
	else
	{
		i = 0;
	}

	while(*(pField + i))
	{
		if ((i > GC_MAX_NUM_PER_FIELD)
			|| (*(pField + i) < '0') 
			|| (*(pField + i) > '9'))
		{
			return FALSE;
		}
		i++;
	}

	return TRUE;
}

char *Cfg_RemoveWhiteSpace(IN char *pszBuf)
{
#define		SPACE					(0x20)
#define		TAB						(0x09)	 //('/t')
#define		IS_WHITE_SPACE(c)		(((c) == SPACE) || ((c) == TAB))

	char    *p = pszBuf;
	char    *pFirstNonSpace = pszBuf, *pLastWhiteSpace = NULL;
	BOOL    bFound = FALSE;  /* first non-space character found flag */

	while (*p)
	{
		if (IS_WHITE_SPACE(*p))
		{
			if(pLastWhiteSpace == NULL)
				pLastWhiteSpace = p;
		}
		else 
		{
			if (!bFound)
			{
				pFirstNonSpace = p;
				bFound = TRUE;
			}

			pLastWhiteSpace = NULL;
		}

		p++;
	}

	if (pLastWhiteSpace != NULL)
		*pLastWhiteSpace = 0;

	return pFirstNonSpace;
}

BOOL Web_CheckUser(IN int nPagesNeedAuthority)
{
#define		USER_ADMIN		"admin"
#define		USER_READ		"read"
#define		USER_WRITE		"write"
#define CGI_NET_PATH_ERROR_FILE			"/app/www/html/netscape/error.htm"	
#define CGI_NET_CONTROL_VALUE			"/*[ID_RETURN]*/"

	char *szReturnAdmin = "You need Admin authority!";
	char *szReturnWrite = "You need Write or Admin authority!";
	char *pHtml = NULL;

	if(strcmp(REMOTE_USER,USER_WRITE) == 0)
	{
		if(nPagesNeedAuthority == WEB_NET_PAGES_ADMIN )	//If manage page, return no authority
		{
			//return no authority
			if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
			{
				printf("Content-type:text/html\n\n");
				ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, szReturnAdmin);
				PostPage(pHtml);
				DELETE(pHtml);
				pHtml = NULL;
			}
			return FALSE;
		}
	}
	else if(strcmp(REMOTE_USER,USER_READ) == 0)
	{
		if(nPagesNeedAuthority == WEB_NET_PAGES_WRITE || nPagesNeedAuthority == WEB_NET_PAGES_ADMIN)	//If manage or write page, return no authority
		{
			//return no authority
			if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
			{
				printf("Content-type:text/html\n\n");
				ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, szReturnWrite);
				PostPage(pHtml);
				DELETE(pHtml);
				pHtml = NULL;
			}
			return FALSE;
		}
	}
	else
	{
		return TRUE;
	}

}

/*==========================================================================*
 * FUNCTION : strlcpy
 * PURPOSE  : instead of strcpy and strncpy
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *dst, const char *src, size_t dsize 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : John(Get From BSD6.0 src)         DATE: 2016-12-1 9:22
 *==========================================================================*/
size_t	strlcpy(char *dst, const char *src, size_t dsize)
{
	const char *osrc = src;
	size_t nleft = dsize;

	/* Copy as many bytes as will fit. */
	if (nleft != 0) {
		while (--nleft != 0) {
			if ((*dst++ = *src++) == '\0')
				break;
		}
	}

	/* Not enough room in dst, add NUL and traverse rest of src. */
	if (nleft == 0) {
		if (dsize != 0)
			*dst = '\0';		/* NUL-terminate dst */
		while (*src++)
			;
	}

	return(src - osrc - 1);	/* count does not include NUL */
}

/*==========================================================================*
 * FUNCTION : strlcat
 * PURPOSE  : instead of strcat and strncat
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *dst, const char *src, size_t dsize 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : John(Get From BSD6.0 src)         DATE: 2016-12-1 9:22
 *==========================================================================*/
size_t	strlcat(char *dst, const char *src, size_t dsize)
{
	const char *odst = dst;
	const char *osrc = src;
	size_t n = dsize;
	size_t dlen;

	/* Find the end of dst and adjust bytes left but don't go past end. */
	while (n-- != 0 && *dst != '\0')
		dst++;
	dlen = dst - odst;
	n = dsize - dlen;

	if (n-- == 0)
		return(dlen + strlen(src));
	while (*src != '\0') {
		if (n != 0) {
			*dst++ = *src;
			n--;
		}
		src++;
	}
	*dst = '\0';

	return(dlen + (src - osrc));	/* count does not include NUL */
}

/*==========================================================================*
 * FUNCTION :   Get random number from /dev/urandom
 * PURPOSE  :  instead of rand()
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  void
 * RETURN   :  unsigned int
 * COMMENTS : 
 * CREATOR  : John               DATE: 2017-01-16 18:18
 *==========================================================================*/
int urandom(void)
{
	int randNum = 0;
        int fd = open("/dev/urandom", O_RDONLY);
        if(-1 == fd)
        {
                printf("error\n");
                return rand();
        }

        read(fd, (char *)&randNum, sizeof(int));
        close(fd);

	return abs(randNum);

}

/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : web_rw_flash.c
*  CREATOR  : Zhao Zicheng                DATE: 2014-05-06
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include <string.h>
#include <stdio.h>
#include "stdsys.h"  
#include "public.h"  
#include "../../lib/data_mgmt/his_data.h"
#include "web_user.h"

//The handle of the mutex for write info to flash
static HANDLE g_hMutexConsumptionMapInfoToFlash = NULL;
static HANDLE g_hMutexBranchInfoToFlash = NULL;

#define WEB_PAGES_CONFIG_FILE		"/app/config/private/web_user/cfg_consumption_map.cfg"

/*==========================================================================*
* FUNCTION :  GetBranchInfoFromFlash
* PURPOSE  :  Get the branch data from flash, if the branch data structure or the record number have been changed, 
		or for the customer's will, create the storage in flash.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
BOOL GetBranchInfoFromFlash(void)
{
    HANDLE hBranchInfoHandle = NULL;
    WEB_SIGNAL_INFO* pBranchInfo;
    WEB_SIGNAL_INFO	*pSignalInfo;
    int iReadRecordNum, iReadRecordNumTemp;
    int i, j, k;
    int num;

    g_hMutexBranchInfoToFlash = Mutex_Create(TRUE);
    if(g_hMutexBranchInfoToFlash == NULL)
    {
	TRACE("\n\rfilename: web_rw_flash.c [GetBranchInfoFromFlash]\
	      Read Branch info from flash mutex create failed");

	AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_ERROR, 
	    "[%s]--GetBranchInfoFromFlash: ERROR: "
	    "read Branch info from flash mutex create failed!\n\r", __FILE__);

	return FALSE;
    }

    /*for(i = 0; i < gs_iCabinetNumber; i++)
    {
	for(j = 0; j < gs_pWebCabinetInfo[i].iNumber; j++)
	{
	    ZERO_POBJS(gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo,	gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum);
	}
    }*/
    

    if(g_hMutexBranchInfoToFlash != NULL && Mutex_Lock(g_hMutexBranchInfoToFlash, CONSUMPTION_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
    {
	return ERR_USR_MUTEX_WAIT_TIMEOUT;
    }

    hBranchInfoHandle = DAT_StorageOpen(BRANCH_LOG);

    if((hBranchInfoHandle != NULL) && (((RECORD_SET_TRACK *)hBranchInfoHandle)->iRecordSize == sizeof(WEB_SIGNAL_INFO))
	&& (((RECORD_SET_TRACK *)hBranchInfoHandle)->iMaxRecords == MAX_BRANCH_NUM) && (gs_stConsumMapInfo.iConfigFlag == 0))
    {
	TRACE_WEB_USER_NOT_CYCLE("No need to Re-Create the Branch storage\n");
	num = sizeof(WEB_SIGNAL_INFO);
	TRACE_WEB_USER_NOT_CYCLE("num is %d\n", num);
	int iStartPos = 1;
	int nUserNum = MAX_BRANCH_NUM;

	WEB_SIGNAL_INFO* pBranchInfoRead = NULL;

	pBranchInfo = NEW(WEB_SIGNAL_INFO, nUserNum);
	if(pBranchInfo == NULL)
	{
	    if(g_hMutexBranchInfoToFlash)
	    {
		Mutex_Unlock(g_hMutexBranchInfoToFlash);
	    }
	    return FALSE;
	}

	pBranchInfoRead = pBranchInfo;

	TRACE_WEB_USER_NOT_CYCLE("Now read the Branch info from flash!\n");

	if (!DAT_StorageReadRecords(hBranchInfoHandle, &iStartPos,
	    &nUserNum, pBranchInfo, 0, TRUE))
	{
	    TRACE_WEB_USER_NOT_CYCLE("Reading Branch info data ... FAILED.\n");
	    //TRACEX("Reading user info data ... FAILED.\n");
	    nUserNum = 0;
	}
	else
	{
	    TRACE_WEB_USER_NOT_CYCLE("Reading Branch info data ... success[%d].\n", nUserNum);
	}

	DAT_StorageClose(hBranchInfoHandle);

	if(MAX_BRANCH_NUM <= gs_iBranchNum)
	{
	    iReadRecordNum = MAX_BRANCH_NUM;
	}
	else
	{
	    iReadRecordNum = gs_iBranchNum;
	}
	iReadRecordNumTemp = 0;
	TRACE_WEB_USER_NOT_CYCLE("nUserNum is %d    gs_iBranchNum is %d	gs_iCabinetNumber is %d\n", nUserNum, gs_iBranchNum, gs_iCabinetNumber);
	for(i = 0; i < gs_iCabinetNumber; i++)
	{
	    TRACE_WEB_USER_NOT_CYCLE("iNumber is %d\n", gs_pWebCabinetInfo[i].iNumber);
	    for(j = 0; j < gs_pWebCabinetInfo[i].iNumber; j++)
	    {
		pSignalInfo = gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo;
		TRACE_WEB_USER_NOT_CYCLE("iSignalNum is %d\n", gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum);
		for(k = 0; k < gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum; k++)
		{
		    pSignalInfo->fRatingCurrent = pBranchInfo->fRatingCurrent;
		    pSignalInfo->bRatingSet = pBranchInfo->bRatingSet;
		    //memcpy(pSignalInfo, pBranchInfo, sizeof(WEB_SIGNAL_INFO));
		    if(pSignalInfo->bRatingSet != 1)
		    {
			pSignalInfo->fRatingCurrent = 9999;
		    }
		    TRACE_WEB_USER_NOT_CYCLE("The fRatingCurrent is %f\n", pSignalInfo->fRatingCurrent);
		    pSignalInfo++;
		    pBranchInfo++;
		    iReadRecordNumTemp++;
		    if(iReadRecordNumTemp >= iReadRecordNum)
		    {
			DELETE(pBranchInfoRead);
			pBranchInfoRead = NULL;
			if(g_hMutexBranchInfoToFlash)
			{
			    Mutex_Unlock(g_hMutexBranchInfoToFlash);
			}
			return TRUE;
		    }
		}
	    }
	}

	DELETE(pBranchInfoRead);
	pBranchInfoRead = NULL;
    }
    else //Create Branch info log
    {
	TRACE_WEB_USER_NOT_CYCLE("Now create the Branch info in flash!\n");

	if(DAT_StorageDeleteRecord(BRANCH_LOG) != TRUE)
	{
	    AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_ERROR, 
		"Delete Branch data file error");

	    Mutex_Unlock(g_hMutexBranchInfoToFlash);

	    return FALSE;
	}

	hBranchInfoHandle = DAT_StorageCreate(BRANCH_LOG,
	    sizeof(WEB_SIGNAL_INFO), 
	    MAX_BRANCH_NUM,
	    FREQ_USER_INFO);

	// log after created...
	if (hBranchInfoHandle)
	{
	    AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_INFO, 
		"Creating Branch data file '%s'"
		"(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s\n", 
		BRANCH_LOG, sizeof(WEB_SIGNAL_INFO), 
		MAX_BRANCH_NUM, FREQ_USER_INFO, "OK");
	}
	else
	{
	    AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_ERROR, 
		"Creating Branch data file '%s'"
		"(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s\n", 
		BRANCH_LOG, sizeof(WEB_SIGNAL_INFO), 
		MAX_BRANCH_NUM, FREQ_USER_INFO, "FAILED");
	}

	DAT_StorageClose(hBranchInfoHandle);
    }
    if(g_hMutexBranchInfoToFlash)
    {
	Mutex_Unlock(g_hMutexBranchInfoToFlash);
    }

    return TRUE;
}

/*==========================================================================*
* FUNCTION :  GetConsumptionMapInfoFromFlash
* PURPOSE  :  Get the cabinet data from flash, if the cabinet data structure or the record number have been changed, then create the storage in flash.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
BOOL GetConsumptionMapInfoFromFlash(void)
{
    HANDLE hConsumptionMapInfoHandle = NULL;
    CABINET_INFO* pConsumptionMapInfo;
    int iReadRecordNum;
    CABINET_INFO *pstCabInfo;
    int num;

    g_hMutexConsumptionMapInfoToFlash = Mutex_Create(TRUE);
    if(g_hMutexConsumptionMapInfoToFlash == NULL)
    {
	TRACE("\n\rfilename: web_rw_flash.c [GetConsumptionMapInfoFromFlash]\
	      write Consumption map info to flash mutex create failed");

	AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_ERROR, 
	    "[%s]--GetConsumptionMapInfoFromFlash: ERROR: "
	    "write Consumption map info to flash mutex create failed!\n\r", __FILE__);

	return FALSE;
    }

    ZERO_POBJS(gs_stConsumMapInfo.pstCabInfo,	gs_stConsumMapInfo.iNumber);

    if (g_hMutexConsumptionMapInfoToFlash != NULL && Mutex_Lock(g_hMutexConsumptionMapInfoToFlash, CONSUMPTION_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
    {
	return ERR_USR_MUTEX_WAIT_TIMEOUT;
    }

    hConsumptionMapInfoHandle = DAT_StorageOpen(CONSUMPTION_LOG);

    if((hConsumptionMapInfoHandle != NULL) && (((RECORD_SET_TRACK *)hConsumptionMapInfoHandle)->iRecordSize == sizeof(CABINET_INFO))
	&& (((RECORD_SET_TRACK *)hConsumptionMapInfoHandle)->iMaxRecords == MAX_CABINET_NUM) && (gs_stConsumMapInfo.iConfigFlag == 0))
    {
	TRACE_WEB_USER_NOT_CYCLE("No need to Re-Create the consumption map storage\n");
	num = sizeof(CABINET_INFO);
	TRACE_WEB_USER_NOT_CYCLE("num is %d\n", num);
	int iStartPos = 1;

	int i, j;
	int iCabinetID, iDUID, iBranchID;
	int nUserNum = MAX_CABINET_NUM;

	CABINET_INFO* pConsumptionMapInfoRead = NULL;

	pConsumptionMapInfo = NEW(CABINET_INFO, nUserNum);
	if(pConsumptionMapInfo == NULL)
	{
	    if(g_hMutexConsumptionMapInfoToFlash)
	    {
		Mutex_Unlock(g_hMutexConsumptionMapInfoToFlash);
	    }
	    return FALSE;
	}

	pConsumptionMapInfoRead = pConsumptionMapInfo;

	TRACE_WEB_USER_NOT_CYCLE("Now read the Consumption map info from flash!\n");

	if (!DAT_StorageReadRecords(hConsumptionMapInfoHandle, &iStartPos,
	    &nUserNum, pConsumptionMapInfo, 0, TRUE))
	{
	    TRACE_WEB_USER_NOT_CYCLE("Reading Consumption map info data ... FAILED.\n");
	    //TRACEX("Reading user info data ... FAILED.\n");
	    nUserNum = 0;
	}
	else
	{
	    TRACE_WEB_USER_NOT_CYCLE("Reading Consumption map info data ... success[%d].\n", nUserNum);
	}

	DAT_StorageClose(hConsumptionMapInfoHandle);

	if(nUserNum <= gs_stConsumMapInfo.iNumber)
	{
	    iReadRecordNum = nUserNum;
	}
	else
	{
	    iReadRecordNum = gs_stConsumMapInfo.iNumber;
	}
	pstCabInfo = gs_stConsumMapInfo.pstCabInfo;
	for (i = 0; i < iReadRecordNum; i++, pConsumptionMapInfo++)
	{
	    if(pConsumptionMapInfo != NULL)
	    {
		memcpy(pstCabInfo, pConsumptionMapInfo, sizeof(CABINET_INFO));
		for(j = 0; j < 20; j++)
		{
		    if(pstCabInfo->stBranch[j].bValid == TRUE)
		    {
			iCabinetID = pstCabInfo->stBranch[j].iCabinetID;
			iDUID = pstCabInfo->stBranch[j].iDUID;
			iBranchID = pstCabInfo->stBranch[j].iBranchID;
			TRACE_WEB_USER_NOT_CYCLE("iCabinetID is %d, iDUID is %d, iBranchID is %d\n", iCabinetID, iDUID, iBranchID);
			if(iCabinetID <= gs_iCabinetNumber)
			{
			    if(iDUID <= gs_pWebCabinetInfo[iCabinetID - 1].iNumber)
			    {
				if(iBranchID <= gs_pWebCabinetInfo[iCabinetID - 1].stWebDU[iDUID - 1].iSignalNum)
				{
				    gs_pWebCabinetInfo[iCabinetID - 1].stWebDU[iDUID - 1].pSignalInfo[iBranchID - 1].bBranchConfig = TRUE;
				}
				else
				{
				    pstCabInfo->stBranch[j].bValid = FALSE;
				    TRACE_WEB_USER_NOT_CYCLE("Can't find the branch\n");
				}
			    }
			    else
			    {
				pstCabInfo->stBranch[j].bValid = FALSE;
				TRACE_WEB_USER_NOT_CYCLE("Can't find the branch\n");
			    }
			}
			else
			{
			    pstCabInfo->stBranch[j].bValid = FALSE;
			    TRACE_WEB_USER_NOT_CYCLE("Can't find the branch\n");
			}
		    }
		}
		//TRACE_WEB_USER_NOT_CYCLE("Run here 1\n");
		if(pstCabInfo->cName[0] == 0)
		{
		    snprintf(pstCabInfo->cName, 64, "Cabinet%d", (i + 1));
		}
		//TRACE_WEB_USER_NOT_CYCLE("Run here 2\n");
		if(FLOAT_EQUAL0(pstCabInfo->fAlarmLevel1))
		{
		    pstCabInfo->fAlarmLevel1 = 100;
		}
		//TRACE_WEB_USER_NOT_CYCLE("Run here 3\n");
		if(FLOAT_EQUAL0(pstCabInfo->fAlarmLevel2))
		{
		    pstCabInfo->fAlarmLevel2 = 100;
		}
		//TRACE_WEB_USER_NOT_CYCLE("Run here 4\n");
		if(isnan(pstCabInfo->fTotalEnergy))
		{
		    pstCabInfo->fTotalEnergy = 0;
		}
		//TRACE_WEB_USER_NOT_CYCLE("Run here 5\n");
		TRACE_WEB_USER_NOT_CYCLE("fTotalEnergy is %f, fPeakPowLast24H is %f, fPeakPowLastWeek is %f, fPeakPowLastMonth is %f\n",
		    pstCabInfo->fTotalEnergy, pstCabInfo->fPeakPowLast24H, pstCabInfo->fPeakPowLastWeek, pstCabInfo->fPeakPowLastMonth);
		pstCabInfo++;
	    }
	}

	DELETE(pConsumptionMapInfoRead);
	pConsumptionMapInfoRead = NULL;
    }
    else //Create Consumption map info log
    {
	TRACE_WEB_USER_NOT_CYCLE("Now create the Consumption map info in flash!\n");

	if(DAT_StorageDeleteRecord(CONSUMPTION_LOG) != TRUE)
	{
	    AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_ERROR, 
		"Delete ConsumptionMap data file error");

	    Mutex_Unlock(g_hMutexConsumptionMapInfoToFlash);

	    return FALSE;
	}

	hConsumptionMapInfoHandle = DAT_StorageCreate(CONSUMPTION_LOG,
	    sizeof(CABINET_INFO), 
	    MAX_CABINET_NUM,
	    FREQ_USER_INFO);

	// log after created...
	if (hConsumptionMapInfoHandle)
	{
	    AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_INFO, 
		"Creating Consumption map data file '%s'"
		"(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s\n", 
		CONSUMPTION_LOG, sizeof(CABINET_INFO), 
		MAX_CABINET_NUM, FREQ_USER_INFO, "OK");
	}
	else
	{
	    AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_ERROR, 
		"Creating Consumption map data file '%s'"
		"(RecSize:%d, MaxRec:%d, RecFreq:%1.5f)...%s\n", 
		CONSUMPTION_LOG, sizeof(CABINET_INFO), 
		MAX_CABINET_NUM, FREQ_USER_INFO, "FAILED");
	}


	DAT_StorageClose(hConsumptionMapInfoHandle);

	if(gs_stConsumMapInfo.iConfigFlag == 1)
	{
	    FILE	*fp;
	    char			*pSearchValue = NULL, szReadString[130];
	    char str_string[2] = {"0"};
#define MAX_READ_LINE_LEN			129
#define	FILE_CFG_FLAG			"[CONFIG_FLAG]"

	    if((fp = fopen(WEB_PAGES_CONFIG_FILE, "rb+")) != NULL)
	    {
		while(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
		{
		    if((pSearchValue = strstr(szReadString, FILE_CFG_FLAG)) != NULL)
		    {
			if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
			{
			    TRACE_WEB_USER_NOT_CYCLE("szReadString is %s\n", szReadString);
			    fseek(fp,(long)-strlen(szReadString), SEEK_CUR);
			    fputs(str_string, fp);
			}
		    }
		}
		fclose(fp);
	    }
	}
    }
    if(g_hMutexConsumptionMapInfoToFlash)
    {
	Mutex_Unlock(g_hMutexConsumptionMapInfoToFlash);
    }

    return TRUE;
}

/*==========================================================================*
* FUNCTION :  UpdateBranchInfoToFlash
* PURPOSE  :  Update the branch data to flash.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
BOOL UpdateBranchInfoToFlash(void)
{
    HANDLE hBranchInfoHandleW = NULL;
    BOOL	bRst;
    WEB_SIGNAL_INFO *pstBranchInfo;
    int	iRecordNum;

    if(g_hMutexBranchInfoToFlash != NULL && Mutex_Lock(g_hMutexBranchInfoToFlash, CONSUMPTION_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
    {
	return ERR_USR_MUTEX_WAIT_TIMEOUT;
    }

    if(DAT_StorageDeleteRecord(BRANCH_LOG) != TRUE)
    {
	AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_ERROR, 
	    "Delete Branch data file error");

	Mutex_Unlock(g_hMutexBranchInfoToFlash);

	return FALSE;
    }

    hBranchInfoHandleW = DAT_StorageOpenW(BRANCH_LOG);

    if (hBranchInfoHandleW != NULL)
    {
	WEB_SIGNAL_INFO *pBranchInfo, *pWriteInfo;
	int i, j, k;

	pBranchInfo = NEW(WEB_SIGNAL_INFO, MAX_BRANCH_NUM);

	if(pBranchInfo == NULL)
	{
	    if(g_hMutexBranchInfoToFlash)
	    {
		Mutex_Unlock(g_hMutexBranchInfoToFlash);
	    }
	    return FALSE;
	}
	TRACE_WEB_USER_NOT_CYCLE("The total record number is %d\n", MAX_BRANCH_NUM);
	TRACE_WEB_USER_NOT_CYCLE("every record is %d bytes\n", sizeof(WEB_SIGNAL_INFO));

	pWriteInfo = pBranchInfo;

	memset(pBranchInfo, 0, sizeof(WEB_SIGNAL_INFO) * MAX_BRANCH_NUM);

	iRecordNum = 0;
	for(i = 0; i < gs_iCabinetNumber; i++)
	{
	    for(j = 0; j < gs_pWebCabinetInfo[i].iNumber; j++)
	    {
		pstBranchInfo = gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo;
		for(k = 0; k < gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum; k++)
		{
		    memcpy(pBranchInfo, pstBranchInfo, sizeof(WEB_SIGNAL_INFO));
		    pBranchInfo++;
		    pstBranchInfo++;
		    iRecordNum++;
		    if(iRecordNum >= MAX_BRANCH_NUM)
		    {
			break;
		    }
		}
	    }
	}

	bRst = DAT_StorageWriteRecord(hBranchInfoHandleW,
	    sizeof(WEB_SIGNAL_INFO) * 
	    MAX_BRANCH_NUM,
	    pWriteInfo);
	TRACE_WEB_USER_NOT_CYCLE("The write flash result is %d\n", bRst);

	DAT_StorageClose(hBranchInfoHandleW);

	TRACE_WEB_USER_NOT_CYCLE("Success to close the interface\n");

	if (pWriteInfo != NULL)
	{
	    DELETE(pWriteInfo);
	}
    }
    if(g_hMutexBranchInfoToFlash)
    {
	Mutex_Unlock(g_hMutexBranchInfoToFlash);
    }

    return TRUE;
}

/*==========================================================================*
* FUNCTION :  UpdateConsumptionMapInfoToFlash
* PURPOSE  :  Update the cabinet data to flash.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
BOOL UpdateConsumptionMapInfoToFlash(void)
{
    HANDLE hConsumptionMapInfoHandleW = NULL;
    BOOL	bRst;
    CABINET_INFO *pstCabInfo;

    TRACE_WEB_USER_NOT_CYCLE("Begin to update the flash\n");

    if(g_hMutexConsumptionMapInfoToFlash != NULL && Mutex_Lock(g_hMutexConsumptionMapInfoToFlash, CONSUMPTION_LOCK_WAIT_TIMEOUT) != ERR_MUTEX_OK)
    {
	return ERR_USR_MUTEX_WAIT_TIMEOUT;
    }

    if(DAT_StorageDeleteRecord(CONSUMPTION_LOG) != TRUE)
    {
	AppLogOut(WEB_RW_FLASH_LOG, APP_LOG_ERROR, 
	    "Delete ConsumptionMap data file error");

	Mutex_Unlock(g_hMutexConsumptionMapInfoToFlash);

	return FALSE;
    }

    hConsumptionMapInfoHandleW = DAT_StorageOpenW(CONSUMPTION_LOG);

    if (hConsumptionMapInfoHandleW != NULL)
    {
	CABINET_INFO *pConsumptionMapInfo, *pWriteInfo;
	int i;

	pConsumptionMapInfo = NEW(CABINET_INFO, MAX_CABINET_NUM);

	if(pConsumptionMapInfo == NULL)
	{
	    if(g_hMutexConsumptionMapInfoToFlash)
	    {
		Mutex_Unlock(g_hMutexConsumptionMapInfoToFlash);
	    }
	    return FALSE;
	}
	TRACE_WEB_USER_NOT_CYCLE("The total record number is %d\n", MAX_CABINET_NUM);
	TRACE_WEB_USER_NOT_CYCLE("every record is %d bytes\n", sizeof(CABINET_INFO));

	pWriteInfo = pConsumptionMapInfo;

	memset(pConsumptionMapInfo, 0, sizeof(CABINET_INFO) * MAX_CABINET_NUM);

	pstCabInfo = gs_stConsumMapInfo.pstCabInfo;
	for (i = 0; i < gs_stConsumMapInfo.iNumber; i++, pConsumptionMapInfo++)
	{
	    memcpy(pConsumptionMapInfo, pstCabInfo, sizeof(CABINET_INFO));
	    pstCabInfo++;
	}

	bRst = DAT_StorageWriteRecord(hConsumptionMapInfoHandleW,
	    sizeof(CABINET_INFO) * 
	    MAX_CABINET_NUM,
	    pWriteInfo);
	TRACE_WEB_USER_NOT_CYCLE("The write flash result is %d\n", bRst);

	DAT_StorageClose(hConsumptionMapInfoHandleW);

	if (pWriteInfo != NULL)
	{
	    DELETE(pWriteInfo);
	}
    }
    if(g_hMutexConsumptionMapInfoToFlash)
    {
	Mutex_Unlock(g_hMutexConsumptionMapInfoToFlash);
    }

    return TRUE;
}


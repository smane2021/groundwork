/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgivars.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-09-28 10:15
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#include "pubfunc.h"
#include "new.h"
#include "cgivars.h"

/* local function prototypes */

char x2c(char *what);

/* hex2char */
/* RFC */
char hex2char(char *hex) {
	char char_value;
	char_value = (hex[0] >= 'A' ? ((hex[0] & 0xdf) - 'A') + 10 : (hex[0] - '0'));
	char_value *= 16;
	char_value += (hex[1] >= 'A' ? ((hex[1] & 0xdf) - 'A') + 10 : (hex[1] - '0'));
	return char_value;
}

/* unescape_url */
/* RFC */
void unescape_url(char *url) {
	int n, k;
	for(n=0, k=0;url[k];++n, ++k) {
		if((url[n] = url[k]) == '%') {
			url[n] = hex2char(&url[k+1]);
			k += 2;
		}
	}
	url[n] = '\0';
}


/* getRequestMethod
 * retn:	from_method (GET or POST) on success,
 *			-1 on failure.  */
int getRequestMethod(void) {
	char *request_method;
	int form_method;

	request_method = getenv("REQUEST_METHOD");
	if(request_method == NULL)
		return -1;

	if (!strcmp(request_method, "GET") || !strcmp(request_method, "HEAD") ) {
		form_method = GET;
	} else if (!strcmp(request_method, "POST")) {
		form_method = POST;
	} else {
		/* wtf was it then?!! */
		return -1;
	}
	return form_method;
}


/* getGETvars
 * retn:	getvars */
char **getGETvars(void) {
	int i;
	char **getvars;
	char *getinput;
	char **pairlist;
	int paircount = 0;
	char *nvpair;
	char *eqpos;

	getinput = getenv("QUERY_STRING");
	//printf("1------ getinput= %s \n", getinput);
	if (getinput)
		getinput = strdup(getinput);

	/*FILE *pf101=NULL;
	pf101=fopen("/var/file101.txt","a+");
	fwrite(getinput,strlen(getinput),1,pf101);
	fclose(pf101);*/
	//printf("2------ getinput= %s \n", getinput);
	/* Change all plusses back to spaces */
	for(i=0; getinput && getinput[i]; i++)
		if(getinput[i] == '+')
			getinput[i] = ' ';
	//printf("3------ getinput= %s \n", getinput);
	//pairlist = (char **) malloc(256*sizeof(char **));
	//#define pChar char*;
	pairlist = NEW(char * ,256);
	if(pairlist == NULL)
	{
		return NULL;
	}

	paircount = 0;
	nvpair = getinput ? strtok(getinput, "&") : NULL;
	//printf("4------ nvpair= %s \n", nvpair);
	while (nvpair) {
		pairlist[paircount++]= strdup(nvpair);
		//printf("pairlist[%d]=%s nvpair= %s \n",paircount,pairlist[paircount],nvpair);
		if(!(paircount%256))
			//pairlist = (char **) realloc(pairlist,(paircount+256)*sizeof(char **));
			pairlist = RENEW(char *, pairlist,(paircount+256)*sizeof(char *));
		nvpair = strtok(NULL, "&");
	}

	pairlist[paircount] = 0;
	//getvars = (char **) malloc((paircount*2+1)*sizeof(char **));
	//typedef char* pChar;
	getvars = NEW(char *,paircount*2 +1);
	if(getvars == NULL)
	{
		DELETE(pairlist);
		pairlist = NULL;
		return NULL;
	}

	for (i= 0; i<paircount; i++) {
		if((eqpos=strchr(pairlist[i], '='))) {
			*eqpos = '\0';
			unescape_url(getvars[i*2+1] = strdup(eqpos+1));
		} else {
			unescape_url(getvars[i*2+1] = strdup(""));
		}
		unescape_url(getvars[i*2] = strdup(pairlist[i]));
		//printf("getvars[%d]=%s getvars[%d]=%s\n",i,getvars[i],(i+1),getvars[i+1]);
	}
	getvars[paircount*2] = 0;
	//changed by wankun, in order to avoid memory leak 2007
	//for(i=0;pairlist[i];i++)	if(pairlist[i]!= NULL) DELETE(pairlist[i]);
	//ended by wankun
	if ( pairlist != NULL ) DELETE(pairlist);
	/*
	if (getinput != NULL )
	DELETE(getinput);
	*/
	return getvars;
}



/* getPOSTvars
 * retn:	postvars */
char **getPOSTvars(void) {
	int i;
	int content_length;
	char **postvars;
	char *postinput;
	char **pairlist;
	int paircount = 0;
	char *nvpair;
	char *eqpos;
	
	postinput = getenv("CONTENT_LENGTH");
	if (!postinput)
		return NULL;
	if(!(content_length = atoi(postinput)))
		return NULL;
	if(content_length <= 0)
	{
		return NULL;
	}
	if(!(postinput = NEW(char,(size_t)content_length + 1)))
		return NULL;
	if(!fread(postinput, (size_t)content_length, (size_t)1, stdin))
	{
		DELETE(postinput);
		return NULL;
	}
	postinput[content_length] = '\0';
	

   	for(i=0;postinput[i];i++)
		if(postinput[i] == '+')
			postinput[i] = ' ';

	//pairlist = (char **) malloc(256*sizeof(char **));
	pairlist = NEW(char *,256);
	if(pairlist == NULL)
	{
		DELETE(postinput);
		return NULL;
	}
	paircount = 0;
	nvpair = strtok(postinput, "&");
	while (nvpair) {
		pairlist[paircount++] = strdup(nvpair);
		if(!(paircount%256))
    			//pairlist = (char **) realloc(pairlist, (paircount+256)*sizeof(char **));
				pairlist = RENEW(char *,pairlist, (paircount+256)*sizeof(char *));
		nvpair = strtok(NULL, "&");
	}
    
	pairlist[paircount] = 0;
	//postvars = (char **) malloc((paircount*2+1)*sizeof(char **));
	postvars = NEW(char *,paircount*2 +1);
	if(postvars == NULL)
	{
		DELETE(postinput);
		DELETE(pairlist);
		return NULL;
	}
	for(i = 0;i<paircount;i++) {
        	if((eqpos = strchr(pairlist[i], '='))) {
       	    		*eqpos= '\0';
	        	unescape_url(postvars[i*2+1] = strdup(eqpos+1));
        	} else {
       	    		unescape_url(postvars[i*2+1] = strdup(""));
	   	}
        	unescape_url(postvars[i*2]= strdup(pairlist[i]));
	}
	postvars[paircount*2] = 0;

	//for(i=0;pairlist[i];i++)
	//	DELETE(pairlist[i]);
	DELETE(pairlist);
	DELETE(postinput);

	return postvars;
}

/* cleanUp
 * free the mallocs */
int cleanUp( char **getvars, char **postvars) {

	if (postvars) {
		//for(i=0;postvars[i];i++)
		//	DELETE(postvars[i]);
		DELETE(postvars);
	}
	if (getvars) {
		//for(i=0;getvars[i];i++)
		//	DELETE(getvars[i]);
		DELETE(getvars);
	}

	return 0;
}

static char *findVar( char **varPairs, const char *var )
{
    int i;
    if( varPairs )
    {
        for(i=0;varPairs[i];i+=2)
        {
            if( strcmp( varPairs[i], var ) == 0 )
                return varPairs[i+1];
        }
    }
    return NULL;
}

char *getValue( char **getVars, char **postVars, const char *var )
{
    char *pval;

    pval = findVar( getVars, var );
    if( pval == NULL )
    {
        pval = findVar( postVars, var );
	}

    return pval;
}

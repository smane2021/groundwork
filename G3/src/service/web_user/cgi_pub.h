/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi-pub.h
 *  CREATOR  : Yang Guoxin                   DATE: 2004-09-28 10:13
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef _CGI_PUB_H
#define	_CGI_PUB_H


#include "stdsys.h"
#include "public.h"
#include "cgivars.h"

/*
#include "stdio.h"
#include "sys/types.h"
#include "string.h"
#include "time.h"
#define IN
#define OUT
*/
#include "unistd.h"
#include "netdb.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <utime.h>
#include <unistd.h>

#include "cgivars.h"

#include "cgi_suicide_timer.h"

#define MAKE_VAR_FIELD(var)				("/*[" var "]*/")

#define LOG_IN							0			//First Connect
#define REALTIME_DATA					1			//get realtime data
#define	CONTROL_COMMAND					2			//control
#define	HISTORY_QUERY					3			//history query
#define	CONFIGURE_MANAGE				4			//config manage
#define FILE_MANAGE						5			//file manage
#define FILE_MANAGE_REPLACEFILE			6			//file manage
//Start:Added by YangGuoxin,8/23/2006
#define WEB_SUPPORT_NETSCAPE			7
//Add by ZhaoZicheng 2015-09-05 
#define HTTP_INTERFACE				8





enum _NETSCAPE_CONTROL
{
	SET_NET_CONTROL	= 0,
	SET_NMS_SETTING = 8,
	GET_NMS_SETTING,
	SET_EEM_SETTING,
	GET_EEM_SETTING,
	SET_IP_SETTING,
	GET_IP_SETTING,
	GET_TIME_SETTING,
	SET_TIME_SETTING,
	GET_LANG_SETTING,
	SET_LANG_SETTING,
	SET_RECOVER_RESET,
	SET_NMS_V3_SETTING,
	GET_NMS_V3_SETTING,
};

enum _NETSCAPE_EEM_TYPE
{
	NET_EEM_GENEARL = 1,
	NET_EEM_PSTN,
	NET_EEM_TCPIP,
	NET_EEM_LEASDLINE,
};

//Start:Added by YangGuoxin,12/20/2006
#define WEB_SUPPORT_NET_SETTING			8
//End:Added by YangGuoxin,12/20/2006
//Start:Added by Wankun, 03/03/2007
#define HISTORY_QUERY_NETSCAPE			9
//End:Added by Wankun, 03/03/2007




#define EQUIP_SAMPLE_SIGNALS			"equip_sample_signals"
#define EQUIP_CONTROL_SIGNALS			"equip_control_signals"
#define EQUIP_SETTING_SIGNALS			"equip_setting_signals"
#define EQUIP_STATUS_SIGNALS			"equip_status_signals"
#define EQUIP_ALARM_SIGNALS				"equip_Alarm_signals"
#define EQUIP_RALARM_SIGNALS			"equip_RAlarm_signals"
#define EQUIP_LIST						"equip_list"
#define EQUIP_ALARM_DATE				"VAR_LAST_ALARM_TIME"
#define EQUIP_NEED_REFRESH_RECTIFIER	"VAR_NEED_REFRESH_RECTIFIER"
#define EQUIP_NEED_REFRESH_ACD	        "VAR_NEED_REFRESH_ACD"
#define EQUIP_NEED_REFRESH_DCD	        "VAR_NEED_REFRESH_DCD"
#define EQUIP_NEED_REFRESH_BATT	        "VAR_NEED_REFRESH_BATT"
#define CGI_NOW_TIME					"VAR_NOW_TIME"

/*CGI Param*/
#define EQUIP_NAME						"equip_name"
#define SESSION_ID						"sessionId"
#define CGI_FILE_NAME					"file_name"
#define	EQUIP_ID						"equip_ID"

#define	EQUIP_BT_ID						"equip_bt_ID"

#define	LANGUAGE_TYPE					"language_type"
#define ALARM_NEW_LEVEL					"signal_new_level"
#define CFG_ALARM_ALARMREG           "_alarmReg"
#define	NEED_RETURN_STRUCTURE			"need_return_structure"
#define CONTROL_VALUE					"control_value"
#define CONTROL_TYPE					"control_type"
#define SIGNAL_TYPE						"signal_type"
#define SIGNAL_ID						"signal_id"
#define VALUE_TYPE						"value_type"
#define SIGNAL_MODIFY_NAME_TYPE			"signal_modify_type"
#define CGI_CLEAR_SESSION				"clear_session"
//changed by Frank Wu,4/N/35,20140527, for adding the the web setting tab page 'DI'
#define SIGNAL_NAME						"signal_new_name"
#define SIGNAL_ABBR_NAME				"signal_new_abbr_name"
#define EQUIP2_ID						"equip2_ID"
#define SIGNAL2_TYPE					"signal2_type"
#define SIGNAL2_ID						"signal2_id"
#define ALARM_NEW_STATE					"signal_new_state"
#define DO_NEW_NAME					"sSignalName_en"
//changed by Song Xu 20160603, for modifying the sSignalAbbrName_en in fuse web
#define DO_NEW_ABBR_NAME					"sSignalAbbrName_en"

#define SHUNT_SIGNAL_VALUE2		"Signal_Value2"
#define SHUNT_SIGNAL_VALUE_TYPE2	"Value2_ValueType"
#define CUSTOM_SIGNAL_TYPE2		"Value2_SignalType"
#define CUSTOM_SIGNAL_ID2		"Value2_SignalID"


//Changed by Marco Yang,5.16/2016, for adding the shunt page setting.
#define SHUNT_SIGNAL_TYPE1		"Value1_SignalType"              //Set as
#define SHUNT_SIGNAL_ID1		"Value1_SignalID"
#define SHUNT_SIGNAL_VALUE_TYPE1	"Value1_ValueType"
#define SHUNT_SIGNAL_VALUE1		"Signal_Value1"
#define SHUNT_SIGNAL_TYPE2		"FullName2_SignalType"
#define SHUNT_SIGNAL_ID2		"FullName2_SignalID"
#define SHUNT_SIGNAL_FULLNAME2		"Signal_FullName2"	 //Full name
#define SHUNT_SIGNAL_ABBREVNAME2	"Signal_AbbrevName2"	//Abbrev name
#define SHUNT_SIGNAL_TYPE3		"Value3_SignalType"
#define SHUNT_SIGNAL_ID3		"Value3_SignalID"
#define SHUNT_SIGNAL_VALUE_TYPE3	"Value3_ValueType"
#define SHUNT_SIGNAL_VALUE3		"Signal_Value3"		//Full scale current
#define SHUNT_SIGNAL_TYPE4		"Value4_SignalType"
#define SHUNT_SIGNAL_ID4		"Value4_SignalID"
#define SHUNT_SIGNAL_VALUE_TYPE4	"Value4_ValueType"
#define SHUNT_SIGNAL_VALUE4		"Signal_Value4"		//Full scale voltage
#define SHUNT_SIGNAL_TYPE5		"Value5_SignalType"
#define SHUNT_SIGNAL_ID5		"Value5_SignalID"
#define SHUNT_SIGNAL_VALUE_TYPE5	"Value5_ValueType"
#define SHUNT_SIGNAL_VALUE5		"Signal_Value5"		//Break value
#define SHUNT_SIGNAL_TYPE6		"Value6_SignalType"
#define SHUNT_SIGNAL_ID6		"Value6_SignalID"
#define SHUNT_SIGNAL_VALUE_TYPE6	"Value6_ValueType"
#define SHUNT_SIGNAL_VALUE6		"Signal_Value6"//High 1 value
#define SHUNT_SIGNAL_TYPE7		"Level7_SignalType"
#define SHUNT_SIGNAL_ID7		"Level7_SignalID"
#define SHUNT_SIGNAL_LEVEL7		"Alarm_Level7"
#define SHUNT_SIGNAL_RELAY7		"Alarm_Relay7"		//High 2 alarm level, relay
#define SHUNT_SIGNAL_TYPE8		"Value8_SignalType"
#define SHUNT_SIGNAL_ID8		"Value8_SignalID"
#define SHUNT_SIGNAL_VALUE_TYPE8	"Value8_ValueType"
#define SHUNT_SIGNAL_VALUE8		"Signal_Value8"//High 2 value

#define SHUNT_SIGNAL_TYPE9		"Level9_SignalType"
#define SHUNT_SIGNAL_ID9		"Level9_SignalID"
#define SHUNT_SIGNAL_LEVEL9		"Alarm_Level9"
#define SHUNT_SIGNAL_RELAY9		"Alarm_Relay9"		//High 2 alarm level, relay.

#define SHUNT_SIGNAL_ALARM_HIGH1_FULL		"Alarm_High1_Full"		//High 2 alarm level, relay.
#define SHUNT_SIGNAL_ALARM_HIGH1_ABBREV		"Alarm_High1_Abbrev"		//High 2 alarm level, relay.
#define SHUNT_SIGNAL_ALARM_HIGH2_FULL		"Alarm_High2_Full"		//High 2 alarm level, relay.
#define SHUNT_SIGNAL_ALARM_HIGH2_ABBREV		"Alarm_High2_Abbrev"		//High 2 alarm level, relay.


#define ANALOG_VOLTAGE_FULLNAME					"Signal_FullName"
#define ANALOG_VOLTAGE_FULLNAME_SIG_TYPE		"Signal_FullName_SignalType"
#define ANALOG_VOLTAGE_FULLNAME_SIG_ID			"Signal_FullName_SignalID"
#define ANALOG_VOLTAGE_FULLNAME_VALUE_TYPE		"Signal_FullName_ValueType"
#define ANALOG_VOLTAGE_ABBREVNAME				"Signal_AbbrevName"
#define ANALOG_VOLTAGE_ABBREVNAME_SIG_TYPE		"Signal_AbbrevName_SignalType"
#define ANALOG_VOLTAGE_ABBREVNAME_SIG_ID		"Signal_AbbrevName_SignalID"
#define ANALOG_VOLTAGE_ABBREVNAME_VALUE_TYPE	"Signal_AbbrevName_ValueType"
#define ANALOG_VOLTAGE_LOW1LIMIT					"Signal_low1Limit"
#define ANALOG_VOLTAGE_LOW1LIMIT_SIG_TYPE		"Signal_low1Limit_SignalType"
#define ANALOG_VOLTAGE_LOW1LIMIT_SIG_ID			"Signal_low1Limit_SignalID"
#define ANALOG_VOLTAGE_LOW1LIMIT_VALUE_TYPE		"Signal_low1Limit_ValueType"
#define ANALOG_VOLTAGE_LOW1SEVERITY				"Signal_low1Severity"
#define ANALOG_VOLTAGE_LOW1SEVERITY_SIG_TYPE	"Signal_low1Severity_SignalType"
#define ANALOG_VOLTAGE_LOW1SEVERITY_SIG_ID		"Signal_low1Severity_SignalID"
#define ANALOG_VOLTAGE_LOW1SEVERITY_VALUE_TYPE	"Signal_low1Severity_ValueType"
#define ANALOG_VOLTAGE_LOW1RELAY					"Signal_low1Relay"
#define ANALOG_VOLTAGE_LOW1RELAY_SIG_TYPE		"Signal_low1Relay_SignalType"
#define ANALOG_VOLTAGE_LOW1RELAY_SIG_ID			"Signal_low1Relay_SignalID"
#define ANALOG_VOLTAGE_LOW1RELAY_VALUE_TYPE		"Signal_low1Relay_ValueType"
#define ANALOG_VOLTAGE_LOW2LIMIT					"Signal_low2Limit"
#define ANALOG_VOLTAGE_LOW2LIMIT_SIG_TYPE		"Signal_low2Limit_SignalType"
#define ANALOG_VOLTAGE_LOW2LIMIT_SIG_ID			"Signal_low2Limit_SignalID"
#define ANALOG_VOLTAGE_LOW2LIMIT_VALUE_TYPE		"Signal_low2Limit_ValueType"
#define ANALOG_VOLTAGE_LOW2SEVERITY							"Signal_low2Severity"
#define ANALOG_VOLTAGE_LOW2SEVERITY_SIG_TYPE				"Signal_low2Severity_SignalType"
#define ANALOG_VOLTAGE_LOW2SEVERITY_SIG_ID					"Signal_low2Severity_SignalID"
#define ANALOG_VOLTAGE_LOW2SEVERITY_VALUE_TYPE				"Signal_low2Severity_ValueType"
#define ANALOG_VOLTAGE_LOW2RELAY								"Signal_low2Relay"
#define ANALOG_VOLTAGE_LOW2RELAY_SIG_TYPE					"Signal_low2Relay_SignalType"
#define ANALOG_VOLTAGE_LOW2RELAY_SIG_ID						"Signal_low2Relay_SignalID"
#define ANALOG_VOLTAGE_LOW2RELAY_VALUE_TYPE					"Signal_low2Relay_ValueType"
#define ANALOG_VOLTAGE_HIGH1LIMIT					"Signal_high1Limit"
#define ANALOG_VOLTAGE_HIGH1LIMIT_SIG_TYPE			"Signal_high1Limit_SignalType"
#define ANALOG_VOLTAGE_HIGH1LIMIT_SIG_ID			"Signal_high1Limit_SignalID"
#define ANALOG_VOLTAGE_HIGH1LIMIT_VALUE_TYPE		"Signal_high1Limit_ValueType"
#define ANALOG_VOLTAGE_HIGH1SEVERITY					"Signal_high1Severity"
#define ANALOG_VOLTAGE_HIGH1SEVERITY_SIG_TYPE		"Signal_high1Severity_SignalType"
#define ANALOG_VOLTAGE_HIGH1SEVERITY_SIG_ID			"Signal_high1Severity_SignalID"
#define ANALOG_VOLTAGE_HIGH1SEVERITY_VALUE_TYPE		"Signal_high1Severity_ValueType"
#define ANALOG_VOLTAGE_HIGH1RELAY					"Signal_high1Relay"
#define ANALOG_VOLTAGE_HIGH1RELAY_SIG_TYPE			"Signal_high1Relay_SignalType"
#define ANALOG_VOLTAGE_HIGH1RELAY_SIG_ID			"Signal_high1Relay_SignalID"
#define ANALOG_VOLTAGE_HIGH1RELAY_VALUE_TYPE		"Signal_high1Relay_ValueType"
#define ANALOG_VOLTAGE_HIGH2LIMIT					"Signal_high2Limit"
#define ANALOG_VOLTAGE_HIGH2LIMIT_SIG_TYPE			"Signal_high2Limit_SignalType"
#define ANALOG_VOLTAGE_HIGH2LIMIT_SIG_ID			"Signal_high2Limit_SignalID"
#define ANALOG_VOLTAGE_HIGH2LIMIT_VALUE_TYPE		"Signal_high2Limit_ValueType"
#define ANALOG_VOLTAGE_HIGH2SEVERITY							"Signal_high2Severity"
#define ANALOG_VOLTAGE_HIGH2SEVERITY_SIG_TYPE				"Signal_high2Severity_SignalType"
#define ANALOG_VOLTAGE_HIGH2SEVERITY_SIG_ID					"Signal_high2Severity_SignalID"
#define ANALOG_VOLTAGE_HIGH2SEVERITY_VALUE_TYPE				"Signal_high2Severity_ValueType"
#define ANALOG_VOLTAGE_HIGH2RELAY							"Signal_high2Relay"
#define ANALOG_VOLTAGE_HIGH2RELAY_SIG_TYPE					"Signal_high2Relay_SignalType"
#define ANALOG_VOLTAGE_HIGH2RELAY_SIG_ID					"Signal_high2Relay_SignalID"
#define ANALOG_VOLTAGE_HIGH2RELAY_VALUE_TYPE				"Signal_high2Relay_ValueType"
#define ANALOG_VOLTAGE_ALM_LOW1_FULL							"Alarm_Low1_Full"
#define ANALOG_VOLTAGE_ALM_LOW2_FULL							"Alarm_Low2_Full"
#define ANALOG_VOLTAGE_ALM_HIGH1_FULL						"Alarm_High1_Full"
#define ANALOG_VOLTAGE_ALM_HIGH2_FULL						"Alarm_High2_Full"
#define ANALOG_VOLTAGE_ALM_LOW1_ABBR							"Alarm_Low1_Abbr"
#define ANALOG_VOLTAGE_ALM_LOW2_ABBR							"Alarm_Low2_Abbr"
#define ANALOG_VOLTAGE_ALM_HIGH1_ABBR						"Alarm_High1_Abbr"
#define ANALOG_VOLTAGE_ALM_HIGH2_ABBR						"Alarm_High2_Abbr"

#define ANALOG_SIGNAL_UNITS									"Signal_Units"
#define ANALOG_SIGNAL_UNITS_SIGNALTYPE						"Signal_Units_SignalType"
#define ANALOG_SIGNAL_UNITS_SIGNALID							"Signal_Units_SignalID"
#define ANALOG_SIGNAL_UNITS_VALUETYPE						"Signal_Units_ValueType"
#define ANALOG_SIGNAL_X1										"Signal_X1"
#define ANALOG_SIGNAL_X1_SIGNALTYPE							"Signal_X1_SignalType"
#define ANALOG_SIGNAL_X1_SIGNALID							"Signal_X1_SignalID"
#define ANALOG_SIGNAL_X1_VALUETYPE							"Signal_X1_ValueType"
#define ANALOG_SIGNAL_Y1										"Signal_Y1"
#define ANALOG_SIGNAL_Y1_SIGNALTYPE							"Signal_Y1_SignalType"
#define ANALOG_SIGNAL_Y1_SIGNALID							"Signal_Y1_SignalID"
#define ANALOG_SIGNAL_Y1_VALUETYPE							"Signal_Y1_ValueType"
#define ANALOG_SIGNAL_X2										"Signal_X2"
#define ANALOG_SIGNAL_X2_SIGNALTYPE							"Signal_X2_SignalType"
#define ANALOG_SIGNAL_X2_SIGNALID							"Signal_X2_SignalID"
#define ANALOG_SIGNAL_X2_VALUETYPE							"Signal_X2_ValueType"
#define ANALOG_SIGNAL_Y2										"Signal_Y2"
#define ANALOG_SIGNAL_Y2_SIGNALTYPE							"Signal_Y2_SignalType"
#define ANALOG_SIGNAL_Y2_SIGNALID							"Signal_Y2_SignalID"
#define ANALOG_SIGNAL_Y2_VALUETYPE							"Signal_Y2_ValueType"



#define AUTO_CONFIG_START_FALGE           	"VAR_AUTO_CONFIG_START_FLAGE"//Added by wj for Auto Config
#define SLAVE_MODE_FALGE           	"VAR_SLAVE_MODE_FALGE"//Added by wj for Auto Config
#define SMDU_CFG_FALGE           	"VAR_SMDU_CFG_FALGE"//Added by wj for Auto Config
#define EQUIP_NEED_REFRESH_CONVERT	"VAR_NEED_REFRESH_CONVERTER"

#define MAIN_FIFO_NAME					"/var/fifo2"
#define CGI_CLIENT_FIFO_PATH			"/var"

/*max len of fifo file name*/
#define FIFO_NAME_LEN					32

//changed by Frank Wu,5/N/35,20140527, for adding the the web setting tab page 'DI'
#define COMMAND_ARGS_BUF_MAX				1024
#define SINGLE_ARG_MAX_LEN					128
#define COMMAND_ARGS_SPLIT_CHAR				59	//the char ';' == 59


/**/
#define QUERY_HIS_DATA				0
#define QUERY_STAT_DATA				1
#define QUERY_HISALARM_DATA			2
#define QUERY_CONTROLCOMMAND_DATA	3
#define QUERY_HISLOG_DATA			4
#define QUERY_BATTERYTEST_DATA		5
#define QUERY_CLEAR_ALARM			6
#define QUERY_DISEL_TEST			7
//Start:Added by Wankun, 03/14/2007
#define QUERY_BATTERYTEST_SUM		8
#define CLEAR_HISTORY_DATA			9
#define CLEAR_HISTORY_LOG			10
#define CLEAR_COMMAND_LOG			11
#define CLEAR_BAT_TEST_LOG			12
//End:Added by Wankun, 03/14/2007


/*command type*/
#define MODIFY_SIGNAL_NAME					0
#define MODIFY_SIGNAL_VALUE					1
#define MODIFY_SIGNAL_ALARM_LEVEL			2
#define MODIFY_EQUIP_NAME					3
#define MODIFY_ACU_INFO						4
#define GET_EQUIP_SIGNAL_INFO				5
#define MODIFY_ALARM_ALL_INFO				6
#define GET_ACU_INFO						7
#define GET_DEVICE_LIST						8
#define CLEAR_HISTORY_DATA					9
#define RESTORE_DEFAULT_CONFIG				10
#define RESTART_ACU							11
#define GET_USER_DEF_PAGE					12
#define SET_USER_DEF_PAGE					13
#define MODIFY_LOC_LANG					14
#define FORGET_PASSWORD					15
#define HEARTBEAT_SIGNAL				16
#define GET_ALARM_INFO					17
#define SET_ALARM_INFO					18
//changed by Frank Wu,6/N/35,20140527, for adding the the web setting tab page 'DI'
#define GET_DI_INFO						19	//not be used, just reserve for later
#define SET_DI_INFO						20

#define GET_DO_INFO						21	//not be used, just reserve for later
#define SET_DO_INFO						22

#define GET_SHUNT_INFO						23
#define SET_SHUNT_INFO						24	//not be used.

#define SET_CUSTOM_INPUTS_INFO					25
#define SET_ANALOG_VOLTAGE_INFO					26
#define SET_ANALOG_TRANSDUCER_VOL_INFO			27
#define SET_ANALOG_TRANSDUCER_CUR_INFO			28


/*Multi Language*/
#define	ENGLISH_LANGUAGE_NAME			0
#define	LOCAL_LANGUAGE_NAME 			1
#define	LOCAL2_LANGUAGE_NAME 			2  ////Added by wj for three languages 2006.4.29

/*aim to send equip list*/
#define ENGLISH_START					59//198 ;
#define LOC2_START                      59//Added by wj for three languages 2006.4.29
#define TREE_LOC_START					59//204	;
#define TREE_LOC2_START					59//204	;//Added by wj for three languages 2006.4.29
#define TREE_ENG_START					59//201	
#define	ACU_LOC_INFO_START				59//202
#define	ACU_LOC2_INFO_START				59//202//Added by wj for three languages 2006.4.29
#define	ACU_ENG_INFO_START				59//203
#define LOC_EQUIPALARM_INFO_START		59
#define LOC2_EQUIPALARM_INFO_START		59//Added by wj for three languages 2006.4.29
#define ENG_EQUIPALARM_INFO_START		59
#define LOC_GROUP_DEVICE_START			59
#define LOC2_GROUP_DEVICE_START			59//Added by wj for three languages 2006.4.29
#define ENG_GROUP_DEVICE_START			59

/*aim to send realtime data*/
#define END_OF_SIGNUALNUM				59//197
#define END_OF_SAMPLESIG				59//198
#define	END_OF_CONTROLSIG				59//200
#define END_OF_SETTINGSIG				59//201
#define END_OF_ALARMSIG					59//202	
#define END_OF_RALARMSIG				59//203
#define END_OF_STATUSSIG				59//204
#define END_OF_RALARM					59




/*define Macro return to the cgi process*/
#define	MODIFY_SIGNALNAME_SUCCESSFUL	0
#define MODIFY_SIGNALNAME_FAIL			1
#define MODIFY_SIGNALVALUE_SUCCESSFUL	1
#define MODIFY_SIGNALVALUE_FAIL			2
#define MODIFY_SIGNALVALUE_INVALID      3
#define MODIFY_ALARMLEVEL_SUCCESSFUL	4
#define	MODIFY_ALARMLEVEL_FAIL			5
#define MODIFY_EQUIP_NAME_SUCCESSFUL	6
#define MODIFY_EQUIP_NAME_FAIL			7
#define	MODIFY_ACU_INFO_SUCCESSFUL		8
#define MODIFY_ACU_INFO_FAIL			9


//Added by YangGuoxin,9/4/2006
#define MODIFY_SIGNALVALUE_SUPPRESSED		10
//End by YangGuoxin,9/4/2006


#define NO_AUTHORITY					4      /*authority*/



/*define log file head*/
#define CGI_APP_LOG_COMM_NAME			"WEB COMM CGI"
#define CGI_APP_LOG_CONTROL_NAME		"WEB CTRL CGI"
#define CGI_APP_LOG_QUERY_NAME			"WEB QURY CGI"
#define CGI_APP_LOG_REALTIME_NAME		"WEB REAL CGI"
#define CGI_APP_LOG_CONFIGURE_NAME		"WEB COFG CGI"
#define CGI_APP_LOG_APPMAIN_NAME		"WEB MAIN CGI"
#define CGI_APP_LOG_APPPUB_NAME			"WEB PUB  CGI"


#define HTML_SAVE_REAL_PATH			"/app/www_user/html/loc"//"/var/tmp"
#define HTML_SAVE_REAL2_PATH			"/app/www/html/cgi-bin/loc2"//"/var/tmp" //Added by wj for three languages 2006.4.28
#define HTML_SAVE_ENG_PATH			"/app/www_user/html/eng"//"/var/tmp"

#define WEB_LOC_VAR				"/var/loc"
#define WEB_LOC2_VAR				"/var/loc2"//Added by wj for three languages 2006.4.28
#define WEB_ENG_VAR				"/var/eng"

#define	WEB_CONFIG_PATH					"/app/config"
#define WEB_RUN_CFG_PATH				"/app/config/run"

#define CGI_APP_OVERTIME_PAGE			"p17_login_overtime.htm"
/*Need return signal structure*/
#define NEED_RETURN_SIGNAL_STRUCTURE	1

#define GET_NETWORK_INFO				0
#define SET_NETWORK_INFO				1
#define GET_NMS_INFO					2
#define SET_NMS_INFO					3
#define GET_ESR_INFO					4		
#define SET_ESR_INFO					5	
#define GET_TIME_INFO					6
#define SET_TIME_VALUE					7
#define SET_TIME_IP					8
#define GET_USER_INFO					9			/*Get All user*/
#define SET_USER_INFO					10			/*Modify user info*/
#define ADD_USER_INFO					11			/*Add user info*/
#define DELETE_USER_INFO				12			/*Delete user info*/
#define SET_USER_PASSWORD				13			/*Modify user info*/

//Added by YangGuoxin; 1/19/2006
//Get prodcut info
#define GET_PRODUCT_INFO				14			
//End Added by YangGuoxin; 1/19/2006



#define CGI_GET_SETTING_TYPE			"setting_type"
#define CGI_GET_AUTH_NAME				"cgi_auth_name"
#define CGI_ADMIN_NAME					"admin"
#define CGI_READ_NAME					"read"
#define CGI_WRITE_NAME					"write"
#define CGI_DI_DISABLED					"DI_DISABLED"

//////////////////////////////////////////////////////////////////////////
//Added by wj for Config PLC private config file 2006.5.13

#define SET_PLC_CONFIG                  15
#define DEL_PLC_CONFIG                  16
#define GET_PLC_CONFIG                  17
//Added by wj for AlarmSupExp Configuration 2006.5.22
#define GET_ALARM_CONFIG                18
#define SET_ALARM_CONFIG	            19
//Added by wj for AlarmReg Configuration 2006.6.23
#define GET_ALARMREG_CONFIG	            20
#define SET_ALARMREG_CONFIG	            21
//Added by wj for YDN23 Configuration 2006.7.26
#define GET_YDN_CONFIG	            22
#define SET_YDN_CONFIG	            23
//add by wj for Power splite
#define GET_GC_PS_CONFIG                   24
#define SET_GC_PS_CONFIG                   25
#define SET_GC_PS_MODE                     26

#define GET_SETTING_PARAM                  27
#define GET_AUTO_CONFIG                    28
#define GET_NMSV3_INFO					29
#define SET_NMSV3_INFO					30
//end////////////////////////////////////////////////////////////////////////

#define GET_SMTP_CONFIG	            31
#define SET_SMTP_CONFIG	            32

#define GET_DHCPSERVER_CONFIG	            33

#define GET_VPN_CONFIG			34
#define SET_VPN_CONFIG			35

#define GET_SMS_CONFIG			36
#define SET_SMS_CONFIG			37


#define GET_MODBUS_CONFIG	            38
#define SET_MODBUS_CONFIG	            39

#define GET_SNMP_TRAP			40
#define SET_SNMP_TRAP			41
#define SET_CABINET			42
#define READ_CABINET			43
#define RESET_CABINET			44
#define SET_CABINET_PARA		45
#define SET_CB_PARA			46
#define GET_V6_NETWORK_INFO		47
#define SET_V6_NETWORK_INFO		48
#define GET_V6_DHCPSERVER_CONFIG	49
#define GET_TL1_CONFIG			50
#define SET_TL1_CONFIG			51
#define GET_TL1_GROUP			52
#define SET_TL1_GROUP			53
#define GET_TL1_SIGNAL			54
#define SET_TL1_SIGNAL			55
#define GET_TL1_EQUIP			56

#define FIRST_CHANGE_PASSWORD			57
#define EXPORT_SETTINGS			58
#define LCD_LOGIN_ADMIN_LOGGED	59			/*Avoid logged in ADMIN account delete*/


#define SESSION_DEFAULT_DIR				"/var/sess_"
#define SPACE   (0x20)
#define TAB     (0x09)
#define WEB_IS_WHITE_SPACE(c)         ( ((c) == SPACE) || ((c) == TAB) )

char *set_session(IN char *name,IN char *pwd,IN char *strAuthority);
int start_session(IN const char *sessionId);
void kill_session(IN char *sessionId);
void print_session_error(IN const int n, IN int iLanguage);
void print_no_authority_error(IN const int n);
void clean_session_file(void);
int find_session_file(void);

int ReplaceString( char **ppsrc, const char *strOld, char *strNew );

//void MakeModiTime(OUT char * strModiTime,int nLen);
int LoadHtmlFile(IN const char *szFile, OUT char **pbuf);
void PostPage(IN char * pbuf);
long FileLength(IN FILE *fp );
char *CGI_PUB_GetData(IN char *ptr, IN char cSplit);
char *Web_RemoveWhiteSpace(IN char *pszBuf);
char *Web_SET_MakeConfigurePagePath(IN const char *szPrePath, IN int iLanguage);
int ResetTime(IN const char *sessionId);
int start_sessionA(IN const char *sessionId, IN char **szUserName);
int CGI_ClearProc(char *lpMsg);
void CGI_ClearSessionFile(IN char *szSessionID);
void refresFileTime(void);
char *getUserName(IN char *sessionId);
char *Cfg_RemoveWhiteSpace(IN char *pszBuf);
BOOL Web_CheckUser(IN int nPagesNeedAuthority);

size_t	strlcpy(char *dst, const char *src, size_t dsize);
size_t	strlcat(char *dst, const char *src, size_t dsize);

enum WEB_NET_SETTING_R
{
	WEB_NET_SETTING_IP = 0,
	WEB_NET_SETTING_EEM,
	WEB_NET_SETTING_TIME,
	WEB_NET_SETTING_NMS
};

enum WEB_NET_PAGES
{
	WEB_NET_PAGES_READ,
	WEB_NET_PAGES_WRITE,
	WEB_NET_PAGES_ADMIN,
};
BOOL IsValidWORD(const char *pField);

#endif

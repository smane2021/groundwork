/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define WEB_ADD_PRODUCT_INFO		1

#define CONFIGURE_OF_NETWORK		0
#define CONFIGURE_OF_NMS			1
#define CONFIGURE_OF_ESR			2
#define CONFIGURE_OF_USER			3
#define CONFIGURE_OF_TIME			4

#ifdef WEB_ADD_PRODUCT_INFO
	#define CONFIGURE_OF_PRODUCTINFO	5
#endif

//////////////////////////////////////////////////////////////////////////
//Added by wj for Config PLC private config file 2006.5.13

#define CONFIGURE_OF_PLC            6
#define CONFIGURE_OF_ALARMS         7
#define CONFIGURE_OF_ALARMREG       8
#define CONFIGURE_OF_YDN            9
#define CONFIGURE_OF_GCPS           10
#define CONFIGURE_OF_SETTINGPARAM   11
#define CONFIGURE_OF_AUTOCONFIG     12

//////////////////////////////////////////////////////////////////////////
#define CONFIGURE_OF_SMTP            13
#define CONFIGURE_OF_DHCPSERVER      14
#define CONFIGURE_OF_VPN		15
#define CONFIGURE_OF_SMS		16

//////////////////////////////////////////////////////////////////////////

#define CONFIGURE_OF_MODBUS            17
#define CONFIGURE_OF_CABINET		18
#define CONFIGURE_OF_TL1		19

//////////////////////////////////////////////////////////////////////////
#define CONFIGURE_OF_SETTINGS_EXPORT	20
//////////////////////////////////////////////////////////////////////////
//For SMTP
#define CFG_SMTP_SENDTO                      "_SMTPsendto"
#define CFG_SMTP_SERVERIP			"_SMTPserverIp"
#define CFG_SMTP_SERVERPORT			"_SMTPserverPort"
#define CFG_SMTP_ACCOUNT			"_SMTPaccount"
#define CFG_SMTP_PASSWORD			"_SMTPpassword"
#define CFG_SMTP_AUTHEN				"_SMTPauthen"
#define CFG_SMTP_ALARMLEVEL			"_SMTPalarmLevel"
#define CFG_SMTP_FROM				"_SMTPsendfrom"

//For VPN

#define	CFG_VPN_ENABLE		"_VPNenable"
#define	CFG_VPN_REMOTE_IP	"_VPNip"
#define	CFG_VPN_REMOTE_PORT	"_VPNport"

//For SMS
#define CFG_SMS_PHONE1		"_SMSphone1"
#define CFG_SMS_PHONE2		"_SMSphone2"
#define CFG_SMS_PHONE3		"_SMSphone3"
#define CFG_SMS_ALARMLEVEL	"_SMSalarmLevel"


//For Config PLC
#define CFG_PLC_ADD                     "addRowContent"
#define CFG_PLC_DEL                     "deleteRowId"

//For Config Alarm Suppressing Exp
#define CFG_ALARM_EQUIPTYPEID           "_stdEquipTypeId"
#define CFG_ALARM_ALARMID               "_alarmId"
#define CFG_ALARM_ALARMSUPEXP           "_alarmSuppressExp"

//For Config Alarm Reg
#define CFG_ALARM_ALARMREG           "_alarmReg"

//end////////////////////////////////////////////////////////////////////////

/*For network configure*/
#define CGI_CLIENT_IP_NAME				"_ip"
#define	CGI_CLIENT_MASK_NAME			"_mask"
#define CGI_CLIENT_GATE_NAME			"_gate"
#define CGI_CLIENT_PREFIX			"_prefix"

/*for user manage*/
#define CGI_USER_NAME					"_name"
#define CGI_USER_PASSWORD				"_password1"
#define CGI_USER_LEVEL					"_authority_level"
//changed by Frank Wu,6/N/15,20140527, for e-mail
#define CGI_USER_CONTACT_ADDR				"_contact"
#define CGI_LCD_lOGIN_ONLY				"_lcdEnable"
#define CGI_ACC_COUNT_EN				"_LoginLimit"
#define CGI_ACC_LOCK_UNLOCK				"_UnlockAccount"
#define CGI_ACC_STATUS					"_AccountStatus"
#define CGI_STRONG_PASS					"_strongPass"

#define CGI_USER_OLD_PASSWORD		"_oldpassword"

#define USER_INFO_LIST					"_user_list"

#define WEB_CGI_TIME_SERVER1			"_time_server1"
#define WEB_CGI_TIME_SERVER2			"_time_server2"
#define WEB_CGI_TIME_INTERVAL			"_time_interval"
#define WEB_CGI_TIME_ZONE			"_time_zone_new"

#define CGI_NMS_LIST					"_nms_list"
#define CGI_ESR_LIST					"_esr_list"

#define CGI_MODIFY_CONFIGURE			"_modify_configure"
#define CGI_MODIFY_CONFIGURE_DETAIL		"_modify_configure_detail"

/*for time*/
#define CGI_TIME_TYPE					"_time_type"
#define CGI_TIME_MAIN_IP				"_time_server1"
#define CGI_TIME_BAK_IP					"_time_server2"
#define CGI_TIME_INTERVAL				"_interval_adjust"
#define CGI_TIME_ZONE					"_time_zone"
#define CGI_TIME_SET_TIME				"_submitted_time"

/*for esr*/
#define CGI_ESR_PROTOCOL_TYPE			"_protocol_type_ex"
#define CGI_ESR_PROTOCOL_MEDIA			"_protocol_media_ex"
#define CGI_ESR_MAX_ALARM_ATTEMPTS		"_esr_max_alarm_report_attempts_ex"
#define CGI_ESR_CALL_ELAPSE_TIME		"_esr_call_elapse_time_ex"
#define CGI_ESR_MAIN_REPORT_PHONE		"_main_report_phone_ex"
#define CGI_ESR_SECOND_REPORT_PHONE		"_second_report_phone_ex"
#define CGI_ESR_CALLBACK_PHONE			"_callback_phone_ex"
#define CGI_ESR_REPORT_IP1				"_report_ip1_ex"
#define CGI_ESR_REPORT_IP2				"_report_ip2_ex"
#define CGI_ESR_SECURITY_IP1			"_security_ip1_ex"
#define CGI_ESR_SECURITY_IP2			"_security_ip2_ex"
#define CGI_ESR_SECURITY_LEVEL			"_securitylevel_ex"
#define CGI_ESR_CALLBACK_INUSE			"_callbackinuse_ex"
#define CGI_ESR_REPORT_INUSE			"_reportinuse_ex"
#define CGI_ESR_CCID					"_ccid_ex"
#define CGI_ESR_SOCID					"_socid_ex"
#define	CGI_ESR_COMMANDPARAM			"_commonparam_ex"

#define CGI_PRODUCT_INFO				"acu_barcode_information"

/*for YDN*/
#define CGI_YDN_PROTOCOL_TYPE			"_protocol_type_ydn"
#define CGI_YDN_PROTOCOL_MEDIA			"_protocol_media_ydn"
#define CGI_YDN_REPORT_INUSE			"_reportinuse_ydn"
#define CGI_YDN_MAX_ALARM_ATTEMPTS		"_ydn_max_alarm_report_attempts_ex"
#define CGI_YDN_CALL_ELAPSE_TIME		"_ydn_call_elapse_time_ex"
#define CGI_YDN_MAIN_REPORT_PHONE		"_main_report_phone_ydn"
#define CGI_YDN_SECOND_REPORT_PHONE		"_second_report_phone_ydn"
#define CGI_YDN_CALLBACK_PHONE			"_callback_phone_ydn"
#define	CGI_YDN_COMMANDPARAM			"_commonparam_ydn"
#define CGI_YDN_CCID					"_ccid_ydn"

#define CGI_YDN_LIST					"_ydn_list"

/*for TL1*/
#define CGI_TL1_PORT_ACTIVE			"_port_active_ex"
#define CGI_TL1_PROTOCOL_MEDIA			"_protocol_media_ex"
#define CGI_TL1_PORT_NUM			"_acess_port_ex"
#define CGI_TL1_PORT_ALIVE			"_port_keep_alive_ex"
#define CGI_TL1_TIMEOUT				"_session_timeout_ex"
#define CGI_TL1_LOGON_USER			"_logon_user_ex"
#define CGI_TL1_IDENTIFIER			"_system_identifier_ex"
#define CGI_TL1_INDEX				"_index"
#define CGI_TL1_EQUIP				"_equip"
#define CGI_TL1_NAME				"_name"
#define CGI_TL1_PREFIX				"_prefix"
#define CGI_TL1_TYPE				"_type"

#define CGI_TL1_SIGNAL_INDEX				"_signal_index"
#define CGI_TL1_SIGNAL_ACTIVE				"_SigActive1"
#define CGI_TL1_SIGNAL_SAMPLE				"_SampleSig"
#define CGI_TL1_SIGNAL_ALARM				"_AlarmSig"
#define CGI_TL1_SIGNAL_SETTING				"_SettingSig"
#define CGI_TL1_SIGNAL_CON_TYPE				"_CondType"
#define CGI_TL1_SIGNAL_CON_DESC				"_CondDesc"
#define CGI_TL1_SIGNAL_NOTI_CODE			"_NotificationCode"
#define CGI_TL1_SIGNAL_SERV_CODE			"_ServiceAffectCode"
#define CGI_TL1_SIGNAL_MONI_FORMAT			"_MonitorFormat"
#define CGI_TL1_SIGNAL_MONI_TYPE			"_MonitorType"



//for GC_PS
#define CGI_PS_MODE                     "_powerSpliteMode"
#define CGI_PS_INFO                     "_powerSpliteExp"

//for Cabinet
#define CGI_CABINET			"_cabinet"
#define CGI_CABINET_CONTENT		"_content"

char			strResult[1000] = {"{\"status\":/*[CONTROL_RESULT]*/,\"content\":[/*[CONTENT_RESULT]*/]}"};

typedef struct tagCommandInfo
{
	int		iConfigureType;		/*Configure type 0:network 1:nms 2:esr 3 : User 4:Time*/
	int		iCommandyType;		/*other than user : 0: Get 1: Set for user : 0:Get 1:Modify 2 :Add 3:Delete*/
	char	szSessionID[64];
	int		iLanguage;		
}ConfigureCommandInfo;

#ifdef WEB_ADD_PRODUCT_INFO
static char CFG_HTML_PATH[14][64] = {"p11_network_config.htm",
									"p12_nms_config.htm",
									"p13_esr_config.htm",
									"p14_user_config.htm",
									"p19_time_config.htm",
									"p30_acu_signal_value.htm",
                                    "p39_edit_config_plc.htm",
                                    "p42_edit_config_alarm.htm",
                                    "p41_edit_config_alarmReg.htm",
                                    "p13_esr_config.htm",
									"p44_cfg_powersplite.htm",
									"p78_get_setting_param.htm",
									"p77_auto_config.htm",
									"p12_nmsv3_config.htm"};
#else
static char CFG_HTML_PATH[10][64] = {"p11_network_config.htm",
									"p12_nms_config.htm",
									"p13_esr_config.htm",
									"p14_user_config.htm",
									"p19_time_config.htm",
                                    "p39_edit_config_plc.htm",
                                    "p42_edit_config_alarm.htm",
                                    "p41_edit_config_alarmReg.htm",
                                    "p43_ydn_config.htm",
									"p44_cfg_powersplite.htm"};
#endif

static void Web_SET_PostUserInfoPage(IN char *pGetBuf,IN int iLanguage);
static char *Web_SET_SendConfigureCommand(IN char *szBuf, IN int iModifyPassword);	//iModifyPassword = 13:Modify password
static int Web_SET_GetCommandParam(IN ConfigureCommandInfo *stConfigure, OUT char **pBuf);
static void Web_SET_PostNetWorkPage(IN char *pGetBuf, IN int iLanguage);
static void Web_SET_PostV6NetWorkPage(IN char *pGetBuf, IN int iLanguage);
static int Web_SET_PostCFGPLCWorkPage(IN char *pGetBuf, IN int iLanguage);//Added by wj for configure PLC 2006.5.13
static int Web_SET_PostCFGALARMSWorkPage(IN char *pGetBuf, IN int iLanguage);//Added by wj for configure AlarmSupExp 2006.5.13
static int Web_SET_PostCFGALARMRegWorkPage(IN char *pGetBuf, IN int iLanguage);//Added by wj for configure AlarmSupExp 2006.5.13
static void Web_SET_PostTimeInfoPage(IN  char *pGetBuf, IN int iLanguage, IN const char *sessionId);
char *Web_RemoveWhiteSpace(IN char *pszBuf);
static void Web_SET_PostESRPage(IN char *pGetBuf, IN int iLanguage, IN int iESRType);
static void Web_SET_PostNMSPage(IN  char *pGetBuf, IN int iLanguage);
static void Web_CGI_PostNoAuthority(IN int iModifyType, IN int iLanguage,IN BOOL boolDeleteSelf);
static void Web_SET_PostYDN_TL1Page(IN char *pGetBuf, IN int iLanguage, IN int iESRType);
static void Web_GET_PostProductInfo(IN char *pGetBuf, IN int iLanguage, IN const char *sessionId);
static int Web_SET_PostCFGGCPSWorkPage(IN char *pGetBuf, IN int iLanguage);
static int Web_SET_PostCFGGetSettingParamWorkPage(IN char *pGetBuf, IN int iLanguage);
static int Web_SET_PostAutoConfigWorkPage(IN int iLanguage);
//static void Web_SET_PostSMTPPage(IN char *pGetBuf, IN int iLanguage, IN int iSMTPType);
static void Web_SET_PostDHCPServerPage(IN char *pGetBuf, IN int iLanguage, IN int iDHCPServerType);
//static void Web_SET_PostVPNPage(IN char *pGetBuf, IN int iLanguage, IN int iVPNType);
//static void Web_SET_PostSMSPage(IN char *pGetBuf, IN int iLanguage, IN int iSMSType);
//static void Web_SET_PostMODBUSPage(IN char *pGetBuf, IN int iLanguage, IN int iModbusType);
static void Web_CGI_PostAutoConfigNoAuthority();
//changed by Frank Wu,5/N/27,20140527, for power split
static void Web_SET_PostCFGGCPSWorkPage2(IN char *pGetBuf, IN int iLanguage, IN int iCmdType);
char		szUserName[32];
char        *pUserNameTransfer;

int main(void)
{

	int							iLanguage = 0;				
	//char						*pHtml = NULL;
	//char						szExchange[8];
	char						*szGetBuf = NULL;
	ConfigureCommandInfo		stConfigureCommand;
	char						*szReturnBuf = NULL;
	int							iAuthority = 0;
	char						*szSessName = NULL ;
	//char						*pTemp = NULL;


	printf("Content-type:text/html\n\n");
	printf("\n\n");

	char	*pHtml = NEW(char,1000);
	if(pHtml == NULL)
	{
		return;
	}
	sprintf(pHtml,"%s",strResult);

	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");

	/*get parameter*/
	
	if (Web_SET_GetCommandParam(&stConfigureCommand, &szGetBuf) != FALSE)
	{
		/*judge overtime and authority*/
		//sprintf(szSessionID,"%s",stConfigureCommand.szSessionID);
		//TRACE("iAuthority : %s", stConfigureCommand.szSessionID);

		iAuthority = start_sessionA(Web_RemoveWhiteSpace(stConfigureCommand.szSessionID), &pUserNameTransfer);//start_session(Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));

/*
		FILE *pf1=NULL;
		pf1=fopen("/var/Fenge111.txt","wb");
		sprintf(pHtml,"szSessionID=%s,iCommandyType=%d,iConfigureType=%d,iAuthority=%d,  szGetBuf = %s\n",stConfigureCommand.szSessionID,stConfigureCommand.iCommandyType,stConfigureCommand.iConfigureType,iAuthority,szGetBuf);
		fwrite(pHtml,strlen(pHtml),1,pf1);
		fclose(pf1);
*/

		//printf("%d",iAuthority);
		//printf("%s",stConfigureCommand.szSessionID);

		/*language type*/
		if(stConfigureCommand.iLanguage > 0)
		{
		    if(stConfigureCommand.iLanguage == 1)
		    {
			iLanguage =	LOCAL_LANGUAGE_NAME;
		    }
		    else
		    {
			iLanguage =	LOCAL2_LANGUAGE_NAME;
		    }
			
		}
		else
		{
			iLanguage = ENGLISH_LANGUAGE_NAME;
		}

		//printf("\n\n");

		if(iAuthority < 0)			//iAuthority < 0 , it is the session fault,
										//not the authority fault

		{
			if( stConfigureCommand.iCommandyType == FIRST_CHANGE_PASSWORD )
			{
				/*
					FILE *pf2=NULL;
					pf2=fopen("/var/Fenge222test.txt","wb");
					fwrite("222",strlen("222"),1,pf2);
					fclose(pf2);
				*/
			}
			else
			{
				if(stConfigureCommand.iCommandyType != GET_NETWORK_INFO &&
				    stConfigureCommand.iCommandyType != GET_V6_NETWORK_INFO &&
					stConfigureCommand.iCommandyType != GET_NMS_INFO &&
					stConfigureCommand.iCommandyType != GET_NMSV3_INFO &&
					stConfigureCommand.iCommandyType != GET_ESR_INFO &&
					stConfigureCommand.iCommandyType != GET_TL1_CONFIG &&
					stConfigureCommand.iCommandyType != GET_TL1_GROUP &&
					stConfigureCommand.iCommandyType != GET_TL1_SIGNAL &&
					stConfigureCommand.iCommandyType != GET_TL1_EQUIP &&
					stConfigureCommand.iCommandyType != GET_TIME_INFO &&
					stConfigureCommand.iCommandyType != GET_USER_INFO && 
					 stConfigureCommand.iCommandyType != GET_PRODUCT_INFO &&
					 stConfigureCommand.iCommandyType != GET_PLC_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_ALARM_CONFIG &&
					//changed by Frank Wu,6/N/27,20140527, for power split
					 stConfigureCommand.iCommandyType != GET_GC_PS_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_ALARMREG_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_YDN_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_SETTING_PARAM &&
					 stConfigureCommand.iCommandyType != GET_AUTO_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_SMTP_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_DHCPSERVER_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_V6_DHCPSERVER_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_VPN_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_SMS_CONFIG &&
					 stConfigureCommand.iCommandyType != GET_MODBUS_CONFIG &&
					 stConfigureCommand.iCommandyType != READ_CABINET &&
					 stConfigureCommand.iCommandyType != GET_SNMP_TRAP ||
					 stConfigureCommand.iCommandyType == GET_SETTING_PARAM)
				{
					//print_session_error(iAuthority,iLanguage);
					ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "98");
					ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");

					PostPage(pHtml);

					DELETE(pHtml);
					pHtml = NULL;


					DELETE(szGetBuf);
					szGetBuf = NULL;

					if(pUserNameTransfer != NULL)
					{
					    DELETE(pUserNameTransfer);
					    pUserNameTransfer = NULL;
					}
					return FALSE;
				}
			}
			
			if(stConfigureCommand.iConfigureType == CONFIGURE_OF_AUTOCONFIG)
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "98");
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");

				PostPage(pHtml);

				DELETE(pHtml);
				pHtml = NULL;


				DELETE(szGetBuf);
				szGetBuf = NULL;

				if(pUserNameTransfer != NULL)
				{
					DELETE(pUserNameTransfer);
					pUserNameTransfer = NULL;
				}
				return FALSE;

			}
		}
		else
		{
			if(stConfigureCommand.iCommandyType == ADD_USER_INFO ||
				stConfigureCommand.iCommandyType == DELETE_USER_INFO ||
				stConfigureCommand.iCommandyType == LCD_LOGIN_ADMIN_LOGGED)
			{
				if(iAuthority < 4)
				{
						Web_CGI_PostNoAuthority(stConfigureCommand.iCommandyType, iLanguage,FALSE);
						if(szGetBuf != NULL)
						{
							DELETE(szGetBuf);
							szGetBuf = NULL;
						}
					    if(pUserNameTransfer != NULL)
					    {
						DELETE(pUserNameTransfer);
						pUserNameTransfer = NULL;
					    }
					    if(pHtml != NULL)
					    {
						DELETE(pHtml);
						pHtml = NULL;
					    }

					return 0;
				}
				else
				{
					szSessName = getUserName(Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));
					if(szSessName != NULL && strcmp(Web_RemoveWhiteSpace(szUserName), Web_RemoveWhiteSpace(szSessName)) == 0)
					{
						Web_CGI_PostNoAuthority(stConfigureCommand.iCommandyType, iLanguage,TRUE);
						if(szGetBuf != NULL)
						{
							DELETE(szGetBuf);
							szGetBuf = NULL;
						}

						if(szSessName != NULL)
						{
							DELETE(szSessName);
							szSessName = NULL;
						}
						if(pUserNameTransfer != NULL)
						{
						    DELETE(pUserNameTransfer);
						    pUserNameTransfer = NULL;
						}
						if(pHtml != NULL)
						{
						    DELETE(pHtml);
						    pHtml = NULL;
						}

						return 0;
					}
					if(szSessName != NULL)
					{
						DELETE(szSessName);
						szSessName = NULL;
					}
					
				}
								 
			}
			else if( stConfigureCommand.iCommandyType == SET_USER_INFO  && iAuthority != 4 )
			{
				
				szSessName = getUserName(Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));
				
				if(szSessName == NULL || strcmp(Web_RemoveWhiteSpace(szUserName), Web_RemoveWhiteSpace(szSessName)) != 0)			 
				{
					Web_CGI_PostNoAuthority(SET_USER_INFO, iLanguage, FALSE);
					if(szSessName != NULL)
					{
						DELETE(szSessName);
						szSessName = NULL;
					}
					
					if(szGetBuf != NULL)
					{
						DELETE(szGetBuf);
						szGetBuf = NULL;
					}
					if(pUserNameTransfer != NULL)
					{
						DELETE(pUserNameTransfer);
						pUserNameTransfer = NULL;
					}
					if(pHtml != NULL)
					{
					    DELETE(pHtml);
					    pHtml = NULL;
					}
					return 0;
				}

				if(szSessName != NULL)
				{
					DELETE(szSessName);
					szSessName = NULL;
				}
				stConfigureCommand.iCommandyType = SET_USER_PASSWORD;
		
			}
			else if(iAuthority < 3 )	//5,6,7,8 is the authority level
										//1 - Browser     2 - operator
										//3 - Engineer    4-Administrator
										//Engineer and administrator have privelege
										//to control command;
										//Brower and operator have no privelege.
			{
				 if(stConfigureCommand.iCommandyType == SET_NETWORK_INFO ||
				     stConfigureCommand.iCommandyType == SET_V6_NETWORK_INFO || 
							stConfigureCommand.iCommandyType ==SET_NMS_INFO || 
							stConfigureCommand.iCommandyType ==SET_NMSV3_INFO ||
							stConfigureCommand.iCommandyType ==SET_TIME_VALUE||
							stConfigureCommand.iCommandyType ==SET_TIME_IP ||
                            stConfigureCommand.iCommandyType == SET_PLC_CONFIG ||
                            stConfigureCommand.iCommandyType == DEL_PLC_CONFIG ||
                            stConfigureCommand.iCommandyType == SET_ALARM_CONFIG ||
                            stConfigureCommand.iCommandyType == SET_ALARMREG_CONFIG ||
                            stConfigureCommand.iCommandyType == SET_YDN_CONFIG ||
			    stConfigureCommand.iCommandyType == SET_MODBUS_CONFIG ||
			    stConfigureCommand.iConfigureType == CONFIGURE_OF_AUTOCONFIG ||
			    stConfigureCommand.iCommandyType == SET_CABINET ||
			    stConfigureCommand.iCommandyType == SET_CABINET_PARA ||
			    stConfigureCommand.iCommandyType == SET_CB_PARA ||
			    stConfigureCommand.iCommandyType == RESET_CABINET ||
			    stConfigureCommand.iCommandyType == SET_TL1_CONFIG ||
			    stConfigureCommand.iCommandyType == SET_TL1_GROUP ||
			    stConfigureCommand.iCommandyType == SET_TL1_SIGNAL ||
			    stConfigureCommand.iCommandyType == SET_SNMP_TRAP)
				{
					if(stConfigureCommand.iConfigureType == CONFIGURE_OF_AUTOCONFIG)
					{
						Web_CGI_PostAutoConfigNoAuthority();
					}
					else
					{
						Web_CGI_PostNoAuthority(stConfigureCommand.iCommandyType, iLanguage, FALSE);
					}


					
					if(szGetBuf != NULL)
					{
						DELETE(szGetBuf);
						szGetBuf = NULL;
					}

					if(pUserNameTransfer != NULL)
					{
						DELETE(pUserNameTransfer);
						pUserNameTransfer = NULL;
					}
					if(pHtml != NULL)
					{
					    DELETE(pHtml);
					    pHtml = NULL;
					}

					return 0;
				}
				else if(stConfigureCommand.iCommandyType == SET_ESR_INFO)
				{
					Web_CGI_PostNoAuthority(SET_ESR_INFO, iLanguage,FALSE);

					if(szGetBuf != NULL)
					{
						DELETE(szGetBuf);
						szGetBuf = NULL;
					}
					if(pUserNameTransfer != NULL)
					{
						DELETE(pUserNameTransfer);
						pUserNameTransfer = NULL;
					}
					if(pHtml != NULL)
					{
					    DELETE(pHtml);
					    pHtml = NULL;
					}

					return 0;
				}

			}

		}


		//TRACE("szGetBuf : %s\n", szGetBuf);
		switch(stConfigureCommand.iConfigureType)
		{
			
			case CONFIGURE_OF_NETWORK://Network
				{
					//if(iAuthority < 3 && stConfigureCommand.iCommandyType == SET_NETWORK_INFO)
					//{
					//	Web_SET_PostNetWorkPage(szReturnBuf, iLanguage, iAuthority);
					//}
					
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
					    if((stConfigureCommand.iCommandyType == GET_NETWORK_INFO) || (stConfigureCommand.iCommandyType == SET_NETWORK_INFO))
					    {
						Web_SET_PostNetWorkPage(szReturnBuf, iLanguage);
					    }
					    else
					    {
						Web_SET_PostV6NetWorkPage(szReturnBuf, iLanguage);
					    }
					}
				
				}
				break;
			case CONFIGURE_OF_NMS://NMS
				{
					szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType);
					if(szReturnBuf != NULL)
					{
					    if(stConfigureCommand.iCommandyType == GET_NMS_INFO)
					    {
						/*FILE *pf10=NULL;
						pf10=fopen("/var/wj11.txt","wb");
						fwrite(szReturnBuf,strlen(szReturnBuf),1,pf10);
						fclose(pf10);*/
					    }
						Web_SET_PostNMSPage(szReturnBuf,iLanguage);
					}
				}
				break;
			case CONFIGURE_OF_CABINET:
			    {
				szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType);
				if(szReturnBuf != NULL)
				{
				    if(stConfigureCommand.iCommandyType == READ_CABINET)
				    {
					FILE *pf2=NULL;
					//pf2=fopen("/var/file2.html","wb");
					//fwrite(szReturnBuf,strlen(szReturnBuf),1,pf2);
					//fclose(pf2);
				    }
				    else if(stConfigureCommand.iCommandyType == SET_CABINET)
				    {
					FILE *pf7=NULL;
					//pf7=fopen("/var/file7.html","wb");
					//fwrite(szReturnBuf,strlen(szReturnBuf),1,pf7);
					//fclose(pf7);
				    }
				    Web_SET_PostNMSPage(szReturnBuf,iLanguage);
				}
			    }
			    break;
			case CONFIGURE_OF_ESR://ESR
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{

						
						Web_SET_PostESRPage(szReturnBuf,iLanguage,stConfigureCommand.iCommandyType);
					}
				}
				break;
			case CONFIGURE_OF_USER://User
			{
				if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
				{

					Web_SET_PostUserInfoPage(szReturnBuf,iLanguage);
						
				}
				break;
			}
			case CONFIGURE_OF_TIME://Time
				if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
				{
				    if(stConfigureCommand.iCommandyType == SET_TIME_VALUE)
				    {
					refresFileTime();
				    }
					//printf("szReturnBuf : %s\n", szReturnBuf);

					Web_SET_PostTimeInfoPage(szReturnBuf, iLanguage,Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));
				}
				break;


            case CONFIGURE_OF_YDN://Config YDN Setting
                {


                    if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
                    {
                        //TRACE("szReturnBuf : %s\n", szReturnBuf);

                        Web_SET_PostYDN_TL1Page(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);
                    }
                    break;


                }

	    case CONFIGURE_OF_TL1:
		{
		    if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
		    {
			//TRACE("szReturnBuf : %s\n", szReturnBuf);

			Web_SET_PostYDN_TL1Page(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);
		    }
		    break;

		}

	    case CONFIGURE_OF_MODBUS://Config YDN Setting
		    {


			    if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
			    {
				    //TRACE("szReturnBuf : %s\n", szReturnBuf);

				    Web_SET_PostYDN_TL1Page(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);
			    }
			    break;


		    }
			//changed by Frank Wu,7/N/27,20140527, for power split
			case CONFIGURE_OF_GCPS://Config PLC private config file
			{
				if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
				{
					//TRACE("szReturnBuf : %s\n", szReturnBuf);
					//changed by Frank Wu,8/N/27,20140527, for power split
					//Web_SET_PostCFGGCPSWorkPage(szReturnBuf, iLanguage);
					Web_SET_PostCFGGCPSWorkPage2(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);
				}
				break;
			}

			case CONFIGURE_OF_SETTINGPARAM://Get Setting Param
				{
				    char	szResult[4];
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
					    strncpyz(szResult, szReturnBuf, 3);
					    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
					    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
					    PostPage(pHtml);
						//Web_SET_PostCFGGetSettingParamWorkPage(szReturnBuf, iLanguage);
					}
					break;
				}

			case CONFIGURE_OF_AUTOCONFIG:
				{
					Web_SET_SendConfigureCommand(szGetBuf,stConfigureCommand.iCommandyType);
					Web_SET_PostAutoConfigWorkPage(iLanguage);

					break;

				}


			case CONFIGURE_OF_SMTP:
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
						Web_SET_PostYDN_TL1Page(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);

					}

					break;

				}

			case CONFIGURE_OF_DHCPSERVER:
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
						Web_SET_PostDHCPServerPage(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);
					}

					break;

				}

			case CONFIGURE_OF_VPN:
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
						Web_SET_PostYDN_TL1Page(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);

					}

					break;

				}

			case CONFIGURE_OF_SMS:
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
						Web_SET_PostYDN_TL1Page(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);

					}

					break;

				}

			case CONFIGURE_OF_SETTINGS_EXPORT:	//Export Settings to a file
				{
				    char	szResult[4];
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
					    strncpyz(szResult, szReturnBuf, 3);
					    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
					    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
					    PostPage(pHtml);
					}
					break;
				}
                
            //end////////////////////////////////////////////////////////////////////////
            default:
                break;
            

		}
		if(szReturnBuf != NULL)
		{
			DELETE(szReturnBuf);
			szReturnBuf = NULL;
		}
		if(szGetBuf != NULL)
		{
			DELETE(szGetBuf);
			szGetBuf = NULL;
		}
        if(pUserNameTransfer != NULL)
        {
            DELETE(pUserNameTransfer);
            pUserNameTransfer = NULL;
        }
	}
	
	if(pHtml != NULL)
	{
	    DELETE(pHtml);
	    pHtml = NULL;
	}
	return TRUE;

}

static char *Web_SET_SendConfigureCommand(IN char *szBuf, IN int iModifyPassword)	//iModifyPassword = 13:Modify password
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	//TRACE("szBuf : %s\n", szBuf);
	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return NULL;
	}
	 
	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return NULL;
	}
	
	/*start buffer with pid and a blank*/
	
	iLen = sprintf(szBuf1,"%10ld%2d%2d%-32s%-256s",(long)getpid(), CONFIGURE_MANAGE,iModifyPassword,pUserNameTransfer,szBuf);

	/*if(iModifyPassword == READ_CABINET)
	{
	    FILE *pf3=NULL;
	    pf3=fopen("/var/file3.html","wb");
	    fwrite(szBuf1,strlen(szBuf1),1,pf3);
	    fclose(pf3);
	}*/
	/*else if(iModifyPassword == SET_CABINET)
	{
	    FILE *pf5=NULL;
	    pf5=fopen("/var/file5.html","wb");
	    fwrite(szBuf1,strlen(szBuf1),1,pf5);
	    fclose(pf5);
	}*/
	/*else if(iModifyPassword == GET_NMSV3_INFO)
	{
	    FILE *pf6=NULL;
	    pf6=fopen("/var/wj6.txt","wb");
	    fwrite(szBuf1,strlen(szBuf1),1,pf6);
	    fclose(pf6);
	}
	else if(iModifyPassword == SET_NMS_INFO)
	{
	    FILE *pf12=NULL;
	    pf12=fopen("/var/wj12.txt","wb");
	    fwrite(szBuf1,strlen(szBuf1),1,pf12);
	    fclose(pf12);
	}
	else if(iModifyPassword == SET_NMSV3_INFO)
	{
	    FILE *pf13=NULL;
	    pf13=fopen("/var/wj13.txt","wb");
	    fwrite(szBuf1,strlen(szBuf1),1,pf13);
	    fclose(pf13);
	}
	else if(iModifyPassword == SET_SNMP_TRAP)
	{
	    FILE *pf14=NULL;
	    pf14=fopen("/var/wj14.txt","wb");
	    fwrite(szBuf1,strlen(szBuf1),1,pf14);
	    fclose(pf14);
	}*/

    
#ifdef WEB_ADD_PRODUCT_INFO
	//printf("szBuf1 : %s\n", szBuf1);
	//return;
#endif

	if((write(fd, (void *)szBuf1, strlen(szBuf1) + 1))<0)
	{
		close(fd);
		return NULL;
	}

	/*FILE *fp;
	if((fp = fopen("/var/error2.txt","wb")) != NULL)
	{
		fwrite(szBuf1,strlen(szBuf1), 1, fp);
		fwrite("3333\n", 5,1,fp);

		fclose(fp);
	}*/

	if((fd2 = open(fifoname,O_RDONLY))<0)
	{
		/*FILE *pf2=NULL;
		pf2=fopen("/var/wj2.txt","a+");
		fwrite(szBuf3,strlen(szBuf3),1,pf2);
		fclose(pf2);*/

		return NULL;
	}



    szBuf3 = NEW(char,10 * PIPE_BUF);
	if(szBuf3 == NULL)
    {
        return NULL;
    }
    memset(szBuf3,0,10 * PIPE_BUF);
    int iBufCount=0;
    

    while((iLen = read(fd2,szBuf2,PIPE_BUF - 1)) > 0)
    {
        if(iBufCount >= 10)
        {
            szBuf3 = RENEW(char, szBuf3, (iBufCount + 1) * PIPE_BUF);
            if(szBuf3 == NULL)
            {
                return NULL;
            }

        }

        strlcat(szBuf3, szBuf2,(iBufCount + 1) * PIPE_BUF);
        iBufCount++;
    }

    //FILE *pf3 = NULL;
    //pf3=fopen("/var/wj2.txt","a+");
    //fwrite(szBuf3,strlen(szBuf3),1,pf3);
    //fclose(pf3);


	close(fd2);
	close(fd);
	unlink(fifoname);

	//if(iModifyPassword == GET_TL1_SIGNAL)
	//{
	//    FILE *pf4=NULL;
	//    pf4=fopen("/var/file4.html","wb");
	//    fwrite(szBuf3,strlen(szBuf3),1,pf4);
	//    fclose(pf4);
	//}
	/*else if(iModifyPassword == SET_CABINET)
	{
	    FILE *pf6=NULL;
	    pf6=fopen("/var/file6.html","wb");
	    fwrite(szBuf3,strlen(szBuf3),1,pf6);
	    fclose(pf6);
	}*/
	/*else if(iModifyPassword == GET_NMSV3_INFO)
	{
	    FILE *pf9=NULL;
	    pf9=fopen("/var/wj9.txt","wb");
	    fwrite(szBuf3,strlen(szBuf3),1,pf9);
	    fclose(pf9);
	}
	else if(iModifyPassword == SET_NMS_INFO)
	{
	    FILE *pf15=NULL;
	    pf15=fopen("/var/wj15.txt","wb");
	    fwrite(szBuf3,strlen(szBuf3),1,pf15);
	    fclose(pf15);
	}
	else if(iModifyPassword == SET_NMSV3_INFO)
	{
	    FILE *pf16=NULL;
	    pf16=fopen("/var/wj16.txt","wb");
	    fwrite(szBuf3,strlen(szBuf3),1,pf16);
	    fclose(pf16);
	}
	else if(iModifyPassword == SET_SNMP_TRAP)
	{
	    FILE *pf17=NULL;
	    pf17=fopen("/var/wj17.txt","wb");
	    fwrite(szBuf3,strlen(szBuf3),1,pf17);
	    fclose(pf17);
	}*/
	
	return szBuf3;
}

static int Web_SET_GetCommandParam(IN ConfigureCommandInfo *stConfigure, OUT char **pBuf)
{
#define CGI_NMS_TRAP_ENABLE				"_trap_enable"

	char	**postvars = NULL; /* POST request data repository */
	char	**getvars = NULL; /* GET request data repository */
	int		form_method; /* POST = 1, GET = 0 */  
	char	*val = NULL;
	char	*szSendBuf = NULL;
	int		iLen = 0;
	int	iCabinetNum;
	int	iTL1GroupNum;

	form_method = getRequestMethod();
	if(form_method == POST) 
	{
		getvars = getGETvars();
		postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
	} 
	else if(form_method == GET) 
	{
		getvars = getGETvars();
	}
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}

	szSendBuf = NEW(char, 1024);
	if(szSendBuf == NULL)
	{
		return FALSE;
	}

	memset(szSendBuf, 0x0, 1024);
	if((val = getValue( getvars, postvars, CGI_MODIFY_CONFIGURE_DETAIL )) != NULL)
	{
		//TRACE("val : %s\n",val);
		stConfigure->iCommandyType = atoi(val);		
	}

	if((val = getValue( getvars, postvars, CGI_MODIFY_CONFIGURE )) != NULL)
	{
		stConfigure->iConfigureType = atoi(val);
	}
	
	if((val = getValue( getvars, postvars, SESSION_ID )) != NULL ) // sessionId
	{
        //stConfigure->iSessionID = atoi(val);
	
		sprintf(stConfigure->szSessionID, "%63s",val);//, sizeof(stConfigure->szSessionID));

	}

	if((val = getValue(getvars,postvars,LANGUAGE_TYPE)) != NULL)
	{
		stConfigure->iLanguage = atoi(val);
	}


	iLen += sprintf(szSendBuf + iLen, "%2d",stConfigure->iCommandyType);
	//TRACE("szSendBuf : %s",szSendBuf);
	//TRACE("stConfigure->iCommandyType :%d\n", stConfigure->iCommandyType);
	iLen += sprintf(szSendBuf + iLen, "%2d",stConfigure->iLanguage);
	//TRACE("stConfigure->iLanguage :%d\n", stConfigure->iLanguage);

	switch(stConfigure->iConfigureType)
	{
		case CONFIGURE_OF_NETWORK://Network
		{
			if(stConfigure->iCommandyType == SET_NETWORK_INFO)
			{
				if((val = getValue( getvars, postvars, CGI_CLIENT_IP_NAME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s%c",val, 59);
				}
				if((val = getValue( getvars, postvars, CGI_CLIENT_MASK_NAME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s%c",val,59);
				}
				if((val = getValue( getvars, postvars, CGI_CLIENT_GATE_NAME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s%c",val,59);
				}
			}
			else if(stConfigure->iCommandyType == SET_V6_NETWORK_INFO)
			{
			    if((val = getValue(getvars, postvars, CGI_CLIENT_IP_NAME)) != NULL)
			    {
				iLen += sprintf(szSendBuf + iLen, "%64s%c",val, 59);
			    }
			    if((val = getValue(getvars, postvars, CGI_CLIENT_GATE_NAME)) != NULL)
			    {
				iLen += sprintf(szSendBuf + iLen, "%64s%c",val,59);
			    }
			    if((val = getValue(getvars, postvars, CGI_CLIENT_PREFIX)) != NULL)
			    {
				iLen += sprintf(szSendBuf + iLen, "%32s%c",val,59);
			    }
			}
			break;
		}
		case CONFIGURE_OF_NMS://NMS
			if(stConfigure->iCommandyType == SET_NMS_INFO)
			{

#define CGI_NMS_IP							"_ip"
#define CGI_NMS_PUBLIC_COMMUNITY			"_public_community"
#define CGI_NMS_PRIVATE_COMMUNITY			"_private_community"
#define	CGI_NMS_TRAP						"_trap_level"
#define CGI_NMS_OPERATE_TYPE				"_operate_type"

				if((val = getValue( getvars, postvars, CGI_NMS_OPERATE_TYPE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_IP )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%64s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_PUBLIC_COMMUNITY )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_PRIVATE_COMMUNITY )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				/*if((val = getValue( getvars, postvars, CGI_NMS_TRAP )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);*/

				if((val = getValue( getvars, postvars, CGI_NMS_TRAP_ENABLE )) != NULL)
				{
				    iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
			}
			else if(stConfigure->iCommandyType == SET_NMSV3_INFO)
			{
#define CGI_NMS_USERNAME			"_username"
#define CGI_NMS_TRAP_IP				"_trapip"
#define CGI_NMS_DES_COMMUNITY			"_des_community"
#define CGI_NMS_MD5_COMMUNITY			"_md5_community"
#define	CGI_NMS_TRAP_LEVEL			"_trap_level"
#define CGI_NMS_ENGIN_ID			"_trap_engine"

				if((val = getValue( getvars, postvars, CGI_NMS_OPERATE_TYPE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_USERNAME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_TRAP_IP )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%64s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_DES_COMMUNITY )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_MD5_COMMUNITY )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				
				if((val = getValue( getvars, postvars, CGI_NMS_TRAP_LEVEL )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_TRAP_ENABLE )) != NULL)
				{
				    iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				/*iLen += sprintf(szSendBuf + iLen, "%c",59);*/
			}
			else if(stConfigure->iCommandyType == SET_SNMP_TRAP)
			{
#define	CGI_NMS_TRAP_LEVEL			"_trap_level"
			    if((val = getValue( getvars, postvars, CGI_NMS_TRAP_LEVEL )) != NULL)
			    {
				iLen += sprintf(szSendBuf + iLen, "%32s",val);
			    }
			    iLen += sprintf(szSendBuf + iLen, "%c",59);
			}
			break;
			case CONFIGURE_OF_CABINET:
			    if((stConfigure->iCommandyType == READ_CABINET) || (stConfigure->iCommandyType == RESET_CABINET))
			    {
				if((val = getValue( getvars, postvars, CGI_CABINET )) != NULL)
				{
				    iCabinetNum = atoi(val);
				    iLen += sprintf(szSendBuf + iLen, "%3d",iCabinetNum);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
			    }
			    else if((stConfigure->iCommandyType == SET_CABINET) || (stConfigure->iCommandyType == SET_CABINET_PARA)
				|| (stConfigure->iCommandyType == SET_CB_PARA))
			    {
				if((val = getValue( getvars, postvars, CGI_CABINET )) != NULL)
				{
				    iCabinetNum = atoi(val);
				    iLen += sprintf(szSendBuf + iLen, "%3d",iCabinetNum);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				if((val = getValue( getvars, postvars, CGI_CABINET_CONTENT )) != NULL)
				{
				    iLen += sprintf(szSendBuf + iLen, "%s", val);
				}
				//iLen += sprintf(szSendBuf + iLen, "%c",59);
			    }
			    break;
		case CONFIGURE_OF_ESR://ESR
			if(stConfigure->iCommandyType == SET_ESR_INFO)
			{
				if((val = getValue( getvars, postvars, CGI_ESR_PROTOCOL_TYPE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_PROTOCOL_MEDIA )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_CALL_ELAPSE_TIME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_MAX_ALARM_ATTEMPTS )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
						
				
				if((val = getValue( getvars, postvars, CGI_ESR_CALLBACK_INUSE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_REPORT_INUSE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_SECURITY_LEVEL )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_MAIN_REPORT_PHONE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				
				if((val = getValue( getvars, postvars, CGI_ESR_SECOND_REPORT_PHONE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				
				if((val = getValue( getvars, postvars, CGI_ESR_CALLBACK_PHONE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				
				if((val = getValue( getvars, postvars, CGI_ESR_REPORT_IP1 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_REPORT_IP2 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_SECURITY_IP1 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_SECURITY_IP2 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_CCID )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_SOCID )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);


				if((val = getValue( getvars, postvars, CGI_ESR_COMMANDPARAM )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

			}

			break;
		case CONFIGURE_OF_USER://User
		{
			
			if(stConfigure->iCommandyType == SET_USER_INFO ||
					stConfigure->iCommandyType == ADD_USER_INFO)
			{
				if((val =  getValue( getvars, postvars, CGI_USER_NAME)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
					strncpyz(szUserName, val, sizeof(szUserName));
					
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val =  getValue( getvars, postvars, CGI_USER_PASSWORD)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val =  getValue( getvars, postvars, CGI_USER_LEVEL)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%4s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				if((val =  getValue( getvars, postvars, CGI_LCD_lOGIN_ONLY)) != NULL) // LCD Login Only check box
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Enabled");
				}
				else
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Disabled");
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				if((val =  getValue( getvars, postvars, CGI_ACC_COUNT_EN)) != NULL) // LCD Login Only check box
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Enabled");
				}

				else
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Disabled");
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val =  getValue( getvars, postvars, CGI_ACC_LOCK_UNLOCK)) != NULL) // LCD Login Only check box
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Enabled");
				}

				else
				{

					iLen += sprintf(szSendBuf + iLen, "%48s","Disabled");
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				//----------------------- Added by Koustubh Mattikalli (IA-01-02) ---------------------------------
                                if((val =  getValue( getvars, postvars, CGI_STRONG_PASS)) != NULL) // Strong Password Only check box
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Enabled");
				}
				else
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Disabled");
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);


			}
			else if(stConfigure->iCommandyType == DELETE_USER_INFO)
			{
				if((val =  getValue( getvars, postvars, CGI_USER_NAME)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
					strncpyz(szUserName, val, sizeof(szUserName));
				}
				
			}
			else if(stConfigure->iCommandyType == FIRST_CHANGE_PASSWORD)
			{
				if((val =  getValue( getvars, postvars, CGI_USER_NAME)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
					strncpyz(szUserName, val, sizeof(szUserName));
					
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val =  getValue( getvars, postvars, CGI_USER_PASSWORD)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val =  getValue( getvars, postvars, CGI_USER_LEVEL)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%4s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				if((val =  getValue( getvars, postvars, CGI_LCD_lOGIN_ONLY)) != NULL) // LCD Login Only check box
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Enabled");
				}
				else
				{
					iLen += sprintf(szSendBuf + iLen, "%48s","Disabled");
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val =  getValue( getvars, postvars, CGI_USER_OLD_PASSWORD)) != NULL)
				{	
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);	
			}
			else if((stConfigure->iCommandyType == DELETE_USER_INFO) ||
					(stConfigure->iCommandyType == LCD_LOGIN_ADMIN_LOGGED))
			{
				if((val =  getValue( getvars, postvars, CGI_USER_NAME)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
					strncpyz(szUserName, val, sizeof(szUserName));
				}
				
			}
			break;
		}
		case CONFIGURE_OF_TIME://Time
		{



				if(stConfigure->iCommandyType == SET_TIME_VALUE )	
				{
					//Set Time
					if((val =  getValue( getvars, postvars, CGI_TIME_SET_TIME)) != NULL)
					{
						iLen += sprintf(szSendBuf + iLen, "%32s;",val);
					}

				}
				else if(stConfigure->iCommandyType == SET_TIME_IP)
				{
					if((val =  getValue( getvars, postvars, CGI_TIME_MAIN_IP)) != NULL)
					{
						//Main IP
						iLen += sprintf(szSendBuf + iLen, "%32s;",val);
					}
					if((val =  getValue( getvars, postvars, CGI_TIME_BAK_IP)) != NULL)
					{
						//Bak IP
						iLen += sprintf(szSendBuf + iLen, "%32s;",val);
					}
					if((val =  getValue( getvars, postvars, CGI_TIME_INTERVAL)) != NULL)
					{
						//Time Interval 
						iLen += sprintf(szSendBuf + iLen, "%16s;",val);
					}			

					if((val =  getValue( getvars, postvars, CGI_TIME_ZONE)) != NULL)
					{
						//Time Zone 

						iLen += sprintf(szSendBuf + iLen, "%16s;",val);
					}
					
				}

				
			break;
		}
		case CONFIGURE_OF_PRODUCTINFO://Product Information
		{

			break;
		}

        //////////////////////////////////////////////////////////////////////////
        //Added by wj for Config PLC private config file 2006.5.13

        case CONFIGURE_OF_PLC: //Config PLC
        {

            
            if(stConfigure->iCommandyType == SET_PLC_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_PLC_ADD)) != NULL)
                {

                    iLen += sprintf(szSendBuf + iLen, "%-256s",val);
                }
            }
            else if(stConfigure->iCommandyType == DEL_PLC_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_PLC_DEL)) != NULL)
                {

                    iLen += sprintf(szSendBuf + iLen, "%-256s",val);
                }
            }
            else if(stConfigure->iCommandyType == GET_PLC_CONFIG)
            {

            }

            break;
        }
        case CONFIGURE_OF_ALARMS: //Config Alarm Suppressing Exp
        {
            if(stConfigure->iCommandyType == GET_ALARM_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_ALARM_EQUIPTYPEID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%4s",val);
                }
            }
            else if(stConfigure->iCommandyType == SET_ALARM_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_ALARM_EQUIPTYPEID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%4s",val);
                }
                //iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val =  getValue( getvars, postvars, CFG_ALARM_ALARMID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%3s",val);
                }
                //iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val =  getValue( getvars, postvars, CFG_ALARM_ALARMSUPEXP)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%-128s",val);
                }

            }
            break;
        }

        case CONFIGURE_OF_ALARMREG: //Config Alarm Relay
        {
            if(stConfigure->iCommandyType == GET_ALARMREG_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_ALARM_EQUIPTYPEID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%4s",val);
                }
            }
            else if(stConfigure->iCommandyType == SET_ALARMREG_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_ALARM_EQUIPTYPEID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%4s",val);
                }
                //iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val =  getValue( getvars, postvars, CFG_ALARM_ALARMID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%3s",val);
                }
                //iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val =  getValue( getvars, postvars, CFG_ALARM_ALARMREG)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%2s",val);
                }

            }
            break;
        }
	case CONFIGURE_OF_TL1:
	    {
		if(stConfigure->iCommandyType == SET_TL1_CONFIG)
		{
		    if((val = getValue( getvars, postvars, CGI_TL1_PORT_ACTIVE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_PROTOCOL_MEDIA )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_PORT_NUM )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_PORT_ALIVE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_TIMEOUT )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_LOGON_USER )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_IDENTIFIER )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		}
		else if(stConfigure->iCommandyType == SET_TL1_GROUP)
		{
		    if((val = getValue( getvars, postvars, CGI_TL1_INDEX )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_NAME )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_PREFIX )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_TYPE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		}
		else if(stConfigure->iCommandyType == SET_TL1_SIGNAL)
		{
		    if((val = getValue( getvars, postvars, CGI_TL1_INDEX )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_EQUIP )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_INDEX )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_ACTIVE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    /*if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_SAMPLE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_ALARM )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_SETTING )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);*/
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_CON_TYPE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_CON_DESC )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_NOTI_CODE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_SERV_CODE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_MONI_FORMAT )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_SIGNAL_MONI_TYPE )) != NULL)
		    {
			iLen += sprintf(szSendBuf + iLen, "%s",val);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		}
		else if(stConfigure->iCommandyType == GET_TL1_SIGNAL)
		{
		    if((val = getValue( getvars, postvars, CGI_TL1_INDEX )) != NULL)
		    {
			iTL1GroupNum = atoi(val);
			iLen += sprintf(szSendBuf + iLen, "%3d",iTL1GroupNum);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		    if((val = getValue( getvars, postvars, CGI_TL1_EQUIP )) != NULL)
		    {
			iTL1GroupNum = atoi(val);
			iLen += sprintf(szSendBuf + iLen, "%3d",iTL1GroupNum);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		}
		else if(stConfigure->iCommandyType == GET_TL1_EQUIP)
		{
		    if((val = getValue( getvars, postvars, CGI_TL1_INDEX )) != NULL)
		    {
			iTL1GroupNum = atoi(val);
			iLen += sprintf(szSendBuf + iLen, "%3d",iTL1GroupNum);
		    }
		    iLen += sprintf(szSendBuf + iLen, "%c",59);
		}
		break;
	    }
        case CONFIGURE_OF_YDN://YDN
		{
			

            if(stConfigure->iCommandyType == SET_YDN_CONFIG)
            {
                if((val = getValue( getvars, postvars, CGI_YDN_PROTOCOL_TYPE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_PROTOCOL_MEDIA )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);
             
                if((val = getValue( getvars, postvars, CGI_YDN_CALL_ELAPSE_TIME )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_MAX_ALARM_ATTEMPTS )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_REPORT_INUSE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_MAIN_REPORT_PHONE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_SECOND_REPORT_PHONE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_CALLBACK_PHONE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_COMMANDPARAM )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_CCID )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);
   
            }

            break;
		}

	case CONFIGURE_OF_MODBUS://MODBUS
		{

			if(stConfigure->iCommandyType == SET_MODBUS_CONFIG)
			{
				if((val = getValue( getvars, postvars, CGI_YDN_PROTOCOL_TYPE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_YDN_PROTOCOL_MEDIA )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_YDN_COMMANDPARAM )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_YDN_CCID )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

			}

			break;
		}
		//changed by Frank Wu,9/N/27,20140527, for power split
		case CONFIGURE_OF_GCPS: //Config GC_PS
		{
			if(stConfigure->iCommandyType == SET_GC_PS_CONFIG)
			{
				if((val =  getValue( getvars, postvars, CGI_PS_INFO)) != NULL)
				{

					iLen += sprintf(szSendBuf + iLen, "%-256s",val);
				}
			}
			else if(stConfigure->iCommandyType == SET_GC_PS_MODE)
			{
				if((val =  getValue( getvars, postvars, CGI_PS_MODE)) != NULL)
				{

					iLen += sprintf(szSendBuf + iLen, "%-256s",val);
				}
			}
			else if(stConfigure->iCommandyType == GET_GC_PS_CONFIG)
			{
				;
			}

			break;
		}
		case CONFIGURE_OF_SETTINGPARAM://Product Information
		{
			break;
		}
		case CONFIGURE_OF_AUTOCONFIG://Product Information
		{

				break;
		}

		case CONFIGURE_OF_SMTP:
		{
			if(stConfigure->iCommandyType == SET_SMTP_CONFIG)
			{
				if((val = getValue( getvars, postvars, CFG_SMTP_SENDTO )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMTP_SERVERIP )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMTP_SERVERPORT )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMTP_ACCOUNT )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMTP_PASSWORD )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMTP_AUTHEN )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMTP_ALARMLEVEL )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMTP_FROM )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);


				//FILE *pf=NULL;
				//pf=fopen("/var/wj0.txt","a+");
				////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
				//fwrite(szSendBuf,strlen(szSendBuf),1,pf);
				//fclose(pf);



			}

			break;
	
		}
		case CONFIGURE_OF_DHCPSERVER:
		{

			break;

		}
		case CONFIGURE_OF_VPN:
		{
			if(stConfigure->iCommandyType == SET_VPN_CONFIG)
			{
				if((val = getValue( getvars, postvars, CFG_VPN_ENABLE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_VPN_REMOTE_IP )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_VPN_REMOTE_PORT )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
			}


			break;
		}

		case CONFIGURE_OF_SMS:
		{
			if(stConfigure->iCommandyType == SET_SMS_CONFIG)
			{
				if((val = getValue( getvars, postvars, CFG_SMS_PHONE1 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMS_PHONE2 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMS_PHONE3 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CFG_SMS_ALARMLEVEL )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
			}


			break;
		}


        //end////////////////////////////////////////////////////////////////////////
        
		default:
			break;
		

	  }
		 
 	*pBuf = szSendBuf;

	//if(stConfigure->iConfigureType == CONFIGURE_OF_CABINET)
	//{
	//    FILE *fp1;
	//    if((fp1 = fopen("/var/file1.html","wb")) != NULL)
	//    {
	//	fwrite(szSendBuf,strlen(szSendBuf), 1, fp1);
	//	fclose(fp1);
	//    }
	//}

	cleanUp(getvars, postvars);

	//TRACE("szSendBuf : %s\n", szSendBuf);
    return TRUE;  

}

static void Web_SET_PostV6NetWorkPage(IN char *pGetBuf, IN int iLanguage)
{
    char	szLocalIP[128], szGlobalIP[128], szGate[128];
    char	szPrefix[4], szLocalPrefix[4], szDHCPState[4];
    int		iPosition = 0;
    char	*pSearchValue = NULL;
    char	*pTGetBuf = pGetBuf;
    //char	szExchange[3][64];
    char	*szTrimText = NULL;
    char	szReturn[3];
    char	szTemp[500];
    BOOL    blModifySuccess = FALSE;
    memset(szReturn, 0, sizeof(szReturn));

    char	*pHtml = NEW(char,1000);
    if(pHtml == NULL)
    {
	return ;
    }
    sprintf(pHtml,"%s",strResult);

    FILE *pf1=NULL;
    //pf1=fopen("/var/wj0.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(pGetBuf,strlen(pGetBuf),1,pf1);
    //fclose(pf1);

    if(pTGetBuf != NULL)
    {
	blModifySuccess = TRUE;
	strncpyz(szReturn, pTGetBuf, sizeof(szReturn));
	/*fwrite(szReturn,strlen(szReturn), 1, fp);*/
	pTGetBuf = pTGetBuf + 2;
	pSearchValue = strchr(pTGetBuf, 44);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - pTGetBuf;
	    if(iPosition > 0)
	    {
		strncpyz(szLocalIP, pTGetBuf, ((iPosition + 1) < 128) ? (iPosition + 1) : 128);
		//fwrite(szIP,strlen(szIP), 1, fp);
		pTGetBuf = pTGetBuf + iPosition;
	    }

	}
	pTGetBuf = pTGetBuf + 1;

	pSearchValue = strchr(pTGetBuf, 44);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - pTGetBuf;
	    if(iPosition > 0)
	    {
		strncpyz(szLocalPrefix, pTGetBuf, ((iPosition + 1) < 4) ?(iPosition + 1) :  4);
		pTGetBuf = pTGetBuf + iPosition;
		//fwrite(szGate,strlen(szGate), 1, fp);
		//pTGetBuf = pTGetBuf + iPosition;
	    }

	}

	pTGetBuf = pTGetBuf + 1;

	pSearchValue = strchr(pTGetBuf, 44);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - pTGetBuf;
	    if(iPosition > 0)
	    {
		strncpyz(szGlobalIP, pTGetBuf, ((iPosition + 1) < 128) ?(iPosition + 1) :  128);
		//fwrite(szMask,strlen(szMask), 1, fp);
		pTGetBuf = pTGetBuf + iPosition;
	    }

	}
	pTGetBuf = pTGetBuf + 1;

	pSearchValue = strchr(pTGetBuf, 44);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - pTGetBuf;
	    if(iPosition > 0)
	    {
		strncpyz(szPrefix, pTGetBuf, ((iPosition + 1) < 4) ?(iPosition + 1) :  4);
		pTGetBuf = pTGetBuf + iPosition;
		//fwrite(szGate,strlen(szGate), 1, fp);
		//pTGetBuf = pTGetBuf + iPosition;
	    }

	}

	pTGetBuf = pTGetBuf + 1;

	pSearchValue = strchr(pTGetBuf, 44);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - pTGetBuf;
	    if(iPosition > 0)
	    {
		strncpyz(szGate, pTGetBuf, ((iPosition + 1) < 128) ?(iPosition + 1) :  128);
		pTGetBuf = pTGetBuf + iPosition;
		//fwrite(szMask,strlen(szMask), 1, fp);
	    }
	}

	pTGetBuf = pTGetBuf + 1;

	pSearchValue = strchr(pTGetBuf, 44);
	if(pSearchValue != NULL)
	{
	    iPosition = pSearchValue - pTGetBuf;
	    if(iPosition > 0)
	    {
		strncpyz(szDHCPState, pTGetBuf, ((iPosition + 1) < 4) ?(iPosition + 1) :  4);
		//fwrite(szGate,strlen(szGate), 1, fp);
		//pTGetBuf = pTGetBuf + iPosition;
	    }

	}

	//fclose(fp);
    }

    //pTGetBuf = pTGetBuf + iPosition;

    //pTGetBuf = pTGetBuf + 1;
    //szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[0], iLanguage);

    //if(LoadHtmlFile(szPath, &pHtml ) > 0 )
    //{
    //pf=fopen("/var/wj5.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(szIP,strlen(szIP),1,pf);
    //fclose(pf);

    /*szTrimText = Web_RemoveWhiteSpace(szIP);
    if(atoi(szTrimText) == 0)
    {
	sprintf(szExchange[0], "%d", inet_addr("0.0.0.0"));
    }
    else
    {
	sprintf(szExchange[0], "%d", inet_addr(szTrimText));
    }*/
    //ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_IP_NAME), szExchange);


    //pf=fopen("/var/wj6.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(szMask,strlen(szMask),1,pf);
    //fclose(pf);

    /*szTrimText = Web_RemoveWhiteSpace(szMask);

    if(atoi(szTrimText) == 0)
    {
	sprintf(szExchange[1], "%d", inet_addr("0.0.0.0"));
    }
    else
    {
	sprintf(szExchange[1], "%d", inet_addr(szTrimText));
    }*/

    //pf=fopen("/var/wj7.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(szGate,strlen(szGate),1,pf);
    //fclose(pf);


    //ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_MASK_NAME), szExchange);


    /*szTrimText = Web_RemoveWhiteSpace(szGate);
    if(atoi(szTrimText) == 0)
    {
	sprintf(szExchange[2], "%d", inet_addr("0.0.0.0"));
    }
    else
    {
	sprintf(szExchange[2], "%d", inet_addr(szTrimText));
    }*/
    //ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_GATE_NAME), szExchange);

    //pf=fopen("/var/wj8.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(szIP,strlen(szIP),1,pf);
    //fclose(pf);

    sprintf(szTemp," \"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"", szLocalIP, szLocalPrefix, szGlobalIP, szPrefix, szGate, szDHCPState);

    //pf=fopen("/var/wj9.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(szTemp,strlen(szTemp),1,pf);
    //fclose(pf);


    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), szTemp);

    //pf=fopen("/var/wj10.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(pHtml,strlen(pHtml),1,pf);
    //fclose(pf);

    if(blModifySuccess == TRUE )
    {
	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szReturn);

    }
    else
    {
	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");

    }




    //ReplaceString(&pHtml, ;MAKE_VAR_FIELD("CONTENT_RESULT"), szTemp);


    //FILE *pf=NULL;
    //pf=fopen("/var/wj3.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(pHtml,strlen(pHtml),1,pf);
    //fclose(pf);


    PostPage(pHtml);

    DELETE(pHtml);
    pHtml = NULL;





}

static void Web_SET_PostNetWorkPage(IN char *pGetBuf, IN int iLanguage)
{
	char	szIP[33],szMask[33], szGate[33];
	//char	*szPath = NULL;
    int		iPosition = 0;
	char	*pSearchValue = NULL;
	char	*pTGetBuf = pGetBuf;
	char	szExchange[3][64];
	char	*szTrimText = NULL;
	char	szReturn[3];
	char	szTemp[100];
	BOOL    blModifySuccess = FALSE;
	memset(szReturn, 0, sizeof(szReturn));

	char	*pHtml = NEW(char,1000);
	if(pHtml == NULL)
	{
		return ;
	}
	sprintf(pHtml,"%s",strResult);

	//FILE *pf=NULL;
	//pf=fopen("/var/wj0.txt","a+");
	////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
	//fwrite(pGetBuf,strlen(pGetBuf),1,pf);
	//fclose(pf);

	if(pTGetBuf != NULL)
	{

		blModifySuccess = TRUE;
		strncpyz(szReturn, pTGetBuf, sizeof(szReturn));
		/*fwrite(szReturn,strlen(szReturn), 1, fp);*/
		pTGetBuf = pTGetBuf + 2;
		pSearchValue = strchr(pTGetBuf, 44);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
			if(iPosition > 0)
			{
				strncpyz(szIP, pTGetBuf, ((iPosition + 1) < 33) ? (iPosition + 1) : 33);
				//fwrite(szIP,strlen(szIP), 1, fp);
				pTGetBuf = pTGetBuf + iPosition;
			}

		}
		pTGetBuf = pTGetBuf + 1;
		
		pSearchValue = strchr(pTGetBuf, 44);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
			if(iPosition > 0)
			{
				strncpyz(szMask, pTGetBuf, ((iPosition + 1) < 33) ?(iPosition + 1) :  33);
				//fwrite(szMask,strlen(szMask), 1, fp);
				pTGetBuf = pTGetBuf + iPosition;
			}

		}
		pTGetBuf = pTGetBuf + 1;

		pSearchValue = strchr(pTGetBuf, 44);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
			if(iPosition > 0)
			{
				strncpyz(szGate, pTGetBuf, ((iPosition + 1) < 33) ?(iPosition + 1) :  33);
				//fwrite(szGate,strlen(szGate), 1, fp);
				//pTGetBuf = pTGetBuf + iPosition;
			}

		}

		//fclose(fp);
	}
	
	//pTGetBuf = pTGetBuf + iPosition;
		
	//pTGetBuf = pTGetBuf + 1;
	//szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[0], iLanguage);

	//if(LoadHtmlFile(szPath, &pHtml ) > 0 )
    //{
	//pf=fopen("/var/wj5.txt","a+");
	////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
	//fwrite(szIP,strlen(szIP),1,pf);
	//fclose(pf);

		szTrimText = Web_RemoveWhiteSpace(szIP);
		if(atoi(szTrimText) == 0)
		{
			sprintf(szExchange[0], "%d", inet_addr("0.0.0.0"));
		}
		else
		{
			sprintf(szExchange[0], "%d", inet_addr(szTrimText));
		}
		//ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_IP_NAME), szExchange);


		//pf=fopen("/var/wj6.txt","a+");
		////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
		//fwrite(szMask,strlen(szMask),1,pf);
		//fclose(pf);
		
		szTrimText = Web_RemoveWhiteSpace(szMask);
		
		if(atoi(szTrimText) == 0)
		{
			sprintf(szExchange[1], "%d", inet_addr("0.0.0.0"));
		}
		else
		{
			sprintf(szExchange[1], "%d", inet_addr(szTrimText));
		}

		//pf=fopen("/var/wj7.txt","a+");
		////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
		//fwrite(szGate,strlen(szGate),1,pf);
		//fclose(pf);


		//ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_MASK_NAME), szExchange);


		szTrimText = Web_RemoveWhiteSpace(szGate);
		if(atoi(szTrimText) == 0)
		{
			sprintf(szExchange[2], "%d", inet_addr("0.0.0.0"));
		}
		else
		{
			sprintf(szExchange[2], "%d", inet_addr(szTrimText));
		}
		//ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_GATE_NAME), szExchange);

		//pf=fopen("/var/wj8.txt","a+");
		////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
		//fwrite(szIP,strlen(szIP),1,pf);
		//fclose(pf);

		sprintf(szTemp," \"%s\",\"%s\",\"%s \"",szExchange[0],szExchange[1],szExchange[2]);

		//pf=fopen("/var/wj9.txt","a+");
		////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
		//fwrite(szTemp,strlen(szTemp),1,pf);
		//fclose(pf);


		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), szTemp);

		//pf=fopen("/var/wj10.txt","a+");
		////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
		//fwrite(pHtml,strlen(pHtml),1,pf);
		//fclose(pf);

		if(blModifySuccess == TRUE )
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szReturn);
			
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");

		}
		

		

		//ReplaceString(&pHtml, ;MAKE_VAR_FIELD("CONTENT_RESULT"), szTemp);


		//FILE *pf=NULL;
		//pf=fopen("/var/wj3.txt","a+");
		////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
		//fwrite(pHtml,strlen(pHtml),1,pf);
		//fclose(pf);


		PostPage(pHtml);

		DELETE(pHtml);
		pHtml = NULL;



		

}

static void Web_SET_PostUserInfoPage(IN char *pGetBuf,IN int iLanguage)
{
	char	*pHtml = NEW(char,1000);
	if(pHtml == NULL)
	{
		return ;
	}
	sprintf(pHtml,"%s",strResult);

	char	szCommandType[3];
	char	*pTGetBuf = pGetBuf;
	int		iCommandType = 0;
	
	//if(pTGetBuf != NULL)
	//{
		strncpyz(szCommandType, pTGetBuf, sizeof(szCommandType));
		pTGetBuf = pTGetBuf + 2;
		//szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[3],iLanguage);
		//if(LoadHtmlFile(szPath, &pHtml ) > 0)
		//{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
			if((iCommandType = atoi(szCommandType)) == 0)
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");
			}
			else
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szCommandType);
			}
			PostPage(pHtml);
			if(pHtml != NULL)
			{
				DELETE(pHtml);
				pHtml = NULL;
			}
		//}
	//}

	/*Need to return fail or successfuly*/

	

}

static void Web_SET_PostNMSPage(IN  char *pGetBuf, IN int iLanguage)
{
	ASSERT(pGetBuf);
	char	*pTGetBuf = NULL;
	char	szResult[3];
	char	*pHtml = NEW(char,1000);
	if(pHtml == NULL)
	{
		return;
	}
	sprintf(pHtml,"%s",strResult);
	//Read file
	pTGetBuf = pGetBuf;

	strncpyz(szResult, pTGetBuf, sizeof(szResult));
	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
	pTGetBuf = pTGetBuf + 2;
	if(pTGetBuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
	}
	PostPage(pHtml);

	
	DELETE(pHtml);
	pHtml = NULL;
	
}

static void Web_SET_PostESRPage(IN char *pGetBuf, IN int iLanguage, IN int iESRType)
{
	ASSERT(pGetBuf);
	
	char	*pHtml = NEW(char,1000);
	if(pHtml == NULL)
	{
		return ;
	}

	sprintf(pHtml,"%s",strResult);

	//char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	szResult[3];


	pTGetBuf = pGetBuf;


	strncpyz(szResult, pTGetBuf, sizeof(szResult));
	if(iESRType == SET_ESR_INFO)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
	}
	else
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
	}

	if(atoi(szResult) != 1)
	{
		pTGetBuf = pTGetBuf + 2;
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
	}
	PostPage(pHtml);
	
	DELETE(pHtml);
	pHtml = NULL;
	

}

#ifdef WEB_ADD_PRODUCT_INFO
static void Web_GET_PostProductInfo(IN char *pGetBuf, IN int iLanguage, IN const char *sessionId)
{
	UNUSED(sessionId);
	ASSERT(pGetBuf);

	char	*pHtml = NULL;
	char	*pTGetBuf = NULL;
	//char	szReturn[3];
	char	*szPath = NULL;// *pSearchValue = NULL, *szTrimText = NULL;
	//int		iPosition;
	//char	szExchange[64];
	//char	szGetInfo[1024];

	pTGetBuf = pGetBuf;
	refresFileTime();

	//printf("pTGetBuf[%s]\n", pTGetBuf);

	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[5], iLanguage);

    //printf("%s\n",szPath);


	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{


		if(pTGetBuf != NULL)
		{
		
		//	szTrimText = Web_RemoveWhiteSpace(pTGetBuf);
		//	printf("szExchange[%s]\n", szExchange);
			ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_PRODUCT_INFO), pTGetBuf);
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "2");
			PostPage(pHtml);

		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "0");
			PostPage(pHtml);
		}
		DELETE(pHtml);
		pHtml = NULL;
	}

	if(szPath != NULL)
	{
    	DELETE(szPath);
		szPath = NULL;
	}
}
#endif

static void Web_SET_PostTimeInfoPage(IN  char *pGetBuf, IN int iLanguage, IN const char *sessionId)
{
	UNUSED(sessionId);
	ASSERT(pGetBuf);

	//FILE *pf=NULL;
	//pf=fopen("/var/wj6.txt","a+");
	////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
	//fwrite(pGetBuf,strlen(pGetBuf),1,pf);
	//fclose(pf);

	char	*pHtml = NEW(char,1000);
	if(pHtml == NULL)
	{
		return ;
	}
	//char	szCommandType[3];
	char	szGetInfo[33];
	//char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	*pSearchValue = NULL;
	int		iPosition = 0;
	char	*szTrimText = NULL;
	char	szExchange[5][64];
	char    szTemp[300];
	char	szReturn[3];

	sprintf(pHtml,"%s",strResult);

	memset(szReturn, 0, sizeof(szReturn));

	pTGetBuf = pGetBuf;
	//ResetTime(sessionId);
	//refresFileTime();
	//szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[4], iLanguage);

	//if(LoadHtmlFile(szPath, &pHtml ) > 0)
	//{	
	//TRACE("TRUE\n");


	if(pTGetBuf != NULL)
	{
		strncpyz(szReturn, pGetBuf, sizeof(szReturn));
		pTGetBuf = pTGetBuf + 2;

		pSearchValue = strchr(pTGetBuf, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
		}
		if(iPosition > 0)
		{
			strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
			szTrimText = Web_RemoveWhiteSpace(szGetInfo);
			if((strcmp(szTrimText,"0.0.0.0") == 0) || atoi(szTrimText) == 0)
			{
				sprintf(szExchange[0], "%d", inet_addr("0.0.0.0"));
			}
			else
			{
				sprintf(szExchange[0], "%d", inet_addr(szTrimText));
			}
			//TRACE("---%s\n", szTrimText);
			//ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_CGI_TIME_SERVER1), szExchange);
			pTGetBuf = pTGetBuf + iPosition;
		}
		pTGetBuf = pTGetBuf + 1;

		pSearchValue = strchr(pTGetBuf, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
		}
		if(iPosition > 0)
		{
			strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
			szTrimText = Web_RemoveWhiteSpace(szGetInfo);
			if((strcmp(szTrimText,"0.0.0.0") == 0) ||atoi(szTrimText) == 0)
			{
				sprintf(szExchange[1], "%d", inet_addr("0.0.0.0"));
			}
			else
			{
				sprintf(szExchange[1], "%d", inet_addr(szTrimText));
			}
			//TRACE("---%s\n", szTrimText);
			//ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_CGI_TIME_SERVER2), szExchange);
			pTGetBuf = pTGetBuf + iPosition;
		}
		pTGetBuf = pTGetBuf + 1;

		pSearchValue = strchr(pTGetBuf, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
		}
		if(iPosition > 0)
		{
			strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
			szTrimText = Web_RemoveWhiteSpace(szGetInfo);
			sprintf(szExchange[2], "%s", szTrimText);
			//ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_CGI_TIME_INTERVAL), szTrimText);
			pTGetBuf = pTGetBuf + iPosition;
		}

		pTGetBuf = pTGetBuf + 1;

		pSearchValue = strchr(pTGetBuf, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
		}
		if(iPosition > 0)
		{
			strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
			szTrimText = Web_RemoveWhiteSpace(szGetInfo);
			sprintf(szExchange[3], "%s", szTrimText);
			pTGetBuf = pTGetBuf + iPosition;
			//ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_CGI_TIME_ZONE), szTrimText);
		}

		pTGetBuf = pTGetBuf + 1;

		pSearchValue = strchr(pTGetBuf, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
		}
		if(iPosition > 0)
		{
			strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
			szTrimText = Web_RemoveWhiteSpace(szGetInfo);
			sprintf(szExchange[4], "%s", szTrimText);//NTP server function enable or disable
		}

		sprintf(szTemp," \"%s\",\"%s\",\"%s\",\"%s\",\"%s\" ",szExchange[0],szExchange[1],szExchange[2],szExchange[3],szExchange[4]);

		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szReturn);

		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), szTemp);

		
		PostPage(pHtml);


	}
	else
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szReturn);

		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
		PostPage(pHtml);
	}

	DELETE(pHtml);
	pHtml = NULL;


	//char temp[3];
	//FILE *pf2=NULL;
	//pf2=fopen("/var/wj5.txt","a+");
	////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
	//fwrite(temp,strlen(temp),1,pf2);
	//fclose(pf2);

}



static void Web_CGI_PostNoAuthority(IN int iModifyType, IN int iLanguage,IN BOOL boolDeleteSelf)
{

	//int			i;
	char		*pHtml = strResult;

	if(iModifyType == SET_ESR_INFO || iModifyType == SET_YDN_CONFIG || iModifyType == SET_MODBUS_CONFIG || iModifyType == SET_TL1_CONFIG)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "8");
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
	}
	else
	{
		if(iModifyType == DELETE_USER_INFO && boolDeleteSelf == TRUE)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "8");
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");

		}
		else if(iModifyType == LCD_LOGIN_ADMIN_LOGGED && boolDeleteSelf == TRUE)
		{
					ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "17");
					ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");

		}
		else if(iModifyType == SET_USER_INFO )
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "6");//only user and admin can modify password
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
		}
		else if((iModifyType == SET_CABINET) || (iModifyType == RESET_CABINET) || (iModifyType == SET_CABINET_PARA) || (iModifyType == SET_CB_PARA))
		{
		    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "2");
		    ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "4");
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
		}
	}

	PostPage(pHtml);

}

static void Web_CGI_PostAutoConfigNoAuthority()
{

	//int			i;
	char		*pHtml = strResult;


	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "3");
	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");



	PostPage(pHtml);

}
//////////////////////////////////////////////////////////////////////////
//Added by wj for PLC Configure
static int Web_SET_PostCFGPLCWorkPage(IN char *pGetBuf, IN int iLanguage)
{

    //FILE *pf=NULL;
    //pf=fopen("/var/wj4.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(pGetBuf,strlen(pGetBuf),1,pf);
    //fclose(pf);


    ASSERT(pGetBuf);

    char	*pHtml = NULL;
    char	*szPath = NULL;
    char	*pTGetBuf = NULL;
    char	szResult[3];

    char        *pszSampling = NULL;
    char        *pszControl  = NULL;
    char        *pszSetting  = NULL;
    char        *pszAlarm    = NULL;
    char        *pszEquip    = NULL;
    char        *pszPLCInfo  = NULL;
    char        *pszTemp     = NULL;
    //Read file
    pTGetBuf = pGetBuf;

    strncpyz(szResult, pTGetBuf, sizeof(szResult));


    char	*ptr = NULL;
    ptr = pGetBuf + 2;

    /*OEquip*/
    char		*pPosition = strchr(ptr,59);
    int			iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszEquip = NEW(char, iPosition + 1);
        if(pszEquip == NULL)
        {
            return FALSE;
        }
        memset(pszEquip, 0x0, (size_t)iPosition);
        strncpyz(pszEquip, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszEquip,strlen(pszEquip),1,pf);


    /*OSampling*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr ;
    if(iPosition > 0)
    {
        pszSampling = NEW(char, iPosition + 1);
        if(pszSampling == NULL)
        {
            return FALSE;
        }
        memset(pszSampling, 0x0, (size_t)iPosition);
        strncpyz(pszSampling, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszSampling,strlen(pszSampling),1,pf);

    /*OControl*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszControl = NEW(char, iPosition + 1);
        if(pszControl == NULL)
        {
            return FALSE;
        }
        memset(pszControl, 0x0, (size_t)iPosition);
        strncpyz(pszControl, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;
    //fwrite(pszControl,strlen(pszControl),1,pf);

    /*OSetting*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszSetting = NEW(char, iPosition + 1);
        if(pszSetting == NULL)
        {
            return FALSE;
        }
        memset(pszSetting, 0x0, (size_t)iPosition);
        strncpyz(pszSetting, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;
    //fwrite(pszSetting,strlen(pszSetting),1,pf);


    /*OAlarm*/

    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszAlarm = NEW(char, iPosition + 1);
        if(pszAlarm == NULL)
        {
            return FALSE;
        }
        memset(pszAlarm, 0x0, (size_t)iPosition);
        strncpyz(pszAlarm, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    /*PLCConfigInfo*/

    iPosition = strlen(ptr);
    if(iPosition > 0)
    {
        pszPLCInfo = NEW(char, iPosition + 1);
        if(pszPLCInfo == NULL)
        {
            return FALSE;
        }
        memset(pszPLCInfo, 0x0, (size_t)iPosition);
        strncpyz(pszPLCInfo, ptr, iPosition + 1);

    }

    //fwrite(pszAlarm,strlen(pszAlarm),1,pf);


    szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[6], iLanguage);


    //fwrite(szPath,strlen(szPath),1,pf);
	pszSetting = NEW(char, iPosition + 1);
    if(LoadHtmlFile(szPath, &pHtml ) > 0)
    {
        ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equipments"), pszEquip);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_sample_signals"), pszSampling);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_control_signals"), pszControl);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_setting_signals"), pszSetting);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_alarm_signals"), pszAlarm);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("configuration_file"), pszPLCInfo);

        if(iLanguage == 0)
        {
            pszTemp = "/var/eng/p40_cfg_plc_Popup.htm";
        }
        else if(iLanguage == 1)
        {
            pszTemp = "/var/loc/p40_cfg_plc_Popup.htm";
        }
        else
        {
            pszTemp = "/var/loc2/p40_cfg_plc_Popup.htm";
        }

        ReplaceString(&pHtml, MAKE_VAR_FIELD("popupPage"),pszTemp);
        
        
        PostPage(pHtml);

        DELETE(pHtml);
        pHtml       = NULL;

     


    }
	DELETE(pszEquip);
	DELETE(pszSampling);
	DELETE(pszControl);
	DELETE(pszSetting);
	DELETE(pszAlarm);
	DELETE(pszPLCInfo);

	pszSampling = NULL;
	pszControl  = NULL;
	pszSetting  = NULL;
	pszAlarm    = NULL;
	pszEquip    = NULL;
    //fclose(pf);

    DELETE(szPath);
    szPath = NULL;

}
//end////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Added by wj for AlarmSupExp Config
static int Web_SET_PostCFGALARMSWorkPage(IN char *pGetBuf, IN int iLanguage)
{

    /*FILE *pf=NULL;
    pf=fopen("/var/wj4.txt","a+");
    char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    fwrite(p,strlen(p),1,pf);*/


    ASSERT(pGetBuf);

    char	*pHtml = NULL;
    char	*szPath = NULL;
    char	*pTGetBuf = NULL;
    char	szResult[3];

    char *pszStdEquipBuf        = NULL; //new OStdEquip( equipTypeId, equipTypeName),... 
    char *pszDispAlarmItemsBuf  = NULL; //new ODispAlarmItem( signalId,signalName,suppressExp),... 
    char *pszRelEquip           = NULL; //new ORelEquip( equipId, equipName, sequnenceNo),... 
    char *pszRelEquipAlarms     = NULL; //new ORelEquipAlarms( equipId,signalId,signalName),... 
    char *pszStdEquipTypeName   = NULL; //StdEquipTypeName

    //Read file
    pTGetBuf = pGetBuf;

    strncpyz(szResult, pTGetBuf, sizeof(szResult));


    char	*ptr = NULL;
    ptr = pGetBuf + 2;

    /*StdEquipTypeName*/
    char		*pPosition = strchr(ptr,59);
    int			iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszStdEquipTypeName = NEW(char, iPosition + 1);
        if(pszStdEquipTypeName == NULL)
        {
            return FALSE;
        }
        memset(pszStdEquipTypeName, 0x0, (size_t)iPosition);
        strncpyz(pszStdEquipTypeName, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;


    //fwrite(pszStdEquipTypeName,strlen(pszStdEquipTypeName),1,pf);


    /*OStdEquip*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszStdEquipBuf = NEW(char, iPosition + 1);
        if(pszStdEquipBuf == NULL)
        {
			DELETE(pszStdEquipTypeName);
            return FALSE;
        }
        memset(pszStdEquipBuf, 0x0, (size_t)iPosition);
        strncpyz(pszStdEquipBuf, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszStdEquipBuf,strlen(pszStdEquipBuf),1,pf);


    /*ODispAlarmItem*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr ;
    if(iPosition > 0)
    {
        pszDispAlarmItemsBuf = NEW(char, iPosition + 1);
        if(pszDispAlarmItemsBuf == NULL)
        {
			DELETE(pszStdEquipTypeName);
			DELETE(pszStdEquipBuf);
            return FALSE;
        }
        memset(pszDispAlarmItemsBuf, 0x0, (size_t)iPosition);
        strncpyz(pszDispAlarmItemsBuf, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszDispAlarmItemsBuf,strlen(pszDispAlarmItemsBuf),1,pf);

    /*ORelEquip*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszRelEquip = NEW(char, iPosition + 1);
        if(pszRelEquip == NULL)
        {
			DELETE(pszStdEquipTypeName);
			DELETE(pszStdEquipBuf);
			DELETE(pszDispAlarmItemsBuf);
			
            return FALSE;
        }
        memset(pszRelEquip, 0x0, (size_t)iPosition);
        strncpyz(pszRelEquip, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;
    //fwrite(pszRelEquip,strlen(pszRelEquip),1,pf);


    /*ORelEquipAlarms*/

    iPosition = strlen(ptr);
    if(iPosition > 0)
    {
        pszRelEquipAlarms = NEW(char, iPosition + 1);
        if(pszRelEquipAlarms == NULL)
        {
			DELETE(pszStdEquipTypeName);
			DELETE(pszStdEquipBuf);
			DELETE(pszDispAlarmItemsBuf);
			DELETE(pszRelEquip);
            return FALSE;
        }
        memset(pszRelEquipAlarms, 0x0, (size_t)iPosition);
        strncpyz(pszRelEquipAlarms, ptr, iPosition + 1);

    }

    //fwrite(pszRelEquipAlarms,strlen(pszRelEquipAlarms),1,pf);


    szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[7], iLanguage);
    //fwrite(szPath,strlen(szPath),1,pf);

    if(LoadHtmlFile(szPath, &pHtml ) > 0)
    {
        ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("stdEquipments"), pszStdEquipBuf);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("dispAlarmItems"), pszDispAlarmItemsBuf);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("relEquipments"), pszRelEquip);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("relEquipAlarms"), pszRelEquipAlarms);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("stdEquipTypeName"), pszStdEquipTypeName);
        

        PostPage(pHtml);

        DELETE(pHtml);
		pHtml = NULL;
       

    }
	DELETE(pszStdEquipBuf);
	DELETE(pszDispAlarmItemsBuf);
	DELETE(pszRelEquip);
	DELETE(pszRelEquipAlarms);
	DELETE(pszStdEquipTypeName);


	pszStdEquipBuf          = NULL;
	pszDispAlarmItemsBuf    = NULL;
	pszRelEquip             = NULL;
	pszRelEquipAlarms       = NULL;
	pszStdEquipTypeName     = NULL;

    //fclose(pf);

    DELETE(szPath);
    szPath = NULL;

    return TRUE;

}
//end////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Added by wj for AlarmRelay Config
static int Web_SET_PostCFGALARMRegWorkPage(IN char *pGetBuf, IN int iLanguage)
{

    /*FILE *pf=NULL;
    pf=fopen("/var/wj4.txt","a+");
    char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    fwrite(p,strlen(p),1,pf);*/


    ASSERT(pGetBuf);

    char	*pHtml = NULL;
    char	*szPath = NULL;
    char	*pTGetBuf = NULL;
    char	szResult[3];

    char *pszStdEquipBuf        = NULL; //new OStdEquip( equipTypeId, equipTypeName),... 
    char *pszDispAlarmItemsBuf  = NULL; //new ODispAlarmItem( signalId,signalName,suppressExp),... 
   
    char *pszStdEquipTypeName   = NULL; //StdEquipTypeName

    //Read file
    pTGetBuf = pGetBuf;

    strncpyz(szResult, pTGetBuf, sizeof(szResult));


    char	*ptr = NULL;
    ptr = pGetBuf + 2;

    /*StdEquipTypeName*/
    char		*pPosition = strchr(ptr,59);
    int			iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszStdEquipTypeName = NEW(char, iPosition + 1);
        if(pszStdEquipTypeName == NULL)
        {
            return FALSE;
        }
        memset(pszStdEquipTypeName, 0x0, (size_t)iPosition);
        strncpyz(pszStdEquipTypeName, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;


    //fwrite(pszStdEquipTypeName,strlen(pszStdEquipTypeName),1,pf);


    /*OStdEquip*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszStdEquipBuf = NEW(char, iPosition + 1);
        if(pszStdEquipBuf == NULL)
        {
            return FALSE;
        }
        memset(pszStdEquipBuf, 0x0, (size_t)iPosition);
        strncpyz(pszStdEquipBuf, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszStdEquipBuf,strlen(pszStdEquipBuf),1,pf);


    /*ODispAlarmItem*/
    iPosition = strlen(ptr);
    if(iPosition > 0)
    {
        pszDispAlarmItemsBuf = NEW(char, iPosition + 1);
        if(pszDispAlarmItemsBuf == NULL)
        {
            return FALSE;
        }
        memset(pszDispAlarmItemsBuf, 0x0, (size_t)iPosition);
        strncpyz(pszDispAlarmItemsBuf, ptr, iPosition + 1);

    }

    //fwrite(pszDispAlarmItemsBuf,strlen(pszDispAlarmItemsBuf),1,pf);



    szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[8], iLanguage);
    //fwrite(szPath,strlen(szPath),1,pf);

    if(LoadHtmlFile(szPath, &pHtml ) > 0)
    {
        ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("stdEquipments"), pszStdEquipBuf);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("dispAlarmItems"), pszDispAlarmItemsBuf);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("stdEquipTypeName"), pszStdEquipTypeName);


        PostPage(pHtml);

        DELETE(pHtml);
        DELETE(pszStdEquipBuf);
        DELETE(pszDispAlarmItemsBuf);
        DELETE(pszStdEquipTypeName);


        pszStdEquipBuf          = NULL;
        pszDispAlarmItemsBuf    = NULL;
        pszStdEquipTypeName     = NULL;


    }

    //fclose(pf);

    DELETE(szPath);
    szPath = NULL;

    return TRUE;
}
//end////////////////////////////////////////////////////////////////////////

static void Web_SET_PostDHCPServerPage(IN char *pGetBuf, IN int iLanguage, IN int iDHCPServerType)
{
	ASSERT(pGetBuf);

	char	*pHtml = NEW(char,1000);
	if(pHtml == NULL)
	{
		return; 
	}
	sprintf(pHtml,"%s",strResult);

	//char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	szResult[3];


	pTGetBuf = pGetBuf;

	strncpyz(szResult, pTGetBuf, sizeof(szResult));

	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);


	if(atoi(szResult) != 1)
	{
		pTGetBuf = pTGetBuf + 2;
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
	}
	PostPage(pHtml);

	DELETE(pHtml);
	pHtml = NULL;

}

//static void Web_SET_PostVPNPage(IN char *pGetBuf, IN int iLanguage, IN int iVPNType)
//{
//	ASSERT(pGetBuf);
//
//	char	*pHtml = NEW(char,1000);
//	sprintf(pHtml,"%s",strResult);
//
//	//char	*szPath = NULL;
//	char	*pTGetBuf = NULL;
//	char	szResult[3];
//
//
//	pTGetBuf = pGetBuf;
//
//	strncpyz(szResult, pTGetBuf, sizeof(szResult));
//	if(iVPNType == SET_VPN_CONFIG)
//	{
//		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
//	}
//	else
//	{
//		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
//	}
//
//
//	pTGetBuf = pTGetBuf + 2;
//	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
//	
//	PostPage(pHtml);
//
//	DELETE(pHtml);
//	pHtml = NULL;
//
//}

//static void Web_SET_PostSMSPage(IN char *pGetBuf, IN int iLanguage, IN int iSMSType)
//{
//	ASSERT(pGetBuf);
//
//	char	*pHtml = NEW(char,1000);
//	sprintf(pHtml,"%s",strResult);
//
//	//char	*szPath = NULL;
//	char	*pTGetBuf = NULL;
//	char	szResult[3];
//
//
//	pTGetBuf = pGetBuf;
//
//	strncpyz(szResult, pTGetBuf, sizeof(szResult));
//	if(iSMSType == SET_SMS_CONFIG)
//	{
//		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
//	}
//	else
//	{
//		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
//	}
//
//
//	pTGetBuf = pTGetBuf + 2;
//	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
//	
//	PostPage(pHtml);
//
//	DELETE(pHtml);
//	pHtml = NULL;
//
//}


//static void Web_SET_PostSMTPPage(IN char *pGetBuf, IN int iLanguage, IN int iSMTPType)
//{
//	ASSERT(pGetBuf);
//
//	char	*pHtml = NEW(char,1000);
//	sprintf(pHtml,"%s",strResult);
//
//	//char	*szPath = NULL;
//	char	*pTGetBuf = NULL;
//	char	szResult[3];
//
//
//	pTGetBuf = pGetBuf;
//
//	strncpyz(szResult, pTGetBuf, sizeof(szResult));
//	if(iSMTPType == SET_SMTP_CONFIG)
//	{
//		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
//	}
//	else
//	{
//		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
//	}
//
//
//	pTGetBuf = pTGetBuf + 2;
//	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
//
//	PostPage(pHtml);
//
//	DELETE(pHtml);
//	pHtml = NULL;
//
//}

//////////////////////////////////////////////////////////////////////////
//Added by wj for YDN Setting config
static void Web_SET_PostYDN_TL1Page(IN char *pGetBuf, IN int iLanguage, IN int iYDNType)
{
    ASSERT(pGetBuf);

    char	*pHtml = NEW(char,4096);
    sprintf(pHtml,"%s",strResult);

    //char	*szPath = NULL;
    char	*pTGetBuf = NULL;
    char	szResult[3];


    pTGetBuf = pGetBuf;

        strncpyz(szResult, pTGetBuf, sizeof(szResult));
        //if(iYDNType == SET_YDN_CONFIG)
        //{
            //ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        //}
        //else
        //{
            ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        //}


        pTGetBuf = pTGetBuf + 2;
        ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
        
        PostPage(pHtml);

        DELETE(pHtml);
        pHtml = NULL;



}

//static void Web_SET_PostMODBUSPage(IN char *pGetBuf, IN int iLanguage, IN int iModbusType)
//{
//	ASSERT(pGetBuf);
//
//	char	*pHtml = NEW(char,1000);
//	sprintf(pHtml,"%s",strResult);
//
//	//char	*szPath = NULL;
//	char	*pTGetBuf = NULL;
//	char	szResult[3];
//
//
//	pTGetBuf = pGetBuf;
//
//	strncpyz(szResult, pTGetBuf, sizeof(szResult));
//	if(iModbusType == SET_MODBUS_CONFIG)
//	{
//		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
//	}
//	else
//	{
//		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
//	}
//
//
//	pTGetBuf = pTGetBuf + 2;
//	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
//
//	PostPage(pHtml);
//
//	DELETE(pHtml);
//	pHtml = NULL;
//
//
//
//}

static int Web_SET_PostCFGGCPSWorkPage(IN char *pGetBuf, IN int iLanguage)
{
	ASSERT(pGetBuf);

	char	*pHtml = NULL;
	char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	szResult[3];

	char        *pszSampling = NULL;
	char        *pszControl  = NULL;
	char        *pszSetting  = NULL;
	char        *pszAlarm    = NULL;
	char        *pszEquip    = NULL;
	char        *pszGCPSInfo = NULL;
	//char        *pszTemp     = NULL;
	char        *pszGCPSMode = NULL;
	//Read file
	pTGetBuf = pGetBuf;

	strncpyz(szResult, pTGetBuf, sizeof(szResult));


	char	*ptr = NULL;
	ptr = pGetBuf + 2;

	/*OEquip*/
	char		*pPosition = strchr(ptr,59);
	int			iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszEquip = NEW(char, iPosition + 1);
		if(pszEquip == NULL)
		{
			return FALSE;
		}
		memset(pszEquip, 0x0, (size_t)iPosition);
		strncpyz(pszEquip, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	//fwrite(pszEquip,strlen(pszEquip),1,pf);


	/*OSampling*/
	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr ;
	if(iPosition > 0)
	{
		pszSampling = NEW(char, iPosition + 1);
		if(pszSampling == NULL)
		{
			DELETE(pszEquip);
			return FALSE;
		}
		memset(pszSampling, 0x0, (size_t)iPosition);
		strncpyz(pszSampling, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	//fwrite(pszSampling,strlen(pszSampling),1,pf);

	/*OControl*/
	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszControl = NEW(char, iPosition + 1);
		if(pszControl == NULL)
		{
			DELETE(pszEquip);
			DELETE(pszSampling);
			return FALSE;
		}
		memset(pszControl, 0x0, (size_t)iPosition);
		strncpyz(pszControl, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;
	//fwrite(pszControl,strlen(pszControl),1,pf);

	/*OSetting*/
	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszSetting = NEW(char, iPosition + 1);
		if(pszSetting == NULL)
		{
			DELETE(pszEquip);
			DELETE(pszSampling);
			DELETE(pszControl);
			
			return FALSE;
		}
		memset(pszSetting, 0x0, (size_t)iPosition);
		strncpyz(pszSetting, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;
	//fwrite(pszSetting,strlen(pszSetting),1,pf);


	/*OAlarm*/

	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszAlarm = NEW(char, iPosition + 1);
		if(pszAlarm == NULL)
		{
			DELETE(pszEquip);
			DELETE(pszSampling);
			DELETE(pszControl);
			DELETE(pszSetting);
			return FALSE;
		}
		memset(pszAlarm, 0x0, (size_t)iPosition);
		strncpyz(pszAlarm, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	/*GCPSConfigInfo*/

	//iPosition = strlen(ptr);
	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszGCPSInfo = NEW(char, iPosition + 1);
		if(pszGCPSInfo == NULL)
		{
			DELETE(pszEquip);
			DELETE(pszSampling);
			DELETE(pszControl);
			DELETE(pszSetting);
			DELETE(pszAlarm);
			return FALSE;
		}
		memset(pszGCPSInfo, 0x0, (size_t)iPosition);
		strncpyz(pszGCPSInfo, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	/*power_splite_mode*/
	iPosition = strlen(ptr);
	if(iPosition > 0)
	{
		pszGCPSMode = NEW(char, iPosition + 1);
		if(pszGCPSMode == NULL)
		{
			DELETE(pszEquip);
			DELETE(pszSampling);
			DELETE(pszControl);
			DELETE(pszSetting);
			DELETE(pszAlarm);
			DELETE(pszGCPSInfo);
			return FALSE;
		}
		memset(pszGCPSMode, 0x0, (size_t)iPosition);
		strncpyz(pszGCPSMode, ptr, iPosition + 1);

	}

	//fwrite(pszAlarm,strlen(pszAlarm),1,pf);


	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[10], iLanguage);


	//fwrite(szPath,strlen(szPath),1,pf);

	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equipments"), pszEquip);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_sample_signals"), pszSampling);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_control_signals"), pszControl);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_setting_signals"), pszSetting);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_alarm_signals"), pszAlarm);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("configuration_file"), pszGCPSInfo);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("power_splite_mode"), pszGCPSMode);


		PostPage(pHtml);

		DELETE(pHtml);
		pHtml       = NULL;



	}

	//fclose(pf);
	DELETE(pszEquip);
	DELETE(pszSampling);
	DELETE(pszControl);
	DELETE(pszSetting);
	DELETE(pszAlarm);
	DELETE(pszGCPSInfo);
	DELETE(pszGCPSMode);

	pszSampling = NULL;
	pszControl  = NULL;
	pszSetting  = NULL;
	pszAlarm    = NULL;
	pszEquip    = NULL;		
	pszAlarm = NULL;
	pszGCPSInfo = NULL;
	pszGCPSMode = NULL;

	DELETE(szPath);
	szPath = NULL;

	return 1;
}

//changed by Frank Wu,10/N/27,20140527, for power split
static void Web_SET_PostCFGGCPSWorkPage2(IN char *pGetBuf, IN int iLanguage, IN int iCmdType)
{
	char *pHtml = NEW(char,1000);
	char *pTGetBuf = pGetBuf;
	char szResult[3];

	if(NULL == pHtml)
	{
		return;
	}
	else if(NULL == pTGetBuf)
	{
		SAFELY_DELETE(pHtml);
		return;
	}

	//framework
	sprintf(pHtml,"%s",strResult);
	//control
	strncpyz(szResult, pTGetBuf, sizeof(szResult));
	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
	//content
	pTGetBuf = pTGetBuf + 2;
	ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), pTGetBuf);
	//output
	PostPage(pHtml);

	SAFELY_DELETE(pHtml);

	pTGetBuf = NULL;
}

static int Web_SET_PostCFGGetSettingParamWorkPage(IN char *pGetBuf, IN int iLanguage)
{
	ASSERT(pGetBuf);
	//FILE *pf;
	//pf=fopen("/var/wj4.txt","a+");
	//char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
	//fwrite(p,strlen(p),1,pf);


	char	*pHtml = NULL;
	char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	szResult[4];

	pTGetBuf = pGetBuf;

	strncpyz(szResult, pTGetBuf, 3);

	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[11], iLanguage);

	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{
		//TRACE("LoadHtmlFile OK!!");

		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
		//fwrite(pHtml,strlen(pHtml),1,pf);


		printf("%s",pHtml);

		DELETE(pHtml);

		pHtml  = NULL;

	}

	//fclose(pf);

	DELETE(szPath);
	szPath = NULL;

	return 1;


}

static int Web_SET_PostAutoConfigWorkPage(iLanguage)
{
	//ASSERT(pGetBuf);


	//char *p="into Web_SET_PostAutoConfigWorkPage OK!!\n";



	char	*pHtml = NEW(char,1000);
		
	sprintf(pHtml,"%s",strResult);

	//char	*szPath = NULL;
	//char	*pTGetBuf = NULL;
	//char	szResult[4];

	//pTGetBuf = pGetBuf;

	//strcpyz(szResult, "1 ", 1);

	//szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[12], iLanguage);

	//if(LoadHtmlFile(szPath, &pHtml ) > 0)
	//{
		//TRACE("LoadHtmlFile OK!!");

		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTENT_RESULT"), "");
		//fwrite(pHtml,strlen(pHtml),1,pf);


		printf("%s",pHtml);

		DELETE(pHtml);

		pHtml  = NULL;

	//}

	//fclose(pf);

	//DELETE(szPath);
	//szPath = NULL;

	return 1;


}

//end////////////////////////////////////////////////////////////////////////



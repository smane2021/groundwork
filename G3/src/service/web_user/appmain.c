/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : appmain.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-09-28 14:47
 *  VERSION  : V1.00
 *  PURPOSE  :When the client connect to the server, this module provide authority validate,
 *			make device page ,create session.
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"  
#include "public.h"
#include "pubfunc.h"
#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/dir.h> 
#include <unistd.h>
#include <string.h>

//#ifndef _SUPPORT_THREE_LANGUAGE
//
//    #define	_SUPPORT_THREE_LANGUAGE
//
//#endif


/*CGI var*/
#define LOG_USERNAME					"user_name"
#define LOG_PASSWORD					"user_password"

/*buffer len to save authority level*/
#define MAX_AUTHORITY_LEVEL_LEN			2

/*User authority var*/
#define USER_BROWSER					5
#define USER_OPERATOR					6
#define	USER_ENGINEER					7
#define USER_ADMINISTRATOR				8


#define HTML_SAVE_TEMPLATE_PATH			"/app/www/html/template"//"/var/template"
#define HTML_SAVE_LOC_PATH				"/app/www/html/cgi-bin/loc"//"/var/loc"
#define HTML_SAVE_LOC2_PATH				"/app/www/html/cgi-bin/loc2"//"/var/loc"
//#define HTML_SAVE_ENG_PATH				"/app/www/html/cgi-bin/eng"//"/var/eng"

#define HTML_TREE_TEMPLATE_FILE			"/j02_tree_view.js"
#define HTML_EQUIP_TEMPLATE_FILE		"/j77_equiplist_def.htm"


#define HTML_TREE_FILE					"/j02_tree_view_t.js"
#define HTML_EQUIP_FILE					"/j77_equiplist_def.htm"
#define HTML_EQUIP_BACKUP_FILE				"/j77_equiplist_def_back.htm"

#define HTML_MAIN_FILE					"/p01_main_frame.htm"
#define HTML_LOGIN_FILE					"/login.htm"
	
/*must match with the client*/
#define HTML_EQUIP_LIST					"equip_list"
#define HTML_TREE_NODE					"tree_node"
#define HTML_ACU_INFO					"acu_information"
#define HTML_EQUIP_ALARM_LIST			"acu_equip_alarm"
#define HTML_GROUP_DEVICE_LIST			"acu_equip_group"
#define CONTROL_RESULT					"CONTROL_RESULT"
#define ID_COOKIE					"ID_COOKIE"
#define USER_DEF_PAGED_NODE				"user_def_pages_node"
#define USER_DEF_PAGED_INFO				"page_list"

#define MAX_CONNECT_NUMBER				5
#define WEB_DATA_LOGIN_CGI				"/app/www_user/html/datas/data.login_cgi.html"
#define WEB_DATA_LOGIN_CGI_VAR				"/var/datas/data.login_cgi.html"
/*Base64 code*/
const char DeBase64Tab[] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    62,        // '+'
    0, 0, 0,
    63,        // '/'
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61,        // '0'-'9'
    0, 0, 0, 0, 0, 0, 0,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
    13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,        // 'A'-'Z'
    0, 0, 0, 0, 0, 0,
    26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
    39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,        // 'a'-'z'
};

char bufFailCommErrCode[64];

//static char *Web_APP_StuffHead( OUT char *pszHead, IN char *pCookie,IN int iLenContent );
static int Web_APP_getCGIParam(OUT char *pszUserInfo,
							   OUT char *pszPass,
							   OUT int *pnLanguage,
							   OUT int *piClearSession,
							   OUT char *szSessionID);

static char  *Web_APP_sendControlCommand(IN const char *pszUserInfo,
								 IN const char *pszPassword,
								 IN const int iLanguage);  //Added by wj for three languages 2006.4.29

int DecodeBase64(const char* pSrc,  char* pDst, int iSrcLen);
int DecodeBase85(const char* pSrc,  char* pDst, int iSrcLen);// Added by Koustubh Mattikalli (IA-01-02)

static BOOL getFileListNumber(void);
//static void MakeModiTime(void);

char		strModiTime[128];
char		stRectNum[6];
//static char 	control_result[20];


int main(void)
{
    //char        *pLoc2EquipHtml = NULL, *pLoc2TreeHtml = NULL;//Added by wj for three languages 2006.4.29
	char		szUserName[16],szPass[65];
	int			iAuthority = 0,	iLanguage = 0, iClearSession = 0;
	char		*pszAuthority = NULL;
	//end////////////////////////////////////////////////////////////////////////
    FILE		*fp = NULL;
	//char		szMainFile[64];			/*get the file : p01_main_frame.htm*/
	//char		pEngTemplateFile[128], pLocTemplateFile[128];
    //char        pLoc2TemplateFile[128];//Added by wj for three languages 2006.4.29
	//char		pEquipFile[128];
	//char		pExchangeFile[64];
	int			iLen = 0;
	char		szLoginFile[64], *pLoginHtml = NULL, szReturnValue[4];
	char		*pCookie = NULL;
	//BOOL		boolInitLoc = FALSE, boolInitEng = FALSE;
	char		szSessionID[17] = {0};
	char		szSessionFile[64] ={0};
	char		szPath[128];

	memset(szUserName, 0, sizeof(szUserName));
	memset(szPass, 0, sizeof(szPass));
	if(Web_APP_getCGIParam(szUserName,szPass,&iLanguage, &iClearSession, szSessionID) == FALSE)
	{
		return FALSE;
	}

	iLanguage =	ENGLISH_LANGUAGE_NAME;

	sprintf(szLoginFile, "%s", WEB_DATA_LOGIN_CGI);
	if( LoadHtmlFile(szLoginFile, &pLoginHtml) <= 0)
	{
	    return FALSE;
	}

	if(iClearSession > 0)
	{
		sprintf(szSessionFile,"%s%s",SESSION_DEFAULT_DIR, szSessionID); 
		remove(szSessionFile);

		return FALSE;
	}

	//list session file number
	if(getFileListNumber() == FALSE)
	{
		clean_session_file();
		ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), "5");
		printf("Content-type:text/html\n\n");
		PostPage(pLoginHtml);
		DELETE(pLoginHtml);
		pLoginHtml = NULL;

		return FALSE;
	}

 	/*send equipId to ACU and get realtimedata*/
	
	pszAuthority = Web_APP_sendControlCommand(szUserName, 
									  szPass, 
									  iLanguage);

	if(pszAuthority != NULL)
	{
		iAuthority = atoi(pszAuthority);
	
		/*head*/

		if(iAuthority > 0)
		{
			/*Construct session file based on User name and password*/
			pCookie = set_session(szUserName,szPass, pszAuthority);
			if(pCookie == NULL)
			{
				return FALSE;
			}


			DELETE(pszAuthority);
			pszAuthority = NULL;

			ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), "1");
			ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(ID_COOKIE), pCookie);
			memset(szPath, 0, sizeof(szPath));
			sprintf(szPath, "%s", WEB_DATA_LOGIN_CGI_VAR);
			/*if(szPath == NULL)
			{
			    fopen("/var/file4.html","wb");
			}
			if((fp = fopen("/var/datas/data.login_cgi.html","wb")) == NULL)
			{
			    fopen("/var/file5.html","wb");
			}
			if(pLoginHtml == NULL)
			{
			    fopen("/var/file6.html","wb");
			}*/
			if((fp = fopen(szPath,"wb")) != NULL && pLoginHtml != NULL)
			{
			    fwrite(pLoginHtml,strlen(pLoginHtml), 1, fp);
			    fclose(fp);
			}
			else
			{
			}
			printf("Content-type:text/html\n\n");
			PostPage(pLoginHtml);

			DELETE(pCookie);
			pCookie = NULL;
			DELETE(pLoginHtml);
			pLoginHtml = NULL;
		}
		else  
		{
		    if(iAuthority == -3)
		    {
			iLen = sprintf(szReturnValue,"%d", 6);
			szReturnValue[iLen] = '\0';
			ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
			printf("Content-type:text/html\n\n");
			PostPage(pLoginHtml);
			DELETE(pLoginHtml);
			pLoginHtml = NULL;
			DELETE(pszAuthority);
			pszAuthority = NULL; 
		    }
			else if(iAuthority == -4)
			{
			    iLen = sprintf(szReturnValue,"%d", 7);
			    szReturnValue[iLen] = '\0';
			    ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
			    printf("Content-type:text/html\n\n");
			    PostPage(pLoginHtml);
			    DELETE(pLoginHtml);
			    pLoginHtml = NULL;
			    DELETE(pszAuthority);
			    pszAuthority = NULL; 
			}
			else if(iAuthority == -5)
			{
			    iLen = sprintf(szReturnValue,"%d", 8);
			    szReturnValue[iLen] = '\0';
			    ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
			    printf("Content-type:text/html\n\n");
			    PostPage(pLoginHtml);
			    DELETE(pLoginHtml);
			    pLoginHtml = NULL;
			    DELETE(pszAuthority);
			    pszAuthority = NULL; 
			}
                        // Added by koustubh (IA-01-04)
                        else if(iAuthority == -9)
                        {
                            iLen = sprintf(szReturnValue,"%d", 11);
                            szReturnValue[iLen] = '\0';
                            ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
                            printf("Content-type:text/html\n\n");
                            PostPage(pLoginHtml);
                            DELETE(pLoginHtml);
                            pLoginHtml = NULL;
                            DELETE(pszAuthority);
                            pszAuthority = NULL;
                        }
			else if(iAuthority == -6)
			{
		//FILE *pf1=NULL;
		//pf1=fopen("/var/Fenge1212.txt","wb");
		//fwrite("-6\n",strlen("-6\n"),1,pf1);
		//fclose(pf1);
			
			    iLen = sprintf(szReturnValue,"%d", 9);
			    szReturnValue[iLen] = '\0';
			    ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
			    printf("Content-type:text/html\n\n");
			    PostPage(pLoginHtml);
			    DELETE(pLoginHtml);
			    pLoginHtml = NULL;
			    DELETE(pszAuthority);
			    pszAuthority = NULL; 
			}
            else
            {
            	if((iAuthority == -7) || (iAuthority == -8))
            	{
            		iLen = sprintf(szReturnValue,"%d", iAuthority + 17);
            	}
            	else
            	{
            		iLen = sprintf(szReturnValue,"%d", iAuthority + 4);
            	}
			szReturnValue[iLen] = '\0';
			ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
			if(iAuthority == 0)
			{
				snprintf(bufFailCommErrCode,32," errCode:9");
				ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(ID_COOKIE), bufFailCommErrCode);
			}
			printf("Content-type:text/html\n\n");
			PostPage(pLoginHtml);
			DELETE(pLoginHtml);
			pLoginHtml = NULL;
			DELETE(pszAuthority);
			pszAuthority = NULL;
            }
		}
			////AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_INFO," Error : Fail to get User or Password!");
		
	}
	else
	{
	    iLen = sprintf(szReturnValue,"%d", 4 );
	    /*Return value :2 incorrect password, 3 : incorrect user*/

	    ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
		ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(ID_COOKIE), bufFailCommErrCode);

	    printf("Content-type:text/html\n\n");

	    PostPage(pLoginHtml);

	    DELETE(pLoginHtml);
	    pLoginHtml = NULL;
	    DELETE(pszAuthority);
	    pszAuthority = NULL; 
	}
	/*Delete */
	if(pszAuthority != NULL)
	{
		DELETE(pszAuthority);
		pszAuthority = NULL;
	}
	if(pLoginHtml != NULL)
	{
	    DELETE(pLoginHtml);
	    pLoginHtml = NULL;
	}
	return TRUE; 
}


/*==========================================================================*
 * FUNCTION :  Web_APP_getCGIParam
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char *pszUserInfo:
				OUT char *pszPass:
				OUT int *pnLanguage:
				OUT int *piClearSession:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : 			             DATE: 2004-11-06 10:20
 *==========================================================================*/
static int Web_APP_getCGIParam(OUT char *pszUserInfo,
							   OUT char *pszPass,
							   OUT int *pnLanguage,
							   OUT int *piClearSession,
							   OUT char *szSessionID)
{
    char **postvars = NULL; /* POST request data repository */
    char **getvars = NULL; /* GET request data repository */
    int form_method; /* POST = 1, GET = 0 */  
    char *val = NULL;
	char szUserVal[100];	 // old value 65 // changed by Koustubh Mattikalli (Ia-01-02)
	/*get html send form method : POST or Get*/
    form_method = getRequestMethod();
    if(form_method == POST) 
	{
        getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		return FALSE;
	}
	
	/*get param*/

    val = getValue( getvars, postvars, LOG_USERNAME );
    if( val != NULL )
    {
		
		/*user*/
		memset(szUserVal, 0, sizeof(szUserVal));
		sprintf(szUserVal,"%s",val);
		//sprintf(pszUserInfo,"%s",val);
		//DecodeBase64(szUserVal,pszUserInfo,(int)strlen(szUserVal));		
		DecodeBase85(szUserVal,pszUserInfo,(int)strlen(szUserVal));//Added by Koustubh Mattikalli (IA-01-02)
    }
	memset(szUserVal, 0, sizeof(szUserVal));
	/*password*/
	val = getValue( getvars, postvars, LOG_PASSWORD );
    if( val != NULL ) 
    {
		sprintf(szUserVal,"%s",val);
		//sprintf(pszPass,"%s",val);
		//DecodeBase64(szUserVal,pszPass,(int)strlen(szUserVal));
		DecodeBase85(szUserVal,pszPass,(int)strlen(szUserVal));//Added by Koustubh Mattikalli (IA-01-02)
    }
	
	
	/*language type*/
	val = getValue(getvars,postvars,LANGUAGE_TYPE);
	if(val != NULL)
	{
		*pnLanguage = atoi(val);   //Language type
	}
	
	/*clear session*/
	val = getValue(getvars,postvars,CGI_CLEAR_SESSION);


	char szVal[32];
	if(val != NULL)
	{
		
		*piClearSession = 1;//atoi(val);   //clear session
		if((val = getValue(getvars,postvars,SESSION_ID)) != NULL)
		{
			strncpyz(szSessionID, val, 17);
		}
		sprintf(szVal, "%d\n", 1);
	}
	else
	{
		*piClearSession = 0;
		sprintf(szVal, "%d\n", 0);
	}



	/*free*/
	cleanUp( getvars, postvars);

    return TRUE;  
}




/*==========================================================================*
 * FUNCTION :  Web_APP_sendControlCommand
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : 			             DATE: 2004-11-06 10:20
 *==========================================================================*/
static char  *Web_APP_sendControlCommand(IN const char *pszUserInfo,
								 IN const char *pszPassword,
								 IN const int iLanguage)  //Added by wj for three languages 2006.4.29
{
	int				fdMain,fdProcess;    //fifo handle
	int				iLen = 0, iBufCount = 0;
	char			szBuf1[1024],szBuf2[PIPE_BUF],szFifoName[32];
 	mode_t			mode = 0666;
	char			*pBuf = NULL;
	char			*pszAuthority = NULL;
	char			*pDelete = NULL;   //Used by save the first address of pBuf, aim to delete;

	/*create FIFO with our PID as part of name*/
	/*open Main fifo to write*/ 

	if((fdMain = open(MAIN_FIFO_NAME,O_WRONLY )) < 0)
	{

		////AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_WARNING,"Error : Fail to open FIFO [%s]", MAIN_FIFO_NAME);
		//TRACE("Error : Fail to open FIFO---2\n");
		snprintf(bufFailCommErrCode,32," errCode:1;%d",fdMain);

		return NULL;
	}


	sprintf(szFifoName,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	
	/*make process fifo*/
	if((mkfifo(szFifoName,mode))<0)
	{
		////AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_WARNING,"Error : Fail to make FIFO [%s]", szFifoName);
		snprintf(bufFailCommErrCode,32," errCode:2");
		return NULL;
	}
	/*start buffer with pid and a blank*/
	iLen = sprintf(szBuf1,"%10ld%2d%16s%64s%2d",(long)getpid(), LOG_IN,pszUserInfo,pszPassword,iLanguage); //password length updated to 64byte
	szBuf1[iLen] = '\0';

	/*write data to web_ui_comm*/
	if((write(fdMain,szBuf1,strlen(szBuf1) + 1)) < 0)
	{
		//AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_WARNING,"Error : Fail to write FIFO %s", MAIN_FIFO_NAME);
		snprintf(bufFailCommErrCode,32," errCode:3");
		close(fdMain);
		return NULL;
	}

	/*open process fifo to read*/
	if((fdProcess = open(szFifoName,O_RDONLY ))<0)
	{
		//AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_WARNING,"Fail to open FIFO [%s]", szFifoName);
		snprintf(bufFailCommErrCode,32," errCode:4;%d",fdProcess);
		close(fdMain);
        close(fdProcess);
		return NULL;
	}

	/*allocate memory to save the data from web_ui_comm*/
	pBuf = NEW(char,1 * PIPE_BUF);
	if(pBuf == NULL)
	{
		snprintf(bufFailCommErrCode,32," errCode:5");
		close(fdMain);
        close(fdProcess);
        return NULL;
	}
	//FILE *fp;
	/*read data from web_ui_comm*/
	//int iGetBufferLen = 0;
	while((iLen = read(fdProcess,szBuf2,PIPE_BUF - 1)) > 0)
	{

		if(iBufCount >= 1)
		{
			pBuf = RENEW(char, pBuf, (iBufCount + 1) * PIPE_BUF);
			if(pBuf == NULL)
			{
				snprintf(bufFailCommErrCode,32," errCode:6");
                close(fdMain);
                close(fdProcess);
				return NULL;
			}

		}
		//strncpyz(pBuf + iBufCount * (PIPE_BUF-1),szBuf2, (int)strlen(szBuf2) + 1);
		strlcat(pBuf, szBuf2,(iBufCount + 1) * PIPE_BUF);
		iBufCount++;
	}
		pDelete = pBuf;

	if(strlen(pBuf) <= 0 )
	{
		DELETE(pBuf);
		pBuf = NULL;
		snprintf(bufFailCommErrCode,32," errCode:7,%d",strlen(pBuf));
        close(fdMain);
        close(fdProcess);
		return NULL;
	}
	pBuf++;

	pszAuthority = NEW(char, MAX_AUTHORITY_LEVEL_LEN + 1);
	if(pszAuthority == NULL)
	{
        DELETE(pBuf);
        pBuf = NULL;
		snprintf(bufFailCommErrCode,32," errCode:8");
        close(fdMain);
        close(fdProcess);
		return NULL;
	}
	strncpyz(pszAuthority, pBuf, MAX_AUTHORITY_LEVEL_LEN + 1);

	DELETE(pDelete);		/*free pBuf*/
	pDelete = NULL;

	close(fdMain);
	close(fdProcess);
	unlink(szFifoName);

	return pszAuthority;	
}

 

/*==========================================================================*
 * FUNCTION :  
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : 			             DATE: 2004-11-06 10:20
 *==========================================================================*/ 
int DecodeBase64(const char* pSrc,  char* pDst, int iSrcLen)
{
    int			iDstLen;            // Output char len count
    int			iValue;             // for decoding
    int			i;
 
    i = 0;
    iDstLen = 0;
	
	iSrcLen = iSrcLen + 3;
    // get 4 char ,then decode to a int and move to 3 bit
	while (i < iSrcLen)
    {
        if (*pSrc != '\r' && *pSrc!='\n')
        {
            iValue = DeBase64Tab[(int)*pSrc++] << 18;
            iValue +=(int) DeBase64Tab[(int)*pSrc++] << 12;
            *pDst++ = (iValue & 0x00ff0000) >> 16;
            iDstLen++;
 
            if (*pSrc != '=')
            {
                iValue += (int)DeBase64Tab[(int)*pSrc++] << 6;
                *pDst++ = (iValue & 0x0000ff00) >> 8;
                iDstLen++;
 
                if (*pSrc != '=')
                {
                    iValue += (int)DeBase64Tab[(int)*pSrc++];
                    *pDst++ =iValue & 0x000000ff;
                    iDstLen++;
                }
            }
 
			//TRACE("pDst[%s]\n", pDst);
            i += 4;
        }
        else        // Enter ,Jump
        {
            pSrc++;
            i++;
        }
     }
 
    // the output add tail
    *pDst = '\0';
	//TRACE("pDst[%s][%d]\n", pDst, iDstLen);
    return iDstLen;
}

/*==========================================================================*
 * FUNCTION :  
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : 	Koustubh Mattikalli (IA-01-02)             DATE: 2019-09-13
 *==========================================================================*/ 
int DecodeBase85(const char* pSrc,  char* pDst, int iSrcLen)
{
    //fileWrite("----------","Entering","DecodeBase85","----------");
    int it = iSrcLen/5;   // loop inerations
    int i,j,k,v;
    char *buf = NULL, *ref = NULL;
    char str[50] = {'\0'};

    if(iSrcLen % 5)
        it++;

    buf = (char*)malloc(sizeof(char)*it*5);
    if(buf == NULL)
    {
        //fileWrite("[DecodeBase85]","ERR","memory alloc failed",NULL);
    }
    ref = buf;

    sprintf(str,"%d -- %d",ref,buf);
    //fileWrite("DecodeBase85","memory",str,NULL);

    memset(buf,'u',sizeof(char)*it*5);
    strncpy(buf,pSrc,iSrcLen);

    k=3;
    for(i = 0; i<it; i++)
    {
        v=0;
        v += (buf[4]-33);
        v += (buf[3]-33) *85;
        v += (buf[2]-33) *85*85;
        v += (buf[1]-33) *85*85*85;
        v += (buf[0]-33) *85*85*85*85;

        pDst[k]   = (v & 0x000000ff);
        pDst[k-1] = (v & 0x0000ff00) >> 8;
        pDst[k-2] = (v & 0x00ff0000) >> 16;
        pDst[k-3] = (v & 0xff000000) >> 24;

        buf += 5;
        k += 4;
    }
    free(ref);
    //fileWrite("[DecodeBase85]",pSrc,pDst,NULL);
    //fileWrite("----------","Entering","DecodeBase85","----------");
    return strlen(pDst);

}

static BOOL getFileListNumber(void)
{
#define VAR_DIR				"/var"
#define SESS_FILE_HEAD		"sess_"
	
	DIR			*dp; 
	struct direct *dir;
	int			iDirNum = 0;
	char		szFileHead[6];

	if ((dp = opendir(VAR_DIR))== NULL) 
	{ 
		return -1; 
	} 
	while ((dir = readdir(dp)) != NULL) 
	{ 
		if(dir->d_ino == 0) 
			continue; 

		strncpyz(szFileHead,dir->d_name,sizeof(szFileHead));

		if(strcmp(szFileHead, SESS_FILE_HEAD) == 0)
		{
			iDirNum++;
		}
	}
	closedir(dp);
	
	if(iDirNum >= MAX_CONNECT_NUMBER )
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}

}


/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NSC3.0(Net Sure Controller)
*
*  FILENAME : jsonFile.c
*  CREATOR  : Zhao Zicheng              DATE: 2013-07-20
*  VERSION  : V1.00
*  PURPOSE  : This file is to make the json data files used by customer PC webpages.
*
*  HISTORY  : 
*
*==========================================================================*/

#include <signal.h>
#include "stdsys.h"


#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/resource.h>		/* setrlimit */
#include <sys/types.h>			/* socket, bind, accept */
#include <sys/socket.h>			/* socket, bind, accept, setsockopt, */
#include <netdb.h>
#include <net/route.h>			/*rtentry*/
#include <net/if.h>			/*ifreq */
#include <time.h>

#include <sys/dir.h> 
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>

#include "web_user.h"

#include "cgi_pub.h"
//#include "cgi_query_setting.h"
#include "../../mainapp/config_mgmt/cfg_mgmt.h"

//static int	iVarSubID = 0;
//static int	iBufLen = 0;
//static int	iTimeOut = 0;
//int	iRAlarmSigNumRet = 0;
//int	g_iLanguage = 0;
static WEB_PRIVATE_RECT_SIGNAL_INFO	*gs_pWebRectSigInfo[6];	//the pointer array pointing 6 kinds of rectifier signal
static WEB_PRIVATE_RECT_SIGNAL_INFO	*gs_pWebBattSigInfo[8];	//the pointer array pointing 7 kinds of battery signal
static WEB_PRIVATE_RECT_SIGNAL_INFO	*gs_pWebDCSigInfo[5];	//the pointer array pointing 4 kinds of DC signal
static WEB_PRIVATE_RECT_SIGNAL_INFO	*gs_pWebOtherSigInfo[20];	//the pointer array pointing Other equip signals, the equip list is in the WEBPAGE_OTHER_EQUIP
// the equip are SMAC, DG, AC meter, DC meter, SMIO, DC
static unsigned int		gs_uiRectifierNumber = 0;
static unsigned int		gs_uiAllRectifierNumber = 0;
static unsigned int		gs_uiRectifierS1Number = 0;
static unsigned int		gs_uiRectifierS2Number = 0;
static unsigned int		gs_uiRectifierS3Number = 0;
static unsigned int		gs_uiConverterNumber = 0;
static unsigned int		gs_uiSolarNumber = 0;
static BOOL			g_bEquipIB1Exist = FALSE;
static WEB_PRIVATE_SIGNAL_INFO	*gs_pWebSigInfo = NULL;
static WEB_PRIVATE_SETTING_INFO	*gs_pWebSetInfo = NULL;
WEB_CABINET_INFO		gs_pWebCabinetInfo[WEB_MAX_CABINET_NUM];
static WEB_CABINET_POWER_DATA	*gs_pWebCabPowerData = NULL;
int			gs_iBranchNum;
int			gs_iCabinetNumber;
static WEB_PRIVATE_DISPLAY_INFO	*gs_pWebDisplayInfo = NULL;
static WEB_PRIVATE_DISPLAY_INFO	*gs_pWebDisplaySamInfo = NULL;
//HIS_DATA_RECORD_LOAD    g_stLoadCurrentData[ONE_MONTH_HOUR];//load current trend curve data
static HIS_DATA_RECORD_LOAD    gs_stACCurrentData[FOUR_WEEK_HOUR];//AC energy resource trend curve data
static HIS_DATA_RECORD_LOAD    gs_stDGCurrentData[FOUR_WEEK_HOUR];//DG energy resource trend curve data
static HIS_DATA_RECORD_LOAD    gs_stSolarCurrentData[FOUR_WEEK_HOUR];//solar energy resource trend curve data
static HIS_DATA_RECORD_LOAD    gs_stWindCurrentData[FOUR_WEEK_HOUR];//wind energy resource trend curve data
static	HIS_DATA_RECORD_LOAD	gs_stAmbientTempData[FOUR_WEEK_HOUR];
static	HIS_DATA_RECORD_LOAD	gs_stCompTempData[FOUR_WEEK_HOUR];
static	HIS_DATA_RECORD_LOAD	gs_stBatt1CapData[FOUR_WEEK_HOUR];
static	HIS_DATA_RECORD_LOAD	gs_stBatt2CapData[FOUR_WEEK_HOUR];
static	WEB_SET_EXIST_STATE	gs_stSetState;
static float gs_floadCurrentAverage = 0;//the average load current
static int gs_iLoadCurrentNum = 0;
static int gs_iHybrid = 0;
//int g_iACCurrentNum = 0;
//int g_iDGCurrentNum = 0;
//int g_iSolarCurrentNum = 0;
//int g_iWindCurrentNum = 0;
static HIS_DATA_RECORD_LOAD            gs_stLoadDataMax;	//the maximum load current

static unsigned int gs_uiRealComBattNum = 0;
static unsigned int gs_uiRealEIBBattNum = 0;
static unsigned int gs_uiRealSMDUBattNum = 0;
static unsigned int gs_uiRealSMDUEBattNum = 0;
static unsigned int gs_uiRealSMBattNum = 0;
static unsigned int gs_uiRealLargeDUBattNum = 0;
static unsigned int gs_uiRealSMBRCBattNum = 0;
static unsigned int gs_uiRealSoNickBattNum = 0;
static unsigned int gs_uiRealSMDUNum = 0;
static unsigned int gs_uiRealSMDUPNum = 0;
static unsigned int gs_uiRealSMDUENum = 0;
static unsigned int gs_uiRealEIBNum = 0;
static unsigned int gs_uiRealSMDUHNum = 0;
static unsigned int gs_uiRealLVDNum = 0;
static unsigned int gs_uiRealLVDUnitNum = 0;
static unsigned int gs_uiRealSMDULVDUnitNum = 0;
static unsigned int gs_uiRealLVDGroupNum = 0;
static unsigned int gs_uiRealBattFuseGroupNum = 0;
static unsigned int gs_uiRealBattFuseNum = 0;
static unsigned int gs_uiRealSMDUBattFuseNum = 0;
//changed by Frank Wu,5/5/32,20140312, for adding new web pages to the tab of "power system"
static unsigned int gs_uiRealSystemGroupNum = 0;
static unsigned int gs_uiRealACGroupNum = 0;
static unsigned int gs_uiRealACUnitNum = 0;
static unsigned int gs_uiRealACMeterGroupNum = 0;
static unsigned int gs_uiRealACMeterUnitNum = 0;
static unsigned int gs_uiRealDCUnitNum = 0;
static unsigned int gs_uiRealDCMeterGroupNum = 0;
static unsigned int gs_uiRealDCMeterUnitNum = 0;
static unsigned int gs_uiRealDieselGroupNum = 0;
static unsigned int gs_uiRealDieselUnitNum = 0;
static unsigned int gs_uiRealFuelGroupNum = 0;
static unsigned int gs_uiRealFuelUnitNum = 0;
static unsigned int gs_uiRealIBGroupNum = 0;
static unsigned int gs_uiRealIBUnitNum = 0;
static unsigned int gs_uiRealEIBGroupNum = 0;
static unsigned int gs_uiRealOBACUnitNum = 0;
static unsigned int gs_uiRealOBLVDUnitNum = 0;
static unsigned int gs_uiRealOBFuelUnitNum = 0;
static unsigned int gs_uiRealSMDUGroupNum = 0;
static unsigned int gs_uiRealSMDUPGroupNum = 0;
static unsigned int gs_uiRealSMDUEGroupNum = 0;
static unsigned int gs_uiRealSMDUHGroupNum = 0;
static unsigned int gs_uiRealSMBRCGroupNum = 0;
static unsigned int gs_uiRealSMBRCUnitNum = 0;
static unsigned int gs_uiRealSMIOGroupNum = 0;
static unsigned int gs_uiRealSMIOUnitNum = 0;
static unsigned int gs_uiRealSMTempGroupNum = 0;
static unsigned int gs_uiRealSMTempUnitNum = 0;
static unsigned int gs_uiRealSMACUnitNum = 0;
static unsigned int gs_uiRealSMLVDUnitNum = 0;
static unsigned int gs_uiRealLVD3UnitNum = 0;
static unsigned int gs_uiRealOBBattFuseUnitNum = 0;

static unsigned int gs_uiSMACNum = 0;
static unsigned int gs_uiDGNum = 0;
static unsigned int gs_uiACMeterNum = 0;
static unsigned int gs_uiDCMeterNum = 0;
static unsigned int gs_uiNaradaBMSNum = 0;
static unsigned int gs_uiRealNaradaBMSGroupNum = 0;
static unsigned int gs_uiSMIONum = 0;
static unsigned int gs_uiACUnitNum = 1;

static long gs_lProtocolType = 0;
//changed by Stone Song ,20160606, for adding the the web setting tab page 'FUSE'
//static unsigned int iOBBATT_FuseRealPNum = 0;
static unsigned int iSMDUBATT_FuseRealNum = 0;
static unsigned int iOBDC_FuseRealNum = 0;
static unsigned int iSMDUDC_FuseRealNum = 0;
static unsigned int iSMDUPLUS_FuseRealNum = 0;
static unsigned int iSMDUEBATT_FuseRealNum = 0;
static unsigned int iSMDUEDC_FuseRealNum = 0;

//int gs_iPaswdChangeFlag = 1;


#define MAX_LENGTH_SET_DATA_BUFFER	8192

static unsigned int gs_uiSourceEIBExist = 0;
static unsigned int gs_uiSourceSMDUExist = 0;

WEB_CONSUMPTION_MAP_INFO gs_stConsumMapInfo;
static int dataRefreshFlag = 1;
//static long gs_lVoltLevel = 0;

//static int gs_iLoadCurrMask = 0;
//static int gs_iWebSetMask = 0;
//static HIS_DATA_RECORD_LOAD *gs_pParam = NULL;

WEB_GENERAL_SIGNAL_INFO *pstSysCurr, *pstSysVolt, *pstConCurr, *pstConVolt, *pstTimeFor;

static char gs_szBuffer[BUFFER_LEN];

#define ID_SYSTEM		1
#define MAX_LENGTH_LANG_OPTION	128
static char gs_szLangOptionBuf[MAX_LENGTH_LANG_OPTION];

static const char szTemplateFileSignal[][64] = {
    "index.html:Number","index.html",
    "tmp.system_rectifier_group.html:Number","tmp.system_rectifier_group.html",
    //"tmp.system_rectifier_single.html:Number","tmp.system_rectifier_single.html",
    "tmp.system_rectifierS1_group.html:Number","tmp.system_rectifierS1_group.html",
    //"tmp.system_rectifierS1_single.html:Number","tmp.system_rectifierS1_single.html",
    "tmp.system_rectifierS2_group.html:Number","tmp.system_rectifierS2_group.html",
    //"tmp.system_rectifierS2_single.html:Number","tmp.system_rectifierS2_single.html",
    "tmp.system_rectifierS3_group.html:Number","tmp.system_rectifierS3_group.html",
    //"tmp.system_rectifierS3_single.html:Number","tmp.system_rectifierS3_single.html",
    "tmp.system_converter_group.html:Number","tmp.system_converter_group.html",
    //"tmp.system_converter_single.html:Number","tmp.system_converter_single.html",
    "tmp.system_solar_group.html:Number","tmp.system_solar_group.html",
    //"tmp.system_solar_single.html:Number","tmp.system_solar_single.html",
    "tmp.system_battery_group.html:Number","tmp.system_battery_group.html",
    //"tmp.system_battery_single.html:Number","tmp.system_battery_single.html",
    //"tmp.system_battery_eib.html:Number","tmp.system_battery_eib.html",
    //"tmp.system_battery_smdu.html:Number","tmp.system_battery_smdu.html",
    //"tmp.system_battery_smbat.html:Number","tmp.system_battery_smbat.html",
    //"tmp.system_battery_largedu.html:Number","tmp.system_battery_largedu.html",
    //"tmp.system_battery_smbrc.html:Number","tmp.system_battery_smbrc.html",
    //"tmp.system_smdu.html:Number","tmp.system_smdu.html",
    //"tmp.system_smdup.html:Number","tmp.system_smdup.html",
    "tmp.system_smdup1.html:Number","tmp.system_smdup1.html",
    //"tmp.system_smduh.html:Number","tmp.system_smduh.html",
    //"tmp.system_eib.html:Number","tmp.system_eib.html",
    "tmp.system_config.html:Number","tmp.system_config.html",
    "DISPLAY_FUNCTION:Number","DISPLAY_FUNCTION"
};

static const char szTemplateCabinet[12][32] = {
    "[Cabinet1_",
    "[Cabinet2_",
    "[Cabinet3_",
    "[Cabinet4_",
    "[Cabinet5_",
    "[Cabinet6_",
    "[Cabinet7_",
    "[Cabinet8_",
    "[Cabinet9_",
    "[Cabinet10_",
    "[Cabinet11_",
    "[Cabinet12_"
};

static const char szTemplateDU[12 * 6][32] = {
    "[Cabinet1_DU1]",
    "[Cabinet1_DU2]",
    "[Cabinet1_DU3]",
    "[Cabinet1_DU4]",
    "[Cabinet1_DU5]",
    "[Cabinet1_DU6]",
    "[Cabinet2_DU1]",
    "[Cabinet2_DU2]",
    "[Cabinet2_DU3]",
    "[Cabinet2_DU4]",
    "[Cabinet2_DU5]",
    "[Cabinet2_DU6]",
    "[Cabinet3_DU1]",
    "[Cabinet3_DU2]",
    "[Cabinet3_DU3]",
    "[Cabinet3_DU4]",
    "[Cabinet3_DU5]",
    "[Cabinet3_DU6]",
    "[Cabinet4_DU1]",
    "[Cabinet4_DU2]",
    "[Cabinet4_DU3]",
    "[Cabinet4_DU4]",
    "[Cabinet4_DU5]",
    "[Cabinet4_DU6]",
    "[Cabinet5_DU1]",
    "[Cabinet5_DU2]",
    "[Cabinet5_DU3]",
    "[Cabinet5_DU4]",
    "[Cabinet5_DU5]",
    "[Cabinet5_DU6]",
    "[Cabinet6_DU1]",
    "[Cabinet6_DU2]",
    "[Cabinet6_DU3]",
    "[Cabinet6_DU4]",
    "[Cabinet6_DU5]",
    "[Cabinet6_DU6]",
    "[Cabinet7_DU1]",
    "[Cabinet7_DU2]",
    "[Cabinet7_DU3]",
    "[Cabinet7_DU4]",
    "[Cabinet7_DU5]",
    "[Cabinet7_DU6]",
    "[Cabinet8_DU1]",
    "[Cabinet8_DU2]",
    "[Cabinet8_DU3]",
    "[Cabinet8_DU4]",
    "[Cabinet8_DU5]",
    "[Cabinet8_DU6]",
    "[Cabinet9_DU1]",
    "[Cabinet9_DU2]",
    "[Cabinet9_DU3]",
    "[Cabinet9_DU4]",
    "[Cabinet9_DU5]",
    "[Cabinet9_DU6]",
    "[Cabinet10_DU1]",
    "[Cabinet10_DU2]",
    "[Cabinet10_DU3]",
    "[Cabinet10_DU4]",
    "[Cabinet10_DU5]",
    "[Cabinet10_DU6]",
    "[Cabinet11_DU1]",
    "[Cabinet11_DU2]",
    "[Cabinet11_DU3]",
    "[Cabinet11_DU4]",
    "[Cabinet11_DU5]",
    "[Cabinet11_DU6]",
    "[Cabinet12_DU1]",
    "[Cabinet12_DU2]",
    "[Cabinet12_DU3]",
    "[Cabinet12_DU4]",
    "[Cabinet12_DU5]",
    "[Cabinet12_DU6]",
};

static const char szTemplateDU1[12 * 6][32] = {
    "[Cabinet1_DU1",
    "[Cabinet1_DU2",
    "[Cabinet1_DU3",
    "[Cabinet1_DU4",
    "[Cabinet1_DU5",
    "[Cabinet1_DU6",
    "[Cabinet2_DU1",
    "[Cabinet2_DU2",
    "[Cabinet2_DU3",
    "[Cabinet2_DU4",
    "[Cabinet2_DU5",
    "[Cabinet2_DU6",
    "[Cabinet3_DU1",
    "[Cabinet3_DU2",
    "[Cabinet3_DU3",
    "[Cabinet3_DU4",
    "[Cabinet3_DU5",
    "[Cabinet3_DU6",
    "[Cabinet4_DU1",
    "[Cabinet4_DU2",
    "[Cabinet4_DU3",
    "[Cabinet4_DU4",
    "[Cabinet4_DU5",
    "[Cabinet4_DU6",
    "[Cabinet5_DU1",
    "[Cabinet5_DU2",
    "[Cabinet5_DU3",
    "[Cabinet5_DU4",
    "[Cabinet5_DU5",
    "[Cabinet5_DU6",
    "[Cabinet6_DU1",
    "[Cabinet6_DU2",
    "[Cabinet6_DU3",
    "[Cabinet6_DU4",
    "[Cabinet6_DU5",
    "[Cabinet6_DU6",
    "[Cabinet7_DU1",
    "[Cabinet7_DU2",
    "[Cabinet7_DU3",
    "[Cabinet7_DU4",
    "[Cabinet7_DU5",
    "[Cabinet7_DU6",
    "[Cabinet8_DU1",
    "[Cabinet8_DU2",
    "[Cabinet8_DU3",
    "[Cabinet8_DU4",
    "[Cabinet8_DU5",
    "[Cabinet8_DU6",
    "[Cabinet9_DU1",
    "[Cabinet9_DU2",
    "[Cabinet9_DU3",
    "[Cabinet9_DU4",
    "[Cabinet9_DU5",
    "[Cabinet9_DU6",
    "[Cabinet10_DU1",
    "[Cabinet10_DU2",
    "[Cabinet10_DU3",
    "[Cabinet10_DU4",
    "[Cabinet10_DU5",
    "[Cabinet10_DU6",
    "[Cabinet11_DU1",
    "[Cabinet11_DU2",
    "[Cabinet11_DU3",
    "[Cabinet11_DU4",
    "[Cabinet11_DU5",
    "[Cabinet11_DU6",
    "[Cabinet12_DU1",
    "[Cabinet12_DU2",
    "[Cabinet12_DU3",
    "[Cabinet12_DU4",
    "[Cabinet12_DU5",
    "[Cabinet12_DU6",
};

static const char szTemplateFileSet[][64] = {
    "CHARGE_FUNCTION:Number","CHARGE_FUNCTION",
    "ECO_FUNCTION:Number","ECO_FUNCTION",
    "LVD_FUNCTION:Number","LVD_FUNCTION",
    "RECT_FUNCTION:Number","RECT_FUNCTION",
    "BATT_TEST_FUNCTION:Number","BATT_TEST_FUNCTION"
    "WIZARD_FUNCTION:Number","WIZARD_FUNCTION",
    "TEMPERATURE_FUNCTION:Number","TEMPERATURE_FUNCTION",
    "HYBRID_FUNCTION:Number","HYBRID_FUNCTION",
    "SINGLE_RECT_FUNCTION:Number","SINGLE_RECT_FUNCTION",
	//changed by Frank Wu,6/6/30,20140527, for add single converter and single solar settings pages
	WEB_PAGES_SINGLE_CONVERTER_STR_NUM, WEB_PAGES_SINGLE_CONVERTER_STR,
	WEB_PAGES_SINGLE_SOLAR_STR_NUM, WEB_PAGES_SINGLE_SOLAR_STR,
	WEB_PAGES_SINGLE_RECT_S1_STR_NUM, WEB_PAGES_SINGLE_RECT_S1_STR,
	WEB_PAGES_SINGLE_RECT_S2_STR_NUM, WEB_PAGES_SINGLE_RECT_S2_STR,
	WEB_PAGES_SINGLE_RECT_S3_STR_NUM, WEB_PAGES_SINGLE_RECT_S3_STR,
    "CONVERTER_FUNCTION:Number","CONVERTER_FUNCTION",
    "EIB_FUNCTION:Number","EIB_FUNCTION",
    "SMDU_FUNCTION:Number","SMDU_FUNCTION",
    "LVD_GROUP_FUNCTION:Number","LVD_GROUP_FUNCTION"
	,"CONFIG1_FUNCTION:Number","CONFIG1_FUNCTION"
	,"CONFIG2_FUNCTION:Number","CONFIG2_FUNCTION"
	,"CONFIG3_FUNCTION:Number","CONFIG3_FUNCTION"
	//changed by Frank Wu,8/8/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
	, WEB_PAGES_SOLAR_SET_STR_NUM, WEB_PAGES_SOLAR_SET_STR
	, WEB_PAGES_SHUNT_SET_STR_NUM, WEB_PAGES_SHUNT_SET_STR
	//changed by Frank Wu,6/6/32,20140312, for adding new web pages to the tab of "power system"
	, WEB_PAGES_SYSTEM_GROUP_STR_NUM, WEB_PAGES_SYSTEM_GROUP_STR
	, WEB_PAGES_AC_GROUP_STR_NUM, WEB_PAGES_AC_GROUP_STR
	, WEB_PAGES_AC_UNIT_STR_NUM, WEB_PAGES_AC_UNIT_STR
	, WEB_PAGES_ACMETER_GROUP_STR_NUM, WEB_PAGES_ACMETER_GROUP_STR
	, WEB_PAGES_ACMETER_UNIT_STR_NUM, WEB_PAGES_ACMETER_UNIT_STR
	, WEB_PAGES_DC_UNIT_STR_NUM, WEB_PAGES_DC_UNIT_STR
	, WEB_PAGES_DCMETER_GROUP_STR_NUM, WEB_PAGES_DCMETER_GROUP_STR
	, WEB_PAGES_DCMETER_UNIT_STR_NUM, WEB_PAGES_DCMETER_UNIT_STR
	, WEB_PAGES_DIESEL_GROUP_STR_NUM, WEB_PAGES_DIESEL_GROUP_STR
	, WEB_PAGES_DIESEL_UNIT_STR_NUM, WEB_PAGES_DIESEL_UNIT_STR
	, WEB_PAGES_FUEL_GROUP_STR_NUM, WEB_PAGES_FUEL_GROUP_STR
	, WEB_PAGES_FUEL_UNIT_STR_NUM, WEB_PAGES_FUEL_UNIT_STR
	, WEB_PAGES_IB_GROUP_STR_NUM, WEB_PAGES_IB_GROUP_STR
	, WEB_PAGES_IB_UNIT_STR_NUM, WEB_PAGES_IB_UNIT_STR
	, WEB_PAGES_EIB_GROUP_STR_NUM, WEB_PAGES_EIB_GROUP_STR
	, WEB_PAGES_OBAC_UNIT_STR_NUM, WEB_PAGES_OBAC_UNIT_STR
	, WEB_PAGES_OBLVD_UNIT_STR_NUM, WEB_PAGES_OBLVD_UNIT_STR
	, WEB_PAGES_OBFUEL_UNIT_STR_NUM, WEB_PAGES_OBFUEL_UNIT_STR
	, WEB_PAGES_SMDU_GROUP_STR_NUM, WEB_PAGES_SMDU_GROUP_STR
	, WEB_PAGES_SMDUP_GROUP_STR_NUM, WEB_PAGES_SMDUP_GROUP_STR
	, WEB_PAGES_SMDUP_UNIT_STR_NUM, WEB_PAGES_SMDUP_UNIT_STR
	, WEB_PAGES_SMDUH_GROUP_STR_NUM, WEB_PAGES_SMDUH_GROUP_STR
	, WEB_PAGES_SMDUH_UNIT_STR_NUM, WEB_PAGES_SMDUH_UNIT_STR
	, WEB_PAGES_SMBRC_GROUP_STR_NUM, WEB_PAGES_SMBRC_GROUP_STR
	, WEB_PAGES_SMBRC_UNIT_STR_NUM, WEB_PAGES_SMBRC_UNIT_STR
	, WEB_PAGES_SMIO_GROUP_STR_NUM, WEB_PAGES_SMIO_GROUP_STR
	, WEB_PAGES_SMIO_UNIT_STR_NUM, WEB_PAGES_SMIO_UNIT_STR
	, WEB_PAGES_SMTEMP_GROUP_STR_NUM, WEB_PAGES_SMTEMP_GROUP_STR
	, WEB_PAGES_SMTEMP_UNIT_STR_NUM, WEB_PAGES_SMTEMP_UNIT_STR
	, WEB_PAGES_SMAC_UNIT_STR_NUM, WEB_PAGES_SMAC_UNIT_STR
	, WEB_PAGES_SMLVD_UNIT_STR_NUM, WEB_PAGES_SMLVD_UNIT_STR
	, WEB_PAGES_LVD3_UNIT_STR_NUM, WEB_PAGES_LVD3_UNIT_STR
	, WEB_PAGES_OBBATTFUSE_UNIT_STR_NUM, WEB_PAGES_OBBATTFUSE_UNIT_STR
//changed by Frank Wu,17/N/35,20140527, for adding the the web setting tab page 'DI'
	, WEB_PAGES_DI_BASIC_STR_NUM, WEB_PAGES_DI_BASIC_STR
	, WEB_PAGES_DI_STATE_STR_NUM, WEB_PAGES_DI_STATE_STR,
	"COMM_BATT_FUNCTION:Number","COMM_BATT_FUNCTION",
	"EIB_BATT_FUNCTION:Number","EIB_BATT_FUNCTION",
	"SMDU_BATT_FUNCTION:Number","SMDU_BATT_FUNCTION",
	"SM_BATT_FUNCTION:Number","SM_BATT_FUNCTION",
	"SMBRC_BATT_FUNCTION:Number","SMBRC_BATT_FUNCTION",

	WEB_PAGES_DO_BASIC_STR_NUM, WEB_PAGES_DO_BASIC_STR,
	WEB_PAGES_DO_RELAY_BASIC_STR_NUM, WEB_PAGES_DO_RELAY_BASIC_STR,
	WEB_PAGES_SHUNT_BASIC_STR_NUM, WEB_PAGES_SHUNT_BASIC_STR,
	WEB_PAGES_SHUNT_SMDUP_BASIC_STR_NUM, WEB_PAGES_SHUNT_SMDUP_BASIC_STR,
	WEB_PAGES_SHUNT_EIB_BASIC_STR_NUM, WEB_PAGES_SHUNT_EIB_BASIC_STR,
	WEB_PAGES_SHUNT_DCD_BASIC_STR_NUM, WEB_PAGES_SHUNT_DCD_BASIC_STR,
	WEB_PAGES_SHUNT_BAT_BASIC_STR_NUM, WEB_PAGES_SHUNT_BAT_BASIC_STR,
//changed by Stone Song ,20160525, for adding the the web setting tab page 'FUSE'
	WEB_PAGES_OBBATT_FUSE_STR_NUM,	WEB_PAGES_OBBATT_FUSE_STR_NUM,
	WEB_PAGES_SMDUBATT_FUSE_STR_NUM, WEB_PAGES_SMDUBATT_FUSE_STR,
	WEB_PAGES_OBDC_FUSE_STR_NUM, WEB_PAGES_OBDC_FUSE_STR,
	WEB_PAGES_SMDUDC_FUSE_STR_NUM, WEB_PAGES_SMDUDC_FUSE_STR,
	WEB_PAGES_SMDUPLUS_FUSE_STR_NUM, WEB_PAGES_SMDUPLUS_FUSE_STR,
	WEB_PAGES_RELAY_OPTIONS_STR_NUM, WEB_PAGES_RELAY_OPTIONS_STR,

};


void Lock_g_hMutexLoadCurrent(int iSelect)
{
    if(Mutex_Lock(g_hMutexLoadCurrent, MAX_TIME_WAITING) == ERR_MUTEX_OK)
    {
	switch(iSelect)
	{
	case MAKE_LOAD_DATA_FILE:
	    {
		TRACE_WEB_USER_NOT_CYCLE("Need to update load current json file\n");
		Web_MakeLoadDataFile();
		break;
	    }
	case INIT_LOAD_DATA:
	    {
		TRACE_WEB_USER_NOT_CYCLE("Need to init load current\n");
		Web_MakeHisDataInit();
		break;
	    }
	default:
	    {
		break;
	    }
	}
	Mutex_Unlock(g_hMutexLoadCurrent);
    }
    else
    {
	TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexLoadCurrent in Lock_g_hMutexLoadCurrent\n");
	AppLogOut("Get Lock", APP_LOG_WARNING, "Can't get the lock of g_hMutexLoadCurrent in Lock_g_hMutexLoadCurrent!\n");
    }
}

float CalRectAllPower(void)
{
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int iError;
    SIG_BASIC_VALUE* pSigValue;
    float ftemp = 0;

#define ID_SIGNAL_RECT_POWER	38
//#define ID_SIGNAL_SLAVE_RECT_POWER    55

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_RECT_POWER);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	2,			// rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_RECT_POWER);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	639,				//S1 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_RECT_POWER);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	640,				//S2 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_RECT_POWER);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	641,				//S3 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    ftemp = ftemp / 1000;

    return(ftemp);
}

float CalRectAllCurrent(void)
{
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int iError;
    SIG_BASIC_VALUE* pSigValue;
    float ftemp = 0;

#define ID_SIGNAL_RECT_CURRENT	1
#define ID_SIGNAL_SLAVE_RECT_CURRENT    3

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_RECT_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	2,			// rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_SLAVE_RECT_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	639,				//S1 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_SLAVE_RECT_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	640,				//S2 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_SLAVE_RECT_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	641,				//S3 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    return(ftemp);
}

float CalSourceRateCurr()
{
	int	iVarSubID = 0;
	int	iBufLen = 0;
	int iTimeOut = 0;
	int iError;
	SIG_BASIC_VALUE* pSigValue;
	float fTotalSourceRatedCurr = 0;
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 242);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			1,			// system
			iVarSubID,
			&iBufLen,
			(void *)&pSigValue,
			iTimeOut);
	if(iError == ERR_DXI_OK)
	{
		fTotalSourceRatedCurr = pSigValue->varValue.fValue;
	}
	else
	{
		fTotalSourceRatedCurr = 0;
	}

	return fTotalSourceRatedCurr;
}

float CalSysPower(void)
{
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int iError;
    SIG_BASIC_VALUE* pSigValue;
    float ftemp = 0;

#define ID_SIGNAL_ALL_RECT_RATE_CURRENT	31
#define ID_SIGNAL_ALL_MPPT_RATE_CURRENT	34
#define ID_SIGNAL_ALL_CONVERTER_RATE_CURRENT		37
#define ID_SIGNAL_ALL_SRECT_RATE_CURRENT	53
#define ID_SIGNAL_CONVERTER_TYPE	38

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_ALL_RECT_RATE_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	2,			// rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_ALL_MPPT_RATE_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	1350,				// solar converter group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_ALL_SRECT_RATE_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	639,				//S1 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_ALL_SRECT_RATE_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	640,				//S2 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_ALL_SRECT_RATE_CURRENT);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	641,				//S3 rectifier group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	ftemp += pSigValue->varValue.fValue;
    }
    else
    {
	ftemp += 0;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_CONVERTER_TYPE);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	210,				//converter group
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	if(pSigValue->varValue.enumValue == 2)	// 400V converter
	{
	    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_SIGNAL_ALL_CONVERTER_RATE_CURRENT);
	    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		210,				//converter group
		iVarSubID,
		&iBufLen,
		(void *)&pSigValue,
		iTimeOut);
	    if(iError == ERR_DXI_OK)
	    {
		ftemp = pSigValue->varValue.fValue;
	    }
	    else
	    {
		ftemp = 0;
	    }
	}
    }

    ftemp = ftemp * 1.075;
	// add source rated current 
	ftemp += CalSourceRateCurr();
    return(ftemp);
}

static int Web_TransferFormatToInt(IN char *szFormat)
{
    //ASSERT(szFormat);
    int		iValue1 = 0,iValue2 = 0, iNum;
    if(szFormat != NULL)
    {

	iNum = sscanf(szFormat,"%d.%d", &iValue1, &iValue2);
	if(iNum == 0)
	{
	    iNum = sscanf(szFormat,".%d", &iValue2);
	}
	return iValue2;
    }
    else
    {
	return 0;
    }
}

static BOOL Web_CheckValid(IN const char *pField)
{
    return (pField[0] && !strstr(pField, "N/A")) ? TRUE:FALSE;
}


/*==========================================================================*
* FUNCTION :  bubble_sort
* PURPOSE  :  Sort ascending.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  pLoadCurrentData: The address of the elements need to sort.
n: The elements number.
* RETURN   :  none
* COMMENTS :  This function comes from web resource.
* CREATOR  : Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void bubble_sort(IN HIS_DATA_RECORD_LOAD *pLoadCurrentData, IN int n)
{
    int i, j, iflag;
    HIS_DATA_RECORD_LOAD stTemp;
    for(i = 0; i < (n - 1); i++)
    {
	iflag = 1;
	for(j = 0; j < (n - i - 1); j++)
	{
	    if((pLoadCurrentData + j)->tmSignalTime > (pLoadCurrentData + j + 1)->tmSignalTime)
	    {
		memcpy(&stTemp, (pLoadCurrentData + j), sizeof(stTemp));
		memcpy((pLoadCurrentData + j), (pLoadCurrentData + j + 1), sizeof(stTemp));
		memcpy((pLoadCurrentData + j + 1), (&stTemp), sizeof(stTemp));
		iflag = 0;
	    }
	}
	if(iflag == 1)
	{
	    break;
	}
    }
    return;
}

/*==========================================================================*
* FUNCTION :  Web_GetModuleNumber
* PURPOSE  :  Get the rectifier number.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  unsigned int: The rectifier number.
* COMMENTS :  This function comes from <communicate.c>.
* CREATOR  : Unknown               DATE: 2013-07-20
*==========================================================================*/
static unsigned int Web_GetModuleNumber(int iEquipID, int iSignalID)
{
    //Get Rectifier number
    SIG_BASIC_VALUE* pSigValue;
    int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, iSignalID);
    int		iError = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipID,	//Rectifier group ID		
	    iVarSubID,		
	    &iBufLen,			
	    (void *)&pSigValue,			
	    iTimeOut);

	if (iError == ERR_DXI_OK)
	{
	    return pSigValue->varValue.ulValue;
	}
	else
	{
	    return 0;
	}
}


/*==========================================================================*
* FUNCTION :  Web_MakeHisLoadData
* PURPOSE  :  Make history load current data string.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  char *: The address of the string including the history load current data.
* COMMENTS :  
* CREATOR  : Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
char *Web_MakeHisLoadData(void)
{
    char	*pMakeFileBuf = NULL;
    int i, iLength, iFileLen;
    char szTime[32];
    HIS_DATA_RECORD_LOAD stTempMaxValue;
    float fTempAverageValue = 0;
    int		iBufLength;

    iLength = 0;
    iFileLen = 0;
    // get the valid length of the data and calculate the max value and average value
    stTempMaxValue.floadCurrent = 0;
    stTempMaxValue.tmSignalTime = 0;
    for(i = 0; i < ONE_MONTH_HOUR; i++)
    {
	if(g_stLoadCurrentData[i].bflag == TRUE)
	{
	    iLength++;
	    fTempAverageValue += g_stLoadCurrentData[i].floadCurrent;
	    if((g_stLoadCurrentData[i].floadCurrent > stTempMaxValue.floadCurrent) || 
		(FLOAT_EQUAL(g_stLoadCurrentData[i].floadCurrent, stTempMaxValue.floadCurrent)))
	    {
		stTempMaxValue.floadCurrent = g_stLoadCurrentData[i].floadCurrent;
		stTempMaxValue.tmSignalTime = g_stLoadCurrentData[i].tmSignalTime;
	    }
	}
    }
    if(iLength != 0)
    {
	gs_floadCurrentAverage = fTempAverageValue / iLength;
    }
	gs_stLoadDataMax.floadCurrent = stTempMaxValue.floadCurrent;
    gs_stLoadDataMax.tmSignalTime = stTempMaxValue.tmSignalTime;

    if(iLength == 0)
    {
	return NULL;
    }
    pMakeFileBuf = NEW(char, iLength * MAX_HIS_DATA_LEN + LENGTH_OF_OTHER_CHAR);// create pMakeFileBuf
    if(pMakeFileBuf == NULL)
    {
	AppLogOut("Web_MakeHisLoadData", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return NULL;
    }
    iBufLength = iLength * MAX_HIS_DATA_LEN + LENGTH_OF_OTHER_CHAR;
    memset(pMakeFileBuf, 0, iBufLength);
    //Attention:You can't use "sizeof(pMakeFileBuf)" to calculate the length of pMakeFileBuf
    //memset(pMakeFileBuf, 0, sizeof(pMakeFileBuf));
    iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[");
    for(i = 0; i < ONE_MONTH_HOUR; i++)
    {
	if(g_stLoadCurrentData[i].bflag == TRUE)
	{
	    TimeToString(g_stLoadCurrentData[i].tmSignalTime, TIME_HISDATA_FMT, 
		szTime, sizeof(szTime));
	    if(g_stLoadCurrentData[i].floadCurrent < 0)
	    {
		g_stLoadCurrentData[i].floadCurrent = 0;
	    }
	    iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
		szTime, g_stLoadCurrentData[i].floadCurrent);
	}
    }
    if(iFileLen >= 1)
    {
	iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
    }
    iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "]");
    //TRACE_WEB_USER("[Web_MakeHisLoadData] pMakeFileBuf is %s\n", pMakeFileBuf);

    return pMakeFileBuf;
}

/*==========================================================================*
* FUNCTION :  Web_GetRectSNIndex
* PURPOSE  :  
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: 
* COMMENTS :  This function comes from <communicate.c>.
* CREATOR  :  Unknown               DATE: 2013-07-20
*==========================================================================*/
static int Web_GetRectSNIndex(void)
{
    SAMPLE_SIG_INFO		*pSampleSigInfo;
    int	iBufLen = 0;

    int	iError = DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
	RECT_STD_EQUIP_ID,			
	SIG_ID_RECT_SN,		
	&iBufLen,			
	(void *)&pSampleSigInfo,			
	0);
    if (iError == ERR_DXI_OK)
    {
	return (pSampleSigInfo->iValueDisplayID - 1);
    }

    return -1;
}

/*==========================================================================*
* FUNCTION :  Web_GetRectHighSNIndex
* PURPOSE  :  
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: 
* COMMENTS :  This function comes from <communicate.c>.
* CREATOR  :  Unknown               DATE: 2013-07-20
*==========================================================================*/
static int Web_GetRectHighSNIndex(void)
{
    SAMPLE_SIG_INFO		*pSampleSigInfo;
    int	iBufLen = 0;

    int	iError = DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
	RECT_STD_EQUIP_ID,			
	SIG_ID_RECT_HI_SN,		
	&iBufLen,			
	(void *)&pSampleSigInfo,			
	0);
    if (iError == ERR_DXI_OK)
    {
	return (pSampleSigInfo->iValueDisplayID - 1);
    }

    return -1;
}

/*==========================================================================*
* FUNCTION :  Web_GetRealTimeAlarmData
* PURPOSE  :   
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	OUT ALARM_SIG_VALUE **ppSigValue:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
*==========================================================================*/
static int Web_GetRealTimeAlarmData(OUT ALARM_SIG_VALUE **ppSigValue)
{
    int					iError = 0;
    ALARM_SIG_VALUE			*stAlarmSigValue = NULL;
    int					iSignalNum = 0;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int	iRAlarmSigNumRet = 0;

    iError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
	ALARM_LEVEL_NONE,			
	iVarSubID,		
	&iBufLen,			
	&iRAlarmSigNumRet,			
	iTimeOut);

    if(iRAlarmSigNumRet > 0)
    {
	stAlarmSigValue = NEW(ALARM_SIG_VALUE, iRAlarmSigNumRet);
	if(stAlarmSigValue == NULL)
	{
	    AppLogOut("Web_GetRealTimeAlarmData", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    return FALSE;
	}
	memset(stAlarmSigValue, 0x0, sizeof(ALARM_SIG_VALUE) * iRAlarmSigNumRet);
    }
    else
    {
	return FALSE;
    }


    iBufLen = sizeof(ALARM_SIG_VALUE) * iRAlarmSigNumRet;//Should assign size

    iError += DxiGetData(VAR_ACTIVE_ALARM_INFO,
	ALARM_LEVEL_NONE,			
	0,		
	&iBufLen,			
	stAlarmSigValue,			
	0);

    iSignalNum = iBufLen / sizeof(ALARM_SIG_VALUE);


    if(iError == ERR_DXI_OK)
    {
	if(stAlarmSigValue != NULL)
	{
	    *ppSigValue = (ALARM_SIG_VALUE *)stAlarmSigValue;
	}

	return iSignalNum;

    }
    else
    {
		if(stAlarmSigValue != NULL)
		{
			DELETE(stAlarmSigValue);
			stAlarmSigValue = NULL;
		}
	return FALSE;
    }
}

/*==========================================================================*
* FUNCTION :  Web_GetEquipListFromInterface
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  OUT EQUIP_INFO *pEquipInfo
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:15
*==========================================================================*/
static int Web_GetEquipListFromInterface(OUT EQUIP_INFO **ppEquipInfo)
{
    int				iError = 0;
    EQUIP_INFO		*pEquipInfo = NULL;
    int				iSBufLen = 0;
    int				iLen = 0;

    iError = DxiGetData(VAR_ACU_EQUIPS_LIST,
	0,			
	0,		
	&iSBufLen,			
	&pEquipInfo,			
	0);

    iLen = iSBufLen/sizeof(EQUIP_INFO);

    if (iError == ERR_DXI_OK   )
    {
	*ppEquipInfo = pEquipInfo;

	return iLen;
    }
    else
    {
	return FALSE;
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeEquipListBufferA
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iLanguage:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:15
* MODIFY   : Zhao Zicheng              DATE: 2013-07-20	    To get the English and local language equip name list.
*==========================================================================*/
static int Web_MakeEquipListBufferA(OUT char **ppBuf)
{
    int				iLen = 0;//, iEquipNum = 0;
    char			*pMakeBuf = NULL;
    EQUIP_INFO		*pEquipInfo = NULL;
    int				i;
    int				iEquipNumber = 0;
    int		iBufLength;

    /*get equip information*/
    if((iEquipNumber = Web_GetEquipListFromInterface(&pEquipInfo)) <= 0)
    {
	return FALSE;
    }

    pMakeBuf = NEW(char, iEquipNumber *  100);
    if(pMakeBuf == NULL)
    {
	AppLogOut("Web_MakeEquipListBufferA", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return FALSE;
    }
    iBufLength = iEquipNumber *  100;

    iLen += snprintf(pMakeBuf + iLen, (iBufLength - iLen), "[");
    for ( i = 0; i < iEquipNumber && pEquipInfo != NULL; i++, pEquipInfo++)
    {
	if(pEquipInfo->bWorkStatus == TRUE)
	{
	    iLen += snprintf(pMakeBuf + iLen, (iBufLength - iLen), "[\"%s\",\"%s\",%d],",
		pEquipInfo->pEquipName->pFullName[0],
		pEquipInfo->pEquipName->pFullName[1],
		pEquipInfo->iEquipID);
	}
    }
    if(iLen >= 1)
    {
	iLen = iLen - 1;	//get rid of the last "," to meet json format
    }
    iLen += snprintf(pMakeBuf + iLen, (iBufLength - iLen), "]");

    *ppBuf = pMakeBuf;
    return TRUE;
}

/*==========================================================================*
* FUNCTION :  Web_MakeHislogDataFile
* PURPOSE  :  To make the json file <data.history_log.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeHislogDataFile(void)
{
    TRACE_WEB_USER("Begin to Web_MakeHislogDataFile\n");
#define ID_DEVICE_NAME		"ID_DEVICE_NAME"

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeHislogDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_HISLOG);*/
    iReturn = LoadHtmlFile(WEB_DATA_HISLOG, &pHtml);
    if(iReturn > 0)
    {
	Web_MakeEquipListBufferA(&pbuf);	//To get the English and local language equip name list.
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DEVICE_NAME), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DEVICE_NAME), "[]");
	}
	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_HISLOG_VAR);*/
	if((fp = fopen(WEB_DATA_HISLOG_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    /* if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeLoadDataFile
* PURPOSE  :  To make the json file <data.index_trend.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeLoadDataFile(void)
{
    TRACE_WEB_USER("Begin to make Load Data File\n");
#define ID_TOTAL_CURRENT		"ID_TOTAL_CURRENT"
#define ID_LOAD_TREND			"ID_LOAD_TREND"
#define ID_LOAD_MAX			"ID_LOAD_MAX"
#define ID_LOAD_AVERAGE			"ID_LOAD_AVERAGE"

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    char	szTime[32];
    char	szString[64];
   /* SIG_BASIC_VALUE* pSigValue;*/
    /*int iError;*/
    char szBuf[16];
    /*int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;*/
    float ftemp = 0;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeLoadDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_LOAD_TREND);*/
    iReturn = LoadHtmlFile(WEB_DATA_LOAD_TREND, &pHtml);
    if(iReturn > 0)
    {

	ftemp = CalSysPower();
	snprintf(szBuf, sizeof(szBuf), "%.1f", ftemp);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_TOTAL_CURRENT), szBuf);
	

	//if(Mutex_Lock(g_hMutexLoadCurrent, MAX_TIME_WAITING) == ERR_MUTEX_OK)
	//{
	    pbuf = Web_MakeHisLoadData();
	    //Mutex_Unlock(g_hMutexLoadCurrent);
	//}
	/*else
	{
	    TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexLoadCurrent in Web_MakeLoadDataFile\n");
	    AppLogOut("Web_MakeLoadDataFile", APP_LOG_WARNING, "Can't get the lock of g_hMutexLoadCurrent!\n");
	}*/
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LOAD_TREND), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LOAD_TREND), "[]");
	}

	if(gs_stLoadDataMax.tmSignalTime == 0)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LOAD_MAX), "[]");
	}
	else
	{
	    TimeToString(gs_stLoadDataMax.tmSignalTime, TIME_HISDATA_FMT, 
		szTime, sizeof(szTime));
	    snprintf(szString, sizeof(szString), "[%.1f,\"%-32s\"]", gs_stLoadDataMax.floadCurrent, szTime);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LOAD_MAX), szString);
	    memset(szString, 0, sizeof(szString));
	}
	snprintf(szString, sizeof(szString), "%.1f", gs_floadCurrentAverage);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LOAD_AVERAGE), szString);

	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_LOAD_TREND_VAR);*/
	if((fp = fopen(WEB_DATA_LOAD_TREND_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to make Load Data File\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeCurveData
* PURPOSE  :  To make the Hybrid sources data string
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  iEquipID: indicate which kind of Hybrid sources
* RETURN   :  pMakeFileBuf: the pointer pointing to the string including the Hybrid sources data.
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
char *Web_MakeCurveData(int iEquipID)
{
    char	*pMakeFileBuf = NULL;
    int i, iLength, iFileLen;
    char	szTime[32];
    int		iBufLength;

    iLength = 0;
    iFileLen = 0;

	float fTempValue;
    TRACE_WEB_USER("Begin to make Web_MakeCurveData\n");

    if(iEquipID == EQUIP_AC_GROUP)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stACCurrentData[i].bflag == TRUE)
	    {
		iLength++;
	    }
	}
    }
    else if(iEquipID == EQUIP_DG_GROUP)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stDGCurrentData[i].bflag == TRUE)
	    {
		iLength++;
	    }
	}
    }
    else if(iEquipID == EQUIP_SOLAR_GROUP)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stSolarCurrentData[i].bflag == TRUE)
	    {
		iLength++;
	    }
	}
    }
    else if(iEquipID == EQUIP_SYSTEM)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stAmbientTempData[i].bflag == TRUE)
	    {
		iLength++;
	    }
	}
    }
    else if(iEquipID == EQUIP_BATT_GROUP)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stCompTempData[i].bflag == TRUE)
	    {
		iLength++;
	    }
	}
    }
    else if(iEquipID == EQUIP_BATTERY1)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stBatt1CapData[i].bflag == TRUE)
	    {
		iLength++;
	    }
	}
    }
    else if(iEquipID == EQUIP_BATTERY2)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stBatt2CapData[i].bflag == TRUE)
	    {
		iLength++;
	    }
	}
    }
    else
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stWindCurrentData[i].bflag == TRUE)
	    {
		iLength++;
	    }
	}
    }

    if(iLength == 0)
    {
	return NULL;
    }
    pMakeFileBuf = NEW(char, iLength * MAX_HIS_DATA_LEN + LENGTH_OF_OTHER_CHAR);// create pMakeFileBuf
    if(pMakeFileBuf == NULL)
    {
	AppLogOut("Web_MakeCurveData", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return NULL;
    }
    iBufLength = iLength * MAX_HIS_DATA_LEN + LENGTH_OF_OTHER_CHAR;
    memset(pMakeFileBuf, 0, iBufLength);
    iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[");
    if(iEquipID == EQUIP_AC_GROUP)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stACCurrentData[i].bflag == TRUE)
	    {
		TimeToString(gs_stACCurrentData[i].tmSignalTime, TIME_HISDATA_FMT, 
		    szTime, sizeof(szTime));
		iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
		    szTime, gs_stACCurrentData[i].floadCurrent);
	    }
	}
    }
    else if(iEquipID == EQUIP_DG_GROUP)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stDGCurrentData[i].bflag == TRUE)
	    {
		TimeToString(gs_stDGCurrentData[i].tmSignalTime, TIME_HISDATA_FMT, 
		    szTime, sizeof(szTime));
		iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
		    szTime, gs_stDGCurrentData[i].floadCurrent);
	    }
	}
    }
    else if(iEquipID == EQUIP_SOLAR_GROUP)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stSolarCurrentData[i].bflag == TRUE)
	    {
		TimeToString(gs_stSolarCurrentData[i].tmSignalTime, TIME_HISDATA_FMT, 
		    szTime, sizeof(szTime));
		iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
		    szTime, gs_stSolarCurrentData[i].floadCurrent);
	    }
	}
    }
    else if(iEquipID == EQUIP_SYSTEM)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stAmbientTempData[i].bflag == TRUE)
	    {
			TimeToString(gs_stAmbientTempData[i].tmSignalTime, TIME_HISDATA_FMT, 
				szTime, sizeof(szTime));
			DXI_GetTempSwitch(TEMP_VALUE,
				gs_stAmbientTempData[i].floadCurrent,
				&fTempValue,
				NULL);
			iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
				szTime, fTempValue);
	    }
	}
    }
    else if(iEquipID == EQUIP_BATT_GROUP)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stCompTempData[i].bflag == TRUE)
	    {
			TimeToString(gs_stCompTempData[i].tmSignalTime, TIME_HISDATA_FMT, 
				szTime, sizeof(szTime));
			DXI_GetTempSwitch(TEMP_VALUE,
				gs_stCompTempData[i].floadCurrent,
				&fTempValue,
				NULL);
			iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
				szTime, fTempValue);
	    }
	}
    }
    else if(iEquipID == EQUIP_BATTERY1)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stBatt1CapData[i].bflag == TRUE)
	    {
		TimeToString(gs_stBatt1CapData[i].tmSignalTime, TIME_HISDATA_FMT, 
		    szTime, sizeof(szTime));
		iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
		    szTime, gs_stBatt1CapData[i].floadCurrent);
	    }
	}
    }
    else if(iEquipID == EQUIP_BATTERY2)
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stBatt2CapData[i].bflag == TRUE)
	    {
		TimeToString(gs_stBatt2CapData[i].tmSignalTime, TIME_HISDATA_FMT, 
		    szTime, sizeof(szTime));
		iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
		    szTime, gs_stBatt2CapData[i].floadCurrent);
	    }
	}
    }
    else
    {
	for(i = 0; i < FOUR_WEEK_HOUR; i++)
	{
	    if(gs_stWindCurrentData[i].bflag == TRUE)
	    {
		TimeToString(gs_stWindCurrentData[i].tmSignalTime, TIME_HISDATA_FMT, 
		    szTime, sizeof(szTime));
		iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "[\"%-32s\",%.1f],", 
		    szTime, gs_stWindCurrentData[i].floadCurrent);
	    }
	}
    }
    if(iFileLen >= 1)
    {
	iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
    }
    iFileLen += snprintf(pMakeFileBuf + iFileLen, (iBufLength - iFileLen), "]");

    //TRACE_WEB_USER("[Web_MakeCurveData] pMakeFileBuf is %s\n", pMakeFileBuf);
    TRACE_WEB_USER("end to make Web_MakeCurveData\n");
    return pMakeFileBuf;
}

/*==========================================================================*
* FUNCTION :  Web_MakeCurveDataFile
* PURPOSE  :  To make the json file <data.hybrid.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeCurveDataFile(void)
{
    TRACE_WEB_USER("Begin to make Hybrid Data File\n");
#define ID_AC_TREND			"ID_AC_TREND"
#define ID_SOLAR_TREND			"ID_SOLAR_TREND"
#define ID_DG_TREND			"ID_DG_TREND"
#define ID_WIND_TREND			"ID_WIND_TREND"
#define ID_AMBIENT_TEMP_TREND		"ID_AMBIENT_TEMP_TREND"
#define ID_COMP_TEMP_TREND			"ID_COMP_TEMP_TREND"
#define ID_BATT1_CAPA_TREND			"ID_BATT1_CAPA_TREND"
#define ID_BATT2_CAPA_TREND			"ID_BATT2_CAPA_TREND"
//#define ID_TEMP_L1			"ID_TEMP_L1"
//#define ID_TEMP_H1			"ID_TEMP_H1"
//#define ID_TEMP_H2			"ID_TEMP_H2"
//#define ID_BATT_TEMP_L1			"ID_BATT_TEMP_L1"
//#define ID_BATT_TEMP_H1			"ID_BATT_TEMP_H1"
//#define ID_BATT_TEMP_H2			"ID_BATT_TEMP_H2"

#define ID_TEMP_L1_SIGNAL		210
#define ID_TEMP_H1_SIGNAL		209
#define ID_TEMP_H2_SIGNAL		219
#define ID_BATT_TEMP_L1_SIGNAL		78
#define ID_BATT_TEMP_H1_SIGNAL		77
#define ID_BATT_TEMP_H2_SIGNAL		76
#define ID_SYSTEM_EQUIP			1
#define ID_BATT_GROUP_EQUIP		115

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    //int	iVarSubID = 0;
    //int	iBufLen = 0;
    //int iTimeOut = 0;
    //int	iError = 0;
    //SIG_BASIC_VALUE* pSigValue;
    //char szBuf[16];
    //float ftemp;
    int iCountTemp = 0;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeCurveDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_HYBRID_TREND);*/
    iReturn = LoadHtmlFile(WEB_DATA_HYBRID_TREND, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeCurveData(EQUIP_AC_GROUP);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_AC_TREND), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    iCountTemp++;
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_AC_TREND), "[]");
	}

	pbuf = Web_MakeCurveData(EQUIP_DG_GROUP);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DG_TREND), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    iCountTemp++;
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DG_TREND), "[]");
	}

	pbuf = Web_MakeCurveData(EQUIP_SOLAR_GROUP);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOLAR_TREND), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    iCountTemp++;
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOLAR_TREND), "[]");
	}

	if(iCountTemp == 3)
	{
	    gs_iHybrid = 0;
	}
	else
	{
	    gs_iHybrid = 1;
	}

	pbuf = Web_MakeCurveData(EQUIP_SYSTEM);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_AMBIENT_TEMP_TREND), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_AMBIENT_TEMP_TREND), "[]");
	}

	pbuf = Web_MakeCurveData(EQUIP_BATT_GROUP);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_COMP_TEMP_TREND), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_COMP_TEMP_TREND), "[]");
	}

	pbuf = Web_MakeCurveData(EQUIP_BATTERY1);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT1_CAPA_TREND), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT1_CAPA_TREND), "[]");
	}

	pbuf = Web_MakeCurveData(EQUIP_BATTERY2);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT2_CAPA_TREND), pbuf);
	    DELETE(pbuf);
	    pbuf = NULL;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT2_CAPA_TREND), "[]");
	}

	// No wind controller at present
	/*pbuf = Web_MakeCurveData(EQUIP_WIND_GROUP);
	if(pbuf != NULL)
	{
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_WIND_TREND), pbuf);
	DELETE(pbuf);
	pbuf = NULL;
	}
	else
	{
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_WIND_TREND), "\"---\"");
	}*/
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_WIND_TREND), "[]");

	/*memset(szBuf, 0, sizeof(szBuf));
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ID_TEMP_L1_SIGNAL);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    ID_SYSTEM_EQUIP,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    iTimeOut);
	if(iError == ERR_DXI_OK)
	{
	    sprintf(szBuf, "%.0f", pSigValue->varValue.fValue);
	}
	else
	{
	    ftemp = -10;
	    sprintf(szBuf, "%.0f", ftemp);
	}*/
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_TEMP_L1), "");

	/*memset(szBuf, 0, sizeof(szBuf));
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ID_TEMP_H1_SIGNAL);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    ID_SYSTEM_EQUIP,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    iTimeOut);
	if(iError == ERR_DXI_OK)
	{
	    if(pSigValue->varValue.fValue > 75)
	    {
		ftemp = 75;
		sprintf(szBuf, "%.0f", ftemp);
	    }
	    else
	    {
		sprintf(szBuf, "%.0f", pSigValue->varValue.fValue);
	    }
	}
	else
	{
	    ftemp = 50;
	    sprintf(szBuf, "%.0f", ftemp);
	}*/
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_TEMP_H1), "");

	/*memset(szBuf, 0, sizeof(szBuf));
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ID_TEMP_H2_SIGNAL);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    ID_SYSTEM_EQUIP,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    iTimeOut);
	if(iError == ERR_DXI_OK)
	{
	    if(pSigValue->varValue.fValue > 75)
	    {
		ftemp = 75;
		sprintf(szBuf, "%.0f", ftemp);
	    }
	    else
	    {
		sprintf(szBuf, "%.0f", pSigValue->varValue.fValue);
	    }
	}
	else
	{
	    ftemp = 60;
	    sprintf(szBuf, "%.0f", ftemp);
	}*/
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_TEMP_H2), "");

	/*memset(szBuf, 0, sizeof(szBuf));
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ID_BATT_TEMP_L1_SIGNAL);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    ID_BATT_GROUP_EQUIP,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    iTimeOut);
	if(iError == ERR_DXI_OK)
	{
	    sprintf(szBuf, "%.0f", pSigValue->varValue.fValue);
	}
	else
	{
	    ftemp = -10;
	    sprintf(szBuf, "%.0f", ftemp);
	}*/
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT_TEMP_L1), "");

	/*memset(szBuf, 0, sizeof(szBuf));
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ID_BATT_TEMP_H1_SIGNAL);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    ID_BATT_GROUP_EQUIP,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    iTimeOut);
	if(iError == ERR_DXI_OK)
	{
	    sprintf(szBuf, "%.0f", pSigValue->varValue.fValue);
	}
	else
	{
	    ftemp = 50;
	    sprintf(szBuf, "%.0f", ftemp);
	}*/
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT_TEMP_H1), "");

	/*memset(szBuf, 0, sizeof(szBuf));
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ID_BATT_TEMP_H2_SIGNAL);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    ID_BATT_GROUP_EQUIP,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    iTimeOut);
	if(iError == ERR_DXI_OK)
	{
	    sprintf(szBuf, "%.0f", pSigValue->varValue.fValue);
	}
	else
	{
	    ftemp = 60;
	    sprintf(szBuf, "%.0f", ftemp);
	}*/
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT_TEMP_H2), "");

	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_HYBRID_TREND_VAR);*/
	if((fp = fopen(WEB_DATA_HYBRID_TREND_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to make Hybrid Data File\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeAlarmDataFile
* PURPOSE  :  To make the json file <data.alarms.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  This function comes from the function of "Web_MakeRAlarmDataBuf" in <communicate.c>
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeAlarmDataFile(void)
{
    TRACE_WEB_USER("Begin to make Alarm Data File\n");
#define ID_ALL_ALARM_NUM			"ID_ALL_ALARM_NUM"
#define ID_OBSERVATION_NUM			"ID_OBSERVATION_NUM"
#define ID_MAJOR_NUM				"ID_MAJOR_NUM"
#define ID_CRITICAL_NUM				"ID_CRITICAL_NUM"
#define ID_ALARM_CONTENT			"ID_ALARM_CONTENT"
#define ID_REC_NUM				"ID_REC_NUM"
//#define ID_RECS1_NUM				"ID_RECS1_NUM"
//#define ID_RECS2_NUM				"ID_RECS2_NUM"
//#define ID_RECS3_NUM				"ID_RECS3_NUM"
#define ID_CONVERTER_NUM			"ID_CONVERTER_NUM"
#define ID_SOLAR_NUM				"ID_SOLAR_NUM"
#define ID_SITE_NAME		"ID_SITE_NAME"
    #define ID_SITE_LOCATION		"ID_SITE_LOCATION"
    #define ID_SITE_DESC		"ID_SITE_DESC"
    #define ID_SYS_VOLT		"ID_SYS_VOLT"
    #define ID_SYS_CURR		"ID_SYS_CURR"
#define ID_CON_VOLT		"ID_CON_VOLT"
#define ID_CON_CURR		"ID_CON_CURR"

#define EQUIP_SYSTEM		1
#define SIG_ALL_ALARM		28
#define SIG_OBSERVATION		29
#define SIG_MAJOR		30
#define SIG_CRITICAL		31

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE	*fp;
    char szBuf[32];
    static	int		iRectSNIndex = -1, iRectHighSNIndex = -1;
    int		iSignalNumber = 0;
    ALARM_SIG_VALUE	*pAlarmSigValue = NULL, *pDelete = NULL;
    char	*pMBuf = NULL;
    int		iNum = 0, iLen = 0;
    time_t	tmSample;
    char	szDisBuf[128];
    char	szDisBuf1[128];
    char	szTime[32];
    int		k;
    int		iAllAlarmNum = 0, iOBAlarmNum = 0, iMAAlarmNum = 0, iCAAlarmNum = 0;
    char szBuf1[80];
    char	*szMakeVar = NULL;
    int		iBufLength;



#define ID_ALARM_SOUND	    "ID_ALARM_SOUND"
#define ALARM_SOUND_ACTIVATION	251

    int	iError = 0;
    int	iBufLen = 0;
    int	iVarSubID = 0;
    SIG_BASIC_VALUE*		pSigValue;


    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeAlarmDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return FALSE;
    }
    sprintf(szFile, "%s", WEB_DATA_ALARM);*/
    iReturn = LoadHtmlFile(WEB_DATA_ALARM, &pHtml);
    if(iReturn > 0)
    {
	if(iRectSNIndex == -1)
	{
	    iRectSNIndex = Web_GetRectSNIndex();
	}

	if(iRectHighSNIndex == -1)
	{
	    iRectHighSNIndex = Web_GetRectHighSNIndex();
	}
	iSignalNumber = Web_GetRealTimeAlarmData(&pAlarmSigValue);
	TRACE_WEB_USER("iSignalNumber is %d\n", iSignalNumber);
	if(iSignalNumber > 0)
	{
	    //Alarm sort
	    DXI_MakeAlarmSigValueSort(pAlarmSigValue, iSignalNumber);

	    if(iSignalNumber > 200)
	    {
		iSignalNumber = 200;
	    }
	    pMBuf = NEW(char, MAX_COMM_RETURN_DATA_LINE_SIZE * iSignalNumber);
	    if(pMBuf == NULL )
	    {
		AppLogOut("Web_MakeAlarmDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_CONTENT), "[]");
		return;
	    }
	    iBufLength = MAX_COMM_RETURN_DATA_LINE_SIZE * iSignalNumber;
	    memset(pMBuf, 0x0, (size_t)MAX_COMM_RETURN_DATA_LINE_SIZE * iSignalNumber);
	    RunThread_Heartbeat(RunThread_GetId(NULL));
	    pDelete = pAlarmSigValue;
	    iLen += snprintf(pMBuf + iLen, (iBufLength - iLen), "[");
	    for(iNum =0; iNum < iSignalNumber && pAlarmSigValue != NULL; iNum++, pAlarmSigValue++)
	    {
		if(pAlarmSigValue->pStdSig->iAlarmLevel != ALARM_LEVEL_NONE)
		{
		    tmSample = pAlarmSigValue->sv.tmStartTime;
		    if(pAlarmSigValue->pEquipInfo->iEquipTypeID == 201)
		    {
			for(k = 0; k <pAlarmSigValue->pEquipInfo->pSampleSigValue->pStdSig->iSigID; k++)
			{
			    if(!SIG_VALUE_IS_CONFIGURED(&pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectHighSNIndex]))
			    {
				snprintf(szDisBuf, sizeof(szDisBuf), "%s", 
				    pAlarmSigValue->pEquipInfo->pEquipName->pFullName[0]);
				snprintf(szDisBuf1, sizeof(szDisBuf1), "%s", 
				    pAlarmSigValue->pEquipInfo->pEquipName->pFullName[1]);
			    }
			    else
			    {
				snprintf(szDisBuf, sizeof(szDisBuf), "%s", 
				    pAlarmSigValue->pEquipInfo->pEquipName->pFullName[0]);
				snprintf(szDisBuf1, sizeof(szDisBuf1), "%s", 
				    pAlarmSigValue->pEquipInfo->pEquipName->pFullName[1]);
			    }
			}
		    }
		    else
		    {
			snprintf(szDisBuf, sizeof(szDisBuf),"%s",pAlarmSigValue->pEquipInfo->pEquipName->pFullName[0]);
			snprintf(szDisBuf1, sizeof(szDisBuf1),"%s",pAlarmSigValue->pEquipInfo->pEquipName->pFullName[1]);
		    }
		    TimeToString(tmSample, TIME_HISDATA_FMT, szTime, sizeof(szTime));
		    if(pAlarmSigValue->pStdSig->pHelpInfo == NULL)
		    {
			iLen += snprintf(pMBuf + iLen,(iBufLength - iLen), "[%d,[\"%16s\",\"%16s\"],[\"%s\",\"%s\"],\"%-32s\",%s],",
			    pAlarmSigValue->pStdSig->iAlarmLevel,
			    szDisBuf,
			    szDisBuf1,
			    pAlarmSigValue->pStdSig->pSigName->pFullName[0],
			    pAlarmSigValue->pStdSig->pSigName->pFullName[1],
			    szTime,
			    "\"\"");
		    }
		    else
		    {
			iLen += snprintf(pMBuf + iLen,(iBufLength - iLen), "[%d,[\"%16s\",\"%16s\"],[\"%s\",\"%s\"],\"%-32s\",[\"%-64s\",\"%-64s\"]],",
			    pAlarmSigValue->pStdSig->iAlarmLevel,
			    szDisBuf,
			    szDisBuf1,
			    pAlarmSigValue->pStdSig->pSigName->pFullName[0],
			    pAlarmSigValue->pStdSig->pSigName->pFullName[1],
			    szTime,
			    pAlarmSigValue->pStdSig->pHelpInfo->pFullName[0],
			    pAlarmSigValue->pStdSig->pHelpInfo->pFullName[1]);
		    }
		    if(pAlarmSigValue->pStdSig->iAlarmLevel == ALARM_LEVEL_OBSERVATION)
		    {
			iOBAlarmNum++;
		    }
		    else if(pAlarmSigValue->pStdSig->iAlarmLevel == ALARM_LEVEL_MAJOR)
		    {
			iMAAlarmNum++;
		    }
		    else if(pAlarmSigValue->pStdSig->iAlarmLevel == ALARM_LEVEL_CRITICAL)
		    {
			iCAAlarmNum++;
		    }
		}
	    }
	    if(iLen > 0)
	    {
		iLen--;// get rid of the last "," to meet json format
	    }
	    iLen += snprintf(pMBuf + iLen, (iBufLength - iLen), "]");
	    //TRACE_WEB_USER("[Web_MakeAlarmDataFile] pMBuf is %s\n", pMBuf);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_CONTENT), pMBuf);
	    RunThread_Heartbeat(RunThread_GetId(NULL));
	    if(pDelete != NULL)
	    {
		DELETE(pDelete);
		pDelete = NULL;
	    }
	    if(pMBuf != NULL)
	    {
		DELETE(pMBuf);
		pMBuf = NULL;
	    }
	    iAllAlarmNum = iSignalNumber;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_CONTENT), "[]");
	}
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iAllAlarmNum);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALL_ALARM_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iOBAlarmNum);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_OBSERVATION_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iMAAlarmNum);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_MAJOR_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iCAAlarmNum);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CRITICAL_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_uiAllRectifierNumber);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_REC_NUM), szBuf);
	/*memset(szBuf, 0, sizeof(szBuf));
	sprintf(szBuf, "%d", gs_uiRectifierS1Number);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECS1_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	sprintf(szBuf, "%d", gs_uiRectifierS2Number);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECS2_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	sprintf(szBuf, "%d", gs_uiRectifierS3Number);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECS3_NUM), szBuf);*/
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_uiConverterNumber);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONVERTER_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_uiSolarNumber);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOLAR_NUM), szBuf);

	memset(szBuf1, 0, sizeof(szBuf1));
	snprintf(szBuf1, sizeof(szBuf1), "[\"%s\",\"%s\"]", g_SiteInfo.langSiteName.pFullName[0], g_SiteInfo.langSiteName.pFullName[0]);//here just display English site name
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITE_NAME), szBuf1);
	memset(szBuf1, 0, sizeof(szBuf1));
	snprintf(szBuf1, sizeof(szBuf1), "[\"%s\",\"%s\"]", g_SiteInfo.langSiteLocation.pFullName[0], g_SiteInfo.langSiteLocation.pFullName[1]);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITE_LOCATION), szBuf1);

	memset(szBuf1, 0, sizeof(szBuf1));
	snprintf(szBuf1, sizeof(szBuf1), "[\"%s\",\"%s\"]", g_SiteInfo.langDescription.pFullName[0], g_SiteInfo.langDescription.pFullName[1]);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITE_DESC), szBuf1);

	szMakeVar = MakeVarField(pstSysCurr->szResourceID);
	Web_FillString(&pHtml, szMakeVar, pstSysCurr->pSigValue, pstSysCurr->iEquipID, pstSysCurr->bValid);
	DELETE(szMakeVar);
	szMakeVar = NULL; 

	szMakeVar = MakeVarField(pstSysVolt->szResourceID);
	Web_FillString(&pHtml, szMakeVar, pstSysVolt->pSigValue, pstSysVolt->iEquipID, pstSysVolt->bValid);
	DELETE(szMakeVar);
	szMakeVar = NULL;

	szMakeVar = MakeVarField(pstConCurr->szResourceID);
	Web_FillString(&pHtml, szMakeVar, pstConCurr->pSigValue, pstConCurr->iEquipID, pstConCurr->bValid);
	DELETE(szMakeVar);
	szMakeVar = NULL;

	szMakeVar = MakeVarField(pstConVolt->szResourceID);
	Web_FillString(&pHtml, szMakeVar, pstConVolt->pSigValue, pstConVolt->iEquipID, pstConVolt->bValid);
	DELETE(szMakeVar);
	szMakeVar = NULL;

	//time format signal for alarm json
	szMakeVar = MakeVarField(pstTimeFor->szResourceID);
	Web_FillString(&pHtml, szMakeVar, pstTimeFor->pSigValue, pstTimeFor->iEquipID, pstTimeFor->bValid);
	DELETE(szMakeVar);
	szMakeVar = NULL;



	//GHMI,use for leitong Debug
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ALARM_SOUND_ACTIVATION);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    1,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    0);
	if(iError == ERR_DXI_OK)
	{
	    memset(szBuf, 0, sizeof(szBuf));
	    snprintf(szBuf, sizeof(szBuf), "%ld", pSigValue->varValue.enumValue);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_SOUND), szBuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_SOUND), "0");
	}


	if((fp = fopen(WEB_DATA_ALARM_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {
    }
    /*memset(szFile, 0, sizeof(szFile));
    sprintf(szFile, "%s", WEB_DATA_ALARM_VAR);*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to make Alarm Data File\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_GetSMDUBattFuseEquipNum
* PURPOSE  :  get the number of SMDU Battery Fuse equips
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-30
*==========================================================================*/
void Web_GetSMDUBattFuseEquipNum(void)
{
#define EQUIP_SMDU_BATTERY_FUSE	402


    int		iRst, i;
    static int		iEquipNum = -1;
    EQUIP_INFO	*pEquipList;
    static EQUIP_INFO	*pEquipList1 = NULL;
    int	iBufLen = 0;

    if(iEquipNum == -1)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM,
	    0, 
	    0, 
	    &iBufLen, 
	    &iEquipNum, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }

    if(pEquipList1 == NULL)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
	    0, 
	    0, 
	    &iBufLen, 
	    &pEquipList1, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }

    pEquipList = pEquipList1;

    gs_uiRealSMDUBattFuseNum = 0;

    for(i = 0; i < iEquipNum; i++)
    {
	if(pEquipList->iEquipTypeID == EQUIP_SMDU_BATTERY_FUSE)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSMDUBattFuseNum++;
	    }
	}
	else
	{}
	pEquipList++;
    }
}
//changed by Frank Wu,4/1/4,20140723, for scanning FuelUnitEquipment
void Web_GetFuelUnitEquipNum(void)
{
#define EQUIP_TYPEID_FUEL_UNIT				2101
#define EQUIP_TYPEID_OBFUEL_UNIT			2102


	int		iRst, i;
	static int		iEquipNum = -1;
	EQUIP_INFO	*pEquipList;
	static EQUIP_INFO	*pEquipList1 = NULL;
	int	iBufLen = 0;

	if(iEquipNum == -1)
	{
		iRst = DxiGetData(VAR_ACU_EQUIPS_NUM,
			0, 
			0, 
			&iBufLen, 
			&iEquipNum, 
			0);
		if(iRst != ERR_OK)
		{
			return;
		}
	}

	if(pEquipList1 == NULL)
	{
		iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
			0, 
			0, 
			&iBufLen, 
			&pEquipList1, 
			0);
		if(iRst != ERR_OK)
		{
			return;
		}
	}

	pEquipList = pEquipList1;

	gs_uiRealFuelUnitNum = 0;
	gs_uiRealOBFuelUnitNum = 0;

	for(i = 0; i < iEquipNum; i++)
	{
		if(pEquipList->iEquipTypeID == EQUIP_TYPEID_FUEL_UNIT)
		{
			if(pEquipList->bWorkStatus == TRUE)
			{
				gs_uiRealFuelUnitNum++;
			}
		}
		else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_OBFUEL_UNIT)
		{
			if(pEquipList->bWorkStatus == TRUE)
			{
				gs_uiRealOBFuelUnitNum++;
			}
		}
		else
		{}
		pEquipList++;
	}
}
//changed by Frank Wu,N/35/38,20140730, for adding new web pages to the tab of "power system"
void Web_GetSystemEquipNum(void)
{
#define EQUIP_TYPEID_OBBATTFUSE_UNIT		401
#define EQUIP_TYPEID_SMLVD_UNIT				602
#define EQUIP_TYPEID_NARADA_BMS_GROUP		2900


	int		iRst, i;
	static int		iEquipNum = -1;
	EQUIP_INFO	*pEquipList;
	static EQUIP_INFO	*pEquipList1 = NULL;
	int	iBufLen = 0;

	if(iEquipNum == -1)
	{
		iRst = DxiGetData(VAR_ACU_EQUIPS_NUM,
			0, 
			0, 
			&iBufLen, 
			&iEquipNum, 
			0);
		if(iRst != ERR_OK)
		{
			return;
		}
	}

	if(pEquipList1 == NULL)
	{
		iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
			0, 
			0, 
			&iBufLen, 
			&pEquipList1, 
			0);
		if(iRst != ERR_OK)
		{
			return;
		}
	}

	pEquipList = pEquipList1;

	gs_uiRealOBBattFuseUnitNum = 0;
	gs_uiRealSMLVDUnitNum = 0;
	gs_uiRealNaradaBMSGroupNum = 0;
	
	for(i = 0; i < iEquipNum; i++)
	{
		if(pEquipList->iEquipTypeID == EQUIP_TYPEID_OBBATTFUSE_UNIT)
		{
			if(pEquipList->bWorkStatus == TRUE)
			{
				gs_uiRealOBBattFuseUnitNum++;
			}
		}
		else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SMLVD_UNIT)
		{
			if(pEquipList->bWorkStatus == TRUE)
			{
				gs_uiRealSMLVDUnitNum++;
			}
		}
		else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_NARADA_BMS_GROUP)
		{
			if(pEquipList->bWorkStatus == TRUE)
			{
				gs_uiRealNaradaBMSGroupNum++;
			}
		}
		else
		{}
		pEquipList++;
	}
}

/*==========================================================================*
* FUNCTION :  Web_GetLVDEquipNum
* PURPOSE  :  get the number of 2 kinds of LVD equips
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_GetLVDEquipNum(void)
{
#define EQUIP_LVD1	601
#define EQUIP_LVD2	602
#define EQUIP_LVD3	604


    int		iRst, i;
    static int		iEquipNum = -1;
    EQUIP_INFO	*pEquipList;
    static EQUIP_INFO	*pEquipList1 = NULL;
    int	iBufLen = 0;

    if(iEquipNum == -1)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM,
	    0, 
	    0, 
	    &iBufLen, 
	    &iEquipNum, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }

    if(pEquipList1 == NULL)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
	    0, 
	    0, 
	    &iBufLen, 
	    &pEquipList1, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }

    pEquipList = pEquipList1;

    gs_uiRealLVDNum = 0;
    gs_uiRealLVDUnitNum = 0;
    gs_uiRealSMDULVDUnitNum = 0;
    gs_uiRealLVD3UnitNum = 0;

    for(i = 0; i < iEquipNum; i++)
    {
	if((pEquipList->iEquipTypeID == EQUIP_LVD1) || (pEquipList->iEquipTypeID == EQUIP_LVD2) || (pEquipList->iEquipTypeID == EQUIP_LVD3))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealLVDNum++;
	    }
	}
	else
	{}
	if(pEquipList->iEquipTypeID == EQUIP_LVD1)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealLVDUnitNum++;
	    }
	}
	else
	{}
	if(pEquipList->iEquipTypeID == EQUIP_LVD2)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSMDULVDUnitNum++;
	    }
	}
	else
	{}
	if(pEquipList->iEquipTypeID == EQUIP_LVD3)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealLVD3UnitNum++;
	    }
	}
	else
	{}
	pEquipList++;
    }
}

/*==========================================================================*
* FUNCTION :  Web_GetOtherEquipNum
* PURPOSE  :  get the number of other equips like SMAC, SMIO etc. These equip number won't change after initial.
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
void Web_GetOtherEquipNum(void)
{
#define EQUIP_TYPEID_SMAC	703
#define EQUIP_TYPEID_DG		1501
#define EQUIP_TYPEID_ACMETER	2801
#define EQUIP_TYPEID_DCMETER	2401
#define EQUIP_TYPEID_SMIO_ST	1203
#define EQUIP_TYPEID_SMIO_END	1208
#define EQUIP_TYPEID_NARADA_BMS		2901


    int		iRst, i;
    static int		iEquipNum = -1;
    EQUIP_INFO	*pEquipList;
    static EQUIP_INFO	*pEquipList1 = NULL;
    int	iBufLen = 0;

    if(iEquipNum == -1)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM,
	    0, 
	    0, 
	    &iBufLen, 
	    &iEquipNum, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }

    if(pEquipList1 == NULL)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
	    0, 
	    0, 
	    &iBufLen, 
	    &pEquipList1, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }
    pEquipList = pEquipList1;

    gs_uiSMACNum = 0;
    gs_uiDGNum = 0;
    gs_uiACMeterNum = 0;
    gs_uiDCMeterNum = 0;
    gs_uiNaradaBMSNum = 0;
    gs_uiSMIONum = 0;
    

    for(i = 0; i < iEquipNum; i++)
    {
	if((pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMIO_ST) && (pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMIO_END))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiSMIONum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SMAC)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiSMACNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_DG)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiDGNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_ACMETER)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiACMeterNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_DCMETER)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiDCMeterNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_NARADA_BMS)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiNaradaBMSNum++;
	    }
	}
	else
	{}
	pEquipList++;
    }
}

/*==========================================================================*
* FUNCTION :  Web_GetFixEquipNum
* PURPOSE  :  get the number of all kinds of fix number equips
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng		 DATE: 2013-12-12 Add the EIB number getting
*==========================================================================*/
void Web_GetFixEquipNum(void)
{
#define EQUIP_TYPEID_SMDU_ST	1001
#define EQUIP_TYPEID_SMDU_END	1008
#define EQUIP_TYPEID_SMDUP	1901
#define EQUIP_TYPEID_SMDUP_END	1920
#define EQUIP_TYPEID_SMDUE		3001
#define EQUIP_TYPEID_SMDUE_END	3008
#define EQUIP_TYPEID_SMDUH_ST	2701
#define EQUIP_TYPEID_SMDUH_END	2708
#define EQUIP_TYPEID_EIB	901
#define EQUIP_TYPEID_EIB_ST	901
#define EQUIP_TYPEID_EIB_END	904
#define EQUIP_TYPEID_LVD_GROUP	600
#define EQUIP_TYPEID_BATT_FUSE_GROUP		400
#define EQUIP_TYPEID_BATT_FUSE			401
#define EQUIP_TYPEID_SMDU_BATT_FUSE		402
#define EQUIP_TYPEID_SMDU_BATT_FUSE_ST		404
#define EQUIP_TYPEID_SMDU_BATT_FUSE_END		410

//changed by Frank Wu,7/7/32,20140312, for adding new web pages to the tab of "power system"
#define EQUIP_TYPEID_SYSTEM_GROUP			100
#define EQUIP_TYPEID_AC_GROUP				700
#define EQUIP_TYPEID_AC_UNIT				701
#define EQUIP_TYPEID_ACMETER_GROUP			2800
#define EQUIP_TYPEID_ACMETER_UNIT			2801
#define EQUIP_TYPEID_DC_UNIT				500
#define EQUIP_TYPEID_DCMETER_GROUP			2400
#define EQUIP_TYPEID_DCMETER_UNIT			2401
#define EQUIP_TYPEID_DIESEL_GROUP			1500
#define EQUIP_TYPEID_DIESEL_UNIT			1501
#define EQUIP_TYPEID_FUEL_GROUP				2100
#define EQUIP_TYPEID_FUEL_UNIT				2101
#define EQUIP_TYPEID_IB_GROUP				800
#define EQUIP_TYPEID_IB_UNIT				801
#define EQUIP_TYPEID_IB_UNIT_ST				801
#define EQUIP_TYPEID_IB_UNIT_END			804
#define EQUIP_TYPEID_EIB_GROUP				900
#define EQUIP_TYPEID_OBAC_UNIT				702
#define EQUIP_TYPEID_OBLVD_UNIT				601
#define EQUIP_TYPEID_OBFUEL_UNIT			2102
#define EQUIP_TYPEID_SMDU_GROUP				1000
#define EQUIP_TYPEID_SMDUP_GROUP			1900
#define EQUIP_TYPEID_SMDUE_GROUP			3000
#define EQUIP_TYPEID_SMDUH_GROUP			2700
#define EQUIP_TYPEID_SMBRC_GROUP			2000
#define EQUIP_TYPEID_SMBRC_UNIT				2001
#define EQUIP_TYPEID_SMIO_GROUP				1200
#define EQUIP_TYPEID_SMIO_UNIT_ST			1201
#define EQUIP_TYPEID_SMIO_UNIT_END			1208
#define EQUIP_TYPEID_SMTEMP_GROUP			2200
#define EQUIP_TYPEID_SMTEMP_UNIT_ST			2201
#define EQUIP_TYPEID_SMTEMP_UNIT_END		2208
#define EQUIP_TYPEID_SMAC_UNIT				703
#define EQUIP_TYPEID_SMLVD_UNIT				602
#define EQUIP_TYPEID_LVD3_UNIT				604
#define EQUIP_TYPEID_OBBATTFUSE_UNIT		401
#define EQUIP_TYPEID_NARADA_BMS_GROUP		2900

//changed by Stone Song ,20160606, for adding the the web setting tab page 'FUSE'
#define EQUIP_TYPEID_OBDC_FUSE				501
#define EQUIP_TYPEID_SMDU_OBDC_FUSE			502
#define EQUIP_TYPEID_SMDU_OBDC_FUSE_ST			512
#define EQUIP_TYPEID_SMDU_OBDC_FUSE_END			518

#define EQUIP_TYPEID_SMDUPLUS_FUSE1			503
#define EQUIP_TYPEID_SMDUPLUS_FUSE_ST			505
#define EQUIP_TYPEID_SMDUPLUS_FUSE_END			511

#define EQUIP_TYPEID_SMDUPLUS_FUSE2_ST			1950
#define EQUIP_TYPEID_SMDUPLUS_FUSE2_END			1961

#define EQUIP_TYPEID_SMDUEBATT_FUSE_ST			3033
#define EQUIP_TYPEID_SMDUEBATT_FUSE_END			3034

#define EQUIP_TYPEID_SMDUEDC_FUSE_ST			3033
#define EQUIP_TYPEID_SMDUEDC_FUSE_END			3040

#define EQUIP_TYPEID_IS_NEED_CHECK(iArgEquipId)					\
	(															\
		(EQUIP_TYPEID_SYSTEM_GROUP == iArgEquipId)				\
		|| (EQUIP_TYPEID_AC_GROUP == iArgEquipId)				\
		|| (EQUIP_TYPEID_AC_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_ACMETER_GROUP == iArgEquipId)			\
		|| (EQUIP_TYPEID_ACMETER_UNIT == iArgEquipId)			\
		|| (EQUIP_TYPEID_DC_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_DCMETER_GROUP == iArgEquipId)			\
		|| (EQUIP_TYPEID_DCMETER_UNIT == iArgEquipId)			\
		|| (EQUIP_TYPEID_DIESEL_GROUP == iArgEquipId)			\
		|| (EQUIP_TYPEID_DIESEL_UNIT == iArgEquipId)			\
		|| (EQUIP_TYPEID_FUEL_GROUP == iArgEquipId)				\
		|| (EQUIP_TYPEID_FUEL_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_IB_GROUP == iArgEquipId)				\
		|| ( (iArgEquipId >= EQUIP_TYPEID_IB_UNIT_ST)			\
			&& (iArgEquipId <= EQUIP_TYPEID_IB_UNIT_END) )		\
		|| (EQUIP_TYPEID_EIB_GROUP == iArgEquipId)				\
		|| (EQUIP_TYPEID_OBAC_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_OBLVD_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_OBFUEL_UNIT == iArgEquipId)			\
		|| (EQUIP_TYPEID_SMDU_GROUP == iArgEquipId)				\
		|| (EQUIP_TYPEID_SMDUP_GROUP == iArgEquipId)			\
		|| (EQUIP_TYPEID_SMDUE_GROUP == iArgEquipId)			\
		|| (EQUIP_TYPEID_SMDUH_GROUP == iArgEquipId)			\
		|| (EQUIP_TYPEID_SMBRC_GROUP == iArgEquipId)			\
		|| (EQUIP_TYPEID_SMBRC_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_SMIO_GROUP == iArgEquipId)				\
		|| ( (iArgEquipId >= EQUIP_TYPEID_SMIO_UNIT_ST)			\
			&& (iArgEquipId <= EQUIP_TYPEID_SMIO_UNIT_END) )	\
		|| (EQUIP_TYPEID_SMTEMP_GROUP == iArgEquipId)			\
		|| ( (iArgEquipId >= EQUIP_TYPEID_SMTEMP_UNIT_ST)		\
			&& (iArgEquipId <= EQUIP_TYPEID_SMTEMP_UNIT_END) )	\
		|| (EQUIP_TYPEID_SMAC_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_SMLVD_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_LVD3_UNIT == iArgEquipId)				\
		|| (EQUIP_TYPEID_OBBATTFUSE_UNIT == iArgEquipId)		\
		|| (EQUIP_TYPEID_NARADA_BMS_GROUP == iArgEquipId)		\
	)

#define EQUIP_TYPEID_EQUAL_VALUE_INC(iArgEquipId, uiArgRealNum)		\
	{	\
		if(iArgEquipId == pEquipList->iEquipTypeID)		\
		{	\
			if(pEquipList->bWorkStatus == TRUE)					\
			{	\
				uiArgRealNum++;							\
			}	\
		}	\
	}

#define EQUIP_TYPEID_EQUAL_RANGE_INC(iArgEquipIdStart, iArgEquipIdEnd, uiArgRealNum)		\
	{	\
		if( (pEquipList->iEquipTypeID >= iArgEquipIdStart)				\
			&& (pEquipList->iEquipTypeID <= iArgEquipIdEnd) )			\
		{	\
			if(pEquipList->bWorkStatus == TRUE)			\
			{		\
				uiArgRealNum++;	\
			}	\
		}	\
	}

    int		iRst, i;
    static int		iEquipNum = -1;
    EQUIP_INFO	*pEquipList;
    static EQUIP_INFO	*pEquipList1 = NULL;
    int	iBufLen = 0;

    if(iEquipNum == -1)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM,
	    0, 
	    0, 
	    &iBufLen, 
	    &iEquipNum, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }

    if(pEquipList1 == NULL)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
	    0, 
	    0, 
	    &iBufLen, 
	    &pEquipList1, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }
    pEquipList = pEquipList1;

    gs_uiRealSMDUNum = 0;
    gs_uiRealSMDUPNum = 0;
    gs_uiRealSMDUENum = 0;
    gs_uiRealSMDUHNum = 0;
    gs_uiRealEIBNum = 0;
    gs_uiRealBattFuseNum = 0;
    gs_uiRealLVDGroupNum = 0;
    gs_uiRealBattFuseGroupNum = 0;
	//changed by Frank Wu,8/8/32,20140312, for adding new web pages to the tab of "power system"
	gs_uiRealSystemGroupNum = 0;
	gs_uiRealACGroupNum = 0;
	gs_uiRealACUnitNum = 0;
	gs_uiRealACMeterGroupNum = 0;
	gs_uiRealACMeterUnitNum = 0;
	gs_uiRealDCUnitNum = 0;
	gs_uiRealDCMeterGroupNum = 0;
	gs_uiRealDCMeterUnitNum = 0;
	gs_uiRealDieselGroupNum = 0;
	gs_uiRealDieselUnitNum = 0;
	gs_uiRealFuelGroupNum = 0;
	gs_uiRealFuelUnitNum = 0;
	gs_uiRealIBGroupNum = 0;
	gs_uiRealIBUnitNum = 0;
	gs_uiRealEIBGroupNum = 0;
	gs_uiRealOBACUnitNum = 0;
	gs_uiRealOBLVDUnitNum = 0;
	gs_uiRealOBFuelUnitNum = 0;
	gs_uiRealSMDUGroupNum = 0;
	gs_uiRealSMDUPGroupNum = 0;
	gs_uiRealSMDUEGroupNum = 0;
	gs_uiRealSMDUHGroupNum = 0;
	gs_uiRealSMBRCGroupNum = 0;
	gs_uiRealSMBRCUnitNum = 0;
	gs_uiRealSMIOGroupNum = 0;
	gs_uiRealSMIOUnitNum = 0;
	gs_uiRealSMTempGroupNum = 0;
	gs_uiRealSMTempUnitNum = 0;
	gs_uiRealSMACUnitNum = 0;
	gs_uiRealSMLVDUnitNum = 0;
	gs_uiRealLVD3UnitNum = 0;
	gs_uiRealOBBattFuseUnitNum = 0;
	gs_uiRealNaradaBMSGroupNum = 0;
//changed by Stone Song ,20160606, for adding the the web setting tab page 'FUSE'
	iSMDUBATT_FuseRealNum = iOBDC_FuseRealNum = iSMDUDC_FuseRealNum = iSMDUPLUS_FuseRealNum = 0;
    for(i = 0; i < iEquipNum; i++)
    {
	if((pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDU_ST) && (pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDU_END))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSMDUNum++;
	    }
	}
	else if((pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUH_ST) && (pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUH_END))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		TRACE_WEB_USER("the smduh iEquipTypeID is %d\n", pEquipList->iEquipTypeID);
		gs_uiRealSMDUHNum++;
	    }
	}
	else if((pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUP) && (pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUP_END))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSMDUPNum++;
	    }   
	}
	else if((pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUE) && (pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUE_END))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSMDUENum++;
	    }
	}
	else if((pEquipList->iEquipTypeID >= EQUIP_TYPEID_EIB_ST) && (pEquipList->iEquipTypeID <= EQUIP_TYPEID_EIB_END))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealEIBNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_BATT_FUSE)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealBattFuseNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_LVD_GROUP)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealLVDGroupNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_BATT_FUSE_GROUP)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealBattFuseGroupNum++;
	    }
	}
//changed by Stone Song ,20160606, for adding the the web setting tab page 'FUSE'
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SMDU_BATT_FUSE)
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUBATT_FuseRealNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDU_BATT_FUSE_ST && pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDU_BATT_FUSE_END)
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUBATT_FuseRealNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_OBDC_FUSE)
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iOBDC_FuseRealNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SMDU_OBDC_FUSE)
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUDC_FuseRealNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDU_OBDC_FUSE_ST && pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDU_OBDC_FUSE_END)
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUDC_FuseRealNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SMDUPLUS_FUSE1)
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUPLUS_FuseRealNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUPLUS_FUSE_ST && pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUPLUS_FUSE_END)
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUPLUS_FuseRealNum++;
	    }
		
	}
	else if(pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUPLUS_FUSE2_ST && pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUPLUS_FUSE2_END)
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUPLUS_FuseRealNum++;
	    }
		
	}
	/*
	else if( (pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUEBATT_FUSE_ST)  && (pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUEBATT_FUSE_END) )
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUEBATT_FuseRealNum++;
	    }
		
	}*/
	else if( (pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUEDC_FUSE_ST)  && (pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUEDC_FUSE_END) )
	{
		if(pEquipList->bWorkStatus == TRUE)
	    {
			iSMDUEDC_FuseRealNum++;
	    }
		
	}
	//changed by Frank Wu,9/9/32,20140312, for adding new web pages to the tab of "power system"
	else if( EQUIP_TYPEID_IS_NEED_CHECK(pEquipList->iEquipTypeID) )
	{
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SYSTEM_GROUP, gs_uiRealSystemGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_AC_GROUP, gs_uiRealACGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_AC_UNIT, gs_uiRealACUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_ACMETER_GROUP, gs_uiRealACMeterGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_ACMETER_UNIT, gs_uiRealACMeterUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_DC_UNIT, gs_uiRealDCUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_DCMETER_GROUP, gs_uiRealDCMeterGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_DCMETER_UNIT, gs_uiRealDCMeterUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_DIESEL_GROUP, gs_uiRealDieselGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_DIESEL_UNIT, gs_uiRealDieselUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_FUEL_GROUP, gs_uiRealFuelGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_FUEL_UNIT, gs_uiRealFuelUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_IB_GROUP, gs_uiRealIBGroupNum);
//		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_IB_UNIT, gs_uiRealIBUnitNum);
		EQUIP_TYPEID_EQUAL_RANGE_INC( EQUIP_TYPEID_IB_UNIT_ST, EQUIP_TYPEID_IB_UNIT_END, gs_uiRealIBUnitNum);

		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_EIB_GROUP, gs_uiRealEIBGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_OBAC_UNIT, gs_uiRealOBACUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_OBLVD_UNIT, gs_uiRealOBLVDUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_OBFUEL_UNIT, gs_uiRealOBFuelUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMDU_GROUP, gs_uiRealSMDUGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMDUP_GROUP, gs_uiRealSMDUPGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMDUE_GROUP, gs_uiRealSMDUEGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMDUH_GROUP, gs_uiRealSMDUHGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMBRC_GROUP, gs_uiRealSMBRCGroupNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMBRC_UNIT, gs_uiRealSMBRCUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMIO_GROUP, gs_uiRealSMIOGroupNum);
		EQUIP_TYPEID_EQUAL_RANGE_INC( EQUIP_TYPEID_SMIO_UNIT_ST, EQUIP_TYPEID_SMIO_UNIT_END, gs_uiRealSMIOUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMTEMP_GROUP, gs_uiRealSMTempGroupNum);
		EQUIP_TYPEID_EQUAL_RANGE_INC( EQUIP_TYPEID_SMTEMP_UNIT_ST, EQUIP_TYPEID_SMTEMP_UNIT_END, gs_uiRealSMTempUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMAC_UNIT, gs_uiRealSMACUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_SMLVD_UNIT, gs_uiRealSMLVDUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_LVD3_UNIT, gs_uiRealLVD3UnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_OBBATTFUSE_UNIT, gs_uiRealOBBattFuseUnitNum);
		EQUIP_TYPEID_EQUAL_VALUE_INC( EQUIP_TYPEID_NARADA_BMS_GROUP, gs_uiRealNaradaBMSGroupNum);
	}
	else
	{}
	pEquipList++;
    }
}

/*==========================================================================*
* FUNCTION :  Web_GetBattNum
* PURPOSE  :  get the number of all kinds of battery equips
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng               DATE: 2015-07-20 To add the SoNick battery
*==========================================================================*/
void Web_GetBattNum(void)
{
#define EQUIP_TYPEID_BATT	301
#define EQUIP_TYPEID_BATT2	311
#define EQUIP_TYPEID_EIBBATT	303
#define EQUIP_TYPEID_EIBBATT_ST		312
#define EQUIP_TYPEID_EIBBATT_END	322

#define EQUIP_TYPEID_SMDUBATT	302
#define EQUIP_TYPEID_SMDUBATT_ST	323
#define EQUIP_TYPEID_SMDUBATT_END	361

#define EQUIP_TYPEID_SMDUEBATT_ST	362
#define EQUIP_TYPEID_SMDUEBATT_END	381

#define EQUIP_TYPEID_SMBAT	304
#define EQUIP_TYPEID_LARGEDUBATT    305
#define EQUIP_TYPEID_SMBRC	306
#define EQUIP_TYPEID_SONICK	310

    int		iRst, i;
    static int		iEquipNum = -1;
    EQUIP_INFO	*pEquipList;
    static EQUIP_INFO	*pEquipList1 = NULL;
    int	iBufLen = 0;

    if(iEquipNum == -1)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM,
	    0, 
	    0, 
	    &iBufLen, 
	    &iEquipNum, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }

    if(pEquipList1 == NULL)
    {
	iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
	    0, 
	    0, 
	    &iBufLen, 
	    &pEquipList1, 
	    0);
	if(iRst != ERR_OK)
	{
	    return;
	}
    }
    pEquipList = pEquipList1;

    gs_uiRealComBattNum = 0;
    gs_uiRealEIBBattNum = 0;
    gs_uiRealSMDUBattNum = 0;
    gs_uiRealSMDUEBattNum = 0;
    gs_uiRealSMBattNum = 0;
    gs_uiRealLargeDUBattNum = 0;
    gs_uiRealSMBRCBattNum = 0;
    gs_uiRealSoNickBattNum = 0;
    TRACE_WEB_USER("iEquipNum is %d\n", iEquipNum);
    for(i = 0; i < iEquipNum; i++)
    {
	if(pEquipList->iEquipTypeID == EQUIP_TYPEID_BATT || pEquipList->iEquipTypeID == EQUIP_TYPEID_BATT2)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealComBattNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_EIBBATT || (pEquipList->iEquipTypeID >= EQUIP_TYPEID_EIBBATT_ST && pEquipList->iEquipTypeID <= EQUIP_TYPEID_EIBBATT_END))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealEIBBattNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SMDUBATT || (pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUBATT_ST && pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUBATT_END))
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		//TRACE_WEB_USER("The smdu equip ID is %d\n", pEquipList->iEquipID);
		gs_uiRealSMDUBattNum++;
	    }
	}
	else if( pEquipList->iEquipTypeID >= EQUIP_TYPEID_SMDUEBATT_ST && pEquipList->iEquipTypeID <= EQUIP_TYPEID_SMDUEBATT_END )
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSMDUEBattNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SMBAT)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSMBattNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_LARGEDUBATT)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealLargeDUBattNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SMBRC)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSMBRCBattNum++;
	    }
	}
	else if(pEquipList->iEquipTypeID == EQUIP_TYPEID_SONICK)
	{
	    if(pEquipList->bWorkStatus == TRUE)
	    {
		gs_uiRealSoNickBattNum++;
	    }
	}
	else
	{}
	pEquipList++;
    }
}
static int Web_GetIB1State(void)
{
//Get DCD Exist state
    SIG_BASIC_VALUE* pSigValue;
    int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 78);//IB number
    int		iError = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int iEquipID = 1;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipID,	//		
	    iVarSubID,		
	    &iBufLen,			
	    (void *)&pSigValue,			
	    iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		return pSigValue->varValue.ulValue >0 ? TRUE:FALSE;
	}
	else
	{
	    return FALSE;
	}
}


void Web_GetRealTimeAlarmData_GHMI(int * pAllAlarmNum,int * pOBAlarmNum, 
												   int * pMAAlarmNum,int * pCAAlarmNum)
{
	int 		iNum = 0;						
	int		iSignalNumber = 0;
	int		iAllAlarmNum = 0, iOBAlarmNum = 0, iMAAlarmNum = 0, iCAAlarmNum = 0;
	ALARM_SIG_VALUE	*pAlarmSigValue = NULL, *pDelete = NULL;

		
	iSignalNumber = Web_GetRealTimeAlarmData(&pAlarmSigValue);
	
	if(iSignalNumber > 0)
	{
		//Alarm sort
		DXI_MakeAlarmSigValueSort(pAlarmSigValue, iSignalNumber);
		if(iSignalNumber > 200)
		{
			iSignalNumber = 200;
		}
		pDelete = pAlarmSigValue;
		for(iNum =0; iNum < iSignalNumber && pAlarmSigValue != NULL; iNum++, pAlarmSigValue++)
		{
			if(pAlarmSigValue->pStdSig->iAlarmLevel != ALARM_LEVEL_NONE)
			{
				if(pAlarmSigValue->pStdSig->iAlarmLevel == ALARM_LEVEL_OBSERVATION)
			    {
					iOBAlarmNum++;
			    }
			    else if(pAlarmSigValue->pStdSig->iAlarmLevel == ALARM_LEVEL_MAJOR)
			    {
					iMAAlarmNum++;
			    }
			    else if(pAlarmSigValue->pStdSig->iAlarmLevel == ALARM_LEVEL_CRITICAL)
			    {
					iCAAlarmNum++;
			    }
			}
		}	
		if(pDelete != NULL)
		{
			DELETE(pDelete);
			pDelete = NULL;
		}
		iAllAlarmNum = iSignalNumber;
	}	

	*pAllAlarmNum = iAllAlarmNum;
	*pOBAlarmNum = iOBAlarmNum;
	*pMAAlarmNum = iMAAlarmNum;
	*pCAAlarmNum =  iCAAlarmNum;

	return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeLoginDataFile
* PURPOSE  :  To make the json file <data.login.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeLoginDataFile(void)
{
#define RECT_EXPANSION		180
#define ALARM_SOUND_ACTIVATION	251
#define ID_SYSTEMNAME2	"ID_SYSTEMNAME2"
#define ID_SITENAME2	"ID_SITENAME2"
#define ID_SITENAME	"ID_SITENAME"
#define ID_PART_NUMBER	"ID_PART_NUMBER"
#define ID_SERIAL_NUMBER	"ID_SERIAL_NUMBER"
#define ID_HARDWARE_VERSION	"ID_HARDWARE_VERSION"
#define ID_SOFTWARE_VERSION	"ID_SOFTWARE_VERSION"
#define ID_CFG_VERSION	"ID_CFG_VERSION"
#define ID_LOC_LANG_TYPE    "ID_LOC_LANG_TYPE"
#define ID_CONTROL_MODE	    "ID_CONTROL_MODE"
#define ID_ALARM_SOUND	    "ID_ALARM_SOUND"
#define ID_ALL_ALARM_NUM			"ID_ALL_ALARM_NUM"
#define ID_OBSERVATION_NUM		"ID_OBSERVATION_NUM"
#define ID_MAJOR_NUM				"ID_MAJOR_NUM"
#define ID_CRITICAL_NUM			"ID_CRITICAL_NUM"
#define ID_LOAD_CURRENT				"ID_LOAD_CURRENT"
#define ID_VOLTAGE			"ID_VOLTAGE"

#define ID_CODE_TYPE	 "ID_CODE_TYPE"

#define ID_RESET_PASSWORD	 "ID_RESET_PASSWORD"


    //char szCommand[128];
    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    ACU_PRODUCT_INFO sAcuProductInfo;
    int	iError = 0;
    FILE	*fp;
    char szBuf[80];
    char	*pLangCode = NULL;
    int i;
    int	iBufLen = 0;
    int	iVarSubID = 0;
    SIG_BASIC_VALUE*		pSigValue;
    
    int		iAllAlarmNum = 0, iOBAlarmNum = 0, iMAAlarmNum = 0, iCAAlarmNum = 0;
    char szformat[32];
    int iValueFormat = 1;

	int iPaswdChangeFlag = 1;

    TRACE_WEB_USER("Begin to exec Web_MakeLoginDataFile\n");
    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeLoginDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_LOGIN);*/
    iReturn = LoadHtmlFile(WEB_DATA_LOGIN, &pHtml);
    if(iReturn > 0)
    {
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[\"%s\",\"%s\"]", g_SiteInfo.langDescription.pFullName[0], g_SiteInfo.langDescription.pFullName[1]);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SYSTEMNAME2), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[\"%s\",\"%s\"]", g_SiteInfo.langSiteLocation.pFullName[0], g_SiteInfo.langSiteLocation.pFullName[1]);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITENAME2), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[\"%s\",\"%s\"]", g_SiteInfo.langSiteName.pFullName[0], g_SiteInfo.langSiteName.pFullName[0]);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITENAME), szBuf);

	memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
	iBufLen = sizeof(sAcuProductInfo);
	iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
	    ACU_PRODUCT_INFO_GET, 
	    0, 
	    &iBufLen,
	    &(sAcuProductInfo),
	    0);
	if(iError == ERR_DXI_OK)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_PART_NUMBER), sAcuProductInfo.szPartNumber);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SERIAL_NUMBER), sAcuProductInfo.szACUSerialNo);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HARDWARE_VERSION), sAcuProductInfo.szHWRevision);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOFTWARE_VERSION), sAcuProductInfo.szSWRevision);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CFG_VERSION), sAcuProductInfo.szCfgFileVersion);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_PART_NUMBER), "");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SERIAL_NUMBER), "");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HARDWARE_VERSION), "");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOFTWARE_VERSION), "");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CFG_VERSION), "");
	}

	iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
	    LOCAL_LANGUAGE_CODE, 
	    0, 
	    &iBufLen,
	    &(pLangCode),
	    0);
	if(iError == ERR_DXI_OK)
	{
	    TRACE_WEB_USER("local language is %s\n", pLangCode);
	    for(i = 0; i < LANGUAGE_NUM; i++)
	    {
		if(strcmp((const char*)pLangCode, szWebLangType[i]) == 0)
		{
		    break;
		}
	    }
	    memset(szBuf, 0, sizeof(szBuf));
	    snprintf(szBuf, sizeof(szBuf), "%d", i);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LOC_LANG_TYPE), szBuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LOC_LANG_TYPE), "7");
	}

	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, RECT_EXPANSION);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    1,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    0);
	if(iError == ERR_DXI_OK)
	{
	    memset(szBuf, 0, sizeof(szBuf));
	    snprintf(szBuf, sizeof(szBuf), "%ld", pSigValue->varValue.enumValue);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONTROL_MODE), szBuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONTROL_MODE), "0");
	}

#ifdef _CODE_FOR_MINI
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CODE_TYPE), "0");
#else
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CODE_TYPE), "1");
#endif

	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ALARM_SOUND_ACTIVATION);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    1,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    0);
	if(iError == ERR_DXI_OK)
	{
	    memset(szBuf, 0, sizeof(szBuf));
	    snprintf(szBuf, sizeof(szBuf), "%ld", pSigValue->varValue.enumValue);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_SOUND), szBuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_SOUND), "0");
	}

	Web_GetRealTimeAlarmData_GHMI(&iAllAlarmNum,&iOBAlarmNum, &iMAAlarmNum,&iCAAlarmNum);
	//printf("Web_MakeLoginDataFile():iAllAlarmNum = %d,iOBAlarmNum=%d,iMAAlarmNum=%d,iCAAlarmNum=%d~~\n",iAllAlarmNum,iOBAlarmNum,iMAAlarmNum,iCAAlarmNum);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iAllAlarmNum);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALL_ALARM_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iOBAlarmNum);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_OBSERVATION_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iMAAlarmNum);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_MAJOR_NUM), szBuf);
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iCAAlarmNum);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CRITICAL_NUM), szBuf);


	memset(szformat, 0, sizeof(szformat));
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szformat, sizeof(szformat), "\"%%.%df\"", iValueFormat);// for example "%.1f"
	snprintf(szBuf, sizeof(szBuf), szformat, pstSysCurr->pSigValue->varValue.fValue);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LOAD_CURRENT), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), szformat, pstSysVolt->pSigValue->varValue.fValue);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_VOLTAGE), szBuf);


	iPaswdChangeFlag = Web_GetPaswdChangeFlag();
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", iPaswdChangeFlag);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RESET_PASSWORD), szBuf);
	//printf("Web_MakeLoginDataFile(): iPaswdChangeFlag = %d~~\n",iPaswdChangeFlag);

	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_LOGIN_VAR);*/
	if((fp = fopen(WEB_DATA_LOGIN_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}
    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    /*strcpy(szCommand,"cp /app/www_user/html/datas/data.loginsubmit.html /var/datas");
    TRACE_WEB_USER("Going to exec _SYSTEM function!\n");
    _SYSTEM(szCommand);*/
    TRACE_WEB_USER("end to exec Web_MakeLoginDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  MakeVarField
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
*==========================================================================*/
static char *MakeVarField(IN char *szBuffer)
{
    char	*szReturnBuffer;
    szReturnBuffer = NEW(char, 128);

    if(szReturnBuffer != NULL)
    {
	snprintf(szReturnBuffer, 128, "/*[%s]*/", szBuffer);
	return szReturnBuffer;
    }
    AppLogOut("MakeVarField", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
    return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_FillString
* PURPOSE  :  Get the value in pSigValue, and change the value to new string,
and replace the strOld to new string in *pHtml
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  pHtml: the pointer pointing to a certain pointer which can be modified
strOld: the old string which will be replaced
pSigValue: the pointer pointing to the value
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_FillString(char **pHtml, char *strOld, SIG_BASIC_VALUE* pSigValue, int iEquip, BOOL bValid)
{
#define ID_DG_GROUP	261
#define ID_MPPT_GROUP	1350
#define ID_AC_GROUP	103
#define ID_SMIO_GROUP	263
#define ID_SYSTEM_EQUIP	    1
#define ID_EXIST	100
#define ID_SYS_VOL_LEVEL	224
#define ID_TIME_FORMAT_SIGNAL	456
#define ID_CONVERTER_GROUP	210
#define ID_400V_CONV		38
#define ID_RECT_GROUP		2
#define ID_AC_PHASE		24
#define ID_MPPT_MODE		446
#define ID_CONVERTER_MODE	451
    char szBuf[80];
    int itemp;
    char *pHtml1 = *pHtml;
    int iSigID, iSigType;
    SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
    SET_SIG_VALUE     *pSettingSigValue = NULL;
    int iBufLen;
    int iValueFormat;
    char szformat[32];
	static float fValueTemp;
    //TRACE_WEB_USER("pSigValue->ucType is %d\n", pSigValue->ucType);

    //TRACE_WEB_USER("strOld is %s\n", strOld);
    // if this is a signal with no pointer
    if(bValid == FALSE)
    {
	ReplaceString(&pHtml1, strOld, "\"\"");
	*pHtml = pHtml1;
	return;
    }
    memset(szBuf, 0, sizeof(szBuf));
    memset(szformat, 0, sizeof(szformat));
    iBufLen = 0;
    if(pSigValue->ucSigType == SIG_TYPE_SAMPLING)
    {
	pSampleSigValue = (SAMPLE_SIG_VALUE *)(pSigValue);
    }
    else if(pSigValue->ucSigType == SIG_TYPE_SETTING)
    {
	pSettingSigValue = (SET_SIG_VALUE *)(pSigValue);
    }
    else
    {}
    if(pSigValue->ucType == VAR_LONG)
    {
		if(pSigValue->ucSigType == SIG_TYPE_SAMPLING)
		{
			iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "[%ld,", pSigValue->varValue.lValue);
		}
		else
		{
			iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "%ld", pSigValue->varValue.lValue);
		}
    }
    else if(pSigValue->ucType == VAR_FLOAT)
    {
		if(pSigValue->ucSigType == SIG_TYPE_SAMPLING)
		{
			iValueFormat = Web_TransferFormatToInt(pSampleSigValue->pStdSig->szValueDisplayFmt);
			snprintf(szformat, sizeof(szformat), "[\"%%.%df\",", iValueFormat);// for example "[\"%.1f\","
			//TRACE_WEB_USER("szformat is %s\n", szformat);
			iSigID = ((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID;
		}
		else
		{
			iValueFormat = Web_TransferFormatToInt(pSettingSigValue->pStdSig->szValueDisplayFmt);
			snprintf(szformat, sizeof(szformat), "%%.%df", iValueFormat);// for example "%.2f"
			iSigID = ((SET_SIG_VALUE *)pSigValue)->pStdSig->iSigID;
		}

		iSigType = pSigValue->ucSigType;
		//if( IS_TEMP_SIGNAL(iEquip, iSigType, iSigID ))
		if(DXI_isTempSignal(pSigValue))
		{
			gs_iTempUnitSwitch = DXI_GetTempSwitch(TEMP_VALUE,
				pSigValue->varValue.fValue,
				&fValueTemp,
				NULL
				);
		}
		else if( IS_TEMP_COE_SIGNAL(iEquip, iSigType, iSigID ))
		{
			gs_iTempUnitSwitch = DXI_GetTempSwitch(TEMP_COE,
				pSigValue->varValue.fValue,
				&fValueTemp,
				NULL
				);
		}
		else
		{
			fValueTemp = pSigValue->varValue.fValue;
		}
		iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), szformat, fValueTemp);
    }
    else if(pSigValue->ucType == VAR_UNSIGNED_LONG)
    {
	if(pSigValue->ucSigType == SIG_TYPE_SAMPLING)
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "[%ld,", pSigValue->varValue.ulValue);
	}
	else
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "%ld", pSigValue->varValue.ulValue);
	}
    }
    else if(pSigValue->ucType == VAR_DATE_TIME)
    {
	if(pSigValue->ucSigType == SIG_TYPE_SAMPLING)
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "[%ld,", pSigValue->varValue.dtValue);
	}
	else
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "%ld", pSigValue->varValue.dtValue);
	}
    }
    else if(pSigValue->ucType == VAR_ENUM)
    {
	itemp = pSigValue->varValue.enumValue;
	if(pSigValue->ucSigType == SIG_TYPE_SAMPLING)
	{
	    iSigID = ((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID;
	    if(((iEquip == ID_DG_GROUP) && (iSigID == ID_EXIST)) || ((iEquip == ID_MPPT_GROUP) && (iSigID == ID_EXIST)) ||
		((iEquip == ID_AC_GROUP) && (iSigID == ID_EXIST)) || ((iEquip == ID_SMIO_GROUP) && (iSigID == ID_EXIST)) ||
		((iEquip == ID_CONVERTER_GROUP) && (iSigID == ID_400V_CONV)) ||
		((iEquip == ID_RECT_GROUP) && (iSigID == ID_AC_PHASE))) 		// DG group and the signal ID is 100
	    {
		iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "%d", itemp);
	    }
	    else
	    {
		iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "[[\"%s\",\"%s\"],", ((SAMPLE_SIG_VALUE *)(pSigValue))->pStdSig->pStateText[itemp]->pFullName[0],
		    ((SAMPLE_SIG_VALUE *)(pSigValue))->pStdSig->pStateText[itemp]->pFullName[1]);
	    }
	}
	else if(pSigValue->ucSigType == SIG_TYPE_SETTING)
	{
	    iSigID = ((SET_SIG_VALUE *)pSigValue)->pStdSig->iSigID;
	    if(((iEquip == ID_SYSTEM_EQUIP) && (iSigID == ID_SYS_VOL_LEVEL)) || ((iEquip == ID_SYSTEM_EQUIP) && (iSigID == ID_TIME_FORMAT_SIGNAL)) ||
			((iEquip == ID_SYSTEM_EQUIP) && (iSigID == ID_MPPT_MODE)) ||
			((iEquip == ID_SYSTEM_EQUIP) && (iSigID == ID_CONVERTER_MODE)))
	    {
		//change ENUM value to int
		iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "%d", itemp);
	    }
	    else
	    {
		iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "[\"%s\",\"%s\"]", ((SET_SIG_VALUE *)(pSigValue))->pStdSig->pStateText[itemp]->pFullName[0],
		    ((SET_SIG_VALUE *)(pSigValue))->pStdSig->pStateText[itemp]->pFullName[1]);
	    }
	}
	else
	{}
	//TRACE_WEB_USER("[VAR_ENUM]szBuf is %s\n", szBuf);
    }
    else
    {
	return;
    }
    if((pSigValue->ucSigType == SIG_TYPE_SAMPLING) && (!((iEquip == ID_DG_GROUP) && (((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID == ID_EXIST))) &&
	(!((iEquip == ID_MPPT_GROUP) && (((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID == ID_EXIST))) &&
	(!((iEquip == ID_CONVERTER_GROUP) && (((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID == ID_400V_CONV))) &&
	(!((iEquip == ID_RECT_GROUP) && (((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID == ID_AC_PHASE))) &&
	(!((iEquip == ID_AC_GROUP) && (((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID == ID_EXIST))) &&
	(!((iEquip == ID_SMIO_GROUP) && (((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID == ID_EXIST))))
    {
	if(pSampleSigValue->sv.iRelatedAlarmLevel > 0) //here add the value state---alarm state
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "1,");
	}
	else
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "0,");
	}
	if(SIG_VALUE_IS_VALID(&pSampleSigValue->bv))
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "0,");
	}
	else
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "1,");
	}
	if(SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv))
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "0]");
	}
	else
	{
	    iBufLen += snprintf(szBuf + iBufLen, (sizeof(szBuf) - iBufLen), "1]");
	}
    }
    else
    {
	//iBufLen += sprintf(szBuf + iBufLen, "0,");
    }
    /*if(pSigValue->ucSigType == SIG_TYPE_SAMPLING)
    {
    }*/
    /*else
    {
	if(SIG_VALUE_IS_VALID(&pSettingSigValue->bv))
	{
	    iBufLen += sprintf(szBuf + iBufLen, "0,");
	}
	else
	{
	    iBufLen += sprintf(szBuf + iBufLen, "1,");
	}
	if(SIG_VALUE_IS_CONFIGURED(&pSettingSigValue->bv))
	{
	    iBufLen += sprintf(szBuf + iBufLen, "0]");
	}
	else
	{
	    iBufLen += sprintf(szBuf + iBufLen, "1]");
	}
    }*/
 //   if((strcmp((const char*)strOld, "/*[ID_TEMP_H1]*/") == 0) || (strcmp((const char*)strOld, "/*[ID_TEMP_H2]*/") == 0))
 //   {
	////TRACE_WEB_USER("Deal with ID_TEMP_H1 and ID_TEMP_H2\n");
	//if(pSigValue->varValue.fValue > 75)
	//{
	//    pSigValue->varValue.fValue = 75;
	//    sprintf(szBuf, "%.1f", pSigValue->varValue.fValue);
	//}
 //   }
    ReplaceString(&pHtml1, strOld, szBuf);
    *pHtml = pHtml1;
}

int Web_GetSignalValuePoint(int iEquipID,
							int iSignalType,
							int iSignalID,
							SIG_BASIC_VALUE** pSigValue)
{
	int iBufLen, iVarSubID, iError;

	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,
		iVarSubID,
		&iBufLen,
		(void *)pSigValue,
		0);
	return iError;
}

/*==========================================================================*
* FUNCTION :  Web_SourceDataInit
* PURPOSE  :  To init source buf for json file 
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Song Xu               DATE: 2017-03-10
*==========================================================================*/
int Web_SourceDataInit(SIG_BASIC_VALUE** pSourCurrSigValue,
						SIG_BASIC_VALUE** pSourNumSigValue,
						SIG_BASIC_VALUE** pRectCurrSigValue,
						SIG_BASIC_VALUE** pMpptCurrSigValue)
{
#define ID_SIGNAL_SOURCE_CURR		240
#define ID_SIGNAL_SOURCE_NUM		241
#define ID_EQUIP_RECT_GROUP			2
#define ID_SIGNAL_RECT_CURR			1
#define ID_EQUIP_MPPT_GROUP			1350
#define ID_SIGNAL_MPPT_CURR			1
	
	int iError = 0;

	iError |= Web_GetSignalValuePoint(ID_SYSTEM,
		SIG_TYPE_SAMPLING,
		ID_SIGNAL_SOURCE_CURR,
		pSourCurrSigValue
		);

	iError |= Web_GetSignalValuePoint(ID_SYSTEM,
		SIG_TYPE_SAMPLING,
		ID_SIGNAL_SOURCE_NUM,
		pSourNumSigValue
		);


	iError |= Web_GetSignalValuePoint(ID_EQUIP_RECT_GROUP,
		SIG_TYPE_SAMPLING,
		ID_SIGNAL_RECT_CURR,
		pRectCurrSigValue
		);


	iError |= Web_GetSignalValuePoint(ID_EQUIP_MPPT_GROUP,
		SIG_TYPE_SAMPLING,
		ID_SIGNAL_MPPT_CURR,
		pMpptCurrSigValue
		);


	if(iError != ERR_DXI_OK)
	{
		AppLogOut("Web_SourceDataInit", APP_LOG_ERROR, "Get Source Signal Failed in Web_SourceDataInit! \n");
	}
	return iError;
}
/*==========================================================================*
* FUNCTION :  Web_MakeSourceDataBuf
* PURPOSE  :  To make the source buf for json file 
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Song Xu               DATE: 2017-03-10
*==========================================================================*/
static char* Web_MakeSourceDataBuf(void)
{
	static BOOL s_iInitFlag = 0;
	static SIG_BASIC_VALUE* pSourCurrSigValue;
	static SIG_BASIC_VALUE* pSourNumSigValue;
	static SIG_BASIC_VALUE* pRectCurrSigValue;
	static SIG_BASIC_VALUE* pMpptCurrSigValue;
	int iError, iBufLen = 0;
	float fTotalCurr = 0;
	int iInputNum = 0;
	char *szTemp = NULL;
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;

	if(s_iInitFlag == 0)
	{	
		iError = Web_SourceDataInit(&pSourCurrSigValue,
			&pSourNumSigValue,
			&pRectCurrSigValue,
			&pMpptCurrSigValue);
		if(iError == ERR_DXI_OK)
		{
			s_iInitFlag = 1;
		}
		else 
		{
			s_iInitFlag = -1;
		}
	}
	
	if (s_iInitFlag == -1) //need no else
	{
		return NULL;
	}
	iBufLen += snprintf(szTemp + iBufLen, (BUFFER_LEN - iBufLen), "[[%d,%.1f],",
		pSourNumSigValue->varValue.lValue, pSourCurrSigValue->varValue.fValue);

	if(gs_uiRectifierNumber > 0)
	{	
		iInputNum++;
		fTotalCurr += pRectCurrSigValue->varValue.fValue;
	}

	if(gs_uiSolarNumber > 0 )
	{
		iInputNum++;
		fTotalCurr += pMpptCurrSigValue->varValue.fValue;
	}

	if(pSourNumSigValue->varValue.lValue > 0)
	{
		iInputNum++;
		fTotalCurr += pSourCurrSigValue->varValue.fValue;
	}

	iBufLen += snprintf(szTemp + iBufLen, (BUFFER_LEN - iBufLen), "[%d,%.1f]",
		iInputNum, fTotalCurr);
	//show whether the EXIST of EIB of SMDU source.
	iBufLen += snprintf(szTemp + iBufLen, (BUFFER_LEN - iBufLen), ",[%d,%d]]",
		gs_uiSourceEIBExist, gs_uiSourceSMDUExist);

return szTemp;
}


#define SMDU_MAX_NUM			8
#define SMDU_SHUNT_MAX_NUM		5
#define EIB_MAX_NUM				4
#define EIB_SHUNT_MAX_NUM		3
static SIG_BASIC_VALUE *gs_pSigSMDUShuntCurr[SMDU_MAX_NUM * SMDU_SHUNT_MAX_NUM] = {NULL};
static SIG_BASIC_VALUE *gs_pSigSMDUShuntType[SMDU_MAX_NUM * SMDU_SHUNT_MAX_NUM] = {NULL};
static SIG_BASIC_VALUE *gs_pSigEIBShuntCurr[EIB_MAX_NUM * EIB_SHUNT_MAX_NUM] = {NULL};
static SIG_BASIC_VALUE *gs_pSigEIBShuntType[EIB_MAX_NUM * EIB_SHUNT_MAX_NUM] = {NULL};
static EQUIP_INFO	*gs_pSMDUEquipInfo[SMDU_MAX_NUM];
static EQUIP_INFO	*gs_pEIBEquipInfo[EIB_MAX_NUM];
#define SOURCE_INDEX_ID_SMDU	1
#define SOURCE_INDEX_ID_EIB		2

/*==========================================================================*
* FUNCTION :  Web_SourceDetailDataInit
* PURPOSE  :  To init the detail source buf for json file 
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Song Xu               DATE: 2017-03-10
*==========================================================================*/
int Web_SourceDetailDataInit()
{
#define EQUIP_ID_SMDU		107
#define EQUIP_ID_EIB		197
	int iBufLen;
	int iSigIdSMDUCurr[SMDU_SHUNT_MAX_NUM] =
	{
			31,32,33,34,35
	};
	int iSigIdEIBCurr[EIB_SHUNT_MAX_NUM] =
	{
			64,65,66
	};
	int iSigIdSMDUType[SMDU_SHUNT_MAX_NUM] =
	{
			16,17,18,19,20
	};
	int iSigIdEIBType[EIB_SHUNT_MAX_NUM] =
	{
			16,17,18
	}; 
	int iError=0, i, j;

	for(i = 0; i < SMDU_MAX_NUM; i++)
	{
		for(j = 0; j < SMDU_SHUNT_MAX_NUM; j++)
		{
			iError |= Web_GetSignalValuePoint(EQUIP_ID_SMDU + i,
				SIG_TYPE_SAMPLING,
				iSigIdSMDUCurr[j],
				&(gs_pSigSMDUShuntCurr[i * SMDU_SHUNT_MAX_NUM + j])
				);

			iError |= Web_GetSignalValuePoint(EQUIP_ID_SMDU + i,
				SIG_TYPE_SETTING,
				iSigIdSMDUType[j],
				&(gs_pSigSMDUShuntType[i * SMDU_SHUNT_MAX_NUM + j])
				);

			iError |= DxiGetData(VAR_A_EQUIP_INFO,
				EQUIP_ID_SMDU + i,
				0,
				&iBufLen,
				&(gs_pSMDUEquipInfo[i]),
				0);	
			
		}
	}
	for(i = 0; i < EIB_MAX_NUM; i++)
	{
		for(j = 0; j < EIB_SHUNT_MAX_NUM; j++)
		{
			iError |= Web_GetSignalValuePoint(EQUIP_ID_EIB + i,
				SIG_TYPE_SAMPLING,
				iSigIdEIBCurr[j],
				&(gs_pSigEIBShuntCurr[i * EIB_SHUNT_MAX_NUM + j])
				);
			iError |= Web_GetSignalValuePoint(EQUIP_ID_EIB + i,
				SIG_TYPE_SETTING,
				iSigIdEIBType[j],
				&(gs_pSigEIBShuntType[i * EIB_SHUNT_MAX_NUM + j])
				);
			iError |= DxiGetData(VAR_A_EQUIP_INFO,
				EQUIP_ID_EIB + i,
				0,
				&iBufLen,
				&(gs_pEIBEquipInfo[i]),
				0);	
		}
	}

 
return iError;
}



/*==========================================================================*
* FUNCTION :  Web_MakeSourceDetailDataBuf
* PURPOSE  :  To make the detail source buf for json file. data.system_source.html 
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Song Xu               DATE: 2017-03-10
*==========================================================================*/
static char* Web_MakeSourceDetailDataBuf(int iIndex)
{
	static BOOL s_iInitFlag = 0;
	int iError, iBufLen = 0;
	int i, j;
	float fTotalCurr = 0;
	int iInputNum = 0;
	char *szTemp = NULL;
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	int iEquipNum = 0, iShuntNum = 0;
	int iShuntNumEachEquip = 0;

	EQUIP_INFO **pEquipInfo;
	SIG_BASIC_VALUE **pSigShuntCurr;
	SIG_BASIC_VALUE **pSigShuntType;

	SAMPLE_SIG_VALUE *pShuntSampleSig = NULL;

	if(iIndex == SOURCE_INDEX_ID_SMDU)
	{
		iEquipNum = SMDU_MAX_NUM;
		iShuntNum = SMDU_SHUNT_MAX_NUM;
		pSigShuntCurr = gs_pSigSMDUShuntCurr;
		pSigShuntType = gs_pSigSMDUShuntType;
		pEquipInfo = gs_pSMDUEquipInfo;
	}
	else if (iIndex == SOURCE_INDEX_ID_EIB)
	{
		iEquipNum = EIB_MAX_NUM;
		iShuntNum = EIB_SHUNT_MAX_NUM;
		pSigShuntCurr = gs_pSigEIBShuntCurr;
		pSigShuntType = gs_pSigEIBShuntType;
		pEquipInfo = gs_pEIBEquipInfo;
	}

	if(s_iInitFlag == 0)
	{	
		if(ERR_DXI_OK == Web_SourceDetailDataInit())
		{
			s_iInitFlag = 1;
		}
		else 
		{
			s_iInitFlag = -1;
		}
	}
	if (s_iInitFlag == -1) //need no else
	{
		return NULL;
	}

	iBufLen += snprintf(szTemp + iBufLen, (BUFFER_LEN - iBufLen), "[");

	if(iIndex == SOURCE_INDEX_ID_SMDU)
	{
		gs_uiSourceSMDUExist = 0;
	}
	else if (iIndex == SOURCE_INDEX_ID_EIB)
	{
		gs_uiSourceEIBExist = 0;
	}
	
	for(i = 0; i < iEquipNum; i++)
	{
		iShuntNumEachEquip = 0;
//		printf("ddddd%d %d %d\n",i,pEquipInfo[i]->bCommStatus,pEquipInfo[i]->bWorkStatus);
		if(pEquipInfo[i]->bCommStatus == TRUE && pEquipInfo[i]->bWorkStatus == TRUE)
		{
			for(j = 0; j < iShuntNum; j++)
			{
				if(pSigShuntCurr[i * iShuntNum + j] != NULL && pSigShuntType[i * iShuntNum + j] != NULL)
				{
					if(pSigShuntType[i * iShuntNum + j]->varValue.lValue == 4)
					{
						if( iShuntNumEachEquip == 0)
						{
							iBufLen += snprintf(szTemp + iBufLen, (BUFFER_LEN - iBufLen), "[[\"%s\",\"%s\"],",
								pEquipInfo[i]->pEquipName->pFullName[0],
								pEquipInfo[i]->pEquipName->pFullName[1]);
						}
						iShuntNumEachEquip++;
						pShuntSampleSig = (SAMPLE_SIG_VALUE *)(pSigShuntCurr[i * iShuntNum + j]);
						iBufLen += snprintf(szTemp + iBufLen, (BUFFER_LEN - iBufLen), "[[\"%s\",\"%s\"],\"%.1f %s\"],",
							pShuntSampleSig->pStdSig->pSigName->pFullName[0],
							pShuntSampleSig->pStdSig->pSigName->pFullName[1],
							pSigShuntCurr[i * iShuntNum + j]->varValue.fValue,
							pShuntSampleSig->pStdSig->szSigUnit);
						//EIB and SMDU source exist
						if(iIndex == SOURCE_INDEX_ID_SMDU)
						{
							gs_uiSourceSMDUExist = 1;
						}
						else if (iIndex == SOURCE_INDEX_ID_EIB)
						{
							gs_uiSourceEIBExist = 1;
						}
					}
				}
			}
		}

		if(iShuntNumEachEquip > 0 )
		{
			iBufLen--;
			iBufLen += snprintf(szTemp + iBufLen, (BUFFER_LEN - iBufLen), "],");
		}
	}
	if(iBufLen > 2)
	{
		iBufLen--;
	}		
	iBufLen += snprintf(szTemp + iBufLen, (BUFFER_LEN - iBufLen), "]");
return szTemp;
}

void Web_MakeSourceDetailDataFile(void)
{
#define SOURCE_SMDU_SIGNAL "SMDU_SIGNAL"
#define SOURCE_EIB_SIGNAL "EIB_SIGNAL"


    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;

    iReturn = LoadHtmlFile(WEB_DATA_SYSTEM_SOURECE, &pHtml);
    if(iReturn > 0)
    {
		pbuf = Web_MakeSourceDetailDataBuf(SOURCE_INDEX_ID_SMDU);
		if(pbuf != NULL)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(SOURCE_SMDU_SIGNAL), pbuf);
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(SOURCE_SMDU_SIGNAL), "[]");
		}

		pbuf = Web_MakeSourceDetailDataBuf(SOURCE_INDEX_ID_EIB);
		if(pbuf != NULL)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(SOURCE_EIB_SIGNAL), pbuf);
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(SOURCE_EIB_SIGNAL), "[]");
		}

		if((fp = fopen(WEB_DATA_SYSTEM_SOURECE_VAR,"wb")) != NULL)
		{
			if(pHtml != NULL)
			{
				fwrite(pHtml,strlen(pHtml), 1, fp);
			}	    
			fclose(fp);
		}
		else
		{}

		SAFELY_DELETE(pHtml);
    }
    else
    {
    }
   
    return;
}



/*==========================================================================*
* FUNCTION :  Web_MakeSingleCostom_inputs_status
* PURPOSE  : 
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  char * : the pointer pointing to the string including the equip data.
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
char* Web_MakeSingleCostom_inputs_status(int itype)
{
    int i, j, k, iAllSignalNum = 0, iEquipNum = 0;
    char *szTemp = NULL;
    int iFileLen = 0;
    char szBuf[128];
    BOOL bdisplay;
    BOOL bdisplay1;
    int		iBufLength;

    TRACE_WEB_USER("Begin to exec Web_MakeSingleOtherEquipDataFile1[%d]\n", itype);
    //check the pointer valid begin
    if(gs_pWebOtherSigInfo[itype] == NULL)
    {
	return NULL;
    }
    if((gs_pWebOtherSigInfo[itype]->stWebEquipInfo == NULL) || (gs_pWebOtherSigInfo[itype]->stWebPrivate == NULL))
    {
	return NULL;
    }
    //check the pointer valid end


    if(itype == SMDUE_SEQ)
    {
		iEquipNum = gs_uiRealSMDUENum;
    }
    else
    {}

    if((gs_pWebOtherSigInfo[itype] != NULL) && (gs_pWebOtherSigInfo[itype]->stWebEquipInfo != NULL))
    {
	for(i = 0; i < iEquipNum; i++)
	{
	    iAllSignalNum += gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].iSamplenum;
	}
    }

    if(iAllSignalNum > 0)
    {
	//szTemp = NEW(char, (80 * iAllSignalNum + 90 * iAllSignalNum + 100));
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	/*if(szTemp == NULL)
	{
	    AppLogOut("Web_MakeSingleDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    return NULL;
	}*/
	iBufLength = 80 * iAllSignalNum + 90 * iAllSignalNum + 100;
	
	k = 0;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	for(i = 0; i < iEquipNum; i++)
	{
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	    if(gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].bSingleValid == FALSE)
	    {
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
	    }
	    else
	    {
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],", gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[0],
		    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[1]);
	    }
	    for(j = 0; j < gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].iSamplenum; j++)
	    {


			//TRACE_WEB_USER("Get signal value, exec [%d][%d] signal\n", i, j);
			if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].bSingleValid == FALSE)
			{
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"---\",\"---\"],");
			}
			else
			{
				if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue->ucSigType == SIG_TYPE_SAMPLING)
				{
					bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->bv);
					bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig), DISPLAY_WEB);

					if((bdisplay == TRUE) && (bdisplay1 == TRUE))
					{
						iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
						((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[0],
						((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[1]);
					}
				}
				else if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue->ucSigType == SIG_TYPE_SETTING)
				{
					bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->bv);
					bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig), DISPLAY_WEB);
					if((bdisplay == TRUE) && (bdisplay1 == TRUE))
					{
						iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
						((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[0],
						((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[1]);
					}
				}
				else
				{}
			}
			memset(szBuf, 0, sizeof(szBuf));

			Makesignalstring_use_Custom(gs_pWebOtherSigInfo[itype]->stWebPrivate[k], szBuf, sizeof(szBuf),gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo);
			//TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
			//TRACE_WEB_USER("[Web_MakeSingleBattDataFile] szBuf is %s\n", szBuf);
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
			if((bdisplay == TRUE) && (bdisplay1 == TRUE))
			{
				if(iFileLen >= 1)
				{
				iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
				}
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
			}
			k++;
		}

		if(iFileLen >= 1)
		{
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
		//printf("end to exec Web_MakeSingleOtherEquipDataFile1[%d] iFileLen = %d\n", itype, iFileLen);
		return szTemp;
    }
    else
    {
		TRACE_WEB_USER("end to exec Web_MakeSingleOtherEquipDataFile1[%d]\n", itype);
		return NULL;
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeHomeDataFile
* PURPOSE  :  To make the json file <data.index.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeHomeDataFile(void)
{
#define ID_SOLAR_CURRENT	"ID_SOLAR_CURRENT"
#define ID_RECT_CURRENT		"ID_RECT_CURRENT"
#define ID_DC1_CURRENT		"ID_DC1_CURRENT"
#define ID_DC2_CURRENT		"ID_DC2_CURRENT"
#define ID_BATT_CURRENT		"ID_BATT_CURRENT"
#define ID_MAINS_L1VOLT		"ID_MAINS_L1VOLT"
#define ID_MAINS_L2VOLT		"ID_MAINS_L2VOLT"
#define ID_MAINS_L3VOLT		"ID_MAINS_L3VOLT"
#define ID_BATT_STATE		"ID_BATT_STATE"
#define ID_TEMP_H2		"ID_TEMP_H2"
#define ID_TEMP_H1		"ID_TEMP_H1"
#define ID_TEMP_L1		"ID_TEMP_L1"
#define ID_AMB_TEMP		"ID_AMB_TEMP"
#define ID_SYS_STATE		"ID_SYS_STATE"
#define ID_H2_VOLTAGE		"ID_H2_VOLTAGE"
#define ID_H1_VOLTAGE		"ID_H1_VOLTAGE"
#define ID_L1_VOLTAGE		"ID_L1_VOLTAGE"
#define ID_L2_VOLTAGE		"ID_L2_VOLTAGE"
#define ID_SYS_VOLT		"ID_SYS_VOLT"
#define ID_SYS_CURR		"ID_SYS_CURR"
#define ID_SYS_CURR_PERCEN	"ID_SYS_CURR_PERCEN"
#define ID_DG_EXIST		"ID_DG_EXIST"
#define ID_MPPT_EXIST		"ID_MPPT_EXIST"
#define ID_AC_EXIST		"ID_AC_EXIST"
#define ID_SMIO_EXIST		"ID_SMIO_EXIST"
#define ID_SOLAR_EXIST		"ID_SOLAR_EXIST"
#define ID_CONVERTER_EXIST	"ID_CONVERTER_EXIST"
#define ID_SYS_NAME		"ID_SYS_NAME"
#define ID_REC_NUM		"ID_REC_NUM"
#define ID_RECS1_NUM		"ID_RECS1_NUM"
#define ID_RECS2_NUM		"ID_RECS2_NUM"
#define ID_RECS3_NUM		"ID_RECS3_NUM"
#define ID_CONVERTER_NUM	"ID_CONVERTER_NUM"
#define ID_SOLAR_NUM		"ID_SOLAR_NUM"
#define ID_PRODUCT_MODEL	"ID_PRODUCT_MODEL"
#define ID_SERIAL_NUM		"ID_SERIAL_NUM"
#define ID_HARD_VER		"ID_HARD_VER"
#define ID_SOFT_VER		"ID_SOFT_VER"
#define ID_CONFIG_VER		"ID_CONFIG_VER"
#define ID_PROTOCOL_TYPE	"ID_PROTOCOL_TYPE"
//#define ID_PRESENT_TIME		"ID_PRESENT_TIME"
#define ID_CONVERTER_NUM1	"ID_CONVERTER_NUM1"
#define ID_SOLAR_NUM1		"ID_SOLAR_NUM1"
#define ID_HIGH_LOAD1		"ID_HIGH_LOAD1"
#define ID_HIGH_LOAD2		"ID_HIGH_LOAD2"
#define ID_VOLT_LEVEL		"ID_VOLT_LEVEL"
#define ID_H2_VOLTAGE_24V	"ID_H2_VOLTAGE_24V"
#define ID_H1_VOLTAGE_24V	"ID_H1_VOLTAGE_24V"
#define ID_L1_VOLTAGE_24V	"ID_L1_VOLTAGE_24V"
#define ID_L2_VOLTAGE_24V	"ID_L2_VOLTAGE_24V"
#define ID_TIME_FORMAT		"ID_TIME_FORMAT"
#define ID_BATT_CAPACITY	"ID_BATT_CAPACITY"
#define ID_400V_CONVERTER	"ID_400V_CONVERTER"
#define ID_CONVERTER_CURR	"ID_CONVERTER_CURR"
#define ID_DC_CURRENT		"ID_DC_CURRENT"
#define ID_SOLAR_POWER		"ID_SOLAR_POWER"
#define ID_RECT_POWER		"ID_RECT_POWER"
#define ID_DC2_POWER		"ID_DC2_POWER"
#define ID_CONVERTER_POWER	"ID_CONVERTER_POWER"
#define ID_SITE_NAME		"ID_SITE_NAME"
#define ID_SITE_LOCATION	"ID_SITE_LOCATION"
#define ID_SITE_DESC		"ID_SITE_DESC"
#define ID_HYBRID_EXIST		"ID_HYBRID_EXIST"
#define ID_UDEF_SAMP_EXIST	"ID_UDEF_SAMP_EXIST"
#define CONSUM_MAP_ENABLE	"CONSUM_MAP_ENABLE"
#define ENERGY_SOURCES_ENABLE	"ENERGY_SOURCES_ENABLE"
#define ID_SOURCE_CURRENT	"ID_SOURCE_CURRENT"
#define ID_AMB_TEMP_UNIT	"ID_AMB_TEMP_UNIT"
#define ID_VERIZON_DIS	"ID_VERIZON_DIS"
#define ID_ALARM_SOUND	    "ID_ALARM_SOUND"

#define COSTOM_INPUT_STATUS	    "COSTOM_INPUT_STATUS"

char *pbuf = NULL;

// equip

//#define ID_RECT_GROUP		2
// signal
#define ID_LOAD_CURRENT		2
//#define ID_AC_PHASE		24
#define ID_AMBIENT_HI1		209
#define ID_AMBIENT_HI2		219
#define ALARM_SOUND_ACTIVATION	251

    TRACE_WEB_USER("Begin to exec Web_MakeHomeDataFile\n");

	int					iRst;


    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    //int	iVarSubID;
    int	iError = 0;
    //SIG_BASIC_VALUE* pSigValue;
    char szBuf[80];
    //int iEquipID, iSignalID;
    ACU_PRODUCT_INFO sAcuProductInfo;
    FILE		*fp;
    int i;
    char	*szMakeVar = NULL;
    //time_t now;
    float ftemp, floadCurrent;
    int	iBufLen = 0,iBufLen1;
    SIG_BASIC_VALUE* pSigValue;
    int	iVarSubID = 0;
    int iTimeOut = 0;
    int iValueFormat = 1, iValueFormat1 = 1;
    char szformat[32];
	float fTempMax = 75;
	static	SIG_BASIC_VALUE*		s_pSigValueVerizon = NULL;
	//int iVarSubID, iError, iBufLen, iTimeOut = 0;

	DXI_GetTempSwitch(TEMP_VALUE,
			fTempMax,
			&fTempMax,
			NULL
			);
     /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeHomeDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_INDEX);*/
    iReturn = LoadHtmlFile(WEB_DATA_INDEX, &pHtml);
    if(iReturn > 0)
    {
	//TRACE_WEB_USER("Begin to exec makeHomeDataFile1\n");
	//TRACE_WEB_USER("gs_pWebSigInfo[0].iNumber is %d\n", gs_pWebSigInfo[0].iNumber);
	for(i = 0; i < gs_pWebSigInfo[INDEX_SEQ].iNumber; i++)
	{
	    //TRACE_WEB_USER("Begin to exec Web_MakeHomeDataFile[%d]\n", i);
	    szMakeVar = MakeVarField(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].szResourceID);
	    if(szMakeVar != NULL)
	    {
		//TRACE_WEB_USER("szResourceID is %s\n", gs_pWebSigInfo[0].stWebPrivate[i].szResourceID);
		//TRACE_WEB_USER("szMakeVar is %s\n", szMakeVar);
		if((gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iEquipID == ID_SYSTEM) && 
		    (gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].pSigValue->ucSigType == SIG_TYPE_SETTING) && 
		    ((((SET_SIG_VALUE *)gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].pSigValue)->pStdSig->iSigID == ID_AMBIENT_HI1) 
		    || (((SET_SIG_VALUE *)gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].pSigValue)->pStdSig->iSigID == ID_AMBIENT_HI2)))
		{
			memset(szBuf, 0, sizeof(szBuf));
			ftemp = gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].pSigValue->varValue.fValue;

			gs_iTempUnitSwitch = DXI_GetTempSwitch(TEMP_VALUE,
				ftemp,
				&ftemp,
				NULL
				); 
			if(ftemp>fTempMax)
				ftemp = fTempMax;
			snprintf(szBuf, sizeof(szBuf), "%.0f", ftemp);
		    ReplaceString(&pHtml, szMakeVar, szBuf);
		}
		else
		{
		    if((gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iEquipID == ID_BATTERY_GROUP) && 
			(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iSignalType == SIG_TYPE_SAMPLING) &&
			(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iSignalID == 2)) //ID_BATT_CURRENT
		    {
			iValueFormat = Web_TransferFormatToInt(((SAMPLE_SIG_VALUE *)(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].pSigValue))->pStdSig->szValueDisplayFmt);
		    }
		    if((gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iEquipID == ID_SYSTEM) && 
			(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iSignalType == SIG_TYPE_SAMPLING) &&
			(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iSignalID == 2)) //ID_SYS_CURR
		    {
			iValueFormat1 = Web_TransferFormatToInt(((SAMPLE_SIG_VALUE *)(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].pSigValue))->pStdSig->szValueDisplayFmt);
		    }
		    Web_FillString(&pHtml, szMakeVar, gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].pSigValue, gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iEquipID,
			gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].bValid);
		}
		DELETE(szMakeVar);
		szMakeVar = NULL;
	    }
	}

	memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
	iBufLen = sizeof(sAcuProductInfo);
	iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
	    ACU_PRODUCT_INFO_GET, 
	    0, 
	    &iBufLen,
	    &(sAcuProductInfo),
	    0);
	if(iError == ERR_DXI_OK)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_PRODUCT_MODEL), sAcuProductInfo.szPartNumber);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SERIAL_NUM), sAcuProductInfo.szACUSerialNo);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HARD_VER), sAcuProductInfo.szHWRevision);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOFT_VER), sAcuProductInfo.szSWRevision);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG_VER), sAcuProductInfo.szCfgFileVersion);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_PRODUCT_MODEL), "");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SERIAL_NUM), "");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HARD_VER), "");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOFT_VER), "");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG_VER), "");
	    TRACE_WEB_USER("Get sAcuProductInfo error\n");
	}
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[\"%s\",\"%s\"]", g_SiteInfo.langDescription.pFullName[0], g_SiteInfo.langDescription.pFullName[1]);
	//TRACE_WEB_USER("szBuf is %s\n", szBuf);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SYS_NAME), szBuf);

	/*memset(szBuf, 0, sizeof(szBuf));
	sprintf(szBuf, "%d", gs_uiAllRectifierNumber);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_REC_NUM), szBuf);*/

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_uiRectifierS1Number);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECS1_NUM), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_uiRectifierS2Number);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECS2_NUM), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_uiRectifierS3Number);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECS3_NUM), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_uiConverterNumber);
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONVERTER_NUM), szBuf);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONVERTER_NUM1), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_uiSolarNumber);
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOLAR_NUM), szBuf);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOLAR_NUM1), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%ld", gs_lProtocolType);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_PROTOCOL_TYPE), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_iHybrid);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HYBRID_EXIST), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[%d]", gs_iTempUnitSwitch);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_AMB_TEMP_UNIT), szBuf);

	szMakeVar = Web_MakeSourceDataBuf();
	if(szMakeVar != NULL)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOURCE_CURRENT), szMakeVar);
	}
	else
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SOURCE_CURRENT), "[]");
	}
	/*now = time(NULL);
	memset(szBuf, 0, sizeof(szBuf));
	sprintf(szBuf, "%ld", now);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_PRESENT_TIME), szBuf);*/

	// changed by Frank Wu,20131213,7/8, for keep the same used rated current percent between WEB and LCD
	g_SiteInfo.pstWebLcdShareData->fRatedCurrPercent = 0;

	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ID_LOAD_CURRENT);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    ID_SYSTEM,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    iTimeOut);
	if(iError == ERR_DXI_OK)
	{
	    floadCurrent = pSigValue->varValue.fValue;
	    ftemp = CalSysPower();
	    TRACE_WEB_USER("The total rate current is %f\n", ftemp);
	    if(!(FLOAT_EQUAL0(ftemp)))
	    {
		floadCurrent = (floadCurrent * 100) / ftemp;
		memset(szBuf, 0, sizeof(szBuf));
		memset(szformat, 0, sizeof(szformat));
		snprintf(szformat, sizeof(szformat), "[\"%%.%df\",0,0,0]", iValueFormat1);// for example "["%.1f",0,0,0]"
		//printf("szformat is %s\n", szformat);
		//snprintf(szBuf, sizeof(szBuf), "[\"%.1f\",0,0,0]", floadCurrent);
		snprintf(szBuf, sizeof(szBuf), szformat, floadCurrent);
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SYS_CURR_PERCEN), szBuf);

			// changed by Frank Wu,20131213,8/8, for keep the same used rated current percent between WEB and LCD
			g_SiteInfo.pstWebLcdShareData->fRatedCurrPercent = floadCurrent;
	    }
	    else
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SYS_CURR_PERCEN), "[\"0\",0,1,0]");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SYS_CURR_PERCEN), "[\"0\",0,1,0]");
	}

	//memset(szBuf, 0, sizeof(szBuf));
	//sprintf(szBuf, "[\"%s\",\"%s\"]", g_SiteInfo.langSiteName.pFullName[0], g_SiteInfo.langSiteName.pFullName[0]);//here just display English site name
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITE_NAME), szBuf);

	/*memset(szBuf, 0, sizeof(szBuf));
	sprintf(szBuf, "[\"%s\",\"%s\"]", g_SiteInfo.langSiteLocation.pFullName[0], g_SiteInfo.langSiteLocation.pFullName[1]);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITE_LOCATION), szBuf);*/

	/*memset(szBuf, 0, sizeof(szBuf));
	sprintf(szBuf, "[\"%s\",\"%s\"]", g_SiteInfo.langDescription.pFullName[0], g_SiteInfo.langDescription.pFullName[1]);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITE_DESC), szBuf);*/

	ftemp = CalRectAllCurrent();
	memset(szBuf, 0, sizeof(szBuf));
	memset(szformat, 0, sizeof(szformat));
	snprintf(szformat, sizeof(szformat), "[\"%%.%df\",0,0,0]", iValueFormat);// for example "["%.1f",0,0,0]"
	//snprintf(szBuf, sizeof(szBuf), "[\"%.1f\",0,0,0]", ftemp);
	snprintf(szBuf, sizeof(szBuf), szformat, ftemp);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECT_CURRENT), szBuf);

	ftemp = CalRectAllPower();
	memset(szBuf, 0, sizeof(szBuf));
	//snprintf(szBuf, sizeof(szBuf), "[\"%.1f\",0,0,0]", ftemp);
	snprintf(szBuf, sizeof(szBuf), szformat, ftemp);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECT_POWER), szBuf);

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[CONFIG].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_UDEF_SAMP_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_UDEF_SAMP_EXIST), "1");
	}

	/*if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[ENERGY_SOUR].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ENERGY_SOURCES_ENABLE), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ENERGY_SOURCES_ENABLE), "1");
	}*/

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[CONSUM_MAP].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(CONSUM_MAP_ENABLE), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(CONSUM_MAP_ENABLE), "1");
	}
	//add for VERIZON feature.
	if(s_pSigValueVerizon == NULL)
	{
		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 700);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			ID_SYSTEM,
			iVarSubID,
			&iBufLen1,
			(void *)&s_pSigValueVerizon,
			iTimeOut);
	}
	if(s_pSigValueVerizon != NULL)
	{
		memset(gs_szBuffer, 0, BUFFER_LEN);
		snprintf(gs_szBuffer, 5, "[%d]", s_pSigValueVerizon->varValue.enumValue);
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_VERIZON_DIS), gs_szBuffer);
	}
	else
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_VERIZON_DIS), "[0]");
	}

	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, ALARM_SOUND_ACTIVATION);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    1,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigValue,
	    0);
	if(iError == ERR_DXI_OK)
	{
	    memset(szBuf, 0, sizeof(szBuf));
	    snprintf(szBuf, sizeof(szBuf), "%ld", pSigValue->varValue.enumValue);
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_SOUND), szBuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ALARM_SOUND), "0");
	}

/*
	pbuf = Web_MakeSingleCostom_inputs_status(SMDUE_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(COSTOM_INPUT_STATUS), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(COSTOM_INPUT_STATUS), "[]");
	}
*/


	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					1,
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 710),
					&iBufLen,
					&pSigValue,
					0);
	if(0==pSigValue->varValue.enumValue)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(COSTOM_INPUT_STATUS), "0");
	}
	else if(1==pSigValue->varValue.enumValue)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(COSTOM_INPUT_STATUS), "1");
	}






	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_INDEX_VAR);*/
	if((fp = fopen(WEB_DATA_INDEX_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{
	    /*if(fp == NULL)
	    {
	    TRACE_WEB_USER("*******************Can't open file[%s]**************************\n", szFile);
	    }*/
	}
	//TRACE_WEB_USER("pHtml is %s\n", pHtml);
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeHomeDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_GetSignalNameForRect
* PURPOSE  :  Get the signal name from the WEB_GENERAL_SIGNAL_INFO pointer for rectifiers
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  pstWebPrivate: the signal pointer
		      szBuf: the buffer used to save the name
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-03-12
*==========================================================================*/
void Web_GetSignalNameForRect(WEB_GENERAL_RECT_SIGNAL_INFO *pstWebPrivate, char *szBuf, int iBufLength)
{
    int iFileLen = 0;
    BOOL bdisplay = 0, bdisplay1;

    // if the signal is with no pointer
    if(pstWebPrivate->bSingleValid == FALSE)
    {
	iFileLen += snprintf(szBuf + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\",0,0],");
    }
    else
    {
	bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->bv);
	bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig), DISPLAY_WEB);
	if((bdisplay == TRUE) && (bdisplay1 == TRUE))
	{
		iFileLen += snprintf(szBuf + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\",0,%d],",
		    ((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[0],
		    ((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[1],
		    ((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->iSigID);
	}
	else
	{}
    }
}

/*==========================================================================*
* FUNCTION :  Web_GetSignalValue
* PURPOSE  :  Get the signal value
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  i == -1 means this is the group equip signal
* szBuf: the buffer used to save the name
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-03-12
*==========================================================================*/
void Web_GetSignalValue(SIG_BASIC_VALUE *pSigValue, BOOL bSingleValid, int iSignalID, int itype1, int i, char *szBuf, int iBufLength, int iEquipID)
{
#define ID_DC_STATUS		1
#define ID_COMM_STATUS		99
#define ID_COMM_STATUS_RECT	10
#define ID_CONVERTER_SWITCH	15
#define ID_RECT_SN		8
#define ID_RECT_HI_SN		26

    int iBufLen = 0;
    unsigned char	ucSigType;
    BOOL bdisplay, bdisplay1;
    SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
    SET_SIG_VALUE     *pSettingSigValue = NULL;
    int iValueFormat;
    char szformat[32];
    int		iRectStartDeviceID = DXI_GetFirstRectDeviceID();
    PRODUCT_INFO	piProductInfo;
    int itemp;
    int iSigID;
	float fTempValue = 0;
	char cTempUnit[8];

    if(bSingleValid == FALSE)
    {
	TRACE_WEB_USER("The signal not exist\n");
	iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"---\",0,1,1],");
    }
    else
    {
	//TRACE_WEB_USER("The signal exist\n");
	ucSigType = pSigValue->ucSigType;
	if(ucSigType == SIG_TYPE_SAMPLING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(pSigValue))->pStdSig), DISPLAY_WEB);
	    pSampleSigValue = (SAMPLE_SIG_VALUE *)(pSigValue);
	}
	else if(ucSigType == SIG_TYPE_SETTING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(pSigValue))->pStdSig), DISPLAY_WEB);
	    pSettingSigValue = (SET_SIG_VALUE *)(pSigValue);
	}
	else
	{
	    bdisplay = FALSE;
	}
	if((bdisplay == TRUE) && (bdisplay1 == TRUE))
	{
	    if(pSigValue->ucType == VAR_LONG)
	    {
			iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld", pSigValue->varValue.lValue);
			if(ucSigType == SIG_TYPE_SAMPLING)
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
			}
			else if(ucSigType == SIG_TYPE_SETTING)
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
			}
			else
			{}
	    }
	    else if(pSigValue->ucType == VAR_FLOAT)
	    {
			if(ucSigType == SIG_TYPE_SAMPLING)
			{
				iValueFormat = Web_TransferFormatToInt(pSampleSigValue->pStdSig->szValueDisplayFmt);
			}
			else
			{
				iValueFormat = Web_TransferFormatToInt(pSettingSigValue->pStdSig->szValueDisplayFmt);
			}
			//if(IS_TEMP_SIGNAL(iEquipID,
			//		ucSigType,
			//		iSignalID))
			if(DXI_isTempSignal(pSigValue))
			{
				DXI_GetTempSwitch(TEMP_VALUE,
					pSigValue->varValue.fValue,
					&fTempValue,
					cTempUnit);
			}
			else
			{
				fTempValue = pSigValue->varValue.fValue;
				if(ucSigType == SIG_TYPE_SAMPLING)
				{
					snprintf(cTempUnit,sizeof(szformat),"%s",
						pSampleSigValue->pStdSig->szSigUnit);
				}
				else if(ucSigType == SIG_TYPE_SETTING)
				{
					snprintf(cTempUnit,sizeof(szformat),"%s",
						pSettingSigValue->pStdSig->szSigUnit);
				}
				else
				{
					
				}
			}
			snprintf(szformat, sizeof(szformat), "[\"%%.%df", iValueFormat);// for example "["%.1f"
			//TRACE_WEB_USER("szformat is %s\n", szformat);
			iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), szformat, fTempValue);
			//if(ucSigType == SIG_TYPE_SAMPLING)
			//{
			//	iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
			//}
			//else if(ucSigType == SIG_TYPE_SETTING)
			//{
			//	iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
			//}
			//else
			//{}
			iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", cTempUnit);
	    }
	    else if(pSigValue->ucType == VAR_UNSIGNED_LONG)
	    {//specially for the SN and high SN
			/*if((gs_pWebSigInfo[itype].stWebPrivate[j + igroupNum].iSignalID == ID_RECT_SN) || 
			(gs_pWebSigInfo[itype].stWebPrivate[j + igroupNum].iSignalID == ID_RECT_HI_SN))
			{
			iBufLen += sprintf(szBuf + iBufLen, "[\"%08X", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].pSigValue->varValue.ulValue);
			}*/
			if((iSignalID == ID_RECT_SN) && (itype1 == RECT_GROUP_SEQ) && (i != -1))
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", g_SiteInfo.stCANRectProductInfo[i].szSerialNumber);
			}
			else if((iSignalID == ID_RECT_SN) && (itype1 == RECTS1_GROUP_SEQ) && (i != -1))
			{
				if(DXI_GetPIByDeviceID((i + iRectStartDeviceID + 110 + 20), &piProductInfo) == ERR_DXI_OK)
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", 
					((Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A"));
				}
				else
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", "N/A");
				}
			}
			else if((iSignalID == ID_RECT_SN) && (itype1 == RECTS2_GROUP_SEQ) && (i != -1))
			{
				if(DXI_GetPIByDeviceID((i + iRectStartDeviceID + 170 + 20), &piProductInfo) == ERR_DXI_OK)
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", 
					((Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A"));
				}
				else
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", "N/A");
				}
			}
			else if((iSignalID == ID_RECT_SN) && (itype1 == RECTS3_GROUP_SEQ) && (i != -1))
			{
				if(DXI_GetPIByDeviceID((i + iRectStartDeviceID + 230 + 20), &piProductInfo) == ERR_DXI_OK)
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", 
					((Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A"));
				}
				else
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", "N/A");
				}
			}
			else if((iSignalID == ID_RECT_SN) && (itype1 == CONVERTER_GROUP_SEQ) && (i != -1))
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", g_SiteInfo.stCANConverterProductInfo[i].szSerialNumber);
			}
			else if((iSignalID == ID_RECT_SN) && (itype1 == SOLAR_GROUP_SEQ) && (i != -1))
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%s", g_SiteInfo.stCANMpptProductInfo[i].szSerialNumber);
			}
			else
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld", pSigValue->varValue.ulValue);
			}
			if(ucSigType == SIG_TYPE_SAMPLING)
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
			}
			else if(ucSigType == SIG_TYPE_SETTING)
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
			}
			else
			{}
	    }
	    else if(pSigValue->ucType == VAR_DATE_TIME)
	    {
			iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld\",", pSigValue->varValue.dtValue);
	    }
	    else if(pSigValue->ucType == VAR_ENUM)
	    {
			itemp = pSigValue->varValue.enumValue;
			if(ucSigType == SIG_TYPE_SAMPLING)
			{
				iSigID = ((SAMPLE_SIG_VALUE *)(pSigValue))->pStdSig->iSigID;
				if((((iSigID == ID_COMM_STATUS_RECT) || (iSigID == ID_DC_STATUS)) && (itype1 == RECT_GROUP_SEQ) && (i != -1)) ||
				(((iSigID == ID_COMM_STATUS_RECT) || (iSigID == ID_DC_STATUS)) && (itype1 == RECTS1_GROUP_SEQ) && (i != -1)) ||
				(((iSigID == ID_COMM_STATUS_RECT) || (iSigID == ID_DC_STATUS)) && (itype1 == RECTS2_GROUP_SEQ) && (i != -1)) ||
				(((iSigID == ID_COMM_STATUS_RECT) || (iSigID == ID_DC_STATUS)) && (itype1 == RECTS3_GROUP_SEQ) && (i != -1)) ||
				((iSigID == ID_COMM_STATUS) && (itype1 == CONVERTER_GROUP_SEQ) && (i != -1)) ||
				(((iSigID == ID_COMM_STATUS) || (iSigID == ID_DC_STATUS)) && (itype1 == SOLAR_GROUP_SEQ) && (i != -1)))
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[%d,", itemp);
				}
				else if((iSigID == ID_CONVERTER_SWITCH) && (itype1 == CONVERTER_GROUP_SEQ) && (i != -1))	    // specially for converter switch status
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[%d,", (!itemp));
				}
				else
				{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[[\"%s\",\"%s\"],", pSampleSigValue->pStdSig->pStateText[itemp]->pFullName[0],
					pSampleSigValue->pStdSig->pStateText[itemp]->pFullName[1]);
				}
			}
			else if(ucSigType == SIG_TYPE_SETTING)
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[[\"%s\",\"%s\"],", pSettingSigValue->pStdSig->pStateText[itemp]->pFullName[0],
				pSettingSigValue->pStdSig->pStateText[itemp]->pFullName[1]);
			}
			else
			{}
	    }
	    else
	    {
			iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"\",");
	    }
	    if(ucSigType == SIG_TYPE_SAMPLING)
	    {
		if(pSampleSigValue->sv.iRelatedAlarmLevel > 0) //here add the value state---alarm state
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
		}
	    }
	    else
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
	    }
	    if(ucSigType == SIG_TYPE_SAMPLING)
	    {
		if(SIG_VALUE_IS_VALID(&pSampleSigValue->bv))
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
		}
		if(SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv))
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0],");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
		}
	    }
	    else
	    {
		if(SIG_VALUE_IS_VALID(&pSettingSigValue->bv))
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
		}
		if(SIG_VALUE_IS_CONFIGURED(&pSettingSigValue->bv))
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0],");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
		}
	    }
	}
    }
}


/*==========================================================================*
* FUNCTION :  Web_MakeConfigDataFile
* PURPOSE  :  To make the json file <data.system_config.html>
* CALLS    :
* CALLED BY:
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :
* CREATOR  :  Zhao Zicheng               DATE: 2014-3-19
*==========================================================================*/
void Web_MakeConfigDataFile(void)
{
#define ID_GROUP_SIGNAL	"ID_GROUP_SIGNAL"

    char szFile[128];
    char *pHtml = NULL;
    int	 iReturn = 0;
    char szBuf[128];
    FILE	*fp;
    //int j, k;
    int i;
    //char	*szMakeVar = NULL;
    char *szTemp = NULL;
    //int iSignalNum;
    int iFileLen = 0;
    int		iBufLength;
    int len1, len2;

    TRACE_WEB_USER("Begin to exec Web_MakeConfigDataFile\n");
    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
	AppLogOut("Web_MakeConfigDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return;
    }*/
    snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_CONFIG);
    iReturn = LoadHtmlFile(szFile, &pHtml);
    if(iReturn > 0)
    {
	//szTemp = NEW(char, (80 * gs_pWebSigInfo[CONFIG_SEQ].iNumber + LENGTH_OF_OTHER_CHAR));// every signal we use 80 length
	//if(szTemp == NULL)
	//{
	//    AppLogOut("Web_MakeConfigDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	//    /*if(szFile != NULL)
	//    {
	//	DELETE(szFile);
	//	szFile = NULL;
	//    }*/
	//    return;
	//}
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iBufLength = 80 * gs_pWebSigInfo[CONFIG_SEQ].iNumber + LENGTH_OF_OTHER_CHAR;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	for(i = 0; i < gs_pWebSigInfo[CONFIG_SEQ].iNumber; i++)
	{
	    len1 = 0;
	    len2 = 0;
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	    memset(szBuf, 0, sizeof(szBuf));
	    Web_GetSignalName(&(gs_pWebSigInfo[CONFIG_SEQ].stWebPrivate[i]), szBuf, sizeof(szBuf));
	    len1 = strlen((const char*)szBuf);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	    memset(szBuf, 0, sizeof(szBuf));
	    Web_GetSignalValue(gs_pWebSigInfo[CONFIG_SEQ].stWebPrivate[i].pSigValue, gs_pWebSigInfo[CONFIG_SEQ].stWebPrivate[i].bValid,
		gs_pWebSigInfo[CONFIG_SEQ].stWebPrivate[i].iSignalID, CONFIG_SEQ, 0, szBuf, sizeof(szBuf),gs_pWebSigInfo[CONFIG_SEQ].stWebPrivate[i].iEquipID);
	    len2 = strlen((const char*)szBuf);
	    //TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
	    //TRACE_WEB_USER("[Web_MakeRectDataFile] szBuf is %s\n", szBuf);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	    if((len1 > 0) && (len2 > 0))
	    {
		if(iFileLen >= 1)
		{
		    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	    }
	    else
	    {
		iFileLen = iFileLen - 1;
	    }
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_GROUP_SIGNAL), szTemp);
	//TRACE_WEB_USER("[Web_MakeRectDataFile] szTemp is %s\n", szTemp);
	/*DELETE(szTemp);
	szTemp = NULL;*/

	memset(szFile, 0, 128);
	snprintf(szFile, 128, "%s", WEB_DATA_CONFIG_VAR);
	if(szFile != NULL && (fp = fopen(szFile,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
	DELETE(szFile);
	szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeConfigDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeRectDataFile
* PURPOSE  :  To make the json file <data.system_rectifier.html> or <data.system_converter.html> or <data.system_solar.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  iRectifierNum: the number of rectifier or converter or MPPT
      itype: indicate the kind of rectifier group
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY  :   Zhao Zicheng               DATE: 2014-03-07 to make the sample signals configurable
*==========================================================================*/
void Web_MakeRectDataFile(unsigned int iRectifierNum, int itype)
{
#define ID_GROUP_SIGNAL	"ID_GROUP_SIGNAL"
#define ID_HEADLINE "ID_HEADLINE"
#define ID_SINGLE_DATA "ID_SINGLE_DATA"

    TRACE_WEB_USER("Begin to exec Web_MakeRectDataFile[%d]\n", itype);

    char szFile[128];
    char *pHtml = NULL;
    int	 iReturn = 0;
    char szBuf[128];
    FILE		*fp;
    int j, k;
    int i;
    char	*szMakeVar = NULL;
    char *szTemp = NULL;
    int iSignalNum;
    int iFileLen = 0;
    //int iBufLen = 0;
    int iSingleRectSignalNum;
    int iseqNum; // indicate the array gs_pWebRectSigInfo
    int iGroupNum;
    int	iBufLength;
    int len1, len2;
    int iChartNum, iTableBegin, iGroupNumMax;

    //szFile = NEW(char, 128);
    /*if(szFile == NULL)
    {
	AppLogOut("Web_MakeRectDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return;
    }*/
    if(itype == RECT_GROUP_SEQ)
    {
	iChartNum = 4;
	iTableBegin = 2;
	iGroupNumMax = 8;
	iseqNum = 0;
	snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_RECT);
    }
    else if(itype == RECTS1_GROUP_SEQ)
    {
	iChartNum = 2;
	iTableBegin = 0;
	iGroupNumMax = 6;
	iseqNum = 1;
	snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_RECTS1);
    }
    else if(itype == RECTS2_GROUP_SEQ)
    {
	iChartNum = 2;
	iTableBegin = 0;
	iGroupNumMax = 6;
	iseqNum = 2;
	snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_RECTS2);
    }
    else if(itype == RECTS3_GROUP_SEQ)
    {
	iChartNum = 2;
	iTableBegin = 0;
	iGroupNumMax = 6;
	iseqNum = 3;
	snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_RECTS3);
    }
    else if(itype == CONVERTER_GROUP_SEQ)
    {
	iChartNum = 2;
	iTableBegin = 0;
	iGroupNumMax = 6;
	iseqNum = 4;
	snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_CONVERTER);
    }
    else
    {
	iChartNum = 2;
	iTableBegin = 0;
	iGroupNumMax = 6;
	iseqNum = 5;
	snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_SOLAR);
    }

    //check the pointer valid begin
    if(gs_pWebRectSigInfo[iseqNum] == NULL)
    {
	return;
    }
    if((gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo == NULL) || (gs_pWebRectSigInfo[iseqNum]->stWebPrivate == NULL))
    {
	return;
    }
    //check the pointer valid end

    iReturn = LoadHtmlFile(szFile, &pHtml);
    if(iReturn > 0)
    {
	// get the group signals
	for(i = 0; i < iChartNum; i++)
	{
	    szMakeVar = MakeVarField(gs_pWebSigInfo[itype].stWebPrivate[i].szResourceID);
	    if(szMakeVar != NULL)
	    {
		/*TRACE_WEB_USER("Group signal i = %d\n", i);
		TRACE_WEB_USER("szResourceID is %s\n", gs_pWebSigInfo[itype].stWebPrivate[i].szResourceID);
		TRACE_WEB_USER("szMakeVar is %s\n", szMakeVar);*/
		Web_FillString(&pHtml, szMakeVar, gs_pWebSigInfo[itype].stWebPrivate[i].pSigValue, gs_pWebSigInfo[itype].stWebPrivate[i].iEquipID,
		    gs_pWebSigInfo[itype].stWebPrivate[i].bValid);
		DELETE(szMakeVar);
		szMakeVar = NULL;
	    }
	}
	iGroupNum = gs_pWebSigInfo[itype].iNumber;
	if(iGroupNum > iGroupNumMax)
	{
	    iGroupNum = iGroupNumMax;
	}
	//szTemp = NEW(char, (80 * iGroupNum + LENGTH_OF_OTHER_CHAR));
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iBufLength = 80 * iGroupNum + LENGTH_OF_OTHER_CHAR;
	//if(szTemp == NULL)
	//{
	//    AppLogOut("Web_MakeRectDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	//    /*if(szFile != NULL)
	//    {
	//	DELETE(szFile);
	//	szFile = NULL;
	//    }*/
	//    return;
	//}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	for(i = iTableBegin; i < iGroupNum; i++)
	{
	    len1 = 0;
	    len2 = 0;
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	    memset(szBuf, 0, sizeof(szBuf));
	    Web_GetSignalName(&(gs_pWebSigInfo[itype].stWebPrivate[i]), szBuf, sizeof(szBuf));
	    len1 = strlen((const char*)szBuf);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	    memset(szBuf, 0, sizeof(szBuf));
	    Web_GetSignalValue(gs_pWebSigInfo[itype].stWebPrivate[i].pSigValue, gs_pWebSigInfo[itype].stWebPrivate[i].bValid,
			gs_pWebSigInfo[itype].stWebPrivate[i].iSignalID, itype, -1, szBuf, sizeof(szBuf),gs_pWebSigInfo[itype].stWebPrivate[i].iEquipID);
	    len2 = strlen((const char*)szBuf);
	    //TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
	    //TRACE_WEB_USER("[Web_MakeRectDataFile] szBuf is %s\n", szBuf);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	    if((len1 > 0) && (len2 > 0))
	    {
		if(iFileLen >= 1)
		{
		    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	    }
	    else
	    {
		iFileLen = iFileLen - 1;
	    }
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_GROUP_SIGNAL), szTemp);
	//TRACE_WEB_USER("[Web_MakeRectDataFile] szTemp is %s\n", szTemp);
	/*DELETE(szTemp);
	szTemp = NULL;*/

	//get the signal name
	iSignalNum = gs_pWebRectSigInfo[iseqNum]->iOneEquipNum;
	TRACE_WEB_USER("iSignalNum is %d\n", iSignalNum);
	if((iSignalNum > 0) && (iRectifierNum > 0))
	{
	    memset(gs_szBuffer, 0, BUFFER_LEN);
	    szTemp = gs_szBuffer;
	    iBufLength = 80 * iSignalNum + LENGTH_OF_OTHER_CHAR;
	    iFileLen = 0;
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	    for(i = 0; i < iSignalNum; i++)
	    {	
		TRACE_WEB_USER("signal name i = %d\n", i);
		memset(szBuf, 0, sizeof(szBuf));
		Web_GetSignalNameForRect(&(gs_pWebRectSigInfo[iseqNum]->stWebPrivate[i]), szBuf, sizeof(szBuf));
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	    }
	    if(iFileLen >= 1)
	    {
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	    }
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HEADLINE), szTemp);
	    //TRACE_WEB_USER("[Web_MakeRectDataFile] szTemp is %s\n", szTemp);
	    /*DELETE(szTemp);
	    szTemp = NULL;*/
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HEADLINE), "[]");
	}
	//get the signal value
	TRACE_WEB_USER("iRectifierNum is %d\n", iRectifierNum);
	if((iRectifierNum > 0) && ((iSignalNum) > 0))
	{
	    k = 0;
	    iFileLen = 0;
	    iSingleRectSignalNum = iRectifierNum * iSignalNum;
	    TRACE_WEB_USER("iSingleRectSignalNum is %d\n", iSingleRectSignalNum);
	    //szTemp = NEW(char, (90 * iSingleRectSignalNum + LENGTH_OF_OTHER_CHAR));
	    memset(gs_szBuffer, 0, BUFFER_LEN);
	    szTemp = gs_szBuffer;
	    iBufLength = 90 * iSingleRectSignalNum + LENGTH_OF_OTHER_CHAR;
	 //   if(szTemp == NULL)
	 //   {
		//AppLogOut("Web_MakeRectDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
		//TRACE_WEB_USER("NEW szTemp fail\n");
		///*if(szFile != NULL)
		//{
		//    DELETE(szFile);
		//    szFile = NULL;
		//}*/
		//return;
	 //   }
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	    for(i = 0; i < iRectifierNum; i++)
	    {
		TRACE_WEB_USER("signal value i = %d\n", i);
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
		if(gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo[i].bSingleValid == FALSE)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
		}
		else
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],", gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[0],
			gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[1]);
		}
		for(j = 0; j < (iSignalNum); j++)
		{   //TRACE_WEB_USER("signal value j = %d\n", j);
		    memset(szBuf, 0, sizeof(szBuf));
		    Web_GetSignalValue(gs_pWebRectSigInfo[iseqNum]->stWebPrivate[k].pSigValue, gs_pWebRectSigInfo[iseqNum]->stWebPrivate[k].bSingleValid, 
			((SAMPLE_SIG_VALUE *)(gs_pWebRectSigInfo[iseqNum]->stWebPrivate[k].pSigValue))->pStdSig->iSigID, itype, i, szBuf, sizeof(szBuf),0);
		    //TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
		    //TRACE_WEB_USER("[Web_MakeRectDataFile] szBuf is %s\n", szBuf);
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
		    k++;
		}
		if(iFileLen >= 1)
		{
		    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	    }
	    if(iFileLen >= 1)
	    {
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	    }
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SINGLE_DATA), szTemp);
	    //TRACE_WEB_USER("[Web_MakeRectDataFile] szTemp is %s\n", szTemp);
	    /*DELETE(szTemp);
	    szTemp = NULL;*/
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SINGLE_DATA), "[]");
	}
	memset(szFile, 0, sizeof(szFile));
	if(itype == RECT_GROUP_SEQ)
	{
	    snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_RECT_VAR);
	}
	else if(itype == CONVERTER_GROUP_SEQ)
	{
	    snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_CONVERTER_VAR);
	}
	else if(itype == RECTS1_GROUP_SEQ)
	{
	    snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_RECTS1_VAR);
	}
	else if(itype == RECTS2_GROUP_SEQ)
	{
	    snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_RECTS2_VAR);
	}
	else if(itype == RECTS3_GROUP_SEQ)
	{
	    snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_RECTS3_VAR);
	}
	else
	{
	    snprintf(szFile, sizeof(szFile), "%s", WEB_DATA_SOLAR_VAR);
	}
	if(szFile != NULL && (fp = fopen(szFile,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
	DELETE(szFile);
	szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeRectDataFile[%d]\n", itype);
    return;
}

#define EQUIP_ID_IB2_START			202
#define EQUIP_ID_EIB_START			197
#define EQUIP_ID_SMTEMP_START		709
#define EQUIP_ID_SMDUE_START	1801
#define MAX_IB2_NUM						2
#define MAX_EIB_NUM						2
#define MAX_SMDUE_TEMP_NUM			2
#define MAX_SMTEMP_NUM					8
#define MAX_TEMP_SENSOR_NUM				(75 +20)
static BOOL gs_bIB2ExistState[MAX_IB2_NUM];
static BOOL gs_bEIBExistState[MAX_EIB_NUM];
static BOOL gs_bSMDUEExistState[MAX_SMDUE_TEMP_NUM];
static BOOL gs_bSMTempExistState[MAX_SMTEMP_NUM];
static BOOL gs_iSelectExist[MAX_TEMP_SENSOR_NUM];

void Web_JudgeTempEquipExitInit()
{

	int iBufLen;
	int i,j,iError;	
	EQUIP_INFO	*pEquipInfo;
	SIG_BASIC_VALUE *pSigValue;
		
	for(i = 0; i < MAX_IB2_NUM; i++)
	{
		iError = DxiGetData(VAR_A_EQUIP_INFO,
			EQUIP_ID_IB2_START + i,
			0,
			&iBufLen,
			&pEquipInfo,
			0);
		if(pEquipInfo->bWorkStatus == TRUE)
			gs_bIB2ExistState[i] = TRUE;
		else
			gs_bIB2ExistState[i] = FALSE;
	}

	
	for(i = 0; i < MAX_EIB_NUM; i++)
	{
		iError = DxiGetData(VAR_A_EQUIP_INFO,
			EQUIP_ID_EIB_START + i,
			0,
			&iBufLen,
			&pEquipInfo,
			0);
		if(pEquipInfo->bWorkStatus == TRUE)
			gs_bEIBExistState[i] = TRUE;
		else
			gs_bEIBExistState[i] = FALSE;
	}
	
	for(i = 0; i < MAX_SMTEMP_NUM; i++)
	{
		iError = DxiGetData(VAR_A_EQUIP_INFO,
			EQUIP_ID_SMTEMP_START + i,
			0,
			&iBufLen,
			&pEquipInfo,
			0);
		if(pEquipInfo->bWorkStatus == TRUE)
			gs_bSMTempExistState[i] = TRUE;
		else
			gs_bSMTempExistState[i] = FALSE;
	}
	for(i = 0; i < MAX_SMDUE_TEMP_NUM; i++)
	{
		iError = DxiGetData(VAR_A_EQUIP_INFO,
			EQUIP_ID_SMDUE_START + i,
			0,
			&iBufLen,
			&pEquipInfo,
			0);
		if(pEquipInfo->bWorkStatus == TRUE)
			gs_bSMDUEExistState[i] = TRUE;
		else
			gs_bSMDUEExistState[i] = FALSE;
	}
}
/*==========================================================================*
* FUNCTION :  Web_JudgeTempEquipExist()
* PURPOSE  :  To Judge whether the temperature equipment exist
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Song Xu 20170405
*==========================================================================*/
void Web_JudgeTempEquipExist()
{
	int i,j,iError;	
	static bInitFlag = FALSE;

	if(bInitFlag == FALSE)
	{
		bInitFlag = TRUE;
		Web_JudgeTempEquipExitInit();
	}
	for(i = 0 ;i < MAX_TEMP_SENSOR_NUM; i++)
	{
		if(i < 3)
		{
			gs_iSelectExist[i] = TRUE;
		}
		else if(i<5)
		{
			if(gs_bIB2ExistState[0] == TRUE)
				gs_iSelectExist[i] = TRUE;
			else
				gs_iSelectExist[i] = FALSE;
		}
		else if(i<7)
		{
			if(gs_bEIBExistState[0] == TRUE)
				gs_iSelectExist[i] = TRUE;
			else
				gs_iSelectExist[i] = FALSE;
		}
		else if(i<71)
		{
			if(gs_bSMTempExistState[(i-7)/8] == TRUE)
				gs_iSelectExist[i] = TRUE;
			else
				gs_iSelectExist[i] = FALSE;
		}
		else if(i<73)
		{
			if(gs_bIB2ExistState[1] == TRUE)
				gs_iSelectExist[i] = TRUE;
			else
				gs_iSelectExist[i] = FALSE;
		}
		else if(i<75)
		{
			if(gs_bEIBExistState[1] == TRUE)
				gs_iSelectExist[i] = TRUE;
			else
				gs_iSelectExist[i] = FALSE;
		}
		else if(i<95)
		{
			if(gs_bSMDUEExistState[(i-75)/10] == TRUE)
				gs_iSelectExist[i] = TRUE;
			else
				gs_iSelectExist[i] = FALSE;	
		}
	}
	
}



int Web_TempSensorEnumSet(int iBeginSelect,int iBufLen,char *szBuf,SIG_BASIC_VALUE *pSigValue)
{

#define MIN_TEMPCOMP_TEMP_NUM (79+20)

	static BOOL bInit = FALSE;
	if(bInit == FALSE)
	{
		bInit= TRUE;
		Web_JudgeTempEquipExist();
	}
	int i, j,iExistFlag;

	for(j = 0; j < ((SET_SIG_VALUE *)(pSigValue))->pStdSig->iStateNum; j++)
	{
		if ( (j<iBeginSelect) ||(j == MIN_TEMPCOMP_TEMP_NUM)  )
		{
			iExistFlag = 1;
		}
		else
		{
			iExistFlag = gs_iSelectExist[j-iBeginSelect];
		}
		
		iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "[\"%s\",\"%s\",%d],", 
			((SET_SIG_VALUE *)(pSigValue))->pStdSig->pStateText[j]->pFullName[0],
			((SET_SIG_VALUE *)(pSigValue))->pStdSig->pStateText[j]->pFullName[1],
			iExistFlag);

		if( (MAX_LENGTH_SET_DATA_BUFFER - iBufLen) < 0 )
		{
			AppLogOut("Web_TempSensorEnumSet", APP_LOG_MILESTONE, "**********Buffer is overflow!*******************\n");
		}
	}
	return iBufLen;
}

/*==========================================================================*
* FUNCTION :  Web_MakeSingleSetDataFile
* PURPOSE  :  To make the setting signal string
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  itype: indicate which kind of setting signal such as LVD, ECO, charge etc
* RETURN   :  char * : the pointer pointing to the string including the setting data.
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng               DATE: 2015-07-20
*==========================================================================*/
char* Web_MakeSingleSetDataFile(int itype)
{
//changed by Frank Wu,7/7/30,20140527, for add single converter and single solar settings pages
#define IS_SINGLE_SET_SEQ(itype)		\
	(	\
		(SINGLE_CONVERTER_SEQ == itype)				\
		|| (SINGLE_SOLAR_SEQ == itype)			\
		|| (SINGLE_RECT_S1_SEQ == itype)			\
		|| (SINGLE_RECT_S2_SEQ == itype)			\
		|| (SINGLE_RECT_S3_SEQ == itype)			\
	)

#define IS_BATTERY_SET_SEQ(itype)		\
    (	\
    (COMM_BATT_SEQ == itype)			\
    || (EIB_BATT_SEQ1 == itype)			\
    || (SMDU_BATT_SEQ1 == itype)			\
    || (SMDUE_BATT_SEQ1 == itype)			\
    || (SM_BATT_SEQ1 == itype)			\
    || (SMBRC_BATT_SEQ1 == itype)			\
    || (SONICK_BATT_SEQ1 == itype)			\
    )

//changed by Frank Wu,10/10/32,20140312, for adding new web pages to the tab of "power system"
#define IS_POWER_SYSTEM_SEQ(itype)		\
	(	\
		(SYSTEM_GROUP_SEQ == itype)				\
		|| (AC_GROUP_SEQ == itype)				\
		|| (AC_UNIT_SEQ == itype)			\
		|| (ACMETER_GROUP_SEQ == itype)		\
		|| (ACMETER_UNIT_SEQ == itype)		\
		|| (DC_UNIT_SEQ == itype)			\
		|| (DCMETER_GROUP_SEQ == itype)		\
		|| (DCMETER_UNIT_SEQ == itype)		\
		|| (DIESEL_GROUP_SEQ == itype)		\
		|| (DIESEL_UNIT_SEQ == itype)		\
		|| (FUEL_GROUP_SEQ == itype)		\
		|| (FUEL_UNIT_SEQ == itype)			\
		|| (IB_GROUP_SEQ == itype)			\
		|| (IB_UNIT_SEQ == itype)			\
		|| (EIB_GROUP_SEQ == itype)			\
		|| (OBAC_UNIT_SEQ == itype)			\
		|| (OBLVD_UNIT_SEQ == itype)		\
		|| (OBFUEL_UNIT_SEQ == itype)		\
		|| (SMDU_GROUP_SEQ == itype)		\
		|| (SMDUP_GROUP_SEQ == itype)		\
		|| (SMDUP_UNIT_SEQ == itype)		\
		|| (SMDUH_GROUP_SEQ == itype)		\
		|| (SMDUH_UNIT_SEQ == itype)		\
		|| (SMBRC_GROUP_SEQ == itype)		\
		|| (SMBRC_UNIT_SEQ == itype)		\
		|| (SMIO_GROUP_SEQ == itype)		\
		|| (SMIO_UNIT_SEQ == itype)			\
		|| (SMTEMP_GROUP_SEQ == itype)		\
		|| (SMTEMP_UNIT_SEQ == itype)		\
		|| (SMAC_UNIT_SEQ == itype)			\
		|| (SMLVD_UNIT_SEQ == itype)		\
		|| (LVD3_UNIT_SEQ == itype)		\
		|| (OBBATTFUSE_UNIT_SEQ == itype)		\
		|| (NARADA_BMS_SEQ1 == itype)		\
		|| (NARADA_BMS_GROUP_SEQ1 == itype)		\
	)

#define WEBPAGE_SETTING_TYPE_MALLOC(iArgCurrentType, uiArgRealNum, iArgHeadSize)		\
	{	\
		if(iArgCurrentType == itype)		\
		{	\
			/* szTemp = NEW(char, (MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * uiArgRealNum) + iArgHeadSize)); */	\
			iBufLength = MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * uiArgRealNum) + iArgHeadSize;	\
		}	\
	}

#define WEBPAGE_SETTING_TYPE_PRE_PROC(iArgCurrentType, uiArgRealNum)		\
	{	\
		if(iArgCurrentType == itype)		\
		{	\
			iSignalNum = gs_pWebSetInfo[itype].iNumber * uiArgRealNum;		\
			stWebPrivateSetTemp = gs_pWebSetInfo[itype].stWebPrivateSet;	\
			gs_pWebSetInfo[itype].stWebPrivateSet = gs_pWebSetInfo[itype].stWebPrivateSet1;	\
		}	\
	}
	
#define ID_EQUIP_BATTERY1		116
#define ID_EQUIP_BATTERY2		117
#define ID_SIGNAL_CAPACITY		10
#define ID_SIGNAL_SHUNT_CUR		1
#define ID_SIGNAL_SHUNT_VOL		2
#define ID_EQUIP_SMDUBAT_BEGIN		126
#define ID_EQUIP_SMDUBAT_END		157
#define ID_EQUIP_SMDUBAT_BEGIN1		659
#define ID_EQUIP_SMDUBAT_END1		666
#define ID_EQUIP_SMDUEBAT_BEGIN		3000
#define ID_EQUIP_SMDUEBAT_END		3019

//changed by Frank Wu,9/N/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
//#define ID_SHUNT_SET_EQUIP_SMDUP_ST			643
//#define ID_SHUNT_SET_EQUIP_SMDUP_END		650
//#define ID_SHUNT_SET_EQUIP_EIB_ST			197
//#define ID_SHUNT_SET_EQUIP_EIB_END			200


    char *szTemp = NULL;
    int iFileLen = 0;
    //unsigned int i;
    int i, j, iBufLen;
    int k = 0;
    
    //char szBuf[4096];
	char szBuf[MAX_LENGTH_SET_DATA_BUFFER];

    int iSignalValid = 0;
    int iSignalValid1 = 0;	// for single rectifier only
    BOOL bdisplay = FALSE;
    BOOL bcontrol = FALSE;
    int iValueFormat;
    char szformat[64];
    int	iError = 0;
    int iSignalNum = 0;
    EQUIP_INFO	*pEquipInfo;
    WEB_GENERAL_SETTING_INFO *stWebPrivateSetTemp = NULL;
    //unsigned long uiRectID = 0;
    int iCtrlExpression = 0;
    BOOL	bWorkStatus;
    int		iBufLength;
	//changed by Frank Wu,10/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
	int iTmpEquipID = 0;
	//int iTmpSignalID = 0;
	float fValueTemp, fValueMax, fValueMin;
	char cTempUnit[8];
    TRACE_WEB_USER("Begin to Web_MakeSingleSetDataFile[%d]\n", itype);


    //check the pointer valid begin
    if(itype == LVD_SEQ)
    {
	if((gs_pWebSetInfo[itype].stWebPrivateSet == NULL) || (gs_pWebSetInfo[itype].stWebEquipInfo == NULL))
	{
	    return NULL;
	}
    }
	//changed by Frank Wu,8/N/30,20140527, for add single converter and single solar settings pages
    else if( (itype == SINGLE_RECT_SEQ)
			|| IS_SINGLE_SET_SEQ(itype) || IS_BATTERY_SET_SEQ(itype)
			)
    {
	if(gs_pWebSetInfo[itype].stWebPrivateSet1 == NULL)
	{
	    return NULL;
	}
    }
    else if((itype == SINGLE_EIB_SEQ) 
			|| (itype == SINGLE_SMDU_SEQ) 
			|| (itype == LVD_GROUP_SEQ)
			//changed by Frank Wu,11/32,20140312, for adding new web pages to the tab of "power system"
			|| IS_POWER_SYSTEM_SEQ(itype)
			)
    {
	if((gs_pWebSetInfo[itype].stWebPrivateSet1 == NULL) || (gs_pWebSetInfo[itype].stWebEquipInfo == NULL))
	{
	    return NULL;
	}
    }
    else
    {
	if(gs_pWebSetInfo[itype].stWebPrivateSet == NULL)
	{
	    TRACE_WEB_USER_NOT_CYCLE("stWebPrivateSet is null, the itype is %d\n", itype);
	    return NULL;
	}
    }
    //check the pointer valid end

    TRACE_WEB_USER("number is %d\n", gs_pWebSetInfo[itype].iNumber);

    //TRACE_WEB_USER("gs_uiRealLVDNum is %d\n", gs_uiRealLVDNum);

    memset(gs_szBuffer, 0, BUFFER_LEN);
    szTemp = gs_szBuffer;
    if(itype == LVD_SEQ)
    {
	//TRACE_WEB_USER("Run here 1\n");
	//szTemp = NEW(char, (MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber + gs_uiRealLVDNum) + 100));
	iBufLength = MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber + gs_uiRealLVDNum) + 100;
	//TRACE_WEB_USER("Run here 2\n");
    }
    else if(itype == SINGLE_RECT_SEQ)
    {
	//szTemp = NEW(char, (MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * gs_uiRectifierNumber) + 100));
	iBufLength = MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * gs_uiRectifierNumber) + 100;
	/*TRACE_WEB_USER_NOT_CYCLE("The iBufLength is %d\n", iBufLength);
	TRACE_WEB_USER_NOT_CYCLE("The szTemp length is %d\n", sizeof(szTemp));*/
	//debug begin
	/*char *szTemp1 = NEW(char, 100);
	memset(szTemp1, '1', sizeof(szTemp1));
	TRACE_WEB_USER_NOT_CYCLE("\nthe content is\n");
	for(i = 0; i < 100; i++)
	{
	    TRACE_WEB_USER_NOT_CYCLE("%c", *(szTemp1 + i));
	}
	memset(szTemp1, '1', 100);
	TRACE_WEB_USER_NOT_CYCLE("\nthe content1 is\n");
	for(i = 0; i < 100; i++)
	{
	    TRACE_WEB_USER_NOT_CYCLE("%c", *(szTemp1 + i));
	}*/
	//debug end
    }
	//changed by Frank Wu,9/8/30,20140527, for add single converter and single solar settings pages
	else if( IS_SINGLE_SET_SEQ(itype) || IS_BATTERY_SET_SEQ(itype))
	{
		WEBPAGE_SETTING_TYPE_MALLOC( SINGLE_CONVERTER_SEQ, gs_uiConverterNumber, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SINGLE_SOLAR_SEQ, gs_uiSolarNumber, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SINGLE_RECT_S1_SEQ, gs_uiRectifierS1Number, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SINGLE_RECT_S2_SEQ, gs_uiRectifierS2Number, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SINGLE_RECT_S3_SEQ, gs_uiRectifierS3Number, 100);

		WEBPAGE_SETTING_TYPE_MALLOC( COMM_BATT_SEQ, gs_uiRealComBattNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( EIB_BATT_SEQ1, gs_uiRealEIBBattNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMDU_BATT_SEQ1, gs_uiRealSMDUBattNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMDUE_BATT_SEQ1, gs_uiRealSMDUEBattNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SM_BATT_SEQ1, gs_uiRealSMBattNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMBRC_BATT_SEQ1, gs_uiRealSMBRCBattNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SONICK_BATT_SEQ1, gs_uiRealSoNickBattNum, 100);
		//WEBPAGE_SETTING_TYPE_MALLOC( SMDU_BATT_SEQ1, gs_uiRealSMDUBattNum, 100);
	}	
    else if(itype == SINGLE_EIB_SEQ)
    {
	//szTemp = NEW(char, (MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * gs_uiRealEIBNum) + 100));
	iBufLength = MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * gs_uiRealEIBNum) + 100;
    }
    else if(itype == SINGLE_SMDU_SEQ)
    {
	//szTemp = NEW(char, (MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * gs_uiRealSMDUNum) + 100));
	iBufLength = MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * gs_uiRealSMDUNum) + 100;
    }
    else if(itype == LVD_GROUP_SEQ)
    {
	//szTemp = NEW(char, (MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * gs_uiRealLVDGroupNum) + 100));
	iBufLength = MAX_SETTING_LEN * (gs_pWebSetInfo[itype].iNumber * gs_uiRealLVDGroupNum) + 100;
    }
    else if((itype == CHARGE_SEQ) || (itype == WIZARD_SEQ))	// specially for Sensor used for temp comp
    {
	//szTemp = NEW(char, (MAX_SETTING_LEN * gs_pWebSetInfo[itype].iNumber + 100 + 4096));
	iBufLength = MAX_SETTING_LEN * gs_pWebSetInfo[itype].iNumber + 100 + MAX_LENGTH_SET_DATA_BUFFER;
    }
    else if(itype == TEMP_SEQ)
    {
	//szTemp = NEW(char, (MAX_SETTING_LEN * gs_pWebSetInfo[itype].iNumber + 100 + (4096 * 3)));	// there are 3 sensors in temperature setting web
	iBufLength = MAX_SETTING_LEN * gs_pWebSetInfo[itype].iNumber + 100 + (MAX_LENGTH_SET_DATA_BUFFER * 3);
    }
	//changed by Frank Wu,11/12/32,20140312, for adding new web pages to the tab of "power system"
	else if( IS_POWER_SYSTEM_SEQ(itype) )
	{
		WEBPAGE_SETTING_TYPE_MALLOC( SYSTEM_GROUP_SEQ, gs_uiRealSystemGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( AC_GROUP_SEQ, gs_uiRealACGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( AC_UNIT_SEQ, gs_uiRealACUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( ACMETER_GROUP_SEQ, gs_uiRealACMeterGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( ACMETER_UNIT_SEQ, gs_uiRealACMeterUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( DC_UNIT_SEQ, gs_uiRealDCUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( DCMETER_GROUP_SEQ, gs_uiRealDCMeterGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( DCMETER_UNIT_SEQ, gs_uiRealDCMeterUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( DIESEL_GROUP_SEQ, gs_uiRealDieselGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( DIESEL_UNIT_SEQ, gs_uiRealDieselUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( FUEL_GROUP_SEQ, gs_uiRealFuelGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( FUEL_UNIT_SEQ, gs_uiRealFuelUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( IB_GROUP_SEQ, gs_uiRealIBGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( IB_UNIT_SEQ, gs_uiRealIBUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( EIB_GROUP_SEQ, gs_uiRealEIBGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( OBAC_UNIT_SEQ, gs_uiRealOBACUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( OBLVD_UNIT_SEQ, gs_uiRealOBLVDUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( OBFUEL_UNIT_SEQ, gs_uiRealOBFuelUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMDU_GROUP_SEQ, gs_uiRealSMDUGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMDUP_GROUP_SEQ, gs_uiRealSMDUPGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMDUP_UNIT_SEQ, gs_uiRealSMDUPNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMDUH_GROUP_SEQ, gs_uiRealSMDUHGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMDUH_UNIT_SEQ, gs_uiRealSMDUHNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMBRC_GROUP_SEQ, gs_uiRealSMBRCGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMBRC_UNIT_SEQ, gs_uiRealSMBRCUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMIO_GROUP_SEQ, gs_uiRealSMIOGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMIO_UNIT_SEQ, gs_uiRealSMIOUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMTEMP_GROUP_SEQ, gs_uiRealSMTempGroupNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMTEMP_UNIT_SEQ, gs_uiRealSMTempUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMAC_UNIT_SEQ, gs_uiRealSMACUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( SMLVD_UNIT_SEQ, gs_uiRealSMLVDUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( LVD3_UNIT_SEQ, gs_uiRealLVD3UnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( OBBATTFUSE_UNIT_SEQ, gs_uiRealOBBattFuseUnitNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( NARADA_BMS_SEQ1, gs_uiNaradaBMSNum, 100);
		WEBPAGE_SETTING_TYPE_MALLOC( NARADA_BMS_GROUP_SEQ1, gs_uiRealNaradaBMSGroupNum, 100);
	}
	else
    {
	//szTemp = NEW(char, (MAX_SETTING_LEN * gs_pWebSetInfo[itype].iNumber + 100));
	iBufLength = MAX_SETTING_LEN * gs_pWebSetInfo[itype].iNumber + 100;
    }
    
    /*if(szTemp == NULL)
    {
	AppLogOut("Web_MakeSingleSetDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return NULL;
    }*/
	//changed by Frank Wu,10/N/30,20140527, for add single converter and single solar settings pages
    if( (itype != SINGLE_RECT_SEQ)    // specially for <data.rect_setting.html>
		&& ( ! IS_SINGLE_SET_SEQ(itype)) && ( ! IS_BATTERY_SET_SEQ(itype))
		)
    {
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
    }
    else if(IS_BATTERY_SET_SEQ(itype))
    {
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "{");
    }
    else
    {
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
    }
    if(itype == LVD_SEQ)
    {
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
		for(i = 0; i < gs_uiRealLVDNum; i++)
		{
			TRACE_WEB_USER("Exec the %d LVD\n", i);
			if(gs_pWebSetInfo[itype].stWebEquipInfo[i].bSingleValid == FALSE)
			{
			TRACE_WEB_USER("Run here 3\n");
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
			}
			else
			{
			TRACE_WEB_USER("Run here 4\n");
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],", gs_pWebSetInfo[itype].stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[0],
				gs_pWebSetInfo[itype].stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[1]);
			TRACE_WEB_USER("szTemp is %s\n", szTemp);
			}
		}
		if(iFileLen >= 1)
		{
			iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	}
    if(itype == SINGLE_RECT_SEQ)
    {
	iSignalNum = gs_pWebSetInfo[itype].iNumber * gs_uiRectifierNumber;
	stWebPrivateSetTemp = gs_pWebSetInfo[itype].stWebPrivateSet;
	gs_pWebSetInfo[itype].stWebPrivateSet = gs_pWebSetInfo[itype].stWebPrivateSet1;
    }
	//changed by Frank Wu,11/9/30,20140527, for add single converter and single solar settings pages
	else if( IS_SINGLE_SET_SEQ(itype) || IS_BATTERY_SET_SEQ(itype))
	{
		WEBPAGE_SETTING_TYPE_PRE_PROC( SINGLE_CONVERTER_SEQ, gs_uiConverterNumber);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SINGLE_SOLAR_SEQ, gs_uiSolarNumber);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SINGLE_RECT_S1_SEQ, gs_uiRectifierS1Number);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SINGLE_RECT_S2_SEQ, gs_uiRectifierS2Number);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SINGLE_RECT_S3_SEQ, gs_uiRectifierS3Number);

		WEBPAGE_SETTING_TYPE_PRE_PROC( COMM_BATT_SEQ, gs_uiRealComBattNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( EIB_BATT_SEQ1, gs_uiRealEIBBattNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMDU_BATT_SEQ1, gs_uiRealSMDUBattNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMDUE_BATT_SEQ1, gs_uiRealSMDUEBattNum);

		/*if(itype == SMDU_BATT_SEQ1)
		{
		    TRACE_WEB_USER_NOT_CYCLE("iSignalNum is %d\n", iSignalNum);
		}*/
		WEBPAGE_SETTING_TYPE_PRE_PROC( SM_BATT_SEQ1, gs_uiRealSMBattNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMBRC_BATT_SEQ1, gs_uiRealSMBRCBattNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SONICK_BATT_SEQ1, gs_uiRealSoNickBattNum);
	}
    else if(itype == SINGLE_EIB_SEQ)
    {
	iSignalNum = gs_pWebSetInfo[itype].iNumber * gs_uiRealEIBNum;
	stWebPrivateSetTemp = gs_pWebSetInfo[itype].stWebPrivateSet;
	gs_pWebSetInfo[itype].stWebPrivateSet = gs_pWebSetInfo[itype].stWebPrivateSet1;
    }
    else if(itype == SINGLE_SMDU_SEQ)
    {
	iSignalNum = gs_pWebSetInfo[itype].iNumber * gs_uiRealSMDUNum;
	stWebPrivateSetTemp = gs_pWebSetInfo[itype].stWebPrivateSet;
	gs_pWebSetInfo[itype].stWebPrivateSet = gs_pWebSetInfo[itype].stWebPrivateSet1;
    }
    else if(itype == LVD_GROUP_SEQ)
    {
	iSignalNum = gs_pWebSetInfo[itype].iNumber * gs_uiRealLVDGroupNum;
	stWebPrivateSetTemp = gs_pWebSetInfo[itype].stWebPrivateSet;
	gs_pWebSetInfo[itype].stWebPrivateSet = gs_pWebSetInfo[itype].stWebPrivateSet1;
    }
	//changed by Frank Wu,12/13/32,20140312, for adding new web pages to the tab of "power system"
	else if( IS_POWER_SYSTEM_SEQ(itype) )
	{
		WEBPAGE_SETTING_TYPE_PRE_PROC( SYSTEM_GROUP_SEQ, gs_uiRealSystemGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( AC_GROUP_SEQ, gs_uiRealACGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( AC_UNIT_SEQ, gs_uiRealACUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( ACMETER_GROUP_SEQ, gs_uiRealACMeterGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( ACMETER_UNIT_SEQ, gs_uiRealACMeterUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( DC_UNIT_SEQ, gs_uiRealDCUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( DCMETER_GROUP_SEQ, gs_uiRealDCMeterGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( DCMETER_UNIT_SEQ, gs_uiRealDCMeterUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( DIESEL_GROUP_SEQ, gs_uiRealDieselGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( DIESEL_UNIT_SEQ, gs_uiRealDieselUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( FUEL_GROUP_SEQ, gs_uiRealFuelGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( FUEL_UNIT_SEQ, gs_uiRealFuelUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( IB_GROUP_SEQ, gs_uiRealIBGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( IB_UNIT_SEQ, gs_uiRealIBUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( EIB_GROUP_SEQ, gs_uiRealEIBGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( OBAC_UNIT_SEQ, gs_uiRealOBACUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( OBLVD_UNIT_SEQ, gs_uiRealOBLVDUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( OBFUEL_UNIT_SEQ, gs_uiRealOBFuelUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMDU_GROUP_SEQ, gs_uiRealSMDUGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMDUP_GROUP_SEQ, gs_uiRealSMDUPGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMDUP_UNIT_SEQ, gs_uiRealSMDUPNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMDUH_GROUP_SEQ, gs_uiRealSMDUHGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMDUH_UNIT_SEQ, gs_uiRealSMDUHNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMBRC_GROUP_SEQ, gs_uiRealSMBRCGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMBRC_UNIT_SEQ, gs_uiRealSMBRCUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMIO_GROUP_SEQ, gs_uiRealSMIOGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMIO_UNIT_SEQ, gs_uiRealSMIOUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMTEMP_GROUP_SEQ, gs_uiRealSMTempGroupNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMTEMP_UNIT_SEQ, gs_uiRealSMTempUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMAC_UNIT_SEQ, gs_uiRealSMACUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( SMLVD_UNIT_SEQ, gs_uiRealSMLVDUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( LVD3_UNIT_SEQ, gs_uiRealLVD3UnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( OBBATTFUSE_UNIT_SEQ, gs_uiRealOBBattFuseUnitNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( NARADA_BMS_SEQ1, gs_uiNaradaBMSNum);
		WEBPAGE_SETTING_TYPE_PRE_PROC( NARADA_BMS_GROUP_SEQ1, gs_uiRealNaradaBMSGroupNum);
	}
	else
    {
	iSignalNum = gs_pWebSetInfo[itype].iNumber;
    }
    
    
    TRACE_WEB_USER("iSignalNum is  %d\n", iSignalNum);
    
    for(i = 0; i < iSignalNum; i++)
    {
	TRACE_WEB_USER("Exec %d\n", i);
	//changed by Frank Wu,12/N/30,20140527, for add single converter and single solar settings pages
	if( (itype == SINGLE_RECT_SEQ)
		|| IS_SINGLE_SET_SEQ(itype)
		)
	{
	    if(k % (gs_pWebSetInfo[itype].iNumber) == 0)
	    {
		/*for(l = 0; l < gs_pWebSetInfo[itype].iNumber; l++)
		{
		    if(gs_pWebSetInfo[itype].stWebPrivateSet[k + l].bSetVaild)
		    {
			if((gs_pWebSetInfo[itype].stWebPrivateSet[k + l].iSignalType == 2) && (gs_pWebSetInfo[itype].stWebPrivateSet[k + l].iSignalID == 1))
			{
			    uiRectID = gs_pWebSetInfo[itype].stWebPrivateSet[k + l].pSigValue->varValue.ulValue;
			}
		    }
		}*/
		//iFileLen += sprintf(szTemp + iFileLen, "\"RECT_%ld\":{\"list\":[", uiRectID);
		iSignalValid1 = 0;
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "\"RECT_%ld\":{\"list\":[", ((k / (gs_pWebSetInfo[itype].iNumber)) + 1));
	    }
	}
	else if(IS_BATTERY_SET_SEQ(itype))
	{
	    if(k % (gs_pWebSetInfo[itype].iNumber) == 0)
	    {
		iSignalValid1 = 0;
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "\"BATT_%ld\":{\"list\":[", ((k / (gs_pWebSetInfo[itype].iNumber)) + 1));
	    }
	}
	//head
	if( (itype == SINGLE_EIB_SEQ) 
		|| (itype == SINGLE_SMDU_SEQ) 
		|| (itype == LVD_GROUP_SEQ)
		//changed by Frank Wu,14/32,20140312, for adding new web pages to the tab of "power system"
		|| IS_POWER_SYSTEM_SEQ(itype)
		)
	{
	    if(k % (gs_pWebSetInfo[itype].iNumber) == 0)
	    {
		iSignalValid1 = 0;
		if(gs_pWebSetInfo[itype].stWebEquipInfo[k / (gs_pWebSetInfo[itype].iNumber)].bSingleValid == TRUE)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "{\"%s,%s\":{\"list\":[", gs_pWebSetInfo[itype].stWebEquipInfo[k / (gs_pWebSetInfo[itype].iNumber)].pEquipInfo->pEquipName->pFullName[0],
			gs_pWebSetInfo[itype].stWebEquipInfo[k / (gs_pWebSetInfo[itype].iNumber)].pEquipInfo->pEquipName->pFullName[1]);
		}
		else
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "{\"\":{\"list\":[");
		}
	    }
	}
	//Judge to display or not
	if(gs_pWebSetInfo[itype].stWebPrivateSet[i].bSetVaild)
	{
	    //TRACE_WEB_USER("Run here\n");
	    if(gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType == SIG_TYPE_SETTING)
	    {
		bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->bv);
	    }
	    else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType == SIG_TYPE_CONTROL)
	    {
		bdisplay = SIG_VALUE_NEED_DISPLAY(&((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->bv);
	    }
	}

	//added to remove setting-system-Power System-DO relay out  by Stone Song 20170722 
	if(SYSTEM_GROUP_SEQ==itype && gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType==SIG_TYPE_CONTROL && gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID==1 &&
		(gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID>=13 && gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID<=25))
	{
		bdisplay = FALSE;
	}

	if(gs_pWebSetInfo[itype].stWebPrivateSet[i].bWorkStatus == FALSE)
	{
	    bWorkStatus = FALSE;
	}
	else
	{
	    bWorkStatus = gs_pWebSetInfo[itype].stWebPrivateSet[i].pEquipInfo->bWorkStatus;
	}
	if(itype == SINGLE_SMDU_SEQ)
	{
	    TRACE_WEB_USER("bWorkStatus is %d bdisplay is %d  bSetVaild is %d\n", bWorkStatus,
		bdisplay, gs_pWebSetInfo[itype].stWebPrivateSet[i].bSetVaild);
	}
	
	if((bWorkStatus == TRUE) && (bdisplay == TRUE) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].bSetVaild == TRUE))
	{
	    iSignalValid = 1;
	    iSignalValid1 = 1;
	    TRACE_WEB_USER("[%d][%d][%d]\n", gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID,
	    gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType,
	    gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID);
	    if(gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType == SIG_TYPE_SETTING)
	    {
    
		iValueFormat = Web_TransferFormatToInt(((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szValueDisplayFmt);

		//signal name
		// special for the following signals
		if(((itype == CHARGE_SEQ) || (itype == WIZARD_SEQ)) && 
		    (((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID == ID_EQUIP_BATTERY1) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == ID_SIGNAL_CAPACITY)) ||
		    ((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID == ID_EQUIP_BATTERY2) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == ID_SIGNAL_CAPACITY)) ||
		    ((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID == ID_EQUIP_BATTERY1) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == ID_SIGNAL_SHUNT_CUR)) ||
		    ((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID == ID_EQUIP_BATTERY1) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == ID_SIGNAL_SHUNT_VOL)) ||
		    ((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID == ID_EQUIP_BATTERY2) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == ID_SIGNAL_SHUNT_CUR)) ||
		    ((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID == ID_EQUIP_BATTERY2) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == ID_SIGNAL_SHUNT_VOL)) ||
		    ((((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID >= ID_EQUIP_SMDUBAT_BEGIN) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID <= ID_EQUIP_SMDUBAT_END)) ||
		    ((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID >= ID_EQUIP_SMDUBAT_BEGIN1) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID <= ID_EQUIP_SMDUBAT_END1))) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == ID_SIGNAL_CAPACITY)) ||
		    (((gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID >= ID_EQUIP_SMDUEBAT_BEGIN) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID <= ID_EQUIP_SMDUEBAT_END)) && (gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == ID_SIGNAL_CAPACITY))))

		{
		    iError = DxiGetData(VAR_A_EQUIP_INFO,
			gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID,
			0,
			&iBufLen,
			&pEquipInfo,
			0);
		    if(iError == ERR_DXI_OK)
		    {
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s %s\",\"%s %s\"],",
			    pEquipInfo->pEquipName->pAbbrName[0],
			    ((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[0],
			    pEquipInfo->pEquipName->pFullName[1],
			    ((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[1]);
		    }
		    else
		    {
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
			    ((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[0],
			    ((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[1]);
		    }
		}
		//changed by Frank Wu,10/11/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
		else if( SHUNT_SET_SEQ == itype )//special process for the same signal names
		{
			iTmpEquipID = gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID;
			//iTmpSignalID = gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID;
			//output equipment name
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
			iError = DxiGetData(VAR_A_EQUIP_INFO,
				iTmpEquipID,
				0,
				&iBufLen,
				&pEquipInfo,
				0);
			if(iError == ERR_DXI_OK)
			{
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
					pEquipInfo->pEquipName->pFullName[0],
					pEquipInfo->pEquipName->pFullName[1]);
			}
			else
			{
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
					"NA",
					"NA");
			}
			//output signal name
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
				((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[0],
				((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[1]);

		}
		else
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
			((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[0],
			((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[1]);
		}
		memset(szformat, 0, sizeof(szformat));
		snprintf(szformat, sizeof(szformat), "%%d,%%d,%%d,%%d,%%ld,%%.%df,%%.%df,", iValueFormat, iValueFormat);// for example "%d,%d,%d,%d,%ld,%.1f,%.1f,"
		//TRACE_WEB_USER("szformat is %s\n", szformat);
		//equip ID, signal type and signal type, authority level, sampling time and range

		//if(IS_TEMP_SIGNAL(gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID,
		//	gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType,
		//	gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID))

		if(DXI_isTempSignal(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))
		{
			DXI_GetTempSwitch(TEMP_VALUE,
				((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->fMaxValidValue,
				&fValueMax,
				NULL);

			DXI_GetTempSwitch(TEMP_VALUE,
				((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->fMinValidValue,
				&fValueMin,
				NULL);
		}
		else if(IS_TEMP_COE_SIGNAL(gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID ,
			gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType,
			gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID ))
		{
			DXI_GetTempSwitch(TEMP_COE,
				((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->fMaxValidValue,
				&fValueMax,
				NULL);

			DXI_GetTempSwitch(TEMP_COE,
				((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->fMinValidValue,
				&fValueMin,
				NULL);
		}
		else
		{
			fValueMax = ((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->fMaxValidValue;
			fValueMin = ((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->fMinValidValue;
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szformat, gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID,
			gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType, gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID,
			((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->byAuthorityLevel,
			gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->tmCurrentSampled,
			fValueMax,
			fValueMin);		//signal value type, signal value and unit/ENUM items
		if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_LONG)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%ld,\" %s\"],",
			VAR_LONG, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.lValue,
			((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szSigUnit);
		}
		else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_FLOAT)
		{
		    //iValueFormat = Web_TransferFormatToInt(((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szValueDisplayFmt);
		    memset(szformat, 0, sizeof(szformat));
			//if(IS_TEMP_SIGNAL(gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID,
			//		gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType,
			//		gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID))
			if(DXI_isTempSignal(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))
			{
				DXI_GetTempSwitch(TEMP_VALUE,
					gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.fValue,
					&fValueTemp,
					cTempUnit);
			}
			else if(IS_TEMP_COE_SIGNAL(gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID ,
					gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType,
					gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID))
			{
				DXI_GetTempSwitch(TEMP_COE,
					gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.fValue,
					&fValueTemp,
					cTempUnit);
			}
			else
			{
				fValueTemp = gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.fValue;
				snprintf(cTempUnit,sizeof(szformat),"%s",
					((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szSigUnit);
			}
			snprintf(szformat, sizeof(szformat), "%%d,\"%%.%df\",\" %%s\"],", iValueFormat);// for example "%d,\"%.1f\","%s"],"
					iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szformat,
					VAR_FLOAT,
					fValueTemp,
					cTempUnit);

		}
		else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_UNSIGNED_LONG)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%ld,\" %s\"],",
			VAR_UNSIGNED_LONG, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.ulValue,
			((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szSigUnit);
		}
		else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_DATE_TIME)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%ld,\" %s\"],",
			VAR_DATE_TIME, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.dtValue,
			((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szSigUnit);
		}
		else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_ENUM)
		{
		    memset(szBuf, 0, sizeof(szBuf));
		    iBufLen = 0;
		    iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "[");
			
			//add the sensor diplay atribution for temperature sensor 
			if(gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID == 1 &&
				gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == 208)
			{
				iBufLen = Web_TempSensorEnumSet(3,iBufLen,szBuf,gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue);
			}
			else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID == 115 &&
				(gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == 63)||(gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == 414))
			{
				/*if((gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == 63))
				{
					printf("(gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID == 63) ~~~~~~~~~~~~~~~~~~~~~\n ");
				}*/

				iBufLen = Web_TempSensorEnumSet(4,iBufLen,szBuf,gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue);
			}
			else
			{	
				for(j = 0; j < ((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->iStateNum; j++)
				{
					iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "[\"%s\",\"%s\"],", 
						((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
				}
			}
			if(iBufLen >= 1)
			{
			iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
			}
			iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "]");
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%ld,%s],",
			VAR_ENUM, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.enumValue, szBuf);
		}
	    }
	    else
	    {
		iValueFormat = Web_TransferFormatToInt(((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szValueDisplayFmt);
		//signal name
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
		    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[0],
		    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pSigName->pFullName[1]);
		memset(szformat, 0, sizeof(szformat));
		snprintf(szformat, sizeof(szformat), "%%d,%%d,%%d,%%d,%%ld,%%.%df,%%.%df,", iValueFormat, iValueFormat);// for example "%d,%d,%d,%d,%ld,%.1f,%.1f,"
		//TRACE_WEB_USER("szformat is %s\n", szformat);
		//equip ID, signal type and signal type, sampling time and range
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szformat, gs_pWebSetInfo[itype].stWebPrivateSet[i].iEquipID,
		    gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalType, gs_pWebSetInfo[itype].stWebPrivateSet[i].iSignalID,
		    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->byAuthorityLevel,
		    gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->tmCurrentSampled,
		    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->fMaxValidValue,
		    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->fMinValidValue);
		//signal value type, signal value and unit/ENUM items
		if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_LONG)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%ld,\" %s\",",
			VAR_LONG, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.lValue,
			((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szSigUnit);
		}
		else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_FLOAT)
		{
			snprintf(szformat, sizeof(szformat), "%%d,%%.%df,\" %%s\",", iValueFormat);// for example "%d,\"%.1f\","%s"],"
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szformat,
		  //iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%.0f,\" %s\",",
			VAR_FLOAT, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.fValue,
			((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szSigUnit);
		}
		else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_UNSIGNED_LONG)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%ld,\" %s\",",
			VAR_UNSIGNED_LONG, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.ulValue,
			((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szSigUnit);
		}
		else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_DATE_TIME)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%ld,\" %s\",",
			VAR_DATE_TIME, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.dtValue,
			((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->szSigUnit);
		}
		else if(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->ucType == VAR_ENUM)
		{
		    memset(szBuf, 0, sizeof(szBuf));
		    iBufLen = 0;
		    iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "[");
		    for(j = 0; j < ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->iStateNum; j++)
		    {
			iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "[\"%s\",\"%s\"],", 
			    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
			    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
		    }
		    if(iBufLen >= 1)
		    {
			iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
		    }
		    iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "]");
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d,%ld,%s,",
			VAR_ENUM, gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue->varValue.enumValue, szBuf);
		}
		// get the control expression
		memset(szBuf, 0, sizeof(szBuf));
		iBufLen = 0;
		iCtrlExpression = ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->iCtrlExpression;
		if(iCtrlExpression > 0)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "{");
		    for(j = 0; j < iCtrlExpression; j++)
		    {
			iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "%.0f:[\"%s\",\"%s\"],",
			    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pCtrlExpression[j].fThreshold,
			    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pCtrlExpression[j].pText->pFullName[0],
			    ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig->pCtrlExpression[j].pText->pFullName[1]);
		    }
		    if(iBufLen >= 1)
		    {
			iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
		    }
		    iBufLen += snprintf(szBuf + iBufLen, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "}");
		}
		else
		{
		    snprintf(szBuf, (MAX_LENGTH_SET_DATA_BUFFER - iBufLen), "{}");
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s,",szBuf);
		// get the control button expression value
		bcontrol = (CTRL_SIG_IS_CTRL_ON_UI(((CTRL_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->pStdSig, CONTROL_WEB)) &&
		    (SIG_VALUE_IS_CONTROLLABLE(&((SET_SIG_VALUE *)(gs_pWebSetInfo[itype].stWebPrivateSet[i].pSigValue))->bv));
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%d],", bcontrol);
	    }
	}
	//tail
	if( (itype == SINGLE_RECT_SEQ)
		//changed by Frank Wu,13/N/30,20140527, for add single converter and single solar settings pages
		|| IS_SINGLE_SET_SEQ(itype)
		|| IS_BATTERY_SET_SEQ(itype)
		|| (itype == SINGLE_EIB_SEQ) 
		|| (itype == SINGLE_SMDU_SEQ) 
		|| (itype == LVD_GROUP_SEQ)
		//changed by Frank Wu,15/32,20140312, for adding new web pages to the tab of "power system"
		|| IS_POWER_SYSTEM_SEQ(itype)
		)
	{
	    k++;
	    if(k % (gs_pWebSetInfo[itype].iNumber) == 0)
	    {
		if(iSignalValid1 == 1)
		{
		    if(iFileLen >= 1)
		    {
			iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		    }
		}
		//changed by Frank Wu,14/N/30,20140527, for add single converter and single solar settings pages
		if( (itype == SINGLE_RECT_SEQ)
			|| IS_SINGLE_SET_SEQ(itype)
			|| IS_BATTERY_SET_SEQ(itype)
			)
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]},");
		}
		else
		{
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]}},");
		}
	    }
	}

    }

    if( (itype == SINGLE_RECT_SEQ)
		//changed by Frank Wu,15/N/30,20140527, for add single converter and single solar settings pages
		|| IS_SINGLE_SET_SEQ(itype)
		|| IS_BATTERY_SET_SEQ(itype)
		|| (itype == SINGLE_EIB_SEQ)
		|| (itype == SINGLE_SMDU_SEQ)
		|| (itype == LVD_GROUP_SEQ)
		//changed by Frank Wu,16/32,20140312, for adding new web pages to the tab of "power system"
		|| IS_POWER_SYSTEM_SEQ(itype)
		)
    {
	gs_pWebSetInfo[itype].stWebPrivateSet = stWebPrivateSetTemp;
    }
    if(iSignalValid == 0)
    {
	/*if(szTemp != NULL)
	{
	    DELETE(szTemp);
	    szTemp = NULL;
	}*/
	//TRACE_WEB_USER_NOT_CYCLE("No signal is valid, the itype is %d\n", itype);
	return NULL;
    }
	//changed by Frank Wu,16/N/30,20140527, for add single converter and single solar settings pages
    if( (itype != SINGLE_RECT_SEQ)
		&& ( ! IS_SINGLE_SET_SEQ(itype)) && ( ! IS_BATTERY_SET_SEQ(itype))
		)
    {
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
    }
    else if(IS_BATTERY_SET_SEQ(itype))
    {
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "}");
    }
    else
    {
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "");
    }
    TRACE_WEB_USER("end to Web_MakeSingleSetDataFile[%d]\n", itype);

    
    return szTemp;
}


int Web_DC_SMDUE_Display(EQUIP_INFO  *pEquipInfo)
{
#define		SHUNT_SET_AS_SIGID	21
#define		ANALOG_INPUT_TYPE		141
#define		CHANEL_NUM				10
#define		DISPLAY					0
#define		NOT_DISPLAY			1


	int iBufLen=0;
	SIG_BASIC_VALUE* pSigValue;
	int i=0;

	for( i=0; i < CHANEL_NUM; i++ )
	{
		DxiGetData(VAR_A_SIGNAL_VALUE,
					pEquipInfo->iEquipID,
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ANALOG_INPUT_TYPE+i),
					&iBufLen,
					&pSigValue,
					0);
		if( 1 == pSigValue->varValue.enumValue )
		{
			DxiGetData(VAR_A_SIGNAL_VALUE,
						pEquipInfo->iEquipID,
						DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SHUNT_SET_AS_SIGID+i),
						&iBufLen,
						&pSigValue,
						0);
			if( (1 == pSigValue->varValue.enumValue) || (2 == pSigValue->varValue.enumValue) )
			{
				return DISPLAY;
			}
		}
		
	}
	
	return NOT_DISPLAY;
}

void Web_GetTransducer_Units(char * pBuf_En,char * pBuf_Loc ,EQUIP_INFO  *pEquipInfo,SAMPLE_SIG_VALUE  *pSampleSigValue)
{
#define		TRANSDUCER_CURR_START		111
#define		TRANSDUCER_VOLT_START		121
#define		TRANSDUCER_VOLT_UNIT_SIGID_OFFSET		230
#define		TRANSDUCER_CURR_UNIT_SIGID_OFFSET		240
#define		TRANSDUCER_UNIT_NUM		14
#define 	MAX_PER_SMUDE_ANALOG_FUNCTION_NUM	38
#define 	UNITS_POSITION_ANALOG_FUNCTION_NUM	11


	int iSignalID = 0;
	int iUnitsSignID=0,iBufLen=0;
	SIG_BASIC_VALUE* pSigValue1;
	int iLen = 0 ;
	WEB_GENERAL_SETTING_INFO *pWebInfoItem = NULL;
	SET_SIG_VALUE *pSetSig = NULL;
	int iSignalPairNum = 0;
	int iCycleTimes = 0;
	int i=0;
	int iMaxSignalNumber = MAX_PER_SMUDE_ANALOG_FUNCTION_NUM;
	
	iSignalPairNum = gs_pWebSetInfo[SMDUE_ANALOG_SEQ].iNumber;
	iCycleTimes = iSignalPairNum*gs_uiRealSMDUENum;


	iSignalID = pSampleSigValue->pStdSig->iSigID;

	if( iSignalID < TRANSDUCER_VOLT_START )
	{
		iUnitsSignID = iSignalID + TRANSDUCER_CURR_UNIT_SIGID_OFFSET;
	}
	else
	{
		iUnitsSignID = iSignalID + TRANSDUCER_VOLT_UNIT_SIGID_OFFSET;
	}



	DxiGetData(VAR_A_SIGNAL_VALUE,
					pEquipInfo->iEquipID,
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, iUnitsSignID),
					&iBufLen,
					&pSigValue1,
					0);
	if( (pSigValue1->varValue.enumValue<0) || (pSigValue1->varValue.enumValue > TRANSDUCER_UNIT_NUM) )
	{
		pSigValue1->varValue.enumValue = 0;
	}

		for(i = 0; i < iCycleTimes; i++)
		{
			if(i % iMaxSignalNumber == UNITS_POSITION_ANALOG_FUNCTION_NUM )
			{
				pWebInfoItem = &gs_pWebSetInfo[SMDUE_ANALOG_SEQ].stWebPrivateSet1[i];
				pSetSig = (SET_SIG_VALUE *)(pWebInfoItem->pSigValue);

				if( pWebInfoItem->iEquipID == pEquipInfo->iEquipID )
				{
					if( pWebInfoItem->iSignalID == iUnitsSignID )
					{
						if( pSigValue1->varValue.enumValue == pSetSig->bv.varValue.enumValue )
						{
							break;
						}	
					}
				}
			}
		}

	if( pWebInfoItem == NULL )
	{
		iLen = strlen("NA");
		strncpy(pBuf_En,"NA",iLen);
		strncpy(pBuf_Loc,"NA",iLen);
	}
	else
	{
		iLen = strlen(((SET_SIG_VALUE *)(pWebInfoItem->pSigValue))->pStdSig->pStateText[pSigValue1->varValue.enumValue]->pFullName[0]);
		strncpy(pBuf_En,((SET_SIG_VALUE *)(pWebInfoItem->pSigValue))->pStdSig->pStateText[pSigValue1->varValue.enumValue]->pFullName[0],iLen);

		iLen = strlen(((SET_SIG_VALUE *)(pWebInfoItem->pSigValue))->pStdSig->pStateText[pSigValue1->varValue.enumValue]->pFullName[1]);
		strncpy(pBuf_Loc,((SET_SIG_VALUE *)(pWebInfoItem->pSigValue))->pStdSig->pStateText[pSigValue1->varValue.enumValue]->pFullName[1],iLen);
	}
	
	return;
}


void Makesignalstring_use_Custom(WEB_GENERAL_RECT_SIGNAL_INFO stWebPrivate, char *szBuf, int iBufLength,EQUIP_INFO	 *pEquipInfo)
{
#define CURR_TRANSDUCER_SAMPLE_SIG_ID_START		111
#define VOLT_TRANSDUCER_SAMPLE_SIG_ID_END	130
#define CABINET_VOLTAGE_SIG_ID_START			41
#define CABINET_VOLTAGE_SIG_ID_END			50

    int	iBufLen = 0;
    unsigned char	ucSigType;
    SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
    SET_SIG_VALUE     *pSettingSigValue = NULL;
    int iValueFormat;
    char szformat[32];
    int itemp;
    BOOL bdisplay;
    BOOL bdisplay1;
	char cUnitBuf_En[10] ;
	char *pUnitBuf_En;
	char cUnitBuf_Loc[10] ;
	char *pUnitBuf_Loc;

	pUnitBuf_En = cUnitBuf_En;
	pUnitBuf_Loc = cUnitBuf_Loc;

	
    iBufLen = 0;
    if(stWebPrivate.bSingleValid == FALSE)
    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"---\",0,1,1],");
    }
    else
    {
		ucSigType = stWebPrivate.pSigValue->ucSigType;
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue))->bv);
		    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue))->pStdSig), DISPLAY_WEB);
		    pSampleSigValue = (SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(stWebPrivate.pSigValue))->bv);
		    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(stWebPrivate.pSigValue))->pStdSig), DISPLAY_WEB);
		    pSettingSigValue = (SET_SIG_VALUE *)(stWebPrivate.pSigValue);
		}
		else
		{}
		if((bdisplay == TRUE) && (bdisplay1 == TRUE))
		{
			//Value and Units
		    if(stWebPrivate.pSigValue->ucType == VAR_LONG)
		    {
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld", stWebPrivate.pSigValue->varValue.lValue);
				if(ucSigType == SIG_TYPE_SAMPLING)
				{	
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
				}
				else if(ucSigType == SIG_TYPE_SETTING)
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
				}
				else
				{}
		    }
		    else if(stWebPrivate.pSigValue->ucType == VAR_FLOAT)
		    {
				if(ucSigType == SIG_TYPE_SAMPLING)
				{
				    iValueFormat = Web_TransferFormatToInt(pSampleSigValue->pStdSig->szValueDisplayFmt);
				}
				else
				{
				    iValueFormat = Web_TransferFormatToInt(pSettingSigValue->pStdSig->szValueDisplayFmt);
				}

				if( (pSampleSigValue->pStdSig->iSigID >= CURR_TRANSDUCER_SAMPLE_SIG_ID_START) && (pSampleSigValue->pStdSig->iSigID <=VOLT_TRANSDUCER_SAMPLE_SIG_ID_END) )
				{
					 snprintf(szformat, sizeof(szformat), "[[\"%%.%df", iValueFormat);
				}
				else
				{
					snprintf(szformat, sizeof(szformat), "[\"%%.%df", iValueFormat);
				}
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), szformat, stWebPrivate.pSigValue->varValue.fValue);
				if(ucSigType == SIG_TYPE_SAMPLING)
				{
					if( (pSampleSigValue->pStdSig->iSigID >= CURR_TRANSDUCER_SAMPLE_SIG_ID_START ) && (pSampleSigValue->pStdSig->iSigID <= VOLT_TRANSDUCER_SAMPLE_SIG_ID_END) )
					{
						memset(cUnitBuf_En,0,sizeof(cUnitBuf_En));
						memset(cUnitBuf_Loc,0,sizeof(cUnitBuf_Loc));

						Web_GetTransducer_Units(pUnitBuf_En,pUnitBuf_Loc,pEquipInfo,pSampleSigValue);
				    	iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "\",[\"%s\",\"%s\"]],",pUnitBuf_En,pUnitBuf_Loc);
					}
					else
					{
				    	iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
					}
				}
				else if(ucSigType == SIG_TYPE_SETTING)
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
				}
				else
				{}
		    }
		    else if(stWebPrivate.pSigValue->ucType == VAR_UNSIGNED_LONG)
		    {
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld", stWebPrivate.pSigValue->varValue.ulValue);
				if(ucSigType == SIG_TYPE_SAMPLING)
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
				}
				else if(ucSigType == SIG_TYPE_SETTING)
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
				}
				else
				{}
		    }
		    else if(stWebPrivate.pSigValue->ucType == VAR_DATE_TIME)
		    {
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld\",", stWebPrivate.pSigValue->varValue.dtValue);
		    }
		    else if(stWebPrivate.pSigValue->ucType == VAR_ENUM)
		    {
				itemp = stWebPrivate.pSigValue->varValue.enumValue;
				if(ucSigType == SIG_TYPE_SAMPLING)
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[[\"%s\",\"%s\"],", pSampleSigValue->pStdSig->pStateText[itemp]->pFullName[0],
					pSampleSigValue->pStdSig->pStateText[itemp]->pFullName[1]);
				}
				else if(ucSigType == SIG_TYPE_SETTING)
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[[\"%s\",\"%s\"],", pSettingSigValue->pStdSig->pStateText[itemp]->pFullName[0],
					pSettingSigValue->pStdSig->pStateText[itemp]->pFullName[1]);
				}
				else
				{}
		    }
		    else
		    {
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"\",");
		    }


		    if(ucSigType == SIG_TYPE_SAMPLING)
		    {
				if(pSampleSigValue->sv.iRelatedAlarmLevel > 0) 
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
				}
				else
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
				}
		    }
		    else
		    {
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
		    }
		    
		    if(ucSigType == SIG_TYPE_SAMPLING)
		    {
				if(SIG_VALUE_IS_VALID(&pSampleSigValue->bv))
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
				}
				else
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
				}

				if( ((pSampleSigValue->pStdSig->iSigID >= CURR_TRANSDUCER_SAMPLE_SIG_ID_START ) && (pSampleSigValue->pStdSig->iSigID <=VOLT_TRANSDUCER_SAMPLE_SIG_ID_END)) || ((pSampleSigValue->pStdSig->iSigID >=CABINET_VOLTAGE_SIG_ID_START) && (pSampleSigValue->pStdSig->iSigID <=CABINET_VOLTAGE_SIG_ID_END)) )
				{
					if(SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv))
					{
					    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0],");

					}
					else
					{
					    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
					}
				}
				else
				{
					iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
				}


		    }
		    else
		    {
				if(SIG_VALUE_IS_VALID(&pSettingSigValue->bv))
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
				}
				else
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
				}
				if(SIG_VALUE_IS_CONFIGURED(&pSettingSigValue->bv))
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0],");
				}
				else
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
				}
		    }
		}
    }

    return;
}

/*==========================================================================*
* FUNCTION :  Makesignalstring_SMDUE
* PURPOSE  :  To make the signal value string
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  stWebPrivate: The structure indicate the signal
*             szBuf       : The string save space
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
void Makesignalstring_SMDUE(WEB_GENERAL_RECT_SIGNAL_INFO stWebPrivate, char *szBuf, int iBufLength)
{
    int	iBufLen = 0;
    unsigned char	ucSigType;
    SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
    SET_SIG_VALUE     *pSettingSigValue = NULL;
    int iValueFormat;
    char szformat[32];
    int itemp;
    BOOL bdisplay;
    BOOL bdisplay1;

    iBufLen = 0;
    if(stWebPrivate.bSingleValid == FALSE)
    {
	iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"---\",0,1,1],");
    }
    else
    {
	ucSigType = stWebPrivate.pSigValue->ucSigType;
	if(ucSigType == SIG_TYPE_SAMPLING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue))->pStdSig), DISPLAY_WEB);
	    pSampleSigValue = (SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue);
	}
	else if(ucSigType == SIG_TYPE_SETTING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(stWebPrivate.pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(stWebPrivate.pSigValue))->pStdSig), DISPLAY_WEB);
	    pSettingSigValue = (SET_SIG_VALUE *)(stWebPrivate.pSigValue);
	}
	else
	{}
	if((bdisplay == TRUE) && (bdisplay1 == TRUE))
	{
	    if(stWebPrivate.pSigValue->ucType == VAR_LONG)
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld", stWebPrivate.pSigValue->varValue.lValue);
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
		}
		else
		{}
	    }
	    else if(stWebPrivate.pSigValue->ucType == VAR_FLOAT)
	    {
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iValueFormat = Web_TransferFormatToInt(pSampleSigValue->pStdSig->szValueDisplayFmt);
		}
		else
		{
		    iValueFormat = Web_TransferFormatToInt(pSettingSigValue->pStdSig->szValueDisplayFmt);
		}
		snprintf(szformat, sizeof(szformat), "[\"%%.%df", iValueFormat);

		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), szformat, stWebPrivate.pSigValue->varValue.fValue);
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
		}
		else
		{}
	    }
	    else if(stWebPrivate.pSigValue->ucType == VAR_UNSIGNED_LONG)
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld", stWebPrivate.pSigValue->varValue.ulValue);
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
		}
		else
		{}
	    }
	    else if(stWebPrivate.pSigValue->ucType == VAR_DATE_TIME)
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld\",", stWebPrivate.pSigValue->varValue.dtValue);
	    }
	    else if(stWebPrivate.pSigValue->ucType == VAR_ENUM)
	    {
		itemp = stWebPrivate.pSigValue->varValue.enumValue;
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[[\"%s\",\"%s\"],", pSampleSigValue->pStdSig->pStateText[itemp]->pFullName[0],
			pSampleSigValue->pStdSig->pStateText[itemp]->pFullName[1]);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[[\"%s\",\"%s\"],", pSettingSigValue->pStdSig->pStateText[itemp]->pFullName[0],
			pSettingSigValue->pStdSig->pStateText[itemp]->pFullName[1]);
		}
		else
		{}
	    }
	    else
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"\",");
	    }
	    if(ucSigType == SIG_TYPE_SAMPLING)
	    {
			if(pSampleSigValue->sv.iRelatedAlarmLevel > 0) 
			{
			    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
			}
			else
			{
			    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
			}
	    }
	    else
	    {
			iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
	    }
	    if(ucSigType == SIG_TYPE_SAMPLING)
	    {
			if(SIG_VALUE_IS_VALID(&pSampleSigValue->bv))
			{
			    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
			}
			else
			{
			    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
			}

			if( ((pSampleSigValue->pStdSig->iSigID >= 1 ) && (pSampleSigValue->pStdSig->iSigID <=10)) || ((pSampleSigValue->pStdSig->iSigID >=61) && (pSampleSigValue->pStdSig->iSigID <=70)) )
			{
				if(SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv))
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0],");
				}
				else
				{
				    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
				}
			}
			else
			{
				iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
			}
	    }
	    else
	    {
			if(SIG_VALUE_IS_VALID(&pSettingSigValue->bv))
			{
			    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
			}
			else
			{
			    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
			}
			if(SIG_VALUE_IS_CONFIGURED(&pSettingSigValue->bv))
			{
			    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0],");
			}
			else
			{
			    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
			}
	    }
	}
    }
}

//??����Custom_input_stats��3??��???��D2��?��D?o?ID?a111-130o��41-50��?��y?Y??��??��
/*==========================================================================*
* FUNCTION :  Makesignalstring
* PURPOSE  :  To make the signal value string
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  stWebPrivate: The structure indicate the signal
*             szBuf       : The string save space
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
void Makesignalstring(WEB_GENERAL_RECT_SIGNAL_INFO stWebPrivate, char *szBuf, int iBufLength)
{
    int	iBufLen = 0;
    unsigned char	ucSigType;
    SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
    SET_SIG_VALUE     *pSettingSigValue = NULL;
    int iValueFormat;
    char szformat[32];
    int itemp;
    BOOL bdisplay;
    BOOL bdisplay1;

    iBufLen = 0;
    if(stWebPrivate.bSingleValid == FALSE)
    {
	iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"---\",0,1,1],");
    }
    else
    {
	ucSigType = stWebPrivate.pSigValue->ucSigType;
	if(ucSigType == SIG_TYPE_SAMPLING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue))->pStdSig), DISPLAY_WEB);
	    pSampleSigValue = (SAMPLE_SIG_VALUE *)(stWebPrivate.pSigValue);
	}
	else if(ucSigType == SIG_TYPE_SETTING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(stWebPrivate.pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(stWebPrivate.pSigValue))->pStdSig), DISPLAY_WEB);
	    pSettingSigValue = (SET_SIG_VALUE *)(stWebPrivate.pSigValue);
	}
	else
	{}
	if((bdisplay == TRUE) && (bdisplay1 == TRUE))
	{
	    if(stWebPrivate.pSigValue->ucType == VAR_LONG)
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld", stWebPrivate.pSigValue->varValue.lValue);
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
		}
		else
		{}
	    }
	    else if(stWebPrivate.pSigValue->ucType == VAR_FLOAT)
	    {
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iValueFormat = Web_TransferFormatToInt(pSampleSigValue->pStdSig->szValueDisplayFmt);
		}
		else
		{
		    iValueFormat = Web_TransferFormatToInt(pSettingSigValue->pStdSig->szValueDisplayFmt);
		}
		snprintf(szformat, sizeof(szformat), "[\"%%.%df", iValueFormat);// for example "["%.1f"
		//TRACE_WEB_USER("szformat is %s\n", szformat);
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), szformat, stWebPrivate.pSigValue->varValue.fValue);
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
		}
		else
		{}
	    }
	    else if(stWebPrivate.pSigValue->ucType == VAR_UNSIGNED_LONG)
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld", stWebPrivate.pSigValue->varValue.ulValue);
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSampleSigValue->pStdSig->szSigUnit);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), " %s\",", pSettingSigValue->pStdSig->szSigUnit);
		}
		else
		{}
	    }
	    else if(stWebPrivate.pSigValue->ucType == VAR_DATE_TIME)
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"%ld\",", stWebPrivate.pSigValue->varValue.dtValue);
	    }
	    else if(stWebPrivate.pSigValue->ucType == VAR_ENUM)
	    {
		itemp = stWebPrivate.pSigValue->varValue.enumValue;
		if(ucSigType == SIG_TYPE_SAMPLING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[[\"%s\",\"%s\"],", pSampleSigValue->pStdSig->pStateText[itemp]->pFullName[0],
			pSampleSigValue->pStdSig->pStateText[itemp]->pFullName[1]);
		}
		else if(ucSigType == SIG_TYPE_SETTING)
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[[\"%s\",\"%s\"],", pSettingSigValue->pStdSig->pStateText[itemp]->pFullName[0],
			pSettingSigValue->pStdSig->pStateText[itemp]->pFullName[1]);
		}
		else
		{}
	    }
	    else
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "[\"\",");
	    }
	    if(ucSigType == SIG_TYPE_SAMPLING)
	    {
		if(pSampleSigValue->sv.iRelatedAlarmLevel > 0) //here add the value state---alarm state
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
		}
	    }
	    else
	    {
		iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
	    }
	    if(ucSigType == SIG_TYPE_SAMPLING)
	    {
		if(SIG_VALUE_IS_VALID(&pSampleSigValue->bv))
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
		}
		if(SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv))
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0],");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
		}
	    }
	    else
	    {
		if(SIG_VALUE_IS_VALID(&pSettingSigValue->bv))
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0,");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1,");
		}
		if(SIG_VALUE_IS_CONFIGURED(&pSettingSigValue->bv))
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "0],");
		}
		else
		{
		    iBufLen += snprintf(szBuf + iBufLen, (iBufLength - iBufLen), "1],");
		}
	    }
	}
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeSingleSMDUEDataFile
* PURPOSE  :  To make the other equip data string like SMIO
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  itype: indicate which kind of equip such as SMIO etc
* RETURN   :  char * : the pointer pointing to the string including the equip data.
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
char* Web_MakeSingleSMDUEDataFile(int itype,int * pSmdueTableDis)
{
    int i, j, k, iAllSignalNum = 0, iEquipNum = 0;
    char *szTemp = NULL;
    int iFileLen = 0;
    char szBuf[128];
    BOOL bdisplay;
    BOOL bdisplay1;
    int		iBufLength;
	int iDisplaySmdue=0;
	int iDisplaySmdueTable=0;

	
    if(gs_pWebOtherSigInfo[itype] == NULL)
    {
    	*pSmdueTableDis =1;
		return NULL;
    }
    if((gs_pWebOtherSigInfo[itype]->stWebEquipInfo == NULL) || (gs_pWebOtherSigInfo[itype]->stWebPrivate == NULL))
    {
       	*pSmdueTableDis =1;
		return NULL;
    }

    if(itype == SMDUE_SEQ)
    {
		iEquipNum = gs_uiRealSMDUENum;
    }


    if((gs_pWebOtherSigInfo[itype] != NULL) && (gs_pWebOtherSigInfo[itype]->stWebEquipInfo != NULL))
    {
		for(i = 0; i < iEquipNum; i++)
		{
		    iAllSignalNum += gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].iSamplenum;
		}
    }


    if(iAllSignalNum > 0)
    {
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;

	iBufLength = 80 * iAllSignalNum + 90 * iAllSignalNum + 100;
	
	k = 0;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");

	
	for(i = 0; i < iEquipNum; i++)
	{
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");

		iDisplaySmdue = Web_DC_SMDUE_Display(gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo);
		if( iDisplaySmdue == 0)
		{
			iDisplaySmdueTable++;
		}

	    if(gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].bSingleValid == FALSE)
	    {
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\",%d],",iDisplaySmdue);
	    }
	    else
	    {
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\",%d],", gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[0],
		    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[1],iDisplaySmdue);
	    }
	    
	    for(j = 0; j < gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].iSamplenum; j++)
	    {
			if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].bSingleValid == FALSE)
			{
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"---\",\"---\"],");
			}
			else
			{
				if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue->ucSigType == SIG_TYPE_SAMPLING)
				{
					bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->bv);
					bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig), DISPLAY_WEB);

					if((bdisplay == TRUE) && (bdisplay1 == TRUE))
					{
						iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
						((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[0],
						((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[1]);
					}
				}
				else if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue->ucSigType == SIG_TYPE_SETTING)
				{
					bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->bv);
					bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig), DISPLAY_WEB);
					if((bdisplay == TRUE) && (bdisplay1 == TRUE))
					{
						iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
						((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[0],
						((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[1]);
					}
				}
				else
				{}
			}
			
			memset(szBuf, 0, sizeof(szBuf));

			Makesignalstring_SMDUE(gs_pWebOtherSigInfo[itype]->stWebPrivate[k], szBuf, sizeof(szBuf));

			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
			if((bdisplay == TRUE) && (bdisplay1 == TRUE))
			{
				if(iFileLen >= 1)
				{
				iFileLen = iFileLen - 1;	
				}
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
			}
			k++;
		}
		if(iFileLen >= 1)
		{
		iFileLen = iFileLen - 1;	
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	
	}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");


		if( 0 == iDisplaySmdueTable )
		{
			*pSmdueTableDis = 1;
		}
		else
		{
			*pSmdueTableDis = 0;
		}
		
		return szTemp;
    }
    else
    {
       	*pSmdueTableDis =1;
		return NULL;
    }
}


/*==========================================================================*
* FUNCTION :  Web_MakeSingleOtherEquipDataFile1
* PURPOSE  :  To make the other equip data string like SMIO
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  itype: indicate which kind of equip such as SMIO etc
* RETURN   :  char * : the pointer pointing to the string including the equip data.
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
char* Web_MakeSingleOtherEquipDataFile1(int itype)
{
    int i, j, k, iAllSignalNum = 0, iEquipNum = 0;
    char *szTemp = NULL;
    int iFileLen = 0;
    char szBuf[128];
    BOOL bdisplay;
    BOOL bdisplay1;
    int		iBufLength;

    TRACE_WEB_USER("Begin to exec Web_MakeSingleOtherEquipDataFile1[%d]\n", itype);
    //check the pointer valid begin
    if(gs_pWebOtherSigInfo[itype] == NULL)
    {
	return NULL;
    }
    if((gs_pWebOtherSigInfo[itype]->stWebEquipInfo == NULL) || (gs_pWebOtherSigInfo[itype]->stWebPrivate == NULL))
    {
	return NULL;
    }
    //check the pointer valid end

    if(itype == SMIO_SEQ)
    {
	iEquipNum = gs_uiSMIONum;
    }
    else if(itype == SMDUP_SEQ)
    {
	iEquipNum = gs_uiRealSMDUPNum;
	
    }
    else if(itype == SMDUH_SEQ)
    {
	iEquipNum = gs_uiRealSMDUHNum;
    }
     else if(itype == SMDUE_SEQ)
    {
	iEquipNum = gs_uiRealSMDUENum;
    }
     else if(itype == SMDU_SEQ)
    {
	iEquipNum = gs_uiRealSMDUNum;
    }
     else if(itype == EIB_SEQ)
    {
	iEquipNum = gs_uiRealEIBNum;
    }
    else
    {}


    if((gs_pWebOtherSigInfo[itype] != NULL) && (gs_pWebOtherSigInfo[itype]->stWebEquipInfo != NULL))
    {
		for(i = 0; i < iEquipNum; i++)
		{
		    iAllSignalNum += gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].iSamplenum;
		}
    }


    if(iAllSignalNum > 0)
    {
	//szTemp = NEW(char, (80 * iAllSignalNum + 90 * iAllSignalNum + 100));
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	/*if(szTemp == NULL)
	{
	    AppLogOut("Web_MakeSingleDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    return NULL;
	}*/
	iBufLength = 80 * iAllSignalNum + 90 * iAllSignalNum + 100;
	
	k = 0;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");

	
	for(i = 0; i < iEquipNum; i++)
	{
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");

	    if(gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].bSingleValid == FALSE)
	    {
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
	    }
	    else
	    {
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],", gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[0],
		    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[1]);
	    }
	    
	    for(j = 0; j < gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].iSamplenum; j++)
	    {
			//TRACE_WEB_USER("Get signal value, exec [%d][%d] signal\n", i, j);
			if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].bSingleValid == FALSE)
			{
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"---\",\"---\"],");
			}
			else
			{
				if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue->ucSigType == SIG_TYPE_SAMPLING)
				{
					bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->bv);
					bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig), DISPLAY_WEB);

					if((bdisplay == TRUE) && (bdisplay1 == TRUE))
					{
						iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
						((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[0],
						((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[1]);
					}
				}
				else if(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue->ucSigType == SIG_TYPE_SETTING)
				{
					bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->bv);
					bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig), DISPLAY_WEB);
					if((bdisplay == TRUE) && (bdisplay1 == TRUE))
					{
						iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[\"%s\",\"%s\"],",
						((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[0],
						((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue))->pStdSig->pSigName->pFullName[1]);
					}
				}
				else
				{}
			}
			
			memset(szBuf, 0, sizeof(szBuf));

			Makesignalstring(gs_pWebOtherSigInfo[itype]->stWebPrivate[k], szBuf, sizeof(szBuf));

			//TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
			//TRACE_WEB_USER("[Web_MakeSingleBattDataFile] szBuf is %s\n", szBuf);
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
			if((bdisplay == TRUE) && (bdisplay1 == TRUE))
			{
				if(iFileLen >= 1)
				{
				iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
				}
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
			}
			k++;
		}
		if(iFileLen >= 1)
		{
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
		//printf("end to exec Web_MakeSingleOtherEquipDataFile1[%d] iFileLen = %d\n", itype, iFileLen);
		return szTemp;
    }
    else
    {
		TRACE_WEB_USER("end to exec Web_MakeSingleOtherEquipDataFile1[%d]\n", itype);
		return NULL;
    }
}
//changed by Frank Wu,18/N/35,20140527, for adding the the web setting tab page 'DI'
static char* Web_MakeDISetDataFile(int iType1, int iType2)
{
	int i = 0;
	char *szTemp = NULL;
	int iFileLen = 0;
	int iBufLength = 0;
	char xOut[128];

	int iIndex = 0;
	int iSignalPairNum = 0;
	WEB_GENERAL_SETTING_INFO *pWebInfoItem = NULL;
	ALARM_SIG_VALUE *pAlarmSig = NULL;
	EQUIP_INFO *pEquipInfo = NULL;

	WEB_GENERAL_SETTING_INFO *pWebInfoItem2 = NULL;
	EQUIP_INFO *pEquipInfo2 = NULL;

	TRACE_WEB_USER("Begin to exec Web_MakeDISetDataFile(iType1=%d, iType2=%d)\n", iType1, iType2);

	static BOOL s_bFirstRunDI = TRUE;
	static BOOL			g_bEquipIB1ExistDI = FALSE;
	//printf("Begin to exec Web_MakeDOSetDataFile(iType1=%d)\n", iType1);
	if(s_bFirstRunDI )
	{
		g_bEquipIB1ExistDI = Web_GetIB1State();
		
		s_bFirstRunDI = FALSE;
	}

	//check the pointer valid begin
	if((NULL == gs_pWebSetInfo[iType1].stWebPrivateSet)
		|| (NULL == gs_pWebSetInfo[iType2].stWebPrivateSet)
		)
	{
		return NULL;
	}

	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iFileLen = 0;
	//init
	snprintf(szTemp, strlen("[]"), "[]");

	if( (DI_BASIC_SEQ == iType1) && (DI_STATE_SEQ == iType2) )
	{
		if(gs_pWebSetInfo[iType1].iNumber < gs_pWebSetInfo[iType2].iNumber)
		{
			iSignalPairNum = gs_pWebSetInfo[iType1].iNumber;
		}
		else
		{
			iSignalPairNum = gs_pWebSetInfo[iType2].iNumber;;
		}
		iBufLength = MAX_SETTING_LEN * iSignalPairNum + 1024;

		//head
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
		//body
		for(i = 0; i < iSignalPairNum; i++)
		{
			//equip existed
			if(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pEquipInfo->iEquipID == 202)
				
			{
				if(g_bEquipIB1ExistDI == FALSE)//IB1)
				{
					continue;
				}
			}
			else if(!gs_pWebSetInfo[iType1].stWebPrivateSet[i].bWorkStatus 
				|| !gs_pWebSetInfo[iType1].stWebPrivateSet[i].pEquipInfo 
				|| !gs_pWebSetInfo[iType1].stWebPrivateSet[i].pEquipInfo->bWorkStatus)
			{
				continue;
			}
/*
			//bSetVaild
			if( (!gs_pWebSetInfo[iType1].stWebPrivateSet[i].bSetVaild)
				|| (!gs_pWebSetInfo[iType2].stWebPrivateSet[i].bSetVaild)
				)
			{
				continue;
			}
			//bWorkStatus
			if( (!gs_pWebSetInfo[iType1].stWebPrivateSet[i].bWorkStatus)
				|| (!gs_pWebSetInfo[iType2].stWebPrivateSet[i].bWorkStatus)
				)
			{
				continue;
			}
*/
			//bdisplay
			//just display the SIG_TYPE_ALARM signals from DI_BASIC_SEQ
			if(SIG_TYPE_ALARM == gs_pWebSetInfo[iType1].stWebPrivateSet[i].iSignalType)
			{
				iIndex++;

				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet[i];
				pAlarmSig = (ALARM_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;

				pWebInfoItem2 = &gs_pWebSetInfo[iType2].stWebPrivateSet[i];
				pEquipInfo2 = pWebInfoItem2->pEquipInfo;
				//the format of one data item in Web UI:
				//[
				//	index,
				//	[
				//		iEquipId,
				//		iSignalType,
				//		iSignalId,
				//		iAlarmRelay,
				//		iAlarmLevel, 
				//		["sEquipName_en", "sEquipName_local"], 
				//		["sSignalName_en", "sSignalName_local"],
				//		["sSignalAbbrName_en", "sSignalAbbrName_local"]
				//	],
				//	[
				//		iEquipId2,
				//		iSignalType2,
				//		iSignalId2,
				//		iAlarmState
				//	]
				//]

				//split char between two items
				if(iIndex != 1)
				{
					iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
				}

				//[iIndex,
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,",
									iIndex);

				//[iEquipID, iSignalType, iSignalID,
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID);

				//iAlarmDelay, iAlarmLevel,
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"%d,%d,",
									pAlarmSig->pStdSig->iAlarmRelayNo,
									pAlarmSig->pStdSig->iAlarmLevel);

				//["sEquipName_en", "sEquipName_local"],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pEquipInfo->pEquipName->pFullName[0],
									pEquipInfo->pEquipName->pFullName[1]);
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
			
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pAlarmSig->pStdSig->pSigName->pFullName[0],
									pAlarmSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"]],",
									pAlarmSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pAlarmSig->pStdSig->pSigName->pFullName[0],
					pAlarmSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    /*TRACE_WEB_USER_NOT_CYCLE("result is %d\n", result);
				    TRACE_WEB_USER_NOT_CYCLE("Original string %s\n", pAlarmSig->pStdSig->pSigName->pAbbrName[1]);
				    TRACE_WEB_USER_NOT_CYCLE("later string %s\n", xOut);*/
				    //["sSignalAbbrName_en", "sSignalAbbrName_local"]],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"]],",
					pAlarmSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{
				
				//["sSignalName_en", "sSignalName_local"],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pAlarmSig->pStdSig->pSigName->pFullName[0],
									pAlarmSig->pStdSig->pSigName->pFullName[1]);
				//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"]],",
									pAlarmSig->pStdSig->pSigName->pAbbrName[0],
									pAlarmSig->pStdSig->pSigName->pAbbrName[1]);
				}
				//[iEquip2ID, iSignal2Type, iSignal2ID,iAlarmState]]
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d]]",
									pWebInfoItem2->iEquipID,
									pWebInfoItem2->iSignalType,
									pWebInfoItem2->iSignalID,
									pWebInfoItem2->pSigValue->varValue.enumValue);
			}
		}
		//tail
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	}

	TRACE_WEB_USER("end to exec Web_MakeDISetDataFile(iType1=%d, iType2=%d)\n", iType1, iType2);
	return szTemp;
}


//changed by Frank Wu,18/N/35,20140527, for adding the the web setting tab page 'DI'
static char* Web_MakeDOSetDataFile(int iType1)
{
	int i = 0, j = 0;
	char *szTemp = NULL;
	int iFileLen = 0;
	int iBufLength = 0;
	char xOut[128];
	char szBuf[4096];
	int iBufLen  = 0;

	int iIndex = 0;
	int iSignalPairNum = 0;
	WEB_GENERAL_SETTING_INFO *pWebInfoItem = NULL;
	CTRL_SIG_VALUE *pCtlSig = NULL;
	SET_SIG_VALUE *pSetSig = NULL;
	EQUIP_INFO *pEquipInfo = NULL;
	static BOOL s_bFirstRun = TRUE;

	//printf("Begin to exec Web_MakeDOSetDataFile(iType1=%d)\n", iType1);
	if(s_bFirstRun && DO_BASIC_SEQ == iType1)
	{
		g_bEquipIB1Exist = Web_GetIB1State();
		
		s_bFirstRun = FALSE;
	}
	//printf("g_bEquipIB1Exist = %d\n", g_bEquipIB1Exist);
	//check the pointer valid begin
	if(NULL == gs_pWebSetInfo[iType1].stWebPrivateSet)		
	{
		return NULL;
	}
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iFileLen = 0;
	//init
	snprintf(szTemp, strlen("[]"), "[]");

	if( DO_BASIC_SEQ == iType1 || DO_RELAY_BASIC_SEQ == iType1 )
	{
		iSignalPairNum = gs_pWebSetInfo[iType1].iNumber;		
		
		//printf("iSignalPairNum = %d\n", iSignalPairNum);
		iBufLength = MAX_SETTING_LEN * iSignalPairNum + 1024;
		//head
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
		//body

//		printf("iSignalPairNum=%d itype=%d\n",iSignalPairNum,iType1);

		for(i = 0; i < iSignalPairNum; i++)
		{
			//bWorkStatus
			//printf("bWorkStatus -------%d\n", i);
			//printf("bWorkStatus = %d\n", gs_pWebSetInfo[iType1].stWebPrivateSet[i].bWorkStatus);
			if(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pEquipInfo->iEquipID == 202)
				
			{
				if(g_bEquipIB1Exist == FALSE)//IB1)
				{
					continue;
				}
			}
			else if(!gs_pWebSetInfo[iType1].stWebPrivateSet[i].bWorkStatus 
				|| !gs_pWebSetInfo[iType1].stWebPrivateSet[i].pEquipInfo 
				|| !gs_pWebSetInfo[iType1].stWebPrivateSet[i].pEquipInfo->bWorkStatus)
			{
				continue;
			}
//			printf("bWorkStatus i=%d  [%d*%d*%d]   ",i,gs_pWebSetInfo[iType1].stWebPrivateSet[i].bWorkStatus,
//				gs_pWebSetInfo[iType1].stWebPrivateSet[i].pEquipInfo,gs_pWebSetInfo[iType1].stWebPrivateSet[i].pEquipInfo->bWorkStatus);
			//bdisplay
			//just display the SIG_TYPE_CTL signals from DO_BASIC_SEQ
			if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet[i].iSignalType)
			{
				iIndex++;
				//printf("bWorkStatus   -3\n");
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet[i];
				pCtlSig = (CTRL_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;		
				

				//split char between two items
				if(iIndex != 1)
				{
					iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
				}

				
				//printf("bWorkStatus   -4\n");
				//[iEquipID, iSignalType, iSignalID,
				if(pCtlSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pCtlSig->bv.varValue.fValue,
									(CTRL_SIG_IS_CTRL_ON_UI(pCtlSig->pStdSig,CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pCtlSig->bv) : 0,
									pCtlSig->pStdSig->iSigValueType,
									pCtlSig->pStdSig->szSigUnit);
				}
				else 
				{
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pCtlSig->bv.varValue.enumValue,
									(CTRL_SIG_IS_CTRL_ON_UI(pCtlSig->pStdSig,CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pCtlSig->bv) : 0,
									pCtlSig->pStdSig->iSigValueType,
									pCtlSig->pStdSig->szSigUnit);
				}
				//printf("bWorkStatus   -5\n");
				//["sEquipName_en", "sEquipName_local"],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pEquipInfo->pEquipName->pFullName[0],
									pEquipInfo->pEquipName->pFullName[1]);
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
			
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pCtlSig->pStdSig->pSigName->pFullName[0],
									pCtlSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pCtlSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pCtlSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pCtlSig->pStdSig->pSigName->pFullName[0],
					pCtlSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pCtlSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);				  
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pCtlSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{
				//printf("bWorkStatus   -6\n");
				//["sSignalName_en", "sSignalName_local"],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pCtlSig->pStdSig->pSigName->pFullName[0],
									pCtlSig->pStdSig->pSigName->pFullName[1]);
				//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pCtlSig->pStdSig->pSigName->pAbbrName[0],
									pCtlSig->pStdSig->pSigName->pAbbrName[1]);
				}
			}
			else if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet[i].iSignalType)
			{
				iIndex++;
				//printf("bWorkStatus   -3\n");
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet[i];
				pSetSig = (SET_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

				//split char between two items
				if(iIndex != 1)
				{
					iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
				}
				
				//printf("bWorkStatus   -4\n");
				//[iEquipID, iSignalType, iSignalID,


				if(pSetSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSetSig->bv.varValue.fValue,
									(SET_SIG_IS_SET_ON_UI(pSetSig->pStdSig,CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pSetSig->bv) : 0,
									pSetSig->pStdSig->iSigValueType,
									pSetSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSetSig->bv.varValue.enumValue,
									(SET_SIG_IS_SET_ON_UI(pSetSig->pStdSig,CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pSetSig->bv) : 0,
									pSetSig->pStdSig->iSigValueType,
									pSetSig->pStdSig->szSigUnit);
				}

				//printf("bWorkStatus   -5\n");
				//["sEquipName_en", "sEquipName_local"],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pEquipInfo->pEquipName->pFullName[0],
									pEquipInfo->pEquipName->pFullName[1]);
				//printf("bWorkStatus   -555\n");
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
			
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSetSig->pStdSig->pSigName->pFullName[0],
									pSetSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSetSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSetSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSetSig->pStdSig->pSigName->pFullName[0],
					pSetSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSetSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSetSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{
				//printf("bWorkStatus   -6\n");
				//["sSignalName_en", "sSignalName_local"],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSetSig->pStdSig->pSigName->pFullName[0],
									pSetSig->pStdSig->pSigName->pFullName[1]);
				//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSetSig->pStdSig->pSigName->pAbbrName[0],
									pSetSig->pStdSig->pSigName->pAbbrName[1]);
				}
			}
			if(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue->ucType == VAR_ENUM)
			{
			    memset(szBuf, 0, sizeof(szBuf));
			    iBufLen = 0;
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[");
			    for(j = 0; j < ((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue))->pStdSig->iStateNum; j++)
			    {
				iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
				    ((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
				    ((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
			    }
			    if(iBufLen >= 1)
			    {
				iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
			    }
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "]");
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s]",szBuf);
			}
			else if(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue->ucType == VAR_FLOAT
				|| gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue->ucType == VAR_UNSIGNED_LONG
				|| gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue->ucType == VAR_LONG)
			{
				 memset(szBuf, 0, sizeof(szBuf));
				iBufLen = 0;
				iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "");
				
				iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[[%f,%f]],", 
					((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue))->pStdSig->fMinValidValue,
					((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet[i].pSigValue))->pStdSig->fMaxValidValue);
			
				if(iBufLen >= 1)
				{
					iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
				}
				iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "");
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s]",szBuf);
			}
		}
		
		//printf("bWorkStatus   -7\n");
		//tail
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	}

	TRACE_WEB_USER("end to exec Web_MakeDISetDataFile(iType1=%d)szTemp = %s\n", iType1, szTemp);
	return szTemp;
}


static void Web_Get_AI_Set_Type(int iEquipID,int iSignalID,int * pVolType,int * pTransducerType,int * pIsDisplay)
{
#define VOLTAGE_SAMPLE_TO_SETTING_OFFEST	20
#define TRANSDUCER_SAMPLE_TO_SETTING_OFFEST	40
#define NOT_DISPLAY	1
#define DISPLAY	0


	int iVarSubID,iError,iBufLen,iSigID;
	SIG_BASIC_VALUE		*pSigBasicVal;

	int iTypeV,iTypeTran,iIsDisplay;

	iSigID = iSignalID + VOLTAGE_SAMPLE_TO_SETTING_OFFEST;
	
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, iSigID);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipID,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigBasicVal,
	    0);
	if(iError == ERR_DXI_OK)
	{
	   iTypeV = pSigBasicVal->varValue.enumValue;
	}
	else
	{
	   iTypeV = 0;
	}

	iSigID = iSignalID + TRANSDUCER_SAMPLE_TO_SETTING_OFFEST;
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, iSigID);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipID,
	    iVarSubID,
	    &iBufLen,
	    (void *)&pSigBasicVal,
	    0);
	if(iError == ERR_DXI_OK)
	{
	   iTypeTran = pSigBasicVal->varValue.enumValue;
	}
	else
	{
	   iTypeTran = 0;
	}

	if( (0==iTypeV) && ( (iTypeTran < 2) || (iTypeTran > 3) ) )
	{
		iIsDisplay = NOT_DISPLAY;
	}
	else
	{
		iIsDisplay = DISPLAY;
	}

	*pVolType = iTypeV;
	*pTransducerType = iTypeTran;
	*pIsDisplay = iIsDisplay;

	return;
}

static char* Web_MakeAnalogSetDataFile(int iType1)
{
#define MAX_PER_SMUDE_ANALOG_FUNCTION_NUM	 38

	int i = 0, j = 0;
	char *szTemp = NULL;
	int iFileLen = 0;
	int iBufLength = 0;
	char xOut[128];
	char szBuf[8192];
	int iBufLen  = 0;

	int iIndex = 0;
	int iSignalPairNum = 0;
	WEB_GENERAL_SETTING_INFO *pWebInfoItem = NULL;
	CTRL_SIG_VALUE *pCtlSig = NULL;
	SET_SIG_VALUE *pSetSig = NULL;
	SAMPLE_SIG_VALUE *pSampleSig = NULL;
	ALARM_SIG_VALUE *pAlarmSig = NULL;
	EQUIP_INFO *pEquipInfo = NULL;
	int iCycleTimes = 0, iMaxSignalNumber = 0;

	int iVolType=0,iTransducerType=0,iInputBlockDisplay=1;

	if(NULL == gs_pWebSetInfo[iType1].stWebPrivateSet1)		
	{
		return NULL;
	}

	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iFileLen = 0;

	snprintf(szTemp, strlen("[]"), "[]");
	
	iSignalPairNum = gs_pWebSetInfo[iType1].iNumber;

	if(SMDUE_ANALOG_SEQ == iType1)
	{
		iCycleTimes = iSignalPairNum*gs_uiRealSMDUENum;

		iMaxSignalNumber = MAX_PER_SMUDE_ANALOG_FUNCTION_NUM;

		iBufLength = MAX_SETTING_LEN * iSignalPairNum * 8 + 1024;

		//head
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
		//body
		for(i = 0; i < iCycleTimes; i++)
		{

			if(!gs_pWebSetInfo[iType1].stWebPrivateSet1[i].bWorkStatus)
			{
				continue;
			}
			
			if(i % iMaxSignalNumber == 0)		//ȡ���õĵ�һ���ź�
			{
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;	

				//printf("Web_MakeAnalogSetDataFile(): pWebInfoItem->iEquipID=%d,pWebInfoItem->iSignalID=%d-----------------------------------------1\n",pWebInfoItem->iEquipID,pWebInfoItem->iSignalID);

				Web_Get_AI_Set_Type(pWebInfoItem->iEquipID,pWebInfoItem->iSignalID,&iVolType,&iTransducerType,&iInputBlockDisplay);
				
				iFileLen += snprintf(szTemp + iFileLen,
							(iBufLength - iFileLen),
							"[%d,%d,%d,%d,%d,[\" %s\",\" %s\"],",
							i / iMaxSignalNumber,
							pWebInfoItem->iEquipID,
							pWebInfoItem->iSignalType,
							pWebInfoItem->iSignalID,
							iInputBlockDisplay,
							pEquipInfo->pEquipName->pFullName[0],
							pEquipInfo->pEquipName->pFullName[1]);

				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{

				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{	
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}
				iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[");
				continue;
			}


			
			if(i % iMaxSignalNumber == 1)//Cabinet1 Volt	
			{
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;	

				iFileLen += snprintf(szTemp + iFileLen,
							(iBufLength - iFileLen),
							"[%d,%d,%d,%d,",
							pWebInfoItem->iEquipID,
							pWebInfoItem->iSignalType,
							pWebInfoItem->iSignalID,
							iVolType);

				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{	

					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}

			iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[");
				continue;
			}


			if(i % iMaxSignalNumber == 10)//Voltage Transducer1	
			{
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;	

				iFileLen += snprintf(szTemp + iFileLen,
							(iBufLength - iFileLen),
							"[%d,%d,%d,%d,",
							pWebInfoItem->iEquipID,
							pWebInfoItem->iSignalType,
							pWebInfoItem->iSignalID,
							iTransducerType);
	
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{	

					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}

			iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[");
				continue;
			}


			if(i % iMaxSignalNumber == 24)//Current Transducer1	
			{
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;	

				iFileLen += snprintf(szTemp + iFileLen,
							(iBufLength - iFileLen),
							"[%d,%d,%d,%d,",
							pWebInfoItem->iEquipID,
							pWebInfoItem->iSignalType,
							pWebInfoItem->iSignalID,
							iTransducerType);
	
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{	

					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}

			iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[");
				continue;
			}

			 if(SIG_TYPE_ALARM == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
					iIndex++;

					pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
					pAlarmSig = (ALARM_SIG_VALUE *)(pWebInfoItem->pSigValue);
					pEquipInfo = pWebInfoItem->pEquipInfo;				

					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[%d,%d,%d,",
										pWebInfoItem->iEquipID,
										pWebInfoItem->iSignalType,
										pWebInfoItem->iSignalID);

					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"%d,%d,%d,%d,\" %s \",",
										pAlarmSig->pStdSig->iAlarmRelayNo,
										pAlarmSig->pStdSig->iAlarmLevel,
										1,//Not used
										VAR_ENUM, //Set it default to enum
										" ");//Unit is null
					
					if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
					{
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										
						KOI8toUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"]],");
					}
					else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
					{
					    iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[\"\",\"\"],");

					    GBKtoUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
					      iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[\"\",\"\"]],");
					}
					else
					{
					

						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"]],");
					}			
			}
			else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{

				iIndex++;
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

				if(pSampleSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSampleSig->bv.varValue.fValue,
									pSampleSig->pStdSig->iSigValueType,
									pSampleSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSampleSig->bv.varValue.enumValue,
									pSampleSig->pStdSig->iSigValueType,
									pSampleSig->pStdSig->szSigUnit);
				}
				
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}
			}
			else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
				iIndex++;
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pCtlSig = (CTRL_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

				if(pCtlSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pCtlSig->bv.varValue.fValue,									
									pCtlSig->pStdSig->iSigValueType,
									pCtlSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pCtlSig->bv.varValue.enumValue,
									pCtlSig->pStdSig->iSigValueType,
									pCtlSig->pStdSig->szSigUnit);
				}

				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
					KOI8toUTF8(pCtlSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"]],");
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");

				    GBKtoUTF8(pCtlSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
				}
				else
				{
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
				}
			}
			else if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
				iIndex++;
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];

				pSetSig = (SET_SIG_VALUE *)(pWebInfoItem->pSigValue);

				pEquipInfo = pWebInfoItem->pEquipInfo;					

				if(pSetSig->pStdSig->iSigValueType == VAR_FLOAT)
				{		
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSetSig->bv.varValue.fValue,									
									pSetSig->pStdSig->iSigValueType,
									pSetSig->pStdSig->szSigUnit);
				}
				else 
				{				
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSetSig->bv.varValue.enumValue,
									pSetSig->pStdSig->iSigValueType,
									pSetSig->pStdSig->szSigUnit);									
				}

				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");

				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");

					KOI8toUTF8(pSetSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");				
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");

				    GBKtoUTF8(pSetSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					
				}
				else
				{
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");										
				}
			}

			if(SIG_TYPE_ALARM == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
			}
			else if(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_ENUM)
			{
			    memset(szBuf, 0, sizeof(szBuf));
			    iBufLen = 0;
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[");
				if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{

					for(j = 0; j < ((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
						iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
					    }
				}
				else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					for(j = 0; j < ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
						iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						 ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
					    }
				}
				else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					for(j = 0; j < ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
						iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						    ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						    ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);

					}
				}
				
				if(iBufLen >= 1)
				{
					iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
				}
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "]");
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s],",szBuf);

			}
			else if(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_FLOAT
				|| gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_UNSIGNED_LONG
				|| gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_LONG)
			{		
				memset(szBuf, 0, sizeof(szBuf));
				iBufLen = 0;
				iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "");
				
				if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				if(iBufLen >= 1)
				{
					iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
				}
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "");
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s],",szBuf);

			}
			
			if(i % iMaxSignalNumber == 9)
			{
				if(iFileLen >= 1)
				{
					iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
				}
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s]],","");
			}

			if(i % iMaxSignalNumber == 23)
			{
				if(iFileLen >= 1)
				{
					iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
				}
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s]],","");
			}
			
			if(i % iMaxSignalNumber == iMaxSignalNumber -1)
			{
				if(iFileLen >= 1)
				{
					iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
				}
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s]],","");
			
				if(iFileLen >= 1)
				{
					iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
				}
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s]],","");
			}
		}
	}
		if(iFileLen > 1)
		{
			iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}		
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");

		return szTemp;
}


static char* Web_MakeCustomInput_SetDataFile(int iType1)
{

	int i = 0, j = 0;
	char *szTemp = NULL;
	int iFileLen = 0;
	int iBufLength = 0;
	char xOut[128];
	char szBuf[4096];
	int iBufLen  = 0;

	int iIndex = 0;
	int iSignalPairNum = 0;
	WEB_GENERAL_SETTING_INFO *pWebInfoItem = NULL;
	CTRL_SIG_VALUE *pCtlSig = NULL;
	SET_SIG_VALUE *pSetSig = NULL;
	SAMPLE_SIG_VALUE *pSampleSig = NULL;
	ALARM_SIG_VALUE *pAlarmSig = NULL;
	EQUIP_INFO *pEquipInfo = NULL;
	int iCycleTimes = 0, iMaxSignalNumber = 0;;

	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iFileLen = 0;


	if(NULL == gs_pWebSetInfo[iType1].stWebPrivateSet1)		
	{
		return NULL;
	}

	snprintf(szTemp, strlen("[]"), "[]");
	iSignalPairNum = gs_pWebSetInfo[iType1].iNumber;
	if( SMDUE_CUSTOM_INPUT_SEQ == iType1)
	{
		iCycleTimes = iSignalPairNum*gs_uiRealSMDUENum ;
		iMaxSignalNumber = 5;
	}
	
	//printf("iSignalPairNum = %d,gs_uiRealSMDUENum = %d,iCycleTimes=%d\n", iSignalPairNum, gs_uiRealSMDUENum,iCycleTimes);

	if(TRUE)
	{
				
		
		iBufLength = MAX_SETTING_LEN * iSignalPairNum * 8 + 1024;

		//head
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
		//body
		for(i = 0; i < iCycleTimes; i++)
		{
			//bWorkStatus
			//printf("bWorkStatus %d\n", i);
			//printf("iType1 = %d bWorkStatus = %d\n", gs_pWebSetInfo[iType1].stWebPrivateSet1[i].bWorkStatus);
			//For debug, to display EIB
			if(!gs_pWebSetInfo[iType1].stWebPrivateSet1[i].bWorkStatus)
			{
				continue;
			}

			if(i % iMaxSignalNumber == 0)		//ȡ���õĵ�һ���źŵ� ���� 
			{
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;	
				iFileLen += snprintf(szTemp + iFileLen,
							(iBufLength - iFileLen),
							"[%d,%d,%d,%d,[\" %s\",\" %s\"],",
							i / iMaxSignalNumber,
							pWebInfoItem->iEquipID,
							pWebInfoItem->iSignalType,
							pWebInfoItem->iSignalID,
							pEquipInfo->pEquipName->pFullName[0],
							pEquipInfo->pEquipName->pFullName[1]);


				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
									printf("bWorkStatus===============3\n");
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
					//printf("bWorkStatus===============4\n");
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{	
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}
				iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[");
				continue;
			}
			
			//printf("bWorkStatus   -2\n");
			//bdisplay
			//just display the SIG_TYPE_CTL signals from DO_BASIC_SEQ
			 if(SIG_TYPE_ALARM == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
				//printf("bWorkStatus SIG_TYPE_ALARM  -2\n");
					iIndex++;

					pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
					pAlarmSig = (ALARM_SIG_VALUE *)(pWebInfoItem->pSigValue);
					pEquipInfo = pWebInfoItem->pEquipInfo;				


					//[iEquipID, iSignalType, iSignalID,
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[%d,%d,%d,",
										pWebInfoItem->iEquipID,
										pWebInfoItem->iSignalType,
										pWebInfoItem->iSignalID);

					//iAlarmDelay, iAlarmLevel,
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"%d,%d,%d,%d,\" %s \",",
										pAlarmSig->pStdSig->iAlarmRelayNo,
										pAlarmSig->pStdSig->iAlarmLevel,
										1,//Not used
										VAR_ENUM, //Set it default to enum
										" ");//Unit is null
					
					if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
					{
						
				
						//["sSignalName_en", "sSignalName_local"],
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pAlarmSig->pStdSig->pSigName->pFullName[0],
										//pAlarmSig->pStdSig->pSigName->pFullName[1]);
						KOI8toUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"]],");
										//"[\"%s\",\"%s\"]],",
										//pAlarmSig->pStdSig->pSigName->pAbbrName[0],
										//xOut);
					}
					else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
					{
					    //["sSignalName_en", "sSignalName_local"],
					    iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[\"\",\"\"],");
						//"[\"%s\",\"%s\"],",
						//pAlarmSig->pStdSig->pSigName->pFullName[0],
						//pAlarmSig->pStdSig->pSigName->pFullName[1]);
					    GBKtoUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
					      iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[\"\",\"\"]],");
						//"[\"%s\",\"%s\"]],",
						//pAlarmSig->pStdSig->pSigName->pAbbrName[0],
						//xOut);
					}
					else
					{
					

						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pAlarmSig->pStdSig->pSigName->pFullName[0],
										//pAlarmSig->pStdSig->pSigName->pFullName[1]);
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"]],");
										//"[\"%s\",\"%s\"]],",
										//pAlarmSig->pStdSig->pSigName->pAbbrName[0],
										//pAlarmSig->pStdSig->pSigName->pAbbrName[1]);
					}
					//printf("pAlarmSig->pStdSig->iSigID = %d pAlarmSig->pStdSig->pSigName->pFullName[0] =%s\n", pAlarmSig->pStdSig->iSigID, pAlarmSig->pStdSig->pSigName->pFullName[0]);
					//printf("111111szTemp = %s\n", szTemp);
			
			}
			else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{

				iIndex++;
				//printf("bWorkStatus  SIG_TYPE_SAMPLING -3\n");
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

				
				//printf("bWorkStatus   -4\n");
				//[iEquipID, iSignalType, iSignalID,
				if(pSampleSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSampleSig->bv.varValue.fValue,
									pSampleSig->pStdSig->iSigValueType,
									pSampleSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSampleSig->bv.varValue.enumValue,
									pSampleSig->pStdSig->iSigValueType,
									pSampleSig->pStdSig->szSigUnit);
				}
				
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
			
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{
				
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}
			}
			
			else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
				iIndex++;
				//printf("bWorkStatus SIG_TYPE_CONTROL  -3\n");
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pCtlSig = (CTRL_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

				if(pCtlSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pCtlSig->bv.varValue.fValue,									
									pCtlSig->pStdSig->iSigValueType,
									pCtlSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pCtlSig->bv.varValue.enumValue,
									pCtlSig->pStdSig->iSigValueType,
									pCtlSig->pStdSig->szSigUnit);
				}

				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
									//"[\"%s\",\"%s\"],",
									//pCtlSig->pStdSig->pSigName->pFullName[0],
									//pCtlSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pCtlSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"]],");
									//"[\"%s\",\"%s\"],",
									//pCtlSig->pStdSig->pSigName->pAbbrName[0],
									//xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					//"[\"%s\",\"%s\"],",
					//pCtlSig->pStdSig->pSigName->pFullName[0],
					//pCtlSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pCtlSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					//"[\"%s\",\"%s\"],",
					//pCtlSig->pStdSig->pSigName->pAbbrName[0],
					//xOut);
				}
				else
				{
				
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pCtlSig->pStdSig->pSigName->pFullName[0],
										//pCtlSig->pStdSig->pSigName->pFullName[1]);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pCtlSig->pStdSig->pSigName->pAbbrName[0],
										//pCtlSig->pStdSig->pSigName->pAbbrName[1]);
				}
			}
			else if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{

				iIndex++;
				//printf("bWorkStatus  SIG_TYPE_SETTING -3\n");
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSetSig = (SET_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

								
				//printf("bWorkStatus  SIG_TYPE_SETTING  -4\n");
				//[iEquipID, iSignalType, iSignalID,


				if(pSetSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
					//printf("bWorkStatus  SIG_TYPE_SETTING  -444\n");
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSetSig->bv.varValue.fValue,									
									pSetSig->pStdSig->iSigValueType,
									pSetSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSetSig->bv.varValue.enumValue,
									pSetSig->pStdSig->iSigValueType,
									pSetSig->pStdSig->szSigUnit);
				}

				//printf("bWorkStatus  SIG_TYPE_SETTING  -5\n");
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
									//remove the content but the front_end code won't modify
									//"[\"%s\",\"%s\"],")
									//pEquipInfo->pEquipName->pFullName[0],
									//pEquipInfo->pEquipName->pFullName[1]);
				//printf("bWorkStatus  SIG_TYPE_SETTING  -6\n");
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
			
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
									//"[\"%s\",\"%s\"],",
									//pSetSig->pStdSig->pSigName->pFullName[0],
									//pSetSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSetSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
									//"[\"%s\",\"%s\"],",
									//pSetSig->pStdSig->pSigName->pAbbrName[0],
									//xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					//"[\"%s\",\"%s\"],",
					//pSetSig->pStdSig->pSigName->pFullName[0],
					//pSetSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSetSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					//"[\"%s\",\"%s\"],",
					//pSetSig->pStdSig->pSigName->pAbbrName[0],
					//xOut);
				}
				else
				{
				
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pSetSig->pStdSig->pSigName->pFullName[0],
										//pSetSig->pStdSig->pSigName->pFullName[1]);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pSetSig->pStdSig->pSigName->pAbbrName[0],
										//pSetSig->pStdSig->pSigName->pAbbrName[1]);
				}
				//printf("bWorkStatus  SIG_TYPE_SETTING  -7\n");
			}

			if(SIG_TYPE_ALARM == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
			}
			else if(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_ENUM)
			{
			    memset(szBuf, 0, sizeof(szBuf));
			    iBufLen = 0;
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[");
				if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					for(j = 0; j < ((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
					    }
				}
				else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					for(j = 0; j < ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
						iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						 ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
					    }
				}
				else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					for(j = 0; j < ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
						iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						    ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						    ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);

					}
				}
				
				if(iBufLen >= 1)
				{
					iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
				}
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "]");
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s],",szBuf);

			}
			else if(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_FLOAT
				|| gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_UNSIGNED_LONG
				|| gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_LONG)
			{
				memset(szBuf, 0, sizeof(szBuf));
				iBufLen = 0;
				iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "");
				
				if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				if(iBufLen >= 1)
				{
					iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
				}
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "");
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s],",szBuf);

			}
			//printf("End szTemp = %s\n", szTemp);
			if(i % iMaxSignalNumber == iMaxSignalNumber -1)
			{
				if(iFileLen >= 1)
				{
					iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
				}
				
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s]],","");
			}
		}
		
		//printf("bWorkStatus   -7\n");
		if(iFileLen > 1)
		{
			iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}		
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	}

		return szTemp;
}



//changed by Frank Wu,18/N/35,20140527, for adding the the web setting tab page 'DI'
static char* Web_MakeShuntSetDataFile(int iType1)
{
#define MAX_PER_SMDUP_SHUNT_FUNCTION_NUM	 10
#define MAX_PER_SMDU_UNIT_NUMBER		9
#define MAX_PER_SMDU_UNIT_NUMBER_SHUNT	12
#define MAX_PER_BATT_UNIT_NUMBER		8
#define MAX_PER_EIB_UNIT_NUMBER_SHUNT	12

	int i = 0, j = 0;
	char *szTemp = NULL;
	int iFileLen = 0;
	int iBufLength = 0;
	char xOut[128];
	char szBuf[4096];
	int iBufLen  = 0;

	int iIndex = 0;
	int iSignalPairNum = 0;
	WEB_GENERAL_SETTING_INFO *pWebInfoItem = NULL;
	CTRL_SIG_VALUE *pCtlSig = NULL;
	SET_SIG_VALUE *pSetSig = NULL;
	SAMPLE_SIG_VALUE *pSampleSig = NULL;
	ALARM_SIG_VALUE *pAlarmSig = NULL;
	EQUIP_INFO *pEquipInfo = NULL;
	int iCycleTimes = 0, iMaxSignalNumber = 0;;

	//printf("Begin to exec Web_MakeDOSetDataFile(iType1=%d)\n", iType1);

	//check the pointer valid begin
	if(NULL == gs_pWebSetInfo[iType1].stWebPrivateSet1)		
	{
		return NULL;
	}

	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iFileLen = 0;
	//init
	snprintf(szTemp, strlen("[]"), "[]");
	iSignalPairNum = gs_pWebSetInfo[iType1].iNumber;
	if( SHUNT_SMDU_BASIC_SEQ == iType1)
	{
		iCycleTimes = iSignalPairNum*gs_uiRealSMDUNum ;
		iMaxSignalNumber = MAX_PER_SMDU_UNIT_NUMBER_SHUNT;
	}
	else if(SHUNT_SMDUP_BASIC_SEQ == iType1)
	{
		iCycleTimes = iSignalPairNum*gs_uiRealSMDUPNum  ;
		iMaxSignalNumber = MAX_PER_SMDUP_SHUNT_FUNCTION_NUM+1;
	}
	else if(SHUNT_SMDUE_BASIC_SEQ == iType1)
	{
		iCycleTimes = iSignalPairNum*gs_uiRealSMDUENum;
		iMaxSignalNumber = 12;//���ֵ������web�����ļ��е�SMDUE_SHUNT_FUNCTION��ÿ��ͨ���ź�����һ��
	}
	else if(SHUNT_EIB_BASIC_SEQ == iType1)
	{
		iCycleTimes =iSignalPairNum*gs_uiRealEIBNum;
		iMaxSignalNumber = MAX_PER_EIB_UNIT_NUMBER_SHUNT;
	}
	else if(SHUNT_BATT_BASIC_SEQ == iType1)
	{
		iCycleTimes =iSignalPairNum*gs_uiRealComBattNum;
		iMaxSignalNumber = MAX_PER_BATT_UNIT_NUMBER;
	}

	else if(SHUNT_DCD_BASIC_SEQ == iType1)
	{
		iCycleTimes =iSignalPairNum ;
		iMaxSignalNumber = MAX_PER_SMDU_UNIT_NUMBER;
	}

	if(TRUE)
	{
				
		
		//printf("iSignalPairNum = %d iType1 = %d\n", iSignalPairNum, iType1);
		iBufLength = MAX_SETTING_LEN * iSignalPairNum * 8 + 1024;

		//head
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
		//body
		for(i = 0; i < iCycleTimes; i++)
		{
			//bWorkStatus
			//printf("bWorkStatus %d\n", i);
			//printf("iType1 = %d bWorkStatus = %d\n", gs_pWebSetInfo[iType1].stWebPrivateSet1[i].bWorkStatus);
			//For debug, to display EIB
			if(!gs_pWebSetInfo[iType1].stWebPrivateSet1[i].bWorkStatus && SHUNT_DCD_BASIC_SEQ != iType1)
			{
				continue;
			}
			
			if(i % iMaxSignalNumber == 0)		//ȡ���õĵ�һ���źŵ� ���� 
			{
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;	
				iFileLen += snprintf(szTemp + iFileLen,
							(iBufLength - iFileLen),
							"[%d,%d,%d,%d,[\" %s\",\" %s\"],",
							i / iMaxSignalNumber,
							pWebInfoItem->iEquipID,
							pWebInfoItem->iSignalType,
							pWebInfoItem->iSignalID,
							pEquipInfo->pEquipName->pFullName[0],
							pEquipInfo->pEquipName->pFullName[1]);



				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);

					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{

				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{	

					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}
				iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[");
				continue;
			}
			
			//printf("bWorkStatus   -2\n");
			//bdisplay
			//just display the SIG_TYPE_CTL signals from DO_BASIC_SEQ
			 if(SIG_TYPE_ALARM == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
				//printf("bWorkStatus SIG_TYPE_ALARM  -2\n");
					iIndex++;

					pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
					pAlarmSig = (ALARM_SIG_VALUE *)(pWebInfoItem->pSigValue);
					pEquipInfo = pWebInfoItem->pEquipInfo;				
					
					////split char between two items
					//if(iIndex != 1)
					//{
					//	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
					//}



					//[iEquipID, iSignalType, iSignalID,
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[%d,%d,%d,",
										pWebInfoItem->iEquipID,
										pWebInfoItem->iSignalType,
										pWebInfoItem->iSignalID);

					//iAlarmDelay, iAlarmLevel,
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"%d,%d,%d,%d,\" %s \",",
										pAlarmSig->pStdSig->iAlarmRelayNo,
										pAlarmSig->pStdSig->iAlarmLevel,
										1,//Not used
										VAR_ENUM, //Set it default to enum
										" ");//Unit is null
					
					if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
					{
						
				
						//["sSignalName_en", "sSignalName_local"],
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pAlarmSig->pStdSig->pSigName->pFullName[0],
										//pAlarmSig->pStdSig->pSigName->pFullName[1]);
						KOI8toUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"]],");
										//"[\"%s\",\"%s\"]],",
										//pAlarmSig->pStdSig->pSigName->pAbbrName[0],
										//xOut);
					}
					else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
					{
					    //["sSignalName_en", "sSignalName_local"],
					    iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[\"\",\"\"],");
						//"[\"%s\",\"%s\"],",
						//pAlarmSig->pStdSig->pSigName->pFullName[0],
						//pAlarmSig->pStdSig->pSigName->pFullName[1]);
					    GBKtoUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
					      iFileLen += snprintf(szTemp + iFileLen,
						(iBufLength - iFileLen),
						"[\"\",\"\"]],");
						//"[\"%s\",\"%s\"]],",
						//pAlarmSig->pStdSig->pSigName->pAbbrName[0],
						//xOut);
					}
					else
					{
					

						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pAlarmSig->pStdSig->pSigName->pFullName[0],
										//pAlarmSig->pStdSig->pSigName->pFullName[1]);
						iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"]],");
										//"[\"%s\",\"%s\"]],",
										//pAlarmSig->pStdSig->pSigName->pAbbrName[0],
										//pAlarmSig->pStdSig->pSigName->pAbbrName[1]);
					}
					//printf("pAlarmSig->pStdSig->iSigID = %d pAlarmSig->pStdSig->pSigName->pFullName[0] =%s\n", pAlarmSig->pStdSig->iSigID, pAlarmSig->pStdSig->pSigName->pFullName[0]);
					//printf("111111szTemp = %s\n", szTemp);
			
			}
			else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{

				iIndex++;
				//printf("bWorkStatus  SIG_TYPE_SAMPLING -3\n");
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSampleSig = (SAMPLE_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

				////split char between two items
				//if(iIndex != 1)
				//{
				//	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
				//}
				
				//printf("bWorkStatus   -4\n");
				//[iEquipID, iSignalType, iSignalID,


				if(pSampleSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSampleSig->bv.varValue.fValue,
									pSampleSig->pStdSig->iSigValueType,
									pSampleSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSampleSig->bv.varValue.enumValue,
									pSampleSig->pStdSig->iSigValueType,
									pSampleSig->pStdSig->szSigUnit);
				}
				
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
			
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pFullName[0],
									pSampleSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"%s\",\"%s\"],",
									pSampleSig->pStdSig->pSigName->pAbbrName[0],
									xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pFullName[0],
					pSampleSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSampleSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"%s\",\"%s\"],",
					pSampleSig->pStdSig->pSigName->pAbbrName[0],
					xOut);
				}
				else
				{
				
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pFullName[0],
										pSampleSig->pStdSig->pSigName->pFullName[1]);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pSampleSig->pStdSig->pSigName->pAbbrName[0],
										pSampleSig->pStdSig->pSigName->pAbbrName[1]);
				}
			}
			
			else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
				iIndex++;
				//printf("bWorkStatus SIG_TYPE_CONTROL  -3\n");
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pCtlSig = (CTRL_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

				if(pCtlSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pCtlSig->bv.varValue.fValue,									
									pCtlSig->pStdSig->iSigValueType,
									pCtlSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pCtlSig->bv.varValue.enumValue,
									pCtlSig->pStdSig->iSigValueType,
									pCtlSig->pStdSig->szSigUnit);
				}

				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
			
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
									//"[\"%s\",\"%s\"],",
									//pCtlSig->pStdSig->pSigName->pFullName[0],
									//pCtlSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pCtlSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"]],");
									//"[\"%s\",\"%s\"],",
									//pCtlSig->pStdSig->pSigName->pAbbrName[0],
									//xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					//"[\"%s\",\"%s\"],",
					//pCtlSig->pStdSig->pSigName->pFullName[0],
					//pCtlSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pCtlSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					//"[\"%s\",\"%s\"],",
					//pCtlSig->pStdSig->pSigName->pAbbrName[0],
					//xOut);
				}
				else
				{
				
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pCtlSig->pStdSig->pSigName->pFullName[0],
										//pCtlSig->pStdSig->pSigName->pFullName[1]);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pCtlSig->pStdSig->pSigName->pAbbrName[0],
										//pCtlSig->pStdSig->pSigName->pAbbrName[1]);
				}
			}
			else if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{

				iIndex++;
				//printf("bWorkStatus  SIG_TYPE_SETTING -3\n");
				pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
				pSetSig = (SET_SIG_VALUE *)(pWebInfoItem->pSigValue);
				pEquipInfo = pWebInfoItem->pEquipInfo;					

								
				//printf("bWorkStatus  SIG_TYPE_SETTING  -4\n");
				//[iEquipID, iSignalType, iSignalID,


				if(pSetSig->pStdSig->iSigValueType == VAR_FLOAT)
				{
					//printf("bWorkStatus  SIG_TYPE_SETTING  -444\n");
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%.2f,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSetSig->bv.varValue.fValue,									
									pSetSig->pStdSig->iSigValueType,
									pSetSig->pStdSig->szSigUnit);
				}
				else 
				{
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[%d,%d,%d,%d,%d,\" %s\",",
									pWebInfoItem->iEquipID,
									pWebInfoItem->iSignalType,
									pWebInfoItem->iSignalID,
									pSetSig->bv.varValue.enumValue,
									pSetSig->pStdSig->iSigValueType,
									pSetSig->pStdSig->szSigUnit);
				}

				//printf("bWorkStatus  SIG_TYPE_SETTING  -5\n");
				iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
									//remove the content but the front_end code won't modify
									//"[\"%s\",\"%s\"],")
									//pEquipInfo->pEquipName->pFullName[0],
									//pEquipInfo->pEquipName->pFullName[1]);
				//printf("bWorkStatus  SIG_TYPE_SETTING  -6\n");
				if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
				{
					
			
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
									//"[\"%s\",\"%s\"],",
									//pSetSig->pStdSig->pSigName->pFullName[0],
									//pSetSig->pStdSig->pSigName->pFullName[1]);
					KOI8toUTF8(pSetSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
									(iBufLength - iFileLen),
									"[\"\",\"\"],");
									//"[\"%s\",\"%s\"],",
									//pSetSig->pStdSig->pSigName->pAbbrName[0],
									//xOut);
				}
				else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
				{
				    //["sSignalName_en", "sSignalName_local"],
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					//"[\"%s\",\"%s\"],",
					//pSetSig->pStdSig->pSigName->pFullName[0],
					//pSetSig->pStdSig->pSigName->pFullName[1]);
				    GBKtoUTF8(pSetSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
				    iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[\"\",\"\"],");
					//"[\"%s\",\"%s\"],",
					//pSetSig->pStdSig->pSigName->pAbbrName[0],
					//xOut);
				}
				else
				{
				
					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pSetSig->pStdSig->pSigName->pFullName[0],
										//pSetSig->pStdSig->pSigName->pFullName[1]);
					//["sSignalAbbrName_en", "sSignalAbbrName_local"]],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"\",\"\"],");
										//"[\"%s\",\"%s\"],",
										//pSetSig->pStdSig->pSigName->pAbbrName[0],
										//pSetSig->pStdSig->pSigName->pAbbrName[1]);
				}
				//printf("bWorkStatus  SIG_TYPE_SETTING  -7\n");
			}
//if(SHUNT_SMDU_BASIC_SEQ == iType1)
//{
//printf(" type  %d equipid %d    ",gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType,pWebInfoItem->iEquipID);
//}
			if(SIG_TYPE_ALARM == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
			}
			else if(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_ENUM)
			{
			    memset(szBuf, 0, sizeof(szBuf));
			    iBufLen = 0;
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[");
				if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					for(j = 0; j < ((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
					    }
				}
				else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					for(j = 0; j < ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
						iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						 ((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);
					    }
				}
				else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					for(j = 0; j < ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->iStateNum; j++)

					    {
						
						iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[\"%s\",\"%s\"],", 
						    ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[0],
						    ((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->pStateText[j]->pFullName[1]);

					}
				}
				
				if(iBufLen >= 1)
				{
					iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
				}
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "]");
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s],",szBuf);

			}
			else if(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_FLOAT
				|| gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_UNSIGNED_LONG
				|| gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue->ucType == VAR_LONG)
			{
				memset(szBuf, 0, sizeof(szBuf));
				iBufLen = 0;
				iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "");
				
				if(SIG_TYPE_SETTING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((SET_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				else if(SIG_TYPE_CONTROL == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((CTRL_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				else if(SIG_TYPE_SAMPLING == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
				{
					iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "[%.1f,%.1f],", 
					((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMinValidValue,
					((SAMPLE_SIG_VALUE *)(gs_pWebSetInfo[iType1].stWebPrivateSet1[i].pSigValue))->pStdSig->fMaxValidValue);
		
				}
				if(iBufLen >= 1)
				{
					iBufLen = iBufLen - 1;	// get rid of the last "," to meet json format
				}
			    iBufLen += snprintf(szBuf + iBufLen, (4096 - iBufLen), "");
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s],",szBuf);

			}
			//printf("End szTemp = %s\n", szTemp);
			if(i % iMaxSignalNumber == iMaxSignalNumber -1)
			{
				if(iFileLen >= 1)
				{
					iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
				}
				
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "%s]],","");
			}
		}
		
		//printf("bWorkStatus   -7\n");
		if(iFileLen > 1)
		{
			iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}		
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	}
	if(SHUNT_EIB_BASIC_SEQ == iType1)
	{
		//printf("end to exec Web_MakeDISetDataFile(iType1=%d)szTemp = %s\n", iType1, szTemp);
	}
		return szTemp;
}


static SIG_ENUM Web_SMDUE_Fuse_is_Display(int iEquipID,int iSignalID)
{
	int iNeedEquipID,iNeedSigID,iError,iBufLen;
	int iTimeOut = 0;
	SIG_BASIC_VALUE* pSigValue;

	iNeedEquipID = iEquipID -32;//see solution.cfg

    iNeedSigID = iSignalID + 130;//see smdue.cfg


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iNeedEquipID,				
						DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, iNeedSigID),
						&iBufLen,
						(void *)&pSigValue,
						iTimeOut);
	
	return pSigValue->varValue.enumValue ;
}

//changed by Stone Song ,20160525, for adding the the web setting tab page 'FUSE'
static char* Web_MakeFusetSetDataFile(int iType1)
{
//#define MAX_PER_SMDU_UNIT_NUMBER		9
//#define MAX_PER_BATT_UNIT_NUMBER		8


	int i = 0, j = 0;
	char *szTemp = NULL;
	int iFileLen = 0;
	int iBufLength = 0;
	char xOut[128];
	char szBuf[4096];
	int iBufLen  = 0;

	int iIndex = 0;
	int iSignalPairNum = 0;
	int iFuseRealNum=0;
	WEB_GENERAL_SETTING_INFO *pWebInfoItem = NULL;
	CTRL_SIG_VALUE *pCtlSig = NULL;
	SET_SIG_VALUE *pSetSig = NULL;
	SAMPLE_SIG_VALUE *pSampleSig = NULL;
	ALARM_SIG_VALUE *pAlarmSig = NULL;
	EQUIP_INFO *pEquipInfo = NULL;
	int iCycleTimes = 0, iMaxSignalNumber = 0;;

	SIG_ENUM		enumValueForDisplay;

//	printf("===================Begin to exec Web_MakeFUSESetDataFile(iType1=%d)\n", iType1);

	//check the pointer valid begin
	if(NULL == gs_pWebSetInfo[iType1].stWebPrivateSet1)		
	{
		//printf("gs_pWebSetInfo[%d].stWebPrivateSet1 return null\n ",iType1);
		return NULL;
	}

	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iFileLen = 0;
	//init
	snprintf(szTemp, strlen("[]"), "[]");
	iSignalPairNum = gs_pWebSetInfo[iType1].iNumber;
	if( OBBATT_FUSE_SEQ == iType1)
	{
		iFuseRealNum = gs_uiRealBattFuseNum;
	}
	else if(OBDC_FUSE_SEQ == iType1)
	{
		iFuseRealNum = iOBDC_FuseRealNum;
	}
	else if(SMDUBATT_FUSE_SEQ == iType1)
	{
		iFuseRealNum =iSMDUBATT_FuseRealNum;
	}
	else if( SMDUDC_FUSE_SEQ==iType1)
	{
		iFuseRealNum =iSMDUDC_FuseRealNum;
	}
	else if(SMDUPLUS_FUSE_SEQ == iType1)
	{
		iFuseRealNum =iSMDUPLUS_FuseRealNum;
	}
	/*else if(SMDUEBATT_FUSE_SEQ == iType1)
	{
		iFuseRealNum =iSMDUEBATT_FuseRealNum;
	}*/
	else if(SMDUEDC_FUSE_SEQ == iType1)
	{
		iFuseRealNum =iSMDUEDC_FuseRealNum;
	}
	
	iCycleTimes = iSignalPairNum*iFuseRealNum;
	
#ifdef _CODE_FOR_MINI
	if( OBBATT_FUSE_SEQ == iType1 )
	{
		iSignalPairNum = 4;//�����˿ֻ��4·��for����
		iCycleTimes = iSignalPairNum*iFuseRealNum;
	}
	else if ( OBDC_FUSE_SEQ == iType1 )
	{
		iSignalPairNum= 1;//������˿ֻ��1·��for����
		iCycleTimes = iSignalPairNum*iFuseRealNum;
	}
	else
	{
	}
#endif

//printf("----iCycleTimes%d iSignalPairNum%d\n",iCycleTimes,iSignalPairNum);

	if(TRUE)
	{
		
//		printf("iSignalPairNum = %d iType1 = %d\n", iSignalPairNum, iType1);
		iBufLength = MAX_SETTING_LEN * iSignalPairNum * iFuseRealNum + 1024;

		//head
//		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
		//body
		for(i = 0; i < iCycleTimes; i++)
		{
			//bWorkStatus
			//printf("bWorkStatus %d\n", i);
			//printf("iType1 = %d bWorkStatus = %d\n", gs_pWebSetInfo[iType1].stWebPrivateSet1[i].bWorkStatus);
			//For debug, to display EIB
			if(!gs_pWebSetInfo[iType1].stWebPrivateSet1[i].bWorkStatus )
			{
				continue;
			}

			 if(SIG_TYPE_ALARM == gs_pWebSetInfo[iType1].stWebPrivateSet1[i].iSignalType)
			{
					iIndex++;

					pWebInfoItem = &gs_pWebSetInfo[iType1].stWebPrivateSet1[i];
					pAlarmSig = (ALARM_SIG_VALUE *)(pWebInfoItem->pSigValue);
					pEquipInfo = pWebInfoItem->pEquipInfo;				

					////split char between two items
					//if(iIndex != 1)
					//{
					//	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
					//}



					//[iEquipID, iSignalType, iSignalID,
					
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[%d,%d,%d,",
										pWebInfoItem->iEquipID,
										pWebInfoItem->iSignalType,
										pWebInfoItem->iSignalID);
			
					//["sEquipName_en", "sEquipName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pEquipInfo->pEquipName->pFullName[0],
										pEquipInfo->pEquipName->pFullName[1]);

					//["sSignalName_en", "sSignalName_local"],
					iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],",
										pAlarmSig->pStdSig->pSigName->pFullName[0],
										pAlarmSig->pStdSig->pSigName->pFullName[1]);

					if(g_SiteInfo.LangInfo.iCodeType == ENUM_KOI8R)
					{

							if(SMDUEDC_FUSE_SEQ == iType1)
							{
								enumValueForDisplay = Web_SMDUE_Fuse_is_Display(pWebInfoItem->iEquipID,pWebInfoItem->iSignalID);

								KOI8toUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
								iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],%d],",
										pAlarmSig->pStdSig->pSigName->pAbbrName[0],
										xOut,
										enumValueForDisplay);
							}
							else
							{
								KOI8toUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,64);
								iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"]],",
										pAlarmSig->pStdSig->pSigName->pAbbrName[0],
										xOut);
							}
					}
					else if(g_SiteInfo.LangInfo.iCodeType == ENUM_GB2312)
					{
							if(SMDUEDC_FUSE_SEQ == iType1)
							{
								enumValueForDisplay = Web_SMDUE_Fuse_is_Display(pWebInfoItem->iEquipID,pWebInfoItem->iSignalID);

					    		GBKtoUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
					      		iFileLen += snprintf(szTemp + iFileLen,
											(iBufLength - iFileLen),
											"[\"%s\",\"%s\"],%d],",
											pAlarmSig->pStdSig->pSigName->pAbbrName[0],
											xOut,
											enumValueForDisplay);
							}
							else
							{
					    		GBKtoUTF8(pAlarmSig->pStdSig->pSigName->pAbbrName[1],64,xOut,128);
					      		iFileLen += snprintf(szTemp + iFileLen,
											(iBufLength - iFileLen),
											"[\"%s\",\"%s\"]],",
											pAlarmSig->pStdSig->pSigName->pAbbrName[0],
											xOut);
							}
					}
					else
					{
							if(SMDUEDC_FUSE_SEQ == iType1)
							{
								enumValueForDisplay = Web_SMDUE_Fuse_is_Display(pWebInfoItem->iEquipID,pWebInfoItem->iSignalID);
								iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"],%d],",
										pAlarmSig->pStdSig->pSigName->pAbbrName[0],
										pAlarmSig->pStdSig->pSigName->pAbbrName[1],
										enumValueForDisplay);
							}
							else
							{
								iFileLen += snprintf(szTemp + iFileLen,
										(iBufLength - iFileLen),
										"[\"%s\",\"%s\"]],",
										pAlarmSig->pStdSig->pSigName->pAbbrName[0],
										pAlarmSig->pStdSig->pSigName->pAbbrName[1]);
							}
					}
				//	printf("pAlarmSig->pStdSig->iSigID = %d pAlarmSig->pStdSig->pSigName->pFullName[0] =%s\n", pAlarmSig->pStdSig->iSigID, pAlarmSig->pStdSig->pSigName->pFullName[0]);
					//printf("111111szTemp = %s\n", szTemp);
			
			}
			

	//		
		}
//		printf("End szTemp = %s\n", szTemp);
//		printf("bWorkStatus   -7\n");
//		if(iFileLen > 1)
//		{
//			iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
//		}		
//		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	}

//printf("End szTemp = %s\n", szTemp);

	return szTemp;
}


/*==========================================================================*
* FUNCTION :  Web_MakeSingleOtherEquipDataFile
* PURPOSE  :  To make the other equip data string
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  itype: indicate which kind of equip such as SMAC, AC meter etc
* RETURN   :  char * : the pointer pointing to the string including the equip data.
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
char* Web_MakeSingleOtherEquipDataFile(int itype)
{
    int i, j, k, iAllSignalNum = 0, iEquipNum = 0, iOneEquipSignalNum = 0;
    char *szTemp = NULL;
    int iFileLen = 0;
    char szBuf[128];
    BOOL bdisplay;
    BOOL bdisplay1;
    int		iBufLength;
    BOOL bSignalValid = 0;

    TRACE_WEB_USER("Begin to exec Web_MakeSingleOtherEquipDataFile[%d]\n", itype);

    //check the pointer valid begin
    if(gs_pWebOtherSigInfo[itype] == NULL)
    {
	return NULL;
    }
    if((gs_pWebOtherSigInfo[itype]->stWebEquipInfo == NULL) || (gs_pWebOtherSigInfo[itype]->stWebPrivate == NULL))
    {
	return NULL;
    }
    //check the pointer valid end

    if(itype == SMAC_SEQ)
    {
	iEquipNum = gs_uiSMACNum;
    }
    else if(itype == DG_SEQ)
    {
	iEquipNum = gs_uiDGNum;
    }
    else if(itype == ACMETER_SEQ)
    {
	iEquipNum = gs_uiACMeterNum;
    }
    else if(itype == DCMETER_SEQ)
    {
	iEquipNum = gs_uiDCMeterNum;
    }
    else if(itype == NARADA_BMS_SEQ)
    {
	iEquipNum = gs_uiNaradaBMSNum;
    }
    else if(itype == DC_BOARD_SEQ)
    {
	iEquipNum = 1;
    }
    else if(itype == SMDU_SEQ)
    {
	iEquipNum = gs_uiRealSMDUNum;	
    }
    else if(itype == BATTERY_FUSE_GROUP_SEQ)
    {
	iEquipNum = gs_uiRealBattFuseGroupNum;
    }
    else if(itype == BATTERY_FUSE_SEQ)
    {
	iEquipNum = gs_uiRealBattFuseNum;
    }
    else if(itype == LVD_GROUP_SEQ1)
    {
	iEquipNum = gs_uiRealLVDGroupNum;
    }
    else if(itype == LVD_SEQ1)
    {
	iEquipNum = gs_uiRealLVDUnitNum;
    }
    else if(itype == LVD_SEQ2)
    {
	iEquipNum = gs_uiRealLVD3UnitNum;
    }
    else if(itype == SMDU_BATT_FUSE_SEQ)
    {
	iEquipNum = gs_uiRealSMDUBattFuseNum;
    }
    else if(itype == SMDU_LVD_SEQ)
    {
	iEquipNum = gs_uiRealSMDULVDUnitNum;
    }
    else if(itype == SMDUP_SEQ)
    {
	iEquipNum = gs_uiRealSMDUPNum;
    }
    else if(itype == SMDUH_SEQ)
    {
	iEquipNum = gs_uiRealSMDUHNum;
    }
    else if(itype == EIB_SEQ)
    {
	iEquipNum = gs_uiRealEIBNum;
    }
     else if(itype == ACUNIT_SEQ)
    {
	iEquipNum = gs_uiACUnitNum;
    }
    else
    {}

    if((gs_pWebOtherSigInfo[itype] != NULL) && (gs_pWebOtherSigInfo[itype]->stWebEquipInfo != NULL))
    {
	iOneEquipSignalNum = gs_pWebOtherSigInfo[itype]->stWebEquipInfo[0].iSamplenum;
    }

    iAllSignalNum = iOneEquipSignalNum * iEquipNum;
    TRACE_WEB_USER("iEquipNum is %d iOneEquipSignalNum is %d\n", iEquipNum, iOneEquipSignalNum);
    /*if(itype == BATTERY_FUSE_GROUP_SEQ)
    {
	TRACE_WEB_USER_NOT_CYCLE("iAllSignalNum is %d\n", iAllSignalNum);
    }*/
    if((iOneEquipSignalNum > 0) && (iEquipNum > 0))
    {
	//szTemp = NEW(char, (80 * iOneEquipSignalNum + 90 * iAllSignalNum + 100));
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	/*if(szTemp == NULL)
	{
	    AppLogOut("Web_MakeSingleDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    return NULL;
	}*/
	iBufLength = 80 * iOneEquipSignalNum + 90 * iAllSignalNum + 100;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[");
	for(i = 0; i < iOneEquipSignalNum; i++)
	{
	    TRACE_WEB_USER("Get signal name, exec %d signal\n", i);
	    if(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].bSingleValid == FALSE)
	    {
		TRACE_WEB_USER("Run here 7\n");
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
	    }
	    else
	    {
		if(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue->ucSigType == SIG_TYPE_SAMPLING)
		{
		    TRACE_WEB_USER("Run here 8\n");
		    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->bv);
		    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->pStdSig), DISPLAY_WEB);
		    TRACE_WEB_USER("Run here 6\n");
		    TRACE_WEB_USER("bdisplay is %d, bdisplay1 is %d\n", bdisplay, bdisplay1);
		    if((bdisplay == TRUE) && (bdisplay1 == TRUE))
		    {
			TRACE_WEB_USER("Run here 1\n");
			bSignalValid = 1;
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
			    ((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->pStdSig->pSigName->pFullName[0],
			    ((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->pStdSig->pSigName->pFullName[1]);
			TRACE_WEB_USER("Run here 2\n");
			if(itype == SMDU_SEQ)
			{
			    TRACE_WEB_USER("%s\n", ((SAMPLE_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->pStdSig->pSigName->pFullName[1]);
			}
		    }
		}
		else if(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue->ucSigType == SIG_TYPE_SETTING)
		{
		    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->bv);
		    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->pStdSig), DISPLAY_WEB);
		    if((bdisplay == TRUE) && (bdisplay1 == TRUE))
		    {
			bSignalValid = 1;
			iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
			    ((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->pStdSig->pSigName->pFullName[0],
			    ((SET_SIG_VALUE *)(gs_pWebOtherSigInfo[itype]->stWebPrivate[i].pSigValue))->pStdSig->pSigName->pFullName[1]);
		    }
		}
		else
		{}
	    }
	}
	if(iFileLen >= 1)
	{
	    if(bSignalValid == 1)
	    {
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	    }
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	/*if(itype == BATTERY_FUSE_GROUP_SEQ)
	{
	    TRACE_WEB_USER_NOT_CYCLE("szTemp is %s\n", szTemp);
	}*/
	k = 0;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	for(i = 0; i < iEquipNum; i++)
	{
	    TRACE_WEB_USER("Exe the %d equip\n", i);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	    if(gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].bSingleValid == FALSE)
	    {
		TRACE_WEB_USER("Run here 3\n");
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
	    }
	    else
	    {
		TRACE_WEB_USER("Run here 4\n");
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],", gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[0],
		    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[1]);
	    }
	    TRACE_WEB_USER("Run here 5\n");
	    for(j = 0; j < gs_pWebOtherSigInfo[itype]->stWebEquipInfo[i].iSamplenum; j++)
	    {
		TRACE_WEB_USER("Get signal value, exec [%d][%d] signal\n", i, j);
		memset(szBuf, 0, sizeof(szBuf));
		Makesignalstring(gs_pWebOtherSigInfo[itype]->stWebPrivate[k], szBuf, sizeof(szBuf));
		//TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
		//TRACE_WEB_USER("[Web_MakeSingleBattDataFile] szBuf is %s\n", szBuf);
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
		k++;
	    }
	    if(iFileLen >= 1)
	    {
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	    }
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]]");	
	TRACE_WEB_USER("end to exec Web_MakeSingleOtherEquipDataFile[%d]\n", itype);
	return szTemp;
    }
    else
    {
	TRACE_WEB_USER("end to exec Web_MakeSingleOtherEquipDataFile[%d]\n", itype);
	return NULL;
    }
}

//return value:true---With signal name include "";false---no signal name
BOOL Web_GetSignalName(WEB_GENERAL_SIGNAL_INFO	*pstWebPrivate, char *szBuf, int iBufLength)
{
    int iFileLen = 0;
    BOOL bdisplay;
    BOOL bdisplay1;

    if(pstWebPrivate->bValid == FALSE)
    {
	iFileLen += snprintf(szBuf + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
	return TRUE;
    }
    else
    {
	if(pstWebPrivate->iSignalType == SIG_TYPE_SAMPLING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig), DISPLAY_WEB);
	    if((bdisplay == TRUE) && (bdisplay1 == TRUE))
	    {
		iFileLen += snprintf(szBuf + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
		    ((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[0],
		    ((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[1]);
		return TRUE;
	    }
	    else
	    {
		return FALSE;
	    }
	}
	else if(pstWebPrivate->iSignalType == SIG_TYPE_SETTING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(pstWebPrivate->pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig), DISPLAY_WEB);
	    if((bdisplay == TRUE) && (bdisplay1 == TRUE))
	    {
		iFileLen += snprintf(szBuf + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],",
		    ((SET_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[0],
		    ((SET_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[1]);
		return TRUE;
	    }
	    else
	    {
		return FALSE;
	    }
	}
	else
	{
	    return FALSE;
	}
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeSingleDCDataFile
* PURPOSE  :  To make the DC data string
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  itype: indicate which kind of DC such as SMDU, SMDUH etc
* RETURN   :  char * : the pointer pointing to the string including the DC data.
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng               DATE: 2013-12-12 Add the EIB function
*==========================================================================*/
char* Web_MakeSingleDCDataFile(int itype)
{
    int i, j, k, iSingleDCSignalNum, iSingleDCNum, iSingleSignalNum;
    char *szTemp = NULL;
    int iFileLen = 0;
    char szBuf[128];
    //BOOL bdisplay;
    //BOOL bdisplay:q1;
    int itype1;
    int		iBufLength;
    int iSignalNameExist = 0;

    TRACE_WEB_USER("Begin to exec Web_MakeSingleDCDataFile[%d]\n", itype);

    //check the pointer valid begin
    if(gs_pWebDCSigInfo[itype] == NULL)
    {
	return NULL;
    }
    if((gs_pWebDCSigInfo[itype]->stWebEquipInfo == NULL) || (gs_pWebDCSigInfo[itype]->stWebPrivate == NULL))
    {
	return NULL;
    }
    //check the pointer valid end

    /*if(itype == SMDU_SEQ)
    {
	itype1 = DC_SMDU_SEQ;
	iSingleDCNum = gs_uiRealSMDUNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_SMDU_SEQ].iNumber;
    }
    else if(itype == SMDUP_SEQ)
    {
	itype1 = DC_SMDUP_SEQ;
	iSingleDCNum = gs_uiRealSMDUPNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_SMDUP_SEQ].iNumber;
    }
    else */if(itype == SMDUP1_SEQ)
    {
	itype1 = DC_SMDUP1_SEQ;
	iSingleDCNum = gs_uiRealSMDUPNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_SMDUP1_SEQ].iNumber;
    }
    /*else if(itype == SMDUH_SEQ)
    {
	itype1 = DC_SMDUH_SEQ;
	iSingleDCNum = gs_uiRealSMDUHNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_SMDUH_SEQ].iNumber;
    }
    else if(itype == EIB_SEQ)
    {
	itype1 = DC_EIB_SEQ;
	iSingleDCNum = gs_uiRealEIBNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_EIB_SEQ].iNumber;
    }*/
    else
    {}

    TRACE_WEB_USER("iSingleDCNum is %d\n", iSingleDCNum);
    if((iSingleSignalNum > 0) && (iSingleDCNum > 0))
    {
	iSingleDCSignalNum = iSingleSignalNum * iSingleDCNum;
	/*szTemp = NEW(char, (80 * iSingleSignalNum + 90 * iSingleDCSignalNum + 100));
	if(szTemp == NULL)
	{
	    AppLogOut("Web_MakeSingleDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    return NULL;
	}*/
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iBufLength = 80 * iSingleSignalNum + 90 * iSingleDCSignalNum + 100;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[[");
	for(i = 0; i < iSingleSignalNum; i++)
	{
	    TRACE_WEB_USER("Get signal name, exec %d signal\n", i);
	    memset(szBuf, 0, sizeof(szBuf));
	    iSignalNameExist += Web_GetSignalName(&(gs_pWebSigInfo[itype1].stWebPrivate[i]), szBuf, sizeof(szBuf));
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	}
	if((iFileLen >= 1) && (iSignalNameExist > 0))
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	k = 0;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	for(i = 0; i < iSingleDCNum; i++)
	{
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	    if(gs_pWebDCSigInfo[itype]->stWebEquipInfo[i].bSingleValid == FALSE)
	    {
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
	    }
	    else
	    {
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],", gs_pWebDCSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[0],
		    gs_pWebDCSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[1]);
	    }
	    for(j = 0; j < iSingleSignalNum; j++)
	    {
		//TRACE_WEB_USER("Get signal value, exec [%d][%d] signal\n", i, j);
		memset(szBuf, 0, sizeof(szBuf));
		Makesignalstring(gs_pWebDCSigInfo[itype]->stWebPrivate[k], szBuf, sizeof(szBuf));
		//TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
		//TRACE_WEB_USER("[Web_MakeSingleBattDataFile] szBuf is %s\n", szBuf);
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
		k++;
	    }
	    if(iFileLen >= 1)
	    {
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	    }
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]]");
	TRACE_WEB_USER("end to exec Web_MakeSingleDCDataFile[%d]\n", itype);
	return szTemp;
    }
    else
    {
	TRACE_WEB_USER("end to exec Web_MakeSingleDCDataFile[%d]\n", itype);
	return NULL;
    }
}

/*==========================================================================*
* FUNCTION :  Web_GetSignalNameForBatt
* PURPOSE  :  To make the battery signals string
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:
* RETURN   :
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-03-13
*==========================================================================*/
void Web_GetSignalNameForBatt(WEB_GENERAL_RECT_SIGNAL_INFO *pstWebPrivate, int itype, char *szBuf, int iBufLength)
{
    int iFileLen = 0;
    int iDisplay = 0;
    int iSignalID;
    BOOL bdisplay;
    BOOL bdisplay1;

	//add for VERIZON feature.
	static	SIG_BASIC_VALUE*		s_pSigValueVerizon = NULL;
	int iVarSubID, iError, iBufLen, iTimeOut = 0;
	if(s_pSigValueVerizon == NULL)
	{
		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 700);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			ID_SYSTEM,
			iVarSubID,
			&iBufLen,
			(void *)&s_pSigValueVerizon,
			iTimeOut);
	}
		
    if(pstWebPrivate->bSingleValid == FALSE)
    {

	TRACE_WEB_USER("The signal is not valid\n");
	iFileLen += snprintf(szBuf + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\",%d],", iDisplay);
    }
    else
    {

	iSignalID = ((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->iSigID;
	/*if(pstWebPrivate->iSignalType == SIG_TYPE_SAMPLING)
	{*/
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig), DISPLAY_WEB);

	    if((bdisplay == TRUE) && (bdisplay1 == TRUE))
	    {
			if(iSignalID == 4 && s_pSigValueVerizon->varValue.enumValue == 1)
			{
				iDisplay = 0;	
			}
			else
			{
				if(itype == COMM_BATT_SINGLE_SEQ)
				{
					if((iSignalID == 4) || (iSignalID == 2) || (iSignalID == 1))
					{
					iDisplay = 1;
					}
					else
					{
					iDisplay = 2;
					}
				}
				else if(itype == EIB_BATT_SEQ)
				{
					if((iSignalID == 4) || (iSignalID == 2))
					{
					iDisplay = 1;
					}
					else
					{
					iDisplay = 2;
					}
				}
				else if(itype == SMDU_BATT_SEQ)
				{
					if((iSignalID == 4) || (iSignalID == 2) || (iSignalID == 51))
					{
					iDisplay = 1;
					}
					else
					{
					iDisplay = 2;
					}
				}
				else if(itype == SMDUE_BATT_SEQ)
				{
					if((iSignalID == 4) || (iSignalID == 2) )
					{
					iDisplay = 1;
					}
					else
					{
					iDisplay = 2;
					}
				}
				else if(itype == SM_BATT_SEQ)
				{
					if((iSignalID == 4) || (iSignalID == 2) || (iSignalID == 1))
					{
					iDisplay = 1;
					}
					else if(iSignalID == 3)
					{
					iDisplay = 2;
					}
					else
					{
					iDisplay = 3;
					}
				}
				else if(itype == SMBRC_BATT_SEQ)
				{
					if((iSignalID == 4) || (iSignalID == 2) || (iSignalID == 1))
					{
					iDisplay = 1;
					}
					else if(iSignalID == 3)
					{
					iDisplay = 2;
					}
					else
					{
					iDisplay = 3;
					}
				}
				else if(itype == SONICK_BATT_SEQ)
				{
					if((iSignalID == 4) || (iSignalID == 2) || (iSignalID == 1))
					{
					iDisplay = 1;
					}
					else if(iSignalID == 3)
					{
					iDisplay = 2;
					}
					else
					{
					iDisplay = 3;
					}
				}
				else
				{
					iDisplay = 0;
				}
			}
			iFileLen += snprintf(szBuf + iFileLen, (iBufLength - iFileLen), "\"_%d\":[\"%s\",\"%s\",%d],", iSignalID,
				((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[0],
				((SAMPLE_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[1],
				iDisplay);
	    }
	//}
	/*else if(pstWebPrivate->iSignalType == SIG_TYPE_SETTING)
	{
	    bdisplay = SIG_VALUE_NEED_DISPLAY(&((SET_SIG_VALUE *)(pstWebPrivate->pSigValue))->bv);
	    bdisplay1 = STD_SIG_IS_DISPLAY_ON_UI((((SET_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig), DISPLAY_WEB);
	    if((bdisplay == TRUE) && (bdisplay1 == TRUE))
	    {
		iDisplay = 2;
		iFileLen += snprintf(szBuf + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\",%d],",
		    ((SET_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[0],
		    ((SET_SIG_VALUE *)(pstWebPrivate->pSigValue))->pStdSig->pSigName->pFullName[1],
		    iDisplay);
	    }
	}*/
	else
	{}
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeSingleBattDataFile
* PURPOSE  :  To make the battery data string
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  itype: indicate which kind of battery such as main board battery, EIB battery, SM battery etc
* RETURN   :  char * : the pointer pointing to the string including the battery data.
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng               DATE: 2015-07-20
*==========================================================================*/
char* Web_MakeSingleBattDataFile(int itype)
{
    int i, j, k, iSingleBattSignalNum, iSingleBattNum, iSingleSignalNum;
    char *szTemp = NULL;
    int iFileLen = 0;
    char szBuf[128];
    int	iBufLength;
    //int iDisplay;
    //int iSignalID;
    //BOOL bdisplay;
    //BOOL bdisplay1;

    TRACE_WEB_USER("Begin to exec Web_MakeSingleBattDataFile[%d]\n", itype);

    //check the pointer valid begin
    if(gs_pWebBattSigInfo[itype] == NULL)
    {
	return NULL;
    }
    if((gs_pWebBattSigInfo[itype]->stWebEquipInfo == NULL) || (gs_pWebBattSigInfo[itype]->stWebPrivate == NULL))
    {
	return NULL;
    }
    //check the pointer valid end

    if((itype < COMM_BATT_SINGLE_SEQ) || (itype > SMDUE_BATT_SEQ))
    {
	return NULL;
    }

    if(itype == COMM_BATT_SINGLE_SEQ)
    {
	iSingleBattNum = gs_uiRealComBattNum;
    }
    else if(itype == EIB_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealEIBBattNum;
    }
    else if(itype == SMDU_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSMDUBattNum;
    }
    else if(itype == SMDUE_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSMDUEBattNum;
	//printf("Web_MakeSingleBattDataFile():gs_uiRealSMDUEBattNum=%d~~~\n",gs_uiRealSMDUEBattNum);
    }
    else if(itype == SM_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSMBattNum;
    }
    /*else if(itype == LARGEDU_BATT_SEQ)	// no largedu at present
    {
	return NULL;
	iSingleBattNum = gs_uiRealLargeDUBattNum;
	iSingleSignalNum = gs_pWebSigInfo[LARGEDU_BATT_SEQ].iNumber;
    }*/
    else if(itype == SMBRC_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSMBRCBattNum;
    }
    else if(itype == SONICK_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSoNickBattNum;
    }
    else
    {}

    iSingleSignalNum = gs_pWebBattSigInfo[itype]->iOneEquipNum;

    TRACE_WEB_USER("iSingleBattNum is %d\n", iSingleBattNum);
    if((iSingleSignalNum > 0) && (iSingleBattNum > 0))
    {
	iSingleBattSignalNum = iSingleSignalNum * iSingleBattNum;
	//szTemp = NEW(char, (80 * iSingleSignalNum + 90 * iSingleBattSignalNum + 100));
	/*if(szTemp == NULL)
	{
	    AppLogOut("Web_MakeSingleBattDataFile", APP_LOG_ERROR, "NEW (%d bytes) operation fails! Out of memory!\n",
		(80 * iSingleSignalNum + 90 * iSingleBattSignalNum + 100));
	    return NULL;
	}*/
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	iBufLength = 80 * iSingleSignalNum + 90 * iSingleBattSignalNum + 100;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[{");

	// get the signal name
	
	for(i = 0; i < iSingleSignalNum; i++)
	{
	    /*TRACE_WEB_USER("(%d,%d,%d)\n", gs_pWebSigInfo[itype].stWebPrivate[i].iEquipID,
	    gs_pWebSigInfo[itype].stWebPrivate[i].iSignalType, gs_pWebSigInfo[itype].stWebPrivate[i].iSignalID);*/
	    memset(szBuf, 0, sizeof(szBuf));
	    Web_GetSignalNameForBatt(&(gs_pWebBattSigInfo[itype]->stWebPrivate[i]), itype, szBuf, sizeof(szBuf));
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	}

	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "},");
	k = 0;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");


	for(i = 0; i < iSingleBattNum; i++)
	{
	    TRACE_WEB_USER("i = %d\n", i);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
 
	    if(gs_pWebBattSigInfo[itype]->stWebEquipInfo[i].bSingleValid == FALSE)
	    {

		TRACE_WEB_USER("The equip signal is not valid\n");
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"---\",\"---\"],");
	    }
	    else
	    {
		/*TRACE_WEB_USER("The equip signal is valid\n");
		TRACE_WEB_USER("itype is %d the equipID is %d\n", itype, gs_pWebBattSigInfo[itype - COMM_BATT_SEQ]->stWebEquipInfo[i].pEquipInfo->iEquipID);
		TRACE_WEB_USER("The equip signal is valid1\n");*/
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[\"%s\",\"%s\"],", gs_pWebBattSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[0],
		    gs_pWebBattSigInfo[itype]->stWebEquipInfo[i].pEquipInfo->pEquipName->pFullName[1]);
	    }
	    
	    for(j = 0; j < iSingleSignalNum; j++)
	    {
		TRACE_WEB_USER("i = %d, j = %d\n", i, j);
		memset(szBuf, 0, sizeof(szBuf));
		Makesignalstring(gs_pWebBattSigInfo[itype]->stWebPrivate[k], szBuf, sizeof(szBuf));
		//TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
		//TRACE_WEB_USER("[Web_MakeSingleBattDataFile] szBuf is %s\n", szBuf);
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
		k++;
	    }
	    if(iFileLen >= 1)
	    {
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	    }
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]]");
	TRACE_WEB_USER("end to exec Web_MakeSingleBattDataFile[%d]\n", itype);
	return szTemp;
    }
    else
    {
	TRACE_WEB_USER("end to exec Web_MakeSingleBattDataFile[%d]\n", itype);
	return NULL;
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeSysSetDataFile
* PURPOSE  :  To make the json file <data.sys_setting.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-02-24
*==========================================================================*/
void Web_MakeSysSetDataFile(void)
{
#define ID_EIB_UNIT_SET			"ID_EIB_UNIT_SET"
#define ID_SMDU_UNIT_SET		"ID_SMDU_UNIT_SET"
#define ID_LVD_GROUP_SET		"ID_LVD_GROUP_SET"
//changed by Frank Wu,13/17/32,20140312, for adding new web pages to the tab of "power system"
#define ID_SYSTEM_GROUP_SET		"ID_SYSTEM_GROUP_SET"
#define ID_AC_GROUP_SET			"ID_AC_GROUP_SET"
#define ID_AC_UNIT_SET			"ID_AC_UNIT_SET"
#define ID_ACMETER_GROUP_SET	"ID_ACMETER_GROUP_SET"
#define ID_ACMETER_UNIT_SET		"ID_ACMETER_UNIT_SET"
#define ID_DC_UNIT_SET			"ID_DC_UNIT_SET"
#define ID_DCMETER_GROUP_SET	"ID_DCMETER_GROUP_SET"
#define ID_DCMETER_UNIT_SET		"ID_DCMETER_UNIT_SET"
#define ID_DIESEL_GROUP_SET		"ID_DIESEL_GROUP_SET"
#define ID_DIESEL_UNIT_SET		"ID_DIESEL_UNIT_SET"
#define ID_FUEL_GROUP_SET		"ID_FUEL_GROUP_SET"
#define ID_FUEL_UNIT_SET		"ID_FUEL_UNIT_SET"
#define ID_IB_GROUP_SET			"ID_IB_GROUP_SET"
#define ID_IB_UNIT_SET			"ID_IB_UNIT_SET"
#define ID_EIB_GROUP_SET		"ID_EIB_GROUP_SET"
#define ID_OBAC_UNIT_SET		"ID_OBAC_UNIT_SET"
#define ID_OBLVD_UNIT_SET		"ID_OBLVD_UNIT_SET"
#define ID_OBFUEL_UNIT_SET		"ID_OBFUEL_UNIT_SET"
#define ID_SMDU_GROUP_SET		"ID_SMDU_GROUP_SET"
#define ID_SMDUP_GROUP_SET		"ID_SMDUP_GROUP_SET"
#define ID_SMDUP_UNIT_SET		"ID_SMDUP_UNIT_SET"
#define ID_SMDUH_GROUP_SET		"ID_SMDUH_GROUP_SET"
#define ID_SMDUH_UNIT_SET		"ID_SMDUH_UNIT_SET"
#define ID_SMBRC_GROUP_SET		"ID_SMBRC_GROUP_SET"
#define ID_SMBRC_UNIT_SET		"ID_SMBRC_UNIT_SET"
#define ID_SMIO_GROUP_SET		"ID_SMIO_GROUP_SET"
#define ID_SMIO_UNIT_SET		"ID_SMIO_UNIT_SET"
#define ID_SMTEMP_GROUP_SET		"ID_SMTEMP_GROUP_SET"
#define ID_SMTEMP_UNIT_SET		"ID_SMTEMP_UNIT_SET"
#define ID_SMAC_UNIT_SET		"ID_SMAC_UNIT_SET"
#define ID_SMLVD_UNIT_SET		"ID_SMLVD_UNIT_SET"
#define ID_LVD3_UNIT_SET		"ID_LVD3_UNIT_SET"
#define ID_OBBATTFUSE_UNIT_SET		"ID_OBBATTFUSE_UNIT_SET"
#define ID_NARADA_BMS_UNIT_SET		"ID_NARADA_BMS_UNIT_SET"
#define ID_NARADA_BMS_GROUP_SET		"ID_NARADA_BMS_GROUP_SET"

#define SYS_SET_DATA_FILE_REPLACE(iArgSeqId, sArgKeyStr)		\
	{	\
		pbuf = Web_MakeSingleSetDataFile(iArgSeqId);		\
		if(pbuf != NULL)	\
		{	\
			ReplaceString(&pHtml, MAKE_VAR_FIELD(sArgKeyStr), pbuf);	\
		}	\
		else	\
		{	\
			ReplaceString(&pHtml, MAKE_VAR_FIELD(sArgKeyStr), "[]");	\
		}	\
		/* if(pbuf) */	\
		if(0)	\
		{	\
			DELETE(pbuf);	\
			pbuf = NULL;	\
		}	\
	}


    TRACE_WEB_USER("Begin to exec Web_MakeSysSetDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    //char szBuf[80];

    iReturn = LoadHtmlFile(WEB_DATA_SYS_SET, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeSingleSetDataFile(SINGLE_EIB_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_EIB_UNIT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_EIB_UNIT_SET), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(SINGLE_SMDU_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDU_UNIT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDU_UNIT_SET), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(LVD_GROUP_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LVD_GROUP_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LVD_GROUP_SET), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/
	//changed by Frank Wu,14/18/32,20140312, for adding new web pages to the tab of "power system"
	SYS_SET_DATA_FILE_REPLACE( SYSTEM_GROUP_SEQ, ID_SYSTEM_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( AC_GROUP_SEQ, ID_AC_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( AC_UNIT_SEQ, ID_AC_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( ACMETER_GROUP_SEQ, ID_ACMETER_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( ACMETER_UNIT_SEQ, ID_ACMETER_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( DC_UNIT_SEQ, ID_DC_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( DCMETER_GROUP_SEQ, ID_DCMETER_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( DCMETER_UNIT_SEQ, ID_DCMETER_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( DIESEL_GROUP_SEQ, ID_DIESEL_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( DIESEL_UNIT_SEQ, ID_DIESEL_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( FUEL_GROUP_SEQ, ID_FUEL_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( FUEL_UNIT_SEQ, ID_FUEL_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( IB_GROUP_SEQ, ID_IB_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( IB_UNIT_SEQ, ID_IB_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( EIB_GROUP_SEQ, ID_EIB_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( OBAC_UNIT_SEQ, ID_OBAC_UNIT_SET);
//	SYS_SET_DATA_FILE_REPLACE( OBLVD_UNIT_SEQ, ID_OBLVD_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( OBFUEL_UNIT_SEQ, ID_OBFUEL_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( SMDU_GROUP_SEQ, ID_SMDU_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( SMDUP_GROUP_SEQ, ID_SMDUP_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( SMDUP_UNIT_SEQ, ID_SMDUP_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( SMDUH_GROUP_SEQ, ID_SMDUH_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( SMDUH_UNIT_SEQ, ID_SMDUH_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( SMBRC_GROUP_SEQ, ID_SMBRC_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( SMBRC_UNIT_SEQ, ID_SMBRC_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( SMIO_GROUP_SEQ, ID_SMIO_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( SMIO_UNIT_SEQ, ID_SMIO_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( SMTEMP_GROUP_SEQ, ID_SMTEMP_GROUP_SET);
	SYS_SET_DATA_FILE_REPLACE( SMTEMP_UNIT_SEQ, ID_SMTEMP_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( SMAC_UNIT_SEQ, ID_SMAC_UNIT_SET);
//	SYS_SET_DATA_FILE_REPLACE( SMLVD_UNIT_SEQ, ID_SMLVD_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( LVD3_UNIT_SEQ, ID_LVD3_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( OBBATTFUSE_UNIT_SEQ, ID_OBBATTFUSE_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( NARADA_BMS_SEQ1, ID_NARADA_BMS_UNIT_SET);
	SYS_SET_DATA_FILE_REPLACE( NARADA_BMS_GROUP_SEQ1, ID_NARADA_BMS_GROUP_SET);

	if((fp = fopen(WEB_DATA_SYS_SET_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeSysSetDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeBattSetDataFile
* PURPOSE  :  To make the json file <data.batt_setting.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-06-07
* MODIFY   :  Zhao Zicheng               DATE: 2015-06-07
*==========================================================================*/
void Web_MakeBattSetDataFile(void)
{
#define ID_COMM_BATT_SET	"ID_COMM_BATT_SET"
#define ID_EIB_BATT_SET		"ID_EIB_BATT_SET"
#define ID_SMDU_BATT_SET	"ID_SMDU_BATT_SET"
#define ID_SM_BATT_SET		"ID_SM_BATT_SET"
#define ID_SMBRC_BATT_SET	"ID_SMBRC_BATT_SET"
#define ID_SONICK_BATT_SET	"ID_SONICK_BATT_SET"
#define ID_SMDUE_BATT_SET		"ID_SMDUE_BATT_SET"

    TRACE_WEB_USER("Begin to exec Web_MakeBattSetDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    //char szBuf[80];

    iReturn = LoadHtmlFile(WEB_DATA_BATTSET, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeSingleSetDataFile(COMM_BATT_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_COMM_BATT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_COMM_BATT_SET), "[]");
	}
	/*if(pbuf)
	{
	DELETE(pbuf);
	pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(EIB_BATT_SEQ1);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_EIB_BATT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_EIB_BATT_SET), "[]");
	}

	pbuf = Web_MakeSingleSetDataFile(SMDU_BATT_SEQ1);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDU_BATT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDU_BATT_SET), "[]");
	}

	pbuf = Web_MakeSingleSetDataFile(SMDUE_BATT_SEQ1);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_BATT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_BATT_SET), "[]");
	}

	pbuf = Web_MakeSingleSetDataFile(SM_BATT_SEQ1);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SM_BATT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SM_BATT_SET), "[]");
	}

	pbuf = Web_MakeSingleSetDataFile(SMBRC_BATT_SEQ1);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMBRC_BATT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMBRC_BATT_SET), "[]");
	}

	pbuf = Web_MakeSingleSetDataFile(SONICK_BATT_SEQ1);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SONICK_BATT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SONICK_BATT_SET), "[]");
	}

	if((fp = fopen(WEB_DATA_BATTSET_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeBattSetDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeRectSetDataFile
* PURPOSE  :  To make the json file <data.rect_setting.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-17
*==========================================================================*/
void Web_MakeRectSetDataFile(void)
{
#define ID_RECT_SET	"ID_RECT_SET"

    TRACE_WEB_USER("Begin to exec Web_MakeRectSetDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    //char szBuf[80];

    iReturn = LoadHtmlFile(WEB_DATA_RECTSET, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeSingleSetDataFile(SINGLE_RECT_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECT_SET), "");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	if((fp = fopen(WEB_DATA_RECTSET_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeRectSetDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeConverterSetDataFile
* PURPOSE  :  To make the json file <data.single_converter_setting.html>
									<single_solar_setting.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Frank Wu               DATE: 2014-03-22
*==========================================================================*/
//changed by Frank Wu,17/10/30,20140527, for add single converter and single solar settings pages
void Web_MakeConverterSetDataFile(void)
{
#define CONVERTER_SET_DATA_FILE_REPLACE( iArgSeq, iArgInFile, iArgOutFile, sArgKeyStr)		\
	{	\
		pHtml = NULL;	\
		iReturn = LoadHtmlFile(iArgInFile, &pHtml);	\
		if(iReturn > 0)	\
		{	\
			pbuf = Web_MakeSingleSetDataFile(iArgSeq);	\
			if(pbuf != NULL)	\
			{	\
				ReplaceString(&pHtml, MAKE_VAR_FIELD(sArgKeyStr), pbuf);	\
			}	\
			else	\
			{	\
				ReplaceString(&pHtml, MAKE_VAR_FIELD(sArgKeyStr), "");	\
			}	\
			/* if(pbuf) */	\
			if(0)	\
			{	\
				DELETE(pbuf);	\
				pbuf = NULL;	\
			}	\
			\
			if((fp = fopen(iArgOutFile, "wb")) != NULL && pHtml != NULL)	\
			{	\
				fwrite(pHtml,strlen(pHtml), 1, fp);	\
				fclose(fp);		\
			}	\
			else	\
			{}	\
		}	\
		else	\
		{	\
		}	\
		if(pHtml)	\
		{	\
			DELETE(pHtml);		\
			pHtml = NULL;		\
		}	\
	}

#define ID_SINGLE_CONVERTER_SET		"ID_SINGLE_CONVERTER_SET"
#define ID_SINGLE_SOLAR_SET			"ID_SINGLE_SOLAR_SET"
#define ID_SINGLE_RECT_S1_SET		"ID_SINGLE_RECT_S1_SET"
#define ID_SINGLE_RECT_S2_SET		"ID_SINGLE_RECT_S2_SET"
#define ID_SINGLE_RECT_S3_SET		"ID_SINGLE_RECT_S3_SET"


	//char *szFile = NULL;
	char *pHtml = NULL;
	int	 iReturn = 0;
	char *pbuf = NULL;
	FILE	*fp;
	//char szBuf[80];

	TRACE_WEB_USER("Begin to exec Web_MakeConverterSetDataFile\n");

	CONVERTER_SET_DATA_FILE_REPLACE( SINGLE_CONVERTER_SEQ,
									WEB_DATA_SINGLE_CONVERTER,
									WEB_DATA_SINGLE_CONVERTER_VAR,
									ID_SINGLE_CONVERTER_SET);
	CONVERTER_SET_DATA_FILE_REPLACE( SINGLE_SOLAR_SEQ,
									WEB_DATA_SINGLE_SOLAR,
									WEB_DATA_SINGLE_SOLAR_VAR,
									ID_SINGLE_SOLAR_SET);
	CONVERTER_SET_DATA_FILE_REPLACE( SINGLE_RECT_S1_SEQ,
									WEB_DATA_SINGLE_RECT_S1,
									WEB_DATA_SINGLE_RECT_S1_VAR,
									ID_SINGLE_RECT_S1_SET);
	CONVERTER_SET_DATA_FILE_REPLACE( SINGLE_RECT_S2_SEQ,
									WEB_DATA_SINGLE_RECT_S2,
									WEB_DATA_SINGLE_RECT_S2_VAR,
									ID_SINGLE_RECT_S2_SET);
	CONVERTER_SET_DATA_FILE_REPLACE( SINGLE_RECT_S3_SEQ,
									WEB_DATA_SINGLE_RECT_S3,
									WEB_DATA_SINGLE_RECT_S3_VAR,
									ID_SINGLE_RECT_S3_SET);


	TRACE_WEB_USER("end to exec Web_MakeConverterSetDataFile\n");
	return;
}

//added by Stone Song to appear the relay DO only exist in system 20160713
char* Web_MakeExistRealy()
{
	int i = 0, j = 0;
	char *szTemp = NULL;
	int iFileLen = 0;
	int iBufLength = 0;
	char xOut[128];
	char szBuf[4096];
	int iBufLen  = 0;
	char relayName[10];

	int iIndex = 0;
	int iSignalPairNum = 0;
	WEB_GENERAL_SETTING_INFO *pWebInfoItem = NULL;
	CTRL_SIG_VALUE *pCtlSig = NULL;

	EQUIP_INFO *pEquipInfo = NULL;
	RELAYOPTIONS webRelayOptions;

	if(NULL == gs_pWebSetInfo[RELAY_OPTIONS_SEQ].stWebPrivateSet)		
	{
		return NULL;
	}
	szTemp = NEW(char, 1200);
	memset(szTemp, 0, 1200);
//	szTemp = existRelayBuffer;
	iFileLen = 0;
	//init
	snprintf(szTemp, strlen("[]"), "[]");


	iSignalPairNum = gs_pWebSetInfo[RELAY_OPTIONS_SEQ].iNumber;		
	
	//printf("iSignalPairNum = %d\n", iSignalPairNum);
	iBufLength = MAX_SETTING_LEN * iSignalPairNum + 1024;
	//head
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	//body
//	printf("NUM%d\n",webRelayOptions.relayOptionNum);
	for(i = 0; i < 30; i++)
	{
		webRelayOptions.relayMess[i].relayOptionEquipid=0;
	}

	for(i = 0; i < iSignalPairNum; i++)
	{
		if(gs_pWebSetInfo[RELAY_OPTIONS_SEQ].stWebPrivateSet[i].pEquipInfo->iEquipID == 202)
			
		{
			if(g_bEquipIB1Exist == FALSE)//IB1)
			{
				continue;
			}
		}
		else if(!gs_pWebSetInfo[RELAY_OPTIONS_SEQ].stWebPrivateSet[i].bWorkStatus 
			|| !gs_pWebSetInfo[RELAY_OPTIONS_SEQ].stWebPrivateSet[i].pEquipInfo 
			|| !gs_pWebSetInfo[RELAY_OPTIONS_SEQ].stWebPrivateSet[i].pEquipInfo->bWorkStatus)
		{
			continue;
		}


		
		//printf("bWorkStatus   -3\n");
		pWebInfoItem = &gs_pWebSetInfo[RELAY_OPTIONS_SEQ].stWebPrivateSet[i];
		pCtlSig = (CTRL_SIG_VALUE *)(pWebInfoItem->pSigValue);
		pEquipInfo = pWebInfoItem->pEquipInfo;		
		

//		printf("[%d,%d,%d,%s,%s]\n",pWebInfoItem->iEquipID,pWebInfoItem->iSignalType,pWebInfoItem->iSignalID,pEquipInfo->pEquipName->pFullName[0],pCtlSig->pStdSig->pSigName->pFullName[0]);

		if(pWebInfoItem->iEquipID == 202)//IB2-1
		{
			webRelayOptions.relayMess[i].relayOptionEquipid=202;
			webRelayOptions.relayMess[i].relayOptionValue=i+1;
			snprintf(webRelayOptions.relayMess[i].relayName,32, "[\"IB2-1 DO%d\",\"IB2-1 DO%d\"]",i+1,i+1);
		}
		else if(pWebInfoItem->iEquipID == 197)//EIB1
		{
			webRelayOptions.relayMess[i].relayOptionEquipid=197;
			webRelayOptions.relayMess[i].relayOptionValue=i+1;
			snprintf(webRelayOptions.relayMess[i].relayName,32, "[\"EIB1 DO%d\",\"EIB1 DO%d\"]",i-7,i-7);

		}
		else if(pWebInfoItem->iEquipID == 1)//NCU
		{
			webRelayOptions.relayMess[i].relayOptionEquipid=1;
			webRelayOptions.relayMess[i].relayOptionValue=i+1;
			snprintf(webRelayOptions.relayMess[i].relayName,32, "[\"NCU DO%d\",\"NCU DO%d\"]",i-12,i-12);

		}
		else if(pWebInfoItem->iEquipID == 203)//IB2-2
		{
			webRelayOptions.relayMess[i].relayOptionEquipid=203;
			webRelayOptions.relayMess[i].relayOptionValue=i+1;
			snprintf(webRelayOptions.relayMess[i].relayName,32, "[\"IB2-2 DO%d\",\"IB2-2 DO%d\"]",i-16,i-16);

		}
		else if(pWebInfoItem->iEquipID == 198)//EIB2
		{
			webRelayOptions.relayMess[i].relayOptionEquipid=198;
			webRelayOptions.relayMess[i].relayOptionValue=i+1;
			snprintf(webRelayOptions.relayMess[i].relayName,32, "[\"EIB2 DO%d\",\"EIB2 DO%d\"]",i-24,i-24);

		}

	}
	webRelayOptions.relayOptionNum=i;
//printf("relayOptionNum=%d, szTemp=%s\n",webRelayOptions.relayOptionNum,szTemp);
	for(i = 0; i < webRelayOptions.relayOptionNum; i++)
	{
		if(webRelayOptions.relayMess[i].relayOptionEquipid==1)
		{
//printf("ncu %d ",i);
			iIndex++;
			//split char between two items
			if(iIndex != 1)
			{
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
			}
			iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[%d,%s]",webRelayOptions.relayMess[i].relayOptionValue,webRelayOptions.relayMess[i].relayName);
//			printf("----%s\n",szTemp);
		}
	}
	for(i = 0; i < webRelayOptions.relayOptionNum; i++)
	{
		if(webRelayOptions.relayMess[i].relayOptionEquipid==202 || webRelayOptions.relayMess[i].relayOptionEquipid==203)
		{
//printf("IB %d ",i);
			iIndex++;
			//split char between two items
			if(iIndex != 1)
			{
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
			}
			iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[%d,%s]",webRelayOptions.relayMess[i].relayOptionValue,webRelayOptions.relayMess[i].relayName);
		}
	}
	for(i = 0; i < webRelayOptions.relayOptionNum; i++)
	{
		if(webRelayOptions.relayMess[i].relayOptionEquipid==197 || webRelayOptions.relayMess[i].relayOptionEquipid==198)
		{
//printf("EIB %d ",i);
			iIndex++;
			//split char between two items
			if(iIndex != 1)
			{
				iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), ",");
			}
			iFileLen += snprintf(szTemp + iFileLen,
					(iBufLength - iFileLen),
					"[%d,%s]",webRelayOptions.relayMess[i].relayOptionValue,webRelayOptions.relayMess[i].relayName);
		}
	}


	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");

	TRACE_WEB_USER("end to exec Web_MakeExistRealy()szTemp = %s\n", szTemp);
//	printf("make existrelay szTemp:%s\n",szTemp );
	return szTemp;
}


//changed by Frank Wu,19/N/35,20140527, for adding the the web setting tab page 'DI'
void Web_MakeDIDataFile(void)
{
#define ID_DI_SET	"ID_DI_SET"
#define ID_DI_RELAY_OPTIONS	"ID_DI_RELAY_OPTIONS"

    TRACE_WEB_USER("Begin to exec Web_MakeDIDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    //char szBuf[80];

    iReturn = LoadHtmlFile(WEB_DATA_DI, &pHtml);
    if(iReturn > 0)
    {
		pbuf = Web_MakeDISetDataFile(DI_BASIC_SEQ, DI_STATE_SEQ);
		if(pbuf != NULL)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DI_SET), pbuf);
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DI_SET), "[]");
		}
		//added by Stone Song to appear the relay DO only exist in system 20160713
		pbuf = Web_MakeExistRealy();
		if(pbuf != NULL)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DI_RELAY_OPTIONS), pbuf);
			DELETE(pbuf);
			pbuf=NULL;
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DI_RELAY_OPTIONS), "[]");
		}
	//if(pbuf)
	//{
	//    DELETE(pbuf);
	//    pbuf = NULL;
	//}

		if((fp = fopen(WEB_DATA_DI_VAR,"wb")) != NULL && (pHtml != NULL))
		{
			fwrite(pHtml,strlen(pHtml), 1, fp);
			fclose(fp);
		}
		else
		{}
    }
    else
    {}

	SAFELY_DELETE(pHtml);

    TRACE_WEB_USER("end to exec Web_MakeDIDataFile\n");
    return;
}

//changed by Frank Wu,19/N/35,20140527, for adding the the web setting tab page 'DI'
void Web_MakeDODataFile(void)
{
#define ID_DO_SET	"ID_DO_SET"
#define ID_NCU_SET	"ID_NCU_SET"
	


    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    //char szBuf[80];

    iReturn = LoadHtmlFile(WEB_DATA_DO, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeDOSetDataFile(DO_RELAY_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DO_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DO_SET), "[]");
	}
	
	pbuf = Web_MakeDOSetDataFile(DO_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_NCU_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_NCU_SET), "[]");
	}

	if((fp = fopen(WEB_DATA_DO_VAR,"wb")) != NULL)
	{
		if(pHtml != NULL)
		{
			fwrite(pHtml,strlen(pHtml), 1, fp);
		}	    
		fclose(fp);
	}
	else
	{}

	SAFELY_DELETE(pHtml);
    }
    else
    {
    }
   
    return;
}


//data.setting_analog.html 
void Web_MakeAnalogDataFile(void)
{

#define ID_SMDUE_ANALOG_SET		"ID_SMDUE_ANALOG_SET"
#define ID_ANALOG_RELAY_OPTIONS	"ID_ANALOG_RELAY_OPTIONS"
#define ID_COMMON_ANALOG			"ID_COMMON_ANALOG"

    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;

	int					iRst;
	int 				iBufLen;
	SIG_BASIC_VALUE*	pSigValue;


    iReturn = LoadHtmlFile(WEB_DATA_ANALOG, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeAnalogSetDataFile(SMDUE_ANALOG_SEQ);
	
	//printf("Web_MakeAnalogDataFile():strlen(pbuf)=%d~~~\n",strlen(pbuf));
	
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_ANALOG_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_ANALOG_SET), "[]");
	}



	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					1,
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 710),
					&iBufLen,
					&pSigValue,
					0);
	if(0==pSigValue->varValue.enumValue)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_COMMON_ANALOG), "0");
	}
	else if(1==pSigValue->varValue.enumValue)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_COMMON_ANALOG), "1");
	}


	pbuf = Web_MakeExistRealy();
	if(pbuf != NULL)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ANALOG_RELAY_OPTIONS), pbuf);
		DELETE(pbuf);
		pbuf=NULL;
	}
	else
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ANALOG_RELAY_OPTIONS), "[]");
	}

	if((fp = fopen(WEB_DATA_ANALOG_VAR,"wb")) != NULL && (pHtml != NULL))
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

	SAFELY_DELETE(pHtml);

   
    return;
}



//data.setting_custominputs.html
void Web_MakeCustom_InputsDataFile(void)
{
#define ID_SMDUE_INPUTBLOCK	"ID_SMDUE_INPUTBLOCK"


    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;

    iReturn = LoadHtmlFile(WEB_DATA_CUSTOM_INPUTS, &pHtml);
    if(iReturn > 0)
    {
		pbuf = Web_MakeCustomInput_SetDataFile(SMDUE_CUSTOM_INPUT_SEQ);
		if(pbuf != NULL)
		{
		    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_INPUTBLOCK), pbuf);
		}
		else
		{
		    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_INPUTBLOCK), "[]");
		}

		if((fp = fopen(WEB_DATA_CUSTOM_INPUTS_VAR,"wb")) != NULL && (pHtml != NULL))
		{
	  	 	fwrite(pHtml,strlen(pHtml), 1, fp);
	   		fclose(fp);
		}
		else
		{
		}
    }
    else
    {
    }

	SAFELY_DELETE(pHtml);

   
    return;
}



//data.setting_shunts.html for EIB and SMDU shunt
void Web_MakeShuntDataFile(void)
{
#define ID_SMDU_SHUNT_SET	"ID_SMDU_SHUNT_SET"
#define ID_EIB_SHUNT_SET	"ID_EIB_SHUNT_SET"
#define ID_DCD_SHUNT_SET	"ID_DCD_SHUNT_SET"
#define ID_SMDUP_SHUNT_SET	"ID_SMDUP_SHUNT_SET"
#define ID_SMDUE_SHUNT_SET	"ID_SMDUE_SHUNT_SET"
#define ID_BATT_SHUNT_SET	"ID_BATT_SHUNT_SET"

    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;

    iReturn = LoadHtmlFile(WEB_DATA_SHUNT_SMDU, &pHtml);
    if(iReturn > 0)
    {

	pbuf = Web_MakeShuntSetDataFile(SHUNT_SMDUP_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUP_SHUNT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUP_SHUNT_SET), "[]");
	}
	
/*
	pbuf = Web_MakeShuntSetDataFile(SHUNT_SMDUE_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_SHUNT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_SHUNT_SET), "[]");
	}
*/

	pbuf = Web_MakeShuntSetDataFile(SHUNT_DCD_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DCD_SHUNT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DCD_SHUNT_SET), "[]");
	}

	pbuf = Web_MakeShuntSetDataFile(SHUNT_BATT_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT_SHUNT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT_SHUNT_SET), "[]");
	}
	//added by Stone Song to appear the relay DO only exist in system 20160713
	pbuf = Web_MakeExistRealy();
	if(pbuf != NULL)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DI_RELAY_OPTIONS), pbuf);
		DELETE(pbuf);
		pbuf=NULL;
	}
	else
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_DI_RELAY_OPTIONS), "[]");
	}
	
	if((fp = fopen(WEB_DATA_SHUNT_SMDU_VAR,"wb")) != NULL && (pHtml != NULL))
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

	SAFELY_DELETE(pHtml);

   
    return;
}

//data.setting_shunts1.html for EIB and SMDU shunt
void Web_MakeShuntDataFile_Sec(void)
{
#define ID_SMDU_SHUNT_SET	"ID_SMDU_SHUNT_SET"
#define ID_EIB_SHUNT_SET	"ID_EIB_SHUNT_SET"
#define ID_SMDUE_SHUNT_SET	"ID_SMDUE_SHUNT_SET"


    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;

    iReturn = LoadHtmlFile(WEB_DATA_SHUNT_SMDU1, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeShuntSetDataFile(SHUNT_SMDU_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDU_SHUNT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDU_SHUNT_SET), "[]");
	}


	pbuf = Web_MakeShuntSetDataFile(SHUNT_SMDUE_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_SHUNT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_SHUNT_SET), "[]");
	}

	pbuf = Web_MakeShuntSetDataFile(SHUNT_EIB_BASIC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_EIB_SHUNT_SET), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_EIB_SHUNT_SET), "[]");
	}

	if((fp = fopen(WEB_DATA_SHUNT_SMDU1_VAR,"wb")) != NULL && (pHtml != NULL))
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

	SAFELY_DELETE(pHtml);

   
    return;
}



//changed by Stone Song ,20160525, for adding the the web setting tab page 'FUSE'
void Web_MakeFuseDataFile(void)
{
#define ID_CR_SET				"ID_CR_SET"

    //printf("============Begin to exec Web_MakefuseDataFile\n");
    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf;
	char *pbuf_temp = NULL;
	int npbuf_temp=0, npbuf=1;
    FILE	*fp;
    //char szBuf[80];
	//CAN NOT USE gs_szBuffer instead of gs_szBuffer1
	char gs_szBuffer1[BUFFER_LEN];
	memset(gs_szBuffer1, 0, BUFFER_LEN);
	pbuf = gs_szBuffer1;
    iReturn = LoadHtmlFile(WEB_DATA_FUSE, &pHtml);
    if(iReturn > 0)
    {

		sprintf(pbuf,"[");
		pbuf_temp = Web_MakeFusetSetDataFile(OBBATT_FUSE_SEQ);
		if(pbuf_temp != NULL)
		{
			npbuf_temp=strlen(pbuf_temp);
			memmove( (pbuf+npbuf), pbuf_temp, (size_t)npbuf_temp);
		}
		else 		
			npbuf_temp=0;

		npbuf+=npbuf_temp;
		pbuf_temp = Web_MakeFusetSetDataFile(SMDUBATT_FUSE_SEQ);
		if(pbuf_temp != NULL)
		{
			npbuf_temp=strlen(pbuf_temp);
			memmove( (pbuf+npbuf), pbuf_temp, (size_t)npbuf_temp);
		}
		else 		
			npbuf_temp=0;

		npbuf+=npbuf_temp;
		pbuf_temp = Web_MakeFusetSetDataFile(OBDC_FUSE_SEQ);
		if(pbuf_temp != NULL)
		{
			npbuf_temp=strlen(pbuf_temp);
			memmove( (pbuf+npbuf), pbuf_temp, (size_t)npbuf_temp);
		}
		else 		
			npbuf_temp=0;

		npbuf+=npbuf_temp;
		pbuf_temp = Web_MakeFusetSetDataFile(SMDUDC_FUSE_SEQ);
		if(pbuf_temp != NULL)
		{	
			npbuf_temp=strlen(pbuf_temp);
			memmove( (pbuf+npbuf), pbuf_temp, (size_t)npbuf_temp);
		}
		else 		
			npbuf_temp=0;

		npbuf+=npbuf_temp;
		pbuf_temp = Web_MakeFusetSetDataFile(SMDUPLUS_FUSE_SEQ);
		if(pbuf_temp != NULL)
		{	
			npbuf_temp=strlen(pbuf_temp);
			memmove( (pbuf+npbuf), pbuf_temp, (size_t)npbuf_temp);
		}
		else 		
			npbuf_temp=0;

/*
		npbuf+=npbuf_temp;
		pbuf_temp = Web_MakeFusetSetDataFile(SMDUEBATT_FUSE_SEQ);
		if(pbuf_temp != NULL)
		{	
			npbuf_temp=strlen(pbuf_temp);
			memmove( (pbuf+npbuf), pbuf_temp, (size_t)npbuf_temp);
		}
		else 		
			npbuf_temp=0;
*/

		npbuf+=npbuf_temp;
		pbuf_temp = Web_MakeFusetSetDataFile(SMDUEDC_FUSE_SEQ);
		if(pbuf_temp != NULL)
		{	
			npbuf_temp=strlen(pbuf_temp);
			memmove( (pbuf+npbuf), pbuf_temp, (size_t)npbuf_temp);
		}
		else 		
			npbuf_temp=0;

		npbuf+=npbuf_temp;
		// get rid of the last "," to meet json format
		if(npbuf==1)
			sprintf( pbuf+npbuf, "]\0");
		else
			sprintf( pbuf+npbuf-1, "]\0");

		if(pbuf != NULL)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CR_SET), pbuf);
		}

//		printf("pHtml:%s\n",pHtml);
		if((fp = fopen(WEB_DATA_FUSE_VAR,"wb")) != NULL && (pHtml != NULL))
		{
			fwrite(pHtml,strlen(pHtml), 1, fp);
			fclose(fp);
		}
		else
		{}
    }
    else
    {}

	SAFELY_DELETE(pHtml);

   
    return;
}


/*==========================================================================*
* FUNCTION :  Web_MakeLangOptionBuf
* PURPOSE  :  To make the language option by directory 
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  SongXu               DATE: 2017-02-24
*==========================================================================*/
void Web_MakeLangOptionBuf(void)
{
	DIR *		dir;
	struct		dirent * ptr;
	char		*filename;
	int			i=0;
	int			iLogFlag = 0;
	int iBufLen = 0;
	//static const char szLangDirName[LANGUAGE_NUM][3] = 
	//{
	//	"de","es","fr","it","ru","tr","tw","zh"
	//};

	char *szTemp = gs_szLangOptionBuf;
	szTemp = gs_szLangOptionBuf;
	iBufLen += snprintf(szTemp + iBufLen, (MAX_LENGTH_LANG_OPTION - iBufLen), "[");

	dir = opendir("/app/config_default/lang/");
	if(dir!=NULL)
	{
		while((ptr = readdir(dir)) != NULL)
		{
			//printf("d_name : %s\n", ptr->d_name);
			filename = ptr->d_name; 
			for(i = 0; i < LANGUAGE_NUM; i++)
			{
				if(strncmp(szWebLangType[i], filename, 2)==0)
				{
					iBufLen += snprintf(szTemp + iBufLen, (MAX_LENGTH_LANG_OPTION - iBufLen), "[%d,\"%s\"],",i, szWebLangType[i]);
					break;
				}
			}
		}
		closedir(dir);
	}

	if(iBufLen > 1)
	{
		iBufLen--;
		iBufLen += snprintf(szTemp + iBufLen, (MAX_LENGTH_LANG_OPTION - iBufLen), "]");
	}
	return;
}


/*==========================================================================*
* FUNCTION :  Web_MakeWizardDataFile
* PURPOSE  :  To make the json file <data.install_wizard.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeWizardDataFile(void)
{
#define ID_WIZARD	"ID_WIZARD"
#define ID_SITENAME	"ID_SITENAME"

    TRACE_WEB_USER("Begin to exec Web_MakeWizardDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    char szBuf[80];

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeWizardDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_WIZARD);*/
    iReturn = LoadHtmlFile(WEB_DATA_WIZARD, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeSingleSetDataFile(WIZARD_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_WIZARD), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_WIZARD), "[]");
	}
	if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[\"%s\",\"%s\"]", g_SiteInfo.langSiteName.pFullName[0], g_SiteInfo.langSiteName.pFullName[0]);//here just display English site name
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SITENAME), szBuf);



	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_WIZARD_VAR);*/
	if((fp = fopen(WEB_DATA_WIZARD_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeWizardDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeSettingConfigDataFile
* PURPOSE  :  To make the json file <data.udef_setting.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeSettingConfigDataFile(void)
{
#define ID_CONFIG1_SET	"ID_CONFIG1_SET"
#define ID_CONFIG2_SET	"ID_CONFIG2_SET"
#define ID_CONFIG3_SET	"ID_CONFIG3_SET"
#define ID_CONFIG4_SET	"ID_CONFIG4_SET"

    TRACE_WEB_USER("Begin to exec Web_MakeSettingConfigDataFile\n");
    char *pHtml = NULL;
    int	 iReturn = 0;
    char *pbuf = NULL;
    FILE	*fp;
    //char szBuf[80];

    iReturn = LoadHtmlFile(WEB_DATA_UDEF_SETTING, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeSingleSetDataFile(CONFIG1_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG1_SET), pbuf);
	    gs_stSetState.iConfig1 = 1;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG1_SET), "[]");
	    gs_stSetState.iConfig1 = 0;
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(CONFIG2_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG2_SET), pbuf);
	    gs_stSetState.iConfig2 = 1;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG2_SET), "[]");
	    gs_stSetState.iConfig2 = 0;
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(CONFIG3_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG3_SET), pbuf);
	    gs_stSetState.iConfig3 = 1;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG3_SET), "[]");
	    gs_stSetState.iConfig3 = 0;
	}

	pbuf = Web_MakeSingleSetDataFile(CONFIG4_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG4_SET), pbuf);
	    gs_stSetState.iConfig4 = 1;
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONFIG4_SET), "[]");
	    gs_stSetState.iConfig4 = 0;
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	if((fp = fopen(WEB_DATA_UDEF_SETTING_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeSettingConfigDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeSettingDataFile
* PURPOSE  :  To make the json file <data.setting.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeSettingDataFile(void)
{
//changed by Frank Wu,12/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
#define SET_DATA_FILE_REPLACE_BASIC(iArgSeqId, iArgDispId, iArgKeyStr, iArgKeyStrExist)		\
	{	\
		pbuf = Web_MakeSingleSetDataFile(iArgSeqId);	\
		if(pbuf != NULL)	\
		{	\
			ReplaceString(&pHtml, MAKE_VAR_FIELD(iArgKeyStr), pbuf);			\
		    if(gs_pWebDisplayInfo[0].stWebDisplaySet[iArgDispId].iDisEnable == 1)		\
		    {	\
				ReplaceString(&pHtml1, MAKE_VAR_FIELD(iArgKeyStrExist), "0");		\
		    }	\
		    else	\
		    {	\
				ReplaceString(&pHtml1, MAKE_VAR_FIELD(iArgKeyStrExist), "1");		\
		    }	\
		}	\
		else	\
		{	\
			ReplaceString(&pHtml, MAKE_VAR_FIELD(iArgKeyStr), "[]");			\
			ReplaceString(&pHtml1, MAKE_VAR_FIELD(iArgKeyStrExist), "1");		\
		}	\
		/* if(pbuf) */	\
		if(0)	\
		{	\
			DELETE(pbuf);	\
			pbuf = NULL;	\
		}	\
	}

#define SET_DATA_FILE_REPLACE_ADVANCE(iArgSeqId, iArgKeyStr)		\
	{	\
		pbuf = Web_MakeSingleSetDataFile(iArgSeqId);	\
		if(pbuf != NULL)	\
		{	\
			ReplaceString(&pHtml, MAKE_VAR_FIELD(iArgKeyStr), pbuf);			\
		}	\
		else	\
		{	\
			ReplaceString(&pHtml, MAKE_VAR_FIELD(iArgKeyStr), "[]");			\
		}	\
		/* if(pbuf) */	\
		if(0)	\
		{	\
			DELETE(pbuf);	\
			pbuf = NULL;	\
		}	\
	}


#define ID_CHARGE_SET	"ID_CHARGE_SET"
#define ID_ECO_SET	"ID_ECO_SET"
#define ID_LVD_SET	"ID_LVD_SET"
#define ID_RECT_SET	"ID_RECT_SET"
#define ID_BATT_TEST_SET	"ID_BATT_TEST_SET"
#define ID_TEMP_SET		"ID_TEMP_SET"
#define ID_HYBRID_SET		"ID_HYBRID_SET"
#define ID_CONVERTER_SET	"ID_CONVERTER_SET"
#define ID_WIZARD	"ID_WIZARD"
#define ID_SITENAME	"ID_SITENAME"
#define ID_SITE_LOCATE	"ID_SITE_LOCATE"
#define ID_SITE_DESC	"ID_SITE_DESC"
//changed by Frank Wu,11/13/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
#define ID_SOLAR_SET_SET		"ID_SOLAR_SET_SET"
#define ID_SHUNT_SET_SET		"ID_SHUNT_SET_SET"

#define ID_CHARGE_EXIST		"ID_CHARGE_EXIST"
#define ID_ECO_EXIST		"ID_ECO_EXIST"
#define ID_LVD_EXIST		"ID_LVD_EXIST"
#define ID_RECT_EXIST		"ID_RECT_EXIST"
#define ID_BATT_TEST_EXIST		"ID_BATT_TEST_EXIST"
#define ID_TEMPERATURE_EXIST		"ID_TEMPERATURE_EXIST"
#define ID_HYBRID_EXIST		"ID_HYBRID_EXIST"
#define ID_CONVERTER_EXIST		"ID_CONVERTER_EXIST"
#define ID_CONFIG1_EXIST    "ID_CONFIG1_EXIST"
#define ID_CONFIG2_EXIST    "ID_CONFIG2_EXIST"
#define ID_CONFIG3_EXIST    "ID_CONFIG3_EXIST"
#define ID_CONFIG4_EXIST    "ID_CONFIG4_EXIST"
#define ID_CABINET_EXIST    "ID_CABINET_EXIST"
#define ID_SYSTEM_EXIST	    "ID_SYSTEM_EXIST"
#define ID_LANGUAGE_OPTION    "ID_LANGUAGE_OPTION"

//changed by Frank Wu,12/14/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
#define ID_SOLAR_SET_EXIST		"ID_SOLAR_SET_EXIST"
//Add the function of configure the advance settings
#define ID_USER_EXIST		"ID_USER_EXIST"
#define ID_SNMP_EXIST		"ID_SNMP_EXIST"
#define ID_LANG_EXIST		"ID_LANG_EXIST"
#define ID_SW_EXIST		"ID_SW_EXIST"
#define ID_ALARM_EXIST		"ID_ALARM_EXIST"
#define ID_DI_ALARM_EXIST		"ID_DI_ALARM_EXIST"
#define ID_REPORT_EXIST		"ID_REPORT_EXIST"
#define ID_GENERATOR_EXIST		"ID_GENERATOR_EXIST"
#define ID_SHUNT_EXIST		"ID_SHUNT_EXIST"
#define ID_POWER_SPLIT_EXIST		"ID_POWER_SPLIT_EXIST"
#define ID_PROTOCOL_EXIST		"ID_PROTOCOL_EXIST"
#define ID_CLEAR_DATA_EXIST		"ID_CLEAR_DATA_EXIST"

#define CUSTOMINPUT_EXIST		"CUSTOMINPUT_EXIST"

#define ID_ANALOG_EXIST	"ID_ANALOG_EXIST"


    TRACE_WEB_USER("Begin to exec Web_MakeSettingDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    char *pHtml1 = NULL;
    int	 iReturn = 0;
    int	 iReturn1 = 0;
    char *pbuf = NULL;
    FILE	*fp;
    FILE	*fp1;
    char szBuf[80];


	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeSettingDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_SETTING);*/
    iReturn = LoadHtmlFile(WEB_DATA_SETTING, &pHtml);
    iReturn1 = LoadHtmlFile(WEB_DATA_WIZARD, &pHtml1);

    
    if((iReturn > 0) && (iReturn1 > 0))
    {
	pbuf = Web_MakeSingleSetDataFile(CHARGE_SEQ);

	
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CHARGE_SET), pbuf);
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[CHARGE_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CHARGE_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CHARGE_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CHARGE_SET), "[]");
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CHARGE_EXIST), "1");
	}

	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/


	//Fengel 
	if( (gs_uiRealSMDUENum >0) && (gs_uiRealSMDUENum <9))
	{
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(CUSTOMINPUT_EXIST), "0");
	}
	else
	{
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(CUSTOMINPUT_EXIST), "1");
	}



	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					1,
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 710),
					&iBufLen,
					&pSigValue,
					0);
	if(0==pSigValue->varValue.enumValue)
	{
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_ANALOG_EXIST), "0");
	}
	else if(1==pSigValue->varValue.enumValue)
	{
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_ANALOG_EXIST), "1");
	}


	pbuf = Web_MakeSingleSetDataFile(ECO_SEQ);

	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ECO_SET), pbuf);
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[ECO_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_ECO_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_ECO_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_ECO_SET), "[]");
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_ECO_EXIST), "1");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/
//language option for WEB
ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_LANGUAGE_OPTION), gs_szLangOptionBuf);

	pbuf = Web_MakeSingleSetDataFile(LVD_SEQ);

	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LVD_SET), pbuf);
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[LVD_FUNCTION].iDisEnable == 1)	
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_LVD_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_LVD_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LVD_SET), "[]");
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_LVD_EXIST), "1");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(RECT_SET_SEQ);

	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECT_SET), pbuf);
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[RECT_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_RECT_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_RECT_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_RECT_SET), "[]");
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_RECT_EXIST), "1");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(BATT_TEST_SEQ);

	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT_TEST_SET), pbuf);
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[BATT_TEST_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_BATT_TEST_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_BATT_TEST_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_BATT_TEST_SET), "[]");
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_BATT_TEST_EXIST), "1");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(TEMP_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_TEMP_SET), pbuf);
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[TEMPERATURE_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_TEMPERATURE_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_TEMPERATURE_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_TEMP_SET), "[]");
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_TEMPERATURE_EXIST), "1");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(HYBRID_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HYBRID_SET), pbuf);
	    /*if(gs_pWebDisplayInfo[0].stWebDisplaySet[HYBRID_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_HYBRID_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_HYBRID_EXIST), "1");
	    }*/
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_HYBRID_SET), "[]");
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_HYBRID_EXIST), "1");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(CONVERTER_SET_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONVERTER_SET), pbuf);
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[CONVERTER_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONVERTER_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONVERTER_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CONVERTER_SET), "[]");
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONVERTER_EXIST), "1");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSetDataFile(WIZARD_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_WIZARD), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_WIZARD), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	if(gs_stSetState.iConfig1 == 1)
	{
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[CONFIG1_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG1_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG1_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG1_EXIST), "1");
	}

	if(gs_stSetState.iConfig2 == 1)
	{
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[CONFIG2_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG2_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG2_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG2_EXIST), "1");
	}

	if(gs_stSetState.iConfig3 == 1)
	{
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[CONFIG3_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG3_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG3_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG3_EXIST), "1");
	}

	if(gs_stSetState.iConfig4 == 1)
	{
	    if(gs_pWebDisplayInfo[0].stWebDisplaySet[CONFIG4_FUNCTION].iDisEnable == 1)
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG4_EXIST), "0");
	    }
	    else
	    {
		ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG4_EXIST), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CONFIG4_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[SYSTEM_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SYSTEM_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SYSTEM_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[SET_CABINET1].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CABINET_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CABINET_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[USER_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_USER_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_USER_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[SNMP_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SNMP_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SNMP_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[LANG_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_LANG_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_LANG_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[SW_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SW_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SW_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[ALARM_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_ALARM_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_ALARM_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[DI_ALARM_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_DI_ALARM_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_DI_ALARM_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[REPORT_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_REPORT_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_REPORT_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[GENERATOR_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_GENERATOR_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_GENERATOR_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[SHUNT_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SHUNT_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SHUNT_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[POWER_SPLIT_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_POWER_SPLIT_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_POWER_SPLIT_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[PROTOCOL_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_PROTOCOL_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_PROTOCOL_EXIST), "1");
	}

	if(gs_pWebDisplayInfo[0].stWebDisplaySet[CLEAR_DATA_FUNCTION].iDisEnable == 1)
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CLEAR_DATA_EXIST), "0");
	}
	else
	{
	    ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_CLEAR_DATA_EXIST), "1");
	}
	
	//changed by Frank Wu,13/15/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
	SET_DATA_FILE_REPLACE_BASIC( SOLAR_SET_SEQ, SOLAR_SET_FUNCTION, ID_SOLAR_SET_SET, ID_SOLAR_SET_EXIST);
	SET_DATA_FILE_REPLACE_ADVANCE( SHUNT_SET_SEQ, ID_SHUNT_SET_SET);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[%d,\"%s\",\"%s\"]", SITE_NAME, g_SiteInfo.langSiteName.pFullName[0], g_SiteInfo.langSiteName.pFullName[0]);//here just display English site name
	ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SITENAME), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[%d,\"%s\",\"%s\"]", SITE_LOCATION, g_SiteInfo.langSiteLocation.pFullName[0], g_SiteInfo.langSiteLocation.pFullName[1]);//here just display English site name
	ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SITE_LOCATE), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "[%d,\"%s\",\"%s\"]", SITE_DESCRIPTION, g_SiteInfo.langDescription.pFullName[0], g_SiteInfo.langDescription.pFullName[1]);//here just display English site name
	ReplaceString(&pHtml1, MAKE_VAR_FIELD(ID_SITE_DESC), szBuf);

	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_SETTING_VAR);*/
	if((fp = fopen(WEB_DATA_SETTING_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}

	if((fp1 = fopen(WEB_DATA_WIZARD_VAR,"wb")) != NULL && pHtml1 != NULL)
	{
	    fwrite(pHtml1,strlen(pHtml1), 1, fp1);
	    fclose(fp1);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    if(pHtml1)
    {
	DELETE(pHtml1);
	pHtml1 = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeSettingDataFile\n");
    
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeSMIODataFile
* PURPOSE  :  To make the json file <data.system_smio.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
void Web_MakeSMIODataFile(void)
{
#define SMIO_SIGNAL "SMIO_SIGNAL"


    TRACE_WEB_USER("Begin to exec Web_MakeSMIODataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE		*fp;
    char *pbuf = NULL;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_DC);*/
    iReturn = LoadHtmlFile(WEB_DATA_SMIO, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeSingleOtherEquipDataFile1(SMIO_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMIO_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMIO_SIGNAL), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	if((fp = fopen(WEB_DATA_SMIO_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeSMIODataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeCabinetDataFileBuf
* PURPOSE  :  Make the string in json file of <data.consumption_map.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  the string address
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
char* Web_MakeCabinetDataFileBuf(void)
{
    char *szTemp;
    int i;
    int	iBufLength;
    int iFileLen = 0;
    CABINET_INFO *pstCabInfo;

    memset(gs_szBuffer, 0, BUFFER_LEN);
    szTemp = gs_szBuffer;
    iBufLength = BUFFER_LEN;

    TRACE_WEB_USER("Begin to exec Web_MakeCabinetDataFileBuf\n");
    if(gs_stConsumMapInfo.iNumber > 0)
    {
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	for(i = 0; i < gs_stConsumMapInfo.iNumber; i++)
	{
	    pstCabInfo = &(gs_stConsumMapInfo.pstCabInfo[i]);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%d,%d,\"%s\",%d,\"%.1f A\",\"%.1f W\",\"%.1f KWH\",\"%.1f W\",\"%.1f W\",\"%.1f W\",\"%.1f %\",\"%.1f %\"],", 
		pstCabInfo->iConfig, pstCabInfo->iAlarmValue, pstCabInfo->cName, pstCabInfo->iBranchNum, pstCabInfo->fTotalCurr, pstCabInfo->fTotalPower, pstCabInfo->fTotalEnergy,
		pstCabInfo->fPeakPowLast24H, pstCabInfo->fPeakPowLastWeek, pstCabInfo->fPeakPowLastMonth, pstCabInfo->fAlarmLevel1, pstCabInfo->fAlarmLevel2);
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	TRACE_WEB_USER("End to exec Web_MakeCabinetDataFileBuf\n");
	return szTemp;
    }
    else
    {
	return NULL;
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeCabinetMapDataFile
* PURPOSE  :  Make the string in json file of <data.cabinet_map.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
char* Web_MakeCabinetMapDataFile(void)
{
    char *szTemp;
    int i, j, k;
    int iFileLen = 0;
    int	iBufLength;
    int iValueFormat;
    char szformat[32];
    SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
    SIG_BASIC_VALUE*		pSigValue;
    int iEquipID, iSignalID;
    int iAlarmStatus;//1-normal 2-shreshold level 3-overload level
    float fAlarmlevel1, fAlarmlevel2;
    SIG_BASIC_VALUE*		pSigValue1;
    int	iError = 0;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    BOOL bSignalVaild, bSignalVaild1;

    memset(gs_szBuffer, 0, BUFFER_LEN);
    szTemp = gs_szBuffer;
    iBufLength = BUFFER_LEN;

    TRACE_WEB_USER("Begin to exec Web_MakeCabinetMapDataFile\n");
    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 477);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	ID_SYSTEM,
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue1,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	fAlarmlevel1 = pSigValue1->varValue.fValue;
    }
    else
    {
	fAlarmlevel1 = 100;
    }

    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 478);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	ID_SYSTEM,
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue1,
	iTimeOut);
    if(iError == ERR_DXI_OK)
    {
	fAlarmlevel2 = pSigValue1->varValue.fValue;
    }
    else
    {
	fAlarmlevel2 = 100;
    }
    //TRACE_WEB_USER_NOT_CYCLE("fAlarmlevel1 is %f fAlarmlevel2 is %f\n", fAlarmlevel1, fAlarmlevel2);

    for(i = 0; i < WEB_MAX_CABINET_NUM; i++)//for cabinets
    {
	if(gs_pWebCabinetInfo[i].iNumber != 0)
	{
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "\"Cabinet %d\":{\"name\":\"%s\",", (i + 1), gs_pWebCabinetInfo[i].szName);
	    bSignalVaild1 = 0;
	    for(j = 0; j < gs_pWebCabinetInfo[i].iNumber; j++)//for dus
	    {
		if(gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum != 0)
		{
		    bSignalVaild1 = 1;
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "\"DU%d.%d\":[\"%s\",[", (i + 1), (j + 1), gs_pWebCabinetInfo[i].stWebDU[j].cName[0]);
		    bSignalVaild = 0;
		    for(k = 0; k < gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum; k++)//for signals
		    {
			if(gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].bSignalVaild == TRUE)
			{
			    bSignalVaild = 1;
			    pSigValue = gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].pSigValue;
			    if(pSigValue->varValue.fValue >= gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].fRatingCurrent * fAlarmlevel2 / 100)
			    {
				iAlarmStatus = 3;
			    }
			    else if(pSigValue->varValue.fValue >= gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].fRatingCurrent * fAlarmlevel1 / 100)
			    {
				iAlarmStatus = 2;
			    }
			    else
			    {
				iAlarmStatus = 1;
			    }
			    iEquipID = gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].iEquipID;
			    iSignalID = gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].iSignalID;
			    pSampleSigValue = (SAMPLE_SIG_VALUE *)(pSigValue);
			    iValueFormat = Web_TransferFormatToInt(pSampleSigValue->pStdSig->szValueDisplayFmt);
			    snprintf(szformat, sizeof(szformat), "\"%%.%df A\",%d,%d,\"%.1f A\"],", 
				iValueFormat, iAlarmStatus, gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].bBranchConfig,
				gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].fRatingCurrent);   //"\"%.1f A\",1,0],"
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[%d,%d,\"DU%d.%d.%d\",", iEquipID, iSignalID,
				(i + 1), (j + 1), (k + 1));
			    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szformat, pSigValue->varValue.fValue);
			}
		    }
		    if((iFileLen >= 1) && (bSignalVaild == 1))// if there is at least one signal 
		    {
			iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		    }
		    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]],");
		}
	    }
	    if((iFileLen >= 1) && (bSignalVaild1 == 1))
	    {
		iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	    }
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "},");
	}
    }
    if(iFileLen >= 1)
    {
	iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
    }
    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "");
    TRACE_WEB_USER("End to exec Web_MakeCabinetMapDataFile\n");
    return szTemp;
}

/*==========================================================================*
* FUNCTION :  pushValue
* PURPOSE  :  Push the value to the array, if the number is over, then delete the first value
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  fValue-the value
	      fArray-the array
	      iLength-the array length
* RETURN   :  none
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-14
* MODIFY   :  
*==========================================================================*/
void pushValue(float fValue, WEB_CABINET_POWER_DATA *fArray, int iLength)
{
    int i;

    if(fArray->iSeq < iLength)
    {
	fArray->f30DayPower[fArray->iSeq] = fValue;
	fArray->iSeq++;
    }
    else
    {
	for(i = 0; i < (iLength - 1); i++)
	{
	    fArray->f30DayPower[i] = fArray->f30DayPower[i + 1];
	}
	fArray->f30DayPower[i] = fValue;
    }
}

/*==========================================================================*
* FUNCTION :  Web_CalCabinetData
* PURPOSE  :  Calculate the cabinet data
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
void Web_CalCabinetData(void)
{
    int i, j;
    CABINET_INFO *pstCabInfo;
    int iCabinetID, iDUID, iBranchID;
    float fBranchCurr, fCBRatingCurr;
    float fCabinetCBRating;
    struct timeval t_start;
    long cost_time = 0;
    static long start = 0, end;
    float sfEnergy = 0;
    static time_t toTime, toTimeLast = 0, toTimeInterval;
    static float fPeakPowLast24H = 0, fPeakPowLastWeek = 0, fPeakPowLastMonth = 0;
    int k;//for debug

    TRACE_WEB_USER("Begin to exec Web_CalCabinetData\n");

    if(toTimeLast == 0)
    {
	toTimeLast = time((time_t *)0);
    }
    toTime = time((time_t *)0);
    toTimeInterval = toTime - toTimeLast;

    gettimeofday(&t_start, NULL);
    if(start == 0)
    {
	start = ((long)t_start.tv_sec) * 1000 + (long)t_start.tv_usec / 1000;
    }
    end = ((long)t_start.tv_sec) * 1000 + (long)t_start.tv_usec / 1000;
    cost_time = end - start;
    start = end;
    //TRACE_WEB_USER_NOT_CYCLE("cost_time is %ld\n", cost_time);

    for(i = 0; i < gs_stConsumMapInfo.iNumber; i++)
    {
	pstCabInfo = &(gs_stConsumMapInfo.pstCabInfo[i]);
	if(pstCabInfo->iConfig == TRUE)
	{
	    pstCabInfo->fTotalCurr = 0;// calculate the total current
	    fCabinetCBRating = 0;
	    for(j = 0; j < 20; j++)
	    {
		if(pstCabInfo->stBranch[j].bValid == TRUE)
		{
		    TRACE_WEB_USER("i=%d,j=%d\n", i, j);
		    iCabinetID = pstCabInfo->stBranch[j].iCabinetID;
		    TRACE_WEB_USER("iCabinetID is %d\n", iCabinetID);
		    iDUID = pstCabInfo->stBranch[j].iDUID;
		    TRACE_WEB_USER("iDUID is %d\n", iDUID);
		    iBranchID = pstCabInfo->stBranch[j].iBranchID;
		    TRACE_WEB_USER("iBranchID is %d\n", iBranchID);
		    fBranchCurr = gs_pWebCabinetInfo[iCabinetID - 1].stWebDU[iDUID - 1].pSignalInfo[iBranchID - 1].pSigValue->varValue.fValue;
		    TRACE_WEB_USER("fBranchCurr is %f\n", fBranchCurr);
		    fCBRatingCurr = gs_pWebCabinetInfo[iCabinetID - 1].stWebDU[iDUID - 1].pSignalInfo[iBranchID - 1].fRatingCurrent;
		    TRACE_WEB_USER("fCBRatingCurr is %f\n", fCBRatingCurr);
		    pstCabInfo->fTotalCurr += fBranchCurr;
		    fCabinetCBRating += fCBRatingCurr;
		    TRACE_WEB_USER("fTotalCurr is %f, fCabinetCBRating is %f\n", pstCabInfo->fTotalCurr, fCabinetCBRating);
		}
	    }
	    TRACE_WEB_USER("fCabinetCBRating is %f\n", fCabinetCBRating);
	    pstCabInfo->fTotalPower = pstCabInfo->fTotalCurr * pstSysVolt->pSigValue->varValue.fValue;// calculate the total power
	    //here record the power value every hour
	    if(toTimeInterval > ONE_HOUR_SECOND)
	    {
		TRACE_WEB_USER_NOT_CYCLE("Now record a value to buffer\n");
		pushValue(pstCabInfo->fTotalPower, &(gs_pWebCabPowerData[i]), DAY30_HOUR);
		//for debug begin
		for(k = 0; k < 100; k++)
		{
		    TRACE_WEB_USER_NOT_CYCLE("%f    ", gs_pWebCabPowerData[i].f30DayPower[k]);
		}
		TRACE_WEB_USER_NOT_CYCLE("\n");
		//for debug end
		toTimeLast = toTime;
	    }
	    else if(toTimeInterval < 0)
	    {
		TRACE_WEB_USER_NOT_CYCLE("The time has been modified\n");
		memset(&(gs_pWebCabPowerData[i]), 0, sizeof(WEB_CABINET_POWER_DATA));
		toTimeLast = toTime;
	    }
	    sfEnergy = (cost_time * pstCabInfo->fTotalPower / 1000) / (1000 * 3600);
	    //TRACE_WEB_USER_NOT_CYCLE("sfEnergy is %f\n", sfEnergy);
	    pstCabInfo->fTotalEnergy += sfEnergy;
	    if(FLOAT_EQUAL0(pstCabInfo->fTotalCurr))
	    {
		pstCabInfo->iAlarmValue = NONE_CURRENT;
	    }
	    else if(pstCabInfo->fTotalCurr >= pstCabInfo->fAlarmLevel2 * fCabinetCBRating / 100)
	    {
		pstCabInfo->iAlarmValue = ALARM_LEVEL2;
	    }
	    else if(pstCabInfo->fTotalCurr >= pstCabInfo->fAlarmLevel1 * fCabinetCBRating / 100)
	    {
		pstCabInfo->iAlarmValue = ALARM_LEVEL1;
	    }
	    else
	    {
		pstCabInfo->iAlarmValue = NORMAL;
	    }
	}
    }

    TRACE_WEB_USER("End to exec Web_CalCabinetData\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeCabinetDataFile
* PURPOSE  :  Make the json file of <data.consumption_map.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-13
* MODIFY   :  
*==========================================================================*/
void Web_MakeCabinetDataFile(void)
{
#define ID_X_NUM	"ID_X_NUM"
#define ID_Y_NUM	"ID_Y_NUM"
#define ID_CABIENT_DATA "ID_CABIENT_DATA"


    TRACE_WEB_USER("Begin to exec Web_MakeCabinetDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE		*fp;
    char *pbuf = NULL;
    char szBuf[64];

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_DC);*/
    iReturn = LoadHtmlFile(WEB_DATA_CONSUMPTION, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeCabinetDataFileBuf();
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CABIENT_DATA), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CABIENT_DATA), "[]");
	}
	
	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_stConsumMapInfo.ix_Axis);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_X_NUM), szBuf);

	memset(szBuf, 0, sizeof(szBuf));
	snprintf(szBuf, sizeof(szBuf), "%d", gs_stConsumMapInfo.iy_Axis);
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_Y_NUM), szBuf);

	if((fp = fopen(WEB_DATA_CONSUMPTION_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeCapMapDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeCapMapDataFile
* PURPOSE  :  To make the json file <data.cabinet_map.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-04-08
*==========================================================================*/
void Web_MakeCapMapDataFile(void)
{
#define ID_CABIENT_DATA "ID_CABIENT_DATA"


    TRACE_WEB_USER("Begin to exec Web_MakeCapMapDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE		*fp;
    char *pbuf = NULL;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_DC);*/
    iReturn = LoadHtmlFile(WEB_DATA_CABINET, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeCabinetMapDataFile();
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CABIENT_DATA), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_CABIENT_DATA), "[]");
	}
	/*if(pbuf)
	{
	DELETE(pbuf);
	pbuf = NULL;
	}*/

	if((fp = fopen(WEB_DATA_CABINET_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeCapMapDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeDGDataFile
* PURPOSE  :  To make the json file <data.system_dg.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
void Web_MakeDGDataFile(void)
{
#define DG_SIGNAL "DG_SIGNAL"


    TRACE_WEB_USER("Begin to exec Web_MakeDGDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE		*fp;
    char *pbuf = NULL;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_DC);*/
    iReturn = LoadHtmlFile(WEB_DATA_DG, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeSingleOtherEquipDataFile(DG_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DG_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DG_SIGNAL), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	if((fp = fopen(WEB_DATA_DG_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeDCDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeACDataFile
* PURPOSE  :  To make the json file <data.system_ac.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
void Web_MakeACDataFile(void)
{
#define SMAC_SIGNAL "SMAC_SIGNAL"
#define ACMETER_SIGNAL "ACMETER_SIGNAL"
#define ACUNIT_SIGNAL "ACUNIT_SIGNAL"

#define SMAC_CONFIG	"SMAC_CONFIG"
#define ACMETER_CONFIG	"ACMETER_CONFIG"
#define ACUNIT_CONFIG	"ACUNIT_CONFIG"


    TRACE_WEB_USER("Begin to exec Web_MakeACDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE		*fp;
    char *pbuf = NULL;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_DC);*/
    iReturn = LoadHtmlFile(WEB_DATA_AC, &pHtml);
    if(iReturn > 0)
    {
	pbuf = Web_MakeSingleOtherEquipDataFile(SMAC_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMAC_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMAC_SIGNAL), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleOtherEquipDataFile(ACMETER_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ACMETER_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ACMETER_SIGNAL), "[]");
	}
	
	pbuf = Web_MakeSingleOtherEquipDataFile(ACUNIT_SEQ);
	if(pbuf != NULL)
	{
		 ReplaceString(&pHtml, MAKE_VAR_FIELD(ACUNIT_SIGNAL), pbuf);
	}
	else											     
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ACUNIT_SIGNAL), "[]");
	}
	
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[ACMETER].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ACMETER_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ACMETER_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[SMAC].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMAC_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMAC_CONFIG), "1");
	}
	if(     gs_pWebDisplaySamInfo[0].stWebDisplaySet[AC_UNIT].iDisEnable == 1)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ACUNIT_CONFIG), "0");	
	}
	else
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(ACUNIT_CONFIG), "1");		
	}

	if((fp = fopen(WEB_DATA_AC_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeDCDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeLVD_FUSEDataFile
* PURPOSE  :  To make the json file <data.system_lvd_fuse.html>
* CALLS    :
* CALLED BY:
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-29
*==========================================================================*/
void Web_MakeLVD_FUSEDataFile(void)
{
#define BATT_FUSE_GROUP_SIGNAL   "BATT_FUSE_GROUP_SIGNAL"
#define BATT_FUSE_SIGNAL	"BATT_FUSE_SIGNAL"
#define LVD_GROUP_SIGNAL	 "LVD_GROUP_SIGNAL"
#define LVD_UNIT_SIGNAL		"LVD_UNIT_SIGNAL"
#define LVD3_UNIT_SIGNAL		"LVD3_UNIT_SIGNAL"
#define SMDU_BATT_FUSE_SIGNAL	"SMDU_BATT_FUSE_SIGNAL"
#define SMDU_LVD_SIGNAL		"SMDU_LVD_SIGNAL"

	TRACE_WEB_USER("Begin to exec Web_MakeLVD_FUSEDataFile\n");

	//char *szFile = NULL;
	char *pHtml = NULL;
	int	 iReturn = 0;
	FILE		*fp;
	char *pbuf = NULL;

	/*szFile = NEW(char, 128);
	if(szFile == NULL)
	{
	AppLogOut("Web_MakeDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
	return;
	}
	sprintf(szFile, "%s", WEB_DATA_DC);*/
	iReturn = LoadHtmlFile(WEB_DATA_LVD_FUSE, &pHtml);
	if(iReturn > 0)
	{
	    pbuf = Web_MakeSingleOtherEquipDataFile(BATTERY_FUSE_GROUP_SEQ);
	    if(pbuf != NULL)
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(BATT_FUSE_GROUP_SIGNAL), pbuf);
	    }
	    else
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(BATT_FUSE_GROUP_SIGNAL), "[]");
	    }
	    /*if(pbuf)
	    {
	    DELETE(pbuf);
	    pbuf = NULL;
	    }*/

	    pbuf = Web_MakeSingleOtherEquipDataFile(BATTERY_FUSE_SEQ);
	    if(pbuf != NULL)
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(BATT_FUSE_SIGNAL), pbuf);
	    }
	    else
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(BATT_FUSE_SIGNAL), "[]");
	    }
	    /*if(pbuf)
	    {
	    DELETE(pbuf);
	    pbuf = NULL;
	    }*/

	    pbuf = Web_MakeSingleOtherEquipDataFile(LVD_GROUP_SEQ1);
	    if(pbuf != NULL)
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(LVD_GROUP_SIGNAL), pbuf);
	    }
	    else
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(LVD_GROUP_SIGNAL), "[]");
	    }

	    pbuf = Web_MakeSingleOtherEquipDataFile(LVD_SEQ1);
	    if(pbuf != NULL)
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(LVD_UNIT_SIGNAL), pbuf);
	    }
	    else
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(LVD_UNIT_SIGNAL), "[]");
	    }

	    pbuf = Web_MakeSingleOtherEquipDataFile(LVD_SEQ2);
	    if(pbuf != NULL)
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(LVD3_UNIT_SIGNAL), pbuf);
	    }
	    else
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(LVD3_UNIT_SIGNAL), "[]");
	    }

	    pbuf = Web_MakeSingleOtherEquipDataFile(SMDU_BATT_FUSE_SEQ);
	    if(pbuf != NULL)
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDU_BATT_FUSE_SIGNAL), pbuf);
	    }
	    else
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDU_BATT_FUSE_SIGNAL), "[]");
	    }

	    pbuf = Web_MakeSingleOtherEquipDataFile(SMDU_LVD_SEQ);
	    if(pbuf != NULL)
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDU_LVD_SIGNAL), pbuf);
	    }
	    else
	    {
		ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDU_LVD_SIGNAL), "[]");
	    }
	    
	    if((fp = fopen(WEB_DATA_LVD_FUSE_VAR,"wb")) != NULL && pHtml != NULL)
	    {
		fwrite(pHtml,strlen(pHtml), 1, fp);
		fclose(fp);
	    }
	    else
	    {}
	}
	else
	{}

	/*if(szFile)
	{
	DELETE(szFile);
	szFile = NULL;
	}*/
	if(pHtml)
	{
	    DELETE(pHtml);
	    pHtml = NULL;
	}
	TRACE_WEB_USER("end to exec Web_MakeLVD_FUSEDataFile\n");
	return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeDCDataFile
* PURPOSE  :  To make the json file <data.system_dc.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeDCDataFile(void)
{
#define DC_SIGNAL   "DC_SIGNAL"
#define SMDU_SIGNAL "SMDU_SIGNAL"
#define SMDUP_SIGNAL "SMDUP_SIGNAL"
#define SMDUP1_SIGNAL	"SMDUP1_SIGNAL"
#define SMDUH_SIGNAL	"SMDUH_SIGNAL"
#define EIB_SIGNAL	"EIB_SIGNAL"
#define DCMETER_SIGNAL	"DCMETER_SIGNAL"
#define NARADA_BMS_SIGNAL	"NARADA_BMS_SIGNAL"
#define SMDUE_SIGNAL "SMDUE_SIGNAL"

#define DC_CONFIG	"DC_CONFIG"
#define SMDU_CONFIG	"SMDU_CONFIG"
#define SMDUP_CONFIG	"SMDUP_CONFIG"
#define SMDUP1_CONFIG	"SMDUP1_CONFIG"
#define SMDUH_CONFIG	"SMDUH_CONFIG"
#define EIB_CONFIG	"EIB_CONFIG"
#define DCMETER_CONFIG	"DCMETER_CONFIG"
#define NARADA_BMS_CONFIG	"NARADA_BMS_CONFIG"
#define CABINETMAP_CONFIG	"CABINETMAP_CONFIG"
#define SMDUE_CONFIG	"SMDUE_CONFIG"

#define CUSTOMINPUTSTATUS_SIGNAL	"CUSTOMINPUTSTATUS_SIGNAL"

    TRACE_WEB_USER("Begin to exec Web_MakeDCDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE		*fp;
    char *pbuf = NULL;
	int iSmdueTabDisplay=0;
	int iTest = 0;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeDCDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_DC);*/
    iReturn = LoadHtmlFile(WEB_DATA_DC, &pHtml);
    if(iReturn > 0)
    {
	//pbuf = Web_MakeSingleOtherEquipDataFile(SMDU_SEQ);
	pbuf = Web_MakeSingleOtherEquipDataFile1(SMDU_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDU_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDU_SIGNAL), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleOtherEquipDataFile1(SMDUP_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUP_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUP_SIGNAL), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleDCDataFile(SMDUP1_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUP1_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUP1_SIGNAL), "[]");
	}

	pbuf = Web_MakeSingleOtherEquipDataFile1(SMDUH_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUH_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUH_SIGNAL), "[]");
	}
	
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleSMDUEDataFile(SMDUE_SEQ,&iSmdueTabDisplay);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUE_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUE_SIGNAL), "[]");
	}

	pbuf = Web_MakeSingleOtherEquipDataFile1(EIB_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(EIB_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(EIB_SIGNAL), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleOtherEquipDataFile(DCMETER_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DCMETER_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DCMETER_SIGNAL), "[]");
	}

	pbuf = Web_MakeSingleOtherEquipDataFile(NARADA_BMS_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(NARADA_BMS_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(NARADA_BMS_SIGNAL), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleOtherEquipDataFile(DC_BOARD_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DC_SIGNAL), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DC_SIGNAL), "[]");
	}

	pbuf = Web_MakeSingleCostom_inputs_status(SMDUE_SEQ);
	if(pbuf != NULL)
	{
	   iTest = ReplaceString(&pHtml, MAKE_VAR_FIELD(CUSTOMINPUTSTATUS_SIGNAL), pbuf);
	   if( -1 == iTest )
	   {
			printf("Web_MakeDCDataFile(): ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~1\n");
	   }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(CUSTOMINPUTSTATUS_SIGNAL), "[]");
	}

	
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	//ReplaceString(&pHtml, MAKE_VAR_FIELD(DC_SIGNAL), "[]");
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUH_CURRENT), "[]");

	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_DC_VAR);*/
	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[DC].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DC_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DC_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[SMDU].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDU_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDU_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[SMDUP].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUP_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUP_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[SMDUP1].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUP1_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUP1_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[SMDUH].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUH_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUH_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[SMDUE].iDisEnable == 1)
	{
		if( 0 == iSmdueTabDisplay )
		{
	    	ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUE_CONFIG), "0");
	    }
	    else
	    {
		    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUE_CONFIG), "1");
	    }
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(SMDUE_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[EIB].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(EIB_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(EIB_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[DCMETER].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DCMETER_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(DCMETER_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[NARADA_BMS].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(NARADA_BMS_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(NARADA_BMS_CONFIG), "1");
	}

	if(gs_pWebDisplaySamInfo[0].stWebDisplaySet[CABINET_MAP].iDisEnable == 1)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(CABINETMAP_CONFIG), "0");
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(CABINETMAP_CONFIG), "1");
	}

	if((fp = fopen(WEB_DATA_DC_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeDCDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_MakeBattDataFile
* PURPOSE  :  To make the json file <data.system_battery.html>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng               DATE: 2015-07-20
*==========================================================================*/
void Web_MakeBattDataFile(void)
{
#define ID_COM_BATT_CONTENT "ID_COM_BATT_CONTENT"
#define ID_EIB_BATT_CONTENT "ID_EIB_BATT_CONTENT"
#define ID_SMDU_BATT_CONTENT "ID_SMDU_BATT_CONTENT"
#define ID_SM_BATT_CONTENT "ID_SM_BATT_CONTENT"
#define ID_LARGEDU_BATT_CONTENT "ID_LARGEDU_BATT_CONTENT"
#define ID_SMBRC_BATT_CONTENT "ID_SMBRC_BATT_CONTENT"
#define ID_SONICK_BATT_CONTENT	"ID_SONICK_BATT_CONTENT"
#define ID_SMDUE_BATT_CONTENT "ID_SMDUE_BATT_CONTENT"
#define ID_GROUP_SIGNAL	"ID_GROUP_SIGNAL"

    TRACE_WEB_USER("Begin to exec Web_MakeBattDataFile\n");

    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE		*fp;
    int i;
    char	*szMakeVar = NULL;
    char *pbuf = NULL;
    char *szTemp = NULL;
    int iFileLen = 0;
    char szBuf[128];
    int len1, len2;
    int iGroupNum;
    int		iBufLength;

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeBattDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_BATT);*/
    iReturn = LoadHtmlFile(WEB_DATA_BATT, &pHtml);
    if(iReturn > 0)
    {// the fix signals number is 5
	for(i = 0; i < 5; i++)
	{
	    szMakeVar = MakeVarField(gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i].szResourceID);
	    if(szMakeVar != NULL)
	    {
		//TRACE_WEB_USER("szResourceID is %s\n", gs_pWebSigInfo[0].stWebPrivate[i].szResourceID);
		//TRACE_WEB_USER("[Web_MakeBattDataFile] szMakeVar is %s\n", szMakeVar);
		Web_FillString(&pHtml, szMakeVar, gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i].pSigValue, gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i].iEquipID,
		    gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i].bValid);
		DELETE(szMakeVar);
		szMakeVar = NULL;
	    }
	}
	iGroupNum = gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].iNumber - 5;
	if(iGroupNum > 6)
	{
	    iGroupNum = 6;
	}
	//szTemp = NEW(char, (80 * (iGroupNum) + LENGTH_OF_OTHER_CHAR));
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTemp = gs_szBuffer;
	/*if(szTemp == NULL)
	{
	    AppLogOut("Web_MakeBattDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    if(pHtml)
	    {
		DELETE(pHtml);
		pHtml = NULL;
	    }
	    return;
	}*/
	iBufLength = 80 * (iGroupNum) + LENGTH_OF_OTHER_CHAR;
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	for(i = 0; i < (iGroupNum); i++)
	{
	    len1 = 0;
	    len2 = 0;
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "[");
	    memset(szBuf, 0, sizeof(szBuf));
	    Web_GetSignalName(&(gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i + 5]), szBuf, sizeof(szBuf));
	    len1 = strlen((const char*)szBuf);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	    memset(szBuf, 0, sizeof(szBuf));
	    Web_GetSignalValue(gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i + 5].pSigValue, gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i + 5].bValid,
			gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i + 5].iSignalID, COMM_BATT_GROUP_SEQ, 0, szBuf, sizeof(szBuf),gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate[i + 5].iEquipID);
	    len2 = strlen((const char*)szBuf);
	    //TRACE_WEB_USER("iSigValueType is %d\n", gs_pWebRectSigInfo[itype - 1]->stWebPrivate[k].iSigValueType);
	    //TRACE_WEB_USER("[Web_MakeRectDataFile] szBuf is %s\n", szBuf);
	    iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), szBuf);
	    if((len1 > 0) && (len2 > 0))
	    {
		if(iFileLen >= 1)
		{
		    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
		}
		iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "],");
	    }
	    else
	    {
		iFileLen = iFileLen - 1;
	    }
	}
	if(iFileLen >= 1)
	{
	    iFileLen = iFileLen - 1;	// get rid of the last "," to meet json format
	}
	iFileLen += snprintf(szTemp + iFileLen, (iBufLength - iFileLen), "]");
	ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_GROUP_SIGNAL), szTemp);
	//TRACE_WEB_USER("[Web_MakeRectDataFile] szTemp is %s\n", szTemp);
	/*DELETE(szTemp);
	szTemp = NULL;*/

	pbuf = Web_MakeSingleBattDataFile(COMM_BATT_SINGLE_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_COM_BATT_CONTENT), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_COM_BATT_CONTENT), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleBattDataFile(EIB_BATT_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_EIB_BATT_CONTENT), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_EIB_BATT_CONTENT), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleBattDataFile(SMDU_BATT_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDU_BATT_CONTENT), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDU_BATT_CONTENT), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	
	pbuf = Web_MakeSingleBattDataFile(SMDUE_BATT_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_BATT_CONTENT), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMDUE_BATT_CONTENT), "[]");
	}

	pbuf = Web_MakeSingleBattDataFile(SM_BATT_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SM_BATT_CONTENT), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SM_BATT_CONTENT), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	//pbuf = Web_MakeSingleBattDataFile(LARGEDU_BATT_SEQ);
	/*if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LARGEDU_BATT_CONTENT), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_LARGEDU_BATT_CONTENT), "[]");
	}*/
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/

	pbuf = Web_MakeSingleBattDataFile(SMBRC_BATT_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMBRC_BATT_CONTENT), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SMBRC_BATT_CONTENT), "[]");
	}

	pbuf = Web_MakeSingleBattDataFile(SONICK_BATT_SEQ);
	if(pbuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SONICK_BATT_CONTENT), pbuf);
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_SONICK_BATT_CONTENT), "[]");
	}
	/*if(pbuf)
	{
	    DELETE(pbuf);
	    pbuf = NULL;
	}*/


	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_BATT_VAR);*/
	if((fp = fopen(WEB_DATA_BATT_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeBattDataFile\n");
    return;
}

/*==========================================================================*
* FUNCTION :  ParsedWebSettingFileDisTableProc
* PURPOSE  :  Parse display enable signal in <cfg_ui_function.cfg>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  szBuf: the string in one line describe one setting signal
pStructData:the struct describe one setting signal
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-03-20
*==========================================================================*/
static int ParsedWebSettingFileDisTableProc(IN char *szBuf, 
					 OUT WEB_GENERAL_DISPLAY_INFO *pStructData)
{
    char *pField = NULL;
    //int		itemp = 0;
    /* used as buffer */

    ASSERT(szBuf);
    ASSERT(pStructData);

    //TRACE_WEB_USER("[ParsedWebSettingFileTableProc] szBuf is %s\n", szBuf);
    /*0.Jump sequence ID*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if(pField != NULL)
    {
    }

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if(pField != NULL)
    {
    }

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 2;    
    }
    pStructData->iDisEnable = atoi(pField);

    return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  ParsedWebDUTableProc
* PURPOSE  :  Parse every signal in <cfg_ui_function.cfg>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  szBuf: the string in one line describe one setting signal
pStructData:the struct describe one setting signal
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
static int ParsedWebDUTableProc(IN char *szBuf, 
					 OUT WEB_SIGNAL_INFO *pStructData)
{
    //TRACE_WEB_USER_NOT_CYCLE("Now enter the ParsedWebDUTableProc function\n");
    char *pField = NULL;
    //int		itemp = 0;
    /* used as buffer */

    ASSERT(szBuf);
    ASSERT(pStructData);

    //TRACE_WEB_USER("[ParsedWebSettingFileTableProc] szBuf is %s\n", szBuf);
    /*0.Jump sequence ID*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if(pField != NULL)
    {
    }

    /*1.Jump Equip Name*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if(pField != NULL)
    {
    }

    //2 Equip ID
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 2;    
    }
    pStructData->iEquipID = atoi(pField);
    //TRACE_WEB_USER_NOT_CYCLE("pStructData->iEquipID is %d\n", pStructData->iEquipID);

    //3. Signal ID
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 2;    
    }
    pStructData->iSignalID = atoi(pField);
    //TRACE_WEB_USER_NOT_CYCLE("pStructData->iSignalID is %d\n", pStructData->iSignalID);

    return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  ParsedWebSettingFileTableProc
* PURPOSE  :  Parse every setting signal in <cfg_ui_function.cfg>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  szBuf: the string in one line describe one setting signal
pStructData:the struct describe one setting signal
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
static int ParsedWebSettingFileTableProc(IN char *szBuf, 
					 OUT WEB_GENERAL_SETTING_INFO *pStructData)
{
    char *pField = NULL;
    //int		itemp = 0;
    /* used as buffer */

    ASSERT(szBuf);
    ASSERT(pStructData);

    //TRACE_WEB_USER("[ParsedWebSettingFileTableProc] szBuf is %s\n", szBuf);
    /*0.Jump sequence ID*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if(pField != NULL)
    {
    }

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if(pField != NULL)
    {
    }

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 2;    
    }
    pStructData->iEquipID = atoi(pField);

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 4;    
    }
    pStructData->iSignalType = atoi(pField);

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 4;    
    }
    pStructData->iSignalID = atoi(pField);

    return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  ParsedWebSignalFileTableProc
* PURPOSE  :  Parse every sampling signal in <cfg_ui_sampling.cfg>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  szBuf: the string in one line describe one signal
pStructData:the struct describe one signal
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
static int ParsedWebSignalFileTableProc(IN char *szBuf, 
					OUT WEB_GENERAL_SIGNAL_INFO *pStructData)
{
    char *pField = NULL;
    //int		itemp = 0;
    /* used as buffer */

    ASSERT(szBuf);
    ASSERT(pStructData);

    //TRACE_WEB_USER("[ParsedWebSignalFileTableProc] szBuf is %s\n", szBuf);
    /*0.Jump sequence ID*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if(pField != NULL)
    {
    }

    /* 1.RES ID field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 1;    
    }

    strncpyz(pStructData->szResourceID, pField, sizeof(pStructData->szResourceID));
    //TRACE_WEB_USER("szResourceID is %s\n", pStructData->szResourceID);

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 2;    
    }
    pStructData->iEquipID = atoi(pField);

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 3;    
    }
    pStructData->iSignalType = atoi(pField);

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
	return 4;    
    }
    pStructData->iSignalID = atoi(pField);

    return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  LoadWebCabinetProc
* PURPOSE  :  Read the file <cfg_consumption_map.cfg>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  iNumber:
* pCfg:the struct describe the file of <cfg_ui_function.cfg>
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-04-08
*==========================================================================*/
static int LoadWebCabinetProc(IN int iNumber, IN void *pCfg, int iSeq)
{
    int		j, i = 0;
    CONFIG_TABLE_LOADER loader[WEB_MAX_DU_NUM];
    WEB_GENERAL_DU_INFO *s_aWebDUInfo = NEW(WEB_GENERAL_DU_INFO, WEB_MAX_DU_NUM);
    int iTemp, iRst;
    char szKeyString[32], szDUName[32];
    //WEB_GENERAL_DU_INFO	s_aWebDUInfo[WEB_MAX_DU_NUM];
    //static WEB_CABINET_INFO	s_aWebCabinetInfo[WEB_MAX_CABINET_NUM];

    gs_pWebCabinetInfo[iSeq].iNumber = iNumber;//DU number
    //snprintf(gs_pWebCabinetInfo[iSeq].szName, 64, "Cabinet %d", (iSeq + 1));

    gs_pWebCabinetInfo[iSeq].stWebDU = s_aWebDUInfo;

    for(i = 0; i < iNumber; i++)
    {
	iTemp = 0;
	memset(szKeyString, 0, sizeof(szKeyString));
	iTemp = snprintf(szKeyString, 32, szTemplateDU1[iSeq * 6 + i]);
	snprintf(szKeyString + iTemp, 32, "_NAME]");
	TRACE_WEB_USER_NOT_CYCLE("%s\n", szKeyString);
	iRst = Cfg_ProfileGetLine(pCfg, szKeyString, szDUName, 32);
	TRACE_WEB_USER_NOT_CYCLE("Cabinet[%d] DU[%d] name is %s\n", iSeq, i, szDUName);
	snprintf(gs_pWebCabinetInfo[iSeq].stWebDU[i].cName[0], 64, szDUName);

	DEF_LOADER_ITEM(&loader[i],
	    NULL,
	    &(gs_pWebCabinetInfo[iSeq].stWebDU[i].iSignalNum),
	    szTemplateDU[iSeq * 6 + i],
	    &(gs_pWebCabinetInfo[iSeq].stWebDU[i].pSignalInfo),
	    ParsedWebDUTableProc);
	TRACE_WEB_USER_NOT_CYCLE("%s\n", szTemplateDU[iSeq * 6 + i]);
    }
    /*for(i = 0; i < iNumber; i++)
    {
	TRACE_WEB_USER_NOT_CYCLE("the %d string is %s\n", i, loader[i].szDataKey);
    }*/

    //TRACE_WEB_USER_NOT_CYCLE("Now parse the string\n");
    if(Cfg_LoadTables(pCfg, iNumber, loader) != ERR_CFG_OK)
    {
	return ERR_LCD_FAILURE;
    }

    //debug begin
    for(i = 0; i < iNumber; i++)
    {
	for(j = 0; j < gs_pWebCabinetInfo[iSeq].stWebDU[i].iSignalNum; j++)
	{
	    TRACE_WEB_USER_NOT_CYCLE("The Cabinet[%d] DU[%d]'s %d signal ID is %d\n", iSeq + 1, i + 1, j + 1, gs_pWebCabinetInfo[iSeq].stWebDU[i].pSignalInfo[j].iSignalID);
	}
    }
    //debug end

    return ERR_CFG_OK;
}

//changed by Frank Wu,33/34,20140513, for adding new web pages to the tab of "power system"
/*==========================================================================*
 * FUNCTION :  Web_GetControlSignalByEquipID
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT CTRL_SIG_VALUE **ppSigValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetControlSignalByEquipID(IN int iEquipID, 
										 OUT CTRL_SIG_VALUE **ppSigValue)
{
		CTRL_SIG_VALUE	*pCtrlSigInfo = NULL;
		int				iSignalNum = 0;
		int				iVarSubID = 0;
		int				iBufLen = 0;
		int				iTimeOut = 0;
		int				iError = DxiGetData(VAR_CON_SIG_VALUE_OF_EQUIP,
											iEquipID,			
											iVarSubID,		
											&iBufLen,			
											(void*)&pCtrlSigInfo,			
											iTimeOut);

		iSignalNum = iBufLen /sizeof(CTRL_SIG_VALUE);
		
		if(iError == ERR_DXI_OK)
		{
			*ppSigValue = pCtrlSigInfo;
			return iSignalNum;
		}
		else
		{
			return FALSE;
		}
 
}



/*==========================================================================*
 * FUNCTION :  Web_GetSettingSignalByEquipID
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT SET_SIG_VALUE **ppSigValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetSettingSignalByEquipID(IN int iEquipID, 
										 OUT SET_SIG_VALUE **ppSigValue)
{
	SET_SIG_VALUE *pSetSigValue = NULL;

	int iError = 0;

	int	iSignalNum = 0;
	int iVarSubID = 0;
	int iBufLen = 0;

	iError = DxiGetData(VAR_SET_SIG_VALUE_OF_EQUIP,
						iEquipID,			
						iVarSubID,		
						&iBufLen,			
						(void *)&pSetSigValue,			
						0);

	iSignalNum = iBufLen / sizeof(SET_SIG_VALUE);
	////TRACE("Web_GetSettingSignalByEquipID:iSignalNum[%d]\n", iSignalNum);
	if (iError == ERR_DXI_OK)
	{
		*ppSigValue = pSetSigValue;
		return iSignalNum;
	}
	else
	{
		return FALSE;
	}
}

static void LoadPowerSystemConfigTables(int iSeqNum, int *piRowNum, WEB_GENERAL_SETTING_INFO **ppstWebPrivateSet)
{
	int iaEquipNumMapTable[][2] = {
		{ SINGLE_RECT_SEQ, RECT_EQUIP_START},
		//changed by Frank Wu,31/22/31,20140527, for add single converter and single solar settings pages
		{ SINGLE_CONVERTER_SEQ, SINGLE_CONVERTER_START_1},
		{ SINGLE_SOLAR_SEQ, SINGLE_SOLAR_START_1},
		{ SINGLE_RECT_S1_SEQ, SINGLE_RECT_S1_START_1},
		{ SINGLE_RECT_S2_SEQ, SINGLE_RECT_S2_START_1},
		{ SINGLE_RECT_S3_SEQ, SINGLE_RECT_S3_START_1},
		{ SYSTEM_GROUP_SEQ, SYSTEM_GROUP_EQUIP_START},
		{ LVD_GROUP_SEQ, LVD_GROUP_START},
		{ AC_GROUP_SEQ, AC_GROUP_EQUIP_START},
		{ AC_UNIT_SEQ, AC_UNIT_EQUIP_START},
		{ ACMETER_GROUP_SEQ, ACMETER_GROUP_EQUIP_START},
		{ ACMETER_UNIT_SEQ, ACMETER_UNIT_EQUIP_START},
		{ DC_UNIT_SEQ, DC_UNIT_EQUIP_START},
		{ DCMETER_GROUP_SEQ, DCMETER_GROUP_EQUIP_START},
		{ DCMETER_UNIT_SEQ, DCMETER_UNIT_EQUIP_START},
		{ DIESEL_GROUP_SEQ, DIESEL_GROUP_EQUIP_START},
		{ DIESEL_UNIT_SEQ, DIESEL_UNIT_EQUIP_START},
		{ FUEL_GROUP_SEQ, FUEL_GROUP_EQUIP_START},
		{ FUEL_UNIT_SEQ, FUEL_UNIT_EQUIP_START},
		{ IB_GROUP_SEQ, IB_GROUP_EQUIP_START},
		{ IB_UNIT_SEQ, IB_UNIT_EQUIP_START},
		{ EIB_GROUP_SEQ, EIB_GROUP_EQUIP_START},
		{ SINGLE_EIB_SEQ, EIB_EQUIP_START},
		{ OBAC_UNIT_SEQ, OBAC_UNIT_EQUIP_START},
		{ OBLVD_UNIT_SEQ, OBLVD_UNIT_EQUIP_START},
		{ OBFUEL_UNIT_SEQ, OBFUEL_UNIT_EQUIP_START},
		{ SMDU_GROUP_SEQ, SMDU_GROUP_EQUIP_START},
		{ SINGLE_SMDU_SEQ, SMDU_EQUIP_START},
		{ SMDUP_GROUP_SEQ, SMDUP_GROUP_EQUIP_START},
		{ SMDUP_UNIT_SEQ, SMDUP_UNIT_EQUIP_START},
		{ SMDUE_GROUP_SEQ, SMDUE_GROUP_EQUIP_START},
		{ SMDUE_UNIT_SEQ, SMDUE_UNIT_EQUIP_START},
		{ SMDUH_GROUP_SEQ, SMDUH_GROUP_EQUIP_START},
		{ SMDUH_UNIT_SEQ, SMDUH_UNIT_EQUIP_START},
		{ SMBRC_GROUP_SEQ, SMBRC_GROUP_EQUIP_START},
		{ SMBRC_UNIT_SEQ, SMBRC_UNIT_EQUIP_START},
		{ SMIO_GROUP_SEQ, SMIO_GROUP_EQUIP_START},
		{ SMIO_UNIT_SEQ, SMIO_UNIT_EQUIP_START},
		{ SMTEMP_GROUP_SEQ, SMTEMP_GROUP_EQUIP_START},
		{ SMTEMP_UNIT_SEQ, SMTEMP_UNIT_EQUIP_START},
		{ SMAC_UNIT_SEQ, SMAC_UNIT_EQUIP_START_1},
		{ SMLVD_UNIT_SEQ, SMLVD_UNIT_EQUIP_START},
		{ LVD3_UNIT_SEQ, LVD3_UNIT_EQUIP_START},
		{ OBBATTFUSE_UNIT_SEQ, OBBATTFUSE_UNIT_EQUIP_START},
		{ COMM_BATT_SEQ, COMM_BATT_UNIT_EQUIP_START},
		{ EIB_BATT_SEQ1, EIB_BATT_UNIT_EQUIP_START},
		{ SMDU_BATT_SEQ1, SMDU_BATT_UNIT_EQUIP_START},
		{ SMDUE_BATT_SEQ1, SMDUE_BATT_UNIT_EQUIP_START},
		{ SM_BATT_SEQ1, SM_BATT_UNIT_EQUIP_START},
		{ SMBRC_BATT_SEQ1, SMBRC_BATT_UNIT_EQUIP_START},
		{ SONICK_BATT_SEQ1, SONICK_BATT_EQUIP_START},
		{ NARADA_BMS_SEQ1, NARADA_BMS_EQUIP_START},
		{ NARADA_BMS_GROUP_SEQ1, NARADA_BMS_GROUP_START},
		{ -1, -1}
	};
	int i;
	int iTotalCount = sizeof(iaEquipNumMapTable)/sizeof(iaEquipNumMapTable[0]);
	int iStartEquipId = -1;
	int iSigCount = 0;
	int iRowNum = 0;
	int iBufSize = 0;
	int iControlNum = 0;
	int iSettingNum = 0;
	WEB_GENERAL_SETTING_INFO *pstWebPrivateSet = NULL;
	CTRL_SIG_VALUE *pControlSigValue = NULL;
	SET_SIG_VALUE *pSettingSigValue = NULL;

	if((piRowNum != NULL) && (ppstWebPrivateSet != NULL))
	{
		iRowNum = 0;
		pstWebPrivateSet = NULL;
		//find the first equipment id
		for(i = 0; i < iTotalCount; i++)
		{
			if(iaEquipNumMapTable[i][0] == iSeqNum)
			{
				iStartEquipId = iaEquipNumMapTable[i][1];
				break;
			}
		}
		//TRACE_WEB_USER("LoadPowerSystemConfigTables---IN--- iStartEquipId=%d\n", iStartEquipId);
		if(iStartEquipId > 0)//find
		{
			iControlNum  = Web_GetControlSignalByEquipID(iStartEquipId, &pControlSigValue);
			iSettingNum  = Web_GetSettingSignalByEquipID(iStartEquipId, &pSettingSigValue);

			//TRACE_WEB_USER("LoadPowerSystemConfigTables iControlNum=%d, iSettingNum=%d\n", iControlNum, iSettingNum);

			if(iControlNum + iSettingNum > 0)
			{
				iBufSize = sizeof(WEB_GENERAL_SETTING_INFO)*(iControlNum + iSettingNum);
				pstWebPrivateSet = (WEB_GENERAL_SETTING_INFO *)NEW(char, iBufSize);

				if(pstWebPrivateSet != NULL)
				{
					memset((void *)pstWebPrivateSet, 0, (size_t)iBufSize);
					iSigCount = 0;
					//control signals
					for(i = 0; i < iControlNum; i++)
					{
						if(STD_SIG_IS_DISPLAY_ON_UI(pControlSigValue->pStdSig, DISPLAY_WEB))
						{
							pstWebPrivateSet[iSigCount].iEquipID = iStartEquipId;
							pstWebPrivateSet[iSigCount].iSignalType = SIG_TYPE_CONTROL;
							pstWebPrivateSet[iSigCount].iSignalID = pControlSigValue->pStdSig->iSigID;

							iSigCount++;
						}

						pControlSigValue++;
					}
					TRACE_WEB_USER("LoadPowerSystemConfigTables iSigCount=%d\n", iSigCount);

					//setting signals
					for(i = 0; i < iSettingNum; i++)
					{
						if(STD_SIG_IS_DISPLAY_ON_UI(pSettingSigValue->pStdSig, DISPLAY_WEB))
						{
							pstWebPrivateSet[iSigCount].iEquipID = iStartEquipId;
							pstWebPrivateSet[iSigCount].iSignalType = SIG_TYPE_SETTING;
							pstWebPrivateSet[iSigCount].iSignalID = pSettingSigValue->pStdSig->iSigID;

							iSigCount++;
						}

						pSettingSigValue++;
					}
					TRACE_WEB_USER("LoadPowerSystemConfigTables iSigCount=%d\n", iSigCount);
					iRowNum = iSigCount;
				}
			}
		}
		TRACE_WEB_USER("LoadPowerSystemConfigTables---OUT--- iStartEquipId=%d\n", iStartEquipId);
		*piRowNum = iRowNum;
		*ppstWebPrivateSet = pstWebPrivateSet;
	}
}



/*==========================================================================*
* FUNCTION :  LoadWebSettingSignalProc
* PURPOSE  :  Read the file <cfg_ui_function.cfg> and initial the struct of s_aWebSetInfo
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  iPages: no use here
pCfg:the struct describe the file of <cfg_ui_function.cfg>
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
static int LoadWebSettingSignalProc(IN int iPages, IN void *pCfg)
{
//changed by Frank Wu,19/32,20140513, for adding new web pages to the tab of "power system"
#define SETTING_SIGNAL_PROC_DEF_ITEM(iArgSeqId, iArgFieldStr)		\
	{	\
		DEF_LOADER_ITEM(&loader[iLoaderItemCount],			\
		NULL,	\
		&(gs_pWebSetInfo[iArgSeqId].iNumber),			\
		iArgFieldStr,	\
		&(gs_pWebSetInfo[iArgSeqId].stWebPrivateSet),	\
		ParsedWebSettingFileTableProc);	\
		iLoaderItemCount++;	\
	}


    UNUSED(iPages);
    int		i = 0, k = 0;
	//changed by Frank Wu,20/32,20140513, for adding new web pages to the tab of "power system"
    //CONFIG_TABLE_LOADER loader[WEB_MAX_HTML_PAGE_SETTING_NUM + 1];
	CONFIG_TABLE_LOADER loader[WEB_MAX_HTML_PAGE_SETTING_NUM];
	CONFIG_TABLE_LOADER loader1[TOTAL_WEBPAGE_SETTING_MAX - WEB_MAX_HTML_PAGE_SETTING_NUM - 1];
    static WEB_PRIVATE_SETTING_INFO	s_aWebSetInfo[WEB_MAX_HTML_PAGE_SETTING_NUM];
    static WEB_PRIVATE_DISPLAY_INFO		s_aWebDisInfo[1];
	int iLoaderItemCount = 0;
	int iLoader1ItemCount = 0;

    gs_pWebSetInfo = s_aWebSetInfo;
    gs_pWebDisplayInfo = s_aWebDisInfo;

    k = 1;
    for(i = 1; i < WEB_MAX_HTML_PAGE_SETTING_NUM + 1; i++)
    {
	strncpyz(gs_pWebSetInfo[i-1].szFileName, szTemplateFileSet[k] , sizeof(gs_pWebSetInfo[i-1].szFileName));
	k = k + 2;
    }

    //DEF_LOADER_ITEM(&loader[CHARGE_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[CHARGE_SEQ].iNumber),
	WEB_PAGES_CHARGE,
	&(gs_pWebSetInfo[CHARGE_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[ECO_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[ECO_SEQ].iNumber),
	WEB_PAGES_ECO,
	&(gs_pWebSetInfo[ECO_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[LVD_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[LVD_SEQ].iNumber),
	WEB_PAGES_LVD,
	&(gs_pWebSetInfo[LVD_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[RECT_SET_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[RECT_SET_SEQ].iNumber),
	WEB_PAGES_RECT1,
	&(gs_pWebSetInfo[RECT_SET_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[BATT_TEST_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[BATT_TEST_SEQ].iNumber),
	WEB_PAGES_BATT_TEST,
	&(gs_pWebSetInfo[BATT_TEST_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[WIZARD_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[WIZARD_SEQ].iNumber),
	WEB_PAGES_WIZARD,
	&(gs_pWebSetInfo[WIZARD_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[TEMP_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[TEMP_SEQ].iNumber),
	WEB_PAGES_TEMP,
	&(gs_pWebSetInfo[TEMP_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[HYBRID_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[HYBRID_SEQ].iNumber),
	WEB_PAGES_HYBRID,
	&(gs_pWebSetInfo[HYBRID_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[SINGLE_RECT_SEQ],
	/*DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[SINGLE_RECT_SEQ].iNumber),
	WEB_PAGES_SINGLE_RECT,
	&(gs_pWebSetInfo[SINGLE_RECT_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;*/

	//changed by Frank Wu,18/11/30,20140527, for add single converter and single solar settings pages
	//SETTING_SIGNAL_PROC_DEF_ITEM( SINGLE_CONVERTER_SEQ, WEB_PAGES_SINGLE_CONVERTER);
	//SETTING_SIGNAL_PROC_DEF_ITEM( SINGLE_SOLAR_SEQ, WEB_PAGES_SINGLE_SOLAR);
	//SETTING_SIGNAL_PROC_DEF_ITEM( SINGLE_RECT_S1_SEQ, WEB_PAGES_SINGLE_RECT_S1);
	//SETTING_SIGNAL_PROC_DEF_ITEM( SINGLE_RECT_S2_SEQ, WEB_PAGES_SINGLE_RECT_S2);
	//SETTING_SIGNAL_PROC_DEF_ITEM( SINGLE_RECT_S3_SEQ, WEB_PAGES_SINGLE_RECT_S3);

    //DEF_LOADER_ITEM(&loader[CONVERTER_SET_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[CONVERTER_SET_SEQ].iNumber),
	WEB_PAGES_CONV,
	&(gs_pWebSetInfo[CONVERTER_SET_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[SINGLE_EIB_SEQ],
	//DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	//NULL,
	//&(gs_pWebSetInfo[SINGLE_EIB_SEQ].iNumber),
	//WEB_PAGES_EIB_UNIT,
	//&(gs_pWebSetInfo[SINGLE_EIB_SEQ].stWebPrivateSet),
	//ParsedWebSettingFileTableProc);
	//iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[SINGLE_SMDU_SEQ],
	//DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	//NULL,
	//&(gs_pWebSetInfo[SINGLE_SMDU_SEQ].iNumber),
	//WEB_PAGES_SMDU_UNIT,
	//&(gs_pWebSetInfo[SINGLE_SMDU_SEQ].stWebPrivateSet),
	//ParsedWebSettingFileTableProc);
	//iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[LVD_GROUP_SEQ],
	//DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	//NULL,
	//&(gs_pWebSetInfo[LVD_GROUP_SEQ].iNumber),
	//WEB_PAGES_LVD_GROUP,
	//&(gs_pWebSetInfo[LVD_GROUP_SEQ].stWebPrivateSet),
	//ParsedWebSettingFileTableProc);
	//iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[CONFIG1_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[CONFIG1_SEQ].iNumber),
	WEB_PAGES_CONFIG1,
	&(gs_pWebSetInfo[CONFIG1_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[CONFIG2_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[CONFIG2_SEQ].iNumber),
	WEB_PAGES_CONFIG2,
	&(gs_pWebSetInfo[CONFIG2_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

    //DEF_LOADER_ITEM(&loader[CONFIG3_SEQ],
	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	NULL,
	&(gs_pWebSetInfo[CONFIG3_SEQ].iNumber),
	WEB_PAGES_CONFIG3,
	&(gs_pWebSetInfo[CONFIG3_SEQ].stWebPrivateSet),
	ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

	DEF_LOADER_ITEM(&loader[iLoaderItemCount],
	    NULL,
	    &(gs_pWebSetInfo[CONFIG4_SEQ].iNumber),
	    WEB_PAGES_CONFIG4,
	    &(gs_pWebSetInfo[CONFIG4_SEQ].stWebPrivateSet),
	    ParsedWebSettingFileTableProc);
	iLoaderItemCount++;

	//changed by Frank Wu,14/16/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
	SETTING_SIGNAL_PROC_DEF_ITEM( SOLAR_SET_SEQ, WEB_PAGES_SOLAR_SET);
	SETTING_SIGNAL_PROC_DEF_ITEM( SHUNT_SET_SEQ, WEB_PAGES_SHUNT_SET);
	//changed by Frank Wu,20/N/35,20140527, for adding the the web setting tab page 'DI'
	SETTING_SIGNAL_PROC_DEF_ITEM( DI_BASIC_SEQ, WEB_PAGES_DI_BASIC);
	SETTING_SIGNAL_PROC_DEF_ITEM( DI_STATE_SEQ, WEB_PAGES_DI_STATE);

	SETTING_SIGNAL_PROC_DEF_ITEM( DO_BASIC_SEQ, WEB_PAGES_DO_BASIC);
	SETTING_SIGNAL_PROC_DEF_ITEM( DO_RELAY_BASIC_SEQ, WEB_PAGES_DO_RELAY_BASIC);
	SETTING_SIGNAL_PROC_DEF_ITEM( SHUNT_SMDU_BASIC_SEQ, WEB_PAGES_SHUNT_BASIC);

	SETTING_SIGNAL_PROC_DEF_ITEM( SHUNT_SMDUP_BASIC_SEQ, WEB_PAGES_SHUNT_SMDUP_BASIC);
	SETTING_SIGNAL_PROC_DEF_ITEM( SHUNT_SMDUE_BASIC_SEQ, WEB_PAGES_SHUNT_SMDUE_BASIC);
	SETTING_SIGNAL_PROC_DEF_ITEM( SHUNT_DCD_BASIC_SEQ, WEB_PAGES_SHUNT_DCD_BASIC);
	SETTING_SIGNAL_PROC_DEF_ITEM( SHUNT_EIB_BASIC_SEQ, WEB_PAGES_SHUNT_EIB_BASIC);
	SETTING_SIGNAL_PROC_DEF_ITEM( SHUNT_BATT_BASIC_SEQ, WEB_PAGES_SHUNT_BAT_BASIC);

	SETTING_SIGNAL_PROC_DEF_ITEM( SMDUE_CUSTOM_INPUT_SEQ, WEB_PAGES_CUSTOM_INPUT_BASIC);

	SETTING_SIGNAL_PROC_DEF_ITEM( SMDUE_ANALOG_SEQ, WEB_PAGES_ANALOG_BASIC);

//changed by Stone Song ,20160525, for adding the the web setting tab page 'FUSE'
	SETTING_SIGNAL_PROC_DEF_ITEM( OBBATT_FUSE_SEQ, WEB_PAGES_OBBATT_FUSE);
	SETTING_SIGNAL_PROC_DEF_ITEM( SMDUBATT_FUSE_SEQ, WEB_PAGES_SMDUBATT_FUSE);
	SETTING_SIGNAL_PROC_DEF_ITEM( OBDC_FUSE_SEQ, WEB_PAGES_OBDC_FUSE);
	SETTING_SIGNAL_PROC_DEF_ITEM( SMDUDC_FUSE_SEQ, WEB_PAGES_SMDUDC_FUSE);
	SETTING_SIGNAL_PROC_DEF_ITEM( SMDUPLUS_FUSE_SEQ, WEB_PAGES_SMDUPLUS_FUSE);
	//SETTING_SIGNAL_PROC_DEF_ITEM( SMDUEBATT_FUSE_SEQ, WEB_PAGES_SMDUEBATT_FUSE);
	SETTING_SIGNAL_PROC_DEF_ITEM( SMDUEDC_FUSE_SEQ, WEB_PAGES_SMDUEDC_FUSE);
	//added by Stone Song to appear the relay DO only exist in system 20160713
	SETTING_SIGNAL_PROC_DEF_ITEM( RELAY_OPTIONS_SEQ, WEB_PAGES_RELAY_OPTIONS);


	//changed by Frank Wu,22/32,20140513, for adding new web pages to the tab of "power system"
    //DEF_LOADER_ITEM(&loader[DISPLAY_SEQ],
	//DEF_LOADER_ITEM(&loader1[DISPLAY_SEQ - WEB_MAX_HTML_PAGE_SETTING_NUM - 1],
	DEF_LOADER_ITEM(&loader1[iLoader1ItemCount],
	NULL,
	&(gs_pWebDisplayInfo[0].iNumber),
	WEB_DISPLAY,
	&(gs_pWebDisplayInfo[0].stWebDisplaySet),
	ParsedWebSettingFileDisTableProc);
	iLoader1ItemCount++;
	
	//changed by Frank Wu,23/32,20140513, for adding new web pages to the tab of "power system"
    //if (Cfg_LoadTables(pCfg, (WEB_MAX_HTML_PAGE_SETTING_NUM + 1), loader) != ERR_CFG_OK)
	//if (Cfg_LoadTables(pCfg, WEB_MAX_HTML_PAGE_SETTING_NUM, loader) != ERR_CFG_OK)
	if (Cfg_LoadTables(pCfg, iLoaderItemCount, loader) != ERR_CFG_OK)
    {
	return ERR_LCD_FAILURE;
    }

	//if (Cfg_LoadTables(pCfg, sizeof(loader1)/sizeof(loader1[0]), loader1) != ERR_CFG_OK)
	if (Cfg_LoadTables(pCfg, iLoader1ItemCount, loader1) != ERR_CFG_OK)
	{
		return ERR_LCD_FAILURE;
	}

    //debug begin
    for(i = 0; i < gs_pWebDisplayInfo[0].iNumber; i++)
    {
	TRACE_WEB_USER_NOT_CYCLE("The settings display[%d] enabled is %d\n", i, gs_pWebDisplayInfo[0].stWebDisplaySet[i].iDisEnable);
    }
    //debug end

	LoadPowerSystemConfigTables(SINGLE_RECT_SEQ, 
							&(gs_pWebSetInfo[SINGLE_RECT_SEQ].iNumber),
							&(gs_pWebSetInfo[SINGLE_RECT_SEQ].stWebPrivateSet));

	//changed by Frank Wu,18/11/30,20140527, for add single converter and single solar settings pages
	LoadPowerSystemConfigTables(SINGLE_CONVERTER_SEQ, 
							&(gs_pWebSetInfo[SINGLE_CONVERTER_SEQ].iNumber),
							&(gs_pWebSetInfo[SINGLE_CONVERTER_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SINGLE_SOLAR_SEQ, 
							&(gs_pWebSetInfo[SINGLE_SOLAR_SEQ].iNumber),
							&(gs_pWebSetInfo[SINGLE_SOLAR_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SINGLE_RECT_S1_SEQ, 
							&(gs_pWebSetInfo[SINGLE_RECT_S1_SEQ].iNumber),
							&(gs_pWebSetInfo[SINGLE_RECT_S1_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SINGLE_RECT_S2_SEQ, 
							&(gs_pWebSetInfo[SINGLE_RECT_S2_SEQ].iNumber),
							&(gs_pWebSetInfo[SINGLE_RECT_S2_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SINGLE_RECT_S3_SEQ, 
							&(gs_pWebSetInfo[SINGLE_RECT_S3_SEQ].iNumber),
							&(gs_pWebSetInfo[SINGLE_RECT_S3_SEQ].stWebPrivateSet));

	//changed by Frank Wu,15/21/32,20140312, for adding new web pages to the tab of "power system"
	LoadPowerSystemConfigTables(SYSTEM_GROUP_SEQ, 
							&(gs_pWebSetInfo[SYSTEM_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[SYSTEM_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(LVD_GROUP_SEQ, 
							&(gs_pWebSetInfo[LVD_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[LVD_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(AC_GROUP_SEQ, 
							&(gs_pWebSetInfo[AC_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[AC_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(AC_UNIT_SEQ, 
							&(gs_pWebSetInfo[AC_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[AC_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(ACMETER_GROUP_SEQ, 
							&(gs_pWebSetInfo[ACMETER_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[ACMETER_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(ACMETER_UNIT_SEQ, 
							&(gs_pWebSetInfo[ACMETER_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[ACMETER_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(DC_UNIT_SEQ, 
							&(gs_pWebSetInfo[DC_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[DC_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(DCMETER_GROUP_SEQ, 
							&(gs_pWebSetInfo[DCMETER_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[DCMETER_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(DCMETER_UNIT_SEQ, 
							&(gs_pWebSetInfo[DCMETER_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[DCMETER_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(DIESEL_GROUP_SEQ, 
							&(gs_pWebSetInfo[DIESEL_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[DIESEL_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(DIESEL_UNIT_SEQ, 
							&(gs_pWebSetInfo[DIESEL_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[DIESEL_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(FUEL_GROUP_SEQ, 
							&(gs_pWebSetInfo[FUEL_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[FUEL_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(FUEL_UNIT_SEQ, 
							&(gs_pWebSetInfo[FUEL_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[FUEL_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(IB_GROUP_SEQ, 
							&(gs_pWebSetInfo[IB_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[IB_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(IB_UNIT_SEQ, 
							&(gs_pWebSetInfo[IB_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[IB_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(EIB_GROUP_SEQ, 
							&(gs_pWebSetInfo[EIB_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[EIB_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SINGLE_EIB_SEQ, 
							&(gs_pWebSetInfo[SINGLE_EIB_SEQ].iNumber),
							&(gs_pWebSetInfo[SINGLE_EIB_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(OBAC_UNIT_SEQ, 
							&(gs_pWebSetInfo[OBAC_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[OBAC_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(OBLVD_UNIT_SEQ, 
							&(gs_pWebSetInfo[OBLVD_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[OBLVD_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(OBFUEL_UNIT_SEQ, 
							&(gs_pWebSetInfo[OBFUEL_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[OBFUEL_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMDU_GROUP_SEQ, 
							&(gs_pWebSetInfo[SMDU_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[SMDU_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SINGLE_SMDU_SEQ, 
							&(gs_pWebSetInfo[SINGLE_SMDU_SEQ].iNumber),
							&(gs_pWebSetInfo[SINGLE_SMDU_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMDUP_GROUP_SEQ, 
							&(gs_pWebSetInfo[SMDUP_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[SMDUP_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMDUP_UNIT_SEQ, 
							&(gs_pWebSetInfo[SMDUP_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[SMDUP_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMDUE_GROUP_SEQ, 
							&(gs_pWebSetInfo[SMDUE_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[SMDUE_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMDUE_UNIT_SEQ, 
							&(gs_pWebSetInfo[SMDUE_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[SMDUE_UNIT_SEQ].stWebPrivateSet));						

	LoadPowerSystemConfigTables(SMDUH_GROUP_SEQ, 
							&(gs_pWebSetInfo[SMDUH_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[SMDUH_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMDUH_UNIT_SEQ, 
							&(gs_pWebSetInfo[SMDUH_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[SMDUH_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMBRC_GROUP_SEQ, 
							&(gs_pWebSetInfo[SMBRC_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[SMBRC_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMBRC_UNIT_SEQ, 
							&(gs_pWebSetInfo[SMBRC_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[SMBRC_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMIO_GROUP_SEQ, 
							&(gs_pWebSetInfo[SMIO_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[SMIO_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMIO_UNIT_SEQ, 
							&(gs_pWebSetInfo[SMIO_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[SMIO_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMTEMP_GROUP_SEQ, 
							&(gs_pWebSetInfo[SMTEMP_GROUP_SEQ].iNumber),
							&(gs_pWebSetInfo[SMTEMP_GROUP_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMTEMP_UNIT_SEQ, 
							&(gs_pWebSetInfo[SMTEMP_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[SMTEMP_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMAC_UNIT_SEQ, 
							&(gs_pWebSetInfo[SMAC_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[SMAC_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMLVD_UNIT_SEQ, 
							&(gs_pWebSetInfo[SMLVD_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[SMLVD_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(LVD3_UNIT_SEQ, 
							&(gs_pWebSetInfo[LVD3_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[LVD3_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(OBBATTFUSE_UNIT_SEQ, 
							&(gs_pWebSetInfo[OBBATTFUSE_UNIT_SEQ].iNumber),
							&(gs_pWebSetInfo[OBBATTFUSE_UNIT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(COMM_BATT_SEQ, 
	    &(gs_pWebSetInfo[COMM_BATT_SEQ].iNumber),
	    &(gs_pWebSetInfo[COMM_BATT_SEQ].stWebPrivateSet));

	LoadPowerSystemConfigTables(EIB_BATT_SEQ1, 
	    &(gs_pWebSetInfo[EIB_BATT_SEQ1].iNumber),
	    &(gs_pWebSetInfo[EIB_BATT_SEQ1].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMDU_BATT_SEQ1, 
	    &(gs_pWebSetInfo[SMDU_BATT_SEQ1].iNumber),
	    &(gs_pWebSetInfo[SMDU_BATT_SEQ1].stWebPrivateSet));

	TRACE_WEB_USER_NOT_CYCLE("SMDU battery signal number is %d\n", gs_pWebSetInfo[SMDU_BATT_SEQ1].iNumber);

	LoadPowerSystemConfigTables(SMDUE_BATT_SEQ1, 
	    &(gs_pWebSetInfo[SMDUE_BATT_SEQ1].iNumber),
	    &(gs_pWebSetInfo[SMDUE_BATT_SEQ1].stWebPrivateSet));


	LoadPowerSystemConfigTables(SM_BATT_SEQ1, 
	    &(gs_pWebSetInfo[SM_BATT_SEQ1].iNumber),
	    &(gs_pWebSetInfo[SM_BATT_SEQ1].stWebPrivateSet));

	LoadPowerSystemConfigTables(SMBRC_BATT_SEQ1, 
	    &(gs_pWebSetInfo[SMBRC_BATT_SEQ1].iNumber),
	    &(gs_pWebSetInfo[SMBRC_BATT_SEQ1].stWebPrivateSet));

	LoadPowerSystemConfigTables(SONICK_BATT_SEQ1, 
	    &(gs_pWebSetInfo[SONICK_BATT_SEQ1].iNumber),
	    &(gs_pWebSetInfo[SONICK_BATT_SEQ1].stWebPrivateSet));

	LoadPowerSystemConfigTables(NARADA_BMS_SEQ1, 
	    &(gs_pWebSetInfo[NARADA_BMS_SEQ1].iNumber),
	    &(gs_pWebSetInfo[NARADA_BMS_SEQ1].stWebPrivateSet));

	LoadPowerSystemConfigTables(NARADA_BMS_GROUP_SEQ1, 
		&(gs_pWebSetInfo[NARADA_BMS_GROUP_SEQ1].iNumber),
		&(gs_pWebSetInfo[NARADA_BMS_GROUP_SEQ1].stWebPrivateSet));

    return ERR_CFG_OK;
}

/*==========================================================================*
* FUNCTION :  LoadWebPrivateSignalProc
* PURPOSE  :  Read the file <cfg_ui_sampling.cfg> and initial the struct of s_aWebSigInfo
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  iPages: no use here
pCfg:the struct describe the file of <cfg_ui_sampling.cfg>
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
static int LoadWebPrivateSignalProc(IN int iPages, IN void *pCfg)
{
    UNUSED(iPages);
    int		i = 0, k = 0;
    //int j;
    CONFIG_TABLE_LOADER loader[WEB_MAX_HTML_PAGE_SIGNAL_NUM + 1];
    static WEB_PRIVATE_SIGNAL_INFO	s_aWebSigInfo[WEB_MAX_HTML_PAGE_SIGNAL_NUM];
    static WEB_PRIVATE_DISPLAY_INFO	s_aWebSamDisInfo[1];

    gs_pWebSigInfo = s_aWebSigInfo;
    gs_pWebDisplaySamInfo = s_aWebSamDisInfo;

    k = 1;
    for(i = 1; i < WEB_MAX_HTML_PAGE_SIGNAL_NUM + 1; i++)
    {
	strncpyz(gs_pWebSigInfo[i-1].szFileName, szTemplateFileSignal[k] , sizeof(gs_pWebSigInfo[i-1].szFileName));
	k = k + 2;
    }

    DEF_LOADER_ITEM(&loader[INDEX_SEQ],
	NULL,
	&(gs_pWebSigInfo[INDEX_SEQ].iNumber), 
	WEB_PAGES_HOMEPAGE,
	&(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    DEF_LOADER_ITEM(&loader[RECT_GROUP_SEQ],
	NULL,
	&(gs_pWebSigInfo[RECT_GROUP_SEQ].iNumber), 
	WEB_PAGES_RECT_GROUP,
	&(gs_pWebSigInfo[RECT_GROUP_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    /*DEF_LOADER_ITEM(&loader[RECT_SINGLE_SEQ],
	NULL,
	&(gs_pWebSigInfo[RECT_SINGLE_SEQ].iNumber), 
	WEB_PAGES_RECT_SINGLE,
	&(gs_pWebSigInfo[RECT_SINGLE_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    DEF_LOADER_ITEM(&loader[RECTS1_GROUP_SEQ],
	NULL,
	&(gs_pWebSigInfo[RECTS1_GROUP_SEQ].iNumber), 
	WEB_PAGES_RECTS1_GROUP,
	&(gs_pWebSigInfo[RECTS1_GROUP_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    /*DEF_LOADER_ITEM(&loader[RECTS1_SINGLE_SEQ],
	NULL,
	&(gs_pWebSigInfo[RECTS1_SINGLE_SEQ].iNumber), 
	WEB_PAGES_RECTS1_SINGLE,
	&(gs_pWebSigInfo[RECTS1_SINGLE_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    DEF_LOADER_ITEM(&loader[RECTS2_GROUP_SEQ],
	NULL,
	&(gs_pWebSigInfo[RECTS2_GROUP_SEQ].iNumber), 
	WEB_PAGES_RECTS2_GROUP,
	&(gs_pWebSigInfo[RECTS2_GROUP_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    /*DEF_LOADER_ITEM(&loader[RECTS2_SINGLE_SEQ],
	NULL,
	&(gs_pWebSigInfo[RECTS2_SINGLE_SEQ].iNumber), 
	WEB_PAGES_RECTS2_SINGLE,
	&(gs_pWebSigInfo[RECTS2_SINGLE_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    DEF_LOADER_ITEM(&loader[RECTS3_GROUP_SEQ],
	NULL,
	&(gs_pWebSigInfo[RECTS3_GROUP_SEQ].iNumber), 
	WEB_PAGES_RECTS3_GROUP,
	&(gs_pWebSigInfo[RECTS3_GROUP_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    /*DEF_LOADER_ITEM(&loader[RECTS3_SINGLE_SEQ],
	NULL,
	&(gs_pWebSigInfo[RECTS3_SINGLE_SEQ].iNumber), 
	WEB_PAGES_RECTS3_SINGLE,
	&(gs_pWebSigInfo[RECTS3_SINGLE_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    DEF_LOADER_ITEM(&loader[CONVERTER_GROUP_SEQ],
	NULL,
	&(gs_pWebSigInfo[CONVERTER_GROUP_SEQ].iNumber), 
	WEB_PAGES_CONVERTER_GROUP,
	&(gs_pWebSigInfo[CONVERTER_GROUP_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    /*DEF_LOADER_ITEM(&loader[CONVERTER_SINGLE_SEQ],
	NULL,
	&(gs_pWebSigInfo[CONVERTER_SINGLE_SEQ].iNumber), 
	WEB_PAGES_CONVERTER_SINGLE,
	&(gs_pWebSigInfo[CONVERTER_SINGLE_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    DEF_LOADER_ITEM(&loader[SOLAR_GROUP_SEQ],
	NULL,
	&(gs_pWebSigInfo[SOLAR_GROUP_SEQ].iNumber), 
	WEB_PAGES_SOLAR_GROUP,
	&(gs_pWebSigInfo[SOLAR_GROUP_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    /*DEF_LOADER_ITEM(&loader[SOLAR_SINGLE_SEQ],
	NULL,
	&(gs_pWebSigInfo[SOLAR_SINGLE_SEQ].iNumber), 
	WEB_PAGES_SOLAR_SINGLE,
	&(gs_pWebSigInfo[SOLAR_SINGLE_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    DEF_LOADER_ITEM(&loader[COMM_BATT_GROUP_SEQ],
	NULL,
	&(gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].iNumber), 
	WEB_PAGES_COMM_BATT_GROUP,
	&(gs_pWebSigInfo[COMM_BATT_GROUP_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    /*DEF_LOADER_ITEM(&loader[COMM_BATT_SINGLE_SEQ],
	NULL,
	&(gs_pWebSigInfo[COMM_BATT_SINGLE_SEQ].iNumber), 
	WEB_PAGES_COMM_BATT_SINGLE,
	&(gs_pWebSigInfo[COMM_BATT_SINGLE_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    /*DEF_LOADER_ITEM(&loader[EIB_BATT_SEQ],
	NULL,
	&(gs_pWebSigInfo[EIB_BATT_SEQ].iNumber), 
	WEB_PAGES_EIB_BATT,
	&(gs_pWebSigInfo[EIB_BATT_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    DEF_LOADER_ITEM(&loader[SMDU_BATT_SEQ],
	NULL,
	&(gs_pWebSigInfo[SMDU_BATT_SEQ].iNumber), 
	WEB_PAGES_SMDU_BATT,
	&(gs_pWebSigInfo[SMDU_BATT_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    DEF_LOADER_ITEM(&loader[SM_BATT_SEQ],
	NULL,
	&(gs_pWebSigInfo[SM_BATT_SEQ].iNumber), 
	WEB_PAGES_SM_BATT,
	&(gs_pWebSigInfo[SM_BATT_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    DEF_LOADER_ITEM(&loader[LARGEDU_BATT_SEQ],
	NULL,
	&(gs_pWebSigInfo[LARGEDU_BATT_SEQ].iNumber), 
	WEB_PAGES_LARGEDU_BATT,
	&(gs_pWebSigInfo[LARGEDU_BATT_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    DEF_LOADER_ITEM(&loader[SMBRC_BATT_SEQ],
	NULL,
	&(gs_pWebSigInfo[SMBRC_BATT_SEQ].iNumber), 
	WEB_PAGES_SMBRC_BATT,
	&(gs_pWebSigInfo[SMBRC_BATT_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    /*DEF_LOADER_ITEM(&loader[DC_SMDU_SEQ],
	NULL,
	&(gs_pWebSigInfo[DC_SMDU_SEQ].iNumber), 
	WEB_PAGES_SMDU,
	&(gs_pWebSigInfo[DC_SMDU_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    DEF_LOADER_ITEM(&loader[DC_SMDUP_SEQ],
	NULL,
	&(gs_pWebSigInfo[DC_SMDUP_SEQ].iNumber), 
	WEB_PAGES_SMDUP,
	&(gs_pWebSigInfo[DC_SMDUP_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    DEF_LOADER_ITEM(&loader[DC_SMDUP1_SEQ],
	NULL,
	&(gs_pWebSigInfo[DC_SMDUP1_SEQ].iNumber), 
	WEB_PAGES_SMDUP1,
	&(gs_pWebSigInfo[DC_SMDUP1_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    /*DEF_LOADER_ITEM(&loader[DC_SMDUH_SEQ],
	NULL,
	&(gs_pWebSigInfo[DC_SMDUH_SEQ].iNumber), 
	WEB_PAGES_SMDUH,
	&(gs_pWebSigInfo[DC_SMDUH_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    DEF_LOADER_ITEM(&loader[DC_EIB_SEQ],
	NULL,
	&(gs_pWebSigInfo[DC_EIB_SEQ].iNumber), 
	WEB_PAGES_EIB,
	&(gs_pWebSigInfo[DC_EIB_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);*/

    DEF_LOADER_ITEM(&loader[CONFIG_SEQ],
	NULL,
	&(gs_pWebSigInfo[CONFIG_SEQ].iNumber), 
	WEB_PAGES_CONFIG,
	&(gs_pWebSigInfo[CONFIG_SEQ].stWebPrivate), 
	ParsedWebSignalFileTableProc);

    DEF_LOADER_ITEM(&loader[DISPLAY_SAM_SEQ],
	NULL,
	&(gs_pWebDisplaySamInfo[0].iNumber), 
	WEB_DISPLAY,
	&(gs_pWebDisplaySamInfo[0].stWebDisplaySet), 
	ParsedWebSettingFileDisTableProc);

    if(Cfg_LoadTables(pCfg, (WEB_MAX_HTML_PAGE_SIGNAL_NUM + 1), loader) != ERR_CFG_OK)
    {
	return ERR_LCD_FAILURE;
    }

    //debug begin
    /*for(i = 0; i < gs_pWebDisplaySamInfo[0].iNumber; i++)
    {
	TRACE_WEB_USER_NOT_CYCLE("The sample display[%d] enabled is %d\n", i, gs_pWebDisplaySamInfo[0].stWebDisplaySet[i].iDisEnable);
    }*/
    //debug end

    //TRACE_WEB_USER("gs_pWebSigInfo[0].iNumber is %d\n", gs_pWebSigInfo[0].iNumber);
    //TRACE_WEB_USER("gs_pWebSigInfo[0].szFileName is %s\n", gs_pWebSigInfo[0].szFileName);
    /*for(j = 0; j < gs_pWebSigInfo[0].iNumber; j++)
    {
    TRACE_WEB_USER("szResourceID[%d] is %s\n", j, gs_pWebSigInfo[0].stWebPrivate[j].szResourceID);
    }*/
    return ERR_CFG_OK;
}

/*==========================================================================*
* FUNCTION :  WEB_ReadSettingSignal
* PURPOSE  :  Read the file <cfg_ui_function.cfg>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
static int WEB_ReadSettingSignal(void)
{
    int		iRst;
    void	*pProf = NULL;  
    FILE	*pFile = NULL;
    char	*szInFile = NULL;
    size_t	ulFileLen;
    //char	*pLangCode = NULL;
    char szCfgFileName[MAX_FILE_PATH];
    int iPagesNumber;

    Cfg_GetFullConfigPath(CONFIG_FILE_WEB_SET_SIGNAL, szCfgFileName, MAX_FILE_PATH);

    pFile = fopen(szCfgFileName, "r");
    if (pFile == NULL)
    {
	TRACE_WEB_USER("Failure to open %s",szCfgFileName);
	return (!ERR_WEB_OK);
    }

    ulFileLen = GetFileLength(pFile);

    szInFile = NEW(char, ulFileLen + 1);
    if (szInFile == NULL)
    {
	AppLogOut("WEB_ReadSettingSignal", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	fclose(pFile);
	return (!ERR_WEB_OK);
    }

    ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
    fclose(pFile);

    if (ulFileLen == 0) 
    {
	/* clear the memory */
	DELETE(szInFile);
	return (!ERR_WEB_OK);
    }
    szInFile[ulFileLen] = '\0';  /* end with NULL */

    /* create SProfile */
    pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

    if (pProf == NULL)
    {
	DELETE(szInFile);
	return (!ERR_WEB_OK);
    }

    //1.Read Number of pages
    iRst = Cfg_ProfileGetInt(pProf,
	WEB_PAGES_NUMBER, 
	&iPagesNumber); 
    TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
    if (iRst != 1)
    {
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "There are no pages number in Resource.cfg.");
    }

    iRst = LoadWebSettingSignalProc(iPagesNumber, pProf);

    if (iRst != ERR_WEB_OK)
    {
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "Fail to get resource table.");

	DELETE(pProf);
	DELETE(szInFile);
	return iRst;
    }

    DELETE(pProf); 
    DELETE(szInFile);
    return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  WEB_ReadConsumptionFile
* PURPOSE  :  Read the file <cfg_consumption_map.cfg>
* CALLS    :
* CALLED BY:
* ARGUMENTS:  void
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-04-02
*==========================================================================*/
static int WEB_ReadConsumptionFile(void)
{
    int		iRst;
    void	*pProf = NULL;  
    FILE	*pFile = NULL;
    char	*szInFile = NULL;
    size_t	ulFileLen;
    //char	*pLangCode = NULL;
    char szCfgFileName[MAX_FILE_PATH];
    int iNumber;
    int iCabinetNumber;
    int iDUNumber;
    int i, iTemp, j;
    char szKeyString[32];
    char szCabinetName[32];
    int	iBranchNum;

    Cfg_GetFullConfigPath(CONFIG_FILE_CONSUMPTION, szCfgFileName, MAX_FILE_PATH);

    pFile = fopen(szCfgFileName, "r");
    if (pFile == NULL)
    {
	TRACE("Failure to open %s",szCfgFileName);
	return (!ERR_WEB_OK);
    }

    ulFileLen = GetFileLength(pFile);

    szInFile = NEW(char, ulFileLen + 1);
    if (szInFile == NULL)
    {
	AppLogOut("WEB_ReadConsumptionFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	fclose(pFile);
	return (!ERR_WEB_OK);
    }

    ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
    fclose(pFile);

    if (ulFileLen == 0) 
    {
	/* clear the memory */
	DELETE(szInFile);
	return (!ERR_WEB_OK);
    }
    szInFile[ulFileLen] = '\0';  /* end with NULL */

    /* create SProfile */
    pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

    if (pProf == NULL)
    {
	DELETE(szInFile);
	return (!ERR_WEB_OK);
    }

    //0.Read config flag
    iRst = Cfg_ProfileGetInt(pProf,
	CONFIG_FLAG, 
	&iNumber);
    gs_stConsumMapInfo.iConfigFlag = iNumber;
    TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iNumber);
    if(iRst != 1)
    {
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "There are no CONFIG_FLAG in cfg_consumption_map.cfg.");
    }
    TRACE_WEB_USER_NOT_CYCLE("gs_stConsumMapInfo.iConfigFlag is %d\n", gs_stConsumMapInfo.iConfigFlag);

    //1.Read X_Axis number
    iRst = Cfg_ProfileGetInt(pProf,
	MAP_XAXIS_NUMBER, 
	&iNumber);
    gs_stConsumMapInfo.ix_Axis = iNumber;
    TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iNumber);
    if(iRst != 1)
    {
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "There are no x_axis number in cfg_consumption_map.cfg.");
    }

    //2.Read Y_Axis number
    iRst = Cfg_ProfileGetInt(pProf,
	MAP_YAXIS_NUMBER, 
	&iNumber);
    gs_stConsumMapInfo.iy_Axis = iNumber;
    TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iNumber);
    if(iRst != 1)
    {
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "There are no y_axis number in cfg_consumption_map.cfg.");
    }

    TRACE_WEB_USER_NOT_CYCLE("The ix_Axis is %d, the iy_Axis is %d\n", gs_stConsumMapInfo.ix_Axis, gs_stConsumMapInfo.iy_Axis);

    gs_stConsumMapInfo.iNumber = gs_stConsumMapInfo.ix_Axis * gs_stConsumMapInfo.iy_Axis;
    gs_stConsumMapInfo.pstCabInfo = NEW(CABINET_INFO, gs_stConsumMapInfo.iNumber);
    if(gs_stConsumMapInfo.pstCabInfo == NULL)
    {
	AppLogOut("WEB_ReadConsumptionFile", APP_LOG_ERROR, "gs_stConsumMapInfo.pstCabInfo NEW operation fails! Out of memory!\n");
	return (!ERR_WEB_OK);
    }
    memset(gs_stConsumMapInfo.pstCabInfo, 0, (sizeof(CABINET_INFO) * gs_stConsumMapInfo.iNumber));

    gs_pWebCabPowerData = NEW(WEB_CABINET_POWER_DATA, gs_stConsumMapInfo.iNumber);
    if(gs_pWebCabPowerData == NULL)
    {
	AppLogOut("WEB_ReadConsumptionFile", APP_LOG_ERROR, "gs_pWebCabPowerData NEW operation fails! Out of memory!\n");
	return (!ERR_WEB_OK);
    }
    //init the buffer to be 0
    memset(gs_pWebCabPowerData, 0, (sizeof(WEB_CABINET_POWER_DATA) * gs_stConsumMapInfo.iNumber));

    //3.Read Number of Cabinet
    iRst = Cfg_ProfileGetInt(pProf,
	MAP_CABINET_NUMBER, 
	&iCabinetNumber); 
    TRACE("Successfully to  Cfg_ProfileGetInt %d\n", iCabinetNumber);
    if (iRst != 1)
    {
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "There are no Cabinets number in cfg_consumption_map.cfg.");
    }
    TRACE_WEB_USER_NOT_CYCLE("The iCabinetNumber is %d\n", iCabinetNumber);
    gs_iCabinetNumber = iCabinetNumber;

    gs_iBranchNum = 0;
    for(i = 0; i < iCabinetNumber; i++)
    {
	// Read the Cabinet name
	iTemp = 0;
	memset(szKeyString, 0, sizeof(szKeyString));
	iTemp = snprintf(szKeyString, 32, szTemplateCabinet[i]);
	snprintf(szKeyString + iTemp, 32, "Name]");
	TRACE_WEB_USER_NOT_CYCLE("%s\n", szKeyString);
	iRst = Cfg_ProfileGetLine(pProf, szKeyString, szCabinetName, 32);
	TRACE_WEB_USER_NOT_CYCLE("Cabinet[%d] name is %s\n", i, szCabinetName);
	snprintf(gs_pWebCabinetInfo[i].szName, 64, szCabinetName);

	//4.Read Number of DU in each Cabinet
	iTemp = 0;
	memset(szKeyString, 0, sizeof(szKeyString));
	iTemp = snprintf(szKeyString, 32, szTemplateCabinet[i]);
	snprintf(szKeyString + iTemp, 32, "DU:Number]");
	TRACE_WEB_USER_NOT_CYCLE("%s\n", szKeyString);
	iRst = Cfg_ProfileGetInt(pProf, szKeyString, &iDUNumber); 
	if (iRst != 1)
	{
	    AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "There are no DU number in cfg_consumption_map.cfg.");
	    continue;
	}
	TRACE_WEB_USER_NOT_CYCLE("Successfully to  Cfg_ProfileGetInt %d\n", iDUNumber);
	TRACE_WEB_USER_NOT_CYCLE("The iDUNumber is %d\n", iDUNumber);
	gs_pWebCabinetInfo[i].iNumber = iDUNumber;

	iRst = LoadWebCabinetProc(iDUNumber, pProf, i);
	iBranchNum = gs_iBranchNum;
	for(j = 0; j < iDUNumber; j++)
	{
	    gs_iBranchNum += gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum;
	}
	if(gs_iBranchNum > MAX_BRANCH_NUM)
	{
	    for(j = 0; j < iDUNumber; j++)
	    {
		if(gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo != NULL)
		{
		    DELETE(gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo);
		    gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo = NULL;
		    gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum = 0;
		}
	    }
	    if(gs_pWebCabinetInfo[i].stWebDU != NULL)
	    {
		DELETE(gs_pWebCabinetInfo[i].stWebDU);
		gs_pWebCabinetInfo[i].stWebDU = NULL;
		gs_pWebCabinetInfo[i].iNumber = 0;
	    }
	    gs_iBranchNum = iBranchNum;
	    break;
	}
    }
    TRACE_WEB_USER_NOT_CYCLE("The total branch number is %d\n", gs_iBranchNum);

    DELETE(pProf);
    DELETE(szInFile);
    return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  WEB_ReadSettingSignal
* PURPOSE  :  Read the file <cfg_ui_sampling.cfg>
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
static int WEB_ReadSignal(void)
{
    int		iRst;
    void	*pProf = NULL;  
    FILE	*pFile = NULL;
    char	*szInFile = NULL;
    size_t	ulFileLen;
    //char	*pLangCode = NULL;
    char szCfgFileName[MAX_FILE_PATH];
    int iPagesNumber;

    Cfg_GetFullConfigPath(CONFIG_FILE_WEB_SIGNAL, szCfgFileName, MAX_FILE_PATH);

    pFile = fopen(szCfgFileName, "r");
    if (pFile == NULL)
    {
	TRACE("Failure to open %s",szCfgFileName);
	return (!ERR_WEB_OK);
    }

    ulFileLen = GetFileLength(pFile);

    szInFile = NEW(char, ulFileLen + 1);
    if (szInFile == NULL)
    {
	AppLogOut("WEB_ReadSignal", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	fclose(pFile);
	return (!ERR_WEB_OK);
    }

    ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
    fclose(pFile);

    if (ulFileLen == 0) 
    {
	/* clear the memory */
	DELETE(szInFile);

	return (!ERR_WEB_OK);
    }
    szInFile[ulFileLen] = '\0';  /* end with NULL */

    /* create SProfile */
    pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

    if (pProf == NULL)
    {
	DELETE(szInFile);
	return (!ERR_WEB_OK);
    }

    //1.Read Number of pages
    iRst = Cfg_ProfileGetInt(pProf,
	WEB_PAGES_NUMBER, 
	&iPagesNumber); 
    TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
    if (iRst != 1)
    {
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "There are no pages number in Resource.cfg.");
    }

    iRst = LoadWebPrivateSignalProc(iPagesNumber, pProf);

    if (iRst != ERR_WEB_OK)
    {
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	    "Fail to get resource table.");

	DELETE(pProf);
	DELETE(szInFile);
	return iRst;
    }

    DELETE(pProf); 
    DELETE(szInFile);
    return ERR_WEB_OK;
}

//changed by Frank Wu,24/32,20140312, for adding new web pages to the tab of "power system"
static int GetSettingSignalPointer( IN int iEquipIdStart1,
									IN int iMaxEquipNum1,
									IN int iEquipIdStart2,
									IN int iMaxEquipNum2,
									IN OUT unsigned int* puiCfgQty, 
									IN OUT unsigned int* puiRealNum,
									OUT WEB_PRIVATE_SETTING_INFO* pWebSetInfo)
{
	unsigned int uiRealNum = *puiRealNum;
	EQUIP_INFO	*pEquipInfo = NULL;
	int iEquipId = 0;
	int iEquipIdOffset = 0;
	int iEquipIdStart = 0;
	int iMaxEquipNum = 0;
	int iEquipIdInConfig = 0;
	int iSigTypeInConfig = 0;
	int iSigIdInConfig = 0;
	int iTimeOut = 0;
	int iError = 0;
	int iBufLen = 0;
	int k = 0, l = 0;
	int iEquipCount = 0;// for equips
	int iSigCount = 0;// for signals

//	printf("Into here uiRealNum = %d (*puiCfgQty) = %d\n", uiRealNum, (*puiCfgQty));
	//malloc space

	if(0 == uiRealNum)
	{
		*puiCfgQty = uiRealNum;
		if(pWebSetInfo->stWebEquipInfo != NULL)
		{
			DELETE(pWebSetInfo->stWebEquipInfo);
			pWebSetInfo->stWebEquipInfo = NULL;
		}
		if(pWebSetInfo->stWebPrivateSet1 != NULL)
		{
			DELETE(pWebSetInfo->stWebPrivateSet1);
			pWebSetInfo->stWebPrivateSet1 = NULL;
		}
		return 0;
	}	
	else if( (*puiCfgQty) != uiRealNum)//need to malloc again
	{
		*puiCfgQty = uiRealNum;
		//malloc stWebEquipInfo
		if(pWebSetInfo->stWebEquipInfo != NULL)
		{
			DELETE(pWebSetInfo->stWebEquipInfo);
			pWebSetInfo->stWebEquipInfo = NULL;
		}
		pWebSetInfo->stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, uiRealNum);
		if(NULL == pWebSetInfo->stWebEquipInfo)
		{
			AppLogOut("GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			return -1;
		}
		//malloc stWebPrivateSet1
		if(pWebSetInfo->stWebPrivateSet1 != NULL)
		{
			DELETE(pWebSetInfo->stWebPrivateSet1);
			pWebSetInfo->stWebPrivateSet1 = NULL;
		}
		pWebSetInfo->stWebPrivateSet1 = NEW(WEB_GENERAL_SETTING_INFO, (uiRealNum * pWebSetInfo->iNumber));
		if(NULL == pWebSetInfo->stWebPrivateSet1)
		{
			if(pWebSetInfo->stWebEquipInfo)
			{
			    DELETE(pWebSetInfo->stWebEquipInfo);
			    pWebSetInfo->stWebEquipInfo = NULL;
			}		
			AppLogOut("GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			return -1;
		}
	}
	else if(iEquipIdStart1 == SMDU_EQUIP_START ||iEquipIdStart1 == SMDUE_EQUIP_START || iEquipIdStart1 == SMDUP_EQUIP_START  || iEquipIdStart1 == EIB_EQUIP_START || iEquipIdStart1 == EQUIP_BATTERY1|| iEquipIdStart1 == 176)
	{	
		if(pWebSetInfo->stWebEquipInfo != NULL)
		{
			DELETE(pWebSetInfo->stWebEquipInfo);
			pWebSetInfo->stWebEquipInfo = NULL;
		}
		pWebSetInfo->stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, uiRealNum);
		if(NULL == pWebSetInfo->stWebEquipInfo)
		{
			AppLogOut("GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			return -1;
		}
		//malloc stWebPrivateSet1
		if(pWebSetInfo->stWebPrivateSet1 != NULL)
		{
			DELETE(pWebSetInfo->stWebPrivateSet1);
			pWebSetInfo->stWebPrivateSet1 = NULL;
		}
		pWebSetInfo->stWebPrivateSet1 = NEW(WEB_GENERAL_SETTING_INFO, (uiRealNum * pWebSetInfo->iNumber));
		if(NULL == pWebSetInfo->stWebPrivateSet1)
		{
			if(pWebSetInfo->stWebEquipInfo)
			{
			    DELETE(pWebSetInfo->stWebEquipInfo);
			    pWebSetInfo->stWebEquipInfo = NULL;
			}		
			AppLogOut("GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			return -1;
		}
	}

	iEquipCount = 0;// for equips
	iSigCount = 0;// for signals

	//process the first range of equipments id
	iEquipIdStart = iEquipIdStart1;
	iMaxEquipNum = iMaxEquipNum1;
	iEquipIdOffset = 0;
//	printf("iEquipIdStart1 = %d, iMaxEquipNum = %d\n",iEquipIdStart1, iMaxEquipNum);

	for(k = 0; k < iMaxEquipNum; k++)
	{
		//the iEquipId which be used to check whether the equip is exist
		iEquipId = iEquipIdStart + k;
		//printf("iEquipId = %d\n", iEquipId);
		iError = DxiGetData(VAR_A_EQUIP_INFO,
			iEquipId,
			0,
			&iBufLen,
			&pEquipInfo,
			0);
 
		if(iError != ERR_DXI_OK)
		{
			printf("GetSettingSignalPointer error1[%d], iError is %d, iEquipId is %d\n", k, iError, iEquipId);
		}
		else if((iError == ERR_DXI_OK) 
				&& (pEquipInfo->bWorkStatus == TRUE) 
				&& (iEquipCount < uiRealNum))
		{
			iError = DxiGetData(VAR_A_EQUIP_INFO,
				iEquipId,
				0,
				&iBufLen,
				&pEquipInfo,
				0);
			if(iError != ERR_DXI_OK)
			{
				//printf("GetSettingSignalPointer error2[%d]\n", iError);
				pWebSetInfo->stWebEquipInfo[iEquipCount].bSingleValid = FALSE;
			}
			else
			{

				pWebSetInfo->stWebEquipInfo[iEquipCount].bSingleValid = TRUE;
				pWebSetInfo->stWebEquipInfo[iEquipCount].pEquipInfo = pEquipInfo;

				for(l = 0; l < pWebSetInfo->iNumber; l++)	// for every signal in equip
				{
					//get iEquipId, SigType and iSigId from configuration file
					
					//Changed by Song xu for SMDU&EIB name changing in SHUNT 20160628
					if(iEquipIdStart==SMDU_EQUIP_START&&pWebSetInfo->stWebPrivateSet[l].iEquipID>=126&&pWebSetInfo->stWebPrivateSet[l].iEquipID<=129)//SMDUBATT
					{
						iEquipIdInConfig = pWebSetInfo->stWebPrivateSet[l].iEquipID + 4*k + iEquipIdOffset;			
					}
					else if(iEquipIdStart==EIB_EQUIP_START&&pWebSetInfo->stWebPrivateSet[l].iEquipID>=118&&pWebSetInfo->stWebPrivateSet[l].iEquipID<=119)//EIBBATT
					{
						iEquipIdInConfig = pWebSetInfo->stWebPrivateSet[l].iEquipID + 2*k + iEquipIdOffset;			
					}
					else
					{
						iEquipIdInConfig = pWebSetInfo->stWebPrivateSet[l].iEquipID + k + iEquipIdOffset;
					}


					if( (1801 == iEquipId) || ( 1802 == iEquipId ) )
					{
						if(iEquipIdStart==SMDUE_EQUIP_START&&pWebSetInfo->stWebPrivateSet[l].iEquipID>=3000 && pWebSetInfo->stWebPrivateSet[l].iEquipID<=3009)//SMDUEBATT
						{
							iEquipIdInConfig = pWebSetInfo->stWebPrivateSet[l].iEquipID + 10*k + iEquipIdOffset;			
						}
					}
				
					iSigTypeInConfig = pWebSetInfo->stWebPrivateSet[l].iSignalType;
					iSigIdInConfig = pWebSetInfo->stWebPrivateSet[l].iSignalID;

					pWebSetInfo->stWebPrivateSet1[iSigCount].iEquipID = iEquipIdInConfig;
					pWebSetInfo->stWebPrivateSet1[iSigCount].iSignalType = iSigTypeInConfig;
					pWebSetInfo->stWebPrivateSet1[iSigCount].iSignalID = iSigIdInConfig;
					pWebSetInfo->stWebPrivateSet1[iSigCount].bWorkStatus = TRUE;
					pWebSetInfo->stWebPrivateSet1[iSigCount].pEquipInfo = pEquipInfo;
					//printf("iSigCount = %d iSigCountiEquipIdInConfig = %d,iSigTypeInConfig = %d,iSigIdInConfig = %d\n",iSigCount, iEquipIdInConfig,iSigTypeInConfig,iSigIdInConfig);
					iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipIdInConfig,
						DXI_MERGE_SIG_ID(iSigTypeInConfig, iSigIdInConfig),
						&iBufLen,
						(void *)&pWebSetInfo->stWebPrivateSet1[iSigCount].pSigValue,
						iTimeOut);
					if(iError != ERR_DXI_OK)
					{
						/*printf("get signal iError=%d, iEquipID=[%d], iSignalType=[%d], iSignalID=[%d]\n", 
											iError, 
											iEquipIdInConfig, 
											iSigTypeInConfig, 
											iSigIdInConfig);*/
						pWebSetInfo->stWebPrivateSet1[iSigCount].bSetVaild = FALSE;
					}
					else
					{
						pWebSetInfo->stWebPrivateSet1[iSigCount].bSetVaild = TRUE;
					}
					iSigCount++;
				}
			}
			iEquipCount++;
		}
	}
		//printf("xxxxxxxxiEquipIdStart1 = %d\n",iEquipIdStart1);
	//process the second range of equipments id 
	iEquipIdStart = iEquipIdStart2;
	iMaxEquipNum = iMaxEquipNum2;
	iEquipIdOffset = iEquipIdStart2 - iEquipIdStart1;
	
	for(k = 0; k < iMaxEquipNum; k++)
	{

		//the iEquipId which be used to check whether the equip is exist
		iEquipId = iEquipIdStart + k;

		iError = DxiGetData(VAR_A_EQUIP_INFO,
			iEquipId,
			0,
			&iBufLen,
			&pEquipInfo,
			0);
		if(iError != ERR_DXI_OK)
		{
		//	printf("GetSettingSignalPointer error1[%d], iError is %d, iEquipId is %d\n", k, iError, iEquipId);
			TRACE_WEB_USER("GetSettingSignalPointer error1[%d], iError is %d\n", k, iError);
		}	
		else if((iError == ERR_DXI_OK) 
			&& (pEquipInfo->bWorkStatus == TRUE) 
			&& (iEquipCount < uiRealNum))
		{
			iError = DxiGetData(VAR_A_EQUIP_INFO,
				iEquipId,
				0,
				&iBufLen,
				&pEquipInfo,
				0);
			if(iError != ERR_DXI_OK)
			{
			//	printf("GetSettingSignalPointer error2[%d]\n", iError);
				TRACE_WEB_USER("GetSettingSignalPointer error2[%d]\n", k);
				pWebSetInfo->stWebEquipInfo[iEquipCount].bSingleValid = FALSE;
			}
			else
			{
				pWebSetInfo->stWebEquipInfo[iEquipCount].bSingleValid = TRUE;
				pWebSetInfo->stWebEquipInfo[iEquipCount].pEquipInfo = pEquipInfo;



				for(l = 0; l < pWebSetInfo->iNumber; l++)	// for every signal in equip
				{

					//get iEquipId, SigType and iSigId from configuration file
					iEquipIdInConfig = pWebSetInfo->stWebPrivateSet[l].iEquipID + k + iEquipIdOffset;
					iSigTypeInConfig = pWebSetInfo->stWebPrivateSet[l].iSignalType;
					iSigIdInConfig = pWebSetInfo->stWebPrivateSet[l].iSignalID;

					pWebSetInfo->stWebPrivateSet1[iSigCount].iEquipID = iEquipIdInConfig;
					pWebSetInfo->stWebPrivateSet1[iSigCount].iSignalType = iSigTypeInConfig;
					pWebSetInfo->stWebPrivateSet1[iSigCount].iSignalID = iSigIdInConfig;
					pWebSetInfo->stWebPrivateSet1[iSigCount].bWorkStatus = TRUE;
					pWebSetInfo->stWebPrivateSet1[iSigCount].pEquipInfo = pEquipInfo;
					iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipIdInConfig,
						DXI_MERGE_SIG_ID(iSigTypeInConfig, iSigIdInConfig),
						&iBufLen,
						(void *)&pWebSetInfo->stWebPrivateSet1[iSigCount].pSigValue,
						iTimeOut);
					if(iError != ERR_DXI_OK)
					{
						TRACE_WEB_USER("get signal iError=%d, iEquipID=[%d], iSignalType=[%d], iSignalID=[%d]\n", 
							iError, 
							iEquipIdInConfig, 
							iSigTypeInConfig, 
							iSigIdInConfig);
						pWebSetInfo->stWebPrivateSet1[iSigCount].bSetVaild = FALSE;
					}
					else
					{
						pWebSetInfo->stWebPrivateSet1[iSigCount].bSetVaild = TRUE;
					}
					iSigCount++;
				}
			}
			iEquipCount++;
		}
	}
		//printf("zzzzzzzzzzzziEquipIdStart1 = %d\n",iEquipIdStart2);
	//if there are some excepts, fill with valid blank data
	if(iEquipCount < uiRealNum)
	{
		AppLogOut("GetSettingSignalPointer",
			APP_LOG_ERROR,
			"Read Signal Pointer Fail!(iEquipIdStart1=%d, iEquipCount=%d, uiRealNum=%d)\n",
			iEquipIdStart1,
			iEquipCount,
			uiRealNum
			);

		for(k = iEquipCount; k < uiRealNum; k++)
		{
			pWebSetInfo->stWebEquipInfo[iEquipCount].bSingleValid = FALSE;
			pWebSetInfo->stWebEquipInfo[iEquipCount].pEquipInfo = NULL;

			for(l = 0; l < pWebSetInfo->iNumber; l++)
			{
				pWebSetInfo->stWebPrivateSet1[iSigCount].bSetVaild = FALSE;
				pWebSetInfo->stWebPrivateSet1[iSigCount].bWorkStatus = FALSE;
				pWebSetInfo->stWebPrivateSet1[iSigCount].pSigValue = NULL;
				pWebSetInfo->stWebPrivateSet1[iSigCount].pEquipInfo = NULL;

				iSigCount++;
			}

			iEquipCount++;
		}
		return (-2);
	}

	return 0;
}


/*==========================================================================*
* FUNCTION :  Web_GetSettingSignalPointer
* PURPOSE  :  Initial the pointer pointing to setting value
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
int Web_GetSettingSignalPointer(void)
{
//changed by Frank Wu,25/32,20140312, for adding new web pages to the tab of "power system"
#define SETTING_SIGNAL_POINTER_PROC(iArgSeqId,				\
									iArgEqupIdStart1,			\
									iArgEqupNum1,				\
									iArgEqupIdStart2,			\
									iArgEqupNum2,				\
									bArgAutoCalcEqupNum,		\
									uiArgCfgQtyOfSetNum,	\
									uiArgRealNum)			\
	{	\
		if(iArgSeqId == i)		\
		{	\
			int iTmpErr;	\
			TRACE_WEB_USER("Web_GetSettingSignalPointer iSEQNum=%d, uiRealNum=%d\n", i, uiArgRealNum);		\
			if(bArgAutoCalcEqupNum)	\
			{	\
				iTmpErr = GetSettingSignalPointer(iArgEqupIdStart1,		\
							(iArgEqupNum1 <= 0)? uiArgRealNum: ((uiArgRealNum > iArgEqupNum1)? iArgEqupNum1: uiArgRealNum),	\
							iArgEqupIdStart2,		\
							(iArgEqupNum1 <= 0)? 0: ((uiArgRealNum > iArgEqupNum1)? uiArgRealNum - iArgEqupNum1: 0),	\
							&uiArgCfgQtyOfSetNum,	\
							&uiArgRealNum,			\
							&gs_pWebSetInfo[i]);	\
			}	\
			else	\
			{	\
				iTmpErr = GetSettingSignalPointer(iArgEqupIdStart1,		\
							iArgEqupNum1,			\
							iArgEqupIdStart2,		\
							iArgEqupNum2,			\
							&uiArgCfgQtyOfSetNum,	\
							&uiArgRealNum,			\
							&gs_pWebSetInfo[i]);	\
			}	\
			if(iTmpErr != 0)		\
			{	\
				if(-1 == iTmpErr)	\
				{	\
					return 1;	\
				}	\
				else if(-2 == iTmpErr)	\
				{	\
					return 2;	\
				}	\
			}	\
		}	\
	}




#define RECT_FIRST_RANGE_NUM		100
#define RECT_SECOND_RANGE_START		1601

    int i, j, k, l;
    unsigned int m, n;
    int	iError = 0;
    EQUIP_INFO	*pEquipInfo;
    int iEquip;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    static unsigned int s_uiCfgQtyOfSetLVD = 0;
    static unsigned int s_uiCfgQtyOfSetEIB = 0;
    static unsigned int s_uiCfgQtyOfSetSMDU = 0;
    static unsigned int s_uiCfgQtyOfSetLVDGroup = 0;
    static unsigned int s_uiSetRectifierNum = 0;
	//changed by Frank Wu,20/13/30,20140527, for add single converter and single solar settings pages
	static unsigned int s_uiSetSingleConverterNum = 0;
	static unsigned int s_uiSetSingleSolarNum = 0;
	static unsigned int s_uiSetSingleRectS1Num = 0;
	static unsigned int s_uiSetSingleRectS2Num = 0;
	static unsigned int s_uiSetSingleRectS3Num = 0;
	//changed by Frank Wu,16/26/32,20140312, for adding new web pages to the tab of "power system"
	static unsigned int s_uiCfgQtyOfSetSystemGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetACGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetACUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetACMeterGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetACMeterUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetDCUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetDCMeterGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetDCMeterUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetDieselGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetDieselUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetFuelGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetFuelUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetIBGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetIBUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetEIBGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetOBACUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetOBLVDUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetOBFuelUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMDUGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMDUPGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMDUPUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMDUEGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMDUEUnitNum = 0;	
	static unsigned int s_uiCfgQtyOfSetSMDUHGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMDUHUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMBRCGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMBRCUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMIOGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMIOUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMTempGroupNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMTempUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMACUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMLVDUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetLVD3UnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetOBBattFuseUnitNum = 0;

	static unsigned int s_uiCfgQtyOfSetCommBattUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetEIBBattUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMDUBattUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMDUEBattUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMBatUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSMBRCBatUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetSonickBatUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetNaradaBMSUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetNaradaBMSGroupNum = 0;

	static unsigned int siEIBNumber = 0;//iRealEIBNum =4;
	static unsigned int siBatteryNumber = 0;//iBatteryRealNumber = 2
	static unsigned int siDCDNumber = 0;//iDCDRealNumber = 1

//changed by Stone Song,20160606, for adding the the web setting tab page 'FUSE'
	static unsigned int iOBBATT_FuseNum = 0;
	static unsigned int iSMDUBATT_FuseNum = 0;
	static unsigned int iOBDC_FuseNum = 0;
	static unsigned int iSMDUDC_FuseNum = 0;
	static unsigned int iSMDUPLUS_FuseNum = 0;
	static unsigned int iSMDUEBATT_FuseNum = 0;
	static unsigned int iSMDUEDC_FuseNum = 0;

    int iMaxEquipNum = 0;
    int iEquipNum = 0;

    TRACE_WEB_USER("Begin to Exec Web_GetSettingSignalPointer\n");
    for(i = 0; i < WEB_MAX_HTML_PAGE_SETTING_NUM; i++)
    {
	for(j = 0; j < gs_pWebSetInfo[i].iNumber; j++)
	{

	    iVarSubID = DXI_MERGE_SIG_ID(gs_pWebSetInfo[i].stWebPrivateSet[j].iSignalType, gs_pWebSetInfo[i].stWebPrivateSet[j].iSignalID);
	    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		gs_pWebSetInfo[i].stWebPrivateSet[j].iEquipID,
		iVarSubID,
		&iBufLen,
		(void *)&gs_pWebSetInfo[i].stWebPrivateSet[j].pSigValue,
		iTimeOut);
	    if(iError != ERR_DXI_OK)
	    {
		TRACE_WEB_USER("Web_GetSettingSignalPointer error[%d][%d]\n", i, j);
		gs_pWebSetInfo[i].stWebPrivateSet[j].bSetVaild = FALSE;
	    }
	    else
	    {
		gs_pWebSetInfo[i].stWebPrivateSet[j].bSetVaild = TRUE;
	    }
	    iError = DxiGetData(VAR_A_EQUIP_INFO,
		gs_pWebSetInfo[i].stWebPrivateSet[j].iEquipID,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);
	    if(iError != ERR_DXI_OK)
	    {
		TRACE_WEB_USER("Web_GetSettingSignalPointer error[%d][%d]\n", i, j);
		gs_pWebSetInfo[i].stWebPrivateSet[j].bWorkStatus = FALSE;
	    }
	    else
	    {
		gs_pWebSetInfo[i].stWebPrivateSet[j].bWorkStatus = TRUE;
		gs_pWebSetInfo[i].stWebPrivateSet[j].pEquipInfo = pEquipInfo;
	    }
	}
	//changed by Frank Wu,17/27/32,20140312, for adding new web pages to the tab of "power system"
	//init member vars which stWebEquipInfo and stWebPrivateSet1

	SETTING_SIGNAL_POINTER_PROC( SYSTEM_GROUP_SEQ,
		SYSTEM_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetSystemGroupNum,
		gs_uiRealSystemGroupNum);

	SETTING_SIGNAL_POINTER_PROC( AC_GROUP_SEQ,
		AC_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetACGroupNum,
		gs_uiRealACGroupNum);

	SETTING_SIGNAL_POINTER_PROC( AC_UNIT_SEQ,
		AC_UNIT_EQUIP_START,
		AC_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetACUnitNum,
		gs_uiRealACUnitNum);

	SETTING_SIGNAL_POINTER_PROC( ACMETER_GROUP_SEQ,
		ACMETER_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetACMeterGroupNum,
		gs_uiRealACMeterGroupNum);

	SETTING_SIGNAL_POINTER_PROC( ACMETER_UNIT_SEQ,
		ACMETER_UNIT_EQUIP_START,
		ACMETER_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetACMeterUnitNum,
		gs_uiRealACMeterUnitNum);

	SETTING_SIGNAL_POINTER_PROC( DC_UNIT_SEQ,
		DC_UNIT_EQUIP_START,
		DC_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetDCUnitNum,
		gs_uiRealDCUnitNum);

	SETTING_SIGNAL_POINTER_PROC( DCMETER_GROUP_SEQ,
		DCMETER_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetDCMeterGroupNum,
		gs_uiRealDCMeterGroupNum);

	SETTING_SIGNAL_POINTER_PROC( DCMETER_UNIT_SEQ,
		DCMETER_UNIT_EQUIP_START,
		DCMETER_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetDCMeterUnitNum,
		gs_uiRealDCMeterUnitNum);

	SETTING_SIGNAL_POINTER_PROC( DIESEL_GROUP_SEQ,
		DIESEL_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetDieselGroupNum,
		gs_uiRealDieselGroupNum);

	SETTING_SIGNAL_POINTER_PROC( DIESEL_UNIT_SEQ,
		DIESEL_UNIT_EQUIP_START,
		DIESEL_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetDieselUnitNum,
		gs_uiRealDieselUnitNum);

	SETTING_SIGNAL_POINTER_PROC( FUEL_GROUP_SEQ,
		FUEL_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetFuelGroupNum,
		gs_uiRealFuelGroupNum);

	SETTING_SIGNAL_POINTER_PROC( FUEL_UNIT_SEQ,
		FUEL_UNIT_EQUIP_START,
		FUEL_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetFuelUnitNum,
		gs_uiRealFuelUnitNum);

	SETTING_SIGNAL_POINTER_PROC( IB_GROUP_SEQ,
		IB_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetIBGroupNum,
		gs_uiRealIBGroupNum);

	SETTING_SIGNAL_POINTER_PROC( IB_UNIT_SEQ,
		IB_UNIT_EQUIP_START,
		IB_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetIBUnitNum,
		gs_uiRealIBUnitNum);

	SETTING_SIGNAL_POINTER_PROC( EIB_GROUP_SEQ,
		EIB_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetEIBGroupNum,
		gs_uiRealEIBGroupNum);

	SETTING_SIGNAL_POINTER_PROC( OBAC_UNIT_SEQ,
		OBAC_UNIT_EQUIP_START,
		OBAC_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetOBACUnitNum,
		gs_uiRealOBACUnitNum);

	SETTING_SIGNAL_POINTER_PROC( OBLVD_UNIT_SEQ,
		OBLVD_UNIT_EQUIP_START,
		OBLVD_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetOBLVDUnitNum,
		gs_uiRealOBLVDUnitNum);

	SETTING_SIGNAL_POINTER_PROC( OBFUEL_UNIT_SEQ,
		OBFUEL_UNIT_EQUIP_START,
		OBFUEL_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetOBFuelUnitNum,
		gs_uiRealOBFuelUnitNum);

	SETTING_SIGNAL_POINTER_PROC( SMDU_GROUP_SEQ,
		SMDU_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetSMDUGroupNum,
		gs_uiRealSMDUGroupNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUP_GROUP_SEQ,
		SMDUP_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetSMDUPGroupNum,
		gs_uiRealSMDUPGroupNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUP_UNIT_SEQ,
		SMDUP_UNIT_EQUIP_START,
		SMDUP_UNIT_NUM,
		SMDUP_UNIT_EQUIP_START1,
		SMDUP_UNIT_NUM1,
		FALSE,
		s_uiCfgQtyOfSetSMDUPUnitNum,
		gs_uiRealSMDUPNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUE_GROUP_SEQ,
		SMDUE_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetSMDUEGroupNum,
		gs_uiRealSMDUEGroupNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUE_UNIT_SEQ,
		SMDUE_UNIT_EQUIP_START,
		SMDUE_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMDUEUnitNum,
		gs_uiRealSMDUENum);

	SETTING_SIGNAL_POINTER_PROC( SMDUH_GROUP_SEQ,
		SMDUH_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetSMDUHGroupNum,
		gs_uiRealSMDUHGroupNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUH_UNIT_SEQ,
		SMDUH_UNIT_EQUIP_START,
		SMDUH_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMDUHUnitNum,
		gs_uiRealSMDUHNum);

	SETTING_SIGNAL_POINTER_PROC( SMBRC_GROUP_SEQ,
		SMBRC_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetSMBRCGroupNum,
		gs_uiRealSMBRCGroupNum);

	SETTING_SIGNAL_POINTER_PROC( SMBRC_UNIT_SEQ,
		SMBRC_UNIT_EQUIP_START,
		SMBRC_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMBRCUnitNum,
		gs_uiRealSMBRCUnitNum);

	SETTING_SIGNAL_POINTER_PROC( SMIO_GROUP_SEQ,
		SMIO_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetSMIOGroupNum,
		gs_uiRealSMIOGroupNum);

	SETTING_SIGNAL_POINTER_PROC( SMIO_UNIT_SEQ,
		SMIO_UNIT_EQUIP_START,
		SMIO_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMIOUnitNum,
		gs_uiRealSMIOUnitNum);

	SETTING_SIGNAL_POINTER_PROC( SMTEMP_GROUP_SEQ,
		SMTEMP_GROUP_EQUIP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetSMTempGroupNum,
		gs_uiRealSMTempGroupNum);

	SETTING_SIGNAL_POINTER_PROC( SMTEMP_UNIT_SEQ,
		SMTEMP_UNIT_EQUIP_START,
		SMTEMP_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMTempUnitNum,
		gs_uiRealSMTempUnitNum);

	SETTING_SIGNAL_POINTER_PROC( SMAC_UNIT_SEQ,
		SMAC_UNIT_EQUIP_START_1,
		SMAC_UNIT_NUM_1,
		SMAC_UNIT_EQUIP_START_2,
		SMAC_UNIT_NUM_2,
		FALSE,
		s_uiCfgQtyOfSetSMACUnitNum,
		gs_uiRealSMACUnitNum);

	SETTING_SIGNAL_POINTER_PROC( SMLVD_UNIT_SEQ,
		SMLVD_UNIT_EQUIP_START,
		SMLVD_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMLVDUnitNum,
		gs_uiRealSMLVDUnitNum);

	SETTING_SIGNAL_POINTER_PROC( LVD3_UNIT_SEQ,
		LVD3_UNIT_EQUIP_START,
		LVD3_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetLVD3UnitNum,
		gs_uiRealLVD3UnitNum);

	SETTING_SIGNAL_POINTER_PROC( OBBATTFUSE_UNIT_SEQ,
		OBBATTFUSE_UNIT_EQUIP_START,
		OBBATTFUSE_UNIT_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetOBBattFuseUnitNum,
		gs_uiRealOBBattFuseUnitNum);


	SETTING_SIGNAL_POINTER_PROC( SHUNT_SMDU_BASIC_SEQ,
		SMDU_EQUIP_START,
		SMDU_SMDUP_SMDUH_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMDU,
		gs_uiRealSMDUNum);


	SETTING_SIGNAL_POINTER_PROC( SHUNT_SMDUP_BASIC_SEQ,
		SMDUP_EQUIP_START,
		SMDU_SMDUP_SMDUH_NUM,
		SMDUP_EQUIP_START1,
		SMDU_SMDUP_SMDUH_NUM1,
		FALSE,
		s_uiCfgQtyOfSetSMDUPUnitNum,
		gs_uiRealSMDUPNum);

	SETTING_SIGNAL_POINTER_PROC( SHUNT_SMDUE_BASIC_SEQ,
		SMDUE_EQUIP_START,
		SMDU_SMDUP_SMDUH_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMDUEUnitNum,
		gs_uiRealSMDUENum);

	SETTING_SIGNAL_POINTER_PROC( SHUNT_EIB_BASIC_SEQ,
		EIB_EQUIP_START,
		EIB_NUM,
		0,
		0,
		FALSE,
		siEIBNumber,
		gs_uiRealEIBNum);


	int iDCDNumber = 0, iDCDRealNumber =1;
	SETTING_SIGNAL_POINTER_PROC( SHUNT_DCD_BASIC_SEQ,
		176,
		1,
		0,
		0,
		FALSE,
		siDCDNumber,
		gs_uiRealDCUnitNum);


	int iBatteryNumber = 0, iBatteryRealNumber =0;
	SETTING_SIGNAL_POINTER_PROC( SHUNT_BATT_BASIC_SEQ,
		EQUIP_BATTERY1,
		2,
		0,
		0,
		FALSE,
		siBatteryNumber,
		gs_uiRealComBattNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUE_CUSTOM_INPUT_SEQ,
		SMDUE_EQUIP_START,
		SMDU_SMDUP_SMDUH_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMDUEUnitNum,
		gs_uiRealSMDUENum);


	SETTING_SIGNAL_POINTER_PROC( SMDUE_ANALOG_SEQ,
		SMDUE_EQUIP_START,
		SMDU_SMDUP_SMDUH_NUM,
		0,
		0,
		FALSE,
		s_uiCfgQtyOfSetSMDUEUnitNum,
		gs_uiRealSMDUENum);

//changed by Stone Song,20160525, for adding the the web setting tab page 'FUSE'

	SETTING_SIGNAL_POINTER_PROC( OBBATT_FUSE_SEQ,
		OBBATT_FUSE_START,
		OBBATT_FUSE_NUM,
		0,
		0,
		FALSE,
		iOBBATT_FuseNum,
		gs_uiRealBattFuseNum);


	SETTING_SIGNAL_POINTER_PROC( SMDUBATT_FUSE_SEQ,
		SMDUBATT_FUSE_START_1,
		SMDUBATT_FUSE_NUM_1,
		SMDUBATT_FUSE_START_2,
		SMDUBATT_FUSE_NUM_2,
		FALSE,
		iSMDUBATT_FuseNum,
		iSMDUBATT_FuseRealNum);

	SETTING_SIGNAL_POINTER_PROC( OBDC_FUSE_SEQ,
		OBDC_FUSE_START,
		OBDC_FUSE_NUM,
		0,
		0,
		FALSE,
		iOBDC_FuseNum,
		iOBDC_FuseRealNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUDC_FUSE_SEQ,
		SMDUDC_FUSE_START,
		SMDUDC_FUSE_NUM,
		0,
		0,
		FALSE,
		iSMDUDC_FuseNum,
		iSMDUDC_FuseRealNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUPLUS_FUSE_SEQ,
		SMDUPLUS_FUSE_START,
		SMDUPLUS_FUSE_NUM,
		SMDUPLUS_FUSE_START1,
		SMDUPLUS_FUSE_NUM1,
		FALSE,
		iSMDUPLUS_FuseNum,
		iSMDUPLUS_FuseRealNum);

	/*SETTING_SIGNAL_POINTER_PROC( SMDUEBATT_FUSE_SEQ,
		SMDUEBATT_FUSE_START,
		SMDUEBATT_FUSE_NUM,
		0,
		0,
		FALSE,
		iSMDUEBATT_FuseNum,
		iSMDUEBATT_FuseRealNum);*/

	SETTING_SIGNAL_POINTER_PROC( SMDUEDC_FUSE_SEQ,
		SMDUEDC_FUSE_START,
		SMDUEDC_FUSE_NUM,
		0,
		0,
		FALSE,
		iSMDUEDC_FuseNum,
		iSMDUEDC_FuseRealNum);

	if((i == SINGLE_EIB_SEQ) || (i == SINGLE_SMDU_SEQ) || (i == LVD_GROUP_SEQ))// init the EIB equip and signals
	{
	    m = 0;// for equips
	    n = 0;// for signals
	    if(i == SINGLE_EIB_SEQ)
	    {
		iMaxEquipNum = EIB_NUM;
		iEquipNum = gs_uiRealEIBNum;
		TRACE_WEB_USER("gs_uiRealEIBNum is %d\n", gs_uiRealEIBNum);
		if(s_uiCfgQtyOfSetEIB != gs_uiRealEIBNum)
		{
		    s_uiCfgQtyOfSetEIB = gs_uiRealEIBNum;
		    if(gs_pWebSetInfo[i].stWebEquipInfo != NULL)
		    {
			DELETE(gs_pWebSetInfo[i].stWebEquipInfo);
			gs_pWebSetInfo[i].stWebEquipInfo = NULL;
		    }
		    gs_pWebSetInfo[i].stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, gs_uiRealEIBNum);
		    if(gs_pWebSetInfo[i].stWebEquipInfo == NULL)
		    {
			AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			return 1;
		    }
		    if(gs_pWebSetInfo[i].stWebPrivateSet1 != NULL)
		    {
			DELETE(gs_pWebSetInfo[i].stWebPrivateSet1);
			gs_pWebSetInfo[i].stWebPrivateSet1 = NULL;
		    }
		    gs_pWebSetInfo[i].stWebPrivateSet1 = NEW(WEB_GENERAL_SETTING_INFO, (gs_uiRealEIBNum * gs_pWebSetInfo[i].iNumber));
		    if(gs_pWebSetInfo[i].stWebPrivateSet1 == NULL)
		    {
			if(gs_pWebSetInfo[i].stWebEquipInfo)
			{
			    DELETE(gs_pWebSetInfo[i].stWebEquipInfo);
			    gs_pWebSetInfo[i].stWebEquipInfo = NULL;
			}
			AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			// here we can't delete gs_pWebSetInfo[i].stWebEquipInfo for we will use it in other function
			return 1;
		    }
		}
	    }
	    else if(i == SINGLE_SMDU_SEQ)
	    {
		iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM;
		iEquipNum = gs_uiRealSMDUNum;
		TRACE_WEB_USER("gs_uiRealSMDUNum is %d\n", gs_uiRealSMDUNum);
		if(s_uiCfgQtyOfSetSMDU != gs_uiRealSMDUNum)
		{
		    s_uiCfgQtyOfSetSMDU = gs_uiRealSMDUNum;
		    if(gs_pWebSetInfo[i].stWebEquipInfo != NULL)
		    {
			DELETE(gs_pWebSetInfo[i].stWebEquipInfo);
			gs_pWebSetInfo[i].stWebEquipInfo = NULL;
		    }
		    gs_pWebSetInfo[i].stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, gs_uiRealSMDUNum);
		    if(gs_pWebSetInfo[i].stWebEquipInfo == NULL)
		    {
			AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			return 1;
		    }
		    if(gs_pWebSetInfo[i].stWebPrivateSet1 != NULL)
		    {
			DELETE(gs_pWebSetInfo[i].stWebPrivateSet1);
			gs_pWebSetInfo[i].stWebPrivateSet1 = NULL;
		    }
		    gs_pWebSetInfo[i].stWebPrivateSet1 = NEW(WEB_GENERAL_SETTING_INFO, (gs_uiRealSMDUNum * gs_pWebSetInfo[i].iNumber));
		    if(gs_pWebSetInfo[i].stWebPrivateSet1 == NULL)
		    {
			if(gs_pWebSetInfo[i].stWebEquipInfo)
			{
			    DELETE(gs_pWebSetInfo[i].stWebEquipInfo);
			    gs_pWebSetInfo[i].stWebEquipInfo = NULL;
			}
			AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			// here we can't delete gs_pWebSetInfo[i].stWebEquipInfo for we will use it in other function
			return 1;
		    }
		}
	    }
	    else if(i == LVD_GROUP_SEQ)
	    {
		iMaxEquipNum = GROUP_NUM;
		iEquipNum = gs_uiRealLVDGroupNum;
		TRACE_WEB_USER("gs_uiRealLVDGroupNum is %d\n", gs_uiRealLVDGroupNum);
		if(s_uiCfgQtyOfSetLVDGroup != gs_uiRealLVDGroupNum)
		{
		    s_uiCfgQtyOfSetLVDGroup = gs_uiRealLVDGroupNum;
		    if(gs_pWebSetInfo[i].stWebEquipInfo != NULL)
		    {
			DELETE(gs_pWebSetInfo[i].stWebEquipInfo);
			gs_pWebSetInfo[i].stWebEquipInfo = NULL;
		    }
		    gs_pWebSetInfo[i].stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, gs_uiRealLVDGroupNum);
		    if(gs_pWebSetInfo[i].stWebEquipInfo == NULL)
		    {
			AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			return 1;
		    }
		    if(gs_pWebSetInfo[i].stWebPrivateSet1 != NULL)
		    {
			DELETE(gs_pWebSetInfo[i].stWebPrivateSet1);
			gs_pWebSetInfo[i].stWebPrivateSet1 = NULL;
		    }
		    gs_pWebSetInfo[i].stWebPrivateSet1 = NEW(WEB_GENERAL_SETTING_INFO, (gs_uiRealLVDGroupNum * gs_pWebSetInfo[i].iNumber));
		    if(gs_pWebSetInfo[i].stWebPrivateSet1 == NULL)
		    {
			if(gs_pWebSetInfo[i].stWebEquipInfo)
			{
			    DELETE(gs_pWebSetInfo[i].stWebEquipInfo);
			    gs_pWebSetInfo[i].stWebEquipInfo = NULL;
			}
			AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			// here we can't delete gs_pWebSetInfo[i].stWebEquipInfo for we will use it in other function
			return 1;
		    }
		}
	    }
	    for(k = 0; k < iMaxEquipNum; k++)
	    {
		if(i == SINGLE_EIB_SEQ)
		{
		    iEquip = EIB_EQUIP_START + k;
		}
		else if(i == SINGLE_SMDU_SEQ)
		{
		    iEquip = SMDU_EQUIP_START + k;
		}
		else if(i == LVD_GROUP_SEQ)
		{
		    iEquip = LVD_GROUP_START + k;
		}
		iError = DxiGetData(VAR_A_EQUIP_INFO,
		    iEquip,
		    0,
		    &iBufLen,
		    &pEquipInfo,
		    0);
		if(iError != ERR_DXI_OK)
		{
		    TRACE_WEB_USER("Web_GetSettingSignalPointer error1[%d]\n", k);
		}
		if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE) && (m < iEquipNum))
		{
		    iError = DxiGetData(VAR_A_EQUIP_INFO,
			iEquip,
			0,
			&iBufLen,
			&gs_pWebSetInfo[i].stWebEquipInfo[m].pEquipInfo,
			0);
		    if(iError != ERR_DXI_OK)
		    {
			TRACE_WEB_USER("Web_GetSettingSignalPointer error2[%d]\n", k);
			gs_pWebSetInfo[i].stWebEquipInfo[m].bSingleValid = FALSE;
		    }
		    else
		    {
			gs_pWebSetInfo[i].stWebEquipInfo[m].bSingleValid = TRUE;
			for(l = 0; l < gs_pWebSetInfo[i].iNumber; l++)	// for every signal in EIB
			{
			    gs_pWebSetInfo[i].stWebPrivateSet1[n].iEquipID = iEquip;
			    gs_pWebSetInfo[i].stWebPrivateSet1[n].iSignalType = gs_pWebSetInfo[i].stWebPrivateSet[l].iSignalType;
			    gs_pWebSetInfo[i].stWebPrivateSet1[n].iSignalID = gs_pWebSetInfo[i].stWebPrivateSet[l].iSignalID;
			    gs_pWebSetInfo[i].stWebPrivateSet1[n].bWorkStatus = TRUE;
			    gs_pWebSetInfo[i].stWebPrivateSet1[n].pEquipInfo = gs_pWebSetInfo[i].stWebEquipInfo[m].pEquipInfo;
			    iVarSubID = DXI_MERGE_SIG_ID(gs_pWebSetInfo[i].stWebPrivateSet[l].iSignalType, gs_pWebSetInfo[i].stWebPrivateSet[l].iSignalID);
			    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				gs_pWebSetInfo[i].stWebPrivateSet1[n].iEquipID,
				iVarSubID,
				&iBufLen,
				(void *)&gs_pWebSetInfo[i].stWebPrivateSet1[n].pSigValue,
				iTimeOut);
			    if(iError != ERR_DXI_OK)
			    {
				//TRACE_WEB_USER("get rectifier signal error[%d][%d]\n", k, m);
				gs_pWebSetInfo[i].stWebPrivateSet1[n].bSetVaild = FALSE;
			    }
			    else
			    {
				gs_pWebSetInfo[i].stWebPrivateSet1[n].bSetVaild = TRUE;
			    }
			    n++;
			}
		    }
		    m++;
		}
	    }
	}
	
	if(i == LVD_SEQ)	//init the LVD equip
	{
	    m = 0;
	    TRACE_WEB_USER("gs_uiRealLVDNum is %d\n", gs_uiRealLVDNum);
	    if(s_uiCfgQtyOfSetLVD != gs_uiRealLVDNum)
	    {
		s_uiCfgQtyOfSetLVD = gs_uiRealLVDNum;
		if(gs_pWebSetInfo[i].stWebEquipInfo != NULL)
		{
		    DELETE(gs_pWebSetInfo[i].stWebEquipInfo);
		    gs_pWebSetInfo[i].stWebEquipInfo = NULL;
		}
		gs_pWebSetInfo[i].stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, gs_uiRealLVDNum);
		if(gs_pWebSetInfo[i].stWebEquipInfo == NULL)
		{
		    AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
		    return 1;
		}
	    }
	    for(k = 0; k < LVD_NUM; k++)
	    {
		if(k < (LVD_NUM - 1))
		{
		    iEquip = LVD_EQUIP_START + k;
		}
		else
		{
		    iEquip = LVD3_EQUIP;
		}
		iError = DxiGetData(VAR_A_EQUIP_INFO,
		    iEquip,			
		    0,		
		    &iBufLen,			
		    &pEquipInfo,			
		    0);
		if(iError != ERR_DXI_OK)
		{
		    TRACE_WEB_USER("Web_GetSettingSignalPointer error1[%d]\n", k);
		    //AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
		    //return (iError);
		}
		if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE) && (m < gs_uiRealLVDNum))
		{
		    iError = DxiGetData(VAR_A_EQUIP_INFO,
			iEquip,
			0,
			&iBufLen,
			&gs_pWebSetInfo[i].stWebEquipInfo[m].pEquipInfo,
			0);
		    if(iError != ERR_DXI_OK)
		    {
			TRACE_WEB_USER("Web_GetSettingSignalPointer error2[%d]\n", k);
			//return (iError);
			//AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
			gs_pWebSetInfo[i].stWebEquipInfo[m].bSingleValid = FALSE;
		    }
		    else
		    {
			gs_pWebSetInfo[i].stWebEquipInfo[m].bSingleValid = TRUE;
		    }
		    m++;
		}
	    }
	}
	if(i == SINGLE_RECT_SEQ)
	{
	    //TRACE_WEB_USER("gs_uiRectifierNumber is %d\n", gs_uiRectifierNumber);
		if(s_uiSetRectifierNum != gs_uiRectifierNumber)
		{
		    s_uiSetRectifierNum = gs_uiRectifierNumber;
		    if(gs_pWebSetInfo[i].stWebPrivateSet1 != NULL)
		    {
			DELETE(gs_pWebSetInfo[i].stWebPrivateSet1);
			gs_pWebSetInfo[i].stWebPrivateSet1 = NULL;
		    }
		    gs_pWebSetInfo[i].stWebPrivateSet1 = NEW(WEB_GENERAL_SETTING_INFO, (gs_uiRectifierNumber * gs_pWebSetInfo[i].iNumber));
		    if(gs_pWebSetInfo[i].stWebPrivateSet1 == NULL)
		    {
			AppLogOut("Web_GetSettingSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
			// here we can't delete gs_pWebSetInfo[i].stWebEquipInfo for we will use it in other function
			return 1;
		    }
		}
	    n = 0;// for signals
	    for(k = 0; k < gs_uiRectifierNumber; k++) // for every rectifier
	    {
		for(m = 0; m < gs_pWebSetInfo[i].iNumber; m++)	// for every signal in rectifier
		{
		    if(k < RECT_FIRST_RANGE_NUM)		// the first range is from 211 to 258, the second range is from 1400 to 1411
		    {
			gs_pWebSetInfo[i].stWebPrivateSet1[n].iEquipID = gs_pWebSetInfo[i].stWebPrivateSet[m].iEquipID + k;
		    }
		    else
		    {
			gs_pWebSetInfo[i].stWebPrivateSet1[n].iEquipID = RECT_SECOND_RANGE_START + k - RECT_FIRST_RANGE_NUM;
		    }
		    //gs_pWebSetInfo[i].stWebPrivateSet1[n].iEquipID = gs_pWebSetInfo[i].stWebPrivateSet[m].iEquipID + k;
		    gs_pWebSetInfo[i].stWebPrivateSet1[n].iSignalID = gs_pWebSetInfo[i].stWebPrivateSet[m].iSignalID;
		    gs_pWebSetInfo[i].stWebPrivateSet1[n].iSignalType = gs_pWebSetInfo[i].stWebPrivateSet[m].iSignalType;
		    iVarSubID = DXI_MERGE_SIG_ID(gs_pWebSetInfo[i].stWebPrivateSet[m].iSignalType, gs_pWebSetInfo[i].stWebPrivateSet[m].iSignalID);
		    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			gs_pWebSetInfo[i].stWebPrivateSet1[n].iEquipID,
			iVarSubID,
			&iBufLen,
			(void *)&gs_pWebSetInfo[i].stWebPrivateSet1[n].pSigValue,
			iTimeOut);
		    if(iError != ERR_DXI_OK)
		    {
			TRACE_WEB_USER("get rectifier signal error[%d][%d]\n", k, m);
			gs_pWebSetInfo[i].stWebPrivateSet1[n].bSetVaild = FALSE;
		    }
		    else
		    {
			gs_pWebSetInfo[i].stWebPrivateSet1[n].bSetVaild = TRUE;
		    }
		    iError = DxiGetData(VAR_A_EQUIP_INFO,
			gs_pWebSetInfo[i].stWebPrivateSet1[n].iEquipID,			
			0,		
			&iBufLen,			
			&pEquipInfo,			
			0);
		    if(iError != ERR_DXI_OK)
		    {
			TRACE_WEB_USER("get rectifier equip error[%d][%d]\n", k, m);
			gs_pWebSetInfo[i].stWebPrivateSet1[n].bWorkStatus = FALSE;
		    }
		    else
		    {
			gs_pWebSetInfo[i].stWebPrivateSet1[n].bWorkStatus = TRUE;
			gs_pWebSetInfo[i].stWebPrivateSet1[n].pEquipInfo = pEquipInfo;
		    }
		    n++;
		}
	    }
	}

	//changed by Frank Wu,21/14/30,20140527, for add single converter and single solar settings pages
	SETTING_SIGNAL_POINTER_PROC( SINGLE_CONVERTER_SEQ,
		SINGLE_CONVERTER_START_1,
		SINGLE_CONVERTER_NUM_1,
		SINGLE_CONVERTER_START_2,
		0,
		TRUE,
		s_uiSetSingleConverterNum,
		gs_uiConverterNumber);

	SETTING_SIGNAL_POINTER_PROC( SINGLE_SOLAR_SEQ,
		SINGLE_SOLAR_START_1,
		0,
		0,
		0,
		TRUE,
		s_uiSetSingleSolarNum,
		gs_uiSolarNumber);

	SETTING_SIGNAL_POINTER_PROC( SINGLE_RECT_S1_SEQ,
		SINGLE_RECT_S1_START_1,
		0,
		0,
		0,
		TRUE,
		s_uiSetSingleRectS1Num,
		gs_uiRectifierS1Number);

	SETTING_SIGNAL_POINTER_PROC( SINGLE_RECT_S2_SEQ,
		SINGLE_RECT_S2_START_1,
		0,
		0,
		0,
		TRUE,
		s_uiSetSingleRectS2Num,
		gs_uiRectifierS2Number);

	SETTING_SIGNAL_POINTER_PROC( SINGLE_RECT_S3_SEQ,
		SINGLE_RECT_S3_START_1,
		0,
		0,
		0,
		TRUE,
		s_uiSetSingleRectS3Num,
		gs_uiRectifierS3Number);

	SETTING_SIGNAL_POINTER_PROC( COMM_BATT_SEQ,
	    COMM_BATT_UNIT_EQUIP_START,
	    SINGLE_COMM_BATT_NUM_1,
	    0,
	    0,
		FALSE,
	    s_uiCfgQtyOfSetCommBattUnitNum,
	    gs_uiRealComBattNum);

	SETTING_SIGNAL_POINTER_PROC( EIB_BATT_SEQ1,
	    EIB_BATT_UNIT_EQUIP_START,
	    SINGLE_EIB_BATT_NUM_1,
	    EIB_BATT_UNIT_EQUIP_START_2,
	    SINGLE_EIB_BATT_NUM_2,
		FALSE,
	    s_uiCfgQtyOfSetEIBBattUnitNum,
	    gs_uiRealEIBBattNum);

	//TRACE_WEB_USER_NOT_CYCLE("gs_uiRealSMDUBattNum is %d\n", gs_uiRealSMDUBattNum);

	SETTING_SIGNAL_POINTER_PROC( SMDU_BATT_SEQ1,
	    SMDU_BATT_UNIT_EQUIP_START,
	    SINGLE_SMDU_BATT_NUM_1,
	    SMDU_BATT_UNIT_EQUIP_START_2,
	    SINGLE_SMDU_BATT_NUM_2,
		FALSE,
	    s_uiCfgQtyOfSetSMDUBattUnitNum,
	    gs_uiRealSMDUBattNum);

	SETTING_SIGNAL_POINTER_PROC( SMDUE_BATT_SEQ1,
	    SMDUE_BATT_UNIT_EQUIP_START,
	    SINGLE_SMDUE_BATT_NUM,
	    0,
	    0,
		FALSE,
	    s_uiCfgQtyOfSetSMDUEBattUnitNum,
	    gs_uiRealSMDUEBattNum);    

	SETTING_SIGNAL_POINTER_PROC( SM_BATT_SEQ1,
	    SM_BATT_UNIT_EQUIP_START,
	    SINGLE_SM_BATT_NUM_1,
	    0,
	    0,
		FALSE,
	    s_uiCfgQtyOfSetSMBatUnitNum,
	    gs_uiRealSMBattNum);

	SETTING_SIGNAL_POINTER_PROC( SMBRC_BATT_SEQ1,
	    SMBRC_BATT_UNIT_EQUIP_START,
	    SINGLE_SMBRC_BATT_NUM_1,
	    0,
	    0,
		FALSE,
	    s_uiCfgQtyOfSetSMBRCBatUnitNum,
	    gs_uiRealSMBRCBattNum);

	SETTING_SIGNAL_POINTER_PROC( SONICK_BATT_SEQ1,
	    SONICK_BATT_UNIT_EQUIP_START,
	    SINGLE_SONICK_BATT_NUM,
	    0,
	    0,
	    FALSE,
	    s_uiCfgQtyOfSetSonickBatUnitNum,
	    gs_uiRealSoNickBattNum);

	SETTING_SIGNAL_POINTER_PROC( NARADA_BMS_SEQ1,
	    NARADA_BMS_EQUIP_START,
	    MAX_NARADA_BMS_NUM,
	    0,
	    0,
	    FALSE,
	    s_uiCfgQtyOfSetNaradaBMSUnitNum,
	    gs_uiNaradaBMSNum);

	SETTING_SIGNAL_POINTER_PROC( NARADA_BMS_GROUP_SEQ1,
		NARADA_BMS_GROUP_START,
		0,
		0,
		0,
		TRUE,
		s_uiCfgQtyOfSetNaradaBMSGroupNum,
		gs_uiRealNaradaBMSGroupNum);

    }

    TRACE_WEB_USER("end to Exec Web_GetSettingSignalPointer\n");
    return ERR_DXI_OK;
}

/*==========================================================================*
* FUNCTION :  Web_GetGroupSignalPointer
* PURPOSE  :  Initial the pointer pointing to the group signals
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY:     Zhao Zicheng               DATE: 2014-03-06
*==========================================================================*/
int Web_GetGroupSignalPointer(int itype)
{
    int i;
    int	iError = 0;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;

    for(i = 0; i < gs_pWebSigInfo[itype].iNumber; i++)
    {
	iVarSubID = DXI_MERGE_SIG_ID(gs_pWebSigInfo[itype].stWebPrivate[i].iSignalType, gs_pWebSigInfo[itype].stWebPrivate[i].iSignalID);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    gs_pWebSigInfo[itype].stWebPrivate[i].iEquipID,
	    iVarSubID,
	    &iBufLen,
	    (void *)&gs_pWebSigInfo[itype].stWebPrivate[i].pSigValue,
	    iTimeOut);
	if(iError != ERR_DXI_OK)
	{
	    TRACE_WEB_USER("iEquipID is %d, iSignalType is %d, iSignalID is %d\n",
		gs_pWebSigInfo[itype].stWebPrivate[i].iEquipID, gs_pWebSigInfo[itype].stWebPrivate[i].iSignalType,
		gs_pWebSigInfo[itype].stWebPrivate[i].iSignalID);
	    TRACE_WEB_USER("[%d] Web_GetGroupSignalPointer error iError is %d\n", i, iError);
	    /*AppLogOut("Web_GetGroupSignalPointer", APP_LOG_ERROR, "Can't get the signal(%d,%d,%d)\n", gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iEquipID,
		gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iSignalType, gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].iSignalID);*/
	    gs_pWebSigInfo[itype].stWebPrivate[i].bValid = FALSE;
	    //return (iError);
	}
	else
	{
	    gs_pWebSigInfo[itype].stWebPrivate[i].bValid = TRUE;
	}
    }
    return ERR_DXI_OK;
}

/*==========================================================================*
* FUNCTION :  Web_GetRectSignalPointer
* PURPOSE  :  Initial the pointer pointing to rectifier or converter or MPPT signal value
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  itype: indicate the kind of rectifier
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng               DATE: 2014-03-07
*==========================================================================*/
int Web_GetRectSignalPointer(int itype)
{
#define FIRST_RANGE_NUM		48
#define SECOND_RANGE_START	1400
#define RECT_FIRST_RANGE_NUM	100
#define RECT_SECOND_RANGE_START		1601
    TRACE_WEB_USER("Exec Web_GetRectSignalPointer[%d] begin\n", itype);
    int i, j, k, iSingleRectSignalNum, iOneEquipSignalNum, iSamNum;
    int	iError = 0;
    //SAMPLE_SIG_INFO	*pSampleSigInfo  = NULL;
    //int igroupNum;
    int iseqNum;
    int iSingleNum;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int iEquipSeq = 0, iEquipSeq1;
    int iInterfaceType;
    SAMPLE_SIG_VALUE	*pSampleSigValue = NULL;

    iInterfaceType = VAR_SAM_SIG_NUM_OF_EQUIP;

    if(itype == RECT_GROUP_SEQ)
    {
	iseqNum = 0;
	iSingleNum = gs_uiRectifierNumber;
	iEquipSeq = 3;
    }
    else if(itype == RECTS1_GROUP_SEQ)
    {
	iseqNum = 1;
	iSingleNum = gs_uiRectifierS1Number;
	iEquipSeq = 339;
    }
    else if(itype == RECTS2_GROUP_SEQ)
    {
	iseqNum = 2;
	iSingleNum = gs_uiRectifierS2Number;
	iEquipSeq = 439;
    }
    else if(itype == RECTS3_GROUP_SEQ)
    {
	iseqNum = 3;
	iSingleNum = gs_uiRectifierS3Number;
	iEquipSeq = 539;
    }
    else if(itype == CONVERTER_GROUP_SEQ)
    {
	iseqNum = 4;
	iSingleNum = gs_uiConverterNumber;
	iEquipSeq = 211;
    }
    else
    {
	iseqNum = 5;
	iSingleNum = gs_uiSolarNumber;
	iEquipSeq = 1351;
    }

    iError = DxiGetData(iInterfaceType,
	iEquipSeq,
	iVarSubID,
	&iBufLen,
	&iOneEquipSignalNum,
	iTimeOut);
    if(iError != ERR_DXI_OK)
    {
	TRACE_WEB_USER_NOT_CYCLE("Get equip sample signal number error!\n");
	iOneEquipSignalNum = 0;
    }
    iSamNum = iOneEquipSignalNum;
    iSingleRectSignalNum = iOneEquipSignalNum * iSingleNum;

    TRACE_WEB_USER("Exec single rectifier all signals\n");
    //initial single rectifier signal value pointer
    //iSingleRectSignalNum = iSingleNum * gs_pWebSigInfo[itype].iNumber;
    if(gs_pWebRectSigInfo[iseqNum] != NULL)
    {
	if(gs_pWebRectSigInfo[iseqNum]->stWebPrivate != NULL)
	{
	    DELETE(gs_pWebRectSigInfo[iseqNum]->stWebPrivate);
	    gs_pWebRectSigInfo[iseqNum]->stWebPrivate = NULL;
	}
	if(gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo != NULL)
	{
	    DELETE(gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo);
	    gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo = NULL;
	}
    }
    if(gs_pWebRectSigInfo[iseqNum] != NULL)
    {
	DELETE(gs_pWebRectSigInfo[iseqNum]);
	gs_pWebRectSigInfo[iseqNum] = NULL;
    }
    gs_pWebRectSigInfo[iseqNum] = NEW(WEB_PRIVATE_RECT_SIGNAL_INFO, 1);
    if(gs_pWebRectSigInfo[iseqNum] == NULL)
    {
	AppLogOut("Web_GetRectSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    gs_pWebRectSigInfo[iseqNum]->iNumber = iSingleRectSignalNum;
    gs_pWebRectSigInfo[iseqNum]->iOneEquipNum = iSamNum;

    gs_pWebRectSigInfo[iseqNum]->stWebPrivate = NEW(WEB_GENERAL_RECT_SIGNAL_INFO, iSingleRectSignalNum);
    if(gs_pWebRectSigInfo[iseqNum]->stWebPrivate == NULL)
    {
	if(gs_pWebRectSigInfo[iseqNum] != NULL)
	{
	    DELETE(gs_pWebRectSigInfo[iseqNum]);
	    gs_pWebRectSigInfo[iseqNum] = NULL;
	}
	AppLogOut("Web_GetRectSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, iSingleNum);
    if(gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo == NULL)
    {
	if(gs_pWebRectSigInfo[iseqNum] != NULL)
	{
	    DELETE(gs_pWebRectSigInfo[iseqNum]);
	    gs_pWebRectSigInfo[iseqNum] = NULL;
	}
	if(gs_pWebRectSigInfo[iseqNum]->stWebPrivate != NULL)
	{
	    DELETE(gs_pWebRectSigInfo[iseqNum]->stWebPrivate);
	    gs_pWebRectSigInfo[iseqNum]->stWebPrivate = NULL;
	}
	AppLogOut("Web_GetRectSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    TRACE_WEB_USER("Exec signal point\n");
    k = 0;
    for(i = 0; i < iSingleNum; i++)// for every rectifier
    {
	// initial the equip structure pointer iEquipSeq
	if(itype == CONVERTER_GROUP_SEQ)
	{
	    if(i < FIRST_RANGE_NUM)		// the first range is from 211 to 258, the second range is from 1400 to 1411
	    {
		iEquipSeq1 = iEquipSeq + i;
	    }
	    else
	    {
		iEquipSeq1 = SECOND_RANGE_START + i - FIRST_RANGE_NUM;
	    }
	}
	else if(itype == RECT_GROUP_SEQ)
	{
	    if(i < RECT_FIRST_RANGE_NUM)		// the first range is from 211 to 258, the second range is from 1400 to 1411
	    {
		iEquipSeq1 = iEquipSeq + i;
	    }
	    else
	    {
		iEquipSeq1 = RECT_SECOND_RANGE_START + i - RECT_FIRST_RANGE_NUM;
	    }
	}
	else		// for solar and S1 S2 S3
	{
	    iEquipSeq1 = iEquipSeq + i;
	}
	iError = DxiGetData(VAR_A_EQUIP_INFO,
	    iEquipSeq1,
	    0,
	    &iBufLen,
	    &gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo[i].pEquipInfo,
	    0);
	if(iError != ERR_DXI_OK)
	{
	    TRACE_WEB_USER("Web_GetRectSignalPointer error2[%d]\n", i);
	    //AppLogOut("Web_GetRectSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquipSeq);
	    gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo[i].bSingleValid = FALSE;
	    //return (iError);
	}
	else
	{
	    gs_pWebRectSigInfo[iseqNum]->stWebEquipInfo[i].bSingleValid = TRUE;
	}
	// initial every signal pointer
	Web_GetSampleSignalByEquipID(iEquipSeq1, &pSampleSigValue);
	for(j = 0; j < iOneEquipSignalNum && pSampleSigValue != NULL; j++, pSampleSigValue++)// for every signal in a rectifier
	{
	    gs_pWebRectSigInfo[iseqNum]->stWebPrivate[k].pSigValue = &(pSampleSigValue->bv);
	    gs_pWebRectSigInfo[iseqNum]->stWebPrivate[k].bSingleValid = TRUE;
	    k++;
	}
    }
    TRACE_WEB_USER("Exec Web_GetRectSignalPointer[%d] end\n", itype);
    return ERR_DXI_OK;
}

/*==========================================================================*
* FUNCTION :  Web_GetSampleSignalByEquipID
* PURPOSE  :   
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iEquipID:
* OUT SAMPLE_SIG_VALUE **ppSigValue:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
*==========================================================================*/
static int Web_GetSampleSignalByEquipID(IN int iEquipID, 
					OUT SAMPLE_SIG_VALUE **ppSigValue)
{
    SAMPLE_SIG_VALUE *pSigValue = NULL;
    int				iSignalNum = 0;
    int				iVarSubID = 0;
    int				iBufLen = 0;
    int				iTimeOut = 0;

    int				iError = DxiGetData(VAR_SAM_SIG_VALUE_OF_EQUIP,
	iEquipID,			
	iVarSubID,		
	&iBufLen,			
	(void*)&pSigValue,			
	iTimeOut);

    iSignalNum = iBufLen /sizeof(SAMPLE_SIG_VALUE);

    if(iError == ERR_DXI_OK)
    {
	*ppSigValue = pSigValue;
	return iSignalNum;
    }
    else
    {
	return FALSE;
    }

}

/*==========================================================================*
* FUNCTION :  Web_GetSingleOtherSignalPointer
* PURPOSE  :  Initial the pointer pointing to other equip
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  indicate the kind of equip
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
int Web_GetSingleOtherSignalPointer(int itype)
{
    int i, j, k, m, iAllSignalNum, iEquipNum, iOneEquipSignalNum, iMaxEquipNum, iMaxEquipNum1;
    int	iError = 0;
    //SAMPLE_SIG_INFO	*pSampleSigInfo  = NULL;
    int iEquip, iEquip1;
    EQUIP_INFO		*pEquipInfo;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int iInterfaceType;
    SAMPLE_SIG_VALUE	*pSampleSigValue = NULL;
    int iSamNum, iSMIO_SMDUPSamNum[20];//8,

    TRACE_WEB_USER("Begin to exe Web_GetSingleOtherSignalPointer[%d]\n", itype);

    iInterfaceType = VAR_SAM_SIG_NUM_OF_EQUIP;
    iAllSignalNum = 0;
    if(itype == SMAC_SEQ)
    {
	iMaxEquipNum = SMAC_NUM;
	iEquipNum = gs_uiSMACNum;
	iEquip = 259;
    }
    else if(itype == DG_SEQ)
    {
	iMaxEquipNum = 1;
	iEquipNum = gs_uiDGNum;
	iEquip = 262;
    }
    else if(itype == ACMETER_SEQ)
    {
	iMaxEquipNum = SMAC_NUM;
	iEquipNum = gs_uiACMeterNum;
	iEquip = 3501;
    }
    else if(itype == DCMETER_SEQ)
    {
	iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM;
	iEquipNum = gs_uiDCMeterNum;
	iEquip = 718;
    }
    else if(itype == NARADA_BMS_SEQ)
    {
	iMaxEquipNum = MAX_NARADA_BMS_NUM;
	iEquipNum = gs_uiNaradaBMSNum;
	iEquip = 3580;
    }
    else if(itype == DC_BOARD_SEQ)
    {
	iMaxEquipNum = 1;
	iEquipNum = 1;		// the number of DC group is always 1
	iEquip = 176;
    }
    else if(itype == SMDU_SEQ)
    {
	iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM;
	iEquipNum = gs_uiRealSMDUNum;
	iEquip = 107;
    }
    else if(itype == BATTERY_FUSE_GROUP_SEQ)
    {
	iMaxEquipNum = GROUP_NUM;
	iEquipNum = gs_uiRealBattFuseGroupNum;
	iEquip = 158;
    }
    else if(itype == BATTERY_FUSE_SEQ)
    {
	iMaxEquipNum = GROUP_NUM;
	iEquipNum = gs_uiRealBattFuseNum;
	iEquip = 159;
    }
    else if(itype == LVD_GROUP_SEQ1)
    {
	iMaxEquipNum = GROUP_NUM;
	iEquipNum = gs_uiRealLVDGroupNum;
	iEquip = 186;
    }
    else if(itype == LVD_SEQ1)
    {
	iMaxEquipNum = GROUP_NUM;
	iEquipNum = gs_uiRealLVDUnitNum;
	iEquip = 187;
    }
    else if(itype == LVD_SEQ2)
    {
	iMaxEquipNum = GROUP_NUM;
	iEquipNum = gs_uiRealLVD3UnitNum;
	iEquip = 3510;
    }
    else if(itype == SMDU_BATT_FUSE_SEQ)
    {
	iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM;
	iEquipNum = gs_uiRealSMDUBattFuseNum;
	iEquip = 160;
    }
    else if(itype == SMDU_LVD_SEQ)
    {
	iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM;
	iEquipNum = gs_uiRealSMDULVDUnitNum;
	iEquip = 188;
    }
    else if(itype == SMDUP_SEQ)
    {
	iMaxEquipNum1 = SMDU_SMDUP_SMDUH_NUM1+8;//EquipID�ֶΣ�643-650��1870-1877
	iEquipNum = gs_uiRealSMDUPNum;
	iEquip = 643;
    }
    else if(itype == SMDUH_SEQ)
    {
	iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM;
	iEquipNum = gs_uiRealSMDUHNum;
	iEquip = 1702;
    }
    else if(itype == SMDUE_SEQ)
    {
	iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM;
	iEquipNum = gs_uiRealSMDUENum;
	iEquip = 1801;
    }
    else if(itype == EIB_SEQ)
    {
	iMaxEquipNum = EIB_NUM;
	iEquipNum = gs_uiRealEIBNum;
	iEquip = 197;
    }
    else if(itype == SMIO_SEQ)
    {
	iMaxEquipNum1 = SMIO_NUM;
	iEquipNum = gs_uiSMIONum;
	iEquip = 266;	//each smio has different sample signal
    }
    else if(itype == ACUNIT_SEQ)
	{
		iEquip = 104;	
		iEquipNum=1;
	}
    // get the equip signal number
    if((itype != SMIO_SEQ) && (itype != SMDUP_SEQ))
    {
		iError = DxiGetData(iInterfaceType,
		    iEquip,
		    iVarSubID,
		    &iBufLen,
		    &iOneEquipSignalNum,
		    iTimeOut);
		if(iError != ERR_DXI_OK)
		{
		    TRACE_WEB_USER_NOT_CYCLE("Get equip sample signal number error!\n");
		    iOneEquipSignalNum = 0;
		}
		iSamNum = iOneEquipSignalNum;
		iAllSignalNum = iOneEquipSignalNum * iEquipNum;
    }
    else
    {
		m = 0;
		if( SMDUP_SEQ == itype )//EquipID�ֶΣ�643-650��1870-1877
		{
			for(i = 0; i < iMaxEquipNum1; i++)
			{
				if( i < 8 )
				{
					iEquip1 = iEquip+i;	
				}
				else
				{
					iEquip1 = 1870 + i-8;	
				}
				
				iError = DxiGetData(VAR_A_EQUIP_INFO,
					iEquip1,
					0,
					&iBufLen,
					&pEquipInfo,
					0);
				if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE))
				{
					iError = DxiGetData(iInterfaceType,
						iEquip1,
						iVarSubID,
						&iBufLen,
						&iOneEquipSignalNum,
						iTimeOut);
					if(iError == ERR_DXI_OK)
					{
						iSMIO_SMDUPSamNum[m] = iOneEquipSignalNum;
						iAllSignalNum += iOneEquipSignalNum;
						m++;
					}
					else
					{
						TRACE_WEB_USER_NOT_CYCLE("Get equip sample signal number error!\n");
						iAllSignalNum = 0;
						break;
					}
				}
			}
		}
		else
		{
			for(i = 0; i < iMaxEquipNum1; i++)// at most 6 SMIO 
			{
			    iError = DxiGetData(VAR_A_EQUIP_INFO,
				iEquip + i,
				0,
				&iBufLen,
				&pEquipInfo,
				0);
			    if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE))
			    {
				iError = DxiGetData(iInterfaceType,
				    iEquip + i,
				    iVarSubID,
				    &iBufLen,
				    &iOneEquipSignalNum,
				    iTimeOut);
				if(iError == ERR_DXI_OK)
				{
				    iSMIO_SMDUPSamNum[m] = iOneEquipSignalNum;
				    iAllSignalNum += iOneEquipSignalNum;
				    m++;
				}
				else
				{
				    TRACE_WEB_USER_NOT_CYCLE("Get equip sample signal number error!\n");
				    iAllSignalNum = 0;
				    break;
				}
			    }
			}
		}
    }
    if(iAllSignalNum == 0)
    {
	TRACE_WEB_USER_NOT_CYCLE("No equip signal\n");
	return ERR_DXI_OK;
    }
    TRACE_WEB_USER("Create the other equip variable\n");
    //Create the other equip variable
    if(gs_pWebOtherSigInfo[itype] != NULL)
    {
	if(gs_pWebOtherSigInfo[itype]->stWebPrivate != NULL)
	{
	    DELETE(gs_pWebOtherSigInfo[itype]->stWebPrivate);
	    gs_pWebOtherSigInfo[itype]->stWebPrivate = NULL;
	}
	if(gs_pWebOtherSigInfo[itype]->stWebEquipInfo != NULL)
	{
	    DELETE(gs_pWebOtherSigInfo[itype]->stWebEquipInfo);
	    gs_pWebOtherSigInfo[itype]->stWebEquipInfo = NULL;
	}
    }
    if(gs_pWebOtherSigInfo[itype] != NULL)
    {
	DELETE(gs_pWebOtherSigInfo[itype]);
	gs_pWebOtherSigInfo[itype] = NULL;
    }
    TRACE_WEB_USER("iAllSignalNum is %d	iEquipNum is %d\n", iAllSignalNum, iEquipNum);
    gs_pWebOtherSigInfo[itype] = NEW(WEB_PRIVATE_RECT_SIGNAL_INFO, 1);
    if(gs_pWebOtherSigInfo[itype] == NULL)
    {
	AppLogOut("Web_GetSingleOtherSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }
    gs_pWebOtherSigInfo[itype]->iNumber = iAllSignalNum;
    // initial the signal space
    gs_pWebOtherSigInfo[itype]->stWebPrivate = NEW(WEB_GENERAL_RECT_SIGNAL_INFO, iAllSignalNum);
    if(gs_pWebOtherSigInfo[itype]->stWebPrivate == NULL)
    {
	if(gs_pWebOtherSigInfo[itype] != NULL)
	{
	    DELETE(gs_pWebOtherSigInfo[itype]);
	    gs_pWebOtherSigInfo[itype] = NULL;
	}
	AppLogOut("Web_GetSingleOtherSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    //initial the equip space
    gs_pWebOtherSigInfo[itype]->stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, iEquipNum);
    if(gs_pWebOtherSigInfo[itype]->stWebEquipInfo == NULL)
    {
	if(gs_pWebOtherSigInfo[itype] != NULL)
	{
	    DELETE(gs_pWebOtherSigInfo[itype]);
	    gs_pWebOtherSigInfo[itype] = NULL;
	}
	if(gs_pWebOtherSigInfo[itype]->stWebPrivate != NULL)
	{
	    DELETE(gs_pWebOtherSigInfo[itype]->stWebPrivate);
	    gs_pWebOtherSigInfo[itype]->stWebPrivate = NULL;
	}
	AppLogOut("Web_GetSingleOtherSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    TRACE_WEB_USER("initial the equip and signal pointer\n");
    m = 0;// for equip
    k = 0;// for signal
    if((itype != SMIO_SEQ) && (itype != SMDUP_SEQ))
    {
		for(i = 0; i < iMaxEquipNum; i++)
		{
		    if(itype == SMAC_SEQ)
		    {
				if(i < 2)
				{
				    iEquip1 = iEquip + i;
				}
				else
				{
			   		 iEquip1 = iEquip + i + 706 - 259 - 2;
				}
		    }
		    else if(itype == SMDU_BATT_FUSE_SEQ)
		    {
				if(i < 3)
				{
				    iEquip1 = iEquip + i;
				}
				else
				{
				    iEquip1 = 171 + i - 3;
				}
		    }
		    else
		    {
				iEquip1 = iEquip + i;
		    }
		    iError = DxiGetData(VAR_A_EQUIP_INFO,
								iEquip1,
								0,
								&iBufLen,
								&pEquipInfo,
								0);
		    TRACE_WEB_USER("iError is %d\n", iError);
		    if(iError != ERR_DXI_OK)
		    {
				TRACE_WEB_USER("Web_GetSingleOtherSignalPointer error1[%d]\n", i);
		    }
		    TRACE_WEB_USER("iError is %d bWorkStatus is %d m is %d iEquipNum is %d\n", iError, pEquipInfo->bWorkStatus, m, iEquipNum);
		    if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE) && (m < iEquipNum))
		    {
				TRACE_WEB_USER("Run here 1\n");
				iError = DxiGetData(VAR_A_EQUIP_INFO,
				    iEquip1,
				    0,
				    &iBufLen,
				    &gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].pEquipInfo,
				    0);
				if(iError != ERR_DXI_OK)
				{
				    TRACE_WEB_USER("Initial equip error[%d]\n", i);
				    //AppLogOut("Web_GetSingleDCSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].bSingleValid = FALSE;
				    //return (iError);
				}
				else
				{
				    TRACE_WEB_USER("Run here 2\n");
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].iSamplenum = iSamNum;// save the sample number
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].bSingleValid = TRUE;
				}
				// initial the signal pointer
				Web_GetSampleSignalByEquipID(iEquip1, &pSampleSigValue);
				TRACE_WEB_USER("iOneEquipSignalNum is %d\n", iOneEquipSignalNum);
				for(j = 0; j < iOneEquipSignalNum && pSampleSigValue != NULL; j++, pSampleSigValue++)
				{
				    gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue = &(pSampleSigValue->bv);
				    gs_pWebOtherSigInfo[itype]->stWebPrivate[k].bSingleValid = TRUE;
				    k++;
				}
				m++;
				if(m >= iEquipNum)
				{
				    break;
				}
		    }
		}
	 }
    else
    {
		for(i = 0; i < iMaxEquipNum1; i++)
		{
		    TRACE_WEB_USER("the equip signal num is %d\n", iSMIO_SMDUPSamNum[i]);
		}
		if( SMDUP_SEQ == itype )//EquipID�ֶΣ�643-650��1870-1877
		{
			for(i = 0; i < iMaxEquipNum1; i++)
			{
			    if( i < 8 )
			    {
				iEquip1 = iEquip+i;	
			    }
			    else
			    {
				iEquip1 = 1870 + i-8;	
			    }
			
			    iError = DxiGetData(VAR_A_EQUIP_INFO,
				iEquip1,
				0,
				&iBufLen,
				&pEquipInfo,
				0);
			    if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE) && (m < iEquipNum))
			    {
				TRACE_WEB_USER("Initial the %d equip\n", (iEquip + i));
				iError = DxiGetData(VAR_A_EQUIP_INFO,
				    iEquip1,
				    0,
				    &iBufLen,
				    &gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].pEquipInfo,
				    0);
				if(iError != ERR_DXI_OK)
				{
				    TRACE_WEB_USER("Initial equip error[%d]\n", i);
				    //AppLogOut("Web_GetSingleDCSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].bSingleValid = FALSE;
				    //return (iError);
				}
				else
				{
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].iSamplenum = iSMIO_SMDUPSamNum[m];
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].bSingleValid = TRUE;
				}
				// initial the signal pointer
				Web_GetSampleSignalByEquipID(iEquip1, &pSampleSigValue);
				for(j = 0; (j < gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].iSamplenum && pSampleSigValue != NULL); j++, pSampleSigValue++)
				{
				    TRACE_WEB_USER("Initial the %d signal\n", k);
				    gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue = &(pSampleSigValue->bv);
				    gs_pWebOtherSigInfo[itype]->stWebPrivate[k].bSingleValid = TRUE;
				    k++;
				}
				m++;
			    }
			}
		}
		else
		{
			for(i = 0; i < iMaxEquipNum1; i++)
			{
			    iError = DxiGetData(VAR_A_EQUIP_INFO,
				iEquip + i,
				0,
				&iBufLen,
				&pEquipInfo,
				0);
			    if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE) && (m < iEquipNum))
			    {
				TRACE_WEB_USER("Initial the %d equip\n", (iEquip + i));
				iError = DxiGetData(VAR_A_EQUIP_INFO,
				    iEquip + i,
				    0,
				    &iBufLen,
				    &gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].pEquipInfo,
				    0);
				if(iError != ERR_DXI_OK)
				{
				    TRACE_WEB_USER("Initial equip error[%d]\n", i);
				    //AppLogOut("Web_GetSingleDCSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].bSingleValid = FALSE;
				    //return (iError);
				}
				else
				{
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].iSamplenum = iSMIO_SMDUPSamNum[m];
				    gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].bSingleValid = TRUE;
				}
				// initial the signal pointer
				Web_GetSampleSignalByEquipID(iEquip + i, &pSampleSigValue);
				for(j = 0; (j < gs_pWebOtherSigInfo[itype]->stWebEquipInfo[m].iSamplenum && pSampleSigValue != NULL); j++, pSampleSigValue++)
				{
				    TRACE_WEB_USER("Initial the %d signal\n", k);
				    gs_pWebOtherSigInfo[itype]->stWebPrivate[k].pSigValue = &(pSampleSigValue->bv);
				    gs_pWebOtherSigInfo[itype]->stWebPrivate[k].bSingleValid = TRUE;
				    k++;
				}
				m++;
			    }
			}
		}
    }
    
    TRACE_WEB_USER("end to exe Web_GetSingleOtherSignalPointer[%d]\n", itype);
    return ERR_DXI_OK;
}

/*==========================================================================*
* FUNCTION :  Web_GetSingleDCSignalPointer
* PURPOSE  :  Initial the pointer pointing to single DC value
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  indicate the kind of DC
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
int Web_GetSingleDCSignalPointer(int itype)
{
    int i, j, k, m, iSingleDCSignalNum, iSingleDCNum, iSingleSignalNum;
    int	iError = 0;
    //SAMPLE_SIG_INFO	*pSampleSigInfo  = NULL;
    int iEquip;
    EQUIP_INFO		*pEquipInfo;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int iMaxEquipNum = 0;
    int itype1;

    TRACE_WEB_USER("Begin to exe Web_GetSingleDCSignalPointer[%d]\n", itype);

    /*if(itype == SMDU_SEQ)
    {
	itype1 = DC_SMDU_SEQ;
	iSingleDCNum = gs_uiRealSMDUNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_SMDU_SEQ].iNumber;
    }
    else if(itype == SMDUP_SEQ)
    {
	itype1 = DC_SMDUP_SEQ;
	iSingleDCNum = gs_uiRealSMDUPNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_SMDUP_SEQ].iNumber;
    }
    else */if(itype == SMDUP1_SEQ)
    {
	itype1 = DC_SMDUP1_SEQ;
	iSingleDCNum = gs_uiRealSMDUPNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_SMDUP1_SEQ].iNumber;
    }
    /*else if(itype == SMDUH_SEQ)
    {
	itype1 = DC_SMDUH_SEQ;
	iSingleDCNum = gs_uiRealSMDUHNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_SMDUH_SEQ].iNumber;
    }
    else if(itype == EIB_SEQ)
    {
	itype1 = DC_EIB_SEQ;
	iSingleDCNum = gs_uiRealEIBNum;
	iSingleSignalNum = gs_pWebSigInfo[DC_EIB_SEQ].iNumber;
    }*/
    else
    {}
    if((iSingleDCNum == 0) && (iSingleSignalNum == 0))
    {
	return ERR_DXI_OK;
    }

    for(i = 0; i < iSingleSignalNum; i++)
    {
	iVarSubID = DXI_MERGE_SIG_ID(gs_pWebSigInfo[itype1].stWebPrivate[i].iSignalType, gs_pWebSigInfo[itype1].stWebPrivate[i].iSignalID);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	    gs_pWebSigInfo[itype1].stWebPrivate[i].iEquipID,
	    iVarSubID,
	    &iBufLen,
	    (void *)&gs_pWebSigInfo[itype1].stWebPrivate[i].pSigValue,
	    iTimeOut);
	if(iError != ERR_DXI_OK)
	{
	    TRACE_WEB_USER("Web_GetSingleDCSignalPointer error1\n");
	    /*AppLogOut("Web_GetRectSignalPointer", APP_LOG_ERROR, "Can't get the signal(%d,%d,%d)\n", gs_pWebSigInfo[itype].stWebPrivate[i].iEquipID,
	    gs_pWebSigInfo[itype].stWebPrivate[i].iSignalType, gs_pWebSigInfo[itype].stWebPrivate[i].iSignalID);*/
	    gs_pWebSigInfo[itype1].stWebPrivate[i].bValid = FALSE;
	    //return (iError);
	}
	else
	{
	    gs_pWebSigInfo[itype1].stWebPrivate[i].bValid = TRUE;
	}
    }

    iSingleDCSignalNum = iSingleDCNum * iSingleSignalNum;
    TRACE_WEB_USER("Exec single DC\n");
    //initial single DC signal value pointer
    if(gs_pWebDCSigInfo[itype] != NULL)
    {
	if(gs_pWebDCSigInfo[itype]->stWebPrivate != NULL)
	{
	    DELETE(gs_pWebDCSigInfo[itype]->stWebPrivate);
	    gs_pWebDCSigInfo[itype]->stWebPrivate = NULL;
	}
	if(gs_pWebDCSigInfo[itype]->stWebEquipInfo != NULL)
	{
	    DELETE(gs_pWebDCSigInfo[itype]->stWebEquipInfo);
	    gs_pWebDCSigInfo[itype]->stWebEquipInfo = NULL;
	}
    }
    if(gs_pWebDCSigInfo[itype] != NULL)
    {
	DELETE(gs_pWebDCSigInfo[itype]);
	gs_pWebDCSigInfo[itype] = NULL;
    }
    TRACE_WEB_USER("iSingleDCSignalNum is %d	iSingleDCNum is %d\n", iSingleDCSignalNum, iSingleDCNum);
    gs_pWebDCSigInfo[itype] = NEW(WEB_PRIVATE_RECT_SIGNAL_INFO, 1);
    if(gs_pWebDCSigInfo[itype] == NULL)
    {
	AppLogOut("Web_GetSingleDCSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }
    gs_pWebDCSigInfo[itype]->iNumber = iSingleDCSignalNum;
    gs_pWebDCSigInfo[itype]->stWebPrivate = NEW(WEB_GENERAL_RECT_SIGNAL_INFO, iSingleDCSignalNum);
    if(gs_pWebDCSigInfo[itype]->stWebPrivate == NULL)
    {
	if(gs_pWebDCSigInfo[itype] != NULL)
	{
	    DELETE(gs_pWebDCSigInfo[itype]);
	    gs_pWebDCSigInfo[itype] = NULL;
	}
	AppLogOut("Web_GetSingleDCSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    gs_pWebDCSigInfo[itype]->stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, iSingleDCNum);
    if(gs_pWebDCSigInfo[itype]->stWebEquipInfo == NULL)
    {
	if(gs_pWebDCSigInfo[itype] != NULL)
	{
	    DELETE(gs_pWebDCSigInfo[itype]);
	    gs_pWebDCSigInfo[itype] = NULL;
	}
	if(gs_pWebDCSigInfo[itype]->stWebPrivate != NULL)
	{
	    DELETE(gs_pWebDCSigInfo[itype]->stWebPrivate);
	    gs_pWebDCSigInfo[itype]->stWebPrivate = NULL;
	}
	AppLogOut("Web_GetSingleDCSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    TRACE_WEB_USER("Exec signal point\n");
    k = 0;// for signal
    m = 0;// for equip
    /*if(itype != EIB_SEQ)
    {*/
	if(itype == SMDUP1_SEQ)
	{
		iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM+12;//SMDUP��EquipID��Ϊ2�Σ�ÿ��8��
	}
	else
	{
		iMaxEquipNum = SMDU_SMDUP_SMDUH_NUM;	
	}
    /*}
    else
    {
	iMaxEquipNum = EIB_NUM;
    }*/
    for(i = 0; i < iMaxEquipNum; i++)
    {
	/*if(itype == SMDU_SEQ)
	{
	    iEquip = SMDU_EQUIP_START + i;
	}
	else */if(/*(itype == SMDUP_SEQ) || */(itype == SMDUP1_SEQ))
	{
		if( i < 8 )	
		{
			iEquip = SMDUP_EQUIP_START + i;
		}
		else
		{
			iEquip = 1870 + i-8;	
		}
	}
	/*else if(itype == SMDUH_SEQ)
	{
	    iEquip = SMDUH_EQUIP_START + i;
	}
	else if(itype == EIB_SEQ)
	{
	    iEquip = EIB_EQUIP_START + i;
	}*/
	else
	{}
	iError = DxiGetData(VAR_A_EQUIP_INFO,
	    iEquip,			
	    0,		
	    &iBufLen,			
	    &pEquipInfo,			
	    0);
	if(iError != ERR_DXI_OK)
	{
	    TRACE_WEB_USER("Web_GetSingleDCSignalPointer error1[%d]\n", i);
	    //AppLogOut("Web_GetSingleDCSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
	    //return (iError);
	}
	if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE) && (m < iSingleDCNum))
	{
	    //TRACE_WEB_USER("iEquip[%d] exist\n", iEquip);
	    iError = DxiGetData(VAR_A_EQUIP_INFO,
		iEquip,
		0,
		&iBufLen,
		&gs_pWebDCSigInfo[itype]->stWebEquipInfo[m].pEquipInfo,
		0);
	    if(iError != ERR_DXI_OK)
	    {
		TRACE_WEB_USER("Web_GetSingleDCSignalPointer error2[%d]\n", i);
		//AppLogOut("Web_GetSingleDCSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
		gs_pWebDCSigInfo[itype]->stWebEquipInfo[m].bSingleValid = FALSE;
		//return (iError);
	    }
	    else
	    {
		gs_pWebDCSigInfo[itype]->stWebEquipInfo[m].bSingleValid = TRUE;
	    }
	    
	    for(j = 0; j < iSingleSignalNum; j++)
	    {
		/*TRACE_WEB_USER("[%d][%d][%d]\n", iEquip, gs_pWebSigInfo[DC_SEQ].stWebPrivate[l + j].iSignalType, 
		gs_pWebSigInfo[DC_SEQ].stWebPrivate[l + j].iSignalID);*/
		iVarSubID = DXI_MERGE_SIG_ID(gs_pWebSigInfo[itype1].stWebPrivate[j].iSignalType, 
		    gs_pWebSigInfo[itype1].stWebPrivate[j].iSignalID);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		    iEquip,
		    iVarSubID,
		    &iBufLen,
		    (void *)&gs_pWebDCSigInfo[itype]->stWebPrivate[k].pSigValue,
		    iTimeOut);
		if(iError != ERR_DXI_OK)
		{
		    TRACE_WEB_USER("Web_GetSingleDCSignalPointer error3[%d]\n", j);
		    gs_pWebDCSigInfo[itype]->stWebPrivate[k].bSingleValid = FALSE;
		    //return (iError);
		}
		else
		{
		    gs_pWebDCSigInfo[itype]->stWebPrivate[k].bSingleValid = TRUE;
		}
		k++;
	    }
	    m++;
	    if(m >= iSingleDCNum)
	    {
		break;
	    }
	}
    }
    TRACE_WEB_USER("end to exe Web_GetSingleDCSignalPointer[%d]\n", itype);
    return ERR_DXI_OK;
}

/*==========================================================================*
* FUNCTION :  Web_GetSingleBattSignalPointer
* PURPOSE  :  Initial the pointer pointing to single battery value
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  indicate the kind of battery
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng               DATE: 2014-03-11
* MODIFY   :  Zhao Zicheng               DATE: 2018-03-11 To add the SoNick battery
*==========================================================================*/
int Web_GetSingleBattSignalPointer(int itype)
{
    int i, j, k, m, iSingleBattSignalNum, iSingleBattNum, iSingleSignalNum, iSingleBattAllNum;
    int	iError = 0;
    //SAMPLE_SIG_INFO	*pSampleSigInfo  = NULL;
    int iEquip, iEquip1;
    EQUIP_INFO		*pEquipInfo;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeOut = 0;
    int iInterfaceType;
    SAMPLE_SIG_VALUE	*pSampleSigValue = NULL;

    TRACE_WEB_USER("Begin to exe Web_GetSingleBattSignalPointer[%d]\n", itype);

    iInterfaceType = VAR_SAM_SIG_NUM_OF_EQUIP;
    if((itype < COMM_BATT_SINGLE_SEQ) || (itype > SMDUE_BATT_SEQ))
    {
	return 1;
    }

    if(itype == COMM_BATT_SINGLE_SEQ)
    {
	iSingleBattNum = gs_uiRealComBattNum;
	//iSingleSignalNum = gs_pWebSigInfo[COMM_BATT_SINGLE_SEQ].iNumber;
	TRACE_WEB_USER("iCfgQtyOfComBAT is %d\n", g_SiteInfo.stBattNum.iCfgQtyOfComBAT);
	iSingleBattAllNum = g_SiteInfo.stBattNum.iCfgQtyOfComBAT;
	if(iSingleBattAllNum == 0)
	{
	    iSingleBattAllNum = 2;
	}
	iEquip = COM_BATT_EQUIP_START;
    }
    else if(itype == EIB_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealEIBBattNum;
	iSingleBattAllNum = g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT1 + g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT2;
	if(iSingleBattAllNum == 0)
	{
	    iSingleBattAllNum = 12;
	}
	iEquip = EIB_BATT_EQUIP_START1;
    }
    else if(itype == SMDU_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSMDUBattNum;
	iSingleBattAllNum = g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT1 + g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT2;
	if(iSingleBattAllNum == 0)
	{
	    iSingleBattAllNum = 40;
	}
	iEquip = SMDU_BATT_EQUIP_START1;
    }
    else if(itype == SMDUE_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSMDUEBattNum;
	iSingleBattAllNum = g_SiteInfo.stBattNum.iCfgQtyOfSMDUEBAT;
	if(iSingleBattAllNum == 0)
	{
	    iSingleBattAllNum = 20;
	}
	iEquip = SMDUE_BATT_EQUIP_START;
    }
    else if(itype == SM_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSMBattNum;
	iSingleBattAllNum = g_SiteInfo.stBattNum.iCfgQtyOfSMBAT;
	if(iSingleBattAllNum == 0)
	{
	    iSingleBattAllNum = 20;
	}
	iEquip = SM_BATT_EQUIP_START2;
    }
    /*else if(itype == LARGEDU_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealLargeDUBattNum;
	iSingleSignalNum = gs_pWebSigInfo[LARGEDU_BATT_SEQ].iNumber;
	iSingleBattAllNum = g_SiteInfo.stBattNum.iCfgQtyOfLargeDUBAT;
    }*/
    else if(itype == SMBRC_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSMBRCBattNum;
	iSingleBattAllNum = g_SiteInfo.stBattNum.iCfgQtyOfSMBRC;
	if(iSingleBattAllNum == 0)
	{
	    iSingleBattAllNum = 20;
	}
	iEquip = SMBRC_BATT_EQUIP_START2;
    }
    else if(itype == SONICK_BATT_SEQ)
    {
	iSingleBattNum = gs_uiRealSoNickBattNum;
	iSingleBattAllNum = 32;
	iEquip = SONICK_BATT_EQUIP_START;
    }
    else
    {}

    iError = DxiGetData(iInterfaceType,
	iEquip,
	iVarSubID,
	&iBufLen,
	&iSingleSignalNum,
	iTimeOut);
    if(iError != ERR_DXI_OK)
    {
	TRACE_WEB_USER_NOT_CYCLE("Get equip sample signal number error!\n");
	iSingleSignalNum = 0;
    }

    if((iSingleBattNum == 0) && (iSingleSignalNum == 0))
    {
	return ERR_DXI_OK;
    }

    iSingleBattSignalNum = iSingleBattNum * iSingleSignalNum;
    TRACE_WEB_USER("Exec single battery\n");
    //initial single battery signal value pointer
    if(gs_pWebBattSigInfo[itype] != NULL)
    {
	if(gs_pWebBattSigInfo[itype]->stWebPrivate != NULL)
	{
	    DELETE(gs_pWebBattSigInfo[itype]->stWebPrivate);
	    gs_pWebBattSigInfo[itype]->stWebPrivate = NULL;
	}
	if(gs_pWebBattSigInfo[itype]->stWebEquipInfo != NULL)
	{
	    DELETE(gs_pWebBattSigInfo[itype]->stWebEquipInfo);
	    gs_pWebBattSigInfo[itype]->stWebEquipInfo = NULL;
	}
    }
    if(gs_pWebBattSigInfo[itype] != NULL)
    {
	DELETE(gs_pWebBattSigInfo[itype]);
	gs_pWebBattSigInfo[itype] = NULL;
    }
    TRACE_WEB_USER("iSingleBattSignalNum is %d	iSingleBattNum is %d	iSingleBattAllNum is %d\n", iSingleBattSignalNum, iSingleBattNum, iSingleBattAllNum);
    gs_pWebBattSigInfo[itype] = NEW(WEB_PRIVATE_RECT_SIGNAL_INFO, 1);
    if(gs_pWebBattSigInfo[itype] == NULL)
    {
	AppLogOut("Web_GetSingleBattSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }
    gs_pWebBattSigInfo[itype]->iNumber = iSingleBattSignalNum;
    gs_pWebBattSigInfo[itype]->iOneEquipNum = iSingleSignalNum;
    gs_pWebBattSigInfo[itype]->stWebPrivate = NEW(WEB_GENERAL_RECT_SIGNAL_INFO, iSingleBattSignalNum);
    if(gs_pWebBattSigInfo[itype]->stWebPrivate == NULL)
    {
	if(gs_pWebBattSigInfo[itype] != NULL)
	{
	    DELETE(gs_pWebBattSigInfo[itype]);
	    gs_pWebBattSigInfo[itype] = NULL;
	}
	AppLogOut("Web_GetSingleBattSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    gs_pWebBattSigInfo[itype]->stWebEquipInfo = NEW(WEB_GENERAL_RECT_EQUIP_INFO, iSingleBattNum);
    if(gs_pWebBattSigInfo[itype]->stWebEquipInfo == NULL)
    {
	if(gs_pWebBattSigInfo[itype] != NULL)
	{
	    DELETE(gs_pWebBattSigInfo[itype]);
	    gs_pWebBattSigInfo[itype] = NULL;
	}
	if(gs_pWebBattSigInfo[itype]->stWebPrivate != NULL)
	{
	    DELETE(gs_pWebBattSigInfo[itype]->stWebPrivate);
	    gs_pWebBattSigInfo[itype]->stWebPrivate = NULL;
	}
	AppLogOut("Web_GetSingleBattSignalPointer", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return 1;
    }

    TRACE_WEB_USER("Exec signal point\n");
    k = 0;
    m = 0;
    for(i = 0; i < iSingleBattAllNum; i++)
    {
	if(itype == COMM_BATT_SINGLE_SEQ)
	{
	    iEquip1 = iEquip + i;
	}
	else if(itype == EIB_BATT_SEQ)
	{
	    if(i < 8)
	    {
		iEquip1 = iEquip + i;
	    }
	    else
	    {
		iEquip1 = EIB_BATT_EQUIP_START2 + i - 8;
	    }
	}
	else if(itype == SMDU_BATT_SEQ)
	{
	    if(i < 32)
	    {
		iEquip1 = iEquip + i;
	    }
	    else
	    {
		iEquip1 = SMDU_BATT_EQUIP_START2 + i - 32;
	    }
	}
	else if(itype == SM_BATT_SEQ)
	{
	    iEquip1 = iEquip + i;
	}
	/*else if(itype == LARGEDU_BATT_SEQ)
	{
	    iEquip = LARGEDU_BATT_EQUIP_START2 + i;
	}*/
	else if((itype == SMBRC_BATT_SEQ) || (itype == SONICK_BATT_SEQ) || (itype == SMDUE_BATT_SEQ))
	{
	    iEquip1 = iEquip + i;
	}
	else
	{}
	iError = DxiGetData(VAR_A_EQUIP_INFO,
	    iEquip1,			
	    0,		
	    &iBufLen,			
	    &pEquipInfo,			
	    0);
	if(iError != ERR_DXI_OK)
	{
	    //AppLogOut("Web_GetSingleBattSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
	    //return (iError);
	}
	if((iError == ERR_DXI_OK) && (pEquipInfo->bWorkStatus == TRUE) && (m < iSingleBattNum))
	{
	    iError = DxiGetData(VAR_A_EQUIP_INFO,
		iEquip1,
		0,
		&iBufLen,
		&gs_pWebBattSigInfo[itype]->stWebEquipInfo[m].pEquipInfo,
		0);
	    if(iError != ERR_DXI_OK)
	    {
		//return (iError);
		//AppLogOut("Web_GetSingleBattSignalPointer", APP_LOG_ERROR, "Can't get the Equip(%d)\n", iEquip);
		gs_pWebBattSigInfo[itype]->stWebEquipInfo[m].bSingleValid = FALSE;
	    }
	    else
	    {
		gs_pWebBattSigInfo[itype]->stWebEquipInfo[m].bSingleValid = TRUE;
	    }
	    Web_GetSampleSignalByEquipID(iEquip1, &pSampleSigValue);
	    for(j = 0; j < iSingleSignalNum && pSampleSigValue != NULL; j++, pSampleSigValue++)// for every signal in a rectifier
	    {
		gs_pWebBattSigInfo[itype]->stWebPrivate[k].pSigValue = &(pSampleSigValue->bv);
		gs_pWebBattSigInfo[itype]->stWebPrivate[k].bSingleValid = TRUE;
		k++;
	    }
	    m++;
	    if(m >= iSingleBattNum)
	    {
		break;
	    }
	}
    }
    TRACE_WEB_USER("end to exe Web_GetSingleBattSignalPointer[%d]\n", itype);

    return ERR_DXI_OK;
}

/*==========================================================================*
* FUNCTION :  Web_GetOtherSignalPointer
* PURPOSE  :  Initial the pointer pointing to other equips signal such as DG, ac meter etc
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-12-25 Merry Christmas
*==========================================================================*/
int Web_GetOtherSignalPointer(void)
{
    TRACE_WEB_USER("Exec Web_GetOtherSignalPointer begin\n");
    int	iError = 0;
    //int	iVarSubID = 0;

    iError |= Web_GetSingleOtherSignalPointer(SMAC_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(DG_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(ACMETER_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(DCMETER_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(SMIO_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(DC_BOARD_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(SMDU_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(SMDUP_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(SMDUH_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(SMDUE_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(EIB_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(BATTERY_FUSE_GROUP_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(BATTERY_FUSE_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(LVD_GROUP_SEQ1);
    iError |= Web_GetSingleOtherSignalPointer(LVD_SEQ1);
    iError |= Web_GetSingleOtherSignalPointer(LVD_SEQ2);
    iError |= Web_GetSingleOtherSignalPointer(SMDU_BATT_FUSE_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(SMDU_LVD_SEQ);
    iError |= Web_GetSingleOtherSignalPointer(NARADA_BMS_SEQ);
	iError |= Web_GetSingleOtherSignalPointer(ACUNIT_SEQ);

    if(iError != ERR_DXI_OK)
    {
	return (iError);
    }

    TRACE_WEB_USER("Exec Web_GetOtherSignalPointer end\n");
    return ERR_DXI_OK;
}



/*==========================================================================*
* FUNCTION :  Web_GetDCSignalPointer
* PURPOSE  :  Initial the pointer pointing to DC value
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
* MODIFY   :  Zhao Zicheng               DATE: 2013-12-12 Add the EIB function
*==========================================================================*/
int Web_GetDCSignalPointer(void)
{
    TRACE_WEB_USER("Exec Web_GetDCSignalPointer begin\n");
    int	iError = 0;
    //int	iVarSubID = 0;

    //iError |= Web_GetSingleDCSignalPointer(SMDU_SEQ);
    //iError |= Web_GetSingleDCSignalPointer(SMDUP_SEQ);
    iError |= Web_GetSingleDCSignalPointer(SMDUP1_SEQ);
    //iError |= Web_GetSingleDCSignalPointer(SMDUH_SEQ);
    //iError |= Web_GetSingleDCSignalPointer(EIB_SEQ);

    if(iError != ERR_DXI_OK)
    {
	return (iError);
    }

    TRACE_WEB_USER("Exec Web_GetDCSignalPointer end\n");
    return ERR_DXI_OK;
}

int Web_GetBattSignalPointer(void)
{
    TRACE_WEB_USER("Exec Web_GetBattSignalPointer begin\n");
    //int i;
    int	iError = 0;
    //int	iVarSubID = 0;
    //int	iBufLen = 0;
    //int iTimeOut = 0;
    //SAMPLE_SIG_INFO	*pSampleSigInfo  = NULL;
    //int igroupNum;

    //initial battery group signal value pointer
    TRACE_WEB_USER("Exec group pointer\n");
    iError |= Web_GetGroupSignalPointer(COMM_BATT_GROUP_SEQ);

    iError |= Web_GetSingleBattSignalPointer(COMM_BATT_SINGLE_SEQ);
    iError |= Web_GetSingleBattSignalPointer(EIB_BATT_SEQ);
    iError |= Web_GetSingleBattSignalPointer(SMDU_BATT_SEQ);
    iError |= Web_GetSingleBattSignalPointer(SMDUE_BATT_SEQ);
    iError |= Web_GetSingleBattSignalPointer(SM_BATT_SEQ);
    //iError |= Web_GetSingleBattSignalPointer(LARGEDU_BATT_SEQ);
    iError |= Web_GetSingleBattSignalPointer(SMBRC_BATT_SEQ);
    iError |= Web_GetSingleBattSignalPointer(SONICK_BATT_SEQ);

    if(iError != ERR_DXI_OK)
    {
	return (iError);
    }

    TRACE_WEB_USER("Exec Web_GetBattSignalPointer end\n");
    return ERR_DXI_OK;
}

int Web_GetCabinetPointer(void)
{
    int i, j, k;// i for cabinets number, j for du number, k for signal number
    int	iError = 0;
    int	iBufLen = 0;
    int iSingalNum;
    int iStartEquipID;
    int	iVarSubID = 0;
    EQUIP_INFO	*pEquipInfo = NULL;

    for(i = 0; i < WEB_MAX_CABINET_NUM; i++)// for every cabinet
    {
	if(gs_pWebCabinetInfo[i].iNumber != 0)
	{
	    for(j = 0; j < gs_pWebCabinetInfo[i].iNumber; j++)// for every du
	    {
		//snprintf(gs_pWebCabinetInfo[i].stWebDU[j].cName[0], 64, "DU%d.%d", (i + 1), (j + 1));
		for(k = 0; k < gs_pWebCabinetInfo[i].stWebDU[j].iSignalNum; k++)//for each signal
		{
		    iVarSubID = DXI_MERGE_SIG_ID(0, gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].iSignalID);
		    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].iEquipID,
			iVarSubID,
			&iBufLen,
			(void *)&gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].pSigValue,
			0);
		    if(gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].iEquipID == 197)
		    {
			TRACE_WEB_USER_NOT_CYCLE("iEquipID is %d, iSignalID is %d, iError is %d\n", gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].iEquipID,
			    gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].iSignalID, iError);
		    }
		    if(iError != ERR_DXI_OK)
		    {
			gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].bSignalVaild = FALSE;
		    }
		    else
		    {
			iError = DxiGetData(VAR_A_EQUIP_INFO,
			    gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].iEquipID,
			    0,
			    &iBufLen,
			    &pEquipInfo,
			    0);
			if(iError == ERR_DXI_OK)
			{
			    if(pEquipInfo->bWorkStatus == TRUE)
			    {
				gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].bSignalVaild = TRUE;
			    }
			    else
			    {
				gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].bSignalVaild = FALSE;
			    }
			}
			else
			{
			    gs_pWebCabinetInfo[i].stWebDU[j].pSignalInfo[k].bSignalVaild = FALSE;
			}
		    }
		}
	    }
	}
    }
    return ERR_DXI_OK;
}

/*==========================================================================*
* FUNCTION :  Web_GetSignalPointer
* PURPOSE  :  Initial the pointer pointing to value displayed in webpages
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
int Web_GetSignalPointer(void)
{
    int	iError = 0;
    int i;

    for(i = 0; i < 6; i++)
    {
	gs_pWebRectSigInfo[i] = NULL;
    }
    iError = (Web_GetGroupSignalPointer(INDEX_SEQ) << 0);
    iError |= (Web_GetGroupSignalPointer(RECT_GROUP_SEQ) << 1);
    iError |= (Web_GetRectSignalPointer(RECT_GROUP_SEQ) << 2);
    iError |= (Web_GetGroupSignalPointer(RECTS1_GROUP_SEQ) << 3);
    iError |= (Web_GetRectSignalPointer(RECTS1_GROUP_SEQ) << 4);
    iError |= (Web_GetGroupSignalPointer(RECTS2_GROUP_SEQ) << 5);
    iError |= (Web_GetRectSignalPointer(RECTS2_GROUP_SEQ) << 6);
    iError |= (Web_GetGroupSignalPointer(RECTS3_GROUP_SEQ) << 7);
    iError |= (Web_GetRectSignalPointer(RECTS3_GROUP_SEQ) << 8);
    iError |= (Web_GetGroupSignalPointer(CONVERTER_GROUP_SEQ) << 9);
    iError |= (Web_GetRectSignalPointer(CONVERTER_GROUP_SEQ) << 10);
    iError |= (Web_GetGroupSignalPointer(SOLAR_GROUP_SEQ) << 11);
    iError |= (Web_GetRectSignalPointer(SOLAR_GROUP_SEQ) << 12);
    iError |= (Web_GetBattSignalPointer() << 13);
    iError |= (Web_GetDCSignalPointer() << 14);
	//changed by Frank Wu,1/1/31,20140715, for add single converter and single solar settings pages
    //iError |= (Web_GetSettingSignalPointer() << 15);
	iError |= (((Web_GetSettingSignalPointer() == 1)? 1: 0) << 15);//malloc error
    iError |= (Web_GetOtherSignalPointer() << 16);
    iError |= (Web_GetGroupSignalPointer(CONFIG_SEQ) << 17);
    iError |= (Web_GetCabinetPointer() << 18);

    return(iError);
}

/*==========================================================================*
* FUNCTION :   Web_fnCompareQueryCondition
* PURPOSE  :	Compare Condition	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   const void *pRecord:
const void *pTCondition:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_fnCompareQueryCondition(IN const void *pRecord, 
				       IN const void *pTCondition)
{
    HIS_GENERAL_CONDITION *pCondition = (HIS_GENERAL_CONDITION *)pTCondition;
    static unsigned int		lnHisDataCount = 0;
    static unsigned int		lnStatDataCount = 0;
    static unsigned int		lnHisAlarmCount = 0;
    static unsigned int		lnHisControlCount = 0;

    //TRACE("Come here compare data type is %d.\n", (int)pCondition->iDataType);

    if((pRecord == NULL) || (pTCondition == NULL))
    {
	return FALSE;
    }

    switch((int)pCondition->iDataType)
    {

    case QUERY_HIS_DATA:
	{	
	    /*History data*/
	    //TRACE("*********QUERY_HIS_DATA*************\n");
	    HIS_DATA_RECORD *pRecordData	= (HIS_DATA_RECORD *)pRecord;
	    lnHisDataCount++;
	    if(lnHisDataCount % 200 == 0)
	    {
		RunThread_Heartbeat(RunThread_GetId(NULL));
		lnHisDataCount = 0;
	    }

	    if(pRecordData->iEquipID == pCondition->iEquipID || 
		pCondition->iEquipID < 0)
	    {
		if(((pRecordData->tmSignalTime - pCondition->tmFromTime) > 0) && 
		    ((pCondition->tmToTime - pRecordData->tmSignalTime) > 0) && (pRecordData->iSignalID == pCondition->iSignalID))
		{

		    return TRUE;
		}
		else
		{
		    return FALSE;
		}
	    }
	    else
	    {
		return FALSE;
	    }
	    break;
	}
    case QUERY_STAT_DATA:
	{
	    /*Statical data*/
	    //TRACE("*********QUERY_STAT_DATA*************\n");
	    HIS_STAT_RECORD		*pRecordData = (HIS_STAT_RECORD *)pRecord;

	    lnStatDataCount++;
	    if(lnStatDataCount % 200 == 0)
	    {
		RunThread_Heartbeat(RunThread_GetId(NULL));
		lnStatDataCount = 0;
	    }

	    if(pRecordData->iEquipID == pCondition->iEquipID || 
		pCondition->iEquipID < 0)
	    {
		if((pRecordData->tmStatTime - pCondition->tmFromTime) > 0 && 
		    ( pCondition->tmToTime - pRecordData->tmStatTime) > 0)
		{	
		    return TRUE;
		}
		else
		{
		    return FALSE;
		}
	    }
	    else
	    {
		return FALSE;
	    }
	    break;

	}
    case QUERY_HISALARM_DATA:
	{
	    /*Query history alarm*/
	    //TRACE("*********QUERY_HISALARM_DATA*************\n");
	    HIS_ALARM_RECORD	*pRecordData = (HIS_ALARM_RECORD *)pRecord;

	    lnHisAlarmCount++;
	    if(lnHisAlarmCount % 200 == 0)
	    {
		RunThread_Heartbeat(RunThread_GetId(NULL));
		lnHisAlarmCount = 0;
	    }

	    /*TRACE("pCondition->iEquipID is %d.pRecordData->tmStartTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
	    pCondition->iEquipID,pRecordData->tmStartTime, pCondition->tmFromTime, pCondition->tmToTime);

	    TRACE("pCondition->iEquipID is %d.Time Interval is %d,Timew is %d\n",
	    pCondition->iEquipID,(pRecordData->tmStartTime - pCondition->tmFromTime), 
	    (pCondition->tmToTime - pRecordData->tmStartTime));*/

	    if(pRecordData->iEquipID == pCondition->iEquipID || 
		pCondition->iEquipID < 0)
	    {
		if((pRecordData->tmStartTime - pCondition->tmFromTime) > 0 && 
		    ( pCondition->tmToTime - pRecordData->tmStartTime) > 0)
		{	
		    /* TRACE("pCondition->iEquipID is %d.pRecordData->tmStartTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
		    pCondition->iEquipID,pRecordData->tmStartTime, pCondition->tmFromTime, pCondition->tmToTime);*/
		    return TRUE;
		}
		else
		{
		    return FALSE;
		}
	    }
	    else
	    {
		return FALSE;
	    }
	    break;

	}
    case QUERY_CONTROLCOMMAND_DATA:
	{

	    /*query control command log*/
	    //TRACE("*********QUERY_CONTROLCOMMAND_DATA*************\n");
	    HIS_CONTROL_RECORD		*pRecordData = (HIS_CONTROL_RECORD *)pRecord;

	    lnHisControlCount++;
	    if(lnHisControlCount % 200 == 0)
	    {
		RunThread_Heartbeat(RunThread_GetId(NULL));
		lnHisControlCount = 0;
	    }


	    /* TRACE("###########pCondition->iEquipID is %d.pRecordData->tmControlTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
	    pCondition->iEquipID,pRecordData->tmControlTime, pCondition->tmFromTime, pCondition->tmToTime);*/

	    if(pRecordData->iEquipID == pCondition->iEquipID || 
		pCondition->iEquipID < 0)
	    {
		if((pRecordData->tmControlTime - pCondition->tmFromTime) > 0 && 
		    ( pCondition->tmToTime - pRecordData->tmControlTime) > 0)
		{	
		    /*TRACE("@@@@@@@@@@@@@@@@pCondition->iEquipID is %d.pRecordData->tmControlTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
		    pCondition->iEquipID,pRecordData->tmControlTime, pCondition->tmFromTime, pCondition->tmToTime);*/

		    return TRUE;
		}
		else
		{
		    return FALSE;
		}
	    }
	    else
	    {
		return FALSE;
	    }
	    break;
	}
    case QUERY_DISEL_TEST:
	{
	    WEB_GC_DSL_TEST_INFO	*pRecordData = (WEB_GC_DSL_TEST_INFO *)pRecord;

	    if((pRecordData->tStartTime - pCondition->tmFromTime) > 0 && 
		( pCondition->tmToTime - pRecordData->tStartTime) > 0)
	    {	
		return TRUE;
	    }
	    else
	    {
		return FALSE;
	    }

	    break;

	}
    default:

	return FALSE;
    }
}

/*==========================================================================*
* FUNCTION :    Web_QueryHisData
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN time_t toTime:
IN time_t fromTime:
IN int iEquipID:
IN int iDataType:
OUT char **ppBuf: 
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_QueryHisData(IN time_t toTime,
			    IN time_t fromTime,
			    IN int iEquipID,
			    IN int iSignalID,
			    IN int iDataType, 
			    OUT void **ppBuf)
{
    /*query parameter equip,data type (his, statistics,from time to time)*/
    int			iRecords = 60000, iResult,iStartRecordNo = -1;		//record number
    HANDLE		hHisData;
    int			iBufLen = 60000;

    /*Judge data type*/
    HIS_GENERAL_CONDITION *pCondition	= NEW(HIS_GENERAL_CONDITION, 1);
    if(pCondition == NULL)
    {
	AppLogOut("Web_QueryHisData", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return FALSE;
    }
    pCondition->tmToTime				= toTime;
    pCondition->tmFromTime				= fromTime;
    pCondition->iEquipID				= iEquipID;
    pCondition->iDataType				= iDataType;
    pCondition->iSignalID				= iSignalID;

    //TRACE("iDataType:[%d]\n", iDataType);
    if(iDataType == QUERY_STAT_DATA)
    {
	hHisData =	DAT_StorageOpen(STAT_DATA_LOG);
	if(hHisData == NULL)
	{
	    if(pCondition != NULL)
	    {
		DELETE(pCondition);
		pCondition = NULL;
	    }
	    return FALSE;
	}
	HIS_STAT_RECORD *pHisDataRecord = NEW(HIS_STAT_RECORD, iBufLen);
	if(pHisDataRecord == NULL)
	{
	    if(pCondition != NULL)
	    {
		DELETE(pCondition);
		pCondition = NULL;
	    }
	    AppLogOut("Web_QueryHisData", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    return FALSE;
	}
	ASSERT(pHisDataRecord);		
	memset(pHisDataRecord, 0x0, iBufLen * sizeof(HIS_STAT_RECORD));

	//TRACE("Start to DAT_StorageFindRecords\n");
	iResult = DAT_StorageFindRecords(hHisData,
	    Web_fnCompareQueryCondition, 
	    (void *)pCondition,
	    &iStartRecordNo, 
	    &iRecords, 
	    (void*)pHisDataRecord, 
	    FALSE,
	    TRUE);
	DELETE(pCondition);
	pCondition = NULL;
	//#ifdef _SHOW_WEB_INFO
	//TRACE("HIS_STAT_RECORD : iResult : [%d] iRecords[%d]\n", iResult, iRecords);
	//#endif

	if(iResult >= 1  && iRecords > 0)
	{
	    //return (HIS_DATA_RECORD *)pHisDataRecord;
	    *ppBuf = (void *)pHisDataRecord;
	    DAT_StorageClose(hHisData);


	    return iRecords;
	}
	else
	{
	    DELETE(pHisDataRecord);
	    pHisDataRecord = NULL;
	    DAT_StorageClose(hHisData);
	    return FALSE;

	}
    }
    else
    {
	hHisData =	DAT_StorageOpen(HIS_DATA_LOG);
	if(hHisData == NULL)
	{
	    if(pCondition != NULL)
	    {
		DELETE(pCondition);
		pCondition = NULL;
	    }
	    return FALSE;
	}

	HIS_DATA_RECORD *pHisDataRecord = NEW(HIS_DATA_RECORD,iBufLen);
	if(pHisDataRecord == NULL)
	{
	    if(pCondition != NULL)
	    {
		DELETE(pCondition);
		pCondition = NULL;
	    }
	    AppLogOut("Web_QueryHisData", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    return FALSE;
	}
	ASSERT(pHisDataRecord);	
	memset(pHisDataRecord, 0x0, iBufLen * sizeof(HIS_DATA_RECORD));

	iResult = DAT_StorageFindRecords(hHisData,
	    Web_fnCompareQueryCondition, 
	    (void *)pCondition,
	    &iStartRecordNo, 
	    &iRecords, 
	    (void*)pHisDataRecord, 
	    0,
	    FALSE);
#ifdef _SHOW_WEB_INFO
	//TRACE("HIS_DATA_RECORD : iResult[%d] : iRecords[%d]\n", iResult, iRecords);
#endif

	DELETE(pCondition);
	pCondition = NULL;

	if(iResult >= 1 && iRecords > 0)
	{
	    //return (HIS_DATA_RECORD *)pHisDataRecord;
	    *ppBuf = (void *)pHisDataRecord;
	    DAT_StorageClose(hHisData);
	    return iRecords;
	}
	else
	{
	    DELETE(pHisDataRecord);
	    pHisDataRecord = NULL;
	    DAT_StorageClose(hHisData);
	    return FALSE;
	}
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeHisDataInit
* PURPOSE  :  Read the history load current data from flash, and select the peak value for every hour,
sort the peak value according to the sampling time, calculate the maximum and average value from these values
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeHisDataInit(void)
{
    time_t fromTime;
    time_t toTime;
    HIS_DATA_RECORD			*pReturnData	= NULL;
    HIS_DATA_RECORD			*pReturnDataTemp	= NULL;// backup of pReturnData
    int						iBufLen			= 0;
    //int				iBufLen1 = 0;  //backup of iBufLen
    struct tm gmTime;
    HIS_DATA_RECORD_LOAD		*pLoadData = NULL, *pLoadData2 = NULL;//pLoadData2 is the backup of pLoadData
    HIS_DATA_RECORD_LOAD		*pLoadData1 = NULL, *pLoadData3 = NULL;//pLoadData3 is the backup of pLoadData1
    HIS_DATA_RECORD_LOAD            LoadData;
    int iBufLenLoad = 0;			//the length of pLoadData
    int iBufLenLoad1 = 0;			//the length of pLoadData1
    int i, j, k, ihour;
    //int l;

    TRACE_WEB_USER("Begin to Web_MakeHisDataInit\n");

    gs_stLoadDataMax.floadCurrent = 0;
    gs_stLoadDataMax.tmSignalTime = 0;
    gs_floadCurrentAverage = 0;
    gs_iLoadCurrentNum = 0;
    memset(g_stLoadCurrentData, 0, sizeof(g_stLoadCurrentData));

    toTime = time((time_t *)0);
    fromTime = toTime - DAY30_SECOND;   //30 days time range
    int iEquipID = 1;
    int iSignalID = 2;

    if((iBufLen = Web_QueryHisData(toTime,fromTime, iEquipID, iSignalID,
	QUERY_HIS_DATA, (void *)&pReturnData)) <= 0 )
    {
	TRACE_WEB_USER("Read history data error\n");
	DELETE(pReturnData);
	pReturnData = NULL;
	return;
    }

    iBufLenLoad = iBufLen;

    pReturnDataTemp = pReturnData;

    // find the number of element which iSignalID is 2 or 182(load current)
 //   while(pReturnDataTemp != NULL && iBufLen > 0)
 //   {
	//if(pReturnDataTemp->iSignalID == 2 || pReturnDataTemp->iSignalID == 182)
	//{
	//    iBufLenLoad++;	
	//    //TRACE_WEB_USER("%ld, %.2f\n", pReturnDataTemp->tmSignalTime, pReturnDataTemp->varSignalVal.fValue);// for debug
	//}

	//pReturnDataTemp++;
	//iBufLen--;

 //   }

    //pReturnDataTemp = pReturnData;
    //iBufLen = iBufLen1;

    pLoadData2 = pLoadData = NEW(HIS_DATA_RECORD_LOAD, iBufLenLoad); //create pLoadData
    if(pLoadData == NULL)
    {
	if(pReturnData != NULL)
	{
	    DELETE(pReturnData);
	    pReturnData = NULL;
	}
	AppLogOut("Web_MakeHisDataInit", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return;
    }
    pLoadData3 = pLoadData1 = NEW(HIS_DATA_RECORD_LOAD, iBufLenLoad); //create pLoadData1

    if(pLoadData1 == NULL)
    {
	if(pReturnData != NULL)
	{
	    DELETE(pReturnData);
	    pReturnData = NULL;
	}
	if(pLoadData != NULL)
	{
	    DELETE(pLoadData);
	    pLoadData = NULL;
	}
	AppLogOut("Web_MakeHisDataInit", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return;
    }

    //save the element which iSignalID is 2 or 182(load current) to pLoadData
    while(pReturnData != NULL && iBufLen > 0)
    {
	//if(pReturnData->iSignalID == 2 || pReturnData->iSignalID == 182)
	{
	    gmtime_r(&(pReturnData->tmSignalTime), &gmTime);
	    pLoadData->tm1Date.tm_hour = gmTime.tm_hour;
	    pLoadData->tm1Date.tm_mday = gmTime.tm_mday;
	    pLoadData->tm1Date.tm_mon = gmTime.tm_mon;
	    pLoadData->tm1Date.tm_year = gmTime.tm_year;
	    pLoadData->floadCurrent = pReturnData->varSignalVal.fValue;
	    pLoadData->tmSignalTime = pReturnData->tmSignalTime;
	    pLoadData->bflag = TRUE;
	    pLoadData++;
	}
	pReturnData++;
	iBufLen--;
    }

    pLoadData = pLoadData2;

    //for debug begin
    /*for (l = 0; l < iBufLenLoad; l++)
    {
    TRACE_WEB_USER("[%d] year %d month %d day %d load %f\n", l, (pLoadData + l)->tm1Date.tm_year, (pLoadData + l)->tm1Date.tm_mon, 
    (pLoadData + l)->tm1Date.tm_mday, (pLoadData + l)->floadCurrent);
    }*/
    //for debug end

    ihour = 0;
    // find the peak load current of the same hour and record it in pMakeFileBuf
    //memset(g_stLoadCurrentData, 0, sizeof(g_stLoadCurrentData));
    for(i = 0; i < iBufLenLoad; i++)
    {
	//TRACE_WEB_USER("i = %d\n", i);// for debug
	if((pLoadData + i)->bflag == TRUE)
	{
	    LoadData.tm1Date.tm_hour = (pLoadData + i)->tm1Date.tm_hour;
	    LoadData.tm1Date.tm_mday = (pLoadData + i)->tm1Date.tm_mday;
	    LoadData.tm1Date.tm_mon = (pLoadData + i)->tm1Date.tm_mon;
	    LoadData.tm1Date.tm_year = (pLoadData + i)->tm1Date.tm_year;
	    LoadData.tmSignalTime = (pLoadData + i)->tmSignalTime;
	    LoadData.floadCurrent = (pLoadData + i)->floadCurrent;

	    //for debug begin
	    /*TRACE_WEB_USER("LoadData year %d month %d day %d hour %d load %f\n", LoadData.tm1Date.tm_year, LoadData.tm1Date.tm_mon,
	    LoadData.tm1Date.tm_mday, LoadData.tm1Date.tm_hour, LoadData.floadCurrent);*/
	    //for debug end

	    // put the element belong to the same hour to pLoadData1, the length is iBufLenLoad1
	    iBufLenLoad1 = 0;
	    for(j = 0; j < iBufLenLoad; j++)
	    {
		//TRACE_WEB_USER("j = %d\n", j);// for debug
		if(((pLoadData + j)->bflag == TRUE) &&
		    (LoadData.tm1Date.tm_hour == (pLoadData + j)->tm1Date.tm_hour) &&
		    (LoadData.tm1Date.tm_mday == (pLoadData + j)->tm1Date.tm_mday) &&
		    (LoadData.tm1Date.tm_mon == (pLoadData + j)->tm1Date.tm_mon) &&
		    (LoadData.tm1Date.tm_year == (pLoadData + j)->tm1Date.tm_year))
		{
		    (pLoadData + j)->bflag = FALSE;// if the element has been compared, set the bflag to be FALSE, in order to neglect it
		    pLoadData1->tmSignalTime = (pLoadData + j)->tmSignalTime;
		    pLoadData1->floadCurrent = (pLoadData + j)->floadCurrent;
		    pLoadData1++;
		    iBufLenLoad1++;
		}
	    }

	    pLoadData1 = pLoadData3;
	    //for debug begin
	    /*for (l = 0; l < iBufLenLoad1; l++)
	    {
	    TRACE_WEB_USER("[%d] pLoadData1 tmSignalTime %ld load %f\n", l, (pLoadData1 + l)->tmSignalTime, (pLoadData1 + l)->floadCurrent);
	    }*/
	    //for debug end

	    // find the peak load current of pLoadData1
	    for(k = 0; k < iBufLenLoad1; k++)
	    {
		//TRACE_WEB_USER("k = %d\n", k);// for debug
		if(LoadData.floadCurrent < (pLoadData1 + k)->floadCurrent)
		{
		    LoadData.floadCurrent = (pLoadData1 + k)->floadCurrent;
		    LoadData.tmSignalTime = (pLoadData1 + k)->tmSignalTime;
		}
	    }
	    gmtime_r(&(LoadData.tmSignalTime), &gmTime);
	    LoadData.tm1Date.tm_hour = gmTime.tm_hour;
	    LoadData.tm1Date.tm_mday = gmTime.tm_mday;
	    LoadData.tm1Date.tm_mon = gmTime.tm_mon;
	    LoadData.tm1Date.tm_year = gmTime.tm_year;
	    memcpy(&(g_stLoadCurrentData[ihour]), &LoadData, sizeof(LoadData));
	    g_stLoadCurrentData[ihour].bflag = TRUE;

	    //find the peak load current of pLoadData
	    if(gs_stLoadDataMax.floadCurrent <= LoadData.floadCurrent)
	    {
		gs_stLoadDataMax.floadCurrent = LoadData.floadCurrent;
		gs_stLoadDataMax.tmSignalTime = LoadData.tmSignalTime;
	    }
	    gs_floadCurrentAverage += LoadData.floadCurrent;
	    //for debug begin
	    //TRACE_WEB_USER("peak load current tmSignalTime %ld load %f\n", LoadData.tmSignalTime, LoadData.floadCurrent);
	    //for debug end

	    // record the peak load current and relative date in pMakeFileBuf
	    //TRACE_WEB_USER("ihour is %d\n", ihour);// for debug
	    ihour++;
	    if(ihour > ONE_MONTH_HOUR)// at most record 30 days' data
	    {
		break;
	    }
	}
    }
    gs_iLoadCurrentNum = ihour;
    bubble_sort(g_stLoadCurrentData, ihour);
    //TRACE_WEB_USER("iFileLen is %d\n", iFileLen);

    if(ihour != 0)
    {
	gs_floadCurrentAverage = gs_floadCurrentAverage / ihour;
    }
    /*for (l = 0; l < ONE_MONTH_HOUR; l++)
    {
    if(g_stLoadCurrentData[l].bflag == TRUE)
    {
    TRACE_WEB_USER("g_stLoadCurrentData[%d] is %ld %f\n", l, g_stLoadCurrentData[l].tmSignalTime, g_stLoadCurrentData[l].floadCurrent);
    }
    }*/
    TRACE_WEB_USER("gs_stLoadDataMax is %f\n", gs_stLoadDataMax.floadCurrent);
    TRACE_WEB_USER("gs_floadCurrentAverage is %f\n", gs_floadCurrentAverage);

    DELETE(pReturnDataTemp);
    pReturnDataTemp = NULL;
    DELETE(pLoadData);
    pLoadData = NULL;
    DELETE(pLoadData1);
    pLoadData1 = NULL;
    TRACE_WEB_USER("end to Web_MakeHisDataInit\n");
    return;
}

/*==========================================================================*
* FUNCTION :  Web_InsertLoadCurrentPoint
* PURPOSE  :  Insert a load current data to the history load current saved in g_stLoadCurrentData
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  lpTrapData: the data inserted to the g_stLoadCurrentData
* RETURN   :  int: success or not
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
int Web_InsertLoadCurrentPoint(void *lpTrapData)
{
    struct tm gmTime;
    int i;
    HIS_DATA_RECORD_LOAD *pLoadData;

    pLoadData = (HIS_DATA_RECORD_LOAD *)lpTrapData;
    //pLoadData = lpTrapData;
    gmtime_r(&(pLoadData->tmSignalTime), &gmTime);
    pLoadData->tm1Date.tm_hour = gmTime.tm_hour;
    pLoadData->tm1Date.tm_mday = gmTime.tm_mday;
    pLoadData->tm1Date.tm_mon = gmTime.tm_mon;
    pLoadData->tm1Date.tm_year = gmTime.tm_year;
    pLoadData->bflag = TRUE;

    TRACE_WEB_USER_NOT_CYCLE("Begin to Web_InsertLoadCurrentPoint\n");
    TRACE_WEB_USER_NOT_CYCLE("year:%d month:%d day:%d hour %d value:%f\n", pLoadData->tm1Date.tm_year, pLoadData->tm1Date.tm_mon,
	pLoadData->tm1Date.tm_mday, pLoadData->tm1Date.tm_hour, pLoadData->floadCurrent);
    TRACE_WEB_USER_NOT_CYCLE("gs_iLoadCurrentNum is %d\n", gs_iLoadCurrentNum);

    if(gs_iLoadCurrentNum == 0)//no record before
    {
	memcpy(g_stLoadCurrentData, pLoadData, sizeof(HIS_DATA_RECORD_LOAD));
	gs_iLoadCurrentNum++;
    }
    else
    {		
	if((pLoadData->tm1Date.tm_hour == g_stLoadCurrentData[gs_iLoadCurrentNum - 1].tm1Date.tm_hour) &&
	    (pLoadData->tm1Date.tm_mday == g_stLoadCurrentData[gs_iLoadCurrentNum - 1].tm1Date.tm_mday) &&
	    (pLoadData->tm1Date.tm_mon == g_stLoadCurrentData[gs_iLoadCurrentNum - 1].tm1Date.tm_mon) &&
	    (pLoadData->tm1Date.tm_year == g_stLoadCurrentData[gs_iLoadCurrentNum - 1].tm1Date.tm_year))
	{   // the data within the same hour
	    if(pLoadData->floadCurrent > g_stLoadCurrentData[gs_iLoadCurrentNum - 1].floadCurrent)
	    {
		memcpy(g_stLoadCurrentData + gs_iLoadCurrentNum - 1, pLoadData, sizeof(HIS_DATA_RECORD_LOAD));
	    }
	    else
	    {}
	}
	else /*if(((pLoadData->tm1Date.tm_hour - g_stLoadCurrentData[gs_iLoadCurrentNum - 1].tm1Date.tm_hour) == 1) &&
	    (pLoadData->tm1Date.tm_mday == g_stLoadCurrentData[gs_iLoadCurrentNum - 1].tm1Date.tm_mday) &&
	    (pLoadData->tm1Date.tm_mon == g_stLoadCurrentData[gs_iLoadCurrentNum - 1].tm1Date.tm_mon) &&
	    (pLoadData->tm1Date.tm_year == g_stLoadCurrentData[gs_iLoadCurrentNum - 1].tm1Date.tm_year))*/
	{   //the data in next hour or the time has been changed
	    if(gs_iLoadCurrentNum < ONE_MONTH_HOUR)
	    {//if the g_stLoadCurrentData is not full
		//memcpy(g_stLoadCurrentData + gs_iLoadCurrentNum - 1, pLoadData, sizeof(HIS_DATA_RECORD_LOAD));
		memcpy(g_stLoadCurrentData + gs_iLoadCurrentNum, pLoadData, sizeof(HIS_DATA_RECORD_LOAD));
		gs_iLoadCurrentNum++;
	    }
	    else
	    {//if the g_stLoadCurrentData is full, delete the oldest data, insert the new data to the last position
		for(i = 0; i < ONE_MONTH_HOUR - 1; i++)
		{
		    memcpy(g_stLoadCurrentData + i, g_stLoadCurrentData + i + 1, sizeof(HIS_DATA_RECORD_LOAD));
		}
		memcpy(g_stLoadCurrentData + ONE_MONTH_HOUR - 1, pLoadData, sizeof(HIS_DATA_RECORD_LOAD));
	    }
	}
	//else
	//{
	//    return FALSE;
	//    //indicate the time has been modified, get the data from flash to g_stLoadCurrentData via Web_MakeHisDataInit
	//}
    }
    TRACE_WEB_USER_NOT_CYCLE("end to Web_InsertLoadCurrentPoint\n");
    return TRUE;
}

int ServiceNotification(HANDLE hService, 
			int iMsgType, 
			int iTrapDataLength, 
			void *lpTrapData,	
			void *lpParam, 
			BOOL bUrgent)
{
    UNUSED(hService);
    UNUSED(iTrapDataLength);
    UNUSED(lpParam);
    UNUSED(bUrgent);
    if((iMsgType & _LOAD_CURRENT_MASK) != 0)
    {
	if(Mutex_Lock(g_hMutexLoadCurrent, MAX_TIME_WAITING) == ERR_MUTEX_OK)
	{
	    TRACE_WEB_USER_NOT_CYCLE("Enter Web User ServiceNotification function [_LOAD_CURRENT_MASK]\n");
	    Web_InsertLoadCurrentPoint(lpTrapData);
	    Mutex_Unlock(g_hMutexLoadCurrent);
	}
	else
	{
	    TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexLoadCurrent [_LOAD_CURRENT_MASK]\n");
	    AppLogOut("Get lock", APP_LOG_WARNING, "Can't get the lock of g_hMutexLoadCurrent in _LOAD_CURRENT_MASK!\n");
	}
	/*gs_iLoadCurrMask = 1;
	gs_pParam = (HIS_DATA_RECORD_LOAD *)lpTrapData;*/
    }
    if((iMsgType & _CLEAR_HISTORY_MASK) != 0)
    {
	TRACE_WEB_USER_NOT_CYCLE("Enter Web User ServiceNotification function [_CLEAR_HISTORY_MASK]\n");
	Lock_g_hMutexLoadCurrent(INIT_LOAD_DATA);
	Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);
	//changed by Frank Wu,20140213,9/9, for displaying history temperature curve more faster
	g_SiteInfo.pstWebLcdShareData->bTrendTIsNeedUpdate = TRUE;

	/*if(Mutex_Lock(g_hMutexLoadCurrent, MAX_TIME_WAITING) == ERR_MUTEX_OK)
	{
	    TRACE_WEB_USER_NOT_CYCLE("Enter Web User ServiceNotification function [_CLEAR_HISTORY_MASK]\n");
	    Web_MakeHisDataInit();
	    Mutex_Unlock(g_hMutexLoadCurrent);
	}
	else
	{
	    TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexLoadCurrent [_CLEAR_HISTORY_MASK]\n");
	    AppLogOut("_CLEAR_HISTORY_MASK", APP_LOG_WARNING, "Can't get the lock of g_hMutexLoadCurrent!\n");
	}*/
    }
    /*if((iMsgType & (_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK)) != 0)
    {
	TRACE_WEB_USER_NOT_CYCLE("Enter Web User ServiceNotification function [_ALARM_OCCUR_MASK][_ALARM_CLEAR_MASK]\n");
	Web_MakeAlarmDataFile();
    }*/
 //   if((iMsgType & _WEB_SET_MASK) != 0)	// the setting signals value have been changed
 //   {
	//if(Mutex_Lock(g_hMutexSetDataFile, MAX_TIME_WAITING) == ERR_MUTEX_OK)
	//{
	//    TRACE_WEB_USER_NOT_CYCLE("Get the lock of g_hMutexSetDataFile in ServiceNotification\n");
	//    gs_iWebSetMask = 1;
	//    Mutex_Unlock(g_hMutexSetDataFile);
	//}
	//else
	//{
	//    TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexSetDataFile in ServiceNotification\n");
	//    AppLogOut("Get lock", APP_LOG_WARNING, "Can't get the lock of g_hMutexSetDataFile in _WEB_SET_MASK!\n");
	//}
 //   }
    return 1;
}

/*==========================================================================*
* FUNCTION :  Web_MakeCurveDataInit
* PURPOSE  :  Read the history data from flash, and select the peak value for every hour,
sort the peak value according to the sampling time
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  iEquipID: the equip ID including the signal
iSignalID: the signal ID
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeCurveDataInit(int iEquipID, int iSignalID)
{
    time_t fromTime;
    time_t toTime;
    HIS_DATA_RECORD			*pReturnData	= NULL;
    HIS_DATA_RECORD			*pReturnDataTemp	= NULL;// backup of pReturnData
    int						iBufLen			= 0;
    //int				iBufLen1 = 0;  //backup of iBufLen
    struct tm gmTime;
    HIS_DATA_RECORD_LOAD		*pLoadData = NULL, *pLoadData2 = NULL;//pLoadData2 is the backup of pLoadData
    HIS_DATA_RECORD_LOAD		*pLoadData1 = NULL, *pLoadData3 = NULL;//pLoadData3 is the backup of pLoadData1
    HIS_DATA_RECORD_LOAD            LoadData;
    int iBufLenLoad = 0;			//the length of pLoadData
    int iBufLenLoad1 = 0;			//the length of pLoadData1
    int i, j, k, ihour;
    //int l;

    TRACE_WEB_USER("Begin to Web_MakeCurveDataInit\n");

    if(iEquipID == EQUIP_AC_GROUP)
    {
	memset(gs_stACCurrentData, 0, sizeof(gs_stACCurrentData));
    }
    else if(iEquipID == EQUIP_DG_GROUP)
    {
	memset(gs_stDGCurrentData, 0, sizeof(gs_stDGCurrentData));
    }
    else if(iEquipID == EQUIP_SOLAR_GROUP)
    {
	memset(gs_stSolarCurrentData, 0, sizeof(gs_stSolarCurrentData));
    }
    else if(iEquipID == EQUIP_SYSTEM)
    {
	memset(gs_stAmbientTempData, 0, sizeof(gs_stAmbientTempData));
    }
    else if(iEquipID == EQUIP_BATT_GROUP)
    {
	memset(gs_stCompTempData, 0, sizeof(gs_stCompTempData));
    }
    else if(iEquipID == EQUIP_BATTERY1)
    {
	memset(gs_stBatt1CapData, 0, sizeof(gs_stBatt1CapData));
    }
    else if(iEquipID == EQUIP_BATTERY2)
    {
	memset(gs_stBatt2CapData, 0, sizeof(gs_stBatt2CapData));
    }
    else
    {
	memset(gs_stWindCurrentData, 0, sizeof(gs_stWindCurrentData));
    }

    toTime = time((time_t *)0);
    /*if((iEquipID == EQUIP_AC_GROUP) || (iEquipID == EQUIP_DG_GROUP) || (iEquipID == EQUIP_SOLAR_GROUP))
    {*/
	fromTime = toTime - DAY28_SECOND;   //28 days time range
 //   }
 //   else if((iEquipID == EQUIP_SYSTEM) || (iEquipID == EQUIP_BATT_GROUP) || (iEquipID == EQUIP_BATTERY1) || (iEquipID == EQUIP_BATTERY2))
 //   {
	//fromTime = toTime - DAY07_SECOND;   //28 days time range
 //   }
 //   else
 //   {
	//return;
 //   }

    if((iBufLen = Web_QueryHisData(toTime,fromTime, iEquipID, iSignalID, 
	QUERY_HIS_DATA, (void *)&pReturnData)) <= 0 )
    {
	//TRACE_WEB_USER("Read history data Hybrid error\n");
	DELETE(pReturnData);
	pReturnData = NULL;
	return;
    }

    iBufLenLoad = iBufLen;

    pReturnDataTemp = pReturnData;

    // find the number of element which is the right iSignalID
 //   while(pReturnDataTemp != NULL && iBufLen > 0)
 //   {
	//if(pReturnDataTemp->iSignalID == iSignalID)
	//{
	//    iBufLenLoad++;	
	//    //TRACE_WEB_USER("%ld, %.2f\n", pReturnDataTemp->tmSignalTime, pReturnDataTemp->varSignalVal.fValue);// for debug
	//}

	//pReturnDataTemp++;
	//iBufLen--;

 //   }

    //pReturnDataTemp = pReturnData;
    //iBufLen = iBufLen1;

    pLoadData2 = pLoadData = NEW(HIS_DATA_RECORD_LOAD, iBufLenLoad); //create pLoadData
    if(pLoadData == NULL)
    {
	if(pReturnData != NULL)
	{
	    DELETE(pReturnData);
	    pReturnData = NULL;
	}
	AppLogOut("Web_MakeCurveDataInit", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return;
    }
    pLoadData3 = pLoadData1 = NEW(HIS_DATA_RECORD_LOAD, iBufLenLoad); //create pLoadData1
    if(pLoadData1 == NULL)
    {
	if(pReturnData != NULL)
	{
	    DELETE(pReturnData);
	    pReturnData = NULL;
	}
	if(pLoadData != NULL)
	{
	    DELETE(pLoadData);
	    pLoadData = NULL;
	}
	AppLogOut("Web_MakeCurveDataInit", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	return;
    }

    while(pReturnData != NULL && iBufLen > 0)
    {
	//if(pReturnData->iSignalID == iSignalID)
	{
	    gmtime_r(&(pReturnData->tmSignalTime), &gmTime);
	    pLoadData->tm1Date.tm_hour = gmTime.tm_hour;
	    pLoadData->tm1Date.tm_mday = gmTime.tm_mday;
	    pLoadData->tm1Date.tm_mon = gmTime.tm_mon;
	    pLoadData->tm1Date.tm_year = gmTime.tm_year;
	    pLoadData->floadCurrent = pReturnData->varSignalVal.fValue;
	    pLoadData->tmSignalTime = pReturnData->tmSignalTime;
	    pLoadData->bflag = TRUE;
	    pLoadData++;
	}
	pReturnData++;
	iBufLen--;
    }

    pLoadData = pLoadData2;

    //for debug begin
    /*for (l = 0; l < iBufLenLoad; l++)
    {
    TRACE_WEB_USER("[%d] year %d month %d day %d load %f\n", l, (pLoadData + l)->tm1Date.tm_year, (pLoadData + l)->tm1Date.tm_mon, 
    (pLoadData + l)->tm1Date.tm_mday, (pLoadData + l)->floadCurrent);
    }*/
    //for debug end

    ihour = 0;
    // find the peak load current of the same hour and record it in pMakeFileBuf
    for(i = 0; i < iBufLenLoad; i++)
    {
	//TRACE_WEB_USER("i = %d\n", i);// for debug
	if((pLoadData + i)->bflag == TRUE)
	{
	    LoadData.tm1Date.tm_hour = (pLoadData + i)->tm1Date.tm_hour;
	    LoadData.tm1Date.tm_mday = (pLoadData + i)->tm1Date.tm_mday;
	    LoadData.tm1Date.tm_mon = (pLoadData + i)->tm1Date.tm_mon;
	    LoadData.tm1Date.tm_year = (pLoadData + i)->tm1Date.tm_year;
	    LoadData.tmSignalTime = (pLoadData + i)->tmSignalTime;
	    LoadData.floadCurrent = (pLoadData + i)->floadCurrent;

	    //for debug begin
	    /*TRACE_WEB_USER("LoadData year %d month %d day %d hour %d load %f\n", LoadData.tm1Date.tm_year, LoadData.tm1Date.tm_mon,
	    LoadData.tm1Date.tm_mday, LoadData.tm1Date.tm_hour, LoadData.floadCurrent);*/
	    //for debug end

	    // put the element belong to the same hour to pLoadData1, the length is iBufLenLoad1
	    iBufLenLoad1 = 0;
	    for(j = 0; j < iBufLenLoad; j++)
	    {
		//TRACE_WEB_USER("j = %d\n", j);// for debug
		if(((pLoadData + j)->bflag == TRUE) &&
		    (LoadData.tm1Date.tm_hour == (pLoadData + j)->tm1Date.tm_hour) &&
		    (LoadData.tm1Date.tm_mday == (pLoadData + j)->tm1Date.tm_mday) &&
		    (LoadData.tm1Date.tm_mon == (pLoadData + j)->tm1Date.tm_mon) &&
		    (LoadData.tm1Date.tm_year == (pLoadData + j)->tm1Date.tm_year))
		{
		    (pLoadData + j)->bflag = FALSE;// if the element has been compared, set the bflag to be FALSE, in order to neglect it
		    pLoadData1->tmSignalTime = (pLoadData + j)->tmSignalTime;
		    pLoadData1->floadCurrent = (pLoadData + j)->floadCurrent;
		    pLoadData1++;
		    iBufLenLoad1++;
		}
	    }

	    pLoadData1 = pLoadData3;
	    //for debug begin
	    /*for (l = 0; l < iBufLenLoad1; l++)
	    {
	    TRACE_WEB_USER("[%d] pLoadData1 tmSignalTime %ld load %f\n", l, (pLoadData1 + l)->tmSignalTime, (pLoadData1 + l)->floadCurrent);
	    }*/
	    //for debug end

	    // find the peak load current of pLoadData1
	    for(k = 0; k < iBufLenLoad1; k++)
	    {
		//TRACE_WEB_USER("k = %d\n", k);// for debug
		if(LoadData.floadCurrent < (pLoadData1 + k)->floadCurrent)
		{
		    LoadData.floadCurrent = (pLoadData1 + k)->floadCurrent;
		    LoadData.tmSignalTime = (pLoadData1 + k)->tmSignalTime;
		}
	    }
	    gmtime_r(&(LoadData.tmSignalTime), &gmTime);
	    LoadData.tm1Date.tm_hour = gmTime.tm_hour;
	    LoadData.tm1Date.tm_mday = gmTime.tm_mday;
	    LoadData.tm1Date.tm_mon = gmTime.tm_mon;
	    LoadData.tm1Date.tm_year = gmTime.tm_year;
	    if(iEquipID == EQUIP_AC_GROUP)
	    {
		memcpy(&(gs_stACCurrentData[ihour]), &LoadData, sizeof(LoadData));
		gs_stACCurrentData[ihour].bflag = TRUE;
	    }
	    else if(iEquipID == EQUIP_DG_GROUP)
	    {
		memcpy(&(gs_stDGCurrentData[ihour]), &LoadData, sizeof(LoadData));
		gs_stDGCurrentData[ihour].bflag = TRUE;
	    }
	    else if(iEquipID == EQUIP_SOLAR_GROUP)
	    {
		memcpy(&(gs_stSolarCurrentData[ihour]), &LoadData, sizeof(LoadData));
		gs_stSolarCurrentData[ihour].bflag = TRUE;
	    }
	    else if(iEquipID == EQUIP_SYSTEM)
	    {
		memcpy(&(gs_stAmbientTempData[ihour]), &LoadData, sizeof(LoadData));
		gs_stAmbientTempData[ihour].bflag = TRUE;
	    }
	    else if(iEquipID == EQUIP_BATT_GROUP)
	    {
		memcpy(&(gs_stCompTempData[ihour]), &LoadData, sizeof(LoadData));
		gs_stCompTempData[ihour].bflag = TRUE;
	    }
	    else if(iEquipID == EQUIP_BATTERY1)
	    {
		memcpy(&(gs_stBatt1CapData[ihour]), &LoadData, sizeof(LoadData));
		gs_stBatt1CapData[ihour].bflag = TRUE;
	    }
	    else if(iEquipID == EQUIP_BATTERY2)
	    {
		memcpy(&(gs_stBatt2CapData[ihour]), &LoadData, sizeof(LoadData));
		gs_stBatt2CapData[ihour].bflag = TRUE;
	    }
	    else
	    {
		memcpy(&(gs_stWindCurrentData[ihour]), &LoadData, sizeof(LoadData));
		gs_stWindCurrentData[ihour].bflag = TRUE;
	    }

	    //find the peak load current of pLoadData
	    //for debug begin
	    //TRACE_WEB_USER("peak load current tmSignalTime %ld load %f\n", LoadData.tmSignalTime, LoadData.floadCurrent);
	    //for debug end

	    // record the peak load current and relative date in pMakeFileBuf
	    //TRACE_WEB_USER("ihour is %d\n", ihour);// for debug
	    ihour++;
	 //   if((iEquipID == EQUIP_SYSTEM) || (iEquipID == EQUIP_BATT_GROUP) || (iEquipID == EQUIP_BATTERY1) || (iEquipID == EQUIP_BATTERY2))
	 //   {
		//if(ihour > ONE_WEEK_HOUR)// at most record 7 days' data
		//{
		//    break;
		//}
	 //   }
	 //   else
	 //   {
		if(ihour > FOUR_WEEK_HOUR)// at most record 28 days' data
		{
		    break;
		}
	    //}
	}
    }
    if(iEquipID == EQUIP_AC_GROUP)
    {
	//g_iACCurrentNum = ihour;
	bubble_sort(gs_stACCurrentData, ihour);
    }
    else if(iEquipID == EQUIP_DG_GROUP)
    {
	//g_iDGCurrentNum = ihour;
	bubble_sort(gs_stDGCurrentData, ihour);
    }
    else if(iEquipID == EQUIP_SOLAR_GROUP)
    {
	//g_iSolarCurrentNum = ihour;
	bubble_sort(gs_stSolarCurrentData, ihour);
    }
    else if(iEquipID == EQUIP_SYSTEM)
    {
	bubble_sort(gs_stAmbientTempData, ihour);
    }
    else if(iEquipID == EQUIP_BATT_GROUP)
    {
	bubble_sort(gs_stCompTempData, ihour);
    }
    else if(iEquipID == EQUIP_BATTERY1)
    {
	bubble_sort(gs_stBatt1CapData, ihour);
    }
    else if(iEquipID == EQUIP_BATTERY2)
    {
	bubble_sort(gs_stBatt2CapData, ihour);
    }
    else
    {
	//g_iWindCurrentNum = ihour;
	bubble_sort(gs_stWindCurrentData, ihour);
    }
    //TRACE_WEB_USER("iFileLen is %d\n", iFileLen);

    DELETE(pReturnDataTemp);
    pReturnDataTemp = NULL;
    DELETE(pLoadData);
    pLoadData = NULL;
    DELETE(pLoadData1);
    pLoadData1 = NULL;
    TRACE_WEB_USER("end to Web_MakeCurveDataInit\n");
    return;
}


static int Web_GetBarcode_Info(IN char **szProductInfo)
{
#define  MAX_PRODUCT_INFO_LEN			384	

    int		iDevice = DXI_GetDeviceNum();
    int		iRectNumber = DXI_GetRectRealNumber();
    int		iRectStartDeviceID =  DXI_GetFirstRectDeviceID();
    int		iSMDUNumber;
    int		iSlave1RectNum=0;
    int		iSlave2RectNum=0;
    int		iSlave3RectNum=0;
    int		iSMDUPNumber;

    int		i = 0, iLen = 0;
    //char	*pSearchValue = NULL;
    char	*szTProductInfo = NULL;
    int iError;
    int iEquipID = 106;
    int iSignalType = 0;
    int iSignalID = 1;
    int iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
    SIG_BASIC_VALUE* pSigValue;
    int j;
    int	iBufLen = 0;
    int k;
    int		iBufLength;

    //for debug begin
    /*int iDeviceNum, ii;
    DEVICE_INFO		*pDevice = NULL;
    printf("iRectStartDeviceID is %d\n", iRectStartDeviceID);
    iDeviceNum = g_SiteInfo.iDeviceNum;
    pDevice = g_SiteInfo.pDeviceInfo;
    for (ii = 0; ii < iDeviceNum; ii++,pDevice++)
    {
	printf("%d  device ID is %d, name is %s, SN is %d\n", ii, pDevice->iDeviceID, pDevice->pProductInfo->szDeviceName[0], pDevice->pProductInfo->szSerialNumber);
    }
    for(ii = 0; ii < 66; ii++)
    {
	printf("485 name %d is %s, SN is %s\n", ii, g_SiteInfo.st485ProductInfo[ii].szDeviceName[0], g_SiteInfo.st485ProductInfo[ii].szSerialNumber);
    }*/
    //for debug end

    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	iEquipID,			
	iVarSubID,		
	&iBufLen,			
	(void *)&pSigValue,			
	10000);

    if(iError != ERR_DXI_OK)
    {
	iSMDUNumber = 0;

    }
    else
    {
	iSMDUNumber = pSigValue->varValue.ulValue;

    }

    iEquipID = 642;//Slave1
    iSignalType = 0;
    iSignalID = 1;
    iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	iEquipID,			
	iVarSubID,		
	&iBufLen,			
	(void *)&pSigValue,			
	10000);

    if(iError != ERR_DXI_OK)
    {
	iSMDUPNumber = 0;

    }
    else
    {
	iSMDUPNumber = pSigValue->varValue.ulValue;

    }

    iEquipID = 639;//Slave1
    iSignalType = 0;
    iSignalID = 2;
    iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	iEquipID,			
	iVarSubID,		
	&iBufLen,			
	(void *)&pSigValue,			
	10000);
    if(iError != ERR_DXI_OK)
    {
	iSlave1RectNum = 0;

    }
    else
    {
	iSlave1RectNum = pSigValue->varValue.ulValue;
    }


    iEquipID = 640;//Slave2
    iSignalType = 0;
    iSignalID = 2;
    iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	iEquipID,			
	iVarSubID,		
	&iBufLen,			
	(void *)&pSigValue,			
	10000);

    if(iError != ERR_DXI_OK)
    {
	iSlave2RectNum = 0;

    }
    else
    {
	iSlave2RectNum = pSigValue->varValue.ulValue;
    }

    iEquipID = 641;//Slave3
    iSignalType = 0;
    iSignalID = 2;
    iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	iEquipID,
	iVarSubID,
	&iBufLen,
	(void *)&pSigValue,
	10000);

    if(iError != ERR_DXI_OK)
    {
	iSlave3RectNum = 0;

    }
    else
    {
	iSlave3RectNum = pSigValue->varValue.ulValue;
    }

    int	iSMDUSamplerStatus;

    iEquipID = 1;//SMDU Sampler Status
    iSignalType = 2;
    iSignalID = 183;
    iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	iEquipID,			
	iVarSubID,		
	&iBufLen,			
	(void *)&pSigValue,			
	10000);
    if(iError != ERR_DXI_OK)
    {
	iSMDUSamplerStatus = 0;

    }
    else
    {
	iSMDUSamplerStatus = pSigValue->varValue.ulValue;
    }

    if(!iRectStartDeviceID)
    {
	return FALSE;
    }

    if(iDevice > 0 && iRectNumber < iDevice )
    {
	PRODUCT_INFO	piProductInfo ;
	PRODUCT_INFO	*pSystemProductInfo ;
	//szTProductInfo = NEW(char, iDevice * MAX_PRODUCT_INFO_LEN);
	memset(gs_szBuffer, 0, BUFFER_LEN);
	szTProductInfo = gs_szBuffer;
	/*if(szTProductInfo == NULL)
	{
	    AppLogOut("Web_GetBarcode_Info", APP_LOG_ERROR, "NEW operation fails! Out of memory!\n");
	    return FALSE;
	}*/
	iBufLength = iDevice * MAX_PRODUCT_INFO_LEN;
	pSystemProductInfo = DXI_GetDeviceProductInfo();

	iLen += snprintf(szTProductInfo + iLen, (iBufLength - iLen), "[");
	iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],",
	    (Web_CheckValid(pSystemProductInfo->szDeviceName[0])) ? pSystemProductInfo->szDeviceName[0]: "N/A",
	    (Web_CheckValid(pSystemProductInfo->szDeviceName[1])) ? pSystemProductInfo->szDeviceName[1]: "N/A",
	    (Web_CheckValid(pSystemProductInfo->szPartNumber)) ? pSystemProductInfo->szPartNumber: "N/A",
	    (Web_CheckValid(pSystemProductInfo->szHWVersion)) ? pSystemProductInfo->szHWVersion: "N/A",
	    (Web_CheckValid(pSystemProductInfo->szSerialNumber)) ? pSystemProductInfo->szSerialNumber: "N/A",
	    (Web_CheckValid(pSystemProductInfo->szSWVersion)) ? pSystemProductInfo->szSWVersion: "N/A");

	//120 rectifiers inventory information
	for(i = 0; i < 160; i++)
	{

	    if(g_SiteInfo.stCANRectProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANRectProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANRectProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANRectProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANRectProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANRectProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANRectProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANRectProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANRectProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANRectProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANRectProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANRectProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANRectProductInfo[i].szSWVersion: "N/A");
	    }
	}

	/*for(i = iRectStartDeviceID; i < iRectStartDeviceID + iRectNumber; i++)
	{

	    if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK) 
	    {
		iLen += sprintf(szTProductInfo + iLen,"[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(piProductInfo.szDeviceName[0])) ?piProductInfo.szDeviceName[0]: "N/A",
		    (Web_CheckValid(piProductInfo.szDeviceName[1])) ?piProductInfo.szDeviceName[1]: "N/A",
		    (Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber : "N/A",
		    (Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
		    (Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
		    (Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
	    }
	}*/
	TRACE("%s\n",szTProductInfo);

	//TRACE_WEB_USER("iSMDUNumber is %d\n", iSMDUNumber);
	if(iSMDUNumber > 0) 
	{
	    for(i = 0; i < 8; i++) 
	    {
		//TRACE_WEB_USER("smdu for cycle %d\n", j);
		//if(iSMDUSamplerStatus == 1)
		//{
		//    i = iRectStartDeviceID + 102 + j;
		//}
		//else
		//{
		//    i = iRectStartDeviceID + 100 + j;
		//}
		//if(IsSMDU_Barcode_NeedDisplay(j))
		//{
		//    //TRACE_WEB_USER("smdu IsSMDU_Barcode_NeedDisplay %d\n", j);
		//    if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
		//    {
		//	//TRACE_WEB_USER("smdu DXI_GetPIByDeviceID %d\n", i);
		//	iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		//	    (Web_CheckValid(piProductInfo.szDeviceName[0])) ? piProductInfo.szDeviceName[0]: "N/A",
		//	    (Web_CheckValid(piProductInfo.szDeviceName[1])) ? piProductInfo.szDeviceName[1]: "N/A",
		//	    (Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
		//	    (Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
		//	    (Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
		//	    (Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
		//    }
		//}
		if(g_SiteInfo.stCANSMDUProductInfo[i].bSigModelUsed == TRUE)
		{
		    iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
			(Web_CheckValid(g_SiteInfo.stCANSMDUProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANSMDUProductInfo[i].szDeviceName[0]: "N/A",
			(Web_CheckValid(g_SiteInfo.stCANSMDUProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANSMDUProductInfo[i].szDeviceName[1]: "N/A",
			(Web_CheckValid(g_SiteInfo.stCANSMDUProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANSMDUProductInfo[i].szPartNumber: "N/A",
			(Web_CheckValid(g_SiteInfo.stCANSMDUProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANSMDUProductInfo[i].szHWVersion: "N/A",
			(Web_CheckValid(g_SiteInfo.stCANSMDUProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANSMDUProductInfo[i].szSerialNumber: "N/A",
			(Web_CheckValid(g_SiteInfo.stCANSMDUProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANSMDUProductInfo[i].szSWVersion: "N/A");
		}
	    }
	}
	else
	{

	}

	for(i = iRectStartDeviceID + 110 + 20, j = 0; i <= iDevice && j < iSlave1RectNum; i++,j++)
	{
	    if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(piProductInfo.szDeviceName[0])) ? piProductInfo.szDeviceName[0]: "N/A",
		    (Web_CheckValid(piProductInfo.szDeviceName[1])) ? piProductInfo.szDeviceName[1]: "N/A",
		    (Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
		    (Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
		    (Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
		    (Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
	    }
	}

	for(i = iRectStartDeviceID + 170 + 20, j = 0; i <= iDevice && j < iSlave2RectNum; i++,j++)
	{

	    if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(piProductInfo.szDeviceName[0])) ? piProductInfo.szDeviceName[0]: "N/A",
		    (Web_CheckValid(piProductInfo.szDeviceName[1])) ? piProductInfo.szDeviceName[1]: "N/A",
		    (Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
		    (Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
		    (Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
		    (Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
	    }
	}
	for(i = iRectStartDeviceID + 230 + 20, j = 0; i <= iDevice && j < iSlave3RectNum; i++,j++)
	{

	    if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(piProductInfo.szDeviceName[0])) ? piProductInfo.szDeviceName[0]: "N/A",
		    (Web_CheckValid(piProductInfo.szDeviceName[1])) ? piProductInfo.szDeviceName[1]: "N/A",
		    (Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
		    (Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
		    (Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
		    (Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
	    }
	}

	for(i = 0; i < 8; i++)
	{

	    if(g_SiteInfo.stI2CProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stI2CProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stI2CProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szPartNumber)) ? g_SiteInfo.stI2CProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szHWVersion)) ? g_SiteInfo.stI2CProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szSerialNumber)) ? g_SiteInfo.stI2CProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szSWVersion)) ? g_SiteInfo.stI2CProductInfo[i].szSWVersion: "N/A");
	    }
	}

	for(i = 0; i < 20; i++)
	{

	    if(g_SiteInfo.stCANSMDUPProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANSMDUPProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANSMDUPProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANSMDUPProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANSMDUPProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANSMDUPProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANSMDUPProductInfo[i].szSWVersion: "N/A");
	    }
	}

	//for smduh
	for(i = 0; i < 8; i++)
	{
	    if(g_SiteInfo.stCANSMDUHProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANSMDUHProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANSMDUHProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUHProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANSMDUHProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUHProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANSMDUHProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUHProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANSMDUHProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUHProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANSMDUHProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUHProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANSMDUHProductInfo[i].szSWVersion: "N/A");
	    }
	}

	for(i = 0; i < 8; i++)
	{
	    if(g_SiteInfo.stCANSMDUEProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANSMDUEProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANSMDUEProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUEProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANSMDUEProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUEProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANSMDUEProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUEProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANSMDUEProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUEProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANSMDUEProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMDUEProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANSMDUEProductInfo[i].szSWVersion: "N/A");
			//printf("Web,InventoryData: SMDUE bSigModelUsed == TRUE , i=%d ~~~\n",i);
	    }
	}
	
	//Frank Wu,20150318, for SoNick Battery
	//for(i = 0; i < 66; i++)
	for(i = 0; i < PRODUCT_INFO_485PRODUCTINFO_MAX; i++)
	{
	    //TRACE_WEB_USER("485 for cycle %d\n", i);
	    if(g_SiteInfo.st485ProductInfo[i].bSigModelUsed == TRUE)
	    {
		//TRACE_WEB_USER("485 bSigModelUsed %d\n", i);
		for(k = 0; k < 32; k++)
		{
		    if(g_SiteInfo.st485ProductInfo[i].szSerialNumber[k] == 0xff)
		    {
			g_SiteInfo.st485ProductInfo[i].szSerialNumber[k] = 0x20;
		    }
		}
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szDeviceName[0])) ? g_SiteInfo.st485ProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szDeviceName[1])) ? g_SiteInfo.st485ProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szPartNumber)) ? g_SiteInfo.st485ProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szHWVersion)) ? g_SiteInfo.st485ProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szSerialNumber)) ? g_SiteInfo.st485ProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szSWVersion)) ? g_SiteInfo.st485ProductInfo[i].szSWVersion: "N/A");
	    }
	}

	for(i = 0; i < 60; i++)
	{

	    if(g_SiteInfo.stCANConverterProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANConverterProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANConverterProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANConverterProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANConverterProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANConverterProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANConverterProductInfo[i].szSWVersion: "N/A");
	    }
	}

	for(i = 0; i < 8; i++)
	{

	    if(g_SiteInfo.stCANSMTempProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANSMTempProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANSMTempProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANSMTempProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANSMTempProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANSMTempProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANSMTempProductInfo[i].szSWVersion: "N/A");
	    }
	}
	for(i = 0; i < 60; i++)
	{

	    if(g_SiteInfo.stCANLiBattProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANLiBattProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANLiBattProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANLiBattProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANLiBattProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANLiBattProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANLiBattProductInfo[i].szSWVersion: "N/A");
	    }
	}
	
	if(g_SiteInfo.stCANLiBridgeCardProductInfo.bSigModelUsed == TRUE)
	{
	    iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[0])) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[0]: "N/A",
		(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[1])) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[1]: "N/A",
		(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szPartNumber)) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szPartNumber: "N/A",
		(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szHWVersion)) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szHWVersion: "N/A",
		(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szSerialNumber)) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szSerialNumber: "N/A",
		(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szSWVersion)) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szSWVersion: "N/A");
	}

#ifdef GC_SUPPORT_MPPT
	for(i = 0; i < 30; i++)//Can
	{

	    if(g_SiteInfo.stCANMpptProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANMpptProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANMpptProductInfo[i].szDeviceName[1]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANMpptProductInfo[i].szPartNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANMpptProductInfo[i].szHWVersion: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANMpptProductInfo[i].szSerialNumber: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANMpptProductInfo[i].szSWVersion: "N/A");
	    }
	}
#endif	
	//for dcem
	for(i = 0; i < 8; i++)
	{
	    if(g_SiteInfo.stCANDCEMProductInfo[i].bSigModelUsed == TRUE)
	    {
		iLen += snprintf(szTProductInfo + iLen,(iBufLength - iLen), "[[\"%16s\",\"%16s\"],\"%16s\",\"%16s\",\"%32s\",\"%16s\"],", 
		    (Web_CheckValid(g_SiteInfo.stCANDCEMProductInfo[i].szDeviceName[0])) ? g_SiteInfo.stCANDCEMProductInfo[i].szDeviceName[0]: "N/A",
		    (Web_CheckValid(g_SiteInfo.stCANDCEMProductInfo[i].szDeviceName[1])) ? g_SiteInfo.stCANDCEMProductInfo[i].szDeviceName[1]: "N/A",
		    /*(Web_CheckValid(g_SiteInfo.stCANDCEMProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANDCEMProductInfo[i].szPartNumber: */"N/A",
		    /*(Web_CheckValid(g_SiteInfo.stCANDCEMProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANDCEMProductInfo[i].szHWVersion: */"N/A",
		    /*(Web_CheckValid(g_SiteInfo.stCANDCEMProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANDCEMProductInfo[i].szSerialNumber: */"N/A",
		    /*(Web_CheckValid(g_SiteInfo.stCANDCEMProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANDCEMProductInfo[i].szSWVersion: */"N/A");
	    }
	}

	if(iLen >= 1)
	{
	    iLen = iLen - 1;	// get rid of the last "," to meet json format
	}
	iLen += snprintf(szTProductInfo + iLen, (iBufLength - iLen), "]");
	//TRACE_WEB_USER("\n%d %d szTProductInfo is %s\n", iDevice, iRectNumber, szTProductInfo);
	*szProductInfo = szTProductInfo;
	return TRUE;
    }
    return FALSE;
}

void Web_MakeInventoryDataFile(void)
{
#define ID_INVENTORY		"ID_INVENTORY"
    char	*pReturnBuf = NULL;
    //char *szFile = NULL;
    char *pHtml = NULL;
    int	 iReturn = 0;
    FILE	*fp;

    TRACE_WEB_USER("Begin to exec Web_MakeInventoryDataFile\n");

    /*szFile = NEW(char, 128);
    if(szFile == NULL)
    {
    AppLogOut("Web_MakeInventoryDataFile", APP_LOG_ERROR, "NEW operation fails! Out of memory!");
    return;
    }
    sprintf(szFile, "%s", WEB_DATA_INVENTORY);*/
    iReturn = LoadHtmlFile(WEB_DATA_INVENTORY, &pHtml);
    if(iReturn > 0)
    {
	Web_GetBarcode_Info(&pReturnBuf);
	if(pReturnBuf != NULL)
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_INVENTORY), pReturnBuf);
	    /*DELETE(pReturnBuf);
	    pReturnBuf = NULL;*/
	}
	else
	{
	    ReplaceString(&pHtml, MAKE_VAR_FIELD(ID_INVENTORY), "[]");
	}
	/*memset(szFile, 0, sizeof(szFile));
	sprintf(szFile, "%s", WEB_DATA_INVENTORY_VAR);*/
	if((fp = fopen(WEB_DATA_INVENTORY_VAR,"wb")) != NULL && pHtml != NULL)
	{
	    fwrite(pHtml,strlen(pHtml), 1, fp);
	    fclose(fp);
	}
	else
	{}
    }
    else
    {}

    /*if(szFile)
    {
    DELETE(szFile);
    szFile = NULL;
    }*/
    if(pHtml)
    {
	DELETE(pHtml);
	pHtml = NULL;
    }
    TRACE_WEB_USER("end to exec Web_MakeInventoryDataFile\n");
}

/*==========================================================================*
* FUNCTION :  GetEquipNum
* PURPOSE  :  Get the number of all kinds of battery, DC, LVD, rectifier, converter and MPPT, this equip number is variable
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  The result is for global variable
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-27
*==========================================================================*/
static void GetEquipNum(void)
{
#define ID_RECTIFIER_GROUP		2
#define ID_CONVERTER_GROUP		210
#define ID_MPPT_GROUP			1350
#define ID_MODULE_NUM			8
#define ID_ALL_MODULE_NUM		36
#define ID_RECTIFIERS1_GROUP		639
#define ID_RECTIFIERS2_GROUP		640
#define ID_RECTIFIERS3_GROUP		641
#define ID_MODULE_SLAVE_NUM		2

    TRACE_WEB_USER("Begin to get the equip number\n");

	Web_GetFuelUnitEquipNum();//changed by Frank Wu,1/1/4,20140723, for scanning FuelUnitEquipment
	Web_GetSystemEquipNum();//changed by Frank Wu,N/36/38,20140730, for adding new web pages to the tab of "power system"
    Web_GetBattNum();
    Web_GetLVDEquipNum();
    Web_GetSMDUBattFuseEquipNum();
    gs_uiRectifierNumber = Web_GetModuleNumber(ID_RECTIFIER_GROUP, ID_MODULE_NUM);
    gs_uiAllRectifierNumber = Web_GetModuleNumber(ID_RECTIFIER_GROUP, ID_ALL_MODULE_NUM);
    gs_uiRectifierS1Number = Web_GetModuleNumber(ID_RECTIFIERS1_GROUP, ID_MODULE_SLAVE_NUM);
    gs_uiRectifierS2Number = Web_GetModuleNumber(ID_RECTIFIERS2_GROUP, ID_MODULE_SLAVE_NUM);
    gs_uiRectifierS3Number = Web_GetModuleNumber(ID_RECTIFIERS3_GROUP, ID_MODULE_SLAVE_NUM);
    gs_uiConverterNumber = Web_GetModuleNumber(ID_CONVERTER_GROUP, ID_MODULE_NUM);
    gs_uiSolarNumber = Web_GetModuleNumber(ID_MPPT_GROUP, ID_MODULE_NUM);
    TRACE_WEB_USER("End to get the equip number\n");
}

/*==========================================================================*
* FUNCTION :  Web_Notification
* PURPOSE  :  To deal with the notification
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-08-20
*==========================================================================*/
void Web_Notification(IN int *iQuitCommand)
{
    BOOL	bContinueRunning = TRUE;

    while(bContinueRunning)
    {
	/*if(gs_iLoadCurrMask)
	{
	    TRACE_WEB_USER_NOT_CYCLE("Enter Web User ServiceNotification function [_LOAD_CURRENT_MASK]\n");
	    Web_InsertLoadCurrentPoint(gs_pParam);
	    gs_iLoadCurrMask = 0;
	}*/
	//if(Mutex_Lock(g_hMutexSetDataFile, MAX_TIME_WAITING) == ERR_MUTEX_OK)
	//{
	//    if(gs_iWebSetMask)
	//    {
	//	TRACE_WEB_USER_NOT_CYCLE("Enter Web User Web_Notification function\n");
	//	Web_GetLVDEquipNum();
	//	gs_uiRectifierNumber = Web_GetModuleNumber(ID_RECTIFIER_GROUP, ID_MODULE_NUM);
	//	Web_GetSettingSignalPointer();	// Consider the LVD number may be changed, when you get the setting json file, you must re-initial the setting signal pointer before
	//	Web_MakeSettingDataFile();
	//	Web_MakeWizardDataFile();
	//	Web_MakeRectSetDataFile();
	//	Sleep(10000);//yield
	//	Web_GetSettingSignalPointer();
	//	Web_MakeSettingDataFile();
	//	Web_MakeWizardDataFile();
	//	Web_MakeRectSetDataFile();
	//	gs_iWebSetMask = 0;
	//	TRACE_WEB_USER_NOT_CYCLE("Exit Web User Web_Notification function\n");
	//    }
	//    Mutex_Unlock(g_hMutexSetDataFile);
	//}
	//else
	//{
	//    TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexSetDataFile in Web_Notification\n");
	//    AppLogOut("Get lock", APP_LOG_WARNING, "Can't get the lock of g_hMutexSetDataFile in Web_Notification!\n");
	//}
	if(*iQuitCommand == SERVICE_STOP_RUNNING)
	{
	    bContinueRunning = FALSE;
	}
	Sleep(1000);//yield
    }

    TRACE_WEB_USER_NOT_CYCLE("***********************************end to Web_Notification!*******************************************\n");

    return;
}

/*==========================================================================*
* FUNCTION :  FreeMemory
* PURPOSE  :  To free the memory
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-09-05
*==========================================================================*/
static void FreeMemory(void)
{
    int i;

    for(i = 0; i < 6; i++)
    {
	if(gs_pWebRectSigInfo[i] != NULL)
	{
	    if(gs_pWebRectSigInfo[i]->stWebPrivate != NULL)
	    {
		DELETE(gs_pWebRectSigInfo[i]->stWebPrivate);
		gs_pWebRectSigInfo[i]->stWebPrivate = NULL;
	    }
	    if(gs_pWebRectSigInfo[i]->stWebEquipInfo != NULL)
	    {
		DELETE(gs_pWebRectSigInfo[i]->stWebEquipInfo);
		gs_pWebRectSigInfo[i]->stWebEquipInfo = NULL;
	    }
	}

	if(gs_pWebBattSigInfo[i] != NULL)
	{
	    if(gs_pWebBattSigInfo[i]->stWebPrivate != NULL)
	    {
		DELETE(gs_pWebBattSigInfo[i]->stWebPrivate);
		gs_pWebBattSigInfo[i]->stWebPrivate = NULL;
	    }
	    if(gs_pWebBattSigInfo[i]->stWebEquipInfo != NULL)
	    {
		DELETE(gs_pWebBattSigInfo[i]->stWebEquipInfo);
		gs_pWebBattSigInfo[i]->stWebEquipInfo = NULL;
	    }
	}

	if(gs_pWebOtherSigInfo[i] != NULL)
	{
	    if(gs_pWebOtherSigInfo[i]->stWebPrivate != NULL)
	    {
		DELETE(gs_pWebOtherSigInfo[i]->stWebPrivate);
		gs_pWebOtherSigInfo[i]->stWebPrivate = NULL;
	    }
	    if(gs_pWebOtherSigInfo[i]->stWebEquipInfo != NULL)
	    {
		DELETE(gs_pWebOtherSigInfo[i]->stWebEquipInfo);
		gs_pWebOtherSigInfo[i]->stWebEquipInfo = NULL;
	    }
	}

	if(gs_pWebRectSigInfo[i] != NULL)
	{
	    DELETE(gs_pWebRectSigInfo[i]);
	    gs_pWebRectSigInfo[i] = NULL;
	}
	if(gs_pWebOtherSigInfo[i] != NULL)
	{
	    DELETE(gs_pWebOtherSigInfo[i]);
	    gs_pWebOtherSigInfo[i] = NULL;
	}
	if(gs_pWebBattSigInfo[i] != NULL)
	{
	    DELETE(gs_pWebBattSigInfo[i]);
	    gs_pWebBattSigInfo[i] = NULL;
	}
    }
    for(i = 0; i < 5; i++)
    {
	if(gs_pWebDCSigInfo[i] != NULL)
	{
	    if(gs_pWebDCSigInfo[i]->stWebPrivate != NULL)
	    {
		DELETE(gs_pWebDCSigInfo[i]->stWebPrivate);
		gs_pWebDCSigInfo[i]->stWebPrivate = NULL;
	    }
	    if(gs_pWebDCSigInfo[i]->stWebEquipInfo != NULL)
	    {
		DELETE(gs_pWebDCSigInfo[i]->stWebEquipInfo);
		gs_pWebDCSigInfo[i]->stWebEquipInfo = NULL;
	    }
	}

	if(gs_pWebDCSigInfo[i] != NULL)
	{
	    DELETE(gs_pWebDCSigInfo[i]);
	    gs_pWebDCSigInfo[i] = NULL;
	}
    }

    /*if(gs_pWebSetInfo[LVD_SEQ].stWebEquipInfo != NULL)
    {
	DELETE(gs_pWebSetInfo[LVD_SEQ].stWebEquipInfo);
	gs_pWebSetInfo[LVD_SEQ].stWebEquipInfo = NULL;
    }*/

    for(i = 0; i < WEB_MAX_HTML_PAGE_SIGNAL_NUM; i++)
    {
	    if(gs_pWebSigInfo[i].stWebPrivate != NULL)
	    {
		DELETE(gs_pWebSigInfo[i].stWebPrivate);
		gs_pWebSigInfo[i].stWebPrivate = NULL;
	    }
    }

    for(i = 0; i < WEB_MAX_HTML_PAGE_SETTING_NUM; i++)
    {
	    if(gs_pWebSetInfo[i].stWebPrivateSet != NULL)
	    {
		DELETE(gs_pWebSetInfo[i].stWebPrivateSet);
		gs_pWebSetInfo[i].stWebPrivateSet = NULL;
	    }
	    if(gs_pWebSetInfo[i].stWebPrivateSet1 != NULL)
	    {
		DELETE(gs_pWebSetInfo[i].stWebPrivateSet1);
		gs_pWebSetInfo[i].stWebPrivateSet1 = NULL;
	    }
	    if(gs_pWebSetInfo[i].stWebEquipInfo != NULL)
	    {
		DELETE(gs_pWebSetInfo[i].stWebEquipInfo);
		gs_pWebSetInfo[i].stWebEquipInfo = NULL;
	    }
    }

    if(gs_pWebDisplayInfo[0].stWebDisplaySet != NULL)
    {
	DELETE(gs_pWebDisplayInfo[0].stWebDisplaySet);
	gs_pWebDisplayInfo[0].stWebDisplaySet = NULL;
    }

    if(gs_pWebDisplaySamInfo[0].stWebDisplaySet != NULL)
    {
	DELETE(gs_pWebDisplaySamInfo[0].stWebDisplaySet);
	gs_pWebDisplaySamInfo[0].stWebDisplaySet = NULL;
    }

    if(gs_stConsumMapInfo.pstCabInfo != NULL)
    {
	DELETE(gs_stConsumMapInfo.pstCabInfo);
	gs_stConsumMapInfo.pstCabInfo = NULL;
    }

    if(gs_pWebCabPowerData != NULL)
    {
	DELETE(gs_pWebCabPowerData);
	gs_pWebCabPowerData = NULL;
    }
}

void Web_InitCurrandVoltPoint(void)
{
#define ID_SYS_VOLT "ID_SYS_VOLT"
#define ID_SYS_CURR "ID_SYS_CURR"
#define ID_CON_VOLT "ID_CON_VOLT"
#define ID_CON_CURR "ID_CON_CURR"
#define ID_TIME_FORMAT		"ID_TIME_FORMAT"

    int i;

    for(i = 0; i < gs_pWebSigInfo[INDEX_SEQ].iNumber; i++)
    {
		if(strcmp((const char*)(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].szResourceID), ID_SYS_VOLT) == 0)
		{
			pstSysVolt = &(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i]);
		}
		else if(strcmp((const char*)(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].szResourceID), ID_SYS_CURR) == 0)
		{
			pstSysCurr = &(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i]);
		}
		if(strcmp((const char*)(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].szResourceID), ID_CON_VOLT) == 0)
		{
			pstConVolt = &(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i]);
		}
		else if(strcmp((const char*)(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].szResourceID), ID_CON_CURR) == 0)
		{
			pstConCurr = &(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i]);
		}
		//time format signal for alarm json
		else if(strcmp((const char*)(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i].szResourceID), ID_TIME_FORMAT) == 0)
		{
			pstTimeFor = &(gs_pWebSigInfo[INDEX_SEQ].stWebPrivate[i]);
		}
    }

    return;
}

/*==========================================================================*
* FUNCTION :  calPeakpower
* PURPOSE  :  Calculate the peak power in past 24 hours, in past 7 days and in past one month for each cabinet
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2014-05-15
*==========================================================================*/
void calPeakpower(void)
{
    float fPeakPower;
    int iBeginPoint;
    int i, j;

    for(i = 0; i < gs_stConsumMapInfo.iNumber; i++)//for every cabinet
    {
	if(gs_stConsumMapInfo.pstCabInfo[i].iConfig == TRUE)
	{
	    // calculate the last 24 hours peak value
	    fPeakPower = 0;
	    if(gs_pWebCabPowerData[i].iSeq <= DAY1_HOUR)
	    {
		iBeginPoint = 0;
	    }
	    else
	    {
		iBeginPoint = gs_pWebCabPowerData[i].iSeq - DAY1_HOUR;
	    }
	    for(j = iBeginPoint; j < (iBeginPoint + DAY1_HOUR); j++)
	    {
		if(gs_pWebCabPowerData[i].f30DayPower[j] > fPeakPower)
		{
		    fPeakPower = gs_pWebCabPowerData[i].f30DayPower[j];
		}
	    }
	    if(gs_pWebCabPowerData[i].iSeq <= DAY1_HOUR)//the controller hasn't run for one day after reset
	    {
		if(gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLast24H < fPeakPower)
		{
		    gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLast24H = fPeakPower;
		}
	    }
	    else
	    {
		gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLast24H = fPeakPower;
	    }

	    // calculate the last 7 days peak value
	    fPeakPower = 0;
	    if(gs_pWebCabPowerData[i].iSeq <= DAY7_HOUR)
	    {
		iBeginPoint = 0;
	    }
	    else
	    {
		iBeginPoint = gs_pWebCabPowerData[i].iSeq - DAY7_HOUR;
	    }
	    for(j = iBeginPoint; j < (iBeginPoint + DAY7_HOUR); j++)
	    {
		if(gs_pWebCabPowerData[i].f30DayPower[j] > fPeakPower)
		{
		    fPeakPower = gs_pWebCabPowerData[i].f30DayPower[j];
		}
	    }
	    if(gs_pWebCabPowerData[i].iSeq <= DAY7_HOUR)//the controller hasn't run for 7 days after reset
	    {
		if(gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLastWeek < fPeakPower)
		{
		    gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLastWeek = fPeakPower;
		}
	    }
	    else
	    {
		gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLastWeek = fPeakPower;
	    }

	    // calculate the last 30 days peak value
	    for(j = 0; j < DAY30_HOUR; j++)
	    {
		if(gs_pWebCabPowerData[i].f30DayPower[j] > fPeakPower)
		{
		    fPeakPower = gs_pWebCabPowerData[i].f30DayPower[j];
		}
	    }
	    if(gs_pWebCabPowerData[i].iSeq < DAY30_HOUR)//the controller hasn't run for 30 days after reset
	    {
		if(gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLastMonth < fPeakPower)
		{
		    gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLastMonth = fPeakPower;
		}
	    }
	    else
	    {
		gs_stConsumMapInfo.pstCabInfo[i].fPeakPowLastMonth = fPeakPower;
	    }
	}
    }
    UpdateConsumptionMapInfoToFlash();
}

void Web_MakeCurveDataFileForTempChanged()
{
	int iVarSubID, iError, iBufLen;
	int iTemperFormat;
	static int iTemperFormatLast = 0;
	static BOOL bInit = FALSE;
	static SIG_BASIC_VALUE* pSigValueTempSwitch;

	if(bInit ==  FALSE)
	{
		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 498);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			ID_SYSTEM,
			iVarSubID,
			&iBufLen,
			(void *)&pSigValueTempSwitch,
			0);
		bInit = TRUE;
	}

	if(pSigValueTempSwitch != NULL)
	{
		iTemperFormat = pSigValueTempSwitch->varValue.enumValue;
		if(iTemperFormatLast != iTemperFormat)
		{
			Web_MakeCurveDataFile();
			iTemperFormatLast = iTemperFormat;
		}
	}
}

/*==========================================================================*
* FUNCTION :  Web_GetPaswdChangeFlag
* PURPOSE  :  
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  int, 0--prompt; 1-- not need prompt
* COMMENTS :  
* CREATOR  :                 DATE: 
*==========================================================================*/
int Web_GetPaswdChangeFlag(void)
{

#define	PASS_CHANGE_FILE_ROUTE		"/home/app_script/pass_change_flag.log"

	int iAccess;
	iAccess = access(PASS_CHANGE_FILE_ROUTE, F_OK);
	if( 0 == iAccess )
	{
		TRACEX("access: File exist~~~~~~~~~~~~~~~~~~~~~~##########################~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

		char line [256];
		FILE	*fp;

		fp = fopen (PASS_CHANGE_FILE_ROUTE, "r");
		

		if (fp == NULL)
		{
			fp = fopen (PASS_CHANGE_FILE_ROUTE, "r");
			if (fp == NULL)
			{
				TRACEX("cannot open /home/app_script/pass_change_flag.log- burps\n\r");
				return -1;
			}
		}

		while (fgets (line, sizeof(line), fp))
		{
			if (strstr(line, "yes")) 
			{
				TRACEX("strstr: yes~~, Time = %d\n",time(NULL));
				fclose(fp);
				return 0;
	 		}
	 		else if (strstr(line, "no"))
	 		{
	 			TRACEX("strstr: no~~\n");
	 			fclose(fp);
	 			return 1;
	 		}
	 		else
	 		{
	 			TRACEX("strstr: other~~\n");
	 			fclose(fp);
	 			return 1;
	 		}
		}
	}
	else
	{
		TRACEX("access: File not exist~~~~~~~~~~~~~~~~~~~~~~!!!!!!!!!!!!!!!!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		return 1; 
	}
}


/*==========================================================================*
* FUNCTION :  Web_MakeDataFile
* PURPOSE  :  To make the json file used by webpages
* CALLS    :  
* CALLED BY: 
* ARGUMENTS:  void
* RETURN   :  void
* COMMENTS :  
* CREATOR  :  Zhao Zicheng               DATE: 2013-07-20
*==========================================================================*/
void Web_MakeDataFile(IN int *iQuitCommand)
{
    TRACE_WEB_USER_NOT_CYCLE("Begin to make file data\n");
    BOOL	bContinueRunning = TRUE;
    int iError;
    time_t toTime, toTimeLast, toTimeInterval, toTimeLast1, toTimeInterval1;
    //static long s_lVoltLevel = 0;
    static unsigned int s_uiRectifierNum = 0;
    static unsigned int s_uiRectifierS1Num = 0;
    static unsigned int s_uiRectifierS2Num = 0;
    static unsigned int s_uiRectifierS3Num = 0;
    static unsigned int s_uiConverterNum = 0;
    static unsigned int s_uiSolarNum = 0;
    static unsigned int s_uiCfgQtyOfComBAT = 0;
    static unsigned int s_uiCfgQtyOfEIBBAT = 0;
    static unsigned int s_uiCfgQtyOfSmduBAT = 0;
    static unsigned int s_uiCfgQtyOfSmdueBAT = 0;
    static unsigned int s_uiCfgQtyOfSMBAT = 0;
    static unsigned int s_uiCfgQtyOfLargeDUBAT = 0;
    static unsigned int s_uiCfgQtyOfSMBRC = 0;
    static unsigned int s_uiCfgQtyOfSoNick = 0;
    static unsigned int s_uiCfgQtyOfSMDU = 0;
    static unsigned int s_uiCfgQtyOfSMDUP = 0;
    static unsigned int s_uiCfgQtyOfSMDUE = 0;
    static unsigned int s_uiCfgQtyOfSMDUH = 0;
    static unsigned int s_uiCfgQtyOfLVD = 0;
    static unsigned int s_uiCfgSMDUBattFuseNum = 0;
    static unsigned int s_uiCfgLVD3Num = 0;
	//changed by Frank Wu,2/1/4,20140723, for scanning FuelUnitEquipment
	static unsigned int s_uiCfgQtyOfSetSMLVDUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetFuelUnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetOBFuelUnitNum = 0;
	//changed by Frank Wu,N/37/38,20140730, for adding new web pages to the tab of "power system"
	static unsigned int s_uiCfgQtyOfSetOBBattFuseUnitNum = 0;
	//changed by Frank Wu,N/N/N,20150429, for scanning LVD3
	static unsigned int s_uiCfgQtyOfSetLVD3UnitNum = 0;
	static unsigned int s_uiCfgQtyOfSetNaradaBMSGroupNum = 0;

	
	
	BOOL bIsGetSettingSignalPointerFail = FALSE;
	int iGetSettingPointerTryCount = 0;

    SIG_BASIC_VALUE* pSigValue;
    int	iVarSubID = 0;
    int	iBufLen = 0;
    int iTimeCount;
    int iTimeOut = 0;
    int iBatteryCount = 0;
    char        szTime[32];
    char szCommand[128];
    HANDLE hCmdThread;
#ifdef		WEB_USER_COST
    struct timeval t_start,t_end;
    long cost_time = 0;
    long start, end;
    struct timeval t_start1,t_end1;
    long cost_time1 = 0;
    long start1, end1;
    struct timeval t_start2,t_end2;
    long cost_time2 = 0;
    long start2, end2;
#endif

    //   iBufLen = sizeof(HIS_DATA_RECORD_LOAD);
    //   iError = DxiSetData(VAR_HIS_DATA_RECORD_LOAD,
    //0,
    //0,
    //iBufLen,
    //(void *)&g_stLoadCurrentData,//gsite_info��ʼ��ʱ��ֵ��
    //iTimeOut);
    //   if(iError != ERR_DXI_OK)
    //   {
    ////logview��¼
    //TRACE_WEB_USER("SET VAR_HIS_DATA_RECORD_LOAD Error\n");
    ////return;
    //   }
#ifdef G3_OPT_DEBUG_THREAD
    AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
	"Thread Jason Service was created, Thread Id = %d.\n", gettid());
#endif

    iError = WEB_ReadSignal();
    if(iError != ERR_WEB_OK)
    {
	TRACE_WEB_USER_NOT_CYCLE("WEB_ReadSignal Error\n");
	AppLogOut("Web_MakeDataFile", APP_LOG_ERROR, "WEB_ReadSignal Error[%d]\n", iError);
	return;
    }

    iError = WEB_ReadSettingSignal();
    if(iError != ERR_WEB_OK)
    {
	AppLogOut("Web_MakeDataFile", APP_LOG_ERROR, "WEB_ReadSettingSignal Error[%d]\n", iError);
	TRACE_WEB_USER_NOT_CYCLE("WEB_ReadSettingSignal Error\n");
	return;
    }

    iError = WEB_ReadConsumptionFile();
    if(iError != ERR_WEB_OK)
    {
	AppLogOut("Web_MakeDataFile", APP_LOG_ERROR, "WEB_ReadConsumptionFile Error[%d]\n", iError);
	TRACE_WEB_USER_NOT_CYCLE("WEB_ReadConsumptionFile Error\n");
	return;
    }

    //Sleep(30000);//wait for 30 seconds, wait the data to be ready
    int	iRst;
    unsigned int uiRunFlag = 0;
    RUN_THREAD_MSG msg;

    //Read the file <LoadAllEquip.run>
    char szFullPath[MAX_FILE_PATH];
    Cfg_GetFullConfigPath(LOADALLEQUIP_FLAGFILE, szFullPath, MAX_FILE_PATH);
    if(access(szFullPath, F_OK) == 0) //find the flag file
    {
	TRACE_WEB_USER_NOT_CYCLE("The file <NotLoadRS485.run>  exist!\n");
	uiRunFlag |= 0x02;	
    }
    else
    {
	TRACE_WEB_USER_NOT_CYCLE("The file <NotLoadRS485.run> not exists!\n");
    }



	//For ESNA Paswd change CR
	//gs_iPaswdChangeFlag = Web_GetPaswdChangeFlag();
	//printf("Web_MakeDataFile():iPaswdChangeFlag=%d~~\n",gs_iPaswdChangeFlag);



    // wait for the data to be ready
    iTimeCount = 0;
    while(1)
    {
		iRst = RunThread_GetMessage(RunThread_GetId(NULL), &msg, FALSE, 0);
		if(iRst == ERR_OK)
		{
		    if(msg.dwMsg == MSG_CAN_SAMPLER_FINISH_SCAN)
		    {
				TRACE_WEB_USER_NOT_CYCLE("Receive MSG_CAN_SAMPLER_FINISH_SCAN!\n");
				uiRunFlag |= 0x01;
				printf("=======Web: Receive the MSG_CAN_SAMPLER_FINISH_SCAN, Now time is %d,uiRunFlag=%d\n",time(NULL),uiRunFlag);
				AppLogOut("Web_MakeDataFile", APP_LOG_INFO, "=======Web: Receive the MSG_CAN_SAMPLER_FINISH_SCAN, Now time is %d,uiRunFlag=%d\n",time(NULL),uiRunFlag);
		    }
	    	else if(msg.dwMsg == MSG_485_SAMPLER_FINISH_SCAN)
	    	{
				TRACE_WEB_USER_NOT_CYCLE("Receive MSG_485_SAMPLER_FINISH_SCAN!\n");
				uiRunFlag |= 0x02;
				printf("=======Web: Receive the MSG_485_SAMPLER_FINISH_SCAN, Now time is %d~~~\n",time(NULL));
				AppLogOut("Web_MakeDataFile", APP_LOG_INFO, "=======Web: Receive the MSG_485_SAMPLER_FINISH_SCAN, Now time is %d~~~\n",time(NULL));
	    	}
	    	else
	    	{
	    	}
		}
		else
		{
		}

		//wait for 120 seconds for CAN at max.
		if(iTimeCount > 120)
		{
			uiRunFlag |= 0x01;
			printf("=======Web :TimeOUT, iTimeCount=%d,uiRunFlag=%d,time(NULL)=%d~~\n",iTimeCount,uiRunFlag,time(NULL));
			AppLogOut("Web_MakeDataFile", APP_LOG_ERROR, "=======Web :TimeOUT, iTimeCount=%d,uiRunFlag=%d,time(NULL)=%d~~\n",iTimeCount,uiRunFlag,time(NULL));
		}
		if((uiRunFlag == 0x03) || (iTimeCount > 300))
		{
			printf("=======Web: Break,iTimeCount=%d,uiRunFlag=%d, Now time is %d~~~\n",iTimeCount,uiRunFlag,time(NULL));
			AppLogOut("Web_MakeDataFile", APP_LOG_INFO, "=======Web: Break,iTimeCount=%d,uiRunFlag=%d, Now time is %d~~~\n",iTimeCount,uiRunFlag,time(NULL));
		    break;
		}
		else
		{
			if( iTimeCount == 0 )		
			{
				printf("=======Web : iTimeCount=%d,uiRunFlag=%d,time(NULL)=%d~~\n",iTimeCount,uiRunFlag,time(NULL));
				AppLogOut("Web_MakeDataFile", APP_LOG_INFO, "=======Web : iTimeCount=%d,uiRunFlag=%d,time(NULL)=%d~~\n",iTimeCount,uiRunFlag,time(NULL));
			}
		    iTimeCount++;
		    TRACE_WEB_USER("Wait......\n");
		    Sleep(1000);//yield
		}
    }	

    AppLogOut("Web_MakeDataFile", APP_LOG_MILESTONE, "**********Now begin to create the web pages!*******************\n");
    TRACE_WEB_USER_NOT_CYCLE("***************************Now begin to create the web pages!************************************\n");
    //Sleep(60000);
    strcpy(szCommand,"cp /app/www_user/html/datas/data.loginsubmit.html /var/datas");
    _SYSTEM(szCommand);
	Web_MakeLangOptionBuf();
    GetEquipNum();
    Web_GetFixEquipNum();
    Web_GetOtherEquipNum();
    iError = Web_GetSignalPointer();
    if(iError != ERR_DXI_OK)
    {
	TRACE_WEB_USER_NOT_CYCLE("Web_GetSignalPointer Error\n");
	AppLogOut("Web_MakeDataFile", APP_LOG_ERROR, "Web_GetSignalPointer Error[%d]\n", iError);
	return;
    }

    TRACE_WEB_USER_NOT_CYCLE("Read the consumption map info from flash!\n");
    GetBranchInfoFromFlash();
    GetConsumptionMapInfoFromFlash();

    Web_InitCurrandVoltPoint();

    toTimeLast = time((time_t *)0);
    toTimeLast1 = time((time_t *)0);
    iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ID_PROTOCOL_TYPE1);
    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						1,
						iVarSubID,
						&iBufLen,
						(void *)&pSigValue,
						iTimeOut);
    if(iError == ERR_DXI_OK)
    {
		gs_lProtocolType = pSigValue->varValue.enumValue;
    }
    else
    {
		gs_lProtocolType = 0;
    }
    
    Web_MakeHisDataInit();
    Web_MakeCurveDataInit(EQUIP_AC_GROUP, SIGNAL_AC_CURRENT);	//initial AC trend curve data
    Web_MakeCurveDataInit(EQUIP_DG_GROUP, SIGNAL_DG_CURRENT);	//initial DG trend curve data
    Web_MakeCurveDataInit(EQUIP_SOLAR_GROUP, SIGNAL_SOLAR_CURRENT);	//initial solar trend curve data
    Web_MakeCurveDataInit(EQUIP_SYSTEM, SIGNAL_AMBIENT_TEMP);
    Web_MakeCurveDataInit(EQUIP_BATT_GROUP, SIGNAL_COMP_TEMP);
    Web_MakeCurveDataInit(EQUIP_BATTERY1, SIGNAL_CAP_PERCENTAGE);
    Web_MakeCurveDataInit(EQUIP_BATTERY2, SIGNAL_CAP_PERCENTAGE);
    //Web_MakeCurveDataInit(EQUIP_WIND_GROUP, SIGNAL_WIND_CURRENT);	//initial wind trend curve data

    Web_MakeLoadDataFile();
    
    Web_MakeCurveDataFile();
    
    Web_MakeAlarmDataFile();

    Web_MakeHislogDataFile();

    Web_MakeInventoryDataFile();

//#ifdef		WEB_USER_COST
//    gettimeofday(&t_start, NULL); 
//    start = ((long)t_start.tv_sec)*1000+(long)t_start.tv_usec/1000;
//#endif

    Web_MakeSettingDataFile();

    //Web_MakeWizardDataFile();
    Web_MakeRectSetDataFile();

	//changed by Frank Wu,22/N/30,20140527, for add single converter and single solar settings pages
	Web_MakeConverterSetDataFile();

	//changed by Frank Wu,21/N/35,20140527, for adding the the web setting tab page 'DI'
	Web_MakeDIDataFile();

	Web_MakeDODataFile();

	Web_MakeSourceDetailDataFile();

    Web_MakeSysSetDataFile();

    Web_MakeShuntDataFile();

	Web_MakeShuntDataFile_Sec();

	Web_MakeCustom_InputsDataFile();

	Web_MakeAnalogDataFile();

	Web_MakeCurveDataFileForTempChanged();

//changed by Stone Song,20160525, for adding the the web setting tab page 'FUSE'
	Web_MakeFuseDataFile();

//#ifdef		WEB_USER_COST
//    gettimeofday(&t_end, NULL);
//    end = ((long)t_end.tv_sec)*1000+(long)t_end.tv_usec/1000;
//    cost_time = end - start;
//    TRACE_WEB_USER("Cost time of Web_MakeSettingDataFile: %ld ms\n", cost_time);
//#endif

    DxiRegisterNotification("Web User", 
								ServiceNotification,
								NULL, // it shall be changed to the actual arg
								(/*_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK | */_WEB_SET_MASK | _CONFIG_MASK | _LOAD_CURRENT_MASK | _CLEAR_HISTORY_MASK), 
								RunThread_GetId(NULL),
								_REGISTER_FLAG);

    while(bContinueRunning)
    {
		if(dataRefreshFlag == 0)
		{
			Sleep(1000);
			Web_CalCabinetData();
			Web_MakeLoginDataFile();

			//printf("dataRefreshFlag == 0 \n");
		}
		else
		{
			//printf("dataRefreshFlag == 1 \n");

#ifdef		WEB_USER_COST
	gettimeofday(&t_start, NULL); 
	start = ((long)t_start.tv_sec)*1000+(long)t_start.tv_usec/1000;
#endif
		/*if(Mutex_Lock(g_hMutexSetDataFile, MAX_TIME_WAITING) == ERR_MUTEX_OK)
			{*/
	    		GetEquipNum();
	    		
	    	/*Mutex_Unlock(g_hMutexSetDataFile);
			}
			else
			{
	    			TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexSetDataFile in [GetEquipNum] of Web_MakeDataFile\n");
	    			AppLogOut("Get lock", APP_LOG_WARNING, "Can't get the lock of g_hMutexSetDataFile in Web_MakeDataFile about [GetEquipNum]!\n");
			}*/
			/*TRACE_WEB_USER("gs_uiRealComBattNum is %d	gs_uiRealEIBBattNum is %d	gs_uiRealSMDUBattNum is %d	gs_uiRealSMBattNum is %d\
			gs_uiRealLargeDUBattNum is %d	gs_uiRealSMBRCBattNum is %d\n", gs_uiRealComBattNum, gs_uiRealEIBBattNum, gs_uiRealSMDUBattNum, gs_uiRealSMBattNum,
			gs_uiRealLargeDUBattNum, gs_uiRealSMBRCBattNum);*/
			/*iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, ID_VOLTAGE_LEVEL);
			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
	   		 1,
	   		 iVarSubID,
	   		 &iBufLen,
	   		 (void *)&pSigValue,
	   		 iTimeOut);
			if(iError == ERR_DXI_OK)
			{
			    gs_lVoltLevel = pSigValue->varValue.enumValue;
			}
			else
			{
			    gs_lVoltLevel = 0;
			}
			if(s_lVoltLevel != gs_lVoltLevel)
			{
			    TRACE_WEB_USER_NOT_CYCLE("Voltage level changed\n");
			    s_lVoltLevel = gs_lVoltLevel;
			    if(Mutex_Lock(g_hMutexSetDataFile, MAX_TIME_WAITING) == ERR_MUTEX_OK)
			    {
				gs_iWebSetMask = 1;
				Mutex_Unlock(g_hMutexSetDataFile);
			    }
			    else
			    {
				TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexSetDataFile in [gs_iWebSetMask] of Web_MakeDataFile\n");
				AppLogOut("Get lock", APP_LOG_WARNING, "Can't get the lock of g_hMutexSetDataFile in Web_MakeDataFile about [gs_iWebSetMask]!\n");
			    }
			}*/

			if(s_uiRectifierNum != gs_uiRectifierNumber)
			{
			    //Sleep(2000);
			    TRACE_WEB_USER_NOT_CYCLE("Rectifiers number changed[%d]\n", gs_uiRectifierNumber);
			    //s_uiRectifierNum = gs_uiRectifierNumber;
			    Web_GetRectSignalPointer(RECT_GROUP_SEQ);
			    //Web_MakeInventoryDataFile();
			    Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);
			    /*if(Mutex_Lock(g_hMutexSetDataFile, MAX_TIME_WAITING) == ERR_MUTEX_OK)
			    {*/
				//gs_uiRectifierNumber = Web_GetModuleNumber(ID_RECTIFIER_GROUP, ID_MODULE_NUM);
				//changed by Frank Wu,1/1/32,20140715, for add single converter and single solar settings pages
				//Web_GetSettingSignalPointer();
				if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
				{
					bIsGetSettingSignalPointerFail = TRUE;
				}
				s_uiRectifierNum = gs_uiRectifierNumber;

				//Web_MakeRectSetDataFile();
				//s_uiRectifierNum = gs_uiRectifierNumber;
				//Mutex_Unlock(g_hMutexSetDataFile);
			    /*}
			    else
			    {
				TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexSetDataFile in [gs_uiRectifierNumber] of Web_MakeDataFile\n");
				AppLogOut("Get lock", APP_LOG_WARNING, "Can't get the lock of g_hMutexSetDataFile in Web_MakeDataFile about [gs_uiRectifierNumber]!\n");
			    }*/
			    //Web_MakeLoadDataFile();
			    //Web_MakeAlarmDataFile();
			}
			if(s_uiRectifierS1Num != gs_uiRectifierS1Number)
			{
			    RunThread_Heartbeat(RunThread_GetId(NULL));
			    hCmdThread = RunThread_Create("Auto Config",
				(RUN_THREAD_START_PROC)DIX_AutoConfigMain_ForSlavePosition_ForWeb,
				(void*)NULL,
				(DWORD*)NULL,
				0);
			    TRACE_WEB_USER_NOT_CYCLE("Rectifiers S1 number changed[%d]\n", gs_uiRectifierS1Number);
				//changed by Frank Wu,1/1/33,20140715, for add single converter and single solar settings pages 
				//Web_GetSettingSignalPointer();
				if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
				{
					bIsGetSettingSignalPointerFail = TRUE;
				}
				s_uiRectifierS1Num = gs_uiRectifierS1Number;

			    Web_GetRectSignalPointer(RECTS1_GROUP_SEQ);
			    //Web_MakeInventoryDataFile();
			    Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);
			    //Web_MakeLoadDataFile();
			    //Web_MakeAlarmDataFile();
			}
			if(s_uiRectifierS2Num != gs_uiRectifierS2Number)
			{
			    RunThread_Heartbeat(RunThread_GetId(NULL));
			    hCmdThread = RunThread_Create("Auto Config",
				(RUN_THREAD_START_PROC)DIX_AutoConfigMain_ForSlavePosition_ForWeb,
				(void*)NULL,
				(DWORD*)NULL,
				0);
			    TRACE_WEB_USER_NOT_CYCLE("Rectifiers S2 number changed[%d]\n", gs_uiRectifierS2Number);
				//changed by Frank Wu,1/1/34,20140715, for add single converter and single solar settings pages 
				//Web_GetSettingSignalPointer();
				if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
				{
					bIsGetSettingSignalPointerFail = TRUE;
				}
				s_uiRectifierS2Num = gs_uiRectifierS2Number;

			    Web_GetRectSignalPointer(RECTS2_GROUP_SEQ);
			    //Web_MakeInventoryDataFile();
			    Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);
			    //Web_MakeLoadDataFile();
			    //Web_MakeAlarmDataFile();
			}
			if(s_uiRectifierS3Num != gs_uiRectifierS3Number)
			{
			    RunThread_Heartbeat(RunThread_GetId(NULL));
			    hCmdThread = RunThread_Create("Auto Config",
				(RUN_THREAD_START_PROC)DIX_AutoConfigMain_ForSlavePosition_ForWeb,
				(void*)NULL,
				(DWORD*)NULL,
				0);
			    TRACE_WEB_USER_NOT_CYCLE("Rectifiers S3 number changed[%d]\n", gs_uiRectifierS3Number);
				//changed by Frank Wu,1/1/35,20140715, for add single converter and single solar settings pages 
				//Web_GetSettingSignalPointer();
				if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
				{
					bIsGetSettingSignalPointerFail = TRUE;
				}
				s_uiRectifierS3Num = gs_uiRectifierS3Number;

			    Web_GetRectSignalPointer(RECTS3_GROUP_SEQ);
			    //Web_MakeInventoryDataFile();
			    Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);
			    //Web_MakeLoadDataFile();
			    //Web_MakeAlarmDataFile();
			}
			if(s_uiConverterNum != gs_uiConverterNumber)
			{
			    //Sleep(2000);
			    TRACE_WEB_USER_NOT_CYCLE("Converter number changed[%d]\n", gs_uiConverterNumber);
				//changed by Frank Wu,1/1/36,20140715, for add single converter and single solar settings pages 
				//Web_GetSettingSignalPointer();
				if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
				{
					bIsGetSettingSignalPointerFail = TRUE;
				}
				s_uiConverterNum = gs_uiConverterNumber;

				Web_GetRectSignalPointer(CONVERTER_GROUP_SEQ);
			    //Web_MakeInventoryDataFile();
			    Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);
			    //Web_MakeLoadDataFile();
			    //Web_MakeAlarmDataFile();
			}
			if(s_uiSolarNum != gs_uiSolarNumber)
			{
			    TRACE_WEB_USER_NOT_CYCLE("Solar number changed[%d]\n", gs_uiSolarNumber);
				//changed by Frank Wu,1/1/37,20140715, for add single converter and single solar settings pages 
				//Web_GetSettingSignalPointer();
				if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
				{
					bIsGetSettingSignalPointerFail = TRUE;
				}
				s_uiSolarNum = gs_uiSolarNumber;

			    Web_GetRectSignalPointer(SOLAR_GROUP_SEQ);
			    Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);
			    //Web_MakeLoadDataFile();
			    //Web_MakeInventoryDataFile();
			    //Web_MakeAlarmDataFile();
			}
	/*if(Mutex_Lock(g_hMutexSetDataFile, MAX_TIME_WAITING) == ERR_MUTEX_OK)
	{*/
		    if(s_uiCfgQtyOfLVD != gs_uiRealLVDNum)
		    {
				TRACE_WEB_USER_NOT_CYCLE("LVD number changed\n");
				//changed by Frank Wu,1/1/38,20140715, for add single converter and single solar settings pages 
				if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
				{
					bIsGetSettingSignalPointerFail = TRUE;
				}
				s_uiCfgQtyOfLVD = gs_uiRealLVDNum;

			//Web_GetLVDEquipNum();
			//Web_GetSettingSignalPointer();
			//Web_MakeSettingDataFile();
		    }
		    if(s_uiCfgSMDUBattFuseNum != gs_uiRealSMDUBattFuseNum)
		    {
				TRACE_WEB_USER_NOT_CYCLE("SMDU Batt fuse number changed\n");
				s_uiCfgSMDUBattFuseNum = gs_uiRealSMDUBattFuseNum;
				Web_GetSingleOtherSignalPointer(SMDU_BATT_FUSE_SEQ);
		    }
		    if(s_uiCfgLVD3Num != gs_uiRealLVD3UnitNum)
		    {
				TRACE_WEB_USER_NOT_CYCLE("LVD3 number changed\n");
				s_uiCfgLVD3Num = gs_uiRealLVD3UnitNum;
				Web_GetSingleOtherSignalPointer(LVD_SEQ2);
		    }
	    //Mutex_Unlock(g_hMutexSetDataFile);
	/*}
	else
	{
	    TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexSetDataFile in [gs_uiRealLVDNum] of Web_MakeDataFile\n");
	    AppLogOut("Get lock", APP_LOG_WARNING, "Can't get the lock of g_hMutexSetDataFile in Web_MakeDataFile about [gs_uiRealLVDNum]!\n");
	}*/
		if((s_uiCfgQtyOfComBAT != gs_uiRealComBattNum) ||
		    (s_uiCfgQtyOfEIBBAT != gs_uiRealEIBBattNum) ||
		    (s_uiCfgQtyOfSmduBAT != gs_uiRealSMDUBattNum) ||
		    (s_uiCfgQtyOfSmdueBAT != gs_uiRealSMDUEBattNum) ||
		    (s_uiCfgQtyOfSMBAT != gs_uiRealSMBattNum) ||
		    (s_uiCfgQtyOfLargeDUBAT != gs_uiRealLargeDUBattNum) ||
		    (s_uiCfgQtyOfSMBRC != gs_uiRealSMBRCBattNum) ||
		    (s_uiCfgQtyOfSoNick != gs_uiRealSoNickBattNum))
			{
			    iBatteryCount = 1;
			    TRACE_WEB_USER_NOT_CYCLE("Battery number changed\n");
				//changed by Frank Wu,1/1/39,20140715, for add single converter and single solar settings pages 
				//Web_GetSettingSignalPointer();
				if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
				{
					bIsGetSettingSignalPointerFail = TRUE;
				}
				s_uiCfgQtyOfComBAT = gs_uiRealComBattNum;
				s_uiCfgQtyOfEIBBAT = gs_uiRealEIBBattNum;
				s_uiCfgQtyOfSmduBAT = gs_uiRealSMDUBattNum;
				s_uiCfgQtyOfSmdueBAT = gs_uiRealSMDUEBattNum;
				s_uiCfgQtyOfSMBAT = gs_uiRealSMBattNum;
				s_uiCfgQtyOfLargeDUBAT = gs_uiRealLargeDUBattNum;
				s_uiCfgQtyOfSMBRC = gs_uiRealSMBRCBattNum;
				s_uiCfgQtyOfSoNick = gs_uiRealSoNickBattNum;

				Web_GetBattSignalPointer();
			}
	/*if((s_uiCfgQtyOfSMDU != gs_uiRealSMDUNum) || (s_uiCfgQtyOfSMDUP != gs_uiRealSMDUPNum) || (s_uiCfgQtyOfSMDUH != gs_uiRealSMDUHNum))
	{
	    TRACE_WEB_USER_NOT_CYCLE("DC number changed\n");
	    s_uiCfgQtyOfSMDU = gs_uiRealSMDUNum;
	    s_uiCfgQtyOfSMDUP = gs_uiRealSMDUPNum;
	    s_uiCfgQtyOfSMDUH = gs_uiRealSMDUHNum;
	    Web_GetDCSignalPointer();
	}*/
		if(s_uiCfgQtyOfSMDU != gs_uiRealSMDUNum)
		{
		    Web_GetSingleOtherSignalPointer(SMDU_SEQ);
		    s_uiCfgQtyOfSMDU = gs_uiRealSMDUNum;
		}
		if(s_uiCfgQtyOfSMDUP != gs_uiRealSMDUPNum)
		{
		    Web_GetSingleOtherSignalPointer(SMDUP_SEQ);
		    s_uiCfgQtyOfSMDUP = gs_uiRealSMDUPNum;
		}
		if(s_uiCfgQtyOfSMDUE != gs_uiRealSMDUENum)
		{
		    Web_GetSingleOtherSignalPointer(SMDUE_SEQ);
		    s_uiCfgQtyOfSMDUE = gs_uiRealSMDUENum;
		}
		if(s_uiCfgQtyOfSMDUH != gs_uiRealSMDUHNum)
		{
		    Web_GetSingleOtherSignalPointer(SMDUH_SEQ);
		    s_uiCfgQtyOfSMDUH = gs_uiRealSMDUHNum;
		}
	//changed by Frank Wu,3/1/4,20140723, for scanning FuelUnitEquipment 
	//changed by Frank Wu,N/38/38,20140730, for adding new web pages to the tab of "power system"
		if((s_uiCfgQtyOfSetSMLVDUnitNum != gs_uiRealSMLVDUnitNum)
			|| (s_uiCfgQtyOfSetFuelUnitNum != gs_uiRealFuelUnitNum)
			|| (s_uiCfgQtyOfSetOBFuelUnitNum != gs_uiRealOBFuelUnitNum)
			|| (s_uiCfgQtyOfSetOBBattFuseUnitNum != gs_uiRealOBBattFuseUnitNum)
			//changed by Frank Wu,N/N/N,20150429, for scanning LVD3
			|| (s_uiCfgQtyOfSetLVD3UnitNum != gs_uiRealLVD3UnitNum)
			|| (s_uiCfgQtyOfSetNaradaBMSGroupNum != gs_uiRealNaradaBMSGroupNum)
			)
		{
			if(Web_GetSettingSignalPointer() != ERR_DXI_OK)
			{
				bIsGetSettingSignalPointerFail = TRUE;
			}
			s_uiCfgQtyOfSetSMLVDUnitNum = gs_uiRealSMLVDUnitNum;
			s_uiCfgQtyOfSetFuelUnitNum = gs_uiRealFuelUnitNum;
			s_uiCfgQtyOfSetOBFuelUnitNum = gs_uiRealOBFuelUnitNum;
			s_uiCfgQtyOfSetOBBattFuseUnitNum = gs_uiRealOBBattFuseUnitNum;
			//changed by Frank Wu,N/N/N,20150429, for scanning LVD3
			s_uiCfgQtyOfSetLVD3UnitNum = gs_uiRealLVD3UnitNum;
			s_uiCfgQtyOfSetNaradaBMSGroupNum = gs_uiRealNaradaBMSGroupNum;
		}


		if(bIsGetSettingSignalPointerFail)
		{
			iGetSettingPointerTryCount = MAX_GET_SETTING_POINTER_TRY_COUNT;
			bIsGetSettingSignalPointerFail = FALSE;
		}

		if(iGetSettingPointerTryCount > 0)
		{
			iGetSettingPointerTryCount--;
			Web_GetSettingSignalPointer();
		}

		Web_MakeSourceDetailDataFile();

		Web_MakeLoginDataFile();//Fix the issue that date can not update on the page of screen saver after login

		if((iBatteryCount == 0) || (iBatteryCount > 3))
		{
		    Web_MakeHomeDataFile();//debug
		}
		Web_MakeAlarmDataFile();
		Sleep(500);//yield
		Web_MakeInventoryDataFile();
#ifdef		WEB_USER_COST
	gettimeofday(&t_start1, NULL); 
	start1 = ((long)t_start1.tv_sec)*1000+(long)t_start1.tv_usec/1000;
#endif
	Web_MakeSettingDataFile();
#ifdef		WEB_USER_COST
	gettimeofday(&t_end1, NULL);
	end1 = ((long)t_end1.tv_sec)*1000+(long)t_end1.tv_usec/1000;
	cost_time1 = end1 - start1;
	TRACE_WEB_USER_NOT_CYCLE("Cost time of Web_MakeSettingDataFile: %ld ms\n", cost_time1);
#endif
	//Web_MakeWizardDataFile();
#ifdef		WEB_USER_COST
	gettimeofday(&t_start2, NULL); 
	start2 = ((long)t_start2.tv_sec)*1000+(long)t_start2.tv_usec/1000;
#endif
		Web_MakeRectSetDataFile();
		//changed by Frank Wu,23/N/30,20140527, for add single converter and single solar settings pages
		Sleep(500);//yield
		Web_MakeConverterSetDataFile();
		//changed by Frank Wu,22/N/35,20140527, for adding the the web setting tab page 'DI'
		Web_MakeDIDataFile();
		Web_MakeDODataFile();
		Web_MakeShuntDataFile();
		Web_MakeShuntDataFile_Sec();

		Web_MakeCustom_InputsDataFile();

		Web_MakeAnalogDataFile();

//changed by Stone Song,20160525, for adding the the web setting tab page 'FUSE'
		Web_MakeFuseDataFile();
#ifdef		WEB_USER_COST
	gettimeofday(&t_end2, NULL);
	end2 = ((long)t_end2.tv_sec)*1000+(long)t_end2.tv_usec/1000;
	cost_time2 = end2 - start2;
	TRACE_WEB_USER_NOT_CYCLE("Cost time of Web_MakeRectSetDataFile: %ld ms\n", cost_time2);
#endif
		Web_MakeSysSetDataFile();


		Web_MakeRectDataFile(s_uiRectifierNum, RECT_GROUP_SEQ);//for debug

		Sleep(500);//yield
		Web_MakeRectDataFile(s_uiRectifierS1Num, RECTS1_GROUP_SEQ);
		Web_MakeRectDataFile(s_uiRectifierS2Num, RECTS2_GROUP_SEQ);
		Web_MakeRectDataFile(s_uiRectifierS3Num, RECTS3_GROUP_SEQ);
		Sleep(500);//yield
		Web_MakeRectDataFile(s_uiConverterNum, CONVERTER_GROUP_SEQ);
		Web_MakeRectDataFile(s_uiSolarNum, SOLAR_GROUP_SEQ);

		if((iBatteryCount == 0) || (iBatteryCount > 3))
		{
		    Web_MakeBattDataFile();//for debug
		}
		if(iBatteryCount != 0)
		{
		    iBatteryCount++;
		    if(iBatteryCount > 3)
		    {
			iBatteryCount = 0;
		    }
		}
		Web_MakeBattSetDataFile();

		Sleep(500);//yield
		Web_MakeDCDataFile();
		Web_MakeACDataFile();
		Sleep(500);//yield
		Web_MakeDGDataFile();
		Web_MakeSMIODataFile();
		Sleep(500);//yield
		Web_MakeLVD_FUSEDataFile();
		Web_MakeConfigDataFile();
		Sleep(500);//yield
		Web_MakeSettingConfigDataFile();
		Web_CalCabinetData();
		Sleep(500);//yield
		Web_MakeCapMapDataFile();
		Web_MakeCabinetDataFile();
		RunThread_Heartbeat(RunThread_GetId(NULL));

#ifdef		WEB_USER_COST
	gettimeofday(&t_end, NULL);
	end = ((long)t_end.tv_sec)*1000+(long)t_end.tv_usec/1000;
	cost_time = end - start;
	TRACE_WEB_USER_NOT_CYCLE("Cost time of Web_MakeDataFile: %ld ms\n", cost_time);
#endif
		if(!bContinueRunning)
		{
			break;
		}
		Sleep(500);//yield
	}
	
	clean_session_file();

	dataRefreshFlag = find_session_file();
		
	toTime = time((time_t *)0);
	toTimeInterval = toTime - toTimeLast;
	toTimeInterval1 = toTime - toTimeLast1;
	if((toTimeInterval < 0) || (toTimeInterval > TWO_HOUR_SECOND))// modify the time
	{
	    memset(szTime, 0, sizeof(szTime));
	    TimeToString(time(NULL), TIME_CHN_FMT, szTime, sizeof(szTime));
	    TRACE_WEB_USER_NOT_CYCLE("%s\n", szTime);

	    TRACE_WEB_USER_NOT_CYCLE("Need to init load or hybrid data\n");
	    Lock_g_hMutexLoadCurrent(INIT_LOAD_DATA);
	    /*if(Mutex_Lock(g_hMutexLoadCurrent, MAX_TIME_WAITING) == ERR_MUTEX_OK)
	    {
		Web_MakeHisDataInit();
		Mutex_Unlock(g_hMutexLoadCurrent);
	    }
	    else
	    {
		TRACE_WEB_USER_NOT_CYCLE("Can't get the lock of g_hMutexLoadCurrent in Web_MakeDataFile\n");
		AppLogOut("Web_MakeDataFile", APP_LOG_WARNING, "Can't get the lock of g_hMutexLoadCurrent!\n");
	    }*/
	    //Web_MakeLoadDataFile();
	    Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);
	    Web_MakeCurveDataInit(EQUIP_AC_GROUP, SIGNAL_AC_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_DG_GROUP, SIGNAL_DG_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_SOLAR_GROUP, SIGNAL_SOLAR_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_SYSTEM, SIGNAL_AMBIENT_TEMP);
	    Web_MakeCurveDataInit(EQUIP_BATT_GROUP, SIGNAL_COMP_TEMP);
	    Web_MakeCurveDataInit(EQUIP_BATTERY1, SIGNAL_CAP_PERCENTAGE);
	    Web_MakeCurveDataInit(EQUIP_BATTERY2, SIGNAL_CAP_PERCENTAGE);
	    //Web_MakeCurveDataInit(EQUIP_WIND_GROUP, SIGNAL_WIND_CURRENT);	
	    Web_MakeCurveDataFile();
	    toTimeLast = toTime;
		// changed by Frank Wu, 20131113, 6/9,for only displaying one data in each interval of 2 hours
		g_SiteInfo.tmLatestUpdate = toTimeLast;
	}
	else if(toTimeInterval > ONE_HOUR_SECOND)	// one hour has been passed
	{
	    memset(szTime, 0, sizeof(szTime));
	    TimeToString(time(NULL), TIME_CHN_FMT, szTime, sizeof(szTime));
	    TRACE_WEB_USER_NOT_CYCLE("%s\n", szTime);

	    TRACE_WEB_USER_NOT_CYCLE("Need to update load current!\n");
	    Lock_g_hMutexLoadCurrent(MAKE_LOAD_DATA_FILE);

	    Web_MakeCurveDataInit(EQUIP_AC_GROUP, SIGNAL_AC_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_DG_GROUP, SIGNAL_DG_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_SOLAR_GROUP, SIGNAL_SOLAR_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_SYSTEM, SIGNAL_AMBIENT_TEMP);
	    Web_MakeCurveDataInit(EQUIP_BATT_GROUP, SIGNAL_COMP_TEMP);
	    Web_MakeCurveDataInit(EQUIP_BATTERY1, SIGNAL_CAP_PERCENTAGE);
	    Web_MakeCurveDataInit(EQUIP_BATTERY2, SIGNAL_CAP_PERCENTAGE);

	    Web_MakeCurveDataFile();
	    calPeakpower();
	    toTimeLast = toTime;
	}
	else
	{}

	if(toTimeInterval1 > ONE_DAY_SECOND)
	{
	    memset(szTime, 0, sizeof(szTime));
	    TimeToString(time(NULL), TIME_CHN_FMT, szTime, sizeof(szTime));
	    TRACE_WEB_USER_NOT_CYCLE("%s\n", szTime);

	    TRACE_WEB_USER_NOT_CYCLE("Need to init hybrid current\n");
	    Web_MakeCurveDataInit(EQUIP_AC_GROUP, SIGNAL_AC_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_DG_GROUP, SIGNAL_DG_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_SOLAR_GROUP, SIGNAL_SOLAR_CURRENT);	
	    Web_MakeCurveDataInit(EQUIP_SYSTEM, SIGNAL_AMBIENT_TEMP);
	    Web_MakeCurveDataInit(EQUIP_BATT_GROUP, SIGNAL_COMP_TEMP);
	    Web_MakeCurveDataInit(EQUIP_BATTERY1, SIGNAL_CAP_PERCENTAGE);
	    Web_MakeCurveDataInit(EQUIP_BATTERY2, SIGNAL_CAP_PERCENTAGE);
	    //Web_MakeCurveDataInit(EQUIP_WIND_GROUP, SIGNAL_WIND_CURRENT);	
	    Web_MakeCurveDataFile();
	    toTimeLast1 = toTime;

		// changed by Frank Wu, 20131113, 7/9,for only displaying one data in each interval of 2 hours
		g_SiteInfo.tmLatestUpdate = toTimeLast1;
	}
	else
	{}

	if (*iQuitCommand == SERVICE_STOP_RUNNING)
	{
	    bContinueRunning = FALSE;
	}

    }


    DxiRegisterNotification("Web User", 
	ServiceNotification,
	NULL, // it shall be changed to the actual arg
	(/*_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK |*/ _WEB_SET_MASK | _CONFIG_MASK | _LOAD_CURRENT_MASK | _CLEAR_HISTORY_MASK),
	RunThread_GetId(NULL),
	_CANCEL_FLAG);

    FreeMemory();

    TRACE_WEB_USER_NOT_CYCLE("***********************************end to make file data!*******************************************\n");
}


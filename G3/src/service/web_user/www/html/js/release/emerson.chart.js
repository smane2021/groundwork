/*!
 * Author: Jerman.Xie | xieguishi@126.com
 * Date: 2013.09
 *
 * 绘制相关图表的方法汇集 
 */
var Chart = {};
//兼容IE对canvas的处理
Chart.canvasForIE = function (ele) {
    if (Configs.Browser == "IE6" || Configs.Browser == "IE7" || Configs.Browser == "IE8"){
        ele = window.G_vmlCanvasManager.initElement(ele);
    }
    return ele;
};
//获取数据中的最大值
Chart.getArrayMax = function (arr) {
    var Max = arr[0];
    for (var i = 0, ilen = arr.length; i < ilen; i++) {
        Max = Math.max(Max, arr[i]);
    };
    return Max;
};
//转换角度为PI值
Chart.transformAngle = function (angle) {
    return (angle / 360) * 2 * Math.PI;
};
//不足10,在其前面补0
Chart.makeupZero = function (num) {
    if (num < 10) {
        return "0" + num;
    } else {
        return num;
    }
};
Chart.ArraySameCount = [];
Chart.AarryDeleteSame = function (arr) {
    for (var i = 0; i < arr.length; i++)
        for (var j = i + 1; j < arr.length; j++)
            if (arr[i] === arr[j]) { this.ArraySameCount.push(i); arr.splice(j, 1); j--; } else {
            }
    return arr.sort(function (a, b) { return a - b });
};

/*
* 画趋势图
*/
Chart.drawCurve = function (target, options, data_y) {
    var setting = {
        canvas: {
            padding: [5, 5, 5, 5],     //画线时距离画布上右下左的距离 
            width: 600, 		//画布的宽度
            height: 110			//画布的高度
        },
        lines: {
            color: ["#858585", "#6CAFD9"],    //[竖线的颜色,曲线的颜色]
            width: 1             //线条的大小
        },
        X: {
            value: []
        },
        Y: {
            unit: "%",
            value: [0, 25, 50, 75, 100],	 	//Y坐标数值,如果在前端设置值,则默认平分画布
            follow: [false,0,0]     //Y轴数值是否跟随数据，[false为否，区间限制最小值，区间限制最大值]
        },
        alarm: {
            value: [],    //告警的值
            color: ["#a2ee00", "#ffae00", "#e35500", "#ff0000"],  //告警线条的颜色
            width: 1,                  //告警线条的宽度
            midline: ["#858585", 2]            //告警线条中间线的颜色及宽度
        },
        value: [], 			//曲线值
        unit: "A",          //value的单位,默认为电流单位
        hasMax: true,           //是否显示曲线最大值
        hasMouse: true,          //鼠标经过是否显示当前点的值
        hasKey: true,             //是否有键盘事件,左右箭头键
        dateFormat: "MM-DD"      //X坐日期格式，目前只有这一种格式

    };
    var that = this;
    //判断目标div是否存在
    var container = document.getElementById(target);
    $(container).html("");
    if ($.type(options) !== "object") {
        alert(Language.Chart["001"]);
        return;
    };
    if (!container) { return; }
    //没有数据时,设置一个空数据，并把max,point,date,mouse_canvas隐藏，并创建一个nodata div显示出来
    //同时应该有回调函数，用来处理AverageCurrent
    var $container = $(container);
    var nodata = false;
    function doNodata(value) {
        nodata = true;
        //取消绑定
        var cvs2 = $(container).find("canvas.curve-mouse")[0];
        if (cvs2) {
            $(cvs2).off();
        }
        if (value == true) {
            //设置空数据
            var Nodatas = {};
            options = $.extend(true, {}, Nodatas, options);
            options.value = [["1970/00/00 00:00:00", "nodata"]];
        }
        //处理各图层显示或隐藏
        $container.find("div.curve-date").hide();
        $container.find("canvas.curve-mouse").hide();
        $container.find("div.curve-max").hide();
        $container.find("div.curve-point").hide();
        $container.find("div.curve-x").hide();

        //创建一个no data层
        var NodataEle = document.createElement("div");
        NodataEle.className = "curve-nodata";
        NodataEle.innerHTML = Language.Html["028"];
        $(container).append(NodataEle);
        if ($.type(options['fn_nodata']) == "function") {
            options['fn_nodata']();
        }
    };
    if (!options["value"]) {
        return false;
    }
    if (options["value"].length == 0) {
        doNodata(true);
    } else {
        $container.find("div.curve-date").hide();
        $container.find("div.curve-nodata").remove();
        $container.find("canvas.curve-mouse").show();
        $container.find("div.curve-date").show();
        $container.find("div.curve-max").hide();
        $container.find("div.curve-x").show();
        if ($.type(options['fn']) == "function") {
            options['fn']();
        }
    }
    //清除提示
    Control.PromptEvent.TipRemove({ remove: "Trend" });
    //清除loading
    Control.PromptEvent.LoadingRemove({ remove: target });
    setting = $.extend(true, {}, setting, options);
    //简化调用
    var canvas = setting.canvas;
    var scale = setting.scale;
    var alarm = setting.alarm;
    var V = setting.value;
    var X = setting.X; //X轴,根据最后的数据来显示，显示31天的数据
    var Y = setting.Y;
    var unit = setting.unit;
    //data_y源数据
    var V2 = data_y ? data_y.value : V;

    //创建画布
    var cvs = document.createElement("canvas");
    cvs.id = target + "_canvas";
    cvs.className = "curve-canvas";
    cvs.width = canvas.width;
    cvs.height = canvas.height;
    $(container).append(cvs);
    this.canvasForIE(cvs);
    var cxt = cvs.getContext("2d");
    cxt.canvas = null;
    //初始画布
    cxt.clearRect(0, 0, canvas.width, canvas.height);
    cxt.lineCap = "round";
    cxt.save();

    //处理Y轴数据
    if (Y.follow && Y.follow[0] === true) {
        //赋值Ymax为负穷大，YMin为正无穷大,以便数据第一个数比YMax大，比YMin小，从而获取数据中的最大及最小值
        var YMax = -Infinity, YMin = Infinity;
        //获取最大值，最小值
        for (var m = 0, mlen = V.length; m < mlen; m++) {
            if (V[m][1] == -273) { continue; }
            if (m == 0) { YMax = V[m][1]; YMin = V[m][1]; }
            YMax = Math.max(YMax, V[m][1]);
            YMin = Math.min(YMin, V[m][1]);
        }
		//所有Value值都是-273时，Y轴显示默认区间-50-75
		if(Infinity==YMin){ YMin= -45; }
		if(-Infinity==YMax){ YMax=70; }
        YMin -= 5;
        YMin = (YMin <= Y.follow[1]) ? Y.follow[1] : YMin;
        YMax += 5;
        YMax = (YMax >= Y.follow[2]) ? Y.follow[2] : YMax;
        var cz = Math.ceil((YMax - YMin) / 5);
        Y.value = [];
        for (var i = 0; i < 6; i++) {
            Y.value.push(parseInt(YMin + cz * i));
        }
        Y.value.sort(function(c,d){ return c-d });
    }
    //处理出X轴数据，确定是哪个月
    if (V.length == 0) { return; }
    if (Configs.SystemTime != "") {
        var currentDateString = Configs.SystemTime.split(" ")[0];
    } else {
        var currentDateString = V[V.length - 1][0].split(" ")[0];
    }

    for (var j = 0; j < 4; j++) {
        var tempDate = new Date(currentDateString);
        X.value[j] = new Date(tempDate.setDate(tempDate.getDate() - j * 10));
    }
    //X轴倒序
    X.value.reverse();
    //画grid
    var canvas_width = canvas.width - (canvas.padding[1] + canvas.padding[3]);
    var canvas_height = canvas.height - (canvas.padding[0] + canvas.padding[2]);
    cxt.save();
    var xdiv = null;

    xdiv = document.createElement("div");
    xdiv.className = "curve-x";
    $(container).append(xdiv);

    $(xdiv)[0].innerHTML = '';
    cxt.strokeStyle = setting.lines.color[0];
    cxt.lineWidth = 1;
    var span = null;
    var spanHTML = "";
    for (var i = 0, z = 0, ilen = (X.value.length - 1) * 10 + 2; i <= ilen; i++) {
        var xItem = (canvas_width - (canvas_width / ilen)) / ilen;
        var x1 = Math.floor(xItem * i) + 0.5;
        if (i == 0) { continue; };
        if (i % 10 == 1) {//实际应该是31天,所以这里余数应该为1
            cxt.lineWidth = 2;
            spanHTML = this.makeupZero(X.value[z].getMonth() + 1) + "-" + this.makeupZero(X.value[z].getDate());
            if (!nodata) {
                span = document.createElement("span");
                span.innerHTML = spanHTML;
                $(xdiv).append(span);
                $(span).css({ "left": x1 - $(span).width() / 2 });
            }
            z++;
        } else {
            cxt.lineWidth = 1;
        }
        cxt.beginPath();
        cxt.moveTo(x1, 0);
        cxt.lineTo(x1, canvas.height);
        cxt.stroke();
    }
    span = null; spanHTML = null;
    cxt.save();
    var Ylast = Y.value[Y.value.length - 1] - Y.value[0];
    var YZeroLine = (canvas_height * (0 - Y.value[0]) / Ylast);
    var Yzero = Y.value[0];
    //输出Y轴
    if (!nodata) {
        var ydiv = null;
        ydiv = document.createElement("div");
        ydiv.className = "curve-y";
        $(ydiv).css({ "height": canvas.height });
        $(container).append(ydiv);
        var top = "", yint = "";
        for (var y = 0, ylen = Y.value.length; y < ylen; y++) {
            top = canvas.padding[0] + Math.floor(canvas_height - (Y.value[y] - Yzero) * canvas_height / Ylast) + 0.5;
            yint = y == 0 ? (Y.value[y] == 0 ? "" : Y.unit) : Y.unit;
            span = document.createElement("span");
            span.innerHTML = Y.value[y] + yint;
            $(ydiv).append(span);
            $(span).css({ "top": top - 8 });
        }
        cxt.save();
        top = null; yint = null;
    }
    if (nodata) { return; }
    //画alarm
    var ay = "";
    if (alarm.value[0] < Y.value[0]) {
        alarm.value[0] = Y.value[0];
    }
    if (alarm.value[alarm.value.length - 1] > Y.value[Y.value.length - 1]) {
        alarm.value[alarm.value.length - 1] = Y.value[Y.value.length - 1];
    }
    for (var a = 0, alen = alarm.value.length; a < alen; a++) {
        ay = canvas.padding[0] + Math.floor(canvas_height - (alarm.value[a] - Yzero) * canvas_height / Ylast) + 0.5;
        cxt.beginPath();
        cxt.lineWidth = alarm.width;
        cxt.strokeStyle = alarm.color[a];
        cxt.moveTo(alarm.width / 2+Configs.ChartdrawCurve_Y[1], ay);
        cxt.lineTo(canvas_width - alarm.width / 2-Configs.ChartdrawCurve_Y[1], ay);
        cxt.stroke();
        if (a==alen-1) {
            cxt.save();
            cxt.beginPath();
            cxt.strokeStyle = alarm.midline[0];
            cxt.lineWidth = alarm.midline[1];
            cxt.moveTo(Configs.ChartdrawCurve_Y[0], 1);
            cxt.lineTo(cvs.width-Configs.ChartdrawCurve_Y[3], 1);
            
            cxt.moveTo(Configs.ChartdrawCurve_Y[0], cvs.height/2);
            cxt.lineTo(cvs.width-Configs.ChartdrawCurve_Y[3], cvs.height/2);
            
            cxt.moveTo(Configs.ChartdrawCurve_Y[0], cvs.height-1);
            cxt.lineTo(cvs.width-Configs.ChartdrawCurve_Y[3], cvs.height-1);
            cxt.stroke();
        }
    }
    ay = null;

    //画曲线
    var allCount = 32 * 24 * 60 * 60 * 1000; //31天的时间毫秒数据(实际是31天)
    var currentDate = new Date(currentDateString);
    var firstCount = new Date(currentDate.setDate(currentDate.getDate() - 31));
    cxt.save();
    cxt.strokeStyle = setting.lines.color[1];
    cxt.fillStyle = setting.lines.color[1];
    cxt.lineWidth = setting.lines.width;
    //赋值maxPoint为负穷大，minPoint为正无穷大,以便数据第一个数比maxPoint大，比minPoint小，从而获取数据中的最大及最小值
    var maxPoint = -Infinity, maxIndex = Y.length - 1;
    var minPoint = Infinity, minIndex = 0;
    var startTime = X.value[0].getTime();
    var endTime = X.value[X.value.length - 1].getTime() + 24 * 60 * 60 * 1000; //后面加24个小时的时间，表示时间到最后一天的24点
    //绘图的上下基线，作这个限制是为了避免线条显示不全
    var curveDownLine = canvas.height - canvas.padding[2];
    var curveUpLine = canvas.padding[0];
    function getQx(ct) {
        return Math.floor(((ct - firstCount) / allCount) * (canvas_width - canvas_width / ilen)) + 0.5;
    }
    function getQy(vq) {
        return canvas.padding[0] + Math.floor(canvas_height - (vq - Yzero) * canvas_height / Ylast) + 0.5;
    }
    var qx = getQx(new Date(V[0][0]).getTime());
    var qy = getQy(V[0][1] ? V[0][1] : 0);
    var redraw = false;
    if (V.length > 1) {
        function DrawPoints(q) {
            cxt.beginPath();
            for (var qlen = V.length; q < qlen; q++) {
                qx = getQx(new Date(V[q][0]).getTime());
                qy = getQy(V[q][1]);
                //当前画线范围内找出最大点
                if (new Date(V[q][0]).getTime() >= startTime && new Date(V[q][0]).getTime() <= endTime) {
                    maxPoint = Math.max(maxPoint, V[q][1]);
                    minPoint = Math.min(minPoint, V[q][1]);
                }
                if (V[q + 1]) {
                    //q+1数据存在，判断时间间隔是否大于2小时
                    if ((new Date(V[q + 1][0]).getTime() - new Date(V[q][0]).getTime()) >= 2 * 60 * 60 * 1000) {
                        if (redraw) {
                            cxt.lineTo(qx, qy);
                            cxt.stroke();
                            cxt.arc(qx, qy, 1, 0, 2 * Math.PI, true);
                            //cxt.fill();
                            redraw = false;
                        } else {
                            cxt.arc(qx, qy, 1, 0, 2 * Math.PI, true);
                            cxt.fill();
                        }
                        q++;
                        break;
                    } else {
                        //下一个点时间间隔小于2小时
                        if (!redraw) {
                            cxt.moveTo(qx, qy);
                            cxt.arc(qx, qy, 1, 0, 2 * Math.PI, true);
                            cxt.fill();
                            redraw = true;
                        } else {
                            cxt.lineTo(qx, qy);
                        }
                    }
                } else {
                    if (V[q - 1]) {
                        //与前一点比较是否大于2小时,大于画点，小于画线
                        if ((new Date(V[q][0]).getTime() - new Date(V[q - 1][0]).getTime()) >= 2 * 60 * 60 * 1000) {
                            cxt.arc(qx, qy, 1, 0, 2 * Math.PI, true);
                            cxt.fill();
                        } else {
                            cxt.lineTo(qx, qy);
                        }
                    }
                }
            }
            cxt.stroke();
            cxt.save();
            if (q < qlen) {
                DrawPoints(q);
            }
        }
        DrawPoints(0);
    } else {
        cxt.beginPath();
        cxt.fillStyle = setting.lines.color[1];
        //当前画线范围内找出最大点
        if (new Date(V[0][0]).getTime() >= startTime && new Date(V[0][0]).getTime() <= endTime) {
            maxPoint = Math.max(maxPoint, V[0][1]);
            minPoint = Math.min(minPoint, V[0][1]);
        }
        cxt.arc(qx, qy, 2, 0, 2 * Math.PI);
        cxt.fill();
    }
    qx = null; qy = null;

    //检测最大数据是否溢出画布，如果溢出画布，则显示提示"部分数值超出范围"
    var Ymax = Y.value[Y.value.length - 1];
    var Ymin = Y.value[0];
    var ChartTip = $container.find("strong.curve-tip");
    if (Number(maxPoint) > Ymax || Number(minPoint) < Ymin) {
        if (ChartTip[0]) {
            ChartTip.show();
        } else {
            ChartTip = document.createElement("strong");
            ChartTip.className = "curve-tip";
            ChartTip.innerHTML = Language.Chart["002"];
            $(container).append(ChartTip);
        }
    } else {
        $(ChartTip).hide();
    }
    var hasMax = setting.hasMax;
    var hasMouse = setting.hasMouse;
    if (hasMouse) {
        if (!$(container).find("div.curve-point")[0]) {
            var PointElement = document.createElement("div");
            PointElement.style.display = "none";
            PointElement.className = "curve-point";
            $(cvs).before(PointElement);
        }
    };
    if (hasMax) {
        if (!$(container).find("div.curve-max")[0]) {
            var MaxElement = document.createElement("div");
            MaxElement.className = "curve-max";
            $(cvs).before(MaxElement);
        };
    };
    if (!$(container).find("div.curve-date")[0]) {
        var DateElement = document.createElement("div");
        DateElement.className = "curve-date";
        DateElement.innerHTML = Language.Chart["003"] + " " + setting.dateFormat;
        $container.append(DateElement)
    }
    var cvs2 = $(container).find("canvas.curve-mouse")[0];
    if (!cvs2) {
        var cvs2 = document.createElement("canvas");
        cvs2.className = "curve-mouse";
        $(cvs).before(cvs2);
    }
    cvs2.width = canvas.width;
    cvs2.height = canvas.height;
    this.canvasForIE(cvs2);
    var cxt2 = cvs2.getContext("2d");
    cxt2.canvas = null;
    var CurveMax = MaxElement ? $(MaxElement) : $container.find("div.curve-max");
    var CurvePoint = PointElement ? $(PointElement) : $container.find("div.curve-point");
    //画出最大值
    if (hasMax) {
        if (V[0][1] == "nodata") { return; }
        var X_StartTime = X.value[0].getTime();
        var X_EndTime = X.value[X.value.length - 1].getTime() + +60 * 60 * 1000 * 24;
        var V_StartTime = new Date(V[0][0]).getTime();
        var V_EndTime = new Date(V[V.length - 1][0]).getTime();
        //V的最小时间大于X的最大时间，表示当前时间段 No Data
        //V的最大时间小于X的最小时间，表示当前时间段 No Data
        if (V_StartTime > X_EndTime || V_EndTime < X_StartTime) {
            doNodata();
            $(cvs2).off();
            cxt2.clearRect(0, 0, canvas.width, canvas.height);
            return;
        };
        function drawMax() {
            cxt2.beginPath();
            var max_value = [];
            var currentTime = 0, qx = 0, qy = 0;
            for (var q = 0, qlen = V.length; q < qlen; q++) {
                currentTime = new Date(V[q][0]).getTime();
                if (currentTime >= startTime && currentTime <= endTime) {
                    if (V[q][1] == maxPoint) {
                        maxIndex = q;
                        qx = Math.floor(((currentTime - firstCount) / allCount) * (canvas_width - canvas.width / ilen)) + 0.5;
                        qy = canvas.padding[0] + Math.floor(canvas_height - (V[q][1] - Yzero) * canvas_height / Ylast) + 0.5;
                        max_value.push([qx, qy, V[q][0], V[q][1]]);
                    }
                }
            };
            if (max_value.length > 0) {
                cxt2.beginPath();
                cxt2.fillStyle = "#ff6600";
                cxt2.arc(max_value[max_value.length - 1][0], max_value[max_value.length - 1][1], 4, 0, 2 * Math.PI);
                if (CurveMax[0]) {
                    CurveMax.html("<p>" + Language.Html["026"] + ":<span id='PeakCurrent'>" + (V2.peakCurrent ? V2.peakCurrent[0] : V2[maxIndex][1]) + "</span>" + unit + "</p><p>" + Pages.footer(Pages.changetime(max_value[max_value.length - 1][2]),"page") + "</p>").css({ "left": qx + 50, "top": qy - 25 });
                };
                var left = qx + 55 + CurveMax.outerWidth(true);
                var trend_width = canvas.width;
                if (left > trend_width) {
                    CurveMax.css({ "left": qx - CurveMax.outerWidth(true) + 40 });
                }
                CurveMax.show();
            }
            cxt2.fill();
            cxt2.save();
            currentTime = null; qx = null; qy = null;
        };
        drawMax();
    }
    if (hasMouse) {
        cxt2.strokeStyle = "#E35500";
        cxt2.fillStyle = "#E35500";
        //绑定鼠标事件
        var timeout = null;
        var time = endTime - startTime; //时间区间
        this.MoveTimer = null;
        var that = this;
        var mouseX = 0;
        var doc = $("html");
        $(cvs2).off().on("mousemove", function (event) {
            if (mouseX == event.clientX) { return; }
            mouseX = event.clientX;
            CurveMax.hide();
            MovePoint(event, "keymove");
        }).on("mouseleave", function () {
            that.MoveTimer = setTimeout(function () {
                Configs.Trend.Enter = false;
                cxt2.clearRect(0, 0, canvas.width, canvas.height);
                CurvePoint.hide();
                if (hasMax) {
                    drawMax();
                }
                $(doc).off("keydown")
            }, 200);
        }).on("mouseenter", function (event) {
            clearTimeout(that.MoveTimer);
            Configs.Trend.Enter = true;
            CurveMax.hide();
            $(doc).off("keydown").on("keydown", function (event) {
                var key = event.keyCode;
                //使用鼠标控制趋势图点的选择
                if (!Configs.Trend.Enter) { return; }
                if (key == 37) {
                    Configs.Trend.Point -= 1;
                } else if (key == 39) {
                    Configs.Trend.Point += 1;
                }
                if (Configs.Trend.Point < 0) {
                    Configs.Trend.Point = 0;
                } else if (Configs.Trend.Point > V2.length - 1) {
                    Configs.Trend.Point = V2.length - 1;
                }
                MovePoint(event, V2[Configs.Trend.Point]);
            });
        });
        var MovePoint = function (event, NextPoint) {
            var X_StartTime = X.value[0].getTime();
            var X_EndTime = X.value[X.value.length - 1].getTime() + 60 * 60 * 1000 * 24;
            var Key_Point = new Date(NextPoint[0]).getTime();
            if (!NextPoint) { return; };
            if (Key_Point < X_StartTime) {
                Configs.Trend.Point++;
                return;
            } else if (Key_Point > X_EndTime) {
                Configs.Trend.Point--;
                return;
            };
            var V_StartTime = new Date(V[0][0]).getTime();
            var V_EndTime = new Date(V[V.length - 1][0]).getTime();
            //V的最小时间大于X的最大时间，表示当前时间段 No Data
            //V的最大时间小于X的最小时间，表示当前时间段 No Data
            if (V_StartTime > X_EndTime || V_EndTime < X_StartTime) {
                return;
            }
            var this_position = $(cvs2).offset();
            var m_left = event.clientX - this_position.left+17;
            var m_top = event.clientY - this_position.top;
            var currentSeconeds = startTime + parseInt(time * (m_left / cvs.width)); //所指时间
            var current = new Date(currentSeconeds);
            var Month = current.getMonth() + 1;
            var day = current.getDate();
            var prev = 0;
            var next = 0;
            var np = 0;
            var value = "";
            var value_time = "";
            if (NextPoint == "keymove") {
                //循环找出当前点值
                for (var i = 0, olen = V.length; i < olen; i++) {
                    var t = new Date(V[i][0]).getTime();
                    if (t == currentSeconeds) {
                        np = i;
                        break;
                    }
                    if (currentSeconeds - t >= 0) {
                        prev = i;
                        np = prev;
                        value = V2[np][1];
                        value_time = V[np][0];
                    } else {
                        next = i;
                        var prevDay = new Date(V[prev][0]),
							 prevDay_month = prevDay.getMonth() + 1,
						     prevDay_day = prevDay.getDay,
						     nextDay = new Date(V[next][0]),
							 nextDay_month = nextDay.getMonth() + 1,
							 nextDay_day = nextDay.getDay;
                        if (prevDay_month == nextDay_month) {
                            if (prevDay_day == day) {
                                np = prev;
                            } else if (nextDay_day == day) {
                                np = next;
                            } else {
                                if (currentSeconeds - prevDay.getTime() > nextDay.getTime() - currentSeconeds) {
                                    np = next;
                                } else {
                                    np = prev;
                                }
                            }
                        } else {
                            if (currentSeconeds - prevDay.getTime() >= nextDay.getTime() - currentSeconeds) {
                                np = next;
                            } else {
                                np = prev;
                            }
                        }
                        value = V2[np][1];
                        value_time = V[np][0];
                        break;
                    }
                };
                Configs.Trend.Point = np;
            };
            if (NextPoint != "keymove" && NextPoint) {
                CurveMax.hide();
                value = NextPoint[1];
                value_time = NextPoint[0];
            };

            if ($.trim(value) == "" || $.trim(value_time) == "") {
                CurvePoint.hide();
                if (hasMax) {
                    drawMax();
                }
                return;
            };
            cxt2.clearRect(0, 0, canvas.width, canvas.height);
            //画当前点垂直，水平线
            cxt2.beginPath();
            var currentTime = new Date(value_time).getTime();
            var x = Math.floor(((currentTime - firstCount) / allCount) * (canvas_width - canvas.width / ilen)) + 0.5;
            var yh = (NextPoint != true && NextPoint) ? (V[Configs.Trend.Point][1] ? V[Configs.Trend.Point][1] : 0) : (V[np][1] ? V[np][1] : 0);
            var y = canvas.padding[0] + Math.floor(canvas_height - (yh - Yzero) * canvas_height / Ylast) + 0.5;
            /*
            cxt2.moveTo(0, y);
            cxt2.lineTo(cvs.width, y);
            cxt2.moveTo(x, 0);
            cxt2.lineTo(x, cvs.height);
            cxt2.stroke();
            */
            cxt2.beginPath();
            cxt2.arc(x, y, 3, 0, 2 * Math.PI);
            cxt2.fill();
            if (container.id == "Trend") {
                value = value < 0 ? 0 : value;
            }
            CurvePoint.html("<span>" + Pages.footer(Pages.changetime(value_time),"page") + "<em>" + value +unit + "</em></span>").show();
        }
    };
};
/*
* 温度计
* 温度计alarm值,刻度值,当前温度必须传递
*/
Chart.Thermometer = {
    height: 0
},
Chart.drawThermometer = function (target, options) {
    var setting = {
        imgs: {
            fingerWidth: 13,
            height: 16,
            left: 26,
            top: 30
        },
        canvas: {
            width: null,
            height: null,
            Class: "thermometer-canvas"
        },
        thermometer: {
            width: 127, //温度计主区域宽度
            height: 6, 	//温度计主区域高度
            top: 34, 	//温度计主区域上边距
            fingerLeft: 26, //温度指针左边距
            fingerTop: 21	//温度指针上边距
        },
        alarm: {
            color: ["#ff5400", "#a8ff00", "#ffae00", "#ff0000"], 	//警示区域不同的颜色显示,与下面的值一一对应
            value: []		//温度计主区域告警显示范围
        },
        scale: {
            value: [], //温度计刻度
            Min: [1, 5], 	//小刻度线宽、高
            Max: [1, 9], 	//大刻度线宽、高
            color: "#2a2a2a",
            Class: "thermometer-text"
        },
        type: 0,           //0->表示首页温度计,1->表示battery页温度计
        value: 0			//温度计指针值
    };
    //判断目标div是否存在
    var container = document.getElementById(target);
    //强制container position为relative
    if (!container) {
        //alert(Language.Html['022'] + target + "\"的元素不存在！");
        return;
    };
    if ($.type(options) !== "object") {
        alert(Language.Chart["001"]);
        return;
    };
    setting = $.extend(true, {}, setting, options);
    //处理value的各值,可以传数字也可以传数组
    setting.valueStatus = [setting.value[1], setting.value[2], setting.value[3]];
    if ($.type(setting.value) == "array") {
        setting.value = $.type(setting.value[0]) == "array" ? setting.value[0][0] : setting.value[0];
    };
    $(container).css({ "position": "relative" });
    //简化调用
    var imgs = setting.imgs,
	thermometer = setting.thermometer,
	alarm = setting.alarm,
	scale = setting.scale,
	canvas = setting.canvas;
    //把刻度的最大值传入alarm value数据,作为绘制的起点和终点.作了一个循环，如果alarm的最大值大于刻度值，则删除这些大于的值
    for (var o = 0, olen = alarm.value.length; o < olen; o++) {
        if (alarm.value[alarm.value.length - 1] >= scale.value[scale.value.length - 1]) {
            alarm.value.pop();
        }
    }
    alarm.value.push(scale.value[scale.value.length - 1]);
    //获取容器的宽高
    var container_w = canvas.width ? canvas.width : $(container).width();
    var container_h = canvas.height ? canvas.height : $(container).height();
    //创建canvas
    var cvs = document.createElement("canvas");
    cvs.id = target + "_canvas";
    cvs.className = canvas.Class;
    cvs.width = container_w;
    cvs.height = container_h;
    $(container).html(cvs);

    this.canvasForIE(cvs);
    var cxt = cvs.getContext("2d");
    cxt.canvas = null;
    //画电源页面的温度计
    if (setting.type == 1) {
        //countScaleHeight用于计算对应值所占刻度的宽度
        function countScaleHeight(value) {
            return ((value - (scale.value[0])) / (scale.value[scale.value.length - 1] - scale.value[0])) * syz_h;
        }
        //绘制警示区域,几个矩形
        cxt.beginPath();
        cxt.lineWidth = 1;
        var x = imgs.left + 21;
        var syz_h = 89;
        var t = imgs.top + syz_h + 14;
        for (var i = 0, ilen = alarm.value.length; i < ilen; i++) {
            var h = countScaleHeight(alarm.value[i]) - (i > 0 ? countScaleHeight(alarm.value[i - 1]) : 0);
            h = parseInt(h) + 0.5;
            var w = 10;
            var y = t - countScaleHeight(alarm.value[i]);
            cxt.fillStyle = alarm.color[i];
            cxt.fillRect(x, y, w, h);
        }
        cxt.save();
        //画水银柱,采用css及背景图片结合
        var syz = document.createElement("div");
        syz.className = target + "-szy";
        $(syz).css({ "left": x-5, top: t - syz_h });
        var syz_v = document.createElement("i");
        var syz_vv = document.createElement("span");
        var syz_value = ((setting.value - scale.value[0]) / (scale.value[scale.value.length - 1] - scale.value[0])) * 89;
        if (setting.valueStatus[1] == 0 && setting.valueStatus[2] == 0) {
            if (/IE6/g.test(Configs.Browser)) {
                $(syz_vv).css({ "height": this.Thermometer.height }).css({ "height": syz_value + 20 - 9});
            } else {
                $(syz_vv).css({ "height": this.Thermometer.height }).animate({ "height": syz_value + 20 - 9 }, 1000);
            }
        }
        this.Thermometer.height = syz_value + 20;
        var em = document.createElement("em");
        $(syz_vv).append(syz_v);
        $(em).append(syz_vv);
        $(syz).append(em);
        $(container).append(syz);
        //画刻度值
        cxt.beginPath();
        cxt.lineWidth = 1;
        cxt.strokeStyle = "#a2a2a2";
        var scaleDiv = document.createElement("div");
        scaleDiv.className = target + "-scale";
        for (var j = 0, jlen = (scale.value.length - 1) * 6; j <= jlen; j++) {
            var h = Math.floor(j * (89 / jlen)) + 39 + (j > 0 ? 0.5 : -0.5);
            cxt.moveTo(x + 20, h);
            if (j % 6 == 0) {
                cxt.lineTo(x + 30, h);
                var span = document.createElement("span");
                span.innerHTML = scale.value[scale.value.length - 1 - j / 6];
                $(span).css({ "top": h - 39 });
                $(scaleDiv).append(span);
            } else {
                cxt.lineTo(x + 27, h);
            }
        }
        cxt.stroke();
        $(container).append(scaleDiv);

        var bg = document.createElement("div");
        var topbg = document.createElement("i");
        bg.className = target + "-bg";
        $(container).append(bg);
        $(bg).css({ "left": imgs.left, "top": imgs.top });
        $(bg).append(topbg);
        return;
    }
    var background = document.createElement("div");
    background.className = target + "-background";
    $(container).append(background);
    $(background).css({ "left": imgs.left, "top": imgs.top });

    var finger_left = thermometer.fingerLeft + countScaleWidth(setting.value);
    var finger = document.createElement("div");
    finger.className = target + "-finger";
    $(container).append(finger);
    $(finger).css({ "left": finger_left, "top": thermometer.fingerTop });
    cxt.save();

    //countScaleWidth用于计算对应值所占刻度的宽度
    function countScaleWidth(value) {
        return ((value - (scale.value[0])) / (scale.value[scale.value.length - 1] - scale.value[0])) * thermometer.width;
    }
    thermometer.left = thermometer.fingerLeft + Math.floor(setting.imgs.fingerWidth / 2);
    //绘制警示区域,几个矩形
    for (var i = 0, ilen = alarm.value.length; i < ilen; i++) {
        var h = thermometer.height;
        var w = countScaleWidth(alarm.value[i]) - (i > 0 ? countScaleWidth(alarm.value[i - 1]) : 0);
        var y = thermometer.top;
        var x = thermometer.left + (i > 0 ? countScaleWidth(alarm.value[i - 1]) : 0);
        cxt.fillStyle = alarm.color[i];
        cxt.fillRect(x, y, w, h);
    }
    cxt.save();
    //绘制刻度
    var scale_y = 45;
    var scale_x = thermometer.fingerLeft + Math.floor(setting.imgs.fingerWidth / 2);
    //绘制刻度值,采用div
    var scaleText = document.createElement("div");
    scaleText.id = target + "_text";
    $(container).append(scaleText);
    $(scaleText).addClass(scale.Class);
    $(scaleText).css({ "top": scale_y + scale.Max[1] });
    cxt.strokeStyle = scale.color;
    cxt.lineWidth = scale.Min[0];
    for (var j = 0, jlen = scale.value.length; j < jlen; j++) {
        var startX = Math.floor(scale_x + countScaleWidth(scale.value[j])) + 0.5;
        cxt.moveTo(startX, scale_y);
        cxt.lineTo(startX, scale_y + 6);
        var minw = countScaleWidth(scale.value[j + 1]) - (j > 0 ? countScaleWidth(scale.value[j]) : 0);
        var startX2 = Math.floor(startX + minw / 2) + 0.5;
        cxt.moveTo(startX2, scale_y);
        cxt.lineTo(startX2, scale_y + 3);
        //刻度值
        var span = document.createElement("span");
        span.innerHTML = scale.value[j];
        $(scaleText).append(span);
        $(span).css({ "left": startX - $(span).width() / 2 });
    }
    cxt.stroke();
    cxt.save();
    cxt = null;
    cvs = null;
};
/*
* 电压电流表
*/
Chart.drawMeter = function (target, options, callBack) {
    var setting = {
        center: [96, 85], //画布的中心点
        finger: {
            color: ["#97a4b5", "#EEEEEE"],
            angle: [180, 30],
            width: [8, 14],
            radius: 28,  //画内指针的长度
            speed: 10    //指针摆动速度，默认10ms
        },
        background: {
            radius1: 27, //第一个背景圆的半径
            radius2: 27, //第二个背景圆的半径
            color: "#f6faff"	//背景圆的颜色
        },
        canvas: {
            width: null, 	//画布宽度
            height: null, //画布高度
            Class: "VAmeter-canvas"	//画布样式
        },
        angle: {
            Min: -30, //仪表最初开始画的角度 
            Max: 210   //仪表最终结束画的角度
        },
        alarm: {
            outer: {
                value: [], //外数值,与外刻度一样
                color: ["#F58800", "#F6A601", "#82C013", "#F6A601", "#F58800"], //外警示值颜色
                radius: 50	//外半径
            },
            inner: {
                value: [], //内数值,与内刻度一样
                color: ["#a2ee00", "#ffae00", "#d8271c"], //内警示颜色
                radius: 31	//内半径
            }
        },
        scale: {
            outer: [], 	//外刻度,无须值
            inner: []		//内刻度,无须值
        },
        value: {
            outer: [45, ""], 	//外值
            inner: [85], 	//内值
            toFix: [1], //保留的小数点位[outer toFix,inner toFix]
            unit: ["", "%"]	//单位值[outer int,inner int]
        },
        rotate: 180,  //绘图的旋转角度
        pa: false,    //相对peak current和average current图
        type: 0       //1->画电源的时间表盘
    };

    var that = this;
    //判断目标div是否存在
    var container = document.getElementById(target);
    //强制container position为relative
    if (!container) {
        //alert("id为\"" + target + "\"的元素不存在！"); 
        return;
    };
    if ($.type(options) !== "object") {
        alert(Language.Chart["001"]);
        return;
    };

    //处理value的各值,可以传数字也可以传数组
    for (var v in options.value) {
        if ($.type(options.value[v]) != "array") {
            options.value[v] = [options.value[v]];
        }
    }

    //针对电源时间表，处理V，如果大于10<=100,V=14,如果大于100小于1000,V==16
    if (options.type == 1) {
        var out = options.value.outer[0];
        if (out > 10 && out <= 100) {
            options.value.outer[0] = 12;
        } else if (out > 100 && out < 1000) {
            options.value.outer[0] = 16;
        }
    };
    this.ArraySameCount = [];
    var optionsColorLength = options.alarm.outer.color.length;

    setting = $.extend(true, {}, setting, options);
    if ($.type(setting.alarm.outer.value) == "array") {
        setting.alarm.outer.value = Chart.AarryDeleteSame(setting.alarm.outer.value);
    };
    if ($.type(setting.alarm.inner.value) == "array") {
        setting.alarm.inner.value = Chart.AarryDeleteSame(setting.alarm.inner.value);
    }
    setting.alarm.outer.color.length = optionsColorLength;
    //处理value值是否大于或小于表盘区间,小于则指针放在最小，大于则指针放在最大
    var svo_0 = setting.value.outer[0];
    var sao_v = setting.alarm.outer.value[0];
    if (svo_0 < sao_v) {
        svo_0 = sao_v;
    }
    if (setting.value.outer[0] > setting.alarm.outer.value[setting.alarm.outer.value.length - 1]) {
        svo_0 = setting.alarm.outer.value[setting.alarm.outer.value.length - 1];
    };
    //深拷贝options的值给setting;
    $(container).css({ "position": "relative" });

    $.extend(true, setting.scale.outer, setting.alarm.outer.value);
    $.extend(true, setting.scale.inner, setting.alarm.inner.value);
    setting.alarm.outer.value.shift();
    setting.alarm.inner.value.shift();

    //简化调用
    var canvas = setting.canvas,
	    background = setting.background,
	    alarm = setting.alarm,
	    scale = setting.scale,
	    angle = setting.angle,
        finger = setting.finger,
        V = setting.value;
    //处理alarm值，如果比刻度值大，则删除
    for (var o = 0, olen = alarm.outer.value.length; o < olen; o++) {
        if (alarm.outer.value[alarm.outer.value.length - 1] > scale.outer[scale.outer.length - 1]) {
            alarm.outer.value.pop();
        }
    }
    for (var o = 0, olen = alarm.inner.value.length; o < olen; o++) {
        if (alarm.inner.value[alarm.inner.value.length - 1] > scale.inner[scale.inner.length - 1]) {
            alarm.inner.value.pop();
        }
    }
    //获取容器的宽高
    var container_w = canvas.width ? canvas.width : $(container).width();
    var container_h = canvas.height ? canvas.height : $(container).height();
    //创建canvas
    //var cvs_html = "<canvas id='" + target + "_canvas' width='" + container_w + "' height='" + container_h + "' class='" + canvas.Class + "'></canvas>";
    var cvs = document.createElement("canvas");
    cvs.id = target+"_canvas";
    cvs.width = container_w;
    cvs.height = container_h;
    cvs.className = canvas.Class;
    $(container).html(cvs);
    this.canvasForIE(cvs);
    var cxt = cvs.getContext("2d");
    cxt.canvas = null;
    //初始画布
    cxt.clearRect(0, 0, 1000, 1000);
    cxt.lineWidth = 1;
    var maxAngle = setting.angle.Max - setting.angle.Min;
    //设置圆心点
    cxt.translate(setting.center[0], setting.center[1]);
    //处理出前边重复了几个，后边重复了几个
    for (var s = 0, slen = this.ArraySameCount.length; s < slen; s++) {
        alarm.outer.color.splice(this.ArraySameCount[s], 1);
    };
    //画大指针，采用div+css->避免闪动;
    var bigFinger = document.createElement("div");
    bigFinger.className = target + "-bigfinger";
    $(container).append(bigFinger);
    $(bigFinger).css({ "left": setting.center[0] - $(bigFinger).outerWidth(true) / 2, "top": setting.center[1] - $(bigFinger).outerHeight(true) / 2 });

    var bigfinger_canvas = document.createElement("canvas");
    var cvs_big = document.createElement("canvas");
    cvs_big.id = target + "_canvas_big";
    cvs_big.className = canvas.Class + "-big";
    cvs_big.width = container_w;
    cvs_big.height = container_h;
    $(container).append(cvs_big);
    this.canvasForIE(cvs_big);
    var cxt_big = cvs_big.getContext("2d");
    cxt_big.canvas = null;
    cxt_big.translate(setting.center[0], setting.center[1]);

    //画大指针
    drawOuter();
    drawInnner();
    drawPAfinger();
    //画电源闪断图片
    if (setting.type == 1) {
        //采用层加CSS比较好,直接用drawImage可能有时候显示不出来
        var breakImg = document.createElement("div");
        breakImg.className = target + "-breakimg";
        $(container).append(breakImg);
    };
    cxt.save();
    //当图片加载完了之后,再绘制仪表上的东西
    var that = this;
    function drawOuter() {
        //画外围的扇形
        var outerList = alarm.outer.value;
        var outerRadius = alarm.outer.radius;
        cxt.rotate(that.transformAngle(setting.rotate));
        for (var i = 0, ilen = outerList.length; i < ilen; i++) {
            var v = ((outerList[i] - scale.outer[0]) / (scale.outer[scale.outer.length - 1] - scale.outer[0])) * maxAngle + angle.Min;
            if (i > 0) {
                var v2 = ((outerList[i - 1] - scale.outer[0]) / (scale.outer[scale.outer.length - 1] - scale.outer[0])) * maxAngle + angle.Min;
            } else {
                var v2 = angle.Min;
            }
            cxt.beginPath();
            //cxt.strokeStyle = alarm.outer.color[i];
            cxt.fillStyle = alarm.outer.color[i];
            cxt.moveTo(background.radius1 * Math.cos(that.transformAngle(v2)), background.radius1 * Math.sin(that.transformAngle(v2)));
            cxt.lineTo(outerRadius * Math.cos(that.transformAngle(v2)), outerRadius * Math.sin(that.transformAngle(v2)));
            cxt.arc(0, 0, outerRadius, that.transformAngle(v2), that.transformAngle(v), false);
            cxt.lineTo(background.radius1 * Math.cos(that.transformAngle(v)), background.radius1 * Math.sin(that.transformAngle(v)));
            cxt.arc(0, 0, background.radius1, that.transformAngle(v), that.transformAngle(v2), true);
            cxt.fill();
            //cxt.stroke();
            //画边影
            cxt.beginPath();
            cxt.strokeStyle = "#999";
            cxt.globalAlpha = 0.4;
            cxt.lineWidth = 1;
            if (i == 0) {
                cxt.arc(0, 0, outerRadius - 1, that.transformAngle(angle.Min), that.transformAngle(v), false);
            }
            if (i > 0) {
                cxt.arc(0, 0, outerRadius - 1, that.transformAngle(v2), that.transformAngle(v), false);
            }
            cxt.stroke();
            cxt.globalAlpha = 1;
            v = null;
            v2 = null;
        }
        cxt.save();
        //布局坐标值,利用div
        var outerDiv = document.createElement("div");
        outerDiv.className = target + "-scale";
        $(container).append(outerDiv);
        for (var z = 0, zlen = scale.outer.length; z < zlen; z++) {
            var span = document.createElement("span");
            if (V.toFix != 0) {
                span.innerHTML = scale.outer[z].toFixed(V.toFix[0]) + (z > 0 ? V.unit[0] : "");
                if (setting.type == 1 && z % 5 != 0) {
                    span.innerHTML = "";
                }
            } else {
                span.innerHTML = scale.outer[z] + (z > 0 ? V.unit[0] : "");
                if (setting.type == 1 && z % 5 != 0) {
                    span.innerHTML = "";
                }
            }
            $(outerDiv).append(span);
            var angle_01 = ((scale.outer[z] - scale.outer[0]) / (scale.outer[scale.outer.length - 1] - scale.outer[0])) * maxAngle;
            var xxx = setting.center[0] - (outerRadius + 15) * Math.cos(that.transformAngle(angle_01 - Math.abs(setting.angle.Min)));
            var yyy = setting.center[1] - (outerRadius + 15) * Math.sin(that.transformAngle(angle_01 - Math.abs(setting.angle.Min)));
            $(span).css({ "position": "absolute", "text-align": "center", "z-inde": "9999", "left": xxx - $(span).width() / 2, "top": yyy - ($(span).height() / 2) });
            span = null;
            angle_01 = null;
            xxx = null;
            yyy = null;
        }
    };

    function drawInnner() {
        if (!setting.pa) {
            cxt.save();
            cxt.rotate(-that.transformAngle(setting.rotate));
        }
    };
    function drawPAfinger() {
        var ov = Configs.Chart[cvs_big.id];
        var v = [svo_0][0];
        drawFinger(v);
        Configs.Chart[cvs_big.id] = [svo_0][0];
        if (setting.pa) { cxt_big.rotate(-that.transformAngle(180)); }
        var meterCenter = document.createElement("div");
        meterCenter.className = target + "-meterCenter";
        $(container).append(meterCenter);
        $(meterCenter).css({ "left": setting.center[0] - $(meterCenter).outerWidth(true) / 2, "top": setting.center[1] - $(meterCenter).outerHeight(true) / 2 });
    }
    function drawFinger(cv) {
        //画pa电流表的指针
        var paValue = cv;
        if (setting.pa) {
            paValue.push(V.inner[0]);
        }
        var finger_tempwidth = finger.width[1],  //设定指针大的一头两点与圆心点的形成圆的半径
		finger_tempangle = finger.angle[1];    //设定指针大的一头两点与圆心点所形成的角度
        cxt_big.fillStyle = finger.color[1];
        var a_current = 2 * angle.Min - ((paValue - scale.outer[0]) / (scale.outer[scale.outer.length - 1] - scale.outer[0])) * maxAngle; //当前数值所占的角度
        var x1 = -finger_tempwidth * Math.sin(that.transformAngle(a_current - finger_tempangle / 2));
        var y1 = -finger_tempwidth * Math.cos(that.transformAngle(a_current - finger_tempangle / 2));
        var x2 = -finger_tempwidth * Math.cos(that.transformAngle(90 - (a_current + finger_tempangle / 2))); //90为直角的度数
        var y2 = -finger_tempwidth * Math.sin(that.transformAngle(90 - (a_current + finger_tempangle / 2))); //90为直角的度数
        var x = alarm.outer.radius * Math.sin(that.transformAngle(a_current));
        var y = alarm.outer.radius * Math.cos(that.transformAngle(a_current));
        cxt_big.beginPath();
        cxt_big.moveTo(x1, y1);
        cxt_big.lineTo(x, y);
        cxt_big.lineTo(x2, y2);
        cxt_big.fill();
        cxt_big.save();
    }
    options = null;
};
/*
* 绘制Hybrid
*/
Chart.drawHybridCurrentTime = null; //记录最后一次画线的X轴最大日期
Chart.drawHybrid = function (target, options) {
    var setting = {
        canvas: {
            width: 510,
            height: 300,
            Class: "hybrid-canvas"
        },
        scale: {
            X: ["04-01", "04-02", "04-03", "04-04", "04-05", "04-06", "04-07", "04-08"],
            Y: ["0.0", "20.0", "40.0", "60.0", "80.0", "100.0"]
        },
        unit: "A",
        period: 7, //数据周期为7天
        week: 1,
        value: {
            AC: [],
            Solar: [],
            DG: [],
            Wind: []
        },
        color: {
            "AC": "#ED1C23",
            "Solar": "#EDB91C",
            "DG": "#000",
            "Wind": "#008CC7"
        }
    };
    var that = this;
    //判断目标div是否存在
    var container = document.getElementById(target);
    //强制container position为relative
    if (!container) {
        //alert("id为\"" + target + "\"的元素不存在！"); 
        return;
    };
    if ($.type(options) !== "object") {
        alert(Language.Chart["001"]);
        return
    };
    //初始化数据
    for (var item in setting.value) {
        setting.value[item] = [];
    };
    //基本参数深拷贝
    setting = $.extend(true, {}, setting, options);
    $(container).css({ "position": "relative" });
    //简化调用
    var canvas = setting.canvas;
    var scale = setting.scale;
    var V = setting.value;
    var isHaveCanvas = document.getElementById(target + "_canvas");
    if (isHaveCanvas) {
        var cvs = isHaveCanvas;
    } else {
        //创建canvas
        var cvs = document.createElement("canvas");
        cvs.id = target + "_canvas";
        cvs.className = canvas.Class;
        cvs.width = canvas.width;
        cvs.height = canvas.height;
        $(container).append(cvs);
    }
    this.canvasForIE(cvs);
    var cxt = cvs.getContext("2d");
    cxt.canvas = null;
    //初始画布
    cxt.clearRect(0, 0, 1000, 1000);
    //根据period,删减scale X
    if (setting.period < scale.X.length) {
        scale.X.splice(0, scale.X.length - setting.period);
    }
    var maxLength = 0, maxItem = null;
    for (var p in V) {
        if (V[p].length > maxLength) {
            maxLength = V[p].length;
            maxItem = p;
        }
    }
    if (maxItem) {
        var currentTime = V[maxItem][maxLength - 1][0].split(" ")[0];
    } else {
        currentTime = this.drawHybridCurrentTime;
    }
    //初始化currentTime
    var cTime = new Date(new Date(currentTime).setDate(new Date(currentTime).getDate() - setting.period * (setting.week - 1)));
    currentTime = cTime.getFullYear() + "/" + this.makeupZero(cTime.getMonth() + 1) + "/" + this.makeupZero(cTime.getDate());
    //从所有数据当中,取出7天的数据
    function getWeekDatas(n) {
        //格式化当前请求"周"的数据
        for (var p in V) {
            if (!V[p] || V[p].length == 0) { continue; }
            if (V[p].length > 0) {
                //确定当前时间,并推算出7天前的那一天
                var currentDate = new Date(currentTime), //当前时间
					currentDateIndex = null, //当前时间所在位置
					prevSeven = new Date(new Date(currentTime).setDate(new Date(currentTime).getDate() - setting.period)), //7天前
					prevSevenIndex = null; //7天前时间所在位置
                for (var i = 0, ilen = V[p].length; i < ilen; i++) {
                    var tempTime1 = V[p][i][0].split(" ")[0],
						tempDate1 = new Date(tempTime1).getTime();
                    if ((currentDate - tempDate1) >= 0) {
                        currentDateIndex = i;
                    }
                    if ((prevSeven - tempDate1) >= 0) {
                        prevSevenIndex = i + 1;
                    }
                }
                if (currentDateIndex != null) { //当前时间存在数据中
                    V[p].splice(currentDateIndex + 1, ilen - (currentDateIndex + 1));
                    if (prevSevenIndex != null) {//7天前的时间存在则删除,没有的话就不再删除
                        V[p].splice(0, prevSevenIndex);
                    }
                } else {//当前时间不存在数据中,删除所有
                    V[p].splice(0, ilen);
                }
            }
        }
    }
    //初始化读取"周"数据,
    //getWeekDatas(setting.week);
    //确定X轴的值,如果某天缺少则补上;
    var lastDate = new Date(currentTime);
    lastDate.setDate(lastDate.getDate() + 1);
    for (var j = setting.period; j >= 0; j--) {
        if (j == setting.period) {
            setting.scale.X[j] = new Date(lastDate.setDate(lastDate.getDate()));
        } else {
            setting.scale.X[j] = new Date(lastDate.setDate(lastDate.getDate() - 1));
        }
        setting.scale.X[j].setHours(0);
        setting.scale.X[j].setMinutes(0);
        setting.scale.X[j].setSeconds(0);
        setting.scale.X[j].setMilliseconds(0);
    }
    //画网格,根据scale的X,Y值
    //画X网格线
    cxt.lineWidth = 1;
    var lineColor = ["#afafaf", "#898989"];
    var Hybrid = $("#hybrid");
    if (isHaveCanvas) {
        var xdiv = Hybrid.find("div." + target + "-x");
    } else {
        var xdiv = document.createElement("div");
        xdiv.className = target + "-x";
        $(container).append(xdiv);
    }
    for (var i = 0, ilen = (scale.X.length - 1) * 12; i <= ilen; i++) {
        var x = canvas.width / ilen;
        var x1 = Math.floor(x * i) + 0.5;
        cxt.beginPath();
        if (i % 12 == 0 || i == ilen) {
            cxt.strokeStyle = lineColor[0];
            var spanHTML = this.makeupZero(scale.X[i / 12].getMonth() + 1) + "-" + this.makeupZero(scale.X[i / 12].getDate());
            if (i == ilen) {
                this.drawHybridCurrentTime = scale.X[i / 12].getFullYear() + "/" + this.makeupZero(scale.X[i / 12].getMonth() + 1) + "/" + this.makeupZero(scale.X[i / 12].getDate() - 1);
            }
            if (isHaveCanvas) {
                var span = xdiv.find("span")[i / 12];
                span.innerHTML = spanHTML;
            } else {
                var span = document.createElement("span");
                span.innerHTML = spanHTML;
                $(xdiv).append(span);
            }
            $(span).css({ "left": x1 - $(span).width() / 2 });
        } else {
            cxt.strokeStyle = lineColor[1];
        }
        cxt.moveTo(x1, 0);
        cxt.lineTo(x1, canvas.height);
        cxt.stroke();
    }
    //没有数据时，隐藏时间轴显示 
    if (no_data_status) {
        $(xdiv).hide();
    } else {
        $(xdiv).show();
    }
    cxt.beginPath();
    cxt.strokeStyle = lineColor[0];
    cxt.moveTo(canvas.width - 0.5, 0);
    cxt.lineTo(canvas.width - 0.5, canvas.height);
    cxt.stroke();
    cxt.save();
    //画Y网格线
    if (isHaveCanvas) {
        var ydiv = Hybrid.find("div." + target + "-y");
    } else {
        var ydiv = document.createElement("div");
        ydiv.className = target + "-y";
        $(container).append(ydiv);
    }
    for (var i = 0, ilen = (scale.Y.length - 1) * 10; i <= ilen; i++) {
        var y = canvas.height / ilen;
        var y1 = Math.floor(y * i) + 0.5;
        cxt.beginPath();
        if (i % 10 == 0 || i == ilen) {
            cxt.strokeStyle = lineColor[0];
            if (isHaveCanvas) {
                var span = ydiv.find("span")[i / 10];
                span.innerHTML = scale.Y[i / 10] + setting.unit;
            } else {
                var span = document.createElement("span");
                span.innerHTML = scale.Y[i / 10] + setting.unit;
                $(ydiv).append(span);
                $(span).css({ "top": canvas.height - y1 - (i == 0 ? $(span).height() : $(span).height() / 2) });
            }
        } else {
            cxt.strokeStyle = lineColor[1];
        }
        cxt.moveTo(0.5, y1);
        cxt.lineTo(canvas.width - 1, y1);
        cxt.stroke();
    }
    cxt.beginPath();
    cxt.strokeStyle = lineColor[0];
    cxt.moveTo(0, canvas.height - 0.5);
    cxt.lineTo(canvas.width, canvas.height - 0.5);
    cxt.stroke();
    cxt.save();
    //画曲线
    //总点数
    var allCount = setting.period * 24 * 60 * 60 * 1000; //一周时间长
    var firstCount = scale.X[0].getTime();
    cxt.lineWidth = 1.5;
    var maxValue = Number(scale.Y[scale.Y.length - 1]);
    var valueOut = false;
    var no_data_status = true;
    var StartTime = scale.X[0].getTime();
    var EndTime = scale.X[scale.X.length - 1].getTime();
    for (var line in V) {
        if (!V[line]) { continue; }
        cxt.strokeStyle = setting.color[line];
        cxt.fillStyle = setting.color[line];
        cxt.beginPath();
        for (var z = 0, zlen = V[line].length; z < zlen; z++) {
            var CurrentTime = new Date(V[line][z][0]).getTime();
            var x = Math.floor(((CurrentTime - firstCount) / allCount) * canvas.width) + 0.5;
            var y = Math.floor(canvas.height - (V[line][z][1] / 100) * canvas.height) + 0.5;
            if (y >= canvas.height) { y = canvas.height - 1; }
            if (y <= 0.5) { y = 1; }
            if (CurrentTime >= StartTime && CurrentTime <= EndTime) {
                //判断当前时间区间是否有数据
                no_data_status = false;
            };
            if (!valueOut) {
                if (CurrentTime >= StartTime && CurrentTime <= EndTime) {
                    if (V[line][z][1] > maxValue) {
                        valueOut = true;
                    };
                };
            };
            if (zlen == 1) {
                //如果一周内只有一个点，则只画一个点
                cxt.beginPath();
                if (y + 1.5 >= canvas.height) { y = canvas.height - 2; }
                if (y <= 2) { y = 2 }
                cxt.arc(x, y, 1, 0, Math.PI * 2, true);
                cxt.fill();
                cxt.save();
                cxt.closePath();
            } else {
                if (z == 0) {
                    cxt.moveTo(x, y);
                } else {
                    cxt.lineTo(x, y);
                }
            }
        }
        cxt.stroke();
    }
    cxt.save();
    //检测值是否有超出范围
    var HybridTip = $(container).find("div.hybrid-tip");
    if (valueOut) {
        if (HybridTip[0]) {
            HybridTip.show();
        } else {
            var HybridTip = document.createElement("div");
            HybridTip.className = "hybrid-tip";
            HybridTip.innerHTML = Language.Chart["002"];
            $(container).append(HybridTip);
        };
    } else {
        HybridTip.hide();
    };
    /*判断是否存在数据，如果不存在,隐藏时间显示，并显示"no data"*/
    var hybirdNoData = $("div.hybird-nodata");
    if (no_data_status) {
        hybirdNoData.show();
    } else {
        hybirdNoData.hide();
    };
    var hybridHandle = $("#hybridHandle");
    if (hybridHandle.css("visibility") == "hidden") {
        hybridHandle.css({ "visibility": "visible" });
    }
}
/*
* 绘制柱形图
*/
Chart.drawColumn = function (target, options) {
    var setting = {
        canvas: {
            width: 480,
            height: 210,
            Class: "cabinet-map-canvas"
        },
        scale: {
            X: [],
            Y: []
        },
        unit: "A",
        color: ["#00c158", "#ffc800", "#ff0000"],
        value: null,
		maxLength:25
    };
    var that = this;
    //判断目标div是否存在
    var container = document.getElementById(target);
    //强制container position为relative
    if (!container) {
        return;
    };
    if ($.type(options) !== "object") {
        alert(Language.Chart["001"]);
        return
    };
    //基本参数深拷贝
    setting = $.extend(true, {}, setting, options);
    $(container).css({ "position": "relative" });
    //简化调用
    var canvas = setting.canvas;
    var scale = setting.scale;
    var V = setting.value;
    var isHaveCanvas = document.getElementById(target + "_canvas");
    if (isHaveCanvas) {
        var cvs = isHaveCanvas;
    } else {
        //创建canvas
        var cvs = document.createElement("canvas");
        cvs.id = target + "_canvas";
        cvs.className = canvas.Class;
        cvs.width = canvas.width;
        cvs.height = canvas.height;
        $(container).append(cvs);
    }
    this.canvasForIE(cvs);
    var cxt = cvs.getContext("2d");
    cxt.canvas = null;
	
    //画X轴
    var ilen = V.length <= setting.maxLength ? V.length : setting.maxLength;
    var item = Math.floor((canvas.width - 4.5) / ilen);
    var canvasWidth = item * ilen;
    cxt.strokeStyle = "#aaa";
    cxt.lineWidth = 1;
    cxt.beginPath();
    cxt.moveTo(4.5, canvas.height - 4.5);
    cxt.lineTo(canvasWidth+4.5, canvas.height - 4.5);
    cxt.stroke();
	
    var DFrag = document.createDocumentFragment();
    var DFrag2 = document.createDocumentFragment();
	var DFrag3 = document.createDocumentFragment();
    //找出数据最大值
    var yMax = -Infinity,yMin = Infinity; //初始化为负无穷大
    for (var j = 0; j < ilen; j++) {
        yMax = Math.max(yMax, parseFloat(V[j][3]));
		yMin = Math.min(yMin, parseFloat(V[j][3]));
		
    }
    //设置yMax为可被10整除的整数
    yMax = yMax + (yMax % 10 == 0 ? 0 : (10 - yMax % 10));
	yMin = yMin - (yMin % 10 == 0 ? 0 : (10 - yMin % 10));
	//如果最大值为0，则设为10
	if(yMax==0){yMax = 10; };
	var vnum = 10;
	var vitem = Math.floor((canvas.height - 4.5) / vnum);
	//画Y轴
	cxt.lineWidth = 1;
	cxt.beginPath();
    cxt.moveTo(4.5, canvas.height-vitem*10-6.5);
    cxt.lineTo(4.5, canvas.height-4.5);
	cxt.stroke();
	for (var y = 0; y <= vnum; y++) {
		cxt.beginPath();
		cxt.strokeStyle = "#aaa";
		var yv = canvas.height-y*vitem-6.5;
		cxt.moveTo(0, yv);
		cxt.lineTo(4.5, yv);
		cxt.stroke();
		//为Y轴赋值
		var span = document.createElement("span");
		//span.innerHTML = yMin + (yMax-yMin)/vnum * y;
		span.innerHTML = yMax/vnum * y;
		span.style.top = yv + "px";
		DFrag3.appendChild(span);
		//画Y轴数值对应横线
		if(y==0){continue; }
		cxt.beginPath();
		cxt.strokeStyle = "#e5e5e5";
		var yv = canvas.height-y*vitem-6.5;
		cxt.moveTo(5.0, yv);
		cxt.lineTo(canvasWidth, yv);
		cxt.stroke();
	}
	cxt.strokeStyle = "#aaa";
	var vDiv = document.createElement("div");
	vDiv.className = target+"-v";
	$(vDiv).css({
		"height":canvas.height+20
	});
	vDiv.appendChild(DFrag3);
    container.appendChild(vDiv);
    //判断是否为IE678
    var isIE678 = /IE6|IE7|IE8/g.test(Configs.Browser);
    var xSpan = [];
    for (var i = 0; i <= ilen; i++) {
        cxt.beginPath();
        cxt.moveTo(item * i + 4.5, canvas.height - 4.5);
        cxt.lineTo(item * i + 4.5, canvas.height);
        cxt.stroke();
        if (i < ilen) {
            var left = item * i + 4.5 + item / 2 - 6.5;
            //为X轴赋值
            var span = document.createElement("span");
            span.title = V[i][2];
            span.innerHTML = V[i][2];
            span.style.left = left + "px";
            DFrag.appendChild(span);
            xSpan.push(span);
            //画柱形
            var span = document.createElement("span");
            $(span).css({
                left: left - item / 4 + 6.5,
                height: parseFloat(V[i][3]) / yMax == 0 ? 1 : 100 * parseFloat(V[i][3]) / yMax + "%",
                width: item / 2,
                background: setting.color[V[i][4] - 1]
            });
            span.innerHTML = V[i][2] + " : " + V[i][3];
            DFrag2.appendChild(span);
        };
    }
    var XDiv = document.createElement("div");
    XDiv.className = target + "-X";
    XDiv.style.width = canvas.width + "px";
    XDiv.appendChild(DFrag);
    container.appendChild(XDiv);
    //设置弹出层的高度，以及X轴的高度，X轴最高为100px,最小为35
    
	var xSpanMax = -Infinity;
	for (var i = 0, ilen = xSpan.length; i < ilen; i++) {
		var xSpanHeight = isIE678 ? Number($(xSpan[i]).height()) : Number($(xSpan[i]).width());
		xSpanMax = Math.max(xSpanMax, xSpanHeight);
	}
	//xSpanMax = xSpanMax > 100 ? 100 : xSpanMax < 35 ? 35 : xSpanMax;
	xSpanMax = xSpanMax > 200 ? 200 : xSpanMax < 35 ? 35 : xSpanMax;
	var PromptPop = document.getElementById(setting.popup);
	if(PromptPop){
		var PromptPopHeight = setting.popupHeight+xSpanMax;
		$(PromptPop).css({"height":PromptPopHeight-100,"margin-top":-(PromptPopHeight-100)/2});
	}
	$(xSpan).css({ "width": xSpanMax + "px", "height": xSpanMax + "px" })
    var YDiv = document.createElement("div");
    YDiv.className = target + "-Y";
    YDiv.style.width = canvasWidth + "px";
    YDiv.style.height = canvas.height - 9 + "px";
    YDiv.appendChild(DFrag2);
    container.appendChild(YDiv);
    //绑定柱形图弹出数值事件
    var tip = document.createElement("div");
    tip.className = target + "-tip";
    YDiv.appendChild(tip);
    var timer = null;
    var YRPosition = $(YDiv).position().left + canvasWidth;
    $(YDiv).find("span").off().on("mousemove", function () {
        clearTimeout(timer);
        var $this = $(this);
        $(tip).html($this.text())
        var thisLeft = $this.position().left;
        var thisWidth = $this.width() / 2;
        var left = thisLeft + thisWidth;
        var bottom = $this.css("height");
        var tipWidth = $(tip).innerWidth();
        if (tipWidth + left > YRPosition) {
            left = thisLeft - tipWidth;
            $(tip).addClass("Pop_CabinetMap-tip-r");
        } else {
            $(tip).removeClass("Pop_CabinetMap-tip-r");
        }
        $(tip).fadeIn().css({ left: left, bottom: bottom });
    }).on("mouseleave", function () {
        timer = setTimeout(function () {
            $(tip).fadeOut();
        }, 1000);
    });
};
/*!
 * Author: Jerman.Xie | xieguishi@126.com
 * Date: 2013.09
 *
 * jQuery扩展: 
 * 汇集了web应用中需要用的相关jQuery扩展插件
 * $.type	->检测变量类型函数
 * $.toJSON	->把json对象转换成字符串
 * $.evalJSON->把字符串转换成JSON对象
 * $.isBrowser->检查当前浏览器
 */
(function ($) {
    // 检测各种文件类型
    $.type = function (o) {
        var _toS = Object.prototype.toString;
        var _types = {
            'undefined': 'undefined',
            'number': 'number',
            'boolean': 'boolean',
            'string': 'string',
            '[object Function]': 'function',
            '[object RegExp]': 'regexp',
            '[object Array]': 'array',
            '[object Date]': 'date',
            '[object Error]': 'error'
        };
        return _types[typeof o] || _types[_toS.call(o)] || (o ? 'object' : 'null');
    };
    // the code of these two functions is from mootools   
    var $specialChars = { '\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"': '\\"', '\\': '\\\\' };
    var $replaceChars = function (chr) {
        return $specialChars[chr] || '\\u00' + Math.floor(chr.charCodeAt() / 16).toString(16) + (chr.charCodeAt() % 16).toString(16);
    };
    // 转换成json字符串
    $.toJSON = function (o) {
        var s = [];
        switch ($.type(o)) {
            case 'undefined':
                return 'undefined';
                break;
            case 'null':
                return 'null';
                break;
            case 'number':
            case 'boolean':
            case 'date':
            case 'function':
                return o.toString();
                break;
            case 'string':
                return '"' + o.replace(/[\x00-\x1f\\"]/g, $replaceChars) + '"';
                break;
            case 'array':
                for (var i = 0, l = o.length; i < l; i++) {
                    s.push($.toJSON(o[i]));
                }
                return '[' + s.join(',') + ']';
                break;
            case 'error':
            case 'object':
                for (var p in o) {
                    s.push('"'+p+'":' + $.toJSON(o[p]));
                }
                return '{' + s.join(',') + '}';
                break;
            default:
                return '';
                break;
        }
    };
    //转换成json对象
    $.evalJSON = function (s) {
        if ($.type(s) != 'string' || !s.length) return null;
        //去掉json字符串中多余的空格，tab，回车
        s = s.replace(/[\t|\n|\x0B|\f|\r]*/g, "");
        return eval('(' + s + ')');
    };
    $.browser = function () {
        return 1;
    };
    $.isBrowser = function () {
        //检测浏览器
        var iUserAgent = navigator.userAgent;
        var iAppVersion = parseFloat(navigator.appVersion);
        var isOpera = iUserAgent.indexOf("Opera") > -1;
        var isKHTML = iUserAgent.indexOf("KHTML") > -1 || iUserAgent.indexOf("Konqueror") > -1 || iUserAgent.indexOf("AppleWebKit") > -1;
        if (isKHTML) {
            var isChrome = iUserAgent.indexOf("Chrome") > -1;
            var isSafari = iUserAgent.indexOf("AppleWebKit") > -1 && !isChrome;
            var isKonq = iUserAgent.indexOf("Konqueror") > -1;
        }
        var isIE = iUserAgent.indexOf("compatible") > -1 && iUserAgent.indexOf("MSIE") > -1 && !isOpera;
        var isMoz = iUserAgent.indexOf("Gecko") > -1 && !isKHTML;
        var isNS4 = !isOpera && !isMoz && !isKHTML && !isIE && (iUserAgent.indexOf("Mozilla") == 0) && (navigator.appName == "Netscape") && (fAppVersion >= 4.0 && fAppVersion <= 5.0);
        //此处为检测平台
        var isWin = (navigator.platform == "Win32") || (navigator.platform == "Windows");
        var isMac = (navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh");
        var isUnix = (navigator.platform == "X11") && !isWin && !isMac;
        if (isOpera) {
            return "opera";
        } else if (isChrome) {
            return "chrome";
        } else if (isSafari) {
            return "safari";
        } else if (isKonq) {
            return "konq";
        } else if (isIE) {
            //此处没用userAgent来检测，主要是考虑IE9浏览器按F12可以切换到IE7，IE8;用userAgent会检测不出来
            if (/MSIE 6.0/gi.test(navigator.appVersion)) {
                return "IE6";
            } else if (document.all && !document.querySelector) {
                return "IE7";
            } else if (document.all && document.querySelector && !document.addEventListener) {
                return "IE8";
            } else {
                return "IE9+";
            }
        } else if (isMoz) {
            return "mozilla";
        } else if (isNS4) {
            return "ns4";
        }
    };
})(jQuery);
/*!
 * $.Tabs ->alarm tab切换插件
 */
(function(a) {
    a.fn.extend({
        Tabs: function(j) {
            j = a.extend({
                event: "mouseover",
				handle : ".tab_menu",
				content : ".tab_box",
				items : ".tab_item",
                timeout: 0,
                auto: 0,
                highlight: "current",
                hide: "hide",
				currentTab : 0,
                callback: null
            },
            j);
            var i = a(this),
			e = a(j.handle,i).children("ul"),
            d = a(j.content, i).children(j.items),
			h = e.find("li");
            e.find("li").eq(j.currentTab).addClass(j.highlight);
			d.eq(j.currentTab).removeClass(j.hide);
            h.bind(j.event,
            function() {
                c(this, j.timeout);
                if (j.callback) {
                    j.callback(i)
                }
            });
            var c = function(l, k) {
                k > 0 ? setTimeout(function() {
                    b(l)
                },
                k) : b(l)
            };
            var b = function(k) {
                a(k).siblings("li").removeClass(j.highlight).end().addClass(j.highlight);
                d.siblings(j.items).addClass(j.hide).end().eq(a(k).index()).removeClass(j.hide);
            };
            if (j.auto > 0) {
                g()
            }
            i.hover(function() {
                clearInterval(i.timer)
            },
            function() {
                g()
            });
            function g() {
                if (j.auto === 0) {
                    return
                }
                i.timer = setInterval(f, j.auto)
            }
            function f() {
                var l = e.find("." + j.highlight),
                o = h.eq(0),
                k = h.length,
                m = l.index() + 1;
                l.removeClass(j.highlight);
                m === k ? n(o, 0) : n(a(l.next("li").eq(0)), m);
                function n(p, q) {
                    p.addClass(j.highlight);
                    d.siblings(j.items).addClass(j.hide).end().eq(q).removeClass(j.hide)
                }
            }
            return this
        }
    })
})(jQuery);
/*!
 * jQuery Cookie Plugin v1.3.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function(factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals.
        factory(jQuery);
    }
} (function($) {

    var pluses = /\+/g;

    function raw(s) {
        return s;
    }

    function decoded(s) {
        return decodeURIComponent(s.replace(pluses, ' '));
    }

    function converted(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }
        try {
            return config.json ? JSON.parse(s) : s;
        } catch(er) {}
    }

    var config = $.cookie = function(key, value, options) {

        // write
        if (value !== undefined) {
            options = $.extend({},
            config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires,
                t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = config.json ? JSON.stringify(value) : String(value);

            return (document.cookie = [config.raw ? key: encodeURIComponent(key), '=', config.raw ? value: encodeURIComponent(value), options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path: '', options.domain ? '; domain=' + options.domain: '', options.secure ? '; secure': ''].join(''));
        }

        // read
        var decode = config.raw ? raw: decoded;
        var cookies = document.cookie.split('; ');
        var result = key ? undefined: {};
        for (var i = 0,
        l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = decode(parts.shift());
            var cookie = decode(parts.join('='));

            if (key && key === name) {
                result = converted(cookie);
                break;
            }

            if (!key) {
                result[name] = converted(cookie);
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function(key, options) {
        if ($.cookie(key) !== undefined) {
            // Must not alter options, thus extending a fresh object...
            $.cookie(key, '', $.extend({},
            options, {
                expires: -1,
                path:"/"
            }));
            return true;
        }
        return false;
    };
}));


/*!
 * Javascript模板渲染器: 
 * 这里使用的是"doT.js"模板渲染器
 * 相关的使用文档可以在本地/docs/目录下找到
 * doT.js
 * 2011, Laura Doktorova, https://github.com/olado/doT
 * Licensed under the MIT license.
 */
(function () {
    //"use strict";

    var doT = {
        version: '1.0.0',
        templateSettings: {
            evaluate: /\<\%([\s\S]+?\}?)\%\>/g,
            interpolate: /\<\%=([\s\S]+?)\%\>/g,
            encode: /\<\%!([\s\S]+?)\%\>/g,
            use: /\<\%#([\s\S]+?)\%\>/g,
            useParams: /(^|[^\w$])def(?:\.|\[[\'\"])([\w$\.]+)(?:[\'\"]\])?\s*\:\s*([\w$\.]+|\"[^\"]+\"|\'[^\']+\'|\{[^\}]+\})/g,
            define: /\<\%##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\%\>/g,
            defineParams: /^\s*([\w$]+):([\s\S]+)/,
            conditional: /\<\%\?(\?)?\s*([\s\S]*?)\s*\%\>/g,
            iterate: /\<\%~\s*(?:\%\>|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\%\>)/g,
            varname: 'it',
            strip: true,
            append: true,
            selfcontained: false
        },
        template: undefined, //fn, compile template
        compile: undefined  //fn, for express
    };
    //扩展,如果要在模板里嵌入子模板,请用{{ }}替换<% %>,在使用子模板前，应用templateChild替换{{ }}成<% %>
    doT.templateChild = function (template) {
        if(!template){
            return "";
        } else {
            return template.replace(/{{/gi, "<%").replace(/}}/gi, "%>");
        }
    };
    if (typeof module !== 'undefined' && module.exports) {
        module.exports = doT;
    } else if (typeof define === 'function' && define.amd) {
        define(function () { return doT; });
    } else {
        (function () { return this || (0, eval)('this'); } ()).doT = doT;
    }

    function encodeHTMLSource() {
        var encodeHTMLRules = { "&": "&#38;", "<": "&#60;", ">": "&#62;", '"': '&#34;', "'": '&#39;', "/": '&#47;' },
			matchHTML = /&(?!#?\w+;)|<|>|"|'|\//g;
        return function () {
            return this ? this.replace(matchHTML, function (m) { return encodeHTMLRules[m] || m; }) : this;
        };
    }
    String.prototype.encodeHTML = encodeHTMLSource();

    var startend = {
        append: { start: "'+(", end: ")+'", endencode: "||'').toString().encodeHTML()+'" },
        split: { start: "';out+=(", end: ");out+='", endencode: "||'').toString().encodeHTML();out+='" }
    }, skip = /$^/;

    function resolveDefs(c, block, def) {
        return ((typeof block === 'string') ? block : block.toString())
		.replace(c.define || skip, function (m, code, assign, value) {
		    if (code.indexOf('def.') === 0) {
		        code = code.substring(4);
		    }
		    if (!(code in def)) {
		        if (assign === ':') {
		            if (c.defineParams) value.replace(c.defineParams, function (m, param, v) {
		                def[code] = { arg: param, text: v };
		            });
		            if (!(code in def)) def[code] = value;
		        } else {
		            new Function("def", "def['" + code + "']=" + value)(def);
		        }
		    }
		    return '';
		})
		.replace(c.use || skip, function (m, code) {
		    if (c.useParams) code = code.replace(c.useParams, function (m, s, d, param) {
		        if (def[d] && def[d].arg && param) {
		            var rw = (d + ":" + param).replace(/'|\\/g, '_');
		            def.__exp = def.__exp || {};
		            def.__exp[rw] = def[d].text.replace(new RegExp("(^|[^\\w$])" + def[d].arg + "([^\\w$])", "g"), "$1" + param + "$2");
		            return s + "def.__exp['" + rw + "']";
		        }
		    });
		    var v = new Function("def", "return " + code)(def);
		    return v ? resolveDefs(c, v, def) : v;
		});
    }

    function unescape(code) {
        return code.replace(/\\('|\\)/g, "$1").replace(/[\r\t\n]/g, ' ');
    }

    doT.template = function (tmpl, c, def) {
        c = c || doT.templateSettings;
        var cse = c.append ? startend.append : startend.split, needhtmlencode, sid = 0, indv,
			str = (c.use || c.define) ? resolveDefs(c, tmpl, def || {}) : tmpl;

        str = ("var out='" + (c.strip ? str.replace(/(^|\r|\n)\t* +| +\t*(\r|\n|$)/g, ' ')
					.replace(/\r|\n|\t|\/\*[\s\S]*?\*\//g, '') : str)
			.replace(/'|\\/g, '\\$&')
			.replace(c.interpolate || skip, function (m, code) {
			    return cse.start + unescape(code) + cse.end;
			})
			.replace(c.encode || skip, function (m, code) {
			    needhtmlencode = true;
			    return cse.start + unescape(code) + cse.endencode;
			})
			.replace(c.conditional || skip, function (m, elsecase, code) {
			    return elsecase ?
					(code ? "';}else if(" + unescape(code) + "){out+='" : "';}else{out+='") :
					(code ? "';if(" + unescape(code) + "){out+='" : "';}out+='");
			})
			.replace(c.iterate || skip, function (m, iterate, vname, iname) {
			    if (!iterate) return "';} } out+='";
			    sid += 1; indv = iname || "i" + sid; iterate = unescape(iterate);
			    return "';var arr" + sid + "=" + iterate + ";if(arr" + sid + "){var " + vname + "," + indv + "=-1,l" + sid + "=arr" + sid + ".length-1;while(" + indv + "<l" + sid + "){"
					+ vname + "=arr" + sid + "[" + indv + "+=1];out+='";
			})
			.replace(c.evaluate || skip, function (m, code) {
			    return "';" + unescape(code) + "out+='";
			})
			+ "';return out;")
			.replace(/\n/g, '\\n').replace(/\t/g, '\\t').replace(/\r/g, '\\r')
			.replace(/(\s|;|\}|^|\{)out\+='';/g, '$1').replace(/\+''/g, '')
			.replace(/(\s|;|\}|^|\{)out\+=''\+/g, '$1out+=');

        if (needhtmlencode && c.selfcontained) {
            str = "String.prototype.encodeHTML=(" + encodeHTMLSource.toString() + "());" + str;
        }
        try {
            return new Function(c.varname, str);
        } catch (e) {
            if (typeof console !== 'undefined') console.log("Could not create a template function: " + str);
            throw e;
        }
    };

    doT.compile = function (tmpl, def) {
        return doT.template(tmpl, null, def);
    };
} ());


/*! Copyright (c) 2013 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.1.3
 *
 * Requires: 1.2.2+
 */

(function (factory) {
    if ( typeof define === 'function' && define.amd ) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'];
    var toBind = 'onwheel' in document || document.documentMode >= 9 ? ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'];
    var lowestDelta, lowestDeltaXY;

    if ( $.event.fixHooks ) {
        for ( var i = toFix.length; i; ) {
            $.event.fixHooks[ toFix[--i] ] = $.event.mouseHooks;
        }
    }

    $.event.special.mousewheel = {
        setup: function() {
            if ( this.addEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.addEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = handler;
            }
        },

        teardown: function() {
            if ( this.removeEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.removeEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = null;
            }
        }
    };

    $.fn.extend({
        mousewheel: function(fn) {
            return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
        },

        unmousewheel: function(fn) {
            return this.unbind("mousewheel", fn);
        }
    });


    function handler(event) {
        var orgEvent = event || window.event,
            args = [].slice.call(arguments, 1),
            delta = 0,
            deltaX = 0,
            deltaY = 0,
            absDelta = 0,
            absDeltaXY = 0,
            fn;
        event = $.event.fix(orgEvent);
        event.type = "mousewheel";

        // Old school scrollwheel delta
        if ( orgEvent.wheelDelta ) { delta = orgEvent.wheelDelta; }
        if ( orgEvent.detail )     { delta = orgEvent.detail * -1; }

        // New school wheel delta (wheel event)
        if ( orgEvent.deltaY ) {
            deltaY = orgEvent.deltaY * -1;
            delta  = deltaY;
        }
        if ( orgEvent.deltaX ) {
            deltaX = orgEvent.deltaX;
            delta  = deltaX * -1;
        }

        // Webkit
        if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY; }
        if ( orgEvent.wheelDeltaX !== undefined ) { deltaX = orgEvent.wheelDeltaX * -1; }

        // Look for lowest delta to normalize the delta values
        absDelta = Math.abs(delta);
        if ( !lowestDelta || absDelta < lowestDelta ) { lowestDelta = absDelta; }
        absDeltaXY = Math.max(Math.abs(deltaY), Math.abs(deltaX));
        if ( !lowestDeltaXY || absDeltaXY < lowestDeltaXY ) { lowestDeltaXY = absDeltaXY; }

        // Get a whole value for the deltas
        fn = delta > 0 ? 'floor' : 'ceil';
        delta  = Math[fn](delta / lowestDelta);
        deltaX = Math[fn](deltaX / lowestDeltaXY);
        deltaY = Math[fn](deltaY / lowestDeltaXY);

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

}));

/*!
 * artTemplate - Template Engine
 * https://github.com/aui/artTemplate
 * Released under the MIT, BSD, and GPL Licenses
 */
(function(global) {
    'use strict';
    /**
 * 模板引擎
 * 若第二个参数类型为 String 则执行 compile 方法, 否则执行 render 方法
 * @name    template
 * @param   {String}            模板ID
 * @param   {Object, String}    数据或者模板字符串
 * @return  {String, Function}  渲染好的HTML字符串或者渲染方法
 */
    var template = function(id, content) {
        return template[typeof content === 'string' ? 'compile': 'render'].apply(template, arguments);
    };
    template.version = '2.0.2';
    template.openTag = '<%'; // 设置逻辑语法开始标签
    template.closeTag = '%>'; // 设置逻辑语法结束标签
    template.isEscape = true; // HTML字符编码输出开关
    template.isCompress = false; // 剔除渲染后HTML多余的空白开关
    template.parser = null; // 自定义语法插件接口
    /**
 * 渲染模板
 * @name    template.render
 * @param   {String}    模板ID
 * @param   {Object}    数据
 * @return  {String}    渲染好的HTML字符串
 */
    template.render = function(id, data) {

        var cache = template.get(id) || _debug({
            id: id,
            name: 'Render Error',
            message: 'No Template'
        });

        return cache(data);
    };
 /**
 * 编译模板
 * 2012-6-6 @TooBug: define 方法名改为 compile，与 Node Express 保持一致
 * @name    template.compile
 * @param   {String}    模板ID (可选，用作缓存索引)
 * @param   {String}    模板字符串
 * @return  {Function}  渲染方法
 */
    template.compile = function(id, source) {
        var params = arguments;
        var isDebug = params[2];
        var anonymous = 'anonymous';
        if (typeof source !== 'string') {
            isDebug = params[1];
            source = params[0];
            id = anonymous;
        }
        try {
            var Render = _compile(id, source, isDebug);
        } catch(e) {
            e.id = id || source;
            e.name = 'Syntax Error';
            return _debug(e);
        }
        function render(data) {
            try {
                return new Render(data, id) + '';
            } catch(e) {
                if (!isDebug) {
                    return template.compile(id, source, true)(data);
                }
                return _debug(e)();
            }
        }
        render.prototype = Render.prototype;
        render.toString = function() {
            return Render.toString();
        };
        if (id !== anonymous) {
            _cache[id] = render;
        }
        return render;
    };
    var _cache = template.cache = {};
    // 辅助方法集合
    var _helpers = template.helpers = {
        $include: template.render,
        $string: function(value, type) {
            if (typeof value !== 'string') {
                type = typeof value;
                if (type === 'number') {
                    value += '';
                } else if (type === 'function') {
                    value = _helpers.$string(value());
                } else {
                    value = '';
                }
            }
            return value;
        },
        $escape: function(content) {
            var m = {
                "<": "&#60;",
                ">": "&#62;",
                '"': "&#34;",
                "'": "&#39;",
                "&": "&#38;"
            };
            return _helpers.$string(content).replace(/&(?![\w#]+;)|[<>"']/g,
            function(s) {
                return m[s];
            });
        },
        $each: function(data, callback) {
            var isArray = Array.isArray ||
            function(obj) {
                return ({}).toString.call(obj) === '[object Array]';
            };
            if (isArray(data)) {
                for (var i = 0,
                len = data.length; i < len; i++) {
                    callback.call(data, data[i], i, data);
                }
            } else {
                for (i in data) {
                    callback.call(data, data[i], i);
                }
            }
        }
    };
/**
 * 添加模板辅助方法
 * @name    template.helper
 * @param   {String}    名称
 * @param   {Function}  方法
 */
    template.helper = function(name, helper) {
        _helpers[name] = helper;
    };

    /**
 * 模板错误事件
 * @name    template.onerror
 * @event
 */
    template.onerror = function(e) {
        var message = 'Template Error\n\n';
        for (var name in e) {
            message += '<' + name + '>\n' + e[name] + '\n\n';
        }

        if (global.console) {
            console.error(message);
        }
    };
    // 获取模板缓存
    template.get = function(id) {
        var cache;
        if (_cache.hasOwnProperty(id)) {
            cache = _cache[id];
        } else if ('document' in global) {
            var elem = document.getElementById(id);

            if (elem) {
                var source = elem.value || elem.innerHTML;
                cache = template.compile(id, source.replace(/^\s*|\s*$/g, ''));
            }
        }
        return cache;
    };
    // 模板调试器
    var _debug = function(e) {
		//return template.onerror(e);这句是为了RenderData里用try{} cath(){}判断出错，以返回自定义错误;
        return template.onerror(e);
		//return '{Template Error}';为引擎默认返回值
        return function() {
            return '{Template Error}';
        };
    };
    // 模板编译器
    var _compile = (function() {
        // 数组迭代
        var forEach = _helpers.$each;
        // 静态分析模板变量
        var KEYWORDS =
        // 关键字
        'break,case,catch,continue,debugger,default,delete,do,else,false' + ',finally,for,function,if,in,instanceof,new,null,return,switch,this' + ',throw,true,try,typeof,var,void,while,with'

        // 保留字
        + ',abstract,boolean,byte,char,class,const,double,enum,export,extends' + ',final,float,goto,implements,import,int,interface,long,native' + ',package,private,protected,public,short,static,super,synchronized' + ',throws,transient,volatile'
        // ECMA 5 - use strict
        + ',arguments,let,yield'
        + ',undefined';
        var REMOVE_RE = /\/\*[\w\W]*?\*\/|\/\/[^\n]*\n|\/\/[^\n]*$|"(?:[^"\\]|\\[\w\W])*"|'(?:[^'\\]|\\[\w\W])*'|[\s\t\n]*\.[\s\t\n]*[$\w\.]+/g;
        var SPLIT_RE = /[^\w$]+/g;
        var KEYWORDS_RE = new RegExp(["\\b" + KEYWORDS.replace(/,/g, '\\b|\\b') + "\\b"].join('|'), 'g');
        var NUMBER_RE = /^\d[^,]*|,\d[^,]*/g;
        var BOUNDARY_RE = /^,+|,+$/g;
        var getVariable = function(code) {
            return code.replace(REMOVE_RE, '').replace(SPLIT_RE, ',').replace(KEYWORDS_RE, '').replace(NUMBER_RE, '').replace(BOUNDARY_RE, '').split(/^$|,+/);
        };
        return function(id, source, isDebug) {
            var openTag = template.openTag;
            var closeTag = template.closeTag;
            var parser = template.parser;
            var code = source;
            var tempCode = '';
            var line = 1;
            var uniq = {
                $data: 1,
                $id: 1,
                $helpers: 1,
                $out: 1,
                $line: 1
            };
            var prototype = {};
            var variables = "var $helpers=this," + (isDebug ? "$line=0,": "");
            var isNewEngine = ''.trim; // '__proto__' in {}
            var replaces = isNewEngine ? ["$out='';", "$out+=", ";", "$out"] : ["$out=[];", "$out.push(", ");", "$out.join('')"];
            var concat = isNewEngine ? "if(content!==undefined){$out+=content;return content;}": "$out.push(content);";
            var print = "function(content){" + concat + "}";
            var include = "function(id,data){" + "data=data||$data;" + "var content=$helpers.$include(id,data,$id);" + concat + "}";
            // html与逻辑语法分离
            forEach(code.split(openTag),
            function(code, i) {
                code = code.split(closeTag);
                var $0 = code[0];
                var $1 = code[1];
                // code: [html]
                if (code.length === 1) {
                    tempCode += html($0);
                    // code: [logic, html]
                } else {
                    tempCode += logic($0);
                    if ($1) {
                        tempCode += html($1);
                    }
                }
            });
            code = tempCode;
            // 调试语句
            if (isDebug) {
                code = "try{" + code + "}catch(e){" + "throw {" + "id:$id," + "name:'Render Error'," + "message:e.message," + "line:$line," + "source:" + stringify(source) + ".split(/\\n/)[$line-1].replace(/^[\\s\\t]+/,'')" + "};" + "}";
            }
            code = variables + replaces[0] + code + "return new String(" + replaces[3] + ");";
            try {
                var Render = new Function("$data", "$id", code);
                Render.prototype = prototype;
                return Render;
            } catch(e) {
                e.temp = "function anonymous($data,$id) {" + code + "}";
                throw e;
            }
            // 处理 HTML 语句
            function html(code) {
                // 记录行号
                line += code.split(/\n/).length - 1;
                // 压缩多余空白与注释
                if (template.isCompress) {
                    code = code.replace(/[\n\r\t\s]+/g, ' ').replace(/<!--.*?-->/g, '');
                }
                if (code) {
                    code = replaces[1] + stringify(code) + replaces[2] + "\n";
                }
                return code;
            }
            // 处理逻辑语句
            function logic(code) {
                var thisLine = line;
                if (parser) {
                    // 语法转换插件钩子
                    code = parser(code);
                } else if (isDebug) {
                    // 记录行号
                    code = code.replace(/\n/g,
                    function() {
                        line++;
                        return "$line=" + line + ";";
                    });
                }
                // 输出语句. 转义: <%=value%> 不转义:<%==value%>
                if (code.indexOf('=') === 0) {
                    var isEscape = code.indexOf('==') !== 0;
                    code = code.replace(/^=*|[\s;]*$/g, '');
                    if (isEscape && template.isEscape) {
                        // 转义处理，但排除辅助方法
                        var name = code.replace(/\s*\([^\)]+\)/, '');
                        if (!_helpers.hasOwnProperty(name) && !/^(include|print)$/.test(name)) {
                            code = "$escape(" + code + ")";
                        }
                    } else {
                        code = "$string(" + code + ")";
                    }
                    code = replaces[1] + code + replaces[2];
                }
                if (isDebug) {
                    code = "$line=" + thisLine + ";" + code;
                }
                getKey(code);
                return code + "\n";
            }
            // 提取模板中的变量名
            function getKey(code) {
                code = getVariable(code);
                // 分词
                forEach(code,
                function(name) {
                    // 除重
                    if (!uniq.hasOwnProperty(name)) {
                        setValue(name);
                        uniq[name] = true;
                    }
                });
            }

            // 声明模板变量
            // 赋值优先级:
            // 内置特权方法(include, print) > 私有模板辅助方法 > 数据 > 公用模板辅助方法
            function setValue(name) {
                var value;
                if (name === 'print') {
                    value = print;
                } else if (name === 'include') {
                    prototype["$include"] = _helpers['$include'];
                    value = include;
                } else {
                    value = "$data." + name;
                    if (_helpers.hasOwnProperty(name)) {
                        prototype[name] = _helpers[name];
                        if (name.indexOf('$') === 0) {
                            value = "$helpers." + name;
                        } else {
                            value = value + "===undefined?$helpers." + name + ":" + value;
                        }
                    }
                }
                variables += name + "=" + value + ",";
            };
            // 字符串转义
            function stringify(code) {
                return "'" + code
                // 单引号与反斜杠转义
                .replace(/('|\\)/g, '\\$1')
                // 换行符转义(windows + linux)
                .replace(/\r/g, '\\r').replace(/\n/g, '\\n') + "'";
            };

        };
    })();
    // RequireJS && SeaJS
    if (typeof define === 'function') {
        define(function() {
            return template;
        });
        // NodeJS
    } else if (typeof exports !== 'undefined') {
        module.exports = template;
    }
    global.template = template;
})(this);


//drag
/*! 
* jquery.event.drag - v 2.2
* Copyright (c) 2010 Three Dub Media - http://threedubmedia.com
* Open Source MIT License - http://threedubmedia.com/code/license
*/
// Created: 2008-06-04 
// Updated: 2012-05-21
// REQUIRES: jquery 1.7.x

(function ($) {

    // add the jquery instance method
    $.fn.drag = function (str, arg, opts) {
        // figure out the event type
        var type = typeof str == "string" ? str : "",
        // figure out the event handler...
	fn = $.isFunction(str) ? str : $.isFunction(arg) ? arg : null;
        // fix the event type
        if (type.indexOf("drag") !== 0)
            type = "drag" + type;
        // were options passed
        opts = (str == fn ? arg : opts) || {};
        // trigger or bind event handler
        return fn ? this.bind(type, opts, fn) : this.trigger(type);
    };

    // local refs (increase compression)
    var $event = $.event,
$special = $event.special,
    // configure the drag special event 
drag = $special.drag = {

    // these are the default settings
    defaults: {
        which: 1, // mouse button pressed to start drag sequence
        distance: 0, // distance dragged before dragstart
        not: ':input', // selector to suppress dragging on target elements
        handle: null, // selector to match handle target elements
        relative: false, // true to use "position", false to use "offset"
        drop: true, // false to suppress drop events, true or selector to allow
        click: false // false to suppress click events after dragend (no proxy)
    },

    // the key name for stored drag data
    datakey: "dragdata",

    // prevent bubbling for better performance
    noBubble: true,

    // count bound related events
    add: function (obj) {
        // read the interaction data
        var data = $.data(this, drag.datakey),
        // read any passed options 
		opts = obj.data || {};
        // count another realted event
        data.related += 1;
        // extend data options bound with this event
        // don't iterate "opts" in case it is a node 
        $.each(drag.defaults, function (key, def) {
            if (opts[key] !== undefined)
                data[key] = opts[key];
        });
    },

    // forget unbound related events
    remove: function () {
        $.data(this, drag.datakey).related -= 1;
    },

    // configure interaction, capture settings
    setup: function () {
        // check for related events
        if ($.data(this, drag.datakey))
            return;
        // initialize the drag data with copied defaults
        var data = $.extend({ related: 0 }, drag.defaults);
        // store the interaction data
        $.data(this, drag.datakey, data);
        // bind the mousedown event, which starts drag interactions
        $event.add(this, "touchstart mousedown", drag.init, data);
        // prevent image dragging in IE...
        if (this.attachEvent)
            this.attachEvent("ondragstart", drag.dontstart);
    },

    // destroy configured interaction
    teardown: function () {
        var data = $.data(this, drag.datakey) || {};
        // check for related events
        if (data.related)
            return;
        // remove the stored data
        $.removeData(this, drag.datakey);
        // remove the mousedown event
        $event.remove(this, "touchstart mousedown", drag.init);
        // enable text selection
        drag.textselect(true);
        // un-prevent image dragging in IE...
        if (this.detachEvent)
            this.detachEvent("ondragstart", drag.dontstart);
    },

    // initialize the interaction
    init: function (event) {
        // sorry, only one touch at a time
        if (drag.touched)
            return;
        // the drag/drop interaction data
        var dd = event.data, results;
        // check the which directive
        if (event.which != 0 && dd.which > 0 && event.which != dd.which)
            return;
        // check for suppressed selector
        if ($(event.target).is(dd.not))
            return;
        // check for handle selector
        if (dd.handle && !$(event.target).closest(dd.handle, event.currentTarget).length)
            return;

        drag.touched = event.type == 'touchstart' ? this : null;
        dd.propagates = 1;
        dd.mousedown = this;
        dd.interactions = [drag.interaction(this, dd)];
        dd.target = event.target;
        dd.pageX = event.pageX;
        dd.pageY = event.pageY;
        dd.dragging = null;
        // handle draginit event... 
        results = drag.hijack(event, "draginit", dd);
        // early cancel
        if (!dd.propagates)
            return;
        // flatten the result set
        results = drag.flatten(results);
        // insert new interaction elements
        if (results && results.length) {
            dd.interactions = [];
            $.each(results, function () {
                dd.interactions.push(drag.interaction(this, dd));
            });
        }
        // remember how many interactions are propagating
        dd.propagates = dd.interactions.length;
        // locate and init the drop targets
        if (dd.drop !== false && $special.drop)
            $special.drop.handler(event, dd);
        // disable text selection
        drag.textselect(false);
        // bind additional events...
        if (drag.touched)
            $event.add(drag.touched, "touchmove touchend", drag.handler, dd);
        else
            $event.add(document, "mousemove mouseup", drag.handler, dd);
        // helps prevent text selection or scrolling
        if (!drag.touched || dd.live)
            return false;
    },

    // returns an interaction object
    interaction: function (elem, dd) {
        var offset = $(elem)[dd.relative ? "position" : "offset"]() || { top: 0, left: 0 };
        return {
            drag: elem,
            callback: new drag.callback(),
            droppable: [],
            offset: offset
        };
    },

    // handle drag-releatd DOM events
    handler: function (event) {
        // read the data before hijacking anything
        var dd = event.data;
        // handle various events
        switch (event.type) {
            // mousemove, check distance, start dragging 
            case !dd.dragging && 'touchmove':
                event.preventDefault();
            case !dd.dragging && 'mousemove':
                //  drag tolerance, x² + y² = distance²
                if (Math.pow(event.pageX - dd.pageX, 2) + Math.pow(event.pageY - dd.pageY, 2) < Math.pow(dd.distance, 2))
                    break; // distance tolerance not reached
                event.target = dd.target; // force target from "mousedown" event (fix distance issue)
                drag.hijack(event, "dragstart", dd); // trigger "dragstart"
                if (dd.propagates) // "dragstart" not rejected
                    dd.dragging = true; // activate interaction
                // mousemove, dragging
            case 'touchmove':
                event.preventDefault();
            case 'mousemove':
                if (dd.dragging) {
                    // trigger "drag"		
                    drag.hijack(event, "drag", dd);
                    if (dd.propagates) {
                        // manage drop events
                        if (dd.drop !== false && $special.drop)
                            $special.drop.handler(event, dd); // "dropstart", "dropend"							
                        break; // "drag" not rejected, stop		
                    }
                    event.type = "mouseup"; // helps "drop" handler behave
                }
                // mouseup, stop dragging
            case 'touchend':
            case 'mouseup':
            default:
                if (drag.touched)
                    $event.remove(drag.touched, "touchmove touchend", drag.handler); // remove touch events
                else
                    $event.remove(document, "mousemove mouseup", drag.handler); // remove page events	
                if (dd.dragging) {
                    if (dd.drop !== false && $special.drop)
                        $special.drop.handler(event, dd); // "drop"
                    drag.hijack(event, "dragend", dd); // trigger "dragend"	
                }
                drag.textselect(true); // enable text selection
                // if suppressing click events...
                if (dd.click === false && dd.dragging)
                    $.data(dd.mousedown, "suppress.click", new Date().getTime() + 5);
                dd.dragging = drag.touched = false; // deactivate element	
                break;
        }
    },

    // re-use event object for custom events
    hijack: function (event, type, dd, x, elem) {
        // not configured
        if (!dd)
            return;
        // remember the original event and type
        var orig = { event: event.originalEvent, type: event.type },
        // is the event drag related or drog related?
		mode = type.indexOf("drop") ? "drag" : "drop",
        // iteration vars
		result, i = x || 0, ia, $elems, callback,
		len = !isNaN(x) ? x : dd.interactions.length;
        // modify the event type
        event.type = type;
        // remove the original event
        event.originalEvent = null;
        // initialize the results
        dd.results = [];
        // handle each interacted element
        do if (ia = dd.interactions[i]) {
            // validate the interaction
            if (type !== "dragend" && ia.cancelled)
                continue;
            // set the dragdrop properties on the event object
            callback = drag.properties(event, dd, ia);
            // prepare for more results
            ia.results = [];
            // handle each element
            $(elem || ia[mode] || dd.droppable).each(function (p, subject) {
                // identify drag or drop targets individually
                callback.target = subject;
                // force propagtion of the custom event
                event.isPropagationStopped = function () { return false; };
                // handle the event	
                result = subject ? $event.dispatch.call(subject, event, callback) : null;
                // stop the drag interaction for this element
                if (result === false) {
                    if (mode == "drag") {
                        ia.cancelled = true;
                        dd.propagates -= 1;
                    }
                    if (type == "drop") {
                        ia[mode][p] = null;
                    }
                }
                // assign any dropinit elements
                else if (type == "dropinit")
                    ia.droppable.push(drag.element(result) || subject);
                // accept a returned proxy element 
                if (type == "dragstart")
                    ia.proxy = $(drag.element(result) || ia.drag)[0];
                // remember this result	
                ia.results.push(result);
                // forget the event result, for recycling
                delete event.result;
                // break on cancelled handler
                if (type !== "dropinit")
                    return result;
            });
            // flatten the results	
            dd.results[i] = drag.flatten(ia.results);
            // accept a set of valid drop targets
            if (type == "dropinit")
                ia.droppable = drag.flatten(ia.droppable);
            // locate drop targets
            if (type == "dragstart" && !ia.cancelled)
                callback.update();
        }
        while (++i < len)
        // restore the original event & type
        event.type = orig.type;
        event.originalEvent = orig.event;
        // return all handler results
        return drag.flatten(dd.results);
    },

    // extend the callback object with drag/drop properties...
    properties: function (event, dd, ia) {
        var obj = ia.callback;
        // elements
        obj.drag = ia.drag;
        obj.proxy = ia.proxy || ia.drag;
        // starting mouse position
        obj.startX = dd.pageX;
        obj.startY = dd.pageY;
        // current distance dragged
        obj.deltaX = event.pageX - dd.pageX;
        obj.deltaY = event.pageY - dd.pageY;
        // original element position
        obj.originalX = ia.offset.left;
        obj.originalY = ia.offset.top;
        // adjusted element position
        obj.offsetX = obj.originalX + obj.deltaX;
        obj.offsetY = obj.originalY + obj.deltaY;
        // assign the drop targets information
        obj.drop = drag.flatten((ia.drop || []).slice());
        obj.available = drag.flatten((ia.droppable || []).slice());
        return obj;
    },

    // determine is the argument is an element or jquery instance
    element: function (arg) {
        if (arg && (arg.jquery || arg.nodeType == 1))
            return arg;
    },

    // flatten nested jquery objects and arrays into a single dimension array
    flatten: function (arr) {
        return $.map(arr, function (member) {
            return member && member.jquery ? $.makeArray(member) :
				member && member.length ? drag.flatten(member) : member;
        });
    },

    // toggles text selection attributes ON (true) or OFF (false)
    textselect: function (bool) {
        $(document)[bool ? "unbind" : "bind"]("selectstart", drag.dontstart)
			.css("MozUserSelect", bool ? "" : "none");
        // .attr("unselectable", bool ? "off" : "on" )
        document.unselectable = bool ? "off" : "on";
    },

    // suppress "selectstart" and "ondragstart" events
    dontstart: function () {
        return false;
    },

    // a callback instance contructor
    callback: function () { }

};

    // callback methods
    drag.callback.prototype = {
        update: function () {
            if ($special.drop && this.available.length)
                $.each(this.available, function (i) {
                    $special.drop.locate(this, i);
                });
        }
    };

    // patch $.event.$dispatch to allow suppressing clicks
    var $dispatch = $event.dispatch;
    $event.dispatch = function (event) {
        if ($.data(this, "suppress." + event.type) - new Date().getTime() > 0) {
            $.removeData(this, "suppress." + event.type);
            return;
        }
        return $dispatch.apply(this, arguments);
    };

    // event fix hooks for touch events...
    var touchHooks =
$event.fixHooks.touchstart =
$event.fixHooks.touchmove =
$event.fixHooks.touchend =
$event.fixHooks.touchcancel = {
    props: "clientX clientY pageX pageY screenX screenY".split(" "),
    filter: function (event, orig) {
        if (orig) {
            var touched = (orig.touches && orig.touches[0])
				|| (orig.changedTouches && orig.changedTouches[0])
				|| null;
            // iOS webkit: touchstart, touchmove, touchend
            if (touched)
                $.each(touchHooks.props, function (i, prop) {
                    event[prop] = touched[prop];
                });
        }
        return event;
    }
};

    // share the same special event configuration with related events...
    $special.draginit = $special.dragstart = $special.dragend = drag;

})(jQuery);

/*! 
 * jquery.event.drag.live - v 2.2
 * Copyright (c) 2010 Three Dub Media - http://threedubmedia.com
 * Open Source MIT License - http://threedubmedia.com/code/license
 */
// Created: 2010-06-07
// Updated: 2012-05-21
// REQUIRES: jquery 1.7.x, event.drag 2.2
(function( $ ){
	
// local refs (increase compression)
var $event = $.event,
// ref the special event config
drag = $event.special.drag,
// old drag event add method
origadd = drag.add,
// old drag event teradown method
origteardown = drag.teardown;

// allow events to bubble for delegation
drag.noBubble = false;

// the namespace for internal live events
drag.livekey = "livedrag";

// new drop event add method
drag.add = function( obj ){ 
	// call the old method
	origadd.apply( this, arguments );
	// read the data
	var data = $.data( this, drag.datakey );
	// bind the live "draginit" delegator
	if ( !data.live && obj.selector ){
		data.live = true;
		$event.add( this, "draginit."+ drag.livekey, drag.delegate );
	}
};

// new drop event teardown method
drag.teardown = function(){ 
	// call the old method
	origteardown.apply( this, arguments );
	// read the data
	var data = $.data( this, drag.datakey ) || {};
	// bind the live "draginit" delegator
	if ( data.live ){
		// remove the "live" delegation
		$event.remove( this, "draginit."+ drag.livekey, drag.delegate );
		data.live = false;
	}
};

// identify potential delegate elements
drag.delegate = function( event ){
	// local refs
	var elems = [], target, 
	// element event structure
	events = $.data( this, "events" ) || {};
	// query live events
	$.each( events || [], function( key, arr ){
		// no event type matches
		if ( key.indexOf("drag") !== 0 )
			return;
		$.each( arr || [], function( i, obj ){
			// locate the element to delegate
			target = $( event.target ).closest( obj.selector, event.currentTarget )[0];
			// no element found
			if ( !target ) 
				return;
			// add an event handler
			$event.add( target, obj.origType+'.'+drag.livekey, obj.origHandler || obj.handler, obj.data );
			// remember new elements
			if ( $.inArray( target, elems ) < 0 )
				elems.push( target );		
		});
	});
	// if there are no elements, break
	if ( !elems.length ) 
		return false;
	// return the matched results, and clenup when complete		
	return $( elems ).bind("dragend."+ drag.livekey, function(){
		$event.remove( this, "."+ drag.livekey ); // cleanup delegation
	});
};
	
})( jQuery );


//drop
/*! 
 * jquery.event.drop - v 2.2
 * Copyright (c) 2010 Three Dub Media - http://threedubmedia.com
 * Open Source MIT License - http://threedubmedia.com/code/license
 */
// Created: 2008-06-04 
// Updated: 2012-05-21
// REQUIRES: jquery 1.7.x, event.drag 2.2
(function($){ // secure $ jQuery alias

// Events: drop, dropstart, dropend

// add the jquery instance method
$.fn.drop = function( str, arg, opts ){
	// figure out the event type
	var type = typeof str == "string" ? str : "",
	// figure out the event handler...
	fn = $.isFunction( str ) ? str : $.isFunction( arg ) ? arg : null;
	// fix the event type
	if ( type.indexOf("drop") !== 0 ) 
		type = "drop"+ type;
	// were options passed
	opts = ( str == fn ? arg : opts ) || {};
	// trigger or bind event handler
	return fn ? this.bind( type, opts, fn ) : this.trigger( type );
};

// DROP MANAGEMENT UTILITY
// returns filtered drop target elements, caches their positions
$.drop = function( opts ){ 
	opts = opts || {};
	// safely set new options...
	drop.multi = opts.multi === true ? Infinity : 
		opts.multi === false ? 1 : !isNaN( opts.multi ) ? opts.multi : drop.multi;
	drop.delay = opts.delay || drop.delay;
	drop.tolerance = $.isFunction( opts.tolerance ) ? opts.tolerance : 
		opts.tolerance === null ? null : drop.tolerance;
	drop.mode = opts.mode || drop.mode || 'intersect';
};

// local refs (increase compression)
var $event = $.event, 
$special = $event.special,
// configure the drop special event
drop = $.event.special.drop = {

	// these are the default settings
	multi: 1, // allow multiple drop winners per dragged element
	delay: 20, // async timeout delay
	mode: 'overlap', // drop tolerance mode
		
	// internal cache
	targets: [], 
	
	// the key name for stored drop data
	datakey: "dropdata",
		
	// prevent bubbling for better performance
	noBubble: true,
	
	// count bound related events
	add: function( obj ){ 
		// read the interaction data
		var data = $.data( this, drop.datakey );
		// count another realted event
		data.related += 1;
	},
	
	// forget unbound related events
	remove: function(){
		$.data( this, drop.datakey ).related -= 1;
	},
	
	// configure the interactions
	setup: function(){
		// check for related events
		if ( $.data( this, drop.datakey ) ) 
			return;
		// initialize the drop element data
		var data = { 
			related: 0,
			active: [],
			anyactive: 0,
			winner: 0,
			location: {}
		};
		// store the drop data on the element
		$.data( this, drop.datakey, data );
		// store the drop target in internal cache
		drop.targets.push( this );
	},
	
	// destroy the configure interaction	
	teardown: function(){ 
		var data = $.data( this, drop.datakey ) || {};
		// check for related events
		if ( data.related ) 
			return;
		// remove the stored data
		$.removeData( this, drop.datakey );
		// reference the targeted element
		var element = this;
		// remove from the internal cache
		drop.targets = $.grep( drop.targets, function( target ){ 
			return ( target !== element ); 
		});
	},
	
	// shared event handler
	handler: function( event, dd ){ 
		// local vars
		var results, $targets;
		// make sure the right data is available
		if ( !dd ) 
			return;
		// handle various events
		switch ( event.type ){
			// draginit, from $.event.special.drag
			case 'mousedown': // DROPINIT >>
			case 'touchstart': // DROPINIT >>
				// collect and assign the drop targets
				$targets =  $( drop.targets );
				if ( typeof dd.drop == "string" )
					$targets = $targets.filter( dd.drop );
				// reset drop data winner properties
				$targets.each(function(){
					var data = $.data( this, drop.datakey );
					data.active = [];
					data.anyactive = 0;
					data.winner = 0;
				});
				// set available target elements
				dd.droppable = $targets;
				// activate drop targets for the initial element being dragged
				$special.drag.hijack( event, "dropinit", dd ); 
				break;
			// drag, from $.event.special.drag
			case 'mousemove': // TOLERATE >>
			case 'touchmove': // TOLERATE >>
				drop.event = event; // store the mousemove event
				if ( !drop.timer )
					// monitor drop targets
					drop.tolerate( dd ); 
				break;
			// dragend, from $.event.special.drag
			case 'mouseup': // DROP >> DROPEND >>
			case 'touchend': // DROP >> DROPEND >>
				drop.timer = clearTimeout( drop.timer ); // delete timer	
				if ( dd.propagates ){
					$special.drag.hijack( event, "drop", dd ); 
					$special.drag.hijack( event, "dropend", dd ); 
				}
				break;
				
		}
	},
		
	// returns the location positions of an element
	locate: function( elem, index ){ 
		var data = $.data( elem, drop.datakey ),
		$elem = $( elem ), 
		posi = $elem.offset() || {}, 
		height = $elem.outerHeight(), 
		width = $elem.outerWidth(),
		location = { 
			elem: elem, 
			width: width, 
			height: height,
			top: posi.top, 
			left: posi.left, 
			right: posi.left + width, 
			bottom: posi.top + height
		};
		// drag elements might not have dropdata
		if ( data ){
			data.location = location;
			data.index = index;
			data.elem = elem;
		}
		return location;
	},
	
	// test the location positions of an element against another OR an X,Y coord
	contains: function( target, test ){ // target { location } contains test [x,y] or { location }
		return ( ( test[0] || test.left ) >= target.left && ( test[0] || test.right ) <= target.right
			&& ( test[1] || test.top ) >= target.top && ( test[1] || test.bottom ) <= target.bottom ); 
	},
	
	// stored tolerance modes
	modes: { // fn scope: "$.event.special.drop" object 
		// target with mouse wins, else target with most overlap wins
		'intersect': function( event, proxy, target ){
			return this.contains( target, [ event.pageX, event.pageY ] ) ? // check cursor
				1e9 : this.modes.overlap.apply( this, arguments ); // check overlap
		},
		// target with most overlap wins	
		'overlap': function( event, proxy, target ){
			// calculate the area of overlap...
			return Math.max( 0, Math.min( target.bottom, proxy.bottom ) - Math.max( target.top, proxy.top ) )
				* Math.max( 0, Math.min( target.right, proxy.right ) - Math.max( target.left, proxy.left ) );
		},
		// proxy is completely contained within target bounds	
		'fit': function( event, proxy, target ){
			return this.contains( target, proxy ) ? 1 : 0;
		},
		// center of the proxy is contained within target bounds	
		'middle': function( event, proxy, target ){
			return this.contains( target, [ proxy.left + proxy.width * .5, proxy.top + proxy.height * .5 ] ) ? 1 : 0;
		}
	},	
	
	// sort drop target cache by by winner (dsc), then index (asc)
	sort: function( a, b ){
		return ( b.winner - a.winner ) || ( a.index - b.index );
	},
		
	// async, recursive tolerance execution
	tolerate: function( dd ){		
		// declare local refs
		var i, drp, drg, data, arr, len, elem,
		// interaction iteration variables
		x = 0, ia, end = dd.interactions.length,
		// determine the mouse coords
		xy = [ drop.event.pageX, drop.event.pageY ],
		// custom or stored tolerance fn
		tolerance = drop.tolerance || drop.modes[ drop.mode ];
		// go through each passed interaction...
		do if ( ia = dd.interactions[x] ){
			// check valid interaction
			if ( !ia )
				return; 
			// initialize or clear the drop data
			ia.drop = [];
			// holds the drop elements
			arr = []; 
			len = ia.droppable.length;
			// determine the proxy location, if needed
			if ( tolerance )
				drg = drop.locate( ia.proxy ); 
			// reset the loop
			i = 0;
			// loop each stored drop target
			do if ( elem = ia.droppable[i] ){ 
				data = $.data( elem, drop.datakey );
				drp = data.location;
				if ( !drp ) continue;
				// find a winner: tolerance function is defined, call it
				data.winner = tolerance ? tolerance.call( drop, drop.event, drg, drp ) 
					// mouse position is always the fallback
					: drop.contains( drp, xy ) ? 1 : 0; 
				arr.push( data );	
			} while ( ++i < len ); // loop 
			// sort the drop targets
			arr.sort( drop.sort );			
			// reset the loop
			i = 0;
			// loop through all of the targets again
			do if ( data = arr[ i ] ){
				// winners...
				if ( data.winner && ia.drop.length < drop.multi ){
					// new winner... dropstart
					if ( !data.active[x] && !data.anyactive ){
						// check to make sure that this is not prevented
						if ( $special.drag.hijack( drop.event, "dropstart", dd, x, data.elem )[0] !== false ){ 	
							data.active[x] = 1;
							data.anyactive += 1;
						}
						// if false, it is not a winner
						else
							data.winner = 0;
					}
					// if it is still a winner
					if ( data.winner )
						ia.drop.push( data.elem );
				}
				// losers... 
				else if ( data.active[x] && data.anyactive == 1 ){
					// former winner... dropend
					$special.drag.hijack( drop.event, "dropend", dd, x, data.elem ); 
					data.active[x] = 0;
					data.anyactive -= 1;
				}
			} while ( ++i < len ); // loop 		
		} while ( ++x < end ) // loop
		// check if the mouse is still moving or is idle
		if ( drop.last && xy[0] == drop.last.pageX && xy[1] == drop.last.pageY ) 
			delete drop.timer; // idle, don't recurse
		else  // recurse
			drop.timer = setTimeout(function(){ 
				drop.tolerate( dd ); 
			}, drop.delay );
		// remember event, to compare idleness
		drop.last = drop.event; 
	}
	
};

// share the same special event configuration with related events...
$special.dropinit = $special.dropstart = $special.dropend = drop;

})(jQuery); // confine scope	

/*! 
 * jquery.event.drop.live - v 2.2
 * Copyright (c) 2010 Three Dub Media - http://threedubmedia.com
 * Open Source MIT License - http://threedubmedia.com/code/license
 */
// Created: 2010-06-07
// Updated: 2012-05-21
// REQUIRES: jquery 1.7.x, event.drag 2.2, event.drop 2.2

(function($){ // secure $ jQuery alias

// local refs (increase compression)
var $event = $.event,
// ref the drop special event config
drop = $event.special.drop,
// old drop event add method
origadd = drop.add,
// old drop event teradown method
origteardown = drop.teardown;

// allow events to bubble for delegation
drop.noBubble = false;

// the namespace for internal live events
drop.livekey = "livedrop";

// new drop event add method
drop.add = function( obj ){ 
	// call the old method
	origadd.apply( this, arguments );
	// read the data
	var data = $.data( this, drop.datakey );
	// bind the live "dropinit" delegator
	if ( !data.live && obj.selector ){
		data.live = true;
		$event.add( this, "dropinit."+ drop.livekey, drop.delegate );
	}
};

// new drop event teardown method
drop.teardown = function(){ 
	// call the old method
	origteardown.apply( this, arguments );
	// read the data
	var data = $.data( this, drop.datakey ) || {};
	// remove the live "dropinit" delegator
	if ( data.live ){
		// remove the "live" delegation
		$event.remove( this, "dropinit", drop.delegate );
		data.live = false;
	}
};

// identify potential delegate elements
drop.delegate = function( event, dd ){
	// local refs
	var elems = [], $targets, 
	// element event structure
	events = $.data( this, "events" ) || {};
	// query live events
	$.each( events || [], function( key, arr ){
		// no event type matches
		if ( key.indexOf("drop") !== 0 )
			return;
		$.each( arr, function( i, obj ){
			// locate the elements to delegate
			$targets = $( event.currentTarget ).find( obj.selector );
			// no element found
			if ( !$targets.length ) 
				return;
			// take each target...
			$targets.each(function(){
				// add an event handler
				$event.add( this, obj.origType +'.'+ drop.livekey, obj.origHandler || obj.handler, obj.data );
				// remember new elements
				if ( $.inArray( this, elems ) < 0 )
					elems.push( this );	
			});	
		});
	});
	// may not exist when artifically triggering dropinit event
	if ( dd )
		// clean-up after the interaction ends
		$event.add( dd.drag, "dragend."+drop.livekey, function(){
			$.each( elems.concat( this ), function(){
				$event.remove( this, '.'+ drop.livekey );							
			});
		});
	//drop.delegates.push( elems );
	return elems.length ? $( elems ) : false;
};

})( jQuery ); // confine scope	


/*!
 * 修复ajax项目，前进，后退按钮
 * 目前某些浏览器对pushState、popstate的支持还不是完美,暂时实现对浏览器后退前进按钮屏蔽(即单击后退前进按钮无效)
 * 但是：由login初次登录index页面时,后退按钮无法屏蔽,即初次登录index时，单击后退按钮会退回到登录页面
 */
(function ($) {
    $.history = {
        set: function (data) {
            if (!data) { return; }
            //创建历史记录
            data = typeof data === "string" ? data : jQuery.toJSON(data);
            if($.trim(data)==this.data) { return; }
            if (this.support) {
                history.pushState(data, null, null);
            } else {
                this.state = 1;
                var doc = this.iframe.contentWindow.document;
                doc.open();
                doc.write('<html><body><div id="state">' + data + '</div></body></html>');
                doc.close();
            }
            this.data = $.trim(data);
        },
        init: function (data) {
            var self = this;
            //绑定触发浏览器后退前进按钮事件
            if (this.support) {
                $(window).off("popstate").on("popstate", function (e) {
                    var state = history.state || e.originalEvent.state;
                    if(state){
		    //alert("禁止使用浏览器后退键！")
		    window.history.forward(1);
		    //Control.GetTemplate(jQuery.evalJSON(state));
                    }
                    self.clear();
                });
            } else {
                this.iframe = document.createElement("iframe");
                this.iframe.src = "about:blank";
                this.iframe.keep = "true";
                this.iframe.style.display = "none";
                $("body").append(this.iframe);
                $(this.iframe).off("load").on("load", function () {
                    var state = $(this).contents().find("#state");
                    if (state[0] && self.state == 0) {
		    //alert("禁止使用浏览器前进键！")
		    self.clear();
		    window.history.forward(1);
		    //Control.GetTemplate(jQuery.evalJSON(stateText));
                    }
                    self.state = 0;
                });
            };
            if(data){
                this.set(typeof data === "string" ? data : jQuery.toJSON(data))
            }
        },
        //清除弹出层等DOM
        clear:function(){
            var doms = "#SetProgress,#PromptCover,#PromptPop,#ui-datepicker-div";
            $(doms).remove();
        },
        data: "",
        iframe: null,
        support: !!(window.history && history.pushState)
    };
})(jQuery);
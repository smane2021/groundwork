/*!
 * Author: Jerman.Xie | xieguishi@126.com
 * Date: 2013.09
 *
 * login
 */
var Login = {
    ControlMode: null,
    Encode64: function (input) {
        input = escape(input);
        var keyStr = "ABCDEFGHIJKLMNOP" + "QRSTUVWXYZabcdef" + "ghijklmnopqrstuv" + "wxyz0123456789+/" + "=",
            output = "",
            chr1, chr2, chr3 = "",
            enc1, enc2, enc3, enc4 = "",
            i = 0,
            nMod = (input.length) % 3;
        do {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = (((chr1 << 4) | (chr2 >> 4)) & 0x3f);
            enc3 = (((chr2 << 2) | (chr3 >> 6)) & 0x3f);
            enc4 = chr3 & 0x3f;
            output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
            chr1 = "";
            chr2 = "";
            chr3 = "";
            enc1 = "";
            enc2 = "";
            enc3 = "";
            enc4 = "";
        } while (i < input.length);
        if (nMod == 1) {
            chr1 = input.charCodeAt(i++);
            enc1 = ((chr1 & 192) >> 2);
            enc2 = ((chr1 & 3) << 4);
            enc3 = "=";
            enc4 = "=";
            output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
        };
        if (nMod == 2) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            enc1 = ((chr1 & 192) >> 2);
            enc2 = ((chr1 & 3) << 4) | ((chr2 & 0xf0) >> 4);
            enc3 = ((chr2 & 15) << 2);
            enc4 = "=";
            output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
        }
        return output;
    },
    LoginSubmit: function () {
        var that = this;
        var form = $("#loginForm"), name = $("#user_name"), pass = $("#user_password");
        var error = $("div.loginError");
        if (!name.val()) {
            error.html(this.Errors[3]).show();
            name.addClass("error");
            pass.removeClass("error");
            name.focus();
            return false;
        } else if (name.val().length > 32) {
            error.html(this.Errors[3]).show();
            name.addClass("error");
            pass.removeClass("error");
            name.focus();
            return false;
        } else if (!pass.val()) {
            error.html(this.Errors[2]).show();
            if (!name.hasClass("error")) {
                pass.addClass("error");
            }
            pass.focus();
            return false;
        }
        this.LoginError = false;
        if (that.ControlMode != null && that.ControlMode == 2) {
            alert(Login.HTML["control_mode"]);
            return;
        }
        $("#language_type").val("0");
        var btn_submit = $("a.form-submit"),
            form = $("#loginForm"),
            name_value = this.Encode64(name.val()),
            pass_value = this.Encode64(pass.val()),
            btn_submit_text = btn_submit.text(),
            XHR = $.ajax({
                type: form.attr("method"),
                url: form.attr("action") + "?_=" + new Date().getTime(),
                data: "user_name=" + name_value + "&user_password=" + pass_value,
                timeout:15000,
                beforeSend: function () {
                    //发送请求时,disabled登录按钮
                    btn_submit.addClass("disabled").html(Login.HTML['loging']);
                },
                success: function (data, textStatus, jqXHR) {
                    var data = jQuery.evalJSON(data);
                    if (data.status == 1) {
                        /*
                        var cookietime = new Date();
                        cookietime.setTime(new Date().getTime() + (2 * 60 * 60 * 10000)); //coockie保存2小时 
                        */
                        var cookies = data.cookie.split(",");
                        for (var j = 0, jlen = cookies.length; j < jlen; j++) {
                            var cookieItem = cookies[j].split("=");
                            jQuery.cookie(cookieItem[0], cookieItem[1], { path: "/" });
                        };
                        jQuery.cookie("user_name", name_value, { path: "/" });
                        jQuery.cookie("user_password", pass_value, { path: "/" });
                        //location.href = "/app/www_user/html/eng/index.html";
                        //根据来源网址进行跳转
                        var location_target = window.location.search.replace("?", "").split("&");
                        var location_obj = {};
                        for (var i = 0, ilen = location_target.length; i < ilen; i++) {
                            var location_tmp = location_target[i].split("=");
                            location_obj[location_tmp[0]] = decodeURIComponent(location_tmp[1]);
                        };
                        if (location_obj['from'] && location_obj != "") {
                            location.href = location_obj['from'];
                        } else {
							//根据不同的语言，在index.html添加后缀
							var loc_lang_type = jQuery.cookie("loc_lang_type");
							var loc_lang = "";
							switch(Number(loc_lang_type)){
								case 0:
									loc_lang ="de";
									break;
								case 1:
									loc_lang = "es";
									break;
								case 2:
									loc_lang = "fr";
									break;
								case 3:
									loc_lang = "it";
									break;
								case 4:
									loc_lang = "ru";
									break;
								case 5:
									loc_lang = "tr";
									break;
								case 6:
									loc_lang = "tw";
									break;
								case 7:
									loc_lang = "zh";
									break;
							}
							var loc_url = "index.html"+(loc_lang!="" ? "?hl="+loc_lang : "");
                            location.href = loc_url;
                        }
                    } else {
                        error.html(that.Errors[data.status]).show();
                        that.ClearCookie();
                        btn_submit.removeClass("disabled").html(Login.HTML["login"]);
                    }
                },
                error: function (data, textStatus) {
                    error.html(that.Errors[4]).show();
                    that.ClearCookie();
                    btn_submit.removeClass("disabled").html(Login.HTML["login"]);
                }
            }).done(function (jqXHR, textStatus) {
                jqXHR = null;
                XHR = null;
            })
    },
    Login: function () {
        var that = this,
            lang = $("#selectLang");
        lang.on("change", function () {
            var v = $(this).val();
            that.getData(v)
        });
        $(".form-submit").on("click", function () {

            if ($(this).hasClass("disabled")) {
                return;
            }
            that.LoginSubmit();
            return false;
        });
        this.URL = $("#loginForm").attr("action");
    },
    ClearCookie: function () {
        var cookies = ["clientIP", "hash_key", "nAuthority", "name", "sessionId", "user_name", "user_password","restart"];
        for (var i = 0, ilen = cookies.length; i < ilen; i++) {
            jQuery.removeCookie(cookies[i], "");
        };
    },
    Initialization: function () {
        var that = this;
        //按下Enter键时,提交登录
        $(document).on("keyup", function (event) {
            if (event.keyCode == 13) {
                that.LoginSubmit();
            }
        });
        var tips = $("#ModuleLoading"), tipsContent = $("#LoadingContent"), login = $("#login");
        //判断cookie是否开启,写入一个cookie,再读取
        jQuery.cookie("cookieisok", "1", { path: "/" });
        if (jQuery.cookie("cookieisok") !== "1") {
            tipsContent.html(Login.HTML["nocookie"]);
            return;
        } else {
            jQuery.removeCookie("cookieisok", "", { path: "/" });
        };
        var template = $("#tmp_login").html();
        GetLogin();
        function GetLogin() {
            var XHR = $.ajax({
                url: "/var/datas/data.login.html?_=" + new Date().getTime(),
                beforeSend: function () {
                    tipsContent.html(Login.HTML['loading']);
                },
                success: function (data, textStatus, jqXHR) {
                    //判断数据是否完整
                    var re = new RegExp("\"%__data_end__%\"", 'g');
                    if (re.test(data)) {//匹配成功
                        data = data.replace("\"%__data_end__%\"", "");
                    } else {
                        setTimeout(function () { tipsContent.html(Login.HTML['nodata']) }, 500);
                        return;
                    }
                    //读取URL lang参数,写入语言到cookie
                    var url = window.location.search.replace("?", "").split("&");
                    var urlObj = {};
                    for (var i = 0, ilen = url.length; i < ilen; i++) {
                        var urlTemp = url[i].split("=");
                        urlObj[urlTemp[0]] = urlTemp[1];
                    }
                    if (urlObj["lang"] && urlObj["lang"] != 0 || /\/loc\//gi.test(window.location.href)) {
                        jQuery.cookie("language_type", 1, { path: "/" });
                    } else {
                        jQuery.cookie("language_type", 0, { path: "/" });
                    };
                    var data = jQuery.evalJSON(data);
                    that.ControlMode = data.control_mode;
                    //这句不要更新到共享，只是为了本地显示没有替换的数据
                    //template = template.replace(/\/\*\[ID_/g, "").replace(/\]\*\//g, "");
                    doTtmpl = doT.template(template);
                    login.html(doTtmpl(data)).show();
                    tips.hide();
                    that.Login();
                    $("#user_name").focus();
                    $("form input").on("keydown", function () {
                        $(this).removeClass("error");
                        $("div.loginError").fadeOut();
                    });
                    jQuery.cookie("loc_lang_type", data["loc_lang_type"], { path: "/" });
                },
                error: function (data, textStatus) {
                    $("#TipsContent").html(Login.HTML["nologindata"]);
                    jQuery.cookie("language_type", 0, { path: "/" });
                    //如果ACU在重启或在loading状态,每隔3秒请求一次数据
//                    setTimeout(function () {
//                        GetLogin();
//                    }, 3000);
                    return;
                }
            }).always(function (jqXHR, textStatus, errorThrown) {
                jqXHR = null;
                textStatus = null;
                errorThrown = null;
                XHR = null;
                //初始化时,检查是否为登录状态，是在登录状态的话，直接跳转到index.html,否则清除cookie（cookie初始化）
                that.ClearCookie();
            })
        };
    }
}
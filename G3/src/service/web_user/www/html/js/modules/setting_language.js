﻿Pages.setting_language = function (data) {
    var that = this;
	var lang = Language["setting_language"];
    var SetLanguageTexts = lang["text"];
    var SetLanguageErrors = lang["errors"];
    var args = data.args, data = data.data;
    var sessionId = "&sessionId=" + args["sessionId"];
    var language_type = "&language_type=" + args["language_type"];
    Pages.BindSelect();
    $("#SetLanguage a").on("click", function () {
        var value = $(this).attr("rel");
        if (value == -1) {
            return false;
        };
        if (Number(value) == Number(jQuery.cookie("loc_lang_type"))) {
            alert(SetLanguageErrors[value]);
            return;
        }
        var form = $(this).closest("form");
        if (confirm(SetLanguageTexts[value])) {
            that.SetProcess(Language.Html['011']);
            var XHR = $.ajax({
                url: form.attr("action") + "?_=" + new Date().getTime(),
                data: "control_value=" + value + "&equip_ID=1&signal_type=-1&signal_id=-1&control_type=14" + sessionId,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };
                    if (data.status == 1) {
                        that.DoRestart();
                    } else if (data.status == 98) {
                        that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
                        return;
                    } else {
                        that.SetProcessDone(OtherErrors[data.status]);
                    }
                },
                error: function (data, textStatus) {
                    that.DoRestart();
                }
            }).done(function (jqXHR, textStatus) {
                jqXHR = null;
                XHR = null;
            });
        }
        return false;
    });
        if(jQuery.cookie("systeminfo")!=1){
            $(".table-container").css({width:'1163px'});
            $(".table-title").css({width:'1143px'});
	        $(".table-container .table").css({width:"1143"});
        }else{
            $(".table-container").css({width:'917px'});
            $(".table-title,.table-container .table").css({width:'897px'});
        }
};
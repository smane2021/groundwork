﻿Pages.setting_alarm_equipment = function (data) {
    var that = this;
	var equipments = $("#AlarmEquipments");
	equipments.next().find("a").off().on("click", function () {
		Configs.HighLight = null;
		var SignalId = $(this).attr("rel");
		if (SignalId == "") { return false; };
		var parameter = data.args.parameter.split("&");
		parameter[0] = "equip_ID=" + SignalId;
		data.args.parameter = parameter.join("&");
		var SetAlarmList = $("#SetAlarmContent");
		SetAlarmList.html("");
		Control.PromptEvent.Loading({ target: SetAlarmList,marginTop:120 });
		data.args["selected"] = $(this).text();
		data.args["equipmentId"] = SignalId;
		that.SetAlarmScroll = 0;
		Control.GetData(data.args);
	});
	data.args['clickfast'] = false;
	data.args['template'] = "tmp.setting_alarm_content.html";
	data.args['modules'] = "SetAlarmContent";
	data.args['idata'] = "mainBody";
	data.args['fn'] = "false";
	data.args['render'] = "artTemplate";
	data.args['onlyTemplate'] = false;
	Control.GetTemplate(data.args,true);
	Configs.Data.polling = false;
	Pages.BindSelect();
}
Pages.SetAlarmContent = function (data) {
	var that = this;
	var equipments = $("#AlarmEquipments");
	$("div.table-title>span").html(equipments.text());
	equipments.attr("disabled", false);
	var settemplates = $("#SetAlarm").attr("template");
	$("#SetAlarmList>div.table-body").on("click", "a.btn_set", function () {
		var post = $(this).attr("post");
		var parent = $(this).parents("ul");
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
		Control.PromptEvent.Pop({ cover: true, title: parent.find("li.t-2").text(), fn: that.SetLevelOrRelay, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args }, html: "<div id='SetAlarmLevelRelay' class='Poplevelrelay'></div>", Class: "Pop-set-alarm" });
	}).on("click", "ul,i,a", function () {
		var ul = $(this).closest("ul");
		Configs.HighLight = ul.attr("id");
		ul.siblings().removeClass("select").end().addClass("select");
	});
	$("#" + Configs.HighLight).addClass("select");
	Configs.Data.polling = false;
	setTimeout(function () {
		if (document.getElementById("SetAlarmContent")) {
			$("#mainBody").scrollTop(that.SetAlarmScroll);
		}
	}, 10);
	if(jQuery.cookie("systeminfo")!=1){
        $(".table li").css({"padding-right":"70px"});
        $(".table-container,.alarm-summary").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table li").animate({"padding-right":"0px"});
        $(".table-container,.alarm-summary").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
	return false;
};
Pages.system_inventory = function(data){
	var that = this;
	var mainBody = $("#mainBody");
	var mainBodyScroll = null;
	mainBody.off("scroll.h").on("scroll.h",function(){
		//为历史记录固定表头
		var scrollTop = $(this).scrollTop();
		var theadID ="thead-fixed";
		if(scrollTop==0){
			$("#"+theadID).hide();  
		}
		if(mainBodyScroll){ clearTimeout(mainBodyScroll); }
		mainBodyScroll = setTimeout(function(){
			that.FixThead("InventoryTable",theadID,scrollTop);
			$("#thead-fixed table").eq(0).css({"margin":"0"});
            if (navigator.userAgent.indexOf("Firefox") > -1||(!!window.ActiveXObject || "ActiveXObject" in window)){
		        if(jQuery.cookie("systeminfo")!=1){
                    $("#thead-fixed,#thead-fixed table").css({width:'1136px'});
                }else{
                    $("#thead-fixed,#thead-fixed table").css({width:'890px'});
                }
		    }else{
		        if(jQuery.cookie("systeminfo")!=1){
                    $("#thead-fixed,#thead-fixed table").css({width:'1143px'});
                }else{
                    $("#thead-fixed,#thead-fixed table").css({width:'897px'});
                }
		    }
		},100);
	});
//	setTimeout('if(!($(".table-container").height()>$("#mainBody").height())){$(".table-container").css({"height":$("#mainBody").height()})}',10);
	$(document).off("scroll.inventory").on("scroll.inventory",function(){
		//body滚动时,thead不重新定位
		Pages.FixThead("InventoryTable","thead-fixed",0);
	})
    if(jQuery.cookie("systeminfo")!=1){
        $("table th").not($('table th').eq(0)).css({"padding-right":"70px"});
        $("table td").not($('table td').eq(0)).css({"padding-right":"70px"});
        $(".table-container,.extbackground").css({width:'1163px'});
        $(".table-title,.table-container table").css({width:'1143px'});
    }else{
        $("table th").not($('table th').eq(0)).css({"padding-right":"30px"});
        $("table td").not($('table td').eq(0)).css({"padding-right":"30px"});
        $(".table-container,.extbackground").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
}
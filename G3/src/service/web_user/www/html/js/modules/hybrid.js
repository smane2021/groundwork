﻿Pages.hybrid = function (data) {
    /*绘制Hybrid*/
    var Hybrid = "hybrid",
		HybridHandle = $("#hybridHandle");
    var hybridLines = HybridHandle.find("input[type='checkbox']");
    var hybridWeek = HybridHandle.find("[name='hybrid-week']");
    hybridLines.off().on("click", function () {
        var name = $(this).val();
        var status = $(this).prop('checked');
        if (!status) {
            Configs.Hybrid.lines[name] = false;
            delete Configs.Hybrid.data.value[name];
        } else {
            Configs.Hybrid.lines[name] = true;
            Configs.Hybrid.data.value[name] = [];
            $.extend(Configs.Hybrid.data.value[name], data.data.value[name]);
        };
        Chart.drawHybrid(Hybrid, Configs.Hybrid.data);
    });
    hybridWeek.off().on("click", function () {
        Configs.week = Number($(this).attr("value"));
        Configs.Hybrid.data = $.extend(true, {}, Configs.Hybrid.data, {
            "week": Configs.week
        });
        Chart.drawHybrid(Hybrid, Configs.Hybrid.data);
    });
    if (Control.isPolling) {
        Configs.Hybrid.data = $.extend(true, {}, Configs.Hybrid.data, data.data);
        /*初始化线条显示*/
        for (var i = 0, ilen = hybridLines.length; i < ilen; i++) {
            var name = $(hybridLines).eq(i).val();
            if (typeof Configs.Hybrid.lines[name] != "undefined") {
                if (!Configs.Hybrid.lines[name]) {
                    $(hybridLines).eq(i).prop('checked', Configs.Hybrid.lines[name]);
                    delete Configs.Hybrid.data.value[name];
                }
            }
        };
        hybridWeek.eq(Configs.week - 1).removeClass("radiodivnot");
        Chart.drawHybrid(Hybrid, Configs.Hybrid.data);
    } else {
        Configs.Hybrid.data = $.extend(true, {}, Configs.Hybrid.data, data.data);
        Chart.drawHybrid(Hybrid, data.data);
    };
    Pages.BindRadio();
    if(jQuery.cookie("systeminfo")!=1){
        $(".hybrid").css({width:'1163px'});
        $(".hybrid-handle").css({right:'100px'});
    }else{
        $(".hybrid").css({width:'917px'});
        $(".hybrid-handle").css({right:'10px'});
    }
};
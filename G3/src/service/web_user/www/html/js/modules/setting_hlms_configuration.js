Pages.setting_hlms_configuration = function (data) {
    Configs.Data.polling = false;
	var lang = Language["setting_hlms_configuration"];
    var HLMSErrors = lang["errors"];
    var ChangeHLMSErrors = lang["changeErrors"];
    var TL1Errors = lang["TL1_errors"];
	var tips = lang["tips"];
    var that = this;
    var pType = Configs.protocol_type;
    var args = data.args;
    var data = data.data;
    if (pType == 1) {
        var old_data = {
            /*YDN23*/
            _protocol_type: data.content[0],
            _protocol_media: data.content[1],
            _esr_call_elapse_time: data.content[2],
            _esr_max_alarm_report_attempts: data.content[3],
            _reportinuse: data.content[4],
            _main_report_phone: data.content[5],
            _second_report_phone: data.content[6],
            _callback_phone: data.content[7],
            _commonparam: data.content[8],
            _ccid_ydn: data.content[9]
        }
    } else if (pType == 0) {
        /*EEM*/
        var old_data = {
            _protocol_type: data.content[0],
            _protocol_media: data.content[1],
            _esr_call_elapse_time: data.content[2],
            _esr_max_alarm_report_attempts: data.content[3],
            _callbackinuse: data.content[4],
            _reportinuse: data.content[5],
            _securitylevel: data.content[6],
            _main_report_phone: data.content[7],
            _second_report_phone: data.content[8],
            _callback_phone: data.content[9],
            _report_ip1: data.content[10],
            _report_ip2: data.content[11],
            _security_ip1: data.content[12],
            _security_ip2: data.content[13],
            _ccid: data.content[14],
            _socid: data.content[15],
            _commonparam: data.content[16]
        }
    } else if (pType == 2) {
        var old_data = {
            _protocol_type: data.content[0],
            _protocol_media: data.content[1],
            _commonparam: data.content[2],
            _ccid_ydn: data.content[3]
        }
    }
    var _port_active;
    function validateDisplay() {
    $.each($("[name='_port_active_ex']"),function(){
            if($(this).prop("className")=="radiodiv"){
                    _port_active=$(this).attr("value")
            }
    })
    if(_port_active == 1)
    {
        $(".tl1_parameter").css("display","table-row");
    }
    else
    {
        $(".tl1_parameter").css("display","none");
    }
    };
    validateDisplay();
    var _logon_user_ex = data.content[6] ? data.content[6] : "";
    $.each($("#_logon_user_ex").next("ul").find("a"),function(){
        if($(this).attr("rel")==_logon_user_ex){
            $("#_logon_user_ex").attr("data-index",_logon_user_ex).text(_logon_user_ex);
        }else{
            $("#_logon_user_ex").attr("data-index",0).text("None");
        }
    });
    /*设置当前设置项目标题*/
    var current_text = $(".hlms-set div").eq(pType).find("label").text();
    $("#current_protocol").html(current_text);
    /*绑定Protocol type设置*/
    $(".hlms-set div").each(function () {
        if ($(this).attr("value") == pType) {
            $(this).parent(".set_value").children().not(".radiodivnot").addClass("radiodivnot");
	        $(this).removeClass("radiodivnot");
        }
    }).on("click", function () {
        if ($(this).prop("className") == "radiodiv radiodivnot" && $(this).attr("value") != pType) {
            $(".btn_change").show();
        } else {
            $(".btn_change").hide();
        }
    });
    $(".btn_change").on("click", function () {
        var $this = $(this);
        var control_value=""
        var text = "";
        $.each($('[name="_protocol_type"]'),function(){
            if($(this).prop("className")=="radiodiv"){
                control_value = $(this).attr("value");
                text = $(this).find("label").text();
            }
        })
        var current_type = $(this).val();
        var submitData = "control_value=" + control_value + "&sessionId=" + args["sessionId"] + "&language_type=" + args["language_type"] + "&equip_ID=1&signal_id=457&signal_type=2&value_type=5&control_type=1";
        if (confirm(tips[0]+" " + text + " ?")) {
            var _form = $(this).closest("form");
            that.SetProcess(Language.Html['011']);
            var TestOline = $.ajax({
                url: "../template/isOnline.html?_=" + new Date().getTime(),
                success: function (data, textStatus, jqXHR) {
                    var XHR = $.ajax({
                        url: _form.attr("action") + "?_=" + new Date().getTime(),
                        type: _form.attr("method") ? _form.attr("method") : "GET",
                        timeout: 5000,
                        data: submitData,
                        success: function (data, textStatus, jqXHR) {
                            pType = current_type;
                            $("div.hlms-list").hide().eq(current_type).show();
                            $this.removeClass("radiodivnot");
                            try {
                                var data = jQuery.evalJSON(data);
                            } catch (err) {
                                that.SetProcessDone(Language.Html["032"]);
                                return false;
                            };
			    if (data.status == 98) {
				that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
				return;
			    } else {
				if(isNaN(data.status) || (data.status >= ChangeHLMSErrors.length) || !/^[0-6]$/g.test(data.status)){
					data.status = 0;
				}
		    	    }
			    if(data.status == 1) {
	                    	/*ACU将重启，设置OK时不返回数据!*/
	                    	that.DoRestart();
                            } else {
                                that.SetProcessDone(ChangeHLMSErrors[data.status]);
                                Control.RefreshModule();
                            }
                        },
                        error: function (data, textStatus) {
                            that.DoRestart();
                        }
                    }).always(function (jqXHR, textStatus, errorThrown) {
                        jqXHR = null;
                        textStatus = null;
                        errorThrown = null;
                        XHR = null;
                    });
                },
                error: function (data, textStatus) {
                    that.SetProcessDone(tips[1]);
                }
            }).always(function (jqXHR, textStatus, errorThrown) {
                jqXHR = null;
                textStatus = null;
                errorThrown = null;
                TestOline = null;
            });
        };
        return false;
    });
    //自定义单选按钮事件
    Pages.BindRadio();
    //自定义下拉列表事件
    Pages.BindSelect();
    //自定义复选框事件
    Pages.BindCheckBox();
     if(jQuery.cookie("systeminfo")!=1){
        $(".table li").css({"padding-right":"70px"});
        $(".table-container").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table li").css({"padding-right":"0px"});
        $(".table-container").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
    /*绑定_protocol_type_ex,_protocol_media_ex下拉框事件*/
    var _commonparam_ex = $("#_commonparam_ex");
    var _ipv6_label =$("label.ipv6_label");
    //port activation的绑定事件
    $("[name='_port_active_ex']").on("click", function () {
        validateDisplay();
    });
    $("[name='_protocol_type_ex'],[name='_protocol_media_ex']").on("click", function () {
       if($("[name='_protocol_type_ex']").length>1){
             $.each($("[name='_protocol_type_ex']"),function(){
                if($(this).prop("className")=="radiodiv"){
                     _protocol_type_v=$(this).attr("value");
                }
             })
        }else{
            _protocol_type_v=$("#_protocol_type_ex").val();
        }
         $.each($("[name='_protocol_media_ex']"),function(){
            if($(this).prop("className")=="radiodiv"){
                    _protocol_media_v=$(this).attr("value")
            }
        })
        /*改变_commonparam_ex值*/
        if ((_protocol_media_v == 2 || _protocol_media_v == 3) && _protocol_type_v == 4) {
            /*YDN23*/
            _commonparam_ex.val(5050);
            _commonparam_ex.attr("disabled", true);
            _commonparam_ex.addClass("disabled");
        } else if ((_protocol_media_v == 2 || _protocol_media_v == 3) && _protocol_type_v == 5) {
            /*Modbus*/
            _commonparam_ex.val(502);
            _commonparam_ex.attr("disabled", true);
            _commonparam_ex.addClass("disabled");
        } else {
            if (old_data._protocol_media == _protocol_media_v) {
                _commonparam_ex.val(old_data._commonparam);
                //if(_protocol_media_v == 2 || _protocol_media_v == 3)//如果当前选择的媒介与原来的一样,恢复IP输入框 zzc modify
                //{
                    //$("#_report_ip1_ex").val(old_data._report_ip1);
                    //$("#_report_ip2_ex").val(old_data._report_ip2);
                    //$("#_security_ip1_ex").val(old_data._security_ip1);
                    //$("#_security_ip2_ex").val(old_data._security_ip2);
                //}
            } else {
                var _commonparam_ex_value;
                if (_protocol_type_v == 4) {
                    _commonparam_ex_value = Language.HLMSConfigs.MediaParam_YDN23[_protocol_media_v]
                } else if (_protocol_type_v == 5) {
                    _commonparam_ex_value = Language.HLMSConfigs.MediaParam_Modbus[_protocol_media_v]
                } else {
                    _commonparam_ex_value = Language.HLMSConfigs.MediaParam[_protocol_media_v];
                    //if(_protocol_media_v == 2 || _protocol_media_v == 3)//如果当前选择的媒介与原来的不一样,清空IP输入框 zzc modify
                    //{
                        //$("#_report_ip1_ex,#_report_ip2_ex,#_security_ip1_ex,#_security_ip2_ex").val("");
                    //}
                }
                _commonparam_ex.val(_commonparam_ex_value);
            }
            _commonparam_ex.attr("disabled", false);
            _commonparam_ex.removeClass("disabled");
        };
	/*if(_protocol_media_v==2){
		_ipv6_label.hide();
	} else if(_protocol_media_v==3){
		_ipv6_label.show();
	}*/
	// IPV4和IPV6显示不同的label zzc modify
	if(_protocol_media_v==2 || _protocol_media_v==3)
	{
	    _ipv6_label.show();
	    if(_protocol_media_v==2)
	    {
	        $("#_ipv6_label1").html(Language.HLMSConfigs.ESRTips[11]);
	        $("#_ipv6_label2").html(Language.HLMSConfigs.ESRTips[12]);
	        $("#_ipv6_label3").html(Language.HLMSConfigs.ESRTips[13]);
	        $("#_ipv6_label4").html(Language.HLMSConfigs.ESRTips[14]);
	    }
	    else if(_protocol_media_v==3)
	    {
	        $("#_ipv6_label1").html(Language.HLMSConfigs.ESRTips[7]);
	        $("#_ipv6_label2").html(Language.HLMSConfigs.ESRTips[8]);
	        $("#_ipv6_label3").html(Language.HLMSConfigs.ESRTips[9]);
	        $("#_ipv6_label4").html(Language.HLMSConfigs.ESRTips[10]);
	    }
	}
        /*显示其他属性*/
        showParam(_protocol_type_v, _protocol_media_v);
    });
    function SetInputs(status, inputs) {
        if ($.type(inputs) == "array" && $.type(status) == "array" && status.length == inputs.length) {
            for (var i = 0, ilen = inputs.length; i < ilen; i++) {
                var input = $("#" + inputs[i]);
                input.attr("disabled", status[i]);
                if (status[i]) {
                    if (input.attr("type") == "checkbox") {
                        input.hide();
                        input.next("label").hide();
                    } else {
                        input.addClass("disabled");
                        input.parents("tr").hide();
                    }
                } else {
                    if (input.attr("type") == "checkbox") {
                        input.show();
                        input.next("label").show();
                    } else {
                        input.removeClass("disabled");
                        input.parents("tr").show();
                    }
                };

            }
        } else if ($.type(inputs) == "string" && $.type(status) == "string") {
            $("#" + inputs).attr("disabled", status);
            if (status) {
                input.addClass("disabled");
                input.parents("tr").hide();
            } else {
                input.removeClass("disabled");
                input.parents("tr").show();
            }
        };
        /*选择select时操作tr*/
        if(pType != 3)
        {
            listTr();
            }
    };
    /*加载时排序tr*/
    if(pType != 3)
    {
        listTr();
        }
    function listTr() {
        /*处理当前列表tr背景颜色的显示*/
        var trs = $("table.table-b tr");
        var index = 0;
        for (var i = 1, ilen = trs.length; i <= ilen; i++) {
            if ($(trs[i]).is(":visible")) {
//                if (index % 2 == 0) {
//                    $(trs[i]).attr("class", "");
//                } else {
//                    $(trs[i]).attr("class", "");
//                }
                index++;
            }
        }
    };
    function showParam(protocol, media) {
        var inputs = ["_ccid_ex", "_socid_ex", "_esr_max_alarm_report_attempts_ex", "_esr_call_elapse_time_ex", "_main_report_phone_ex", "_second_report_phone_ex", "_callback_phone_ex", "_report_ip1_ex", "_report_ip2_ex", "_security_ip1_ex", "_security_ip2_ex", "_securitylevel_ex", "_ccid_ydn", "_callbackinuse_ex", "_reportinuse_ex", "_reportinuse_ydn"];
        /*特别注意一下，下面一溜的true,false 针对的是disabled是否为true，如果为true,表示input将不可用*/
        if (protocol == 1 || protocol == 2 || protocol == 3) {
            switch (Number(media)) {
                case 0:
                    SetInputs([false, true, false, false, true, true, true, true, true, true, true, false, true, false, false, true], inputs);
                    break;
                case 1:
                    SetInputs([false, true, false, false, false, false, false, true, true, true, true, false, true, false, false, true], inputs);
                    break;
                case 2:
		case 3:
                    SetInputs([false, true, false, false, true, true, true, false, false, false, false, false, true, false, false, true], inputs);
                    break;
            };
            if (protocol == 3) {
                SetInputs([true, false], ["_ccid_ex", "_socid_ex"]);
            }
            SetInputs([false], ["_commonparam_ex"]);
        } else if (protocol == 4) {
            SetInputs([true, true, true, true, true, true, true, true, true, true, true, true, false, true, true, false], inputs);
            if (media == 2 || media == 3) {
                SetInputs([true], ["_commonparam_ex"]);
            } else {
                SetInputs([false], ["_commonparam_ex"]);
            }
        } else if (protocol == 5) {
            SetInputs([true, true, true, true, true, true, true, true, true, true, true, true, false, true, true, true], inputs);
            if (media == 2 || media == 3) {
                SetInputs([true], ["_commonparam_ex"]);
            } else {
                SetInputs([false], ["_commonparam_ex"]);
            }
        }
    };
    function isNumber(str) {
        return /\D/.test(str);
    };
    function validateForm() {
        if(pType == 3)
        {
            var _acess_port_ex = $("input[name='_commonparam_tl1']");
            var _session_timeout_ex = $("input[name='_session_timeout_ex']");
            var _system_identifier_ex = $("input[name='_system_identifier_ex']");
            
            var _acess_port_ex_value = $.trim(_acess_port_ex.val());
            var _session_timeout_ex_value = $.trim(_session_timeout_ex.val());
            var _system_identifier_ex_value = _system_identifier_ex.val();
            if((_acess_port_ex_value.length == 0) || (_acess_port_ex_value < 1024) || (_acess_port_ex_value > 65534))
            {
                alert(tips[27]);
                _acess_port_ex.select();
                return false;
            }
            if((_session_timeout_ex_value.length == 0) || (_session_timeout_ex_value < 0) || (_session_timeout_ex_value > 1440))
            {
                alert(tips[28]);
                _session_timeout_ex.select();
                return false;
            }
            if((_system_identifier_ex_value.length > 20))
            {
                alert(tips[29]);
                _system_identifier_ex.select();
                return false;
            }
            if(!(/^(\w){0,20}$/gi.test(_system_identifier_ex_value))){
                alert(tips[29]);
                _system_identifier_ex.select();
                return false;
                    }
            return true;
        }
        if ($("#_protocol_type_ex")[0]) {
            var _protocol_type_value = $("#_protocol_type_ex").val();
        } else {
             var _protocol_type_value ="";
            $.each($("[name='_protocol_type_ex']"),function(){
                if($(this).prop("className")=="radiodiv"){
                        _protocol_type_value=$(this).attr("value")
                }
            })
        }
	    var _protocol_media = "";
	    $.each($("[name='_protocol_media_ex']"),function(){
            if($(this).prop("className")=="radiodiv"){
                    _protocol_media=$(this).attr("value")
            }
        })
        var _ccid = $("#_ccid_ex"), _ccid_value = _ccid.val();
        var _esr_max_alarm_report_attempts = $("#_esr_max_alarm_report_attempts_ex"),
                _esr_max_alarm_report_attempts_value = _esr_max_alarm_report_attempts.val();
        var _esr_call_elapse_time = $("#_esr_call_elapse_time_ex"),
                _esr_call_elapse_time_value = _esr_call_elapse_time.val();
        var _socid = $("#_socid_ex"), _socid_value = _socid.val();
        /*validate ccid*/
        if (_ccid.prop("disabled") == false) {
            if (_ccid_value == "" || isNumber(_ccid_value) == true) {
                alert(tips[2]);
                _ccid[0].select();
                return false;
            } else if (parseInt(_ccid_value) > 255 || parseInt(_ccid_value) < 0) {
                alert(tips[3]+" " + tips[4]+" ");
                _ccid[0].select();
                return false;
            };
        }
        if (old_data[0] == 4) {
            /*validate max alarm report*/
            if (_esr_max_alarm_report_attempts.prop("disabled") == false) {
                if (isNumber(_esr_max_alarm_report_attempts_value) == true) {
                    alert(tips[5]);
                    _esr_max_alarm_report_attempts[0].select();
                    return false;
                } else if (parseInt(_esr_max_alarm_report_attempts_value) > 5 || parseInt(_esr_max_alarm_report_attempts_value) < 0) {
                    alert(tips[6]);
                    _esr_max_alarm_report_attempts[0].select();
                    return false;
                }
            }
            /*validate max elapse time*/
            if (_esr_call_elapse_time.prop("disabled") == false) {
                if (_esr_call_elapse_time_value != "") {
                    if (isNumber(_esr_call_elapse_time_value) == true) {
                        alert(tips[7]);
                        _esr_call_elapse_time[0].select();
                        return false;
                    }
                    else if (parseInt(_esr_call_elapse_time_value) > 300 || parseInt(_esr_call_elapse_time_value) < 0) {
                        alert(tips[8]);
                        _esr_call_elapse_time[0].select();
                        return false;
                    }
                }
            }
        } else {
            /*validate socid*/
            if (_socid.prop("disabled") == false) {
                if (_socid_value != "") {
                    if (isNumber(_socid_value) == true) {
                        alert(tips[9]);
                        _socid[0].select();
                        return false;
                    } else if (_protocol_type_value == 3 && parseInt(_socid_value) == 0) {
                        alert(tips[10] + tips[11]);
                        _socid[0].select();
                        return false;
                    } else if (parseInt(_socid_value) < 0 && parseInt(_socid_value) > 20479) {
                        alert(tips[12] + tips[13]);
                        _socid[0].select();
                        return false;
                    }
                }
            }
            /*validate max alarm report*/
            if (_esr_max_alarm_report_attempts.prop("disabled") == false) {
                if (_esr_max_alarm_report_attempts_value != "") {
                    if (isNumber(_esr_max_alarm_report_attempts_value) == true) {
                        alert(tips[14]);
                        _esr_max_alarm_report_attempts[0].select();
                        return false;
                    } else if (parseInt(_esr_max_alarm_report_attempts_value) > 255 || parseInt(_esr_max_alarm_report_attempts_value) < 0) {
                        alert(tips[15]);
                        _esr_max_alarm_report_attempts[0].select();
                        return false;
                    }
                };
            }
            /*validate max elapse time*/
            if (_esr_call_elapse_time.prop("disabled") == false) {
                if (_esr_call_elapse_time_value != "") {
                    if (isNumber(_esr_call_elapse_time_value) == true) {
                        alert(tips[16]);
                        _esr_call_elapse_time[0].select();
                        return false;
                    } else if (parseInt(_esr_call_elapse_time_value) > 600 || parseInt(_esr_call_elapse_time_value) < 0) {
                        alert(tips[17]);
                        _esr_call_elapse_time[0].select();
                        return false;
                    }
                }
            }
            /*validate main report phone*/
            var _main_report_phone = $("#_main_report_phone_ex"), _main_report_phone_value = _main_report_phone.val();
            if (_main_report_phone.prop("disabled") == false) {
                if (_main_report_phone_value != "" && isNumber(_main_report_phone_value) == true) {
                    alert(tips[18]);
                    _main_report_phone[0].select();
                    return false;
                };
            }
            /*validate second report phone*/
            var _second_report_phone = $("#_second_report_phone_ex"), _second_report_phone_value = _second_report_phone.val();
            if (_second_report_phone.prop("disabled") == false) {
                if (_second_report_phone_value != "" && isNumber(_second_report_phone_value) == true) {
                    alert(tips[19]);
                    _second_report_phone[0].select();
                    return false;
                };
            }
            /*validate callback phone*/
            var _callback_phone = $("#_callback_phone_ex"), _callback_phone_value = _callback_phone.val();
            if (_callback_phone.prop("disabled") == false) {
                if (_callback_phone_value != "" && isNumber(_callback_phone_value) == true) {
                    alert(tips[20]);
                    _callback_phone[0].select();
                    return false;
                };
            }
            /*validate report ip*/
			var _report_ip1_ex = $("#_report_ip1_ex"),_report_ip2_ex = $("#_report_ip2_ex"),_security_ip1_ex = $("#_security_ip1_ex"),_security_ip2_ex =$("#_security_ip2_ex");
			if(_protocol_media==3){
				if (!checkipv6(_report_ip1_ex, tips[21])) {
					return false;
				}
				if (!checkipv6(_report_ip2_ex, tips[22])) {
					return false;
				}
				if (!checkipv6(_security_ip1_ex, tips[23])) {
					return false;
				}
				if (!checkipv6(_security_ip2_ex, tips[24])) {
					return false;
				}
			} else {
				if (!checkips(_report_ip1_ex, tips[21])) {
					return false;
				}
				if (!checkips(_report_ip2_ex, tips[22])) {
					return false;
				}
				if (!checkips(_security_ip1_ex, tips[23])) {
					return false;
				}
				if (!checkips(_security_ip2_ex, tips[24])) {
					return false;
				}
			}
        };
	//验证IPV6
		function checkipv6(input,text){
			var ipv6 = $.trim(input.val());
			if(ipv6!=""){
				//验证格式"[ipv6地址]:端口号"
				if(!Validates.CheckIPV6(ipv6,true)){
					alert(text);
					input[0].focus();
					return false;
				} else {
					return true;
				}
			}
			return true;
		}
	//验证IPV4
        function checkips(input, text) {
            var ipBrd = Validates.ParseIntIP("255.255.255.255");
            if (input.prop("disabled") == true) {
                return true;
            };
            var value = input.val();
            if (value != "") {
                varIndex = value.indexOf(":");
                if (varIndex < 0) {
                    alert(tips[25]);
                    input[0].focus();
                    return false;
                }
                strIp = value.substring(0, varIndex);
                strPort = value.substring(varIndex + 1, value.length);
                ip = Validates.ParseIntIP(strIp);
                if (!Validates.CheckIP(strIp) || (ip == 0) || (ip == ipBrd)) {
                    alert(text);
                    input[0].select();
                    return false;
                } else if (parseInt(strPort) < 0 || parseInt(strPort) > 65534) {
                    alert(tips[26]);
                    input[0].select();
                    return false;
                }
            };
            return true;
        }
        return true;
    };

    /*提交设置*/
    /*如果发生了改变，数据+"*1"，没有发生改变，数据+"*0"*/
    /*旧数据*/
    var old_datas = data.content;
    $(".hlms_submit").on("click", function () {
        if (!validateForm()) {
            return false;
        }
        /*array;
        /*获取新数据*/
        var new_data = [];
        if (old_datas[0] == 4) {
            var new_ids = [
                    "_protocol_type_ex",
                    "_protocol_media_ex",
                    "_esr_call_elapse_time_ex",
                    "_esr_max_alarm_report_attempts_ex",
                    "_reportinuse_ydn",
                    "_main_report_phone_ex",
                    "_second_report_phone_ex",
                    "_callback_phone_ex",
                    "_commonparam_ex",
                    "_ccid_ydn"
                ];
            /*
            _ydn_max_alarm_report_attempts_ex
            _ydn_call_elapse_time_ex

            _ccid_ydn
            */
        } else if (old_datas[0] == 5) {
            var new_ids = [
                    "_protocol_type_ex",
                    "_protocol_media_ex",
                    "_commonparam_ex",
                    "_ccid_ydn"
                ];
        } else {
            var new_ids = [
                    "_protocol_type_ex",
                    "_protocol_media_ex",
                    "_esr_call_elapse_time_ex",
                    "_esr_max_alarm_report_attempts_ex",
                    "_callbackinuse_ex",
                    "_reportinuse_ex",
                    "_securitylevel_ex",
                    "_main_report_phone_ex",
                    "_second_report_phone_ex",
                    "_callback_phone_ex",
                    "_report_ip1_ex",
                    "_report_ip2_ex",
                    "_security_ip1_ex",
                    "_security_ip2_ex",
                    "_ccid_ex",
                    "_socid_ex",
                    "_commonparam_ex"
                ];
        }
        var submitData = {};
        /*比较old_datas*/;
        if(pType != 3){
        for (var i = 0, ilen = old_datas.length; i < ilen; i++) {
            var input="";
            if(document.getElementById(new_ids[i])){
                input = document.getElementById(new_ids[i]);
            }else{
                $.each($("[name='" + new_ids[i] + "']"),function(){
                    if($(this).prop("className")=="radiodiv"){
                        input=$(this);
                    }
                });
            }
            if (!input) { continue; };
            var value="";
            if(!input.nodeName){
            	value = $.trim($(input).attr("value"));
            }else{
                value = $.trim($(input).val());
            }
            if (old_datas[0] == 4) {
                if (i == 4) {
                     if ($(input).prop("checked") == true) {
                        value = "1";
                    } else {
                        value = "";
                    }
                }
            } else {
                if (i == 4 || i == 5) {
                    if ($(input).prop("checked") == true) {
                        value = "1";
                    } else {
                        value = "";
                    }
                } else if (i == 6) {
                    value = $(input).attr("data-index");
                }
            }
           if (i == 0 ) {
                $.each($("dd.set_value div"),function(){
                    if($(this).prop("className") == "radiodiv"){
                        if($(this).attr("value")==0||$(this).attr("value")==3){
                            $.each(input,function(){
                                if ($(input).prop("className") == "radiodiv") {
                                   value = $(input).attr("value")
                                }
                            });
                        }else{
                            value = $(input).val()
                        }
                    }
                });
            }
            if(i==1){
                $.each(input,function(){
                    if ($(input).prop("className") == "radiodiv") {
                      value = $(input).attr("value")
                    }
                });
            }
            if (old_datas[0] == 4 || old_datas[0] == 5) {
                if (i == 2 || i == 3) {
                    var name = $.trim($(input).attr("name")).replace("_esr_", "_ydn_");
                } else {
                    var name = $.trim($(input).attr("name")).replace("_ex", "_ydn");
                }
            } else {
                var name = $.trim($(input).attr("name"));
            }
            if (value == old_datas[i]) {
                submitData[name] = value + "*0";
            } else {
                submitData[name] = value + "*1";
            }
        }}
        else
        {
                $.each($("[name='_port_active_ex']"),function(){
                    if($(this).prop("className")=="radiodiv"){
                        submitData["_port_active_ex"]=$(this).attr("value");
                    }
                })
               // submitData["_port_active_ex"] = $("[name='_port_active_ex']").val();
                
                $.each($("[name='_protocol_media_ex']"),function(){
                    if($(this).prop("className")=="radiodiv"){
                        submitData["_protocol_media_ex"]=$(this).attr("value");
                    }
                })
                //submitData["_protocol_media_ex"] = $("input[name='_protocol_media_ex']:checked").val();
                
                
                submitData["_acess_port_ex"] = $("input[name='_commonparam_tl1']").val();
                
                $.each($("[name='_port_keep_alive_ex']"),function(){
                    if($(this).prop("className")=="radiodiv"){
                        submitData["_port_keep_alive_ex"]=$(this).attr("value");
                    }
                })
                //submitData["_port_keep_alive_ex"] = $("input[name='_port_keep_alive_ex']:checked").val();
                
                
                submitData["_session_timeout_ex"] = $("input[name='_session_timeout_ex']").val();
                submitData["_logon_user_ex"] = $("#_logon_user_ex").attr("data-index");
                submitData["_system_identifier_ex"] = $("input[name='_system_identifier_ex']").val();
        }
        submitData["sessionId"] = args["sessionId"];
        submitData["language_type"] = args["language_type"];
        if (args["level"] < 3) {
            alert(HLMSErrors[8]);
            return false;
        } else {
            if (validateForm()) {
                if (pType == 1) {
                    /*YDN23*/
                    submitData["_modify_configure_detail"] = 23;
                    submitData["_modify_configure"] = 9;
                    /*submitData["_protocol_type_ex"] = 4 + "*0";*/
                    submitData["_protocol_type_ydn"] = 4 + "*0";
                } else if (pType == 0) {
                    /*EEM*/
                    submitData["_modify_configure_detail"] = 5;
                    submitData["_modify_configure"] = 2;
                } else if (pType == 2) {
                    /*Modbus*/
                    submitData["_modify_configure_detail"] = 39;
                    submitData["_modify_configure"] = 17;
                    /*submitData["_protocol_type_ex"] = 5 + "*0";*/
                    submitData["_protocol_type_ydn"] = 5 + "*0";
                } else if(pType == 3)
                {
                    submitData["_modify_configure_detail"] = 51;
                    submitData["_modify_configure"] = 19;
                }
            }
        };
        var _form = $(this).closest("form");
        that.SetProcess(Language.Html['011'], "", true);
        sendHLMS();
        function sendHLMS() {
            that.SetProcessStart();
            var XHR = $.ajax({
                url: _form.attr("action") + "?_=" + new Date().getTime(),
                type: _form.attr("method") ? _form.attr("method") : "GET",
                data: submitData,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };
					if (data.status == 98) {
						that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
						return;
					} else {
					        if(pType != 3)
						{if(isNaN(data.status) || data.status >= HLMSErrors.length){
							data.status = 100;
						}}
					}
					setTimeout(function () {
						if (data.status != 100) {
							if (data.status == 0) {
								old_datas = data.content;
								if(pType==0){
								    old_data._commonparam = data.content[16];
								}
								if(pType==1){
								    old_data._protocol_media = data.content[1];
								    old_data._commonparam=data.content[8];
								}
								if(pType==2){
								    old_data._protocol_media = data.content[1];
								    old_data._commonparam=data.content[2];
								}
								that.SetProcessOK(HLMSErrors[data.status]);
							} else {
							        if(pType == 3)
							        {
							            that.SetProcessDone(TL1Errors[data.status]);
							        }
							        else
							        {
							            that.SetProcessDone(HLMSErrors[data.status], sendHLMS);
							            Control.RefreshModule();
							        }
								
							}
						} else {
							/*Unknown error*/
							that.SetProcessDone(HLMSErrors[data.status], sendHLMS);
							Control.RefreshModule();
						}
					}, 500);
                    
                },
                error: function (data, textStatus) {
                    setTimeout(function () {
                        that.SetProcessDone(Language.Html['002'], sendHLMS);
                    }, 500);
                }
            }).done(function (data, textStatus, jqXHR) {
                jqXHR = null;
                XHR = null;
            });
        };
        return false;
    });
};
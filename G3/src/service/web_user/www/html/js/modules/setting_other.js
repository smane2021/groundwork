Pages.setting_other = function (data) {
    Configs.Data.polling = false;
	var lang = Language["setting_other"];
    var OtherErrors = lang["errors"];
	var tips = lang["tips"];
    var that = this;
    var args = data.args, data = data.data;
    var sessionId = "&sessionId=" + args["sessionId"];
    var language_type = "&language_type=" + args["language_type"];
      if(jQuery.cookie("systeminfo")!=1){
            $(".table-title,.table-container .table").css({width:'1143px'});
            $(".table-container").css({width:'1163px'});
        }else{
            $(".table-title,.table-container .table").css({width:'897px'});
            $(".table-container").css({width:'917px'});
        }
    /*SMTP*/
    var smtp_form = $("#SettingSMTP"), account_set = $("#SMTP_account");
    $(".SMTPauthen", smtp_form).on("click", function () {
        var value = $(this).val();
        if (value == 1) {
            account_set.find("input").removeClass("disabled").attr("disabled", false);
        } else {
            account_set.find("input").addClass("disabled").attr("disabled", true);
        }
    });   
    $(".smtp_submit", smtp_form).on("click", function () {
        if ($(this).hasClass("btn_2_disabled")) {
            return false;
        }
        /*validate*/
        /*验证邮箱*/
        var EmailError = tips[0];
        var mailto = $("input[name='_SMTPsendto']", smtp_form);
        var mailfrom = $("input[name='_SMTPsendfrom']", smtp_form);
        if (!Validates.CheckEmail(mailto)) {
            alert(EmailError);
            return false;
        };
        /*验证IP*/
        var ip = $("input[name='_SMTPserverIp']", smtp_form);
        if (!Validates.CheckIP(ip.val())) {
            alert(tips[1]);
            ip[0].select();
            return false;
        }
        if (!Validates.CheckEmail(mailfrom)) {
            alert(EmailError);
            return false;
        };
        that.SetProcess(Language.Html['011'], "", true);
        var _SMTPauthen=""
        var _SMTPalarmLevel="";
        $.each($('[name="_SMTPauthen"]'),function(){
            if($(this).prop("className")=="radiodiv"){
                _SMTPauthen=$(this).attr("value");
            }
        });
         $.each($('[name="_SMTPalarmLevel"]'),function(){
            if($(this).prop("className")=="radiodiv"){
                _SMTPalarmLevel=$(this).attr("value");
            }
        });
        sendSMTP();
        var ResetNames = ["_SMTPsendto", "_SMTPserverIp", "_SMTPserverPort", "_SMTPsendfrom", "_SMTPauthen", "_SMTPaccount", "_SMTPpassword", "_SMTPalarmLevel"];
        function sendSMTP() {
            that.SetProcessStart();
            var submitdata = smtp_form.serialize()+"&_SMTPauthen="+_SMTPauthen+"&_SMTPalarmLevel="+_SMTPalarmLevel+"" + sessionId + language_type + "&_modify_configure=13&_modify_configure_detail=32";
            var XHR = $.ajax({
                url: smtp_form.attr("action") + "?_=" + new Date().getTime(),
                type: smtp_form.attr("method") ? smtp_form.attr("method") : "GET",
                data: submitdata,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };
                    if (data.status == 1) {
                        that.SetProcessOK(OtherErrors[1]);
                        /*Control.RefreshModule()*/
                    } else if (data.status == 98) {
                        that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
                        return;
                    } else {
                        setTimeout(function () {
                            that.SetProcessDone(OtherErrors[data.status], sendSMTP);
                            /*重置表单*/
                            ResetForm(data["content"], ResetNames, { "ip": ["_SMTPserverIp"] });
                        }, 500);
                    }
                },
                error: function (data, textStatus) {
                    setTimeout(function () {
                        that.SetProcessDone(Language.Html['002'], sendSMTP);
                        ResetForm(data["content"], ResetNames, { "ip": ["_SMTPserverIp"] });
                    }, 500);
                }
            }).done(function (data, textStatus, jqXHR) {
                jqXHR = null;
                XHR = null;
            });
        };
        return false;
    });

        GetOtherData("SMSList", [36, 16], "../cgi-bin/web_cgi_setting.cgi", doSMS);
      
    /*SMS*/
    function doSMS() {
        var sms_form = $("#SMSForm");
        $(".sms_submit", sms_form).on("click", function () {
            if ($(this).hasClass("btn_2_disabled")) {
                return false;
            }
            var sms_p1 = sms_form.find("input[name='_SMSphone1']");
            var sms_p2 = sms_form.find("input[name='_SMSphone2']");
            var sms_p3 = sms_form.find("input[name='_SMSphone3']");
            var inputError = tips[2];
            if (!/^ *\d* *$/g.test(sms_p1.val())) {
                sms_p1.focus();
                alert(inputError);
                return false;
            };
            if (!/^ *\d* *$/g.test(sms_p2.val())) {
                sms_p2.focus();
                alert(inputError);
                return false;
            };
            if (!/^ *\d* *$/g.test(sms_p3.val())) {
                sms_p3.focus();
                alert(inputError);
                return false;
            };
            var _SMSalarmLevel="";
             $.each($('[name="_SMSalarmLevel"]'),function(){
            if($(this).prop("className")=="radiodiv"){
                _SMSalarmLevel=$(this).attr("value");
            }
        });
            that.SetProcess(Language.Html['011'], "", true);
            sendSMS();
            var ResetNames = ["_SMSphone1", "_SMSphone2", "_SMSphone3", "_SMSalarmLevel"];
            function sendSMS() {
                that.SetProcessStart();
                var submitdata = sms_form.serialize()+"&_SMSalarmLevel="+_SMSalarmLevel +""+sessionId + language_type + "&_modify_configure=16&_modify_configure_detail=37";
                submitdata = submitdata.replace("_SMSphone1=", "_SMSphone1= ").replace("_SMSphone2=", "_SMSphone2= ").replace("_SMSphone3=", "_SMSphone3= ");
                var XHR = $.ajax({
                    url: sms_form.attr("action") + "?_=" + new Date().getTime(),
                    type: sms_form.attr("method") ? sms_form.attr("method") : "GET",
                    data: submitdata,
                    success: function (data, textStatus, jqXHR) {
                        try {
                            var data = jQuery.evalJSON(data);
                        } catch (err) {
                            alert(Language.Html["032"]);
                            return false;
                        };
                        if (data.status == 1) {
                            that.SetProcessOK(OtherErrors[1]);
                            /*Control.RefreshModule()*/
                        } else if (data.status == 98) {
                            that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
                            return;
                        } else {
                            setTimeout(function () {
                                that.SetProcessDone(OtherErrors[data.status], sendSMS);
                                ResetForm(data["content"], ResetNames);
                            }, 500);
                        }
                    },
                    error: function (data, textStatus) {
                        setTimeout(function () {
                            that.SetProcessDone(Language.Html['002'], sendSMS);
                            ResetForm(data["content"], ResetNames);
                        }, 500);
                    }
                }).done(function (data, textStatus, jqXHR) {
                    jqXHR = null;
                    XHR = null;

                });
            }
            return false;
        });
        $(".btn_2").removeClass("btn_2_disabled");
    }
    function GetOtherData(target, configure, url, callback) { /*configure为数组,分别代表_modify_configure_detail,_modify_configure*/
        if (!document.getElementById(target)) {
            return false;
        };
        var args = arguments;
        Control.SettingOtherXHR = $.ajax({
            url: url + "?_=" + new Date().getTime(),
            type: "get",
            data: "_modify_configure_detail=" + configure[0] + "&_modify_configure=" + configure[1] + sessionId + language_type,
            beforeSend: function () {
                Control.PromptEvent.Loading({ target: target, marginTop: 35 });
            },
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    loadfail(args, true, callback);
                    return false;
                }
                var tmp = doT.templateChild($("#" + target).attr("template"));
                var doTtmpl = doT.template(tmp);
                $("#" + target).html(doTtmpl(data));
                $("#" + target + "_set").show();
                if ($.type(callback) == "function") {
                    setTimeout(function () {
                        callback();
                    }, 200);
                };
                that.bindInput();
                Pages.BindRadio();
            },
            error: function (data, textStatus) {
                if (textStatus != "abort") {
                    loadfail(args, false, callback);
                }
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            Control.SettingOtherXHR = null;
        });
    }
    function loadfail(args, format, callback) {
        var loading = $("#" + args[0]).find(".setting_other_loading");
        if (format) {
            loading.html(tips[3]+" " + args[0].replace("Form", "") + " "+tips[4]);
        } else {
            loading.html(tips[3]+" " + args[0].replace("Form", "") + " "+tips[5]);
        };
        loading.addClass("loadingError");
        if ($.type(callback) == "function") {
            callback();
        }
    }

    /*设置失败时,把失败返回的值恢复到表单中*/
    function ResetForm(data, names, spec) {/*spec={"ip":[""]}*/
		if(!data || !names){ return; }
        for (var item in names) {
            var input = $("input[name=" + names[item] + "]");
            var type = input.attr("type");
            switch (type) {
                case "radio":
                    input.eq(data[item]).prop("checked", true);
                    break;
                case "text":
                    input.val(data[item]);
                    if ($.type(spec) == "object") {
                        if (spec.ip) {
                            for (var ip in spec.ip) {
                                if (spec.ip[ip] == names[item]) {
                                    input.val(Validates.ConvertIP(data[item]));
                                }
                            }
                        }
                    }
                    break;
            };
        }
    };
};
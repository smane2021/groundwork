Pages.setting_user = function (data) {
    Configs.Data.polling = false;
	var lang = Language["setting_user"];
    var UserErrors = lang["errors"];
	var tips = lang["tips"];
    var that = this;
    var _form = $("#userForm"), 
		_name = $("#_name"),
		_privilege = $("#_privilege"),
		_authority_level = $("#_authority_level"),	//_authority_level是一个隐藏的input
		_modify_configure_detail = $("#_modify_configure_detail"),
		_submit = $("#_submit"),
		_lcdEnable= $("#_lcdEnable"),
		_password1 = $("#_password1"),
		_password2 = $("#_password2"),
		_LoginLimit = $("#_LoginLimit"),
		_UnlockAccount = $("#_UnlockAccount"),
		_strongPass = $("#_strongPass"),
		_btnReset = $("a.formReset"),
		_btnModify = $("a.userModify"),
		_btnAdd = $("a.userAdd"),
		_btnDelete = $("a.userDelete"),
		_userList =  $("div.table-body"),
		_userSelected = null,	//当前选择行
		_userFocus = null,		//当前光标定位的input
		_SelectedClass = "selected";
	
       var _rform = $("#RadiusForm"), 
		_pserver = $("#_pserver"),
		_sserver = $("#_sserver"),
		_rsubmit = $("#_rsubmit"),
		_savesettings = $("#_savesettings"),
		_p1port = $("#_p1port"),
		_p2port = $("#_p2port"),
		_nasidentifier = $("#_nasidentifier"),
		_radiusenable = $("#_radiusenable"),
		_passwordR = $("#_passwordR"),
		_passwordC = $("#_passwordC"),
		_btnSave = $("a.rSave"),
		_userFocus = null,
		_SelectedClass = "selected";
//function for Read the Radius Server Configuration

readTextFile("../../../radius/radius_config/rserver_param.txt");
function readTextFile(file)
{
	var rawFile = new XMLHttpRequest();
	rawFile.open("GET", file, false);
	rawFile.onreadystatechange = function ()
	{
		if(rawFile.readyState === 4)
		{
			if(rawFile.status === 200 || rawFile.status == 0)
			{
				allText = rawFile.responseText;
			}	                                     
               }
    }
    //Fixed Bug No . 19 - Prevent load data from cache
    rawFile.setRequestHeader('Cache-Control', 'no-cache');
         rawFile.send(null);
};

var array = allText;
print = array.split(",");
var first = print[0];
second = print[5];


//Added on keypress event for user and radius form 
$(document).on("keypress", "#userForm", function (e) {
	_ResetInputstyleUserform();
});

function _ResetInputstyleUserform(){
	$("#_name").removeClass("error");
	$("#_password1").removeClass("error");
	$("#_password2").removeClass("error");
	$("div.setting_user_name").hide();
	$("div.setting_password").hide();
	$("div.setting_confirm").hide();
	$("div.setting_LcdLogin").hide();
	$("div.setting_LoginLimit").hide();
	$("div.setting_UnlockAccount").hide();
	$("div.setting_StrongPassword").hide();
}

$(document).on("keypress", "#RadiusForm", function (e) {
    _ResetInputstyleRadiusform()         
});
function _ResetInputstyleRadiusform(){
    $("#_pserver").removeClass("error");                               
    $("#_passwordR").removeClass("error");                             
    $("#_passwordC").removeClass("error");
    $("#_p2port").removeClass("error");
    $("#_sserver").removeClass("error");
    $("div.setting_r_primary_server").hide();
    $("div.setting_r_secondary_server").hide();
    $("div.setting_r_secondary_port_i").hide();
    $("div.setting_r_secret_key").hide();
    $("div.setting_r_confirm").hide();
}


$(document).ready(function() {
         	   
	if( first == "on" && second == "on")
		{
			document.getElementById("_radiusenable").checked = true;
  	   	document.getElementById("_nasidentifier").checked = true;
  		 }
	else if (first == "on" && second == "")
       {
         document.getElementById("_radiusenable").checked = true;
         document.getElementById("_nasidentifier").checked = false;
       }
   else
       {
         document.getElementById("_nasidentifier").checked = false;
			document.getElementById("_radiusenable").checked = false;
       } 
       $("#_pserver").val(print[1]);
       $("#_p1port").val(print[2]);
       $("#_sserver").val(print[3]);
       $("#_p2port").val(print[4]);
         	   
});
	//取消输入框的禁用
	function _EnableInputs(){
        _ResetInputstyleUserform();
		_name.attr("disabled", false);
		Pages.BindSelect();
	}
	//启用输入框的禁用
	function _DisableInputs(){
		_name.attr("disabled", true);
		_privilege.attr("disabled", true);
	}
	//取消选择
	function _Deselect(){
		if(!_userSelected){ return; }
		_contact.val("");
		_userSelected.removeClass(_SelectedClass).find("div").eq(0).addClass("radiodivnot");
	}
	//选择当前行
	function _Reselect(){
		if(!_userSelected){ return; }
		_userSelected.siblings().removeClass(_SelectedClass).end().addClass(_SelectedClass).find("div").eq(0).removeClass("radiodivnot");
		_userSelected.siblings().find("div").addClass("radiodivnot").end();
		_password1.val("");
		_password2.val("");
	}

	//增加按钮改为修改按钮
	function _ResetBtn(){
		_btnAdd.add(_btnModify).show();
	}
	//显示修改、隐藏增加
	function _ShowModify(){
		_btnModify.show();
		_btnAdd.hide();
	}
	//显示添加按钮、重置按钮
	function _ShowAdd(){
		_btnModify.hide();
		_btnAdd.show();
	}
	//重置Form所有选项,并取消当前用户的选择
	function ResetForm(){
		_form[0].reset();
		_privilege.attr("data-index",1).text($(".language li").eq(0).text());
		_EnableInputs();
		_Deselect();
		_ResetBtn();
		_name.focus();
	}
	//重置按钮
    _btnReset.off().on("click", function () {
        ResetForm();
        _ResetInputstyleUserform();
        return false;
    });
	//写入form
	function RewriteForm(datas){
		if(!datas){ return ; }
		_name.val(datas["_name"] || "");

                document.getElementById("_UnlockAccount").checked = false;

                if((datas["_contact"]) == "Enabled"){
                    document.getElementById("_lcdEnable").checked = true;
                }else{
                    document.getElementById("_lcdEnable").checked = false;
                }
                
                if((datas["_LoginLimit"]) == "Enabled"){
                    document.getElementById("_LoginLimit").checked = true;
                }else{
                    document.getElementById("_LoginLimit").checked = false;
                }               
                
		if((datas["_strongPass"]) == "Enabled"){
                    document.getElementById("_strongPass").checked = true;
                }else{
                    document.getElementById("_strongPass").checked = false;
                }  
                _privilege.attr("data-index",(datas["_privilege"] || "")).text($(".language li").eq(datas["_privilege"]-1).text());
                _privilege.text
		_authority_level.val(datas["_privilege"] || "");
		_contact.val(datas["_contact"] || "");
	};
	
	//处理admin、engineer用户,这两个用户不能修改权限和名称
	function handlerAdmin(name){
		if(!name){ return; }
		switch(name){
			case "admin":
			case "engineer":
			    _name.prop("disabled", true);
			    _privilege.unbind();
				break;
			default:
				Pages.BindSelect();
		}
	}

	//选择用户,并将相应信息写入Form
	_userList.off().on("click","ul",function(){
		_userSelected = $(this);
		//取消选择
		if($(this).hasClass(_SelectedClass)){
			ResetForm();
			_ShowAdd();
			_userSelected = null;
			return;	
		}
		//选择  
		_Reselect();
		//rewrite form
		var datas = jQuery.evalJSON($(this).find("div").eq(0).attr("value"))	//val()值是一个json数据
		RewriteForm(datas);
		_ShowModify();
		handlerAdmin(datas["_name"]);
	});

	//改变用户级别
	_privilege.next("ul").find("li").off().on("click",function(){
		_authority_level.val($(this).find("a").attr("rel"));
	});

	//获取当前定位focus
	_form.off().on("focus","input,select,textarea",function(){
		_userFocus = $(this);
	});
	
	        

	//验证表单
    function checkUserForm() {
		//验证用户名：不能为空,且只允许数字、字母、下划线
        //START__reference added
        console.log('user_setting_debug_1');
        var gu = $("div.setting_user_name");
        var s_password = $("div.setting_password");
        var s_confirm = $("div.setting_confirm");
        var LcdLogin = $("div.setting_LcdLogin");
        LcdLogin.hide();
        //END__reference added

     	if ($.trim(_name.val()) == "") {
            //alert('EMPTY');
            console.log('user_setting_debug_3');
            gu.html(tips[0]).show();
	    s_password.hide();
	    s_confirm.hide();
	    _password1.val("");
            LcdLogin.hide();
            _lcdEnable.removeClass("error");
	    
	    _password2.val("");
            _name.addClass("error");
            _password1.removeClass("error");
            _password2.removeClass("error");
            _name.focus();
            console.log('user_setting_debug_4');
            return false;
        };
        var InputValueError = tips[6];
        if (!(/^(\w){1,14}$/gi.test(_name.val()))) {
            //alert(InputValueError);
            gu.html(InputValueError).show();
            s_password.hide();
            s_confirm.hide();
            LcdLogin.hide();
            _lcdEnable.removeClass("error");
            _password1.val("");         
            _password2.val("");
            _name.addClass("error");
            _password1.removeClass("error");
            _password2.removeClass("error");
            _name.focus();
            return false;
        }
	//验证密码：至少为6位数,且不能为空，两个密码必须输入一致
        if ($.trim(_password1.val()) == "") {
            //alert(tips[3]);
            s_password.html(tips[3]).show();
            gu.hide();                      
            s_confirm.hide();
            LcdLogin.hide();
            _lcdEnable.removeClass("error");
            _password2.val("");            
            _password1.addClass("error");
            _password1.focus();
            _name.removeClass("error");
            _password2.removeClass("error");
            //s_confirm.remove();
            return false;
        };

      	// if strong password checkbox is checked, then only validate password against special char and length
        var check = document.getElementById("_strongPass").checked;
        if(check){
            // minimum password length should be 16
            var PasswordLengthError = tips[22];
            if (_password1.val().length < 16) {
                s_password.html(PasswordLengthError).show();
                _password1.focus();
                LcdLogin.hide();
                _lcdEnable.removeClass("error");
                _name.removeClass("error");                                                                
                _password1.addClass("error");                                                        
                _password2.removeClass("error");
                s_confirm.hide();
                gu.hide();
                _password2.val("");
                return false;
            }
            //  Password input should contain at least one Upper, Lower, Numeric and special character   
            var InputValueError = tips[23];
            if(!(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!"#$%&'()*+,-./:<=>?@[\]^_`{|}~])/g.test(_password1.val()))){  
                s_password.html(InputValueError).show();
                _password1.focus();
                s_confirm.hide();
                LcdLogin.hide();
                _lcdEnable.removeClass("error");
                gu.hide();
                _password2.val("");
                _name.removeClass("error");                                                                
                _password1.addClass("error");   
                _password2.removeClass("error");
                return false;
            }
        }
  	else{
            var PasswordLengthError = tips[24];
            if (_password1.val().length < 6 || _password1.val().length > 13) {
                //alert(PasswordLengthError);
                s_password.html(PasswordLengthError).show();
                _password1.focus();
                LcdLogin.hide();
                _lcdEnable.removeClass("error");
                _name.removeClass("error"); 
                _password1.addClass("error");
                _password2.removeClass("error");
                s_confirm.hide();
                gu.hide();
                _password2.val("");
                return false;
            }
            var InputValueError = tips[6];
            if (!(/^(\w){1,14}$/gi.test(_password1.val()))) {
                //alert(InputValueError);
                s_password.html(InputValueError).show();
                _password1.focus();
                s_confirm.hide();
                gu.hide();
                LcdLogin.hide();
                _lcdEnable.removeClass("error");
                _password2.val("");
                _name.removeClass("error");                                                                
                _password1.addClass("error");                                                        
                _password2.removeClass("error");
                return false;
            }
        };

        if ($.trim(_password2.val()) == "") {
            //alert(tips[4]);
            s_confirm.html(tips[4]).show();
            _password2.focus();
            s_password.hide();
            gu.hide();
            LcdLogin.hide();
            _lcdEnable.removeClass("error");
            
            s_password.hide();
                        
            //s_confirm.hide();
            _name.removeClass("error");                                                                
            _password1.removeClass("error");                                                        
            _password2.addClass("error");
            //gu.remove();
            return false;
        };

	/*if(check){
            var PasswordLengthError = tips[22];
            if (_password2.val().length < 16) {
                //alert(PasswordLengthError);
                s_confirm.html(PasswordLengthError).show();
                _password2.focus();
                s_password.hide();
                gu.hide();
                LcdLogin.hide();
                _lcdEnable.removeClass("error");
                _name.removeClass("error");                                                             
                _password1.removeClass("error");                                                        
                _password2.addClass("error");
                //gu.remove();
                return false;
            }
            var InputValueError = tips[23];
            if(!(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!"#$%&'()*+,-./:<=>?@[\]^_`{|}~])/g.test(_password2.val()))){  
                //alert(InputValueError);
                s_confirm.html(InputValueError).show();
                _password2.focus();
                s_password.hide();
                gu.hide();
                LcdLogin.hide();
                _lcdEnable.removeClass("error");
                _name.removeClass("error");                                                                
                _password1.removeClass("error");                                                        
                _password2.addClass("error");
                //gu.remove();
                return false;
            }
        }
        else{
            var PasswordLengthError = tips[24];
            if (_password2.val().length < 6 || _password2.val().length > 13) {
                //alert(PasswordLengthError);
                s_confirm.html(PasswordLengthError).show();
                _password2.focus();
                s_password.hide();
                gu.hide();
                LcdLogin.hide();
                _lcdEnable.removeClass("error");
                _name.removeClass("error");                                                             
                _password1.removeClass("error");                                                        
                _password2.addClass("error");
                //gu.remove();
                return false;
            }
            var InputValueError = tips[6];
            if (!(/^(\w){1,14}$/gi.test(_password2.val()))) {
                //alert(InputValueError);
                s_confirm.html(InputValueError).show();
                _password2.focus();
                s_password.hide();
                gu.hide();
                LcdLogin.hide();
                _lcdEnable.removeClass("error");
                _name.removeClass("error");                                                                
                _password1.removeClass("error");                                                        
                _password2.addClass("error");
                //gu.remove();
                return false;
            }
        }*/
      
        if (_password1.val() !== _password2.val()) {
            //alert(tips[1]);
            s_confirm.html(tips[1]).show();
            _password2.focus();
            s_password.hide();
            gu.hide();
            LcdLogin.hide();
            _lcdEnable.removeClass("error");
            _name.removeClass("error");                                                                
            _password1.removeClass("error");                                                        
            _password2.addClass("error");
            //gu.remove()
            return false;
        };	

	var InputValueError = tips[21];
        var LCD_LOGIN_STATUS = document.getElementById("_lcdEnable").checked;
        var LOGIN_LIMIT_STATUS = document.getElementById("_LoginLimit").checked;
        var STRONG_PASSWORD_STATUS = document.getElementById("_strongPass").checked;
        if(LCD_LOGIN_STATUS && (LOGIN_LIMIT_STATUS || STRONG_PASSWORD_STATUS))
        {
            //alert('LCD LOGIN with Account Limit or Complex Password is not allowed'); 
            LcdLogin.html(InputValueError).show();
            //alert(InputValueError1);
            _lcdEnable.focus();
            gu.hide();
            s_password.hide();
            s_confirm.hide();
            _name.removeClass("error");
            _password1.removeClass("error");
            _password2.removeClass("error");
            _lcdEnable.addClass("error");
            return false;
        }

        return true;
    };
 //Check Radius Server Form

    function checkRadiusForm() {
	
	console.log('DEBUG_RADIUS_1');
	var s_r_primary_server = $("div.setting_r_primary_server");
	var s_r_primary_port = $("div.setting_r_primary_port");
	var s_r_secondary_server = $("div.setting_r_secondary_server");
	var s_r_secondary_port = $("div.setting_r_secondary_port_i");
	var s_r_secret_key = $("div.setting_r_secret_key");
	var s_r_confirm = $("div.setting_r_confirm");

	var x = document.getElementById("_radiusenable").checked;
	if(x == true)
	{
        if ($.trim(_pserver.val()) == "") {
            //alert(tips[13]);
            s_r_primary_server.html(tips[13]).show();
            s_r_secondary_server.hide();
            s_r_secret_key.hide();
            s_r_confirm.hide();
            s_r_secondary_port.hide();
            _pserver.focus();           
            _pserver.addClass("error"); 
            			
            _sserver.removeClass("error");                  
            						
            _p2port.removeClass("error");  
            						            
       	    _passwordR.removeClass("error");
            						            			
            _passwordC.removeClass("error");
            
            return false;
        };

     //Check the Valid IP Address For Primary Server 
        var InputValueError = tips[14];
  	  if (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gi.test(_pserver.val()))) {
            //alert(InputValueError);
            s_r_primary_server.html(tips[14]).show();
            s_r_secondary_server.hide();                                                                                                                                                                    
            s_r_secret_key.hide();                                                                                                                                                                          
            s_r_confirm.hide();
            s_r_secondary_port.hide();
            _pserver.focus();
            _pserver.addClass("error"); 
            
            _sserver.removeClass("error");                  
            			
            _p2port.removeClass("error");  
            			            
            _passwordR.removeClass("error");
            			            			
            _passwordC.removeClass("error");
            return false;
        }
        
        
	//Check the Valid IP Address For Secondary Server
	console.log('DEBUG_RADIUS_2');
        var InputValueError1 = tips[15];
        console.log('DEBUG_RADIUS_3');
        
        console.log(_sserver.val());
        
        console.log('DEBUG_RADIUS_4');
	if(_sserver.val()!="" && (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/gi.test(_sserver.val())))) {
        	//alert(InputValueError1);
        	console.log('DEBUG_RADIUS_5');
        	s_r_secondary_server.html(tips[15]).show();
        	s_r_primary_server.hide();                                                                                                                                                                    
        	s_r_secret_key.hide();                                                                                                                                                                          
        	s_r_confirm.hide();
        	s_r_secondary_port.hide();
		_sserver.focus();
		_pserver.removeClass("error"); 
		
                _sserver.addClass("error");                  
					
                _p2port.removeClass("error");  
					            
                _passwordR.removeClass("error");
					            			
                _passwordC.removeClass("error");
		return false;
        }
       
        var InputPortError = tips[17];
        if (_sserver.val()!="" && $.trim(_p2port.val()) == "") {
                 //alert(InputPortError);
                 s_r_secondary_server.hide();
                 s_r_secondary_port.html(tips[17]).show();
                 s_r_primary_server.hide();                                                                                                                                                                  
                 s_r_secret_key.hide();                                                                                                                                                                      
                 s_r_confirm.hide();
                 _p2port.focus();
                 _pserver.removeClass("error"); 
                 
                 _sserver.removeClass("error");                  
                 			
                 _p2port.addClass("error");  
                 			            
                 _passwordR.removeClass("error");
                 			            			
                 _passwordC.removeClass("error");
                 return false;
                 
         };
 	
	//Verify password: 31 digits, and cannot be empty, both passwords must be entered consistently
        var EnterSecret = tips[18];
        if ($.trim(_passwordR.val()) == "") {
            //alert(EnterSecret);
            s_r_secret_key.html(tips[18]).show();
            s_r_secondary_server.hide();
            s_r_primary_server.hide();
            s_r_confirm.hide();
            s_r_secondary_port.hide();
            _passwordR.focus();
            _pserver.removeClass("error"); 
            
            _sserver.removeClass("error");                  
            			
            _p2port.removeClass("error");  
            			            
            _passwordR.addClass("error");
            			            			
            _passwordC.removeClass("error");
            return false;
        }
        var ConfirmSecret = tips[19];
        if ($.trim(_passwordC.val()) == "") {
            //alert(ConfirmSecret);
            s_r_confirm.html(ConfirmSecret).show();
            _passwordC.focus();
            //s_r_secret_key.remove();
            s_r_primary_server.hide();
            s_r_secondary_server.hide();
            s_r_secret_key.hide();
            s_r_secondary_port.hide();
            _pserver.removeClass("error"); 
            
            _sserver.removeClass("error");                  
            			
            _p2port.removeClass("error");  
            			            
            _passwordR.removeClass("error");
            			            			
            _passwordC.addClass("error");
            return false;
        }
        if (_passwordR.val() !== _passwordC.val()) {
            //alert(tips[20]);
            s_r_confirm.html(tips[20]).show();
            _passwordC.focus();
            //s_r_secret_key.remove();
            s_r_secret_key.hide();
            s_r_primary_server.hide();
            s_r_secondary_server.hide();
            s_r_secondary_port.hide();
            _pserver.removeClass("error"); 
            
            _sserver.removeClass("error");                  
            			
            _p2port.removeClass("error");  
            			            
            _passwordR.removeClass("error");
            			            			
            _passwordC.addClass("error");
            return false;
        };
        return true;
 	}
 	else
 	{
 	}
 	return true;
   };
    
    //绑定自定义下拉列表
    Pages.BindSelect();
    
     if(jQuery.cookie("systeminfo")!=1){
        $(".table-container,.set-form").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table-container,.set-form").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
	//提交用户信息
	 function SubmitUserForm() {
		that.SetProcessStart();
		that.SetProcess(Language.Html['011'], "", true);
        var name_privilege = "";
        if (_name.attr("disabled")) {
            name_privilege = "&_name=" + _name.val() + "&_privilege=" + _privilege.attr("data-index");
        }
        var XHR = $.ajax({
            url: _form.attr("action") + "?_=" + new Date().getTime(),
            type: _form.attr("method") ? _form.attr("method") : "GET",
            data: _form.serialize()+ "&_privilege=" + _privilege.attr("data-index") + name_privilege,
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    alert(Language.Html["032"]);
                    return false;
                };
				if (data.status == 98) {
					that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
					return;
				} else {
					if(isNaN(data.status) || data.status >= UserErrors.length){
						data.status = 0;
					}
				}
                if (data.status == 1) {
                    that.SetProcessOK(UserErrors[1]);
                    Control.RefreshModule()
                } else if(data.status == 18) {   
		    that.ErrSetProcessDone(UserErrors[data.status]);                                        
                } else {
                    setTimeout(function () {
                        //that.ErrSetProcessDone(UserErrors[data.status] || UserErrors[0], SubmitUserForm);
                        that.ErrSetProcessDone(UserErrors[data.status] || UserErrors[0]);
                    }, 500);
                }
            },
            error: function (data, textStatus) {
                setTimeout(function () {
                    //that.ErrSetProcessDone(Language.Html['002'], SubmitUserForm);
                    that.ErrSetProcessDone(Language.Html['002']);
                }, 500);
            }
        }).done(function (data, textStatus, jqXHR) {
            jqXHR = null;
            XHR = null;
        });
    };
	


// RADIUS FORM
	function RadiusUserForm() {
	that.SetProcessStart();
 	that.SetProcess(Language.Html['011'], "", true); 
	var check;
	
        check = document.getElementById("_radiusenable").checked;
	var SaveSettings = tips[18];
        var XHR = $.ajax({
             
            url: _rform.attr("action") + "?_=" + new Date().getTime(),
            type: _rform.attr("method") ? _rform.attr("method") : "GET",
            data: _rform.serialize(),

            success: function (data, textStatus, jqXHR) {
            
            
            if (check == true) {
            that.SetProcessOK(UserErrors[13]);
            Control.RefreshModule()
            return;
            }else if (check == false)
             { 
            that.SetProcessOK(UserErrors[14]);
            Control.RefreshModule()
            return;
            } else {
            	if(isNaN(data.status) || data.status >= UserErrors.length){
            		data.status = 0;
            	}
            }	
           },
              error: function (data, textStatus) {
					 alert("Error");
					 alert("Error"+ textStatus);
					 console.log(data);                 
                setTimeout(function () {
                
                    that.SetProcessDone(Language.Html['002'], SubmitRadiusForm);
                }, 500);
            }
        }).done(function (data, textStatus, jqXHR) {
				//$("#SetProgress").hide();
            jqXHR = null;
            XHR = null;
        });
    };

	// Save Radius Server Information
	
	_btnSave.off().on("click",function(){
		if (!checkRadiusForm()) {
            	return false;
        };
		_modify_configure_detail.val(10);
		_rsubmit.val("_rSave");
		RadiusUserForm();
		return false;
      }); 

	//修改用户信息
	_btnModify.off().on("click",function(){
		//check
		if (!checkUserForm()) {
            return false;
        };
		//confirm
		 if (!confirm(tips[7]+ " " + _name.val() + tips[8])) {
			_userFocus.focus();
            return false;
        };
        var LCD_LOGIN_STATUS = document.getElementById("_lcdEnable").checked;
        if(LCD_LOGIN_STATUS)
        {
        //alert("LCD_LOGIN_ENABLED");
	    if(_name.val() == jQuery.cookie("name"))
	        {
	        //alert("MODIFY NAME AND LOGIN NAME SAME");
	        _modify_configure_detail.val(59);
			_submit.val("_remove_users");
			}
		else
			{
			//alert("MODIFY NAME AND LOGIN NAME NOT SAME");
			_modify_configure_detail.val(10);
			_submit.val("_modify_user");
			}
		}
		else
		{
		//alert("LCD_LOGIN_DISABLED");
		_modify_configure_detail.val(10);
		_submit.val("_modify_user");
		}
		//alert("FINAL FORM SUBMISSION");
		SubmitUserForm();
		return false;
	});

	//添加新用户
	_btnAdd.off().on("click",function(){
		//check
		if (!checkUserForm()) {
            return false;
        };
		_modify_configure_detail.val(11);
		_submit.val("_add_user");
		SubmitUserForm();
		return false;
	});

	//删除选中用户
	_btnDelete.off().on("click",function(){
		if(!_userSelected){
			alert(tips[2]);
            return false;
		}
		var userName="";
		$.each($(".radiodiv"),function(){
			if($(this).prop("className")=="radiodiv"){
				 userName=jQuery.evalJSON($(this).attr("value"))["_name"];
			}
		})
		if (!confirm(tips[10]+" " + userName + "?")) {
            return false;
        };
		_modify_configure_detail.val(12);
		_submit.val("_remove_users");
		SubmitUserForm();
		return false;
	});
	
	//检测输入的用记名是否已经存在，存在就只显示修改按钮,否则显示添加按钮
    _name.off().on("keyup focusout", function (event) {
		//循环数据比较name值
		var name = $(this).val();
		var dataList = data.data.content;
		var list = _userList.find("ul");
		for (var i = 0, ilen = dataList.length; i < ilen; i++) {
			//判断列表存在当前输入的用户,退出循环
			if (name == data.data.content[i][1]) {
				if (name == "admin" || name=="engineer") {
					if(event.type=="focusout"){
						//如果用户名为admin、engineer,弹出确认框
						var confirmText = dataList[i][1]=="engineer" ? tips[11] : tips[5];
						 if (confirm(confirmText)) {
							list.eq(i).trigger("click");
							Pages.moveCursorToEnd(_contact[0]);
						} else {
							_name.click();
							$(this).val(name.slice(0,name.length-1));
							setTimeout(function(){
								_name.focus();
								Pages.moveCursorToEnd(_name[0]);
							},1);
						}
					}
				} else {
					//如果为其他用户
					list.eq(i).trigger("click");
				}
				$(this).focus();
				break;
			} else {
				//如果列表不存在输入的用户，则默认添加新用户
				_Deselect();
				_privilege.val(1);
				_ShowAdd();
			}
		}
    });
};

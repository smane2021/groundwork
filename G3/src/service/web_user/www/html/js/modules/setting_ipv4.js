Pages.setting_ipv4 = function (data) {
    var lang = Language["setting_ipv4"];
    var IPV4Errors = lang.errors;
    var tips = lang.tips;
    var that = this;
    Configs.Data.polling = false;
    //$("#_ip").val(Validates.ConvertIP(data.data.content[0]));
    //$("#_mask").val(Validates.ConvertIP(data.data.content[1]));
    //$("#_gate").val(Validates.ConvertIP(data.data.content[2]));
    function IPV4Callback(){
    $("#_btn_ipv4").off().on("click", function () {
        /*check ip;*/
        var _ip = $("#_ip"), _mask = $("#_mask"), _gate = $("#_gate");
        if (!Validates.CheckIP(_ip.val())) {
            alert(tips[1]);
            _ip.focus();
            return false;
        } else if (!Validates.CheckIP(_mask.val())) {
            alert(tips[2]);
            _mask.focus();
            return false;
        } else if (!Validates.CheckIP(_gate.val())) {
            alert(tips[3]);
            _gate.focus();
            return false;
        };
        if (confirm(tips[0])) {
            var _form = $("#settingForm");
            var submitData = _form.serialize();
            that.SetProcess(Language.Html['011']);
            var XHR = $.ajax({
                url: _form.attr("action") + "?_=" + new Date().getTime(),
                type: _form.attr("method") ? _form.attr("method") : "GET",
                data: submitData,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        that.SetProcessDone(Language.Html["032"]);
                        return false;
                    };
					if (data.status == 98) {
						that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
						return;
					} else {
						if(isNaN(data.status) || data.status >= IPV4Errors.length){
							data.status = 0;
						}
					}
                    if (data.status == 1) {
                        that.SetProcessOK(IPV4Errors[data.status]);
                    } else {
                        that.SetProcessDone(IPV4Errors[data.status]);
                        Control.RefreshModule();
                    }
                },
                error: function (data, textStatus) {
                    that.SetProcessDone(Language.Html['002']);
                }
            }).always(function (jqXHR, textStatus, errorThrown) {
                jqXHR = null;
                textStatus = null;
                errorThrown = null;
                XHR = null;
            })
        }
        return false;
    });
    }

    var args = data.args, data = data.data;
    var sessionId = "&sessionId=" + args["sessionId"];
    var language_type = "&language_type=" + args["language_type"];
    
        function GetIPV4Data(target, configure, url, callback) {
        /*configure为数组,分别代表_modify_configure_detail,_modify_configure*/
        if (!document.getElementById(target)) {
            return false;
        };
        var args = arguments;
        Control.SettingOtherXHR = $.ajax({
            url: url + "?_=" + new Date().getTime(),
            type: "get",
            data: "_modify_configure_detail=" + configure[0] + "&_modify_configure=" + configure[1] + sessionId + language_type,
            beforeSend: function () {
                Control.PromptEvent.Loading({ target: target, marginTop: 5 });
            },
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                    if (!(/IE/g.test(Configs.Browser)))
                    {
                          console.log("IPV4 data is");
                          console.log(data);
                    }
                } catch (err) {
                    loadfail(args, true, callback);
                    return false;
                }
                var tmp = doT.templateChild($("#" + target).attr("template"));
                var doTtmpl = doT.template(tmp);
                $("#" + target).html(doTtmpl(data));
                $("#" + target).show();
                if ($.type(callback) == "function") {
                    setTimeout(function () {
                        callback();
                    }, 200);
                };
                that.bindInput();
            },
            error: function (data, textStatus) {
                if (textStatus != "abort") {
                    loadfail(args, false, callback);
                }
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            Control.SettingOtherXHR = null;
        });
    }
    
    function GetOtherData(target, configure, url, callback) {
        /*configure为数组,分别代表_modify_configure_detail,_modify_configure*/
        if (!document.getElementById(target)) {
            return false;
        };
        var args = arguments;
        Control.SettingOtherXHR = $.ajax({
            url: url + "?_=" + new Date().getTime(),
            type: "get",
            data: "_modify_configure_detail=" + configure[0] + "&_modify_configure=" + configure[1] + sessionId + language_type,
            beforeSend: function () {
                Control.PromptEvent.Loading({ target: target, marginTop: 5 });
            },
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    loadfail(args, true, callback);
                    return false;
                }
                var tmp = doT.templateChild($("#" + target).attr("template"));
                var doTtmpl = doT.template(tmp);
                $("#" + target).html(doTtmpl(data));
                $("#" + target + "_set").show();
                if ($.type(callback) == "function") {
                    setTimeout(function () {
                        callback();
                    }, 200);
                };
                that.bindInput();
            },
            error: function (data, textStatus) {
                if (textStatus != "abort") {
                    loadfail(args, false, callback);
                }
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            Control.SettingOtherXHR = null;
        });
    }
    function loadfail(args, format, callback) {
        var loading = $("#" + args[0]).find(".setting_other_loading");
        if (format) {
            loading.html(tips[4] + " " + args[0].replace("Form", "") + " " + tips[6]);
        } else {
            loading.html(tips[5] + " " + args[0].replace("Form", "") + " " + tips[7]);
        };
        loading.addClass("loadingError");
        if ($.type(callback) == "function") {
            callback();
        }
    };
    /*请求DHCP*/
    //GetOtherData("DHCPList", [33, 14], "../cgi-bin/web_cgi_setting.cgi");
    GetOtherData("DHCPList", [33, 14], "../cgi-bin/web_cgi_setting.cgi");
    

	function IPV6Callback(){
		//验证IPV6
		var _btnIPV6 = $("#_btn_ipv6"),
			ipv6 = $("#_ipv6"),
			ipv6Prefix = $("#_ip6_prefix"),
			ipv6Gate = $("#_ipv6_gate"),
			IPV6Form = $("#IPV6Form");
		_btnIPV6.off().on("click",function(){//先解除绑定,再重新绑定 zzc modify
			var ip = ipv6.val(),
				prefix = ipv6Prefix.val();
				//alert(prefix);
				gate = ipv6Gate.val();
			if(ipv6.attr("type")=="text"){
				if(!Validates.CheckIPV6(ip)){
					alert(tips[8]);
					ipv6.focus();
					return false;
				}
			}
			if(ipv6Prefix.attr("type")=="text"){//增加检查前缀长度 zzc modify
				if(prefix=="" || prefix < 0 || prefix > 128){
					alert(tips[9]);
					ipv6Prefix.focus();
					return false;
				}
			}
			if(!Validates.CheckIPV6(gate)){
				alert(tips[10]);
				ipv6Gate.focus();
				return false;
			}
			if (confirm(tips[0])) {
				var submitData = decodeURIComponent(IPV6Form.serialize());
				that.SetProcess(Language.Html['011']);
				var XHR = $.ajax({
					url: IPV6Form.attr("action") + "?_=" + new Date().getTime(),
					type: IPV6Form.attr("method") ? IPV6Form.attr("method") : "GET",
					data: submitData,
					success: function (data, textStatus, jqXHR) {
						try {
							var data = jQuery.evalJSON(data);
						} catch (err) {
							that.SetProcessDone(Language.Html["032"]);
							return false;
						};
						if (data.status == 98) {
							that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
							return;
						} else {
							if(isNaN(data.status) || data.status >= IPV4Errors.length){
								data.status = 0;
							}
						}
						if (data.status == 1) {
							that.SetProcessOK(IPV4Errors[data.status]);
						} else {
							that.SetProcessDone(IPV4Errors[data.status]);
							Control.RefreshModule();
						}
					},
					error: function (data, textStatus) {
						that.SetProcessDone(Language.Html['002']);
					}
				}).always(function (jqXHR, textStatus, errorThrown) {
					jqXHR = null;
					textStatus = null;
					errorThrown = null;
					XHR = null;
				})
			}
			return false;
		});
	}

	//获取IPV6
	GetOtherData("IPV6List", [47, 0], "../cgi-bin/web_cgi_setting.cgi",IPV6Callback);

	//获取ipv6 dhcp
	GetOtherData("IPV6DHCP", [49, 14], "../cgi-bin/web_cgi_setting.cgi");
	
	GetIPV4Data("IPV4List", [0, 0], "../cgi-bin/web_cgi_setting.cgi", IPV4Callback);
	if(jQuery.cookie("systeminfo")!=1){
        $(".table li").css({"padding-right":"70px"});
        $(".table-container,.extbackground").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table li").css({"padding-right":"0px"});
        $(".table-container,.extbackground").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
};
/*---------------------------------*
* Setting页面(5个)所共用的设置函数
* data->渲染当前页面所需数据（格式: {args:{},data:{}},args:{}包涵链接中data={}中的数据，data:{}包涵服务器返回的数据)
* fn->单击设置按钮后，调用的回调函数
*---------------------------------*/
Pages.install_wizard = function (data) {
	var that = this;
	var lang = Language["install_wizard"];
	var InstallWizardErrors = lang["errors"];
	var getSubmitTime = $("#getSubmitTime");
	var _getLocalTime = $("#getCurrentTime");
	var dateTime="";
	_getLocalTime.off().on("click", function (event, param) {
		if (!param) {
			if ($(this).hasClass("btn_2_disabled")) { return false; }
		}
		var now = new Date();
		var current_time = now.getFullYear() + "/" + Validates.MakeupZero(now.getMonth() + 1) + "/" + Validates.MakeupZero(now.getDate()) + " " + Validates.MakeupZero(now.getHours()) + ":" + Validates.MakeupZero(now.getMinutes()) + ":" + Validates.MakeupZero(now.getSeconds());
		
		var times = new Date(current_time).getTime() / 1000 - Configs.timezone * 3600;
		//getSubmitTime.val(current_time);
		
		dateTime={"data":{"present_time":times}};
		Pages.footer(dateTime,getSubmitTime);//根据不同信号值显示不同时间格式
		
		return false;
	});    
	_getLocalTime.trigger("click", [true]);
	if ($.type(getSubmitTime.datetimepicker) == "function") {
		this.SetDateTimePicker(getSubmitTime);
	};
	var time_set = $("a.time_set");
	time_set.off().on("click", function () {
		if (!Validates.CheckDateTime(getSubmitTime,"Ht")) {
		    var timefomat = jQuery.cookie("timeformat");
		    if(timefomat==0){
                alert(Language.Validate['010']);
            }else if(timefomat==1){
                alert(Language.Validate['011']);
            }else{
                alert(Language.Validate['009']);
            }
			return false;
		};
		var ymdtime = Validates.RedDateTime(getSubmitTime.val());
		var times = new Date(ymdtime).getTime() / 1000 - Configs.timezone * 3600;
		dateTime={"data":{"present_time":times}};
		Pages.footer(dateTime,getSubmitTime);//根据不同信号值显示不同时间格式
		that.SetProcess(Language.Html['011'], "", true);
		var _form = $(this).closest("form");
		_submitData = $(this).attr("post") + "&_submitted_time=" + times;
		var XHR = $.ajax({
			url: _form.attr("action") + "?_=" + new Date().getTime(),
			type: _form.attr("method") ? _form.attr("method") : "GET",
			data: _submitData,
			beforeSend: function () {
				that.SetProcessStart();
			},
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					alert(Language.Html["032"]);
					return false;
				};
				if (data.status == 1) {
					/*更新footer的时间*/
					Pages.footer({
						data: {
							present_time: times
						}
					});
					/*Successful*/
					that.SetProcessOK(InstallWizardErrors[1]);
				} else if (data.status == 98) {
					that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
					return;
				} else {
					setTimeout(function () {
						that.SetProcessDone(InstallWizardErrors[data.status]);
					}, 500);
				}
			},
			error: function (data, textStatus) {
				setTimeout(function () {
					that.SetProcessDone(Language.Html['002']);
				}, 500);
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		});
		return false;
	});
	var listheight=mainheight=0;
	$.each($(".table-list"),function(){
	    listheight+=$(this).height();
	});
	mainheight=listheight+28*3;
	//setTimeout('if(mainheight<$("#mainBody").height()){$(".table-container").eq(2).css({"height":$("#mainBody").height()-$(".table-container").eq(1).height()-$(".table-container").eq(0).height()})}',100);
//	if(jQuery.cookie("systeminfo")!=1){
//        $(".table li").css({"padding-right":"60px"});
//        $(".table-container").css({width:'1163px'});
//        $(".table-title").css({width:'1143px'});
//	    $(".table-container .table").css({width:"1143"});
//    }else{
//        $(".table li").css({"padding-right":"0px"});
//        $(".table-container").css({width:'917px'});
//        $(".table-title,.table-container .table").css({width:'897px'});
//    }
};
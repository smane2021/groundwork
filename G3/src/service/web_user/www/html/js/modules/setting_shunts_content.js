Pages.setting_shunts_content  = function (data) {
    var that = this;
        var datalist;
        var SV1,SA2,SF2,SV3,SV4,SV5,SV6,AL7,AR7,SV8,AL9,AR9;//创建参数对象
	//列表模板
	var Set_PowerSystem = $("#Set_ShuntsSystem");
	var listTemplate = doT.templateChild(Set_PowerSystem.attr("template"));
		//打开列表
	function openList(target){
		var datas ={
			"args": data.args,
			"jQuery": jQuery,
			"$" : jQuery,
			"Configs" : Configs,
			"Pages" : Pages,
			"Control" :Control,
			"Validates":Validates,
			"window":window
		};
		var item = target.attr("data").split("|");
		var datalist = data.data[item[0]][item[1]][item[2]].list;
		datas["list"] = datalist;
		var doTtmpl = template.compile(listTemplate)(datas);

		target.html(doTtmpl).show();
		that.Setting(data);
		that.bindInput();
	}
	//打开或隐藏设置列表
	Set_PowerSystem.off('click').on("click","div.table-ex",function(){
		var p = $(this).parent("div.table-container");
		var next = $(this).next("div.table-content");
		next.toggle();
		p.toggleClass("border-bn");
		var span = $(this).find("span");
		var nextHasList = next.attr("data");
		if (next.is(":hidden")) {
			//close
			span.removeClass("title-sl").addClass("title-zk");
			that.DelArrayValue(Configs.PowerSystem, [Number(next.attr("index"))]);
			if(nextHasList){
				next.html("");
			}
		} else {
			//open
			span.removeClass("title-zk").addClass("title-sl");
			Configs.PowerSystem.push(Number(next.attr("index")));
			if(nextHasList){
				openList(next);
			}
		}
	});
	
    $(".table li").css({"padding-right":"0px"});
	var uls = $("div.table-body ul");
	var settemplates = $("#Set_ShuntsSystem").attr("template");
    $(".table-body").on("click", "a.btn_mod", function () {
        var post = $(this).attr("post");
		var parent = $(this).parents("ul");
		var title=parent.parents(".table-list").prev().find("span").text();
		var sdtitle=parent.find(".t-1").find("span").text();
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
        var obj = modifydatas;
        var na4 = $(this).attr("na4");
        var lang = $(this).attr("lang");
		Control.PromptEvent.Pop({ cover: true, title:title+" > "+sdtitle , fn: that.SetShuntInfo, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args }, html: "<div id='SetAlarmLevelRelay' class='Poplevelrelay'></div>", Class: "Pop-shunts-alarm" });
		//统一赋值对象
		 SV1 = $('[name="Signal_Value1"]');
		 SA2 = $('[name="Signal_FullName2"]');
		 SF2 = $('[name="Signal_AbbrevName2"]');
		 SV3 = $('[name="Signal_Value3"]');
         SV4 = $('[name="Signal_Value4"]');
         SV5 = $('[name="Signal_Value5"]');
         SV6 = $('[name="Signal_Value6"]');
         AL7 = $('[name="Alarm_Level7"]');
         AR7 = $('[name="Alarm_Relay7"]');
         SV8 = $('[name="Signal_Value8"]');
         AL9 = $('[name="Alarm_Level9"]');
         AR9 = $('[name="Alarm_Relay9"]');
         var SV = "SV";
         var datatype;
         var strindex;
         var endindex;
         var idstatus = false;
		for (var item in modifydatas) {
            if (item != "toJSONString") {
                datalist = obj[item][7];
                    if(datalist[0][3]==1&&(item=="EIB"||item=="SMDU"||item=="SMDUE"||item=="SMDUP")){
                         $('[name="Signal_FullName2"]').val(datalist[8][6][lang]).prev().prev().text(datalist[8][6][lang]).attr("title",datalist[8][6][lang]);
                         $('[name="Signal_AbbrevName2"]').val(datalist[8][7][lang]).prev().prev().text(datalist[8][7][lang]).attr("title",datalist[8][7][lang]);
                         SF2.attr("Signal_Type",datalist[8][1]);
                         SF2.attr("Signal_ID",datalist[8][2]);
                         SA2.attr("Signal_Type",datalist[8][1]);
                         SA2.attr("Signal_ID",datalist[8][2]);
                     }else if(datalist[0][3]==3&&(item=="EIB"||item=="SMDU"||item=="SMDUE"||item=="SMDUP")){
                         $('[name="Signal_FullName2"]').val( datalist[9][6][lang]).prev().prev().text(datalist[9][6][lang]).attr("title",datalist[9][6][lang]);
                         $('[name="Signal_AbbrevName2"]').val(datalist[9][7][lang]).prev().prev().text(datalist[9][7][lang]).attr("title",datalist[9][7][lang]);
                         SF2.attr("Signal_Type",datalist[9][1]);
                         SF2.attr("Signal_ID",datalist[9][2]);
                         SA2.attr("Signal_Type",datalist[9][1]);
                         SA2.attr("Signal_ID",datalist[9][2]);
                     }else if(datalist[0][3]==4&&(item=="EIB"||item=="SMDU"||item=="SMDUE"||item=="SMDUP")){
                         $('[name="Signal_FullName2"]').val( datalist[10][6][lang]).prev().prev().text(datalist[10][6][lang]).attr("title",datalist[10][6][lang]);
                         $('[name="Signal_AbbrevName2"]').val(datalist[10][7][lang]).prev().prev().text(datalist[10][7][lang]).attr("title",datalist[10][7][lang]);
                         SF2.attr("Signal_Type",datalist[10][1]);
                         SF2.attr("Signal_ID",datalist[10][2]);
                         SA2.attr("Signal_Type",datalist[10][1]);
                         SA2.attr("Signal_ID",datalist[10][2]);
                     }else{
                        //全称
                         $('[name="Signal_FullName2"]').val(obj[item][5][lang]).prev().prev().text(obj[item][5][lang]).attr("title",obj[item][5][lang]);
                         SF2.attr("Signal_Type",obj[item][2]);
                         SF2.attr("Signal_ID",obj[item][3]);
                        //简称
                         $('[name="Signal_AbbrevName2"]').val(obj[item][6][lang]).prev().prev().text(obj[item][6][lang]).attr("title",obj[item][6][lang]);
                         SA2.attr("Signal_Type",obj[item][2]);
                         SA2.attr("Signal_ID",obj[item][3]);
                      }
                      if(item=="SMDUP"){
                        if(datalist[9][3]==2){
                            $(".fillsc").hide()
                        }
                      }
                if (item != "BATTERY") {
                    datatype = 1;
                    strindex = 2;
                    endindex = 6;
                    //设置为
                      var html="";
                      //添加设置选项
                    for (var i = 0; i < datalist[0][9].length; i++) {
                        html += '<li><a href="javascript:void(0)" rel=' + i + '>'+datalist[0][9][i][lang]+'</a></li>';
                    }
                    SV1.attr("data-index",0).text(datalist[0][9][0][lang]);
                    SV1.next().append(html);
                    Pages.BindSelect();
                    //当设置的新值为否的时候隐藏其他选项
                    SV1.next().find("li").on("click",function(){
                        if($(this).find("a").attr("rel")==0){
                           $.each($(this).parent().parent().parent().parent().find("li"),function(index){
                                if(index!=0&&index!=1&&index!=$(this).parent().parent().find("li").length-1&&index!=$(this).parent().parent().find("li").length-2){
                                    $(this).hide();
                                }
                           })
                        }else{
                            if($(this).find("a").attr("rel")==1&&(item=="EIB"||item=="SMDU"||item=="SMDUE"||item=="SMDUP")){
                                 $('[name="Signal_FullName2"]').val(datalist[8][6][lang]).prev().prev().text(datalist[8][6][lang]).attr("title",datalist[8][6][lang]);
                                 $('[name="Signal_AbbrevName2"]').val(datalist[8][7][lang]).prev().prev().text(datalist[8][7][lang]).attr("title",datalist[8][7][lang]);
                                 SF2.attr("Signal_Type",datalist[8][1]);
                                 SF2.attr("Signal_ID",datalist[8][2]);
                                 SA2.attr("Signal_Type",datalist[8][1]);
                                 SA2.attr("Signal_ID",datalist[8][2]);
                             }else if($(this).find("a").attr("rel")==3&&(item=="EIB"||item=="SMDU"||item=="SMDUE"||item=="SMDUP")){
                                 $('[name="Signal_FullName2"]').val( datalist[9][6][lang]).prev().prev().text(datalist[9][6][lang]).attr("title",datalist[9][6][lang]);
                                 $('[name="Signal_AbbrevName2"]').val(datalist[9][7][lang]).prev().prev().text(datalist[9][7][lang]).attr("title",datalist[9][7][lang]);
                                 SF2.attr("Signal_Type",datalist[9][1]);
                                 SF2.attr("Signal_ID",datalist[9][2]);
                                 SA2.attr("Signal_Type",datalist[9][1]);
                                 SA2.attr("Signal_ID",datalist[9][2]);
                             }else if($(this).find("a").attr("rel")==4&&(item=="EIB"||item=="SMDU"||item=="SMDUE"||item=="SMDUP")){
                                 $('[name="Signal_FullName2"]').val( datalist[10][6][lang]).prev().prev().text(datalist[10][6][lang]).attr("title",datalist[10][6][lang]);
                                 $('[name="Signal_AbbrevName2"]').val(datalist[10][7][lang]).prev().prev().text(datalist[10][7][lang]).attr("title",datalist[10][7][lang]);
                                 SF2.attr("Signal_Type",datalist[10][1]);
                                 SF2.attr("Signal_ID",datalist[10][2]);
                                 SA2.attr("Signal_Type",datalist[10][1]);
                                 SA2.attr("Signal_ID",datalist[10][2]);
                             }else{
                                //全称
                                 $('[name="Signal_FullName2"]').val(obj[item][5][lang]).prev().prev().text(obj[item][5][lang]).attr("title",obj[item][5][lang]);
                                 SF2.attr("Signal_Type",obj[item][2]);
                                 SF2.attr("Signal_ID",obj[item][3]);
                                //简称
                                 $('[name="Signal_AbbrevName2"]').val(obj[item][6][lang]).prev().prev().text(obj[item][6][lang]).attr("title",obj[item][6][lang]);
                                 SA2.attr("Signal_Type",obj[item][2]);
                                 SA2.attr("Signal_ID",obj[item][3]);
                              }
                            $(this).parent().parent().parent().parent().find("li").show();
                             if(item=="SMDUP"){
                                if(datalist[9][3]==2){
                                    $(".fillsc").hide()
                                }
                              }
                        }
                    });
                    
                    $('[name="sSignalVal"]').text(SV1.next().find("li").eq(datalist[0][3]).text());
                    SV1.text(SV1.next().find("li").eq(datalist[0][3]).text());
                    SV1.attr("data-index",datalist[0][3]);
                    SV1.attr("Signal_Type",datalist[0][1]);
                    SV1.attr("Signal_ID",datalist[0][2]);
                    SV1.attr("Signal_ValueType",datalist[0][4]);
                    if(datalist[0][3]==0){
                         $.each(SV1.parent().parent().parent().find("li"),function(index){
                                if(index!=0&&index!=1&&index!=$(this).parent().parent().find("li").length-1&&index!=$(this).parent().parent().find("li").length-2){
                                    $(this).hide();
                                }
                           })
                     }
                    LoadAlarmData("other",lang,na4);
                    $(".btn_set").attr("datatype",item);
                } else {
                    datatype = 0;
                    strindex = 3;
                    endindex = 5;
                    $('[name="Signal_Value1"]').parent().parent().hide();//隐藏设置
                    LoadAlarmData("battery",lang,na4);
                    $(".btn_set").attr("datatype",item);
                    Pages.BindSelect();
                }
                
                //循环绑定所有类型信号的参数
                for (var i = datatype; i <= endindex; i++) {
                 //这个循环中index不能等于7,但是i的增长不能变,当i + strindex等于7时不执行第二个if条件并赋值idstatus为true,接下的循环中
                 //将i的值减1,为了避免死循环又添加第一个if条件
                 if (i == endindex && !idstatus) { } else {
                     if (i != endindex - 1) {
                        var index = i + strindex;//设置初始对象,此值不能等于7
                        var liobj = eval(SV + "" + index + "");//拼接对象
                        if (idstatus) { i--; idstatus = false; }//上次循环中i等于最大值减1的时候
                            liobj.prev().prev().text(datalist[i][3]);
                            liobj.val(datalist[i][3]);
                            liobj.prev().text(datalist[i][5]);
                            liobj.next().text(datalist[i][9][0] + " to " + datalist[i][9][1]);
                            liobj.attr("validate", "{'range':[" + datalist[i][9][0] + "," + datalist[i][9][1] + "],'type':"+datalist[i][4]+"}");
                            liobj.attr("Signal_Type",datalist[i][1]);
                            liobj.attr("Signal_ID",datalist[i][2]);
                            liobj.attr("Signal_ValueType",datalist[i][4]);
                      } else { idstatus = true; }
                  }
                }
            }
        }
    }).on("click", "ul,i,a", function () {
       if($(this).hasClass("btn_set_disabled")){
           uls.removeClass("select");
       }else{
	       uls.removeClass("select");
           $(this).addClass("select");
       }
    });
	
function LoadAlarmData(obj,lang,na4) {
        var Ldoindex;
        var Ldbindex;
        var startindex;
        var minval;
        var AL = "AL";
        var AR = "AR";
        if (obj == "other") {
            startindex = 6;
            minval = 1;
        } else {
            startindex = 5;
            minval = 2;
        }
        for (var i = startindex; i <= startindex + 1; i++) {
            if (i == startindex) {
                Ldbindex = i + minval;
            } else {
                Ldbindex = i + minval + 1;
            }
            var liobjAL = eval(AL + "" + Ldbindex + "");//拼接对象
            var liobjAR = eval(AR + "" + Ldbindex + "");//拼接对象
            
            liobjAL.parent().prev().prev().text(liobjAL.next().find("li").eq(datalist[i][4]).text());
           // liobjAL.next().find("li").eq(datalist[i][4]).attr("selected", true);
            liobjAL.attr("data-index",datalist[i][4]).text(liobjAL.next().find("li").eq(datalist[i][4]).text());
            liobjAL.attr("Signal_Type", datalist[i][1]);
            liobjAL.attr("Signal_ID", datalist[i][2]);
            liobjAL.attr("Signal_ValueType", datalist[i][6]);
            var SelNo=0;
            for(var x=0;x<liobjAR.next().find("li").length;x++){
                if(liobjAR.next().find("li").eq(x).find("a").attr("rel")==datalist[i][3]){
                    SelNo++;
                    liobjAR.parent().prev().prev().text(liobjAR.next().find("li").eq(x).text());
                    liobjAR.attr("data-index", liobjAR.next().find("li").eq(x).find("a").attr("rel")).text(liobjAR.next().find("li").eq(x).text());
                    //liobjAR.next().find("li").eq(x).attr("selected", true);
                }
            }
            if(SelNo==0){
                liobjAR.parent().prev().prev().text(na4);
                //liobjAR.next().find("li").eq(0).attr("selected", true);
                 liobjAR.attr("data-index", liobjAR.next().find("li").eq(0).find("a").attr("rel")).text(liobjAR.next().find("li").eq(0).text());
            }
            
        }
    }
    	//刷新时，初始化状态
	var tables = $("div.table-ex");
	for(var i=0,ilen=Configs.PowerSystem.length; i<ilen; i++){
		var next = tables.eq(Configs.PowerSystem[i]).next("div.table-content");
		if(next.attr("data")){
			openList(next);
		}
	}
}


Pages.SetShuntInfo=function(datas){
    var that = Pages;
	var content = $("#SetAlarmLevelRelay");
	content.html(datas.template);
	var info = content.find(".info").addClass("setting");
	var form = content.find("form");
    var inputs = form.find(".set_newval");
    
    $("#SetAlarmLevelRelay input").off("focus blur keydown").on("focus", function () {
		//如果redonly为true，一般为时间设置，即不可以使用backspace
		if (!$(this).attr("readonly")) {
			Configs.Backspace = true;
		}
	}).on("blur", function () {
		Configs.Backspace = false;
	}).on("keydown", function () {
		if ($(this).hasClass("set_value")) {
			Configs.Backspace = true;
		};
	});

    content.find("a.btn_set").off().on("click", function () {
        var Hi1Cur = $(this).attr("hi1cur");
        var Hi2Cur = $(this).attr("hi2cur");
        var Hi1Current = $(this).attr("hi1current");
        var Hi2Current = $(this).attr("hi2current");
        var datatype = $(this).attr("datatype");
		if ($(this).hasClass("btn_set_disabled")) { return false; }
		info.html(Language.Html['011']).addClass("setting");
		$("#PromptPopClose").hide();
		var check = false;
		var vals = "";
		var hi1="";
		var hi2="";
			for (var i = 0, ilen = inputs.length; i < ilen; i++) {
			    var item = inputs.eq(i);
			    var validate = jQuery.evalJSON(item.attr("validate"));
			    var pname = item.attr("pname");
			     var sNewName="";
			    //判断表单素类型
			    if(item[0].nodeName=="DIV"){
			        sNewName=item.attr("data-index");
			    }else{
			        sNewName = item.val();
			    }
			    if(pname=="Signal_FullName2"||pname=="Signal_AbbrevName2"){
			        if(sNewName==""){
			             info.html(Language.Html["032"]);
			             info.removeClass("setting");
			             $("#PromptPopClose").show();
			             return false;
			        }
			    }
			    if(pname=="Alarm_Level7"||pname=="Alarm_Level9"){
			        var valuetype = pname.substring(6, pname.length);
			    }else{
			        var valuetype = pname.substring(7, pname.length);
			    }
			    if(validate!=null){
			        if (!Validates.CheckSetting(inputs, validate, sNewName)) {
			            item.focus();
			            info.html("");
			            info.removeClass("setting");
			            $("#PromptPopClose").show();
                        return false;
                    }
                }
                
                if(datatype=="SMDU"||datatype=="SMDUP"){
			        if(sNewName==""){
			           alert(item.parent().find("span").eq(0).text()+Language.Validate['001']);
			           info.removeClass("setting");
			           $("#PromptPopClose").show();
			           return false;
			        }
			    }
			        
			    if(pname=="Signal_FullName2"||pname=="Signal_AbbrevName2"){//信号名称
				    var iNameLength = 0;
				    for (var iIndex = 0; iIndex < sNewName.length; iIndex++) {
					    if (escape(sNewName[iIndex]).indexOf("%u") != -1) {
						    iNameLength += 3; //Chinese char
					    } else {
						    iNameLength += 1; //Ascii
					    }
				    }
				    if (iNameLength > 32) {
					    check = true;
					    info.html(Language.Html["061"]);
				    }
			    } 
			    if(pname=="Alarm_Relay7"||pname=="Alarm_Relay9"){
			        if(sNewName==""||sNewName==undefined||sNewName==null){
			        	 alert(item.parent().find("span").eq(0).text()+Language.Validate['001']);
			        	 info.removeClass("setting");
			        	 $("#PromptPopClose").show();
			             return false;
			        }
			    }
			    if(i==0){
			        if(sNewName==3){
			            var equip_bt_ID;
			            if(datas.modifydatas.SMDU!=null&&datas.modifydatas.SMDU!=undefined){
			        		 equip_bt_ID = datas.modifydatas.SMDU[7][9][0];
			            }else if(datas.modifydatas.SMDUE!=null&&datas.modifydatas.SMDUE!=undefined){
			        		  equip_bt_ID = datas.modifydatas.SMDUE[7][9][0];  
			            }else{
			                 equip_bt_ID = datas.modifydatas.EIB[7][9][0];
			            }
			            datas.post=datas.post+"&equip_bt_ID="+equip_bt_ID+"";
			        }else{
			            datas.post=datas.post+"&equip_bt_ID=-1";
			        }
			    }
			    if(i==6){
					hi1=sNewName;
			    }
			    if(i==9){
					hi2=sNewName;
					if(Number(hi1)>Number(hi2)){
						info.html(Language.Html["062"]);
						item.focus();
						info.removeClass("setting");
						$("#PromptPopClose").show();
						return false;
					}
			    }
			    //获取语言类型（0：英文、1：中文）
			    var langtype=datas.post.split("&equip_ID")[0].split("=")[1];
                var FullName,AbbrName,High1_Full,High1_Abbr,High2_Full,High2_Abbr;
			    if(sNewName==null||sNewName==undefined||sNewName==""){ vals += "&" + item.attr("pname") + "=" + encodeURIComponent(-1);}else{
			    vals += "&" + item.attr("pname") + "=" + encodeURIComponent(sNewName);
			        if(item.attr("pname")=="Signal_FullName2"){
			             FullName= sNewName;
			        }
			        if(item.attr("pname")=="Signal_AbbrevName2"){
			              AbbrName = sNewName;
			        }
			    }
			   
			    if(pname!="Alarm_Relay7"&&pname!="Alarm_Relay9"){
			        if(item.attr("Signal_Type")==""||item.attr("Signal_Type")==null){item.attr("Signal_Type",-1);}
			        if(item.attr("Signal_ID")==""||item.attr("Signal_ID")==null){item.attr("Signal_ID",-1);}
			        if(item.attr("Signal_ValueType")==""||item.attr("Signal_ValueType")==null){item.attr("Signal_ValueType",-1);}
			        
			         vals+="&"+valuetype+"_SignalType="+item.attr("Signal_Type");
			         vals+="&"+valuetype+"_SignalID="+item.attr("Signal_ID");
			         if(pname!="Signal_FullName2"&&pname!="Signal_AbbrevName2"){
			             vals+="&"+valuetype+"_ValueType="+item.attr("Signal_ValueType");
			         }
			         
			         if(item.attr("pname")=="Signal_Value4"||item.attr("pname")=="Signal_Value5"){
			         if(item.prev().prev().prev().text().indexOf("1")>0)
			         {
								  High1_Full = Hi1Current;
								  High1_Abbr = Hi1Cur;
			         }
			         if(item.prev().prev().prev().text().indexOf("2")>0)
			         {
			             	 	 High2_Full = Hi2Current;
								 High2_Abbr = Hi2Cur;
			         }
			        }
			      }
			  
			  
			}
		vals += "&sessionId=" + datas.args["sessionId"];
	    vals+="&Alarm_High1_Full="+encodeURIComponent(FullName)+" "+encodeURIComponent(High1_Full)+"&Alarm_High1_Abbrev="+encodeURIComponent(AbbrName)+" "+encodeURIComponent(High1_Abbr)+"&Alarm_High2_Full="+encodeURIComponent(FullName)+" "+encodeURIComponent(High2_Full)+"&Alarm_High2_Abbrev="+encodeURIComponent(AbbrName)+" "+encodeURIComponent(High2_Abbr)+"&High1_AbbrevName="+encodeURIComponent(AbbrName)+" "+encodeURIComponent(Hi1Cur)+"&High2_AbbrevName="+encodeURIComponent(AbbrName)+" "+encodeURIComponent(Hi2Cur);
	
		if (check) {
			info.removeClass("setting");
			return false;
		}
		$(this).addClass("btn_set_disabled");
		var $this = $(this);
		var XHR = $.ajax({
			timeout: 30000,
			url: form.attr("action") + "?_=" + new Date().getTime(),
			type: form.type,
			data: datas.post + vals,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["001"]);
					return false;
				}
				if (data.status == 98) {
					info.html(Language.Html['019']);
					return;
				}
				if (data.status == 1) {
				    $(this).addClass("btn_set_disabled");
				    setTimeout(function(){delayload()},5000);
				    info.html(Language.Html['011']);
				}else{
				   info.html(Language.Setting[data.status]);
				    $("#PromptPopClose").show();
				}
			},
			error: function (data, textStatus, errorThrown) {
				info.html(Language.Html['016']).removeClass("setting");
				 $("#PromptPopClose").show();
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
	
	function delayload(){
	info.html(Language.Setting[1]);
	    $("#PromptPopClose").show();
		info.addClass("setting");
		that.SetProcessOK(Language.Setting[1]);
		$("#PromptPopClose").click();
		that.SetAlarmScroll = Configs.MainScrollHeight;

	}
}
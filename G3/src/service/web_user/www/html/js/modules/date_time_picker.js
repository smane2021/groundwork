/*---------------------------------*
 * 调用时间控件
 *---------------------------------*/
Pages.SetDateTimePicker = function (target, data) {
		//IE6、IE7速度太慢，不建议使用时间控件
		if(/IE6|IE7/g.test(Configs.Browser)){ return false; }
        if ($(target).prop("disabled") || $(target).attr("disabled") == "disabled") {
            return false;
        }
        var that = this;
        var DateAndTimeControl = {
            create: function (tp_inst, obj, unit, val, min, max, step) {
                $('<input class="ui-timepicker-input" value="' + val + '" style="width:50%"  readonly="readonly" onFocus="this.blur();">')
			.appendTo(obj)
			.spinner({
			    min: min,
			    max: max,
			    step: step,
			    change: function (e, ui) { // key events
			        // don't call if api was used and not key press
			        if (e.originalEvent !== undefined)
			            tp_inst._onTimeChange();
			        tp_inst._onSelectHandler();
			    },
			    spin: function (e, ui) { // spin events
			        tp_inst.control.value(tp_inst, obj, unit, ui.value);
			        tp_inst._onTimeChange();
			        tp_inst._onSelectHandler();
			    }
			});
                return obj;
            },
            options: function (tp_inst, obj, unit, opts, val) {
                if (typeof (opts) == 'string' && val !== undefined)
                    return obj.find('.ui-timepicker-input').spinner(opts, val);
                return obj.find('.ui-timepicker-input').spinner(opts);
            },
            value: function (tp_inst, obj, unit, val) {
                if (val !== undefined)
                    return obj.find('.ui-timepicker-input').spinner('value', val);
                return obj.find('.ui-timepicker-input').spinner('value');
            }
        };
        var Selected = false;
        var dateFormat="";
        var CurrentYear = new Date().getFullYear();
        var timefomat=jQuery.cookie("timeformat");
        if(timefomat==undefined){
            timefomat=0;
        }
        switch(Number(timefomat)){
            case 0:
                dateFormat ="dd/mm/yy";
            break;
            case 1:
                dateFormat ="mm/dd/yy";
            break;
            case 2:
                dateFormat ="yy/mm/dd";
            break;
        }
        var defaultDatas = {
            controlType: DateAndTimeControl,
            changeMonth: true,
            changeYear: true,
            showSecond: true,
            dateFormat: dateFormat,
            timeFormat: "HH:mm:ss",
            showOn: "button",
            showAnim: "",
            yearRange: CurrentYear - 1 + ":" + (Number(CurrentYear) + 9),
            monthNames: Language.Datetimepicker['monthNames'],
            monthNamesShort: Language.Datetimepicker['monthNamesShort'],
            dayNames: Language.Datetimepicker['dayNames'],
            dayNamesShort: Language.Datetimepicker['dayNamesShort'],
            dayNamesMin: Language.Datetimepicker['dayNamesMin'],
            currentText: Language.Datetimepicker['currentText'],
            closeText: Language.Datetimepicker['closeText'],
            timeText: Language.Datetimepicker['timeText'],
            hourText: Language.Datetimepicker['hourText'],
            minuteText: Language.Datetimepicker['minuteText'],
            secondText: Language.Datetimepicker['secondText'],
            onChangeMonthYear: function (year, month, obj) {
                if (Selected) {
                    return false;
                } else {
                    var Current = obj.input.val().split(" ");
                    var CurrentDate = Current[0].split("/");
                    CurrentDate[0] = year;
                    CurrentDate[1] = month < 10 ? "0" + month : month;
                    CurrentDate[2] = "01";
                   // obj.input.val(CurrentDate.join("/") + " " + Current[1]);
                    switch(Number(timefomat)){
						case 0:
							obj.input.val(CurrentDate[2]+"/"+CurrentDate[1]+"/"+CurrentDate[0] + " " + Current[1]);
						break;
						case 1:
							obj.input.val(CurrentDate[1]+"/"+CurrentDate[2]+"/"+CurrentDate[0] + " " + Current[1]);
						break;
						case 2:
							obj.input.val(CurrentDate.join("/") + " " + Current[1]);
						break;
					}
                }
            },
            onClose: function (data, obj) {
                Selected = false;
                obj.input.val(data);
            },
            onSelect: function (data, obj) {
                Selected = true;
                setTimeout(function () {
                    Selected = false;
                }, 100);
            }
        };
        if ($.type(data) == "object") {
            defaultDatas = $.extend(true, {}, defaultDatas, data);
        }
        $(target).datetimepicker(defaultDatas);
        $(target).off("keyup");

    }
Pages.setting_panel=function(data){
    var that = this;
	var ConsumptionMapList = $(".panel_home");
	var isabsoulte = ConsumptionMapList.attr("isabsoulte");
	var isAbsoluteCenter = isabsoulte ? [true,ConsumptionMapList] : null;
	var maxr = ConsumptionMapList.attr("maxwidth");
	Configs.Popup.Data = {
		list: data.data.element,
		close:true,
		maxr: maxr,
		parent: true,
		isAbsoluteCenter: isAbsoluteCenter
    }
    this.PopupInfo(ConsumptionMapList, "click", ".panelch", ["#popup"], Configs.Popup.Data);
	this.PopupClose("#popup");
}
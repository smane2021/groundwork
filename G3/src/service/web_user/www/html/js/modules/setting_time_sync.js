Pages.setting_time_sync = function (data) {
    Configs.Data.polling = false;
    var lang = Language["setting_time_sync"];
    var SettingTimeErrors = lang["errors"];
    var tips = lang["tips"];
    var that = this;
    var a_time_zone = $("#a_time_zone"),
            _use_time_server = $("[name='_use_time_server']"),
            _time_server1 = $("#_time_server1"),
            _time_server2 = $("#_time_server2"),
            _interval_adjust = $("#_interval_adjust"),
            _select_1 = $("input.select_1"),
            _select_2 = $("input.select_2"),
            _getLocalZone = $("#getLocalZone"),
            _getLocalTime = $("#getLocalTime"),
            _current_date_time = $("#_current_date_time"),
        _form = $("#settingForm");
    /*手动写入数据,因为IP需要处理*/
    _time_server1.val(Validates.ConvertIP(data.data.content[0]));
    _time_server2.val(Validates.ConvertIP(data.data.content[1]));
    _interval_adjust.val(Validates.Atoi(data.data.content[2]));
    a_time_zone.val(Validates.MakeTimeZoneInfo(parseInt(data.data.content[3])));
    if(data.data.content[4]==0){
         _use_time_server.eq(0).removeClass("radiodivnot");
         _use_time_server.eq(1).addClass("radiodivnot");
    }else{
         _use_time_server.eq(1).removeClass("radiodivnot");
         _use_time_server.eq(0).addClass("radiodivnot");
    }
    /*根据focus的位置，改变选择radio*/
    _select_1.off().on("click", function () {
        _use_time_server.eq(1).removeClass("radiodivnot");
        _use_time_server.eq(0).addClass("radiodivnot");
    });
    _select_2.off().on("click", function () {
        _use_time_server.eq(0).removeClass("radiodivnot");
        _use_time_server.eq(1).addClass("radiodivnot");
    });
   _use_time_server.eq(0).off().on("click",function(){
        _use_time_server.eq(0).removeClass("radiodivnot");
        _use_time_server.eq(1).addClass("radiodivnot");
    });
    _use_time_server.eq(1).off().on("click",function(){
        _use_time_server.eq(1).removeClass("radiodivnot");
        _use_time_server.eq(0).addClass("radiodivnot");
    });
    /*getLocalzone*/
    _getLocalZone.off().on("click", function () {
        a_time_zone.val(Validates.MakeTimeZoneInfo(new Date().getTimezoneOffset() * 60));
        return false;
    });
    /*getLocalTime*/
    _getLocalTime.off().on("click", function () {
        var now = new Date();
        var current_time = now.getFullYear() + "/" + Validates.MakeupZero(now.getMonth() + 1) + "/" + Validates.MakeupZero(now.getDate()) + " " + Validates.MakeupZero(now.getHours()) + ":" + Validates.MakeupZero(now.getMinutes()) + ":" + Validates.MakeupZero(now.getSeconds());
      //current_date_time.val(current_time);
        var times = new Date(current_time).getTime() / 1000 - Configs.timezone * 3600;
		var dateTime={"data":{"present_time":times}};
		Pages.footer(dateTime,_current_date_time);//根据不同信号值显示不同时间格式
        return false;
    });
    _getLocalTime.trigger("click");
    /*检测datetimepicker是否已经加载,如果没加载则return;*/
    if ($.type(_current_date_time.datetimepicker) == "function") {
        /*如果可以用插件，设置input为readyonly*/
        /*
        _current_date_time.attr("readonly", true);
        _current_date_time.focus(function () {
        this.blur();
        });
        */
        this.SetDateTimePicker(_current_date_time);
    }
    if(jQuery.cookie("systeminfo")!=1){
        $(".table-container").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table-container").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
    /*submit*/
    $("#_bnt_time").off().on("click", function () {
        if (!jQuery.cookie("nAuthority") || jQuery.cookie("nAuthority") < 3) {
            alert(SettingTimeErrors[4]);
            return false;
        }
        var a_time_zone_value = Validates.ParseTimeZoneInfo(a_time_zone.val());
        if (a_time_zone_value == -1) {
            return false;
        };
        $("#_time_zone").val(a_time_zone_value);
        var _use_time_server_checked ="";
        $.each($('[name="_use_time_server"]'),function(){
            if($(this).prop("className")=="radiodiv"){
                _use_time_server_checked=$(this).attr("value");
            }
        });
        var submitData = _form.serialize() + "&_use_time_server=" + _use_time_server_checked;
        if (_use_time_server_checked == 1) {
            if (!Validates.CheckIP(_time_server1.val(), true)) {
                alert(tips[0]);
                _time_server1[0].select();
                return false;
            }
            if (!Validates.CheckIP(_time_server2.val(), true)) {
                alert(tips[0]);
                _time_server2[0].select();
                return false;
            }
            if (Validates.Atoi(_interval_adjust.val()) <= 0) {
                alert(tips[1]);
                _interval_adjust[0].select();
                return false;
            }
            submitData += "&_modify_configure_detail=8&" + _time_server1.attr("name") + "=" + _time_server1.val() + "&" + _time_server2.attr("name") + "=" + _time_server2.val() + "&" + _interval_adjust.attr("name") + "=" + _interval_adjust.val();

        } else if (_use_time_server_checked == 2) {
            var current = $.trim(_current_date_time.val());
            var spaceNum = 0;
            for (var i = 0, ilen = current.length; i < ilen; i++) {
                if (/ /.test(current[i])) {
                    spaceNum++;
                }
            };
            if (spaceNum > 1) {
                alert(tips[3]);
                return false;
            }
            if (!Validates.CheckDateTime(_current_date_time,"Ht")) {
                alert(Language.Validate['009']);
                _current_date_time.focus();
                return false;
            };
            
            var ymdtime = Validates.RedDateTime(_current_date_time.val());
            var times = new Date(ymdtime).getTime() / 1000 - Configs.timezone * 3600;
		    dateTime={"data":{"present_time":times}};
		    Pages.footer(dateTime,_current_date_time);//根据不同信号值显示不同时间格式
            
            
            /*var current_seconds = new Date(current).getTime();*/
            var timezone = new Date().getTimezoneOffset() / 60;
           // var current_seconds = new Date(current).getTime() / 1000 - timezone * 3600;

            if ((times < new Date("1970/01/01 00:00:00").getTime()) || (times >= new Date("2038/01/01 00:00:00").getTime())) {
                alert(tips[2]);
                return false;
            }
            submitData += "&_modify_configure_detail=7&_submitted_time=" + times;
        }
        that.SetProcess(Language.Html['011'], "", true);
        Send();
        function Send() {
            that.SetProcessStart();
            var XHR = $.ajax({
                url: _form.attr("action") + "?_=" + new Date().getTime(),
                type: _form.attr("method") ? _form.attr("method") : "GET",
                data: submitData,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };                    
					if (data.status == 98) {
						that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
						return;
					} else {
						if (isNaN(data.status) || data.status >= SettingTimeErrors.length) {
							data.status = 0;
						}
					}
                    if (data.status == 1) {
                        if (_use_time_server_checked == 2) {
                            /*更新footer的时间*/
                            Pages.footer({
                                data: {
                                    present_time: times
                                }
                            });
                        }
                        that.SetProcessOK(SettingTimeErrors[data.status]);
                    } else {
                        setTimeout(function () {
                            that.SetProcessDone(SettingTimeErrors[data.status], Send);
                        }, 500);
                    }
                },
                error: function (data, textStatus) {
                    setTimeout(function () {
                        that.SetProcessDone(Language.Html['002'], Send);
                    }, 500);
                }
            }).done(function (data, textStatus, jqXHR) {
                jqXHR = null;
                XHR = null;
            });
        };
        return false;
    });
};
/*---------------------------------*
* Power Split页面回调函数,
* data->渲染当前页面所需数据（格式: {args:{},data:{}},args:{}包涵链接中data={}中的数据，data:{}包涵服务器返回的数据)
*---------------------------------*/
Pages.SetPowerSplit = function (datas) {
	var that = Pages;
	var content = $("#WidgetSetPowerSplit");
	content.html(datas.template);
	var info = content.find(".info").addClass("setting");
	var form = content.find("form");
	var inputs = form.find(".set_value");
	
	//select object changed
	$("#settingForm .input_select").on("click", function () {
	   setoption(this);
	}).trigger("click","tig");
    function setoption(obj){
          var that = $(obj);
	        //Configs.HighLight = null;
		var vList = datas.data.content[3];
		var lang = datas.args["language_type"];
		if ((vList == null) || (lang == null)) {
			return;
		}
		
		var vName = that.attr("name");
		var vValue = that.attr("data-index");

		var oInputs = $("#settingForm .input_select");
		var oEquip = oInputs.eq(0);
		var oSigType = oInputs.eq(1);
		var oSig = oInputs.eq(2);
		var iEquipID = oEquip.attr("data-index");
		var iSigTypeID = oSigType.attr("data-index");
		var iSigID = oSig.attr("data-index");

		if ((vValue != "") && (vValue < 0)) {//set NA
			oEquip.attr("data-index","-1").text("NA");
			oSigType.attr("data-index","-1").text("NA");
			oSig.attr("data-index","-1").text("NA");
			
			return;
		}

		if (vName == "_EquipID") {
			iSigTypeID = "";
			iSigID = "";
		} else if (vName == "_SigTypeID") {
			iSigID = "";
		} else {
			return;
		}

		var vEquipList = [];
		var vSigList = [];
		//get list from data
		for (var i = 0; i < vList.length; i++) {
			vEquipList[i] = vList[i][0]; //get all equipments info
			if ((iSigTypeID != "")
				&& (iSigTypeID >= 0)
				&& (vEquipList[i][0] == iEquipID)) {
				vSigList = vList[i][Number(iSigTypeID) + 1][1]; //get all signals info
			}
		}

		//generate equipment options list
		var vHtml = [];
		vHtml[0] = '<li><a href="javascript:void(0)" rel="">' + Language.Html['056'] + '</a></li>';
		vHtml[1] = '<li><a href="javascript:void(0)" rel="-1">' + Language.Html['057'] + '</a></li>';
		
		
		for (var i = 0; i < vEquipList.length; i++) {
			vHtml[i + 2] = "<li><a href='javascript:void(0)' rel='" + vEquipList[i][0] + "'>" + vEquipList[i][1][lang] + "</a></li>";
		}
		if (vHtml.length > 0) {
			oEquip.next("ul").html(vHtml.join(""));
		}

		//generate signal options list
		vHtml = [];
		vHtml[0] = '<li><a href="javascript:void(0)" rel="">' + Language.Html['056'] + '</a></li>';
		vHtml[1] = '<li><a href="javascript:void(0)" rel="-1">' + Language.Html['057'] + '</a></li>';
		for (var i = 0; i < vSigList.length; i++) {
			vHtml[i + 2] = "<li><a href='javascript:void(0)' rel='" + vSigList[i][0] + "'>" + vSigList[i][1][lang] + "</a></li>";
		}
		if (vHtml.length > 0) {
			oSig.next("ul").html(vHtml.join(""));
		}
		vHtml = [];

		//init value after reseting options list
		oEquip.attr("data-index",iEquipID)
		$.each(oEquip.next().find("li").find("a"),function(){
		    if($(this).attr("rel")==iEquipID){
		        oEquip.text($(this).text());
		    }
		});
		oSigType.attr("data-index",iSigTypeID)
		$.each(oSigType.next().find("li").find("a"),function(){
		    if($(this).attr("rel")==iSigTypeID){
		        oSigType.text($(this).text());
		    }
		});
		oSig.attr("data-index",iSigID)
		$.each(oSig.next().find("li").find("a"),function(){
		    if($(this).attr("rel")==iSigID){
		        oSig.text($(this).text());
		    }
		});
		//避免重复绑定事件
		$(".language li").off("click");
		//新追加的option选项和旧的重新绑定事件
		Pages.BindSelect(setoption);
    }
    //初始化弹出框数据
    if (datas.modifydatas) {
	    for (var i = 0, ilen = inputs.length; i < ilen; i++) {
		    var name = inputs.eq(i).attr("name");
		    //inputs.eq(i).val(datas.modifydatas[name]);
		    $.each(inputs.eq(i).next("ul").find("a"),function(){
		        if($(this).attr("rel")==datas.modifydatas[name]){
		           inputs.eq(i).text($(this).text());
		           inputs.eq(i).attr("data-index",datas.modifydatas[name]);
		        }
		    })
		    //inputs.eq(i).trigger("click","tig");
		    setoption(inputs.eq(i));
	    }
    }
	//submit button
	content.find("a.btn_set").off().on("click", function () {
		if ($(this).hasClass("btn_set_disabled")) { return false; }
		info.html(Language.Html['011']).addClass("setting");
		var check = false;
		var vals = "&_powerSpliteExp=" + datas.modifydatas["OutSigName"];
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
			var item = inputs.eq(i);
			var pname = item.attr("pname");
			if (pname == "_EquipID") {
				if (item.attr("data-index") == "") {
					check = true;
					info.html(Language.Html["058"]);
					break;
				}
			} else if (pname == "_SigTypeID") {
				if (item.attr("data-index") == "") {
					check = true;
					info.html(Language.Html["059"]);
					break;
				}
			} else if (pname == "_SigID") {
				if (item.attr("data-index") == "") {
					check = true;
					info.html(Language.Html["060"]);
					break;
				}
			};

			if (item.attr("data-index") == "-1") {
				vals += encodeURIComponent("\t") + "NA";
			} else {
			    vals += encodeURIComponent("\t") + item.attr("data-index");
			}
		};

		vals += "&sessionId=" + datas.args["sessionId"];
		if (check) {
			info.removeClass("setting");
			return false;
		}
		$(this).addClass("btn_set_disabled");
		var $this = $(this);
		var XHR = $.ajax({
			timeout: 10000,
			url: form.attr("action") + "?_=" + new Date().getTime(),
			type: form.type,
			data: datas.post + vals,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["001"]);
					return false;
				}
				if (data.status == 98) {
					info.html(Language.Html['019']);
					return;
				}
				if (data.status == 1) {
					info.addClass("setting");
					that.SetProcess("");
					that.SetProcessOK(Language.Setting[data.status]);
					$("#PromptPopClose").click();
					that.SetAlarmScroll = Configs.MainScrollHeight;
					Control.RefreshModule();
				}
				info.html(Language.Setting[data.status]);
			},
			error: function (data, textStatus) {
				info.html(Language.Html['016']).removeClass("setting");
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			$this.removeClass("btn_set_disabled");
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
};

Pages.setting_power_split = function (data) {
    Pages.BindSelect();
	var that = this;
	var settemplates = $("#SetPowerSplit").attr("template");
	$("#SetAlarmList>div.table-body").on("click", "a.btn_set", function () {
		var post = $(this).attr("post");
		var parent = $(this).parents("ul");
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
		Control.PromptEvent.Pop({ cover: true, title: parent.find("li.t-2").text(), fn: that.SetPowerSplit, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args, data: data.data }, html: "<div id='WidgetSetPowerSplit' class='Poplevelrelay'></div>", Class: "Pop-power-split" });
	}).on("click", "ul,i,a", function () {
		var ul = $(this).closest("ul");
		Configs.HighLight = ul.attr("id");
		ul.siblings().removeClass("select").end().addClass("select");
	});
	//for set mode button
	$("#BtnSetMode").off().on("click", function () {
		var $this = $(this);
		var _trap = $("#_powerSpliteMode");
		var _trap_level = _trap.attr("data-index");
		var _trap_level_text = _trap.text();
		var submitData = "_modify_configure=10&_modify_configure_detail=26&_powerSpliteMode=" + _trap_level + "&sessionId=" + data.args["sessionId"] + "&language_type=" + data.args["language_type"];
		var _form = $this.closest("form");
		that.SetProcess(Language.Html['011']);
		var XHR = $.ajax({
			url: _form.attr("action") + "?_=" + new Date().getTime(),
			type: _form.attr("method") ? _form.attr("method") : "GET",
			timeout: 10000,
			data: submitData,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["032"]);
					return false;
				};
				data.status = $.trim(data.status);
				if (data.status == 98) {
					that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
					return;
				} else {
					if(isNaN(data.status) || data.status >= Language.Setting.length){
						data.status = 0;
					}
				};
				if (data.status == 1) {
					//$("#ATL-value").html(_trap_level_text);
					that.SetProcessOK(Language.Setting[data.status]);
					Control.RefreshModule();
				} else {
					that.SetProcessDone(Language.Setting[data.status]);
				}
			},
			error: function (data, textStatus) {
				that.SetProcessDone(Language.Html['016']);
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		});
		return false;
	});
	$("#" + Configs.HighLight).addClass("select");
	//Configs.Data.polling = false;
	setTimeout(function () {
		if (document.getElementById("SetAlarmContent")) {
			$("#mainBody").scrollTop(that.SetAlarmScroll);
		}
	}, 10);
	if(jQuery.cookie("systeminfo")!=1){
        $(".table-container,.power-split-summary").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
	    $(".table li").css({"padding-right":"50px"});
    }else{
        $(".table-container,.power-split-summary").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
        $(".table li").css({"padding-right":"0px"});
    }
	return false;
};


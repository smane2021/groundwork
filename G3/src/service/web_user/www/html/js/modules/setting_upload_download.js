Pages.setting_upload_download = function (data) {
    var that = this;
	var lang = Language["setting_upload_download"];
    var AutoConfigurationErrors = lang["errors_config"];
	var UploadDownloadErrors = lang["errors_up_down"];
	var tips = lang["tips"];
    $("#btn_auto_configuration").off().on("click", function () {
        var $this = $(this);
        if (jQuery.cookie("nAuthority") >= 3) {
            if (!confirm(tips[0])) {
                return false;
            };
            var _form = $("#FormAutoConfig");
            that.SetProcess(Language.Html['011']);
            var TestOline = $.ajax({
                url: "../template/isOnline.html?_=" + new Date().getTime(),
                success: function (data, textStatus, jqXHR) {
                    var XHR = $.ajax({
                        url: _form.attr("action") + "?_=" + new Date().getTime(),
                        type: _form.attr("method") ? _form.attr("method") : "GET",
                        timeout: 5000,
                        data: _form.serialize(),
                        success: function (data, textStatus, jqXHR) {
                            /*成功时没有任何返回,等待5秒如果还没有返回值，认为就成功了reboot*/
                            try {
                                var data = jQuery.evalJSON(data);
                            } catch (err) {
                                alert(Language.Html["032"]);
                                return false;
                            };
                            
							if (data.status == 98) {
								that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
								return;
							} else if (data.status == 8) {
								alert(AutoConfigurationErrors[3]);
								return;
							} else {
								if (isNaN(data.status) || (data.status >= AutoConfigurationErrors.length)) {
									data.status = 0;
								}
							}
                            if (data.status == 1) {
                                /*ACU将会重启*/
                                that.DoRestart();
                            } else {
                                that.SetProcessDone(AutoConfigurationErrors[data.status]);
                                Control.RefreshModule();
                            }
                        },
                        error: function (data, textStatus) {
                            that.DoRestart();
                            /*
                            clearTimeout(ACURestartTimer);
                            that.SetProcessDone(Language.Html['002']);
                            */
                        }
                    }).always(function (jqXHR, textStatus, errorThrown) {
                        jqXHR = null;
                        textStatus = null;
                        errorThrown = null;
                        XHR = null;
                    });
                },
                error: function (data, textStatus) {
                    that.SetProcessDone("network error");
                }
            }).always(function (jqXHR, textStatus, errorThrown) {
                jqXHR = null;
                textStatus = null;
                errorThrown = null;
                TestOline = null;
            });
        } else {
            alert(AutoConfigurationErrors[3]);
        }
        return false;
    });
    $("#btn_restore").off().on("click", function () {
        if (confirm(tips[1])) {
            SendForm(10, tips[2]);
        };
        return false;
    });
    $("#btn_reboot1").off().on("click", function () {
        if (confirm(tips[3])) {
            SendForm(11, tips[4]);
        };
        return false;
    });
    if(jQuery.cookie("systeminfo")!=1){
        $(".table-container").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table-container").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
    function SendForm(type, text) {
        var form = $("#RestoreDefault");
        var posts = "equip_ID=-2&signal_type=-2&signal_id=-2&control_value=-2&control_type=" + type + "&sessionId=" + data.args["sessionId"] + "&language_type=" + data.args["language_type"];
        that.SetProcess(text);
        var XHR = $.ajax({
            timeout: 10000,
            url: form.attr("action") + "?_=" + new Date().getTime(),
            data: posts,
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    that.SetProcessDone(Language.Html["001"]);
                    return false;
                }
                if (data.status == 98) {
                    that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
                } else {
                    that.DoRestart();
                }
            },
            error: function (data, textStatus) {
                that.DoRestart();
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            XHR = null;
        });
    };

    var StopControllerError = lang["errors_stop"];
    $("#btn_stop").off().on("click", function () {
        var form = $(this).closest("form");
        var posts = "_close_acu=1&sessionId=" + data.args["sessionId"] + "&language_type=" + data.args["language_type"];
        that.SetProcess(tips[5]);
        var XHR = $.ajax({
            timeout: 10000,
            type: "POST",
            url: form.attr("action") + "?_=" + new Date().getTime(),
            data: posts,
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    that.SetProcessDone(Language.Html["001"]);
                    return false;
                }
                if (data.status == 98) {
                    that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
                } else if (data.status == 1) {
                    that.SetProcessDone(StopControllerError[data.status]);
                    $("#SetAutoConfiguration,#RetrieveFile,#SetRestoreDefault,#SetStopController,#RetrieveDiagnosticPack").hide();
                    $("#SetUploadDownload").fadeIn();
                    Control.StopKeepLogin();
                    Control.StopPollingAlarm();
                    Control.StopPollingTrend();
                    Control.StopPolling();
                    Control.StopReload();
                } else {
                    that.SetProcessDone(StopControllerError[data.status]);
                }
            },
            error: function (data, textStatus) {
                that.SetProcessDone(Language.Html['021']);
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            XHR = null;
        });
        return false;
    });
    $("#_fileselect").off().on("change", function () {
        var value = $(this).val();
        $("#_fileselectvalue").val(value);
        $("#file_name").val(value);
    });
    var errErrSynax = new Array("!", "@", "#", "$", "%", "^", "&", "(", ")", "[", "]", "'", "{", "}", ";", "`");
    function checkValidFile(varFileName) {
        var i = 0;
        while (i < errErrSynax.length) {
            var index = varFileName.indexOf(errErrSynax[i]);
            if (index > 0) {
                return false;
            }
            i++;
        }
        var index = varFileName.indexOf(".tar.gz");
        //var index1 = varFileName.indexOf("app_V");
        //if ((index > 0) && (index1 >= 0)) {
        if (index > 0) {
            return true;
        };
        var index = varFileName.indexOf(".tar");
        //var index1 = varFileName.indexOf("app_V");
        //if ((index > 0) && (index1 >= 0)) {
        if (index > 0) {
            return true;
        };
        var index = varFileName.indexOf("MonitoringSolution.cfg");
        if (index >= 0) {
            return true;
        };
        var index = varFileName.indexOf("SettingParam.run");
        if (index >= 0) {
            return true;
        };
        return false;
    };

    $("#btn_upload").off().on("click", function () {
        var _fileselect = $("#_fileselect").val();
        if (_fileselect == "") {
            alert(tips[6]);
            return false;
        } else if (checkValidFile(_fileselect)) {
            var form = $(this).closest("form");
            $("#filetype").val($("#_uploadfiletype").val());
            if (confirm(tips[7])) {
                that.SetProcess(tips[8]);
                var f = form.submit();
                  /*20分钟没有返回则提示网络连接失败*/
                Configs.UploadTimeout = setTimeout(function () {
                    if ($("#SetUploadDownload")[0] && $("#SetProgress")[0]) {
                        that.SetProcessDone(Language.Html['021']);
                    }
                }, 1200000);
            };
        } else {
            alert(tips[9]);
        }
        return false;
    });
    Pages.BindSelect();
    var downloadURL = function downloadURL(url) {
        var hiddenIFrameID = 'hiddenDownloader',
            iframe = document.getElementById(hiddenIFrameID);
        if (iframe === null) {
            iframe = document.createElement('iframe');
            iframe.id = hiddenIFrameID;
            iframe.style.display = 'none';
            $(iframe).attr("keep", "true");
            document.body.appendChild(iframe);
        }
        iframe.src = url;
    };
    $("#btn_download").off().on("click", function () {
        var form = $(this).closest("form");
        var _uploadfiletype = $("#_uploadfiletype").attr("data-index");
        var _upload_cfg = 0, _upload_lang = 0;
        if (_uploadfiletype == 0) {
            _upload_cfg = 1;
        } else if (_uploadfiletype == 1) {
            _upload_lang = 1;
        }
        var posts = "file_name=0&_start_acu=0&_upload_file=&filetype=&_upload_cfg=" + _upload_cfg + "&_upload_lang=" + _upload_lang + "&filedata0=&sessionId=" + data.args["sessionId"] + "&language_type=" + data.args["language_type"];
        that.SetProcess(tips[10]);
        var XHR = $.ajax({
            timeout: 10000,
            type: "POST",
            url: form.attr("action") + "?_=" + new Date().getTime(),
            data: posts,
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    that.SetProcessDone(Language.Html["001"]);
                    return false;
                }
                if (data.status == 12) {
                    that.SetProcessDone(UploadDownloadErrors[10]);
                } else if (data.status == 10) {
                    downloadURL("/var/download/app_cfg.tar");
                    that.SetProcessRemove();
                } else if (data.status == 11) {
                    downloadURL("/var/download/app_lang.tar");
                    that.SetProcessRemove();
                } else {
                    that.SetProcessDone(UploadDownloadErrors[data.status]);
                }
            },
            error: function (data, textStatus) {
                that.SetProcessDone(Language.Html['021']);
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            XHR = null;
        });
        return false;
    });
    /*reboot*/
    $("#btn_reboot2").off().on("click", function () {
        if (confirm(tips[11])) {
            var form = $(this).closest("form");
            var posts = "file_name=0&_start_acu=1&_upload_file=&filetype=&_upload_cfg=0&_upload_lang=0&filedata0=&sessionId=" + data.args["sessionId"] + "&language_type=" + data.args["language_type"];
            that.SetProcess(tips[12]);
            var XHR = $.ajax({
                timeout: 10000,
                type: "POST",
                url: form.attr("action") + "?_=" + new Date().getTime(),
                data: posts,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        that.SetProcessDone(Language.Html["001"]);
                        return false;
                    }
                    if (data.status == 98) {
                        that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
                    } else {
                        that.DoRestart();
                    }
                },
                error: function (data, textStatus) {
                    that.DoRestart();
                }
            }).always(function (jqXHR, textStatus, errorThrown) {
                jqXHR = null;
                textStatus = null;
                errorThrown = null;
                XHR = null;
            });
        };
        return false;
    });
    var RetrieveErrors = lang["errors_retrieve"];
    $("#btn_retrieve").off().on("click", function () {
        var form = $(this).closest("form");
        var posts = "_modify_configure=11&_modify_configure_detail=27&sessionId=" + data.args["sessionId"] + "&language_type=" + data.args["language_type"];
        that.SetProcess(tips[13]);
        var XHR = $.ajax({
            timeout: 10000,
            type: "POST",
            url: form.attr("action") + "?_=" + new Date().getTime(),
            data: posts,
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    that.SetProcessDone(Language.Html["001"]);
                    return false;
                }
                if (data.status == 98) {
                    that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
                } else if (data.status == 1) {
                    that.SetProcessOK(RetrieveErrors[data.status]);
                    if(jQuery.cookie("code_mode")==0){//MiNi
                        downloadURL("/var/download/SettingParam.run");
                    }
                    if(jQuery.cookie("code_mode")==1){//ESNA
                        downloadURL("/var/download/SettingParam.tar");
                    }
                } else {
                    that.SetProcessDone(RetrieveErrors[data.status]);
                }
            },
            error: function (data, textStatus) {
                that.SetProcessDone(Language.Html['021']);
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            XHR = null;
        });
        return false;
    });

    function formatCurrentDate() {
        var date = new Date();
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!

        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return (yyyy.toString() + mm.toString() + dd.toString());
    };

    function sendQuery(command_parameters, cgi) {
        var XHR = $.ajax({
            timeout: 20000,
            type: "POST",
            async: false,
            url: cgi + "?_=" + new Date().getTime(),
            data: command_parameters + "&sessionId=" + data.args['sessionId'] + "&language_type=" + data.args["language_type"]
        });
    };

    function RetrieveLog() {
        //generate Alarm History log
        sendQuery("equipID=-1&_data_type=2&_timeFrom=1&_timeTo=" + new Date().getTime(), "../cgi-bin/web_cgi_query.cgi");

        //generate Event log
        sendQuery("equipID=0&_data_type=3&_timeFrom=1&_timeTo=" + new Date().getTime(), "../cgi-bin/web_cgi_query.cgi");

        //generate Data History log
        sendQuery("equipID=-1&_data_type=0&_timeFrom=1&_timeTo=" + new Date().getTime(), "../cgi-bin/web_cgi_query.cgi");

        //generate System log
        sendQuery("equipID=0&_data_type=4&_timeFrom=1&_timeTo=" + new Date().getTime(), "../cgi-bin/web_cgi_query.cgi");

        //generate Battery Test log
        sendQuery("equipID=11&_data_type=5&_timeFrom=1&_timeTo=" + new Date().getTime(), "../cgi-bin/web_cgi_query.cgi");

        //create settings file, SettingParam.tar, SettingParam.txt, current alarms file, controller info file
        sendQuery("_modify_configure=20&_modify_configure_detail=58", "../cgi-bin/web_cgi_setting.cgi");

        //Make a tar and download the file.
        var package_file_name = formatCurrentDate() + "_AllLogs.tar";
        var posts = "_upload_logs=1&file_name=" + package_file_name + "&sessionId=" + data.args["sessionId"] + "&language_type=" + data.args["language_type"];
        var XHR = $.ajax({
            timeout: 20000,
            type: "POST",
            async: false,
            url: "../cgi-bin/web_cgi_filemanage.cgi" + "?_=" + new Date().getTime(),
            data: posts,
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    that.SetProcessDone(Language.Html["001"]);
                    return false;
                }
                if (data.status == 20) {
                    downloadURL("/var/download/" + package_file_name);
                    that.SetProcessRemove();
                } else {
                    that.SetProcessDone(Language.Html['021']);
                }
            },
            error: function (data, textStatus) {
                that.SetProcessDone(Language.Html['021']);
            }
        });
    };

    $("#btn_diagnostic").off().on("click", function () {
        that.SetProcess(tips[10]);
        setTimeout(RetrieveLog, 1000);
        return false;
    });
  
    Configs.Data.polling = false;
};
Pages.UploadDownloadCallback = function (data) {
    try {
        var data = jQuery.evalJSON(data);
    } catch (err) {
        this.SetProcessDone(Language.Html["001"]);
        return false;
    }
	var lang = Language["setting_upload_download"];
	var UploadDownloadErrors = lang["errors_up_down"];
    this.SetProcessDone(UploadDownloadErrors[data.status]);
    if (Configs.UploadTimeout) {
        clearTimeout(Configs.UploadTimeout);
        Configs.UploadTimeout = null;
    }
};
Pages.setting_nms = function (data) {
    Configs.Data.polling = false;
	var lang = Language["setting_nms"];
	var nms_errors = lang["errors"];
	var tips = lang["tips"];
	var that = this;
    var args = data.args, data = data.data;
    var sessionId = "&sessionId=" + args["sessionId"];
    var language_type = "&language_type=" + args["language_type"];
    /*切换Trap*/
    $("a.btn_change_trap").on("click", function () {
        var $this = $(this);
        var _trap = $("#_trap_level");
        var _trap_level = _trap.attr("data-index");
        var _trap_level_text = _trap.text();
        var submitData = "_modify_configure=1&_modify_configure_detail=41&_trap_level="+_trap_level+"&sessionId=" + args["sessionId"] + "&language_type=" + args["language_type"];
        var _form = $this.closest("form");
        that.SetProcess(Language.Html['011']);
        var XHR = $.ajax({
            url: _form.attr("action") + "?_=" + new Date().getTime(),
            type: _form.attr("method") ? _form.attr("method") : "GET",
            timeout: 5000,
            data: submitData,
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    that.SetProcessDone(Language.Html["032"]);
                    return false;
                };
                data.status = $.trim(data.status);
				if (data.status == 98) {
					that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
					return;
				} else {
					if (isNaN(data.status) || data.status >= nms_errors.length) {
						data.status = 0;
					}
				}
                if (data.status == 1) {
                    $("#ATL-value").html(_trap_level_text);
                    that.SetProcessOK(nms_errors[data.status]);
                } else {
                    that.SetProcessDone(nms_errors[data.status]);
                }
                _trap.val(0);
            },
            error: function (data, textStatus) {
                    that.SetProcessDone(Language.Html['016']);
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            XHR = null;
        });
        return false;
    });
    /*Do NMS2*/
        GetOtherData("NMS2List", [2, 1], "../cgi-bin/web_cgi_setting.cgi",doNMS2);
    /*Do NMS2 CALLBACK*/
    function doNMS2(data,refresh){
        doList("#NMS2List");
        doForm("#NMS2List","#_ip",data);
        if(!refresh){
            	GetOtherData("NMS3List", [29, 1], "../cgi-bin/web_cgi_setting.cgi",doNMS3);
        }
    };
       
    /*Do NMS3*/
    function doNMS3(data){
        doList("#NMS3List");
        doForm("#NMS3List","#_username",data);
         $("#_trap_enable2").next().find("li").on("click",function(){
            if($(this).find("a").attr("rel")==0){
                $("#_trapip,#_trap_level2,#NMS3_isIPV6").attr("disabled",true).addClass("set_disabled");
            } else { 
                $("#_trapip,#_trap_level2,#NMS3_isIPV6").attr("disabled",false).removeClass("set_disabled");
            }
        });
        $("#NMS3List").find("a.formReset").on("click",function(){
            $("#_trapip").attr("disabled","disabled").addClass("set_disabled");
            $("#_trap_level2").attr("disabled","disabled").addClass("set_disabled");
        });
    }
    function doList(t){
        var t = $(t);
        var form = t.find("form");
		var ipv6_checkbox = form.find("span.nms2-ipv6");
		var NMS3_isIPV6 = $("#NMS3_isIPV6");
        t.find("div.table-body>ul").off().on("click", function () {
            var selected = $(this).find("input").eq(0);
            selected.prop("checked", true);
            form.find("a.modify").show();
            form.find("a.add").hide();
            $(this).siblings().removeClass("selected").end().addClass("selected").find("div").eq(0).removeClass("radiodivnot");
            $(this).siblings().find("div").addClass("radiodivnot").end();
            var data = jQuery.evalJSON($(this).find("div.radiodiv").attr("data"));
            var trap_disabled = false;
            for(var item in data){
                var trap_item = $("#"+item);
                var typpp = trap_item[0].nodeName;
                if(typpp=="DIV"){
                    trap_item.attr("data-index",$.trim(data[item]));
                    $.each(trap_item.next("ul").find("a"),function(){
                        if($(this).attr("rel")==$.trim(data[item])){
                            trap_item.text($(this).text());
                        }
                    });
                }else{
                    trap_item.val($.trim(data[item]));
                }
                
					if(trap_disabled){
						trap_item.attr("disabled",true).addClass("set_disabled");
						NMS3_isIPV6.attr("disabled",true).addClass("set_disabled");
					} else {
						trap_item.attr("disabled",false).removeClass("set_disabled");
						NMS3_isIPV6.attr("disabled",false).removeClass("set_disabled");
				}
                if(item=="_trap_enable2" && data[item]==0){
                trap_disabled = true;
                }
            }
			ipv6_checkbox.hide();
        });
    }
    function doForm(t,firstInput,data){
        var _t = $(t);
        var _form = _t.find("form");
        var _key  = $(firstInput);
        var _uls = _t.find("div.table-body>ul");
        var _btn_delete = _t.find("a.delete");
        var _btn_modify = _form.find("a.modify");
        var _btn_add = _form.find("a.add");
        var _btn_reset = _form.find("a.formReset");
		var ipv6_checkbox = _form.find("span.nms2-ipv6");
        _key.on("keyup",function(){
            klen1=data.content.length;
            if(klen1==0)
            {
                ipv6_checkbox.show();
                //$("#isIPV6").attr("disabled",false);
            }
            var value = $.trim($(this).val());
            for(var k=0,klen=data.content.length;k<klen; k++){
                var currentUL = _uls.eq(k);
                var currentInput = currentUL.find("div.radiodiv");
                if(value==$.trim(data.content[k][1])){
					//修改
					currentUL.addClass("selected").siblings().removeClass("selected");
					currentUL.find("div").removeClass("radiodivnot");
					currentInput.prop("checked", true);
					_btn_modify.show();
					_btn_add.hide();
					//alert("111");
					ipv6_checkbox.hide();
					//$("#isIPV6").attr("disabled",true);
					break;
                } else {
					//添加
					currentUL.removeClass("selected");
					currentUL.find("div").addClass("radiodivnot");
					currentInput.prop("checked", false);
					_btn_modify.hide();
					_btn_add.show();
					ipv6_checkbox.show();
					//$("#isIPV6").attr("disabled",false);
                }
            };
        });
        /*重置表单*/
        _btn_reset.on("click",function(){
            _form[0].reset();
            _uls.removeClass("selected");
            _uls.find("div").addClass("radiodivnot");
            _uls.find("div.radiodiv").prop("checked", false);
            _btn_modify.show();
            _btn_add.show();
            return false;
        });
        var _url = _form.attr("action");
        var _datacommon = "sessionId="+args['sessionId']+"&language_type="+args['language_type']+"&_modify_configure=1";
        var isNMS2 = (t=="#NMS2List") ? true : false;
        var isNMS3 = (t=="#NMS3List") ? true : false;
		var nms2_ipv6 = $("#NMS2_ipv6");
        /*删除*/
        _btn_delete.on("click",function(){
            var _select = _t.find("ul.selected");
            var _data = _datacommon;
            if(!_select[0]){
                alert(tips[0]);
                return false;
            };
            that.SetProcess(Language.Html['011']);
            var itemdata = jQuery.evalJSON(_select.find("div.radiodiv").attr("data"));
            if(isNMS2){
                _data +="&_modify_configure_detail=3&_operate_type="+(nms2_ipv6.val()==1 ? 6 : 3)+"&_submit=&_nms_list=1%3A"+itemdata._ip+"&_ip="+itemdata._ip+"&_public_community="+itemdata._public_community+"&_private_community="+itemdata._private_community+"&_trap_enable="+itemdata._trap_enable;
                SubmitForm(_data,"NMS2");
            } else if(isNMS3) {
                _data +="&_modify_configure_detail=30&_operate_type=3&_submit=&_nms_list=1%3A"+itemdata._username+"&_username="+itemdata._username+"&_trapip="+itemdata._trapip+"&_des_community="+itemdata._des_community+"&_md5_community="+itemdata._md5_community+"&_trap_level="+itemdata._trap_level2+"&_trap_enable="+itemdata._trap_enable2;
                SubmitForm(_data,"NMS3");
            }
            return false;
        });
        /*修改*/
        _btn_modify.on("click",function(){
            doModifyOrAdd(2);
            return false;
        });
        /*添加*/
        _btn_add.on("click",function(){
            doModifyOrAdd(1);
            return false;
        });
         //绑定自定义下拉列表
        Pages.BindSelect();
        function doModifyOrAdd(_operate_type){
            var _data = _datacommon;
            var trap_level=$("#_trap_level2").attr("data-index");
            var itemdata = _form.serialize()+"&_trap_level="+trap_level;
            if(itemdata.indexOf("_username")>-1){
                itemdata+="&_trap_enable="+$("#_trap_enable2").attr("data-index");
            }else{
                itemdata+="&_trap_enable="+$("#_trap_enable").attr("data-index");
            }
            if(jQuery.cookie("nAuthority") < 3){
		        alert(nms_errors[4]);
                return false;
	        };
            var InputErrors ={
                "01":tips[1],
                "02":tips[2],
                "03":tips[3]
            };
            if(isNMS2){
                var ip = $("#_ip"),ipv6 = $("#isIPV6");
                var _ip = $.trim(ip.val());
				var isipv6 = ipv6.prop("checked");
				//添加的时候，判断是否为IPV6
				if(_operate_type==1){
					/*验证ip*/
					if(isipv6){
						if (!Validates.CheckIPV6(_ip)) {
							alert(InputErrors["01"]);//解决弹出undefined问题zzc modify
							ip[0].select();
							return false;
						};
						//添加IPV6地址时，_operate_type改为4
						_operate_type = 4;
					} else {
						if (!Validates.CheckIP(_ip,true)) {
							alert(InputErrors["01"]);//解决弹出undefined问题zzc modify
							ip[0].select();
							return false;
						};
					}
				} else if(_operate_type==2){
					//修改的时候，根据返回的值判断是否为IPV6;
					var isipv6_modidy = (nms2_ipv6.val()==0) ? false : true;
					if(isipv6_modidy){
						_operate_type = 5;
					}
				}
                var _public_community = $("#_public_community");
                var _public_community_v = $.trim(_public_community.val());
                if(!CheckInput(_public_community_v,true) || _public_community_v==""){
                    alert(tips[6]+" "+InputErrors["02"]);
                _public_community[0].select();
                return false;
                };
                var _private_community = $("#_private_community");
                var _private_community_v = $.trim(_private_community.val());
                if(!CheckInput(_private_community_v,true) || _private_community_v==""){
                    alert(tips[7]+" "+InputErrors["02"]);
                _private_community[0].select();
                return false;
                };
                that.SetProcess(Language.Html['011']);
                _data +="&_modify_configure_detail=3&_operate_type="+_operate_type+"&_submit=&"+itemdata;
                if(_operate_type==2){
                _data += "&_nms_list=0%3A"+_ip;
                }
                SubmitForm(_data,"NMS2");
            } else if(isNMS3) {
                var username = $("#_username");
                var _username = $.trim(username.val());
                if(!CheckInput(_username,true) || _username==""){
                    alert(tips[8]+" "+InputErrors["02"]);
                username[0].select();
                return false;
                };
                var _des_community = $("#_des_community");
                var _des_community_v = $.trim(_des_community.val());
                if(!CheckInput(_des_community_v) || _des_community_v==""){
                    alert(tips[9]+" "+InputErrors["03"]);
                _des_community[0].select();
                return false;
                };
                var _md5_community = $("#_md5_community");
                //var _md5_community_v = $.trim(_md5_community.val());
                if(!CheckInput($.trim(_md5_community.val())) || $.trim(_md5_community.val())==""){
                    alert(tips[10]+" "+InputErrors["03"]);
                _md5_community[0].select();
                return false;
                };
                var trap_enable2 = $("#_trap_enable2");
                var _trap_enable2 = trap_enable2.attr("data-index");
                if(_trap_enable2==1){
                    var ip = $("#_trapip");
                    var _ip = $.trim(ip.val());
					var isipv6 = $("#NMS3_isIPV6").prop("checked");

					/*验证ip*/
					if(isipv6){
						if (!Validates.CheckIPV6(_ip)) {
							alert(InputErrors["01"]);//解决弹出undefined问题zzc modify
							ip[0].select();
							return false;
						};
						//添加IPV6地址时，_operate_type改为4
						if(_operate_type==1){
							_operate_type = 5;
						} else if(_operate_type==2){
							_operate_type = 6;
						}
					} else {
						if (!Validates.CheckIP(_ip)) {
							alert(InputErrors["01"]);//解决弹出undefined问题zzc modify
							ip[0].select();
							return false;
						};
					}
                };
                that.SetProcess(Language.Html['011']);
                _data +="&_modify_configure_detail=30&_operate_type="+_operate_type+"&_submit=&"+itemdata;
                if(_operate_type==2){
                _data += "&_nms_list=1%3A"+_username;
                }
                SubmitForm(_data,"NMS3");
            };
            function CheckInput(v,e){
                if(e){
                    if(!(/^(\w){0,16}$/gi.test(v))){
                        return false;
                    };
                }else {
                    if(!(/^(\w){8,16}$/gi.test(v))){
                        return false;
                    };
                }
                return true;
            };
        };
        /*发送修改、添加、删除请求*/
        function SubmitForm(post,target)  {
            that.SetProcessStart();
            var XHR = $.ajax({
                    url: _form.attr("action") + "?_=" + new Date().getTime(),
                    type: _form.attr("method") ? _form.attr("method") : "GET",
                data: post,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };
                    data.status = $.trim(data.status);                    
					if (data.status == 98) {
						that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
						return;
					} else {
						if (isNaN(data.status) || data.status >= nms_errors.length) {
							data.status = 0;
						}
					}
                    if (data.status == 1) {
                        that.SetProcessOK(nms_errors[1]);
                        var tmp = doT.templateChild($("#"+target+"List").attr("template"));
					    var doTtmpl = doT.template(tmp);
                        $("#"+target+"List").html(doTtmpl(data));
                        setTimeout(function(){
                            if(target=="NMS2"){
                                doNMS2(data,true);
                            } else if(target=="NMS3"){
                                doNMS3(data);
                            }
                        },200);
                        if(jQuery.cookie("systeminfo")!=1){
                            $(".table li").css({"padding-right":"50px"});
                            $(".table-container").css({width:'1163px'});
	                        $(".table-container .table,.table-title").css({width:"1143"});
                        }else{
                            $(".table li").css({"padding-right":"0px"});
                            $(".table-container").css({width:'917px'});
                            $(".table-title,.table-container .table").css({width:'897px'});
                        }
                        that.bindInput();
                    } else {
                        that.SetProcessDone(nms_errors[data.status] || nms_errors[0], SubmitForm);
                    }
                },
                error: function (data, textStatus) {
                    that.SetProcessDone(Language.Html['002'], SubmitForm);
                }
            }).done(function (data, textStatus, jqXHR) {
                jqXHR = null;
                XHR = null;
            });
        };
    };
    function GetOtherData(target, configure, url, callback) { /*configure为数组,分别代表_modify_configure_detail,_modify_configure*/
        if(!document.getElementById(target)){
            return false;
        };
        var args = arguments;
        var params = "";
        if(configure[0]==2){
            params = "_modify_configure="+configure[1]+"&_modify_configure_detail="+configure[0]+"&_operate_type=&_submit=&_ip=&_public_community=public&_private_community=private&_trap_enable=0";
        } else if(configure[0]==29){
            params = "_modify_configure="+configure[1]+"&_modify_configure_detail="+configure[0]+"&_operate_type=&_submit=&_username=&_trapip=&_des_community=&_md5_community=&_trap_level=1&_trap_enable=0";
        };
        params += sessionId + language_type;
        Control.SettingOtherXHR = $.ajax({
            url: url + "?_=" + new Date().getTime(),
            data: params,
            beforeSend:function(){
                Control.PromptEvent.Loading({ target: target,marginTop:35 });
            },
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    loadfail(args, true, callback);
                    return false;
                }
                var datas = {
                    data : data,
                    args : args
                };
				/*如果data.status==3，即无SNMPV3功能;*/
				var NMS3 = $("#NMS3");
				if(configure[0]==29){
					if(data.status==3){
						NMS3.hide();
						return;
					}
				}
                var tmp = doT.templateChild($("#"+target).attr("template"));
				var doTtmpl = doT.template(tmp);
                $("#"+target).html(doTtmpl(datas));
                if ($.type(callback) == "function") {
                    setTimeout(function(){
                        callback(data);
                    },200);
                };
                 if(jQuery.cookie("systeminfo")!=1){
                    $(".table li").css({"padding-right":"50px"});
                    $(".table-container").css({width:'1163px'});
	                $(".table-container .table,.table-title").css({width:"1143"});
                }else{
                    $(".table li").css({"padding-right":"0px"});
                    $(".table-container").css({width:'917px'});
                    $(".table-title,.table-container .table").css({width:'897px'});
                }
                that.bindInput();
            },
            error: function (data, textStatus) {
                if(textStatus!="abort"){
                    loadfail(args, false, callback);
                }
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            Control.SettingOtherXHR = null;
        });
    }
    function loadfail(args, format, callback) {
        var loading = $("#" + args[0]).find(".setting_other_loading");
        if (format) {
            loading.html(tips[11]+" " + args[0].replace("Form", "") + " "+tips[4]);
        } else {
            loading.html(tips[11]+" " + args[0].replace("Form", "") + " "+tips[5]);
        };
        loading.addClass("loadingError");
        if ($.type(callback) == "function") {
            callback();
        }
    };
};
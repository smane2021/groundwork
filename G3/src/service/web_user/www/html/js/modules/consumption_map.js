﻿Pages.consumption_map = function (data) {
	var that = this;
	var ConsumptionMapList = $("#ConsumptionMapList");
	var isabsoulte = ConsumptionMapList.attr("isabsoulte");
	var isAbsoluteCenter = isabsoulte ? [true,ConsumptionMapList] : null;
	var maxr = ConsumptionMapList.attr("maxwidth");
	Configs.Popup.Data = {
		list: data.data.element,
		close:true,
		maxr: maxr,
		parent: true,
		isAbsoluteCenter: isAbsoluteCenter
    }
    this.PopupInfo(ConsumptionMapList, "click", "li>a", ["#popup"], Configs.Popup.Data);
	this.PopupClose("#popup");
	//窗口变化时需要重新刷新
	Configs.refresh = true;
	$("#mainBody").css("overflow-x","auto");
};
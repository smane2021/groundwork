﻿Pages.SetAlarmScroll = 0;
Pages.SetLevelOrRelay = function (datas) {
	var that = Pages;
	var content = $("#SetAlarmLevelRelay");
	content.html(datas.template);
	var info = content.find(".info").addClass("setting");
	var form = content.find("form");
	var inputs = form.find(".set_value");
	//初始化弹出框数据
	if (datas.modifydatas) {
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
			var name = inputs.eq(i).attr("name");
			inputs.eq(i).attr("data-index",datas.modifydatas[name]);
			$.each(inputs.eq(i).next("ul").find("a"),function(){
			    if($(this).attr("rel")==datas.modifydatas[name]){
			       inputs.eq(i).text($(this).text())
			    }
			})
		}
	}
	Pages.BindSelect();
	content.find("a.btn_set").off().on("click", function () {
		if ($(this).hasClass("btn_set_disabled")) { return false; }
		info.html(Language.Html['011']).addClass("setting");
		var check = false;
		var vals = "";
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
			var item = inputs.eq(i);
			var pname = item.attr("pname");
			if (pname == "signal_new_level") {
				if (item.attr("data-index") == "") {
					check = true;
					info.html(Language.Html["051"]);
					break;
				}
			} else if (pname == "_alarmReg") {
				if (item.attr("data-index") == "") {
					check = true;
					info.html(Language.Html["052"]);
					break;
				}
			};
			vals += "&" + item.attr("pname") + "=" + item.attr("data-index");
		};
		vals += "&sessionId=" + datas.args["sessionId"] + "&language_type=" + datas.args["language_type"];
		if (check) {
			info.removeClass("setting");
			return false;
		}
		$(this).addClass("btn_set_disabled");
		var $this = $(this);
		var XHR = $.ajax({
			timeout: 10000,
			url: form.attr("action") + "?_=" + new Date().getTime(),
			type: form.type,
			data: datas.post + vals,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["001"]);
					return false;
				}
				if (data.status == 98) {
					info.html(Language.Html['019']);
					return;
				}
				if (data.status == 1) {
					info.addClass("setting");
					that.SetProcessOK(Language.Setting[data.status]);
					$("#PromptPopClose").click();
					that.SetAlarmScroll = Configs.MainScrollHeight;
					Control.RefreshModule();
				}
				info.html(Language.Setting[data.status]);
			},
			error: function (data, textStatus) {
				info.html(Language.Html['016']).removeClass("setting");
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			$this.removeClass("btn_set_disabled");
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
};
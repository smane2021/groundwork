﻿Pages.cabinet_set = function(data){
	var idata = data.data;
	var that= this;
	var isIE6 = /IE6/g.test(Configs.Browser);
	var mainBody = $("#mainBody");
	//初始化缓存数据数据结构
	var cacheData = mainBody.data('cabinet');
	if(!cacheData){
		mainBody.data('cabinet',{
			BranchList: [],
			Branch: {},
			Allocation: {},
			BranchLength: 0,
			Current:null,
			len:0,
			selected:[],
			//构造Cabinet页面布局函数
			CabinetLayout : function(){
				var CabinetBranchContent = $("#CabinetBranchContent");
				var mainBody = $("#mainBody");
				var CabinetBox = $("#CabinetBox");
				mainBodyHeight = mainBody.height();
				CabinetBranchContent.css({ "height": mainBodyHeight - 39 });
				CabinetBox.css({ "height": mainBodyHeight - 82 });
			}
		});
	}
	mainBody.css({"overflow-y":"hidden"});
	//传递过来的data是分支的数据,保存左侧所有分支数据
	var BranchAll = idata.CabinetBranch;
	//右边Map的数据
	var CabinetData = mainBody.data("cabinet");

	//页面布局
	CabinetData.CabinetLayout();
	//禁止轮询
	Configs.Data.polling = false;
	//窗口变化时需要重新刷新
	Configs.refresh = [true,CabinetData.CabinetLayout];

	//获取语言信息
	var lang = Language["cabinet_set"];
	var tips = lang["tips"];
	var CabinetSetError = lang["error"];
	//获取相关DOM,
	var CabinetBox = $("#CabinetBox");
	var ConsumptionMapList = $("#ConsumptionMapList");
	var BoxInfo = $("#BoxInfo");
	var CabinetDetail = $("#CabinetDetail");
	var CabinetArrow = $("#CabinetArrow");
	var CabinetBranchs = $("#CabinetBranch");
	var CabinetDetailContent = $("#CabinetDetailContent");
	var CabinetRefresh = $("#CabinetRefresh");
	//按钮DOM
	var BtnDetail = $("#ShowDetail");
	var BtnDetailClose = $("#BtnCloseDetail");
	var BtnReset = $("#BtnReset");
	var BtnSet = $("#BtnCabinetSet");
	//初始化变量
	var CabinetDataPrefix = "Cabinet ";
	var DuDataPrefix = "DU";
	var mainBodyHeight = mainBody.height();
	var SetURL = "../cgi-bin/web_cgi_setting.cgi";
	var sessionLanguage = "sessionId="+jQuery.cookie("sessionId")+"&language_type="+jQuery.cookie("language_type");
	var scrolltop = 0;
	var scrollleft = 0;
	//==>操作右边code开始

	//右侧滚动时记录滚动高、宽
	CabinetBox.off("scroll").on("scroll",function(){
		scrolltop = $(this).scrollTop();
		scrollleft = $(this).scrollLeft();
	});
	if(jQuery.cookie("systeminfo")!=1){
	    $("#CabinetSet").css({width:"1143"});
	    $(".CabinetBox").css({width:"851"});
    }else{
        $(".table-container").css({width:'917px'});
         $(".CabinetBox").css({width:"620"});
    }
	//Cabinet详细浮动层定位
	function DetailPos(scroll){
		if(!CabinetData.Current){ return; }
		var pos = CabinetData.Current.position();
		var height = CabinetData.Current.outerHeight(true);
		var left = pos.left;
		var top = pos.top;
		var width = CabinetDetail.width();
		var scrollLeft = CabinetBox.scrollLeft();
		if(scrollLeft+440-left<0){
			CabinetBox.scrollLeft(left);
		}
		if(left<scrollLeft){
			CabinetBox.scrollLeft(left);
		}
		if(top<scrolltop || scrolltop+(mainBodyHeight-145)-top<0){
			CabinetBox.scrollTop(top-(mainBodyHeight-80)/2);
		}
		//延迟10ms,以便取到右侧滚动高度
		setTimeout(function(){
			scrollLeft = CabinetBox.scrollLeft();
			CabinetArrow.css("left",left+35-scrollLeft);
			var CabinetDetailHeight = CabinetDetail.height();
			var CabinetDetailTitle = CabinetDetail.find("div.CabinetDetailTitle");
			if(top-scrolltop<CabinetDetailHeight){
				CabinetDetail.css({"left":scrollLeft,"top":top+height+2});
				CabinetDetailContent.before(CabinetDetailTitle.clone().end().remove());
				CabinetArrow.removeClass("CabinetArrow2");
			} else {
				CabinetDetail.css({"left":scrollLeft,"top":top-CabinetDetailHeight-2}).append(CabinetDetailTitle.clone().end().remove());
				CabinetArrow.addClass("CabinetArrow2");
			}
			CabinetDetail.show();
		},10);
	}
	//打开Cabinet详细
	BtnDetail.click(function(){
		var tmp = "";
		var tmpArr =  [];
		var setted = false;
		for(var item in CabinetData.Branch){
			setted = true;
			var otmp = "";
			otmp += "<span "
			if(CabinetData.Branch[item][1]===true){
				//当前选中的已经设置成功
				otmp += ">"+CabinetData.Branch[item][0]+"</span>";
			} else {
				otmp += "class='y' name='"+item+"' title='"+tips[1]+" "+CabinetData.Branch[item]+"'>"+CabinetData.Branch[item]+"<i></i></span>";
			}
			tmpArr.push(otmp);
		};
		//使预设置的分支显示在详细列表的最前面
		tmp +=tmpArr.reverse().join("");
		var noallocation = "<div class='noallocation'>"+tips[6]+"</div>";
		if(CabinetData.BranchList.length>0){
			for(var i=0,ilen=CabinetData.BranchList.length; i<ilen; i++){
				tmp += "<span>"+CabinetData.BranchList[i]+"</span>";
			}
		} else {
			if(!setted){
				tmp = noallocation;
			}
		};
		CabinetDetailContent.html(tmp);
		DetailPos();
		$(this).hide();
		BtnDetailClose.show();
		CabinetDetail.find("span.y>i").off().on("click",function(){
			var p = $(this).parent();
			var name = p.attr("name");
			delete CabinetData.Branch[name];
			$("#"+name).removeClass("disabled").addClass("set");
			//修改选择单元中已经预设的数字显示
			CabinetData.BranchLength--;
			CabinetData.Current.find('span').html(CabinetData.BranchLength==0 ? "" : CabinetData.BranchLength);
			p.remove();
			if(CabinetDetailContent.find("span").length==0){
				CabinetDetailContent.html(noallocation);
			}
			DetailPos();
			return false;
		});
		return false;
	});
	//关闭Cabinet详细
	BtnDetailClose.click(function(){
		CabinetDetail.hide();
		BtnDetailClose.hide();
		BtnDetail.show();
		CabinetBranchs.show();
		return false;
	});
	//重置Cabinet分支
	BtnReset.click(function(){
		if(!confirm(tips[15])){ return false; }
		var ResetPost = sessionLanguage+"&_modify_configure=18&_modify_configure_detail=44&_content="
		ResetPost +="&_cabinet="+CabinetData.Current.attr("index");
		that.SetProcess(tips[9]);
		var XHR = $.ajax({
			timeout: 10000,
			url: SetURL + "?_=" + new Date().getTime(),
			data: ResetPost,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					SelectFail(-1);
					return false;
				}
				if(data.status == 1){
					//取消高亮状态
					CabinetData.Current.removeClass("allocate").addClass("unallocated");
					CabinetData.Current.find("span").html(0).hide();
					CabinetData.Current.find("em").addClass("hc");
					//提示成功，等待10秒后刷新当前页面
					that.DoRestart(false,10,tips[10],function(){
						that.SetProcessRemove();
						Control.RefreshModule();
					});
				} else {
					SelectFail(data.status);
				}
			},
			error: function (data, textStatus) {
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
	//弹出Cabinet参数设置浮动层
	$("li>a",ConsumptionMapList).off().on("click",function(){
		var currentItem = $(this).closest("li");
		var index = currentItem.attr("index")-1;
		var datas = idata.element[index];
		var name = datas[2];
		var list = [name,datas[10].replace("%",""),datas[11].replace("%","")];
		var template = CabinetBox.attr("template")
		function doCabinetParam(d){
			var CabinetParam = $("#CabinetParam");
			var template = doT.templateChild(d.template);
			var htmlDatas = doT.template(template)(d);
			CabinetParam.html(htmlDatas);
			Pages.bindInput();
			var btn = CabinetParam.find("a.btn_set");
			var setTips = $("#SetTips");
			var close = $("#PromptPopClose");
			btn.show().off().on("click",function(){
				if($(this).hasClass("btn_set_disabled")){ return false; }
				var posts = sessionLanguage+"&_modify_configure=18&_modify_configure_detail=45&_cabinet="+(index+1)+"&_content="
				var pd = CabinetParam.find("input[name='_content']");
				//一些验证
				for(var i=0;i<pd.length;i++){
					var pdi = pd.eq(i)
					var val = $.trim(pdi.val());
					if(i==0){
						if(val.length==0 || val.length>16 || !/^[A-Za-z0-9_ ]+$/g.test(val)){
							pdi.select();
							setTips.html(tips[12]);
							return false;
						}
					} else {
						var vals = val.split(".");
						if(val<0 || val>100 || val.length==0 || isNaN(val) || (vals[1] && vals[1].length>1)){
							pdi.select();
							setTips.html(tips[13]);
							return false;
						}
					}
					posts += val+";"
				}
				if(Number(pd.eq(1).val())>Number(pd.eq(2).val())){
					setTips.html(tips[13]);
					pd[1].select();
					return false;
				};
				//验证通过，执行设置
				setTips.html(Language.Html["011"]);
				$(this).addClass("btn_set_disabled");
				var that = $(this);
				close.hide();
				var XHR = $.ajax({
					timeout: 10000,
					url: SetURL + "?_=" + new Date().getTime(),
					data: posts,
					success: function (data, textStatus, jqXHR) {
						try {
							var data = jQuery.evalJSON(data);
						} catch (err) {
							setTips.html(Language.Html["001"]);
							return false;
						}
						if(data.status == 98){
							setTips.html(Language.Html['019']);
						} else if(data.status==1){
							//setTips.html(CabinetSetError[1]);
							for(var i=0;i<pd.length;i++){
								var percent = i>0 ?" %" : "";
								pd.eq(i).prev().html(data.content[i]+percent).attr('title',data.content[i]+percent);
							}
							//修改缓存数据
							datas[2] = data.content[0];
							datas[10] = data.content[1]+percent;
							datas[11] = data.content[2]+percent;
							//实时修改名称
							currentItem.find("em").html(datas[2]);
							close.click();
							Pages.SetProcessOK(CabinetSetError[1],1000);
						} else {
							setTips.html(CabinetSetError[data.status]);
						}
					},
					error: function (data, textStatus) {
						setTips.html(CabinetSetError[0]);
					}
				}).always(function (jqXHR, textStatus, errorThrown) {
					jqXHR = null;
					textStatus = null;
					errorThrown = null;
					XHR = null;
					that.removeClass("btn_set_disabled");
					close.show();
				})
				return false;
			});
		}
		Control.PromptEvent.Pop({ 
			cover: true, 
			title: name, 
			fn: doCabinetParam,
			data: { 
				template: template,
				list:list,  
				args: data.args
			},
			html: "<div id='CabinetParam' class='PopCabinet'></div>",
			Class: "PopSetCabinet" 
		});
		return false;
	});
	//取消预设置分支的确认函数
	function SetConfirm(){
		if(CabinetData.Current){
			var number = Number(CabinetData.Current.find("span").text() || 0);
			if(number>0){
				if(!confirm(tips[3]+" "+(number-CabinetData.len)+" "+tips[4])){
					return false;
				}
			}
		}
		return true;
	};
	//选择Cabinet失败回调函数
	function SelectFail(num){
		that.SetProcess("",null,null,function(){
			CabinetRefresh.hide();
		});
		if (num == 98) {
			that.SetProcessDone(Language.Html['019']);
		} else if(num==-1){
			that.SetProcessDone(Language.Html["001"]);
		} else {
			that.SetProcessDone(CabinetSetError[num]);
		}
	};
	var SelectXHR = null;
	//Cabinet绑定选择操作
	ConsumptionMapList.on("click","li",function(event){
		var $this = $(this);
		var configNum = $this.find("span").text();
		if(configNum!="" && (CabinetData.BranchLength>CabinetData.len || CabinetData.BranchLength!=CabinetData.len)){
			//判断是否已经设置了，取消时提示
			if(!SetConfirm()){ 
				return false; 
			} else {
				CabinetData.BranchList = [];
			}
		}
		var index = $this.attr("index");
		var item = idata["element"][index-1][2];
		//发送选择post
		var p = $this.position();
		CabinetRefresh.css({"left":p.left,"top":p.top}).show();
		//当前cabinet序号
		var PostData = sessionLanguage +"&_modify_configure=18&_modify_configure_detail=43&_content=&_cabinet="+index;
		if(SelectXHR!=null) { SelectXHR.abort(); }
		SelectXHR = $.ajax({
			timeout: 10000,
			url: SetURL + "?_=" + new Date().getTime(),
			data: PostData,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					SelectFail(-1);
					return false;
				}
				if(data.status == 1){
					CabinetRefresh.hide();
					//发送成功，变化选中状态
					$this.siblings().removeClass("selected selected-ie6");
					var claName = "selected"
					if(/IE6/.test(Configs.Browser) && $this.hasClass("unallocated")){
						claName = "selected-ie6";
					}
					$this.toggleClass(claName);

					//已经选择的分支清空
					CabinetData.Branch = {};
					//已经选择的长度归0
					CabinetData.BranchLength = data.content.length;
					CabinetData.len = data.content.length;
					CabinetBranchs.find("a.disabled").removeClass("disabled").addClass("set");
					if($this.hasClass(claName)){
						//that.SetProcessOK(tips[0]);
						BoxInfo.show().find("span").html(item).end().find("i").html(data.content.length);
						CabinetData.Current = $this;
						BtnDetail.show();
						BtnReset.show();
						CabinetData.BranchList = data.content;
						if(data.content.length>0){
						    CabinetData.Current.find("span").html(data.content.length).show();
						    CabinetData.Current.find("em").removeClass("hc");
						    CabinetData.Current.removeClass("unallocated").addClass("allocate");
							if(/IE6/.test(Configs.Browser)){
								CabinetData.Current.removeClass(claName).addClass("selected")	
							}
						}else {
						    CabinetData.Current.find("span").html(data.content.length).hide();
						    CabinetData.Current.find("em").addClass("hc");
						    CabinetData.Current.removeClass("allocate").addClass("unallocated");
							if(/IE6/.test(Configs.Browser)){
								CabinetData.Current.removeClass(claName).addClass("selected-ie6")	
							}
						}
					} else {
						//that.SetProcessOK(tips[8]);
						BtnDetailClose.hide();
						BtnDetail.hide();
						BtnReset.hide();
						BoxInfo.hide();
						CabinetData.Current = null;
						CabinetDetail.hide();
						CabinetBranchs.show();
					}
					if(CabinetDetail.is(":visible") && CabinetData.Current){
						BtnDetail.click();	
					}
				} else {
					SelectFail(data.status);
				}
			},
			error: function (data, textStatus) {
				if (textStatus == "abort") { return false; }
				SelectFail(0);
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			SelectXHR = null;
		})
	});
	//设置按钮
	BtnSet.off().on("click",function(){
		if(CabinetData.BranchLength == CabinetData.len){
			that.SetProcessDone(tips[7]);
			return false;
		}
		var SetPost = sessionLanguage+"&_modify_configure=18&_modify_configure_detail=42&_content="
		var tmp = "";
		var hasSet = [];
		for(var i=0;i<CabinetData.BranchList.length; i++){//CabinetData.BranchList为原来存在的支路信息
			var BranchItem = CabinetData.BranchList[i].replace(DuDataPrefix,"").split(".");
			var Cabinet = CabinetDataPrefix+BranchItem[0];
			var Du = DuDataPrefix+BranchItem[0]+"."+BranchItem[1];
			if(BranchAll[Cabinet] && BranchAll[Cabinet][Du] && BranchAll[Cabinet][Du][1][BranchItem[2]-1]){
				var index = BranchAll[Cabinet][Du][1][BranchItem[2]-1];
				hasSet.push(index[0]+","+index[1]+","+BranchItem.join(","));//设备ID,信号ID,cabinetID,DUID,indix
			}
		}
		if(hasSet.length>0){
			SetPost+= hasSet.join(";")+";"
		}
		var branch = CabinetData.Branch;//新增加的支路信息
		for(var item in branch){
			tmp += item.replace(/_/g,",")+";";
		};
		SetPost +=tmp;
		
		SetPost +="&_cabinet="+$(CabinetData.Current).attr("index");
		that.SetProcess(Language.Html['011']);
		var XHR = $.ajax({
			timeout: 10000,
			type:"get",
			url: SetURL + "?_=" + new Date().getTime(),
			data: SetPost,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					SelectFail(-1);
					return false;
				}
				if(data.status == 1){
					that.SetProcessOK(CabinetSetError[1]);
					//对已经设置过的缓存的数据进行处理,所有值都改为数据，并第二个数为true;
					//相对应的单击show detail按钮时，判断是否为true,为true则显示实线框
					//如果页面重新刷新，后端会返回一个数值，告知某个cabinet是否已经分配过
					for(var item in CabinetData.Branch){
						if($.type(CabinetData.Branch[item])!="array"){
							CabinetData.Branch[item] = [CabinetData.Branch[item],true];
						}

					};
					//如果显示在设置详细页面，则重新触发一次btn_show单击
					if(BtnDetail.is(":hidden")){
						BtnDetail.trigger("click");
					}
					//修改选择框设置的分支数
					var BoxInfoNum = BoxInfo.find("i");
					BoxInfoNum.html(data.content.length);
					//重置设置个数
					CabinetData.BranchLength = data.content.length;
					CabinetData.len = data.content.length;
					//左侧disabled改为location,表示已经设置了
					CabinetBranchs.find("a.disabled").removeClass("disabled").addClass("allocation");
					//已经设置的归到数据Allocation中
					$.extend(true,CabinetData.Allocation, CabinetData.Branch);
					//如果未分配的设置为已经分配，添加allocate样式
					if($(CabinetData.Current).hasClass("unallocated")){
						$(CabinetData.Current).removeClass("unallocated").addClass("allocate")
					}
				} else {
					SelectFail(data.status);
				}
			},
			error: function (data, textStatus) {
				SelectFail(0);
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
	

	//==>操作左边code开始
	var selectCabinet = $("#selectCabinet");
	var selectDu = $("#selectDu");
	var CabinetBranchContent = $("#CabinetBranchContent");
	var cabinet = "";
	var du = "";

	//左侧Cabinet单击事件
	var BindCabinetClick = function(){
		CabinetBranchContent.find('span.c').off().on("click",function(){
			var name = $(this).attr("value");
			$.each(selectCabinet.next().find("p"),function(){
			    if($(this).attr("rel")==name){
			        selectCabinet.text($(this).text());
			    }
			});
			selectCabinet.attr("data-index",name).next().find("li").trigger("click");
		});
	}
	BindCabinetClick();
	//Cabinet下拉框事件
	selectCabinet.next().find("li").on("click", function (event) {
		 var oElem = event.target ? event.target : event.srcElement;
	     if(oElem.nodeName=="P"){
           		$(this).parent().prev(".input_select").text($(this).text()).attr("data-index",$(this).find("p").attr("rel"));
			    $(this).parents(".language").slideUp(10).addClass("yinc");
			    $(this).parents(".language").find("li").slideUp(10).addClass("yinc");
           		cabinet = $(this).find("p").attr("rel");
           }else{
           		cabinet = selectCabinet.attr("data-index");
           }
		CabinetData.selected[0] = cabinet;
		var tmp = "";
		var option = "<li><p rel='-1'>"+tips[5]+"</p></li>";
		if(cabinet==-1){
			for(var item in BranchAll){
				tmp += "<span class='c' value='"+item+"'>"+BranchAll[item]["name"]+"</span>";
			};
			CabinetBranchContent.html(tmp);
			BindCabinetClick();
			selectDu.next().html(option).end().text(tips[5]).attr("-1");
			return;
		}
		var list = BranchAll[cabinet];
		for(var item in list){
			if(item=="name"){ continue; }
			option += "<li><p rel='"+item+"'>"+list[item][0]+"</p></li>";
			tmp += "<span class='d' value='"+item+"'>"+list[item][0]+"</span>";
		};
		selectDu.text(tips[5]).attr("data-index",-1);
		selectDu.next().html(option);
		//DU下拉框事件
        selectDu.next().find("li").on("click",function(e){
            allocationDu(this,e);
        });
		CabinetBranchContent.html("");
		CabinetBranchContent.html(tmp);
		CabinetBranchContent.find('span.d').off().on("click",function(){
			var name = $(this).attr("value");
			$.each(selectDu.next().find("p"),function(){
			    if($(this).attr("rel")==name){
			        selectDu.text($(this).text());
			    }
			});
			selectDu.attr("data-index",name).next().find("li").trigger("click");
		});
		InitDu();
		return false;
	});
	var template = doT.templateChild(CabinetBranchContent.attr('template'));
	
	
	function allocationDu(obj,e){
	     var oElem = e.target ? e.target : e.srcElement;
	     if(oElem.nodeName=="P"){
           		$(obj).parent().prev(".input_select").text($(obj).text()).attr("data-index",$(obj).find("p").attr("rel"));
			    $(obj).parents(".language").slideUp(10).addClass("yinc");
			    $(obj).parents(".language").find("li").slideUp(10).addClass("yinc");
           		du = $(obj).find("p").attr("rel");
           }else{
           		du = selectDu.attr("data-index");
           }
		CabinetData.selected[1] = du;
		if(du!="-1"){
			var list = {
				list: BranchAll[cabinet][du][1], 
				cabinet:du.replace(DuDataPrefix,"").split("."),
				name:du,
				data:CabinetData
			};
			var tmp = doT.template(template)(list);
			CabinetBranchContent.html(tmp);
		} else {
			selectCabinet.next().find("li").trigger("click");
			return;
		}
		//绑定拖动
		$("li.config",ConsumptionMapList).add($("a",CabinetBranchs)).off().on("dragstart", function (event, m) {
			var target = event.target;
			var nodeName = target.nodeName.toLowerCase();
			if(nodeName=="i"){return false;}
			if (!$(this).hasClass("set")) { return false; }
			if(CabinetRefresh.is(":visible")){
				return false;
			}
			BtnDetailClose.click();//关闭详细
			m.winscroll = $(window).scrollTop();
			m.origin = $(this);
			m.data = $(this)[0].id;
			m.name = $(this).attr("name");
			m.parent = $(this).parent();
			var p = $(this).position();
			m.fly = $(this).clone();
			m.fly.insertAfter(this);
			m.fly.css({ "position": "absolute", "top": p.top, "left": p.left, opacity: 1,"border": "#77933c dashed 1px", "background":"#f4fce3", width: $(this).width(), height: $(this).height() });
			return $(this).clone().addClass('active').insertAfter(this);
		}).on('drag', function (event, m) {
			var x = CabinetBranchs.offset().left;
			var y = CabinetBranchs.offset().top;
			$(m.proxy).css({
				"top": event.clientY - y+10+m.winscroll,
				"left": event.clientX - x+1,
				"position": "absolute",
				"border": "#77933c dashed 1px",
				"background":"#f4fce3",
				"opacity":0.7,
				"z-index":9999
			});
			if ($(event.target).hasClass("set")) {
				$(event.target).addClass("active");
				$(event.target).one("mouseout", function () {
					$(this).removeClass("active");
				});
			}
		}).on("dragend", function (event, m) {
			var target = $(event.target);
			var nodeName = target[0].nodeName.toLowerCase();
			if(nodeName=="em" || nodeName=="a" || nodeName=="span"){
				target = target.closest("li");	
			}
			var exceed = true;
			if(CabinetData.BranchLength<20){
				exceed = false;
			}
			var isSelected = $(target).hasClass("selected") || $(target).hasClass("selected-ie6");
			if (!isSelected || exceed) {
				$(m.fly).remove();
				$(m.proxy).animate({
					top: $(this).position().top,
					left: $(this).position().left,
					opacity: 1
				}, function () {
					$(this).remove();
				});
				if(!isSelected){
					return false;
				}
				that.SetProcessDone(tips[11]);
				return false;
			};
			$(m.origin).removeClass("set").addClass("disabled");
			$(m.proxy).remove();
			$(m.fly).remove();
			CabinetData.Branch[m.data] = m.name;
			CabinetData.BranchLength++;
			target.find("span").html(CabinetData.BranchLength).show();
			target.find("em").removeClass("hc");
		})
		//左侧分支选择
		var InputList = CabinetBranchContent.find("input");
		$("a",CabinetBranchs).off("click").on("click",function(event){
			var target = event.target;
			var nodeName = target.nodeName.toLowerCase();
			if(nodeName!="i"){return false;}
			BranchSetBox($(this));
		});
	};
	//左侧分支参数设置浮动层
	function BranchSetBox(target){
		var name = target.attr('name').replace(DuDataPrefix, "");
		var nums = name.split(".");
		var index = nums.pop();
		var cabinet = CabinetDataPrefix + nums[0];
		var du = DuDataPrefix + nums.join(".");
		var list = [name.replace(/\./g, ";") + ";", BranchAll[cabinet][du][1][index - 1][6].replace('A', "")]
		var template = CabinetBranchs.attr("template");
		//弹出窗回调函数
		function doBranchParam(d) {
			var CabinetParam = $("#CabinetParam");
			var template = doT.templateChild(d.template);
			var htmlDatas = doT.template(template)(d);
			CabinetParam.html(htmlDatas);
			Pages.bindInput();
			var btn = CabinetParam.find("a.btn_set");
			var setTips = $("#SetTips");
			var close = $("#PromptPopClose");
			btn.show().off().on("click", function () {
				if ($(this).hasClass("btn_set_disabled")) { return false; }
				var pd = CabinetParam.find("input[name='_content']");
				var posts = sessionLanguage + "&_modify_configure=18&_modify_configure_detail=46&_cabinet=0&_content=" + list[0];
				//一些验证
				for (var i = 0; i < pd.length; i++) {
					var pdi = pd.eq(i)
					var val = $.trim(pdi.val());
					if (i == 0) {
						var vals = val.split(".");
						if (val <= 0 || val > 10000 || val.length == 0 || isNaN(val) || (vals[1] && vals[1].length > 1)) {
							pdi.select();
							setTips.html(tips[14]);
							return false;
						}
					}
					posts += val + ";"
				}
				$(this).addClass("btn_set_disabled");
				var that = $(this);
				close.hide();
				setTips.html(Language.Html["011"]);
				var XHR = $.ajax({
					timeout: 10000,
					url: SetURL + "?_=" + new Date().getTime(),
					data: posts,
					success: function (data, textStatus, jqXHR) {
						try {
							var data = jQuery.evalJSON(data);
						} catch (err) {
							setTips.html(Language.Html["001"]);
							return false;
						}
						if (data.status == 98) {
							setTips.html(Language.Html['019']);
						} else if (data.status == 1) {
							//setTips.html(CabinetSetError[1]);
							for (var i = 0; i < pd.length; i++) {
								pd.eq(i).prev().html(data.content[i] + "A").attr('title', data.content[i] + "A");
							}
							//修改缓存数据
							BranchAll[cabinet][du][1][index - 1][6] = data.content[0] + " A";
							close.click();
							Pages.SetProcessOK(CabinetSetError[1],1000);
						} else {
							setTips.html(CabinetSetError[data.status]);
						}
					},
					error: function (data, textStatus) {
						setTips.html(CabinetSetError[0]);
					}
				}).always(function (jqXHR, textStatus, errorThrown) {
					jqXHR = null;
					textStatus = null;
					errorThrown = null;
					XHR = null;
					that.removeClass("btn_set_disabled");
					close.show();
				})
				return false;
			});
		}
		var CabinetName = $("#selectCabinet").text();
		var DuName = target.attr("title");
		//弹出窗口
		Control.PromptEvent.Pop({
			cover: true,
			title: CabinetName+ " > " +DuName,
			fn: doBranchParam,
			data: {
				template: template,
				list: list,
				args: data.args
			},
			html: "<div id='CabinetParam' class='PopCabinet'></div>",
			Class: "PopSetBranch"
		});
		return false;
	};
	//判断下拉框是否有某个值
	function CheckVal(select,value){
		var option = select.find("option");
		for(var i=0,ilen=option.length;i<ilen; i++){
			if(option.eq(i).text()==value){
				return true;
			}
		}
		return false;
	}
	function InitCabinet(){
		//初始化下拉框状态
		if(CabinetData.selected[0]){
			if(CheckVal(selectCabinet,CabinetData.selected[0])){
				$.each(selectCabinet.next().find("p"),function(){
			        if($(this).attr("rel")==CabinetData.selected[0]){
			            selectCabinet.text($(this).text());
			        }
			    });
			    selectCabinet.attr("data-index",CabinetData.selected[0]).next().find("li").trigger("click");
			} else {
				CabinetData.selected[0] = null;
			}
		};
	}
	InitCabinet();
	function InitDu(){
		if(CabinetData.selected[1]){
			var timer =  isIE6 ? 10 : 0;
			setTimeout(function(){
				if(CheckVal(selectDu,CabinetData.selected[1])){
					$.each(selectDu.next().find("p"),function(){
			        if($(this).attr("rel")==CabinetData.selected[1]){
			            selectDu.text($(this).text());
			        }
			    });
			    selectDu.attr("data-index",CabinetData.selected[1]).next().find("li").trigger("click");
				} else {
					CabinetData.selected[1] = null;
				}
			},timer);
		};
	}
	//自定义下拉列表事件
	BindSelect();
	function BindSelect(){
	     $(".input_select").off().click(function () {
           var cutobj=$(this).next(".language");
           cutobj.css({"width":""+Number($(this).width()+7)+"px"});
           if(cutobj.hasClass("yinc")) {
               //当下拉选项高度大于下拉框距离底部的高度时向上展开，反之向下正常展开
               if(cutobj.find("li").length*19>(document.documentElement.clientHeight - $(this).offset().top-21-32)){
                    //当下拉选项高度大于下拉框距离顶部的高度时向下展开并添加滚动条
                    if(cutobj.find("li").length*19>$(this).offset().top-166){
               		    cutobj.css({"margin-top":"0px","height":document.documentElement.clientHeight - $(this).offset().top-21-32-20,"overflow":"auto"});
               	    }else{
                        cutobj.css({"margin-top":"-"+(cutobj.find("li").length*19+21)+"px","height":"auto"});
               	    }
               }else{
               		cutobj.css({"margin-top":"0px","height":"auto"});
               }
               cutobj.slideDown(50).removeClass("yinc");
               cutobj.find("li").slideDown(50).removeClass("yinc");
           }else{
               cutobj.slideUp(10).addClass("yinc");
               cutobj.find("li").slideUp(10).addClass("yinc");
           }
         });
         $(document).off("click").click(function (event) {
               var oElem = event.target ? event.target : event.srcElement;
               if($(oElem).prop("className")!="input_select set_value"&&$(oElem).prop("className")!="language") {
                   $(".language").slideUp(20).addClass("yinc");
                   $(".language").find("li").slideUp(20).addClass("yinc");
               }
                $(".input_select").next(".language").not($(oElem).next(".language")).slideUp(20).addClass("yinc");
                $(".input_select").next(".language").not($(oElem).next(".language")).find("li").slideUp(20).addClass("yinc");
         });
	}
}
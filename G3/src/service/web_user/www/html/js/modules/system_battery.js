/*---------------------------------*
* Battery页面显示详细信息需要用到的三个参数
* [0]->是否有detail,[1]->如果有detail,detail数据所在的序号,[2]->当前mainBody滚动的位置,[3]->数据位置key
*---------------------------------*/
Pages.SystemBatteryStatus = [false, 0, 0, ""];
Pages.system_battery = function (data) {
    var that = this;
    if(data.data['BATT_CONTENT']['ID_SONICK_BATT_CONTENT'].length > 0)
    {
        $(".system-battery-left").remove();
        $(".system-batteryInfo").css("left", "0px");
        $(".system-batteryInfo").css("width", "578px");
        $(".table-a").css("width", "578px");
    }
    
    var Verizon_Mode = jQuery.cookie("Verizon_Mode");
    //是否显示为Verizon_Mode风格
    if(Verizon_Mode==1){
        $(".system-battery-left,.system-battery-img").css({"display":"none"});
        $(".system-batteryInfo").css({"left":"0"});
        $(".system-battery-item ul").css({"width":"193px"});
    }
    
    var dls = $("div.system-battery-item").find("dl");
    dls.off();
	var detailList = $("#batteryDetail"),batteryList = $("#batteryList"),mainBody = $("#mainBody");
    if (!Control.isPolling) {
        this.SystemBatteryStatus[0] = false;
    }
	Configs.tmpTimer[0] = setTimeout(function(){
		if(Control.isPolling){
			mainBody.scrollTop(Configs.MainScrollHeight);
		} else {
			mainBody.scrollTop(that.SystemBatteryStatus[2]);
			that.SystemBatteryStatus[2] = 0;
		}
	},1);
    var itemData = {};
    itemData.index = this.SystemBatteryStatus[1];
    itemData.back = Language.Html["040"];
    if (this.SystemBatteryStatus[0]) {
        batteryList.hide();
        detailList.show();
        renderDetail();
        bindClose();
    } else {
        detailList.hide();
        batteryList.show();
    };	
    this.bindEvent("click", "#batteryList", "dl.battery-dl-eib");
    function renderDetail() {
        itemData.list = data.data['BATT_CONTENT'][that.SystemBatteryStatus[3]][1][that.SystemBatteryStatus[1]];
        itemData.headLine = data.data['BATT_CONTENT'][that.SystemBatteryStatus[3]][0];
        var template = doT.templateChild($(detailList).attr("template")),
			doTtmpl = doT.template(template);
        $(detailList).html(doTtmpl(itemData)).show();
    }
    function bindClose() {
        $("#closeDetail").off().on("click", function (data) {
            batteryList.show();
            detailList.hide();
            that.SystemBatteryStatus[0] = false;
            mainBody.scrollTop(that.SystemBatteryStatus[2]);
            return false;
        });
    }
    var thermometerData = {
        type: 1,
        imgs: {
            left: 55,
            top: 25
        },
        alarm: {
            color: ["#ff5400", "#a8ff00", "#ffae00", "#ff5400"],
                value: data.data.group.chart.Battery_Temp_Alarm || [0, 75]
        },
        scale: {
            value: [-50, -25, 0, 25, 50, 75]
        },
            value: data.data.group.chart['Battery_Temperature']
    };
    var unittype=jQuery.cookie("drawThermometer");//温度类型0-摄氏度 1-华氏度
     if(unittype==1){
        for(var i=0;i<thermometerData['scale']['value'].length;i++){
            thermometerData['scale']['value'][i]=thermometerData['scale']['value'][i]*1.8+32;
        }
    }
    /*画温度计*/
    Chart.drawThermometer("batteryThermometer", thermometerData);
    var meterDatas = {
        center: [122, 130],
        background: {
            radius1: 55
        },
        finger: {
            angle: [180, 30],
            width: [11, 20],
            radius: 53,
            speed: 40
        },
        alarm: {
            outer: {
                color: ["#f26a26", "#f27826", "#f28626", "#f29926", "#f2a326", "#f2b626", "#f2c426", "#f2c926", "#f2d326", "#f2dc26", "#c6f223", "#c6f223"],
                value: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 18],
                /*此处18->为一个假设值,所有大于10的值都会在10-18的范围之内，取18只是为了无限的区域比10的区域小一点点*/
                radius: 90
            }
        },
        scale: {
            outer: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 18] /*18->的意义同上*/
        },
        value: {
                outer: data.data.group.chart['Estimated_Remaining_Time'],
            toFix: [0],
            unit: ["h"]
        },
        type: 1
    };
    Chart.drawMeter("batteryMeter", meterDatas);
	var PopupData = {
		dataKey: "",
		dataIndex: "",
		BATT_CONTENT: data.data['BATT_CONTENT'],
		battery: true,
		close:true,
		hide: false,
		interval:20
	};
	if(Configs.Popup.Data==null){
		Configs.Popup.Data = PopupData;
	} else {
		Configs.Popup.Data = $.extend(true, {}, PopupData,  Configs.Popup.Data);
	}

    this.PopupInfo("div.system-battery-item", "mouseenter", "dl", ["#popup"], PopupData);
	this.PopupClose("#popup");
    /*绑定温度计弹出温度趋势图*/
    $("div.system-batteryThermometer").off("click").on("click", function () {
        var setting = {
            canvas: {
                width: 540,
                height: 180
            },
            Y: {
                unit: "°C",
                value: [-50, -25, 0, 25, 50, 75],
                follow: [true, -50, 75]
            },
            unit: "°C",
            hasMax: false
        };
        var unittype=jQuery.cookie("drawThermometer");//温度类型0-摄氏度 1-华氏度
        if(unittype==1){
            for(var i=0;i<setting['Y']['value'].length;i++){
                setting['Y']['value'][i]=setting['Y']['value'][i]*1.8+32;
            }
            setting['Y']['follow'][1]=setting['Y']['value'][0];
            setting['Y']['follow'][2]=setting['Y']['value'][5];
            setting['Y']['unit']="°F";
            setting['unit']="°F";
        }
        Control.PromptEvent.Pop({ cover: true, title: Language.Chart["004"], fn: that.GetCurveData, data: { name: "Comp_Temp", setting: setting }, html: "<div id='PopComp_TempCurve' class='PopCurve'></div>" });
    });
    /*绑定第一第二个电池弹出容量曲线*/
    if(Verizon_Mode!=1){
        for (var i = 0; i < 2; i++) {
            if (dls.eq(i) && dls.eq(i).attr("name") == "ID_COM_BATT_CONTENT") {
                dls.eq(i).addClass("PopCurrent");
            }
        }
    }
	var SystemBatteryItem = $("div.system-battery-item");
    SystemBatteryItem.off("click").on("click", "dl", function (event) {
		var target = $(event.target);
		if(target.hasClass("set")){
			that.SystemBatteryStatus[2] = Configs.MainScrollHeight;
			that.SystemBatteryStatus[1] = $(this).attr('index');
			that.SystemBatteryStatus[3] = $(this).attr('name');
			SystemBatteryItem.hide();
			var idata = that.GetAttrData(target);
			that.InitData(idata);
			Control.GetTemplate(idata,true);
			return false;
		}
		if($(this).hasClass("PopCurrent")){
			var index = $(this).index() + 1;
			var name = $(this).attr("name");
			if (index > 2 || name != "ID_COM_BATT_CONTENT") { return; }
			var name = "Batt" + index + "_Capa";
			var setting = {
				canvas: {
					width: 540,
					height: 180
				},
				Y: {
					value: [0, 20, 40, 60, 80, 100],
					follow: [true, 0, 100]
				},
				unit: "%",
				hasMax: false
			};
			var id = "Pop" + name + "Curve";
			Control.PromptEvent.Pop({ cover: true, title: Language.Chart["005"], fn: that.GetCurveData, data: { name: name, setting: setting }, html: "<div id='" + id + "' class='PopCurve'></div>" });
		} else if($(this).hasClass("battery-dl")){
			that.SystemBatteryStatus[2] = Configs.MainScrollHeight;
			that.SystemBatteryStatus[1] = $(this).attr('index');
			that.SystemBatteryStatus[3] = $(this).attr('name');
			batteryList.hide();
			renderDetail();
			that.SystemBatteryStatus[0] = true;
			bindClose();
			mainBody.scrollTop(240);
		}
    });	
    if(jQuery.cookie("systeminfo")!=1){
        $(".system-battery-summary,.system-battery-list,.system-battery-item,.extbackground").css({width:'1163px'});
        
         if(Verizon_Mode==1){
            $(".system-meterInfo").css({width:'986px'});
            $(".system-meterInfo .table-a").css({width:'960px'});
        }else{
            $(".system-meterInfo").css({width:'739px'});
            $(".system-meterInfo .table-a").css({width:'719px'});
        }
    }else{
        $(".system-battery-summary,.system-battery-list,.system-battery-item,.extbackground").css({width:'917px'});
        if(Verizon_Mode==1){
            $(".system-meterInfo").css({width:'740px'});
            $(".system-meterInfo .table-a").css({width:'720px'});
        }else{
            $(".system-meterInfo").css({width:'493px'});
            $(".system-meterInfo .table-a").css({width:'473px'});
        }
    }
};
/*---------------------------------*
* DI Alarm页面回调函数, 
* data->渲染当前页面所需数据（格式: {args:{},data:{}},args:{}包涵链接中data={}中的数据，data:{}包涵服务器返回的数据)
*---------------------------------*/
Pages.SetDILevelOrRelay = function (datas) {
	var that = Pages;
	var content = $("#SetAlarmLevelRelay");
	content.html(datas.template);
	var info = content.find(".info").addClass("setting");
	var form = content.find("form");
	var inputs = form.find(".set_value");
	//初始化弹出框数据
	if (datas.modifydatas) {
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
			var name = inputs.eq(i).attr("name");
			inputs.eq(i).val(datas.modifydatas[name]);
			$.each(inputs.eq(i).next("ul").find("a"),function(){
			    if($(this).attr("rel")==datas.modifydatas[name]){
			       inputs.eq(i).text($(this).text());
			       inputs.eq(i).attr("data-index",datas.modifydatas[name]);
			    }
			})
		}
	}
	Pages.BindSelect();
	//const char szWebLangType[LANGUAGE_NUM][8] = {"de","es","fr","it","ru","tr","tw","zh"};
	var iLocLangType = jQuery.cookie("loc_lang_type");
	var iLangType = jQuery.cookie("language_type");
//	if ((iLangType == "1")
//		&& ((iLocLangType == "6") || (iLocLangType == "7"))) {
//		$("#SetDIAlarmAbbrName").hide();
//		if ($("#PromptPop").hasClass("Pop-di-alarm")) {
//			$("#PromptPop").removeClass("Pop-di-alarm");
//			$("#PromptPop").addClass("Pop-di-alarm-no-abbr");
//		}
//	}

	$("#SetAlarmLevelRelay input").off("focus blur keydown").on("focus", function () {
		//如果redonly为true，一般为时间设置，即不可以使用backspace
		if (!$(this).attr("readonly")) {
			Configs.Backspace = true;
		}
	}).on("blur", function () {
		Configs.Backspace = false;
	}).on("keydown", function () {
		if ($(this).hasClass("set_value")) {
			Configs.Backspace = true;
		};
	});
	content.find("a.btn_set").off().on("click", function () {
		if ($(this).hasClass("btn_set_disabled")) { return false; }
		info.html(Language.Html['011']).addClass("setting");
		var check = false;
		var vals = "";
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
			var item = inputs.eq(i);
			var pname = item.attr("pname");
			if (pname == "signal_new_level") {
				if (item.attr("data-index") == "") {
					check = true;
					info.html(Language.Html["051"]);
					break;
				}
			} else if (pname == "_alarmReg") {
				if (item.attr("data-index") == "") {
					check = true;
					info.html(Language.Html["052"]);
					break;
				}
			} else if (pname == "signal_new_name") {
				var sNewName = item.val();
				var iNameLength = 0;
				for (var iIndex = 0; iIndex < sNewName.length; iIndex++) {
					if (escape(sNewName[iIndex]).indexOf("%u") != -1) {
						iNameLength += 3; //Chinese char
					} else {
						iNameLength += 1; //Ascii
					}
				}
				if (iNameLength > 32) {
					check = true;
					info.html(Language.Html["061"]);
					break;
				}
			};
			
			if (pname == "signal_new_level"||pname == "_alarmReg"||pname == "signal_new_state") {
			    vals += "&" + item.attr("pname") + "=" + encodeURIComponent(item.attr("data-index"));
			}else{
			    vals += "&" + item.attr("pname") + "=" + encodeURIComponent(item.val());
			}
		};
		vals += "&sessionId=" + datas.args["sessionId"];
		if (check) {
			info.removeClass("setting");
			return false;
		}
		$(this).addClass("btn_set_disabled");
		var $this = $(this);
		var XHR = $.ajax({
			timeout: 10000,
			url: form.attr("action") + "?_=" + new Date().getTime(),
			type: form.type,
			data: datas.post + vals,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["001"]);
					return false;
				}
				if (data.status == 98) {
					info.html(Language.Html['019']);
					return;
				}
				if (data.status == 1) {
					info.addClass("setting");
					//that.SetProcess("");
					that.SetProcessOK(Language.Setting[data.status]);
					$("#PromptPopClose").click();
					that.SetAlarmScroll = Configs.MainScrollHeight;
					Control.RefreshModule();
				}
				info.html(Language.Setting[data.status]);
			},
			error: function (data, textStatus) {
				info.html(Language.Html['016']).removeClass("setting");
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			$this.removeClass("btn_set_disabled");
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
};

Pages.setting_di_content = function (data) {
	var that = this;
	var settemplates = $("#SetDIAlarm").attr("template");
	$("#SetAlarmList>div.table-body").on("click", "a.btn_set", function () {
		var post = $(this).attr("post");
		var parent = $(this).parents("ul");
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
		Control.PromptEvent.Pop({ cover: true, title: parent.find("li.t-2").text(), fn: that.SetDILevelOrRelay, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args }, html: "<div id='SetAlarmLevelRelay' class='Poplevelrelay'></div>", Class: "Pop-di-alarm" });
	}).on("click", "ul,i,a", function () {
		var ul = $(this).closest("ul");
		Configs.HighLight = ul.attr("id");
		ul.siblings().removeClass("select").end().addClass("select");
	});
	$("#" + Configs.HighLight).addClass("select");
	Configs.Data.polling = true;
	setTimeout(function () {
		if (document.getElementById("SetAlarmContent")) {
			$("#mainBody").scrollTop(that.SetAlarmScroll);
		}
	}, 10);
	if(jQuery.cookie("systeminfo")!=1){
        $(".table li").css({"padding-right":"50px"});
        $(".table-container").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table li").css({"padding-right":"0px"});
        $(".table-container").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
	return false;
};




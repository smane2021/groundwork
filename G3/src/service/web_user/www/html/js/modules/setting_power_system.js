﻿Pages.setting_power_system  = function (data) {
	var that = this;
	//列表模板
	var Set_PowerSystem = $("#Set_PowerSystem");
	var listTemplate = doT.templateChild(Set_PowerSystem.attr("template"));
	//打开列表
	function openList(target){
		var datas ={
			"args": data.args,
			"jQuery": jQuery,
			"$" : jQuery,
			"Configs" : Configs,
			"Pages" : Pages,
			"Control" :Control,
			"Validates":Validates,
			"window":window
		};
		var item = target.attr("data").split("|");
		var datalist = data.data[item[0]][item[1]][item[2]].list;
		datas["list"] = datalist;
		var doTtmpl = template.compile(listTemplate)(datas);

		target.html(doTtmpl).show();
		that.Setting(data);
		that.bindInput();
	}

	//expand
	function Expand(item){
		var p = item.parent("div.table-container");
		var next = item.next("div.table-content");
		var span = $(this).find("span");
		p.removeClass("border-bn");
		var nextHasList = next.attr("data");
		span.removeClass("title-zk").addClass("title-sl");
		Configs.PowerSystem.push(Number(next.attr("index")));
		if(nextHasList){
			openList(next);
		}
	}

	//打开或隐藏设置列表
	Set_PowerSystem.off('click').on("click","div.table-ex",function(){
		var p = $(this).parent("div.table-container");
		var next = $(this).next("div.table-content");
		next.toggle();
		p.toggleClass("border-bn");
		var span = $(this).find("span");
		var nextHasList = next.attr("data");
		if (next.is(":hidden")) {
			//close
			span.removeClass("title-sl").addClass("title-zk");
			that.DelArrayValue(Configs.PowerSystem, [Number(next.attr("index"))]);
			if(nextHasList){
				next.html("");
			}
		} else {
			//open
			span.removeClass("title-zk").addClass("title-sl");
			Configs.PowerSystem.push(Number(next.attr("index")));
			if(nextHasList){
				openList(next);
			}
		}
	});
	//刷新时，初始化状态
	var tables = $("div.table-ex");
	for(var i=0,ilen=Configs.PowerSystem.length; i<ilen; i++){
		var next = tables.eq(Configs.PowerSystem[i]).next("div.table-content");
		if(next.attr("data")){
			openList(next);
		}
	}
}
﻿Pages.setting_analog  = function (data) {
    var that = this;
        var datalist;
	//列表模板
	var Set_Analog = $("#Set_Analog");
	var listTemplate = doT.templateChild(Set_Analog.attr("template"));
		//打开列表
	function openList(target){
		var datas ={
			"args": data.args,
			"jQuery": jQuery,
			"$" : jQuery,
			"Configs" : Configs,
			"Pages" : Pages,
			"Control" :Control,
			"Validates":Validates,
			"window":window
		};
		var item = target.attr("data").split("|");
		var datalist = data.data[item[0]][item[1]][item[2]].list;
		datas["list"] = datalist;
		var doTtmpl = template.compile(listTemplate)(datas);

		target.html(doTtmpl).show();
		that.Setting(data);
		that.bindInput();
	}
	//打开或隐藏设置列表
	Set_Analog.off('click').on("click","div.table-ex",function(){
		var p = $(this).parent("div.table-container");
		var next = $(this).next("div.table-content");
		next.toggle();
		p.toggleClass("border-bn");
		var span = $(this).find("span");
		var nextHasList = next.attr("data");
		if (next.is(":hidden")) {
			//close
			span.removeClass("title-sl").addClass("title-zk");
			that.DelArrayValue(Configs.PowerSystem, [Number(next.attr("index"))]);
			if(nextHasList){
				next.html("");
			}
		} else {
			//open
			span.removeClass("title-zk").addClass("title-sl");
			Configs.PowerSystem.push(Number(next.attr("index")));
			if(nextHasList){
				openList(next);
			}
		}
	});
	//刷新时，初始化状态
	var tables = $("div.table-ex");
	for(var i=0,ilen=Configs.PowerSystem.length; i<ilen; i++){
		var next = tables.eq(Configs.PowerSystem[i]).next("div.table-content");
		if(next.attr("data")){
			openList(next);
		}
	}
	if(jQuery.cookie("systeminfo")!=1){
        $(".table li").css({"padding-right":"50px"});
        $(".table-container").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table li").css({"padding-right":"0px"});
        $(".table-container").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
	var uls = $("div.table-body ul");
	var settemplates = $("#Set_Analog").attr("template");
    $(".table-body").on("click", "a.btn_mod", function () {
        var post = $(this).attr("post");
        var signatype = $(this).attr("SignaType");
		var parent = $(this).parents("ul");
		var title=parent.parents(".table-list").prev().find("span").text();
		var sdtitle=parent.find(".t-1").find("span").text();
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
		var inputmessage = jQuery.evalJSON($(this).attr("inputmessage"));
        var obj = modifydatas;
        var na4 = $(this).attr("na4");
        var lang = $(this).attr("lang");
		Control.PromptEvent.Pop({ cover: true, title:title+" > "+sdtitle , fn: that.SetAnalog, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args }, html: "<div id='SetAlarmLevelRelay' class='Poplevelrelay'></div>", Class: "Pop-SettingAnalog" });
        		//统一赋值对象
		var SV1 = $('[name="Signal_FullName"]');
		var SV2 = $('[name="Signal_AbbrevName"]');
		
	    var Limit1 = $('[name="Signal_low1Limit"]');
	    var Limit2 = $('[name="Signal_low2Limit"]');
	    var Limit3 = $('[name="Signal_high1Limit"]');
		var Limit4 = $('[name="Signal_high2Limit"]');
		
        var Relay1 = $('[name="Signal_low1Relay"]');
        var Relay2 = $('[name="Signal_low2Relay"]');
        var Relay3 = $('[name="Signal_high1Relay"]');
        var Relay4 = $('[name="Signal_high2Relay"]');
        
        var Severity1 = $('[name="Signal_low1Severity"]');
        var Severity2 = $('[name="Signal_low2Severity"]');
        var Severity3 = $('[name="Signal_high1Severity"]');
        var Severity4 = $('[name="Signal_high2Severity"]');
        
        var TransValue1 = $('[name="Signal_Units"]');
        var TransValue2 = $('[name="Signal_X1"]');
        var TransValue3 = $('[name="Signal_Y1"]');
        var TransValue4 = $('[name="Signal_X2"]');
        var TransValue5 = $('[name="Signal_Y2"]');
        var signaNameObj;
        var Limit = "Limit";
        var Severity = "Severity";
        var Relay = "Relay";
        var TransValue = "TransValue";
		for (var item in modifydatas) {
		    if (item != "toJSONString") {
		         if(signatype>=41&&signatype<=50){
		            datalist = obj[item][8][0][6];
		            signaNameObj = obj[item][8][0];
		            $(".TransValue").hide();
		         }else if(signatype>=121&&signatype<=130){
		            datalist = obj[item][8][1][6];
		            signaNameObj = obj[item][8][1];
		         }else{
		            datalist = obj[item][8][2][6];
		            signaNameObj = obj[item][8][2];
		         }
		         
                 SV1.val(signaNameObj[4][lang]).prev().prev().text(signaNameObj[4][lang]).attr("title",signaNameObj[4][lang]);
                 SV2.val(signaNameObj[5][lang]).prev().prev().text(signaNameObj[5][lang]).attr("title",signaNameObj[5][lang]);
                 SV1.attr("Signal_Type",signaNameObj[1]);
                 SV1.attr("Signal_ID",signaNameObj[2]);
                 SV2.attr("Signal_Type",signaNameObj[1]);
                 SV2.attr("Signal_ID",signaNameObj[2]);
                 var html="";
                 //添加设置选项
                 for (var m = 0; m < datalist[0][9].length; m++) {
                    html += '<li><a href="javascript:void(0)" rel=' + m + '>'+datalist[0][9][m][lang]+'</a></li>';
                 }
                 TransValue1.attr("data-index",0).text(datalist[0][9][0][lang]);
                 TransValue1.next().append(html);
                 Pages.BindSelect();
                 var dataindex;
                 var dataindex2
                 for(var i=0;i<4;i++){
                     if(signatype>=41&&signatype<=50){
                         dataindex = i*2;
                         dataindex2 = Number(i+i+1);
                     }else{
                         dataindex = 5+i*2;
                         dataindex2 = 6+i*2;
                     }
                    //拼接Limit对象
                    var liobjLimit = eval(Limit + "" + Number(i+1) + "");
                    liobjLimit.val(datalist[dataindex][3]).prev().prev().text(datalist[dataindex][3]).attr("title",datalist[dataindex][3]);
                    liobjLimit.prev().text(datalist[dataindex][5]);
                    liobjLimit.attr("Signal_Type",datalist[dataindex][1]);
                    liobjLimit.attr("Signal_ID",datalist[dataindex][2]);
                    liobjLimit.attr("Signal_ValueType",datalist[dataindex][4]);
                    if(signatype>=41&&signatype<=50){
                         liobjLimit.attr("validate", "{'range':[" + datalist[dataindex][9][0] + "," + datalist[dataindex][9][1] + "],'type':"+datalist[dataindex][4]+"}")
                         liobjLimit.next().text(datalist[dataindex][9][0] + " to " + datalist[dataindex][9][1]);
                         
                     }else{
                        liobjLimit.attr("validate", "{'range':[" + datalist[2][3] + "," + datalist[4][3] + "],'type':"+datalist[dataindex][4]+"}")
                        liobjLimit.next().text("Y1 to Y2");
                        //liobjLimit.next().text(datalist[2][3] + " to " + datalist[4][3]);
                     }
                    
                    //拼接Severity对象
                    var liobjSeverity = eval(Severity + "" +  Number(i+1) + "");
                    liobjSeverity.val(datalist[dataindex2][4]).prev().prev().text(datalist[dataindex2][4]).attr("title",datalist[dataindex2][4]);
                    liobjSeverity.attr("Signal_Type",datalist[dataindex2][1]);
                    liobjSeverity.attr("Signal_ID",datalist[dataindex2][2]);
                    liobjSeverity.attr("Signal_ValueType",datalist[dataindex2][6]);
                    for(var x=0;x<liobjSeverity.next().find("li").length;x++){
                        if(liobjSeverity.next().find("li").eq(x).find("a").attr("rel")==datalist[dataindex2][4]){
                            liobjSeverity.parent().prev().prev().text(liobjSeverity.next().find("li").eq(x).text());
                            liobjSeverity.attr("data-index", liobjSeverity.next().find("li").eq(x).find("a").attr("rel")).text(liobjSeverity.next().find("li").eq(x).text());
                        }
                    }
                    //拼接Relay对象
                    var liobjRelay = eval(Relay + "" +  Number(i+1) + "");
                    liobjRelay.val(datalist[dataindex2][3]).prev().prev().text(datalist[dataindex2][3]).attr("title",datalist[dataindex2][3]);
                    for(var x=0;x<liobjRelay.next().find("li").length;x++){
                        if(liobjRelay.next().find("li").eq(x).find("a").attr("rel")==datalist[dataindex2][3]){
                            liobjRelay.parent().prev().prev().text(liobjRelay.next().find("li").eq(x).text());
                            liobjRelay.attr("data-index", liobjRelay.next().find("li").eq(x).find("a").attr("rel")).text(liobjRelay.next().find("li").eq(x).text());
                        }
                    }
                    if(!(signatype>=41&&signatype<=50)){
                        //拼接TransValue对象
                        for(var x=0;x<5;x++){
                            var liobjTransValue = eval(TransValue + "" +  Number(x+1) + "");
                            if(liobjTransValue[0].nodeName=="DIV"){
		                        for(var y=0;y<liobjTransValue.next().find("li").length;y++){
                                    if(liobjTransValue.next().find("li").eq(y).find("a").attr("rel")==datalist[x][3]){
                                        liobjTransValue.parent().prev().prev().text(liobjTransValue.next().find("li").eq(y).text());
                                        liobjTransValue.attr("data-index", liobjTransValue.next().find("li").eq(y).find("a").attr("rel")).text(liobjTransValue.next().find("li").eq(y).text());
                                    }
                                }
		                    }else{
		                        liobjTransValue.val(datalist[x][3]).prev().prev().text(datalist[x][3]).attr("title",datalist[x][3]);
                                liobjTransValue.prev().text(datalist[x][5]);
                                liobjTransValue.next().text(datalist[x][9][0] + " to " + datalist[x][9][1]);
                                liobjTransValue.attr("validate", "{'range':[" + datalist[x][9][0] + "," + datalist[x][9][1] + "],'type':"+datalist[x][4]+"}")
		                    }
		                     liobjTransValue.attr("Signal_Type",datalist[x][1]);
                             liobjTransValue.attr("Signal_ID",datalist[x][2]);
                             liobjTransValue.attr("Signal_ValueType",datalist[x][4]);
                        }
                    }
                }
            Pages.BindSelect();
		    }
		}
		
		
    }).on("click", "ul,i,a", function () {
       if($(this).hasClass("btn_set_disabled")){
           uls.removeClass("select");
       }else{
	       uls.removeClass("select");
           $(this).addClass("select");
       }
    });
}

Pages.SetAnalog=function(datas){
    var that = Pages;
	var content = $("#SetAlarmLevelRelay");
	content.html(datas.template);
	var info = content.find(".info").addClass("setting");
	var form = content.find("form");
    var inputs = form.find(".set_newval");
    
    $("#SetAlarmLevelRelay input").off("focus blur keydown").on("focus", function () {
		//如果redonly为true，一般为时间设置，即不可以使用backspace
		if (!$(this).attr("readonly")) {
			Configs.Backspace = true;
		}
	}).on("blur", function () {
		Configs.Backspace = false;
	}).on("keydown", function () {
		if ($(this).hasClass("set_value")) {
			Configs.Backspace = true;
		};
	});

    content.find("a.btn_set").off().on("click", function () {
        var Low = $(this).attr("low");
        var High = $(this).attr("high");
        var Low2 = $(this).attr("low2");
        var High2 = $(this).attr("high2");
        var Signal_Y1,Signal_Y2,Signal_X1,Signal_X2;
        var Limit=[];
		if ($(this).hasClass("btn_set_disabled")) { return false; }
		info.html(Language.Html['011']).addClass("setting");
		$("#PromptPopClose").hide();
		var vals = "";
		var FullName,AbbrName;
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
		    var item = inputs.eq(i);
		    if(!item.parent().is(":hidden")){
		        var validate = jQuery.evalJSON(item.attr("validate"));
		        var pname = item.attr("pname");
		        var sNewName="";
		        if(item[0].nodeName=="DIV"){
		            sNewName=item.attr("data-index");
		        }else{
		            sNewName = item.val();
		        }
		        if(sNewName==""){
	                 info.html(Language.Html["032"]);
	                 item.focus();
	                 $("#PromptPopClose").show();
	                 return false;
	            }
		        if(item.attr("pname")=="Signal_FullName"){
	                 FullName= sNewName;
	            }
	            if(item.attr("pname")=="Signal_AbbrevName"){
	                 AbbrName = sNewName;
	            }
	            if(item.attr("pname")=="Signal_Y1"){
	                 Signal_Y1=Number(sNewName);
	            }
	            if(item.attr("pname")=="Signal_Y2"){
	                 Signal_Y2=Number(sNewName);
	            }
	             if(item.attr("pname")=="Signal_X1"){
	                 Signal_X1=Number(sNewName);
	            }
	            if(item.attr("pname")=="Signal_X2"){
	                 Signal_X2=Number(sNewName);
	            }
		        //获取语言类型（0：英文、1：中文）
		        var langtype=datas.post.split("&equip_ID")[0].split("=")[1];
		        if(sNewName==null||sNewName==undefined||sNewName==""){ 
		            vals += "&" + item.attr("name") + "=" + encodeURIComponent(-1);
		        }else{
		            vals += "&" + item.attr("name") + "=" + encodeURIComponent(sNewName);
		            //setvalue.push(encodeURIComponent(sNewName))
		        }
    		    
		        if(validate!=null){
		            if(item.attr("pname").indexOf("Limit")>-1&&Signal_Y1==Number($('[name="Signal_Y1"]').prev().prev().text())&&Signal_Y2==Number($('[name="Signal_Y2"]').prev().prev().text())){
		                if (!Validates.CheckSetting(inputs, validate, sNewName)) {
		                    item.focus();
		                    info.html("");
		                    $("#PromptPopClose").show();
                            return false;
                        }
		            }else{
		                if (!Validates.CheckSetting(inputs, validate, sNewName)) {
	                        item.focus();
	                        info.html("")
	                        $("#PromptPopClose").show();
                            return false;
                        }
		            }
                }
                if(item.attr("pname").indexOf("Limit")>-1){
                    Limit.push(item.val())
                }
		        if(item.attr("Signal_Type")==""||item.attr("Signal_Type")==null){
		            item.attr("Signal_Type",-1);
		        }
		        if(item.attr("Signal_ID")==""||item.attr("Signal_ID")==null){
		            item.attr("Signal_ID",-1);
		        }
		        if(item.attr("Signal_ValueType")==""||item.attr("Signal_ValueType")==null){
		            item.attr("Signal_ValueType",-1);
		        }
		        vals+="&"+pname+"_SignalType="+item.attr("Signal_Type");
		        vals+="&"+pname+"_SignalID="+item.attr("Signal_ID");
                vals+="&"+pname+"_ValueType="+item.attr("Signal_ValueType");
		    }
		}
		if(Signal_Y1>=Signal_Y2){
		     info.html(Language.Validate['003']);
		     $('[name="Signal_Y2"]').focus();
		     $("#PromptPopClose").show();
		     return false;
		}
		if(Signal_X1>=Signal_X2){
		     info.html(Language.Validate['003']);
		     $('[name="Signal_X2"]').focus();
		     $("#PromptPopClose").show();
		     return false;
		}
		Limit=Limit.map(function(num){return Number(num)})
		for(var x=0;x<Limit.length;x++){
		    if(Signal_Y1>Limit[x]){
		        info.html(Language.Validate['003']);
		        $('[name="Signal_Y1"]').focus();
		        $("#PromptPopClose").show();
		        return false;
		    }else if(Signal_Y2<Limit[x]){
		        info.html(Language.Validate['003']);
		        $('[name="Signal_Y2"]').focus();
		        $("#PromptPopClose").show();
		        return false;
		    }
		}
		
		if(Limit[0]>=Limit[2]||Limit[0]>=Limit[3]||Limit[1]>=Limit[2]||Limit[1]>=Limit[3]||Limit[1]>Limit[0]||Limit[2]>Limit[3]){
		    info.html(Language.Validate['003']);
		    $("#PromptPopClose").show();
		    return false;
		}
		/*if(Limit[1]>=Limit[0]||Limit[1]>=Limit[2]||Limit[1]>=Limit[3]){
		    info.html(Language.Validate['003']);
		    $('[name="Signal_low2Limit"]').focus();
		    $("#PromptPopClose").show();
		     return false;
		}
		if(Limit[2]<=Limit[0]||Limit[2]<=Limit[1]||Limit[2]>=Limit[3]){
		    info.html(Language.Validate['003']);
		    $('[name="Signal_high1Limit"]').focus();
		    $("#PromptPopClose").show();
		     return false;
		}
		if(Limit[3]<=Limit[0]||Limit[3]<=Limit[1]||Limit[3]<=Limit[2]){
		    info.html(Language.Validate['003']);
		    $('[name="Signal_high2Limit"]').focus();
		    $("#PromptPopClose").show();
		    return false;
		}*/

		vals+="&Alarm_Low1_Full="+FullName+Low+"1&Alarm_Low2_Full="+FullName+Low+"2&Alarm_High1_Full="+FullName+High+"1&Alarm_High2_Full="+FullName+High+"2"
        vals+="&Alarm_Low1_Abbr="+AbbrName+Low2+"1&Alarm_Low2_Abbr="+AbbrName+Low2+"2&Alarm_High1_Abbr="+AbbrName+High2+"1&Alarm_High2_Abbr="+AbbrName+High2+"2"
		vals += "&sessionId=" + datas.args["sessionId"];
		$(this).addClass("btn_set_disabled");
		var $this = $(this);
		var XHR = $.ajax({
			timeout: 20000,
			url: form.attr("action") + "?_=" + new Date().getTime(),
			type: form.type,
			data: datas.post + vals,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["001"]);
					$("#PromptPopClose").show();
					return false;
				}
				if (data.status == 98) {
					info.html(Language.Html['019']);
					return;
				}
				if (data.status == 1) {
				    $(this).addClass("btn_set_disabled");
				    setTimeout(function(){delayload()},5000);
				    info.html(Language.Html['011']);
				}else{
				   info.html(Language.Setting[data.status]);
				   $("#PromptPopClose").show();
				}
			},
			error: function (data, textStatus) {
				info.html(Language.Html['016']).removeClass("setting");
				$("#PromptPopClose").show();
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
	
	function delayload(){//setvalue
	    info.html(Language.Setting[1]);
	    $("#PromptPopClose").show();
		info.addClass("setting");
		that.SetProcessOK(Language.Setting[1]);
		$("#PromptPopClose").click();
		that.SetAlarmScroll = Configs.MainScrollHeight;
	}
}
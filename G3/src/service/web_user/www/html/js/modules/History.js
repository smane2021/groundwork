/*---------------------------------*
* 历史查询页面公用回调函数
* data->渲染当前页面所需数据（格式: {args:{},data:{}},args:{}包涵链接中data={}中的数据，data:{}包涵服务器返回的数据)
*---------------------------------*/
Pages.HistoryNewClick = 1;
Pages.History = function (data) {
    //Data.polling设置false表示当前页面不进行定时轮询
    Configs.Data.polling = false;
    this.HistoryNewClick = 1;
    //设置date to为当前值
    var nowDate = new Date();
    var $DateTo = $("#dateTo"), $DateFrom = $("#dateFrom");
    
  //  $DateTo.val(nowDate.getFullYear() + "/" + Validates.MakeupZero(nowDate.getMonth() + 1) + "/" + Validates.MakeupZero(nowDate.getDate()) + " " + Validates.MakeupZero(nowDate.getHours()) + ":" + Validates.MakeupZero(nowDate.getMinutes()) + ":" + Validates.MakeupZero(nowDate.getSeconds()));
  var current_time = nowDate.getFullYear() + "/" + Validates.MakeupZero(nowDate.getMonth() + 1) + "/" + Validates.MakeupZero(nowDate.getDate()) + " " + Validates.MakeupZero(nowDate.getHours()) + ":" + Validates.MakeupZero(nowDate.getMinutes()) + ":" + Validates.MakeupZero(nowDate.getSeconds());
        var times = new Date(current_time).getTime() / 1000 - Configs.timezone * 3600;
		var dateTime={"data":{"present_time":times}};
		Pages.footer(dateTime,$DateTo);//根据不同信号值显示不同时间格式
		
    var nextDate = new Date(new Date().setDate(new Date().getDate() - 1));
    
    //设置date from为当前值的前一天（即昨天)
    //$DateFrom.val(nextDate.getFullYear() + "/" + Validates.MakeupZero(nextDate.getMonth() + 1) + "/" + Validates.MakeupZero(nextDate.getDate()) + " " + Validates.MakeupZero(nextDate.getHours()) + ":" + Validates.MakeupZero(nextDate.getMinutes()) + ":" + Validates.MakeupZero(nextDate.getSeconds()));
     var current_time2 = nextDate.getFullYear() + "/" + Validates.MakeupZero(nextDate.getMonth() + 1) + "/" + Validates.MakeupZero(nextDate.getDate()) + " " + Validates.MakeupZero(nextDate.getHours()) + ":" + Validates.MakeupZero(nextDate.getMinutes()) + ":" + Validates.MakeupZero(nextDate.getSeconds());
        var times2 = new Date(current_time2).getTime() / 1000 - Configs.timezone * 3600;
		var dateTime2={"data":{"present_time":times2}};
		Pages.footer(dateTime2,$DateFrom);//根据不同信号值显示不同时间格式
    
    //绑定时间选择器
    if ($.type($DateFrom.datetimepicker) == "function") {
        this.SetDateTimePicker($DateFrom);
        this.SetDateTimePicker($DateTo);
    };
	var mainBody = $("#mainBody");
    mainBody.css({ "overflow-y": "auto" });
    //提交查询
    var that = this;
    var form = $("#historyForm");
    var templatehtml = doT.templateChild($("#temp_historyList").html());
    var fn = $("#temp_historyList").attr("fn");
    var historyList = $("#historyList");
    var mainBody = $("#mainBody");
    //自定义下拉列表事件
    Pages.BindSelect();
    if(jQuery.cookie("systeminfo")!=1){
	    $(".history-summary,.history-list,.extbackground").css({width:"1163px"});
	    $(".table-title,.table-a").css({width:'1143px'});
    }else{
        $(".history-summary,.history-list,.extbackground").css({width:'917px'});
        $(".table-title,.table-a").css({width:'897px'});
    }
    $("#HistoryQuery").off().on("click", function () {
        that.HistoryNewClick = 0;
        if ($(this).hasClass("btn_2_disabled")) { return false; }
        var Button_HistoryUpload = form.find("a.historyUpload");
        Button_HistoryUpload.hide();
        if (historyList[0]) {
            historyList[0].innerHTML = "";
        };
        if ($DateFrom[0] && !Validates.CheckDateTime($DateFrom,"Ht")) {
            Control.PromptEvent.Tip({ target: "historyList", text: Language.Validate['009'], marginTop: 60 });
            $DateFrom.focus();
            return false;
        };
        if ($DateTo[0] && !Validates.CheckDateTime($DateTo,"Ht")) {
            Control.PromptEvent.Tip({ target: "historyList", text: Language.Validate['009'], marginTop: 60 });
            $DateTo.focus();
            return false;
        };
        //post数据
        var timezone = new Date().getTimezoneOffset() / 60;
        var dateFrom = Date.parse(Validates.RedDateTime($("#dateFrom").val()).replace(/-/g,"/")) / 1000 - timezone * 3600;
        var dateTo = Date.parse(Validates.RedDateTime($("#dateTo").val()).replace(/-/g,"/")) / 1000 - timezone * 3600;
        var equipID;
        var data_type;
        equipID=$("#equipID")[0].tagName=="INPUT"?$("#equipID").val():$("#equipID").attr("data-index");
        data_type=$("#data_type")[0].tagName=="INPUT"?$("#data_type").val():$("#data_type").attr("data-index");
        var post_data = "equipID=" + equipID + "&_data_type=" + data_type + "&sessionId=" + data.args['sessionId'] + "&language_type=" + data.args["language_type"];

        if (dateFrom) {
            post_data += "&_timeFrom=" + dateFrom;
        }
        if (dateTo) {
            post_data += "&_timeTo=" + dateTo;
        }
        var XHR = $.ajax({
            type: form.attr("method"),
            url: form.attr("action") + "?_=" + new Date().getTime(),
            data: post_data,
            beforeSend: function () {
                Control.PromptEvent.Loading({ target: "historyList", interval: 200, marginTop: 60, clear: true });
				mainBody.scrollTop(0);
            },
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    Control.PromptEvent.Tip({ target: "historyList", text: Language.Html['001'], marginTop: 60 });
                    return false;
                };
                setTimeout(function () {
                    if (!document.getElementById("HistoryQuery")) { return false; }
                    if (that.HistoryNewClick == 1) { return false; }
                    //判断返回数据是否为空
                    var h_no_data = false;
                    if (data.history_content == "" || data.history_content.length == 0) {
                        Button_HistoryUpload.hide();
                        h_no_data = true;
                    } else {
                        Button_HistoryUpload.show();
                    }
                    data['unittype'] =jQuery.cookie("drawThermometer");
                    var result = data['control_result'];
                    if (result != 1) {
                        var h_class = h_no_data ? "PromptError PromptNoData" : "PromptError";
                        Control.PromptEvent.Tip({ target: "historyList", text: Language.History[result], marginTop: 60, Class: "h_class", clear: true });
                        return false;
                    };
                    data["Pages"] = Pages;
                    var render = template.compile(templatehtml);
                    var htmlDatas = render(data);
//                    var unittype = jQuery.cookie("drawThermometer");//温度类型0-摄氏度 1-华氏度
//                    if(unittype==1){
//                        htmlDatas.toString().replace(/deg.C/g, 'deg.F');
//                    }
                    if (historyList[0]) {
                        historyList[0].innerHTML = htmlDatas;
                    }
                    //避免重复绑定自定义下拉列表事件
		            $(".language li").off("click");
                    Pages.BindSelect();
                    if(jQuery.cookie("systeminfo")!=1){
	                    $(".table-a,#history_t2").css({width:'1143px'});
                    }else{
                        $(".table-a,#history_t2").css({width:'897px'});
                    }
		    //reset mainBody scroll 
		    Configs.MainScrollHeight = 0;
		    mainBody.scrollTop(0);

                    if (fn && $.type(that[fn]) === "function") {
                        that[fn](data);
                    }
                }, 200);
            },
            error: function (data, textStatus) {
                Control.PromptEvent.Tip({ target: "historyList", text: Language.Html['023'], marginTop: 60, clear: true });
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            XHR = null;
        });
        return false;
    });
	var mainBodyScroll = null;
	mainBody.off("scroll.h").on("scroll.h",function(){
		//为历史记录固定表头
		var scrollTop = $(this).scrollTop();
		var theadID ="thead-fixed";
		if(scrollTop<166){
			$("#"+theadID).hide();  
		}
		if(mainBodyScroll){ clearTimeout(mainBodyScroll); }
		mainBodyScroll = setTimeout(function(){
			that.FixThead("HistoryTable",theadID,scrollTop);
            if (navigator.userAgent.indexOf("Firefox") > -1||(!!window.ActiveXObject || "ActiveXObject" in window)){
		        if(jQuery.cookie("systeminfo")!=1){
                   $("#thead-fixed,#thead-fixed .table-a").css({"width":"1136px","margin":"0"});
                }else{
                   $("#thead-fixed,#thead-fixed .table-a").css({"width":"890px","margin":"0"});
                }
		    }else{
		        if(jQuery.cookie("systeminfo")!=1){
                   $("#thead-fixed,#thead-fixed .table-a").css({"width":"1143px","margin":"0"});
                }else{
                   $("#thead-fixed,#thead-fixed .table-a").css({"width":"897px","margin":"0"});
                }
		    }
		},50);
	});
	$(document).off("scroll.h").on("scroll.h",function(){
		//body滚动时,thead不重新定位
		Pages.FixThead("HistoryTable","thead-fixed",0);
	})    
};
/*---------------------------------*
* 历史查询页面->电源测试查询页面回调函数
* data->渲染当前页面所需数据（格式: {args:{},data:{}},args:{}包涵链接中data={}中的数据，data:{}包涵服务器返回的数据)
*---------------------------------*/
Pages.HistoryBattList = function (data) {
    var HistoryList = $("#historyList");
    var t2 = $("#history_t2");
    var t1 = $("#history_t1");
    var mainBody = $("#mainBody");
    var HistorySummary = $("#history_summary");
    HistoryList.height("auto");
    mainBody.css({ "overflow-y": "hidden" });
    t2.height(mainBody.height() - HistorySummary.outerHeight(true) - t1.outerHeight(true) - 48);
    var col = $("#td_location");
    var col_current = null;
    var row = $("#tr_location");
    var row_current = null;
    t2.scrollLeft(0);
    t2.scrollTop(0);
    HistoryList.off().on("click", ".language li", function () {
        var minRow = Math.ceil(t2.height() / 30) - 2;
        if (row_current != null) {
            $(row_current).removeClass("tr_selected");
        }
        if (col_current != null) {
            $(col_current).removeClass("td_selected");
        }
        var index_row = Number(row.find("div").attr("data-index")) + 1;
        var index = col.find("div").attr("data-index");
        if (index_row == 0) {
            t2.scrollTop(0);
            if (index == -1) {
                t2.scrollLeft(0);
                return false;
            } else {
                index_row = 0;
                row_current = document.getElementById("tr_" + index_row);
                location_td();
            }
        } else {
            row_current = document.getElementById("tr_" + index_row);
            if (index_row > minRow) {
                t2.scrollTop((index_row - minRow / 2) * 30);
            } else {
                t2.scrollTop(0);
            };
            $(row_current).addClass("tr_selected");
            if (index == -1) {
                t2.scrollLeft(0);
                return false;
            } else {
                location_td();
            }
        }
        function location_td() {
            t2.scrollLeft(0);
            if (index == -1) { return false; }
            if (index_row == 0) {
                col_current = document.getElementById("td_1_" + index);
            } else {
                col_current = document.getElementById("td_" + index_row + "_" + index);
            }
            var $col_current = $(col_current);
            var offset_left = $col_current.offset().left;
            var width = $col_current.width();
            if (offset_left + width - 352.5 > 750) {
                t2.scrollLeft(offset_left - 600);
            };
            $(col_current).addClass("td_selected");
        };
    });
    t2.off("click").on("click", function (event) {
        if (row_current) {
            $(row_current).removeClass("tr_selected");
        }
        var td = event.target;
        var tr = $(td).closest("tr");
        tr.addClass("tr_selected");
        row_current = tr[0];
        if (col_current) {
            $(col_current).removeClass("td_selected");
        };
        col_current = td;
        $(td).addClass("td_selected");
    });
};
Pages.setting_TL1_group = function (data) {
    //alert("OK");
    Configs.Data.polling = false;
    var lang = Language["setting_group"];
	var TL1_group_errors = lang["errors"];
	var tips = lang["tips"];
	var that = this;
    var args = data.args, data = data.data;
    var sessionId = "&sessionId=" + args["sessionId"];
    var language_type = "&language_type=" + args["language_type"];
    var index;
    var NameSuffixMaxLen;
    var _form = $("#GroupList").find("form");
    var _btn_modify = _form.find("a.modify");
    Pages.BindSelect();
    var _datacommon = "sessionId="+args['sessionId']+"&language_type="+args['language_type']+"&_modify_configure=19"+"&_modify_configure_detail=53&";
    $("#GroupList").find("div.table-body>ul").off().on("click", function () {
            _btn_modify.removeClass("btn_2_disabled").addClass("btn_2");
            var selected = $(this).find("div").eq(0);
            selected.removeClass("radiodivnot");//����
            $(this).siblings().find("div").addClass("radiodivnot").end();
            $(this).siblings().removeClass("selected").end().addClass("selected");
            var data = jQuery.evalJSON($(this).find("div").attr("data"));
            for(var item in data){
            index = data["_index"];
            if((item != "_index") && (item != "_prefixValid") && (item != "_NameSuffixMaxLen"))
            {
                var list_item = $("#"+item);
                list_item.val($.trim(data[item]));
            }
            if(data["_prefixValid"] == 0)
            {
                $("#_prefix").attr("disabled",true).addClass("set_disabled");
            }
            else
            {
                $("#_prefix").attr("disabled",false).removeClass("set_disabled");
            }
            NameSuffixMaxLen = data["_NameSuffixMaxLen"];
        }
    });
    if(jQuery.cookie("systeminfo")!=1){
            $(".table-container,.set-form").css({width:'1163px'});
            $(".table-title").css({width:'1143px'});
            $(".table li").css({"padding-right":"60px"});
	        $(".table-container .table").css({width:"1143"});
	        $(".btn_2,.btn_2_disabled").css({"margin-right":"153px"});
        }else{
            $(".table-container,.set-form").css({width:'917px'});
            $(".table-title,.table-container .table").css({width:'897px'});
             $(".btn_2,.btn_2_disabled").css({"margin-right":"67px"});
        }
    _btn_modify.on("click",function(){
        if(_btn_modify.hasClass("btn_2_disabled"))
        {
            return false;
        }
        var _data = _datacommon;
        var itemdata = _form.serialize();
        var _name = $("#_name");
        var _prefix = $("#_prefix");
        var _name_value = $.trim(_name.val());
        var _prefix_value = $.trim(_prefix.val());
        
        if(jQuery.cookie("nAuthority") < 3){
		        alert(TL1_group_errors[12]);
                return false;
	        };
	        
	if(!(/^[A-Za-z0-9]{1,20}$/gi.test(_name_value))){
	                alert(TL1_group_errors[13]);
	                _name.select();
                        return false;
                    };
        
        if(_name_value == "")
        {
            alert(TL1_group_errors[14]);
	    _name.select();
            return false;
        }
        
        if(!(/^[A-Za-z0-9]{0,20}$/gi.test(_prefix_value))){
	                alert(TL1_group_errors[13]);
	                _prefix.select();
                        return false;
                    };
                    
        if((_name_value.length + _prefix_value.length) > (20 - NameSuffixMaxLen))
        {
            alert(TL1_group_errors[15]);
	                _name.select();
                        return false;
        }
                    
        _data = _data + itemdata + "&_index=" + index;
        that.SetProcess(Language.Html['011']);
        var XHR = $.ajax({
                    url: _form.attr("action") + "?_=" + new Date().getTime(),
                    type: _form.attr("method") ? _form.attr("method") : "GET",
                data: _data,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };
                    data.status = $.trim(data.status);                    
					if (data.status == 98) {
						that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
						return;
					}
                    if (data.status == 0) {
                        that.SetProcessOK(TL1_group_errors[0]);
                        Control.RefreshModule();
                        that.bindInput();
                    } else {
                        that.SetProcessDone(TL1_group_errors[data.status]);
                    }
                },
                error: function (data, textStatus) {
                    that.SetProcessDone(Language.Html['002']);
                }
            }).done(function (data, textStatus, jqXHR) {
                jqXHR = null;
                XHR = null;
            });
    });
};

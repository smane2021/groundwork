
Pages.setting_relay_content=function(data){
 var uls = $("div.table-body ul");
 var that = this;
var settemplates = $("#Set_Relay").attr("template");
$(".table-body").on("click", "a.btn_mod", function () {
        var post = $(this).attr("post");
		var parent = $(this).parents("ul");
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
		Control.PromptEvent.Pop({ cover: true, title: parent.parents(".table-list").prev().find("span").text(), fn: that.SetRelayName, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args }, html: "<div id='SetAlarmLevelRelay' class='Poplevelrelay'></div>", Class: "Pop-relay-alarm" });
	}).on("click", "ul,i,a", function () {
    if($(this).hasClass("btn_set_disabled")){
        uls.removeClass("select");
      }else{
	    uls.removeClass("select");
       // $(this).addClass("select");
       }
	});
}
   
Pages.SetRelayName=function(datas){
    var that = Pages;
	var content = $("#SetAlarmLevelRelay");
	content.html(datas.template);
	var info = content.find(".info").addClass("setting");
	var form = content.find("form");
	var lab=content.find("label").eq(0);
	var inputs = form.find(".set_value");
	var name=inputs.attr("name");
	var SetVal=datas.modifydatas[name];
	var DefVal;
	 if (SetVal.split("-").length > 1) {
	    DefVal=SetVal.split("-")[SetVal.split("-").length-2]+"-";
        SetVal = SetVal.split("-")[SetVal.split("-").length-1];
        inputs.css({"width":"150px"});
    } else {
        SetVal = SetVal.split("-");
    }
    lab.text(DefVal);
	inputs.val(SetVal);
	
	$("#SetAlarmLevelRelay input").off("focus blur keydown").on("focus", function () {
		//如果redonly为true，一般为时间设置，即不可以使用backspace
		if (!$(this).attr("readonly")) {
			Configs.Backspace = true;
		}
	}).on("blur", function () {
		Configs.Backspace = false;
	}).on("keydown", function () {
		if ($(this).hasClass("set_value")) {
			Configs.Backspace = true;
		};
	});
	
	
	content.find("a.btn_set").off().on("click", function () {
		if ($(this).hasClass("btn_set_disabled")) { return false; }
		info.html(Language.Html['011']).addClass("setting");
		var check = false;
		var vals = "";
			var item = inputs;
			var pname = item.attr("pname");
			var deftext=lab.text();
			if(deftext!=""){
			    deftext=deftext;
			}
			if (pname == "sSignalName_en") {
				var sNewName = item.val();
				var iNameLength = 0;
				for (var iIndex = 0; iIndex < sNewName.length; iIndex++) {
					if (escape(sNewName[iIndex]).indexOf("%u") != -1) {
						iNameLength += 3; //Chinese char
					} else {
						iNameLength += 1; //Ascii
					}
				}
				if (iNameLength > 32) {
					check = true;
					info.html(Language.Html["061"]);
				}
			} 
			vals += "&" + item.attr("pname") + "=" + encodeURIComponent(deftext+item.val());
		vals += "&sessionId=" + datas.args["sessionId"];
		if (check) {
			info.removeClass("setting");
			return false;
		}
		$(this).addClass("btn_set_disabled");
		var $this = $(this);
		var XHR = $.ajax({
			timeout: 10000,
			url: form.attr("action") + "?_=" + new Date().getTime(),
			type: form.type,
			data: datas.post + vals,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["001"]);
					return false;
				}
				if (data.status == 98) {
					info.html(Language.Html['019']);
					return;
				}
				if (data.status == 1) {
					info.addClass("setting");
					//that.SetProcess("");
					that.SetProcessOK(Language.Setting[data.status]);
					$("#PromptPopClose").click();
					that.SetAlarmScroll = Configs.MainScrollHeight;
					Control.RefreshModule();
				}
				info.html(Language.Setting[data.status]);
			},
			error: function (data, textStatus) {
				info.html(Language.Html['016']).removeClass("setting");
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			$this.removeClass("btn_set_disabled");
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
}


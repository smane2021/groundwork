﻿Pages.setting_clear_data = function (data) {
    var that = this;
	var lang = Language["setting_clear_data"];
    var clearErrors = lang["errors"];
	var tips = lang["tips"];
    var _equipList = $("#_equipList");
     //自定义下拉列表事件
    Pages.BindSelect();
    $("#btn_cleardata").off().on("click", function () {
        var form = $(this).closest("form");
        var value = $(".input_select").attr("data-index");
        var posts = "equip_ID=-2&signal_type=-2&signal_id=-2&control_value=" + value + "&control_type=9&sessionId=" + data.args["sessionId"] + "&language_type=" + data.args["language_type"];
        that.SetProcess(tips[0]);
        var XHR = $.ajax({
            timeout: 30000,
            url: form.attr("action") + "?_=" + new Date().getTime(),
            data: posts,
            success: function (data, textStatus, jqXHR) {
                try {
                    var data = jQuery.evalJSON(data);
                } catch (err) {
                    that.SetProcessDone(Language.Html["001"]);
                    return false;
                }
                if (data.status == 98) {
                    that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
                } else if (data.status == 1) {
                    that.SetProcessOK(clearErrors[data.status]);
                } else {
                    that.SetProcessDone(clearErrors[data.status]);
                }
            },
            error: function (data, textStatus) {
                that.SetProcessDone(Language.Html['016'] + Language.Html["055"]);
            }
        }).always(function (jqXHR, textStatus, errorThrown) {
            jqXHR = null;
            textStatus = null;
            errorThrown = null;
            XHR = null;
        });
        return false;
    });
    Configs.Data.polling = false;
    $(".table-container .table").css({"background-color":"#4B4B4B"});
    if(jQuery.cookie("systeminfo")!=1){
        $(".table-container").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table-container").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
};
Pages.setting_feed=function(data){
    var that = this;
    var Trend = $("#Trend");
    var isIE = /IE/gi.test(Configs.Browser);
     //绘制电流表
    var MeterDatas = $.extend(true, {}, data.data['PAmeter'], {
        center: [95, 90],
        alarm: {
            outer: {
                color: ["#a2ee00", "#ffae00", "#e35500"],
                radius: 65
            }
        },
        value: {
            toFix: [0],
            unit: ["%"]
        }
    });
    var VADatas = {};
    VADatas = $.extend(true, {}, VADatas, data.data['VAmeter']);
    //绘制电压表
    var st = data.data['system_type'];
    if (st == 0) {
        VADatas["alarm"]["outer"]["value"] = data.data['system_voltage']["value"];
    } else if (st == 1) {
        VADatas["alarm"]["outer"]["value"] = data.data['system_voltage']["value_24V"];
    };
    VADatas.alarm.outer.color = ["#e35500", "#ffae00", "#a2ee00", "#ffae00", "#e35500"];
    VADatas.finger = {};
    VADatas.finger.speed = 50;
    //调用Trend函数,绘制趋势图曲线
    if (!Control.isPolling && isIE) {
        Control.PromptEvent.Loading({ target: "Trend", html: Language.Html["018"] });
        Control.PromptEvent.Loading({ target: "batteryThermometer", html: Language.Html["018"] });
        Control.PromptEvent.Loading({ target: "VAmeter", html: Language.Html["018"] });
        Control.PromptEvent.Loading({ target: "PACurrent", html: Language.Html["018"] });
        setTimeout(function () {
            Chart.drawMeter("VAmeter", VADatas);
            Chart.drawMeter("PACurrent", MeterDatas);
            that.HomeTrend();
            Control.PromptEvent.LoadingRemove({ remove: "TrendLoading" });
            Control.PromptEvent.LoadingRemove({ remove: "batteryThermometer" });
        }, 50);
    } else {
        Chart.drawMeter("VAmeter", VADatas);
        Chart.drawMeter("PACurrent", MeterDatas);
        that.HomeTrend();
    }
}
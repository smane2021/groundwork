Pages.setting_custominputs  = function (data) {
    var that = this;
        var datalist;
	//列表模板
	var Set_CustomInput = $("#Set_CustomInput");
	var listTemplate = doT.templateChild(Set_CustomInput.attr("template"));
		//打开列表
	function openList(target){
		var datas ={
			"args": data.args,
			"jQuery": jQuery,
			"$" : jQuery,
			"Configs" : Configs,
			"Pages" : Pages,
			"Control" :Control,
			"Validates":Validates,
			"window":window
		};
		var item = target.attr("data").split("|");
		var datalist = data.data[item[0]][item[1]][item[2]].list;
		datas["list"] = datalist;
		var doTtmpl = template.compile(listTemplate)(datas);

		target.html(doTtmpl).show();
		that.Setting(data);
		that.bindInput();
	}
	//打开或隐藏设置列表
	Set_CustomInput.off('click').on("click","div.table-ex",function(){
		var p = $(this).parent("div.table-container");
		var next = $(this).next("div.table-content");
		next.toggle();
		p.toggleClass("border-bn");
		var span = $(this).find("span");
		var nextHasList = next.attr("data");
		if (next.is(":hidden")) {
			//close
			span.removeClass("title-sl").addClass("title-zk");
			that.DelArrayValue(Configs.PowerSystem, [Number(next.attr("index"))]);
			if(nextHasList){
				next.html("");
			}
		} else {
			//open
			span.removeClass("title-zk").addClass("title-sl");
			Configs.PowerSystem.push(Number(next.attr("index")));
			if(nextHasList){
				openList(next);
			}
		}
	});
	if(jQuery.cookie("systeminfo")!=1){
        $(".table li").css({"padding-right":"50px"});
        $(".table-container").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
    }else{
        $(".table li").css({"padding-right":"0px"});
        $(".table-container").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
    }
	var uls = $("div.table-body ul");
	var settemplates = $("#Set_CustomInput").attr("template");
    $(".table-body").on("click", "a.btn_mod", function () {
        var post = $(this).attr("post");
		var parent = $(this).parents("ul");
		var title=parent.parents(".table-list").prev().find("span").text();
		var sdtitle=parent.find(".t-1").find("span").text();
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
		var inputmessage = jQuery.evalJSON($(this).attr("inputmessage"));
        var obj = modifydatas;
        var na4 = $(this).attr("na4");
        var lang = $(this).attr("lang");
		Control.PromptEvent.Pop({ cover: true, title:title+" > "+sdtitle , fn: that.SetInputBlock, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args, inputmessage:inputmessage,data:data.data }, html: "<div id='SetInputBlock' class='Poplevelrelay'></div>", Class: "Pop-CustomInput" });
		var SV1 = $('[name="Signal_Value1"]');
		var SV2 = $('[name="Signal_Value2"]');
		var SV3 = $('[name="Signal_Value3"]');
		for (var item in modifydatas) {
		    if (item != "toJSONString") {
		        datalist = obj[item][7];
		        var html="";

		        for (var i = 0; i < datalist[0][9].length; i++) {
                        html += '<li><a href="javascript:void(0)" rel=' + i + '>'+datalist[0][9][i][lang]+'</a></li>';
                }
                SV1.attr("data-index",0).text(datalist[0][9][0][lang]);
                SV1.next().append(html);
                $('[name="sSignalVal1"]').text(SV1.next().find("li").eq(datalist[0][3]).text());
                if(datalist[0][3]==1){
                    $('[name="sSignalMessage1"]').text(inputmessage[1])
                }
                SV1.text(SV1.next().find("li").eq(datalist[0][3]).text());
                SV1.attr("data-index",datalist[0][3]);
                SV1.attr("Signal_Type",datalist[0][1]);
                SV1.attr("Signal_ID",datalist[0][2]);
                SV1.attr("Signal_ValueType",datalist[0][4]);
                html="";
                
                for (var i = 0; i < datalist[1][9].length; i++) {
                        html += '<li><a href="javascript:void(0)" rel=' + i + '>'+datalist[1][9][i][lang]+'</a></li>';
                }
                SV2.attr("data-index",0).text(datalist[1][9][0][lang]);
                SV2.next().append(html);
                $('[name="sSignalVal2"]').text(SV2.next().find("li").eq(datalist[1][3]).text());
                if(datalist[1][3]==1){
                    $('[name="sSignalMessage2"]').text(inputmessage[2])
                }
                SV2.text(SV2.next().find("li").eq(datalist[1][3]).text());
                SV2.attr("data-index",datalist[1][3]);
                SV2.attr("Signal_Type",datalist[1][1]);
                SV2.attr("Signal_ID",datalist[1][2]);
                SV2.attr("Signal_ValueType",datalist[1][4]);
                html="";
                
                for (var i = 0; i < datalist[3][9].length; i++) {
                        html += '<li><a href="javascript:void(0)" rel=' + i + '>'+datalist[3][9][i][lang]+'</a></li>';
                }
                SV3.attr("data-index",0).text(datalist[3][9][0][lang]);
                SV3.next().append(html);
                $('[name="sSignalVal3"]').text(SV3.next().find("li").eq(datalist[3][3]).text());
                if(datalist[3][3]!=0){
                    $('[name="sSignalMessage3"]').text(inputmessage[3][datalist[3][3]-1])
                }
                SV3.text(SV3.next().find("li").eq(datalist[3][3]).text());
                SV3.attr("data-index",datalist[3][3]);
                SV3.attr("Signal_Type",datalist[3][1]);
                SV3.attr("Signal_ID",datalist[3][2]);
                SV3.attr("Signal_ValueType",datalist[3][4]);
                html="";

                Pages.BindSelect();
                
                
		    }
		}
		
		
    }).on("click", "ul,i,a", function () {
       if($(this).hasClass("btn_set_disabled")){
           uls.removeClass("select");
       }else{
	       uls.removeClass("select");
           $(this).addClass("select");
       }
    });
}

Pages.SetInputBlock=function(datas){
    var that = Pages;
	var content = $("#SetInputBlock");
	content.html(datas.template);
	var info = content.find(".info").addClass("setting");
	var form = content.find("form");
    var inputs = form.find(".set_newval");
    
    $("#SetInputBlock input").off("focus blur keydown").on("focus", function () {
		//如果redonly为true，一般为时间设置，即不可以使用backspace
		if (!$(this).attr("readonly")) {
			Configs.Backspace = true;
		}
	}).on("blur", function () {
		Configs.Backspace = false;
	}).on("keydown", function () {
		if ($(this).hasClass("set_value")) {
			Configs.Backspace = true;
		};
	});

    content.find("a.btn_set").off().on("click", function () {
        var setvalue=[];
        var inputmessage=datas.inputmessage;
		if ($(this).hasClass("btn_set_disabled")) { return false; }
		info.html(Language.Html['011']).addClass("setting");
		$("#PromptPopClose").hide();
		var vals = "";
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
		    var item = inputs.eq(i);
		    var validate = jQuery.evalJSON(item.attr("validate"));
		    var pname = item.attr("pname");
		    var sNewName="";
		    sNewName=item.attr("data-index");
		    //获取语言类型（0：英文、1：中文）
		    var langtype=datas.post.split("&equip_ID")[0].split("=")[1];
		    if(sNewName==null||sNewName==undefined||sNewName==""){ 
		        vals += "&" + item.attr("name") + "=" + encodeURIComponent(-1);
		    }else{
		        vals += "&" + item.attr("name") + "=" + encodeURIComponent(sNewName);
		        setvalue.push(encodeURIComponent(sNewName))
		    }
		    
		    if(item.attr("Signal_Type")==""||item.attr("Signal_Type")==null){
		        item.attr("Signal_Type",-1);
		    }
		    if(item.attr("Signal_ID")==""||item.attr("Signal_ID")==null){
		        item.attr("Signal_ID",-1);
		    }
		    if(item.attr("Signal_ValueType")==""||item.attr("Signal_ValueType")==null){
		        item.attr("Signal_ValueType",-1);
		    }
		        
		    vals+="&"+pname+"_SignalType="+item.attr("Signal_Type");
		    vals+="&"+pname+"_SignalID="+item.attr("Signal_ID");
            vals+="&"+pname+"_ValueType="+item.attr("Signal_ValueType");
		}
		vals += "&sessionId=" + datas.args["sessionId"];
		$(this).addClass("btn_set_disabled");
		var $this = $(this);
		var XHR = $.ajax({
			timeout: 20000,
			url: form.attr("action") + "?_=" + new Date().getTime(),
			type: form.type,
			data: datas.post + vals,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["001"]);
					return false;
				}
				if (data.status == 98) {
					info.html(Language.Html['019']);
					return;
				}
				if (data.status == 1) {
				    $(this).addClass("btn_set_disabled");
				    setTimeout(function(){delayload(setvalue,inputmessage,$this)},6000);
				}else{
				   info.html(Language.Setting[data.status]);
				   $("#PromptPopClose").show();
				}
			},
			error: function (data, textStatus) {
				info.html(Language.Html['016']).removeClass("setting");
				$("#PromptPopClose").show();
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
	
	function delayload(setvalue,inputmessage,$this){
	    info.html(Language.Setting[1]);	
	    $("#PromptPopClose").show();    
		info.addClass("setting");
		$this.hide();
		that.SetAlarmScroll = Configs.MainScrollHeight;
		showNewSignal(setvalue,inputmessage);		
	}
	function showNewSignal(SignalObj,inputmessage){
	    var SignalValue1=$("[name='Signal_Value1']").next().find("li").eq(SignalObj[0]).text();
	    var SignalValue2=$("[name='Signal_Value2']").next().find("li").eq(SignalObj[1]).text();
	    var SignalValue3=$("[name='Signal_Value3']").next().find("li").eq(SignalObj[2]).text();
	    var Signal_Value1=SignalObj[0];
	    var Signal_Value3=SignalObj[2];
	    $("[name='sSignalVal1']").text(SignalValue1)
	    $("[name='sSignalVal2']").text(SignalValue2)
	    $("[name='sSignalVal3']").text(SignalValue3)
	    $("[name='Signal_Value1']").attr("data-index",SignalObj[0]).text(SignalValue1)
	    $("[name='Signal_Value2']").attr("data-index",SignalObj[1]).text(SignalValue2)
	    $("[name='Signal_Value3']").attr("data-index",SignalObj[2]).text(SignalValue3)
	    if(SignalObj[0]==1){
            $('[name="sSignalMessage1"]').text(inputmessage[1])
	    }else{
	        $('[name="sSignalMessage1"]').text("")
	    }
	    if(SignalObj[1]==1){
            $('[name="sSignalMessage2"]').text(inputmessage[2])
	    }else{
	        $('[name="sSignalMessage2"]').text("")
	    }
	    if(SignalObj[2]!=0){
            $('[name="sSignalMessage3"]').text(inputmessage[3][SignalObj[2]-1])
	    }else{
	        $('[name="sSignalMessage3"]').text("")
	    }
	}
}
Pages.HomePage = function (data) {
    Configs.wrapper.show();
    //手动检查模块是否加载OK，否则reload()
    var Modules = Configs.Modules.Renders;
    var isOK = true;
    for (var item in Modules) {
        if ($.trim($("#" + Modules[item]).html()) == "") {
            isOK = false;
            break;
        }
    }
    if (!isOK) {
        setTimeout(function () {
            location.reload();
        }, 500);
        return false;
    };
    var that = this;
    var Trend = $("#Trend");
    var isIE = /IE/gi.test(Configs.Browser);
    if (!Control.isPolling || Configs.wrapper.is(":hidden")) {
        Control.GetTrend();
        Configs.ClickHomeNum++;
    };
    if(jQuery.cookie("Verizon_Mode")==1){
        $(".h-s-088").find("i").hide();
    }
    var moveobj=[".h-s-line6",".h-s-06",".h-s-line7",".h-s-value3",".h-s-value3_1",".h-s-value5",".h-s-value4"];
    var moveobj2=[".h-s-value1",".h-s-value1_1",".h-s-02",".h-s-04",".h-s-value2",".h-s-value2_1",".h-s-line5",".h-s-line11",".h-s-line12",".h-s-line13",".h-s-value7",".h-s-value8",".h-s-10-1"];
    var batteryimg=".h-s-08-f4,.h-s-08-f3,.h-s-08-f2,.h-s-08-f1,.h-s-08-f0,.h-s-08-c4,.h-s-08-c3,.h-s-08-c2,.h-s-08-c1,.h-s-08-c0";
    
    if(jQuery.cookie("systeminfo")==1){
        NarrowChar();
        $("#siderinfo").css({right:'0px'});
	    $(".home-system,.home-loadTrend,.table-container,.extbackground").css({width:'917px'});
	    $(".table-title").css({width:'897px'});
	    $(".curve-trend").css({"width":"650px"});
	    if(!$(".TL1-signal-summary").length>0){
    	    $(".table-container .table").css({width:"1143"});
    	}
    	$(".home-loadTrendContent").css({"margin-left":"35px"});
	    $(".home-system-l").css({width:'694px'});
        $(".h-s-07").css({"left":"610px"});
        $(".h-s-09").css({"left":"604px"});
        $(".h-s-line8").css({"width":"150px","left":"463px"});
        $(".h-s-line2").css({"width":"520px"});
        $(".h-s-line1").css({"width":"270px"});
        $(".h-s-line11-1").css({"width":"239px"});
        $(batteryimg).css({"left":"414px"});
        $("#System_DC .table-body ul").css({width:"441px"});
        $("#siderinfo i").removeClass("hiderti");
    }else{
        WideChar();
        $("#siderinfo").css({right:'-247px'});
        $(".curve-trend").css({"width":"950px"});
        $(".home-loadTrendContent").css({"margin-left":"0px"});
	    $(".home-system,.home-loadTrend,.table-container,.extbackground").css({width:'1163px'});
	    $(".table-title").css({width:'1143px'});
	    $(".home-system-l").css({width:'939px'});
     	$(".h-s-07").css({"left":"850px"});
    	$(".h-s-09").css({"left":"844px"});
    	$(".h-s-line10").css({"left":"422px"});
    	$(".h-s-line8").css({"width":"240px","left":"613px"});
    	$(".h-s-line2").css({"width":"760px"});
    	$(".h-s-line1").css({"width":"340px"});
    	$(".h-s-line11-1").css({"width":"309px"});
    	$(batteryimg).css({"left":"564px"});
    	$("#System_DC .table-body ul").css({width:"564px"});
    	for(var i=0;i<moveobj.length;i++){
    	    if($(moveobj[i]).prop("className"))
    	    $(moveobj[i]).css({"left":$(moveobj[i]).position().left+150});
    	}
    	for(var j=0;j<moveobj2.length;j++){
    	    if($(moveobj2[j]).prop("className"))
    	    $(moveobj2[j]).css({"left":$(moveobj2[j]).position().left+70});
    	}
    	if(!$(".TL1-signal-summary").length>0){
    	    $(".table-container .table").css({width:"1143"});
    	}
	    $("#siderinfo i").addClass("hiderti");
    }
    $("#siderinfo i").off().on("click",function(){
        $("#popup").hide();
        if($(this).hasClass("hiderti")){
            NarrowChar();
            $("#siderinfo").animate({right:'0px'});
            $(".home-loadTrendContent").animate({"margin-left":"35px"});
            $(".curve-trend").animate({"width":"650px"});
	        $(".CabinetBox").animate({width:"620px"});
    	    $(".home-system-l").animate({width:'694px'});
    	    $(".hybrid-handle").animate({right:'10px'});
    	    $(".h-s-07").animate({"left":"610px"});
    	    $(".h-s-09").animate({"left":"604px"});
    	    $(".h-s-line10").animate({"left":"352px"});
    	    $(".h-s-line8").animate({"width":"150px","left":"463px"});
        	$(".h-s-line2").animate({"width":"520px"});
            $(".h-s-line1").animate({"width":"270px"});
            $(".h-s-line11-1").animate({"width":"239px"});
            $(batteryimg).animate({"left":"414px"});
            $("#System_DC .table-body ul,#System_EIB .table-body ul,#System_User_Def .table-body ul,#System_SMIO .table-body ul,#System_AC .table-body ul").animate({width:"441px"});
    	    for(var i=0;i<moveobj.length;i++){
    	        if($(moveobj[i]).prop("className"))
    	        $(moveobj[i]).animate({"left":$(moveobj[i]).position().left-150});
    	    }
    	    for(var j=0;j<moveobj2.length;j++){
    	        if($(moveobj2[j]).prop("className"))
    	        $(moveobj2[j]).animate({"left":$(moveobj2[j]).position().left-70});
    	    }
            $(this).removeClass("hiderti");
            jQuery.cookie("systeminfo", "1", { path: "/" });//显示
            
            $(".table li").animate({"padding-right":"0px"});
            if($("#SettingNMS").length>0){
    	        $(".set-form").animate({width:'897px'});
    	    }else{
    	        $(".set-form").animate({width:'917px'});
    	    }
    	    if($(".TL1-signal-summary").length>0){
                $(".TL1-signal-summary,.extbackground").animate({width:'917px'});
                $("#InventoryTable").animate({width:'897px'});
                $(".set-form-TL1-signal").animate({width:'592px'});
                $(".set-form-TL1-signal .table-title").animate({width:'572px'});
                $(".set-form-TL1-signal p,form > span").animate({"padding-left":"0"});
            }else{
                $(".home-system,.home-loadTrend,.table-container,.alarm-summary,.history-summary,.history-list,.hybrid,.TL1-signal-summary,.system-summary,.system-list,.system-list ul,.system-battery-summary,.system-battery-list,.system-battery-item,.power-split-summary,.system-no-data,.extbackground").animate({width:'917px'});
                $(".table-title,.table-container .table,#InventoryTable,#thead-fixed,#thead-fixed table,.history-list table,#CabinetSet,#history_t2").animate({width:'897px'});
            }
    	    if($("#Setting_TL1_Group").length>0){
    	        $(".btn_2,.btn_2_disabled").animate({"margin-right":"67px"});
    	    }
    	    if($(".system-battery-summary").length>0){
    	        if(jQuery.cookie("Verizon_Mode")==1){
    	            $(".system-meterInfo").animate({width:'740px'});
                    $(".system-meterInfo .table-a").animate({width:'720px'});
    	        }else{
    	            $(".system-meterInfo").animate({width:'493px'});
                    $(".system-meterInfo .table-a").animate({width:'473px'});
                }
    	    }else{
                $(".system-meterInfo").animate({width:'560px'});
                $(".system-meterInfo .table-a").animate({width:'540px'});
            }
            if($("#InventoryTable").length>0){
                 $("table th").not($('table th').eq(0)).animate({"padding-right":"30px"});
                 $("table td").not($('table td').eq(0)).animate({"padding-right":"30px"});
            }
        }else{
            WideChar();
            $(".curve-trend").animate({"width":"950px"});
    	    $("#siderinfo").animate({right:'-247'});
	        $(".CabinetBox").animate({width:"851px"});
    	    $(".home-system-l").animate({width:'939px'});
    	    $(".home-loadTrendContent").animate({"margin-left":"0px"});
    	    $(".hybrid-handle").animate({right:'100px'});
        	$(".h-s-07").animate({"left":"850px"});
    	    $(".h-s-09").animate({"left":"844px"});
    	    $(".h-s-line10").animate({"left":"422px"});
    	    $(".h-s-line8").animate({"width":"240px","left":"613px"});
    	    $(".h-s-line2").animate({"width":"760px"});
    	    $(".h-s-line1").animate({"width":"340px"});
    	    $(".h-s-line11-1").animate({"width":"309px"});
    	    $(batteryimg).animate({"left":"564px"});
    	    $("#System_DC .table-body ul,#System_EIB .table-body ul,#System_User_Def .table-body ul,#System_SMIO .table-body ul,#System_AC .table-body ul").animate({width:"564px"});
            for(var i=0;i<moveobj.length;i++){
                if($(moveobj[i]).prop("className"))
    	        $(moveobj[i]).animate({"left":$(moveobj[i]).position().left+150});
    	    }
    	    for(var j=0;j<moveobj2.length;j++){
    	        if($(moveobj2[j]).prop("className"))
    	        $(moveobj2[j]).animate({"left":$(moveobj2[j]).position().left+70});
    	    }
    	    $(this).addClass("hiderti");
    	    jQuery.cookie("systeminfo", "0", { path: "/" });//不显示
    	    if(!$("#set_other").length>0){
    	            if($("#Set_Relay,#Set_PowerSystem,#Set_Charge,#Set_Rect,#SettingNMS,#SetDIAlarm,#Set_Converter,#SetPowerSplit").length>0){
    	                $(".table li").animate({"padding-right":"50px"});
    	            }else if($(".TL1-signal-summary").length>0||$("#Set_ShuntsSystem").length>0){
    	                $(".table li").animate({"padding-right":"0"});
    	            }else{
    	                $(".table li").animate({"padding-right":"60px"});
    	            }
    	    }
    	    if($("#SettingNMS").length>0){
    	        $(".set-form").animate({width:'1143px'});
    	    }else{
    	        $(".set-form").animate({width:'1163px'});
    	    }
    	    if($(".TL1-signal-summary").length>0){
    	        $(".TL1-signal-summary,.extbackground").animate({width:'1163px'});
    	        $("#CabinetSet,#InventoryTable").animate({width:'1143px'});
    	        $(".set-form-TL1-signal").animate({width:'838px'});
    	        $(".set-form-TL1-signal .table-title").animate({width:'818px'});
    	        $(".set-form-TL1-signal p,form > span").animate({"padding-left":"200px"});
    	    }else{
    	        $(".home-system,.home-loadTrend,.table-container,.alarm-summary,.history-summary,.history-list,.hybrid,.TL1-signal-summary,.system-summary,.system-list,.system-list ul,.system-battery-summary,.system-battery-list,.system-battery-item,.power-split-summary,.system-no-data,.extbackground").animate({width:'1163px'});
    	        $(".table-title,.table-container .table,#CabinetSet,#InventoryTable,#thead-fixed,#thead-fixed table,.history-list table,#history_t2").animate({width:'1143px'});
    	    }
    	    
    	    if($("#Setting_TL1_Group").length>0){
    	        $(".btn_2,.btn_2_disabled").animate({"margin-right":"152px"});
    	    }
    	     if($(".system-battery-summary").length>0){
    	        if(jQuery.cookie("Verizon_Mode")==1){
    	            $(".system-meterInfo").animate({width:'986px'});
                    $(".system-meterInfo .table-a").animate({width:'960px'});
    	        }else{
    	            $(".system-meterInfo").animate({width:'739px'});
                    $(".system-meterInfo .table-a").animate({width:'719px'});
                }
    	    }else{
                $(".system-meterInfo").animate({width:'806px'});
                $(".system-meterInfo .table-a").animate({width:'786px'});
            }
            if($("#InventoryTable").length>0){
                 $("table th").not($('table th').eq(0)).animate({"padding-right":"70px"});
                 $("table td").not($('table td').eq(0)).animate({"padding-right":"70px"});
            }
    	}
    });
    setTimeout('$("#siderinfo i").css({"top":$("#siderinfo").height()/2-40});',100);
    //首页右上角告警状态事件
    var alarmTip = $("#alarmTip");
    alarmTip.off().on("click", function () {
        Configs.Alarm.expand = true;
        Configs.Alarm.show = true;
        Configs.Alarm.tab=0;
        Control.SetLayout(true);
        $(".alarmPop").eq(0).addClass("current");
        $(".alarmPop").eq(0).siblings("li").removeClass("current");
	if(document.getElementById("PromptTip_alarm")){
		Control.PromptEvent.Tip({ target: "alarm", text: Language.Html['001'],Class:"alarm-prompt"});
	}
	var alarmLoading = $(document.getElementById("PromptLoading_alarm"));
	if(alarmLoading[0]){
		alarmLoading.css({"top":70});
	}
        return false;
    });
    
    //绘制温度计
    var thermometerData = {
        type: 1,
        imgs: {
            left: 55,
            top: 25
        },
        alarm: {
            color: ["#F58800", "#82C013", "#ffae00", "#F58800"]
        },
        scale: {
            value: [-50, -25, 0, 25, 50, 75]
        }
    };
    var unittype = data.data['drawThermometer']['unit'][0];//温度类型0-摄氏度 1-华氏度
    jQuery.cookie("drawThermometer",unittype,{path:"/"});
    if(unittype==1){
        for(var i=0;i<thermometerData['scale']['value'].length;i++){
            thermometerData['scale']['value'][i]=thermometerData['scale']['value'][i]*1.8+32;
        }
    }
    
    var ThermometerDatas = $.extend(true, {}, thermometerData, data.data['drawThermometer']);
    //绘制电流表
    var MeterDatas = $.extend(true, {}, data.data['PAmeter'], {
        center: [95, 90],
        alarm: {
            outer: {
                color: ["#a2ee00", "#ffae00", "#e35500"],
                radius: 50
            }
        },
        value: {
            toFix: [0],
            unit: ["%"]
        }
    });
    var VADatas = {};
    VADatas = $.extend(true, {}, VADatas, data.data['VAmeter']);
    //绘制电压表
    var st = data.data['system_type'];
    if (st == 0) {
        VADatas["alarm"]["outer"]["value"] = data.data['system_voltage']["value"];
    } else if (st == 1) {
        VADatas["alarm"]["outer"]["value"] = data.data['system_voltage']["value_24V"];
    };
    VADatas.alarm.outer.color = ["#e35500", "#ffae00", "#a2ee00", "#ffae00", "#e35500"];
    VADatas.finger = {};
    VADatas.finger.speed = 50;
    //调用Trend函数,绘制趋势图曲线
    if (!Control.isPolling && isIE) {
        Control.PromptEvent.Loading({ target: "Trend", html: Language.Html["018"] });
        Control.PromptEvent.Loading({ target: "batteryThermometer", html: Language.Html["018"] });
        Control.PromptEvent.Loading({ target: "VAmeter", html: Language.Html["018"] });
        Control.PromptEvent.Loading({ target: "PACurrent", html: Language.Html["018"] });
        setTimeout(function () {
            Chart.drawThermometer("batteryThermometer", ThermometerDatas);
            Chart.drawMeter("VAmeter", VADatas);
            Chart.drawMeter("PACurrent", MeterDatas);
            that.HomeTrend();
            Control.PromptEvent.LoadingRemove({ remove: "TrendLoading" });
            Control.PromptEvent.LoadingRemove({ remove: "batteryThermometer" });
            Changeposition();
	        V_Changeposition();
        }, 50);
    } else {
        Chart.drawThermometer("batteryThermometer", ThermometerDatas);
        Chart.drawMeter("VAmeter", VADatas);
        Chart.drawMeter("PACurrent", MeterDatas);
        that.HomeTrend();
        Changeposition();
	    V_Changeposition();
    }
    //绑定首页各图标的单击事件
    this.bindEvent("click", ".home-system-l", "a");
    //设置首页alarm图标状态
    var alarmTip = $("#alarmTip"), alarmList = $("#alarmList"),alarmListItems=alarmList.find("ul"),alarmtype =$("#alarm")
	if(alarmList[0]){
	    if(alarmtype.find(".alarm-c").find("em").text()>0){
	        alarmTip.attr("class","h-s-alarm3");
	    }else if(alarmtype.find(".alarm-m").find("em").text()>0){
	        alarmTip.attr("class","h-s-alarm");
	    }else if(alarmtype.find(".alarm-o").find("em").text()>0){
	        alarmTip.attr("class","h-s-alarm1");
	    }else{
	        alarmTip.attr("class","h-s-ok");
	    }
		//alarmTip.attr("class", alarmListItems.length == 0 ? "h-s-ok" : "h-s-alarm");
	}
    //预存solar,converter,用于rectifier、converter、solar三个页面，对solar、converter菜单是否显示的判断
    Configs.Commons.converter = data.data.system.number[0];
    Configs.Commons.solar = data.data.system.number[1];
    Configs.Commons.rectifier = [data.data.system.number[2], data.data.system.number[3], data.data.system.number[4]];
    //Configs.Data.polling = false;
    $("#alarm").find("div.PromptLoading2").css({ "visibility": "visible" });
    //绑定温度计弹出温度趋势图
    $("div.home-system-r2").off().on("click", function () {
        var setting = {
            canvas: {
                width: 540,
                height: 180
            },
            Y: {
                unit: "°C",
                value: [-50, -25, 0, 25, 50, 75],
                follow: [true, -50, 75]
            },
            unit: "°C",
            hasMax: false
        };
        var unittype = data.data['drawThermometer']['unit'][0];//温度类型0-摄氏度 1-华氏度
        if(unittype==1){
            for(var i=0;i<setting['Y']['value'].length;i++){
                setting['Y']['value'][i]=setting['Y']['value'][i]*1.8+32;
            }
            setting['Y']['follow'][1]=setting['Y']['value'][0];
            setting['Y']['follow'][2]=setting['Y']['value'][5];
            setting['Y']['unit']="°F";
            setting['unit']="°F";
        }
        Control.PromptEvent.Pop({ cover: true, title: Language.Chart["004"], fn: that.GetCurveData, data: { name: "Ambient_Temp", setting: setting }, html: "<div id='PopAmbient_TempCurve' class='PopCurve'></div>" });
    });
    function Changeposition() {
        var dataarray = data.data['PAmeter']['alarm']['outer']['value'];//电流数据
        var arr = dataarray,
          result = [],
          i,
          j,
          len = arr.length;
         for(i = 0; i < len; i++){
          for(j = i + 1; j < len; j++){
           if(arr[i] === arr[j]){
            j = ++i;
           }
          }
          result.push(arr[i]);
        }
        if(result[0]==result[1]){
                   var html='<span style="display:none;position: absolute; text-align: center; left: 12.6708px; top: 101px;">40.0</span>';
                   var VAmeterhtml=$(".VAmeter-scale").html();
                   $(".VAmeter-scale").children().remove();
                   $(".VAmeter-scale").append(html+VAmeterhtml);
        }else if(result[result.length-1]==result[result.length-2]){
                 var html='<span style="display:none;position: absolute; text-align: center; left: 148.952px; top: 115.5px;">60.0</span>';
                   var VAmeterhtml=$(".VAmeter-scale").html();
                   $(".VAmeter-scale").children().remove();
                   $(".VAmeter-scale").append(VAmeterhtml+html);
        }
        var sObj = $(".PACurrent-scale").find("span");//电流
        var range = result[result.length - 1] - result[0];
        var rangehalf = result[0] + range / 2;
        for (var i = 0; i < result.length - 1; i++) {
            if (result[i] < rangehalf && result[i + 1] < rangehalf) {
                if (-1 <= result[i] - result[i + 1] && result[i] - result[i + 1] <= 0) {
                    var offsettop1 = sObj[i].offsetTop + 5;
                    var offsettop2 = sObj[i + 1].offsetTop - 5;
                    sObj.eq(i).css({ "top": "" + offsettop1 + "px" });
                    sObj.eq(i + 1).css({ "top": "" + offsettop2 + "px" });
                }
            } else {
                if (-1 <= result[i] - result[i + 1] && result[i] - result[i + 1] <= 0) {
                    var offsettop1 = sObj[i].offsetTop - 5;
                    var offsettop2 = sObj[i + 1].offsetTop + 5;
                    sObj.eq(i).css({ "top": "" + offsettop1 + "px" });
                    sObj.eq(i + 1).css({ "top": "" + offsettop2 + "px" });
                }
            }
        }
   }
   function V_Changeposition() {
        var sObj = $(".VAmeter-scale").find("span");//电压
        var dataarray = data.data['system_voltage']['value'];//电压数据
        var arr = dataarray,
          result = [],
          i,
          j,
          len = arr.length;
         for(i = 0; i < len; i++){
          for(j = i + 1; j < len; j++){
           if(arr[i] === arr[j]){
            j = ++i;
           }
          }
          result.push(arr[i]);
         }
        var range = result[result.length - 1] - result[0];
        var rangehalf = result[0] + range / 2;
        for (var i = 0; i < result.length - 1; i++) {
            if (result[i] < rangehalf && result[i + 1] < rangehalf) {
                if (-1 <= result[i] - result[i + 1] && result[i] - result[i + 1] <= 0) {
                    var offsettop1 = sObj[i].offsetTop + 5;
                    var offsettop2 = sObj[i + 1].offsetTop - 5;
                    sObj.eq(i).css({ "top": "" + offsettop1 + "px" });
                    sObj.eq(i + 1).css({ "top": "" + offsettop2 + "px" });
                }
            } else {
                if (-1 <= result[i] - result[i + 1] && result[i] - result[i + 1] <= 1) {
                    var offsettop1 = sObj[i].offsetTop - 5;
                    var offsettop2 = sObj[i + 1].offsetTop + 5;
                    sObj.eq(i).css({ "top": "" + offsettop1 + "px" });
                    sObj.eq(i + 1).css({ "top": "" + offsettop2 + "px" });
                }
            }
        }
    }
    function WideChar(){
        Configs.TrendCvWidth=900;
        var bigcharindexarry=[26,27,28,39];
        Configs.ChartdrawCurve_Y.splice(0,Configs.ChartdrawCurve_Y.length);
        for(var i=0;i<bigcharindexarry.length;i++){
            Configs.ChartdrawCurve_Y.push(bigcharindexarry[i]);
        }
        that.HomeTrend();
    }
    function NarrowChar(){
        Configs.TrendCvWidth=600;
        var bigcharindexarry=[17,19,28,28];
        Configs.ChartdrawCurve_Y.splice(0,Configs.ChartdrawCurve_Y.length);
        for(var i=0;i<bigcharindexarry.length;i++){
            Configs.ChartdrawCurve_Y.push(bigcharindexarry[i]);
        }
        that.HomeTrend();
    }
};
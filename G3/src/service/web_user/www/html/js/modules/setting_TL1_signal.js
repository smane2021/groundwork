Pages.setting_TL1_signal = function (data) {
    Configs.Data.polling = false;
    var lang = Language["setting_signal"];
	var TL1_signal_errors = lang["errors"];
	var tips = lang["tips"];
    var args = data.args, data = data.data;
    var that = this;
    var sessionId = "&sessionId=" + args["sessionId"];
    var language_type = "&language_type=" + args["language_type"];
    var _datacommon = "sessionId="+args['sessionId']+"&language_type="+args['language_type']+"&_modify_configure=19"+"&_modify_configure_detail=54&";
    var _datacommon1 = "sessionId="+args['sessionId']+"&language_type="+args['language_type']+"&_modify_configure=19"+"&_modify_configure_detail=55&";
    var _datacommon2 = "sessionId="+args['sessionId']+"&language_type="+args['language_type']+"&_modify_configure=19"+"&_modify_configure_detail=56&";
    var aid_group = $("#AID_Group");
    var aid_equip = $("#AID_Equip");
    var _signal_index;
    var _index;
    var data1;
    var _form = $("#SignalForm");
    var _btn_modify = _form.find("a.modify");
    
    function bindClickList(){
    $("#SignalList").find("div.table-body>ul").off().on("click", function () {
        _btn_modify.removeClass("btn_2_disabled").addClass("btn_2");
        var selected = $(this).find("input").eq(0);
        selected.prop("checked", true);//����
        $(this).siblings().removeClass("selected").end().addClass("selected");
        data1 = jQuery.evalJSON($(this).find("span.signal_attr").html());
        $("#TL1_signal_parameter").html(data1["_signal_name"]);
            for(var item in data1){
            _signal_index = data1["_signal_index"];
            if((item != "_signal_index") && (item != "_signal_name"))
            {
                 var list_item = $("#"+item);
                 if(item=="_NotificationCode"|| item=="_ServiceAffectCode" || item=="_MonitorFormat"){
                    list_item.attr("data-index",$.trim(data1[item]));
                    list_item.text(list_item.next().find("a").eq($.trim(data1[item])).text());
                 }else{
                    list_item.val($.trim(data1[item]));
                 }
            }
        }
        if(data1["_SigActive"] == 1)
        {
            $("#_SigActive").prop("checked", true);
        }
        else
        {
            $("#_SigActive").prop("checked", false);
        }
    });
    };
    Pages.BindSelect();
    if(jQuery.cookie("systeminfo")==1){
        $(".TL1-signal-summary").css({width:'917px'});
        $(".table-container .table,#InventoryTable").css({width:'897px'});
        $(".set-form-TL1-signal").css({width:'592px'});
        $(".set-form-TL1-signal .table-title").css({width:'572px'});
        $(".set-form-TL1-signal p,form > span").css({"padding-left":"0"});
    }else{
        $(".TL1-signal-summary").css({width:'1163px'});
        $(".table-container .table,#CabinetSet,#InventoryTable").css({width:'1143px'});
        $(".set-form-TL1-signal").css({width:'838px'});
        $(".set-form-TL1-signal .table-title").css({width:'818px'});
        $(".set-form-TL1-signal p,form > span").css({"padding-left":"200px"});
    }
     aid_group.next().find("li").on("click", function () {
            ClickGroup(this);
    });
     function ClickGroup(obj){
            var aid_group_value = $(obj).find("a").attr("rel");
            if(aid_group_value == 0)
            {
                return false;
            }
            _index = aid_group_value;
            $("#aid_prefix").html(data.content[aid_group_value - 1][2]);
            var _data = _datacommon2 + "_index=" + aid_group_value;
            var XHR = $.ajax({
                    url: "../cgi-bin/web_cgi_setting.cgi" + "?_=" + new Date().getTime(),
                    type: "POST",
                data: _data,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };
                    data.status = $.trim(data.status);                    
					if (data.status == 98) {
						that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
						return;
					}
                    if (data.status == 1) {
                        that.SetProcessOK(TL1_signal_errors[0]);
                        $("#AID_Equip").text(tips[0]);
                        var option1="<ul class='language yinc'>";
                        for(var item in data.content)
                        {
                           option1 += " <li><a href='javascript:void(0)' rel='" + item + "'>"+ data.content[item] +"</a></li>"
                        }
                         option1+="</ul>";
                        $(".TL1-signal-summary .dropdown").eq(1).find("ul").remove();
                        $(".TL1-signal-summary .dropdown").eq(1).html($(".TL1-signal-summary .dropdown").eq(1).html()+option1);
                        $(".language li").off("click");
                        Pages.BindSelect(ClickEquip);
                        aid_group.next().find("li").bind("click",function(){
                            ClickGroup(this);
                        });
                        that.bindInput();
                    } else {
                        that.SetProcessDone(TL1_signal_errors[data.status]);
                    }
                },
                error: function (data, textStatus) {
                    that.SetProcessDone(Language.Html['002']);
                }
            }).done(function (data, textStatus, jqXHR) {
                jqXHR = null;
                XHR = null;
            });
    };
    
    function ClickEquip(){
    var aid_group_value = aid_group.attr("data-index");
    var aid_equip_value = $("#AID_Equip").attr("data-index");
    if(aid_group_value == -1)
    {
        return false;
    }
    _index = aid_group_value;
    //$("#aid_prefix").html(data.content[aid_group_value - 1][2]);
    $("#TL1_signal_title").html(data.content[aid_group_value - 1][1] + " > " + $("#AID_Equip").text());
    var _data = _datacommon + "_index=" + aid_group_value + "&_equip=" + aid_equip_value;
    var XHR = $.ajax({
                    url: "../cgi-bin/web_cgi_setting.cgi" + "?_=" + new Date().getTime(),
                    type: "POST",
                data: _data,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };
                    data.status = $.trim(data.status);                    
					if (data.status == 98) {
						that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
						return;
					}
                    if (data.status == 1) {
                        that.SetProcessOK(TL1_signal_errors[0]);
                        var tmp = doT.templateChild($("#SignalList").attr("template"));
					    var doTtmpl = doT.template(tmp);
                        $("#SignalList").html(doTtmpl(data));
                        $("#Setting_TL1_Signal .table-body").css({"height":$("#mainBody").height()-($(".TL1-signal-summary").outerHeight()+5)-($("#Setting_TL1_Signal .table-container .table-title").height()+21)});
                        that.bindInput();
                        bindClickList();
                    } else {
                        that.SetProcessDone(TL1_signal_errors[data.status]);
                    }
                },
                error: function (data, textStatus) {
                    that.SetProcessDone(Language.Html['002']);
                }
            }).done(function (data, textStatus, jqXHR) {
                jqXHR = null;
                XHR = null;
            });
    };

    _btn_modify.on("click",function(){
        if(_btn_modify.hasClass("btn_2_disabled"))
        {
            return false;
        }
        var _data = _datacommon1;
        var itemdata = _form.serialize();
        itemdata+="&_NotificationCode="+$("#_NotificationCode").attr("data-index")+"&_ServiceAffectCode="+$("#_ServiceAffectCode").attr("data-index")+"&_MonitorFormat="+$("#_MonitorFormat").attr("data-index");
        var _SampleSig = $("#_SampleSig");
        var _SampleSig_value = $.trim(_SampleSig.val());
        var _AlarmSig = $("#_AlarmSig");
        var _AlarmSig_value = $.trim(_AlarmSig.val());
        var _SettingSig = $("#_SettingSig");
        var _SettingSig_value = $.trim(_SettingSig.val());
        var _CondType = $("#_CondType");
        var _CondType_value = $.trim(_CondType.val());
        var _CondDesc = $("#_CondDesc");
        var _CondDesc_value = $.trim(_CondDesc.val());
        var _MonitorType = $("#_MonitorType");
        var _MonitorType_value = $.trim(_MonitorType.val());
        var aid_equip_value1 = $("#AID_Equip").attr("data-index");
        
        if(jQuery.cookie("nAuthority") < 3){
		        alert(TL1_signal_errors[12]);
                return false;
	        };
	        
        if(!(/^[^#]{0,20}$/gi.test(_CondType_value))){
	                alert(TL1_signal_errors[13]);
	                _CondType.select();
                        return false;
                    };
                    
        if(!(/^[^#\\]{0,32}$/gi.test(_CondDesc_value))){
	                alert(TL1_signal_errors[13]);
	                _CondDesc.select();
                        return false;
                    };
                    
        if(!(/^[A-Za-z]{0,10}$/gi.test(_MonitorType_value))){
	                alert(TL1_signal_errors[13]);
	                _MonitorType.select();
                        return false;
                    };
        var isactive = $("#_SigActive").prop("checked");
        if(isactive == false)
        {
            isactive = 0;
        }
        else if(isactive == true)
        {
            isactive = 1;
        }
        else
        {
            isactive = 0;
        }
        _data = _data + itemdata + "&_index=" + _index + "&_signal_index=" + _signal_index + "&_equip=" + aid_equip_value1 + "&_SigActive1=" + isactive;
        that.SetProcess(Language.Html['011']);
        var XHR = $.ajax({
                    url: _form.attr("action") + "?_=" + new Date().getTime(),
                    type: _form.attr("method") ? _form.attr("method") : "GET",
                data: _data,
                success: function (data, textStatus, jqXHR) {
                    try {
                        var data = jQuery.evalJSON(data);
                    } catch (err) {
                        alert(Language.Html["032"]);
                        return false;
                    };
                    data.status = $.trim(data.status);
					if (data.status == 98) {
						that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
						return;
					}
                    if (data.status == 0) {
                        that.SetProcessOK(TL1_signal_errors[0]);
                        ClickEquip();
                        that.bindInput();
                    } else {
                        that.SetProcessDone(TL1_signal_errors[data.status]);
                    }
                },
                error: function (data, textStatus) {
                    that.SetProcessDone(Language.Html['002']);
                }
            }).done(function (data, textStatus, jqXHR) {
                jqXHR = null;
                XHR = null;
            });
    });
};


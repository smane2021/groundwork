﻿Pages.di_setting = function (data) {
	var that = this;
	var settemplates = $("#SetDiAlarm").attr("template");
	$("#SetAlarmList>div.table-body").on("click", "a.btn_set", function () {
		var post = $(this).attr("post");
		var parent = $(this).parents("ul");
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
		Control.PromptEvent.Pop({ cover: true, title: parent.find("li.t-2").text(), fn: that.SetLevelOrRelay, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args }, html: "<div id='SetAlarmLevelRelay' class='Poplevelrelay'></div>", Class: "Pop-di-alarm" });
	}).on("click", "ul,i,a", function () {
		var ul = $(this).closest("ul");
		Configs.HighLight = ul.attr("id");
		ul.siblings().removeClass("select").end().addClass("select");
	});
	$("#" + Configs.HighLight).addClass("select");
	Configs.Data.polling = false;
	Configs.tmpTimer[0] = setTimeout(function () {
		if (document.getElementById("SetAlarmContent")) {
			$("#mainBody").scrollTop(that.SetAlarmScroll);
		}
	}, 10);
	return false;
}
/*---------------------------------*
* Rectifier页面回调函数,
* data->渲染当前页面所需数据（格式: {args:{},data:{}},args:{}包涵链接中data={}中的数据，data:{}包涵服务器返回的数据)
*---------------------------------*/
Pages.system_rectifier = function (data) {
    //绘制电流表
    this.SystemDrawMeter("rectifierMeter", data);
    this.SystemCommon(data);
    if(jQuery.cookie("systeminfo")!=1){
       $(".system-summary,.system-list,.system-list ul,.extbackground").css({width:'1163px'});
       $(".system-meterInfo").css({width:'806px'});
       $(".system-meterInfo .table-a").css({width:'786px'});
    }else{
       $(".system-summary,.system-list,.system-list ul,.extbackground").css({width:'917px'});
       $(".system-meterInfo").css({width:'560px'});
       $(".system-meterInfo .table-a").css({width:'540px'});
    }
};
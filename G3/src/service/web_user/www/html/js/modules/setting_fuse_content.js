Pages.setting_fuse_content=function(data){
 var uls = $("div.table-body ul");
 var that = this;
 //列表模板
	var Set_PowerSystem = $("#Set_Fuse");
	var listTemplate = doT.templateChild(Set_PowerSystem.attr("template"));
	var settemplates = $("#Set_Fuse").attr("template");
	 if(jQuery.cookie("systeminfo")!=1){
        $(".system-no-data").css({width:'1163px'});
     }else{
        $(".system-no-data").css({width:'917px'});
     }
$(".table-body").on("click", "a.btn_mod", function () {
        var post = $(this).attr("post");
        if(!post){return false}
		var parent = $(this).parents("ul");
		var modifydatas = jQuery.evalJSON($(this).attr("modifydatas"));
		Control.PromptEvent.Pop({ cover: true, title: parent.parents(".table-list").prev().find("span").text(), fn: that.SetFuseName, data: { template: settemplates, post: post, modifydatas: modifydatas, args: data.args }, html: "<div id='SetAlarmLevelRelay' class='Poplevelrelay'></div>", Class: "Pop-fuse-alarm" });
	}).on("click", "ul,i,a", function () {
    if($(this).hasClass("btn_set_disabled")){
        uls.removeClass("select");
      }else{
	    uls.removeClass("select");
        $(this).addClass("select");
       }
	});
		//打开列表
	function openList(target){
		var datas ={
			"args": data.args,
			"jQuery": jQuery,
			"$" : jQuery,
			"Configs" : Configs,
			"Pages" : Pages,
			"Control" :Control,
			"Validates":Validates,
			"window":window
		};
		var item = target.attr("data").split("|");
		var datalist = data.data[item[0]][item[1]][item[2]].list;
		datas["list"] = datalist;
		var doTtmpl = template.compile(listTemplate)(datas);

		target.html(doTtmpl).show();
		that.Setting(data);
		that.bindInput();
	}
	//打开或隐藏设置列表
	Set_PowerSystem.off('click').on("click","div.table-ex",function(){
		var p = $(this).parent("div.table-container");
		var next = $(this).next("div.table-content");
		next.toggle();
		p.toggleClass("border-bn");
		var span = $(this).find("span");
		var nextHasList = next.attr("data");
		if (next.is(":hidden")) {
			//close
			span.removeClass("title-sl").addClass("title-zk");
			that.DelArrayValue(Configs.PowerSystem, [Number(next.attr("index"))]);
			if(nextHasList){
				next.html("");
			}
		} else {
			//open
			span.removeClass("title-zk").addClass("title-sl");
			Configs.PowerSystem.push(Number(next.attr("index")));
			if(nextHasList){
				openList(next);
			}
		}
	});
}
Pages.SetFuseName=function(datas){
    var that = Pages;
	var content = $("#SetAlarmLevelRelay");
	content.html(datas.template);
	var info = content.find(".info").addClass("setting");
	var form = content.find("form");
	var inputs = form.find(".set_value");
	var name=inputs.attr("name");
	//inputs.val(datas.modifydatas[name]);
	//初始化弹出框数据
	if (datas.modifydatas) {
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
			var name = inputs.eq(i).attr("name");
			inputs.eq(i).val(datas.modifydatas[name]);
		}
	}
	
	$("#SetAlarmLevelRelay input").off("focus blur keydown").on("focus", function () {
		//如果redonly为true，一般为时间设置，即不可以使用backspace
		if (!$(this).attr("readonly")) {
			Configs.Backspace = true;
		}
	}).on("blur", function () {
		Configs.Backspace = false;
	}).on("keydown", function () {
		if ($(this).hasClass("set_value")) {
			Configs.Backspace = true;
		};
	});
	
	
	content.find("a.btn_set").off().on("click", function () {
		if ($(this).hasClass("btn_set_disabled")) { return false; }
		info.html(Language.Html['011']).addClass("setting");
		$("#PromptPopClose").hide();
		var check = false;
		var vals = "";
		for (var i = 0, ilen = inputs.length; i < ilen; i++) {
		    var chinieseCount = 0;
            var enandnumCount = 0;
			var item = inputs.eq(i);
			var pname = item.attr("pname");
			if (pname == "sSignalName_en"||pname=="sSignalAbbrName_en") {
				var sNewName = item.val();
			    if(!/^[\u4e00-\u9fa5_a-zA-Z0-9_ \t\n\x0B\f\r]+$/.test(sNewName)){
			        info.html(Language.Html["032"]);
			        return false;
			    }
			var langtype=datas.post.split("&equip_ID")[0].split("=")[1];
			if(langtype==1){
			    var badChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                badChar += "abcdefghijklmnopqrstuvwxyz";
                badChar += "0123456789";
                if ("" == sNewName) {
                    return false;
                }
                for (var j = 1; j <= sNewName.length; j++) {
                    var c = sNewName.charAt(j);//字符串str中的字符 
                    if (badChar.indexOf(c) > -1) {
                        enandnumCount++;
                    }
                    else {
                        chinieseCount++;
                    }
                }
                if(pname == "sSignalName_en"){
			         if(enandnumCount+chinieseCount*2>32){
			             info.html(Language.Html["061"]);
			             return false;
			         }
			     }
			     if(pname == "sSignalAbbrName_en"){
			         if(enandnumCount+chinieseCount*2>15){
			             info.html(Language.Html["061"]);
			             return false;
			         }
			     }
			 }
			    
			    
			    
				var iNameLength = 0;
				for (var iIndex = 0; iIndex < sNewName.length; iIndex++) {
					if (escape(sNewName[iIndex]).indexOf("%u") != -1) {
						iNameLength += 3; //Chinese char
					} else {
						iNameLength += 1; //Ascii
					}
				}
				if (iNameLength > 32) {
					check = true;
					info.html(Language.Html["061"]);
				}
			} 
			vals += "&" + item.attr("pname") + "=" + encodeURIComponent(item.val());
		};
		vals += "&sessionId=" + datas.args["sessionId"];
		if (check) {
			info.removeClass("setting");
			return false;
		}
		$(this).addClass("btn_set_disabled");
		var $this = $(this);
		var XHR = $.ajax({
			timeout: 10000,
			url: form.attr("action") + "?_=" + new Date().getTime(),
			type: form.type,
			data: datas.post + vals,
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					that.SetProcessDone(Language.Html["001"]);
					return false;
				}
				if (data.status == 98) {
					info.html(Language.Html['019']);
					return;
				}
				if (data.status == 1) {
					info.addClass("setting");
					//that.SetProcess("");
					that.SetProcessOK(Language.Setting[data.status]);
					$("#PromptPopClose").click();
					that.SetAlarmScroll = Configs.MainScrollHeight;
					//Control.RefreshModule();
				}
				info.html(Language.Setting[data.status]);
			},
			error: function (data, textStatus) {
				info.html(Language.Html['016']).removeClass("setting");
				$("#PromptPopClose").show();
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			$this.removeClass("btn_set_disabled");
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		})
		return false;
	});
}

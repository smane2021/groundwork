﻿Pages.cabinet_map = function (data) {
	var that = this;
	var data = data.data;
	var CabinetMap = $("#CabinetMap"),mainBody = $("#mainBody");
	var pageNum =25;
	var Lang = Language["cabinet_map"],Errors=Lang["errors"];
	function Column(d) {
		$("#PromptPopTitle").find("span").html(d.name);
		$("#Pop_CabinetMap").html(CabinetMap.attr("template"));
		Chart.drawColumn("Pop_CabinetMap", { value: d.data,maxLength:pageNum,popup:"PromptPop",popupHeight:380 });
	}
	CabinetMap.off().on("click", "a", function () {
		mainBody.removeData();
		var item = $(this).parent().attr("name");
		var du = $(this).attr("name");
		triggerPop([item,du]);
		return false;
	});
	function triggerPop(arr,subData){
		var allDatas = data[arr[0]][arr[1]][1];
		var CabinetName = data[arr[0]]["name"]
		var DuName = data[arr[0]][arr[1]][0];
		var datas = subData ? allDatas.slice(subData[0]-1,subData[1]) : allDatas;
		if(datas.length==0){
			alert(Errors[0]);
			return;
		}
		Control.PromptEvent.Pop({ cover: true, title: Language.Chart["004"], fn: Column, Class: "PromptCabinet", data: { name: CabinetName + " > " + DuName, data: datas }, html: "<div id='Pop_CabinetMap' class='PopCurve'></div>" });
		mainBody.data('index',arr);
		var dlen = allDatas.length;		
		if(dlen>pageNum){
			var ChangeList = $("#changeList");
			//显示列表
			var page = Math.ceil(dlen/pageNum);
			var tmp = "";
			for(var i=0; i<page; i++){
				if(i==page-1){
					tmp +="<option>"+(i*pageNum+1)+"-"+dlen+"</option>";
				} else {
					tmp +="<option>"+(i*pageNum+1)+"-"+((i+1)*pageNum)+"</option>";
				}
			}
			ChangeList.append(tmp).show();
			if(subData){
				ChangeList.val(subData[0]+"-"+subData[1]);
			}
			ChangeList.off().on("change",function(){
				var val = $(this).val().split("-");
				mainBody.data('sub',val);
				triggerPop(arr,val);
			});	
		}
	}
	var PromptPop = $("#PromptPop");
	if(PromptPop[0]){
		var PromptPopData = mainBody.data('index');
		if(PromptPopData.length==2){
			if(mainBody.data('sub')){
				triggerPop(PromptPopData,mainBody.data('sub'));
			} else {
				triggerPop(PromptPopData);
			}
		}
	}
	setTimeout('$("#CabinetMap").css({"height":$("#mainBody").height()});',10);
	if(jQuery.cookie("systeminfo")!=1){
        $("#CabinetMap").css({width:'1163px'});
    }else{
        $("#CabinetMap").css({width:'917px'});
    }
}
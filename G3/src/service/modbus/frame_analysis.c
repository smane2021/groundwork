/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : frame_analysis.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "modbus.h"

#if 0
#define TRACE_MODBUS_CFG_EXT printf
#define TRACE_MODBUS_CFG_EXT2 printf
#else
#define TRACE_MODBUS_CFG_EXT
#define TRACE_MODBUS_CFG_EXT2
#endif


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
*
* Modbus CRC  
*
*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
BYTE gabyCRCHi[] =
{
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,
	0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,
	0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,
	0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
	0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,
	0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
	0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,
	0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
	0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,
	0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40
};

BYTE gabyCRCLo[] =
{
	0x00,0xc0,0xc1,0x01,0xc3,0x03,0x02,0xc2,0xc6,0x06,
	0x07,0xc7,0x05,0xc5,0xc4,0x04,0xcc,0x0c,0x0d,0xcd,
	0x0f,0xcf,0xce,0x0e,0x0a,0xca,0xcb,0x0b,0xc9,0x09,
	0x08,0xc8,0xd8,0x18,0x19,0xd9,0x1b,0xdb,0xda,0x1a,
	0x1e,0xde,0xdf,0x1f,0xdd,0x1d,0x1c,0xdc,0x14,0xd4,
	0xd5,0x15,0xd7,0x17,0x16,0xd6,0xd2,0x12,0x13,0xd3,
	0x11,0xd1,0xd0,0x10,0xf0,0x30,0x31,0xf1,0x33,0xf3,
	0xf2,0x32,0x36,0xf6,0xf7,0x37,0xf5,0x35,0x34,0xf4,
	0x3c,0xfc,0xfd,0x3d,0xff,0x3f,0x3e,0xfe,0xfa,0x3a,
	0x3b,0xfb,0x39,0xf9,0xf8,0x38,0x28,0xe8,0xe9,0x29,
	0xeb,0x2b,0x2a,0xea,0xee,0x2e,0x2f,0xef,0x2d,0xed,
	0xec,0x2c,0xe4,0x24,0x25,0xe5,0x27,0xe7,0xe6,0x26,
	0x22,0xe2,0xe3,0x23,0xe1,0x21,0x20,0xe0,0xa0,0x60,
	0x61,0xa1,0x63,0xa3,0xa2,0x62,0x66,0xa6,0xa7,0x67,
	0xa5,0x65,0x64,0xa4,0x6c,0xac,0xad,0x6d,0xaf,0x6f,
	0x6e,0xae,0xaa,0x6a,0x6b,0xab,0x69,0xa9,0xa8,0x68,
	0x78,0xb8,0xb9,0x79,0xbb,0x7b,0x7a,0xba,0xbe,0x7e,
	0x7f,0xbf,0x7d,0xbd,0xbc,0x7c,0xb4,0x74,0x75,0xb5,
	0x77,0xb7,0xb6,0x76,0x72,0xb2,0xb3,0x73,0xb1,0x71,
	0x70,0xb0,0x50,0x90,0x91,0x51,0x93,0x53,0x52,0x92,
	0x96,0x56,0x57,0x97,0x55,0x95,0x94,0x54,0x9c,0x5c,
	0x5d,0x9d,0x5f,0x9f,0x9e,0x5e,0x5a,0x9a,0x9b,0x5b,
	0x99,0x59,0x58,0x98,0x88,0x48,0x49,0x89,0x4b,0x8b,
	0x8a,0x4a,0x4e,0x8e,0x8f,0x4f,0x8d,0x4d,0x4c,0x8c,
	0x44,0x84,0x85,0x45,0x87,0x47,0x46,0x86,0x82,0x42,
	0x43,0x83,0x41,0x81,0x80,0x40
};

/*==========================================================================*
*	FUNCTION:�Գ���ΪLen�������ݰ�У�飬��У���ֵ
*   PURPOSE :���У���ֵֻ��1���ֽڣ���д��pChkBytes[0]��
*	Input   :
*			  pFrame  Ҫ���ݰ��ĵ�ַ.
*			  nLen	��У������ݰ�����
*			  pChkBytes װУ��ֵ�Ĵ�
*	Output  :	pChkBytes У��ֵ
*	RETURN  :��
*	HISTORY :
*==========================================================================*/
WORD	ModbusCRC(BYTE *pData, int len)
{
    BYTE i = (BYTE)len;
    BYTE  byCRCHi = 0xff;
    BYTE  byCRCLo = 0xff;
    BYTE  byIdx;
    WORD  crc;

    while(i--)
    {
	byIdx = byCRCHi ^ *pData++;
	byCRCHi = byCRCLo ^ gabyCRCHi[byIdx];
	byCRCLo = gabyCRCLo[byIdx];
    }

    crc = byCRCHi;
    crc <<= 8;
    crc += byCRCLo;

    return crc;
}

MODBUS_FRAME_TYPE Modbus_03DecodeandPerfrom(unsigned char *pCmdData)
{
	MODBUSMODEL_CONFIG_INFO		*pstModelCfg = &g_ModbusGlobals.ModbusModelConfig;
	int							temp1,temp2;
	int							iRegStart, iRegCount;
	
	temp1 =(int)(MAKEWORD(*(pCmdData+1),*pCmdData));
	
	//Frank Wu,20151112, for extending configuration file
	if(MODBUS_CONFIG_VERSION_2 == pstModelCfg->iExtCurrentCfgVersion)
	{
		TRACE_MODBUS_CFG_EXT("Modbus_03DecodeandPerfrom Start\n");
		
		iRegStart =(int)(MAKEWORD(*(pCmdData+1),*pCmdData));
		iRegCount = (int)(MAKEWORD(*(pCmdData+3),*(pCmdData+2)));
		
		TRACE_MODBUS_CFG_EXT("iRegStart=%d, iRegCount=%d\n", iRegStart, iRegCount);
	
		if((iRegStart < 0) 
			|| (iRegStart > (pstModelCfg->iExtMaxRegNum - 1)))
		{
			return  MODBUS_REGERR;
		}

		if((iRegCount < 1 )
			||(iRegCount > 125)
			||(iRegStart + iRegCount) > pstModelCfg->iExtMaxRegNum)
		{
			return  MODBUS_REGERR;
		}
		
		TRACE_MODBUS_CFG_EXT("Modbus_03DecodeandPerfrom End\n");
		return  MODBUS_NOR;
	}
	
	if((temp1<MODBUSREGISTERSTART)||(temp1>MODBUSREGISTEREND))
	{

		return  MODBUS_REGERR;
	}

	temp2 = (int)(MAKEWORD(*(pCmdData+3),*(pCmdData+2)));
	
	if((temp2<0)||(temp2 > 125)||(temp1+temp2)>(MODBUSREGISTEREND+1))
	{

	    return  MODBUS_REGERR;
	}

	return  MODBUS_NOR;

}

MODBUS_FRAME_TYPE Modbus_FindRegInfoForConfigVersion2(int iRegAddr,
													int *piEquipID,
													MODBUS_MAPENTRIES_INFO **ppCurEntry)
{
	int							iEquipIDOrder, iEquipID, i, j, iTemp;
	MODBUS_MAPENTRIES_INFO		*pCurEntry;
	BOOL						bDxiRTN;
	MODBUSMODEL_CONFIG_INFO		*pstModelCfg = &g_ModbusGlobals.ModbusModelConfig;
	MODBUS_EXT_TYPE_MAP_INFO	*pCurExtTypeMapInfo = NULL;
	int							iEquipOffset;
	int							iEntriesItemIndex;
	int							iFirstRegAddr;
	MODBUS_DataValue			DataValue;


	//be careful, here is from pstModelCfg->iExtTypeMapNum to 0
	//because the bottom item is the newest item. it will be the highest priority
	for(i = pstModelCfg->iExtTypeMapNum - 1; i >= 0; i--)
	{
		pCurExtTypeMapInfo = &pstModelCfg->pExtTypeMapInfo[i];

		if((iRegAddr >= pCurExtTypeMapInfo->iRegStart)
			&& (iRegAddr <= pCurExtTypeMapInfo->iRegEnd))
		{
			TRACE_MODBUS_CFG_EXT("find iRegAddr=%d in (%d-%d) of pCurExtTypeMapInfo[%d], SequenceNum=%d, iMaxRegCountPerEquip=%d\n",
								iRegAddr,
								pCurExtTypeMapInfo->iRegStart,
								pCurExtTypeMapInfo->iRegEnd,
								i,
								pCurExtTypeMapInfo->iSequenceNum1stEquip,
								pCurExtTypeMapInfo->iMaxRegCountPerEquip);

			iEquipIDOrder = -1;
		
			//skip "7.Sequence Number of First Equip" to get the target equipment iEquipIDOrder
			if( pCurExtTypeMapInfo->iSequenceNum1stEquip > 0 )
			{
				for(j = 1; j < pCurExtTypeMapInfo->iSequenceNum1stEquip; j++)
				{
					iEquipIDOrder = GetEquipIDOrderwithstartpositionByRange(
						pCurExtTypeMapInfo->iEquipTypeStart,
						pCurExtTypeMapInfo->iEquipTypeEnd,
						iEquipIDOrder + 1);

					if(MODBUS_MAX_EQUIP_NUM == iEquipIDOrder)
					{
						return MODBUS_CTLFAIL;//can't find the equipment
					}
				}
			}

			//find out which equipment the register address(temp1) belongs to
			iEquipOffset = (iRegAddr - pCurExtTypeMapInfo->iRegStart)/pCurExtTypeMapInfo->iMaxRegCountPerEquip;
			iFirstRegAddr = pCurExtTypeMapInfo->iRegStart + iEquipOffset*pCurExtTypeMapInfo->iMaxRegCountPerEquip;
			
			TRACE_MODBUS_CFG_EXT("iEquipOffset=%d, iFirstRegAddr=%d\n", iEquipOffset, iFirstRegAddr);

			//skip iEquipOffset to locate the equipment
			for(j = 0; j < iEquipOffset + 1; )
			{
				iEquipIDOrder = GetEquipIDOrderwithstartpositionByRange(
					pCurExtTypeMapInfo->iEquipTypeStart,
					pCurExtTypeMapInfo->iEquipTypeEnd,
					iEquipIDOrder + 1);

				if(MODBUS_MAX_EQUIP_NUM == iEquipIDOrder)
				{
					return MODBUS_CTLFAIL;
				}

				if(MODBUS_SPEC_PROC_FLAG_1_SKIP_NONEXIST_EQUIP & pCurExtTypeMapInfo->uiSpecProcFlag)
				{
					//check equipment's exist status
					iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
					bDxiRTN = ModbusGetSCUPSingleSigValues(SIG_TYPE_SAMPLING,100 , iEquipID, &DataValue);
					if(( bDxiRTN == FALSE)||(DataValue.VData.itemp == 1))
					{
						continue;
					}
				}

				j++;
			}

			if((iEquipIDOrder < 0 ) || (MODBUS_MAX_EQUIP_NUM == iEquipIDOrder))
			{
				return MODBUS_CTLFAIL;
			}
			iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];

			for(iEntriesItemIndex = 0;
				(iEntriesItemIndex < pCurExtTypeMapInfo->iMapEntriesNum) && (iEntriesItemIndex < pCurExtTypeMapInfo->iMaxRegCountPerEquip);
				iEntriesItemIndex++)
			{
				pCurEntry = &pCurExtTypeMapInfo->pMapEntriesInfo[iEntriesItemIndex];
				if(iFirstRegAddr + pCurEntry->iModbusPort == iRegAddr)//find the register
				{
					TRACE_MODBUS_CFG_EXT("iEntriesItemIndex=%d, iEquipID=%d, iEquipType=%d, iEquipTypeStart=%d, iEquipTypeEnd=%d\n",
										iEntriesItemIndex,
										iEquipID,
										pCurEntry->iEquipType,
										pCurExtTypeMapInfo->iEquipTypeStart,
										pCurExtTypeMapInfo->iEquipTypeEnd);
					
					//if iEquipType is exist, use it to find new iEquipID
					if((pCurEntry->iEquipType > 0)
						&& ((pCurEntry->iEquipType < pCurExtTypeMapInfo->iEquipTypeStart) || (pCurEntry->iEquipType > pCurExtTypeMapInfo->iEquipTypeEnd)))
					{
						iTemp = GetEquipIDOrderwithstartpositionByRange(
							pCurEntry->iEquipType,
							pCurEntry->iEquipType,
							0);
						if(MODBUS_MAX_EQUIP_NUM == iTemp)
						{
							return MODBUS_CTLFAIL;
						}
						iEquipID = g_ModbusGlobals.iEquipIDOrder[iTemp][0];
					}
				
					*ppCurEntry = pCurEntry;
					*piEquipID = iEquipID;

					return MODBUS_NOR;
				}
			}
			
			return  MODBUS_REGERR;
		}
	}

	return  MODBUS_REGERR;
}



MODBUS_FRAME_TYPE Modbus_06DecodeandPerfrom(unsigned char *pCmdData)
{
    int temp1,temp2,iEquipIDOrder,iEquipID ,iData ,i;
    int isetdevicenum,idevicenum,iregnum;
    MODBUS_MAPENTRIES_INFO * pCurEntry;
    BOOL bRTN;
    BOOL AnalogyorDigital;

    BOOL bDxiRTN;
    MODBUS_DataValue DataValue;

	MODBUSMODEL_CONFIG_INFO		*pstModelCfg = &g_ModbusGlobals.ModbusModelConfig;
	int							iRegStart;


    temp1 =(int)(MAKEWORD(*(pCmdData+1),*pCmdData));

	//Frank Wu,20151112, for extending configuration file
	if(MODBUS_CONFIG_VERSION_2 == pstModelCfg->iExtCurrentCfgVersion)
	{
		TRACE_MODBUS_CFG_EXT("Modbus_06DecodeandPerfrom Start\n");
		
		iRegStart =(int)(MAKEWORD(*(pCmdData+1),*pCmdData));
		
		TRACE_MODBUS_CFG_EXT("iRegStart=%d\n", iRegStart);

		if( (iRegStart < 0)
			|| (iRegStart > (pstModelCfg->iExtMaxRegNum - 1)) )
		{
			return  MODBUS_REGERR;
		}
	
		//According to Modbus Protocol, The Request PDU specifies the address of the register to be written. 
		//Registers are addressed starting at zero. Therefore register numbered 1 is addressed as 0.
		bRTN = Modbus_FindRegInfoForConfigVersion2(iRegStart + 1, &iEquipID, &pCurEntry);
		if(MODBUS_NOR != bRTN)
		{
			return bRTN;
		}

		TRACE_MODBUS_CFG_EXT("iEquipID=%d, iModbusPort=%d, iEquipType=%d, iSCUPSigType=%d, "
							"iSCUPSigID=%d, iAccessAuthority=%d, iFlag0=%d, iFlag1=%d\n",
							iEquipID, 
							pCurEntry->iModbusPort,
							pCurEntry->iEquipType,
							pCurEntry->iSCUPSigType,
							pCurEntry->iSCUPSigID,
							pCurEntry->iAccessAuthority,
							pCurEntry->iFlag0,
							pCurEntry->iFlag1);

		if(MODBUS_REG_ACCESS_AUTHORITY_WRITE != pCurEntry->iAccessAuthority)
		{
			return MODBUS_REGERR;
		}
		
		if((SIG_TYPE_SAMPLING != pCurEntry->iSCUPSigType )
			&& (SIG_TYPE_CONTROL != pCurEntry->iSCUPSigType )
			&& (SIG_TYPE_SETTING != pCurEntry->iSCUPSigType )
			&& (SIG_TYPE_ALARM != pCurEntry->iSCUPSigType ))
		{
			return MODBUS_REGERR;
		}
		
		if(pCurEntry->iSCUPSigID <= 0)
		{
			return MODBUS_REGERR;
		}
		
		if(MODBUS_TWOREG_FOR_ONEDATA_FLAG == pCurEntry->iFlag0)//don't support setting double registers 
		{
			return MODBUS_REGERR;
		}
		
		iData = (int)(MAKEWORD(*(pCmdData+3),*(pCmdData+2)));
		iData =  (iData - pCurEntry->iFlag0);

		bRTN = SetModbusSigValues(iEquipID, 1, pCurEntry, iData, pCurEntry->iFlag1);

		if(bRTN != TRUE)
		{
			return MODBUS_CTLFAIL;
		}

		TRACE_MODBUS_CFG_EXT("Modbus_06DecodeandPerfrom End\n");
		
		return MODBUS_NOR;
	}
	
	
    if((temp1<MODBUSREGISTERSTART)||(temp1>MODBUSREGISTEREND))
    {
	return  MODBUS_REGERR;
    }
    temp2 = (int)(MAKEWORD(*(pCmdData+1),*pCmdData));
  
    pCurEntry = g_ModbusGlobals.ModbusModelConfig.pMapEntriesInfo;

    if((temp1>=MODBUS_Batt_REGISTERSTART)&&(temp1<=MODBUS_Batt_REGISTEREND))
    {

	 isetdevicenum =(temp1-MODBUS_Batt_REGISTERSTART)/MODBUS_PERBatt_NUM; 

	 iregnum = (temp1-MODBUS_Batt_REGISTERSTART)%MODBUS_PERBatt_NUM +MODBUS_Batt_REGISTERSTART;
 
	 if((iregnum<MODBUS_Batt1SET_REGISTERSTART)||(iregnum>MODBUS_Batt1SET_REGISTEREND))
	 {
		return  MODBUS_REGERR;
	 }
	 idevicenum = 0;

	 for(i= 0;i< g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum;i++)
	 {	

		if(pCurEntry->iModbusPort == iregnum )
		{
			iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,0);
			if(iEquipIDOrder == MODBUS_MAX_EQUIP_NUM)
			{
				return MODBUS_CTLFAIL;
			}
			iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
			bDxiRTN = ModbusGetSCUPSingleSigValues(SIG_TYPE_SAMPLING,100 , iEquipID, &DataValue);
 
			if(( bDxiRTN == FALSE)||(DataValue.VData.itemp == 1))
			{
				iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );				
				if(iEquipIDOrder == MODBUS_MAX_EQUIP_NUM)
				{
					return MODBUS_CTLFAIL;
				}
				iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
				continue;		
			}

			while(iEquipIDOrder !=MODBUS_MAX_EQUIP_NUM)
			{
				if(isetdevicenum ==  idevicenum)
				{
					break;
				}
				else
				{
					iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
					if(iEquipIDOrder == MODBUS_MAX_EQUIP_NUM)
					{
						return MODBUS_CTLFAIL;
					}
					iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0]; 

					bDxiRTN = ModbusGetSCUPSingleSigValues(SIG_TYPE_SAMPLING,100 , iEquipID, &DataValue);
					
					if(( bDxiRTN == FALSE)||(DataValue.VData.itemp == 1))
					{
						iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
						if(iEquipIDOrder == MODBUS_MAX_EQUIP_NUM)
						{
							return MODBUS_CTLFAIL;
						}
						iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
						continue;		
					}
					 

				}	
				idevicenum++;
			}
			if(iEquipIDOrder == 0)
			{

				return  MODBUS_REGERR;
			}
			else
			{
				break;
			}

		 }
		

		 pCurEntry++;
	 }
	 if(i>= g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum )
	 {
		 return  MODBUS_REGERR;
	 }

    }
    else
    {
	for(i= 0;i< g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum;i++)
	{	    
		if(pCurEntry->iModbusPort == temp1)
		{

			if((pCurEntry ->iSCUPSigType != SIG_TYPE_SETTING)&&(pCurEntry ->iSCUPSigType != SIG_TYPE_CONTROL))
			{
				return  MODBUS_REGERR;
			}
			break;
		}
		pCurEntry++;
	}

	if(i>= g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum )
	{
		return  MODBUS_REGERR;
	}
	iEquipIDOrder = Modbus_GetEquipIDOrder(pCurEntry->iEquipType);
	iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
    }

	iData = (int)(MAKEWORD(*(pCmdData+3),*(pCmdData+2)));
	iData =  (iData - pCurEntry->iFlag0);

    bRTN = SetModbusSigValues(iEquipID, 1, pCurEntry,iData,pCurEntry->iFlag1);

    if(bRTN!=TRUE)
    {
	return MODBUS_CTLFAIL;
    }

    return  MODBUS_NOR;
}

MODBUS_FRAME_TYPE Modbus_10DecodeandPerfrom(unsigned char *pCmdData)
{
    int temp1,temp2,temp3,i,iData,j;
    MODBUS_MAPENTRIES_INFO * pCurEntry;
    int isetdevicenum,idevicenum,iregnum;
    int iEquipIDOrder,iEquipID ;
    MODBUS_VAR_Data Vdata;
    BOOL bRTN;
    BOOL AnalogyorDigital;

    BOOL bDxiRTN;
    MODBUS_DataValue DataValue;
	MODBUSMODEL_CONFIG_INFO		*pstModelCfg = &g_ModbusGlobals.ModbusModelConfig;
	int							iRegStart, iRegCount, iByteCount;


    temp1 =(int)(MAKEWORD(*(pCmdData+1),*pCmdData));


	//Frank Wu,20151112, for extending configuration file
	if(MODBUS_CONFIG_VERSION_2 == pstModelCfg->iExtCurrentCfgVersion)
	{
		TRACE_MODBUS_CFG_EXT("Modbus_06DecodeandPerfrom Start\n");

		iRegStart =(int)(MAKEWORD(*(pCmdData+1),*pCmdData));
		iRegCount = (int)(MAKEWORD(*(pCmdData+3),*(pCmdData+2)));
		iByteCount = (int)(*(pCmdData+4));

		TRACE_MODBUS_CFG_EXT("iRegStart=%d, iRegCount=%d, iByteCount=%d\n", iRegStart, iRegCount, iByteCount);

		if( (iRegStart < 0)
			|| (iRegStart > (pstModelCfg->iExtMaxRegNum - 1)) )
		{
			return  MODBUS_REGERR;
		}

		if((iRegCount < 1 )
			|| (iRegCount > MAX_MODBUSFRAME_LEN)
			|| (iRegStart + iRegCount) > pstModelCfg->iExtMaxRegNum)
		{
			return  MODBUS_REGERR;
		}
		
		if(2*iRegCount != iByteCount)
		{
			return  MODBUS_REGERR;
		}
		
		for( i= 0; i < iRegCount; i++)
		{
			TRACE_MODBUS_CFG_EXT("i=%d\n", i);

			//According to Modbus Protocol, The Request PDU specifies the address of the register to be written. 
			//Registers are addressed starting at zero. Therefore register numbered 1 is addressed as 0.
			bRTN = Modbus_FindRegInfoForConfigVersion2((iRegStart + 1) + i, &iEquipID, &pCurEntry);
			if(MODBUS_NOR != bRTN)
			{
				return bRTN;
			}

			TRACE_MODBUS_CFG_EXT("iEquipID=%d, iModbusPort=%d, iEquipType=%d, iSCUPSigType=%d, "
				"iSCUPSigID=%d, iAccessAuthority=%d, iFlag0=%d, iFlag1=%d\n",
				iEquipID, 
				pCurEntry->iModbusPort,
				pCurEntry->iEquipType,
				pCurEntry->iSCUPSigType,
				pCurEntry->iSCUPSigID,
				pCurEntry->iAccessAuthority,
				pCurEntry->iFlag0,
				pCurEntry->iFlag1);

			if(MODBUS_REG_ACCESS_AUTHORITY_WRITE != pCurEntry->iAccessAuthority)
			{
				return MODBUS_REGERR;
			}

			if((SIG_TYPE_SAMPLING != pCurEntry->iSCUPSigType )
				&& (SIG_TYPE_CONTROL != pCurEntry->iSCUPSigType )
				&& (SIG_TYPE_SETTING != pCurEntry->iSCUPSigType )
				&& (SIG_TYPE_ALARM != pCurEntry->iSCUPSigType ))
			{
				return MODBUS_REGERR;
			}

			if(pCurEntry->iSCUPSigID <= 0)
			{
				return MODBUS_REGERR;
			}

			if(MODBUS_TWOREG_FOR_ONEDATA_FLAG == pCurEntry->iFlag0)//don't support setting double registers 
			{
				return MODBUS_REGERR;
			}

			iData = (int)(MAKEWORD(*(pCmdData+6+i*2),*(pCmdData+5+i*2)));
			iData =  (iData - pCurEntry->iFlag0);

			bRTN = SetModbusSigValues(iEquipID, 1, pCurEntry, iData, pCurEntry->iFlag1);
			if(bRTN != TRUE)
			{
				return MODBUS_CTLFAIL;
			}
		}
		
		TRACE_MODBUS_CFG_EXT("Modbus_10DecodeandPerfrom End\n");

		return MODBUS_NOR;
	}


    if((temp1<MODBUSREGISTERSTART)||(temp1>MODBUSREGISTEREND))
    {

	return  MODBUS_REGERR;
    }
    temp2 = (int)(MAKEWORD(*(pCmdData+3),*(pCmdData+2)));

    if((temp2<0)||(temp2 > MAX_MODBUSFRAME_LEN)||(temp1+temp2)>MODBUSREGISTEREND)
    {
	return  MODBUS_REGERR;
    }
    temp3 =  *(pCmdData+4);

    pCurEntry = g_ModbusGlobals.ModbusModelConfig.pMapEntriesInfo;

    for(i= 0;i< temp2;i++)
    {
	    if(((temp1+i)>=MODBUS_Batt_REGISTERSTART)&&((temp1+i)<=MODBUS_Batt_REGISTEREND))

	    {
		    isetdevicenum =(temp1+i-MODBUS_Batt_REGISTERSTART)/MODBUS_PERBatt_NUM; 
		    iregnum = (temp1+i-MODBUS_Batt_REGISTERSTART)%MODBUS_PERBatt_NUM +MODBUS_Batt_REGISTERSTART;
		    if((iregnum <MODBUS_Batt1SET_REGISTERSTART)||(iregnum >MODBUS_Batt1SET_REGISTEREND))
		    {
			    return  MODBUS_REGERR;
		    }
		   
		    idevicenum = 0;
		    for(j= 0;j< g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum;j++)
		    {	    
			    if(pCurEntry->iModbusPort == iregnum )
			    {
				    iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,0);
				    if(iEquipIDOrder == MODBUS_MAX_EQUIP_NUM)
				    {
					    return MODBUS_CTLFAIL;
				    }
				    iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];

				    bDxiRTN = ModbusGetSCUPSingleSigValues(SIG_TYPE_SAMPLING,100 , iEquipID, &DataValue);

				    if(( bDxiRTN == FALSE)||(DataValue.VData.itemp == 1))
				    {
					    iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
					    if(iEquipIDOrder == MODBUS_MAX_EQUIP_NUM)
					    {
						    return MODBUS_CTLFAIL;
					    }
					    iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
					    continue;		
				    }
				    while(iEquipIDOrder !=0)
				    {	
					   
					    if(isetdevicenum ==  idevicenum)
					    {
						    break;
					    }
					    else
					    {
						    iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
						    if(iEquipIDOrder == MODBUS_MAX_EQUIP_NUM)
						    {
							    return MODBUS_CTLFAIL;
						    }
						    iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0]; 
						    bDxiRTN = ModbusGetSCUPSingleSigValues(SIG_TYPE_SAMPLING,100 , iEquipID, &DataValue);
						    
						    if(( bDxiRTN == FALSE)||(DataValue.VData.itemp == 1))
						    {
							    iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
							    if(iEquipIDOrder == MODBUS_MAX_EQUIP_NUM)
							    {
								    return MODBUS_CTLFAIL;
							    }
							    iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
							    continue;		
						    }
					    }	
					     idevicenum++;
				    }
				    if(iEquipIDOrder == 0)
				    {
					    return  MODBUS_REGERR;
				    }
				    else
				    {
					    break;
				    }

			    }
			    pCurEntry++;
		    }
		    if(j>= g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum )
		    {
			    return  MODBUS_REGERR;
		    }

	    }
	    else
	    {
		    if(i==0)
		    {
			    for(j= 0;j< g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum;j++)
			    {
				    
				    if(pCurEntry->iModbusPort == temp1)
				    {
					    if((pCurEntry ->iSCUPSigType != SIG_TYPE_SETTING)&&(pCurEntry ->iSCUPSigType != SIG_TYPE_CONTROL))
					    {
						    
						    return  MODBUS_REGERR;
					    }
					    break;
				    }
				    pCurEntry++;
				   
			    }
		    }
		    else
		    {
			pCurEntry++;
			if((pCurEntry ->iSCUPSigType != SIG_TYPE_SETTING)&&(pCurEntry ->iSCUPSigType != SIG_TYPE_CONTROL))
			{

				return  MODBUS_REGERR;
			}
		    }
		    
		    if(j>= g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum )
		    {

			    return  MODBUS_REGERR;
		    }
		    iEquipIDOrder = Modbus_GetEquipIDOrder(pCurEntry->iEquipType);
		    iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
	    }

	iData = (int)(MAKEWORD(*(pCmdData+6+i*2),*(pCmdData+5+i*2)));


	iData= iData-pCurEntry->iFlag0;

	bRTN = SetModbusSigValues(iEquipID, 1, pCurEntry,iData,pCurEntry->iFlag1);

	if(bRTN!=TRUE)
	{
	    return MODBUS_CTLFAIL;
	}
    }
    

    return  MODBUS_NOR;

}

/*==========================================================================*
* FUNCTION : MODBUS_AnalyseFrame
* PURPOSE  : 
* CALLS    : SOC_AnalyseSTXFrame
*			  RSOC_AnalyseSTXFrame
* CALLED BY: 
* ARGUMENTS: unsigned unsigned char *  pFrame : 
*            int                       iLen   : 
* RETURN   : FRAME_TYPE : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2006-05-12 16:32
*==========================================================================*/
MODBUS_FRAME_TYPE Modbus_AnalyseFrame(const unsigned char *pFrame, 
				int iLen,
				MODBUS_BASIC_ARGS *pThis,
				OUT unsigned char *pCmdData,
				OUT unsigned char *pMBAPhead)
{
    UNUSED(pThis);
    unsigned int   iControllerAdr,iLengthfromTCPIP;
    int TypeMapNum,i,LengthExCRC;
    MODBUS_TYPE_MAP_INFO *pTypeMap;
    WORD CRCValue,CRCFrame;

    if (iLen < 1)
    {
	return MODBUS_FRAME_ERR;
    }

    if(g_ModbusGlobals.CommonConfig.iMediaType == MODBUS_MEDIA_TYPE_TCPIP )
    {
	    iLengthfromTCPIP =(int)(MAKEWORD(*(pFrame+5),*(pFrame+4)));
	    if((iLengthfromTCPIP+MODBUS_MBAP_LEN-MODBUS_MBAP_IDENTIFIER_LEN) > iLen)
	    {

		return MODBUS_FRAME_ERR;
	    }
	    for(i=0;i<MODBUS_MBAP_LEN ;i++)
	    {
		    *(pMBAPhead+i) = *(pFrame+i);
	    }

	    for(i=0;i<(iLengthfromTCPIP-MODBUS_MBAP_IDENTIFIER_LEN) && i<MAX_MODBUSFRAME_LEN ;i++)
	    {
		    *(pCmdData+i) = *(pFrame+MODBUS_MBAP_LEN+1+i);
	    }
	    if(i<MAX_MODBUSFRAME_LEN)
	    {
		 *(pCmdData+i) = '\0';
	    }
	
    }
    else
    {
	iControllerAdr = (*pFrame);
	if( iControllerAdr != g_ModbusGlobals.CommonConfig.byADR)
	 {
 
		TRACE("\n Modbus_AnalyseFrame:byADR is %d\n", iControllerAdr);
		TRACE("\n g_ModbusGlobals.CommonConfig.byADR is %d\n", g_ModbusGlobals.CommonConfig.byADR);
		return MODBUS_FRAME_ERR;//added at 2006.9.1��not send anything if address is wrong.
	}

	pTypeMap = g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo;
	TypeMapNum = g_ModbusGlobals.ModbusModelConfig.iTypeMapNum;

	for (i= 0;i< TypeMapNum;i++,pTypeMap++)
	{

		 if(*(pFrame+1) == pTypeMap->iFunctionCode)
		{
			if(pTypeMap->iLengthFix == 0)
			{
				LengthExCRC = pTypeMap->iLength -2;
			}
			else
			{
				LengthExCRC =pTypeMap->iLength+(*(pFrame+pTypeMap->iLength -1));

			}
			break;
		}
	 }
	if(i>=TypeMapNum)
	{

		return MODBUS_FRAME_ERR;
 
	}
	CRCValue = ModbusCRC(pFrame,LengthExCRC);
	CRCFrame = MAKEWORD(*(pFrame+LengthExCRC+1),*(pFrame+LengthExCRC));
	if(CRCValue != CRCFrame)
	{
		return MODBUS_FRAME_ERR;
	}

	 for(i=0;i< LengthExCRC && i<MAX_MODBUSFRAME_LEN ;i++)
	{
		*(pCmdData+i) = *(pFrame+2+i);

	}
	 if(i<MAX_MODBUSFRAME_LEN)
	 {
		 *(pCmdData+i) = '\0';
	 }
    }


    return MODBUS_NOR;
}


/*==========================================================================*
* FUNCTION : Modbus_BuildResponseFrame
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN const unsigned char  *szResData    : 
*            OUT unsigned char        *pFrameData  :
*			  OUT int      *piFrameDataLen :
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2004-10-13 09:37
*==========================================================================*/
void Modbus_03CodeResponseFrame(  IN  MODBUS_FRAME_TYPE RTN,
				  IN  int iAddr,//use at TCP/IP
				  unsigned char *pCmdData,
				  OUT unsigned char *pFrameData,
				  OUT int *piFrameDataLen)
{
    unsigned char *pData,len;
    int i,iStartREG;

    pData = pFrameData;
    *pData++ = iAddr;

    if(RTN == MODBUS_NOR)
    {
	*pData++ = (unsigned char)0x03;
	 iStartREG=(int)(MAKEWORD(*(pCmdData+1),*pCmdData));
	 len= (unsigned char)(MAKEWORD(*(pCmdData+3),*pCmdData+2));
	*pData++ =  len;
	for(i=0;i<len;i++)
	{
	    *pData++ = HIBYTE(g_ModbusRespData.szRespData1[iStartREG+i]);
	    *pData++ = LOBYTE(g_ModbusRespData.szRespData1[iStartREG+i]);

	}
	WORD wCRC = ModbusCRC(pFrameData, 3+len*2);
	*pData++ = HIBYTE(wCRC);
	*pData++ = LOBYTE(wCRC);
	*piFrameDataLen = 3+len*2+2;


    }
    else
    {
	*pData++ = (unsigned char)(0x03|0x80);
	*pData++ = (unsigned char)RTN;
	WORD wCRC = ModbusCRC(pFrameData, 3);
	*pData++ = HIBYTE(wCRC);
	*pData++ = LOBYTE(wCRC);
	*piFrameDataLen = 5;

    }

}

/*==========================================================================*
* FUNCTION : Modbus_BuildResponseFrame
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN const unsigned char  *szResData    : 
*            OUT unsigned char        *pFrameData  :
*			  OUT int      *piFrameDataLen :
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2004-10-13 09:37
*==========================================================================*/
void Modbus_06CodeResponseFrame(  IN  MODBUS_FRAME_TYPE RTN,
				IN  int iAddr,//use at TCP/IP
				unsigned char *pCmdData,
				OUT unsigned char *pFrameData,
				OUT int *piFrameDataLen)
{
    unsigned char *pData;
    int i;

    pData = pFrameData;
    *pData++ = iAddr;

    if(RTN == MODBUS_NOR)
    {
	*pData++ = (unsigned char)0x06;
	for(i=0;i<4;i++)
	{
	    *pData++ =  *pCmdData;
	}
	WORD wCRC = ModbusCRC(pFrameData, 6);
	*pData++ = HIBYTE(wCRC);
	*pData++ = LOBYTE(wCRC);
	*piFrameDataLen = 8;


    }
    else
    {
	*pData++ = (unsigned char)(0x06|0x80);
	*pData++ = (unsigned char)RTN;
	WORD wCRC = ModbusCRC(pFrameData, 3);
	*pData++ = HIBYTE(wCRC);
	*pData++ = LOBYTE(wCRC);
	*piFrameDataLen = 5;

    }

}

/*==========================================================================*
* FUNCTION : Modbus_BuildResponseFrame
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN const unsigned char  *szResData    : 
*            OUT unsigned char        *pFrameData  :
*			  OUT int      *piFrameDataLen :
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2004-10-13 09:37
*==========================================================================*/
void Modbus_10CodeResponseFrame(  IN  MODBUS_FRAME_TYPE RTN,
				IN  int iAddr,//use at TCP/IP
				unsigned char *pCmdData,
				OUT unsigned char *pFrameData,
				OUT int *piFrameDataLen)
{
    unsigned char *pData;
    int i;

    pData = pFrameData;
    *pData++ = iAddr;

    if(RTN == MODBUS_NOR)
    {
	*pData++ = (unsigned char)0x10;
	for(i=0;i<4;i++)
	{
	    *pData++ =  *pCmdData;
	}
	WORD wCRC = ModbusCRC(pFrameData, 6);
	*pData++ = HIBYTE(wCRC);
	*pData++ = LOBYTE(wCRC);
	*piFrameDataLen = 8;


    }
    else
    {
	*pData++ = (unsigned char)(0x06|0x80);
	*pData++ = (unsigned char)RTN;
	WORD wCRC = ModbusCRC(pFrameData, 3);
	*pData++ = HIBYTE(wCRC);
	*pData++ = LOBYTE(wCRC);
	*piFrameDataLen = 5;

    }

}

/*==========================================================================*
* FUNCTION : Modbus_BuildResponseFrame
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN const unsigned char  *szResData    : 
*            OUT unsigned char        *pFrameData  :
*			  OUT int      *piFrameDataLen :
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2004-10-13 09:37
*==========================================================================*/
void Modbus_CodeResponseFrame(  IN  MODBUS_FRAME_TYPE RTN,
				IN  int iAddr,
				IN  int iFunctionCode,
				unsigned char *pCmdData,
				unsigned char *pMBAPhead,
				OUT unsigned char *pFrameData,
				OUT int *piFrameDataLen)
{
    unsigned char *pData,len;
    int i,iStartREG;
	MODBUSMODEL_CONFIG_INFO		*pstModelCfg = &g_ModbusGlobals.ModbusModelConfig;

    pData = pFrameData;
    if(g_ModbusGlobals.CommonConfig.iMediaType == MODBUS_MEDIA_TYPE_TCPIP )
    {
	    for(i=0;i<4;i++)
	    {
		*pData++ = *(pMBAPhead+i);

	    }

	    if(RTN == MODBUS_NOR)
	    {

		    if(iFunctionCode ==MODBUS_GETDATA_CMD)
		    {
			    iStartREG=(int)(MAKEWORD(*(pCmdData+1),*pCmdData));

			    len= (unsigned char)(MAKEWORD(*(pCmdData+3),*(pCmdData+2)));
			    *pData++ = HIBYTE(len*2+3);
			    *pData++ = LOBYTE(len*2+3);
			    *pData++ = *(pMBAPhead +MODBUS_MBAP_LEN-1);
			    *pData++ = (unsigned char)iFunctionCode; 
			    *pData++ =  len*2;
			    for(i=0;i<len;i++)
			    {
					//Frank Wu,20151112, for extending configuration file
					if(MODBUS_CONFIG_VERSION_2 == pstModelCfg->iExtCurrentCfgVersion)
					{
						*pData++ = HIBYTE(g_ModbusRespData.pszExtRespData1[(iStartREG + 1) + i]);
						*pData++ = LOBYTE(g_ModbusRespData.pszExtRespData1[(iStartREG + 1) + i]);
					}
					else
					{
						*pData++ = HIBYTE(g_ModbusRespData.szRespData1[iStartREG+i]);
						*pData++ = LOBYTE(g_ModbusRespData.szRespData1[iStartREG+i]);
					}
			    }
			    *piFrameDataLen = MODBUS_MBAP_LEN+ 2+len*2;

		    }
		    else if((iFunctionCode ==MODBUS_SETSINGLEDATA_CMD)||(iFunctionCode ==MODBUS_SETMULTIDATA_CMD))
		    {
			    *pData++ = HIBYTE(6);
			    *pData++ = LOBYTE(6);
			    *pData++ = *(pMBAPhead +MODBUS_MBAP_LEN-1);

			    *pData++ = (unsigned char)iFunctionCode; 
			    for(i=0;i<4;i++)
			    {
				    *pData++ =  *pCmdData++;
			    }
			    *piFrameDataLen = MODBUS_MBAP_LEN+5;

		    }
		    
	    }
	    else
	    {
		    *pData++ = HIBYTE(MODBUS_ERRORRTN_LEN);
		    *pData++ = LOBYTE(MODBUS_ERRORRTN_LEN);
		    *pData++ = *(pMBAPhead +MODBUS_MBAP_LEN-1);
		    *pData++ = ((unsigned char)iFunctionCode)|0x80;
		    *pData++ = (unsigned char)RTN;
		    *piFrameDataLen = MODBUS_MBAP_LEN +2;
	    }
    }
    else
    {
	*pData++ = iAddr;
   
	    if(RTN == MODBUS_NOR)
	    {
		*pData++ = (unsigned char)iFunctionCode;
		if(iFunctionCode ==MODBUS_GETDATA_CMD)
		{
		    iStartREG=(int)(MAKEWORD(*(pCmdData+1),*pCmdData));
		    len= (unsigned char)(MAKEWORD(*(pCmdData+3),*(pCmdData+2)));
		    *pData++ =  len*2;
		    for(i=0;i<len;i++)
		    {
				//Frank Wu,20151112, for extending configuration file
				if(MODBUS_CONFIG_VERSION_2 == pstModelCfg->iExtCurrentCfgVersion)
				{
					*pData++ = HIBYTE(g_ModbusRespData.pszExtRespData1[(iStartREG + 1) + i]);
					*pData++ = LOBYTE(g_ModbusRespData.pszExtRespData1[(iStartREG + 1) + i]);
				}
				else
				{
					*pData++ = HIBYTE(g_ModbusRespData.szRespData1[iStartREG+i]);
					*pData++ = LOBYTE(g_ModbusRespData.szRespData1[iStartREG+i]);
				}
		    }
		    WORD wCRC = ModbusCRC(pFrameData, 3+len*2);
		    *pData++ = HIBYTE(wCRC);
		    *pData++ = LOBYTE(wCRC);
		    *piFrameDataLen = 3+len*2+2;

		}
		else if((iFunctionCode ==MODBUS_SETSINGLEDATA_CMD)||(iFunctionCode ==MODBUS_SETMULTIDATA_CMD))
		{
		    for(i=0;i<4;i++)
		    {
			*pData++ =  *pCmdData++;
		    }
		    WORD wCRC = ModbusCRC(pFrameData, 6);
		    *pData++ = HIBYTE(wCRC);
		    *pData++ = LOBYTE(wCRC);
		    *piFrameDataLen = 8;

		}


	    }
	    else
	    {
		*pData++ = ((unsigned char)iFunctionCode)|0x80;
		*pData++ = (unsigned char)RTN;
		WORD wCRC = ModbusCRC(pFrameData, 3);
		*pData++ = HIBYTE(wCRC);
		*pData++ = LOBYTE(wCRC);
		*piFrameDataLen = 5;

	    }
    }

}


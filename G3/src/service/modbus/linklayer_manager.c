/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : linklayer_manager.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "modbus.h"
#include <arpa/inet.h>			/* inet_ntoa */


/* timeout uses in communication, unit: ms */
#define MODBUS_WAITFORCONN_TIMEOUT    2000  /* wait for connected timeout */
#define MODBUS_READCOM_TIMEOUT		   1000  
#define MODBUS_WRITECOM_TIMEOUT	   2000
#define MODBUS_CONN_TIMEOUT           5000 /* connect to remote timeout */


/* log Macros for Link-layer Manager sub-module(LM = linklayer manager) */
#define MODBUS_TASK_LM		"MODBUS Linklayer Manager"

/* error log */
#define LOG_MODBUS_LM_E(_szLogText)  LOG_MODBUS_E(MODBUS_TASK_LM, _szLogText)
	
/* extended error log(logged with error code) */
#define LOG_MODBUS_LM_E_EX(_szLogText, _errCode)  \
	LOG_MODBUS_E_EX(MODBUS_TASK_LM, _szLogText, _errCode)

/* warning log */
#define LOG_MODBUS_LM_W(_szLogText)  LOG_MODBUS_W(MODBUS_TASK_LM, _szLogText)

/* info log */
#define LOG_MODBUS_LM_I(_szLogText)  LOG_MODBUS_I(MODBUS_TASK_LM, _szLogText)


#define MODBUS_STDPORT_TCP		1
#define	MODBUS_STDPORT_LEASEDLINE	3
#define MODBUS_STDPORT_MODEM		4
#define	MODBUS_STDPORT_RS485		5


/* wait for comm port has been closed */
__INLINE static void SafeOpenComm(MODBUS_BASIC_ARGS *pThis)
{
#ifdef COM_SHARE_SERVICE_SWITCH
	int iMediaType;
	//HANDLE hMutex;  /* not need to use Mutex */
	SERVICE_SWITCH_FLAG *pFlag;

	HANDLE hLocalThread;

	iMediaType = g_ModbusGlobals.CommonConfig.iMediaType;
	pFlag = DXI_GetServiceSwitchFlag();
	hLocalThread = pThis->hThreadID[1];

	/* wait until Serial Port has closed by Maintenace Service */
	if (MODBUS_IS_SHARE_COM(iMediaType))
	{
		//TRACE("pFlag->bCOMHaveClosed is %d\n", pFlag->bCOMHaveClosed);
		//TRACE("iMediaType is %d\n", iMediaType);
		while (1)
		{
			if (pFlag->bCOMHaveClosed)
			{
				pFlag->bCOMHaveClosed = FALSE;
				pFlag->bHLMSUseCOM = TRUE;

				break;
			}
			else
			{	
#ifdef _DEBUG_MODBUS_LINKLAYER
				
				TRACE("Serial Port has not been closed!!\n");
#endif 

				Sleep(3000);//yield

				/* feed watch dog */
				RunThread_Heartbeat(hLocalThread);
			}
		}
	}

#endif //COM_SHARE_SERVICE_SWITCH

	return;
}

void LeaveCommUsing(void)
{
#ifdef COM_SHARE_SERVICE_SWITCH
	int iMediaType;
	//HANDLE hMutex;  /* not need to use Mutex */
	SERVICE_SWITCH_FLAG *pFlag;

	iMediaType = g_ModbusGlobals.CommonConfig.iMediaType;
	pFlag = DXI_GetServiceSwitchFlag();

	
	TRACE("iMediaType:LeaveCommUsing is %d\n", iMediaType);
	pFlag->bCOMHaveClosed = TRUE;
	

#endif //COM_SHARE_SERVICE_SWITCH

	return;
}

/*==========================================================================*
 * FUNCTION : ComQueue
 * PURPOSE  : communicate with Event Queues
 * CALLS    : Queue_Get
 *			  Queue_Put
 *			  CommWrite
 * CALLED BY: ComMC
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis       : 
 *            BOOL            bSendTimeOut : 
 * RETURN   : int : MODBUS_COMM_STATUS 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 14:20
 *==========================================================================*/
static int ComQueue(MODBUS_BASIC_ARGS *pThis, BOOL bSendTimeOut)
{
	MODBUS_EVENT *pEvent;
	int iRet, iCommWriteRet;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[1]);

	if (bSendTimeOut) /* send timeout event to EventInputQueue */
	{
		pEvent = &g_ModbusTimeoutEvent;

		/* send it to the EventInputQueue */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);

		if (iRet != ERR_OK)
		{
			LOG_MODBUS_LM_E_EX("send event to Event InputQueue failed", iRet);

			/* exit Modbus service */
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

			return MODBUS_COMM_STATUS_DISCONNECT;
		}
	}

	iRet = Queue_Get(pThis->hEventOutputQueue, &pEvent, FALSE, MODBUS_WAIT_FOR_O_QUEUE);
	//TRACE("\n iRet:ComQueue is %d\n", iRet);

#ifdef _TEST_MODBUS_QUEUE_SYNCHRONIZATION
	{
		int iCount;
		MODBUS_EVENT *pLocalEvent;
		iCount = Queue_GetCount(pThis->hEventOutputQueue, NULL);
		if (iCount > 0)
		{
			TRACEX("Output Queue: Count is %d\n", iCount);
			TRACE("Time is: %.3f\n", GetCurrentTime());
			Queue_Get(pThis->hEventOutputQueue, &pLocalEvent, TRUE, 1000);
			MODBUS_PrintEvent(pLocalEvent);
		}

		iCount = Queue_GetCount(pThis->hEventInputQueue, NULL);
		if (iCount > 0)
		{
			TRACEX("Input Queue: Count is %d\n", iCount);
			TRACE("Time is: %.3f\n", GetCurrentTime());
			Queue_Get(pThis->hEventInputQueue, &pLocalEvent, TRUE, 1000);
			MODBUS_PrintEvent(pLocalEvent);
		}
	}
#endif //_TEST_MODBUS_QUEUE_SYNCHRONIZATION

	switch (iRet)
	{
	case ERR_OK:
		switch (pEvent->iEventType)
		{
		case MODBUS_FRAME_EVENT:
			if (pEvent->bDummyFlag)
			{
				/* sleep 100 ms in lest two frame are sent together */
				/* normally, byte-read-delay is 50 ms */
				Sleep(100);//yield
				return MODBUS_COMM_STATUS_NEXT;
			}
			else
			{
#ifdef _DEBUG_MODBUS_LINKLAYER
				{
					int i, iLen;
					unsigned char chr;
					TRACE_MODBUS_TIPS("received frame event from OutputQueue, "
						"write the Data to Comm Port");
					iLen = pEvent->iDataLength;
					TRACE("\tData Length: %d\n", iLen);

					TRACE("\tData Content(Hex format):");
					for (i = 0; i < iLen; i++)
					{
						printf("%02X ", pEvent->sData[i]);
					}
					printf("\n");

					TRACE("\tData Content(Text format):");
					for (i = 0; i < iLen; i++)
					{
						chr = (unsigned char)pEvent->sData[i];
						if (chr < 0x21 || chr > 0X7E)
						{
							printf("%s", MODBUS_GetUnprintableChr(chr));
						}
						else
						{
							printf("%c", chr);
						}
					}
					printf("\n");

				}					
#endif 
		
				int		iRst;

				CommWrite(pThis->hComm, 
					pEvent->sData, 
					pEvent->iDataLength);
				//DELETE(pEvent);

				/* get the error code */
				iCommWriteRet = CommGetLastError(pThis->hComm);
				if (iCommWriteRet != ERR_OK)
				{
					LOG_MODBUS_LM_E_EX("Write to Comm Port failed", 
						iCommWriteRet);

					/* When timeout, not exit. */
					if (iCommWriteRet != ERR_TIMEOUT)
					{
						/* exit Modbus service */
						pThis->bServerModeNeedExit = TRUE;
						pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
					}

					iRst = MODBUS_COMM_STATUS_DISCONNECT;
				}


				else if (pEvent->bSkipFlag)
				{
					iRst = MODBUS_COMM_STATUS_SKIP;
				}
				else
				{
					iRst = MODBUS_COMM_STATUS_NEXT;
				}

				DELETE(pEvent);
				return iRst;
			}

		case MODBUS_DISCONNECTED_EVENT:
			TRACE_MODBUS_TIPS("Disconnected event received from "
				"EventOutputQueue");
			/*DELETE(pEvent);*/
			return MODBUS_COMM_STATUS_DISCONNECT;

		default:
			TRACE_MODBUS_TIPS("received none-expected event from EventOutputQueue");

			DELETE_MODBUS_EVENT(pEvent);
			return MODBUS_COMM_STATUS_NEXT;
		}

	case ERR_QUEUE_EMPTY: //means timeout
		TRACE_MODBUS_TIPS("get event from EventOutputQueue timeout");

		return MODBUS_COMM_STATUS_NEXT;

	default:         /* is ERR_INVALID_ARGS */ 
		LOG_MODBUS_LM_E_EX("Get event from Event OutputQueue failed", iRet);

		/* exit Modbus service */
		pThis->bServerModeNeedExit = TRUE;
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

		return MODBUS_COMM_STATUS_DISCONNECT;
	}
}


/*==========================================================================*
 * FUNCTION : PickupAFrame
 * PURPOSE  : Pickup a frame from input data buffer
 * CALLS    : 
 * CALLED BY: CommReadFrame
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis        : 
 *            int             iProtocolType : 
 *            char            *pDstBuf      : 
 *            int             *piLen        : 
 * RETURN   : BOOL : TRUE means get a frame from the received buf
 *					 FALSE means received buf is empty
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-12 09:55
 *==========================================================================*/
static BOOL PickupAFrame(MODBUS_BASIC_ARGS *pThis, int iProtocolType, 
						 char *pDstBuf, int *piLen)
{
	int iRet = FALSE;
	int iSrcPos, iSrcLen;
	char *pSrcBuf;

	iSrcPos = pThis->iCurPos;
	iSrcLen = pThis->iLen;
	//iSrcLen = strlen(pThis->sReceivedBuff);
	pSrcBuf = pThis->sReceivedBuff;
	*piLen = 0;

	if (iSrcPos < iSrcLen)
	{
		iRet = TRUE;
		switch (iProtocolType)
		{
		    //need extra code
		    case MODBUS:

#ifdef	_DEBUG_MODBUS
	    		if((iSrcLen) > MAX_MODBUSFRAME_LEN)
			{
					AppLogOut("MODBUS SRV", APP_LOG_ERROR, "***Error: PickupAFrame iSrcLen greater than MAX_MODBUSFRAME_LEN.\n");
			}
#endif
			   memcpy(pDstBuf, pSrcBuf + iSrcPos, (size_t)iSrcLen);
			   *piLen = iSrcLen;
			   iSrcPos = iSrcLen;
			  
		}
		/* to delete */
		if (iSrcPos < iSrcLen)
		{
			TRACE("Split a frame!\n");
		}
		/* end */
	}

	pThis->iCurPos = iSrcPos;
	return iRet;
}


/*==========================================================================*
 * FUNCTION : CommReadFrame
 * PURPOSE  : 
 * CALLS    : PickupAFrame
 * CALLED BY: ComMC
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis   : 
 *            char            *pBuffer : 
 *            int             *piLen   : 
 *            int             iTimeOut : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-12 09:56
 *==========================================================================*/
static int CommReadFrame(MODBUS_BASIC_ARGS *pThis, char *pBuffer, 
						 int *piLen, int iTimeOut)
{
	HANDLE hComm;
	int    iProtocolType, iErrCode;
	COMM_TIMEOUTS timeOut;

	hComm = pThis->hComm;
	iErrCode = ERR_COMM_OK;
	iProtocolType = g_ModbusGlobals.CommonConfig.iProtocolType;

	if (!PickupAFrame(pThis, iProtocolType, pBuffer, piLen))  /* received buff is empty */
	{
		/* set timeout */
		INIT_TIMEOUTS(timeOut, iTimeOut, MODBUS_WRITECOM_TIMEOUT);
		CommControl(hComm, 
			COMM_SET_TIMEOUTS,
			(char *)&timeOut,
			sizeof(COMM_TIMEOUTS)); 

		pThis->iCurPos = 0;
	 	pThis->iLen = CommRead(hComm, pThis->sReceivedBuff, MAX_MODBUSFRAME_LEN);
		/* get last err code */
		CommControl(hComm, 
			COMM_GET_LAST_ERROR, 
			(char *)&iErrCode, 
			sizeof(int));

		PickupAFrame(pThis, iProtocolType, pBuffer, piLen);
	}
	return iErrCode;
}

/*==========================================================================*
* FUNCTION : ModbusFrameCheck
* PURPOSE  : check the frame is right
* CALLS    : CommRead 
*	     Queue_Put  
*	    ComQueue
* CALLED BY: DataExchange
* ARGUMENTS: MODBUS_BASIC_ARGS  *pThis    : 
*            int             iTimeOut  : 
*            BOOL            bSkipFlag : 
* RETURN   : int : MODBUS_COMM_STATUS
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2006-06-11 13:05
*==========================================================================*/
static BOOL ModbusFrameCheck(char *pBuffer,int length, int* startposition ,int* validlen)
{
    int i,start ,TypeMapNum,framelength;
    MODBUS_TYPE_MAP_INFO *pTypeMap;
    char *pReceiveBuffer;
    
    pReceiveBuffer = pBuffer;
    pTypeMap = g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo;
    TypeMapNum = g_ModbusGlobals.ModbusModelConfig.iTypeMapNum;

    if(length <0)
    {
	return FALSE;
    }

    for (i= 0;i<length;i++)
    {
	if((*pReceiveBuffer++) == g_ModbusGlobals.CommonConfig.byADR)
	    break;

    }
    if(i>=length)
    {
	return FALSE;
    }
    start = i;
    for (i= 0;i< TypeMapNum;i++,pTypeMap++)
    {
	if((*pReceiveBuffer) == (pTypeMap->iFunctionCode))
	{
	    if((pTypeMap->iLengthFix) == 0)
	    {
		if((length -start)<(pTypeMap->iLength))
		{
		    return FALSE;
		}
		else
		{
		    (*startposition)= start;
		    (*validlen) = (pTypeMap->iLength);
		    return TRUE;
		}
	    }
	    else
	    {
		framelength =(pTypeMap->iLength)+(*(pReceiveBuffer+(pTypeMap->iLength) -2))+2;
		if((length -start)<framelength)
		{
		    return FALSE;
		}
		else
		{
		    (*startposition)= start;
		    (*validlen) = framelength;
		    return TRUE;
		}

	    }
	}
    }
   
	return FALSE;


}
/*==========================================================================*
 * FUNCTION : ComMC
 * PURPOSE  : communicate with MC
 * CALLS    : CommRead 
 *			  Queue_Put  
 *			  ComQueue
 * CALLED BY: DataExchange
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis    : 
 *            int             iTimeOut  : 
 *            BOOL            bSkipFlag : 
 * RETURN   : int : MODBUS_COMM_STATUS
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 13:05
 *==========================================================================*/
static int ComMC(MODBUS_BASIC_ARGS *pThis, int iTimeOut, BOOL bSkipFlag)
{
	HANDLE hComm;
	char sBuffer[MAX_MODBUSFRAME_LEN] = {'\0'};
	//char sBuffer[512];
	int  iLen, iErrCode, iRet, i, j, k;
	MODBUS_EVENT *pEvent;

	hComm = pThis->hComm;

	i = 0;
	j = 0;
	k = 0;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[1]);

	/* skip the step to keep synchronization */
	if (bSkipFlag)
	{
		return ComQueue(pThis, FALSE);
	}
	iErrCode = CommReadFrame(pThis, sBuffer, &iLen, iTimeOut);

	/* check the error code */
	switch (iErrCode) 
	{
	    case ERR_COMM_OK:
		    break;

	    case ERR_COMM_TIMEOUT:

		    if (iLen > 0)  //  is not timeout
		    {
			if(g_ModbusGlobals.CommonConfig.iMediaType !=MODBUS_MEDIA_TYPE_TCPIP)
			{
				if(ModbusFrameCheck(sBuffer,iLen,&j,&k) == FALSE)
				{

				    return ComQueue(pThis, TRUE);
				}
				else
				{

				     iLen = k ;
				}
			}
			if((iLen) > MAX_MODBUSFRAME_LEN)
			  {
			    AppLogOut("MODBUS SRV", APP_LOG_ERROR, "***Error: iLen greater than 256.\n");
			 				
			   }
			break;
		    }

		    return ComQueue(pThis, TRUE);

	    case ERR_COMM_CONNECTION_BROKEN:

		    return MODBUS_COMM_STATUS_DISCONNECT;

	    default:
		    TRACE("The received string is : %s\n", sBuffer);
		    LOG_MODBUS_LM_E_EX("Read Comm Port failed", iErrCode);

		    pThis->bServerModeNeedExit = TRUE;
		    pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

		    return MODBUS_COMM_STATUS_DISCONNECT;
	}

	pEvent = NEW(MODBUS_EVENT, 1);
	if (pEvent == NULL)
	{
		LOG_MODBUS_LM_E("no memory to create Frame Event");
		
		/* exit MODBUS service */
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_MEMORY;

		return MODBUS_COMM_STATUS_DISCONNECT;
	}
	MODBUS_INIT_FRAME_EVENT(pEvent, iLen, sBuffer + j, 0, 0);

	/* send it to the EventInputQueue */
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_MODBUS_LM_E_EX("Send Frame event to InputQueue failed", iErrCode);

		pThis->bServerModeNeedExit = TRUE;
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

		return MODBUS_COMM_STATUS_DISCONNECT;
	}
	return ComQueue(pThis, FALSE);

}
			

/*==========================================================================*
 * FUNCTION : DataExchange
 * PURPOSE  : data exchange between MC and Event Queues
 * CALLS    : ComMC
 * CALLED BY: RunAsServer
 *			  RunAsClient
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 11:50
 *==========================================================================*/
static void DataExchange(MODBUS_BASIC_ARGS *pThis)
{
	int iRet;
	BOOL bSkipFlag;

	bSkipFlag = FALSE;


	while (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		iRet = ComMC(pThis, MODBUS_READCOM_TIMEOUT, bSkipFlag);

		switch (iRet)
		{
		    case MODBUS_COMM_STATUS_DISCONNECT:
			return;

		    case MODBUS_COMM_STATUS_SKIP:
			bSkipFlag = TRUE;
			break;

		    case MODBUS_COMM_STATUS_NEXT:
			bSkipFlag = FALSE;
			break;
		}
	}		
}


/*==========================================================================*
 * FUNCTION : RunAsServer
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: MODBUS_LinkLayerManager
 * ARGUMENTS: MODBUS_BASIC_ARGS *  pThis      : 
 *            PORT_INFO         *pCommPort : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-10 21:27
 *==========================================================================*/
static void RunAsServer(MODBUS_BASIC_ARGS *pThis, PORT_INFO *pCommPort)
{
	HANDLE hOpenComm, hDataComm;
	int iErrCode, iTryCount;

	/* to get MODBUS common config info */
	MODBUS_COMMON_CONFIG *pCommonCfg;
	int iMediaType;   

	/* to send disconnected event */
	int iRet;
	MODBUS_EVENT *pEvent;

    /* to get client IP */
	COMM_PEER_INFO cpi;   
	DWORD dwPeerIP = 0;
	char *szClientIP;

	HANDLE hLocalThread;
	COMM_TIMEOUTS timeOut;

	char *szLibName;
	char szOpenParam[64];

	hLocalThread = pThis->hThreadID[1];
	pCommonCfg = &g_ModbusGlobals.CommonConfig;
	iMediaType = pCommonCfg->iMediaType;

	/* reset flag */
	pThis->bServerModeNeedExit = FALSE;
	
	if (iMediaType == MODBUS_MEDIA_TYPE_TCPIP)
	{
		pCommPort->iStdPortTypeID = MODBUS_STDPORT_TCP;
		//strcpy(szOpenParam, "142.100.4.35:");
		strcpy(szOpenParam, "127.0.0.1:");
	}
	else if (iMediaType == MODBUS_MEDIA_TYPE_LEASED_LINE)
	{
		pCommPort->iStdPortTypeID = MODBUS_STDPORT_LEASEDLINE;
	}
	else 
	{
		pCommPort->iStdPortTypeID = MODBUS_STDPORT_RS485;
	}
	

	
    
	/* get lib name and port param */
	szLibName = GetStandardPortDriverName(pCommPort->iStdPortTypeID);

	/* get comm port param */
	if (iMediaType == MODBUS_MEDIA_TYPE_TCPIP)
	{
		strncat(szOpenParam, pCommonCfg->szCommPortParam,
			sizeof(szOpenParam));
	}
	else
	{
		strncpyz(szOpenParam, pCommonCfg->szCommPortParam,
				sizeof(szOpenParam));
	}

    #ifdef _DEBUG_MODBUS_LINKLAYER
		TRACE_MODBUS_TIPS("CC Comm Port Info:");
		TRACE("\tComm Port ID: %d\n", pCommPort->iPortID);
		TRACE("\tStdPort ID: %d\n", pCommPort->iStdPortTypeID);
		TRACE("\tStdPort accessing libname: %s\n", szLibName);
		TRACE("\tOpen Param: %s\n", szOpenParam);
		TRACE("\tpiTimeout: %d\n", pCommPort->iTimeout);
		TRACE("\tpszDeviceDesp: %s\n", pCommPort->szDeviceDesp);
		TRACE("\szPortName: %s\n", pCommPort->szPortName);
	#endif //_DEBUG_MODBUS_LINKLAYER

	//SafeOpenComm(pThis);
    //TRACE("Get out of SafeOpenComm!!\n");
	//printf("szLibName is %s\n", szLibName);
	//printf("pCommPort->szDeviceDesp is %s\n", pCommPort->szDeviceDesp);
	/* open the comm, retry 3 times */
	for (iTryCount = 0; iTryCount < 3; iTryCount++)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);
		
		hOpenComm = CommOpen(szLibName,
			pCommPort->szDeviceDesp,
			szOpenParam,
			COMM_SERVER_MODE,
			pCommPort->iTimeout,
			&iErrCode);
		TRACE("\niErrCode is: servermode %u\n", iErrCode);

		switch (iErrCode)
		{
		    case ERR_COMM_STD_PORT:
			    LOG_MODBUS_LM_E("Open Comm Port failed for StdPort ID is invalid");

			    /* exit the service */
			    pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
			    return;

		    case ERR_COMM_OPENING_PARAM:
				TRACE("\tiErrCode: %d\n", iErrCode);
				LOG_MODBUS_LM_E("Open Comm Port failed for Opening Param is invalid");
	            
				/* exit programme */
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_CFG;
				return;

		   case ERR_OK:
				iTryCount = 3;  /* break while loop */
				break;

		   default:  /* do nothing, just retry */
				Sleep(1000); //yield
				break;
		}
	}


	if (iErrCode != ERR_OK)
	{
		LOG_MODBUS_LM_E_EX("Open failed after 3 times retry", iErrCode);

		/* exit programme */
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
		return;
	}

	/* main loop */
	INIT_TIMEOUTS(timeOut, MODBUS_WAITFORCONN_TIMEOUT, MODBUS_WRITECOM_TIMEOUT);
	while (!pThis->bServerModeNeedExit &&
		     pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);

		Sleep(50);//yield

        //pThis->bSecureLine = TRUE;
CommControl(hOpenComm, 
			COMM_GET_LAST_ERROR,
			(char *)&iErrCode, 
			sizeof(int));

		/* set accept timeout */
		CommControl(hOpenComm, 
			COMM_SET_TIMEOUTS,
			(char *)&timeOut,
			sizeof(COMM_TIMEOUTS)); 
CommControl(hOpenComm, 
			COMM_GET_LAST_ERROR,
			(char *)&iErrCode, 
			sizeof(int));

		hDataComm = CommAccept(hOpenComm);

		if (hDataComm == NULL)
		{
			/* get last err code */
			CommControl(hOpenComm, 
				        COMM_GET_LAST_ERROR,
						(char *)&iErrCode, 
						sizeof(int));
			//TRACE("\niErrCode1 is: %d", iErrCode);


			if (iErrCode == ERR_COMM_TIMEOUT)
			{
				continue;
			}
			else  /* for other err, exit programme */
			{
				LOG_MODBUS_LM_E_EX("Accept failed", iErrCode);
				
				CommClose(hOpenComm);
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
				TRACE("\nErr1!!\n");
                 
				return;
			}
		} /* end of if (hDataComm == NULL) */

		/* successfully got hDataComm */
		/* check mode, if has something to report, return */ 
		/* Note: mode can be changed when bCommRunning is FALSE */
		if (pThis->iOperationMode !=MODBUS_MODE_SERVER)
		{
		    TRACE("\n Run Serial_CommClose1!!\n");
			CommClose(hDataComm);
			CommClose(hOpenComm);
			return;
		}
		else
		{
			pThis->hComm = hDataComm;
		}

		TRACE_MODBUS_TIPS("Connected");

		/* set Running flag first to inhibit report */
		pThis->bCommRunning = TRUE;


        // set security flag
		switch (iMediaType)
		{
		    case  MODBUS_MEDIA_TYPE_RS485:
			pThis->bSecureLine = TRUE;
			break;
		    case MODBUS_MEDIA_TYPE_LEASED_LINE:  //alaways secure
			pThis->bSecureLine = TRUE;
			break;
		    case MODBUS_MEDIA_TYPE_TCPIP:  //check secure IP list
			pThis->bSecureLine = TRUE;

			iErrCode = CommControl(hDataComm, 
				COMM_GET_PEER_INFO, (char *)&cpi,
				sizeof(COMM_PEER_INFO));
			
			if (iErrCode == ERR_COMM_OK)
			{
				// szAddr is in network format.
				dwPeerIP = *(DWORD *)cpi.szAddr;
				szClientIP = inet_ntoa(*(struct in_addr *)&dwPeerIP);
	
				
#ifdef _DEBUG_MODBUS_LINKLAYER
				{
					char szLogText[MODBUS_LOG_TEXT_LEN];
					sprintf(szLogText, "Client %s is accepted", szClientIP);
					TRACE_MODBUS_TIPS(szLogText);
				}
#endif 
	
			}

			break;

		default:  //do nothing
			break;
		}

		/* clear InputQueue */
		Modbus_ClearEventQueue(pThis->hEventInputQueue, FALSE);

		/* communication begin */
		#ifdef _DEBUG_MODBUS_LINKLAYER
		TRACE("RunAsServer->DataExchange");
		#endif //_DEBUG_MODBUS_LINKLAYER

		DataExchange(pThis);
		pThis->bCommRunning = FALSE;
		/* communication end */

		/* clear OutputQueue */
		Modbus_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

		pEvent = &g_ModbusDiscEvent;

		/* send it */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
		if (iRet != ERR_OK)
		{
			LOG_MODBUS_LM_E("Put event to the Input Queue failed");
			/*DELETE(pEvent);*/
		}
		/* close Accept Comm Handle */
		CommClose(hDataComm);
	}

	/* close Open Comm Handle */
	TRACE("\n Run Serial_CommClose3!!\n");
	CommClose(hOpenComm);

	return;
}


/*==========================================================================*
 * FUNCTION : MODBUS_LinkLayerManager
 * PURPOSE  : Linklayer thread entry function
 * CALLS    : RunAsServer
 *			  RunAsClient
 * CALLED BY: ServiceMain
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-11 16:02
 *==========================================================================*/
int Modbus_LinkLayerManager(MODBUS_BASIC_ARGS *pThis)
{
	int iCommPortID;
	PORT_INFO *pCommPort = NULL;
	int iMediaType;

	/* to get comm port info through DixGetData interface */
	int iError, iBufLen, iTryCount;

	HANDLE hLocalThread;

	/* get Local thread id */	
	hLocalThread = RunThread_GetId(NULL);
	/* assign the reference in the Thread to insure valid */
	pThis->hThreadID[1] = hLocalThread;
	/* feed watch dog */
	RunThread_Heartbeat(hLocalThread);

	/* get current Comm Port ID */
	if (g_ModbusGlobals.CommonConfig.iMediaType == MODBUS_MEDIA_TYPE_TCPIP)
	{
		/* for net data port, port id is 3 */
		iCommPortID = 2;
	}
	else  if (g_ModbusGlobals.CommonConfig.iMediaType == MODBUS_MEDIA_TYPE_LEASED_LINE) /* Modem or Leased Line */
	{
		iCommPortID = 3;
	}
	else 
	{
		iCommPortID = 8;
	}

	/* get Comm Port config Info by port id */
	for (iTryCount = 0; iTryCount < 3; iTryCount++)
	{
		iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_PORT_INFO,			
			iCommPortID,		
			&iBufLen,			
			&pCommPort,
			0);

		if (iError != ERR_DXI_OK) 
		{
			if (iError == ERR_DXI_TIME_OUT)
			{
				/* try 3 times */
				Sleep(1000);//yield
				continue;
			}
			else
			{
				LOG_MODBUS_LM_E_EX("Get Comm Port config info failed", iError);

				/* notify MODBUS service to exit */
				pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
				return SERVICE_EXIT_FAIL;
			}
		}
		else
		{
			break;
		}
	}
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread Modbus Linker Service was created, Thread Id = %d.\n", gettid());
#endif
	/* begin main loop */
	iMediaType = g_ModbusGlobals.CommonConfig.iMediaType;
	while (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);
		TRACE_MODBUS_TIPS("Enter the Server Mode");
		RunAsServer(pThis, pCommPort);
		LeaveCommUsing();
		TRACE_MODBUS_TIPS("Exit the server mode");
		
	}

	TRACE_MODBUS_TIPS("Exit the Link-layer sub-thread");

	/* notify MODBUS service to exit(only for abnormal case) */
	if (pThis->iLinkLayerThreadExitCmd != SERVICE_EXIT_OK)
	{
		TRACE("\nExit code: %d\n", pThis->iLinkLayerThreadExitCmd);
		pThis->iInnerQuitCmd = pThis->iLinkLayerThreadExitCmd;
	}

	return pThis->iLinkLayerThreadExitCmd;
	
}


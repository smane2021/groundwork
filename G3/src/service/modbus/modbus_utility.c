/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : modbus_utility.c
 *  CREATOR  :                    DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "modbus.h"

#if 0
#define TRACE_MODBUS_CFG_EXT printf
#define TRACE_MODBUS_CFG_EXT2 printf
#else
#define TRACE_MODBUS_CFG_EXT
#define TRACE_MODBUS_CFG_EXT2
#endif

/*==========================================================================*
 * FUNCTION : Modbus_GetADR
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BYTE : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-02 14:43
 *==========================================================================*/
__INLINE BYTE Modbus_GetADR(void)
{
	MODBUS_COMMON_CONFIG *pCommonCfg;

	pCommonCfg = &g_ModbusGlobals.CommonConfig;

	return pCommonCfg->byADR;
}


/*==========================================================================*
 * FUNCTION : Modbus_CFG_CheckBase
 * PURPOSE  : basic function for check a null-ended string, used by other 
 *            specifically realized check functions
 * CALLS    : 
 * CALLED BY: specifically realized check functions, such as MODBUS_CheckPhoneNumber
 * ARGUMENTS: const char  *szObject : 
 *            LEGAL_CHR   fnCmp     : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 11:18
 *==========================================================================*/
BOOL Modbus_CFG_CheckBase(const char *szObject, LEGAL_CHR fnCmp)
{
	const char *p;

	if (szObject == NULL)
	{

		return FALSE;
	}

	p = szObject;
	for (; p != NULL; p++)
	{
		if (*p == '\0')
		{

			return TRUE;
		}

		if (!fnCmp(*p))
		{
			return FALSE;
		}

	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : Modbus_CheckIPAddress
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szIPAddress : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 20:49
 *==========================================================================*/
static BOOL IsIPAddress(char c)
{

	return  ((c >= '0' && c <= '9') ||
		c == '.' || c == ':');
}

BOOL Modbus_CheckIPAddress(const char *szIPAddress)
{
	return Modbus_CFG_CheckBase(szIPAddress, IsIPAddress);
}


static BOOL IsBautrateCfg(char c)
{
    return  ((c >= '0' && c <= '9') ||
	c == ',' || c == 'n' || c == 'N');
}
/*==========================================================================*
 * FUNCTION : Modbus_CheckBautrateCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szBautrateCfg : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-18 21:49
 *==========================================================================*/

BOOL Modbus_CheckBautrateCfg(const char *szBautrateCfg)
{
	int i, iLen, count = 0;

	if (!Modbus_CFG_CheckBase(szBautrateCfg, IsBautrateCfg))
	{

		return FALSE;
	}

	iLen = (int)strlen(szBautrateCfg);
	for (i = 0; i < iLen; i++)
	{
		if (szBautrateCfg[i] == ',')
		{
			count++;
		}
	}

	if (count != 3)
	{
		return FALSE;
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : Modbus_AsciiHexToChar
 * PURPOSE  : get value from ascii hex. eg: "1A" = 1*16+10 =26
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : HanTao                   DATE: 2006-06-12 19:24
 *==========================================================================*/
BYTE Modbus_AsciiHexToChar(IN const unsigned char *pStr)
{
	BYTE value;
	unsigned char c;

	ASSERT(pStr);

	c = pStr[0];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = c * 0x10;

	c = pStr[1];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value += c;

	return value;
}


/*==========================================================================*
 * FUNCTION : MODBUS_GetUnprintableChr
 * PURPOSE  : for trace
 * CALLS    : 
 * CALLED BY: MODBUS_PrintEvent
 * ARGUMENTS: IN unsigned char  chr   : 
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-25 14:30
 *==========================================================================*/
unsigned char *MODBUS_GetUnprintableChr(IN unsigned char chr)
{
	switch (chr)
	{

	case 0x06:
		return "<ACK>";

	case 0x15:
		return "<NAK>";

	case 0x04:
		return "<EOT>";

	case 0x05:
		return "<ENQ>";

	case 0x01:
		return "<SOH>";

	case 0x02:
		return "<STX>";

	case 0x03:
		return "<ETX>";

	case 0x0D:
		return "<EOI>";

	default:
		return "<NOT EXIST>";
	}
}


/*==========================================================================*
 * FUNCTION : MODBUS_PrintEvent
 * PURPOSE  : print MODBUS Event info(used for debug)
 * CALLS    : MODBUS_GetUnprintableChr
 * CALLED BY: 
 * ARGUMENTS: MODBUS_EVENT  *pEvent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-24 10:33
 *==========================================================================*/
void Modbus_PrintEvent(MODBUS_EVENT *pEvent)
{
#ifdef _SHOW_MODBUS_EVENT_INFO
	int i;
	unsigned char chr;
	char szEventType[50];

	switch (pEvent->iEventType)
	{
	case MODBUS_FRAME_EVENT:
		strncpy(szEventType, "MODBUS_FRAME_EVENT", 50);
		break;

	case MODBUS_CONNECTED_EVENT:
		strncpy(szEventType, "MODBUS_CONNECTED_EVENT", 50);
		break;

	case MODBUS_CONNECT_FAILED_EVENT:
		strncpy(szEventType, "MODBUS_CONNECT_FAILED_EVENT", 50);
		break;

	case MODBUS_DISCONNECTED_EVENT:
		strncpy(szEventType, "MODBUS_DISCONNECTED_EVENT", 50);
		break;

	case MODBUS_TIMEOUT_EVENT:
		strncpy(szEventType, "MODBUS_TIMEOUT_EVENT", 50);
		break;

	default:
		strncpy(szEventType, "ERROR: not defined event type!", 50);
	}

	TRACE("MODBUS Event Info\n");
	TRACE("Event Type: %s\n", szEventType);
	TRACE("Event Data length: %d\n", pEvent->iDataLength);

	TRACE("Event Data(Hex format):");
	for (i = 0; i < pEvent->iDataLength; i++)
	{
		TRACE("%02X ", pEvent->sData[i]);
	}
	TRACE("\n");

	TRACE("Event Data(Text format):");
	for (i = 0; i < pEvent->iDataLength; i++)
	{
		chr = pEvent->sData[i];
		if (chr < 0x21 || chr > 0X7E)
		{
			TRACE("%s", MODBUS_GetUnprintableChr(chr));
		}
		else
		{
			TRACE("%c", chr);
		}
	}
	TRACE("\n");
		
	TRACE("Event Flags: \nSkip Falg: %d	Dummy Flag: %d\n", 
		pEvent->bSkipFlag, pEvent->bDummyFlag);

#endif 
    UNUSED(pEvent);
	return;
}


/*==========================================================================*
 * FUNCTION : Modbus_PrintState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iState : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-27 09:45
 *==========================================================================*/
void Modbus_PrintState(int iState)
{
#ifdef _SHOW_MODBUS_CUR_STATE
	char *szStateName;

	if (g_ModbusGlobals.CommonConfig.iProtocolType == MODBUS)  //MODBUS State machine
	{
		switch (iState)
		{
		case MODBUS_IDLE:
			szStateName = "MODBUS_IDLE";
			break;
		case MODBUS_STATE_MACHINE_QUIT:
			szStateName = "STATE_MACHINE_QUIT";
			break;

		default:
			szStateName = "Error MODBUS State Name";
		}
	}

	TRACE("++++++++++++++++++++++++++++++++++++++++++++++\n");
	TRACE("+  Current State: %-25s  +\n", szStateName);
	TRACE("++++++++++++++++++++++++++++++++++++++++++++++\n");
	
	return;
#endif 

	UNUSED(iState);
	return;
}





/*==========================================================================*
 * FUNCTION : Modbus_ClearEventQueue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hq       : 
 *            BOOL    bDestroy : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-21 15:11
 *==========================================================================*/
void Modbus_ClearEventQueue(HANDLE hq, BOOL bDestroy)
{
	int i, iCount, iRet;
	MODBUS_EVENT *pEvent;

	if (hq)
	{
		iCount = Queue_GetCount(hq, NULL);

		/* clear memory */
		for (i = 0; i < iCount; i++)
		{
			iRet = Queue_Get(hq, &pEvent, FALSE, 1000);

			if (iRet == ERR_OK)
			{
				DELETE_MODBUS_EVENT(pEvent);
			}
		}

		/* destroy queue */
		if (bDestroy)
		{
			Queue_Destroy(hq);
		}
	}
}



/*==========================================================================*
 * FUNCTION : PackReps
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : char * : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-8 10:43
 *==========================================================================*/
void PackReps(unsigned char *pStr)
{
	int nLen;	
	unsigned char *pnStr = "0"; 

	nLen = strlen(pnStr);
	
	memcpy(pStr,pnStr, (size_t)nLen);
}


/*==========================================================================*
 * FUNCTION : GetEquipID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iEquipTypeID : 
 * RETURN   : pCurEquip->iEquipID : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-19 11:29
 *==========================================================================*/
int Modbus_GetEquipIDOrder(int iEquipTypeID)
{
	int i, iEquipID;
	EQUIP_INFO *pCurEquip;
	pCurEquip = g_ModbusGlobals.pEquipInfo;

	for (i = 0; i <  g_ModbusGlobals.iEquipNum; i++, pCurEquip++)
	{
		if(iEquipTypeID == g_ModbusGlobals.iEquipIDOrder[i][1])
		{
		    iEquipID = pCurEquip->iEquipID;
			break;
		}
	}

	/* not found */
	if (i == g_ModbusGlobals.iEquipNum)
	{
		i = 0;
		//TRACE("\n i is: %d\n", i);
	}

	return i;
}

/*==========================================================================*
* FUNCTION : GetEquipID
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iEquipTypeID : 
* RETURN   : pCurEquip->iEquipID : 
* COMMENTS : 
* CREATOR  : HanTao                   DATE: 2006-05-19 11:29
*==========================================================================*/
int GetEquipIDOrderwithstartposition(int iEquipTypeID, int startposition)
{
	int i;
	
	for (i = startposition; i < g_ModbusGlobals.iEquipNum; i++)
	{
		if(((g_ModbusGlobals.iEquipIDOrder[i][1]/iEquipTypeID)==1)
		    &&((g_ModbusGlobals.iEquipIDOrder[i][1]%iEquipTypeID)<90))
		{
			break;
		}
	}

	/* not found */
	if (i == g_ModbusGlobals.iEquipNum)
	{
		i = MODBUS_MAX_EQUIP_NUM;
	}

	return i;
}

int GetEquipIDOrderwithstartpositionByRange(int iMinEquipTypeID, int iMaxEquipTypeID, int startposition)
{
	int i;

	for (i = startposition; i < g_ModbusGlobals.iEquipNum; i++)
	{
		if((g_ModbusGlobals.iEquipIDOrder[i][1] >= iMinEquipTypeID)
			&& (g_ModbusGlobals.iEquipIDOrder[i][1] <= iMaxEquipTypeID))
		{
			break;
		}
	}

	/* not found */
	if (i == g_ModbusGlobals.iEquipNum)
	{
		i = MODBUS_MAX_EQUIP_NUM;
	}

	return i;
}



void Modbus_AlarmData(WORD *szOutData,int iposition , MODBUS_DataValue Alarmvalue)
{
	if(iposition >=5)
	{
		return;
	}
	if((*szOutData)==MODBUS_INVALIDDATA)
	{
		(*szOutData) = 0;
	}

	if(iposition >2)
	{
		if(Alarmvalue.VData.ctemp[0] == 0)
		{
			*szOutData =  (*szOutData) &(~(0x0001<<(iposition-3+12)));
		}
		else
		{
			*szOutData =  (*szOutData) |(0x0001<<(iposition-3+12));
		}
		
	}
	else
	{
		if(Alarmvalue.VData.ctemp[0] == 0)
		{
			*szOutData =  (*szOutData) &(~(0x0001<<(iposition*4)));
		}
		else
		{
			*szOutData =  (*szOutData) |(0x0001<<(iposition*4));
		}
		if(Alarmvalue.VData.ctemp[1] == 0)
		{
			*szOutData =  (*szOutData) & (~(0x0006<<(iposition*4)));
		}
		else if(Alarmvalue.VData.ctemp[1] == 1)
		{
			*szOutData = (*szOutData) | (0x0002<<(iposition*4));
		}
		else if(Alarmvalue.VData.ctemp[1] == 2)
		{
			*szOutData = (*szOutData) | (0x0004<<(iposition*4));
		}
		else
		{
			*szOutData = (*szOutData) | (0x0006<<(iposition*4));
		}
	}	
}

static void ModbusDebug_StoreDataToFile(const char *pszFilePath, void *pData, int iLen)
{
	int fd, iTemp;
	
	fd = open(pszFilePath, O_CREAT | O_RDWR);
	if(fd > 0)
	{
		iTemp = write(fd, pData, iLen);
		if(iTemp != iLen)
		{
			printf("write /var/modbus.txt error!!!\n");
		}
		close(fd);
	}
}


static void Modbus_RefreshDataForConfigVersion2(void)
{
	int							i, j, k, iEquipIDOrder, iEquipID, iTemp;
	int							iMaxEquipNum, iEquipCount;
	WORD						*szOutData;
	MODBUS_MAPENTRIES_INFO		*pCurEntry;
	HANDLE						hLocalThread;
	BOOL						bDxiRTN;
	MODBUS_DataValue			DataValue;
	MODBUSMODEL_CONFIG_INFO		*pstModelCfg = &g_ModbusGlobals.ModbusModelConfig;
	MODBUS_EXT_TYPE_MAP_INFO	*pCurExtTypeMapInfo = NULL;
	int							iRegAddr, iFirstRegAddr;
	int							iEntriesItemIndex;
	int							iSCUPSigType, iSCUPSigID;

	if(pstModelCfg->iExtMaxRegNum <= 0)
	{
		return ;
	}

	TRACE_MODBUS_CFG_EXT("Modbus_RefreshDataForConfigVersion2 start\n");


	hLocalThread = RunThread_GetId(NULL);

	//1. init to invalid
	//register data is stored in pszExtRespDataX, from 1 to N, the first data is in [1], not [0]
	memset(g_ModbusRespData.pszExtRespData2, MODBUS_INVALIDDATA, sizeof(WORD)*(pstModelCfg->iExtMaxRegNum + 1));

	//2. packet data
	//process each item in [EXT_TYPE_MAP_INFO]
	for(i = 0; i < pstModelCfg->iExtTypeMapNum; i++)
	{
		pCurExtTypeMapInfo = &pstModelCfg->pExtTypeMapInfo[i];

		TRACE_MODBUS_CFG_EXT("Modbus_RefreshDataForConfigVersion2 [%d], uiSpecProcFlag=%X\n",
							i, pCurExtTypeMapInfo->uiSpecProcFlag);
		
		iEquipIDOrder = -1;
		
		//2.1 skip "7.Sequence Number of First Equip" to get the target equipment iEquipIDOrder
		if( pCurExtTypeMapInfo->iSequenceNum1stEquip > 0 )
		{
			//the first equipment iSequenceNum1stEquip is 1
			for(j = 1; j < pCurExtTypeMapInfo->iSequenceNum1stEquip; j++)
			{
				iEquipIDOrder = GetEquipIDOrderwithstartpositionByRange(
					pCurExtTypeMapInfo->iEquipTypeStart,
					pCurExtTypeMapInfo->iEquipTypeEnd,
					iEquipIDOrder + 1);
					
				if(MODBUS_MAX_EQUIP_NUM == iEquipIDOrder)
				{
					break;
				}
			}

			if(MODBUS_MAX_EQUIP_NUM == iEquipIDOrder)//can't find the equipment
			{
				continue;
			}
		}

		iMaxEquipNum = (pCurExtTypeMapInfo->iRegEnd - pCurExtTypeMapInfo->iRegStart + 1)/pCurExtTypeMapInfo->iMaxRegCountPerEquip;

		//2.2 process each equipment of one item of [EXT_TYPE_MAP_INFO]
		for(iEquipCount = 0; iEquipCount < iMaxEquipNum;)
		{
			iEquipIDOrder = GetEquipIDOrderwithstartpositionByRange(
				pCurExtTypeMapInfo->iEquipTypeStart,
				pCurExtTypeMapInfo->iEquipTypeEnd,
				iEquipIDOrder + 1);

			if(MODBUS_MAX_EQUIP_NUM == iEquipIDOrder)
			{
				TRACE_MODBUS_CFG_EXT("Modbus_RefreshDataForConfigVersion2 [%d], "
									"iEquipCount(%d)/iMaxEquipNum(%d), no more equipments\n",
									i,
									iEquipCount,
									iMaxEquipNum);
				break;
			}

			iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
			iFirstRegAddr = pCurExtTypeMapInfo->iRegStart + iEquipCount*pCurExtTypeMapInfo->iMaxRegCountPerEquip;

			if(MODBUS_SPEC_PROC_FLAG_1_SKIP_NONEXIST_EQUIP & pCurExtTypeMapInfo->uiSpecProcFlag)
			{
				//check equipment's exist status
				bDxiRTN = ModbusGetSCUPSingleSigValues(SIG_TYPE_SAMPLING, 100, iEquipID, &DataValue);
				if(( bDxiRTN == FALSE)||(DataValue.VData.itemp == 1))
				{
					if( ( (iEquipID >= 1801) && (iEquipID <= 1808)  ) 
					     || ( (iEquipID >= 643) && (iEquipID <= 650)  ) 
					  	 || ( (iEquipID >= 1870) && (iEquipID <= 1881)  )  )
					{
						//printf("Not exit: iEquipID=%d,iEquipCount=%d,iMaxEquipNum=%d\n",iEquipID,iEquipCount,iMaxEquipNum);
						iEquipCount++;
					}
					continue;
				}
			}
			
			TRACE_MODBUS_CFG_EXT("Modbus_RefreshDataForConfigVersion2 [%d], iEquipID=%d, iFirstRegAddr=%d, iEquipCount=%d\n",
								i, iEquipID, iFirstRegAddr, iEquipCount);
			//printf("Modbus_RefreshDataForConfigVersion2 [%d], iEquipID=%d, iFirstRegAddr=%d, iEquipCount=%d,iEquipIDOrder=%d\n",i, iEquipID, iFirstRegAddr, iEquipCount,iEquipIDOrder);
			
			//process each item of [EXT_MAPENTRIES_INFO_MAPXXX]
			for(iEntriesItemIndex = 0;
				(iEntriesItemIndex < pCurExtTypeMapInfo->iMapEntriesNum) && (iEntriesItemIndex < pCurExtTypeMapInfo->iMaxRegCountPerEquip);
				iEntriesItemIndex++)
			{
				pCurEntry = &pCurExtTypeMapInfo->pMapEntriesInfo[iEntriesItemIndex];
				iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];

				iRegAddr = iFirstRegAddr + pCurEntry->iModbusPort;

				//printf("iRegAddr=%d,iFirstRegAddr=%d,pCurEntry->iModbusPort=%d,iEquipCount=%d~~~\n",iRegAddr,iFirstRegAddr,pCurEntry->iModbusPort,iEquipCount);
				
				if((iRegAddr < pCurExtTypeMapInfo->iRegStart)
					|| (iRegAddr > pCurExtTypeMapInfo->iRegEnd))
				{
					TRACE_MODBUS_CFG_EXT("Modbus_RefreshDataForConfigVersion2 [%d][%d] invalid,iRegAddr=%d\n",
											i,
											iEntriesItemIndex,
											iRegAddr);						
					continue;
				}
				szOutData =&(g_ModbusRespData.pszExtRespData2[iRegAddr]);

				//if iEquipType is exist, use it to find new iEquipID
				//if iEquipType is exist, use it to find new iEquipID
				if((pCurEntry->iEquipType > 0)
					&& ((pCurEntry->iEquipType < pCurExtTypeMapInfo->iEquipTypeStart) || (pCurEntry->iEquipType > pCurExtTypeMapInfo->iEquipTypeEnd)))
				{
					TRACE_MODBUS_CFG_EXT("Modbus_RefreshDataForConfigVersion2 [%d][%d] iEquipType=%d\n",
						i,
						iEntriesItemIndex,
						pCurEntry->iEquipType);
						
					iTemp = GetEquipIDOrderwithstartpositionByRange(
						pCurEntry->iEquipType,
						pCurEntry->iEquipType,
						0);
					if(MODBUS_MAX_EQUIP_NUM == iTemp)
					{
						printf("Modbus_RefreshData::pCurExtTypeMapInfo[%d] pCurExtTypeMapInfo[%d] iEquipType=%d is invalid\n",
								i,
								iEntriesItemIndex,
								pCurEntry->iEquipType);
						continue;
					}
					
					iEquipID = g_ModbusGlobals.iEquipIDOrder[iTemp][0];

					TRACE_MODBUS_CFG_EXT("Modbus_RefreshDataForConfigVersion2 [%d][%d] iEquipType=%d find iEquipID=%d\n",
						i,
						iEntriesItemIndex,
						pCurEntry->iEquipType,
						iEquipID);
				}

				if(pCurEntry->iSCUPSigType == SIG_TYPE_ALARM)
				{
					for(k= 0;k<5;k++)
					{
						if(pCurEntry->iStatusValue[k] != -1)
						{

							bDxiRTN=ModbusGetSCUPSingleSigValues(pCurEntry->iSCUPSigType, pCurEntry->iStatusValue[k], iEquipID, &DataValue);
							if(bDxiRTN == FALSE)
							{
								*szOutData = MODBUS_INVALIDDATA;
								break;
							}
							Modbus_AlarmData(szOutData, k, DataValue);

						}
						else
						{
							DataValue.VData.ctemp[0] = 0;
							DataValue.VData.ctemp[1] = 0;
							Modbus_AlarmData(szOutData, k, DataValue);
						}
					}
				}
				else if((pCurEntry->iSCUPSigType == SIG_TYPE_SAMPLING)
					||(pCurEntry->iSCUPSigType == SIG_TYPE_SETTING)
					||(pCurEntry->iSCUPSigType == SIG_TYPE_CONTROL))
				{
					iSCUPSigType = pCurEntry->iSCUPSigType;
					iSCUPSigID = pCurEntry->iSCUPSigID;
					
					//special process
					if(MODBUS_SPEC_PROC_FLAG_3_COMPATIBLE_CONV_SIG_REG_51_650 & pCurExtTypeMapInfo->uiSpecProcFlag)
					{
						//for keeping the same processing as MODBUS_CONFIG_VERSION_1
						//special process for SMDUXBattery X
						if( (( iEquipID>=126) && ( iEquipID<=157))
							|| (( iEquipID>=659) && ( iEquipID<=666)) )
						{
							if( (iSCUPSigType == SIG_TYPE_SAMPLING) && (iSCUPSigID ==1) )
							{
								iSCUPSigID = 51;//replace (0, 1) to (0, 51)
							}
						}
						//special process for EIBXBattery X
						else if( (( iEquipID>=118)&&( iEquipID<=125))
							|| (( iEquipID>=206)&&( iEquipID<=209)) )
						{
							if( (iSCUPSigType == SIG_TYPE_SAMPLING) && (iSCUPSigID ==1) )
							{
								iSCUPSigID = -1;//replace (0, 1) to invalid
							}
						}
					}

					if(iSCUPSigID < 0)
					{
						*szOutData = MODBUS_INVALIDDATA;
						continue;
					}
					
					//get signal value
					bDxiRTN = ModbusGetSCUPSingleSigValues(iSCUPSigType, iSCUPSigID, iEquipID, &DataValue);
					if( bDxiRTN == FALSE)
					{
						*szOutData = MODBUS_INVALIDDATA;
						continue;
					}

					if(DataValue.iDataType != VAR_FLOAT)
					{
						//not float
						if(pCurEntry->iFlag0 == MODBUS_TWOREG_FOR_ONEDATA_FLAG )
						{
							*szOutData = (WORD)((int)(DataValue.VData.itemp*pCurEntry->iFlag1 + pCurEntry->iFlag0)/MODBUS_DIVISOR);
							szOutData =&(g_ModbusRespData.pszExtRespData2[iRegAddr + 1]);
							*szOutData = (WORD)((int)(DataValue.VData.itemp*pCurEntry->iFlag1 + pCurEntry->iFlag0)%MODBUS_DIVISOR);

							iEntriesItemIndex++;
						}
						else
						{
							*szOutData = (WORD)(DataValue.VData.itemp*pCurEntry->iFlag1 + pCurEntry->iFlag0);
						}
					}
					else
					{
						//float

						if(pCurEntry->iFlag0 == MODBUS_TWOREG_FOR_ONEDATA_FLAG )
						{
							*szOutData = (WORD)((int)(DataValue.VData.ftemp*pCurEntry->iFlag1 + pCurEntry->iFlag0 + 0.5)/MODBUS_DIVISOR);
							szOutData =&(g_ModbusRespData.pszExtRespData2[iRegAddr + 1]);
							*szOutData = (WORD)((int)(DataValue.VData.ftemp*pCurEntry->iFlag1 + pCurEntry->iFlag0 + 0.5)%MODBUS_DIVISOR);
							
							iEntriesItemIndex++;
						}
						else
						{
							*szOutData = (WORD)(DataValue.VData.ftemp*pCurEntry->iFlag1 + pCurEntry->iFlag0 + 0.5);
						}
					}
					
					
					//special process
					if(MODBUS_SPEC_PROC_FLAG_2_COMPATIBLE_LIMIT_TEMP_REG_1_50 & pCurExtTypeMapInfo->uiSpecProcFlag)
					{
						//for keeping the same processing as MODBUS_CONFIG_VERSION_1
						//Ambient Temperature and Comp Temp
						if( ((1 == iEquipID) && (SIG_TYPE_SAMPLING == iSCUPSigType ) && (69 == iSCUPSigID))
							|| ((115 == iEquipID) && (SIG_TYPE_SAMPLING == iSCUPSigType ) && (36 == iSCUPSigID)) )
						{
							if(DataValue.VData.ftemp < -250 )
							{
								*szOutData = MODBUS_INVALIDDATA;
							}
						}
					}
				}
			}//process each item of [EXT_MAPENTRIES_INFO_MAPXXX]

			if ( 0 == ( iEquipCount % MODBUS_REFRESHDATA_NUM ) )
			{
				RunThread_Heartbeat(hLocalThread);
				Sleep(MODBUS_REFRESHDATA_DELAY);//yield
			}

			iEquipCount++;
			
		}//2.2 process each equipment of one item of [EXT_TYPE_MAP_INFO]
	}//process each item in [EXT_TYPE_MAP_INFO]

	// 3.make data take effect
	//register data is stored in pszExtRespDataX, from 1 to N, the first data is in [1], not [0]
	memcpy(g_ModbusRespData.pszExtRespData1, g_ModbusRespData.pszExtRespData2, sizeof(WORD)*(pstModelCfg->iExtMaxRegNum + 1));

	//ModbusDebug_StoreDataToFile("/var/modbus_ext_reg_map",
	//							g_ModbusRespData.pszExtRespData1,
	//							sizeof(WORD)*(pstModelCfg->iExtMaxRegNum + 1));

	TRACE_MODBUS_CFG_EXT("Modbus_RefreshDataForConfigVersion2 end\n");
}



/*==========================================================================*
* FUNCTION : PackRACA
* PURPOSE  : Package AC distribution analog data 
* CALLS    : 
*			  
* CALLED BY: 
* ARGUMENTS: MODBUS_BASIC_ARGS  *pThis : 
* RETURN   : int : THREAD_EXIT_CODE
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static void Modbus_RefreshData(void)
{
    int i,j,k,signalnum,iEquipIDOrder,iEquipID,ibattnum,irectnum,interval;
    int iMaxEquipNum;
    WORD *szOutData;
    MODBUS_MAPENTRIES_INFO *pCurEntry;
    MODBUS_MAPENTRIES_INFO *pCurEntrytemp;
    HANDLE hLocalThread;
    BOOL AnalogyorDigital,bDxiRTN;
    MODBUS_DataValue DataValue;
    MODBUSMODEL_CONFIG_INFO		*pstModelCfg = &g_ModbusGlobals.ModbusModelConfig;


	hLocalThread = RunThread_GetId(NULL);
	
	
	//Frank Wu,20151112, for extending configuration file
	//1. check configure file version
	//if "ModbusModelMap.cfg" uses extensive segment [EXT_TYPE_MAP_INFO],
	//we use [EXT_TYPE_MAP_INFO] and [EXT_MAPENTRIES_INFO_MAPXXX] to replace [MAPENTRIES_INFO_MAP].
	//It means the [MAPENTRIES_INFO_MAP] is invalid.
	if(MODBUS_CONFIG_VERSION_2 == pstModelCfg->iExtCurrentCfgVersion)
	{
		Modbus_RefreshDataForConfigVersion2();
		return;
	}


	memset(g_ModbusRespData.szRespData2, MODBUS_INVALIDDATA,sizeof(WORD)*MAX_MODBUS_SIGNAL_NUM);

	szOutData = g_ModbusRespData.szRespData2;
	signalnum = g_ModbusGlobals.ModbusModelConfig.iMapEntriesNum;
	pCurEntry = g_ModbusGlobals.ModbusModelConfig.pMapEntriesInfo;

	iEquipIDOrder = 0;
	ibattnum = 0;
	irectnum = 0;

	for(i=0;i<signalnum;)
	{
	   if(((pCurEntry->iModbusPort>=MODBUS_SYS_REGISTERSTART)&&(pCurEntry->iModbusPort<=MODBUS_SYS_REGISTEREND))||
	      ((pCurEntry->iModbusPort>=MODBUS_BattGroup_REGISTERSTART)&&(pCurEntry->iModbusPort<=MODBUS_BattGroup_REGISTEREND))||
	      ((pCurEntry->iModbusPort>=MODBUS_RectGroup_REGISTERSTART)&&(pCurEntry->iModbusPort<=MODBUS_RectGroup_REGISTEREND))||
	      ((pCurEntry->iModbusPort>=MODBUS_DC_REGISTERSTART)&&(pCurEntry->iModbusPort<=MODBUS_DC_REGISTEREND))||
	      ((pCurEntry->iModbusPort>=MODBUS_AC_REGISTERSTART)&&(pCurEntry->iModbusPort<=MODBUS_AC_REGISTEREND)))
	   {


		szOutData =&(g_ModbusRespData.szRespData2[pCurEntry->iModbusPort]);
		iEquipIDOrder = Modbus_GetEquipIDOrder(pCurEntry->iEquipType);

		iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];

		if(pCurEntry->iSCUPSigType == SIG_TYPE_ALARM)
		{

			for(k= 0;k<5;k++)
			{
				if(pCurEntry->iStatusValue[k] != -1)
				{

					bDxiRTN=ModbusGetSCUPSingleSigValues(pCurEntry->iSCUPSigType, pCurEntry->iStatusValue[k], iEquipID, &DataValue);
					if(bDxiRTN == FALSE)
					{
						*szOutData = MODBUS_INVALIDDATA;
						break;
					}
					Modbus_AlarmData(szOutData,k,DataValue);

				}
				else
				{
					DataValue.VData.ctemp[0] = 0;
					DataValue.VData.ctemp[1] = 0;
					Modbus_AlarmData(szOutData,k,DataValue);
				}
			}
		}
		else 
		{
			bDxiRTN= ModbusGetSCUPSingleSigValues(pCurEntry->iSCUPSigType, pCurEntry->iSCUPSigID, iEquipID, &DataValue);

			if( bDxiRTN == FALSE)
			{
				*szOutData = MODBUS_INVALIDDATA;
			}
			else
			{
				if(DataValue.iDataType != VAR_FLOAT)
				{
					if((pCurEntry->iModbusPort ==MODBUS_SYSTEMP_REGISTER)
						||(pCurEntry->iModbusPort ==MODBUS_BATTGROUPTEMP_REGISTER))
					{
						if(DataValue.VData.itemp < -250 )
						{
							*szOutData = MODBUS_INVALIDDATA;	
						}
						else
						{
							*szOutData = (WORD)DataValue.VData.itemp;
						}
					}
					else
					{	
						*szOutData = (WORD)DataValue.VData.itemp;
					}
				}
				else
				{	
					if((pCurEntry->iModbusPort ==MODBUS_SYSTEMP_REGISTER)
						||(pCurEntry->iModbusPort ==MODBUS_BATTGROUPTEMP_REGISTER))
					{
						if(DataValue.VData.ftemp < -250 )
						{
							*szOutData = MODBUS_INVALIDDATA;	
						}
						else
						{
							*szOutData = (WORD)(DataValue.VData.ftemp *pCurEntry->iFlag1+pCurEntry->iFlag0 +0.5);
						}
					}
					else if(pCurEntry->iFlag0 == MODBUS_TWOREG_FOR_ONEDATA_FLAG )
					{

						*szOutData = (WORD)((int)(DataValue.VData.ftemp *pCurEntry->iFlag1+pCurEntry->iFlag0+0.5)/MODBUS_DIVISOR);

						pCurEntry++;
						i++;
						szOutData =&(g_ModbusRespData.szRespData2[pCurEntry->iModbusPort]);
						*szOutData = (WORD)((int)(DataValue.VData.ftemp *pCurEntry->iFlag1+pCurEntry->iFlag0+0.5)%MODBUS_DIVISOR);

					}
					else
					{	
						*szOutData = (WORD)(DataValue.VData.ftemp *pCurEntry->iFlag1+pCurEntry->iFlag0+0.5);
					}	
				} 
			}
			
		}

		 pCurEntry++;
		 i++;

	   }
	   else if((pCurEntry->iModbusPort>=MODBUS_Batt_REGISTERSTART)&&(pCurEntry->iModbusPort<=MODBUS_Batt_REGISTEREND))
	   {

		szOutData = &(g_ModbusRespData.szRespData2[pCurEntry->iModbusPort]);
		iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,0 );
		interval = MODBUS_PERBatt_NUM ;

		while(iEquipIDOrder !=MODBUS_MAX_EQUIP_NUM)
		{
			iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];
			bDxiRTN = ModbusGetSCUPSingleSigValues(SIG_TYPE_SAMPLING,100 , iEquipID, &DataValue);
			if(( bDxiRTN == FALSE)||(DataValue.VData.itemp == 1))
			{
				iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
				continue;		
			}	

			if(ibattnum >= MODBUS_MAX_Batt_NUM )
			{
				break;
			}

			pCurEntrytemp = pCurEntry;
			for(j=0;j<interval;j++)
			{

				if((pCurEntrytemp->iSCUPSigType == SIG_TYPE_SAMPLING)||(pCurEntrytemp->iSCUPSigType == SIG_TYPE_SETTING)
				||(pCurEntrytemp->iSCUPSigType == SIG_TYPE_CONTROL))
				{
	

					if(pCurEntrytemp->iSCUPSigID == -1)
					{
						*(szOutData+(ibattnum *interval)+j) = MODBUS_INVALIDDATA;
						pCurEntrytemp++;
						continue;
					}
					if((( iEquipID>=126)&&( iEquipID<=157))||(( iEquipID>=659)&&( iEquipID<=666)))
					{
						if((pCurEntrytemp->iSCUPSigType == 0)&&(pCurEntrytemp->iSCUPSigID ==1))
						{
							bDxiRTN = ModbusGetSCUPSingleSigValues(pCurEntrytemp->iSCUPSigType, 51, iEquipID, &DataValue);
						}
						else
						{
							bDxiRTN = ModbusGetSCUPSingleSigValues(pCurEntrytemp->iSCUPSigType, pCurEntrytemp->iSCUPSigID, iEquipID, &DataValue);
						}
					}
					else if((( iEquipID>=118)&&( iEquipID<=125))||(( iEquipID>=206)&&( iEquipID<=209)))
					{
						if((pCurEntrytemp->iSCUPSigType == 0)&&(pCurEntrytemp->iSCUPSigID ==1))
						{
							bDxiRTN = FALSE;
						}
						else
						{
							bDxiRTN = ModbusGetSCUPSingleSigValues(pCurEntrytemp->iSCUPSigType, pCurEntrytemp->iSCUPSigID, iEquipID, &DataValue);
						}
					}
					else
					{
						bDxiRTN = ModbusGetSCUPSingleSigValues(pCurEntrytemp->iSCUPSigType, pCurEntrytemp->iSCUPSigID, iEquipID, &DataValue);
					}
					

					if( bDxiRTN == FALSE)
					{
						*(szOutData+(ibattnum *interval)+j) = MODBUS_INVALIDDATA;
					}
					else
					{


						if(DataValue.iDataType != VAR_FLOAT)
						{	
							*(szOutData+(ibattnum *interval)+j)= (WORD)DataValue.VData.itemp;
						}
						else if(pCurEntrytemp->iFlag0 == MODBUS_TWOREG_FOR_ONEDATA_FLAG )
						{
							*(szOutData+(ibattnum *interval)+j) = (WORD)((int)(DataValue.VData.ftemp *pCurEntrytemp->iFlag1+pCurEntrytemp->iFlag0+0.5)/MODBUS_DIVISOR);
							pCurEntrytemp++;
							j++;
							*(szOutData+(ibattnum *interval)+j) = (WORD)((int)(DataValue.VData.ftemp *pCurEntrytemp->iFlag1+pCurEntrytemp->iFlag0+0.5)%MODBUS_DIVISOR);

						}
						else
						{	
							*(szOutData+(ibattnum *interval)+j) = (WORD)(DataValue.VData.ftemp * pCurEntrytemp->iFlag1+pCurEntrytemp->iFlag0+0.5);
						}
					}
				}
				pCurEntrytemp++;
			}
			ibattnum++;
			iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
			
		}
		
		pCurEntry= pCurEntry+interval;
		i = i+interval;

	   }
	   else if(((pCurEntry->iModbusPort>=MODBUS_Rect_REGISTERSTART)&&(pCurEntry->iModbusPort<=MODBUS_Rect_REGISTEREND))
		   ||((pCurEntry->iModbusPort>=MODBUS_SLAVE1_Rect_REGSTART)&&(pCurEntry->iModbusPort<=MODBUS_SLAVE1_Rect_REGEND))
		   ||((pCurEntry->iModbusPort>=MODBUS_SLAVE2_Rect_REGSTART)&&(pCurEntry->iModbusPort<=MODBUS_SLAVE2_Rect_REGEND))
		   ||((pCurEntry->iModbusPort>=MODBUS_SLAVE3_Rect_REGSTART)&&(pCurEntry->iModbusPort<=MODBUS_SLAVE3_Rect_REGEND)))
	   {

		szOutData =&(g_ModbusRespData.szRespData2[pCurEntry->iModbusPort]);
		iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,0 );
		if((pCurEntry->iModbusPort>=MODBUS_Rect_REGISTERSTART)&&(pCurEntry->iModbusPort<=MODBUS_Rect_REGISTEREND))
		{	
			interval = MODBUS_PERRect_NUM ;
			iMaxEquipNum = MODBUS_MAX_RECT_NUM;
		}
		else
		{
			interval = MODBUS_SLAVE_PERRect_NUM ;
			iMaxEquipNum = MODBUS_MAX_SLAVE_RECT_NUM;
		}
		if((pCurEntry->iModbusPort==MODBUS_Rect_REGISTERSTART)
		    ||(pCurEntry->iModbusPort==MODBUS_SLAVE1_Rect_REGSTART)
		    ||(pCurEntry->iModbusPort==MODBUS_SLAVE2_Rect_REGSTART)
		    ||(pCurEntry->iModbusPort==MODBUS_SLAVE3_Rect_REGSTART))
		{
			irectnum =0;
		}
		

		while(iEquipIDOrder !=MODBUS_MAX_EQUIP_NUM)
		{
			iEquipID = g_ModbusGlobals.iEquipIDOrder[iEquipIDOrder][0];

			bDxiRTN = ModbusGetSCUPSingleSigValues(SIG_TYPE_SAMPLING,100 , iEquipID, &DataValue);

			if(( bDxiRTN == FALSE)||(DataValue.VData.itemp == 1))
			{
				iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
				continue;		
			}	

			if(irectnum >= iMaxEquipNum )
			{
				break;
			}
			pCurEntrytemp = pCurEntry;
			for(j=0;j<interval;j++)
			{
				
				if((pCurEntrytemp->iSCUPSigType == SIG_TYPE_SAMPLING)||(pCurEntrytemp->iSCUPSigType == SIG_TYPE_SETTING)
				    ||(pCurEntrytemp->iSCUPSigType == SIG_TYPE_CONTROL))
				{

					   bDxiRTN = ModbusGetSCUPSingleSigValues(pCurEntrytemp->iSCUPSigType, pCurEntrytemp->iSCUPSigID, iEquipID, &DataValue);

					   if( bDxiRTN == FALSE)
					   {
						   *(szOutData+(irectnum *interval)+j) = MODBUS_INVALIDDATA;
					   }
					   else
					   {
						   if(DataValue.iDataType != VAR_FLOAT)
						   {
							   *(szOutData+(irectnum *interval)+j)= (WORD)DataValue.VData.itemp;
						   }
						   else
						   {	

							   *(szOutData+(irectnum *interval)+j) = (WORD)(DataValue.VData.ftemp * pCurEntrytemp->iFlag1+pCurEntrytemp->iFlag0+0.5);

						   }
					   }

				}
				else if(pCurEntrytemp->iSCUPSigType == SIG_TYPE_ALARM)
				{
					for(k= 0;k<5;k++)
					{

						if(pCurEntrytemp->iStatusValue[k] != -1)
						 {
							   bDxiRTN=ModbusGetSCUPSingleSigValues(pCurEntrytemp->iSCUPSigType, pCurEntrytemp->iStatusValue[k], iEquipID, &DataValue);
							   if(bDxiRTN == FALSE)
							   {
								   *(szOutData+(irectnum *interval)+j) = MODBUS_INVALIDDATA;
								   break;
							   }
							   Modbus_AlarmData(szOutData+(irectnum *interval)+j,k,DataValue);
						}
						else
						{
							DataValue.VData.ctemp[0] = 0;
							DataValue.VData.ctemp[1] = 0;
							Modbus_AlarmData(szOutData+(irectnum *interval)+j,k,DataValue);
						}
					}
				}

				pCurEntrytemp++;
			}
			irectnum++ ;
			iEquipIDOrder = GetEquipIDOrderwithstartposition(pCurEntry->iEquipType,iEquipIDOrder+1 );
		
		}
		pCurEntry= pCurEntry+interval;
		i = i+interval;
	   }
	   if ((i%MODBUS_REFRESHDATA_NUM)==0)
	   {
		RunThread_Heartbeat(hLocalThread);
		Sleep(MODBUS_REFRESHDATA_DELAY);//yield
	   }

	}
	// 2.Package the DataFlag
	memcpy(g_ModbusRespData.szRespData1,g_ModbusRespData.szRespData2,(MODBUSREGISTEREND+1)*2);

	//ModbusDebug_StoreDataToFile("/var/modbus_reg_map",
	//							g_ModbusRespData.szRespData1,
	//							(MODBUSREGISTEREND+1)*2);

}

/*==========================================================================*
 * FUNCTION : Modbus_PackData
 * PURPOSE  : PackData thread entry function
 * CALLS    : 
 *			  
 * CALLED BY: ServiceMain
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis : 
 * RETURN   : int : THREAD_EXIT_CODE
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-11-09 16:02
 *==========================================================================*/
int Modbus_PackData(MODBUS_BASIC_ARGS *pThis)
{
	HANDLE hLocalThread;

	/* get Local thread id */	
	hLocalThread = RunThread_GetId(NULL);
	/* assign the reference in the Thread to insure valid */
	pThis->hThreadID[2] = hLocalThread;
	/* feed watch dog */
	RunThread_Heartbeat(hLocalThread);
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread Modbus Pack Service was created, Thread Id = %d.\n", gettid());
#endif	
	/* begin pack data loop */
	while (pThis->iPackDataThreadExitCmd == SERVICE_EXIT_NONE)
	{				/* feed watch dog */

		RunThread_Heartbeat(hLocalThread);

		Modbus_RefreshData();

		RunThread_Heartbeat(hLocalThread);
		
		Sleep(5000);//yield
	}
	return 0;
}
/*==========================================================================*
* FUNCTION : Modbus_CheckSPortParam
* PURPOSE  : Check the SPort Param
* CALLS    : 
*			  
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Wang Jing                   DATE: 2006-11-09 16:02
*==========================================================================*/
BOOL Modbus_CheckSPortParam(IN char *pSPortParam)
{
    char STDSPortParam[][32] = {"1200,n,8,1",
                                "2400,n,8,1",
                                "4800,n,8,1",
                                "9600,n,8,1",
                                "19200,n,8,1",
                                "38400,n,8,1"};
    char *pTempSPortParam = Cfg_RemoveWhiteSpace(pSPortParam);
    int i = 0;
    int iResult = 0;
    for(i = 0;i < 6;i++)
    {
        if(strcmp(pTempSPortParam,STDSPortParam[i]) == 0)
        {
            iResult = TRUE;
            break;
        }
        else
        {

            iResult = FALSE;
        }
    }
    return iResult;
}

BOOL Modbus_CheckNA(IN const char *szData)
{
#define MODBUS_CFG_SPEC_VAL_NA						"NA"

	if(0 == stricmp(szData, MODBUS_CFG_SPEC_VAL_NA))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}



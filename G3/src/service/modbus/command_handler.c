/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : command_handler.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "modbus.h"
#include <signal.h>

/* buff size for one type sig.  AI(0-63) AO(0-63) DI(0-255) DO(0-255) */
#define MODBUS_SIGTYPE_SIZE   512

/*time format */
#define MODBUS_TIME_FORMAT    "%y%m%d%H%M%S"

/* float value format definition */
struct tagIEEEFloat
{
	unsigned a:1;
	unsigned b:8;
	unsigned c:22;
	unsigned d:1;	 
};
typedef struct tagIEEEFloat IEEE_FLOAT;

struct tagModbusFloat
{
	unsigned a:1;
	unsigned b:1;
	unsigned c:22;
	unsigned d:8;	
};
typedef struct tagModbusFloat MODBUS_FLOAT;

union IEEE_FVALUE
{
	float fValue;
	unsigned char   cValue[4];
	IEEE_FLOAT fIEEE;
};

union MODBUS_FVALUE
{
	float fValue;
	unsigned char   cValue[4];
	MODBUS_FLOAT fModbus;
};


/* log Macros for Command Handler sub-module */
#define MODBUS_TASK_CMD		"MODBUS Command process"
	
/* used to init command handlers */
#define DEF_MODBUS_CMD_HANDLER(_p, _szCmdType, _bWriteFlag, _funExcute) \
          ((_p)->szCmdType = (_szCmdType), \
		   (_p)->bIsWriteCommand = (_bWriteFlag), \
		   (_p)->funExecute = _funExcute)


/* test battery log */


__INLINE static float GetAnalogFromSBV(SIG_BASIC_VALUE* pBv)
{
	VAR_VALUE *pVarValue;
	float fRetValue = 0;

	if (pBv != NULL)
	{
		pVarValue = &(pBv->varValue);
		switch (pBv->ucType)
		{
		case VAR_LONG:
			fRetValue = (float)pVarValue->lValue;
			break;

		case VAR_FLOAT:
			fRetValue = pVarValue->fValue;
			break;

		case VAR_UNSIGNED_LONG:
			fRetValue = (float)pVarValue->ulValue;
			break;

		case VAR_DATE_TIME:
			fRetValue = (float)pVarValue->dtValue;

#ifdef _TEST_DATA_PRECISION
			if (pVarValue->dtValue)
			{
				TRACEX("Actual time:      %ld\n", pVarValue->dtValue);
				TRACEX("After convertion: %f\n", fRetValue);
			}
#endif //_TEST_DATA_PRECISION

			break;

		case VAR_ENUM:
			fRetValue = (float)pVarValue->enumValue;
		}
	}

	return fRetValue;
}

__INLINE static SIG_ENUM GetDigitalFromSBV(SIG_BASIC_VALUE* pBv)
{
	/* for multi ACU equip type mapping to one MODBUS equip type, 
	   some signal may have not been defined in ACU model,
	   so pBv is NULL at the case. eg. Phase B current is not defined 
	   in SCUACUnit2 (803).
     */
	return (pBv == NULL ? 0 : pBv->varValue.enumValue);	
}



/*==========================================================================*
 * FUNCTION : *GetAnalogueStr
 * PURPOSE  : change float value to MODBUS analogue data format
 * CALLS    : 
 * CALLED BY: GetSigValues
 * ARGUMENTS: BOOL bReserved:
 *			  float  fValue : 
 *			  unsigned char *strValue: length of the buf is 9
 * RETURN   : unsigned char *: 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 10:21
 *==========================================================================*/
//static unsigned char *GetAnalogueStr(BOOL bReserved, float fValue, 
//									 unsigned char *strValue)
//{
//	union IEEE_FVALUE ieee_uf;
//	union MODBUS_FVALUE Modbus_uf;
//	union MODBUS_FVALUE tmpModbusFValue;
//
//	int i;
//
//	/* for reserved values */
//	if (!bReserved)
//	{
//		strcpy(strValue, "7FFFFF80");
//		return strValue;
//	}
//
//	/* normal value */
//	/* 1.endian process */
//	Modbus_uf.fValue = fValue;
//	tmpModbusFValue.fValue = fValue;
//    #ifdef  _CPU_BIG_ENDIAN  //for ppc
//		ieee_uf.fValue = fValue;
//
//    #else  //for x86
//	{
//	   union IEEE_FVALUE tmpFValue;
//	   tmpFValue.fValue = fValue;
//		for (i = 0; i < 4; i++)
//		{
//			ieee_uf.cValue[i] = tmpFValue.cValue[3-i];
//		}
//		
//	}
//    #endif 
//    
//	for (i = 0; i < 4; i++)
//	{
//	  
//		Modbus_uf.cValue[i] = tmpModbusFValue.cValue[i];
//		HexToTwoAsciiData(Modbus_uf.cValue[i], strValue  + 2*i);
//	}
//	
//	return strValue;
//}

/*==========================================================================*
 * FUNCTION : Power
 * PURPOSE  : simple assistant function
 * CALLS    : 
 * CALLED BY: GetDigitalStr
 * ARGUMENTS: int  x : 
 *            int  y : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 10:46
 *==========================================================================*/
static int Power(int x, int y)
{
	int i;
	int value = 1;

	for (i = 0; i < y; i++)
	{
		value *= x;
	}

	return value;
}

/*==========================================================================*
 * FUNCTION : ConvertTime
 * PURPOSE  : conver MODBUS23  time to UTC or convert UTC time to MODBUS time, MODBUS23 time zone is 8
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: time_t* pTime    : 
 *            BOOL bUTCToLocal  : TRUE, UTC time to MODBUS23 time , that is to add the zone8 , else
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : zwh                   DATE: 2008-11-10 15:00
 *==========================================================================*/
BOOL ConvertTime(time_t* pTime, BOOL bUTCToMODBUS)
{
	SIG_BASIC_VALUE* pSigValue;

	int nBufLen;

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
		SIG_ID_TIME_ZONE_SET,
		&nBufLen,			
		&pSigValue,			
		0) != ERR_DXI_OK)
	{	
		return FALSE;
	}

	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{	
		int nTimeOffset = (pSigValue->varValue.lValue) * 3600 / 2;

		if (bUTCToMODBUS)
		{
			*pTime += nTimeOffset;
		}
		else//MODBUS23 time to UTC time
		{
			*pTime -= nTimeOffset;
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : *GetDigitalStr
 * PURPOSE  : change BOOL value to MODBUS digital data format
 * CALLS    : Power
 * CALLED BY: GetSigValues
 * ARGUMENTS: SIG_ENUM  *peValue : the number of the array is 4.
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 10:30
 *==========================================================================*/
static unsigned char GetDigitalStr(SIG_ENUM *peValue)
{
	unsigned char cValue;
	int i;

	cValue = 0;

	/* get the nibble value */
	for (i = 0; i < 4; i++)
	{
		if (peValue[3-i] == 1)    //reverse order
		{
			cValue |= Power(2, i);
		}
	}

	/* get the hex character */
	if (cValue <= 9)
	{
		cValue += '0';
	}
	else  
	{
		cValue += ('A' - 10);
	}

	return cValue;
}
/*==========================================================================*
 * FUNCTION : GetSigValues
 * PURPOSE  : get sig values by batch and convert to MODBUS value format
 * CALLS    : DxiGetData
 *			  GetAnalogueStr
 *			  GetDigitalStr
 * CALLED BY: GetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : TRUE for Analogue sig, 
 *											   FALSE for Digital sig
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            MODBUS_MAPENTRIES_INFO  *pEntriesInfo : 
 *            unsigned char    *szOutData    : length is MODBUS_SIGTYPE_SIZE, 
 *										cleared to zero already
 * RETURN   : BOOL : FALSE for call DXI interface to get sig values failed 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-17 15:26
 *==========================================================================*/
BOOL ModbusGetSCUPSigValues(  MODBUS_MAPENTRIES_INFO *pEntriesInfo,
			int iEquipID,
			MODBUS_DataValue *szOutData)
{

     return ModbusGetSCUPSingleSigValues( pEntriesInfo->iSCUPSigType,
				    pEntriesInfo->iSCUPSigID,
				    iEquipID,
				    szOutData);
    
}

/*==========================================================================*
 * FUNCTION : GetDigitalData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SetSigValues
 * ARGUMENTS: IN unsigned char  chr      : 
 *            OUT SIG_ENUM      *peValue : 4 elements array
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-20 19:49
 *==========================================================================*/
static void GetDigitalData(IN unsigned char *chr, OUT SIG_ENUM *peValue)
{
	
	int iData;

	iData = (int)Modbus_AsciiHexToChar(chr);
	peValue[0] = iData;
	
	return;
}

/*==========================================================================*
 * FUNCTION : GetSubStringCount
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char*  szStr : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 16:03
 *==========================================================================*/
static int GetSubStringCount(IN const unsigned char* szStr)
{
	int iCount;
	const unsigned char *p;

	iCount = 0;
	p = szStr;
	while (*p != '\0')
	{
		if (*p == '!')
		{
			iCount++;
		}

		p++;
	}

	return iCount + 1;
}


/*==========================================================================*
 * FUNCTION : *GetSubString
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int                   iIndex : start from 1
 *            IN const unsigned char*  szStr  : 
 *			  OUT int *piSubStrLen            :
 * RETURN   : const unsigned char : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 16:06
 *==========================================================================*/
static const unsigned char *GetSubString(IN int iIndex, 
										 IN const unsigned char* szStr,
										 OUT int *piSubStrLen)
{
	int iCount;
	const unsigned char *p, *pSubStr;

	iCount = 0;
	p = szStr;

	/* special case */
	if (iIndex == 1)
	{
		pSubStr = p;
	}
	else
	{
		/* 1.move to the start pos */
		while (*p != '\0')
		{
			if (*p == '!')
			{
				iCount++;
			}

			if (iCount == iIndex - 1)
			{
				break;
			}

			p++;
		}

		/* 2.pick the sub-string */
		pSubStr = ++p;
	}

	*piSubStrLen = 0;
	while (*p != '!' && *p != '*' && *p != '\0')
	{
		(*piSubStrLen)++;
		p++;
	}

	if (*piSubStrLen == 0)
	{
		pSubStr = NULL;
	}

	return pSubStr;
}

__INLINE static BOOL SetAnalogToVarValue(int iEquipID, long lSigID,
										 VAR_VALUE_EX *pVarValue, int iData,int iDatadiv)
{
	int nBufLen;
	BOOL bRet;
	SIG_BASIC_VALUE  *pBv = NULL;

	bRet = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,	
		lSigID,
		&nBufLen,			
		&pBv,			
		0);
	//TRACE("\npBv->ucType is: %d\n", pBv->ucType);
	if (bRet == ERR_DXI_OK && pBv)
	{
		switch (pBv->ucType)
		{
		case VAR_FLOAT:
		
			pVarValue->varValue.fValue =((float) iData)/((float)iDatadiv);
			break;

		case VAR_LONG:
			pVarValue->varValue.lValue = iData/iDatadiv;
			break;

		case VAR_UNSIGNED_LONG:
			pVarValue->varValue.ulValue = iData/iDatadiv;
			break;

		case VAR_DATE_TIME:
			pVarValue->varValue.dtValue = iData/iDatadiv;
			break;

		case VAR_ENUM:
			pVarValue->varValue.enumValue = iData/iDatadiv;
			break;
		}

		return TRUE;

	}

	return FALSE;
}


__INLINE static BOOL SetDigitalToVarValue(int iEquipID, long lSigID,
								 VAR_VALUE_EX *pVarValue, SIG_ENUM eValue)
{
	int nBufLen;
	BOOL bRet = FALSE;
	SIG_BASIC_VALUE  *pBv;

	bRet = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,	
		lSigID,
		&nBufLen,			
		&pBv,			
		0);

	if ( bRet == ERR_DXI_OK && pBv)
	{
		pVarValue->varValue.enumValue = eValue;
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : SetSigValues
 * PURPOSE  : convert from MODBUS value format and set sig values by batch
 * CALLS    : GetAnalogueData
 *			  GetDigitalData
 * CALLED BY: SetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : 
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            MODBUS_MAPENTRIES_INFO  *pEntriesInfo : 
 *            const unsigned char    *szInData  : 
 * RETURN   : BOOL : 
 * COMMENTS : ensure the length of szInData is legal before call it!
 * CREATOR  : HanTao                   DATE: 2006-06-10 19:01
 *==========================================================================*/
static BOOL SetModbusSingleSigValues(int iSCUPSigType,
						 int iSCUPSigID,
						 int iEquipID,
						 int iSigNum, 
						 int iData,
						 int iDatadiv)
{
	UNUSED(iSigNum);
	int i;
	BOOL bRet = TRUE;
	SIG_ENUM digitals[4];
	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"Modbus Get SysVolt Level");

	/* need add in DXI module */
	int nError, nBufLen;
	int nInterfaceType, nVarID, nVarSubID;
	SET_MULTIPAL_SIGNALS stSetMulSignals;// = NEW(SET_MULTIPAL_SIGNALS, 1);
	SIG_BASIC_VALUE	*pBv;
  
	memset(&stSetMulSignals, 0, sizeof(SET_MULTIPAL_SIGNALS));
	nBufLen = sizeof(SET_MULTIPAL_SIGNALS);

	float	fRectAveVolt = 48;
	int isignalid;
	int idatalen;
	nError = DxiGetData(VAR_A_SIGNAL_VALUE,
		MODBUS_RECT_GROUP_ID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,MODBUS_RECT_RATED_VOLT_ID),		
		&idatalen,			
		&pBv,			
		0);

	if (nError != ERR_DXI_OK)
	{
		fRectAveVolt = 48;
	}
	else
	{
		fRectAveVolt = pBv->varValue.fValue;
	}

	isignalid = iSCUPSigID;
	//if(fRectAveVolt<35)
	if(stVoltLevelState) //24 V
	{
		if((iEquipID==1)&&(iSCUPSigType==2)&&
			((iSCUPSigID ==189)||(iSCUPSigID ==190)||(iSCUPSigID ==187)))
		{
			isignalid = iSCUPSigID+4;
		}
		else if((iEquipID==115)&&(iSCUPSigType==2)&&
			((iSCUPSigID ==6)||(iSCUPSigID ==16)||(iSCUPSigID ==17)
			||(iSCUPSigID ==46)||(iSCUPSigID ==68)))
		{
			isignalid = iSCUPSigID+100;
		}
	}

	/* note: check the length of sInData out */
	/* 1.format process */
	if (iSCUPSigType != -1) //not reserved pos
	{
		stSetMulSignals.lIDs[0] = DXI_MERGE_SIG_ID(iSCUPSigType, isignalid);
		if (!SetAnalogToVarValue(iEquipID, stSetMulSignals.lIDs[0],&(stSetMulSignals.vData[0]),iData,iDatadiv))
		{
				//j--;
		}
	}

	stSetMulSignals.nSetNum = 1;

	/* add extra info */
	for (i = 0; i < 1; i++)
	{
		stSetMulSignals.vData[i].nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
		if(iSCUPSigType == SIG_TYPE_SAMPLING)
			stSetMulSignals.vData[i].nSenderType = SERVICE_OF_LOGIC_CONTROL;
		else
		    stSetMulSignals.vData[i].nSenderType = SERVICE_OF_DATA_COMM;
		stSetMulSignals.vData[i].pszSenderName = "Modbus Protocol";
	}

	/* 2.set data by DXI interface */
#ifdef _DEBUG_MODBUS_CMD_HANDLER
	TRACE_MODBUS_TIPS("Will call DXI to set the batch of sig values");
	TRACE("Sigs ID info:\n");
	TRACE("\tTotal num: %d\n", stSetMulSignals.nSetNum);
	for (i = 0; i < stSetMulSignals.nSetNum; i++)
	{
		TRACE("\tEquip ID: %d", iEquipID);
		TRACE("\t\tMerged ID: %08X", stSetMulSignals.lIDs[i]);
		if (bAnalogue)
		{
			TRACE("\t\tNew values: %f\n", 
				stSetMulSignals.vData[i].varValue.fValue);
		}
		else // for Digital
		{
			TRACE("\t\tNew values: %ld\n", 
				stSetMulSignals.vData[i].varValue.enumValue);
		}
	}
#endif

	nInterfaceType = VAR_A_SIGNAL_VALUE;
	nVarID = iEquipID;  //  equip id
	nBufLen = sizeof(VAR_VALUE_EX);

	for (i = 0; i < stSetMulSignals.nSetNum; i++)
	{
		nVarSubID = stSetMulSignals.lIDs[i];

		nError = DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stSetMulSignals.vData[i],			
			0);

	    if (nError != ERR_DXI_OK)
		{
			    TRACEX("Set #%d sig failed. Error code from DXI: %08X iEquipID =%d\n", i+1, nError,iEquipID);
				TRACE("Set #%d sig failed. Error code from DXI: %08X\n", i+1, nError);
			bRet = FALSE;
		}
	}
	
	return bRet;
}

/*==========================================================================*
 * FUNCTION : SetSigValues
 * PURPOSE  : convert from MODBUS value format and set sig values by batch
 * CALLS    : GetAnalogueData
 *			  GetDigitalData
 * CALLED BY: SetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : 
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            MODBUS_MAPENTRIES_INFO  *pEntriesInfo : 
 *            const unsigned char    *szInData  : 
 * RETURN   : BOOL : 
 * COMMENTS : ensure the length of szInData is legal before call it!
 * CREATOR  : HanTao                   DATE: 2006-06-10 19:01
 *==========================================================================*/
BOOL SetModbusSigValues(int iEquipID,
						 int iSigNum, 
						 MODBUS_MAPENTRIES_INFO *pEntriesInfo,
						 int iData,
						 int iDatadiv )
{

    BOOL bRet = TRUE;

    bRet = SetModbusSingleSigValues(pEntriesInfo->iSCUPSigType,
						 pEntriesInfo->iSCUPSigID,
						 iEquipID,
						 iSigNum, 
						 iData,
						 iDatadiv);
   return bRet;
}


/*==========================================================================*
 * FUNCTION : ModbusGetSCUPSingleSigValues
 * PURPOSE  : get some signal values by batch and convert to MODBUS value format
 * CALLS    : DxiGetData
 *			  GetAnalogueStr
 *			  GetDigitalStr
 * CALLED BY: GetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : TRUE for Analogue sig, 
 *											   FALSE for Digital sig
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            MODBUS_MAPENTRIES_INFO  *pEntriesInfo : 
 *            unsigned char    *szOutData    : length is MODBUS_SIGTYPE_SIZE, 
 *										cleared to zero already
 * RETURN   : BOOL : FALSE for call DXI interface to get sig values failed 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-31 15:26
 *==========================================================================*/
BOOL ModbusGetSCUPSingleSigValues(int iSCUPSigType,
						 int iSCUPSigID,
						 int iEquipID,
						 MODBUS_DataValue *szOutData)
{
    //ASSERT(szOutData);
	//float	fBuf;

	char	      szStrValue[9];      //for IEEE format analogue string value

	//unsigned char			digital;

	/* to call DXI interface to get sig values */
	int		nError, nBufLen;
	int		nInterfaceType, nVarID, nVarSubID;
	SIG_BASIC_VALUE	*pBv;
	ALARM_SIG_VALUE *    palarmlevel;
	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"Modbus Get SysVolt Level");

	GET_MULTIPAL_SIGNALS	stGetMulSignals;


	/* 1.get data by DXI interface */
	nError = 0;
	nInterfaceType = VAR_MULTIPLE_SIGNALS_INFO;
	nVarID = MUL_SIGNALS_GET_VALUE;
	nVarSubID = 0;

	float	fRectAveVolt = 48;
	int isignalid;
	int idatalen;
	nError = DxiGetData(VAR_A_SIGNAL_VALUE,
		MODBUS_RECT_GROUP_ID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,MODBUS_RECT_RATED_VOLT_ID),		
		&idatalen,			
		&pBv,			
		0);

	if (nError != ERR_DXI_OK)
	{
		fRectAveVolt = 48;
	}
	else
	{
		fRectAveVolt = pBv->varValue.fValue;
	}

	isignalid = iSCUPSigID;
	//if(fRectAveVolt<35)
	if(stVoltLevelState)
	{
		if((iEquipID==1)&&(iSCUPSigType==2)&&
		((iSCUPSigID ==189)||(iSCUPSigID ==190)||(iSCUPSigID ==187)))
		{
			isignalid = iSCUPSigID+4;
		}
		else if((iEquipID==115)&&(iSCUPSigType==2)&&
			((iSCUPSigID ==6)||(iSCUPSigID ==16)||(iSCUPSigID ==17)
			||(iSCUPSigID ==46)||(iSCUPSigID ==68)))
		{
			isignalid = iSCUPSigID+100;
		}
	}
	memset(&stGetMulSignals, 0, sizeof(GET_MULTIPAL_SIGNALS));
	nBufLen = sizeof(GET_MULTIPAL_SIGNALS);
	
	stGetMulSignals.lData[0] = DXI_MERGE_UNIQUE_SIG_ID(iEquipID, 
				iSCUPSigType, isignalid);

	stGetMulSignals.nGetNum = 1;
	/* call DXI interface */
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&stGetMulSignals,			
		0);

	if ((nError != ERR_DXI_OK)||(stGetMulSignals.lData[0] ==NULL))
	{
		szOutData->VData.ctemp[0] = '\0';
		return FALSE;
	}

	/* 2.format process */
       pBv = (SIG_BASIC_VALUE*)(stGetMulSignals.lData[0]);
	palarmlevel =  *((ALARM_SIG_VALUE **)(stGetMulSignals.lData));
	szOutData->iDataType = pBv->ucType;


	if (pBv->ucType == VAR_FLOAT) //for Analogue sig
	{
		float fValue = GetAnalogFromSBV(pBv);

		szOutData->VData.ftemp =  fValue;

	}  //end of Analogue Sig
	else  //for Digital sig
	{
		if(iSCUPSigType == SIG_TYPE_ALARM)//added at 2007.3.8
		{
			szOutData->VData.ctemp[0] =  (unsigned char)GetDigitalFromSBV(pBv);
			szOutData->VData.ctemp[1] =  (unsigned char)(palarmlevel->pStdSig->iAlarmLevel);
		}
		else
		{
			szOutData->VData.itemp =  (int)GetDigitalFromSBV(pBv);
		}
		
	}  //end of Digital sig
    //end of loop

	return TRUE;
}


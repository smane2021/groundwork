/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : modbus.h
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __MODBUS_H_2006_05_09__
#define __MODBUS_H_2006_05_09__


#define	_DEBUG_MODBUS


/* const definitions */
#define MAX_MODBUSFRAME_LEN		256

#define MAX_MODBUS_SIGNAL_NUM		4096

#define MODBUS_RECT_GROUP_ID		2
#define MODBUS_RECT_RATED_VOLT_ID	21

/* used when call Queue_Get interface to get MODBUS Event, Unit: ms */

#define MODBUS_WAIT_FOR_I_QUEUE		3000	//wait for Event Input Queue
#define MODBUS_WAIT_FOR_O_QUEUE		6000	//wait for Event Output Queue

/* const definition for State Machine */
#define MODBUS_STATE_NUM           3


#define MODBUS_INVALIDDATA	 0xFFFF

#define MODBUS_REFRESHDATA_NUM		20
#define MODBUS_REFRESHDATA_DELAY	100

/* MODBUS command number supported */
#define MODBUS_CMD_NUM                3
#define MODBUS_GETDATA_CMD            0x03
#define MODBUS_SETSINGLEDATA_CMD            0x06
#define MODBUS_SETMULTIDATA_CMD            0x10

#define MODBUS_READ_CMD_NUM           1


/* input event queue and output event queue size */
#define MODBUS_EVENT_QUEUE_SIZE		20


//MAX EquipNum
#define MODBUS_MAX_EQUIP_NUM        1500

#define MODBUS_NETSERVICE_PORT    "502"

// compare with MODBUS_BASIC_ARGS.iTimeoutCounter. When ACU is on MODBUS_IDLE 
//state, if MC has no action within READCOM_TIMEOUT*MODBUS_MAX_TIMEOUTCOUNT,
// ACU will disconnect the communication actively.
#define MODBUS_MAX_TIMEOUTCOUNT    60


// for SMIO dynamic signal names
//#define SMIO_TYPE				"900"  //mapped to 901-908
#define MODBUS_MAX_SMIO_UNIT			8
#define MODBUS_SMIO_REF_TEXTS_NUM		18
#define MODBUS_MAX_REF_TEXTS			(MODBUS_MAX_SMIO_UNIT * MODBUS_SMIO_REF_TEXTS_NUM)


#define MODBUS_MAX_RECT_NUM		120
#define MODBUS_MAX_Batt_NUM		120
#define MODBUS_MAX_SLAVE_RECT_NUM	60

#define MODBUS_MBAP_LEN     7
#define MODBUS_MBAP_IDENTIFIER_LEN     1
#define MODBUS_ERRORRTN_LEN     3



#define mDigital 0
#define mAnalogy 1

#define  MODBUSREGISTERSTART 0
#define  MODBUS_SYS_REGISTERSTART 0
#define  MODBUS_SYS_REGISTEREND   21

#define  MODBUS_BattGroup_REGISTERSTART 22
#define  MODBUS_BattGroup_REGISTEREND   49

#define  MODBUS_Batt_REGISTERSTART 50
#define  MODBUS_PERBatt_NUM 5

#define  MODBUS_Batt1SET_REGISTERSTART 54
#define  MODBUS_Batt1SET_REGISTEREND   54

#define  MODBUS_Batt_REGISTEREND   649

#define  MODBUS_RectGroup_REGISTERSTART 650
#define  MODBUS_RectGroup_REGISTEREND   662

#define  MODBUS_Rect_REGISTERSTART 663
#define  MODBUS_PERRect_NUM 9;
#define  MODBUS_Rect_REGISTEREND 1742


#define  MODBUS_DC_REGISTERSTART 1743
#define  MODBUS_DC_REGISTEREND   1745

#define  MODBUS_AC_REGISTERSTART 1746
#define  MODBUS_AC_REGISTEREND   1760

#define  MODBUS_SLAVE1_Rect_REGSTART 1761
#define  MODBUS_SLAVE_PERRect_NUM 9;
#define  MODBUS_SLAVE1_Rect_REGEND 2300

#define  MODBUS_SLAVE2_Rect_REGSTART 2301
#define  MODBUS_SLAVE2_Rect_REGEND 2840

#define  MODBUS_SLAVE3_Rect_REGSTART 2841
#define  MODBUS_SLAVE3_Rect_REGEND 3380

#define  MODBUSREGISTEREND   3380

#define  MODBUS_SYSTEMP_REGISTER 8   //Ambient Temperature
#define  MODBUS_BATTGROUPTEMP_REGISTER 25 //Temperature

#define  MODBUS_TWOREG_FOR_ONEDATA_FLAG 1000000 
#define  MODBUS_DIVISOR			65536 




union tegModbusVARData
{
	float ftemp;
	char ctemp[4];
	int  itemp;
};
typedef union tegModbusVARData MODBUS_VAR_Data;

struct tagModbusDataValue
{
	int		iDataType;			/* event type */	
	MODBUS_VAR_Data	VData;
};
typedef struct tagModbusDataValue MODBUS_DataValue;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * basic structures 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* define protocol types */
enum MODBUS_PROTOCOL
{
	EEM_MODBUS = 1,
	RSOC_MODBUS,
	SOC_TPE_MODBUS,
	YDN_MODBUS,
	MODBUS,
	MODBUS_PROTOCOL_NUM
};


/* define transmit media types */
enum MODBUS_MEDIA_TYPE
{		
    MODBUS_MEDIA_TYPE_LEASED_LINE			= 0,
    MODBUS_MEDIA_TYPE_RS485,
    MODBUS_MEDIA_TYPE_TCPIP,
    MODBUS_MEDIA_TYPE_NUM
};

/* define timers */
enum MODBUS_TIMER
{
	MODBUS_ALARM_TIMER		= 1,
	MODBUS_CALLBACK_TIMER,

	MODBUS_SOC_WAIT_DLE0_TIMER,
	MODBUS_SOC_WAIT_CMD_TIMER,
	MODBUS_SOC_WAIT_ACK_TIMER,

	MODBUS_TIMER_NUM
};

/* define MODBUS Event types */
enum MODBUS_EVENT_TYPE
{
	 MODBUS_FRAME_EVENT				= 1,

	 MODBUS_CONNECTED_EVENT,
	 MODBUS_CONNECT_FAILED_EVENT,
	 MODBUS_DISCONNECTED_EVENT,

	 MODBUS_TIMEOUT_EVENT
};

/* MODBUS Event definition */
struct tagModbusEvent
{
	int		iEventType;			/* event type */	
	int		iDataLength;	/* data length of the event(only for FrameEvent) */
	unsigned char	sData[MAX_MODBUSFRAME_LEN];	/* data of the event(only for FrameEvent) */
	BOOL		bDummyFlag;		/* dummy flag for FrameEvent */
	BOOL		bSkipFlag;		/* skip flag for FrameEvent  */
};
typedef struct tagModbusEvent MODBUS_EVENT;

#define MODBUS_INIT_FRAME_EVENT(_pEvent, _iLen, _szData, _bDummyFlag, _bSkipFlag)   \
			((_pEvent)->iEventType = MODBUS_FRAME_EVENT, \
			(_pEvent)->iDataLength = (_iLen), \
			((_iLen) == 0 ? (_pEvent)->sData[0] = '\0' : \
				(void)memcpy((_pEvent)->sData, (_szData), (size_t)(_iLen))), \
			(_pEvent)->bDummyFlag = (_bDummyFlag), \
			(_pEvent)->bSkipFlag = (_bSkipFlag))

#define MODBUS_INIT_SIMPLE_FRAME_EVENT(_pEvent, _chr) \
			((_pEvent)->iEventType = MODBUS_FRAME_EVENT, \
			(_pEvent)->iDataLength = 1, \
			(_pEvent)->sData[0] = (_chr), \
			(_pEvent)->bDummyFlag = FALSE, \
			(_pEvent)->bSkipFlag = FALSE)

#define MODBUS_INIT_NONEFRAME_EVENT(_pEvent, _EventType)    \
					((_pEvent)->iEventType = (_EventType), \
					 (_pEvent)->iDataLength = 0, \
					 (_pEvent)->sData[0] = '\0', \
					 (_pEvent)->bDummyFlag = FALSE, \
					 (_pEvent)->bSkipFlag = FALSE)




/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Link-layer Manager Sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* link-layer operation mode definition */
enum MODBUS_OPERATION_MODE 
{
	MODBUS_MODE_SERVER						= 0,
	MODBUS_MODE_CLIENT
};


/* communication status between MC and EventQueues */
enum MODBUS_COMM_STATUS                  
{
	MODBUS_COMM_STATUS_DISCONNECT		= 0,
	MODBUS_COMM_STATUS_NEXT,
	MODBUS_COMM_STATUS_SKIP
};



// extend basic sig typed
enum SIG_TYPE_MODBUS_EX
{
	MODBUS_RESERVED = -1,
	MODBUS_ENABLE_BIT_OF_DI = -2,
	MODBUS_DISABLE_BIT_OF_DI = -3
};

//Frank Wu,20151112, for extending configuration file
enum MODBUS_REG_ACCESS_AUTHORITY_TYPE
{
	MODBUS_REG_ACCESS_AUTHORITY_READ,
	MODBUS_REG_ACCESS_AUTHORITY_WRITE,
	MODBUS_REG_ACCESS_AUTHORITY_NUM
};

enum MODBUS_CONFIG_VERSION_TYPE
{
	//if [TYPE_MAP_INFO_EXT] is exist in ModbusModelMap.cfg, current cfg version is MODBUS_CONFIG_VERSION_2
	//otherwise it is MODBUS_CONFIG_VERSION_1
	MODBUS_CONFIG_VERSION_1,
	MODBUS_CONFIG_VERSION_2,
	MODBUS_CONFIG_VERSION_NUM
};

struct tagModbusMapEntriesInfo	/* sig mapping */
{
	int	iModbusPort;			/* sig serial no. or command type*/
	int	iEquipType;         /* equip type ID*/
	int	iSCUPSigType;     	/* defined by SIG_TYPE enum */
	int	iSCUPSigID;	        /* signal ID in equip*/
	int	iFlag0;
	int	iFlag1;
	int	iLen;
	int	iStatusValue[5];
	float	fPeakValue[5];
	
	//Frank Wu,20151112, for extending configuration file
	int	iAccessAuthority;//register access authority, enum MODBUS_REG_ACCESS_AUTHORITY_TYPE
};
typedef struct tagModbusMapEntriesInfo MODBUS_MAPENTRIES_INFO;

struct tagModbusTypeMapInfo				  /* type mapping */
{
	int	                iFunctionCode;	          /* Modbus command CID1 */
	int	                iLengthFix;	          /* the Modbus command length is fix */
	int	                iLength;	          /* the Modbus command length is fix */						
};
typedef struct tagModbusTypeMapInfo MODBUS_TYPE_MAP_INFO;

//#define MODBUS_CFG_DESCRIPTION_MAX_LEN					(48 + 1)//end with '\0'

#define MODBUS_SPEC_PROC_FLAG_NA										0
//skip all non-exist equipments, just packet the exist equipments' data
#define MODBUS_SPEC_PROC_FLAG_1_SKIP_NONEXIST_EQUIP						(0x1 << 0)
//limit temperature range, for keeping the same processing as the old register table
#define MODBUS_SPEC_PROC_FLAG_2_COMPATIBLE_LIMIT_TEMP_REG_1_50			(0x1 << 1)
//convert non-exist signals, for keeping the same processing as the old register table
#define MODBUS_SPEC_PROC_FLAG_3_COMPATIBLE_CONV_SIG_REG_51_650			(0x1 << 2)

struct tagModbusExtTypeMapInfo				  /* type mapping */
{
	int							iIndex;
	//char						szDescription[MODBUS_CFG_DESCRIPTION_MAX_LEN];
	int							iMapentriesID;
	int							iRegStart;
	int							iRegEnd;
	int							iMaxRegCountPerEquip;//it will be always larger than 0
	int							iSequenceNum1stEquip;
	int							iEquipTypeStart;
	int							iEquipTypeEnd;
	unsigned int				uiSpecProcFlag;//each bit indicates one special process

	int							iMapEntriesNum;				/* the number of current model type */ 
	MODBUS_MAPENTRIES_INFO		*pMapEntriesInfo;			/* sig mapping info */

	//run info

};
typedef struct tagModbusExtTypeMapInfo MODBUS_EXT_TYPE_MAP_INFO;


struct tagModbusModelConfigInfo				  /* YDN model config file info */
{
	int	                iTypeMapNum;	 
	int	                iMapEntriesNum;	          /* the number of current Modbus model type */ 
	int                     iUnitNum;		  /* the number of units for current type */
	MODBUS_TYPE_MAP_INFO	pTypeMapInfo[MODBUS_CMD_NUM];		  /* type mapping info */
	MODBUS_MAPENTRIES_INFO	*pMapEntriesInfo;	  /* sig mapping info */	
	
	//Frank Wu,20151112, for extending configuration file
	int							iExtTypeMapNum;
	MODBUS_EXT_TYPE_MAP_INFO	*pExtTypeMapInfo;/* extend type mapping info, be defined in [EXT_TYPE_MAP_INFO] */

	//run info
	int							iExtCurrentCfgVersion;//MODBUS_CONFIG_VERSION_TYPE
	int							iExtMaxRegNum;//the max register end number in [EXT_TYPE_MAP_INFO]

};
typedef struct tagModbusModelConfigInfo MODBUSMODEL_CONFIG_INFO;



struct tagModbusBlockInfo	
{
	int			iFunctionCode;		
	int			iEquipID;	         /* relevant equip id of ACU model */			
	MODBUS_TYPE_MAP_INFO	*pTypeInfo;	 /* reference of relevant type info */

};
typedef struct tagModbusBlockInfo MODBUS_BLOCK_INFO;

struct tagModbusModelInfo	 
{
	int		iBlocksNum;	    /* the number of blocks */
	MODBUS_BLOCK_INFO	*pModbusBlockInfo;	 
};
typedef struct tagModbusModelInfo MODBUS_MODEL_INFO;



/* 3.Modbus common config info */
/* const definitions */
#define MODBUS_COMM_PORT_PARAM_LEN	   64


/* MODBUS common config info */
struct tagModbusCommonConfig
{
	unsigned int	    byADR;                
	unsigned int	    iProtocolType;	/* see enum MODBUS_PROTOCOL definition */
	unsigned int	    iMediaType;	    /* see enum MODBUS_MEDIA_TYPE definition */
	char		    szCommPortParam[MODBUS_COMM_PORT_PARAM_LEN];
	char                cModifyUser[40];//added by ht,2006.12.13, no use in common config file,just for system log.
};
typedef struct tagModbusCommonConfig MODBUS_COMMON_CONFIG;


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * structures for Frame Analysis sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
enum tagModbusFrameType
{
    MODBUS_NOR		= 0x00,
    MODBUS_FUNCCODEERR,	
    MODBUS_REGERR,	
    MODBUS_CTLFAIL	= 0x04,
    MODBUS_FRAME_ERR    = 0xF3
};
typedef enum tagModbusFrameType MODBUS_FRAME_TYPE;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Command Handler sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* command execution function prototype definition */
typedef struct tagModbusBasicArgs MODBUS_BASIC_ARGS;
typedef int (*MODBUS_CMD_EXECUTE_FUN) (MODBUS_BASIC_ARGS *pThis,
								 unsigned char *szInData,
								 unsigned char *szOutData);

struct tagModbusCmdHandler
{
	const char        *szCmdType;
	BOOL              bIsWriteCommand;
	MODBUS_CMD_EXECUTE_FUN   funExecute;
};
typedef struct tagModbusCmdHandler MODBUS_CMD_HANDLER;

struct tagModbusPackDataHandler
{
	const char        *szCmdType;
	unsigned char     *pPackData;
};
typedef struct tagModbusPackDataHandler MODBUS_PACKDATA_HANDLER;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for State Machine sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* states definition */
/* states of MODBUS State Machine */
enum MODBUS_STATES
{
	MODBUS_STATE_MACHINE_QUIT  = -1,

	MODBUS_IDLE,
	
};

/* on-state function prototype */
typedef int (*MODBUS_ON_STATE_PROC) (MODBUS_BASIC_ARGS *pThis);


struct tagModbusStateInfo
{
	int			iStateID;	                /* defined by MODBUS_STATES enum */
	MODBUS_ON_STATE_PROC	fnStateProc;	
}; 
typedef struct tagModbusStateInfo MODBUS_STATE_INFO;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * globals structures 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* basic arg structure */
struct tagModbusBasicArgs
{
	/* event queues */
	HANDLE		hEventInputQueue;	
	HANDLE		hEventOutputQueue;	

	/* data buffers */
	int		iTimeoutCounter; //used by MODBUS_IDEL and SOC_OFF state
	int		iResendCounter;  /*used when timeout
					(by MODBUS_WAIT_FOR_RESP_ACK,
					    MODBUS_WAIT_FOR_ALARM_ACK,
					    MODBUS_WAIT_FOR_CALLBACK_ACK,
					    SOC_WAIT_DLE0, 
					    SOC_ALARM_REPORT ) */	
	unsigned char       sReceivedBuff[MAX_MODBUSFRAME_LEN];	 /* used for CHAE-based frame recognize */
	int		    iCurPos;				 //for Received Buff
	int                 iLen;				 //for Received Buff
	unsigned char	    szCmdRespBuff[MAX_MODBUSFRAME_LEN];	 // command reply data buf 		
	char                *szBattLog;				//battery log buf, size is MODBUS_BATTLOG_SIZE

	/* link-layer current operation mode */
	int		    iOperationMode; /* use MODBUS_OPERATION_MODE const */

	/* communication handle */
	HANDLE	            hComm;	

	
	/* hThreadID[0]: Modbus Service thread id;
	 * hThreadID[1]: Linklayer thread id.
	 * hThreadID[2]: PackData thread id*/
	HANDLE  hThreadID[3];

	/* flags */
	int     iMachineState;    /* state machine current state */ 
	int	*pOutQuitCmd;	  /* MODBUS service exit command, from Main Module */
	int     iInnerQuitCmd;               /* use SERVICE_EXIT_CODE const     */
	int 	iLinkLayerThreadExitCmd;	 /* use THREAD_EXIT_CODE const      */
	int 	iPackDataThreadExitCmd;	 /* use THREAD_EXIT_CODE const      */
	BOOL	bServerModeNeedExit;	     /* server operation mode exit flag */
	BOOL	bProtocolChangedFlag;	     /* protocol type changed flag      */
	BOOL	bCommRunning;	             /* communication busy flag         */
	BOOL	bSecureLine;	             /* security connection flag        */
	BOOL    bFrameDone;                  /* used by frame split function    */
	BOOL	bADRChangedFlag;	     /* protocol type changed flag      */
};

struct tagModbusRespDataBuffer
{
    WORD szRespData1[MAX_MODBUS_SIGNAL_NUM];// derectly send buffer
    WORD szRespData2[MAX_MODBUS_SIGNAL_NUM];// szRespData1's buffer
    
	//Frank Wu,20151112, for extending configuration file
	//use malloc to get memory space in function CreateModbusBlocks
	WORD *pszExtRespData1;
	WORD *pszExtRespData2;
};
typedef struct tagModbusRespDataBuffer MODBUS_RESPDATA_BUFF;
extern MODBUS_RESPDATA_BUFF g_ModbusRespData;

/* used by RX* command */
struct tagModbusRefText
{
	int iModuleNo;  //No. of SMIO module, range from 1 to 8
	int iRefNo;     //fixed Ref. Text No. for each SMIO module, from 1 to 18

	char *szText1;  //reference of SMIO signal name 
	char *szText2;  //reference of Unit for AIs

	int *piAlarmLevel;  //reference of High or Low AI alarm level 
};
typedef struct tagModbusRefText MODBUS_REF_TEXT;

/* used to wait for Muxtex */
#define TIME_WAIT_MUTEX_COMCFG  1000
#define TIME_WAIT_MUTEX_SWITCH  3000
struct tagModbusGlobals
{
	MODBUSMODEL_CONFIG_INFO ModbusModelConfig;
	MODBUS_MODEL_INFO	ModbusModelInfo;
	MODBUS_COMMON_CONFIG	CommonConfig;

	/* reference texts info of the solution (used for RX* command) */
	int			iRefTexts;

	MODBUS_REF_TEXT		RefTexts[MODBUS_MAX_REF_TEXTS];
	MODBUS_PACKDATA_HANDLER PackDataHandlers[MODBUS_READ_CMD_NUM];
	MODBUS_STATE_INFO	ModbusStates[MODBUS_STATE_NUM];
	EQUIP_INFO* pEquipInfo;
	int SMAddr;
	int iEquipNum;
	int iEquipIDOrder[MODBUS_MAX_EQUIP_NUM][2];
	//HANDLE  hMutexCommonCfg;  //for MODBUS common config file

};
typedef struct tagModbusGlobals MODBUS_GLOBALS;

/* global variable declare */
extern MODBUS_GLOBALS  g_ModbusGlobals;

/* Simple Events declare */
extern MODBUS_EVENT g_ModbusDummyEvent, g_ModbusTimeoutEvent;
extern MODBUS_EVENT g_ModbusDiscEvent, g_ModbusConnFailEvent,g_ModbusConnEvent;


/* interface */
/* Config Builder sub-module */
int Modbus_InitConfig(void);

/* Linklayer Manager sub-module */
int Modbus_LinkLayerManager(MODBUS_BASIC_ARGS *pThis);

/* pack response data sub-module*/
int Modbus_PackData(MODBUS_BASIC_ARGS *pThis);

/* Frame Analyse sub-module */
MODBUS_FRAME_TYPE Modbus_AnalyseFrame(const unsigned char *pFrame, int iLen, 
							MODBUS_BASIC_ARGS *pThis,
							OUT unsigned char *pCmdData,
							OUT unsigned char *pMBAPhead);



MODBUS_FRAME_TYPE Modbus_03DecodeandPerfrom(unsigned char *pCmdData);
MODBUS_FRAME_TYPE Modbus_06DecodeandPerfrom(unsigned char *pCmdData);
MODBUS_FRAME_TYPE Modbus_10DecodeandPerfrom(unsigned char *pCmdData);
void Modbus_CodeResponseFrame(  IN  MODBUS_FRAME_TYPE RTN,
			      IN  int iAddr,
			      IN  int iFunctionCode,
			      unsigned char *pCmdData,
			      unsigned char *pMBAPhead,
			      OUT unsigned char *pFrameData,
			      OUT int *piFrameDataLen);

		
/* State Machine sub-module */
/* for YDN state machine */
int Modbus_OnIdle(MODBUS_BASIC_ARGS *pThis);


BYTE Modbus_AsciiHexToChar(IN const unsigned char *pStr);
unsigned int Modbus_CharToInt(IN const unsigned char *pStr, int iCNum);
BOOL Modbus_IsStrAsciiHex(const unsigned char *pStr, int iLen);
BOOL Modbus_CheckPhoneNumber(const char *szPhoneNumber);
BOOL Modbus_CheckIPAddress(const char *szIPAddress);
BOOL Modbus_CheckBautrateCfg(const char *szBautrateCfg);
BOOL Modbus_CheckModemCfg(const char *szModemCfg);
unsigned char *MODBUS_GetUnprintableChr(IN unsigned char chr);
void Modbus_PrintState(int iState);

void Modbus_ClearEventQueue(HANDLE hq, BOOL bDestroy);

BOOL ModbusGetSCUPSigValues( MODBUS_MAPENTRIES_INFO *pEntriesInfo,
						 int iEquipID,
						 MODBUS_DataValue *szOutData);
int Modbus_ModifyCommonCfg(BOOL bServiceIsRunning,
						BOOL bByForce,
						MODBUS_BASIC_ARGS *pThis,
						int iItemID,
						MODBUS_COMMON_CONFIG *pUserCfg);

BOOL ModbusGetSCUPSingleSigValues(int iSCUPSigType,
						 int iSCUPSigID,
						 int iEquipID,
						MODBUS_DataValue *szOutData);
BOOL SetModbusSigValues(int iEquipID,
						 int iSigNum, 
						 MODBUS_MAPENTRIES_INFO *pEntriesInfo,
						 int iData,
						 int iDatadiv);

BOOL Modbus_CheckSPortParam(IN char *pSPortParam);

int Modbus_GetEquipIDOrder(int iEquipTypeID);
int GetEquipIDOrderwithstartposition(int iEquipTypeID, int startposition);
extern int GetEquipIDOrderwithstartpositionByRange(int iMinEquipTypeID, int iMaxEquipTypeID, int startposition);
extern BOOL Modbus_CheckNA(IN const char *szData);


						
/* safe delete MODBUS event */
#define DELETE_MODBUS_EVENT(_pEvent) \
	do {                          \
	if ((_pEvent)->iEventType == MODBUS_FRAME_EVENT && !(_pEvent)->bDummyFlag) \
	{     \
		DELETE(_pEvent);  \
	}   \
	} while(0)

/* log utilities */
#define MODBUS_LOG_TEXT_LEN	256

/* error log */
#define LOG_MODBUS_E(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_ERROR, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: ERROR: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* extended error log(logged with error code) */
#define LOG_MODBUS_E_EX(_task, _szLogText, _errCode)  \
	(AppLogOut((_task), APP_LOG_ERROR, "%s(Error Code: %X).\n", \
	(_szLogText), (_errCode)),  \
	TRACE("[%s:%d]--%s: ERROR: %s(Error Code: %X).\n", __FILE__, __LINE__, \
	__FUNCTION__, (_szLogText), (_errCode)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* warning log */
#define LOG_MODBUS_W(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_WARNING, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: WARNING: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* info log */
#define LOG_MODBUS_I(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_INFO, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: MESSAGE: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime())) 




#ifdef _DEBUG_MODBUS_SERVICE
	#define _SHOW__SHOW_MODBUS_CUR_STATE_EVENT_INFO
	//#define _SHOW_MODBUS_CONFIG_INFO

    /* show the current state name of the State Machine */
	#define _SHOW_MODBUS_CUR_STATE

	#define _DEBUG_MODBUS_LINKLAYER
	#define _DEBUG_MODBUS_CMD_HANDLER
	#define _DEBUG_MODBUS_REPORT
#endif 

#ifdef _TEST_MODBUS_SERVICE
	#define _TEST_MODBUS_BATTLOG
	//#define	_TEST_MODBUS_MODIFYCONFIG
	#define _TEST_DATA_PRECISION
	#define _TEST_MODBUS_QUEUE_SYNCHRONIZATION
#endif //_TEST_MODBUD_SERVICE


#ifdef _DEBUG_MODBUS_SERVICE
	#define TRACE_MODBUS_TIPS(_tipInfo)   \
		(TRACE("[%s:%d]--%s: Message: %s.\n", __FILE__, __LINE__, \
		 __FUNCTION__, (_tipInfo)), \
		 TRACE("Time is: %.3f\n", GetCurrentTime()))

	#define MODBUS_GO_HERE  TRACE("[%s:%d]--%s: Go here!.\n", __FILE__, \
					__LINE__, __FUNCTION__)
#else
	#define MODBUS_GO_HERE  1 ? (void)0 : (void)0
	#define TRACE_MODBUS_TIPS(_tipInfo)   1 ? (void)0 : (void)0
#endif 


/* switch with Maintenance service */
#define COM_SHARE_SERVICE_SWITCH

#ifdef COM_SHARE_SERVICE_SWITCH
#define MODBUS_IS_SHARE_COM(_iMediaType)  \
	((_iMediaType) != MODBUS_MEDIA_TYPE_TCPIP ? TRUE : FALSE)


#endif //COM_SHARE_SERVICE_SWITCH


#endif 

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : modbus_state_machine.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "modbus.h"

/* timeout = MODBUS_READCOM_TIMEOUT * timeout counter */

#define MODBUS_RESEND_TIMES		4	//resend times(response and alarm frames)

/* some operations */

#define MODBUS_CLEAR_TIMEOUT_COUNTER(_pThis) \
	((_pThis)->iTimeoutCounter = 0)

#define MODBUS_NEED_RESEND(_pThis) \
	(++(_pThis)->iResendCounter <= MODBUS_RESEND_TIMES)

#define MODBUS_CLEAR_RESEND_COUNTER(_pThis) \
	((_pThis)->iResendCounter = 0)



#define MODBUS_TASK_ESM		"MODBUS State Machine"

/* error log */
#define LOG_MODBUS_ESM_E(_szLogText)  LOG_MODBUS_E(MODBUS_TASK_ESM, _szLogText)

/* warning log */
#define LOG_MODBUS_ESM_W(_szLogText)  LOG_MODBUS_W(MODBUS_TASK_ESM, _szLogText) 

/* info log */
#define LOG_MODBUS_ESM_I(_szLogText)  LOG_MODBUS_I(MODBUS_TASK_ESM, _szLogText)

/* assistant functional Macros */
/* create and send Dummy frame event */
#define MODBUS_SEND_DUMMY(_pThis, _pEvent)  \
	do {  \
	    (_pEvent) = &g_ModbusDummyEvent; \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_MODBUS_ESM_E("Put frame event to Output Queue failed"); \
			return MODBUS_IDLE;    \
		}  \
	}  \
	while (0)

/* send static MODBUS event to the OutputQueue */
#define MODBUS_SEND_SEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_MODBUS_ESM_E("Put frame event to Output Queue failed"); \
			return MODBUS_IDLE;    \
		}  \
	}  \
	while (0)

/* send dynamic MODBUS event to the OutputQueue */
#define MODBUS_SEND_DEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_MODBUS_ESM_E("Put frame event to Output Queue failed"); \
			DELETE((_pEvent));  \
			return MODBUS_IDLE;    \
		}  \
	}  \
	while (0)






/*==========================================================================*
 * FUNCTION : OnIdle_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Modbus_OnIdle
 * ARGUMENTS: MODBUS_BASIC_ARGS *pThis:
 *			  MODBUS_EVENT  *pEvent   : 
 * RETURN   : int : next MODBUS State, defined by MODBUS_STATES enum const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-17 20:02
 *==========================================================================*/
static int OnIdle_HandleFrameEvent(MODBUS_BASIC_ARGS *pThis, MODBUS_EVENT *pEvent)
{
	MODBUS_FRAME_TYPE frameType,getdataresult;
	unsigned char pCmdData[MAX_MODBUSFRAME_LEN];
	unsigned char pMBAPhead[MODBUS_MBAP_LEN];
	
	int  iFunctionCode, iAddr;

	/* frame analyse */
	//double tTime1 = GetCurrentTime();
	frameType = Modbus_AnalyseFrame(pEvent->sData, pEvent->iDataLength, pThis, pCmdData,pMBAPhead);

	if(g_ModbusGlobals.CommonConfig.iMediaType == MODBUS_MEDIA_TYPE_TCPIP )
	{
		iAddr = (int)g_ModbusGlobals.CommonConfig.byADR;
		iFunctionCode = (int)(pEvent->sData[MODBUS_MBAP_LEN]);
	}
	else
	{
		iAddr = (int)(pEvent->sData[0]);
		iFunctionCode = (int)(pEvent->sData[1]);
	}


	switch (frameType)
	{
	     case MODBUS_NOR:  
			if(iFunctionCode == MODBUS_GETDATA_CMD)
			{
			    getdataresult = Modbus_03DecodeandPerfrom(pCmdData);

			    DELETE(pEvent);

			}
			else if(iFunctionCode == MODBUS_SETSINGLEDATA_CMD)
			{
			    getdataresult = Modbus_06DecodeandPerfrom(pCmdData);

			    DELETE(pEvent);

			}
			else if(iFunctionCode == MODBUS_SETMULTIDATA_CMD)
			{
			    getdataresult = Modbus_10DecodeandPerfrom(pCmdData);

			    DELETE(pEvent);
			}
			else
			{
			    getdataresult = MODBUS_FUNCCODEERR;
			    DELETE(pEvent);
			}
			break;


		default:  //unexpected frame event
	   	      DELETE(pEvent);
		    break;
	}

	if(frameType == MODBUS_FRAME_ERR)//added at 2006.9.1
		return MODBUS_IDLE;

	pEvent = NEW(MODBUS_EVENT, 1);
	if (pEvent == NULL)
	{
		LOG_MODBUS_ESM_E("Memory is not enough");
		return MODBUS_IDLE;
	}

	
	MODBUS_INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);
	Modbus_CodeResponseFrame(getdataresult, iAddr,iFunctionCode,pCmdData,pMBAPhead,
	    pEvent->sData, &pEvent->iDataLength);

	MODBUS_SEND_DEVENT(pThis, pEvent);

	return MODBUS_IDLE;
	
}

/*==========================================================================*
 * FUNCTION : Modbus_OnIdle
 * PURPOSE  : 
 * CALLS    : Modbus_SendAlarm
 *			  OnIdle_HandleFrameEvent
 * CALLED BY: 
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis : 
 * RETURN   : int : next MODBUS State, defined by MODBUS_STATES enum const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-17 19:23
 *==========================================================================*/
int Modbus_OnIdle(MODBUS_BASIC_ARGS *pThis)
{
	int iRet;
	MODBUS_EVENT  *pEvent;

	/* feed watch dog */
	
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, MODBUS_WAIT_FOR_I_QUEUE);

	if (iRet == ERR_QUEUE_EMPTY)  
	{
		/* In Idel state, it means timeout, keep wait in next Idle state */
		return MODBUS_IDLE;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_MODBUS_ESM_E("Get from Input Queue failed(invalid args)");
		return MODBUS_IDLE;
	}

	/* reset timeout counter */
	if (pEvent->iEventType != MODBUS_TIMEOUT_EVENT)
	{
		MODBUS_CLEAR_TIMEOUT_COUNTER(pThis);
	}

		
	/* now iRet is ERR_OK */
	switch (pEvent->iEventType)
	{
	    case MODBUS_FRAME_EVENT:  //frame event process
			return OnIdle_HandleFrameEvent(pThis, pEvent);
	
	    case MODBUS_TIMEOUT_EVENT:  //timeout event process, send disconnect event

		   if (++pThis->iTimeoutCounter > MODBUS_MAX_TIMEOUTCOUNT )
		   {
			int iNextState = MODBUS_IDLE;
			pThis->iTimeoutCounter = 0;  //reset first

			/* create Disconnect event */
			pEvent = &g_ModbusDiscEvent;
				
			/* send it */
			MODBUS_SEND_SEVENT(pThis, pEvent);
			return iNextState;
		   }
		   else  //just send dummy
		   {
				MODBUS_SEND_DUMMY(pThis, pEvent);
				return MODBUS_IDLE;
		   }

	   case MODBUS_DISCONNECTED_EVENT:  //Disconnected Event
			return MODBUS_IDLE;

	  default: //unexpected event
			
			DELETE_MODBUS_EVENT(pEvent);
			return MODBUS_IDLE;
	}	

}


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : config_builder.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "modbus.h"

#if 0
#define TRACE_MODBUS_CFG_EXT printf
#define TRACE_MODBUS_CFG_EXT2 printf
#else
#define TRACE_MODBUS_CFG_EXT
#define TRACE_MODBUS_CFG_EXT2
#endif



/* define splitter charater used by MODBUS config files */
#define MODBUS_SPLITTER		 ('\t')

/* config file names */
#define CONFIG_FILE_MODBUSMODELMAP	  "private/modbus/ModbusModelMap.cfg"
#define CONFIG_FILE_MODBUSCOMMON	  "private/modbus/ModbusCommonConfig.cfg"

/* section names of MODBUS common config file */
#define  MODBUS_PROTOCOL_TYPE			 "[Protocol Type]"
#define  MODBUS_ADR			 "[ADR]"
#define  MODBUS_OPERATION_MEDIA		 "[Operation media]"
#define  MODBUS_MEDIA_PORT_PARAM		 "[Media Port Param]"


/* config items of MODBUS common Config file */
#define  MODBUS_CFG_ITEM_NUM			 4     

/* max length of cfg itme */
#define  MODBUS_MAX_CFG_ITEM_LEN        100

/* used to detect modified config item */
#define MODBUS_CFG_ITEM_TEST(_nVarID, _ItemMask) \
	(((_nVarID) & (_ItemMask)) == (_ItemMask) || \
	 ((_nVarID) & MODBUS_CFG_ALL) == MODBUS_CFG_ALL)


/* sections names of MODBUS Model Map config file */
#define  MODBUS_TYPE_MAP_NUM			 "[TYPE_MAP_NUM]"
#define  MODBUS_TYPE_MAP_INFO_SEC       "[TYPE_MAP_INFO]"
/* note: will be extended at runtime */
#define  MODBUS_MAPENTRIES_INFO_SEC		 "[MAPENTRIES_INFO_MAP]"

#define  MODBUS_EXT_TYPE_MAP_INFO_SEC		"[EXT_TYPE_MAP_INFO]"
#define  MODBUS_EXT_MAPENTRIES_INFO_SEC		"[EXT_MAPENTRIES_INFO]"



/* max type maps of Modbus Model config file */
#define  MODBUSMODEL_MAX_MAPS       2



/* to simplify log action when read MODBUS common config file */
#define LOG_MODBUS_COMMON_CFG(_section)  \
	(AppLogOut("Read Modbus Common Config", APP_LOG_ERROR, "Read %s failed, " \
			"not such section or format is invalid.\n", (_section)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s failed, not such section or format" \
             " is invalid.\n", __FILE__, __LINE__, __FUNCTION__, (_section)))

#define LOG_MODBUS_COMMON_CFG_2(_section)  \
	(AppLogOut("Read Modbus Common Config", APP_LOG_ERROR, "Read %s failed, " \
			"format is invalid.\n", (_section)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s failed, format is invalid.\n", \
            __FILE__, __LINE__, __FUNCTION__, (_section)))



/* simplify log function when read fields of a cfg table */
/* DT: data type error */
#define LOG_MODBUS_READ_TABLE_DT(_tableName, _fieldName)  \
	(AppLogOut("Load Modbus Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"data type of %s field is wrong.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, data type of %s "  \
        "field is wrong.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* DR: data range exceeds */ 
#define LOG_MODBUS_READ_TABLE_DR(_tableName, _fieldName)  \
	(AppLogOut("Load Modbus Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"%s field data exceeds the bound.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field data exceeds"  \
        "the bound.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* NO: not find the filed. may caused by bad format */
#define LOG_MODBUS_READ_TABLE_NO(_tableName, _fieldName) \
	(AppLogOut("Load Modbus Model Config", APP_LOG_ERROR, "Read %s table failed, %s " \
		"field not found(please use the correct EMPTY FIELD character).\n", \
		(_tableName), (_fieldName)), \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field not found"  \
        "(please use the correct EMPTY FIELD character).\n", \
		__FILE__, __LINE__, __FUNCTION__, (_tableName), (_fieldName)))

/* ET: field is empty */
#define LOG_MODBUS_READ_TABLE_ET(_tableName, _fieldName) \
	(AppLogOut("Load Modbus Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"%s field should not be empty.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field should not"  \
        "be empty.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* function prototype for MODBUS common config file item modification */
typedef BOOL (*MODBUS_HANDLE_CFG_ITEM) (int nVarID, 
				 MODBUS_COMMON_CONFIG *pUserCfg, 
				 CONFIG_FILE_MODIFIER *pModififier,
				 char *szContent);

/*==========================================================================*
 * FUNCTION : IsEmptyField
 * PURPOSE  : empty config item in MODBUS common cfg file
 * CALLS    : 
 * CALLED BY: LoadMODBUSCommonConfigProc
 * ARGUMENTS: const char  *pField : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-01 16:07
 *==========================================================================*/
static BOOL IsEmptyField(const char *pField)
{
	return (pField[0] == '-');
}


/*==========================================================================*
 * FUNCTION : LoadModbusCommonConfigProc
 * PURPOSE  : callback function called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: void  *pCfg       : 
 *            void  *pLoadToBuf : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-10 16:38
 *==========================================================================*/
static int LoadModbusCommonConfigProc(void *pCfg, void *pLoadToBuf)
{
	MODBUS_COMMON_CONFIG *pCommonCfg;
	int iRet, iBuf;

	pCommonCfg = (MODBUS_COMMON_CONFIG *)pLoadToBuf;

	/* 1.protocol type */
	iRet = Cfg_ProfileGetInt(pCfg, MODBUS_PROTOCOL_TYPE, &iBuf);
	pCommonCfg->iProtocolType = MODBUS;

	/* 2.ADR */
	iRet = Cfg_ProfileGetInt(pCfg, MODBUS_ADR, &iBuf);
	if (iRet != 1)
	{
		LOG_MODBUS_COMMON_CFG("ADR");
		return ERR_CFG_FAIL;
	}
	if (iBuf < 0 || iBuf > 255)
	{
		LOG_MODBUS_COMMON_CFG_2("ADR");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->byADR = (BYTE)iBuf;

	
	/* 3.operation media */
	iRet = Cfg_ProfileGetInt(pCfg, MODBUS_OPERATION_MEDIA, &iBuf);
	if (iRet != 1)
	{
		LOG_MODBUS_COMMON_CFG("Operation Media");
		return ERR_CFG_FAIL;
	}
	/* check value */
	if (iBuf < 0 || iBuf > 2)
	{
		LOG_MODBUS_COMMON_CFG_2("Operation Media");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->iMediaType = iBuf;


	/* 4.operation media port param */
	iRet = Cfg_ProfileGetString(pCfg, 
				    MODBUS_MEDIA_PORT_PARAM, 
				    pCommonCfg->szCommPortParam,
				    MODBUS_COMM_PORT_PARAM_LEN);
	if (iRet != 1)
	{
		LOG_MODBUS_COMMON_CFG_2("Media Port Param");
		return ERR_CFG_FAIL;
	}
	switch (pCommonCfg->iMediaType)
	{
	    case MODBUS_MEDIA_TYPE_LEASED_LINE:
	    case MODBUS_MEDIA_TYPE_RS485:
		if (!Modbus_CheckBautrateCfg(pCommonCfg->szCommPortParam))
		{
			LOG_MODBUS_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;

	    case MODBUS_MEDIA_TYPE_TCPIP:
		if (!Modbus_CheckIPAddress(pCommonCfg->szCommPortParam))
		{
			LOG_MODBUS_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;

	    default:
		LOG_MODBUS_COMMON_CFG_2("Media Port Param");
		return ERR_CFG_BADCONFIG;
	}
	    
	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : LoadModbusCommonConfig
 * PURPOSE  : to load MODBUS common config file
 * CALLS    : 
 * CALLED BY: Modbus_InitConfig
 * ARGUMENTS:  
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-18 20:56
 *==========================================================================*/
static int LoadModbusCommonConfig(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_MODBUSCOMMON, szCfgFileName, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadModbusCommonConfigProc,
		&g_ModbusGlobals.CommonConfig);

	
	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : ParseTypeMapTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Cfg_LoadSingleTable
 * ARGUMENTS: char           *szBuf       : 
 *            MODBUS_TYPE_MAP_INFO  *pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 09:28
 *==========================================================================*/
static int ParseTypeMapTableProc(char *szBuf, MODBUS_TYPE_MAP_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);
	
	/* 1.jump Sequence id field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);

	/* 2.jump Type Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);

	/* 3.command Function code field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Types Map", "Modbus Function Code");
		return 4;
	}
	if (!CFG_CheckHexNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Types Map", "Modbus Function Code");
		 return 4;    /* not a num, error */
	}
	sscanf(pField, "%x", &pStructData->iFunctionCode);

	/* 4. command Length fix field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Types Map", "Modbus LengthFix");
		return 4;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Types Map", "Modbus LengthFix");
		return 4;    /* not a num, error */
	}
	sscanf(pField, "%x", &pStructData->iLengthFix);

	/* 5.command Postion field */

	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Types Map", "Modbus CRC or LengthPosition");
		return 4;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Types Map", "Modbus CRC or LengthPosition");
		return 4;    /* not a num, error */
	}
	sscanf(pField, "%x", &pStructData->iLength);



	return 0;
}

/* assistant function for judge enable bit of MODBUS DI table.
 * modified by Thomas for CR, 2005-2-28 */
BOOL Modbus_IsEnableBit(IN const char *pField)
{
	if (*pField == 'E' && *(pField + 1) == 'N' &&
		*(pField + 2) == '\0')
	{
		return TRUE;
	}
	else 
	{
		return FALSE;
	}
}

BOOL Modbus_IsDisableBit(IN const char *pField)
{
	if (*pField == 'D' && *(pField + 1) == 'N' &&
		*(pField + 2) == '\0')
	{
		return TRUE;
	}
	else 
	{
		return FALSE;
	}
}
/* Modified end, Thoams, 2006-2-28 */


/*==========================================================================*
 * FUNCTION : ParseExtTypeMapTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Cfg_LoadSingleTable
 * ARGUMENTS: char           *szBuf       : 
 *            MODBUS_EXT_TYPE_MAP_INFO  *pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 09:28
 *==========================================================================*/
static int ParseExtTypeMapTableProc(IN char *szBuf, OUT MODBUS_EXT_TYPE_MAP_INFO *pStructData)
{
	char	*pField;
	int		iRet = 0;
	char	*pErrInfo = NULL;
	char	*ptempBuf;
	int		i = 0;
	int		iSplitCount = 0;
	int		iTemp;
	
	ASSERT(szBuf);
	ASSERT(pStructData);
	

	/* 1. Index */
	iRet++;
	pErrInfo = "Index";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iIndex = atoi(pField);
	if(pStructData->iIndex <= 0)
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 2. Description */
	iRet++;
	pErrInfo = "Description";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);

	/* 3. Map Entries ID */
	iRet++;
	pErrInfo = "Map Entries ID";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iMapentriesID = atoi(pField);
	if(pStructData->iMapentriesID <= 0)
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 4. Reg Start */
	iRet++;
	pErrInfo = "Reg Start";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iRegStart = atoi(pField);
	if((pStructData->iRegStart <= 0) 
		|| (pStructData->iRegStart > 65535))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 5. Reg End */
	iRet++;
	pErrInfo = "Reg End";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iRegEnd = atoi(pField);
	if((pStructData->iRegEnd <= 0) 
		|| (pStructData->iRegEnd > 65535))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	
	//check the relation between "Reg Start" and "Reg End"
	pErrInfo = "Reg Start > Reg End";
	if(pStructData->iRegStart > pStructData->iRegEnd)
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 6. Max Reg Count of Each Equip */
	iRet++;
	pErrInfo = "Max Reg Count of Each Equip";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if( Modbus_CheckNA(pField) )
	{
		pStructData->iMaxRegCountPerEquip = pStructData->iRegEnd - pStructData->iRegStart + 1;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
		pStructData->iMaxRegCountPerEquip = atoi(pField);
		if(pStructData->iMaxRegCountPerEquip <= 0)
		{
			LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
	}

	//check the relation among "Reg Start" "Max Reg Count of Each Equip" and "Reg End"
	pErrInfo = "MaxRegCountPerEquip != (Reg End - Reg Start + 1)/EquipNum";
	if(( 0 != ((pStructData->iRegEnd - pStructData->iRegStart + 1) % pStructData->iMaxRegCountPerEquip) ))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 7. Sequence Number of First Equip */
	iRet++;
	pErrInfo = "Sequence Number of First Equip";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if( Modbus_CheckNA(pField) )
	{
		pStructData->iSequenceNum1stEquip = -1;
	}
	else
	{
		if ((*pField < '0') || (*pField > '9'))
		{
			LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
		pStructData->iSequenceNum1stEquip = atoi(pField);
		if(pStructData->iSequenceNum1stEquip <= 0)
		{
			LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
			return iRet;
		}
	}

	/* 8. SCU+ StdEquipType Start */
	iRet++;
	pErrInfo = "SCU+ StdEquipType Start";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iEquipTypeStart = atoi(pField);
	if(pStructData->iEquipTypeStart <= 0)
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	
	/* 9. SCU+ StdEquipType End */
	iRet++;
	pErrInfo = "SCU+ StdEquipType End";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}
	pStructData->iEquipTypeEnd = atoi(pField);
	if(pStructData->iEquipTypeEnd <= 0)
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	//check the relation between "SCU+ StdEquipType Start" and "SCU+ StdEquipType End"
	pErrInfo = "iEquipTypeStart > iEquipTypeEnd";
	if( pStructData->iEquipTypeStart > pStructData->iEquipTypeEnd )
	{
		LOG_MODBUS_READ_TABLE_NO(MODBUS_EXT_TYPE_MAP_INFO_SEC, pErrInfo);
		return iRet;
	}

	/* 10. special Process Flag */
	iRet++;
	pErrInfo = "Special Process Flag";
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if( Modbus_CheckNA(pField) )
	{
		pStructData->uiSpecProcFlag = MODBUS_SPEC_PROC_FLAG_NA;
	}
	else
	{
		pStructData->uiSpecProcFlag = MODBUS_SPEC_PROC_FLAG_NA;
	
		i = 0;
		iSplitCount = Cfg_GetSplitStringCount(pField, ',');
		while(i < iSplitCount)
		{
			if(i != iSplitCount -1)
			{
				pField = Cfg_SplitStringEx(pField, &ptempBuf, ',');
				//TRACE("\nptempBuf is %s\n", ptempBuf);
				iTemp = atoi(ptempBuf);
			}
			else
			{
				iTemp = atoi(pField);
			}
			
			if(1 == iTemp)
			{
				pStructData->uiSpecProcFlag |= MODBUS_SPEC_PROC_FLAG_1_SKIP_NONEXIST_EQUIP;
			}
			else if(2 == iTemp)
			{
				pStructData->uiSpecProcFlag |= MODBUS_SPEC_PROC_FLAG_2_COMPATIBLE_LIMIT_TEMP_REG_1_50;
			}
			else if(3 == iTemp)
			{
				pStructData->uiSpecProcFlag |= MODBUS_SPEC_PROC_FLAG_3_COMPATIBLE_CONV_SIG_REG_51_650;
			}
			
			i++;
		}
	}

	return 0;
}





/*==========================================================================*
 * FUNCTION : ParseMapEntriesTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: LoadMODBUSModelMapConfigProc
 * ARGUMENTS: char               *szBuf      : 
 *            MODBUS_MAPENTRIES_INFO *  pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 11:39
 *==========================================================================*/
static int ParseMapEntriesTableProc(char *szBuf, MODBUS_MAPENTRIES_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	char *ptempBuf;
	int i = 0;
	int iSplitCount = 0;

	/* 1.Port ID  */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "Port ID");
		return 2;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Port ID");
		return 2;
	}
	pStructData->iModbusPort = atoi(pField);

    /* 2.jump Signal Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);

    /* 3.M810G StdEquipType ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Types Map", "StdEquipType ID");
		return 3;
	}
	else if( Modbus_CheckNA(pField) )
	{
		pStructData->iEquipType = -1;
	}
	else
	{
		pStructData->iEquipType = atoi(pField);
	}
	
	/* 4.M810G model signal type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (Cfg_IsEmptyField(pField))  // means no relevant sig type in M810G model
	{
		pStructData->iSCUPSigType = MODBUS_RESERVED;
		pStructData->iSCUPSigID = MODBUS_RESERVED;   /* just finish */
		return 0;
	}

	/* Added by Thomas for CR: using enable bit in DI table to distinguage the availability 
	 * of the front value, 2006-2-28. */
	else if (Modbus_IsEnableBit(pField))
	{
		pStructData->iSCUPSigType = MODBUS_ENABLE_BIT_OF_DI;
		pStructData->iSCUPSigID = MODBUS_RESERVED;   /* just finish */
		return 0;
	}
	else if (Modbus_IsDisableBit(pField))
	{
		pStructData->iSCUPSigType = MODBUS_DISABLE_BIT_OF_DI;
		pStructData->iSCUPSigID = MODBUS_RESERVED;   /* just finish */
		return 0;
	}
	/* modified end, Thomas, 2006-2-28 */

	else
	{
		if (*pField == '\0')
		{
			LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "ACU+ model signal type");
			return 4;
		}

		/* check and assign(valid value: SP(sampling) ST(setting) C(control) A(alarm)) */
		switch (*pField)
		{
		case 'S':
			switch (*(pField + 1))
			{
			case 'P':   /* SP = sampling sig type */
				pStructData->iSCUPSigType = SIG_TYPE_SAMPLING;
				break;

			case 'T':   /* ST = setting sig type */
				pStructData->iSCUPSigType = SIG_TYPE_SETTING;
				break;

			default:    /* S? : invalid */
				LOG_MODBUS_READ_TABLE_DR("Model Map Entries", "ACU+ model signal type");
				return 4;
			}
			break;

		case 'C':       /* C = control sig type */
			pStructData->iSCUPSigType = SIG_TYPE_CONTROL;
			break;

		case 'A':		/* A = alarm sig type */
			pStructData->iSCUPSigType = SIG_TYPE_ALARM;
			break;

		case 'N':
			break;
        	
		default:
			LOG_MODBUS_READ_TABLE_DR("Model Map Entries", "ACU+ model signal type");
			return 4;
		}
	}
	
	/* 5.Signal ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "Signal ID");
		return 5;
	}
	
	if( Modbus_CheckNA(pField) )
	{
		pStructData->iSCUPSigID = -1;
	}
	else if (*pField == '*')
	{
		pStructData->iSCUPSigID = -1;
		return 0;    /* succeed return */
	}

	if (!CFG_CheckNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Signal ID");
		return 5;
	}
	pStructData->iSCUPSigID = atoi(pField);

	/* 6.Flag0 */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "Flag0");
		return 6;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Flag0");
		return 6;
	}
	pStructData->iFlag0 = atoi(pField);

	/* 7.Flag1 */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "Flag1");
		return 7;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Flag1");
		return 7;
	}
	pStructData->iFlag1 = atoi(pField);

	/*8.iStatusValue*/

	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	iSplitCount = Cfg_GetSplitStringCount(pField, '/');
	if(*pField != 'N')
	{
		while(i < iSplitCount)
		{
		    if(i != iSplitCount -1)
		    	{
		        pField = Cfg_SplitStringEx(pField, &ptempBuf, 0x2F);
		        //TRACE("\nptempBuf is %s\n", ptempBuf);
		        pStructData->iStatusValue[i] = atoi(ptempBuf);
				pStructData->fPeakValue[i] = atof(ptempBuf);
		    	}
		    
		    else
		    	{
		        pStructData->iStatusValue[i] = atoi(pField);
				pStructData->fPeakValue[i] = atof(pField);
		    	}
		    i++;
		}
		pStructData->iLen = iSplitCount + 1;
	}
	else
		pStructData->iLen = -1;


	return 0;
}


/*==========================================================================*
 * FUNCTION : ParseMapEntriesTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: LoadMODBUSModelMapConfigProc
 * ARGUMENTS: char               *szBuf      : 
 *            MODBUS_MAPENTRIES_INFO *  pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-11 11:39
 *==========================================================================*/
static int ParseExtMapEntriesTableProc(char *szBuf, MODBUS_MAPENTRIES_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	char *ptempBuf;
	int i = 0;
	int iSplitCount = 0;

	/* 1.Port ID  */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "Port ID");
		return 2;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Port ID");
		return 2;
	}
	pStructData->iModbusPort = atoi(pField);

    /* 2.jump Signal Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);

    /* 3.M810G StdEquipType ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Types Map", "StdEquipType ID");
		return 3;
	}
	else if( Modbus_CheckNA(pField) )
	{
		pStructData->iEquipType = -1;
	}
	else
	{
		pStructData->iEquipType = atoi(pField);
	}
	
	/* 4.M810G model signal type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (Cfg_IsEmptyField(pField))  // means no relevant sig type in M810G model
	{
		pStructData->iSCUPSigType = MODBUS_RESERVED;
		pStructData->iSCUPSigID = MODBUS_RESERVED;   /* just finish */
		return 0;
	}

	/* Added by Thomas for CR: using enable bit in DI table to distinguage the availability 
	 * of the front value, 2006-2-28. */
	else if (Modbus_IsEnableBit(pField))
	{
		pStructData->iSCUPSigType = MODBUS_ENABLE_BIT_OF_DI;
		pStructData->iSCUPSigID = MODBUS_RESERVED;   /* just finish */
		return 0;
	}
	else if (Modbus_IsDisableBit(pField))
	{
		pStructData->iSCUPSigType = MODBUS_DISABLE_BIT_OF_DI;
		pStructData->iSCUPSigID = MODBUS_RESERVED;   /* just finish */
		return 0;
	}
	/* modified end, Thomas, 2006-2-28 */

	else
	{
		if (*pField == '\0')
		{
			LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "ACU+ model signal type");
			return 4;
		}

		/* check and assign(valid value: SP(sampling) ST(setting) C(control) A(alarm)) */
		switch (*pField)
		{
		case 'S':
			switch (*(pField + 1))
			{
			case 'P':   /* SP = sampling sig type */
				pStructData->iSCUPSigType = SIG_TYPE_SAMPLING;
				break;

			case 'T':   /* ST = setting sig type */
				pStructData->iSCUPSigType = SIG_TYPE_SETTING;
				break;

			default:    /* S? : invalid */
				LOG_MODBUS_READ_TABLE_DR("Model Map Entries", "ACU+ model signal type");
				return 4;
			}
			break;

		case 'C':       /* C = control sig type */
			pStructData->iSCUPSigType = SIG_TYPE_CONTROL;
			break;

		case 'A':		/* A = alarm sig type */
			pStructData->iSCUPSigType = SIG_TYPE_ALARM;
			break;

		case 'N':
			break;
        	
		default:
			LOG_MODBUS_READ_TABLE_DR("Model Map Entries", "ACU+ model signal type");
			return 4;
		}
	}
	
	/* 5.Signal ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "Signal ID");
		return 5;
	}
	
	if( Modbus_CheckNA(pField) )
	{
		pStructData->iSCUPSigID = -1;
	}
	else if (*pField == '*')
	{
		pStructData->iSCUPSigID = -1;
		return 0;    /* succeed return */
	}

	if (!CFG_CheckNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Signal ID");
		return 5;
	}
	pStructData->iSCUPSigID = atoi(pField);

	/* 6.Flag0 */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "Flag0");
		return 6;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Flag0");
		return 6;
	}
	pStructData->iFlag0 = atoi(pField);

	/* 7.Flag1 */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if (*pField == '\0')
	{
		LOG_MODBUS_READ_TABLE_NO("Model Map Entries", "Flag1");
		return 7;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Flag1");
		return 7;
	}
	pStructData->iFlag1 = atoi(pField);

	/*8.iStatusValue*/

	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	iSplitCount = Cfg_GetSplitStringCount(pField, '/');
	if(*pField != 'N')
	{
		while(i < iSplitCount)
		{
		    if(i != iSplitCount -1)
		    	{
		        pField = Cfg_SplitStringEx(pField, &ptempBuf, 0x2F);
		        //TRACE("\nptempBuf is %s\n", ptempBuf);
		        pStructData->iStatusValue[i] = atoi(ptempBuf);
				pStructData->fPeakValue[i] = atof(ptempBuf);
		    	}
		    
		    else
		    	{
		        pStructData->iStatusValue[i] = atoi(pField);
				pStructData->fPeakValue[i] = atof(pField);
		    	}
		    i++;
		}
		pStructData->iLen = iSplitCount + 1;
	}
	else
		pStructData->iLen = -1;


	/* 9.Access Authority(R/W) */
	//the field is optional
	szBuf = Cfg_SplitStringEx(szBuf, &pField, MODBUS_SPLITTER);
	if ((*pField != 'R')
		&& (*pField != 'r')
		&& (*pField != 'W')
		&& (*pField != 'w'))
	{
		LOG_MODBUS_READ_TABLE_DT("Model Map Entries", "Access Authority(R/W)");
		return 9;    /* not a num, error */
	}

	if((*pField == 'R') || (*pField == 'r'))
	{
		pStructData->iAccessAuthority = MODBUS_REG_ACCESS_AUTHORITY_READ;
	}
	else if((*pField == 'W') || (*pField == 'w'))
	{
		pStructData->iAccessAuthority = MODBUS_REG_ACCESS_AUTHORITY_WRITE;
	}
	else//number
	{
		pStructData->iAccessAuthority = MODBUS_REG_ACCESS_AUTHORITY_READ;
	}

	return 0;
}



/*==========================================================================*
 * FUNCTION : GetMapEntriesSecName
 * PURPOSE  : assistant function to get Map Entries Section Name
 * CALLS    : 
 * CALLED BY: LoadModbusModelMapConfigProc
 * ARGUMENTS: IN const char  *szBaseName       : 
 *            IN int         iMapIndex         : 
 *            OUT char       *szEntriesSecName : 
 *            IN int         iLen              : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : 
 *==========================================================================*/
static void GetMapEntriesSecName(IN const char *szBaseName, 
								 IN int iMapIndex,
								 OUT char *szEntriesSecName,
								 IN int iLen)
{
	char szFix[10], *p;
	int i;
    
	sprintf(szFix, "_MAP%d]%c", iMapIndex, '\0');
	strncpy(szEntriesSecName, szBaseName, (size_t)iLen);

	p = szEntriesSecName;
	for (i = iLen; *p != ']' && --i > 0; p++)
	{
		;
	}

	*p = '\0';

    i = strlen(szEntriesSecName);
	strncat(szEntriesSecName, szFix, (size_t)(iLen - i -1));

	return;
}

/*==========================================================================*
 * FUNCTION : LoadExtModelMapConfigProc
 * PURPOSE  : callback funtion called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: LoadModelMapConfig
 * ARGUMENTS: void         *pCfg       : 
 *            void         *pLoadToBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao
 *==========================================================================*/
static int LoadExtModelMapConfigProc(IN void *pCfg, OUT void *pLoadToBuf)
{
    MODBUSMODEL_CONFIG_INFO *pBuf;

	CONFIG_TABLE_LOADER loader[1];
	//char szMapEntriesSecName[MODEL_MAX_MAPS][30];
	int iMaps, i;

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	TRACE_MODBUS_CFG_EXT("LoadExtModelMapConfigProc Start\n");


	pBuf = (MODBUSMODEL_CONFIG_INFO *)pLoadToBuf;


	//1. load [AID_GROUP_INFO] at first
	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pBuf->iExtTypeMapNum),
		MODBUS_EXT_TYPE_MAP_INFO_SEC,
		&(pBuf->pExtTypeMapInfo), 
		ParseExtTypeMapTableProc);

	if (Cfg_LoadTables(pCfg,1,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	/* then load map entries */
	iMaps = pBuf->iExtTypeMapNum;
	
	TRACE_MODBUS_CFG_EXT("iExtTypeMapNum=%d\n", pBuf->iExtTypeMapNum);

	if(iMaps <= 0)
	{
		return ERR_CFG_OK;
	}

	//2. load all [EXT_MAPENTRIES_INFO_MAPXXX]
	CONFIG_TABLE_LOADER loader1[iMaps];
	char szMapEntriesSecName[iMaps][35];

	for (i = 0; i < iMaps; i++)
	{
		GetMapEntriesSecName(
			MODBUS_EXT_MAPENTRIES_INFO_SEC,
			pBuf->pExtTypeMapInfo[i].iMapentriesID,
			szMapEntriesSecName[i],
			35);

		DEF_LOADER_ITEM(&loader1[i],
			NULL,
			&(pBuf->pExtTypeMapInfo[i].iMapEntriesNum),
			szMapEntriesSecName[i],
			&(pBuf->pExtTypeMapInfo[i].pMapEntriesInfo),
			ParseExtMapEntriesTableProc);
	}

	if (Cfg_LoadTables(pCfg,iMaps,loader1) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	TRACE_MODBUS_CFG_EXT("LoadExtModelMapConfigProc End\n");

	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : LoadModbusModelMapConfigProc
 * PURPOSE  : callback funtion called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: LoadModbusModelMapConfig
 * ARGUMENTS: void         *pCfg       : 
 *            void         *pLoadToBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao
 *==========================================================================*/
static int LoadModbusModelMapConfigProc(void *pCfg, void *pLoadToBuf)
{
	MODBUSMODEL_CONFIG_INFO *pBuf;

	CONFIG_TABLE_LOADER loader[MODBUSMODEL_MAX_MAPS];
	
	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	TRACE_MODBUS_CFG_EXT("LoadModbusModelMapConfigProc Start\n");

	pBuf = (MODBUSMODEL_CONFIG_INFO *)pLoadToBuf;

	
	DEF_LOADER_ITEM(&loader[1],
			NULL, &(pBuf->iMapEntriesNum), 
			MODBUS_MAPENTRIES_INFO_SEC, &(pBuf->pMapEntriesInfo), 
			ParseMapEntriesTableProc);

	if (Cfg_LoadTables(pCfg,1,loader + 1) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}
	
	TRACE_MODBUS_CFG_EXT("MODBUS_EXT_TYPE_MAP_INFO_SEC start\n");

	//Frank Wu,20151112, for extending configuration file
	//for configuration file forward compatible
	//the new section maybe not exist
	if(-1 == Cfg_GetSectionLineCount(pCfg, MODBUS_EXT_TYPE_MAP_INFO_SEC))
	{
		//use default value
		g_ModbusGlobals.ModbusModelConfig.iExtTypeMapNum = 0;
		g_ModbusGlobals.ModbusModelConfig.pExtTypeMapInfo = NULL;

		g_ModbusGlobals.ModbusModelConfig.iExtCurrentCfgVersion = MODBUS_CONFIG_VERSION_1;
	}
	else
	{
		g_ModbusGlobals.ModbusModelConfig.iExtCurrentCfgVersion = MODBUS_CONFIG_VERSION_2;
		
		if( ERR_CFG_OK != LoadExtModelMapConfigProc(pCfg, &g_ModbusGlobals.ModbusModelConfig) )
		{
			return ERR_CFG_FAIL;
		}
	}
	TRACE_MODBUS_CFG_EXT("iExtCurrentCfgVersion=%d\n",
						g_ModbusGlobals.ModbusModelConfig.iExtCurrentCfgVersion);

	TRACE_MODBUS_CFG_EXT("LoadModbusModelMapConfigProc End\n");

	return ERR_CFG_OK;

}

/*==========================================================================*
 * FUNCTION : LoadModbusModelMapConfig
 * PURPOSE  : to load Modbus Model Map config file
 * CALLS    : Cfg_LoadConfigFile
 * CALLED BY: Modbus_InitConfig
 * ARGUMENTS: 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-19 15:43
 *==========================================================================*/
static int LoadModbusModelMapConfig(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	g_ModbusGlobals.ModbusModelConfig.iTypeMapNum = MODBUS_CMD_NUM;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[0].iFunctionCode = MODBUS_GETDATA_CMD;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[0].iLengthFix = 0;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[0].iLength = 8;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[1].iFunctionCode = MODBUS_SETSINGLEDATA_CMD;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[1].iLengthFix = 0;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[1].iLength = 8;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[2].iFunctionCode = MODBUS_SETMULTIDATA_CMD;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[2].iLengthFix = 1;
	g_ModbusGlobals.ModbusModelConfig.pTypeMapInfo[2].iLength = 7;
		;
	Cfg_GetFullConfigPath(CONFIG_FILE_MODBUSMODELMAP, szCfgFileName, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadModbusModelMapConfigProc,
		&g_ModbusGlobals.ModbusModelConfig);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}


/* Added by Thomas end, support product info. 2006-2-9 */

/*==========================================================================*
 * FUNCTION : OrderEquipID
 * PURPOSE  : 
 * CALLS    : GetUnitNum
 *			  SetUnitNum
 * CALLED BY: CreateModbusBlocks
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-23 19:45
 *==========================================================================*/
 static void OrderEquipID(int EquipNum, EQUIP_INFO* pEquipInfo)
{
	int i, j, k, iError, iBufLen;
	int iEquipTypeID;
	int iStdEquipTypeNum;
	STDEQUIP_TYPE_INFO *pStdEquiPTypeInfo;
	EQUIP_INFO *pCurEquip;

	iError = DxiGetData(VAR_STD_EQUIPS_LIST, 0, 0, &iBufLen, &pStdEquiPTypeInfo, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Order EquipID",APP_LOG_ERROR, 
			"Get StdEquip List through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		
		return;
	}

	iError = DxiGetData(VAR_STD_EQUIPS_NUM, 0, 0, &iBufLen, &iStdEquipTypeNum, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Order EquipID",APP_LOG_ERROR, 
			"Get StdEquip Num through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		
		return;
	}

	k = 0;
	for(i = 0; i < iStdEquipTypeNum; i++, pStdEquiPTypeInfo++)
	{
		pCurEquip = pEquipInfo;
		for(j = 0; j < EquipNum; j++, pCurEquip++)
		{
			iEquipTypeID = pCurEquip->iEquipTypeID;
			if(iEquipTypeID == pStdEquiPTypeInfo->iTypeID)
			{
				g_ModbusGlobals.iEquipIDOrder[k][0] = pCurEquip->iEquipID;
				g_ModbusGlobals.iEquipIDOrder[k][1] = pCurEquip->iEquipTypeID;
				k++;
			}
		}
	}
}
/*==========================================================================*
 * FUNCTION : CreateModbusBlocks
 * PURPOSE  : 
 * CALLS    : GetUnitNum
 *			  SetUnitNum
 * CALLED BY: Modbus_InitConfig
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-18 19:45
 *==========================================================================*/
static int CreateModbusBlocks(void)
{
	EQUIP_INFO* pEquipInfo;//, *pCurEquip;
	int iEquipNum;
	int iInterfaceType, iBufLen, iError;
	MODBUSMODEL_CONFIG_INFO		*pstModelCfg = &g_ModbusGlobals.ModbusModelConfig;
	int							i;

	TRACE_MODBUS_CFG_EXT("CreateModbusBlocks Start\n");

	// 1.get equip list 
	iInterfaceType = VAR_ACU_EQUIPS_LIST;

	iError = DxiGetData(iInterfaceType, 0, 0, &iBufLen, &pEquipInfo, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Create Modbus Blocks",APP_LOG_ERROR, 
			"Get Equip List through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		return ERR_CFG_FAIL;
	}

	iEquipNum = iBufLen/sizeof(EQUIP_INFO);
	g_ModbusGlobals.iEquipNum = iEquipNum;
	g_ModbusGlobals.pEquipInfo = pEquipInfo;
	

	OrderEquipID(iEquipNum, pEquipInfo);
	
	
	//2.Init buffer. use malloc to get memory space for MODBUS_RESPDATA_BUFF
	g_ModbusRespData.pszExtRespData1 = NULL;
	g_ModbusRespData.pszExtRespData2 = NULL;
	pstModelCfg->iExtMaxRegNum = 0;
	
	if(MODBUS_CONFIG_VERSION_2 == pstModelCfg->iExtCurrentCfgVersion)
	{
		//2.1 find the max register end number
		for(i = 0; i < pstModelCfg->iExtTypeMapNum; i++)
		{
			if(pstModelCfg->iExtMaxRegNum < pstModelCfg->pExtTypeMapInfo[i].iRegEnd)
			{
				pstModelCfg->iExtMaxRegNum = pstModelCfg->pExtTypeMapInfo[i].iRegEnd;
			}
		}
		
		//2.2 malloc memory space
		if(pstModelCfg->iExtMaxRegNum > 0)
		{
			//register data is stored in pszExtRespDataX, from 1 to N, the first data is in [1], not [0]
			g_ModbusRespData.pszExtRespData1 = NEW(WORD, pstModelCfg->iExtMaxRegNum + 1);
			g_ModbusRespData.pszExtRespData2 = NEW(WORD, pstModelCfg->iExtMaxRegNum + 1);
			if((NULL == g_ModbusRespData.pszExtRespData1)
				|| (NULL == g_ModbusRespData.pszExtRespData2))
			{
				return ERR_CFG_FAIL;
			}
		}
		
	}
	TRACE_MODBUS_CFG_EXT("iExtMaxRegNum=%d, pszExtRespData1=%p, pszExtRespData2=%p\n",
						pstModelCfg->iExtMaxRegNum,
						g_ModbusRespData.pszExtRespData1,
						g_ModbusRespData.pszExtRespData2);


	TRACE_MODBUS_CFG_EXT("CreateModbusBlocks End\n");

	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : *GetAlarmLevelRef
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CreateRefTexts
 * ARGUMENTS: BOOL            bHigh          : 
 *            int             iAlarmSigNum   : 
 *            ALARM_SIG_INFO  *pAlarmSigInfo : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-19 12:08
 *==========================================================================*/
__INLINE static int *GetAlarmLevelRef(BOOL bHigh,
									  int iAlarmSigNum,
									  ALARM_SIG_INFO *pAlarmSigInfo)
{
	int i;
	int iSigID;

	for (i = 0; i < iAlarmSigNum; i++, pAlarmSigInfo++)
	{
		iSigID = pAlarmSigInfo->iSigID;
		if (bHigh && iSigID % 2 == 1 && iSigID <= 11)
		{
			return &pAlarmSigInfo->iAlarmLevel;
		}

		if (!bHigh && iSigID % 2 == 0 && iSigID <= 12)
		{
			return &pAlarmSigInfo->iAlarmLevel;
		}
	}

	return NULL;
}


/*==========================================================================*
 * FUNCTION : Modbus_InitConfig
 * PURPOSE  : 
 * CALLS    : LoadModbusCommonConfig  
 *			  LoadModbusModelMapConfig
 *			  SortMapEntries
 *			  CreateModbusBlocks
 * CALLED BY: InitModbusGlobals (service_provider.c)
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-19 20:40
 *==========================================================================*/
int Modbus_InitConfig()
{

	/* read Modbus Common Config */
	if (LoadModbusCommonConfig() != ERR_CFG_OK )
	{
		return ERR_CFG_FAIL;
	}

	/* read Modbus Model Config */
	if (LoadModbusModelMapConfig() != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	/* init Modbus Model */
    if (CreateModbusBlocks() != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : HandleProtocolType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            MODBUS_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:16
 *==========================================================================*/
static BOOL HandleProtocolType(int nVarID, 
						MODBUS_COMMON_CONFIG *pUserCfg, 
						CONFIG_FILE_MODIFIER *pModifier,
						char *szContent)
{
	if (MODBUS_CFG_ITEM_TEST(nVarID, MODBUS_CFG_PROTOCOL_TYPE))
	{
		sprintf(szContent, "%d%c", pUserCfg->iProtocolType, '\0');

		DEF_MODIFIER_ITEM(pModifier, 
			MODBUS_PROTOCOL_TYPE, 
			0, 
			0, 
			szContent);
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleADR
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            MODBUS_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModifier, char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 10:16
 *==========================================================================*/
static BOOL HandleADR(int nVarID, 
					   MODBUS_COMMON_CONFIG *pUserCfg, 
					   CONFIG_FILE_MODIFIER *pModifier,
					   char *szContent)
{
	if (MODBUS_CFG_ITEM_TEST(nVarID, MODBUS_CFG_ADR))
	{
		sprintf(szContent, "%d%c", pUserCfg->byADR, '\0');

		DEF_MODIFIER_ITEM(pModifier, 
			MODBUS_ADR, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}





/*==========================================================================*
 * FUNCTION : HandleMediaType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            MODBUS_COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 10:31
 *==========================================================================*/
static BOOL HandleMediaType(int nVarID, 
							MODBUS_COMMON_CONFIG *pUserCfg, 
							CONFIG_FILE_MODIFIER *pModififier,
							char *szContent)
{
	if (MODBUS_CFG_ITEM_TEST(nVarID, MODBUS_CFG_MEDIA_TYPE))
	{
		sprintf(szContent, "%d%c", pUserCfg->iMediaType, '\0');
	
		DEF_MODIFIER_ITEM(pModififier, 
			MODBUS_OPERATION_MEDIA, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


static BOOL HandleCommPortParam(int nVarID, 
								MODBUS_COMMON_CONFIG *pUserCfg, 
								CONFIG_FILE_MODIFIER *pModififier,
								char *szContent)
{
	if (MODBUS_CFG_ITEM_TEST(nVarID, MODBUS_CFG_MEDIA_PORT_PARAM) ||
		MODBUS_CFG_ITEM_TEST(nVarID, MODBUS_CFG_MEDIA_TYPE))
	{
		strcpy(szContent, pUserCfg->szCommPortParam);
	
		DEF_MODIFIER_ITEM(pModififier, 
			MODBUS_MEDIA_PORT_PARAM, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : Modbus_GetModifiedFileBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID       : 
 *            Modbus_COMMON_CONFIG  *pUserCfg    : 
 *            char           **pszOutFile : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-12 14:03
 *==========================================================================*/
BOOL Modbus_GetModifiedFileBuf(int nVarID, 
							MODBUS_COMMON_CONFIG *pUserCfg,
							char **pszOutFile)
{
	CONFIG_FILE_MODIFIER Modififier[MODBUS_CFG_ITEM_NUM];

	/* note: MODBUS_MAX_CFG_ITEM_LEN should be long enough! */
	char szCfgFileName[MAX_FILE_PATH]; 
	char szContent[MODBUS_CFG_ITEM_NUM][MODBUS_MAX_CFG_ITEM_LEN];
	int iRet, iIndex, iModifiers;
	BOOL bModified;

	const char *pLogText;  //for log

	MODBUS_HANDLE_CFG_ITEM fnHandlers[MODBUS_CFG_ITEM_NUM];


	/* init the handlers */
	fnHandlers[0] = HandleProtocolType;
	fnHandlers[1] = HandleADR;
	fnHandlers[2] = HandleMediaType;
	fnHandlers[3] = HandleCommPortParam;

	
	

	/* fill Modififier one by one */
	iModifiers = 0;
	for (iIndex = 0; iIndex < MODBUS_CFG_ITEM_NUM; iIndex++)
	{
		bModified = fnHandlers[iIndex](nVarID, 
			pUserCfg,
			&Modififier[iModifiers],
			szContent[iModifiers]);

		if (bModified)
		{
			iModifiers++;
		}
	}

	/* get the file buf */
	Cfg_GetFullConfigPath(CONFIG_FILE_MODBUSCOMMON, szCfgFileName, MAX_FILE_PATH);
	iRet = Cfg_ModifyConfigFile(szCfgFileName,
						 iModifiers,
						 Modififier,
						 pszOutFile);


	/* to simplify log action */
#define TASK_NAME        "Modifify Modbus common config file"
#define LOG_MODIFY_FILE(_pLogText)  \
	(AppLogOut(TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	switch (iRet)
	{
	case ERR_CFG_OK:
		break;

	case ERR_CFG_FILE_OPEN:
		pLogText = "Open Modbus Common Config file for modification failed.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_NO_MEMORY:
		pLogText = "No memory.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_FILE_READ:
		pLogText = "Read Modbus Common Config file for modification failed.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_PARAM:
		pLogText = "Something is wrong with CONFIG_FILE_MODIFIER param for "
			"modification.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	default:
		pLogText = "Unhandled error when create new Modbus Common Config file"
			" in memory.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;
	}

	return TRUE;
}
		

/*==========================================================================*
 * FUNCTION : Modbus_UpdateCommonConfigFile
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-29 14:20
 *==========================================================================*/
BOOL Modbus_UpdateCommonConfigFile(const char *szContent)
{
	FILE *pFile;
	char szCfgFileName[MAX_FILE_PATH]; 
	size_t count;

	Cfg_GetFullConfigPath(CONFIG_FILE_MODBUSCOMMON, szCfgFileName, MAX_FILE_PATH);
	pFile = fopen(szCfgFileName, "w");
	if (pFile == NULL)
	{
		return FALSE;
	}

	count = strlen(szContent);
	if (fwrite(szContent, 1, count, pFile) != count)
	{
		fclose(pFile);
		return FALSE;
	}

	fclose(pFile);

	return TRUE;
}

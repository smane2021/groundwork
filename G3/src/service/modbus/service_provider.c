/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU+(Standard Controller Unit Plus)
 *
 *  FILENAME : service_provider.c
 *  CREATOR  : HanTao                   DATE: 2006-05-09 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "modbus.h"
/* define log tasks */
#define LOG_INIT_MODBUS_SERVICE    "Init Modbus service"

#define LOG_MODIFY_MODBUS_COMMON_CONFIG    "Mod PARAM"


 
/* used to init state machines */
#define MODBUS_DEF_STATE_MACHINE(_p, _iStateID, _fnProc)  \
	      ((_p)->iStateID = (_iStateID), \
		   (_p)->fnStateProc = (_fnProc))



/* define the global variable of MODBUSservice */
MODBUS_GLOBALS g_ModbusGlobals;

/* define the share-used simple events */
MODBUS_EVENT g_ModbusDummyEvent, g_ModbusTimeoutEvent;
MODBUS_EVENT g_ModbusDiscEvent, g_ModbusConnFailEvent, g_ModbusConnEvent;

MODBUS_RESPDATA_BUFF g_ModbusRespData;

__INLINE static void InitStaticEvents()
{
	MODBUS_INIT_FRAME_EVENT(&g_ModbusDummyEvent, 0, 0, TRUE, FALSE);
	MODBUS_INIT_NONEFRAME_EVENT(&g_ModbusTimeoutEvent, MODBUS_TIMEOUT_EVENT);
	MODBUS_INIT_NONEFRAME_EVENT(&g_ModbusDiscEvent, MODBUS_DISCONNECTED_EVENT);
	MODBUS_INIT_NONEFRAME_EVENT(&g_ModbusConnFailEvent, MODBUS_CONNECT_FAILED_EVENT);
	MODBUS_INIT_NONEFRAME_EVENT(&g_ModbusConnEvent, MODBUS_CONNECTED_EVENT);
}
/*==========================================================================*
 * FUNCTION : InitModbusGlobals
 * PURPOSE  : assistant function to init g_MODBUSGlobals
 * CALLS    : 
 * CALLED BY: ServiceMain
 * RETURN   : DWORD : use SERVICE_EXIT_CODE const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-05-21 20:08
 *==========================================================================*/
static DWORD InitModbusGlobals(void)
{
	MODBUS_STATE_INFO  *pStateInfo;

	/* 1.init config */
	if (Modbus_InitConfig() != ERR_CFG_OK)
	{
		return SERVICE_EXIT_CFG;
	}

	/* 2.init Modbus state machine */
	pStateInfo = g_ModbusGlobals.ModbusStates;

	MODBUS_DEF_STATE_MACHINE(&pStateInfo[0], 
		MODBUS_IDLE, 
		Modbus_OnIdle);

	return SERVICE_EXIT_NONE;
}


/*==========================================================================*
 * FUNCTION : StateMachineLoop
 * PURPOSE  : for main loop of state machine
 * CALLS    : 
 * CALLED BY: ServiceMain
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis : 
 * RETURN   : DWORD : use SERVICE_EXIT_CODE const
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 15:01
 *==========================================================================*/
static DWORD StateMachineLoop(MODBUS_BASIC_ARGS *pThis)
{
	int iLastState;
	int *piState;
	MODBUS_STATE_INFO *pCurStateMachine;

	piState = &(pThis->iMachineState);
	
	while (*(pThis->pOutQuitCmd) == SERVICE_CONTINUE_RUN && 
		pThis->iInnerQuitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		
		RunThread_Heartbeat(pThis->hThreadID[0]);
		
		pCurStateMachine = g_ModbusGlobals.ModbusStates;
		*piState = MODBUS_IDLE;

		TRACE_MODBUS_TIPS("Begin MODBUS state machine");


		while (*piState != MODBUS_STATE_MACHINE_QUIT)
		{
			/* for TRACE */
			iLastState = *piState;

			/* process one state */
			*piState = pCurStateMachine[*piState].fnStateProc(pThis);

			/* check quit conditions */
			if (*(pThis->pOutQuitCmd) == SERVICE_STOP_RUNNING ||
				pThis->iInnerQuitCmd != SERVICE_EXIT_NONE ||
				pThis->bProtocolChangedFlag)
			{
				TRACE("Exit state machine loop");
				TRACE("OutQuitCmd: %d\tInnerQuitCmd: %d\tProtocol Changed "
					"Flag: %d\n", *(pThis->pOutQuitCmd), pThis->iInnerQuitCmd,
					pThis->bProtocolChangedFlag);

				*piState = MODBUS_STATE_MACHINE_QUIT;

				/* reset flag */
				if (pThis->bProtocolChangedFlag)
				{
					pThis->bProtocolChangedFlag = FALSE;
					pThis->bFrameDone = TRUE;
				}
			}
			/* check quit conditions */
			if (*(pThis->pOutQuitCmd) == SERVICE_STOP_RUNNING ||
				pThis->iInnerQuitCmd != SERVICE_EXIT_NONE ||
				pThis->bADRChangedFlag)
			{
				TRACE_MODBUS_TIPS("Exit state machine loop");
				TRACE("OutQuitCmd: %d\tInnerQuitCmd: %d\tAdress Changed "
					"Flag: %d\n", *(pThis->pOutQuitCmd), pThis->iInnerQuitCmd,
					pThis->bADRChangedFlag);

				*piState = MODBUS_STATE_MACHINE_QUIT;

				/* reset flag */
				if (pThis->bADRChangedFlag)
				{
					pThis->bADRChangedFlag = FALSE;
					pThis->bFrameDone = TRUE;
				}
			}
	
		}
	}

	if (pThis->iInnerQuitCmd != SERVICE_EXIT_NONE)
	{
		return pThis->iInnerQuitCmd;
	}

	return SERVICE_EXIT_OK;
}


/*==========================================================================*
 * FUNCTION : RoutineBeforeExitService
 * PURPOSE  : to do clean work before service exit
 * CALLS    : MODBUS_ClearEventQueue
 * CALLED BY: ServiceMain
 * ARGUMENTS: MODBUS_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 15:53
 *==========================================================================*/
static void RoutineBeforeExitService(MODBUS_BASIC_ARGS *pThis)
{
	int i;
	/*HANDLE hMutex;*/

	MODBUSMODEL_CONFIG_INFO *pModelConfig;
	

	

	/* 2.kill all the timers */
	
	/* 4.stop child thread(it is safe even when child thread is not running) */
	if (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;
	}
	/* wait 60s at most*/
	TRACE("to stop iLinkLayerThreadExitCmd!!\n");
	int iTemp = RunThread_Stop(pThis->hThreadID[1], 5000, TRUE);
	pThis->hThreadID[1] = NULL;
	TRACE("iTemp1 is %d\n", iTemp);
	TRACE("stop iLinkLayerThreadExitCmd end!!\n");


    /* 3.stop child thread(it is safe even when child thread is not running) */
	if (pThis->iPackDataThreadExitCmd == SERVICE_EXIT_NONE)
	{
		pThis->iPackDataThreadExitCmd = SERVICE_EXIT_OK;
	}
	/* wait 60s most*/
	TRACE("to stop iPackDataThreadExitCmd!!\n");
	iTemp = RunThread_Stop(pThis->hThreadID[2], 5000, TRUE);
	pThis->hThreadID[2] = NULL;
	TRACE("iTemp2 is %d\n", iTemp);
	TRACE("stop iPackDataThreadExitCmd end!!\n");

		/* 5.destroy event queues */
	Modbus_ClearEventQueue(pThis->hEventInputQueue, TRUE);
	Modbus_ClearEventQueue(pThis->hEventOutputQueue, TRUE);
		

	/* 6.Free memories */
	/* free memory of ModbusModelConfig */
	pModelConfig = &g_ModbusGlobals.ModbusModelConfig;
	//pTypeMap = pModelConfig->pTypeMapInfo;
	
	DELETE(pModelConfig->pMapEntriesInfo);
	//DELETE(pModelConfig->pTypeMapInfo);

	/* free memory of ModbusModelInfo */
	DELETE(g_ModbusGlobals.ModbusModelInfo.pModbusBlockInfo);

	//Frank Wu,20151112, for extending configuration file
	for(i = 0; i < pModelConfig->iExtTypeMapNum; i++)
	{
		SAFELY_DELETE(pModelConfig->pExtTypeMapInfo[i].pMapEntriesInfo);
	}
	pModelConfig->iExtTypeMapNum = 0;
	SAFELY_DELETE(pModelConfig->pExtTypeMapInfo);
	
	SAFELY_DELETE(g_ModbusRespData.pszExtRespData1);
	SAFELY_DELETE(g_ModbusRespData.pszExtRespData2);

	return;
}


/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SERVICE_ARGUMENTS *  pArgs : 
 * RETURN   : DWORD : service exit code, defined in app_service.h
 * COMMENTS : 
 * CREATOR  : 
 *==========================================================================*/
#define		GET_SERVICE_OF_MODBUS_NAME		"modbus.so"

DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{
	DWORD dwExitCode;
	//int iRet;
	const char *pLogText;

	MODBUS_BASIC_ARGS l_ModbusBasicArgs;
	HANDLE hInputQueue, hOutputQueue;
	HANDLE hServiceThread, hLinklayerThread, hPackDataThread;
	APP_SERVICE		*MODBUS_AppService = NULL;
	MODBUS_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_MODBUS_NAME);

	/* clear to zero */
	memset(&g_ModbusGlobals, 0, sizeof(MODBUS_GLOBALS));
	memset(&l_ModbusBasicArgs, 0, sizeof(MODBUS_BASIC_ARGS));

	/* get the thread handle, used to feed watch dog */
	hServiceThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hServiceThread);

	/* 1.initialize g_ModbusGlobals */

	TRACE("enter Modbus ServiceMain\n");
	
	dwExitCode = InitModbusGlobals();

	if (dwExitCode != SERVICE_EXIT_NONE)
	{
		RoutineBeforeExitService(&l_ModbusBasicArgs);
		return dwExitCode;
	}
	
	/* init the static events */
	 InitStaticEvents();

	/* feed watch dog */
	RunThread_Heartbeat(hServiceThread);

	/* 2.init MODBUS_BASIC_ARGS */
	/* event queues */
	hInputQueue = Queue_Create(MODBUS_EVENT_QUEUE_SIZE,
		sizeof(MODBUS_EVENT *), 0);
	hOutputQueue = Queue_Create(MODBUS_EVENT_QUEUE_SIZE,
		sizeof(MODBUS_EVENT *), 0);

	if (hInputQueue == NULL || hOutputQueue == NULL)
	{
		AppLogOut(LOG_INIT_MODBUS_SERVICE, APP_LOG_ERROR, "Create Event Queue "
			"failed.\n");
		TRACE("[%s]--%s: ERROR: Create Event Queue failed.\n", __FILE__,
			__FUNCTION__);

		RoutineBeforeExitService(&l_ModbusBasicArgs);
		return SERVICE_EXIT_FAIL;
	}

	l_ModbusBasicArgs.hEventInputQueue = hInputQueue;
	l_ModbusBasicArgs.hEventOutputQueue = hOutputQueue;

	/* get MODBUS Service thread id */
	l_ModbusBasicArgs.hThreadID[0] = hServiceThread;

	/* flags */
	l_ModbusBasicArgs.iOperationMode = MODBUS_MODE_SERVER;
	l_ModbusBasicArgs.pOutQuitCmd = &pArgs->nQuitCommand;
	l_ModbusBasicArgs.iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;
	l_ModbusBasicArgs.iPackDataThreadExitCmd = SERVICE_EXIT_NONE;
	l_ModbusBasicArgs.iInnerQuitCmd = SERVICE_EXIT_NONE;
	l_ModbusBasicArgs.bFrameDone = TRUE;

	/* register the MODBUS basic agrs */
	pArgs->pReserved = &l_ModbusBasicArgs;

#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread Modbus Service was created, Thread Id = %d.\n", gettid());
#endif	
    // 4.create PackData thread 

	hPackDataThread = RunThread_Create("MODBUS_PACK",
		(RUN_THREAD_START_PROC)Modbus_PackData,
		&l_ModbusBasicArgs,
		NULL,
		0);

	l_ModbusBasicArgs.hThreadID[2] = hPackDataThread;

	if (hPackDataThread == NULL)
	{
		pLogText = "Create PackData thread failed.";

		AppLogOut(LOG_INIT_MODBUS_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
		TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);

		RoutineBeforeExitService(&l_ModbusBasicArgs);
		return SERVICE_EXIT_FAIL;
	}
	else
	{
		TRACE("Created PackData thread succesfully!!");
	}

	Sleep(50);//yield
	
#ifdef _CODE_FOR_MINI
	/* 5.create LinkLayer thread */
	//added by kenn, in order to avoid sample and communication by one Rs485 at the same time, Mini controller's requirement
        MODBUS_COMMON_CONFIG *pOriCfg;
        pOriCfg = &g_ModbusGlobals.CommonConfig;
        unsigned int            iMediaType;
        int             iBufLen = 0;
        int             iSystemID = 1 ;
        int             iVarSubID;
        VAR_VALUE_EX value;
	value.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
       value.nSenderType   = SERVICE_OF_LOGIC_CONTROL;
       value.pszSenderName = "Modbus";

        iMediaType = pOriCfg->iMediaType;
        char                    szCommPortParam[64];
        memcpy(szCommPortParam, pOriCfg->szCommPortParam, 64);
        TRACE("pOriCfg->iMediaType = %d, szCommPortParam = %s\n",  pOriCfg->iMediaType, szCommPortParam);
        if((pOriCfg->iMediaType ==MODBUS_MEDIA_TYPE_RS485) & g_SiteInfo.bLoadAllEquip)
        {
                //Generate a warning message for user, let user know we only one RS-485 port, we can't do this at the same time
                iBufLen = sizeof(VAR_VALUE_EX);
                iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 250);
                value.varValue.ulValue  = 1;

                DxiSetData(VAR_A_SIGNAL_VALUE,
                                iSystemID,
                                iVarSubID,
                                iBufLen,
                                &value,
                                10000);
		pLogText = "Modbus config failed,it can't transfer data by rs485.";

		AppLogOut(LOG_INIT_MODBUS_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
		TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);
		
        }
        else
        {
                //Dismiss a warning message for user
                iBufLen = sizeof(VAR_VALUE_EX);
                iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 250);
                value.varValue.enumValue  = 0;

                DxiSetData(VAR_A_SIGNAL_VALUE,
                                iSystemID,
                                iVarSubID,
                                iBufLen,
                                &value,
                                10000);
								
#endif								
								
	hLinklayerThread = RunThread_Create("MODBUS_LINK",
			(RUN_THREAD_START_PROC)Modbus_LinkLayerManager,
			&l_ModbusBasicArgs,
			NULL,
			0);

		/* assign the reference out of the Thread to insure valid */
	l_ModbusBasicArgs.hThreadID[1] = hLinklayerThread;

	if (hLinklayerThread == NULL)
	{
		pLogText = "Create link-layer thread failed.";

		AppLogOut(LOG_INIT_MODBUS_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
		TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);

		RoutineBeforeExitService(&l_ModbusBasicArgs);
		return SERVICE_EXIT_FAIL;
	}
	if(MODBUS_AppService != NULL)
	{
		MODBUS_AppService->bReadyforNextService = TRUE;
	}
	
#ifdef _CODE_FOR_MINI
		}
#endif

	/* 5.begin main state machine loop */
	TRACE_MODBUS_TIPS("Begin state machine");
	dwExitCode = StateMachineLoop(&l_ModbusBasicArgs);


	/* 6.do the clean work */
	RoutineBeforeExitService(&l_ModbusBasicArgs);

	return dwExitCode;
}


/*==========================================================================*
 * FUNCTION : CheckUserConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID    : 
 *            MODBUS_COMMON_CONFIG  *pUserCfg : 
 *            MODBUS_COMMON_CONFIG  *pOriCfg  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 16:39
 *==========================================================================*/
static BOOL CheckUserConfig(int nVarID, MODBUS_COMMON_CONFIG *pUserCfg,
							MODBUS_COMMON_CONFIG *pOriCfg)
{
	/* prepare for check accordance between config items */
	if (!(nVarID & MODBUS_CFG_PROTOCOL_TYPE))
	{
		pUserCfg->iProtocolType = pOriCfg->iProtocolType;
	}

	if (!(nVarID & MODBUS_CFG_ADR))
	{
		pUserCfg->byADR = pOriCfg->byADR;
	}

	if (!(nVarID & MODBUS_CFG_MEDIA_TYPE))
	{
		pUserCfg->iMediaType = pOriCfg->iMediaType;
	}

	if (!(nVarID & MODBUS_CFG_MEDIA_PORT_PARAM))
	{
		strcpy(pUserCfg->szCommPortParam, pOriCfg->szCommPortParam);
	}

	
	/* 1.check protocol type and the accordance */
	
	if (nVarID & MODBUS_CFG_ALL || nVarID & MODBUS_CFG_PROTOCOL_TYPE)
	{
		if (pUserCfg->iProtocolType < EEM_MODBUS || 
			pUserCfg->iProtocolType >= MODBUS_PROTOCOL_NUM)
		{
			return FALSE;
		}
	}

   //TRACE("\npUserCfg4->byADR is: %d\n", pUserCfg->byADR);
  // TRACE("\nnVarID is: %d\n", nVarID);

	/* 2.check ADR -- always true due to data type */
	if (nVarID & MODBUS_CFG_ALL || nVarID & MODBUS_CFG_ADR)
	{
		if (pUserCfg->byADR < 1 || pUserCfg->byADR > 255)
		{

			return FALSE;
		}

	}

	/* 4.chech operation media and comm port param */
	if (nVarID & MODBUS_CFG_ALL || nVarID & MODBUS_CFG_MEDIA_TYPE || 
		  nVarID & MODBUS_CFG_MEDIA_PORT_PARAM)
	{
		if (pUserCfg->iMediaType < MODBUS_MEDIA_TYPE_LEASED_LINE || 
			pUserCfg->iMediaType >= MODBUS_MEDIA_TYPE_NUM)
		{
			return FALSE;
		}
		
#ifdef _CODE_FOR_MINI		
	int		iBufLen = 0;
	int		iSystemID = 1 ;
	int		iVarSubID;
	VAR_VALUE_EX value;
	value.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
       	value.nSenderType   = SERVICE_OF_LOGIC_CONTROL;
       	value.pszSenderName = "Modbus";
#endif

		switch (pUserCfg->iMediaType)
		{
		case MODBUS_MEDIA_TYPE_LEASED_LINE:
		case MODBUS_MEDIA_TYPE_RS485:

#ifdef _CODE_FOR_MINI
			if((pUserCfg->iMediaType == MODBUS_MEDIA_TYPE_RS485) & g_SiteInfo.bLoadAllEquip)
			{
				//return false because we only one RS-485 port, we can't do this at the same time
				return FALSE;
			}

			if((pUserCfg->iMediaType == MODBUS_MEDIA_TYPE_LEASED_LINE) & g_SiteInfo.bLoadAllEquip)
			{
				//Dismiss a warning message for user
				iBufLen = sizeof(VAR_VALUE_EX);
				iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 250);
				value.varValue.ulValue  = 0;
			

				DxiSetData(VAR_A_SIGNAL_VALUE,
						iSystemID,			
						iVarSubID,		
						iBufLen,			
						&value,			
						10000);
			}
#endif				
				
			if ((!Modbus_CheckBautrateCfg(pUserCfg->szCommPortParam))||(!Modbus_CheckSPortParam(pUserCfg->szCommPortParam)))
			{
				return FALSE;
			}
			break;	

		case MODBUS_MEDIA_TYPE_TCPIP:
		
#ifdef _CODE_FOR_MINI		
			if( g_SiteInfo.bLoadAllEquip)
			{
				//Dismiss a warning message for user
				iBufLen = sizeof(VAR_VALUE_EX);
				iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 250);
				value.varValue.ulValue  = 0;
			

				DxiSetData(VAR_A_SIGNAL_VALUE,
						iSystemID,			
						iVarSubID,		
						iBufLen,			
						&value,			
						10000);
			}
#endif			

			if (!Modbus_CheckIPAddress(pUserCfg->szCommPortParam))
			{
				return FALSE;
			}
		}
	}

	return TRUE;
}


/* for copy MODBUS_COMMON_CONFIG structure, used as inline function */
#define MODBUS_COPY_COMMON_CONFIG(_src, _dst) \
   { \
   (_dst).byADR = (_src).byADR;  \
   (_dst).iProtocolType = (_src).iProtocolType;  \
   (_dst).iMediaType = (_src).iMediaType;  \
   strcpy((_dst).szCommPortParam, (_src).szCommPortParam);  \
   }

/*==========================================================================*
 * FUNCTION : UpdateNormalCfgItems
 * PURPOSE  : assistant function for ServiceConfig
 * CALLS    : 
 * CALLED BY: ServiceConfig
 * ARGUMENTS: int            nVarID    : 
 *            MODBUS_COMMON_CONFIG  *pOriCfg  : 
 *            MODBUS_COMMON_CONFIG  *pUserCfg : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 16:27
 *==========================================================================*/
static void UpdateNormalCfgItems(int nVarID,
								 MODBUS_COMMON_CONFIG *pOriCfg, 
								 MODBUS_COMMON_CONFIG *pUserCfg)
{
	if (nVarID & MODBUS_CFG_ALL)
	{
		MODBUS_COPY_COMMON_CONFIG(*pUserCfg, *pOriCfg);
		return;
	}

	if (nVarID & MODBUS_CFG_ADR)
	{
		pOriCfg->byADR = pUserCfg->byADR;
	}


	return;
}


/*==========================================================================*
 * FUNCTION : WriteModbusCfgToFlash
 * PURPOSE  : 
 * CALLS    : MODBUS_GetModifiedFileBuf
 * CALLED BY: 
 * ARGUMENTS: int            nVarID : 
 *            MODBUS_COMMON_CONFIG  *pCfg  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-26 17:23
 *==========================================================================*/
/* used interface defined in config_builder.c */
BOOL Modbus_GetModifiedFileBuf(int nVarID, 
							MODBUS_COMMON_CONFIG *pUserCfg,
							char **pszOutFile);

BOOL Modbus_UpdateCommonConfigFile(const char *szContent);

static BOOL WriteModbusCfgToFlash(int nVarID, MODBUS_COMMON_CONFIG *pUserCfg)
{
	HANDLE hMutex;
	char *szOutFile;
	char *pLogText;

	/* to simplify log action */
#define TASK_NAME        "Modify Modbus Config File"
#define LOG_MODIFY_FILE(_pLogText)  \
	(AppLogOut(TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	hMutex = DXI_GetESRCommonCfgMutex();

	if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_COMCFG) == ERR_MUTEX_OK)
	{
		/* create new file in memory first */
		if (!Modbus_GetModifiedFileBuf(nVarID, pUserCfg, &szOutFile))
		{
			pLogText = "Failed to create new Modbus common config file in memory.";
			LOG_MODIFY_FILE(pLogText);

			DELETE(szOutFile);
			Mutex_Unlock(hMutex);
			return FALSE;
		}

		/* write to flash */
		if (!Modbus_UpdateCommonConfigFile(szOutFile))
		{
			pLogText = "Write the new Modbus common config file to flash failed.";
			LOG_MODIFY_FILE(pLogText);

			DELETE(szOutFile);
			Mutex_Unlock(hMutex);
			return FALSE;
		}
	}
	else
	{
		pLogText = "Wait for Mutex of Common Config file timeout.";
		LOG_MODIFY_FILE(pLogText);

		return FALSE;
	}


	DELETE(szOutFile);
	Mutex_Unlock(hMutex);
	return TRUE;
}

__INLINE static BOOL MODBUS_IsCommBusy(MODBUS_BASIC_ARGS *pThis)
{
	BOOL bRet = pThis->bCommRunning;

	/* for leased line, when stay in MODBUS_IDLE or SOC_OFF as Server Mode, we 
	 * think communication is not running. 
	 */
	if ((g_ModbusGlobals.CommonConfig.iMediaType == MODBUS_MEDIA_TYPE_LEASED_LINE)||
	    (g_ModbusGlobals.CommonConfig.iMediaType == MODBUS_MEDIA_TYPE_RS485))
	{
		if (!pThis->iMachineState && pThis->iOperationMode == MODBUS_MODE_SERVER)
		{
			bRet = FALSE;
		}
	}
	
    TRACE("pThis->bCommRunning is %d\n", pThis->bCommRunning);
	return bRet;
}


/*==========================================================================*
 * FUNCTION : Modbus_ModifyCommonCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ServiceConfig
 * ARGUMENTS: BOOL bServiceIsRunning    :
 *			  BOOL bByForce				: ignore CommBusy restriction
 *			  MODBUS_BASIC_ARGS  *pThis    : 
 *            int             iItemID   : 
 *            MODBUS_COMMON_CONFIG   *pUserCfg : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-10 15:53
 *==========================================================================*/
int Modbus_ModifyCommonCfg(BOOL bServiceIsRunning,
						BOOL bByForce,
						MODBUS_BASIC_ARGS *pThis,
						int iItemID,
						MODBUS_COMMON_CONFIG *pUserCfg)
{
	MODBUS_COMMON_CONFIG *pOriCfg;
	HANDLE hLinklayerThread;

	unsigned int	    byADR;                
	unsigned int		iProtocolType;	/* see enum MODBUS_PROTOCOL definition */
	unsigned int		iMediaType;	    /* see enum MODBUS_MEDIA_TYPE definition */
	char				szCommPortParam[MODBUS_COMM_PORT_PARAM_LEN];



	/* phone number for alarm report*/
	

	char *pLogText;

	/* original common config */
	pOriCfg = &g_ModbusGlobals.CommonConfig;

    byADR = pOriCfg->byADR;
    iProtocolType = pOriCfg->iProtocolType;
    iMediaType = pOriCfg->iMediaType;
    memcpy(szCommPortParam, pOriCfg->szCommPortParam, MODBUS_COMM_PORT_PARAM_LEN);
    TRACE("pOriCfg->iMediaType = %d, szCommPortParam = %s\n",  pOriCfg->iMediaType, szCommPortParam);
    TRACE("-------iItemID = %d-----------\n", iItemID);
   /* bReportInUse = pOriCfg->bReportInUse;
    iMaxAttempts = pOriCfg->iMaxAttempts;
    iAttemptElapse = pOriCfg->iAttemptElapse;
    memcpy(szAlarmReportPhoneNumber[0], pOriCfg->szAlarmReportPhoneNumber[0], PHONENUMBER_LEN);
    memcpy(szAlarmReportPhoneNumber[1], pOriCfg->szAlarmReportPhoneNumber[1], PHONENUMBER_LEN);
    memcpy(szAlarmReportPhoneNumber[2], pOriCfg->szAlarmReportPhoneNumber[2], PHONENUMBER_LEN);*/

	
	/* read config */
	if (!(iItemID & MODBUS_CFG_W_MODE))  
	{
		MODBUS_COPY_COMMON_CONFIG(*pOriCfg, *pUserCfg);

		return ERR_SERVICE_CFG_OK;
	}

	//TRACE("pUserCfg->iAttemptElapse is %d\n", pUserCfg->iAttemptElapse);
	/* update config */
	if (!bByForce && MODBUS_IsCommBusy(pThis))
	{

		return ERR_SERVICE_CFG_BUSY;
	}
  
	/* check data first */
	if (!CheckUserConfig(iItemID, pUserCfg, pOriCfg))
	{

		return ERR_SERVICE_CFG_DATA;
	}
	
	/* check if the non-shared com has been occupied already */
#ifdef COM_SHARE_SERVICE_SWITCH
	{
		HANDLE hMutex;
		SERVICE_SWITCH_FLAG *pFlag;

		/* note: MediaType value of pUserCfg has been updated 
		in CheckUserConfig function */
		if (iItemID & MODBUS_CFG_MEDIA_TYPE || iItemID & MODBUS_CFG_ALL ||
			iItemID & MODBUS_CFG_MEDIA_PORT_PARAM)
		{
			pFlag = DXI_GetServiceSwitchFlag();

			if (MODBUS_IS_SHARE_COM(pUserCfg->iMediaType))
			{
				hMutex = DXI_GetServiceSwitchMutex();
				if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_SWITCH) == ERR_MUTEX_OK)
				{
					if (pFlag->bMtnCOMHasClient)
					{
						Mutex_Unlock(hMutex);
						return ERR_SERVICE_CFG_MODBUS_COMUSED;
					}
					else
					{
						pFlag->bHLMSUseCOM = TRUE;
						Mutex_Unlock(hMutex);
					}
				}
				else
				{
					return ERR_SERVICE_CFG_MODBUS_COMUSED;
				}
			}
			else
			{
				pFlag->bHLMSUseCOM = FALSE;
			}
		}

	}
#endif //COM_SHARE_SERVICE_SWITCH
	/* write to flash then */
	/*
	TRACE("pUserCfg->byADR is %d\n", pUserCfg->byADR);
	TRACE("pUserCfg->iProtocolType is %d\n", pUserCfg->iProtocolType);
	TRACE("pUserCfg->iMediaType is %d\n", pUserCfg->iMediaType);
	TRACE("pUserCfg->szCommPortParam is %s\n", pUserCfg->szCommPortParam);
   */
	if (!WriteModbusCfgToFlash(iItemID, pUserCfg))
	{
		return ERR_SERVICE_CFG_FLASH;
	}
	/* now process in the memory */
	if (bServiceIsRunning)
	{
        /* Address updated */
		if ((iItemID & MODBUS_CFG_ADR) ||
			(iItemID & MODBUS_CFG_ALL))
		{
			/* changed */
			if (pOriCfg->byADR != pUserCfg->byADR)
			{
				pOriCfg->byADR = pUserCfg->byADR;
				pThis->bADRChangedFlag = TRUE;
			}
			
		}

		/* protocol type updated */
		if ((iItemID & MODBUS_CFG_PROTOCOL_TYPE) ||
			(iItemID & MODBUS_CFG_ALL))
		{
			/* changed */
			if (pOriCfg->iProtocolType != pUserCfg->iProtocolType)
			{
				pOriCfg->iProtocolType = pUserCfg->iProtocolType;
				pThis->bProtocolChangedFlag = TRUE;
			}
		}

		/* link-layer media updated */
		
				
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;

			//Sleep(3000);
			/* wait 20s */
			if(pThis->hThreadID[1] != NULL)
			{
				RunThread_Stop(pThis->hThreadID[1], 20000, TRUE);
			}
			/* update the value */
			if (iItemID & MODBUS_CFG_MEDIA_TYPE)
			{
				pOriCfg->iMediaType = pUserCfg->iMediaType;
			}
			if (iItemID & MODBUS_CFG_MEDIA_PORT_PARAM)
			{
				strcpy(pOriCfg->szCommPortParam, pUserCfg->szCommPortParam);
			}
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;
				/* restart link-layer thread */
				
			hLinklayerThread = RunThread_Create("MODBUS_LINK",
					(RUN_THREAD_START_PROC)Modbus_LinkLayerManager,
					pThis,
					NULL,
					0);
			if (hLinklayerThread == NULL)
			{
				pLogText = "Create link-layer thread failed.";
	
				AppLogOut(LOG_INIT_MODBUS_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
				TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);
	
				pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
				return ERR_SERVICE_CFG_EXIT;
			}
				

				pThis->hThreadID[1] = hLinklayerThread;
				
		
		/* for other config item, just modify values */
		UpdateNormalCfgItems(iItemID, pOriCfg, pUserCfg);
	}

    char logText[MODBUS_LOG_TEXT_LEN];

    TRACE("iItemID is %d [%d]\n", iItemID, bServiceIsRunning);

    if(iItemID & MODBUS_CFG_ADR)
    {
        sprintf(logText, "Modify Modbus Address from %d to %d%s%s", byADR, pUserCfg->byADR, " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_MODBUS_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', MODBUS_LOG_TEXT_LEN);
    }
    if(iItemID & MODBUS_CFG_PROTOCOL_TYPE)
    {
	    int iProtocolNum,iUserProtocolNum;
	    if((iProtocolType>0)&&(iProtocolType<6))
	    {
		    iProtocolNum =iProtocolType-1;
	    }
	    else
	    {
		    iProtocolNum = MODBUS-1;
	    }
	    if((pUserCfg->iProtocolType>0)&&(pUserCfg->iProtocolType<6))
	    {
		    iUserProtocolNum =pUserCfg->iProtocolType-1;
	    }
	    else
	    {
		    iUserProtocolNum = MODBUS-1;
	    }
	    char protocolType[5][20] = { {"EEM"}, {"RSOC"}, {"SOC_TPE"}, {"YDN23"}, {"MODBUS"}};
	    
        sprintf(logText, "Modify Protocol Type from %s to %s%s%s", protocolType[iProtocolNum], 
        	protocolType[iUserProtocolNum], " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_MODBUS_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', MODBUS_LOG_TEXT_LEN);
    }
    if(iItemID & MODBUS_CFG_MEDIA_TYPE)
    {
    
	   char mediaType[3][20] = {{"LEASED_LINE"}, {"MODEM"}, {"Ethernet"}, {"RS485"}};
	    
        sprintf(logText, "Modify Media Type from %s to %s%s%s", mediaType[iMediaType],
        	mediaType[pUserCfg->iMediaType], " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_MODBUS_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', MODBUS_LOG_TEXT_LEN);
    }
    if(iItemID & MODBUS_CFG_MEDIA_PORT_PARAM)
    {
        sprintf(logText, "Modify Media Port Parameter from %s to %s%s%s", szCommPortParam, 
        	pUserCfg->szCommPortParam, " by ", pUserCfg->cModifyUser);
        
    	AppLogOut(LOG_MODIFY_MODBUS_COMMON_CONFIG, APP_LOG_INFO, "%s\n", logText);
    	memset(logText, '\0', MODBUS_LOG_TEXT_LEN);
    }
  
	return ERR_SERVICE_CFG_OK;

}


/*==========================================================================*
 * FUNCTION : ServiceConfig
 * PURPOSE  : for MODBUS common config info access
 * CALLS    : Modbus_ModifyCommonCfg
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService : 
 *            SERVICE_ARGUMENTS * pArgs:
 *            int     nVarID   : 
 *            int    *nBufLen  : 
 *            void   *pDataBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HanTao                   DATE: 2006-06-22 16:37
 *==========================================================================*/
int ServiceConfig(HANDLE hService, 
				  BOOL bServiceIsRunning,
				  SERVICE_ARGUMENTS *pArgs, 
				  int nVarID,
				  int *nBufLen, 
				  void *pDataBuf)
{
	UNUSED(hService);
	UNUSED(nBufLen);

	int iRtn = 0;
	iRtn = Modbus_ModifyCommonCfg(bServiceIsRunning,
		   FALSE,
		   (MODBUS_BASIC_ARGS *)pArgs->pReserved,
		   nVarID, (MODBUS_COMMON_CONFIG *)pDataBuf);
	
	return iRtn;
}

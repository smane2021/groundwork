
#include "SMS.h"

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <locale.h>

#include "../include/new.h"
//#include <iconv.h>

#include "../include/cfg_model.h"


#define _SMS_DEBUG  0

#define SMS_RECIVE_ENABLE	0	    //预留接收功能

#define CONFIG_FILE_SMS			"private/sms/SMS_private.cfg"		//私有配置文件名

#define MODEM_DEVICE_NAME1		"/dev/ttyusb0"  
#define MODEM_DEVICE_NAME2		"/dev/ttyusb1"
#define MODEM_BAUD_RATE			115200


#define __CFG_SIG_NAME_TO_UPPER

#define		XMDEM_SIZE			133
#define		RECV_NUM			512

#define SIG_FULL_NAME_MAX_LEN	33

#define MAX_PENDING_SMS_SEND	    64

#define SMS_TIMER_ID 88

#define IOC_SET_USBPW	 _IOW(DIDO_MAJOR, 3, unsigned long)


#define	DEBUG_SMS_INFO(Info) \
    TRACE("****SMS**** %s.\n", Info)

#define	DEBUG_SMS_INFO_STRING(Info, StringVar) \
    TRACE("****SMS**** %s %s.\n", Info, StringVar)

#define DEBUG_SMS_FILE_FUN_LINE_STRING(StringVar) \
    TRACE("****SMS**** [%s]-[%s]-[%d]: %s.\n", \
    __FILE__, __FUNCTION__, __LINE__, StringVar)

#define DEBUG_SMS_FILE_FUN_LINE_INT(VarName, VarValue) \
    TRACE("****SMS**** [%s]-[%s]-[%d]: %s = %d.\n", \
    __FILE__, __FUNCTION__, __LINE__, VarName, VarValue)

#define DEBUG_SMS_FILE_FUN_LINE_HEX(VarName, VarValue) \
    TRACE("****SMS**** [%s]-[%s]-[%d]: %s = 0x%X.\n", \
    __FILE__, __FUNCTION__, __LINE__, VarName, VarValue)

#define	STRING_EQU(str1, str2)	(strcmp(str1, str2) == 0)
#define SMS_HEART_BEAT		{RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);}

#define TOUPPER(c)	(c >= 'a' && c <= 'z' ? c - ('a' - 'A') : c)
#define TOLOWER(c)	(c >= 'A' && c <= 'Z' ? c + ('a' - 'A') : c)
#define SWAP_BYTE(by)	(((by) >> 4) + ((by) << 4))			//把一个字节中的高4位与低4位互换

#define DELETE_ITEM(p) if ((p) != NULL) \
    { DELETE(p);  (p) = NULL; }

typedef struct tagSMS_TIME_STAMP
{
	BYTE	byYear;		// 03 = 2003
	BYTE	byMonth;	// 01 = Jan.
	BYTE	byDate;

	BYTE	byHour;
	BYTE	byMinute;
	BYTE	bySecond;

	BYTE	byTimeZone;
}SMS_TIME_STAMP;

typedef union tagSMS_PDU_VP
{
	SMS_TIME_STAMP	stTimeStamp;
	BYTE			byRelativeTime;
}SMS_PDU_VP;

typedef struct tagPDU_FRAME
{
	BYTE	bySMSCenterLen;					//	短信息中心地址长度
	BYTE	bySMSCenterType;				//	短信息中心号码类型
	char	abySMSCenterNo[PHONE_NO_STRING_LEN];	//	短信息中心号码,字符串格式,8613812345678
	BYTE	byBasePara;						//	基本参数
	BYTE	byMsgReference;					//	消息基准值
	BYTE	byDesPhoneNoLen;				//	被叫号码长度
	BYTE	byDesPhoneNoType;				//	被叫号码类型
	char	abyDesPhoneNo[PHONE_NO_STRING_LEN];	//	被叫号码,字符串格式
	BYTE	byProtocolID;					//	协议标识
	BYTE	byDataCodingScheme;				//	数据编码方案

	SMS_PDU_VP	unValidityPeriod;			//	有效期

	BYTE	byUserDataLength;				//	用户数据长度
	BYTE	abyUserData[TP_UD_MAX_LEN + 1];	//	用户数据+NULL
}PDU_FRAME;


// 总的配置文件
typedef struct tagSMS_PRIVATE_CONFIG		//	SMS私有配置文件
{
	//SMS_EQUIP_LIST	stEquipList;
	SMS_GET_SET_CMD_LIST	stGetSetCmdList;
	SMS_READ_CMD_LIST		stReadCmdList;
	
	SMS_SMS_LIST	stSmsList;
	SMS_ALARM_LIST	stAlarmList;
} SMS_PRIVATE_CONFIG;


//全局变量
typedef struct tagSMSGlobals
{
	//SMS私有配置文件
	SMS_PRIVATE_CONFIG	SMSPrivateConfig;

	//Handle of This Service thread
	HANDLE			hThreadSelf;

	//HANDLE		hCommPort;
	int			fdUsbModem;

	//命令
	//SMS_COMMAND		stCommand;		

	//接收的SMS信息
	PDU_FRAME		RecvPduFrame;
	PDU_FRAME		SendPduFrame;


	int				iModemType;
	//	BYTE			acRecvPhoneNo[PHONE_NO_STRING_LEN];		//接收短信的手机号码,仅用于CDMA方式

	//	BOOL			bReply;						//是回复还是上报

	BOOL			bHasModem;
	BOOL			bMMSModem;				//是否为彩信MODEM


	HANDLE			hTrapSendQueue;	  /* trap send queue*/
	ULONG			ulAlarmSequenceID;
	int			nTrapLevel;    // The receiving trap level of sms
	int			nSendFailCnt;   //发送失败次数
	int			nSendActionFlag;  //本次循环中有无执行过发送动作 0-没有执行过动作 1-执行过动作

	char			cPhoneNumber[3][PHONE_NO_STRING_LEN];

} SMS_GLOBALS; 


SMS_GLOBALS g_SMSGlobals;

//The extend alarm signal value
//Include the active or deactive status of alarm
struct tagAlarmSigValueExt
{
    int				nAlarmMsgType;
    ALARM_SIG_VALUE	stAlarmSigValue;

    ULONG				ulAlarmSequenceID;
};
typedef struct tagAlarmSigValueExt ALARM_SIG_VALUE_EXT;


static int ParseSmsAlarmProc(char *szBuf, char *pData);
static int LoadSMSCfgProc(void *pCfg, void *pLoadToBuf);

static BOOL EquipIsExistence(int iEquipId);
static int FindSmsIdxByNo(SMS_SMS_LIST *pSmsList, int iSmsNo);
static int GetCmdIdx(void *pCmdList, char *pcCmdInfo, int iCmdCode);
static int GetRealTimeAlarmData(OUT ALARM_SIG_VALUE **ppSigValue);
static int GetSignalName(int iEquipID, int iSigType, int iSigId, OUT char* pcSignalName);
static void HandleErrorCmd(SMS_COMMAND *pSmsCmd);
static void HandleGetSetCmd(SMS_COMMAND *pSmsCmd, BOOL bGet);
static void HandleHelpCmd(SMS_COMMAND *pSmsCmd);
static void HandleReadCmd(SMS_COMMAND *pSmsCmd);
static void HandleSpecialGetSetCmd(BOOL bGet, int iEquipNo, int iCmdNo, SMS_COMMAND *pSmsCmd);
static void HandleSpecialReadCmd(int iCmdType, SMS_COMMAND *pSmsCmd);
static void MakeAlarmInfo(ALARM_SIG_VALUE	*pAlarmSigValue, OUT char *pBuf);
static int MakeGetSetSmsInfo(BOOL bGet, int iFrom, int iTo, OUT char *pBuf);
static int MakeReadSmsInfo(int iFrom, int iTo, OUT char *pBuf);
static BOOL MakeSmsInfoByNo(int iSmsNo, OUT char *pBuf);
static void PrintNetInfo(OUT char *pszRespond);
static void SendGetSetHelpInfo(SMS_COMMAND *pSmsCmd, BOOL bGet);
static void SendReadHelpInfo(SMS_COMMAND *pSmsCmd);
static void SMS_CommandHandle(SMS_COMMAND *pSMSCmd);
static void SMS_ErrorCommandHandle(SMS_COMMAND *pSmsCmd, char *pUserData, int iUnpackResult);
static BOOL DeleteAllMsg(void);
static int  MyWrite(int fd,  BYTE* pBuffer, int nBytesToWrite);
static int  SMS_ModemPortRead(IN int fd, OUT BYTE* pBuffer, IN int nBytesToRead);
static BOOL SMS_RecvShortMsg(char *pcFrom, char* pcUserData);
BOOL SMS_SendCmd(char* pSendBuf, char* pReadBuf);
static BOOL SMS_SendShortMsg(char *pszTo, char *pszData);
static int LoadSMSCfgProc(void *pCfg, void *pLoadToBuf);
static int ParseGetSetCmdProc(char *szBuf, char *pData);
static int ParseReadCmdProc(char *szBuf, char *pData);
static int ParseSmsAlarmProc(char *szBuf, char *pData);
static int ParseSmsInfoProc(char *szBuf, char *pData);
int SMS_ReadPrivateCfg(void);

static BOOL detectMODEM(void);
static void	ExitSMS(void);
//static void GetCfgPhoneNum(IN int iIdx, OUT char *pszPhoneNum);
static void	InitSMS(void);
void rePowerUSB(void);
DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs);
static void SMS_Alarm_Trip(void);
static void SMS_CloseModemPort(IN int fd);
int SMS_InitMODEM(void);
static BOOL SMS_ModemPortDetect(void);
static int SMS_OpenModemPort(IN char *pDevName,IN int nBaudRate);
static int SMS_UnpackUserData(char *pUserData, SMS_COMMAND *pSMSCmd);
static void StringToUpper(char *pszString);
static void SMS_PrintCfgFile(void);

//in_addr_t inet_addr(const char *cp);
DWORD inet_addr(const char *cp);
/*=====================================================================*
* Function name: ConvertBaudrateToUnix
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
// the linux supported baud rates of serial
struct SUnixBaudList
{
	unsigned long ulUnixBaud;
	int     nNormalBaud;
};

static struct SUnixBaudList s_UnixBaudList[] =
{
	{ B50	  ,  50 },
	{ B75	  ,  75 },
	{ B110	  ,  110 },
	{ B134	  ,  134 },
	{ B150	  ,  150 },
	{ B200	  ,  200 },
	{ B300	  ,  300 },
	{ B600	  ,  600 },
	{ B1200	  ,  1200 },
	{ B1800	  ,  1800 },
	{ B2400	  ,  2400 },
	{ B4800	  ,  4800 },
	{ B9600	  ,  9600 },
	{ B19200  ,  19200 },
	{ B38400  ,  38400 },
	{ B57600  ,  57600 },
	{ B115200 ,  115200 },
	{ B230400 ,  230400 },
	{ B460800 ,  460800 },
	{ B500000 ,  500000 },
	{ B576000 ,  576000 },
	{ B921600 ,  921600 },
	{ B1000000,  1000000 },
	{ B1152000,  1152000 },
	{ B1500000,  1500000 },
	{ B2000000,  2000000 },
	{ B2500000,  2500000 },
	{ B3000000,  3000000 },
	{ B3500000,  3500000 },
	{ B4000000,  4000000 },
	{ 0,         0       } /* end flag */
};

static unsigned long ConvertBaudrateToUnix(int baud)
{
	struct SUnixBaudList    *p = s_UnixBaudList;

	while((p->nNormalBaud != baud) && (p->nNormalBaud != 0))
	{
		p ++;
	}

	return p->ulUnixBaud;
}



/***************************************************************************
*@brief   设置串口数据位，停止位和效验位
*@param  fd     类型  int  打开的串口文件句柄
*@param  databits 类型  int 数据位   取值 为 7 或者8
*@param  stopbits 类型  int 停止位   取值为 1 或者2
*@param  parity  类型  int  效验类型 取值为N,E,O,,S
*****************************************************************************/
static int set_Parity(int fd,int databits,int stopbits,int parity)
{ 
	struct termios options; 
	if  ( tcgetattr( fd,&options)  !=  0) { 
		perror("SetupSerial 1");     
		return(-1);  
	}
	options.c_cflag &= ~CSIZE; 


	switch (databits) /*设置数据位数*/
	{   
	case 7:		
		options.c_cflag |= CS7; 
		break;
	case 8:     
		options.c_cflag |= CS8;
		break;   
	default:    
		fprintf(stderr,"Unsupported data size\n"); return (-1);  
	}
	switch (parity) 
	{   
	case 'n':
	case 'N':    
		options.c_cflag &= ~PARENB;   /* Clear parity enable */
		options.c_iflag &= ~INPCK;     /* Clear parity checking */ 
		break;  
	case 'o':   
	case 'O':     
		options.c_cflag |= (PARODD | PARENB); /* 设置为奇效验*/  
		options.c_iflag |= INPCK;             /* enable parity checking */ 
		break;  
	case 'e':  
	case 'E':   
		options.c_cflag |= PARENB;     /* Enable parity */    
		options.c_cflag &= ~PARODD;   /* 转换为偶效验*/     
		options.c_iflag |= INPCK;       /* enable parity checking */
		break;
	case 'S': 
	case 's':  /*as no parity*/   
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;break;  
	default:   
		fprintf(stderr,"Unsupported parity\n");    
		return (-1);  
	}  
	/* 设置停止位*/  
	switch (stopbits)
	{   
	case 1:    
		options.c_cflag &= ~CSTOPB;  
		break;  
	case 2:    
		options.c_cflag |= CSTOPB;  
		break;
	default:    
		fprintf(stderr,"Unsupported stop bits\n");  
		return (-1); 
	} 
	/* Set input parity option */ /*have set it befor*/
	//if (parity != 'n')   
	//	options.c_iflag |= INPCK; 

	options.c_iflag=0;/*直接设置为0最方便；否则以前的设置会影响接收*/

	options.c_cflag     |= (CLOCAL | CREAD);
	options.c_lflag     &= ~(ICANON | ECHO | ECHOE | ISIG);
	options.c_oflag  &= ~OPOST;   /*Output*/

	tcflush(fd,TCIFLUSH);
	options.c_cc[VTIME] = 0;		/* 无超时,立即返回*/   
	options.c_cc[VMIN] = 0;		// 最少接收字节数
	if (tcsetattr(fd,TCSANOW,&options) != 0)     /* Update the options and do it NOW */
	{ 
		perror("Set Attrib");   
		return (-1);  
	} 
	return (0);  
}

/*=====================================================================*
* Function name: SMS_CloseModemPort
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void SMS_CloseModemPort(IN int fd)
{
	close(fd);
}
/*=====================================================================*
* Function name: SMS_OpenModemPort
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int SMS_OpenModemPort(IN char *pDevName,IN int nBaudRate)
{
	int		fd;	// the file descriptor

	fd = open(pDevName, O_RDWR | O_NOCTTY | O_NONBLOCK | O_EXCL); 
	if (fd < 0)
	{
		TRACE("Open device(%s) Fail, fd = %d.\n", pDevName, fd);
		return -1;
	}
	TRACE("Open device(%s) OK, fd = %d.\n", pDevName, fd);

	// set baud
	ULONG ulBaud = ConvertBaudrateToUnix(nBaudRate);

	struct termios   Opt;
	tcgetattr(fd, &Opt);

	tcflush(fd, TCIOFLUSH);
	cfsetispeed(&Opt, ulBaud);
	cfsetospeed(&Opt, ulBaud);

	if (tcsetattr(fd, TCSANOW, &Opt) != 0)
	{
		perror("tcsetattr");
		close(fd);
		return -1;
	}
	tcflush(fd,TCIOFLUSH);


	// set parity
	if(set_Parity(fd, 8, 1, 'N') < 0)
	{
		TRACE("SMS MODEM PORT:set parity fail.\n");
		close(fd);
		return -1;
	}

	return fd;
}
/*=====================================================================*
* Function name: SMS_ModemPortDetect
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static BOOL SMS_ModemPortDetect(void)
{
	// Send "AT" to detect online
	char szSend[AT_CMD_MAX_LEN] = {0};
	char szRead[AT_CMD_MAX_LEN] = {0};

	//Send "AT" to test
	sprintf(szSend, "%s%c", "AT", CHAR_CR);
	SMS_SendCmd(szSend, szRead);
	TRACE("SMS:echo of AT is: %s\n", szRead);

	//return (szRead[0] != NULL);
	if(szRead[0] != (char)NULL)
	{
	    return TRUE;
	}
	else
	{
	    return FALSE;
	}
}
/*=====================================================================*
* Function name: SMS_InitMODEM
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
int SMS_InitMODEM(void)
{

	if(!g_SMSGlobals.bHasModem)
	{
		return 0;
	}

	char szSend[AT_CMD_MAX_LEN];
	char szRead[AT_CMD_MAX_LEN];

	//Send "AT" to test
	SMS_ModemPortDetect();

	//设置回显为ON:1;  OFF:0
	sprintf(szSend, "%s%c", "ATE0", CHAR_CR);
	SMS_SendCmd(szSend, szRead);

	//设置波特率
	sprintf(szSend, "%s%c", "AT+IPR=115200", CHAR_CR);
	SMS_SendCmd(szSend, szRead);


	//设置新短消息通知
	sprintf(szSend, "%s%c", "AT+CNMI=2,1,0,0,0", CHAR_CR);
	SMS_SendCmd(szSend, szRead);

	//设置模式:0-PDU;1-文本
	sprintf(szSend, "%s%c", "AT+CMGF=1", CHAR_CR);
	SMS_SendCmd(szSend, szRead);

	//设置网络运营商格式
	sprintf(szSend, "%s%c", "AT+COPS=3,1", CHAR_CR);
	SMS_SendCmd(szSend, szRead);

	//获取网络运营商
	sprintf(szSend, "%s%c", "AT+COPS?", CHAR_CR);
	SMS_SendCmd(szSend, szRead);
	TRACE("SMS:echo of AT+COPS? is: %s.\n", szRead);
	//+COPS: 0,1,"CMCC"

	char *pReadBuf, *pCops;
	pReadBuf = szRead;

	pReadBuf = Cfg_SplitStringEx(pReadBuf, &pCops, '\"');
	pReadBuf = Cfg_SplitStringEx(pReadBuf, &pCops, '\"');

	if (STRING_EQU(pCops, "CMCC"))
	{
		TRACE("[SMS] Connect GSM MODEM.\n");
		g_SMSGlobals.iModemType = MODEM_TYPE_GSM;

		//设置为发送英文短信
		sprintf(szSend, "%s%c", "AT+WSCL=1,2", CHAR_CR);
		SMS_SendCmd(szSend, szRead);

	}
	else //if (STRING_EQU(pCops, "CDMA"))
	{
		TRACE("[SMS] Connect CDMA MODEM.\n");
		g_SMSGlobals.iModemType = MODEM_TYPE_CDMA;

		//设置为发送英文短信
		sprintf(szSend, "%s%c", "AT+WSCL=1,2", CHAR_CR);
		SMS_SendCmd(szSend, szRead);

	}
	//else
	//{
	//	g_SMSGlobals.iModemType = MODEM_TYPE_NONE;
	//	TRACE("SMS: Can NOT get COPS!\n");
	//	return -1;
	//}


	//删除所有短消息
	DeleteAllMsg();

	return 0;
}
/*=====================================================================*
* Function name: rePowerUSB
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
void rePowerUSB(void)
{

    if(g_SMSGlobals.bHasModem)
    {
	close(g_SMSGlobals.fdUsbModem);
    }

    g_SMSGlobals.bHasModem = FALSE;
    g_SMSGlobals.bMMSModem = FALSE;
    g_SMSGlobals.fdUsbModem = -1;
}

/*=====================================================================*
* Function name: detectMODEM
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static BOOL detectMODEM(void)
{
    char szSend[50];
    char szRead[50];


    BOOL Sendok = FALSE;

    int i = 0;
    while(i++ < 3)
    {
			sprintf(szSend, "%s%c", "AT", CHAR_CR);
			if(SMS_SendCmd(szSend, szRead) == FALSE)
			{
			    return FALSE;
			}
			printf("[SendShortMsg Cmd1:] %s\n", szSend);
			printf("[SendShortMsg Recv1:] %s\n", szRead);
			if(strstr(szRead, "OK") != NULL)
			{
			    Sendok = TRUE;
			    break;
			}
		
			Sleep(5000);//yield
		
			int nBytes = SMS_ModemPortRead(g_SMSGlobals.fdUsbModem, (BYTE *)szRead, 50);
			szRead[nBytes] = (char)NULL;
			RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);	
			printf("[SendShortMsg Recv1:] %s\n", szRead);
			if(strstr(szRead, "OK") != NULL)
			{
			    Sendok = TRUE;
			    break;
			}
		
			Sleep(10000);//yield
			nBytes = SMS_ModemPortRead(g_SMSGlobals.fdUsbModem, (BYTE *)szRead, 50);
			szRead[nBytes] = (char)NULL;
			RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);	
			printf("[SendShortMsg Recv1:] %s\n", szRead);
			if(strstr(szRead, "OK") != NULL)
			{
			    Sendok = TRUE;
			    break;
			}
			Sleep(20000);//yield
		
			nBytes = SMS_ModemPortRead(g_SMSGlobals.fdUsbModem, (BYTE *)szRead, 50);
			szRead[nBytes] = (char)NULL;
			RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);	
			printf("[SendShortMsg Recv1:] %s\n", szRead);
			if(strstr(szRead, "OK") != NULL)
			{
			    Sendok = TRUE;
			    break;
			}
    }

    if(Sendok !=TRUE)
    {
				return FALSE;
    }
    return TRUE;
}
/*=====================================================================*
* Function name: AddCurrentAlarmsToQueue
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/


static BOOL AddCurrentAlarmsToQueue(HANDLE hTrapSendQueue)
{
    int				nActiveAlarmNum = 0;
    ALARM_SIG_VALUE* pActiveAlarmSigValue = NULL;
    ALARM_SIG_VALUE* pAlarmSigValue = NULL;

    if (DXI_GetActiveAlarmItems(&nActiveAlarmNum, 
	&pActiveAlarmSigValue)
	== ERR_DXI_OK)
    {
	int i;

	ALARM_SIG_VALUE_EXT	stAlarmSigValue;

	pAlarmSigValue = pActiveAlarmSigValue;

	for (i = 0; i < nActiveAlarmNum; i++, pActiveAlarmSigValue++)
	{
	    memcpy(&(stAlarmSigValue.stAlarmSigValue),
		pActiveAlarmSigValue, 
		sizeof(ALARM_SIG_VALUE));

	    stAlarmSigValue.nAlarmMsgType = _ALARM_OCCUR_MASK;

	    g_SMSGlobals.ulAlarmSequenceID += 1;

	    stAlarmSigValue.ulAlarmSequenceID = 
		g_SMSGlobals.ulAlarmSequenceID;

	    //Put the report info to alarm queue
	    Queue_Put(hTrapSendQueue, &stAlarmSigValue, FALSE);

	}

	DELETE_ITEM(pAlarmSigValue);

	return TRUE;
    }

    return FALSE;
}
/*==========================================================================*
* FUNCTION : ServiceNotification
* PURPOSE  : Get the report alarm info and put it into queue
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hService        : 
*            int     nMsgType        : 
*            int     nTrapDataLength : 
*            void*   lpTrapData      : 
*            void*   lpParam         :  
*            BOOL    bUrgent         : 
* RETURN   : int : 0 for success, 1 for error(defined in DXI module)
* COMMENTS : 
* CREATOR  : HULONGWEN                   DATE: 2004-10-22 17:23
*==========================================================================*/
int ServiceNotification(HANDLE hService, 
			int nMsgType, 
			int nTrapDataLength, 
			void *lpTrapData,	
			void *lpParam, 
			BOOL bUrgent)
{
    UNUSED(hService);
    UNUSED(bUrgent);
    //int i = 0;

    if ((nMsgType & (_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK)) != 0)
    {
	//Because the trap send info include active or deactive status,
	//so extend the alarm signal value structure
	ALARM_SIG_VALUE_EXT	stAlarmSigValue;
	SMS_GLOBALS* pSnmpInfoStru = (SMS_GLOBALS*)lpParam;

	ASSERT(pSnmpInfoStru);

	if (nTrapDataLength != sizeof(ALARM_SIG_VALUE))
	{
	    return 0;
	}

	memcpy(&(stAlarmSigValue.stAlarmSigValue), lpTrapData, 
	    sizeof(ALARM_SIG_VALUE));

	stAlarmSigValue.nAlarmMsgType = nMsgType;

	pSnmpInfoStru->ulAlarmSequenceID += 1;

	stAlarmSigValue.ulAlarmSequenceID = pSnmpInfoStru->ulAlarmSequenceID;

	//Put the report info to alarm queue
	Queue_Put(pSnmpInfoStru->hTrapSendQueue, &stAlarmSigValue, FALSE);

	//i++;
	//printf("\nSMS Notification %d : %s \n", i, stAlarmSigValue.stAlarmSigValue.pStdSig->pSigName->pFullName[0]);
    }

    return 0;
}
/*=====================================================================*
* Function name: InitSMS
* Description  : 
* Arguments    : 
* Return type  : int 
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void	InitSMS(void)
{
    int			iRet;

	g_SMSGlobals.hThreadSelf = RunThread_GetId(NULL);	

	g_SMSGlobals.bHasModem = FALSE;
	g_SMSGlobals.bMMSModem = FALSE;
	g_SMSGlobals.nSendFailCnt = 0;
	g_SMSGlobals.nSendActionFlag = 0;

	//20130921 lkf 预留扩展功能用，暂不支持    
	//SMS_ReadPrivateCfg();

	memset(g_SMSGlobals.cPhoneNumber[0], 0, sizeof(g_SMSGlobals.cPhoneNumber[0]));
	memset(g_SMSGlobals.cPhoneNumber[1], 0, sizeof(g_SMSGlobals.cPhoneNumber[1]));
	memset(g_SMSGlobals.cPhoneNumber[2], 0, sizeof(g_SMSGlobals.cPhoneNumber[2]));


	/* register in the DXI module to receive Alarms */
	iRet = DxiRegisterNotification("SMS", 
	    ServiceNotification,
	    &g_SMSGlobals, // it shall be changed to the actual arg
	    _ALARM_OCCUR_MASK|_ALARM_CLEAR_MASK/*|_SAMPLE_MASK|_SET_MASK|_CONTROL_MASK*/, 
	    g_SMSGlobals.hThreadSelf,
	    _REGISTER_FLAG);

	if (iRet != 0)
	{
	    AppLogOut(SMS_TASK, APP_LOG_ERROR,
		"Fails on calling DxiRegisterNotification.\n");
	}

	// init the trap send queue
	g_SMSGlobals.hTrapSendQueue = Queue_Create(
	    MAX_PENDING_SMS_SEND,
	    sizeof(ALARM_SIG_VALUE_EXT),
	    0);

	if (g_SMSGlobals.hTrapSendQueue == NULL)
	{
	    AppLogOut(SMS_TASK, APP_LOG_ERROR, 
		"Fails on creating SMS Queue.\n");

	}

	if(g_SMSGlobals.hTrapSendQueue != NULL)
	{
	    AddCurrentAlarmsToQueue(g_SMSGlobals.hTrapSendQueue);
	}

	return;
}
/*=====================================================================*
* Function name: ExitSMS
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void	ExitSMS(void)
{
	int i;
	SMS_PRIVATE_CONFIG *pPriCfg = &g_SMSGlobals.SMSPrivateConfig;

	//SMS info
	for(i = 0; i < pPriCfg->stSmsList.iSmsNum; i++)
	{
		SAFELY_DELETE((pPriCfg->stSmsList.pSmsInfo + i)->pSigInfo);
	}
	SAFELY_DELETE(pPriCfg->stSmsList.pSmsInfo);

	//Get/Set command
	SAFELY_DELETE(pPriCfg->stGetSetCmdList.pGetSetCmdInfo);
	
	//Read command
	for(i = 0; i < pPriCfg->stReadCmdList.iCmdNum; i++)
	{
			SAFELY_DELETE((pPriCfg->stReadCmdList.pReadCmdInfo + i)->piSmsList);
	}	
	SAFELY_DELETE(pPriCfg->stReadCmdList.pReadCmdInfo);

	
	//Alarm list
	SAFELY_DELETE(pPriCfg->stAlarmList.pSmsAlarmInfo);


	//Comm Port
	if(g_SMSGlobals.fdUsbModem > 0)
	{
		TRACE("SMS: Close USB MODEM Comm Port.\n");

		SMS_CloseModemPort(g_SMSGlobals.fdUsbModem);
		g_SMSGlobals.fdUsbModem = -1;
		g_SMSGlobals.bHasModem = FALSE;
	}

	if (g_SMSGlobals.hTrapSendQueue != NULL)
	{
	    Queue_Destroy(g_SMSGlobals.hTrapSendQueue);
	}

	return;
}
/*=====================================================================*
* Function name: SMS_UnpackUserData
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int SMS_UnpackUserData(char *pUserData, SMS_COMMAND *pSMSCmd)
{
	// trim white char
	ASSERT(pUserData);
	ASSERT(pSMSCmd);

	//Except pSMSCmd->acPhoneNum
	//memset(pSMSCmd, 0, sizeof(*pSMSCmd));
	memset(pSMSCmd->acCmdCode, 0, SMS_CMD_CODE_MAX_LEN);
	//memset(pSMSCmd->acEquipName, 0, SMS_EQUIP_NAME_MAX_LEN);
	memset(pSMSCmd->acCmdName, 0, SMS_CMD_NAME_MAX_LEN);
	memset(pSMSCmd->acCmdPara, 0, SMS_CMD_PARA_MAX_LEN);

	char	*pField;

	////User Name
	//if(*pUserData == NULL)
	//{
	//	DEBUG_SMS_FILE_FUN_LINE_STRING("No User Name!");
	//	return FALSE;
	//}
	//pUserData = Cfg_SplitStringEx(pUserData, &pField, CHAR_COMMA);
	//strncpy(pSMSCmd->acUserName, pField, USERNAME_LEN);

	////Password
	//if(*pUserData == NULL)
	//{
	//	DEBUG_SMS_FILE_FUN_LINE_STRING("No Password!");
	//	return FALSE;
	//}
	//pUserData = Cfg_SplitStringEx(pUserData, &pField, CHAR_COMMA);
	//strncpy(pSMSCmd->acPassword, pField, PASSWORD_LEN);

	//Command Code
	if(*pUserData == (char)NULL)
	{
		DEBUG_SMS_FILE_FUN_LINE_STRING("No Command Code!");
		return ERR_SMS_NO_CMD_CODE;
	}
	
	pUserData = Cfg_SplitStringEx(pUserData, &pField, CHAR_COMMA);
	strncpy(pSMSCmd->acCmdCode, pField, SMS_CMD_CODE_MAX_LEN);
	// zzc modify start
	if((*pField == 'y') || (*pField == 'Y'))
	{
	    return ERR_SMS_OK;
	}
	// zzc modify end

	//EquipName  -- 预留项
	if(*pUserData != (char)NULL)
	{
		pUserData = Cfg_SplitStringEx(pUserData, &pField, CHAR_COMMA);
		strncpy(pSMSCmd->acEquipName, pField, SMS_EQUIP_NAME_MAX_LEN);
	}
	else
	{
		DEBUG_SMS_FILE_FUN_LINE_STRING("No Equip Name!");
		//return ERR_SMS_NO_EQUIP;
	}

	//Command No
	if(*pUserData != (char)NULL)
	{
		pUserData = Cfg_SplitStringEx(pUserData, &pField, CHAR_COMMA);
		strncpy(pSMSCmd->acCmdName, pField, SMS_CMD_NAME_MAX_LEN);
	}
	else
	{
		DEBUG_SMS_FILE_FUN_LINE_STRING("No Command No!");
		//return ERR_SMS_NO_CMD_NO;
	}

	//Parameter
	if(*pUserData != (char)NULL)
	{
		pUserData = Cfg_SplitStringEx(pUserData, &pField, CHAR_COMMA);
		strncpy(pSMSCmd->acCmdPara, pField, SMS_CMD_PARA_MAX_LEN);
	}

	return ERR_SMS_OK;
}
/*==========================================================================*
* FUNCTION : GetCfgPhoneNum
* PURPOSE  : 读取配置的手机号码，格式为13812345678,不包括+86
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int       iIdx         : 
*            OUT char  *pszPhoneNum : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2010-04-27 16:41
*==========================================================================*/
/*static void GetCfgPhoneNum(IN int iIdx, OUT char *pszPhoneNum)
{
	int iBufflenth;
	
	DxiGetData(VAR_SMS_PHONE_INFO, iIdx, 0, &iBufflenth, pszPhoneNum, 0);


	return;
}*/

/*==========================================================================*
* FUNCTION : ClearCOmBuffer
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE:  2006年5月9日 13:40:10
*==========================================================================*/
void ClearCOmBuffer(int fd)
{
	char * pChar;
	while(read(fd, pChar, 1)>0);
	return;

}
/*==========================================================================*
* FUNCTION : MyDelay
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE:  2006年5月9日 13:40:10
*==========================================================================*/
void MyDelay(int x)
{
	for(; x>= 0; x--)
	{
		int y;
		for(y=0;y<1000;y++);				

	}

}
/*==========================================================================*
* FUNCTION : GetTimeString
* PURPOSE  : Get a DateAndTime string according to time_t struct 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: time_t  tCurtime : 
*            char*   string   : return the needed string
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 11:40
*==========================================================================*/
static int GetTimeString(time_t tCurtime, char* string)
{
    struct tm		tmCurtime;
    //char			tmp;
#define TM_YEAR_START	1900

    gmtime_r(&(tCurtime), &tmCurtime);

    sprintf(string, "%04d%02d%02d %02d:%02d:%02d", 
	(tmCurtime.tm_year + TM_YEAR_START),
	(tmCurtime.tm_mon + 1),
	tmCurtime.tm_mday,
	tmCurtime.tm_hour,
	tmCurtime.tm_min,
	tmCurtime.tm_sec
	);

    return ERR_SNP_OK;
}
/*==========================================================================*
* FUNCTION : ServiceInit
* PURPOSE  : 
* CALLS    : 
* CALLED BY: Main Module of ACU Plus
* RETURN   : DWORD : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE:  2006年5月9日 13:40:10
*==========================================================================*/
DWORD ServiceInit(IN int argc,  IN char **argv)
{

	UNUSED(argc);
	UNUSED(argv);

	// 1.Clear buffer
	memset(&g_SMSGlobals, 0, sizeof(g_SMSGlobals));

	g_SMSGlobals.hThreadSelf = RunThread_GetId(NULL);

	return 0;

}
/*==========================================================================*
* FUNCTION : GetAlarmDescription
* PURPOSE  : Get alarm description: alarm name + its owner
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*            
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
#define SMS_ALARM_DISCRIPTION_LEN   80

int GetAlarmDescription(ALARM_SIG_VALUE_EXT* pAlarmSigValue, char* string)
{
    char cAlarmLevel[10]= {0};
    //char powerMIB_timestr[15];

    switch(pAlarmSigValue->stAlarmSigValue.pStdSig->iAlarmLevel)
    {
	case ALARM_LEVEL_OBSERVATION:		/* OA */
	    sprintf(cAlarmLevel, "OA");
	    break;

	case ALARM_LEVEL_MAJOR:			/* MA */
	    sprintf(cAlarmLevel, "MA");
	    break;

	case ALARM_LEVEL_CRITICAL:		/* CA */
	    sprintf(cAlarmLevel, "CA");
	    break;

	default:
	    break;
    }

    //GetTimeString(pAlarmSigValue->stAlarmSigValue.sv.tmStartTime, powerMIB_timestr);

    snprintf(string, SMS_ALARM_DISCRIPTION_LEN, "%s, its owner: %s, %s", 
	pAlarmSigValue->stAlarmSigValue.pStdSig->pSigName->pAbbrName[0],
	pAlarmSigValue->stAlarmSigValue.pEquipInfo->pEquipName->pAbbrName[0],
	cAlarmLevel
	);
    return ERR_SNP_OK;
}
/*==========================================================================*
* FUNCTION : SMS_Trip
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* RETURN   : BOOL -  FALSE-Send nothing   TRUE-has sent one message
* COMMENTS : 
* CREATOR  :
*==========================================================================*/
//The status of alarm
enum STATUS_CHANGE_ENUM
{
    ALARM_STATUS_INVALIDATE = 0,
    ALARM_STATUS_ACTIVATED,
    ALARM_STATUS_DEACTIVATED,
};
//wait time on get from SMS trap queue.
#define WAIT_TIME_SMS_TRAP_QUEUE_GET	500		// ms. 

#define EQUIP_SMS_SYSTEM				    1
#define SYSTEM_SETTING_SMS_ALARM_TRAP_LEVEL		    458	

#define SMS_MAXLEN_SITENAME		20

static BOOL SMS_Trip(void)
{
    ALARM_SIG_VALUE_EXT stAlarmSigValue;
    time_t	tCurTime;
    unsigned long nalarmTrapNo = 0;	
    int nalarmStatusChange = 0;
    int i;
    BOOL SentOk = FALSE;

    char powerMIB_timestr[20] = {0};
    char cAlarmDiscription[SMS_ALARM_DISCRIPTION_LEN]= {0};
    char cAlarmTimeType[10]= {0};
    char acRespond[TP_UD_MAX_LEN] = {0};
    int  iSentCnt = 0;
    char acSiteName[SMS_MAXLEN_SITENAME] = {0};

    int nBuflen = 0;
    LANG_TEXT*	pLangInfo;
        
   g_SMSGlobals.nSendActionFlag = 0;

    //Get the alarm info from queue
    if ((Queue_Get(g_SMSGlobals.hTrapSendQueue, &stAlarmSigValue, FALSE, 
	WAIT_TIME_SMS_TRAP_QUEUE_GET) == ERR_QUEUE_OK))
    {
	if ((g_SMSGlobals.nTrapLevel != ALARM_LEVEL_NONE)
	    &&(g_SMSGlobals.nTrapLevel <= stAlarmSigValue.stAlarmSigValue.iAlarmLevel))
	{
	    //Send message 
	    nalarmTrapNo = stAlarmSigValue.ulAlarmSequenceID;

	    if ((stAlarmSigValue.nAlarmMsgType & _ALARM_OCCUR_MASK)
		== _ALARM_OCCUR_MASK)
	    {

		tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmStartTime;

		nalarmStatusChange = ALARM_STATUS_ACTIVATED;
		sprintf(cAlarmTimeType, "%s", "Start");
	    }
	    else if ((stAlarmSigValue.nAlarmMsgType & _ALARM_CLEAR_MASK)
		== _ALARM_CLEAR_MASK)
	    {
		tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmEndTime;

		nalarmStatusChange = ALARM_STATUS_DEACTIVATED;
		sprintf(cAlarmTimeType,"%s", "End");
	    }
	    else
	    {
		tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmStartTime;

		nalarmStatusChange = ALARM_STATUS_INVALIDATE;
		sprintf(cAlarmTimeType, "%s", "Start");
	    }

	    GetTimeString(tCurTime, powerMIB_timestr);

	    GetAlarmDescription(&stAlarmSigValue, cAlarmDiscription);

	   
	    DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		SITE_NAME,
		0,
		&nBuflen,
		&pLangInfo,
		0);

	    snprintf(acSiteName, sizeof(acSiteName), "%s", pLangInfo->pFullName[0]);

	
	    if((g_SMSGlobals.bHasModem == TRUE) && ( g_SMSGlobals.bMMSModem == FALSE))	//普通MODEM只发短信处理
	    {
		
		snprintf(acRespond, sizeof(acRespond), "%s, %s at: %s, From:%s", 
		    cAlarmDiscription,
		    cAlarmTimeType,
		    powerMIB_timestr,
		    acSiteName
		    );
		
		for(i = 0; i < 3; i++)
		{
		    if(!STRING_EQU(g_SMSGlobals.cPhoneNumber[i], ""))
		    {
			iSentCnt++;
			if(SMS_SendShortMsg(g_SMSGlobals.cPhoneNumber[i], acRespond) == TRUE)  //成功
			{			    
			    SentOk = TRUE;
			}
			else
			{			    
			    AppLogOut(SMS_TASK, APP_LOG_INFO, "Failed to send sms to %s.\r\n", g_SMSGlobals.cPhoneNumber[i]);
#if _SMS_DEBUG
			    printf("\n Failed to send sms to %s.", g_SMSGlobals.cPhoneNumber[i]);
#endif
			}			
		    }
		}	
		
		//发送过消息,需要判断有无发送成功过
		if(iSentCnt > 0)
		{
		    if(SentOk)
		    {
			g_SMSGlobals.nSendFailCnt = 0;			
		    }
		    else
		    {
			g_SMSGlobals.nSendFailCnt++;			
		    }

		    g_SMSGlobals.nSendActionFlag = 1;
		}
		

		return SentOk;
	    }
	    else
	    {
		return FALSE;
	    }
	    
	}
	else
	{
	    return FALSE;
	}
    }
    else
    {
	return FALSE;
    }
}
/*==========================================================================*
* FUNCTION : ServiceMain
* PURPOSE  : To provide the standard interface function for SCU Plus main
module
* CALLS    : 
* CALLED BY: Main Module of ACU Plus
* RETURN   : DWORD : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE:  2006年5月9日 13:40:10
*==========================================================================*/

#define		GET_SERVICE_OF_SMS_NAME		"sms.so"
#define	 SMS_FAIL_DELAY	    7	    //当原来有modem，后检测不到了，延时该次数(x)后告警，相当于5*(x-1)秒延时
DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs)
{
	UNUSED(pArgs);

	int		iRst;
	BOOL	bExitFlag = FALSE;

	int		iComFailCounter = 0;

	char			RecvUserData[TP_UD_MAX_LEN];
	//char			SendUserData[TP_UD_MAX_LEN];
	char			RecvUserDataBackup[TP_UD_MAX_LEN];
	APP_SERVICE		*SMS_AppService = NULL;
	SMS_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_SMS_NAME);

	SMS_COMMAND		stCommand;	
	SMS_COMMAND		*pSmsCmd = &stCommand;
	static int s_iRunTime = 0;
	static int s_iTtyUsbNo = 0;  //SMS modem采用设备节点名ttyusb0或ttyusb1, 用该值标识 0-ttyusb0 / 1-ttyusb1
	int	iBufLength;

	SMS_PHONE_INFO szSMSPhoneInfo;
	VAR_VALUE_EX		SigValue;
	SIG_ENUM iSetSmsFailFlag = 9999;

	int iSMSModemLostCnt = 0;  //短讯模块丢失告警计数器

	InitSMS();

	rePowerUSB();
	
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread SMS Service was created, Thread Id = %d.\n", gettid());
#endif	
	//Following is a message cycle
	while(!bExitFlag)
	{

		RUN_THREAD_MSG	msg;

		iRst = RunThread_GetMessage(g_SMSGlobals.hThreadSelf, &msg, FALSE, 0);
		if(iRst == ERR_OK)
		{		  
			switch(msg.dwMsg)
			{
				case MSG_QUIT:
					bExitFlag = TRUE;
					break;	
					
				case MSG_TIMER:	
					break;
	
				default:
					break;
			}//switch(msg.dwMsg)
		}

		//END :  if(iRst == ERR_OK)
		else
		{	
		    /* 1. None Modem */
		    if(!g_SMSGlobals.bHasModem)
		    {
			Sleep(5000);//yield
			if(g_SMSGlobals.fdUsbModem)
			{
			    close(g_SMSGlobals.fdUsbModem);
			}
			
			//循环查询两个设备节点ttyusb0/ttyusb1
			if(s_iTtyUsbNo ==0)
			{
			    g_SMSGlobals.fdUsbModem = SMS_OpenModemPort(MODEM_DEVICE_NAME1, MODEM_BAUD_RATE); 
			}
			else
			{
			    g_SMSGlobals.fdUsbModem = SMS_OpenModemPort(MODEM_DEVICE_NAME2, MODEM_BAUD_RATE); 
			}

			if(g_SMSGlobals.fdUsbModem >0)
			{
			    g_SMSGlobals.bHasModem = TRUE;
			    g_SMSGlobals.bMMSModem = FALSE;   //非彩信MODEM
			    g_SMSGlobals.nSendFailCnt = 0;
			    s_iRunTime = 0;

			    AppLogOut(SMS_TASK, APP_LOG_INFO, "Detect USB MODEM, open it.\r\n");	
			    Sleep(15000);	// Wait for MODEM initialize itself
			    SMS_InitMODEM();
#if _SMS_DEBUG
			    printf("\n\n Detect USB MODEM!");
#endif
			}
			//No modem, clear the message
			else
			{
			    Queue_Empty(g_SMSGlobals.hTrapSendQueue);

			    //切换ttyusb设备节点名称
			    s_iTtyUsbNo++;
			    if(s_iTtyUsbNo > 1)
			    {
				s_iTtyUsbNo = 0;
			    }
#if _SMS_DEBUG
			    printf("\n\n No USB MODEM!");
#endif
			}
		    } //END: /* 1. None Modem */

		    /* 2. Modem is exist! */
		    else
		    {
			//2.1 Get the phone number
			if(DxiGetData(VAR_SMS_PHONE_INFO, SMS_INFO_PHONE_ALL, 0,  &iBufLength,  &szSMSPhoneInfo, 0) == ERR_DXI_OK) 
			{
			    strncpy(g_SMSGlobals.cPhoneNumber[0], szSMSPhoneInfo.szPhoneNumber1, sizeof(g_SMSGlobals.cPhoneNumber[0]));
			    strncpy(g_SMSGlobals.cPhoneNumber[1], szSMSPhoneInfo.szPhoneNumber2, sizeof(g_SMSGlobals.cPhoneNumber[1]));
			    strncpy(g_SMSGlobals.cPhoneNumber[2], szSMSPhoneInfo.szPhoneNumber3, sizeof(g_SMSGlobals.cPhoneNumber[2]));

			    g_SMSGlobals.nTrapLevel = szSMSPhoneInfo.iAlarmLevel;
			}
			else
			{
			    g_SMSGlobals.nTrapLevel= ALARM_LEVEL_NONE;
			}

			//2.2 发送告警消息
			if(SMS_Trip() == FALSE)  //没有发送消息，或发送失败
			{
			    if(g_SMSGlobals.nSendActionFlag)  //执行了发送动作, 但发送失败
			    {			    
				if(g_SMSGlobals.nSendFailCnt > 0)
				{
				    if(!SMS_ModemPortDetect()) //ModemPort检测失败，执行初始化
				    {
					AppLogOut(SMS_TASK, APP_LOG_WARNING, "USB MODEM is no response, close it.\r\n");
					rePowerUSB();
#if _SMS_DEBUG
					printf("\n\n Done the send action, Bud failed! ModemPortDetect() Fail, SendFailCnt = %d ,init the USB MODEM.", g_SMSGlobals.nSendFailCnt);
#endif
				    }
				    //否则,失败次数超过2次,执行一遍初始化
				    else if(g_SMSGlobals.nSendFailCnt > 2)
				    {
					AppLogOut(SMS_TASK, APP_LOG_WARNING, "SMS send failures exceeds 2 , init the USB MODEM.\r\n");
					rePowerUSB();
#if _SMS_DEBUG
					printf("\n\n Done the send action, Bud failed! ModemPortDetect() Ok, SendFailCnt>2, Need to repower the MOdem!");
#endif
				    }
#if _SMS_DEBUG
				    else
				    {
					printf("\n\n Done the send action, Bud failed! ModemPortDetect() Ok, SendFailCnt = %d ", g_SMSGlobals.nSendFailCnt);
				    }
#endif
				}
			    }
			    // 没有执行真正的发送动作(包括: 无消息需要发送、无接收号码、设置成不发送)
			    else
			    {
				//隔120秒检测modem
				s_iRunTime++;
				if(s_iRunTime >= 60)
				{				   
				    s_iRunTime = 0;
				    if(detectMODEM() == FALSE)
				    {
					rePowerUSB();
					AppLogOut(SMS_TASK, APP_LOG_INFO, "USB MODEM may be dead, close it!\n");
#if _SMS_DEBUG
					printf("\n\n No send action (no message,no rcv phone, or disable sms), SMS s_iRunTime > 100, detectMODEM() Fail, need repower modem!");
#endif
				    }
#if _SMS_DEBUG
				    else
				    {
					printf("\n\n No send action (no message,no rcv phone, or disable sms), SMS s_iRunTime > 100, detectMODEM() Ok!");
				    }
#endif
				}   
#if _SMS_DEBUG
				else
				{
				    printf("\n\n No send action (no message,no rcv phone, or disable sms), SMS s_iRunTime = %d, g_SMSGlobals.nSendFailCnt = %d", s_iRunTime, g_SMSGlobals.nSendFailCnt);
				}
#endif
			    }
			}  // END: if(SMS_Trip() == FALSE)  //没有发送消息，或发送失败	

			else  //发送消息， 且发送成功
			{
			    s_iRunTime = 0;
			    g_SMSGlobals.nSendFailCnt = 0;
#if _SMS_DEBUG
			    printf("\n\n Success to send one message, s_iRunTime = 0!");	
#endif			    
			}

		    } //End:  /* 2. Modem is exist! */    

		    Sleep(2000);//yield
		}
		//END :  if(iRst == ERR_OK) else


		SigValue.nSendDirectly = (int)TRUE;
		SigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		SigValue.pszSenderName = "SMS";
		
		//modem丢失告警计数,累计30秒，如果加上不发短讯状态下隔两分钟检测modem的延时，大约共需3分钟产生告警
		if(g_SMSGlobals.bHasModem == TRUE)
		{
		    iSMSModemLostCnt = 1;	

		    SigValue.varValue.enumValue = 0;
		    if(iSetSmsFailFlag != SigValue.varValue.enumValue)
		    {
			iSetSmsFailFlag = SigValue.varValue.enumValue;
		    
			DxiSetData(VAR_A_SIGNAL_VALUE,
			    1,
			    DXI_MERGE_SIG_ID(0, 205),
			    sizeof(VAR_VALUE_EX),
			    &SigValue,
			    0);
		    }
		}
		else if(iSMSModemLostCnt > 0)
		{
		    if(iSMSModemLostCnt < SMS_FAIL_DELAY)
			iSMSModemLostCnt++;

		    if(iSMSModemLostCnt >= SMS_FAIL_DELAY)
		    {
			SigValue.varValue.enumValue = 1;
			if(iSetSmsFailFlag != SigValue.varValue.enumValue)
			{
			    iSetSmsFailFlag = SigValue.varValue.enumValue;

			    DxiSetData(VAR_A_SIGNAL_VALUE,
				1,
				DXI_MERGE_SIG_ID(0, 205),
				sizeof(VAR_VALUE_EX),
				&SigValue,
				0);
			}
		    }
		}
		if(SMS_AppService != NULL)
		{
			SMS_AppService->bReadyforNextService = TRUE;
		}

		//To tell the thread manager I'm living.
		RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);	
		
	}//while(bExitFlag)

	
	ExitSMS();

//	Timer_Kill(RunThread_GetId(NULL),SMS_TIMER_ID);

	return	iRst;
}
/*==========================================================================*
* FUNCTION : GetCmdIdx
* PURPOSE  :
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static int GetCmdIdx(void *pCmdList, char *pcCmdInfo, int iCmdCode)
{
	// cmd info is number
	int iCmdNo = atoi(pcCmdInfo);

	//printf("	!!!	iCmdNo=%d	pcCmdInfo=%s	\n",iCmdNo,pcCmdInfo);

	// cmd info is cmd name
	char acCmdInfo[SMS_CMD_NAME_MAX_LEN];
	strncpy(acCmdInfo, pcCmdInfo, SMS_CMD_NAME_MAX_LEN);

#ifdef __CFG_SIG_NAME_TO_UPPER
	StringToUpper(acCmdInfo);
#endif

	int i;
	int iCmdNum;
	SMS_GET_SET_CMD_LIST	*pGetSetCmdList;
	SMS_READ_CMD_LIST		*pReadCmdList;
	SMS_GET_SET_CMD_INFO	*pGetSetCmdInfo;
	SMS_READ_CMD_INFO		*pReadCmdInfo;

	if((iCmdCode == SMS_CMD_CODE_GET)
		||(iCmdCode == SMS_CMD_CODE_SET))
	{
		pGetSetCmdList = (SMS_GET_SET_CMD_LIST*)pCmdList;
		iCmdNum = pGetSetCmdList->iCmdNum;

		for(i = 0; i < iCmdNum; i++)
		{
			pGetSetCmdInfo = pGetSetCmdList->pGetSetCmdInfo + i;

			if((iCmdNo > 0) && (pGetSetCmdInfo->iCmdNo == iCmdNo))
			{
				return i;
			}

			if(STRING_EQU(pGetSetCmdInfo->acCmdNameEn, acCmdInfo))
			{
				return i;
			}
		}
	}
	else if(iCmdCode == SMS_CMD_CODE_READ)
	{
		pReadCmdList = (SMS_READ_CMD_LIST *)pCmdList;
		iCmdNum = pReadCmdList->iCmdNum;

		//printf(" ***	CODE_READ	iCmdNum = %d		\n",iCmdNum);

		for(i = 0; i < iCmdNum; i++)
		{
			pReadCmdInfo = pReadCmdList->pReadCmdInfo + i;

			//printf("	GetCmdIdx() En= %s	acCmdInfo= %s	\n",
			//	pReadCmdInfo->acCmdNameEn,acCmdInfo);

			if((iCmdNo > 0) && (pReadCmdInfo->iCmdNo == iCmdNo))
			{
				return i;
			}

			if(STRING_EQU(pReadCmdInfo->acCmdNameEn, acCmdInfo))
			{
				return i;
			}
		}
	}
	else
	{
		return -1;
	}

	return -1;
}
/*==========================================================================*
* FUNCTION : PrintNetInfo
* PURPOSE  :
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
#define FOUR_PART_OF_NET_ADDR(dwAddr)		\
	LOBYTE(LOWORD(dwAddr)),		\
	HIBYTE(LOWORD(dwAddr)),		\
	LOBYTE(HIWORD(dwAddr)),		\
	HIBYTE(HIWORD(dwAddr))

static void PrintNetInfo(OUT char *pszRespond)
{
	ASSERT(pszRespond);
	pszRespond[0] = (char)NULL;

	char acNetInfoTemp[40] = {0};

	int nBufLen;
	DWORD dwNetAddr;

	if(DxiGetData(VAR_ACU_NET_INFO,	NET_INFO_IP,			
		0, &nBufLen, &dwNetAddr, 0) == ERR_DXI_OK)
	{
		sprintf(acNetInfoTemp, "IP=%d.%d.%d.%d; ", FOUR_PART_OF_NET_ADDR(dwNetAddr));
		strcat(pszRespond, acNetInfoTemp);
	}

	if(DxiGetData(VAR_ACU_NET_INFO,	NET_INFO_NETMASK,			
		0, &nBufLen, &dwNetAddr, 0) == ERR_DXI_OK)
	{
		sprintf(acNetInfoTemp, "SUB MASK=%d.%d.%d.%d; ", FOUR_PART_OF_NET_ADDR(dwNetAddr));
		strcat(pszRespond, acNetInfoTemp);
	}

	if(DxiGetData(VAR_ACU_NET_INFO,	NET_INFO_DEFAULT_GATEWAY,			
		0, &nBufLen, &dwNetAddr, 0) == ERR_DXI_OK)
	{
		sprintf(acNetInfoTemp, "GATE WAY=%d.%d.%d.%d;", FOUR_PART_OF_NET_ADDR(dwNetAddr));
		strcat(pszRespond, acNetInfoTemp);
	}

	return;
}
/*==========================================================================*
* FUNCTION : HandleSpecialGetSetCmd
* PURPOSE  : 特殊命令处理,生成响应信息,并发送
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int       iCmdCode    : 
*            int       iEquipNo    : 
*            int       iCmdNo      : 
*            char      *pszCmdPara : 
*            OUT char  *pszRespond : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2010-04-23 19:32
*==========================================================================*/
static void HandleSpecialGetSetCmd(BOOL bGet, int iEquipNo, int iCmdNo, SMS_COMMAND *pSmsCmd)
{
	UNUSED(iEquipNo);

	BOOL bSet = !bGet;

	char acRespond[TP_UD_MAX_LEN] = {0};
	char acTemp[TP_UD_MAX_LEN] = {0};
	char acTemp2[TP_UD_MAX_LEN] = {0};
	int iBuffLenth;

	//if(iEquipNo == 0)	//FDZJ
	{
		DWORD dwNetAddr;
		switch(iCmdNo)
		{
		case SPECIAL_CMD_NO_IP:
		case SPECIAL_CMD_NO_SUBMASK:
		case SPECIAL_CMD_NO_GATEWAY:
			{
				printf("Enter get net parameter\n");
				dwNetAddr = inet_addr(pSmsCmd->acCmdPara);

				if(bSet)
				{
					if(DxiSetData(VAR_ACU_NET_INFO,
						NET_INFO_IP + iCmdNo - SPECIAL_CMD_NO_IP,			
						0,		
						sizeof(dwNetAddr),			
						&dwNetAddr,			
						0) == ERR_DXI_OK)
					{
						sprintf(acRespond, "SET OK! ");
					}
					else
					{
						sprintf(acRespond, "SET FAIL! ");
					}
				}
				else
				{
					sprintf(acRespond, "GET OK! ");
				}

				Sleep(500);//yield
				PrintNetInfo(acTemp);

				strcat(acRespond, acTemp);
			}
			break;

		case SPECIAL_CMD_NO_ALARM_NUM1:
		case SPECIAL_CMD_NO_ALARM_NUM2:
		case SPECIAL_CMD_NO_ALARM_NUM3:
		//case SPECIAL_CMD_NO_GET_NUM1:
		//case SPECIAL_CMD_NO_GET_NUM2:
		//case SPECIAL_CMD_NO_GET_NUM3:
		//case SPECIAL_CMD_NO_SET_NUM1:
		//case SPECIAL_CMD_NO_SET_NUM2:
		//case SPECIAL_CMD_NO_SET_NUM3:
			{
				sprintf(acTemp2, "%s=", pSmsCmd->acCmdName);
				if(bSet)
				{
					//if(DXI_SetPhoneNumber(iCmdNo-SPECIAL_CMD_NO_ALARM_NUM1, pSmsCmd->acCmdPara))
				        iBuffLenth = SMS_PHONE_NUMBER_LENTH;
					if(DxiSetData(VAR_SMS_PHONE_INFO, iCmdNo-SPECIAL_CMD_NO_ALARM_NUM1+SMS_INFO_PHONE1, 0, iBuffLenth, pSmsCmd->acCmdPara, 0))
					{
						sprintf(acRespond, "SET OK! ");
					}
					else
					{
						sprintf(acRespond, "SET FAIL! ");
					}
					Sleep(500);//yield
				}
				else
				{
					sprintf(acRespond, "GET OK! ");
				}

				DxiGetData(VAR_SMS_PHONE_INFO, iCmdNo-SPECIAL_CMD_NO_ALARM_NUM1 + SMS_INFO_PHONE1, 0, &iBuffLenth, acTemp, 0);
				//DXI_GetPhoneNumber(, acTemp);

				strcat(acRespond, acTemp2);
				strcat(acRespond, acTemp);

			}
			break;

		default:
			{
				TRACE("Error Cmd No:%d\n", iCmdNo);
			}

		}
	}

	SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);

	return;
}

/*==========================================================================*
* FUNCTION : EquipIs Existence
* PURPOSE  : 
module
* CALLS    : 
* CALLED BY: Main Module of ACU Plus
* RETURN   : DWORD : 
* COMMENTS : 
* CREATOR  : IlockTeng               DATE:  2006年5月9日 13:40:10
*==========================================================================*/
static BOOL EquipIsExistence(int iEquipId)
{
#define		SAMPLING_SIG_TYPE	0
#define		EXIST_SIG_ID		100
	int						iBufLen;
	INT32						nError;
	SIG_BASIC_VALUE*			pSigValue;

	//System must existence!!!
	if (0x01 == iEquipId)
	{
		return TRUE;
	}
	else
	{
		//Normal!!!
	}

	nError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipId,			
		DXI_MERGE_SIG_ID(SAMPLING_SIG_TYPE, EXIST_SIG_ID),		
		&iBufLen,			
		&pSigValue,			
		0);
	if(nError == ERR_DXI_OK)
	{
		switch(pSigValue->ucType)
		{
		case VAR_LONG:
			printf("	VAR_LONG:In Equip IsExistence*****	This ucType is Error, iEquipId=%d\n",iEquipId);
			//pSigValue->varValue.lValue;
			return FALSE;
			break;

		case VAR_FLOAT:
			printf("	VAR_FLOAT:In Equip IsExistence*****	This ucType is Error, iEquipId=%d\n",iEquipId);
			//pSigValue->varValue.fValue;
			return FALSE;
			break;

		case VAR_UNSIGNED_LONG:
			printf("	VAR_UNSIGNED_LONG:In Equip IsExistence*****	This ucType is Error, iEquipId=%d\n",iEquipId);
			//pSigValue->varValue.ulValue;
			return FALSE;
			break;
		case VAR_ENUM:
			if (0 == pSigValue->varValue.enumValue)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
			//printf("");
			//pSigValue->varValue.enumValue;
			//printf("\nReport  i= %d   enumValue =%d \n",i,pSigValue->varValue.enumValue);
			break;

		default:	
			printf("	default:In Equip IsExistence*****	This ucType is Error, iEquipId=%d\n",iEquipId);
			return FALSE;
			break;
		}
		//return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*==========================================================================*
* FUNCTION : SetSigValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2010-04-27 17:55
*==========================================================================*/
static BOOL SetSigValue(SMS_SIG_INFO *pSmsSigInfo, char *pszPara)
{
    int			iRst;
    int			nBufLen;
    SIG_BASIC_VALUE		*pSigInfo;
    VAR_VALUE_EX		sigValue;

    //获取信号信息
    //VAR_A_SIGNAL_INFO_STRU
    iRst = DxiGetData(VAR_A_SIGNAL_VALUE,			
	pSmsSigInfo->iEquipId,
	DXI_MERGE_SIG_ID(pSmsSigInfo->iSigType, pSmsSigInfo->iSigId),
	&nBufLen,			
	&pSigInfo,			
	0);
    if(iRst != ERR_OK)
    {
	TRACE("[SMS]Get Signal Info Fail!\n");
	//printf("1 EquipID = %d, iSigType = %d, iSigID = %d.\n", 
	//	pSmsSigInfo->iEquipId, pSmsSigInfo->iSigType, pSmsSigInfo->iSigId);

	return FALSE;
    }

    //printf("\n	pSigInfo->ucType =%d \n",pSigInfo->ucType);

    //判断信号值的类型
    switch(pSigInfo->ucType)
    {
    case VAR_FLOAT:
	sigValue.varValue.fValue = atof(pszPara);
	//printf(" SetSigValue fValue=%f\n",sigValue.varValue.fValue);
	break;

    case VAR_LONG:
	sigValue.varValue.lValue = atoi(pszPara);
	//printf(" SetSigValue lValue=%d\n",sigValue.varValue.lValue);
	break;

    case VAR_UNSIGNED_LONG:
	sigValue.varValue.ulValue = atoi(pszPara);
	//printf(" SetSigValue ulValue=%d\n",sigValue.varValue.ulValue);
	break;

    case VAR_ENUM:
	sigValue.varValue.enumValue = atoi(pszPara);
	//printf(" SetSigValue enumValue=%d\n",sigValue.varValue.enumValue);
	break;

    default:
	sigValue.varValue.ulValue = atoi(pszPara);
	//printf(" ddd  SetSigValue ulValue=%d\n",sigValue.varValue.ulValue);
	break;
    }

    //设置信号值
    sigValue.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
    //sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
    sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
    sigValue.pszSenderName = SMS_TASK;

    //VAR_A_SIGNAL_VALUE
    iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
	pSmsSigInfo->iEquipId,
	DXI_MERGE_SIG_ID(pSmsSigInfo->iSigType, pSmsSigInfo->iSigId),
	sizeof(VAR_VALUE_EX),
	&sigValue,
	0);

    if(iRst != ERR_OK)
    {
	TRACE("[SMS]Set Signal Value Fail!\n");

	//printf("2 iRst=%d EquipID = %d iSigType = %d iSigID = %d Value = %f ValueS=%s\n",
	//	iRst,pSmsSigInfo->iEquipId, pSmsSigInfo->iSigType, pSmsSigInfo->iSigId, sigValue.varValue.fValue,pszPara);

	return FALSE;
    }

    return TRUE;
}
/*==========================================================================*
* FUNCTION : GetSigValueString
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int        iEquipID      : 
*            int        iSigType      : 
*            int        iSigId        : 
*            OUT char*  pcValueString : 
* RETURN   : static int : strlen of value string
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2010-04-27 17:55
*==========================================================================*/
static int GetSigValueString(int iEquipID, int iSigType, int iSigId, OUT char* pcValueString)
{
    ASSERT(pcValueString);

    void*		pSigInfo;
    SAMPLE_SIG_INFO	*pSampSigInfo;
    CTRL_SIG_INFO	*pCtrlSigInfo;
    SET_SIG_INFO	*pSetSigInfo;
    LANG_TEXT*	pLangText;

    SIG_BASIC_VALUE*	pSigValue;
    int			iRst, iBufLen;

    iRst = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
	iEquipID,
	DXI_MERGE_SIG_ID(iSigType,iSigId),
	&iBufLen,
	&pSigInfo,
	0);

    if (iRst != ERR_OK)
    {
	return 0;
    }
    pSampSigInfo = (SAMPLE_SIG_INFO*)pSigInfo;


    iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	iEquipID,
	DXI_MERGE_SIG_ID(iSigType,iSigId),
	&iBufLen,
	&pSigValue,
	0);

    if (iRst != ERR_OK)
    {
	return 0;
    }

    if (VAR_FLOAT == pSigValue->ucType)
    {
	//因为set,2,160 set,2,2,42 等短信回的数值不对   修改为
	//sprintf(pcValueString, "%g%s", pSigValue->varValue.fValue, pSampSigInfo->szSigUnit);
	sprintf(pcValueString, "%f%s", pSigValue->varValue.fValue, pSampSigInfo->szSigUnit);	
    }
    else if (VAR_LONG == pSigValue->ucType)
    {
	sprintf(pcValueString, "%ld%s", pSigValue->varValue.lValue, pSampSigInfo->szSigUnit);	
    }
    else if (VAR_UNSIGNED_LONG == pSigValue->ucType)
    {
	sprintf(pcValueString, "%lu%s", pSigValue->varValue.ulValue, pSampSigInfo->szSigUnit);	
    }
    else if (VAR_ENUM == pSigValue->ucType)
    {
	if(iSigType == SIG_TYPE_SAMPLING)
	{
	    pLangText = pSampSigInfo->pStateText[pSigValue->varValue.enumValue];
	    strcpy(pcValueString, pLangText->pFullName[0]);	// 0 is English Name
	}
	else if(iSigType == SIG_TYPE_CONTROL)
	{
	    pCtrlSigInfo = (CTRL_SIG_INFO*)pSigInfo;
	    pLangText = pCtrlSigInfo->pStateText[pSigValue->varValue.enumValue];
	    strcpy(pcValueString, pLangText->pFullName[0]);	// 0 is English Name
	}
	else if(iSigType == SIG_TYPE_SETTING)
	{
	    pSetSigInfo = (SET_SIG_INFO*)pSigInfo;
	    pLangText = pSetSigInfo->pStateText[pSigValue->varValue.enumValue];
	    strcpy(pcValueString, pLangText->pFullName[0]);	// 0 is English Name
	}
	else
	{
	    DEBUG_SMS_FILE_FUN_LINE_STRING("SMS: SigType Error!");
	    return 0;
	}

    }
    else
    {
	DEBUG_SMS_FILE_FUN_LINE_STRING("SMS can not support this signal value type");
	DEBUG_SMS_FILE_FUN_LINE_INT("pSigValue->ucType", pSigValue->ucType);
	return 0;
    }

    return strlen(pcValueString);
}
// 检查用户的手机号码是否可以Get或者Set
/*==========================================================================*
* FUNCTION : CheckUserPhoneNum
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char  *pszPhoneNum : 手机号码，无"+",86138...或者138...
*            IN BOOL  bGet         : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2010-04-27 16:43
*==========================================================================*/
static BOOL CheckUserPhoneNum(IN char *pszPhoneNum, IN BOOL bGet)
{
    UNUSED(bGet);

    // skip '+'
    if(*pszPhoneNum == '+')
    {
	pszPhoneNum++;
    }

    // skip 86
    if((*pszPhoneNum == '8') && (*(pszPhoneNum + 1) == '6'))
    {
	pszPhoneNum += 2;
    }

    //char acCfgPhoneNum[PHONE_NO_STRING_LEN];    
    int i;
    for(i = 0; i < 3; i++)
    {
	//GetCfgPhoneNum(i, acCfgPhoneNum);
	if(STRING_EQU(g_SMSGlobals.cPhoneNumber[i], pszPhoneNum))
	{
	    return TRUE;
	}
    }
    
    return FALSE;
}
/*==========================================================================*
* FUNCTION : HandleGetSetCmd
* PURPOSE  :
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static void HandleGetSetCmd(SMS_COMMAND *pSmsCmd, BOOL bGet)
{
	UNUSED(pSmsCmd->acCmdCode);
	UNUSED(pSmsCmd->iUserLevel);

	char acRespond[TP_UD_MAX_LEN] = {0};
	char acSigName[SIG_FULL_NAME_MAX_LEN];
	char acSigValue[SIG_FULL_NAME_MAX_LEN];

	printf("Enter Get/Set cmd process \n");

	// Check User Phone Number
	if(!CheckUserPhoneNum(pSmsCmd->acPhoneNum, bGet))
	{
		SMS_SendShortMsg(pSmsCmd->acPhoneNum, "Permission Denied");
		return;
	}

	SMS_GET_SET_CMD_LIST *pGetSetCmdList = &(g_SMSGlobals.SMSPrivateConfig.stGetSetCmdList);

	int iCmdIdx = GetCmdIdx(pGetSetCmdList, pSmsCmd->acCmdName, SMS_CMD_CODE_GET);

	//printf("GetSetCmd() iCmdIdx = %d\n",iCmdIdx);

	if(iCmdIdx < 0)
	{
		sprintf(acRespond, "Error Command Number or Command Name: %s", pSmsCmd->acCmdName);
		SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
		return;
	}

	// Get Command info
	SMS_GET_SET_CMD_INFO *pCmdInfo = pGetSetCmdList->pGetSetCmdInfo + iCmdIdx;

	//printf("GetSetCmd() iEquipId=%d iSigType=%d iSigId=%d\n",
	//			pCmdInfo->stSigInfo.iEquipId,
	//			pCmdInfo->stSigInfo.iSigType,
	//			pCmdInfo->stSigInfo.iSigId);

	// Get Command Handle
	if(bGet)
	{
		// Special Handle
		if(pCmdInfo->stSigInfo.iEquipId < 0)
		{
			printf("Enter Special Handle\n");
			HandleSpecialGetSetCmd(bGet, pCmdInfo->stSigInfo.iEquipId, pCmdInfo->iCmdNo, pSmsCmd);
		}
		else
		{
			printf("Enter other Handle\n");
			GetSignalName(pCmdInfo->stSigInfo.iEquipId,
				pCmdInfo->stSigInfo.iSigType,
				pCmdInfo->stSigInfo.iSigId,
				acSigName);

			GetSigValueString(pCmdInfo->stSigInfo.iEquipId,
				pCmdInfo->stSigInfo.iSigType,
				pCmdInfo->stSigInfo.iSigId,
				acSigValue);

			if(!pCmdInfo->bGet)
			{
				sprintf(acRespond, "GET FAIL! This signal can NOT be GET: %s", acSigName);
			}
			else
			{
				sprintf(acRespond, "GET OK! %s = %s", acSigName, acSigValue);
			}

			if (!EquipIsExistence(pCmdInfo->stSigInfo.iEquipId))
			{
				memset(acRespond, 0, TP_UD_MAX_LEN);
				sprintf(acRespond, "GET FAIL! This Equip is Inexistence");
			}

			SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
		}
	}
	// Set Command Handle
	else
	{
		// Special Handle
		printf("Enter set commad Handle\n");
		if(pCmdInfo->stSigInfo.iEquipId < 0)
		{
			HandleSpecialGetSetCmd(bGet, pCmdInfo->stSigInfo.iEquipId, pCmdInfo->iCmdNo, pSmsCmd);
		}
		else
		{
			BOOL bSetOK;

			GetSignalName(pCmdInfo->stSigInfo.iEquipId,
				pCmdInfo->stSigInfo.iSigType,
				pCmdInfo->stSigInfo.iSigId,
				acSigName);

			if(!pCmdInfo->bSet)
			{
				sprintf(acRespond, "SET FAIL! This signal can NOT be SET: %s", acSigName);
			}
			else
			{
				bSetOK = SetSigValue(&pCmdInfo->stSigInfo, pSmsCmd->acCmdPara);

				GetSigValueString(pCmdInfo->stSigInfo.iEquipId,
					pCmdInfo->stSigInfo.iSigType,
					pCmdInfo->stSigInfo.iSigId,
					acSigValue);

				sprintf(acRespond, "SET %s! Setting Need Time,Try Get Again! Now %s = %s",
					bSetOK ? "OK" : "FAIL",
					acSigName, acSigValue);

				if (!EquipIsExistence(pCmdInfo->stSigInfo.iEquipId))
				{
					memset(acRespond,0,TP_UD_MAX_LEN);
					sprintf(acRespond, "Set FAIL! This Equip is Inexistence");
				}
			}

			SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
		}
	}
}
/*=====================================================================*
* Function name: MakeAlarmInfo
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int GetSignalName(int iEquipID, int iSigType, int iSigId, OUT char* pcSignalName)
{
	ASSERT(pcSignalName);

	void*		pSigInfo;
	int			iRst, iBufLen;

	iRst = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
		iEquipID,
		DXI_MERGE_SIG_ID(iSigType,iSigId),
		&iBufLen,
		&pSigInfo,
		0);

	if (iRst != ERR_OK)
	{
		return 0;
	}

	LANG_TEXT*	pLangText;
	pLangText = ((SAMPLE_SIG_INFO*)pSigInfo)->pSigName;

	strcpy(pcSignalName, pLangText->pFullName[0]);	// 0 is English Name

	return strlen(pcSignalName);
}
/*==========================================================================*
* FUNCTION : ConvertTime
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: time_t* pTime    : 
*            BOOL bUTCToLocal  : TRUE, UTC time to ydn23 time , that is to add the zone8 , else
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : zwh                   DATE: 2008-11-10 15:00
*==========================================================================*/
BOOL ConvertTime(time_t* pTime, BOOL bUTCToYDN)
{
    SIG_BASIC_VALUE* pSigValue;

    int nBufLen;

    if (DxiGetData(VAR_A_SIGNAL_VALUE,
	DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
	SIG_ID_TIME_ZONE_SET,
	&nBufLen,			
	&pSigValue,			
	0) != ERR_DXI_OK)
    {	
	return FALSE;
    }

    ASSERT(pSigValue);	

    if(SIG_VALUE_IS_VALID(pSigValue))
    {	
	int nTimeOffset = (pSigValue->varValue.lValue) * 3600 / 2;

	if (bUTCToYDN)
	{
	    *pTime += nTimeOffset;
	}
	else//ydn23 time to UTC time
	{
	    *pTime -= nTimeOffset;
	}
    }
    else
    {
	return FALSE;
    }

    return TRUE;
}
/*=====================================================================*
* Function name: MakeAlarmInfo
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void MakeAlarmInfo(ALARM_SIG_VALUE	*pAlarmSigValue, OUT char *pBuf)
{
	char acAlmSigName[40];
	char acAlmSigLevel[4];
	char acAlmTime[40];

	// Signal Name
	GetSignalName(pAlarmSigValue->pEquipInfo->iEquipID, SIG_TYPE_ALARM, pAlarmSigValue->pStdSig->iSigID, acAlmSigName);

	// Alarm Level
	if(pAlarmSigValue->iAlarmLevel == ALARM_LEVEL_OBSERVATION)
	{
		sprintf(acAlmSigLevel, "OA");
	}
	else if(pAlarmSigValue->iAlarmLevel == ALARM_LEVEL_CRITICAL)
	{
		sprintf(acAlmSigLevel, "CA");
	}
	else if(pAlarmSigValue->iAlarmLevel == ALARM_LEVEL_MAJOR)
	{
		sprintf(acAlmSigLevel, "MA");
	}
	else
	{
		sprintf(acAlmSigLevel, "NA");
	}

	// Alarm start time
	time_t tmStart = pAlarmSigValue->sv.tmStartTime;
	struct tm tmAlarmTime;

	ConvertTime((time_t *)&tmStart, TRUE);

	//printf("\n	.........	AlarmInfo()	ConvertTime	\n");

	strftime(acAlmTime, sizeof(acAlmTime), "%y%m%d-%H:%M:%S", 
		gmtime_r(&(tmStart), &tmAlarmTime));

	//printf("\n	.........	AlarmInfo()	strftime	\n");

	// Alarm info
	sprintf(pBuf, "%s(%s)%s, ",	acAlmSigName, acAlmSigLevel, acAlmTime);
	//printf("\n	.........	AlarmInfo()	sprintf	\n");

}
/*=====================================================================*
* Function name: GetRealTimeAlarmData
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
int				iVarSubID = 0;
int				iBufLen = 0;
int				iTimeOut = 0;
static int GetRealTimeAlarmData(OUT ALARM_SIG_VALUE **ppSigValue)
{
	//int iSigNumber = 0;
	int iGetSigNumber = 0, iSignalNum = 0;
	ALARM_SIG_VALUE			*stAlarmSigValue = NULL;
	int iError = 0;

	iError += DxiGetData(VAR_ACTIVE_ALARM_NUM,
		ALARM_LEVEL_NONE,			
		iVarSubID,		
		&iBufLen,			
		&iGetSigNumber,			
		iTimeOut);


	if(iGetSigNumber > 0)
	{
		stAlarmSigValue = NEW(ALARM_SIG_VALUE, iGetSigNumber);
		if(stAlarmSigValue == NULL)
		{
			return FALSE;
		}
		memset(stAlarmSigValue, 0x0, sizeof(ALARM_SIG_VALUE) * iGetSigNumber);
	}
	else
	{
		return FALSE;
	}


	iBufLen = sizeof(ALARM_SIG_VALUE) * iGetSigNumber;//Should assign size

	iError += DxiGetData(VAR_ACTIVE_ALARM_INFO,
		ALARM_LEVEL_NONE,			
		0,		
		&iBufLen,			
		stAlarmSigValue,			
		0);

	iSignalNum = iBufLen / sizeof(ALARM_SIG_VALUE);

	//TRACE("\niSignalNum:%d\n", iSignalNum);
	if(iError == ERR_DXI_OK)
	{
		if(stAlarmSigValue != NULL)
		{
			*ppSigValue = (ALARM_SIG_VALUE *)stAlarmSigValue;
		}

		return iSignalNum;

	}
	else
	{
		return FALSE;
	}
}
/*=====================================================================*
* Function name: HandleSpecialReadCmd
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
#define ACT_ALM_NUM_PER_SMS		2

static void HandleSpecialReadCmd(int iCmdType, SMS_COMMAND *pSmsCmd)
{
	char acRespond[TP_UD_MAX_LEN] = {0};
	char acAlmInfo[TP_UD_MAX_LEN] = {0};
	ALARM_SIG_VALUE	*pAlarmSigValue;
	ALARM_SIG_VALUE	*pAlarmSigValueTemp;
	//int		iBufLen;
	int		iActiveAlarmNum;

	//iCmdType:
	//	-1:ACTIVE ALARM
	if(iCmdType == -1)
	{
		iActiveAlarmNum = GetRealTimeAlarmData(&pAlarmSigValue);

		printf("\n *******	This is iActiveAlarmNum = %d	******\n",iActiveAlarmNum);

		int i;
		int iSmsAlmNum = 0;
		//for(i = 0; (i < iActiveAlarmNum) && (pAlarmSigValue != NULL); i++, pAlarmSigValue++)
		pAlarmSigValueTemp = pAlarmSigValue;
		for(i = 0; i < iActiveAlarmNum; i++,pAlarmSigValueTemp++)
		{
			if (pAlarmSigValueTemp != NULL)
			{
				Sleep(50);//yield
			
					iSmsAlmNum++;
					MakeAlarmInfo(pAlarmSigValueTemp, acAlmInfo);

					//2010/10/08此处是个问题，不能将告警信号全部发送，所以将此判断条件屏蔽
					//2010/10/08if((iSmsAlmNum % ACT_ALM_NUM_PER_SMS) == 0)
					//2010/10/08{
					memcpy(acRespond,acAlmInfo,TP_UD_MAX_LEN);

/*
					if (!EquipIsExistence(iEquipId))
					{
						memset(acRespond,0,TP_UD_MAX_LEN);
						sprintf(acRespond, "Read FAIL! The Equip is Inexistence \n");
					}
*/
					SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
					memset(acRespond, 0, sizeof(acRespond));
					//2010/10/08}
			
			}
		}

		if(iSmsAlmNum == 0)
		{
			sprintf(acRespond, "No active alarm of this equip.");
			SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
		}

		SAFELY_DELETE(pAlarmSigValue);
	}
	else
	{
		printf("\n	IN	Handle SpecialReadCmd() This  iCmdType is Error =%d	\n",iCmdType);
	}

	return;
}
/*=====================================================================*
* Function name: FindSmsIdxByNo
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int FindSmsIdxByNo(SMS_SMS_LIST *pSmsList, int iSmsNo)
{
	SMS_SMS_INFO *pSmsInfo;
	int i;
	for(i = 0; i < pSmsList->iSmsNum; i++)
	{
		pSmsInfo = pSmsList->pSmsInfo + i;
		if(pSmsInfo->iSmsNo == iSmsNo)
		{
			return i;
		}
	}

	return -1;
}
/*==========================================================================*
* FUNCTION : MakeSmsInfoByNo
* PURPOSE  : 生成该条短信的内容:Sig1:Sig1Value;Sig2:Sig2Value;
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int       iSmsNo : 
*            OUT char  *pBuf  : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2010-04-27 18:06
*==========================================================================*/
static BOOL MakeSmsInfoByNo(int iSmsNo, OUT char *pBuf)
{
	ASSERT(pBuf);
	pBuf[0] = (char)NULL;

	char acSigName[SIG_FULL_NAME_MAX_LEN];
	char acSigValue[SIG_FULL_NAME_MAX_LEN];
	char acTemp[SIG_FULL_NAME_MAX_LEN * 2];

	SMS_SMS_LIST *pSmsList = &g_SMSGlobals.SMSPrivateConfig.stSmsList;

	int iSmsIdx = FindSmsIdxByNo(pSmsList, iSmsNo);
	if(iSmsIdx < 0)
	{
		TRACE("Error SMS No: %d\n", iSmsNo);
		return FALSE;
	}

	SMS_SMS_INFO *pSmsInfo = pSmsList->pSmsInfo + iSmsIdx;
	SMS_SIG_INFO *pSigInfo;

	int i;
	for(i = 0; i < pSmsInfo->iSigNum; i++)
	{
		pSigInfo = pSmsInfo->pSigInfo + i;

		GetSignalName(pSigInfo->iEquipId, pSigInfo->iSigType, pSigInfo->iSigId, acSigName);
		GetSigValueString(pSigInfo->iEquipId, pSigInfo->iSigType, pSigInfo->iSigId, acSigValue);
		//sprintf(acTemp, "%s:%s;",);

		if (!EquipIsExistence(pSigInfo->iEquipId))
		{
			//memset(acRespond,0,TP_UD_MAX_LEN);
			//sprintf(acTemp, "FAIL! The Equip is Inexistence %s",acSigName);
			sprintf(acTemp, "%s:%s;",acSigName,"There isn't  the Equip");
			printf("	The Equip ID  is inexistence	!!!!!!!\n");
		}
		else
		{
			sprintf(acTemp, "%s:%s;",acSigName,acSigValue);
		}

		strcat(pBuf, acTemp);
	}

	return TRUE;
}
/*=====================================================================*
* Function name: HandleReadCmd
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void HandleReadCmd(SMS_COMMAND *pSmsCmd)
{
	UNUSED(pSmsCmd->acCmdCode);
	UNUSED(pSmsCmd->iUserLevel);

	char acRespond[TP_UD_MAX_LEN] = {0};
	char acSignalInfo[TP_UD_MAX_LEN * 2] = {0};

	// Check User Phone Number
	if(!CheckUserPhoneNum(pSmsCmd->acPhoneNum, TRUE))
	{
		SMS_SendShortMsg(pSmsCmd->acPhoneNum, "Permission Denied");
		return;
	}

	

	/**********************************************/
	SMS_READ_CMD_LIST *pReadCmdList = &(g_SMSGlobals.SMSPrivateConfig.stReadCmdList);
	/**********************************************/

	// Get Command No.
	//SMS_READ_CMD_LIST *pReadCmdList
	//	= &g_SMSGlobals.SMSPrivateConfig.stEquipList.pSmsEquipCmdInfo->stReadCmdList;

	int iCmdNo = GetCmdIdx(pReadCmdList, pSmsCmd->acCmdName, SMS_CMD_CODE_READ);

	printf("	Read()  iCmdNo=%d *********\n", iCmdNo);

	if(iCmdNo < 0)
	{
		sprintf(acRespond, "Error Command Number or Command Name: %s", pSmsCmd->acCmdName);
		SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
		return;
	}

	// Get Command info
	SMS_READ_CMD_INFO *pCmdInfo = pReadCmdList->pReadCmdInfo + iCmdNo;

	// Read Command Handle
	int i;

	// Special Handle
	if(pCmdInfo->iSmsNum <= 0)
	{
		HandleSpecialReadCmd( pCmdInfo->iSmsNum, pSmsCmd);
	}
	else
	{
		for(i = 0; i < pCmdInfo->iSmsNum; i++)
		{
			//	(i/N)Signal1:Sig1Value;Signal2:Sig2Value;
			MakeSmsInfoByNo(pCmdInfo->piSmsList[i], acSignalInfo);

			if(strlen(acSignalInfo) >= TP_UD_MAX_LEN - 6)
			{
				acSignalInfo[TP_UD_MAX_LEN - 6] = (char)NULL;
			}

			sprintf(acRespond, "(%d/%d)%s", i, pCmdInfo->iSmsNum, acSignalInfo);

			SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
		}
	}
}
#define CMD_NUM_PER_SMS		5

/*==========================================================================*
* FUNCTION : MakeGetSetSmsInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int       iEquipIdx : Equip index
*            BOOL      bGet      : Get or Set
*            int       iFrom     : 
*            int       iTo       : 
*            OUT char  *pBuf     : 
* RETURN   : static int :  if pBuf == NULL, return config command number
else return strlen(*pBuf)
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2010-04-28 09:30
*==========================================================================*/
static int MakeGetSetSmsInfo( BOOL bGet, int iFrom, int iTo, OUT char *pBuf)
{
	//ASSERT(iEquipIdx >= 0);


	int iGetSetNum = g_SMSGlobals.SMSPrivateConfig.stGetSetCmdList.iCmdNum;
	if(pBuf == NULL)
	{
		return iGetSetNum;
	}

	int i;

	// 每条短信最多可能有25个字符
	char acTemp[SMS_EQUIP_NAME_MAX_LEN + 6];
	SMS_GET_SET_CMD_INFO *pGetSetCmdInfo;
	
	pGetSetCmdInfo = g_SMSGlobals.SMSPrivateConfig.stGetSetCmdList.pGetSetCmdInfo;

	*pBuf = (char)NULL;

	for(i = iFrom; (i <= iTo) && (i < iGetSetNum); i++)
	{
		// 1-IP, 2-SUBMASK
		pGetSetCmdInfo = pGetSetCmdInfo + i;

		if((bGet && pGetSetCmdInfo->bGet)
			|| (!bGet && pGetSetCmdInfo->bSet))
		{
			sprintf(acTemp, "%d-%s%s", pGetSetCmdInfo->iCmdNo, pGetSetCmdInfo->acCmdNameEn,
				i != iGetSetNum - 1 ? ", " : "");		//最后一个不输出", "
			strcat(pBuf, acTemp);
		}
	}

	return strlen(pBuf);
}
/*=====================================================================*
* Function name: SendGetSetHelpInfo
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void SendGetSetHelpInfo(SMS_COMMAND *pSmsCmd, BOOL bGet)
{
	int iCmdNum = MakeGetSetSmsInfo(bGet, 0, 0, NULL);
	int iSendSmsNum = iCmdNum / 5 + 1;

	//No.1: Tips info + 0, 1, 2, 3
	//No.2: 4,5,6,7,8
	//No.3: 9,10,...

	char acRespond[TP_UD_MAX_LEN] = {0};
	char acTemp[TP_UD_MAX_LEN] = {0};
	sprintf(acTemp, "%s List of %s:", bGet ? "GET" : "SET", pSmsCmd->acEquipName);
	int i;

	for(i = 0; i < iSendSmsNum; i++)
	{
		if(i == 0)
		{
			MakeGetSetSmsInfo(bGet, 0, 3, acRespond);
			strcat(acTemp, acRespond);
			SMS_SendShortMsg(pSmsCmd->acPhoneNum, acTemp);
		}
		else
		{
			MakeGetSetSmsInfo(bGet, i * 5 - 1, i * 5 + 3, acRespond);
			SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
		}
	}

}
/*=====================================================================*
* Function name: MakeReadSmsInfo
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int MakeReadSmsInfo(int iFrom, int iTo, OUT char *pBuf)
{
	//ASSERT(iEquipIdx >= 0);

	int iReadNum = g_SMSGlobals.SMSPrivateConfig.stReadCmdList.iCmdNum;
	if(pBuf == NULL)
	{
		return iReadNum;
	}

	int i;

	// 每条短信最多可能有25个字符
	char acTemp[SMS_EQUIP_NAME_MAX_LEN + 6];
	SMS_READ_CMD_INFO *pReadCmdInfo = g_SMSGlobals.SMSPrivateConfig.stReadCmdList.pReadCmdInfo;

	*pBuf = (char)NULL;

	for(i = iFrom; (i <= iTo) && (i < iReadNum); i++)
	{
		// 1-IP, 2-SUBMASK
		pReadCmdInfo = pReadCmdInfo + i;
		sprintf(acTemp, "%d-%s%s", pReadCmdInfo->iCmdNo, pReadCmdInfo->acCmdNameEn,
			i != iReadNum - 1 ? ", " : "");		//最后一个不输出", "
		strcat(pBuf, acTemp);
	}

	return strlen(pBuf);
}
/*=====================================================================*
* Function name: SendReadHelpInfo
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void SendReadHelpInfo(SMS_COMMAND *pSmsCmd)
{
	int iCmdNum = MakeReadSmsInfo(0, 0, NULL);
	int iSendSmsNum = iCmdNum / 5 + 1;

	//No.1: Tips info + 0, 1, 2, 3
	//No.2: 4,5,6,7,8
	//No.3: 9,10,...

	char acRespond[TP_UD_MAX_LEN] = {0};
	char acTemp[TP_UD_MAX_LEN] = {0};
	sprintf(acTemp, "READ List of %s:",pSmsCmd->acEquipName);
	int i;

	for(i = 0; i < iSendSmsNum; i++)
	{
		if(i == 0)
		{
			MakeReadSmsInfo(0, 3, acRespond);
			strcat(acTemp, acRespond);
			SMS_SendShortMsg(pSmsCmd->acPhoneNum, acTemp);
		}
		else
		{
			MakeReadSmsInfo(i * 5 - 1, i * 5 + 3, acRespond);
			SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
		}
	}

}
/*=====================================================================*
* Function name: HandleHelpCmd
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void HandleHelpCmd(SMS_COMMAND *pSmsCmd)
{
	UNUSED(pSmsCmd->acCmdCode);
	UNUSED(pSmsCmd->iUserLevel);

	char acRespond[TP_UD_MAX_LEN] = {0};
	//char acTemp[TP_UD_MAX_LEN] = {0};

	//int i;
	//int iEqpNum = g_SMSGlobals.SMSPrivateConfig.stEquipList.iEquipNum;
	//SMS_EQUIP_INFO *pSmsEqpInfo;
	//EQUIP_INFO		*pEquipInfo;

	//int iEquipNo;


	//only "HELP":
	//	Command Format: Command Code, Equip No. or Equip Name, Command No. or Signal Name ,Parameter(optional)
	//	Send “HELP, CODE” to get command code list; Send “HELP, EQUIP” to get Equip List.

	//新增加的，要不HELP命令会小写有问题
	StringToUpper(pSmsCmd->acEquipName);

	if(pSmsCmd->acEquipName[0] == (char)NULL)
	{
		sprintf(acRespond, 
			"Command Format: Command Code, Equip No. or Equip Name, Command No. or Signal Name ,Parameter(optional)");
		SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);

		Sleep(2000);//yield

		sprintf(acRespond, 
			"Send \"HELP, CODE\" to get command code list; Send \"HELP, EQUIP\" to get Equip List.");
		SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
	}

	// "HELP,CODE"
	//	Command List:GET,SET,READ,HELP
	else if(STRING_EQU(pSmsCmd->acEquipName, "CODE"))
	{
		sprintf(acRespond, 
			"Command List:GET,SET,READ,HELP");
		SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
	}

	// "HELP,EQUIP"
	//	Equip List: 0-
	else if(STRING_EQU(pSmsCmd->acEquipName, "EQUIP"))
	{
			//预留		
	}
		
	// "HELP,0,READ"
	else
	{
		
			//新增加的，要不HELP命令会小写有问题
			StringToUpper(pSmsCmd->acCmdName);

			if((STRING_EQU(pSmsCmd->acCmdName, "1"))
				|| (STRING_EQU(pSmsCmd->acCmdName, "01"))
				|| (STRING_EQU(pSmsCmd->acCmdName, "GET")))
			{
				SendGetSetHelpInfo(pSmsCmd, TRUE);
			}
			else if((STRING_EQU(pSmsCmd->acCmdName, "2"))
				|| (STRING_EQU(pSmsCmd->acCmdName, "02"))
				|| (STRING_EQU(pSmsCmd->acCmdName, "SET")))
			{
				SendGetSetHelpInfo(pSmsCmd, FALSE);
			}
			else if((STRING_EQU(pSmsCmd->acCmdName, "3"))
				|| (STRING_EQU(pSmsCmd->acCmdName, "03"))
				|| (STRING_EQU(pSmsCmd->acCmdName, "READ")))
			{
				SendReadHelpInfo(pSmsCmd);
			}
			else
			{
				sprintf(acRespond, "Unknown Command Name of HELP,%s,%s", pSmsCmd->acCmdCode, pSmsCmd->acEquipName);
				SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
			}
		
	}

	return;	
}
/*=====================================================================*
* Function name: HandleErrorCmd
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void HandleErrorCmd(SMS_COMMAND *pSmsCmd)
{
	char acRespond[TP_UD_MAX_LEN] = {0};
	sprintf(acRespond, "Unknown Command Code:%s", pSmsCmd->acCmdCode);
	SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
}
/*=====================================================================*
* Function name: HandleErrorCmd
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static void SMS_ErrorCommandHandle(SMS_COMMAND *pSmsCmd, char *pUserData, int iUnpackResult)
{
	UNUSED(iUnpackResult);
	char acRespond[TP_UD_MAX_LEN] = {0};
	sprintf(acRespond, "Unknown Command: %s", pUserData);
	SMS_SendShortMsg(pSmsCmd->acPhoneNum, acRespond);
}

/*=====================================================================*
* Function name: ParseSmsInfoProc
* Description  : 
* Arguments    : 
* Return type  : int 
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int ParseSmsInfoProc(char *szBuf, char *pData)
{
	ASSERT(szBuf);
	ASSERT(pData);

	char *pField;
	SMS_SMS_INFO	*pRecord = (SMS_SMS_INFO *)pData;

	// 1.Sms No.
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->iSmsNo = atoi(pField);

	// 2.Sig Num
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->iSigNum = atoi(pField);

	// 3.Signal info

	pRecord->pSigInfo = NEW(SMS_SIG_INFO, pRecord->iSigNum);
	if (pRecord->pSigInfo == NULL)
	{
		AppLogOut(SMS_TASK, APP_LOG_ERROR, 
			"[%s]--ParseSmsInfoProc: ERROR: no memory to load "
			"SMS signal info!\r\n", __FILE__);

		return ERR_CFG_NO_MEMORY;
	}
	ZERO_POBJS(pRecord->pSigInfo, pRecord->iSigNum);

	// Cut to many signals
	int i;
	char *pSig;
	SMS_SIG_INFO *pSigInfo;

	//szBuf ->	//1,1,1;2,2,2;3,3,3;
	pSigInfo = pRecord->pSigInfo;
	for(i = 0; i < pRecord->iSigNum; i++, pSigInfo++)
	{
		szBuf = Cfg_SplitStringEx(szBuf, &pField, CHAR_SEMICOLON);
		//pField -> 1,1,1
		//szBuf -> 2,2,2;3,3,3;

		if(pField == NULL)
		{
			AppLogOut(SMS_TASK, APP_LOG_ERROR, 
				"SMS(%d) Config Error. Signal(%d) info is NOT exist.\r\n", pRecord->iSmsNo, i);
			return ERR_CFG_BADCONFIG;
		}

		pField = Cfg_SplitStringEx(pField, &pSig, CHAR_COMMA);
		//pSig -> 1
		//pField -> 1,1
		pSigInfo->iEquipId = atoi(pSig);

		pField = Cfg_SplitStringEx(pField, &pSig, CHAR_COMMA);
		pSigInfo->iSigType = atoi(pSig);

		pField = Cfg_SplitStringEx(pField, &pSig, CHAR_COMMA);
		pSigInfo->iSigId = atoi(pSig);
	}


	return ERR_CFG_OK;
}
/*=====================================================================*
* Function name: ParseSmsAlarmProc
* Description  : 
* Arguments    : 
* Return type  : int 
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int ParseSmsAlarmProc(char *szBuf, char *pData)
{
    ASSERT(szBuf);
    ASSERT(pData);

    char *pField;
    SMS_ALARM_INFO	*pRecord = (SMS_ALARM_INFO *)pData;

    // 1.No.
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    pRecord->iAlarmListNo = atoi(pField);

    // 2.SMS content
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    ASSERT(strlen(pField) <= SMS_ALARM_LIST_LEN);
    strncpy(pRecord->acSMSAlarmNameEn, pField, SMS_ALARM_LIST_LEN);

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    pRecord->stSigInfo.iEquipId = atoi(pField);

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    pRecord->stSigInfo.iSigType = atoi(pField);

    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    pRecord->stSigInfo.iSigId = atoi(pField);
    return ERR_CFG_OK;
}
/*=====================================================================*
* Function name: ParseGetSetCmdProc
* Description  : 
* Arguments    : 
* Return type  : int 
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int ParseGetSetCmdProc(char *szBuf, char *pData)
{
	ASSERT(szBuf);
	ASSERT(pData);

	char *pField;
	SMS_GET_SET_CMD_INFO	*pRecord = (SMS_GET_SET_CMD_INFO *)pData;

	// 1.Command No
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->iCmdNo = atoi(pField);

	// 2.Command Name, English
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	ASSERT(strlen(pField) <= SMS_EQUIP_NAME_MAX_LEN);
	strncpy(pRecord->acCmdNameEn, pField, SMS_CMD_NAME_MAX_LEN);

#ifdef __CFG_SIG_NAME_TO_UPPER
	StringToUpper(pRecord->acCmdNameEn);
#endif

	// 3.Command Name, Chinese
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	ASSERT(strlen(pField) <= SMS_EQUIP_NAME_MAX_LEN);
	strncpy(pRecord->acCmdNameZh, pField, SMS_CMD_NAME_MAX_LEN);

	/* 4.Equip ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->stSigInfo.iEquipId = atoi(pField);

	/* 5.Signal Type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->stSigInfo.iSigType = atoi(pField);

	/* 6.Signal ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->stSigInfo.iSigId = atoi(pField);

	/* 7.GET */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->bGet = (BOOL)atoi(pField);

	/* 8.SET */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->bSet = (BOOL)atoi(pField);

	return ERR_CFG_OK;
}

/*=====================================================================*
* Function name: ParseReadCmdProc
* Description  : 
* Arguments    : 
* Return type  : int 
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int ParseReadCmdProc(char *szBuf, char *pData)
{
	ASSERT(szBuf);
	ASSERT(pData);

	char *pField;
	char *pSpeCode;
	SMS_READ_CMD_INFO	*pRecord = (SMS_READ_CMD_INFO*)pData;

	// 1.Command No
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->iCmdNo = atoi(pField);

	// 2.Command Name, English
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	ASSERT(strlen(pField) <= SMS_EQUIP_NAME_MAX_LEN);
	strncpy(pRecord->acCmdNameEn, pField, SMS_CMD_NAME_MAX_LEN);

#ifdef __CFG_SIG_NAME_TO_UPPER
	StringToUpper(pRecord->acCmdNameEn);
#endif

	// 3.Command Name, Chinese
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	ASSERT(strlen(pField) <= SMS_EQUIP_NAME_MAX_LEN);
	strncpy(pRecord->acCmdNameZh, pField, SMS_CMD_NAME_MAX_LEN);

	// 4.SMS Num
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pRecord->iSmsNum = atoi(pField);

	if(pRecord->iSmsNum == 0)
	{
		//pSpeCode = TrimLeft(szBuf, SPLITTER);
		pSpeCode = Cfg_RemoveWhiteSpace(szBuf);

		if(STRING_EQU(pSpeCode, "ACTIVE_ALARM"))
		{
			pRecord->iSmsNum = -1;
			return ERR_CFG_OK;
		}
		else
		{
			AppLogOut(SMS_TASK, APP_LOG_ERROR, 
				"SMS info of READ command Config Error. cmd no = %d, cmd name is %s.\r\n",
				pRecord->iCmdNo, pRecord->acCmdNameEn);
			return ERR_CFG_BADCONFIG;
		}
	}
	ASSERT(pRecord->iSmsNum > 0);

	// 5.SMS info list
	//szBuf -> 1,2,3,4

	pRecord->piSmsList = NEW(int, pRecord->iSmsNum);
	if (pRecord->piSmsList == NULL)
	{
		AppLogOut(SMS_TASK, APP_LOG_ERROR, 
			"[%s]--ParseReadCmdProc: ERROR: no memory to load "
			"SMS signal info!\r\n", __FILE__);

		return ERR_CFG_NO_MEMORY;
	}
	ZERO_POBJS(pRecord->piSmsList, pRecord->iSmsNum);

	int i;
	for(i = 0; i < pRecord->iSmsNum; i++)
	{
		szBuf = Cfg_SplitStringEx(szBuf, &pField, CHAR_COMMA);
		//pField -> 1
		//szBuf -> 2,3,4

		if(pField == NULL)
		{
			AppLogOut(SMS_TASK, APP_LOG_ERROR, 
				"SMS info of READ command Config Error. cmd no = %d, cmd name is %s.\r\n",
				pRecord->iCmdNo, pRecord->acCmdNameEn);
			return ERR_CFG_BADCONFIG;
		}
		else
		{
			*(pRecord->piSmsList + i) = atoi(pField);
		}
	}

	return ERR_CFG_OK;
}
/*=====================================================================*
* Function name: LoadSMSCfgProc
* Description  : 
* Arguments    : 
* Return type  : int 
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int LoadSMSCfgProc(void *pCfg, void *pLoadToBuf)
{
	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	SMS_PRIVATE_CONFIG *pBuf;
	pBuf = (SMS_PRIVATE_CONFIG *)pLoadToBuf;

	CONFIG_TABLE_LOADER loader[4];

	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pBuf->stSmsList.iSmsNum),
		"[SIG_INFO_OF_SMS]", 
		&(pBuf->stSmsList.pSmsInfo), 
		ParseSmsInfoProc);

	DEF_LOADER_ITEM(&loader[1],
	    NULL,
	    &(pBuf->stAlarmList.iAlarmListNum),
	    "[SMS_ALARM_LIST]", 
	    &(pBuf->stAlarmList.pSmsAlarmInfo), 
	    ParseSmsAlarmProc);

	DEF_LOADER_ITEM(&loader[2],
		NULL,
		&(pBuf->stGetSetCmdList.iCmdNum),
		"[GET_SET_COMMAND]", 
		&(pBuf->stGetSetCmdList.pGetSetCmdInfo), 
		ParseGetSetCmdProc);
		
	DEF_LOADER_ITEM(&loader[3],
		NULL,
		&(pBuf->stReadCmdList.iCmdNum),
		"[READ_COMMAND]", 
		&(pBuf->stReadCmdList.pReadCmdInfo), 
		ParseReadCmdProc);		
		
	if (Cfg_LoadTables(pCfg,sizeof(loader)/sizeof(*loader),loader) 
		!= ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}
	
	return ERR_CFG_OK;

}

/*=====================================================================*
* Function name: SMS_ReadPrivateCfg
* Description  : 
* Arguments    : 
* Return type  : int 
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
int SMS_ReadPrivateCfg(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_SMS, 
		szCfgFileName, MAX_FILE_PATH);

	ret = Cfg_LoadConfigFile(szCfgFileName, 
		LoadSMSCfgProc,
		&g_SMSGlobals.SMSPrivateConfig);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	// print to check
	SMS_PrintCfgFile();

	return ERR_CFG_OK;
}


/*=====================================================================*
* Function name: MyWrite
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int  MyWrite(int fd,  BYTE* pBuffer, int nBytesToWrite)
{
	int bytes_left, written_bytes;
	char * ptr;
	ptr = (char *)pBuffer;
	bytes_left = nBytesToWrite;
	int iwriteTime;

	iwriteTime = 0;
	while(bytes_left>0)
	{

	    if((g_SMSGlobals.bHasModem == 0) || (iwriteTime >= 3))
	    {
				return -1;
				break;
	    }
	    
			written_bytes = write(fd, ptr, bytes_left);

			if(written_bytes<=0)
			{
				/*
				if(errno == EINTR)
				{
				written_bytes = 0;
				}
				else 
				{
				printf("MyWrite function when write the comm error\n");
				return -1;
				}
				*/
			  iwriteTime++; // 防止MODEM出错后线程陷在本函数
				printf("written_bytes=%d",written_bytes);
				sleep(2);//yield
				written_bytes = 0;

			}
			MyDelay(50);
			bytes_left -= written_bytes;
			ptr += written_bytes;
		//	printf("write_bytes= %d , bytes_left=%d\n", written_bytes,bytes_left);
	}


	return 0;

}
/*=====================================================================*
* Function name: SMS_ModemPortRead
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
static int  SMS_ModemPortRead(IN int fd, OUT BYTE* pBuffer, IN int nBytesToRead)
{
	return read(fd, pBuffer, nBytesToRead);
}
/*=====================================================================*
* Function name: SMS_SendCmd
* Description  : 
* Arguments    : 
* Return type  :  
*
* Create       : 
* Comment(s)   : 
*=====================================================================*/
BOOL SMS_SendCmd(char* pSendBuf, char* pReadBuf)
{
#ifdef __SMS_SELF_TEST
	return TRUE;
#endif

	if(!g_SMSGlobals.bHasModem)
	{
		return FALSE;
	}

	//SMS_ModemPortWrite(g_SMSGlobals.fdUsbModem, pSendBuf, strlen(pSendBuf));
	if(MyWrite(g_SMSGlobals.fdUsbModem, (BYTE *)pSendBuf, (int)strlen(pSendBuf)) == -1)
	{
		printf("write function is wrong\n");
		return FALSE;
	}
	Sleep(500);//yield

	int nBytes = SMS_ModemPortRead(g_SMSGlobals.fdUsbModem, (BYTE *)pReadBuf, AT_CMD_MAX_LEN);
	pReadBuf[nBytes] = (char)NULL;

	RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);	
	//Sleep(2000);
	Sleep(500);//yield

	return TRUE;
}
/*==========================================================================*
* FUNCTION : SendShortMsg
* PURPOSE  : // 生成PDU包，并发送
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char  *pszTo   : 
*            char  *pszData : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2010-04-27 15:34
*==========================================================================*/
static BOOL SMS_SendShortMsg(char *pszTo, char *pszData)
{
	TRACE("****SMS**** Send To %s:\n%s \n", pszTo, pszData);

#ifdef __SMS_SELF_TEST
	return TRUE;
#endif

	//int nResult = -1;
	//int nLen = strlen(pszData);

	char szSend[AT_CMD_MAX_LEN];
	char szRead[AT_CMD_MAX_LEN];



	//BOOL bCanSend = FALSE;
	BOOL Sendok = FALSE;

	int i = 0;
	while(i++ < 3)
	{
	    sprintf(szSend, "%s%c", "AT+CMGF=1", CHAR_CR);
	    SMS_SendCmd(szSend, szRead);
	    
			snprintf(szSend, AT_CMD_MAX_LEN, "AT+CMGS=\"%s\"%c", pszTo, CHAR_CR);
	
			SMS_SendCmd(szSend, szRead);
			//TRACE("[SendShortMsg Cmd1:] %s\n", szSend);
			//TRACE("[SendShortMsg Recv1:] %s\n", szRead);
			printf("[SendShortMsg Cmd1:] %s\n", szSend);
			printf("[SendShortMsg Recv1:] %s\n", szRead);

		/* if((strchr(szRead, '>') != NULL)
		|| (strstr(szRead, "OK") != NULL))*/

		Sleep(2000);//yield
		//{
		//		bCanSend = TRUE;
		//	break;
		//}

		//	Sleep(2000);
		SMS_HEART_BEAT;
		//	}

		//	if(bCanSend)
		//	{
		snprintf(szSend, TP_UD_MAX_LEN, "%s%c", pszData, CHAR_CTRL_Z);
		SMS_SendCmd(szSend, szRead);
		printf("[SendShortMsg Cmd1:] %s\n", szSend);
		printf("[SendShortMsg Recv1:] %s\n", szRead);

		//TRACE("[SendShortMsg Cmd2:] %s\n", szSend);
		//TRACE("[SendShortMsg Recv2:] %s\n", szRead);
		if(strstr(szRead, "OK") != NULL)
		{
			Sendok = TRUE;
			break;
		}

		Sleep(5000);//yield

		int nBytes = SMS_ModemPortRead(g_SMSGlobals.fdUsbModem, (BYTE *)szRead, AT_CMD_MAX_LEN);
		szRead[nBytes] = (char)NULL;
		RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);	
		printf("[SendShortMsg Recv1:] %s\n", szRead);

		if(strstr(szRead, "OK") != NULL)
		{
			Sendok = TRUE;
			break;
		}

		Sleep(10000);//yield

		nBytes = SMS_ModemPortRead(g_SMSGlobals.fdUsbModem, (BYTE *)szRead, AT_CMD_MAX_LEN);
		szRead[nBytes] = (char)NULL;
		RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);	
		printf("[SendShortMsg Recv1:] %s\n", szRead);

		if(strstr(szRead, "OK") != NULL)
		{
			Sendok = TRUE;
			break;
		}

		Sleep(20000);//yield

		nBytes = SMS_ModemPortRead(g_SMSGlobals.fdUsbModem, (BYTE *)szRead, AT_CMD_MAX_LEN);
		szRead[nBytes] = (char)NULL;
		RunThread_Heartbeat(g_SMSGlobals.hThreadSelf);	
		printf("[SendShortMsg Recv1:] %s\n", szRead);

		if(strstr(szRead, "OK") != NULL)
		{
			Sendok = TRUE;
			break;
		}

	}
	//else
	if(Sendok !=TRUE)
	{
		TRACE("[SMS]: Fail to send msg.\n");
		return FALSE;
	}

	Sleep(2000);//yield
	return TRUE;
}

/*==========================================================================*
* FUNCTION : DeleteAllMsg
* PURPOSE  :
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static BOOL DeleteAllMsg(void)
{
	char szSend[AT_CMD_MAX_LEN] = "";
	char szRead[AT_CMD_MAX_LEN] = "";

	snprintf(szSend, AT_CMD_MAX_LEN, "AT+CMGD=1,4%c", CHAR_CR);
	SMS_SendCmd(szSend, szRead);

	TRACE("[CDMA Recv after Remove all:] %s\n", szRead);

	return TRUE;

}

/*==========================================================================*
* FUNCTION : StringToUpper
* PURPOSE  :
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static void StringToUpper(char *pszString)
{
    while(*pszString)
    {
	*pszString = TOUPPER(*pszString);
	pszString++;
    }
}
/*==========================================================================*
* FUNCTION : SMS_PrintCfgFile
* PURPOSE  :
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
//#define __PRINT_CONFIG_FILE
static void SMS_PrintCfgFile(void)
{
#ifdef __PRINT_CONFIG_FILE
    SMS_PRIVATE_CONFIG *pPriCfg = &g_SMSGlobals.SMSPrivateConfig;
    int i, j, k;

    SMS_SMS_INFO	*pSmsInfo;
    SMS_SIG_INFO	*pSigInfo;

    SMS_GET_SET_CMD_INFO *pGetSetCmdInfo;
    SMS_READ_CMD_INFO	*pReadCmdInfo;
    SMS_ALARM_INFO	*pReadAlarmListInfo;

    
    printf("\n ============SMS INFO==============\n");
    for(i = 0; i < pPriCfg->stSmsList.iSmsNum; i++)
    {
	pSmsInfo = pPriCfg->stSmsList.pSmsInfo + i;
	printf("%d \t%d \t", 
	    pSmsInfo->iSmsNo, pSmsInfo->iSigNum);

	for(j = 0; j < pSmsInfo->iSigNum; j++)
	{
	    pSigInfo = pSmsInfo->pSigInfo + j;
	    printf("%d,%d,%d;",
		pSigInfo->iEquipId, pSigInfo->iSigType, pSigInfo->iSigId);
	}

	printf("\n");
    }

    // Cmd info.....
	printf("============GET SET CMD INFO====\n");

	for(j = 0; j < pPriCfg->stGetSetCmdList.iCmdNum; j++)
	{
	    pGetSetCmdInfo = pPriCfg->stGetSetCmdList.pGetSetCmdInfo + j;
	    printf("%d\t%s\t%s\t%d\t%d\t%d\t%d\t%d\n",
		pGetSetCmdInfo->iCmdNo,
		pGetSetCmdInfo->acCmdNameEn,
		pGetSetCmdInfo->acCmdNameZh,
		pGetSetCmdInfo->stSigInfo.iEquipId,
		pGetSetCmdInfo->stSigInfo.iSigType,
		pGetSetCmdInfo->stSigInfo.iSigId,
		pGetSetCmdInfo->bGet,
		pGetSetCmdInfo->bSet);
	}

	printf("============READ CMD INFO====\n");
	for(j = 0; j < pPriCfg->stReadCmdList.iCmdNum; j++)
	{
	    pReadCmdInfo = pPriCfg->stReadCmdList.pReadCmdInfo + j;
	    printf("%d\t%s\t%s\t%d\t",
		pReadCmdInfo->iCmdNo,
		pReadCmdInfo->acCmdNameEn,
		pReadCmdInfo->acCmdNameZh,
		pReadCmdInfo->iSmsNum);
	    for(k = 0; k < pReadCmdInfo->iSmsNum; k++)
	    {
		printf("%d,", *(pReadCmdInfo->piSmsList + k));
	    }
	    printf("\n");
	}

	printf("============READ ALARM LIST INFO====\n");
	for(j = 0; j < pPriCfg->stAlarmList.iAlarmListNum; j++)
	{
	    pReadAlarmListInfo = pPriCfg->stAlarmList.pSmsAlarmInfo + j; 
	    printf("%d\t%s\t%d\t%d\t%d",  pReadAlarmListInfo->iAlarmListNo, pReadAlarmListInfo->acSMSAlarmNameEn,
		pReadAlarmListInfo->stSigInfo.iEquipId, pReadAlarmListInfo->stSigInfo.iSigType, pReadAlarmListInfo->stSigInfo.iSigId);
	    printf("\n");
	}

#endif

    return;
}

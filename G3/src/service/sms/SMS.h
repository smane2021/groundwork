/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : Controller Family Framework
*
*  FILENAME : SMS.h
*  CREATOR  : Samson Fan               DATE: 2009-07-06 09:51
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/


#ifndef _SMS_H_
#define _SMS_H_



#include	"basetypes.h"

#include	"stdlib.h"
#include	"stdio.h"
#include	"stdsys.h"
#include	"new.h"
#include	"public.h"


//Used to record log
#ifndef SMS_TASK
#define SMS_TASK		"SMS"
#endif //SMS_TASK


#define SPLITTER					('\t')
#define CHAR_SEMICOLON					(';')
#define CHAR_COMMA					(',')

#define		AT_CMD_MAX_LEN				500
#define		CHAR_CR					'\r'		//0x0D		//
#define		CHAR_CTRL_Z				'\x1A'		//0x1A		//

#define		MSG_FIELD_SPLIT				"\x0D\x0A"
#define		MSG_SECTION_SPLITTER			','

#define SMS_ALARM_LIST_LEN		50
#define SMS_PRI_CFG_SIG_LEN		21


// ����ÿ���ź�
typedef struct tagSMS_SIG_INFO
{
	int		iEquipId;
	int		iSigType;
	int		iSigId;
}SMS_SIG_INFO;

// �豸��Ϣ
typedef struct tagSMS_ALARM_INFO
{
	int		iAlarmListNo;
	SMS_SIG_INFO	stSigInfo;
	char	acSMSAlarmNameEn[SMS_ALARM_LIST_LEN];
}SMS_ALARM_INFO;


// ����ÿ�����Ŷ�Ӧ���ź���Ϣ

typedef struct tagSMS_SMS_INFO
{
	int		iSmsNo;
	int		iSigNum;
	SMS_SIG_INFO		*pSigInfo;
}SMS_SMS_INFO;
typedef struct tagSMS_SMS_LIST
{
	int		iSmsNum;
	SMS_SMS_INFO		*pSmsInfo;
}SMS_SMS_LIST;  //[SIG_INFO_OF_SMS]

typedef struct tagSMS_ALARM_LIST
{
    int			iAlarmListNum;
    SMS_ALARM_INFO	*pSmsAlarmInfo;
}SMS_ALARM_LIST;  //[SMS_ALARM_LIST]





// GET/SET����
typedef struct tagSMS_GET_SET_CMD_INFO		//	GET/SET�������Ϣ
{
	int		iCmdNo;							//	�����
	char	acCmdNameEn[SMS_PRI_CFG_SIG_LEN];	//Ӣ���ź�����
	char	acCmdNameZh[SMS_PRI_CFG_SIG_LEN];	//�����ź�����
	SMS_SIG_INFO	stSigInfo;				//	�������Ӧ���ź���Ϣ
	BOOL	bGet;							//	��������GET
	BOOL	bSet;							//	��������SET
}SMS_GET_SET_CMD_INFO;

typedef struct tagSMS_GET_SET_CMD_LIST		//	������������źŵĹ�ϵ
{
	int		iCmdNum;							//	��ǰϵͳ�����õ�����Ÿ���
	SMS_GET_SET_CMD_INFO	*pGetSetCmdInfo;	//	��ǰϵͳ�����õ��������Ϣ
}SMS_GET_SET_CMD_LIST;

// READ����
typedef struct tagSMS_READ_CMD_INFO			//	READ�������Ϣ
{
	int		iCmdNo;							//	�����
	char	acCmdNameEn[SMS_PRI_CFG_SIG_LEN];	//Ӣ����������
	char	acCmdNameZh[SMS_PRI_CFG_SIG_LEN];	//������������
	int		iSmsNum;		// -1:ACTIVE_ALARM
	int		*piSmsList;		// ע��:��int��ָ�룬ָ��SMS��Ϣ������б�
}SMS_READ_CMD_INFO;
typedef struct tagSMS_READ_CMD_LIST
{
	int		iCmdNum;
	SMS_READ_CMD_INFO		*pReadCmdInfo;
}SMS_READ_CMD_LIST;

#define		AT_CMD_MAX_LEN			500

#define TP_UD_MAX_LEN					140									//PDU���е��û�������󳤶�
#define SMS_CMD_CODE_MAX_LEN	8										//��������󳤶�
#define SMS_EQUIP_NAME_MAX_LEN	20									//�豸���ƻ���� -- Ԥ��
#define SMS_CMD_NAME_MAX_LEN	20									//�������ƻ������󳤶�
#define SMS_CMD_PARA_MAX_LEN	64									//������󳤶�
#define PHONE_NO_STRING_LEN		20									//�绰�����ַ�������

typedef struct tagSMSCommand				//	���������ַ���
{
	//CHAR	acUserName[16];					//	�û���
	//CHAR	acPassword[16];					//	����
	int		iUserLevel;

	CHAR	acCmdCode[SMS_CMD_CODE_MAX_LEN];//	������
	char	acEquipName[SMS_EQUIP_NAME_MAX_LEN];//	�豸���ƻ������  -- Ԥ��
	CHAR	acCmdName[SMS_CMD_NAME_MAX_LEN];	//	�������ƻ������
	CHAR	acCmdPara[SMS_CMD_PARA_MAX_LEN];	//	�������
	CHAR	acPhoneNum[PHONE_NO_STRING_LEN];	//�û��绰����
} SMS_COMMAND;

enum eCmdCode
{
	SMS_CMD_CODE_GET = 0,
	SMS_CMD_CODE_SET,
	SMS_CMD_CODE_READ,
	SMS_CMD_CODE_HELP,

	SMS_CMD_CODE_MAX,
};


enum eModemType
{
	MODEM_TYPE_NONE,
	MODEM_TYPE_GSM,
	MODEM_TYPE_CDMA,

	MODEM_TYPE_MAX,
};


enum eSpecialCmdNo
{
	SPECIAL_CMD_NO_IP = 1,
	SPECIAL_CMD_NO_SUBMASK,
	SPECIAL_CMD_NO_GATEWAY,

	SPECIAL_CMD_NO_ALARM_NUM1 = 5,
	SPECIAL_CMD_NO_ALARM_NUM2,
	SPECIAL_CMD_NO_ALARM_NUM3,
	

	SPECIAL_CMD_NO_ACTIVE_ALARM = 999,

};

enum eSMS_ERR_CODE
{
	ERR_SMS_OK = ERR_OK,
	ERR_SMS_STRING_TOO_SHORT,
	ERR_SMS_NO_CMD_CODE,
	ERR_SMS_NO_EQUIP,
	ERR_SMS_NO_CMD_NO,
};



//�����ӿ�����

DWORD ServiceInit(IN int argc, IN char **argv);

DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs);

#endif

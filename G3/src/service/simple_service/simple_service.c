/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : simple_service.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-16 15:04
 *  VERSION  : V1.00
 *  PURPOSE  : A simple service, sample of application service of ACU
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"


/* define log tasks */
#define SIMPLE_SERVICE    "Simple Service"


#define ENV_SERVICE_SHOW_MSG	"SIMPLE_SERVICE_SHOW_MSG"
static	BOOL		s_bKeepSilence = TRUE;

#undef TRACEX		// always print msg in this sample service.
#ifndef TRACEX
#define TRACEX		s_bKeepSilence ? 0 : printf
#endif

//#define _TEST_RECT_COMM		1

/*==========================================================================*
 * FUNCTION : ServiceNotification
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService        : 
 *            int     nMsgType        : 
 *            int     nTrapDataLength : 
 *            void*   lpTrapData      : 
 *            void*   lpParam         :  
 *            BOOL    bUrgent         : 
 * RETURN   : int : 0 for success, 1 for error(defined in DXI module)
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-22 17:23
 *==========================================================================*/
int ServiceNotification(HANDLE hService, 
						int nMsgType, 
						int nTrapDataLength, 
						void *lpTrapData,	
						void *lpParam, 
						BOOL bUrgent)
{
	UNUSED(hService);
	UNUSED(nTrapDataLength);
	UNUSED(lpTrapData);
	UNUSED(bUrgent);
	UNUSED(lpParam);
	UNUSED(nMsgType);

	TRACEX("[%s] received a trap, code is %d(0x%x).\n", 
		SIMPLE_SERVICE,	nMsgType, nMsgType);

	return 0;
}


#ifdef _TEST_RECT_COMM

#define RECT_LOG_FILE	"lost_rect.txt"

static BOOL SimpleService_CheckRectifierNum(HANDLE hSelf)
{
	int		nEquipID = DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID);
	int		nTotalMountedRect = 0, nNormalRect = 0;
	char	*pszEnvTotalRect;

	//1. get the num of rectifiers
	int iRectifierNum = 0, nBufLen;
	SIG_BASIC_VALUE  *pSigValue;

	UNUSED(hSelf);

	pszEnvTotalRect = getenv("TOTAL_RECT");
	if (pszEnvTotalRect != NULL)
	{
		nTotalMountedRect = atoi(pszEnvTotalRect);
	}

	if (nTotalMountedRect <= 0)
	{
		nTotalMountedRect = 11; //default.
	}

	/* to get the actual rectifier number */
	if ((DxiGetData(VAR_A_SIGNAL_VALUE,
		nEquipID,	
		SIG_ID_ACTUAL_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) != ERR_DXI_OK) || !SIG_VALUE_IS_VALID(pSigValue))
	{
		return TRUE;	// continue;
	}

	iRectifierNum = pSigValue->varValue.ulValue;

	// get number of rect in normal state 
	if ((DxiGetData(VAR_A_SIGNAL_VALUE,
		nEquipID,	
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 6),
		&nBufLen,			
		&pSigValue,			
		0) != ERR_DXI_OK) || !SIG_VALUE_IS_VALID(pSigValue))
	{
		return TRUE;	// continue;
	}

	nNormalRect = pSigValue->varValue.ulValue;

	if ((iRectifierNum < nTotalMountedRect) || 
		(nNormalRect < iRectifierNum))
	{
		FILE	*fp = NULL;
		char	szTime[32];

		TimeToString(time(NULL), TIME_CHN_FMT, szTime, sizeof(szTime));

		fp = fopen(RECT_LOG_FILE, "a");
		if (fp != NULL)
		{
			fprintf(fp, "%s: Rectifiers total configured: %d, found: %d, comm normal %d.\n",
				szTime, nTotalMountedRect, iRectifierNum, nNormalRect);
			fclose(fp);
		}

		return FALSE;
	}

	return TRUE;	// continue;
}
#endif


/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SERVICE_ARGUMENTS *  pArgs : 
 * RETURN   : DWORD : service exit code, defined in app_service.h
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-21 19:30
 *==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{
	int			iRet;
	HANDLE		hSelf = RunThread_GetId(NULL);

	UNUSED(pArgs);

	TRACEX("[%s] is starting...\n", 
		SIMPLE_SERVICE);

	/* register in the DXI module to receive Alarms */
	iRet = DxiRegisterNotification(SIMPLE_SERVICE, 
			ServiceNotification,
			NULL, // it shall be changed to the actual arg, and be same as WHEN CANCEL 
			_ALARM_OCCUR_MASK|_ALARM_CLEAR_MASK|_SAMPLE_MASK|_SET_MASK|_CONTROL_MASK, 
			hSelf,
			_REGISTER_FLAG);

	if (iRet != 0)
	{
		TRACEX("[%s] fails on calling DxiRegisterNotification.\n",
			SIMPLE_SERVICE);

		return SERVICE_EXIT_FAIL;
	}


	// OK, let's go.
	while (THREAD_IS_RUNNING(hSelf))
	{
		// The service and its child threads which created by RunThread_Create
		// must call RunThread_Heartbeat periodly.
		RunThread_Heartbeat(hSelf);	// notify the thread manager.

		// TODO:
		// mainloop,,,
		Sleep(1000);

#ifdef _TEST_RECT_COMM
		if (SimpleService_CheckRectifierNum(hSelf))
		{
			static	int			nLeftRunningTime = 30;
			
			nLeftRunningTime--;
			if (nLeftRunningTime <= 0)
			{
				// reboot ACU and SCU
				if (getenv("REBOOT_SCU") != NULL)
				{
					printf("Reset SCU...\n");
					DXI_RebootACUorSCU(TRUE, FALSE);
				}

				system("/sbin/reboot");//quick reboot ACU

				break;
			}
		}
		else
		{
			break;	// do NOT check again.
		}
#endif
	}

	// unresigster
	DxiRegisterNotification(SIMPLE_SERVICE, 
			ServiceNotification,
			NULL, // it shall be changed to the actual arg, and be same as WHEN CANCEL 
			_ALARM_OCCUR_MASK|_ALARM_CLEAR_MASK|_SAMPLE_MASK|_SET_MASK|_CONTROL_MASK, 
			hSelf,
			_CANCEL_FLAG);

	TRACEX("[%s] now exited.\n", 
		SIMPLE_SERVICE);

	return SERVICE_EXIT_OK;
}



/*==========================================================================*
 * FUNCTION : ServiceInit
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int   argc   : 
 *            IN char  **argv : 
 * RETURN   : void *: return your private var, which will be save to 
 *                    SERVICE_ARGUMENTS.pReserved that will be passed to 
 *                    ServiceMain.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-16 15:12
 *==========================================================================*/
void *ServiceInit(IN int argc, IN char **argv)//args
{
	UNUSED(argc);
	UNUSED(argv);

	if (getenv(ENV_SERVICE_SHOW_MSG) != NULL)
	{
		s_bKeepSilence = FALSE;
	}
	else
	{
		printf("[%s]: Please use 'export %s=1' to show msg.\n",
			SIMPLE_SERVICE, ENV_SERVICE_SHOW_MSG);
	}

	TRACEX("[%s] is initializing...OK\n", 
		SIMPLE_SERVICE);

	return NULL;
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : service_provider.c
 *  CREATOR  : LinTao                   DATE: 2004-10-21 18:55
 *  VERSION  : V1.00
 *  PURPOSE  : provide ESR service
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "esr.h"
/* define log tasks */
#define LOG_INIT_ESR_SERVICE    "Init ESR service"


 
/* used to init state machines */
#define DEF_STATE_MACHINE(_p, _iStateID, _fnProc)  \
	      ((_p)->iStateID = (_iStateID), \
		   (_p)->fnStateProc = (_fnProc))



/* define the global variable of ESR service */
ESR_GLOBALS  g_EsrGlobals;

/* define the share-used simple events */
ESR_EVENT g_EsrDummyEvent, g_EsrTimeoutEvent;
ESR_EVENT g_EsrDiscEvent, g_EsrConnFailEvent, g_EsrConnEvent;

__INLINE static void InitStaticEvents()
{
	INIT_FRAME_EVENT(&g_EsrDummyEvent, 0, 0, TRUE, FALSE);
	INIT_NONEFRAME_EVENT(&g_EsrTimeoutEvent, ESR_TIMEOUT_EVENT);
	INIT_NONEFRAME_EVENT(&g_EsrDiscEvent, ESR_DISCONNECTED_EVENT);
	INIT_NONEFRAME_EVENT(&g_EsrConnFailEvent, ESR_CONNECT_FAILED_EVENT);
	INIT_NONEFRAME_EVENT(&g_EsrConnEvent, ESR_CONNECTED_EVENT);
}
/*==========================================================================*
 * FUNCTION : AlarmProc   
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ServiceNotification
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-26 19:26
 *==========================================================================*/
static int AlarmProc(ESR_BASIC_ARGS *pThis)
{
#ifdef _DEBUG_ESR_REPORT

	TRACE_ESR_TIPS("Alarm captured by ESR module");

	if (g_EsrGlobals.CommonConfig.bReportInUse)
	{
		TRACE("Alarm Report function is on, will report to MC.\n");
	}
#endif //_DEBUG_ESR_REPORT

	ESR_RegisterAlarm(pThis, FALSE);

	return 0;
}


/*==========================================================================*
 * FUNCTION : ServiceNotification
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService        : 
 *            int     nMsgType        : 
 *            int     nTrapDataLength : 
 *            void*   lpTrapData      : 
 *            void*   lpParam         :  
 *            BOOL    bUrgent         : 
 * RETURN   : int : 0 for success, 1 for error(defined in DXI module)
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-22 17:23
 *==========================================================================*/
int ServiceNotification(HANDLE hService, 
						int nMsgType, 
						int nTrapDataLength, 
						void *lpTrapData,	
						void *lpParam, 
						BOOL bUrgent)
{
	ESR_BASIC_ARGS *pBasicArgs = (ESR_BASIC_ARGS *)lpParam;

	/* Added by Thomas, 2006-06-06, for CR# 0133-06-ACU(suppress OA alarm report) */
	ALARM_SIG_VALUE *pAlarmSig = (ALARM_SIG_VALUE *)lpTrapData;

	UNUSED(hService);
	UNUSED(nTrapDataLength);
	UNUSED(lpTrapData);
	UNUSED(bUrgent);
	
	/* Modified by Thomas, 2006-06-06, for CR# 0133-06-ACU */
	/*if (nMsgType == _ALARM_OCCUR_MASK || nMsgType == _ALARM_CLEAR_MASK)*/
	if (pAlarmSig->iAlarmLevel >= ALARM_LEVEL_MAJOR && 
		(nMsgType == _ALARM_OCCUR_MASK || nMsgType == _ALARM_CLEAR_MASK))
	{
		return AlarmProc(pBasicArgs);
	}

	return 1;
}

/*==========================================================================*
 * FUNCTION : InitESRGlobals
 * PURPOSE  : assistant function to init g_EsrGlobals
 * CALLS    : 
 * CALLED BY: ServiceMain
 * RETURN   : DWORD : use SERVICE_EXIT_CODE const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-21 20:08
 *==========================================================================*/
static DWORD InitESRGlobals(void)
{
	STATE_INFO  *pStateInfo;

	/* 1.init config */
	if (ESR_InitConfig() != ERR_CFG_OK)
	{
		return SERVICE_EXIT_CFG;
	}


	/* create mutex for ESR Common Config file */
	/*g_ESRGlobals.hMutexCommonCfg = Mutex_Create(TRUE);
	if (g_EsrGlobals.hMutexCommonCfg == NULL)
	{
		AppLogOut(LOG_INIT_ESR_SERVICE, APP_LOG_ERROR, 
			"Create Mutex for ESR Common Config file failed\n");
		TRACE("[%s]--%s: ERROR: Create Mutex for ESR Common Config file"
			" failed.\n", __FILE__, __FUNCTION__);

			return SERVICE_EXIT_FAIL;
	}*/

	/* 2.init command handler */
	ESR_InitCmdHander();
	
	/* 3.init EEM state machine */
	pStateInfo = g_EsrGlobals.EEMStates;

	DEF_STATE_MACHINE(&pStateInfo[0], 
		EEM_IDLE, 
		EEM_OnIdle);

	DEF_STATE_MACHINE(&pStateInfo[1], 
		EEM_WAIT_FOR_POLL, 
		EEM_OnWaitForPoll);

	DEF_STATE_MACHINE(&pStateInfo[2],
		EEM_WAIT_FOR_RESP_ACK, 
		EEM_OnWaitForRespACK);

	DEF_STATE_MACHINE(&pStateInfo[3], 
		EEM_SEND_CALLBACK, 
		EEM_OnSendCallback);

	DEF_STATE_MACHINE(&pStateInfo[4],
		EEM_WAIT_FOR_CALLBACK_ACK,
		EEM_OnWaitForCallbackACK);

	DEF_STATE_MACHINE(&pStateInfo[5], 
		EEM_SEND_ALARM, 
		EEM_OnSendAlarm);

	DEF_STATE_MACHINE(&pStateInfo[6],
		EEM_WAIT_FOR_ALARM_ACK, 
		EEM_OnWaitForAlarmACK);


	/* 4.init SOC state machine */
	pStateInfo = g_EsrGlobals.SOCStates;

	DEF_STATE_MACHINE(&pStateInfo[0], 
		SOC_OFF, 
		SOC_OnOff);

	DEF_STATE_MACHINE(&pStateInfo[1], 
		SOC_SEND_ENQ, 
		SOC_OnSendENQ);

	DEF_STATE_MACHINE(&pStateInfo[2], 
		SOC_WAIT_DLE0, 
		SOC_OnWaitDLE0);

	DEF_STATE_MACHINE(&pStateInfo[3],
		SOC_ALARM_REPORT, 
		SOC_OnAlarmReport);

	DEF_STATE_MACHINE(&pStateInfo[4], 
		SOC_WAIT_CMD, 
		SOC_OnWaitCmd);

	DEF_STATE_MACHINE(&pStateInfo[5], 
		SOC_WAIT_ACK, 
		SOC_OnWaitACK);

	return SERVICE_EXIT_NONE;
}

#ifdef AUTO_CONFIG
void MainModuleMsgHandle(ESR_BASIC_ARGS *pThis)
{
	int	iRst;
    RUN_THREAD_MSG msg;

    iRst = RunThread_GetMessage(RunThread_GetId(NULL), &msg, FALSE, 0);

    if(iRst == ERR_OK)
    {
        TRACE("*****EEM*****msg.dwMsg received by EEM is: %d\n",msg.dwMsg);		//to delete
        if(msg.dwMsg == MSG_AUTOCONFIG)
        {
            switch(msg.dwParam1)
            {
            case ID_AUTOCFG_START:
				TRACE("*****EEM*****START AUTO CONFIG!!!\n");		//to delete
                break;

            case ID_AUTOCFG_REFRESH_DATA:
                TRACE("*****EEM*****START TO RECREATE MODEL!\n");		//to delete
                pThis->bAutoCfg = TRUE;
                break;

            default:
                break;
            }
		}
    }


	if (pThis->iMachineState == 0 && pThis->bAutoCfg)   //should be in IDEL state!
	{
		//re-create EEM block info
		DELETE(g_EsrGlobals.EEMModelInfo.pEEMBlockInfo);

		//if (CreateEEMBlocks(12) != ERR_CFG_OK)
		if (CreateEEMBlocks(0) != ERR_CFG_OK)//?a��?2?D����a�̨���y
		{
			AppLogOut(LOG_INIT_ESR_SERVICE, APP_LOG_ERROR, "Re-create EEM Block info "
			"failed during Auo-config processing.\n");

			pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
		}
		ESR_EquipListChange(TRUE);
		pThis->bAutoCfg = FALSE;

	}

	return;
}
#endif //AUTO_CONFIG
/*==========================================================================*
 * FUNCTION : StateMachineLoop
 * PURPOSE  : for main loop of state machine
 * CALLS    : 
 * CALLED BY: ServiceMain
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : DWORD : use SERVICE_EXIT_CODE const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-22 15:01
 *==========================================================================*/
static DWORD StateMachineLoop(ESR_BASIC_ARGS *pThis)
{
	int iLastState;
	int *piState;
	STATE_INFO *pCurStateMachine;

	piState = &(pThis->iMachineState);
	while (*(pThis->pOutQuitCmd) == SERVICE_CONTINUE_RUN && 
		pThis->iInnerQuitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		RunThread_Heartbeat(pThis->hThreadID[0]);

		if (g_EsrGlobals.CommonConfig.iProtocolType == EEM)
		{
			pCurStateMachine = g_EsrGlobals.EEMStates;
			*piState = EEM_IDLE;

			TRACE_ESR_TIPS("Begin EEM state machine");
		}
		else  /* SOC/TPE or RSOC protocol */
		{
			pCurStateMachine = g_EsrGlobals.SOCStates;
			*piState = SOC_OFF;

			TRACE_ESR_TIPS("Begin SOC state machine");
		}


		while (*piState != STATE_MACHINE_QUIT)
		{
			/* for TRACE */
			iLastState = *piState;

			/* process one state */
			*piState = pCurStateMachine[*piState].fnStateProc(pThis);
			//printf("Now the SOCTPE statemachine is %d\n",*piState);		// zzc debug 
			
#ifdef AUTO_CONFIG
			MainModuleMsgHandle(pThis);
#endif //AUTO_CONFIG
			if((pThis->iMachineState == 0)&&(TRUE==ESR_EquipListChange(FALSE)))
			{

				pThis->bEquipListChange = TRUE;
				DELETE(g_EsrGlobals.EEMModelInfo.pEEMBlockInfo);

				//if (CreateEEMBlocks(12) != ERR_CFG_OK)
				if (CreateEEMBlocks(10) != ERR_CFG_OK)// 10�̨���y�����?1����¡��䨬??��D?
				{
					AppLogOut(LOG_INIT_ESR_SERVICE, APP_LOG_ERROR, "Re-create EEM Block info "
						"failed during EquipList processing.\n");

					pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
				}
				ESR_EquipListChange(TRUE);

				pThis->bEquipListChange = FALSE;

			}

			/* check quit conditions */
			if (*(pThis->pOutQuitCmd) == SERVICE_STOP_RUNNING ||
				pThis->iInnerQuitCmd != SERVICE_EXIT_NONE ||
				pThis->bProtocolChangedFlag)
			{
				TRACE_ESR_TIPS("Exit state machine loop");
				TRACE("OutQuitCmd: %d\tInnerQuitCmd: %d\tProtocol Changed "
					"Flag: %d\n", *(pThis->pOutQuitCmd), pThis->iInnerQuitCmd,
					pThis->bProtocolChangedFlag);

				*piState = STATE_MACHINE_QUIT;

				/* reset flag */
				if (pThis->bProtocolChangedFlag)
				{
					pThis->bProtocolChangedFlag = FALSE;
					pThis->bFrameDone = TRUE;
				}

			}

#ifdef _SHOW_ESR_CUR_STATE
			if (*piState != iLastState)
			{
				TRACE_ESR_TIPS("Change to another State");
				ESR_PrintState(*piState);
			}
#endif //_SHOW_ESR_CUR_STATE

		}
	}

	if (pThis->iInnerQuitCmd != SERVICE_EXIT_NONE)
	{
		return pThis->iInnerQuitCmd;
	}

	return SERVICE_EXIT_OK;
}


/*==========================================================================*
 * FUNCTION : RoutineBeforeExitService
 * PURPOSE  : to do clean work before service exit
 * CALLS    : ESR_ClearEventQueue
 * CALLED BY: ServiceMain
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-22 15:53
 *==========================================================================*/
static void RoutineBeforeExitService(ESR_BASIC_ARGS *pThis)
{
	int i;
	/*HANDLE hMutex;*/

	EEMMODEL_CONFIG_INFO *pModelConfig;
	TYPE_MAP_INFO *pTypeMap;
	ALARM_INFO *pAlarmBuf;

	/* 1.unregister in DXI module(it is safe even when not registered) */
	DxiRegisterNotification("ESR Service", 
							ServiceNotification,
							pThis, 							  
							_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 							  
							pThis->hThreadID[0],							  
							_CANCEL_FLAG);

	/* 2.kill all the timers */
	for (i = 1; i < ESR_TIMER_NUM; i++)
	{
		Timer_Kill(pThis->hThreadID[0], i);
	}

	/* 3.stop child thread(it is safe even when child thread is not running) */
	if (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;
	}
	/* wait 5s */
	RunThread_Stop(pThis->hThreadID[1], 5000, TRUE);
	pThis->hThreadID[1] = NULL;


	/* 4.destroy event queues */
	ESR_ClearEventQueue(pThis->hEventInputQueue, TRUE);
	ESR_ClearEventQueue(pThis->hEventOutputQueue, TRUE);
		

	/* 5.Free memories */
	/* free memory of EEMModelConfig */
	pModelConfig = &g_EsrGlobals.EEMModelConfig;
	pTypeMap = pModelConfig->pTypeMapInfo;
	for (i = 0; i < pModelConfig->iTypeMapNum; i++, pTypeMap++)
	{
		DELETE(pTypeMap->pMapEntriesInfo);
	}
	DELETE(pModelConfig->pTypeMapInfo);

	/* free memory of EEMModelInfo */
	DELETE(g_EsrGlobals.EEMModelInfo.pEEMBlockInfo);

	/* free memory of ESR_BASIC_ARGS */
	pAlarmBuf = pThis->AlarmBuffer.pCurAlarmInfo;
	if (pAlarmBuf != NULL)
	{
		DELETE(pAlarmBuf);
	}

	pAlarmBuf = pThis->AlarmBuffer.pHisAlarmInfo;
	if (pAlarmBuf != NULL)
	{
		DELETE(pAlarmBuf);
	}

	/* 6.free the Mutex -- Delet for Mutex now is allocated in DXI module */ 
	/*hMutex = g_ESRGlobals.hMutexCommonCfg;
	if (hMutex != NULL) 
	{ 
		Mutex_Destroy(hMutex);
		hMutex = NULL;
	}*/

	return;
}


#define		GET_SERVICE_OF_EEM_NAME	"eem_soc.so"

/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SERVICE_ARGUMENTS *  pArgs : 
 * RETURN   : DWORD : service exit code, defined in app_service.h
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-21 19:30
 *==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{
	DWORD dwExitCode;
	int iRet;
	int iRetryTimes = 0 ;
	const char *pLogText;
	COMMON_CONFIG *pOriCfg;
	pOriCfg = &g_EsrGlobals.CommonConfig;
	pOriCfg->bCanChangeCfg = FALSE;

	ESR_BASIC_ARGS l_ESRBasicArgs;
	HANDLE hInputQueue, hOutputQueue;
	HANDLE hServiceThread, hLinklayerThread;

	/* clear to zero */
	memset(&g_EsrGlobals, 0, sizeof(ESR_GLOBALS));
	memset(&l_ESRBasicArgs, 0, sizeof(ESR_BASIC_ARGS));

	/* get the thread handle, used to feed watch dog */
	hServiceThread = RunThread_GetId(NULL);
	RunThread_Heartbeat(hServiceThread);
	APP_SERVICE		*EEM_AppService = NULL;
	EEM_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_EEM_NAME);

	//Wait 240s to get all equipment information.
#define EEM_MAX_WAIT_RETRY_TIMES		120
	for(iRetryTimes = 0; iRetryTimes< EEM_MAX_WAIT_RETRY_TIMES; iRetryTimes++)
	{
		if(THREAD_IS_RUNNING(hServiceThread))
		{
			Sleep(2000);
			RunThread_Heartbeat(hServiceThread);
		}
		else
		{
			return SERVICE_EXIT_NONE;
		}		
	}

	/* 1.initialize g_ESRGlobals */
	dwExitCode = InitESRGlobals();

	if (dwExitCode != SERVICE_EXIT_NONE)
	{
		RoutineBeforeExitService(&l_ESRBasicArgs);
		return dwExitCode;
	}
	 
	pOriCfg->bCanChangeCfg = TRUE;
	/* init the static events */
	InitStaticEvents();

	/* feed watch dog */
	RunThread_Heartbeat(hServiceThread);

	/* 2.init ESR_BASIC_ARGS */
	/* event queues */
	hInputQueue = Queue_Create(ESR_EVENT_QUEUE_SIZE,
		sizeof(ESR_EVENT *), 0);
	hOutputQueue = Queue_Create(ESR_EVENT_QUEUE_SIZE,
		sizeof(ESR_EVENT *), 0);

	if (hInputQueue == NULL || hOutputQueue == NULL)
	{
		AppLogOut(LOG_INIT_ESR_SERVICE, APP_LOG_ERROR, "Create Event Queue "
			"failed.\n");
		TRACE("[%s]--%s: ERROR: Create Event Queue failed.\n", __FILE__,
			__FUNCTION__);

		RoutineBeforeExitService(&l_ESRBasicArgs);
		return SERVICE_EXIT_FAIL;
	}

	l_ESRBasicArgs.hEventInputQueue = hInputQueue;
	l_ESRBasicArgs.hEventOutputQueue = hOutputQueue;

#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread EEM/SOC/RSOC Service was created, Thread Id = %d.\n", gettid());
#endif

	/* AlarmBuffer: iCurAlarmNum and iHisAlarmNum cleared to zero already */
	/* alarmHandler: all members cleared to zero already */

	/* get ESR Service thread id */
	l_ESRBasicArgs.hThreadID[0] = hServiceThread;

	/* flags */
	l_ESRBasicArgs.iOperationMode = ESR_MODE_SERVER;
	l_ESRBasicArgs.pOutQuitCmd = &pArgs->nQuitCommand;
	l_ESRBasicArgs.iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;
	l_ESRBasicArgs.iInnerQuitCmd = SERVICE_EXIT_NONE;
	l_ESRBasicArgs.bFrameDone = TRUE;
	l_ESRBasicArgs.bEquipListChange = FALSE;

	/* other flags cleared to zero already */

	/* register the ESR basic agrs */
    pArgs->pReserved = &l_ESRBasicArgs;

	/* 3.register in the DXI module to receive Alarms */
	iRet = DxiRegisterNotification("ESR Service", 
							ServiceNotification,
							&l_ESRBasicArgs, 							  
							_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 							  
							l_ESRBasicArgs.hThreadID[0],							  
							_REGISTER_FLAG);

	if (iRet != 0)
	{
		pLogText = "Register alarm callback function in DXI module failed.";

		AppLogOut(LOG_INIT_ESR_SERVICE, APP_LOG_ERROR, "%s.\n", pLogText);
		TRACE("[%s]--%s: ERROR: %s Call DxiRegisterNotification, returned"
			" %d.\n", __FILE__, __FUNCTION__, pLogText, iRet);

		RoutineBeforeExitService(&l_ESRBasicArgs);
		return SERVICE_EXIT_FAIL;
	}

	/* 4.create LinkLayer thread */
	
	if(g_EsrGlobals.CommonConfig.iProtocolType >= YDN23)
	{
		l_ESRBasicArgs.hThreadID[1] = NULL;
	}
	else
	{
	hLinklayerThread = RunThread_Create("ESR linklayer thread",
		(RUN_THREAD_START_PROC)ESR_LinkLayerManager,
		&l_ESRBasicArgs,
		NULL,
		0);

	/* assign the reference out of the Thread to insure valid */
	l_ESRBasicArgs.hThreadID[1] = hLinklayerThread;

	if (hLinklayerThread == NULL)
	{
		pLogText = "Create link-layer thread failed.";

		AppLogOut(LOG_INIT_ESR_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
		TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);

		RoutineBeforeExitService(&l_ESRBasicArgs);
		return SERVICE_EXIT_FAIL;
	}
	}
#ifdef _TEST_ESR_MODIFYCONFIG
	{

		int iRet, nVarID;
		BOOL bServiceIsRunning = TRUE;
		COMMON_CONFIG UserCfg;

		//Sleep(5000);
		UserCfg.iProtocolType = 1;
		UserCfg.bCallbackInUse = TRUE;
		UserCfg.byCCID = 66;
		UserCfg.bReportInUse = TRUE;
		UserCfg.iAttemptElapse = 199;
		UserCfg.iSecurityLevel = 3;
		strcpy(UserCfg.szAlarmReportPhoneNumber[0], "1111111");
		strcpy(UserCfg.szCallbackPhoneNumber[0], "2222222");
		UserCfg.iMediaType = 2;
		strcpy(UserCfg.szCommPortParam, "100.200.1.1:12001");

		nVarID = ESR_CFG_W_MODE | ESR_CFG_ATTEMPT_ELAPSE;
		iRet = ESR_ModifyCommonCfg(bServiceIsRunning,
						FALSE,
						&l_ESRBasicArgs,
						nVarID,
						&UserCfg);

		if (iRet != ERR_SERVICE_CFG_OK)
		{
			TRACE("Change Config file failed\n");
			TRACE("Return Value is: %d\n", iRet);
		}

		ESR_PrintCommonCfg(&UserCfg);
		ESR_PrintCommonCfg(&g_EsrGlobals.CommonConfig);
	}
#endif //_TEST_ESR_MODIFYCONFIG

	if(EEM_AppService != NULL)
	{
		EEM_AppService->bReadyforNextService = TRUE;
	}

	/* 5.begin main state machine loop */
	TRACE_ESR_TIPS("Begin state machine");
	dwExitCode = StateMachineLoop(&l_ESRBasicArgs);


	/* 6.do the clean work */
	RoutineBeforeExitService(&l_ESRBasicArgs);
	TRACE_ESR_TIPS("End state machine");
	return dwExitCode;
}

/*==========================================================================*
 * FUNCTION : CheckUserConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID    : 
 *            COMMON_CONFIG  *pUserCfg : 
 *            COMMON_CONFIG  *pOriCfg  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-26 16:39
 *==========================================================================*/
static BOOL CheckUserConfig(int nVarID, COMMON_CONFIG *pUserCfg,
							COMMON_CONFIG *pOriCfg)
{
	/* prepare for check accordance between config items */
	if (!(nVarID & ESR_CFG_PROTOCOL_TYPE))
	{
		pUserCfg->iProtocolType = pOriCfg->iProtocolType;
	}

	if (!(nVarID & ESR_CFG_CCID))
	{
		pUserCfg->byCCID = pOriCfg->byCCID;
	}

	if (!(nVarID & ESR_CFG_SOCID))
	{
		pUserCfg->iSOCID = pOriCfg->iSOCID;
	}

	if (!(nVarID & ESR_CFG_MEDIA_TYPE))
	{
		pUserCfg->iMediaType = pOriCfg->iMediaType;
	}

	if (!(nVarID & ESR_CFG_MEDIA_PORT_PARAM))
	{
		strcpy(pUserCfg->szCommPortParam, pOriCfg->szCommPortParam);
	}

	
	/* 1.check protocol type and the accordance */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_PROTOCOL_TYPE ||
		nVarID & ESR_CFG_CCID || nVarID & ESR_CFG_SOCID)
	{
		if (pUserCfg->iProtocolType < EEM || 
			pUserCfg->iProtocolType >= ESR_PROTOCOL_NUM)
		{
			return FALSE;
		}

		/* check the accordance */
		if (pUserCfg->iProtocolType != SOC_TPE && pUserCfg->byCCID == 0)
		{
			return FALSE;
		}

		if (pUserCfg->iProtocolType == SOC_TPE && pUserCfg->iSOCID == 0)
		{
			return FALSE;
		}
	}

	/* 2.check CCID -- always true due to data type */
	/*if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_CCID)
	{
		if (pUserCfg->byCCID < 0 || pUserCfg->byCCID > 255)
		{
			return FALSE;
		}

	}*/

	/* 3.check SOCID */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_SOCID)
	{
		if (pUserCfg->iSOCID < 0 || pUserCfg->iSOCID > 20479)
		{
			return FALSE;
		}
	}


	/* 4.chech operation media and comm port param */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_MEDIA_TYPE || 
		  nVarID & ESR_CFG_MEDIA_PORT_PARAM)
	{
		if (pUserCfg->iMediaType < MEDIA_TYPE_LEASED_LINE || 
			pUserCfg->iMediaType >= ESR_MEDIA_TYPE_NUM)
		{
			return FALSE;
		}

		switch (pUserCfg->iMediaType)
		{
		case MEDIA_TYPE_LEASED_LINE:
			if (!ESR_CheckBautrateCfg(pUserCfg->szCommPortParam))
			{
				return FALSE;
			}
			break;

		case MEDIA_TYPE_MODEM:
			if (!ESR_CheckModemCfg(pUserCfg->szCommPortParam))
			{
				return FALSE;
			}
			break;

		case MEDIA_TYPE_TCPIP:
			if (!ESR_CheckIsANumber(pUserCfg->szCommPortParam))
			{
				return FALSE;
			}
			break;
		case MEDIA_TYPE_TCPIPV6:
			if (!ESR_CheckIsANumber(pUserCfg->szCommPortParam))
			{
				return FALSE;
			}
			break;
		}
	}

	/* 5.check Max alarm report attempts */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_MAX_ATTEMPTS)
	{
		if (pUserCfg->iMaxAttempts < 0 || pUserCfg->iMaxAttempts > 255)
		{
			return FALSE;
		}
	}

	/* 6.check Call elapse time */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_ATTEMPT_ELAPSE)
	{
		if (pUserCfg->iAttemptElapse < 0 || pUserCfg->iAttemptElapse > 1000 * 600)
		{
			return FALSE;
		}
	}

	/* 7.check report phone number 1 */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_REPORT_NUMBER_1)
	{
		if (!ESR_CheckPhoneNumber(pUserCfg->szAlarmReportPhoneNumber[0]))
		{
			return FALSE;
		}
	}

	/* 8.check report phone number 2 */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_REPORT_NUMBER_2)
	{
		if (!ESR_CheckPhoneNumber(pUserCfg->szAlarmReportPhoneNumber[1]))
		{
			return FALSE;
		}
	}

	/* 9.check callback phone number */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_CALLBACK_NUMBER)
	{
		if (!ESR_CheckPhoneNumber(pUserCfg->szCallbackPhoneNumber[0]))
		{
			return FALSE;
		}
	} 

	/* 10.check report IP 1 */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_IPADDR_1)
	{
		if (!ESR_CheckIPAddress(pUserCfg->szReportIP[0],(pUserCfg->iMediaType == MEDIA_TYPE_TCPIPV6) ? CHECK_IPV6_ADDR_AND_PORT2 : CHECK_IPV4))
		{
			return FALSE;
		}
	}

	/* 11.check report IP 2 */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_IPADDR_2)
	{
		if (!ESR_CheckIPAddress(pUserCfg->szReportIP[1],(pUserCfg->iMediaType == MEDIA_TYPE_TCPIPV6) ? CHECK_IPV6_ADDR_AND_PORT2 : CHECK_IPV4))
		{
			return FALSE;
		}
	}

	/* 12.check security IP 1 */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_SECURITY_IP_1)
	{
		if (!ESR_CheckIPAddress(pUserCfg->szSecurityIP[0],(pUserCfg->iMediaType == MEDIA_TYPE_TCPIPV6) ? CHECK_IPV6_ADDR_AND_PORT2 : CHECK_IPV4))
		{
			return FALSE;
		}
	}

	/* 13.check security IP 2 */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_SECURITY_IP_2)
	{
		if (!ESR_CheckIPAddress(pUserCfg->szSecurityIP[1],(pUserCfg->iMediaType == MEDIA_TYPE_TCPIPV6) ? CHECK_IPV6_ADDR_AND_PORT2 : CHECK_IPV4))
		{
			return FALSE;
		}
	}

	/* 14.check security level */
	if (nVarID & ESR_CFG_ALL || nVarID & ESR_CFG_SECURITY_LEVEL)
	{
		if (pUserCfg->iSecurityLevel < 1 || pUserCfg->iSecurityLevel > 3)
		{
			return FALSE;
		}
	}

	return TRUE;
}


/* for copy COMMON_CONFIG structure, used as inline function */
#define ESR_COPY_COMMON_CONFIG(_src, _dst) \
   { \
   int i;  \
   (_dst).byCCID = (_src).byCCID;  \
   (_dst).iSOCID = (_src).iSOCID;  \
   (_dst).iProtocolType = (_src).iProtocolType;  \
   (_dst).iMediaType = (_src).iMediaType;  \
   strcpy((_dst).szCommPortParam, (_src).szCommPortParam);  \
   (_dst).bReportInUse = (_src).bReportInUse;  \
   (_dst).bCallbackInUse = (_src).bCallbackInUse;  \
   (_dst).iSecurityLevel = (_src).iSecurityLevel;  \
   (_dst).iMaxAttempts = (_src).iMaxAttempts;  \
   (_dst).iAttemptElapse = (_src).iAttemptElapse;  \
   for (i = 0; i < ESR_ALARM_REPORT_NUM; i++)  \
   {  \
	 strcpy((_dst).szAlarmReportPhoneNumber[i], (_src).szAlarmReportPhoneNumber[i]);  \
   }  \
   for (i = 0; i < ESR_CALLBACK_NUM; i++)  \
   {  \
	 strcpy((_dst).szCallbackPhoneNumber[i], (_src).szCallbackPhoneNumber[i]);  \
   }  \
   for (i = 0; i < ESR_ALARM_REPORT_NUM; i++)  \
   {  \
	strcpy((_dst).szReportIP[i], (_src).szReportIP[i]);  \
   }  \
   for (i = 0; i < ESR_SECURITY_IP_NUM; i++)  \
   {  \
	strcpy((_dst).szSecurityIP[i], (_src).szSecurityIP[i]);  \
   } }


/*==========================================================================*
 * FUNCTION : UpdateNormalCfgItems
 * PURPOSE  : assistant function for ServiceConfig
 * CALLS    : 
 * CALLED BY: ServiceConfig
 * ARGUMENTS: int            nVarID    : 
 *            COMMON_CONFIG  *pOriCfg  : 
 *            COMMON_CONFIG  *pUserCfg : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-26 16:27
 *==========================================================================*/
static void ReplaceMarks(char* pszSrc, int iStrLen)
{
	int i = 0;
	for(; i < iStrLen; i++)
	{
		if(*(pszSrc + i) == '(')
		{
			*(pszSrc + i) = '[';
		}
		else if(*(pszSrc + i) == ')')
		{
			*(pszSrc + i) = ']';
		}
	}
}
static void UpdateNormalCfgItems(int nVarID,
								 COMMON_CONFIG *pOriCfg, 
								 COMMON_CONFIG *pUserCfg)
{
	if (nVarID & ESR_CFG_ALL)
	{
		ESR_COPY_COMMON_CONFIG(*pUserCfg, *pOriCfg);
		return;
	}

	if (nVarID & ESR_CFG_CCID)
	{
		pOriCfg->byCCID = pUserCfg->byCCID;
	}

	if (nVarID & ESR_CFG_SOCID)
	{
		pOriCfg->iSOCID = pUserCfg->iSOCID;

		/* calculate CCID from SOCID */
		pOriCfg->byCCIDfromSOC = (BYTE)(pOriCfg->iSOCID & 0XFF);
	}

	if (nVarID & ESR_CFG_REPORT_IN_USE)
	{
		pOriCfg->bReportInUse = pUserCfg->bReportInUse;
	}

	if (nVarID & ESR_CFG_CALLBACK_IN_USE)
	{
		pOriCfg->bCallbackInUse = pUserCfg->bCallbackInUse;
	}

	if (nVarID & ESR_CFG_MAX_ATTEMPTS)
	{
		pOriCfg->iMaxAttempts = pUserCfg->iMaxAttempts;
	}

	if (nVarID & ESR_CFG_ATTEMPT_ELAPSE)
	{
		pOriCfg->iAttemptElapse = pUserCfg->iAttemptElapse;
	}

	if (nVarID & ESR_CFG_SECURITY_LEVEL)
	{
		pOriCfg->iSecurityLevel = pUserCfg->iSecurityLevel;
	}

	if (nVarID & ESR_CFG_REPORT_NUMBER_1)
	{
		strcpy(pOriCfg->szAlarmReportPhoneNumber[0],
			pUserCfg->szAlarmReportPhoneNumber[0]);
	}

	if (nVarID & ESR_CFG_REPORT_NUMBER_2)
	{
		strcpy(pOriCfg->szAlarmReportPhoneNumber[1],
			pUserCfg->szAlarmReportPhoneNumber[1]);
	}

	if (nVarID & ESR_CFG_CALLBACK_NUMBER)
	{
		strcpy(pOriCfg->szCallbackPhoneNumber[0],
			pUserCfg->szCallbackPhoneNumber[0]);
	}

	if (nVarID & ESR_CFG_IPADDR_1)
	{
		ReplaceMarks(pUserCfg->szReportIP[0],strlen(pUserCfg->szReportIP[0]));
		strcpy(pOriCfg->szReportIP[0], pUserCfg->szReportIP[0]);
	}

	if (nVarID & ESR_CFG_IPADDR_2)
	{
		ReplaceMarks(pUserCfg->szReportIP[1],strlen(pUserCfg->szReportIP[1]));
		strcpy(pOriCfg->szReportIP[1], pUserCfg->szReportIP[1]);
	}

	if (nVarID & ESR_CFG_SECURITY_IP_1)
	{
		ReplaceMarks(pUserCfg->szSecurityIP[0],strlen(pUserCfg->szSecurityIP[0]));
		strcpy(pOriCfg->szSecurityIP[0], pUserCfg->szSecurityIP[0]);
	}

	if (nVarID & ESR_CFG_SECURITY_IP_2)
	{
		ReplaceMarks(pUserCfg->szSecurityIP[1],strlen(pUserCfg->szSecurityIP[1]));
		strcpy(pOriCfg->szSecurityIP[1], pUserCfg->szSecurityIP[1]);
	}
	if(nVarID & ESR_CFG_PRODCT_INFOM)
	{
		strcpy(pOriCfg->szProdctInfom, pUserCfg->szProdctInfom);
	}
	return;
}


/*==========================================================================*
 * FUNCTION : WriteESRCfgToFlash
 * PURPOSE  : 
 * CALLS    : ESR_GetModifiedFileBuf
 * CALLED BY: 
 * ARGUMENTS: int            nVarID : 
 *            COMMON_CONFIG  *pCfg  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-26 17:23
 *==========================================================================*/
/* used interface defined in config_builder.c */
BOOL ESR_GetModifiedFileBuf(int nVarID, 
							COMMON_CONFIG *pUserCfg,
							char **pszOutFile);

BOOL ESR_UpdateCommonConfigFile(const char *szContent);

static BOOL WriteESRCfgToFlash(int nVarID, COMMON_CONFIG *pUserCfg)
{
	HANDLE hMutex;
	char *szOutFile;
	char *pLogText;

	/* to simplify log action */
#define TASK_NAME        "Modify ESR Config File"
#define LOG_MODIFY_FILE(_pLogText)  \
	(AppLogOut(TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	hMutex = DXI_GetESRCommonCfgMutex();

	if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_COMCFG) == ERR_MUTEX_OK)
	{
		/* create new file in memory first */
		if (!ESR_GetModifiedFileBuf(nVarID, pUserCfg, &szOutFile))
		{
			pLogText = "Failed to create new ESR common config file in memory.";
			LOG_MODIFY_FILE(pLogText);

			DELETE(szOutFile);
			Mutex_Unlock(hMutex);
			return FALSE;
		}

		/* write to flash */
		if (!ESR_UpdateCommonConfigFile(szOutFile))
		{
			pLogText = "Write the new ESR common config file to flash failed.";
			LOG_MODIFY_FILE(pLogText);

			DELETE(szOutFile);
			Mutex_Unlock(hMutex);
			return FALSE;
		}
	}
	else
	{
		pLogText = "Wait for Mutex of Common Config file timeout.";
		LOG_MODIFY_FILE(pLogText);

		return FALSE;
	}


	DELETE(szOutFile);
	Mutex_Unlock(hMutex);
	return TRUE;
}

__INLINE static BOOL ESR_IsCommBusy(ESR_BASIC_ARGS *pThis)
{
	BOOL bRet = pThis->bCommRunning;

	/* for leased line, when stay in EEM_IDLE or SOC_OFF as Server Mode, we 
	 * think communication is not running. 
	 */
	if (g_EsrGlobals.CommonConfig.iMediaType == MEDIA_TYPE_LEASED_LINE)
	{
		if (!pThis->iMachineState && pThis->iOperationMode == ESR_MODE_SERVER)
		{
			bRet = FALSE;
		}
	}

	return bRet;
}


/*==========================================================================*
 * FUNCTION : ESR_ModifyCommonCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ServiceConfig
 * ARGUMENTS: BOOL bServiceIsRunning    :
 *			  BOOL bByForce				: ignore CommBusy restriction
 *			  ESR_BASIC_ARGS  *pThis    : 
 *            int             iItemID   : 
 *            COMMON_CONFIG   *pUserCfg : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-10 15:53
 *==========================================================================*/
int ESR_ModifyCommonCfg(BOOL bServiceIsRunning,
						BOOL bByForce,
						ESR_BASIC_ARGS *pThis,
						int iItemID,
						COMMON_CONFIG *pUserCfg)
{
	COMMON_CONFIG *pOriCfg;
	HANDLE hLinklayerThread;
	

	char *pLogText;

	/* original common config */
	pOriCfg = &g_EsrGlobals.CommonConfig;



	/* read config */
	if (!(iItemID & ESR_CFG_W_MODE))  
	{
		ESR_COPY_COMMON_CONFIG(*pOriCfg, *pUserCfg);
		return ERR_SERVICE_CFG_OK;
	}
	if(!pOriCfg->bCanChangeCfg)
	{
		return ERR_SERVICE_CFG_BUSY;
	}

	/* update config */
	if (!bByForce && ESR_IsCommBusy(pThis))
	{
		return ERR_SERVICE_CFG_BUSY;
	}

	/* check data first */
	if (!CheckUserConfig(iItemID, pUserCfg, pOriCfg))
	{
		return ERR_SERVICE_CFG_DATA;
	}

	/* check if the non-shared com has been occupied already */
#ifdef COM_SHARE_SERVICE_SWITCH
	{
		HANDLE hMutex;
		SERVICE_SWITCH_FLAG *pFlag;

		/* note: MediaType value of pUserCfg has been updated 
		in CheckUserConfig function */
		if (iItemID & ESR_CFG_MEDIA_TYPE || iItemID & ESR_CFG_ALL ||
			iItemID & ESR_CFG_MEDIA_PORT_PARAM)
		{
			pFlag = DXI_GetServiceSwitchFlag();

			if (ESR_IS_SHARE_COM(pUserCfg->iMediaType))
			{
				hMutex = DXI_GetServiceSwitchMutex();

				if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_SWITCH) == ERR_MUTEX_OK)
				{
					if (pFlag->bMtnCOMHasClient)
					{
						Mutex_Unlock(hMutex);
						return ERR_SERVUCE_CFG_ESR_COMUSED;
					}
					else
					{
						pFlag->bHLMSUseCOM = TRUE;
						Mutex_Unlock(hMutex);
					}
				}
				else
				{
					return ERR_SERVUCE_CFG_ESR_COMUSED;
				}
			}
			else
			{
				pFlag->bHLMSUseCOM = FALSE;
			}
		}

	}
#endif //COM_SHARE_SERVICE_SWITCH


	/* write to flash then */
	if (!WriteESRCfgToFlash(iItemID, pUserCfg))
	{
		return ERR_SERVICE_CFG_FLASH;
	}

	/* now process in the memory */
	if (bServiceIsRunning)
	{
		/* protocol type updated */
		if ((iItemID & ESR_CFG_PROTOCOL_TYPE) ||
			(iItemID & ESR_CFG_ALL))
		{
			/* changed */
			if (pOriCfg->iProtocolType != pUserCfg->iProtocolType)
			{
				pOriCfg->iProtocolType = pUserCfg->iProtocolType;
				pThis->bProtocolChangedFlag = TRUE;
			}
		}

		/* link-layer media updated */
		//if ((iItemID & ESR_CFG_MEDIA_TYPE) || (iItemID & ESR_CFG_ALL) ||
		//	(iItemID & ESR_CFG_MEDIA_PORT_PARAM))
			
		{
			/* changed */
			//if (pOriCfg->iMediaType != pUserCfg->iMediaType || 
			//	strcmp(pOriCfg->szCommPortParam, 
			//	pUserCfg->szCommPortParam) != 0)
			//{		
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;

				//Sleep(3000);
				/* wait 20s */

				RunThread_Stop(pThis->hThreadID[1], 20000, TRUE);

				/* update the value */
				if (iItemID & ESR_CFG_MEDIA_TYPE)
				{
					pOriCfg->iMediaType = pUserCfg->iMediaType;
				}
				if (iItemID & ESR_CFG_MEDIA_PORT_PARAM)
				{
					strcpy(pOriCfg->szCommPortParam, pUserCfg->szCommPortParam);
				}

				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;

				/* restart link-layer thread */
				
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_NONE;
				//if(pOriCfg->iProtocolType == YDN23)
				//{
				//	TRACE("Go to stop linker thread\n");
				//	if(pThis->hThreadID[1])
				//	{
				//		RunThread_Stop(pThis->hThreadID[1], 
				//						1000, TRUE);
				//		
				//	}
				//	pThis->hThreadID[1] = NULL;
				//}
				//else
				//{
				
					UpdateNormalCfgItems(iItemID, pOriCfg, pUserCfg);
					printf("Go to create linker thread[%d] [%0x]\n", iItemID, iItemID);
					hLinklayerThread = RunThread_Create("ESR linklayer thread",
						(RUN_THREAD_START_PROC)ESR_LinkLayerManager,
						pThis,
						NULL,
						0);
	
					if (hLinklayerThread == NULL)
					{
						pLogText = "Create link-layer thread failed.";
	
						AppLogOut(LOG_INIT_ESR_SERVICE, APP_LOG_ERROR, "%s\n", pLogText);
						TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, pLogText);
	
						pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
						return ERR_SERVICE_CFG_EXIT;
					}

					pThis->hThreadID[1] = hLinklayerThread;
				//}
			//}/* end of changed */
		}/* end of link-layer media updated */

		/* for other config item, just modify values */
		
	}

	return ERR_SERVICE_CFG_OK;

}

int ESR_ModifyCommonCfg_MS(BOOL bServiceIsRunning,
			   BOOL bByForce,
			   ESR_BASIC_ARGS *pThis,
			   int iItemID,
			   COMMON_CONFIG *pUserCfg)
{
	COMMON_CONFIG *pOriCfg;
	HANDLE hLinklayerThread;


	char *pLogText;

	/* original common config */
	pOriCfg = &g_EsrGlobals.CommonConfig;

	/* read config */
	if (!(iItemID & ESR_CFG_W_MODE))  
	{
		ESR_COPY_COMMON_CONFIG(*pOriCfg, *pUserCfg);
		return ERR_SERVICE_CFG_OK;
	}

	if(!pOriCfg->bCanChangeCfg)
	{
		return ERR_SERVICE_CFG_BUSY;
	}

	/* update config */
	if (!bByForce && ESR_IsCommBusy(pThis))
	{
		return ERR_SERVICE_CFG_BUSY;
	}

	/* check data first */
	if (!CheckUserConfig(iItemID, pUserCfg, pOriCfg))
	{
		return ERR_SERVICE_CFG_DATA;
	}

	/* check if the non-shared com has been occupied already */
#ifdef COM_SHARE_SERVICE_SWITCH
	{
		HANDLE hMutex;
		SERVICE_SWITCH_FLAG *pFlag;

		/* note: MediaType value of pUserCfg has been updated 
		in CheckUserConfig function */
		if (iItemID & ESR_CFG_MEDIA_TYPE || iItemID & ESR_CFG_ALL ||
			iItemID & ESR_CFG_MEDIA_PORT_PARAM)
		{
			pFlag = DXI_GetServiceSwitchFlag();

			if (ESR_IS_SHARE_COM(pUserCfg->iMediaType))
			{
				hMutex = DXI_GetServiceSwitchMutex();

				if (Mutex_Lock(hMutex, TIME_WAIT_MUTEX_SWITCH) == ERR_MUTEX_OK)
				{
					if (pFlag->bMtnCOMHasClient)
					{
						Mutex_Unlock(hMutex);
						return ERR_SERVUCE_CFG_ESR_COMUSED;
					}
					else
					{
						pFlag->bHLMSUseCOM = TRUE;
						Mutex_Unlock(hMutex);
					}
				}
				else
				{
					return ERR_SERVUCE_CFG_ESR_COMUSED;
				}
			}
			else
			{
				pFlag->bHLMSUseCOM = FALSE;
			}
		}

	}
#endif //COM_SHARE_SERVICE_SWITCH


	/* write to flash then */
	if (!WriteESRCfgToFlash(iItemID, pUserCfg))
	{
		return ERR_SERVICE_CFG_FLASH;
	}

	/* now process in the memory */
	if (bServiceIsRunning)
	{
		/* protocol type updated */
		if ((iItemID & ESR_CFG_PROTOCOL_TYPE) ||
			(iItemID & ESR_CFG_ALL))
		{
			/* changed */
			if (pOriCfg->iProtocolType != pUserCfg->iProtocolType)
			{
				pOriCfg->iProtocolType = pUserCfg->iProtocolType;
				pThis->bProtocolChangedFlag = TRUE;
			}
		}

		/* for other config item, just modify values */
		UpdateNormalCfgItems(iItemID, pOriCfg, pUserCfg);
	}

	return ERR_SERVICE_CFG_OK;

}
/*==========================================================================*
 * FUNCTION : ServiceConfig
 * PURPOSE  : for ESR common config info access
 * CALLS    : ESR_ModifyCommonCfg
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService : 
 *            SERVICE_ARGUMENTS * pArgs:
 *            int     nVarID   : 
 *            int    *nBufLen  : 
 *            void   *pDataBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-22 16:37
 *==========================================================================*/
int ServiceConfig(HANDLE hService, 
				  BOOL bServiceIsRunning,
				  SERVICE_ARGUMENTS *pArgs, 
				  int nVarID, 
				  int *nBufLen, 
				  void *pDataBuf)
{
	UNUSED(hService);
	UNUSED(nBufLen);

	return ESR_ModifyCommonCfg(bServiceIsRunning,
		FALSE,
		(ESR_BASIC_ARGS *)pArgs->pReserved, 
		nVarID, (COMMON_CONFIG *)pDataBuf);	
}

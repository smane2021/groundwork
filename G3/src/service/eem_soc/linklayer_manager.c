/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : linklayer_manager.c
*  CREATOR  : LinTao                   DATE: 2004-10-09 16:34
*  VERSION  : V1.00
*  PURPOSE  : 
*
*
*  HISTORY  :
*
*==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "sys/times.h"
#include "esr.h"
#include <arpa/inet.h>			/* inet_ntoa */


/* timeout uses in communication, unit: ms */
#define WAITFORCONN_TIMEOUT    2000  /* wait for connected timeout */
#define READCOM_TIMEOUT		   1000  
#define WRITECOM_TIMEOUT	   2000
#define CONN_TIMEOUT           5000 /* connect to remote timeout */

/* log Macros for Link-layer Manager sub-module(LM = linklayer manager) */
#define ESR_TASK_LM		"ESR Linklayer Manager"

/* error log */
#define LOG_ESR_LM_E(_szLogText)  LOG_ESR_E(ESR_TASK_LM, _szLogText)

/* extended error log(logged with error code) */
#define LOG_ESR_LM_E_EX(_szLogText, _errCode)  \
	LOG_ESR_E_EX(ESR_TASK_LM, _szLogText, _errCode)

/* warning log */
#define LOG_ESR_LM_W(_szLogText)  LOG_ESR_W(ESR_TASK_LM, _szLogText)

/* info log */
#define LOG_ESR_LM_I(_szLogText)  LOG_ESR_I(ESR_TASK_LM, _szLogText)


#define ESR_STDPORT_TCP		1
#define ESR_STDPORT_MODEM	4
#define ESR_TCP_IPV6		7
#define	ESR_STDPORT_LEASEDLINE		3
#define TIMEOUT_FOR_SAFE_OPEN		10//3*60	//set 3 min for timeout Jimmy 2011/08/03

/* wait for comm port has been closed */
__INLINE static void SafeOpenComm(ESR_BASIC_ARGS *pThis)
{
#ifdef COM_SHARE_SERVICE_SWITCH
	int iMediaType;
	//HANDLE hMutex;  /* not need to use Mutex */
	SERVICE_SWITCH_FLAG *pFlag;

	HANDLE hLocalThread;

	iMediaType = g_EsrGlobals.CommonConfig.iMediaType;
	pFlag = DXI_GetServiceSwitchFlag();
	hLocalThread = pThis->hThreadID[1];

	//To avoid Flag not set , here Jimmy Set a time out
	time_t tStart_Of_Enter = time(NULL);
	time_t tEnd_of_Enter = time(NULL);

	/* wait until Serial Port has closed by Maintenace Service */
	if (ESR_IS_SHARE_COM(iMediaType))
	{
		while (1)
		{
			if (pFlag->bCOMHaveClosed)
			{
				pFlag->bCOMHaveClosed = FALSE;
				pFlag->bHLMSUseCOM = TRUE;

				break;
			}
			else
			{	
#ifdef _DEBUG_ESR_LINKLAYER
				TRACE_ESR_TIPS("Serial Port has not been Closed");
#endif //_DEBUG_ESR_LINKLAYER
				// This section of codes is added by Jimmy 2011/08/03
				time_t tEnd_of_Enter = time(NULL);
				if((tEnd_of_Enter - tStart_Of_Enter) > TIMEOUT_FOR_SAFE_OPEN)
				{
					//printf("\n----Time out,so manual set flat for SAFEOPEN!!!\n");
					pFlag->bCOMHaveClosed = TRUE;
				}
				//end adding
				//printf("bCOMHaveClosed = %d; TimeOut is %d\n", pFlag->bCOMHaveClosed,tEnd_of_Enter - tStart_Of_Enter);
				Sleep(3000);

				/* feed watch dog */
				RunThread_Heartbeat(hLocalThread);
			}
		}
	}

#endif //COM_SHARE_SERVICE_SWITCH

	return;
}

__INLINE static void LeaveCommUsing(void)
{
#ifdef COM_SHARE_SERVICE_SWITCH
	int iMediaType;
	//HANDLE hMutex;  /* not need to use Mutex */
	SERVICE_SWITCH_FLAG *pFlag;

	iMediaType = g_EsrGlobals.CommonConfig.iMediaType;
	pFlag = DXI_GetServiceSwitchFlag();

	//if (ESR_IS_SHARE_COM(iMediaType))
	{
		pFlag->bCOMHaveClosed = TRUE;
	}

#endif //COM_SHARE_SERVICE_SWITCH

	return;
}

/*==========================================================================*
* FUNCTION : ComQueue
* PURPOSE  : communicate with Event Queues
* CALLS    : Queue_Get
*			  Queue_Put
*			  CommWrite
* CALLED BY: ComMC
* ARGUMENTS: ESR_BASIC_ARGS  *pThis       : 
*            BOOL            bSendTimeOut : 
* RETURN   : int : ESR_COMM_STATUS 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2004-10-11 14:20
*==========================================================================*/
static int ComQueue(ESR_BASIC_ARGS *pThis, BOOL bSendTimeOut) 
{
	ESR_EVENT *pEvent;
	int iRet, iCommWriteRet;
	int iTimerSet;
	struct timeval tv;
	
	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[1]);

	if (bSendTimeOut) /* send timeout event to EventInputQueue */
	{
		
		pEvent = &g_EsrTimeoutEvent;

		/* send it to the EventInputQueue */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);

		if (iRet != ERR_OK)
		{
			LOG_ESR_LM_E_EX("send event to Event InputQueue failed", iRet);

			/* exit ESR service */
			//Edited by Jimmy 2012/09/08,���ﲻ�˳�������RS232ģʽ�£��Զ����õ�ʱ�������˳�
			//pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
			ESR_ClearEventQueue(pThis->hEventInputQueue, FALSE);

			
			return COMM_STATUS_DISCONNECT;
		}

	}

	iRet = Queue_Get(pThis->hEventOutputQueue, &pEvent, FALSE, WAIT_FOR_O_QUEUE);
	//gettimeofday(&tv,NULL);
	//printf("In hEventOutputQueue time %u:%u\n",tv.tv_sec,tv.tv_usec);
#ifdef _TEST_ESR_QUEUE_SYNCHRONIZATION
	{
		int iCount;
		ESR_EVENT *pLocalEvent;
		iCount = Queue_GetCount(pThis->hEventOutputQueue, NULL);
		if (iCount > 0)
		{
			TRACEX("Output Queue: Count is %d\n", iCount);
			TRACE("Time is: %.3f\n", GetCurrentTime());
			Queue_Get(pThis->hEventOutputQueue, &pLocalEvent, TRUE, 1000);
			ESR_PrintEvent(pLocalEvent);
		}

		iCount = Queue_GetCount(pThis->hEventInputQueue, NULL);
		if (iCount > 0)
		{
			TRACEX("Input Queue: Count is %d\n", iCount);
			TRACE("Time is: %.3f\n", GetCurrentTime());
			Queue_Get(pThis->hEventInputQueue, &pLocalEvent, TRUE, 1000);
			ESR_PrintEvent(pLocalEvent);
		}
	}
#endif //_TEST_ESR_QUEUE_SYNCHRONIZATION

	switch (iRet)
	{
	case ERR_OK:
		switch (pEvent->iEventType)
		{
		case ESR_FRAME_EVENT:
			if (pEvent->bDummyFlag)
			{
				// TRACE_ESR_TIPS("received Dummy frame event from OutputQueue");
				/*DELETE(pEvent);*/

				/* sleep 100 ms in lest two frame are sent together */
				/* normally, byte-read-delay is 50 ms */
				Sleep(100);
				return COMM_STATUS_NEXT;
			}
			else
			{

#ifdef _DEBUG_ESR_LINKLAYER
				{
					int i, iLen;
					unsigned char chr;
					TRACE_ESR_TIPS("received frame event from OutputQueue, "
						"write the Data to Comm Port");
					iLen = pEvent->iDataLength;
					TRACE("\tData Length: %d\n", iLen);

					TRACE("\tData Content(Hex format):");
					for (i = 0; i < iLen; i++)
					{
						printf("%02X ", pEvent->sData[i]);
					}
					TRACE("\n");

					TRACE("\tData Content(Text format):");
					for (i = 0; i < iLen; i++)
					{
						chr = (unsigned char)pEvent->sData[i];
						if (chr < 0x21 || chr > 0X7E)
						{
							printf("%s", ESR_GetUnprintableChr(chr));
						}
						else
						{
							printf("%c", chr);
						}
					}
					TRACE("\n");

				}					
#endif //_DEBUG_ESR_LINKLAYER
				//gettimeofday(&tv,NULL);
				//printf("In CommWrite time %u:%u pEvent->sData = \n",tv.tv_sec,tv.tv_usec);
				CommWrite(pThis->hComm, 
					pEvent->sData, 
					pEvent->iDataLength);
				DELETE(pEvent);
				//gettimeofday(&tv,NULL);
				//printf("OUT CommWrite time %u:%u\n",tv.tv_sec,tv.tv_usec);

				/* get the error code */
				iCommWriteRet = CommGetLastError(pThis->hComm);

				iTimerSet = Timer_Reset(RunThread_GetId("WEB_UI"),1);
				if(iTimerSet == ERR_OK)
				{
					TRACE("\n Web_ResetNetDriver:Reset Timer OK!!!\n");
				}

				if (iCommWriteRet != ERR_OK)
				{
					LOG_ESR_LM_E_EX("Write to Comm Port failed", 
						iCommWriteRet);

					/* When timeout, not exit. */
					if (iCommWriteRet != ERR_TIMEOUT)
					{
						/* exit ESR service */
						pThis->bServerModeNeedExit = TRUE;
						pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
					}

					return COMM_STATUS_DISCONNECT;
				}

				//gettimeofday(&tv,NULL);
				//printf("More CommWrite time %u:%u\n",tv.tv_sec,tv.tv_usec);
				if (pEvent->bSkipFlag)
				{
					return COMM_STATUS_SKIP;
				}
				else
				{
					return COMM_STATUS_NEXT;
				}

			}

		case ESR_DISCONNECTED_EVENT:
			TRACE_ESR_TIPS("Disconnected event received from "
				"EventOutputQueue");
			/*DELETE(pEvent);*/
			return COMM_STATUS_DISCONNECT;

		default:
			TRACE_ESR_TIPS("received none-expected event from EventOutputQueue");

			DELETE_ESR_EVENT(pEvent);
			return COMM_STATUS_NEXT;
		}

	case ERR_QUEUE_EMPTY: //means timeout
		TRACE_ESR_TIPS("get event from EventOutputQueue timeout");

		return COMM_STATUS_NEXT;


	default:         /* is ERR_INVALID_ARGS */ 
		LOG_ESR_LM_E_EX("Get event from Event OutputQueue failed", iRet);


		/* exit ESR service */
		pThis->bServerModeNeedExit = TRUE;
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

		return COMM_STATUS_DISCONNECT;
	}
}


/*==========================================================================*
* FUNCTION : PickupAFrame
* PURPOSE  : Pickup a frame from input data buffer
* CALLS    : 
* CALLED BY: CommReadFrame
* ARGUMENTS: ESR_BASIC_ARGS  *pThis        : 
*            int             iProtocolType : 
*            char            *pDstBuf      : 
*            int             *piLen        : 
* RETURN   : BOOL : TRUE means get a frame from the received buf
*					 FALSE means received buf is empty
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2005-06-12 09:55
*==========================================================================*/
static BOOL PickupAFrame(ESR_BASIC_ARGS *pThis, int iProtocolType, 
			 char *pDstBuf, int *piLen)
{
	int iRet = FALSE;
	int i, iCount, iFrameLen;
	int iSrcPos, iSrcLen, iTotalLen;
	char *pSrcBuf;

	iSrcPos = pThis->iCurPos;
	iSrcLen = pThis->iLen;
	iTotalLen = pThis->iCalcLen;
	pSrcBuf = pThis->sReceivedBuff;


	if (iSrcPos < iSrcLen)
	{
		iRet = TRUE;
		switch (iProtocolType)
		{
		case SOC_TPE:		//process as RSOC
			//iFrameLen = MAX_SOCFRAME_LEN;

		case RSOC:
			//iFrameLen = MAX_RSOCFRAME_LEN;
			iFrameLen = (iProtocolType == SOC_TPE) ? MAX_SOCFRAME_LEN : MAX_RSOCFRAME_LEN;
			switch (pSrcBuf[iSrcPos])
			{
			case ENQ:
			case ACK:
			case NAK:
			case EOT:
				memcpy(pDstBuf, pSrcBuf + iSrcPos, 1);
				*piLen = 1;
				iSrcPos++;
				break;

			case DLE:
				if (pSrcBuf[++iSrcPos] == 0x30)
				{
					memcpy(pDstBuf, pSrcBuf + iSrcPos - 1, 2);
					*piLen = 2;
				}
				break;

			case STX:
				i = iSrcPos, iCount = 1;
				while (pSrcBuf[i] != ETX && iCount < iFrameLen && i < iSrcLen)
				{
					i++, iCount++;
				}
				if (pSrcBuf[i] == ETX)
				{
					memcpy(pDstBuf, pSrcBuf + iSrcPos, (size_t)iCount + 1);
					*piLen = iCount + 1;
					iSrcPos = iCount + 1;
					break;
				}

				//if not found, go to default section

			default:
				memcpy(pDstBuf + iTotalLen, pSrcBuf, (size_t)iSrcLen);

				*piLen += iSrcLen;
				iSrcPos = iSrcLen;
			}
			break;

		case EEM:
			memcpy(pDstBuf + iTotalLen, pSrcBuf , (size_t)iSrcLen);
			*piLen += iSrcLen;
			iSrcPos = iSrcLen;
			//printf("pDstBuf = %s\n", pDstBuf);
		}
		/* to delete */
		if (iSrcPos < iSrcLen)
		{
			TRACEX("Split a frame!\n");
		}
		/* end */
	}

	pThis->iCalcLen += iSrcLen;
	pThis->iCurPos = iSrcPos;
	return iRet;
}

static BOOL PickupAFrameSOC(ESR_BASIC_ARGS *pThis, int iProtocolType, 
			    char *pDstBuf, int *piLen)
{
	int iRet = FALSE;
	int i, iCount, iFrameLen;
	int iSrcPos, iSrcLen;
	char *pSrcBuf;

	iSrcPos = pThis->iCurPos;
	iSrcLen = pThis->iLen;
	pSrcBuf = pThis->sReceivedBuff;
	*piLen = 0;

	if (iSrcPos < iSrcLen)
	{
		iRet = TRUE;
		switch (iProtocolType)
		{
		case SOC_TPE:		//process as RSOC
			//iFrameLen = MAX_SOCFRAME_LEN;

		case RSOC:
			//iFrameLen = MAX_RSOCFRAME_LEN;
			iFrameLen = (iProtocolType == SOC_TPE) ? MAX_SOCFRAME_LEN : MAX_RSOCFRAME_LEN;

			switch (pSrcBuf[iSrcPos])
			{
			case ENQ:
			case ACK:
			case NAK:
			case EOT:
				memcpy(pDstBuf, pSrcBuf + iSrcPos, 1);
				*piLen = 1;
				iSrcPos++;
				break;

			case DLE:
				if (pSrcBuf[++iSrcPos] == 0x30)
				{
					memcpy(pDstBuf, pSrcBuf + iSrcPos - 1, 2);
					*piLen = 2;
				}
				break;

			case STX:
				i = iSrcPos, iCount = 1;
				while (pSrcBuf[i] != ETX && iCount < iFrameLen && i < iSrcLen)
				{
					i++, iCount++;
				}
				if (pSrcBuf[i] == ETX)
				{
					memcpy(pDstBuf, pSrcBuf + iSrcPos, (size_t)iCount + 1);
					*piLen = iCount + 1;
					iSrcPos += iCount + 1;
					break;
				}

				//if not found, go to default section

			default:
				memcpy(pDstBuf, pSrcBuf + iSrcPos, (size_t)iSrcLen);
				*piLen = iSrcLen;
				iSrcPos = iSrcLen;
			}
			break;

		case EEM:
			memcpy(pDstBuf, pSrcBuf + iSrcPos, (size_t)iSrcLen);
			*piLen = iSrcLen;
			iSrcPos = iSrcLen;
		}
		/* to delete */
		if (iSrcPos < iSrcLen)
		{
			TRACEX("Split a frame!\n");
		}
		/* end */
	}

	pThis->iCurPos = iSrcPos;
	return iRet;
}

/*==========================================================================*
* FUNCTION : CommReadFrame
* PURPOSE  : 
* CALLS    : PickupAFrame
* CALLED BY: 
* ARGUMENTS: ESR_BASIC_ARGS  *pThis   : 
*            char            *pBuffer : 
*            int             *piLen   : 
*            int             iTimeOut : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2005-06-12 09:56
*==========================================================================*/
static int CommReadFrame(ESR_BASIC_ARGS *pThis, char *pBuffer, 
			 int *piLen, int iTimeOut)
{
	HANDLE hComm;
	int    iProtocolType, iErrCode,i;
	COMM_TIMEOUTS timeOut;

	struct timeval tv;


	unsigned char readData_success_flg = 0;//hulifeng add  , for eem slow


	hComm = pThis->hComm;
	iErrCode = ERR_COMM_OK;
	iProtocolType = g_EsrGlobals.CommonConfig.iProtocolType;

	*piLen = 0;
	pThis->iCalcLen = 0;	

	if(iProtocolType != EEM)
	{
		//printf("Into here time is %d\n", time(NULL));
		//printf("\n*******COMM READ BEGIN, CurPos = %d, iLen = %d*******\n",pThis->iCurPos,pThis->iLen);
		if (!PickupAFrameSOC(pThis, iProtocolType, pBuffer, piLen))  /* received buff is empty */
		{
			/* to delete */
			//TRACEX("Pick none of the received buff. piLen is: %d\n", *piLen);


			/* set timeout */
			INIT_TIMEOUTS(timeOut, iTimeOut, WRITECOM_TIMEOUT);
			CommControl(hComm, 
				COMM_SET_TIMEOUTS,
				(char *)&timeOut,
				sizeof(COMM_TIMEOUTS)); 

			pThis->iCurPos = 0;
			pThis->iLen = CommRead(hComm, pThis->sReceivedBuff, ESR_MAX_FRAME_LEN);
			//printf("\n*******COMM READ EXCUTED, AND LEN = %d*******\n",pThis->iLen);
			/* get last err code */
			CommControl(hComm, 
				COMM_GET_LAST_ERROR, 
				(char *)&iErrCode, 
				sizeof(int));

			PickupAFrameSOC(pThis, iProtocolType, pBuffer, piLen);

			/* to delete */
			//TRACEX("Try pick again from the received buff. piLen is: %d\n", *piLen);
		}

	}
	else
	{
		for(i = 0;i < 10;i++)  /* received buff is empty */
		{
			

			/* to delete */
			//printf("\n EEM__Pick none of the received buff. piLen is: %d\n", *piLen);

			/* set timeout */
			//memset(pThis->sReceivedBuff, 0x0, sizeof(pThis->sReceivedBuff));
		/*	INIT_TIMEOUTS(timeOut, iTimeOut, WRITECOM_TIMEOUT);
			CommControl(hComm, 
				COMM_SET_TIMEOUTS,
				(char *)&timeOut,
				sizeof(COMM_TIMEOUTS)); 
		*/
		//hulifeng modify
			if( 0 == readData_success_flg )
			{
				INIT_TIMEOUTS(timeOut, iTimeOut, WRITECOM_TIMEOUT);
				CommControl(hComm, 
				COMM_SET_TIMEOUTS,
				(char *)&timeOut,
				sizeof(COMM_TIMEOUTS)); 
			}
			else
			{
				INIT_TIMEOUTS(timeOut, 100, WRITECOM_TIMEOUT);
				CommControl(hComm, 
				COMM_SET_TIMEOUTS,
				(char *)&timeOut,
				sizeof(COMM_TIMEOUTS)); 
			}


			pThis->iCurPos = 0;
			pThis->iLen = CommRead(hComm, pThis->sReceivedBuff, ESR_MAX_FRAME_LEN);


			/* get last err code */
			CommControl(hComm, 
				COMM_GET_LAST_ERROR, 
				(char *)&iErrCode, 
				sizeof(int));
			if((pThis->iCalcLen+pThis->iLen)>ESR_MAX_FRAME_LEN)
			{
				break;
			}
			if(PickupAFrame(pThis, iProtocolType, pBuffer, piLen)==FALSE)
			{
				readData_success_flg = 0;//hulifeng add
				break;
			}
			else//hulifeng add
			{
				readData_success_flg = 1;  
			}

			/* to delete */
			//printf("Try pick again from the received buff. piLen is: %d:%s\n", *piLen, pBuffer);
		}

	}
	return iErrCode;
}


/*==========================================================================*
* FUNCTION : ComMC
* PURPOSE  : communicate with MC
* CALLS    : CommRead 
*			  Queue_Put  
*			  ComQueue
* CALLED BY: DataExchange
* ARGUMENTS: ESR_BASIC_ARGS  *pThis    : 
*            int             iTimeOut  : 
*            BOOL            bSkipFlag : 
* RETURN   : int : ESR_COMM_STATUS
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2004-10-11 13:05
*==========================================================================*/
static int ComMC(ESR_BASIC_ARGS *pThis, int iTimeOut, BOOL bSkipFlag)
{
	HANDLE hComm;
	char sBuffer[ESR_MAX_FRAME_LEN];
	int  iLen, iErrCode, iRet;
	COMM_TIMEOUTS timeOut;
	ESR_EVENT *pEvent;
	struct timeval tv;

	hComm = pThis->hComm;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[1]);

	/* skip the step to keep synchronization */
	if (bSkipFlag)
	{
		return ComQueue(pThis, FALSE);
	}

	
	iErrCode = CommReadFrame(pThis, sBuffer, &iLen, iTimeOut);
	
	/* check the error code */
	switch (iErrCode) 
	{
	case ERR_COMM_OK:
		//printf("The received string is : %s\n", sBuffer);  // zzc debug
		break;

	case ERR_COMM_TIMEOUT:
		if (iLen > 0)  //  is not timeout
		{
			//printf("The received string is : %s\n", sBuffer);  // zzc debug
			break;
		}

		/* send timeout event to EventQueue */
#ifdef _DEBUG_ESR_LINKLAYER
		/*TRACE_ESR_TIPS("read from MC timeout, send Timeout Event to "
		"the InputQueue");*/
#endif //_DEBUG_ESR_LINKLAYER

		//printf("\n!!!!!!!!!Read Frame TimeOut!!!!!!!!!\n"); // zzc debug
		return ComQueue(pThis, TRUE);

	case ERR_COMM_CONNECTION_BROKEN:

#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("connection closed by MC");
#endif //_DEBUG_ESR_LINKLAYER

		return COMM_STATUS_DISCONNECT;

	default:
		LOG_ESR_LM_E_EX("Read Comm Port failed", iErrCode);

		/* exit ESR service */
		//printf("The received string is : %s\n", sBuffer);  // zzc debug
		pThis->bServerModeNeedExit = TRUE;
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

		return COMM_STATUS_DISCONNECT;
	}

#ifdef _DEBUG_ESR_LINKLAYER
	{
		int i;
		unsigned char chr;

		TRACE_ESR_TIPS("Comm Port received data");
		TRACE("\tData Length: %d\n", iLen);

		TRACE("\tData Content(Hex format):");
		for (i = 0; i < iLen; i++)
		{
			printf("%02X ", sBuffer[i]);
		}
		TRACE("\n");

		TRACE("\tData Content(Text format):");
		for (i = 0; i < iLen; i++)
		{
			chr = (unsigned char)sBuffer[i];
			if (chr < 0x21 || chr > 0X7E)
			{
				printf("%s", ESR_GetUnprintableChr(chr));
			}
			else
			{
				printf("%c", chr);
			}
		}
		printf("\n");

	}
#endif //_DEBUG_ESR_LINKLAYER

	pEvent = NEW(ESR_EVENT, 1);
	if (pEvent == NULL)
	{
		LOG_ESR_LM_E("no memory to create Frame Event");

		/* exit ESR service */
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_MEMORY;
		return COMM_STATUS_DISCONNECT;
	}
	INIT_FRAME_EVENT(pEvent, iLen, sBuffer, 0, 0);


#ifdef _DEBUG_ESR_LINKLAYER
	TRACE("The received data will sent to InputQueue as Frame Event.\n");
	ESR_PrintEvent(pEvent);
#endif //_DEBUG_ESR_LINKLAYER


	/* send it to the EventInputQueue */
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_ESR_LM_E_EX("Send Frame event to InputQueue failed", iErrCode);
		DELETE(pEvent);

		if(pThis->bEquipListChange == FALSE)
		{
			pThis->bServerModeNeedExit = TRUE;
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
		}
		return COMM_STATUS_DISCONNECT;
	}

	//	printf("Run here2!\n"); // zzc debug
	gettimeofday(&tv,NULL);
	//printf("OUT ComMC time %u:%u\n",tv.tv_sec,tv.tv_usec);
	return ComQueue(pThis, FALSE);


}


/*==========================================================================*
* FUNCTION : DataExchange
* PURPOSE  : data exchange between MC and Event Queues
* CALLS    : ComMC
* CALLED BY: RunAsServer
*			  RunAsClient
* ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2004-10-11 11:50
*==========================================================================*/
static void DataExchange(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	BOOL bSkipFlag;

	bSkipFlag = FALSE;

	while (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		iRet = ComMC(pThis, READCOM_TIMEOUT/2, bSkipFlag);

		switch (iRet)
		{
		case COMM_STATUS_DISCONNECT:
			return;

		case COMM_STATUS_SKIP:
#ifdef _DEBUG_ESR_LINKLAYER
			TRACE_ESR_TIPS("skip ComMC");
			TRACE("returned value: %d\n", iRet);
#endif //_DEBUG_ESR_LINKLAYER
			bSkipFlag = TRUE;
			break;

		case COMM_STATUS_NEXT:
			bSkipFlag = FALSE;
			break;
		}
	}

}


/*==========================================================================*
* FUNCTION : RunAsServer
* PURPOSE  : 
* CALLS    : 
* CALLED BY: ESR_LinkLayerManager
* ARGUMENTS: ESR_BASIC_ARGS *  pThis      : 
*            PORT_INFO         *pCommPort : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2004-10-10 21:27
*==========================================================================*/
static void RunAsServer(ESR_BASIC_ARGS *pThis, PORT_INFO *pCommPort)
{
	HANDLE hOpenComm, hDataComm;
	int iErrCode, iTryCount;

	/* to get ESR common config info */
	COMMON_CONFIG *pCommonCfg;
	int i;
	int iMediaType;  
	char (*szSecurityIP)[64];  

	/* to send disconnected event */
	int iRet;
	ESR_EVENT *pEvent;

	/* to get client IP */
	COMM_PEER_INFO cpi;   
	DWORD dwPeerIP = 0;
	char *szClientIP;

	HANDLE hLocalThread;
	COMM_TIMEOUTS timeOut;

	char *szLibName;
	char szOpenParam[64];

	hLocalThread = pThis->hThreadID[1];
	pCommonCfg = &g_EsrGlobals.CommonConfig;
	iMediaType = pCommonCfg->iMediaType;
	szSecurityIP = pCommonCfg->szSecurityIP;

	/* reset flag */
	pThis->bServerModeNeedExit = FALSE;
	
	if (iMediaType == MEDIA_TYPE_TCPIP)
	{
		pCommPort->iStdPortTypeID = ESR_STDPORT_TCP;
		strcpy(szOpenParam, "127.0.0.1:");
	}
	else if (iMediaType == MEDIA_TYPE_LEASED_LINE)
	{
		pCommPort->iStdPortTypeID = ESR_STDPORT_LEASEDLINE;
	}
	else if(iMediaType == MEDIA_TYPE_TCPIPV6)
	{
		pCommPort->iStdPortTypeID = ESR_TCP_IPV6;
		strcpy(szOpenParam, "[::1]:");
	}
	else
	{
		pCommPort->iStdPortTypeID = ESR_STDPORT_MODEM;
	}

	/* get lib name and port param */
	szLibName = GetStandardPortDriverName(pCommPort->iStdPortTypeID);

	/* get comm port param */
	if (iMediaType == MEDIA_TYPE_TCPIP 
		|| iMediaType == MEDIA_TYPE_TCPIPV6)
	{
		strncat(szOpenParam, pCommonCfg->szCommPortParam,
			sizeof(szOpenParam));
	}
	else
	{
		strncpyz(szOpenParam, pCommonCfg->szCommPortParam,
			sizeof(szOpenParam));
	}

#ifdef _DEBUG_ESR_LINKLAYER
	TRACE_ESR_TIPS("CC Comm Port Info:");
	TRACE("\tComm Port ID: %d\n", pCommPort->iPortID);
	TRACE("\tStdPort ID: %d\n", pCommPort->iStdPortTypeID);
	TRACE("\tStdPort accessing libname: %s\n", szLibName);
	TRACE("\tOpen Param: %s\n", szOpenParam);
#endif //_DEBUG_ESR_LINKLAYER

	SafeOpenComm(pThis);

	/* open the comm, retry 3 times */
	for (iTryCount = 0; iTryCount < 3; iTryCount++)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);

		hOpenComm = CommOpen(szLibName,
			pCommPort->szDeviceDesp,
			szOpenParam,
			COMM_SERVER_MODE,
			pCommPort->iTimeout,
			&iErrCode);

		//printf("iErrCode = %d time is %d\n", iErrCode, time(NULL));
		switch (iErrCode)
		{
		case ERR_COMM_STD_PORT:
			LOG_ESR_LM_E("Open Comm Port failed for StdPort ID is invalid");

			/* exit the service */
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
			return;

		case ERR_COMM_OPENING_PARAM:	
			LOG_ESR_LM_E("Open Comm Port failed for Opening Param is invalid");

			/* exit programme */
			pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_CFG;
			return;

		case ERR_OK:
			iTryCount = 3;  /* break while loop */
			break;

		default:  /* do nothing, just retry */
			Sleep(1000); 
			break;
		}
	}


	if (iErrCode != ERR_OK)
	{
		LOG_ESR_LM_E_EX("Open failed after 3 times retry", iErrCode);
		//printf("Exit\n");

		/* exit programme */
		pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;
		return;
	}

#ifdef _DEBUG_ESR_LINKLAYER
	TRACE_ESR_TIPS("Open comm port OK, now begin to wait for connected");
#endif //_DEBUG_ESR_LINKLAYER


	// printf("Into Main loop\n");
	/* main loop */
	INIT_TIMEOUTS(timeOut, WAITFORCONN_TIMEOUT, WRITECOM_TIMEOUT);
	while (!pThis->bServerModeNeedExit &&
		pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		//printf("Into loop---0\n");
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);

		/* set accept timeout */
		CommControl(hOpenComm, 
			COMM_SET_TIMEOUTS,
			(char *)&timeOut,
			sizeof(COMM_TIMEOUTS)); 

		//	printf("Into loop--%d-%d %d time is %d\n", timeOut.nReadTimeout,timeOut.nWriteTimeout,timeOut.nIntervalTimeout, time(NULL));
		hDataComm = CommAccept(hOpenComm);
		//printf("Into loop---1 time is %d\n", time(NULL));

		if (hDataComm == NULL)
		{
			//printf("Into loop---2\n");
			/* get last err code */
			CommControl(hOpenComm, 
				COMM_GET_LAST_ERROR,
				(char *)&iErrCode, 
				sizeof(int));

			if (iErrCode == ERR_COMM_TIMEOUT)
			{
				//printf("Timeout\n");
				continue;
			}
			else  /* for other err, exit programme */
			{
				LOG_ESR_LM_E_EX("Accept failed", iErrCode);

				CommClose(hOpenComm);
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_FAIL;

				return;
			}
		} /* end of if (hDataComm == NULL) */

		/* successfully got hDataComm */
		/* check mode, if has something to report, return */ 
		/* Note: mode can be changed when bCommRunning is FALSE */
		if (pThis->iOperationMode != ESR_MODE_SERVER)
		{
			CommClose(hDataComm);
			CommClose(hOpenComm);
			return;
		}
		else
		{
			pThis->hComm = hDataComm;
		}

		TRACE_ESR_TIPS("Connected");

		/* set Running flag first to inhibit report */
		pThis->bCommRunning = TRUE;

		/* set security flag */
		switch (iMediaType)
		{
		case MEDIA_TYPE_LEASED_LINE:  //alaways secure
			pThis->bSecureLine = TRUE;
			break;

		case MEDIA_TYPE_MODEM:  //need callback
			pThis->bSecureLine = FALSE; 
			break;

		case MEDIA_TYPE_TCPIP:  //check secure IP list
			pThis->bSecureLine = FALSE;

			iErrCode = CommControl(hDataComm, 
				COMM_GET_PEER_INFO, (char *)&cpi,
				sizeof(COMM_PEER_INFO));
			if (iErrCode == ERR_COMM_OK)
			{
				// szAddr is in network format.
				dwPeerIP = *(DWORD *)cpi.szAddr;
				szClientIP = inet_ntoa(*(struct in_addr *)&dwPeerIP);

#ifdef _DEBUG_ESR_LINKLAYER
				{
					char szLogText[ESR_LOG_TEXT_LEN];
					sprintf(szLogText, "Client %s is accepted", szClientIP);
					TRACE_ESR_TIPS(szLogText);
				}
#endif //_DEBUG_ESR_LINKLAYER


				for (i = 0; i < ESR_SECURITY_IP_NUM; i++)
				{
					if (strcmp(szClientIP, szSecurityIP[i]) == 0)
					{
						pThis->bSecureLine = TRUE;
						break;
					}
				}
			}

			break;
		case MEDIA_TYPE_TCPIPV6:  //check secure IP list
			pThis->bSecureLine = FALSE;

			iErrCode = CommControl(hDataComm, 
				COMM_GET_PEER_INFO, (char *)&cpi,
				sizeof(COMM_PEER_INFO));
			if (iErrCode == ERR_COMM_OK)
			{
				// szAddr is in network format.
				IN6_ADDR ip6Addr = *(IN6_ADDR *)(DWORD *)cpi.szAddr;
				char szClinetIP1[64];
				inet_ntop(AF_INET6,&ip6Addr,szClinetIP1,64);

#ifdef _DEBUG_ESR_LINKLAYER
				{
					char szLogText[ESR_LOG_TEXT_LEN];
					sprintf(szLogText, "Client %s is accepted", szClientIP);
					TRACE_ESR_TIPS(szLogText);
				}
#endif //_DEBUG_ESR_LINKLAYER


				for (i = 0; i < ESR_SECURITY_IP_NUM; i++)
				{
					if (strcmp(szClinetIP1, szSecurityIP[i]) == 0)
					{
						pThis->bSecureLine = TRUE;
						break;
					}
				}
			}

			break;
		default:  //do nothing
			break;
		}

		/* clear InputQueue */
		ESR_ClearEventQueue(pThis->hEventInputQueue, FALSE);
		

		/* communication begin */
		DataExchange(pThis);
		pThis->bCommRunning = FALSE;

		/* communication end */

		/* clear OutputQueue */
		ESR_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

		/* create event for sending disconnected message */
		//pEvent = NEW(ESR_EVENT, 1);
		//if (pEvent == NULL)
		//{
		//	LOG_ESR_LM_E("Memory is not enough");

		//	/* exit ESR service */
		//	pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_MEMORY;
		//	return;
		//}
		//INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);
		pEvent = &g_EsrDiscEvent;

		/* send it */
		iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
		if (iRet != ERR_OK)
		{
			LOG_ESR_LM_E("Put event to the Input Queue failed");
			/*DELETE(pEvent);*/
		}

		/* close Accept Comm Handle */
		CommClose(hDataComm);
	}

	/* close Open Comm Handle */
	CommClose(hOpenComm);

	return;
}


/*==========================================================================*
* FUNCTION : GetCommOpenParam
* PURPOSE  : assistant function to get open params for CommOpen interface 
*			  when in Client Mode
* CALLS    : 
* CALLED BY: RunAsClient
* ARGUMENTS: ESR_BASIC_ARGS  *pThis       : 
*            PORT_INFO       *pCommPort   : 
*            char            *szOpenParam : 
*            int             iBufLen      : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2004-10-11 18:41
*==========================================================================*/
static void GetCommOpenParam(ESR_BASIC_ARGS *pThis,
			     PORT_INFO *pCommPort,
			     char *szOpenParam,
			     int iBufLen)
{
	COMMON_CONFIG *pCommonCfg;
	int iMediaType;
	BYTE byCurReportType;

	/* init local variable */
	pCommonCfg = &g_EsrGlobals.CommonConfig;
	iMediaType = pCommonCfg->iMediaType;
	byCurReportType = pThis->alarmHandler.byCurReportRouteType;

	switch (iMediaType)
	{
	case MEDIA_TYPE_MODEM:
		/* get baud rate (szAccessingDriverSet: 9600,n,8,1:86011382)*/
		Cfg_SplitString(pCommPort->szAccessingDriverSet,
			szOpenParam, iBufLen, ':');
		/* now szOpenParam ended with null */
		strncat(szOpenParam, 
			":",
			iBufLen - strlen(szOpenParam) - 1); /* ensure end with null */

		switch (byCurReportType)
		{
		case ESR_ROUTE_CALLBACK:
			strncat(szOpenParam, 
				pCommonCfg->szCallbackPhoneNumber[0],
				iBufLen - strlen(szOpenParam) - 1);
			break;

		case ESR_ROUTE_ALARM_1:
			strncat(szOpenParam,
				pCommonCfg->szAlarmReportPhoneNumber[0],
				iBufLen - strlen(szOpenParam) - 1);
			break;

		case ESR_ROUTE_ALARM_2:
			strncat(szOpenParam,
				pCommonCfg->szAlarmReportPhoneNumber[1],
				iBufLen - strlen(szOpenParam) - 1);
			break;

		default:
			/* error */
			szOpenParam[0] = '\0';
		}
		break;

	case MEDIA_TYPE_TCPIP:
	case MEDIA_TYPE_TCPIPV6:
		/* TCPIP param format: 100.100.100.1:23 */
		switch (byCurReportType)
		{
		case ESR_ROUTE_ALARM_1:
			strncpyz(szOpenParam, pCommonCfg->szReportIP[0], iBufLen);
			break;

		case ESR_ROUTE_ALARM_2:
			strncpyz(szOpenParam, pCommonCfg->szReportIP[1], iBufLen);
			break;

		default:
			/* error */
			szOpenParam[0] = '\0';
		}
		break;

	case MEDIA_TYPE_LEASED_LINE:
		/* Leased line open format:   9600,n,8,1 */
		strncpyz(szOpenParam, pCommonCfg->szCommPortParam, iBufLen);
		break;

	default:  /* error */
		szOpenParam[0] = '\0';
	}

	return;
}


/*==========================================================================*
* FUNCTION : RunAsClient
* PURPOSE  : 
* CALLS    : GetCommOpenParam
* CALLED BY: ESR_LinkLayerManager
* ARGUMENTS: ESR_BASIC_ARGS  *pThis     : 
*            PORT_INFO       *pCommPort : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2004-10-11 19:55
*==========================================================================*/
static void RunAsClient(ESR_BASIC_ARGS *pThis, PORT_INFO *pCommPort)
{
	char szOpenParam[64];  /* note: the buffer size should be enough */
	int  iErrCode, iRet;
	HANDLE hLocalThread, hDataComm;
	char *szLibName;
	char *pSearchValue = NULL;

	char szLogText[ESR_LOG_TEXT_LEN];

	ESR_EVENT *pEvent;


	/* get Linklayer thread id */
	hLocalThread = pThis->hThreadID[1];

	/* prepare port open param */
	GetCommOpenParam(pThis, pCommPort, szOpenParam, 64);
	//printf("szOpenParam = [%s]\n", szOpenParam);
	pSearchValue = strrchr(szOpenParam,':');
	//printf("Into here pSearchValue = %s\n", *(pSearchValue + 1));
	if(pSearchValue != NULL && (Cfg_RemoveWhiteSpace(pSearchValue + 1) == NULL || *(pSearchValue + 1) == '\0'))
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);

		SafeOpenComm(pThis);

		/*INIT_NONEFRAME_EVENT(pEvent, ESR_CONNECT_FAILED_EVENT);*/
		pEvent = &g_EsrConnFailEvent;
	}
	else
	{

		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);

		SafeOpenComm(pThis);

		/* connect to remote */
		szLibName = GetStandardPortDriverName(pCommPort->iStdPortTypeID);
		hDataComm = CommOpen(szLibName,
			pCommPort->szDeviceDesp,
			szOpenParam,
			COMM_CLIENT_MODE,
			CONN_TIMEOUT,
			&iErrCode);

#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("MC Comm Port Info:");
		TRACE("\tComm Port ID: %d\n", pCommPort->iPortID);
		TRACE("\tStdPort ID: %d\n", pCommPort->iStdPortTypeID);
		TRACE("\tStdPort accessing libname: %s\n", szLibName);
		TRACE("\tOpen Param: %s\n", szOpenParam);
#endif //_DEBUG_ESR_LINKLAYER

		/* connect to remote takes time, feed watch dog again */
		RunThread_Heartbeat(hLocalThread);

		/* create event for sending connected or connect failed message */
		//pEvent = NEW(ESR_EVENT, 1);
		//if (pEvent == NULL)
		//{
		//	LOG_ESR_LM_E("Memory is not enough");

		//	/* exit ESR service */
		//	pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_MEMORY;
		//	return;
		//}

		if (iErrCode != ERR_OK)  // init connect failed event
		{
			snprintf(szLogText, ESR_LOG_TEXT_LEN, "Connect to remote failed"
				"(Error Code: %X)", iErrCode);
			TRACE_ESR_TIPS(szLogText);
			/*INIT_NONEFRAME_EVENT(pEvent, ESR_CONNECT_FAILED_EVENT);*/
			pEvent = &g_EsrConnFailEvent;
		}

		else  //init connected event
		{
			/* clear InputQueue */
			ESR_ClearEventQueue(pThis->hEventInputQueue, FALSE);

			/*INIT_NONEFRAME_EVENT(pEvent, ESR_CONNECTED_EVENT);*/
			pEvent = &g_EsrConnEvent;
		}
	}



	/* send it to the input queue */
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_ESR_LM_E("Put event to the Input Queue failed");
		/*DELETE(pEvent);*/
	}

	/* if not connected, return */
	if (iErrCode != ERR_OK)
	{
		/* keep sychrnization */
		Sleep(50);
		return;
	}


	pThis->hComm = hDataComm;

	/* communication begin */
	pThis->bCommRunning = TRUE;

	/* now is security connect */
	pThis->bSecureLine = TRUE;

	/* send "CallBack and Report Alarms" frame from output queue */
	ComQueue(pThis, FALSE);
		

	DataExchange(pThis);

	/* communication end */
	pThis->bSecureLine = FALSE;
	pThis->bCommRunning = FALSE;

	/* clear OutputQueue */
	ESR_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

	/* create event for sending disconnected message */
	//pEvent = NEW(ESR_EVENT, 1);
	//if (pEvent == NULL)
	//{
	//	LOG_ESR_LM_E("Memory is not enough");

	//	/* exit ESR service */
	//	pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_MEMORY;
	//	return;
	//}
	//INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);

	pEvent = &g_EsrDiscEvent;
	/* send it */
	iRet = Queue_Put(pThis->hEventInputQueue, &pEvent, FALSE);
	if (iRet != ERR_OK)
	{
		LOG_ESR_LM_E("Put event to the Input Queue failed");
		/*DELETE(pEvent);*/
	}

	//printf("Run here 4\n");// zzc debug

	CommClose(hDataComm);

	return;

}


/*==========================================================================*
* FUNCTION : ESR_LinkLayerManager
* PURPOSE  : Linklayer thread entry function
* CALLS    : RunAsServer
*			  RunAsClient
* CALLED BY: ServiceMain
* ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
* RETURN   : int : THREAD_EXIT_CODE
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2004-10-11 16:02
*==========================================================================*/
int ESR_LinkLayerManager(ESR_BASIC_ARGS *pThis)
{
	int iCommPortID;
	PORT_INFO *pCommPort = NULL;
	int iMediaType;

	/* to get comm port info through DixGetData interface */
	int iError, iBufLen, iTryCount;

	HANDLE hLocalThread;


	/* get Local thread id */	
	hLocalThread = RunThread_GetId(NULL);
	/* assign the reference in the Thread to insure valid */
	pThis->hThreadID[1] = hLocalThread;

	/* feed watch dog */
	RunThread_Heartbeat(hLocalThread);

	/* get current Comm Port ID */
	if (g_EsrGlobals.CommonConfig.iMediaType == MEDIA_TYPE_TCPIP
		|| g_EsrGlobals.CommonConfig.iMediaType == MEDIA_TYPE_TCPIPV6)
	{
		/* for net data port, port id is 3 */
		iCommPortID = 2;
	}
	else /* Modem or Leased Line */
	{
		iCommPortID = 3;
	}
	//printf("iCommPortID = %d\n", iCommPortID);
	/* get Comm Port config Info by port id */
	for (iTryCount = 0; iTryCount < 3; iTryCount++)
	{
		iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_PORT_INFO,			
			iCommPortID,		
			&iBufLen,			
			&pCommPort,
			0);

		if (iError != ERR_DXI_OK) 
		{
			if (iError == ERR_DXI_TIME_OUT)
			{
				/* try 3 times */
				Sleep(1000);
				continue;
			}
			else
			{
				LOG_ESR_LM_E_EX("Get Comm Port config info failed", iError);

				/* notify ESR service to exit */
				pThis->iInnerQuitCmd = SERVICE_EXIT_FAIL;
				return SERVICE_EXIT_FAIL;
			}
		}
		else
		{
			break;
		}
	}
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread EEM Linker Service was created, Thread Id = %d.\n", gettid());
#endif
	/* begin main loop */
	iMediaType = g_EsrGlobals.CommonConfig.iMediaType;
	//printf("g_EsrGlobals.CommonConfig.iMediaType = %d\n", g_EsrGlobals.CommonConfig.iMediaType);
	while (pThis->iLinkLayerThreadExitCmd == SERVICE_EXIT_NONE)
	{
		/* feed watch dog */
		RunThread_Heartbeat(hLocalThread);
		//printf("------------------------\n");

		/* run in the Server mode */
		if (pThis->iOperationMode == ESR_MODE_SERVER) 
		{   
			TRACE_ESR_TIPS("Enter the Server Mode\n");
			printf("Enter the Server Mode"); // zzc debug
			//printf("2Enter the Server Mode"); // zzc debug
			RunAsServer(pThis, pCommPort);
			LeaveCommUsing();
			TRACE_ESR_TIPS("Exit the server mode");
			//printf("Exit the server mode\n");// zzc debug
		}
		else			/* run in the Client mode */
		{
			printf("Enter the Client Mode\n"); // zzc debug
			TRACE_ESR_TIPS("Enter the Client Mode");
			RunAsClient(pThis, pCommPort);
			LeaveCommUsing();
			TRACE_ESR_TIPS("Exit the Client Mode");
		}
	}


	TRACE_ESR_TIPS("Exit the Link-layer sub-thread");
	printf("Exit code: %d\n", pThis->iLinkLayerThreadExitCmd);

	/* notify ESR service to exit(only for abnormal case) */
	if (pThis->iLinkLayerThreadExitCmd != SERVICE_EXIT_OK)
	{
		pThis->iInnerQuitCmd = pThis->iLinkLayerThreadExitCmd;
	}

	return pThis->iLinkLayerThreadExitCmd;

}


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FIiLenAME : Frame_analysis.c
 *  CREATOR  : LinTao                   DATE: 2004-10-12 16:27
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "esr.h"


/*==========================================================================*
 * FUNCTION : IsAddressOk
 * PURPOSE  : assistant funtion, check CCID
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char  *pStr : 2 ascii char
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 19:38
 *==========================================================================*/
static BOOL IsAddressOk(IN const unsigned char *pStr)
{
	if (pStr == NULL || pStr + 1 == NULL)
	{
		return FALSE;
	}

	if (ESR_AsciiHexToChar(pStr) == ESR_GetCCID())
	{
		return TRUE;
	}

	else
	{
		return FALSE;
	}
}


/*==========================================================================*
 * FUNCTION : IsCharPrintable
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CaclBCC (BCC is between 0x20-0x7f)
 * ARGUMENTS: IN const unsigned char  c : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 20:20
 *==========================================================================*/
static BOOL IsCharPrintable(IN const unsigned char c)
{
	if(c >= 0x20 && c <= 0x7f)
	{
      return TRUE;
	}

    return FALSE;
}

/*==========================================================================*
 * FUNCTION : CalcBCC
 * PURPOSE  : assistant function, calculate BCC(used by EEM frame)
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const char  *pStr : 
 *            IN int         iLen  : string length
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 20:34
 *==========================================================================*/
static unsigned char CalcBCC(IN const char *pStr, IN int iLen)
{
	unsigned char sum = 0;

	while (iLen--)
	{
		sum += *pStr++;
	}
	/* clear most significant bit */
	sum &= 0x7f;

	/* make printabe */
	if (sum < 0x20) 
	{	
		sum += 0x20;
	}

	return sum;
}


/*==========================================================================*
 * FUNCTION : CalcLRC
 * PURPOSE  : assistant function, calculate LRC(used by SOC frame)
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char *  pStr : 
 *            IN int           iLen : 
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 20:43
 *==========================================================================*/
static unsigned char CalcLRC(IN const unsigned char * pStr, IN int iLen)
{
	unsigned char result;
	int i;

	result = 0;

	for (i = 0 ; i < iLen; i++) 
	{
		result ^= pStr[i];
	}

	if (result < 0x21)
	{
		result ^= 0x7F;
	}

	return result;  
}


/*==========================================================================*
 * FUNCTION : AnalyseSelectFrame
 * PURPOSE  : assistant function to analyse SELECT frame 
 * CALLS    : 
 * CALLED BY: ESR_AnalyseFrame
 * ARGUMENTS: IN const unsigned unsigned char  *pFrame : 
 *            IN int            iLen    : 
 * RETURN   : FRAME_TYPE : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-13 13:34
 *==========================================================================*/
static FRAME_TYPE AnalyseSelectFrame(IN const unsigned char *pFrame, IN int iLen)
{
	/* ensure safe access */
	if (iLen < 21)
	{
		return SELECT_PROT;
	}

	/* <EOT> */
	if (pFrame[0] != EOT)
	{
		return SELECT_PROT;
	}  

	/* AD1-AD6 */
	if (!ESR_IsStrAsciiHex(pFrame+1, 6))
	{
		return SELECT_PROT;
	}

	/* Select Type (Fast, Test, Group, Broadcast) */
	if (!( pFrame[7] == 'F' || pFrame[7] == 'T' || 
		pFrame[7] == 'G' || pFrame[7] == 'B'))
	{
		return SELECT_PROT;
	}

	/* <SOH> */
	if (pFrame[8] != SOH) 
	{
		return SELECT_PROT;
	}

	/* AD1-AD6 */
	if (!ESR_IsStrAsciiHex(pFrame+9, 6)) 
	{
		return SELECT_PROT;
	}

	/* <STX> */
	if (pFrame[15] != STX)
	{
		return SELECT_PROT;
	}

	/* <ETX> */
	if (pFrame[iLen-2] != ETX)
	{
		return SELECT_PROT;
	}

	/* BCC */
	if (!IsCharPrintable(pFrame[iLen-1])) 
	{
		return SELECT_PROT;
	}

	/* Check that both address fields are identical */
	if (strncmp((const char*)pFrame+1, (const char*)pFrame+9, 6) != 0) 
	{
		return SELECT_PROT;
	}

	/* Check Address towards current CCID */
	if (!IsAddressOk(pFrame+1))
	{
		return SELECT_ADDR;
	}

	/* If Test Select, ignore checksum calculation */
	if (pFrame[7] == 'T')
	{
		return SELECT_OK;
	}
	
	/* checksum calculation */
	if (pFrame[iLen-1] != CalcBCC(pFrame + 9, (iLen - 1) - 9))
	{
		return SELECT_BCC;
	}

	return SELECT_OK;
}


/*==========================================================================*
 * FUNCTION : AsciiToSocID_base
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const char  *pStr : 
 *            int            iBase : 16 or 32
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-13 14:52
 *==========================================================================*/
 static int AsciiToSocID_base(IN const char *pStr, int iBase)
{
	int  i, j;
	char c;
	int  bitValue, socID;

	socID = 0;

	for (i = 0; i < 3; i++)
	{
		c = *(pStr + i);
		if (c <= '9' && c >= '0')
		{
			c -= '0';
		}
		else if (c <= 'V' && c >= 'A')
		{
			c = c - 'A' + 10;
		}
		else //invalid format
		{
			socID = 0;
			break;
		}

		bitValue = (unsigned char)c;
		for (j = 2; j > i; j--)
		{
			bitValue = bitValue * iBase;
		}

		socID += bitValue;
	}

	/* for is not true 32-base */
	if (iBase == 32)
	{
		socID -= 12288;
	}

	return (socID > 0 ? socID : 0);
}

/*==========================================================================*
 * FUNCTION : AsciiToSocID
 * PURPOSE  : assistant function to calculate socID 32-base asciiStr
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const char *pStr : from "001" to "VVV"
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-13 15:00
 *==========================================================================*/
static int AsciiToSocID(IN const char *pStr)
{
	int i, iBase;
	
	iBase = 16;
	for (i = 0; i < 3; i++)
	{
		if (pStr[i] >= 'G')
		{
			iBase = 32;
			break;
		}
	}

	return AsciiToSocID_base(pStr, iBase);
}
	

/*==========================================================================*
 * FUNCTION : SocIDToAscii
 * PURPOSE  : Get ascii string(3 characters + zero) from socid
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int    iSocID  : 
 *            OUT char  *szSocID : 
 * RETURN   : void: 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-14 15:48
 *==========================================================================*/
static void SocIDToAscii(IN int iSocID, OUT char *szSocID)
{
	int i, id, bitBase;
	unsigned char bitValue[3];

	ASSERT(szSocID != NULL);

	/* 16-base value (FFF = 4095) */
    if (iSocID <= 4095) 
	{
		sprintf(szSocID, "%03X", iSocID);
		return;
	}

	/* change to 32-base value */
	if (iSocID > 20479)
	{
		/* beyound bound! */
		return;
	}

	/* change to real 32-base value */
	id = iSocID + 12288;
	bitBase = 32 * 32;
	for (i = 0; i < 3; i++)
	{
		bitValue[i] = id / bitBase;
		id = id % bitBase;

		bitBase /= 32;
	}


	/* get the ascii char */
	for (i = 0; i < 3; i++)
	{
		if (bitValue[i] <= 9) /*unsigned char bitValue[i] >= 0*/
		{
			bitValue[i] += '0';
		}
		else if (bitValue[i] >= 10 && bitValue[i] <= 31)
		{
			bitValue[i] += 'A' - 10;
		}

		szSocID[i] = bitValue[i];
	}
	
	szSocID[3] = '\0';
	return;
}




/*==========================================================================*
 * FUNCTION : SOC_AnalyseSTXFrame
 * PURPOSE  : assistant function to analyse STX frame for SOC/TPE protocol
 * CALLS    : 
 * CALLED BY: ESR_AnalyseFrame
 * ARGUMENTS: IN const unsigned unsigned char  *pFrame : 
 *            IN int            iLen    : 
 * RETURN   : FRAME_TYPE : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-13 13:43
 *==========================================================================*/
static FRAME_TYPE SOC_AnalyseSTXFrame(IN const unsigned char *pFrame, 
									  IN int iLen,
									  ESR_BASIC_ARGS *pThis)
{
	int iSocID, AppDataSize;
	unsigned char ccid;

	iSocID = g_EsrGlobals.CommonConfig.iSOCID;
	ccid = ESR_GetCCID();

	/* check frame length */
	if (iLen < 19)
	{
		return STX_SOC_PROT;
	}

	if (!ESR_IsStrAsciiHex(pFrame + 10, 2))
	{
		return STX_SOC_PROT;
	}

	AppDataSize = ESR_AsciiHexToChar(pFrame + 10);
	if (iLen != AppDataSize + 19)
	{
		return STX_SOC_PROT;
	}

	/* <STX> */
	if (pFrame[0] != STX)
	{
		return STX_SOC_PROT;
	}

	/* <+> or <*> */
	if (pFrame[4] != '+' && pFrame[4] != '*')
	{
		return STX_SOC_PROT;
	}

	 /*
	 * modified by Thomas, 2005-10-4
	 * According to the TR(K/G0007052) rasied from EMEA, ACU should ignore the 
	 * soc_id check for the special <*> frame 
	 */
	/* [soc_id] */
	if (pFrame[4] == '+' && AsciiToSocID(pFrame + 5) != iSocID)
	{
		return STX_SOC_ADDR;
	}

    /*
	 * modified by Thomas, 2005-10-4
	 * According to the TR(K/G0007051) rasied from EMEA, ACU should ignore the 
	 * field 
	 */
	/* <0><0> */
	//if (pFrame[8] != '0' || pFrame[9] != '0')
	//{
	//	return STX_SOC_PROT;
	//}

	/* <ETX> */
	if (pFrame[17 + AppDataSize] != ETX)
	{
		return STX_SOC_PROT;
	}

	/* <LRC> */
	if (pFrame[18 + AppDataSize] != CalcLRC(pFrame, iLen -1))
	{
		return STX_SOC_LRC;
	}

	/* note: (17 + AppDataSize) is the position of <ETX>  */
	/* special command */
	if (pFrame[17 + AppDataSize - 1] == LF)
	{
		return STX_SOC_SPECIAL;
	}

	/* splitted SOC frame(sepcial command has been detected already) */
	if (pFrame[17 + AppDataSize - 1] != '*')
	{
		if (pThis->bFrameDone)
		{
			/* check ccid for the first split frame */
			if (!ESR_IsStrAsciiHex(pFrame + 17, 2))
			{
				return STX_SOC_ADDR;
			}

			if (ccid != ESR_AsciiHexToChar(pFrame + 17))
			{
				return STX_SOC_ADDR;
			}

			/* reset flag */
			pThis->bFrameDone = FALSE;

			return STX_SOC_SPLIT_F;
		}
		else
		{
			return STX_SOC_SPLIT;
		}
	}

	/* now can check ccid of [App Data] (for completed normal command only)*/
	if (pThis->bFrameDone)
	{
		if (!ESR_IsStrAsciiHex(pFrame + 17, 2))
		{
			return STX_SOC_ADDR;
		}

		if (ccid != ESR_AsciiHexToChar(pFrame + 17))
		{
			return STX_SOC_ADDR;
		}
		
		return STX_SOC_OK;
	}

	/* the splitted last frame */
	pThis->bFrameDone = TRUE;
	return STX_SOC_SPLIT_L;

}

/*==========================================================================*
 * FUNCTION : RSOC_AnalyseSTXFrame
 * PURPOSE  : assistant function to analyse STX frame for RSOC protocol
 * CALLS    : 
 * CALLED BY: ESR_AnalyseFrame
 * ARGUMENTS: IN const unsigned unsigned char  *pFrame : 
 *            IN int            iLen    : 
 * RETURN   : FRAME_TYPE : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-13 13:54
 *==========================================================================*/
static FRAME_TYPE RSOC_AnalyseSTXFrame(IN const unsigned char *pFrame, 
									   IN int iLen,
									   ESR_BASIC_ARGS *pThis)
{
	unsigned char ccid;

	ccid = ESR_GetCCID();

	/* check the length */
	if (iLen < 6)
	{
		return STX_RSOC_PROT;
	}
	
	/* <STX> */
	if (pFrame[0] != STX)
	{
		return STX_RSOC_PROT;
	}

	/* <ETX> */
	if (pFrame[iLen - 2] != ETX)
	{
		return STX_RSOC_PROT;
	}

	/* <LRC> */
	if (pFrame[iLen - 1] != CalcLRC(pFrame, iLen -1))
	{
		return STX_RSOC_LRC;
	}

	/* splitted frame */
	if (pFrame[iLen - 3] != '*')
	{
		/* the split frames done, it is the new frame */
		if (pThis->bFrameDone)
		{
			/* check ccid for the first split frame */
			if (!ESR_IsStrAsciiHex(pFrame + 1, 2))
			{
				return STX_RSOC_ADDR;
			}

			if (ccid != ESR_AsciiHexToChar(pFrame + 1))
			{
				return STX_RSOC_ADDR;
			}

			/* reset flag */
			pThis->bFrameDone = FALSE;

			return STX_RSOC_SPLIT_F;
		}
		else
		{
			return STX_RSOC_SPLIT;
		}
	}

	/* now can check ccid for non-splitter frame */
	/* ccid of [App Data] */
	if (pThis->bFrameDone)
	{
		if (!ESR_IsStrAsciiHex(pFrame + 1, 2))
		{
			return STX_RSOC_ADDR;
		}

		if (ccid != ESR_AsciiHexToChar(pFrame + 1))
		{
			return STX_RSOC_ADDR;
		}

		return STX_RSOC_OK;
	}

	/* it is the splitted last frame */
	pThis->bFrameDone = TRUE;
	return STX_RSOC_SPLIT_L;

}


/*==========================================================================*
 * FUNCTION : ESR_AnalyseFrame
 * PURPOSE  : 
 * CALLS    : SOC_AnalyseSTXFrame
 *			  RSOC_AnalyseSTXFrame
 * CALLED BY: 
 * ARGUMENTS: unsigned unsigned char *  pFrame : 
 *            int                       iLen   : 
 * RETURN   : FRAME_TYPE : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 16:32
 *==========================================================================*/
FRAME_TYPE ESR_AnalyseFrame(const unsigned char *pFrame, 
							int iLen,
							ESR_BASIC_ARGS *pThis)
{
	if (pFrame == NULL)
	{
		TRACE("[%s]--AnalysepFrame: ERROR: Get NULL pFrame!\n", __FILE__);
		return FRAME_ERR_PROT;
	}

	if (iLen < 1)
	{
		return FRAME_ERR_PROT;
	}

	/* One char Frame */
	if (iLen == 1)
	{
		switch (pFrame[0])
		{
		case ACK:
			return FRAME_ACK;

		case NAK:
			return FRAME_NAK;

		case EOT:
			return FRAME_EOT;

		case ENQ:
			return FRAME_ENQ;

		default:
			return FRAME_ERR_PROT;
		}
	}

	/* <DLE0> frame? */
	if (iLen == 2)
	{
		if (pFrame[0] == DLE && pFrame[1] == '0')
		{
			return FRAME_DLE0;
		}
		else
		{
			/* for EEM, SOC/TPE and RSOC protocol, no other frame length is 2 */
			return FRAME_ERR_PROT;
		}
	}

	/* POLL Frame? */
	if (pFrame[0] == EOT && iLen == 9)
	{
		/* AD1-AD6 */
		if (!ESR_IsStrAsciiHex(pFrame+1, 6)) 
		{
			return POLL_PROT;
		}

		/* Check CCID */
		if (!IsAddressOk(pFrame+1))
		{
			return POLL_ADDR;
		}

		/* 'P' (Poll) */
		if (pFrame[7] != 'P') 
		{
			return POLL_PROT;
		}

		/* <ENQ> */
		if (pFrame[8] != ENQ) 
		{
			return POLL_PROT;
		}

		/* everything OK! */
		return POLL_OK;
	}

	/* Select frame? */
	if (pFrame[0] == EOT && iLen >= 21)
	{
		return AnalyseSelectFrame(pFrame, iLen);
	}

	/* STX frame? */
    if (pFrame[0] == STX)
	{
		/* ensure safe access */
		if (iLen < 8)
		{
			return FRAME_ERR_PROT;
		}

		if (pFrame[4] == '+' || pFrame[4] == '*')
		{
			return SOC_AnalyseSTXFrame(pFrame, iLen, pThis);
		}
		else
		{
			return RSOC_AnalyseSTXFrame(pFrame, iLen, pThis);
		}
	}

	return FRAME_ERR_PROT;
}

/*==========================================================================*
 * FUNCTION : ESR_ExtractDataFromFrame
 * PURPOSE  : extract cmd data form frame
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN FRAME_TYPE frameType  : 
 *			  IN const unsigned char  *pFrame   : 
 *            IN int         iFrameLen : 
 *            OUT int        *piDataLen: 
 * RETURN   : const unsigned char *: 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 20:46
 *==========================================================================*/
const unsigned char *ESR_ExtractDataFromFrame(IN FRAME_TYPE frameType,
							   IN const unsigned char *pFrame, 
						       IN int iFrameLen, 
						       OUT int *piDataLen)
{
	const unsigned char *p;

	switch (frameType)
	{
	case SELECT_OK:
		*piDataLen = (iFrameLen - 2) -16;
		p = pFrame + 16;
		break;

	case STX_SOC_OK:
	case STX_SOC_SPLIT_F:
		*piDataLen = (iFrameLen - 19 ) - 3;
		p = pFrame + 20;
		break;
	
	case STX_SOC_SPLIT:
	case STX_SOC_SPLIT_L:
		*piDataLen = iFrameLen - 19;
		p = pFrame + 17;
		break;

	case STX_SOC_SPECIAL:
		/* <LF> has moved */
		*piDataLen = iFrameLen - 19 -1;
		p = pFrame + 17;
		break;

	case STX_RSOC_OK:
	case STX_RSOC_SPLIT_F:
		*piDataLen = (iFrameLen - 3) - 3;
		p = pFrame + 4;
		break;

	case STX_RSOC_SPLIT:
	case STX_RSOC_SPLIT_L:
		*piDataLen = iFrameLen - 3;
		p = pFrame + 1;
		break;

	default:
		*piDataLen = 0;
		p = NULL;
	}

	return p;
}


/*==========================================================================*
 * FUNCTION : ESR_BuildEEMResponseFrame
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char  *szResData    : 
 *            OUT unsigned char        *pFrameData  :
 *			  OUT int      *piFrameDataLen :
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-13 09:37
 *==========================================================================*/
void ESR_BuildEEMResponseFrame(IN const unsigned char *szResData,
							   OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen)
{
	COMMON_CONFIG *pConfig;
	unsigned char *pData;


	pData = pFrameData;
	/* frame format: <SOH> <AD1-AD6> <STX> <DATA> <ETX> <BCC> */ 
	/* 1.build frame head */
	pConfig = &g_EsrGlobals.CommonConfig;
	sprintf(pData, "%c%02X0000%c", SOH, pConfig->byCCID, STX);
	*piFrameDataLen = 8;

	/* 2.builder DATA */
	pData += 8;
	while (*szResData != '\0' && (*piFrameDataLen) <= ESR_MAX_FRAME_LEN - 3)
	{
		*pData++ = *szResData++;
		(*piFrameDataLen)++;
	}

	/* change to '*' for overflow case */
	if ((*piFrameDataLen) > ESR_MAX_FRAME_LEN - 3)
	{
		*(--pData) = '*';
		pData++;
	}

	/* 3.build frame tail */
	*piFrameDataLen += 2;
	*pData++ = ETX;
	*pData = CalcBCC(pFrameData + 1, (*piFrameDataLen) - 2);


	return;
}
	

/*==========================================================================*
 * FUNCTION : ESR_BuildSOCResponseFrame
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN BOOL                 bSoc            : 
 *            IN const unsigned char  *szResData      : 
 *            IN BOOL                 bInsertCCID     : 
 *            IN const unsigned char  *szSoc_proc     : NULL for alarm report,
 *														use FF051 to report
 *            OUT unsigned char       *pFrameData     : 
 *            OUT int                 *piFrameDataLen : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-14 16:41
 *==========================================================================*/
void ESR_BuildSOCResponseFrame(IN BOOL bSoc,
							   IN const unsigned char *szResData, 
						       IN BOOL bInsertCCID, 
						       IN const unsigned char *szSoc_proc, 
						       OUT unsigned char *pFrameData,
						       OUT int *piFrameDataLen)
{
	int iResDataLen, iFrameDataLen;
	BYTE byCCID;
	char szSOCID[4], szCCID[4] = "";  //both of them end with NULL
	int  iSocID, iCCIDStrLen = 0;

	iResDataLen = strlen(szResData);
	iSocID = g_EsrGlobals.CommonConfig.iSOCID;

	byCCID = ESR_GetCCID();


	if (bInsertCCID)
	{	
		sprintf(szCCID, "%02X!", byCCID);
		iCCIDStrLen = 3;
	}


	/* RSOC protocol */
	if (!bSoc)
	{
		iFrameDataLen = 3 + iCCIDStrLen + iResDataLen;
		sprintf(pFrameData, "%c%s%s%c", STX, szCCID, szResData, ETX);
	}

	/* SOC protocol */
	else
	{
		SocIDToAscii(iSocID, szSOCID);
		iFrameDataLen = 19 + iResDataLen + iCCIDStrLen;

		if (szSoc_proc == NULL) /* alarm report */
		{
			sprintf(pFrameData, "%c000+FF051%02X%s00%s%s%c", STX,
				iResDataLen + iCCIDStrLen, szSOCID, szCCID, szResData, ETX);
		}
		else
		{
			sprintf(pFrameData, "%c000+%s%02X%s00%s%s%c", STX, szSoc_proc,
				iResDataLen + iCCIDStrLen, szSOCID, szCCID, szResData, ETX);
		}
	}

	/* add LRC for both */
	sprintf(pFrameData + iFrameDataLen - 1, 
		"%c", CalcLRC(pFrameData, iFrameDataLen - 1));

	*piFrameDataLen = iFrameDataLen;
}




#ifdef COM_SHARE_SERVICE_SWITCH

#define CRC32_DEFAULT    0x04C10DB7
static void BuildTable32(unsigned long aPoly , unsigned long *Table_CRC)
{
    unsigned long i, j;
    unsigned long iData;
    unsigned long iAccum;

    for (i = 0; i < 256; i++)
    {
        iData = (unsigned long )( i << 24);
        iAccum = 0;
        for (j = 0; j < 8; j++)
        {
            if (( iData ^ iAccum ) & 0x80000000)
			{
                iAccum = ( iAccum << 1 ) ^ aPoly;
			}
            else
			{
                iAccum <<= 1;
			}
            iData <<= 1;
        }
        Table_CRC[i] = iAccum;
    }
}

static unsigned long RunCRC32(const unsigned char *aData, 
							  unsigned long aSize, 
							  unsigned long aPoly)
{
    unsigned long Table_CRC[256]; // CRC table
    unsigned long i;
    unsigned long iAccum = 0;

    BuildTable32(aPoly, Table_CRC);
    
    for (i = 0; i < aSize; i++)
	{
        iAccum = ( iAccum << 8 ) ^ Table_CRC[( iAccum >> 24 ) ^ *aData++];
	}

    return iAccum;
}

BOOL ESR_IsMtnFrame(const unsigned char *pFrame, int iLen)
{
	DWORD dwCalCRC32;

	if (iLen < 13)
	{ 
		return FALSE;
	}

	if (pFrame[0] != 0x7e)
	{
		return FALSE;
	}

	if (pFrame[3] != 0xd5)
	{
		return FALSE;
	}

	if (pFrame[4] != 0xcc)
	{
		return FALSE;
	}
	
	if (pFrame[6] != 0 || pFrame[7] != 0)
	{
		return FALSE;
	}

	if (pFrame[12] != 0x0d)
	{
		return FALSE;
	}

	dwCalCRC32 =  *((DWORD *)(pFrame + 8));
	if (dwCalCRC32 != RunCRC32(pFrame + 1, 7, (unsigned long)CRC32_DEFAULT))
	{
		return FALSE;
	}

	return TRUE;
}

	

void ESR_BuildMtnResponseFrame(OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen)
{
	pFrameData[0] = 0x7e;
	pFrameData[1] = 1;
	pFrameData[2] = 0xff;
	pFrameData[3] = 0xd5;

	/* com is using */
	pFrameData[4] = 0x11;    

	pFrameData[5] = 0;
	pFrameData[6] = 0;
	pFrameData[7] = 0;

	/* get CRC */
	*((DWORD *)(pFrameData + 8)) = 
		RunCRC32(pFrameData + 1, 7, (unsigned long)CRC32_DEFAULT);

	pFrameData[12] = 0x0d;

	*piFrameDataLen = 13;

	return;
}

#endif //COM_SHARE_SERVICE_SWITCH

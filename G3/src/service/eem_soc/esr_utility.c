/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : esr_utility.c
 *  CREATOR  : LinTao                   DATE: 2004-11-25 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "esr.h"

/*==========================================================================*
 * FUNCTION : ESR_GetCCID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BYTE : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-02 14:43
 *==========================================================================*/
__INLINE BYTE ESR_GetCCID(void)
{
	COMMON_CONFIG *pCommonCfg;

	pCommonCfg = &g_EsrGlobals.CommonConfig;

	if (pCommonCfg->iProtocolType == SOC_TPE)
	{
		return pCommonCfg->byCCIDfromSOC;
	}
	else
	{
		return pCommonCfg->byCCID;
	}
}


/*==========================================================================*
 * FUNCTION : CFG_CheckBase
 * PURPOSE  : basic function for check a null-ended string, used by other 
 *            specifically realized check functions
 * CALLS    : 
 * CALLED BY: specifically realized check functions, such as ESR_CheckPhoneNumber
 * ARGUMENTS: const char  *szObject : 
 *            LEGAL_CHR   fnCmp     : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 11:18
 *==========================================================================*/
BOOL CFG_CheckBase(const char *szObject, LEGAL_CHR fnCmp)
{
	const char *p;

	if (szObject == NULL)
	{
		return FALSE;
	}

	p = szObject;
	for (; p != NULL; p++)
	{
		if (*p == '\0')
		{
			return TRUE;
		}

		if (!fnCmp(*p))
		{
			return FALSE;
		}

	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : ESR_CheckPhoneNumber
 * PURPOSE  : for checking phone number null-ended string
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szPhoneNumber : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-18 20:25
 *==========================================================================*/
static BOOL IsPhoneNumber(char c)
{
	return  ((c >= '0' && c <= '9') ||
		 c == ',' || c == '-');
}
static BOOL IsProdctInfom_Char(char c)
{
	return (c >= 0x20 && c < 0x7F && (c != '*') && (c != '#'));
}
	
BOOL ESR_CheckPhoneNumber(const char *szPhoneNumber)
{
	return CFG_CheckBase(szPhoneNumber, IsPhoneNumber);
}

BOOL ESR_CheckProdctInfom(const char *szProdctInfom)
{
	return CFG_CheckBase(szProdctInfom, IsProdctInfom_Char);
}

/*==========================================================================*
 * FUNCTION : ESR_CheckIPAddress
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szIPAddress : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-18 20:49
 *==========================================================================*/
static BOOL IsIPAddress(char c)
{
	return  ((c >= '0' && c <= '9') ||
		c == '.' || c == ':');
}
static BOOL IsIPV6Address(char c)
{
	return  ((c >= '0' && c <= '9')
			|| (c >= 'a' && c <= 'f')
			|| (c >= 'A' && c <= 'F')
			|| c == ':');
}
static BOOL IsAValidNumber(char c)
{
	return  ((c >= '0' && c <= '9'));
}
BOOL ESR_CheckIsANumber(const char *szIn)
{
	return CFG_CheckBase(szIn, IsAValidNumber);
}
BOOL ESR_CheckIPAddress(char *szIPAddress, int nCheckType)
{
	//printf("\n-------Begin Checking {%s}, nType = %d---------------\n",szIPAddress,nCheckType);
	if(nCheckType == CHECK_IPV4)
	{
		return CFG_CheckBase(szIPAddress, IsIPAddress);
	}
	else if(nCheckType == CHECK_IPV6_ADDR_ONLY)
	{
		return CFG_CheckBase(szIPAddress, IsIPV6Address);
	}
	else 
	{
		if (szIPAddress == NULL)
		{
			return FALSE;
		}
		else if(*szIPAddress == '\0')
		{
			return TRUE;
		}
		else
		{
			int iLen = strlen(szIPAddress);
			if (iLen < 3)
			{
				return FALSE;
			}
			char cNeedChk1 = '(';
			char cNeedChk2 = ')';
			char cNeedReplace1 = '[';
			char cNeedReplace2 = ']';
			if(nCheckType == CHECK_IPV6_ADDR_AND_PORT2)
			{
				cNeedChk1 = '[';
				cNeedChk2 = ']';
				cNeedReplace1 = '(';
				cNeedReplace2 = ')';
			}

			//��Ҫ�û�'('�롮[���Լ�')'��']'
			char *p;
			p = szIPAddress;
			BOOL bHasStart = FALSE;
			BOOL bHasEnd = FALSE;
			int i = 0;
			for(;szIPAddress != NULL;szIPAddress++,i++)
			{
				if(*szIPAddress == '\0')
				{
					break;
				}
				else if(*szIPAddress == cNeedChk1)
				{
					if(i == 0)
					{
						bHasStart = TRUE;
					}
					*szIPAddress = cNeedReplace1;
				}
				else if(*szIPAddress == cNeedChk2)
				{
					bHasEnd = TRUE;
					*szIPAddress = cNeedReplace2;
				}
			}

			if(!bHasStart || !bHasEnd)
			{
				return FALSE;
			}

			for (; p != NULL; p++)
			{
				if (*p == '\0')
				{
					return TRUE;
				}
				if (!IsIPV6Address(*p))
				{
					if((*p != cNeedReplace1) && (*p != cNeedReplace2))
					{
						return FALSE;
					}
				}

			}

			return TRUE;
		}

	}

}


/*==========================================================================*
 * FUNCTION : ESR_CheckBautrateCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szBautrateCfg : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-18 21:49
 * Modify   : ZhaoZicheng	       DATE: 2015-12-07 Support 'E' and 'O'
 *==========================================================================*/
static BOOL IsBautrateCfg(char c)
{
    return  ((c >= '0' && c <= '9') ||
	c == ',' || c == 'n' || c == 'N' || c == 'e'|| c == 'E'|| c == 'o'|| c == 'O');
}

BOOL ESR_CheckBautrateCfg(const char *szBautrateCfg)
{
	int i, iLen, count = 0;

	if (!CFG_CheckBase(szBautrateCfg, IsBautrateCfg))
	{
		return FALSE;
	}

	iLen = (int)strlen(szBautrateCfg);
	for (i = 0; i < iLen; i++)
	{
		if (szBautrateCfg[i] == ',')
		{
			count++;
		}
	}

	if (count != 3)
	{
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ESR_CheckModemCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szBautrateCfg : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-18 21:55
 *==========================================================================*/
BOOL ESR_CheckModemCfg(const char *szModemCfg)
{
	char szSubField[30];

	szModemCfg = Cfg_SplitString((char *)szModemCfg, szSubField, sizeof(szSubField), ':');
	if (!ESR_CheckBautrateCfg(szSubField))
	{
		return FALSE;
	}

	Cfg_SplitString((char *)szModemCfg, szSubField, sizeof(szSubField), ':');
	if (!ESR_CheckPhoneNumber(szSubField))
	{
		return FALSE;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ESR_AsciiHexToChar
 * PURPOSE  : get value from ascii hex. eg: "1A" = 1*16+10 =26
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : LinTao                   DATE: 2004-10-12 19:24
 *==========================================================================*/
BYTE ESR_AsciiHexToChar(IN const unsigned char *pStr)
{
	BYTE value;
	unsigned char c;

	ASSERT(pStr);

	c = pStr[0];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = c * 0x10;

	c = pStr[1];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value += c;

	return value;
}


/*==========================================================================*
 * FUNCTION : ESR_IsStrAsciiHex
 * PURPOSE  : assistant functions
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const unsigned char  *pStr : 
 *            int                  iLen  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 17:24
 *==========================================================================*/
BOOL ESR_IsStrAsciiHex(const unsigned char *pStr, int iLen)
{
	int i;
	
	for (i = 0; i < iLen; i++)
	{
		if ((pStr[i] >= '0' && pStr[i] <= '9') ||
			(pStr[i] >= 'A' && pStr[i] <= 'F'))
		{
			continue;
		}
		
		return FALSE;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ESR_GetUnprintableChr
 * PURPOSE  : for trace
 * CALLS    : 
 * CALLED BY: ESR_PrintEvent
 * ARGUMENTS: IN unsigned char  chr   : 
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-25 14:30
 *==========================================================================*/
unsigned char *ESR_GetUnprintableChr(IN unsigned char chr)
{
	switch (chr)
	{
	case 0x06:
		return "<ACK>";

	case 0x15:
		return "<NAK>";

	case 0x04:
		return "<EOT>";

	case 0x05:
		return "<ENQ>";

	case 0x01:
		return "<SOH>";

	case 0x02:
		return "<STX>";

	case 0x03:
		return "<ETX>";

	default:
		return "<NOT EXIST>";
	}
}


/*==========================================================================*
 * FUNCTION : ESR_PrintEvent
 * PURPOSE  : print ESR Event info(used for debug)
 * CALLS    : ESR_GetUnprintableChr
 * CALLED BY: 
 * ARGUMENTS: ESR_EVENT  *pEvent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-24 10:33
 *==========================================================================*/
void ESR_PrintEvent(ESR_EVENT *pEvent)
{
#ifdef _SHOW_ESR_EVENT_INFO
	int i;
	unsigned char chr;
	char szEventType[50];

	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:
		strncpy(szEventType, "ESR_FRAME_EVENT", 50);
		break;

	case ESR_CONNECTED_EVENT:
		strncpy(szEventType, "ESR_CONNECTED_EVENT", 50);
		break;

	case ESR_CONNECT_FAILED_EVENT:
		strncpy(szEventType, "ESR_CONNECT_FAILED_EVENT", 50);
		break;

	case ESR_DISCONNECTED_EVENT:
		strncpy(szEventType, "ESR_DISCONNECTED_EVENT", 50);
		break;

	case ESR_TIMEOUT_EVENT:
		strncpy(szEventType, "ESR_TIMEOUT_EVENT", 50);
		break;

	default:
		strncpy(szEventType, "ERROR: not defined event type!", 50);
	}

	printf("ESR Event Info\n");
	printf("Event Type: %s\n", szEventType);
	printf("Event Data length: %d\n", pEvent->iDataLength);

	printf("Event Data(Hex format):");
	for (i = 0; i < pEvent->iDataLength; i++)
	{
		printf("%02X ", pEvent->sData[i]);
	}
	printf("\n");

	printf("Event Data(Text format):");
	for (i = 0; i < pEvent->iDataLength; i++)
	{
		chr = pEvent->sData[i];
		if (chr < 0x21 || chr > 0X7E)
		{
			printf("%s", ESR_GetUnprintableChr(chr));
		}
		else
		{
			printf("%c", chr);
		}
	}
	printf("\n");
		
	printf("Event Flags: \nSkip Falg: %d	Dummy Flag: %d\n", 
		pEvent->bSkipFlag, pEvent->bDummyFlag);

#endif //_DEBUG_ESR_LINKLAYER
    UNUSED(pEvent);
	return;
}


/*==========================================================================*
 * FUNCTION : ESR_PrintState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iState : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-27 09:45
 *==========================================================================*/
void ESR_PrintState(int iState)
{
#ifdef _SHOW_ESR_CUR_STATE
	char *szStateName;

	if (g_EsrGlobals.CommonConfig.iProtocolType == EEM)  //EEM State machine
	{
		switch (iState)
		{
		case EEM_IDLE:
			szStateName = "EEM_IDLE";
			break;

		case EEM_WAIT_FOR_POLL:
			szStateName = "EEM_WAIT_FOR_POLL";
			break;

		case EEM_WAIT_FOR_RESP_ACK:
			szStateName = "EEM_WAIT_FOR_RESP_ACK";
			break;

		case EEM_SEND_CALLBACK:
			szStateName = "EEM_SEND_CALLBACK";
			break;

		case EEM_WAIT_FOR_CALLBACK_ACK:
			szStateName = "EEM_WAIT_FOR_CALLBACK_ACK";
			break;

		case EEM_SEND_ALARM:
			szStateName = "EEM_SEND_ALARM";
			break;

		case EEM_WAIT_FOR_ALARM_ACK:
			szStateName = "EEM_WAIT_FOR_ALARM_ACK";
			break;

		case STATE_MACHINE_QUIT:
			szStateName = "STATE_MACHINE_QUIT";
			break;

		default:
			szStateName = "Error EEM State Name";
		}
	}

	else
	{
		switch (iState)
		{
		case SOC_OFF:
			szStateName = "SOC_OFF";
			break;

		case SOC_SEND_ENQ:
			szStateName = "SOC_SEND_ENQ";
			break;

		case SOC_WAIT_DLE0:
			szStateName = "SOC_WAIT_DLE0";
			break;

		case SOC_ALARM_REPORT:
			szStateName = "SOC_ALARM_REPORT";
			break;

		case SOC_WAIT_CMD:
			szStateName = "SOC_WAIT_CMD";
			break;

		case SOC_WAIT_ACK:
			szStateName = "SOC_WAIT_ACK";
			break;

		case STATE_MACHINE_QUIT:
			szStateName = "STATE_MACHINE_QUIT";
			break;

		default:
			szStateName = "Error SOC State Name";

		}
	}

	TRACE("++++++++++++++++++++++++++++++++++++++++++++++\n");
	TRACE("+  Current State: %-25s  +\n", szStateName);
	TRACE("++++++++++++++++++++++++++++++++++++++++++++++\n");
	
	return;
#endif //_SHOW_ESR_CUR_STATE

	UNUSED(iState);
	return;
}

/*==========================================================================*
 * FUNCTION : ESR_GetReportMsg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN BOOL      bAlarm		: TRUE  for SendAlarm 
 *										  FALSE for SendCallback
 *		      IN OUT char  *szReportMsg : 
 *            IN int       iBufLen      : 
 * RETURN   : int : 0 for success, 
 *					1 for call DXI to get site name failed,
 *					2 for site name is too long
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 19:18
 *==========================================================================*/
int ESR_GetReportMsg(IN BOOL bAlarm, 
					 IN OUT char *szReportMsg, 
					 IN int iBufLen)
{
	size_t iLen;
	int nError, nInterfaceType, nVarID, nVarSubID, nBufLen;
	LANG_TEXT *pLangInfo;
	char szSiteName[64];

	BYTE  byCCID;

	ASSERT(iBufLen > 70);

	/* 1.get site name */
	nInterfaceType = VAR_ACU_PUBLIC_CONFIG;
	nVarID = SITE_NAME;
	nVarSubID = 0;
	pLangInfo = NULL;
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pLangInfo,			
		0);

	if (nError != ERR_DXI_OK)
	{
		szReportMsg[0] = '\0';
		return 1;
	}

	strncpyz(szSiteName, pLangInfo->pFullName[0], sizeof(szSiteName));


	/* 2.get CCID */
	//byCCID = g_EsrGlobals.CommonConfig.byCCID;
	byCCID =  ESR_GetCCID();

	/* 3.now build the string */
	if (bAlarm)  //called by OnSendAlarm state
	{
		sprintf(szReportMsg, "%s!%02X!E*", szSiteName, byCCID);
	}
	else  //called by OnSendCallback state
	{
		sprintf(szReportMsg, "%s!%02X!C*", szSiteName, byCCID);
	}

	/* 4.check buffer length */
	iLen = strlen(pLangInfo->pFullName[0]);
	if (iLen >= 64)
	{
		return 2;
	}

	return 0;

}

/*==========================================================================*
 * FUNCTION : PrintCommonCfg
 * PURPOSE  : Print ESR Common Config info
 * CALLS    : 
 * CALLED BY: PrintESRConfigInfo
 * ARGUMENTS: COMMON_CONFIG *pConfig : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-24 10:39
 *==========================================================================*/
void ESR_PrintCommonCfg(COMMON_CONFIG *pConfig)
{
	char *pText;
	int iTmp, i;

	printf("************************************************\n");
	printf("*****          ESR Common Config         *******\n");
	printf("************************************************\n");

	/* protocol type */
	iTmp = pConfig->iProtocolType;
	switch (iTmp)
	{
	case 1:
		pText = "EEM";
		break;
		
	case 2:
		pText = "RSOC";
		break;

	case 3:
		pText = "";
		break;

	default:
		pText = "ERROR! not such protocol type!";
	}
	printf("Protocol Type: %s\n", pText);

	/* ccid and socid */
	iTmp = pConfig->byCCID;
	printf("CCID is: %d\n", iTmp);

	iTmp = pConfig->iSOCID;
	printf("SOCID is: %d\n", iTmp);

	iTmp = pConfig->byCCIDfromSOC;
	printf("CCID calculated from SOCID is: %d\n", iTmp);

	/* report and callback in use */	
	printf("Report In use: %d\n", pConfig->bReportInUse);
	printf("Callback In use: %d\n", pConfig->bCallbackInUse);

	/* media */
	switch (pConfig->iMediaType)
	{
	case 0:
		pText = "Leased Line";
		break;

	case 1:
		pText = "Modem";
		break;

	case 2:
		pText = "TCPIP";
		break;

	default:
		pText = "ERROR, not such media.\n";
	}

	printf("Operation media is: %s\n", pText);

	/* attemps and elaps */
	iTmp = pConfig->iMaxAttempts;
	printf("Max attemps: %d\n", iTmp);
	iTmp = pConfig->iAttemptElapse;
	printf("Elapse time is: %d\n", iTmp);

	/* phone numbers */
	for (i = 0; i < ESR_ALARM_REPORT_NUM; i++)
	{
		printf("Alarm report number %d is: %s\n", 
			i + 1, pConfig->szAlarmReportPhoneNumber[i]);
	}

	for (i = 0; i < ESR_CALLBACK_NUM; i++)
	{
		printf("Callback phone number %d is: %s\n", 
			i + 1, pConfig->szCallbackPhoneNumber[i]);
	}

	/* ips */
	for (i = 0; i < ESR_ALARM_REPORT_NUM; i++)
	{
		printf("Alarm report IP %d is: %s\n", 
			i + 1, pConfig->szReportIP[i]);
	}

	for (i = 0; i < ESR_SECURITY_IP_NUM; i++)
	{
		printf("Security IP %d is: %s\n", 
			i + 1, pConfig->szSecurityIP[i]);
	}

	/* security level */
	printf("Security level is: %d\n", pConfig->iSecurityLevel);

	return;
}

/*==========================================================================*
 * FUNCTION : ESR_ClearEventQueue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hq       : 
 *            BOOL    bDestroy : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-04-21 15:11
 *==========================================================================*/
void ESR_ClearEventQueue(HANDLE hq, BOOL bDestroy)
{
	int i, iCount, iRet;
	ESR_EVENT *pEvent;
	struct timeval tv;

	if (hq)
	{
		iCount = Queue_GetCount(hq, NULL);
		printf("-----------------iCount = %d\n", iCount);
		/* clear memory */
		for (i = 0; i < iCount; i++)
		{

			gettimeofday(&tv,NULL);
			printf("--------In hEventOutputQueue time %u:%u\n",tv.tv_sec,tv.tv_usec);
			iRet = Queue_Get(hq, &pEvent, FALSE, 1000);
			gettimeofday(&tv,NULL);
			printf("-----------Out hEventOutputQueue time %u:%u iRet= %d\n",tv.tv_sec,tv.tv_usec, iRet);
			if (iRet == ERR_OK)
			{
				DELETE_ESR_EVENT(pEvent);
			}
		}

		/* destroy queue */
		if (bDestroy)
		{
			Queue_Destroy(hq);
		}
	}
}


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : config_builder.c
 *  CREATOR  : LinTao                   DATE: 2004-10-15 17:03
 *  VERSION  : V1.00
 *  PURPOSE  : init config
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "esr.h"

/* define splitter charater used by ESR config files */
#define SPLITTER		 ('\t')

/* config file names */
#define CONFIG_FILE_EEMMODELMAP	  "private/eem/EEMModelMap.cfg"
#define CONFIG_FILE_ESRCOMMON	  "private/eem/ESRCommonConfig.cfg"

/* section names of ESR common config file */
#define  PROTOCOL_TYPE			 "[Protocol Type]"
#define  ESR_CCID                "[CCID]"
#define  ESR_SOCID				 "[SOCID]"
#define  REPORT_IN_USE			 "[Report in use]"
#define  CALLBACK_IN_USE		 "[callback in use]"
#define  OPERATION_MEDIA		 "[Operation media]"
#define  MEDIA_PORT_PARAM		 "[Media Port Param]"
#define  MAX_REPORT_ATTAMPS		 "[Max alarm report attempts]"
#define  ELAPS_TIME				 "[Call elapse time]"

#define  REPORT_NUMBER_1		 "[Primary report number]"
#define  REPORT_NUMBER_2		 "[Secondary report number]"
#define  CALLBACK_NUMBER		 "[Callback phone number]"

#define  REPORT_IP_1			 "[Primary report IP-address]"
#define  REPORT_IP_2			 "[Secondary report IP-address]"

#define  SECURITYI_IP_1			 "[IP-address1]"
#define  SECURITYI_IP_2			 "[IP-address2]"

#define  SECURITY_LEVEL			 "[Security Level]"

#define  PRODCT_INFOM			 "[Product Info]"

/* config items of ESR common Config file */
#define  CFG_ITEM_NUM			 18     

/* max length of cfg itme */
#define  MAX_CFG_ITEM_LEN        100

/* used to detect modified config item */
#define ESR_CFG_ITEM_TEST(_nVarID, _ItemMask) \
	(((_nVarID) & (_ItemMask)) == (_ItemMask) || \
	 ((_nVarID) & ESR_CFG_ALL) == ESR_CFG_ALL)


/* sections names of EEM Model Map config file */
#define  TYPE_MAP_NUM			 "[TYPE_MAP_NUM]"
#define  TYPE_MAP_INFO_SEC       "[TYPE_MAP_INFO]"
/* note: will be extended at runtime */
#define  MAPENTRIES_INFO_SEC		 "[MAPENTRIES_INFO]"


/* max type maps of EEM Model config file */
#define  EEMMODEL_MAX_MAPS       50


/* to simplify log action when read ESR common config file */
#define LOG_ESR_COMMON_CFG(_section)  \
	(AppLogOut("Read ESR Common Config", APP_LOG_ERROR, "Read %s failed, " \
			"not such section or format is invalid.\n", (_section)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s failed, not such section or format" \
             " is invalid.\n", __FILE__, __LINE__, __FUNCTION__, (_section)))

#define LOG_ESR_COMMON_CFG_2(_section)  \
	(AppLogOut("Read ESR Common Config", APP_LOG_ERROR, "Read %s failed, " \
			"format is invalid.\n", (_section)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s failed, format is invalid.\n", \
            __FILE__, __LINE__, __FUNCTION__, (_section)))



/* simplify log function when read fields of a cfg table */
/* DT: data type error */
#define LOG_READ_TABLE_DT(_tableName, _fieldName)  \
	(AppLogOut("Load EEM Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"data type of %s field is wrong.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, data type of %s "  \
        "field is wrong.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* DR: data range exceeds */ 
#define LOG_READ_TABLE_DR(_tableName, _fieldName)  \
	(AppLogOut("Load EEM Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"%s field data exceeds the bound.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field data exceeds"  \
        "the bound.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))

/* NO: not find the filed. may caused by bad format */
#define LOG_READ_TABLE_NO(_tableName, _fieldName) \
	(AppLogOut("Load EEM Model Config", APP_LOG_ERROR, "Read %s table failed, %s " \
		"field not found(please use the correct EMPTY FIELD character).\n", \
		(_tableName), (_fieldName)), \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field not found"  \
        "(please use the correct EMPTY FIELD character).\n", \
		__FILE__, __LINE__, __FUNCTION__, (_tableName), (_fieldName)))

/* ET: field is empty */
#define LOG_READ_TABLE_ET(_tableName, _fieldName) \
	(AppLogOut("Load EEM Model Config", APP_LOG_ERROR, "Read %s table failed, " \
		"%s field should not be empty.\n", (_tableName), (_fieldName)),  \
	TRACE("[%s:%d]--%s: ERROR: Read %s table failed, %s field should not"  \
        "be empty.\n", __FILE__, __LINE__, __FUNCTION__, \
		(_tableName), (_fieldName)))


/*==========================================================================*
 * FUNCTION : IsEmptyField
 * PURPOSE  : empty config item in ESR common cfg file
 * CALLS    : 
 * CALLED BY: LoadESRCommonConfigProc
 * ARGUMENTS: const char  *pField : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-01 16:07
 *==========================================================================*/
static BOOL IsEmptyField(const char *pField)
{
	return (pField[0] == '-');
}

static  BOOL ESR_IsIB1Type()
{
#define IB_GROUP_ID			201

	SIG_BASIC_VALUE*		pSigValue = NULL;
	int				iBufLen;
	
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							IB_GROUP_ID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 1),//IB type
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (pSigValue && (iError == ERR_DXI_OK))
	{
		if(pSigValue->varValue.ulValue == 0)//IB1
		{
			return TRUE;//IB1 
		}
		

	}
	return FALSE;//IB2	
	
}
/*==========================================================================*
 * FUNCTION : LoadESRCommonConfigProc
 * PURPOSE  : callback function called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: void  *pCfg       : 
 *            void  *pLoadToBuf : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-18 16:38
 *==========================================================================*/
static int LoadESRCommonConfigProc(void *pCfg, void *pLoadToBuf)
{
	COMMON_CONFIG *pCommonCfg;
	int iRet, iBuf;

	pCommonCfg = (COMMON_CONFIG *)pLoadToBuf;

	/* 1.protocol type */
	iRet = Cfg_ProfileGetInt(pCfg, PROTOCOL_TYPE, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("protocol type");
		return ERR_CFG_FAIL;
	}
	/*check protocol type */
	/* 1: EEM  2: RSOC  3: SOC/TPE */
	if (iBuf< 1 || iBuf > 4)
	{
		LOG_ESR_COMMON_CFG_2("Protocol Type");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->iProtocolType = iBuf;


	/* 2.ccid */
	iRet = Cfg_ProfileGetInt(pCfg, ESR_CCID, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("CCID");
		return ERR_CFG_FAIL;
	}
    if (iBuf < 0 || iBuf > 255)
	{
		LOG_ESR_COMMON_CFG_2("CCID");
		return ERR_CFG_BADCONFIG;
	}
	/* EEM or RSOC protocol should configurd CCID */
	if (iBuf == 0 && (pCommonCfg->iProtocolType == 1 || 
		pCommonCfg->iProtocolType == 2))
	{
		LOG_ESR_COMMON_CFG_2("CCID");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->byCCID = (BYTE)iBuf;

	/* 3.socid */
	iRet = Cfg_ProfileGetInt(pCfg, ESR_SOCID, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("SOCID");
		return ERR_CFG_FAIL;
	}
    if (iBuf < 0 || iBuf > 20479)
	{
		LOG_ESR_COMMON_CFG_2("SOCID");
		return ERR_CFG_BADCONFIG;
	}
	/* SOC/TPE protocol should configurd SOCID */
	if (iBuf == 0 && pCommonCfg->iProtocolType == 3)
	{
		LOG_ESR_COMMON_CFG_2("SOCID");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->iSOCID = iBuf;
	/* calculate ccid from socid */
	pCommonCfg->byCCIDfromSOC = (BYTE)(iBuf & 0XFF);

	/* 4.report in use */
	iRet = Cfg_ProfileGetInt(pCfg, REPORT_IN_USE, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Report In Use flag");
		return ERR_CFG_FAIL;
	}
	/* check value */
	switch (iBuf)
	{
	case 0:
		pCommonCfg->bReportInUse = FALSE;
		break;

	case 1:
		pCommonCfg->bReportInUse = TRUE;
		break;

	default:
		LOG_ESR_COMMON_CFG_2("Report In Use flag");
		return ERR_CFG_BADCONFIG;
	}
	

	/* 5.callback in use */
	iRet = Cfg_ProfileGetInt(pCfg, CALLBACK_IN_USE, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Callback In Use");
		return ERR_CFG_FAIL;
	}
	/* check value */
	switch (iBuf)
	{
	case 0:
		pCommonCfg->bCallbackInUse = FALSE;
		break;

	case 1:
		pCommonCfg->bCallbackInUse = TRUE;
		break;

	default:
		LOG_ESR_COMMON_CFG_2("Callback In Use");
		return ERR_CFG_BADCONFIG;
	}


	/* 6.operation media */
	iRet = Cfg_ProfileGetInt(pCfg, OPERATION_MEDIA, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Operation Media");
		return ERR_CFG_FAIL;
	}
	/* check value */
	if (iBuf < MEDIA_TYPE_LEASED_LINE || iBuf > MEDIA_TYPE_TCPIPV6)
	{
		LOG_ESR_COMMON_CFG_2("Operation Media");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->iMediaType = iBuf;


	/* 7.operation media port param */
	iRet = Cfg_ProfileGetString(pCfg, 
								MEDIA_PORT_PARAM, 
								pCommonCfg->szCommPortParam,
								COMM_PORT_PARAM_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Media Port Param");
		return ERR_CFG_FAIL;
	}
	switch (pCommonCfg->iMediaType)
	{
	case MEDIA_TYPE_LEASED_LINE:
		if (!ESR_CheckBautrateCfg(pCommonCfg->szCommPortParam))
		{
			LOG_ESR_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;

	case MEDIA_TYPE_MODEM:
		if (!ESR_CheckModemCfg(pCommonCfg->szCommPortParam))
		{
			LOG_ESR_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;

	case MEDIA_TYPE_TCPIP:
		if (!ESR_CheckIsANumber(pCommonCfg->szCommPortParam))
		{
			LOG_ESR_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;

	case MEDIA_TYPE_TCPIPV6:
		if (!ESR_CheckIsANumber(pCommonCfg->szCommPortParam))
		{
			LOG_ESR_COMMON_CFG_2("Media Port Param");
			return ERR_CFG_BADCONFIG;
		}
		break;
	default:
		LOG_ESR_COMMON_CFG_2("Media Port Param");
		return ERR_CFG_BADCONFIG;
	}
	

	/* 8.max report attempts */
	iRet = Cfg_ProfileGetInt(pCfg, MAX_REPORT_ATTAMPS, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Max Report Attempts");
		return ERR_CFG_FAIL;
	}
	pCommonCfg->iMaxAttempts = iBuf;

	/* 9.call elapse time */
	iRet = Cfg_ProfileGetInt(pCfg, ELAPS_TIME, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Elapse Time");
		return ERR_CFG_FAIL;
	}
	pCommonCfg->iAttemptElapse = 1000 * iBuf; //ACU timer based on ms

	/* 10.Primary report number */
	iRet = Cfg_ProfileGetString(pCfg, 
								REPORT_NUMBER_1, 
								pCommonCfg->szAlarmReportPhoneNumber[0],
								PHONENUMBER_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Primary report number");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szAlarmReportPhoneNumber[0]))
	{
		pCommonCfg->szAlarmReportPhoneNumber[0][0] = '\0';
	}
	else
	{
		if (!ESR_CheckPhoneNumber(pCommonCfg->szAlarmReportPhoneNumber[0]))
		{
			LOG_ESR_COMMON_CFG_2("Primary report number");
			return ERR_CFG_BADCONFIG;
		}
	}

	/* 11.Secondary report number */
	iRet = Cfg_ProfileGetString(pCfg, 
								REPORT_NUMBER_2, 
								pCommonCfg->szAlarmReportPhoneNumber[1],
								PHONENUMBER_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Secondary report number");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szAlarmReportPhoneNumber[1]))
	{
		pCommonCfg->szAlarmReportPhoneNumber[1][0] = '\0';
	}
	else
	{
		if (!ESR_CheckPhoneNumber(pCommonCfg->szAlarmReportPhoneNumber[1]))
		{
			LOG_ESR_COMMON_CFG_2("Secondary report number");
			return ERR_CFG_BADCONFIG;
		}
	}

	/* 12.Callback phone number */
	iRet = Cfg_ProfileGetString(pCfg, 
								CALLBACK_NUMBER, 
								pCommonCfg->szCallbackPhoneNumber[0],
								PHONENUMBER_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Callback phone number");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szCallbackPhoneNumber[0]))
	{
		pCommonCfg->szCallbackPhoneNumber[0][0] = '\0';
	}
	else
	{
		if (!ESR_CheckPhoneNumber(pCommonCfg->szCallbackPhoneNumber[0]))
		{
			LOG_ESR_COMMON_CFG_2("Callback phone number number");
			return ERR_CFG_BADCONFIG;
		}
	}

	/* 13.Primary report IP-address */
	iRet = Cfg_ProfileGetString(pCfg, 
								REPORT_IP_1, 
								pCommonCfg->szReportIP[0],
								IPADDRESS_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Primary Report IP-address");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szReportIP[0]))
	{
		pCommonCfg->szReportIP[0][0] = '\0';
	}
	else
	{
		if (!ESR_CheckIPAddress(pCommonCfg->szReportIP[0],(pCommonCfg->iMediaType == MEDIA_TYPE_TCPIPV6) ? CHECK_IPV6_ADDR_AND_PORT1 : CHECK_IPV4))
		{
			LOG_ESR_COMMON_CFG_2("Primary Report IP-address");
			return ERR_CFG_BADCONFIG;
		}
	}

	/* 14.Secondary report IP-address */
	iRet = Cfg_ProfileGetString(pCfg, 
								REPORT_IP_2, 
								pCommonCfg->szReportIP[1],
								IPADDRESS_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Secondary Report IP-address");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szReportIP[1]))
	{
		pCommonCfg->szReportIP[1][0] = '\0';
	}
	else
	{
		if (!ESR_CheckIPAddress(pCommonCfg->szReportIP[1],(pCommonCfg->iMediaType == MEDIA_TYPE_TCPIPV6) ? CHECK_IPV6_ADDR_AND_PORT1 : CHECK_IPV4))
		{
			LOG_ESR_COMMON_CFG_2("Secondary Report IP-address");
			return ERR_CFG_BADCONFIG;
		}
	}
	
	/* 15.security IP-address1 */
	iRet = Cfg_ProfileGetString(pCfg, 
								SECURITYI_IP_1, 
								pCommonCfg->szSecurityIP[0],
								IPADDRESS_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Security IP-address 1");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szSecurityIP[0]))
	{
		pCommonCfg->szSecurityIP[0][0] = '\0';
	}
	else
	{
		if (!ESR_CheckIPAddress(pCommonCfg->szSecurityIP[0],(pCommonCfg->iMediaType == MEDIA_TYPE_TCPIPV6) ? CHECK_IPV6_ADDR_AND_PORT1 : CHECK_IPV4))
		{
			LOG_ESR_COMMON_CFG_2("Security IP-address 1");
			return ERR_CFG_BADCONFIG;
		}
	}

	/* 16.security IP-address2 */
	iRet = Cfg_ProfileGetString(pCfg, 
								SECURITYI_IP_2, 
								pCommonCfg->szSecurityIP[1],
								IPADDRESS_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Security IP-address 2");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szSecurityIP[1]))
	{
		pCommonCfg->szSecurityIP[1][0] = '\0';
	}
	else
	{
		if (!ESR_CheckIPAddress(pCommonCfg->szSecurityIP[1],(pCommonCfg->iMediaType == MEDIA_TYPE_TCPIPV6) ? CHECK_IPV6_ADDR_AND_PORT1 : CHECK_IPV4))
		{
			LOG_ESR_COMMON_CFG_2("Security IP-address 2");
			return ERR_CFG_BADCONFIG;
		}
	}

	/* 17.Security Level */
	iRet = Cfg_ProfileGetInt(pCfg, SECURITY_LEVEL, &iBuf);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Security Level");
		return ERR_CFG_FAIL;
	}
	/* check value */
	if (iBuf < 1 || iBuf > 3)
	{
		LOG_ESR_COMMON_CFG_2("Security Level");
		return ERR_CFG_BADCONFIG;
	}
	pCommonCfg->iSecurityLevel = iBuf;

	//18 added by Jimmy 2013.12.13 ProductInform
	iRet = Cfg_ProfileGetString(pCfg, 
		PRODCT_INFOM, 
		pCommonCfg->szProdctInfom,
		PRODCT_INFOM_LEN);
	if (iRet != 1)
	{
		LOG_ESR_COMMON_CFG("Product Info");
		return ERR_CFG_FAIL;
	}
	if (IsEmptyField(pCommonCfg->szProdctInfom))
	{
		pCommonCfg->szProdctInfom[0] = '\0';
	}
	else
	{
		if (!ESR_CheckProdctInfom(pCommonCfg->szProdctInfom))
		{
			LOG_ESR_COMMON_CFG_2("Product Info");
			return ERR_CFG_BADCONFIG;
		}
	}

    
	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : LoadESRCommonConfig
 * PURPOSE  : to load ESR common config file
 * CALLS    : 
 * CALLED BY: ESR_InitConfig
 * ARGUMENTS:  
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-18 20:56
 *==========================================================================*/
static int LoadESRCommonConfig(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_ESRCOMMON, szCfgFileName, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadESRCommonConfigProc,
		&g_EsrGlobals.CommonConfig);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : ParseTypeMapTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Cfg_LoadSingleTable
 * ARGUMENTS: char           *szBuf       : 
 *            TYPE_MAP_INFO  *pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 09:28
 *==========================================================================*/
static int ParseTypeMapTableProc(char *szBuf, TYPE_MAP_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);
	const char *szSMIOType = SMIO_TYPE;
	const char *szSMBatType = SMBAT_TYPE;
	const char *szACUnitType = ACUNIT_TYPE;
	const char *szSMDUHType = SMDUH_TYPE;
	const char *szSMDUPType = SMDUP_TYPE;


	/* 1.jump Sequence id field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 2.jump Type Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 3.ACU StdEquipType ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	TRACE("pField[0] = %s\n", pField);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Types Map", "StdEquipType ID");
		return 3;
	}
	/* is SMIO type? */
	if (pField[0]  == szSMIOType[0] && pField[1]  == szSMIOType[1] && pField[2]  == 'x')
	{
		TRACE("SMIO\n");
		pStructData->iEquipType = atoi(szSMIOType);
		
	}
	///* is SM battery type? */
	else if (pField[0] == szSMBatType[0] &&
		pField[1] == szSMBatType[1]  && (pField[2] == szSMBatType[2]) )
	{
		TRACE("SMBAT_TYPE\n");
		pStructData->iEquipType = atoi(SMBAT_TYPE);
	}
	/* is AC Unit type? */
	else if (pField[0] == szACUnitType[0] && pField[1] == szACUnitType[1] && 
		(pField[2] == szACUnitType[2]))
	{
		TRACE("ACUNIT_TYPE\n");
		pStructData->iEquipType = atoi(ACUNIT_TYPE);
	}
	/* is SMDUH type? */
	else if (pField[0]  == szSMDUHType[0] && pField[1]  == szSMDUHType[1] && pField[2]  == 'x')
	{
		TRACE("SMDUH\n");
		pStructData->iEquipType = atoi(szSMDUHType);
	}
	/* is SMDUP type? */
	else if (pField[0]  == szSMDUPType[0] && pField[1]  == szSMDUPType[1] && pField[2]  == 'x')
	{
		TRACE("SMDUP\n");
		pStructData->iEquipType = atoi(szSMDUPType);
	}
	else
	{
		if (!CFG_CheckPNumber(pField))
		{
			LOG_READ_TABLE_DT("Model Types Map", "StdEquipType ID");
			return 3;    /* not a num, error */
		}

		pStructData->iEquipType = atoi(pField);
	}
	
	/* 4.EEM Group ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Types Map", "EEM Group ID");
		return 4;
	}
    if (!CFG_CheckHexNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Types Map", "EEM Group ID");
        return 4;    /* not a num, error */
    }
	sscanf(pField, "%x", &pStructData->iGroupID);

	/* 5.EEM Sub-Group ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Types Map", "EEM Sub-Group ID");
		return 5;
	}
    if (!CFG_CheckHexNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Types Map", "EEM Sub-Group ID");
        return 5;    /* not a num, error */
    }
	sscanf(pField, "%x", &pStructData->iSubGroupID);

	/* 4.Unit Level Flag */
	if (pStructData->iSubGroupID == 0)
	{
		pStructData->bUnitLevel = FALSE;
	}
	else
	{
		pStructData->bUnitLevel = TRUE;
	}


	return 0;
}

/* assistant function for judge enable bit of EEM DI table.
 * modified by Thomas for CR, 2005-2-28 */
BOOL ESR_IsEnableBit(IN const char *pField)
{
	if (*pField == 'E' && *(pField + 1) == 'N' &&
		*(pField + 2) == '\0')
	{
		return TRUE;
	}
	else 
	{
		return FALSE;
	}
}

BOOL ESR_IsDisableBit(IN const char *pField)
{
	if (*pField == 'D' && *(pField + 1) == 'N' &&
		*(pField + 2) == '\0')
	{
		return TRUE;
	}
	else 
	{
		return FALSE;
	}
}
/* Modified end, Thoams, 2006-2-28 */


/*==========================================================================*
 * FUNCTION : ParseMapEntriesTableProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char               *szBuf      : 
 *            MAPENTRIES_INFO *  pStructData : 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 11:39
 *==========================================================================*/
static int ParseMapEntriesTableProc(char *szBuf, MAPENTRIES_INFO *pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.EEM model signal type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Map Entries", "EEM Model Sig Type");
		return 1;
	}
	/* check and assign type value(valid values: AI AO DI DO) */
    switch (*pField)
	{
	case 'A':
		switch (*(pField + 1))
		{
		case 'I':
			pStructData->iEEMSigType = EEM_SIGTYPE_AI;
			break;

		case 'O':
			pStructData->iEEMSigType = EEM_SIGTYPE_AO;
			break;

		default:
			LOG_READ_TABLE_DR("Model Map Entries", "EEM Model Sig Type");
			return 1;
		}
		break;

	case 'D':
		switch (*(pField + 1))
		{
		case 'I':
			pStructData->iEEMSigType = EEM_SIGTYPE_DI;
			break;

		case 'O':
			pStructData->iEEMSigType = EEM_SIGTYPE_DO;
			break;

		default:
			LOG_READ_TABLE_DR("Model Map Entries", "EEM Model Sig Type");
			return 1;
		}
		break;

	default:
		LOG_READ_TABLE_DR("Model Map Entries", "EEM Model Sig Type");
		return 1;
	}

	/* 2.Port ID  */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Map Entries", "Port ID");
		return 2;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Map Entries", "Port ID");
		return 2;
	}
	pStructData->iEEMPort = atoi(pField);


	/* 3.ACU model signal type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))  // means no relevant sig type in ACU model
	{
		pStructData->iACUSigType = EEM_RESERVED;
		pStructData->iACUSigID = EEM_RESERVED;   /* just finish */
		return 0;
	}

	/* Added by Thomas for CR: using enable bit in DI table to distinguage the availability 
	 * of the front value, 2006-2-28. */
	else if (ESR_IsEnableBit(pField))
	{
		pStructData->iACUSigType = EEM_ENABLE_BIT_OF_DI;
		pStructData->iACUSigID = EEM_RESERVED;   /* just finish */
		return 0;
	}
	else if (ESR_IsDisableBit(pField))
	{
		pStructData->iACUSigType = EEM_DISABLE_BIT_OF_DI;
		pStructData->iACUSigID = EEM_RESERVED;   /* just finish */
		return 0;
	}
	/* modified end, Thomas, 2006-2-28 */

	else
	{
		if (*pField == '\0')
		{
			LOG_READ_TABLE_NO("Model Map Entries", "ACU model signal type");
			return 3;
		}

		/* check and assign(valid value: SP(sampling) ST(setting) C(control) A(alarm)) */
		switch (*pField)
		{
		case 'S':
			switch (*(pField + 1))
			{
			case 'P':   /* SP = sampling sig type */
				pStructData->iACUSigType = SIG_TYPE_SAMPLING;
				break;

			case 'T':   /* ST = setting sig type */
				pStructData->iACUSigType = SIG_TYPE_SETTING;
				break;					
						
			default:    /* S? : invalid */
				LOG_READ_TABLE_DR("Model Map Entries", "ACU model signal type");
				return 3;
			}
			break;

		case 'C':       /* C = control sig type */
			pStructData->iACUSigType = SIG_TYPE_CONTROL;
			break;

		case 'A':		/* A = alarm sig type */
			pStructData->iACUSigType = SIG_TYPE_ALARM;
			break;			
		default:
			LOG_READ_TABLE_DR("Model Map Entries", "ACU model signal type");
			return 3;
		}
	}

	/* 4.Signal ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		LOG_READ_TABLE_NO("Model Map Entries", "Signal ID");
		return 4;
	}
	if (*pField == '-' && *(pField + 1) == '\0')
	{
		pStructData->iACUSigID = -1;
		return 0;    /* succeed return */
	}

	if (!CFG_CheckPNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Map Entries", "Signal ID");
		return 4;
	}
	pStructData->iACUSigID = atoi(pField);

	/* 5.Equipment ID zzc modify*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		//LOG_READ_TABLE_NO("Model Map Entries", "Equip ID");
		return 0;
	}
	if (!CFG_CheckPNumber(pField))
	{
		LOG_READ_TABLE_DT("Model Map Entries", "Equip ID");
		return 5;
	}
	pStructData->iEquipmentID = atoi(pField);  // The additional item
	/* zzc modify end */

	return 0;
}

/*==========================================================================*
 * FUNCTION : GetMapEntriesSecName
 * PURPOSE  : assistant function to get Map Entries Section Name
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const char  *szBaseName       : 
 *            IN int         iMapIndex         : 
 *            OUT char       *szEntriesSecName : 
 *            IN int         iLen              : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 15:14
 *==========================================================================*/
static void GetMapEntriesSecName(IN const char *szBaseName, 
								 IN int iMapIndex,
								 OUT char *szEntriesSecName,
								 IN int iLen)
{
	char szFix[10], *p;
	int i;
    
	sprintf(szFix, "_MAP%d]%c", iMapIndex, '\0');
	strncpy(szEntriesSecName, szBaseName, (size_t)iLen);

	p = szEntriesSecName;
	for (i = iLen; *p != ']' && --i > 0; p++)
	{
		;
	}

	*p = '\0';

    i = strlen(szEntriesSecName);
	strncat(szEntriesSecName, szFix, (size_t)(iLen - i -1));

	return;
}

/*==========================================================================*
 * FUNCTION : LoadEEMModelMapConfigProc
 * PURPOSE  : callback funtion called by Cfg_LoadConfigFile
 * CALLS    : 
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: void         *pCfg       : 
 *            void         *pLoadToBuf : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 14:11
 *==========================================================================*/
static int LoadEEMModelMapConfigProc(void *pCfg, void *pLoadToBuf)
{
    EEMMODEL_CONFIG_INFO *pBuf;

	CONFIG_TABLE_LOADER loader[EEMMODEL_MAX_MAPS + 1];
	char szMapEntriesSecName[EEMMODEL_MAX_MAPS][30];
	int iMaps, i;
	TYPE_MAP_INFO *pMap;  /* used as buf */

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (EEMMODEL_CONFIG_INFO *)pLoadToBuf;


	/* load type map table first */
	DEF_LOADER_ITEM(&loader[0],
			TYPE_MAP_NUM, &(pBuf->iTypeMapNum), 
			TYPE_MAP_INFO_SEC, &(pBuf->pTypeMapInfo), 
			ParseTypeMapTableProc);

	if (Cfg_LoadTables(pCfg,1,loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	/* then load map entries */
	iMaps = pBuf->iTypeMapNum;
	for (i = 0; i < iMaps; i++)
	{
		GetMapEntriesSecName(MAPENTRIES_INFO_SEC,
							 i + 1, 
			                 szMapEntriesSecName[i],
			                 30);

		pMap = pBuf->pTypeMapInfo + i;
		DEF_LOADER_ITEM(&loader[i + 1],
			NULL, &(pMap->iMapEntriesNum), 
			szMapEntriesSecName[i], &(pMap->pMapEntriesInfo), 
			ParseMapEntriesTableProc);
	}

	if (Cfg_LoadTables(pCfg,iMaps,loader + 1) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;

}


/*==========================================================================*
 * FUNCTION : LoadEEMModelMapConfig
 * PURPOSE  : to load EEM Model Map config file
 * CALLS    : Cfg_LoadConfigFile
 * CALLED BY: ESR_InitConfig
 * ARGUMENTS: 
 * RETURN   : int : error code defined in the err_code.h , 0 for success
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 15:43
 *==========================================================================*/
static int LoadEEMModelMapConfig(void)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_EEMMODELMAP, szCfgFileName, MAX_FILE_PATH);
	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadEEMModelMapConfigProc,
		&g_EsrGlobals.EEMModelConfig);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

/* use SWAP	macro */
/* used to exchange member data of MAPENTRIES_INFO */
#define EXCHANGE_ENTRY(_entry1, _entry2, _buf)  \
   ((_buf).iEEMSigType = (_entry1).iEEMSigType, \
	(_buf).iEEMPort = (_entry1).iEEMPort, \
	(_buf).iACUSigType = (_entry1).iACUSigType, \
	(_buf).iACUSigID = (_entry1).iACUSigID, \
	(_entry1).iEEMSigType = (_entry2).iEEMSigType, \
	(_entry1).iEEMPort = (_entry2).iEEMPort, \
	(_entry1).iACUSigType = (_entry2).iACUSigType, \
	(_entry1).iACUSigID = (_entry2).iACUSigID, \
	(_entry2).iEEMSigType = (_buf).iEEMSigType, \
	(_entry2).iEEMPort = (_buf).iEEMPort, \
	(_entry2).iACUSigType = (_buf).iACUSigType, \
	(_entry2).iACUSigID = (_buf).iACUSigID)

/*==========================================================================*
 * FUNCTION : SortMapEntriesByType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SortMapEntries
 * ARGUMENTS: MAPENTRIES_INFO  *pEntries : 
 *            int              iNum      : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 18:58
 *==========================================================================*/
static void SortMapEntriesByType(MAPENTRIES_INFO *pEntries, int iNum)
{
	BOOL bDone;
	int i, j;

	i = 0;
	bDone = FALSE;
	while (i < iNum && (!bDone))
	{
		bDone = TRUE;
		for (j = 1; j < iNum - i; j++)
		{
			if (pEntries[j].iEEMSigType < pEntries[j - 1].iEEMSigType)
			{
				bDone = FALSE;
				//EXCHANGE_ENTRY(pEntries[j], pEntries[j - 1], tmp);
				SWAP(MAPENTRIES_INFO, pEntries[j], pEntries[j - 1]);
			}
		}

		i++;
	}
}

/*==========================================================================*
 * FUNCTION : SortMapEntriesForEachType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SortMapEntries
 * ARGUMENTS: MAPENTRIES_INFO  *pEntries : 
 *            int              iNum      : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 17:05
 *==========================================================================*/
static void SortMapEntriesForEachType(MAPENTRIES_INFO *pEntries, int iNum)
{
	BOOL bDone;
	int i, j;

	i = 0;
	bDone = FALSE;
	while (i < iNum && (!bDone))
	{
		bDone = TRUE;
		for (j = 1; j < iNum - i; j++)
		{
			if (pEntries[j].iEEMPort < pEntries[j - 1].iEEMPort)
			{
				bDone = FALSE;
				//EXCHANGE_ENTRY(pEntries[j], pEntries[j - 1], tmp);
				SWAP(MAPENTRIES_INFO, pEntries[j], pEntries[j - 1]);
			}
		}

		i++;
	}
}


/*==========================================================================*
 * FUNCTION : SortMapEntries
 * PURPOSE  : 
 * CALLS    : SortMapEntriesByType
 *			  SortMapEntriesForEachType
 * CALLED BY: ESR_InitConfig
 * ARGUMENTS: TYPE_MAP_INFO  *pMapInfo : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 18:59
 *==========================================================================*/
static void SortMapEntries(TYPE_MAP_INFO  *pMapInfo)
{
	int i, iMapEntriesNum;
	MAPENTRIES_INFO  *pMapEntries;
	BLOCK_MAPENTRIES *pBlockEntries;

	iMapEntriesNum = pMapInfo->iMapEntriesNum;
	pMapEntries = pMapInfo->pMapEntriesInfo;
	pBlockEntries = &pMapInfo->BlockMapEntries;

	/* sort by type(AI AO DI DO) */
	SortMapEntriesByType(pMapEntries, iMapEntriesNum);
	//NEW(int, 100);  // for test

	/* init and sort AIs */
	i = 0;
	pBlockEntries->iAINum = 0;
	pBlockEntries->pAIs = pMapEntries;
	while (i < iMapEntriesNum && pMapEntries->iEEMSigType == 0)  /* 0 = AI */
	{
		pBlockEntries->iAINum++;
		pMapEntries++;
		i++;
	}
	SortMapEntriesForEachType(pBlockEntries->pAIs, pBlockEntries->iAINum);

	if (i == iMapEntriesNum)
	{
		return;
	}

	/* init and sort AOs */
	pBlockEntries->iAONum = 0;
	pBlockEntries->pAOs = pMapEntries;
	while (i < iMapEntriesNum && pMapEntries->iEEMSigType == 1)  /* 1 = AO */
	{
		pBlockEntries->iAONum++;
		pMapEntries++;
		i++;
	}
	//SortMapEntriesForEachType(pBlockEntries->pAOs, pBlockEntries->iAONum);

	if (i == iMapEntriesNum)
	{
		return;
	}

	/* init and sort DIs */
	pBlockEntries->iDINum = 0;
	pBlockEntries->pDIs = pMapEntries;
	while (i < iMapEntriesNum && pMapEntries->iEEMSigType == 2)  /* 2 = DI */
	{
		pBlockEntries->iDINum++;
		pMapEntries++;
		i++;
	}
	//SortMapEntriesForEachType(pBlockEntries->pDIs, pBlockEntries->iDINum);

	if (i == iMapEntriesNum)
	{
		return;
	}

	/* init and sort DOs */
	pBlockEntries->iDONum = 0;
	pBlockEntries->pDOs = pMapEntries;
	
	if(pMapInfo->iEquipType == 801 && ESR_IsIB1Type())//IB1 only have 4 DO //IB1
	{
		while (i < iMapEntriesNum && pMapEntries->iEEMSigType == 3)  /* 3 = DO */
		{
			if(pBlockEntries->iDONum > 4)
			{
				pMapEntries->iACUSigType == EEM_RESERVED;

			}
			
			pBlockEntries->iDONum++;
			pMapEntries++;
			i++;
		}
		//SortMapEntriesForEachType(pBlockEntries->pDOs, pBlockEntries->iDONum);
	}
	else
	{
		while (i < iMapEntriesNum && pMapEntries->iEEMSigType == 3)  /* 3 = DO */
		{
			pBlockEntries->iDONum++;
			pMapEntries++;
			i++;
		}
		//SortMapEntriesForEachType(pBlockEntries->pDOs, pBlockEntries->iDONum);
	}

	return;
}


/*==========================================================================*
 * FUNCTION : GetTypeMap
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iStdEquipID : 
 * RETURN   : TYPE_MAP_INFO : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 19:39
 *==========================================================================*/
//static TYPE_MAP_INFO *GetTypeMap(int iEquipTypeID)
//{
//	int i, iTypeMapNum;
//	TYPE_MAP_INFO *pTypeMap;
//
//	pTypeMap = g_EsrGlobals.EEMModelConfig.pTypeMapInfo;
//	iTypeMapNum = g_EsrGlobals.EEMModelConfig.iTypeMapNum;
//
//	for (i = 0; i < iTypeMapNum; i++, pTypeMap++)
//	{
		///* special process for SMIO generic unit */
		//if (pTypeMap->iEquipType == atoi(SMIO_TYPE) &&
		//	iEquipTypeID/100 == pTypeMap->iEquipType/100)
		//{
		//	return pTypeMap;
		//}

		///* specical process for SM Battery unit */
		//if (pTypeMap->iEquipType == atoi(SMBAT_TYPE) &&
		//	iEquipTypeID/10 == pTypeMap->iEquipType/10)
		//{
		//	return pTypeMap;
		//}

		///* specical process for AC unit */
		//if (pTypeMap->iEquipType == atoi(ACUNIT_TYPE) &&
		//	iEquipTypeID > 700 && iEquipTypeID < 800)
		//{
		//	return pTypeMap;
		//}

		/* normal process */
//		if (pTypeMap->iEquipType == iEquipTypeID)
//		{
//			return pTypeMap;
//		}
//	}
//
//	return NULL;
//}

static TYPE_MAP_INFO *GetTypeMap(int iEquipTypeID, int *piStartIndex)
{
	int i, iTypeMapNum;
	TYPE_MAP_INFO *pTypeMap;

	//pTypeMap = g_EsrGlobals.EEMModelConfig.pTypeMapInfo;
	iTypeMapNum = g_EsrGlobals.EEMModelConfig.iTypeMapNum;

	//For multi-mapping between EEM-ACU(n-1) model, Thomas, 2007-5-4
	if (*piStartIndex >= iTypeMapNum)
	{
		return NULL;
	}
	pTypeMap = g_EsrGlobals.EEMModelConfig.pTypeMapInfo + (*piStartIndex);

	
	for (i = *piStartIndex; i < iTypeMapNum; i++, pTypeMap++)
	{
		/* special process for SMIO generic unit */
		if (pTypeMap->iEquipType == atoi(SMIO_TYPE) &&
			//iEquipTypeID/100 == pTypeMap->iEquipType/100)
			iEquipTypeID >= atoi(SMIO_TYPE) && iEquipTypeID < atoi(SMIO_TYPE)+MAX_SMIO_UNIT )
		{
			*piStartIndex = iTypeMapNum; //no multi-mapping for it, end it!
			//TRACE("iEquipTypeID = %d iTypeMapNum = %d %s\n",  iEquipTypeID, iTypeMapNum, (pTypeMap == NULL ? "NULL" : "Not NULL"));
			return pTypeMap;
		}

		///* specical process for SM Battery unit */
		if (pTypeMap->iEquipType == atoi(SMBAT_TYPE) &&
			iEquipTypeID == pTypeMap->iEquipType)
		{
			*piStartIndex = iTypeMapNum; //no multi-mapping for it, end it!
			TRACE("pTypeMap->iEquipType = %d, atoi(SMBAT_TYPE) = %d,iEquipTypeID = %d iTypeMapNum = %d %s\n", pTypeMap->iEquipType, atoi(SMBAT_TYPE), iEquipTypeID, iTypeMapNum, (pTypeMap == NULL ? "NULL" : "Not NULL"));
			return pTypeMap;
		}

		///* specical process for AC unit */
		if (pTypeMap->iEquipType == atoi(ACUNIT_TYPE) &&
			iEquipTypeID > 702 && iEquipTypeID < 799)
		{
			*piStartIndex = iTypeMapNum; //no multi-mapping for it, end it!
			//TRACE("iEquipTypeID = %d iTypeMapNum = %d %s\n",  iEquipTypeID, iTypeMapNum, (pTypeMap == NULL ? "NULL" : "Not NULL"));
			return pTypeMap;
		}
		
		/* special process for SMDUH generic unit */
		if (pTypeMap->iEquipType == atoi(SMDUH_TYPE) &&
			iEquipTypeID >= atoi(SMDUH_TYPE) && iEquipTypeID < atoi(SMDUH_TYPE)+MAX_SMDUH_UNIT )
		{
			*piStartIndex = iTypeMapNum; //no multi-mapping for it, end it!
			return pTypeMap;
		}
		
		/* special process for SMDUP generic unit */
		if (pTypeMap->iEquipType == atoi(SMDUP_TYPE) &&
			iEquipTypeID >= atoi(SMDUP_TYPE) && iEquipTypeID < atoi(SMDUP_TYPE)+MAX_SMDUP_UNIT )
		{
			*piStartIndex = iTypeMapNum; //no multi-mapping for it, end it!
			return pTypeMap;
		}

		if((pTypeMap->iEquipType == FUELMNG_TYPE)&&( iEquipTypeID == OBFUELMNG_TYPE))
		{
			*piStartIndex = i+1; //point to the next index
			return pTypeMap;
		}

		/* normal process */
		if (pTypeMap->iEquipType == iEquipTypeID)
		{
			*piStartIndex = i+1; //point to the next index
			//TRACE("pTypeMap->iEquipType = %d iEquipTypeID = %d *piStartIndex = %d %s\n", pTypeMap->iEquipType, iEquipTypeID, *piStartIndex, (pTypeMap == NULL ? "NULL" : "Not NULL"));
			return pTypeMap;
		}
	}
	//TRACE("iEquipTypeID = %d\n",  iEquipTypeID);
	return NULL;
}
static TYPE_MAP_INFO *GetTypeMapX()
{
	int i, iTypeMapNum;
	TYPE_MAP_INFO *pTypeMap;

	//pTypeMap = g_EsrGlobals.EEMModelConfig.pTypeMapInfo;
	iTypeMapNum = g_EsrGlobals.EEMModelConfig.iTypeMapNum;

	pTypeMap = g_EsrGlobals.EEMModelConfig.pTypeMapInfo;


	for (i = 0; i < iTypeMapNum; i++, pTypeMap++)
	{
		TRACE("[%d]pTypeMap->iGroupID = %d, pTypeMap->iUnitNum = %d, pTypeMap->iSubGroupID =%d pTypeMap->iEquipType = %d\n", i, pTypeMap->iGroupID, pTypeMap->iUnitNum, pTypeMap->iSubGroupID, pTypeMap->iEquipType);
		
	}

	return NULL;
}
/*==========================================================================*
 * FUNCTION : GetUnitNum
 * PURPOSE  : get created Unit numbers of a group at present
 * CALLS    : 
 * CALLED BY: CreateEEMBlocks
 * ARGUMENTS: int  iGroupID : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-20 10:21
 *==========================================================================*/
static int GetUnitNum(int iGroupID)
{
	int i, iTypeMapNum, iUnitNum;
	EEMMODEL_CONFIG_INFO *pModelConfig;
	TYPE_MAP_INFO *pCurTypeMap;

	pModelConfig = &g_EsrGlobals.EEMModelConfig;
	iTypeMapNum = pModelConfig->iTypeMapNum;
	pCurTypeMap = pModelConfig->pTypeMapInfo;
	for (i = 0, iUnitNum = 0; i < iTypeMapNum; i++, pCurTypeMap++)
	{
		if (pCurTypeMap->iGroupID == iGroupID && 
					pCurTypeMap->iUnitNum > iUnitNum)
		{
			iUnitNum = pCurTypeMap->iUnitNum;		 
		}
	}

	return iUnitNum;
}

/*==========================================================================*
 * FUNCTION : SetUnitNum
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CreateEEMBlocks
 * ARGUMENTS: int  iGroupID : 
 *            int  iUnitNum : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-20 10:31
 *==========================================================================*/
static void SetUnitNum(int iGroupID, int iUnitNum)
{
	int i, iTypeMapNum;
	EEMMODEL_CONFIG_INFO *pModelConfig;
	TYPE_MAP_INFO *pCurTypeMap;

	pModelConfig = &g_EsrGlobals.EEMModelConfig;
	iTypeMapNum = pModelConfig->iTypeMapNum;
	pCurTypeMap = pModelConfig->pTypeMapInfo;
	for (i = 0; i < iTypeMapNum; i++, pCurTypeMap++)
	{
		if (pCurTypeMap->iGroupID == iGroupID)
		{
			pCurTypeMap->iUnitNum = iUnitNum;		 
		}
	}

	return;
}

/* Added by Thomas begin, support product info. 2006-2-9 */
#ifdef PRODUCT_INFO_SUPPORT
/*==========================================================================*
 * FUNCTION : AttachDevice
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Lintao                   DATE: 2006-2-9 14:49
 *==========================================================================*/
static BOOL AttachDevice(void)
{
	int i, j, iEquipID, iBlockNum, iDeviceNum, iIndex;
	EEM_BLOCK_INFO *pBlock;

	pBlock = g_EsrGlobals.EEMModelInfo.pEEMBlockInfo;
	iBlockNum = g_EsrGlobals.EEMModelInfo.iBlocksNum;

	iDeviceNum = DXI_GetDeviceNum();
	if (iDeviceNum > EEM_MAX_DEVICE_NUM)
	{
		AppLogOut("Creat EEM Model", APP_LOG_ERROR,
			"EEM_MAX_DEVICE_NUM is not big enough");
		return FALSE;
	}

	for (i = 0; i < iBlockNum; i++, pBlock++)
	{
		iEquipID = pBlock->iEquipID;

		pBlock->iRelatedDeviceNum = 0;

		//Device ID started from 1
		for (j = 1; j <= iDeviceNum; j++)
		{
			if (DXI_IsDeviceRelativeEquip(j, iEquipID))
			{
				pBlock->iRelatedDeviceNum++;
				iIndex = pBlock->iRelatedDeviceNum - 1;

				if (iIndex > EEM_MAX_DEVICE_NUM)
				{
					AppLogOut("Creat EEM Model", APP_LOG_ERROR,
						"EEM_MAX_DEVICE_NUM is not big enough");
					return FALSE;
				}

				pBlock->piRelatedDeviceIndex[iIndex] = j;

			}
		}

	}

	return TRUE; 
}
#endif //PRODUCT_INFO_SUPPORT
/* Added by Thomas end, support product info. 2006-2-9 */

#ifdef AUTO_CONFIG
static int ClearUnitNum()
{
	int i, iTypeMapNum;
	EEMMODEL_CONFIG_INFO *pModelConfig;
	TYPE_MAP_INFO *pCurTypeMap;

	pModelConfig = &g_EsrGlobals.EEMModelConfig;
	iTypeMapNum = pModelConfig->iTypeMapNum;
	pCurTypeMap = pModelConfig->pTypeMapInfo;
	for (i = 0; i < iTypeMapNum; i++, pCurTypeMap++)
	{
		ASSERT(pCurTypeMap);
		pCurTypeMap->iUnitNum = 0;		 
	}

	return;
}
#endif //AUTO_CONFIG


static int blockcomp(EEM_BLOCK_INFO *block1, EEM_BLOCK_INFO *block2)
{

	if(block1->iGroupID != block2->iGroupID )
		return block1->iGroupID - block2->iGroupID;
	else if(block1->iSubGroupID != block2->iSubGroupID )
		return block1->iSubGroupID - block2->iSubGroupID;
	else if(block1->iEquipTypeID != block2->iEquipTypeID )
		return block1->iEquipTypeID - block2->iEquipTypeID;
	else
		return block1->iEquipID - block2->iEquipID;
}
static int Battblockcomp(Batt_Block_Info_Comp *block1, Batt_Block_Info_Comp *block2)
{
		return block1->Num -block2->Num;
}
static void EIBBattblockNum(int iEIBbattNum,Batt_Block_Info_Comp *pEIBBattBlockTemp)
{
	int EIBIDlist[EIB_BATT_NUM]={118,119,206,//EIB1Batt1-3
		           120,121,207,//EIB2Batt1-3
			   122,123,208,//EIB3Batt1-3
			   124,125,209};//EIB4Batt1-3
	int i,j;
	for(i=0;i<iEIBbattNum;i++)
	{	
		for(j=0;j<EIB_BATT_NUM;j++)
		{
			if(pEIBBattBlockTemp->block.iEquipID == EIBIDlist[j])
				pEIBBattBlockTemp->Num = j;
		}
		pEIBBattBlockTemp++;
	}
	
}
static void SMDUBattblockNum(int iSMDUbattNum,Batt_Block_Info_Comp *pSMDUBattBlockTemp)
{
	int SMDUIDlist[SMDU_BATT_NUM]={126,127,128,129,659,//SMDU1Batt1-5
		             130,131,132,133,660,//SMDU2Batt1-5
			     134,135,136,137,661,//SMDU3Batt1-5
			     138,139,140,141,662,//SMDU4Batt1-5
			     142,143,144,145,663,//SMDU5Batt1-5
			     146,147,148,149,664,//SMDU6Batt1-5
			     150,151,152,153,665,//SMDU7Batt1-5
			     154,155,156,157,666};//SMDU8Batt1-5
	int i,j;
	for(i=0;i<iSMDUbattNum;i++)
	{	
		for(j=0;j<SMDU_BATT_NUM;j++)
		{
			if(pSMDUBattBlockTemp->block.iEquipID == SMDUIDlist[j])
				pSMDUBattBlockTemp->Num = j;
		}
		pSMDUBattBlockTemp++;
	}

}
/*==========================================================================*
 * FUNCTION : CreateEEMBlocks
 * PURPOSE  : 
 * CALLS    : GetUnitNum
 *			  SetUnitNum
 * CALLED BY: ESR_InitConfig
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 19:45
 *==========================================================================*/
int CreateEEMBlocks(int iWaitCount)
{
	EQUIP_INFO* pEquipInfo, *pCurEquip;
	int iEquipNum, iBlockNum, iUnitNum;
	int iInterfaceType, iBufLen, iError;
	int i =0;
	int iEquipTypeID;

	EEM_BLOCK_INFO *pBlocks;
	TYPE_MAP_INFO *pTypeMap;


	int iEIBbattNum;
	int iSMDUbattNum;
	Batt_Block_Info_Comp EIBBattBlockTemp[EIB_BATT_NUM];
	Batt_Block_Info_Comp SMDUBattBlockTemp[SMDU_BATT_NUM];
	iEIBbattNum =0,iSMDUbattNum =0;

	/* 0.Wait change of bWorkStatus is OK when dev is add*/
	for(i = 0;  i < iWaitCount; i++)
	{
		Sleep(1000);
		RunThread_Heartbeat(NULL);
	}

	/* 1.get equip list */
	iInterfaceType = VAR_ACU_EQUIPS_LIST;

	iError = DxiGetData(iInterfaceType, 0, 0, &iBufLen, &pEquipInfo, 0);
	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Create EEM Blocks",APP_LOG_ERROR, 
			"Get Equip List through DXI failed.\n");
		TRACE("[%s]--%s: ERROR: Get Equip List failed.\n",
			__FILE__, __FUNCTION__);
		return ERR_CFG_FAIL;
	}
	iEquipNum = iBufLen/sizeof(EQUIP_INFO);

	/* 2.create EEM blocks */
	pBlocks = NEW(EEM_BLOCK_INFO, iEquipNum);
	if (pBlocks == NULL)
	{
		AppLogOut("Create EEM Blocks", APP_LOG_ERROR, 
			"no memory to create blocks.\n");
		TRACE("[%s]--%s: ERROR: no memory to create blocks.\n",
			__FILE__, __FUNCTION__);
		return ERR_CFG_NO_MEMORY;
	}
	ZERO_POBJS(pBlocks, iEquipNum);
	g_EsrGlobals.EEMModelInfo.pEEMBlockInfo = pBlocks;

#ifdef AUTO_CONFIG
	ClearUnitNum();

#endif //AUTO_CONFIG

	//GetTypeMapX();
	/* 3.init data of each block */
	pCurEquip = pEquipInfo;
	for (i = 0, iBlockNum = 0; i < iEquipNum; i++, pCurEquip++, pBlocks++)
	{
		int iStartIndex = 0;   //For multi-mapping between EEM-ACU(n-1) model, Thomas, 2007-5-4
		iEquipTypeID = pCurEquip->iEquipTypeID;


		do
		{
			pTypeMap = GetTypeMap(iEquipTypeID, &iStartIndex);

			//modified by Thomas, 2007-7-10
 
#ifdef AUTO_CONFIG
			if (pTypeMap == NULL || (!pCurEquip->bWorkStatus))
#else
			if (pTypeMap == NULL)
#endif //AUTO_CONFIG
			{

#ifdef _SHOW_ESR_CONFIG_INFO
				//if (iStartIndex == 0)  //not found from the first search
				{
					//only TRACE, not log! Modified by Thomas, 2007-1-22
					/*AppLogOut("Create EEM Blocks", APP_LOG_WARNING,
					"ACU std equip type: %d not used "
					"in the EEM Model config\n", iEquipTypeID);*/
					TRACE("[%s]--%s: WARNING: ACU std equip type: %d not exist in the "
						"EEM Model config\n", __FILE__, __FUNCTION__, iEquipTypeID);
				}
#endif //_SHOW_ESR_CONFIG_INFO


				pBlocks--;
				break;			
			}
			
			iBlockNum++;
			pBlocks->iEquipID = pCurEquip->iEquipID;
			pBlocks->iEquipTypeID = pCurEquip->iEquipTypeID;
			pBlocks->iGroupID = pTypeMap->iGroupID;
			pBlocks->iSubGroupID = pTypeMap->iSubGroupID;

			TRACE(" pBlocks->iEquipID =%d, pBlocks->iGroupID =%d, pBlocks->iSubGroupID =%d\n", pBlocks->iEquipID, pBlocks->iGroupID, pBlocks->iSubGroupID);
			//if (pTypeMap->bUnitLevel)
			//{
			//	/*iUnitNum cleared to 0 in Cfg_LoadSingleTable interface */
			//	iUnitNum = GetUnitNum(pTypeMap->iGroupID);
			//	iUnitNum++;
			//	SetUnitNum(pTypeMap->iGroupID, iUnitNum);

			//	pBlocks->iUnitID = pTypeMap->iUnitNum;
			//}

			pBlocks->pTypeInfo = pTypeMap;

			if(pBlocks->iEquipTypeID ==303)
			{

				EIBBattBlockTemp[iEIBbattNum].block = *pBlocks;
				iEIBbattNum++;
			}
			if(pBlocks->iEquipTypeID ==302)
			{
				SMDUBattBlockTemp[iSMDUbattNum].block = *pBlocks;
				iSMDUbattNum++;
			}
			pBlocks++;
		}
		while (1);
	}
	RunThread_Heartbeat(NULL);
	/* ansign */
	g_EsrGlobals.EEMModelInfo.iBlocksNum = iBlockNum;

	qsort(g_EsrGlobals.EEMModelInfo.pEEMBlockInfo,
	      g_EsrGlobals.EEMModelInfo.iBlocksNum,
	      sizeof(EEM_BLOCK_INFO),
	      blockcomp);

	RunThread_Heartbeat(NULL);
	if(iEIBbattNum>1)
	{
		EIBBattblockNum(iEIBbattNum-1,EIBBattBlockTemp);
		qsort(EIBBattBlockTemp,
			iEIBbattNum-1,
			sizeof(Batt_Block_Info_Comp),
			Battblockcomp);
	}


	RunThread_Heartbeat(NULL);

	if(iSMDUbattNum>1)
	{
		SMDUBattblockNum(iSMDUbattNum-1,SMDUBattBlockTemp);
		qsort(SMDUBattBlockTemp,
			iSMDUbattNum-1,
			sizeof(Batt_Block_Info_Comp),
			Battblockcomp);
	}


	RunThread_Heartbeat(NULL);

	EEM_BLOCK_INFO *pBlockstemp;
	pBlockstemp = g_EsrGlobals.EEMModelInfo.pEEMBlockInfo;
	for(i=0,iEIBbattNum=0,iSMDUbattNum=0 ;i<iBlockNum;i++,pBlockstemp++)
	{
		if(pBlockstemp->iEquipTypeID ==303)
		{
			*pBlockstemp = EIBBattBlockTemp[iEIBbattNum].block;
			iEIBbattNum++;
		}
		if(pBlockstemp->iEquipTypeID ==302)
		{
			*pBlockstemp = SMDUBattBlockTemp[iSMDUbattNum].block;
			iSMDUbattNum++;
		}
	}
	RunThread_Heartbeat(NULL);

	pBlockstemp = g_EsrGlobals.EEMModelInfo.pEEMBlockInfo;
	for(i=0;i<iBlockNum;i++,pBlockstemp++)
	{
		if (pBlockstemp->iSubGroupID!=0)
		{
			iUnitNum = GetUnitNum(pBlockstemp->iGroupID);
			iUnitNum++;
			SetUnitNum(pBlockstemp->iGroupID, iUnitNum);
			pBlockstemp->iUnitID = iUnitNum;
		}
	}

	/* Added for DL* and DP* commands Thomas, 2006-2-9 */
#ifdef PRODUCT_INFO_SUPPORT
	if (!AttachDevice())
	{
		return ERR_CFG_FAIL;
	}
#endif //PRODUCT_INFO_SUPPORT


#ifdef _SHOW_ESR_CONFIG_INFO
	printf("block created.\n");
	printf("-----------------");
	printf("Block info----------\n");
	pBlocks = g_EsrGlobals.EEMModelInfo.pEEMBlockInfo;
	for (i = 0; i < iBlockNum; i++, pBlocks++)
	{
		printf("Block #%d: \n", i + 1);
		printf("Group ID: %d\t", pBlocks->iGroupID);
		printf("SubGroup ID: %d\t\t", pBlocks->iSubGroupID);
		printf("Unit ID: %d\t", pBlocks->iUnitID);
		printf("Equip ID: %d\n", pBlocks->iEquipID);

		//Show relevant device info, added by Thomas, 2006-2-17
		printf("Relevant Device Number: %d\nDevice ID List:",
			   pBlocks->iRelatedDeviceNum);
		{
			int j;
			for (j = 0; j < pBlocks->iRelatedDeviceNum; j++)
			{
				printf("%d\t", pBlocks->piRelatedDeviceIndex[j]);
			}
		}
		printf("\n");
	}
	printf("-----------------");
	printf("End of Block info----------\n");
#endif //_SHOW_ESR_CONFIG_INFO


	return ERR_CFG_OK;
}



/*==========================================================================*
 * FUNCTION : *GetSMIOStdEquip
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CreateRefTexts
 * ARGUMENTS: int                 iNo          : 
 *            int                 iStdEquipNum : 
 *            STDEQUIP_TYPE_INFO  *pStdEquip   : 
 * RETURN   : STDEQUIP_TYPE_INFO : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 12:09
 *==========================================================================*/
__INLINE static STDEQUIP_TYPE_INFO *GetSMIOStdEquip(int iNo, 
				int iStdEquipNum, STDEQUIP_TYPE_INFO *pStdEquip)
{
	int i;
	int iTypeID;

	iTypeID = atoi(SMIO_TYPE) + iNo -1;

	for (i = 0; i < iStdEquipNum; i++, pStdEquip++)
	{
		if (pStdEquip->iTypeID == iTypeID)
		{
			return pStdEquip;
		}
	}

	return NULL;
}



/*==========================================================================*
 * FUNCTION : *GetAlarmLevelRef
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CreateRefTexts
 * ARGUMENTS: BOOL            bHigh          : 
 *            int             iAlarmSigNum   : 
 *            ALARM_SIG_INFO  *pAlarmSigInfo : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 12:08
 *==========================================================================*/
__INLINE static int *GetAlarmLevelRef(BOOL bHigh,
									  int iAlarmSigNum,
									  ALARM_SIG_INFO *pAlarmSigInfo)
{
	int i;
	int iSigID;

	for (i = 0; i < iAlarmSigNum; i++, pAlarmSigInfo++)
	{
		iSigID = pAlarmSigInfo->iSigID;
		if (bHigh && iSigID % 2 == 1 && iSigID <= 11)
		{
			return &pAlarmSigInfo->iAlarmLevel;
		}

		if (!bHigh && iSigID % 2 == 0 && iSigID <= 12)
		{
			return &pAlarmSigInfo->iAlarmLevel;
		}
	}

	return NULL;
}


/*==========================================================================*
 * FUNCTION : CreateRefTexts
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ESR_InitConfig
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 12:08
 *==========================================================================*/
BOOL CreateRefTexts(void)
{
	int i, j, iError, iBufLen, iStdEquipNum;
	ESR_REF_TEXT *pRefTexts;
	int *piAlarmLevel;
	STDEQUIP_TYPE_INFO *pStdEquip, *pSMIOStdEquip;
	SAMPLE_SIG_INFO *pSampleSigInfo;

	/* clear to zero first */
	g_EsrGlobals.iRefTexts = 0;

	/* 1.call DXI interface to get stdEquip info */
	iError =  DxiGetData(VAR_STD_EQUIPS_LIST,
						0,			
						0,		
						&iBufLen,			
						(void *)&pStdEquip,			
						0);

	if (iError != ERR_DXI_OK)
	{
		AppLogOut("Create Reference Texts for SMIO", APP_LOG_ERROR,
			"call DXI to get StdEquipType info failed\n");
		TRACE("[%s]--%s: ERROR: call DXI to get StdEquipType info "
			"failed\n", __FILE__, __FUNCTION__);

		return FALSE;
	}

	iStdEquipNum = iBufLen/sizeof(STDEQUIP_TYPE_INFO);

	/* 2.create RefTexts */
	pRefTexts = g_EsrGlobals.RefTexts;
	for (i = 1; i <= MAX_SMIO_UNIT; i++)
	{
		pSMIOStdEquip = GetSMIOStdEquip(i, iStdEquipNum, pStdEquip);

		if (pSMIOStdEquip != NULL)
		{
			/* get Ref1-Ref16(note: some may be jumpped) */
			pSampleSigInfo = pSMIOStdEquip->pSampleSigInfo;
			for (j = 0; j < pSMIOStdEquip->iSampleSigNum; j++)
			{
				if (pSampleSigInfo->iSigID >= 1 && pSampleSigInfo->iSigID <= 16)
				{
					pRefTexts->iModuleNo = i;
					pRefTexts->szText1 = pSampleSigInfo->pSigName->pFullName[0];
					pRefTexts->szText2 = pSampleSigInfo->szSigUnit;

					/* do the mapping */
					/* Note: SigID 1 to 16 is fixed to map to Ref 1 to 16 */
					pRefTexts->iRefNo = pSampleSigInfo->iSigID;

					g_EsrGlobals.iRefTexts++;
					pSampleSigInfo++;
					pRefTexts++;
				}
			}

			/* get Ref17: high alarm level */
			piAlarmLevel = GetAlarmLevelRef(TRUE, pSMIOStdEquip->iAlarmSigNum, 
				pSMIOStdEquip->pAlarmSigInfo);
			if (piAlarmLevel != NULL)
			{
				pRefTexts->iModuleNo = i;
				pRefTexts->iRefNo = 17;
				pRefTexts->piAlarmLevel = piAlarmLevel;
				pRefTexts++;
				g_EsrGlobals.iRefTexts++;
			}

			/* get Ref18: low alarm level */
			piAlarmLevel = GetAlarmLevelRef(FALSE, pSMIOStdEquip->iAlarmSigNum, 
				pSMIOStdEquip->pAlarmSigInfo);
			if (piAlarmLevel != NULL)
			{
				pRefTexts->iModuleNo = i;
				pRefTexts->iRefNo = 18;
				pRefTexts->piAlarmLevel = piAlarmLevel;
				pRefTexts++;
				g_EsrGlobals.iRefTexts++;
			}
		}
	}


#ifdef _SHOW_ESR_CONFIG_INFO
	TRACE("Show Ref Texts Info:\n");
	TRACE("Total Ref Texts: %d\n", g_EsrGlobals.iRefTexts);
	for (i = 0; i < g_EsrGlobals.iRefTexts; i++)
	{
		TRACE("Ref Text %d: Module No. is %d,    "
			"Ref No. is %d\n", i+1, g_EsrGlobals.RefTexts[i].iModuleNo,
			g_EsrGlobals.RefTexts[i].iRefNo);
	}
#endif //_SHOW_ESR_CONFIG_INFO


	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ESR_InitConfig
 * PURPOSE  : 
 * CALLS    : LoadESRCommonConfig  
 *			  LoadEEMModelMapConfig
 *			  SortMapEntries
 *			  CreateEEMBlocks
 * CALLED BY: InitEsrGlobals (service_provider.c)
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-19 20:40
 *==========================================================================*/
int ESR_InitConfig()
{
	int i, iMapNum;
	
	TYPE_MAP_INFO *pTypeInfo;  /* for sorting mapentries */

	/* read ESR Common Config */
	if (LoadESRCommonConfig() != ERR_CFG_OK )
	{
		return ERR_CFG_FAIL;
	}

	/* read EEM Model Config */
	if (LoadEEMModelMapConfig() != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	//print model config info

#ifdef _SHOW_ESR_CONFIG_INFO
	{
		MAPENTRIES_INFO  *pMapEntries;
		int j;
		iMapNum = g_EsrGlobals.EEMModelConfig.iTypeMapNum;
		pTypeInfo = g_EsrGlobals.EEMModelConfig.pTypeMapInfo;

		printf("Before sorting...\n");

		for (i = 0; i < iMapNum; i++, pTypeInfo++)
		{
			
			printf("Now is Map %d(before sorting)\n", i + 1);			
			pMapEntries = pTypeInfo->pMapEntriesInfo;
			printf("the number of entries: %d\n", pTypeInfo->iMapEntriesNum);
			for (j = 0; j < pTypeInfo->iMapEntriesNum; j++, pMapEntries++)
			{
				printf("EEM Type: %d\t", pMapEntries->iEEMSigType);
				printf("EEM Port: %d\t", pMapEntries->iEEMPort);
				printf("ACU Type: %d\t", pMapEntries->iACUSigType);
				printf("ACU Port: %d\n", pMapEntries->iACUSigID);
			}
			
		}
	}

	TRACE_ESR_TIPS("Sort Map-entries of EEM Model Map");
#endif //_SHOW_ESR_CONFIG_INFO

	/* arrange EEM Model Config */
	iMapNum = g_EsrGlobals.EEMModelConfig.iTypeMapNum;
	pTypeInfo = g_EsrGlobals.EEMModelConfig.pTypeMapInfo;
	for (i = 0; i < iMapNum; i++, pTypeInfo++)
	{
		SortMapEntries(pTypeInfo);
	}

#ifdef _SHOW_ESR_CONFIG_INFO
	{
		BLOCK_MAPENTRIES *pBlockMapEntries;
		MAPENTRIES_INFO  *pMapEntries;
		int j;
		iMapNum = g_EsrGlobals.EEMModelConfig.iTypeMapNum;
		pTypeInfo = g_EsrGlobals.EEMModelConfig.pTypeMapInfo;

		printf("After sorting...\n");

		for (i = 0; i < iMapNum; i++, pTypeInfo++)
		{
			pBlockMapEntries = &pTypeInfo->BlockMapEntries;
			printf("Now is Map %d(after sorting)\n", i + 1);

			//AIs
			pMapEntries = pBlockMapEntries->pAIs;
			printf("the number of AIs: %d\n", pBlockMapEntries->iAINum);
			for (j = 0; j < pBlockMapEntries->iAINum; j++, pMapEntries++)
			{
				printf("EEM Type: %d\t", pMapEntries->iEEMSigType);
				printf("EEM Port: %d\t", pMapEntries->iEEMPort);
				printf("ACU Type: %d\t", pMapEntries->iACUSigType);
				printf("ACU Port: %d\n", pMapEntries->iACUSigID);
			}
			//AOs
			pMapEntries = pBlockMapEntries->pAOs;
			printf("the number of AOs: %d\n", pBlockMapEntries->iAONum);
			for (j = 0; j < pBlockMapEntries->iAONum; j++, pMapEntries++)
			{
				printf("EEM Type: %d\t", pMapEntries->iEEMSigType);
				printf("EEM Port: %d\t", pMapEntries->iEEMPort);
				printf("ACU Type: %d\t", pMapEntries->iACUSigType);
				printf("ACU Port: %d\n", pMapEntries->iACUSigID);
			}
			//DIs
			pMapEntries = pBlockMapEntries->pDIs;
			printf("the number of DIs: %d\n", pBlockMapEntries->iDINum);
			for (j = 0; j < pBlockMapEntries->iDINum; j++, pMapEntries++)
			{
				printf("EEM Type: %d\t", pMapEntries->iEEMSigType);
				printf("EEM Port: %d\t", pMapEntries->iEEMPort);
				printf("ACU Type: %d\t", pMapEntries->iACUSigType);
				printf("ACU Port: %d\n", pMapEntries->iACUSigID);
			}
			//DOs
			pMapEntries = pBlockMapEntries->pDOs;
			printf("the number of DOs: %d\n", pBlockMapEntries->iDONum);
			for (j = 0; j < pBlockMapEntries->iDONum; j++, pMapEntries++)
			{
				printf("EEM Type: %d\t", pMapEntries->iEEMSigType);
				printf("EEM Port: %d\t", pMapEntries->iEEMPort);
				printf("ACU Type: %d\t", pMapEntries->iACUSigType);
				printf("ACU Port: %d\n", pMapEntries->iACUSigID);
			}
		}
	}
#endif //_SHOW_ESR_CONFIG_INFO
   
	/* init EEM Model */
//    if (CreateEEMBlocks(12) != ERR_CFG_OK)
	 if (CreateEEMBlocks(0) != ERR_CFG_OK)// 3?��??������2?D����a�̨���y
	{
		return ERR_CFG_FAIL;
	}

	ESR_EquipListChange(TRUE);

	/* init RefTexts */
	if (!CreateRefTexts())
	{
		return ERR_CFG_FAIL;
	}

 
	return ERR_CFG_OK;
}


/* function prototype for ESR common config file item modification */
typedef BOOL (*HANDLE_CFG_ITEM) (int nVarID, 
						COMMON_CONFIG *pUserCfg, 
						CONFIG_FILE_MODIFIER *pModififier,
						char *szContent);

/*==========================================================================*
 * FUNCTION : HandleProtocolType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:16
 *==========================================================================*/
static BOOL HandleProtocolType(int nVarID, 
						COMMON_CONFIG *pUserCfg, 
						CONFIG_FILE_MODIFIER *pModififier,
						char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_PROTOCOL_TYPE))
	{
		sprintf(szContent, "%d%c", pUserCfg->iProtocolType, '\0');

		DEF_MODIFIER_ITEM(pModififier, 
			PROTOCOL_TYPE, 
			0, 
			0, 
			szContent);
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleProtocolType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:16
 *==========================================================================*/
static BOOL HandleCCID(int nVarID, 
					   COMMON_CONFIG *pUserCfg, 
					   CONFIG_FILE_MODIFIER *pModififier,
					   char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_CCID))
	{
		sprintf(szContent, "%d%c", pUserCfg->byCCID, '\0');

		DEF_MODIFIER_ITEM(pModififier, 
			ESR_CCID, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleSOCID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:23
 *==========================================================================*/
static BOOL HandleSOCID(int nVarID, 
						COMMON_CONFIG *pUserCfg, 
						CONFIG_FILE_MODIFIER *pModififier,
						char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_SOCID))
	{
		sprintf(szContent, "%d%c", pUserCfg->iSOCID, '\0');
		
		DEF_MODIFIER_ITEM(pModififier, 
			ESR_SOCID, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleReportInUse
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:25
 *==========================================================================*/
static BOOL HandleReportInUse(int nVarID, 
							  COMMON_CONFIG *pUserCfg, 
							  CONFIG_FILE_MODIFIER *pModififier,
							  char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_REPORT_IN_USE))
	{
		pUserCfg->bReportInUse == TRUE ?  (void)strcpy(szContent, "1") :
				(void)strcpy(szContent, "0");

		DEF_MODIFIER_ITEM(pModififier, 
			REPORT_IN_USE, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleCallbackInUse
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:28
 *==========================================================================*/
static BOOL HandleCallbackInUse(int nVarID, 
								COMMON_CONFIG *pUserCfg, 
								CONFIG_FILE_MODIFIER *pModififier,
								char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_CALLBACK_IN_USE))
	{
		pUserCfg->bCallbackInUse == TRUE ?  (void)strcpy(szContent, "1") :
				(void)strcpy(szContent, "0");

		DEF_MODIFIER_ITEM(pModififier, 
			CALLBACK_IN_USE, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleMediaType
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:31
 *==========================================================================*/
static BOOL HandleMediaType(int nVarID, 
							COMMON_CONFIG *pUserCfg, 
							CONFIG_FILE_MODIFIER *pModififier,
							char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_MEDIA_TYPE))
	{
		sprintf(szContent, "%d%c", pUserCfg->iMediaType, '\0');
	
		DEF_MODIFIER_ITEM(pModififier, 
			OPERATION_MEDIA, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


static BOOL HandleCommPortParam(int nVarID, 
								COMMON_CONFIG *pUserCfg, 
								CONFIG_FILE_MODIFIER *pModififier,
								char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_MEDIA_PORT_PARAM) ||
		ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_MEDIA_TYPE))
	{
		strcpy(szContent, pUserCfg->szCommPortParam);
	
		DEF_MODIFIER_ITEM(pModififier, 
			MEDIA_PORT_PARAM, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : HandleAttempts
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:33
 *==========================================================================*/
static BOOL HandleAttempts(int nVarID, 
						   COMMON_CONFIG *pUserCfg, 
						   CONFIG_FILE_MODIFIER *pModififier,
						   char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_MAX_ATTEMPTS))
	{
		sprintf(szContent, "%d%c", pUserCfg->iMaxAttempts, '\0');

		DEF_MODIFIER_ITEM(pModififier, 
			MAX_REPORT_ATTAMPS, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleElapseTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:38
 *==========================================================================*/
static BOOL HandleElapseTime(int nVarID, 
							 COMMON_CONFIG *pUserCfg, 
							 CONFIG_FILE_MODIFIER *pModififier,
							 char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_ATTEMPT_ELAPSE))
	{
		/* note: ACU based on ms */
		sprintf(szContent, "%d%c", pUserCfg->iAttemptElapse/1000, '\0');

		DEF_MODIFIER_ITEM(pModififier, 
			ELAPS_TIME, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleRP_Number1
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:40
 *==========================================================================*/
static BOOL HandleRP_Number1(int nVarID, 
							 COMMON_CONFIG *pUserCfg, 
							 CONFIG_FILE_MODIFIER *pModififier,
							 char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_REPORT_NUMBER_1))
	{
		//for empty string type config item, fill '-'
		if (pUserCfg->szAlarmReportPhoneNumber[0][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szAlarmReportPhoneNumber[0]);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			REPORT_NUMBER_1, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleRP_Number2
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:42
 *==========================================================================*/
static BOOL HandleRP_Number2(int nVarID, 
							 COMMON_CONFIG *pUserCfg, 
							 CONFIG_FILE_MODIFIER *pModififier,
							 char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_REPORT_NUMBER_2))
	{
		if (pUserCfg->szAlarmReportPhoneNumber[1][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szAlarmReportPhoneNumber[1]);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			REPORT_NUMBER_2, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleCB_Number
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:43
 *==========================================================================*/
static BOOL HandleCB_Number(int nVarID, 
							COMMON_CONFIG *pUserCfg, 
							CONFIG_FILE_MODIFIER *pModififier,
							char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_CALLBACK_NUMBER))
	{
		if (pUserCfg->szCallbackPhoneNumber[0][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szCallbackPhoneNumber[0]);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			CALLBACK_NUMBER, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleRP_IP1
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:44
 *==========================================================================*/
static BOOL  HandleRP_IP1(int nVarID, 
						  COMMON_CONFIG *pUserCfg, 
						  CONFIG_FILE_MODIFIER *pModififier,
						  char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_IPADDR_1))
	{
		if (pUserCfg->szReportIP[0][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szReportIP[0]);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			REPORT_IP_1, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleRP_IP2
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:46
 *==========================================================================*/
static BOOL HandleRP_IP2(int nVarID, 
						 COMMON_CONFIG *pUserCfg, 
						 CONFIG_FILE_MODIFIER *pModififier,
						 char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_IPADDR_2))
	{
		if (pUserCfg->szReportIP[1][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szReportIP[1]);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			REPORT_IP_2, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleSR_IP1
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:47
 *==========================================================================*/
static BOOL HandleSR_IP1(int nVarID, 
						 COMMON_CONFIG *pUserCfg, 
						 CONFIG_FILE_MODIFIER *pModififier,
						 char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_SECURITY_IP_1))
	{
		if (pUserCfg->szSecurityIP[0][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szSecurityIP[0]);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			SECURITYI_IP_1, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleSR_IP2
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:48
 *==========================================================================*/
static BOOL HandleSR_IP2(int nVarID, 
						 COMMON_CONFIG *pUserCfg, 
						 CONFIG_FILE_MODIFIER *pModififier,
						 char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_SECURITY_IP_2))
	{
		if (pUserCfg->szSecurityIP[1][0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szSecurityIP[1]);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			SECURITYI_IP_2, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleSecurityLevel
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int                                     nVarID     : 
 *            COMMON_CONFIG                           *pUserCfg  : 
 *            CONFIG_FILE_MODIFIER *pModififier, char  *szContent : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 10:49
 *==========================================================================*/
static BOOL HandleSecurityLevel(int nVarID, 
								COMMON_CONFIG *pUserCfg, 
								CONFIG_FILE_MODIFIER *pModififier,
								char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_SECURITY_LEVEL))
	{
		sprintf(szContent, "%d%c", pUserCfg->iSecurityLevel, '\0');
		
		DEF_MODIFIER_ITEM(pModififier, 
			SECURITY_LEVEL, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}

//added by Jimmy 2013.12.13
static BOOL Handle_Prodct_Infom(int nVarID, 
				COMMON_CONFIG *pUserCfg, 
				CONFIG_FILE_MODIFIER *pModififier,
				char *szContent)
{
	if (ESR_CFG_ITEM_TEST(nVarID, ESR_CFG_PRODCT_INFOM))
	{
		//for empty string type config item, fill '-'
		if (pUserCfg->szProdctInfom[0] == '\0')
		{
			szContent[0] = '-';
			szContent[1] = '\0';
		}
		else
		{
			strcpy(szContent, pUserCfg->szProdctInfom);
		}

		DEF_MODIFIER_ITEM(pModififier, 
			PRODCT_INFOM, 
			0, 
			0, 
			szContent);

		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : ESR_GetModifiedFileBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int            nVarID       : 
 *            COMMON_CONFIG  *pUserCfg    : 
 *            char           **pszOutFile : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 14:03
 *==========================================================================*/
BOOL ESR_GetModifiedFileBuf(int nVarID, 
							COMMON_CONFIG *pUserCfg,
							char **pszOutFile)
{
	CONFIG_FILE_MODIFIER Modififier[CFG_ITEM_NUM];

	/* note: MAX_CFG_ITEM_LEN should be long enough! */
	char szCfgFileName[MAX_FILE_PATH]; 
	char szContent[CFG_ITEM_NUM][MAX_CFG_ITEM_LEN];
	int iRet, iIndex, iModifiers;
	BOOL bModified;

	const char *pLogText;  //for log

	HANDLE_CFG_ITEM fnHandlers[CFG_ITEM_NUM];


	/* init the handlers */
	fnHandlers[0] = HandleProtocolType;
	fnHandlers[1] = HandleCCID;
	fnHandlers[2] = HandleSOCID;
	fnHandlers[3] = HandleReportInUse;
	fnHandlers[4] = HandleCallbackInUse;

	fnHandlers[5] = HandleMediaType;
	fnHandlers[6] = HandleCommPortParam;

	fnHandlers[7] = HandleAttempts;
	fnHandlers[8] = HandleElapseTime;
	fnHandlers[9] = HandleRP_Number1;
	fnHandlers[10] = HandleRP_Number2;

	fnHandlers[11] = HandleCB_Number;
	fnHandlers[12] = HandleRP_IP1;
	fnHandlers[13] = HandleRP_IP2;
	fnHandlers[14] = HandleSR_IP1;
	fnHandlers[15] = HandleSR_IP2;
	fnHandlers[16] = HandleSecurityLevel;
	fnHandlers[17] = Handle_Prodct_Infom;
	
	/* fill Modififier one by one */
	iModifiers = 0;
	for (iIndex = 0; iIndex < CFG_ITEM_NUM; iIndex++)
	{
		bModified = fnHandlers[iIndex](nVarID, 
			pUserCfg,
			&Modififier[iModifiers],
			szContent[iModifiers]);

		if (bModified)
		{
			iModifiers++;
		}
	}

	/* get the file buf */
	Cfg_GetFullConfigPath(CONFIG_FILE_ESRCOMMON, szCfgFileName, MAX_FILE_PATH);
	iRet = Cfg_ModifyConfigFile(szCfgFileName,
						 iModifiers,
						 Modififier,
						 pszOutFile);


	/* to simplify log action */
#define TASK_NAME        "Modifify ESR common config file"
#define LOG_MODIFY_FILE(_pLogText)  \
	(AppLogOut(TASK_NAME, APP_LOG_ERROR, "%s\n", (_pLogText)),  \
	TRACE("[%s]--%s: ERROR: %s\n", __FILE__, __FUNCTION__, (_pLogText)))

	switch (iRet)
	{
	case ERR_CFG_OK:
		break;

	case ERR_CFG_FILE_OPEN:
		pLogText = "Open ESR Common Config file for modification failed.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_NO_MEMORY:
		pLogText = "No memory.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_FILE_READ:
		pLogText = "Read ESR Common Config file for modification failed.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	case ERR_CFG_PARAM:
		pLogText = "Something is wrong with CONFIG_FILE_MODIFIER param for "
			"modification.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;

	default:
		pLogText = "Unhandled error when create new ESR Common Config file"
			" in memory.";
		LOG_MODIFY_FILE(pLogText);
		return FALSE;
	}

	return TRUE;
}
		

/*==========================================================================*
 * FUNCTION : ESR_UpdateCommonConfigFile
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szContent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-29 14:20
 *==========================================================================*/
BOOL ESR_UpdateCommonConfigFile(const char *szContent)
{
	FILE *pFile;
	char szCfgFileName[MAX_FILE_PATH]; 
	size_t count;

	Cfg_GetFullConfigPath(CONFIG_FILE_ESRCOMMON, szCfgFileName, MAX_FILE_PATH);
	pFile = fopen(szCfgFileName, "w");
	if (pFile == NULL)
	{
		return FALSE;
	}

	count = strlen(szContent);
	if (fwrite(szContent, 1, count, pFile) != count)
	{
		fclose(pFile);
		return FALSE;
	}

	fclose(pFile);

	return TRUE;
}


/*==========================================================================*
* FUNCTION : ESR_EquipListChange
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  :                    DATE: 
*==========================================================================*/
BOOL ESR_EquipListChange(BOOL bIsFirstTime)
{
	int iRectifierNum,iMpptNum, nBufLen,iFuelNum,iOBFuelNum;
	int iMBBattNum,iEIB1BattNum, iEIB2BattNum,iEIB3BattNum, iEIB4BattNum;
	int iSMDUFlag,iSMBRCFlag;
	SIG_BASIC_VALUE  * pSigValue;
	int iS1RectifierNum,iS2RectifierNum,iS3RectifierNum;
	BOOL bResult = FALSE;

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iRectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iRectifierNum = 1000;  //has no limit
	}

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(MPPT_STD_EQUIP_ID-1),	
		SIG_ID_ACTUAL_MPPT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iMpptNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iMpptNum = 1000;  //has no limit
	}

	//S1
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S1_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS1RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS1RectifierNum = 1000;  //has no limit
	}
	//S2
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S2_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS2RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS2RectifierNum = 1000;  //has no limit
	}
	//S3
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S3_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS3RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS3RectifierNum = 1000;  //has no limit
	}

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_FUEL_MANAGEMENT_GROUP_EQUIP),	
		SIG_ID_FUELMANG_FUEL_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{

		iFuelNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iFuelNum = 1000;  //has no limit
	}

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_FUEL_MANAGEMENT_GROUP_EQUIP),	
		SIG_ID_FUELMANG_OB_FUEL_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iOBFuelNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iOBFuelNum = 1000;  //has no limit
	}

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(BAT_GROUP_STD_EQUIP_ID),	
		SIG_ID_MB_BATT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iMBBattNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iMBBattNum = 1000;  //has no limit
	}

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		EIB1_STD_EQUIP_ID,	
		SIG_ID_EIB_BATT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iEIB1BattNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iEIB1BattNum = 1000;  //has no limit
	}
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		EIB2_STD_EQUIP_ID,	
		SIG_ID_EIB_BATT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iEIB2BattNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iEIB2BattNum = 1000;  //has no limit
	}
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		EIB3_STD_EQUIP_ID,	
		SIG_ID_EIB_BATT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iEIB3BattNum= pSigValue->varValue.ulValue;
	}
	else
	{
		iEIB3BattNum = 1000;  //has no limit
	}
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		EIB4_STD_EQUIP_ID,	
		SIG_ID_EIB_BATT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iEIB4BattNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iEIB4BattNum = 1000;  //has no limit
	}

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(SMDU_GROUP_STD_EQUIP_ID),	
		SIG_ID_SMDU_BATT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iSMDUFlag = pSigValue->varValue.ulValue;
	}
	else
	{
		iSMDUFlag = 1000;  //has no limit
	}

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(SMBRC_GROUP_STD_EQUIP_ID),	
		SIG_ID_SMBRC_BATT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iSMBRCFlag = pSigValue->varValue.ulValue;
	}
	else
	{
		iSMBRCFlag = 1000;  //has no limit
	}


	if(bIsFirstTime==FALSE)
	{
		if((g_EsrGlobals.EquipNumRecord.iSelfRectNum !=iRectifierNum)
	          ||(g_EsrGlobals.EquipNumRecord.iSelfMpptNum !=iMpptNum)
		   ||(g_EsrGlobals.EquipNumRecord.iSlave1RectNum !=iS1RectifierNum)
		   ||(g_EsrGlobals.EquipNumRecord.iSlave2RectNum !=iS2RectifierNum)
		   ||(g_EsrGlobals.EquipNumRecord.iSlave3RectNum !=iS3RectifierNum)
		   ||(g_EsrGlobals.EquipNumRecord.iFuelBankNum!=iFuelNum)
		   ||(g_EsrGlobals.EquipNumRecord.iOBFuelBankNum !=iOBFuelNum)
		   ||(g_EsrGlobals.EquipNumRecord.iMBBattNum!=iMBBattNum)
		   ||(g_EsrGlobals.EquipNumRecord.iEIB1BattNum!= iEIB1BattNum)
		   ||(g_EsrGlobals.EquipNumRecord.iEIB2BattNum!= iEIB2BattNum)
		   ||(g_EsrGlobals.EquipNumRecord.iEIB3BattNum!= iEIB3BattNum)
		   ||(g_EsrGlobals.EquipNumRecord.iEIB4BattNum!= iEIB4BattNum)
		   ||(g_EsrGlobals.EquipNumRecord.iSMDUFlag!=iSMDUFlag)
		   ||(g_EsrGlobals.EquipNumRecord.iSMBRCFlag!=iSMBRCFlag))
		{
			bResult = TRUE;
		}
		else
		{
			bResult = FALSE;
		}
	}

	g_EsrGlobals.EquipNumRecord.iSelfRectNum =iRectifierNum;
	g_EsrGlobals.EquipNumRecord.iSelfMpptNum =iMpptNum;
	g_EsrGlobals.EquipNumRecord.iSlave1RectNum =iS1RectifierNum;
	g_EsrGlobals.EquipNumRecord.iSlave2RectNum =iS2RectifierNum;
	g_EsrGlobals.EquipNumRecord.iSlave3RectNum =iS3RectifierNum;
	g_EsrGlobals.EquipNumRecord.iFuelBankNum=iFuelNum;
	g_EsrGlobals.EquipNumRecord.iOBFuelBankNum=iOBFuelNum;
	g_EsrGlobals.EquipNumRecord.iMBBattNum=iMBBattNum;
	g_EsrGlobals.EquipNumRecord.iEIB1BattNum= iEIB1BattNum;
	g_EsrGlobals.EquipNumRecord.iEIB2BattNum= iEIB2BattNum;
	g_EsrGlobals.EquipNumRecord.iEIB3BattNum= iEIB3BattNum;
	g_EsrGlobals.EquipNumRecord.iEIB4BattNum= iEIB4BattNum;
	g_EsrGlobals.EquipNumRecord.iSMDUFlag=iSMDUFlag;
	g_EsrGlobals.EquipNumRecord.iSMBRCFlag=iSMBRCFlag;

	
	return bResult;
}


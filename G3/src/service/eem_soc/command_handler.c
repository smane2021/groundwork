/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : command_handler.c
*  CREATOR  : LinTao                   DATE: 2004-10-26 18:54
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "esr.h"
#include <signal.h>

/* moved to esr.h file, Thomas, 2006-2-28 */
//#define EEM_RESERVED_VALUE		1

/* buff size for one type sig.  AI(0-63) AO(0-63) DI(0-255) DO(0-255) */
#define ESR_SIGTYPE_SIZE   1024

/* EEM time format */
#define EEM_TIME_FORMAT    "%y%m%d%H%M%S"


/* float value format definition */
struct tagIEEEFloat
{
	unsigned a:1;
	unsigned b:8;
	unsigned c:22;
	unsigned d:1;	 
};
typedef struct tagIEEEFloat IEEE_FLOAT;

struct tagEEMFloat
{
	unsigned a:1;
	unsigned b:1;
	unsigned c:22;
	unsigned d:8;	
};
typedef struct tagEEMFloat EEM_FLOAT;


union IEEE_FVALUE
{
	float fValue;
	unsigned long iValue;
	unsigned char   cValue[4];
	IEEE_FLOAT fIEEE;
};

union EEM_FVALUE
{
	float fValue;
	unsigned long iValue;
	unsigned char   cValue[4];
	EEM_FLOAT fEEM;
};

union ARM_INT
{
	unsigned int	  iValue;
	char	  cValue[4];
};
/* log Macros for Command Handler sub-module */
#define ESR_TASK_CMD		"EEM Command process"

/* error log */
#define LOG_ESR_CMD_E(_szLogText)  LOG_ESR_E(ESR_TASK_CMD, _szLogText)

/* extended error log (logged with error code) */
#define LOG_ESR_CMD_E_EX(_szLogText, _errCode)  \
	LOG_ESR_E_EX(ESR_TASK_CMD, _szLogText, _errCode)

/* warning log */
#define LOG_ESR_CMD_W(_szLogText)  LOG_ESR_W(ESR_TASK_CMD, _szLogText)

/* info log */
#define LOG_ESR_CMD_I(_szLogText)  LOG_ESR_I(ESR_TASK_CMD, _szLogText)
	


/* used to init command handlers */
#define DEF_CMD_HANDLER(_p, _szCmdType, _bWriteFlag, _fnExcute) \
          ((_p)->szCmdType = (_szCmdType), \
		   (_p)->bIsWriteCommand = (_bWriteFlag), \
		   (_p)->fnExecute = _fnExcute)


#define IS_ALARMBLOCKED_ALARM(_pAlarmSigValue)  \
	((_pAlarmSigValue)->pEquipInfo->iEquipTypeID == STD_ID_ACU_SYSTEM_EQUIP &&  \
			(_pAlarmSigValue)->pStdSig->iSigID == DXI_SPLIT_SIG_MID_ID(SIG_ID_A_ALARM_OUTGOING_BLOCKED))


/* test battery log */
#ifdef _TEST_ESR_BATTLOG
  int iBattLogID;
#endif //_TEST_ESR_BATTLOG
static int ESR_GetDeviceStatus(IN int iEquipID);

/*==========================================================================*
* FUNCTION : Is24VSystem
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN const unsigned char*  szStr : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : LinTao                   DATE: 2004-11-21 16:03
*==========================================================================*/
static BOOL Is24VSystem(void)
{
	//float	fRectAveVolt ;
	//int isignalid,nError,idatalen;
	//SIG_BASIC_VALUE	 *pBv;

	//fRectAveVolt = 48;
	//nError = DxiGetData(VAR_A_SIGNAL_VALUE,
	//	EEM_RECT_GROUP_ID,			
	//	DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,EEM_RECT_RATED_VOLT_ID),		
	//	&idatalen,			
	//	&pBv,			
	//	0);

	//if (nError != ERR_DXI_OK)
	//{
	//	fRectAveVolt = 48;
	//}
	//else
	//{
	//	fRectAveVolt = pBv->varValue.fValue;
	//}

	//
	//if(fRectAveVolt <35)
	//	return TRUE;
	//else
	//	return FALSE;

	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"SMDUP Get SysVolt Level");
	return stVoltLevelState;
}
/*==========================================================================*
 * FUNCTION : GetSubStringCount
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN const unsigned char*  szStr : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-21 16:03
 *==========================================================================*/
static int GetSubStringCount(IN const unsigned char* szStr)
{
	int iCount;
	const unsigned char *p;

	iCount = 0;
	p = szStr;
	while (*p != '\0')
	{
		if (*p == '!')
		{
			iCount++;
		}

		p++;
	}

	return iCount + 1;
}

/*==========================================================================*
 * FUNCTION : *GetSubString
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int                   iIndex : start from 1
 *            IN const unsigned char*  szStr  : 
 *			  OUT int *piSubStrLen            :
 * RETURN   : const unsigned char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-21 16:06
 *==========================================================================*/
static const unsigned char *GetSubString(IN int iIndex, 
										 IN const unsigned char* szStr,
										 OUT int *piSubStrLen)
{
	int iCount;
	const unsigned char *p, *pSubStr;

	iCount = 0;
	p = szStr;

	/* special case */
	if (iIndex == 1)
	{
		pSubStr = p;
	}
	else
	{
		/* 1.move to the start pos */
		while (*p != '\0')
		{
			if (*p == '!')
			{
				iCount++;
			}

			if (iCount == iIndex - 1)
			{
				break;
			}

			p++;
		}

		/* 2.pick the sub-string */
		pSubStr = ++p;
	}

	*piSubStrLen = 0;
	while (*p != '!' && *p != '*' && *p != '\0')
	{
		(*piSubStrLen)++;
		p++;
	}

	if (*piSubStrLen == 0)
	{
		pSubStr = NULL;
	}

	return pSubStr;
}

/*==========================================================================*
 * FUNCTION : IsAlarmBlocked
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-18 16:35
 *==========================================================================*/
static BOOL IsAlarmBlocked(void)
{
	int nEquipID, nBufLen;
	int nError;
	SIG_BASIC_VALUE* pSigValue;
	BOOL bBlock = FALSE;

	/* get Outgoing alarm blocked sig value: (1, 2, 8) */
	nEquipID = DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);
	nError = DxiGetData(VAR_A_SIGNAL_VALUE,
		nEquipID,			
		SIG_ID_ALARM_OUTGOING_BLOCKED,		
		&nBufLen,			
		&pSigValue,			
		0);

	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E("Call DXI interface to get Outgoing alarm blocked "
			"signal value failed");
	}
	else
	{
		/* 0: false   1: true */
		if (pSigValue->varValue.enumValue == 1)
		{
			bBlock = TRUE;
		}
	}

	return bBlock;
}

static int CompareBlockIDs(char *szID1, char *szID2)
{
	return strcmp(szID1, szID2);
}

/* Added by Thomas, support product info, 2006-2-10 */
static int CompareDeviceIDs(int *pID1, int *pID2)
{
	return (*pID1 > *pID2);
}

/*==========================================================================*
 * FUNCTION : ExecuteRI
 * PURPOSE  : RI, Read block identifications
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : length of szOutData is ESR_RESP_SIZE
 * CREATOR  : LinTao                   DATE: 2004-11-14 19:00
 *==========================================================================*/
static void ExecuteRI(ESR_BASIC_ARGS *pThis,
				   const unsigned char *szInData,
				   unsigned char *szOutData)
{
#define ESR_MAX_BLOCK_IDS 300
	int i, j, iPos;

	EEM_MODEL_INFO *pEEMModel;
	int iBlockNum;
	EEM_BLOCK_INFO *pBlock;

	int iIDs;
	char szIDBuf[ESR_MAX_BLOCK_IDS][6];

	int iRectifierNum, nBufLen;
	SIG_BASIC_VALUE  *pSigValue;

	BOOL DieselGroupFlag = TRUE;

	/* not used the param */
	UNUSED(pThis);

	/* 1.check validity */
	if (strcmp(szInData, "RI*") != 0)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 2.excute the command */
	pEEMModel = &g_EsrGlobals.EEMModelInfo;
	iBlockNum = pEEMModel->iBlocksNum;
	pBlock = pEEMModel->pEEMBlockInfo;



	int iS1RectifierNum,iS2RectifierNum,iS3RectifierNum;
	//Jimmyע��������Slaveģ����ź�,�ʴ˴��޸Ĵ��� 2012.07.12
	/* to get the actual rectifier number */
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iRectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iRectifierNum = 1000;  //has no limit
	}
	//S1
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S1_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS1RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS1RectifierNum = 1000;  //has no limit
	}
	//S2
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S2_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS2RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS2RectifierNum = 1000;  //has no limit
	}
	//S3
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S3_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS3RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS3RectifierNum = 1000;  //has no limit
	}

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(DEISEL_GROUP_STD_EQUIP_ID),	
		SIG_ID_DEISEL_GROUP_EXIST_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		 DieselGroupFlag  = pSigValue->varValue.ulValue;
	}
	else
	{	
		 DieselGroupFlag  = TRUE;  //has no limit
	}
	



//	printf("\n****The M RectNum = %d, S1 = %d, S2 = %d, S3 = %d****\n",
//		iRectifierNum,iS1RectifierNum,iS2RectifierNum,iS3RectifierNum);
	/* prepare the buff */

	for (i = 0, j = 0 ; i < iBlockNum && i < ESR_MAX_BLOCK_IDS; i++, pBlock++)
	{
		/* jump the not used rectifiers */
		iRectifierNum += iS1RectifierNum + iS2RectifierNum + iS3RectifierNum;
		if (pBlock->iGroupID == 0x02 && pBlock->iUnitID > iRectifierNum)
		{
			continue;
		}
		
		/* TR# 0137-07-ACU: jump System Group, added by Thomas, 2007-1-31 */
		if (pBlock->iGroupID == 0x00 && pBlock->iUnitID == 0x00)
		{
			continue;
		}

		if(DieselGroupFlag== TRUE &&pBlock->iGroupID == 0x08)
		{
			continue;
		}
		sprintf(szIDBuf[j++], "%02X%02X%1X", pBlock->iGroupID,
			pBlock->iUnitID, pBlock->iSubGroupID);
	}
	iIDs = j;
//	qsort(szIDBuf, (size_t)iIDs, 6, 
//		(int (*)(const void *, const void *))CompareBlockIDs); 

	/* insure not beyond the bound */
	for (i = 0, iPos = 0; i < iIDs && iPos < ESR_RESP_SIZE - 7; i++)
	{
		sprintf(szOutData + iPos, "%s!", szIDBuf[i]);
		iPos += 6;
	}

	/* replace the last '!' with '*' */
	szOutData[iPos - 1] = '*';
	szOutData[iPos] = '\0';

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RI* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : *GetAnalogueStr
 * PURPOSE  : change float value to EEM analogue data format
 * CALLS    : 
 * CALLED BY: GetSigValues
 * ARGUMENTS: BOOL bReserved:
 *			  float  fValue : 
 *			  unsigned char *strValue: length of the buf is 9
 * RETURN   : unsigned char *: 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 10:21
 *==========================================================================*/
static unsigned char *GetAnalogueStr(BOOL bReserved, float fValue, 
									 unsigned char *strValue)
{
	union IEEE_FVALUE ieee_uf;
	union EEM_FVALUE eem_uf;
	union ARM_INT  eem_ui1;
	union ARM_INT  eem_ui2;

	int i;

	/* for reserved values */
	if (bReserved)
	{
		/* Modified by Thomas for CR, using string directly, 2006-2-28 */
		//fValue = EEM_RESERVED_VALUE;
		strcpy(strValue, "7FFFFF80");
		return strValue;
	}
	//added by Jimmy 2011-12-21, specially handle the value '0'.
	if(fValue == 0.0)
	{
		strcpy(strValue, "00000000");
		return strValue;
	}
	/* normal value */
	/* 1.endian process */
#ifdef  _CPU_BIG_ENDIAN  //for ppc
		ieee_uf.fValue = fValue;

#else  //for x86
	{	       
		union IEEE_FVALUE tmpFValue;

		tmpFValue.fValue = fValue;
		
		//printf("signal value = %f \n",fValue);

		for(i=0;i<4;i++)
		{
			ieee_uf.cValue[i] = tmpFValue.cValue[i];
		}
		
	}
#endif 
	
	/* 2.format convert */

	unsigned long tmp;

	tmp = (ieee_uf.iValue & 0x80000000)>>31;
	if(tmp == 0)
	{
		//printf("positive num\n");
		eem_ui1.iValue = 0;

		tmp = (ieee_uf.iValue & 0x007FFFFF)>>1;
		eem_ui1.iValue = tmp << 8;

		tmp = (ieee_uf.iValue & 0x7F800000)>>23;

		eem_ui1.iValue |= ((tmp -126)& 0x000000FF);
	
		eem_ui1.iValue |= 0x40000000;

	}
	else
	{
		//2011-12-21 jimmy modified the arithmetic  for Cristian's TR
		//*****************************
		//eem_ui1.iValue = 0x40000000;

		//tmp = (ieee_uf.iValue & 0x7fffff)>>1;
		//tmp =((~tmp)& 0x7fffff)+1;
		//tmp = tmp << 8;
		//if(tmp != 0)
		//{
		//	eem_ui1.iValue &= ~(0x40000000);
		//}

		//eem_ui1.iValue |= tmp;

		//tmp = (ieee_uf.iValue & 0x7F800000)>>23;

		//eem_ui1.iValue |= ((tmp -126) & 0x000000FF);

		//eem_ui1.iValue |= 0x80000000;
		//*****************************
		eem_ui1.iValue = 0x80000000;

		tmp = (ieee_uf.iValue & 0x007FFFFF)>>1;
		
		tmp |= 0x00400000;

		tmp =((~tmp)& 0x007FFFFF)+1;
		
		//if(tmp != 0)
		//{
		//	eem_ui1.iValue &= ~(0x40000000);
		//}
		tmp = tmp << 8;

		eem_ui1.iValue |= tmp;
		
		tmp = (ieee_uf.iValue & 0x7F800000)>>23;

		eem_ui1.iValue |= ((tmp -126) & 0x000000FF);

	}
	for (i = 0; i < 4; i++)
	{
		sprintf(strValue + 2*i, "%02X", eem_ui1.cValue[3-i]);
	}
	//printf("\n[EEM Test] The fvalue is %f: ; and string is %s ",fValue,strValue);
	return strValue;
}



/*==========================================================================*
 * FUNCTION : Power
 * PURPOSE  : simple assistant function
 * CALLS    : 
 * CALLED BY: GetDigitalStr
 * ARGUMENTS: int  x : 
 *            int  y : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 10:46
 *==========================================================================*/
static int Power(int x, int y)
{
	int i;
	int value = 1;

	for (i = 0; i < y; i++)
	{
		value *= x;
	}

	return value;
}


/*==========================================================================*
 * FUNCTION : *GetDigitalStr
 * PURPOSE  : change bool value to EEM digital data format
 * CALLS    : Power
 * CALLED BY: GetSigValues
 * ARGUMENTS: SIG_ENUM  *peValue : the number of the array is 4.
 * RETURN   : unsigned char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 10:30
 *==========================================================================*/
static unsigned char GetDigitalStr(SIG_ENUM *peValue)
{
	unsigned char cValue;
	int i;

	cValue = 0;

	/* get the nibble value */
	for (i = 0; i < 4; i++)
	{
		if (peValue[3-i] == 1)    //reverse order
		{
			cValue |= Power(2, i);
		}
	}

	/* get the hex character */
	if (cValue <= 9)
	{
		cValue += '0';
	}
	else  
	{
		cValue += ('A' - 10);
	}

	return cValue;
}

/*==========================================================================*
 * FUNCTION : GetBlock
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ExecuteRB
 * ARGUMENTS: int  iGroupID : 
 *            int  iUnitID  : 
 * RETURN   : EEM_BLOCK_INFO* : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 11:29
 *==========================================================================*/
static EEM_BLOCK_INFO* GetBlock(int iGroupID, int iUnitID)
{
	int i;
	EEM_MODEL_INFO *pModel;
	EEM_BLOCK_INFO *pBlock = NULL;

	int iRectifierNum, nBufLen;
	SIG_BASIC_VALUE  *pSigValue;

	pModel = &g_EsrGlobals.EEMModelInfo;
	pBlock = pModel->pEEMBlockInfo;

	int iS1RectifierNum,iS2RectifierNum,iS3RectifierNum;
	//Jimmyע��������Slaveģ����ź�,�ʴ˴��޸Ĵ��� 2012.07.12
	/* to get the actual rectifier number */
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iRectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iRectifierNum = 1000;  //has no limit
	}
	//S1
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S1_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS1RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS1RectifierNum = 1000;  //has no limit
	}
	//S2
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S2_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS2RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS2RectifierNum = 1000;  //has no limit
	}
	//S3
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_S3_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_SLAVE_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		iS3RectifierNum = pSigValue->varValue.ulValue;
	}
	else
	{
		iS3RectifierNum = 1000;  //has no limit
	}

	
	for (i = 0; i < pModel->iBlocksNum; i++, pBlock++)
	{
		if (pBlock->iGroupID == iGroupID && pBlock->iUnitID == iUnitID)
		{
			//printf("pBlock->iGroupID=%d,pBlock->iUnitID=%d\n",pBlock->iGroupID,pBlock->iUnitID);
			break;
		}
	}

	/* not found */
	if (i == pModel->iBlocksNum)
	{
		pBlock = NULL;
	}
	else
	{
		iRectifierNum += iS1RectifierNum + iS2RectifierNum + iS3RectifierNum;
		/* check the actual rectifer number */
		//Jimmyע��������Slaveģ����ź�,�ʴ˴��޸Ĵ��� 2012.07.12
		if (pBlock->iGroupID == 0x02 && pBlock->iUnitID > iRectifierNum)
		{
			pBlock = NULL;
		}	
	}

	return pBlock;
}
static EEM_BLOCK_INFO* GetBlockX()
{
	
	EEM_MODEL_INFO *pModel;
	EEM_BLOCK_INFO *pBlock = NULL;	
	SIG_BASIC_VALUE  *pSigValue;

	pModel = &g_EsrGlobals.EEMModelInfo;
	pBlock = pModel->pEEMBlockInfo;	
	int i;

	for (i = 0; i < pModel->iBlocksNum; i++, pBlock++)
	{
		TRACE("%d, pBlock->iGroupID = %d, pBlock->iSubGroupID= %d, pBlock->iUnitID = %d, pBlock->iEquipID = %d \n", i, pBlock->iGroupID, pBlock->iSubGroupID, pBlock->iUnitID, pBlock->iEquipID);
	}

	
	return NULL;
}
__INLINE static float GetAnalogFromSBV(SIG_BASIC_VALUE* pBv)
{
	VAR_VALUE *pVarValue;
	float fRetValue = 0;

	/* for multi ACU equip type mapping to one EEM equip type, 
	   some signal may have not been defined in a certain ACU model,
	   so pBv is NULL at the case. eg. Phase B current is not defined 
	   in SCUACUnit2 (803).
     */
	if (pBv != NULL)
	{
		pVarValue = &(pBv->varValue);
		switch (pBv->ucType)
		{
		case VAR_LONG:
			fRetValue = (float)pVarValue->lValue;
			break;

		case VAR_FLOAT:
			fRetValue = pVarValue->fValue;
			break;

		case VAR_UNSIGNED_LONG:
			fRetValue = (float)pVarValue->ulValue;
			break;

		case VAR_DATE_TIME:
			fRetValue = (float)pVarValue->dtValue;

#ifdef _TEST_DATA_PRECISION
			if (pVarValue->dtValue)
			{
				TRACEX("Actual time:      %ld\n", pVarValue->dtValue);
				TRACEX("After convertion: %f\n", fRetValue);
			}
#endif //_TEST_DATA_PRECISION

			break;

		case VAR_ENUM:
			fRetValue = (float)pVarValue->enumValue;
		}
	}

	return fRetValue;
}

__INLINE static SIG_ENUM GetDigitalFromSBV(SIG_BASIC_VALUE* pBv)
{
	/* for multi ACU equip type mapping to one EEM equip type, 
	   some signal may have not been defined in ACU model,
	   so pBv is NULL at the case. eg. Phase B current is not defined 
	   in SCUACUnit2 (803).
     */
	return (pBv == NULL ? 0 : pBv->varValue.enumValue);	
}

/*==========================================================================*
 * FUNCTION : GetSigValues
 * PURPOSE  : get sig values by batch and convert to EEM value format
 * CALLS    : DxiGetData
 *			  GetAnalogueStr
 *			  GetDigitalStr
 * CALLED BY: GetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : TRUE for Analogue sig, 
 *											   FALSE for Digital sig
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            MAPENTRIES_INFO  *pEntriesInfo : 
 *            unsigned char    *szOutData    : length is ESR_SIGTYPE_SIZE, 
 *										cleared to zero already
 * RETURN   : BOOL : FALSE for call DXI interface to get sig values failed 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 15:26
 *==========================================================================*/
#define EEM_EQUIPID_OF_SYSTEM			1 
#define EEM_SIGNALID_OF_AMBIENTTEM		50		//Temperature start setting signal
#define EEM_SIGNALID_OF_SAMAMBIENTTEM		43		//Temperature start sample signal

static long GetAmbientTemperature()
{
	int iError, iBufLen, i;
	SIG_BASIC_VALUE* pSigValue;
	
	for( i = 0; i < 7; i++)
	{
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EEM_EQUIPID_OF_SYSTEM,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,EEM_SIGNALID_OF_AMBIENTTEM + i),		
			&iBufLen,			
			(void *)&pSigValue,			
			10000);
		 if(iError == ERR_DXI_OK && pSigValue->varValue.enumValue == 1)//finding ambient temp
		{
			return DXI_MERGE_UNIQUE_SIG_ID(EEM_EQUIPID_OF_SYSTEM, 
				SIG_TYPE_SAMPLING, (EEM_SIGNALID_OF_SAMAMBIENTTEM + i));
		}
	}
	return DXI_MERGE_UNIQUE_SIG_ID(EEM_EQUIPID_OF_SYSTEM, 
				SIG_TYPE_SAMPLING, (EEM_SIGNALID_OF_SAMAMBIENTTEM + 0));
}
static BOOL GetSigValues(BOOL bAnalogue,
						 int iEquipID,
						 //int iUnitID,  // zzc modify
						 int iSigNum, 
						 MAPENTRIES_INFO *pEntriesInfo,
						 unsigned char *szOutData,
						 int iGroupId,
						 int iSubGroupId,
						 int iUnitId)
{
	int		i, j, k;
	float	fBuf;
	BOOL	bReserved,bNotExist;
	BOOL    bDIDisableFlag = FALSE;     //dynamically Disable process,
								        //added by Thomas, 2006-3-29,
	char	      szStrValue[9];      //for IEEE format analogue string value

	SIG_ENUM			digitals[4];
	MAPENTRIES_INFO		*pCurEntry;

	/* to call DXI interface to get sig values */
	int		nError, nBufLen;
	int		nInterfaceType, nVarID, nVarSubID,iEquipmentId;
	SIG_BASIC_VALUE			*pBv;
	GET_MULTIPAL_SIGNALS	stGetMulSignals;

	int iSignalID;
	int idatalen;


	/* 1.get data by DXI interface */
	nError = 0;
	nInterfaceType = VAR_MULTIPLE_SIGNALS_INFO;
	nVarID = MUL_SIGNALS_GET_VALUE;
	nVarSubID = 0;

	memset(&stGetMulSignals, 0, sizeof(GET_MULTIPAL_SIGNALS));
	nBufLen = sizeof(GET_MULTIPAL_SIGNALS);

	
#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("Will call DXI to get a batch of sig values");
	TRACE("Sigs ID info:\n");
#endif //_DEBUG_ESR_CMD_HANDLER
	
	/* prepare GET_MULTIPAL_SIGNALS structure */
	/* note: buffer length of stGetMulSignals is MAX_GET_SIGNALS_NUM(256)! */
	pCurEntry = pEntriesInfo;
	
	for (i = 0, j = 0; i < iSigNum; i++, pCurEntry++)
	{
		/* only get configured sig value from DXI interface */
		if (pCurEntry->iACUSigType >= 0)
		{

#ifdef _DEBUG_ESR_CMD_HANDLER
			TRACE("\tEquip ID: %d", iEquipID);
			TRACE("\t\tACU Sig Type: %d", pCurEntry->iACUSigType);
			TRACE("\t\tACU Sig ID: %d\n", pCurEntry->iACUSigID);
#endif //_DEBUG_ESR_CMD_HANDLER
			
			iSignalID = pCurEntry->iACUSigID;

			if(Is24VSystem())  //���24Vϵͳ��ѹ�澯�ϴ�����
			{

				if((iEquipID==1)&&(pCurEntry->iACUSigType==3)
				    &&((pCurEntry->iACUSigID==154)
				        ||(pCurEntry->iACUSigID==156)
					||(pCurEntry->iACUSigID==157)))
				{
					iSignalID = pCurEntry->iACUSigID+4;
				}
				if((iEquipID==1)&&(pCurEntry->iACUSigType==2)
				    &&((pCurEntry->iACUSigID ==187)
				        ||(pCurEntry->iACUSigID ==189)
					||(pCurEntry->iACUSigID ==190)))
				{
					iSignalID= pCurEntry->iACUSigID+4;
				}
				if(((iEquipID==115)||(pCurEntry->iEquipmentID==115))
				    &&(pCurEntry->iACUSigType==2)
				    &&((pCurEntry->iACUSigID ==6)
				       ||(pCurEntry->iACUSigID ==16)
				       ||(pCurEntry->iACUSigID ==17)
				       ||(pCurEntry->iACUSigID ==46)
				       ||(pCurEntry->iACUSigID ==68)))
				{
					iSignalID = pCurEntry->iACUSigID+100;
				}

			}
				
			/* zzc modify begin */
			if(pCurEntry->iEquipmentID == 0) // the nomal condition
			{
				stGetMulSignals.lData[j++] = DXI_MERGE_UNIQUE_SIG_ID(iEquipID, 
					pCurEntry->iACUSigType, iSignalID);
			}
			else
			{
				if((iGroupId== 4)&&(iSubGroupId==2)
					&&((pCurEntry->iEquipmentID==EEM_FIRST_DCEM_ID)
					||(pCurEntry->iEquipmentID==EEM_FIRST_SMDUPUNIT_ID)))//DCEM�ϴ�ʱ��Ҫ��SMDUP���豸��һ��
				{
					
					stGetMulSignals.lData[j++] = DXI_MERGE_UNIQUE_SIG_ID(pCurEntry->iEquipmentID+iEquipID-651, 
					pCurEntry->iACUSigType, iSignalID);
				}
				else
				{
					stGetMulSignals.lData[j++] = DXI_MERGE_UNIQUE_SIG_ID(pCurEntry->iEquipmentID, 
						pCurEntry->iACUSigType, iSignalID);
				}
			}
			/* zzc modify end */
			//}
		}
	}

	stGetMulSignals.nGetNum = j;

	/* call DXI interface */
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&stGetMulSignals,			
		0);

	if (nError != ERR_DXI_OK)
	{
		szOutData[0] = '\0';
		return FALSE;
	}

	
#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE("\tTotal Sig Num: %d\n", stGetMulSignals.nGetNum);
	TRACE_ESR_TIPS("Get Sig Values from DXI successfully");
	TRACE("Sig Values:\n");
	TRACE("\tSig Num: %d\n", stGetMulSignals.nGetNum);
	for (i = 0; i < stGetMulSignals.nGetNum; i++)
	{
		pBv = (SIG_BASIC_VALUE*)(stGetMulSignals.lData[i]);
		if (bAnalogue)
		{
			TRACE("\tSig Value: #%-4d%.4f\n", i+1, GetAnalogFromSBV(pBv));
		}
		else
		{
			TRACE("\tSig Value: #%-4d%ld\n", i+1, GetDigitalFromSBV(pBv));
		}
	}
#endif //_DEBUG_ESR_CMD_HANDLER


	/* 2.format process */
	pCurEntry = pEntriesInfo;
	for (i = 0, j = 0, k = 0; i < iSigNum; i++, pCurEntry++)
	{
		if (bAnalogue) //for Analogue sig
		{
			bNotExist = FALSE;
			if(pCurEntry->iEquipmentID != 0) 
			{
				if((iGroupId== 4)&&(iSubGroupId==2)
				    &&((pCurEntry->iEquipmentID==EEM_FIRST_DCEM_ID)
				        ||(pCurEntry->iEquipmentID==EEM_FIRST_SMDUPUNIT_ID)))//DCEM�ϴ�ʱ��Ҫ��SMDUP���豸��һ��
				{
					iEquipmentId = pCurEntry->iEquipmentID+iEquipID-EEM_FIRST_SMDUPFUSE_ID;
				}
				else
				{
					iEquipmentId = pCurEntry->iEquipmentID;
				}
				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					iEquipmentId,			
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING,EEM_DEVICE_EXIST_ID),		
					&idatalen,			
					&pBv,			
					0);
				if ((nError != ERR_DXI_OK)||(pBv->varValue.enumValue == TRUE))
				{
					bNotExist =  TRUE;	
				}
				if((pCurEntry->iEquipmentID == EEM_SYSTEM_ID)||(pCurEntry->iEquipmentID == EEM_BATTGROUP_ID))
				{
					bNotExist = FALSE;
				}
			}
			if((pCurEntry->iEquipmentID != 0)&&(bNotExist ==  TRUE))
			{
				bReserved = TRUE;
			}
			else if (pCurEntry->iACUSigType == EEM_RESERVED) //reserved pos
			{
				bReserved = TRUE;
			}

			else
			{
				//bReserved = FALSE;  //modified by Thomas, 2006-3-29
				pBv = (SIG_BASIC_VALUE*)(stGetMulSignals.lData[j++]);

				/* dynamically Disable process(raised by Test Guy), 
				added by Thomas, 2006-3-29 */

				//modified by Jimmy for TR that identifies the value is invalid
				//20120409��Ŀǰû�ܺý������������¶�Ϊ-273ʱ��7FFFFF80��Ҫ�󣬶����²��¶ȵȣ����Ǵ�ԭʼ��-273
				if(pBv)
				{
					bReserved = (SIG_VALUE_IS_VALID(pBv) ? FALSE : TRUE);
				}
				else
				{
					bReserved = TRUE;
				}

				//end modified					
				fBuf = GetAnalogFromSBV(pBv);	
			}

			sprintf(szOutData + 8*i, "%s", 
				GetAnalogueStr(bReserved, fBuf, szStrValue));
		}  //end of Analogue Sig

		else  //for Digital sig
		{
			if (bDIDisableFlag) //added by Thomas, 2006-3-29, dynamically Disable process
			{
				digitals[k++] = EEM_DISABLE_VALUE;
				bDIDisableFlag = FALSE;
			}
			else
			{
				/* modified by Thomas for CR, distinguage enable bit of DI table, 2006-2-28 */
				switch (pCurEntry->iACUSigType)
				{
				case EEM_RESERVED:
					digitals[k++] = EEM_RESERVED_VALUE;
					break;

				case EEM_ENABLE_BIT_OF_DI:
					digitals[k++] = EEM_ENABLE_VALUE;
					break;

				case EEM_DISABLE_BIT_OF_DI:
					digitals[k++] = EEM_DISABLE_VALUE;
					break;

				default:
					pBv = (SIG_BASIC_VALUE*)(stGetMulSignals.lData[j++]);

					//added by Thomas, 2006-3-29, dynamically Disable process
					bDIDisableFlag = (pBv == 0 ? TRUE : FALSE);

					digitals[k++]  = GetDigitalFromSBV(pBv);
					
				}
				//if (pCurEntry->iACUSigType == EEM_RESERVED) //reserved pos
				//{
				//	digitals[k++] = EEM_RESERVED_VALUE;	
				//}
				//else
				//{
				//	pBv = (SIG_BASIC_VALUE*)(stGetMulSignals.lData[j++]);
				//	digitals[k++]  = GetDigitalFromSBV(pBv);	
				//}
				/* modified end, Thomas, 2006-2-28 */
			}

			if (k == 4)
			{
				/* reset */
				k = 0;
				sprintf(szOutData + i/4, "%c", GetDigitalStr(digitals));
			}
		}  //end of Digital sig

	}  //end of loop

	/* process left digital bits */
	if (!bAnalogue && k != 0)
	{
		while (k != 4)
		{
			digitals[k++] = 0;  //just fill 0
		}
		sprintf(szOutData + i/4, "%c", GetDigitalStr(digitals));
	}

	
#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("Convert the format of Sig values");
	if (bAnalogue)
	{
		TRACE("After format converting(Analogue):\n");
		for (i = 0, k = 0; szOutData[i] != '\0'; k++)
		{
			TRACE("\tSig Value #%-4d", k + 1);
			for (j = 0; j < 8; i++, j++)
			{
				TRACE("%c", szOutData[i]);
			}
			TRACE("\n");
		}
	}
	else
	{   TRACE("After format converting(Digital):\n\t");
		for (i = 0, j = 0; szOutData[i] != '\0'; i++, j++)
		{
			TRACE("%c", szOutData[i]);
			if (j == 4)
			{
				TRACE("\t");
			}
		}
		TRACE("\nTotal characters: %d\n", i);
	}
#endif //_DEBUG_ESR_CMD_HANDLER

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : GetBlockData
 * PURPOSE  : 
 * CALLS    : GetSigValues
 * CALLED BY: ExecuteRB
 * ARGUMENTS: EEM_BLOCK_INFO  *pBlock    : 
 *            unsigned char   *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 11:35
 *==========================================================================*/
static void GetBlockData(EEM_BLOCK_INFO *pBlock, unsigned char *szOutData)
{
	int iCurPos;
	size_t iLen;
	BLOCK_MAPENTRIES *pMapEntries;

	BOOL bRet;
	char szDataBuf[ESR_SIGTYPE_SIZE];
	time_t tmTime;
	char szTime[13];

	iCurPos = 0;
	memset(szDataBuf, 0, ESR_SIGTYPE_SIZE * sizeof(char));
	pMapEntries = &pBlock->pTypeInfo->BlockMapEntries;


	/* 1.for <Identity> */
	sprintf(szOutData, "%02X%02X%1X!", pBlock->iGroupID, pBlock->iUnitID,
		pBlock->iSubGroupID);
	iCurPos += 6;

	/* 2.for <Status register>, now just fill 0, modify later */
	if (pBlock->iGroupID == 0)  //function level
	{
		sprintf(szOutData + iCurPos, "012B!");
	}
	else  //group level or unit level
	{
		sprintf(szOutData + iCurPos, "0000!");
	}
	iCurPos += 5;

    /* 3.for AIs */
	// zzc modify begin
	bRet = GetSigValues(TRUE, 
						pBlock->iEquipID, 
						pMapEntries->iAINum,
						pMapEntries->pAIs,
						szDataBuf,
						pBlock->iGroupID,
						pBlock->iSubGroupID,
						pBlock->iUnitID
						);
	/*bRet = GetSigValues(TRUE, 
						pBlock->iEquipID, 
						pBlock->iUnitID, 
						pMapEntries->iAINum,
						pMapEntries->pAIs,
						szDataBuf);*/
	// zzc modify end

	if (!bRet)
	{
		LOG_ESR_CMD_E("Call DXI to get block data(AIs) failed");
		szOutData[0] = '\0';
		return;
	}
	sprintf(szOutData + iCurPos, "%s!", szDataBuf);
	iCurPos += strlen(szDataBuf) + 1;

	/* 4.for AOs */
	memset(szDataBuf, 0, ESR_SIGTYPE_SIZE * sizeof(char));
	// zzc modify begin
	bRet = GetSigValues(TRUE, 
						pBlock->iEquipID, 
						pMapEntries->iAONum,
						pMapEntries->pAOs,
						szDataBuf,
						pBlock->iGroupID,
						pBlock->iSubGroupID,
						pBlock->iUnitID
						);
	/*bRet = GetSigValues(TRUE, 
						pBlock->iEquipID,
						pBlock->iUnitID, 
						pMapEntries->iAONum,
						pMapEntries->pAOs,
						szDataBuf);*/
	// zzc modify end

	if (!bRet)
	{
		LOG_ESR_CMD_E("Call DXI to get block data(AOs) failed");
		szOutData[0] = '\0';
		return;
	}

	iLen = strlen(szDataBuf);
	if ((size_t)ESR_RESP_SIZE - iCurPos < iLen)
	{
		LOG_ESR_CMD_E("ESR_RESP_SIZE is not big enough");
		szOutData[0] = '\0';
		return;
	}
	sprintf(szOutData + iCurPos, "%s!", szDataBuf);
	iCurPos += iLen + 1;

	/* 5.for DIs */
	memset(szDataBuf, 0, ESR_SIGTYPE_SIZE * sizeof(char));
	// zzc modify begin
	bRet = GetSigValues(FALSE, 
						pBlock->iEquipID, 
						pMapEntries->iDINum,
						pMapEntries->pDIs,
						szDataBuf,
						pBlock->iGroupID,
						pBlock->iSubGroupID,
						pBlock->iUnitID
						);
	/*bRet = GetSigValues(FALSE, 
						pBlock->iEquipID,
						pBlock->iUnitID, 
						pMapEntries->iDINum,
						pMapEntries->pDIs,
						szDataBuf);*/
	// zzc modify end

	if (!bRet)
	{
		LOG_ESR_CMD_E("Call DXI to get block data(DIs) failed");
		szOutData[0] = '\0';
		return;
	}

	iLen = strlen(szDataBuf);
	if ((size_t)ESR_RESP_SIZE - iCurPos < iLen)
	{
		LOG_ESR_CMD_E("ESR_RESP_SIZE is not big enough");
		szOutData[0] = '\0';
		return;
	}
	sprintf(szOutData + iCurPos, "%s!", szDataBuf);
	iCurPos += iLen + 1;

	/* 6.for DOs */
	memset(szDataBuf, 0, ESR_SIGTYPE_SIZE * sizeof(char));
	// zzc modify begin
	bRet = GetSigValues(FALSE, 
						pBlock->iEquipID, 
						pMapEntries->iDONum,
						pMapEntries->pDOs,
						szDataBuf,
						pBlock->iGroupID,
						pBlock->iSubGroupID,
						pBlock->iUnitID
						);
	/*bRet = GetSigValues(FALSE, 
						pBlock->iEquipID,
						pBlock->iUnitID, 
						pMapEntries->iDONum,
						pMapEntries->pDOs,
						szDataBuf);*/
	// zzc modify end

	if (!bRet)
	{
		LOG_ESR_CMD_E("Call DXI to get block data(DOs) failed");
		szOutData[0] = '\0';
		return;
	}

	iLen = strlen(szDataBuf);
	if ((size_t)ESR_RESP_SIZE - iCurPos < iLen)
	{
		LOG_ESR_CMD_E("ESR_RESP_SIZE is not big enough");
		szOutData[0] = '\0';
		return;
	}
	sprintf(szOutData + iCurPos, "%s!", szDataBuf);
	iCurPos += iLen + 1;

	/* 7.for <Time stamp> */
	tmTime = time(&tmTime);
	sprintf(szOutData + iCurPos, "%s*",
		TimeToString(tmTime, EEM_TIME_FORMAT, szTime, sizeof (szTime)));


	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteRB
 * PURPOSE  : RB, Read block 
 * CALLS    : GetBlockData
 * CALLED BY: ESR_DecodeAndPerform
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : length of szOutData is ESR_RESP_SIZE
 * CREATOR  : LinTao                   DATE: 2004-11-14 19:15
 *==========================================================================*/
static void ExecuteRB(ESR_BASIC_ARGS *pThis,
				   const unsigned char *szInData,
				   unsigned char *szOutData)
{
	size_t  stLength;
	int iGroupID, iUnitID;
	EEM_BLOCK_INFO *pBlock;
	char logText[ESR_LOG_TEXT_LEN];

	UNUSED(pThis);

	/* for log */
	memset(logText, '\0', ESR_LOG_TEXT_LEN * sizeof(char));

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 7)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'B' || szInData[6] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 2.get the block */
	iGroupID = (int)ESR_AsciiHexToChar(szInData + 2);
	iUnitID = (int)ESR_AsciiHexToChar(szInData + 4);

	pBlock = GetBlock(iGroupID, iUnitID);
	//GetBlockX();
	if (pBlock == NULL)
	{
		sprintf(logText, "Block(GroupID:%d, UnitID:%d) not exits in system",
			iGroupID, iUnitID);
		LOG_ESR_CMD_I(logText);

		strcpy(szOutData, "ERR1-RB*");
		return;
	}

//#define _DEBUG_ESR_CMD_HANDLER
#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("Will read data from a Block");
	TRACE("the Block Info\n");
	TRACE("\tGroup ID: %d\n", pBlock->iGroupID);
	TRACE("\tUnit ID: %d\n", pBlock->iUnitID);
	TRACE("\tSubGroup ID: %d\n", pBlock->iSubGroupID);
#endif //_DEBUG_ESR_CMD_HANDLER

	/* 3.get data of the block */
	GetBlockData(pBlock, szOutData);

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RB* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : GetAnalogueData
 * PURPOSE  : get float value from EEM analogue data format
 * CALLS    : 
 * CALLED BY: SetSigValues
 * ARGUMENTS: IN const unsigned char *sValue : char array containing 8 elements
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-20 19:34
 *==========================================================================*/
static float GetAnalogueData(IN const unsigned char *sValue)
{
	union IEEE_FVALUE ieee_uf;
	union IEEE_FVALUE test_uf;
	union EEM_FVALUE eem_uf;
	unsigned long tmp_long, tmp;

	int i;
	float fResult;

	/* 1.format convert */
	for (i = 0; i < 4; i++)
	{
		eem_uf.cValue[3 -i] = ESR_AsciiHexToChar(sValue + 2 * i);
	}

	/* 2.endian process */
#ifdef  _CPU_BIG_ENDIAN  //for ppc
		fResult = ieee_uf.fValue;
#else  //for x86
	//{
	//	union IEEE_FVALUE tmpFValue;
	//	for (i = 0; i < 4; i++)
	//	{
	//		tmpFValue.cValue[i] = test_uf.cValue[3-i];
	//	}
	//	fResult = tmpFValue.fValue;
	//	printf("\nppc fResult = %f\n", fResult);
	//	fResult = ieee_uf.fValue;
	//	printf("\nnon ppc fResult = %f\n", fResult);
	//	
	//	
	//}
		
		
	//printf("---------------------0---------------------\n");
	
	//for (i = 0; i < 4; i++)
	//{
	//	printf("%02x", eem_uf.cValue[i]);
	//	
	//}
	//printf("\n------------------0------------------------\n");
	
	//printf("%02x", (eem_uf.iValue & 0xFF000000) >> 24);
	
	//printf("%02x", (eem_uf.iValue & 0x00FF0000) >> 16);
	//printf("%02x", (eem_uf.iValue & 0x0000FF00) >> 8);
	//printf("%02x", (eem_uf.iValue & 0x000000FF));

	
	tmp_long = (eem_uf.iValue & 0x80000000)>>31;
	
	
	if(eem_uf.iValue == 0)
	{
		test_uf.fValue = 0;
	}	
	else if (tmp_long == 0) //positive value
	{
		//test_uf.iValue = 0 ;
		//test_uf.iValue |= ((eem_uf.iValue & 0x000000FF) + 125) << 23;
		//test_uf.iValue |= (eem_uf.iValue & 0x7FFFFFFF)  >> 7 ;
		//test_uf.iValue |= 0x00800000; 	
		//fResult = test_uf.fValue * 2;	
		
		
		test_uf.iValue = 0 ;
		test_uf.iValue |= ((eem_uf.iValue & 0x000000FF) + 126) << 23;
		tmp = (eem_uf.iValue & 0x3FFFFFFF)  >> 8 ;	
		tmp = (tmp & 0x3fffff) << 1;
		test_uf.iValue |= tmp;					
		test_uf.iValue &= 0x7FFFFFFF;
	}
	else
	{
		test_uf.iValue =  0x00000000;
		
		tmp = (eem_uf.iValue & 0x3FFFFFFF) >> 8 ;			 		
		tmp = (~tmp) + 1;
		tmp = (tmp & 0x003FFFFF) << 1;		
		test_uf.iValue |= tmp; 
		test_uf.iValue |= ((eem_uf.iValue & 0x000000FF) + 126) << 23;
		test_uf.iValue |= 0x80000000;
	}
#endif 
	fResult = test_uf.fValue;
	TRACE("\nfResult = %f\n", fResult);

	return fResult;
}

/*==========================================================================*
 * FUNCTION : GetDigitalData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SetSigValues
 * ARGUMENTS: IN unsigned char  chr      : 
 *            OUT SIG_ENUM      *peValue : 4 elements array
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-20 19:49
 *==========================================================================*/
static void GetDigitalData(IN unsigned char chr, OUT SIG_ENUM *peValue)
{
	int i;
	unsigned char c;

	c = chr;

	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}

	/* convert */
	for (i = 0; i < 4; i++)
	{
		peValue[i] = (c & Power(2, 3-i)) == 0 ? 0 : 1;
	}
	
	return;
}


__INLINE static BOOL SetAnalogToVarValue(int iEquipID, long lSigID,
										 VAR_VALUE_EX *pVarValue, float fData)
{
	int nBufLen;
	BOOL bRet;
	SIG_BASIC_VALUE  *pBv = NULL;

	bRet = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,	
		lSigID,
		&nBufLen,			
		&pBv,			
		0);
	
	if (bRet == ERR_DXI_OK && pBv)
	{
		switch (pBv->ucType)
		{
		case VAR_FLOAT:
			pVarValue->varValue.fValue = fData;
			break;

		case VAR_LONG:
			pVarValue->varValue.lValue = fData;
			break;

		case VAR_UNSIGNED_LONG:
			pVarValue->varValue.ulValue = fData;
			break;

		case VAR_DATE_TIME:
			pVarValue->varValue.dtValue = fData;
			break;

		case VAR_ENUM:
			pVarValue->varValue.enumValue = fData;
			break;
		}

		return TRUE;

	}

	return FALSE;
}


__INLINE static BOOL SetDigitalToVarValue(int iEquipID, long lSigID,
								 VAR_VALUE_EX *pVarValue, SIG_ENUM eValue)
{
	int nBufLen;
	BOOL bRet = FALSE;
	SIG_BASIC_VALUE  *pBv;

	bRet = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,	
		lSigID,
		&nBufLen,			
		&pBv,			
		0);

	if ( bRet == ERR_DXI_OK && pBv)
	{
		pVarValue->varValue.enumValue = eValue;
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : SetSigValues
 * PURPOSE  : convert from EEM value format and set sig values by batch
 * CALLS    : GetAnalogueData
 *			  GetDigitalData
 * CALLED BY: SetBlockData
 * ARGUMENTS: BOOL             bAnalogue     : 
 *            int              iEquipID      : 
 *            int              iSigNum       : 
 *            MAPENTRIES_INFO  *pEntriesInfo : 
 *            const unsigned char    *szInData  : 
 * RETURN   : BOOL : 
 * COMMENTS : ensure the length of szInData is legal before call it!
 * CREATOR  : LinTao                   DATE: 2004-11-20 19:01
 *==========================================================================*/
static BOOL SetSigValues(BOOL bAnalogue,
						 int iEquipID,
						 //int iUnitID,  // zzc modify 
						 int iSigNum, 
						 MAPENTRIES_INFO *pEntriesInfo,
						 const unsigned char *sInData)
{
	int i, j, k, iCurPos;
	BOOL bRet = TRUE;
	SIG_ENUM digitals[4];
	MAPENTRIES_INFO *pCurEntry;

	/* need add in DXI module */
	int nError, nBufLen,isignalid;
	int nInterfaceType, nVarID, nVarSubID;
	SET_MULTIPAL_SIGNALS stSetMulSignals;
	int temp[MAX_GET_SIGNALS_NUM];// zzc modify
	EQUIP_INFO		*pEquipInfo1 = NULL, *pEquipInfo2 = NULL;
	int				nTempError, nTempBufLen;

	memset(&stSetMulSignals, 0, sizeof(SET_MULTIPAL_SIGNALS));
	memset(temp, 0, sizeof(temp)); // zzc modify
	nBufLen = sizeof(SET_MULTIPAL_SIGNALS);

	
#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("Received a batch of Sig Values, will call DXI to set them");
	if (bAnalogue)
	{
		TRACE("Sigs value(Analogue) info -- before format converting:\n");
		for (i = 0, k = 0; i < iSigNum; i++)
		{
			TRACE("\tSig Value #%-4d", i + 1);
			for (j = 0; j < 8; j++)
			{
				TRACE("%c", sInData[k++]);
			}
			TRACE("\n");
		}
	}
	else
	{   TRACE("Sigs value(Digital) info -- before format converting:\n");
		TRACE("\t");
		for (i = 0 ; i < (iSigNum + 3)/4; i++)
		{
			TRACE("%c", sInData[i]);
		}
		TRACE("\n");
	}
#endif //_DEBUG_ESR_CMD_HANDLER


	/* note: check the length of sInData out */
	/* 1.format process */
	pCurEntry = pEntriesInfo;
	for (i = 0, j = 0, iCurPos = 0; i < iSigNum;)
	{
		if (bAnalogue) //for Analogue sig
		{
			
			if (pCurEntry->iACUSigType != -1) //not reserved pos
			{
				isignalid = pCurEntry->iACUSigID;
				if(Is24VSystem())  //���24Vϵͳ��ѹ��������
				{
					if((iEquipID==1)&&(pCurEntry->iACUSigType==2)
						&&((pCurEntry->iACUSigID ==187)
						||(pCurEntry->iACUSigID ==189)
						||(pCurEntry->iACUSigID ==190)))
					{
						isignalid = pCurEntry->iACUSigID+4;
					}
					if(((iEquipID==115)||(pCurEntry->iEquipmentID==115))
						&&(pCurEntry->iACUSigType==2)
						&&((pCurEntry->iACUSigID ==6)
						||(pCurEntry->iACUSigID ==16)
						||(pCurEntry->iACUSigID ==17)
						||(pCurEntry->iACUSigID ==46)
						||(pCurEntry->iACUSigID ==68)))
					{
						isignalid = pCurEntry->iACUSigID+100;
					}

				}

				stSetMulSignals.lIDs[j] = DXI_MERGE_SIG_ID(
							pCurEntry->iACUSigType, isignalid);
                // zzc modify begin
				if(pCurEntry->iEquipmentID == 0)
				{
						//printf("Run here1\n");  // zzc debug
					//printf("\n*********iEquipID=%d,pCurEntry->iEquipmentID=%d,iUnitID=%d, fValue = %f\n",iEquipID,pCurEntry->iEquipmentID,pCurEntry->iACUSigID,GetAnalogueData(sInData + iCurPos));// zzc debug
					if (!SetAnalogToVarValue(iEquipID, 
					    stSetMulSignals.lIDs[j],
					    &(stSetMulSignals.vData[j++]),
					    GetAnalogueData(sInData + iCurPos)))
				    {
					    j--;
				    }
				}
				else
				{
					//printf("\n!!!!!!!!!!iEquipID=%d,pCurEntry->iEquipmentID=%d,iUnitID=%d, fValue = %f\n",iEquipID,pCurEntry->iEquipmentID,pCurEntry->iACUSigID,GetAnalogueData(sInData + iCurPos));// zzc debug
					temp[j] = pCurEntry->iEquipmentID;// zzc modify
					if (!SetAnalogToVarValue(pCurEntry->iEquipmentID, 
					    stSetMulSignals.lIDs[j],
					    &(stSetMulSignals.vData[j++]),
					    GetAnalogueData(sInData + iCurPos)))
				    {
						temp[j] = 0;// zzc modify
					    j--;
				    }
				}
				// zzc modify end
				//stSetMulSignals.vData[j++].varValue.fValue = 
				//	GetAnalogueData(sInData + iCurPos);	

			}
			
			/* move to next EEM model sig */
			i++;
			iCurPos += 8;
			pCurEntry++;

		}  //end of Analogue sig process

		else  //for Digital sig
		{
			GetDigitalData(*(sInData + iCurPos), digitals);
			iCurPos += 1;

			for (k = 0; k < 4 && i < iSigNum; k++, i++, pCurEntry++)
			{
				if (pCurEntry->iACUSigType != -1) //not reserved pos
				{
				
					stSetMulSignals.lIDs[j] = DXI_MERGE_SIG_ID(
								pCurEntry->iACUSigType, pCurEntry->iACUSigID);
					// zzc modify begin
					if(pCurEntry->iEquipmentID == 0)
					{
					    if (!SetDigitalToVarValue(iEquipID, 
						    stSetMulSignals.lIDs[j],
						    &(stSetMulSignals.vData[j++]),
						    digitals[k]))
					    {
						    j--;
					    }
					}
					else
					{
						temp[j] = pCurEntry->iEquipmentID;// zzc modify
						if (!SetDigitalToVarValue(pCurEntry->iEquipmentID, 
						    stSetMulSignals.lIDs[j],
						    &(stSetMulSignals.vData[j++]),
						    digitals[k]))
					    {
							temp[j] = 0;   // zzc modify
						    j--;
					    }
					}
					// zzc modify begin
					//stSetMulSignals.vData[j++].varValue.enumValue = digitals[k];									
				}
			}
		}  //end of Digital sig process

	}
	stSetMulSignals.nSetNum = j;

	/* add extra info */
	for (i = 0; i < j; i++)
	{
		stSetMulSignals.vData[i].nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
		stSetMulSignals.vData[i].nSenderType = SERVICE_OF_DATA_COMM;
		stSetMulSignals.vData[i].pszSenderName = "ESR Protocol";

	}


	/* 2.set data by DXI interface */
#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("Will call DXI to set the batch of sig values");
	TRACE("Sigs ID info:\n");
	TRACE("\tTotal num: %d\n", stSetMulSignals.nSetNum);
	for (i = 0; i < stSetMulSignals.nSetNum; i++)
	{
		TRACE("\tEquip ID: %d", iEquipID);
		TRACE("\t\tMerged ID: %08X", stSetMulSignals.lIDs[i]);
		if (bAnalogue)
		{
			TRACE("\t\tNew values: %f\n", 
				stSetMulSignals.vData[i].varValue.fValue);
		}
		else // for Digital
		{
			TRACE("\t\tNew values: %ld\n", 
				stSetMulSignals.vData[i].varValue.enumValue);
		}
	}
#endif //_DEBUG_ESR_CMD_HANDLER

	nInterfaceType = VAR_A_SIGNAL_VALUE;
	nVarID = iEquipID;  //  equip id
	nBufLen = sizeof(VAR_VALUE_EX);

	pEquipInfo1 = NULL;
	nTempError = DxiGetData(VAR_A_EQUIP_INFO,
		nVarID,
		0,
		&nTempBufLen,			
		&pEquipInfo1,			
		0);
	if(ERR_DXI_OK != nTempError)
	{
		pEquipInfo1 = NULL;
	}


	for (i = 0; i < stSetMulSignals.nSetNum; i++)
	{
		nVarSubID = stSetMulSignals.lIDs[i];

		// zzc modify begin
		if(temp[i] == 0)
		{
		nError = DxiSetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			nBufLen,			
			&stSetMulSignals.vData[i],			
			0);
		}
		else
		{
			nError = DxiSetData(nInterfaceType,
			temp[i],			
			nVarSubID,		
			nBufLen,			
			&stSetMulSignals.vData[i],			
			0);
		}
		// zzc modify end

		//Frank Wu,20151106, for that 
		//When there is no SM-BRC connected, accept the incorrect value 7FFFFF80. 
		//It will in this case not matter, since there is no SM-BRC connected,
		//so it will ignore all errors when equipment isn't exist here.
		if(temp[i] == 0)
		{
			if((NULL == pEquipInfo1)
			|| ( !pEquipInfo1->bWorkStatus ))
			{
				continue;
			}
		}
		else
		{
			pEquipInfo2 = NULL;
			nTempError = DxiGetData(VAR_A_EQUIP_INFO,
				temp[i],
				0,
				&nTempBufLen,			
				&pEquipInfo2,			
				0);
			if((ERR_DXI_OK != nTempError)
				|| (NULL == pEquipInfo2)
				|| ( !pEquipInfo2->bWorkStatus ))
			{
				continue;
			}
		}

	    if (nError != ERR_DXI_OK)
		{
#ifdef _DEBUG_ESR_CMD_HANDLER
			TRACEX("Set #%d sig failed. Error code from DXI: %08X\n", i+1, nError);
			/*TRACEX("Detail Info:\n nInterfaceType: %d\tnVarID: %d\t"
				"nVarSubID: %08X\tnBufLen: %d\tNew Data: %f\n",
				nInterfaceType, nVarID, nVarSubID, nBufLen, 
				stSetMulSignals.vData[i].varValue.fValue);*/
#endif //_DEBUG_ESR_CMD_HANDLER

			bRet = FALSE;
		}
	}
	
	return bRet;
}


/*==========================================================================*
 * FUNCTION : SetBlockData
 * PURPOSE  : 
 * CALLS    : SetSigValues
 * CALLED BY: ExecuteWB
 * ARGUMENTS: EEM_BLOCK_INFO  *pBlock   : 
 *            const unsigned char   *szInData : 
 * RETURN   : void : 
 * COMMENTS : 2012/04 Jimmy changed the return type
 * CREATOR  : LinTao                   DATE: 2004-11-20 19:10
 *==========================================================================*/
static BOOL SetBlockData(EEM_BLOCK_INFO *pBlock, const unsigned char *szInData)
{
	const unsigned char *pAOStr, *pDOStr;
	int iStrLen;
	
	BOOL bRet;
	int iEquipID;
	BLOCK_MAPENTRIES *pMapEntries;

	pMapEntries = &pBlock->pTypeInfo->BlockMapEntries;
	iEquipID = pBlock->iEquipID;
	BOOL bResultOfSet = TRUE;

	/* 1.set AOs */
	pAOStr = GetSubString(2, szInData, &iStrLen); 
	if ((iStrLen != 0) && (pAOStr[0] != '$'))  //leave out
	{
		// zzc modify begin
		bRet = SetSigValues(TRUE, iEquipID, 
			pMapEntries->iAONum, pMapEntries->pAOs, pAOStr);
		/*bRet = SetSigValues(TRUE, iEquipID, pBlock->iUnitID,
			pMapEntries->iAONum, pMapEntries->pAOs, pAOStr);*/
		// zzc modify end 

		if (!bRet)
		{
			TRACE_ESR_TIPS("Call DXI interface to set sig values failed");
			bResultOfSet = FALSE;
			//return;  -- continue to set DOs!
		}	
#ifdef _DEBUG_ESR_CMD_HANDLER
		else
		{
			TRACE_ESR_TIPS("Call DXI interface to set sig values successfully");
		}
#endif //_DEBUG_ESR_CMD_HANDLER		
	}

#ifdef _DEBUG_ESR_CMD_HANDLER
	else
	{
		TRACE_ESR_TIPS("Not need to set AOs");
	}
#endif //_DEBUG_ESR_CMD_HANDLER

	/* 2.Set DOs */
	pDOStr = GetSubString(3, szInData, &iStrLen); 

	if ((iStrLen != 0) && (pDOStr[0] != '$'))  //leave out
	{
		// zzc modify begin
		bRet = SetSigValues(FALSE, iEquipID, 
			pMapEntries->iDONum, pMapEntries->pDOs, pDOStr);
		/*bRet = SetSigValues(FALSE, iEquipID, pBlock->iUnitID,
			pMapEntries->iDONum, pMapEntries->pDOs, pDOStr);*/
		// zzc modify end 
		if (!bRet)
		{
			TRACE_ESR_TIPS("Call DXI interface to set sig values failed");
			//return;
			bResultOfSet = FALSE;
		}
#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Call DXI interface to set sig values successfully");
#endif //_DEBUG_ESR_CMD_HANDLER	
	}

#ifdef _DEBUG_ESR_CMD_HANDLER
	else
	{
		TRACE_ESR_TIPS("Not need to set DOs");
	}
#endif //_DEBUG_ESR_CMD_HANDLER


	return bResultOfSet;
}


/*==========================================================================*
 * FUNCTION : CheckWBParam
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int iAONum                      :
 *			  int iDONum                      :
 *			  const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-21 15:52
 *==========================================================================*/
static BOOL CheckWBParam(int iAONum,
						 int iDONum,
						 const unsigned char *szInData,
						 unsigned char *szOutData)
{
	const unsigned char *pAOStr, *pDOStr;
	int iStrLen;

	if (GetSubStringCount(szInData) != 3)
	{
		TRACE_ESR_TIPS("format of WB* command is incorrect");

		strcpy(szOutData, "ERR-CMD*");
		return FALSE;
	}

	/* check AO info */
	pAOStr = GetSubString(2, szInData, &iStrLen);
	if ((iStrLen != 0) && (pAOStr[0] != '$')) //leave out all AOs is acceptable
	{
		if (iStrLen != iAONum * 8)  
		{
			TRACE_ESR_TIPS("AOs length is incorrect in WB* command");
			TRACE("\n------AOs length is incorrect in WB* command iStrLen = %d iAONum = %d iDONum = %d\n", iStrLen, iAONum, iDONum);

			strcpy(szOutData, "ERR1-WB*");
			return FALSE;
		}

		if (!ESR_IsStrAsciiHex(pAOStr, iStrLen))
		{
			TRACE_ESR_TIPS("AOs format is incorrect in WB* command"
				"(should be Hex characters)");

			strcpy(szOutData, "ERR1-WB*");
			return FALSE;
		}
	}

	 
	/* check DO info */
	pDOStr = GetSubString(3, szInData, &iStrLen);
	if ((iStrLen != 0) && (pDOStr[0] != '$'))  //leave out all DOs is acceptable
	{
		if (iStrLen != (iDONum + 3)/4)  
		{
			TRACE_ESR_TIPS("DOs length is incorrect in WB* command");

			strcpy(szOutData, "ERR1-WB*");
			return FALSE;
		}

		if (!ESR_IsStrAsciiHex(pDOStr, iStrLen))
		{
			TRACE_ESR_TIPS("DOs format is incorrect in WB* command"
				"(should be Hex characters)");

			strcpy(szOutData, "ERR1-WB*");
			return FALSE;
		}
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ExecuteWB
 * PURPOSE  : WB, Write block
 * CALLS    : SetBlockData
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 16:21
 *==========================================================================*/
static void ExecuteWB(ESR_BASIC_ARGS *pThis,
				   const unsigned char *szInData,
				   unsigned char *szOutData)
{
	size_t  stLength;
	int iGroupID, iUnitID;
	EEM_BLOCK_INFO *pBlock;
	char logText[ESR_LOG_TEXT_LEN];

	/* for checking szInData length */
	int iAONum, iDONum;  

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength < 9)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'W' || szInData[1] != 'B')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


	/* 2.get the block */
	iGroupID = (int)ESR_AsciiHexToChar(szInData + 2);
	iUnitID = (int)ESR_AsciiHexToChar(szInData + 4);

	pBlock = GetBlock(iGroupID, iUnitID);
	//GetBlockX();
	if (pBlock == NULL)
	{
		sprintf(logText, "Block(GroupID:%d, UnitID:%d) not exits in system",
			iGroupID, iUnitID);
		LOG_ESR_CMD_I(logText);

		strcpy(szOutData, "ERR1-WB*");
		return;
	}

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("Will write data to a Block");
	TRACE("the Block Info\n");
	TRACE("\tGroup ID: %d\n", pBlock->iGroupID);
	TRACE("\tUnit ID: %d\n", pBlock->iUnitID);
	TRACE("\tSubGroup ID: %d\n", pBlock->iSubGroupID);
#endif //_DEBUG_ESR_CMD_HANDLER

	/* 3.check parameters first */
	iAONum = pBlock->pTypeInfo->BlockMapEntries.iAONum;
	iDONum = pBlock->pTypeInfo->BlockMapEntries.iDONum;

	if (!CheckWBParam(iAONum, iDONum, szInData, szOutData))
	{
		return;
	}
	

	/* 3.write data to block */
	//Jimmy Modified
	//SetBlockData(pBlock, szInData);
	/* 4.just reply "WB*" */
	//strcpy(szOutData, "WB*");
	if(SetBlockData(pBlock, szInData))
	{
		strcpy(szOutData, "WB*");
	}
	else
	{
		strcpy(szOutData, "ERR2-WB*");
	}
	//end modified by Jimmy 2012/04/09
	//printf("\n*******");
	//printf(szOutData);
#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("WB* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteRN
 * PURPOSE  : RN, read name
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-01 10:53
 *==========================================================================*/
static void ExecuteRN(ESR_BASIC_ARGS *pThis,
			          const unsigned char *szInData,
			          unsigned char *szOutData)
{
	size_t stLength;

	int nError, nBufLen;
	LANG_TEXT* pSiteName;
	BYTE  bySiteID;

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 3)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'N' || szInData[2] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


#ifdef _TEST_ESR_MODIFYCONFIG
	{
		COMMON_CONFIG UserCfg;
		int iRet;

		iRet = ESR_ModifyCommonCfg(1,
			FALSE,
			pThis,
			0,
			&UserCfg);
		if (iRet != ERR_SERVICE_CFG_OK)
		{
			TRACE("Change Config file failed\n");
			TRACE("Return Value is: %d\n", iRet);
		}

		ESR_PrintCommonCfg(&UserCfg);
	}
#endif //_TEST_ESR_MODIFYCONFIG


	/* 2.get site name */
	bySiteID = ESR_GetCCID(); 
	nError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_NAME,			
			0,		
			&nBufLen,			
			&pSiteName,			
			0);
	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E("Call DXI interface to get Site Name failed");
		sprintf(szOutData, "!%02X!$*", bySiteID);
		return;
	}

	sprintf(szOutData, "%s!%02X!$*", pSiteName->pFullName[0], bySiteID);

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RN* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteSN
 * PURPOSE  : SN, set name
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-01 11:15
 *==========================================================================*/
static void ExecuteSN(ESR_BASIC_ARGS *pThis,
				      const unsigned char *szInData,
				      unsigned char *szOutData)
{
	size_t i, stLength, stName;

	int nError;
	unsigned char szSiteName[32];
	unsigned char c;

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength < 3)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'S' || szInData[1] != 'N' || 
		szInData[stLength-1] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 2.check the param */
	if (stLength > 35)
	{
		strcpy(szOutData, "ERR1-SN*");
		return;
	}

	for (i = 2; i < stLength - 1; i++)
	{
		c = szInData[i];

		/* 0x20(Space) is allowed */
		if (c < 0x20 || c >= 0X7F || c == '*' || c == '!'|| c == '#')
		{
			strcpy(szOutData, "ERR1-SN*");
			return;
		}
	}

	/* 3.set site name */
	stName = stLength - 3; 
	strncpyz(szSiteName, szInData + 2, 
		(int)MIN(sizeof(szSiteName), stName + 1));

	nError = DxiSetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_NAME,			
			MODIFY_SITE_ENGLISH_FULL_NAME,		
			(int)strlen(szSiteName),			
			(void *)szSiteName,			
			0);
	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E_EX("Call DXI interface to set Site Name failed", nError);
	}

	sprintf(szOutData, "SN*");

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("SN* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteRT
 * PURPOSE  : RT, read time
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-01 11:34
 *==========================================================================*/
static void ExecuteRT(ESR_BASIC_ARGS *pThis,
				      const unsigned char *szInData,
				      unsigned char *szOutData)
{
	size_t stLength;

	int nError, nBufLen;
	time_t	tCurtime;
	char szTime[13];

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 3)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'T' || 
		szInData[2] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 2.read time */
	nError = DxiGetData(VAR_TIME_SERVER_INFO,
		SYSTEM_TIME,			
		0,		
		&nBufLen,			
		&tCurtime,			
		0);
	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E("Call DXI interface to get System time failed");
		sprintf(szOutData, "*");
		return;
	}


	sprintf(szOutData, "%s*", 
		TimeToString(tCurtime, EEM_TIME_FORMAT, szTime, sizeof (szTime)));

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RT* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
	
}


/*==========================================================================*
 * FUNCTION : ExecuteST
 * PURPOSE  : ST, set time
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-01 11:43
 *==========================================================================*/
static void ExecuteST(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	size_t stLength;

	int i, j, nError;
	struct tm  stTime; 
	time_t tTime;
	char szBuf[6][3];

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 15)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'S' || szInData[1] != 'T' || 
		szInData[stLength - 1] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 2.get and check param */
	for (i = 2; i < 14; i++)
	{
		if (szInData[i] < '0' || szInData[i] > '9')
		{
			strcpy(szOutData, "ERR1-ST*");
			return;
		}
	}
			
	for (i = 0; i < 6; i++)
	{
		j = 2*(i+1);
		szBuf[i][0] = szInData[j];
		szBuf[i][1] = szInData[j+1];
		szBuf[i][2] = '\0';
	}

	stTime.tm_year = atoi(szBuf[0]) + 100;
	stTime.tm_mon  = atoi(szBuf[1]) - 1;
	stTime.tm_mday = atoi(szBuf[2]);

	stTime.tm_hour = atoi(szBuf[3]);
	stTime.tm_min  = atoi(szBuf[4]);
	stTime.tm_sec  = atoi(szBuf[5]);

	if (stTime.tm_mon < 0 || stTime.tm_mon > 11 ||
		stTime.tm_mday < 1 || stTime.tm_mday > 31 ||
		stTime.tm_hour < 0 || stTime.tm_hour > 24 ||
		stTime.tm_min < 0 || stTime.tm_min > 60 ||
		stTime.tm_sec < 0 || stTime.tm_sec > 60)
	{
		strcpy(szOutData, "ERR1-ST*");
		return;
	}

	tTime = mktime(&stTime);
	
	/* 3.set time */
	nError = DxiSetData(VAR_TIME_SERVER_INFO,
		SYSTEM_TIME,			
		0,		
		sizeof(tTime),			
		&tTime,			
		0);
	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E("Call DXI interface to set System time failed");
	}

	sprintf(szOutData, "ST*");

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("ST* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteCR
 * PURPOSE  : CR, control reset
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-12-01 16:58
 *==========================================================================*/
static void ExecuteCR(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	UNUSED(pThis);
	UNUSED(szInData);
	UNUSED(szOutData);

	/* Call DXI to reset */
	DXI_RebootACUorSCU(TRUE, TRUE);

	/* not need reply */
	//strcpy(szOutData, "ERR-CMD*");

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteRP
 * PURPOSE  : RP, read product information
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-01 15:01
 *==========================================================================*/
static void ExecuteRP(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	size_t  stLength;
	int iGroupID, iUnitID;
	EEM_BLOCK_INFO *pBlock;

	int nError, nBufLen;
	EQUIP_INFO* pEquipInfo;

	char logText[ESR_LOG_TEXT_LEN];

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 7)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'P' || szInData[6] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


	/* 2.get the block */
	iGroupID = (int)ESR_AsciiHexToChar(szInData + 2);
	iUnitID = (int)ESR_AsciiHexToChar(szInData + 4);

	pBlock = GetBlock(iGroupID, iUnitID);

	if (pBlock == NULL)
	{
		sprintf(logText, "Block(GroupID:%d, UnitID:%d) not exits in system",
			iGroupID, iUnitID);
		LOG_ESR_CMD_I(logText);

		strcpy(szOutData, "ERR1-RP*");
		return;
	}
	//------------------added by Jimmy 2013.12.13, special for WP0000*
	if(iGroupID == 0 && iUnitID == 0)
	{
		COMMON_CONFIG *pOriCfg;
		/* original common config */
		pOriCfg = &g_EsrGlobals.CommonConfig;

		char pszOriProdctInfo[PRODCT_INFOM_LEN];
		strncpyz(pszOriProdctInfo, 
			pOriCfg->szProdctInfom, PRODCT_INFOM_LEN);

		int i,iLen;
		iLen = strlen(pszOriProdctInfo);
		for(i = 0; i < iLen; i++)
		{
			if(pszOriProdctInfo[i] == '!')
			{
				pszOriProdctInfo[i] = '#'; //�滻����ΪWP�������ԣ��滻��д��Flash�ġ�
			}
		}

		//printf("\n---------Jimmy, the common cfg = %s, and the pszProInfo = %s-----------\n",
		//	pOriCfg->szProdctInfom,pszOriProdctInfo);
		sprintf(szOutData, "%02X%02X%1X!%s*", iGroupID, iUnitID,
			pBlock->iSubGroupID, pszOriProdctInfo);
		return;
	}
	/* 3.read product info(Equip English Full Name) */
	nError = DxiGetData(VAR_A_EQUIP_INFO,
			pBlock->iEquipID,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0);
	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E("Call DXI interface to get Equip Info failed");
		sprintf(szOutData, "%02X%02X%1X!*", iGroupID, iUnitID,
			pBlock->iSubGroupID);
		return;
	}

	sprintf(szOutData, "%02X%02X%1X!%s*", iGroupID, iUnitID,
		pBlock->iSubGroupID, pEquipInfo->pEquipName->pFullName[0]);

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RP* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;

}


/*==========================================================================*
 * FUNCTION : ExecuteWP
 * PURPOSE  : WP, write product information
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-01 15:49
 *==========================================================================*/
static void ExecuteWP(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	size_t  stLength, stName;
	int iGroupID, iUnitID;
	EEM_BLOCK_INFO *pBlock;

	int i, nError, iLen;
	unsigned char szProductInfo[200], c;
	char logText[ESR_LOG_TEXT_LEN];

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength < 9)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'W' || szInData[1] != 'P' || szInData[6] != '!' ||
			szInData[stLength - 1] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


	/* check the param length */
	if (stLength > 210)
	{
		strcpy(szOutData, "ERR1-WP*");
		return;
	}

	/* 2.get the block */
	iGroupID = (int)ESR_AsciiHexToChar(szInData + 2);
	iUnitID = (int)ESR_AsciiHexToChar(szInData + 4);

	pBlock = GetBlock(iGroupID, iUnitID);

	if (pBlock == NULL)
	{
		sprintf(logText, "Block(GroupID:%d, UnitID:%d) not exits in system",
			iGroupID, iUnitID);
		LOG_ESR_CMD_I(logText);

		strcpy(szOutData, "ERR1-WP*");
		return;
	}

	/* 3.write product info(Equip Full English Name) through DXI */
	stName = stLength - 8;
	iLen = (int)MIN(sizeof(szProductInfo), stName + 1);
	strncpyz(szProductInfo, szInData + 7, iLen);

	for (i = 0; i < iLen - 1; i++)
	{
		c = szProductInfo[i];

		/* 0x20(Space) is allowed */
		//Jimmy Modified: EMEAҪ��֧��'#'����ACU+���ܴ洢'#'��������ת����'!'�洢
		//��ϸ�����뿴cfg_loadrun.c�ļ�
		//--------------begin modifying
		if (c < 0x20 || c >= 0X7F || c == '*' || c == '!' /*|| c == '#'*/)
		{
			strcpy(szOutData, "ERR1-WP*");
			return;
		}
		if(c == '#')
		{
			szProductInfo[i] = '!';
		}
		//--------------end modifying
	}
	//--------added by Jimmy 2013.12.13, special for WP0000*
	if (iGroupID == 0 && iUnitID == 0)
	{
		COMMON_CONFIG commonCfg;
		/* get the new Prodct Infom */
		strncpyz(commonCfg.szProdctInfom,
			szProductInfo, PRODCT_INFOM_LEN);

		/* change the alarm report phone number */
		int iRet = ESR_ModifyCommonCfg_MS(TRUE, TRUE, pThis,ESR_CFG_W_MODE | ESR_CFG_PRODCT_INFOM, &commonCfg);
		//printf("\n===Jimmy, the modified string = %s, and set result = %d",
		//	commonCfg.szProdctInfom,iRet);
		if(iRet == ERR_SERVICE_CFG_OK)
		{
			sprintf(szOutData, "WP*");
		}
		else
		{
			strcpy(szOutData, "ERR1-WP*");
		}
		return;
	}

	//--------ended

	szProductInfo[32] = '\0'; //������������������32
	
	nError = DxiSetData(VAR_A_EQUIP_INFO,
			pBlock->iEquipID,			
			MODIFY_SIGNAL_ENGLISH_FULL_NAME,		
			(int)strlen(szProductInfo),			
			(void *)szProductInfo,			
			0);
	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E("Call DXI interface to set Product Info failed");
	}

	sprintf(szOutData, "WP*");

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("WP* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteCB
 * PURPOSE  : CB, call back
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-01 13:43
 *==========================================================================*/
static void  ExecuteCB(ESR_BASIC_ARGS *pThis,
					   const unsigned char *szInData,
					   unsigned char *szOutData)
{
	size_t stLength;
	BOOL   bCallbackInUse;

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 3)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'C' || szInData[1] != 'B' || szInData[2] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 2.check common config */
	bCallbackInUse = g_EsrGlobals.CommonConfig.bCallbackInUse;
	if (!bCallbackInUse)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 3.excute */
	if (!pThis->bSecureLine)
	{

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Register Callback by Command Handler"); 
#endif //_DEBUG_ESR_CMD_HANDLER
		  
		ESR_RegisterCallback(pThis, FALSE);
	}

	sprintf(szOutData, "CB*");

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("CB* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

    return;
}


/*==========================================================================*
 * FUNCTION : GetBlockByEquipID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CurAlarmFormatProcess
 * ARGUMENTS: int  iEquipID : 
 * RETURN   : EEM_BLOCK_INFO* : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 13:20
 *==========================================================================*/
//static EEM_BLOCK_INFO* GetBlockByEquipID(int iEquipID)
//{
//	int i;
//	EEM_MODEL_INFO *pModel;
//	EEM_BLOCK_INFO *pBlock;
//
//	pModel = &g_EsrGlobals.EEMModelInfo;
//	pBlock = pModel->pEEMBlockInfo;
//
//	for (i = 0; i < pModel->iBlocksNum; i++, pBlock++)
//	{
//		if (pBlock->iEquipID == iEquipID)
//		{
//			return pBlock;
//		}
//	}
//
//	return NULL;
//}


/*==========================================================================*
 * FUNCTION : GetEEMSigIdentity
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: CurAlarmFormatProcess
 * ARGUMENTS: IN EEM_BLOCK_INFO  *pBlock       : 
 *            IN int             iACUSigType   : 
 *            IN int             iACUSigID     : 
 *            OUT int            *piEEMSigType : 
 *            OUT int            *piEEMPort    : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 13:44
 *==========================================================================*/
//static BOOL GetEEMSigIdentity(IN EEM_BLOCK_INFO *pBlock, 
//							  IN int iACUSigType, IN int iACUSigID, 
//							  OUT int *piEEMSigType, OUT int *piEEMPort)
//{
//	int i, iNum;
//	BLOCK_MAPENTRIES *pBlockMapEntries;
//	MAPENTRIES_INFO *pCurMapEntries;
//
//	pBlockMapEntries = &(pBlock->pTypeInfo->BlockMapEntries);
//	
//	/* 1.check AIs */
//	pCurMapEntries = pBlockMapEntries->pAIs;
//	iNum = pBlockMapEntries->iAINum;
//	printf("\n[EEM_TEST_RA]: Curr Equip ID is %d\t DI: %d\t DO: %d\t",pCurMapEntries->iEquipmentID,pBlockMapEntries->iDINum,pBlockMapEntries->iDONum);
//	for (i = 0; i < iNum; i++, pCurMapEntries++)
//	{
//		if (pCurMapEntries->iACUSigType == iACUSigType &&
//				pCurMapEntries->iACUSigID == iACUSigID)
//		{
//			*piEEMSigType = EEM_SIGTYPE_AI;
//			*piEEMPort = i;
//
//			return TRUE;
//		}
//	}
//
//	/* 2.check AOs */
//	pCurMapEntries = pBlockMapEntries->pAOs;
//	iNum = pBlockMapEntries->iAONum;
//
//	for (i = 0; i < iNum; i++, pCurMapEntries++)
//	{
//		if (pCurMapEntries->iACUSigType == iACUSigType &&
//				pCurMapEntries->iACUSigID == iACUSigID)
//		{
//			*piEEMSigType = EEM_SIGTYPE_AO;
//			*piEEMPort = i;
//
//			return TRUE;
//		}
//	}
//
//	/* 3.check DIs */
//	pCurMapEntries = pBlockMapEntries->pDIs;
//	iNum = pBlockMapEntries->iDINum;
//
//	for (i = 0; i < iNum; i++, pCurMapEntries++)
//	{
//		if(iNum == 2)
//		{
//			printf("\nFind!!![EEM_Test_RA] Entry[%d]: Type %d and SigID %d",i+1,pCurMapEntries->iACUSigType,pCurMapEntries->iACUSigID);
//		}
//		if (pCurMapEntries->iACUSigType == iACUSigType &&
//				pCurMapEntries->iACUSigID == iACUSigID)
//		{
//			*piEEMSigType = EEM_SIGTYPE_DI;
//			*piEEMPort = i;
//
//			return TRUE;
//		}
//	}
//
//	/* 4.check DOs */
//	pCurMapEntries = pBlockMapEntries->pDOs;
//	iNum = pBlockMapEntries->iDONum;
//
//	for (i = 0; i < iNum; i++, pCurMapEntries++)
//	{
//		if (pCurMapEntries->iACUSigType == iACUSigType &&
//				pCurMapEntries->iACUSigID == iACUSigID)
//		{
//			*piEEMSigType = EEM_SIGTYPE_DO;
//			*piEEMPort = i;
//
//			return TRUE;
//		}
//	}
//
//	/* not found */
//	return FALSE;
//}
//
//
//

/*********************************************************************************
*  
*  FUNCTION NAME : JudgeEEMSigExistence
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-1-5 15:55:41
*  DESCRIPTION   : Revised for the reason of TR 'OB LVD2 open alarm not reported'.
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static EEM_BLOCK_INFO* JudgeEEMSigExistence(IN int iEquipID,IN int iACUSigType, IN int iACUSigID, 
				 OUT int *piEEMSigType, OUT int *piEEMPort)
{
	int i, iNum;
	EEM_MODEL_INFO *pModel;	
	BLOCK_MAPENTRIES *pBlockMapEntries;
	MAPENTRIES_INFO *pCurMapEntries;

	pModel = &g_EsrGlobals.EEMModelInfo;
	EEM_BLOCK_INFO* pBlock = pModel->pEEMBlockInfo;

	for (i = 0; i < pModel->iBlocksNum; i++, pBlock++)
	{
		if (pBlock->iEquipID == iEquipID)
		{
			pBlockMapEntries = &(pBlock->pTypeInfo->BlockMapEntries);

			/* 1.check AIs */
			pCurMapEntries = pBlockMapEntries->pAIs;
			iNum = pBlockMapEntries->iAINum;
			for (i = 0; i < iNum; i++, pCurMapEntries++)
			{
				if (pCurMapEntries->iACUSigType == iACUSigType &&
					pCurMapEntries->iACUSigID == iACUSigID)
				{
					*piEEMSigType = EEM_SIGTYPE_AI;
					*piEEMPort = i;

					return pBlock;
				}
			}

			/* 2.check AOs */
			pCurMapEntries = pBlockMapEntries->pAOs;
			iNum = pBlockMapEntries->iAONum;

			for (i = 0; i < iNum; i++, pCurMapEntries++)
			{
				if (pCurMapEntries->iACUSigType == iACUSigType &&
					pCurMapEntries->iACUSigID == iACUSigID)
				{
					*piEEMSigType = EEM_SIGTYPE_AO;
					*piEEMPort = i;

					return pBlock;
				}
			}

			/* 3.check DIs */
			pCurMapEntries = pBlockMapEntries->pDIs;
			iNum = pBlockMapEntries->iDINum;

			for (i = 0; i < iNum; i++, pCurMapEntries++)
			{
				if (pCurMapEntries->iACUSigType == iACUSigType &&
					pCurMapEntries->iACUSigID == iACUSigID)
				{
					*piEEMSigType = EEM_SIGTYPE_DI;
					*piEEMPort = i;

					return pBlock;
				}
			}

			/* 4.check DOs */
			pCurMapEntries = pBlockMapEntries->pDOs;
			iNum = pBlockMapEntries->iDONum;

			for (i = 0; i < iNum; i++, pCurMapEntries++)
			{
				if (pCurMapEntries->iACUSigType == iACUSigType &&
					pCurMapEntries->iACUSigID == iACUSigID)
				{
					*piEEMSigType = EEM_SIGTYPE_DO;
					*piEEMPort = i;

					return pBlock;
				}
			}

		}
	}
	
	/* not found */
	pBlock = NULL;
	return NULL;
}



/*==========================================================================*
 * FUNCTION : CurAlarmFormatProcess
 * PURPOSE  : 
 * CALLS    : GetBlockByEquipID
 *			  GetEEMSigIdentity
 * CALLED BY: GetCurAlarms
 * ARGUMENTS: BOOL                 bAlarmBlocked:
 *            time_t               tmBlockedTime:
 *			  ALARM_INFO_BUFFER       *pAlarmBuf:		
 *			  ALARM_INFO        *pCurAlarmInfo	:
 *            ALARM_SIG_VALUE   *pAlarmSigValue :             
 * RETURN   : int : error code defined in err_code.h, including:
 *                  ERR_ESR_CMD_OK
 *					ERR_ESR_CMD_NEXT(jump current process)
 *					ERR_ESR_CMD_FAIL
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 11:17
 *==========================================================================*/
static int CurAlarmFormatProcess(BOOL bAlarmBlocked,
								 time_t tmBlockedTime,
								 ALARM_INFO_BUFFER *pAlarmBuf,
								 ALARM_SIG_VALUE *pAlarmSigValue,
								 ALARM_INFO *pCurAlarmInfo)
{
	EEM_BLOCK_INFO *pBlock;
	BOOL bRet;
	int  iEquipID, iACUSigID;
	int  iEEMSigType, iEEMSigID, iEEMAlarmLevel;

	iEquipID = pAlarmSigValue->pEquipInfo->iEquipID;

	/* outgoing alarm blocked process */
	if (bAlarmBlocked)
	{
		/* not need report */
		if (!IS_ALARMBLOCKED_ALARM(pAlarmSigValue) && 
			pAlarmSigValue->sv.tmStartTime > tmBlockedTime)
		{
			return ERR_ESR_CMD_NEXT;
		}
	}

	/* 1.time format process */
	TimeToString(pAlarmSigValue->sv.tmStartTime,
				 EEM_TIME_FORMAT,
		         pCurAlarmInfo->szStartTime, 
				 sizeof (pCurAlarmInfo->szStartTime));

	TimeToString(pAlarmSigValue->sv.tmEndTime,
				 EEM_TIME_FORMAT,
		         pCurAlarmInfo->szEndTime, 
				 sizeof (pCurAlarmInfo->szEndTime));


	/* 2.Identity format process */
	/* get relevant block */
	//Jimmy Has changed the logic,because for LVD,there would be trouble when 
	//2 LVD are using the same EuipID but different Block

	//pBlock = GetBlockByEquipID(iEquipID);	
	/* get EEM sig identity */
	iACUSigID = pAlarmSigValue->pStdSig->iSigID;
	pBlock = JudgeEEMSigExistence(iEquipID,SIG_TYPE_ALARM, iACUSigID,
				    &iEEMSigType, &iEEMSigID);
	if (pBlock == NULL)
	{
#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Find an equip which is not mapped to EEM model");
		TRACE("Equip ID: %d\n", iEquipID);
#endif //_DEBUG_ESR_CMD_HANDLER

		return ERR_ESR_CMD_NEXT;
	}
	pCurAlarmInfo->iGroupID = pBlock->iGroupID;
	pCurAlarmInfo->iSubGroupID = pBlock->iSubGroupID;
	pCurAlarmInfo->iUnitID = pBlock->iUnitID;	

	//printf("\niEquipID=%d iGroupID=%d iSubGroup=%d iUnitID=%d",pBlock->iEquipID,pBlock->iGroupID,pBlock->iSubGroupID,pBlock->iUnitID);
	//bRet = GetEEMSigIdentity(pBlock, SIG_TYPE_ALARM, iACUSigID,
	//	&iEEMSigType, &iEEMSigID);
//	if (!bRet)
//	{
//#ifdef _DEBUG_ESR_CMD_HANDLER
//		TRACE_ESR_TIPS("Find an sig which is not mapped to EEM model");
//		TRACE("ACU Sig Type: Alarm\t\tACU Sig ID: %d\n", iACUSigID);
//#endif //_DEBUG_ESR_CMD_HANDLER
//
//		return ERR_ESR_CMD_NEXT;
//	}

	/* assign EEM sig identity */
	switch (iEEMSigType)
	{
	case EEM_SIGTYPE_DI:
		pCurAlarmInfo->cSection = 'I';
		break;

	case EEM_SIGTYPE_DO:
		pCurAlarmInfo->cSection = 'O';
		break;

	default:  //error!
		LOG_ESR_CMD_E("EEM sig type should be DI or DO for alarms");

		return ERR_ESR_CMD_FAIL;
	}

	pCurAlarmInfo->iCno = iEEMSigID;



	/* 3.alarm level format process */
	switch (pAlarmSigValue->pStdSig->iAlarmLevel)
	{
	case ALARM_LEVEL_CRITICAL:
		iEEMAlarmLevel = EEM_ALARM_LEVEL_A1;		/* A1 */
		pAlarmBuf->iA1Num++;
		break;

	case ALARM_LEVEL_MAJOR:
		iEEMAlarmLevel = EEM_ALARM_LEVEL_A2;		/* A2 */
		pAlarmBuf->iA2Num++;
		break;

	case ALARM_LEVEL_OBSERVATION:
		iEEMAlarmLevel = EEM_ALARM_LEVEL_O1;		/* O1 */
		pAlarmBuf->iO1Num++;
		break;

	default:  //error
		LOG_ESR_CMD_E("New ACU alarm level used, which can not be handled");

		return ERR_ESR_CMD_FAIL;
	}

	pCurAlarmInfo->iAlarmLevel = iEEMAlarmLevel;
	return ERR_ESR_CMD_OK;

}

/*==========================================================================*
 * FUNCTION : SortAlarmsByTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: GetCurAlarms
 * ARGUMENTS: ALARM_INFO  *pAlarmInfo : 
 *            int         iNum        : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 19:34
 *==========================================================================*/
static void SortAlarmsByTime(ALARM_INFO *pAlarmInfo, int iNum)
{
	BOOL bDone;
	int i, j;

	i = 0;
	bDone = FALSE;
	while (i < iNum && (!bDone))
	{
		bDone = TRUE;
		for (j = 1; j < iNum - i; j++)
		{
			if (strcmp(pAlarmInfo[j].szStartTime, 
					pAlarmInfo[j - 1].szStartTime) > 0)
			{
				bDone = FALSE;
				SWAP(ALARM_INFO, pAlarmInfo[j], pAlarmInfo[j - 1]);
			}
		}

		i++;
	}

}


/*==========================================================================*
 * FUNCTION : SortAlarmsByLevel
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: GetCurAlarms
 * ARGUMENTS: ALARM_INFO  *pAlarmInfo : 
 *            int         iNum        : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 19:17
 *==========================================================================*/
static void SortAlarmsByLevel(ALARM_INFO *pAlarmInfo, int iNum)
{
	BOOL bDone;
	int i, j;
	int iLevel, iStartPos, nAlarms;
	
	/* sort by Level */
	i = 0;
	bDone = FALSE;
	while (i < iNum && (!bDone))
	{
		bDone = TRUE;
		for (j = 1; j < iNum - i; j++)
		{
			if (pAlarmInfo[j].iAlarmLevel < pAlarmInfo[j - 1].iAlarmLevel)
			{
				bDone = FALSE;
				SWAP(ALARM_INFO, pAlarmInfo[j], pAlarmInfo[j - 1]);
			}
		}

		i++;
	}

	/* sort by time for each level */
	iStartPos = 0;
	iLevel = pAlarmInfo[iStartPos].iAlarmLevel;
	nAlarms = 0;
	for (i = 0; i < iNum; i++, nAlarms++)
	{
		if (pAlarmInfo[i].iAlarmLevel != iLevel)
		{
			SortAlarmsByTime(&pAlarmInfo[iStartPos], nAlarms);
			iStartPos = i;
			nAlarms = 0;

			iLevel = pAlarmInfo[i].iAlarmLevel;
		}
	}

	/* sort the last level */
	SortAlarmsByTime(&pAlarmInfo[iStartPos], nAlarms);

}

/*==========================================================================*
 * FUNCTION : ClearAlarmInfoBuf
 * PURPOSE  : used to clear alarm info buffer
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL               bCurrent   : TRUE for Active Alarms
 *											  FALSE for History Alarms
 *            ALARM_INFO_BUFFER  *pAlarmBuf : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-02 15:28
 *==========================================================================*/
static void ClearAlarmInfoBuf(BOOL bCurrent, ALARM_INFO_BUFFER *pAlarmBuf)
{
	int iAlarmNum;
	
	/* clear statistic data */
	pAlarmBuf->iA1Num = 0;
	pAlarmBuf->iA2Num = 0;
	pAlarmBuf->iO1Num = 0;

	if (bCurrent)  //for active alarm info
	{
		iAlarmNum = pAlarmBuf->iCurAlarmNum;
		pAlarmBuf->iCurAlarmNum = 0;

		if (iAlarmNum > ESR_ALARM_BUF_SIZE)  //use dynamic buf
		{
			DELETE(pAlarmBuf->pCurAlarmInfo);
			pAlarmBuf->pCurAlarmInfo = NULL;
		}
		else		//use static buf
		{
			memset(pAlarmBuf->sCurAlarmInfo, 0, 
				sizeof(pAlarmBuf->sCurAlarmInfo));
		}
	}
	else  //for history alarm info
	{
		pAlarmBuf->iHisAlarmNum = 0;

		DELETE(pAlarmBuf->pHisAlarmInfo);
		pAlarmBuf->pHisAlarmInfo = NULL;
	}

	return;
}



/*==========================================================================*
 * FUNCTION : GetAlarmBlockedTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ALARM_SIG_VALUE  *pAlarmSigValueBuf : 
 *            int              nSigNum            : 
 * RETURN   : time_t : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-18 20:05
 *==========================================================================*/
static time_t GetAlarmBlockedTime(ALARM_SIG_VALUE *pAlarmSigValue,
								  int nSigNum)
{
	int i;

	for (i = 0; i < nSigNum; i++, pAlarmSigValue++)
	{
		if (IS_ALARMBLOCKED_ALARM(pAlarmSigValue))
		{
			return pAlarmSigValue->sv.tmStartTime;
		}
	}

	LOG_ESR_CMD_E("Alarm blocked flag is inconsistent with current"
		"alarm info");
	return 0;
}

/*==========================================================================*
 * FUNCTION : GetCurAlarms
 * PURPOSE  : Get Current active alarm info to fill ALARM_INFO_BUFFER
 * CALLS    : CurAlarmFormatProcess
 *			  SortAlarmsByLevel
 *			  SortAlarmsByTime
 * CALLED BY: ExecuteRA
 * ARGUMENTS: ALARM_INFO_BUFFER *pAlarmBuf : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 10:47
 *==========================================================================*/
static void GetCurAlarms(ALARM_INFO_BUFFER *pAlarmBuf)
{
	int i, iRet;
	int nError, nBufLen, nSigNum;

	time_t tmAlarmBlocked;
	BOOL   bAlarmBlocked;

	ALARM_SIG_VALUE *pCurAlarmSigValue, *pAlarmSigValueBuf;
	ALARM_INFO *pCurAlarmInfo;     //EEM format alarm info
	

	/* 1.call DXI to get active alarms */
	/* get alarm number first */
	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
		ALARM_LEVEL_NONE,			
		0,		
		&nBufLen,			
		&nSigNum,			
		0);
	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E("Call DXI interface to get active alarm number failed");
		return;
	}

	if (nSigNum == 0)
	{
#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Call DXI interface to get active alarm info "
			"successfully, but no alarm in ACU now");
#endif //_DEBUG_ESR_CMD_HANDLER

		return;
	}

	/* now get alarm info */
	pAlarmSigValueBuf = NEW(ALARM_SIG_VALUE, nSigNum);
	if (pAlarmSigValueBuf == NULL)
	{
		LOG_ESR_CMD_E("no memory to load active alarms");
		return;
	}

	nBufLen = sizeof(ALARM_SIG_VALUE) * nSigNum;

	nError = DxiGetData(VAR_ACTIVE_ALARM_INFO,
		ALARM_LEVEL_NONE,			
		0,		
		&nBufLen,			
		pAlarmSigValueBuf,			
		0);
	if (nError != ERR_DXI_OK)
	{
		LOG_ESR_CMD_E("Call DXI interface to get active alarm info failed");
		DELETE(pAlarmSigValueBuf);
		return;
	}
	
	/* get alarm number at present */
	nSigNum = nBufLen/sizeof(ALARM_SIG_VALUE);
			
#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("Call DXI interface to get alarm info successfully");
	TRACE("Alarm Info(total: %d):\n", nSigNum);
#endif //_DEBUG_ESR_CMD_HANDLER

	/* 2.store the alarms to ALARM_INFO_BUFFER */
	/* init the buffer */
	pAlarmBuf->iCurAlarmNum = nSigNum;
	if (nSigNum > ESR_ALARM_BUF_SIZE)
	{
		pCurAlarmInfo = NEW(ALARM_INFO, nSigNum);
		if (pCurAlarmInfo == NULL)
		{
			LOG_ESR_CMD_E("no memory to load active alarms");

			/* clear the number counter */
			pAlarmBuf->iCurAlarmNum = 0;

			DELETE(pAlarmSigValueBuf);
			return;
		}
		pAlarmBuf->pCurAlarmInfo = pCurAlarmInfo;
	}
	else
	{
		pCurAlarmInfo = pAlarmBuf->sCurAlarmInfo;
	}

	/* format changing */
	if (IsAlarmBlocked())  //alarm blocked process
	{
		tmAlarmBlocked = GetAlarmBlockedTime(pAlarmSigValueBuf, nSigNum);
		bAlarmBlocked = TRUE;
	}
	else
	{
		bAlarmBlocked = FALSE;
	}

	pCurAlarmSigValue = pAlarmSigValueBuf;
	for (i = 0; i < nSigNum; i++, pCurAlarmSigValue++, pCurAlarmInfo++)
	{

#ifdef _DEBUG_ESR_CMD_HANDLER
		{
			EQUIP_INFO *pEquip;
			pEquip = pCurAlarmSigValue->pEquipInfo;
			TRACE("\tEquip ID: %d\tEquip Name: %s\t\tAlarm Sig Name: %s\n", 
				pEquip->iEquipID, pEquip->pEquipName->pFullName[0], 
				pCurAlarmSigValue->pStdSig->pSigName->pFullName[0]);
		}
#endif //_DEBUG_ESR_CMD_HANDLER

		
		iRet = CurAlarmFormatProcess(bAlarmBlocked, tmAlarmBlocked,
			pAlarmBuf, pCurAlarmSigValue, pCurAlarmInfo);

		switch (iRet)
		{
		case ERR_ESR_CMD_OK:  //do nothing
			break;

		case ERR_ESR_CMD_NEXT:  //just jump
			pAlarmBuf->iCurAlarmNum--;
			pCurAlarmInfo--;
			break;

		case ERR_ESR_CMD_FAIL:
			/* clear the buffer */
			ClearAlarmInfoBuf(TRUE, pAlarmBuf);

			/* jump the loop */
			DELETE(pAlarmSigValueBuf);
			return;

		default:  //will not go here
			break;
		}  //end of switch

	}  //end of loop

	/* sort alarms */
	nSigNum = pAlarmBuf->iCurAlarmNum;
	pCurAlarmInfo -= nSigNum;

	SortAlarmsByLevel(pCurAlarmInfo, nSigNum);

	DELETE(pAlarmSigValueBuf);
	return;
}


/*==========================================================================*
 * FUNCTION : GetActiveAlarmNumber
 * PURPOSE  : not used now, for EEM model is different from ACU
 * CALLS    : 
 * CALLED BY: ExecuteRA
 * ARGUMENTS: int             iEEMAlarmLevel : 
 * RETURN   : int : the active alarm number for a certain alarm level
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 16:06
 *==========================================================================*/
//static int GetActiveAlarmNumber(int iEEMAlarmLevel)
//{
//	int nError, nBufLen, iAlarmNum;
//	int iACUAlarmLevel;
//
//	/* 1.get relevent ACU alarm level */
//	switch (iEEMAlarmLevel)
//	{
//	case EEM_ALARM_LEVEL_A1:
//		iACUAlarmLevel = ALARM_LEVEL_CRITICAL;      /* CA */
//		break;
//
//	case EEM_ALARM_LEVEL_A2:
//		iACUAlarmLevel = ALARM_LEVEL_MAJOR;         /* MA */
//		break;
//
//	case EEM_ALARM_LEVEL_O1:
//		iACUAlarmLevel = ALARM_LEVEL_OBSERVATION;   /* OA */
//		break;
//
//	default:  //not support
//		LOG_ESR_CMD_I("Find not-supported EEM Alarm Level(only support "
//			"A1, A2, O1)");
//		return 0;
//	}
//
//	/* 2.Outgoing alarm blocked process */
//	if (IsAlarmBlocked())
//	{
//		/* only report "Outgoing alarm blocked" alarm */
//		if (iACUAlarmLevel == ALARM_LEVEL_OBSERVATION)
//		{
//			return 1;
//		}
//		else
//		{
//			return 0;
//		}
//	}
//
//	/* 3.call DXI to get the number of active alarms */
//	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
//		iACUAlarmLevel,			
//		0,		
//		&nBufLen,			
//		&iAlarmNum,			
//		0);
//	if (nError != ERR_DXI_OK)
//	{
//		LOG_ESR_CMD_E("Call DXI interface to get active alarm number failed");
//		iAlarmNum = 0;
//	}
//
//	return iAlarmNum;
//}


/*==========================================================================*
 * FUNCTION : ExecuteRA
 * PURPOSE  : RA, read number of active alarms
 * CALLS    : GetActiveAlarmNumber
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 15:37
 *==========================================================================*/
static void  ExecuteRA(ESR_BASIC_ARGS *pThis,
					   const unsigned char *szInData,
					   unsigned char *szOutData)
{
	size_t stLength;
	int iA1s, iA2s, iO1s;
	ALARM_INFO_BUFFER *pAlarmBuf;

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 3)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'A' || szInData[2] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


	/* 2.get alarm count for each level */
	pAlarmBuf = &(pThis->AlarmBuffer);

	/* clear buffer first to ensure new */
	ClearAlarmInfoBuf(TRUE, pAlarmBuf);

	/* fill the buffer */
	GetCurAlarms(pAlarmBuf);

	/* get the count */
	iA1s = pAlarmBuf->iA1Num;
	iA2s = pAlarmBuf->iA2Num;
	iO1s = pAlarmBuf->iO1Num;

	ClearAlarmInfoBuf(TRUE, pAlarmBuf);

	/*iA1s = GetActiveAlarmNumber(EEM_ALARM_LEVEL_A1);
	iA2s = GetActiveAlarmNumber(EEM_ALARM_LEVEL_A2);
	iO1s = GetActiveAlarmNumber(EEM_ALARM_LEVEL_O1);*/

	/* 3.write to buf */
	sprintf(szOutData, "%d!%d!%d*", iA1s, iA2s, iO1s);

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RA* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}



/*==========================================================================*
 * FUNCTION : ReadAlarmFromBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ExecuteRC
 * ARGUMENTS: BOOL          bHisAlarm     :
 *			  int           iSequenceID	  : 
 *            ALARM_INFO    *pAlarmInfo   : 
 *            int            iAlarms      : total alarms in the buff
 *            unsigned char  *szOutData   : 
 *			  BOOL *pbReadAll			  : output flag
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 20:20
 *==========================================================================*/
static void ReadAlarmFromBuf(BOOL bHisAlarm,
							 IN int iSequenceID, 
							 IN ALARM_INFO *pAlarmInfo, 
							 IN int iAlarms, 
							 OUT BOOL *pbReadAll,
							 OUT unsigned char *szOutData)
{
	int i, iPos, iOffSet;
	ALARM_INFO *pCurAlarmInfo;

	*pbReadAll = FALSE;


	/* 1.for head */
	sprintf(szOutData, "%02X#", iSequenceID);
	iOffSet = 3;

	/* 2.for alarm info one by one */
	iPos = 10 * iSequenceID;
	pCurAlarmInfo = pAlarmInfo + iPos;
	for (i = 0; i < 10 && iPos < iAlarms; i++, iPos++, pCurAlarmInfo++)
	{
		if (bHisAlarm)
		{
			sprintf(szOutData + iOffSet, "%s!", pCurAlarmInfo->szEndTime);
			iOffSet += 13;
		}

		sprintf(szOutData + iOffSet, "%s!%02X%02X%1X!%c%02X!%1X#",
			pCurAlarmInfo->szStartTime, pCurAlarmInfo->iGroupID,
			pCurAlarmInfo->iUnitID, pCurAlarmInfo->iSubGroupID,
			pCurAlarmInfo->cSection, pCurAlarmInfo->iCno, 
			pCurAlarmInfo->iAlarmLevel);

		iOffSet += 25;
	}

	/* have read all the alarms */
	if (i < 10)
	{
		*pbReadAll = TRUE;
	}

	
	/* change the last '#' to '*' */
	if (iOffSet != 3)
	{
		szOutData[--iOffSet] = '*';
	}
	else	// means empty, reply: xx*
	{
		szOutData[2] = '*';
		szOutData[3] = '\0';
	}

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteRC
 * PURPOSE  : RC, read current alarms
 * CALLS    : ClearAlarmInfoBuf
 *			  GetCurAlarms
 *			  ReadAlarmFromBuf
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-30 16:19
 *==========================================================================*/
static void  ExecuteRC(ESR_BASIC_ARGS *pThis,
					   const unsigned char *szInData,
					   unsigned char *szOutData)
{
	size_t stLength;
	int iSequenceID;

	BOOL bReadAll;
	int iAlarms;
	ALARM_INFO *pAlarmInfo;
	ALARM_INFO_BUFFER *pAlarmBuf;

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 5)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'C' || szInData[4] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


	/* 2.get param and check it */
	if (!ESR_IsStrAsciiHex(szInData + 2, 2))
	{
		strcpy(szOutData, "ERR1-RC*");
		return;
	}

	iSequenceID = (int)ESR_AsciiHexToChar(szInData + 2);


	/* 3.prepare alarm buffer */
	pAlarmBuf = &(pThis->AlarmBuffer);
	iAlarms = pAlarmBuf->iCurAlarmNum;

	if (iSequenceID == 0)  //only RC00* need prepare alarm buffer
	{
	
		/* clear buffer first to ensure new */
		ClearAlarmInfoBuf(TRUE, pAlarmBuf);

		/* fill the buffer */
		GetCurAlarms(pAlarmBuf);
		iAlarms = pAlarmBuf->iCurAlarmNum;
		
		/* no alarm at present */
		if (iAlarms == 0)
		{
			sprintf(szOutData, "%02X*", iSequenceID);

#ifdef _DEBUG_ESR_CMD_HANDLER
			TRACE_ESR_TIPS("RC* has been excuted ok!");
			TRACE("Reply Data:\n");
			TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

			return;
		}
	}


	/* 4.get alarm */
	if (iAlarms > ESR_ALARM_BUF_SIZE)
	{
		pAlarmInfo = pAlarmBuf->pCurAlarmInfo;
	}
	else
	{
		pAlarmInfo = pAlarmBuf->sCurAlarmInfo;
	}

	/*
	printf("ExecuteRC(): start~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");	
	printf("pAlarmBuf->iCurAlarmNum = %d\n",pAlarmBuf->iCurAlarmNum);
	int i;
	for(i=0;i<iAlarms;i++)
	{
		printf("pAlarmBuf->pCurAlarmInfo->iGroupID = %#x\n",(pAlarmInfo+i)->iGroupID);
		printf("pAlarmBuf->pCurAlarmInfo->iSubGroupID = %#x\n",(pAlarmInfo+i)->iSubGroupID);
		printf("pAlarmBuf->pCurAlarmInfo->iCno = %d\n",(pAlarmInfo+i)->iCno);
		printf("pAlarmBuf->pCurAlarmInfo->iAlarmLevel = %d\n",(pAlarmInfo+i)->iAlarmLevel);
	}

	//alarm level of EEM Model, currently support :A1=0, A2=1, O1=3;
	printf("ExecuteRC(): end~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	*/
	
	ReadAlarmFromBuf(FALSE, iSequenceID, 
		pAlarmInfo, iAlarms, &bReadAll, szOutData);


	/* read all the alarms, clear the buf */
	if (bReadAll)
	{
		ClearAlarmInfoBuf(TRUE, pAlarmBuf);
	}

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RC* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}



/*==========================================================================*
 * FUNCTION : HisAlarmFormatProcess
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HIS_ALARM_RECORD  *pAlarmRecord : 
 *            ALARM_INFO        *pAlarmInfo   : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 21:34
 *==========================================================================*/
static int HisAlarmFormatProcess(HIS_ALARM_RECORD *pAlarmRecord,
								 ALARM_INFO *pAlarmInfo)
{
	EEM_BLOCK_INFO *pBlock;
	BOOL bRet;
	int  iEquipID, iACUSigID;
	int  iEEMSigType, iEEMSigID, iEEMAlarmLevel;

	
	/* 1.time format process */
	TimeToString(pAlarmRecord->tmStartTime,
				 EEM_TIME_FORMAT,
		         pAlarmInfo->szStartTime, 
				 sizeof(pAlarmInfo->szStartTime));

	TimeToString(pAlarmRecord->tmEndTime,
				 EEM_TIME_FORMAT,
		         pAlarmInfo->szEndTime, 
				 sizeof(pAlarmInfo->szEndTime));


	/* 2.identity format process */
	/* 2.1 get relevant block */
	iEquipID = pAlarmRecord->iEquipID;
	//pBlock = GetBlockByEquipID(iEquipID);
	iACUSigID = pAlarmRecord->iAlarmID;
	pBlock = JudgeEEMSigExistence(iEquipID,SIG_TYPE_ALARM, iACUSigID,
				    &iEEMSigType, &iEEMSigID);
	if (pBlock == NULL)
	{
#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Find an equip which is not mapped to EEM model");
		TRACE("Equip ID: %d\n", iEquipID);
#endif //_DEBUG_ESR_CMD_HANDLER

		return ERR_ESR_CMD_NEXT;
	}
	pAlarmInfo->iGroupID = pBlock->iGroupID;
	pAlarmInfo->iSubGroupID = pBlock->iSubGroupID;
	pAlarmInfo->iUnitID = pBlock->iUnitID;

	/* 2.2 get EEM sig identity */
	/*iACUSigID = pAlarmRecord->iAlarmID;*/
	//bRet = GetEEMSigIdentity(pBlock, SIG_TYPE_ALARM, iACUSigID,
	//	&iEEMSigType, &iEEMSigID);
//	if (!bRet)
//	{
//#ifdef _DEBUG_ESR_CMD_HANDLER
//		TRACE_ESR_TIPS("Find an sig which is not mapped to EEM model");
//		TRACE("ACU Sig Type: Alarm\t\tACU Sig ID: %d\n", iACUSigID);
//#endif //_DEBUG_ESR_CMD_HANDLER
//
//		return ERR_ESR_CMD_NEXT;
//	}

	/* 2.3 assign EEM sig identity */
	switch (iEEMSigType)
	{
	case EEM_SIGTYPE_DI:
		pAlarmInfo->cSection = 'I';
		break;

	case EEM_SIGTYPE_DO:
		pAlarmInfo->cSection = 'O';
		break;

	default:  //error!
		LOG_ESR_CMD_E("EEM sig type should be DI or DO for alarms");

		return ERR_ESR_CMD_FAIL;
	}

	pAlarmInfo->iCno = iEEMSigID;


	/* 3.alarm level format process */
	switch (pAlarmRecord->byAlarmLevel)
	{
	case ALARM_LEVEL_CRITICAL:
		iEEMAlarmLevel = EEM_ALARM_LEVEL_A1;		/* A1 */
		break;

	case ALARM_LEVEL_MAJOR:
		iEEMAlarmLevel = EEM_ALARM_LEVEL_A2;		/* A2 */
		break;

	case ALARM_LEVEL_OBSERVATION:
		iEEMAlarmLevel = EEM_ALARM_LEVEL_O1;		/* O1 */
		break;

	default:  //error
		LOG_ESR_CMD_E("New ACU alarm level used, which can not be handled");

		return ERR_ESR_CMD_FAIL;
	}

	pAlarmInfo->iAlarmLevel = iEEMAlarmLevel;
	return ERR_ESR_CMD_OK;

}

/*==========================================================================*
 * FUNCTION : GetHisAlarms
 * PURPOSE  : Get History alarm info to fill ALARM_INFO_BUFFER
 * CALLS    : 
 * CALLED BY: ExecuteRH
 * ARGUMENTS: ALARM_INFO_BUFFER  *pAlarmBuf : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 16:48
 *==========================================================================*/
static void GetHisAlarms(ALARM_INFO_BUFFER *pAlarmBuf)
{
	HANDLE hHisAlarmLog;
	BOOL bRet;
	int i, iRet, iStartPos, iRecords;

	HIS_ALARM_RECORD *pAlarmRecord;
	HIS_ALARM_RECORD HisAlarmRecord[MAX_HIS_ALARM_COUNT];
	ALARM_INFO *pAlarmInfo;

	/* 1.Read history alarm info from the Storage */
	hHisAlarmLog = DAT_StorageOpen(HIST_ALARM_LOG);
	if (hHisAlarmLog == NULL)
	{
		LOG_ESR_CMD_E("Open History Alarm Log storage failed");
		return;
	}

	iStartPos = 1;
	iRecords = MAX_HIS_ALARM_COUNT;
	bRet = DAT_StorageReadRecords(hHisAlarmLog, &iStartPos,
		&iRecords, HisAlarmRecord, FALSE, FALSE);
	DAT_StorageClose(hHisAlarmLog);
	if (!bRet)
	{
		LOG_ESR_CMD_E("Read History Alarm Log storage failed");
		return;
	}

	/* 2.init the buffer */
	pAlarmBuf->iHisAlarmNum = iRecords;
	pAlarmInfo = NEW(ALARM_INFO, iRecords);
	if (pAlarmInfo == NULL)
	{
		LOG_ESR_CMD_E("no memory to load history alarms");

		/* clear the number counter */
		pAlarmBuf->iHisAlarmNum = 0;
		return;
	}
	pAlarmBuf->pHisAlarmInfo = pAlarmInfo;
	

	/* 3.format process */
	pAlarmRecord = HisAlarmRecord;
	for (i = 0; i < iRecords; i++, pAlarmRecord++, pAlarmInfo++)
	{

#ifdef _DEBUG_ESR_CMD_HANDLER
		{
			TRACE("\tEquip ID: %d\tAlarm Sig ID: %d\t\tAlarm Level: %d\n", 
				pAlarmRecord->iEquipID, pAlarmRecord->iAlarmID, 
				pAlarmRecord->byAlarmLevel);
		}
#endif //_DEBUG_ESR_CMD_HANDLER


		iRet = HisAlarmFormatProcess(pAlarmRecord, pAlarmInfo);

		switch (iRet)
		{
		case ERR_ESR_CMD_OK:  //do nothing
			break;

		case ERR_ESR_CMD_NEXT:  //just jump
			pAlarmBuf->iHisAlarmNum--;
			pAlarmInfo--;
			break;

		case ERR_ESR_CMD_FAIL:
			/* clear the buffer */
			ClearAlarmInfoBuf(FALSE, pAlarmBuf);

			/* jump the loop */
			return;

		default:  //will not go here
			break;
		}  //end of switch

	}  //end of loop

	/* note: the alarm has been sorted in Storage already */
	return;
}

/*==========================================================================*
 * FUNCTION : ExecuteRH
 * PURPOSE  : RH, Read alarm history
 * CALLS    : GetHisAlarms
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 14:42
 *==========================================================================*/
static void ExecuteRH(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{ 
	size_t stLength;
	int iSequenceID, iAlarms;
	BOOL bReadAll;

	ALARM_INFO *pAlarmInfo;
	ALARM_INFO_BUFFER *pAlarmBuf;


	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 5)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'H' || szInData[4] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 2.get param and check it */
	if (!ESR_IsStrAsciiHex(szInData + 2, 2))
	{
		strcpy(szOutData, "ERR1-RH*");
		return;
	}

	iSequenceID = (int)ESR_AsciiHexToChar(szInData + 2);


#ifdef _TEST_ESR_BATTLOG
  iBattLogID = iSequenceID;
#endif //_TEST_ESR_BATTLOG


	/* 3.read alarm from storage to buf */
	pAlarmBuf = &(pThis->AlarmBuffer);
	
	if (iSequenceID == 0)  //only RH00* need prepare the buffer
	{
		/* clear buffer first to ensure new */
		ClearAlarmInfoBuf(FALSE, pAlarmBuf);

		/* fill the buffer */
		GetHisAlarms(pAlarmBuf);
	}

	/* 4.get alarm */
	pAlarmInfo = pAlarmBuf->pHisAlarmInfo;
	iAlarms = pAlarmBuf->iHisAlarmNum;
	ReadAlarmFromBuf(TRUE, iSequenceID, 
		pAlarmInfo, iAlarms, &bReadAll, szOutData);

	/* read all the alarms, clear the buf */
	if (bReadAll) // no need to clear it, because if not due to index,it will cause empty reply, Jimmy noted 2011/10/09
	{
		//ClearAlarmInfoBuf(FALSE, pAlarmBuf);
	}

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RH* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteCH
 * PURPOSE  : CH, Control alarm history reset
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 15:52
 *==========================================================================*/
static void ExecuteCH(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	size_t stLength;

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 3)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'C' || szInData[1] != 'H' || szInData[2] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	UNUSED(pThis);
	strcpy(szOutData, "CH*");

	/* 2.perform the command */
	DAT_StorageDeleteRecord(HIST_ALARM_LOG);

	return;
}


/*==========================================================================*
 * FUNCTION : CompareProc
 * PURPOSE  : realization of COMPARE_PROC, used by DAT_StorageSearchRecords
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const void  *pRecord    : 
 *            const void  *pCondition : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-28 15:00
 *==========================================================================*/
static int ESR_CompareProc(const void *pRecord, const void *pCondition)
{
	UNUSED(pCondition);
	return (stricmp(pRecord, "Head of a Battery Test") == 0 ? TRUE : FALSE);
}

/*==========================================================================*
 * FUNCTION : *GetBTResult
 * PURPOSE  : assisitant function
 * CALLS    : 
 * CALLED BY: BattTestLogFormatProcess
 * ARGUMENTS: int  iResultCode : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 11:45
 *==========================================================================*/
__INLINE static char *GetBTResult(int iResultCode)
{
	switch (iResultCode)
	{
	case RESULT_NONE:
		return "No test result";

	case RESULT_GOOD:
		return "Battery is OK";

	case RESULT_BAD:
		return "Battery is bad";

	case RESULT_POWER_SPLIT:
		return "It's a Power Split Test";

	default:
		return "Not defined test result";
	}
}


/*==========================================================================*
 * FUNCTION : *GetBTStartReason
 * PURPOSE  : assisitant function
 * CALLS    : 
 * CALLED BY: BattTestLogFormatProcess
 * ARGUMENTS: int  iReasonCode : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 11:45
 *==========================================================================*/
__INLINE static char *GetBTStartReason(int iReasonCode)
{
	switch (iReasonCode)
	{
	case START_PLANNED_TEST:
		return "Planned Test";

	case START_MANUAL_TEST:
		return "Manual Test";

	case START_AC_FAIL_TEST:
		return "AC Failure Test";

	case START_MASTER_TEST:
		return "Power Splitter Test";

	default:
		return "Undefined Test";
	}
}


/*==========================================================================*
 * FUNCTION : *GetBTEndReason
 * PURPOSE  : assisitant function
 * CALLS    : 
 * CALLED BY: BattTestLogFormatProcess
 * ARGUMENTS: int  iReasonCode : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 11:45
 *==========================================================================*/
__INLINE static char *GetBTEndReason(int iReasonCode)
{
	switch (iReasonCode)
	{
	case END_MANUAL:
		return "Stop test manually";

	case END_ALARM:			
		return "Stop test for alarm";

	case END_TIME:			
		return "Stop test for test time-out";

	case END_CAPACITY:		
		return "Stop test for capacity condition";

	case END_VOLTAGE:		
		return "Stop test for voltage condition";

	case END_AC_FAIL:		
		return "Stop test for AC fail";

	case END_AC_RESTORE:		
		return "Stop AC fail test for AC restore";

	case END_AC_FAIL_TEST_DIS:
		return "Stop AC fail test for disabled";

	case END_MASTER_STOP:	
		return "Stop test for Master Power stop test";

	case END_TURN_TO_MAN:	
		return "Stop a PowerSplit BT for Auto/Man turn to Man";

	case END_TURN_TO_AUTO:	
		return "Stop PowerSplit Man-BT for Auto/Man turn to Auto";

	default:
		return "Undefined Reason";
	}
}


/*==========================================================================*
 * FUNCTION : BattTestLogFormatProcess
 * PURPOSE  : 
 * CALLS    : GetBTResult
 *			  GetBTStartReason
 *			  GetBTEndReason
 * CALLED BY: ReadBattTestLogToBuf
 * ARGUMENTS: int              iBattLogID   : 
 *            BT_LOG_SUMMARY   *pSummary    : 
 *            BT_LOG_BATT_CFG  *pLogBattCfg : 
 *            char             *sLogDetail  : 
 *            char             **pszBattLog : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 11:44
 *==========================================================================*/
static BOOL BattTestLogFormatProcess(int iBattLogID,
									 BT_LOG_SUMMARY *pSummary,
									 BT_LOG_BATT_CFG *pLogBattCfg,
									 char *sLogDetail,
									 char **pszBattLog)
{
	LANG_TEXT *pLangInfo;
	int nBufLen;

	int i, iDstOffset, iSrcOffset, nError;
	float fCapacity, fRatedCap;
	BT_LOG_BATT_CFG *pCurLogBattCfg;

	char strStartTime[20], strEndTime[20];
#define BT_TIME_FORMAT  "%Y-%m-%d %H:%M:%S"

    /* the memory will be deleted after report */
	*pszBattLog = NEW(char, ESR_BATT_TESTLOG_SIZE);
	if (*pszBattLog == NULL)
	{
		LOG_ESR_CMD_E("no memory for read battery test log");
		return FALSE;
	}
	*pszBattLog[0] = '\0';

	/* 1.write site name and ccid */
	pLangInfo = NULL;
	nError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		SITE_NAME,			
		0,		
		&nBufLen,			
		&pLangInfo,			
		0);

	if (nError != ERR_DXI_OK)
	{
		DELETE(*pszBattLog);
		return FALSE;
	}

	iDstOffset = sprintf(*pszBattLog, "Site: %s\tCCID: %d\n", 
		pLangInfo->pFullName[0], ESR_GetCCID());

	/* 2.write log summary info */
	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Battery Test Log NO: %d\t"
		"Test Result: %s\n", iBattLogID, GetBTResult(pSummary->iTestResult));

	TimeToString(pSummary->tStartTime, BT_TIME_FORMAT, 
		strStartTime, sizeof(strStartTime));
	TimeToString(pSummary->tEndTime, BT_TIME_FORMAT, 
		strEndTime, sizeof(strEndTime));
	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Test Start Time: %s\n"
		"Test End Time: %s\n", strStartTime, strEndTime);

	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Test Start Reason: %s\n", 
			GetBTStartReason(pSummary->iStartReason));
	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Test End Reason: %s\n", 
			GetBTEndReason(pSummary->iEndReason));

 	iDstOffset += sprintf(*pszBattLog + iDstOffset, "Quantity of Battery "
 			"Strings: %d\n", pSummary->iQtyBatt);

	/* 3.write log detail data(only write capacity) */
	iDstOffset += sprintf(*pszBattLog + iDstOffset, "\nRemained Capacity:\n");
	fRatedCap = pSummary->fBattRatedCap;
	pCurLogBattCfg = pLogBattCfg;

	//printf("iTempNumber is %d iQtyRecord is %d iSizeRecord is %d\n", pCurLogBattCfg->iTempNumber, pSummary->iQtyRecord, pSummary->iSizeRecord);
	iSrcOffset = 8 + 4 * pCurLogBattCfg->iTempNumber + (pSummary->iQtyRecord-1) * pSummary->iSizeRecord;
	/*for(i = 0; i < 143; i++)
	{
	    printf("%d, %f\n", i, *(float *)(sLogDetail + iSrcOffset + i * 4));
	}*/
 	for (i = 0; i < pSummary->iQtyBatt; i++, pCurLogBattCfg++)
	{
#ifdef _TEST_ESR_BATTLOG
		TRACE("\n\n\n\n\nCapcity configured is %d\n", BATT_CFGRPOP_TEST(pCurLogBattCfg, BATT_CFGPROT_CAP));
#endif //_TEST_ESR_BATTLOG

		if (BATT_CFGRPOP_TEST(pCurLogBattCfg, BATT_CFGPROT_CAP))
		{
			fCapacity = *(float *)(sLogDetail + iSrcOffset +
				pCurLogBattCfg->iOffsetCap);
			iDstOffset += sprintf(*pszBattLog + iDstOffset, "\tBattery String"
				"%d: %.1fAh (%.1f%c)\n", i+1, fCapacity, 
				fCapacity/fRatedCap*100, '\x25');
			//printf("iSrcOffset is %d pCurLogBattCfg->iOffsetCap is %d fCapacity is %f\n", iSrcOffset, pCurLogBattCfg->iOffsetCap, fCapacity);
			iSrcOffset += 12;//����bug
		}

		//iSrcOffset += pCurLogBattCfg->iOffsetTotal;
		//iSrcOffset += 12;//����bug
	}
 
	return TRUE;
}



/*==========================================================================*
 * FUNCTION : ReadBattTestLogToBuf
 * PURPOSE  : 
 * CALLS    : BattTestLogFormatProcess
 * CALLED BY: 
 * ARGUMENTS: IN int    iBattLogID   : 
 *            OUT char  **pszBattLog : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 11:44
 *==========================================================================*/
static BOOL ReadBattTestLogToBuf(IN int iLogID, OUT char **pszBattLog)
{
	HANDLE	hTestLog;
	int sRecordNoBuff[ESR_BATT_TESTLOG_NUM];
	int  iBattLogID, iStartNo, iRet, iRecords;
	BOOL bRet;

	BT_LOG_SUMMARY *pLogSummary,stLogSummary;
	BT_LOG_BATT_CFG *pLogBattCfg, *pCurLogBattCfg; 
	int i, j, k, iQtyBatt;
	char SummaryRecordBuf[256], CfgRecordBuf[1024];//CfgRecordBuf��С��256��Ϊ1024
	char *sBTLogDetail;

	iBattLogID = iLogID;

	/* 1.open the storage */
	hTestLog = DAT_StorageOpen(BATT_TEST_LOG_FILE);
	if (hTestLog == NULL)
	{
		LOG_ESR_CMD_E("Open Battery Test Log storage failed");
		return FALSE;
	}

	/* 2.search the record no */
	iStartNo = -1;
	iRet = DAT_StorageSearchRecords(hTestLog, 
		ESR_CompareProc, 
		NULL,
		&iStartNo,           //search from the last record
		&iBattLogID, 
		sRecordNoBuff, 
		FALSE,
		FALSE,
		TRUE);

	if (!iRet || iBattLogID != iLogID || iBattLogID <= 1)
	{
		DAT_StorageClose(hTestLog);

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Seach Battery Log failed");
		TRACE("Log No. is: %d\n", iBattLogID);
		TRACE("iRet value is: %d\n", iRet);
#endif //_DEBUG_ESR_CMD_HANDLER

		return FALSE;
	}


#ifdef _TEST_ESR_BATTLOG
	{
		int ii;
		TRACE("\n\n\n\n\n\nBattLog Head Record No. Info:\n");
		for (ii = 0; ii < iBattLogID; ii++)
		{
			TRACE("Record No. of Battery Log %d is: %d\n", 
				ii + 1, sRecordNoBuff[ii]);
		}
	}
#endif //_TEST_ESR_BATTLOG


	/* 3.read the test log summary info */
	iRecords = 1;  
	iStartNo = sRecordNoBuff[iBattLogID-1];
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	iRet = DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, SummaryRecordBuf, FALSE, TRUE);
	//pLogSummary = (BT_LOG_SUMMARY *)SummaryRecordBuf + 64;
	memcpy(&stLogSummary,SummaryRecordBuf + 64, sizeof(BT_LOG_SUMMARY));
	pLogSummary = &stLogSummary;
	
	if (iRet == FALSE)
	{
		DAT_StorageClose(hTestLog);

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Read Battery Test Log Summary Info failed");
		TRACE("Log No. is: %d\n", iBattLogID);
#endif //_DEBUG_ESR_CMD_HANDLER

		return FALSE;
	}
	iQtyBatt = pLogSummary->iQtyBatt;

#ifdef _TEST_ESR_BATTLOG
	{
		char strStartTime[20], strEndTime[20];
#define BT_TIME_FORMAT  "%Y-%m-%d %H:%M:%S"
		TimeToString(pLogSummary->tStartTime, BT_TIME_FORMAT, 
			strStartTime, sizeof(strStartTime));
		TimeToString(pLogSummary->tEndTime, BT_TIME_FORMAT, 
			strEndTime, sizeof(strEndTime));
		TRACE_ESR_TIPS("Read Battery Test Log Summary Info OK");
		TRACE("Summary Info:\n");
		TRACE("Test Start Time:%s\nTest End Time:%s\n",  strStartTime, strEndTime);
		TRACE("Test Start Reason: %d\nTest End Reason: %d\n", 
			pLogSummary->iStartReason, pLogSummary->iEndReason);
		TRACE("Test Result: %d\n", pLogSummary->iTestResult);
		TRACE("Rated Capacity: %f\n", pLogSummary->fBattRatedCap);
		TRACE("Battery String quentity: %d\n", pLogSummary->iQtyBatt);
		TRACE("Battery Test Record quentity: %d\n", pLogSummary->iQtyRecord);
		TRACE("Record Size: %d\n", pLogSummary->iSizeRecord);
		
	}
#endif //_TEST_ESR_BATTLOG



	/* 4.read battery string config info */
	iRecords = 4;// ��Ϊ��4��iRecords
	iStartNo = sRecordNoBuff[iBattLogID-1] + 1;
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	iRet = DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, CfgRecordBuf, FALSE, TRUE);
	if (iRet == FALSE)
	{
		DAT_StorageClose(hTestLog);

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Read Battery Test Log Config Info failed");
		TRACE("Log No. is: %d\n", iBattLogID);
#endif //_DEBUG_ESR_CMD_HANDLER

		return FALSE;
	}
	pLogBattCfg = NEW(BT_LOG_BATT_CFG, iQtyBatt);

#ifdef _TEST_ESR_BATTLOG
	TRACE("\n\n\n\n\nBatt Test Config Info:\n String 1 Capa %d\n "
		"String 2 capa: %d\n", CfgRecordBuf[2], CfgRecordBuf[7]);
#endif //_TEST_ESR_BATTLOG

	if (pLogBattCfg == NULL)
	{
		LOG_ESR_CMD_E("No memory for read battery test log");
		DAT_StorageClose(hTestLog);
		return FALSE;
	}
	ZERO_POBJS(pLogBattCfg, iQtyBatt);

	
//for(i = 0; i < 1024; i++)
//{
//    printf("%d", CfgRecordBuf[i]);
//}
//printf("\n");
	pCurLogBattCfg = pLogBattCfg;  
	for(j = 0; j < 7; j++)
	{
		if (CfgRecordBuf[j] == 1)
		{
			pCurLogBattCfg->iTempNumber++;
		}
	}
	for (i = 0, j = 7, k = 0; i < iQtyBatt; i++, pCurLogBattCfg++)
	{
		if (CfgRecordBuf[j] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_CUR);
			pCurLogBattCfg->iOffsetCur = 0;
			k++;
		}
		if (CfgRecordBuf[j + 1] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_VOL);
			pCurLogBattCfg->iOffsetVol = 4 * k;
			k++;
		}
		if (CfgRecordBuf[j + 2] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_CAP);
			pCurLogBattCfg->iOffsetCap = 4 * k;
			k++;
		}
		/*if (CfgRecordBuf[j + 3] == 1)
		{
			BATT_CFGRPOP_SET(pCurLogBattCfg, BATT_CFGPROT_TEMP);
			pCurLogBattCfg->iOffsetTemp = 4 * k;
			k++;
		}*/

	/*	pCurLogBattCfg->iQtyBlockVol = CfgRecordBuf[j + 4];
		pCurLogBattCfg->iOffsetBlockVol = 4 * k;

		k = 0;
		j += 5;*/
		//printf("%d iPropFlag is %d\n", i, pCurLogBattCfg->iPropFlag);
		k = 0;
		j +=3;
	}


	/* 5.read data */
	iRecords = (pLogSummary->iQtyRecord * pLogSummary->iSizeRecord - 1)/256 + 1;
	iStartNo = sRecordNoBuff[iBattLogID - 1] + 5;//�ӵ�5��iRecords��ʼ��
	sBTLogDetail = NEW(char, iRecords*256);


#ifdef _TEST_ESR_BATTLOG
	TRACE("\n\n\n\n\n\niRecords value is: %d\n", iRecords);
	TRACE("Detail Data buffer is: %d\n\n\n\n", iRecords*128);
#endif //_TEST_ESR_BATTLOG



	if (sBTLogDetail == NULL)
	{
		LOG_ESR_CMD_E("no memory for read battery test log");
		DELETE(pLogBattCfg);
		DAT_StorageClose(hTestLog);
		return FALSE;
	}
	DAT_RecoverReadHandle(&hTestLog);  //recover the handle
	if (DAT_StorageReadRecords(hTestLog, &iStartNo,
		&iRecords, sBTLogDetail, FALSE, TRUE) == FALSE)
	{
		DELETE(pLogBattCfg);
		DELETE(sBTLogDetail);

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Read Battery Test Log Detail Info failed");
		TRACE("Log No. is: %d\n", iBattLogID);
#endif //_DEBUG_ESR_CMD_HANDLER

		DAT_StorageClose(hTestLog);
		return FALSE;
	}

	/* 6.convert to the EEM format */
	bRet = BattTestLogFormatProcess(iBattLogID, pLogSummary, 
			pLogBattCfg, sBTLogDetail, pszBattLog);

	/* clear */
	DELETE(pLogBattCfg);
	DELETE(sBTLogDetail);
	DAT_StorageClose(hTestLog);

	if (!bRet)
	{
		return FALSE;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : GetBattTestLogFromBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ExecuteRL
 * ARGUMENTS: int   iSequenceID : 
 *            char  *szOut      : the length of the array is
*								  ESR_BATT_TESTLOG_SEG + 2
 *            char  *szBTLog    : battery test log buffer
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 14:05
 *==========================================================================*/
static int GetBattTestLogFromBuf(int iSequenceID, char *szOut, char *szBTLog)
{
	int iLogSize;

	/* buffer has not perpared */
	if (szBTLog == NULL)
	{
		strcpy(szOut, "*");
		return 0;
	}

	iLogSize = (int)strlen(szBTLog);


	if (iSequenceID > (iLogSize-1)/ESR_BATT_TESTLOG_SEG || iLogSize == 0)
	{
		strcpy(szOut, "*");
		return 0;
	}

	strncpy(szOut, szBTLog + iSequenceID * ESR_BATT_TESTLOG_SEG, 
		ESR_BATT_TESTLOG_SEG);
	strcat(szOut, "*");

	return (int)strlen(szOut) - 1;
}


/*==========================================================================*
 * FUNCTION : ExecuteRL
 * PURPOSE  : RL, Read battery test log
 * CALLS    : ReadBattTestLogToBuf
 *			  GetBattTestLogFromBuf
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-28 11:00
 *==========================================================================*/
static void ExecuteRL(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	unsigned char c;
	unsigned char LogSeg[ESR_BATT_TESTLOG_SEG + 2]; //add '*' and NULL
	int iBattLogID, iSequenceID;
	BOOL bRet;
	size_t stLength;

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 6)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'L' || szInData[5] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


	/* 2.get param and check it */
	if (!ESR_IsStrAsciiHex(szInData + 2, 3))
	{
		strcpy(szOutData, "ERR1-RL*");
		return;
	}

	/* only support 10 battery test log */
	c = szInData[2];
	//start from 000(based on TR), Thomas, 2005-7-15
	if (c >= '0' && c <= '9')  
	{
		iBattLogID = c - '0' + 1;
	}
	/*else if (c == 'A' || c == 'a')
	{
		iBattLogID = 10;
	}*/
	else
	{
		strcpy(szOutData, "ERR1-RL*");
		return;
	}

	iSequenceID = (int)ESR_AsciiHexToChar(szInData + 3);


	/* 3.prepare battery test log buffer */
	if (iSequenceID == 0)  //only RLx00* need prepare the buffer
	{
		/* ensure the buffer is free */
		if (pThis->szBattLog != NULL)
		{
			DELETE(pThis->szBattLog);
			pThis->szBattLog = NULL;
		}

		bRet = ReadBattTestLogToBuf(iBattLogID, &pThis->szBattLog);

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Read Battery Test Log OK!");
		TRACE("Log No. is: %d\n", iBattLogID);
#endif //_DEBUG_ESR_CMD_HANDLER
	}

	/* 4.get the log */
	if (0 == GetBattTestLogFromBuf(iSequenceID, LogSeg, pThis->szBattLog))
	{
		DELETE(pThis->szBattLog);
		pThis->szBattLog = NULL;
	}

	/* 5.reply the command */
	sprintf(szOutData, "%c%c%c#%s", szInData[2], 
		szInData[3], szInData[4], LogSeg);

#ifdef _DEBUG_ESR_CMD_HANDLER
	TRACE_ESR_TIPS("RL* has been excuted ok!");
	TRACE("Reply Data:\n");
	TRACE("\t%s\n", szOutData);
#endif //_DEBUG_ESR_CMD_HANDLER

	return;
}


/*==========================================================================*
 * FUNCTION : *GetEEMAlarmLevelText
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  *piACUAlarmLevel : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 14:57
 *==========================================================================*/
__INLINE static char *GetEEMAlarmLevelText(int *piACUAlarmLevel)
{
	if (piACUAlarmLevel != NULL)
	{
		switch (*piACUAlarmLevel)
		{
		case ALARM_LEVEL_CRITICAL:
			return "A1";		/* A1 */

		case ALARM_LEVEL_MAJOR:
			return "A2";		/* A2 */

		case ALARM_LEVEL_OBSERVATION:
			return "O1";		/* O1 */

		default:  //error
			LOG_ESR_CMD_E("New ACU alarm level used, which can not be handled");
		}
	}

	return "Undefined";

}



/*==========================================================================*
 * FUNCTION : ReadADTexts
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int             iSequenceID : 
 *            OUT unsigned char  *szOutData  : 
 * RETURN   :  void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 14:55
 *==========================================================================*/
static void ReadADTexts(IN int iSequenceID, 
						OUT unsigned char *szOutData)
{
	int i, iPos, iRefTexts;
	ESR_REF_TEXT *pRefText;
	int iOffSet;
	time_t tmTime;
	char szTime[13];
	
	/* reply format: "RX00 #00!text1!text2 #01!text1!text2...#<timestamp>*" */
    iPos = 10 * iSequenceID;
	iRefTexts = g_EsrGlobals.iRefTexts;
	pRefText = g_EsrGlobals.RefTexts + iPos;

	/* 1.for head, eg: RX01 */
	sprintf(szOutData, "RX%02X", iSequenceID);
	iOffSet = 4;

	/* 2.read texts one by one */
	for (i = 0; i < 10 && iPos < iRefTexts; i++, iPos++, pRefText++)
	{
		/* alarm level text handle */
		if (pRefText->iRefNo == 17 || pRefText->iRefNo == 18)
		{
			sprintf(szOutData + iOffSet, "#%02X!%s!", 
				iPos, GetEEMAlarmLevelText(pRefText->piAlarmLevel));
		}
		else
		{
			sprintf(szOutData + iOffSet, "#%02X!%s!%s", 
				iPos, pRefText->szText1, pRefText->szText2);
		}

		iOffSet = strlen(szOutData);
	}

	/* 3.add <Time stamp> */
	if (i != 0)
	{
		tmTime = time(&tmTime);
		sprintf(szOutData + iOffSet, "#%s*",
			TimeToString(tmTime, EEM_TIME_FORMAT, szTime, sizeof (szTime)));
	}
	else
	{
		sprintf(szOutData + iOffSet, "*");
	}


	return;
}


/*==========================================================================*
 * FUNCTION : ExecuteRX
 * PURPOSE  : RX, read AD text
 * CALLS    : ReadRefTexts
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 13:38
 *==========================================================================*/
static void ExecuteRX(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	size_t stLength;
	int iSequenceID;

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 5)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'X' || szInData[4] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


	/* 2.get param and check it */
	if (!ESR_IsStrAsciiHex(szInData + 2, 2))
	{
		strcpy(szOutData, "ERR1-RX*");
		return;
	}

	iSequenceID = (int)ESR_AsciiHexToChar(szInData + 2);


	/* test RL command */
#ifdef _TEST_ESR_BATTLOG
	{		
	sprintf(szInData, "RL%01X%02X*", iBattLogID, iSequenceID);
	return ExecuteRL(pThis, szInData, szOutData);
	}
#endif //_TEST_ESR_BATTLOG



	/* 3.get texts */
	ReadADTexts(iSequenceID, szOutData);

	return;
}



/*==========================================================================*
 * FUNCTION : HasLinkedToRefText
 * PURPOSE  : ExecuteRR
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iModuleNo : 
 *            int  iRefNo    : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 15:37
 *==========================================================================*/
__INLINE static BOOL HasLinkedToRefText(int iModuleNo, int iRefNo)
{
	int i, iRefTexts;
	ESR_REF_TEXT *pRefTexts;

	iRefTexts = g_EsrGlobals.iRefTexts;
	pRefTexts = g_EsrGlobals.RefTexts;

	for (i = 0; i < iRefTexts; i++, pRefTexts++)
	{
		if (pRefTexts->iModuleNo == iModuleNo &&
			pRefTexts->iRefNo == iRefNo)
		{
			return TRUE;
		}
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : ExecuteRR
 * PURPOSE  : RR, Read AD reference
 * CALLS    : HasLinkedToRefText
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 14:57
 *==========================================================================*/
static void ExecuteRR(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	size_t stLength;
	int iModuleNo, iRefNo, iTextNo, iOffSet;
	time_t tmTime;
	char szTime[13];

	UNUSED(pThis);

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 3)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'R' || szInData[1] != 'R' || szInData[2] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* 2.reply the command */
	sprintf(szOutData, "RR");
	iOffSet = 2;

	iTextNo = 0;
	for (iModuleNo = 1; iModuleNo <= MAX_SMIO_UNIT; iModuleNo++)
	{
		for (iRefNo = 1; iRefNo <= SMIO_REF_TEXTS_NUM; iRefNo++)
		{
			if (HasLinkedToRefText(iModuleNo, iRefNo))
			{
				sprintf(szOutData + iOffSet, "%02X", iTextNo++);
			}
			else
			{
				/* if has no text assigned, fill "FF" */
				sprintf(szOutData + iOffSet, "FF");
			}

			iOffSet += 2;
		}
	}

	/* add <time stamp> */
	tmTime = time(&tmTime);
	sprintf(szOutData + iOffSet, "!%s*",
		TimeToString(tmTime, EEM_TIME_FORMAT, szTime, sizeof(szTime)));


	return;
}



/*==========================================================================*
 * FUNCTION : ExecuteWR
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 15:43
 *==========================================================================*/
static void ExecuteWR(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	UNUSED(pThis);
	UNUSED(szInData);
	UNUSED(szOutData);

	/* not support by ACU */
	strcpy(szOutData, "ERR-CMD*");
	return;

}


/*==========================================================================*
 * FUNCTION : ExecuteWX
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2005-01-19 15:43
 *==========================================================================*/
static void ExecuteWX(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
	UNUSED(pThis);
	UNUSED(szInData);
	UNUSED(szOutData);

	/* not support by ACU */
	strcpy(szOutData, "ERR-CMD*");
	return;

}


/* Added by Thomas begin, support product info, 2006-2-9 */
#ifdef PRODUCT_INFO_SUPPORT
/*==========================================================================*
 * FUNCTION : GetUnitNumByGroupID
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iGroupID : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Lintao                   DATE: 2006-2-9 16:20
 *==========================================================================*/
static int GetUnitNumByGroupID(int iGroupID)
{
	int i, iTypeMapNum;
	TYPE_MAP_INFO *pTypeMap;

	pTypeMap = g_EsrGlobals.EEMModelConfig.pTypeMapInfo;
	iTypeMapNum = g_EsrGlobals.EEMModelConfig.iTypeMapNum;

	for (i = 0; i < iTypeMapNum; i++, pTypeMap++)
	{
		if (pTypeMap->iGroupID == iGroupID)
		{
			return pTypeMap->iUnitNum;
		}
	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : IsBlockRelativeDevice
 * PURPOSE  : Aided function, judge if a device relative to a block
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: EEM_BLOCK_INFO  *pBlock   : 
 *            int             iDeviceID : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Lintao                   DATE: 2006-2-9 15:40
 *==========================================================================*/
static BOOL IsBlockRelativeDevice(EEM_BLOCK_INFO *pBlock, int iDeviceID)
{
	int i;

	if (pBlock)
	{
		for (i = 0; i < pBlock->iRelatedDeviceNum; i++)
		{
			if (iDeviceID == pBlock->piRelatedDeviceIndex[i])
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : AllocateDeviceToGroup
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2006-2-9 10:17
 *==========================================================================*/
static void AllocateDeviceToGroup(void)
{
	int i, j, k, iGroupID, iUnitID;
	int iBlockNum, iDeviceNum, iUnitNum;
	EEM_BLOCK_INFO *pBlock, *pUnitBlock, *pSysBlock;

	iDeviceNum = DXI_GetDeviceNum();

	//get System level block
	pSysBlock = GetBlock(0, 0);
	/* scu board has been taken as the ACU device, not like PSC.
	 * so can delete it here. Thomas, 2006-2-16 */
	//pSysBlock->iRelatedDeviceNum = 1;
	//pSysBlock->piRelatedDeviceIndex[0] = 1;  //PSC device

	//go through the Group level block
	pBlock = g_EsrGlobals.EEMModelInfo.pEEMBlockInfo;
	iBlockNum = g_EsrGlobals.EEMModelInfo.iBlocksNum;
	for (i = 0; i < iBlockNum; i++, pBlock++)
	{
		iGroupID = pBlock->iGroupID;
		iUnitID = pBlock->iUnitID;

		//for Group Level block, reply the devices related to its units
		if (iGroupID != 0 && iUnitID == 0)
		{
			pBlock->iRelatedDeviceNum = 0;
			iUnitNum = GetUnitNumByGroupID(iGroupID);

			for (j = 1; j <= iDeviceNum; j++)  //Device ID started from 1
			{
				for (k = 1; k <= iUnitNum; k++) //Unit ID started from 1
				{
				    /* Rect number dynamically recognization,
				     * if Rect block is not pushed, ruturn NULL
					 */
					pUnitBlock = GetBlock(iGroupID, k);
					if (IsBlockRelativeDevice(pUnitBlock, j))
					{
						//find!
						/* check if has been added already, Thomas, 2006-2-16 */
						if (!IsBlockRelativeDevice(pBlock, j))
						{
							pBlock->piRelatedDeviceIndex[pBlock->iRelatedDeviceNum] = j;
							pBlock->iRelatedDeviceNum++;
						}

						//attach to System level block
						/* check if has been added already, Thomas, 2006-2-16 */
						if (!IsBlockRelativeDevice(pSysBlock, j))
						{
							pSysBlock->piRelatedDeviceIndex[pSysBlock->iRelatedDeviceNum] = j;
							pSysBlock->iRelatedDeviceNum++;
						}
					}
				}
			}

		}
	}

	qsort(pSysBlock->piRelatedDeviceIndex,
		(size_t)pSysBlock->iRelatedDeviceNum,
		sizeof(int), 
		(int (*)(const void *, const void *))CompareDeviceIDs); 

	return;
}
#endif

/*==========================================================================*
 * FUNCTION : ExecuteDL
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2006-2-10 9:10
 *==========================================================================*/
static void ExecuteDL(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{
#ifdef PRODUCT_INFO_SUPPORT
	UNUSED(pThis);

	size_t  stLength;
	int iGroupID, iUnitID;
	EEM_BLOCK_INFO *pBlock;
	char logText[ESR_LOG_TEXT_LEN];

	int  i, iPos;

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 7)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'D' || szInData[1] != 'L' || szInData[6] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	/* attach the Device to Group/System block -- refreash it! */
	AllocateDeviceToGroup();

	/* 2.get the block */
	iGroupID = (int)ESR_AsciiHexToChar(szInData + 2);
	iUnitID = (int)ESR_AsciiHexToChar(szInData + 4);

	pBlock = GetBlock(iGroupID, iUnitID);

	if (pBlock == NULL)
	{
		sprintf(logText, "Block(GroupID:%d, UnitID:%d) not exits in system",
			iGroupID, iUnitID);
		LOG_ESR_CMD_I(logText);

		strcpy(szOutData, "ERR1-DL*");
		return;
	}


    /* 3. reply the command */
	sprintf(szOutData, "%02X%02X", iGroupID, iUnitID);

	for (iPos = 4, i = 0; i < pBlock->iRelatedDeviceNum; i++)
	{
		sprintf(szOutData + iPos, "!%04X", pBlock->piRelatedDeviceIndex[i]);
		iPos += 5;
	}

	sprintf(szOutData + iPos, "*");
	return;

#else  //PRODUCT_INFO_SUPPORT
	UNUSED(pThis);
	UNUSED(szInData);
	UNUSED(szOutData);

	/* not support by ACU */
	strcpy(szOutData, "ERR-CMD*");
	return;
#endif //PRODUCT_INFO_SUPPORT

}

#ifdef PRODUCT_INFO_SUPPORT
/*==========================================================================*
 * FUNCTION : GetDeviceIDFromStr
 * PURPOSE  : get value from 4 ascii hex. eg: "121A" = 1*16*16*16 + 2*16*16
 *            + 1*16+10 = 4634
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: *pStr : four char buffer, shall be checked out of the func
 * RETURN   : int : 
 * COMMENTS : assistant function
 * CREATOR  : Lintao                   DATE: 2006-2-10 9:02
 *==========================================================================*/
static int GetDeviceIDFromStr(const unsigned char *pStr)
{
	int  iRet;
	BYTE byHiValue;

	//ESR_AsciiHexToChar function will process 2 char Hex.
	byHiValue = ESR_AsciiHexToChar(pStr);
	iRet = byHiValue*0x100 + ESR_AsciiHexToChar(pStr + 2);

	return iRet;
}
#endif //PRODUCT_INFO_SUPPORT


/*==========================================================================*
 * FUNCTION : ExecuteDP
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS       *pThis     : 
 *            const unsigned char  *szInData  : 
 *            unsigned char        *szOutData : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2006-2-10 9:23
 *==========================================================================*/
static void ExecuteDP(ESR_BASIC_ARGS *pThis,
					  const unsigned char *szInData,
					  unsigned char *szOutData)
{	
#ifdef PRODUCT_INFO_SUPPORT
	UNUSED(pThis);

	int iDeviceID, iGroupID, iSubGroupID, iPos = 0;
	char szDeviceID[6];  //for reply
	EEM_BLOCK_INFO *pSysBlock;
	PRODUCT_INFO sProductInfo;
	DEVICE_INFO	 *pDeviceInfo;  //to get related equip info
	size_t stLength;

	memset(&sProductInfo, 0, sizeof(sProductInfo));

	/* 1.check the command */
	stLength = strlen(szInData);
	if (stLength != 7)
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}

	if (szInData[0] != 'D' || szInData[1] != 'P' || szInData[6] != '*')
	{
		strcpy(szOutData, "ERR-CMD*");
		return;
	}


	/* 2.get param and check it */
	if (!ESR_IsStrAsciiHex(szInData + 2, 4))
	{
		strcpy(szOutData, "ERR1-DP*");
		return;
	}
	iDeviceID = GetDeviceIDFromStr(szInData + 2);


	// attach the Device to Group/System block -- refreash it!
	AllocateDeviceToGroup();
	
	if (!IsBlockRelativeDevice(GetBlock(0, 0), iDeviceID))
	{
		//invalid Device ID
		strcpy(szOutData, "ERR1-DP*");
		return;
	}

	pDeviceInfo = DXI_GetDeviceByID(iDeviceID);
	if (!pDeviceInfo)
	{
        //invalid Device ID
		strcpy(szOutData, "ERR1-DP*");
		return;
	}


	/* 3.retrive the Product Info */
	iGroupID = 0;
	iSubGroupID = 0;
	if (pDeviceInfo->nRelatedEquip)
	{
		EEM_BLOCK_INFO *pBlock;
		pBlock = GetBlockByEquipID(pDeviceInfo->ppRelatedEquip[0]->iEquipID);

		iGroupID = pBlock->iGroupID;
		iSubGroupID = pBlock->iSubGroupID;
	}
	

	if (DXI_GetPIByDeviceID(iDeviceID, &sProductInfo) != ERR_DXI_OK)
	{
		//simply log it
		LOG_ESR_CMD_E("Call DXI interface to get Product "
			"Info failed");
	}

	

    //Device ID field
	strncpyz(szDeviceID, szInData + 2, 5);
	szDeviceID[4] = '!';
	szDeviceID[5] = '\0';

	//GroupID & SubGroupID
	sprintf(szOutData, "%s#01%02X#02%1X", szDeviceID, iGroupID, iSubGroupID);
	iPos += 14;

	//Part number
	sprintf(szOutData + iPos, "#03%s", sProductInfo.szPartNumber);
	iPos += 3 + strlen(sProductInfo.szPartNumber);  

	//serial number
	sprintf(szOutData + iPos, "#04%s", sProductInfo.szSerialNumber);
	iPos += 3 + strlen(sProductInfo.szSerialNumber);  

	//HW version
	sprintf(szOutData + iPos, "#05%s", sProductInfo.szHWVersion);
	iPos += 3 + strlen(sProductInfo.szHWVersion);  

	//SW version
 	sprintf(szOutData + iPos, "#06%s", sProductInfo.szSWVersion);
	iPos += 3 + strlen(sProductInfo.szSWVersion);  


	sprintf(szOutData + iPos, "*");
	return;


#else  //PRODUCT_INFO_SUPPORT
	UNUSED(pThis);
	UNUSED(szInData);
	UNUSED(szOutData);

	/* not support by ACU */
	strcpy(szOutData, "ERR-CMD*");
	return;
#endif //PRODUCT_INFO_SUPPORT

}
/* Added by Thomas end, support product info, 2006-2-9 */


/*==========================================================================*
 * FUNCTION : ESR_InitCmdHander
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: InitEsrGlobals
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-29 14:39
 *==========================================================================*/
void ESR_InitCmdHander()
{
	CMD_HANDLER *pCmdHandlers;

	pCmdHandlers = g_EsrGlobals.CmdHandlers;

	DEF_CMD_HANDLER(&pCmdHandlers[0], "RI", FALSE, ExecuteRI);
	DEF_CMD_HANDLER(&pCmdHandlers[1], "RB", FALSE, ExecuteRB);
	DEF_CMD_HANDLER(&pCmdHandlers[2], "WB", TRUE,  ExecuteWB);
	DEF_CMD_HANDLER(&pCmdHandlers[3], "RN", FALSE, ExecuteRN);
	DEF_CMD_HANDLER(&pCmdHandlers[4], "SN", TRUE,  ExecuteSN);

	DEF_CMD_HANDLER(&pCmdHandlers[5], "RT", FALSE, ExecuteRT);
	DEF_CMD_HANDLER(&pCmdHandlers[6], "ST", TRUE,  ExecuteST);
	DEF_CMD_HANDLER(&pCmdHandlers[7], "CR", TRUE,  ExecuteCR);
	DEF_CMD_HANDLER(&pCmdHandlers[8], "RP", FALSE, ExecuteRP);
	DEF_CMD_HANDLER(&pCmdHandlers[9], "WP", TRUE,  ExecuteWP);

	DEF_CMD_HANDLER(&pCmdHandlers[10], "CB", FALSE, ExecuteCB);
	DEF_CMD_HANDLER(&pCmdHandlers[11], "RA", FALSE, ExecuteRA);
	DEF_CMD_HANDLER(&pCmdHandlers[12], "RC", FALSE, ExecuteRC);
	DEF_CMD_HANDLER(&pCmdHandlers[13], "RH", FALSE, ExecuteRH);
	DEF_CMD_HANDLER(&pCmdHandlers[14], "CH", TRUE,  ExecuteCH);

	DEF_CMD_HANDLER(&pCmdHandlers[15], "RL", FALSE, ExecuteRL);
	DEF_CMD_HANDLER(&pCmdHandlers[16], "RX", FALSE, ExecuteRX);
	DEF_CMD_HANDLER(&pCmdHandlers[17], "RR", FALSE, ExecuteRR);

	/* not support for keeping mul-language consistance */
	DEF_CMD_HANDLER(&pCmdHandlers[18], "WR", TRUE,  ExecuteWR);
	DEF_CMD_HANDLER(&pCmdHandlers[19], "WX", TRUE,  ExecuteWX);


	/* Added by Thomas begin, support product info, 2006-2-10 */
	DEF_CMD_HANDLER(&pCmdHandlers[20], "DL", FALSE, ExecuteDL);
	DEF_CMD_HANDLER(&pCmdHandlers[21], "DP", FALSE, ExecuteDP);
	/* Added by Thomas end, support product info, 2006-2-10 */


	return;
}


/*==========================================================================*
 * FUNCTION : *GetCmdHandler
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ESR_DecodeAndPerform
 * ARGUMENTS: const unsigned char *  szCmdData : 
 * RETURN   : CMD_HANDLER : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-14 17:33
 *==========================================================================*/
static CMD_HANDLER *GetCmdHandler(const unsigned char * szCmdData)
{
	int i;
	char szCmdType[3];
	CMD_HANDLER *pCmdHandler;

	strncpyz(szCmdType, szCmdData, 3);

	pCmdHandler = g_EsrGlobals.CmdHandlers;
	for (i = 0; i < EEM_CMD_NUM; i++, pCmdHandler++)
	{
		if (strcmp(szCmdType, pCmdHandler->szCmdType) == 0)
		{
			return pCmdHandler;
		}
	}

	return NULL;
}


/*==========================================================================*
 * FUNCTION : VerifyExecutable
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ESR_DecodeAndPerform
 * ARGUMENTS: BOOL         bSecureLine  : 
 *            CMD_HANDLER  *pCmdHandler : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-14 18:30
 *==========================================================================*/
static BOOL VerifyExecutable(BOOL bSecureLine, CMD_HANDLER *pCmdHandler)
{
	COMMON_CONFIG *pConfig;
	int iSecurityLevel;
	BOOL bWriteForbid;

	/* 1.get common config */
	pConfig = &g_EsrGlobals.CommonConfig;
	iSecurityLevel = pConfig->iSecurityLevel;

	/* 2.get hardward write-protected switch status */
	bWriteForbid = GetSiteInfo()->bSettingChangeDisabled; 

	/* 3.now judge it */
	if (bWriteForbid && pCmdHandler->bIsWriteCommand)
	{
		return FALSE;
	}

	switch (iSecurityLevel)
	{
	case 1:
		break;

	case 2:
		if (!bSecureLine && pCmdHandler->bIsWriteCommand)
		{
			return FALSE;
		}
		break;

	case 3:
		if (!bSecureLine && strcmp(pCmdHandler->szCmdType, "CB"))
		{
			return FALSE;
		}
		break;
		
	default:
		break;
	}

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ESR_DecodeAndPerform
 * PURPOSE  : 
 * CALLS    : GetCmdHandler
 *			  VerifyExecutable
 * CALLED BY: Static Machine functions
 * ARGUMENTS: ESR_BASIC_ARGS         *pThis       : 
 *            const unsigned char   *szCmdData    : 
 * RETURN   : void : 
 * COMMENTS : length of szCmdRespBuff is ESR_RESP_SIZE
 * CREATOR  : LinTao                   DATE: 2004-11-14 17:04
 *==========================================================================*/
void ESR_DecodeAndPerform(ESR_BASIC_ARGS *pThis,
						  const unsigned char *szCmdData)
{
	CMD_HANDLER *pCmdHandler;
	char *szCmdRespBuff;

	szCmdRespBuff = pThis->szCmdRespBuff;

	/* 1.Get Cmd Handler */
	pCmdHandler = GetCmdHandler(szCmdData);
	if (pCmdHandler == NULL)
	{
		strcpy(szCmdRespBuff, "ERR-CMD*");

#ifdef _DEBUG_ESR_CMD_HANDLER
		TRACE_ESR_TIPS("Get Command Handler failed");
#endif //_DEBUG_ESR_CMD_HANDLER

		return;
	}

	
#ifdef _DEBUG_ESR_CMD_HANDLER
	{
		char *szFlagText;
		if (pCmdHandler->bIsWriteCommand)
		{
			szFlagText = "TRUE";
		}
		else
		{
			szFlagText = "FALSE";
		}

		TRACE_ESR_TIPS("Get Cmd Handler");
		TRACE("Cmd Handler info:\n");
		TRACE("\tCmd Type: %s\n", pCmdHandler->szCmdType);
		TRACE("\tIs Write Cmd: %s\n", szFlagText);
	}
#endif //_DEBUG_ESR_CMD_HANDLER


	/* 2.judge excutable */
	if (!VerifyExecutable(pThis->bSecureLine, pCmdHandler))
	{
		sprintf(szCmdRespBuff, "ERR2-%c%c*", szCmdData[0], szCmdData[1]);
		return;
	}

	/* 3.excute it */
	pCmdHandler->fnExecute(pThis, szCmdData, szCmdRespBuff);

	return;
}


static int ESR_GetDeviceStatus(IN int iEquipID)
{
	EQUIP_INFO* pEquipInfo;
	int  nBufLen, nError;
 

	nError = DxiGetData(VAR_A_EQUIP_INFO,
		iEquipID,			
		0,		
		&nBufLen,			
		&pEquipInfo,			
		0);

	if (nError == ERR_DXI_OK)
	{
		return pEquipInfo->bWorkStatus;
	}
	else
	{
		return FALSE;
	}
}

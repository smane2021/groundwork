/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGC
 *
 *  FILENAME : addydn.h
 *  CREATOR  : YangGuoxin                   DATE: 2008-09-28 15:10
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __ADDYDN_H_2008_09_28__
#define __ADDYDN_H_2008_09_28__



/* define protocol types */
enum ESR_PROTOCOL
{
	EEM		= 1,
	RSOC,
	SOC_TPE,
	YDN23,
	//YDN = 1,
	ESR_PROTOCOL_NUM
};


enum ESR_MEDIA_TYPE
{		
    MEDIA_TYPE_LEASED_LINE			= 0,
    MEDIA_TYPE_MODEM,
    MEDIA_TYPE_TCPIP,
	ESR_MEDIA_TYPE_NUM
};

/* define timers */
/* define timers */
enum ESR_TIMER
{
	ESR_ALARM_TIMER		= 1,
	ESR_CALLBACK_TIMER,

	SOC_WAIT_DLE0_TIMER,
	SOC_WAIT_CMD_TIMER,
	SOC_WAIT_ACK_TIMER,

	ESR_TIMER_NUM
};
#define YDN_ALARM_TIMER		1
#define YDN_CALLBACK_TIMER		2
//
//enum YDN_TIMER
//{
//	YDN_ALARM_TIMER		= 1,  //NAME NEED BE CHANGED
//	YDN_CALLBACK_TIMER,
//
//	SOC_WAIT_DLE0_TIMER,
//	SOC_WAIT_CMD_TIMER,
//	SOC_WAIT_ACK_TIMER,
//
//	YDN_TIMER_NUM
//};


/* communication status between MC and EventQueues */
enum ESR_COMM_STATUS                  
{
	COMM_STATUS_DISCONNECT		= 0,
	COMM_STATUS_NEXT,
	COMM_STATUS_SKIP
};


/* alarm info structure */
struct tagAlarmInfo
{
	char szStartTime[13];	/* alarm start time (YYMMDDHHMMSS) */
	char szEndTime[13];		/* alarm end time (YYMMDDHHMMSS)   */

	int	 iGroupID;	        /* Group ID */
	int	 iSubGroupID;	    /* Subgroup ID */
	int	 iUnitID;	        /* Unit ID */
	char cSection;			/* I for DI signal, O for DO signal */
	int	 iCno;				/* sig no of EEM model */

	/* 	alarm level of EEM Model, currently support :A1=0, A2=1, O1=3;
	 *  they are mapped to CA=3, MA=2, OA=1 in ACU model. */
	int	iAlarmLevel;	
};
typedef struct tagAlarmInfo ALARM_INFO;

/* define buffer size */
/* alarm buf */
#ifndef ESR_ALARM_BUF_SIZE
#define ESR_ALARM_BUF_SIZE	    50
#endif

/* alarm info buffer */
struct tagAlarmInfoBuffer
{
	/* alarm number for each level */
	int			iA1Num;
	int			iA2Num;
	int			iO1Num;

	int			iCurAlarmNum;	/* number of active alarms of the buf */
	ALARM_INFO  sCurAlarmInfo[ESR_ALARM_BUF_SIZE];      /* static buf */
	ALARM_INFO	*pCurAlarmInfo;	/* active alarm info, dynamic buf */

	int			iHisAlarmNum;	/* number of history alarms of the buf */
	ALARM_INFO	*pHisAlarmInfo;	/* history alarm info */

};
typedef struct tagAlarmInfoBuffer ALARM_INFO_BUFFER;
/* states definition */
/* states of EEM State Machine */
enum EEM_STATES
{
	STATE_MACHINE_QUIT  = -1,

	EEM_IDLE,
	EEM_WAIT_FOR_POLL,
	EEM_WAIT_FOR_RESP_ACK,
	EEM_SEND_CALLBACK,
	EEM_WAIT_FOR_CALLBACK_ACK,
	EEM_SEND_ALARM,
	EEM_WAIT_FOR_ALARM_ACK
};

/* states of SOC State Machine */
enum SOC_STATES
{
	SOC_OFF				= 0,
	SOC_SEND_ENQ,
	SOC_WAIT_DLE0,
	SOC_ALARM_REPORT,
	SOC_WAIT_CMD,
	SOC_WAIT_ACK
};
enum YDN_STATES
{
	
	YDN_IDLE = 0,
	YDN_WAIT_FOR_CMD,
	YDN_SEND_ALARM,
	YDN_WAIT_FOR_ALARM_ACK
};

/* command reply buf size */
#define YDN_RESP_SIZE				4114
struct tagYDNAlarmHandler
{
	BYTE	byCurReportRouteType;	/* route type defined by YDN_ROUTE_TYPE */

	BOOL	bAlarmAtHand;	
	BYTE	byAlarmRetryCounter;	/* alarm report retried times       */
};
typedef struct tagYDNAlarmHandler YDN_ALARM_HANDLER;
/* basic arg structure */
struct tagYDNBasicArgs
{
	/* event queues */
	HANDLE	hEventInputQueue;	
	HANDLE	hEventOutputQueue;	

	/* data buffers */
	int					iTimeoutCounter; //used by YDN_IDEL and SOC_OFF state
	int					iResendCounter;  /*used when timeout
										   (by YDN_WAIT_FOR_RESP_ACK,
											   YDN_WAIT_FOR_ALARM_ACK,
											   YDN_WAIT_FOR_CALLBACK_ACK,
											   SOC_WAIT_DLE0, 
											   SOC_ALARM_REPORT ) */
	ALARM_INFO_BUFFER	AlarmBuffer;	
#define YDN_R_BUFF_SIZE			4114
	unsigned char       sReceivedBuff[YDN_R_BUFF_SIZE]; /* used for CHAE-based frame recognize */
	int					iCurPos;                        //for Received Buff
	int                 iLen;						    //for Received Buff
	unsigned char	    szCmdRespBuff[YDN_RESP_SIZE];	 // command reply data buf 	
	YDN_ALARM_HANDLER	    alarmHandler;	
	char                *szBattLog;  //battery log buf, size is YDN_BATTLOG_SIZE

	/* link-layer current operation mode */
	int					iOperationMode; /* use YDN_OPERATION_MODE const */

	/* communication handle */
	HANDLE	hComm;	

	
	/* hThreadID[0]: YDN Service thread id;
	 * hThreadID[1]: Linklayer thread id.
	 * hThreadID[2]: PackData thread id*/
	HANDLE  hThreadID[3];

	/* flags */
	int     iMachineState;    /* state machine current state */ 
	int		*pOutQuitCmd;	  /* YDN service exit command, from Main Module */
	int     iInnerQuitCmd;               /* use SERVICE_EXIT_CODE const     */
	int 	iLinkLayerThreadExitCmd;	 /* use THREAD_EXIT_CODE const      */
	int 	iPackDataThreadExitCmd;	 /* use THREAD_EXIT_CODE const      */
	BOOL	bServerModeNeedExit;	     /* server operation mode exit flag */
	BOOL	bProtocolChangedFlag;	     /* protocol type changed flag      */
	BOOL	bCommRunning;	             /* communication busy flag         */
	BOOL	bSecureLine;	             /* security connection flag        */
	BOOL    bFrameDone;                  /* used by frame split function    */
	BOOL	bADRChangedFlag;	     /* protocol type changed flag      */
};
typedef struct tagYDNBasicArgs YDN_BASIC_ARGS;
/* global variable declare */
struct tagYDNMapEntriesInfo	/* sig mapping */
{
	int	iYDNPort;			/* sig serial no. or command type*/
	int  iEquipType;         /* equip type ID*/
	int	iSCUPSigType;     	/* defined by SIG_TYPE enum */
	int	iSCUPSigID;	        /* signal ID in equip*/
	int  iFlag0;
	int  iFlag1;
	int  iLen;
	int  iStatusValue[5];
	float fPeakValue[5];
};
typedef struct tagYDNMapEntriesInfo YDN_MAPENTRIES_INFO;

struct tagYDNTypeMapInfo				  /* type mapping */
{
	int	 iCID1;	                  /* YDN23 command CID1 */
	int	 iCID2;			          /* YDN23 command CID2 */
	int	 iMapEntriesNum;	          /* the number of current YDN model type */ 
	int  iUnitNum;					  /* the number of units for current type */
	YDN_MAPENTRIES_INFO	*pMapEntriesInfo; /* sig mapping info */							
};
typedef struct tagYDNTypeMapInfo YDN_TYPE_MAP_INFO;


struct tagYDNModelConfigInfo          /* YDN model config file info */
{
	int	iTypeMapNum;	 
	YDN_TYPE_MAP_INFO	*pTypeMapInfo;	  /* type mapping info */
};
typedef struct tagYDNModelConfigInfo YDNMODEL_CONFIG_INFO;

/* 2.YDN model info */
#define YDN_MAX_DEVICE_NUM  80
#define YDN_MAX_RELATIVE_DEVICE_NUM	20
struct tagYDNBlockInfo	
{
	int	    iCID1;	
	int	    iCID2;	
    int	    iEquipID;	         /* relevant equip id of ACU model */
				
	YDN_TYPE_MAP_INFO  *pTypeInfo;	 /* reference of relevant type info */

};
typedef struct tagYDNBlockInfo YDN_BLOCK_INFO;

struct tagYDNModelInfo	 
{
	int				iBlocksNum;	    /* the number of blocks */
	YDN_BLOCK_INFO	*pYDNBlockInfo;	 
};
typedef struct tagYDNModelInfo YDN_MODEL_INFO;

/* 3.YDN common config info */
/* const definitions */
#define COMM_PORT_PARAM_LEN	   64

/* define the number of settable alarm phone number/network address */
#define YDN_ALARM_REPORT_NUM    3   

/* define the number of settable callback phone number */
#define YDN_CALLBACK_NUM		1   

/* define the number of security ip address */
#define YDN_SECURITY_IP_NUM		2

/* define max phone number length */
#define PHONENUMBER_LEN     20

#define IPADDRESS_LEN       30

/* YDN common config info */
struct tagYDNCommonConfig
{
	int			iProtocolType;	/* see enum YDN_PROTOCOL definition, must be first */
	unsigned int		byADR;                
	
	unsigned int		iMediaType;	    /* see enum YDN_MEDIA_TYPE definition */
	char			szCommPortParam[COMM_PORT_PARAM_LEN];

	BOOL			bReportInUse;		
	
	/* max attemps to report alarms */
	int			iMaxAttempts;	
	/* elaps time between each attemps (unit: second) */
	int			iAttemptElapse;	

	/* phone number for alarm report*/
	char			szAlarmReportPhoneNumber[YDN_ALARM_REPORT_NUM][PHONENUMBER_LEN];

	char			cModifyUser[40];//added by ht,2006.12.13, no use in common config file,just for system log.
};
typedef struct tagYDNCommonConfig YDN_COMMON_CONFIG;


/* command execution function prototype definition */
//typedef struct tagYDNBasicArgs YDN_BASIC_ARGS;
typedef int (*YDN_CMD_EXECUTE_FUN) (YDN_BASIC_ARGS *pThis,
								 unsigned char *szInData,
								 unsigned char *szOutData);

struct tagYDNCmdHandler
{
	const char        *szCmdType;
	BOOL              bIsWriteCommand;
	YDN_CMD_EXECUTE_FUN   funExecute;
};
typedef struct tagYDNCmdHandler YDN_CMD_HANDLER;

struct tagYDNPackDataHandler
{
	const char        *szCmdType;
	unsigned char     *pPackData;
};
typedef struct tagYDNPackDataHandler YDN_PACKDATA_HANDLER;

/* used by RX* command */
struct tagYDNRefText
{
	int iModuleNo;  //No. of SMIO module, range from 1 to 8
	int iRefNo;     //fixed Ref. Text No. for each SMIO module, from 1 to 18

	char *szText1;  //reference of SMIO signal name 
	char *szText2;  //reference of Unit for AIs

	int *piAlarmLevel;  //reference of High or Low AI alarm level 
};
typedef struct tagYDNRefText YDN_REF_TEXT;

#define MAX_SMIO_UNIT			8
#define SMIO_REF_TEXTS_NUM		18
#define MAX_REF_TEXTS			(MAX_SMIO_UNIT * SMIO_REF_TEXTS_NUM)

/* YDN command number supported */
//#define YDN_CMD_NUM                 44
//zwh:增加了两个与ngc_lf兼容的协议404D,404E
//zwh:增加了写键盘的协议e1e4
//zwh:增加读历史告警e1ea
#define YDN_CMD_NUM                 49
#define YDN_READ_CMD_NUM           10

/* const definition for State Machine */
#define YDN_STATE_NUM           3

/* on-state function prototype */
typedef int (*ON_YDN_STATE_PROC) (YDN_BASIC_ARGS *pThis);


struct tagYDNStateInfo
{
	int	iStateID;	                /* defined by YDN_STATES enum */
	ON_YDN_STATE_PROC	fnStateProc;	
}; 
typedef struct tagYDNStateInfo YDN_STATE_INFO;

//MAX EquipNum
#define MAX_EQUIP_NUM         1000

struct tagYDNGlobals
{
	YDNMODEL_CONFIG_INFO YDNModelConfig;
	YDN_MODEL_INFO	YDNModelInfo;
	YDN_COMMON_CONFIG	CommonConfig;

	/* reference texts info of the solution (used for RX* command) */
	int iRefTexts;
	YDN_REF_TEXT	RefTexts[MAX_REF_TEXTS];

	YDN_CMD_HANDLER		CmdHandlers[YDN_CMD_NUM];
	YDN_PACKDATA_HANDLER PackDataHandlers[YDN_READ_CMD_NUM];
	YDN_STATE_INFO		YDNStates[YDN_STATE_NUM];
	EQUIP_INFO* pEquipInfo;
	int SMAddr;
	int iEquipNum;
	int iEquipIDOrder[MAX_EQUIP_NUM][2];
	//HANDLE  hMutexCommonCfg;  //for YDN common config file
};
typedef struct tagYDNGlobals YDN_GLOBALS;
extern YDN_GLOBALS  g_YDNGlobals;


DWORD ServiceCallbackByEEM(SERVICE_ARGUMENTS *pArgs, YDN_BASIC_ARGS *stYDNBasicArgs);
int YDN_ModifyCommonCfg(BOOL bServiceIsRunning,
						BOOL bByForce,
						YDN_BASIC_ARGS *pThis,
						int iItemID,
						YDN_COMMON_CONFIG *pUserCfg);
void RoutineBeforeExitYDNService(YDN_BASIC_ARGS *pThis);
#endif //__ADDYDN_H_2008_09_28__

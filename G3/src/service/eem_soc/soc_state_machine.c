/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : soc_state_machine.c
 *  CREATOR  : LinTao                   DATE: 2004-11-18 13:35
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "esr.h"

#define _SOC_WAINT_CMD_ALARM_REPORT  //new requirement from Cristian, need
						             //report alarm in WAIT_CMD status

#define SOC_CMD_LEN		512

/* const defined by SOC protocol */
#define SOC_RETRY_TIMES		4
#define SOC_WAIT_DLE0_ELAPSE	2000    /* 2 seconds  */
#define SOC_WAIT_CMD_ELAPSE	10000	/* 10 seconds */
#define SOC_WAIT_ACK_ELAPSE	3000	/* 3 seconds  */

/* log Macros (SSM = SOC State Machine) */
#define ESR_TASK_SSM		"SOC State Machine"

/* error log */
#define LOG_ESR_SSM_E(_szLogText)  LOG_ESR_E(ESR_TASK_SSM, _szLogText)
	
/* warning log */
#define LOG_ESR_SSM_W(_szLogText)  LOG_ESR_W(ESR_TASK_SSM, _szLogText)
	
/* info log */
#define LOG_ESR_SSM_I(_szLogText)  LOG_ESR_I(ESR_TASK_SSM, _szLogText)


/* assistant functional Macros */
#define MOVE_TBUFF_SENDPOS(_SOCMachineBuff)  (_SOCMachineBuff->iCurTBSendPos \
	 = (_SOCMachineBuff->iCurTBSendPos + 1) % SOC_TBUFF_NUM)

/* for Timer Operation */
/* set timers */
#define SET_WAIT_TIMER(_pThis, _TimerProc, _TimerID, _TimerElapse, _TimerFlag) \
	do {								\
			int iRet;					\
			(_TimerFlag) = FALSE;  \
			iRet = Timer_Set((_pThis)->hThreadID[0],		\
				 (_TimerID),		\
				 (_TimerElapse),	\
				 (_TimerProc),		\
				 (DWORD)(_pThis));		\
			if (iRet == ERR_TIMER_EXISTS)		\
			{			\
				Timer_Reset((_pThis)->hThreadID[0], (_TimerID));	\
			}			\
			else if (iRet == ERR_TIMER_SET_FAIL)	\
			{		\
				LOG_ESR_SSM_E("Set SOC-Wait-Timer failed");	\
			}	\
	} while(0)

#define SET_WAIT_DLE0_TIMER(_pThis)  \
	SET_WAIT_TIMER((_pThis), SOCTimerProc, SOC_WAIT_DLE0_TIMER, \
	  SOC_WAIT_DLE0_ELAPSE, (_pThis)->SOCMachineBuff.bWaitDLE0Timeout)

#define SET_WAIT_CMD_TIMER(_pThis) \
	SET_WAIT_TIMER((_pThis), SOCTimerProc, SOC_WAIT_CMD_TIMER, \
	  SOC_WAIT_CMD_ELAPSE, (_pThis)->SOCMachineBuff.bWaitCmdTimeout)

#define SET_WAIT_ACK_TIMER(_pThis)  \
	SET_WAIT_TIMER((_pThis), SOCTimerProc, SOC_WAIT_ACK_TIMER, \
	  SOC_WAIT_ACK_ELAPSE, (_pThis)->SOCMachineBuff.bWaitACKTimeout)

/* kill timers */
#define KILL_WAIT_DLE0_TIMER(_pThis) \
	((_pThis)->SOCMachineBuff.bWaitDLE0Timeout = FALSE,  \
		Timer_Kill((_pThis)->hThreadID[0], SOC_WAIT_DLE0_TIMER))

#define KILL_WAIT_CMD_TIMER(_pThis) \
	((_pThis)->SOCMachineBuff.bWaitCmdTimeout = FALSE,  \
		Timer_Kill((_pThis)->hThreadID[0], SOC_WAIT_CMD_TIMER))

#define KILL_WAIT_ACK_TIMER(_pThis) \
	((_pThis)->SOCMachineBuff.bWaitACKTimeout = FALSE,  \
		Timer_Kill((_pThis)->hThreadID[0], SOC_WAIT_ACK_TIMER))

/* reset timers */
#define RESET_WAIT_DLE0_TIMER(_pThis) \
	((_pThis)->SOCMachineBuff.bWaitDLE0Timeout = FALSE,  \
		Timer_Reset((_pThis)->hThreadID[0], SOC_WAIT_DLE0_TIMER))

#define RESET_WAIT_CMD_TIMER(_pThis) \
	((_pThis)->SOCMachineBuff.bWaitCmdTimeout = FALSE,  \
		Timer_Reset((_pThis)->hThreadID[0], SOC_WAIT_CMD_TIMER))

#define RESET_WAIT_ACK_TIMER(_pThis) \
	((_pThis)->SOCMachineBuff.bWaitACKTimeout = FALSE,  \
		Timer_Reset((_pThis)->hThreadID[0], SOC_WAIT_ACK_TIMER))


/* clear action before leave certain State */
#define ONLEAVE_SOC_WAIT_DLE0(_pThis) \
	((_pThis)->iResendCounter = 0, KILL_WAIT_DLE0_TIMER(_pThis))

#define ONLEAVE_SOC_ALARM_REPORT(_pThis) \
	((_pThis)->iResendCounter = 0, KILL_WAIT_ACK_TIMER(_pThis))

#define ONLEAVE_SOC_WAIT_ACK(_pThis) \
	(ClearSOCMachineBuf(&((_pThis)->SOCMachineBuff)), \
	 KILL_WAIT_ACK_TIMER(_pThis))

#define	ONLEAVE_SOC_WAIT_CMD(_pThis) \
	(ClearSOCMachineBuf(&((_pThis)->SOCMachineBuff)), \
	 KILL_WAIT_CMD_TIMER(_pThis))


/* send dummy frame event */
#define SSM_SEND_DUMMY(_pThis, _pEvent)  \
	do {  \
		(_pEvent) = &g_EsrDummyEvent; \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_ESR_SSM_E("Put frame event to Output Queue failed"); \
			return SOC_OFF;    \
		}  \
	}  \
	while (0)


/* send static ESR event to the OutputQueue */
#define SSM_SEND_SEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_ESR_SSM_E("Put frame event to Output Queue failed"); \
			return SOC_OFF;    \
		}  \
	}  \
	while (0)

/* send dynamic ESR event to the OutputQueue */
#define SSM_SEND_DEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_ESR_SSM_E("Put frame event to Output Queue failed"); \
			DELETE((_pEvent));  \
			return SOC_OFF;    \
		}  \
	}  \
	while (0)


/* interface provider by service_provider.c */
int ESR_ModifyCommonCfg(BOOL bServiceIsRunning,
						BOOL bByForce,
						ESR_BASIC_ARGS *pThis,
						int iItemID,
						COMMON_CONFIG *pUserCfg);
int ESR_ModifyCommonCfg_MS(BOOL bServiceIsRunning,
			   BOOL bByForce,
			   ESR_BASIC_ARGS *pThis,
			   int iItemID,
			   COMMON_CONFIG *pUserCfg);
/*==========================================================================*
 * FUNCTION : ClearSOCMachineBuf
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SOC_MACHINE_BUFF  *pBuff : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-10 16:36
 *==========================================================================*/
static void ClearSOCMachineBuf(SOC_MACHINE_BUFF *pBuff)
{
	int i;

	/* clear RBuff */
	pBuff->szReceiveBuff[0] = '\0';
	pBuff->iReceivePos = 0;

	/* clear TBuff */
	pBuff->iMsgsInTB = 0;
	pBuff->iNextFreeTBPos = 0;
	pBuff->iCurTBSendPos = 0;
	pBuff->iTBReSendCounter = 0;

	for (i = 0; i < SOC_TBUFF_NUM; i++)
	{
		pBuff->szTransmitBuff[i][0] = '\0';
	}

	/* clear buffed last command */
	pBuff->sLastSOCCmd[0] = '\0';

	return;
}

/* not used at present */
__INLINE static void SOC_OnDisconnectedEvent(ESR_BASIC_ARGS *pThis)
{
	SOC_MACHINE_BUFF *pSOCMachineBuff;
	
	/* 1.clear the flags and the buf */
	pThis->iTimeoutCounter = 0;
	pThis->iResendCounter = 0;

	pThis->szCmdRespBuff[0] = '\0';
	
	pSOCMachineBuff = &pThis->SOCMachineBuff;
	ClearSOCMachineBuf(pSOCMachineBuff);


	/* 2.kill the communication-related timers */
	KILL_WAIT_DLE0_TIMER(pThis);
	KILL_WAIT_CMD_TIMER(pThis);
	KILL_WAIT_ACK_TIMER(pThis);

	return;
}

/*==========================================================================*
 * FUNCTION : SOCTimerProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hTimerOwner : 
 *            int     idTimer     : 
 *            void    *pArgs      : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-07 16:35
 *==========================================================================*/
static int SOCTimerProc(HANDLE  hTimerOwner,
						int     idTimer,
						void    *pArgs)
{
	ESR_BASIC_ARGS *pThis;
    UNUSED(hTimerOwner);

	pThis = (ESR_BASIC_ARGS *)pArgs;

	switch (idTimer)
	{
	case SOC_WAIT_DLE0_TIMER:
		TRACE_ESR_TIPS("Timeout occurs caused by SOC_WAIT_DLE0_TIMER");
		pThis->SOCMachineBuff.bWaitDLE0Timeout = TRUE;
		break;
	
	case SOC_WAIT_CMD_TIMER:
		TRACE_ESR_TIPS("Timeout occurs caused by SOC_WAIT_CMD_TIMER");
		pThis->SOCMachineBuff.bWaitCmdTimeout = TRUE;
		break;

	case SOC_WAIT_ACK_TIMER:
		TRACE_ESR_TIPS("Timeout occurs caused by SOC_WAIT_ACK_TIMER");
		pThis->SOCMachineBuff.bWaitACKTimeout = TRUE;
		break;

	default:
		TRACE_ESR_TIPS("Invalid Timer id");
		break;
	}
	
	return TIMER_CONTINUE_RUN;  //keep running to make Timer_Reset valid
}


/*==========================================================================*
 * FUNCTION : OnOff_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SOC_OnOff
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis  : 
 *            ESR_EVENT       *pEvent : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-07 11:07
 *==========================================================================*/
static int OnOff_HandleFrameEvent(ESR_BASIC_ARGS *pThis, ESR_EVENT *pEvent)
{
	FRAME_TYPE frameType;

	frameType = ESR_AnalyseFrame(pEvent->sData, pEvent->iDataLength, pThis);

	if (frameType == FRAME_ENQ)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received Frame Event from InputQueue: <ENQ>");
#endif //_DEBUG_ESR_LINKLAYER

		DELETE(pEvent);

		/* create Frame event containing <DLE0> */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			return SOC_OFF;
		}
		INIT_FRAME_EVENT(pEvent, 2, DLE0, 0, 0);

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);

		/* set timer */
		SET_WAIT_CMD_TIMER(pThis);

		return SOC_WAIT_CMD;
	}

	else if (FRAME_ERR_PROT == frameType)  //handle by third-party handler
	{

#ifdef COM_SHARE_SERVICE_SWITCH
		{
			SERVICE_SWITCH_FLAG *pFlag;
			pFlag = DXI_GetServiceSwitchFlag();

			if (ESR_IsMtnFrame(pEvent->sData, pEvent->iDataLength))
			{
#ifdef _DEBUG_ESR_LINKLAYER
				TRACE_ESR_TIPS("received request frame event from PowerKit");
#endif //_DEBUG_ESR_LINKLAYER

				DELETE(pEvent);

				/* set the flag */
				pFlag->bHLMSUseCOM = FALSE;
	
				/* replay to PowerKit */
				pEvent = NEW(ESR_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_ESR_SSM_E("Memory is not enough");
					return SOC_OFF;
				}

				INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, TRUE);
				ESR_BuildMtnResponseFrame(pEvent->sData, &pEvent->iDataLength);
							   
				/* send it */
				SSM_SEND_DEVENT(pThis, pEvent);

				/* create Disconnect event */
				Sleep(500);
				/*pEvent = NEW(ESR_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_ESR_SSM_E("Memory is not enough");
					return SOC_OFF;
				}
				INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);*/

				pEvent = &g_EsrDiscEvent;

				/* send it */
				SSM_SEND_SEVENT(pThis, pEvent);

				/* exit the service */
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;
				pThis->iInnerQuitCmd = SERVICE_EXIT_OK;

				return SOC_OFF;
			}
		}
#endif //COM_SHARE_SERVICE_SWITCH


#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: FRAME_ERR_PROT");
#endif //_DEBUG_ESR_LINKLAYER

		DELETE(pEvent);

		/* send dummy for synchronization */
		SSM_SEND_DUMMY(pThis, pEvent);
		return SOC_OFF;

	} /* end else if FRAME_ERR_PROT process */
	else
	{
		TRACE_ESR_TIPS("Received unexpected frame event");

		DELETE(pEvent);

		/* send dummy for synchronization */
		SSM_SEND_DUMMY(pThis, pEvent);

		return SOC_OFF;
	}
	
}


/*==========================================================================*
 * FUNCTION : SOC_OnOff
 * PURPOSE  : 
 * CALLS    : ESR_SendAlarm
 *			  OnOff_HandleFrameEvent
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 09:50
 *==========================================================================*/
int SOC_OnOff(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;

	ALARM_HANDLER *pAlarmHandler;
	pAlarmHandler = &pThis->alarmHandler; 

	BOOL bStopCommForAlarm;  //need to stop communication for alarm occurs

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* alarm process(note: not need callback in SOC state machine) */
	if (!pThis->bCommRunning && pAlarmHandler->bAlarmAtHand)
	{
		/* reset timeout counter */
		pThis->iTimeoutCounter = 0; 

		ESR_SendAlarm(pThis);
		return SOC_SEND_ENQ;
	}

	/* get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	if (iRet == ERR_QUEUE_EMPTY)  
	{
		/* In Off state, it means timeout, keep wait in next Off state */
		//printf("\n***Get empty event from Queue so self on/off***\n");
		return SOC_OFF;
	}

	if (iRet == ERR_INVALID_ARGS)								
	{
		LOG_ESR_SSM_E("Get from Input Queue failed(invalid args)");
		return SOC_OFF;
	}

	/* reset timeout counter */
	if (pEvent->iEventType != ESR_TIMEOUT_EVENT)
	{
		pThis->iTimeoutCounter = 0; 
	}

	/* get the flag */
	bStopCommForAlarm = (pAlarmHandler->bAlarmAtHand && 
		pThis->iOperationMode == ESR_MODE_SERVER) ? TRUE : FALSE;

	/* now iRet is OK */
	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:
		//printf("\nThe curr event string buffer is %s\n",pEvent->sData);
		return OnOff_HandleFrameEvent(pThis, pEvent);

	case ESR_TIMEOUT_EVENT:  //timeout event process, send disconnect event
		/*DELETE(pEvent);*/

		if (++pThis->iTimeoutCounter > ESR_MAX_TIMEOUTCOUNT ||
			bStopCommForAlarm)
		{
			int iNextState = SOC_OFF;

#ifdef _DEBUG_ESR_LINKLAYER
			TRACE_ESR_TIPS("keep Timeout too long or have alarm to report, "
				"will dissconnect current communication actively");
#endif //_DEBUG_ESR_LINKLAYER

			pThis->iTimeoutCounter = 0;  //reset first

			/* create Disconnect event */
			pEvent = &g_EsrDiscEvent;
			/*pEvent = NEW(ESR_EVENT, 1);
			if (pEvent == NULL)
			{
				LOG_ESR_SSM_E("Memory is not enough");
				return SOC_OFF;
			}

			INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);*/

#ifdef _DEBUG_ESR_LINKLAYER
			TRACE_ESR_TIPS("Disconnected Event created, send to OutputQueue");
#endif //_DEBUG_ESR_LINKLAYER

			if (g_EsrGlobals.CommonConfig.iMediaType == 
				MEDIA_TYPE_LEASED_LINE && bStopCommForAlarm)
			{
				/* set flag first, insure change to the client mode */
				ESR_SendAlarm(pThis);
				iNextState = SOC_SEND_ENQ;
			}

			/* send it */
			SSM_SEND_SEVENT(pThis, pEvent);

			return iNextState;
		}
		else  //just send dummy
		{
			break;
		}

	case ESR_DISCONNECTED_EVENT:  //Disconnected Event
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER
		/*DELETE(pEvent);*/

		/* wait in another Off state */
		return SOC_OFF;

	default:  //for others, send dummy and stay in Off state
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
		DELETE_ESR_EVENT(pEvent);
	}	

	SSM_SEND_DUMMY(pThis, pEvent);

	return SOC_OFF;
	
}

/*==========================================================================*
 * FUNCTION : SOC_OnSendENQ
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 10:27
 *==========================================================================*/
int SOC_OnSendENQ(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* 1.get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
		/* keep on waiting */
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Wait for Connected Event timeout, keep on waiting");
#endif //_DEBUG_ESR_LINKLAYER

		return SOC_SEND_ENQ;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_ESR_SSM_E("Get from InputQueue failed for invalid args");
		return SOC_OFF;
	}

	/* now iRet is OK */
	switch (pEvent->iEventType)
	{
	case ESR_CONNECTED_EVENT:  //send <ENQ> 
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received Connected Event");
#endif //_DEBUG_ESR_LINKLAYER
		/*DELETE(pEvent);*/

		/* create Frame event containing "<ENQ>" */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			return SOC_OFF;
		}
		INIT_SIMPLE_FRAME_EVENT(pEvent, ENQ);

		printf("Sending the AR strings.\n");// zzc debug

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);

		/* set the timer */
		SET_WAIT_DLE0_TIMER(pThis);

		return SOC_WAIT_DLE0;


	case ESR_CONNECT_FAILED_EVENT:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Connect Failed Event");
#endif //_DEBUG_ESR_LINKLAYER			
		/*DELETE(pEvent);*/

		ESR_ClientReportFailed(pThis, TRUE);
		return SOC_OFF;


	case ESR_DISCONNECTED_EVENT:  //drop it
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/

		return SOC_SEND_ENQ;


	default:  //unexpected event
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER
		DELETE_ESR_EVENT(pEvent);

		/* keep on waiting */
		return SOC_SEND_ENQ;
	}  //end of switch
}


/*==========================================================================*
 * FUNCTION : SendAlarmFrame
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: OnWaitDLE0_HandleFrameEvent
 *			  OnAlarmReport_HandleTimeout
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   :  BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 15:26
 *==========================================================================*/
static BOOL SendAlarmFrame(ESR_BASIC_ARGS *pThis, BOOL bSkip)
{
	BOOL bSoc;
	int  iRet;
	char szSendMsg[128];
	ESR_EVENT *pEvent;

	/* judge protocol type */
	if (g_EsrGlobals.CommonConfig.iProtocolType ==	SOC_TPE)
	{
		bSoc =	TRUE;
	}
	else
	{
		bSoc = FALSE;
	}

	/* 1.get the string */
	iRet = ESR_GetReportMsg(TRUE, szSendMsg, sizeof(szSendMsg));

	if (iRet == 1)  //call DXI interface failed
	{
		LOG_ESR_SSM_E("Get Site Name through DXI interface failed");

		return FALSE;
	}

	if (iRet == 2)  //buffer is too small, truncate it and give tip
	{
		LOG_ESR_SSM_I("Site Name length is bigger than 64bytes");
	}

	/* 2.build "E*" frame event and send it */
	pEvent = NEW(ESR_EVENT, 1);
	if (pEvent == NULL)
	{
		LOG_ESR_SSM_E("No memory");
		return FALSE;
	}
	INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

	/* modified by Thomas, 2005-10-4 */
	/* For SOC/TPE, AlarmReport string must not start with "<ccid>!" */
	ESR_BuildSOCResponseFrame(bSoc, szSendMsg, FALSE, NULL,
		pEvent->sData, &pEvent->iDataLength);

	/* skip the next ComMC */
	if (bSkip)
	{
		SET_EVENT_SKIP_FLAG(pEvent);
	}

	SSM_SEND_DEVENT(pThis, pEvent);

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : OnWaitDLE0_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : SendAlarmFrame
 * CALLED BY: SOC_OnWaitDLE0
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis  : 
 *            ESR_EVENT       *pEvent : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 16:07
 *==========================================================================*/
static int OnWaitDLE0_HandleFrameEvent(ESR_BASIC_ARGS *pThis,
									   ESR_EVENT *pEvent)
{
	FRAME_TYPE frameType;

	frameType = ESR_AnalyseFrame(pEvent->sData, pEvent->iDataLength, pThis);
	DELETE(pEvent);

	if (frameType == FRAME_DLE0 || frameType == FRAME_ENQ)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		if (frameType == FRAME_DLE0)
		{
			TRACE_ESR_TIPS("Received <DLE0>");
		}
		else
		{
			TRACE_ESR_TIPS("Received <ENQ>");
		}
#endif //_DEBUG_ESR_LINKLAYER

		/* prepare to change to next state */
		ONLEAVE_SOC_WAIT_DLE0(pThis);


		if (!SendAlarmFrame(pThis, FALSE))
		{
			return SOC_OFF;
		}	

		/* set Wait-ACK timer */
		SET_WAIT_ACK_TIMER(pThis);
		return SOC_ALARM_REPORT;
	}
	else  //ignore, just send dummy event
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected Frame Event");
#endif //_DEBUG_ESR_LINKLAYER

		SSM_SEND_DUMMY(pThis, pEvent);

		/* keep on waiting */
		return SOC_WAIT_DLE0;
	}  
}   


/*==========================================================================*
 * FUNCTION : OnWaitDLE0_HandleTimeout
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SOC_OnWaitDLE0
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis  : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 16:50
 *==========================================================================*/
static int OnWaitDLE0_HandleTimeout(ESR_BASIC_ARGS *pThis)
{
	ESR_EVENT *pEvent;
	SOC_MACHINE_BUFF *pSocBuf;

	pSocBuf = &pThis->SOCMachineBuff;
	if (++pThis->iResendCounter > SOC_RETRY_TIMES)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Give up resent <ENQ> after 4 retries");
#endif //_DEBUG_ESR_LINKLAYER
		
		ESR_ClientReportFailed(pThis, FALSE);
		ONLEAVE_SOC_WAIT_DLE0(pThis);

		/* create Frame event containing <EOT> */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			return SOC_OFF;
		}
		INIT_SIMPLE_FRAME_EVENT(pEvent, EOT);

		/* skip the next ComMC calling */
		SET_EVENT_SKIP_FLAG(pEvent);

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);

		/* create and send Disconnect event */
		pEvent = &g_EsrDiscEvent;
		/*pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			return SOC_OFF;
		}
		INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);*/

		SSM_SEND_SEVENT(pThis, pEvent);

		return SOC_OFF;
	}
	else //resend <ENQ>
	{
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			ONLEAVE_SOC_WAIT_DLE0(pThis);
			return SOC_OFF;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, ENQ);

		/* skip the next ComMC calling */
		SET_EVENT_SKIP_FLAG(pEvent);

		SSM_SEND_DEVENT(pThis, pEvent);
		RESET_WAIT_DLE0_TIMER(pThis);
		
		return SOC_WAIT_DLE0;
	}

}


/*==========================================================================*
 * FUNCTION : SOC_OnWaitDLE0
 * PURPOSE  : 
 * CALLS    : OnWaitDLE0_HandleFrameEvent
 *			  OnWaitDLE0_HandleTimeout
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 15:10
 *==========================================================================*/
int SOC_OnWaitDLE0(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;
	SOC_MACHINE_BUFF *pSocBuf;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);
	pSocBuf = &pThis->SOCMachineBuff;

	/* 1. timeout check */
	if (pSocBuf->bWaitDLE0Timeout)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Wait DLE0 timeout");
#endif //_DEBUG_ESR_LINKLAYER

		return OnWaitDLE0_HandleTimeout(pThis);
	}

	/* 2.get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		//TRACE_ESR_TIPS("Event Input Queue is empty, keep on waiting");
#endif //_DEBUG_ESR_LINKLAYER
	
		return SOC_WAIT_DLE0;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_ESR_SSM_E("Get from InputQueue failed for invalid args");

		ONLEAVE_SOC_WAIT_DLE0(pThis);
		return SOC_OFF;
	}

	/* now iRet is ERR_OK */
	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:  //frame event process
		return OnWaitDLE0_HandleFrameEvent(pThis, pEvent);

	case ESR_DISCONNECTED_EVENT:
		ESR_ClientReportFailed(pThis, FALSE);
		ONLEAVE_SOC_WAIT_DLE0(pThis);
		return SOC_OFF;

	case ESR_TIMEOUT_EVENT:  //keep on waiting after send dummy until 2s is up
		/*DELETE(pEvent);*/
		break;

	default:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);
	}

	/* send dummy for synchronization */
	SSM_SEND_DUMMY(pThis, pEvent);

	return SOC_WAIT_DLE0;	
}

/*==========================================================================*
 * FUNCTION : OnAlarmReport_HandleTimeout
 * PURPOSE  : 
 * CALLS    : SendAlarmFrame
 * CALLED BY: SOC_OnAlarmReport
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis  :  
 *			  BOOL			   bENQ   :
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 20:45
 *==========================================================================*/
static int OnAlarmReport_HandleTimeout(ESR_BASIC_ARGS *pThis,
									   BOOL bENQ)
{
	ESR_EVENT *pEvent;

	if (++pThis->iResendCounter > SOC_RETRY_TIMES)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Give up resend Alarm Frame after 4 retries");
#endif //_DEBUG_ESR_LINKLAYER

		ESR_ClientReportFailed(pThis, FALSE);
		ONLEAVE_SOC_ALARM_REPORT(pThis);


		/* create Frame event containing <EOT> */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			return SOC_OFF;
		}
		INIT_SIMPLE_FRAME_EVENT(pEvent, EOT);

		/* skip the next ComMC calling */
		SET_EVENT_SKIP_FLAG(pEvent);

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);

		/* create and send Disconnect event */
		pEvent = &g_EsrDiscEvent;
		/*pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			return SOC_OFF;
		}
		INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);*/		

		SSM_SEND_SEVENT(pThis, pEvent);

		return SOC_OFF;
	}
	else //resend "E*"
	{
		if (bENQ)  //reply <ENQ> first
		{
			pEvent = NEW(ESR_EVENT, 1);
			if (pEvent == NULL)
			{
				LOG_ESR_SSM_E("Memory is not enough");
				ONLEAVE_SOC_ALARM_REPORT(pThis);
				return SOC_OFF;
			}
			INIT_SIMPLE_FRAME_EVENT(pEvent, ENQ);

			/* if not skip, may timeout again */
			SET_EVENT_SKIP_FLAG(pEvent);

			/* send it */
			SSM_SEND_DEVENT(pThis, pEvent);
		}

		if (!SendAlarmFrame(pThis, TRUE))
		{
			ONLEAVE_SOC_ALARM_REPORT(pThis);
			return SOC_OFF;
		}

		RESET_WAIT_ACK_TIMER(pThis);
		return SOC_ALARM_REPORT;
	}
}


/*==========================================================================*
 * FUNCTION : OnAlarmReport_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: SOC_OnAlarmReport
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis  : 
 *            ESR_EVENT       *pEvent : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 20:35
 *==========================================================================*/
static int OnAlarmReport_HandleFrameEvent(ESR_BASIC_ARGS *pThis,
										  ESR_EVENT *pEvent)
{
	FRAME_TYPE frameType;

	frameType = ESR_AnalyseFrame(pEvent->sData, pEvent->iDataLength, pThis);

	switch (frameType)
	{
	case FRAME_ACK:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received <ACK>, alarm report successfully");
#endif //_DEBUG_ESR_LINKLAYER
		DELETE(pEvent);
		
		ESR_AlarmReported(pThis);
		ONLEAVE_SOC_ALARM_REPORT(pThis);
				
		/* send dummy frame event to keep synchronization */
		SSM_SEND_DUMMY(pThis, pEvent);

		/* set timer */
		SET_WAIT_CMD_TIMER(pThis);	
		return SOC_WAIT_CMD;

	case FRAME_NAK:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: <NAK>, will resend Alarm Frame");
#endif //_DEBUG_ESR_LINKLAYER
		DELETE(pEvent);

		return OnAlarmReport_HandleTimeout(pThis, FALSE);

	case FRAME_ENQ:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: <ENQ>, will resend Alarm Frame");
#endif //_DEBUG_ESR_LINKLAYER
		DELETE(pEvent);

		return OnAlarmReport_HandleTimeout(pThis, TRUE);

	default:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected frame event");
		ESR_PrintEvent(pEvent);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);
	}

	/* send dummy for synchronization */
	SSM_SEND_DUMMY(pThis, pEvent);

	return SOC_ALARM_REPORT;

}


/*==========================================================================*
 * FUNCTION : SOC_OnAlarmReport
 * PURPOSE  : 
 * CALLS    : OnAlarmReport_HandleFrameEvent
 *			  OnAlarmReport_HandleTimeout
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-06 19:12
 *==========================================================================*/
int SOC_OnAlarmReport(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* 1. timeout check */
	if (pThis->SOCMachineBuff.bWaitACKTimeout)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("wait Alarm Report ACK timeout");
#endif //_DEBUG_ESR_LINKLAYER

		return OnAlarmReport_HandleTimeout(pThis, FALSE);
	}

	/* 2.get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("InputQueue is empty, keep on wait Alarm Frame ACK");
#endif //_DEBUG_ESR_LINKLAYER

		return SOC_ALARM_REPORT;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_ESR_SSM_E("Get from InputQueue failed for invalid args");

		ONLEAVE_SOC_ALARM_REPORT(pThis);
		return SOC_OFF;
	}

	/* now iRet is ERR_OK */
	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:  //frame event process
		return OnAlarmReport_HandleFrameEvent(pThis, pEvent);

	case ESR_DISCONNECTED_EVENT:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		ESR_ClientReportFailed(pThis, FALSE);
		ONLEAVE_SOC_ALARM_REPORT(pThis);
		/*DELETE(pEvent);*/
		return SOC_OFF;

	case ESR_TIMEOUT_EVENT:  //continue wait after send dummy
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Timeout Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/
		break;

	default:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("Event Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE_ESR_EVENT(pEvent);
	}

	/* send dummy for synchronization */
	SSM_SEND_DUMMY(pThis, pEvent);

	return SOC_ALARM_REPORT;
}


/*==========================================================================*
 * FUNCTION : IsIdenticalCmd
 * PURPOSE  : check identical frame
 * CALLS    : 
 * CALLED BY: HandleSTXFrame
 * ARGUMENTS: SOC_MACHINE_BUFF  *pBuff  : 
 *            ESR_EVENT         *pEvent : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-09 10:32
 *==========================================================================*/
static BOOL IsIdenticalCmd(SOC_MACHINE_BUFF *pBuff,
						   ESR_EVENT *pEvent)
{
	int iLastLen, iCurLen;
	unsigned char *pLastCmd, *pCurCmd;

	iLastLen = pBuff->iLastSOCLength;
	pLastCmd = pBuff->sLastSOCCmd;
	iCurLen = pEvent->iDataLength;
	pCurCmd = pEvent->sData;

	if (iCurLen == iLastLen &&
		!memcmp(pCurCmd, pLastCmd, (size_t)iLastLen))  //identical command
	{ 
		return TRUE;
	}
	else  //update buff
	{
		pBuff->iLastSOCLength = iCurLen;
		memcpy(pLastCmd, pCurCmd, (size_t)iCurLen);
		return FALSE;
	}
}

/*==========================================================================*
 * FUNCTION : InsertMsgToTB
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN BOOL                 bSoc         : 
 *            IN const unsigned char  *szResData   : 
 *            IN const unsigned char  *szSoc_proc  : 
 *            SOC_MACHINE_BUFF        *pMachineBuf : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-09 16:49
 *==========================================================================*/
static BOOL InsertMsgToTB(IN BOOL bSoc,
						  IN const unsigned char *szResData, 
						  IN const unsigned char *szSoc_proc,
						  SOC_MACHINE_BUFF *pMachineBuf,
						  IN BOOL bSOCSpecialCmd)
{
	int i, iParts, iOffSet, iNotUsed;
	int iNextFreeTBPos;
	char szPartData[ESR_MAX_APPDATA_LEN + 1];  //end with NULL

	/* note: 3 means ccid + <!> */
	iParts = 1 + (strlen(szResData) + 3 - 1) / ESR_MAX_APPDATA_LEN;

	/* check the buff size */
	if (pMachineBuf->iMsgsInTB + iParts > SOC_TBUFF_NUM)
	{
		LOG_ESR_SSM_W("TBuff is full, discard the current response data");
		return FALSE;
	}

	iNextFreeTBPos = pMachineBuf->iNextFreeTBPos;
	iOffSet = 0;

	/* the first part should add ccid + <!> */
	strncpyz(szPartData, szResData, ESR_MAX_APPDATA_LEN - 3 + 1);
	//ESR_BuildSOCResponseFrame(bSoc, szPartData, TRUE, szSoc_proc,
	//	pMachineBuf->szTransmitBuff[iNextFreeTBPos], &iNotUsed);
	if(bSOCSpecialCmd)
	{
		ESR_BuildSOCResponseFrame(bSoc, szPartData, FALSE, szSoc_proc,
			pMachineBuf->szTransmitBuff[iNextFreeTBPos], &iNotUsed);
	}
	else
	{
		ESR_BuildSOCResponseFrame(bSoc, szPartData, TRUE, szSoc_proc,
			pMachineBuf->szTransmitBuff[iNextFreeTBPos], &iNotUsed);
	}

	pMachineBuf->iMsgsInTB++;
	iNextFreeTBPos = (iNextFreeTBPos + 1) % SOC_TBUFF_NUM;
	iOffSet += ESR_MAX_APPDATA_LEN - 3;

	for (i = 1; i < iParts; i++)
	{
		strncpyz(szPartData, szResData + iOffSet, ESR_MAX_APPDATA_LEN + 1);
		ESR_BuildSOCResponseFrame(bSoc, szPartData, FALSE, szSoc_proc,
			pMachineBuf->szTransmitBuff[iNextFreeTBPos], &iNotUsed);
	 
		pMachineBuf->iMsgsInTB++;
		iNextFreeTBPos = (iNextFreeTBPos + 1) % SOC_TBUFF_NUM;
		iOffSet += ESR_MAX_APPDATA_LEN;
	}

	/* update the buff */
	pMachineBuf->iNextFreeTBPos = iNextFreeTBPos;

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : HandleSpecialCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis     : 
 *            ESR_EVENT       *pEvent    : 
 *            unsigned char   *szOutData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-10 09:44
 *==========================================================================*/
static BOOL HandleSpecialCmd(ESR_BASIC_ARGS *pThis,
							 ESR_EVENT *pEvent, 
							 unsigned char *szOutData)
{
	const unsigned char *pCmd;
	int iCmdLen;

	COMMON_CONFIG commonCfg;  //used for MP_SAVE_PHONE command
	int index, iItemID;
	char szPhoneNumber[PHONENUMBER_LEN];

	pCmd = ESR_ExtractDataFromFrame(STX_SOC_SPECIAL,
		pEvent->sData, pEvent->iDataLength, &iCmdLen);

	/* 1.MP_STATE command */
	if (strncmp(pCmd, "F000", 4) == 0)
	{
		sprintf(szOutData, "F0001E%c", LF);
		return TRUE;
	}

	/* 2.MP_TIME command */
	if (pCmd[0] == 'j')
	{
		sprintf(szOutData, "j%c", LF);
		return TRUE;
	}

	/* 3.MP_SAVE_PHONE command */
	if (pCmd[0] == 'U' && (pCmd[1] >= '0' && pCmd[1] <= '9'))
	{
		if (pCmd[1] == '0' || pCmd[1] == '1')
		{
			/* get the new phone number */
			/* note: end with NULL */
			strncpyz(szPhoneNumber, pCmd + 2, iCmdLen - 2 + 1); 

			if (pCmd[1] == '0')
			{
				index = 0;
				iItemID = ESR_CFG_W_MODE | ESR_CFG_REPORT_NUMBER_1;
			}
			else  // pCmd[1] == '1'
			{
				index = 1;
				iItemID = ESR_CFG_W_MODE | ESR_CFG_REPORT_NUMBER_2;
			}

			strncpyz(commonCfg.szAlarmReportPhoneNumber[index],
				szPhoneNumber, PHONENUMBER_LEN);

			/* change the alarm report phone number */
			ESR_ModifyCommonCfg_MS(TRUE, TRUE, pThis, iItemID, &commonCfg);
		}

		sprintf(szOutData, "U%c", LF);
		return TRUE;
	}
	/* 4.MP_SAVE_IP command */
	char szReportIPAddr[IPADDRESS_LEN];
	if (pCmd[0] == 'T' && iCmdLen > 12)
	{

		BYTE byOrig[6], i;
		for(i = 0; i < 6; i++)
		{
			byOrig[i] = ESR_AsciiHexToChar(pCmd + 1 + i*2);
		}

		sprintf(szReportIPAddr, "%d.%d.%d.%d:%d", (int)(byOrig[0]), (int)(byOrig[1]),
			(int)(byOrig[2]), (int)(byOrig[3]) ,(int)(byOrig[4]*256 + byOrig[5]));

		//printf("\n-----Jimmy, the report IP is [%s], and iLen = %d \n",szReportIPAddr,iCmdLen);

		strncpyz(commonCfg.szReportIP[0],
			szReportIPAddr, IPADDRESS_LEN);

		iItemID = ESR_CFG_W_MODE | ESR_CFG_IPADDR_1;
		/* change the report IP number */
		ESR_ModifyCommonCfg_MS(TRUE, TRUE, pThis, iItemID, &commonCfg);

		sprintf(szOutData, "T%c", LF);
		return TRUE;
	}

	/* 4.others, ignore */
	sprintf(szOutData, "%c?0A%c", pCmd[0], LF);
	//return FALSE;
	//Jimmy noted here 2013.11.20 that always will return TRUE; because unknown MP cmd should also 
	//be correctly responded,(not included CCID)
	return TRUE;
}


/* to delete */
void PrintRBuff(char * szInData, int iLen)
{
	int i;
	char chr;

	TRACE("Info of the Received Buff\n");
	TRACE("\tData Content(Text format):");
		for (i = 0; i < iLen; i++)
		{
			chr = (unsigned char)szInData[i];
			if (chr < 0x21 || chr > 0X7E)
			{
				printf("%s", ESR_GetUnprintableChr(chr));
			}
			else
			{
				printf("%c", chr);
			}
		}
		printf("\n");

}
/*==========================================================================*
 * FUNCTION : HandleSTXFrame
 * PURPOSE  : handle STX frame when used SOC/TPE protocol
 * CALLS    : IsIdenticalCmd
 * CALLED BY: OnWaitCmdOrACK_HandleFrameEvent
 * ARGUMENTS: int             iCurState : SOC_WAIT_CMD or SOC_WAIT_ACK
 *			  BOOL			  bSOC      : TRUE for SOC_TPE, FALSE for RSOC
 *            FRAME_TYPE      frameType : 
 *            ESR_BASIC_ARGS  *pThis    : 
 *			  ESR_EVENT       *pEvent   :
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-08 17:27
 *==========================================================================*/
static int HandleSTXFrame(int iCurState,
						  BOOL bSOC,
						  FRAME_TYPE frameType, 
						  ESR_BASIC_ARGS *pThis,
						  ESR_EVENT *pEvent)
{
	SOC_MACHINE_BUFF *pSOCMachineBuff;     
	unsigned char *pRBuff;
	int iRBuffLen;
	int *piRBuffPos;

	int iRet, iCmdDataLen;
	const unsigned char *pCmdData;  //point to the Frame data
	/* copy from the Frame data */
	unsigned char szCmdData[SOC_CMD_LEN], szSoc_proc[6]; 
	BOOL	bSOCSpecialCmd = FALSE;


	ESR_EVENT *pReEvent;

	/* initial local variables */
	pSOCMachineBuff = &pThis->SOCMachineBuff;
	iRBuffLen = sizeof(pSOCMachineBuff->szReceiveBuff);
	piRBuffPos = &pSOCMachineBuff->iReceivePos;
    pRBuff = pSOCMachineBuff->szReceiveBuff;

	switch (frameType)
	{
	case STX_SOC_SPLIT:
	case STX_SOC_SPLIT_F:
		if (!bSOC)  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		if (STX_SOC_SPLIT == frameType)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_SOC_SPLIT");
		}
		else if(STX_SOC_SPLIT_F == frameType)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_SOC_SPLIT_F");
		}
#endif //_DEBUG_ESR_LINKLAYER

	case STX_RSOC_SPLIT:
	case STX_RSOC_SPLIT_F:

		if (bSOC && (frameType == STX_RSOC_SPLIT ||
			         frameType == STX_RSOC_SPLIT_F))  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		if (frameType == STX_RSOC_SPLIT)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_RSOC_SPLIT");
		}
		else if (frameType == STX_RSOC_SPLIT_F)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_RSOC_SPLIT_F");
		}
#endif //_DEBUG_ESR_LINKLAYER

//		if (IsIdenticalCmd(pSOCMachineBuff, pEvent))
//		{
//
//#ifdef _DEBUG_ESR_LINKLAYER
//			TRACE_ESR_TIPS("Received a identical command");
//#endif //_DEBUG_ESR_LINKLAYER
//
//			if (iCurState == SOC_WAIT_CMD)
//			{
//				RESET_WAIT_CMD_TIMER(pThis);
//			}
//
//			DELETE(pEvent);
//
//			/* send dummy */
//			SSM_SEND_DUMMY(pThis, pEvent);
//			return iCurState;  //SOC_WAIT_CMD or SOC_WAIT_ACK
//		}
//		else
		{
			/* STX_xx_SPLIT process */

			/* modified! -- not need send <ACK>, just send Dummy to get more */
			///* send <ACK> */
			//pReEvent = NEW(ESR_EVENT, 1);
			//if (pReEvent == NULL)
			//{
			//	LOG_ESR_SSM_E("Memory is not enough");
			//	DELETE(pEvent);

			//	if (iCurState == SOC_WAIT_CMD)
			//	{
			//		ONLEAVE_SOC_WAIT_CMD(pThis);
			//	}
			//	else
			//	{
			//		ONLEAVE_SOC_WAIT_ACK(pThis);
			//	}

			//	return SOC_OFF;
			//}
			//INIT_SIMPLE_FRAME_EVENT(pReEvent, ACK);

			///* send it */
			//iRet = Queue_Put(pThis->hEventOutputQueue, &pReEvent, FALSE);
			//if (iRet != ERR_OK)
			//{
			//	LOG_ESR_SSM_E("Put frame event to Output Queue failed");
			//	DELETE(pReEvent);

			//	DELETE(pEvent);  //it is special, so could not use the Macro

			//	if (iCurState == SOC_WAIT_CMD)
			//	{
			//		ONLEAVE_SOC_WAIT_CMD(pThis);
			//	}
			//	else
			//	{
			//		ONLEAVE_SOC_WAIT_ACK(pThis);
			//	}

			//	return SOC_OFF;
			//}

			/* handle splitter frame */
			pCmdData = ESR_ExtractDataFromFrame(frameType,
				pEvent->sData, pEvent->iDataLength, &iCmdDataLen);

			if (iCmdDataLen + (*piRBuffPos) > iRBuffLen - 1)
			{
				/* RBuff is not big enough, clear */

				/* to delete */
				TRACEX("Receive Buffer is not big enough!");
				/* end */

				pRBuff[0] = '\0';
				*piRBuffPos = 0;

				/* restart the timer(for Wait-ACK, just continue) */
				if (iCurState == SOC_WAIT_CMD)
				{
					RESET_WAIT_CMD_TIMER(pThis);
				}
				DELETE(pEvent);
				return iCurState;  //SOC_WAIT_CMD or SOC_WAIT_ACK
			}

			memcpy(pRBuff + (*piRBuffPos), pCmdData, (size_t)iCmdDataLen);
			*piRBuffPos += iCmdDataLen;
			pRBuff[*piRBuffPos] = '\0';

			/* to delete */
			PrintRBuff(pSOCMachineBuff->szReceiveBuff, pSOCMachineBuff->iReceivePos);
			/* end */

			/* restart the timer */
			if (iCurState == SOC_WAIT_CMD)
			{
				RESET_WAIT_CMD_TIMER(pThis);
			}
			DELETE(pEvent);

			///* modified! -- just send Dummy to get more */
			///* send dummy */
			//SSM_SEND_DUMMY(pThis, pEvent);

			/* modified -- send ACK first! 2005-7-12 */
			/* send <ACK> first */
			pEvent = NEW(ESR_EVENT, 1);
			if (pEvent == NULL)
			{
				LOG_ESR_SSM_E("Memory is not enough");

				if (iCurState == SOC_WAIT_CMD)
				{
					ONLEAVE_SOC_WAIT_CMD(pThis);
				}
				else
				{
					ONLEAVE_SOC_WAIT_ACK(pThis);
				}

				return SOC_OFF;
			}
			INIT_SIMPLE_FRAME_EVENT(pEvent, ACK);
			

			/* send it */
			iRet = Queue_Put(pThis->hEventOutputQueue, &pEvent, FALSE);
			if (iRet != ERR_OK)
			{
				LOG_ESR_SSM_E("Put frame event to Output Queue failed");
				DELETE(pEvent);

				if (iCurState == SOC_WAIT_CMD)
				{
					ONLEAVE_SOC_WAIT_CMD(pThis);
				}
				else
				{
					ONLEAVE_SOC_WAIT_ACK(pThis);
				}
				return SOC_OFF;
			}
			/* modified end! 2005-7-12 */

			return iCurState;
		}

	case STX_SOC_OK:
	case STX_SOC_SPLIT_L:

		if (!bSOC)  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		if (STX_SOC_OK == frameType)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_SOC_OK");
		}
		else if (STX_SOC_SPLIT_L == frameType)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_SOC_SPLIT_L");
		}
#endif //_DEBUG_ESR_LINKLAYER

	case STX_RSOC_OK:
	case STX_RSOC_SPLIT_L:

		if (bSOC && (STX_RSOC_OK == frameType ||
			STX_RSOC_SPLIT_L == frameType))  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		if (STX_RSOC_OK == frameType)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_RSOC_OK");
		}
		if (STX_RSOC_SPLIT_L == frameType)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_RSOC_SPLIT_L");
		}
#endif //_DEBUG_ESR_LINKLAYER

	case STX_SOC_SPECIAL:

		if (!bSOC && frameType == STX_SOC_SPECIAL)  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		if (frameType == STX_SOC_SPECIAL)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_SOC_SPECIAL");
		}
#endif //_DEBUG_ESR_LINKLAYER

//		if (IsIdenticalCmd(pSOCMachineBuff, pEvent))
//		{
//
//#ifdef _DEBUG_ESR_LINKLAYER
//			TRACE_ESR_TIPS("Received an identical command");
//#endif //_DEBUG_ESR_LINKLAYER
//
//			/* restart the timer(for Wait-ACK, just continue) */
//			if (iCurState == SOC_WAIT_CMD)
//			{
//				RESET_WAIT_CMD_TIMER(pThis);
//			}
//
//			DELETE(pEvent);
//
//			/* send dummy */
//			SSM_SEND_DUMMY(pThis, pEvent);
//			return iCurState;  //SOC_WAIT_CMD or SOC_WAIT_ACK
//		}
//		else
		{
			/* STX_xx_OK or STX_SOC_SPECIAL process */
			/* send <ACK> first */
			pReEvent = NEW(ESR_EVENT, 1);
			if (pReEvent == NULL)
			{
				LOG_ESR_SSM_E("Memory is not enough");

				if (iCurState == SOC_WAIT_CMD)
				{
					ONLEAVE_SOC_WAIT_CMD(pThis);
				}
				else
				{
					ONLEAVE_SOC_WAIT_ACK(pThis);
				}

				return SOC_OFF;
			}
			INIT_SIMPLE_FRAME_EVENT(pReEvent, ACK);
			if (iCurState == SOC_WAIT_CMD)
			{
				SET_EVENT_SKIP_FLAG(pReEvent);  //will send response later
			}

			/* send it */
			iRet = Queue_Put(pThis->hEventOutputQueue, &pReEvent, FALSE);
			if (iRet != ERR_OK)
			{
				LOG_ESR_SSM_E("Put frame event to Output Queue failed");
				DELETE(pReEvent);

				DELETE(pEvent); //it is special, so could not use the Macro
				if (iCurState == SOC_WAIT_CMD)
				{
					ONLEAVE_SOC_WAIT_CMD(pThis);
				}
				else
				{
					ONLEAVE_SOC_WAIT_ACK(pThis);
				}
				return SOC_OFF;
			}

			/* for STX_SOC_OK or STX_RSOC_OK */
			if (frameType != STX_SOC_SPECIAL)
			{
				pCmdData = ESR_ExtractDataFromFrame(frameType,
					pEvent->sData, pEvent->iDataLength, &iCmdDataLen);

				/* merge the command */
				strncpyz(szCmdData, pRBuff, *piRBuffPos + 1);
				strncat(szCmdData, pCmdData, (size_t)iCmdDataLen);

				/* to delete */
				PrintRBuff(szCmdData, strlen(szCmdData));
				/* end */

				/* clear RBuff */
				pRBuff[0] = '\0';
				*piRBuffPos = 0;

				/* perform the command */
				ESR_DecodeAndPerform(pThis, szCmdData);
			}
			else  //for STX_SOC_SPECIAL
			{
				bSOCSpecialCmd = HandleSpecialCmd(pThis, pEvent, pThis->szCmdRespBuff);
			}

			/* get [soc_proc] */
			if (bSOC)
			{
				strncpyz(szSoc_proc, pEvent->sData + 12, 6);
			}

			/* put to TBuff */
			InsertMsgToTB(bSOC,	pThis->szCmdRespBuff, 
				szSoc_proc, pSOCMachineBuff, bSOCSpecialCmd );

			/* clear RespBuff now */
			pThis->szCmdRespBuff[0] = '\0';

			/* send TBuff Msg if necessary */
			if (iCurState == SOC_WAIT_CMD)
			{
				int iCurPos;

				if (pSOCMachineBuff->iMsgsInTB == 0)
				{
					RESET_WAIT_CMD_TIMER(pThis);

					DELETE(pEvent);
					return SOC_WAIT_CMD;
				}

				iCurPos = pSOCMachineBuff->iCurTBSendPos;
				pReEvent = NEW(ESR_EVENT, 1);
				if (pReEvent == NULL)
				{
					LOG_ESR_SSM_E("Memory is not enough");
					ONLEAVE_SOC_WAIT_CMD(pThis);
					return SOC_OFF;
				}
				INIT_FRAME_EVENT(pReEvent, 
					strlen(pSOCMachineBuff->szTransmitBuff[iCurPos]),
					pSOCMachineBuff->szTransmitBuff[iCurPos],
					FALSE,
					FALSE);

				/* send it */
				iRet = Queue_Put(pThis->hEventOutputQueue, &pReEvent, FALSE);
				if (iRet != ERR_OK)
				{
					LOG_ESR_SSM_E("Put frame event to Output Queue failed");
					DELETE(pReEvent);

					DELETE(pEvent); //it is special, so could not use the Macro
					ONLEAVE_SOC_WAIT_CMD(pThis);
					return SOC_OFF;
				}

				pSOCMachineBuff->iTBReSendCounter = 0;

				/* kill Wait-CMD timer */
				KILL_WAIT_CMD_TIMER(pThis);

				/* set Wait-ACK timer */
				SET_WAIT_ACK_TIMER(pThis);
			}
			DELETE(pEvent);
			return SOC_WAIT_ACK;  
		}

	case STX_SOC_LRC:

		if (!bSOC)  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: STX_SOC_LRC");
#endif //_DEBUG_ESR_LINKLAYER

	case STX_RSOC_LRC:

		if (bSOC && frameType == STX_RSOC_LRC)  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		if (frameType == STX_RSOC_LRC)
		{
			TRACE_ESR_TIPS("Received Frame Event: STX_RSOC_LRC");
		}
#endif //_DEBUG_ESR_LINKLAYER

		/* STX_xx_LRC process */
		/* create Frame event containing "<NAK>" */
		pReEvent = NEW(ESR_EVENT, 1);
		if (pReEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			if (iCurState == SOC_WAIT_CMD)
			{
				ONLEAVE_SOC_WAIT_CMD(pThis);
			}
			else
			{
				ONLEAVE_SOC_WAIT_ACK(pThis);
			}
			return SOC_OFF;
		}
		INIT_SIMPLE_FRAME_EVENT(pReEvent, NAK);

		/* send it */
		iRet = Queue_Put(pThis->hEventOutputQueue, &pReEvent, FALSE);
		if (iRet != ERR_OK)
		{
			LOG_ESR_SSM_E("Put frame event to Output Queue failed");
			DELETE(pReEvent);

			DELETE(pEvent); //it is special, so could not use the Macro
			if (iCurState == SOC_WAIT_CMD)
			{
				ONLEAVE_SOC_WAIT_CMD(pThis);
			}
			else
			{
				ONLEAVE_SOC_WAIT_ACK(pThis);
			}
			return SOC_OFF;
		}

		/* reset the timer(for Wait-ACK, just continue) */
		if (iCurState == SOC_WAIT_CMD)
		{
			RESET_WAIT_CMD_TIMER(pThis);
		}
		DELETE(pEvent);
		return iCurState;  //SOC_WAIT_CMD or SOC_WAIT_ACK

	case STX_SOC_ADDR:

		if (!bSOC)  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: STX_SOC_ADDR");
#endif //_DEBUG_ESR_LINKLAYER

		
		/* reset the timer(for Wait-ACK, just continue) */
		if (iCurState == SOC_WAIT_CMD)
		{
			RESET_WAIT_CMD_TIMER(pThis);
		}
		DELETE(pEvent);

		/* send dummy */
		SSM_SEND_DUMMY(pThis, pEvent);

		return iCurState;  //SOC_WAIT_CMD or SOC_WAIT_ACK

	case STX_RSOC_ADDR:  

		if (bSOC)  //unexpected frame
		{
			break;
		}

#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: STX_RSOC_ADDR");
#endif //_DEBUG_ESR_LINKLAYER

		/* seems strange:), need to send <ACK> first */
		//pReEvent = NEW(ESR_EVENT, 1);
		//if (pReEvent == NULL)
		//{
		//	LOG_ESR_SSM_E("Memory is not enough");
		//	if (iCurState == SOC_WAIT_CMD)
		//	{
		//		ONLEAVE_SOC_WAIT_CMD(pThis);
		//	}
		//	else
		//	{
		//		ONLEAVE_SOC_WAIT_ACK(pThis);
		//	}
		//	return SOC_OFF;
		//}
		//INIT_SIMPLE_FRAME_EVENT(pReEvent, ACK);
		//SET_EVENT_SKIP_FLAG(pReEvent);  //will send response later

		///* send it */
		//iRet = Queue_Put(pThis->hEventOutputQueue, &pReEvent, FALSE);
		//if (iRet != ERR_OK)
		//{
		//	LOG_ESR_SSM_E("Put frame event to Output Queue failed");
		//	DELETE(pReEvent);

		//	DELETE(pEvent); //it is special, so could not use the Macro
		//	if (iCurState == SOC_WAIT_CMD)
		//	{
		//		ONLEAVE_SOC_WAIT_CMD(pThis);
		//	}
		//	else
		//	{
		//		ONLEAVE_SOC_WAIT_ACK(pThis);
		//	}
		//	return SOC_OFF;
		//}


		/* reset the timer(for Wait-ACK, just continue) */
		if (iCurState == SOC_WAIT_CMD)
		{
			RESET_WAIT_CMD_TIMER(pThis);
		}

		DELETE(pEvent);

		/* send dummy */
		SSM_SEND_DUMMY(pThis, pEvent);

		return iCurState;  //SOC_WAIT_CMD or SOC_WAIT_ACK
		
	default:  //ignore others
		break;
	}

	/* unexpected frame event process */
#ifdef _DEBUG_ESR_LINKLAYER
	TRACE_ESR_TIPS("Received unexpected frame event");
	TRACE("Frame Type: %d\n", frameType);
#endif //_DEBUG_ESR_LINKLAYER
	
	DELETE_ESR_EVENT(pEvent);

	/* send dummy */
	SSM_SEND_DUMMY(pThis, pEvent);

	return iCurState;  //continue SOC_WAIT_CMD or SOC_WAIT_ACK
}


/*==========================================================================*
 * FUNCTION : ResendDataOfTBuff
 * PURPOSE  : resend the last item of TBuff
 * CALLS    : 
 * CALLED BY: OnWaitCmdOrACK_HandleFrameEvent
 *			  SOC_OnWaitACK
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-07 15:41
 *==========================================================================*/
static int ResendDataOfTBuff(ESR_BASIC_ARGS *pThis)
{
	int iCurPos;
	ESR_EVENT *pEvent;

	SOC_MACHINE_BUFF *pSOCMachineBuff;

	pSOCMachineBuff = &pThis->SOCMachineBuff;
	if (++(pSOCMachineBuff->iTBReSendCounter) <= SOC_RETRY_TIMES)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Resend the frame");
		TRACE("Resend counter is: %d\n", pSOCMachineBuff->iTBReSendCounter);
#endif //_DEBUG_ESR_LINKLAYER
		iCurPos = pSOCMachineBuff->iCurTBSendPos;

		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			ONLEAVE_SOC_WAIT_ACK(pThis);
			return SOC_OFF;
		}
		INIT_FRAME_EVENT(pEvent, 
			strlen(pSOCMachineBuff->szTransmitBuff[iCurPos]),
			pSOCMachineBuff->szTransmitBuff[iCurPos],
			FALSE,
			FALSE);

		/* skip the next ComMC calling for synchronization*/
		SET_EVENT_SKIP_FLAG(pEvent);

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);

		RESET_WAIT_ACK_TIMER(pThis);		
		return SOC_WAIT_ACK;

	}
	else
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Resend failed, throw current frame, move to the next");
#endif //_DEBUG_ESR_LINKLAYER
		/* move to the next item */
		MOVE_TBUFF_SENDPOS(pSOCMachineBuff);

		if (--(pSOCMachineBuff->iMsgsInTB) == 0)  //no more items need to send
		{
			/* kill the Wait-ACK timer */
			KILL_WAIT_ACK_TIMER(pThis);

			/* set the Wait-CMD timer */
			SET_WAIT_CMD_TIMER(pThis);
			return SOC_WAIT_CMD;
		}

		/* send the next item */
		iCurPos = pSOCMachineBuff->iCurTBSendPos;

		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			ONLEAVE_SOC_WAIT_ACK(pThis);
			return SOC_OFF;
		}
		INIT_FRAME_EVENT(pEvent, 
			strlen(pSOCMachineBuff->szTransmitBuff[iCurPos]),
			pSOCMachineBuff->szTransmitBuff[iCurPos],
			FALSE,
			FALSE);

		/* skip the next ComMC calling for synchronization*/
		SET_EVENT_SKIP_FLAG(pEvent);

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);

		/* reset the counter */
		pSOCMachineBuff->iTBReSendCounter = 0;

		RESET_WAIT_ACK_TIMER(pThis);
		return SOC_WAIT_ACK;
	}
}


/*==========================================================================*
 * FUNCTION : OnWaitCmdOrACK_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : HandleSTXFrame
 *			  ResendDataOfTBuff
 * CALLED BY: SOC_OnWaitCmd
 *			  SOC_OnWaitACK
 * ARGUMENTS: int			  iCurState: SOC_WAIT_CMD or SOC_WAIT_ACK
 *			  ESR_BASIC_ARGS  *pThis   : 
 *            ESR_EVENT       *pEvent  : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-07 15:57
 *==========================================================================*/
static int OnWaitCmdOrACK_HandleFrameEvent(int iCurState,
										   ESR_BASIC_ARGS *pThis,
										   ESR_EVENT *pEvent)
{
	FRAME_TYPE frameType;
	BOOL  bNeedResetTimer;
	int iProtocolType, iCurPos;
	SOC_MACHINE_BUFF *pSOCMachineBuff;

	pSOCMachineBuff = &pThis->SOCMachineBuff;
	iProtocolType = g_EsrGlobals.CommonConfig.iProtocolType;
	bNeedResetTimer = FALSE;


	frameType = ESR_AnalyseFrame(pEvent->sData, pEvent->iDataLength, pThis);

	switch (frameType)
	{
	case FRAME_ENQ: //clear buff and send DLE0
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: <ENQ>");
#endif //_DEBUG_ESR_LINKLAYER
		DELETE(pEvent);

		/* clear RBuff and TBuff */
		ClearSOCMachineBuf(pSOCMachineBuff);


#ifdef _SOC_WAINT_CMD_ALARM_REPORT
		/* alarm report in WAIT_CMD status */
		if (iCurState == SOC_WAIT_CMD && iProtocolType == RSOC 
			&& g_EsrGlobals.CommonConfig.iMediaType == MEDIA_TYPE_LEASED_LINE
			&& pThis->alarmHandler.bAlarmAtHand)
		{
			pEvent = &g_EsrDiscEvent;

			ESR_SendAlarm(pThis);
			SSM_SEND_SEVENT(pThis, pEvent);
			return SOC_SEND_ENQ;
		}
#endif

		/* create Frame event containing <DLE0> */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			return SOC_OFF;
		}
		INIT_FRAME_EVENT(pEvent, 2, DLE0, 0, 0);

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);	

		if (iCurState == SOC_WAIT_ACK)
		{
			KILL_WAIT_ACK_TIMER(pThis);
			SET_WAIT_CMD_TIMER(pThis);
		}
		else
		{
			RESET_WAIT_CMD_TIMER(pThis);
		}

		return SOC_WAIT_CMD;  

	case FRAME_EOT:  //go to OFF state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: <EOT>");
#endif //_DEBUG_ESR_LINKLAYER
		DELETE(pEvent);

		/* clear RBuff and TBuff */
		ClearSOCMachineBuf(pSOCMachineBuff);

		/* kill the timer */
		iCurState == SOC_WAIT_CMD ? KILL_WAIT_CMD_TIMER(pThis) : 
			KILL_WAIT_ACK_TIMER(pThis);

		/* send dummy for synchronization */
		SSM_SEND_DUMMY(pThis, pEvent);

		return SOC_OFF;

	case FRAME_DLE0:  //only when on SOC_WAIT_CMD
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: <DLE0>");
#endif //_DEBUG_ESR_LINKLAYER

		if (iCurState == SOC_WAIT_ACK)  //unexpected
		{
			break;
		}

		DELETE(pEvent);
		RESET_WAIT_CMD_TIMER(pThis);
		return SOC_WAIT_CMD;


	case FRAME_NAK:  //only when on SOC_WAIT_ACK state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: <NAK>");
#endif //_DEBUG_ESR_LINKLAYER

		if (iCurState == SOC_WAIT_CMD)  //unexpected
		{
			break;
		}

		DELETE(pEvent);

		/* resend the last item of TBuff */
		return ResendDataOfTBuff(pThis);


	case FRAME_ACK: //only when on SOC_WAIT_ACK state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Frame Event: <ACK>");
#endif //_DEBUG_ESR_LINKLAYER

		if (iCurState == SOC_WAIT_CMD)  //unexpected
		{
			break;
		}

		DELETE(pEvent);
		pSOCMachineBuff = &pThis->SOCMachineBuff;

		/* move to the next item */
		MOVE_TBUFF_SENDPOS(pSOCMachineBuff);

		if (--(pSOCMachineBuff->iMsgsInTB) == 0)  //no more items need to send
		{
			KILL_WAIT_ACK_TIMER(pThis);
			SET_WAIT_CMD_TIMER(pThis);

			/* send dummy for synchronization */
			SSM_SEND_DUMMY(pThis, pEvent);
		
			return SOC_WAIT_CMD;
		}

		/* send the next item */
		iCurPos = pSOCMachineBuff->iCurTBSendPos;
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			ONLEAVE_SOC_WAIT_ACK(pThis);
			return SOC_OFF;
		}
		INIT_FRAME_EVENT(pEvent, 
			strlen(pSOCMachineBuff->szTransmitBuff[iCurPos]),
			pSOCMachineBuff->szTransmitBuff[iCurPos],
			FALSE,
			FALSE);

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);

		/* reset the counter */
		pSOCMachineBuff->iTBReSendCounter = 0;

		/* restart the Wait-ACK timer */
		RESET_WAIT_ACK_TIMER(pThis);
		
		return SOC_WAIT_ACK;

	default:
		break;
	}

	/* handled by STX frame handler */
	if (iProtocolType == SOC_TPE)
	{
		return HandleSTXFrame(iCurState, TRUE, frameType, pThis, pEvent);
	}
	else  //RSOC protocol
	{
		return HandleSTXFrame(iCurState, FALSE, frameType, pThis, pEvent);
	}
	
}


/*==========================================================================*
 * FUNCTION : SOC_OnWaitCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-07 14:11
 *==========================================================================*/
int SOC_OnWaitCmd(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;
	SOC_MACHINE_BUFF *pSocBuf;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);
	pSocBuf = &pThis->SOCMachineBuff;

	/* 1.ten seconds timeout check */
	if (pSocBuf->bWaitCmdTimeout)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Wait Command timeout, go to OFF state");
#endif //_DEBUG_ESR_LINKLAYER

		ONLEAVE_SOC_WAIT_CMD(pThis);

		/* create Frame Event containing <EOT> */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_SSM_E("Memory is not enough");
			return SOC_OFF;
		}
		INIT_SIMPLE_FRAME_EVENT(pEvent, EOT);

		/* skip the next ComMC calling for synchronization*/
		SET_EVENT_SKIP_FLAG(pEvent);

		/* send it */
		SSM_SEND_DEVENT(pThis, pEvent);

		return SOC_OFF;
	}

	/* 2.get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		//TRACE_ESR_TIPS("Event Input Queue is empty, keep on waiting");
#endif //_DEBUG_ESR_LINKLAYER

		return SOC_WAIT_CMD;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_ESR_SSM_E("Get from InputQueue failed for invalid args");

		ONLEAVE_SOC_WAIT_CMD(pThis);
		return SOC_OFF;
	}

	/* now iRet is ERR_OK */
	switch (pEvent->iEventType)
	{

	case ESR_FRAME_EVENT:  //frame event process
		return OnWaitCmdOrACK_HandleFrameEvent(SOC_WAIT_CMD, pThis, pEvent);

	case ESR_DISCONNECTED_EVENT: //return to OFF state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/
		ONLEAVE_SOC_WAIT_CMD(pThis);

		return SOC_OFF;  

	case ESR_TIMEOUT_EVENT:  //keep wait after send dummy until 10s is up
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Timeout Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/
		break;

	default:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);
		break;
	}

	/* send dummy for synchronization */
	SSM_SEND_DUMMY(pThis, pEvent);

	return SOC_WAIT_CMD;	
	
}


/*==========================================================================*
 * FUNCTION : SOC_OnWaitACK
 * PURPOSE  : 
 * CALLS    : ResendDataOfTBuff
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next SOC State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-12-08 17:03
 *==========================================================================*/
int SOC_OnWaitACK(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;
	SOC_MACHINE_BUFF *pSocBuf;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	pSocBuf = &pThis->SOCMachineBuff;

	/* 1.three seconds timeout check */
	if (pSocBuf->bWaitACKTimeout)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Wait Command Replay ACK timeout");
#endif //_DEBUG_ESR_LINKLAYER

		return ResendDataOfTBuff(pThis);
	}


	//Sleep(100);
	/* 1.get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		//TRACE_ESR_TIPS("Event Input Queue is empty, keep on waiting");
#endif //_DEBUG_ESR_LINKLAYER
		
		return SOC_WAIT_ACK;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_ESR_SSM_E("Get from InputQueue failed for invalid args");

		ONLEAVE_SOC_WAIT_ACK(pThis);
		return SOC_OFF;
	}

	/* now iRet is ERR_OK */
	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:  //frame event process
		return OnWaitCmdOrACK_HandleFrameEvent(SOC_WAIT_ACK, pThis, pEvent);

	case ESR_DISCONNECTED_EVENT:  //go to OFF state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/
		ONLEAVE_SOC_WAIT_ACK(pThis);
		return SOC_OFF;  

	case ESR_TIMEOUT_EVENT:  //keep wait after send dummy until 3s is up
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Timeout Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/
		break;

	default:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE_ESR_EVENT(pEvent);
		break;
	}

	/* send dummy for synchronization */
	//modified by Jimmy for TR
	SSM_SEND_DUMMY(pThis, pEvent);	

	return SOC_WAIT_ACK;	
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : eem_state_machine.c
 *  CREATOR  : LinTao                   DATE: 2004-11-17 16:26
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "esr.h"

/* command data length of EEM Select Frame */
#define CMD_DATA_LEN  512


/* timeout = READCOM_TIMEOUT * timeout counter */
#define EEM_WAITACK_TIMEOUT_C	3   //wait ACK timeout counter, means 3s
#define EEM_WAITPOLL_TIMEOUT_C	5	//wait POLL timeout counter, means 6s
#define EEM_RESEND_TIMES		4	//resend times(response and alarm frames)


/* some operations */
#define IS_WAITACK_TIMEOUT(_pThis) \
	(++(_pThis)->iTimeoutCounter > EEM_WAITACK_TIMEOUT_C)

#define RESET_TIMEOUT_COUNTER(_pThis) \
	((_pThis)->iTimeoutCounter = 0)

#define NEED_RESEND(_pThis) \
	(++(_pThis)->iResendCounter <= EEM_RESEND_TIMES)

#define RESET_RESEND_COUNTER(_pThis) \
	((_pThis)->iResendCounter = 0)


/* log Macros (ESM = eem state machine) */
#define ESR_TASK_ESM		"EEM State Machine"

/* error log */
#define LOG_ESR_ESM_E(_szLogText)  LOG_ESR_E(ESR_TASK_ESM, _szLogText)

/* warning log */
#define LOG_ESR_ESM_W(_szLogText)  LOG_ESR_W(ESR_TASK_ESM, _szLogText) 

/* info log */
#define LOG_ESR_ESM_I(_szLogText)  LOG_ESR_I(ESR_TASK_ESM, _szLogText)


/* assistant functional Macros */
/* create and send Dummy frame event */
#define ESM_SEND_DUMMY(_pThis, _pEvent)  \
	do {  \
	    (_pEvent) = &g_EsrDummyEvent; \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_ESR_ESM_E("Put frame event to Output Queue failed"); \
			return EEM_IDLE;    \
		}  \
	}  \
	while (0)


/* send static ESR event to the OutputQueue */
#define ESM_SEND_SEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_ESR_ESM_E("Put frame event to Output Queue failed"); \
			return EEM_IDLE;    \
		}  \
	}  \
	while (0)

/* send dynamic ESR event to the OutputQueue */
#define ESM_SEND_DEVENT(_pThis, _pEvent)  \
	do {  \
		if (Queue_Put((_pThis)->hEventOutputQueue, &(_pEvent), FALSE) != \
				ERR_OK ) \
		{  \
			LOG_ESR_ESM_E("Put frame event to Output Queue failed"); \
			DELETE((_pEvent));  \
			return EEM_IDLE;    \
		}  \
	}  \
	while (0)


__INLINE static void EEM_OnDisconnectedEvent(ESR_BASIC_ARGS *pThis)
{
	/* clear the flags and the buf */
	pThis->iTimeoutCounter = 0;
	pThis->iResendCounter = 0;

	pThis->szCmdRespBuff[0] = '\0';

}

/*==========================================================================*
 * FUNCTION : OnIdle_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: EEM_OnIdle
 * ARGUMENTS: ESR_BASIC_ARGS *pThis:
 *			  ESR_EVENT  *pEvent   : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 20:02
 *==========================================================================*/
static int OnIdle_HandleFrameEvent(ESR_BASIC_ARGS *pThis, ESR_EVENT *pEvent)
{
	FRAME_TYPE frameType;
	const unsigned char *sFrameData, *pCmdData;
	unsigned char szCmdData[CMD_DATA_LEN];
	int iFrameDataLen, iCmdDataLen;

	sFrameData = pEvent->sData;
	iFrameDataLen = pEvent->iDataLength;


	/* frame analyse */
	frameType = ESR_AnalyseFrame(sFrameData, iFrameDataLen, pThis);

	switch (frameType)
	{
	case SELECT_OK:  //reply <ACK>

#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received frame event: SELECT_OK");
#endif //_DEBUG_ESR_LINKLAYER

#ifdef _DEBUG_ESR_CMD_HANDLER
		ESR_PrintEvent(pEvent);
#endif //_DEBUG_ESR_CMD_HANDLER

		/* extract data field from select frame */
		pCmdData = ESR_ExtractDataFromFrame(SELECT_OK, 
			sFrameData, iFrameDataLen, &iCmdDataLen);

		/* should not happen! */
		if (iCmdDataLen + 1 > CMD_DATA_LEN)
		{
			LOG_ESR_ESM_E("CMD_DATA_LEN(512) is not big enough");

			DELETE(pEvent);
			return EEM_IDLE;
		}

		/* note: add '\0' at the end of szCmdData */
		strncpyz(szCmdData, pCmdData, iCmdDataLen + 1);

		/* decode and perform it */
		ESR_DecodeAndPerform(pThis, szCmdData);
		DELETE(pEvent);

		/* create Frame event containing "<ACK>" */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, ACK);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_WAIT_FOR_POLL;

	case SELECT_BCC:  //send <NAK>
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: SELECT_BCC");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);
		
		/* create Frame event containing "<NAK>" */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, NAK);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_IDLE;

	case POLL_OK: // send <EOT>
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: POLL_OK");
#endif //_DEBUG_ESR_LINKLAYER
	
		DELETE(pEvent);

		/* create frame event containing <EOT> */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, EOT);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_IDLE;

	case FRAME_ERR_PROT:  //handle by third-party handler
		
#ifdef COM_SHARE_SERVICE_SWITCH
		{
			SERVICE_SWITCH_FLAG *pFlag;
			pFlag = DXI_GetServiceSwitchFlag();

			if (ESR_IsMtnFrame(sFrameData, iFrameDataLen))
			{
#ifdef _DEBUG_ESR_LINKLAYER
				TRACE_ESR_TIPS("received request frame event from PowerKit");
#endif //_DEBUG_ESR_LINKLAYER

				DELETE(pEvent);

				/* set the flag */
				pFlag->bHLMSUseCOM = FALSE;
	
				/* replay to PowerKit */
				pEvent = NEW(ESR_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_ESR_ESM_E("Memory is not enough");
					return EEM_IDLE;
				}

				INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, TRUE);
				ESR_BuildMtnResponseFrame(pEvent->sData, &pEvent->iDataLength);

							   
				/* send it */
				ESM_SEND_DEVENT(pThis, pEvent);

					
				/* create Disconnect event */
				Sleep(500);
				pEvent = &g_EsrDiscEvent;
				/*pEvent = NEW(ESR_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_ESR_ESM_E("Memory is not enough");
					return EEM_IDLE;
				}
				INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);*/

				/* send it */
				ESM_SEND_SEVENT(pThis, pEvent);
			

				/* exit the service */
				pThis->iLinkLayerThreadExitCmd = SERVICE_EXIT_OK;
				pThis->iInnerQuitCmd = SERVICE_EXIT_OK;

				return EEM_IDLE;
			}
		}
#endif //COM_SHARE_SERVICE_SWITCH


#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: FRAME_ERR_PROT");
#endif //_DEBUG_ESR_LINKLAYER
		DELETE(pEvent);
		ESM_SEND_DUMMY(pThis, pEvent);
		return EEM_IDLE;
		

	default:  //unexpected frame event
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received unexpected frame event");
		TRACE("\tFrame Type: %d\n", frameType);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);
		ESM_SEND_DUMMY(pThis, pEvent);

		return EEM_IDLE;
	}
}


/*==========================================================================*
 * FUNCTION : EEM_OnIdle
 * PURPOSE  : 
 * CALLS    : ESR_SendAlarm
 *			  OnIdle_HandleFrameEvent
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-17 19:23
 *==========================================================================*/
int EEM_OnIdle(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;

	ALARM_HANDLER *pAlarmHandler;
	pAlarmHandler = &pThis->alarmHandler; 

	BOOL bStopCommForAlarm;  //need to stop communication for alarm occurs


	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* alarm and callback process */
	if (!pThis->bCommRunning && pAlarmHandler->bAlarmAtHand)
	{
		/* reset timeout counter */
		pThis->iTimeoutCounter = 0; 

		ESR_SendAlarm(pThis);
		return EEM_SEND_ALARM;
	}

	if (!pThis->bCommRunning && pAlarmHandler->bCallbackAtHand)
	{
		/* reset timeout counter */
		pThis->iTimeoutCounter = 0; 

		ESR_SendCallback(pThis);

		return EEM_SEND_CALLBACK;
	}

	/* get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	if (iRet == ERR_QUEUE_EMPTY)  
	{
		/* In Idel state, it means timeout, keep wait in next Idle state */
		return EEM_IDLE;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_ESR_ESM_E("Get from Input Queue failed(invalid args)");
		return EEM_IDLE;
	}

	/* reset timeout counter */
	if (pEvent->iEventType != ESR_TIMEOUT_EVENT)
	{
		RESET_TIMEOUT_COUNTER(pThis);
	}

	/* get the flag */
	bStopCommForAlarm = ((pAlarmHandler->bAlarmAtHand || 
		pAlarmHandler->bCallbackAtHand) && 
		pThis->iOperationMode == ESR_MODE_SERVER) ? TRUE : FALSE;

	/* now iRet is ERR_OK */
	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:  //frame event process
		return OnIdle_HandleFrameEvent(pThis, pEvent);
	
	case ESR_TIMEOUT_EVENT:  //timeout event process, send disconnect event
		/*DELETE(pEvent);*/

		if (++pThis->iTimeoutCounter > ESR_MAX_TIMEOUTCOUNT || 
			bStopCommForAlarm)
		{
			int iNextState = EEM_IDLE;

#ifdef _DEBUG_ESR_LINKLAYER
			TRACE_ESR_TIPS("keep Timeout too long or have alarm/callback to "
				"report, will dissconnect current communication actively");
#endif //_DEBUG_ESR_LINKLAYER

			pThis->iTimeoutCounter = 0;  //reset first

			/* create Disconnect event */
			pEvent = &g_EsrDiscEvent;
			/*pEvent = NEW(ESR_EVENT, 1);
			if (pEvent == NULL)
			{
				LOG_ESR_ESM_E("Memory is not enough");
				return EEM_IDLE;
			}
			INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);*/

			if (g_EsrGlobals.CommonConfig.iMediaType == 
				MEDIA_TYPE_LEASED_LINE && bStopCommForAlarm)
			{
				/* not need callback when using Leased Line */
				/* set flag first, insure change to the client mode */
				ESR_SendAlarm(pThis);
				iNextState = EEM_SEND_ALARM;
			}

			/* send it */
			ESM_SEND_SEVENT(pThis, pEvent);

			return iNextState;
		}
		else  //just send dummy
		{
			ESM_SEND_DUMMY(pThis, pEvent);
			return EEM_IDLE;
		}

	case ESR_DISCONNECTED_EVENT:  //Disconnected Event
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/

		/* wait in another Idle state */
		return EEM_IDLE;

	default: //unexpected event
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);
		return EEM_IDLE;
	}	

}


/*==========================================================================*
 * FUNCTION : OnWaitForPoll_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis  : 
 *            ESR_EVENT       *pEvent : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-18 13:53
 *==========================================================================*/
static int OnWaitForPoll_HandleFrameEvent(ESR_BASIC_ARGS *pThis, 
										  ESR_EVENT *pEvent)
{
	FRAME_TYPE frameType;
	const unsigned char *sFrameData, *pCmdData;
	unsigned char szCmdData[CMD_DATA_LEN];
	int iFrameDataLen, iCmdDataLen;

	sFrameData = pEvent->sData;
	iFrameDataLen = pEvent->iDataLength;

	/* frame analyse */
	frameType = ESR_AnalyseFrame(sFrameData, iFrameDataLen, pThis);

	switch (frameType)
	{
	case SELECT_OK:  //reply <ACK>
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: SELECT_OK");
#endif //_DEBUG_ESR_LINKLAYER

		/* extract data field from select frame */
		pCmdData = ESR_ExtractDataFromFrame(SELECT_OK, 
			sFrameData, iFrameDataLen, &iCmdDataLen);

		/* should not happen! */
		if (iCmdDataLen + 1 > CMD_DATA_LEN)
		{
			LOG_ESR_ESM_E("CMD_DATA_LEN(512) is not big enough");

			DELETE(pEvent);
			return EEM_IDLE;
		}

		/* note: add '\0' at the end of szCmdData */
		strncpyz(szCmdData, pCmdData, iCmdDataLen + 1);

		/* decode and perform it */
		ESR_DecodeAndPerform(pThis, szCmdData);
		DELETE(pEvent);

		/* create Frame event containing "<ACK>" */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, ACK);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_WAIT_FOR_POLL;

	case SELECT_BCC:  //send <NAK>
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: SELECT_BCC");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);
		
		/* create Frame event containing "<NAK>" */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, NAK);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_IDLE;

	case POLL_OK: // send Response frame
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: POLL_OK");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);

		/* create frame event */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

		/* build response frame */
		ESR_BuildEEMResponseFrame(pThis->szCmdRespBuff, 
			pEvent->sData, &pEvent->iDataLength);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		/* add resend counter */
		pThis->iResendCounter++;

		return EEM_WAIT_FOR_RESP_ACK;


	case FRAME_EOT:  //go to EEM_IDLE state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: FRAME_EOT");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);
		ESM_SEND_DUMMY(pThis, pEvent);
		return EEM_IDLE;

	default:  //unexpected frame event
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received unexpected frame event");
		TRACE("\tFrame Type: %d\n", frameType);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);

		ESM_SEND_DUMMY(pThis, pEvent);
		return EEM_WAIT_FOR_POLL;
	}
}


/*==========================================================================*
 * FUNCTION : EEM_OnWaitForPoll
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-18 11:45
 *==========================================================================*/
int EEM_OnWaitForPoll(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
		/* timeout, keep on waiting */
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Event Input Queue is empty");
#endif //_DEBUG_ESR_LINKLAYER

		return EEM_WAIT_FOR_POLL;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_ESR_ESM_E("Get from Input Queue failed(invalid args)");
		return EEM_IDLE;
	}
	
	/* reset timeout counter */
	if (pEvent->iEventType != ESR_TIMEOUT_EVENT)
	{
		RESET_TIMEOUT_COUNTER(pThis);
	}

	/* now iRet is OK */
	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:  //frame event process
		return OnWaitForPoll_HandleFrameEvent(pThis, pEvent);

	case ESR_TIMEOUT_EVENT:  //keep on waiting
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received Timeout Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/
		
		/* avoid locked in EEM_WAIT_FOR_POLL state */
		if (++pThis->iTimeoutCounter > EEM_WAITPOLL_TIMEOUT_C)
		{
			pThis->iTimeoutCounter = 0;  //reset first
			ESM_SEND_DUMMY(pThis, pEvent);
			return EEM_IDLE;
		}

		/* keep on waiting */
		ESM_SEND_DUMMY(pThis, pEvent);
		return EEM_WAIT_FOR_POLL;

	case ESR_DISCONNECTED_EVENT:  //return to Idle state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/

		return EEM_IDLE;

	default:  //ignore others
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);
		return EEM_WAIT_FOR_POLL;
	}	
}


/*==========================================================================*
 * FUNCTION : OnWaitForRespACK_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: EEM_OnWaitForRespACK
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis  : 
 *            ESR_EVENT       *pEvent : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-18 15:28
 *==========================================================================*/
static int OnWaitForRespACK_HandleFrameEvent(ESR_BASIC_ARGS *pThis, 
											 ESR_EVENT *pEvent)
{
	FRAME_TYPE frameType;
	const unsigned char *sFrameData, *pCmdData;
	unsigned char szCmdData[CMD_DATA_LEN];
	int iFrameDataLen, iCmdDataLen;

	sFrameData = pEvent->sData;
	iFrameDataLen = pEvent->iDataLength;

	/* frame analyse */
	frameType = ESR_AnalyseFrame(sFrameData, iFrameDataLen, pThis);

	switch (frameType)
	{
	case SELECT_OK:  //reply <ACK>, change to EEM_WAIT_FOR_POLL state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: SELECT_OK");
#endif //_DEBUG_ESR_LINKLAYER

		/* clear resend counter */
		pThis->iResendCounter = 0;

		/* extract data field from select frame */
		pCmdData = ESR_ExtractDataFromFrame(SELECT_OK, 
			sFrameData, iFrameDataLen, &iCmdDataLen);

		/* should not happen! */
		if (iCmdDataLen + 1 > CMD_DATA_LEN)
		{
			LOG_ESR_ESM_E("CMD_DATA_LEN(512) is not big enough");

			DELETE(pEvent);
			return EEM_IDLE;
		}

		/* note: add '\0' at the end of szCmdData */
		strncpyz(szCmdData, pCmdData, iCmdDataLen + 1);

		/* decode and perform it */
		ESR_DecodeAndPerform(pThis, szCmdData);
		DELETE(pEvent);

		/* create Frame event containing "<ACK>" */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, ACK);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_WAIT_FOR_POLL;

	case SELECT_BCC:  //send <NAK>, change to EEM_IDLE state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: SELECT_BCC");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);
		
		/* clear resend counter */
		pThis->iResendCounter = 0;

		/* create Frame event containing "<NAK>" */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, NAK);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_IDLE;


	case POLL_OK: // send response frame again
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: POLL_OK");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);

		/* clear resend counter */
		pThis->iResendCounter = 0;

		/* create frame event */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

		/* build response frame */
		ESR_BuildEEMResponseFrame(pThis->szCmdRespBuff, 
			pEvent->sData, &pEvent->iDataLength);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_WAIT_FOR_RESP_ACK;

	case FRAME_ACK:  //send <EOT>, go to EEM_IDLE state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: FRAME_ACK");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);

		/* clear resend counter */
		pThis->iResendCounter = 0;

		/* clear response buf */
		pThis->szCmdRespBuff[0] = '\0';
		
		/* create frame event containing <EOT> */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, EOT);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_IDLE;

	case FRAME_NAK:  //send response frame again
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: FRAME_NAK");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);

		/* clear resend counter */
		pThis->iResendCounter = 0;

		/* create frame event */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			LOG_ESR_ESM_E("Memory is not enough");
			return EEM_IDLE;
		}

		INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

		/* build response frame */
		ESR_BuildEEMResponseFrame(pThis->szCmdRespBuff, 
			pEvent->sData, &pEvent->iDataLength);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_WAIT_FOR_RESP_ACK;

	case FRAME_EOT:  //go to Idle state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received frame event: FRAME_EOT");
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);
		ESM_SEND_DUMMY(pThis, pEvent);
		return EEM_IDLE;

	default:  //unexpected frame event
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received unexpected frame event");
		TRACE("\tFrame Type: %d\n", frameType);
#endif //_DEBUG_ESR_LINKLAYER
		DELETE_ESR_EVENT(pEvent);

		return EEM_WAIT_FOR_RESP_ACK;
	}
}


/*==========================================================================*
 * FUNCTION : EEM_OnWaitForRespACK
 * PURPOSE  : 
 * CALLS    : OnWaitForRespACK_HandleFrameEvent
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int  : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-18 15:07
 *==========================================================================*/
int EEM_OnWaitForRespACK(ESR_BASIC_ARGS *pThis)
{
	int iRet;
	ESR_EVENT  *pEvent;

	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	/* get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
		/* keep on waiting */
		return EEM_WAIT_FOR_RESP_ACK;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		LOG_ESR_ESM_E("Get from Input Queue failed(invalid args)");

		/* clear resend counter */
		RESET_TIMEOUT_COUNTER(pThis);
		RESET_RESEND_COUNTER(pThis);
		
		return EEM_IDLE;
	}
	
	if (pEvent->iEventType != ESR_TIMEOUT_EVENT)
	{
		RESET_TIMEOUT_COUNTER(pThis);
	}

	/* now iRet is OK */
	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:  //frame event process
		return OnWaitForRespACK_HandleFrameEvent(pThis, pEvent);
	
	case ESR_TIMEOUT_EVENT:  //timeout event process, resend response frame
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("received Timeout Event");
#endif //_DEBUG_ESR_LINKLAYER
		
		/*DELETE(pEvent);*/

		if (IS_WAITACK_TIMEOUT(pThis))
		{
			RESET_TIMEOUT_COUNTER(pThis);

			if (!NEED_RESEND(pThis))
			{
#ifdef _DEBUG_ESR_LINKLAYER
				TRACE_ESR_TIPS("no response from MC, send command reply failed");
#endif //_DEBUG_ESR_LINKLAYER

				ESM_SEND_DUMMY(pThis, pEvent);

				/* reset the counter */
				RESET_RESEND_COUNTER(pThis);
				return EEM_IDLE;
			}

			else
			{
				pEvent = NEW(ESR_EVENT, 1);
				if (pEvent == NULL)
				{
					LOG_ESR_ESM_E("Memory is not enough");
					return EEM_IDLE;
				}

				INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

				/* build response frame */
				ESR_BuildEEMResponseFrame(pThis->szCmdRespBuff, 
					pEvent->sData, &pEvent->iDataLength);

				ESM_SEND_DEVENT(pThis, pEvent);
			
				return EEM_WAIT_FOR_RESP_ACK;
			}
		}
		break; //3s is not used up, send dummy, continue waiting


	case ESR_DISCONNECTED_EVENT:  //return to Idle state
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/

		return EEM_IDLE;

	default:
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER
		DELETE_ESR_EVENT(pEvent);

		break;
	}	

	ESM_SEND_DUMMY(pThis, pEvent);
	return EEM_WAIT_FOR_RESP_ACK;
	
}


/*==========================================================================*
 * FUNCTION : OnSend
 * PURPOSE  : assistant function
 * CALLS    : 
 * CALLED BY: EEM_OnSendCallback
 *			  EEM_OnSendAlarm	
 * ARGUMENTS: BOOL            bAlarm : TRUE for send alarm,
 *									   FALSE for send callback
 *            ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-19 10:50
 *==========================================================================*/
static int OnSend(BOOL bAlarm, ESR_BASIC_ARGS *pThis)
{
	int iRet, iCurState;
	char szLogText[ESR_LOG_TEXT_LEN];
	char *pSendTypeMsg;

	ESR_EVENT  *pEvent;

	/* to load "C*" or "E*" string */
	char szSendMsg[128];

	/* 1.get send type messgae for trace/log */
	if (bAlarm)
	{
		pSendTypeMsg = "EEM_OnSendAlarm";
		iCurState = EEM_SEND_ALARM;
	}
	else
	{
		pSendTypeMsg = "EEM_OnSendCallback";
		iCurState = EEM_SEND_CALLBACK;
	}

	/* 2.get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("InputQueue is empty");
#endif //_DEBUG_ESR_LINKLAYER

		return iCurState;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		sprintf(szLogText, "Get from InputQueue failed for invalid args(%s)",
			pSendTypeMsg);
		LOG_ESR_ESM_E(szLogText);
		return EEM_IDLE;
	}

	/* now iRet is OK */
	switch (pEvent->iEventType)
	{
	case ESR_CONNECTED_EVENT:  //send C* or E*
		/* now is secure connection */
		pThis->bSecureLine = TRUE;

#ifdef _DEBUG_ESR_LINKLAYER
		sprintf(szLogText, "received Connected Event(%s)", pSendTypeMsg);
		TRACE_ESR_TIPS(szLogText);
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/

		/* create frame event to hold report msg */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			sprintf(szLogText, "Memory is not enough(%s)", pSendTypeMsg);
			LOG_ESR_ESM_E(szLogText);
			return EEM_IDLE;
		}
		INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

		/* get the string */
		iRet = ESR_GetReportMsg(bAlarm, szSendMsg, 128);
		
		if (iRet == 1)  //call DXI interface failed
		{
			sprintf(szLogText, "Get Site Name through DXI interface failed(%s)",
				pSendTypeMsg);
			LOG_ESR_ESM_E(szLogText);
			DELETE(pEvent);

			return EEM_IDLE;
		}

		if (iRet == 2)  //buffer is too small, just give tip
		{
			sprintf(szLogText, "Site Name length is bigger than 64bytes(%s)",
				pSendTypeMsg);
			LOG_ESR_ESM_I(szLogText);
		}
		 
 		/* build frame and send it */
		ESR_BuildEEMResponseFrame(szSendMsg, 
			pEvent->sData, &pEvent->iDataLength);

		ESM_SEND_DEVENT(pThis, pEvent);

		if (bAlarm)  //called by OnSendAlarm state
		{
			return EEM_WAIT_FOR_ALARM_ACK;
		}

		//called by OnSendCallback state
		return EEM_WAIT_FOR_CALLBACK_ACK;


	case ESR_CONNECT_FAILED_EVENT:
#ifdef _DEBUG_ESR_LINKLAYER
		sprintf(szLogText, "received Connect Failed Event(%s)", 
			pSendTypeMsg);
		TRACE_ESR_TIPS(szLogText);
#endif //_DEBUG_ESR_LINKLAYER
		
		/*DELETE(pEvent);*/

		ESR_ClientReportFailed(pThis, TRUE);
		return EEM_IDLE;


	case ESR_DISCONNECTED_EVENT:  //drop it
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/

		return iCurState;

	default:  //unexpected event
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received unexpected event");
		TRACE("\tEvent Type: %d\n", pEvent->iEventType);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);

		return iCurState;
	}  //end of switch	
}


/*==========================================================================*
 * FUNCTION : EEM_OnSendAlarm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-19 10:43
 *==========================================================================*/
int EEM_OnSendAlarm(ESR_BASIC_ARGS *pThis)
{
	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	return OnSend(TRUE, pThis);	
}


/*==========================================================================*
 * FUNCTION : EEM_OnSendCallback
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-18 16:53
 *==========================================================================*/
int EEM_OnSendCallback(ESR_BASIC_ARGS *pThis)
{
	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	return OnSend(FALSE, pThis);	
}


/*==========================================================================*
 * FUNCTION : OnWaitForSendACK_HandleFrameEvent
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL            bAlarm  : 
 *            ESR_BASIC_ARGS  *pThis  : 
 *            ESR_EVENT       *pEvent : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-19 14:27
 *==========================================================================*/
static int OnWaitForSendACK_HandleFrameEvent(BOOL bAlarm,
										  ESR_BASIC_ARGS *pThis, 
										  ESR_EVENT *pEvent)
{
	int iRet, iCurState;
	char szLogText[ESR_LOG_TEXT_LEN];
	char *pSendTypeMsg;
	char szSendMsg[128];

	FRAME_TYPE frameType;
	unsigned char *sFrameData;
	int iFrameDataLen;

	sFrameData = pEvent->sData;
	iFrameDataLen = pEvent->iDataLength;


	/* 1.get send type messgae for trace/log */
	if (bAlarm)
	{
		pSendTypeMsg = "EEM_OnSendAlarm";
		iCurState = EEM_WAIT_FOR_ALARM_ACK;
	}
	else
	{
		pSendTypeMsg = "EEM_OnSendCallback";
		iCurState = EEM_WAIT_FOR_CALLBACK_ACK;
	}

	/* 2.frame analyse */
	frameType = ESR_AnalyseFrame(sFrameData, iFrameDataLen, pThis);

	switch (frameType)
	{
	case FRAME_ACK:  

#ifdef _DEBUG_ESR_LINKLAYER
		sprintf(szLogText, "received frame event: FRAME_ACK(%s)", 
			pSendTypeMsg);
		TRACE_ESR_TIPS(szLogText);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE(pEvent);

		pThis->iResendCounter = 0;

		if (bAlarm)
		{
			ESR_AlarmReported(pThis);
		}
		else
		{
			ESR_CallbackReported(pThis);
		}

		/* create Frame event containing "<EOT>" */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			sprintf(szLogText, "Memory is not enough(%s)", 
				pSendTypeMsg);
			LOG_ESR_ESM_E(szLogText);
			return EEM_IDLE;
		}

		INIT_SIMPLE_FRAME_EVENT(pEvent, EOT);

		/* send it */
		ESM_SEND_DEVENT(pThis, pEvent);

		return EEM_IDLE;

	case FRAME_NAK:  //resend
#ifdef _DEBUG_ESR_LINKLAYER
		sprintf(szLogText, "received frame event: FRAME_NAK(%s)", 
			pSendTypeMsg);
		TRACE_ESR_TIPS(szLogText);
#endif //_DEBUG_ESR_LINKLAYER
		
		DELETE(pEvent);

		/* reset the counter */
		pThis->iResendCounter = 0;

		/* create frame event */
		pEvent = NEW(ESR_EVENT, 1);
		if (pEvent == NULL)
		{
			sprintf(szLogText, "Memory is not enough(%s)", 
				pSendTypeMsg);
			LOG_ESR_ESM_E(szLogText);
			return EEM_IDLE;
		}

		INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

		/* get the string */
		iRet = ESR_GetReportMsg(bAlarm, szSendMsg, 128);

		if (iRet == 1)  //call DXI interface failed
		{
			sprintf(szLogText, "Get Site Name through DXI interface failed(%s)",
				pSendTypeMsg);
			LOG_ESR_ESM_E(szLogText);
			DELETE(pEvent);

			return EEM_IDLE;
		}

		if (iRet == 2)  //buffer is too small, just give tip
		{
			sprintf(szLogText, "Site Name length is bigger than 64bytes(%s)",
				pSendTypeMsg);
			LOG_ESR_ESM_I(szLogText);
		}

		/* build frame and send it */
		ESR_BuildEEMResponseFrame(szSendMsg, 
			pEvent->sData, &pEvent->iDataLength);

		ESM_SEND_DEVENT(pThis, pEvent);

		return iCurState;
		

	default:  //unexpected frame
#ifdef _DEBUG_ESR_LINKLAYER
		sprintf(szLogText, "received unexpected frame event(%s)", 
			pSendTypeMsg);
		TRACE_ESR_TIPS(szLogText);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);
		return iCurState;
	} //end of switch

}

/*==========================================================================*
 * FUNCTION : OnWaitForSendACK
 * PURPOSE  : assistant function
 * CALLS    : 
 * CALLED BY: EEM_OnWaitForAlarmACK
 *			  EEM_OnWaitForCallbackACK
 * ARGUMENTS: BOOL            bAlarm : TRUE for send alarm,
 *									   FALSE for send callback
 *			  ESR_BASIC_ARGS  *pThis :
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-19 11:13
 *==========================================================================*/
static int OnWaitForSendACK(BOOL bAlarm, ESR_BASIC_ARGS *pThis)
{
	int iRet, iCurState;
	char szLogText[ESR_LOG_TEXT_LEN];
	char *pSendTypeMsg;

    char szSendMsg[128];
	ESR_EVENT  *pEvent;


	/* 1.get send type messgae for trace/log */
	if (bAlarm)
	{
		pSendTypeMsg = "EEM_OnSendAlarm";
		iCurState = EEM_WAIT_FOR_ALARM_ACK;
	}
	else
	{
		pSendTypeMsg = "EEM_OnSendCallback";
		iCurState = EEM_WAIT_FOR_CALLBACK_ACK;
	}

	/* 2.get event from input queue */
	iRet = Queue_Get(pThis->hEventInputQueue, &pEvent, FALSE, WAIT_FOR_I_QUEUE);

	/* abnormal cases */
	if (iRet == ERR_QUEUE_EMPTY)
	{
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("InputQueue is empty");
#endif //_DEBUG_ESR_LINKLAYER

		return iCurState;
	}

	if (iRet == ERR_INVALID_ARGS)
	{
		sprintf(szLogText, "Get from InputQueue failed for invalid args(%s)",
			pSendTypeMsg);
		LOG_ESR_ESM_E(szLogText);
		return EEM_IDLE;
	}

	if (pEvent->iEventType != ESR_TIMEOUT_EVENT)
	{
		RESET_TIMEOUT_COUNTER(pThis);
	}

	/* now iRet is OK */
	switch (pEvent->iEventType)
	{
	case ESR_FRAME_EVENT:
		return OnWaitForSendACK_HandleFrameEvent(bAlarm, pThis, pEvent);

	case ESR_TIMEOUT_EVENT:

#ifdef _DEBUG_ESR_LINKLAYER
		sprintf(szLogText, "received Timeout Event(%s)", 
			pSendTypeMsg);
		TRACE_ESR_TIPS(szLogText);
#endif //_DEBUG_ESR_LINKLAYER
		
		/*DELETE(pEvent);*/

		if (IS_WAITACK_TIMEOUT(pThis))
		{
			RESET_TIMEOUT_COUNTER(pThis);

			if (!NEED_RESEND(pThis))
			{
				/* clear the resend counter(for disconnect) */
				RESET_RESEND_COUNTER(pThis);

				///* create disconnect event */
				//INIT_NONEFRAME_EVENT(pEvent, ESR_DISCONNECTED_EVENT);
				pEvent = &g_EsrDiscEvent;

				/* send it */
				ESM_SEND_SEVENT(pThis, pEvent);

				ESR_ClientReportFailed(pThis, FALSE);  //do log in it!
				return EEM_IDLE;
			}
			else  //resend 
			{
				/* create frame event */
				pEvent = NEW(ESR_EVENT, 1);
				if (pEvent == NULL)
				{
					sprintf(szLogText, "Memory is not enough(%s)", 
						pSendTypeMsg);
					LOG_ESR_ESM_E(szLogText);
					return EEM_IDLE;
				}
				INIT_FRAME_EVENT(pEvent, 0, 0, FALSE, FALSE);

				/* get the string */
				iRet = ESR_GetReportMsg(bAlarm, szSendMsg, 128);

				if (iRet == 1)  //call DXI interface failed
				{
					sprintf(szLogText, "Get Site Name through DXI interface failed(%s)",
						pSendTypeMsg);
					LOG_ESR_ESM_E(szLogText);
					DELETE(pEvent);
					RESET_RESEND_COUNTER(pThis);
					return EEM_IDLE;
				}

				if (iRet == 2)  //buffer is too small, just give tip
				{
					sprintf(szLogText, "Site Name length is bigger than 64bytes(%s)",
						pSendTypeMsg);
					LOG_ESR_ESM_I(szLogText);
				}

				/* build frame and send it */
				ESR_BuildEEMResponseFrame(szSendMsg, 
					pEvent->sData, &pEvent->iDataLength);

				ESM_SEND_DEVENT(pThis, pEvent);

				return iCurState;
			}
		}
		break; //3s has not been used up, send dummy, continue waiting


	case ESR_DISCONNECTED_EVENT:  //means report failed
#ifdef _DEBUG_ESR_LINKLAYER
		TRACE_ESR_TIPS("Received Disconnected Event");
#endif //_DEBUG_ESR_LINKLAYER

		/*DELETE(pEvent);*/

		ESR_ClientReportFailed(pThis, FALSE);  //do log in it!
		return EEM_IDLE;

	default:  //unexpected event
#ifdef _DEBUG_ESR_LINKLAYER
		sprintf(szLogText, "received unexpected event, event type is %d(%s)",
			pEvent->iEventType, pSendTypeMsg);
		LOG_ESR_ESM_E(szLogText);
#endif //_DEBUG_ESR_LINKLAYER

		DELETE_ESR_EVENT(pEvent);
		break;
	}

	ESM_SEND_DUMMY(pThis, pEvent);
	return iCurState;
}


/*==========================================================================*
 * FUNCTION : EEM_OnWaitForAlarmACK
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-19 14:25
 *==========================================================================*/
int EEM_OnWaitForAlarmACK(ESR_BASIC_ARGS *pThis)
{
	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	return OnWaitForSendACK(TRUE, pThis);
}


/*==========================================================================*
 * FUNCTION : EEM_OnWaitForCallbackACK
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : int : next EEM State, defined by ESR_STATES enum const
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-11-19 14:25
 *==========================================================================*/
int EEM_OnWaitForCallbackACK(ESR_BASIC_ARGS *pThis)
{
	/* feed watch dog */
	RunThread_Heartbeat(pThis->hThreadID[0]);

	return OnWaitForSendACK(FALSE, pThis);
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : esr.h
 *  CREATOR  : LinTao                   DATE: 2004-10-09 15:54
 *  VERSION  : V1.00
 *  PURPOSE  : public head file for ESR module
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __ESR_H_2004_10_09__
#define __ESR_H_2004_10_09__

/* basic ascii code(used by frame-analysis and command-handler) */
#define	 ACK		(0x06)
#define  NAK		(0x15)
#define  EOT		(0x04)
#define  ENQ		(0x05)
#define  SOH		(0x01)
#define  STX		(0x02)
#define  ETX        (0x03)
#define  ENQ		(0x05)
#define  DLE		(0x10)
#define  LF         (0X0A)
/* DLE + '0' */
#define  DLE0       "\x10\x30"


/* const definitions */
#define MAX_EEMFRAME_LEN		512
#define MAX_RSOCFRAME_LEN		171
#define MAX_SOCFRAME_LEN		187
#define ESR_MAX_FRAME_LEN		2048

#define EEM_RECT_GROUP_ID		2
#define EEM_RECT_RATED_VOLT_ID		21
#define EEM_DEVICE_EXIST_ID		100
#define EEM_DEVICE_INTERRUPT_ID		99

#define EEM_FIRST_SMDUPUNIT_ID		643
#define EEM_FIRST_SMDUPFUSE_ID		651
#define EEM_FIRST_DCEM_ID		718

#define EEM_DCEM_GROUP_ID		717

#define EEM_SYSTEM_ID		1
#define EEM_BATTGROUP_ID	115

#define RECT_S1_GROUP_STD_EQUIP_ID 1600
#define RECT_S2_GROUP_STD_EQUIP_ID 1700
#define RECT_S3_GROUP_STD_EQUIP_ID 1800

#define EIB1_STD_EQUIP_ID 197
#define EIB2_STD_EQUIP_ID 198
#define EIB3_STD_EQUIP_ID 199
#define EIB4_STD_EQUIP_ID 200

#define EIB_BATT_NUM 12
#define SMDU_BATT_NUM 40



/* used when call Queue_Get interface to get ESR Event, Unit: ms */
#define WAIT_FOR_I_QUEUE		1000	//wait for Event Input Queue
#define WAIT_FOR_O_QUEUE		1000	//wait for Event Output Queue


/* const definition for State Machine */
#define EEM_STATE_NUM			7
#define SOC_STATE_NUM			6


/* EEM command number supported */
#define EEM_CMD_NUM					22


/* maximum length of [Application Data] in SOC, RSOC protocol */
#define ESR_MAX_APPDATA_LEN		168


/* number of transmit buf for SOC, RSOC protocol */
//#define SOC_TBUFF_NUM			8
#define SOC_TBUFF_NUM			11 /*fengel modify*/

/* max alarm number reported in a frame */
#define ESR_MAX_ALARMS_IN_FRAME		10

/* command reply buf size */
#define ESR_RESP_SIZE				2048 

/* input event queue and output event queue size */
#define ESR_EVENT_QUEUE_SIZE		20

/* define buffer size */
/* alarm buf */
#define ESR_ALARM_BUF_SIZE	    50

/* battery test log buf */
#define ESR_BATT_TESTLOG_SIZE   (2*1024)  //only report Capacity now

/* max battery test log quentity */
#define ESR_BATT_TESTLOG_NUM	10

/* battery test log segment size(transmint in a frame) */
#define	ESR_BATT_TESTLOG_SEG	127  //128 - 1, excluding '*'

/* define ESR service net port */
#define ESR_NETSERVICE_PORT    "2000"

// compare with ESR_BASIC_ARGS.iTimeoutCounter. When ACU is on EEM_IDLE or
// SOC_OFF state, if MC has no action within READCOM_TIMEOUT*ESR_MAX_TIMEOUTCOUNT,
// ACU will disconnect the communication actively.
#define ESR_MAX_TIMEOUTCOUNT    50

/* for SMIO dynamic signal names */
#define SMIO_TYPE				"1203"  //mapped to 901-908
#define MAX_SMIO_UNIT			8
#define SMIO_REF_TEXTS_NUM		18
#define MAX_REF_TEXTS			(MAX_SMIO_UNIT * SMIO_REF_TEXTS_NUM)

#define SMBAT_TYPE				"304"  //mapped to 304-310
#define ACUNIT_TYPE				"703"  //mapped to 703-705
#define SMDUH_TYPE				"2701" //Mapped to 1702 -1765
#define MAX_SMDUH_UNIT				64
#define SMDUP_TYPE				"1901" //Mapped to 1901 -1999
#define MAX_SMDUP_UNIT				99


#define FUELMNG_TYPE				2101 
#define OBFUELMNG_TYPE				2102 
enum ESR_IP_CHECK_TYPE
{
	CHECK_IPV4,
	CHECK_IPV6_ADDR_ONLY,
	CHECK_IPV6_ADDR_AND_PORT1,
	CHECK_IPV6_ADDR_AND_PORT2
};
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * basic structures 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* define protocol types */
enum ESR_PROTOCOL
{
	EEM		= 1,
	RSOC,
	SOC_TPE,
	YDN23,
	ESR_PROTOCOL_NUM
};


/* define transmit media types */
enum ESR_MEDIA_TYPE
{		
    MEDIA_TYPE_LEASED_LINE			= 0,
    MEDIA_TYPE_MODEM,
    MEDIA_TYPE_TCPIP,
	MEDIA_TYPE_TCPIPV6,
	ESR_MEDIA_TYPE_NUM
};

/* define timers */
enum ESR_TIMER
{
	ESR_ALARM_TIMER		= 1,
	ESR_CALLBACK_TIMER,

	SOC_WAIT_DLE0_TIMER,
	SOC_WAIT_CMD_TIMER,
	SOC_WAIT_ACK_TIMER,

	ESR_TIMER_NUM
};

/* define ESR Event types */
enum ESR_EVENT_TYPE
{
	ESR_FRAME_EVENT				= 1,

	ESR_CONNECTED_EVENT,
	ESR_CONNECT_FAILED_EVENT,
	ESR_DISCONNECTED_EVENT,

	ESR_TIMEOUT_EVENT
};

/* ESR Event definition */
struct tagESREvent
{
	int	iEventType;			/* event type */

	/* data length of the event(only for FrameEvent) */
	int		iDataLength;	
	/* data of the event(only for FrameEvent) */
	unsigned char sData[ESR_MAX_FRAME_LEN];	

	BOOL	bDummyFlag;		/* dummy flag for FrameEvent */
	BOOL	bSkipFlag;		/* skip flag for FrameEvent  */
};
typedef struct tagESREvent ESR_EVENT;

#define INIT_FRAME_EVENT(_pEvent, _iLen, _szData, _bDummyFlag, _bSkipFlag)   \
			((_pEvent)->iEventType = ESR_FRAME_EVENT, \
			(_pEvent)->iDataLength = (_iLen), \
			((_iLen) == 0 ? (_pEvent)->sData[0] = '\0' : \
				(void)memcpy((_pEvent)->sData, (_szData), (size_t)(_iLen))), \
			(_pEvent)->bDummyFlag = (_bDummyFlag), \
			(_pEvent)->bSkipFlag = (_bSkipFlag))

#define INIT_SIMPLE_FRAME_EVENT(_pEvent, _chr) \
			((_pEvent)->iEventType = ESR_FRAME_EVENT, \
			(_pEvent)->iDataLength = 1, \
			(_pEvent)->sData[0] = (_chr), \
			(_pEvent)->bDummyFlag = FALSE, \
			(_pEvent)->bSkipFlag = FALSE)

#define INIT_NONEFRAME_EVENT(_pEvent, _EventType)    \
					((_pEvent)->iEventType = (_EventType), \
					 (_pEvent)->iDataLength = 0, \
					 (_pEvent)->sData[0] = '\0', \
					 (_pEvent)->bDummyFlag = FALSE, \
					 (_pEvent)->bSkipFlag = FALSE)

/* use it after init the Event already */
#define SET_EVENT_SKIP_FLAG(_pEvent)	((_pEvent)->bSkipFlag = TRUE)


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Link-layer Manager Sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* link-layer operation mode definition */
enum ESR_OPERATION_MODE 
{
	ESR_MODE_SERVER						= 0,
	ESR_MODE_CLIENT
};


/* communication status between MC and EventQueues */
enum ESR_COMM_STATUS                  
{
	COMM_STATUS_DISCONNECT		= 0,
	COMM_STATUS_NEXT,
	COMM_STATUS_SKIP
};





/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Alarm Handler sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
enum ESR_ROUTE_TYPE
{
	ESR_ROUTE_CALLBACK   = 0,
	ESR_ROUTE_ALARM_1,
	ESR_ROUTE_ALARM_2
};

struct tagAlarmHandler
{
	BYTE	byCurReportRouteType;	/* route type defined by ESR_ROUTE_TYPE */

	BOOL	bAlarmAtHand;	
	BYTE	byAlarmRetryCounter;	/* alarm report retried times       */
	
	BOOL	bCallbackAtHand;	
	BYTE	byCallbackRetryCounter;	/*callback retried times            */
};
typedef struct tagAlarmHandler ALARM_HANDLER;



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Config Builder sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* 1.EEM Model config info */
/* mapping info from ACU Model to EEM Model */
enum EEMSigType             /* Note: not change their values! */
{
	EEM_SIGTYPE_AI  = 0,
	EEM_SIGTYPE_AO,
	EEM_SIGTYPE_DI,
	EEM_SIGTYPE_DO
};

enum EEMAlarmLevel
{
	EEM_ALARM_LEVEL_A1    =0,
	EEM_ALARM_LEVEL_A2,
	EEM_ALARM_LEVEL_A3,
	EEM_ALARM_LEVEL_O1,
	EEM_ALARM_LEVEL_O2,
	EEM_ALARM_LEVEL_NUM
};


/* Added by Thomas for CR to distinguage the enable bit of EEM DI table,
   2006-2-28 */
// extend basic sig typed
enum SIG_TYPE_EEM_EX
{
	EEM_RESERVED = -1,
	EEM_ENABLE_BIT_OF_DI = -2,
	EEM_DISABLE_BIT_OF_DI = -3
};

// define EEM SPECIAL VALUEs, returned by RB* command
#define	EEM_RESERVED_VALUE		1
#define EEM_ENABLE_VALUE		1
#define EEM_DISABLE_VALUE		0
/* Added end by Thomas, 2006-2-28 */


struct tagMapEntriesInfo	/* sig mapping */
{
	int	iEEMSigType;		/* 0=AI, 1=AO, 2=DI, 3=DO */
	int	iEEMPort;			/* sig serial no. of EEM Model Block Layout */
	int	iACUSigType;     	/* defined by SIG_TYPE enum */
	int	iACUSigID;
	int iEquipmentID;       // zzc modify
	
};
typedef struct tagMapEntriesInfo MAPENTRIES_INFO;


struct tagBlockMapEntries  /* sig mapping from EEM block view */
{
	int	iAINum;
	MAPENTRIES_INFO	  *pAIs;

	int	iAONum;
	MAPENTRIES_INFO	  *pAOs;

	int	iDINum;
	MAPENTRIES_INFO	  *pDIs;

	int	iDONum;
	MAPENTRIES_INFO	  *pDOs;
};
typedef struct tagBlockMapEntries BLOCK_MAPENTRIES;


struct tagTypeMapInfo				  /* type mapping */
{
	int	 iEquipType;	              /* std Equip ID of ACU model */
	int	 iGroupID;			          /* Group ID of EEM model */
	int	 iSubGroupID;	              /* Sub-group ID of EEM model */
	BOOL bUnitLevel;	 
	int	 iMapEntriesNum;	          /* the number of current EEM model type */      
	MAPENTRIES_INFO	*pMapEntriesInfo; /* sig mapping info */

	/* run-time info */
	int  iUnitNum;					  /* the number of units for current type */
	BLOCK_MAPENTRIES BlockMapEntries;  									
};
typedef struct tagTypeMapInfo TYPE_MAP_INFO;


struct tagEEMModelConfigInfo          /* EEM model config file info */
{
	int	iTypeMapNum;	 
	TYPE_MAP_INFO	*pTypeMapInfo;	  /* type mapping info */
};
typedef struct tagEEMModelConfigInfo EEMMODEL_CONFIG_INFO;

/* 2.EEM model info */
#define EEM_MAX_DEVICE_NUM  750//150
#define EEM_MAX_RELATIVE_DEVICE_NUM	20
struct tagEEMBlockInfo	
{
	int	    iGroupID;	
	int	    iSubGroupID;
	int	    iEquipTypeID;	
	int	    iEquipID;	         /* relevant equip id of ACU model */
	int	    iUnitID;
				
	TYPE_MAP_INFO  *pTypeInfo;	 /* reference of relevant type info */

	/* Added for DL* and DP* commands, Thomas, 2006-2-9 */
#ifdef PRODUCT_INFO_SUPPORT
	int		iRelatedDeviceNum;
	int		piRelatedDeviceIndex[EEM_MAX_RELATIVE_DEVICE_NUM];
#endif //PRODUCT_INFO_SUPPORT

};
typedef struct tagEEMBlockInfo EEM_BLOCK_INFO;

struct tagEEMModelInfo	 
{
	int				iBlocksNum;	    /* the number of blocks */
	EEM_BLOCK_INFO	*pEEMBlockInfo;	 
};
typedef struct tagEEMModelInfo EEM_MODEL_INFO;

struct tagBattBlockInfoComp
{
	int Num;
	EEM_BLOCK_INFO  block;
};
typedef struct tagBattBlockInfoComp Batt_Block_Info_Comp;

/* 3.EEM common config info */
/* const definitions */
#define COMM_PORT_PARAM_LEN	   64

/* define the number of settable alarm phone number/network address */
#define ESR_ALARM_REPORT_NUM    2   

/* define the number of settable callback phone number */
#define ESR_CALLBACK_NUM		1   

/* define the number of security ip address */
#define ESR_SECURITY_IP_NUM		2

/* define max phone number length */
#define PHONENUMBER_LEN     20

#define IPADDRESS_LEN       64

#define PRODCT_INFOM_LEN	200 //added by Jimmy 2013.12.13

/* ESR common config info */
struct tagCommonConfig
{
	BYTE	    byCCID;         
	BYTE        byCCIDfromSOC;  /* calculate from iSOCID */
	int         iSOCID;         
	int			iProtocolType;	/* see enum ESR_PROTOCOL definition */
	int         iMediaType;	    /* see enum ESR_MEDIA_TYPE definition */
	char		szCommPortParam[COMM_PORT_PARAM_LEN];

	BOOL		bReportInUse;		
	BOOL		bCallbackInUse;	

	/* note: meaning of the iSecurityLevel(used for non-secure connection)
	   = 1: all command can be executed for non-secure connection;
	   = 2: only read command can be executed;
	   = 3: none command can be executed except "CB*" 
	*/
	int		iSecurityLevel;
	
	/* max attemps to report alarms or callback */
	int		iMaxAttempts;	
	/* elaps time between each attemps (unit: second) */
	int		iAttemptElapse;	
	BOOL		bCanChangeCfg;

	/* phone number for alarm report and security call back*/
	char    szAlarmReportPhoneNumber[ESR_ALARM_REPORT_NUM][PHONENUMBER_LEN];
	char    szCallbackPhoneNumber[ESR_CALLBACK_NUM][PHONENUMBER_LEN];


	/* for alarm report when used TCPIP media type */
	char	szReportIP[ESR_ALARM_REPORT_NUM][IPADDRESS_LEN];	

	/* secure TCP/IP address */
	char	szSecurityIP[ESR_SECURITY_IP_NUM][IPADDRESS_LEN];

	// product Info for WP0000* and RP0000*, added by Jimmy 2013.12.13
	char	szProdctInfom[PRODCT_INFOM_LEN];
};
typedef struct tagCommonConfig COMMON_CONFIG;


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * structures for Frame Analysis sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
enum tagFrameType
{
	/* EEM frames */
	SELECT_OK,       /* correct select frame */
	SELECT_BCC,      /* select frame with incorrect BCC checksum */
    SELECT_PROT,     /* incorrect select frame */
	SELECT_ADDR,     /* select frame with incorrect address field */
    POLL_OK,         /* correct poll frame */
	POLL_PROT,       /* incorrect poll frame */
	POLL_ADDR,       /* poll frame with incorrect address field */	
	
	/* SOC frames */
	STX_SOC_OK,	      /* STX frame for SOC/TPE */
	STX_SOC_SPLIT_F,  /* the first one of the splitter frames */
	STX_SOC_SPLIT_L,  /* the first one of the splitter frames */
	STX_SOC_SPLIT,    /* splitter STX frame */
	STX_SOC_SPECIAL,  /*	special command */    
	STX_SOC_ADDR,     /* STX frame with incorrect SOCID or CCID */
	STX_SOC_PROT,     /* incorrect STX frame */
	STX_SOC_LRC,      /* with incorrect LRC checksum */
	STX_RSOC_OK,	  /* STX frame for RSOC */
	STX_RSOC_SPLIT_F, /* the first one of splitter frames */
	STX_RSOC_SPLIT_L, /* the last one of splitter frames */
	STX_RSOC_SPLIT,   /* other splitter STX frame */
	STX_RSOC_ADDR,    /* STX frame with incorrect CCID */
	STX_RSOC_PROT,    /* incorrect STX frame */
	STX_RSOC_LRC,     /* with incorrect LRC checksum */
	FRAME_ENQ,	      /* <ENQ> frame */
	FRAME_DLE0,       /* <DLE0> frame */
	
	/* public frames */
	FRAME_ACK,       /* <ACK> frame */
	FRAME_NAK,       /* <NAK> frame */
	FRAME_EOT,       /* <EOT> frame */

	FRAME_ERR_PROT   /* incorrect frame */
};
typedef enum tagFrameType FRAME_TYPE;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for Command Handler sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* command execution function prototype definition */
typedef struct tagESRBasicArgs ESR_BASIC_ARGS;
typedef void (*CMD_EXECUTE_FUN) (ESR_BASIC_ARGS *pThis,
								 const unsigned char *szInData,
								 unsigned char *szOutData);

struct tagCmdHandler
{
	const char        *szCmdType;
	BOOL              bIsWriteCommand;
	CMD_EXECUTE_FUN   fnExecute;
};
typedef struct tagCmdHandler CMD_HANDLER;



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * structures for State Machine sub-module 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* states definition */
/* states of EEM State Machine */
enum EEM_STATES
{
	STATE_MACHINE_QUIT  = -1,

	EEM_IDLE,
	EEM_WAIT_FOR_POLL,
	EEM_WAIT_FOR_RESP_ACK,
	EEM_SEND_CALLBACK,
	EEM_WAIT_FOR_CALLBACK_ACK,
	EEM_SEND_ALARM,
	EEM_WAIT_FOR_ALARM_ACK
};

/* states of SOC State Machine */
enum SOC_STATES
{
	SOC_OFF				= 0,
	SOC_SEND_ENQ,
	SOC_WAIT_DLE0,
	SOC_ALARM_REPORT,
	SOC_WAIT_CMD,
	SOC_WAIT_ACK
};

/* on-state function prototype */
typedef int (*ON_STATE_PROC) (ESR_BASIC_ARGS *pThis);


struct tagStateInfo
{
	int	iStateID;	                /* defined by ESR_STATES enum */
	ON_STATE_PROC	fnStateProc;	
}; 
typedef struct tagStateInfo STATE_INFO;


/* some kinds of buffer definition */
/* used by soc state machine for splittering frame process */
struct tagSOCMachineBuff
{
	BOOL			bWaitDLE0Timeout; //processed by SOC_WAIT_DLE0 state
	BOOL            bWaitCmdTimeout;  //processed by SOC_WAIT_CMD state
	BOOL			bWaitACKTimeout;  /*processed by SOC_WAIT_ACK or 
									    SOC_ALARM_REPORT state */

	/* Rbuff */
	/* modified begin: extended to 3 frames for Battery Group WB* command 
	   Thomas 2005-7-14 */
	//unsigned char   szReceiveBuff[2 * ESR_MAX_APPDATA_LEN + 1];
	unsigned char   szReceiveBuff[3 * ESR_MAX_APPDATA_LEN + 1];
	/* modified end */

	int	iReceivePos;  //current free pos

	/* Tbuff */
	unsigned char	szTransmitBuff[SOC_TBUFF_NUM][MAX_SOCFRAME_LEN + 1];
	int				iMsgsInTB;
	int				iNextFreeTBPos;
	int				iCurTBSendPos;
	int				iTBReSendCounter; //used by SOC_WAIT_ACK

	/* for check identical STX frame */
	unsigned char	sLastSOCCmd[MAX_SOCFRAME_LEN+1];
	int				iLastSOCLength;	
};
typedef struct tagSOCMachineBuff SOC_MACHINE_BUFF;


/* alarm info structure */
struct tagAlarmInfo
{
	char szStartTime[13];	/* alarm start time (YYMMDDHHMMSS) */
	char szEndTime[13];		/* alarm end time (YYMMDDHHMMSS)   */

	int	 iGroupID;	        /* Group ID */
	int	 iSubGroupID;	    /* Subgroup ID */
	int	 iUnitID;	        /* Unit ID */
	char cSection;			/* I for DI signal, O for DO signal */
	int	 iCno;				/* sig no of EEM model */

	/* 	alarm level of EEM Model, currently support :A1=0, A2=1, O1=3;
	 *  they are mapped to CA=3, MA=2, OA=1 in ACU model. */
	int	iAlarmLevel;	
};
typedef struct tagAlarmInfo ALARM_INFO;



/* alarm info buffer */
struct tagAlarmInfoBuffer
{
	/* alarm number for each level */
	int			iA1Num;
	int			iA2Num;
	int			iO1Num;

	int			iCurAlarmNum;	/* number of active alarms of the buf */
	ALARM_INFO  sCurAlarmInfo[ESR_ALARM_BUF_SIZE];      /* static buf */
	ALARM_INFO	*pCurAlarmInfo;	/* active alarm info, dynamic buf */

	int			iHisAlarmNum;	/* number of history alarms of the buf */
	ALARM_INFO	*pHisAlarmInfo;	/* history alarm info */

};
typedef struct tagAlarmInfoBuffer ALARM_INFO_BUFFER;


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
 *
 * globals structures 
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* basic arg structure */
struct tagESRBasicArgs
{
	/* event queues */
	HANDLE	hEventInputQueue;	
	HANDLE	hEventOutputQueue;	

	/* data buffers */
	int					iTimeoutCounter; //used by EEM_IDEL and SOC_OFF state
	int					iResendCounter;  /*used when timeout
										   (by EEM_WAIT_FOR_RESP_ACK,
											   EEM_WAIT_FOR_ALARM_ACK,
											   EEM_WAIT_FOR_CALLBACK_ACK,
											   SOC_WAIT_DLE0, 
											   SOC_ALARM_REPORT ) */
	ALARM_INFO_BUFFER	AlarmBuffer;	
//#define ESR_R_BUFF_SIZE			512
	unsigned char       sReceivedBuff[ESR_MAX_FRAME_LEN]; /* used for CHAE-based frame recognize */
	int					iCurPos;                        //for Received Buff
	int                 iLen;						    //for Received Buff
	int			iCalcLen;

	unsigned char	    szCmdRespBuff[ESR_RESP_SIZE];	 /* command reply data buf */
	SOC_MACHINE_BUFF	SOCMachineBuff;	
	ALARM_HANDLER	    alarmHandler;	
	char                *szBattLog;  //battery log buf, size is ESR_BATTLOG_SIZE

	/* link-layer current operation mode */
	int					iOperationMode; /* use ESR_OPERATION_MODE const */

	/* communication handle */
	HANDLE	hComm;	

	
	/* hThreadID[0]: ESR Service thread id;
	 * hThreadID[1]: Linklayer thread id.  */
	HANDLE  hThreadID[2];

	/* flags */
	int     iMachineState;    /* state machine current state */ 
	int		*pOutQuitCmd;	  /* ESR service exit command, from Main Module */
	int     iInnerQuitCmd;               /* use SERVICE_EXIT_CODE const     */
	int 	iLinkLayerThreadExitCmd;	 /* use THREAD_EXIT_CODE const      */
	BOOL	bServerModeNeedExit;	     /* server operation mode exit flag */
	BOOL	bProtocolChangedFlag;	     /* protocol type changed flag      */
	BOOL	bCommRunning;	             /* communication busy flag         */
	BOOL	bSecureLine;	             /* security connection flag        */
	BOOL    bFrameDone;                  /* used by frame split function    */
	
#ifdef AUTO_CONFIG
	BOOL    bAutoCfg;					/* need begin Auto_config process flag */
#endif //AUTO_CONFIG
	BOOL    bEquipListChange;
	
};

/* used by RX* command */
struct tagESRRefText
{
	int iModuleNo;  //No. of SMIO module, range from 1 to 8
	int iRefNo;     //fixed Ref. Text No. for each SMIO module, from 1 to 18

	char *szText1;  //reference of SMIO signal name 
	char *szText2;  //reference of Unit for AIs

	int *piAlarmLevel;  //reference of High or Low AI alarm level 
};
typedef struct tagESRRefText ESR_REF_TEXT;

/* used to wait for Muxtex */
#define TIME_WAIT_MUTEX_COMCFG  1000
#define TIME_WAIT_MUTEX_SWITCH  3000



struct tagESREquipNumRecord
{
	int	iSelfRectNum;
	int	iSelfMpptNum;
	int	iSlave1RectNum;
	int	iSlave2RectNum;
	int	iSlave3RectNum;
	int	iFuelBankNum;
	int	iOBFuelBankNum;
	int	iMBBattNum;
	int	iEIB1BattNum; 
	int	iEIB2BattNum;
	int	iEIB3BattNum; 
	int	iEIB4BattNum;
	int	iSMDUFlag;
	int	iSMBRCFlag;
	int	iSMDUPFlag;
};
typedef struct tagESREquipNumRecord ESR_EQUIPNUM_RECORD;

struct tagESRGlobals
{
	EEMMODEL_CONFIG_INFO EEMModelConfig;
	EEM_MODEL_INFO	EEMModelInfo;
	COMMON_CONFIG	CommonConfig;

	/* reference texts info of the solution (used for RX* command) */
	int iRefTexts;
	ESR_REF_TEXT	RefTexts[MAX_REF_TEXTS];

	CMD_HANDLER		CmdHandlers[EEM_CMD_NUM];
	STATE_INFO		EEMStates[EEM_STATE_NUM];
	STATE_INFO		SOCStates[SOC_STATE_NUM];

	ESR_EQUIPNUM_RECORD     EquipNumRecord;

	//HANDLE  hMutexCommonCfg;  //for ESR common config file
};
typedef struct tagESRGlobals ESR_GLOBALS;

/* global variable declare */
extern ESR_GLOBALS  g_EsrGlobals;

/* Simple Events declare */
extern ESR_EVENT g_EsrDummyEvent, g_EsrTimeoutEvent;
extern ESR_EVENT g_EsrDiscEvent, g_EsrConnFailEvent, g_EsrConnEvent;


/* interface */
/* Config Builder sub-module */
int ESR_InitConfig(void);

/* Linklayer Manager sub-module */
int ESR_LinkLayerManager(ESR_BASIC_ARGS *pThis);

/* Alarm Handler sub-module */
void ESR_RegisterAlarm(ESR_BASIC_ARGS *pThis, BOOL bDelayedAlarm);
void ESR_SendAlarm(ESR_BASIC_ARGS *pThis);
void ESR_AlarmReported (ESR_BASIC_ARGS *pThis);

void ESR_RegisterCallback(ESR_BASIC_ARGS *pThis, BOOL bDealyedCallback);
void ESR_SendCallback(ESR_BASIC_ARGS *pThis);
void ESR_CallbackReported(ESR_BASIC_ARGS *pThis);

void ESR_ClientReportFailed(ESR_BASIC_ARGS *pThis, 
							BOOL bConnetedFailed);

/* Frame Analyse sub-module */
FRAME_TYPE ESR_AnalyseFrame(const unsigned char *pFrame, int iLen, 
							ESR_BASIC_ARGS *pThis);
const unsigned char *ESR_ExtractDataFromFrame(IN FRAME_TYPE frameType,
							   IN const unsigned char *pFrame, 
						       IN int iFrameLen, 
						       OUT int *piDataLen);

void ESR_BuildEEMResponseFrame(IN const unsigned char *szResData,
							   OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen);

void ESR_BuildSOCResponseFrame(IN BOOL bSoc,
							   IN const unsigned char *szResData, 
						       IN BOOL bInsertCCID, 
						       IN const unsigned char *szSoc_proc, 
						       OUT unsigned char *pFrameData,
						       OUT int *piFrameDataLen);

/* Command Handler sub-module */
void ESR_InitCmdHander(void);

void ESR_DecodeAndPerform(ESR_BASIC_ARGS *pThis,
					  const unsigned char * szCmdData);

		
/* State Machine sub-module */
/* for EEM state machine */
int EEM_OnIdle(ESR_BASIC_ARGS *pThis);
int EEM_OnWaitForPoll(ESR_BASIC_ARGS *pThis);
int EEM_OnWaitForRespACK(ESR_BASIC_ARGS *pThis);
int EEM_OnSendCallback(ESR_BASIC_ARGS *pThis);
int EEM_OnWaitForCallbackACK(ESR_BASIC_ARGS *pThis);
int EEM_OnSendAlarm(ESR_BASIC_ARGS *pThis);
int EEM_OnWaitForAlarmACK(ESR_BASIC_ARGS *pThis);

/* for SOC state machine */
int SOC_OnOff(ESR_BASIC_ARGS *pThis);
int SOC_OnSendENQ(ESR_BASIC_ARGS *pThis);
int SOC_OnWaitDLE0(ESR_BASIC_ARGS *pThis);
int SOC_OnAlarmReport(ESR_BASIC_ARGS *pThis);
int SOC_OnWaitCmd(ESR_BASIC_ARGS *pThis);
int SOC_OnWaitACK(ESR_BASIC_ARGS *pThis);


/* Utilities used by ESR Module */
BYTE ESR_GetCCID(void);
int ESR_GetReportMsg(IN BOOL bAlarm, 
					 IN OUT char *szReportMsg, 
					 IN int iBufLen);
BYTE ESR_AsciiHexToChar(IN const unsigned char *pStr);
BOOL ESR_IsStrAsciiHex(const unsigned char *pStr, int iLen);
BOOL ESR_CheckPhoneNumber(const char *szPhoneNumber);
BOOL ESR_CheckIPAddress(char *szIPAddress, int nCheckType);
BOOL ESR_CheckIsANumber(const char *szIn);
BOOL ESR_CheckBautrateCfg(const char *szBautrateCfg);
BOOL ESR_CheckModemCfg(const char *szModemCfg);
unsigned char *ESR_GetUnprintableChr(IN unsigned char chr);
void ESR_PrintEvent(ESR_EVENT *pEvent);
void ESR_PrintState(int iState);
void ESR_PrintCommonCfg(COMMON_CONFIG *pConfig);
void ESR_ClearEventQueue(HANDLE hq, BOOL bDestroy);
int CreateEEMBlocks(int iWaitCount);
BOOL ESR_EquipListChange(BOOL bIsFirstTime);
BOOL CreateRefTexts(void);
/* safe delete ESR event */
#define DELETE_ESR_EVENT(_pEvent) \
	do {                          \
	if ((_pEvent)->iEventType == ESR_FRAME_EVENT && !(_pEvent)->bDummyFlag) \
	{     \
		DELETE(_pEvent);  \
	}   \
	} while(0)

/* log utilities */
#define ESR_LOG_TEXT_LEN	256

/* error log */
#define LOG_ESR_E(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_ERROR, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: ERROR: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* extended error log(logged with error code) */
#define LOG_ESR_E_EX(_task, _szLogText, _errCode)  \
	(AppLogOut((_task), APP_LOG_ERROR, "%s(Error Code: %X).\n", \
	(_szLogText), (_errCode)),  \
	TRACE("[%s:%d]--%s: ERROR: %s(Error Code: %X).\n", __FILE__, __LINE__, \
	__FUNCTION__, (_szLogText), (_errCode)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* warning log */
#define LOG_ESR_W(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_WARNING, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: WARNING: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime()))

/* info log */
#define LOG_ESR_I(_task, _szLogText)  \
	(AppLogOut((_task), APP_LOG_INFO, "%s.\n", (_szLogText)),  \
	TRACE("[%s:%d]--%s: MESSAGE: %s.\n", __FILE__, __LINE__, __FUNCTION__, \
	(_szLogText)), \
	TRACE("Time is: %.3f\n", GetCurrentTime())) 


/* debug utilities */
#ifdef _DEBUG
	#define _DEBUG_ESR_SERVICE       //for ESR Module debug 
	#define _TEST_ESR_SERVICE        //for ESR Module test version
#endif

#ifdef _DEBUG_ESR_SERVICE
	//#define _SHOW_ESR_EVENT_INFO
	//#define _SHOW_ESR_CONFIG_INFO

    /* show the current state name of the State Machine */
	#define _SHOW_ESR_CUR_STATE

	#define _DEBUG_ESR_LINKLAYER
	//#define _DEBUG_ESR_CMD_HANDLER
	//#define _DEBUG_ESR_REPORT
#endif //_DEBUG_ESR_SERVICE

#ifdef _TEST_ESR_SERVICE
	//#define _TEST_ESR_BATTLOG
	//#define	_TEST_ESR_MODIFYCONFIG
	//#define _TEST_DATA_PRECISION
	#define _TEST_ESR_QUEUE_SYNCHRONIZATION
#endif //_TEST_ESR_SERVICE


#ifdef _DEBUG_ESR_SERVICE
	#define TRACE_ESR_TIPS(_tipInfo)   \
		(TRACE("[%s:%d]--%s: Message: %s.\n", __FILE__, __LINE__, \
		 __FUNCTION__, (_tipInfo)), \
		 TRACE("Time is: %.3f\n", GetCurrentTime()))

	#define ESR_GO_HERE  TRACE("[%s:%d]--%s: Go here!.\n", __FILE__, \
					__LINE__, __FUNCTION__)
#else
	#define ESR_GO_HERE  1 ? (void)0 : (void)0
	#define TRACE_ESR_TIPS(_tipInfo)   1 ? (void)0 : (void)0
#endif //_DEBUG_ESR_SERVICE


/* switch with Maintenance service */
#define COM_SHARE_SERVICE_SWITCH

#ifdef COM_SHARE_SERVICE_SWITCH
#define ESR_IS_SHARE_COM(_iMediaType)  \
	((((_iMediaType) != MEDIA_TYPE_TCPIP) && ((_iMediaType) != MEDIA_TYPE_TCPIPV6)) ? TRUE : FALSE)

BOOL ESR_IsMtnFrame(const unsigned char *pFrame, int iLen);
void ESR_BuildMtnResponseFrame(OUT unsigned char *pFrameData,
							   OUT int *piFrameDataLen);
#endif //COM_SHARE_SERVICE_SWITCH


#endif //__ESR_H_2004_10_09__

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : alarm_handler.c
 *  CREATOR  : LinTao                   DATE: 2004-10-12 10:59
 *  VERSION  : V1.00
 *  PURPOSE  : provide functions to process alarm report and callback
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"

#include "esr.h"

/* log Macros (AH = Alarm Handler) */
#define ESR_TASK_AH		"ESR Alarm Handler"

/* error log */
#define LOG_ESR_AH_E(_szLogText)  LOG_ESR_E(ESR_TASK_AH, _szLogText)

/* warning log */
#define LOG_ESR_AH_W(_szLogText)  LOG_ESR_W(ESR_TASK_AH, _szLogText) 

/* info log */
#define LOG_ESR_AH_I(_szLogText)  LOG_ESR_I(ESR_TASK_AH, _szLogText)


/* do in the config builder sub-module */
//static BOOL CheckNumbers (ESR_BASIC_ARGS *pThis, BYTE byRouteType);

//static void ConnectToRemote (ESR_BASIC_ARGS *pThis, BYTE byRouteType)

/*==========================================================================*
 * FUNCTION : ESR_RegisterAlarm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis        : 
 *            BOOL            bDealyedAlarm : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 16:11
 *==========================================================================*/
void ESR_RegisterAlarm(ESR_BASIC_ARGS *pThis, BOOL bDelayedAlarm)
{
	ALARM_HANDLER *pAlarmHandler;
	COMMON_CONFIG *pCommonCfg;

	pCommonCfg = &g_EsrGlobals.CommonConfig;

	/* alarm report function is disabled */
	if (!pCommonCfg->bReportInUse || pCommonCfg->iMaxAttempts == 0)
	{
		return;
	}

	pAlarmHandler = &pThis->alarmHandler;

	/* if is delayed alarm or last report is running, do not reset retry counter */
	if (!bDelayedAlarm && !pAlarmHandler->bAlarmAtHand)
	{
		pAlarmHandler->byAlarmRetryCounter = 0;
	}


	/* if last report is not running (means not using Route2,...) */
	if (!pAlarmHandler->bAlarmAtHand) 
	{
		pAlarmHandler->byCurReportRouteType = ESR_ROUTE_ALARM_1;
	}
	pAlarmHandler->bAlarmAtHand = TRUE;
	

	

	return;
}


/*==========================================================================*
 * FUNCTION : ESR_RegisterCallback
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis           : 
 *            BOOL            bDealyedCallback : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 16:13
 *==========================================================================*/
void ESR_RegisterCallback(ESR_BASIC_ARGS *pThis, BOOL bDealyedCallback)
{
	ALARM_HANDLER *pAlarmHandler;
	COMMON_CONFIG *pCommonCfg;

	pCommonCfg = &g_EsrGlobals.CommonConfig;

	/* callback function is disabled */
	if (!pCommonCfg->bCallbackInUse || pCommonCfg->iMaxAttempts == 0)
	{
		return;
	}

	/* only when using Modem, call back function is on */
	if (pCommonCfg->iMediaType != MEDIA_TYPE_MODEM)
	{
		return;
	}

	/* SOC state machine do not need callback */
	if (pCommonCfg->iProtocolType != EEM)
	{
		return;
	}

	pAlarmHandler = &pThis->alarmHandler;
	pAlarmHandler->bCallbackAtHand = TRUE;
	pAlarmHandler->byCurReportRouteType = ESR_ROUTE_CALLBACK;


	/* if is delayed alarm, do not reset retry counter */
	if (!bDealyedCallback)
	{
		pAlarmHandler->byCallbackRetryCounter = 0;
	}

	return;
}

/*==========================================================================*
 * FUNCTION : ESR_SendAlarm
 * PURPOSE  : set link-layer operation mode change flag and link-layer manager
 *            will send alarm later
 * CALLS    : 
 * CALLED BY: EEM_OnIdle
 *			  SOC_OnOff
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 11:18
 *==========================================================================*/
void ESR_SendAlarm(ESR_BASIC_ARGS *pThis)
{
	ALARM_HANDLER *pAlarmHandler;
	pAlarmHandler = &pThis->alarmHandler;

	/* set server mode exit flag */
	pThis->bServerModeNeedExit = TRUE;

	/* set client mode */
	pThis->iOperationMode = ESR_MODE_CLIENT;

	return;
}


/*==========================================================================*
 * FUNCTION : ESR_SendCallback
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 16:21
 *==========================================================================*/
void ESR_SendCallback(ESR_BASIC_ARGS *pThis)
{
	ALARM_HANDLER *pAlarmHandler;
	pAlarmHandler = &pThis->alarmHandler;

	/* set server mode exit flag */
	pThis->bServerModeNeedExit = TRUE;

	/* set client mode */
	pThis->iOperationMode = ESR_MODE_CLIENT;

	return;
}




/*==========================================================================*
 * FUNCTION : ESR_AlarmReported
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: (ESR_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 16:25
 *==========================================================================*/
void ESR_AlarmReported (ESR_BASIC_ARGS *pThis)
{
	ALARM_HANDLER *pAlarmHandler;

	pAlarmHandler = &pThis->alarmHandler;

	pAlarmHandler->bAlarmAtHand = FALSE;
	pAlarmHandler->byAlarmRetryCounter = 0;

	/* set link-layer flags */
	pThis->bServerModeNeedExit = FALSE;
	pThis->iOperationMode = ESR_MODE_SERVER;

	return;
}


/*==========================================================================*
 * FUNCTION : ESR_CallbackReported
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 16:25
 *==========================================================================*/
void ESR_CallbackReported(ESR_BASIC_ARGS *pThis)
{
	ALARM_HANDLER *pAlarmHandler;

	pAlarmHandler = &pThis->alarmHandler;

	pAlarmHandler->bCallbackAtHand = FALSE;
	pAlarmHandler->byCallbackRetryCounter = 0;

	/* set link-layer flags */
	pThis->bServerModeNeedExit = FALSE;
	pThis->iOperationMode = ESR_MODE_SERVER;

	return;
}


/*==========================================================================*
 * FUNCTION : ESR_AlarmReportTimerProc
 * PURPOSE  : timer call back function
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hTimerOwner : The thread ID of timer owner
 *            int     idTimer     : The ID of the timer
 *            void    *pArgs      : The args passed by timer owner, 
 *                                  here is ESR_BASIC_ARGS *pThis
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 14:12
 *==========================================================================*/
static int ESR_ReportTimerProc(HANDLE  hTimerOwner,
							   int     idTimer,
							   void    *pArgs)
{
	ESR_BASIC_ARGS *pThis;
    UNUSED(hTimerOwner);

	pThis = (ESR_BASIC_ARGS *)pArgs;

	if (idTimer == ESR_ALARM_TIMER)
	{
#ifdef _DEBUG_ESR_REPORT
		TRACE_ESR_TIPS("Register Alarm by Timer");
#endif //_DEBUG_ESR_REPORT

		ESR_RegisterAlarm(pThis, TRUE);
	}
	else  /* idTimer == CALLBACK_ TIMER */
	{
#ifdef _DEBUG_ESR_REPORT
		TRACE_ESR_TIPS("Register Callback by Timer");
#endif //_DEBUG_ESR_REPORT
		  
		ESR_RegisterCallback(pThis, TRUE);
	}

	return TIMER_KILL_THIS;
}


/*==========================================================================*
 * FUNCTION : ESR_ClientReportFailed
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ESR_BASIC_ARGS  *pThis          : 
 *            BOOL            bConnetedFailed : 
 * RETURN   :  void : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-10-12 14:39
 *==========================================================================*/
void ESR_ClientReportFailed(ESR_BASIC_ARGS *pThis, 
							BOOL bConnetedFailed)
{
	COMMON_CONFIG *pConfig;
	ALARM_HANDLER *pAlarmHandler;
	int iCurRouteType, iTimerSetRet;
	BOOL bLeasedLine;    //for lease line media type

	char *pLog;
	char szLogText[ESR_LOG_TEXT_LEN];  //for log

	pConfig = &g_EsrGlobals.CommonConfig;
	pAlarmHandler = &pThis->alarmHandler;
	iCurRouteType = pAlarmHandler->byCurReportRouteType;

	bLeasedLine = (pConfig->iMediaType == MEDIA_TYPE_LEASED_LINE) ? TRUE : FALSE;
	
	//fengel debug
	/*
	printf("[%s]-(%d)--<%s>\n iCurRouteType = %d,bLeasedLine = %d,bConnetedFailed = %d \n",__FILE__, __LINE__, __FUNCTION__,iCurRouteType,bLeasedLine,bConnetedFailed);		
	int i = 0,j=0;
	for(i = 0;i<2; i++)
	{
		if( 1 == i)
		{
			printf("\n");
		}	
		for(j=0; j<30; j++)
			printf(" szReportIP[%d][%d] = %c\n",i,j,pConfig->szReportIP[i][j]);
	}		
	*/
	/*����: �����ж�web��Second Report IP�Ƿ����õķ����Ƿ�100%׼ȷ?
	
			pConfigָ����ڴ��Ƿ�Ϊ����flash��ʵʱ���ݣ�
	*/
	BOOL bRout2NoExist;//fengle add
	bRout2NoExist = ('\0' == pConfig->szReportIP[1][0]) ? TRUE : FALSE;
	
	/* //for debug
	if( bRout2NoExist )
		printf("Second Report IP has not set\n");
	else
		printf("Second Report IP has set\n");
	*/
	
	
	/*
		�и�Լ������web�в���"Main Report IP"�Ƿ����ã��澯����ʱ���᳢������route-1��Ȼ��Ż����web
	���Ƿ�������"Second Report IP"�������Ƿ�������route-2����ˣ��ڸ澯�����Ĺ����У���ò�Ҫ��ȥ
	����web��"Second Report IP"��
	*/
	switch (iCurRouteType)
	{
	case ESR_ROUTE_ALARM_1:
		
		if(bRout2NoExist)
		{
			/* for leased line, only has one route */
			
			if (++(pAlarmHandler->byAlarmRetryCounter) <
				pConfig->iMaxAttempts)
			{
			/* reset flags */
				pAlarmHandler->bAlarmAtHand = FALSE;
				pThis->bServerModeNeedExit = FALSE;
				pThis->iOperationMode = ESR_MODE_SERVER;

				/* log and trace */
				pLog = bLeasedLine ? "Lease Line" : "Route 1";
				
					
				if (bConnetedFailed)
				{
					sprintf(szLogText, "Alarm Report using %s failed(can not "
						"connect to MC). Try later", pLog);
					LOG_ESR_AH_E(szLogText);
					//printf("Alarm Report using Route 1 failed(can not connect to MC). Try later\n");
				}
				else
				{
					sprintf(szLogText, "Alarm Report using %s failed. Try later",
						pLog);
					LOG_ESR_AH_E(szLogText);
					//printf("Alarm Report using Route 1 failed. Try later\n");
				}
				

				/* statr timer, retry later */
				iTimerSetRet = Timer_Set(RunThread_GetId(NULL),
										 ESR_ALARM_TIMER,
										 pConfig->iAttemptElapse,
										 ESR_ReportTimerProc,
										 (DWORD)pThis);
				if (iTimerSetRet == ERR_TIMER_EXISTS)
				{
					TRACE_ESR_TIPS("Alarm Report Timer has existed");
				}
				if (iTimerSetRet == ERR_TIMER_SET_FAIL)
				{
					LOG_ESR_AH_E("Alarm Report failed(set re-alarmReport "
						"timer failed)");
				}
			}
			else
			{
				/* not try to report */
				pAlarmHandler->bAlarmAtHand = FALSE;
				pThis->bServerModeNeedExit = FALSE;
				pThis->iOperationMode = ESR_MODE_SERVER;

				/* clear OutputQueue */
				ESR_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

				sprintf(szLogText, "Alarm report failed after %d retries",
					pConfig->iMaxAttempts);
				LOG_ESR_AH_E(szLogText);	
				
				//printf("Alarm report failed after 3 retries\n");
				
				//printf("Alarm report failed after %d retries.\n", pConfig->iMaxAttempts);// zzc debug 
				/* clear OutputQueue */
				ESR_ClearEventQueue(pThis->hEventOutputQueue, FALSE);
			}
			
		}
		else
		{
		
			/* for leased line, only has one route */
			if (!bLeasedLine)
			{
				pAlarmHandler->byCurReportRouteType = ESR_ROUTE_ALARM_2;
			
				/* log and trace */
				if (bConnetedFailed)
				{
					LOG_ESR_AH_I("Alarm report using Route 1 failed(can not connect"
						" to MC), try to use Route 2");	
						
					//printf("Alarm report using Route 1 failed(can not connect to MC), try to use Route 2\n");	
				}
				else
				{
					LOG_ESR_AH_I("Alarm report using Route 1 failed, try to use "
						"Route 2");
						
					//printf("Alarm report using Route 1 failed, try to use Route 2\n");		
				}
				
			}
			
		}
		break;
		
	case ESR_ROUTE_ALARM_2:
		if (++(pAlarmHandler->byAlarmRetryCounter) <
			pConfig->iMaxAttempts)
		{
			/* reset flags */
			pAlarmHandler->bAlarmAtHand = FALSE;
			pThis->bServerModeNeedExit = FALSE;
			pThis->iOperationMode = ESR_MODE_SERVER;

			/* log and trace */
			pLog = bLeasedLine ? "Lease Line" : "Route 2";
			
				
			if (bConnetedFailed)
			{
				sprintf(szLogText, "Alarm Report using %s failed(can not "
					"connect to MC). Try later", pLog);
				LOG_ESR_AH_E(szLogText);
			}
			else
			{
				sprintf(szLogText, "Alarm Report using %s failed. Try later",
					pLog);
				LOG_ESR_AH_E(szLogText);
			}
			

			/* statr timer, retry later */
			iTimerSetRet = Timer_Set(RunThread_GetId(NULL),
									 ESR_ALARM_TIMER,
									 pConfig->iAttemptElapse,
				                     ESR_ReportTimerProc,
				                     (DWORD)pThis);
			if (iTimerSetRet == ERR_TIMER_EXISTS)
			{
				TRACE_ESR_TIPS("Alarm Report Timer has existed");
			}
			if (iTimerSetRet == ERR_TIMER_SET_FAIL)
			{
				LOG_ESR_AH_E("Alarm Report failed(set re-alarmReport "
					"timer failed)");
			}
		}
		else
		{
			/* not try to report */
			pAlarmHandler->bAlarmAtHand = FALSE;
			pThis->bServerModeNeedExit = FALSE;
			pThis->iOperationMode = ESR_MODE_SERVER;

			/* clear OutputQueue */
			ESR_ClearEventQueue(pThis->hEventOutputQueue, FALSE);

			sprintf(szLogText, "Alarm report failed after %d retries",
				pConfig->iMaxAttempts);
			LOG_ESR_AH_E(szLogText);	
			//printf("Alarm report failed after %d retries.\n", pConfig->iMaxAttempts);// zzc debug 
			/* clear OutputQueue */
			ESR_ClearEventQueue(pThis->hEventOutputQueue, FALSE);
		}
		break;

	case ESR_ROUTE_CALLBACK:
		if (++(pAlarmHandler->byCallbackRetryCounter) < 
			pConfig->iMaxAttempts)
		{
			/* reset flags */
			pAlarmHandler->bCallbackAtHand = FALSE;
			pThis->bServerModeNeedExit = FALSE;
			pThis->iOperationMode = ESR_MODE_SERVER;

			/* log and trace */
			if (bConnetedFailed)
			{
				LOG_ESR_AH_I("Callback failed(can not connect to MC). "
					"Try later");
			}
			else
			{
				LOG_ESR_AH_I("Callback failed. Try later");
			}

			/* set timer, retry later */
			iTimerSetRet = Timer_Set(RunThread_GetId(NULL),
									 ESR_CALLBACK_TIMER,
									 pConfig->iAttemptElapse,
				                     ESR_ReportTimerProc,
				                     (DWORD)pThis);

			if (iTimerSetRet == ERR_TIMER_EXISTS)
			{
				TRACE_ESR_TIPS("Callback Report Timer has existed");
			}
			if (iTimerSetRet == ERR_TIMER_SET_FAIL)
			{
				LOG_ESR_AH_E("Callback failed(set re-callback "
					"timer failed)");
			}
		}
		else
		{
			/* not try to report */
			pAlarmHandler->bCallbackAtHand = FALSE;
			pThis->bServerModeNeedExit = FALSE;
			pThis->iOperationMode = ESR_MODE_SERVER;

			sprintf(szLogText, "Callback failed after %d retries",
				pConfig->iMaxAttempts);
			LOG_ESR_AH_E(szLogText);
		}
		break;

	default: /* not be others */
		TRACE_ESR_TIPS("Report route setting is error!!!");
		break;
	}

	return;
}

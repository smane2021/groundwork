/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : debug_maintn.c
 *  CREATOR  : LIXIDONG                 DATE: 2004-09-24 10:59
 *  VERSION  : V1.00
 *  PURPOSE  : This file will be used in communicating with powerkit
 *			   It will be running in linux system of ACU.
 *			   According to current running different mode:
 *             0:DSI 1:DSP different mode
 *			   DSI-Debug service interface DSP-Debug service program 
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"
#include "debug_maintn.h"

#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/resource.h>		/* setrlimit */
#include <sys/types.h>			/* socket, bind, accept */
#include <sys/socket.h>			/* socket, bind, accept, setsockopt */
#include <syslog.h>
#include <netdb.h>
#include <net/route.h>			/*rtentry*/
#include <net/if.h>				/*ifreq */

#include <sys/mount.h>
#include <linux/mtd/mtd.h>

#include <sys/types.h>
#include <dirent.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>


#ifdef _DEBUG
//#define _DEBUG_MAINTN_		1
#endif
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//All commands CID2 from powerkit
#define		CONNECT_INFO_ID			0xCC
#define		CONNECT_INFO			0x00
#define		RIGHT_VERIFY			0x01
#define		SWITCH_STATUS			0x02
#define		GET_ACU_ADDR			0x03
#define		MODIFY_ACU_ADDR			0x04
#define		GET_ACU_IP				0x05
#define		MODIFY_ACU_IP			0x06
#define		GET_COMM_SETTING		0x07
#define		SET_COMM_SETTING		0x08
#define		GET_MANUFACR_INFO 		0x09
#define		SET_MANUFACR_INFO		0x0A
#define		REBOOT_SYSTEM	 		0x0B
#define		GET_DIAGNOSE_INFO		0x0C
#define		FILE_UPLOAD				0x0E
#define		FILE_DOWNLOAD			0x0F
#define		FILE_DELETE				0x10
#define		FILE_NAME_MODIFY		0x11
#define		GET_FILES_LIST			0x12
#define		GET_SAMPLERS_LIST		0x13
#define		GET_EQUIPS_LIST			0x14
#define		ACQUISITE_DATA			0x15
#define		DELETE_HISDATA			0x16
#define		END_CURR_CONNECTION		0x17
#define		CONFIRM_FILE_UPDATE		0x18
#define		UPDAT_ACU_SW			0x19
#define		RECEIVE_ACU_SW			0x20
#define		RELOAD_DDEAULT_CFG		0x21


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Return to PowerKit RTN's definition
#define		IS_OK					0x00
#define		LOW_LEV_NO_RESPONSE		0x01
#define		CRC32_ERROR				0x02
#define		INVALID_CIDI			0x03
#define		INVALID_CIDII			0x04
#define		INVALID_DATA			0x05
#define		ERROR_CMD_FORMAT		0x06
#define		OTHER_ERROR				0x07
#define		MAIN_PRO_RUNNING		0x08   //Right verification
#define		DEBUG_PRO_RUNNING		0x09   //Right verification
#define		PASSWORD_ERROR	 		0x0A
#define		USER_NOT_EXIST			0x0B
#define		FILE_NOT_EXIST			0x0C
#define		ACU_INFO_READ_ONLY		0x0E
#define		RETURN_ADDR				0x10
#define		EEM_USING_COM			0x11   //EEM is using serial port
#define		FAIL_OPERATE			0xFF

///////////////////////////////////////////////////////////////////////////////
//History data sequence No. definition
#define		DBS_ALL_HIS_DATAS			0	
#define		DBS_HIST_ALARM_LOG			1	// historical alarm log
#define 	DBS_HIS_DATA_LOG			2	// historical sampling data log
#define 	DBS_STAT_DATA_LOG			3	// the log of statistics signal
#define		DBS_BATT_TEST_LOG_FILE		4	// battery test log file
#define		DBS_DSL_TEST_LOG			5	// diesel test log
#define 	DBS_CTRL_CMD_LOG			6	// the ctrl cmd and lt log
#define		DBS_RUNNING_LOG				7	// System running log
#define 	DBS_ACTIVE_ALARM_LOG		8	// the active alarm info.
#define 	DBS_PERSISTENT_SIG_LOG		9	// Runtime persistent aramlog
#define		DBS_GEN_CTL_DATA			10	// General Controller Data
#define 	DBS_SYSTEM_USER_LOG			11	// system user info log
#define		DBS_ALLDATA_INFLASH			12	// All hisdata in flash
///////////////////////////////////////////////////////////////////////////////
//Historical data definition from data_mgmt.h
const char *pszHisdataName[] =
{
	"all historical records",
	HIST_ALARM_LOG,
	HIS_DATA_LOG,
	STAT_DATA_LOG,
	BATT_TEST_LOG_FILE,
	DSL_TEST_LOG,
	CTRL_CMD_LOG,
	ACU_RUNNING_LOG,
	ACTIVE_ALARM_LOG,
	PERSISTENT_SIG_LOG,
	GEN_CTL_DATA,
	SYSTEM_USER_LOG,	
	"all flash data",
};

///////////////////////////////////////////////////////////////////////////////
//Set manufactory information command type definistion
#define		INSTALL_TIME_CMD	   0x81
#define		SERVICE_TIME_CMD	   0x82
#define		INSTALL_TIME_LINE	   1
#define		SERVICE_TIME_LINE	   2
#define		INSTALL_TIME_COL	   2
#define		SERVICE_TIME_COL	   2

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#define		CR						(0x0D)
#define		LF						(0x0A)
#define		SPACE					(0x20)
#define		TAB						(0x09)	 //('/t')
#define		DBS_EQUAL				(0x3D)	 //('=')
#define		DBS_SPITT				(0x2D)	 //('-')
#define		DBS_SPITT2				(0x2F)	 //('-')
#define		COLON_SPIT				(0x3A)	 //('-')
#define		IS_DBS_SPITT(c)		    (((c) == DBS_SPITT) || ((c) == DBS_SPITT2))

#define		PATH_FLAG				"/"
#define		COMMENT_FLAG			"#"
#define		LEFT_PREFIX				'['
#define		RIGHT_SUFFIX			']'

#define		IS_WHITE_SPACE(c)		(((c) == SPACE) || ((c) == TAB))
#define		IS_ONLY_SPACE(c)		((c) == SPACE)
#define		IS_WHITE_TAB(c)			((c) == TAB)
#define		END_OF_LINE(c)			(((c) == CR) || ((c) == LF))

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Program running modes definition
//Current running debug service interface mode
#define		DSI_RUNNING_MODE			1  
//Current running debug service program mode
#define		DSP_RUNNING_MODE			2

///////////////////////////////////////////////////////////////////////////////
//Special unit definition
#define		MV_PER_DEG_C_LEN			8
#define		SIG_NAME_LEN				32
#define		SIG_UNIT_LEN				5
//Program running flag definition
#define		I_RUNNING_FLAG				1
#define		I_EXIT_FLAG					0

//User Management File name
#define		ACU_CFG_FILE			"solution/MonitoringSolution.cfg"
#define		ACU_DIAGNOSE_FILE		"/var/scuptest.log" 
#define		MANUFACR_INFO_FILE		"/var/scupinfo.log"
#define		MANUSW_INFO_FILE		"/log/scupinfosw.log" 
#define		DBS_TIME_CHN_FMT		"%Y-%m-%d"	// YYYYMMDD
#define		SW_SYS_UPDATE_FILE		"/var/scup.tar.gz"
#define		SW_SYS_UPDATE_FILE_S	"scup.tar.gz"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Manufacture information definition
#define		PRODUCT_NAME_LEN		10
#define		MANU_NAME_LEN			10
#define		VERSION_LEN				2    //2*
#define		SERIAL_NUM_LEN			20
#define		DATE_LEN				4    //3*
#define		MANU_INFO_ALL_LEN       56

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Const length definition
#define		CRC32_BYTES				4
#define		FRAME_HEAD				1
#define		FRAME_END				1
#define		NOT_CHECKED_BYTES		6 //include CRC32,head,tail

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Communication references definition
#define		NET_PORT_NUM			6999

//Port ID	index
#define		TOTAL_PORT_NUMS			4
#define		SAMPLER_PORT_I_ID		1	//It acts as rs485 in scu plus
//#define		SAMPLER_PORT_II_ID		2	//it is canceled
#define		NET_PORT_ID				2	
#define		REPORT_PORT_ID			3	
//Port description
#define		NET_PORT_DESC_LEN		4
#define		SERIAL_PORT_DESC_LEN    5
#define		SAMPLER_PORT_I_DESCR	"ttyS1"	
#define		SAMPLER_PORT_II_DESCR	"ttyS3"	
#define		NET_PORT_DESCR			"NET0"	
#define		REPORT_PORT_DESCR		"ttyS2"	
//Default serial port com setting
#define		SERAIL_COM_SETTING_LEN	10
#define		NET_COM_SETTING_LEN		2
#define		SERAIL_COM_SETTING		"9600,n,8,1"
#define		NET_COM_SETTING		    "NA"

//Standard port type id
#define		SERIAL_TYPE_PORT		3
#define		DIAL_TYPE_PORT			4
#define		NET_TYPE_PORT			1
#define		SAMPLING_PORT2			5

#define		SAMPLING_PORT_TIMEOUT	1000	//1s
#define		SERIAL_PORT_TIMEOUT		2000    //2s
#define		NET_PORT_TIMEOUT		5000	//5s
#define     DIAL_PORT_TIMEOUT		60000   //1min

//Port Type
#define		SERIAL_MODE				0
#define		DIAL_MODE				1
#define		ETHERNET_MODE			2
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/*Define sub-field position(include line pos,column pos) ,it will be used in 
reading monitoringsolution.cfg after confirm father field,sub-field will be 
identified by this name.*/
//SITE_INFO
#define     ACU_SITEINFO_FIELD		"[SITE_INFO]"
#define		SITE_INFO_LINES			3
#define		SITE_ID_COL				1

//COMMUNICATION_PORT_INFO
#define     COMM_SETTING_FIELD		"[COMMUNICATION_PORT_INFO]"
#define		REPRT_SERIAL_PORT_LINES	6
#define		PORT_TYPE_COL			3
#define		PORT_TIMEOUT_COL		6
#define		PORT_SETTING_COL		7

///////////////////////////////////////////////////////////////////////////////
#define		MAX_SERI_READ_TIMES		2    //Used in HaveSerialClient



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Frame relevant definition
#define		MAX_DATA_FRAMES			4096
#define		LAST_DATA_BLOCK			4096
#define		ONLY_ONE_BLOCK			1
#define		SYS_FILE_BUFF_SIZE		2048   //512
#define		MAX_FILE_BUFF_SIZE		4096
#define		MAX_BUFFER_SIZE			1088   //300
#define		FILECOPY_FRAME_SIZE		1024	//Only data length  //256
#define     MIN_FRAME_LENGTH		13		//Not include data
#define		MAX_DATABUFF_LEN		1024    //256
#define		LOW_LEVEL_MAX_DATA_LEN  3072    //Low level device data length
#define		MIN_UPDATE_FILE_LEN	    1000000 //1Mbites

//Include DATA_FRAME(13) and FILECOPY_FRAME_SIZE(256) and nBlockNo(2)
#define		FILE_SEND_FRAME_LEN		1039	//271   

//Receive file data,data start after iBlockNo of FRAME
#define		FILE_DATA_START_POS		10

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Some time const definition
#define		OPERATE_DELAY_COUNTS    240    //s
#define		EXIT_DELAY_COUNTS		300    //s
#define		MAX_TIME_OUTMS			1000   //ms
#define		REBOOT_DELAY_TIME		5000   //ms

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Some flash relevant reference definition
#define		MAX_SAMPLERS		    20
#define		MAX_HISDATA_SECTORS     63         
#define		START_SECTOR			1			
#define		MAX_SIGNALS				256			
#define     SIZE_PER_SECTOR			65536			
#define		DM_MAX_EQUIPS			100	
#define		FILE_NAME_LEN			64
#define		MAX_USERS				20

//Define erase sector result values
#define		ERASE_SUCCEED			1
#define		ERASE_FAILED			0

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Some Global variable definition
//Port comm port definition
static	MOD_DBS_DATA	*g_pDbsData;

//Define global variables used to process EEM-Debug Service to use serial port

#define		EEM_SOC_NAME			"eem_soc.so"
#define		SERVICE_YDN23_NAME		"ydn23.so"

#ifdef     EEM_SERVICE_USED		
#define		HLMS_SERVICE_NAME		EEM_SOC_NAME
#else
#define		HLMS_SERVICE_NAME		SERVICE_YDN23_NAME
#endif


#define		MAX_RESTART_EEM_TIMES	5
static BOOL DBS_Ini_Report_Port(void);
static BOOL DBS_Stop_Report_Port(void);
static BOOL ProcessSerialPort(void);
static BOOL StartHLMSService(void);
//End global variable definition

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Declare CRC32 check calculation functions
///////////////////////////////////////////////////////////////////////////////
#define CRC32_DEFAULT    0x04C10DB7
static void BuildTable32(unsigned long aPoly , 
						 unsigned long* Table_CRC);

static unsigned long RunCRC32(const unsigned char * aData, 
					   unsigned long aSize, 
					   unsigned long aPoly );

///////////////////////////////////////////////////////////////////////////////
//Declare communication function and relevant processing functions

static BOOL AcceptClient(HANDLE hPort, HANDLE * hClient);
static BOOL HaveSeriClient(HANDLE hPort, HANDLE * pPort);
static void DBS_ClearRxTx( HANDLE hPort);

static void Send_File(char * pszFileName);
//Check command frame is right or not
static int CheckFrame(char * pDataFrame);

//Return simple command to powerkit(frame length is MIN_FRAME_LENGTH)
static void ReturnToPowerKit(int iReturnNo, void * pBuff);

//Change checkno crc32 after modify data
static void ChangeCheckNo(char * szBuffer, int iFrameLength);

//Send file package to powerkit(frame length is FILE_SEND_FRAME_LEN)
static BOOL SendFilePackage(char *szBuffer, int iFrameLength);

//Pack data to frame szFrameBuffer to be sent
static void PackFileBlock(char *szFrameBuffer, char *szDataBuff, 
				   int iDataLen, unsigned short iBlockNo);

///////////////////////////////////////////////////////////////////////////////
//Declare initiation and exit processing functions
static void Ini_Global_Vars(int iRunningMode);
static int DBS_Ini_Port(void);
static BOOL Ini_OnePort(DBS_PORT_INFO * pPortInfo, HANDLE * pHandle);
static void Clear_CurrConnect(void);
static void EndCurrConnect(void);
static void Exit_DBS_Service(void);

//Main process thread function to process communication with PowerKit
static void fnProcessThread(void);

//Declare erase sectors function in flash
static int DM_EraseSector(BYTE byStartSectorNo, int iSectors);

//Delete all white space
static char *DBS_RemoveWhiteSpace(char *pszBuf);

///////////////////////////////////////////////////////////////////////////////
//Get all communication port informaiton
static int ReadCommPortInfo(char * pFileName);

///////////////////////////////////////////////////////////////////////////////
//Declare configuration file operation functions
static BOOL ReadCfgInfo(char * pszBuff, char * pszInfoName, 
				 int iSubFieldLine, int iSubFieldCol);
static BOOL SetCfgInfo(char * pszBuff, char * pszInfoName,
				int iSubFieldLine, int iSubFieldCol);
//////////////////////////////////////////////////////////////////////////////
extern int SetNetworkInfo(int nIfNum, 
				   int bSetIp,		ULONG ulIp,
				   int bSetMask,	ULONG ulMask,
				   int bSetGateway,	ULONG ulGateway, 
				   int bBroardcast,	ULONG ulBroardcast);

static BOOL SetDefaultPortSetting(void);				   
//////////////////////////////////////////////////////////////////////////////
//End function declaration

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//static void TestHisData(void);
///////////////////////////////////////////////////////////////////////////////
//Start Function Code
/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SERVICE_ARGUMENTS  *pArgs : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-28 17:07
 *==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs)
{	
	BOOL				bExitFlag;
	BOOL				bRebootSysCmd;
	BOOL				bMainToDsp;
	int					iErrCode;
	long				tmInterval;
    
	SERVICE_SWITCH_FLAG *pFlag;	
	int					iRestartEEMTimes;

	int					iRst;
	//int					iExitResult;
	HANDLE				DBS_hThreadSelf;
	RUN_THREAD_MSG		msg;

	UNUSED(pArgs);
	
	bRebootSysCmd		= FALSE;
	bMainToDsp			= FALSE;
    iErrCode	 		= ERR_COMM_OK;
	//////////////////////////////////////////////////////////
	//1.Allocate memory to global variable
	g_pDbsData = NEW(MOD_DBS_DATA, 1);
	if (g_pDbsData == NULL)
	{
		AppLogOut("DBS_INIT", APP_LOG_ERROR, 
			"Debug service interface doesn't get memory cornnectly.");
		return ERR_INIT_NO_MEMORY;
	}	
	//Set running mode
	g_pDbsData->g_iRunningMode = DSI_RUNNING_MODE;

	//2.Initiate all global variables
	Ini_Global_Vars(DSI_RUNNING_MODE);

	///////////////////////////////////////////////////////////////
	//3.Read port information from cfg file
	if (ERR_CFG_OK != ReadCommPortInfo(ACU_CFG_FILE))
	{
		AppLogOut("DBS_READ_PORT", APP_LOG_ERROR, "Fail to read port information,"
			"System will use default port setting to run debug service.");
		//Exit_DBS_Service();
		if (!SetDefaultPortSetting())
		{
			AppLogOut("DBS_READ_PORT", APP_LOG_ERROR, 
				"Fail to use default port setting.");
			return ERR_DBS_CFG_READ_FAIL;
		}

	}	
#ifdef _DEBUG_MAINTN_
	printf("Succeed to ReadCommPortInfo\n");
#endif

	Sleep(10000);  //Sleep ensure EEM initiate serial port firstly 2005/3/22

#ifdef _DEBUG_MAINTN_
	printf("End sleep for EEM initiate serial port.\n");
	TRACE("Time is: %.3f\n", GetCurrentTime());
#endif
	//////////////////////////////////////////////////////////////
	//4.Initiate all ports
	if (!DBS_Ini_Port())
	{
		AppLogOut("DBS_INIT_DSI", APP_LOG_ERROR, 
			"Fail to initiate communication ports."
			"Debug service interface doesn't start cornnectly.");	
		Exit_DBS_Service();
		return ERR_INIT_PORT_FAIL;   //Fail to ini_port
	}
#ifdef _DEBUG_MAINTN_
	printf("Succeed to DBS_Ini_Port\n");
#endif
	
	g_pDbsData->g_bDBS_RunFlag = TRUE;	
	DBS_hThreadSelf = RunThread_GetId(NULL);	

	g_pDbsData->tmLastTime = time(NULL);  //Get time for delay exit
	///////////////////////////////////////////////////////////////////////////
	/*RunThread_Create("TestHisData", 
						(RUN_THREAD_START_PROC)TestHisData,
						NULL,
						NULL,
						0);	*///It is used to test DAT_StorageReadRecords
	///////////////////////////////////////////////////////////////////////////
	//5.Continue recycling until exit message(g_iDBS_Running = I_EXIT_FLAG)
	pFlag = NULL;
	pFlag = DXI_GetServiceSwitchFlag();
	iRestartEEMTimes = 0;

	bExitFlag = FALSE;
	while(!bExitFlag)
	{
		g_pDbsData->tmCurrentTime = time(NULL);
		//To tell the thread manager I'm living.
		RunThread_Heartbeat(DBS_hThreadSelf);	
		Sleep(1000);
	
		iRst = RunThread_GetMessage(DBS_hThreadSelf, &msg, FALSE, 0);

		if (!THREAD_IS_RUNNING(DBS_hThreadSelf))
		{
			bExitFlag = TRUE;
			if (g_pDbsData->bHaveClient)
			{
#ifdef _DEBUG_MAINTN_
				printf("In ServiceMain, enter to Clear_CurrConnect\n");
#endif
				 Clear_CurrConnect();
			}			 
			break;
		}
	
		if(iRst == ERR_OK)
		{
			switch(msg.dwMsg)
			{
			case MSG_QUIT:
				bExitFlag = TRUE;
				break;
			default:
				bExitFlag = FALSE;
				break;
			}//switch(msg.dwMsg)
		}

		////////////////////////////////////////
		if (!g_pDbsData->bHaveClient)   //No client connected
		{
			if (g_pDbsData->g_hEthPort != NULL)
			{
				g_pDbsData->bHaveClient = AcceptClient(g_pDbsData->g_hEthPort, 
					&(g_pDbsData->g_hSubEthPort));

				//Succeed
				if (g_pDbsData->bHaveClient)
				{
					g_pDbsData->g_hPort = g_pDbsData->g_hSubEthPort;
				}
			}
	
			//if there is a network client, clear out serial port
			//if some client come to here from reporting serial port			
			if (!(g_pDbsData->bHaveClient))
			{
				ProcessSerialPort();
			}	

			//Someone powerkit connects.
			if (g_pDbsData->bHaveClient)
			{
				////////////////////////////////////////
				g_pDbsData->g_hRecSend = RunThread_Create("DBS_RSThread", 
						(RUN_THREAD_START_PROC)fnProcessThread,
						NULL,
						NULL,
						0);

				if( g_pDbsData->g_hRecSend == NULL)
				{
					//Clear old connect
					Clear_CurrConnect();
					g_pDbsData->bHaveClient  		= FALSE;
					
					if (pFlag->bMtnCOMHasClient || 
						g_pDbsData->g_bHaveSeriClient)
					{
						pFlag->bMtnCOMHasClient = FALSE;
						g_pDbsData->g_bHaveSeriClient = FALSE;
					}				
				}
				else
				{
					ReturnToPowerKit(MAIN_PRO_RUNNING, NULL);					
					//Reset counter
					g_pDbsData->tmLastTime = time(NULL);	
				}
			}

			g_pDbsData->tmCurrentTime = time(NULL);
			tmInterval = (long)(difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime));
			if (tmInterval > 2 * EXIT_DELAY_COUNTS)
			{
				g_pDbsData->tmLastTime = time(NULL);
			}

			//Sleep(1000); 
			//If there's no client, it will recycle run.
#ifdef _DEBUG_MAINTN_			
			if (tmInterval%5 == 0)
			{
				printf("DSI is running, waiting client coming.\n");
				printf("Current tmInterval is %d.\n", tmInterval);
				printf("Enter to Debug Maintenance "
				"RunThread_GetMessage.iRst is %d\n",iRst);
			}		
#endif
		}
		else    //Some client is connected and operating in another thread.
		{
			Sleep(1000);  
			g_pDbsData->tmCurrentTime = time(NULL);
            
			//Stop the process thread and disconnect client
			tmInterval = (long)difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime);
			if (((tmInterval >= OPERATE_DELAY_COUNTS) && 
				g_pDbsData->bHaveClient) ||
				g_pDbsData->g_bEndCurrConnect ||
				g_pDbsData->g_bMainToDsp)
			{
				if (g_pDbsData->g_bMainToDsp)
				{
					bMainToDsp = TRUE;
					bExitFlag = TRUE;
				}
				//If the connection is by serial port,the g_hRecSend should be
				//same name handle.
				g_pDbsData->bHaveClient  		= FALSE;
				g_pDbsData->g_bValidUser 		= FALSE;
				g_pDbsData->g_bEndCurrConnect	= FALSE;
		
				//Clear old connect
                Clear_CurrConnect();	
				//If current EEM Module is not running and DSI used the serial port
				//communication,it has to close current serial port and restart EEM service
				if (g_pDbsData->g_bHaveSeriClient) 					
				{
					pFlag->bMtnCOMHasClient = FALSE;
					g_pDbsData->g_bHaveSeriClient = FALSE;	

					//Directly connect serial port and communicate
					if (g_pDbsData->g_hSeriPort != NULL)
					{
						if (!DBS_Stop_Report_Port())
						{
							AppLogOut("DBS_SER_MAIN", APP_LOG_ERROR, 
								"Fail to close serial port,EEM service will not "
								"be restarted");
						}
						else
						{
							pFlag->bCOMHaveClosed = TRUE;
							pFlag->bMtnCOMHasClient = FALSE;
							g_pDbsData->g_bHaveSeriClient = FALSE;
						}
						//EEM will start its service by itself
					}
					else
					{
						pFlag->bCOMHaveClosed = TRUE;
					}

					if (pFlag->bCOMHaveClosed &&
						!(pFlag->bMtnCOMHasClient) && !bExitFlag)
					{
#ifdef _DEBUG_MAINTN_
						printf("EEM service is starting.\n");	
#endif										
						StartHLMSService();
					}
					//StartHLMSService();
				} //End if				
			} //End close current connection

#ifdef _DEBUG_MAINTN_
			tmInterval = (long)difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime);
			if (tmInterval%5 == 0)
			{
				printf("DSI is running, one client is operating.\n");	
				printf("Current tmInterval is %d.\n", tmInterval);
			}
#endif
			//If current network port is used by powerkit,EEM want switch to  
			//Serial port,DSI should free serial port resource immediately
			if ((g_pDbsData->g_hSubEthPort != NULL) && 
				(pFlag != NULL))
			{
				if (pFlag ->bHLMSUseCOM && pFlag->bMtnCOMHasClient)
				{
					if (!DBS_Stop_Report_Port())
					{
						AppLogOut("DBS_SER_MAIN", APP_LOG_ERROR, 
							"Fail to close serial port,EEM service will not "
							"be restarted");
					}
					else
					{
						pFlag->bCOMHaveClosed = TRUE;
						pFlag->bMtnCOMHasClient = FALSE;
						g_pDbsData->g_bHaveSeriClient = FALSE;
					}
				}
			}
		}  //End else	
		Sleep(100);

		if (g_pDbsData->g_bRebootCmd)
		{
			bRebootSysCmd	= TRUE;
			bExitFlag		= TRUE;
		}
		
	}//while(bExitFlag)  
	
	//////////////////////////////////////////////////////////////////////////
	g_pDbsData->g_bDBS_RunFlag = FALSE;
	Sleep(2000);
	//////////////////////////////////////////////////////////////////////////
	//6.End running, clear resource
	Exit_DBS_Service();

	//Send a message to main program
	//''''''''''''''''''''''''''
	//Wait main program send quiting msg 
	while(!bExitFlag)     //Not used now
	{
		RunThread_Heartbeat(DBS_hThreadSelf);	
		Sleep(1000);
		iRst = RunThread_GetMessage(DBS_hThreadSelf, &msg, FALSE, 0);

		if (!THREAD_IS_RUNNING(DBS_hThreadSelf))
		{
			bExitFlag = FALSE;	 
			break;
		}

		if(iRst == ERR_OK)
		{
			switch(msg.dwMsg)
			{
			case MSG_QUIT:
				bExitFlag = FALSE;  //Quiting command come here.
				break;
			default:
				bExitFlag = TRUE;
				break;
			}//switch(msg.dwMsg)
		}
	}

	/* If powerkit's operater forget to send command to debug service program,
	   it should make system reset.*/
	//7.If it needs reset system
	if (bRebootSysCmd)
	{
		//Send message to other thread and wait until they exit
		AppLogOut("DBS_INTERFACE", APP_LOG_INFO, "System will be reset.");
		Sleep(REBOOT_DELAY_TIME);
		//iExitResult = system("reboot");
        DXI_RebootACUorSCU(TRUE, TRUE);			
	}

	if (bMainToDsp)
	{
		raise(SIGUSR1);
		Sleep(10000);
	}
	
	return	0;		
}

///////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : ProcessSerialPort
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-03-17 11:02
 *==========================================================================*/
static BOOL ProcessSerialPort(void)
{
	SERVICE_SWITCH_FLAG *pFlag;
	HANDLE hMutexSerialSwitch;
	long	tmInterval;	
	APP_SERVICE *pEEMService;

	hMutexSerialSwitch = DXI_GetServiceSwitchMutex();
	pFlag = DXI_GetServiceSwitchFlag();

	if (pFlag == NULL)
	{
		return FALSE;
	}

	Mutex_Lock(hMutexSerialSwitch, 5000);
	if ((!(pFlag->bHLMSUseCOM)))
	{
		//Clear to zero
		g_pDbsData->g_iRestartHLMSTimes = 0;
#ifdef _DEBUG_MAINTN_
		//printf("g_pDbsData->g_iRestartHLMSTimes is %d.\n", g_pDbsData->g_iRestartHLMSTimes);
		//printf("pFlag->bMtnCOMHasClient is %d.\n", pFlag->bMtnCOMHasClient);						
#endif
		//Only let try three times
		if ((g_pDbsData->g_iReIniSeriPortTimes <= 3) &&
			(pFlag->bCOMHaveClosed) && 
			(g_pDbsData->g_hSeriPort == NULL))
		{
			//Initiate serial port
			if (!DBS_Ini_Report_Port())
			{
				AppLogOut("DBS_SER_MAIN", APP_LOG_WARNING, 
								"Fail to initiate serial port the %d times,"
								"please check your serial setting.", 
								g_pDbsData->g_iReIniSeriPortTimes);
				g_pDbsData->g_iReIniSeriPortTimes++;
			}
			else
			{
				g_pDbsData->g_iReIniSeriPortTimes = 0;
				pFlag->bCOMHaveClosed = FALSE;
				g_pDbsData->tmLastTime = g_pDbsData->tmCurrentTime;
			}
		}

		//Directly connect serial port and communicate
		if (g_pDbsData->g_hSeriPort != NULL)
		{				
			g_pDbsData->bHaveClient = HaveSeriClient(g_pDbsData->g_hSeriPort,
							&(g_pDbsData->g_hSubSeriPort));
			//Succeed
			if (g_pDbsData->bHaveClient)
			{
				g_pDbsData->g_hPort = g_pDbsData->g_hSubSeriPort;
				pFlag->bMtnCOMHasClient = TRUE;
				g_pDbsData->g_bHaveSeriClient = TRUE;
			}
			else
			{
				pFlag->bMtnCOMHasClient = FALSE;
				g_pDbsData->g_bHaveSeriClient = FALSE;

				tmInterval = (long)difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime);

				//Over time no powerkit client connect to serial port
				if (tmInterval >= EXIT_DELAY_COUNTS)
				{					
					pEEMService 
						= ServiceManager_GetService(SERVICE_GET_BY_LIB, 
						EEM_SOC_NAME);
					if (pEEMService == NULL)
					{
						g_pDbsData->tmLastTime = g_pDbsData->tmCurrentTime;
					}
					else
					{
						if (!(pEEMService->bServiceRunning))
						{
							pFlag->bHLMSUseCOM = TRUE;
							g_pDbsData->tmLastTime = g_pDbsData->tmCurrentTime;
						}  
					}
				}
			}
		}
	}
	else  //EEM want to change network to serial to communicate
	{
		//Directly connect serial port and communicate
		if (g_pDbsData->g_hSeriPort != NULL)
		{
			if (!DBS_Stop_Report_Port())
			{
				AppLogOut("DBS_SER_MAIN", APP_LOG_ERROR, 
					"Fail to close serial port,EEM service will not "
					"be restarted");
			}
			else
			{
				pFlag->bCOMHaveClosed = TRUE;
				pFlag->bMtnCOMHasClient = FALSE;
				g_pDbsData->g_bHaveSeriClient = FALSE;
			}
			//EEM will start its service by itself
		}		
		
		if (pFlag->bCOMHaveClosed &&
			!(pFlag->bMtnCOMHasClient) &&
			pFlag->bHLMSUseCOM && 
			(g_pDbsData->g_iRestartHLMSTimes < MAX_RESTART_EEM_TIMES))
		{
			StartHLMSService();
		}
		//Heihei have a rest
		Sleep(100);
	}
#ifdef _DEBUG_MAINTN_
			if (g_pDbsData->tmCurrentTime%5 == 0)
			{
				//printf("pFlag->bMtnCOMHasClient is %d.\n", pFlag->bMtnCOMHasClient);	
				//printf("pFlag->bCOMHaveClosed is %d.\n", pFlag->bCOMHaveClosed);	
				//printf("pFlag->bHLMSUseCOM is %d.\n", pFlag->bHLMSUseCOM);
			}
#endif
		
 
	Mutex_Unlock(hMutexSerialSwitch);

	return TRUE;
}
	

///////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : StartHLMSService
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-03-17 11:21
 *==========================================================================*/
static BOOL StartHLMSService(void)
{
	APP_SERVICE *pHLMSService;
	
	pHLMSService = ServiceManager_GetService(SERVICE_GET_BY_LIB, HLMS_SERVICE_NAME);
	if (!(pHLMSService->bServiceRunning))
	{
		g_pDbsData->g_iRestartHLMSTimes++;
		if (!Service_Start(pHLMSService))
		{
			AppLogOut("DBS_SER_MAIN", APP_LOG_ERROR, 
					"Debug service interface failed to restart HLMS service.");
			return FALSE;
		}
		else
		{
			AppLogOut("DBS_SER_MAIN", APP_LOG_INFO, 
					"DSI trys to restart HLMS service with the %d times.", 
					g_pDbsData->g_iRestartHLMSTimes);
		}
	}//End process eem service	
	else
	{
#ifdef _DEBUG_MAINTN_
		printf("HLMS may be running by network method.\n");
#endif
	}
	pHLMSService = NULL;

	return TRUE;
}
//Debug service program 
/*==========================================================================*
 * FUNCTION : DBS_Pro_Ini
 * PURPOSE  : Initiate different ports and start listenning thread
 * CALLS    : 
 * CALLED BY:   Debug service program main function
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  :	LIXIDONG                 DATE: 2004-10-05 15:15
 *==========================================================================*/
void DBS_Pro_Ini(void)
{
	int	 iErrCode;
	int	 iExitResult;
	char szCurrDir[256];
	long tmInterval;

	char * pszDir;

	//Get current working path
	pszDir = getcwd(szCurrDir, 256);	
	memmove((void *)g_szACUConfigDir, (void *)szCurrDir, strlen(szCurrDir));
	strcat(g_szACUConfigDir, "/config/");

	if (pszDir == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

    iErrCode	 		= ERR_DBS_OK;

	//////////////////////////////////////////////////////////
	//1.Allocate memory to global variable
	g_pDbsData = NEW(MOD_DBS_DATA, 1);
	if (g_pDbsData == NULL)
	{
		AppLogOut("DBS_INIT_DSP", APP_LOG_ERROR, 
			"Debug service program doesn't get memory cornnectly.");
		return;
	}	
	//Set running mode
	g_pDbsData->g_iRunningMode = DSP_RUNNING_MODE;

	//2.Initiate all global variables
	Ini_Global_Vars(DSP_RUNNING_MODE);

	///////////////////////////////////////////////////////////////
	//3.Read port information from cfg file
	if (ERR_CFG_OK != ReadCommPortInfo(ACU_CFG_FILE))
	{
		AppLogOut("DBS_READ_PORT", APP_LOG_ERROR, "Fail to read port information,"
			"System will use default port setting to run debug service.");
		//Exit_DBS_Service();
		if (!SetDefaultPortSetting())
		{
			AppLogOut("DBS_READ_PORT", APP_LOG_ERROR, 
				"Fail to use default port setting.");
			//Exit_DBS_Service();
			return;
		}
	}	
#ifdef _DEBUG_MAINTN_
	printf("Succeed to Read Cfg File.\n");
#endif
	//////////////////////////////////////////////////////////////
	//4.Initiate all ports
	if (!DBS_Ini_Port())
	{
		AppLogOut("DBS_PROG_INIT", APP_LOG_ERROR, 
			"Fail to initiate communication ports."
			"Debug service program doesn't start cornnectly.");	
		Exit_DBS_Service();
		return;   //Fail to ini_port
	}

#ifdef _DEBUG_MAINTN_
	printf("Succeed to DBS_Ini_Port.\n");
#endif

	//5.In debug service program it is necessary to initiate user management
	// It is different from debug service interface
	if (!InitUserManage())
	{
		AppLogOut("DBS_PROG_INIT", APP_LOG_ERROR, 
			"Fail to initiate users management."
			"Debug service program doesn't start cornnectly.");	
		Exit_DBS_Service();
		return;
	}

	g_pDbsData->g_bDBS_RunFlag = TRUE;
	
	//6.Continue recycling until receiving exit message(g_iDBS_Running = I_EXIT_FLAG)
	while (g_pDbsData->g_bDBS_RunFlag)
	{	
		if (!(g_pDbsData->bHaveClient))   //No client connected
		{
			if (g_pDbsData->g_hEthPort != NULL)
			{
				g_pDbsData->bHaveClient = AcceptClient(g_pDbsData->g_hEthPort, 
					&(g_pDbsData->g_hSubEthPort));

				if (g_pDbsData->bHaveClient)
				{
					g_pDbsData->g_hPort = g_pDbsData->g_hSubEthPort;
				}				
			}

			/////////////////////////////////////////////////////////////////
			if (g_pDbsData->g_hSeriPort != NULL && (!(g_pDbsData->bHaveClient)))
			{
#ifdef _DEBUG_MAINTN_
					printf("Start to HaveSeriClient., g_pDbsData->g_hSubSeriPort is %d\n",
						g_pDbsData->g_hSubSeriPort);
					printf("Start to HaveSeriClient., g_pDbsData->g_hSeriPort is %d\n",
						g_pDbsData->g_hSeriPort);
#endif
				g_pDbsData->bHaveClient = HaveSeriClient(g_pDbsData->g_hSeriPort, 
					&g_pDbsData->g_hSubSeriPort);

				if (g_pDbsData->bHaveClient)
				{
					g_pDbsData->g_hPort = g_pDbsData->g_hSubSeriPort;
#ifdef _DEBUG_MAINTN_
					printf("SerialPort AcceptClient is succeed.\n");
#endif
					//ReturnToPowerKit(DEBUG_PRO_RUNNING, NULL);
				}
			}

			if (g_pDbsData->bHaveClient)
			{
				g_pDbsData->g_hRecSend = RunThread_Create("RecSendThread", 
						(RUN_THREAD_START_PROC)fnProcessThread,
						NULL,
						NULL,
						0);
			
				/*
                if( pthread_create( &(g_pDbsData->g_hRecSend_t), 
					NULL,//&attr
					(PTHREAD_START_ROUTINE)fnProcessThread,
					NULL) != 0)
				*/
				if( g_pDbsData->g_hRecSend == NULL)
				{
					//Clear old connect
					Clear_CurrConnect();
					g_pDbsData->bHaveClient  		= FALSE;
				}
				else
				{
					ReturnToPowerKit(DEBUG_PRO_RUNNING, NULL);					
					//Reset counter
					g_pDbsData->tmLastTime = time(NULL);	
				}
			}

			Sleep(1000); 

#ifdef _DEBUG_MAINTN_
			printf("DSP is running, waiting client coming.\n");
#endif
			//Exit debug service program

			g_pDbsData->tmCurrentTime = time(NULL);
			tmInterval = (long)difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime);
			if (tmInterval >= EXIT_DELAY_COUNTS)
			{
				/////////////////////////////////
				/*
				if (g_pDbsData->g_hRecSend_t)
				{
					pthread_cancel(g_pDbsData->g_hRecSend_t);
					g_pDbsData->g_hRecSend_t = NULL;
				}
				*/
				if (g_pDbsData->g_hRecSend)
				{
					RunThread_Stop(g_pDbsData->g_hRecSend, 1000, 1);
					g_pDbsData->g_hRecSend = NULL;
				}

				g_pDbsData->g_bDBS_RunFlag = I_EXIT_FLAG;
			}
		}
		else    //Some client is connected and operating in another thread.
		{
			Sleep(1000);  
			//Stop the process thread and disconnect client
			g_pDbsData->tmCurrentTime = time(NULL);
			tmInterval = (long)difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime);
			if (((tmInterval >= OPERATE_DELAY_COUNTS) && 
				g_pDbsData->bHaveClient) ||
				g_pDbsData->g_bEndCurrConnect)  //client has exited
			{
				g_pDbsData->bHaveClient			= FALSE;
				g_pDbsData->g_bValidUser		= FALSE;
				g_pDbsData->g_bEndCurrConnect	= FALSE;						

				//Clear old connect
                Clear_CurrConnect();	
#ifdef _DEBUG_MAINTN_
				printf("Current thread has been stopped.\n");
#endif						
			}

		}//end else	

		Sleep(100);

		//Exit debug service program when out of time
		g_pDbsData->tmCurrentTime = time(NULL);
		tmInterval = (long)difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime);
		if (tmInterval >= EXIT_DELAY_COUNTS)
		{
			/* 
			if (g_pDbsData->g_hRecSend_t)
			{
				pthread_cancel(g_pDbsData->g_hRecSend_t);
				g_pDbsData->g_hRecSend_t = NULL;
			}
			*/
			if (g_pDbsData->g_hRecSend)
			{
				RunThread_Stop(g_pDbsData->g_hRecSend, 1000, 1);
				g_pDbsData->g_hRecSend = NULL;
			}

			g_pDbsData->g_bDBS_RunFlag = I_EXIT_FLAG;
			g_pDbsData->g_bRebootCmd = TRUE;
		}
		
	};//end while
	
	//6.End running, clear resource
	Exit_DBS_Service();
	DestroyUserManager();         //Clear user information resource
    
	/* If powerkit's operater forget to send command to debug service program,
	   it should make system reset.*/

	//7.If it needs to reset system
	if (g_pDbsData->g_bRebootCmd)
	{
		AppLogOut( "DBS_PRO_EXIT", APP_LOG_INFO, "System will be reset.");
		//iExitResult = system("reboot");
		DXI_RebootACUorSCU(TRUE, FALSE);  //Reboot SCU firstly
		Sleep(REBOOT_DELAY_TIME);
		//iExitResult = system("reboot");    //Reboot ACU secondly
		iExitResult = _SYSTEM("reboot");    //Reboot ACU secondly
	}
	
	return;
}




/////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : DBS_fnProProcess
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-10 19:43
 *==========================================================================*/
void DBS_fnProProcess(void)
{
	int iExitResult;
	long tmInterval;
	int iTest;
	iTest = 1;
	//1.Continue recycling until receiving exit message
	while (g_pDbsData->g_bDBS_RunFlag)
	{		
		//1.1-if
		if (!(g_pDbsData->bHaveClient))   //No client connected
		{
			//1.1.1 -if
			if (g_pDbsData->g_hEthPort != NULL)
			{
				g_pDbsData->bHaveClient = AcceptClient(g_pDbsData->g_hEthPort, 
					&(g_pDbsData->g_hSubEthPort));

				if (g_pDbsData->bHaveClient)
				{
					g_pDbsData->g_hPort = g_pDbsData->g_hSubEthPort;
				}				
			}//1.1.1 -endif

			/////////////////////////////////////////////////////////////////
			//1.1.2 -if
			if (g_pDbsData->g_hSeriPort != NULL && (!(g_pDbsData->bHaveClient)))
			{
				g_pDbsData->bHaveClient = HaveSeriClient(g_pDbsData->g_hSeriPort, 
					&g_pDbsData->g_hSubSeriPort);

				if (g_pDbsData->bHaveClient)
				{
					g_pDbsData->g_hPort = g_pDbsData->g_hSubSeriPort;
#ifdef _DEBUG_MAINTN_
					printf("SerialPort AcceptClient is succeed.\n");
#endif
					//ReturnToPowerKit(DEBUG_PRO_RUNNING, NULL);
				}
			}//1.1.2 -endif

			////1.1.3 -if
			if (g_pDbsData->bHaveClient)
			{
				g_pDbsData->g_hRecSend = RunThread_Create("RecSendThread", 
						(RUN_THREAD_START_PROC)fnProcessThread,
						NULL,
						NULL,
						0);
			
				if( g_pDbsData->g_hRecSend == NULL)
				{
					//Clear old connect
					Clear_CurrConnect();
					g_pDbsData->bHaveClient  		= FALSE;
				}
				else
				{
					ReturnToPowerKit(DEBUG_PRO_RUNNING, NULL);					
					//Reset counter
					g_pDbsData->tmLastTime = time(NULL);	
				}
			}////1.1.3 -endif

			Sleep(1000); 

			//Exit debug service program
			g_pDbsData->tmCurrentTime = time(NULL);
			tmInterval = (long)difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime);
#ifdef _DEBUG_MAINTN_
			//printf("DSP is running, waiting client coming.\n");
			//printf("Current tmInterval is %d.\n", tmInterval);
#endif
			if (tmInterval >= EXIT_DELAY_COUNTS)
			{
				if (g_pDbsData->g_hRecSend)
				{
					RunThread_Stop(g_pDbsData->g_hRecSend, 1000, 1);
					g_pDbsData->g_hRecSend = NULL;
				}
				
				g_pDbsData->g_bDBS_RunFlag = I_EXIT_FLAG;
				g_pDbsData->g_bRebootCmd = TRUE;

				AppLogOut("DBS_PRO_FUN", APP_LOG_INFO, "Debug service program"
					" exceeded 5 minutes without powerkit's signal.");
			}
		}////1.2 -else
		else//////////Some client is connected and operating in another thread.
		{
			Sleep(1000);  
			//Stop the process thread and disconnect client
			//1.2.1-if start
			g_pDbsData->tmCurrentTime = time(NULL);
			tmInterval = (long)difftime(g_pDbsData->tmCurrentTime, 
				g_pDbsData->tmLastTime);

			if (((tmInterval >= OPERATE_DELAY_COUNTS) && 
				g_pDbsData->bHaveClient) ||
				g_pDbsData->g_bEndCurrConnect)  //client has exited
			{
#ifdef _DEBUG_MAINTN_
				printf("It will end current thread.\n");
#endif
				g_pDbsData->bHaveClient			= FALSE;
				g_pDbsData->g_bValidUser		= FALSE;
				g_pDbsData->g_bEndCurrConnect	= FALSE;						

				//Clear old connect
                Clear_CurrConnect();	
#ifdef _DEBUG_MAINTN_
				printf("Current thread has been stopped.\n");
#endif
			}//1.2.1-else -end

		}//////1.x endif

		Sleep(100);
		RunThread_Heartbeat(g_pDbsData->g_hDbsMainHandle);		
	};//end while

	//2.If it needs to reset system
	if (g_pDbsData->g_bRebootCmd)
	{
		Exit_DBS_Service();
		AppLogOut( "DBS_PRO_EXIT", APP_LOG_INFO, "System will be reset.");
		//iExitResult = system("reboot");
		DXI_RebootACUorSCU(TRUE, FALSE);  //Reboot SCU firstly
		Sleep(REBOOT_DELAY_TIME);
		//iExitResult = system("reboot");    //Reboot ACU secondly
		iExitResult = _SYSTEM("reboot");    //Reboot ACU secondly
	}
}

/*==========================================================================*
 * FUNCTION : DBS_IniDebugPro
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-10 19:17
 *==========================================================================*/
BOOL DBS_IniDebugPro(void)
{
	int	 iErrCode;

	char szCurrDir[256];

	char * pszDir;

	//Get current working path
	pszDir = getcwd(szCurrDir, 256);	
	memmove((void *)g_szACUConfigDir, (void *)szCurrDir, strlen(szCurrDir));
	strcat(g_szACUConfigDir, "/config/");

	if (pszDir == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return FALSE;
	}

    iErrCode	 		= ERR_DBS_OK;

	//////////////////////////////////////////////////////////
	//1.Allocate memory to global variable
	g_pDbsData = NEW(MOD_DBS_DATA, 1);
	if (g_pDbsData == NULL)
	{
		AppLogOut("DBS_INIT_DSP", APP_LOG_ERROR, 
			"Debug service program doesn't get memory cornnectly.");
		return FALSE;
	}	
	//Set running mode
	g_pDbsData->g_iRunningMode = DSP_RUNNING_MODE;

	//2.Initiate all global variables
	Ini_Global_Vars(DSP_RUNNING_MODE);

	///////////////////////////////////////////////////////////////
	//3.Read port information from cfg file
	if (ERR_CFG_OK != ReadCommPortInfo(ACU_CFG_FILE))
	{
		AppLogOut("DBS_READ_PORT", APP_LOG_ERROR, "Fail to read port information,"
			"System will use default port setting to run debug service.");
		//Exit_DBS_Service();
		if (!SetDefaultPortSetting())
		{
			AppLogOut("DBS_READ_PORT", APP_LOG_ERROR, 
				"Fail to use default port setting.");
			return FALSE;
		}
	}

#ifdef _DEBUG_MAINTN_
	printf("Succeed to Read Cfg File.\n");
#endif
	//////////////////////////////////////////////////////////////
	//4.Initiate all ports
	if (!DBS_Ini_Port())
	{
		AppLogOut("DBS_PROG_INIT", APP_LOG_ERROR, 
			"Fail to initiate communication ports."
			"Debug service program doesn't start cornnectly.");	
		Exit_DBS_Service();
		return FALSE;   //Fail to ini_port
	}

#ifdef _DEBUG_MAINTN_
	printf("Succeed to DBS_Ini_Port.\n");
#endif

	//5.In debug service program it is necessary to initiate user management
	// It is different from debug service interface
	if (!InitUserManage())
	{
		AppLogOut("DBS_PROG_INIT", APP_LOG_ERROR, 
			"Fail to initiate users management."
			"Debug service program doesn't start cornnectly.");	
		Exit_DBS_Service();
		return FALSE;
	}

	g_pDbsData->g_bDBS_RunFlag = TRUE;

	g_pDbsData->g_hDbsMainHandle = RunThread_Create("DBS_DSP_PRO", 
						(RUN_THREAD_START_PROC)DBS_fnProProcess,
						NULL,
						NULL,
						0);

	if (g_pDbsData->g_hDbsMainHandle == NULL)
	{
		AppLogOut("DBS_THD_CREATE", APP_LOG_ERROR, 
			"Fail to Create debug service program main thread."
			"Debug service program can't start cornnectly.");	
		Exit_DBS_Service();
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : DBS_ExitDebugService
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-10 19:45
 *==========================================================================*/
void DBS_ExitDebugService(void)
{
	//1.Exit main thread handle
	if (g_pDbsData->g_hDbsMainHandle)
	{
		RunThread_Stop(g_pDbsData->g_hDbsMainHandle, 1000, 1);
		g_pDbsData->g_hDbsMainHandle = NULL;
	}

	//2.End running, clear resource
	Exit_DBS_Service();
	DestroyUserManager();         //Clear user information resource	

	return;
}

///////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : Ini_Global_Vars
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  : int iRunningMode
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-04 17:22
 *==========================================================================*/
static void Ini_Global_Vars(int iRunningMode)
{
	//Program running mode
	g_pDbsData->g_iRunningMode		= iRunningMode;

	//Program running flag
	g_pDbsData->g_bDBS_RunFlag		= TRUE;

	//Port information
	g_pDbsData->g_pPortInfo			= NULL;		
		
	//g_pDbsData->g_iCounter		= 0;
	g_pDbsData->tmLastTime			= time(NULL);
	g_pDbsData->tmCurrentTime		= time(NULL);

	//Communication handles
	g_pDbsData->g_hPort				= NULL;
	g_pDbsData->g_hEthPort			= NULL;
	g_pDbsData->g_hSubEthPort		= NULL;
	g_pDbsData->g_hSeriPort			= NULL;
	g_pDbsData->g_hSubSeriPort		= NULL;
	g_pDbsData->g_hSamplerPort1		= NULL;
	g_pDbsData->g_hSamplerPort2		= NULL;

	//Logical variables
	g_pDbsData->bHaveClient   		= FALSE; //Client connects flag
	g_pDbsData->g_bRebootCmd		= FALSE; //Reset flag
	g_pDbsData->g_bEndCurrConnect	= FALSE; //End current client flag
	g_pDbsData->g_bValidUser 		= FALSE;
	g_pDbsData->g_bHaveSeriClient   = FALSE;
	g_pDbsData->g_bMainToDsp		= FALSE;

	//Thread handle

	//g_pDbsData->g_hRecSend_t		= NULL;
	g_pDbsData->g_hRecSend			= NULL;
	g_pDbsData->g_hDbsMainHandle	= NULL;
	g_pDbsData->bEndUnzip			= FALSE;

	g_pDbsData->g_pFileInfo			= NULL;
	g_pDbsData->g_iFileNum			= 0;

	g_pDbsData->g_iRestartHLMSTimes		= 0;
	g_pDbsData->g_iReIniSeriPortTimes	= 0;
}

/*==========================================================================*
 * FUNCTION : SetDefaultPortSetting
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   BOOL : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-23 11:16
 *==========================================================================*/
static BOOL SetDefaultPortSetting(void)
{
	int i;
	DBS_PORT_INFO *pPortInfo;

	if (g_pDbsData->g_pPortInfo)
	{
		DELETE(g_pDbsData->g_pPortInfo);
		g_pDbsData->g_pPortInfo = NULL;
	}

	g_pDbsData->g_pPortInfo = NEW(DBS_PORT_INFO, TOTAL_PORT_NUMS);
	
	if (g_pDbsData->g_pPortInfo == NULL)
	{
		return FALSE;
	}

	g_pDbsData->g_iComPortNums = TOTAL_PORT_NUMS;

	pPortInfo = g_pDbsData->g_pPortInfo;
	for (i = 0; i < TOTAL_PORT_NUMS; i++, pPortInfo++)
	{
		/* 1.The function point of iPortID field */
		pPortInfo->iPortID = i + 1;	

		/* 2.The PortName field- Not used*/
		//Do nothing due to not use the portname
		
		/* 3.The iStdPortTypeID field */
		if ((pPortInfo->iPortID == 1) || (pPortInfo->iPortID == 4))
		{
			pPortInfo->iStdPortTypeID = SERIAL_TYPE_PORT;
		}
		else if(pPortInfo->iPortID == 2)
		{
			pPortInfo->iStdPortTypeID = SAMPLING_PORT2;
		}
		else
		{
			pPortInfo->iStdPortTypeID = NET_TYPE_PORT;
		}

		/* 4.Device Descriptor field */
		if (pPortInfo->iPortID == 1) 
		{
			memmove((void *)(pPortInfo->szDeviceDesp), SAMPLER_PORT_I_DESCR, 
				(unsigned)SERIAL_PORT_DESC_LEN);
			*(pPortInfo->szDeviceDesp + SERIAL_PORT_DESC_LEN) = 0;
		}
		else if(pPortInfo->iPortID == 2)
		{
			memmove((void *)(pPortInfo->szDeviceDesp), SAMPLER_PORT_II_DESCR, 
				(unsigned)SERIAL_PORT_DESC_LEN);
			*(pPortInfo->szDeviceDesp + SERIAL_PORT_DESC_LEN) = 0;
		}
		else if(pPortInfo->iPortID == 3)
		{
			memmove((void *)(pPortInfo->szDeviceDesp), NET_PORT_DESCR, 
				(unsigned)NET_PORT_DESC_LEN);
			*(pPortInfo->szDeviceDesp + NET_PORT_DESC_LEN) = 0;
		}
		else
		{
            memmove((void *)(pPortInfo->szDeviceDesp), REPORT_PORT_DESCR, 
				(unsigned)SERIAL_PORT_DESC_LEN);
			*(pPortInfo->szDeviceDesp + SERIAL_PORT_DESC_LEN) = 0;
		}

		/* 5.The Disconnect Delay(s) field */
		pPortInfo->iDisconnDelay = (-1);

		/* 6.Timeout information */
		if ((pPortInfo->iPortID == 1) || (pPortInfo->iPortID == 2)) 
		{
			pPortInfo->iTimeout = SAMPLING_PORT_TIMEOUT;
		}		
		else if(pPortInfo->iPortID == 3)
		{
			pPortInfo->iTimeout = NET_PORT_TIMEOUT;
		}
		else
		{
			pPortInfo->iTimeout = SERIAL_PORT_TIMEOUT;
		}

		/* 7.Parameter setting information */
		if(pPortInfo->iPortID == 3)
		{
			 memmove((void *)(pPortInfo->szSetting), NET_COM_SETTING, 
				(unsigned)NET_COM_SETTING_LEN);
			*(pPortInfo->szSetting + NET_COM_SETTING_LEN) = 0;
		}
		else
		{
             memmove((void *)(pPortInfo->szSetting), SERAIL_COM_SETTING, 
				(unsigned)SERAIL_COM_SETTING_LEN);
			*(pPortInfo->szSetting + SERAIL_COM_SETTING_LEN) = 0;
		}
	}
	
	return TRUE;
}
/*==========================================================================*
 * FUNCTION : ParsePortInfo
 * PURPOSE  : parsing communication port information
 * CALLS    : Cfg_SplitStringEx
 * CALLED BY: LoadCommPortInfo
 * ARGUMENTS: IN char				*szBuf       : line data
 *            DBS_PORT_INFO			*pPortInfo : to store parsed data
 * RETURN   : int : (-1) loaded failed , zero for success
 * COMMENTS : 
 * CREATOR  : LIXIDONG                   DATE: 2004-11-25 09:49
 *==========================================================================*/
static int ParsePortInfo(IN char *szBuf, OUT DBS_PORT_INFO *pPortInfo)
{
	char	*pField;

	if ((szBuf == NULL) || (pPortInfo == NULL))
	{
		return (-1);
	}

	/* 1.The function point of iPortID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, TAB);
	pPortInfo->iPortID = atoi(pField);	

	/* 2.The PortName field- Not used*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, TAB);
	//Do nothing due to not use the portname
	
	/* 3.The iStdPortTypeID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, TAB);
	pPortInfo->iStdPortTypeID = atoi(pField);

	/* 4.Device Descriptor field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, TAB);
	memmove((void *)(pPortInfo->szDeviceDesp), (void *)pField, 
		(unsigned)strlen(pField));
	*(pPortInfo->szDeviceDesp + strlen(pField)) = 0;

	/* 5.The Disconnect Delay(s) field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, TAB);
	pPortInfo->iDisconnDelay = atoi(pField);

	/* 6.Timeout information */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, TAB);
	//pPortInfo->iTimeout = atoi(pField);
	pPortInfo->iTimeout = 1000;
	//Set all iTimeOut as 1s, in order to response powerkit quickly

	/* 7.Parameter setting information */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, TAB);
	memmove((void *)(pPortInfo->szSetting), (void *)pField, 
		(unsigned)strlen(pField));
	*(pPortInfo->szSetting + strlen(pField)) = 0;

	return 0;
}

/*==========================================================================*
 * FUNCTION : LoadCommPortInfo
 * PURPOSE  : 
 * CALLS    : ParsePortInfo
 * CALLED BY: ReadCommPortInfo
 * ARGUMENTS: IN void   *pCfg       : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 10:34
 *==========================================================================*/
static int LoadCommPortInfo(IN void *pCfg)
{
	CONFIG_TABLE_LOADER loader;
	
	DEF_LOADER_ITEM(&loader, NULL, &(g_pDbsData->g_iComPortNums),
		COMM_SETTING_FIELD, 
		&(g_pDbsData->g_pPortInfo), ParsePortInfo);

	if (Cfg_LoadTables(pCfg, 1, &loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;	
}
/*==========================================================================*
 * FUNCTION : ReadCommPortInfo
 * PURPOSE  : 
 * CALLS    : LoadCommPortInfo
 * CALLED BY: 
 * ARGUMENTS:  char * pFileName : 
 * RETURN   :   int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-04 15:16
 *==========================================================================*/
static int ReadCommPortInfo(char * pFileName)
{
	int  iRst;
	void *pProf;  
	FILE *pFile;
	char *szInFile;
	size_t ulFileLen;

	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(pFileName, szCfgFileName, MAX_FILE_PATH);

#ifdef _DEBUG_MAINTN_
	printf("szCfgFileName is %s\n", szCfgFileName);
#endif
	/* open file */
	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{		
		return ERR_CFG_FILE_OPEN;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return ERR_CFG_NO_MEMORY;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);
		return ERR_CFG_FILE_READ;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);
	
	if (pProf == NULL)
	{
		DELETE(szInFile);
		return ERR_CFG_FILE_READ;
	}
	
	//1.Read current all command content and command numbers

    iRst = LoadCommPortInfo(pProf);

	if (iRst != ERR_CFG_OK)
	{
		AppLogOut("DBS_READ_PORT_IN", APP_LOG_ERROR, 
				"Fail to load communication port informaion.");

		DELETE(pProf); 
		DELETE(szInFile);
		return iRst;
	}

   	DELETE(pProf); 
	DELETE(szInFile);
	return ERR_CFG_OK;
}
//2.1 Initiate ACU ports in debug service program.
/*==========================================================================*
 * FUNCTION : DBS_Ini_Port
 * PURPOSE  : Open the port,in order to listen client connection
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :	BOOL  : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG         DATE: 2004-10-05 15:15
 *==========================================================================*/
static BOOL DBS_Ini_Port(void)
{
	BOOL	bResult;	
	DBS_PORT_INFO		*pPortInfo;
	SERVICE_SWITCH_FLAG *pFlag;
	HANDLE				hMutexServiceSwitch;
	int		iCtrl;
	BOOL	bSerialSucceed, bNetSucceed;

	//////////////////////////////////////////
	bSerialSucceed	= TRUE;
	bNetSucceed		= TRUE;

	pPortInfo  =  g_pDbsData->g_pPortInfo;
	if (pPortInfo == NULL)
	{
		return FALSE;
	}
	bResult	= FALSE;

	//Set first value to variable
	//If current program is debug service interface, it it true
	if (g_pDbsData->g_iRunningMode == DSI_RUNNING_MODE)
	{
		for(iCtrl = 0; iCtrl < g_pDbsData->g_iComPortNums; iCtrl++)
		{
			if (pPortInfo->iPortID == NET_PORT_ID)
			{
				bResult = Ini_OnePort(pPortInfo, &(g_pDbsData->g_hEthPort));
				bNetSucceed = bResult;
				if (!bResult)
				{
					AppLogOut("DBS_INI_PORT", APP_LOG_ERROR, 
					"In debug service interface, Fail to open ethernet port.");
					return FALSE;
				}
			}
			else if (pPortInfo->iPortID == REPORT_PORT_ID)
			{
				hMutexServiceSwitch = DXI_GetServiceSwitchMutex();				 
				pFlag = DXI_GetServiceSwitchFlag();
				if (pFlag != NULL)
				{	
					Mutex_Lock(hMutexServiceSwitch, 5000);
					if (pFlag->bCOMHaveClosed && (!(pFlag->bHLMSUseCOM)))
					{
						bResult = Ini_OnePort(pPortInfo, &(g_pDbsData->g_hSeriPort));
						bSerialSucceed = bResult;
						//It must succeed.Service can be started.
						if (!bResult)
						{
							//Let it be
							AppLogOut("DBS_INI_PORT", APP_LOG_ERROR, 
								"In debug service interface,Fail to open report serial port.");
						}
						else
						{
							pFlag->bCOMHaveClosed = FALSE;
						}
					}

					Mutex_Unlock(hMutexServiceSwitch);
				}
			}
			pPortInfo++;
		}		
	}
	else //If current program is debug service program, it it true
	{
		for(iCtrl = 0; iCtrl < g_pDbsData->g_iComPortNums; iCtrl++)
		{
			//Process variable
			if (pPortInfo->iPortID == NET_PORT_ID)
			{
				bResult = Ini_OnePort(pPortInfo, &(g_pDbsData->g_hEthPort));
				//It must succeed.Service can be started.
				bNetSucceed = bResult;
				if (!bResult)
				{
					AppLogOut("DBS_INI_PORT", APP_LOG_ERROR, 
						"In debug service program,Fail to open ethernet port.");

					if (!bSerialSucceed)
					{
						//Two methods failed
						return FALSE;
					}
				}
			}
			else if (pPortInfo->iPortID == REPORT_PORT_ID)
			{
				bResult = Ini_OnePort(pPortInfo, &(g_pDbsData->g_hSeriPort));
				bSerialSucceed = bResult;
				//It must succeed.Service can be started.
				if (!bResult)
				{
					AppLogOut("DBS_INI_PORT", APP_LOG_ERROR, 
						"In debug service program,Fail to open report serial port.");

					if (!bNetSucceed)
					{
						//Two methods failed
						return FALSE;
					}
				}
			}
			else if (pPortInfo->iPortID == SAMPLER_PORT_I_ID)
			{
				bResult = Ini_OnePort(pPortInfo, &(g_pDbsData->g_hSamplerPort1));
				if (!bResult)
				{
					AppLogOut("DBS_INI_PORT", APP_LOG_WARNING, 
						"In debug service program,Fail to open samplering port1");
				}
			}
			

			pPortInfo++;
		}	
	}	

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : GetValidIpAddr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ULONG *  puIPAddr : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-04 18:47
 *==========================================================================*/
static BOOL GetValidIpAddr(ULONG * puIPAddr)
{
	int		iBufLen, iVarID, iError, iTimeOut;
	ACU_NET_INFO stACUNetInfo;

	/////////////////////////////////////
	//Get local IP address,add port number
	iTimeOut = 100;
	iError = ERR_DXI_OK;
	iVarID = NET_INFO_ALL;
	iBufLen = sizeof(ACU_NET_INFO);

	iError += DxiGetData(VAR_ACU_NET_INFO,
			iVarID,			
			0,		
			&iBufLen,			
			&stACUNetInfo,			
			iTimeOut);
	
	if (iError != ERR_DBS_OK)
	{
		AppLogOut("DBS_INI_GETIP", APP_LOG_ERROR,  
			"It can't get ip address %s",
			"PowerKit can't use net connect!");

		return FALSE;
	}
	
	*puIPAddr	=  stACUNetInfo.ulIp;

	return TRUE;
}
/*==========================================================================*
 * FUNCTION : *GetStddPortDriverName
 * PURPOSE  : Because data exchange interface isn't running under debug service
 *			  program
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int  iStdPortType : From Cfg solution definition
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-12 11:43
 *==========================================================================*/
static char *GetStddPortDriverName(IN int iStdPortType)
{
	const char *pszStdDriver[] =
	{
		"xxx.so",
		"comm_net_tcpip.so",			
		"comm_std_serial.so",
		"comm_std_serial.so",
		"comm_hayes_modem.so",		
		"comm_acu_485.so"
	};

	return (char *)pszStdDriver[iStdPortType];
}
/*==========================================================================*
 * FUNCTION : Ini_OnePort
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   DBS_PORT_INFO	* pPortInfo 
 *				HANDLE			* pHandle
 * RETURN   :   BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-10 17:12
 *==========================================================================*/
static BOOL Ini_OnePort(DBS_PORT_INFO * pPortInfo, HANDLE * pHandle)
{
	int		iErrCode;
	ULONG	ulIP;
	DWORD	dwPortAttr;
	struct  in_addr inIp;

	if (pPortInfo == NULL)
	{
		return FALSE;
	}

	iErrCode = ERR_COMM_OK;        // nLastErr = 0;

	//Process variable
	if (pPortInfo->iStdPortTypeID == BASIC_PORT_TCP)
	{
		if (GetValidIpAddr(&ulIP))
		{
			//(ULONG)inIp.s_addr = ulIP;
			memset(pPortInfo->szSetting, 0, sizeof(pPortInfo->szSetting));
			sprintf(pPortInfo->szSetting, "%s:%d",inet_ntoa(inIp), 
				NET_PORT_NUM); 
#ifdef _DEBUG_MAINTN_
			printf("pPortInfo->szSetting is %s\n", pPortInfo->szSetting);
#endif
		}
		else
		{
			AppLogOut("DBS_INI_ONEPORT", APP_LOG_ERROR, "Fail open network port.");
			return FALSE;
		}

		dwPortAttr	= COMM_SERVER_MODE;
	}
	else if (pPortInfo->iPortID == REPORT_PORT_ID)
	{
        dwPortAttr	= COMM_SERVER_MODE;
		//pPortInfo->iTimeout = 1000;
	}
	else   //Sampler port
	{
		dwPortAttr	= COMM_CLIENT_MODE;
	}

	/////////////////////////////////////
	//Open the communication port
#ifdef _DEBUG_MAINTN_
			//printf("GetStddPortDriverName(pPortInfo->iStdPortTypeID) is %s\n",
			//	GetStddPortDriverName(pPortInfo->iStdPortTypeID));
			//printf("pPortInfo->szDeviceDesp is %s\n",pPortInfo->szDeviceDesp);
			//printf("pPortInfo->szSetting is %s\n",pPortInfo->szSetting);
			//printf("pPortInfo->iStdPortTypeID is %d\n",pPortInfo->iStdPortTypeID);
			//printf("pPortInfo->iTimeout is %d\n",pPortInfo->iTimeout);
#endif
	*pHandle = CommOpen(
		GetStddPortDriverName(pPortInfo->iStdPortTypeID),  
		pPortInfo->szDeviceDesp, 
		pPortInfo->szSetting, 
		dwPortAttr, 
		pPortInfo->iTimeout,
		&iErrCode);

	if (*pHandle == NULL)
	{
#ifdef _DEBUG_MAINTN_
			printf("iErrCode is %d\n", iErrCode);
#endif	
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

//2 Accept client connection
/*==========================================================================*
 * FUNCTION : AcceptClient
 * PURPOSE  : 
 * CALLS    : ReturnToPowerKit(), pthread_create()
 * CALLED BY: 
 * ARGUMENTS: HANDLE : hPort	
 *			: HANDLE : *hClient
 * RETURN   : BOOL	 : TRUE: Accept client successfully   FALSE:Fail to accept
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-05 15:15
 *==========================================================================*/
static BOOL AcceptClient(HANDLE hPort, HANDLE *hClient)
{
	*hClient = CommAccept(hPort);	

	//Check port client 
	if (*hClient != NULL) 
	{	
		//Answer the client connection
		//Debug program is running,you can do Rights verification	
		return TRUE;					
	}
	return FALSE;		
}


/*==========================================================================*
 * FUNCTION : ResponseClose
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hWrite : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-27 14:55
 *==========================================================================*/
static void ResponseClose(HANDLE hWrite)
{
	DATA_FRAME stDataFrame;
	char szBuffer[MAX_BUFFER_SIZE];
	int  iLength, iCheckLen;
	int  iBytesWritten;
	
	stDataFrame.bySOH		= 0x7e;
	stDataFrame.bySEQ		= 0x01;
	stDataFrame.byACUAddr	= 0x00;
	stDataFrame.byCID1		= 0xD5;
	stDataFrame.byFlag		= 0;
	stDataFrame.iDataLength = 0;	
	stDataFrame.byCID2 = IS_OK;			

	if (stDataFrame.iDataLength == 0) //No data to sent
	{
		//Data will be packed here				 
		iCheckLen = sizeof(stDataFrame) - FRAME_HEAD 
			- FRAME_END - CRC32_BYTES;

		stDataFrame.dwCRC32 = RunCRC32((const unsigned char *)&(stDataFrame) 
			+ FRAME_HEAD, 
			(unsigned long)iCheckLen, 
			(unsigned long)CRC32_DEFAULT);

		stDataFrame.byEOF = 0x0D;

		iLength = sizeof(stDataFrame);
		iBytesWritten = CommWrite (hWrite, 
						(char *)&stDataFrame, 
						iLength);

		
#ifdef _DEBUG_MAINTN_
		printf("iBytesWrite is %d\n", iBytesWritten);
#endif //_DEBUG_MAINTN_
	}
	
	return;
}
/*==========================================================================*
 * FUNCTION : HaveSeriClient
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hPort : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-11 13:48
 *==========================================================================*/
static BOOL HaveSeriClient(HANDLE hPort, HANDLE *pPort)
{	
	DATA_FRAME * pDataFrame;
	int		i, iCounter;
	int		iBytesToRead, iBytesRead;
	char	szBuffer[MAX_DATABUFF_LEN];

#ifdef _DEBUG_MAINTN_
	//printf("Enter to HaveSeriClient.\n");
#endif
	//Restart accept
	if (*pPort == NULL) 
	{
		//Only do once
		*pPort = CommAccept(hPort);	 //It will need a lot of time
		if (*pPort == NULL)
		{
#ifdef _DEBUG_MAINTN_
			//printf("Fail to accept Serial Client.\n");
#endif
			return FALSE;
		}
	}
	
	iBytesRead		= 0;
	iBytesToRead	= MAX_DATABUFF_LEN;
	pDataFrame		= NULL;
	i				= 0;
	iCounter		= 0;

	//To read three time from an opened serial port 
	while(iCounter < MAX_SERI_READ_TIMES)
	{
		memset(szBuffer, 0, (unsigned)MAX_DATABUFF_LEN);
		iBytesRead = CommRead (*pPort, 
							   szBuffer, 
						       iBytesToRead);	

		//No data to process
		if(iBytesRead < MIN_FRAME_LENGTH)//MIN_FRAME_LENGTH)
		{
#ifdef _DEBUG_MAINTN_
			printf("iBytesRead is %d\n", iBytesRead);
			
			if (iBytesRead > 0)
			{
				printf("iBytesRead is %s\n", szBuffer);
			}
#endif
			Sleep(100);		
		}
		else
		{
			//fresh the counter
#ifdef _DEBUG_MAINTN_
			printf("iBytesRead is %d\n", iBytesRead);
#endif
			//Find the start postion
			for (i = 0; i < iBytesRead; i++)
			{
				pDataFrame = (DATA_FRAME *)(&szBuffer[i]);
                //if ((BYTE)szBuffer[i] == 0x7e)
				if (pDataFrame->bySOH == 0x7e)
				{	
					break;															
				}
			}

			//Associate PowerKit close serial port
			if (pDataFrame->byCID2 == END_CURR_CONNECTION)
			{
				ResponseClose(*pPort);
			}

			//There is a client connecting
			if (i < iBytesRead && pDataFrame->byCID2 == CONNECT_INFO_ID)
			{
				DBS_ClearRxTx(*pPort);
				break;
			}
	
		}//end else
		
		Sleep(100);
		iCounter++;
	}//end while recycle

	if (iCounter >= MAX_SERI_READ_TIMES)
	{
		return FALSE;
	}

	return TRUE;	
}
/*==========================================================================*
 * FUNCTION : Clear_CurrConnect
 * PURPOSE  : Clear current connection resource
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 	 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-09 14:30
 *==========================================================================*/
static void Clear_CurrConnect(void)
{
	int iErrors;

	//It must be stopped before closing the g_hPort
	if (g_pDbsData->g_hRecSend)
	{
		RunThread_Stop(g_pDbsData->g_hRecSend, 1000, 1);
		g_pDbsData->g_hRecSend = NULL;
	}

	/*It must be stopped after closing the RunThread,because 
	  thread use the the g_hPort.*/
    if (g_pDbsData->g_hPort != NULL)
	{
		//It needs to close net port
		if (g_pDbsData->g_hSubEthPort)
		{
			iErrors = CommClose(g_pDbsData->g_hSubEthPort);
			if (iErrors != ERR_COMM_OK)
			{
#ifdef _DEBUG_MAINTN_
				printf("In Clear_CurrConnect, CommClose error is %d\n", 
					iErrors);
#endif				
			}
			g_pDbsData->g_hSubEthPort = NULL;

		}
		g_pDbsData->g_hPort = NULL;	
	}

	g_pDbsData->tmLastTime = time(NULL);

	//If do file upload,it is necessary to delete buffer
	if (g_pDbsData->g_pFileInfo)
	{
		DELETE(g_pDbsData->g_pFileInfo);
		g_pDbsData->g_pFileInfo = NULL;
		g_pDbsData->g_iFileNum = 0;
	}

#ifdef _DEBUG_MAINTN_
	printf("Clear current connect correctly.\n");
#endif

	return;
}

/*==========================================================================*
 * FUNCTION : Exit_DBS_Service
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-05 09:16
 *==========================================================================*/
static void Exit_DBS_Service(void)
{
	int iResult;
	//It must be stopped before closing port handle
	//Thread handle
	if (g_pDbsData->g_hRecSend)
	{
		RunThread_Stop(g_pDbsData->g_hRecSend, 1000, 1);
		g_pDbsData->g_hRecSend = NULL;
	}
	
	if (g_pDbsData->g_hPort != NULL)
	{
		g_pDbsData->g_hPort = NULL;
	}
	//Report serial port handle
    if (g_pDbsData->g_hSeriPort)
	{
		if (g_pDbsData->g_hSubSeriPort)
		{
			iResult = CommClose(g_pDbsData->g_hSubSeriPort);
#ifdef _DEBUG_MAINTN_
			if (iResult != ERR_COMM_OK)
			{
				TRACEX("Can't close sub-serial port correctly.\n");
			}
#endif
			g_pDbsData->g_hSubSeriPort = NULL;
		}

		iResult = CommClose(g_pDbsData->g_hSeriPort);
#ifdef _DEBUG_MAINTN_
		if (iResult != ERR_COMM_OK)
		{
			TRACEX("Can't close father-serial port correctly.\n");
		}
#endif
		g_pDbsData->g_hSeriPort = NULL;
	}
	  
	//Network  port handle
	if (g_pDbsData->g_hSubEthPort)
	{
		iResult = CommClose(g_pDbsData->g_hSubEthPort);
#ifdef 		_DEBUG_MAINTN_
		if (iResult != ERR_COMM_OK)
		{
			TRACEX("Can't close sub-eth port correctly.\n");
		}
#endif
		g_pDbsData->g_hSubEthPort = NULL;
	}

	if (g_pDbsData->g_hEthPort)
	{
		iResult = CommClose(g_pDbsData->g_hEthPort);

#ifdef 		_DEBUG_MAINTN_
		if (iResult != ERR_COMM_OK)
		{
			TRACEX("Can't close father-eth port correctly.\n");
		}
#endif
		g_pDbsData->g_hEthPort = NULL;		
	}


	//Samplering port1 handle
    if (g_pDbsData->g_hSamplerPort1)
	{
		iResult = CommClose(g_pDbsData->g_hSamplerPort1);
#ifdef 		_DEBUG_MAINTN_
		if (iResult != ERR_COMM_OK)
		{
			TRACEX("Can't close SamplerPort1 correctly.\n");
		}
#endif
		g_pDbsData->g_hSamplerPort1 = NULL;
	}
	  
	//Samplering port2 handle
	if (g_pDbsData->g_hSamplerPort2)
	{
		iResult = CommClose(g_pDbsData->g_hSamplerPort2);

#ifdef 		_DEBUG_MAINTN_
		if (iResult != ERR_COMM_OK)
		{
			TRACEX("Can't close SamplerPort1 correctly.\n");
		}
#endif
		g_pDbsData->g_hSamplerPort2 = NULL;
	}
	
	if (g_pDbsData->g_pPortInfo)
	{
		DELETE(g_pDbsData->g_pPortInfo);
		g_pDbsData->g_pPortInfo = NULL;
	}

	if (g_pDbsData->g_pFileInfo)
	{
		DELETE(g_pDbsData->g_pFileInfo);
		g_pDbsData->g_pFileInfo = NULL;
	}

	if (g_pDbsData)
	{
		DELETE(g_pDbsData);
		g_pDbsData = NULL;
	}

#ifdef _DEBUG_MAINTN_
	TRACEX("Clear all resource correctly.\n");
#endif

	return;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : Send_File
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  pszFileName : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-03-07 13:34
 *==========================================================================*/
static void Send_File(char * pszFileName)
{
	
	char	szTmpBuff[MAX_BUFFER_SIZE];
	char	szFrameBuffer[MAX_BUFFER_SIZE];

	FILE	*fp;
	unsigned short iBlockNo;
	int		iFrames, iFileSize, iReadSize;
	long	iOffset;
	

	if (pszFileName == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

	fp = fopen(pszFileName, "rb");

	if (fp == NULL)
	{
#ifdef _DEBUG_MAINTN_
	printf("Fail to open file %s\n",pszFileName);
#endif
		ReturnToPowerKit(FILE_NOT_EXIST, NULL);
		return;
	}

#ifdef _DEBUG_MAINTN_
	printf("Succed to open file %s\n",pszFileName);
#endif

	fseek(fp, 0l, SEEK_END);
	iFileSize = ftell(fp);

	iFrames = (iFileSize + FILECOPY_FRAME_SIZE - 1) / FILECOPY_FRAME_SIZE;

	//File size is too large.
	if (iFrames > LAST_DATA_BLOCK)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		fclose(fp);
		return;
	}

#ifdef _DEBUG_MAINTN_
	printf("iFrames is %d\n", iFrames);
#endif
	///////////////////////////////////////////////////////////////////////////
	iBlockNo = 0;

	while(iBlockNo < iFrames)
	{		
		memset(szTmpBuff, 0, (unsigned)sizeof(szTmpBuff));
		memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
		iOffset = iBlockNo * FILECOPY_FRAME_SIZE;
		
		g_pDbsData->tmLastTime = time(NULL); //Clear counter.
		iBlockNo++;
		if (iBlockNo >= iFrames)
		{
			iBlockNo = LAST_DATA_BLOCK;

#ifdef _DEBUG_MAINTN_
			printf("The last block will be sent.\n");
#endif
		}

		fseek(fp, iOffset, SEEK_SET);
		iReadSize = fread(szTmpBuff, 1,FILECOPY_FRAME_SIZE, fp);
		PackFileBlock(szFrameBuffer, szTmpBuff, iReadSize, iBlockNo);

		if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);
			return;
		}

#ifdef _DEBUG_MAINTN_
			printf("The %d block will be sent.\n", iBlockNo);
#endif
	}

	return;
}
/*==========================================================================*
 * FUNCTION : ResponseConnect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 14:00
 *==========================================================================*/
static void ResponseConnect(void)
{
	if (g_pDbsData->g_iRunningMode == DSP_RUNNING_MODE )
	{
		ReturnToPowerKit(DEBUG_PRO_RUNNING, NULL);
	}
	else
	{
		ReturnToPowerKit(MAIN_PRO_RUNNING, NULL);
	}
}

/*==========================================================================*
 * FUNCTION : EncryptUserInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszUserInfo : 
 * RETURN   : BOOL  : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-17 13:08
 *==========================================================================*/
static void EncryptUserInfo(char *pszUserInfo, char *szEncrypt)
{
	int i, iLen;
    //char szEncrypt[16];
    unsigned char cc, sc;

    iLen = strlen(pszUserInfo);
	
	//memset((void *)szEncrypt, 0, (unsigned)sizeof(szEncrypt));
    for(i = 0; i < iLen; i++)
    {
		sc = (0x1a + i%3);
		cc = *(pszUserInfo + i);
		cc = cc^sc;

		*(szEncrypt + i) = cc;				
	}

    return;
}
/*==========================================================================*
 * FUNCTION : RightsVerify
 * PURPOSE  : According to user information from powerkit, 
 *			  check this user exist or not,
 *			  at the same time get its right level.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void *pUserInfo : It includes username and password
 *			  int	*piRightLevel  
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 14:00
 *==========================================================================*/
static void RightsVerify(IN void * pUserInfo, OUT int	*piRightLevel)
{
	int iResult;
	USER_INFO_STRU	*pUser;
	char pszUserName[16];
	char pszPasswd[16];

	*piRightLevel = 0;
	//New code  FindUserInfo
	USER_INFO_STRU stUserInfo, stUserInfoFrom;
	pUser = (USER_INFO_STRU *)pUserInfo;

	memset(&stUserInfoFrom, 0, (unsigned)sizeof(stUserInfoFrom));
	memset((void *)pszUserName, 0, (unsigned)sizeof(pszUserName));
	EncryptUserInfo(pUser->szUserName, pszUserName);

	memmove((void *)(stUserInfoFrom.szUserName), (void *)pszUserName, 
		(unsigned)strlen(pszUserName));

	memset((void *)pszPasswd, 0, (unsigned)sizeof(pszPasswd));
	EncryptUserInfo(pUser->szPassword, pszPasswd);

	memmove((void *)(stUserInfoFrom.szPassword), (void *)pszPasswd, 
		(unsigned)strlen(pszPasswd));

	pUser = &stUserInfoFrom;

	iResult = FindUserInfo(pUser->szUserName, &stUserInfo);

	if(iResult == ERR_SEC_USER_NOT_EXISTED)
	{
		ReturnToPowerKit(USER_NOT_EXIST, NULL);
	}
	else if (iResult == ERR_SEC_OK)		//Find user of same name
	{
		if (strcmp(pUser->szPassword, stUserInfo.szPassword) == 0) //Succeed
		{
			ReturnToPowerKit(IS_OK, NULL);

			//Open the right to this user;
			g_pDbsData->g_bValidUser = TRUE;			
			
			if (stUserInfo.byLevel >= ENGINEER_LEVEL)
			{
#ifdef _DEBUG_MAINTN_
				printf("You are a valid user.\n");
#endif
				*piRightLevel = stUserInfo.byLevel;
			}
			else
			{
#ifdef _DEBUG_MAINTN_
				printf("You are not a super user.\n");
#endif
			}
		}
		else				//Password is wrong
		{
			ReturnToPowerKit(PASSWORD_ERROR, NULL);
		}
	}
	else
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
	}

	return;
}


/*==========================================================================*
 * FUNCTION : Switch_Run_Status
 * PURPOSE  : This function will be used in debug service interface of main
 *			  program.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-09 15:13
 *==========================================================================*/
static void Switch_Run_Status(void)
{
	//1.Post message to thread manager to stop every thread of main program
	//2.Clear every port
	//3.Start Debug_Maintenance program
	//4.Exit main program completely
	//5.Debug Service Program must be running.
	ReturnToPowerKit(IS_OK, NULL);

	g_pDbsData->g_bMainToDsp = TRUE;
	//raise(SIGUSR1);
	return;
}

/*==========================================================================*
 * FUNCTION : Get_Acu_Addr
 * PURPOSE  : Read ACU address from configuration file
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 14:01
 *==========================================================================*/
static void Get_Acu_Addr(void)
{
	BYTE   byAddr;
    char   szAddr[8];
	BOOL	bReadResult;

	bReadResult = ReadCfgInfo(szAddr, ACU_SITEINFO_FIELD, 
		SITE_INFO_LINES, SITE_ID_COL);

	if (!bReadResult)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}
	
	byAddr = atoi(szAddr);
	
#ifdef _DEBUG_MAINTN_
	printf("byAddr is %d\n", byAddr);
#endif //_DEBUG

	//Directly Read address info from CFG file's Address item
	//It needs to modify

	if ((byAddr < 255) && (byAddr > 0))    //Succeed
	{
		ReturnToPowerKit(RETURN_ADDR, &byAddr);
	}
	else	//Read error,read again
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
	}

	return;
}

/*==========================================================================*
 * FUNCTION : Modify_Acu_Addr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE  byAddr : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 14:22
 *==========================================================================*/
static void Modify_Acu_Addr(BYTE  byAddr)
{
	char szAddr[6];

    sprintf(szAddr, "%d", byAddr);
#ifdef _DEBUG_MAINTN_
	  printf("szAddr is %s byAddr is %d\n", szAddr, byAddr);
#endif //_DEBUG

   if(SetCfgInfo(szAddr, ACU_SITEINFO_FIELD, SITE_INFO_LINES, SITE_ID_COL))
   {
	  ReturnToPowerKit(IS_OK, NULL);   
	  
#ifdef _DEBUG_MAINTN_
	  printf("Succeed to set acuaddr is %s\n", szAddr);
#endif //_DEBUG

   }
   else	//Fail to set
   {
		ReturnToPowerKit(FAIL_OPERATE, NULL);
   }
   return;
}

/*==========================================================================*
 * FUNCTION : Get_AcuIP_Addr
 * PURPOSE  : Get acu current ip address
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 14:22
 *==========================================================================*/
static void Get_AcuIP_Addr(void)
{
    int     iBytesWritten;	
	int	    iDataLen;
	DWORD	dwCRC32;

	char szIPFrame[128];
    int	 iLength;
	DATA_FRAME stDataFrame;
	int	  iBufLen, iVarID, iError, iTimeOut; 

	ACU_NET_INFO stACUNetInfo;

	iTimeOut = 100;
	iError = ERR_DXI_OK;
	iVarID = NET_INFO_ALL;
	iBufLen = sizeof(ACU_NET_INFO);

	iError += DxiGetData(VAR_ACU_NET_INFO,
			iVarID,			
			0,		
			&iBufLen,			
			&stACUNetInfo,			
			iTimeOut);
	
	if (iError != ERR_DBS_OK)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}	

	//If ip address is right
	//Succeed
	stDataFrame.bySOH = 0x7e;
	stDataFrame.bySEQ = 0x01;
	stDataFrame.byACUAddr = 0;
	stDataFrame.byCID1 = 0xD5;
	stDataFrame.byCID2 = 0x00;			//Succeed to Get IP address
	stDataFrame.byFlag = 0;
	stDataFrame.iDataLength = 3 * sizeof(unsigned long);

#ifdef _DEBUG_MAINTN_
	printf("stDataFrame.iDataLength is %d\n", stDataFrame.iDataLength);
#endif

	iDataLen = sizeof(DATA_FRAME) - CRC32_BYTES - FRAME_END;
	memmove((void*)szIPFrame, (void *)&stDataFrame, (unsigned)iDataLen);
	
	memmove((void*)(szIPFrame + iDataLen), &(stACUNetInfo.ulIp), 
		sizeof(unsigned long));
	iDataLen = iDataLen + sizeof(unsigned long);
	memmove((void*)(szIPFrame + iDataLen), &(stACUNetInfo.ulMask), 
		sizeof(unsigned long));
	iDataLen = iDataLen + sizeof(unsigned long);
	memmove((void*)(szIPFrame + iDataLen), &(stACUNetInfo.ulGateway), 
		sizeof(unsigned long));

	iLength = iDataLen - FRAME_HEAD + stDataFrame.iDataLength;

	dwCRC32 = RunCRC32((const unsigned char *)szIPFrame 
		+ FRAME_HEAD, 
		(unsigned long)iLength, 
		(unsigned long)CRC32_DEFAULT);

	iDataLen = iLength + FRAME_HEAD;
	*((DWORD *)(szIPFrame + iDataLen)) = dwCRC32;
	iDataLen = iDataLen + CRC32_BYTES;
	*((BYTE *)(szIPFrame + iDataLen)) = 0x0D;	
	iDataLen = iDataLen + FRAME_END;

	iBytesWritten = CommWrite (g_pDbsData->g_hPort, 
						szIPFrame, 
						iDataLen);		
	
}

/*==========================================================================*
 * FUNCTION : Modify_AcuIP_Addr
 * PURPOSE  : According to szIP from PowerKit, check items to modify, and then
 *			  modify relevant items.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  szIP : format:IP-MASK-GATEWAY="ULONG-ULONG-ULONG"
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 14:23
 *==========================================================================*/
static void Modify_AcuIP_Addr(char * szIP)
{
	ULONG  ulIPAddr, ulMask, ulGateWay;
	ACU_NET_INFO stACUNetInfo;
	int	   iVarID, iBufLen, iError, iTimeOut;

	//Return to powerkit at first
	ReturnToPowerKit(IS_OK, NULL);

	/////////////////////////////////////////////////////////////
	ulIPAddr	=	*((ULONG *)(szIP));
	ulMask		=	*((ULONG *)(szIP + 4));
	ulGateWay	=	*((ULONG *)(szIP + 8));

		
	/////////////////////////////////////////////////////////////
	iVarID   = NET_INFO_ALL;
	iBufLen  = sizeof(ACU_NET_INFO);
	iTimeOut = 100;

	stACUNetInfo.ulIp = ulIPAddr;
	stACUNetInfo.ulMask = ulMask;;
	stACUNetInfo.ulGateway = ulGateWay;

	if (g_pDbsData->g_iRunningMode == DSI_RUNNING_MODE)
	{
		iError = DxiSetData(VAR_ACU_NET_INFO,
				iVarID,			
				0,		
				iBufLen,			
				&stACUNetInfo,			
				iTimeOut);
	}
	else
	{
		iError = SetNetworkInfo(0,
				1, 
				ulIPAddr, 
				1, 
				ulMask, 
				1, 
				ulGateWay,
				0, 
				0);
	}

#ifdef _DEBUG_MAINTN_
	printf("Test SetACUNetInfo() success\r\n");
#endif

/////////////////////////////////////////////////////////
	if (iError == ERR_DBS_OK)
	{
		//Network connection mothod
		if (g_pDbsData->g_hSubEthPort != NULL)
		{
#ifdef _DEBUG_MAINTN_
			printf("Current connection is network method.\n");
#endif
			EndCurrConnect();
		}
		else
		{
#ifdef _DEBUG_MAINTN_
			printf("Current connection is Serial method.\n");
#endif
		}
	}
	else
	{
		AppLogOut("DBS_MOD_IP", APP_LOG_WARNING, "Fail to set ip address.");
#ifdef _DEBUG_MAINTN_
		printf("Fail to set ip address.\n");
#endif
	}

	return;
}

/*==========================================================================*
 * FUNCTION : GetSubStringofLine
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *pszStrLine  : 
 *            int  iSubFieldCol: 
 * RETURN   : char * : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-22 10:46
 *==========================================================================*/
static char * GetSubStringofLine(char * pszStrLine, int iSubFieldCol)
{
	int iStrLen, iColCounters;
	char * p;
	char * pszDataStr;
	char * pszDesn;
	BOOL bFindData, bHaveTab;

	bFindData = FALSE;
	bHaveTab  = FALSE;
	iColCounters = 1;      //From first position start
	iStrLen = strlen(pszStrLine);
	p = pszStrLine;

	//Find the destination string position
	while(iColCounters < iSubFieldCol)
	{		
		//Find the iColCounters position
		if ((IS_WHITE_TAB(*p)) && !bHaveTab)	
		{	
			//find the start position
			bHaveTab = TRUE;
			iColCounters++;	
		}
		
		//find data
		if (!IS_WHITE_SPACE(*p))
		{
			bHaveTab = FALSE;
		}	
		p++;
		iStrLen--;

		if (iStrLen < 0)
		{
			return NULL;
		}
	} //The first while

	// Secondly while find the string iColCounters == iSubFieldCol 
	while(*p)		
	{		
		if ((!bFindData))		
		{		
			if (!(IS_WHITE_SPACE(*p)))		
			{		
				//find the start position		
				pszDataStr = p;		
				bFindData = TRUE;		
			}		
		}		
		else		
		{		
			if ((IS_WHITE_TAB(*p)))		
			{		
				//find the start position
				*p = 0;
				break;		
			}		
		}
		
		p++;
		iStrLen--;

		if (iStrLen < 0)
		{
			return NULL;
		}
	}

	// Thirdly delete all white space
	pszDesn = DBS_RemoveWhiteSpace(pszDataStr);

	return pszDesn;
}


/*==========================================================================*
 * FUNCTION : *GetValidData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  pszCfgContent : 
 *            char *  szFatherField : 
 *			  int iSubFieldLines:
*			  int iSubFieldCol
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-19 11:08
 *==========================================================================*/
static char *GetValidData(char * pszCfgContent, char * szFatherField, 
				   int iSubFieldLine, int iSubFieldCol)
{
	char * pszStrt_r;
	char * pszCurrInfo;
	char * pszInfoLine;
	char * pszDataStr;
	char * pszStep = "\n";
	int  iLines;

	pszDataStr = NULL;
	iLines = 1;
	pszCurrInfo = strstr(pszCfgContent, szFatherField);
	
	pszInfoLine = strtok_r(pszCurrInfo, pszStep, &pszStrt_r); 			
	//Get infomation line
	while(iLines < iSubFieldLine)
	{
		pszInfoLine = strtok_r(NULL, pszStep, &pszStrt_r);
		if (pszInfoLine == NULL)
		{
			return NULL;
		}
		iLines++;
	}

	//Get destination info from info line
	pszDataStr = GetSubStringofLine(pszInfoLine, iSubFieldCol);
	if (pszDataStr != NULL)
	{
		return pszDataStr;
	}
	else
	{
		return NULL;
	}

}


/*==========================================================================*
 * FUNCTION : ReadCfgInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char * pszBuff : 
 *			  char * pszInfoName:
 *			  int iSubFieldLine:
 *			  int iSubFieldCol
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-17 19:14
 *==========================================================================*/
static BOOL ReadCfgInfo(char * pszBuff, char * pszInfoName, 
				 int iSubFieldLine, int iSubFieldCol)
{
	int  iReadBytes;
	long iFileLen; 
	char *pszCurrData;
	char *pszCfgInfo;

	FILE * fp;

	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(ACU_CFG_FILE, szCfgFileName, MAX_FILE_PATH);

	fp = fopen(szCfgFileName,"r");

	if (fp == NULL)
	{
		AppLogOut("DBS_READ_CFG", APP_LOG_ERROR, "Fails on opening cfg file, "
			"errcode:0x%08x\n", ERR_DBS_CFG_OPEN_FAIL);
		return FALSE;
	}

	fseek(fp, 0l, SEEK_END);
	iFileLen = ftell(fp);
	fseek(fp, 0l, SEEK_SET);
	pszCfgInfo = NEW(char, iFileLen);

	iReadBytes = fread(pszCfgInfo, sizeof(char), (unsigned)iFileLen, fp);
	
	pszCurrData = GetValidData(pszCfgInfo, pszInfoName, iSubFieldLine, iSubFieldCol);

#ifdef _DEBUG_MAINTN_
	printf("Current string is %s, len is %d\n", pszCurrData, strlen(pszCurrData));
#endif

	if (pszCurrData == NULL)
	{
		DELETE(pszCfgInfo);
		AppLogOut("DBS_READ_CFG", APP_LOG_ERROR, 
			"Fail to read cfg information.");
		return FALSE;
	}

	memmove(pszBuff, pszCurrData, strlen(pszCurrData));
	*(pszBuff + strlen(pszCurrData)) = 0; 

	DELETE(pszCfgInfo);

    return TRUE;
}
/*==========================================================================*
 * FUNCTION : Get_Comm_Setting
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :	void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 15:39
 *==========================================================================*/
static void Get_Comm_Setting(void)
{
	char	szSetting[32];
	char	szSetFrame[64];
	DATA_FRAME stDataFrame;
	int		iBytesWritten;
	int		iLength;
	DWORD	dwCRC32;
	BOOL	bReadResult;

	//Directly Read setting info from CFG file's Address item
	//It need to modify
	//memset(szSetting, 0, (unsigned)sizeof(szSetting));
	bReadResult = ReadCfgInfo(szSetting, COMM_SETTING_FIELD, 
				 REPRT_SERIAL_PORT_LINES, PORT_SETTING_COL);

	if (!bReadResult)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

	//If szSetting is right
	stDataFrame.bySOH		= 0x7e;
	stDataFrame.bySEQ		= 0x01;
	stDataFrame.byACUAddr	= 0x0;
	stDataFrame.byCID1		= 0xD5;
	stDataFrame.byCID2		= 0x00;	//Succeed to Get setting
	stDataFrame.byFlag		= 0x0;
	stDataFrame.iDataLength = strlen(szSetting);

#ifdef _DEBUG_MAINTN_
	printf("szSetting is %s , lengh is %d\n", szSetting, strlen(szSetting));
#endif

	iLength = sizeof(DATA_FRAME) - CRC32_BYTES - FRAME_END;
	memmove((void*)szSetFrame, (void *)&stDataFrame, (unsigned)iLength);

	memmove((void*)(szSetFrame + iLength), szSetting, strlen(szSetting));

	iLength = iLength + stDataFrame.iDataLength;

	iLength = iLength - FRAME_HEAD;

	//Calculate check number
	dwCRC32 = RunCRC32((const unsigned char *)(szSetFrame + FRAME_HEAD),			 
			(unsigned long)iLength, 
			(unsigned long)CRC32_DEFAULT);

	
	iLength = iLength + FRAME_HEAD;
	*((DWORD *)(szSetFrame + iLength)) = dwCRC32;
	iLength = iLength + CRC32_BYTES;
	*((BYTE *)(szSetFrame + iLength)) = 0x0D;	
	iLength = iLength + FRAME_END;

	iBytesWritten = CommWrite (g_pDbsData->g_hPort, 
						szSetFrame, 
						iLength);	
	
	return;
}

/*==========================================================================*
 * FUNCTION : FindStringPosofLine
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *pszStrLine  : 
 *            int  iSubFieldCol: 
 *			  long iCurrPos:
*			  int *piStrLen
 * RETURN   : char * : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-22 10:46
 *==========================================================================*/
static long FindStringPosofLine(char *pszStrLine, int iSubFieldCol, 
						 long iCurrPos, int *piStrLen)
{
	long iPos;
	int iColCounters, iDataLen;
	char *p;
	BOOL bFindData, bHaveTab;

	bFindData = FALSE;
	bHaveTab  = FALSE;
	iPos = iCurrPos;
	iColCounters = 1;      //From first position start
	iDataLen = 0;

	p = pszStrLine;

	//1.Find the destination string starting position
	while((iColCounters < iSubFieldCol) && ((*p) != CR))
	{		
		//Find the iColCounters position
		if ((IS_WHITE_TAB(*p)) && !bHaveTab)	
		{	
			//find the start position
			bHaveTab = TRUE;
			iColCounters++;	
		}
		
		//find data
		if (!IS_WHITE_SPACE(*p))
		{
			bHaveTab = FALSE;
		}	
		p++;
		iPos++;
	
	} //The first while

	//Secondly find the string length  
	while(*p)		
	{		
		if ((!bFindData))		
		{	
			if (IS_WHITE_SPACE(*p))
			{
				iPos++;
			}

			if (!(IS_WHITE_SPACE(*p)) && !(END_OF_LINE(*p)))		
			{		
				//find the start position		
				bFindData = TRUE;
				iDataLen = 1;
			}		
		}		
		else		
		{		
			if ((IS_WHITE_TAB(*p)) || (END_OF_LINE(*p)))		
			{		
				//find the start position
				break;		
			}
			iDataLen++;
		}		
		p++;
		//iPos++;	
	}
	
	*piStrLen = iDataLen;
	return iPos;
}

/*==========================================================================*
 * FUNCTION : FindModifyPos
 * PURPOSE  : Find the position to modify info
 * CALLS    : 
 * CALLED BY: SetCfgInfo
 * ARGUMENTS: char   *pszCfgInfo  : 
 *            int    iFileLen     : 
 *            char   *pszFatherField : 
 *            int    *piOldInfoLen:
 *			  int    iSubFieldLine:
 *			  int	 iSubFieldCol:
 * RETURN   : long : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-19 14:35
 *==========================================================================*/
static long FindModifyPos(char *pszCfgInfo, int iFileLen, 
				   char *pszFatherField, int *piOldInfoLen,
				   int	iSubFieldLine, int iSubFieldCol)
{
	int iColCounters, iLineCounters;
	int i, iDataLen;
	long iPos, iTargetPos, iStartPos;
	char *p;
	char szTmpField[64];

	BOOL bStartFlag, bFindField, bSubField;

	bStartFlag	= FALSE;
	bFindField	= FALSE;
	bSubField   = FALSE;
	p = pszCfgInfo;
	iPos = 0;
	*piOldInfoLen = 0;
	iDataLen = 0;

	//From the first lint to start
	iLineCounters = 1;
	iColCounters  = 1;

	for (i = 0; i < iFileLen; i++)
	{
		//1.Search field position
		if (!bFindField)
		{
			if (*p == LEFT_PREFIX)
			{
				iStartPos = i;
				bStartFlag = TRUE;
			}

			if (bStartFlag && (*p == RIGHT_SUFFIX))
			{
				memset(szTmpField, 0, sizeof(szTmpField));
				memmove(szTmpField, pszCfgInfo + iStartPos, 
					(unsigned)(i - iStartPos + 1));

				if (strcmp(szTmpField, pszFatherField) == 0)
				{
					//Find the father field position
					bFindField = TRUE;
				}
				else
				{
					bStartFlag = FALSE;
#ifdef _DEBUG_MAINTN_
					//printf("szTmpField is %s\n", szTmpField);
#endif
				}
			}
		}
		else //2.Search current line position in current field
		{
			if (iLineCounters == iSubFieldLine)
			{
				p++;
				iPos++;
				break;				
			}

			if ((*p) == CR)
			{
				iLineCounters++;				
			}							
		}		

		p++;
		iPos++;
	}

	//3.Search the column starting position in current line
	 iTargetPos = FindStringPosofLine(p, iSubFieldCol, iPos, piOldInfoLen);

	return iTargetPos;
}
/*==========================================================================*
 * FUNCTION : SetCfgInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *szBuff : 
 *			  char *pszInfoName:
 *			  int  iSubFieldLine:
 *			  int  iSubFieldCol:
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-17 19:14
 *==========================================================================*/
static BOOL SetCfgInfo(char * pszBuff, char * pszInfoName,
				int iSubFieldLine, int iSubFieldCol)
{
	int  iReadBytes, iWriteBytes;
	int  iOldInfoLen, iNewInfoLen, iOldInfoLenbk;
	long iFileLen, iSetPos; 
	char *pszCfgInfo;
	char *pszSpace = " ";

	FILE * fp;
	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(ACU_CFG_FILE, szCfgFileName, MAX_FILE_PATH);

	fp = fopen(szCfgFileName,"r+");

	if (fp == NULL)
	{
		return FALSE;
	}

	fseek(fp, 0l, SEEK_END);
	iFileLen = ftell(fp);
	fseek(fp, 0l, SEEK_SET);
	pszCfgInfo = NEW(char, iFileLen);

	if (pszCfgInfo == NULL)
	{
		fclose(fp);
		return FALSE;
	}

	iReadBytes = fread(pszCfgInfo, sizeof(char), (unsigned)iFileLen, fp);
	
	iOldInfoLen = 0;

	iSetPos = FindModifyPos(pszCfgInfo, iFileLen, pszInfoName, &iOldInfoLen, 
		iSubFieldLine, iSubFieldCol);

	iNewInfoLen = strlen(pszBuff);
	iOldInfoLenbk = iOldInfoLen;
	//Clear old invalid ascii
	fseek(fp, iSetPos, SEEK_SET);
	while(iOldInfoLen > 0)
	{
		iWriteBytes = fwrite((void *)pszSpace, 1, (unsigned)1, fp);
		iOldInfoLen--;
	}

	if ((iNewInfoLen > iOldInfoLenbk) && 
		(iSubFieldCol == PORT_SETTING_COL))
	{	
		fclose(fp);
		fp = fopen(szCfgFileName,"w+");

		if (fp == NULL)
		{
			return FALSE;
		}

		fseek(fp, 0, SEEK_SET);
		iWriteBytes = fwrite((void *)pszCfgInfo, 1, (unsigned)iSetPos, fp);
#ifdef _DEBUG_MAINTN_
		printf("The first ibytes is %d\n", iWriteBytes);
#endif
		iWriteBytes = fwrite((void *)pszSpace, 1, 
			(unsigned)(iNewInfoLen - iOldInfoLenbk), fp);
#ifdef _DEBUG_MAINTN_
		printf("The second ibytes is %d\n", iWriteBytes);
#endif		
		iWriteBytes = fwrite((void *)(pszCfgInfo + iSetPos), 1, 
			(unsigned)(iFileLen - iSetPos), fp);
#ifdef _DEBUG_MAINTN_
		printf("The third ibytes is %d\n", iWriteBytes);
#endif			
	}

#ifdef _DEBUG_MAINTN_
	printf("pszBuff is %s, iNewInfoLen is %d\n", (char *)pszBuff, iNewInfoLen);
#endif

	fseek(fp, iSetPos, SEEK_SET);
	iWriteBytes = fwrite((void *)pszBuff, 1, (unsigned)iNewInfoLen, fp);


	fclose(fp);
	DELETE(pszCfgInfo);
    return TRUE;
}

/*==========================================================================*
 * FUNCTION : Set_Comm_Setting
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  szSetting : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 15:40
 *==========================================================================*/
static void Set_Comm_Setting(char * szSetting)
{
	
	BOOL bChangeToModem;
	BOOL bResult;
	char szIDBuff[16];
	char szTimeBuff[16];
	bChangeToModem = FALSE;
	char *p;
	DBS_PORT_INFO *pPortInfo;

	p = szSetting;
	if (p == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

#ifdef _DEBUG_MAINTN_
	printf("Current setting is %s\n", szSetting);
#endif

	while(*p)
	{
		if ((*p) == COLON_SPIT)
		{

#ifdef _DEBUG_MAINTN_
			printf("Current setting is Modem method.\n");
#endif
			bChangeToModem = TRUE;
			break;
		}
		p++;
	}
	
	if (g_pDbsData->g_pPortInfo != NULL)
	{
		pPortInfo = g_pDbsData->g_pPortInfo + 3; //Go to back port
#ifdef _DEBUG_MAINTN_
			printf("DBS_PORT_INFO is right.\n");
#endif
	}
	else
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

	if (pPortInfo == NULL)
	{
#ifdef _DEBUG_MAINTN_
		printf("DBS_PORT_INFO is wrong.\n");
#endif
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

	memset((void *)szIDBuff, 0, (unsigned)16);
	memset((void *)szTimeBuff, 0, (unsigned)16);
	if (bChangeToModem)
	{
		if (pPortInfo->iStdPortTypeID == SERIAL_TYPE_PORT)
		{
			//1.Change the Port Type ID in memory 
			pPortInfo->iStdPortTypeID = DIAL_TYPE_PORT;
			//2.Change Port Type ID to file  MonitoringSolution.cfg
			sprintf(szIDBuff, "%d", DIAL_TYPE_PORT);
			bResult = SetCfgInfo(szIDBuff, COMM_SETTING_FIELD,
				REPRT_SERIAL_PORT_LINES, PORT_TYPE_COL);
			//3.Change the Timeout(ms,default:60000)
			pPortInfo->iTimeout =DIAL_PORT_TIMEOUT;
			//4.Change timeout to file  MonitoringSolution.cfg
			sprintf(szTimeBuff, "%d", DIAL_PORT_TIMEOUT);
			bResult = SetCfgInfo(szTimeBuff, COMM_SETTING_FIELD,
				REPRT_SERIAL_PORT_LINES, PORT_TIMEOUT_COL);
		}
	}
	else
	{
		if (pPortInfo->iStdPortTypeID == DIAL_TYPE_PORT)
		{
			//1.Change the Port Type ID in memory 
			pPortInfo->iStdPortTypeID = SERIAL_TYPE_PORT;
			//2.Change Port Type ID to file  MonitoringSolution.cfg
			sprintf(szIDBuff, "%d", SERIAL_TYPE_PORT);
			bResult = SetCfgInfo(szIDBuff, COMM_SETTING_FIELD,
				REPRT_SERIAL_PORT_LINES, PORT_TYPE_COL);
			//3.Change the Timeout(ms,default:60000)
			pPortInfo->iTimeout = SERIAL_PORT_TIMEOUT;
			//4.Change timeout to file  MonitoringSolution.cfg
			sprintf(szTimeBuff, "%d", SERIAL_PORT_TIMEOUT);
			bResult = SetCfgInfo(szTimeBuff, COMM_SETTING_FIELD,
				REPRT_SERIAL_PORT_LINES, PORT_TIMEOUT_COL);
		}	
	} 

#ifdef _DEBUG_MAINTN_
		printf("Finish to set id and port type.\n");
#endif			

	//5.Change szSetting
	bResult = SetCfgInfo(szSetting, COMM_SETTING_FIELD,
			REPRT_SERIAL_PORT_LINES, PORT_SETTING_COL);
	if(bResult)
	{
		ReturnToPowerKit(IS_OK, NULL);
	}
	else	//Fail to set
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		AppLogOut("DBS_SET_COMS", APP_LOG_ERROR, 
			"Fail to set report port's communication setting.");
		return;
	}
	
	//6.Update memory
#ifdef _DEBUG_MAINTN_
		printf("Old setting is %s.\n", pPortInfo->szSetting);
#endif	
	memset ((void *)(pPortInfo->szSetting), 0, 
		(unsigned)sizeof(pPortInfo->szSetting));
	strncpyz(pPortInfo->szSetting, szSetting, (size_t)32);	

#ifdef _DEBUG_MAINTN_
		printf("New setting is %s.\n", pPortInfo->szSetting);
#endif
	return;
}

/*==========================================================================*
 * FUNCTION : *DBS_RemoveWhiteSpace
 * PURPOSE  : Delete space before and after string
 * CALLS    : 
 * CALLED BY: ReadManuInfo, ReadCfgInfo
 * ARGUMENTS: char  *pszBuf : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-17 20:19
 *==========================================================================*/
static char *DBS_RemoveWhiteSpace(char *pszBuf)
{
    char    *p = pszBuf;
    char    *pFirstNonSpace = pszBuf, *pLastWhiteSpace = NULL;
	BOOL    bFound = FALSE;  /* first non-space character found flag */

    while (*p)
    {
        if (IS_WHITE_SPACE(*p))
        {
            if(pLastWhiteSpace == NULL)
			{
				pLastWhiteSpace = p;
			}
                
        }
		else if(END_OF_LINE(*p))  //delete return and end 
		{
			*p = 0;
			break;
		}
        else 
        {
            if (!bFound)
			{
                pFirstNonSpace = p;
				bFound = TRUE;
			}

            pLastWhiteSpace = NULL;
        }

        p++;
    }

    if (pLastWhiteSpace != NULL)
	{
		*pLastWhiteSpace = 0;
	}        

    return pFirstNonSpace;
}


/*==========================================================================*
 * FUNCTION : *DBS_RemoveInValidSpace
 * PURPOSE  : Delete space before and after string
 * CALLS    : 
 * CALLED BY: ReadManuInfo, ReadCfgInfo
 * ARGUMENTS: char  *pszBuf : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-17 20:19
 *==========================================================================*/
static char *DBS_RemoveInValidSpace(char *pszBuf)
{
    char    *p = pszBuf;
    char    *pFirstNonSpace = pszBuf, *pLastWhiteSpace = NULL;
	BOOL    bFound = FALSE;  /* first non-space character found flag */

    while (*p)
    {
        if (IS_WHITE_SPACE(*p) || ((*p) == DBS_EQUAL))
        {
            if(pLastWhiteSpace == NULL)
			{
				pLastWhiteSpace = p;
			}
                
        }
		else if(END_OF_LINE(*p))  //delete return and end 
		{
			*p = 0;
			break;
		}
        else 
        {
            if (!bFound)
			{
                pFirstNonSpace = p;
				bFound = TRUE;
			}

            pLastWhiteSpace = NULL;
        }

        p++;
    }

    if (pLastWhiteSpace != NULL)
	{
		*pLastWhiteSpace = 0;
	}        

    return pFirstNonSpace;
}
/*==========================================================================*
 * FUNCTION : ProcessString
 * PURPOSE  : 
 * CALLS    : DBS_RemoveInValidSpace
 * CALLED BY: ReadManuInfo
 * ARGUMENTS: char *        token     : 
 *            const char *  szField   : 
 *            int           iFieldLen : 
 *            char *        szBuff    : 
 * RETURN   : BOOL * : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-17 20:51
 *==========================================================================*/
static BOOL ProcessString(char * token, const char * szField, 
				   int iFieldLen, char * szBuff)
{
	int iStrLen, iDataLen, iTotalLen;
	char * szStr;

	iDataLen = strlen(szField);
	token = strstr(token, szField);

	iTotalLen = strlen(token);
	if ((token == NULL) || iTotalLen < iDataLen)
	{
		return FALSE;
	}

	token = token + iDataLen;
	szStr = DBS_RemoveInValidSpace(token);

	iStrLen = strlen(szStr);	
	memmove((void *)szBuff, (void *)szStr, (unsigned)iStrLen);

	memset(szBuff + iStrLen, 0x20, (unsigned)(iFieldLen - iStrLen));

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : ProcessDate
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ReadManuInfo
 * ARGUMENTS: char *        token     : 
 *            const char *  szField   : 
 *            int           iFieldLen : 
 *            char *        szBuff    : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-18 09:28
 *==========================================================================*/
static BOOL ProcessDate(char * token, const char * szField, 
				 char * szBuff)
{
	char	*szStr;
	char	*p;
	char	szData[4];
	int		iData, iFieldLen, iPos, iSpitSize;
	int		iYearSize, iMonSize, iDaySize;
	BOOL	bFindFirst;
	int		iBufPos, iTotalLen;
	
	iFieldLen = strlen(szField);
	token = strstr(token, szField);

	iTotalLen = strlen(token);
	if ((token == NULL) || iTotalLen < iFieldLen)
	{
		return FALSE;
	}

	token = token + iFieldLen;
	szStr = DBS_RemoveInValidSpace(token);

	p = szStr;
	iPos = 0;
	iYearSize = 0;
	iMonSize = 0;
	iDaySize = 0;
	iSpitSize = 1;        //For example:'-'
	bFindFirst = FALSE;
	while(*p)    //Time Format1:2004-06-08  20040608 04-06-08
	{
		if (IS_DBS_SPITT(*p))    //'-'  '/'
		{
			if (!bFindFirst)
			{
				iYearSize = iPos;
				bFindFirst = TRUE;
				iPos = 0;
			}
			else
			{
				iMonSize = iPos - 1;
				iPos = 0;
			}
            
 		}
		iPos++;
		p++;
	}
	iDaySize = iPos;

	if (!bFindFirst)            //Format: 20040608
	{
		memmove(szData, szStr, 2);
		iData = atoi(szData);
		szBuff[0] = iData;
		iBufPos = 2;

		memmove(szData, szStr + iBufPos, 2);
		iData = atoi(szData);
		szBuff[1] = iData;
		iBufPos = iBufPos + 2;

		memmove(szData, szStr + iBufPos, 2);
		iData = atoi(szData);
		szBuff[2] = iData;
		iBufPos = iBufPos + 2;

		memmove(szData, szStr + iBufPos, 2);
		iData = atoi(szData);
		szBuff[3] = iData;

	}
	else
	{
		if ((iYearSize >= 2) &&  (iYearSize <= 4))
		{
			if (iYearSize == 2)   //This format: 04-6-8
			{
				iData = 20;
				szBuff[0] = iData;

				memmove((void *)szData, (void *)szStr, (unsigned)iYearSize);

			}
			else if(iYearSize == 4)  //This format:2004-06-08
			{
				memmove((void *)szData, (void *)szStr, (unsigned)2);
				iData = atoi(szData);
				szBuff[0] = iData;
				iBufPos = 2;

				memmove((void *)szData, (void *)(szStr + iBufPos), (unsigned)2);
			}
		
			iData = atoi(szData);		
			szBuff[1] = iData;
			iBufPos = iYearSize + iSpitSize;

			memmove((void *)szData, (void *)(szStr + iBufPos), (unsigned)iMonSize);
			iData = atoi(szData);
			szBuff[2] = iData;
			iBufPos = iBufPos + iMonSize + iSpitSize;

			memmove((void *)szData, (void *)(szStr + iBufPos), (unsigned)iDaySize);
			iData = atoi(szData);
			szBuff[3] = iData;

		}
		else			//Error
		{
			return FALSE;
		}
	}	

	return	TRUE;
}

/*==========================================================================*
 * FUNCTION : ProcessVersion
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ReadManuInfo
 * ARGUMENTS: char *        token     : 
 *            const char *  szField   : 
 *            char *        szBuff    : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-18 09:29
 *==========================================================================*/
static BOOL ProcessVersion(char * token, const char * szField, 
				    char * szBuff)
{
	char * pStrtok_r;
	char * szStr;
	char * szData;
	char * szStep = ".";
	int iDataLen, iTotalLen;

	iDataLen = strlen(szField);
	token = strstr(token, szField);
	iTotalLen = strlen(token);


	if ((token == NULL) || iTotalLen < iDataLen)
	{
		return FALSE;
	}

	token = token + iDataLen;
	szStr = DBS_RemoveInValidSpace(token);

	//szData = strt_ok(szStr, szStep);   //It's not safe in multi-threads
	szData = strtok_r(szStr, szStep, &pStrtok_r);
	szBuff[0] = atoi(szData);
	szData = strtok_r(NULL, szStep, &pStrtok_r);

	if (szData == NULL)
	{
		return FALSE;
	}

	szBuff[1] = atoi(szData);

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ProcessSWInfoFile
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-14 10:37
 *==========================================================================*/
static BOOL  ProcessSWInfoFile(char * pszCurrDir)
{
	char    szTime[32];
	FILE	*fpsw;

	//Get current working path
	fpsw = fopen(pszCurrDir, "a+");

	if (fpsw == NULL)
	{
		return FALSE;
	}

	// get current time and convert to YYYY-MM-DD hh:mm:ss.
	TimeToString(time(NULL), DBS_TIME_CHN_FMT, szTime, sizeof(szTime));
	fprintf(fpsw, "INSTALL_TIME=%s\r\n", szTime); //\r must be first
	fprintf(fpsw, "SERVICE_TIME=%s\r\n", szTime);

	fclose(fpsw);
	return TRUE;
}
/*==========================================================================*
 * FUNCTION : *ReadCurrentData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ReadManuInfo
 * ARGUMENTS: FILE    *fp      : 
 *            char *  szField  : 
 *            int     iFileLen : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-18 14:53
 *==========================================================================*/
static char *ReadCurrentData(FILE *fp, char * szField, int iFileLen)
{
	char *pszStr_r;
	char *szDesn;
	char *token;
	char *szRline = "\n";
	char szBuffer[1024];

	fseek(fp, 0l, SEEK_SET);
	fread((void *)szBuffer, sizeof(char), (unsigned)iFileLen, fp);

	token = strstr(szBuffer, szField);
	if (token == NULL)
	{
		return NULL;
	}
	szDesn = strtok_r(token, szRline, &pszStr_r);

	return szDesn;

}

/*==========================================================================*
 * FUNCTION : ReadManuInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  szBuff : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-17 19:14
 *==========================================================================*/
static BOOL ReadManuInfo(char * szBuff)
{
	int		iDataLen, iBufLen, iError;
	long	iFileLen, iFileLensw; 
	char	*szCurrData;
	char    szTime[32];
	char	szCurrDir[128];
	char	*pszDir;
	char	szBuffer[64];
	DWORD	dwGetData;

	FILE * fp;
	FILE * fpsw;


	fp = fopen(MANUFACR_INFO_FILE,"r");
	if (fp == NULL)
	{
		return FALSE;
	}

	//Get current working path
	pszDir = getcwd(szCurrDir, 128);
	strcat(szCurrDir, MANUSW_INFO_FILE); 
	fpsw = fopen(szCurrDir, "r");
	if (fpsw == NULL)
	{
		if (ProcessSWInfoFile(szCurrDir))
		{
			//Open again
			fpsw = fopen(szCurrDir, "r");
		}
	}

	 if (fpsw != NULL)
	{
		fseek(fpsw, 0l, SEEK_END);
		iFileLensw = ftell(fpsw);
	}

	fseek(fp, 0l, SEEK_END);
	iFileLen = ftell(fp);

	//1.Get PRODUCT_NAME
	szCurrData = ReadCurrentData(fp, "PRODUCT_NAME", iFileLen);
	if (szCurrData == NULL)
	{
		fclose(fp);
		return FALSE;
	}

	if (!ProcessString(szCurrData, "PRODUCT_NAME", PRODUCT_NAME_LEN, szBuff))
	{
		fclose(fp);
		return FALSE;
	}	

	iDataLen = PRODUCT_NAME_LEN;	

	//2.Get MANUFACTORY_NAME
	szCurrData = ReadCurrentData(fp, "MANUFACTORY_NAME", iFileLen);
	if (szCurrData == NULL)
	{
		fclose(fp);
		return FALSE;
	}

	if (!ProcessString(szCurrData, "MANUFACTORY_NAME", MANU_NAME_LEN, 
		(szBuff + iDataLen)))
	{
		fclose(fp);
		return FALSE;
	}

	iDataLen = iDataLen + MANU_NAME_LEN;

	//3.Get SOFTWARE_VERSION=1.0
	iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_SW_VERSION,			
			0,		
			&iBufLen,			
			&dwGetData,			
			100);

	if (iError != ERR_DXI_OK)
	{		
		*(szBuff + iDataLen) = 0;
		*(szBuff + iDataLen + 1) = 0;
	}
	else
	{
		*(szBuff + iDataLen) = dwGetData/100;
		*(szBuff + iDataLen + 1) = dwGetData%100;
	}

	iDataLen = iDataLen + VERSION_LEN;

	//4.Get HARDWARE_VERSION
	szCurrData = ReadCurrentData(fp, "HARDWARE_VERSION", iFileLen);
	if (szCurrData == NULL)
	{
		fclose(fp);
		return FALSE;
	}

	if (!ProcessVersion(szCurrData, "HARDWARE_VERSION",  
		(szBuff + iDataLen)))
	{
		fclose(fp);
		return FALSE;
	}	
	iDataLen = iDataLen + VERSION_LEN;

	//5.Get PRODUCT_SERIAL_NUMBER
	szCurrData = ReadCurrentData(fp, "PRODUCT_SERIAL_NUMBER", iFileLen);
	if (szCurrData == NULL)
	{
		fclose(fp);
		return FALSE;
	}

	if (!ProcessString(szCurrData, "PRODUCT_SERIAL_NUMBER", SERIAL_NUM_LEN, 
		(szBuff + iDataLen)))
	{
		fclose(fp);
		return FALSE;
	}

	iDataLen = iDataLen + SERIAL_NUM_LEN;

	//6.Get MANUFACTURE_TIME
	szCurrData = ReadCurrentData(fp, "MANUFACTURE_TIME", iFileLen);
	if (szCurrData == NULL)
	{
		fclose(fp);
		return FALSE;
	}
	if (!ProcessDate(szCurrData, "MANUFACTURE_TIME",  
		(szBuff + iDataLen)))
	{
		fclose(fp);
		return FALSE;
	}
	iDataLen = iDataLen + DATE_LEN;
	
	//7.Get INSTALL_TIME=20041121
	if (fpsw != NULL)
	{	
		szCurrData = ReadCurrentData(fpsw, "INSTALL_TIME", iFileLensw);		
	}

	if ((fpsw == NULL) || (szCurrData == NULL))
	{
		// get current time and convert to YYYY-MM-DD
		memset(szBuffer, 0, (unsigned)sizeof(szBuffer));
		TimeToString(time(NULL), DBS_TIME_CHN_FMT, szTime, sizeof(szTime));
		sprintf(szBuffer, "INSTALL_TIME=%s", szTime);
		szCurrData = szBuffer;
	}
	
	if(!ProcessDate(szCurrData, "INSTALL_TIME",  
			(szBuff + iDataLen)))
	{
		fclose(fp);
		return FALSE;
		
	}
	iDataLen = iDataLen + DATE_LEN;

	//8.Get SERVICE_TIME=20041121
	//The last string has been treated specially
	if (fpsw != NULL)
	{	
		szCurrData = ReadCurrentData(fpsw, "SERVICE_TIME", iFileLensw);		
	}
	
	if ((fpsw == NULL) || (szCurrData == NULL))
	{
		// get current time and convert to YYYY-MM-DD
		memset(szBuffer, 0, (unsigned)sizeof(szBuffer));
		TimeToString(time(NULL), DBS_TIME_CHN_FMT, szTime, sizeof(szTime));
		sprintf(szBuffer, "SERVICE_TIME=%s", szTime);
		szCurrData = szBuffer;
	}	

	if (!ProcessDate(szCurrData, "SERVICE_TIME",  
		(szBuff + iDataLen)))
	{
		fclose(fp);
		return FALSE;
	}	
	iDataLen = iDataLen + DATE_LEN;

	if (fpsw)
	{
		fclose(fpsw);
	}

	if (fp)
	{
		fclose(fp);
	}

    return TRUE;
}
/*==========================================================================*
 * FUNCTION : Get_Manufacr_Info
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-08 13:02
 *==========================================================================*/
static void Get_Manufacr_Info(void)
{	

	char szManuInfoFrame[128];	
	char szBuff[64];
	DATA_FRAME	stDataFrame;

	int		iBytesWritten;
	int		iLength;
	DWORD	dwCRC32;
	
    if (!ReadManuInfo(szBuff))
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}
	
	//If szSetting is right
	stDataFrame.bySOH		= 0x7e;
	stDataFrame.bySEQ		= 0x01;
	stDataFrame.byACUAddr	= 0x0;
	stDataFrame.byCID1		= 0xD5;
	stDataFrame.byCID2		= 0x00;	//Succeed to Get setting
	stDataFrame.byFlag		= 0x0;
	stDataFrame.iDataLength = MANU_INFO_ALL_LEN;

	iLength = sizeof(DATA_FRAME) - CRC32_BYTES - FRAME_END;
	memmove((void*)szManuInfoFrame, (void *)&stDataFrame, (unsigned)iLength);

	memmove((void*)(szManuInfoFrame + iLength), szBuff, MANU_INFO_ALL_LEN);

	iLength = iLength + stDataFrame.iDataLength;

	iLength = iLength - FRAME_HEAD;
	dwCRC32 = RunCRC32((const unsigned char *)(szManuInfoFrame + FRAME_HEAD),			 
			(unsigned long)iLength, 
			(unsigned long)CRC32_DEFAULT);

	
	iLength = iLength + FRAME_HEAD;
	*((DWORD *)(szManuInfoFrame + iLength)) = dwCRC32;
	iLength = iLength + CRC32_BYTES;
	*((BYTE *)(szManuInfoFrame + iLength)) = 0x0D;	
	iLength = iLength + FRAME_END;

	iBytesWritten = CommWrite (g_pDbsData->g_hPort, 
						szManuInfoFrame, 
						iLength);	
	
#ifdef _DEBUG_MAINTN_
	printf("iBytesWritten is %d\n", iBytesWritten);
#endif //_DEBUG


	return;

}

/*==========================================================================*
 * FUNCTION : FindPosByLineCol
 * PURPOSE  : Only used by SetOneManuInfo
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszFileInfo  : 
 *             int  iFileLen      : 
 *             int  *piOldInfoLen  : 
 *             int  iSubFieldLine : 
 *             int  iSubFieldCol  : 
 * RETURN   : long : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-23 10:21
 *==========================================================================*/
static long FindPosByLineCol(char *pszFileInfo, int iFileLen, int *piOldInfoLen, 
		int iSubFieldLine, int iSubFieldCol)
{
	int iColCounters, iLineCounters;
	int iDataLen;
	long iPos;
	char *p;

	BOOL bFindCol, bStartInfo;
	
	iPos			= 0;
	*piOldInfoLen	= 0;
	iDataLen		= 0;
	bFindCol		= FALSE;
	bStartInfo		= FALSE;
	p				= pszFileInfo;

	//From the first lint to start
	iLineCounters = 1;
	iColCounters  = 1;
	iSubFieldCol++;
	iSubFieldCol--;    //useless, delete warning during compiling
	
	//1.Search the line position in current file
	while(*p)
	{
		if (iLineCounters >= iSubFieldLine)
		{
			break;
		}
		
		if (*p == CR)
		{
			iLineCounters++;
		}

		iPos++;
		p++;
		iFileLen--;		
	}	

	//2.Search the column position in current line
#ifdef _DEBUG_MAINTN_
	printf("Current iLineCounters is %d\n", iLineCounters);
	printf("Current iSubFieldLine is %d\n", iSubFieldLine); 
#endif

	while(*p)
	{		
		//Find the iColCounters position
		if ((*p) == DBS_EQUAL)	
		{	
			//find the start position
			p++;
			iPos++;
            break;
		}	
		p++;
		iPos++;
	
	} //The first while

	//3.Find the string length  
	while(*p)		
	{		
		if ((!bStartInfo))		
		{		
			if (!(IS_WHITE_SPACE(*p)) && !(END_OF_LINE(*p)))		
			{		
				//find the start position		
				bStartInfo = TRUE;
				iDataLen = 1;
			}
			else
			{
				iPos++;
			}
		}		
		else		
		{		
			if ((IS_WHITE_SPACE(*p)) || (END_OF_LINE(*p)))		
			{		
				//find the start position
				break;		
			}
			iDataLen++;
		}		
		p++;
		//iPos++;	
	}
	
	*piOldInfoLen = iDataLen;

	return iPos;	
}

/*==========================================================================*
 * FUNCTION : SetOneManuInfo
 * PURPOSE  : Set information, identify different segment by space
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *szBuff :
 *			  int iDataLen :
 *			  int  iSubFieldLine:
 *			  int  iSubFieldCol:
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-17 19:14
 *==========================================================================*/
static BOOL SetOneManuInfo(char * pszBuff, int iDataLen, 
					int iSubFieldLine, int iSubFieldCol)
{
	int  iReadBytes, iWriteBytes;
	int  iOldInfoLen, iNewInfoLen;
	long iFileLen, iSetPos; 
	char *pszManuInfo;
	char *pszSpace = " ";
	char *pszDir;
	char szCurrDir[128];

	FILE * fp;

	pszDir = getcwd(szCurrDir, 128);
	strcat(szCurrDir, MANUSW_INFO_FILE); 
	fp = fopen(szCurrDir, "r+");

	if (fp == NULL)
	{
		return FALSE;
	}

	fseek(fp, 0l, SEEK_END);
	iFileLen = ftell(fp);
	fseek(fp, 0l, SEEK_SET);
	pszManuInfo = NEW(char, iFileLen);

	iReadBytes = fread(pszManuInfo, sizeof(char), (unsigned)iFileLen, fp);
	
	iOldInfoLen = 0;

	iSetPos = FindPosByLineCol(pszManuInfo, iFileLen, &iOldInfoLen, 
		iSubFieldLine, iSubFieldCol);		

	iNewInfoLen = iDataLen;

	fseek(fp, iSetPos, SEEK_SET);
	while(iOldInfoLen > 0)
	{
		iWriteBytes = fwrite((void *)pszSpace, 1, (unsigned)1, fp);
		iOldInfoLen--;
	}

#ifdef _DEBUG_MAINTN_
		printf("Set iWriteBytes space is %d\n", iOldInfoLen);
#endif

	fseek(fp, iSetPos, SEEK_SET);
	iWriteBytes = fwrite((void *)pszBuff, 1, (unsigned)iNewInfoLen, fp);

#ifdef _DEBUG_MAINTN_
	printf("Acutally iWriteBytes space is %d\n", iWriteBytes);
#endif
	
	fclose(fp);
	DELETE(pszManuInfo);

    return TRUE;
}

//////////////////////////////////////////////////////////////////////////////

/*==========================================================================*
 * FUNCTION : Set_Manufacr_Info
 * PURPOSE  : According to manufactory information from powerkit, update 
 *			  information in ACU
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  pManInfo : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-08 13:02
 *==========================================================================*/
static void Set_Manufacr_Info(char * pszManInfo)	
{
	char szManInfo[16];
	int	 iTimeNumber;
	int	 iCmdNo;
	
	//Directly write manufactory info to MANUFACR_INFO_FILE
	//It need to modify
	iCmdNo = *pszManInfo;

	memset(szManInfo, 0, sizeof(szManInfo));
	iTimeNumber = 	(*(pszManInfo + 1)) * 1000000 + (*(pszManInfo + 2)) * 10000 
		+ (*(pszManInfo + 3)) * 100 + *(pszManInfo + 4);
	sprintf(szManInfo, "%d", iTimeNumber);
#ifdef _DEBUG_MAINTN_
	printf("Set ManInfo is %d\n", iTimeNumber);
#endif

	if (iCmdNo == INSTALL_TIME_CMD)
	{
		if (!SetOneManuInfo(szManInfo, (int)strlen(szManInfo), 
					INSTALL_TIME_LINE, INSTALL_TIME_COL))
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);
		}
		else
		{
			ReturnToPowerKit(IS_OK, NULL);
		}

	}
	else if (iCmdNo == SERVICE_TIME_CMD)
	{
		if (!SetOneManuInfo(szManInfo, (int)strlen(szManInfo), 
					SERVICE_TIME_LINE, SERVICE_TIME_COL))
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);
		}
		else
		{
			ReturnToPowerKit(IS_OK, NULL);
		}
	}
	else
	{
		ReturnToPowerKit(ERROR_CMD_FORMAT, NULL);
	}

	return;
}

/*==========================================================================*
 * FUNCTION : ProcessFileName
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  szLongFileName :  //Include directory
 * RETURN   : char * : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-08 17:24
 *==========================================================================*/
char * ProcessFileName(char * szLongFileName)
{
	char * szShortFileName;
	char * szFilePath;
	char * p;
	int	 iPos, iNamePos;
	FILE_INFO_REC * pFileInfo;
	FILE_INFO_REC * pCurrFileInfo;

	szFilePath = szLongFileName;
	p = szLongFileName;
	
	iPos = 0;
	iNamePos = 0;
#ifdef _DEBUG_MAINTN_
	printf("szLongFileName is %s\n", szLongFileName);
#endif

	while(*p)
	{
		if ((*p) == '/')
		{
			szShortFileName = p + 1;
			iNamePos = iPos;
#ifdef _DEBUG_MAINTN_
	printf("szShortFileName is %s\n", szShortFileName);
#endif
		}
		iPos++;
		p++;
	}

	*(szFilePath + iNamePos) = 0;

#ifdef _DEBUG_MAINTN_
	printf("szFilePath is %s\n", szFilePath);
#endif

	if (g_pDbsData->g_pFileInfo == NULL)
	{
		g_pDbsData->g_pFileInfo = NEW(FILE_INFO_REC, 1);

		if (g_pDbsData->g_pFileInfo == NULL)
		{
			AppLogOut("DBS_PRO_FILENAM", APP_LOG_ERROR, 
				"No enough memory to store file name information.");
			return NULL;
		}
		g_pDbsData->g_iFileNum++;
	}
	else
	{
        g_pDbsData->g_iFileNum++;		 
		pFileInfo = RENEW(FILE_INFO_REC, g_pDbsData->g_pFileInfo, 
			g_pDbsData->g_iFileNum);

		if (pFileInfo == NULL)
		{
			AppLogOut("DBS_PRO_FILENAM", APP_LOG_ERROR, 
				"No enough memory to store file name information.");
			return NULL;
		}

		g_pDbsData->g_pFileInfo = pFileInfo;
	}

	pCurrFileInfo = g_pDbsData->g_pFileInfo + (g_pDbsData->g_iFileNum - 1);
	
	memset((void *)(pCurrFileInfo->szFileName), 0, 
		(unsigned)sizeof(pCurrFileInfo->szFileName));

	memset((void *)(pCurrFileInfo->szFilePath), 0, 
		(unsigned)sizeof(pCurrFileInfo->szFilePath));

	sprintf(pCurrFileInfo->szFileName, "/var/%s", szShortFileName);

	memmove((void *)(pCurrFileInfo->szFilePath), 
				(void *)szFilePath, 
				(unsigned)strlen(szFilePath));

	strcat(pCurrFileInfo->szFilePath, "/");

	pCurrFileInfo->bValidFlag = FALSE;

	return pCurrFileInfo->szFileName;
}

/*==========================================================================*
 * FUNCTION : short GetStrLen
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pData : 
 * RETURN   : static unsigned : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-03-07 12:47
 *==========================================================================*/
static unsigned short GetStrLen(unsigned char *pData, unsigned short iMaxLen)
{
	unsigned short iLen;
	char *pch;
	iLen = 0;
	
	if (pData == NULL)
	{
		return 0;
	}

	pch = pData;

	while(*pch)
	{
		if (IS_WHITE_SPACE(*pch))
		{
			if (iLen < iMaxLen)
			{
				pch++;
				if (IS_WHITE_SPACE(*pch))
				{
					break;
				}
				else
				{
					iLen++;
				}
			}
			else
			{
				break;
			}
		}

		iLen++;
		if (iLen >= iMaxLen)
		{
			break;
		}
		pch++;
	}

	return iLen;
}

static unsigned short GetBackStrLen(unsigned char *pData, unsigned short iMaxLen)
{
	unsigned short iLen;
	char *pch;
	iLen = iMaxLen;
	
	if (pData == NULL)
	{
		return 0;
	}

	pch = pData + iMaxLen - 1;

	while(*pch)
	{
		if (IS_WHITE_SPACE(*pch))
		{
			iLen--;				
			if (iLen <= 0)
			{
				break;
			}
			pch--;
		}
		else
		{
			break;
		}
	}

	return iLen;
}
/*==========================================================================*
 * FUNCTION : File_Upload
 * PURPOSE  : According to file name from powerkit, acu knows that this file 
 *			  will be sent to acu and prepare to receive it, and until end.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void * pBuff
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-08 13:04
 *==========================================================================*/
static void File_Upload(void * pBuff)
{
	char szFileName[256];
	char szBuffer[MAX_BUFFER_SIZE];
	char *pszFile;
	FILE_INFO_REC * pFileInfo;
	
	int iBytesRead, iFrameCheck;
	int i, iErrorTimes, iBytesToRead;
	unsigned char * pData;

	FILE * fp;
	unsigned short iDataLen, iBlockNo; 

	DATA_FRAME *pFrame;
	pFrame = (DATA_FRAME *)pBuff;
	iDataLen = pFrame->iDataLength - sizeof(iBlockNo) ;

	pData = (unsigned char *)&(pFrame->dwCRC32);

	iBlockNo = *((unsigned short *)pData);
	pData = pData + sizeof(iBlockNo);
	
	iDataLen = GetStrLen(pData, FILECOPY_FRAME_SIZE);
	//1.The first package only file name information.
	if (iBlockNo == 0)
	{
		memmove(szFileName, (void *)pData, iDataLen);
		*(szFileName + iDataLen) = 0;
#ifdef _DEBUG_MAINTN_
		printf("SzFileName is %s\n", szFileName);
#endif
	}
	else
	{
		ReturnToPowerKit(OTHER_ERROR, NULL);
		return;
	}	

	//2.process file name
	 pszFile = ProcessFileName(szFileName);
	 if (pszFile == NULL)
	 {
		 ReturnToPowerKit(FAIL_OPERATE, NULL);
		 return;
	 }

#ifdef _DEBUG_MAINTN_
	 printf("pszFile is %s\n", pszFile);
#endif

	//3.Open file szFileName
	remove(pszFile);
	fp = fopen(pszFile, "a+");

	if (fp != NULL)
	{
		ReturnToPowerKit(IS_OK, NULL);
	}
	else
	{
		ReturnToPowerKit(OTHER_ERROR, NULL);
		return;
	}	

	iErrorTimes = 0;
	iBytesToRead = FILE_SEND_FRAME_LEN;
	//4.Continue to read file data
	while (iBlockNo != LAST_DATA_BLOCK)
	{
		//Read data from port 
		iBytesRead = CommRead (g_pDbsData->g_hPort, 
						      szBuffer, 
					          iBytesToRead);

		Sleep(100);
		//Clear counter.
		g_pDbsData->tmLastTime = time(NULL);

		if (iBytesRead < FILE_SEND_FRAME_LEN)
		{
			iErrorTimes++;
			Sleep(1000);
			if (iErrorTimes > 5)
			{
				goto FAIL_UPLOAD;
			}
			continue;
		}

		//Find the start postion
		for (i = 0; i < iBytesRead; i++)
		{
			if (szBuffer[i] == 0x7e)
			{
				pFrame = (DATA_FRAME*)(szBuffer + i);
				break;
			}
		}

		//Call CheckFrame to Check frame is right or not
		iFrameCheck = CheckFrame((char *)pFrame);		

		if (iFrameCheck != 0) //Frame is wrong
		{	
			ReturnToPowerKit(iFrameCheck, NULL);
			if (iErrorTimes > 5)
			{
				goto FAIL_UPLOAD;
			}
			iErrorTimes++;
			Sleep(100);
			continue;
		}
		
		iErrorTimes = 0;
		//iFrameCheck == 0, Frame is right
		//Write data to file at the end of file
		iDataLen = pFrame->iDataLength - sizeof(iBlockNo);
		iBlockNo = *((unsigned short *)(szBuffer + i 
			+ FILE_DATA_START_POS - sizeof(short)));	

#ifdef _DEBUG_MAINTN_
		printf("The iBlockNo is %d\n", iBlockNo);
#endif
		
		if (iDataLen > 0 && (iDataLen <= FILECOPY_FRAME_SIZE))
		{
			pData = (unsigned char *)(szBuffer + i + FILE_DATA_START_POS); 
			if (iBlockNo == LAST_DATA_BLOCK)
			{
				 iDataLen = GetBackStrLen(pData, FILECOPY_FRAME_SIZE);
			}

			fwrite((void *)pData, iDataLen, 1, fp);
			ReturnToPowerKit(IS_OK, NULL);
		}	
		else if (iDataLen == 0)
		{
			if (iBlockNo != LAST_DATA_BLOCK)
			{
				ReturnToPowerKit(INVALID_DATA, NULL);
			}
			else
			{
				ReturnToPowerKit(IS_OK, NULL);
			}			
		}
		else
		{
			ReturnToPowerKit(INVALID_DATA, NULL);
		}	
	};

	//It should add file check at here.
	
	if (fp != NULL)
	{
		fclose(fp);
	}

	//5.Update global variable
	pFileInfo = g_pDbsData->g_pFileInfo + (g_pDbsData->g_iFileNum - 1);
	pFileInfo->bValidFlag = TRUE;

	return;

FAIL_UPLOAD:
	DBS_ClearRxTx(g_pDbsData->g_hPort);
	ReturnToPowerKit(FAIL_OPERATE, NULL);
	if (fp != NULL)
	{
		fclose(fp);
	}
	remove(pszFile);
	return;   
}

/*==========================================================================*
 * FUNCTION : ConfirmCfgUpdate
 * PURPOSE  : Copy current valid file from /var to working directory
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-08 16:35
 *==========================================================================*/
void ConfirmFileUpdate(void)
{
	FILE_INFO_REC * pFileInfo;
	int iRlt;
	int i;
	char szFilePathName[256];
	char szFileNameToDel[256];

	pFileInfo = g_pDbsData->g_pFileInfo;

	if (pFileInfo == NULL || g_pDbsData->g_iFileNum == 0)
	{
		ReturnToPowerKit(IS_OK, NULL);
		return;
	}
	
	for (i = 0; i < g_pDbsData->g_iFileNum; i++)
	{
		if (pFileInfo->bValidFlag)
		{
			memset(szFilePathName, 0, sizeof(szFilePathName));
			memset(szFileNameToDel, 0, sizeof(szFileNameToDel));
			
			sprintf(szFilePathName,"%s %s %s", "cp",pFileInfo->szFileName, 
				pFileInfo->szFilePath);
			sprintf(szFileNameToDel,"%s %s", "rm",pFileInfo->szFileName);

#ifdef _DEBUG_MAINTN_
			printf("Current Copy file name is %s\n", szFilePathName);
			printf("Current Del file name is %s\n", szFileNameToDel);
#endif
			//iRlt = system(szFilePathName);
			iRlt = _SYSTEM(szFilePathName);
#ifdef _DEBUG_MAINTN_
			printf("Current file Copy iRlt is %d\n", iRlt);
#endif
			//iRlt = system(szFileNameToDel);
			iRlt = _SYSTEM(szFileNameToDel);
#ifdef _DEBUG_MAINTN_
			printf("Current file Del iRlt is %d\n", iRlt);
#endif
		}

		pFileInfo++;
	}

	ReturnToPowerKit(IS_OK, NULL);

	if (g_pDbsData->g_pFileInfo)
	{
		DELETE(g_pDbsData->g_pFileInfo);
		g_pDbsData->g_pFileInfo = NULL;
		g_pDbsData->g_iFileNum = 0;
	}
	return; 

}
/*==========================================================================*
 * FUNCTION : File_Download
 * PURPOSE  : According to file name from powerkit, pack the file in acu and 
 *			  send it to powerkit.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void * pBuff 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-08 13:05
 *==========================================================================*/
static void File_Download(void * pBuff)
{
	char szTmpBuff[300];

	int iDataLen;

	DATA_FRAME *pFrame;

	pFrame = (DATA_FRAME *)pBuff;
	iDataLen = pFrame->iDataLength;

#ifdef _DEBUG_MAINTN_
	printf("File name length is %d\n", iDataLen);
#endif
	// ((pFrame->iDataLength <  MIN_FRAME_LENGTH) ||
	if (pFrame->iDataLength >  FILE_SEND_FRAME_LEN)
	{
		ReturnToPowerKit(OTHER_ERROR, NULL);
		return;
	}

	//At the start position of pFrame->dwCRC32 ,its content is
	//szFileName's starting position actually.
	memmove((void *)szTmpBuff, (void *)&(pFrame->dwCRC32), 
		(unsigned)iDataLen);
	*(szTmpBuff + iDataLen) = 0;
	
#ifdef _DEBUG_MAINTN_
	printf("filemame is %s\n",szTmpBuff);
#endif

	Send_File(szTmpBuff);

	return;
}

/*==========================================================================*
 * FUNCTION : File_Delete
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char * pFileName : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-08 13:56
 *==========================================================================*/
static void File_Delete(char * pFileName)
{
	int iResult;
	int iErrTimes;
	
	iErrTimes = 0;
	iResult = -1;

#ifdef _DEBUG_MAINTN_
	printf("File %s will be deleted.\n", pFileName);
#endif
	do
	{
		iErrTimes++;
		iResult = remove(pFileName);

	} while((iResult == -1) && (iErrTimes < 3));
	
	if (iResult == 0)  //Succeed
	{
#ifdef _DEBUG_MAINTN_
	printf("File %s has been deleted.\n", pFileName);
#endif
		ReturnToPowerKit(IS_OK, NULL);
	}
	else   //Failed
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
	}

	return;
}

/*==========================================================================*
 * FUNCTION : Reset_System
 * PURPOSE  : Exit current program and restart system
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-08 13:58
 *==========================================================================*/
void DSP_Reboot_System(void)
{
	//Record reboot event
	AppLogOut("DBS_REBOOT", APP_LOG_INFO, 
		"System received a reboot command requst from PowerKit.");
	
	//Set variables exit flag
	g_pDbsData->g_bDBS_RunFlag = I_EXIT_FLAG;
	g_pDbsData->g_bRebootCmd = TRUE;	
	//iResult = system("reboot");
	return;
	
}


/*==========================================================================*
 * FUNCTION : EndCurrConnect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-12 15:06
 *==========================================================================*/
static void EndCurrConnect(void)
{
	//Send information to powerkit.
	ReturnToPowerKit(IS_OK, NULL);
	
#ifdef _DEBUG_MAINTN_
	printf("Comand is 17.\n");
#endif //_DEBUG_MAINTN_

	//Set variables end flag
	g_pDbsData->g_bEndCurrConnect = TRUE;
	g_pDbsData->g_bValidUser = FALSE;
	return;	
}

/*==========================================================================*
 * FUNCTION :Get_Samplers_List
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-09 14:53
 *==========================================================================*/
static void Get_Samplers_List(void)
{
	char szSamplerList[MAX_BUFFER_SIZE];
	char szFrameBuffer[MAX_BUFFER_SIZE];

	unsigned short iBlockNo;
	int	 iSamplersPerBlock;
	int  iRecordSize, iDataStartPos;
	int  iCounters;
    
	int	 iReturnErrs;
	int  iCurrSampers;
	int  iBufLen, iNameLen;

	USED_SAMPLER_INFO stSamplerInfo;

	//g_SiteInfo.pSamplerInfo->   
	SAMPLER_INFO * pSamplerList;  //From data exchange interface

	//Get the sample signals value list  of a equipment
	iReturnErrs = DxiGetData(VAR_SAMPLERS_LIST, 0, 0, &iBufLen, 
		&pSamplerList, 100);

	if (iReturnErrs != ERR_DBS_OK)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

#ifdef _DEBUG_MAINTN_
	printf("GetSamplerList is complete.iReturnErrs is %d\n", iReturnErrs);
#endif

	iCurrSampers = iBufLen / sizeof(SAMPLER_INFO);
	iRecordSize  = sizeof(USED_SAMPLER_INFO);
	iSamplersPerBlock = FILECOPY_FRAME_SIZE / iRecordSize;
	///////////////////////////////////////////////////////////////////////////

	iCounters		= 1;	
	iBlockNo		= 1;
	iDataStartPos	= 0;

	while (iCounters <= iCurrSampers)
	{		
		//pSamplerList = Next Sampler Information
		memset(&stSamplerInfo, 0x20, (unsigned)sizeof(stSamplerInfo));
		stSamplerInfo.bySamplerAddr = (BYTE)pSamplerList->iSamplerAddr;
		stSamplerInfo.byPortNo		= (BYTE)pSamplerList->iPollingPortID;
		stSamplerInfo.byCommStatus	= // added handling of no-attached equipment. maofuhua. 2005-4-15
			(pSamplerList->nRelatedEquipment > 0) ? (BYTE)pSamplerList->bCommStatus 
												  : 2; // Not attached equip
		stSamplerInfo.dwSamplerID	= (DWORD)pSamplerList->iSamplerID;

		iNameLen = strlen(pSamplerList->pSamplerName->pFullName[0]);
		strncpyz(stSamplerInfo.szSamplerName, 
			pSamplerList->pSamplerName->pFullName[0], (size_t)32);	
		
		memset((void *)(stSamplerInfo.szSamplerName + iNameLen), 0x20, 
			(unsigned)(32 - iNameLen));
		
#ifdef _DEBUG_MAINTN_
		printf("stSamplerInfo.szSamplerName is %s\n", 
			stSamplerInfo.szSamplerName);
#endif
		//Read data from file and fill to szBuffer
		memmove((void *)(szSamplerList + iDataStartPos), (void *)&stSamplerInfo, 
				sizeof(USED_SAMPLER_INFO));

		iDataStartPos = iDataStartPos + sizeof(USED_SAMPLER_INFO);
		
		//Send data
		if (iCounters%iSamplersPerBlock == 0 || (iCounters == iCurrSampers))
		{	
			if (iCounters == iCurrSampers)
			{
				iBlockNo = LAST_DATA_BLOCK;
			}

			PackFileBlock(szFrameBuffer, szSamplerList, iDataStartPos, iBlockNo);
			if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
			{
				ReturnToPowerKit(FAIL_OPERATE, NULL);
				return;
			}
			
			iBlockNo++;	
			iDataStartPos = 0;
			memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
			memset(szSamplerList, 0, (unsigned)sizeof(szSamplerList));
		}

		iCounters++;
		pSamplerList++;
	}	

	return;	
}
/*==========================================================================*
 * FUNCTION : Acquisite_Data
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iEquipID : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-11 11:09
 *==========================================================================*/
static void Acquisite_Data(int iEquipID)
{
	int iBufLen;
	int iTotalSignals;
	int iTimeOut;
	int iErrCounts;
	char szSignalsList[MAX_BUFFER_SIZE];
	char szFrameBuffer[MAX_BUFFER_SIZE];

	unsigned short iBlockNo;
	int	 iSignalsPerBlock;
	int  iRecordSize, iDataStartPos;
	int  iSigNameLen, iSigUnitLen;
	int  iCounters;

	SAMPLE_SIG_VALUE * pSigValue;  //From data exchange interface
	ACQUI_SIG_VAL		stSigVal;

	iTimeOut = 1000;

	//Get the sample signals value list  of a equipment
	//GetSamSigValueofEquip(iEquipID, 0,&iBufLen,(void *)pSigValue, iTimeOut);
	iErrCounts = DxiGetData(VAR_SAM_SIG_VALUE_OF_EQUIP, iEquipID, 0,
		&iBufLen, &pSigValue, iTimeOut);
	
	if (iErrCounts != ERR_DXI_OK)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
	}

	iTotalSignals = iBufLen / sizeof(SAMPLE_SIG_VALUE);	
	iRecordSize  = sizeof(ACQUI_SIG_VAL);	
	iSignalsPerBlock = FILECOPY_FRAME_SIZE / iRecordSize;

	///////////////////////////////////////////////////////////////////////////

	iCounters		= 1;	
	iBlockNo		= 1;
	iDataStartPos	= 0;

	memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
	memset(szSignalsList, 0, (unsigned)sizeof(szSignalsList));

	while (iCounters <= iTotalSignals)
	{		
		//pSamplerList = Next Sampler Information
		memset(&stSigVal, 0, (unsigned)sizeof(ACQUI_SIG_VAL));

		iSigNameLen = strlen(pSigValue->pStdSig->pSigName->pFullName[0]);
		memmove(stSigVal.szSigName, pSigValue->pStdSig->pSigName->pFullName[0], 
			(unsigned)iSigNameLen);

#ifdef _DEBUG_MAINTN_
		printf("stSigVal.szSigName is %s\n", stSigVal.szSigName);
#endif

		if (iSigNameLen < SIG_NAME_LEN)
		{
			memset((void *)(stSigVal.szSigName + iSigNameLen), 0x20,
				(unsigned)(SIG_NAME_LEN - iSigNameLen));
		}

		stSigVal.byValueType = pSigValue->bv.ucType;
		stSigVal.varValue = pSigValue->bv.varValue;

		iSigUnitLen = strlen(pSigValue->pStdSig->szSigUnit);

#ifdef _DEBUG_MAINTN_
		printf("iSigUnitLen is %d\n", iSigUnitLen);
#endif
		if (iSigUnitLen == MV_PER_DEG_C_LEN)
		{
			stSigVal.szUnit[0] = (char)(MV_PER_DEG_C_LEN);
			iSigUnitLen = 1;
		}
		else
		{
			memmove(stSigVal.szUnit, pSigValue->pStdSig->szSigUnit, 
				(unsigned)iSigUnitLen);
#ifdef _DEBUG_MAINTN_
			printf("stSigVal.szUnit is %s\n", stSigVal.szUnit);
#endif
		}
		
		if (iSigUnitLen < SIG_UNIT_LEN)
		{
			memset((void *)(stSigVal.szUnit + iSigUnitLen), 0x20,
				(unsigned)(SIG_UNIT_LEN - iSigUnitLen));
		}

		//memcpy((void *)&(stSigVal.varValue), (void *)&(pSigValue->bv.varValue), 
		//	(unsigned)sizeof(VAR_VALUE));

		//Read data from file and fill to szBuffer
		memmove(szSignalsList + iDataStartPos, &stSigVal, 
			(unsigned)sizeof(ACQUI_SIG_VAL));

		iDataStartPos = iDataStartPos + sizeof(ACQUI_SIG_VAL);
		
		//Send data
		if (iCounters%iSignalsPerBlock == 0 || (iCounters == iTotalSignals))
		{	
			if (iCounters == iTotalSignals)
			{
				iBlockNo = LAST_DATA_BLOCK;
			}

			PackFileBlock(szFrameBuffer, szSignalsList, iDataStartPos, iBlockNo);
			if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
			{
				ReturnToPowerKit(FAIL_OPERATE, NULL);
				return;
			}
			
			iBlockNo++;	
			iDataStartPos = 0;
			memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
			memset(szSignalsList, 0, (unsigned)sizeof(szSignalsList));
		}
			
		iCounters++;
		pSigValue++;
	}	

	return;
}

/*==========================================================================*
 * FUNCTION : PackFileBlock
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Get_File_List
 * ARGUMENTS: OUT	char *szFrameBuffer
 *			  IN	char *          szDataBuff : 
 *            IN	int             iDataLen   : 
 *            IN	unsigned short  iBlockNo   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-20 09:56
 *==========================================================================*/
static void PackFileBlock(char *szFrameBuffer, char *szDataBuff, 
				   int iDataLen, unsigned short iBlockNo)
{
	DWORD dwCRC32;

	int iLength;
	DATA_FRAME stFrame;

	//Start to pack frame
	stFrame.bySOH		= 0x7e;
	stFrame.bySEQ		= 0x01;
	stFrame.byACUAddr	= 0x00;
	stFrame.byCID1		= 0xD5;
	stFrame.byCID2		= 0x00;	//00H   Frame is right, Or pass user verification 		
	stFrame.byFlag		= 0;
	stFrame.iDataLength = iDataLen + sizeof(iBlockNo);

	//Change length in order to let Mr.Bao process more simple
	stFrame.iDataLength = FILECOPY_FRAME_SIZE + sizeof(iBlockNo);

	iLength = sizeof(stFrame) - CRC32_BYTES - FRAME_END;
	memmove((void *)szFrameBuffer, (void *)&stFrame, (unsigned)iLength);

	*((unsigned short *)(szFrameBuffer + iLength)) = iBlockNo;
	iLength = iLength + sizeof(iBlockNo);

	if (iDataLen > 0)
	{
		memmove(szFrameBuffer + iLength, szDataBuff, (unsigned)iDataLen);
	}
	
	iLength = iLength + iDataLen;

	if (iDataLen < FILECOPY_FRAME_SIZE)
	{
		memset(szFrameBuffer + iLength, 0x20, 
			(unsigned)(FILECOPY_FRAME_SIZE - iDataLen));
	}

	iLength = iLength + (FILECOPY_FRAME_SIZE - iDataLen);

	dwCRC32 = RunCRC32((const unsigned char *)szFrameBuffer + FRAME_HEAD,
				(unsigned long)iLength,
				(unsigned long)CRC32_DEFAULT);


	*(DWORD*)(szFrameBuffer + iLength) = dwCRC32;
	iLength = iLength + CRC32_BYTES;
	*(BYTE *)(szFrameBuffer + iLength) = 0x0D;
	iLength = iLength + FRAME_END;

	return;
	//End package
	/////////////////////////////////////////////////	
}

/*==========================================================================*
 * FUNCTION : ChangeCheckNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  szBuffer     : 
 *            int     iFrameLength : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-20 10:13
 *==========================================================================*/
static void ChangeCheckNo(char * szBuffer, int iFrameLength)
{
	DWORD dwCRC32;
	int iLength;

	iLength = iFrameLength - FRAME_HEAD - CRC32_BYTES - FRAME_END;
	
	dwCRC32 = RunCRC32((const unsigned char *)szBuffer + FRAME_HEAD,
				(unsigned long)iLength,
				(unsigned long)CRC32_DEFAULT);

	iLength = iLength + FRAME_HEAD;
	*(DWORD*)(szBuffer + iLength) = dwCRC32;
	/////////////////////////////////////////////////	
	return;
}
/*==========================================================================*
 * FUNCTION : SendFilePackage
 * PURPOSE  : 
 * CALLS    : ChangeCheckNo
 * CALLED BY: Get_File_List
 * ARGUMENTS: char  *szBuffer    : 
 *            int   iFramelength : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-20 10:07
 *==========================================================================*/
static BOOL SendFilePackage(char *szBuffer, int iFrameLength)
{
	char szCheckBuff[32];
	int iBytesRead, iFrameCheck;
	int iErrTimes, iReadErrTimes, iBytesWritten;
	int i;
	DATA_FRAME * pFrame;

	iErrTimes = 0;
	iReadErrTimes = 0;

	//Send file
	////////////////////////////////////////////////
	iFrameCheck = 0xFF;
	//Start writing data and communicate
	iBytesWritten = CommWrite (g_pDbsData->g_hPort, 
						szBuffer, 
						iFrameLength);	
	
#ifdef _DEBUG_MAINTN_
	printf("iBytesWritten is %d\n",iBytesWritten);
#endif //_DEBUG_MAINTN_


	//Wait a second, it will be adjusted according debug
	Sleep(500);

	//Verify data sending		
	while(iFrameCheck != IS_OK)
	{
		//Read feedback data from powerkit 
		iBytesRead = CommRead(g_pDbsData->g_hPort, szCheckBuff, 32);

		
#ifdef _DEBUG_MAINTN_
		printf("iReadErrTimes is %d\n",iReadErrTimes);
#endif //_DEBUG_MAINTN_

		if (iBytesRead <= 0)
		{
			iReadErrTimes++;
			if ( iReadErrTimes > 5)
			{
				goto FAIL_SEND;
			}
			Sleep(1000);
			continue;
		}

		//Find the start postion
		for (i = 0; i < iBytesRead; i++)
		{
			if (szCheckBuff[i] == 0x7e)
			{
				pFrame = (DATA_FRAME *)(szCheckBuff + i);
				break;
			}
		}

		//Call CheckFrame to Check frame is right or not
		iFrameCheck = CheckFrame((char *)pFrame);

		if ((iFrameCheck != IS_OK)) //Frame is wrong
		{	
			iReadErrTimes++;
			ReturnToPowerKit(iFrameCheck, NULL);

			if ( iReadErrTimes > 50)
			{
				goto FAIL_SEND;
			}
			Sleep(1000);
			continue;
		}

		//recover 
		iReadErrTimes = 0;
		if (pFrame->byCID2 != IS_OK)      //Send data again
		{
			iErrTimes++;
			*((BYTE *)(szBuffer + 1)) = iErrTimes + 1;

			//recheck
			ChangeCheckNo(szBuffer, iFrameLength);

			iBytesWritten = CommWrite(g_pDbsData->g_hPort,  
						szBuffer, 
						iFrameLength);
			Sleep(500);				
		}
		
		//three times
		if(iErrTimes > 3)
		{
			goto FAIL_SEND;
		}

	}; //End while and pass verification		
	
	return TRUE;

FAIL_SEND:
	DBS_ClearRxTx(g_pDbsData->g_hPort);
	ReturnToPowerKit(FAIL_OPERATE, NULL);	
	return FALSE; 
}


/*==========================================================================*
 * FUNCTION : IsDirect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszDirName : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-17 17:13
 *==========================================================================*/
BOOL Dbs_IsDirect(char *pszDirName, char * pszBoot)
{
	char  szDirName[256];
	struct  stat sbuf; 

#ifdef _DEBUG_MAINTN_
	//printf("Current folder is pszDirName.pszBoot is %s\n", pszBoot);		
#endif

	strcpy(szDirName, pszBoot);
	strcat(szDirName, "/");
	strcat(szDirName, pszDirName);

#ifdef _DEBUG_MAINTN_
	//printf("Complete path is %s\n", szDirName);		
#endif

	lstat(szDirName, &sbuf); 	
		
	//Check it is (sbuf.st_mode&S_IFMT)==S_IFDIR 
	if ((sbuf.st_mode & S_IFMT) == S_IFDIR)

	{
		return TRUE;
	} 
	
	return FALSE;
}

/*==========================================================================*
 * FUNCTION : SendFileFolder
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  pszDirName : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-20 09:47
 *==========================================================================*/
BOOL SendFileFolder(char * pszDirName, unsigned short *iBlockNo)
{
	char szCurrDir[128];
	char szTmpFileName[128];
	char szNameList[MAX_BUFFER_SIZE];
	char szFrameBuffer[MAX_BUFFER_SIZE];

	unsigned short iBlockNum;

	BOOL bHaveData;
	DIR * pDir;
	struct dirent * pdirent;
		
	int	 iDataLen, iNamelen, iSurLen;

	iBlockNum = *iBlockNo;
	//To read config directory		
	memset(szNameList, 0, (unsigned)sizeof(szNameList));
	memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));

	pDir = opendir(pszDirName);

	if (pDir == NULL)
	{
		return FALSE;
	}

	strncpyz(szNameList, pszDirName, (size_t)300);
	memmove((void *)szNameList, (void *)pszDirName, 
				(unsigned)strlen(pszDirName));

	strcat(szNameList, "|");

	iDataLen = strlen(pszDirName) + 1;		
	bHaveData = FALSE;

	//Continue to read file name list
	while ((pdirent = readdir(pDir)) != NULL) 
	{ 
		if(pdirent->d_ino == 0) 
		{
			continue;
		}

		if ((strcmp(pdirent->d_name, ".") == 0) ||
			(strcmp(pdirent->d_name,"..") == 0)) 
		{
			continue;
		}

#ifdef _DEBUG_MAINTN_
			printf("Come here.x, pszDirName is %s, pdirent->d_name is %s\n", 
				pszDirName, pdirent->d_name);
#endif
		if (Dbs_IsDirect(pdirent->d_name, pszDirName))
		{
            memset(szCurrDir, 0, (unsigned)sizeof(szCurrDir));
			memmove((void *)szCurrDir, (void *)pszDirName, 
				(unsigned)strlen(pszDirName));
			strcat(szCurrDir, "/");
			strcat(szCurrDir, pdirent->d_name);	

			if (!SendFileFolder(szCurrDir, &iBlockNum))
			{
				closedir(pDir);
				return FALSE;
			}			
			
			iBlockNum = iBlockNum + 1;
			continue;
		}

		iNamelen = strlen(pdirent->d_name);
		iSurLen = FILECOPY_FRAME_SIZE - iDataLen;

		if (iSurLen > iNamelen)
		{
			strcat(szNameList, pdirent->d_name); 
			strcat(szNameList, "|");
			iDataLen = iDataLen + iNamelen + 1;
		}
		else
		{
			//temporary storage
			strncpyz(szTmpFileName, pdirent->d_name, (size_t)128);
			bHaveData = TRUE;

			//*iBlockNo = *iBlockNo + 1;			
			PackFileBlock(szFrameBuffer, szNameList, iDataLen, iBlockNum);

#ifdef _DEBUG_MAINTN_
			printf("Come here.2.1, iDataLen is %d\n", iDataLen);
#endif
			if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
			{
				ReturnToPowerKit(FAIL_OPERATE, NULL);
				return FALSE;
			}

			memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
			memset(szNameList, 0, (unsigned)sizeof(szNameList));

			if (bHaveData)
			{
				//New block
				//memmove((void *)szNameList, (void *)pszDirName, 
				//(unsigned)strlen(pszDirName));
				//strcat(szNameList, "|");
				strcat(szNameList, szTmpFileName);
				strcat(szNameList, "|");
				//iDataLen = strlen(pszDirName) + 1;
				//iDataLen = iDataLen + strlen(szTmpFileName) + 1;
				iDataLen = strlen(szTmpFileName) + 1;
				memset((void *)szTmpFileName, 0, (unsigned)sizeof(szTmpFileName));
				bHaveData = FALSE;

			}
			else
			{
				iDataLen = 0;
			}

			iBlockNum = iBlockNum + 1;
		}//end else

	}//end while
	

	if ((iDataLen != 0) && 
		((unsigned)iDataLen > (unsigned)(strlen(pszDirName) + 1)))
	{
		//iBlockNo++;
		PackFileBlock(szFrameBuffer, szNameList, iDataLen, iBlockNum);
		if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);
			return FALSE;
		}
		memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
		memset(szNameList, 0, (unsigned)sizeof(szNameList));
	}

	if ((unsigned)iDataLen == (unsigned)(strlen(pszDirName) + 1))
	{
		iBlockNum--;	
	}

	closedir(pDir);

	*iBlockNo = iBlockNum;

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : Send_First_Files_Block
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: unsigned short  *piBlockNo : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-27 09:11
 *==========================================================================*/
static BOOL Send_First_Files_Block(unsigned short *piBlockNo)
{
	char szCurrDir[256];
	char szTmpFileName[128];
	char szNameList[MAX_BUFFER_SIZE];
	char szFrameBuffer[MAX_BUFFER_SIZE];

	BOOL bHaveData;
	char * pszDir;
	DIR * pDir;
	struct dirent * pdirent;
	
	unsigned short iBlockNo;
	int  iCounters;
	int	 iDataLen, iNamelen, iSurLen;

	//Get current working path
	memset(szCurrDir, 0, (unsigned)sizeof(szCurrDir));
	pszDir = getcwd(szCurrDir, 256);
	*piBlockNo = 0;

	if (pszDir == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return FALSE;
	}

	iCounters = 0;	
	iBlockNo = 0;

	//To read config directory		
	memset(szNameList, 0, (unsigned)sizeof(szNameList));
	memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
		
	pDir = opendir(szCurrDir);

	if (pDir == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return FALSE;
	}
		
	memmove((void *)szNameList, (void *)szCurrDir, 
				(unsigned)strlen(szCurrDir));
	strcat(szNameList, "|");

	iDataLen = strlen(szCurrDir) + 1;		
	//Continue to read file name list
	while ((pdirent = readdir(pDir)) != NULL) 
	{ 
		//Reset timer counter
		g_pDbsData->tmLastTime = time(NULL);

		if(pdirent->d_ino == 0) 
		{
			continue;
		}

		if ((strcmp(pdirent->d_name, ".") == 0) ||
				(strcmp(pdirent->d_name,"..") == 0) ||
				(strcmp(pdirent->d_name,"acu.running") == 0))
		{
			continue;
		}
			
		//If it is a directory,skip
		if (Dbs_IsDirect(pdirent->d_name, szCurrDir))
		{
			continue;
		}

		iNamelen = strlen(pdirent->d_name);
		iSurLen = FILECOPY_FRAME_SIZE - iDataLen;

		if (iSurLen > iNamelen)
		{
			strcat(szNameList, pdirent->d_name); 
			strcat(szNameList, "|");
			iDataLen = iDataLen + iNamelen + 1;
		}
		else
		{
				//temporary storage
			memmove((void *)szTmpFileName, (void *)pdirent->d_name, 
				(unsigned)strlen(pdirent->d_name));
			bHaveData = TRUE;

			iBlockNo++;
			PackFileBlock(szFrameBuffer, szNameList, iDataLen, iBlockNo);
			if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
			{
				ReturnToPowerKit(FAIL_OPERATE, NULL);
				return FALSE;
			}
			memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
			memset(szNameList, 0, (unsigned)sizeof(szNameList));

			if (bHaveData)
			{
				memmove((void *)szNameList, (void *)szTmpFileName, 
				(unsigned)strlen(szTmpFileName));
	
				strcat(szNameList, "|");
				iDataLen = strlen(szTmpFileName) + 1;
				memset((void *)szTmpFileName, 0, (unsigned)sizeof(szTmpFileName));
				bHaveData = FALSE;
			}
			else
			{
				iDataLen = 0;
			}
		}

		iCounters++;
	}//end internal while

	//The  block
	if ((iDataLen != 0))
	{
		iBlockNo++;
		PackFileBlock(szFrameBuffer, szNameList, iDataLen, iBlockNo);
		if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);
			return FALSE;
		}
		memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
		memset(szNameList, 0, (unsigned)sizeof(szNameList));
	}

	closedir(pDir);
	*piBlockNo = iBlockNo;
	return TRUE;	
}

/*==========================================================================*
 * FUNCTION : Get_File_List
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  pPathName : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-11 16:39
 *==========================================================================*/
static void Get_Files_List_Admin(void)
{
	char szCurrDir[256];
	char szTmpCurrDir[256];

	char szTmpFileName[128];
	char szNameList[MAX_BUFFER_SIZE];
	char szFrameBuffer[MAX_BUFFER_SIZE];

	BOOL bHaveData;
	char * pszDir;
	DIR * pDir;
	struct dirent * pdirent;

	
	unsigned short iBlockNo;
	int  iCounters;
	int	 iDataLen, iNamelen, iSurLen;

	//Get current working path
	memset(szCurrDir, 0, (unsigned)sizeof(szCurrDir));
	pszDir = getcwd(szCurrDir, 256);

	if (pszDir == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

	iCounters = 0;	
	iBlockNo = 0;

	//1.Send first block only include file under /scup 
	//in order to powerkit process more convienent
	if (!Send_First_Files_Block(&iBlockNo))
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

	//2.To read other file under ""/scup/xxx"" directory		
	memset(szNameList, 0, (unsigned)sizeof(szNameList));
	memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
		
	pDir = opendir(szCurrDir);

	if (pDir == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;	
	}
		
	memmove((void *)szNameList, (void *)szCurrDir, 
				(unsigned)strlen(szCurrDir));
	strcat(szNameList, "|");

	iDataLen = strlen(szCurrDir) + 1;		
	bHaveData = FALSE;
	//Continue to read file name list
	while ((pdirent = readdir(pDir)) != NULL) 
	{ 
		//Reset timer counter
		g_pDbsData->tmLastTime = time(NULL);

		if(pdirent->d_ino == 0) 
		{
			continue;
		}

		if ((strcmp(pdirent->d_name, ".") == 0) ||
				(strcmp(pdirent->d_name,"..") == 0) ||
				(strcmp(pdirent->d_name,"acu.running") == 0))
		{
			continue;
		}
			
		//
		if (Dbs_IsDirect(pdirent->d_name, szCurrDir))
		{
			memset(szTmpCurrDir, 0, (unsigned)sizeof(szTmpCurrDir));
#ifdef _DEBUG_MAINTN_			
			printf("pdirent->d_name is %s.szCurrDir is %s\n", 
				pdirent->d_name, szCurrDir);
#endif			
			memmove((void *)szTmpCurrDir, (void *)szCurrDir, 
					(unsigned)strlen(szCurrDir));

			strcat(szTmpCurrDir, "/");
			strcat(szTmpCurrDir, pdirent->d_name);
#ifdef _DEBUG_MAINTN_			
			printf("szTmpCurrDir is %s.\n", szTmpCurrDir);
#endif					
			if (!SendFileFolder(szTmpCurrDir, &iBlockNo))
			{
				closedir(pDir);
				return;
			}
				
				iBlockNo = iBlockNo + 1;
				continue;
		}

		iNamelen = strlen(pdirent->d_name);
		iSurLen = FILECOPY_FRAME_SIZE - iDataLen;

		if (iSurLen > iNamelen)
		{
			strcat(szNameList, pdirent->d_name); 
			strcat(szNameList, "|");
			iDataLen = iDataLen + iNamelen + 1;
		}
		else
		{
				//temporary storage
			memmove((void *)szTmpFileName, (void *)pdirent->d_name, 
				(unsigned)strlen(pdirent->d_name));
			bHaveData = TRUE;

			iBlockNo++;
			PackFileBlock(szFrameBuffer, szNameList, iDataLen, iBlockNo);
			if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
			{
				ReturnToPowerKit(FAIL_OPERATE, NULL);
				return;
			}
			memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
			memset(szNameList, 0, (unsigned)sizeof(szNameList));

			if (bHaveData)
			{
				memmove((void *)szNameList, (void *)szTmpFileName, 
				(unsigned)strlen(szTmpFileName));
	
				strcat(szNameList, "|");
				iDataLen = strlen(szTmpFileName) + 1;
				memset((void *)szTmpFileName, 0, (unsigned)sizeof(szTmpFileName));
				bHaveData = FALSE;
			}
			else
			{
				iDataLen = 0;
			}
		}

		iCounters++;
	}//end internal while

	iBlockNo++;
	//The last block
	iBlockNo = LAST_DATA_BLOCK;

	memset(szNameList, 0x20, (unsigned)sizeof(szNameList));

	PackFileBlock(szFrameBuffer, szNameList, iDataLen, iBlockNo);
	if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		closedir(pDir);
		return;
	}
	closedir(pDir);

	return;	
}
/*==========================================================================*
 * FUNCTION : DeleteHisData
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void *  pBuff : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-11 16:47
 *==========================================================================*/
static void DeleteHisData(int iSeqHisData, int iRightLevel)
{
	int iResult;

	if (g_pDbsData->g_iRunningMode == DSI_RUNNING_MODE)
	{
		if ((iSeqHisData == DBS_ALL_HIS_DATAS)||
			(iSeqHisData == DBS_ACTIVE_ALARM_LOG) ||
			(iSeqHisData == DBS_PERSISTENT_SIG_LOG) ||
			(iSeqHisData == DBS_GEN_CTL_DATA) ||
			(iSeqHisData == DBS_SYSTEM_USER_LOG))
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);
            return;
		}
	}

	//Return to powerkit at first
	if ((iSeqHisData < 0) || (iSeqHisData > MAX_DATA_TYPES))
	{
		ReturnToPowerKit(INVALID_DATA, NULL);
		return;
	}
	else
	{
        ReturnToPowerKit(IS_OK, NULL);
	}
	
	switch(iSeqHisData)
	{
	case DBS_ALL_HIS_DATAS:
        if (g_pDbsData->g_iRunningMode == DSP_RUNNING_MODE)
		{
			if (iRightLevel == ADMIN_LEVEL)
			{
#ifdef SYS_LOG_STORE_STRATEGY_CHANGED
				DAT_StorageDeleteRecord (ALL_HIS_DATAS);//Now,Run log is in his data sectors
#else
				//iResult = system(ERASE_HISDATA);
				iResult = _SYSTEM(ERASE_HISDATA);
#endif
				
			}
			else
			{
				//It will erase all history data not include static sector
				DAT_StorageDeleteRecord (ALL_HIS_DATAS);
			}			
		}
		else
		{
			//It will erase all history data not include static sector
			DAT_StorageDeleteRecord (ALL_HIS_DATAS);
		}	
		break;
	case DBS_HIST_ALARM_LOG:
		DAT_StorageDeleteRecord (HIST_ALARM_LOG);
		break;

	case DBS_HIS_DATA_LOG:
		DAT_StorageDeleteRecord (HIS_DATA_LOG);
		break;
	case DBS_STAT_DATA_LOG:
		break;
	case DBS_CTRL_CMD_LOG:
		DAT_StorageDeleteRecord (CTRL_CMD_LOG);
		break;
	case DBS_ACTIVE_ALARM_LOG:
		DAT_StorageDeleteRecord (ACTIVE_ALARM_LOG);
		break;
	case DBS_PERSISTENT_SIG_LOG:
		DAT_StorageDeleteRecord (PERSISTENT_SIG_LOG);
		break;
	case DBS_SYSTEM_USER_LOG:
		if (iRightLevel == ADMIN_LEVEL)
		{
			DAT_StorageDeleteRecord (SYSTEM_USER_LOG);
		}
		break;
	case DBS_BATT_TEST_LOG_FILE:
		DAT_StorageDeleteRecord (BATT_TEST_LOG_FILE);
		break;
	
	case DBS_DSL_TEST_LOG:
		DAT_StorageDeleteRecord (DSL_TEST_LOG);
		break;
	case DBS_GEN_CTL_DATA:
		DAT_StorageDeleteRecord (GEN_CTL_DATA);
		break;
	case DBS_RUNNING_LOG:
		DAT_StorageDeleteRecord(ACU_RUNNING_LOG);
		break;

	case DBS_ALLDATA_INFLASH:

#ifdef SYS_LOG_STORE_STRATEGY_CHANGED
		//iResult = system(ERASE_HISDATA);//Now,Run log is in his data sectors
		iResult = _SYSTEM(ERASE_HISDATA);//Now,Run log is in his data sectors
#else
		/*iResult = system(ERASE_RUN_LOG);
		iResult = system(ERASE_HISDATA);*/
		iResult = _SYSTEM(ERASE_RUN_LOG);
		iResult = _SYSTEM(ERASE_HISDATA);
#endif	

		AppLogOut("DBS_DEL_HISDATA", APP_LOG_INFO, 
			"PowerKit user deleted all flash records, system must be reset.");
		DSP_Reboot_System();

		break;

	default:
		break;
	}

	if (g_pDbsData->g_iRunningMode == DSP_RUNNING_MODE)
	{
		AppLogOut("DBS_DEL_HISDATA", APP_LOG_INFO, 
			"PowerKit user deleted %s under debug program.", 
			pszHisdataName[iSeqHisData]);
	}
	else
	{
		AppLogOut("DBS_DEL_HISDATA", APP_LOG_INFO, 
			"PowerKit user deleted %s under main program.", 
			pszHisdataName[iSeqHisData]);
	}	
	

#ifdef _DEBUG_MAINTN_	
	printf("Current data seq is %d.\n", iSeqHisData);
#endif

	return;
}

/*==========================================================================*
 * FUNCTION : PackSpecialBlock
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: Get_File_List
 * ARGUMENTS: char *          szDataBuff : 
 *            int             iDataLen   : 
 *			  int			  iTranFlag
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-20 09:56
 *==========================================================================*/
static void PackSpecialBlock(char *szFrameBuffer, 
							 char *szDataBuff, int iDataLen, int iTranFlag)
{
	DWORD dwCRC32;

	int iLength;
	DATA_FRAME stFrame;

	//Start to pack frame
	stFrame.bySOH		= 0x7e;
	stFrame.bySEQ		= 0x01;
	stFrame.byACUAddr	= 0x00;
	stFrame.byCID1		= 0xD5;
	stFrame.byCID2		= 0x00;	//00H   Frame is right, Or pass user verification 
	if (iTranFlag == 0)
	{	
		stFrame.byFlag	= 0;
	}
	else
	{
		stFrame.byFlag	= 1;
	}
	stFrame.iDataLength = iDataLen;
	
	iLength = sizeof(stFrame) - CRC32_BYTES - FRAME_END;
	memmove((void *)szFrameBuffer, (void *)&stFrame, (unsigned)iLength);

	if (iDataLen > 0)
	{
		memmove(szFrameBuffer + iLength, szDataBuff, (unsigned)iDataLen);
	}
	
	iLength = iLength + iDataLen;

	dwCRC32 = RunCRC32((const unsigned char *)szFrameBuffer + FRAME_HEAD,
				(unsigned long)(iLength - FRAME_HEAD),
				(unsigned long)CRC32_DEFAULT);

	*(DWORD*)(szFrameBuffer + iLength) = dwCRC32;
	iLength = iLength + CRC32_BYTES;
	*(BYTE *)(szFrameBuffer + iLength) = 0x0D;
	iLength = iLength + FRAME_END;

	return;
	//End package
	/////////////////////////////////////////////////	
}

/*==========================================================================*
 * FUNCTION : AscToChar
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  cc1 : 
 *            char  cc2 : 
 * RETURN   : BYTE : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-12-24 13:39
 *==========================================================================*/
BYTE AscToChar(char cc1, char cc2)
{
	int c1,c2;
    
	if ((cc1==cc2) 
		&& (cc1 == 0x20))
	{
        return 0x20;
	}

	if(cc1 > '9') 
	{
		c1 = (cc1 | 0x20) - 'a' + 10;
	}
	else 
	{
		c1 = cc1 - '0';
	}

	if(cc2 > '9') 
	{
        c2 = (cc2 | 0x20) - 'a' + 10;
	}
	else 
	{
		c2 = cc2 - '0';
	}

   	return (BYTE)( (c1 << 4) | c2 );
}

/*==========================================================================*
 * FUNCTION : SendDataToLowL
 * PURPOSE  : Send data to low level equipment and wait response
 * CALLS    : ChangeCheckNo
 * CALLED BY: TransferData
 * ARGUMENTS: HANDLE hPort
 *			  char  *szBuffer    : To write
 *            int   *piFramelength: The szBuffer length to send to LL equipment 
 *			                        The pszBufL length from low level equipment
 *			  char  *pszBufL:      To receive
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-20 10:07
 *==========================================================================*/
static BOOL SendDataToLowL(HANDLE hPort, char *szBuffer, 
					int *piFrameLength, char * pszBufL)
{
	char szCheckBuff[128];
	int iBytesRead;
	int iErrTimes, iReadErrTimes, iWriteErrTimes, iBytesWritten;
	int i, iLenToReceive, iLenReceived;
	char *pszBuff;


	iErrTimes = 0;

	DBS_ClearRxTx(hPort);
	//Start writing data and communicate
	iWriteErrTimes = 0;
	while(1)
	{
		iBytesWritten = CommWrite (hPort, 
						szBuffer + 1, 
						*piFrameLength);

		if (iBytesWritten == (*piFrameLength))
		{
			break;
		}
		
		iWriteErrTimes++;
		if (iWriteErrTimes > 3)
		{
			*piFrameLength = 0;
			return FALSE;
		}

		Sleep(200);
	}
	
#ifdef _DEBUG_MAINTN_
	printf("iBytesWritten to write to low level is %d\n",iBytesWritten);
#endif //_DEBUG_MAINTN_


	//Verify data sending
	iReadErrTimes = 0;
	while(1)
	{
		//Read feedback data from powerkit 
		iBytesRead = CommRead(hPort, szCheckBuff, 128);
		
#ifdef _DEBUG_MAINTN_
		printf("iReadErrTimes from low level is %d\n",iReadErrTimes);
		printf("from low level iBytesRead is %d\n", iBytesRead);
#endif //_DEBUG_MAINTN_

		if (iBytesRead == 0)
		{
			iReadErrTimes++;
			if ( iReadErrTimes > 3)
			{
				*piFrameLength = 0;
				return FALSE;
			}

			Sleep(200);
			continue;
		}

#ifdef _DEBUG_MAINTN_
		printf("Come here. iBytes is %d\n", iBytesRead);
#endif
		//Find the start postion		
		for (i = 0; i < iBytesRead; i++)
		{
			if (szCheckBuff[i] == 0x7e)
			{
				break;
			}
		}

		if (i == iBytesRead)
		{
			continue;
		}

		iBytesRead = iBytesRead - i;

		if (iBytesRead < 13)
		{
			*piFrameLength = 0;
			return FALSE;
		}
///////////////////////////////////////////////////////////////////////////////
		pszBuff = szCheckBuff + i;
		iLenToReceive = ((AscToChar(*(pszBuff + 9), 
			*(pszBuff + 10)))&0x0f) * 256 +
                            AscToChar(*(pszBuff + 11),
							*(pszBuff + 12)) + 18;

		if (iLenToReceive > LOW_LEVEL_MAX_DATA_LEN)
		{
			*piFrameLength = 0;
			return FALSE;
		}
		else
		{
			break;
		}
///////////////////////////////////////////////////////////////////////////////
	}

	memmove((void *)pszBufL, (void *)(pszBuff), 
			(unsigned)iBytesRead);

	iLenReceived = iBytesRead;
	iLenToReceive = iLenToReceive - iBytesRead;

#ifdef _DEBUG_MAINTN_
	printf("iLenReced is %d, iLenToRec is %d\n", iLenReceived, iLenToReceive);
#endif

	//Continue read other data
	iReadErrTimes = 0;
	while(iLenToReceive > 0)
	{
		memset((void *)szCheckBuff, 0, 128);
		//Read feedback data from powerkit 
		iBytesRead = CommRead(hPort, szCheckBuff, 128);

		
#ifdef _DEBUG_MAINTN_
		printf("iReadErrTimes from low level is %d\n",iReadErrTimes);
		printf("from low level iBytesRead is %d\n", iBytesRead);
#endif //_DEBUG_MAINTN_

		if (iBytesRead == 0)
		{
			iReadErrTimes++;
			if ( iReadErrTimes > 10)
			{
				return FALSE;
			}
			Sleep(200);
			continue;
		}

		if (iBytesRead > 0)
		{
			memmove((void *)(pszBufL + iLenReceived), (void *)(szCheckBuff), 
			(unsigned)iBytesRead);
			iLenReceived = iLenReceived + iBytesRead;
			iLenToReceive = iLenToReceive - iBytesRead;

#ifdef _DEBUG_MAINTN_
			printf("Succeed from low level read iBytesRead is %d\n", iBytesRead);
#endif
		}

		Sleep(100);

	}; //End while and pass verification	

	*(pszBufL + iLenReceived) = 0;
    *piFrameLength = iLenReceived;
	
	DBS_ClearRxTx(hPort);

#ifdef _DEBUG_MAINTN_
			printf("Succeed from low level read iBytesRead \n");
#endif

	return TRUE;  

}

/*==========================================================================*
 * FUNCTION : TransferData
 * PURPOSE  : Transfer data to low level device-for example:SM-BAT,SM-IO
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void * pBuff : 
 *			  int   iDataLen
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-11 15:16
 *==========================================================================*/
static void TransferData(void * pBuff, int iDataLen)
{
	int iBytesToRead, iErrorTimes, iBytesWritten;
	int iFrameLength;
	char szFrameBuffer[3072];
	char szReceivBuff[3072];
	BYTE byPortNo;

	iBytesToRead = 32;
	iErrorTimes  = 0;
	
	//1.parse the pBuff,find the com port
	byPortNo = *((BYTE *)pBuff);
	iFrameLength = iDataLen - 1;
	//2.write data to port
	if (byPortNo == 1)       //COM3
	{
		//PackSpecialBlock(szFrameBuffer, pBuff, iDataLen, 0);
		//iFrameLength = iDataLen + MIN_FRAME_LENGTH;		
		if (g_pDbsData->g_hSamplerPort1 == NULL)
		{
#ifdef _DEBUG_MAINTN_
			printf("Current handle is null.\n");
#endif
			ReturnToPowerKit(FAIL_OPERATE, NULL);
			return;
		}

#ifdef _DEBUG_MAINTN_
		printf("Current buffer is %s.\n", ((char *)pBuff));
#endif
		DBS_ClearRxTx(g_pDbsData->g_hSamplerPort1);
		if (!SendDataToLowL(g_pDbsData->g_hSamplerPort1, ((char *)pBuff), 
			&iFrameLength, szReceivBuff))
		{
#ifdef _DEBUG_MAINTN_
			printf("Fail to SendDataToLowL\n");
#endif
			ReturnToPowerKit(FAIL_OPERATE, NULL);
			return;
		}			
	}
	else if (byPortNo == 2) //COM4
	{
		//PackSpecialBlock(szFrameBuffer, szBuff, iDataLen, 0);
		//iFrameLength = iDataLen + MIN_FRAME_LENGTH; 
		//iFrameLength = iDataLen;

		if (g_pDbsData->g_hSamplerPort2 == NULL)
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);
			return;
		}

		DBS_ClearRxTx(g_pDbsData->g_hSamplerPort2);
		if (!SendDataToLowL(g_pDbsData->g_hSamplerPort2, ((char *)pBuff), 
			&iFrameLength, szReceivBuff))
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);
			return;
		}	
	}
	else
	{
		ReturnToPowerKit(ERROR_CMD_FORMAT, NULL);
#ifdef _DEBUG_MAINTN_
	printf("ERROR_CMD_FORMAT.\n");
#endif
		return;
	}

#ifdef _DEBUG_MAINTN_
	printf("Come here to write data to upper level.\n");
#endif
	//3.Return connect information to Powerkit	
	memset(szFrameBuffer, 0, sizeof(szFrameBuffer));
    PackSpecialBlock(szFrameBuffer, szReceivBuff, iFrameLength, 1);
	iFrameLength = iFrameLength + MIN_FRAME_LENGTH; 

	iBytesWritten = 0;  //Set first value

#ifdef _DEBUG_MAINTN_
	printf("szFrameBuffer + iFrameLength - 1 is %d.\n", *(szFrameBuffer + iFrameLength - 1));
#endif

	while(iBytesWritten != iFrameLength)
	{
		iBytesWritten = CommWrite (g_pDbsData->g_hPort, 
						szFrameBuffer, 
						iFrameLength);
#ifdef _DEBUG_MAINTN_
	printf("iBytesWritten is %d write data to upper level.\n", iBytesWritten);
#endif
		//At most try three times
		if (iErrorTimes > 2)
		{
			ReturnToPowerKit(FAIL_OPERATE, NULL);	
			break;
		}

		iErrorTimes++;	
	}

	return;
}

/*==========================================================================*
 * FUNCTION : ModifyFileName
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char * pFileName :  It need to confirm by baopeiyou. 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-12 11:14
 *==========================================================================*/
static void ModifyFileName(char * pFileName)
{
    int iResult, i;
	int iErrTimes;
	char szOldName[FILE_NAME_LEN * 2];
	char szNewName[FILE_NAME_LEN * 2];
	char *p;
	char *q;
	
	iErrTimes = 0;
	iResult = -1;

	memmove(szOldName, pFileName, FILE_NAME_LEN);
	memmove(szNewName, pFileName + FILE_NAME_LEN, FILE_NAME_LEN);

	p = szOldName;
	q = szNewName;

	while (i < FILE_NAME_LEN)
	{
		if (*(p + i) == 0x20)
		{
			*(p + i) = 0;
		}

		if (*(q + i) == 0x20)
		{
			*(q + i) = 0;
		}
		p++;
		q++;
		i++;		
	}

#ifdef _DEBUG_MAINTN_
	printf("szOldName is %s,szNewName is %s\n", szOldName, szNewName);
#endif

	while((iResult == -1) && (iErrTimes < 3))
	{
		iErrTimes++;
		iResult = rename(szOldName, szNewName);
	}
	
	if (iResult == 0)  //Succeed
	{
		ReturnToPowerKit(IS_OK, NULL);
	}
	else   //Failed
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
	}	
}

/*==========================================================================*
 * FUNCTION : Get_Equips_List
 * PURPOSE  : Get current all equipments ID and Name
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-11 10:57
 *==========================================================================*/
static void Get_Equips_List(void)
{
	char szEquipsList[MAX_BUFFER_SIZE];
	char szFrameBuffer[MAX_BUFFER_SIZE];

	unsigned short iBlockNo;
	int	 iEquipsPerBlock;
	int  iRecordSize, iDataStartPos;
	int  iCounters;
    
	int	 iReturnErrs;
	int  iTotalEquips;
	int  iBufLen, iNameLen;
	int	 iTimeOut;
	//////////////////////////////	

	iTimeOut = 200;
	USED_EQUIP_INFO stUseEquipInfo;
	EQUIP_INFO * pEquipInfo;  //From data exchange interface

	//GetEquipList(0, 0,&iBufLen,	(void*)pInfo, iTimeOut);
	iReturnErrs = DxiGetData(VAR_ACU_EQUIPS_LIST, 0, 0, &iBufLen, 
		&pEquipInfo, iTimeOut);

	if (iReturnErrs != ERR_DBS_OK)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}

	iTotalEquips	= iBufLen / sizeof(EQUIP_INFO);
	iRecordSize		= sizeof(USED_EQUIP_INFO);	
	iEquipsPerBlock = FILECOPY_FRAME_SIZE / iRecordSize;
	///////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG_MAINTN_
		printf("iRecordSize is %d.iBufLen is %d\n", iRecordSize,
			iBufLen);
#endif

	iCounters		= 1;	
	iBlockNo		= 1;
	iDataStartPos	= 0;

	while (iCounters <= iTotalEquips)
	{		
		//pSamplerList = Next Sampler Information
		memset(&stUseEquipInfo, 0x20, (unsigned)sizeof(USED_EQUIP_INFO));

		stUseEquipInfo.iEquipID = pEquipInfo->iEquipID;
		
		strncpyz(stUseEquipInfo.szEquipName, 
			pEquipInfo->pEquipName->pFullName[0], (size_t)32);	

		iNameLen = strlen(pEquipInfo->pEquipName->pFullName[0]);

		memset((void *)(stUseEquipInfo.szEquipName + iNameLen), 
			0x20, (unsigned)(32 - iNameLen));

		//Read data from file and fill to szBuffer
		memmove((void *)(szEquipsList + iDataStartPos), (void *)&stUseEquipInfo, 
				sizeof(USED_EQUIP_INFO));

		iDataStartPos = iDataStartPos + sizeof(USED_EQUIP_INFO);
		
		//Send data
		if ((iCounters%iEquipsPerBlock == 0) || (iCounters == iTotalEquips))
		{	
			if (iCounters == iTotalEquips)
			{
				iBlockNo = LAST_DATA_BLOCK;
			}

			PackFileBlock(szFrameBuffer, szEquipsList, iDataStartPos, iBlockNo);
			if (!SendFilePackage(szFrameBuffer, FILE_SEND_FRAME_LEN))
			{
				ReturnToPowerKit(FAIL_OPERATE, NULL);
				return;
			}
			
			iBlockNo++;	
			iDataStartPos = 0;
			memset(szFrameBuffer, 0, (unsigned)sizeof(szFrameBuffer));
			memset(szEquipsList, 0, (unsigned)sizeof(szEquipsList));
		}
		
		iCounters++;
		pEquipInfo++;
	}	

	return;	
}

/*==========================================================================*
 * FUNCTION : FileIsExisted
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *szFilePathName : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-26 17:05
 *==========================================================================*/
static BOOL UpdateFileExistedInVar(void)
{
	DIR * pDir;
	struct dirent * pdirent;

	pDir = opendir("/var");

	if (pDir == NULL)
	{
		return FALSE;
	}

	//Continue to read file name list
	while ((pdirent = readdir(pDir)) != NULL) 
	{ 
		if(pdirent->d_ino == 0) 
		{
			continue;
		}

		if ((strcmp(pdirent->d_name, SW_SYS_UPDATE_FILE_S) == 0))
		{
			closedir(pDir);
			return TRUE;
		}
		else
		{
			continue;
		}
	}

	closedir(pDir);
	AppLogOut("DBS_UPD_SW", APP_LOG_WARNING, 
			"Current updating file isn't existed.");
	return FALSE;
}

/*==========================================================================*
 * FUNCTION : fnUnzipThread
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-14 13:56
 *==========================================================================*/
static void fnUnzipThread_XXXX(void)
{
	char szOperateStr[256];
	int iRlt;
	long iFileLen;

	FILE *fp;

	//1.This function is only used under debug service program
	if (g_pDbsData->g_iRunningMode == DSI_RUNNING_MODE)
	{
		g_pDbsData->bEndUnzip = TRUE;
		return;
	}

	//2.Check the file "acu.tar.gz"  existed or not
	if (!UpdateFileExistedInVar())
	{
#ifdef _DEBUG_MAINTN_
		printf("%s is not exist.\n", SW_SYS_UPDATE_FILE);
#endif		
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		g_pDbsData->bEndUnzip = TRUE;
		return;
	}
	else
	{
		fp = fopen(SW_SYS_UPDATE_FILE, "rb");

		if (fp != NULL)
		{
			fseek(fp, 0, SEEK_END);
			iFileLen = ftell(fp);

			if (iFileLen <= MIN_UPDATE_FILE_LEN)
			{
                AppLogOut("DBS_UPD_SW", APP_LOG_WARNING, 
					"Current file isn't correct updating *.tar.gz file.");
			}
			fclose(fp);
			fp = NULL;
		}		
	}

#ifdef _DEBUG_MAINTN_
	printf("%s is exist.\n", SW_SYS_UPDATE_FILE);
#endif

	ReturnToPowerKit(IS_OK, NULL);

	//1.unmount /scup/
	//iRlt = system("cd /");
	iRlt = _SYSTEM("cd /");

#ifdef _DEBUG_MAINTN_
	printf("cd / iRlt is %d\n", iRlt);
#endif
	//1.unmount /scup
	//iRlt = system("umount /scup");  //It failed anytime because acu use this acu directory

#ifdef _DEBUG_MAINTN_
	//printf("umount /scup iRlt is %d\n", iRlt);
#endif

	//2.erase first flash
	//iRlt = system(ERASE_PRO_FLS);
	iRlt = _SYSTEM(ERASE_PRO_FLS);
	
#ifdef _DEBUG_MAINTN_
	printf("erase mtd3 flash iRlt is %d\n", iRlt);
#endif

	//3.Recover mount
	//iRlt = system("mount /scup");

#ifdef _DEBUG_MAINTN_
	//printf("mount /scup,iRlt is %d\n", iRlt);
#endif
	//4.cd scup
	//iRlt = system("cd /scup");
	iRlt = _SYSTEM("cd /scup");

#ifdef _DEBUG_MAINTN_
	//printf("mount /scup,iRlt is %d\n", iRlt);
#endif
	if (iRlt == ERR_DBS_OK)
	{
		AppLogOut("DBS_UPD_SW", APP_LOG_INFO, 
			"System has deleted old application software files.");
	}
	else
	{
		/*
        AppLogOut("DBS_UPD_SW", APP_LOG_WAR, 
			"System failed to delete old application software files."
			"Failed to update");

		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
		*/
	}

	
#ifdef _DEBUG_MAINTN_
	printf("cd /scup\n");
#endif

	//5.Extract file
	memset(szOperateStr, 0, sizeof(szOperateStr));
	sprintf(szOperateStr,"%s %s", "tar -zxvf", SW_SYS_UPDATE_FILE);
#ifdef _DEBUG_MAINTN_
	printf("szOperateStr is %s\n", szOperateStr);
#endif
	
	
	//iRlt = system(szOperateStr); 
	iRlt = _SYSTEM(szOperateStr); 
	if (iRlt == ERR_DBS_OK)
	{
		AppLogOut("DBS_UPD_SW", APP_LOG_INFO, 
			"System successfully upgrade application software files.");
		//ReturnToPowerKit(IS_OK, NULL);
	}
	else
	{
       AppLogOut("DBS_UPD_SW", APP_LOG_INFO, 
			"System failed to upgrade application software files.");
	   g_pDbsData->bEndUnzip = TRUE;
	   //ReturnToPowerKit(FAIL_OPERATE, NULL);
	   return;
	}

	g_pDbsData->bEndUnzip = TRUE;
	return;
}

/*==========================================================================*
 * FUNCTION : fnUnzipThread
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-14 13:56
 *==========================================================================*/
static void fnUnzipThread(void)
{
	char szOperateStr[256];
	int iRlt;
	long iFileLen;

	FILE *fp;

	//1.This function is only used under debug service program
	if (g_pDbsData->g_iRunningMode == DSI_RUNNING_MODE)
	{
		g_pDbsData->bEndUnzip = TRUE;
		return;
	}

	//2.Check the file "acu.tar.gz"  existed or not
	if (!UpdateFileExistedInVar())
	{
#ifdef _DEBUG_MAINTN_
		printf("%s is not exist.\n", SW_SYS_UPDATE_FILE);
#endif		
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		g_pDbsData->bEndUnzip = TRUE;
		return;
	}
	else
	{
		fp = fopen(SW_SYS_UPDATE_FILE, "rb");

		if (fp != NULL)
		{
			fseek(fp, 0, SEEK_END);
			iFileLen = ftell(fp);

			if (iFileLen <= MIN_UPDATE_FILE_LEN)
			{
                AppLogOut("DBS_UPD_SW", APP_LOG_WARNING, 
					"Current file isn't correct updating *.tar.gz file.");
			}
			fclose(fp);
			fp = NULL;
		}		
	}

#ifdef _DEBUG_MAINTN_
	printf("%s is exist.\n", SW_SYS_UPDATE_FILE);
#endif

	ReturnToPowerKit(IS_OK, NULL);

	//1.cd /scup
	//iRlt = system("cd /scup");
	iRlt = _SYSTEM("cd /scup");

	//2.Delete all old file sys
	//iRlt = system("rm -rf *");
	iRlt = _SYSTEM("rm -rf *");

	if (iRlt == ERR_DBS_OK)
	{
		AppLogOut("DBS_UPD_SW", APP_LOG_INFO, 
			"System has deleted old application software files.");
	}
	else
	{
		/*
        AppLogOut("DBS_UPD_SW", APP_LOG_WAR, 
			"System failed to delete old application software files."
			"Failed to update");

		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
		*/
	}

#ifdef _DEBUG_MAINTN_
	printf("rm -rf *\n");
#endif
	
#ifdef _DEBUG_MAINTN_
	printf("cd /scup\n");
#endif
	//5.Extract file
	memset(szOperateStr, 0, sizeof(szOperateStr));
	sprintf(szOperateStr,"%s %s", "tar -zxvf", SW_SYS_UPDATE_FILE);
#ifdef _DEBUG_MAINTN_
	printf("szOperateStr is %s\n", szOperateStr);
#endif
	
	
	//iRlt = system(szOperateStr); 
	iRlt = _SYSTEM(szOperateStr); 
	if (iRlt == ERR_DBS_OK)
	{
		AppLogOut("DBS_UPD_SW", APP_LOG_INFO, 
			"System succeed updating application software files.");
		//ReturnToPowerKit(IS_OK, NULL);
	}
	else
	{
       AppLogOut("DBS_UPD_SW", APP_LOG_INFO, 
			"System failed to update application software files.");
	   g_pDbsData->bEndUnzip = TRUE;
	   //ReturnToPowerKit(FAIL_OPERATE, NULL);
	   return;
	}

	g_pDbsData->bEndUnzip = TRUE;
	return;
}
/*==========================================================================*
 * FUNCTION : UpdateAcuSwSys
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-26 15:43
 *==========================================================================*/
static void UpdateAcuSwSys(void)
{
	HANDLE hProcessThread;
	pthread_t hUnzipHandle_t;
	int	iCounter;

	hProcessThread = RunThread_GetId(NULL);	
	RunThread_Heartbeat(hProcessThread);

	g_pDbsData->bEndUnzip = FALSE;

	if (pthread_create( &(hUnzipHandle_t), 
					NULL,//&attr
					(PTHREAD_START_ROUTINE)fnUnzipThread,
					NULL) != 0)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		return;
	}
	else
	{
		//ReturnToPowerKit(IS_OK, NULL);
	}

	iCounter = 60;   //Two minutes = 24*5s
	while(1)
	{	
		if (iCounter > 0)
		{
			iCounter--;

#ifdef _DEBUG_MAINTN_
			printf("hUnzipHandle_t is %d.\n", (int)hUnzipHandle_t);
#endif
		}
		Sleep(5000);
		RunThread_Heartbeat(hProcessThread);

		if ((g_pDbsData->bEndUnzip) ||
			((hUnzipHandle_t != 0) && (iCounter <= 0)) ||
			(hUnzipHandle_t == 0))
		{
			if (hUnzipHandle_t != 0)
			{
				pthread_cancel(hUnzipHandle_t);
			}

			hUnzipHandle_t = 0;
			g_pDbsData->bEndUnzip = FALSE;
			break;
		}
	}
	
	//Delete old file
	remove(SW_SYS_UPDATE_FILE);
	return;
}

/*==========================================================================*
 * FUNCTION : ReceiveAcuSwSys
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pBuff : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-03-08 13:54
 *==========================================================================*/
static void ReceiveAcuSwSys(void)
{
	char *szBuffer;
	
	int iBytesRead, iFrameCheck;
	int i, iErrorTimes, iBytesToRead;
	unsigned char * pData;

	FILE * fp;
	unsigned short iDataLen, iBlockNo; 

	DATA_FRAME *pFrame;
	
	//1.This function is only used under debug service program
	if (g_pDbsData->g_iRunningMode == DSI_RUNNING_MODE)
	{
        ReturnToPowerKit(MAIN_PRO_RUNNING, NULL);
		return;
	}

	//2.Open file szFileName
	remove(SW_SYS_UPDATE_FILE);
	fp = fopen(SW_SYS_UPDATE_FILE, "a+");

	if (fp != NULL)
	{
		ReturnToPowerKit(IS_OK, NULL);
	}
	else
	{
		ReturnToPowerKit(OTHER_ERROR, NULL);
		return;
	}	

	szBuffer = NEW(char, (SYS_FILE_BUFF_SIZE + 64));

	if (szBuffer == NULL)
	{
		ReturnToPowerKit(FAIL_OPERATE, NULL);
		AppLogOut("DBS_REC_SYS", APP_LOG_WARNING, 
					"Fail to new memory,no enough memory.");
		return;
	}
	iErrorTimes = 0;
	iBytesToRead = SYS_FILE_BUFF_SIZE + 32;  //It must less than buffer len
	//3.Continue to read file data
	while (iBlockNo != LAST_DATA_BLOCK)
	{
		//Read data from port 
		if (g_pDbsData->g_hPort == NULL)
		{
			goto FAIL_UPLOAD;
		}

		iBytesRead = CommRead (g_pDbsData->g_hPort, 
						      szBuffer, 
					          iBytesToRead);

		Sleep(100);
		//Clear counter.
		g_pDbsData->tmLastTime = time(NULL);

		if (iBytesRead < (SYS_FILE_BUFF_SIZE + 15))
		{
			iErrorTimes++;
			Sleep(1000);
			if (iErrorTimes > 5)
			{
				AppLogOut("DBS_REC_SYS", APP_LOG_WARNING, 
					"Fail to receive software update file");
				
				goto FAIL_UPLOAD;
			}
			continue;
		}

		//Find the start postion
		for (i = 0; i < iBytesRead; i++)
		{
			if (szBuffer[i] == 0x7e)
			{
				pFrame = (DATA_FRAME*)(szBuffer + i);
				break;
			}
		}

		//Call CheckFrame to Check frame is right or not
		iFrameCheck = CheckFrame((char *)pFrame);		

		if (iFrameCheck != 0) //Frame is wrong
		{	
			ReturnToPowerKit(iFrameCheck, NULL);
			if (iErrorTimes > 5)
			{
				goto FAIL_UPLOAD;
			}
			iErrorTimes++;
			Sleep(100);
			continue;
		}
		
		iErrorTimes = 0;
		//iFrameCheck == 0, Frame is right
		//Write data to file at the end of file
		iDataLen = pFrame->iDataLength - sizeof(iBlockNo);
		iBlockNo = *((unsigned short *)(szBuffer + i 
			+ FILE_DATA_START_POS - sizeof(short)));	

#ifdef _DEBUG_MAINTN_
		printf("The iBlockNo is %d\n", iBlockNo);
#endif
		
		if (iDataLen > 0 && (iDataLen <= SYS_FILE_BUFF_SIZE))
		{
			pData = (unsigned char *)(szBuffer + i + FILE_DATA_START_POS); 
			if (iBlockNo == LAST_DATA_BLOCK)
			{
				 iDataLen = GetBackStrLen(pData, SYS_FILE_BUFF_SIZE);
			}

			fwrite((void *)pData, iDataLen, 1, fp);
			ReturnToPowerKit(IS_OK, NULL);
		}	
		else if (iDataLen == 0)
		{
			if (iBlockNo != LAST_DATA_BLOCK)
			{
				ReturnToPowerKit(INVALID_DATA, NULL);
			}
			else
			{
				ReturnToPowerKit(IS_OK, NULL);
			}			
		}
		else
		{
			ReturnToPowerKit(INVALID_DATA, NULL);
		}	
	};

	//It should add file check at here.
	
	if (fp != NULL)
	{
		fclose(fp);
	}

	if (szBuffer != NULL)
	{
		DELETE(szBuffer);
		szBuffer = NULL;
	}

	AppLogOut("DBS_REC_SW", APP_LOG_ERROR, 
			"System succeed to receive new application software files.");

	return;

FAIL_UPLOAD:
	//Read data from port 
	DBS_ClearRxTx(g_pDbsData->g_hPort);
    ReturnToPowerKit(FAIL_OPERATE, NULL);
    AppLogOut("DBS_REC_SW", APP_LOG_ERROR, 
			"System failed to receive new application software files.");

	if (fp != NULL)
	{
		fclose(fp);
	}

	if (szBuffer != NULL)
	{
		DELETE(szBuffer);
		szBuffer = NULL;
	}
	remove(SW_SYS_UPDATE_FILE);

#ifdef _DEBUG_MAINTN_
	printf("End ReceiveAcuSwSys\n");
#endif
	return;
}
/*==========================================================================*
 * FUNCTION : ACUIsReadOnly
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-18 15:46
 *==========================================================================*/
/*
static int ACUIsReadOnly(void)
{
	char	*pszHardwareSetting;
	BOOL	bACUIsReadOnly;

	pszHardwareSetting = getenv(ENV_ACU_JUMP_APP_RDONLY);
	bACUIsReadOnly = (pszHardwareSetting == NULL) ? FALSE
		: (atoi(pszHardwareSetting) != 0) ? TRUE : FALSE;

	if (bACUIsReadOnly)
	{
#ifdef _DEBUG_MAINTN_
		printf("ACU is read-only.\n");
#endif
		return ACU_JUMP_READONLY;	
	}
	else
	{
#ifdef _DEBUG_MAINTN_
		printf("ACU is not read-only.\n");
#endif
		return ACU_JUMP_NOT_SET;
	}*/
	/*
	FILE *fp;
	char *pszFileContents;
	char *pStep = ":";
	char *pDsnStr;
	char *pSetStr;
	long iFilelen;

	fp = fopen(ACU_RW_SET_FILE, "r");

	if (fp == NULL)
	{
		return ACU_JUMP_OTHER_ERR;
	}
	
	fseek(fp, 0, SEEK_END);
	iFilelen = ftell(fp);

	if (iFilelen == -1)
	{
		fclose(fp);
		return ACU_JUMP_OTHER_ERR;
	}
	fseek(fp, 0, SEEK_SET);

	pszFileContents = NEW(char, iFilelen + 1);
	if (pszFileContents == NULL)
	{
		DELETE(pszFileContents);
		fclose(fp);
		return ACU_JUMP_OTHER_ERR;
	}
	
	iFilelen = fread(pszFileContents, sizeof(char), (size_t)iFilelen, fp);
	fclose(fp);

	if (iFilelen < 0) 
	{
		DELETE(pszFileContents);
		return ACU_JUMP_OTHER_ERR;
	}

	pDsnStr = strstr(pszFileContents, ACU_JUMP_RDONLY);
	
	if (pDsnStr == NULL)
	{
		DELETE(pszFileContents);
		return ACU_JUMP_OTHER_ERR;
	}

	pDsnStr = strtok(pDsnStr, pStep);

	if (pDsnStr == NULL)
	{
		DELETE(pszFileContents);
		return ACU_JUMP_OTHER_ERR;
	}
	
	pDsnStr = strtok(NULL, pStep); //prohibited
	
	if (pDsnStr == NULL)
	{
		DELETE(pszFileContents);
		return ACU_JUMP_OTHER_ERR;
	}

	pSetStr = DBS_RemoveWhiteSpace(pDsnStr);
	if (pSetStr == NULL)
	{
		DELETE(pszFileContents);
		return ACU_JUMP_OTHER_ERR;
	}

	if (strcmp(pSetStr, ACU_JUMP_NOT_RDONLY) != 0)
	{
		DELETE(pszFileContents);
		return ACU_JUMP_READONLY;
	}

	DELETE(pszFileContents);
	
	return ACU_JUMP_NOT_SET;
	*/
//}

/*==========================================================================*
 * FUNCTION : DBS_ReloadDefaultConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-03 13:35
 *==========================================================================*/
static int DBS_ReloadDefaultConfig(void)
{
	int nRet = ERR_DXI_OK;
	char *ppszCfgDirs[] = {CONFIG_DIR, CONFIG_BAK_DIR, CONFIG_DEFAULT_DIR};

	//disable ACU changes
	if (GetSiteInfo()->bSettingChangeDisabled)
	{
		return ERR_DXI_HARDWARE_SWITCH_STATUS;
	}

	if (DXI_CopyConfigFiles(ppszCfgDirs[CONFIG_DEFAULT_TYPE], 
		ppszCfgDirs[CONFIG_NORMAL_TYPE], TRUE))
	{
		// remove the last solution run file will cause system remove all
		// historical data files after system restarted. maofuhua, 2005-3-24
		char szRunningSolution[MAX_FILE_PATH];
	
		MakeFullFileName(szRunningSolution, getenv(ENV_ACU_ROOT_DIR), 
			CONFIG_DIR, SCUP_RUNNING_SOLUTION);

		remove(szRunningSolution);

		//Tell main app to reboot ACU and SCU
		//
		AppLogOut("DBS_DEFAULT_CFG", APP_LOG_INFO, 
			"PowerKit succeed to load default system configuration.");

		//Debug service will reboot system.
		if (g_pDbsData->g_iRunningMode == DSI_RUNNING_MODE)
		{
            DXI_RebootACUorSCU(TRUE, TRUE);
		}
		else
		{
			DSP_Reboot_System();
		}
	}
	else
	{
		nRet = ERR_DXI_LOAD_DEFAULT_CONFIG;

		AppLogOut("DBS_DEFAULT_CFG", APP_LOG_WARNING,
			"Fails on loading default config\n");
	}

	return nRet;
}

//2.3 Thread process function
/*==========================================================================*
 * FUNCTION : fnProcessThread
 * PURPOSE  : Process all kinds of commands from Powerkit and response it
 * CALLS    : 
 * CALLED BY: Process thread
 * ARGUMENTS:    : 
 * RETURN   : void : 
 * COMMENTS : According to different commands, call different functions to 
 *            process relevant service.
 * CREATOR  : LIXIDONG                 DATE: 2004-10-05 15:13
 *==========================================================================*/
static void fnProcessThread(void)
{
	DATA_FRAME * pDataFrame;
	int		i, iErrCode, iErrResult;
	int		iBytesToRead, iBytesRead;
	int		iEquipID, iHisDataID;
	int     iFrameCheck;
	HANDLE  hProcessThread;
	BYTE	byAddr;
	BOOL	bAcuIsReadOnly;
	BOOL	bSuperRightLevel;
	int		iRightLevel;

	char	szBuffer[MAX_FILE_BUFF_SIZE];
	char	szTmpBuff[128];

	iRightLevel = 0;
	iBytesRead = 0;
	iBytesToRead = MAX_FILE_BUFF_SIZE;
	
	hProcessThread = RunThread_GetId(NULL);	

	///////////////////////////////////////////////////////////////////////////
	DBS_ClearRxTx(g_pDbsData->g_hPort);
	
	//Check current ACU is set Read-Only or not
	bAcuIsReadOnly = FALSE;
	//if (ACUIsReadOnly() != ACU_JUMP_NOT_SET)
	if(DAT_CurrentStatusIsRDONLY(ENV_SYS_JUMP_APP_RDONLY))
	{
		bAcuIsReadOnly = TRUE;		
	}

	if (g_pDbsData->g_iRunningMode == DSP_RUNNING_MODE )
	{
		if (bAcuIsReadOnly)
		{
			GetSiteInfo()->bSettingChangeDisabled = bAcuIsReadOnly;
		}
		else
		{
			GetSiteInfo()->bSettingChangeDisabled = FALSE;
		}
	}

	//Super User right level ,it will not be permitted to modify anything
	bSuperRightLevel = FALSE;
	

	//Continue recycling until current user exit 
	while (g_pDbsData->bHaveClient)
	{
		pDataFrame = NULL;
		i = 0;
		
		RunThread_Heartbeat(hProcessThread);

		// To read data from an opened port 
		memset(szBuffer, 0, (unsigned)sizeof(szBuffer));
		iBytesRead = CommRead (g_pDbsData->g_hPort, 
							   szBuffer, 
						       iBytesToRead );	
		//No data to process
		if(iBytesRead < MIN_FRAME_LENGTH)//Get a error frame
		{
			iErrCode = 0;
			iErrResult = CommControl(g_pDbsData->g_hPort, 
						COMM_GET_LAST_ERROR, 
						(char *)&iErrCode,
						sizeof(iErrCode));

			//Client has exited.
			if (iErrCode == ERR_COMM_CONNECTION_BROKEN)
			{
				g_pDbsData->g_bEndCurrConnect = TRUE;
				break;
			}
			
#ifdef _DEBUG_MAINTN_
			printf("iErrResult is %d, iErrCode is %d\n", iErrResult, iErrCode);
#endif //_DEBUG_MAINTN_

			//ReturnToPowerKit(INVALID_DATA, NULL);
			Sleep(1000);
			continue;
		}
		else
		{
			//fresh the timer recounter
			g_pDbsData->tmLastTime = time(NULL);

#ifdef _DEBUG_MAINTN_
			printf("iBytesRead is %d\n", iBytesRead);
#endif
			//Find the start postion
			for (i = 0; i < iBytesRead; i++)
			{
				pDataFrame = (DATA_FRAME *)(&szBuffer[i]);
                //if ((BYTE)szBuffer[i] == 0x7e)
				if (pDataFrame->bySOH == 0x7e)
				{	
					
#ifdef _DEBUG_MAINTN_
					printf("pDataFrame->dwCRC32 is %u\n", (pDataFrame->dwCRC32));
#endif //_DEBUG_MAINTN_
					break;
				}
			}

			if (i == iBytesRead)
			{
				i = 0;
				iBytesRead = 0;
				ReturnToPowerKit(INVALID_DATA, NULL);
				Sleep(100);
				continue;
			}

#ifdef _DEBUG_MAINTN_			
			printf("pDataFrame->byCID2 is %d\n", pDataFrame->byCID2);
#endif

			//Call CheckFrame to Check frame is right or not
			iFrameCheck = CheckFrame(szBuffer + i);

#ifdef _DEBUG_MAINTN_
			printf("iFrameCheck is %d\n", iFrameCheck);
#endif //_DEBUG_MAINTN_

			if (iFrameCheck != 0) //Frame is wrong
			{	
				ReturnToPowerKit(iFrameCheck, NULL);
				Sleep(1000);
				continue;
			}
			
			//Frame is right and valid
			if (i < iBytesRead)
			{
				//Right check
				if (!g_pDbsData->g_bValidUser)
				{
					if (pDataFrame->byCID2 > RIGHT_VERIFY)
					{
						//Any conditions permit exit message
						if (pDataFrame->byCID2 == END_CURR_CONNECTION)
						{
							EndCurrConnect();
						}
							
						if (g_pDbsData->g_iRunningMode == DSP_RUNNING_MODE )
						{
							ReturnToPowerKit(DEBUG_PRO_RUNNING, NULL);
						}
						else
						{
							ReturnToPowerKit(MAIN_PRO_RUNNING, NULL);
						}
						
						Sleep(500);
						continue;
					}					
				}

				//Data needs to tranfer to low level device
				if (pDataFrame->byFlag == 1)
				{
					TransferData((void *)(szBuffer + i + 8), 
								pDataFrame->iDataLength);					
				}
				else //Data will be processed in ACU device
				{	
					/* According to current running different status:0:DSI 1:DSP
					   different command can be implimented.
					   DSI-Debug service interface
				       DSP-Debug service program */
					if (g_pDbsData->g_iRunningMode == DSP_RUNNING_MODE)
					{
						if ((pDataFrame->byCID2 == SWITCH_STATUS) ||
							(pDataFrame->byCID2 == GET_SAMPLERS_LIST) ||
							(pDataFrame->byCID2 == GET_EQUIPS_LIST) ||
							(pDataFrame->byCID2 == ACQUISITE_DATA))
						{
							ReturnToPowerKit(DEBUG_PRO_RUNNING, NULL);
							Sleep(500);
							continue;
						}
					}
					
					//Current ACU is set Read-Only or other error
					//User want to update acu software file
					if (bAcuIsReadOnly || (iRightLevel < ENGINEER_LEVEL))
					{
						if ((pDataFrame->byCID2 == MODIFY_ACU_ADDR) ||
							(pDataFrame->byCID2 == MODIFY_ACU_IP) ||
							(pDataFrame->byCID2 == SET_COMM_SETTING) ||
							(pDataFrame->byCID2 == SET_MANUFACR_INFO) ||
							(pDataFrame->byCID2 == FILE_DELETE) ||
							(pDataFrame->byCID2 == FILE_NAME_MODIFY) ||
							(pDataFrame->byCID2 == CONFIRM_FILE_UPDATE) ||
							(pDataFrame->byCID2 == DELETE_HISDATA) ||
							(pDataFrame->byCID2 == FILE_UPLOAD) ||
							(pDataFrame->byCID2 == UPDAT_ACU_SW) ||
							(pDataFrame->byCID2 == RECEIVE_ACU_SW) ||
							(pDataFrame->byCID2 == RELOAD_DDEAULT_CFG))
						{
							ReturnToPowerKit(ACU_INFO_READ_ONLY, NULL);
							Sleep(500);
							continue;
						}						
					}

					//Command process
					switch(pDataFrame->byCID2) 
					{						
						case CONNECT_INFO_ID:
							ResponseConnect();
							break;

						case RIGHT_VERIFY:
							//User Info start address
							RightsVerify((void *)&(pDataFrame->dwCRC32), 
								&iRightLevel); 							
							break;

						case SWITCH_STATUS:
							//It doesn't exit in Debug program
							Switch_Run_Status();
							break;

						case GET_ACU_ADDR:				 	
							Get_Acu_Addr();
							//UpdateAcuSwSys();  //Used to test
							break;

						case MODIFY_ACU_ADDR:
							byAddr = *((BYTE *)&(pDataFrame->dwCRC32));
							Modify_Acu_Addr(byAddr);						
							break;

						case GET_ACU_IP:
							Get_AcuIP_Addr();
							break;

						case MODIFY_ACU_IP:	
							memmove(szTmpBuff, (void *)&(pDataFrame->dwCRC32), 
							pDataFrame->iDataLength);
							*(szTmpBuff + pDataFrame->iDataLength) = 0;
							Modify_AcuIP_Addr(szTmpBuff);							
							break;

						case GET_COMM_SETTING:	
							Get_Comm_Setting();
							break;

						case SET_COMM_SETTING:

							memmove(szTmpBuff, (void *)&(pDataFrame->dwCRC32), 
								pDataFrame->iDataLength);
							*(szTmpBuff + pDataFrame->iDataLength) = 0;
							Set_Comm_Setting(szTmpBuff);
							break;
						
						case GET_MANUFACR_INFO:	
							Get_Manufacr_Info();
							break;

						case SET_MANUFACR_INFO:	
							memmove(szTmpBuff, (void *)&(pDataFrame->dwCRC32), 
								pDataFrame->iDataLength);
							*(szTmpBuff + pDataFrame->iDataLength) = 0;
							Set_Manufacr_Info(szTmpBuff);
							break;

						case REBOOT_SYSTEM:	
							//Send information to powerkit.
							ReturnToPowerKit(IS_OK, NULL);
							DSP_Reboot_System();
							break;

						case GET_DIAGNOSE_INFO:		
							Send_File(ACU_DIAGNOSE_FILE);	
							break;

						case FILE_UPLOAD:
							File_Upload((void *)pDataFrame);
							break;

						case FILE_DOWNLOAD:	
							File_Download((void *)pDataFrame);
							break;

						case FILE_DELETE:
							memmove(szTmpBuff, 
								(void *)&(pDataFrame->dwCRC32), 
								pDataFrame->iDataLength);
							*(szTmpBuff + pDataFrame->iDataLength) = 0;
							File_Delete(szTmpBuff);
							break;

						case FILE_NAME_MODIFY:
							memmove((void *)szTmpBuff, 
								(void *)&(pDataFrame->dwCRC32), 
								2 * FILE_NAME_LEN); 
							*(szTmpBuff + 2 * FILE_NAME_LEN) = 0;
							ModifyFileName(szTmpBuff);
							break;

						case GET_FILES_LIST:	
							//Get_Files_List();
							Get_Files_List_Admin();
							break;	

						case GET_SAMPLERS_LIST:	
							Get_Samplers_List();
							break;

						case GET_EQUIPS_LIST:	
							Get_Equips_List();
							break;

						case ACQUISITE_DATA:
							iEquipID = *((int*)&(pDataFrame->dwCRC32));
							Acquisite_Data(iEquipID);
							break;

						case DELETE_HISDATA:
							iHisDataID = *((int*)&(pDataFrame->dwCRC32));
							DeleteHisData(iHisDataID, iRightLevel);
							break;

						case END_CURR_CONNECTION:
							EndCurrConnect();
							break;
							
						case CONFIRM_FILE_UPDATE:
							ConfirmFileUpdate();
							break;
						case UPDAT_ACU_SW:
							UpdateAcuSwSys();
							break;

						case RECEIVE_ACU_SW:
							ReceiveAcuSwSys();
							break;

						case RELOAD_DDEAULT_CFG:
							ReturnToPowerKit(IS_OK, NULL);
							DBS_ReloadDefaultConfig();						
							break;

						default:
							ReturnToPowerKit(CRC32_ERROR, NULL);
							break;

					} //End switch, complete processing

				} //End current process in ACU  

			}	  
			else  //This frame is invalid frame, its format is wrong
			{
				ReturnToPowerKit(INVALID_DATA, NULL);
				sleep(1000);
			}			
			/////////////////////////////////////////////////////////////
#ifdef _DEBUG_MAINTN_
			printf("Receive one data.\n");
#endif
		} //End else, current frame must be completely processed.
	}//End while, g_Running must be set to 0.
}

/*==========================================================================*
 * FUNCTION : DBS_ClearRxTx
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hPort : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-12 09:31
 *==========================================================================*/
static void DBS_ClearRxTx( HANDLE hPort)
{
	CommControl( hPort, COMM_PURGE_TXCLEAR, (void*)1, 0);
	CommControl( hPort, COMM_PURGE_RXCLEAR, (void*)1, 0);
}
/*==========================================================================*
 * FUNCTION : CheckFrame
 * PURPOSE  : Check the data frame from powerkit is right or not.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char *  pDataFrame : It include SOH(0x7e).
 * RETURN   : int  :  0x00H		 : Data is right
 *					  0xFFH		 : Data is wrong,it need to send again
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 14:49
 *==========================================================================*/
static int CheckFrame(char * pDataFrame)
{
	DWORD dwOldCRC32, dwCalCRC32;
	int iPos, iLength;
	
	DATA_FRAME * pFrame;
	pFrame = (DATA_FRAME *)pDataFrame;
	
	if (pFrame->iDataLength > (SYS_FILE_BUFF_SIZE + 15))
	{
		
#ifdef _DEBUG_MAINTN_
		printf("pFrame->iDataLength is %d\n", pFrame->iDataLength);
#endif //_DEBUG_MAINTN_

		return CRC32_ERROR;
	}

	iPos = sizeof(DATA_FRAME) + pFrame->iDataLength  //Some data:UserName,password and so on
		- FRAME_END - CRC32_BYTES ;
	
	dwOldCRC32 = *((DWORD *)(pDataFrame + iPos));
	
	iLength = iPos - FRAME_HEAD;
	dwCalCRC32 = RunCRC32((const unsigned char *)pDataFrame + FRAME_HEAD, 
			(unsigned long)iLength, (unsigned long)CRC32_DEFAULT);

	if (dwCalCRC32 == dwOldCRC32)
	{
#ifdef _DEBUG_MAINTN_
		printf("Frame is right.\n");
#endif
		return 0;
	}	

    return CRC32_ERROR;                 //Frame check error
}


/*==========================================================================*
 * FUNCTION : ReturnToPowerKit
 * PURPOSE  : ACU returns some information to powerkit according to different 
 *			  conditions include some error information
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   int		:   iReturnNo	
 *				void    :   *pBuff     : some simple information to powerkit
 * RETURN   :	void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-06 16:22
 *==========================================================================*/
static void ReturnToPowerKit(int iReturnNo, void * pBuff)
{
	DATA_FRAME stDataFrame;
	char szBuffer[MAX_BUFFER_SIZE];
	int  iLength, iCheckLen;
	int  iBytesWritten;
	
	stDataFrame.bySOH		= 0x7e;
	stDataFrame.bySEQ		= 0x01;
	stDataFrame.byACUAddr	= 0x00;
	stDataFrame.byCID1		= 0xD5;
	stDataFrame.byFlag		= 0;
	stDataFrame.iDataLength = 0;
	
	switch(iReturnNo) 
	{					
		case IS_OK:						//00H   Frame is right, Or pass user verification 
			stDataFrame.byCID2 = IS_OK;			
			break;
		case LOW_LEV_NO_RESPONSE:		//01H   No response at low level device
			stDataFrame.byCID2 = LOW_LEV_NO_RESPONSE;
			break;

		case CRC32_ERROR:				//02H	CRC32 number is wrong			
			stDataFrame.byCID2 = CRC32_ERROR;
			break;

		case INVALID_CIDI:				//03H	CID1 is invalid
			stDataFrame.byCID2 = INVALID_CIDI;
			break;

		case INVALID_CIDII:				//04H	CID2 is invalid
			stDataFrame.byCID2 = INVALID_CIDII;
			break;

		case INVALID_DATA:				//05H	Data Information is invalid
			stDataFrame.byCID2 = INVALID_DATA;
			break;

		case ERROR_CMD_FORMAT:			//06H	Command format is wrong	
			stDataFrame.byCID2 = ERROR_CMD_FORMAT;
			break;

		case OTHER_ERROR:				//07H	Other error
			stDataFrame.byCID2 = OTHER_ERROR;
			break;

		case MAIN_PRO_RUNNING:			//08H	Please do right verification
										//		Main program is running
			stDataFrame.byCID2 = MAIN_PRO_RUNNING;
			break;

		case DEBUG_PRO_RUNNING:			//09H	Please do right verification
										//      Debug program is running
			stDataFrame.byCID2 = DEBUG_PRO_RUNNING;
			break;	

		case PASSWORD_ERROR:			//0AH	User password is not correct
			stDataFrame.byCID2 = PASSWORD_ERROR;
			break;

		case USER_NOT_EXIST:			//0BH	User does not exist
			stDataFrame.byCID2 = USER_NOT_EXIST;
			break;

		case FILE_NOT_EXIST:			//0CH	File does not exist
			stDataFrame.byCID2 = FILE_NOT_EXIST;
			break;

		case ACU_INFO_READ_ONLY:		//0EH	ACU Read_Only
			stDataFrame.byCID2 = ACU_INFO_READ_ONLY;
			break;

		case RETURN_ADDR:				//10H   Return acu addr
			stDataFrame.byCID2 = 0x10;
			stDataFrame.byACUAddr = *((BYTE *)pBuff);
			stDataFrame.iDataLength = sizeof(char);
			PackSpecialBlock(szBuffer, pBuff, sizeof(char), 0);
			iLength = sizeof(DATA_FRAME) + stDataFrame.iDataLength;

			
#ifdef _DEBUG_MAINTN_
			printf("stDataFrame.byACUAddr is %d\n",stDataFrame.byACUAddr);
#endif //_DEBUG_MAINTN_

			break;
		case EEM_USING_COM:				//11H   EEM is using serial com
			stDataFrame.byCID2 = EEM_USING_COM;
			break;

		case FAIL_OPERATE:				//FFH   Frame errorr
			stDataFrame.byCID2 = 0xFF;
			break;

		case 0x12:				//FFH   Frame errorr
			stDataFrame.byCID2 = 0x12;
			break;

		default:
			stDataFrame.byCID2 = 0xFF;
			break;
	} //end switch

	if (g_pDbsData->g_hPort == NULL)
	{
		return;
	}

	if (stDataFrame.iDataLength == 0) //No data to sent
	{
		//Data will be packed here				 
		iCheckLen = sizeof(stDataFrame) - FRAME_HEAD 
			- FRAME_END - CRC32_BYTES;

		stDataFrame.dwCRC32 = RunCRC32((const unsigned char *)&(stDataFrame) 
			+ FRAME_HEAD, 
			(unsigned long)iCheckLen, 
			(unsigned long)CRC32_DEFAULT);

		stDataFrame.byEOF = 0x0D;

		iLength = sizeof(stDataFrame);
		iBytesWritten = CommWrite (g_pDbsData->g_hPort, 
						(char *)&stDataFrame, 
						iLength);

		
#ifdef _DEBUG_MAINTN_
		printf("iBytesWrite is %d\n", iBytesWritten);
#endif //_DEBUG_MAINTN_


	}
	else    //There is data to be sent
	{
		//Data has been packed.		
		iBytesWritten = CommWrite (g_pDbsData->g_hPort, 
						szBuffer, 
						iLength);
		
#ifdef _DEBUG_MAINTN_
		printf("iBytesWrite is %d\n", iBytesWritten);
#endif //_DEBUG_MAINTN_

	}

	return;

}

/*==========================================================================*
 * FUNCTION : DBS_non_region_erase
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: DM_EraseSector
 * ARGUMENTS: int  Fd       : //Device file handle
 *            int  start    : //Start position
 *            int  count    : //current sector size divided by erase_info_t.length
 *								erase_info_t.length = mtd_info_t.erasesize
 *            int  unlock   : //0:all is unlocked.1;some bites is locked
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-09-23 15:35
 *==========================================================================*/
static int DBS_non_region_erase(int Fd, int start, int count, int unlock)
{
	mtd_info_t meminfo;

	if (ioctl(Fd,MEMGETINFO,&meminfo) == 0)
	{
		erase_info_t erase;

		erase.start = start;

		erase.length = meminfo.erasesize;

#ifdef _DEBUG_MAINTN_
		printf("Erase Unit Size 0x%x, ", meminfo.erasesize);
#endif

		for (; count > 0; count--) 
		{

#ifdef _DEBUG_MAINTN_
		printf("\rPerforming Flash Erase of length %u at offset 0x%x",
					erase.length, erase.start);
#endif			
			fflush(stdout);

			if(unlock != 0)
			{
				//Unlock the sector first.
#ifdef _DEBUG_MAINTN_
				printf("\rPerforming Flash unlock at offset 0x%x",erase.start);
#endif
				if(ioctl(Fd, MEMUNLOCK, &erase) != 0)
				{
#ifdef _DEBUG_MAINTN_
					perror("\nMTD Unlock failure");
#endif
					close(Fd);
					return 8;
				}
			}

			if (ioctl(Fd,MEMERASE, &erase) != 0)
			{  
#ifdef _DEBUG_MAINTN_
				perror("\nMTD Erase failure");
#endif
				close(Fd);
				return 8;
			}
			erase.start += meminfo.erasesize;
		}

	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : DM_EraseSector
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE  byStartSectorNo : Start sector to erase
 *            int   iSectors        : Sector numbers to erase
 * RETURN   : int :					: SUCCEED:1  FAIL:0
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-01 10:22
 *==========================================================================*/
static int DM_EraseSector(BYTE byStartSectorNo, int iSectors)
{
	int Fd;
	int iStart;					//Start position
	int iCount;					
	int unlock;	
	int iEraseReturn;    

	//check input arguments 
	if (iSectors <= 0 || byStartSectorNo <= 0)
	{
		return ERASE_FAILED;
	}
	
    Fd = open(MTD_NAME, O_RDWR);

	if (Fd == (-1))
	{
		return ERASE_FAILED;
	}
    
	iStart = (byStartSectorNo - 1) * SIZE_PER_SECTOR;

	//if every block is 1024bites
	iCount = iSectors;      
	unlock = 0;       //if any block is unlocked.

	iEraseReturn = DBS_non_region_erase(Fd, iStart, iCount, unlock);

	if(iEraseReturn == 0)
	{
		close(Fd);
		return ERASE_SUCCEED;
	}
	else
	{
		//Failed,try again
		iEraseReturn = DBS_non_region_erase(Fd, iStart, iCount, unlock);
		if(iEraseReturn != 0)
		{
			//Failed, try third time
			iEraseReturn = DBS_non_region_erase(Fd, iStart, iCount, unlock);
			if (iEraseReturn != 0)
			{
				close(Fd);
				return ERASE_FAILED;
			}
		}

        close(Fd);
		return ERASE_SUCCEED;
	}	
}

/*==========================================================================*
 * FUNCTION : BuildTable32
 * PURPOSE  : Create tables which CRC32 will use
 * CALLS    : 
 * CALLED BY: RunCRC32
 * ARGUMENTS: unsigned long   aPoly     : aPoly[in]:multinomial 
 *            unsigned long*  Table_CRC : Table_CRC[in][out]:Table's buff
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : (zoudeqiang1979@tsinghua.org.cn)  DATE: 2003-12-19
 *==========================================================================*/
static void BuildTable32( unsigned long aPoly , unsigned long *Table_CRC )
{
    unsigned long i, j;
    unsigned long iData;
    unsigned long iAccum;

    for ( i = 0; i < 256; i++ )
    {
        iData = ( unsigned long )( i << 24 );
        iAccum = 0;
        for ( j = 0; j < 8; j++ )
        {
            if ( ( iData ^ iAccum ) & 0x80000000 )
                iAccum = ( iAccum << 1 ) ^ aPoly;
            else
                iAccum <<= 1;
            iData <<= 1;
        }
        Table_CRC[i] = iAccum;
    }
}

/*==========================================================================*
 * FUNCTION : RunCRC32
 * PURPOSE  : Execute CRC32 cycling redundance check
 * CALLS    : BuildTable32
 * CALLED BY: All communicate functions and relevant functions
 * ARGUMENTS: const unsigned char *  aData : aData[in]:Data checked
 *            unsigned long          aSize : aSize[in]:Data length 
 *            unsigned long          aPoly : aPoly[in]:multinomial table needs
 * RETURN   : unsigned long :			   : redundance check result
 * COMMENTS : 
 * CREATOR  : (zoudeqiang1979@tsinghua.org.cn)  DATE: 2003-12-19
 *==========================================================================*/
static unsigned long RunCRC32( const unsigned char * aData, 
					    unsigned long aSize, 
					    unsigned long aPoly )
{
    unsigned long Table_CRC[256]; // CRC table
    unsigned long i;
    unsigned long iAccum = 0;

    BuildTable32( aPoly, Table_CRC );
    
    for ( i = 0; i < aSize; i++ )
        iAccum = ( iAccum << 8 ) ^ Table_CRC[( iAccum >> 24 ) ^ *aData++];

    return iAccum;
}

/*==========================================================================*
 * FUNCTION : DBS_AnalyseFrame
 * PURPOSE  : Check where the current data is from.			  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const unsigned char  *pFrame : 
 *            int                  iLen    : 
 * RETURN   : BOOL :			   TRUE:the frame is the format which powerkit 
 *										send data;
 *								   FALSE:Debug service can't identify the frame
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-29 14:36
 *==========================================================================*/
BOOL DBS_AnalyseFrame(const unsigned char *pFrame, int iLen)
{
	DATA_FRAME *pDataFrame;
	int i, iFrameCheck;
	int iFrameLen;

	if (iLen < MIN_FRAME_LENGTH)
	{ 
#ifdef _DEBUG_MAINTN_
		printf("The fram iLen is %d, "
			"it shouldn't be standard frame from powerkit.\n", iLen);
#endif
		return FALSE;
	}
	else
	{
		//Find the start postion
		for (i = 0; i < iLen; i++)
		{
			pDataFrame = (DATA_FRAME *)(&pFrame[i]);
			if (pDataFrame->bySOH == 0x7e)
			{	
				break;															
			}
		}

		//It's not a frame from powerkit
		iFrameLen = iLen - i;
		if (iFrameLen < MIN_FRAME_LENGTH)
		{
			return FALSE;
		}

		//Call CheckFrame to Check frame is right or not
		iFrameCheck = CheckFrame(pFrame + i);

#ifdef _DEBUG_MAINTN_
			printf("iFrameCheck is %d\n", iFrameCheck);
#endif //_DEBUG_MAINTN_

		if (iFrameCheck != 0) //Frame is wrong
		{	
			return FALSE;
		}

		//There is a client connecting
		if (pDataFrame->byCID2 != CONNECT_INFO_ID)
		{
#ifdef _DEBUG_MAINTN_			
			printf("pDataFrame->byCID2 is %d, It's not a frame from powerkit\n",
				pDataFrame->byCID2);
#endif
            return FALSE;
		}

	return TRUE;
	}
}

/*==========================================================================*
 * FUNCTION : DBS_Ini_Report_Port
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   BOOL : TRUE:Succeed to initiate report port
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-29 15:37
 *==========================================================================*/
static BOOL DBS_Ini_Report_Port(void)
{
	BOOL	bResult;	
	DBS_PORT_INFO		*pPortInfo;
	int		iCtrl;

	//////////////////////////////////////////
	pPortInfo  =  g_pDbsData->g_pPortInfo;
	if (pPortInfo == NULL)
	{
#ifdef _DEBUG_MAINTN_
		printf("Fail to open serial port for pPortInfo == NULL.\n");
#endif
		return FALSE;
	}

	bResult	= FALSE;

	//Set first value to variable
	//If current program is debug service interface, it it true
	if (g_pDbsData->g_iRunningMode == DSI_RUNNING_MODE)
	{
		for(iCtrl = 0; iCtrl < g_pDbsData->g_iComPortNums; iCtrl++)
		{
			if (pPortInfo->iPortID == REPORT_PORT_ID)
			{
				break;
			}
			pPortInfo++;
		}

		if (iCtrl == g_pDbsData->g_iComPortNums)
		{
#ifdef _DEBUG_MAINTN_
			printf("Fail to open serial port for iCtrl == g_pDbsData->g_iComPortNums.\n");
#endif
			return FALSE;
		}

		bResult = Ini_OnePort(pPortInfo, &(g_pDbsData->g_hSeriPort));
		//It must succeed.Service can be started.
		if (!bResult)
		{
			AppLogOut("DBS_INI_PORT", APP_LOG_ERROR, 
				"In debug service,Fail to open report serial port.");
			return FALSE;
		}
	}
	else
	{
#ifdef _DEBUG_MAINTN_
		printf("Fail to open serial port for DSP_RUNNING_MODE.\n");
#endif
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : DBS_Stop_Report_Port
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-29 15:48
 *==========================================================================*/
static BOOL DBS_Stop_Report_Port(void)
{
    if (g_pDbsData->g_hSeriPort)
	{
		if (g_pDbsData->g_hSubSeriPort)
		{
			CommClose(g_pDbsData->g_hSubSeriPort);
			g_pDbsData->g_hSubSeriPort = NULL;
		}
		CommClose(g_pDbsData->g_hSeriPort);
		g_pDbsData->g_hSeriPort = NULL;
	}

#ifdef _DEBUG_MAINTN_
	printf("Clear current serial report port connect resource correctly.\n");
#endif

	return TRUE;
}





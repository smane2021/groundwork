/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : debug_maintn.h
 *  CREATOR  : LIXIDONG                 DATE: 2004-09-24 10:56
 *  VERSION  : V1.00
 *  PURPOSE  : This file will be used in communicating with powerkit 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __DEBUG_MAINTN_H
#define __DEBUG_MAINTN_H

/*DBS Data: In the ServiceMain function, a TYPE_DBS_DATA 
  variant shall be defined, which shall be initialized by the DBS_Init. */
struct tagDbsPortInfo
{
	int	  iPortID;		
	int	  iStdPortTypeID;	/* the type of the port, defined by STDPORT_INFO */
	char  szDeviceDesp[16];	    /* used by the system to access the port,
							       eg:COM1(/dev/ttyS1), COM2(/dev/ttySA1), ��NET1(eth0) */
	int	  iDisconnDelay;	/* -1:not disconnect, others: the delay time, counted by seconds*/
	int   iTimeout;			/* Unit: millisecond */
	char  szSetting[32];	/* Parameter setting for the driver */	
};
typedef struct tagDbsPortInfo DBS_PORT_INFO;
//all port info:SamplerPortI,SamplerPortII,EthnetPort;ReportPort;

//file information is used to store up_load file info
struct tagFILEINFORECORD
{
    char szFileName[128];
	char szFilePath[128];
	BOOL bValidFlag;
};
typedef struct tagFILEINFORECORD FILE_INFO_REC;

struct tagDBS_DATA
{
	/*Handle of General Controller Service thread*/
	int					g_iRunningMode;   //
	BOOL				g_bDBS_RunFlag;
	BOOL				bHaveClient;

	DBS_PORT_INFO		*g_pPortInfo;
	int					g_iComPortNums;
	HANDLE				g_hPort;          //Comm main handle
	HANDLE				g_hEthPort;
	HANDLE				g_hSubEthPort;	 //Used to storage net port handle
	HANDLE				g_hSeriPort;	  
	HANDLE				g_hSubSeriPort;   //Used to storage serial port handle
	HANDLE				g_hSamplerPort1;
	HANDLE				g_hSamplerPort2;

	//Thread handle definition
	HANDLE				g_hDbsMainHandle;
	HANDLE				g_hRecSend;    //Mr.Mao said
	//pthread_t			g_hRecSend_t;  //Old useful method,it should be best method
    BOOL				bEndUnzip;     //Used to unzip acu.tar.gz

	//This variable is used to count time delay, if it >= 300
	//System will be reset
	//int				g_iCounter;   //Modify to tmLastTime
	time_t				tmLastTime;
	time_t				tmCurrentTime;
	//User definition
	BOOL				g_bValidUser;
	BOOL				g_bRebootCmd;
	BOOL				g_bMainToDsp;
	BOOL				g_bEndCurrConnect;
	BOOL				g_bHaveSeriClient;

	//Process file 
	FILE_INFO_REC		*g_pFileInfo;
	int					g_iFileNum;

	//Restart EEM times
	int					g_iRestartHLMSTimes;
	int					g_iReIniSeriPortTimes;
};

typedef struct tagDBS_DATA MOD_DBS_DATA;

//#pragma pack(1)
//////////////////////////////////////////////////////////////////////
//Frame Struct definition to Powerkit
struct tagDataFrame
{
	BYTE    		bySOH;					//0x7e
	BYTE			bySEQ;					//Package serial No.
	BYTE    		byACUAddr;				//ACU address 0-254
    BYTE    		byCID1;					//
	BYTE    		byCID2;					//Command 
	BYTE    		byFlag;					//Transfer flag
	unsigned short  iDataLength;			//Data Length
	DWORD    		dwCRC32;				//Check Number
    BYTE      		byEOF;					//0x0D
	    	 
 };

typedef struct tagDataFrame DATA_FRAME;

//Frame Struct definition to low level device
struct tagDataFrameLow
{
	BYTE    		bySOH;					//0x7e
	BYTE			byVer;					//Package serial No.
	BYTE    		byAddr;					//ACU address 0-254
    BYTE    		byCID1;					//
	BYTE    		byCID2;					//Command 
	unsigned short  iDataLength;			//Data Length
	DWORD    		dwCRC32;				//Check Number
    BYTE      		byEOF;					//0x0D
	    	 
 };
typedef struct tagDataFrameLow DATA_FRAME_L;

struct tagUsedSamplerInfo
{
	DWORD    		dwSamplerID;				//Sampler ID
	char    		szSamplerName[32];			//Sampler Name
	BYTE    		byPortNo;					//Serial No.
	BYTE			bySamplerAddr;				//Samper address 1-254
	BYTE			byCommStatus;				//Communication status
	
 };
typedef struct tagUsedSamplerInfo USED_SAMPLER_INFO;

struct tagUsedEquipInfo
{
	char    		szEquipName[32];			//Equipment Name
	int	    		iEquipID;					//Equip ID 
 };
typedef struct tagUsedEquipInfo USED_EQUIP_INFO;

struct tagAcquiSigVal
{
	char	szSigName[32];				//Signal name
	BYTE	byValueType;				//Signal type
	VAR_VALUE varValue;					//Value
	char	szUnit[5];					//if it is (mv/deg.C) use "8" replace it
};
typedef struct tagAcquiSigVal ACQUI_SIG_VAL;
//#pragma pack()


/////////////////////////////////////////////////////////////////////
//Function definition
/*==========================================================================*
 * FUNCTION : DBS_Pro_Ini
 * PURPOSE  : Initiate different ports and start listenning thread
 * CALLS    : 
 * CALLED BY:   Debug service program main function
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  :	LIXIDONG                 DATE: 2004-10-05 15:15
 *==========================================================================*/
void DBS_Pro_Ini(void);

/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : The interface function for ACU main module
 * CALLS    : DBS_Init
 * CALLED BY: Main Module of ACU
 * ARGUMENTS: SERVICE_ARGUMENTS  *pArgs : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-28 17:06
 *==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs);

/*==========================================================================*
 * FUNCTION : DBS_IniDebugPro
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-10 19:17
 *==========================================================================*/
BOOL DBS_IniDebugPro(void);

/*==========================================================================*
 * FUNCTION : DBS_ExitDebugService
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-10 19:45
 *==========================================================================*/
void DBS_ExitDebugService(void);

/*==========================================================================*
 * FUNCTION : DBS_AnalyseFrame
 * PURPOSE  : Check where the current data is from.			  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const unsigned char  *pFrame : 
 *            int                  iLen    : 
 * RETURN   : BOOL :			   TRUE:the frame is the format which powerkit 
 *										send data;
 *								   FALSE:Debug service can't identify the frame
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-29 14:36
 *==========================================================================*/
BOOL DBS_AnalyseFrame(const unsigned char *pFrame, int iLen);

#endif //_DEBUG_MAINTN_H


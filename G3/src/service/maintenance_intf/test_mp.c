/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : test_data_mng.c
 *  CREATOR  : LIXIDONG                 DATE: 2004-09-27 10:23
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "debug_maintn.h"


//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO  g_SiteInfo;

//need to be defined in Miscellaneous Function Module
TIME_SRV_INFO g_sTimeSrvInfo;

//need to be defined in equip management Module
BOOL Equip_Control(int nEquipID, int nSigType, int nSigID, 
				   VAR_VALUE varCtrlValue)
{
	UNUSED(nEquipID);
	UNUSED(nSigType);
	UNUSED(nSigID);
	UNUSED(varCtrlValue);

	return TRUE;
}

//need to be defined in Miscellaneous Function Module
int UpdateACUTime (time_t* pTimeRet)
{
	return stime(pTimeRet);
}

char g_szACUConfigDir[MAX_FILE_PATH];

SERVICE_MANAGER		g_ServiceManager;
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//Start my testing coding

void TestRightVerify(void)
{
	/*
	char szBuffer[1024];
	unsigned long dwCRC32;
	int iLen;
	USER_INFO stUserInfo;
	strcpy(stUserInfo.szUserName, "admin");
	strcpy(stUserInfo.szPassword, "aculinux");
	stUserInfo.byRightLevel = 4;

	DATA_FRAME stDFrame;
	stDFrame.bySOH		= 0x7e;
	stDFrame.bySEQ		= 0x01;
	stDFrame.byACUAddr	= 0x00;
	stDFrame.byCID1		= 0xD5;
	stDFrame.byCID2		= 0x01;
	stDFrame.byFlag		= 0;
	stDFrame.iDataLength = sizeof(stUserInfo);
	memcpy((void *)szBuffer, (void *)&stDFrame, 8);
	memcpy((void *)(szBuffer + 8), (void *)&stUserInfo, 
		sizeof(stUserInfo));
	
	iLen = 7 + sizeof(stUserInfo);
	dwCRC32 = RunCRC32((const unsigned char *)(szBuffer + 1), 
			(unsigned long)iLen, (unsigned long)CRC32_DEFAULT);
	*(unsigned long *)(szBuffer + iLen + 1) = dwCRC32;
	*(unsigned char *)(szBuffer + iLen + 5) = 0x0D;

	iBytesRead = CommWrite (g_hPort, 
							   szBuffer, 
						       iLen + 6);*/
	return;
}

void TestModifyIPAddr(void)
{
	/*
	int iLen;
	int iBytesRead;
	DWORD dwCRC32;
	char szIP[64] = "120.4.5.6:5.6.7.8:456.8.9.0";
	char szBuffer[128];

	DATA_FRAME stDFrame;
	stDFrame.bySOH		= 0x7e;
	stDFrame.bySEQ		= 0x01;
	stDFrame.byACUAddr	= 0x00;
	stDFrame.byCID1		= 0xD5;
	stDFrame.byCID2		= 0x06;
	stDFrame.byFlag		= 0;
	stDFrame.iDataLength = sizeof(szIP);


	iLen = 8;
	memcpy((void *)szBuffer, (void *)&stDFrame, 8);
	memcpy((void *)(szBuffer + iLen), szIP, sizeof(szIP));

	iLen = iLen + sizeof(szIP) - FRAME_HEAD;
	
	dwCRC32 = RunCRC32((const unsigned char *)szBuffer + 1, 
			(unsigned long)iLen, (unsigned long)CRC32_DEFAULT);

	iLen = iLen + FRAME_HEAD;
	*((DWORD *)(szBuffer + iLen)) = dwCRC32;
	iLen = iLen + CRC32_BYTES;
	*((BYTE *)(szBuffer + iLen)) = 0x0D;
	iLen = iLen + FRAME_END;

	iBytesRead = CommWrite (g_hPort, 
							   szBuffer, 
						       iLen);*/
	return;
}

void UploadDataTest(void)
{
	/*
	char szBuffer[1024];
	char szFileName[32];
	unsigned long dwCRC32;
	int iLen, iBytesRead;
	unsigned short iBlockNo;

	DATA_FRAME stDFrame;
	
	strcpy(szFileName, "testdata99.txt");

	stDFrame.bySOH		= 0x7e;
	stDFrame.bySEQ		= 0x01;
	stDFrame.byACUAddr	= 0x00;
	stDFrame.byCID1		= 0xD5;
	stDFrame.byCID2		= 0x0E;
	stDFrame.byFlag		= 0;
	stDFrame.iDataLength = sizeof(short) + strlen(szFileName);
	iBlockNo = 0;

	memcpy((void *)szBuffer, (void *)&stDFrame, 8);
	memcpy((void *)(szBuffer + 8), (void *)&iBlockNo, 
		sizeof(iBlockNo));
	memcpy((void *)(szBuffer + 10), (void *)szFileName, 
		strlen(szFileName));
	
	iLen = 7 + strlen(szFileName) + sizeof(iBlockNo);

	dwCRC32 = RunCRC32((const unsigned char *)(szBuffer + 1), 
			(unsigned long)iLen, (unsigned long)CRC32_DEFAULT);
	*(unsigned long *)(szBuffer + iLen + 1) = dwCRC32;
	*(unsigned char *)(szBuffer + iLen + 5) = 0x0D;

	iBytesRead = CommWrite (g_hPort, 
							   szBuffer, 
						       iLen + 6);*/
	return;
}

char *GetStandardPortDriverName(IN int nStdPortType)
{
	const char *pszStdDriver[] =
	{
		"xxx.so",
		"comm_net_tcpip.so",			
		"comm_std_serial.so",
		"comm_std_serial.so",
		"comm_hayes_modem.so",		
		"comm_acu_485.so"
	};

	return (char *)pszStdDriver[nStdPortType];
}

/*==========================================================================*
 * FUNCTION : test_mp     
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-19 12:57
 *==========================================================================*/
//int main(int argc, char *argv[])
int main(void)
{
	int iExitResult;

	DBS_Pro_Ini();

	return 0;
}

/*
void TestDirName(void)
{
	char szCurrDir[128];
	char szDIR[128];
	char * pszDir;
	DIR * pDir;
	struct dirent * pdirent;

	pszDir = getcwd(szCurrDir, 128);
	if (pszDir == NULL)
	{
		return;
	}
	printf("szCurrDir is %s\n", szCurrDir);
	pDir = opendir(szCurrDir);

	while ((pdirent = readdir(pDir)) != NULL) 
	{ 
		if(pdirent->d_ino == 0) 
		{
			continue;
		}
		strcpy(szDIR, pszDir); 
		strcat(szDIR, "/"); 
		strcat(szDIR, pdirent->d_name); 
		printf("pdirent->d_name is %s,value is %s\n",szDIR, pdirent->d_name);
	}
	closedir(pDir);
	
	return;

}
*/




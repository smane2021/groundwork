/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : qt_fifo.h
*  CREATOR  : Jimmy Wu              DATE: 2013-02-19
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef _QT_FIFO_H
#define _QT_FIFO_H



#include "pubfunc.h"
#include "public.h"
#include "cfg_model.h"

#define CFG_LOADER			"CFG LDR"
#define SPLITTER		 ('\t')
#define DISP_INFORMATION	"[NORMALSTRU]"
#define ICOINFO_LCD		"[ICOINFO]"
#define MODULE_INFO		"[MODULEINFO]"
#define SETTING_INFO		"[SETTINGINFO]"
#define DISTRIB_INFO		"[NORMALVALUE]"
#define TEMP_INFO		"[GROUPINFO]"
#define BATTINFO_LCD		"[BATTERYINFO]"

int StartWebCommunicate(int *iQuitCommand);
DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs);

#define EQIPID_RECTGRP_TIMEOUT		12000	//changed by Frank Wu,20140102,3/3, for the rectifier setting menu in LCD
#define INTERVALTIME	200
//changed by Frank Wu,20140110,1/1, for LCD sometimes restart  at the starting
//#define mBEATNUM	20000/INTERVALTIME
#define mBEATNUM	120000/INTERVALTIME
/*max len of fifo file name*/
#define FIFO_NAME_LEN					32
//#define WEB_SUPPORT_XML
//#ifdef WEB_SUPPORT_XML
#define XML_APP_LOG_COMM_NAME		"DATA_EXCHANGE"
#define XML_SIGNALVALUE_FAIL		2
#define USER_DEF_LCDCFG				"private/lcd/lcd_private.cfg"
#define FILENAME_LCD_ROTATION		"/etc/lcd_rotation"		//changed by Frank Wu,20131228,3/3, for lcd display mode


#define MAX_NUM1_RECT					100
#define MAX_NUM2_RECT					60
#define MAX_NUM_MPPT					16
#define MAX_NUM1_CONV					48
#define MAX_NUM2_CONV					12
#define	MAX_NUM_CONVERT					(MAX_NUM1_CONV + MAX_NUM2_CONV)
#define MAXNUM_SMDUE					8

enum
{
	XML_CHANGEVALUE = 0,
	XML_CHANGERELAY,
	XML_CHANGELEVEL,
	XML_GETPASS,
	XML_GETCLASS
};

#define VALUE_STATE_IS_INVALID	    0
#define SAMP_INVALID			(-9999)
#define EQUIP_EXIST			0
#define TIMEHOUR		3600
#define TIMESEC_WEEK		3600*24*7

#define mSINGLE			1

#define ACSingle		0

#define MAX_CONV_GRP1	48
//Frank Wu,20150318, for SoNick Battery
//#define RS485NUM	66
#define RS485NUM	PRODUCT_INFO_485PRODUCTINFO_MAX
#define I2CNUM		8
#define SMDUNUM		8
#define SMDUHNUM	8
#define SMDUPNUM1	8
#define SMDUPNUM2	12
#define SMTEMPNUM	8
#define LIBATTNUM	60
#define DCEMNUM		8

#define CMD_READ	0
#define CMD_SET		1
#define CMD_CTRL	2
#define CMD_REBOOT	4
#define CMD_RESETPASSWORD 5 //hhy
#define DATA_LEN	(64 * 1024)

#define NORMAL_STRU_SCREENTYPE		0
#define ICO_SCREENTYPE					1
#define MODULE_SCREENTYPE				2
#define NORMAL_VALUE_SCREENTYPE		3
#define BATT_SCREENTYPE				4
#define SETTING_SCREENTYPE				5
#define SPECIAL_SCREENTYPE				6
#define NORMAL_VALUEGRP_SCREENTYPE	7

#define INIT_DATA_SCREEN	0x0ff
#define MAIN_ICO_SCREEN		0x000
#define SELFINV_SCREEN		0x001
#define PWD_SCREEN		0x003
#define RECTINV_SCREEN		0x004
#define SRECTINV_SCREEN		0x005
#define SMI2CINV_SCREEN		0x006
#define RS485INV_SCREEN		0x007
#define CONVINV_SCREEN		0x008
#define LIBATTINV_SCREEN	0x009
#define MPPTINV_SCREEN		0x00A
#define SMDUEINV_SCREEN		0x00B

#define ACTIVEALMNUM_SCREEN	0x100
#define ACTIVEALM_SCREEN	0x101
#define HISALMNUM_SCREEN	0x102
#define HISALM_SCREEN		0x103

#define SETTING_SCREEN		0x200
#define WIZARD_SITENAME_SCREEN	0x201
//changed by Frank Wu,6/6,20140221 for TR194 which No battery size settings in Installation Wizard---start---
//#define WIZARD_BATT_SCREEN	0x202
//#define WIZARD_ECO_SCREEN	0x203
//#define WIZARD_ALARM_SCREEN	0x204
//#define WIZARD_SYS_SCREEN	0x205
#define WIZARD_COMMON_SCREEN		0x202
#define WIZARD_BATT_SCREEN			0x203
#define WIZARD_BATT_CAPACITY_SCREEN	0x204
#define WIZARD_ECO_SCREEN			0x205
#define WIZARD_ALARM_SCREEN			0x206
#define WIZARD_COMMUNICATE_SCREEN	0x207


#define SLAVE_SETTING_SCREEN		0x271
#define MAINTENANCE_SCREEN			0x272
#define ENERGY_SAVING_SCREEN		0x273
#define ALARM_SETTINGS_SCREEN		0x274
#define BAT_SETTINGS_BASIC_SCREEN	0x275
#define BAT_SETTINGS_CHARGE_SCREEN	0x276
#define BAT_SETTINGS_TEST_SCREEN	0x277
#define BAT_SETTINGS_COMP_SCREEN	0x278
#define BAT_SETTINGS_BAT_1_SCREEN	0x279
#define BAT_SETTINGS_BAT_2_SCREEN	0x27A
#define AC_SETTINGS_SCREEN			0x27B
#define LVD_SETTINGS_SCREEN			0x27C
#define RECT_SETTINGS_SCREEN		0x27D
#define SYS_SETTINGS_SCREEN			0x27E
#define COMM_SETTINGS_SCREEN		0x27F
#define OTHER_SETTINGS_SCREEN		0x280
#define BAT_SETTING_GROUP_SCREEN	0x281

//changed by Frank Wu,6/6,20140221 for TR194 which No battery size settings in Installation Wizard---end---

#define ACVOLT_ICO_SCREEN	0x300

#define MODULENUM_SCREEN	0x400
#define RECT_SCREEN		0x401
#define MPPT_SCREEN		0x402
#define S1RECT_SCREEN		0x403
#define S2RECT_SCREEN		0x404
#define S3RECT_SCREEN		0x405
#define SOURCE_INFO_SCREEN 	0x410 //Added by wangminghui

#define DCVOLT_ICO_SCREEN	0x500
#define LTREND_ICO_SCREEN	0x501
#define AMBT_ICO_SCREEN		0x502
#define BLOAD_SCREEN		0x503
#define AMBT_DATA_SCTEEN	0x504
#define AMBT_TREND_SCREEN	0x505
#define CONVT_SCREEN		0x507

#define BATTCAP_ICO_SCREEN	0x600
#define COMPT_ICO_SCREEN	0x601
#define BATT_DATA_SCREEN	0x602
#define BATTEMP_DATA_SCREEN	0x603
#define BATTEMP_TREND_SCREEN	0x604

#define INV_SELF_SCREEN		0x700
#define INV_RT_SCREEN		0x701
#define INV_SMDU_SCREEN		0x702

#define SPECIALID			-1
//changed by Frank Wu,20140915,1/N/N, for Adding conveterter and rectifier total current and voltage to the local display
#define SPECIALID2			-2
#define SYSUSED				1
#define SIGSYSOA				29
#define SIGSYSMA			30
#define SIGSYSCA				31
#define EQUIPTYPE_BATT_MIN	301
#define EQUIPTYPE_BATT_MAX	399
#define EQIPID_SYS		1
#define EQIPID_RECTGRP		2
#define EQIPID_SMDUGRP		106
#define EQIPID_BATTGRP		115
#define EQIPID_MPPTGRP		1350

#define EQIPID_RECT1		3
#define EQIPID_SMDU		107
#define EQIPID_CONVGRP		210
#define EQIPID_CONV1		211
#define EQIPID_DGGRP		261
#define EQIPID_SLAVE1RECT	339
//changed by Frank Wu,1/4,20140317, for twinkle of the green led of slave rectifier
#define EQIPID_SLAVE2RECT	439
#define EQIPID_SLAVE3RECT	539
#define EQIPID_SLAVERECTGRP1	639
#define EQIPID_SLAVERECTGRP2	640
#define EQIPID_SLAVERECTGRP3	641
#define EQIPID_SMDUP1		643
#define EQIPID_SMDUP2		1870
#define EQIPID_SMTEMP		709
#define EQIPID_MPPTGRP		1350
#define EQIPID_MPPT		1351
#define EQIPID_CONV2		1400
#define EQIPID_RECT2		1601
#define EQIPID_SMDUH		1702
#define EQIPID_SMDUEGRP	1800
#define EQIPID_SMDUE_0		1801

//����LCD��web�������
#define SIGID_CLOSE_ALARM				(500)
#define SIGID_ALMVOICE_ACTIVATE 		(251)
#define ALM_VOICE_CLOSE_YES			(0)
#define ALM_VOICE_CLOSE_NO			(1)
#define ALM_VOICE_ACTIVATE_YES		(1)
#define ALM_VOICE_ACTIVATE_NO		(0)
#define ALM_VOICE_ENABLE				(23)

#define SIGID_SYSLOAD		2

#define SIGID_SRECTNUM		2
#define SIGID_SYSUSED		3

#define SIGID_SLAVE_RATEDCURR	53

#define SIGID_MPPT_RATEDCURR	34
#define SIGID_LANG		22
#define SIGID_MODULENUM		8
#define SIGID_ACPHASE		24
#define SIGID_RATEDCURR		35
#define SIGID_BARCODE1		37
#define SIGID_BARCODE2		38

#define SIGID_SNL		8
#define SIGID_VER		27
#define SIGID_RESPOND		99
#define SIGID_STATE		100
#define SIGID_AMBTEMP		69
#define SIGID_BATTTEMP		36

#define SIGID_SYSLOAD		2
#define SIGID_REXPAN		180

#define SIGID_FLOAT48		16
#define SIGID_FLOAT24		116
#define SIGID_BOOST48		17
#define SIGID_BOOST24		117
#define SIGID_SMDUENUM	1

#define CTRL_MODULE_LED		3
#define CTRL_GRP_LED		5
#define CTRL_RECTCOMMFAIL	14
#define CTRL_RECTLOST		8
#define CTRL_BADBATT		3
#define CTRL_TESTFAIL		10
#define CTRL_TEST_START		4
#define CTRL_TEST_STOP		5

#define CTRLLED_RECT		0
#define CTRLLED_MPPT		1
#define CTRLLED_CONV		2
#define CTRLLED_S1RECT		3
#define CTRLLED_S2RECT		4
#define CTRLLED_S3RECT		5
#define CTRLLED_SMDU		6
#define CTRLLED_SMDUP		7
#define CTRLLED_SMDUH		8

#define DEL_RECTCOMMFAIL	0
#define DEL_RECTLOST		1
#define DEL_BADBATT		2
#define DEL_TESTFAIL		3

#define TEST_START		0
#define TEST_STOP		1

#define LEN_MAC			32
//changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
#define LEN_IPV6		40	//"09AF: 19AF: 29AF: 39AF: 49AF: 59AF: 69AF: 79AF", len=4*8 + 7 + 1=40
#define MAXLEN_SIG		3
#define MAXLEN_ENUM		10
#define MAXLEN_NAME		20
#define MAXLEN_NUMBER		16
#define MAXLEN_UNIT		10//changed by Frank Wu,20131231
#define MAXLEN_SET		15
#define MAXLEN_PROINFO		16
//Frank Wu,20151103, for Narada BMS
//#define MAXLEN_VER		4
#define MAXLEN_VER		MAXLEN_PROINFO
//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
#define MAX_DISP_ARGS_NUM	6

#define MAXNUM_SEL		100
#define MAXNUM_NORVALUE		10
//changed by Frank Wu,1/4,20140225,for TR160 which Missing alarm levels on temperature bar
#define MAXNUM_NORLEVEL		3
//Frank Wu,20150318, for SoNick Battery
//#define MAXNUM_BATT		120
#define MAXNUM_BATT			(120 + 32)
#define MAXNUM_BATINFO		3
#define MAXNUM_MODULE		180
#define MAXNUM_BRANCH		750
#define MAXNUM_SIG		3
#define MAXNUM_ACTALM		200
#define MAXNUM_HISALM		500
#define MAXNUM_HELP		64
#define MAXNUM_TREND		85//84
#define MAXNUM_TRENDT		84
#define MODINFO_MAX		10
#define MAXNUM_TRENDDATA	(MAXNUM_TREND - 1) * 2

#define LIMNUM			6

#define SITENAME_SETTING	0
#define TIME_SETTING		1
#define IP_SETTING		2
#define MASK_SETTING		3
#define GATEWAY_SETTING		4
//#define CLEAR_SETTING		5
//#define BATTTEST_SETTING	6
#define DEFAULTCFG_SETTING	7
#define DHCPCLIENT_SETTING	8
#define AUTOCFG_SETTING		9
//changed by Frank Wu,20131228,5/6,for support settings of part EEM protocol in LCD ---start---
#define COMM_PROTOCOL_SETTING		(AUTOCFG_SETTING + 1)	//10
#define YDN23_ADDR_SETTING			(COMM_PROTOCOL_SETTING + 1)	//11
#define YDN23_METHOD_SETTING		(YDN23_ADDR_SETTING + 1)	//12
#define YDN23_BAUDRATE_SETTING		(YDN23_METHOD_SETTING + 1)	//13
#define MODBUS_ADDR_SETTING			(YDN23_BAUDRATE_SETTING + 1)	//14
#define MODBUS_METHOD_SETTING		(MODBUS_ADDR_SETTING + 1)	//15
#define MODBUS_BAUDRATE_SETTING		(MODBUS_METHOD_SETTING + 1)	//16
//changed by Frank Wu,20131228,5/6,for support settings of part EEM protocol in LCD ---end---
//changed by Frank Wu,20140108
#define ONLY_DATE_SETTING			(MODBUS_BAUDRATE_SETTING + 1)//17
#define ONLY_TIME_SETTING			(ONLY_DATE_SETTING + 1)//18
//changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
#define IPV6_IP_SETTING_1			(ONLY_TIME_SETTING + 1)		//19
#define IPV6_IP_SETTING_2			(IPV6_IP_SETTING_1 + 1)		//20
#define IPV6_MASK_SETTING			(IPV6_IP_SETTING_2 + 1)		//21
#define IPV6_GATEWAY_SETTING_1		(IPV6_MASK_SETTING + 1)		//22
#define IPV6_GATEWAY_SETTING_2		(IPV6_GATEWAY_SETTING_1 + 1)	//23
#define IPV6_DHCPCLIENT_SETTING		(IPV6_GATEWAY_SETTING_2 + 1)	//24
#define IPV6_IP_SETTING_ROT90_1		(IPV6_DHCPCLIENT_SETTING + 1)	//25
#define IPV6_IP_SETTING_ROT90_2		(IPV6_IP_SETTING_ROT90_1 + 1)	//26
#define IPV6_IP_SETTING_ROT90_3		(IPV6_IP_SETTING_ROT90_2 + 1)	//27
#define IPV6_GATEWAY_SETTING_ROT90_1		(IPV6_IP_SETTING_ROT90_3 + 1)		//28
#define IPV6_GATEWAY_SETTING_ROT90_2		(IPV6_GATEWAY_SETTING_ROT90_1 + 1)		//29
#define IPV6_GATEWAY_SETTING_ROT90_3		(IPV6_GATEWAY_SETTING_ROT90_2 + 1)		//30
#define IP_FRONT_PORT						(IPV6_GATEWAY_SETTING_ROT90_3 + 1)		//31


#define CALMODE_NORMAL		0
#define CALMODE_ACVOLT		1



/******************************************Add start***************************************************/
//Added by wang minghui
//Equip ID
#define EQP_ID_EIB_START				(197)		
#define EQP_ID_EIB_END					(200)

#define EQP_ID_SMDU_UNIT_START		(107)
#define EQP_ID_SMDU_UNIT_END			(114)

//Signal ID for display
//SMDU
#define SIG_ID_SMDU_SHUNT1_CURRENT			(31)
#define SIG_ID_SMDU_SHUNT2_CURRENT			(32)
#define SIG_ID_SMDU_SHUNT3_CURRENT			(33)
#define SIG_ID_SMDU_SHUNT4_CURRENT			(34)
#define SIG_ID_SMDU_SHUNT5_CURRENT			(35)
//EIB
#define SIG_ID_EIB_SHUNT1_CURRENT				(64)
#define SIG_ID_EIB_SHUNT2_CURRENT				(65)
#define SIG_ID_EIB_SHUNT3_CURRENT				(66)

//Signal ID for judgement
//SMDU
#define SIG_ID_SMDU_SHUNT1_IS_SOURCE			(16)
#define SIG_ID_SMDU_SHUNT2_IS_SOURCE			(17)
#define SIG_ID_SMDU_SHUNT3_IS_SOURCE			(18)
#define SIG_ID_SMDU_SHUNT4_IS_SOURCE			(19)
#define SIG_ID_SMDU_SHUNT5_IS_SOURCE			(20)
//EIB
#define SIG_ID_EIB_SHUNT1_IS_SOURCE			(16)
#define SIG_ID_EIB_SHUNT2_IS_SOURCE			(17)
#define SIG_ID_EIB_SHUNT3_IS_SOURCE			(18)

#define ENUM_SHUNT_SET_AS_SOURCE				(4)


#define DEG_F_MIN_VALUE						(-13)
#define DEG_F_MAX_VALUE						(167)

#define DEG_C_MIN_VALUE						(-25)
#define DEG_C_MAX_VALUE						(75)


//Temp. sensor equip ID
#define EQUIP_ID_IB2_1					(202)
#define EQUIP_ID_IB2_2					(203)
#define EQUIP_ID_EIB1					(197)
#define EQUIP_ID_EIB2					(198)
#define EQUIP_ID_SMTEMP_1				(709)
#define EQUIP_ID_SMTEMP_2				(710)
#define EQUIP_ID_SMTEMP_3				(711)
#define EQUIP_ID_SMTEMP_4				(712)
#define EQUIP_ID_SMTEMP_5				(713)
#define EQUIP_ID_SMTEMP_6				(714)
#define EQUIP_ID_SMTEMP_7				(715)
#define EQUIP_ID_SMTEMP_8				(716)
#define EQUIP_ID_SMDUE1                  (1801)
#define EQUIP_ID_SMDUE2                  (1802)

#define TEMP_SENSOR_EXIST				1
#define TEMP_SENSOR_NOT_EXIST			0
#define MAX_TEMPSENSOR_NUM			(14)//changed by hhy

/*****************************************Add end****************************************************/

const char* g_QtCmd = "/semCmd";
const char* g_SvAck = "/semAck";
const char* g_NewAlm = "/SemNewAlm";
const char* g_QtBeat = "/semBeat";

//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
#define QT_EQUIP_OFFSET					10000
#define QT_STYLE_OFFSET					1000
#define QT_SUB_FUN_OFFSET				10000
#define QT_SUB_FUN_PARA_OFFSET			1000000

#define QT_MERGE_SIGTYPE_ID(iArgEquip, iArgType, iArgStyle) \
			((iArgEquip)*QT_EQUIP_OFFSET + (iArgStyle)*QT_STYLE_OFFSET + (iArgType))

#define QT_SPLIT_SIGTYPE_ID(iArgMergeSigTypeID, iArgEquip, iArgType, iArgStyle) \
			((iArgEquip) = (iArgMergeSigTypeID)/QT_EQUIP_OFFSET, \
			(iArgStyle) = ((iArgMergeSigTypeID)%QT_EQUIP_OFFSET)/QT_STYLE_OFFSET, \
			(iArgType) = (iArgMergeSigTypeID)%QT_STYLE_OFFSET)

#define QT_MERGE_SIG_ID(iArgSubFunPara, iArgSubFunID, iArgSigID) \
			((iArgSubFunPara)*QT_SUB_FUN_PARA_OFFSET + (iArgSubFunID)*QT_SUB_FUN_OFFSET + (iArgSigID))

#define QT_SPLIT_SIG_ID(iArgMergeSigID, iArgSubFunPara, iArgSubFunID, iArgSigID) \
			((iArgSubFunPara) = (iArgMergeSigID)/QT_SUB_FUN_PARA_OFFSET, \
			(iArgSubFunID) = ((iArgMergeSigID)%QT_SUB_FUN_PARA_OFFSET)/QT_SUB_FUN_OFFSET, \
			(iArgSigID) = (iArgMergeSigID)%QT_SUB_FUN_OFFSET)


/*for G3*/
struct tagSigInfoLCD
{
	int		iPageID;
	int		iEquipID;
	int		iSigID;
	int		iSigType;
};
typedef struct tagSigInfoLCD SigINFO_LCD;

struct tagSigInfoICO
{
	int		iPageID;
	int		iEquipID;
	int		iSigID;
	int		iCalType;
	int		iEqIDType;
};
typedef struct tagSigInfoICO SigINFO_ICO;

struct tagSigInfoData
{
	int		iPageID;
	int		iEquipID;
	int		iSigID;
};
typedef struct tagSigInfoData SigINFO_DATA;

struct tagSigInfoMod
{
	int		iPageID;
	int		iEquipID1;
	int		iNum1;
	int		iEquipID2;
	int		iNum2;
	int		iSigID;
};
typedef struct tagSigInfoMod SigINFO_MOD;

struct tagSigInfoDistrib
{
	int	iPageID;
	int	iEquipID;
	int	iSigID;
	int	iSigType;
};
typedef struct tagSigInfoDistrib SigINFO_DISTRIB;

struct tagSigInfoTemp
{
	int	iPageID;
	int	iEquipID_st;
	int	iEquipID_end;
	int	iSigID_st;
	int	iSigID_end;
};
typedef struct tagSigInfoTemp SigINFO_TEMP;

struct tagSigInfoBatt
{
	int	iSigID;
	int	iSigType;
};
typedef struct tagSigInfoBatt SigINFO_BATT;

struct tagDispInfoLCD
{
	int		iDispInfoNum;
	SigINFO_LCD	*pSigInfoLCD;
};
typedef struct tagDispInfoLCD DISPINFO_LCD;

struct tagDispInfoICO
{
	int		iICOInfoNum;
	SigINFO_ICO	*pSigInfoICO;
};
typedef struct tagDispInfoICO DISPINFO_ICO;

struct tagDispInfoMOD
{
	int		iMODInfoNum;
	SigINFO_MOD	*pSigInfoMOD;
};
typedef struct tagDispInfoMOD DISPINFO_MODULE;

struct tagDispInfoSET
{
	int		iSETInfoNum;
	SigINFO_DISTRIB	*pSigInfoSET;
};
typedef struct tagDispInfoSET DISPINFO_SET;

struct tagDispInfoDistr
{
	int		iDistrNum;
	SigINFO_DISTRIB	*pSigInfoDistr;
};
typedef struct tagDispInfoDistr DISPINFO_DISTRIB;

struct tagDispInfoTemp
{	int		iTempNum;
	SigINFO_TEMP	*pSigInfoTemp;
};
typedef struct tagDispInfoTemp DISPINFO_TEMP;

struct tagDispInfoBatt
{
	int		iBattInfoNum;
	SigINFO_BATT	*pSigInfoBatt;
};
typedef struct tagDispInfoBatt DISOINFO_EACHBATT;
struct tagUserDefLCD
{
	DISPINFO_LCD *pDispInfoLCD;
	DISPINFO_ICO		*pDispInfoICO;
	DISPINFO_MODULE		*pDispInfoMOD;
	DISPINFO_SET		*pDispInfoSET;
	DISPINFO_DISTRIB	*pDispInfoDistr;
	DISPINFO_TEMP		*pDispInfoTemp;
	DISOINFO_EACHBATT	*pDispInfoBatt;

};
typedef struct tagUserDefLCD USER_DEF_LCD;

USER_DEF_LCD	pUserDefLCD;		//G3 for LCD

/***/

struct tagEquipInfo_LCD
{
	int iBattNum;
	int BattInfo[MAXNUM_BATT][2];	//[0]-EquipTypeID; [1]-EquipID
};
typedef struct tagEquipInfo_LCD EQUIPINFO_LCD;

struct AlmFlag
{
	HANDLE	iAlmHandle;
	int	iAlmFlag;
};
typedef struct AlmFlag ALMFLAG;

ALMFLAG	g_AlmFlag;

void* p;
void* pAlm;
sem_t* psemNewAlm;


#define CMD_SIZE	44
//changed by Frank Wu,20140110,1/19, for add ScreenID
struct _Head
{
	int iScreenID;
};
typedef struct _Head Head_t;

/*for G3*/
struct SetInformation
{
	int EquipID;
	int SigID;
	int SigType;
	VAR_VALUE value;
	char			Name[MAXLEN_NAME];
};
typedef struct SetInformation CMD_SET_INFO;

struct CmdItem
{
	int iMapHandle;
	int CmdType;
	int ScreenID;
	CMD_SET_INFO setinfo;

};
typedef struct CmdItem CMD_INFO;

//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
struct SpecialID2Info
{
	int iDispCtrl;//0, auto; 1, show; 2, hide;
	int iCtrlArgs[MAX_DISP_ARGS_NUM];
	int iDispStyle;//0, auto; 1, label, just for displaying; 2, menu, can enter;
};
typedef struct SpecialID2Info SPECIAL_ID2_INFO;


struct PackGetInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,2/19, for add ScreenID
	int		iEquipID;
	int		iSigID;
	char		cSigName[MAXLEN_NAME];
	int		iSigType;
	int		iFormat;
	VAR_VALUE	vSigValue;

	char		cSigUnit[MAXLEN_UNIT];
	char		cEnumText[MAXLEN_NAME];

	//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
	SPECIAL_ID2_INFO stSpecialID2Info;//just if (iEquipID == -2), it becomes valid
	int iDataNum;//all PackGetInfo items counts
};
typedef struct PackGetInfo PACK_INFO;

struct IcoInfo
{
	int		iEquipID;
	int		iSigID;
	int		iFormat;
	char		cSigName[MAXLEN_NAME];
	float		fSigValue;
	char		cSigUnit[MAXLEN_UNIT];
};
typedef struct IcoInfo ICO_INFO;


struct Pack_Ico
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,3/19, for add ScreenID
	float		LimitValue[LIMNUM];
	int		iDataNum;
	ICO_INFO	DispData[MAXNUM_SIG];
};
typedef struct Pack_Ico PACK_ICOINFO;

struct ActAlmInfo
{
	//changed by Frank Wu,20131211,18/26 for twinkle of the green led of rectifier  
	int		iEqIDType;
	int		iEquipID;
	int		iSigID;
	
	char		AlmName[MAXLEN_NAME];
	char		EquipName[MAXLEN_NAME];
	int		AlmLevel;
	time_t		StartTime;
	//char		EquipSN[32];
	char		AlmHelp[MAXNUM_HELP];
};
typedef struct ActAlmInfo ACTALM_INFO;

struct PackActAlmInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,4/19, for add ScreenID
	int		ActAlmNum;
	ACTALM_INFO	ActAlmItem[MAXNUM_ACTALM];
};
typedef struct PackActAlmInfo PACK_ACTALM;

struct PackAlmNumInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,5/19, for add ScreenID
	int		OANum;
	int		MANum;
	int		CANum;
};
typedef struct PackAlmNumInfo PACK_ALMNUM;

typedef struct HisCondition
{
	int 	iEquipID;
	int	iNum;
	BYTE	iLevel;
}HIS_CONDITION;

struct HisAlmInfo
{
	char		EquipName[MAXLEN_NAME];
	char		AlmName[MAXLEN_NAME];
	BYTE		AlmLevel;
	time_t		StartTime;
	time_t		EndTime;
	//char		EquipSN[32];
	//char		AlmHelp[MAXNUM_HELP];
};
typedef struct HisAlmInfo HISALM_INFO;

struct PackHisAlmInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,6/19, for add ScreenID
	int		HisAlmNum;
	HISALM_INFO	HisAlmItem[MAXNUM_HISALM];
};
typedef struct PackHisAlmInfo PACK_HISALM;

struct PackModuleNumInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,7/19, for add ScreenID
	int	iRectNum;
	int	iMPPTNum;
};
typedef struct PackModuleNumInfo PACK_MODNUM;

struct EachModule
{
	int		iSigID;
	int		iSigType;
	VAR_VALUE	vSigValue;
	int		iFormat;
	char		cEnumText[MAXLEN_ENUM];
};
typedef struct EachModule EACHMOD_INFO;

struct ModuleInfo
{
	int		iEquipID;
	char		cEqName[MAXLEN_NAME];
	EACHMOD_INFO	EachMod[MAXNUM_SIG];
};
typedef struct ModuleInfo MODINFO;

struct PackModInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,8/19, for add ScreenID
	int		iModuleNum;
	MODINFO		ModInfo[MAXNUM_MODULE];
};
typedef struct PackModInfo PACK_MODINFO;

struct SettingInfo
{
	int		iEquipID;
	int		iSigID;
	int		iSigType;	
	int		iSigValueType;
	VAR_VALUE	vSigValue;
	VAR_VALUE	vUpLimit;
	VAR_VALUE	vDnLimit;
	VAR_VALUE	iStep;
	char		cSigName[MAXLEN_NAME];
	char		cSigUnit[MAXLEN_UNIT];
	char		cEnumText[MAXNUM_SEL][MAXLEN_NAME];
};
typedef struct SettingInfo SET_INFO;

struct PackSetInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,9/19, for add ScreenID
	int		SetNum;
	SET_INFO	SettingInfo[MAXLEN_SET];
};
typedef struct PackSetInfo PACK_SETINFO;

struct DataInfo
{
	int		iSigID;
	int		iFormat;
	VAR_VALUE	vValue;
};
typedef struct DataInfo DATA_INFO;

struct NormalInfo
{
	int		iEquipID;
	int		iSigID;
	int		iSigType;
	VAR_VALUE	vValue;
};
typedef struct NormalInfo NORMAL_INFO;

struct PackDataInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,10/19, for add ScreenID
	int		DataNum;
	NORMAL_INFO	DataInfo[MAXNUM_NORVALUE];
};
typedef struct PackDataInfo PACK_DATAINFO;

struct GroupInfo
{
	int		iEquipID;
	int		iSigID;
	int		iSigType;
	int		iRespond;
	int		iFormat;
	//changed by Frank Wu,1/4,20131113,for keeping the same order of temperature sensor  in all the LCD pages 
	int		iIndex;
	VAR_VALUE	vValue;
	//changed by Frank Wu,2/4,20140225,for TR160 which Missing alarm levels on temperature bar
	VAR_VALUE	vaLevel[MAXNUM_NORLEVEL];//value type float;0, Low Level; 1, High Level;2, Very High Level
};
typedef struct GroupInfo GROUP_INFO;

struct PackGrpInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,11/19, for add ScreenID
	int		DataNum;
	GROUP_INFO	DataInfo[MAXNUM_MODULE];
};
typedef struct PackGrpInfo PACK_GRPINFO;

struct LoadInfo
{
	int		iEquipID;
	int		iSigNo[MAXLEN_SIG];
	int		iRespond;
	int		iFormat;
	VAR_VALUE	vValue[MAXLEN_SIG];
	char		cEqName[MAXLEN_NAME];
};
typedef struct LoadInfo LOAD_INFO;

struct PackLoadInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,12/19, for add ScreenID
	int		LoadNum;
	LOAD_INFO	LoadInfo[MAXNUM_BRANCH];
};
typedef struct PackLoadInfo PACK_LOADINFO;

struct BattInfo
{
	int		iRespond;
	int		iEquipID;
	char		cEquipName[MAXLEN_NAME];
	DATA_INFO	BattItem[MAXNUM_BATINFO];
};
typedef struct BattInfo BATT_INFO;

struct PackBattInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,13/19, for add ScreenID
	int		BattNum;
	BATT_INFO	BattInfo[MAXNUM_BATT];
};
typedef struct PackBattInfo PACK_BATTINFO;

struct ModInv
{
	int		iEqIDType;//changed by Frank Wu,20131211,19/26 for twinkle of the green led of rectifier
	int		iEquipID;
	int		iSigID;//changed by Frank Wu,20131211,20/26 for twinkle of the green led of rectifier
	char		cEquipName[MAXLEN_PROINFO];
	char		cSerialNumber[MAXLEN_PROINFO];
	char		cPartNumber[MAXLEN_PROINFO];
	char		cPVer[MAXLEN_VER];
	char		cSWver[MAXLEN_PROINFO];
};
typedef struct ModInv MOD_INV;

struct PackInvInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,14/19, for add ScreenID
	int		iModuleNum;
	MOD_INV		ModuleInv[MAXNUM_MODULE];

};
typedef struct PackInvInfo PACK_INVINFO;

struct PackTrendInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,15/19, for add ScreenID
	float		fData[MAXNUM_TREND];
	//changed by Frank Wu,1/4,20140221 for TR203 On DC load graph show the load in % if possible show the A and %
	float		fCurrentData[MAXNUM_TREND];
};
typedef struct PackTrendInfo PACK_TRENDINFO;

struct Temp_Trend
{
	time_t		TimeTrend;
	float		fTemp;
};
typedef struct Temp_Trend TEMP_TREND;

struct PackTempTrend
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,16/19, for add ScreenID
	int		iNum;
	TEMP_TREND	TempTrend[MAXNUM_TREND];
};
typedef struct PackTempTrend PACK_TEMPTREND;

struct PackSelfInv
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,17/19, for add ScreenID
	char	cPartNumber[MAXLEN_NUMBER];
	char	cSN[MAXLEN_NAME];
	char	cPVer[MAXLEN_NUMBER];
	char	cSWver[MAXLEN_NUMBER];
	char	cCfgFileVer[MAXLEN_NUMBER];
	char	cFileSystemVer[MAXLEN_NUMBER];
	char	cEthaddr[LEN_MAC];
	ULONG	DHCP_IP;
	ULONG	MAIN_IP;
	//changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller��s IP address
	ULONG	FRONT_CRAFT_IP;
	//end -- changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller��s IP address
	//changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
	char	cIpv6Local[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"
	char	cIpv6Global[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"
	char	cIpv6DHCPServer[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"

};
typedef struct PackSelfInv PACK_SELFINV;

//changed by Frank Wu,20140617,1/2, for users display error
struct UserPwdInfo
{
	char szUserName[USERNAME_LEN];	//User name
	char szPassword[PASSWORD_LEN];	//User password
	BYTE byLevel;					//User level
};
typedef struct UserPwdInfo USER_PWD_INFO;

struct PackPwdInfo
{
	//Head_t must be the first, do not move
	Head_t stHead;//changed by Frank Wu,20140110,18/19, for add ScreenID
	int		iNum;
	//changed by Frank Wu,20140617,2/2, for users display error
	//USER_INFO_STRU	UserInfo[USERS_MAX_NUMBER];
	USER_PWD_INFO	UserInfo[USERS_MAX_NUMBER];
};
typedef struct PackPwdInfo PACK_PWD;
/***/

typedef struct HisGeneralCondition
{
    int 	iEquipID;
    int		iSigID;
    time_t	tmFromTime;
    time_t	tmToTime;
}TEMP_CONDITION;


int iLangType;
int iQTRunning=0;

static int Web_GetAuthorityByUser(IN char *pszUserInfo,IN char *pszPasswordInfo);
static int Web_ModifySignalAlarmLevel(IN int iEquipID,
									  IN int iSignalType,
									  IN int iSignalID,
									  IN int iAlarmLevel,
									  IN char *szWriteUserName);
static int Web_GetEquipInfoByTypeId(IN int iEquipTypeId, OUT EQUIP_INFO **ppszEquipInfo);
static int Web_ModifySignalValue_new(IN int iEquipID,
									 IN int iSignalType,
									 IN int iSignalID,
									 IN VAR_VALUE_EX value);
static int Web_ModifyXMLAlarmRelay(IN int iEquipTypeID,IN int iAlarmSigID, IN int iAlarmRelay, IN char *szUserName);
int QTCommunicate(IN int *iQuitCommand);


#endif

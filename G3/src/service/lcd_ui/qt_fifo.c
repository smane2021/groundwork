
/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : qt_fifo.c
*  CREATOR  : Jimmy Wu              DATE: 2013-02-19
*  VERSION  : V1.00
*  PURPOSE  :
*   
*
*  HISTORY  :
*
*==========================================================================*/
#include <signal.h>
#include "stdsys.h"  
#include <semaphore.h>
#include <sys/mman.h>


#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/resource.h>		/* setrlimit */
#include <sys/types.h>			/* socket, bind, accept */
#include <sys/socket.h>			/* socket, bind, accept, setsockopt, */
#include <netdb.h>
#include <net/route.h>			/*rtentry*/
#include <net/if.h>				/*ifreq */
#include <time.h>
#include <sys/dir.h> 
#include <unistd.h>
#include "qt_fifo.h"
#include "cfg_model.h"

#include "../../mainapp/config_mgmt/cfg_mgmt.h"
//changed by Frank Wu,20131228,1/6,for support settings of part EEM protocol in LCD
#include "../../service/eem_soc/esr.h"
#include "../../service/modbus/modbus.h"
#include "../../service/ydn23/ydn.h"
#include "../../service/tl1/tl1.h"
//changed by Frank Wu,1/3,20140308, for filter the user of "vertivvadmin"
#include "../../include/user_manage.h"

#define QT_DEBUG	0

EQUIPINFO_LCD g_LCDEquipInfo;
int	iOBAlmNum,iMAlmNum,iCAlmNum,TotalAlmNum;

extern int RcfgFlag;
PACK_ALMNUM g_stActAlmNum;


// changed by Frank Wu, 20131113, 1/9,for only displaying one data in each interval of 2 hours-----start---
//Declaration of all the static functions of qt_fifo module
static time_t ComputeLatestTimePoint(void);
static void FilterTrendTData(IN time_t tmFarthest, 
							IN time_t tmLatest,	
							IN time_t tmInterval,	
							IN OUT HIS_DATA_RECORD *pHisDataRecord,	
							IN OUT int *piRecords);
// changed by Frank Wu, 20131113, 1/9,for only displaying one data in each interval of 2 hours-----end---
//changed by Frank Wu,20131228,2/6,for support settings of part EEM protocol in LCD ---start---
static BOOL PackSpecialItemCommProtocol(OUT SET_INFO *pstSetInfo, OUT int *pIndex);
static BOOL PackSpecialItemCommAddr(OUT SET_INFO *pstSetInfo, OUT int *pIndex);
static BOOL PackSpecialItemCommMediaType(OUT SET_INFO *pstSetInfo, OUT int *pIndex);
static BOOL PackSpecialItemCommBaudrate(OUT SET_INFO *pstSetInfo, OUT int *pIndex);

#define SYSTEM_UNIT						1
#define SIG_ID_PROTOCOL_TYPE			457

static int GetProtocolType(void)
{
	int nBufLen = 0;
	SIG_BASIC_VALUE *pSigValue = NULL;
	int iProtocolType = PROTOCOL_EEM;
	int iErr;

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
		SYSTEM_UNIT,
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SIG_ID_PROTOCOL_TYPE),
		&nBufLen,
		(void *)&pSigValue,
		0);

	iProtocolType = PROTOCOL_EEM;
	if(iErr == ERR_DXI_OK)
	{
		iProtocolType = pSigValue->varValue.enumValue;
	}

	return iProtocolType;
}

static BOOL SetProtocolType(int iProtocolType)
{
	VAR_VALUE_EX sigValue;
	int nBufLen = sizeof(sigValue);
	int iErr;

	memset(&sigValue, 0, sizeof(sigValue));
	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = "LUI";
	sigValue.varValue.enumValue = iProtocolType;


	iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						SYSTEM_UNIT,
						DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SIG_ID_PROTOCOL_TYPE),
						nBufLen,
						&sigValue,
						0);
	if(iErr == ERR_DXI_OK)
	{
		return TRUE;
	}

	return FALSE;
}


static BOOL ProcCmdSetOrGetComm(IN int iProtocolType, IN OUT void *pstCommonConfig, IN int iModifyType)
{
#define GET_SERVICE_OF_ESR_NAME			"eem_soc.so"
#define GET_SERVICE_OF_NMS_NAME			"snmp_agent.so"
#define GET_SERVICE_OF_YDN_NAME			"ydn23.so"
#define GET_SERVICE_OF_MODBUS_NAME		"modbus.so"
#define GET_SERVICE_OF_TL1_NAME			"tl1.so"

	APP_SERVICE *pstAppService = NULL;
	int iReturn = ERR_SERVICE_CFG_FAIL;

	switch(iProtocolType)
	{
	case PROTOCOL_EEM:
		{
			pstAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);
			break;
		}
	case PROTOCOL_YDN23:
		{
			pstAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN_NAME);
			break;
		}
	case PROTOCOL_MODBUS:
		{
			pstAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_MODBUS_NAME);
			break;
		}
	case PROTOCOL_TL1:
		{
			pstAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_TL1_NAME);
			break;
		}
	default://PROTOCOL_EEM
		{
			pstAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);
			break;
		}
	}

	if(pstAppService != NULL && pstAppService->pfnServiceConfig != NULL)
	{
		//printf("bServiceRunning : %d\n",pstAppService->bServiceRunning);
		iReturn = pstAppService->pfnServiceConfig(pstAppService->hServiceThread,
			pstAppService->bServiceRunning,
			&pstAppService->args, 
			iModifyType,
			0,
			(void *)pstCommonConfig);
	}

	//printf("iModifyType : %x\n", iModifyType);
	//printf("iReturn = %d\n", iReturn);

	if(iReturn == ERR_SERVICE_CFG_OK)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static BOOL ProcCmdSetCommProtocol(IN void *pCmdPara)
{
	int iNewProtocolType = *( (int *)pCmdPara );
	int iProtocolType = GetProtocolType();
	int bIsOK = FALSE;

	if(iProtocolType == iNewProtocolType)
	{
		bIsOK = TRUE;
	}
	else
	{
		bIsOK = SetProtocolType(iNewProtocolType);
		if(bIsOK)
		{
			_SYSTEM("reboot");
		}
	}

	return bIsOK;
}

static BOOL ProcCmdSetCommAddr(IN void *pCmdPara)
{
	unsigned int uiADR = *( (unsigned int *)pCmdPara );
	int iModifyType = 0;
	int iProtocolType = 0;
	int bIsOK = FALSE;

	iProtocolType = GetProtocolType();
	//printf("ProcCmdSetCommAddr iProtocolType=%d, uiADR=%u\n", iProtocolType, uiADR);
	switch(iProtocolType)
	{
	case PROTOCOL_YDN23:
		{
			YDN_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~YDN_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("ProcCmdSetCommAddr read uiADR=%u\n", stCommonConfig.byADR);
			//write
			if(bIsOK)
			{
				if(stCommonConfig.byADR != uiADR)
				{
					stCommonConfig.byADR = uiADR;
					iModifyType = 0;
					iModifyType |= YDN_CFG_W_MODE;//bit:0, read, 1,write
					iModifyType |= YDN_CFG_ADR;//update address
					bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
				}
				else
				{
					bIsOK = TRUE;
				}
			}

			break;
		}
	case PROTOCOL_MODBUS:
		{
			MODBUS_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~MODBUS_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("ProcCmdSetCommAddr read uiADR=%u\n", stCommonConfig.byADR);
			//write
			if(bIsOK)
			{
				if(stCommonConfig.byADR != uiADR)
				{
					stCommonConfig.byADR = uiADR;
					iModifyType = 0;
					iModifyType |= MODBUS_CFG_W_MODE;//bit:0, read, 1,write
					iModifyType |= MODBUS_CFG_ADR;//update address
					bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
				}
				else
				{
					bIsOK = TRUE;
				}
			}

			break;
		}
	default:
		{
			break;
		}
	}

	return bIsOK;
}


#define QT_FIFO_DEF_PARA_ID_YDN23_LINE		0
#define QT_FIFO_DEF_PARA_ID_YDN23_MODEM		1
#define QT_FIFO_DEF_PARA_ID_YDN23_TCPIP		2
#define QT_FIFO_DEF_PARA_ID_MODBUS_LINE		3
#define QT_FIFO_DEF_PARA_ID_MODBUS_RS485	4
#define QT_FIFO_DEF_PARA_ID_MODBUS_TCPIP	5
#define QT_FIFO_DEF_PARA_ID_OTHER			6

const char sDefaultCommPara[][30] = {
	"9600,n,8,1",//0
	"9600,n,8,1",//1
	"2000",//2
	"9600,n,8,1",//3
	"9600,n,8,1",//4
	"502",//5
	"",
};

static const char * QueryCommDefaultPara(IN int iProtocol, IN int iMedia)
{
	switch(iProtocol)
	{
		case PROTOCOL_YDN23:
		{
			if(iMedia == YDN_MEDIA_TYPE_LEASED_LINE)
			{
				return sDefaultCommPara[QT_FIFO_DEF_PARA_ID_YDN23_LINE];
			}
			else if(iMedia == YDN_MEDIA_TYPE_MODEM)
			{
				return sDefaultCommPara[QT_FIFO_DEF_PARA_ID_YDN23_MODEM];
			}
			else if(iMedia == YDN_MEDIA_TYPE_TCPIP)
			{
				return sDefaultCommPara[QT_FIFO_DEF_PARA_ID_YDN23_TCPIP];
			}
			break;
		}
		case PROTOCOL_MODBUS:
		{
			if(iMedia == MODBUS_MEDIA_TYPE_LEASED_LINE)
			{
				return sDefaultCommPara[QT_FIFO_DEF_PARA_ID_MODBUS_LINE];
			}
			else if(iMedia == MODBUS_MEDIA_TYPE_RS485)
			{
				return sDefaultCommPara[QT_FIFO_DEF_PARA_ID_MODBUS_RS485];
			}
			else if(iMedia == MODBUS_MEDIA_TYPE_TCPIP)
			{
				return sDefaultCommPara[QT_FIFO_DEF_PARA_ID_MODBUS_TCPIP];
			}
			break;
		}
	}
	
	return sDefaultCommPara[QT_FIFO_DEF_PARA_ID_OTHER];
}

static BOOL ProcCmdSetCommMedia(IN void *pCmdPara)
{
	int iMediaType = (int)(*( (unsigned int *)pCmdPara ));
	int iModifyType = 0;
	int iProtocolType = 0;
	int bIsOK = FALSE;
	const char *pDefPara = NULL;

	iProtocolType = GetProtocolType();
	//printf("ProcCmdSetCommMedia iProtocolType=%d, iMediaType=%d\n", iProtocolType, iMediaType);
	switch(iProtocolType)
	{
	case PROTOCOL_YDN23:
		{
			YDN_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~YDN_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("ProcCmdSetCommMedia read iMediaType=%d\n", stCommonConfig.iMediaType);
			//write
			if(bIsOK)
			{
				if(stCommonConfig.iMediaType != iMediaType)
				{
					stCommonConfig.iMediaType = iMediaType;
					pDefPara = QueryCommDefaultPara(iProtocolType, iMediaType);
					sprintf(stCommonConfig.szCommPortParam, "%s", pDefPara);//reset port para
					//printf("ProcCmdSetCommMedia write szCommPortParam=%s\n", stCommonConfig.szCommPortParam);
					//printf("ProcCmdSetCommMedia write pDefPara=%s\n", pDefPara);
					iModifyType = 0;
					iModifyType |= YDN_CFG_W_MODE;//bit:0, read, 1,write
					iModifyType |= YDN_CFG_MEDIA_TYPE;
					iModifyType |= YDN_CFG_MEDIA_PORT_PARAM;
					bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
				}
				else
				{
					bIsOK = TRUE;
				}
			}

			break;
		}
	case PROTOCOL_MODBUS:
		{
			MODBUS_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~MODBUS_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("ProcCmdSetCommMedia read iMediaType=%d\n", stCommonConfig.iMediaType);
			//write
			if(bIsOK)
			{
				if(stCommonConfig.iMediaType != iMediaType)
				{
					stCommonConfig.iMediaType = iMediaType;
					pDefPara = QueryCommDefaultPara(iProtocolType, iMediaType);
					sprintf(stCommonConfig.szCommPortParam, "%s", pDefPara);//reset port para
					//printf("ProcCmdSetCommMedia write szCommPortParam=%s\n", stCommonConfig.szCommPortParam);
					//printf("ProcCmdSetCommMedia write pDefPara=%s\n", pDefPara);
					iModifyType = 0;
					iModifyType |= MODBUS_CFG_W_MODE;//bit:0, read, 1,write
					iModifyType |= MODBUS_CFG_MEDIA_TYPE;
					iModifyType |= MODBUS_CFG_MEDIA_PORT_PARAM;
					bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
				}
				else
				{
					bIsOK = TRUE;
				}
			}

			break;
		}
	default:
		{
			break;
		}
	}

	return bIsOK;
}

static const char s_sCommBaudrateList[][MAXLEN_NAME] = {
	"38400",
	"19200",
	"9600",
	"4800",
	"2400",
	"1200",
};

static char * GetBaudrateStrByIndex(IN int iIndex)
{
	int iCount = sizeof(s_sCommBaudrateList)/sizeof(s_sCommBaudrateList[0]);
	char *psBaudrateStr = NULL;

	//printf("GetBaudrateStrByIndex iIndex=%d\n", iIndex);
	if( (iIndex >= 0) && (iIndex < iCount) )
	{
		psBaudrateStr = s_sCommBaudrateList[iIndex];
		//printf("GetBaudrateStrByIndex ssss=%s\n", s_sCommBaudrateList[iIndex]);

	}

	return psBaudrateStr;
}

static int GetBaudrateIndexByStr(IN char *psBaudrateStr)
{
	int iCount = sizeof(s_sCommBaudrateList)/sizeof(s_sCommBaudrateList[0]);
	int i = 0;
	int iRet = -1;

	for(i = 0; i < iCount; i++)
	{
		if( strncmp(s_sCommBaudrateList[i],
			psBaudrateStr, 
			strlen(s_sCommBaudrateList[i])) == 0 )
		{
			iRet = i;
			break;
		}
	}

	return iRet;
}

static void ReplayBaudrateStr(IN OUT char *psCommPortParam, IN char *psBaudrate)
{
	char sParaSuffix[200];

	memset(sParaSuffix, 0, sizeof(sParaSuffix));
	sscanf(psCommPortParam, "%*d%s", sParaSuffix);//PORT para eg. "9600,n,8,1"
	sprintf(psCommPortParam, "%s%s", psBaudrate, sParaSuffix);
}

static BOOL ProcCmdSetCommBaudrate(IN void *pCmdPara)
{
	int iBaudrateIndex = (int)(*( (unsigned int *)pCmdPara ));
	char *psNewBaudrateStr = NULL;
	int iModifyType = 0;
	int iProtocolType = 0;
	BOOL bIsOK = FALSE;

	psNewBaudrateStr = GetBaudrateStrByIndex(iBaudrateIndex);
	if(psNewBaudrateStr == NULL)
	{
		return bIsOK;
	}
	//printf("ProcCmdSetCommBaudrate psNewBaudrateStr=%s\n", psNewBaudrateStr);
	iProtocolType = GetProtocolType();
	//printf("ProcCmdSetCommBaudrate iProtocolType=%d, iBaudrateIndex=%d\n", iProtocolType, iBaudrateIndex);
	switch(iProtocolType)
	{
	case PROTOCOL_YDN23:
		{
			YDN_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~YDN_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("ProcCmdSetCommBaudrate read szCommPortParam=%s bIsOK=%d\n", stCommonConfig.szCommPortParam, bIsOK);
			//write
			if(bIsOK)
			{
				if( strncmp(stCommonConfig.szCommPortParam,
					psNewBaudrateStr,
					strlen(psNewBaudrateStr)) != 0 )
				{
					ReplayBaudrateStr(stCommonConfig.szCommPortParam, psNewBaudrateStr);
					//printf("ProcCmdSetCommBaudrate psNewBaudrateStr=%s\n", psNewBaudrateStr);
					//printf("ProcCmdSetCommBaudrate szCommPortParam=%s\n", stCommonConfig.szCommPortParam);
					iModifyType = 0;
					iModifyType |= YDN_CFG_W_MODE;//bit:0, read, 1,write
					iModifyType |= YDN_CFG_MEDIA_PORT_PARAM;//update
					bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
				}
				else
				{
					bIsOK = TRUE;
				}
			}

			break;
		}
	case PROTOCOL_MODBUS:
		{
			MODBUS_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~MODBUS_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("ProcCmdSetCommBaudrate read szCommPortParam=%s bIsOK=%d\n", stCommonConfig.szCommPortParam, bIsOK);
			//write
			if(bIsOK)
			{
				if( strncmp(stCommonConfig.szCommPortParam,
					psNewBaudrateStr,
					strlen(psNewBaudrateStr)) != 0 )
				{
					ReplayBaudrateStr(stCommonConfig.szCommPortParam, psNewBaudrateStr);
					//printf("ProcCmdSetCommBaudrate psNewBaudrateStr=%s\n", psNewBaudrateStr);
					//printf("ProcCmdSetCommBaudrate szCommPortParam=%s\n", stCommonConfig.szCommPortParam);
					iModifyType = 0;
					iModifyType |= MODBUS_CFG_W_MODE;//bit:0, read, 1,write
					iModifyType |= MODBUS_CFG_MEDIA_PORT_PARAM;//update
					bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
				}
				else
				{
					bIsOK = TRUE;
				}
			}

			break;
		}
	default:
		{
			break;
		}
	}

	return bIsOK;
}

static BOOL QueryProtocalBaudrate(OUT int *pBaudrateIndex)
{
	int iModifyType = 0;
	int iProtocolType = 0;
	BOOL bIsOK = FALSE;
	int iBaudrateIndex = -1;

	iProtocolType = GetProtocolType();
	//printf("QueryProtocalBaudrate iProtocolType=%d\n", iProtocolType);
	switch(iProtocolType)
	{
	case PROTOCOL_YDN23:
		{
			YDN_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~YDN_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("QueryProtocalBaudrate szCommPortParam=%s\n", stCommonConfig.szCommPortParam);
			//write
			if(bIsOK)
			{
				if( (stCommonConfig.iMediaType == YDN_MEDIA_TYPE_LEASED_LINE)
					|| (stCommonConfig.iMediaType == YDN_MEDIA_TYPE_MODEM) )
				{
					iBaudrateIndex = GetBaudrateIndexByStr(stCommonConfig.szCommPortParam);
				}
			}

			break;
		}
	case PROTOCOL_MODBUS:
		{
			MODBUS_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~MODBUS_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("QueryProtocalBaudrate szCommPortParam=%s\n", stCommonConfig.szCommPortParam);
			//write
			if(bIsOK)
			{
				if( (stCommonConfig.iMediaType == MODBUS_MEDIA_TYPE_LEASED_LINE)
					|| (stCommonConfig.iMediaType == MODBUS_MEDIA_TYPE_RS485) )
				{
					iBaudrateIndex = GetBaudrateIndexByStr(stCommonConfig.szCommPortParam);
				}
			}

			break;
		}
	default:
		{
			break;
		}
	}

	if(iBaudrateIndex >= 0)//valid 
	{
		*pBaudrateIndex = iBaudrateIndex;
		return TRUE;
	}

	return FALSE;
}

static BOOL IsValidCommMediaTypeIndex(IN int iIndex)
{
	if( (iIndex >= 0) 
		&& ( (iIndex < YDN_MEDIA_TYPE_NUM) || (iIndex < MODBUS_MEDIA_TYPE_NUM) ) )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static BOOL QueryProtocalMediaType(OUT int *piQueryResult)
{
	int iModifyType = 0;
	int iProtocolType = 0;
	BOOL bIsOK = FALSE;
	int iMediaTypeIndex = -1;

	iProtocolType = GetProtocolType();
	//printf("QueryProtocalMediaType iProtocolType=%d\n", iProtocolType);
	switch(iProtocolType)
	{
	case PROTOCOL_YDN23:
		{
			YDN_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~YDN_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("QueryProtocalMediaType iMediaType=%d\n", stCommonConfig.iMediaType);
			//write
			if(bIsOK)
			{
				iMediaTypeIndex = stCommonConfig.iMediaType;
			}

			break;
		}
	case PROTOCOL_MODBUS:
		{
			MODBUS_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~MODBUS_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("QueryProtocalMediaType iMediaType=%d\n", stCommonConfig.iMediaType);
			//write
			if(bIsOK)
			{
				iMediaTypeIndex = stCommonConfig.iMediaType;
			}

			break;
		}
	default:
		{
			break;
		}
	}

	if( IsValidCommMediaTypeIndex(iMediaTypeIndex))
	{
		*piQueryResult = iMediaTypeIndex;
		return TRUE;
	}

	return FALSE;
}

static BOOL IsValidCommAddr(IN int iAddr)
{
	if((iAddr > 0) && (iAddr < 255))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static BOOL QueryProtocalAddr(OUT int *piQueryResult)
{
	int iModifyType = 0;
	int iProtocolType = 0;
	BOOL bIsOK = FALSE;
	int iAddr = -1;

	iProtocolType = GetProtocolType();
	//printf("QueryProtocalAddr iProtocolType=%d\n", iProtocolType);
	switch(iProtocolType)
	{
	case PROTOCOL_YDN23:
		{
			YDN_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~YDN_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("QueryProtocalAddr iAddr=%d\n", stCommonConfig.byADR);
			//write
			if(bIsOK)
			{
				iAddr = stCommonConfig.byADR;
			}

			break;
		}
	case PROTOCOL_MODBUS:
		{
			MODBUS_COMMON_CONFIG stCommonConfig;

			memset( (void *)&stCommonConfig, 0, sizeof(stCommonConfig) );
			//read
			iModifyType = 0;
			iModifyType &= (~MODBUS_CFG_W_MODE);//bit:0, read, 1,write
			bIsOK = ProcCmdSetOrGetComm(iProtocolType, (void *)&stCommonConfig, iModifyType);
			//printf("QueryProtocalAddr iAddr=%d\n", stCommonConfig.byADR);
			//write
			if(bIsOK)
			{
				iAddr = stCommonConfig.byADR;
			}

			break;
		}
	default:
		{
			break;
		}
	}

	if( IsValidCommAddr(iAddr))
	{
		*piQueryResult = iAddr;
		return TRUE;
	}

	return FALSE;
}

static BOOL QueryProtocalName(OUT int *piQueryResult)
{
	*piQueryResult = GetProtocolType();

	return TRUE;
}

static BOOL PackSpecialItemCommProtocol(OUT SET_INFO *pstSetInfo, OUT int *pIndex)
{
	BOOL bRet = TRUE;
	int iQueryResult;

	bRet = QueryProtocalName(&iQueryResult);
	if(bRet)
	{
		pstSetInfo->iSigValueType = VAR_ENUM;
		pstSetInfo->vSigValue.enumValue = iQueryResult;
		(*pIndex)++;
	}
	//printf("PackSpecialItemCommProtocol bRet=%d, iQueryResult=%d\n", bRet, iQueryResult);

	return bRet;
}

static BOOL PackSpecialItemCommAddr(OUT SET_INFO *pstSetInfo, OUT int *pIndex)
{
	BOOL bRet = TRUE;
	int iQueryResult;

	bRet = QueryProtocalAddr(&iQueryResult);
	if(bRet)
	{
		pstSetInfo->iSigValueType = VAR_LONG;
		pstSetInfo->vSigValue.lValue = iQueryResult;
		(*pIndex)++;
	}
	//printf("PackSpecialItemCommAddr bRet=%d, iQueryResult=%d\n", bRet, iQueryResult);

	return bRet;
}

static BOOL PackSpecialItemCommMediaType(OUT SET_INFO *pstSetInfo, OUT int *pIndex)
{
	BOOL bRet = TRUE;
	int iQueryResult;

	bRet = QueryProtocalMediaType(&iQueryResult);
	if(bRet)
	{
		pstSetInfo->iSigValueType = VAR_ENUM;
		pstSetInfo->vSigValue.enumValue = iQueryResult;
		(*pIndex)++;
	}
	//printf("PackSpecialItemCommMediaType bRet=%d, iQueryResult=%d\n", bRet, iQueryResult);

	return bRet;
}

static BOOL PackSpecialItemCommBaudrate(OUT SET_INFO *pstSetInfo, OUT int *pIndex)
{
	BOOL bRet = TRUE;
	int iQueryResult;

	bRet = QueryProtocalBaudrate(&iQueryResult);
	if(bRet)
	{
		pstSetInfo->iSigValueType = VAR_ENUM;
		pstSetInfo->vSigValue.enumValue = iQueryResult;
		(*pIndex)++;
	}
	//printf("PackSpecialItemCommBaudrate bRet=%d, iQueryResult=%d\n", bRet, iQueryResult);

	return bRet;
}
//changed by Frank Wu,20131228,2/6,for support settings of part EEM protocol in LCD ---end---

//changed by Frank Wu,20131211,1/26 for twinkle of the green led of rectifier
static int GetEquipTypeID(IN int iEquipId)
{
	int iError;
	int iBufLen;
	EQUIP_INFO* pEquipInfo;
	int iEquipTypeID = SPECIALID;;

	iError = DxiGetData(VAR_A_EQUIP_INFO,
		iEquipId,
		0,
		&iBufLen,
		&pEquipInfo,
		0);

	if(iError == ERR_DXI_OK)
	{
		iEquipTypeID = pEquipInfo->iEquipTypeID;
	}

	return iEquipTypeID;
}


void	LCD_EquipInfoInit(void)
{
    printf("LCD_EquipInfoInit\n");//hhy
	int			i,j, iIdx, iEquipNum;
	int			iRst;
	int			iBufLen;
	int			iTemp[2];
	EQUIP_INFO	*pEquipList;

	//Get Quantity of all of equipments
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM, 
					0, 
					0, 
					&iBufLen, 
					&iEquipNum, 
					0);

	//Get point of Equipment List
	iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
					0, 
					0, 
					&iBufLen, 
					&pEquipList, 
					0);
	iIdx = 0;
	iTemp[0] = 0;
	iTemp[1] = 0;

	for(i = 0;i < iEquipNum;i++)
	{
		if((((pEquipList + i)->iEquipTypeID) >= EQUIPTYPE_BATT_MIN)
			&& (((pEquipList + i)->iEquipTypeID) <= EQUIPTYPE_BATT_MAX))
		{
			if(iIdx < MAXNUM_BATT)
			{
				g_LCDEquipInfo.BattInfo[iIdx][0] = (pEquipList + i)->iEquipTypeID;
				g_LCDEquipInfo.BattInfo[iIdx][1] = (pEquipList + i)->iEquipID;
				iIdx++;
			}
		}
	}
	g_LCDEquipInfo.iBattNum = iIdx;

	//ÅÅÐò
	for(j = 0;j < (iIdx - 1);j++)
	{
		for(i = 0;i < (iIdx - 1);i++)
		{
			if(g_LCDEquipInfo.BattInfo[i][1] > g_LCDEquipInfo.BattInfo[i+1][1])
			{
				iTemp[0] = g_LCDEquipInfo.BattInfo[i+1][0];
				iTemp[1] = g_LCDEquipInfo.BattInfo[i+1][1];
				g_LCDEquipInfo.BattInfo[i+1][0] = g_LCDEquipInfo.BattInfo[i][0];
				g_LCDEquipInfo.BattInfo[i+1][1] = g_LCDEquipInfo.BattInfo[i][1];
				g_LCDEquipInfo.BattInfo[i][0] = iTemp[0];
				g_LCDEquipInfo.BattInfo[i][1] = iTemp[1];
			}
/*
			else if(g_LCDEquipInfo.BattInfo[i][0] == g_LCDEquipInfo.BattInfo[i+1][0])
			{
				if(g_LCDEquipInfo.BattInfo[i][1] > g_LCDEquipInfo.BattInfo[i+1][1])
				{
					iTemp[1] = g_LCDEquipInfo.BattInfo[i+1][1];
					g_LCDEquipInfo.BattInfo[i+1][1] = g_LCDEquipInfo.BattInfo[i][1];
					g_LCDEquipInfo.BattInfo[i][1] = iTemp[1];
				}
			}
*/
		}
	}

	//for(j = 0;j < iIdx;j++)
	//	printf("EquipTypeID %d=%d;EquipID %d=%d\n",j, g_LCDEquipInfo.BattInfo[j][0],j,g_LCDEquipInfo.BattInfo[j][1]);
}

static int ParseDispInfoTableProc(IN char *szBuf, OUT SigINFO_LCD * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Page ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Page ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iPageID = atoi(pField);

	/* 2.EquipID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	/*if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;   
	}*/
	if ((*pField < '0' || *pField > '9') && 
		*pField != '-')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	/* for negative number, only -1 is valid */
	//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
	//if (*pField == '-' && *(pField + 1) != '1')
	if (*pField == '-'
		&& *(pField + 1) != '1'
		&& *(pField + 1) != '2')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Index of Sampler "
			"list is not an invalid number!\n", __FILE__);
		return 2;    /* not a num, error */
	}
	pStructData->iEquipID = atoi(pField);

	/* 3.SigID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 4.SigType field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 4;    /* not a num, error */
	}
	pStructData->iSigType = atoi(pField);

	return 0;
}

static int ParseIcoInfoTableProc(IN char *szBuf, OUT SigINFO_ICO * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Page ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Page ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iPageID = atoi(pField);

	/* 2.EquipID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	pStructData->iEquipID = atoi(pField);

	/* 3.SigID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 4.CalType field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 4;    /* not a num, error */
	}
	pStructData->iCalType = atoi(pField);

	/* 5.EqIDtype field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 5;    /* not a num, error */
	}
	pStructData->iEqIDType = atoi(pField);

	return 0;
}

static int ParseModuleInfoProc(IN char *szBuf, OUT SigINFO_MOD * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Page ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Page ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iPageID = atoi(pField);

	/* 2.EquipID1 field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	pStructData->iEquipID1 = atoi(pField);

	/* 3.Num1 field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iNum1 = atoi(pField);

	/* 4.EquipID2 field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 4;    /* not a num, error */
	}
	pStructData->iEquipID2 = atoi(pField);

	/* 5.Num2 field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 5;    /* not a num, error */
	}
	pStructData->iNum2 = atoi(pField);

	/* 6.SigID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 6;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	return 0;
}

static int ParseSetInfoProc(IN char *szBuf, OUT SigINFO_DISTRIB * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Page ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Page ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iPageID = atoi(pField);

	/* 2.EquipID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	//if ((*pField < '0') || (*pField > '9'))
	if ((*pField < '0' || *pField > '9') && 
		*pField != '-')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	/* for negative number, only -1 is valid */
	if (*pField == '-' && *(pField + 1) != '1')
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseSetSigTableProc: ERROR: Index of Sampler "
			"list is not an invalid number!\n", __FILE__);
		return 2;    /* not a num, error */
	}
	pStructData->iEquipID = atoi(pField);

	/* 3.SigID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 3.SigID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 4;    /* not a num, error */
	}
	pStructData->iSigType = atoi(pField);
	return 0;
}

static int ParseDistribInfoProc(IN char *szBuf, OUT SigINFO_DISTRIB * pStructData)
{
	char *pField;

	/* used as buffer */
	int iMaxLenForFull, iMaxLenForAbbr;
	char *szText;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Page ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Page ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iPageID = atoi(pField);

	/* 2.EquipID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	pStructData->iEquipID = atoi(pField);

	/* 3.SigID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 4.SigType field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 4;    /* not a num, error */
	}
	pStructData->iSigType = atoi(pField);

	return 0;
}

static int ParseTempInfoProc(IN char *szBuf, OUT SigINFO_TEMP *pStructData)
{
	char *pField;

	/* used as buffer */
	int iMaxLenForFull, iMaxLenForAbbr;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.Page ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Page ID is not "
			"a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iPageID = atoi(pField);

	/* 2.EquipID_st field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	pStructData->iEquipID_st = atoi(pField);

	/* 3.EquipID_end field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 3;    /* not a num, error */
	}
	pStructData->iEquipID_end = atoi(pField);

	/* 4.SigID_st field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 4;    /* not a num, error */
	}
	pStructData->iSigID_st = atoi(pField);

	/* 5.SigID_end field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParsePageInfoTableProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 5;    /* not a num, error */
	}
	pStructData->iSigID_end = atoi(pField);

	return 0;
}

static int ParseBattInfoProc(IN char *szBuf, OUT SigINFO_BATT * pStructData)
{
	char *pField;

	/* used as buffer */
	int iMaxLenForFull, iMaxLenForAbbr;
	char *szText;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.SigID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseBattInfoProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iSigID = atoi(pField);

	/* 2.SigType field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--ParseBattInfoProc: ERROR: Max length(full) "
			"is not a number!\n", __FILE__);

		return 2;    /* not a num, error */
	}
	pStructData->iSigType = atoi(pField);

	return 0;
}

static int LoadUserDefLCDInfoProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	USER_DEF_LCD *pBuf;
	char szLineData[MAX_LINE_SIZE];
	int  ret;
	CONFIG_TABLE_LOADER loader[7];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (USER_DEF_LCD *)pLoadToBuf;

	//pBuf = NEW(USER_DEF_LCD,1);
	pBuf->pDispInfoLCD = NEW(DISPINFO_LCD,1);
	if(pBuf->pDispInfoLCD == NULL)
	{
		return ERR_CFG_NO_MEMORY;
	}

	pBuf->pDispInfoICO = NEW(DISPINFO_ICO,1);
	if(pBuf->pDispInfoICO == NULL)
	{
		SAFELY_DELETE(pBuf->pDispInfoLCD);
		return ERR_CFG_NO_MEMORY;
	}

	pBuf->pDispInfoMOD = NEW(DISPINFO_MODULE,1);
	if(pBuf->pDispInfoMOD == NULL)
	{
		SAFELY_DELETE(pBuf->pDispInfoLCD);
		SAFELY_DELETE(pBuf->pDispInfoICO);
		return ERR_CFG_NO_MEMORY;
	}
	pBuf->pDispInfoSET = NEW(DISPINFO_SET,1);
	if(pBuf->pDispInfoSET == NULL)
	{
		SAFELY_DELETE(pBuf->pDispInfoLCD);
		SAFELY_DELETE(pBuf->pDispInfoICO);
		SAFELY_DELETE(pBuf->pDispInfoMOD);
		return ERR_CFG_NO_MEMORY;
	}
	pBuf->pDispInfoDistr = NEW(DISPINFO_DISTRIB,1);
	if(pBuf->pDispInfoDistr == NULL)
	{
		SAFELY_DELETE(pBuf->pDispInfoLCD);
		SAFELY_DELETE(pBuf->pDispInfoICO);
		SAFELY_DELETE(pBuf->pDispInfoMOD);
		SAFELY_DELETE(pBuf->pDispInfoSET);
		return ERR_CFG_NO_MEMORY;
	}
	pBuf->pDispInfoTemp = NEW(DISPINFO_TEMP,1);
	if(pBuf->pDispInfoTemp == NULL)
	{
		SAFELY_DELETE(pBuf->pDispInfoLCD);
		SAFELY_DELETE(pBuf->pDispInfoICO);
		SAFELY_DELETE(pBuf->pDispInfoMOD);
		SAFELY_DELETE(pBuf->pDispInfoSET);
		SAFELY_DELETE(pBuf->pDispInfoDistr);
		return ERR_CFG_NO_MEMORY;
	}
	pBuf->pDispInfoBatt = NEW(DISOINFO_EACHBATT,1);
	if(pBuf->pDispInfoBatt == NULL)
	{
		SAFELY_DELETE(pBuf->pDispInfoLCD);
		SAFELY_DELETE(pBuf->pDispInfoICO);
		SAFELY_DELETE(pBuf->pDispInfoMOD);
		SAFELY_DELETE(pBuf->pDispInfoSET);
		SAFELY_DELETE(pBuf->pDispInfoDistr);
		SAFELY_DELETE(pBuf->pDispInfoTemp);
		return ERR_CFG_NO_MEMORY;
	}
	/*if(pBuf->pDispInfoLCD == NULL || pBuf->pDispInfoICO == NULL || pBuf->pDispInfoMOD == NULL
		|| pBuf->pDispInfoSET == NULL || pBuf->pDispInfoDistr == NULL || pBuf->pDispInfoTemp == NULL
		|| pBuf->pDispInfoBatt = NULL)
	{
		SAFELY_DELETE(pBuf->pDispInfoLCD);
		SAFELY_DELETE(pBuf->pDispInfoICO);
		SAFELY_DELETE(pBuf->pDispInfoMOD);
		SAFELY_DELETE(pBuf->pDispInfoSET);
		SAFELY_DELETE(pBuf->pDispInfoDistr);
		SAFELY_DELETE(pBuf->pDispInfoTemp);
		SAFELY_DELETE(pBuf->pDispInfoBatt);
		return  ERR_CFG_NO_MEMORY;
	}*/
	//printf("NEW DISPINFO_LCD OK\n");
	
	//1.Read tables: Page Info,Signal Info
	DEF_LOADER_ITEM(&loader[0], 
		NULL, &(pBuf->pDispInfoLCD->iDispInfoNum), 
		DISP_INFORMATION,&(pBuf->pDispInfoLCD->pSigInfoLCD), 
		ParseDispInfoTableProc);

	DEF_LOADER_ITEM(&loader[1], 
		NULL, &(pBuf->pDispInfoICO->iICOInfoNum), 
		ICOINFO_LCD,&(pBuf->pDispInfoICO->pSigInfoICO), 
		ParseIcoInfoTableProc);

	DEF_LOADER_ITEM(&loader[2], 
		NULL, &(pBuf->pDispInfoMOD->iMODInfoNum), 
		MODULE_INFO,&(pBuf->pDispInfoMOD->pSigInfoMOD), 
		ParseModuleInfoProc);
	DEF_LOADER_ITEM(&loader[3], 
		NULL, &(pBuf->pDispInfoDistr->iDistrNum), 
		DISTRIB_INFO,&(pBuf->pDispInfoDistr->pSigInfoDistr), 
		ParseDistribInfoProc);
	DEF_LOADER_ITEM(&loader[4], 
		NULL, &(pBuf->pDispInfoTemp->iTempNum), 
		TEMP_INFO,&(pBuf->pDispInfoTemp->pSigInfoTemp), 
		ParseTempInfoProc);
	DEF_LOADER_ITEM(&loader[5], 
		NULL, &(pBuf->pDispInfoBatt->iBattInfoNum), 
		BATTINFO_LCD,&(pBuf->pDispInfoBatt->pSigInfoBatt), 
		ParseBattInfoProc);
	DEF_LOADER_ITEM(&loader[6], 
		NULL, &(pBuf->pDispInfoSET->iSETInfoNum), 
		SETTING_INFO,&(pBuf->pDispInfoSET->pSigInfoSET), 
		ParseSetInfoProc);
	//printf("Parse OK\n");

	if (Cfg_LoadTables(pCfg,7,loader) != ERR_CFG_OK)
	{
		//printf("CFG Fail\n");
		return ERR_CFG_FAIL;
	}

	//DELETE(pBuf);
	
	//printf("CFG OK\n");
	return ERR_CFG_OK;	

}

static int Cfg_LoadUserDefLCDInfo(char *szConfigFile)
{
	int ret;
	ret = Cfg_LoadConfigFile(szConfigFile, LoadUserDefLCDInfoProc, &pUserDefLCD);

	if (ret != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}

DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{	
	DWORD dwExitCode;
	//dwExitCode = StartWebCommunicate(&pArgs->nQuitCommand);
	dwExitCode = QTCommunicate(&pArgs->nQuitCommand);

	DELETE(pUserDefLCD.pDispInfoLCD->pSigInfoLCD);
	DELETE(pUserDefLCD.pDispInfoICO->pSigInfoICO);
	DELETE(pUserDefLCD.pDispInfoMOD->pSigInfoMOD);
	DELETE(pUserDefLCD.pDispInfoSET->pSigInfoSET);
	DELETE(pUserDefLCD.pDispInfoDistr->pSigInfoDistr);
	DELETE(pUserDefLCD.pDispInfoTemp->pSigInfoTemp);
	DELETE(pUserDefLCD.pDispInfoBatt->pSigInfoBatt);

	DELETE(pUserDefLCD.pDispInfoLCD);
	DELETE(pUserDefLCD.pDispInfoICO);
	DELETE(pUserDefLCD.pDispInfoMOD);
	DELETE(pUserDefLCD.pDispInfoSET);
	DELETE(pUserDefLCD.pDispInfoDistr);
	DELETE(pUserDefLCD.pDispInfoTemp);
	DELETE(pUserDefLCD.pDispInfoBatt);

	return dwExitCode;

}

static int ServiceNotification(HANDLE hService, 
							   int nMsgType, 
							   int nTrapDataLength, 
							   void *lpTrapData,	
							   void *lpParam, 
							   BOOL bUrgent)
{
	int nError, nBufLen;
	
	
	PACK_ALMNUM*	pWrite;


	/*pWrite = (PACK_ALMNUM*)pAlm;
	//sem_trywait(psemNewAlm);//debug

	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
				ALARM_LEVEL_OBSERVATION,			
				0,		
				&nBufLen,			
				&pWrite->OANum,			
				0);

	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
				ALARM_LEVEL_MAJOR,			
				0,		
				&nBufLen,			
				&pWrite->MANum,			
				0);

	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
				ALARM_LEVEL_CRITICAL,			
				0,		
				&nBufLen,			
				&pWrite->CANum,			
				0);

	printf("MA = %d; CA = %d;\n", pWrite->MANum,pWrite->CANum);

	//munmap(pAlm, 20);
	//close(iMapHandle);
	sem_post(psemNewAlm);*/

	return 0;
}

/*BOOL	IsDisplay(int iEqIDType)
{
	int			i,j, iIdx, iEquipNum;
	int			iRst;
	int			iBufLen;
	int			iTemp[2];
	STDEQUIP_TYPE_INFO	*pEquipList;
	SET_SIG_INFO			*pSetSigInfo = NULL;

	//Get Quantity of all of equipments
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM, 
					0, 
					0, 
					&iBufLen, 
					&iEquipNum, 
					0);

	//Get point of Equipment List
	iRst = DxiGetData(VAR_STD_EQUIPS_LIST, 
					0, 
					0, 
					&iBufLen, 
					&pEquipList, 
					0);

	for(i = 0; i < iEquipNum;i++)
	{
		if((pEquipList + i)->iTypeID == iEqIDType)
		{
			pSetSigInfo = (pEquipList + i)->pSetSigInfo;
			if(STD_SIG_IS_DISPLAY_ON_UI(pSetSigInfo,DISPLAY_LCD) == TRUE)
				return TRUE;
		}
	}
	return FALSE;
}*/

void PackICOScreen(int iScreenID)
{
	SigINFO_ICO*		sTemp= NULL;
	SIG_BASIC_VALUE*	pSigValue = NULL;
	SAMPLE_SIG_INFO*	pSampSigInfo= NULL;
	SET_SIG_INFO*		pSetSigInfo = NULL;
	int		iCalMode = 0, nTimeOut = 0;		//Time out
	float			fTemp[3];
	int			i,j=0,k=1,m=0, nError,nBufLen = 0, iSigID;
	BOOL		bStat = FALSE;
	PACK_ICOINFO* pWrite = NULL;
	float fTmpVal = 0.0;
	int nTempType = 0;
	int nMaxTempVal=0;

	pWrite = (PACK_ICOINFO*)p;
	fTemp[0] = 0;
	fTemp[1] = 0;
	fTemp[2] = 0;

	//printf("ScreenID is OK!");

	for(i = 0;i < pUserDefLCD.pDispInfoICO->iICOInfoNum;i++)
	{
		sTemp = pUserDefLCD.pDispInfoICO->pSigInfoICO + i;

		if(sTemp->iPageID == iScreenID)
		{
			if(sTemp->iCalType == 255)
			{
				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					sTemp->iEquipID,			
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, sTemp->iSigID),
					&nBufLen,			
					(void *)&pSigValue,			
					0);
				
//				printf("iScreenID:%d iCalType:%d iEquipID:%d iSigID:%d\n",iScreenID,sTemp->iCalType,sTemp->iEquipID,sTemp->iSigID);

//				if((SIG_VALUE_IS_CONFIGURED(pSigValue)) && (SIG_VALUE_NEED_DISPLAY(pSigValue)))
				{
					if(k == 1)
					{
						nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
									sTemp->iEquipID,			
									DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, sTemp->iSigID),		
									&nBufLen,			
									&pSetSigInfo,			
									0);

						if((iScreenID == AMBT_ICO_SCREEN) || \
							(iScreenID == COMPT_ICO_SCREEN))
						{
							nTempType = DXI_GetTempSwitch(1,pSetSigInfo->fMaxValidValue,&(pWrite->LimitValue[0]),NULL);
							pWrite->LimitValue[0] = (nTempType == 0 )  ?  DEG_C_MAX_VALUE  :  DEG_F_MAX_VALUE;
							
							nTempType = DXI_GetTempSwitch(1,pSigValue->varValue.fValue,&(pWrite->LimitValue[1]),NULL);
							nMaxTempVal = (nTempType == 0) ? DEG_C_MAX_VALUE : DEG_F_MAX_VALUE;
							pWrite->LimitValue[1] = ((pWrite->LimitValue[1]) > nMaxTempVal) ? 
														nMaxTempVal : (pWrite->LimitValue[1]);

							nTempType = DXI_GetTempSwitch(1,pSetSigInfo->fMinValidValue,&(pWrite->LimitValue[5]),NULL);
							pWrite->LimitValue[5] = (nTempType == 0) ? DEG_C_MIN_VALUE : DEG_F_MIN_VALUE;
							
//							printf("LimitValue[0]:%f\nLimitValue[1]:%f\nLimitValue[5]:%f\n",
//														pWrite->LimitValue[0],
//														pWrite->LimitValue[1],
//														pWrite->LimitValue[5]);
						}
						else	
						{	
							pWrite->LimitValue[0] = pSetSigInfo->fMaxValidValue;
							pWrite->LimitValue[1] = pSigValue->varValue.fValue;
							pWrite->LimitValue[5] = pSetSigInfo->fMinValidValue;
						}
					}
					else if(k < (LIMNUM - 1))
					{
						//Added by wangminghui
						if((iScreenID == AMBT_ICO_SCREEN) || \
								(iScreenID == COMPT_ICO_SCREEN))
						{
							nTempType = DXI_GetTempSwitch(1,pSigValue->varValue.fValue,&(pWrite->LimitValue[k]),NULL);
							nMaxTempVal = (nTempType == 0) ? DEG_C_MAX_VALUE : DEG_F_MAX_VALUE;

							pWrite->LimitValue[k] = ((pWrite->LimitValue[k]) > nMaxTempVal) ?
															nMaxTempVal : (pWrite->LimitValue[k]);
//							printf("LimitValue[%d]:%f\n",k,pWrite->LimitValue[k]);
						}
						else
						{
							pWrite->LimitValue[k] = pSigValue->varValue.fValue;
						}
					}

					k++;
				}
				iCalMode = CALMODE_NORMAL;
			}
			else if(sTemp->iCalType == CALMODE_ACVOLT)
			{
				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					sTemp->iEquipID,			
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, sTemp->iSigID),
					&nBufLen,			
					(void *)&pSigValue,			
					0);

				if((SIG_VALUE_IS_CONFIGURED(pSigValue)) && (SIG_VALUE_NEED_DISPLAY(pSigValue)))
				{
					if((sTemp->iSigID == 1) && (fTemp[0] > 0))
					{
						continue;
					}
					else
					{
						if(pSigValue->ucType == VAR_FLOAT)
						{
							fTemp[m] = pSigValue->varValue.fValue;
						}
						else if(pSigValue->ucType == VAR_UNSIGNED_LONG)
						{
							fTemp[m] = (float)pSigValue->varValue.ulValue;
						}
						else if(pSigValue->ucType == VAR_LONG)
						{
							fTemp[m] = (float)pSigValue->varValue.lValue;
						}
						m++;
						iCalMode = CALMODE_ACVOLT;
					}
				}
			}
			else
			{
				if(iCalMode == CALMODE_ACVOLT)
				{
					nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					EQIPID_RECTGRP,			
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_ACPHASE),
					&nBufLen,			
					(void *)&pSigValue,			
					0);

					if(pSigValue->varValue.enumValue == ACSingle)
					{
						iSigID = sTemp->iSigID;
					}
					else
					{
						iSigID = sTemp->iSigID + 3;
					}
				}
				else
				{
					iSigID = sTemp->iSigID;
				}

				nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
					sTemp->iEquipID,			
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, iSigID),		
					&nBufLen,			
					&pSampSigInfo,			
					0);

				pWrite->DispData[j].iFormat = *(pSampSigInfo->szValueDisplayFmt + 1) - 0x30;
				pWrite->DispData[j].iEquipID = sTemp->iEquipID;
				pWrite->DispData[j].iSigID = iSigID;
				strncpyz(pWrite->DispData[j].cSigName, pSampSigInfo->pSigName->pAbbrName[iLangType], MAXLEN_NAME);

				strncpyz(pWrite->DispData[j].cSigUnit, pSampSigInfo->szSigUnit, MAXLEN_UNIT);

				nError = DxiGetData(VAR_A_SIGNAL_VALUE, 
							sTemp->iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, iSigID),
							&nBufLen,			
							(void *)&pSigValue,			
							0);

//				printf("11111  iEquipId:%d SigType:%d iSigId:%d\n",sTemp->iEquipID,SIG_TYPE_SAMPLING,iSigID);

				if((SIG_VALUE_IS_CONFIGURED(pSigValue)) && (SIG_VALUE_NEED_DISPLAY(pSigValue)))
				{
					pWrite->DispData[j].fSigValue = pSigValue->varValue.fValue;
				}
				else
				{
					pWrite->DispData[j].fSigValue = -9999;
				}

				
				//Èç¹ûÊÇ»ñÈ¡ ÎÂ¶È£¬ÔòÒª¸ù¾Ýµ±Ç°µÄ»·¾³½«ÉãÊÏÎÂ¶È×ª»»³É»ªÊÏ¶È¡£
				//Added by wangminghui
				if((iScreenID == AMBT_ICO_SCREEN) || \
					(iScreenID == COMPT_ICO_SCREEN))
				{
					//printf("11111 varValue.fValue:%f\t varValue.fValue:%f\n",pSigValue->varValue.fValue,pWrite->DispData[j].fSigValue);
					memset(pWrite->DispData[j].cSigUnit,0,MAXLEN_UNIT);
					DXI_GetTempSwitch(1,pWrite->DispData[j].fSigValue,&(pWrite->DispData[j].fSigValue),pWrite->DispData[j].cSigUnit);
					//printf("22222 varValue.fValue:%f\t varValue.fValue:%f\n",pSigValue->varValue.fValue,pWrite->DispData[j].fSigValue);
				}
				j++;
			}
		}
	}

	pWrite->iDataNum = j;
	if(iCalMode == CALMODE_ACVOLT)
	{
		if(fTemp[0] < 300)
		{
			pWrite->LimitValue[0] = 400;
			pWrite->LimitValue[5] = 50;
		}
		else
		{
			pWrite->LimitValue[0] = 600;
			pWrite->LimitValue[5] = 120;
		}

		float fTempValue = 0.0;
		fTempValue = fTemp[0] * (1 + fTemp[2]/100) ;
		
		pWrite->LimitValue[1] = (fTempValue >  (pWrite->LimitValue[0])) ? (pWrite->LimitValue[0] )
															: ((fTempValue <  (pWrite->LimitValue[5]))  ?  (pWrite->LimitValue[5]) 
															: (fTempValue));
		
		fTempValue = fTemp[0] * (1 + fTemp[1]/100);
		pWrite->LimitValue[2] = (fTempValue >  (pWrite->LimitValue[0])) ? (pWrite->LimitValue[0] )
															: ((fTempValue <  (pWrite->LimitValue[5]))  ?  (pWrite->LimitValue[5]) 
															: (fTempValue));

		fTempValue = fTemp[0] * (1 - fTemp[1]/100);
		pWrite->LimitValue[3] = (fTempValue >  (pWrite->LimitValue[0])) ? (pWrite->LimitValue[0] )
															: ((fTempValue <  (pWrite->LimitValue[5]))  ?  (pWrite->LimitValue[5]) 
															: (fTempValue));
		fTempValue = fTemp[0] * (1 - fTemp[2]/100);
		pWrite->LimitValue[4] = (fTempValue >  (pWrite->LimitValue[0])) ? (pWrite->LimitValue[0] )
															: ((fTempValue <  (pWrite->LimitValue[5]))  ?  (pWrite->LimitValue[5]) 
															: (fTempValue));
	}
}

static char *GetHisAlmName(IN int iSignalID,  
			IN int iEquipID, 
			IN int iLanguage)
{
	ALARM_SIG_INFO	*pSigInfo = NULL;
	int		iVarSubID, iError = 0;
	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_ALARM,iSignalID);
	char	*szSignalName = NULL;
	int				iBufLen = 0;
	int				iTimeOut = 0;

	szSignalName = NEW(char, MAXLEN_NAME);
	if(szSignalName == NULL)
	{
		return NULL;
	}
	memset(szSignalName, 0x0, MAXLEN_NAME);
		
	iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
				iEquipID,			
				iVarSubID,		
				&iBufLen,			
				(void *)&pSigInfo,			
				iTimeOut);
		
	if (iError == ERR_DXI_OK)
	{
		strncpyz(szSignalName, pSigInfo->pSigName->pAbbrName[iLanguage], MAXLEN_NAME);

		return	szSignalName;
	}
	else
	{
		DELETE(szSignalName);
		return NULL;
	}	
}

static int fnCompNumCondition(IN const void *pRecord,IN const void *pTCondition)
{
	HIS_CONDITION *pCondition = (HIS_CONDITION *)pTCondition;
	static BYTE		bHisCount = 0;
	char		*pAlmName;

	//printf("Comp begin!\n");
	if((pRecord == NULL) || (pTCondition == NULL))
	{
		//printf("Data OK!\n");
		return FALSE;
	}

	HIS_ALARM_RECORD	*pRecordData = (HIS_ALARM_RECORD *)pRecord;
			
	bHisCount++;
	if(bHisCount % 200 == 0)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));
		bHisCount = 0;
	}

	//pAlmName = GetHisAlmName(pRecordData->iAlarmID,  
	//		pRecordData->iEquipID, 
	//		iLangType);

	//printf("AlarmLevel is %d!!!\n", pRecordData->byAlarmLevel);

	if(pRecordData->byAlarmLevel == pCondition->iLevel)
	{
                return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static int fnCompareCondition(IN const void *pRecord,IN const void *pTCondition)
{
	HIS_CONDITION *pCondition = (HIS_CONDITION *)pTCondition;
	static BYTE		bHisAlmCount = 0;
	char		*pAlmName;

	if((pRecord == NULL) || (pTCondition == NULL))
	{
		return FALSE;
	}

	HIS_ALARM_RECORD	*pRecordData = (HIS_ALARM_RECORD *)pRecord;
			
	bHisAlmCount++;
	if(bHisAlmCount % 200 == 0)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));
		bHisAlmCount = 0;
	}

	//pAlmName = GetHisAlmName(pRecordData->iAlarmID,  
	//		pRecordData->iEquipID, 
	//		iLangType);
	if(pRecordData->byAlarmLevel == 1)
		iOBAlmNum++;
	else if(pRecordData->byAlarmLevel == 2)
		iMAlmNum++;
	else if(pRecordData->byAlarmLevel == 3)
		iCAlmNum++;

	//printf("His Alm Num=%d;%d;%d!\n",iOBAlmNum,iMAlmNum,iCAlmNum);

	if(pRecordData->iEquipID == pCondition->iEquipID || pCondition->iEquipID < 0)
	{
                return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*==========================================================================*
* FUNCTION :    QT_GetEquipNameFromInterface
* PURPOSE  :	 Get EquipName
* CALLS    : 
* CALLED BY:	
* ARGUMENTS:IN int iEquipID,IN int iLanguage,IN int iPosition
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2013-11-27 11:20
*==========================================================================*/
static int QT_GetEquipName(IN OUT char *szEquipName)
{
#define IS_NUMBER(c)								(((c)>=(0x30) && (c)<=(0x39)) || (c) == 0x23)
	char *p=szEquipName;
	int i=0;
	while(*p)
	{
		if(IS_NUMBER(*p))
		{
			p=p++;
			break;
		}
		else
		{
			p++;
			i++;
		}
	}	
	return i;
}
static char *QT_GetEquipNameFromInterface(IN int iEquipID,IN int iLanguage,IN int iPosition)
{


	char	*szEquipName = NULL;
	int			iPos, iBufLen;
	EQUIP_INFO		*pEquipInfo = NULL;

	int iError = DxiGetData(VAR_A_EQUIP_INFO,			
		iEquipID,	
		0,
		&iBufLen,			
		&pEquipInfo,			
		0);

	szEquipName = NEW(char, 64);


	if(szEquipName == NULL)
	{
		return NULL;
	}
	memset(szEquipName, 0, 64);


	if(iError == ERR_DXI_OK)
	{		
		if(iPosition != 0)
		{
			strncpyz(szEquipName, pEquipInfo->pEquipName->pAbbrName[iLanguage], 64);
			iPos = QT_GetEquipName(pEquipInfo->pEquipName->pAbbrName[iLanguage]);
			snprintf(szEquipName + iPos, 5, "#%d", iPosition);			
		}				
		else
		{
			strncpyz(szEquipName, pEquipInfo->pEquipName->pAbbrName[iLanguage], 64);
		}


		return szEquipName;

	}
	else
	{
		SAFELY_DELETE(szEquipName);
		return NULL;
	}
}
void PackHisAlmNum(int iScreenID)
{
	PACK_ALMNUM*	pWrite;
	int		iNum, iRecords = 4000, iResult,iStartRecordNo = -1;
	char		*pAlmName;
	PACK_HISALM	*pHisAlm;
	EQUIP_INFO*	pEquipInfo;
	int		i, nError, nBufLen;
	char*		pStrName;
	HANDLE		hHisData;
	char		*pszEquipName;

	hHisData = DAT_StorageOpen(HIST_ALARM_LOG);
	if(iScreenID == HISALMNUM_SCREEN)
	{
		pWrite = (PACK_ALMNUM*)p;
		if(hHisData == NULL)
		{
			pWrite->CANum = 0;
			pWrite->MANum = 0;
			pWrite->OANum = 0;
			return;
		}
	}
	else
	{
		pHisAlm = (PACK_HISALM*)p;
		if(hHisData == NULL)
		{
			pHisAlm->HisAlmNum = 0;
			return;
		}
	}

	/*hHisData = DAT_StorageOpen(HIST_ALARM_LOG);
	if(hHisData == NULL)
	{
		pWrite->CANum = 0;
		pWrite->MANum = 0;
		pWrite->OANum = 0;
		return;
	}*/

	HIS_ALARM_RECORD *pHisDataRecord = NEW(HIS_ALARM_RECORD, 4000);
	ASSERT(pHisDataRecord);		
	memset(pHisDataRecord, 0x0, 4000 * sizeof(HIS_ALARM_RECORD));
	HIS_CONDITION *pCondition	= NEW(HIS_CONDITION,1);
	ASSERT(pCondition);	
	pCondition->iNum		= 4000;
	pCondition->iLevel	= 1;
	pCondition->iEquipID	= -1;
	iOBAlmNum = 0;
	iMAlmNum = 0;
	iCAlmNum = 0;
	TotalAlmNum = 0;

	iResult = DAT_StorageFindRecords(hHisData,
					fnCompareCondition, 
					(void *)pCondition,
					&iStartRecordNo, 
					&iRecords, 
					(void*)pHisDataRecord, 
					FALSE,
					FALSE);
	TotalAlmNum = iRecords;

	//printf("!!hisAlm Read OK! TotalAlmNum=%d; iResult=%d\n", TotalAlmNum,iResult);

	if(iScreenID == HISALMNUM_SCREEN)
	{
		pWrite->OANum = iOBAlmNum;
		pWrite->MANum = iMAlmNum;
		pWrite->CANum = iCAlmNum;
		//printf("TotalAlmNum=%d\n",TotalAlmNum);
		//printf("OANUM=%d\n",pWrite->OANum);
		//printf("MANUM=%d\n",pWrite->MANum);
		//printf("CANUM=%d\n",pWrite->CANum);
		DELETE(pHisDataRecord);
		DELETE(pCondition);
		DAT_StorageClose(hHisData);
		return;
	}

	if(TotalAlmNum > 500)
		iNum = 500;
	else
		iNum = TotalAlmNum;
	pHisAlm->HisAlmNum = iNum;

	for(i = 0;i < iNum;i++)
	{
		//printf("i = %d;  ",i);
		nError = DxiGetData(VAR_A_EQUIP_INFO,
			pHisDataRecord[i].iEquipID,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0);

		pszEquipName = QT_GetEquipNameFromInterface(pHisDataRecord[i].iEquipID, iLangType, pHisDataRecord[i].iPosition); 
		memset(pHisAlm->HisAlmItem[i].EquipName, 0x0, MAXLEN_NAME);
		if(pszEquipName != NULL)
		{
			strncpyz(pHisAlm->HisAlmItem[i].EquipName, pszEquipName, MAXLEN_NAME);
			DELETE(pszEquipName);
			pszEquipName = NULL;
		}
		else
		{
			//strncpyz(pDataReturn->szEquipName,"--",
			//	sizeof(pDataReturn->szEquipName));
			strncpyz(pHisAlm->HisAlmItem[i].EquipName, pEquipInfo->pEquipName->pAbbrName[iLangType], MAXLEN_NAME);
		}		

		
		pStrName = GetHisAlmName(pHisDataRecord[i].iAlarmID,  
								pHisDataRecord[i].iEquipID, 
								iLangType);
		//printf("Name %d = %s  ",i,pStrName);
		memset(pHisAlm->HisAlmItem[i].AlmName, 0x0, 20);
		strncpyz(pHisAlm->HisAlmItem[i].AlmName, pStrName, 20);
		//printf("Name %d = %s  ",i,pHisAlm->HisAlmItem[i].AlmName);
		pHisAlm->HisAlmItem[i].AlmLevel = pHisDataRecord[i].byAlarmLevel;
		pHisAlm->HisAlmItem[i].StartTime = pHisDataRecord[i].tmStartTime;
		//printf("StartTime =%d",pHisAlm->HisAlmItem[i].StartTime);
		pHisAlm->HisAlmItem[i].EndTime = pHisDataRecord[i].tmEndTime;
		//printf("EndTime =%d, ",pHisAlm->HisAlmItem[i].EndTime);
		DELETE(pStrName);
	}
	
	DELETE(pHisDataRecord);
	DELETE(pCondition);
	DAT_StorageClose(hHisData);

}
/*void PackHisAlm(int iScreenID)
{
	int		iRecords = 4000, iResult,iStartRecordNo = -1;
	HANDLE		hHisData;
	char		*pAlmName;
	PACK_HISALM	*pHisAlm;
	EQUIP_INFO*	pEquipInfo;
	int		i, nError, nBufLen;
	char*		pStrName;

	pHisAlm = (PACK_HISALM*)p;

	hHisData = DAT_StorageOpen(HIST_ALARM_LOG);
	if(hHisData == NULL)
	{
		pHisAlm->HisAlmNum = 0;
		return;
	}

	HIS_ALARM_RECORD *pHisDataRecord = NEW(HIS_ALARM_RECORD, 4000);
	ASSERT(pHisDataRecord);		
	memset(pHisDataRecord, 0x0, 4000 * sizeof(HIS_ALARM_RECORD));
	HIS_CONDITION *pCondition	= NEW(HIS_CONDITION,1);
	pCondition->iNum		= 200;
	pCondition->iEquipID	= -1;

	iResult = DAT_StorageFindRecords(hHisData,
					fnCompareCondition, 
					(void *)pCondition,
					&iStartRecordNo, 
					&iRecords, 
					(void*)pHisDataRecord, 
					FALSE,
					FALSE);

	pHisAlm->HisAlmNum = iRecords;

	//printf("HisAlmNumA:%d\n",iRecords);

	for(i = 0;i < pHisAlm->HisAlmNum;i++)
	{
		//printf("i = %d;  ",i);
		nError = DxiGetData(VAR_A_EQUIP_INFO,
			pHisDataRecord[i].iEquipID,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0);
		memset(pHisAlm->HisAlmItem[i].EquipName, 0x0, 20);
		strncpyz(pHisAlm->HisAlmItem[i].EquipName, pEquipInfo->pEquipName->pAbbrName[0], 20);
		pStrName = GetHisAlmName(pHisDataRecord[i].iAlarmID,  
								pHisDataRecord[i].iEquipID, 
								0);
		//printf("Name %d = %s  ",i,pStrName);
		memset(pHisAlm->HisAlmItem[i].AlmName, 0x0, 20);
		strncpyz(pHisAlm->HisAlmItem[i].AlmName, pStrName, 20);
		//printf("Name %d = %s  ",i,pHisAlm->HisAlmItem[i].AlmName);
		pHisAlm->HisAlmItem[i].AlmLevel = pHisDataRecord[i].byAlarmLevel;
		pHisAlm->HisAlmItem[i].StartTime = pHisDataRecord[i].tmStartTime;
		//printf("StartTime =%d",pHisAlm->HisAlmItem[i].StartTime);
		pHisAlm->HisAlmItem[i].EndTime = pHisDataRecord[i].tmEndTime;
		//printf("EndTime =%d, ",pHisAlm->HisAlmItem[i].EndTime);
		//memset(pHisAlm->HisAlmItem[i].EquipSN, 0x0, 20);
		//strncpyz(pHisAlm->HisAlmItem[i].EquipSN, pHisDataRecord[i].szSerialNumber,32);
		//printf("SN = %s\n", pHisAlm->HisAlmItem[i].EquipSN);
		
	}


	DELETE(pHisDataRecord);
	DAT_StorageClose(hHisData);

}*/

void PackActALMScreen(void)
{
	int			i,iAlmNumTemp,nError,nBufLen = 0;
	int			nTimeOut = 0;		//Time out
	PACK_ACTALM*		pActAlm;
	ALARM_SIG_VALUE*	pActiveAlarmSigValue = NULL;

	pActAlm = (PACK_ACTALM*)p;

	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
		ALARM_LEVEL_NONE,			
		0,		
		&nBufLen,			
		&iAlmNumTemp,			
		0);

	if(iAlmNumTemp != 0)
	{
		pActiveAlarmSigValue = NEW(ALARM_SIG_VALUE, iAlmNumTemp);
		if(pActiveAlarmSigValue == NULL)
		{
			pActAlm->ActAlmNum = 0;
			return;
		}
		nBufLen = sizeof(ALARM_SIG_VALUE) * iAlmNumTemp;

		nError = DxiGetData(VAR_ACTIVE_ALARM_INFO,
					ALARM_LEVEL_NONE,			
					0,		
					&nBufLen,			
					pActiveAlarmSigValue,			
					0);
		if(nError != ERR_DXI_OK)
		{
			//printf("Get Alarm OK! nBufLen = %d\n",(nBufLen / sizeof(ALARM_SIG_VALUE)));
			pActAlm->ActAlmNum = 0;
			return;
		} 

		pActAlm->ActAlmNum = nBufLen / sizeof(ALARM_SIG_VALUE);
		// *ppSigValue = (ALARM_SIG_VALUE *)pActiveAlarmSigValue;
		DXI_MakeAlarmSigValueSort(pActiveAlarmSigValue, pActAlm->ActAlmNum);
		//printf("Sort OK!\n");
		if(pActAlm->ActAlmNum > 200)
			pActAlm->ActAlmNum = 200;
		
		for(i = 0;i < pActAlm->ActAlmNum;i++)
		{

			//printf ("ALM No.%d!\n",i);
			strncpyz(pActAlm->ActAlmItem[i].AlmName, 
				pActiveAlarmSigValue[i].pStdSig->pSigName->pAbbrName[iLangType],MAXLEN_NAME);
			strncpyz(pActAlm->ActAlmItem[i].EquipName,
				pActiveAlarmSigValue[i].pEquipInfo->pEquipName->pAbbrName[iLangType],MAXLEN_NAME);
			pActAlm->ActAlmItem[i].AlmLevel = pActiveAlarmSigValue[i].iAlarmLevel;
			pActAlm->ActAlmItem[i].StartTime = pActiveAlarmSigValue[i].sv.tmStartTime;
			if(pActiveAlarmSigValue[i].pStdSig->pHelpInfo != NULL)
			{
				strncpyz(pActAlm->ActAlmItem[i].AlmHelp, 
					pActiveAlarmSigValue[i].pStdSig->pHelpInfo->pAbbrName[iLangType],MAXNUM_HELP);
			}
			else
			{
				pActAlm->ActAlmItem[i].AlmHelp[0] = 0;
			}
			
			//changed by Frank Wu,20131211,2/26 for twinkle of the green led of rectifier
			pActAlm->ActAlmItem[i].iEqIDType = pActiveAlarmSigValue[i].pEquipInfo->iEquipTypeID;
			pActAlm->ActAlmItem[i].iEquipID = pActiveAlarmSigValue[i].pEquipInfo->iEquipID;
			pActAlm->ActAlmItem[i].iSigID = pActiveAlarmSigValue[i].pStdSig->iSigID;
		}
		DELETE(pActiveAlarmSigValue);
	}
	
}

void PackActAlmNumScreen(void)
{
	int			i,iAlmNumTemp,nError,nBufLen = 0;
	int			nTimeOut = 0;		//Time out
	PACK_ALMNUM*		pActAlmNum = NULL;;
	ALARM_SIG_VALUE*	pActiveAlarmSigValue = NULL;
	int OANum = 0,CANum = 0,MANum = 0,nTotalAlmNum = 0;

	pActAlmNum = (PACK_ALMNUM*)p;

	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
		ALARM_LEVEL_NONE,			
		0,		
		&nBufLen,			
		&iAlmNumTemp,			
		0);

	if(iAlmNumTemp != 0)
	{
		pActiveAlarmSigValue = NEW(ALARM_SIG_VALUE, iAlmNumTemp);
		if(pActiveAlarmSigValue == NULL)
		{
			pActAlmNum->CANum = CANum;
			pActAlmNum->MANum = MANum;
			pActAlmNum->OANum = OANum;

			return;
		}
		nBufLen = sizeof(ALARM_SIG_VALUE) * iAlmNumTemp;

		nError = DxiGetData(VAR_ACTIVE_ALARM_INFO,
					ALARM_LEVEL_NONE,			
					0,		
					&nBufLen,			
					pActiveAlarmSigValue,			
					0);
		if(nError != ERR_DXI_OK)
		{
			pActAlmNum->CANum = CANum;
			pActAlmNum->MANum = MANum;
			pActAlmNum->OANum = OANum;
			DELETE(pActiveAlarmSigValue);
			return;
		} 

		nTotalAlmNum = nBufLen / sizeof(ALARM_SIG_VALUE);
		// *ppSigValue = (ALARM_SIG_VALUE *)pActiveAlarmSigValue;
		DXI_MakeAlarmSigValueSort(pActiveAlarmSigValue,nTotalAlmNum);

		if(nTotalAlmNum > 200)
		{
			nTotalAlmNum = 200;
		}
		
		for(i = 0;i < nTotalAlmNum;i++)
		{		
			 switch(pActiveAlarmSigValue[i].iAlarmLevel)
			 {
				case ALARM_LEVEL_NONE:
				{
					;
				}
				break;
				
				case ALARM_LEVEL_OBSERVATION:
				{
					OANum ++;
				}
				break;
				
				case ALARM_LEVEL_MAJOR:
				{
					MANum ++;
				}
				break;
				
				case ALARM_LEVEL_CRITICAL:
				{
					CANum ++;
				}
				break;
			 }
		}
		DELETE(pActiveAlarmSigValue);
	}

	pActAlmNum->CANum = CANum;
	pActAlmNum->MANum = MANum;
	pActAlmNum->OANum = OANum;
}

float GetRatedCurr(void)
{
	float fRateCurr = 0;
	int	nBufLen = 0;
	SIG_BASIC_VALUE*	pSigValue;

	if(DXI_IsEquipExist(EQIPID_RECTGRP))
	{
		DxiGetData(VAR_A_SIGNAL_VALUE,
				EQIPID_RECTGRP,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_RATEDCURR),	
				&nBufLen,			
				(void *)&pSigValue,			
				0);
		fRateCurr += pSigValue->varValue.fValue;
	}
	//printf ("fRateCurr R = %f\n",fRateCurr);

	if(DXI_IsEquipExist(EQIPID_MPPTGRP))
	{
		DxiGetData(VAR_A_SIGNAL_VALUE,
				EQIPID_MPPTGRP,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MPPT_RATEDCURR),	
				&nBufLen,			
				(void *)&pSigValue,			
				0);
		fRateCurr += pSigValue->varValue.fValue;
	}
	//printf ("fRateCurr M = %f\n",fRateCurr);

	if(DXI_IsEquipExist(EQIPID_SLAVERECTGRP1))
	{
		DxiGetData(VAR_A_SIGNAL_VALUE,
				EQIPID_SLAVERECTGRP1,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SLAVE_RATEDCURR),	
				&nBufLen,			
				(void *)&pSigValue,			
				0);
		fRateCurr += pSigValue->varValue.fValue;
	}
	//printf ("fRateCurr S1 = %f\n",fRateCurr);

	if(DXI_IsEquipExist(EQIPID_SLAVERECTGRP2))
	{
		DxiGetData(VAR_A_SIGNAL_VALUE,
				EQIPID_SLAVERECTGRP2,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SLAVE_RATEDCURR),	
				&nBufLen,			
				(void *)&pSigValue,			
				0);
		fRateCurr += pSigValue->varValue.fValue;
	}
	//printf ("fRateCurr S2 = %f\n",fRateCurr);

	if(DXI_IsEquipExist(EQIPID_SLAVERECTGRP3))
	{
		DxiGetData(VAR_A_SIGNAL_VALUE,
				EQIPID_SLAVERECTGRP3,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SLAVE_RATEDCURR),	
				&nBufLen,			
				(void *)&pSigValue,			
				0);
		fRateCurr += pSigValue->varValue.fValue;
	}

	return fRateCurr;
}

// changed by Frank Wu,20131213,5/8, for keep the same used rated current percent between WEB and LCD
/*==========================================================================*
* FUNCTION : QueryRatedCurrentPercent
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*           
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
* DATE: 
*==========================================================================*/
static float QueryRatedCurrentPercent(void)
{
	static float s_fLatestUpdate = 0;
	int i, count;
	float fUpdate[5];

	//read time for several times
	count = sizeof(fUpdate)/sizeof(fUpdate[0]);
	for(i = 0; i < count; i++)
	{
		fUpdate[i] = g_SiteInfo.pstWebLcdShareData->fRatedCurrPercent;
		Sleep(5);//read interval
	}
	//checking data of reading
	for(i = 1; i < count; i++)
	{
		if(fUpdate[i - 1] != fUpdate[i])
		{
			return  s_fLatestUpdate;//read data error
		}
	}

	//update the latest refreshing data
	s_fLatestUpdate = fUpdate[0];

	return s_fLatestUpdate;
}

void ProcessShuntSourcesInfo()
{
	int i=0;
	int nSigIndex = 0;
	int nEquipId = 0;
	int nSigID = 0;
	int nError = ERR_OK;
	int nBufLen = 0;
	char szSigNameTemp[100]={0};
	PACK_INFO*		pSigVal = NULL;
	SAMPLE_SIG_INFO *pSamSigInfo = NULL;
	void *pSigInfo = NULL;
	SIG_BASIC_VALUE*	pSigValue = NULL;
	EQUIP_INFO*	pEquipInfo = NULL;
	SAMPLE_SIG_VALUE *pSampSigValue = NULL;
	
	pSigVal = (PACK_INFO*)p;
	//EIB
	//¼ì²éËüÃÇµÄshuntÊÇ·ñÉèÖÃÎªsource
	for(nEquipId=0;(EQP_ID_EIB_START+nEquipId)<=EQP_ID_EIB_END;nEquipId ++)
	{
		
		for(nSigID = 0;(SIG_ID_EIB_SHUNT1_IS_SOURCE+nSigID) <= SIG_ID_EIB_SHUNT3_IS_SOURCE;nSigID ++)
		{
			nError = DxiGetData(VAR_A_SIGNAL_VALUE,
				(EQP_ID_EIB_START+nEquipId),			
				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, nSigID+SIG_ID_EIB_SHUNT1_IS_SOURCE),
				&nBufLen,			
				(void *)&pSigValue,			
				0);
			if((nError == ERR_OK) && (pSigValue != NULL))
			{
//				printf("iEquipId:%d\t iSigId:%d\tiSigType:%d\n",(EQP_ID_EIB_START+nEquipId),nSigID+SIG_ID_EIB_SHUNT1_IS_SOURCE,
//					SIG_TYPE_SETTING);
//				printf("-------------EnumIndex:%d\n",pSigValue->varValue.enumValue);
//				printf("-------------longIndex:%d\n",pSigValue->varValue.lValue);
				if(pSigValue->varValue.enumValue == ENUM_SHUNT_SET_AS_SOURCE)
				{
					//Íâ²ãÇ¶Ì×ÅÐ¶ÏEIBµÄshuntÊÇÉèÖÃÎªsource.
					//´ËÊ±»ñÈ¡ÐÅºÅÃû¼°µçÁ÷Öµ¡£
					nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						(EQP_ID_EIB_START+nEquipId),			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, nSigID+SIG_ID_EIB_SHUNT1_CURRENT),
						&nBufLen,			
						(void *)&pSigValue,			
						0);
					if((nError == ERR_OK) && (pSigValue != NULL))
					{
						//¿½±´ÐÅºÅÖµ
						memset(&(pSigVal[nSigIndex].vSigValue),0,sizeof(VAR_VALUE));
						memcpy(&(pSigVal[nSigIndex].vSigValue),&(pSigValue->varValue),sizeof(VAR_VALUE));
//						printf("111111111111111111\n");

						//»ñÈ¡¸¡µãÐÅºÅ¾«¶È
						pSampSigValue = (SAMPLE_SIG_VALUE*)pSigValue;
						pSamSigInfo = (SAMPLE_SIG_INFO*)(pSampSigValue->pStdSig);
						pSigVal[nSigIndex].iFormat = *(pSamSigInfo->szValueDisplayFmt + 1) - 0x30;
//						printf("222222222222222222\n");

						//ÐÅºÅÃû
						//ÕâÀïÒª¶ÔÐÅºÅÃû×öÌØÊâ´¦Àí.
						//ÐÅºÅÃû=Éè±¸Ãû+shuntµçÁ÷ÐÅºÅÃû
						nError = DxiGetData(VAR_A_EQUIP_INFO,
							(EQP_ID_EIB_START+nEquipId),			
							0,
							&nBufLen,			
							(void *)&pEquipInfo,			
							0);
						if((nError == ERR_OK) && (pEquipInfo != NULL))
						{
							memset(szSigNameTemp,0,100);
							memcpy(szSigNameTemp,pEquipInfo->pEquipName->pAbbrName[iLangType],MAXLEN_NAME);
//							printf("3333333333333333333\n");

							strncat(szSigNameTemp," ",1);

							strncat(szSigNameTemp,pSampSigValue->pStdSig->pSigName->pAbbrName[iLangType],\
								strlen(pSampSigValue->pStdSig->pSigName->pAbbrName[iLangType]));
//							printf("444444444444444444444444444\n");

							memset(pSigVal[nSigIndex].cSigName,0,MAXLEN_NAME);
							memcpy(pSigVal[nSigIndex].cSigName,szSigNameTemp,strlen(szSigNameTemp));

//							printf("555555555555555555555\n");
						}
						//µ¥Î»
						memset(pSigVal[nSigIndex].cSigUnit,0,MAXLEN_UNIT);
						memcpy(pSigVal[nSigIndex].cSigUnit,pSamSigInfo->szSigUnit,MAXLEN_UNIT);
//						printf("666666666666666666666666");
						
						nSigIndex++;
					}					
				}
			}
		}
	}

	//SMDU
	for(nEquipId=0;(EQP_ID_SMDU_UNIT_START+nEquipId)<=EQP_ID_SMDU_UNIT_END;nEquipId ++)
	{
		
		for(nSigID = 0;(SIG_ID_SMDU_SHUNT1_IS_SOURCE+nSigID) <= SIG_ID_SMDU_SHUNT5_IS_SOURCE;nSigID ++)
		{
			nError = DxiGetData(VAR_A_SIGNAL_VALUE,
				(EQP_ID_SMDU_UNIT_START+nEquipId),			
				DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, nSigID+SIG_ID_SMDU_SHUNT1_IS_SOURCE),
				&nBufLen,			
				(void *)&pSigValue,			
				0);
			if((nError == ERR_OK) && (pSigValue != NULL))
			{
				if(pSigValue->varValue.enumValue == ENUM_SHUNT_SET_AS_SOURCE)
				{
					//Íâ²ãÇ¶Ì×ÅÐ¶ÏEIBµÄshuntÊÇÉèÖÃÎªsource.
					//´ËÊ±»ñÈ¡ÐÅºÅÃû¼°µçÁ÷Öµ¡£
					nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						(EQP_ID_SMDU_UNIT_START+nEquipId),			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, nSigID+SIG_ID_SMDU_SHUNT1_CURRENT),
						&nBufLen,			
						(void *)&pSigValue,			
						0);
					if((nError == ERR_OK) && (pSigValue != NULL))
					{
						//¿½±´ÐÅºÅÖµ
						memset(&(pSigVal[nSigIndex].vSigValue),0,sizeof(VAR_VALUE));
						memcpy(&(pSigVal[nSigIndex].vSigValue),&(pSigValue->varValue),sizeof(VAR_VALUE));

						//»ñÈ¡¸¡µãÐÅºÅ¾«¶È
						pSampSigValue = (SAMPLE_SIG_VALUE*)pSigValue;
						pSamSigInfo = (SAMPLE_SIG_INFO*)(pSampSigValue->pStdSig);
						pSigVal[nSigIndex].iFormat = *(pSamSigInfo->szValueDisplayFmt + 1) - 0x30;

						//ÐÅºÅÃû
						//ÕâÀïÒª¶ÔÐÅºÅÃû×öÌØÊâ´¦Àí.
						//ÐÅºÅÃû=Éè±¸Ãû+shuntµçÁ÷ÐÅºÅÃû
						nError = DxiGetData(VAR_A_EQUIP_INFO,
							(EQP_ID_SMDU_UNIT_START+nEquipId),			
							0,
							&nBufLen,			
							(void *)&pEquipInfo,			
							0);
						if((nError == ERR_OK) && (pEquipInfo != NULL))
						{
							memset(szSigNameTemp,0,100);
							memcpy(szSigNameTemp,pEquipInfo->pEquipName->pAbbrName[iLangType],MAXLEN_NAME);

							strncat(szSigNameTemp,pSampSigValue->pStdSig->pSigName->pAbbrName[iLangType],\
								strlen(pSampSigValue->pStdSig->pSigName->pAbbrName[iLangType]));
						}

						memset(pSigVal[nSigIndex].cSigName,0,MAXLEN_NAME);
						memcpy(pSigVal[nSigIndex].cSigName,szSigNameTemp,strlen(szSigNameTemp));

						//µ¥Î»
						memset(pSigVal[nSigIndex].cSigUnit,0,MAXLEN_UNIT);
						memcpy(pSigVal[nSigIndex].cSigUnit,pSamSigInfo->szSigUnit,MAXLEN_UNIT);
						
						nSigIndex++;
					}					
				}
			}
		}
	}	

//	printf("***************%d**********\n",nSigIndex);
	
	pSigVal[0].iDataNum = nSigIndex;
	for(i = 0; i < pSigVal[0].iDataNum; i++)
	{
		pSigVal[i].iDataNum = pSigVal[0].iDataNum;
	}	
}

void PackMainScreen(int iScreenID)
{
//changed by Frank Wu,20131216,1/2, for slave rectifiers display exception when the first running  after APP updating
#define SIGID_SLAVERECT_EXIST_STATE		100

	PACK_INFO*		pSigVal;
	int			iTemp, i, nError, j, pt, nBufLen = 0;
	SigINFO_LCD		*sTemp;
	int			nTimeOut = 0;		//Time out
	SIG_BASIC_VALUE*	pSigValue;
	SAMPLE_SIG_INFO*	pSampSigInfo;
	SET_SIG_INFO*		pSetSigInfo;

	pSigVal = (PACK_INFO*)p;
	j = 0;

	//´¦Àí½«EIB£¬SMDUµÄshuntÉèÖÃÎªsourceµÄÂß¼­
	if(iScreenID == SOURCE_INFO_SCREEN)
	{
		ProcessShuntSourcesInfo();
		return;
	}
	

	for(i = 0;i < pUserDefLCD.pDispInfoLCD->iDispInfoNum;i++)
	{
		sTemp = pUserDefLCD.pDispInfoLCD->pSigInfoLCD + i;
		iTemp = sTemp->iPageID;
		strncpy(pSigVal[j].cSigName, "\0", MAXLEN_NAME);

		if(iTemp == iScreenID)
		{
			pSigVal[j].iEquipID = sTemp->iEquipID;
			pSigVal[j].iSigID = sTemp->iSigID;

			if(sTemp->iEquipID == SPECIALID)
			{
				if(sTemp->iSigID == SYSUSED)
				{
					pSigVal[j].vSigValue.fValue = QueryRatedCurrentPercent();

					nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
						EQIPID_RECTGRP,			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SYSUSED),		
						&nBufLen,			
						&pSampSigInfo,			
						nTimeOut);
					pSigVal[j].iFormat = *(pSampSigInfo->szValueDisplayFmt + 1) - 0x30;
					strcpy(pSigVal[j].cSigName, pSampSigInfo->pSigName->pAbbrName[iLangType]);
					pSigVal[j].iSigType = pSampSigInfo->iSigValueType;
					//printf("Name %d= %s; Fmt=%d\n", j, pSigVal[j].cSigName,pSigVal[j].iFormat);
					strcpy(pSigVal[j].cSigUnit, pSampSigInfo->szSigUnit);

				}
				else if(sTemp->iSigID == SIGSYSOA)
				{
					pSigVal[j].vSigValue.lValue = g_stActAlmNum.OANum;
					pSigVal[j].vSigValue.ulValue = g_stActAlmNum.OANum;
					pSigVal[j].iSigType = VAR_LONG;				
				}
				else if(sTemp->iSigID == SIGSYSMA)
				{
					pSigVal[j].vSigValue.lValue = g_stActAlmNum.MANum;
					pSigVal[j].vSigValue.ulValue = g_stActAlmNum.MANum;
					pSigVal[j].iSigType = VAR_LONG;				
				}
				else if(sTemp->iSigID == SIGSYSCA)
				{
					pSigVal[j].vSigValue.lValue = g_stActAlmNum.CANum;
					pSigVal[j].vSigValue.ulValue = g_stActAlmNum.CANum;
					pSigVal[j].iSigType = VAR_LONG;				
				}
			}
			//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
			else if(SPECIALID2 == sTemp->iEquipID)
			{
				int iParaLen = 0;
				int iTmpEquipID, iTmpSigType, iTmpSigID, iTmpStyle;
				int iTmpSubFunPara, iTmpSubFunID;
				int iTmpCondEquipID, iTmpCondSigType, iTmpCondSigID;
				int iTmpDataEquipID, iTmpDataSigType, iTmpDataSigID;
				int iTmpDisplayMethod = 2;//0, auto; 1, show; 2, hide;
				
				QT_SPLIT_SIGTYPE_ID(sTemp->iSigType, iTmpEquipID, iTmpSigType, iTmpStyle);
				QT_SPLIT_SIG_ID(sTemp->iSigID, iTmpSubFunPara, iTmpSubFunID, iTmpSigID);

				memset(&pSigVal[j].stSpecialID2Info, 0, sizeof(pSigVal[j].stSpecialID2Info));
				pSigVal[j].stSpecialID2Info.iDispStyle = iTmpStyle;
				pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = iTmpEquipID;
				pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = iTmpSigType;
				pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = iTmpSigID;

				//check whether or not need be displayed
				if((1 == iTmpSubFunID)
					|| (5 == iTmpSubFunID))
				{//use StdEquip_System.cfg [SETTING_SIGNAL_INFO]
					iTmpCondEquipID = 1;
					iTmpCondSigType = 2;
					iTmpCondSigID = iTmpSubFunPara;
				}
				else if((2 == iTmpSubFunID)
					|| (6 == iTmpSubFunID))
				{//use the same field as the displayed signal
					iTmpCondEquipID = iTmpEquipID;
					iTmpCondSigType = iTmpSigType;
					iTmpCondSigID = iTmpSubFunPara;
				}
				else if(7 == iTmpSubFunID)
				{
					iTmpCondEquipID = -1;
					iTmpCondSigType = -1;
					iTmpCondSigID = -1;
					iTmpDisplayMethod = 0;//0, auto; 1, show; 2, hide;
				}
				else
				{
					iTmpCondEquipID = iTmpEquipID;
					iTmpCondSigType = iTmpSigType;
					iTmpCondSigID = iTmpSigID;
				}

				if((iTmpCondEquipID > -1)
					&& (iTmpCondSigType > -1)
					&& (iTmpCondSigID > -1))
				{
					nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iTmpCondEquipID,			
						DXI_MERGE_SIG_ID(iTmpCondSigType, iTmpCondSigID),
						&nBufLen,			
						(void *)&pSigValue,			
						0);
					if((ERR_DXI_OK == nError)
						&& SIG_VALUE_NEED_DISPLAY(pSigValue))
					{
						iTmpDisplayMethod = 1;//0, auto; 1, show; 2, hide;
					}
					//printf("SPECIALID2=-2, nError1=%d\n",nError);
				}

				//get data for LCD front point
				if((3 == iTmpSubFunID)
					|| (5 == iTmpSubFunID))
				{
					iTmpDataEquipID = 1;
					iTmpDataSigType = 2;
					iTmpDataSigID = iTmpSubFunPara;
				}
				else if((4 == iTmpSubFunID)
					|| (6 == iTmpSubFunID))
				{
					iTmpDataEquipID = iTmpEquipID;
					iTmpDataSigType = iTmpSigType;
					iTmpDataSigID = iTmpSubFunPara;
				}
				else
				{
					iTmpDataEquipID = -1;
					iTmpDataSigType = -1;
					iTmpDataSigID = -1;
				}

				if( (iTmpDataEquipID > 0)
					&& (iTmpDataSigType > -1)
					&& (iTmpDataSigID > 0) )
				{
					nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iTmpDataEquipID,			
						DXI_MERGE_SIG_ID(iTmpDataSigType, iTmpDataSigID),
						&nBufLen,			
						(void *)&pSigValue,			
						0);
					if(ERR_DXI_OK == nError)
					{
						if(VAR_LONG == pSigValue->ucType)
						{
							pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = pSigValue->varValue.lValue;
						}
						else if(VAR_FLOAT == pSigValue->ucType)
						{
							pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = pSigValue->varValue.fValue;
						}
						else if(VAR_UNSIGNED_LONG == pSigValue->ucType)
						{
							pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = pSigValue->varValue.ulValue;
						}
						else if(VAR_ENUM == pSigValue->ucType)
						{
							pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = pSigValue->varValue.enumValue;
						}
						else if(VAR_DATE_TIME == pSigValue->ucType)
						{
							pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = pSigValue->varValue.dtValue;
						}
						else
						{
							pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = pSigValue->varValue.lValue;
						}
					}
					else
					{
						pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = 0;
					}
					//printf("SPECIALID2=-2, nError2=%d\n",nError);
				}
				else
				{
					pSigVal[j].stSpecialID2Info.iCtrlArgs[iParaLen++] = iTmpSubFunPara;
				}


				//printf("SPECIALID2=-2,iTmpEquipID=%d,iTmpSigType=%d,iTmpSigID=%d,iTmpStyle=%d\n"
				//	"iTmpCondEquipID=%d,iTmpCondSigType=%d,iTmpCondSigID=%d\n"
				//	"iTmpDataEquipID=%d,iTmpDataSigType=%d,iTmpDataSigID=%d\n"
				//	"iTmpSubFunID=%d,iTmpSubFunPara=%d\n",
				//	iTmpEquipID, iTmpSigType, iTmpSigID, iTmpStyle,
				//	iTmpCondEquipID, iTmpCondSigType, iTmpCondSigID,
				//	iTmpDataEquipID, iTmpDataSigType, iTmpDataSigID,
				//	iTmpSubFunID, iTmpSubFunPara
				//	);

				//get signal data
				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					iTmpEquipID,			
					DXI_MERGE_SIG_ID(iTmpSigType, iTmpSigID),		
					&nBufLen,			
					(void *)&pSigValue,			
					0);
				//printf("SPECIALID2=-2, nError3=%d\n",nError);

				if(ERR_DXI_OK == nError)
				{
					pSigVal[j].vSigValue = pSigValue->varValue;

					if(SIG_TYPE_SAMPLING == iTmpSigType)
					{
						nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
							iTmpEquipID,			
							DXI_MERGE_SIG_ID(iTmpSigType, iTmpSigID),		
							&nBufLen,			
							&pSampSigInfo,			
							nTimeOut);
						pSigVal[j].iFormat = *(pSampSigInfo->szValueDisplayFmt + 1) - 0x30;
						strncpyz(pSigVal[j].cSigName, pSampSigInfo->pSigName->pAbbrName[iLangType], MAXLEN_NAME);
						pSigVal[j].iSigType = pSampSigInfo->iSigValueType;
						//printf("Name %d= %s; Fmt=%d\n", j, pSigVal[j].cSigName,pSigVal[j].iFormat);
						strncpyz(pSigVal[j].cSigUnit, pSampSigInfo->szSigUnit, MAXLEN_UNIT);
						//printf("Unit %d:%s\n",j, pSigVal[j].cSigUnit);

						if(pSigVal[j].iSigType == VAR_ENUM)
						{
							pt = pSigValue->varValue.lValue;
							strncpyz(pSigVal[j].cEnumText, pSampSigInfo->pStateText[pt]->pAbbrName[iLangType], MAXLEN_NAME);
							//printf("Alias %d= %s\n",j,pSigVal[j].cEnumText);
						}
					}
					else
					{
						nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
							iTmpEquipID,			
							DXI_MERGE_SIG_ID(iTmpSigType, iTmpSigID),		
							&nBufLen,			
							&pSetSigInfo,			
							nTimeOut);
						pSigVal[j].iFormat = *(pSetSigInfo->szValueDisplayFmt + 1) - 0x30;;
						strncpyz(pSigVal[j].cSigName, pSetSigInfo->pSigName->pAbbrName[iLangType], MAXLEN_NAME);
						pSigVal[j].iSigType = pSetSigInfo->iSigValueType;
						//printf("@Name %d= %s\n", j, pSigVal[j].cSigName);
						strncpyz(pSigVal[j].cSigUnit, pSetSigInfo->szSigUnit, MAXLEN_UNIT);
						//printf("Unit %d:%s\n",j, pSigVal[j].cSigUnit);

						if(pSigVal[j].iSigType == VAR_ENUM)
						{
							pt = pSigValue->varValue.lValue;
							strncpyz(pSigVal[j].cEnumText, pSetSigInfo->pStateText[pt]->pAbbrName[iLangType], MAXLEN_NAME);
							//printf("Alias %d= %s\n",j,pSigVal[j].cEnumText);
						}
					}
					//printf("pSigVal[%d].vSigValue iSigType=%d,enumValue=%d, ulValue=%u, lValue=%d, fValue=%f\n"
					//	"name=%s,unit=%s\n",
					//	j,
					//	pSigVal[j].iSigType,
					//	pSigVal[j].vSigValue.enumValue,
					//	pSigVal[j].vSigValue.ulValue,
					//	pSigVal[j].vSigValue.lValue,
					//	pSigVal[j].vSigValue.fValue,
					//	pSigVal[j].cSigName,
					//	pSigVal[j].cSigUnit);

					pSigVal[j].stSpecialID2Info.iDispCtrl = iTmpDisplayMethod;//0, auto; 1, show; 2, hide;
				}
				else
				{
					pSigVal[j].stSpecialID2Info.iDispCtrl = 2;//0, auto; 1, show; 2, hide;
				}

				//printf("------------iTmpDisplayMethod=%d,iDispCtrl=%d,iDispStyle=%d-------------\n",
				//	iTmpDisplayMethod,
				//	pSigVal[j].stSpecialID2Info.iDispCtrl,
				//	pSigVal[j].stSpecialID2Info.iDispStyle);
				//printf("---iParaLen=%d,para list[0]=%d, [1]=%d, [2]=%d, [3]=%d, [4]=%d, [5]=%d\n",
				//	iParaLen,
				//	pSigVal[j].stSpecialID2Info.iCtrlArgs[0],
				//	pSigVal[j].stSpecialID2Info.iCtrlArgs[1],
				//	pSigVal[j].stSpecialID2Info.iCtrlArgs[2],
				//	pSigVal[j].stSpecialID2Info.iCtrlArgs[3],
				//	pSigVal[j].stSpecialID2Info.iCtrlArgs[4],
				//	pSigVal[j].stSpecialID2Info.iCtrlArgs[5]);
			}
			else
			{
				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
							sTemp->iEquipID,			
							DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),
							&nBufLen,			
							(void *)&pSigValue,			
							0);

				if(nError == ERR_DXI_OK)
				{
					pSigVal[j].vSigValue = pSigValue->varValue;
					//printf("EqID=%d;SigID=%d;Value=%d\n",sTemp->iEquipID, sTemp->iSigID, pSigVal[j].vSigValue.lValue);

					if(sTemp->iSigType == SIG_TYPE_SAMPLING)
					{
						nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
							sTemp->iEquipID,			
							DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),		
							&nBufLen,			
							&pSampSigInfo,			
							nTimeOut);
						pSigVal[j].iFormat = *(pSampSigInfo->szValueDisplayFmt + 1) - 0x30;
						strcpy(pSigVal[j].cSigName, pSampSigInfo->pSigName->pAbbrName[iLangType]);
						pSigVal[j].iSigType = pSampSigInfo->iSigValueType;
						//printf("Name %d= %s; Fmt=%d\n", j, pSigVal[j].cSigName,pSigVal[j].iFormat);
						strcpy(pSigVal[j].cSigUnit, pSampSigInfo->szSigUnit);
						//printf("Unit %d:%s\n",j, pSigVal[j].cSigUnit);

						if(pSigVal[j].iSigType == VAR_ENUM)
						{
							pt = pSigValue->varValue.lValue;
							strncpyz(pSigVal[j].cEnumText, pSampSigInfo->pStateText[pt]->pAbbrName[iLangType], MAXLEN_NAME);
							//printf("Alias %d= %s\n",j,pSigVal[j].cEnumText);
						}
					}
					else
					{
						nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
							sTemp->iEquipID,			
							DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),		
							&nBufLen,			
							&pSetSigInfo,			
							nTimeOut);
						pSigVal[j].iFormat = *(pSetSigInfo->szValueDisplayFmt + 1) - 0x30;;
						strcpy(pSigVal[j].cSigName, pSetSigInfo->pSigName->pAbbrName[iLangType]);
						pSigVal[j].iSigType = pSetSigInfo->iSigValueType;
						//printf("@Name %d= %s\n", j, pSigVal[j].cSigName);
						strcpy(pSigVal[j].cSigUnit, pSetSigInfo->szSigUnit);
						//printf("Unit %d:%s\n",j, pSigVal[j].cSigUnit);

						if(pSigVal[j].iSigType == VAR_ENUM)
						{
							pt = pSigValue->varValue.lValue;
							strncpyz(pSigVal[j].cEnumText, pSetSigInfo->pStateText[pt]->pAbbrName[iLangType], MAXLEN_NAME);
							//printf("Alias %d= %s\n",j,pSigVal[j].cEnumText);
						}
					}
				}
				else if(sTemp->iEquipID == EQIPID_DGGRP)
				{
					pSigVal[j].vSigValue.enumValue = 1;
					pSigVal[j].iSigType = VAR_ENUM;
				}
				//changed by Frank Wu,20131216,2/2, for slave rectifiers display exception when the first running  after APP updating
				else if( (sTemp->iEquipID == EQIPID_SLAVERECTGRP1)
					|| (sTemp->iEquipID == EQIPID_SLAVERECTGRP2)
					|| (sTemp->iEquipID == EQIPID_SLAVERECTGRP3))
				{
					if(sTemp->iSigID == SIGID_SLAVERECT_EXIST_STATE)
					{
						pSigVal[j].vSigValue.lValue = 1;//Not Existent, not display
						pSigVal[j].iSigType = VAR_LONG;
					}
				}
			}
			j++;
		}
	}

	//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
	pSigVal[0].iDataNum = j;
	for(i = 0; i < pSigVal[0].iDataNum; i++)
	{
		pSigVal[i].iDataNum = pSigVal[0].iDataNum;
	}
	//printf("------iDataNum=%d\n", pSigVal[0].iDataNum);
}

/*void PackModNum(void)
{
	PACK_MODNUM*	pWrite;
	SIG_BASIC_VALUE*	pSigValue;
	int		nError, nTimeOut = 0;		//Time out
	int		nBufLen = 0;

	pWrite = (PACK_MODNUM*)p;

	nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_RECTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			nTimeOut);
	pWrite->iRectNum = pSigValue->varValue.ulValue;

	nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_MPPTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			nTimeOut);
	pWrite->iMPPTNum = pSigValue->varValue.ulValue;
}*/

void PackModule(int iScreenID)
{
	PACK_MODINFO*		pWrite;
	SIG_BASIC_VALUE*	pSigValue;
	int			i,iModNum, nBufLen, nError, j = 0;
	int			iEqID, iIndex,iNameIndex = 0;
	SigINFO_MOD*		sTemp;
	int			iTemp, pt, iMaxNum1, iMaxNum2;
	SAMPLE_SIG_INFO*	pSetSigInfo;
	EQUIP_INFO*		pEquipInfo;
	char	cName[MAXLEN_NAME];

	pWrite = (PACK_MODINFO*)p;

	iTemp = pUserDefLCD.pDispInfoMOD->iMODInfoNum;
	/*for(j = 0;j < iTemp;j++)
	{
		sTemp = pUserDefLCD.pDispInfoMOD->pSigInfoMOD + j;
		if(sTemp->iPageID == iScreenID)
		{
			nError = DxiGetData(VAR_A_SIGNAL_VALUE,
				(sTemp->iEquipID1 - 1),			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
				&nBufLen,			
				(void *)&pSigValue,			
				0);
			pWrite->iModuleNum = pSigValue->varValue.ulValue;
		}
	}*/
	if(iScreenID == RECT_SCREEN)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_RECTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
		
	}
	else if(iScreenID == MPPT_SCREEN)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_MPPTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
		//iModNum = pSigValue->varValue.ulValue;
		//iModNum2 = 0;
	}
	else if(iScreenID == CONVT_SCREEN)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_CONVGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	}
	else if(iScreenID == S1RECT_SCREEN)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_SLAVERECTGRP1,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SRECTNUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	}
	else if(iScreenID == S2RECT_SCREEN)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_SLAVERECTGRP2,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SRECTNUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	}
	else if(iScreenID == S3RECT_SCREEN)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_SLAVERECTGRP3,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SRECTNUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	}
	else
	{
		 pSigValue->varValue.ulValue = 0;
	}

	pWrite->iModuleNum = pSigValue->varValue.ulValue;
	//printf("Rectnum:%d\n",pWrite->iModuleNum)

	for(i = 0;i < pWrite->iModuleNum;i++)
	{
		iIndex = 0;
		for(j = 0;j < iTemp;j++)
		{
			sTemp = pUserDefLCD.pDispInfoMOD->pSigInfoMOD + j;
			if(sTemp->iPageID == iScreenID)
			{
				if(i < sTemp->iNum1)
				{
					iEqID = sTemp->iEquipID1;
					iModNum = i;
				}
				else
				{
					iEqID = sTemp->iEquipID2;
					iModNum = i - sTemp->iNum1;
				}
				nError = DxiGetData(VAR_A_EQUIP_INFO,
						(iEqID + iModNum),			
						0,		
						&nBufLen,			
						(void *)&pEquipInfo,			
						0);
				strncpyz(cName, pEquipInfo->pEquipName->pAbbrName[iLangType], MAXLEN_NAME);
				//changed by Frank Wu,1/1,20140319, for special equip name "solar convert #N"
				iNameIndex = 0;
				while(iNameIndex < MAXLEN_NAME)//find the first '#'
				{
					if('#' == cName[iNameIndex])
						break;
					iNameIndex++;
				}
				if( MAXLEN_NAME == iNameIndex )//find none
				{
					//move to end
					iNameIndex = 0;
					while( cName[iNameIndex] )
					{
						iNameIndex++;
					}
					
					//find the first (space + number) from end to start
					while(iNameIndex)
					{
						iNameIndex--;
						
						//find(space + number)
						if((iNameIndex > 0)
							&& (' ' == cName[iNameIndex - 1])
							&& ((cName[iNameIndex] <= '9') && (cName[iNameIndex] >= '0')))
						{
							break;
						}
					}
				}

				strncpyz(pWrite->ModInfo[i].cEqName, (cName + iNameIndex), MAXLEN_NAME);
				pWrite->ModInfo[i].iEquipID = iEqID + iModNum;
				//printf("nError1 = %d; ", nError);
				//printf("EqName:%s;Resp:%d; \n", pWrite->ModInfo[i].cEqName, pWrite->ModInfo[i].EachMod[0].vSigValue.enumValue);

				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						(iEqID + iModNum),			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, sTemp->iSigID),		
						&nBufLen,			
						&pSigValue,			
						0);
				//printf("nError3 = %d; Data=%f/%d\n", nError,pSigValue->varValue.fValue,pSigValue->varValue.ulValue);

				nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
						(iEqID + iModNum),			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, sTemp->iSigID),		
						&nBufLen,			
						&pSetSigInfo,			
						0);
				//printf("nError2 = %d; ", nError);
				pWrite->ModInfo[i].EachMod[iIndex].iSigID = sTemp->iSigID;
				pWrite->ModInfo[i].EachMod[iIndex].iSigType = pSetSigInfo->iSigValueType;
				pWrite->ModInfo[i].EachMod[iIndex].iFormat = *(pSetSigInfo->szValueDisplayFmt + 1) - 0x30;
				pWrite->ModInfo[i].EachMod[iIndex].vSigValue = pSigValue->varValue;

				if(pWrite->ModInfo[i].EachMod[iIndex].iSigType == VAR_ENUM)
				{
					pt = pSigValue->varValue.lValue;
					strncpyz(pWrite->ModInfo[i].EachMod[iIndex].cEnumText, pSetSigInfo->pStateText[pt]->pAbbrName[iLangType],MAXLEN_ENUM);
				}

				//printf("EqID=%d;SigID %d,%d:%f;Fmt=%d\n",iEqID + iModNum, iIndex,sTemp->iSigID, 
				//	pWrite->ModInfo[i].EachMod[iIndex].vSigValue.fValue,pWrite->ModInfo[i].EachMod[iIndex].iFormat);
				iIndex++;
			}
		}
		//printf("Eq %d Name:%s;Resp:%d;V=%f\n", (iEqID + iModNum), pWrite->ModInfo[i].cEqName, pWrite->ModInfo[i].EachMod[0].vSigValue.enumValue, pWrite->ModInfo[i].EachMod[1].vSigValue.fValue);
	}
}
//changed by Frank Wu,20131228,1/4,for filter signals which belong to other voltage level ---start---
//#define QT_FIFO_VOLTAGE_LEVEL_24V	24
//#define QT_FIFO_VOLTAGE_LEVEL_48V	48
//
//int QuerySysVoltageLevel(void)
//{
//#define QT_FIFO_VOLTAGE_LEVEL_EQUIP			1
//#define QT_FIFO_VOLTAGE_LEVEL_SIG_TYPE		2
//#define QT_FIFO_VOLTAGE_LEVEL_SIG_ID		224
//
//	int iErr = -1;
//	int iBufLen = 0;
//	SIG_BASIC_VALUE *pSigVal = NULL;
//	int iSysVoltageLevel = QT_FIFO_VOLTAGE_LEVEL_48V;
//
//	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
//						QT_FIFO_VOLTAGE_LEVEL_EQUIP,
//						DXI_MERGE_SIG_ID(QT_FIFO_VOLTAGE_LEVEL_SIG_TYPE, QT_FIFO_VOLTAGE_LEVEL_SIG_ID),
//						&iBufLen,
//						(void *)&pSigVal,
//						0);
//	if(iErr == ERR_DXI_OK)
//	{
//		if(pSigVal->varValue.enumValue == 0)
//		{
//			iSysVoltageLevel = QT_FIFO_VOLTAGE_LEVEL_48V;
//		}
//		else if(pSigVal->varValue.enumValue == 1)
//		{
//			iSysVoltageLevel = QT_FIFO_VOLTAGE_LEVEL_24V;
//		}
//	}
//
//	return iSysVoltageLevel;
//};
//
//int QuerySigVoltageLevel(IN const char *pEngFullName, IN int iMaxLen)
//{
//	int i;
//	const char sKey[] = "(24V)";
//
//	if(pEngFullName != NULL)
//	{
//		//printf("QuerySigVoltageLevel pEngFullName=%s, iMaxLen=%d\n", pEngFullName, iMaxLen);
//		for(i = 0; i < iMaxLen; i++)
//		{
//			if(pEngFullName[i] == sKey[0])
//			{
//				if( strncmp(&pEngFullName[i], sKey, strlen(sKey)) == 0 )
//				{
//					//printf("QuerySigVoltageLevel pEngFullName=%s\n", pEngFullName);
//					return QT_FIFO_VOLTAGE_LEVEL_24V;
//				}
//			}
//			else if(pEngFullName[i] == 0)
//			{
//				break;
//			}
//		}
//	}
//
//	return QT_FIFO_VOLTAGE_LEVEL_48V;
//}
//changed by Frank Wu,20131228,1/4,for filter signals which belong to other voltage level ---end---

//changed by Frank Wu,3/6,20140221 for TR194 which No battery size settings in Installation Wizard
static void ProcSettingItemName(IN int iScreenID, OUT SET_INFO* pSettingInfo)
{
    //printf("\nProcSettingItemName start");//hhy
	int nError;
	int nBufLen;
	EQUIP_INFO* pEquipInfo = NULL;
	if(WIZARD_BATT_CAPACITY_SCREEN == iScreenID)
	{
        printf("\nWIZARD_BATT_CAPACITY_SCREEN == iScreenID\n");
		nError = DxiGetData(VAR_A_EQUIP_INFO,
			pSettingInfo->iEquipID,			
			0,		
			&nBufLen,			
			(void *)&pEquipInfo,			
			0);

		if(ERR_DXI_OK == nError)
		{
			strncpyz(pSettingInfo->cSigName, 
                pEquipInfo->pEquipName->pAbbrName[iLangType], MAXLEN_NAME);
		}
	}
}

//changed by Frank Wu,9/9,20140303 for TR194 which No battery size settings in Installation Wizard
static BOOL IsValidSettingItem(IN int iScreenID, IN int iItemCount)
{
#define QT_FIFO_SETTINGS_BATT_CAP_MAX		15

	switch(iScreenID)
	{
		case WIZARD_BATT_CAPACITY_SCREEN:
		{
			if(iItemCount > QT_FIFO_SETTINGS_BATT_CAP_MAX)
			{
				return FALSE;
			}
			break;
		}
		default:
		{
			break;
		}

	}

	return TRUE;
}

BOOL IsEquipExist(int nEquipId)
{
	int nError=0;
	int nBuflen;
	EQUIP_INFO *pEquipInfo = NULL;

	nError = DxiGetData(VAR_A_EQUIP_INFO,nEquipId,0,&nBuflen,&pEquipInfo,0);
	if(nError == ERR_OK)
	{
		if(pEquipInfo->bWorkStatus == TRUE)
		{
			return TRUE;
		}
	}
	return FALSE;
}

void PackSetting(int iScreenID)
{
    //printf("PackSetting start");//hhy
	PACK_SETINFO* pWrite;
	SIG_BASIC_VALUE*	pSigValue;
	int			i, nBufLen, nError, j = 0;
	SigINFO_DISTRIB*		sTemp;
    int			iTemp, nTimeOut, iIndex = 0;
	SET_SIG_INFO*	pSetSigInfo;
	CTRL_SIG_INFO*	pCtrlSigInfo;
	char szTempUnit[MAXLEN_UNIT]={0};
	float fTempVal = 0;
	static int iFloatFlag = 0;
	//´æ´¢ÕæÊµµÄ´æÔÚ×´Ì¬
	int aSensorStat[MAXNUM_SEL]={0};
	//changed by Frank Wu,20131228,4/4,for filter signals which belong to other voltage level
	//int iSysVoltageLevel = QuerySysVoltageLevel();
	//int iSigVoltageLevel = iSysVoltageLevel;
	//changed by Frank Wu,20131228,6/6,for support settings of part EEM protocol in LCD 
	int iCurrentProtocol = GetProtocolType();
	//changed by Frank Wu,7/9,20140303 for TR194 which No battery size settings in Installation Wizard
	int iItemCount = 0;
	EQUIP_INFO	*pEquipInfo = NULL;
	int nRealIdx=0;


	pWrite = (PACK_SETINFO*)p;
	iTemp = pUserDefLCD.pDispInfoSET->iSETInfoNum;

	for(i = 0;i < iTemp;i++)
	{
		sTemp = pUserDefLCD.pDispInfoSET->pSigInfoSET + i;
        //printf("EQID = %d; Sig = %d\n", sTemp->iEquipID, sTemp->iPageID);
		if(sTemp->iPageID == iScreenID)
		{
			//changed by Frank Wu,8/9,20140303 for TR194 which No battery size settings in Installation Wizard
			iItemCount++;
			if( !IsValidSettingItem(iScreenID, iItemCount))
			{
				continue;
			}

			//printf("EQID1 = %d;Sig1=%d\n", sTemp->iEquipID, sTemp->iPageID );
			if(sTemp->iEquipID == SPECIALID)
			{
                //printf("=========SPECIALID\n");//hhy
				pWrite->SettingInfo[iIndex].iEquipID = sTemp->iEquipID;
				pWrite->SettingInfo[iIndex].iSigID = sTemp->iSigID;
				pWrite->SettingInfo[iIndex].cSigName[0] = 0;
                printf("sTemp->iSigID<%d>",sTemp->iSigID);
				switch (sTemp->iSigID)
				{
					case SITENAME_SETTING:
					{
						LANG_TEXT*	pLangInfo;
						//printf("iLangType=%d\n",iLangType);

						DxiGetData(VAR_ACU_PUBLIC_CONFIG,
							SITE_NAME,
							1,
							&nBufLen,
							&pLangInfo,
							0);
						//pWrite->SettingInfo[iIndex].iEquipID = sTemp->iEquipID;
						//pWrite->SettingInfo[iIndex].iSigID = sTemp->iSigID;
						pWrite->SettingInfo[iIndex].iSigType = sTemp->iSigType;
						pWrite->SettingInfo[iIndex].iSigValueType = VAR_ENUM;
						strncpyz(pWrite->SettingInfo[iIndex].cEnumText[0], pLangInfo->pFullName[0],MAXLEN_NAME);
						pWrite->SettingInfo[iIndex].iStep.lValue = 1;
						//printf("SiteName=%s\n", pWrite->SettingInfo[iIndex].cEnumText[0]);
						iIndex++;
						
						break;
					}
					//changed by Frank Wu,20140108
					case ONLY_DATE_SETTING:
					case ONLY_TIME_SETTING:
					case TIME_SETTING:
					{
						time_t	iTimeData;
						nBufLen = sizeof(time_t);
						DxiGetData(VAR_TIME_SERVER_INFO,
							SYSTEM_TIME,			
							0,		
							&nBufLen,			
							&iTimeData,			
							0);
						//printf("TimeData = %d;%d;\n",(int)(iTimeData>>32), (int)iTimeData);
						//pWrite->SettingInfo[iIndex].iEquipID = sTemp->iEquipID;
						//pWrite->SettingInfo[iIndex].iSigID = sTemp->iSigID;
						pWrite->SettingInfo[iIndex].iSigType = sTemp->iSigType;
						pWrite->SettingInfo[iIndex].iSigValueType = VAR_LONG;
						pWrite->SettingInfo[iIndex].vSigValue.lValue = iTimeData;
						pWrite->SettingInfo[iIndex].iStep.lValue = 1;
						iIndex++;
						break;
					}
					case IP_SETTING:
					case MASK_SETTING:
					case GATEWAY_SETTING:
					{
						ULONG uData;

						nError = DxiGetData(VAR_ACU_NET_INFO,
							(NET_INFO_IP + sTemp->iSigID - IP_SETTING),			
							0,		
							&nBufLen,			
							&uData,			
							0);
						//pWrite->SettingInfo[iIndex].iEquipID = sTemp->iEquipID;
						//pWrite->SettingInfo[iIndex].iSigID = sTemp->iSigID;
						pWrite->SettingInfo[iIndex].iSigType = sTemp->iSigType;
						pWrite->SettingInfo[iIndex].iSigValueType = VAR_UNSIGNED_LONG;
						pWrite->SettingInfo[iIndex].vSigValue.ulValue = uData;
						pWrite->SettingInfo[iIndex].iStep.lValue = 1;
						pWrite->SettingInfo[iIndex].vUpLimit.ulValue = 255;
						pWrite->SettingInfo[iIndex].vDnLimit.ulValue = 0;
						iIndex++;
						break;
					}
					case DHCPCLIENT_SETTING:
					{
						int nDHCP;

						nError += DxiGetData(VAR_APP_DHCP_INFO,
											0,			
											0,		
											&nBufLen,			
											&nDHCP,			
											0);
						pWrite->SettingInfo[iIndex].iSigValueType = VAR_ENUM;
						pWrite->SettingInfo[iIndex].vSigValue.enumValue = nDHCP;
						pWrite->SettingInfo[iIndex].iStep.ulValue = 1;
						pWrite->SettingInfo[iIndex].vUpLimit.enumValue = 2;
						pWrite->SettingInfo[iIndex].vDnLimit.enumValue = 0;
						iIndex++;
						break;
					}
					//changed by Frank Wu,20131228,3/6,for support settings of part EEM protocol in LCD ---start---
					case COMM_PROTOCOL_SETTING:
					{
						PackSpecialItemCommProtocol(&pWrite->SettingInfo[iIndex], &iIndex);
						break;
					}
					case YDN23_ADDR_SETTING:
					{
						if(iCurrentProtocol == PROTOCOL_YDN23)
						{
							PackSpecialItemCommAddr(&pWrite->SettingInfo[iIndex], &iIndex);
						}
						break;
					}
					case YDN23_METHOD_SETTING:
					{
						if(iCurrentProtocol == PROTOCOL_YDN23)
						{
							PackSpecialItemCommMediaType(&pWrite->SettingInfo[iIndex], &iIndex);
						}
						break;
					}
					case YDN23_BAUDRATE_SETTING:
					{
						if(iCurrentProtocol == PROTOCOL_YDN23)
						{
							PackSpecialItemCommBaudrate(&pWrite->SettingInfo[iIndex], &iIndex);
						}
						break;
					}
					case MODBUS_ADDR_SETTING:
					{
						if(iCurrentProtocol == PROTOCOL_MODBUS)
						{
							PackSpecialItemCommAddr(&pWrite->SettingInfo[iIndex], &iIndex);
						}
						break;
					}
					case MODBUS_METHOD_SETTING:
					{
						if(iCurrentProtocol == PROTOCOL_MODBUS)
						{
							PackSpecialItemCommMediaType(&pWrite->SettingInfo[iIndex], &iIndex);
						}
						break;
					}
					case MODBUS_BAUDRATE_SETTING:
					{
						if(iCurrentProtocol == PROTOCOL_MODBUS)
						{
							PackSpecialItemCommBaudrate(&pWrite->SettingInfo[iIndex], &iIndex);
						}
						break;
					}
					//changed by Frank Wu,20131228,3/6,for support settings of part EEM protocol in LCD ---end---
					//changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
					case IPV6_IP_SETTING_1:
					case IPV6_IP_SETTING_2:
					case IPV6_IP_SETTING_ROT90_1:
					case IPV6_IP_SETTING_ROT90_2:
					case IPV6_IP_SETTING_ROT90_3:
					{
						pWrite->SettingInfo[iIndex].iSigType = sTemp->iSigType;
						pWrite->SettingInfo[iIndex].iSigValueType = VAR_UNSIGNED_LONG;

						ACU_V6_NET_INFO stIpV6Info;

						memset((void *)&stIpV6Info, 0, sizeof(stIpV6Info));

						nError = DxiGetData(VAR_NET_IPV6_INFO,
							0,			
							0,		
							&nBufLen,			
							(void *)&stIpV6Info,			
							0);

						if((ERR_DXI_OK == nError)
							&& (sizeof(pWrite->SettingInfo[iIndex].cSigName) >= 16))
						{
							memset((void *)pWrite->SettingInfo[iIndex].cSigName,
								0,
								sizeof(pWrite->SettingInfo[iIndex].cSigName));

							memcpy((void *)pWrite->SettingInfo[iIndex].cSigName,
								(void *)(stIpV6Info.stGlobalAddr.ifr6_addr.in6_u.u6_addr8),
								16);

							iIndex++;
						}

						break;
					}
					case IPV6_GATEWAY_SETTING_1:
					case IPV6_GATEWAY_SETTING_2:
					case IPV6_GATEWAY_SETTING_ROT90_1:
					case IPV6_GATEWAY_SETTING_ROT90_2:
					case IPV6_GATEWAY_SETTING_ROT90_3:
					{
						pWrite->SettingInfo[iIndex].iSigType = sTemp->iSigType;
						pWrite->SettingInfo[iIndex].iSigValueType = VAR_UNSIGNED_LONG;

						ACU_V6_NET_INFO stIpV6Info;

						memset((void *)&stIpV6Info, 0, sizeof(stIpV6Info));

						nError = DxiGetData(VAR_NET_IPV6_INFO,
							0,			
							0,		
							&nBufLen,			
							(void *)&stIpV6Info,			
							0);

						if((ERR_DXI_OK == nError)
							&& (sizeof(pWrite->SettingInfo[iIndex].cSigName) >= 16))
						{
							memset((void *)pWrite->SettingInfo[iIndex].cSigName,
								0,
								sizeof(pWrite->SettingInfo[iIndex].cSigName));

							memcpy((void *)pWrite->SettingInfo[iIndex].cSigName,
								(void *)(stIpV6Info.stGateWay.in6_u.u6_addr8),
								16);

							iIndex++;
						}

						break;
					}
					case IPV6_MASK_SETTING:
					{
						ULONG uData;

						pWrite->SettingInfo[iIndex].iSigType = sTemp->iSigType;
						pWrite->SettingInfo[iIndex].iSigValueType = VAR_UNSIGNED_LONG;
						pWrite->SettingInfo[iIndex].iStep.ulValue = 1;
						pWrite->SettingInfo[iIndex].vDnLimit.ulValue = 0;
						pWrite->SettingInfo[iIndex].vUpLimit.ulValue = 128;

						ACU_V6_NET_INFO stIpV6Info;

						memset((void *)&stIpV6Info, 0, sizeof(stIpV6Info));

						nError = DxiGetData(VAR_NET_IPV6_INFO,
							0,			
							0,		
							&nBufLen,			
							(void *)&stIpV6Info,			
							0);

						if(ERR_DXI_OK == nError)
						{
							pWrite->SettingInfo[iIndex].vSigValue.ulValue = stIpV6Info.stGlobalAddr.ifr6_prefixlen;
							iIndex++;
						}

						break;
					}
					case IPV6_DHCPCLIENT_SETTING:
					{
						int nDHCP;

						nError += DxiGetData(VAR_APP_DHCP_INFO,
											DXI_VAR_ID_IPV6,			
											0,		
											&nBufLen,			
											&nDHCP,			
											0);

						pWrite->SettingInfo[iIndex].iSigValueType = VAR_ENUM;
						pWrite->SettingInfo[iIndex].vSigValue.enumValue = nDHCP;
						pWrite->SettingInfo[iIndex].iStep.ulValue = 1;
						pWrite->SettingInfo[iIndex].vUpLimit.enumValue = 2;
						pWrite->SettingInfo[iIndex].vDnLimit.enumValue = 0;
						iIndex++;
						break;
					}
					default:
						;
				}
			}
			else
            {
                //printf("normal ID\n");//hhy
				//changed by Frank Wu,N/N/N,20150429, for scanning LVD3
				nError = DxiGetData(VAR_A_EQUIP_INFO,
					sTemp->iEquipID,
					0,
					&nBufLen,
					&pEquipInfo,
					0);
				if((nError != ERR_DXI_OK) 
					|| (pEquipInfo->bWorkStatus != TRUE))
				{
					continue;
				}

				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
							sTemp->iEquipID,			
							DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),		
							&nBufLen,			
							(void *)&pSigValue,			
							0);
                //printf("==VAR_A_SIGNAL_VALUE<%d>,sTemp->iEquipID<%d>,sTemp->iSigType<%d>,sTemp->iSigID<%d>",
                       //VAR_A_SIGNAL_VALUE,sTemp->iEquipID,sTemp->iSigType,sTemp->iSigID);//hhy
				if(SIG_VALUE_IS_CONFIGURED(pSigValue) && SIG_VALUE_NEED_DISPLAY(pSigValue))
				{
					//printf("No.%d need display!\n");
					pWrite->SettingInfo[iIndex].iEquipID = sTemp->iEquipID;
					pWrite->SettingInfo[iIndex].iSigID =	sTemp->iSigID;
					pWrite->SettingInfo[iIndex].iSigType = sTemp->iSigType;

					if(sTemp->iSigType == SIG_TYPE_SETTING)
					{
                        printf("c:SIG_TYPE_SETTING\n");
						if((sTemp->iEquipID == EQIPID_BATTGRP) 
							&& ((sTemp->iSigID == SIGID_FLOAT48) || (sTemp->iSigID == SIGID_BOOST48)))
							iFloatFlag = TRUE;
						else if((sTemp->iEquipID == EQIPID_BATTGRP) 
							&& ((sTemp->iSigID == SIGID_FLOAT24) || (sTemp->iSigID == SIGID_BOOST24)))
						{
							if(iFloatFlag == TRUE)
							continue;
						}
				
						nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
								sTemp->iEquipID,			
								DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),		
								&nBufLen,			
								&pSetSigInfo,			
								0);
                        //printf("sTemp->iEquipID<%d>,sTemp->iSigID<%d>\n",sTemp->iEquipID,sTemp->iSigID);//hhy
						pWrite->SettingInfo[iIndex].iSigValueType = pSetSigInfo->iSigValueType;
						pWrite->SettingInfo[iIndex].vSigValue = pSigValue->varValue;
						strncpyz(pWrite->SettingInfo[iIndex].cSigName, 
							pSetSigInfo->pSigName->pAbbrName[iLangType], MAXLEN_NAME);
                        //printf("hhypSetSigInfo->iSigValueType<%d>,pSigValue->varValue<%d>",
                               //pSetSigInfo->iSigValueType,pSigValue->varValue);//hhy 79
						//Èç¹ûÊÇÎÂ¶ÈÐÅºÅÔòÒª¸ù¾ÝÎÂ¶ÈµÄÏÔÊ¾¸ñÊ½»ñÈ¡µ¥Î»
						if(DXI_isTempSignal(pSigValue))
						{
							DXI_GetTempSwitch(1,pSigValue->varValue.fValue,&fTempVal,szTempUnit);
							strncpyz(pWrite->SettingInfo[iIndex].cSigUnit,szTempUnit,MAXLEN_UNIT);
							pWrite->SettingInfo[iIndex].vSigValue.fValue = fTempVal;
						}
						else if(IS_TEMP_COE_SIGNAL(sTemp->iEquipID,sTemp->iSigType, sTemp->iSigID))
						{
							DXI_GetTempSwitch(0,pSigValue->varValue.fValue,&fTempVal,szTempUnit);
							strncpyz(pWrite->SettingInfo[iIndex].cSigUnit,szTempUnit,MAXLEN_UNIT);	
							pWrite->SettingInfo[iIndex].vSigValue.fValue = fTempVal;
						}
						else
						{
							strncpyz(pWrite->SettingInfo[iIndex].cSigUnit, pSetSigInfo->szSigUnit, MAXLEN_UNIT);
						}
						//changed by Frank Wu,4/6,20140221 for TR194 which No battery size settings in Installation Wizard
						ProcSettingItemName(iScreenID, &pWrite->SettingInfo[iIndex]);
                        //ÅÐ¶ÏÊÇÊ²Ã´ÀàÐÍµÄ²ÎÊýfloat/emum
						if(pWrite->SettingInfo[iIndex].iSigValueType == VAR_FLOAT)
						{
                            printf("VAR_FLOAT start\n");//hhy
							//Èç¹ûÊÇÎÂ¶ÈÏà¹ØµÄÐÅºÅ£¬ÉÏ¡¢ÏÂÏÞÖµÒ²Ó¦¸Ã¸ù¾ÝÎÂ¶ÈµÄÏÔÊ¾
							//¸ñÊ½»»Ëã
							//ÉÏÏÞÖµ
							if(DXI_isTempSignal(pSigValue))
							{
								DXI_GetTempSwitch(1,pSetSigInfo->fMaxValidValue,&fTempVal,szTempUnit);
							}
							else if(IS_TEMP_COE_SIGNAL(sTemp->iEquipID,sTemp->iSigType,sTemp->iSigID))
							{
								DXI_GetTempSwitch(0,pSetSigInfo->fMaxValidValue,&fTempVal,szTempUnit);
							}
							else
							{
								fTempVal = pSetSigInfo->fMaxValidValue;
							}
							pWrite->SettingInfo[iIndex].vUpLimit.fValue = fTempVal;

							//ÏÂÏÞÖµ
							if(DXI_isTempSignal(pSigValue))
							{
								DXI_GetTempSwitch(1,pSetSigInfo->fMinValidValue,&fTempVal,szTempUnit);
							}
							else if(IS_TEMP_COE_SIGNAL(sTemp->iEquipID,sTemp->iSigType,sTemp->iSigID))
							{
								DXI_GetTempSwitch(0,pSetSigInfo->fMinValidValue,&fTempVal,szTempUnit);
							}
							else
							{
								fTempVal = pSetSigInfo->fMinValidValue;
							}							
							pWrite->SettingInfo[iIndex].vDnLimit.fValue = fTempVal;
						
							pWrite->SettingInfo[iIndex].iStep.fValue = pSetSigInfo->fSettingStep;
                            printf("Value = %f/%d; limup=%f;limdn=%f;step=%f\n",pSigValue->varValue.fValue, pSigValue->varValue.ulValue, pSetSigInfo->fMaxValidValue, pSetSigInfo->fMinValidValue,pSetSigInfo->fMinValidValue);
						}
						else if(pWrite->SettingInfo[iIndex].iSigValueType == VAR_UNSIGNED_LONG)
						{
							pWrite->SettingInfo[iIndex].vUpLimit.ulValue = (unsigned long)pSetSigInfo->fMaxValidValue;
							pWrite->SettingInfo[iIndex].vDnLimit.ulValue = (unsigned long)pSetSigInfo->fMinValidValue;
							pWrite->SettingInfo[iIndex].iStep.ulValue = 1;
                            printf("Value = %f/%d; limup=%f;limdn=%f;step=%f\n",pSigValue->varValue.fValue, pSigValue->varValue.ulValue, pSetSigInfo->fMaxValidValue, pSetSigInfo->fMinValidValue,pSetSigInfo->fMinValidValue);
						}
						else if(pWrite->SettingInfo[iIndex].iSigValueType == VAR_ENUM)
						{
                            //printf("VAR_ENUM\n");

							//if(pSetSigInfo->iStateNum < MAXNUM_SEL)
							pWrite->SettingInfo[iIndex].vUpLimit.enumValue = pSetSigInfo->iStateNum - 1;
							//else
								//pWrite->SettingInfo[iIndex].vUpLimit.enumValue = MAXNUM_SEL - 1;

							pWrite->SettingInfo[iIndex].vDnLimit.enumValue = 0;
							pWrite->SettingInfo[iIndex].iStep.ulValue = 1;
	
							//Èç¹ûÊÇÎÂ¶ÈÌ½ÕëÑ¡ÏîÔòÖ»ÏÔÊ¾´æÔÚµÄÌ½Õë
							if((sTemp->iEquipID == 115) && 
								(sTemp->iSigType == SIG_TYPE_SETTING) && 
								(sTemp->iSigID == 63))
                            {
                                nRealIdx = 0;
								for(j = 0;j <= pWrite->SettingInfo[iIndex].vUpLimit.enumValue;j++)
								{
									//1-7¿ÉÒÔÒ»Ö±±»Ñ¡
									//8,9È¡¾öÓÚIB2-1
									//10,11È¡¾öÓÚEIB1
									//12-75È¡¾öÓÚSMTemp1-8
									//76-77È¡¾öÓÚIB2-2
									//78-79È¡¾öÓÚEIB2
									//80Ò»Ö±ÏÔÊ¾
									if((j==8) || (j==7))
									{
										if(!IsEquipExist(EQUIP_ID_IB2_1))
										{
											continue;
										}
									}
									else if((j == 9) || (j==10))
									{
										if(!IsEquipExist(EQUIP_ID_EIB1))
										{
											continue;
										}
									}
									else if((j == 75) || (j==76))
									{
										if(!IsEquipExist(EQUIP_ID_IB2_2))
										{
											continue;
										}
									}
									else if((j == 77) || (j==78))
									{
										if(!IsEquipExist(EQUIP_ID_EIB2))
										{
											continue;
										}
									}
									else if((11<=j) && (j<=74))
									{
										if(!IsEquipExist(EQUIP_ID_SMTEMP_1 + ((j-12)/8)))
										{
											continue;
										}
									}
									else if(j>=79 && j<=88)
                                    {
											if(!IsEquipExist(EQUIP_ID_SMDUE1))
										{
											continue;
										}
									}
                                    else if(j>=89 && j<=98)
                                    {
                                            if(!IsEquipExist(EQUIP_ID_SMDUE2))
                                        {
                                            continue;
                                        }
                                    }

									aSensorStat[nRealIdx] = j;
                                    //printf("output j<%d>",j);//hhy
									strncpyz(pWrite->SettingInfo[iIndex].cEnumText[nRealIdx], \
										pSetSigInfo->pStateText[j]->pAbbrName[iLangType], MAXLEN_NAME);
                                    //printf("name<%s>",pSetSigInfo->pStateText[j]->pAbbrName[iLangType]);//hhy
									nRealIdx++;	
								}
								
								//¸ù¾ÝÕæÊµ´æÔÚµÄ×´Ì¬À´È·¶¨ÆäÎ»ÖÃ¡£
                                for(j=0;j<MAXNUM_SEL;j++)//MAXNUM_SEL
                                {
									if(aSensorStat[j] == pWrite->SettingInfo[iIndex].vSigValue.enumValue)
                                    {//ÏÔÊ¾µ±Ç°ËùÓÐÑ¡Ïî
                                        pWrite->SettingInfo[iIndex].vSigValue.enumValue = j;
                                        //printf("write j<%d>",j);//hhy
									}
								}
								
								pWrite->SettingInfo[iIndex].vUpLimit.enumValue = nRealIdx-1;
                                //printf("nRealIdx-1<%d>",nRealIdx-1);//hhy 7
							}
							else
							{
								for(j = 0;j <= pWrite->SettingInfo[iIndex].vUpLimit.enumValue;j++)
								{
									strncpyz(pWrite->SettingInfo[iIndex].cEnumText[j], \
										pSetSigInfo->pStateText[j]->pAbbrName[iLangType], MAXLEN_NAME);									
								}								
							}						
						}
                        iIndex++;
					}
					else if(sTemp->iSigType == SIG_TYPE_CONTROL)
					{
                        printf("SIG_TYPE_CONTROL\n");
						nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
								sTemp->iEquipID,			
								DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, sTemp->iSigID),		
								&nBufLen,			
								&pCtrlSigInfo,			
								0);


						pWrite->SettingInfo[iIndex].iSigValueType = pCtrlSigInfo->iSigValueType;
						pWrite->SettingInfo[iIndex].vSigValue = pSigValue->varValue;
						strncpyz(pWrite->SettingInfo[iIndex].cSigName, pCtrlSigInfo->pSigName->pAbbrName[iLangType], MAXLEN_NAME);
						strncpyz(pWrite->SettingInfo[iIndex].cSigUnit, pCtrlSigInfo->szSigUnit, MAXLEN_UNIT);
						//changed by Frank Wu,5/6,20140221 for TR194 which No battery size settings in Installation Wizard
						ProcSettingItemName(iScreenID, &pWrite->SettingInfo[iIndex]);
						//printf("SigValueType=%d; ",pWrite->SettingInfo[iIndex].iSigValueType);

						if(pWrite->SettingInfo[iIndex].iSigValueType == VAR_FLOAT)
						{
							pWrite->SettingInfo[iIndex].vUpLimit.fValue = pCtrlSigInfo->fMaxValidValue;
							pWrite->SettingInfo[iIndex].vDnLimit.fValue = pCtrlSigInfo->fMinValidValue;
							pWrite->SettingInfo[iIndex].iStep.fValue = pCtrlSigInfo->fControlStep;
						}
						else if(pWrite->SettingInfo[iIndex].iSigValueType == VAR_ENUM)
						{
							pWrite->SettingInfo[iIndex].vUpLimit.enumValue = pCtrlSigInfo->iStateNum - 1;
							pWrite->SettingInfo[iIndex].vDnLimit.enumValue = 0;
							pWrite->SettingInfo[iIndex].iStep.lValue = 1;
							for(j = 0;j < pCtrlSigInfo->iStateNum;j++)
								strncpyz(pWrite->SettingInfo[iIndex].cEnumText[j], pCtrlSigInfo->pStateText[j]->pAbbrName[iLangType], MAXLEN_NAME);
							
						}
						else
							;
						iIndex++;
					}
					//Frank Wu,20151023
					else if(sTemp->iSigType == SIG_TYPE_SAMPLING)
					{
                        printf("SIG_TYPE_SAMPLING");
						pWrite->SettingInfo[iIndex].iSigValueType = VAR_ENUM;
						pWrite->SettingInfo[iIndex].vSigValue.enumValue = pSigValue->varValue.ulValue;
                        //printf("\nthe VAR_ENUM<%d>enumValue<%d>",VAR_ENUM,pSigValue->varValue.ulValue);//hhy
						iIndex++;
					}
				}
				else
                {
					if((sTemp->iEquipID == EQIPID_BATTGRP)
						&& ((sTemp->iSigID == SIGID_FLOAT48) || (sTemp->iSigID == SIGID_BOOST48)))
							iFloatFlag = FALSE;
				}
			}
			
			/*printf("EqID=%d; SigID=%d; Step=%f\n",pWrite->SettingInfo[iIndex].iEquipID,
				pWrite->SettingInfo[iIndex].iSigID,
				pWrite->SettingInfo[iIndex].iStep.fValue);*/
		}
	}
    pWrite->SetNum = iIndex;
	//printf("Send Cmd=%d\n",time(NULL));
}

void PackNormalValue(int iScreenID)
{
	PACK_DATAINFO * pWrite;
	SIG_BASIC_VALUE*	pSigValue;
	int			i,iDistrNum = 0, nBufLen, nError;
	SigINFO_DISTRIB*	sTemp;
	int			iTemp, nTimeOut = 3000;//changed by Frank Wu,20140102,1/1, for Keyboard voice in LCD

	pWrite = (PACK_DATAINFO*)p;
	iTemp = pUserDefLCD.pDispInfoDistr->iDistrNum;
	for(i = 0;i < iTemp;i++)
	{
		sTemp = pUserDefLCD.pDispInfoDistr->pSigInfoDistr + i;
		
		if(sTemp->iPageID == iScreenID)
		{
			
			nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					sTemp->iEquipID,			
					DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),		
					&nBufLen,			
					(void *)&pSigValue,			
					nTimeOut);
			if(SIG_VALUE_IS_CONFIGURED(pSigValue))
			{
				pWrite->DataInfo[iDistrNum].iEquipID = sTemp->iEquipID;
				pWrite->DataInfo[iDistrNum].iSigID = sTemp->iSigID;
				pWrite->DataInfo[iDistrNum].vValue = pSigValue->varValue;
				pWrite->DataInfo[iDistrNum].iSigType = sTemp->iSigType;
				iDistrNum++;
			}
		}
	}
	pWrite->DataNum = iDistrNum;
}


//changed by Frank Wu,20131224,1/3, for TR 113 Numbering of load currents ---start---
int computeBLoadSigNum(IN char *pEngName)
{
	int iSigNum = -1;
	int iRet = -1;
	int i = 0;
	int iLastDigitPos = -1;
	char sBuf[MAXLEN_NAME + 1];//the first and  last char is '\0'
	//scan the last digit in english name
	if(pEngName != NULL)
	{
		sBuf[0] = 0;
		strncpy(&sBuf[1], pEngName, MAXLEN_NAME);
		iLastDigitPos = -1;

		for(i = 1; i <= MAXLEN_NAME; i++)
		{
			if(0 == sBuf[i])//end
			{
				break;
			}
			else if( (sBuf[i] >= '0') && (sBuf[i] <= '9') )//sBuf[i] is digit
			{
				if( ! ((sBuf[i - 1] >= '0') && (sBuf[i - 1] <= '9')) )//sBuf[i - 1] isn't digit
				{
					iLastDigitPos = i;//find a new number
				}
			}
		}

		if(iLastDigitPos >= 0)//the last number
		{
			iRet = sscanf(&sBuf[iLastDigitPos], "%d", &iSigNum);
			if(1 == iRet)
			{
				return iSigNum;
			}
		}
	}

	return (-1);
}

void sortBLoadResult(IN OUT PACK_LOADINFO *pLoadInfo)
{
	PACK_LOADINFO stLoadInfo[100];//equip max count
	int iLoadInfoCount = 0;
	int iGroup = 0, iOffset = 0;
	int i, j, k;

	if(pLoadInfo != NULL)
	{
		memset(&stLoadInfo, 0, sizeof(stLoadInfo));
		iLoadInfoCount = 0;
		//copy data to tmp data struct
		for(i = 0; i < pLoadInfo->LoadNum; i++)
		{
			for(j = 0; j < iLoadInfoCount; j++)//find insert pos
			{
				if(pLoadInfo->LoadInfo[i].iEquipID == stLoadInfo[j].LoadInfo[0].iEquipID)
				{
					break;
				}
			}

			if(j == iLoadInfoCount)//find none ,append a new one
			{
				iLoadInfoCount++;
			}
			//copy data
			memcpy( &( stLoadInfo[j].LoadInfo[stLoadInfo[j].LoadNum] ),
				&(pLoadInfo->LoadInfo[i] ),
				sizeof( pLoadInfo->LoadInfo[i] ) );
			stLoadInfo[j].LoadNum++;
		}

		//output data from temp data struct
		iGroup = 0;
		iOffset = 0;
		for(i = 0; i < iLoadInfoCount; i++)
		{
			for(j = 0; j < stLoadInfo[i].LoadNum; j++)
			{
				for(k = 0; k < MAXLEN_SIG; k++)
				{
					if(stLoadInfo[i].LoadInfo[j].iSigNo[k] != 0)
					{
						if(iOffset == 0)//current pos
						{
							memcpy(&( pLoadInfo->LoadInfo[iGroup] ),
									&( stLoadInfo[i].LoadInfo[j] ),
									sizeof(stLoadInfo[i].LoadInfo[j]) );

							memset(pLoadInfo->LoadInfo[iGroup].iSigNo,
									0,
									sizeof(pLoadInfo->LoadInfo[iGroup].iSigNo) );

							memset(pLoadInfo->LoadInfo[iGroup].vValue,
									0,
									sizeof(pLoadInfo->LoadInfo[iGroup].vValue) );
						}

						memcpy(&(pLoadInfo->LoadInfo[iGroup].iSigNo[iOffset]  ),
								&( stLoadInfo[i].LoadInfo[j].iSigNo[k] ),
								sizeof(stLoadInfo[i].LoadInfo[j].iSigNo[k]));

						memcpy( &( pLoadInfo->LoadInfo[iGroup].vValue[iOffset] ),
								&( stLoadInfo[i].LoadInfo[j].vValue[k] ),
								sizeof(stLoadInfo[i].LoadInfo[j].vValue[k]) );

						iOffset++;//next pos
						iOffset = iOffset % MAXLEN_SIG;
						if( iOffset == 0)
						{
							iGroup++;
						}
					}
				}
			}

			if(iOffset != 0)//next pos, change new equip
			{
				iGroup++;
				iOffset = 0;
			}
		}

		pLoadInfo->LoadNum = iGroup;
	}
}
//changed by Frank Wu,20131224,1/3, for TR 113 Numbering of load currents ---end---

void PackBLoad(void)
{
	PACK_LOADINFO*	pWrite;
	int		iGrpNum = 0, i, j, k, iTemp, nError, nBufLen, SigNum = 0, iSigNo = 0;
	SIG_BASIC_VALUE*	pSigValue;
	SigINFO_TEMP*		sTemp;
	EQUIP_INFO*		pEquipInfo;
	SAMPLE_SIG_INFO*	pSampSigInfo;

	pWrite = (PACK_LOADINFO*)p;
	iTemp = pUserDefLCD.pDispInfoTemp->iTempNum;

	for(i = 0;i < iTemp;i++)
	{
		if(iGrpNum == MAXNUM_BRANCH)
			break;

		sTemp = pUserDefLCD.pDispInfoTemp->pSigInfoTemp + i;
		if(sTemp->iPageID == BLOAD_SCREEN)
		{
			for(j = sTemp->iEquipID_st;j <= sTemp->iEquipID_end;j++)
			{
				if(DXI_IsEquipExist(j))
				{
					for(k = sTemp->iSigID_st;k <= sTemp->iSigID_end;k++)
					{
						pWrite->LoadInfo[iGrpNum].iEquipID = j;
						nError = DxiGetData(VAR_A_SIGNAL_VALUE,
								j,			
								DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_RESPOND),		
								&nBufLen,			
								(void *)&pSigValue,			
								0);
						pWrite->LoadInfo[iGrpNum].iRespond = pSigValue->varValue.ulValue;

						nError = DxiGetData(VAR_A_EQUIP_INFO,
											j,			
											0,		
											&nBufLen,			
											(void *)&pEquipInfo,			
											0);
						memset(pWrite->LoadInfo[iGrpNum].cEqName, 0, MAXLEN_NAME);
						strncpyz(pWrite->LoadInfo[iGrpNum].cEqName, 
							pEquipInfo->pEquipName->pAbbrName[iLangType], MAXLEN_NAME);
						//printf("EquipName%d=%s; Resp=%d\n", iGrpNum,pWrite->LoadInfo[iGrpNum].cEqName, pSigValue->varValue.ulValue);

				//for(k = sTemp->iSigID_st;k <= sTemp->iSigID_end;k++)
				//{
					nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
								j,			
								DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, k),		
								&nBufLen,			
								&pSampSigInfo,			
								0);

					pWrite->LoadInfo[iGrpNum].iFormat = *(pSampSigInfo->szValueDisplayFmt + 1) - 0x30;

					nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						j,			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, k),		
						&nBufLen,			
						(void *)&pSigValue,			
						0);
					
					if(SIG_VALUE_IS_CONFIGURED(pSigValue)
						&& SIG_VALUE_NEED_DISPLAY(pSigValue))
					{
						//if(SIG_VALUE_IS_VALID(pSigValue))
						//changed by Frank Wu,20131224,2/3, for TR 113 Numbering of load currents
						//iSigNo++;
						iSigNo = (k-sTemp->iSigID_st) + 1;//computeBLoadSigNum(pSampSigInfo->pSigName->pAbbrName[0]);
						pWrite->LoadInfo[iGrpNum].iSigNo[SigNum] = iSigNo;
						pWrite->LoadInfo[iGrpNum].vValue[SigNum] = pSigValue->varValue;
						//printf("Sig %d: %f\n", SigNum, pWrite->LoadInfo[iGrpNum].vValue[SigNum].fValue);
						
						if(SigNum < (MAXLEN_SIG - 1))
						{
							SigNum++;
						}
						else
						{
							//pWrite->LoadInfo[iGrpNum].iSigNum = MAXLEN_SIG;
							iGrpNum++;
							pWrite->LoadInfo[iGrpNum].iEquipID = j;
							pWrite->LoadInfo[iGrpNum].iRespond = pSigValue->varValue.ulValue;
							strncpyz(pWrite->LoadInfo[iGrpNum].cEqName, 
								pEquipInfo->pEquipName->pAbbrName[iLangType], MAXLEN_NAME);
							//printf("EquipName%d=%s;\n", iGrpNum,pWrite->LoadInfo[iGrpNum].cEqName);
							SigNum = 0;
						}	
					}
				}
				if(SigNum != 0)
				{
					//pWrite->LoadInfo[iGrpNum].iSigNum = SigNum;
					iGrpNum++;
					SigNum = 0;
					if(iGrpNum == MAXNUM_BRANCH)
						break;
				}
				}
			}
		}
	}
	pWrite->LoadNum = iGrpNum;

	sortBLoadResult(pWrite);//changed by Frank Wu,20131224,3/3, for TR 113 Numbering of load currents

	//printf("LoadNum=%d,\n", iGrpNum);
}

//changed by Frank Wu,3/4,20140225,for TR160 which Missing alarm levels on temperature bar
static void QueryNorGrpLevelInfo(IN int iScreenID, IN OUT GROUP_INFO *pGrpInfo)
{
#define SYSTEM_TEMP_LEVEL_SIG_START		231
#define BATTGRP_TEMP_LEVEL_SIG_START	201

	int i;
	float fTmpVal= 0;
	int iEquipId, iSigStart;
	int nError;
	int nBufLen;
	SIG_BASIC_VALUE* pSigValue = NULL;

	memset(pGrpInfo->vaLevel, 0, sizeof(pGrpInfo->vaLevel));

	iEquipId = pGrpInfo->iEquipID;
	if(AMBT_DATA_SCTEEN == iScreenID)
	{
		iSigStart = SYSTEM_TEMP_LEVEL_SIG_START + (pGrpInfo->iIndex - 1)*3;
	}
	else if(BATTEMP_DATA_SCREEN == iScreenID)
	{
		iSigStart = BATTGRP_TEMP_LEVEL_SIG_START + (pGrpInfo->iIndex - 1)*3;
	}
	else
	{
		return;
	}

	for(i = 0; i < MAXNUM_NORLEVEL; i++)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iEquipId,
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, iSigStart + i),		
			&nBufLen,
			(void *)&pSigValue,
			0);

		if(nError == ERR_DXI_OK)
		{
			DXI_GetTempSwitch(1,pSigValue->varValue.fValue,
				&fTmpVal,NULL);	
			
			memcpy(&pGrpInfo->vaLevel[MAXNUM_NORLEVEL - i - 1],
					&pSigValue->varValue,
					sizeof(pSigValue->varValue));
			pGrpInfo->vaLevel[MAXNUM_NORLEVEL - i - 1].fValue = fTmpVal;
		}
	}	

}

void PackNorGrp(int iScreenID)
{
	int			iGrpNum, i, j, k, iTemp, nError, nBufLen;
	PACK_GRPINFO*		pWrite;
	SIG_BASIC_VALUE*	pSigValue;
	SigINFO_TEMP*		sTemp;
	SAMPLE_SIG_INFO*	pSampSigInfo;
	//changed by Frank Wu,2/4,20131113,for keeping the same order of temperature sensor in all the LCD pages 
	int nSignalOrderNum = 0;
	float fTempVal = 0.0;

	iGrpNum = 0;

	pWrite = (PACK_GRPINFO*)p;
	iTemp = pUserDefLCD.pDispInfoTemp->iTempNum;

	for(i = 0;i < iTemp;i++)
	{
		sTemp = pUserDefLCD.pDispInfoTemp->pSigInfoTemp + i;
		if(sTemp->iPageID == iScreenID)
		{
			for(j = sTemp->iEquipID_st;j <= sTemp->iEquipID_end;j++)
			{
				for(k = sTemp->iSigID_st; k <= sTemp->iSigID_end;k++)
				{
					//changed by Frank Wu,3/4,20131113,for keeping the same order of temperature sensor in all the LCD pages 
					nSignalOrderNum++;
					nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
								j,			
								DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, k),		
								&nBufLen,			
								&pSampSigInfo,			
								0);

					pWrite->DataInfo[iGrpNum].iFormat = *(pSampSigInfo->szValueDisplayFmt + 1) - 0x30;

					nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						j,			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, k),		
						&nBufLen,			
						(void *)&pSigValue,			
						0);
					//printf("iSigID = %d;Cfg = %d\n", k, pSigValue->usStateMask);
					//printf("iSigID = %d;Fmt=%d;", k, pWrite->DataInfo[iGrpNum].iFormat);
					if(SIG_VALUE_IS_CONFIGURED(pSigValue)
						&& SIG_VALUE_NEED_DISPLAY(pSigValue))
					{
						pWrite->DataInfo[iGrpNum].iEquipID = j;
						pWrite->DataInfo[iGrpNum].iSigID = k;
						pWrite->DataInfo[iGrpNum].iSigType = VAR_FLOAT;
						//changed by Frank Wu,4/4,20131113,for keeping the same order of temperature sensor in all the LCD pages
						pWrite->DataInfo[iGrpNum].iIndex = nSignalOrderNum; 
						if(SIG_VALUE_IS_VALID(pSigValue))
						{
							if((iScreenID == AMBT_DATA_SCTEEN) ||\
								(iScreenID == BATTEMP_DATA_SCREEN))
							{
								DXI_GetTempSwitch(1,pSigValue->varValue.fValue,&fTempVal,NULL);
								pWrite->DataInfo[iGrpNum].vValue.fValue = fTempVal;
							}
							else
							{
								pWrite->DataInfo[iGrpNum].vValue = pSigValue->varValue;
							}
							pWrite->DataInfo[iGrpNum].iRespond = 0;
						}
						else
						{
							DXI_GetTempSwitch(1,0,&fTempVal,NULL);
							pWrite->DataInfo[iGrpNum].vValue.fValue = fTempVal;
							pWrite->DataInfo[iGrpNum].iRespond = 1;
						}
						//printf("Value=%f;Resp=%d\n", pWrite->DataInfo[iGrpNum].vValue.fValue,pWrite->DataInfo[iGrpNum].iRespond);
						//changed by Frank Wu,4/4,20140225,for TR160 which Missing alarm levels on temperature bar
						QueryNorGrpLevelInfo(iScreenID, &pWrite->DataInfo[iGrpNum]);

						iGrpNum++;
					}
				}
			}
		}
	}
	pWrite->DataNum = iGrpNum;
	//printf("TempNum:%d!\n",iGrpNum);
}

void PackEachBatt(void)
{
    printf("PackEachBatt");//HHY
	PACK_BATTINFO* pWrite;
	SIG_BASIC_VALUE*	pSigValue;
	int			i,iIndex = 0, nBufLen, nError, j=0;
	SigINFO_BATT*		sTemp;
	int			iTemp, nTimeOut = 0;
	EQUIP_INFO*		pEquipInfo;
	SAMPLE_SIG_INFO*	pSampSigInfo;
	SET_SIG_INFO*		pSetSigInfo;

	LCD_EquipInfoInit();

	pWrite = (PACK_BATTINFO*)p;
	iTemp = pUserDefLCD.pDispInfoBatt->iBattInfoNum;
	if(iTemp > 3)
		iTemp = 3;

	for(i = 0;i < g_LCDEquipInfo.iBattNum;i++)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					g_LCDEquipInfo.BattInfo[i][1],			
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_STATE),		
					&nBufLen,			
					(void *)&pSigValue,			
					nTimeOut);

		if(pSigValue->varValue.enumValue == 0)
		{
			nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						g_LCDEquipInfo.BattInfo[i][1],			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_RESPOND),		
						&nBufLen,			
						(void *)&pSigValue,			
						nTimeOut);
			pWrite->BattInfo[iIndex].iRespond = pSigValue->varValue.ulValue;

			nError = DxiGetData(VAR_A_EQUIP_INFO,
						g_LCDEquipInfo.BattInfo[i][1],			
						0,		
						&nBufLen,			
						(void *)&pEquipInfo,			
						nTimeOut);
			strncpyz(pWrite->BattInfo[iIndex].cEquipName,pEquipInfo->pEquipName->pAbbrName[iLangType],MAXLEN_NAME);
			pWrite->BattInfo[iIndex].iEquipID = g_LCDEquipInfo.BattInfo[i][1];

			for(j = 0;j < iTemp;j++)
			{
				sTemp = pUserDefLCD.pDispInfoBatt->pSigInfoBatt + j;
				if(sTemp->iSigType == SIG_TYPE_SAMPLING)
				{
					nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
								g_LCDEquipInfo.BattInfo[i][1],			
								DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),		
								&nBufLen,			
								(void *)&pSampSigInfo,			
								0);

					pWrite->BattInfo[iIndex].BattItem[j].iFormat = *(pSampSigInfo->szValueDisplayFmt + 1) - 0x30;
				}
				else if(sTemp->iSigType == SIG_TYPE_SETTING)
				{
					nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
								g_LCDEquipInfo.BattInfo[i][1],			
								DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),		
								&nBufLen,			
								(void *)&pSetSigInfo,			
								0);

					pWrite->BattInfo[iIndex].BattItem[j].iFormat = *(pSetSigInfo->szValueDisplayFmt + 1) - 0x30;
				}

				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
						g_LCDEquipInfo.BattInfo[i][1],			
						DXI_MERGE_SIG_ID(sTemp->iSigType, sTemp->iSigID),		
						&nBufLen,			
						(void *)&pSigValue,			
						nTimeOut);

				//pWrite->BattInfo[iIndex].BattItem[j].iEquipID = g_LCDEquipInfo.BattInfo[i][1];
				pWrite->BattInfo[iIndex].BattItem[j].iSigID = sTemp->iSigID;
				pWrite->BattInfo[iIndex].BattItem[j].vValue = pSigValue->varValue;
				/*printf("EquipID=%d; EqName=%s; EqRes=%d", pWrite->BattInfo[iIndex].iEquipID,
									pWrite->BattInfo[iIndex].cEquipName,
									pWrite->BattInfo[iIndex].iRespond);
				printf("vValue%d:%f;fmt=%d\n", j,pWrite->BattInfo[iIndex].BattItem[j].vValue.fValue,pWrite->BattInfo[iIndex].BattItem[j].iFormat);*/

			}
			iIndex++;
		}
	}
	pWrite->BattNum = iIndex;
	//printf("BattNum=%d", iIndex);
}

/*Rect & SMDU Inv  ScreenID = 04*/
void PackRectInv(void)
{
	SIG_BASIC_VALUE*	pSigValue;
	PACK_INVINFO*		pWrite;
	int i, iMaxNum, nBufLen = 0,iIndex = 0;

	pWrite = (PACK_INVINFO*)p;
	DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_RECTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	iMaxNum = pSigValue->varValue.lValue;

	for(i = 0;i < iMaxNum;i++)
	{
		//if(g_SiteInfo.stCANRectProductInfo[i].bSigModelUsed == TRUE)
		{
			if(i < MAX_NUM1_RECT)
				pWrite->ModuleInv[iIndex].iEquipID = EQIPID_RECT1 + i;
			else
				pWrite->ModuleInv[iIndex].iEquipID = EQIPID_RECT2 + i - MAX_NUM1_RECT;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANRectProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANRectProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANRectProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANRectProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANRectProductInfo[i].szSWVersion, MAXLEN_PROINFO);
			//printf("Rect %d SW:%s!\n", i,g_SiteInfo.stCANRectProductInfo[i].szSWVersion);

			//changed by Frank Wu,20131211,3/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
#if QT_DEBUG
			printf("bSigModelUsed[%d]=%d; ",i, g_SiteInfo.stCANRectProductInfo[i].bSigModelUsed);
			printf("Name=%s\n", g_SiteInfo.stCANRectProductInfo[i].szDeviceAbbrName[iLangType]);
#endif

	}

	for(i = 0; i < SMDUNUM;i++)
	{
		if(g_SiteInfo.stCANSMDUProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = 107 + i;
			//printf("q_SMDU(g_SiteInfo) = %s\n",g_SiteInfo.stCANSMDUProductInfo[0].szSerialNumber);
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANSMDUProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANSMDUProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANSMDUProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANSMDUProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANSMDUProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,4/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}

	pWrite->iModuleNum = iIndex;

	/*DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_RECTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 37),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	printf("!!!CANTIME=%d!\n", pSigValue->varValue.lValue);*/
}

/*Slave Rect Inv ScreenID=05*/
void PackSRectInv(void)
{
	PACK_INVINFO*		pWrite;
	int		iSlave1RectNum=0, iSlave2RectNum=0, iSlave3RectNum=0, iIndex = 0;
	int		iError, iBufLen, i, j;
	SIG_BASIC_VALUE* pSigValue;
	PRODUCT_INFO	piProductInfo ;
	int		iDevice = DXI_GetDeviceNum();
	int		iRectStartDeviceID =  DXI_GetFirstRectDeviceID();

	pWrite = (PACK_INVINFO*)p;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		EQIPID_SLAVERECTGRP1,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SRECTNUM),		
		&iBufLen,			
		(void *)&pSigValue,			
		0);
	if(iError != ERR_DXI_OK)
	{
		iSlave1RectNum = 0;

	}
	else
	{
		iSlave1RectNum = pSigValue->varValue.ulValue;
	}
	//printf("SlaveRect=%d\n", iSlave1RectNum);

	 DxiGetData(VAR_A_SIGNAL_VALUE,
		EQIPID_SLAVERECTGRP2,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SRECTNUM),		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);
	
	if(iError != ERR_DXI_OK)
	{
		iSlave2RectNum = 0;

	}
	else
	{
		iSlave2RectNum = pSigValue->varValue.ulValue;
	}

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		EQIPID_SLAVERECTGRP3,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SRECTNUM),		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);
	
	if(iError != ERR_DXI_OK)
	{
		iSlave3RectNum = 0;

	}
	else
	{
		iSlave3RectNum = pSigValue->varValue.ulValue;
	}

	for(i = iRectStartDeviceID + 110 + 20, j = 0; i <= iDevice && j < iSlave1RectNum; i++,j++)
	{
		if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
		{
				/*iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
					(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
					(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
					(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");*/
			//changed by Frank Wu,2/4,20140317, for twinkle of the green led of slave rectifier
			//pWrite->ModuleInv[j].iEquipID = EQIPID_SLAVE1RECT + i;
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_SLAVE1RECT + j;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, piProductInfo.szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, piProductInfo.szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, piProductInfo.szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, piProductInfo.szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, piProductInfo.szSWVersion, MAXLEN_PROINFO);
//			printf("SR%d:%s;%s;%s;%s;%s\n",iIndex,pWrite->ModuleInv[iIndex].cEquipName,
//					pWrite->ModuleInv[iIndex].cSerialNumber,
//					pWrite->ModuleInv[iIndex].cPartNumber,
//					pWrite->ModuleInv[iIndex].cPVer,
//					pWrite->ModuleInv[iIndex].cSWver);

			//changed by Frank Wu,20131211,5/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[j].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[j].iEquipID);
			pWrite->ModuleInv[j].iSigID = SPECIALID;

			iIndex++;
		}
	}

	for(i = iRectStartDeviceID + 170+ 20, j = 0; i <= iDevice && j < iSlave2RectNum; i++,j++)
	{

		if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
		{
			//changed by Frank Wu,3/4,20140317, for twinkle of the green led of slave rectifier
			//pWrite->ModuleInv[j].iEquipID = EQIPID_SLAVE1RECT + i;
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_SLAVE2RECT + j;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, piProductInfo.szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, piProductInfo.szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, piProductInfo.szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, piProductInfo.szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, piProductInfo.szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,6/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[j].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[j].iEquipID);
			pWrite->ModuleInv[j].iSigID = SPECIALID;

			iIndex++;
		}
	}

	for(i = iRectStartDeviceID + 230 + 20, j = 0; i <= iDevice && j < iSlave3RectNum; i++,j++)
	{
		if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
		{
			//changed by Frank Wu,4/4,20140317, for twinkle of the green led of slave rectifier
			//pWrite->ModuleInv[j].iEquipID = EQIPID_SLAVE1RECT + i;
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_SLAVE3RECT + j;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, piProductInfo.szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, piProductInfo.szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, piProductInfo.szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, piProductInfo.szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, piProductInfo.szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,7/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[j].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[j].iEquipID);
			pWrite->ModuleInv[j].iSigID = SPECIALID;

			iIndex++;
		}
	}

	pWrite->iModuleNum = iIndex;
}

#if 0
void PackRectInv(void)
{
	PRODUCT_INFO	piProductInfo ;
	SIG_BASIC_VALUE*	pSigValue;
	PACK_INVINFO*		pWrite;
	int i, iMaxNum1, iMaxNum2, nBufLen,iEquipID1, iEquipID2, iIndex = 0;
	int		iRectStartDeviceID =  DXI_GetFirstRectDeviceID();
	 int		iRectNumber = DXI_GetRectRealNumber();

	printf("!!!iRectStartDeviceID=%d", iRectStartDeviceID);

	pWrite = (PACK_INVINFO*)p;

	DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_RECTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);

	if(pSigValue->varValue.lValue > MAX_RECTNUM_CAN)
	{
		iMaxNum1 = MAX_RECTNUM_CAN;
		iEquipID1 = EQIPID_RECT1;
		iMaxNum2 = pSigValue->varValue.lValue - MAX_RECTNUM_CAN;
		iEquipID2 = EQIPID_RECT2;
	}
	else
	{
		iMaxNum1 = pSigValue->varValue.lValue;
		iEquipID1 = EQIPID_RECT1;
		iMaxNum2 = 0;
		iEquipID2 = 0;
	}
	printf("RectNum = %d;%d\n", pSigValue->varValue.lValue,iRectNumber);

	//for(i = 0; i < iMaxNum1;i++)
	//for(i = iRectStartDeviceID;i < (iRectStartDeviceID + pSigValue->varValue.lValue);i++)
	for(i = iRectStartDeviceID; i < iRectStartDeviceID + iRectNumber; i++)
	{
		pWrite->ModuleInv[iIndex].iEquipID = iEquipID1 + i;
		//DXI_GetPIByDeviceID((iEquipID1 + i), &piProductInfo);
		if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
		{
		strncpyz(pWrite->ModuleInv[iIndex].cEquipName, piProductInfo.szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, piProductInfo.szSerialNumber, MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, piProductInfo.szPartNumber, MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cPVer, piProductInfo.szHWVersion, MAXLEN_VER);
		strncpyz(pWrite->ModuleInv[iIndex].cSWver, piProductInfo.szSWVersion, MAXLEN_PROINFO);
		printf("No.%d:\nName:%s; Serial:%s;\nPart:%s; Pver:%s;SWver:%s; %s\n",iIndex,
			pWrite->ModuleInv[iIndex].cEquipName,
			pWrite->ModuleInv[iIndex].cSerialNumber,
			pWrite->ModuleInv[iIndex].cPartNumber,
			pWrite->ModuleInv[iIndex].cPVer,
			pWrite->ModuleInv[iIndex].cSWver,
			piProductInfo.szPartNumber);
		iIndex++;
		}
	}

	/*for(i = 0;i < iMaxNum2;i++)
	{
		pWrite->ModuleInv[iIndex].iEquipID = iEquipID2 + i;
		DXI_GetPIByDeviceID((iEquipID2 + i), &piProductInfo);
		strncpyz(pWrite->ModuleInv[iIndex].cEquipName, piProductInfo.szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, piProductInfo.szSerialNumber, MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, piProductInfo.szPartNumber, MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cPVer, piProductInfo.szHWVersion, MAXLEN_VER);
		strncpyz(pWrite->ModuleInv[iIndex].cSWver, piProductInfo.szSWVersion, MAXLEN_PROINFO);
		iIndex++;
	}*/
	pWrite->iModuleNum = iIndex;
}
#endif

/*MPPT & DCEM Inv ScreenID = 10*/
void PackMPPTInv(void)
{
	SIG_BASIC_VALUE*	pSigValue;
	PACK_INVINFO*		pWrite;
	int i, iMaxNum, nBufLen = 0,iIndex = 0;

	pWrite = (PACK_INVINFO*)p;
	DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_MPPTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	iMaxNum = pSigValue->varValue.lValue;
	
	for(i = 0;i < iMaxNum;i++)
	{
		if(g_SiteInfo.stCANMpptProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_MPPT + i;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANMpptProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANMpptProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANMpptProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANMpptProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANMpptProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,8/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}

	}

	for(i = 0; i < DCEMNUM;i++)
	{
		if(g_SiteInfo.stCANDCEMProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = 718 + i;
			//printf("q_SMDU(g_SiteInfo) = %s\n",g_SiteInfo.stCANSMDUProductInfo[0].szSerialNumber);
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANDCEMProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			/*strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANDCEMProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANDCEMProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANDCEMProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANDCEMProductInfo[i].szSWVersion, MAXLEN_PROINFO);*/
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, "-- ", 3);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber,"-- ", 3);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer,"-- ", 3);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, "-- ", 3);
			/*pWrite->ModuleInv[iIndex].cSerialNumber[0] = 0;
			pWrite->ModuleInv[iIndex].cPartNumber[0] = 0;
			pWrite->ModuleInv[iIndex].cPVer[0] = 0;
			pWrite->ModuleInv[iIndex].cSWver[0] = 0;*/
				
			//changed by Frank Wu,20131211,9/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}

	pWrite->iModuleNum = iIndex;
}

void PackSMDUEInv()
{
	SIG_BASIC_VALUE*	pSigValue;
	PACK_INVINFO*		pWrite;
	int i, iMaxNum, nBufLen = 0,iIndex = 0;

	pWrite = (PACK_INVINFO*)p;
/*
	DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_SMDUEGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SMDUENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	iMaxNum = pSigValue->varValue.lValue;
*/
	
	for(i = 0;i < MAXNUM_SMDUE;i++)
	{
		if(g_SiteInfo.stCANSMDUEProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_SMDUE_0 + i;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, 
				g_SiteInfo.stCANSMDUEProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
//			printf("cEquipName:%s\n",g_SiteInfo.stCANSMDUEProductInfo[i].szDeviceAbbrName[iLangType]);
			
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber,
				g_SiteInfo.stCANSMDUEProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
//			printf("cSerialNumber:%s\n",g_SiteInfo.stCANSMDUEProductInfo[i].szSerialNumber);
			
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, 
				g_SiteInfo.stCANSMDUEProductInfo[i].szPartNumber, MAXLEN_PROINFO);
//			printf("cPartNumber:%s\n",g_SiteInfo.stCANSMDUEProductInfo[i].szPartNumber);
			
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, 
				g_SiteInfo.stCANSMDUEProductInfo[i].szHWVersion, MAXLEN_VER);
//			printf("szHWVersion:%s\n",g_SiteInfo.stCANSMDUEProductInfo[i].szHWVersion);
			
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, 
				g_SiteInfo.stCANSMDUEProductInfo[i].szSWVersion, MAXLEN_PROINFO);
//			printf("cSWver:%s\n",g_SiteInfo.stCANSMDUEProductInfo[i].szSWVersion);

			//changed by Frank Wu,20131211,8/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}
	pWrite->iModuleNum = iIndex;
}

void PackConvInv(void)
{
	SIG_BASIC_VALUE*	pSigValue;
	PACK_INVINFO*		pWrite;
	int i, iMaxNum, nBufLen, iIndex = 0;

	pWrite = (PACK_INVINFO*)p;
	DxiGetData(VAR_A_SIGNAL_VALUE,
		EQIPID_CONVGRP,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
		&nBufLen,			
		(void *)&pSigValue,		
		0);
	iMaxNum = pSigValue->varValue.lValue;

	for(i = 0;i < iMaxNum;i++)
	{
		if(g_SiteInfo.stCANConverterProductInfo[i].bSigModelUsed == TRUE)
		{
			if(i < MAX_CONV_GRP1)
			{
				pWrite->ModuleInv[iIndex].iEquipID = EQIPID_CONV1 + i;
			}
			else
			{
				pWrite->ModuleInv[iIndex].iEquipID = EQIPID_CONV2 + i - MAX_CONV_GRP1;
			}
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANConverterProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANConverterProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANConverterProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANConverterProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANConverterProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,10/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}
	pWrite->iModuleNum = iIndex;
}

void PackRS485Inv(void)
{
	PACK_INVINFO*		pWrite;
	int i, iIndex = 0;

	pWrite = (PACK_INVINFO*)p;
	for(i = 0;i < RS485NUM;i++)
	{
		if(g_SiteInfo.st485ProductInfo[i].bSigModelUsed == TRUE)
		{
//#if QT_DEBUG
			//printf("RS485 Nam_CH=%s;\n",g_SiteInfo.st485ProductInfo[i].szDeviceName[1]);
			//printf("RS485 Nam_EN=%s;\n",g_SiteInfo.st485ProductInfo[i].szDeviceName[0]);
//#endif
			pWrite->ModuleInv[iIndex].iEquipID = SPECIALID;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.st485ProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.st485ProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.st485ProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.st485ProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.st485ProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,11/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = SPECIALID;
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}
	pWrite->iModuleNum = iIndex;
}

/*I2C & SMDUP & SMDUH Inv ScreenID=06*/
void PackI2CSMInv(void)
{
	SIG_BASIC_VALUE*	pSigValue;
	PACK_INVINFO*		pWrite;
	PRODUCT_INFO	piProductInfo ;
	int i, nBufLen = 0, iIndex = 0;

	pWrite = (PACK_INVINFO*)p;
	for(i = 0;i < I2CNUM;i++)
	{
		if(g_SiteInfo.stI2CProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = SPECIALID;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stI2CProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stI2CProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stI2CProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stI2CProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stI2CProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,12/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = SPECIALID;
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}

	/*for(i = 0; i < SMDUNUM;i++)
	{
		if(g_SiteInfo.stCANSMDUProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = 107 + i;
			//printf("q_SMDU(g_SiteInfo) = %s\n",g_SiteInfo.stCANSMDUProductInfo[0].szSerialNumber);
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANSMDUProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANSMDUProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANSMDUProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANSMDUProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANSMDUProductInfo[i].szSWVersion, MAXLEN_PROINFO);
				
			iIndex++;
		}
	}*/

	for(i = 0;i < SMDUPNUM1;i++)
	{
		if(g_SiteInfo.stCANSMDUPProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_SMDUP1 + i;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANSMDUPProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANSMDUPProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANSMDUPProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANSMDUPProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANSMDUPProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,13/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}

	for(i = 0;i < SMDUPNUM2;i++)
	{
		if(g_SiteInfo.stCANSMDUPProductInfo[SMDUPNUM1+i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_SMDUP2 + i;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANSMDUPProductInfo[SMDUPNUM1+i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANSMDUPProductInfo[SMDUPNUM1+i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANSMDUPProductInfo[SMDUPNUM1+i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANSMDUPProductInfo[SMDUPNUM1+i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANSMDUPProductInfo[SMDUPNUM1+i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,13/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}

	for(i = 0; i < SMDUHNUM;i++)
	{
		if(g_SiteInfo.stCANSMDUHProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_SMDUH + i;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANSMDUHProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANSMDUHProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANSMDUHProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANSMDUHProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANSMDUHProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,14/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}

	pWrite->iModuleNum = iIndex;
}

/*SMTEMP & LiBatt Inv ScreenID=09*/
void PackLiBattInv(void)
{
	SIG_BASIC_VALUE*	pSigValue;
	PACK_INVINFO*		pWrite;
	int i, nBufLen = 0, iIndex = 0;

	pWrite = (PACK_INVINFO*)p;

	for(i = 0;i < SMTEMPNUM;i++)
	{
		if(g_SiteInfo.stCANSMTempProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = EQIPID_SMTEMP + i;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANSMTempProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANSMTempProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANSMTempProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANSMTempProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANSMTempProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,15/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = GetEquipTypeID(pWrite->ModuleInv[iIndex].iEquipID);
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}

	for(i = 0;i < LIBATTNUM;i++)
	{
		if(g_SiteInfo.stCANLiBattProductInfo[i].bSigModelUsed == TRUE)
		{
			pWrite->ModuleInv[iIndex].iEquipID = SPECIALID;
			strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANLiBattProductInfo[i].szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANLiBattProductInfo[i].szSerialNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANLiBattProductInfo[i].szPartNumber, MAXLEN_PROINFO);
			strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANLiBattProductInfo[i].szHWVersion, MAXLEN_VER);
			strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANLiBattProductInfo[i].szSWVersion, MAXLEN_PROINFO);

			//changed by Frank Wu,20131211,16/26 for twinkle of the green led of rectifier
			pWrite->ModuleInv[iIndex].iEqIDType = SPECIALID;
			pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

			iIndex++;
		}
	}

	if(g_SiteInfo.stCANLiBridgeCardProductInfo.bSigModelUsed == TRUE)
	{
		pWrite->ModuleInv[iIndex].iEquipID = SPECIALID;
		strncpyz(pWrite->ModuleInv[iIndex].cEquipName, g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceAbbrName[iLangType], MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cSerialNumber, g_SiteInfo.stCANLiBridgeCardProductInfo.szSerialNumber, MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cPartNumber, g_SiteInfo.stCANLiBridgeCardProductInfo.szPartNumber, MAXLEN_PROINFO);
		strncpyz(pWrite->ModuleInv[iIndex].cPVer, g_SiteInfo.stCANLiBridgeCardProductInfo.szHWVersion, MAXLEN_VER);
		strncpyz(pWrite->ModuleInv[iIndex].cSWver, g_SiteInfo.stCANLiBridgeCardProductInfo.szSWVersion, MAXLEN_PROINFO);

		//changed by Frank Wu,20131211,17/26 for twinkle of the green led of rectifier
		pWrite->ModuleInv[iIndex].iEqIDType = SPECIALID;
		pWrite->ModuleInv[iIndex].iSigID = SPECIALID;

		iIndex++;
	}
	pWrite->iModuleNum = iIndex;
}

/*void PackModuleInv(int iScreenID)
{
	PACK_INVINFO*		pWrite;
	SIG_BASIC_VALUE*	pSigValue;
	EQUIP_INFO*	pEquipInfo;
	int		nError, nBufLen, nTimeOut=0;
	int		i, j,iRectNo, iEqID1, iEqID2, iEquipID;
	unsigned long	uTemp;

	pWrite = (PACK_INVINFO*)p;

	if(iScreenID == RECTINV_SCREEN)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_RECTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			nTimeOut);
		iEqID1 = EQIPID_RECT1;
		iEqID2 = EQIPID_RECT2;
	}
	else if(iScreenID == SMDUINV_SCREEN)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_MPPTGRP,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_MODULENUM),		
			&nBufLen,			
			(void *)&pSigValue,			
			nTimeOut);
		iEqID1 = EQIPID_MPPT;
		iEqID2 = 0;
	}
	pWrite->iModuleNum = pSigValue->varValue.ulValue;

	for(i = 0;i < pSigValue->varValue.ulValue;i++)
	{
		if(i < 60)
		{
			iEquipID = iEqID1;
			iRectNo = i;
		}
		else
		{
			iEquipID = iEqID2;
			iRectNo = i - 60;
		}
		pWrite->ModuleInv[i].iEquipID = iEquipID;

		nError = DxiGetData(VAR_A_EQUIP_INFO,
			iEquipID + iRectNo,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0);
		strncpyz(pWrite->ModuleInv[i].cEquipName, pEquipInfo->pEquipName->pAbbrName[0], MAXLEN_NAME);

		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iEquipID + iRectNo,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_BARCODE1),		
			&nBufLen,			
			(void *)&pSigValue,			
			nTimeOut);
		pWrite->ModuleInv[i].cBarcodeL[0] = (char)(pSigValue->varValue.ulValue & 0xff);
		pWrite->ModuleInv[i].cBarcodeH[0] = (char)((pSigValue->varValue.ulValue >> 24) & 0xff);
		pWrite->ModuleInv[i].cBarcodeH[1] = (char)((pSigValue->varValue.ulValue >> 16) & 0xff);
		pWrite->ModuleInv[i].cBarcodeH[11] = (char)((pSigValue->varValue.ulValue >> 8) & 0xff);

		for(j = 0;j < 3;j++)
		{
			nError = DxiGetData(VAR_A_SIGNAL_VALUE,
				iEquipID + iRectNo,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, (SIGID_BARCODE2 + j)),		
				&nBufLen,			
				(void *)&pSigValue,			
				nTimeOut);
			pWrite->ModuleInv[i].cBarcodeL[4*j+1] = (char)((pSigValue->varValue.ulValue >> 24) & 0xff);
			pWrite->ModuleInv[i].cBarcodeL[4*j+2] = (char)((pSigValue->varValue.ulValue >> 16) & 0xff);
			pWrite->ModuleInv[i].cBarcodeL[4*j+3] = (char)((pSigValue->varValue.ulValue >> 8) & 0xff);
			pWrite->ModuleInv[i].cBarcodeL[4*j+4] = (char)(pSigValue->varValue.ulValue & 0xff);
		}
		pWrite->ModuleInv[i].cBarcodeL[12] = 0;

		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iEquipID + iRectNo,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SNL),		
			&nBufLen,			
			(void *)&pSigValue,			
			nTimeOut);
		pWrite->ModuleInv[i].cBarcodeH[2] = (char)((pSigValue->varValue.ulValue >> 24) & 0x1f) / 10 + 0x30;
		pWrite->ModuleInv[i].cBarcodeH[3] = (char)((pSigValue->varValue.ulValue >> 24) & 0x1f) % 10 + 0x30;
		pWrite->ModuleInv[i].cBarcodeH[4] = (char)((pSigValue->varValue.ulValue >> 16) & 0x0f) / 10 + 0x30;
		pWrite->ModuleInv[i].cBarcodeH[5] = (char)((pSigValue->varValue.ulValue >> 16) & 0x0f) % 10 + 0x30;
		uTemp = (pSigValue->varValue.ulValue & 0xffff);
		pWrite->ModuleInv[i].cBarcodeH[6] = (char)(uTemp / 10000) + 0x30;
		uTemp = uTemp % 10000;
		pWrite->ModuleInv[i].cBarcodeH[7] = (char)(uTemp / 1000) + 0x30;
		uTemp = uTemp % 1000;
		pWrite->ModuleInv[i].cBarcodeH[8] = (char)(uTemp / 100) + 0x30;
		uTemp = uTemp % 100;
		pWrite->ModuleInv[i].cBarcodeH[9] = (char)(uTemp / 10) + 0x30;
		pWrite->ModuleInv[i].cBarcodeH[10] = (char)uTemp % 10 + 0x30;
		pWrite->ModuleInv[i].cBarcodeH[12] = 0;
		
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iEquipID + iRectNo,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_VER),		
			&nBufLen,			
			(void *)&pSigValue,			
			nTimeOut);
		pWrite->ModuleInv[i].cPVer[0] = (char)((pSigValue->varValue.ulValue >> 24) & 0xff) + 0x41;
		pWrite->ModuleInv[i].cPVer[1] = (char)((pSigValue->varValue.ulValue >> 16) & 0xff) / 10 + 0x30;
		pWrite->ModuleInv[i].cPVer[2] = (char)((pSigValue->varValue.ulValue >> 16) & 0xff) % 10 + 0x30;
		pWrite->ModuleInv[i].fSWVer = (float)(pSigValue->varValue.ulValue & 0xffff) / 100;

		printf("%s: %s; %s; %s;%f\n",pWrite->ModuleInv[i].cEquipName, pWrite->ModuleInv[i].cBarcodeH,
						pWrite->ModuleInv[i].cBarcodeL,pWrite->ModuleInv[i].cPVer,
						pWrite->ModuleInv[i].fSWVer);
	}
}*/
//changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
static void formatIpv6Addr(IN BYTE *pbIpv6, OUT char *pcIpv6, IN int iIpLen)
{
	int i;
	int iVal = 0;
	char *pCh = (char *)pbIpv6;

	for(i = 0; i < 16; i++)
	{
		iVal += pbIpv6[i];
	}

	if(iVal != 0)//valid ip
	{
		inet_ntop(AF_INET6, (void*)pbIpv6, pcIpv6, iIpLen);
	}
	else
	{
		memset((void *)pcIpv6, 0, iIpLen);
	}
	//strcpy(pcIpv6, "09af:19af:29af:39af:49af:59af:69af:79af");
}

void PackSelfInv(void)
{
	PACK_SELFINV* pWrite;
	int	iError, iBufLen, iEnable;
	ACU_PRODUCT_INFO sAcuProductInfo;

	pWrite = (PACK_SELFINV*)p;
	memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
	iBufLen = sizeof(sAcuProductInfo);
	iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
						ACU_PRODUCT_INFO_GET, 
						0, 
						&iBufLen,
						&(sAcuProductInfo),
						0);
	//memset(pWrite->cPartNumber, 0, MAXLEN_NUMBER);
	strncpyz(pWrite->cPartNumber,sAcuProductInfo.szPartNumber, MAXLEN_NUMBER);
	//strncpyz(pWrite->cPartNumber,sAcuProductInfo.szProductNo, MAXLEN_NUMBER);
	strncpyz(pWrite->cSN, sAcuProductInfo.szACUSerialNo,MAXLEN_NAME);
	strncpyz(pWrite->cPVer, sAcuProductInfo.szHWRevision, MAXLEN_NUMBER);
	strncpyz(pWrite->cSWver, sAcuProductInfo.szSWRevision, MAXLEN_NUMBER);
	strncpyz(pWrite->cCfgFileVer, sAcuProductInfo.szCfgFileVersion, MAXLEN_NUMBER);
	strncpyz(pWrite->cFileSystemVer, sAcuProductInfo.szFileSystemVersion, MAXLEN_NUMBER);
	strncpyz(pWrite->cEthaddr, sAcuProductInfo.szEthaddr, LEN_MAC);
	//printf("PartNumber:%s;\n",pWrite->cPartNumber);
	//printf("Serial:%s;\n", pWrite->cSN);
	//printf("PVer:%s\n", pWrite->cPVer);
	//printf("SWVer:%s\n", pWrite->cSWver);
	//printf("Mac:%s/%s/%d",pWrite->cEthaddr, sAcuProductInfo.szEthaddr,sAcuProductInfo.szEthaddr[0]);

	iError = DxiGetData(VAR_DHCP_SERVER_INFO,
					DHCP_SERVER_INFO_ENABLE, 
					0, 
					&iBufLen,
					&iEnable,
					0);
	
	if(iEnable == APP_DHCP_SERVER_ON)
	{
		iError = DxiGetData(VAR_DHCP_SERVER_INFO,
					DHCP_SERVER_INFO_IP, 
					0, 
					&iBufLen,
					&(pWrite->DHCP_IP),
					0);
	}
	else
	{
		pWrite->DHCP_IP = 0xffffffff;
	}
	//printf("DHCP Enable : %d; IP:%d\n", iEnable, pWrite->DHCP_IP);

	iError = DxiGetData(VAR_ACU_NET_INFO,
							NET_INFO_IP,			
							0,		
							&iBufLen,			
							&(pWrite->MAIN_IP),			
							0);

	//changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller��s IP address
	iError = DxiGetData(VAR_NET_FRONT_IP_INFO,
							IP_FRONT_PORT,
							0,
							&iBufLen,
							&(pWrite->FRONT_CRAFT_IP),
							0);
	//end -- changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller��s IP address

	//changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
	DHCP_SERVER_NET_INFO_IPV6 stDHCPServerIPV6;
	ACU_V6_NET_INFO stIpV6Info;

	memset((void *)&stDHCPServerIPV6, 0, sizeof(stDHCPServerIPV6));
	memset((void *)&stIpV6Info, 0, sizeof(stIpV6Info));

	iError = DxiGetData(VAR_DHCP_SERVER_INFO,
		DHCP_SERVER_INFO_ENABLE, 
		DXI_VAR_ID_IPV6, 
		&iBufLen,
		&iEnable,
		0);
	if((iError == ERR_DXI_OK) && (iEnable == APP_DHCP_SERVER_ON))
	{
		iError = DxiGetData(VAR_DHCP_SERVER_INFO,
			DHCP_SERVER_INFO_IP, 
			DXI_VAR_ID_IPV6, 
			&iBufLen,
			&stDHCPServerIPV6.stServerAddr,
			0);
		if(ERR_DXI_OK == iError)
		{
			formatIpv6Addr((BYTE *)stDHCPServerIPV6.stServerAddr.ifr6_addr.in6_u.u6_addr8,
							pWrite->cIpv6DHCPServer,
							sizeof(pWrite->cIpv6DHCPServer));
		}
	}
	else
	{
		memset((void *)pWrite->cIpv6DHCPServer, 0, sizeof(pWrite->cIpv6DHCPServer));
	}


	iError = DxiGetData(VAR_NET_IPV6_INFO,
		0,			
		0,		
		&iBufLen,			
		(void *)&stIpV6Info,
		0);
	if(ERR_DXI_OK == iError)
	{
		formatIpv6Addr((BYTE *)stIpV6Info.stGlobalAddr.ifr6_addr.in6_u.u6_addr8,
			pWrite->cIpv6Global,
			sizeof(pWrite->cIpv6Global));
		formatIpv6Addr((BYTE *)stIpV6Info.stLocalAddr.ifr6_addr.in6_u.u6_addr8,
			pWrite->cIpv6Local,
			sizeof(pWrite->cIpv6Local));
	}
	else
	{
		memset((void *)pWrite->cIpv6Global, 0, sizeof(pWrite->cIpv6Global));
		memset((void *)pWrite->cIpv6Local, 0, sizeof(pWrite->cIpv6Local));
	}

}

//changed by Frank Wu,2/3,20140308, for filter the user of "vertivvadmin"
static BOOL PWDInfo_IsNeedDisplay(IN USER_INFO_STRU *pUserInfo)
{
	const char *pName = pUserInfo->szUserName;
	int iLen = sizeof(pUserInfo->szUserName);

	if( 0 == strncmp(pName, SUPER_ADMIN_NAME, iLen) ||
		0 == strcmp(pName, "vertivadmin"))
	{
		return FALSE;
	}

	return TRUE;
}

void PackPWDInfo(void)
{
	USER_INFO_STRU		*pUserInfo = NULL;
	PACK_PWD*		pWrite;
	int			i, iIndex = 0, iUserNum = 0;

	pWrite = (PACK_PWD*)p;

	if((iUserNum = GetUserInfo(&pUserInfo)) > 0)
	{
		for(i = 0;i < iUserNum;i++)
		{
			//changed by Frank Wu,3/3,20140308, for filter the user of "vertivvadmin"
			if( ! PWDInfo_IsNeedDisplay(&pUserInfo[i]) )
			{
				continue;
			}
			//if((pUserInfo + i)->byLevel > BROWSER_LEVEL)
                        //Changed by Koustubh Mattikalli (IA-01-02)
                        if(((pUserInfo + i)->byLevel > BROWSER_LEVEL) && (strcmp((pUserInfo + i)->szStrongPass,"Enabled")!=0))
			{
				//strncpyz(&(pWrite->UserInfo[iIndex]), (pUserInfo + i), sizeof(USER_INFO_STRU));
				strncpyz(pWrite->UserInfo[iIndex].szUserName, (pUserInfo + i)->szUserName, USERNAME_LEN);
				strncpyz(pWrite->UserInfo[iIndex].szPassword, (pUserInfo + i)->szPassword, PASSWORD_LEN);
				//printf("Name %d:%s; %s\n",i,(pUserInfo + i)->szUserName,(pUserInfo + i)->szPassword);
				//printf("Name %d:%s; %s\n",i,pWrite->UserInfo[iIndex].szUserName, pWrite->UserInfo[iIndex].szPassword);
				iIndex++;
			}
			
		}
	}
	pWrite->iNum = iIndex;
	//printf("iIndex= %d\n",iIndex);
}

static int LCD_fnCmpCondition(IN const void *pRecord, 
				       IN const void *pTCondition)
{
	TEMP_CONDITION *pCondition = (TEMP_CONDITION *)pTCondition;
	static int	lnTempCount=0;

	if((pRecord == NULL) || (pTCondition == NULL))
	{
		return FALSE;
	}

	HIS_DATA_RECORD		*pRecordData = (HIS_DATA_RECORD *)pRecord;

	lnTempCount++;
	if(lnTempCount % 200 == 0)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));
		lnTempCount = 0;
	}

	if((pRecordData->iEquipID == pCondition->iEquipID) 
		&& (pRecordData->iSignalID == pCondition->iSigID))
	{
		//printf("Tst = %d\n",pRecordData->tmSignalTime);
		if((pRecordData->tmSignalTime - pCondition->tmFromTime) >= 0 && 
			    ( pCondition->tmToTime - pRecordData->tmSignalTime) >= 0)
		{	
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}

	//return FALSE;
}

void PackTrendL(void)
{
	int	nBufLen, i,j;
	float	fDataTemp[MAXNUM_TRENDDATA], fRateCurr = 0;
	PACK_TRENDINFO* pWrite;
	HIS_DATA_RECORD_LOAD* pLoadRecord;
	SIG_BASIC_VALUE*	pSigValue;
	time_t		tmTnow;

	pWrite = (PACK_TRENDINFO*)p;
	//pLoadRecord = NEW(HIS_DATA_RECORD_LOAD, 720);

	/*DxiGetData(VAR_HIS_DATA_RECORD_LOAD,
			0,			
			0,		
			&nBufLen,			
			pLoadRecord,			
			0);*/
	pLoadRecord = g_SiteInfo.pLoadCurrentData;
	//pLoadRecord = g_stLoadCurrentData;

	DxiGetData(VAR_A_SIGNAL_VALUE,
			EQIPID_SYS,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIGID_SYSLOAD),		
			&nBufLen,			
			(void *)&pSigValue,			
			0);
	pWrite->fData[MAXNUM_TREND - 1] = pSigValue->varValue.fValue;
	//changed by Frank Wu,2/4,20140221 for TR203 On DC load graph show the load in % if possible show the A and %
	pWrite->fCurrentData[MAXNUM_TREND - 1] = pWrite->fData[MAXNUM_TREND - 1];

	tmTnow = (time(NULL) /TIMEHOUR) * TIMEHOUR - TIMESEC_WEEK;

	if((tmTnow / TIMEHOUR) % 2)
		tmTnow -= TIMEHOUR;
	/*printf("Tnow=%d\n",tmTnow);

	for(i = 0;i < ONE_MONTH_HOUR;i++)
	{
		if(g_stLoadCurrentData[i].bflag == TRUE)
		{
			printf("No.%d bflag=%d;L=%f\n",i,g_stLoadCurrentData[i].bflag,g_stLoadCurrentData[i].floadCurrent);
			printf("NoB.%d bflag=%d;L=%f;T=%d:%d\n",i,(pLoadRecord+i)->bflag,(pLoadRecord+i)->floadCurrent,(pLoadRecord+i)->tm1Date.tm_mday,(pLoadRecord+i)->tm1Date.tm_hour);
		}
	}*/

	fRateCurr = GetRatedCurr();
	//printf ("fRateCurr S3 = %f\n",fRateCurr);
	//tmTnow = time(NULL);

	for(i = 0;i < (MAXNUM_TREND - 1);i++)
	{
		pWrite->fData[i] = -9999;
		fDataTemp[2*i] = -9999;
		fDataTemp[2*i + 1] = -9999;
	//changed by Frank Wu,3/4,20140221 for TR203 On DC load graph show the load in % if possible show the A and %
		pWrite->fCurrentData[i] = pWrite->fData[i];
	}

	if(fRateCurr < 0.1)
		return;

	fRateCurr *= 1.075;	//1.075ÊÇÀÏÍâµÄ¾­ÑéÖµ

	for(j = 0;j < ONE_MONTH_HOUR;j++)
	{
		if(pLoadRecord->bflag == FALSE)
		{
			//printf("No Data!!!");
			return;
		}

		if(pLoadRecord->tmSignalTime < tmTnow)
			pLoadRecord++;
		else
			break;
	}

	if(j >= ONE_MONTH_HOUR)
		return;

	//pLoadRecord += j;
	//printf("j = %d\n fRateCurr=%f",j,fRateCurr);

	for(i = 0;i < MAXNUM_TRENDDATA;i++)
	{
		//printf("T%d=%d;Up/Dn=%d/%d\n",i, pLoadRecord->tmSignalTime,(tmTnow + TIMEHOUR*(i + 1)),(tmTnow + TIMEHOUR * i));
		//printf("No.%d bflag=%d;L=%f\n",i,g_stLoadCurrentData[i].bflag, g_stLoadCurrentData[i].floadCurrent);
		if((pLoadRecord->tmSignalTime < (tmTnow + TIMEHOUR*(i + 1)))
			&& (pLoadRecord->tmSignalTime >= (tmTnow + TIMEHOUR * i)))
		{
			fDataTemp[i] = pLoadRecord->floadCurrent;
			//printf("T%d=%d;Data=%f;Up/Dn=%d/%d\n",i, pLoadRecord->tmSignalTime,fDataTemp[i],(tmTnow + TIMEHOUR*(i + 1)),(tmTnow + TIMEHOUR * i));
			pLoadRecord++;
			j++;
			if(j == ONE_MONTH_HOUR)
				break;
		}
		else
		{
			fDataTemp[i] = -9999;
		}
		
	}

	//printf ("fRateCurr = %f\n",fRateCurr);

	for(i = 0;i < (MAXNUM_TREND - 1);i++)
	{
	//changed by Frank Wu,4/4,20140221 for TR203 On DC load graph show the load in % if possible show the A and %
		//if(fDataTemp[i*2] < -9000)
		//{
		//	if(fDataTemp[i*2+1] < -9000)
		//		pWrite->fData[i] = -9999;
		//	else
		//		pWrite->fData[i] = fDataTemp[i*2+1] * 100 / fRateCurr;
		//}
		//else if(fDataTemp[i*2+1] < -9000)
		//	pWrite->fData[i] = fDataTemp[i*2] * 100 / fRateCurr;
		//else
		//	pWrite->fData[i] = (fDataTemp[i*2] + fDataTemp[i*2+1]) * 100 / 2 / fRateCurr;

		if(fDataTemp[i*2] < -9000)
		{
			if(fDataTemp[i*2+1] < -9000)
			{
				pWrite->fCurrentData[i] = -9999;
				pWrite->fData[i] = pWrite->fCurrentData[i];
			}
			else
			{
				pWrite->fCurrentData[i] = fDataTemp[i*2+1];
				pWrite->fData[i] =  (pWrite->fCurrentData[i]* 100) / fRateCurr;
			}
		}
		else if(fDataTemp[i*2+1] < -9000)
		{
			pWrite->fCurrentData[i] = fDataTemp[i*2];
			pWrite->fData[i] =  (pWrite->fCurrentData[i]* 100) / fRateCurr;
		}
		else
		{
			if(fDataTemp[i*2] > fDataTemp[i*2+1])
			{
				pWrite->fCurrentData[i] = fDataTemp[i*2];
			}
			else
			{
				pWrite->fCurrentData[i] = fDataTemp[i*2+1];
			}
			pWrite->fData[i] =  (pWrite->fCurrentData[i]* 100) / fRateCurr;
		}

		//printf("fData%d = %f\n",i,pWrite->fData[i]);
	}

	//printf("Load=%f\n",pWrite->fData[MAXNUM_TREND - 1]);
	/*for(i = 0;i < (MAXNUM_TREND - 1);i++)
	{
		printf("bflag %d=%d;Load= %f\n", i*2,(pLoadRecord + i*2)->bflag, (pLoadRecord + i*2)->floadCurrent);
		printf("bflag %d=%d;Load= %f\n", i*2+1,(pLoadRecord + i*2+1)->bflag, (pLoadRecord + i*2 + 1)->floadCurrent);

		if((pLoadRecord + i*2)->bflag == FALSE)		//data invalid
		{
			if((pLoadRecord + i*2 + 1)->bflag == FALSE)
				fDataTemp = -9999;
			else
				fDataTemp = (pLoadRecord + i*2 + 1)->floadCurrent;
		}
		else 
		{
			fDataTemp = ((pLoadRecord + i*2)->floadCurrent 
				+ (pLoadRecord + i*2 + 1)->floadCurrent) / 2;
		}

		if(fDataTemp < -9000)
			pWrite->fData[i] = -9999;
		else
		{
			pWrite->fData[i] = fDataTemp * 100/ fRateCurr;
		}
		printf("LoadTrend%d:%f\n", i, pWrite->fData[i]);
	}
	printf("bflag=%d\n", (pLoadRecord + 719)->bflag);*/

	

	//DELETE(pLoadRecord);
}

// changed by Frank Wu, 20131113, 2/9,for only displaying one data in each interval of 2 hours-----start---
/*==========================================================================*
* FUNCTION : QT_FIFO_SetOrGetTrendTUpdateTime
* PURPOSE  : store the latest temperature data updating time for query
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BOOL bIsSet:
*           time_t tmLastestUpdate:
* RETURN   : time_t    :
* COMMENTS : not be used now  
* CREATOR  : 
* DATE: 
*==========================================================================*/
extern time_t QT_FIFO_SetOrGetTrendTUpdateTime(BOOL bIsSet, time_t tmLatestUpdate)
{
	static time_t s_tmLatestUpdate = 0;
	time_t tmRet;
	//lock
	if(bIsSet)
	{
		s_tmLatestUpdate = tmLatestUpdate;
	}

	tmRet = s_tmLatestUpdate;
	//unlock
	return tmRet;
}

/*==========================================================================*
* FUNCTION : ComputeLatestTimePoint
* PURPOSE  : get the latest temperature data updating time
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*           
* RETURN   : time_t    :
* COMMENTS : 
* CREATOR  : 
* DATE: 
*==========================================================================*/
static time_t ComputeLatestTimePoint(void)
{
	//time_t tmCurTime = time((time_t *)0);
	//return ( (time_t)(tmCurTime/tmInterval) )*tmInterval;
#define QT_FIFO_TREND_T_MAX_UPDATE_INTERVAL		(TIMESEC_WEEK)
	static time_t s_tmLatestUpdate = 0;
	int i, count;
	time_t tmUpdate;
	time_t tmCurrent;
	
	if(s_tmLatestUpdate == 0)
	{
		s_tmLatestUpdate = time(NULL);
	}
	
	tmUpdate = g_SiteInfo.tmLatestUpdate;
	
	//checking time range
	tmCurrent = time(NULL);
	if((tmCurrent - tmUpdate > QT_FIFO_TREND_T_MAX_UPDATE_INTERVAL)
		|| (tmUpdate - tmCurrent > QT_FIFO_TREND_T_MAX_UPDATE_INTERVAL))
	{
		return s_tmLatestUpdate;//time except
	}
	//update the latest refreshing time
//	if(s_tmLatestUpdate != tmUpdate)
//	{
//		Sleep(100);
//   }
	s_tmLatestUpdate = tmUpdate;

	return s_tmLatestUpdate;
}

/*==========================================================================*
* FUNCTION : FilterTrendTData
* PURPOSE  : filter max temperature in each sampling interval
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN time_t tmFarthest:
*            IN time_t tmLatest: 
*            IN time_t tmInterval: the interval of display data
*            IN OUT HIS_DATA_RECORD *pHisDataRecord: all these data during the tmFarthest time and the tmLatest time 
*            IN OUT int *piRecords: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
* DATE: 
*==========================================================================*/
static void FilterTrendTData(IN time_t tmFarthest,
							IN time_t tmLatest,	
							IN time_t tmInterval,	
							IN OUT HIS_DATA_RECORD *pHisDataRecord,	
							IN OUT int *piRecords)
{
#define QT_FIFO_TREND_T_OFFSET_FACTOR (0.3)

	int nValidNum = (int)( ( (double)(tmLatest - tmFarthest) )/tmInterval + QT_FIFO_TREND_T_OFFSET_FACTOR);
	int nMaxNum = nValidNum + 5; //required ( nValidNum + n , n > 1)
	HIS_DATA_RECORD *psRecordBuf = NULL;
	int *piMaxTempRecord = NULL;
	int nRecordCount = 0, nMaxTempReocrdIndex = 0, i = 0, nIntervalCount = 0;
	time_t tmRecordTime = 0;

	if( nValidNum < 1 )// parameters with errors
	{
		return;
	}

	psRecordBuf = NEW(HIS_DATA_RECORD, nMaxNum);
	piMaxTempRecord = NEW(int, nMaxNum);
	ASSERT(psRecordBuf);
	ASSERT(piMaxTempRecord);

	for(i = 0; i < nMaxNum; i++)
	{
		piMaxTempRecord[i] = -1; 
	}

	// scan all records for finding out max temperature record in each interval
	nRecordCount = *piRecords;
	for( i = 0; i < nRecordCount; i++ ) //from the farthest record to the latest record
	{
		tmRecordTime = pHisDataRecord[i].tmSignalTime;
		if((tmRecordTime >= tmFarthest) 
			&& (tmRecordTime <= tmLatest))
		{
			nIntervalCount = (tmLatest - tmRecordTime)/tmInterval;//compute this record in which interval 
			//find out the max temperature record in this interval
			nMaxTempReocrdIndex = piMaxTempRecord[nIntervalCount];
			if(nMaxTempReocrdIndex >= 0) 
			{
				// changed by Frank Wu, 20131219, 2/2,for just updating data every 2 hours, eg. 0, 2, 4
				//if(pHisDataRecord[i].varSignalVal.fValue 
				//	> pHisDataRecord[nMaxTempReocrdIndex].varSignalVal.fValue)
				if(pHisDataRecord[i].varSignalVal.fValue 
					>= pHisDataRecord[nMaxTempReocrdIndex].varSignalVal.fValue)
				{
					nMaxTempReocrdIndex = i;
				}
			}
			else
			{
				nMaxTempReocrdIndex = i;
			}
			piMaxTempRecord[nIntervalCount] = nMaxTempReocrdIndex;
		}
	}
	//return all max temperature records in each interval
	nRecordCount = 0;
	for(i = nValidNum - 1; i >= 0; i--)//output all these max temperature records to temporary buffer
	{
		if(piMaxTempRecord[i] >= 0)
		{
			memcpy(&psRecordBuf[nRecordCount], 
				&pHisDataRecord[piMaxTempRecord[i]], 
				sizeof(HIS_DATA_RECORD));
			nRecordCount++;
		}
	}
	//copy records from temporary buffer to the returned parameter	
	for(i = 0; i < nRecordCount; i++)
	{
		memcpy(&pHisDataRecord[i], 
			&psRecordBuf[i], 
			sizeof(HIS_DATA_RECORD));
	}
	*piRecords = nRecordCount;


	DELETE(psRecordBuf);
	DELETE(piMaxTempRecord);	

}
// changed by Frank Wu, 20131113, 2/9,for only displaying one data in each interval of 2 hours-----end---
void PackTrendT(int iScreenID, BOOL isNeedUpdate)
{
	HANDLE		hHisData;
	PACK_TEMPTREND*	pWrite;
	int		iRecords = 6000, iResult,iStartRecordNo = -1;		//record number
	int		i, iBufLen = 6000;

	time_t tmToTime = 0, tmFromTime = 0, tmInterval = 0;

	pWrite = (PACK_TEMPTREND*)p;

	hHisData =	DAT_StorageOpen(HIS_DATA_LOG);//(STAT_DATA_LOG);
	if(hHisData == NULL)
	{
		pWrite->iNum = 0;
		return;
	}

	TEMP_CONDITION *pCondition	= NEW(TEMP_CONDITION, 1);
	ASSERT(pCondition);
	tmInterval = (TIMEHOUR*2); //2 hours
	tmToTime = time((time_t *)0);
	tmFromTime = tmToTime - TIMESEC_WEEK;	//60*60*24*7=604800
	pCondition->tmToTime				= tmToTime;
	pCondition->tmFromTime				= tmFromTime;
	// changed by Frank Wu, 20131113, 4/9,for only displaying one data in each interval of 2 hours-----end---
	printf("Tnow = %d;Tfrm = %d;\n", pCondition->tmToTime,pCondition->tmFromTime);
	// pCondition->iDataType				= iDataType;
	if(iScreenID == AMBT_TREND_SCREEN)
	{
		pCondition->iEquipID			= EQIPID_SYS;
		pCondition->iSigID			= SIGID_AMBTEMP;
	}
	else 
	{
		pCondition->iEquipID			= EQIPID_BATTGRP;
		pCondition->iSigID			= SIGID_BATTTEMP;
	}
	HIS_DATA_RECORD *pHisDataRecord = NEW(HIS_DATA_RECORD, iBufLen);
	ASSERT(pHisDataRecord);
	memset(pHisDataRecord, 0x0, iBufLen * sizeof(HIS_DATA_RECORD));

	iResult = DAT_StorageFindRecords(hHisData,
	    LCD_fnCmpCondition, 
	    (void *)pCondition,
	    &iStartRecordNo, 
	    &iRecords, 
	    (void*)pHisDataRecord, 
	    FALSE,
	    TRUE);
	
	// changed by Frank Wu, 20131113, 5/9,for only displaying one data in each interval of 2 hours-----start---
	FilterTrendTData(tmFromTime, 
		tmToTime, 
		tmInterval,
		pHisDataRecord,
		&iRecords);
	printf("Temp Num:%d\n", iRecords);
	// changed by Frank Wu, 20131113, 5/9,for only displaying one data in each interval of 2 hours-----end---

	if(iRecords > 85)
		iRecords = 85;

	pWrite->iNum = iRecords;
	printf("Temp Num:%d\n", iRecords);

	char szTime[32];

	for(i = 0;i < iRecords;i++)
	{
		pWrite->TempTrend[i].TimeTrend = (pHisDataRecord + i)->tmSignalTime;
		pWrite->TempTrend[i].fTemp = (pHisDataRecord + i)->varSignalVal.fValue;
		TimeToString(pWrite->TempTrend[i].TimeTrend, TIME_HISDATA_FMT, 
				 szTime, sizeof(szTime));

		//printf("No.%d T:%s; f:%f\n", i, szTime, pWrite->TempTrend[i].fTemp);
	}

	DELETE(pCondition);
	DELETE(pHisDataRecord);
}


void UpdateAlm(void)
{
	int OANum, MANum, CANum, nBufLen = 0;
	PACK_ALMNUM*	pWrite;


	pWrite = (PACK_ALMNUM*)pAlm;

	DxiGetData(VAR_ACTIVE_ALARM_NUM,
				ALARM_LEVEL_OBSERVATION,			
				0,		
				&nBufLen,			
				&OANum,			
				0);

	DxiGetData(VAR_ACTIVE_ALARM_NUM,
				ALARM_LEVEL_MAJOR,			
				0,		
				&nBufLen,			
				&MANum,			
				0);

	DxiGetData(VAR_ACTIVE_ALARM_NUM,
				ALARM_LEVEL_CRITICAL,			
				0,		
				&nBufLen,			
				&CANum,			
				0);
	//printf("MA = %d; CA = %d;\n", pWrite->MANum,pWrite->CANum);

	if((pWrite->OANum != OANum) || (pWrite->MANum != MANum) || (pWrite->CANum != CANum))
	{
		pWrite->OANum = OANum;
		pWrite->MANum = MANum;
		pWrite->CANum = CANum;

		sem_post(psemNewAlm);
		
	}
	//munmap(pAlm, 20);
	//close(iMapHandle);
	
}

//changed by Frank Wu,20131228,1/3, for lcd display mode
static void updateLcdDisplayMode(BOOL bNeedReboot)
{
#define QT_FIFO_LCD_DISPLAY_MODE_EQUIP_ID		1
#define QT_FIFO_LCD_DISPLAY_MODE_SIG_TYPE		2
#define QT_FIFO_LCD_DISPLAY_MODE_SIG_ID			186



#define MS_MODE_SIGNAL_ID				180


	SIG_BASIC_VALUE *pSigVal = NULL;
	int iErr;
	int iBufLen;
	struct stat stFileStat;
	int iRet = -1;
	BOOL bRS485Rebot = FALSE;

	//Added by Marco Yang, if now is not standalone mode and there is exist "NotLoadRS485.run" file,
	//Should add the file automatically and reboot.
	//Because QT is the first service to start, so add the code here.
	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
						QT_FIFO_LCD_DISPLAY_MODE_EQUIP_ID,
						DXI_MERGE_SIG_ID(QT_FIFO_LCD_DISPLAY_MODE_SIG_TYPE, MS_MODE_SIGNAL_ID),
						&iBufLen,
						(void *)&pSigVal,
						0);
	if(iErr == ERR_DXI_OK 
		&& pSigVal->varValue.enumValue != 0 //Not standalone
		&& !g_SiteInfo.bLoadAllEquip)
	{
		char szFullPath[MAX_FILE_PATH];
				char szCmdLine[128];

		Cfg_GetFullConfigPath(LOADALLEQUIP_FLAGFILE, szFullPath, MAX_FILE_PATH);
		snprintf(szCmdLine, sizeof(szCmdLine), "rm -rf %s",
			szFullPath);	
		_SYSTEM(szCmdLine);
		bRS485Rebot = TRUE;
	}

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
						QT_FIFO_LCD_DISPLAY_MODE_EQUIP_ID,
						DXI_MERGE_SIG_ID(QT_FIFO_LCD_DISPLAY_MODE_SIG_TYPE, QT_FIFO_LCD_DISPLAY_MODE_SIG_ID),
						&iBufLen,
						(void *)&pSigVal,
						0);

	
	if(iErr == ERR_DXI_OK)
	{
		if(pSigVal->varValue.enumValue == 0)//0.deg
		{
			//make sure file not exist
			iRet = stat(FILENAME_LCD_ROTATION, &stFileStat);
			if(iRet == 0)//file exist
			{
				unlink(FILENAME_LCD_ROTATION);
				//printf("updateLcdDisplayMode 0 unlink\n");
				if(bNeedReboot)
				{
					_SYSTEM("reboot");
				}
			}
		}
		else if(pSigVal->varValue.enumValue == 1)//90.deg
		{
			//make sure file exist
			iRet = stat(FILENAME_LCD_ROTATION, &stFileStat);
			if(iRet != 0)//file not exist
			{
				open(FILENAME_LCD_ROTATION, O_CREAT|O_RDWR);
				//printf("updateLcdDisplayMode 90 open\n");
				if(bNeedReboot)
				{
					_SYSTEM("reboot");
				}
			}
		}
		else if(pSigVal->varValue.enumValue == 2)//big screen
		{
			//make sure file not exist
			iRet = stat(FILENAME_LCD_ROTATION, &stFileStat);
			if(iRet == 0)//file exist
			{
				unlink(FILENAME_LCD_ROTATION);
				//printf("updateLcdDisplayMode big unlink\n");
				if(bNeedReboot)
				{
					_SYSTEM("reboot");
				}
			}
		}
	}

	if(bRS485Rebot)
	{
		_SYSTEM("reboot");
	}
}

//changed by Frank Wu,20140213,6/9, for displaying history temperature curve more faster
static void updateTrendTData(void)
{

	static time_t s_tmTrendTUpdateTime, s_tmTrendTDataTime;
	time_t tmNewUpdateTime = time(NULL);
	time_t tmToTime = 0, tmInterval = 0;

	tmInterval = (TIMEHOUR*2); //2 hours
	tmToTime = ComputeLatestTimePoint();
	tmToTime = ( (int)(tmToTime/tmInterval) )*tmInterval;
	if((TRUE == g_SiteInfo.pstWebLcdShareData->bTrendTIsNeedUpdate)
		|| (tmToTime != s_tmTrendTDataTime)
		|| (tmNewUpdateTime < s_tmTrendTUpdateTime)
		|| (tmNewUpdateTime > s_tmTrendTUpdateTime + (2*TIMEHOUR)))
	{
		g_SiteInfo.pstWebLcdShareData->bTrendTIsNeedUpdate = FALSE;
		s_tmTrendTDataTime = tmToTime;
		s_tmTrendTUpdateTime = tmNewUpdateTime;
		PackTrendT(AMBT_TREND_SCREEN, TRUE);
		PackTrendT(BATTEMP_TREND_SCREEN, TRUE);
	}
}
//changed by Frank Wu,29/30,20140217, for upgrading software of rectifier
#define RT_HVDC_UPDATE				54
#define RT_HVDC_FORCEUPDATE			55

static BOOL QueryRectUpdateResult(OUT int *pResult)
{
#define DLOAD_STATUS_NONE				0
#define DLOAD_STATUS_START_DLOAD		1
#define DLOAD_STATUS_NORMALUPDATE_OK	2
#define DLOAD_STATUS_NORMALUPDATE_FAIL	3
#define DLOAD_STATUS_FORCEUPDATE_OK		4
#define DLOAD_STATUS_FORCEUPDATE_FAIL	5
#define DLOAD_STATUS_COMM_TIMEOUT		6
#define DLOAD_STATUS_OPEN_FILE_FAIL		7

#define RT_SAMP_UpLoadOK_Number			39
#define RT_SAMP_UpLoadOK_State			40

	int iErr;
	int iBuflen = 0;
	SIG_BASIC_VALUE* pSigValue = NULL;
	int iTmp = 0;

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
		EQIPID_RECTGRP,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, RT_SAMP_UpLoadOK_State),
		&iBuflen,			
		(void *)&pSigValue,			
		0);

	if(iErr == ERR_DXI_OK)
	{
		if(pSigValue->varValue.enumValue != DLOAD_STATUS_START_DLOAD)
		{
			if((pSigValue->varValue.enumValue == DLOAD_STATUS_NORMALUPDATE_FAIL)
				|| (pSigValue->varValue.enumValue == DLOAD_STATUS_FORCEUPDATE_FAIL))
			{
				*pResult = -1;
			}
			else if(pSigValue->varValue.enumValue == DLOAD_STATUS_COMM_TIMEOUT)
			{
				*pResult = -2;
			}
			else if(pSigValue->varValue.enumValue == DLOAD_STATUS_OPEN_FILE_FAIL)
			{
				*pResult = -3;
			}
			else
			{
				iTmp = 0;
				iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
					EQIPID_RECTGRP,			
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, RT_SAMP_UpLoadOK_Number),
					&iBuflen,			
					(void *)&pSigValue,			
					0);
				if(iErr == ERR_DXI_OK)
				{
					iTmp = pSigValue->varValue.fValue;
				}

				*pResult = iTmp;
			}

			return TRUE;
		}
	}

	return FALSE;
}

void GetActAlmNum(PACK_ALMNUM *pActAlmNum)
{
	int			i,iAlmNumTemp,nError,nBufLen = 0;
	int			nTimeOut = 0;		//Time out
	ALARM_SIG_VALUE*	pActiveAlarmSigValue = NULL;
	int OANum = 0,CANum = 0,MANum = 0,nTotalAlmNum = 0;

	nError = DxiGetData(VAR_ACTIVE_ALARM_NUM,
		ALARM_LEVEL_NONE,			
		0,		
		&nBufLen,			
		&iAlmNumTemp,			
		0);

	if(iAlmNumTemp != 0)
	{
		pActiveAlarmSigValue = NEW(ALARM_SIG_VALUE, iAlmNumTemp);
		if(pActiveAlarmSigValue == NULL)
		{
			pActAlmNum->CANum = CANum;
			pActAlmNum->MANum = MANum;
			pActAlmNum->OANum = OANum;

			return;
		}
		nBufLen = sizeof(ALARM_SIG_VALUE) * iAlmNumTemp;

		nError = DxiGetData(VAR_ACTIVE_ALARM_INFO,
					ALARM_LEVEL_NONE,			
					0,		
					&nBufLen,			
					pActiveAlarmSigValue,			
					0);
		if(nError != ERR_DXI_OK)
		{
			pActAlmNum->CANum = CANum;
			pActAlmNum->MANum = MANum;
			pActAlmNum->OANum = OANum;
			DELETE(pActiveAlarmSigValue);
			return;
		} 

		nTotalAlmNum = nBufLen / sizeof(ALARM_SIG_VALUE);
		// *ppSigValue = (ALARM_SIG_VALUE *)pActiveAlarmSigValue;
		DXI_MakeAlarmSigValueSort(pActiveAlarmSigValue,nTotalAlmNum);

		if(nTotalAlmNum > 200)
		{
			nTotalAlmNum = 200;
		}
		
		for(i = 0;i < nTotalAlmNum;i++)
		{		
			 switch(pActiveAlarmSigValue[i].iAlarmLevel)
			 {
				case ALARM_LEVEL_NONE:
				{
					;
				}
				break;
				
				case ALARM_LEVEL_OBSERVATION:
				{
					OANum ++;
				}
				break;
				
				case ALARM_LEVEL_MAJOR:
				{
					MANum ++;
				}
				break;
				
				case ALARM_LEVEL_CRITICAL:
				{
					CANum ++;
				}
				break;
			 }
		}
		DELETE(pActiveAlarmSigValue);
	}

	pActAlmNum->CANum = CANum;
	pActAlmNum->MANum = MANum;
	pActAlmNum->OANum = OANum;
}

#define		GET_SERVICE_OF_QT_NAME	"qt_fifo.so"
int QTCommunicate(IN int *iQuitCommand)
{
    printf("QTCommunicate");//HHY
	int i=0;
    int k;
	int nBaseIdx = 0;
	int nBufLen=0;
	int nTempType = 0;
	int	ret,iScreenType, iScreen, iErr;
	float 	fTempVal = 0.0f;
	static int		iSemErrT = 0;//, iGetSlaveT = 0;
	BOOL			bContinueRunning = TRUE;
    //define a bool to check if it's needed to change password,hhy
	static CMD_INFO		CmdInfo;
	VAR_VALUE_EX		sigValue;
	SIG_BASIC_VALUE*	pSigValue = NULL;
	char szFullPath[MAX_FILE_PATH];  //full config file name
	APP_SERVICE		*QT_AppService = NULL;
	
	QT_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_QT_NAME);

	const int cnTempSensorEqpId[MAX_TEMPSENSOR_NUM]= {
								EQUIP_ID_IB2_1,
								EQUIP_ID_EIB1,
								EQUIP_ID_SMTEMP_1,
								EQUIP_ID_SMTEMP_2,
								EQUIP_ID_SMTEMP_3,
								EQUIP_ID_SMTEMP_4,
								EQUIP_ID_SMTEMP_5,
								EQUIP_ID_SMTEMP_6,
								EQUIP_ID_SMTEMP_7,
								EQUIP_ID_SMTEMP_8,
								EQUIP_ID_IB2_2,
                                EQUIP_ID_EIB2,
                                EQUIP_ID_SMDUE1,
                                EQUIP_ID_SMDUE2,};//changed by hhy
	
	int nTempSensorStat[MAXNUM_SEL] = {TEMP_SENSOR_EXIST};

	char s[20] = "/var/mmap.dat";
	char sAlm[20] = "/var/mAlm.dat";
	int iMapHandle;

	iMapHandle = open(sAlm, O_RDWR | O_CREAT);
	lseek(iMapHandle, 0, SEEK_SET);
	write(iMapHandle,"",1);

	pAlm = mmap(NULL, 20, PROT_READ|PROT_WRITE,
			MAP_SHARED, iMapHandle, 0);

	sem_t* psemCmd = sem_open(g_QtCmd, O_CREAT,
            S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);

	sem_t* psemAck = sem_open(g_SvAck, O_CREAT,
            S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);

	psemNewAlm = sem_open(g_NewAlm, O_CREAT,
            S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);

        sem_t* psemBeat = sem_open(g_QtBeat, O_CREAT,
            S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);

	DxiRegisterNotification("Alarm thread", 
		ServiceNotification,
		&g_AlmFlag, 							  
		_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 							  
		g_AlmFlag.iAlmHandle,							  
		_REGISTER_FLAG);

	//sem_post(psemCmd);//debug

	//Load LCD Info for G3
	Cfg_GetFullConfigPath(USER_DEF_LCDCFG, szFullPath, MAX_FILE_PATH);
	ret = Cfg_LoadUserDefLCDInfo(szFullPath);
	if (ret != ERR_CFG_OK)
	{
		AppLogOut(CFG_LOADER, APP_LOG_ERROR, 
			"[%s]--Cfg_InitialConfig: ERROR: Load LCD solution config "
			"failed!\n", __FILE__);
		return ret;
	}

	LCD_EquipInfoInit();

#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread LCD Service was created, Thread Id = %d.\n", gettid());
#endif

	updateLcdDisplayMode(TRUE);//changed by Frank Wu,20131228,3/3, for lcd display mode

	while(bContinueRunning)
	{
		
		RunThread_Heartbeat(RunThread_GetId(NULL));

		UpdateAlm();

		GetActAlmNum(&g_stActAlmNum);
		
		updateLcdDisplayMode(FALSE);//changed by Frank Wu,20131228,2/3, for lcd display mode
		if(psemCmd == SEM_FAILED)
		{
			//printf("psemCmd create fail!\n");
			Sleep(50);//yield
			psemCmd = sem_open(g_QtCmd, O_CREAT,
				S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);

			if(iSemErrT < 10)
			{
				iSemErrT++;
			}
			else
			{
				AppLogOut("QT_FIFO", APP_LOG_ERROR, "%s", "psemCmd fail!\n");
				bContinueRunning = FALSE;
			}
			continue;
		}
		else
		{
			iSemErrT = 0;
		}

		while(sem_trywait(psemCmd) != 0)
		{
			UpdateAlm();

			RunThread_Heartbeat(RunThread_GetId(NULL));

			if (*iQuitCommand == SERVICE_STOP_RUNNING)
			{
				bContinueRunning = FALSE;
				break;
			}


			if(sem_trywait(psemBeat) != 0)
			{
				if(iQTRunning == mBEATNUM)
				{
					AppLogOut("QT_FIFO", APP_LOG_ERROR, "%s", "QT exit!\n");
					bContinueRunning = FALSE;
					break;
				}
				else
					iQTRunning++;
			}
			else
				iQTRunning = 0;
			
			Sleep(INTERVALTIME);//yield
		}

		if (!bContinueRunning)
		{
			break;
		}

		iQTRunning = 0;
		//printf("Get Cmd=%d\n",time(NULL));
		
		CmdInfo.iMapHandle = open(s, O_RDWR);
	
		if(CmdInfo.iMapHandle < 0)
		{
			//printf("open fail");
			CmdInfo.iMapHandle = open(s, O_RDWR);
			continue;
		}
        //printf("Cmd Open OK!!!\n");

		p = mmap(NULL, DATA_LEN, PROT_READ|PROT_WRITE,
			MAP_SHARED, CmdInfo.iMapHandle, 0);
		
		if (MAP_FAILED==p)
		{
			//printf("mmap fail");
			continue;
		}

		read(CmdInfo.iMapHandle, 
				&CmdInfo.CmdType,
				CMD_SIZE);

		memset(p, 0,DATA_LEN);	

		DxiGetData(VAR_A_SIGNAL_VALUE,
						EQIPID_SYS,			
						DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SIGID_LANG),		
						&nBufLen,			
						(void *)&pSigValue,			
						0);
		iLangType = pSigValue->varValue.enumValue;
        //printf("iLangType:%d\n",iLangType);
        //printf("CmdInfo.CmdType<%d>",CmdInfo.CmdType);//HHY
		if(CmdInfo.CmdType == CMD_READ)
		{
			iScreenType = (CmdInfo.ScreenID>>16) & 0xff;
			iScreen = CmdInfo.ScreenID & 0xffff;

            //printf("Sitename/iScreen = %d; Type=%d\n", iScreen,iScreenType);
			Head_t *pHead = (Head_t *)p;//changed by Frank Wu,20140110,19/19, for add ScreenID
			if(pHead != NULL)
			{
				pHead->iScreenID = CmdInfo.ScreenID;
			}

			switch(iScreenType)
			{
				case NORMAL_STRU_SCREENTYPE:
				{
					PackMainScreen(iScreen);
				}
				break;

				case ICO_SCREENTYPE:
				{
					PackICOScreen(iScreen);
				}
				break;

				case MODULE_SCREENTYPE:
				{
					PackModule(iScreen);
				}
				break;

				case NORMAL_VALUE_SCREENTYPE:
				{
					PackNormalValue(iScreen);
				}
				break;

				case BATT_SCREENTYPE:
				{
					PackEachBatt();
				}
				break;

				case SETTING_SCREENTYPE:
				{
                //printf("packsetting");
					PackSetting(iScreen);
				}
				break;

				case NORMAL_VALUEGRP_SCREENTYPE:
				{
					if(iScreen == BLOAD_SCREEN)
					{
						PackBLoad();
					}
					else
					{
						PackNorGrp(iScreen);
					}
				}
				break;

				case SPECIAL_SCREENTYPE:
				{
					if((iScreen == HISALM_SCREEN) || \
						(iScreen == HISALMNUM_SCREEN))
					{
						PackHisAlmNum(iScreen);
					}
					else if(iScreen == ACTIVEALMNUM_SCREEN)
					{
						PackActAlmNumScreen();
					}
					else if(iScreen == ACTIVEALM_SCREEN)
					{
						PackActALMScreen();
					}
					else if(iScreen == RECTINV_SCREEN)
					{
						PackRectInv();
					}
					else if(iScreen == SRECTINV_SCREEN)
					{
						PackSRectInv();
					}
					else if(iScreen == SMI2CINV_SCREEN)
					{
						PackI2CSMInv();
					}
					else if(iScreen == RS485INV_SCREEN)
					{
						PackRS485Inv();
					}
					else if(iScreen == CONVINV_SCREEN)
					{
						PackConvInv();
					}			
					else if(iScreen == LIBATTINV_SCREEN)
					{
						PackLiBattInv();
					}
					else if(iScreen == MPPTINV_SCREEN)
					{
						PackMPPTInv();
					}
					else if(iScreen == SMDUEINV_SCREEN)
					{
						PackSMDUEInv();
					}
					else if(iScreen == SELFINV_SCREEN)
					{
						PackSelfInv();
					}
					else if(iScreen == PWD_SCREEN)
					{
						PackPWDInfo();
					}
					else if((iScreen == AMBT_TREND_SCREEN) || \
							(iScreen == BATTEMP_TREND_SCREEN))
					{
						//changed by Frank Wu,20140213,8/9, for displaying history temperature curve more faster
						PackTrendT(iScreen, TRUE);
					}
					else if(iScreen == LTREND_ICO_SCREEN)
					{
						PackTrendL();
					}
				}
				break;

				default:
				break;
			}
		}
		else if(CmdInfo.CmdType == CMD_SET)
		{
			int*	pWrite;
			pWrite = (int*)p;

			if(CmdInfo.setinfo.EquipID == SPECIALID)
			{
				switch(CmdInfo.setinfo.SigID)
				{
					case SITENAME_SETTING:
					{
						nBufLen = strlen(CmdInfo.setinfo.Name);
						iErr = DxiSetData(VAR_ACU_PUBLIC_CONFIG,
									SITE_NAME,
									MODIFY_SITE_ENGLISH_FULL_NAME,
									nBufLen,
									CmdInfo.setinfo.Name,
									0);
					}
					break;
					
					//changed by Frank Wu,20140108
					case ONLY_DATE_SETTING:
					case ONLY_TIME_SETTING:
					case TIME_SETTING:
					{
						nBufLen = sizeof(time_t);
						iErr = DxiSetData(VAR_TIME_SERVER_INFO,
							SYSTEM_TIME,			
							0,		
							nBufLen,			
							&CmdInfo.setinfo.value.lValue,			
							0);
						//printf("SetTime=%d", CmdInfo.setinfo.value.lValue);
					}
					break;
					
					case IP_SETTING:
					case MASK_SETTING:
					case GATEWAY_SETTING:
					{
						nBufLen = sizeof(unsigned long);
						iErr = DxiSetData(VAR_ACU_NET_INFO,
								(NET_INFO_IP + CmdInfo.setinfo.SigID - IP_SETTING),			
								0,		
								nBufLen,			
								&CmdInfo.setinfo.value.ulValue,			
								0);
					}
					break;
										
					case DEFAULTCFG_SETTING:
					{
						iErr = DXI_ReloadDefaultConfig();
					}
					break;
					
					case DHCPCLIENT_SETTING:
					{
						nBufLen = sizeof(int);
						iErr = DxiSetData(VAR_APP_DHCP_INFO,
									0,			
									0,		
									nBufLen,			
									&CmdInfo.setinfo.value.ulValue,			
									0);
						//printf("!!Err = %d\n", iErr);
						 VAR_VALUE_EX  varValueEx;
						  int nDHCP;

						 memset(&varValueEx, 0, sizeof(varValueEx));

						   varValueEx.nSendDirectly = EQUIP_CTRL_SEND_URGENTLY;
						   varValueEx.nSenderType = SERVICE_OF_LOGIC_CONTROL;
						   varValueEx.pszSenderName = "";

						   if(DxiGetData(VAR_APP_DHCP_INFO,
								  0,
								  0,
								  &nBufLen,
								  &nDHCP,
								  0) == ERR_DXI_OK)
						   {
								  varValueEx.varValue.enumValue = (nDHCP == APP_DCHP_ERR);

								  DxiSetData(VAR_A_SIGNAL_VALUE,
										 EQIPID_SYS,               
										 DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 54),//54      DHCP Failure             
										 sizeof(VAR_VALUE_EX),                 
										 &(varValueEx),                  
										 0);
								 
								  varValueEx.varValue.enumValue = (nDHCP != APP_DHCP_OFF);

								  DxiSetData(VAR_A_SIGNAL_VALUE,
										 EQIPID_SYS,               
										 DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 485),//54      DHCP Failure             
										 sizeof(VAR_VALUE_EX),                 
										 &(varValueEx),                  
										 0);
						   }
					}
					break;
					
					case AUTOCFG_SETTING:
					{
						if(DXI_AutoConfigWithReboot())
						{
							iErr = ERR_EQP_OK;
						}
						else
						{
							iErr = ERR_DXI_INVALID_VARIABLE_TYPE;
						}
					}
					break;
					
					//changed by Frank Wu,20131228,4/6,for support settings of part EEM protocol in LCD ---start---
					case	COMM_PROTOCOL_SETTING:
					{
						void *pCmdPara = &CmdInfo.setinfo.value.ulValue;

						iErr = ERR_DXI_INVALID_VARIABLE_TYPE;
						if(ProcCmdSetCommProtocol(pCmdPara))
						{
							iErr = ERR_EQP_OK;
						}
					}
					break;
					
					case YDN23_ADDR_SETTING:
					case MODBUS_ADDR_SETTING:
					{
						void *pCmdPara = &CmdInfo.setinfo.value.ulValue;

						iErr = ERR_DXI_INVALID_VARIABLE_TYPE;
						if(ProcCmdSetCommAddr(pCmdPara))
						{
							iErr = ERR_EQP_OK;
						}
					}
					break;		
					
					case YDN23_METHOD_SETTING:
					case MODBUS_METHOD_SETTING:
					{
						void *pCmdPara = &CmdInfo.setinfo.value.ulValue;

						iErr = ERR_DXI_INVALID_VARIABLE_TYPE;
						if(ProcCmdSetCommMedia(pCmdPara))
						{
							iErr = ERR_EQP_OK;
						}
					}
					break;				
					
					case YDN23_BAUDRATE_SETTING:
					case MODBUS_BAUDRATE_SETTING:
					{
						void *pCmdPara = &CmdInfo.setinfo.value.ulValue;

						iErr = ERR_DXI_INVALID_VARIABLE_TYPE;
						if(ProcCmdSetCommBaudrate(pCmdPara))
						{
							iErr = ERR_EQP_OK;
						}
						break;
					}
					//changed by Frank Wu,20131228,4/6,for support settings of part EEM protocol in LCD ---end---
					//changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
					//case IPV6_IP_SETTING_1:
					case IPV6_IP_SETTING_2:
					case IPV6_IP_SETTING_ROT90_3:
					{
						ACU_V6_NET_INFO stIpV6Info;

						memset((void *)&stIpV6Info, 0, sizeof(stIpV6Info));
						if(sizeof(CmdInfo.setinfo.Name) >= 16)
						{
							memcpy((void *)stIpV6Info.stGlobalAddr.ifr6_addr.in6_u.u6_addr8,
								(void *)CmdInfo.setinfo.Name,
								16);

							iErr = DxiSetData(VAR_NET_IPV6_INFO,
								IPV6_CHANGE_ADDR,
								0,
								nBufLen,
								(void *)&stIpV6Info,
								0);
						}
						else
						{
							iErr = !ERR_DXI_OK;
						}
					}
					break;
					
					case IPV6_GATEWAY_SETTING_2:
					case IPV6_GATEWAY_SETTING_ROT90_3:
					{
						ACU_V6_NET_INFO stIpV6Info;

						memset((void *)&stIpV6Info, 0, sizeof(stIpV6Info));
						if(sizeof(CmdInfo.setinfo.Name) >= 16)
						{
							memcpy((void *)stIpV6Info.stGateWay.in6_u.u6_addr8,
								(void *)CmdInfo.setinfo.Name,
								16);

							iErr = DxiSetData(VAR_NET_IPV6_INFO,
								IPV6_CHANGE_GW,
								0,
								nBufLen,
								(void *)&stIpV6Info,
								0);
						}
						else
						{
							iErr = !ERR_DXI_OK;
						}
					}
					break;
					
					case IPV6_MASK_SETTING:
					{
						ACU_V6_NET_INFO stIpV6Info;

						memset((void *)&stIpV6Info, 0, sizeof(stIpV6Info));
							stIpV6Info.stGlobalAddr.ifr6_prefixlen = CmdInfo.setinfo.value.ulValue;

							iErr = DxiSetData(VAR_NET_IPV6_INFO,
								IPV6_CHANGE_PREFIX,
								0,
								nBufLen,
								(void *)&stIpV6Info,
								0);
					}
					break;
					
					case IPV6_DHCPCLIENT_SETTING:
					{
						nBufLen = sizeof(int);
						iErr = DxiSetData(VAR_APP_DHCP_INFO,
									DXI_VAR_ID_IPV6,			
									0,		
									nBufLen,			
									&CmdInfo.setinfo.value.ulValue,			
									0);
						 VAR_VALUE_EX  varValueEx;
						  int nDHCP;

						 memset(&varValueEx, 0, sizeof(varValueEx));

						   varValueEx.nSendDirectly = EQUIP_CTRL_SEND_URGENTLY;
						   varValueEx.nSenderType = SERVICE_OF_LOGIC_CONTROL;
						   varValueEx.pszSenderName = "";

						   if(DxiGetData(VAR_APP_DHCP_INFO,
								  DXI_VAR_ID_IPV6,
								  0,
								  &nBufLen,
								  &nDHCP,
								  0) == ERR_DXI_OK)
						   {
								  varValueEx.varValue.enumValue = (nDHCP == APP_DCHP_ERR);

								  DxiSetData(VAR_A_SIGNAL_VALUE,
										 EQIPID_SYS,               
										 DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 54),//54      DHCP Failure             
										 sizeof(VAR_VALUE_EX),                 
										 &(varValueEx),                  
										 0);

								varValueEx.varValue.enumValue = (nDHCP != APP_DHCP_OFF);

								DxiSetData(VAR_A_SIGNAL_VALUE,
										 EQIPID_SYS,               
										 DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 485),//54      DHCP Failure             
										 sizeof(VAR_VALUE_EX),                 
										 &(varValueEx),                  
										 0);
						   }
					}
					break;
					
					default:
					break;
				}
			}
			else
			{
				if((CmdInfo.setinfo.EquipID == EQIPID_SYS) && 
				(CmdInfo.setinfo.SigType == SIG_TYPE_SAMPLING) && 
				(CmdInfo.setinfo.SigID == SIGID_ALMVOICE_ACTIVATE))
				{
					SIG_BASIC_VALUE*	pSigValueTemp = NULL;
					iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
								EQIPID_SYS,			
								DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,ALM_VOICE_ENABLE),		
								&nBufLen,			
								&pSigValueTemp,			
								0);
					if(pSigValueTemp->varValue.enumValue == 1)
					{
						CmdInfo.setinfo.value.enumValue = ALM_VOICE_ACTIVATE_NO;
					}
				}
				
				if(CmdInfo.setinfo.SigType == SIG_TYPE_CONTROL)
				{
					CTRL_SIG_INFO*	pCtrlSigInfo;
				
					iErr = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
								CmdInfo.setinfo.EquipID,			
								DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, CmdInfo.setinfo.SigID),		
								&nBufLen,			
								&pCtrlSigInfo,			
								0);
					if(pCtrlSigInfo->iSigValueType == VAR_ENUM)
					{
						if(pCtrlSigInfo->iStateNum == mSINGLE)
						{
							sigValue.varValue.enumValue = TRUE;
						}
						else
						{
							sigValue.varValue = CmdInfo.setinfo.value;
						}
					}
					else
					{
						sigValue.varValue = CmdInfo.setinfo.value;
					}
				}
				else
				{
					sigValue.varValue = CmdInfo.setinfo.value;

					//ÉèÖÃÎÂ¶ÈÌ½ÕëÐèÒªÌØÊâ´¦Àí¡£ÒòÎªÉèÖÃÊ±ºòµÄÃ¶¾ÙÖµ²»ÊÇÅäÖÃÎÄ¼þÖÐµÄË³Ðò£¬
					//ËùÒÔÏÈÒª°ÑÑ¡ÖÐµÄÃ¶¾ÙÏîµÄÐòºÅ×ª»»³ÉÅäÖÃÎÄ¼þÖÐ¶ÔÓ¦µÄÐòºÅ£¬ÔÙ ÉèÖÃ¡£
					if((CmdInfo.setinfo.EquipID == 115) &&
						(CmdInfo.setinfo.SigType == SIG_TYPE_SETTING) && \
						(CmdInfo.setinfo.SigID == 63))
					{
						//ÏÈ³õÊ¼»¯ËùÓÐµÄÎÂ¶ÈÌ½ÕëÎª´æÔÚ¡£
						for(i=0;i<MAXNUM_SEL;i++)
						{
							nTempSensorStat[i] = TEMP_SENSOR_EXIST;
						}

						//¼ì²é¶ÔÓ¦µÄÓ²¼þÉè±¸ÊÇ·ñ´æÔÚ£¬Èç¹û²»´æÔÚ ¶ÔÓ¦µÄÌ½ÕëÒ²²»´æÔÚ¡£
                        //add by hhy,which is use for handle the SMDUE1 SMDUE2.
                        for(i = 0;i < MAX_TEMPSENSOR_NUM;i++)//MAX_TEMPSENSOR_NUM
						{
                            if (cnTempSensorEqpId[i]== EQUIP_ID_SMDUE1){

                                for(k=79;k<=88;k++){
                                    if(!IsEquipExist(cnTempSensorEqpId[i])){

                                        nTempSensorStat[k] = TEMP_SENSOR_NOT_EXIST;
                                    }
                                }

                            }else if (cnTempSensorEqpId[i]== EQUIP_ID_SMDUE2){

                                for(k=89;k<=98;k++){
                                    if(!IsEquipExist(cnTempSensorEqpId[i])){

                                        nTempSensorStat[k] = TEMP_SENSOR_NOT_EXIST;
                                    }
                                }

                            }
							if(!IsEquipExist(cnTempSensorEqpId[i]))
							{

                                //printf("cnTempSensorEqpId[i]<%d>",cnTempSensorEqpId[i]);//hhy
								switch(cnTempSensorEqpId[i])
								{
									case EQUIP_ID_IB2_1:
									{
										nTempSensorStat[7] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[8] = TEMP_SENSOR_NOT_EXIST;
									}
									break;

									case EQUIP_ID_IB2_2:
									{
										nTempSensorStat[75] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[76] = TEMP_SENSOR_NOT_EXIST;										
									}
									break;

									case EQUIP_ID_EIB1:
									{
										nTempSensorStat[9] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[10] = TEMP_SENSOR_NOT_EXIST;										
									}
									break;

									case EQUIP_ID_EIB2:
									{
										nTempSensorStat[77] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[78] = TEMP_SENSOR_NOT_EXIST;												
									}
									break;

									case EQUIP_ID_SMTEMP_1:
									case EQUIP_ID_SMTEMP_2:
									case EQUIP_ID_SMTEMP_3:
									case EQUIP_ID_SMTEMP_4:
									case EQUIP_ID_SMTEMP_5:
									case EQUIP_ID_SMTEMP_6:
									case EQUIP_ID_SMTEMP_7:
									case EQUIP_ID_SMTEMP_8:
									{
										nBaseIdx = 11+8*(cnTempSensorEqpId[i]-EQUIP_ID_SMTEMP_1);
										nTempSensorStat[nBaseIdx] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[nBaseIdx+1] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[nBaseIdx+2] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[nBaseIdx+3] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[nBaseIdx+4] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[nBaseIdx+5] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[nBaseIdx+6] = TEMP_SENSOR_NOT_EXIST;
										nTempSensorStat[nBaseIdx+7] = TEMP_SENSOR_NOT_EXIST;
									}
									break;

									default:
										break;
								}
							}
						}
						
						//×ª»»Ã¶¾ÙÖµ
						for(i = 0,nBaseIdx = 0;i<MAXNUM_SEL;i++)
						{
                            //printf("change the emun");
							if(nTempSensorStat[i] == TEMP_SENSOR_EXIST)
							{
								if(nBaseIdx == CmdInfo.setinfo.value.enumValue)
								{
									sigValue.varValue.enumValue = i;
									sigValue.varValue.lValue = i;
                                    sigValue.varValue.ulValue = i;
									break;
								}
								nBaseIdx++;
							}
						}
					}

					if((CmdInfo.setinfo.EquipID == EQIPID_SYS) && \
							(CmdInfo.setinfo.SigID == SIGID_REXPAN))
					{
						iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
							EQIPID_SYS,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SIGID_REXPAN),		
							&nBufLen,			
							(void *)&pSigValue,			
							0);
						if(pSigValue->varValue.ulValue != CmdInfo.setinfo.value.ulValue)
						{
							//bContinueRunning = FALSE;
							sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
							sigValue.pszSenderName = "LCD_UI";
							sigValue.nSenderType = SERVICE_OF_USER_INTERFACE;

							iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
									CmdInfo.setinfo.EquipID,
									DXI_MERGE_SIG_ID(CmdInfo.setinfo.SigType, CmdInfo.setinfo.SigID),
									sizeof(VAR_VALUE_EX),
									&sigValue,
									0);
							AppLogOut("QT_FIFO", APP_LOG_INFO, "%s", "Rect Expansion!\n");
							_SYSTEM("reboot");
						}
					}
				}
				
				iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
							CmdInfo.setinfo.EquipID,			
							DXI_MERGE_SIG_ID(CmdInfo.setinfo.SigType, CmdInfo.setinfo.SigID),		
							&nBufLen,			
							(void *)&pSigValue,			
							0);
				//Èç¹ûÊÇÎÂ¶ÈÐÅºÅ»òÕßÎÂ¶ÈÏµÊýÐÅºÅÔòÐèÒª½«µ±Ç°ÊýÖµ×ª»»
				//³É¶ÔÓ¦µÄÊýÖµÔÙÉèÖÃ¡£
				if(pSigValue->ucType == VAR_FLOAT)
				{
					if(DXI_isTempSignal(pSigValue))
					{
						DXI_SetTempSwitch(1,CmdInfo.setinfo.value.fValue,&fTempVal);
						sigValue.varValue.fValue = fTempVal;
					}
					else if(IS_TEMP_COE_SIGNAL(CmdInfo.setinfo.EquipID,CmdInfo.setinfo.SigType, CmdInfo.setinfo.SigID))
					{
						DXI_SetTempSwitch(0,CmdInfo.setinfo.value.fValue,&fTempVal);
						sigValue.varValue.fValue = fTempVal;
					}
				}
				
				//changed by Frank Wu,20140102,1/3, for the rectifier setting menu in LCD
				int iTimeout = 0;
				if(CmdInfo.setinfo.EquipID == EQIPID_RECTGRP)
				{
					iTimeout = EQIPID_RECTGRP_TIMEOUT;
				}

				sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
				sigValue.pszSenderName = "LCD_UI";
				sigValue.nSenderType = SERVICE_OF_USER_INTERFACE;


				if((CmdInfo.setinfo.EquipID == TL1_CFG_SIG_EQUIP_ID)
					&&(CmdInfo.setinfo.SigType == TL1_CFG_SIG_SIGTYPE_ID)
					&& (CmdInfo.setinfo.SigID >= TL1_CFG_SIG_SIGID_START)
					&& (CmdInfo.setinfo.SigID <= TL1_CFG_SIG_SIGID_END))
				{
					TL1_COMMON_CONFIG		stCommonConfig;
					int						iModifyType;
					
					iModifyType = 0;
					iModifyType |= TL1_CFG_W_MODE;//bit:0, read, 1,write

					strncpyz(stCommonConfig.cModifyUser, "LUI", sizeof(stCommonConfig.cModifyUser));

					if(TL1_CFG_SIG_SIGID_PORT_ACTIVATION == CmdInfo.setinfo.SigID)
					{
						iModifyType |= TL1_CFG_PORT_ACTIVATION;
						stCommonConfig.iPortActivation = CmdInfo.setinfo.value.enumValue;
					}
					else if(TL1_CFG_SIG_SIGID_MEDIA_TYPE == CmdInfo.setinfo.SigID)
					{
						iModifyType |= TL1_CFG_MEDIA_TYPE;
						stCommonConfig.iMediaType = TL1_MEDIA_TYPE_TCPIP + CmdInfo.setinfo.value.enumValue;
					}
					else if(TL1_CFG_SIG_SIGID_PORT_NUMBER == CmdInfo.setinfo.SigID)
					{
						iModifyType |= TL1_CFG_MEDIA_PORT_PARAM;
						snprintf(stCommonConfig.szCommPortParam,
							sizeof(stCommonConfig.szCommPortParam),
							"%d",
							CmdInfo.setinfo.value.ulValue);
					}
					else if(TL1_CFG_SIG_SIGID_KEEP_ALIVE == CmdInfo.setinfo.SigID)
					{
						iModifyType |= TL1_CFG_PORT_KEEP_ALIVE;
						stCommonConfig.iPortKeepAlive = CmdInfo.setinfo.value.enumValue;
					}
					else if(TL1_CFG_SIG_SIGID_SESSION_TIMEOUT == CmdInfo.setinfo.SigID)
					{
						iModifyType |= TL1_CFG_SESSION_TIMEOUT;
						stCommonConfig.iSessionTimeout = CmdInfo.setinfo.value.ulValue;
					}
					else
					{
						iModifyType = 0;
					}

					if( ProcCmdSetOrGetComm(PROTOCOL_TL1, (void *)&stCommonConfig, iModifyType) )
					{
						iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
							CmdInfo.setinfo.EquipID,
							DXI_MERGE_SIG_ID(CmdInfo.setinfo.SigType, CmdInfo.setinfo.SigID),
							sizeof(VAR_VALUE_EX),
							&sigValue,
							iTimeout);
					}
					else
					{
						iErr = ERR_EQP_COMM_BUSY;
					}
				}
				else
				{
					iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						CmdInfo.setinfo.EquipID,
						DXI_MERGE_SIG_ID(CmdInfo.setinfo.SigType, CmdInfo.setinfo.SigID),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						iTimeout);
				}
			}

			if(iErr != ERR_EQP_OK)
			{
				*pWrite = FALSE;
			}
			else
			{
				*pWrite = TRUE;
				//changed by Frank Wu,30/30,20140217, for upgrading software of rectifier
				if( (CmdInfo.setinfo.EquipID == EQIPID_RECTGRP)&& \
					((CmdInfo.setinfo.SigID == RT_HVDC_UPDATE) || \
					(CmdInfo.setinfo.SigID == RT_HVDC_FORCEUPDATE))&& \
					(1 == CmdInfo.setinfo.value.enumValue))
				{
					Sleep(30000);//wait for CAN module process
					while(1)
					{
						if(QueryRectUpdateResult(pWrite))
						{
							break;
						}
						Sleep(2000);//yield
					}
				}
			}
		}
		else if(CmdInfo.CmdType == CMD_CTRL)
		{
			int*	pWrite;
			pWrite = (int*)p;

			memset(&sigValue, 0, sizeof(sigValue));
			sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;

			sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
			sigValue.pszSenderName = "LUI";
			//printf("cmdType: %d;\n", CmdInfo.setinfo.SigID);
			//changed by Frank Wu,20131211,21/26 for twinkle of the green led of rectifier
			//if(CmdInfo.setinfo.SigID == CTRLLED_RECT)
			{
				iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						EQIPID_RECTGRP,
						DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, CTRL_GRP_LED),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);	
			}
			//changed by Frank Wu,20131211,22/26 for twinkle of the green led of rectifier
			//else if(CmdInfo.setinfo.SigID == CTRLLED_MPPT)
			{
				iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						EQIPID_MPPTGRP,
						DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, CTRL_GRP_LED),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);
			}
			//changed by Frank Wu,20131211,23/26 for twinkle of the green led of rectifier
			//else if(CmdInfo.setinfo.SigID == CTRLLED_CONV)
			{
				iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						EQIPID_CONVGRP,
						DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, CTRL_GRP_LED),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);
			}
			//changed by Frank Wu,20131211,24/26 for twinkle of the green led of rectifier
			//else if(CmdInfo.setinfo.SigID == CTRLLED_S1RECT)
			{
				iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						EQIPID_SLAVERECTGRP1,
						DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, CTRL_GRP_LED),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);	
			}
			//changed by Frank Wu,20131211,25/26 for twinkle of the green led of rectifier
			//else if(CmdInfo.setinfo.SigID == CTRLLED_S2RECT)
			{
				iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						EQIPID_SLAVERECTGRP2,
						DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, CTRL_GRP_LED),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);	
			}
			//changed by Frank Wu,20131211,26/26 for twinkle of the green led of rectifier
			//else if(CmdInfo.setinfo.SigID == CTRLLED_S3RECT)
			{
				iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						EQIPID_SLAVERECTGRP3,
						DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, CTRL_GRP_LED),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);	
			}
			

			if((CmdInfo.setinfo.SigID >= CTRLLED_RECT)
				&& (CmdInfo.setinfo.SigID <= CTRLLED_SMDUH)
				&& (CmdInfo.setinfo.value.enumValue == TRUE))
			{
				sigValue.varValue.enumValue = TRUE;
				iErr = DxiSetData(VAR_A_SIGNAL_VALUE,
						CmdInfo.setinfo.EquipID,
						DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, CTRL_MODULE_LED),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);
			}
		}
		else if(CmdInfo.CmdType == CMD_REBOOT)
		{
            //printf("===============come on reboot\n");
            //printf("QT_fifo CMD_REBOOT");//hhy
            //if need to change the password ,change the password first
            //if ()
			//printf("QT Reboot!!!\n");
			AppLogOut("QT_FIFO", APP_LOG_INFO, "%s", "QT reboot!\n");//changed by Frank Wu,20131228,1/1
			_SYSTEM("reboot");
        }else if(CmdInfo.CmdType == CMD_RESETPASSWORD) {

            char* userName = "admin";
            char* password = "640275";
            //char userName[4]

            char* szTrimContactAddr = "";
            int iNewAuthority = -1;//¸ÄÃÜÂë,ÓÊÏä
            int iReturnValue = ModifyUserInfo2(userName, password, iNewAuthority, szTrimContactAddr);
            //printf("iReturnValue<%d>",iReturnValue);//hhy
            //printf("============userName<%s>",userName);
            //printf("===============come on CT_RESETPASSWORD\n");
            //snprintf(szCmdChangePass, sizeof(szCmdChangePass), "echo %s > /home/app_script/pass_change_flag.log","yes");

            sleep(1);
            _SYSTEM("echo yes > /home/app_script/pass_change_flag.log");
            _SYSTEM("reboot");

        }


		munmap(p, DATA_LEN);
		iErr = close(CmdInfo.iMapHandle);
		//printf("Cmd Close OK!!! St=%d\n", iErr);
		sem_post(psemAck);

		if (*iQuitCommand == SERVICE_STOP_RUNNING)
		{
			bContinueRunning = FALSE;
		}
		QT_AppService->bReadyforNextService = TRUE;
		Sleep(200);//yield

		//iGetSlaveT++;
	}

	
	return FALSE;
}

#if 0
/*==========================================================================*
* FUNCTION :  Web_GetAuthorityByUser
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *pszUserInfo:
IN char *pszPasswordInfo:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:15
*==========================================================================*/
static int Web_GetAuthorityByUser(IN char *pszUserInfo,IN char *pszPasswordInfo)
{
	USER_INFO_STRU		*pUserInfo;
	int					iError;
	int					iAuthorLevel = 0;



	pUserInfo = NEW(USER_INFO_STRU,1);
	if(pUserInfo == NULL)
	{
		return FALSE;
	}

	//strncpyz(pUserInfo->szUserName , pszUserInfo,strlen(pUserInfo->szUserName));
	iError = FindUserInfo(pszUserInfo,pUserInfo);

	//TRACE("-----------[%s][%s]----[%s][%s]\n", pszUserInfo, pszPasswordInfo,pUserInfo->szUserName, pUserInfo->szPassword);
	if(iError == ERR_SEC_USER_NOT_EXISTED)
	{
		DELETE(pUserInfo);
		pUserInfo = NULL;
		return -1;
	}
	else
	{
		if(strcmp(pszPasswordInfo,pUserInfo->szPassword) != 0)
		{
			DELETE(pUserInfo);
			pUserInfo = NULL;
			return -2;
		}
		else
		{
			iAuthorLevel = pUserInfo->byLevel;
			DELETE(pUserInfo);
			pUserInfo = NULL;
			return iAuthorLevel;
		}
	}
}

/*==========================================================================*
* FUNCTION :  Web_ModifySignalAlarmLevel
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int equipID:
IN int signalType:
IN int signalID:
IN int alarmLevel:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
*==========================================================================*/

static int Web_ModifySignalAlarmLevel(IN int iEquipID,
									  IN int iSignalType,
									  IN int iSignalID,
									  IN int iAlarmLevel,
									  IN char *szWriteUserName)
{
	SET_A_SIGNAL_INFO_STU		stSetASignalInfo;

	int iError	= 0;
	int iBufLen = 0;
	int iVarSubID	= DXI_MERGE_SIG_ID(iSignalType,iSignalID);


	strncpyz(stSetASignalInfo.cModifyUser, 
		szWriteUserName,
		strlen(szWriteUserName) + 1);

	stSetASignalInfo.byModifyType		= MODIFY_ALARM_LEVEL;
	stSetASignalInfo.bModifyAlarmLevel	= iAlarmLevel;

	/*iError += DxiSetData(VAR_A_SIGNAL_INFO_STRU,
	iEquipID,			
	iVarSubID,		
	iBufLen,			
	(void *)&stSetASignalInfo,			
	iTimeOut);*/
	iBufLen = sizeof(SET_A_SIGNAL_INFO_STU);




	iError += DxiSetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		(void *)&stSetASignalInfo,			
		0);


	if (iError == ERR_DXI_OK )
	{
		return TRUE;
	}
	else
	{
		AppLogOut(XML_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Modify signal alarm level fail");
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return 1;
		}
		else
		{
			return FALSE;
		}
	}

}
/*==========================================================================*
* FUNCTION :  Web_GetEquipInfoByTypeId
* PURPOSE  :  Get A EQUIP_INFO struct point used StdEquipID
* CALLS    : 
* CALLED BY:  Web_MakeAlarmSupExpWebPageBuf 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
//¸ù¾Ý±ê×¼Éè±¸ÀàÐÍID»ñÈ¡ÆäÖÐÒ»¸öÉè±¸½á¹¹ÌåÖ¸Õë
static int Web_GetEquipInfoByTypeId(IN int iEquipTypeId, OUT EQUIP_INFO **ppszEquipInfo)
{

	EQUIP_INFO *pEquipInfo = NULL;
	EQUIP_INFO *pTempEquipInfo = NULL;
	int iEquipNum;
	int iBufLen;
	int i;

	/*get equip information*/
	int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	/*get equip number*/
	int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
		0,			
		0,		
		&iBufLen,			
		(void *)&iEquipNum,			
		0);

	if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
	{

		return FALSE;
	}

	pTempEquipInfo = pEquipInfo;
	for(i = 0; i< iEquipNum; i++, pTempEquipInfo++)
	{
		if(pTempEquipInfo->iEquipTypeID == iEquipTypeId)
		{
			break;
		}
	}

	*ppszEquipInfo = pTempEquipInfo;

	return TRUE;

}
/*==========================================================================*
* FUNCTION :  Web_ModifyXMLAlarmRelay
* PURPOSE  :  
* CALLS    : 
* CALLED BY:
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_ModifyXMLAlarmRelay(IN int iEquipTypeID,IN int iAlarmSigID, IN int iAlarmRelay, IN char *szUserName)
{


	int iStdEquipTypeID;

	//char stdEquipTypeID[5];
	//char alarmSigID[4];
	//char *pszAlarmRelay = NULL;

	int iError = 0;


	EQUIP_INFO *pszEquipInfo = NULL;

	iStdEquipTypeID = iEquipTypeID;
	Web_GetEquipInfoByTypeId(iStdEquipTypeID, &pszEquipInfo);

	SET_A_SIGNAL_INFO_STU       stSetASignalInfo;


	int iBufLen = 0;
	int iVarSubID = DXI_MERGE_SIG_ID(3,iAlarmSigID);


	strncpyz(stSetASignalInfo.cModifyUser,szUserName,strlen(szUserName)+1);

	stSetASignalInfo.byModifyType      = MODIFY_ALARM_RELAY;
	stSetASignalInfo.bModifyAlarmRelay = iAlarmRelay;


	iBufLen = sizeof(SET_A_SIGNAL_INFO_STU);


	iError += DxiSetData(VAR_A_SIGNAL_INFO_STRU,
		pszEquipInfo->iEquipID,         
		iVarSubID,    
		iBufLen,          
		(void *)&stSetASignalInfo,         
		0);

	if (iError == ERR_DXI_OK )
	{
		return 1;
	}
	else
	{

		return 0;

	}



}





static int Web_ModifySignalValue_new(IN int iEquipID,
									 IN int iSignalType,
									 IN int iSignalID,
									 IN VAR_VALUE_EX value)
{
	int        iError = -1;
	int        iBufLen = sizeof(VAR_VALUE_EX);
	int        iVarSubID;


	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	//printf("iError = %d\n", iError);
	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,         
		iVarSubID,    
		iBufLen,          
		&value,           
		0);

	//printf("iError = %d\n", iError);
	if (iError == ERR_DXI_OK )
	{
		return TRUE;
	}
	else
	{
		if(iError == ERR_EQP_CTRL_SUPPRESSED)
		{
			return XML_SIGNALVALUE_FAIL;
		}
		else if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return 1;
		}
		else
		{
			return FALSE;
		}
	}

}
#endif

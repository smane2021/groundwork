/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : snmp_public.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:55
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __SNMP_PUBLIC_H__041008
#define __SNMP_PUBLIC_H__041008

#ifdef  __cplusplus
extern "C" {
#endif

#include "stdsys.h"  
#include "public.h"  

#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>

//#define GET_NMS_INFO_FROM_CONFIG_FILE
#define MAX_SNMP_OID_LENTH	8

	struct tagSNMP_OID
	{
		int iLenth;					//Oid real Lengh
		u_long _oids[MAX_SNMP_OID_LENTH];	
	};
	typedef struct tagSNMP_OID _OID;

	/* define log tasks */
#define SNMP_UI_TASK    "SNMP Agent"

#define DELETE_ITEM(p) if ((p) != NULL) \
	{ DELETE(p);  (p) = NULL; }

	//the map of MIB Table definition
	struct tagSNMP_TableItem
	{
		int		nTableID;
		int		iIndexMethod;	//1--'x',EquipID; 2--'y'Signal Index, 3---'z'dynamic
		_OID	oid_index;		//Corresponding Index Oid
		int		nEquipTypeNums;	//the Num of EquipTypeList
		int*	pEquipTypeIDList;	
	};
	typedef struct tagSNMP_TableItem SNMP_TABLE_ITEM_INFO;
	//the map of MIB signals 


	struct tagSNMP_SigItem
	{
		_OID	oid_this;	//Max Oid Length is 8
		SNMP_TABLE_ITEM_INFO*	pTableInfo;
		int		nStdEquipID;;	//Corresponding Equip ID
		BOOL	bMultiple;		//If is Multiple.
		int	nEquipIndexNum;	//Using Index, the total
		int*	piIndexList;	//Index list,e,g 1,2,3,4,5,6
		int		nSigType;		//Corresponding Signal Type
		int		nSigID;			//Corresponding Signal ID
		int		nValueType;		//Value Type: I/F/SN/EN/SP.....
		int		nAccessAttr;	//Access: W/R
	};
	typedef struct tagSNMP_SigItem SNMP_SIG_ITEM_INFO;

	//active items List
	struct tagActive_Item
	{
		BOOL	bIsValid;					//The Item is valid or not
		int		iColIdx;					// = the oid last -1 num
		long	iRowIdx;					// = the oid last num		
		int		nEquipID;					//for DXI Get Equip ID

		SNMP_SIG_ITEM_INFO* pSNMPSigInfo;		//The Corresponding Item Info
	};
	typedef struct tagActive_Item SNMP_ACT_ITEM;
	#define  MAX_SNMP_TABLE_COLUMN	20 //max column
	#define MAX_SNMP_TABLE_ROWS		500
	struct tagActive_Table
	{
		SNMP_TABLE_ITEM_INFO*	pTableInfo;		//Related Table Info
		int		nColumnNum;						//Total Columns
		int		nRowsNum;						//Total Rows
		int		nGetTimes;						//GET times
		SNMP_ACT_ITEM pActItems[MAX_SNMP_TABLE_COLUMN*MAX_SNMP_TABLE_ROWS];	//Active item list. It is fixed
		ULONG		iActRowList[MAX_SNMP_TABLE_ROWS];//the row ID list, used for GETNET cmd
	};
	typedef struct tagActive_Table SNMP_ACT_TABLE;

#define MAX_PRICFG_STR_LEN	128
	struct tagSNMP_PriCfgInfo
	{
		//char	szSNMPpriCfgName[MAX_PRICFG_STR_LEN];
		char	szSNMPpriCfgVer[MAX_PRICFG_STR_LEN];
		//char	szSNMPpriCfgChangeDate[MAX_PRICFG_STR_LEN];
		//char	szSNMPpriCfgEditor[MAX_PRICFG_STR_LEN];
		//char	szSNMPpriCfgChangeDescrib[MAX_PRICFG_STR_LEN];
	};
	typedef struct tagSNMP_PriCfgInfo SNMP_PRICFG_INFO;

#define MAX_PENDING_TRAP_SEND 64 

//All the info used in snmp agent

struct SSnmpInfoStruct
{
	u_long				nSnmpPriCfgNum;
	SNMP_PRICFG_INFO* pSnmpPriCfgInfos;

	u_long				nSnmpSigNum;		//SNMP Sig num
	SNMP_SIG_ITEM_INFO*	pSnmpSigInfos;		//SNMP Sig Infos

	u_long				nSnmpTableNum;		//SNMP Table num
	SNMP_TABLE_ITEM_INFO* pSnmpTableInfos;	//Table Infos

	SNMP_ACT_ITEM*		pTobeSetActItem;	//For GET Command

	u_long			nNodNum;	//Nod total

	u_long			nNmsNum;		//NMS info num
	NMS_INFO*		pNmsInfos;		//NMS info array

	HANDLE			hThreadThis; //SNMP main thread handle

	// max TRAP numbers in trap queue, see MAX_PENDING_TRAP_SEND
	HANDLE			hTrapSendQueue;	  /* trap send queue*/

	//the Trap send thread handle
	HANDLE			hTrapSendThread;

#define MAX_TIME_WAITING_NMS_INFO_LOCK		10000
	HANDLE			hNmsInfoSyncLock;	/* the synchronization lock */

	ULONG			ulAlarmSequenceID;

};
typedef struct SSnmpInfoStruct SNMP_INFO_STRUCT;

//The extend alarm signal value
//Include the active or deactive status of alarm
struct tagAlarmSigValueExt
{
	int				nAlarmMsgType;
	ALARM_SIG_VALUE	stAlarmSigValue;

	ULONG				ulAlarmSequenceID;
};
typedef struct tagAlarmSigValueExt ALARM_SIG_VALUE_EXT;

//The status of alarm
enum STATUS_CHANGE_ENUM
{
	ALARM_STATUS_INVALIDATE = 0,
	ALARM_STATUS_ACTIVATED,
	ALARM_STATUS_DEACTIVATED,
};

enum SPECIAL_SNMP_SIG_VALUE_TYPE
{
	//'I'/'F'
	SNMP_VT_INT,
	SNMP_VT_FLOAT,
	//Index of a table
	SNMP_VT_INDEX,
	//'SE'/'SN'/'EN'.....
	SNMP_VT_EQUIP_NAME,
	SNMP_VT_SIGNAL_NAME,
	SNMP_VT_PRDCT_NUM,
	SNMP_VT_HW_VER,
	SNMP_VT_SW_VER,
	SNMP_VT_SERIAL_NUM,
	//'SP'
	SNMP_VT_MANIFACTURER,
	SNMP_VT_MODEL,
	SNMP_VT_FIRMWARE,
	SNMP_VT_SITENAME,
	SNMP_VT_ALARMLASTNO,
	SNMP_VT_ALARMTIME,
	SNMP_VT_ALARMSTCHANGE,
	SNMP_VT_ALARMLEVEL,
	SNMP_VT_ALARMDESCRB,
	SNMP_VT_ALARMTYPE,
	SNMP_VT_SNMP_CFGVER,
	SNMP_VT_MAX
};
//Used to conversion of actual alarm level to trap send severity
enum ALARM_SEVERITY_SEND_TO_NMS_ENUM
{
	INVALID_ALARM_SEVERITY = 0,
	OBSERVATION_ALARM_SEVERITY = 3,
	MAJOR_ALARM_SEVERITY = 5,
	CRITICAL_ALARM_SEVERITY = 6,
};

int GetTimeString(time_t tm, char* string);
int GetAlarmSeverity(int nAlarmLevel);

int GetAlarmDescription(ALARM_SIG_VALUE* pAlarmSigValue, char* string);

//Get the snmp info reference
SNMP_INFO_STRUCT* GetSnmpInfoRef(void);


#ifdef __cplusplus
}
#endif

#endif //__SNMP_PUBLIC_H__041008

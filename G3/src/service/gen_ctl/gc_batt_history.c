/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_batt_history.c
 *  CREATOR  : Frank Cao                DATE: 2004-10-13 17:32
 *  VERSION  : V1.00
 *  PURPOSE  : battery log interface functions
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#include	"gc_batt_history.h"
#include	"gen_ctl.h"
#include	"gc_index_def.h"
#include	"gc_sig_value.h"
#include	"gc_batt_mgmt.h"


/*==========================================================================*
 * FUNCTION : GC_BattHisInit
 * PURPOSE  : To initialize the state and chage time of Battery History state
 * CALLS    : time
 * CALLED BY: GC_BattMgmtInit
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-15 10:41
 *==========================================================================*/
void	GC_BattHisInit()
{
	int	i;
	//Initialize the Battrey Discharge State
	g_pGcData->RunInfo.Bt.DischState.byState = DISCH_STATE_NORMAL;
	g_pGcData->RunInfo.Bt.DischState.tChangeTime 
		= g_pGcData->SecTimer.tTimeNow;

	//Initialize the Battrey Temperatrue State
	for(i = 0; i < g_pGcData->EquipInfo.iCfgQtyOfBatt; i++)
	{
		g_pGcData->RunInfo.Bt.aTempState[i].byState = DISCH_STATE_NORMAL;
		g_pGcData->RunInfo.Bt.aTempState[i].tChangeTime 
			= g_pGcData->SecTimer.tTimeNow;
	}
	return;
}

/*==========================================================================*
 * FUNCTION : GC_BattHisTimeProof
 * PURPOSE  : To proof the battery history time with lDetaTm
 * CALLS    : 
 * CALLED BY: GC_MaintainSecTimers
 * ARGUMENTS: long  lDeltaTm : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-15 10:41
 *==========================================================================*/
void GC_BattHisTimeProof(long lDeltaTm)
{
	int	i;
	//Proof the Battrey Discharge State Change Time
	g_pGcData->RunInfo.Bt.DischState.tChangeTime += lDeltaTm;

	//Proof the Battrey Temperatrue State Change Time
	for(i = 0; i < g_pGcData->EquipInfo.iCfgQtyOfBatt; i++)
	{
		g_pGcData->RunInfo.Bt.aTempState[i].tChangeTime += lDeltaTm;
	}
	return;
}



/*==========================================================================*
 * FUNCTION : GC_BattHisRefresh
 * PURPOSE  : According to DC Voltage and Battery Temperature to refresh
              Battery History Parameter.
 * CALLS    : GC_GetFloatValue GC_GetDwordValue GC_SetDwordValue
 * CALLED BY: GC_BattMgmt
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-15 10:46
 *==========================================================================*/
void	GC_BattHisRefresh()
{
	int		i;
	BOOL	bSave;
	DWORD	dwTime;

	bSave = FALSE;

	struct tm		tmNow; 
	static int		s_iLastHour = -1;

	float fShallowVlot;
	float fMiddleVolt;
	float fDeepVolt;

	SIG_ENUM stVoltLevelState = 0;
	stVoltLevelState  = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	//if(35.0 < g_pGcData->RunInfo.Rt.fRatedVolt)
	if(0 == stVoltLevelState)
	{
		/* 48V system's value */
		fShallowVlot = 50.0;
		fMiddleVolt = 46.8;
		fDeepVolt = 42.0;
	}
	else 
	{
		/* 24V system's value */
		fShallowVlot =  25.0;
		fMiddleVolt = 23.4;
		fDeepVolt = 21.0;
	}

	gmtime_r(&(g_pGcData->SecTimer.tTimeNow), &tmNow);

	switch(g_pGcData->RunInfo.Bt.DischState.byState)
	{
	case 	DISCH_STATE_NORMAL:
		if(g_pGcData->RunInfo.fSysVolt < fShallowVlot)
		{
			g_pGcData->RunInfo.Bt.dwShallowTimes++;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_SHALLOW_TIMES, 
							g_pGcData->RunInfo.Bt.dwShallowTimes);
			bSave = TRUE;

			g_pGcData->RunInfo.Bt.DischState.byState = DISCH_STATE_SHALLOW;
			g_pGcData->RunInfo.Bt.DischState.tChangeTime 
				= g_pGcData->SecTimer.tTimeNow;

		}
		break;

	case DISCH_STATE_SHALLOW:
		if(g_pGcData->RunInfo.fSysVolt < fMiddleVolt)
		{
			g_pGcData->RunInfo.Bt.dwMidTimes++;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_MID_TIMES, 
							g_pGcData->RunInfo.Bt.dwMidTimes);

			g_pGcData->RunInfo.Bt.dwShallowTime 
				+= g_pGcData->SecTimer.tTimeNow 
					- g_pGcData->RunInfo.Bt.DischState.tChangeTime;

			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_SHALLOW_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.dwShallowTime 
									/ SECONDS_PER_MIN));
			bSave = TRUE;

			g_pGcData->RunInfo.Bt.DischState.byState 
				= DISCH_STATE_MIDDLE;
			g_pGcData->RunInfo.Bt.DischState.tChangeTime 
				= g_pGcData->SecTimer.tTimeNow;

			break;
		}

		if(g_pGcData->RunInfo.fSysVolt > fShallowVlot)
		{
			g_pGcData->RunInfo.Bt.dwShallowTime 
				+= g_pGcData->SecTimer.tTimeNow 
					- g_pGcData->RunInfo.Bt.DischState.tChangeTime;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_SHALLOW_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.dwShallowTime 
									/ SECONDS_PER_MIN));

			bSave = TRUE;

			g_pGcData->RunInfo.Bt.DischState.byState = DISCH_STATE_NORMAL;
			g_pGcData->RunInfo.Bt.DischState.tChangeTime 
				= g_pGcData->SecTimer.tTimeNow;

			break;
		}
		
		dwTime = g_pGcData->RunInfo.Bt.dwShallowTime 
				+ g_pGcData->SecTimer.tTimeNow 
				- g_pGcData->RunInfo.Bt.DischState.tChangeTime;

		GC_SetDwordValue(TYPE_OTHER, 
						0, 
						BT_PUB_SHALLOW_TIME, 
						(DWORD)(dwTime / SECONDS_PER_MIN));

		if(s_iLastHour != tmNow.tm_hour)
		{
			g_pGcData->RunInfo.Bt.dwShallowTime 
				+= g_pGcData->SecTimer.tTimeNow 
					- g_pGcData->RunInfo.Bt.DischState.tChangeTime;
			GC_SetDwordValue(TYPE_OTHER, 
					0, 
					BT_PUB_SHALLOW_TIME, 
					(DWORD)(g_pGcData->RunInfo.Bt.dwShallowTime 
						/ SECONDS_PER_MIN));

			g_pGcData->RunInfo.Bt.DischState.tChangeTime
				= g_pGcData->SecTimer.tTimeNow;

			bSave = TRUE;		
		}

		break;

	case DISCH_STATE_MIDDLE:
		if(g_pGcData->RunInfo.fSysVolt < fDeepVolt)
		{
			g_pGcData->RunInfo.Bt.dwDeepTimes++; 
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_DEEP_TIMES, 
							g_pGcData->RunInfo.Bt.dwDeepTimes);

			g_pGcData->RunInfo.Bt.dwMidTime 
				+= g_pGcData->SecTimer.tTimeNow 
					- g_pGcData->RunInfo.Bt.DischState.tChangeTime;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_MID_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.dwMidTime 
									/ SECONDS_PER_MIN));
			bSave = TRUE;

			g_pGcData->RunInfo.Bt.DischState.byState 
				= DISCH_STATE_DEEP;
			g_pGcData->RunInfo.Bt.DischState.tChangeTime 
				= g_pGcData->SecTimer.tTimeNow;

			break;
		}

		if(g_pGcData->RunInfo.fSysVolt > fMiddleVolt)
		{
			g_pGcData->RunInfo.Bt.dwMidTime 
				+= g_pGcData->SecTimer.tTimeNow 
					- g_pGcData->RunInfo.Bt.DischState.tChangeTime;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_MID_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.dwMidTime 
									/ SECONDS_PER_MIN));
			bSave = TRUE;

			g_pGcData->RunInfo.Bt.DischState.byState
				= DISCH_STATE_SHALLOW;
			g_pGcData->RunInfo.Bt.DischState.tChangeTime 
				= g_pGcData->SecTimer.tTimeNow;

			break;
		}

		dwTime = g_pGcData->RunInfo.Bt.dwMidTime 
				+ g_pGcData->SecTimer.tTimeNow 
				- g_pGcData->RunInfo.Bt.DischState.tChangeTime;

		GC_SetDwordValue(TYPE_OTHER, 
						0, 
						BT_PUB_MID_TIME, 
						(DWORD)(dwTime / SECONDS_PER_MIN));

		if(s_iLastHour != tmNow.tm_hour)
		{
			g_pGcData->RunInfo.Bt.dwMidTime 
				+= g_pGcData->SecTimer.tTimeNow 
					- g_pGcData->RunInfo.Bt.DischState.tChangeTime;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_MID_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.dwMidTime 
									/ SECONDS_PER_MIN));

			g_pGcData->RunInfo.Bt.DischState.tChangeTime
				= g_pGcData->SecTimer.tTimeNow;

			bSave = TRUE;	
		}

		break;
	
	case DISCH_STATE_DEEP:
		if(g_pGcData->RunInfo.fSysVolt > fDeepVolt)
		{
			g_pGcData->RunInfo.Bt.dwDeepTime 
				+= g_pGcData->SecTimer.tTimeNow 
					- g_pGcData->RunInfo.Bt.DischState.tChangeTime;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_DEEP_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.dwDeepTime 
									/ SECONDS_PER_MIN));
			bSave = TRUE;

			g_pGcData->RunInfo.Bt.DischState.byState 
				= DISCH_STATE_MIDDLE;
			g_pGcData->RunInfo.Bt.DischState.tChangeTime 
				= g_pGcData->SecTimer.tTimeNow;
			break;
		}

		dwTime = g_pGcData->RunInfo.Bt.dwDeepTime 
				+ g_pGcData->SecTimer.tTimeNow 
				- g_pGcData->RunInfo.Bt.DischState.tChangeTime;

		GC_SetDwordValue(TYPE_OTHER, 
						0, 
						BT_PUB_DEEP_TIME, 
						(DWORD)(dwTime / SECONDS_PER_MIN));

		if(s_iLastHour != tmNow.tm_hour)
		{
			g_pGcData->RunInfo.Bt.dwDeepTime 
				+= g_pGcData->SecTimer.tTimeNow 
					- g_pGcData->RunInfo.Bt.DischState.tChangeTime;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_DEEP_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.dwDeepTime 
									/ SECONDS_PER_MIN));

			g_pGcData->RunInfo.Bt.DischState.tChangeTime
				= g_pGcData->SecTimer.tTimeNow;
			
			bSave = TRUE;

		}

		break;
	default:
		break;
	}

	for(i = 0; i < g_pGcData->EquipInfo.iCfgQtyOfBatt; i++)
	{
		STATE_RECORD *pTempState = &(g_pGcData->RunInfo.Bt.aTempState[i]);
		if((GC_IsSigValid(TYPE_BATT_UNIT, i, BT_PRI_TEMP)) 
		    && ((g_pGcData->EquipInfo.BattTestLogInfo.aExistBattInfo[i].bTemp) 
		        || (g_pGcData->EquipInfo.aiEquipIdBtPri[i] >= 272 
			    && g_pGcData->EquipInfo.aiEquipIdBtPri[i] <= 291 )))
		{
			float	fTemp;
			BOOL bRTN;
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_TEMP,&fTemp);
			if (bRTN == FALSE)
			{
				continue;
			}
			switch(pTempState->byState)
			{
			case	BATT_TEMP_NORMAL:
				if(fTemp > BATT_HIGH_TEMP)
				{
					pTempState->byState = BATT_TEMP_HIGH;
					pTempState->tChangeTime = g_pGcData->SecTimer.tTimeNow;
					break;
				}

				if(fTemp < BATT_LOW_TEMP)
				{
					pTempState->byState = BATT_TEMP_LOW;
					pTempState->tChangeTime = g_pGcData->SecTimer.tTimeNow;
					break;
				}
				break;
			
			case	BATT_TEMP_HIGH:
				if(fTemp < BATT_HIGH_TEMP)
				{
					g_pGcData->RunInfo.Bt.adwHighTempTime[i]
						+= g_pGcData->SecTimer.tTimeNow 
							- pTempState->tChangeTime;
					GC_SetDwordValue(TYPE_BATT_UNIT, 
							i, 
							BT_PRI_HIGH_TEMP_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.adwHighTempTime[i]
									/ SECONDS_PER_MIN));
					bSave = TRUE;

					pTempState->byState	= BATT_TEMP_NORMAL;
					pTempState->tChangeTime = g_pGcData->SecTimer.tTimeNow;

					break;
				}

				dwTime = g_pGcData->RunInfo.Bt.adwHighTempTime[i]
						+ g_pGcData->SecTimer.tTimeNow 
						- pTempState->tChangeTime;
				GC_SetDwordValue(TYPE_BATT_UNIT, 
							i, 
							BT_PRI_HIGH_TEMP_TIME, 
							(DWORD)(dwTime / SECONDS_PER_MIN));

				if(s_iLastHour != tmNow.tm_hour)
				{
					g_pGcData->RunInfo.Bt.adwHighTempTime[i]
						+= g_pGcData->SecTimer.tTimeNow 
							- pTempState->tChangeTime;
					GC_SetDwordValue(TYPE_BATT_UNIT, 
							i, 
							BT_PRI_HIGH_TEMP_TIME, 
							(DWORD)(g_pGcData->RunInfo.Bt.adwHighTempTime[i]
									/ SECONDS_PER_MIN));

					pTempState->tChangeTime = g_pGcData->SecTimer.tTimeNow;

					bSave = TRUE;
				}

				break;
			
			case	BATT_TEMP_LOW:
				if(fTemp > BATT_LOW_TEMP)
				{
					g_pGcData->RunInfo.Bt.adwLowTempTime[i]
						+= g_pGcData->SecTimer.tTimeNow 
							- pTempState->tChangeTime;
					GC_SetDwordValue(TYPE_BATT_UNIT, 
							i, 
							BT_PRI_LOW_TEMP_TIME,
							(DWORD)(g_pGcData->RunInfo.Bt.adwLowTempTime[i] 
									/ SECONDS_PER_MIN));
					bSave = TRUE;

					pTempState->byState = BATT_TEMP_NORMAL;
					pTempState->tChangeTime = g_pGcData->SecTimer.tTimeNow;

					break;
				}

				dwTime = g_pGcData->RunInfo.Bt.adwLowTempTime[i]
						+ g_pGcData->SecTimer.tTimeNow 
						- pTempState->tChangeTime;
				GC_SetDwordValue(TYPE_BATT_UNIT, 
							i, 
							BT_PRI_LOW_TEMP_TIME, 
							(DWORD)(dwTime / SECONDS_PER_MIN));

				if(s_iLastHour != tmNow.tm_hour)
				{
					g_pGcData->RunInfo.Bt.adwLowTempTime[i]
						+= g_pGcData->SecTimer.tTimeNow 
							- pTempState->tChangeTime;
					GC_SetDwordValue(TYPE_BATT_UNIT, 
							i, 
							BT_PRI_LOW_TEMP_TIME,
							(DWORD)(g_pGcData->RunInfo.Bt.adwLowTempTime[i] 
									/ SECONDS_PER_MIN));
	
					pTempState->tChangeTime = g_pGcData->SecTimer.tTimeNow;

					bSave = TRUE;
				}

				break;
			default:
				break;
			}
		}
	}

	if(bSave)
	{
		GC_SaveData();
	}

	s_iLastHour = tmNow.tm_hour;
	return;
}

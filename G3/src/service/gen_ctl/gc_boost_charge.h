/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_boost_charge.h
 *  CREATOR  : Frank Cao                DATE: 2004-10-29 10:09
 *  VERSION  : V1.00
 *  PURPOSE  : To provide Boost Charge State handling interface functions
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_BOOST_CHARGE_H_
#define _GC_BOOST_CHARGE_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"



//Changed by Yangguoxin,2012-3-31
#define	GC_BC_PROTECT_NUM	1//3 

void GC_BoostChargeStart(int iNextState, 
							 const char* szLogOut);
void GC_BoostChargeEnd(int iNextState,
						   BOOL bIsPerfectBC,
						   const char* szLogOut);
void GC_CycPreBC(void);
void GC_ManAutoBC(SIG_ENUM BmState);
void GC_BcProtect(void);
BOOL GC_NeedAutoBC(void);
BOOL GC_CmpCapAndFcToBcCap(void);
int	GC_GetIntervalCycBcDuration(int iIdx);
int	GC_GetIntervalCycBcInterval(int iIdx);
int	GC_GetBcToFcDelay(int iIdx);
int GC_GetBcProtectTime(int iIdx);
BOOL CompareBattCurrAndBcToFcCurr(void);
#endif //_GC_BOOST_CHARGE_H_

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU Plus Rack_mounted
 *
 *  FILENAME : gc_run_info.c
 *  CREATOR  : Frank Cao                DATE: 2006-05-09 16:00
 *  VERSION  : V1.00
 *  PURPOSE  : to provide run infomation handling interface
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_run_info.h"
#include	"gen_ctl.h"
#include	"gc_index_def.h"
#include	"gc_rect_list.h"
#include	"pub_list.h"
#include	"gc_sig_value.h"
#include	"gc_batt_mgmt.h"
#include	"gc_discharge.h"
#include	"gc_engy_sav.h"
#include	"gc_lvd.h"
#include	"batt_test_log.h"
#include	"gc_init_equip.h"

#include	<math.h>

//#define		GC_RUN_INFO_DEBUG  


static void		TurnManualToAuto(void);
static BOOL		IsDslValid(void);
static BOOL		GetDslState(void);
static BOOL		IsTotalPowerValid(void);
static BOOL		CalcRectTime();
//static float 	CalcWorkOnRectTotalRatedCurr();
static void AcRunInfo(void);
static void	ConvRunInfo(void);
static BOOL SetMaxCurrentLimit(IN int iQtyOfRect, IN float fRatedCurr);
static void SetMaxConvCurrentLimit(IN int iQtyOfRect);
static void	MpptRunInfo(void);
static float GC_GetFiammBatteryCurr();


#define  CONV_TYPE_24_48V		0
#define  CONV_TYPE_48_24V		1
#define  CONV_TYPE_400_48V		2
/*==========================================================================*
 * FUNCTION : IsMultiRectFail
 * PURPOSE  : Judge if multi-rectifiers fail
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-01-03 15:37
 *==========================================================================*/
 BOOL IsMultiRectFail(void)
{
	int	i;
	int	iNumOfFailRect = 0;

	/*if(g_pGcData->RunInfo.Rt.bAcFail)
	{
		return FALSE;
	}*/
	
	for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect; i++)
	{
		if(((GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_FAIL))
			&& (!g_pGcData->RunInfo.Rt.bMasterAcFail)) //only when not AC Failure
			|| ((GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_PROTCTION))
			&& (!g_pGcData->RunInfo.Rt.bMasterAcFail)) //only when not AC Failure
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_OVER_TEMP))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_FAULT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_OVER_VOLT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_FAN_FAULT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_NO_RESPOND)))
		{
			iNumOfFailRect++;
			if(iNumOfFailRect > 1)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}
//BOOL Is_IB_ComFail()
//{
//#define SIGID_IB_COMFAIL	96
//	static s_bFirstRun=TRUE;
//	static SIG_BASIC_VALUE *pSigIBComFail = NULL;
//		
//	if(s_bFirstRun)
//	{
//		//pSigIBComFail=(SIG_BASIC_VALUE *)Init_GetEquipSigRefById(1,SIG_TYPE_SAMPLING,SIGID_IB_COMFAIL);
//		s_bFirstRun=FALSE;
//	}
//	if(pSigIBComFail!=NULL)
//	{
//		return (pSigIBComFail->varValue.enumValue);	
//	}
//	return FALSE;
//	
//}
BOOL IsSMDU_EIB_Mode()
{
	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SMDU_IB_MODE))
	{
		return TRUE;
	}
	return FALSE;
}
BOOL IsNeed_Calc_BattCurr()
{
	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_NEED_CALC_BATT_CURR))//��Ҫ����ʱ�ż���
	{
		return TRUE;
	}
	return FALSE;
}
/*==========================================================================*
* FUNCTION : IsRectFail
* PURPOSE  : Judge if multi-rectifiers fail
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2005-01-03 15:37
*==========================================================================*/
BOOL IsRectFail(void)
{
	int	i;
	int	iNumOfFailRect = 0;

	/*if(g_pGcData->RunInfo.Rt.bAcFail)
	{
	return FALSE;
	}*/

	for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect; i++)
	{
		if(((GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_FAIL))
			&& (!g_pGcData->RunInfo.Rt.bMasterAcFail)) //only when not AC Failure
			|| ((GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_PROTCTION))
			&& (!g_pGcData->RunInfo.Rt.bMasterAcFail)) //only when not AC Failure
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_OVER_TEMP))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_FAULT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_OVER_VOLT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_FAN_FAULT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_NO_RESPOND)))
		{

				return TRUE;

		}
	}

	return FALSE;
}
/*==========================================================================*
* FUNCTION : IsMultiConvFail
* PURPOSE  : Judge if multi-Converters fail
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2005-01-03 15:37
*==========================================================================*/
static BOOL IsMultiConvFail(IN int iConvNum)
{
	int	i;
	int	iNumOfFailRect = 0;

	for(i = 0; i < iConvNum; i++)
	{
		if(	 (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_POWER_LIMIT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_OVER_TEMP))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_CONV_FAIL))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_EEROM_FAIL))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_FAN_FAIL))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_NO_RESPONSE))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_OVER_VOLTAGE))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_UNDER_VOLTAGE))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_OVER_VOLTAGE_24V))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_UNDER_VOLTAGE_24V))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_HVSD_ALM))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_AC_UNDERVOLT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_ID_OVERLAP))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_THERM_SD)))
		{
			iNumOfFailRect++;
			if(iNumOfFailRect > 1)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}
#define	RECT_LOST_DELAY_TIMES		30 //10
#define	RECT_LOST_STABLE_TIMES		12//4
/*==========================================================================*
 * FUNCTION : JudgeRectLost
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-09-11 16:54
 *==========================================================================*/
static void JudgeRectLost(void)
{
	static int	s_iDelay = 0;
	static int  s_iStableDelay = 0;

	int			iQty = GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_QTY_SELF);
	int			iCfgQty	= GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_CFG_RECT_NUM);

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif
	// when user clear rect comm fail alarm, sampler will let rectifiers reallocate, iCfgQty is set to 101 by config file
	// and then clear iCfgQty to zero, wait for reallocation complete
	if(iCfgQty == 161)
	{
		TRACE("iCfgQty=101, user maybe clear rect comm fail alarm. \n");
		s_iDelay = 0;
		iCfgQty = 0;
		GC_SetDwordValue(TYPE_OTHER,
			0,
			RT_PUB_CFG_RECT_NUM, 
			(DWORD)iCfgQty);
	}

	// wait for n cycles when Power on!
	if(s_iDelay < RECT_LOST_DELAY_TIMES)
	{
		s_iDelay++;
		return;
	}

	//Update RT_PUB_CFG_RECT_NUM
#ifdef GC_SUPPORT_MPPT
	if(stState)//MPPT enable
	{
		if(iQty > iCfgQty || iQty == 44)//Max number is 44 for MPPT mode.
		{
			GC_SetDwordValue(TYPE_OTHER,
				0,
				RT_PUB_CFG_RECT_NUM, 
				(DWORD)iQty);
			return;
		}
	}
	else
	{
		if(iQty > iCfgQty )
		{
			GC_SetDwordValue(TYPE_OTHER,
				0,
				RT_PUB_CFG_RECT_NUM, 
				(DWORD)iQty);
			return;
		}
	}

#else
	if(iQty > iCfgQty )
	{
		GC_SetDwordValue(TYPE_OTHER,
			0,
			RT_PUB_CFG_RECT_NUM, 
			(DWORD)iQty);
		return;
	}
#endif
	BOOL iGcAlarmStatus = iCfgQty > iQty ? GC_ALARM : GC_NORMAL;
	
	if (iGcAlarmStatus)
	{
		if (s_iStableDelay < RECT_LOST_STABLE_TIMES)
		{
			s_iStableDelay++;
			return;
		}
		else
		{
			//Continue  process!!
		}
	}
	else
	{
		s_iStableDelay = 0;
	}

	//Set alarm signal to ALARM or NORMAL
	GC_SetEnumValue(TYPE_OTHER,
		0,
		RT_PUB_CFG_RECT_LOST,
		iCfgQty > iQty ? GC_ALARM : GC_NORMAL,
		TRUE);

	return;	
}

void GC_SendCTCurrentLimit(IN float fCurrentLimit)
{
	float	fRatedCurr = GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_RATED_CURRENT);
	float	fCurr = 0.0;
	float	fRatedInput = GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_INTPUT_RATED_VOLT);

	int	iConvNumber = GC_GetDwordValue(TYPE_OTHER, 0, CT_PUB_QTY_SELF);
	//If there is no Converter, just return;
	if(!iConvNumber)
	{
		return;
	}

	fCurr = fCurrentLimit;
	
	if(fRatedInput > 200.0)
	{
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			CT_PUB_CTL_CURR,
			fCurr);
	}
	else
	{
		if(fRatedInput > 35.0)
		{
			if(fCurrentLimit > GC_MAX_CURR_LMT_CONV_24V)
			{
				fCurr = GC_MAX_CURR_LMT_CONV_24V;
			}
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_CTL_CURR_24V,
				fCurr);

		}
		else
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_CTL_CURR,
				fCurr);

		}
		
	}
	return;
}
#define	CONVERT_LOST_STABLE_TIMES		4
/*==========================================================================*
* FUNCTION : JudgeSMTempLost
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Jimmy Wu                DATE: 2011-11-25
*==========================================================================*/
#define	RECT_SMTEMP_DELAY_TIMES		30
static void JudgeSMTempLost(void)
{
	static int	s_iDelay = 0;

	//G3_OPT [load], by Lin.Tao.Thomas, 2013-4
	//check configred or not first
	if (!GC_IsGCSigExist(TYPE_OTHER, 0, GC_PUB_SMTEMP_ACTUAL_NUM) ||
		!GC_IsGCSigExist(TYPE_OTHER, 0, GC_PUB_CFG_SMTEMP_NUM))
	{
		return;
	}
	//G3_OPT end

	int			iQty = GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_SMTEMP_ACTUAL_NUM);
	int			iCfgQty	= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_CFG_SMTEMP_NUM);
	//printf("\n*******The current actual SM Temp Num is <%d>, and Cfg num is <%d>******\n",iQty,iCfgQty);
	// when user clear rect comm fail alarm, sampler will let rectifiers reallocate, iCfgQty is set to 101 by config file
	// and then clear iCfgQty to zero, wait for reallocation complete
	if(iCfgQty == 101)
	{
		//TRACE("iCfgQty=101, user maybe clear rect comm fail alarm. \n");
		s_iDelay = 0;
		iCfgQty = 0;
		GC_SetDwordValue(TYPE_OTHER,
			0,
			GC_PUB_CFG_SMTEMP_NUM, 
			(DWORD)iCfgQty);
	}

	// wait for n cycles
	if(s_iDelay < RECT_SMTEMP_DELAY_TIMES)
	{
		s_iDelay++;
		return;
	}

	//Update RT_PUB_CFG_RECT_NUM
	if(iQty > iCfgQty)
	{
		GC_SetDwordValue(TYPE_OTHER,
			0,
			GC_PUB_CFG_SMTEMP_NUM, 
			(DWORD)iQty);
		return;
	}

	//Set alarm signal to ALARM or NORMAL
	//GC_SetEnumValue(TYPE_OTHER,
	//	0,
	//	GC_PUB_CFG_SMTEMP_LOST,
	//	iCfgQty > iQty ? GC_ALARM : GC_NORMAL,
	//	TRUE);

	return;	
}

/*********************************************************************************
*  
*  FUNCTION NAME : JudgeDCEMLost
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-5 14:25:21
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
#define	DCEM_LOST_STABLE_TIMES		30
static void JudgeDCEMLost(void)
{
	static int	s_iDelay = 0;

	//G3_OPT [load], by Lin.Tao.Thomas, 2013-4
	//check configred or not first
	if (!GC_IsGCSigExist(TYPE_OTHER, 0, GC_PUB_DCEM_ACTUAL_NUM) ||
		!GC_IsGCSigExist(TYPE_OTHER, 0, GC_PUB_CFG_DCEM_NUM))
	{
		return;
	}
	//G3_OPT end

	int	iQty =	GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_DCEM_ACTUAL_NUM);
	int	iCfgQty	= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_CFG_DCEM_NUM);
	//printf("\n*******The current actual SM Temp Num is <%d>, and Cfg num is <%d>******\n",iQty,iCfgQty);
	// when user clear rect comm fail alarm, sampler will let rectifiers reallocate, iCfgQty is set to 101 by config file
	// and then clear iCfgQty to zero, wait for reallocation complete
	if(iCfgQty == 101)
	{
		//TRACE("iCfgQty=101, user maybe clear rect comm fail alarm. \n");
		s_iDelay = 0;
		iCfgQty = 0;
		GC_SetDwordValue(TYPE_OTHER,
			0,
			GC_PUB_CFG_DCEM_NUM, 
			(DWORD)iCfgQty);
	}

	
	if(s_iDelay < DCEM_LOST_STABLE_TIMES)
	{
		s_iDelay++;
		return;
	}

	if(iQty > iCfgQty)
	{
		GC_SetDwordValue(TYPE_OTHER,
			0,
			GC_PUB_CFG_DCEM_NUM, 
			(DWORD)iQty);
		return;
	}
	return;	
}

/*********************************************************************************
*  
*  FUNCTION NAME : JudgeLiBattLost
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-5 14:43:37
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
#define	LIBATT_LOST_STABLE_TIMES		30
static void JudgeLiBattLost(void)
{
	if(!IsWorkIn_LiBattMode())
	{

		//Set alarm signal to ALARM or NORMAL
		GC_SetEnumValue(TYPE_OTHER,
			0,
			GC_PUB_LI_BATT_LOST_STAT,
			GC_NORMAL,
			TRUE);
		return;
	}
	else
	{
		//continue
	}
	static int	s_iDelay = 0;

	int	iQty =	GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LI_BATT_ACTUAL_NUM);
	int	iCfgQty	= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_CFG_LI_BATT_NUM);	
	//printf("\n*******The current actual SM Temp Num is <%d>, and Cfg num is <%d>******\n",iQty,iCfgQty);
	// when user clear rect comm fail alarm, sampler will let rectifiers reallocate, iCfgQty is set to 101 by config file
	// and then clear iCfgQty to zero, wait for reallocation complete
	if(iCfgQty == 101)
	{
		//TRACE("iCfgQty=101, user maybe clear rect comm fail alarm. \n");
		s_iDelay = 0;
		iCfgQty = 0;
		GC_SetDwordValue(TYPE_OTHER,
			0,
			GC_PUB_CFG_LI_BATT_NUM, 
			(DWORD)iCfgQty);
	}
#define SIGVALUE_CLEAR_LOST	0
#define SIGVALUE_NOCLEAR_LOST	1

	if(s_iDelay < LIBATT_LOST_STABLE_TIMES)
	{
		//�ڲɼ����ֵ�autocfg�³�ʼ���˱�������������澯
		if(s_iDelay == 10)
		{
			GC_SetDwordValue(TYPE_OTHER,
				0,
				GC_PUB_LI_BATT_LOST_CLEAR_STAT,
				SIGVALUE_NOCLEAR_LOST);
		}
		s_iDelay++;
		return;
	}
	//�ڲɼ����ֵ�autocfg�³�ʼ���˱�������������澯
	int iClsLostSig = GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LI_BATT_LOST_CLEAR_STAT);
	if(iQty > iCfgQty)
	{
		GC_SetDwordValue(TYPE_OTHER,
			0,
			GC_PUB_CFG_LI_BATT_NUM, 
			(DWORD)iQty);
		return;
	}

	if(iClsLostSig == SIGVALUE_CLEAR_LOST)
	{
		//Set alarm signal to ALARM or NORMAL
		GC_SetEnumValue(TYPE_OTHER,
			0,
			GC_PUB_LI_BATT_LOST_STAT,
			GC_NORMAL,
			TRUE);
		if(iCfgQty > iQty)
		{
			GC_SetDwordValue(TYPE_OTHER,
				0,
				GC_PUB_CFG_LI_BATT_NUM, 
				(DWORD)iQty);
		}
		else
		{

		}
	}
	else
	{
		//Set alarm signal to ALARM or NORMAL
		GC_SetEnumValue(TYPE_OTHER,
			0,
			GC_PUB_LI_BATT_LOST_STAT,
			iCfgQty > iQty ? GC_ALARM : GC_NORMAL,
			TRUE);
	}
	//printf("\niQty = %d; iCfgQty = %d",iQty,iCfgQty);
	return;	
}
/*==========================================================================*
* FUNCTION : JudgeConvLost
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2006-09-11 16:54
*==========================================================================*/
static void JudgeConvLost(void)
{
	static int	s_iDelay = 0;
	static int  s_iStableDelay = 0;

	int			iQty = GC_GetDwordValue(TYPE_OTHER, 0, CT_PUB_QTY_SELF);
	int			iCfgQty	= GC_GetDwordValue(TYPE_OTHER, 0, CT_PUB_CFG_RECT_NUM);

	// when user clear rect comm fail alarm, sampler will let rectifiers reallocate, iCfgQty is set to 101 by config file
	// and then clear iCfgQty to zero, wait for reallocation complete
	if(iCfgQty == 101)
	{
		TRACE("iCfgQty=101, user maybe clear rect comm fail alarm. \n");
		s_iDelay = 0;
		iCfgQty = 0;
		GC_SetDwordValue(TYPE_OTHER,
			0,
			CT_PUB_CFG_RECT_NUM, 
			(DWORD)iCfgQty);
	}

	// wait for n cycles
	if(s_iDelay < RECT_LOST_DELAY_TIMES)
	{
		s_iDelay++;
		return;
	}

	//Update RT_PUB_CFG_RECT_NUM
	if(iQty > iCfgQty)
	{
		GC_SetDwordValue(TYPE_OTHER,
			0,
			CT_PUB_CFG_RECT_NUM, 
			(DWORD)iQty);
		return;
	}

	BOOL iGcAlarmStatus = iCfgQty > iQty ? GC_ALARM : GC_NORMAL;

	if (iGcAlarmStatus)
	{
		if (s_iStableDelay < CONVERT_LOST_STABLE_TIMES)
		{
			s_iStableDelay++;
			return;
		}
		else
		{
			//Continue  process!!
		}
	}
	else
	{
		s_iStableDelay = 0;
	}


	//Set alarm signal to ALARM or NORMAL
	GC_SetEnumValue(TYPE_OTHER,
		0,
		CT_PUB_CFG_RECT_LOST,
		iCfgQty > iQty ? GC_ALARM : GC_NORMAL,
		TRUE);

	return;	
}
#define  SLAVE_LOST_STABLE_TIMES	4
/*==========================================================================*
* FUNCTION : JudgeSlaveLost
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2006-09-11 16:54
*==========================================================================*/
static void JudgeSlaveLost(void)
{
#define MIN_LOST_SLAVE_NUMBER		1

	int			iQty = 0, i;	
	int			iCfgQty	= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_CFG_SLAVE_NUM);
	static			int iSleepTimes = 0;
	static int		s_iStableDelay = 0;

	//Sleep 200s after rebooting to verify the alarm.
	if(iSleepTimes < 30)
	{
		iSleepTimes++;
		return;
	}

	if(g_pGcData->enumMasterSlaveMode != MODE_MASTER)
	{
		if(iCfgQty != 0)
		{
			iCfgQty = 0;
			//Update GC_PUB_CFG_RECT_NUM
			GC_SetDwordValue(TYPE_OTHER,
				0,
				GC_PUB_CFG_SLAVE_NUM, 
				(DWORD)iCfgQty);			
		}

		//Restore normal
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			GC_PUB_CFG_SLAVE_LOST,
			GC_NORMAL,
			TRUE);

		return;
	}


	for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
	{
		if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
		{		

			iQty++;			
		}		
	}

	if(iQty > iCfgQty)
	{
		//Update GC_PUB_CFG_RECT_NUM
		GC_SetDwordValue(TYPE_OTHER,
			0,
			GC_PUB_CFG_SLAVE_NUM, 
			(DWORD)iQty);
		return;
	}

	BOOL iGcAlarmStatus = iCfgQty > iQty ? GC_ALARM : GC_NORMAL;

	if (iGcAlarmStatus)
	{
		if (s_iStableDelay < SLAVE_LOST_STABLE_TIMES)
		{
			s_iStableDelay++;
			return;
		}
		else
		{
			//Continue  process!!
		}
	}
	else
	{
		s_iStableDelay = 0;
	}

	if(iCfgQty - iQty >= MIN_LOST_SLAVE_NUMBER)
	{
		//Raise alarm
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			GC_PUB_CFG_SLAVE_LOST,
			GC_ALARM,
			TRUE);
	}
	else
	{
		//Restore normal
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			GC_PUB_CFG_SLAVE_LOST,
			GC_NORMAL,
			TRUE);
	}
	return;

}

/*==========================================================================*
 * FUNCTION : JudgeRectNoRespond
 * PURPOSE  : To judge if there are rectifiers lost(communication break)
 * CALLS    : GC_GetDwordValue
 * CALLED BY: RectRunInfo
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:44
 *==========================================================================*/
static BOOL	JudgeRectNoRespond(void)
{
	int		i, j;

	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		for(j = 0; j < g_pGcData->RunInfo.Rt.Slave[i].iQtyOfRect; j++)
		{
			if(!(g_pGcData->RunInfo.Rt.Slave[i].abComm[j]))
			{
				return TRUE;
			}
		}
	}


	return FALSE;
}
#ifdef GC_SUPPORT_MPPT
/*==========================================================================*
* FUNCTION : JudgeMpptNoRespond
* PURPOSE  : To judge if there are rectifiers lost(communication break)
* CALLS    : GC_GetDwordValue
* CALLED BY: RectRunInfo
* RETURN   : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-25 13:44
*==========================================================================*/
static BOOL	JudgeMpptNoRespond(void)
{
	int		j;

	for(j = 0; j < g_pGcData->RunInfo.Mt.iQtyOfRect; j++)
	{
		if(!(g_pGcData->RunInfo.Mt.abComm[j]))
		{
			return TRUE;
		}
	}

	return FALSE;
}


/*==========================================================================*
* FUNCTION : IsMpptNightStatus
* PURPOSE  : To judge if the retifier AC is failure
* CALLS    : 
* CALLED BY: 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-12-08 14:58
*==========================================================================*/
static BOOL IsMpptNightStatus(BOOL bLast)
{
    BOOL       bNight = TRUE;
    int        i;
    int        iQtyCommRect = 0;
    int        iMPPTNum = 0;

    iQtyCommRect = (int)GC_GetDwordValue(TYPE_OTHER, 0, MT_PUB_COMM_QTY);
    iMPPTNum = (int)GC_GetDwordValue(TYPE_OTHER, 0, MT_PUB_QTY_SELF);

    //All rectifiers communication fail
    //if(!iQtyCommRect)
    //{
    //  return bLast;     
    //}

    if( iMPPTNum == 0)
    {
       return bLast;     
    }

    for(i = 0; i < g_pGcData->RunInfo.Mt.iQtyOfRect; i++)
    {
       if(g_pGcData->RunInfo.Mt.abComm[i])
       {   
           if(GC_NORMAL 
              == GC_GetEnumValue(TYPE_MPPT_UNIT, i, MT_PRI_NIGHT_STATUS))
           {
              bNight = FALSE;
              break;
           }
       }
    }

    return bNight;
}

#endif
/*==========================================================================*
 * FUNCTION : 
 * PURPOSE  : To judge if thre are rectifiers lost(communication break)
 * CALLS    : GC_GetDwordValue
 * CALLED BY: RectRunInfo
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:44
 *==========================================================================*/
static BOOL	JudgeMasterRectNoRespond(void)
{
	int		i, j;
#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif


	for(j = 0; j < g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect; j++)
	{
		if(!(g_pGcData->RunInfo.Rt.Slave[0].abComm[j]))
		{
			return TRUE;
		}
	}

#ifdef GC_SUPPORT_MPPT
	if(stState != GC_MPPT_DISABLE)
	{	
		return g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect > 0 ? FALSE : TRUE;
	}
#endif

	return FALSE;
}
/*==========================================================================*
 * FUNCTION : IsRectAcFail
 * PURPOSE  : To judge if the retifier AC is failure
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-08 14:58
 *==========================================================================*/
static BOOL IsRectAcFail(BOOL bLast)
{
	BOOL		bAcFail = TRUE;
	int			i;
	int			iQtyCommRect = 0;

	iQtyCommRect = (int)GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_COMM_QTY);
	
	//All rectifiers communication fail
	if(!iQtyCommRect)
	{
		if(RECTIFIER_DOUBLE_POWER_TYPE 
			== GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_POWER_TYPE))
		{
			return bLast;
		}
		else
		{
			if(g_pGcData->RunInfo.Bt.bDischarge)
			{
				return TRUE;
			}
			else
			{
				return bLast;
			}
		}
	}

	for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect; i++)
	{
		if(g_pGcData->RunInfo.Rt.Slave[0].abComm[i])
		{	
			if(GC_NORMAL 
				== GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_FAIL))
			{
				bAcFail = FALSE;
				break;
			}
		}
	}
	if(bAcFail)
	{
		for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID) 
				&& GC_NORMAL == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_MAINS_FAILURE_SLAVE1 + i - 1))
			{					
				bAcFail = FALSE;
				break;
			}		
		}
	}

	return bAcFail;
}

static BOOL IsMasterRectAcFail(BOOL bLast)
{
	BOOL		bAcFail = TRUE;
	int			i;
	int			iQtyCommRect = 0;

	iQtyCommRect = (int)GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_COMM_QTY);
	
	//All rectifiers communication fail
	if(!iQtyCommRect)
	{
		if(RECTIFIER_DOUBLE_POWER_TYPE 
			== GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_POWER_TYPE))
		{
			return bLast;
		}
		else
		{
			if(g_pGcData->RunInfo.Bt.bDischarge)
			{
				return TRUE;
			}
			else
			{
				return bLast;
			}
		}
	}

	for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect; i++)
	{
		if(g_pGcData->RunInfo.Rt.Slave[0].abComm[i])
		{	
			if(GC_NORMAL 
				== GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_FAIL))
			{
				bAcFail = FALSE;
				break;
			}
		}
	}
	return bAcFail;
}

/*==========================================================================*
 * FUNCTION : GetRectVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-09-30 09:53
 *==========================================================================*/
static float GetRectVolt(void)
{
	int		i, j;
	int		iRectNum = 0;
	float	fRectVolt[MAX_NUM_RECT_EQUIP];

	if(!(g_pGcData->RunInfo.Rt.bValid))
	{
		return 0.0;
	}

	for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect; i++)
	{
		if(g_pGcData->RunInfo.Rt.Slave[0].abComm[i])
		{
			fRectVolt[iRectNum] 
					= GC_GetFloatValue(TYPE_RECT_UNIT, 
									i,
									RT_PRI_OUT_VOLT);
			iRectNum++;
		}
	}

    //effervescing sort
	if(iRectNum)
	{
		for (i = 0; i < (iRectNum - 1); i++)
		{
			for (j = 0; j < (iRectNum - 1 - i); j++)
			{
				if(fRectVolt[j] > fRectVolt[j + 1])
				{
					float fTemp = fRectVolt[j + 1];
					fRectVolt[j + 1] = fRectVolt[j];
					fRectVolt[j] = fTemp;
				}
			}
		}

		return fRectVolt[(iRectNum - 1) / 2];
	}
	else
	{
		return 0.0;
	}
	
}



/*==========================================================================*
 * FUNCTION : IsRectValid
 * PURPOSE  : To judge if rectifiers are valid
 * CALLS    : GC_GetSigVal
 * CALLED BY: RectRunInfo
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:41
 *==========================================================================*/
static BOOL	IsRectValid(void)
{
	//int					i, iRst;
	//SIG_BASIC_VALUE*	pSigValue;
	//
	//for(i = 0; i < g_pGcData->EquipInfo.iCfgQtyOfRect; i++)
	//{
	//	iRst = GC_GetSigVal(TYPE_RECT_UNIT, i, RT_PRI_OUT_CURR, &pSigValue);
	//	GC_ASSERT((iRst == ERR_OK), 
	//		ERR_CTL_GET_VLAUE, 
	//		"GC_GetSigVal RT_PRI_OUT_CURR error!\n");
	//	if(SIG_VALUE_IS_VALID(pSigValue)) 
	//		/*&& (g_pGcData->RunInfo.Rt.abCommSelf[i]))*/
	//	{
	//		return TRUE;
	//	}
	//}
	return g_pGcData->RunInfo.Rt.iQtyOnWorkRect ? TRUE : FALSE;
}


#define	 HYSTERESIS_TIMES_MULTI_RECT_FAIL	2
/*==========================================================================*
 * FUNCTION : RectRunInfo()
 * PURPOSE  : refresh rectifiers run infomation 
 * CALLS    : GC_MaintainRectList List_GotoHead List_Get List_GotoNext
              GC_GetFloatValue IsRectAcFail
 * CALLED BY: GC_RunInfoRefresh
 * RETURN   :
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-15 17:08
 *==========================================================================*/
static void	RectRunInfo(void)
{
	int				i, j;
	float				fCapOfRtWithLmt = 0.0;
	float				fUsedCap = 0.0;
	float				fRatedCurr = 0.0;
	float				fUnitRatedCurr = 0.0, fTotalRatedCurr = 0.0, fMasterRatedCurr = 0.0;
	float				fSingleRatedCurr = 0.0,fTotalOffRateCurr=0.0;
	int					iEnableECO=0;
	//Calculate the master rect current in master mode;
	float				fMasterRectCurr = 0.0;
	static BOOL			s_bFirstRunning = TRUE;
	static int			s_iBCLAlarmcount = 0;

	GC_RUN_INFO_RECT		*pRtRunInfo = &(g_pGcData->RunInfo.Rt);
	int					aiRectType[] =
							{TYPE_RECT_UNIT,
							TYPE_SLAVE1_RECT,
							TYPE_SLAVE2_RECT,
							TYPE_SLAVE3_RECT,};
	static	int			s_iMultiRectFailCounter = 0;
	int				iQtyOfRect = 0;
	float				fPriUsedCap = 0.0, fTotalUsedCap = 0.0;
	static	float			fMaxUsedCap = 0.0, fMinUsedCap = 110.0;
	float	fBCLAlarmlimit,fBCLNormallimit ;
	BOOL    bRTN;

	int			aiRTOutCurrSid[] = {RT_PRI_OUT_CURR, 
									S1_PRI_OUT_CURR,
									S2_PRI_OUT_CURR,
									S3_PRI_OUT_CURR,};

	int			aiRTCurrLimitSid[] = {RT_PRI_CURR_LMT, 
								S1_PRI_CURR_LMT,
								S2_PRI_CURR_LMT,
								S3_PRI_CURR_LMT,};

	int			aiRTValidCapSid[] = {RT_PRI_VALID_CAP, 
									S1_PRI_VALID_CAP,
									S2_PRI_VALID_CAP,
									S3_PRI_VALID_CAP,};
	BOOL			bMaxCurrentSleep = TRUE;


	pRtRunInfo->fRatedVolt = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_RATED_VOLT);

	if(pRtRunInfo->fRatedVolt < 20.0)
	{
		pRtRunInfo->fRatedVolt = 48.0;
	}

	//Qty of rectifiers
	pRtRunInfo->Slave[RECT_TYPE_SELF].iQtyOfRect 
		= GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_QTY_SELF);
	iQtyOfRect = GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_QTY_SELF);

	for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
	{
		if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_IsGCEquipExist))
		{
			pRtRunInfo->Slave[RECT_TYPE_SLAVE1 + i - RECT_TYPE_SLAVE1].iQtyOfRect 
				= GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_QTY_SLAVE1 + i - RECT_TYPE_SLAVE1);
		}
		else
		{
			pRtRunInfo->Slave[RECT_TYPE_SLAVE1 + i - RECT_TYPE_SLAVE1].iQtyOfRect = 0;
		}
		iQtyOfRect += pRtRunInfo->Slave[RECT_TYPE_SLAVE1 + i - RECT_TYPE_SLAVE1].iQtyOfRect;
	}
	
	pRtRunInfo->iQtyOfRect = iQtyOfRect;

	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		for(j = 0; j < pRtRunInfo->Slave[i].iQtyOfRect; j++)
		{
			pRtRunInfo->Slave[i].abComm[j] = GC_IsRectComm(i, j);
			pRtRunInfo->Slave[i].fRatedCurrA[j] = GC_GetRectValidCapacity(i, j);

		}
	}

	fTotalRatedCurr = 0.0;
	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		for(j = 0; j < pRtRunInfo->Slave[i].iQtyOfRect; j++)
		{			
			fTotalRatedCurr += GC_GetFloatValue(aiRectType[i], 
							j,
							aiRTValidCapSid[i]);			
		}	
	}
	GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_TOTAL_RATED_CURRENT_T,
			fTotalRatedCurr);	

	//Set Rectifier number(Include slave rectifier number)
	int iTotalNumber = 0;
	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		iTotalNumber +=  pRtRunInfo->Slave[i].iQtyOfRect;		
	}
	GC_SetDwordValue(TYPE_OTHER, 
			0,
			RT_PUB_TOTAL_NUMBER_INCLUDE_SLAVE,
			iTotalNumber);	


	/*for(i = 0; i < pRtRunInfo->Slave[RECT_TYPE_SELF].iQtyOfRect; i++)
	{
		if(pRtRunInfo->Slave[RECT_TYPE_SELF].abComm[i])
		{
			GC_SetEnumValue(TYPE_RECT_UNIT, 
							i,
							RT_PRI_CURR_LMT_SET,
							GC_NORMAL,
							TRUE);	

		}
	}

	for(; i < MAX_NUM_RECT_EQUIP; i++)
	{
		GC_SetEnumValue(TYPE_RECT_UNIT, 
						i,
						RT_PRI_CURR_LMT_SET,
						GC_NORMAL,
						TRUE);	
	}*/
	/*printf("\n-----before GC_MaintainRectList---PrintRectTime()--------\n");
	PrintRectTime();*/

	GC_MaintainRectList();
	/*printf("\n-----end GC_MaintainRectList---PrintRectTime()--------\n");
		PrintRectTime();*/

	RUN_THREAD_MSG	msg;

	if(s_bFirstRunning || (RunThread_GetMessage(RunThread_GetId(NULL), &msg, FALSE, 0) == ERR_THREAD_OK && msg.dwMsg == MSG_RECT_ALLOCATION))
	{
		InitRectTime();
		s_bFirstRunning = FALSE;

	}

	CalcRectTime();		//calculate the rect running time self.
//printf("\n-----end CalcRectTime---PrintRectTime()--------\n");
//	PrintRectTime();

	pRtRunInfo->bNoRespond = JudgeRectNoRespond();
	pRtRunInfo->bMasterNoRespond = JudgeMasterRectNoRespond();
	pRtRunInfo->bAcFail = IsRectAcFail(pRtRunInfo->bAcFail);
	pRtRunInfo->bMasterAcFail = IsMasterRectAcFail(pRtRunInfo->bMasterAcFail); 

	pRtRunInfo->fVolt = GetRectVolt();
	pRtRunInfo->bValid = IsRectValid();

	GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_AVE_VOLT, pRtRunInfo->fVolt);

	//Calculate sum of rectifies current
	pRtRunInfo->fSumOfCurr = 0.0;
	fUnitRatedCurr = 0.0;
	fTotalRatedCurr = 0.0;
	List_GotoHead(pRtRunInfo->hListOnWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(pRtRunInfo->hListOnWorkRect, 
					(void*)(&RectifierInfo)))
		{
			float fPriCurr = GC_GetFloatValue(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType, 
											RectifierInfo.iRectNo,
											aiRTOutCurrSid[RectifierInfo.iRectEquipType]);
			float fPriLmt = GC_GetFloatValue(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType, 
											RectifierInfo.iRectNo,
											aiRTCurrLimitSid[RectifierInfo.iRectEquipType]);

			fUnitRatedCurr = GC_GetFloatValue(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType, 
											RectifierInfo.iRectNo,
											aiRTValidCapSid[RectifierInfo.iRectEquipType]);


			pRtRunInfo->fSumOfCurr 	+= fPriCurr;

			fCapOfRtWithLmt += fUnitRatedCurr * fPriLmt / 100.0;

			fPriUsedCap = 100.0 * fPriCurr / fUnitRatedCurr/GC_CALCULATE_CAPACITY_DELTA;
			fTotalRatedCurr += fUnitRatedCurr;
						

			//manage the self rectifier
			if(RectifierInfo.iRectEquipType == RECT_TYPE_SELF)
			{
				if(!isnan(fPriUsedCap))
				{
					GC_SetFloatValue(TYPE_RECT_UNIT, 
								RectifierInfo.iRectNo,
								RT_PRI_USED_CAP,
								fPriUsedCap );//Add /1.075 to avoid the value > 100, 1.075 provide by Alfred								
				}

				fMasterRectCurr  += fPriCurr;
				fTotalUsedCap += fPriUsedCap;
				fMasterRatedCurr += fUnitRatedCurr;
							

				/*if(fPriCurr < (fPriLmt * fUnitRatedCurr * 0.95 / 100))
				{
					GC_SetEnumValue(TYPE_RECT_UNIT, 
									RectifierInfo.iRectNo,
									RT_PRI_CURR_LMT_SET,
									GC_NORMAL,
									TRUE);
				}
				else if(fPriCurr > (fPriLmt * fUnitRatedCurr * 0.97 / 100))
				{
					GC_SetEnumValue(TYPE_RECT_UNIT, 
									RectifierInfo.iRectNo,
									RT_PRI_CURR_LMT_SET,
									GC_ALARM,
									TRUE);	
				}*/
			}

		}

	}while(List_GotoNext(pRtRunInfo->hListOnWorkRect));	


	pRtRunInfo->fSumOnRatedCurrent = fTotalRatedCurr;
	//GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR, fTotalRatedCurr);
	//For web/lcd to calculate the used capacity, it only inlude the master rated current,
	//Web/LCD will add slave rated current by themself.
	GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR, fMasterRatedCurr);

	
	
	if(pRtRunInfo->iQtyOnWorkRect > 0)
	{
		pRtRunInfo->fRatedCurr = fTotalRatedCurr/pRtRunInfo->iQtyOnWorkRect;
		
		//2016-9-13, to include all the rectifier from secondary rectifier. by Marco
		//bMaxCurrentSleep = SetMaxCurrentLimit(pRtRunInfo->Slave[RECT_TYPE_SELF].iQtyOfRect, pRtRunInfo->fRatedCurr);		
//		printf("gc_run_info_----------------------%d \n",pRtRunInfo->iQtyOfRect);
		bMaxCurrentSleep = SetMaxCurrentLimit(pRtRunInfo->iQtyOfRect, pRtRunInfo->fRatedCurr);
		//End 2016-9-13, to include all the rectifier from secondary rectifier. by Marco

		if(GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_ENABLE) && STATE_AUTO == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN) && !bMaxCurrentSleep)
		{	

			float fMaxLmtCurr = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_POINT);
			pRtRunInfo->fMaxCurrLmtPoint = 100 * fMaxLmtCurr / fTotalRatedCurr;		
			
		}
		else
		{
			pRtRunInfo->fMaxCurrLmtPoint = GC_MAX_CURR_LMT;
		}

		if(pRtRunInfo->fMaxCurrLmtPoint > GC_MAX_CURR_LMT)
		{
			pRtRunInfo->fMaxCurrLmtPoint = GC_MAX_CURR_LMT;
		}
		else if(pRtRunInfo->fMaxCurrLmtPoint < GC_MIN_CURR_LMT)
		{
			pRtRunInfo->fMaxCurrLmtPoint = GC_MIN_CURR_LMT;
		
		}
		
	}
	else
	{
		fRatedCurr = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_RATED_CURR);
		if(fRatedCurr < 3.0)
		{
			fRatedCurr = 50.0;
		}
		pRtRunInfo->fRatedCurr = fRatedCurr;

		pRtRunInfo->fMaxCurrLmtPoint = GC_MAX_CURR_LMT;
	}
	GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_MAX_CURR_LMT_SAMPLE,
				pRtRunInfo->fMaxCurrLmtPoint);
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		RT_PUB_MAX_CURR_LMT_VALUE,
		pRtRunInfo->fRatedCurr * pRtRunInfo->iQtyOfRect * GC_MAX_CURR_LMT / 100);
	
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		RT_PUB_MIN_CURR_LMT_SAMPLE,
		pRtRunInfo->fRatedCurr * pRtRunInfo->iQtyOfRect * GC_MIN_CURR_LMT_NA / 100);

	List_GotoHead(pRtRunInfo->hListOffWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(pRtRunInfo->hListOffWorkRect, 
					(void*)(&RectifierInfo)))
		{
			if(RectifierInfo.iRectEquipType == RECT_TYPE_SELF)
			{
				/*GC_SetEnumValue(TYPE_RECT_UNIT , 
								RectifierInfo.iRectNo,
								RT_PRI_CURR_LMT_SET,
								GC_NORMAL,
								TRUE);	*/

				GC_SetFloatValue(TYPE_RECT_UNIT, 
								RectifierInfo.iRectNo,
								RT_PRI_USED_CAP,
								0.0);

				//2019-6, TR#:1194-19-NSC 3.0
				fSingleRatedCurr = GC_GetFloatValue(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType, 
											RectifierInfo.iRectNo,
											aiRTValidCapSid[RectifierInfo.iRectEquipType]);
				fTotalOffRateCurr += fSingleRatedCurr;
			}
		}
	}while(List_GotoNext(pRtRunInfo->hListOffWorkRect));	


	if(SEC_TMR_TIME_IN != GC_GetStateOfSecTimer(SEC_TMR_ID_RECT_CURR))
	{
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					GC_PUB_RECT_ALLCURRENT,
					pRtRunInfo->fSumOfCurr);

		GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_TOTAL_CURR,
					fMasterRectCurr);

	}

	if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
	{
		if(ST_FLOAT == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) || ST_MAN_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)
			|| ST_AUTO_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) || ST_CYCLIC_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)
			|| ST_POWER_SPLIT_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		{

			if(g_pGcData->RunInfo.Bt.iBattCurrlimitFlag == GC_ALARM)
			{
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					BT_PUB_CURR_LMT_SET,
					GC_ALARM,
					TRUE);		
			
			}
			else
			{	
				GC_SetEnumValue(TYPE_OTHER, 
				0,
				BT_PUB_CURR_LMT_SET,
				GC_NORMAL,
				TRUE);
			}
		}
		else
		{
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				BT_PUB_CURR_LMT_SET,
				GC_NORMAL,
				TRUE);
		}

	}
	else
	{
		g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_NORMAL;
				

		if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_BCL_ALARM_POINT_CURR_MODE))
		{
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_BCL_ALARM_POINT_CURR_MODE,&fBCLAlarmlimit);
			if(bRTN == FALSE)
			{
				fBCLAlarmlimit = GC_BCL_ALARM_CURR_MODE;
			}
		}
		else
		{
			fBCLAlarmlimit = GC_BCL_ALARM_CURR_MODE;
		}
		if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_BCL_NORMAL_POINT_CURR_MODE))
		{
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_BCL_NORMAL_POINT_CURR_MODE,&fBCLNormallimit);
			if(bRTN == FALSE)
			{
				fBCLNormallimit = GC_BCL_NORMAL_CURR_MODE;
			}
		}
		else
		{
			fBCLNormallimit = GC_BCL_NORMAL_CURR_MODE;
		}


		if(!GC_IsDislPwrLmtMode() && GC_DISABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_ENABLE) &&
			MODE_SLAVE != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MASTER_SLAVE_MODE) &&
			(ST_FLOAT == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) || ST_MAN_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)
			|| ST_AUTO_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) || ST_CYCLIC_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)
			|| ST_POWER_SPLIT_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
			)
		{
			if(pRtRunInfo->fSumOfCurr > (fCapOfRtWithLmt * fBCLAlarmlimit) && (pRtRunInfo->fSumOfCurr < 0.95*fTotalRatedCurr ))
			{
				s_iBCLAlarmcount = 0;
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					BT_PUB_CURR_LMT_SET,
					GC_ALARM,
					TRUE);	
			}
		}

		if((pRtRunInfo->fSumOfCurr < (fCapOfRtWithLmt * fBCLNormallimit))
			|| (pRtRunInfo->fSumOfCurr < 0.2) || (pRtRunInfo->fSumOfCurr > 0.95*fTotalRatedCurr ))
		{
			if(s_iBCLAlarmcount < 5)
			{
				s_iBCLAlarmcount ++;
			}
			else
			{
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					BT_PUB_CURR_LMT_SET,
					GC_NORMAL,
					TRUE);
			}
		}
	}
	

	//2019-6, TR#:1194-19-NSC 3.0	
	iEnableECO = GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_ENB);


	if(pRtRunInfo->Slave[0].iQtyOfRect)
	{
		if(fTotalUsedCap > 0.1)
		{
			if( 1 == iEnableECO )
			{
				fUsedCap = 100 * fMasterRectCurr/(fMasterRatedCurr+fTotalOffRateCurr)/GC_CALCULATE_CAPACITY_DELTA ; //Add /1.075 to avoid the value > 100, 1.075 provide by Alfred
			}
			else
			{
				fUsedCap = 100 * fMasterRectCurr/ fMasterRatedCurr/GC_CALCULATE_CAPACITY_DELTA ; //Add /1.075 to avoid the value > 100, 1.075 provide by Alfred
			}
		}
		else
		{
			fUsedCap = 0.0;
		}
		
	}
	else
	{
		fUsedCap = 0.0;
	}

	if(isnan(fUsedCap))
	{
		fUsedCap = 0.0;
	}
	if(fUsedCap > fMaxUsedCap)
	{
		fMaxUsedCap = fUsedCap;
	}
	if(fUsedCap < fMinUsedCap && fUsedCap >  0.1)
	{
		fMinUsedCap = fUsedCap;
	}

	
	GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_USED_CAP,
					fUsedCap);	
	GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_MAX_USED_CAP,
					fMaxUsedCap);
	GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_MIN_USED_CAP,
					fMinUsedCap);		
	


	//Rctifier AC_Fail
	/*if(g_pGcData->RunInfo.Rt.bAcFail)
	{
		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_AC_FAIL, GC_ALARM, TRUE);
	}
	else
	{
		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_AC_FAIL, GC_NORMAL, TRUE);	
	}*/

	if(IsMultiRectFail())
	{
		if(s_iMultiRectFailCounter > HYSTERESIS_TIMES_MULTI_RECT_FAIL)
		{
			GC_SetEnumValue(TYPE_OTHER, 
						0,
						RT_PUB_MULTI_RECT_FAULT,
						GC_ALARM, 
						TRUE);
		}
		else
		{
			s_iMultiRectFailCounter++;	
		}
	}
	else
	{
		s_iMultiRectFailCounter = 0;
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						RT_PUB_MULTI_RECT_FAULT,
						GC_NORMAL, 
						TRUE);	
	}

	JudgeRectLost();	
	JudgeSlaveLost();
	

	//CalcWorkOnRectTotalRatedCurr();

	return;
}

static BOOL SetMaxCurrentLimit(IN int iQtyOfRect, IN float fRatedCurr)
{
//TRUE: in wait state
//FALSE: Other 
	static int iSleepTimes = 0;
	static int iRectNumber = 0;
	
	if(iSleepTimes < 40)//Delay, to confirm all rectifier can be scanned.
	{
		iSleepTimes++;
		return TRUE; 

	}
	if(GC_DISABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_ENABLE))
	{
		return FALSE;
	}
	
	if(iRectNumber == 0)
	{
		iRectNumber = GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_SYS_CURR_LIMIT_RECT_NUM);//��ȡֵ;
		if(iRectNumber == 0)
		{
			iRectNumber = iQtyOfRect;
			GC_SetDwordValue(TYPE_OTHER, 0, RT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}			
	}

	float   fDefaultCurrPoint =  GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DEFAULT_MAX_CURR_LMT_POINT);
	float	fDefaultValue = iQtyOfRect * fRatedCurr * fDefaultCurrPoint/100;

	float	fInitValue = iQtyOfRect * fRatedCurr * GC_MAX_CURR_LMT/100;
	float	fInitMinValue = iQtyOfRect * fRatedCurr * GC_MIN_CURR_LMT_IN_NA/100;

	float   fDefinedCurrValue =  GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_POINT);	


	if(FLOAT_EQUAL(fInitValue, 0.0) == TRUE)//No rectifier,just skip
	{
		return FALSE;
	}
	else if(FLOAT_EQUAL(fDefinedCurrValue, GC_MAX_CURR_LIMIT_VALUE) == TRUE || fDefinedCurrValue > GC_MAX_CURR_LIMIT_VALUE)//�����һ������ֵ�����ȡ��ǰģ������Ȼ����Ϊ��ǰֵ��
	{
		//First run, set it as max current of system
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_MAX_CURR_LMT_POINT,
			fDefaultValue);
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_DEFAULT_POINT_INTERAL,
			fDefaultValue * 100/(iQtyOfRect * fRatedCurr) );
	}
	else 
	{	

		//If now max current of system < setting value, change it.
		if(fDefinedCurrValue > fInitValue  )
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_MAX_CURR_LMT_POINT,
				fInitValue);
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_DEFAULT_POINT_INTERAL,
				GC_MAX_CURR_LMT);

			fDefinedCurrValue = fInitValue;
			iRectNumber = iQtyOfRect;
			GC_SetDwordValue(TYPE_OTHER, 0, RT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}
		else if(fDefinedCurrValue < fInitMinValue)
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_MAX_CURR_LMT_POINT,
				fInitMinValue);
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_DEFAULT_POINT_INTERAL,
				GC_MIN_CURR_LMT_IN_NA);

			fDefinedCurrValue = fInitMinValue;
			iRectNumber = iQtyOfRect;
			GC_SetDwordValue(TYPE_OTHER, 0, RT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}

		float	fCalculateCurrValue = iQtyOfRect * fRatedCurr * GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DEFAULT_POINT_INTERAL) / 100;

		//if(FLOAT_EQUAL(fDefinedCurrValue, fCalculateCurrValue) == TRUE)
		//{
		//	return FALSE;
		//}

		if(iQtyOfRect > iRectNumber)
		{
			iRectNumber = iQtyOfRect;
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_MAX_CURR_LMT_POINT,
				fCalculateCurrValue);
			GC_SetDwordValue(TYPE_OTHER, 0, RT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}
		else if(iQtyOfRect < iRectNumber)
		{
			iRectNumber = iQtyOfRect;
			GC_SetDwordValue(TYPE_OTHER, 0, RT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}
		else
		{
			float fSetInterValue = fDefinedCurrValue * 100/(iQtyOfRect * fRatedCurr);
			if(fSetInterValue > GC_MAX_CURR_LMT)
			{
				GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_DEFAULT_POINT_INTERAL,
					GC_MAX_CURR_LMT);

			}
			else
			{			
				GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_DEFAULT_POINT_INTERAL,
					 fSetInterValue);
			}
			
		}
	}

	return FALSE;

}
static void	ConvRunInfo(void)
{
	int				i, j, iValidConv;
	static	SIG_ENUM	s_enumLastAutoManState =  STATE_MAN;
	
	GC_CONV_INFO_CONV		*pConvRunInfo = &(g_pGcData->RunInfo.Conv);
	int				iQtyOfConv = 0;
	float				fConvVolt[MAX_NUM_CT_EQUIP], fAveVolt[MAX_NUM_CT_EQUIP];
	float				fAverageVolt = 0.0;
	float				fDefaultValue = 0.0;
	float				fUsedCapacity = 0.0;
	static		BOOL		bFirstStart = TRUE;
	static		int		iSleepTimes = 0;
	static	float			fMaxUsedCap = 0.0, fMinUsedCap = 110.0;
	
	pConvRunInfo->fSumOfCurr = 0.0;
	
	iQtyOfConv = GC_GetDwordValue(TYPE_OTHER, 0, CT_PUB_QTY_SELF);

	j = 0;
	for(i = 0; i < iQtyOfConv; i++)
	{
		if(GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_ONOFF_STATE) == GC_CT_ON 
			&& GC_IsConvComm(i))
		{
			float fPriCurr = GC_GetFloatValue(TYPE_CT_UNIT, 
								i,
								CT_PRI_OUT_CURR);
			

			pConvRunInfo->fSumOfCurr += fPriCurr;
			fConvVolt[j++] = GC_GetFloatValue(TYPE_CT_UNIT, 
							i,
							CT_PRI_OUT_VOLT);			
		}

	}
	if(j)
	{
		fUsedCapacity =100 * pConvRunInfo->fSumOfCurr/(GC_CALCULATE_CAPACITY_DELTA * j * GC_GetFloatValue(TYPE_OTHER, 
									0,
									CT_PUB_RATED_CURRENT));
	}
	else
	{
		fUsedCapacity = 0.0;
	}
	pConvRunInfo->fSumOnRatedCurrent = j * GC_GetFloatValue(TYPE_OTHER, 
									0,
									CT_PUB_RATED_CURRENT);
	if(GC_IsGCSigExist(TYPE_OTHER,0,CT_PUB_USED_CAP))
	{
		GC_SetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_USED_CAP,
				fUsedCapacity);
	}

	if(fUsedCapacity > fMaxUsedCap)
	{
		fMaxUsedCap = fUsedCapacity;
	}
	if(fUsedCapacity < fMinUsedCap && fUsedCapacity >  0.1)
	{
		fMinUsedCap = fUsedCapacity;
	}

	if(GC_IsGCSigExist(TYPE_OTHER,0,CT_PUB_MAX_USED_CAP))
	{
		GC_SetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_MAX_USED_CAP,
						fMaxUsedCap);		
	}
	if(GC_IsGCSigExist(TYPE_OTHER,0,CT_PUB_MIN_USED_CAP))
	{
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					CT_PUB_MIN_USED_CAP,
					fMinUsedCap);	
	}	
	
	if(GC_IsGCSigExist(TYPE_OTHER,0,CT_PUB_CONVERTER_TYPE))
	{
		if(FLOAT_EQUAL(g_pGcData->RunInfo.fSysLoad, 0.0) && GC_GetEnumValue(TYPE_OTHER, 0, CT_PUB_CONVERTER_TYPE) != CONV_TYPE_400_48V)
		{
			pConvRunInfo->fSumOfCurr = 0.0;
		}
	}
	
	
	GC_SetFloatValue(TYPE_OTHER, 
				 0,
				 CT_PUB_TOTAL_CURRENT,
				pConvRunInfo->fSumOfCurr);
	
	float	fRatedInput = GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_RATED_CURRENT);

	GC_SetFloatValue(TYPE_OTHER, 
		0,
		CT_PUB_TOTAL_RATED_CURR,
		fRatedInput*iQtyOfConv);

	
	GC_MaintainConvList(iQtyOfConv);

	//effervescing sort
	iValidConv = j;
	if(iValidConv)
	{
		for (i = 0; i < (iValidConv - 1); i++)
		{
			for (j = 0; j < (iValidConv - 1 - i); j++)
			{
				if(fConvVolt[j] > fConvVolt[j + 1])
				{
					float fTemp = fConvVolt[j + 1];
					fConvVolt[j + 1] = fConvVolt[j];
					fConvVolt[j] = fTemp;
				}
			}
		}
		
		fAverageVolt = fConvVolt[(iValidConv - 1)/2]; 
		
		
	}
	else
	{
		fAverageVolt = 0.0;
	}
	pConvRunInfo->fAverVoltage = fAverageVolt;

	GC_SetFloatValue(TYPE_OTHER, 
		0,
		CT_PUB_AVERAGE_VOLT,
		fAverageVolt);

	if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
	{
		//Any converter communicate lost, release the current limit and in auto mode
		for(i = 0; i < iQtyOfConv; i++)
		{
			if(GC_GetEnumValue(TYPE_CT_UNIT, i, CT_PRI_COMMUNIATE_STATE) != GC_NORMAL )
			{
				GC_SendCTCurrentLimit(GC_MAX_CURR_LMT_CONV);
				//GC_SetFloatValue(TYPE_OTHER, 
				//	0,
				//	CT_PUB_CTL_CURR,
				//	GC_MAX_CURR_LMT_CONV);//GC_CT_MAX_CURR_LMT);
				break;
			}
		}

		//State from manual to auto, release the voltage, hvsd voltage, current limit to the default.
		if(STATE_MAN == s_enumLastAutoManState)
		{
			fDefaultValue = GC_GetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_CFG_DEFAULT_CURR);

			GC_SendCTCurrentLimit(fDefaultValue);
			/*GC_SetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_CTL_CURR,
				fDefaultValue);*/
			//printf("ConvRunInfo\n");

	/*		
*/
			s_enumLastAutoManState = STATE_AUTO;
		}
		
		int	iConvNumber = GC_GetDwordValue(TYPE_OTHER, 0, CT_PUB_QTY_SELF);
		//If there is no Converter, just return;
		if(iConvNumber)
		{
			/*if(GC_GetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_OUTPUT_RATED_VOLT) > 35.0)*/
			if (0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CT_VOLT_LEVEL))//48V
			{
				
				fDefaultValue = GC_GetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_CFG_DEFAULT_VOLT);
				GC_SetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_CTL_VOLT,
						fDefaultValue);
			}
			else
			{
				fDefaultValue = GC_GetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_CFG_DEFAULT_VOLT_24V);
				GC_SetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_CTL_VOLT_24V,
						fDefaultValue);
				
			}
		}
			
				
	}
	else
	{
		s_enumLastAutoManState = STATE_MAN;
	}

	if(IsMultiConvFail(iQtyOfConv))
	{
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			CT_PUB_CFG_MULTI_FAIL_ALARM,
			GC_ALARM,
			TRUE);	
	}
	else
	{
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			CT_PUB_CFG_MULTI_FAIL_ALARM,
			GC_NORMAL,
			TRUE);
	}

	JudgeConvLost();
	SetMaxConvCurrentLimit(iQtyOfConv);

	

	return;
}
static void SetMaxConvCurrentLimit(IN int iQtyOfRect)
{

	static int iSleepTimes = 0;
	static int iRectNumber = 0;//֮ǰģ������
	static BOOL s_bLastState = GC_DISABLED;
	float	fRatedCurr = GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_RATED_CURRENT);//27.0;
	float	fRatedInput = GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_INTPUT_RATED_VOLT);

	if(iSleepTimes < 20)//Delay, to confirm all rectifier can be scanned.
	{
		iSleepTimes++;
		return;

	}
	if(GC_DISABLED == GC_GetEnumValue(TYPE_OTHER, 0, CT_PUB_MAX_CURR_LMT_ENABLE))
	{
		//Release the current limit point.
		if( s_bLastState == GC_ENABLED)
		{	
			if(fRatedInput > 200.0)
			{
				GC_SendCTCurrentLimit(GC_MAX_CURR_LMT_CONV);	

			}
			else
			{
				if(fRatedInput > 35.0)
				{
					GC_SendCTCurrentLimit(GC_MAX_CURR_LMT_CONV_24V);	
				}
				else
				{
					GC_SendCTCurrentLimit(GC_MAX_CURR_LMT_CONV);	

				}
			}
			
			//GC_SetFloatValue(TYPE_OTHER, 
			//	0,
			//	CT_PUB_CTL_CURR,
			//	GC_MAX_CURR_LMT_CONV);//fRatedCurr * GC_MAX_CURR_LMT_CONV/100);//Max output 
			s_bLastState = GC_DISABLED;			
		}
		return;
	}

	s_bLastState = GC_ENABLED;
	if(STATE_AUTO != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN))
	{
		return;
	}

	if(iRectNumber == 0)
	{
		iRectNumber = GC_GetDwordValue(TYPE_OTHER, 0, CT_PUB_SYS_CURR_LIMIT_RECT_NUM);//��ȡֵ;
		if(iRectNumber == 0)
		{
			iRectNumber = iQtyOfRect;
			GC_SetDwordValue(TYPE_OTHER, 0, CT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}

	}

	float   fDefaultCurrPoint =  GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_DEFAULT_MAX_CURR_LMT_POINT);//Ĭ��������%
	float	fDefaultValue = iQtyOfRect * fRatedCurr * fDefaultCurrPoint/100;//����Ĭ��������͵�ǰģ�����������������㡣

	float	fInitMaxValue ;//= iQtyOfRect * fRatedCurr * GC_MAX_CURR_LMT_CONV/100;//������������������������ֵ

	if(fRatedInput > 200.0)
	{
		fInitMaxValue = iQtyOfRect * fRatedCurr * GC_MAX_CURR_LMT_CONV/100;//������������������������ֵ

	}
	else
	{
		if(fRatedInput > 35.0)
		{
			fInitMaxValue = iQtyOfRect * fRatedCurr * GC_MAX_CURR_LMT_CONV_24V/100;//������������������������ֵ
		}
		else
		{
			fInitMaxValue = iQtyOfRect * fRatedCurr * GC_MAX_CURR_LMT_CONV/100;//������������������������ֵ

		}
	}


	float	fInitMinValue = iQtyOfRect * fRatedCurr * GC_MIN_CURR_LMT_IN_CONV/100;//������С�������������С���ֵ

	float   fDefinedCurrValue;

	if(iQtyOfRect > iRectNumber)
	{
		fDefinedCurrValue = iQtyOfRect * fRatedCurr * GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_DEFAULT_POINT_INTERAL) / 100;
	}
	else
	{
		fDefinedCurrValue =  GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_MAX_CURR_LMT_POINT);//��ǰ��ʵ��������ֵ��
	}

	//printf("fDefinedCurrValue = %f\n", fDefinedCurrValue);

	if(FLOAT_EQUAL(fInitMaxValue, 0.0) == TRUE)//No rectifier,just skip
	{
		return;
	}
	else if(FLOAT_EQUAL(fDefinedCurrValue, GC_MAX_CURR_LIMIT_VALUE) == TRUE || fDefinedCurrValue > GC_MAX_CURR_LIMIT_VALUE)//�����һ������ֵ�����ȡ��ǰģ������Ȼ����Ϊ��ǰֵ��
	{
		//First run, set it as max current of system
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			CT_PUB_MAX_CURR_LMT_POINT,//210		2		51
			fDefaultValue);
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			CT_PUB_DEFAULT_POINT_INTERAL,
			fDefaultValue * 100/(iQtyOfRect * fRatedCurr) );
	}
	else 
	{	

		//If now max current of system < setting value, change it.
		if(fDefinedCurrValue > fInitMaxValue  )
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_MAX_CURR_LMT_POINT,
				fInitMaxValue);
			

			if(fRatedInput > 200.0)
			{
				GC_SetFloatValue(TYPE_OTHER, 
					0,
					CT_PUB_DEFAULT_POINT_INTERAL,
					GC_MAX_CURR_LMT_CONV);

			}
			else
			{
				if(fRatedInput > 35.0)
				{
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_DEFAULT_POINT_INTERAL,
						GC_MAX_CURR_LMT_CONV_24V);
				}
				else
				{
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_DEFAULT_POINT_INTERAL,
						GC_MAX_CURR_LMT_CONV);

				}
			}

			fDefinedCurrValue = fInitMaxValue;
			iRectNumber = iQtyOfRect;
			GC_SetDwordValue(TYPE_OTHER, 0, CT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}
		else if(fDefinedCurrValue < fInitMinValue )
		{
			GC_SetFloatValue(TYPE_OTHER, 
					0,
					CT_PUB_MAX_CURR_LMT_POINT,
					fInitMinValue);
			GC_SetFloatValue(TYPE_OTHER, 
					0,
					CT_PUB_DEFAULT_POINT_INTERAL,
					GC_MIN_CURR_LMT_IN_CONV);

			fDefinedCurrValue = fInitMinValue;
			iRectNumber = iQtyOfRect;
			GC_SetDwordValue(TYPE_OTHER, 0, CT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}

		float	fCalculateCurrValue = iQtyOfRect * fRatedCurr * GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_DEFAULT_POINT_INTERAL) / 100;
		//printf("GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_DEFAULT_POINT_INTERAL)  = %f\n", GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_DEFAULT_POINT_INTERAL) );
		//printf("iQtyOfRect = %d iRectNumber = %d fCalculateCurrValue = %f fDefinedCurrValue =%f\n",iQtyOfRect,iRectNumber, fCalculateCurrValue, fDefinedCurrValue);

		//if(FLOAT_EQUAL(fDefinedCurrValue, fCalculateCurrValue) == TRUE)
		//{
		//	return;

		//}

		if(iQtyOfRect > iRectNumber)
		{
			iRectNumber = iQtyOfRect;
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_MAX_CURR_LMT_POINT,
				fCalculateCurrValue);
			GC_SetDwordValue(TYPE_OTHER, 0, CT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}
		else if(iQtyOfRect < iRectNumber)
		{
			iRectNumber = iQtyOfRect;
			GC_SetDwordValue(TYPE_OTHER, 0, CT_PUB_SYS_CURR_LIMIT_RECT_NUM, (DWORD)iRectNumber);
		}
		else
		{
			float fSetInterValue = fDefinedCurrValue * 100/(iQtyOfRect * fRatedCurr);//�ڲ�ʹ��Ĭ��������
			if(fRatedInput > 200.0)
			{
				if(fSetInterValue > GC_MAX_CURR_LMT_CONV)
				{
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_DEFAULT_POINT_INTERAL,
						GC_MAX_CURR_LMT_CONV);

				}
				else if(fSetInterValue < GC_MIN_CURR_LMT_IN_CONV)
				{			
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_DEFAULT_POINT_INTERAL,
						GC_MIN_CURR_LMT_IN_CONV);		
				}
				else
				{
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						CT_PUB_DEFAULT_POINT_INTERAL,
						fSetInterValue);

				}

			}
			else
			{
				if(fRatedInput > 35.0)
				{
					if(fSetInterValue > GC_MAX_CURR_LMT_CONV_24V)
					{
						GC_SetFloatValue(TYPE_OTHER, 
							0,
							CT_PUB_DEFAULT_POINT_INTERAL,
							GC_MAX_CURR_LMT_CONV_24V);

					}
					else if(fSetInterValue < GC_MIN_CURR_LMT_IN_CONV)
					{			
						GC_SetFloatValue(TYPE_OTHER, 
							0,
							CT_PUB_DEFAULT_POINT_INTERAL,
							GC_MIN_CURR_LMT_IN_CONV);		
					}
					else
					{
						GC_SetFloatValue(TYPE_OTHER, 
							0,
							CT_PUB_DEFAULT_POINT_INTERAL,
							fSetInterValue);

					}
				}
				else
				{
					if(fSetInterValue > GC_MAX_CURR_LMT_CONV)
					{
						GC_SetFloatValue(TYPE_OTHER, 
							0,
							CT_PUB_DEFAULT_POINT_INTERAL,
							GC_MAX_CURR_LMT_CONV);

					}
					else if(fSetInterValue < GC_MIN_CURR_LMT_IN_CONV)
					{			
						GC_SetFloatValue(TYPE_OTHER, 
							0,
							CT_PUB_DEFAULT_POINT_INTERAL,
							GC_MIN_CURR_LMT_IN_CONV);		
					}
					else
					{
						GC_SetFloatValue(TYPE_OTHER, 
							0,
							CT_PUB_DEFAULT_POINT_INTERAL,
							fSetInterValue);

					}
				}
			}
			
		}
	}
	//GC_SetFloatValue(TYPE_OTHER, 
	//	0,
	//	CT_PUB_MAX_CURR_LMT_SAMPLE,
	//	pRtRunInfo->fMaxCurrLmtPoint);
	float fMaxLmtCurr = GC_GetFloatValue(TYPE_OTHER, 0, CT_PUB_MAX_CURR_LMT_POINT);
	//printf("fMaxLmtCurr = %f\n", fMaxLmtCurr);
	int iConvQty = GC_GetDwordValue(TYPE_OTHER, 0, CT_PUB_QTY_AC_ON_COMM);
	if(iConvQty > 0)
	{
		fMaxLmtCurr = fMaxLmtCurr/ iConvQty;
	}

	//printf("fMaxLmtCurr = %f\n", fMaxLmtCurr);
	float fSendCurr;
	if(fRatedInput > 200.0)
	{
		if(fMaxLmtCurr > fRatedCurr * GC_MAX_CURR_LMT_CONV/100)
		{
			fSendCurr = fRatedCurr * GC_MAX_CURR_LMT_CONV/100;
		}
		else if(fMaxLmtCurr < fRatedCurr * GC_MIN_CURR_LMT_IN_CONV/100)
		{
			fSendCurr = fRatedCurr * GC_MIN_CURR_LMT_IN_CONV/100;
		}
		else
		{
			fSendCurr = fMaxLmtCurr;
		}

	}
	else
	{
		if(fRatedInput > 35.0)
		{
			if(fMaxLmtCurr > fRatedCurr * GC_MAX_CURR_LMT_CONV_24V/100)
			{
				fSendCurr = fRatedCurr * GC_MAX_CURR_LMT_CONV_24V/100;
			}
			else if(fMaxLmtCurr < fRatedCurr * GC_MIN_CURR_LMT_IN_CONV/100)
			{
				fSendCurr = fRatedCurr * GC_MIN_CURR_LMT_IN_CONV/100;
			}
			else
			{
				fSendCurr = fMaxLmtCurr;
			}
		}
		else
		{
			if(fMaxLmtCurr > fRatedCurr * GC_MAX_CURR_LMT_CONV/100)
			{
				fSendCurr = fRatedCurr * GC_MAX_CURR_LMT_CONV/100;
			}
			else if(fMaxLmtCurr < fRatedCurr * GC_MIN_CURR_LMT_IN_CONV/100)
			{
				fSendCurr = fRatedCurr * GC_MIN_CURR_LMT_IN_CONV/100;
			}
			else
			{
				fSendCurr = fMaxLmtCurr;
			}

		}
	}
	

	GC_SendCTCurrentLimit(100 * fSendCurr / fRatedCurr);
	/*GC_SetFloatValue(TYPE_OTHER, 
		0,
		CT_PUB_CTL_CURR,
		100 * fSendCurr / fRatedCurr);*/

	//printf("fSendCurr = %f\n", fSendCurr);
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		CT_PUB_MAX_CURR_LMT_SAMPLE,
		fSendCurr);

	if(fRatedInput > 200.0)
	{
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			CT_PUB_MAX_CURR_LMT_VALUE,
			iQtyOfRect * fRatedCurr * GC_MAX_CURR_LMT_CONV/ 100);


	}
	else
	{
		if(fRatedInput > 35.0)
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_MAX_CURR_LMT_VALUE,
				iQtyOfRect * fRatedCurr * GC_MAX_CURR_LMT_CONV_24V/ 100);

		}
		else
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				CT_PUB_MAX_CURR_LMT_VALUE,
				iQtyOfRect * fRatedCurr * GC_MAX_CURR_LMT_CONV/ 100);


		}
	}


	
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		CT_PUB_MIN_CURR_LMT_SAMPLE,
		iQtyOfRect * fRatedCurr *GC_MIN_CURR_LMT_IN_CONV  / 100);
	return;

}
/*==========================================================================*
 * FUNCTION : IsLvdSigVaild
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdxLvdEquip : 
 *            int  iIdxLvdSig   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-09-20 11:20
 *==========================================================================*/
static BOOL	IsLvdSigVaild(int iIdxLvdEquip, int iIdxLvdSig)
{
	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;
	
	if(GC_EQUIP_EXIST != GC_IsGCEquipExist(TYPE_LVD_UNIT, iIdxLvdEquip, LVD_EQUIP_STATE,GC_EQUIP_INVALID))
	{
		return FALSE;
	}

	iRst = GC_GetSigVal(TYPE_LVD_UNIT, 
					iIdxLvdEquip,
					LVD1_PRI_CTL + iIdxLvdSig,
					&pSigValue);
	if(iRst != ERR_OK)
	{		
		return FALSE;	
	}

	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_GET_VLAUE, 
			"GC_GetSigVal LVD1_PRI_CTL error!\n");
	

	return SIG_VALUE_IS_VALID(pSigValue);
}



/*==========================================================================*
 * FUNCTION : LvdRunInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-09-20 11:19
 *==========================================================================*/
static void	LvdRunInfo(void)
{
	int					i;
	float				fLVDQuantity = 0.0;
	
	//G3_OPT [loader], NOT use the maximun configuration, modified by Lin.Tao.Thomas, 2013-4
	//for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	//int iCfgQtyOfLvd = g_pGcData->EquipInfo.iCfgQtyOfLvd;
	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{
		//if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD_EQUIP_STATE,GC_EQUIP_INVALID))
		//Move the judgement into IsLvdSigValid,to avoid the no assignment of abValid.
		{
			//There are 2 LVD in a LVD equipment
			g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0] = IsLvdSigVaild(i, 0);
			g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1] = IsLvdSigVaild(i, 1);

			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
			{
				g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0] 
					= GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD1_PRI_CTL,GC_EQUIP_INVALID);//GC_GetEnumValue(TYPE_LVD_UNIT, i, LVD1_PRI_CTL);
 			}
	
			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
			{
				g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1] 
					= GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD2_PRI_CTL,GC_EQUIP_INVALID);//GC_GetEnumValue(TYPE_LVD_UNIT, i, LVD2_PRI_CTL); 
			}
		}
		
	}
	
	//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	//for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	if (GC_EQUIP_EXIST !=GC_IsGCEquipExist(TYPE_OTHER, 0, LDU_PUB_EXISTENCE,GC_EQUIP_INVALID))
	{
		//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
		//for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{
			//if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD_EQUIP_STATE,GC_EQUIP_INVALID))
			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0]
				&& (GC_ENABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD1_PRI_ENB,GC_EQUIP_INVALID)))
			{
				fLVDQuantity = fLVDQuantity + 1.0;
			}

			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1]
				&& (GC_ENABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD2_PRI_ENB,GC_EQUIP_INVALID)))
			{
				fLVDQuantity = fLVDQuantity + 1.0;
			}

		}		
	}
	else
	{
		//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
		//for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{
			//if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD_EQUIP_STATE,GC_EQUIP_INVALID))
			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0]
				&& (GC_ENABLED == GC_IsGCEquipExist(TYPE_OTHER, 0,LDU_LVD1_PRI_ENB,GC_EQUIP_INVALID)))
			{
				fLVDQuantity = fLVDQuantity + 1.0;
			}

			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1]
				&& (GC_ENABLED == GC_IsGCEquipExist(TYPE_OTHER, 0,LDU_LVD2_PRI_ENB,GC_EQUIP_INVALID)))
			{
				fLVDQuantity = fLVDQuantity + 1.0;
			}
		}

	}


	GC_SetFloatValue(TYPE_OTHER, 0, GC_PUB_LVD_QUANTITY, fLVDQuantity);
	return;
}


#define	USED_BY_BATT_MGMT		0
#define	NOT_USED_BY_BATT_MGMT	1
/*==========================================================================*
 * FUNCTION : IsBattValid
 * PURPOSE  : To judge the battery is valid
 * CALLS    : GC_GetSigVal
 * CALLED BY: BattRunInfo
 * ARGUMENTS: int  iBattNo : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:50
 *==========================================================================*/
static BOOL	IsBattValid(int iBattNo)
{

	return GC_IsBattValid(TYPE_BATT_UNIT, iBattNo, 0);

	//return GC_IsSigValid(TYPE_BATT_UNIT, iBattNo, BT_PRI_CURR);
	
}


/*==========================================================================*
 * FUNCTION : IsBattVoltValid
 * PURPOSE  : To judge if the battery voltage signal is valid
 * CALLS    : GC_GetSigVal
 * CALLED BY: BattRunInfo
 * ARGUMENTS: int  iBattNo : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:50
 *==========================================================================*/

/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-5 17:00:25
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
BOOL IsWorkIn_LiBattMode()
{
	//return (g_pGcData->RunInfo.Bt.iSysBattType > 0) ? TRUE : FALSE;
	//�������۾���ֻ�����Ƿ����������ļ�����������״̬
	return (g_pGcData->EquipInfo.iCfgQtyOfLiBatt > 0) ? TRUE:FALSE;
}
static BOOL	IsBattVoltValid(int iBattNo)
{
	return GC_IsSigValid(TYPE_BATT_UNIT, iBattNo, BT_PRI_VOLT);
}


/*==========================================================================*
 * FUNCTION : GC_CmpBattCurrAndFcToBcCurr
 * PURPOSE  : To judge if one of Battery Current is greater than FC to BC  
			  Current.
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-05 14:17
 *==========================================================================*/
#define	GC_FC_TO_BC_DELAY	3	//min
static BOOL CmpBattCurrAndFcToBcCurr(void)
{
	int		i;
	float	fFcToBcCurr = 0.0,fRatedCap;
	BOOL	bExceed = FALSE,bRTN;

	/*fFcToBcCurr 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_FC_TO_BC_CURR)
		* GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_RATED_CAP);*/
	
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		//modified by Jimmy 20120508
		if(GC_Is_ABatt_Li_Type(i))
		{
			fFcToBcCurr = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_FC_TO_BC_CURR_LIBATT);
				
		}
		else
		{
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
			if(bRTN == FALSE)
			{
				continue;
			}
			fFcToBcCurr = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_FC_TO_BC_CURR)* fRatedCap;
		}
		//end modified

		if(g_pGcData->RunInfo.Bt.abValid[i] 
			&& g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			if(g_pGcData->RunInfo.Bt.afBattCurr[i] > fFcToBcCurr)
			{
				bExceed = TRUE;
				break;
			}
		}
	}

	if(bExceed)
	{
		if(SEC_TMR_SUSPENDED 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_FC_TO_BC_DELAY))
		{
			GC_SetSecTimer(SEC_TMR_ID_FC_TO_BC_DELAY, 
						GC_FC_TO_BC_DELAY * SECONDS_PER_MIN,
						NULL,
						0);
		}
		if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_FC_TO_BC_DELAY))
		{
			return TRUE;
		}
	}
	else
	{
		GC_SuspendSecTimer(SEC_TMR_ID_FC_TO_BC_DELAY);
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : IsBattDisch
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-06-16 15:30
 *==========================================================================*/
static BOOL IsBattDisch(void)
{
	int			i;
	BOOL		bDisch = FALSE,bRTN;
	static int	s_iCounter = 0;
	float		fRatedCap;

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i]
			&& g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			//float fMinCurr = MIN_CURR_COEF
			//	* GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_RATED_CAP);
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
			if(bRTN == FALSE)
			{
				continue;
			}
			float fMinCurr = MIN_CURR_COEF*fRatedCap;

			if((0.0 - fMinCurr)
				> g_pGcData->RunInfo.Bt.afBattCurr[i])
			{
				bDisch = TRUE;
				break;
			}
		}
	}
	
	if(bDisch)
	{
		if(s_iCounter > 4)
		{
			return TRUE;
		}
		else
		{
			s_iCounter++;
			return FALSE;
		}
	}
	else
	{
		s_iCounter = 0;
		return FALSE;
	}
}






/*********************************************************************************
*  
*  FUNCTION NAME : Refresh_LiBattInfo
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-5 19:30:18
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Refresh_LiBattInfo()
{
	//BOOL				bResetBattCap;
	int				i = 0;
	static int			iRunTime = 0;
	static BOOL			bFlag = FALSE;
	BOOL				bRTN;
#ifdef	_GEN_CTL_FOR_CHN
	float				fDeltaCap;
#endif
	g_pGcData->RunInfo.Bt.bZeroCurr = TRUE;

	g_pGcData->RunInfo.Bt.iQtyValidBatt = 0; //��ʼ��

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(GC_Is_ABatt_Li_Type(i))
		{
			g_pGcData->RunInfo.Bt.abValid[i] = IsBattValid(i);
		}
		else
		{
			g_pGcData->RunInfo.Bt.abValid[i] = FALSE;
		}		
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&g_pGcData->RunInfo.Bt.afBattRatedCap[i]);

		if((!(g_pGcData->RunInfo.Bt.abValid[i]))||(bRTN == FALSE))
		{
			continue;
		}
		else
		{

			float fMinCurr = MIN_CURR_COEF
				* GC_GetFloatValue(TYPE_BATT_UNIT, i ,BT_PRI_RATED_CAP);

			//BlkVoltDiffAlm(i);

			//Quantity of valid batteries�����ܰ�װ�ĵ���������㼴�ɣ�������Ĵ���
			//g_pGcData->RunInfo.Bt.iQtyValidBatt++;

			g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i] = TRUE; //ʹ�õ�ع������������û�����
			//= GC_GetEnumValue(TYPE_BATT_UNIT, i, BT_PRI_USED_BY_BM) ? FALSE : TRUE;
		//�������������������ڲɼ����Ѿ�����ƽ����ѹ���ܵ��������Բ����ظ�����
			//Battery current
			g_pGcData->RunInfo.Bt.afBattCurr[i]
			= GC_GetFloatValue(TYPE_BATT_UNIT, i, BT_PRI_CURR);

			////Sum of Current
			//if(g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
			//{
			//	g_pGcData->RunInfo.Bt.fSumOfCurr 
			//		+= g_pGcData->RunInfo.Bt.afBattCurr[i];
			//}

			//Discharge flag and discharge capacity
			if((0.0 - fMinCurr)
				> g_pGcData->RunInfo.Bt.afBattCurr[i])
			{
#ifdef	_GEN_CTL_FOR_CHN
				fDeltaCap = g_pGcData->RunInfo.Bt.afBattCurr[i] 
				* GC_INTERVAL_REFRESH_DATA / SECONDS_PER_HOUR;
#endif
				g_pGcData->RunInfo.Bt.bZeroCurr = FALSE;
			}
#ifdef	_GEN_CTL_FOR_CHN
			else
			{
				fDeltaCap = 0;
			}
#endif

			if(fMinCurr
				< g_pGcData->RunInfo.Bt.afBattCurr[i])
			{
				g_pGcData->RunInfo.Bt.bZeroCurr = FALSE;
			}

#ifdef	_GEN_CTL_FOR_CHN
			//fDisCap is initialized when battery test start
			g_pGcData->RunInfo.Bt.afDisCap[i] -= fDeltaCap;
			g_pGcData->RunInfo.Bt.fTotalDisCap 
				+= g_pGcData->RunInfo.Bt.afDisCap[i];
#endif

		}
	}

	g_pGcData->RunInfo.Bt.bOverFc2BcCurr = CmpBattCurrAndFcToBcCurr();
	//
	//bResetBattCap = FALSE;//GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_RST_BATT_CAP);
	//if(bResetBattCap)
	//{
	//	GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_RST_BATT_CAP, FALSE, TRUE);
	g_pGcData->RunInfo.Bt.bNeedRegulateCap = FALSE;
	//}

	g_pGcData->RunInfo.Bt.bDischarge = IsBattDisch();
	GC_SetEnumValue(TYPE_OTHER, 
		0,
		BT_PUB_BATT_DISCH_SET, 
		g_pGcData->RunInfo.Bt.bDischarge,
		TRUE);

	//g_pGcData->RunInfo.Bt.iValidVoltNum = iValidVoltNum;
	//if(iValidVoltNum)
	
	//ƽ����ѹ
	g_pGcData->RunInfo.Bt.fVolt = GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_AVG_VOLT);//fSumOfBattVolt / iValidVoltNum;
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		BT_PUB_AVERAGE_VOLT, 
		g_pGcData->RunInfo.Bt.fVolt);
	//�ܵ���
	g_pGcData->RunInfo.Bt.fSumOfCurr = GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_TOTAL_CURR);
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		BT_PUB_TOTAL_CURR, 
		g_pGcData->RunInfo.Bt.fSumOfCurr);
	//ƽ���¶�
	g_pGcData->RunInfo.Bt.fAvgTemp = GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_AVG_TEMP);
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_LI_AVGTEMP, 
		g_pGcData->RunInfo.Bt.fAvgTemp);
	//Installed�����Ŀ
	DWORD dwTempNum = 0;
	dwTempNum= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_INST_BATT_NUM);
	GC_SetDwordValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_INST_BATT_NUM, 
		dwTempNum);
	//ע������Ӧ��ʹ������ͨ����disconnected״̬ΪFALSE�ĵ�أ���Ϊ��ʹ��ض��ˣ�Bridge Card���ǻ���ACU+ͨ��
	//g_pGcData->RunInfo.Bt.iQtyValidBatt = dwTempNum;
	//Disconnected�����Ŀ
	dwTempNum = 0;
	dwTempNum= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_DISCONN_BATT_NUM);
	GC_SetDwordValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_DISCONN_BATT_NUM, 
		dwTempNum);
	//NoReply�����Ŀ
	dwTempNum = 0;
	dwTempNum= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_NOREPLY_BATT_NUM);
	GC_SetDwordValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_NOREPLY_BATT_NUM, 
		dwTempNum);
	//���¼��㡮��Ч�������Ŀ��������������
	// = �ܰ�װ���-disconnected���-no reply���
	//---------------------------------begin (Jimmy 2012/07/27)
	g_pGcData->RunInfo.Bt.iQtyValidBatt = \
		GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_INST_BATT_NUM) \
		- GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_DISCONN_BATT_NUM) \
		- GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_NOREPLY_BATT_NUM);
	if(g_pGcData->RunInfo.Bt.iQtyValidBatt < 0)
	{
		g_pGcData->RunInfo.Bt.iQtyValidBatt = 0;
	}
	//-------------------------------end
	//Inventory update Flag״̬
	BOOL bFlagUpdate = FALSE;
	bFlagUpdate= (GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_INVENTORY_UPDATE_FLAG) > 0) ? TRUE: FALSE;
	GC_SetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_INVENTORY_UPDATE_FLAG, 
		bFlagUpdate,TRUE);

	if(g_pGcData->RunInfo.Bt.iCmpCapDelay < 30)
	{
		g_pGcData->RunInfo.Bt.iCmpCapDelay++;
	}

	if(bFlag == FALSE)
	{
		if(iRunTime >= 30)
		{
			BattTestLogInfoInit();
			bFlag = TRUE;
		}
		else
		{
			iRunTime++;
		}

	}

	return;
}
#define	MIN_VALID_BATT_VOLT	16.0
#define	MAX_VALID_BATT_VOLT	80.0

/*==========================================================================*
 * FUNCTION : BattRunInfo
 * PURPOSE  : To refresh batteries run infomation
 * CALLS    : IsBattValid GC_GetFloatValue
 * CALLED BY: GC_RunInfoRefresh
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:48
 *==========================================================================*/
static void	BattRunInfo(void)
{
	//added by Jimmy 2012/04/05, is Li Batt or not??
	//**********************************************************************
	//��������������������ע�� �������۾���ֱ���ڱ�׼�汾��ע�͵�Li��������ļ�����˲����жϹ���״̬������ֱ�Ӹ������������ļ������ж�
	//static int iRefresh_LiTypeTimes = 0;
	//if(g_pGcData->EquipInfo.iCfgQtyOfLiBatt >0) //ֻ���������ļ����ڵ�����²���Ϊ�п��ܻ���Li��ؽ���
	//{
	//	if(iRefresh_LiTypeTimes == 10)
	//	{
	//		g_pGcData->RunInfo.Bt.iSysBattType 
	//			= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_IS_LI_BATT_TYPE);
	//	}
	//}
	//else
	//{
	//	g_pGcData->RunInfo.Bt.iSysBattType = 0; //Ĭ��Ϊ��ͳ���
	//}
	//iRefresh_LiTypeTimes++;
	//if(iRefresh_LiTypeTimes > 30)
	//{
	//	iRefresh_LiTypeTimes = 0;
	//}
	//����Li��ػ����ȹ�������û��ɨ��Li��ص�ʱ���תΪ��ͳģʽ������ˢ��Li�������
	if (IsWorkIn_LiBattMode())
	{
		//printf("***\nNow work in Li-Batt Mode\n");
		Refresh_LiBattInfo();
		return;
	}
	else
	{
		//printf("***\nNow work in Traditional-Batt Mode\n");
		//Go on with existed logic
	}
	//************************************************************************
	int					i;
	int					iValidVoltNum = 0;
	float				fSumOfBattVolt = 0;
#ifdef	_GEN_CTL_FOR_CHN
	float				fDeltaCap;
#endif
	BOOL				bResetBattCap;

	static int			iRunTime = 0;
	static BOOL			bFlag = FALSE;
	 BOOL				bRTN;
	 float				fRatedCap;
	float				fMinCapacity =100.0, fCurrentCapacity=0.0,fMaxCurrent = 0.0;
	static int			iSleepTimeXs = 0;
	BOOL				bExistBattery = FALSE;
	
	g_pGcData->RunInfo.Bt.fSumOfCurr = 0;

	g_pGcData->RunInfo.Bt.iQtyValidBatt = 0;

#ifdef	_GEN_CTL_FOR_CHN
	g_pGcData->RunInfo.Bt.fTotalDisCap = 0;
#endif
	g_pGcData->RunInfo.Bt.bZeroCurr = TRUE;
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(IsBattVoltValid(i))//ֻ���ڵ�������ļ��вɼ��ź�IDλ1��λ�����˵�ص�ѹ�������вɼ��������ѹ�źϷ�
		{
			
			float	fBattVolt;
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_VOLT,&fBattVolt);
			if(bRTN == FALSE)
			{

				continue;
			}
			if((fBattVolt > MIN_VALID_BATT_VOLT) 
				&& (fBattVolt < MAX_VALID_BATT_VOLT))
			{
				iValidVoltNum++;
				fSumOfBattVolt += fBattVolt;
			}
		}
		
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
		if(bRTN == FALSE)
		{

			continue;
		}

		g_pGcData->RunInfo.Bt.abValid[i] = IsBattValid(i);

		//printf("BattRunInfo(): g_pGcData->EquipInfo.aiEquipIdBtPri[%d] =%d,g_pGcData->RunInfo.Bt.abValid[%d]=%d~~~~~\n",i,g_pGcData->EquipInfo.aiEquipIdBtPri[i],i,g_pGcData->RunInfo.Bt.abValid[i]);

		
		g_pGcData->RunInfo.Bt.afBattRatedCap[i] = fRatedCap;

		if(!(g_pGcData->RunInfo.Bt.abValid[i]))
		{
			continue;
		}
		else
		{
			
			bExistBattery = TRUE;
			////TRACE("%d is valid battery\n", i);
			float fMinCurr = MIN_CURR_COEF* fRatedCap;

			if(!((g_pGcData->EquipInfo.aiEquipIdBtPri[i] >= 680) && (g_pGcData->EquipInfo.aiEquipIdBtPri[i] <= 699)))
			{

				BlkVoltDiffAlm(i);
			}

			//g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i] 
			//= GC_GetEnumValue(TYPE_BATT_UNIT, i, BT_PRI_USED_BY_BM) ? FALSE : TRUE;
			if(!GC_Is_ABatt_Li_Type(i))
			{
				//g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i] 
				//= GC_GetEnumValue(TYPE_BATT_UNIT, i, BT_PRI_USED_BY_BM) ? FALSE : TRUE;
				g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i] 
				=(GC_BT_NOUSED_BY_BM == GC_IsGCEquipExist(TYPE_BATT_UNIT, i, BT_PRI_USED_BY_BM,GC_BT_INVALIAD)) ? FALSE : TRUE;
				//Quantity of valid batteries
				g_pGcData->RunInfo.Bt.iQtyValidBatt++;
			}
			else
			{
				g_pGcData->RunInfo.Bt.abValid[i]  = FALSE;
				g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i]  = FALSE; //��׼���
			}

			//Battery current
			g_pGcData->RunInfo.Bt.afBattCurr[i]
				= GC_GetFloatValue(TYPE_BATT_UNIT, i, BT_PRI_CURR);

			//Sum of Current
			if(g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
			{
				g_pGcData->RunInfo.Bt.fSumOfCurr 
					+= g_pGcData->RunInfo.Bt.afBattCurr[i];
				if(GC_IsGCSigExist(TYPE_BATT_UNIT, i, BT_PRI_CAP_PERCENT))
				{
					fCurrentCapacity = GC_GetFloatValue(TYPE_BATT_UNIT, i, BT_PRI_CAP_PERCENT);
				}
				if(fMinCapacity > fCurrentCapacity)
				{
					fMinCapacity = fCurrentCapacity;
				}
				if(fMaxCurrent < g_pGcData->RunInfo.Bt.afBattCurr[i])
				{
					fMaxCurrent = g_pGcData->RunInfo.Bt.afBattCurr[i];
				}
			}
			
			//Discharge flag and discharge capacity
			if((0.0 - fMinCurr)
				> g_pGcData->RunInfo.Bt.afBattCurr[i])
			{
#ifdef	_GEN_CTL_FOR_CHN
				fDeltaCap = g_pGcData->RunInfo.Bt.afBattCurr[i] 
						* GC_INTERVAL_REFRESH_DATA / SECONDS_PER_HOUR;
#endif
				g_pGcData->RunInfo.Bt.bZeroCurr = FALSE;
			}
#ifdef	_GEN_CTL_FOR_CHN
			else
			{
				fDeltaCap = 0;
			}
#endif

			if(fMinCurr
				< g_pGcData->RunInfo.Bt.afBattCurr[i])
			{
				g_pGcData->RunInfo.Bt.bZeroCurr = FALSE;
			}

#ifdef	_GEN_CTL_FOR_CHN
			//fDisCap is initialized when battery test start
			g_pGcData->RunInfo.Bt.afDisCap[i] -= fDeltaCap;
			g_pGcData->RunInfo.Bt.fTotalDisCap 
				+= g_pGcData->RunInfo.Bt.afDisCap[i];
#endif

		}
	}

	g_pGcData->RunInfo.Bt.bOverFc2BcCurr = CmpBattCurrAndFcToBcCurr();

	bResetBattCap = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_RST_BATT_CAP);
	if(bResetBattCap)
	{
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_RST_BATT_CAP, FALSE, TRUE);
		g_pGcData->RunInfo.Bt.bNeedRegulateCap = TRUE;
		printf("static void TimerResetCap()\n");
	}

	g_pGcData->RunInfo.Bt.bDischarge = IsBattDisch();
	GC_SetEnumValue(TYPE_OTHER, 
					0,
					BT_PUB_BATT_DISCH_SET, 
					g_pGcData->RunInfo.Bt.bDischarge,
					TRUE);

	g_pGcData->RunInfo.Bt.iValidVoltNum = iValidVoltNum;
	if(iValidVoltNum)
	{
		g_pGcData->RunInfo.Bt.fVolt = fSumOfBattVolt / iValidVoltNum;
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					BT_PUB_AVERAGE_VOLT, 
					g_pGcData->RunInfo.Bt.fVolt);
	}
	else
	{
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					BT_PUB_AVERAGE_VOLT, 
					0.0);
	}
	g_pGcData->RunInfo.Bt.fSumOfCurr += GC_GetFiammBatteryCurr();
	GC_SetFloatValue(TYPE_OTHER, 
					0,
					BT_PUB_TOTAL_CURR, 
					g_pGcData->RunInfo.Bt.fSumOfCurr);

	if(g_pGcData->RunInfo.Bt.iCmpCapDelay < 30)
	{
		g_pGcData->RunInfo.Bt.iCmpCapDelay++;
	}

	if(bFlag == FALSE)
	{
		if(iRunTime >= 30)
		{
			BattTestLogInfoInit();

			bFlag = TRUE;

		}
		else
		{
			iRunTime++;
		}

	}
	if(GC_IsGCSigExist(TYPE_OTHER, 
							0,
							BT_PUB_LOWER_CAPACITY))
	{	
		if(iSleepTimeXs > 20 ||fMinCapacity > 0.1)
		{		
			if(!bExistBattery)
			{
				GC_SetFloatValue(TYPE_OTHER, 
							0,
							BT_PUB_LOWER_CAPACITY, 
							100.0);
				
			}
			else
			{
				GC_SetFloatValue(TYPE_OTHER, 
							0,
							BT_PUB_LOWER_CAPACITY, 
							fMinCapacity);
				//iSleepTimeXs = 20;
			}		
		
			
		}
	}	

	iSleepTimeXs++;
	if(GC_IsGCSigExist(TYPE_OTHER,0,BT_PUB_MAX_CHARGE_CURRENT))
	{
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					BT_PUB_MAX_CHARGE_CURRENT, 
					fMaxCurrent);
	}
	return;
}

/*==========================================================================*
 * FUNCTION : Filter_MidAve
 * PURPOSE  : Middle average filter
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: float    Input        : 
 *            float *  FIFO         : 
 *            int *    FIFO_Head    : 
 *            int      SizeOfBuffer : 
 *            int      SizeOfMiddle : 
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-05-08 13:28
 *==========================================================================*/
static float Filter_MidAve(const float fInput, 
						   float* pfFIFO,
						   float* pfSort_Buffer,
						   int* piFIFO_Head,
						   const int iSizeOfBuffer,
						   const int iSizeOfMiddle)

{
	int		i , j;
	float	fSum = 0.0;
    
	//Put the input into the array
	*(pfFIFO + (*piFIFO_Head)) = fInput;
	(*piFIFO_Head)++;
	if((*piFIFO_Head) >= iSizeOfBuffer)
	{
		*piFIFO_Head = 0;
	}

	//TRACE("GC:*** pfFIFO = ");

    //Put the array into buffer
	for (i = 0; i < iSizeOfBuffer; i++)
	{
       *(pfSort_Buffer + i) = *(pfFIFO + i);
		//TRACE("   %.02f", *(pfFIFO + i));
	}
	//TRACE("\n");

    //effervescing sort
    for (i = 0; i < (iSizeOfBuffer - 1); i++)
	{
		for (j = 0; j < (iSizeOfBuffer - 1 - i); j++)
		{
			if((*(pfSort_Buffer + j)) > ( *(pfSort_Buffer + j + 1)))
			{
				float fTemp = *(pfSort_Buffer + j + 1);
				*(pfSort_Buffer + j + 1) = *(pfSort_Buffer + j);
				*(pfSort_Buffer + j) = fTemp;
			}
		}
	}

	//TRACE("GC:*** pfSort_Buffer = ");

	/*for (i = 0; i < iSizeOfBuffer; i++)
	{
		TRACE("   %.02f", *(pfSort_Buffer + i));
	}
	TRACE("\n");*/

    //middle average
    fSum = 0;
    for (i = 0; i < iSizeOfMiddle; i++)
	{
		fSum += (*(pfSort_Buffer + ((iSizeOfBuffer - iSizeOfMiddle) / 2 + i)));
    }
	
	//TRACE("GC:*** fSum = %.02f\n\n\n", fSum);

    return (fSum / iSizeOfMiddle);
}




/*==========================================================================*
 * FUNCTION : GetSysLoad
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-15 12:15
 *==========================================================================*/
#define	SIZE_SYS_LOAD_BUF	3
#define	SIZE_SYS_LOAD_MID	1
static float GetSysLoad(float  fSysLoad)
{
	float			fLoad = 0.0;
	//_________________added by Jimmy 2013.07.10
	if(IsSMDU_EIB_Mode())
	{
		SIG_BASIC_VALUE*	pSigValue;
		SIG_BASIC_VALUE*	pSigLoadEnableValue;
		//�������
		int iRst = GC_GetSigVal(TYPE_OTHER, 
						0,
						GC_PUB_TOTAL_MB_LOADS,
						&pSigValue);
		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal GC_PUB_TOTAL_MB_LOADS error!\n");

		iRst = GC_GetSigVal(TYPE_OTHER, 
						0,
						GC_PUB_LOAD_ENABLE,
						&pSigLoadEnableValue);

		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal GC_PUB_LOAD_ENABLE error!\n");

		//printf("-------1---------\n");
		if(SIG_VALUE_IS_VALID(pSigValue))
		{
			//printf("-------2---pSigLoadEnableValue->varValue.enumValue = %d------\n", pSigLoadEnableValue->varValue.enumValue);
			if(SIG_VALUE_IS_VALID(pSigLoadEnableValue)&& pSigLoadEnableValue->varValue.enumValue)
			{

				fLoad += pSigValue->varValue.fValue;
			}
		}
		//SMDU����
		iRst = GC_GetSigVal(TYPE_OTHER, 
						0,
						GC_PUB_TOTAL_SMDU_LOADS,
						&pSigValue);
		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal SMDU_EIB_PARAMMM error!\n");
		
		if(SIG_VALUE_IS_VALID(pSigValue))
		{
			fLoad += pSigValue->varValue.fValue;
		}

		//SMDUE����
		iRst = GC_GetSigVal(TYPE_OTHER, 
						0,
						GC_PUB_TOTAL_SMDUE_LOADS,
						&pSigValue);
		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal SMDUE_EIB_PARAMMM error!\n");
		
		if(SIG_VALUE_IS_VALID(pSigValue))
		{
			fLoad += pSigValue->varValue.fValue;
		}
		//SMDUP����
		iRst = GC_GetSigVal(TYPE_OTHER, 
						0,
						GC_PUB_TOTAL_SMDUP_LOADS,
						&pSigValue);
		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal SMDUP_EIB_PARAMMM error!\n");
		
		if(SIG_VALUE_IS_VALID(pSigValue))
		{
			fLoad += pSigValue->varValue.fValue;
		}

		
		//EIB����
		iRst = GC_GetSigVal(TYPE_OTHER, 
						0,
						GC_PUB_TOTAL_EIB_LOADS,
						&pSigValue);
		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal SMDU_EIB_PARAMMM error!\n");
		
		if(SIG_VALUE_IS_VALID(pSigValue))
		{
			fLoad += pSigValue->varValue.fValue;
		}
//printf("GetSysLoad(): fLoad=%f~~~~~~~~~~~~~~~~0\n",fLoad);
		
		GC_SetFloatValue(TYPE_OTHER, 
					0, 
					GC_PUB_TOTAL_LOAD_CURR, 
					fLoad);
		//return fLoad;
		//Jimmy 2013.11.18,���߱���Brian��Ҫ���ܸ��ػ��Ǹ���rect curr - batt curr���㣬
		//����ֻ�Ǽ���DC Load Current

	}
	//___________________end adding
	int				i;
	static BOOL		s_bFirstRun = TRUE;
	static int		s_iBufHead = 0;
	static float	s_afLoad[SIZE_SYS_LOAD_BUF];
	float			afSortBuffer[SIZE_SYS_LOAD_BUF];
	static int		iCalcNo = 0;
	SIG_ENUM	stConvState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CONVERTER_ONLY_MODE);

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif

#ifdef GC_SUPPORT_MPPT
	if(stState != GC_MPPT_DISABLE)
	{
		fLoad = g_pGcData->RunInfo.Rt.fSumOfCurr + g_pGcData->RunInfo.Mt.fSumOfCurr 
				- g_pGcData->RunInfo.Bt.fSumOfCurr;
	}
	else
	{
		fLoad = g_pGcData->RunInfo.Rt.fSumOfCurr 
				- g_pGcData->RunInfo.Bt.fSumOfCurr;
	}
#else
	fLoad = g_pGcData->RunInfo.Rt.fSumOfCurr 
					- g_pGcData->RunInfo.Bt.fSumOfCurr;

#endif
	if(GC_ENABLED == stConvState )
	{
		fLoad = g_pGcData->RunInfo.Conv.fSumOfCurr;
		
	}

	if(s_bFirstRun)
	{
		for(i = 0; i < SIZE_SYS_LOAD_BUF; i++)
		{
			s_afLoad[i] = fLoad;	
		}

		s_bFirstRun = FALSE;
	}

	
	
	fLoad = Filter_MidAve(fLoad, 
						s_afLoad,
						afSortBuffer,
						&s_iBufHead, 
						SIZE_SYS_LOAD_BUF, 
						SIZE_SYS_LOAD_MID);

//printf("GetSysLoad(): fLoad=%f~~~~~~~~~~~~~~~~1\n",fLoad);

	if ((fLoad < 0.0)
		&& (!(g_pGcData->PriCfg.bSlaveMode)))
	{
		fLoad = 0.0;
//printf("GetSysLoad(): fLoad=%f~~~~~~~~~~~~~~~~2\n",fLoad);
		
	}

	if(FLOAT_EQUAL(fLoad, 0.0))
	{		
		if(iCalcNo < 3)
		{
			iCalcNo++;
			return fSysLoad;
		}
	}
	else
	{
		iCalcNo = 0;
	}
	
	return	fLoad;
}



enum FIXED_REALY_DEFINITION
{
	FIXED_REALY_POWER_ON = 0,
	FIXED_REALY_OA,
	FIXED_REALY_MA_CA,
};
#define	FIXED_REALY_OUTPUT_ENABLED		0
/*==========================================================================*
 * FUNCTION : FixedRelayCtl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-12-24 10:39
 *==========================================================================*/
static void FixedRelayCtl(void)
{
	VAR_VALUE_EX		SigValue;

	GC_SIG_INFO*		pSigInfo;

	SigValue.nSendDirectly = (int)FALSE;
	SigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	SigValue.pszSenderName = "GEN_CTRL";
	//SigValue.varValue.enumValue = enumVal;
	//BOOL bIBComFail=Is_IB_ComFail();
	static s_bLastIBComFail=FALSE;	      
	int   i=0;
	////IB1 ��ͨ�Żָ�	,�ָ�IB���  �����ź�ֵ Ϊopen״̬һ��
	//if(s_bLastIBComFail==TRUE
	//	&&bIBComFail==FALSE )
	//{
	//	for(i=0;i<8;i++)
	//	{
	//		GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_GENERATOR_ALARM+i,GC_HYBRID_RELAY_STATUS_OPEN);
	//	}
	//}

	//s_bLastIBComFail=   bIBComFail;
#ifdef   GC_SUPPORT_HIGHTEMP_DISCONNECT
	int iEquipID = 1;
	int iSigType = 1;
	int iSigID;

	iSigID = 13;//relay 1
	if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) &&
		(IsRectFail() || GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_OVER_VOLTAGE)))
	{
		SigValue.varValue.enumValue = GC_ALARM;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}
	else if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)))
	{
		SigValue.varValue.enumValue = GC_NORMAL;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}

	iSigID = 14;//relay 2
	if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) &&
		(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_A_HIGH)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_B_HIGH)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_C_HIGH)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_A_LOW)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_B_LOW)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_C_LOW)))
	{
		SigValue.varValue.enumValue = GC_ALARM;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}
	else if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)))
	{
		SigValue.varValue.enumValue = GC_NORMAL;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}

	iSigID = 15;//relay 3

	SigValue.varValue.enumValue = GC_ALARM;
	DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,
		DXI_MERGE_SIG_ID(iSigType, iSigID),
		sizeof(VAR_VALUE_EX),
		&SigValue,
		0);



	iSigID = 16;//relay 4
	if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) &&
		( GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE1_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE2_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE3_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE4_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_DISCHARGE)))
	{
		SigValue.varValue.enumValue = GC_ALARM;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);

	}
	else if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)))
	{
		SigValue.varValue.enumValue = GC_NORMAL;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}

	iSigID = 17;//relay 5
	if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) &&
		(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE1_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE2_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE3_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE4_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE5_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE6_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE7_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE8_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE9_FAULT)))
	{
		SigValue.varValue.enumValue = GC_ALARM;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);

	}
	else if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)))
	{
		SigValue.varValue.enumValue = GC_NORMAL;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}

	iSigID = 19;//relay 7

	SigValue.varValue.enumValue = GC_ALARM;
	DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,
		DXI_MERGE_SIG_ID(iSigType, iSigID),
		sizeof(VAR_VALUE_EX),
		&SigValue,
		0);




	iSigID = 18;//relay 6
	if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) && 
		(IsRectFail()||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AMB_TEMP_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_TEMP_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_A_HIGH)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_B_HIGH)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_C_HIGH)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_A_LOW)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_B_LOW)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AC_C_LOW)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_DISCHARGE)))
	{
		SigValue.varValue.enumValue = GC_ALARM;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}
	else if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)))
	{
		SigValue.varValue.enumValue = GC_NORMAL;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}

	iSigID = 20;//relay 8
	if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) && 
				(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AMB_TEMP_HIGH)||	
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AMB_TEMP_LOW)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_TEMP_HIGH)||	
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_TEMP_LOW)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_OVER_VOLTAGE)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE1_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE2_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE3_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE4_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE1_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE2_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE3_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE4_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE5_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE6_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE7_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE8_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE9_FAULT)||
				GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE10_FAULT)
				))
	{
		SigValue.varValue.enumValue = GC_ALARM;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);

	}
	else if((0 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) &&
		!(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AMB_TEMP_HIGH)||	
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AMB_TEMP_LOW)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_TEMP_HIGH)||	
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_TEMP_LOW)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_OVER_VOLTAGE)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE1_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE2_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE3_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_FUSE4_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE1_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE2_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE3_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE4_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE5_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE6_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE7_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE8_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE9_FAULT)||
		GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LOAD_FUSE10_FAULT)
		))
	{
		SigValue.varValue.enumValue = GC_NORMAL;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}

	iSigID = 20;//relay 8
	if((1 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) && (GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_MANU_TEST_ALARM)))
	{
		SigValue.varValue.enumValue = GC_ALARM;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}
	else if((1 == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_TYPE)) && !(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_BATT_MANU_TEST_ALARM)))
	{
		SigValue.varValue.enumValue = GC_NORMAL;
		DxiSetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			DXI_MERGE_SIG_ID(iSigType, iSigID),
			sizeof(VAR_VALUE_EX),
			&SigValue,
			0);


	}
#else
	
#ifdef GC_SUPPORT_RELAY_TEST	
	if(FIXED_REALY_OUTPUT_ENABLED 
		!= GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_FIXED_RELAY_ENB) 
		|| GC_DISABLED != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_RELAY_TEST))
#else
	if(FIXED_REALY_OUTPUT_ENABLED 
	   != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_FIXED_RELAY_ENB))
#endif
	{
		
		return;
	}
	
	pSigInfo = g_pGcData->PriCfg.pFixedRelayOutput + FIXED_REALY_POWER_ON;
	if(pSigInfo->bEnabled)
	{
		SigValue.varValue.enumValue = GC_ALARM;
		DxiSetData(VAR_A_SIGNAL_VALUE,
				pSigInfo->iEquipId,
				DXI_MERGE_SIG_ID(pSigInfo->iSignalType, pSigInfo->iSignalId),
				sizeof(VAR_VALUE_EX),
				&SigValue,
				0);
	}

	pSigInfo = g_pGcData->PriCfg.pFixedRelayOutput + FIXED_REALY_OA;
	if(pSigInfo->bEnabled)
	{
		if((GC_GetAlarmNum(ALARM_LEVEL_OBSERVATION) > 0)
			&& (!(GetSiteInfo()->bAlarmOutgoingBlocked)))
		{
			SigValue.varValue.enumValue = GC_ALARM;
		}
		else
		{
			SigValue.varValue.enumValue = GC_NORMAL;		
		}
		DxiSetData(VAR_A_SIGNAL_VALUE,
		pSigInfo->iEquipId,
		DXI_MERGE_SIG_ID(pSigInfo->iSignalType, pSigInfo->iSignalId),
		sizeof(VAR_VALUE_EX),
		&SigValue,
		0);
	}

	pSigInfo = g_pGcData->PriCfg.pFixedRelayOutput + FIXED_REALY_MA_CA;
	if(pSigInfo->bEnabled)
	{
		if(((GC_GetAlarmNum(ALARM_LEVEL_MAJOR) > 0)
				|| (GC_GetAlarmNum(ALARM_LEVEL_CRITICAL) > 0))
			&&(!(GetSiteInfo()->bAlarmOutgoingBlocked)))
		{
			SigValue.varValue.enumValue = GC_ALARM;
		}
		else
		{
			SigValue.varValue.enumValue = GC_NORMAL;		
		}
		DxiSetData(VAR_A_SIGNAL_VALUE,
		pSigInfo->iEquipId,
		DXI_MERGE_SIG_ID(pSigInfo->iSignalType, pSigInfo->iSignalId),
		sizeof(VAR_VALUE_EX),
		&SigValue,
		0);
	}
#endif   GC_SUPPORT_HIGHTEMP_DISCONNECT

	return;

}


/*==========================================================================*
 * FUNCTION : MsRunInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-02 20:04
 *==========================================================================*/
static void	MsRunInfo()
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;

	GC_RUN_INFO_MS	*pMsInfo = &(g_pGcData->RunInfo.Ms);

	pMsInfo->bInputValid = TRUE;
    
	//To refresh MS_PUB_DOOR_CLOSE
	iRst = GC_GetSigVal(TYPE_OTHER, 
						0, 
						MS_PUB_DOOR_CLOSE, 
						&pSigValue);

	/*GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_GET_VLAUE, 
			"DxiGetData MS_PUB_DOOR_CLOSE error!\n");*/

	if((iRst == ERR_OK) && SIG_VALUE_IS_VALID(pSigValue))
	{
		pMsInfo->bDoorClose = pSigValue->varValue.enumValue;
	}
	else
	{
		pMsInfo->bInputValid = FALSE;
		return;
	}
	
	//To refresh MS_PUB_SWITCH_OPEN	
	iRst = GC_GetSigVal(TYPE_OTHER, 
						0, 
						MS_PUB_SWITCH_OPEN, 
						&pSigValue);

	/*GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_GET_VLAUE, 
			"DxiGetData MS_PUB_SWITCH_OPEN error!\n");*/

	if((iRst == ERR_OK) && SIG_VALUE_IS_VALID(pSigValue))
	{
		pMsInfo->bSwitchOpen = pSigValue->varValue.enumValue;
	}
	else
	{
		pMsInfo->bInputValid = FALSE;
		return;
	}

	//To refresh MS_PUB_AUTOMATIC	
	iRst = GC_GetSigVal(TYPE_OTHER, 
						0, 
						MS_PUB_AUTOMATIC, 
						&pSigValue);


	if((iRst == ERR_OK) && SIG_VALUE_IS_VALID(pSigValue))
	{
		pMsInfo->bAuto = pSigValue->varValue.enumValue;
	}
	else
	{
		pMsInfo->bInputValid = FALSE;
		return;
	}


	//To refresh MS_PUB_RELAY_TRIP	
	iRst = GC_GetSigVal(TYPE_OTHER, 
						0, 
						MS_PUB_RELAY_TRIP, 
						&pSigValue);


	if((iRst == ERR_OK) && SIG_VALUE_IS_VALID(pSigValue))
	{
		pMsInfo->bRelayTrip = pSigValue->varValue.enumValue;
	}
	else
	{
		pMsInfo->bInputValid = FALSE;
		return;
	}
	
	return;
}


/*==========================================================================*
 * FUNCTION : SysRunInfo
 * PURPOSE  : refresh Sysytem run information
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-01-03 11:09
 *==========================================================================*/
static void SysRunInfo(void)
{
#define	 INTERVAL_BOOTING_ACUP		(3 * 60)		//180 sec, Voltage is valid
#define	 INTERVAL_SYNCH_LVD		(1 * 30)			//30 sec, synch LVD Board

	static SIG_ENUM	enumLastState = ST_FLOAT;
	SIG_ENUM	enumNowState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);
	static int	s_iFirstTime = 1;
	static int	s_iFirstLVD = 1;
	float	fSysLoadNoConverter  = 0.0;
	SIG_ENUM	enumConvType = 0;
	static BOOL sb_Inited = FALSE;
	static SIG_BASIC_VALUE *pSigSourceCurr = NULL;
	int iBufLen;
	float fSourceCurr;
	//modified for add source current into system load current by SongXu 20170527
	if(sb_Inited == FALSE)
	{
		sb_Inited = TRUE;

		DxiGetData(VAR_A_SIGNAL_VALUE,
			1,
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 240),
			&iBufLen,
			(void *)&pSigSourceCurr,
			0);	
	}
	if(pSigSourceCurr != NULL && SIG_VALUE_IS_VALID(pSigSourceCurr))
	{
		fSourceCurr = pSigSourceCurr->varValue.fValue;
	}
	else
	{
		fSourceCurr = 0;
	}
	//printf("fSourceCurr %f \n",fSourceCurr);
	if(GC_IsGCSigExist(TYPE_OTHER, 0, CT_PUB_CONVERTER_TYPE))
	{
		enumConvType = GC_GetEnumValue(TYPE_OTHER, 0, CT_PUB_CONVERTER_TYPE);
	}
	else
	{
		enumConvType = 0;
	}
	float		fWorkRatedCurr;

	if(enumLastState != enumNowState)
	{
		GC_SaveData();
		enumLastState = enumNowState;
	}

	g_pGcData->RunInfo.enumAutoMan 
		= GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN);
	
	g_pGcData->enumMasterSlaveMode
		= GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MASTER_SLAVE_MODE);

	g_pGcData->SecTimer.tTimeNow 
		= GC_MaintainSecTimers(g_pGcData->SecTimer.tTimeNow, 
						10 + GC_INTERVAL_REFRESH_DATA);

	GC_ShortTestIntervalTimerCheck();

	g_pGcData->RunInfo.fSysVolt = GC_GetSysVolt();

	if(s_iFirstTime && SEC_TMR_TIME_IN != GC_GetStateOfSecTimer(SEC_TMR_ID_BOOTING_DELAY))
	{
		GC_SetSecTimer(SEC_TMR_ID_BOOTING_DELAY, 
						INTERVAL_BOOTING_ACUP,
						NULL,
						0);

		s_iFirstTime = 0;

	}
	else if(g_pGcData->RunInfo.fSysVolt < 5.0 &&  SEC_TMR_TIME_IN == GC_GetStateOfSecTimer(SEC_TMR_ID_BOOTING_DELAY))
	{
		//Do nothing.
	}
	else
	{
		if(s_iFirstLVD)
		{

			GC_SetSecTimer(SEC_TMR_ID_ESNA_LVD_SYNCH_INTERVAL, 
				INTERVAL_SYNCH_LVD,
				NULL,
				0);
			s_iFirstLVD = 0;
			//printf("Go to here----1\n");
		}
		if(g_pGcData->RunInfo.fSysVolt < GC_MIN_DC_VOLT - 2)
		{
			GC_SetFloatValue(TYPE_OTHER, 
					0, 
					GC_PUB_SYS_VOLT, 
					0);
		}
		else
		{
			GC_SetFloatValue(TYPE_OTHER, 
						0, 
						GC_PUB_SYS_VOLT, 
						g_pGcData->RunInfo.fSysVolt);
		}
	}

	g_pGcData->RunInfo.fSysLoad = GetSysLoad(g_pGcData->RunInfo.fSysLoad);
	//printf("SysRunInfo(): g_pGcData->RunInfo.fSysLoad =%f,IsSMDU_EIB_Mode()=%d~~~~~~~~~\n",g_pGcData->RunInfo.fSysLoad,IsSMDU_EIB_Mode());
	if(g_pGcData->RunInfo.fSysLoad < 0 && SEC_TMR_TIME_IN == GC_GetStateOfSecTimer(SEC_TMR_ID_BOOTING_DELAY))
	{
		//Do nothing.
	}
	else
	{
		GC_SetFloatValue(TYPE_OTHER, 
					0, 
					GC_PUB_SYS_LOAD, 
					g_pGcData->RunInfo.fSysLoad + fSourceCurr);
		//added by Jimmy 2013.07.10
		//Jimmy 2013.11.19 �ܸ��ػ��Ǹ���ret - batt��
		if(!IsSMDU_EIB_Mode())
		{
			GC_SetFloatValue(TYPE_OTHER, 
						0, 
						GC_PUB_TOTAL_LOAD_CURR, 
						(g_pGcData->RunInfo.fSysLoad + fSourceCurr));
		}
	}
	

#define CONV_OUTPUT_EFFICIENCY		(0.94)
	if(enumConvType == CONV_TYPE_400_48V)//400-48V Converter
	{
		fSysLoadNoConverter = g_pGcData->RunInfo.Conv.fSumOfCurr;
	}
	else if(enumConvType == CONV_TYPE_24_48V)//24-48V Converter
	{
		if(g_pGcData->RunInfo.fSysVolt < 1.0)
		{
			fSysLoadNoConverter = g_pGcData->RunInfo.fSysLoad;
		}
		else
		{
			fSysLoadNoConverter = g_pGcData->RunInfo.fSysLoad -  g_pGcData->RunInfo.Conv.fSumOfCurr * g_pGcData->RunInfo.Conv.fAverVoltage / g_pGcData->RunInfo.fSysVolt / CONV_OUTPUT_EFFICIENCY;
		}
	}
	else if(enumConvType == CONV_TYPE_48_24V)//48-24V Converter
	{
		if(g_pGcData->RunInfo.fSysVolt < 1.0)
		{
			fSysLoadNoConverter = g_pGcData->RunInfo.fSysLoad;
		}
		else
		{
			fSysLoadNoConverter = g_pGcData->RunInfo.fSysLoad -  g_pGcData->RunInfo.Conv.fSumOfCurr * g_pGcData->RunInfo.Conv.fAverVoltage/g_pGcData->RunInfo.fSysVolt/ CONV_OUTPUT_EFFICIENCY;
		}
	}
	else
	{
		fSysLoadNoConverter = g_pGcData->RunInfo.fSysLoad ;
	}
	//modified for add source current into system load current by SongXu 20170527
	fSysLoadNoConverter = fSysLoadNoConverter + fSourceCurr;
	//printf("fSysLoadNoConverter = %f\n",fSysLoadNoConverter);
	if(GC_IsGCSigExist(TYPE_OTHER, 
						0, 
						GC_PUB_SYSTEM_LOAD_NO_CONVERTER))
	{
		if(fSysLoadNoConverter > 0.000001)
		{
		
			GC_SetFloatValue(TYPE_OTHER, 
						0, 
						GC_PUB_SYSTEM_LOAD_NO_CONVERTER, 
						fSysLoadNoConverter);
		}
		else
		{
			GC_SetFloatValue(TYPE_OTHER, 
						0, 
						GC_PUB_SYSTEM_LOAD_NO_CONVERTER, 
						0.0);
		}
	}

	//Emergency shut-down input
	//Move to ServiceMain function.
	//GetEstopEshutdownInput();
	/*if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL))
	{
		//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0 )
		if(stVoltLevelState)//24V
		{
			if(GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_VOLTAGE_LEVEL) != VOLTAGE_24V)
			{
				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_SYSTEM_VOLTAGE_LEVEL,
					VOLTAGE_24V,
					TRUE);	
			}	
		}
		else
		{
			if(GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_VOLTAGE_LEVEL) != VOLTAGE_48V)
			{
				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_SYSTEM_VOLTAGE_LEVEL,
					VOLTAGE_48V,
					TRUE);	
			}
		}
	}*/

	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_PUB_SYSTEM_POWER_INPUT))
	{
		if((g_pGcData->RunInfo.Ac.bDslValid 
				&& (GC_DIESEL_ON == g_pGcData->RunInfo.Ac.bDslState))) //Hybrid
		{
			
			if(GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_POWER_INPUT) != POWER_INPUT_DIESEL)
			{
				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_SYSTEM_POWER_INPUT,
					POWER_INPUT_DIESEL,
					TRUE);	
			}
		}
		else
		{
			if(GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYSTEM_POWER_INPUT) != POWER_INPUT_AC)
			{
				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_SYSTEM_POWER_INPUT,
					POWER_INPUT_AC,
					TRUE);	
			}
		}
	}


#ifdef GC_SUPPORT_MPPT
	if(enumConvType == CONV_TYPE_400_48V)//400-48V Converter
	{
		fWorkRatedCurr = g_pGcData->RunInfo.Conv.fSumOnRatedCurrent;
	}
	else
	{
		fWorkRatedCurr = g_pGcData->RunInfo.Rt.fSumOnRatedCurrent + g_pGcData->RunInfo.Mt.fSumOnRatedCurrent;
	}

#else
	if(enumConvType == CONV_TYPE_400_48V)//400-48V Converter
	{
		fWorkRatedCurr = g_pGcData->RunInfo.Conv.fSumOnRatedCurrent;
	}
	else
	{
		fWorkRatedCurr = g_pGcData->RunInfo.Rt.fSumOnRatedCurrent;
	}
#endif	
	if(GC_IsGCSigExist(TYPE_OTHER,0,GC_PUB_TOTAL_RATED))
	{
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			GC_PUB_TOTAL_RATED,
			fWorkRatedCurr);
	}
	

	FixedRelayCtl();
	TurnManualToAuto();

	return;
}



/*==========================================================================*
 * FUNCTION : GC_RunInfoRefresh
 * PURPOSE  : to provide a interface for refresh run-infomation of ACU
 * CALLS    : RectRunInfo BattRunInfo GC_MaintainSecTimers 
             
 * CALLED BY: ServiceMain
 * RETURN   :
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-15 17:08
 *==========================================================================*/
void	GC_RunInfoRefresh()
{
	static int iLi_Count = 0; //﮵�����⴦���߼�
	if(iLi_Count < 3)
	{
		iLi_Count++;
		if(IsWorkIn_LiBattMode())
		{
#define ENUM_DISABLE 0
#define ENUM_ENBALE 1
			//������Զʹ��
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				BT_PUB_CURR_LMT_ENB,
				ENUM_ENBALE,
				TRUE);	
			//�����Զ�����
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				BT_PUB_AUTO_BC_ENB,
				ENUM_DISABLE,
				TRUE);	
			//����Cyclic����
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				BT_PUB_CYC_BC_ENB,
				ENUM_DISABLE,
				TRUE);	
		}
	}
	else
	{
		//do nothing
	}
	RectRunInfo();

	//DcRunInfo();

	LvdRunInfo();

	BattRunInfo();

	AcRunInfo();

	MsRunInfo();

	SysRunInfo();

	ConvRunInfo();

#ifdef GC_SUPPORT_MPPT
	MpptRunInfo();
#endif
	//Jimmyע�ͣ�����ֻ���Զ����õ�ʱ����жϣ���ҪÿȦ�����ж�
	//if(g_pGcData->bAutoConfig)
	{
		//added by Jimmy 2011/11/25
		JudgeSMTempLost();
		//added by Jimmy 2012/04/05
		JudgeDCEMLost();
		//��JudgeLiBattLost�ڲ��Ѿ�������߼�����˲����ټ�if�ж�
		//if(IsWorkIn_LiBattMode())
		{
			JudgeLiBattLost();
		}
		
	}

	return;
}



/*==========================================================================*
 * FUNCTION : GC_IsRectComm
 * PURPOSE  : To judge if the retifier communication is not break
 * CALLS    : GC_GetDwordValue
 * CALLED BY: GC_MaintainRectList 
 * ARGUMENTS: int  iRectNo : 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:45
 *==========================================================================*/
BOOL	GC_IsRectComm(int iRectType, int iRectNo)
{
	switch(iRectType)
	{
	case RECT_TYPE_SELF:
		return (GC_NORMAL == (GC_GetEnumValue(TYPE_RECT_UNIT, 
									iRectNo,
									RT_PRI_NO_RESPOND_STATE))
				&& (GC_NORMAL == GC_GetEnumValue(TYPE_OTHER,
									0,
									RT_PUB_ALL_INTERRUPT_SELF)));
		
		
	case RECT_TYPE_SLAVE1:
		return (GC_NORMAL == (GC_IsGCEquipExist(TYPE_SLAVE1_RECT, 
									iRectNo,
									S1_PRI_NO_RESPOND_STATE,
									GC_EQUIP_INVALID))
				&& (GC_NORMAL == GC_IsGCEquipExist(TYPE_OTHER,
									0,
									RT_PUB_ALL_INTERRUPT_SLAVE1,
									GC_EQUIP_INVALID)));
		
	case RECT_TYPE_SLAVE2:
		return (GC_NORMAL == (GC_IsGCEquipExist(TYPE_SLAVE2_RECT, 
									iRectNo,
									S2_PRI_NO_RESPOND_STATE,
									GC_EQUIP_INVALID))
				&& (GC_NORMAL == GC_IsGCEquipExist(TYPE_OTHER,
									0,
									RT_PUB_ALL_INTERRUPT_SLAVE2,
									GC_EQUIP_INVALID)));
		
	case RECT_TYPE_SLAVE3:
		return (GC_NORMAL == (GC_IsGCEquipExist(TYPE_SLAVE3_RECT, 
									iRectNo,
									S3_PRI_NO_RESPOND_STATE,
									GC_EQUIP_INVALID))
				&& (GC_NORMAL == GC_IsGCEquipExist(TYPE_OTHER,
									0,
									RT_PUB_ALL_INTERRUPT_SLAVE3,
									GC_EQUIP_INVALID)));
		

#ifdef GC_SUPPORT_MPPT
	case TYPE_MPPT_UNIT:
		return (GC_NORMAL == (GC_GetEnumValue(TYPE_MPPT_UNIT, 
			iRectNo,
			MT_PRI_NO_RESPOND))
			&& (GC_NORMAL == GC_GetEnumValue(TYPE_OTHER,
			0,
			MT_PUB_ALL_INTERRUPT)));
#endif	
	default:
		return TRUE;
	}
}
/*==========================================================================*
* FUNCTION : GC_IsRectComm
* PURPOSE  : To judge if the retifier communication is not break
* CALLS    : GC_GetDwordValue
* CALLED BY: GC_MaintainRectList 
* ARGUMENTS: int  iRectNo : 
* RETURN   :  
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-25 13:45
*==========================================================================*/
BOOL	GC_IsConvComm(IN int iConvNo)
{
	return (GC_NORMAL == (GC_GetEnumValue(TYPE_CT_UNIT, 
			iConvNo,
			CT_PRI_NO_RESPONSE))
			&& (GC_NORMAL == GC_GetEnumValue(TYPE_OTHER,
			0,
			CT_PUB_ALL_INTERRUPT_SELF))
			&& (GC_NORMAL == GC_GetEnumValue(TYPE_OTHER,
			0,
			GC_PUB_CAN_INTERRUPT)));
	
	
}
/*==========================================================================*
 * FUNCTION : GC_GetRectValidCapacity
 * PURPOSE  : To get rectifier unit's valid capacity
 * CALLS    : 
 * CALLED BY: GC_MaintainRectList 
 * ARGUMENTS:	int  iRectNo : 
		int iRectType :

 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Marco Yang               DATE: 2009-9-29 10:24
 *==========================================================================*/
float	GC_GetRectValidCapacity(int iRectType, int iRectNo)
{
	float fSlaveCap;
	BOOL bRTN;
	switch(iRectType)
	{
	case RECT_TYPE_SELF:
		return (GC_GetFloatValue(TYPE_RECT_UNIT, 
									iRectNo,
									RT_PRI_VALID_CAP));
				
	case RECT_TYPE_SLAVE1:
		bRTN =  GC_GetFloatValueWithStateReturn(RECT_TYPE_SLAVE1, 
							iRectNo,
							S1_PRI_VALID_CAP,
							&fSlaveCap);
		if(bRTN == TRUE)
		{
			return fSlaveCap;
		}
		else
		{
			return 0.0;
		}
		
	case RECT_TYPE_SLAVE2:
		bRTN =  GC_GetFloatValueWithStateReturn(RECT_TYPE_SLAVE2, 
							iRectNo,
							S2_PRI_VALID_CAP,
							&fSlaveCap);
		if(bRTN == TRUE)
		{
			return fSlaveCap;
		}
		else
		{
			return 0.0;
		}
		
	case RECT_TYPE_SLAVE3:
		bRTN =  GC_GetFloatValueWithStateReturn(RECT_TYPE_SLAVE3, 
							iRectNo,
							S3_PRI_VALID_CAP,
							&fSlaveCap);
		if(bRTN == TRUE)
		{
			return fSlaveCap;
		}
		else
		{
			return 0.0;
		}
		
#ifdef GC_SUPPORT_MPPT
	case TYPE_MPPT_UNIT:
		return (GC_GetFloatValue(TYPE_MPPT_UNIT, 
			iRectNo,
			MT_PRI_VALID_CAP));
#endif
	default:
		return 50.0;
	}
}
/*==========================================================================*
 * FUNCTION : IsDslValid
 * PURPOSE  : To judge diesel is valid
 * CALLS    : GC_GetSigVal
 * CALLED BY: GC_RunInfoRefresh
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:51
 *==========================================================================*/
//static BOOL	IsDslValid()
//{
//	int					iRst;
//	SIG_BASIC_VALUE*	pSigValue;
//
//	if(!(g_pGcData->EquipInfo.AcInfo.bDslStatus))
//	//No Diesel Status Signal
//	{
//		return FALSE;
//	}
//
//	iRst = GC_GetSigVal(TYPE_OTHER, 0, DSL_PUB_STATUS, &pSigValue);
//	GC_ASSERT((iRst == ERR_OK), 
//			ERR_CTL_GET_VLAUE, 
//			"GC_GetSigVal DSL_PUB_STATUS error!\n");
//
//	return SIG_VALUE_IS_VALID(pSigValue);
//
//}



/*==========================================================================*
 * FUNCTION : GetDslState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : TRUE: Diesel openning, FALSE: closed 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-10 11:37
 *==========================================================================*/
//static BOOL	GetDslState(void)
//{
//
//	if(!(g_pGcData->RunInfo.Ac.bDslValid))
//	//Signal invalid, return diesel open state
//	{
//		return GC_DIESEL_OFF;
//	}
//
//	return GC_GetEnumValue(TYPE_OTHER, 0, DSL_PUB_STATUS);
//}


/*==========================================================================*
 * FUNCTION : IsTotalPowerValid
 * PURPOSE  : To judge Total Power signal is valid
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-09 17:11
 *==========================================================================*/
//static BOOL	IsTotalPowerValid()
//{
//	int					iRst;
//	SIG_BASIC_VALUE*	pSigValue;
//
//	if(!(g_pGcData->EquipInfo.AcInfo.bTotalPower))
//	{
//		return FALSE;
//	}
//
//	iRst = GC_GetSigVal(TYPE_OTHER, 0, AC_PUB_TOTAL_POWER, &pSigValue);
//	GC_ASSERT((iRst == ERR_OK), 
//			ERR_CTL_GET_VLAUE, 
//			"GC_GetSigVal AC_PUB_TOTAL_POWER error!\n");
//
//	return SIG_VALUE_IS_VALID(pSigValue);
//
//}


/*==========================================================================*
 * FUNCTION : IsDcCurrValid
 * PURPOSE  : To judge DC current signal is valid
 * CALLS    : GC_GetSigVal
 * CALLED BY: GC_RunInfoRefresh
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:52
 *==========================================================================*/
//static BOOL	IsDcCurrValid(int iDcNo)
//{
//	int					iRst;
//	SIG_BASIC_VALUE*	pSigValue;
//
//	iRst = GC_GetSigVal(TYPE_OTHER, 0, DC_PUB_CURR1 + iDcNo, &pSigValue);
//	GC_ASSERT((iRst == ERR_OK), 
//			ERR_CTL_GET_VLAUE, 
//			"GC_GetSigVal DC_PUB_CURR1 error!\n");
//
//	return SIG_VALUE_IS_VALID(pSigValue);
//		/*(VALUE_STATE_IS_INVALID 
//		!= (VALUE_STATE_IS_INVALID & pSigValue->usStateMask)) ? TRUE : FALSE;*/
//}


/*==========================================================================*
 * FUNCTION : IsDcVoltValid
 * PURPOSE  : To judge DC voltage signal is valid
 * CALLS    : GC_GetSigVal
 * CALLED BY: GC_RunInfoRefresh
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:53
 *==========================================================================*/
//static BOOL	IsDcVoltValid(int iDcNo)
//{
//	int					iRst;
//	SIG_BASIC_VALUE*	pSigValue;
//
//	iRst = GC_GetSigVal(TYPE_OTHER, 0, DC_PUB_VOLT1 + iDcNo, &pSigValue);
//	GC_ASSERT((iRst == ERR_OK), 
//			ERR_CTL_GET_VLAUE, 
//			"GC_GetSigVal DC_PUB_VOLT1 error!\n");
//
//	return SIG_VALUE_IS_VALID(pSigValue);
//		/*(VALUE_STATE_IS_INVALID 
//		!= (VALUE_STATE_IS_INVALID & pSigValue->usStateMask)) ? TRUE : FALSE;*/
//}


/*==========================================================================*
 * FUNCTION : LvdRefresh
 * PURPOSE  : To judge LVD signal is valid
 * CALLS    : DxiGetData
 * CALLED BY: GC_RunInfoRefresh
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:53
 *==========================================================================*/
//static void	LvdRefresh()
//{
//}


/*==========================================================================*
 * FUNCTION : GC_GetSysVolt
 * PURPOSE  : Calculate and return the system voltage
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-26 16:02
 *==========================================================================*/
float	GC_GetSysVolt(void)
{
	int		i;
	float	fDcVolt = 0.0;
	//printf("Go to Get sys voltage %d\n",time(NULL) );
	SIG_ENUM	stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CONVERTER_ONLY_MODE);

	if(GC_ENABLED == stState )
	{
		return g_pGcData->RunInfo.Conv.fAverVoltage;
	}

	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{
		if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
			&& (g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0] == GC_DISCONNECTED))
		{
			if(g_pGcData->RunInfo.Rt.iQtyOnWorkRect)
			{
				if((g_pGcData->RunInfo.Rt.fVolt > GC_MIN_DC_VOLT)
					 && (g_pGcData->RunInfo.Rt.fVolt < GC_MAX_DC_VOLT))
				{
					return g_pGcData->RunInfo.Rt.fVolt;
				}
				
			}
#ifdef GC_SUPPORT_MPPT
			if(g_pGcData->RunInfo.Mt.iQtyOnWorkRect)
			{
printf("\n Mt.fVolt=%f",g_pGcData->RunInfo.Mt.fVolt );
				if(( g_pGcData->RunInfo.Mt.fVolt> GC_MIN_DC_VOLT)
				 && ( g_pGcData->RunInfo.Mt.fVolt< GC_MAX_DC_VOLT))
				{
					return g_pGcData->RunInfo.Mt.fVolt;
				}
			}
#endif	
		}

		if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
			&& (g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1] == GC_DISCONNECTED))
		{
			if(g_pGcData->RunInfo.Rt.iQtyOnWorkRect)
			{
				if((g_pGcData->RunInfo.Rt.fVolt > GC_MIN_DC_VOLT)
				&& (g_pGcData->RunInfo.Rt.fVolt < GC_MAX_DC_VOLT))
				{
					return g_pGcData->RunInfo.Rt.fVolt;
				}
			}
#ifdef GC_SUPPORT_MPPT
			if(g_pGcData->RunInfo.Mt.iQtyOnWorkRect)
			{
printf("\n LVD2 Mt.fVolt=%f",g_pGcData->RunInfo.Mt.fVolt );
				if(( g_pGcData->RunInfo.Mt.fVolt> GC_MIN_DC_VOLT)
				 && ( g_pGcData->RunInfo.Mt.fVolt< GC_MAX_DC_VOLT))
				{
					return g_pGcData->RunInfo.Mt.fVolt;
				}
			}
#endif	
		}
	}

	//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	//if(GC_EQUIP_EXIST == GC_GetEnumValue(TYPE_OTHER, 0, LDU_PUB_EXISTENCE))
	//if (GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, LDU_PUB_EXISTENCE,GC_EQUIP_INVALID))
	//{
	//	BOOL		bDcdComm = FALSE;
	//	for(i = 0; i < 10; i++)
	//	{
	//		if(GC_ALARM != GC_GetEnumValue(TYPE_OTHER, 0, LDU_DC_PUB_COMM_STATE1 + i))
	//		{
	//			bDcdComm = TRUE;
	//			break;
	//		}
	//	}
	//	
	//	fDcVolt = GC_GetFloatValue(TYPE_OTHER, 0, LDU_DC_PUB_VOLT);
	//	if(bDcdComm && (fDcVolt > GC_MIN_DC_VOLT)
	//		&& (fDcVolt < GC_MAX_DC_VOLT))
	//	{
	//		return fDcVolt;
	//	}
	//}
	//else
	{

		fDcVolt = GC_GetFloatValue(TYPE_OTHER, 0, DC_PUB_VOLT);
		if((fDcVolt > GC_MIN_DC_VOLT)
			&& (fDcVolt < GC_MAX_DC_VOLT))
		{
			//printf("\n GC Get sys voltage from DC =%f time is %d\n",fDcVolt,time(NULL) );
			return fDcVolt;
		}
	}
	
	if(g_pGcData->RunInfo.Rt.iQtyOnWorkRect)
	{
		//printf("\n GC Get sys voltage from Rectifier =%f time is %d\n",fDcVolt,time(NULL) );
		return g_pGcData->RunInfo.Rt.fVolt;
	}
	//printf("",g_pGcData->RunInfo.Rt.iQtyOnWorkRect, g_pGcData->RunInfo.Rt.fVolt);
#ifdef GC_SUPPORT_MPPT
	if(g_pGcData->RunInfo.Mt.iQtyOnWorkRect)
	{
		return g_pGcData->RunInfo.Mt.fVolt;
	}
#endif	
	return	fDcVolt;

}



/*==========================================================================*
 * FUNCTION : MsRunInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-02 20:04
 *==========================================================================*/
//static void	MsRunInfo()
//{
//	SIG_BASIC_VALUE*	pSigValue;
//	int					iRst;
//
//	GC_RUN_INFO_MS	*pMsInfo = &(g_pGcData->RunInfo.Ms);
//
//	pMsInfo->bInputValid = TRUE;
//    
//	//To refresh MS_PUB_DOOR_CLOSE
//	iRst = GC_GetSigVal(TYPE_OTHER, 
//						0, 
//						MS_PUB_DOOR_CLOSE, 
//						&pSigValue);
//
//	/*GC_ASSERT((iRst == ERR_OK), 
//			ERR_CTL_GET_VLAUE, 
//			"DxiGetData MS_PUB_DOOR_CLOSE error!\n");*/
//
//	if((iRst == ERR_OK) && SIG_VALUE_IS_VALID(pSigValue))
//	{
//		pMsInfo->bDoorClose = pSigValue->varValue.enumValue;
//	}
//	else
//	{
//		pMsInfo->bInputValid = FALSE;
//		return;
//	}
//	
//	//To refresh MS_PUB_SWITCH_OPEN	
//	iRst = GC_GetSigVal(TYPE_OTHER, 
//						0, 
//						MS_PUB_SWITCH_OPEN, 
//						&pSigValue);
//
//	/*GC_ASSERT((iRst == ERR_OK), 
//			ERR_CTL_GET_VLAUE, 
//			"DxiGetData MS_PUB_SWITCH_OPEN error!\n");*/
//
//	if((iRst == ERR_OK) && SIG_VALUE_IS_VALID(pSigValue))
//	{
//		pMsInfo->bSwitchOpen = pSigValue->varValue.enumValue;
//	}
//	else
//	{
//		pMsInfo->bInputValid = FALSE;
//		return;
//	}
//
//	//To refresh MS_PUB_AUTOMATIC	
//	iRst = GC_GetSigVal(TYPE_OTHER, 
//						0, 
//						MS_PUB_AUTOMATIC, 
//						&pSigValue);
//
//	/*GC_ASSERT((iRst == ERR_OK), 
//			ERR_CTL_GET_VLAUE, 
//			"DxiGetData MS_PUB_AUTOMATIC error!\n");*/
//
//	if((iRst == ERR_OK) && SIG_VALUE_IS_VALID(pSigValue))
//	{
//		pMsInfo->bAuto = pSigValue->varValue.enumValue;
//	}
//	else
//	{
//		pMsInfo->bInputValid = FALSE;
//		return;
//	}
//
//
//	//To refresh MS_PUB_RELAY_TRIP	
//	iRst = GC_GetSigVal(TYPE_OTHER, 
//						0, 
//						MS_PUB_RELAY_TRIP, 
//						&pSigValue);
//
//	/*GC_ASSERT((iRst == ERR_OK), 
//			ERR_CTL_GET_VLAUE, 
//			"DxiGetData MS_PUB_RELAY_TRIP error!\n");*/
//
//	if((iRst == ERR_OK) && SIG_VALUE_IS_VALID(pSigValue))
//	{
//		pMsInfo->bRelayTrip = pSigValue->varValue.enumValue;
//	}
//	else
//	{
//		pMsInfo->bInputValid = FALSE;
//		return;
//	}
//	
//	return;
//}





void	 BlkVoltDiffAlm(int iBattNo)
{
	int		i, j;
	float	fMaxDiff;
	float	fDiff;
	int		iBlocks;
	BOOL	bOverMaxDiff = FALSE,bRTN;
	static	int	s_aiCounter[MAX_NUM_BATT_EQUIP] = {0};

	iBlocks = g_pGcData->EquipInfo.BattTestLogInfo.aExistBattInfo[iBattNo].iQtyBlock;
	//TRACE("\n Battery No.:%d\n",iBattNo);
	//TRACE("\n Battery Block Number:%d\n",iBlocks);

	// test the battery has cell block or not at first. maofuhua, 2005-4-16
	if (iBlocks <= 0)
	{
		return;
	}

	bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, 
				iBattNo, 
				BT_PRI_MAX_BLK_VOLT_DIFF,
				&fMaxDiff);
	if(bRTN = FALSE)
	{
		return;
	}

	for(i = 0; i < iBlocks; i++)
	{
		for(j = i + 1; j < iBlocks; j++)
		{			
			fDiff = GC_GetFloatValue(TYPE_BATT_UNIT, 
								iBattNo, 
								BT_PRI_BLK1_VOLT + i) 
				- GC_GetFloatValue(TYPE_BATT_UNIT, 
								iBattNo, 
								BT_PRI_BLK1_VOLT + j);
			fDiff = ABS(fDiff);
			if(fDiff > fMaxDiff)
			{
				bOverMaxDiff = TRUE;
				goto GC_BLK_VOLT_ALM_LOOP_END;
			}
		}
	}

GC_BLK_VOLT_ALM_LOOP_END:

	if(TRUE == bOverMaxDiff)
	{
		if(s_aiCounter[iBattNo] > 3)
		{
			GC_SetEnumValue(TYPE_BATT_UNIT, 
						iBattNo, 
						BT_PRI_OVER_BLK_VOLT_DIFF_SET, 
						GC_ALARM,
						TRUE);
		}
		else
		{
			s_aiCounter[iBattNo]++;
		}
	}
	else
	{
		s_aiCounter[iBattNo] = 0;
		GC_SetEnumValue(TYPE_BATT_UNIT, 
					iBattNo, 
					BT_PRI_OVER_BLK_VOLT_DIFF_SET, 
					GC_NORMAL,
					TRUE);
	}

	return;
}





static void	TurnManualToAuto(void)
{
	SIG_ENUM	eAutoMode = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN);
	int			iToAutoDeley = SECONDS_PER_HOUR * (int)GC_GetDwordValue(TYPE_OTHER, 0, GC_MAN_TO_AUTO_DELAY);

	if(eAutoMode == STATE_AUTO)
	{
		GC_SuspendSecTimer(SEC_TMR_ID_MANUAL_TO_AUTO);
		return;
	}

	//Manual
	if(GC_TIMER_IS_STOP(SEC_TMR_ID_MANUAL_TO_AUTO) && (iToAutoDeley > 0))
	{
		GC_SetSecTimer(SEC_TMR_ID_MANUAL_TO_AUTO, iToAutoDeley, NULL, 0);
	}
	
	if(GC_TIMER_IS_OUT(SEC_TMR_ID_MANUAL_TO_AUTO))
	{
		GC_SetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN, STATE_AUTO, TRUE);
	}

	return;
}

/*
static float 	CalcWorkOnRectTotalRatedCurr()
{
	SIG_ENUM eRectDeratingMode = GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_RECT_DERATING_RUN);
	float fTotalRatedCurr = 0;
	float fRectRatedCurr;

	GC_RUN_INFO_RECT		*pRtRunInfo = &(g_pGcData->RunInfo.Rt);

	// ����Can�ɼ����ڵײ��Ѿ������ģ�����Ч��������������ﲻ�������Ƿ񽵹���ģʽ
	List_GotoHead(pRtRunInfo->hListOnWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(pRtRunInfo->hListOnWorkRect, 
			(void*)(&RectifierInfo)))
		{
			fRectRatedCurr = GC_GetFloatValue(TYPE_RECT_UNIT, RectifierInfo.iRectNo, RT_PRI_VALID_RATED_CURRENT);
			fTotalRatedCurr += fRectRatedCurr;
		}

	}while(List_GotoNext(pRtRunInfo->hListOnWorkRect));	

	GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR, fTotalRatedCurr);
	return fTotalRatedCurr;
}
*/

/*==========================================================================*
 * FUNCTION : AcRunInfo
 * PURPOSE  : refresh AC run information 
 * CALLS    : 
 * CALLED BY: GC_RunInfoRefresh
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-09 16:59
 *==========================================================================*/
static void AcRunInfo(void)
{
	
	g_pGcData->RunInfo.Ac.bDslValid = IsDslValid();
	g_pGcData->RunInfo.Ac.bDslState = GetDslState();
	g_pGcData->RunInfo.Ac.bPowerValid = IsTotalPowerValid();

	if(g_pGcData->RunInfo.Ac.bPowerValid)
	{
		g_pGcData->RunInfo.Ac.fTotalPower 
			= GC_GetFloatValue(TYPE_OTHER, 0, AC_PUB_TOTAL_POWER);
	}
	//printf("AcRunInfo: \n", g_pGcData->RunInfo.Ac.bDslValid, g_pGcData->RunInfo.Ac.bDslState, g_pGcData->RunInfo.Ac.bPowerValid);
	return;
}
/*==========================================================================*
 * FUNCTION : IsDslValid
 * PURPOSE  : To judge diesel is valid
 * CALLS    : GC_GetSigVal
 * CALLED BY: GC_RunInfoRefresh
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-25 13:51
 *==========================================================================*/
static BOOL	IsDslValid()
{
	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	//printf("IsDslValid g_pGcData->EquipInfo.AcInfo.bDslStatus = %d\n", g_pGcData->EquipInfo.AcInfo.bDslStatus);
	if(!(g_pGcData->EquipInfo.AcInfo.bDslStatus))
	//No Diesel Status Signal
	{
		return FALSE;
	}

	iRst = GC_GetSigVal(TYPE_OTHER, 0, DSL_PUB_STATUS, &pSigValue);

	if (iRst == ERR_DXI_INVALID_EQUIP_ID)
	{
		return FALSE;
	}

	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_GET_VLAUE, 
			"GC_GetSigVal DSL_PUB_STATUS error!\n");
	

	return SIG_VALUE_IS_VALID(pSigValue);

}



/*==========================================================================*
 * FUNCTION : GetDslState
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : TRUE: Diesel openning, FALSE: closed 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-10 11:37
 *==========================================================================*/
static BOOL	GetDslState(void)
{

	if(!(g_pGcData->RunInfo.Ac.bDslValid))
	//Signal invalid, return diesel open state
	{
		return GC_DIESEL_OFF;
	}

	return GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_STATUS,GC_DIESEL_OFF);
}


/*==========================================================================*
 * FUNCTION : IsTotalPowerValid
 * PURPOSE  : To judge Total Power signal is valid
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-09 17:11
 *==========================================================================*/
static BOOL	IsTotalPowerValid()
{
	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	if(!(g_pGcData->EquipInfo.AcInfo.bTotalPower))
	{
		return FALSE;
	}

	iRst = GC_GetSigVal(TYPE_OTHER, 0, AC_PUB_TOTAL_POWER, &pSigValue);
	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_GET_VLAUE, 
			"GC_GetSigVal AC_PUB_TOTAL_POWER error!\n");
	if(iRst != ERR_OK)
	{		
		return FALSE;	
	}
	return SIG_VALUE_IS_VALID(pSigValue);

}


/*==========================================================================*
 * FUNCTION : CalcRectTime
 * PURPOSE  : To judge Total Power signal is valid
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-09 17:11
 *==========================================================================*/
static BOOL	CalcRectTime()
{

	int			i, iQtyOfRect = 0;
	static time_t		tmOld = 0;
	time_t			tmNow =	time(NULL);
	SIG_ENUM		enumTestState;
	DWORD			dwReduceTimes;

	GC_RUN_INFO_RECT	*pRtRunInfo = &(g_pGcData->RunInfo.Rt);

	iQtyOfRect= pRtRunInfo->Slave[RECT_TYPE_SELF].iQtyOfRect;
	
	//2.4. Rect running time
	if(tmOld == 0)
	{
		tmOld = tmNow;
	}
	
	/*List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
			(void*)(&RectifierInfo)))
		{
				
			printf("Rect%d RectifierInfo.fOperTime = %f RectifierInfo.dwSelfOperTime = %d\n", RectifierInfo.iRectNo, RectifierInfo.fOperTime, RectifierInfo.dwSelfOperTime);
			

		}	

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));			*/

	
	float fElapsedHrs = (float)(tmNow - tmOld) / SEC_PER_HOUR;

#define RECT_MAINTN_TIME_RESOLUTION		(0.01)		// 0.01 hr. - do NOT change!
#define RECT_MAINTN_TIME_1HOUR			(1.0)		// 1 hr. - do NOT change!

	if (fElapsedHrs >= RECT_MAINTN_TIME_RESOLUTION)
	{
		if (fElapsedHrs >= (2*RECT_MAINTN_TIME_RESOLUTION))
		{
			// the time may be changed to future.
			fElapsedHrs = RECT_MAINTN_TIME_RESOLUTION;
		}
		
		List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);
		do
		{
			GC_RECT_INFO	RectifierInfo;

			if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
						(void*)(&RectifierInfo)))
			{
				enumTestState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_TEST_STATE);
				dwReduceTimes = GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_TEST_REDUCE_TIMES);
				
				if(enumTestState)	//test state
				{
					RectifierInfo.fOperTime += fElapsedHrs * (float)dwReduceTimes;	
					
				}
				else
				{
					
					RectifierInfo.fOperTime += fElapsedHrs;		
					
				}

			}
			if(RectifierInfo.fOperTime > RECT_MAINTN_TIME_1HOUR)
			{
				RectifierInfo.dwSelfOperTime++;
				RectifierInfo.fOperTime = 0.0;
				
			}
			UpdateAndRefresh(RectifierInfo, 
				g_pGcData->RunInfo.Rt.hListOnWorkRect);

		}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));			

		tmOld = tmNow;
	}
	else if (fElapsedHrs < 0)
	{
		// the time changed back. set the last time.
		tmOld = tmNow;
	}
	/*printf("------out--CalcRectTime----------\n");
	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
			(void*)(&RectifierInfo)))
		{

			printf("----Rect%d RectifierInfo.fOperTime = %f RectifierInfo.dwSelfOperTime = %d\n", RectifierInfo.iRectNo, RectifierInfo.fOperTime, RectifierInfo.dwSelfOperTime);


		}	

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));	*/
	
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : InitRectTime
 * PURPOSE  : To judge Total Power signal is valid
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-09 17:11
 *==========================================================================*/
BOOL	InitRectTime()
{
	int			i, j;
	GC_RECT_INFO		RectifierInfo;
	GC_RUN_INFO_RECT	*pRt = &(g_pGcData->RunInfo.Rt);

	/*Create and stuff the QueueAcOnRect[]/QueueAcOffRect[], if some rectifer 
	 is not in AC-on list / AC-off list, insert them into the list*/

	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
			(void*)(&RectifierInfo)))
		{
		
			RectifierInfo.fOperTime = 0;
			RectifierInfo.dwSelfOperTime = 0;
			SearchAndRefresh(RectifierInfo, 
				g_pGcData->RunInfo.Rt.hListOnWorkRect);
	
		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));

	List_GotoHead(g_pGcData->RunInfo.Rt.hListOffWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(g_pGcData->RunInfo.Rt.hListOffWorkRect, 
			(void*)(&RectifierInfo)))
		{

			RectifierInfo.fOperTime = 0;
			RectifierInfo.dwSelfOperTime = 0;
			SearchAndRefresh(RectifierInfo, 
				g_pGcData->RunInfo.Rt.hListOnWorkRect);

		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOffWorkRect));

	return TRUE;
}
BOOL	PrintRectTime()
{
	int			i, j;
	GC_RECT_INFO		RectifierInfo;
	GC_RUN_INFO_RECT	*pRt = &(g_pGcData->RunInfo.Rt);

	/*Create and stuff the QueueAcOnRect[]/QueueAcOffRect[], if some rectifer 
	is not in AC-on list / AC-off list, insert them into the list*/

	////Debug
	//printf("-----------------------------------------------------\n");
	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
			(void*)(&RectifierInfo)))
		{
			printf("Rect%d time is = %f, dw = %d\n", RectifierInfo.iRectNo,RectifierInfo.fOperTime, RectifierInfo.dwSelfOperTime );

		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));

	List_GotoHead(g_pGcData->RunInfo.Rt.hListOffWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(g_pGcData->RunInfo.Rt.hListOffWorkRect, 
			(void*)(&RectifierInfo)))
		{
			printf("Rect%d time is = %f, dw = %d\n", RectifierInfo.iRectNo,RectifierInfo.fOperTime, RectifierInfo.dwSelfOperTime );
		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOffWorkRect));

	return TRUE;
}
/*==========================================================================*
* FUNCTION : GC_MaintainRectList
* PURPOSE  : To maintain the two lists--Swictch_on rectifier list and 
Swictch_off rectifier list 
* CALLS    : List_GotoHead, List_GotoNext, 
List_Delete, SearchAndRefresh, List_Insert,
* CALLED BY: GC_InitRectList, RefreshData
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-09-15 16:57
*==========================================================================*/
void	GC_MaintainConvList(IN int iQtyConv)
{
	int					j;	

	int					iQtyCommConv = 0;
	int					iQtyOnWorkConv = 0;
	
	for(j = 0; j <iQtyConv; j++)
	{
		if(GC_IsConvComm(j))
		{
			BOOL bValid;

			if(GC_CT_ON == GC_GetEnumValue(TYPE_CT_UNIT,
				j,
				CT_PRI_ONOFF_STATE))											
			{
				iQtyOnWorkConv++;
			}
			iQtyCommConv++;				
		}
	}

	//Update number
	GC_SetDwordValue(TYPE_OTHER,
		0,
		CT_PUB_QTY_COMM, 
		(DWORD)iQtyCommConv);

	GC_SetDwordValue(TYPE_OTHER, 
		0,
		CT_PUB_QTY_AC_ON_COMM, 
		(DWORD)iQtyOnWorkConv);

	return;
}
#ifdef GC_SUPPORT_MPPT
/*==========================================================================*
* FUNCTION : GetMpptVolt
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static float : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-30 09:53
*==========================================================================*/
static float GetMpptVolt(void)
{
	int		i, j;
	int		iMpptNum = 0;
	float	fMpptVolt[MAX_NUM_MPPT_EQUIP];

	if(!(g_pGcData->RunInfo.Mt.bValid))
	{
		return 0.0;
	}

	for(i = 0; i < g_pGcData->RunInfo.Mt.iQtyOfRect; i++)
	{
		if(g_pGcData->RunInfo.Mt.abComm[i])
		{
			fMpptVolt[iMpptNum] 
			= GC_GetFloatValue(TYPE_MPPT_UNIT, 
				i,
				MT_PRI_OUT_VOLT);
			iMpptNum++;
		}
	}

	//effervescing sort
	if(iMpptNum)
	{
		for (i = 0; i < (iMpptNum - 1); i++)
		{
			for (j = 0; j < (iMpptNum - 1 - i); j++)
			{
				if(fMpptVolt[j] > fMpptVolt[j + 1])
				{
					float fTemp = fMpptVolt[j + 1];
					fMpptVolt[j + 1] = fMpptVolt[j];
					fMpptVolt[j] = fTemp;
				}
			}
		}

		return fMpptVolt[(iMpptNum - 1) / 2];
	}
	else
	{
		return 0.0;
	}

}
/*==========================================================================*
* FUNCTION : IsMpptValid
* PURPOSE  : To judge if rectifiers are valid
* CALLS    : GC_GetSigVal
* CALLED BY: RectRunInfo
* RETURN   : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-25 13:41
*==========================================================================*/
static BOOL	IsMpptValid(void)
{
	return g_pGcData->RunInfo.Mt.iQtyOnWorkRect ? TRUE : FALSE;
}
#define	MPPT_LOST_DELAY_TIMES		10
#define	MPPT_LOST_STABLE_TIMES		120
#define DELAY_AFTER_MPPT_LOST		(3 * 24 * 3600)
/*==========================================================================*
* FUNCTION : JudgeRectLost
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2006-09-11 16:54
*==========================================================================*/
static void JudgeMpptLost(void)
{
	static int	s_iDelay = 0;
	static int  s_iStableDelay = 0;

	int			iQty = GC_GetDwordValue(TYPE_OTHER, 0, MT_PUB_QTY_SELF);
	int			iCfgQty	= GC_GetDwordValue(TYPE_OTHER, 0, MT_PUB_CFG_RECT_NUM);
	int			iCommQty = GC_GetDwordValue(TYPE_OTHER, 0, MT_PUB_COMM_QTY);
	SIG_ENUM		enumTestState;
	DWORD			dwReduceTimes = 0;

	// when user clear rect comm fail alarm, sampler will let rectifiers reallocate, iCfgQty is set to 101 by config file
	// and then clear iCfgQty to zero, wait for reallocation complete
	if(iCfgQty == 101)
	{

		s_iDelay = 0;
		iCfgQty = 0;
		GC_SetDwordValue(TYPE_OTHER,
			0,
			MT_PUB_CFG_RECT_NUM, 
			(DWORD)iCfgQty);
	}

	// wait for n cycles when Power on!
	if(s_iDelay < MPPT_LOST_DELAY_TIMES)
	{
		s_iDelay++;
		return;
	}

	//Update RT_PUB_CFG_RECT_NUM
	if(iQty > iCfgQty)
	{
		GC_SetDwordValue(TYPE_OTHER,
			0,
			MT_PUB_CFG_RECT_NUM, 
			(DWORD)iQty);
		return;
	}

	BOOL iGcAlarmStatus = iCfgQty > iQty ? GC_ALARM : GC_NORMAL;

	if (iGcAlarmStatus)
	{
		if (s_iStableDelay < MPPT_LOST_STABLE_TIMES)
		{
			s_iStableDelay++;
			return;
		}
		else
		{
			if(iCommQty == 0 )
			{
				if(SEC_TMR_SUSPENDED == GC_GetStateOfSecTimer(SEC_TMR_ID_MPPT_LOST_INTERVAL))
				{
					enumTestState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_TEST_STATE);
					dwReduceTimes = GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_TEST_REDUCE_TIMES);
					dwReduceTimes =enumTestState ? dwReduceTimes : 1;

					GC_SetSecTimer(SEC_TMR_ID_MPPT_LOST_INTERVAL,
						DELAY_AFTER_MPPT_LOST / dwReduceTimes,
						NULL,
						0);
				}
			}
			else
			{
				GC_SuspendSecTimer(SEC_TMR_ID_MPPT_LOST_INTERVAL);
			}
			//Continue  process!!
		}
	}
	else
	{
		s_iStableDelay = 0;
	}

	if(iCommQty == 0)
	{
		if(SEC_TMR_TIME_OUT == GC_GetStateOfSecTimer(SEC_TMR_ID_MPPT_LOST_INTERVAL))
		{			 
			//Set alarm signal to ALARM or NORMAL
			GC_SetEnumValue(TYPE_OTHER,
				0,
				MT_PUB_CFG_RECT_LOST,
				iCfgQty > iQty ? GC_ALARM : GC_NORMAL,
				TRUE);
		}
	}
	else
	{
		//Set alarm signal to ALARM or NORMAL
		GC_SetEnumValue(TYPE_OTHER,
			0,
			MT_PUB_CFG_RECT_LOST,
			iCfgQty > iQty ? GC_ALARM : GC_NORMAL,
			TRUE);
		GC_SuspendSecTimer(SEC_TMR_ID_MPPT_LOST_INTERVAL);
	}

	return;	
}
/*==========================================================================*
* FUNCTION : IsMultiMpptFail
* PURPOSE  : Judge if multi-rectifiers fail
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2005-01-03 15:37
*==========================================================================*/
BOOL IsMultiMpptFail(void)
{
	int	i;
	int	iNumOfFailMppt = 0;

	for(i = 0; i < g_pGcData->RunInfo.Mt.iQtyOfRect; i++)
	{
		/*if(((GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT, i, MT_PRI_NIGHT_STATUS))
			&& (!g_pGcData->RunInfo.Mt.bNight)) //only when not AC Failure*/

		if((GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT, i, MT_PRI_PROTCTION)) 
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT, i, MT_PRI_OVER_TEMP))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT, i, MT_PRI_FAULT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT, i, MT_PRI_OVER_VOLT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT, i, MT_PRI_FAN_FAULT))
			|| (GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT, i, MT_PRI_NO_RESPOND)))
		{
			iNumOfFailMppt++;
			if(iNumOfFailMppt > 1)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}
static void	MpptRunInfo(void)
{
	int				i, j;
	float				fCapOfMtWithLmt = 0.0;
	float				fUsedCap = 0.0;
	float				fRatedCurr = 0.0;
	float				fUnitRatedCurr = 0.0, fTotalRatedCurr= 0.0;

	GC_RUN_INFO_MPPT		*pMtRunInfo = &(g_pGcData->RunInfo.Mt);
	int					aiMpptType =TYPE_MPPT_UNIT;

	static	int			s_iMultiMpptFailCounter = 0;
	int				iQtyOfMppt = 0;
	float				fPriUsedCap = 0.0, fTotalUsedCap = 0.0;

	int			aiMTOutCurrSid = MT_PRI_OUT_CURR;
	int			aiMTCurrLimitSid = MT_PRI_CURR_LMT;
	int			aiMTValidCapSid = MT_PRI_VALID_CAP;
	int			aiMTInputCurrent = MT_PRI_INPUT_CURRENT;
	int			aiMTInputVoltage = MT_PRI_INPUT_VOLTAGE;

	int			aiMTInputPower = MT_PRI_INPUT_POWER;
	int			aiMTOutputPower = MT_PRI_OUTPUT_POWER;


	float			fTotalInputCurrent = 0.0, fTotalInputPower= 0.0,fTotalOutputPower= 0.0,fTotalCurrent = 0.0;
	
	static		BOOL		bFirstStart = TRUE;
	static		int	iSleepTimes = 0;
	static	float		fMaxUsedCap = 0.0, fMinUsedCap = 110.0;
	

	pMtRunInfo->fRatedVolt = GC_GetFloatValue(TYPE_OTHER, 0, MT_PUB_RATED_VOLT);


	//Qty of rectifiers
	iQtyOfMppt = GC_GetDwordValue(TYPE_OTHER, 0, MT_PUB_QTY_SELF);
	pMtRunInfo->iQtyOfRect 	= iQtyOfMppt;	

	for(j = 0; j < pMtRunInfo->iQtyOfRect; j++)
	{
		pMtRunInfo->abComm[j] = GC_IsRectComm(TYPE_MPPT_UNIT, j);
		pMtRunInfo->fRatedCurr = GC_GetRectValidCapacity(TYPE_MPPT_UNIT, j);

	}

	//clear virtual alarm signal
	fTotalRatedCurr = 0.0;
	for(i = 0; i < pMtRunInfo->iQtyOfRect; i++)
	{
		if(pMtRunInfo->abComm[i])
		{
			/*GC_SetEnumValue(TYPE_MPPT_UNIT, 
				i,
				MT_PRI_CURR_LMT_SET,
				GC_NORMAL,
				TRUE);	*/

		}
		fTotalRatedCurr += GC_GetFloatValue(TYPE_MPPT_UNIT, 
			i,
			MT_PRI_VALID_CAP);
	}

	/*GC_SetFloatValue(TYPE_OTHER, 
		0,
		MT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR,
		fTotalRatedCurr);*/


	/*for(; i < MAX_NUM_RECT_EQUIP; i++)
	{
		GC_SetEnumValue(TYPE_MPPT_UNIT, 
			i,
			MT_PRI_CURR_LMT_SET,
			GC_NORMAL,
			TRUE);	
	}*/


	GC_MaintainMpptList();	

	pMtRunInfo->bNoRespond = JudgeMpptNoRespond();
	pMtRunInfo->bNight = IsMpptNightStatus(pMtRunInfo->bNight);

	pMtRunInfo->fVolt = GetMpptVolt();
	pMtRunInfo->bValid = IsMpptValid();

	GC_SetFloatValue(TYPE_OTHER, 0, MT_PUB_AVE_VOLT, pMtRunInfo->fVolt);

	//Calculate sum of rectifies current
	pMtRunInfo->fSumOfCurr = 0.0;
	fUnitRatedCurr = 0.0;
	fTotalRatedCurr = 0.0;
	List_GotoHead(pMtRunInfo->hListOnWorkRect);
	do
	{
		GC_MPPT_INFO	RectifierInfo;

		if(List_Get(pMtRunInfo->hListOnWorkRect, 
			(void*)(&RectifierInfo)))
		{
			
			float fPriCurr = GC_GetFloatValue(TYPE_MPPT_UNIT , 
				RectifierInfo.iRectNo,
				aiMTOutCurrSid);
			float fPriLmt = GC_GetFloatValue(TYPE_MPPT_UNIT, 
				RectifierInfo.iRectNo,
				aiMTCurrLimitSid);
			float fInputCurrent = GC_GetFloatValue(TYPE_MPPT_UNIT, 
							RectifierInfo.iRectNo,
							aiMTInputCurrent);
			
			float fInputVoltage = GC_GetFloatValue(TYPE_MPPT_UNIT, 
							RectifierInfo.iRectNo,
							aiMTInputVoltage);
			float fInputPower = GC_GetFloatValue(TYPE_MPPT_UNIT, 
							RectifierInfo.iRectNo,
							aiMTInputPower);
			float fOutputPower = GC_GetFloatValue(TYPE_MPPT_UNIT, 
							RectifierInfo.iRectNo,
							aiMTOutputPower);


			fTotalInputCurrent += fInputCurrent;
			fTotalInputPower += fInputPower;//fInputCurrent*fInputVoltage;
			fTotalOutputPower += fOutputPower;

			fUnitRatedCurr = GC_GetFloatValue(TYPE_MPPT_UNIT, 
				RectifierInfo.iRectNo,
				aiMTValidCapSid);
			
			

			pMtRunInfo->fSumOfCurr 	+= fPriCurr;

			fCapOfMtWithLmt += fUnitRatedCurr * fPriLmt / 100.0;

			fPriUsedCap = 100.0 * fPriCurr / fUnitRatedCurr/GC_CALCULATE_CAPACITY_DELTA;
			fTotalRatedCurr += fUnitRatedCurr;
				
			//manage the self rectifier
			if(fPriUsedCap >=0 && fPriUsedCap <= 200)
			{			
				GC_SetFloatValue(TYPE_MPPT_UNIT, 
					RectifierInfo.iRectNo,
					MT_PRI_USED_CAP,
					fPriUsedCap);

			}
			
			fTotalUsedCap += fPriUsedCap;
			fTotalCurrent += fPriCurr;


			/*if(fPriCurr < (fPriLmt * fUnitRatedCurr * 0.95 / 100))
			{
				GC_SetEnumValue(TYPE_MPPT_UNIT, 
					RectifierInfo.iRectNo,
					MT_PRI_CURR_LMT_SET,
					GC_NORMAL,
					TRUE);
			}
			else if(fPriCurr > (fPriLmt * fUnitRatedCurr * 0.97 / 100))
			{
				GC_SetEnumValue(TYPE_MPPT_UNIT, 
					RectifierInfo.iRectNo,
					MT_PRI_CURR_LMT_SET,
					GC_ALARM,
					TRUE);	
			}*/
		}

	}while(List_GotoNext(pMtRunInfo->hListOnWorkRect));	

	pMtRunInfo->fSumOnRatedCurrent = fTotalRatedCurr;
	if(GC_IsGCSigExist(TYPE_OTHER, 0, MT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR))
	{
		GC_SetFloatValue(TYPE_OTHER, 0, MT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR, fTotalRatedCurr);
	}
	

	List_GotoHead(pMtRunInfo->hListOffWorkRect);
	do
	{
		//GC_RECT_INFO	RectifierInfo;
		GC_MPPT_INFO	RectifierInfo;

		if(List_Get(pMtRunInfo->hListOffWorkRect, 
			(void*)(&RectifierInfo)))
		{
			//printf("Get off rectifier\n");
			//GC_SetEnumValue(TYPE_MPPT_UNIT , 
			//	RectifierInfo.iRectNo,
			//	MT_PRI_CURR_LMT_SET,
			//	GC_NORMAL,
			//	TRUE);	

			GC_SetFloatValue(TYPE_MPPT_UNIT, 
				RectifierInfo.iRectNo,
				MT_PRI_USED_CAP,
				0.0);

		}
	}while(List_GotoNext(pMtRunInfo->hListOffWorkRect));

	
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		MT_PUB_TOTAL_CURR,
		pMtRunInfo->fSumOfCurr);

	if(GC_IsGCSigExist(TYPE_OTHER, 
			0,
			MT_PUB_TOTAL_INPUT_CURRENT))
	{	
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			MT_PUB_TOTAL_INPUT_CURRENT,
			fTotalInputCurrent);
	}
	if(GC_IsGCSigExist(TYPE_OTHER, 
			0,
			MT_PUB_TOTAL_INPUT_POWER))
	{
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			MT_PUB_TOTAL_INPUT_POWER,
			fTotalInputPower);
	}
	if(GC_IsGCSigExist(TYPE_OTHER, 
			0,
			MT_PUB_OUTPUT_POWER))
	{
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			MT_PUB_OUTPUT_POWER,
			fTotalOutputPower/1000);
	}

	//printf("pMtRunInfo->iQtyOfRect = %d,fTotalCurrent = %f fTotalRatedCurr =%f\n",pMtRunInfo->iQtyOfRect,fTotalCurrent,fTotalRatedCurr);
	if(pMtRunInfo->iQtyOfRect)
	{
		if(fTotalUsedCap > 0.1)
		{
			fUsedCap = 100 * fTotalCurrent
				/ (fTotalRatedCurr*GC_CALCULATE_CAPACITY_DELTA);

		}
		else
		{
			fUsedCap = 0.0;
		}

	}
	else
	{
		fUsedCap = 0.0;
	}

	//printf("fUsedCap = %f\n",fUsedCap);
	if(isnan(fUsedCap))
	{
		fUsedCap = 0.0;
	}
	if(fUsedCap > fMaxUsedCap)
	{
		fMaxUsedCap = fUsedCap;
	}
	if(fUsedCap < fMinUsedCap && fUsedCap >  0.1)
	{
		fMinUsedCap = fUsedCap;
	}

	GC_SetFloatValue(TYPE_OTHER, 
		0,
		MT_PUB_USED_CAP,
		fUsedCap);
	GC_SetFloatValue(TYPE_OTHER, 
			0,
			MT_PUB_MAX_USED_CAP,
			fMaxUsedCap);		
	GC_SetFloatValue(TYPE_OTHER, 
			0,
			MT_PUB_MIN_USED_CAP,
			fMinUsedCap);		
	


	if(IsMultiMpptFail())
	{
		if(s_iMultiMpptFailCounter > HYSTERESIS_TIMES_MULTI_RECT_FAIL)
		{
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				MT_PUB_MULTI_RECT_FAULT,
				GC_ALARM, 
				TRUE);
		}
		else
		{
			s_iMultiMpptFailCounter++;	
		}
	}
	else
	{
		s_iMultiMpptFailCounter = 0;
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			MT_PUB_MULTI_RECT_FAULT,
			GC_NORMAL, 
			TRUE);	
	}

	JudgeMpptLost();

	return;
}
#endif
static float GC_GetFiammBatteryCurr()
{
#define MAX_FIAMMBATTERY_NUMBER		8
#define FIAMM_BATTERY_START_ID		3515

	float fSumCurr = 0.0;
	int	i = 0;
	int	iBufLen, iError;
	SIG_BASIC_VALUE*		pSigValue;

	for(i = 0; i< MAX_FIAMMBATTERY_NUMBER; i++)
	{
		iError= DxiGetData(VAR_A_SIGNAL_VALUE,
					FIAMM_BATTERY_START_ID + i,
					DXI_MERGE_SIG_ID(0,2),//Battery current
					&iBufLen,
					(void *)&pSigValue,	
					0);
		if(iError == ERR_DXI_OK && SIG_VALUE_IS_VALID(pSigValue))
		{
			fSumCurr += pSigValue->varValue.fValue;
		}
		
	}
	return fSumCurr;
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_float_charge.h
 *  CREATOR  : Frank Cao                DATE: 2004-10-28 16:44
 *  VERSION  : V1.00
 *  PURPOSE  : To provide Float Charge State handling interface functions
 *             
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _FLOAT_CHARGE_H_
#define _FLOAT_CHARGE_H_

void GC_FloatCharge(void);

#endif //_FLOAT_CHARGE_H_

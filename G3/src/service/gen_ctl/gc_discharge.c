/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_discharge.c
 *  CREATOR  : Frank Cao                DATE: 2004-10-29 11:08
 *  VERSION  : V1.00
 *  PURPOSE  : To provide Battery Test  and AC Fail State handling 
 *             interface functions
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gc_batt_mgmt.h"
#include	"gen_ctl.h"
#include	"gc_sig_value.h"
#include	"gc_boost_charge.h"
#include	"gc_float_charge.h"
#include	"gc_discharge.h"


static void	ImbAlmHandling(void);
static void ConstTestCtl(void);
static BOOL DischCapCmp(void);
static void ConstTestCtlOnEnoughLoad(float fConstTestCurr, 
									 float fRectRatedCurr,BOOL bSleep);

/*==========================================================================*
 * FUNCTION : GC_BattTestStart
 * PURPOSE  : To start a test.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int         iNextState   : 
 *            const char*  szLogOut      : 
 *            int         iStartReason : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-29 16:03
 *==========================================================================*/
void	GC_BattTestStart(int iNextState, 
					const char* szLogOut,
					int iStartReason)
{
	int	i;

	//Set BattTest Timer	
	GC_SetSecTimer(SEC_TMR_ID_BATT_TEST_DURATION,
					0,
					GC_GetBtDuration,
					0);

	//Modify the Battery Management State to iNextState
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BM_STATE,
					iNextState,
					TRUE);

	//Set BT_PUB_TEST_CTL Signal to CTL_TEST_START
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_TEST_CTL,
					CTL_TEST_START,
					TRUE);
	//Set state changed 
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_STATUS_CHANGED,
					GC_ALARM,
					TRUE);

	if(MODE_CONST_CURR_TEST 
		!= GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CON_CURR_TEST_ENB))
	{

		float fVoltOfRt = GC_GetSettingVolt(BT_PUB_TEST_VOLT);
		GC_DcVoltCtlForTest(fVoltOfRt, TRUE);

		Sleep(5000);

		for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{

				GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF + i,
				GC_MAX_CURR_LMT);				

			}		
		}

		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;


		GC_SetFloatValue(TYPE_OTHER, 
						0,
						RT_PUB_CURR_LMT_CTL_SELF, 
						GC_MAX_CURR_LMT);

	}


	//Event Log
	if(szLogOut)
	{
		GC_LOG_OUT(APP_LOG_INFO, szLogOut);
	}

	//Battery Test Log
	GC_TestLogStart(iStartReason);

	return;
}


/*==========================================================================*
 * FUNCTION : GC_BattTestEnd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int         iNextState : 
 *            const char*  szLogOut    : 
 *            int         iEndReason   : 
 *            int         iTestResult  : 
 *            BOOL         bTurnAuto   : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-22 14:34
 *==========================================================================*/
void	GC_BattTestEnd(int iNextState, 
					const char* szLogOut,
					int iEndReason,
					int iTestResult,
					BOOL bTurnToAuto)
{

	//Suspend Test Timer
	GC_SuspendSecTimer(SEC_TMR_ID_BATT_TEST_DURATION);

	//Modify the Battery Management State
	GC_SetEnumValue(TYPE_OTHER, 
				0, 
				BT_PUB_BM_STATE,
				iNextState,
				TRUE);
	//Set state changed 
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_STATUS_CHANGED,
					GC_ALARM,
					TRUE);

	
	//Turn to Auto
	if(bTurnToAuto)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						GC_PUB_AUTO_MAN, 
						STATE_AUTO,
						TRUE);
		if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
		{	
			g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

			CurrentLimitThroughVolt(TRUE ,BT_PUB_NOM_VOLT, FALSE);
		}
		else
		{
			GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);
		}

		Sleep(2500); //to ensure the rectifiers complete the action
	}	
	
	
	//Set Manual Control Signal
	if(ST_FLOAT == iNextState)
	{
		//Control rectifiers volt to Float Voltage
		if((!bTurnToAuto)||(GC_IsNeedVoltAdjust() == FALSE))
		{
			GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);
		}

		GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BOOST_CTL,
					CTL_FLOAT,
					TRUE);	
	}

	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_TEST_CTL,
					CTL_TEST_STOP,
					TRUE);
	
	//Event Log
	if(szLogOut)
	{
		GC_LOG_OUT(APP_LOG_INFO, szLogOut);
	}
	
	//Test Log
	GC_TestLogEnd(iEndReason, iTestResult);

	//Alarm Handling
	if(RESULT_BAD == iTestResult)
	{
		GC_SetEnumValue(TYPE_OTHER,
						0, 
						BT_PUB_BAD_BATT_SET, 
						GC_ALARM,
						TRUE);	
	}

	/*if(GC_NORMAL 
		!= GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_PLAN_TEST_SET))
	{
		GC_SetEnumValue(TYPE_OTHER,
						0, 
						BT_PUB_PLAN_TEST_SET, 
						GC_NORMAL,
						TRUE);
	}

	if(GC_NORMAL 
		!=GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_MAN_TEST_SET))
	{
		GC_SetEnumValue(TYPE_OTHER,
						0, 
						BT_PUB_MAN_TEST_SET, 
						GC_NORMAL,
						TRUE);
	}
	
	if(GC_NORMAL 
		!= GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_AC_FAIL_TEST_SET))
	{
		GC_SetEnumValue(TYPE_OTHER,
						0, 
						BT_PUB_AC_FAIL_TEST_SET, 
						GC_NORMAL,
						TRUE);
	}*/


	return;
}



/*==========================================================================*
 * FUNCTION : GC_ShortTest
 * PURPOSE  : Short Test State handling
 * CALLS    : GC_PreCurrLmt GC_RectVoltCtl GC_GetAlarmNum GC_SuspendSecTimer
              ImbAlmHandling GC_SetEnumValue 
 * CALLED BY: StateMgmt
 * ARGUMENTS:  
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-13 16:47
 *==========================================================================*/
void GC_ShortTest()
{
	GC_RectVoltCtl(TYPE_TEST_VOLT, FALSE);

	if(GC_AlmJudgmentForBT())
	{
		GC_SuspendSecTimer(SEC_TMR_ID_SHORT_TEST_DURATION);
		if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
		{	
			g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

			CurrentLimitThroughVolt(TRUE ,BT_PUB_NOM_VOLT, FALSE);
		}
		else
		{
			GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);
		}

		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE, 
						ST_FLOAT,
						TRUE);
		//Set state changed 
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						GC_PUB_STATUS_CHANGED,
						GC_ALARM,
						TRUE);

		GC_LOG_OUT(APP_LOG_INFO, "CA/MA alarm, turn to FC.\n");
		return;
	}

	if(SEC_TMR_SUSPENDED 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_SHORT_TEST_DURATION))
	{
		GC_SetSecTimer(SEC_TMR_ID_SHORT_TEST_DURATION,
						0,
						GC_GetShortTestDuration,
						0);
		return;
	}

	if(SEC_TMR_TIME_OUT 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_SHORT_TEST_DURATION))
	{
		GC_SuspendSecTimer(SEC_TMR_ID_SHORT_TEST_DURATION);
		
		ImbAlmHandling();
		if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
		{	
			g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

			CurrentLimitThroughVolt(TRUE ,BT_PUB_NOM_VOLT, FALSE);
		}
		else
		{

			GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);
		}

		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE, 
						ST_FLOAT,
						TRUE);
		//Set state changed 
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						GC_PUB_STATUS_CHANGED,
						GC_ALARM,
						TRUE);


		GC_LOG_OUT(APP_LOG_INFO, 
			"Short test end, turn to FC.\n");
		
		return;
	}
	return ;
}



#define	MIN_TEST_LOAD_IN_A	(3.0)
/*==========================================================================*
 * FUNCTION : GC_PlanManTest
 * PURPOSE  : Manual Battery test and Planned Battery Test handling
 * CALLS    : GC_BattTestEnd GC_GetEnumValue 
              ConstTestCtl GC_RectVoltCtl GC_GetBattFuseAlm GC_GetFloatValue
			  DischCapCmp
 * CALLED BY: StateMgmt
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-20 18:46
 *==========================================================================*/
void GC_PlanManTest(SIG_ENUM BmState)
{
	BOOL			bConstTestMode;

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif
	UNUSED(BmState);

	if(g_pGcData->RunInfo.Rt.bAcFail)
	//All of rectifiers AC failure
	{
		GC_BattTestEnd(ST_AC_FAIL, 
				"AC failure, turn to AC Fail.\n",
				END_AC_FAIL, 
				RESULT_NONE,
				TRUE);
		return;
	}
#ifdef GC_SUPPORT_MPPT
	else if(stState != GC_MPPT_DISABLE)
	{
		//No test in MPPT.
		GC_BattTestEnd(ST_FLOAT, 
			"Mppt mode, turn to Float Charge.\n",
			END_OTHER, 
			RESULT_NONE,
			TRUE);
		return;
	}
#endif
	//AC not failure
	else
	{
		GC_TestLogHandling();

		//Get Constant Current Test Signal
		if(MODE_CONST_CURR_TEST 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CON_CURR_TEST_ENB))
		{
			bConstTestMode = TRUE;
		}
		else
		{
			bConstTestMode = FALSE;
		}

		if(bConstTestMode)
		{
			ConstTestCtl();
		}
		else
		{
			GC_RectVoltCtl(TYPE_TEST_VOLT, FALSE);
		}

		//Manually stop the test
		if(CTL_TEST_STOP 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
		{
			if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
			{
				GC_BattTestEnd(ST_FLOAT, 
						"Manually stop the battery test, turn to FC.\n",
						END_MANUAL, 
						RESULT_NONE,
						TRUE);
			}
			else
			{
				GC_BattTestEnd(ST_FLOAT, 
						"Manually stop the battery test, turn to FC.\n",
						END_MANUAL, 
						RESULT_NONE,
						FALSE);
			}
			return;		
		}

		//Alarm condition
	
#ifndef   GC_SUPPORT_HIGHTEMP_DISCONNECT

		if(GC_GetBattFuseAlm())
		{
			GC_BattTestEnd(ST_FLOAT, 
					"Battery fuse alarm, stop battery test and turn to FC.\n",
					END_ALARM, 
					RESULT_NONE,
					TRUE);
			//if(STATE_MAN == g_pGcData->RunInfo.enumAutoMan)
			{
				GC_SetEnumValue(TYPE_OTHER, 
								0, 
								BT_PUB_BATT_TEST_FAIL_ALM,
								GC_ALARM,
								TRUE);
			}

			return;			
		}

#endif   //GC_SUPPORT_HIGHTEMP_DISCONNECT
		
		if(GC_AlmJudgmentForBT())		
		{
			char	strOut[254];

			sprintf(strOut, 
				"CA(%d)/MA(%d) alarm, stop battery test and turn to FC.\n",
				GC_GetAlarmNum(ALARM_LEVEL_CRITICAL),
				GC_GetAlarmNum(ALARM_LEVEL_MAJOR));

			GC_BattTestEnd(ST_FLOAT, 
					strOut,
					END_ALARM, 
					RESULT_NONE,
					TRUE);
			
			//if(STATE_MAN == g_pGcData->RunInfo.enumAutoMan)
			{
				GC_SetEnumValue(TYPE_OTHER, 
								0, 
								BT_PUB_BATT_TEST_FAIL_ALM,
								GC_ALARM,
								TRUE);
			}

			return;			
		}


		//time condition
		if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_BATT_TEST_DURATION))
		{
			GC_BattTestEnd(ST_FLOAT,
					"Time condition fulfilled, stop battery test and turn to FC.\n",
					END_TIME, 
					RESULT_GOOD,
					TRUE);
			return;
		}

		if(DischCapCmp())
		{
			GC_BattTestEnd(ST_FLOAT, 
				"Capacity condition fulfilled, stop battery test and turn to FC.\n",
				END_CAPACITY, 
				RESULT_GOOD,
				TRUE);
			return;
		}

		// voltage condition
		if(g_pGcData->RunInfo.fSysVolt < GC_GetSettingVolt(BT_PUB_TEST_END_VOLT))
		{
			GC_BattTestEnd(ST_FLOAT, 
				"Voltage condition fulfilled, stop battery test and turn to FC.\n",
				END_VOLTAGE, 
				RESULT_BAD,
				TRUE);
			return;
		}

		//Victor Fan for CR# 0175-07-SCU, 2007-06-13 */
		//Asked by Brian, to remove battery test end condition 2015-4-3
		//if(g_pGcData->RunInfo.fSysLoad < MIN_TEST_LOAD_IN_A)
		//{
		//	GC_BattTestEnd(ST_FLOAT, 
		//		"System Load is less than 3A, stop battery test and turn to FC.\n",
		//		END_LOW_LOAD, 
		//		RESULT_NONE,
		//		TRUE);

		//	//if(STATE_MAN == g_pGcData->RunInfo.enumAutoMan)
		//	{
		//		GC_SetEnumValue(TYPE_OTHER, 
		//						0, 
		//						BT_PUB_BATT_TEST_FAIL_ALM,
		//						GC_ALARM,
		//						TRUE);
		//	}

		//	return;
		//}
	}
	return;
}


/*==========================================================================*
 * FUNCTION : GC_AcFail
 * PURPOSE  : AC-Fail State handling
 * CALLS    : GC_GetEnumValue
 * CALLED BY: StateMgmt
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-22 10:14
 *==========================================================================*/
void GC_AcFail()
{
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);

#ifdef GC_SUPPORT_MPPT
	if((stState == GC_DISABLED && g_pGcData->RunInfo.Rt.bAcFail)
		|| (stState ==  GC_MPPT_MPPT_RECT && g_pGcData->RunInfo.Rt.bAcFail && g_pGcData->RunInfo.Mt.bNight)
		|| (stState ==  GC_MPPT_MPPT && g_pGcData->RunInfo.Mt.bNight) )
#else
	if(g_pGcData->RunInfo.Rt.bAcFail)
#endif
	{

		if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
		//Automatically
		{

			if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
			{

				CurrentLimitThroughVolt(TRUE ,BT_PUB_NOM_VOLT, FALSE);
			}
			else
			{

				if(RECTIFIER_DOUBLE_POWER_TYPE 
					== GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_POWER_TYPE))
				{
	#ifdef GC_SUPPORT_MPPT
					if(stState != GC_MPPT_DISABLE)
					{
						GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect + g_pGcData->RunInfo.Mt.iQtyOnWorkRect, TRUE, FALSE);
					}
					else
					{
						GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect, TRUE, FALSE);
					}

	#else
			GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect, TRUE, FALSE);
	#endif

				}

				else
				{
	#ifdef GC_SUPPORT_MPPT
					if(stState != GC_MPPT_DISABLE)
					{
						GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect + g_pGcData->RunInfo.Mt.iQtyOnWorkRect, TRUE, FALSE);
					}
					else
					{
						GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect, TRUE, FALSE);
					}
	 
					GC_PreCurrLmt_Mppt(g_pGcData->RunInfo.Mt.iQtyOfRect);
	 

	#else
					 GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect, TRUE, FALSE);
	#endif
	               
				}
			}
		}
	}
	else
	{
		SIG_ENUM	enumGridState, enumState;
		enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
		enumGridState =  GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get Grid state

		//�������Hybridģʽ�£��Ҵ���grid off״̬�£����ͣ����Խ���֮����ֱ��ת���䡣
		//���Ƿѹ��ת����ʱ�����н���ͣ�����ʱ��ת���䡣���DG on��ʼ���޷�ת���䡣
		if(enumGridState== GC_HYBRID_GRID_OFF && enumState != GC_HYBRID_DISABLED)
		{
			GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						ST_AUTO_BC,
						TRUE);

			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							BT_PUB_BOOST_CTL,
							CTL_BOOST,
							TRUE);

			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							BT_PUB_TEST_CTL,
							CTL_TEST_STOP,
							TRUE);


			GC_LOG_OUT(APP_LOG_INFO, 
				"AC Power restore, turn to BC.\n");
		}
		else
		{
			GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						ST_FLOAT,
						TRUE);

			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							BT_PUB_BOOST_CTL,
							CTL_FLOAT,
							TRUE);

			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							BT_PUB_TEST_CTL,
							CTL_TEST_STOP,
							TRUE);


			GC_LOG_OUT(APP_LOG_INFO, 
				"AC Power restore, turn to FC.\n");
		}
		
	}

	GC_SetEnumValue(TYPE_OTHER, 
					0,
					BT_PUB_TEMP_COMPEN_SET,
					GC_NORMAL,
					FALSE);	

	return;
}




/*==========================================================================*
 * FUNCTION : GC_AcFailTest
 * PURPOSE  : AC-Fail Test State handling
 * CALLS    : GC_GetEnumValue GC_BattTestEnd DischCapCmp
              GC_GetFloatValue
 * CALLED BY: StateMgmt
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-17 10:29
 *==========================================================================*/
void GC_AcFailTest()
{
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);	
	GC_TestLogHandling();
	
	if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
	//Automatically
	{
		if(GC_IsNeedVoltAdjust() == TRUE ) 
		{	
			CurrentLimitThroughVolt(TRUE ,BT_PUB_TEST_VOLT, FALSE);
		}
		else
		{

			if(RECTIFIER_DOUBLE_POWER_TYPE 
			== GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_POWER_TYPE))
			{
				 GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, TRUE, FALSE);
			}
			else
			{
				GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect, TRUE, FALSE);
			}
		}
	}

#ifdef GC_SUPPORT_MPPT
	if((stState  ==  GC_MPPT_MPPT_RECT && (!g_pGcData->RunInfo.Rt.bAcFail || !g_pGcData->RunInfo.Mt.bNight))
		|| (stState  ==  GC_MPPT_MPPT && !g_pGcData->RunInfo.Mt.bNight)
		|| (stState  ==  GC_MPPT_DISABLE && !g_pGcData->RunInfo.Rt.bAcFail))
#else
	if(!(g_pGcData->RunInfo.Rt.bAcFail))
#endif

	//AC Fail restore
	{
		SIG_ENUM	enumGridState, enumState;
		enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
		enumGridState =  GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get Grid state

		//�������Hybridģʽ�£��Ҵ���grid off״̬�£����ͣ����Խ���֮����ֱ��ת���䡣
		//���Ƿѹ��ת����ʱ�����н���ͣ�����ʱ��ת���䡣���DG on��ʼ���޷�ת���䡣
		if(enumGridState== GC_HYBRID_GRID_OFF && enumState != GC_HYBRID_DISABLED)
		{
			GC_BattTestEnd(ST_AUTO_BC, 
						"AC Power restore, turn to BC.\n",
						END_AC_RESTORE, 
						RESULT_NONE,
						FALSE);
		}
		else
		{
			GC_BattTestEnd(ST_FLOAT, 
						"AC Power restore, turn to FC.\n",
						END_AC_RESTORE, 
						RESULT_NONE,
						FALSE);
		}
		return;
	}

	if(GC_GetBattFuseAlm())
	{
		GC_BattTestEnd(ST_AC_FAIL, 
					"Battery fuse alarm, turn to AC Fail.\n",
					END_ALARM, 
					RESULT_NONE,
					FALSE);
		return;	
	}

	//Manually stop the test
	if(CTL_TEST_STOP 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
	{
		GC_BattTestEnd(ST_AC_FAIL, 
				"Manually stop the battery test, turn to AC Fail.\n",
				END_MANUAL, 
				RESULT_NONE,
				FALSE);
			return;		
	}
	
	//Fulfil time condition
	if(SEC_TMR_TIME_OUT 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_BATT_TEST_DURATION))
	{
		GC_BattTestEnd(ST_AC_FAIL, 
			"Time condition fulfilled, stop battery test and turn to AC Fail.\n",
			END_TIME, 
			RESULT_GOOD,
			FALSE);
		return;
	}

	if(DischCapCmp())
	{
		GC_BattTestEnd(ST_AC_FAIL, 
			"Capacity condition fulfilled, stop battery test and turn to AC Fail.\n",
				END_CAPACITY, 
				RESULT_GOOD,
				FALSE);
		return;
	}

	if(g_pGcData->RunInfo.fSysVolt < GC_GetSettingVolt(BT_PUB_TEST_END_VOLT))
	{
		GC_BattTestEnd(ST_AC_FAIL, 
			"Voltage condition fulfilled, stop battery test and turn to AC Fail.\n",
				END_VOLTAGE, 
				RESULT_BAD,
				FALSE);
		return;
	}

	GC_SetEnumValue(TYPE_OTHER, 
					0,
					BT_PUB_TEMP_COMPEN_SET,
					GC_NORMAL,
					FALSE);	

	return;
}



/*==========================================================================*
 * FUNCTION : ImbAlmHandling
 * PURPOSE  : To judge the batteries current, if there is big difference
              then set Discharge Current Imbalance alarm
 * CALLS    : 
 * CALLED BY: GC_ShortTest
 * ARGUMENTS: TYPE_GEN_CTL_DATA*  g_pGcData : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-14 16:26
 *==========================================================================*/
static void	 ImbAlmHandling()
{
	int		i, j;
	float	fMaxCurr, fMaxBatRatedCurr = 0;
	float	fDifference;

	/*Calculate maximum difference battery current */
	
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(fMaxBatRatedCurr < g_pGcData->RunInfo.Bt.afBattRatedCap[i])
		{
			fMaxBatRatedCurr = g_pGcData->RunInfo.Bt.afBattRatedCap[i];
		}
	}

	fMaxCurr = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_MAX_DIFF)
			* fMaxBatRatedCurr;


	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		

		if(!(g_pGcData->RunInfo.Bt.abValid[i]))
		{
			continue;
		}

		for(j = i + 1; j < GC_Get_Total_BattNum(); j++)
		{
			if(!(g_pGcData->RunInfo.Bt.abValid[j]))
			{
				continue;
			}
			
			fDifference = g_pGcData->RunInfo.Bt.afBattCurr[i] 
						- g_pGcData->RunInfo.Bt.afBattCurr[j];
			fDifference = ABS(fDifference);
			if(fDifference > fMaxCurr)
			{
				GC_SetEnumValue(TYPE_OTHER, 
								0, 
								BT_PUB_DISCH_IMB_SET, 
								GC_ALARM,
								TRUE);
				return;
			}
		}
	}
	return;
}



/*==========================================================================*
 * FUNCTION : ConstTestCtl
 * PURPOSE  : To control rectifier voltage and current limit in a constant 
              current battery test
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-30 17:13
 *==========================================================================*/
#define	LOAD_CURR_HYSTERESIS	1
static void ConstTestCtl(void)
{
	float	fLoadCurr;
	float	fRectRatedCurr;
	float	fConstTestCurr;
	//float	fDelta;
	static	BOOL	s_bLastState = TRUE;	//It means fLoadCurr < fConstTestCurr

	//Calculate the total of load current
	fLoadCurr = g_pGcData->RunInfo.Rt.fSumOfCurr
		- g_pGcData->RunInfo.Bt.fSumOfCurr;

	//Get constant test current
	fConstTestCurr = GC_GetFloatValue(TYPE_OTHER, 
									0,
									BT_PUB_CONST_TEST_CURR);

	//Get rectifier reated current
	fRectRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;

	if(fLoadCurr < (fConstTestCurr - LOAD_CURR_HYSTERESIS))
	{
		GC_RectVoltCtl(TYPE_TEST_VOLT, FALSE);
		s_bLastState = TRUE;
	}
	else if(fLoadCurr < fConstTestCurr)
	{
		if(s_bLastState)
		{
			GC_RectVoltCtl(TYPE_TEST_VOLT, FALSE);		
		}
		else
		{
			ConstTestCtlOnEnoughLoad(fConstTestCurr, fRectRatedCurr,s_bLastState);
		}
	}
	else //if(fLoadCurr > fConstTestCurr)
	{
		ConstTestCtlOnEnoughLoad(fConstTestCurr, fRectRatedCurr,s_bLastState);
		s_bLastState = FALSE;
	}
	
	return;
}

/*==========================================================================*
 * FUNCTION : GC_ShortTestIntervalTimerCheck
 * PURPOSE  : To check the SEC_TMR_ID_SHORT_TEST_INTERVAL timer if need to 
              start
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-07 15:54
 *==========================================================================*/
void	GC_ShortTestIntervalTimerCheck(void)
{
	if(!(g_pGcData->RunInfo.Bt.bDischarge))
	//Not discharge
	{
		if(SEC_TMR_SUSPENDED 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_SHORT_TEST_INTERVAL))
		{

			GC_SetSecTimer(SEC_TMR_ID_SHORT_TEST_INTERVAL, 
						0,
						GC_GetShortTestInterval,
						0);			
		}
	}
	else
	{
		GC_SuspendSecTimer(SEC_TMR_ID_SHORT_TEST_INTERVAL);
	}
	return;
}

/*==========================================================================*
 * FUNCTION : GC_ShortTestStart
 * PURPOSE  : To start a short test
 * CALLS    : GC_SuspendSecTimer GC_SetEnumValue GC_GetDwordValue 
              GC_SetSecTimer
 * CALLED BY: GC_FloatCharge
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-31 16:07
 *==========================================================================*/
void	GC_ShortTestStart()
{
	int	i;

	GC_SuspendSecTimer(SEC_TMR_ID_SHORT_TEST_INTERVAL);
	
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BM_STATE,
					ST_SHORT_TEST,
					TRUE);

	//Set state changed 
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_STATUS_CHANGED,
					GC_ALARM,
					TRUE);

	float fVoltOfRt = GC_GetSettingVolt(BT_PUB_TEST_VOLT);
	GC_DcVoltCtlForTest(fVoltOfRt, TRUE);

	Sleep(5000);

	for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
	{
		if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
		{

			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF + i,
				GC_MAX_CURR_LMT);				

		}		
	}

	g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;

	GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF, 
					GC_MAX_CURR_LMT);

	

	GC_LOG_OUT(APP_LOG_INFO, "Turn to Short Test.\n");

	return;
}


/*==========================================================================*
 * FUNCTION : DischCapComp
 * PURPOSE  : To judge if discharge capacity is greater than set point
 * CALLS    : GC_GetFloatValue
 * CALLED BY: GC_PlanManTest GC_AcFailTest
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-31 17:52
 *==========================================================================*/
static BOOL DischCapCmp()
{
	int		i;
	float	fTestEndCap,fRatedCap;
	char		szLogOut[128];
	BOOL	bRTN;



	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i ,BT_PRI_RATED_CAP,&fRatedCap);
			if(bRTN == FALSE)
				continue;

			fTestEndCap = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_TEST_END_CAP)* fRatedCap/ 100;

			float	fBattCap ;
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i ,BT_PRI_CAP,&fBattCap);
			if(bRTN == FALSE)
				continue;
				
			if(fBattCap < fTestEndCap)
			{
				//sprintf(szLogOut, "Battery Unit is %f\n", fBattCap);
				//GC_LOG_OUT(APP_LOG_INFO, szLogOut);

				return TRUE;
			}
		}
	}
	return FALSE;
}


static void ConstTestCtlOnEnoughLoad(float fConstTestCurr, float fRectRatedCurr,BOOL bSleep)
{
	float	fDelta;
	int	i;
	

	if(SEC_TMR_TIME_IN 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_CONST_TEST_INTERVAL))
	{
		return;
	}

	GC_SetSecTimer(SEC_TMR_ID_CONST_TEST_INTERVAL, 
				0, 
				GC_GetCurrLmtPeriod, 
				0);

	//calculate and set expected rectifier current limit
	if(g_pGcData->RunInfo.Rt.iQtyOnWorkRect			//These are valid rectifers
		&& (((-g_pGcData->RunInfo.Bt.fSumOfCurr) 
				> (fConstTestCurr * 1.03))
			|| ((-g_pGcData->RunInfo.Bt.fSumOfCurr) 
				< (fConstTestCurr * 0.97))))		//not in dead area
	{
		fDelta = ((-100.0 
				* (fConstTestCurr + g_pGcData->RunInfo.Bt.fSumOfCurr))
				/ g_pGcData->RunInfo.Rt.iQtyOnWorkRect)
				/ fRectRatedCurr;
		if((fDelta < 2.0) && (fDelta > (-2.0)))
		{
			fDelta = fDelta * 0.7;
			if((fDelta < 0.1) && (fDelta > 0.0))
			{
				fDelta = 0.1;
			}
			if((fDelta > (-0.1)) && (fDelta < 0.0))
			{
				fDelta = -0.1;
			}
		}
	}
	else
	{
		fDelta = 0.0;
	}

	if((g_pGcData->RunInfo.Rt.fCurrLmt 
			* g_pGcData->RunInfo.Rt.iQtyOnWorkRect 
			* fRectRatedCurr / 100) 
			> g_pGcData->RunInfo.Rt.fSumOfCurr * 1.05)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt 
			= 100.0 * g_pGcData->RunInfo.Rt.fSumOfCurr 
				/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect
				* fRectRatedCurr);
	}		
		
	g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;

	if(g_pGcData->RunInfo.Rt.fCurrLmt > GC_MAX_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
	}
	/* Added by Marco Yang, 2011-04-27, for NA's requirement:Limit the system max output */
	if(g_pGcData->RunInfo.Rt.fCurrLmt  > g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt  = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;
	}
	/* End by Marco Yang, 2011-04-27, for NA's requirement:Limit the system max output */
        
	if(g_pGcData->RunInfo.Rt.fCurrLmt < GC_MIN_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MIN_CURR_LMT;
	}

	/* Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
	if(GC_IsDislPwrLmtMode()) 
	{
		float fDislPwrLmtPoint = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DISL_PWR_LMT_POINT);
		if(g_pGcData->RunInfo.Rt.fCurrLmt > fDislPwrLmtPoint)
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = fDislPwrLmtPoint;
		}
	}
	/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */

	GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF,
					g_pGcData->RunInfo.Rt.fCurrLmt);

	//for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
	//{
	//	if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
	//	{

	//		GC_SetFloatValue(TYPE_OTHER, 
	//				0,
	//				RT_PUB_CURR_LMT_CTL_SELF + i,
	//				g_pGcData->RunInfo.Rt.fCurrLmt);				

	//	}		
	//}
	GC_CurrlmtCtl_Slave(g_pGcData->RunInfo.Rt.fCurrLmt);

	if(bSleep)
	{
		Sleep(2500);
	}
	GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);	
	return;

}


int GC_GetBtDuration(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN
		* GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_TEST_DURATION);
}

int GC_GetShortTestDuration(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN 
		* GC_GetDwordValue(TYPE_OTHER, 
							0,
							BT_PUB_SHORT_TEST_DURATION);
}

int GC_GetShortTestInterval(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_DAY
		* GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_NO_DISCH_TIME);
}


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_boost_charge.c
 *  CREATOR  : Frank Cao                DATE: 2004-10-29 10:29
 *  VERSION  : V1.00
 *  PURPOSE  : To provide Boost Charge State handling interface functions
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gc_batt_mgmt.h"
#include	"gen_ctl.h"
#include	"gc_sig_value.h"
#include	"gc_boost_charge.h"
#include	"gc_float_charge.h"
#include	"gc_discharge.h"

#ifdef	_GC_TEST
#include	"gc_picket.h"
#endif

int testi = 255;
/*==========================================================================*
 * FUNCTION : GC_BoostChargeStart
 * PURPOSE  : The handling of a Boost Charge Start
 * CALLS    : GC_SuspendSecTimer GC_GetDwordValue GC_SetEnumValue 
 * CALLED BY: GC_FloatCharge 
 * ARGUMENTS: BYTE         iNextState : 
 *            const char*  szLogOut    : 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-30 15:03
 *==========================================================================*/
void	GC_BoostChargeStart(int iNextState, 
					const char* szLogOut)
{

	//if(!g_pGcData->RunInfo.Bt.iQtyValidBatt)
	//{
	//	//Set Battery Management State
	//	GC_SetEnumValue(TYPE_OTHER, 
	//				0, 
	//				BT_PUB_BM_STATE,
	//				ST_FLOAT,
	//				TRUE);	
	//	GC_LOG_OUT(APP_LOG_INFO, "There is no battery, don't start boost charge ,turn to float");
	//	return; 
	//}
	//Control rectifiers volt to Boost Voltage
	if(GC_IsNeedVoltAdjust() == TRUE )  
	{	
		g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

		CurrentLimitThroughVolt(FALSE ,BT_PUB_BST_VOLT, FALSE);
	}
	else
	{
		GC_RectVoltCtl(TYPE_BOOST_VOLT, FALSE);
	}

	//Bc Duration Timer Handling
	if((ST_BC_FOR_TEST == iNextState) || (ST_CYCLIC_BC == iNextState))
	{
		GC_SetSecTimer(SEC_TMR_ID_CYCLIC_BC_DURATION,
					0,
					GC_GetIntervalCycBcDuration,
					0);
	}

	//Manual Control Signal handling
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BOOST_CTL,
					CTL_BOOST,
					TRUE);	

	//Set Battery Management State
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BM_STATE,
					iNextState,
					TRUE);	

	//Set state changed 
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_STATUS_CHANGED,
					GC_ALARM,
					TRUE);	


	//Event Log
	if(szLogOut)
	{
		GC_LOG_OUT(APP_LOG_INFO, szLogOut);
	}

	return;
}

/*==========================================================================*
 * FUNCTION : GC_BoostChargeEnd
 * PURPOSE  : The handling of a Boost Charge End
 * CALLS    : GC_GetEnumValue GC_GetDwordValue GC_SetEnumValue  
              GC_SetSecTimer GC_SuspendSecTimer
 * CALLED BY: GC_CycPreBC GC_ManAutoBC
 * ARGUMENTS: BYTE         iNextState : 
 *            const char*  szLogOut    : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-30 15:00
 *==========================================================================*/
void	GC_BoostChargeEnd(int iNextState,
						   BOOL bIsPerfectBC,
						   const char* szLogOut)
{

	SIG_ENUM	enumState;

	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	//Control rectifiers volt to Float Voltage
	if(GC_IsNeedVoltAdjust() == TRUE)
	{	
		g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

		CurrentLimitThroughVolt(FALSE ,BT_PUB_NOM_VOLT, FALSE);
	}
	else
	{
		GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);
	}

	//Stable Boost Charge timer
	GC_SuspendSecTimer(SEC_TMR_ID_STABLE_BC);

	//Cyclic Boost Charge Duration timer
	GC_SuspendSecTimer(SEC_TMR_ID_CYCLIC_BC_DURATION);
	if(!GC_IsGCSigExist(TYPE_OTHER, 0, BT_PUB_CYC_START_TIME))
	{	GC_SetSecTimer(SEC_TMR_ID_CYCLIC_BC_INTERVAL,
				0,
				GC_GetIntervalCycBcInterval,
				0);
	}
	GC_SuspendSecTimer(SEC_TMR_ID_FC_TO_BC_DELAY);

	//If Boost Charge is perfect
	if(bIsPerfectBC)
	{

		GC_SetSecTimer(SEC_TMR_ID_AFTER_BC, 
						INTERVAL_AFTER_BC,
						NULL,
						0);

		g_pGcData->RunInfo.Bt.bNeedRegulateCap = TRUE;
	}
	
	//Manual Control Signal handling
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BOOST_CTL,
					CTL_FLOAT,
					TRUE);		

	
	//Control rectifiers volt to Float Voltage
	/*if(GC_IsNeedVoltAdjust() == TRUE)
	{	
		g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

		CurrentLimitThroughVolt(FALSE ,BT_PUB_NOM_VOLT, FALSE);
	}
	else
	{
		GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);
	}*/

	//Set state changed 
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_STATUS_CHANGED,
					GC_ALARM,
					TRUE);	


	//To wait system voltage back to float voltage, prevent DC High Voltage alarm raising.
	if(g_pGcData->enumMasterSlaveMode == MODE_MASTER && (GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST,GC_EQUIP_INVALID)))
	{
		Sleep(15000);
		
		//Battery Management State
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						iNextState,
						TRUE);	
		
		//Event Log
		if(szLogOut)
		{
			GC_LOG_OUT(APP_LOG_INFO, szLogOut);
		}
	}
	else
	{
		Sleep(5000);
		
		//Battery Management State
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						iNextState,
						TRUE);	
		
		//Event Log
		if(szLogOut)
		{
			GC_LOG_OUT(APP_LOG_INFO, szLogOut);
		}
	}

	return;
}


/*==========================================================================*
 * FUNCTION : GC_CycPreBC
 * PURPOSE  : Cyclic Boost Charge State handling
 * CALLS    : GC_GetEnumValue GC_BoostChargeEnd GC_BattTestStart
              GC_GetBattFuseAlm
 * CALLED BY: StateMgmt
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-30 17:12
 *==========================================================================*/
void GC_CycPreBC(void)
{
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#ifdef GC_SUPPORT_MPPT

	if((stState  ==  GC_MPPT_MPPT_RECT && g_pGcData->RunInfo.Rt.bAcFail && g_pGcData->RunInfo.Mt.bNight)
	   || (stState  ==  GC_MPPT_MPPT && g_pGcData->RunInfo.Mt.bNight)
	   || (stState  ==  GC_MPPT_DISABLE && g_pGcData->RunInfo.Rt.bAcFail))
#else
	if(g_pGcData->RunInfo.Rt.bAcFail)
#endif
	//All of rectifiers AC failure
	{		
		if(GC_DISABLED 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_AC_FAIL_TEST_ENB))	
		{		//not permit Ac-fail test
			GC_BoostChargeEnd(ST_AC_FAIL,
						FALSE,
						"AC failure, turn to AC Fail.\n");
		}
		else
		{
			GC_BoostChargeEnd(ST_AC_FAIL_TEST,
						FALSE,
						"AC failure, turn to AC Fail Test.\n");

			GC_BattTestStart(ST_AC_FAIL_TEST, 
						NULL,
						START_AC_FAIL_TEST);
		}
		return;
	}

	//Not all of rectifiers AC failure

	//Manually Float Charge
	if(CTL_FLOAT 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BOOST_CTL))
	{
		/*GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);*/

		GC_BoostChargeEnd(ST_FLOAT,
				FALSE,
				"Manual FC, turn to FC.\n");
		return;
	}
#ifdef GC_SUPPORT_MPPT
	if(stState  ==  GC_MPPT_DISABLE && CTL_TEST_START 
					== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
#else		
	//Manually Battery Test
	if(CTL_TEST_START 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
#endif
	{
		GC_BoostChargeEnd(ST_MAN_TEST,
				FALSE,
				"Manual start test, turn to Manual Test.\n");
		GC_BattTestStart(ST_MAN_TEST, 
				NULL,
				START_MANUAL_TEST);
		return;
	}
#define VALID_EQ_DISABLE 1
	if(VALID_EQ_DISABLE
		==GC_GetEnumValue(TYPE_OTHER,0, BT_PUB_CYC_VALID_ENABLE))
	{
		//printf("Valid EQ is set to 0 by PLC \n ");
		GC_BoostChargeEnd(ST_FLOAT,
				TRUE,
				"Valid EQ is set to Disable  !\n");
		return ;
	}
	if(STATE_AUTO != g_pGcData->RunInfo.enumAutoMan)
	//Manual State
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						ST_MAN_BC,
						TRUE);
		int	i;	
		for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
		{

			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				RT_PUB_AC_ON_OFF_CTL_SELF + i,
				GC_RECT_ON,
				TRUE);

			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				RT_PUB_DC_ON_OFF_CTL_SELF + i,
				GC_RECT_ON,
				TRUE);
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF + i,
				GC_MAX_CURR_LMT);
		}						
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
		GC_LOG_OUT(APP_LOG_INFO, 
			"AUTO turn to MAN, turn to Manual BC.\n");

		return;
	}

	//Automatically
	
	//Cyclic Boost Charge Enabled
	//if(GC_DISABLED 
	//	== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CYC_BC_ENB))
	//		
	//not permit Cyclic Boost Charge
	//{
	//	GC_BoostChargeEnd(ST_FLOAT,
	//			FALSE,
	//			"Cyclic BC is disabled, turn to FC.\n");
	//	return;
	//}
			
	//If alarm condition is fulfilled, turn to FC
	if(GC_AlmJudgmentForBC())
	{
		GC_BoostChargeEnd(ST_FLOAT,
				FALSE,
				"Some alarm raise, turn to FC.\n");
		return;			
	}

	//If BC Duration timer is timeout
	if(SEC_TMR_TIME_OUT 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_CYCLIC_BC_DURATION))
	{
		GC_BoostChargeEnd(ST_FLOAT,
			TRUE,
			"Cyc-BC Duration Timer timeout, turn to FC.\n");
	
		return;						
	}

	if(GC_IsNeedVoltAdjust() == TRUE)  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
	{	

		CurrentLimitThroughVolt(FALSE ,BT_PUB_BST_VOLT, FALSE);
	}
	else
	{

		GC_RectVoltCtl(TYPE_BOOST_VOLT, FALSE);
		GC_CurrentLmt();
	}
	
	return;
}



/*==========================================================================*
 * FUNCTION : GC_ManAutoBC
 * PURPOSE  : To handle ManBC state and AutoBC state
 * CALLS    : GC_GetEnumValue
 * CALLED BY: StateMgmt
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-29 11:53
 *==========================================================================*/
void GC_ManAutoBC(SIG_ENUM BmState)
{
	static	BOOL		s_bManualBCInAuto = FALSE;
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
/*test*/
	BOOL bRTN;
	float fBattCap;
		if(testi != 255)
		{
			bRTN =  GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT,testi, BT_PRI_CAP,&fBattCap); 

		}
/*test*/
#ifdef GC_SUPPORT_MPPT

	if((stState  ==  GC_MPPT_MPPT_RECT && g_pGcData->RunInfo.Rt.bAcFail && g_pGcData->RunInfo.Mt.bNight)
		|| (stState  ==  GC_MPPT_MPPT && g_pGcData->RunInfo.Mt.bNight)
		|| (stState  ==  GC_MPPT_DISABLE && g_pGcData->RunInfo.Rt.bAcFail))
#else
	if(g_pGcData->RunInfo.Rt.bAcFail)
#endif
	{		
		if(GC_DISABLED 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_AC_FAIL_TEST_ENB))
		{		//not permit Ac-fail test
			GC_BoostChargeEnd(ST_AC_FAIL,
						FALSE,
						"AC failure, turn to AC Fail.\n");
		}
		else
		{
			SIG_ENUM	enumGridState, enumState;
			enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
			enumGridState =  GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get Grid state

			//�������Hybridģʽ�£��Ҵ���grid off״̬�£����ͣ����Խ���֮����ֱ��ת���䡣
			//���Ƿѹ��ת����ʱ�����н���ͣ�����ʱ��ת���䡣���DG on��ʼ���޷�ת���䡣
			if(enumGridState== GC_HYBRID_GRID_OFF && enumState != GC_HYBRID_DISABLED)
				
			{
				//Don't change to AC fail test.
			}
			else
			{
				GC_BoostChargeEnd(ST_AC_FAIL_TEST,
							FALSE,
							"AC failure, turn to AC Fail Test.\n");

				GC_BattTestStart(ST_AC_FAIL_TEST, 
							NULL,
							START_AC_FAIL_TEST);
			}
		}
		return;
	}
	
	//Not all of rectifiers AC failure

	//Manually Float Charge
	if(CTL_FLOAT 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BOOST_CTL))
	{
		GC_BoostChargeEnd(ST_FLOAT,
				FALSE,
				"Manual FC, turn to FC.\n");
		return;
	}

#ifdef GC_SUPPORT_MPPT
	if(stState  ==  GC_MPPT_DISABLE && CTL_TEST_START 
					== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
#else
		
	//Manually Battery Test
	if(CTL_TEST_START 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
#endif
	{
		GC_BoostChargeEnd(ST_MAN_TEST,
				FALSE,
				"Manual start test, turn to Manual Test.\n");
		GC_BattTestStart(ST_MAN_TEST, 
				NULL,
				START_MANUAL_TEST);
		return;
	}

	if(STATE_AUTO != g_pGcData->RunInfo.enumAutoMan)
	//Manually
	{
		int	i;	
		if(ST_AUTO_BC == BmState)
		{
			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							BT_PUB_BM_STATE,
							ST_MAN_BC,
							TRUE);

		
			for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
			{

				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					RT_PUB_AC_ON_OFF_CTL_SELF + i,
					GC_RECT_ON,
					TRUE);

				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					RT_PUB_DC_ON_OFF_CTL_SELF + i,
					GC_RECT_ON,
					TRUE);
				GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF + i,
					GC_MAX_CURR_LMT);
			}
			g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
			GC_LOG_OUT(APP_LOG_INFO, 
				"AUTO turn to MAN, turn to Manual BC.\n");
		}
		else if(ST_MAN_BC == BmState && !s_bManualBCInAuto)
		{
			s_bManualBCInAuto = TRUE;
			for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
			{

				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					RT_PUB_AC_ON_OFF_CTL_SELF + i,
					GC_RECT_ON,
					TRUE);

				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					RT_PUB_DC_ON_OFF_CTL_SELF + i,
					GC_RECT_ON,
					TRUE);
				GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF + i,
					GC_MAX_CURR_LMT);
			}
			g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
		}

		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_TEMP_COMPEN_SET,
						GC_NORMAL,
						FALSE);	

		return;
	}

	//Automatically
	s_bManualBCInAuto = FALSE;
	////If in Manual BC state, turn to Automatic BC
	//2010-11-11:Follow EMEA suggestions,keep the manual boost charge state in Auto mode.
	//if(ST_MAN_BC == BmState)
	//{
	//	GC_SetEnumValue(TYPE_OTHER, 
	//					0, 
	//					BT_PUB_BM_STATE,
	//					ST_AUTO_BC,
	//					TRUE);


	//	GC_LOG_OUT(APP_LOG_INFO, 
	//		"MAN to AUTO, turn to Automatic BC.\n");
	//	return;
	//}

			
	//If alarm condition is fulfilled, turn to FC
	if(GC_AlmJudgmentForBC())
	{
		GC_BoostChargeEnd(ST_FLOAT,
				FALSE,
				"Some alarm raise, turn to FC.\n");
		return;			
	}

	//If BC to FC Delay timer is timeout
	if(SEC_TMR_TIME_OUT 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_STABLE_BC))
	{
		GC_BoostChargeEnd(ST_FLOAT,
				TRUE,
				"BC to FC delay is time-out, turn to FC.\n");
		return;						
	}
			

	//If there are no battery ,stop boostcharge
	//if(!g_pGcData->RunInfo.Bt.iQtyValidBatt)
	//{
	//	GC_BoostChargeEnd(ST_FLOAT,
	//			TRUE,
	//			"There is no battery, turn to FC.\n");
	//}
	if(CompareBattCurrAndBcToFcCurr())
	//All of BattCurr < BC to FC Curr
	{
		if(SEC_TMR_SUSPENDED
			== GC_GetStateOfSecTimer(SEC_TMR_ID_STABLE_BC))
		{
			GC_SetSecTimer(SEC_TMR_ID_STABLE_BC,
						0,
						GC_GetBcToFcDelay,
						0);

			/*GC_LOG_OUT(APP_LOG_INFO, 
				"Start stable boost charge.\n");*/
		}
	}
	else
	//Some of BattCurr > BC to FC Curr
	{
		if(SEC_TMR_TIME_IN 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_STABLE_BC))
		{
			GC_SuspendSecTimer(SEC_TMR_ID_STABLE_BC);
			/*GC_LOG_OUT(APP_LOG_INFO, 
				"Stop stable boost charge.\n");*/
		}			
	}
	if(GC_IsNeedVoltAdjust() == TRUE )
	{
		CurrentLimitThroughVolt(FALSE ,BT_PUB_BST_VOLT, FALSE);
	}
	else
	{
		GC_RectVoltCtl(TYPE_BOOST_VOLT, FALSE);	
		GC_CurrentLmt();
#ifdef GC_SUPPORT_MPPT
		GC_PreCurrLmt_Mppt(g_pGcData->RunInfo.Mt.iQtyOfRect);	
#endif
	}
	return;
}



/*==========================================================================*
 * FUNCTION : CompareBattCurrAndBcToFcCurr
 * PURPOSE  : If all of Battery Current is less than BC to FC Current, 
			  return TRUE, otherwise return FALSE.
 * CALLS    : GC_GetFloatValue
 * CALLED BY: GC_ManAutoBC
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-05 14:17
 *==========================================================================*/
 BOOL CompareBattCurrAndBcToFcCurr()
{
	int		i;
	float	fBcToFcCurr,fRatedCap;
	BOOL	bRTN;

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			//modified by Jimmy 20120508 for Li Batt CR
			if(GC_Is_ABatt_Li_Type(i))
			{
				fBcToFcCurr
					= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BC_TO_FC_CURR_LIBATT);
			}
			else
			{
				bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
				if(bRTN == FALSE)
					continue;
				fBcToFcCurr
					= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BC_TO_FC_CURR) * fRatedCap;
			}

			//end modified
			if(g_pGcData->RunInfo.Bt.afBattCurr[i] > fBcToFcCurr)
			{
				//printf("Battery %d's voltage is %f\n", i, g_pGcData->RunInfo.Bt.afBattCurr[i]);
				return FALSE;
			}
		}
	}
#ifdef GC_SUPPORT_MPPT
	//For MPPT, it may the mppt can not output the large current , it will make the charge current too small, 
	//Which lead to go to stable charge in advance.
	if(stState  !=  GC_DISABLED)
	{
		if(g_pGcData->RunInfo.fSysVolt < GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BST_VOLT_RECT) - 5)
		{
			return FALSE;
		}
	}
#endif

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : GC_BcProtect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-11 11:30
 *==========================================================================*/
void	GC_BcProtect()
{
	BYTE	byState;

	byState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	if((byState != ST_MAN_BC) 
		&& (byState != ST_AUTO_BC)
		&& (byState != ST_CYCLIC_BC)
		&& (byState != ST_BC_FOR_TEST))
	//Not in boost charge
	{
		GC_SuspendSecTimer(SEC_TMR_ID_BC_PROTECT);
		g_pGcData->RunInfo.Bt.dwBcProtectCounter = 0;
		return;
	}

	//Following is handling in boost charge
	if(SEC_TMR_SUSPENDED == GC_GetStateOfSecTimer(SEC_TMR_ID_BC_PROTECT))
	{

		GC_SetSecTimer(SEC_TMR_ID_BC_PROTECT, 
						0,
						GC_GetBcProtectTime,
						0);
		return;	
	}

	if(SEC_TMR_TIME_OUT == GC_GetStateOfSecTimer(SEC_TMR_ID_BC_PROTECT))
	{
		//g_pGcData->RunInfo.Bt.bNeedRegulateCap = TRUE;

		GC_SuspendSecTimer(SEC_TMR_ID_BC_PROTECT);

		GC_BoostChargeEnd(ST_FLOAT,
						TRUE,
						"BC protection, turn to FC.\n");

		if((byState != ST_AUTO_BC) 
			|| (GC_DISABLED == GC_GetEnumValue(TYPE_OTHER, 
											0,
											BT_PUB_AUTO_BC_ENB)))
		{
			g_pGcData->RunInfo.Bt.dwBcProtectCounter = 0;
			return;
		}

		if(!GC_NeedAutoBC())
		{
			g_pGcData->RunInfo.Bt.dwBcProtectCounter = 0;
			return;
		}

		g_pGcData->RunInfo.Bt.dwBcProtectCounter++;
		if(g_pGcData->RunInfo.Bt.dwBcProtectCounter < GC_BC_PROTECT_NUM)
		{	
			//Only 1 times to battery protect. so this will always be false;
			return;
		}

		g_pGcData->RunInfo.Bt.dwBcProtectCounter = 0;

		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_AUTO_BC_ENB, 
						GC_DISABLED,
						TRUE);

		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_ABNOR_BATT_CURR_SET, 
						GC_ALARM,
						TRUE);
	}
	return;
}


/*==========================================================================*
 * FUNCTION : GC_CmpCapAndFcToBcCap
 * PURPOSE  : To judge if one of Battery Capcity is less than FC to BC  
			  Capacity.
 * CALLS    : GC_GetFloatValue
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-28 16:34
 *==========================================================================*/
BOOL GC_CmpCapAndFcToBcCap()
{
	int		i;
	float	fFcToBcCap,fRatedCap,fBattCap;
	BOOL	bRTN;

	
	if(g_pGcData->RunInfo.Bt.iCmpCapDelay < 30)	//Because there is fault turn to boost charge when controller is restarting, delay 80 second.
	{
		return FALSE;
	}

	
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
		if(bRTN == FALSE)
			continue;
		bRTN =  GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_CAP,&fBattCap); 
		if(bRTN == FALSE)
			continue;

		fFcToBcCap 
			= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_FC_TO_BC_CAP)* fRatedCap/ 100;
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			if(fBattCap < fFcToBcCap)
			{

			testi =i;
				return TRUE;
			}
		}
	}

	return FALSE;
}




/*==========================================================================*
 * FUNCTION : GC_NeedAutoBC
 * PURPOSE  : Judge if fulfil FC to BC condition
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-12 11:48
 *==========================================================================*/
BOOL GC_NeedAutoBC()
{
	if(GC_AlmJudgmentForBC())
	{
		return FALSE;
	}

	if(g_pGcData->RunInfo.Bt.bOverFc2BcCurr)
	{
		return TRUE;
	}

	if(GC_CmpCapAndFcToBcCap())
	{
		return TRUE;
	}

	return FALSE;
}

int	GC_GetIntervalCycBcDuration(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN
		* GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_CYC_BC_DURATION);
}

int	GC_GetIntervalCycBcInterval(int iIdx)
{
	UNUSED(iIdx);
	
	return SECONDS_PER_DAY 
		* GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_CYC_BC_INTERVAL);
}

int	GC_GetBcToFcDelay(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN 
		* GC_GetDwordValue(TYPE_OTHER, 
						0, 
						BT_PUB_BC_TO_FC_DLY);
}

int GC_GetBcProtectTime(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN
		* (int)GC_GetDwordValue(TYPE_OTHER, 
								0,
								BT_PUB_MAX_BC_TIME);
}



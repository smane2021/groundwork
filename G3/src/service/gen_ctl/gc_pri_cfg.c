/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU Plus Rack_mounted
 *
 *  FILENAME : gc_pri_cfg.c
 *  CREATOR  : Frank Cao                DATE: 2006-05-09 16:15
 *  VERSION  : V1.00
 *  PURPOSE  :Read and parse private configuration file, initialize
 *             g_GcData->PriCfg
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_pri_cfg.h"
#include	"gen_ctl.h"
#include	"gc_index_def.h"

static int LoadGcSigDefProc(IN void *pCfg, OUT void *pBuf);
static int LoadGcCfgProc(IN void *pCfg, OUT void *pBuf);

static int ParseSigIdTypeProc(char *szBuf, PRIMARY_SIG_ID_TYPE *pData);
static int ParseSigDefProc(char *szBuf, PRIMARY_SIG_INFO *pData);
static int ParseBattParamProc(char *szBuf, GC_BATT_PARAM *pData);
static int ParsePowerSplitInputProc(char *szBuf, 
								  GC_SIG_INFO *pData);
static int ParseEmergShutdownInputProc(char *szBuf, 
								  GC_SIG_INFO *pData);
static BOOL IsValidStr(const char *pField);
static BOOL IsValidNum(const char *pField);

static void ConvertCfgData(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg);

static int ParseEngySavOutputProc(char *szBuf,
								  GC_SIG_INFO *pData); 
static int ParseReduceConsumptionActProc(char *szBuf, 
										 GC_REDUCE_CONSUMPTION_ACTION *pData);
static int ParseLmtMaxPowerActProc(char *szBuf, 
								   GC_LIMIT_MAX_POWER_ACTION *pData);
static int ParseEngySavModeProc(char *szBuf, 
								GC_ENGY_SAV_MODE *pData);
static int ParseEngySavScheduleProc(char *szBuf, 
									GC_ENGY_SAV_SCHEDULE *pData);
static void StuffCTPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg);


/*==========================================================================*
 * FUNCTION : GC_PriCfgInit
 * PURPOSE  : Read and parse gen_ctl.cfg, initialize g_pGcData->PriCfg
 * CALLS    : Cfg_LoadConfigFile(), ConvertCfgData()
 * CALLED BY: InitGC()
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-5-9 16:17
 *==========================================================================*/
#define	GC_PRI_CFG_FILE	"private/gen_ctl/gen_ctl.cfg"
void	GC_PriCfgInit(void)
{
	PRIMARY_PRI_CFG	PrimaryPriCfg;
	int				iRst;
	char			szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(GC_PRI_CFG_FILE, szCfgFileName, MAX_FILE_PATH);
	iRst = Cfg_LoadConfigFile(szCfgFileName,
							LoadGcSigDefProc,
							&PrimaryPriCfg);
	GC_ASSERT((iRst == ERR_CFG_OK), 
			ERR_CTL_CFG_LOAD_FLIE, 
			"Cfg_LoadConfigFile error!\n");

	iRst = Cfg_LoadConfigFile(szCfgFileName,
							LoadGcCfgProc,
							&(g_pGcData->PriCfg));
	GC_ASSERT((iRst == ERR_CFG_OK), 
			ERR_CTL_CFG_LOAD_FLIE, 
			"Cfg_LoadConfigFile error!\n");

	ConvertCfgData(&PrimaryPriCfg, &(g_pGcData->PriCfg));
	return;	
}


/*==========================================================================*
 * FUNCTION : int LoadGcSigDefProc
 * PURPOSE  : This is callback function, that load gen_ctl.cfg to 
              pBuf
 * CALLS    : Cfg_LoadTables
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: void  *pCfg : Handle of configuration 
 *            void  *pBuf : 
 * RETURN   : 0: successful 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-18 14:01
 *==========================================================================*/
static int LoadGcSigDefProc(IN void *pCfg, OUT void *pBuf)
{
	PRIMARY_PRI_CFG		*pPrimaryPri = (PRIMARY_PRI_CFG*)pBuf;
	
	CONFIG_TABLE_LOADER	loader[GC_CFG_SIG_DEF_NUM];

	GC_ASSERT(pCfg, 
			ERR_CTL_CFG_POINT_NULL, 
			"LoadGcSigDefProc - pCfg is NULL\n");
	GC_ASSERT(pBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"LoadGcSigDefProc - pBuf is NULL\n");

	DEF_LOADER_ITEM(&loader[GC_CFG_RECT_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iRectPriSigNum), 
				"[RECT_UNIT_SIGNAL]",
				&(pPrimaryPri->pRectPriSig),
				ParseSigIdTypeProc);

	DEF_LOADER_ITEM(&loader[GC_CFG_S1_RECT_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iS1RectPriSigNum), 
				"[S1_RECT_UNIT_SIGNAL]",
				&(pPrimaryPri->pS1RectPriSig),
				ParseSigIdTypeProc);

	DEF_LOADER_ITEM(&loader[GC_CFG_S2_RECT_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iS2RectPriSigNum), 
				"[S2_RECT_UNIT_SIGNAL]",
				&(pPrimaryPri->pS2RectPriSig),
				ParseSigIdTypeProc);

	DEF_LOADER_ITEM(&loader[GC_CFG_CT_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iCTPriSigNum), 
				"[CONV_UNIT_SIGNAL]",
				&(pPrimaryPri->pCTPriSig),
				ParseSigIdTypeProc);

#ifdef GC_SUPPORT_MPPT
	DEF_LOADER_ITEM(&loader[GC_CFG_MPPT_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iMpptPriSigNum), 
				"[MPPT_UNIT_SIGNAL]",
				&(pPrimaryPri->pMpptPriSig),
				ParseSigIdTypeProc);

#endif

#ifdef GC_SUPPORT_BMS
	DEF_LOADER_ITEM(&loader[GC_CFG_BMS_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iBMSPriSigNum), 
				"[BMS_UNIT_SIGNAL]",
				&(pPrimaryPri->pBMSPriSig),
				ParseSigIdTypeProc);

#endif

	DEF_LOADER_ITEM(&loader[GC_CFG_S3_RECT_PRI_SIG],
		NULL, 
		&(pPrimaryPri->iS3RectPriSigNum), 
		"[S3_RECT_UNIT_SIGNAL]",
		&(pPrimaryPri->pS3RectPriSig),
		ParseSigIdTypeProc);


	DEF_LOADER_ITEM(&loader[GC_CFG_BATT_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iBattPriSigNum), 
				"[BATT_UNIT_SIGNAL]",
				&(pPrimaryPri->pBattPriSig),
				ParseSigIdTypeProc);

	DEF_LOADER_ITEM(&loader[GC_CFG_LVD_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iLvdPriSigNum), 
				"[LVD_UNIT_SIGNAL]",
				&(pPrimaryPri->pLvdPriSig),
				ParseSigIdTypeProc);

	DEF_LOADER_ITEM(&loader[GC_CFG_BATT_FUSE_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iBattFusePriSigNum), 
				"[BATT_FUSE_UNIT_SIGNAL]",
				&(pPrimaryPri->pBattFusePriSig),
				ParseSigIdTypeProc);
	
	DEF_LOADER_ITEM(&loader[GC_CFG_EIB_PRI_SIG],
				NULL, 
				&(pPrimaryPri->iEibPriSigNum), 
				"[EIB_UNIT_SIGNAL]",
				&(pPrimaryPri->pEibPriSig),
				ParseSigIdTypeProc);

	DEF_LOADER_ITEM(&loader[GC_CFG_SIG],
				NULL, 
				&(pPrimaryPri->iSigNum), 
				"[SIGNAL_DEF]",
				&(pPrimaryPri->pSig),
				ParseSigDefProc);


	if(ERR_CFG_OK != 
		Cfg_LoadTables(pCfg, GC_CFG_SIG_DEF_NUM, loader))
	{
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;	
}


/*==========================================================================*
 * FUNCTION : ParseLvdOnVeryHiTempProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char         *szBuf : 
 *            GC_SIG_INFO  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-31 13:56
 *==========================================================================*/
static int ParseLvdOnVeryHiTempProc(char *szBuf, 
									GC_SIG_INFO *pData)
{
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEmergShutdownInputProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEmergShutdownInputProc - pData is NULL\n");

	pData->bEnabled = TRUE;

	/* 1.Equipment ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iEquipId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 2.Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalType = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 3.Signal ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	return ERR_OK;
}



static int ParseFixedRelayOutputProc(char *szBuf,
									 GC_SIG_INFO *pData)
{
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseFixedRelayOutputProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseFixedRelayOutputProc - pData is NULL\n");

	pData->bEnabled = TRUE;

	/* 0.Named field */
	// Do nothing
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);

	/* 1.Equipment ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iEquipId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 2.Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalType = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 3.Signal ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	return ERR_OK;
}



/*==========================================================================*
 * FUNCTION : LoadGcCfgProc
 * PURPOSE  : This is callback function, that load gen_ctl.cfg to 
              pBuf
 * CALLS    : Cfg_LoadTables
 * CALLED BY: Cfg_LoadConfigFile
 * ARGUMENTS: void  *pCfg : Handle of configuration 
 *            void  *pBuf : 
 * RETURN   : 0: successful 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-18 14:01
 *==========================================================================*/
static int LoadGcCfgProc(IN void *pCfg, OUT void *pBuf)
{
	int					iRst;
	int					iCfgValue;
	char				szBuf[256];
	GC_PRI_CFG			*pPriCfg = (GC_PRI_CFG*)pBuf;
	
	CONFIG_TABLE_LOADER	loader[GC_CFG_NUM];

	GC_ASSERT(pCfg,
			ERR_CTL_CFG_POINT_NULL, 
			"LoadGcCfgProc - pCfg is NULL\n");
	GC_ASSERT(pBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"LoadGcCfgProc - pBuf is NULL\n");

	//Battery Type Parameter
	DEF_LOADER_ITEM(&loader[GC_CFG_BATT_PARAM],
				NULL,
				&(pPriCfg->iBattTypeNum), 
				"[BATT_TYPE_PARAM]",
				&(pPriCfg->pBattParam),
				ParseBattParamProc);

	//Energy Saving Output Signal Definition
	DEF_LOADER_ITEM(&loader[GC_CFG_ENGY_SAV_OUTPUT],
				NULL,
				&(pPriCfg->iEnergySavingOutputNum), 
				"[ENERGY_SAVING_OUTPUT]",
				&(pPriCfg->pEngySavOutput),
				ParseEngySavOutputProc);

	//Lower Consumption Action
	DEF_LOADER_ITEM(&loader[GC_CFG_REDUCE_CONSUMPTION_ACTION],
				NULL,
				&(pPriCfg->iReduceConsumptionActNum), 
				"[GC_LOWER_CONSUMPTION_ACTION]",
				&(pPriCfg->pReduceConsumptionAct),
				ParseReduceConsumptionActProc);

	//Limit Maximum Power Action
	DEF_LOADER_ITEM(&loader[GC_CFG_LIMIT_MAX_POWER_ACTION],
				NULL,
				&(pPriCfg->iLmtMaxPowerActNum), 
				"[GC_LIMIT_MAX_POWER_ACTION]",
				&(pPriCfg->pLmtMaxPowerAct),
				ParseLmtMaxPowerActProc);

	//Energy Saving Mode for Day
	DEF_LOADER_ITEM(&loader[GC_CFG_ENGY_SAV_MODE],
				NULL,
				&(pPriCfg->iEngySavModeNum), 
				"[MODE_DEF_FOR_DAY]",
				&(pPriCfg->pEngySavMode),
				ParseEngySavModeProc);

	//Energy Saving Schedule
	DEF_LOADER_ITEM(&loader[GC_CFG_ENGY_SAV_SCHEDULE],
				NULL,
				&(pPriCfg->iEngySavScheduleNum), 
				"[ENERGY_SAVING_SCHEDULE]",
				&(pPriCfg->pEngySavSchedule),
				ParseEngySavScheduleProc);

	//Power Split Input Signal definition
	DEF_LOADER_ITEM(&loader[GC_CFG_POWER_SPLIT_INPUT],
				NULL, 
				&(pPriCfg->iPowerSplitInputNum), 
				"[POWER_SPLIT_INPUT]",
				&(pPriCfg->pPowerSplitInput),
				ParsePowerSplitInputProc);
	
	//Emergency Shut-down Input Signal definition
	DEF_LOADER_ITEM(&loader[GC_CFG_EMERG_SHUTDOWN_INPUT],
				NULL, 
				&(pPriCfg->iEstopEshutdownInputNum), 
				"[ESTOP_ESHUTDOWN_INPUT]",
				&(pPriCfg->pEstopEshutdownInput),
				ParseEmergShutdownInputProc);

	//LVD control Signal for very high temperature alarm
	DEF_LOADER_ITEM(&loader[GC_CFG_LVD_VERY_HI_TEMP],
				NULL, 
				&(pPriCfg->iLvdNumOnVeryHiTemp), 
				"[LVD_ON_VERY_HIGH_TEMPERATURE]",
				&(pPriCfg->pLvdOnVeryHiTemp),
				ParseLvdOnVeryHiTempProc);
	
	DEF_LOADER_ITEM(&loader[GC_CFG_FIXED_RELAY_OUTPUT],
				NULL, 
				&(pPriCfg->iSigNumFixedRelayOutput), 
				"[FIXED_RELAY_OUTPUT]",
				&(pPriCfg->pFixedRelayOutput),
				ParseFixedRelayOutputProc);
	
	
	if(ERR_CFG_OK != 
		Cfg_LoadTables(pCfg, GC_CFG_NUM, loader))
	{
		return ERR_CFG_FAIL;
	}

	//Battery Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
							"[BATT_EQUIP_TYPE_ID]", 
							&(pPriCfg->iEquipTypeIdBatt)); 
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [BATT_EQUIP_TYPE_ID].\n");

	//Rectifier Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
							"[RECT_EQUIP_TYPE_ID]", 
							&(pPriCfg->iEquipTypeIdRect)); 
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [RECT_EQUIP_TYPE_ID].\n");

	//LVD Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
							"[LVD_EQUIP_TYPE_ID]", 
							&(pPriCfg->iEquipTypeIdLvd)); 
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [LVD_EQUIP_TYPE_ID].\n");

	//Battery Fuse Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
							"[BATT_FUSE_EQUIP_TYPE_ID]", 
							&(pPriCfg->iEquipTypeIdBattFuse)); 
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [BATT_FUSE_EQUIP_TYPE_ID].\n");

	//EIB Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
							"[EIB_EQUIP_TYPE_ID]", 
							&(pPriCfg->iEquipTypeIdEib)); 
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [EIB_EQUIP_TYPE_ID].\n");

	//Slave1 Rectifier Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
							"[S1_RECT_EQUIP_TYPE_ID]", 
							&(pPriCfg->iEquipTypeIdS1Rect)); 
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [S1_RECT_EQUIP_TYPE_ID].\n");

	//Slave2 Rectifier Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
							"[S2_RECT_EQUIP_TYPE_ID]", 
							&(pPriCfg->iEquipTypeIdS2Rect)); 
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [S2_RECT_EQUIP_TYPE_ID].\n");

	//Slave3 Rectifier Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
							"[S3_RECT_EQUIP_TYPE_ID]", 
							&(pPriCfg->iEquipTypeIdS3Rect)); 
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [S3_RECT_EQUIP_TYPE_ID].\n");

	//Converter Equipment Type ID
	iRst = Cfg_ProfileGetInt(pCfg,
		"[CONV_EQUIP_TYPE_ID]", 
		&(pPriCfg->iEquipTypeIdCT)); 
	GC_ASSERT((iRst == 1),
		ERR_CTL_CFG_INTERFACE, 
		"Cfg_ProfileGetInt - [CONV_EQUIP_TYPE_ID].\n");

#ifdef GC_SUPPORT_MPPT
	//Mppt  
	iRst = Cfg_ProfileGetInt(pCfg,
		"[MPPT_EQUIP_TYPE_ID]", 
		&(pPriCfg->iEquipTypeIdMppt)); 
	GC_ASSERT((iRst == 1),
		ERR_CTL_CFG_INTERFACE, 
		"Cfg_ProfileGetInt - [MPPT_EQUIP_TYPE_ID].\n");
#endif

#ifdef GC_SUPPORT_BMS
	//Mppt  
	iRst = Cfg_ProfileGetInt(pCfg,
		"[BMS_EQUIP_TYPE_ID]", 
		&(pPriCfg->iEquipTypeIdBMS)); 
	GC_ASSERT((iRst == 1),
		ERR_CTL_CFG_INTERFACE, 
		"Cfg_ProfileGetInt - [MPPT_EQUIP_TYPE_ID].\n");
#endif

	//Energy Saving Calcultion Period
	iRst = Cfg_ProfileGetInt(pCfg,
							"[ENERGY_SAVING_CALC_PERIOD]", 
							&iCfgValue);
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetInt - [ENERGY_SAVING_CALC_PERIOD].\n");

	pPriCfg->iEnergySavingPeriod = iCfgValue * 1000;

	//Power Split Mode
	iRst = Cfg_ProfileGetString(pCfg,
							"[POWER_SPLIT_MODE]", 
							szBuf,
							10);
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetString - [POWER_SPLIT_MODE].\n");

	if(!stricmp(szBuf, "SLAVE"))
	//Matched
	{
		pPriCfg->bSlaveMode = TRUE;
	}
	else
	{
		pPriCfg->bSlaveMode = FALSE;	
	}


	//Main Switch Enabled
	iRst = Cfg_ProfileGetString(pCfg,
							"[MAIN_SWITCH_ENABLED]", 
							szBuf,
							10);
	GC_ASSERT((iRst == 1),
			ERR_CTL_CFG_INTERFACE, 
			"Cfg_ProfileGetString - [MAIN_SWITCH_ENABLED].\n");

	if(!stricmp(szBuf, "YES"))
	//Matched
	{
		pPriCfg->bMainSwitchEnb = TRUE;
	}
	else
	{
		pPriCfg->bMainSwitchEnb = FALSE;	
	}


	return ERR_CFG_OK;	
 }



/*==========================================================================*
 * FUNCTION : ParseSigIdTypeProc
 * PURPOSE  : This is a callback function, that parse one line of Signal ID
              and Signal Type Configuration
 * CALLS    : Cfg_SplitStringEx, NEW_strdup, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char *szBuf :Source string 
 *            PRIMARY_SIG_ID_TYPE  *pData : Data after parsing
 * RETURN   : 0: successful 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:35
 *==========================================================================*/
static int ParseSigIdTypeProc(char *szBuf, PRIMARY_SIG_ID_TYPE *pData)
{
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseSigIdTypeProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseSigIdTypeProc - pData is NULL\n");


	/* 1.Signal Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);

	GC_ASSERT(IsValidStr(pField),
			ERR_CTL_CFG_SIG_NAME,
			"gen_ctl.cfg Signal Name is not a valid string!\n");

	pData->pName = NEW_strdup(pField);
	GC_ASSERT(pData->pName, 
			ERR_CTL_NO_MEMORY, 
			"ParseSigIdTypeProc - NEW_strdup failed!\n");

	/* 2.Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	
	GC_ASSERT(IsValidNum(pField),
			ERR_CTL_CFG_SIG_ID,
			"gen_ctl.cfg Signal Type is not a valid number!\n");

	pData->iType = atoi(pField);

	/* 3.Signal Id field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField),
			ERR_CTL_CFG_SIG_ID,
			"gen_ctl.cfg Signal ID is not a valid number!\n");
	pData->iId = atoi(pField);

	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : ParseSigDefProc
 * PURPOSE  : This is a callback function, that parse one line of Signal ID
              and Signal Type Configuration
 * CALLS    : Cfg_SplitStringEx, NEW_strdup, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char              *szBuf : 
 *            PRIMARY_SIG_ID_TYPE  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-27 16:48
 *==========================================================================*/
static int ParseSigDefProc(char *szBuf, PRIMARY_SIG_INFO *pData)
{
	char *pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseSigDefProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseSigDefProc - pData is NULL\n");

	/* 1.Signal Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidStr(pField),
			ERR_CTL_CFG_SIG_NAME,
			"gen_ctl.cfg Signal Name is not a valid string!\n");

	pData->pName = NEW_strdup(pField);
	GC_ASSERT(pData->pName, ERR_CTL_NO_MEMORY, "NEW_strdup failed!\n");

	/* 2.Equip Type Id field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField),
			ERR_CTL_CFG_EQUIP_ID,
			"gen_ctl.cfg EquipId is not a valid number!\n");

	pData->iEquipId = atoi(pField);


	/* 3.Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField),
			ERR_CTL_CFG_SIG_TYPE,
			"gen_ctl.cfg SignalType is not a valid number!\n");

	pData->iType = atoi(pField);

	/* 4.Signal Id field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField),
			ERR_CTL_CFG_SIG_ID,
			"gen_ctl.cfg Signal ID is not a valid number!\n");

	pData->iId = atoi(pField);

	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : ParseBattParamProc
 * PURPOSE  : This is a callback function, that parse one line of Battery 
              Type Parameter Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char          *szBuf : 
 *            GC_TEST_PLAN  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-01 20:15
 *==========================================================================*/
static int ParseBattParamProc(char *szBuf, GC_BATT_PARAM *pData)
{
	int		i;
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseBattParamProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseBattParamProc - pData is NULL\n");

	/* 1.Battery Type No. field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Battery Type No. is not a valid number!\n");
	{
		int		iBattTypeNo;
		iBattTypeNo = atoi(pField);
		GC_ASSERT(((iBattTypeNo <= 10) && (iBattTypeNo > 0)), 
				ERR_CTL_CFG_BATT_TYPE_PARAM,
				"Battery Type No. is not valid!\n");
	}

	/* 2.Rated Capcity field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Rated Capcity is not a valid number!\n");

	pData->fRatedCap = atof(pField);

	/* 3.Over Current field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Over Current is not a valid number!\n");
	pData->fOverCurr = atof(pField);
	

	/* 4.Current Limitation field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Current Limitation is not a valid number!\n");
	pData->fCurrLmt = atof(pField);

	/*5.Very Under Voltage field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Very Under Voltage is not a valid number!\n");
	pData->fVeryUnderVoltage = atof(pField);


	/* 6.Under Voltage field  */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Under Voltage is not a valid number!\n");
	pData->fUnderVoltage = atof(pField);

	/*7.High Voltage field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"High Voltage is not a valid number!\n");
	pData->fHighVoltage = atof(pField);


	/* 8.Over Voltage1 field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Over Voltage is not a valid number!\n");
	pData->fOverVoltage = atof(pField);

	/* 9.Capacity Coefficient field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Capacity Coefficient is not a valid number!\n");
	pData->fCapCoef = atof(pField);

	/* 10.Float Voltage field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Float Voltage is not a valid number!\n");
	pData->fFCVoltage = atof(pField);

	/* 11.Boost Voltage field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"Boost Voltage is not a valid number!\n");
	pData->fBCVoltage = atof(pField);


	/* 13.LVD Voltage1 field */
	/*szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"LVD Voltage1 is not a valid number!\n");
	pData->fLVD1 = atof(pField);*/

	/* 14.LVD Voltage2 field */
	/*szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_BATT_TYPE_PARAM,
			"LVD Voltage2 is not a valid number!\n");
	pData->fLVD2 = atof(pField);*/

	/* 15.Discharge Time field */
	for(i = 0; i < 10; i++)
	{
		szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
		GC_ASSERT(IsValidNum(pField), 
				ERR_CTL_CFG_BATT_TYPE_PARAM,
				"Discharge Time is not a valid number!\n");
		pData->afDischCurve[i] = atof(pField);
	}

	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : ParseEngySavOutputProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Output Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char             *szBuf : 
 *            GC_SIG_INFO  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 11:51
 *==========================================================================*/
//static int ParseEngySavOutputProc(char *szBuf, 
//								  GC_SIG_INFO *pData)
//{
//	char	*pField;
//
//	GC_ASSERT(szBuf,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseEngySavOutputProc - szBuf is NULL\n");
//	GC_ASSERT(pData,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseEngySavOutputProc - pData is NULL\n");
//
//	pData->bEnabled = TRUE;
//
//	/* 1.Output Name field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	GC_ASSERT(IsValidStr(pField), 
//			ERR_CTL_CFG_ENGY_SAV_OUTPUT,
//			"Output Name is not a valid string!\n");
//
//	/* 2.Equipment ID field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	if (IsValidNum(pField))
//	{
//		pData->iEquipId = atoi(pField);
//	}
//	else
//	{
//		pData->bEnabled = FALSE;
//	}
//
//	/* 3.Signal Type field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	if (IsValidNum(pField))
//	{
//		pData->iSignalType = atoi(pField);
//	}
//	else
//	{
//		pData->bEnabled = FALSE;
//	}
//
//	/* 4.Signal ID field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	if (IsValidNum(pField))
//	{
//		pData->iSignalId = atoi(pField);
//	}
//	else
//	{
//		pData->bEnabled = FALSE;
//	}
//
//	return ERR_OK;
//}

/*==========================================================================*
 * FUNCTION : ParseReduceConsumptionActProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Lower Consumption Action Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char                       *szBuf : 
 *            GC_REDUCE_CONSUMPTION_ACTION  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 13:58
 *==========================================================================*/
//static int ParseReduceConsumptionActProc(char *szBuf, 
//										 GC_REDUCE_CONSUMPTION_ACTION *pData)
//{
//	char	*pField;
//
//	GC_ASSERT(szBuf,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseReduceConsumptionActProc - szBuf is NULL\n");
//	GC_ASSERT(pData,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseReduceConsumptionActProc - pData is NULL\n");
//
//	/* 1.Rate Type field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	GC_ASSERT(IsValidStr(pField), 
//			ERR_CTL_CFG_REDUCE_CONSUMPTION_ACT,
//			"Rate Type is not a valid string!\n");
//
//	/* 2.Action of Output1*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//
//	if(GC_MATCHED == stricmp("ON", pField))
//	{
//		pData->enumOutput1 = ENGY_SAV_ON;
//	}
//	else if(GC_MATCHED == stricmp("OFF", pField))
//	{
//		pData->enumOutput1 = ENGY_SAV_OFF;
//	}
//	else
//	{
//		pData->enumOutput1 = ENGY_SAV_NO_ACT;
//	}
//	
//
//	/* 3.Action of Output2*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//
//	if(GC_MATCHED == stricmp("ON", pField))
//	{
//		pData->enumOutput2 = ENGY_SAV_ON;
//	}
//	else if(GC_MATCHED == stricmp("OFF", pField))
//	{
//		pData->enumOutput2 = ENGY_SAV_OFF;
//	}
//	else
//	{
//		pData->enumOutput2 = ENGY_SAV_NO_ACT;
//	}
//
//
//	/* 2.Action of Output3*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	if(GC_MATCHED == stricmp("ON", pField))
//	{
//		pData->enumOutput3 = ENGY_SAV_ON;
//	}
//	else if(GC_MATCHED == stricmp("OFF", pField))
//	{
//		pData->enumOutput3 = ENGY_SAV_OFF;
//	}
//	else
//	{
//		pData->enumOutput3 = ENGY_SAV_NO_ACT;
//	}
//	
//
//	/* 5.Prohibit Battery Charge field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//
//	if(GC_MATCHED == stricmp("YES", pField))
//	{
//		pData->bProhibitBattChg = TRUE;
//	}
//	else
//	{
//		pData->bProhibitBattChg = FALSE;
//	}
//
//	return ERR_OK;
//}

/*==========================================================================*
 * FUNCTION : ParseLmtMaxPowerActProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Limit Max Power Action Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char                    *szBuf : 
 *            GC_LIMIT_MAX_POWER_ACTION  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 16:13
 *==========================================================================*/
//static int ParseLmtMaxPowerActProc(char *szBuf, 
//								   GC_LIMIT_MAX_POWER_ACTION *pData)
//{
//	char	*pField;
//
//	GC_ASSERT(szBuf,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseLmtMaxPowerActProc - szBuf is NULL\n");
//	GC_ASSERT(pData,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseLmtMaxPowerActProc - pData is NULL\n");
//
//	/* 1.Condition field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	GC_ASSERT(IsValidStr(pField), 
//			ERR_CTL_CFG_LMT_MAX_POWER_ACT,
//			"Condition is not a valid string!\n");
//
//	/* 2.Action of Output1*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//
//	if(GC_MATCHED == stricmp("ON", pField))
//	{
//		pData->enumOutput1 = ENGY_SAV_ON;
//	}
//	else if(GC_MATCHED == stricmp("OFF", pField))
//	{
//		pData->enumOutput1 = ENGY_SAV_OFF;
//	}
//	else
//	{
//		pData->enumOutput1 = ENGY_SAV_NO_ACT;
//	}
//
//	/* 3.Action of Output2*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	if(GC_MATCHED == stricmp("ON", pField))
//	{
//		pData->enumOutput2 = ENGY_SAV_ON;
//	}
//	else if(GC_MATCHED == stricmp("OFF", pField))
//	{
//		pData->enumOutput2 = ENGY_SAV_OFF;
//	}
//	else
//	{
//		pData->enumOutput2 = ENGY_SAV_NO_ACT;
//	}
//
//
//	/* 2.Action of Output3*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	if(GC_MATCHED == stricmp("ON", pField))
//	{
//		pData->enumOutput3 = ENGY_SAV_ON;
//	}
//	else if(GC_MATCHED == stricmp("OFF", pField))
//	{
//		pData->enumOutput3 = ENGY_SAV_OFF;
//	}
//	else
//	{
//		pData->enumOutput3 = ENGY_SAV_NO_ACT;
//	}
//	
//	return ERR_OK;
//}


/*==========================================================================*
 * FUNCTION : ParseEngySavModeProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Mode Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char           *szBuf : 
 *            GC_ENGY_SAV_MODE  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 17:27
 *==========================================================================*/
//static int ParseEngySavModeProc(char *szBuf, 
//								GC_ENGY_SAV_MODE *pData)
//{
//	int		i;
//	char	*pField;
//
//	GC_ASSERT(szBuf,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseEngySavModeProc - szBuf is NULL\n");
//	GC_ASSERT(pData,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseEngySavModeProc - pData is NULL\n");
//
//	/* 1. O'clock field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	GC_ASSERT(IsValidNum(pField), 
//			ERR_CTL_CFG_ENGY_SAV_MODE,
//			"O'clock is not a valid number!\n");
//	GC_ASSERT(((atoi(pField) < 24) && (atoi(pField) >= 0)), 
//				ERR_CTL_CFG_ENGY_SAV_MODE,
//				"O'clock is too big or small!\n");
//
//
//	/* 2. Mode A--H field */
//	for(i = 0; i < ENGY_SAV_MODE_NUM; i++)
//	{
//		szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//
//		switch(*pField)
//		{
//		case 'P':
//			pData->aHourDef[i].enumRateType = ENGY_SAV_RATE_PEAK;
//			break;
//		case 'H':
//			pData->aHourDef[i].enumRateType = ENGY_SAV_RATE_HIGH;
//			break;
//		case 'F':
//			pData->aHourDef[i].enumRateType = ENGY_SAV_RATE_FLAT;
//			break;
//		default:
//			pData->aHourDef[i].enumRateType = ENGY_SAV_RATE_LOW;
//			break;
//		}
//
//		{
//			BOOL	bRst = ('/' == *(pField + 1)) || ('\\' == *(pField + 1));
//			GC_ASSERT(bRst, 
//				ERR_CTL_CFG_ENGY_SAV_MODE,
//				"Invalid mode config!\n");
//		}
//		
//		if (IsValidNum(pField + 2))
//		{
//			pData->aHourDef[i].fMaxPower = atof(pField + 2);
//		}
//		else
//		{
//			pData->aHourDef[i].fMaxPower = ENGY_SAV_MAX_POWER;
//		}
//	}
//
//	return ERR_OK;
//}

/*==========================================================================*
 * FUNCTION : ParseEngySavScheduleProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Schedule Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char               *szBuf : 
 *            GC_ENGY_SAV_SCHEDULE  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 19:29
 *==========================================================================*/
//static int ParseEngySavScheduleProc(char *szBuf, 
//									GC_ENGY_SAV_SCHEDULE *pData)
//{
//	int		i;
//	char	*pField;
//
//	GC_ASSERT(szBuf,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseEngySavScheduleProc - szBuf is NULL\n");
//	GC_ASSERT(pData,
//			ERR_CTL_CFG_POINT_NULL, 
//			"ParseEngySavScheduleProc - pData is NULL\n");
//
//
//	/* 1. Date field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//	GC_ASSERT(IsValidNum(pField), 
//			ERR_CTL_CFG_ENGY_SAV_SCHEDULE,
//			"Date is not a valid number!\n");
//
//	GC_ASSERT(((atoi(pField) < 32) && (atoi(pField) > 0)), 
//			ERR_CTL_CFG_ENGY_SAV_SCHEDULE,
//			"Invalid date!\n");
//
//	/* 2. Month 1--12 field */
//	for(i = 0; i < MONTHS_PER_YEAR; i++)
//	{
//		szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
//		GC_ASSERT(((*pField >= 'A') 
//						&& (*pField <= 'H') 
//						&& (*(pField + 1) == 0)), 
//					ERR_CTL_CFG_ENGY_SAV_SCHEDULE,
//					"Invalid mode for a day!\n");
//
//		switch(*pField)
//		{
//		case 'A':
//			pData->aenumMode[i] = ENGY_SAV_MODE_A;
//			break;
//		case 'B':
//			pData->aenumMode[i] = ENGY_SAV_MODE_B;
//			break;
//		case 'C':
//			pData->aenumMode[i] = ENGY_SAV_MODE_C;
//			break;
//		case 'D':
//			pData->aenumMode[i] = ENGY_SAV_MODE_D;
//			break;
//		case 'E':
//			pData->aenumMode[i] = ENGY_SAV_MODE_E;
//			break;
//		case 'F':
//			pData->aenumMode[i] = ENGY_SAV_MODE_F;
//			break;
//		case 'G':
//			pData->aenumMode[i] = ENGY_SAV_MODE_G;
//			break;
//		case 'H':
//			pData->aenumMode[i] = ENGY_SAV_MODE_H;
//			break;
//		default:
//			break;
//		}
//	}
//
//	return ERR_OK;
//}


/*==========================================================================*
 * FUNCTION : ParsePowerSplitInputProc
 * PURPOSE  : This is a callback function, that parse one line of Power  
              Split Output Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char                  *szBuf : 
 *            GC_SIG_INFO  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-21 11:19
 *==========================================================================*/
static int ParsePowerSplitInputProc(char *szBuf, 
									GC_SIG_INFO *pData)
{
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParsePowerSplitInputProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParsePowerSplitInputProc - pData is NULL\n");

	pData->bEnabled = TRUE;

	/* 1.Input Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidStr(pField), 
			ERR_CTL_CFG_POWER_SPLIT_INPUT,
			"Input Name is not valid string!\n");

	/* 2.Equipment ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iEquipId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 3.Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalType = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 4.Signal ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : ParseEmergShutdownInputProc
 * PURPOSE  : This is a callback function, that parse one line of Emergency 
			  Shut-down Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char *szBuf : 
 *            GC_SIG_INFO  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-04-06 13:54
 *==========================================================================*/
static int ParseEmergShutdownInputProc(char *szBuf, 
									   GC_SIG_INFO *pData)
{
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEmergShutdownInputProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEmergShutdownInputProc - pData is NULL\n");

	pData->bEnabled = TRUE;

	/* 1.Equipment ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iEquipId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 2.Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalType = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 3.Signal ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : ParseEngySavOutputProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Output Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char             *szBuf : 
 *            GC_SIG_INFO  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 11:51
 *==========================================================================*/
static int ParseEngySavOutputProc(char *szBuf, 
								  GC_SIG_INFO *pData)
{
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEngySavOutputProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEngySavOutputProc - pData is NULL\n");

	pData->bEnabled = TRUE;

	/* 1.Output Name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidStr(pField), 
			ERR_CTL_CFG_ENGY_SAV_OUTPUT,
			"Output Name is not a valid string!\n");

	/* 2.Equipment ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iEquipId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 3.Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalType = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	/* 4.Signal ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if (IsValidNum(pField))
	{
		pData->iSignalId = atoi(pField);
	}
	else
	{
		pData->bEnabled = FALSE;
	}

	return ERR_OK;
}

/*==========================================================================*
 * FUNCTION : ParseReduceConsumptionActProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Lower Consumption Action Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char                       *szBuf : 
 *            GC_REDUCE_CONSUMPTION_ACTION  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 13:58
 *==========================================================================*/
static int ParseReduceConsumptionActProc(char *szBuf, 
										 GC_REDUCE_CONSUMPTION_ACTION *pData)
{
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseReduceConsumptionActProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseReduceConsumptionActProc - pData is NULL\n");

	/* 1.Rate Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidStr(pField), 
			ERR_CTL_CFG_REDUCE_CONSUMPTION_ACT,
			"Rate Type is not a valid string!\n");

	/* 2.Action of Output1*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);

	if(GC_MATCHED == stricmp("ON", pField))
	{
		pData->enumOutput1 = ENGY_SAV_ON;
	}
	else if(GC_MATCHED == stricmp("OFF", pField))
	{
		pData->enumOutput1 = ENGY_SAV_OFF;
	}
	else
	{
		pData->enumOutput1 = ENGY_SAV_NO_ACT;
	}
	

	/* 3.Action of Output2*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);

	if(GC_MATCHED == stricmp("ON", pField))
	{
		pData->enumOutput2 = ENGY_SAV_ON;
	}
	else if(GC_MATCHED == stricmp("OFF", pField))
	{
		pData->enumOutput2 = ENGY_SAV_OFF;
	}
	else
	{
		pData->enumOutput2 = ENGY_SAV_NO_ACT;
	}


	/* 2.Action of Output3*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if(GC_MATCHED == stricmp("ON", pField))
	{
		pData->enumOutput3 = ENGY_SAV_ON;
	}
	else if(GC_MATCHED == stricmp("OFF", pField))
	{
		pData->enumOutput3 = ENGY_SAV_OFF;
	}
	else
	{
		pData->enumOutput3 = ENGY_SAV_NO_ACT;
	}
	

	/* 5.Prohibit Battery Charge field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);

	if(GC_MATCHED == stricmp("YES", pField))
	{
		pData->bProhibitBattChg = TRUE;
	}
	else
	{
		pData->bProhibitBattChg = FALSE;
	}

	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : ParseLmtMaxPowerActProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Limit Max Power Action Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char                    *szBuf : 
 *            GC_LIMIT_MAX_POWER_ACTION  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 16:13
 *==========================================================================*/
static int ParseLmtMaxPowerActProc(char *szBuf, 
								   GC_LIMIT_MAX_POWER_ACTION *pData)
{
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseLmtMaxPowerActProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseLmtMaxPowerActProc - pData is NULL\n");

	/* 1.Condition field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidStr(pField), 
			ERR_CTL_CFG_LMT_MAX_POWER_ACT,
			"Condition is not a valid string!\n");

	/* 2.Action of Output1*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);

	if(GC_MATCHED == stricmp("ON", pField))
	{
		pData->enumOutput1 = ENGY_SAV_ON;
	}
	else if(GC_MATCHED == stricmp("OFF", pField))
	{
		pData->enumOutput1 = ENGY_SAV_OFF;
	}
	else
	{
		pData->enumOutput1 = ENGY_SAV_NO_ACT;
	}

	/* 3.Action of Output2*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if(GC_MATCHED == stricmp("ON", pField))
	{
		pData->enumOutput2 = ENGY_SAV_ON;
	}
	else if(GC_MATCHED == stricmp("OFF", pField))
	{
		pData->enumOutput2 = ENGY_SAV_OFF;
	}
	else
	{
		pData->enumOutput2 = ENGY_SAV_NO_ACT;
	}


	/* 2.Action of Output3*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	if(GC_MATCHED == stricmp("ON", pField))
	{
		pData->enumOutput3 = ENGY_SAV_ON;
	}
	else if(GC_MATCHED == stricmp("OFF", pField))
	{
		pData->enumOutput3 = ENGY_SAV_OFF;
	}
	else
	{
		pData->enumOutput3 = ENGY_SAV_NO_ACT;
	}
	
	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : ParseEngySavModeProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Mode Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char           *szBuf : 
 *            GC_ENGY_SAV_MODE  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 17:27
 *==========================================================================*/
static int ParseEngySavModeProc(char *szBuf, 
								GC_ENGY_SAV_MODE *pData)
{
	int		i;
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEngySavModeProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEngySavModeProc - pData is NULL\n");

	/* 1. O'clock field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_ENGY_SAV_MODE,
			"O'clock is not a valid number!\n");
	GC_ASSERT(((atoi(pField) < 24) && (atoi(pField) >= 0)), 
				ERR_CTL_CFG_ENGY_SAV_MODE,
				"O'clock is too big or small!\n");


	/* 2. Mode A--H field */
	for(i = 0; i < ENGY_SAV_MODE_NUM; i++)
	{
		szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);

		switch(*pField)
		{
		case 'P':
			pData->aHourDef[i].enumRateType = ENGY_SAV_RATE_PEAK;
			break;
		case 'H':
			pData->aHourDef[i].enumRateType = ENGY_SAV_RATE_HIGH;
			break;
		case 'F':
			pData->aHourDef[i].enumRateType = ENGY_SAV_RATE_FLAT;
			break;
		default:
			pData->aHourDef[i].enumRateType = ENGY_SAV_RATE_LOW;
			break;
		}

		{
			BOOL	bRst = ('/' == *(pField + 1)) || ('\\' == *(pField + 1));
			GC_ASSERT(bRst, 
				ERR_CTL_CFG_ENGY_SAV_MODE,
				"Invalid mode config!\n");
		}
		
		if (IsValidNum(pField + 2))
		{
			pData->aHourDef[i].fMaxPower = atof(pField + 2);
		}
		else
		{
			pData->aHourDef[i].fMaxPower = ENGY_SAV_MAX_POWER;
		}
	}

	return ERR_OK;
}

/*==========================================================================*
 * FUNCTION : ParseEngySavScheduleProc
 * PURPOSE  : This is a callback function, that parse one line of Energy  
              Saving Schedule Private Configuration
 * CALLS    : Cfg_SplitStringEx, Cfg_SplitStringEx, IsValidStr,
              IsValidNum
 * CALLED BY: Cfg_LoadTables
 * ARGUMENTS: char               *szBuf : 
 *            GC_ENGY_SAV_SCHEDULE  *pData : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-29 19:29
 *==========================================================================*/
static int ParseEngySavScheduleProc(char *szBuf, 
									GC_ENGY_SAV_SCHEDULE *pData)
{
	int		i;
	char	*pField;

	GC_ASSERT(szBuf,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEngySavScheduleProc - szBuf is NULL\n");
	GC_ASSERT(pData,
			ERR_CTL_CFG_POINT_NULL, 
			"ParseEngySavScheduleProc - pData is NULL\n");


	/* 1. Date field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
	GC_ASSERT(IsValidNum(pField), 
			ERR_CTL_CFG_ENGY_SAV_SCHEDULE,
			"Date is not a valid number!\n");

	GC_ASSERT(((atoi(pField) < 32) && (atoi(pField) > 0)), 
			ERR_CTL_CFG_ENGY_SAV_SCHEDULE,
			"Invalid date!\n");

	/* 2. Month 1--12 field */
	for(i = 0; i < MONTHS_PER_YEAR; i++)
	{
		szBuf = Cfg_SplitStringEx(szBuf, &pField, GC_SPLITTER);
		GC_ASSERT(((*pField >= 'A') 
						&& (*pField <= 'H') 
						&& (*(pField + 1) == 0)), 
					ERR_CTL_CFG_ENGY_SAV_SCHEDULE,
					"Invalid mode for a day!\n");

		switch(*pField)
		{
		case 'A':
			pData->aenumMode[i] = ENGY_SAV_MODE_A;
			break;
		case 'B':
			pData->aenumMode[i] = ENGY_SAV_MODE_B;
			break;
		case 'C':
			pData->aenumMode[i] = ENGY_SAV_MODE_C;
			break;
		case 'D':
			pData->aenumMode[i] = ENGY_SAV_MODE_D;
			break;
		case 'E':
			pData->aenumMode[i] = ENGY_SAV_MODE_E;
			break;
		case 'F':
			pData->aenumMode[i] = ENGY_SAV_MODE_F;
			break;
		case 'G':
			pData->aenumMode[i] = ENGY_SAV_MODE_G;
			break;
		case 'H':
			pData->aenumMode[i] = ENGY_SAV_MODE_H;
			break;
		default:
			break;
		}
	}

	return ERR_OK;
}


/*==========================================================================*
 * FUNCTION : IsValidStr
 * PURPOSE  : Check if the field is a valid string
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *pField : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:38
 *==========================================================================*/
static BOOL IsValidStr(const char *pField)
{
	return (*pField >= 'A' && *pField <= 'Z') 
			|| (*pField >= 'a' && *pField <= 'z');
}



/*==========================================================================*
 * FUNCTION : IsValidNum
 * PURPOSE  : Check if the field is a valid number
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *pField : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:38
 *==========================================================================*/
static BOOL IsValidNum(const char *pField)
{
	int	i;
	int	iNumOfPoint = 0;

	if(!(*pField))
	{
		return FALSE;
	}

	if((*pField == '-') 
		|| (*pField == '+')
		|| (*pField == ' '))
	{
		i = 1;
	}
	else
	{
		i = 0;
	}

	while(*(pField + i))
	{
		if(*(pField + i) == '.')
		{
			iNumOfPoint++;
			if(iNumOfPoint > 1)
			{
				return FALSE;
			}
		}
		else if ((i > GC_MAX_NUM_PER_FIELD)
			|| (*(pField + i) < '0') 
			|| (*(pField + i) > '9'))
		{
			return FALSE;
		}
		i++;
	}

	return TRUE;
}



/*==========================================================================*
 * FUNCTION : StuffRectSig
 * PURPOSE  : Stuff RectPri Id and Type data into pPriCfg, then destroy 
              pPrimaryPriCfg->pRectPriSig
 * CALLS    : strcmp
 * CALLED BY: ConvertCfgData
 * ARGUMENTS: PRIMARY_PRI_CFG  *pPrimaryPriCfg : 
 *            GC_PRI_CFG*   pPriCfg      : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:45
 *==========================================================================*/
static void StuffRectPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{RT_PRI_OUT_VOLT,		"RT_PRI_OUT_VOLT",		},
		{RT_PRI_OUT_CURR,		"RT_PRI_OUT_CURR",		},
		{RT_PRI_CURR_LMT,		"RT_PRI_CURR_LMT",		},
		{RT_PRI_RECT_ID,		"RT_PRI_RECT_ID",		},
		{RT_PRI_RECT_HI_ID,		"RT_PRI_RECT_HI_ID",	},
		{RT_PRI_USED_CAP,		"RT_PRI_USED_CAP",		},
		{RT_PRI_OPER_TIME,		"RT_PRI_OPER_TIME",		},
		{RT_PRI_CURR_LMT_SET,	"RT_PRI_CURR_LMT_SET",	},
		{RT_PRI_DC_ON_OFF_STA,	"RT_PRI_DC_ON_OFF_STA",	},
		{RT_PRI_DC_ON_OFF_CTL,	"RT_PRI_DC_ON_OFF_CTL",	},
		{RT_PRI_AC_ON_OFF_STA,	"RT_PRI_AC_ON_OFF_STA",	},
		{RT_PRI_AC_ON_OFF_CTL,	"RT_PRI_AC_ON_OFF_CTL",	},
		{RT_PRI_AC_FAIL,		"RT_PRI_AC_FAIL",		},
		{RT_PRI_OVER_TEMP,		"RT_PRI_OVER_TEMP",		},
		{RT_PRI_FAULT,			"RT_PRI_FAULT",			},
		{RT_PRI_OVER_VOLT,		"RT_PRI_OVER_VOLT",		},
		{RT_PRI_PROTCTION,		"RT_PRI_PROTCTION",		},
		{RT_PRI_FAN_FAULT,		"RT_PRI_FAN_FAULT",		},
		{RT_PRI_NO_RESPOND,		"RT_PRI_NO_RESPOND",	},
		{RT_PRI_POWER_LMT_RECT,	"RT_PRI_POWER_LMT_RECT",},
		{RT_PRI_VALID_CAP,		"RT_PRI_VALID_CAP",		},
		{RT_PRI_EFFICIENCY_NO,	"RT_PRI_EFFICIENCY_NO",	},
		{RT_PRI_HVSD_STATUS,	"RT_PRI_HVSD_STATUS",	},
		{RT_PRI_VALID_RATED_CURRENT,	"RT_PRI_VALID_RATED_CURRENT",	},
		{RT_PRI_BARCODE1,		"RT_PRI_BARCODE1",	},
		{RT_PRI_NO_RESPOND_STATE,	"RT_PRI_NO_RESPOND_STATE"},
		 
		{MAX_RECT_PRI_SIG_NUM,	"",						},
	};


	pPriCfg->iRectPriSigNum = pPrimaryPriCfg->iRectPriSigNum;
	ASSERT(pPrimaryPriCfg->iRectPriSigNum);

	pPriCfg->pRectPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iRectPriSigNum);
	GC_ASSERT(pPriCfg->pRectPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pRectPriSig! \n");


	j = 0;
	while(Sig[j].iIndex < MAX_RECT_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iRectPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			"The Rect signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iRectPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pRectPriSig + i)->pName))
			{
				(pPriCfg->pRectPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pRectPriSig + i)->iId;

				(pPriCfg->pRectPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pRectPriSig + i)->iType;

				//TRACE("GEN_CTL Debug: %s \n", Sig[j].szName);

				//DELETE((pPrimaryPriCfg->pRectPriSig + i)->pName);

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iRectPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iRectPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pRectPriSig + i)->pName);
	}
	DELETE(pPrimaryPriCfg->pRectPriSig);
	return;
}


/*==========================================================================*
 * FUNCTION : StuffS1RectSig
 * PURPOSE  : Stuff RectPri Id and Type data into pPriCfg, then destroy 
              pPrimaryPriCfg->pRectPriSig
 * CALLS    : strcmp
 * CALLED BY: ConvertCfgData
 * ARGUMENTS: PRIMARY_PRI_CFG  *pPrimaryPriCfg : 
 *            GC_PRI_CFG*   pPriCfg      : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:45
 *==========================================================================*/
static void StuffS1RectPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{S1_PRI_OUT_CURR,		"S1_PRI_OUT_CURR",		},
		{S1_PRI_CURR_LMT,		"S1_PRI_CURR_LMT",		},
		{S1_PRI_RECT_ID,		"S1_PRI_RECT_ID",		},
		{S1_PRI_RECT_HI_ID,		"S1_PRI_RECT_HI_ID",	},
		{S1_PRI_OPER_TIME,		"S1_PRI_OPER_TIME",		},
		{S1_PRI_AC_FAIL,		"S1_PRI_AC_FAIL",		},
		{S1_PRI_DC_ON_OFF_STA,	"S1_PRI_DC_ON_OFF_STA",	},
		{S1_PRI_AC_ON_OFF_STA,	"S1_PRI_AC_ON_OFF_STA",	},
		{S1_PRI_DC_ON_OFF_CTL,	"S1_PRI_DC_ON_OFF_CTL",	},
		{S1_PRI_AC_ON_OFF_CTL,	"S1_PRI_AC_ON_OFF_CTL",	},
		{S1_PRI_OVER_TEMP,		"S1_PRI_OVER_TEMP",		},
		{S1_PRI_FAULT,			"S1_PRI_FAULT",			},
		{S1_PRI_OVER_VOLT,		"S1_PRI_OVER_VOLT",		},
		{S1_PRI_PROTCTION,		"S1_PRI_PROTCTION",		},
		{S1_PRI_FAN_FAULT,		"S1_PRI_FAN_FAULT",		},
		{S1_PRI_NO_RESPOND,		"S1_PRI_NO_RESPOND",	},
		{S1_PRI_POWER_LMT_RECT,	"S1_PRI_POWER_LMT_RECT",},
		{S1_PRI_VALID_CAP,		"S1_PRI_VALID_CAP",		},
		{S1_PRI_EFFICIENCY_NO,	"S1_PRI_EFFICIENCY_NO",	},
		{S1_PRI_HVSD_STATUS,	"S1_PRI_HVSD_STATUS",	},
		{S1_PRI_OUT_VOLT,	"S1_PRI_OUT_VOLT"},
		{S1_PRI_BARCODE1,	"S1_PRI_BARCODE1"},
		{S1_PRI_NO_RESPOND_STATE,"S1_PRI_NO_RESPOND_STATE"},	
		 
		{MAX_S1_RECT_PRI_SIG_NUM,	"",					},
	};


	pPriCfg->iS1RectPriSigNum = pPrimaryPriCfg->iS1RectPriSigNum;
	ASSERT(pPrimaryPriCfg->iS1RectPriSigNum);

	pPriCfg->pS1RectPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iS1RectPriSigNum);
	GC_ASSERT(pPriCfg->pS1RectPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pS1RectPriSig! \n");


	j = 0;
	while(Sig[j].iIndex < MAX_S1_RECT_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iS1RectPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			"The Rect signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iS1RectPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pS1RectPriSig + i)->pName))
			{
				(pPriCfg->pS1RectPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pS1RectPriSig + i)->iId;

				(pPriCfg->pS1RectPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pS1RectPriSig + i)->iType;

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iS1RectPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iS1RectPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pS1RectPriSig + i)->pName);
	}
	DELETE(pPrimaryPriCfg->pS1RectPriSig);

	return;
}


/*==========================================================================*
 * FUNCTION : StuffS2RectSig
 * PURPOSE  : Stuff RectPri Id and Type data into pPriCfg, then destroy 
              pPrimaryPriCfg->pRectPriSig
 * CALLS    : strcmp
 * CALLED BY: ConvertCfgData
 * ARGUMENTS: PRIMARY_PRI_CFG  *pPrimaryPriCfg : 
 *            GC_PRI_CFG*   pPriCfg      : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:45
 *==========================================================================*/
static void StuffS2RectPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{S2_PRI_OUT_CURR,		"S2_PRI_OUT_CURR",		},
		{S2_PRI_CURR_LMT,		"S2_PRI_CURR_LMT",		},
		{S2_PRI_RECT_ID,		"S2_PRI_RECT_ID",		},
		{S2_PRI_RECT_HI_ID,		"S2_PRI_RECT_HI_ID",	},
		{S2_PRI_OPER_TIME,		"S2_PRI_OPER_TIME",		},
		{S2_PRI_AC_FAIL,		"S2_PRI_AC_FAIL",		},
		{S2_PRI_DC_ON_OFF_STA,	"S2_PRI_DC_ON_OFF_STA",	},
		{S2_PRI_AC_ON_OFF_STA,	"S2_PRI_AC_ON_OFF_STA",	},
		{S2_PRI_DC_ON_OFF_CTL,	"S2_PRI_DC_ON_OFF_CTL",	},
		{S2_PRI_AC_ON_OFF_CTL,	"S2_PRI_AC_ON_OFF_CTL",	},
		{S2_PRI_OVER_TEMP,		"S2_PRI_OVER_TEMP",		},
		{S2_PRI_FAULT,			"S2_PRI_FAULT",			},
		{S2_PRI_OVER_VOLT,		"S2_PRI_OVER_VOLT",		},
		{S2_PRI_PROTCTION,		"S2_PRI_PROTCTION",		},
		{S2_PRI_FAN_FAULT,		"S2_PRI_FAN_FAULT",		},
		{S2_PRI_NO_RESPOND,		"S2_PRI_NO_RESPOND",	},
		{S2_PRI_POWER_LMT_RECT,	"S2_PRI_POWER_LMT_RECT",},
		{S2_PRI_VALID_CAP,		"S2_PRI_VALID_CAP",		},
		{S2_PRI_EFFICIENCY_NO,	"S2_PRI_EFFICIENCY_NO",	},
		{S2_PRI_HVSD_STATUS,	"S2_PRI_HVSD_STATUS",	},
		{S2_PRI_OUT_VOLT,	"S2_PRI_OUT_VOLT"},
		{S2_PRI_BARCODE1,	"S2_PRI_BARCODE1"},
		{S2_PRI_NO_RESPOND_STATE,"S2_PRI_NO_RESPOND_STATE"},
		{MAX_S2_RECT_PRI_SIG_NUM,	"",					},
	};


	pPriCfg->iS2RectPriSigNum = pPrimaryPriCfg->iS2RectPriSigNum;
	ASSERT(pPrimaryPriCfg->iS2RectPriSigNum);

	pPriCfg->pS2RectPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iS2RectPriSigNum);
	GC_ASSERT(pPriCfg->pS2RectPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pS2RectPriSig! \n");


	j = 0;
	while(Sig[j].iIndex < MAX_S2_RECT_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iS2RectPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			"The Rect signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iS2RectPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pS2RectPriSig + i)->pName))
			{
				(pPriCfg->pS2RectPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pS2RectPriSig + i)->iId;

				(pPriCfg->pS2RectPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pS2RectPriSig + i)->iType;

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iS2RectPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iS2RectPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pS2RectPriSig + i)->pName);
	}
	DELETE(pPrimaryPriCfg->pS2RectPriSig);

	return;
}


/*==========================================================================*
 * FUNCTION : StuffS3RectSig
 * PURPOSE  : Stuff RectPri Id and Type data into pPriCfg, then destroy 
              pPrimaryPriCfg->pRectPriSig
 * CALLS    : strcmp
 * CALLED BY: ConvertCfgData
 * ARGUMENTS: PRIMARY_PRI_CFG  *pPrimaryPriCfg : 
 *            GC_PRI_CFG*   pPriCfg      : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:45
 *==========================================================================*/
static void StuffS3RectPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{S3_PRI_OUT_CURR,		"S3_PRI_OUT_CURR",		},
		{S3_PRI_CURR_LMT,		"S3_PRI_CURR_LMT",		},
		{S3_PRI_RECT_ID,		"S3_PRI_RECT_ID",		},
		{S3_PRI_RECT_HI_ID,		"S3_PRI_RECT_HI_ID",	},
		{S3_PRI_OPER_TIME,		"S3_PRI_OPER_TIME",		},
		{S3_PRI_AC_FAIL,		"S3_PRI_AC_FAIL",		},
		{S3_PRI_DC_ON_OFF_STA,	"S3_PRI_DC_ON_OFF_STA",	},
		{S3_PRI_AC_ON_OFF_STA,	"S3_PRI_AC_ON_OFF_STA",	},
		{S3_PRI_DC_ON_OFF_CTL,	"S3_PRI_DC_ON_OFF_CTL",	},
		{S3_PRI_AC_ON_OFF_CTL,	"S3_PRI_AC_ON_OFF_CTL",	},
		{S3_PRI_OVER_TEMP,		"S3_PRI_OVER_TEMP",		},
		{S3_PRI_FAULT,			"S3_PRI_FAULT",			},
		{S3_PRI_OVER_VOLT,		"S3_PRI_OVER_VOLT",		},
		{S3_PRI_PROTCTION,		"S3_PRI_PROTCTION",		},
		{S3_PRI_FAN_FAULT,		"S3_PRI_FAN_FAULT",		},
		{S3_PRI_NO_RESPOND,		"S3_PRI_NO_RESPOND",	},
		{S3_PRI_POWER_LMT_RECT,	"S3_PRI_POWER_LMT_RECT",},
		{S3_PRI_VALID_CAP,		"S3_PRI_VALID_CAP",		},
		{S3_PRI_EFFICIENCY_NO,	"S3_PRI_EFFICIENCY_NO",	},
		{S3_PRI_HVSD_STATUS,	"S3_PRI_HVSD_STATUS",	},
		{S3_PRI_OUT_VOLT,	"S3_PRI_OUT_VOLT"},
		{S3_PRI_BARCODE1,	"S3_PRI_BARCODE1"},
		{S3_PRI_NO_RESPOND_STATE,"S3_PRI_NO_RESPOND_STATE"},
		{MAX_S1_RECT_PRI_SIG_NUM,	"",					},
	};


	pPriCfg->iS3RectPriSigNum = pPrimaryPriCfg->iS3RectPriSigNum;
	ASSERT(pPrimaryPriCfg->iS3RectPriSigNum);

	pPriCfg->pS3RectPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iS3RectPriSigNum);
	GC_ASSERT(pPriCfg->pS3RectPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pS3RectPriSig! \n");


	j = 0;
	while(Sig[j].iIndex < MAX_S3_RECT_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iS3RectPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			"The Rect signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iS3RectPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pS3RectPriSig + i)->pName))
			{
				(pPriCfg->pS3RectPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pS3RectPriSig + i)->iId;

				(pPriCfg->pS3RectPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pS3RectPriSig + i)->iType;

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iS3RectPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iS3RectPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pS3RectPriSig + i)->pName);
	}
	DELETE(pPrimaryPriCfg->pS3RectPriSig);

	return;
}


/*==========================================================================*
* FUNCTION : StuffS3RectSig
* PURPOSE  : Stuff RectPri Id and Type data into pPriCfg, then destroy 
pPrimaryPriCfg->pRectPriSig
* CALLS    : strcmp
* CALLED BY: ConvertCfgData
* ARGUMENTS: PRIMARY_PRI_CFG  *pPrimaryPriCfg : 
*            GC_PRI_CFG*   pPriCfg      : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-19 16:45
*==========================================================================*/
static void StuffCTPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{		
		{CT_PRI_OUT_VOLT,		"CT_PRI_OUT_VOLT",		},	
		{CT_PRI_OUT_CURR,		"CT_PRI_OUT_CURR",		},	
		{CT_PRI_ONOFF_STATE,		"CT_PRI_ONOFF_STATE",		},
		{CT_PRI_COMMUNIATE_STATE,	"CT_PRI_COMMUNIATE_STATE",	},
		{CT_PRI_EXIST_STATE,		"CT_PRI_EXIST_STATE",		},

		{CT_PRI_NO_RESPONSE,		"CT_PRI_NO_RESPONSE",		},	
		{CT_PRI_OVER_TEMP,		"CT_PRI_OVER_TEMP",		},	
		{CT_PRI_HVSD_ALM,		"CT_PRI_HVSD_ALM",		},
		{CT_PRI_FAN_FAIL,		"CT_PRI_FAN_FAIL",	},
		{CT_PRI_POWER_LIMIT,		"CT_PRI_POWER_LIMIT",		},
		{CT_PRI_AC_UNDERVOLT,		"CT_PRI_AC_UNDERVOLT",		},	
		{CT_PRI_CONV_FAIL,		"CT_PRI_CONV_FAIL",		},	
		{CT_PRI_EEROM_FAIL,		"CT_PRI_EEROM_FAIL",		},
		{CT_PRI_THERM_SD,		"CT_PRI_THERM_SD",		},
		{CT_PRI_ID_OVERLAP,		"CT_PRI_ID_OVERLAP",		},

		{CT_PRI_OVER_VOLTAGE,		"CT_PRI_OVER_VOLTAGE",		},
		{CT_PRI_UNDER_VOLTAGE,		"CT_PRI_UNDER_VOLTAGE",		},

		{CT_PRI_OVER_VOLTAGE_24V,	"CT_PRI_OVER_VOLTAGE_24V",		},
		{CT_PRI_UNDER_VOLTAGE_24V,	"CT_PRI_UNDER_VOLTAGE_24V",		},

		{MAX_CT_PRI_SIG_NUM,	"",					},
	};


	pPriCfg->iCTPriSig = pPrimaryPriCfg->iCTPriSigNum;
	ASSERT(pPrimaryPriCfg->iCTPriSigNum);

	pPriCfg->pCTPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iCTPriSig);
	GC_ASSERT(pPriCfg->pCTPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pS3RectPriSig! \n");


	j = 0;
	while(Sig[j].iIndex < MAX_CT_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iCTPriSig),
			ERR_CTL_CFG_RECT_SIG,
			"The Converter signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iCTPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
				(pPrimaryPriCfg->pCTPriSig + i)->pName))
			{
				(pPriCfg->pCTPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pCTPriSig + i)->iId;

				(pPriCfg->pCTPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pCTPriSig + i)->iType;

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iCTPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iCTPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pCTPriSig + i)->pName);
	}
	DELETE(pPrimaryPriCfg->pCTPriSig);

	return;
}
/*==========================================================================*
 * FUNCTION : StuffBattSig
 * PURPOSE  : Stuff BattPri Id and Type data into pPriCfg, then destroy 
              pPrimaryPriCfg->pBattPriSig
 * CALLS    : strcmp
 * CALLED BY: ConvertCfgData
 * ARGUMENTS: PRIMARY_PRI_CFG  *pPrimaryPriCfg : 
 *            GC_PRI_CFG*   pPriCfg      : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:45
 *==========================================================================*/
static void StuffBattPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{BT_PRI_CURR,			"BT_PRI_CURR",			},
		{BT_PRI_VOLT,			"BT_PRI_VOLT",			},
		{BT_PRI_CAP,			"BT_PRI_CAP",			},
		{BT_PRI_USED_BY_BM,		"BT_PRI_USED_BY_BM",		},
		{BT_PRI_RATED_CAP,		"BT_PRI_RATED_CAP",		},
		//{BT_PRI_TEMP_NO,		"BT_PRI_TEMP_NO",		},
		{BT_PRI_TEMP,			"BT_PRI_TEMP",			},
		{BT_PRI_BLK1_VOLT,		"BT_PRI_BLK1_VOLT",		},
		{BT_PRI_BLK2_VOLT,		"BT_PRI_BLK2_VOLT",		},
		{BT_PRI_BLK3_VOLT,		"BT_PRI_BLK3_VOLT",		},
		{BT_PRI_BLK4_VOLT,		"BT_PRI_BLK4_VOLT",		},
		{BT_PRI_BLK5_VOLT,		"BT_PRI_BLK5_VOLT",		},
		{BT_PRI_BLK6_VOLT,		"BT_PRI_BLK6_VOLT",		},
		{BT_PRI_BLK7_VOLT,		"BT_PRI_BLK7_VOLT",		},
		{BT_PRI_BLK8_VOLT,		"BT_PRI_BLK8_VOLT",		},
		{BT_PRI_BLK9_VOLT,		"BT_PRI_BLK9_VOLT",		},
		{BT_PRI_BLK10_VOLT,		"BT_PRI_BLK10_VOLT",		},
		{BT_PRI_BLK11_VOLT,		"BT_PRI_BLK11_VOLT",		},
		{BT_PRI_BLK12_VOLT,		"BT_PRI_BLK12_VOLT",		},
		{BT_PRI_BLK13_VOLT,		"BT_PRI_BLK13_VOLT",		},
		{BT_PRI_BLK14_VOLT,		"BT_PRI_BLK14_VOLT",		},
		{BT_PRI_BLK15_VOLT,		"BT_PRI_BLK15_VOLT",		},
		{BT_PRI_BLK16_VOLT,		"BT_PRI_BLK16_VOLT",		},
		{BT_PRI_BLK17_VOLT,		"BT_PRI_BLK17_VOLT",		},
		{BT_PRI_BLK18_VOLT,		"BT_PRI_BLK18_VOLT",		},
		{BT_PRI_BLK19_VOLT,		"BT_PRI_BLK19_VOLT",		},
		{BT_PRI_BLK20_VOLT,		"BT_PRI_BLK20_VOLT",		},
		{BT_PRI_BLK21_VOLT,		"BT_PRI_BLK21_VOLT",		},
		{BT_PRI_BLK22_VOLT,		"BT_PRI_BLK22_VOLT",		},
		{BT_PRI_BLK23_VOLT,		"BT_PRI_BLK23_VOLT",		},
		{BT_PRI_BLK24_VOLT,		"BT_PRI_BLK24_VOLT",		},
		{BT_PRI_HIGH_TEMP_TIME,		"BT_PRI_HIGH_TEMP_TIME",	},
		{BT_PRI_LOW_TEMP_TIME,		"BT_PRI_LOW_TEMP_TIME",		},
		{BT_PRI_MAX_BLK_VOLT_DIFF,	"BT_PRI_MAX_BLK_VOLT_DIFF",	},
		{BT_PRI_OVER_BLK_VOLT_DIFF_SET, "BT_PRI_OVER_BLK_VOLT_DIFF_SET",},
		{BT_PRI_LDU_FUSE_ALARM,		"BT_PRI_LDU_FUSE_ALARM",	},
		{BT_PRI_CAP_PERCENT,		"BT_PRI_CAP_PERCENT",	},
		
		 
		{MAX_BATT_PRI_SIG_NUM,	"",						},
	};


	pPriCfg->iBattPriSigNum = pPrimaryPriCfg->iBattPriSigNum;
	ASSERT(pPrimaryPriCfg->iBattPriSigNum);

	pPriCfg->pBattPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iBattPriSigNum);
	GC_ASSERT(pPriCfg->pBattPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pBattPriSig! \n");

	j = 0;
	while(Sig[j].iIndex < MAX_BATT_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iBattPriSigNum),
			ERR_CTL_CFG_BATT_SIG,
			"The Batt signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i < pPrimaryPriCfg->iBattPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pBattPriSig + i)->pName))
			{

				(pPriCfg->pBattPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pBattPriSig + i)->iId;

				(pPriCfg->pBattPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pBattPriSig + i)->iType;

				//DELETE((pPrimaryPriCfg->pBattPriSig + i)->pName);
				

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iBattPriSigNum),
			ERR_CTL_CFG_BATT_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iBattPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pBattPriSig + i)->pName);

	}
	DELETE(pPrimaryPriCfg->pBattPriSig);
	return;
}



static void StuffBattFusePriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{BF_PRI_STATE1, 	"BF_PRI_STATE1", 	},
		{BF_PRI_STATE2,		"BF_PRI_STATE2",	},
		{BF_PRI_STATE3,		"BF_PRI_STATE3",	},
		{BF_PRI_STATE4,		"BF_PRI_STATE4",	},
		{BF_PRI_STATE5,		"BF_PRI_STATE5",	},
		{BF_PRI_STATE6,		"BF_PRI_STATE6",	},		
		 
		{MAX_BATT_FUSE_PRI_SIG_NUM,	"",			},
	};


	pPriCfg->iBattFusePriSigNum = pPrimaryPriCfg->iBattFusePriSigNum;
	ASSERT(pPrimaryPriCfg->iBattFusePriSigNum);

	pPriCfg->pBattFusePriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iBattFusePriSigNum);
	GC_ASSERT(pPriCfg->pBattFusePriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pBattFusePriSig! \n");

	j = 0;
	while(Sig[j].iIndex < MAX_BATT_FUSE_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iBattFusePriSigNum),
			ERR_CTL_CFG_BATT_FUSE_SIG,
			"The BattFuse signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iBattFusePriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pBattFusePriSig + i)->pName))
			{

				(pPriCfg->pBattFusePriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pBattFusePriSig + i)->iId;

				(pPriCfg->pBattFusePriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pBattFusePriSig + i)->iType;

				//TRACE("GEN_CTL Debug: %s \n", Sig[j].szName);
				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iBattFusePriSigNum),
			ERR_CTL_CFG_BATT_FUSE_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iBattFusePriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pBattFusePriSig + i)->pName);

	}
	DELETE(pPrimaryPriCfg->pBattFusePriSig);

	return;
}


static void StuffEibPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{EIB_BLOCK1_VOLTAGE,	"EIB_BLOCK1_VOLTAGE", 	},
		{EIB_BLOCK2_VOLTAGE,	"EIB_BLOCK2_VOLTAGE",	},
		{EIB_BLOCK3_VOLTAGE,	"EIB_BLOCK3_VOLTAGE",	},
		{EIB_BLOCK4_VOLTAGE,	"EIB_BLOCK4_VOLTAGE",	},	
		{EIB_BLOCK5_VOLTAGE, 	"EIB_BLOCK5_VOLTAGE", 	},
		{EIB_BLOCK6_VOLTAGE,	"EIB_BLOCK6_VOLTAGE",	},
		{EIB_BLOCK7_VOLTAGE,	"EIB_BLOCK7_VOLTAGE",	},
		{EIB_BLOCK8_VOLTAGE,	"EIB_BLOCK8_VOLTAGE",	},
		{EIB_EXIST_STATE,	"EIB_EXIST_STATE",	},
		 
		{MAX_EIB_PRI_SIG_NUM,	"",						},
	};


	pPriCfg->iEibPriSigNum = pPrimaryPriCfg->iEibPriSigNum;
	ASSERT(pPrimaryPriCfg->iEibPriSigNum);

	pPriCfg->pEibPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iEibPriSigNum);
	GC_ASSERT(pPriCfg->pEibPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pEibPriSig! \n");

	j = 0;
	while(Sig[j].iIndex < MAX_EIB_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iEibPriSigNum),
			ERR_CTL_CFG_EIB_SIG,
			"The EIB signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iEibPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pEibPriSig + i)->pName))
			{
				(pPriCfg->pEibPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pEibPriSig + i)->iId;

				(pPriCfg->pEibPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pEibPriSig + i)->iType;
				//TRACE("GEN_CTL Debug: %s \n", Sig[j].szName);
				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iEibPriSigNum),
			ERR_CTL_CFG_EIB_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iEibPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pEibPriSig + i)->pName);

	}
	DELETE(pPrimaryPriCfg->pEibPriSig);

	return;
}

#ifdef GC_SUPPORT_MPPT

static void StuffMpptPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{MT_PRI_OUT_VOLT,		"MT_PRI_OUT_VOLT",		},
		{MT_PRI_OUT_CURR,		"MT_PRI_OUT_CURR",		},
		{MT_PRI_CURR_LMT,		"MT_PRI_CURR_LMT",		},
		{MT_PRI_RECT_ID,		"MT_PRI_RECT_ID",		},
		{MT_PRI_RECT_HI_ID,		"MT_PRI_RECT_HI_ID",	},
		{MT_PRI_USED_CAP,		"MT_PRI_USED_CAP",		},
		{MT_PRI_OPER_TIME,		"MT_PRI_OPER_TIME",		},
		{MT_PRI_CURR_LMT_SET,	"MT_PRI_CURR_LMT_SET",	},
		{MT_PRI_DC_ON_OFF_STA,	"MT_PRI_DC_ON_OFF_STA",	},
		{MT_PRI_DC_ON_OFF_CTL,	"MT_PRI_DC_ON_OFF_CTL",	},
		{MT_PRI_OVER_TEMP,		"MT_PRI_OVER_TEMP",		},
		{MT_PRI_FAULT,			"MT_PRI_FAULT",			},
		{MT_PRI_OVER_VOLT,		"MT_PRI_OVER_VOLT",		},
		{MT_PRI_PROTCTION,		"MT_PRI_PROTCTION",		},
		{MT_PRI_FAN_FAULT,		"MT_PRI_FAN_FAULT",		},
		{MT_PRI_NO_RESPOND,		"MT_PRI_NO_RESPOND",	},
		{MT_PRI_POWER_LMT_RECT,	"MT_PRI_POWER_LMT_RECT",},
		{MT_PRI_VALID_CAP,		"MT_PRI_VALID_CAP",		},		
		{MT_PRI_VALID_RATED_CURRENT,	"MT_PRI_VALID_RATED_CURRENT",	},
		{MT_PRI_BARCODE1,		"MT_PRI_BARCODE1",	},
		{MT_PRI_NIGHT_STATUS,		"MT_PRI_NIGHT_STATUS",	},
		{MT_PRI_INPUT_CURRENT,		"MT_PRI_INPUT_CURRENT",	},
		{MT_PRI_INPUT_VOLTAGE,		"MT_PRI_INPUT_VOLTAGE",	},
		{MT_PRI_INPUT_POWER,		"MT_PRI_INPUT_POWER",	},
		{MT_PRI_OUTPUT_POWER,		"MT_PRI_OUTPUT_POWER",	},

		{MAX_MPPT_PRI_SIG_NUM,	"",						},
	};


	pPriCfg->iMpptPriSigNum = pPrimaryPriCfg->iMpptPriSigNum;
	ASSERT(pPrimaryPriCfg->iMpptPriSigNum);

	pPriCfg->pMpptPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iMpptPriSigNum);
	GC_ASSERT(pPriCfg->pMpptPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pRectPriSig! \n");


	j = 0;
	while(Sig[j].iIndex < MAX_MPPT_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");
		GC_ASSERT((Sig[j].iIndex < pPriCfg->iMpptPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			"The Mppt signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iMpptPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
				(pPrimaryPriCfg->pMpptPriSig + i)->pName))
			{
				(pPriCfg->pMpptPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pMpptPriSig + i)->iId;

				(pPriCfg->pMpptPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pMpptPriSig + i)->iType;

				//TRACE("GEN_CTL Debug: %s \n", Sig[j].szName);

				//DELETE((pPrimaryPriCfg->pMpptPriSig + i)->pName);

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iMpptPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iMpptPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pMpptPriSig + i)->pName);
	}
	DELETE(pPrimaryPriCfg->pMpptPriSig);
	return;
}

#endif
#ifdef GC_SUPPORT_BMS

static void StuffBMSPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{BMS_PRI_CURRENT,		"BMS_PRI_CURRENT",		},
		{BMS_PRI_PROTECT_STA,		"BMS_PRI_PROTECT_STA",		},
		{MAX_BMS_PRI_SIG_NUM,	"",						},
	};


	pPriCfg->iBMSPriSigNum = pPrimaryPriCfg->iBMSPriSigNum;
	ASSERT(pPrimaryPriCfg->iBMSPriSigNum);

	pPriCfg->pBMSPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iBMSPriSigNum);
	GC_ASSERT(pPriCfg->pBMSPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pBMSPriSig! \n");


	j = 0;
	while(Sig[j].iIndex < MAX_BMS_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");
		GC_ASSERT((Sig[j].iIndex < pPriCfg->iBMSPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			"The BMS signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iBMSPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
				(pPrimaryPriCfg->pBMSPriSig + i)->pName))
			{
				(pPriCfg->pBMSPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pBMSPriSig + i)->iId;

				(pPriCfg->pBMSPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pBMSPriSig + i)->iType;

				printf("GEN_CTL Debug: %s \n", Sig[j].szName);
				printf("GEN_CTL Debug: %d \n", (pPriCfg->pBMSPriSig + Sig[j].iIndex)->iSignalID, (pPriCfg->pBMSPriSig + Sig[j].iIndex)->iSignalType);

				//DELETE((pPrimaryPriCfg->pMpptPriSig + i)->pName);

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iBMSPriSigNum),
			ERR_CTL_CFG_RECT_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iBMSPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pBMSPriSig + i)->pName);
	}
	DELETE(pPrimaryPriCfg->pBMSPriSig);
	return;
}

#endif
static void StuffLvdPriSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{LVD1_PRI_CTL, 			"LVD1_PRI_CTL",			},
		{LVD2_PRI_CTL,			"LVD2_PRI_CTL",			},
		{LVD1_PRI_ENB,			"LVD1_PRI_ENB",			},
		{LVD2_PRI_ENB,			"LVD2_PRI_ENB",			},
		{LVD1_PRI_MODE,			"LVD1_PRI_MODE",		},
		{LVD2_PRI_MODE,			"LVD2_PRI_MODE",		},
		{LVD1_HTD_ENB,			"LVD1_HTD_ENB",			},
		{LVD2_HTD_ENB,			"LVD2_HTD_ENB",			},
		{LVD1_PRI_VOLT,			"LVD1_PRI_VOLT",		},
		{LVD2_PRI_VOLT,			"LVD2_PRI_VOLT",		},
		{LVD1_PRI_TIME,			"LVD1_PRI_TIME",		},
		{LVD2_PRI_TIME,			"LVD2_PRI_TIME",		},
		{LVD1_PRI_RECON_VOLT,	"LVD1_PRI_RECON_VOLT",	},
		{LVD2_PRI_RECON_VOLT,	"LVD2_PRI_RECON_VOLT",	},
		{LVD1_PRI_DELAY,		"LVD1_PRI_DELAY",		},
		{LVD2_PRI_DELAY,		"LVD2_PRI_DELAY",		},
		{LVD1_PRI_DEPEND,		"LVD1_PRI_DEPEND",		},
		{LVD2_PRI_DEPEND,		"LVD2_PRI_DEPEND",		},
		{LVD1_PRI_VOLT24,		"LVD1_PRI_VOLT24",		},
		{LVD2_PRI_VOLT24,		"LVD2_PRI_VOLT24",		},
		{LVD1_PRI_RECON_VOLT24,	"LVD1_PRI_RECON_VOLT24",},
		{LVD2_PRI_RECON_VOLT24,	"LVD2_PRI_RECON_VOLT24",},
		{LVD_EQUIP_STATE,	"LVD_EQUIP_STATE",},
		{LVD1_STATE,		"LVD1_STATE",},
		{LVD2_STATE,		"LVD2_STATE",},
	 
		{MAX_LVD_PRI_SIG_NUM,	"",						},
	};


	pPriCfg->iLvdPriSigNum = pPrimaryPriCfg->iLvdPriSigNum;
	ASSERT(pPrimaryPriCfg->iLvdPriSigNum);

	pPriCfg->pLvdPriSig = NEW(GC_SIGNAL_ID_TYPE, pPriCfg->iLvdPriSigNum);
	GC_ASSERT(pPriCfg->pLvdPriSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pLvdPriSig! \n");

	j = 0;
	while(Sig[j].iIndex < MAX_LVD_PRI_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		GC_ASSERT((Sig[j].iIndex < pPriCfg->iLvdPriSigNum),
			ERR_CTL_CFG_LVD_SIG,
			"The LVD signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iLvdPriSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pLvdPriSig + i)->pName))
			{

				(pPriCfg->pLvdPriSig + Sig[j].iIndex)->iSignalID 
					= (pPrimaryPriCfg->pLvdPriSig + i)->iId;

				(pPriCfg->pLvdPriSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pLvdPriSig + i)->iType;
				//TRACE("GEN_CTL Debug: %s \n", Sig[j].szName);
				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iLvdPriSigNum),
			ERR_CTL_CFG_BATT_FUSE_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iLvdPriSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pLvdPriSig + i)->pName);

	}

	DELETE(pPrimaryPriCfg->pLvdPriSig);

	return;
}



/*==========================================================================*
 * FUNCTION : StuffSig
 * PURPOSE  : Stuff Id and Type data into pPriCfg, then destroy 
              pPrimaryPriCfg->pBattSig
 * CALLS    : strcmp
 * CALLED BY: ConvertCfgData
 * ARGUMENTS: PRIMARY_PRI_CFG  *pPrimaryPriCfg : 
 *            GC_PRI_CFG*   pPriCfg      : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:45
 *==========================================================================*/
static void StuffSig(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	int				i, j;
	GC_CFG_INDEX	Sig[] =
	{
		{GC_PUB_CAN_INTERRUPT,			"GC_PUB_CAN_INTERRUPT",			},
		{GC_PUB_AUTO_MAN,				"GC_PUB_AUTO_MAN",				},
		{GC_MAN_TO_AUTO_DELAY,			"GC_MAN_TO_AUTO_DELAY",			},
		{GC_PUB_MASTER_SLAVE_MODE,		"GC_PUB_MASTER_SLAVE_MODE",		},
		{GC_PUB_SYS_VOLT,				"GC_PUB_SYS_VOLT",				},
		{GC_PUB_SYS_LOAD,				"GC_PUB_SYS_LOAD",				},
		{DC_PUB_UNDER_VOLT,				"DC_PUB_UNDER_VOLT",			},
		{DC_PUB_VERY_HIGH_VOLT,			"DC_PUB_VERY_HIGH_VOLT",		},
		{DC_PUB_UNDER_VOLT24,			"DC_PUB_UNDER_VOLT24",			},
		{DC_PUB_VERY_HIGH_VOLT24,		"DC_PUB_VERY_HIGH_VOLT24",		},
		{DC_PUB_HIGH_VOLT,				"DC_PUB_HIGH_VOLT",				},
		{DC_PUB_HIGH_VOLT24,			"DC_PUB_HIGH_VOLT24",			},
		{DC_PUB_VERY_UNDER_VOLT,		"DC_PUB_VERY_UNDER_VOLT",		},
		{GC_PUB_TIME_ZONE,				"GC_PUB_TIME_ZONE",				},
		{GC_PUB_ABNOR_LOAD_SET,			"GC_PUB_ABNOR_LOAD_SET",		},
		{GC_PUB_IMBAL_PROT_ENB,			"GC_PUB_IMBAL_PROT_ENB",		},
		{GC_PUB_TEST_STATE,				"GC_PUB_TEST_STATE",			},
		{GC_PUB_TEST_REDUCE_TIMES,		"GC_PUB_TEST_REDUCE_TIMES",		},
		{DC_PUB_VOLT,					"DC_PUB_VOLT",					},
		{GC_PUB_TEMPERATURE1,			"GC_PUB_TEMPERATURE1",			},
		{GC_PUB_TEMPERATURE2,			"GC_PUB_TEMPERATURE2",			},
		{GC_PUB_TEMPERATURE3,			"GC_PUB_TEMPERATURE3",			},
		{GC_PUB_TEMPERATURE4,			"GC_PUB_TEMPERATURE4",			},
		{GC_PUB_TEMPERATURE5,			"GC_PUB_TEMPERATURE5",			},
		{GC_PUB_TEMPERATURE6,			"GC_PUB_TEMPERATURE6",			},
		{GC_PUB_TEMPERATURE7,			"GC_PUB_TEMPERATURE7",			},
		{GC_PUB_LduTEMPERATURE1,		"GC_PUB_LduTEMPERATURE1",		},
		{GC_PUB_LduTEMPERATURE2,		"GC_PUB_LduTEMPERATURE2",		},
		{GC_PUB_LduTEMPERATURE3,		"GC_PUB_LduTEMPERATURE3",		},		
		{GC_PUB_AUTO_MODE,			"GC_PUB_AUTO_MODE",			},
		{GC_PUB_TEMP1_USAGE,			"GC_PUB_TEMP1_USAGE",			},
		{GC_PUB_TEMP2_USAGE,			"GC_PUB_TEMP2_USAGE",			},
		{GC_PUB_TEMP3_USAGE,			"GC_PUB_TEMP3_USAGE",			},
		{GC_PUB_TEMP4_USAGE,			"GC_PUB_TEMP4_USAGE",			},
		{GC_PUB_TEMP5_USAGE,			"GC_PUB_TEMP5_USAGE",			},
		{GC_PUB_TEMP6_USAGE,			"GC_PUB_TEMP6_USAGE",			},
		{GC_PUB_TEMP7_USAGE,			"GC_PUB_TEMP7_USAGE",			},
		//{GC_PUB_HI_TEMP1_ALM,			"GC_PUB_HI_TEMP1_ALM",			},
		//{GC_PUB_HI_TEMP2_ALM,			"GC_PUB_HI_TEMP2_ALM",			},
		//{GC_PUB_HI_TEMP3_ALM,			"GC_PUB_HI_TEMP3_ALM",			},
		//{GC_PUB_HI_TEMP4_ALM,			"GC_PUB_HI_TEMP4_ALM",			},
		//{GC_PUB_HI_TEMP5_ALM,			"GC_PUB_HI_TEMP5_ALM",			},
		//{GC_PUB_HI_TEMP6_ALM,			"GC_PUB_HI_TEMP6_ALM",			},
		//{GC_PUB_HI_TEMP7_ALM,			"GC_PUB_HI_TEMP7_ALM",			},		
		//{GC_PUB_VERY_HI_TEMP1_ALM,		"GC_PUB_VERY_HI_TEMP1_ALM",		},
		//{GC_PUB_VERY_HI_TEMP2_ALM,		"GC_PUB_VERY_HI_TEMP2_ALM",		},
		//{GC_PUB_VERY_HI_TEMP3_ALM,		"GC_PUB_VERY_HI_TEMP3_ALM",		},
		//{GC_PUB_VERY_HI_TEMP4_ALM,		"GC_PUB_VERY_HI_TEMP4_ALM",		},
		//{GC_PUB_VERY_HI_TEMP5_ALM,		"GC_PUB_VERY_HI_TEMP5_ALM",		},
		//{GC_PUB_VERY_HI_TEMP6_ALM,		"GC_PUB_VERY_HI_TEMP6_ALM",		},
		//{GC_PUB_VERY_HI_TEMP7_ALM,		"GC_PUB_VERY_HI_TEMP7_ALM",		},
		//{GC_PUB_VERY_HI_TEMP1_PARAM,		"GC_PUB_VERY_HI_TEMP1_PARAM",		},
		//{GC_PUB_VERY_HI_TEMP2_PARAM,		"GC_PUB_VERY_HI_TEMP2_PARAM",		},
		//{GC_PUB_VERY_HI_TEMP3_PARAM,		"GC_PUB_VERY_HI_TEMP3_PARAM",		},
		//{GC_PUB_VERY_HI_TEMP4_PARAM,		"GC_PUB_VERY_HI_TEMP4_PARAM",		},
		//{GC_PUB_VERY_HI_TEMP5_PARAM,		"GC_PUB_VERY_HI_TEMP5_PARAM",		},
		//{GC_PUB_VERY_HI_TEMP6_PARAM,		"GC_PUB_VERY_HI_TEMP6_PARAM",		},
		//{GC_PUB_VERY_HI_TEMP7_PARAM,		"GC_PUB_VERY_HI_TEMP7_PARAM",		},
		{GC_PUB_TEMP1_NOT_CONNECT_ALM,		"GC_PUB_TEMP1_NOT_CONNECT_ALM",		},
		{GC_PUB_TEMP2_NOT_CONNECT_ALM,		"GC_PUB_TEMP2_NOT_CONNECT_ALM",		},
		{GC_PUB_TEMP3_NOT_CONNECT_ALM,		"GC_PUB_TEMP3_NOT_CONNECT_ALM",		},
		{GC_PUB_TEMP4_NOT_CONNECT_ALM,		"GC_PUB_TEMP4_NOT_CONNECT_ALM",		},
		{GC_PUB_TEMP5_NOT_CONNECT_ALM,		"GC_PUB_TEMP5_NOT_CONNECT_ALM",		},
		{GC_PUB_TEMP6_NOT_CONNECT_ALM,		"GC_PUB_TEMP6_NOT_CONNECT_ALM",		},
		{GC_PUB_TEMP7_NOT_CONNECT_ALM,		"GC_PUB_TEMP7_NOT_CONNECT_ALM",		},
		{GC_PUB_TEMP1_FAULT_ALM,		"GC_PUB_TEMP1_FAULT_ALM",		},
		{GC_PUB_TEMP2_FAULT_ALM,		"GC_PUB_TEMP2_FAULT_ALM",		},
		{GC_PUB_TEMP3_FAULT_ALM,		"GC_PUB_TEMP3_FAULT_ALM",		},
		{GC_PUB_TEMP4_FAULT_ALM,		"GC_PUB_TEMP4_FAULT_ALM",		},
		{GC_PUB_TEMP5_FAULT_ALM,		"GC_PUB_TEMP5_FAULT_ALM",		},
		{GC_PUB_TEMP6_FAULT_ALM,		"GC_PUB_TEMP6_FAULT_ALM",		},
		{GC_PUB_TEMP7_FAULT_ALM,		"GC_PUB_TEMP7_FAULT_ALM",		},
		//{GC_PUB_BATTTEMP1_HIGH_LIMIT,		"GC_PUB_BATTTEMP1_HIGH_LIMIT",		},
		//{GC_PUB_BATTTEMP2_HIGH_LIMIT,		"GC_PUB_BATTTEMP2_HIGH_LIMIT",		},
		//{GC_PUB_BATTTEMP3_HIGH_LIMIT,		"GC_PUB_BATTTEMP3_HIGH_LIMIT",		},
		//{GC_PUB_BATTTEMP4_HIGH_LIMIT,		"GC_PUB_BATTTEMP4_HIGH_LIMIT",		},
		//{GC_PUB_BATTTEMP5_HIGH_LIMIT,		"GC_PUB_BATTTEMP5_HIGH_LIMIT",		},
		//{GC_PUB_BATTTEMP6_HIGH_LIMIT,		"GC_PUB_BATTTEMP6_HIGH_LIMIT",		},
		//{GC_PUB_BATTTEMP7_HIGH_LIMIT,		"GC_PUB_BATTTEMP7_HIGH_LIMIT",		},
		{GC_PUB_LARUBATTTEMP8_HIGH_LIMIT,	"GC_PUB_LARUBATTTEMP8_HIGH_LIMIT",	},
		{GC_PUB_LARUBATTTEMP9_HIGH_LIMIT,	"GC_PUB_LARUBATTTEMP9_HIGH_LIMIT",	},
		{GC_PUB_LARUBATTTEMP10_HIGH_LIMIT,	"GC_PUB_LARUBATTTEMP10_HIGH_LIMIT",	},
		//{GC_PUB_BATTTEMP1_LOW_LIMIT,		"GC_PUB_BATTTEMP1_LOW_LIMIT",		},
		//{GC_PUB_BATTTEMP2_LOW_LIMIT,		"GC_PUB_BATTTEMP2_LOW_LIMIT",		},
		//{GC_PUB_BATTTEMP3_LOW_LIMIT,		"GC_PUB_BATTTEMP3_LOW_LIMIT",		},
		//{GC_PUB_BATTTEMP4_LOW_LIMIT,		"GC_PUB_BATTTEMP4_LOW_LIMIT",		},
		//{GC_PUB_BATTTEMP5_LOW_LIMIT,		"GC_PUB_BATTTEMP5_LOW_LIMIT",		},
		//{GC_PUB_BATTTEMP6_LOW_LIMIT,		"GC_PUB_BATTTEMP6_LOW_LIMIT",		},
		//{GC_PUB_BATTTEMP7_LOW_LIMIT,		"GC_PUB_BATTTEMP7_LOW_LIMIT",		},
		{GC_PUB_LARUBATTTEMP8_LOW_LIMIT,	"GC_PUB_LARUBATTTEMP8_LOW_LIMIT",	},
		{GC_PUB_LARUBATTTEMP9_LOW_LIMIT,	"GC_PUB_LARUBATTTEMP9_LOW_LIMIT",	},
		{GC_PUB_LARUBATTTEMP10_LOW_LIMIT,	"GC_PUB_LARUBATTTEMP10_LOW_LIMIT",	},
		{GC_PUB_NO_FOR_COMPEN,			"GC_PUB_NO_FOR_COMPEN",			},
		{GC_PUB_NO_FOR_BTRM,			"GC_PUB_NO_FOR_BTRM",			},
		{GC_PUB_UNDER_VOLT_ALM,			"GC_PUB_UNDER_VOLT_ALM",		},
		{GC_PUB_OVER_VOLT_ALM,			"GC_PUB_OVER_VOLT_ALM",			},
		{GC_PUB_HIGH_VOLT_ALM,			"GC_PUB_HIGH_VOLT_ALM",			},
		{GC_PUB_HIGH_VOLT_ALM_24V,		"GC_PUB_HIGH_VOLT_ALM_24V",		},
		{GC_PUB_UNDER_VOLT_ALM_24V,		"GC_PUB_UNDER_VOLT_ALM_24V",		},
		{GC_PUB_OVER_VOLT_ALM_24V,		"GC_PUB_OVER_VOLT_ALM_24V",		},
		{GC_PUB_VERY_UNDER_VOLT_ALM_24V,	"GC_PUB_VERY_UNDER_VOLT_ALM_24V",	},
		{GC_PUB_VERY_UNDER_VOLT_ALM,		"GC_PUB_VERY_UNDER_VOLT_ALM",		},
		{GC_PUB_ESTOP_ESHUTDOWN,		"GC_PUB_ESTOP_ESHUTDOWN",		},
		{GC_PUB_SYS_ALM_LIGHT_CTL,		"GC_PUB_SYS_ALM_LIGHT_CTL",		},
		{GC_PUB_FIXED_RELAY_ENB,		"GC_PUB_FIXED_RELAY_ENB",		},		
		{GC_PUB_RECT_ALLCURRENT,		"GC_PUB_RECT_ALLCURRENT",		},
		{GC_PUB_STATUS_CHANGED,			"GC_PUB_STATUS_CHANGED",		},
		{GC_PUB_ESTOP_ESHUTDOWN_ALARM,	"GC_PUB_ESTOP_ESHUTDOWN_ALARM",		},
		{GC_PUB_CFG_SLAVE_NUM,			"GC_PUB_CFG_SLAVE_NUM",		},
		{GC_PUB_CFG_SLAVE_LOST,			"GC_PUB_CFG_SLAVE_LOST",		},
		{GC_PUB_CAN_FAILURE,			"GC_PUB_CAN_FAILURE",		},
		{GC_PUB_AMBINE_TEMPERATURE,		"GC_PUB_AMBINE_TEMPERATURE",		},
		{GC_PUB_FAILSAFE,			"GC_PUB_FAILSAFE",		},
		{GC_PUB_FAILSAFE_NA,			"GC_PUB_FAILSAFE_NA",		},

//*#ifdef   */GC_SUPPORT_HIGHTEMP_DISCONNECT
//		{GC_PUB_TEMP_DELTA		,"GC_PUB_TEMP_DELTA",},            
		{GC_PUB_BATT_MANU_TEST_ALARM	,"GC_PUB_BATT_MANU_TEST_ALARM",},            
		{GC_PUB_AMB_TEMP_HIGH		,"GC_PUB_AMB_TEMP_HIGH",},            
		{GC_PUB_AMB_TEMP_LOW		,"GC_PUB_AMB_TEMP_LOW",},            
		{GC_PUB_AMB_TEMP_FAULT		,"GC_PUB_AMB_TEMP_FAULT",},  
		{GC_PUB_BATT_TEMP_HIGH		,"GC_PUB_BATT_TEMP_HIGH",},            
		{GC_PUB_BATT_TEMP_LOW		,"GC_PUB_BATT_TEMP_LOW",},            
		{GC_PUB_BATT_TEMP_FAULT		,"GC_PUB_BATT_TEMP_FAULT",},
		{GC_PUB_MULTI_RECT_FAULT	,"GC_PUB_MULTI_RECT_FAULT",},    
		{GC_PUB_OVER_VOLTAGE		,"GC_PUB_OVER_VOLTAGE",},            
		{GC_PUB_AC_A_HIGH		,"GC_PUB_AC_A_HIGH",},            
		{GC_PUB_AC_B_HIGH		,"GC_PUB_AC_B_HIGH",},            
		{GC_PUB_AC_C_HIGH		,"GC_PUB_AC_C_HIGH",},            
		{GC_PUB_AC_A_LOW		,"GC_PUB_AC_A_LOW",},    
		{GC_PUB_AC_B_LOW		,"GC_PUB_AC_B_LOW",},    
		{GC_PUB_AC_C_LOW		,"GC_PUB_AC_C_LOW",},    
		{GC_PUB_BATT_DISCHARGE		,"GC_PUB_BATT_DISCHARGE",},            
		{GC_PUB_BATT_FUSE1_FAULT	,"GC_PUB_BATT_FUSE1_FAULT",},    
		{GC_PUB_BATT_FUSE2_FAULT	,"GC_PUB_BATT_FUSE2_FAULT",},    
		{GC_PUB_BATT_FUSE3_FAULT	,"GC_PUB_BATT_FUSE3_FAULT",},    
		{GC_PUB_BATT_FUSE4_FAULT	,"GC_PUB_BATT_FUSE4_FAULT",},    
		{GC_PUB_LOAD_FUSE1_FAULT	,"GC_PUB_LOAD_FUSE1_FAULT",},    
		{GC_PUB_LOAD_FUSE2_FAULT	,"GC_PUB_LOAD_FUSE2_FAULT",},    
		{GC_PUB_LOAD_FUSE3_FAULT	,"GC_PUB_LOAD_FUSE3_FAULT",},    
		{GC_PUB_LOAD_FUSE4_FAULT	,"GC_PUB_LOAD_FUSE4_FAULT",},    
		{GC_PUB_LOAD_FUSE5_FAULT	,"GC_PUB_LOAD_FUSE5_FAULT",},    
		{GC_PUB_LOAD_FUSE6_FAULT	,"GC_PUB_LOAD_FUSE6_FAULT",},    
		{GC_PUB_LOAD_FUSE7_FAULT	,"GC_PUB_LOAD_FUSE7_FAULT",},    
		{GC_PUB_LOAD_FUSE8_FAULT	,"GC_PUB_LOAD_FUSE8_FAULT",},    
		{GC_PUB_LOAD_FUSE9_FAULT	,"GC_PUB_LOAD_FUSE9_FAULT",},    
		{GC_PUB_LOAD_FUSE10_FAULT	,"GC_PUB_LOAD_FUSE10_FAULT",}, 
//		{GC_PUB_SYSTEM_TYPE,		"GC_PUB_SYSTEM_TYPE",},



//#endif   GC_SUPPORT_HIGHTEMP_DISCONNECT

		/* Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		{GC_PUB_SYS_DI1,			"GC_PUB_SYS_DI1",},
		{GC_PUB_SYS_DI2,			"GC_PUB_SYS_DI2",},
		{GC_PUB_SYS_DI3,			"GC_PUB_SYS_DI3",},
		{GC_PUB_SYS_DI4,			"GC_PUB_SYS_DI4",},
		{GC_PUB_SYS_DI5,			"GC_PUB_SYS_DI5",},
		{GC_PUB_SYS_DI6,			"GC_PUB_SYS_DI6",},
		{GC_PUB_SYS_DI7,			"GC_PUB_SYS_DI7",},
		{GC_PUB_SYS_DI8,			"GC_PUB_SYS_DI8",},
		/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		{GC_PUB_SYS_DI_STATE_1,			"GC_PUB_SYS_DI_STATE_1",},
		{GC_PUB_SYS_DI_STATE_2,			"GC_PUB_SYS_DI_STATE_2",},
		{GC_PUB_SYS_DI_STATE_3,			"GC_PUB_SYS_DI_STATE_3",},
		{GC_PUB_SYS_DI_STATE_4,			"GC_PUB_SYS_DI_STATE_4",},
		{GC_PUB_SYS_DI_STATE_5,			"GC_PUB_SYS_DI_STATE_5",},
		{GC_PUB_SYS_DI_STATE_6,			"GC_PUB_SYS_DI_STATE_6",},
		{GC_PUB_SYS_DI_STATE_7,			"GC_PUB_SYS_DI_STATE_7",},
		{GC_PUB_SYS_DI_STATE_8,			"GC_PUB_SYS_DI_STATE_8",},
		{GC_PUB_SYS_DI_STATE_9,			"GC_PUB_SYS_DI_STATE_9",},
		{GC_PUB_SYS_DI_STATE_10,			"GC_PUB_SYS_DI_STATE_10",},
		{GC_PUB_SYS_DI_STATE_11,			"GC_PUB_SYS_DI_STATE_11",},
		{GC_PUB_SYS_DI_STATE_12,			"GC_PUB_SYS_DI_STATE_12",},


#ifdef GC_SUPPORT_RELAY_TEST
		{GC_PUB_RELAY_TEST,			"GC_PUB_RELAY_TEST",		},
		{GC_PUB_RELAY_TEST_TIME,		"GC_PUB_RELAY_TEST_TIME",	},
		{GC_PUB_RELAY_TESTING,			"GC_PUB_RELAY_TESTING",		},
#endif		
		{GC_PUB_MPPT_RUNNING_MODE,		"GC_PUB_MPPT_RUNNING_MODE",	},
		{GC_PUB_MPPT_RUNNING_WAY,		"GC_PUB_MPPT_RUNNING_WAY",	},
		{GC_PUB_CONVERTER_ONLY_MODE,		"GC_PUB_CONVERTER_ONLY_MODE",	},
		{GC_PUB_SYSTEM_LOAD_NO_CONVERTER,	"GC_PUB_SYSTEM_LOAD_NO_CONVERTER",	},
		//{GC_PUB_SYSTEM_VOLTAGE_LEVEL,		"GC_PUB_SYSTEM_VOLTAGE_LEVEL",	},
		{GC_PUB_SYSTEM_POWER_INPUT,		"GC_PUB_SYSTEM_POWER_INPUT",	},
		{GC_PUB_TOTAL_RATED,			"GC_PUB_TOTAL_RATED",	},
		{GC_PUB_SYS_VOLT_LEVEL,			"GC_PUB_SYS_VOLT_LEVEL",},
		{GC_PUB_CT_VOLT_LEVEL,			"GC_PUB_CT_VOLT_LEVEL",},
		{GC_PUB_FIAMM_BATTERY,			"GC_PUB_FIAMM_BATTERY",},

		{RT_PUB_QTY_SELF,			"RT_PUB_QTY_SELF",			},
		{RT_PUB_QTY_SLAVE1,			"RT_PUB_QTY_SLAVE1",			},
		{RT_PUB_QTY_SLAVE2,			"RT_PUB_QTY_SLAVE2",			},
		{RT_PUB_QTY_SLAVE3,			"RT_PUB_QTY_SLAVE3",			},
		{RT_PUB_USED_CAP,			"RT_PUB_USED_CAP",			},
		{RT_PUB_MAX_USED_CAP,			"RT_PUB_MAX_USED_CAP",			},
		{RT_PUB_MIN_USED_CAP,			"RT_PUB_MIN_USED_CAP",			},
		{RT_PUB_RATED_CURR,			"RT_PUB_RATED_CURR",			},
		{RT_PUB_RATED_VOLT,			"RT_PUB_RATED_VOLT",			},
		{RT_PUB_TOTAL_CURR,			"RT_PUB_TOTAL_CURR",			},
		{RT_PUB_AVE_VOLT,			"RT_PUB_AVE_VOLT",			},
		{RT_PUB_DC_ON_OFF_CTL_SELF,		"RT_PUB_DC_ON_OFF_CTL_SELF",		},
		{RT_PUB_DC_ON_OFF_CTL_SLAVE1,		"RT_PUB_DC_ON_OFF_CTL_SLAVE1",		},
		{RT_PUB_DC_ON_OFF_CTL_SLAVE2,		"RT_PUB_DC_ON_OFF_CTL_SLAVE2",		},
		{RT_PUB_DC_ON_OFF_CTL_SLAVE3,		"RT_PUB_DC_ON_OFF_CTL_SLAVE3",		},
		{RT_PUB_AC_ON_OFF_CTL_SELF,		"RT_PUB_AC_ON_OFF_CTL_SELF",		},
		{RT_PUB_AC_ON_OFF_CTL_SLAVE1,		"RT_PUB_AC_ON_OFF_CTL_SLAVE1",		},
		{RT_PUB_AC_ON_OFF_CTL_SLAVE2,		"RT_PUB_AC_ON_OFF_CTL_SLAVE2",		},
		{RT_PUB_AC_ON_OFF_CTL_SLAVE3,		"RT_PUB_AC_ON_OFF_CTL_SLAVE3",		},
		{RT_PUB_CURR_LMT_CTL_SELF,		"RT_PUB_CURR_LMT_CTL_SELF",		},
		{RT_PUB_CURR_LMT_CTL_SLAVE1,		"RT_PUB_CURR_LMT_CTL_SLAVE1",		},
		{RT_PUB_CURR_LMT_CTL_SLAVE2,		"RT_PUB_CURR_LMT_CTL_SLAVE2",		},
		{RT_PUB_CURR_LMT_CTL_SLAVE3,		"RT_PUB_CURR_LMT_CTL_SLAVE3",		},
		{RT_PUB_COMM_INTERRUPT_SLAVE1,		"RT_PUB_COMM_INTERRUPT_SLAVE1",		},
		{RT_PUB_COMM_INTERRUPT_SLAVE2,		"RT_PUB_COMM_INTERRUPT_SLAVE2",		},
		{RT_PUB_COMM_INTERRUPT_SLAVE3,		"RT_PUB_COMM_INTERRUPT_SLAVE3",		},
		{RT_PUB_MAINS_FAILURE_SLAVE1,		"RT_PUB_MAINS_FAILURE_SLAVE1",		},
		{RT_PUB_MAINS_FAILURE_SLAVE2,		"RT_PUB_MAINS_FAILURE_SLAVE2",		},
		{RT_PUB_MAINS_FAILURE_SLAVE3,		"RT_PUB_MAINS_FAILURE_SLAVE3",		},
		{RT_PUB_MAINS_FAILURE_MASTER,		"RT_PUB_MAINS_FAILURE_MASTER",		},
		{RT_PUB_SLAVE1_EXIST,			"RT_PUB_SLAVE1_EXIST",			},
		{RT_PUB_SLAVE2_EXIST,			"RT_PUB_SLAVE2_EXIST",			},
		{RT_PUB_SLAVE3_EXIST,			"RT_PUB_SLAVE3_EXIST",			},
		{RT_PUB_DC_VOLT_CTL,			"RT_PUB_DC_VOLT_CTL",			},
		{RT_PUB_DC_VOLT_CTL_SLAVE1,		"RT_PUB_DC_VOLT_CTL_SLAVE1",		},
		{RT_PUB_DC_VOLT_CTL_SLAVE2,		"RT_PUB_DC_VOLT_CTL_SLAVE2",		},
		{RT_PUB_DC_VOLT_CTL_SLAVE3,		"RT_PUB_DC_VOLT_CTL_SLAVE3",		},
		{RT_PUB_DC_VOLT_CTL_24V,		"RT_PUB_DC_VOLT_CTL_24V",		},
		{RT_PUB_COMM_QTY,			"RT_PUB_COMM_QTY",			},
		{RT_PUB_VAILD_QTY,			"RT_PUB_VAILD_QTY",			},
		{RT_PUB_ESTOP_CTL,			"RT_PUB_ESTOP_CTL",			},
		{RT_PUB_ESTOP_CTL_SLAVE1,		"RT_PUB_ESTOP_CTL_SLAVE1",		},
		{RT_PUB_ESTOP_CTL_SLAVE2,		"RT_PUB_ESTOP_CTL_SLAVE2",		},
		{RT_PUB_ESTOP_CTL_SLAVE3,		"RT_PUB_ESTOP_CTL_SLAVE3",		},
		{RT_PUB_REDU_ENB,			"RT_PUB_REDU_ENB",			},
		//{RT_PUB_REDU_PCL_ENB,			"RT_PUB_REDU_PCL_ENB",			},
		//{RT_PUB_MIN_REDU,			"RT_PUB_MIN_REDU",			},
		//{RT_PUB_MAX_REDU,			"RT_PUB_MAX_REDU",			},
		{RT_PUB_AC_OFF_DLY,			"RT_PUB_AC_OFF_DLY",			},
		{RT_PUB_REDU_CLC_PERD,			"RT_PUB_REDU_CLC_PERD",			},
		{RT_PUB_RECT_DRYING_TIME,		"RT_PUB_RECT_DRYING_TIME",		},
		//{RT_PUB_ACTIVE_CLK,			"RT_PUB_ACTIVE_CLK",			},
		{RT_PUB_BEST_EFFICIENCY,		"RT_PUB_BEST_EFFICIENCY",		},
		{RT_PUB_ES_ACTIVE_SET,			"RT_PUB_ES_ACTIVE_SET",			},
		{RT_PUB_ES_FLUCT_RANGE,			"RT_PUB_ES_FLUCT_RANGE",		},
		{RT_PUB_ES_SYSTEM_POINT,		"RT_PUB_ES_SYSTEM_POINT",		},
		{RT_PUB_HVSD_ENABLE,			"RT_PUB_HVSD_ENABLE",			},
		{RT_PUB_HVSD_VOLT_DIFF,			"RT_PUB_HVSD_VOLT_DIFF",		},
		{RT_PUB_RECT_INFO_CHANGED,		"RT_PUB_RECT_INFO_CHANGED",		},
		{RT_PUB_RECT_DERATING_RUN,		"RT_PUB_RECT_DERATING_RUN",		},

		{RT_PUB_REDU_OSCILL,			"RT_PUB_REDU_OSCILL",			},
		//{RT_PUB_AC_FAIL,			"RT_PUB_AC_FAIL",			},
		{RT_PUB_MULTI_RECT_FAULT,		"RT_PUB_MULTI_RECT_FAULT",		},
		{RT_PUB_POWER_TYPE,			"RT_PUB_POWER_TYPE",			},
		{RT_PUB_CFG_RECT_NUM,			"RT_PUB_CFG_RECT_NUM",			},
		{RT_PUB_CFG_RECT_LOST,			"RT_PUB_CFG_RECT_LOST",			},
		{RT_PUB_SEQ_START_INTERVAL,		"RT_PUB_SEQ_START_INTERVAL",		},
		{RT_PUB_OV_RESTART_INTERVAL,		"RT_PUB_OV_RESTART_INTERVAL",		},
		{RT_PUB_OV_RESTART_ENB,			"RT_PUB_OV_RESTART_ENB",		},
		{RT_PUB_WALKIN_TIME,			"RT_PUB_WALKIN_TIME",			},
		{RT_PUB_WALKIN_ENB,			"RT_PUB_WALKIN_ENB",			},
		{RT_PUB_AC_OV_RESTART_ENB,		"RT_PUB_AC_OV_RESTART_ENB",		},
		{RT_PUB_ALL_INTERRUPT_SELF,		"RT_PUB_ALL_INTERRUPT_SELF",		},
		{RT_PUB_ALL_INTERRUPT_SLAVE1,		"RT_PUB_ALL_INTERRUPT_SLAVE1",		},
		{RT_PUB_ALL_INTERRUPT_SLAVE2,		"RT_PUB_ALL_INTERRUPT_SLAVE2",		},
		{RT_PUB_ALL_INTERRUPT_SLAVE3,		"RT_PUB_ALL_INTERRUPT_SLAVE3",		},

		{RT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR,		"RT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR",		},

/* Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		{RT_PUB_DISL_PWR_LMT_ENB,	"RT_PUB_DISL_PWR_LMT_ENB",	},	
		{RT_PUB_DISL_DI_INPUT,		"RT_PUB_DISL_DI_INPUT",		},
		{RT_PUB_DISL_PWR_LMT_POINT,	"RT_PUB_DISL_PWR_LMT_POINT",	},
/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */

		{RT_PUB_MAX_CURR_LMT_POINT,		"RT_PUB_MAX_CURR_LMT_POINT",		},
		{RT_PUB_MAX_CURR_LMT_ENABLE,		"RT_PUB_MAX_CURR_LMT_ENABLE",		},		
		{RT_PUB_DEFAULT_MAX_CURR_LMT_POINT,	"RT_PUB_DEFAULT_MAX_CURR_LMT_POINT",	},
		{RT_PUB_DEFAULT_POINT_INTERAL,		"RT_PUB_DEFAULT_POINT_INTERAL",		},

		{RT_PUB_MAX_CURR_LMT_SAMPLE,		"RT_PUB_MAX_CURR_LMT_SAMPLE",		},
		{RT_PUB_MAX_CURR_LMT_VALUE,		"RT_PUB_MAX_CURR_LMT_VALUE",		},
		{RT_PUB_MIN_CURR_LMT_SAMPLE,		"RT_PUB_MIN_CURR_LMT_SAMPLE",		},
		{RT_PUB_SYS_CURR_LIMIT_RECT_NUM,	"RT_PUB_SYS_CURR_LIMIT_RECT_NUM",	},
		{RT_PUB_TOTAL_RATED_CURRENT_T,		"RT_PUB_TOTAL_RATED_CURRENT_T",		},
		{RT_PUB_TOTAL_NUMBER_INCLUDE_SLAVE,	"RT_PUB_TOTAL_NUMBER_INCLUDE_SLAVE",	},



		{CT_PUB_TOTAL_CURRENT,			"CT_PUB_TOTAL_CURRENT",			},
		{CT_PUB_AVERAGE_VOLT,			"CT_PUB_AVERAGE_VOLT",			},
		{CT_PUB_DC_ON_OFF_CTL,			"CT_PUB_DC_ON_OFF_CTL",			},
		{CT_PUB_ESTOP_CTL,			"CT_PUB_ESTOP_CTL",			},	
		{CT_PUB_QTY_SELF,			"CT_PUB_QTY_SELF",			},
		{CT_PUB_QTY_AC_ON_COMM,			"CT_PUB_QTY_AC_ON_COMM",		},
		{CT_PUB_QTY_COMM,			"CT_PUB_QTY_COMM",			},
		{CT_PUB_CFG_RECT_NUM,			"CT_PUB_CFG_RECT_NUM",			},
		{CT_PUB_CFG_RECT_LOST,			"CT_PUB_CFG_RECT_LOST",			},
		{CT_PUB_CFG_DEFAULT_CURR,		"CT_PUB_CFG_DEFAULT_CURR",		},
		{CT_PUB_CFG_DEFAULT_VOLT,		"CT_PUB_CFG_DEFAULT_VOLT",		},
		{CT_PUB_CFG_DEFAULT_VOLT_24V,		"CT_PUB_CFG_DEFAULT_VOLT_24V",		},
		{CT_PUB_CFG_DEFAULT_HVSD,		"CT_PUB_CFG_DEFAULT_HVSD",		},
		{CT_PUB_CFG_DEFAULT_HVSD_24V,		"CT_PUB_CFG_DEFAULT_HVSD_24V",		},
		{CT_PUB_CTL_CURR,			"CT_PUB_CTL_CURR",			},
		{CT_PUB_CTL_CURR_24V,			"CT_PUB_CTL_CURR_24V",			},
		{CT_PUB_CTL_VOLT,			"CT_PUB_CTL_VOLT",			},
		{CT_PUB_CTL_VOLT_24V,			"CT_PUB_CTL_VOLT_24V",			},
		{CT_PUB_CTL_HVSD,			"CT_PUB_CTL_HVSD",			},
		{CT_PUB_CFG_MULTI_FAIL_ALARM,		"CT_PUB_CFG_MULTI_FAIL_ALARM",		},
		{CT_PUB_ALL_INTERRUPT_SELF,		"CT_PUB_ALL_INTERRUPT_SELF",		},
		{CT_PUB_MAX_CURR_LMT_POINT,		"CT_PUB_MAX_CURR_LMT_POINT",		},
		{CT_PUB_MAX_CURR_LMT_ENABLE,		"CT_PUB_MAX_CURR_LMT_ENABLE",		},
		{CT_PUB_DEFAULT_MAX_CURR_LMT_POINT,	"CT_PUB_DEFAULT_MAX_CURR_LMT_POINT",	},
		{CT_PUB_DEFAULT_POINT_INTERAL,		"CT_PUB_DEFAULT_POINT_INTERAL",		},
		{CT_PUB_MAX_CURR_LMT_SAMPLE,		"CT_PUB_MAX_CURR_LMT_SAMPLE",		},
		{CT_PUB_MAX_CURR_LMT_VALUE,		"CT_PUB_MAX_CURR_LMT_VALUE",		},
		{CT_PUB_MIN_CURR_LMT_SAMPLE,		"CT_PUB_MIN_CURR_LMT_SAMPLE",		},
		{CT_PUB_SYS_CURR_LIMIT_RECT_NUM,	"CT_PUB_SYS_CURR_LIMIT_RECT_NUM",	},
		{CT_PUB_RATED_CURRENT,			"CT_PUB_RATED_CURRENT",			},
		{CT_PUB_OUTPUT_RATED_VOLT,		"CT_PUB_OUTPUT_RATED_VOLT",		},
		{CT_PUB_TOTAL_RATED_CURR,		"CT_PUB_TOTAL_RATED_CURR",		},

		{CT_PUB_USED_CAP,			"CT_PUB_USED_CAP",	},
		{CT_PUB_MAX_USED_CAP,			"CT_PUB_MAX_USED_CAP",			},
		{CT_PUB_MIN_USED_CAP,			"CT_PUB_MIN_USED_CAP",		},
		{CT_PUB_CONVERTER_TYPE,			"CT_PUB_CONVERTER_TYPE",		},
		{CT_PUB_INTPUT_RATED_VOLT,			"CT_PUB_INTPUT_RATED_VOLT",		},

		
		{MT_PUB_TOTAL_CURR,			"MT_PUB_TOTAL_CURR",			},
		{MT_PUB_AVE_VOLT,			"MT_PUB_AVE_VOLT",			},
		{MT_PUB_USED_CAP,			"MT_PUB_USED_CAP",			},
		{MT_PUB_RATED_VOLT,			"MT_PUB_RATED_VOLT",			},
		{MT_PUB_RATED_CURR,			"MT_PUB_RATED_CURR",			},
		{MT_PUB_MAX_USED_CAP,			"MT_PUB_MAX_USED_CAP",			},
		{MT_PUB_MIN_USED_CAP,			"MT_PUB_MIN_USED_CAP",			},
		{MT_PUB_COMM_QTY,			"MT_PUB_COMM_QTY",			},
		{MT_PUB_VAILD_QTY,			"MT_PUB_VAILD_QTY",			},
		{MT_PUB_QTY_SELF,			"MT_PUB_QTY_SELF",			},
		{MT_PUB_MULTI_RECT_FAULT,		"MT_PUB_MULTI_RECT_FAULT",		},
		{MT_PUB_CFG_RECT_LOST,			"MT_PUB_CFG_RECT_LOST",			},
		{MT_PUB_CFG_RECT_NUM,	"MT_PUB_CFG_RECT_NUM",	},
		{MT_PUB_RECT_INFO_CHANGED,		"MT_PUB_RECT_INFO_CHANGED",		},
		{MT_PUB_CURR_LMT_CTL,			"MT_PUB_CURR_LMT_CTL",			},
		{MT_PUB_DC_VOLT_CTL,			"MT_PUB_DC_VOLT_CTL",			},
		{MT_PUB_DC_ON_OFF_CTL,			"MT_PUB_DC_ON_OFF_CTL",			},
		{MT_PUB_ALL_INTERRUPT,			"MT_PUB_ALL_INTERRUPT",				},		
		{MT_PUB_TOTAL_INPUT_CURRENT,		"MT_PUB_TOTAL_INPUT_CURRENT",				},		
		{MT_PUB_TOTAL_INPUT_POWER,		"MT_PUB_TOTAL_INPUT_POWER",				},		
		{MT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR,	"MT_PUB_WORK_ON_RECT_TOTAL_RATED_CURR",				},		
		{MT_PUB_ESTOP_CTL,			"MT_PUB_ESTOP_CTL",				},		
		{MT_PUB_OUTPUT_POWER,			"MT_PUB_OUTPUT_POWER",				},		
			


		{BT_PUB_ABNOR_BATT_CURR_SET,		"BT_PUB_ABNOR_BATT_CURR_SET",		},
		{BT_PUB_TEMP_COMPEN_SET,		"BT_PUB_TEMP_COMPEN_SET",		},
		//add 3 parameters by Jimmy 2011-12-23 for ESNA CR
		{BT_PUB_ESNA_COMP_MODE_ENABLE,		"BT_PUB_ESNA_COMP_MODE_ENABLE"		},
		{BT_PUB_ESNA_COMP_HI_VOLT,		"BT_PUB_ESNA_COMP_HI_VOLT"		},
		{BT_PUB_ESNA_COMP_LOW_VOLT,		"BT_PUB_ESNA_COMP_LOW_VOLT"		},
		{BT_PUB_ESNA_COMP_HI_VOLT_24V,		"BT_PUB_ESNA_COMP_HI_VOLT_24V"		},
		{BT_PUB_ESNA_COMP_LOW_VOLT_24V,		"BT_PUB_ESNA_COMP_LOW_VOLT_24V"		},

		{BT_PUB_PRE_BC_ENB,			"BT_PUB_PRE_BC_ENB",			},
		{BT_PUB_AUTO_BC_ENB,			"BT_PUB_AUTO_BC_ENB",			},
		{BT_PUB_CYC_BC_ENB,			"BT_PUB_CYC_BC_ENB",			},
		{BT_PUB_MAX_BC_TIME,			"BT_PUB_MAX_BC_TIME",			},
		{BT_PUB_FC_TO_BC_CURR,			"BT_PUB_FC_TO_BC_CURR",			},
		{BT_PUB_FC_TO_BC_CURR_LIBATT,		"BT_PUB_FC_TO_BC_CURR_LIBATT",		},
		{BT_PUB_ABCL_IS_ACTIVE,			"BT_PUB_ABCL_IS_ACTIVE",		},
		{BT_PUB_FC_TO_BC_CAP,			"BT_PUB_FC_TO_BC_CAP",			},
		{BT_PUB_BC_TO_FC_CURR,			"BT_PUB_BC_TO_FC_CURR",			},
		{BT_PUB_BC_TO_FC_CURR_LIBATT,		"BT_PUB_BC_TO_FC_CURR_LIBATT",		},
		{BT_PUB_BC_TO_FC_DLY,			"BT_PUB_BC_TO_FC_DLY",			},
		{BT_PUB_CYC_BC_INTERVAL,		"BT_PUB_CYC_BC_INTERVAL",		},
		{BT_PUB_CYC_BC_DURATION,		"BT_PUB_CYC_BC_DURATION",		},
		{BT_PUB_CYC_START_TIME,			"BT_PUB_CYC_START_TIME",		},
		{BT_PUB_CYC_LASTPLAN_TIME,		"BT_PUB_CYC_LASTPLAN_TIME",		},
		{BT_PUB_CYC_VALID_ENABLE,		"BT_PUB_CYC_VALID_ENABLE",		},
		{BT_PUB_BAD_BATT_SET,			"BT_PUB_BAD_BATT_SET",			},
		{BT_PUB_DISCH_IMB_SET,			"BT_PUB_DISCH_IMB_SET",			},
		{BT_PUB_CON_CURR_TEST_ENB,		"BT_PUB_CON_CURR_TEST_ENB",		},
		//{BT_PUB_PLAN_TEST_ENB,		"BT_PUB_PLAN_TEST_ENB",			},
		{BT_PUB_AC_FAIL_TEST_ENB,		"BT_PUB_AC_FAIL_TEST_ENB",		},
		{BT_PUB_SHORT_TEST_ENB,			"BT_PUB_SHORT_TEST_ENB",		},
		{BT_PUB_MAX_DIFF,			"BT_PUB_MAX_DIFF",			},
		{BT_PUB_NO_DISCH_TIME,			"BT_PUB_NO_DISCH_TIME",			},
		{BT_PUB_SHORT_TEST_DURATION,		"BT_PUB_SHORT_TEST_DURATION",		},
		{BT_PUB_TEST_END_CAP,			"BT_PUB_TEST_END_CAP",			},
		{BT_PUB_TEST_DURATION,			"BT_PUB_TEST_DURATION",			},
		{BT_PUB_CONST_TEST_CURR,		"BT_PUB_CONST_TEST_CURR",		},
		{BT_PUB_NOM_VOLT, 			"BT_PUB_NOM_VOLT",			},
		{BT_PUB_BST_VOLT,			"BT_PUB_BST_VOLT",			},
		{BT_PUB_TEST_VOLT,			"BT_PUB_TEST_VOLT",			},
		{BT_PUB_TEST_END_VOLT,			"BT_PUB_TEST_END_VOLT",			},
		{BT_PUB_TEST_CTL,			"BT_PUB_TEST_CTL",			},
		{BT_PUB_BOOST_CTL,			"BT_PUB_BOOST_CTL",			},
		{BT_PUB_RST_BATT_CAP,			"BT_PUB_RST_BATT_CAP",			},
		{BT_PUB_TOTAL_CURR,			"BT_PUB_TOTAL_CURR",			},
		{BT_PUB_TEST_THRESHOLD,			"BT_PUB_TEST_THRESHOLD",		},
		{BT_PUB_AVERAGE_VOLT,			"BT_PUB_AVERAGE_VOLT",			},
		{BT_PUB_SHALLOW_TIME,			"BT_PUB_SHALLOW_TIME",			},
		{BT_PUB_SHALLOW_TIMES,			"BT_PUB_SHALLOW_TIMES",			},
		{BT_PUB_MID_TIME,			"BT_PUB_MID_TIME",			},
		{BT_PUB_MID_TIMES,			"BT_PUB_MID_TIMES",			},
		{BT_PUB_DEEP_TIME,			"BT_PUB_DEEP_TIME",			},
		{BT_PUB_DEEP_TIMES,			"BT_PUB_DEEP_TIMES",			},
		{BT_PUB_TYPE_NO,			"BT_PUB_TYPE_NO",			},
		{BT_PUB_RATED_CAP,			"BT_PUB_RATED_CAP",			},
		{BT_PUB_PREDICT_DISCH_TIME,		"BT_PUB_PREDICT_DISCH_TIME",		},	
		{BT_PUB_OVER_CURR,			"BT_PUB_OVER_CURR",			},
		{BT_PUB_CURR_LMT,			"BT_PUB_CURR_LMT",			},
		{BT_PUB_EQ_COMPEN_FACTOR,		"BT_PUB_EQ_COMPEN_FACTOR",		},
		{BT_PUB_CURR_LMT_LIBATT,		"BT_PUB_CURR_LMT_LIBATT",		},
		{BT_PUB_CAP_COEF,			"BT_PUB_CAP_COEF",			},	
		{BT_PUB_DISCH_TIME01,			"BT_PUB_DISCH_TIME01",			},
		{BT_PUB_DISCH_TIME02,			"BT_PUB_DISCH_TIME02",			},
		{BT_PUB_DISCH_TIME03,			"BT_PUB_DISCH_TIME03",			},
		{BT_PUB_DISCH_TIME04,			"BT_PUB_DISCH_TIME04",			},
		{BT_PUB_DISCH_TIME05,			"BT_PUB_DISCH_TIME05",			},
		{BT_PUB_DISCH_TIME06,			"BT_PUB_DISCH_TIME06",			},
		{BT_PUB_DISCH_TIME07,			"BT_PUB_DISCH_TIME07",			},
		{BT_PUB_DISCH_TIME08,			"BT_PUB_DISCH_TIME08",			},	
		{BT_PUB_DISCH_TIME09,			"BT_PUB_DISCH_TIME09",			},
		{BT_PUB_DISCH_TIME10,			"BT_PUB_DISCH_TIME10",			},	
		{BT_PUB_CURR_LMT_SET,			"BT_PUB_CURR_LMT_SET",			},
		{BT_PUB_COMPEN_FACTOR,			"BT_PUB_COMPEN_FACTOR",			},
		{BT_PUB_NOM_TEMP,			"BT_PUB_NOM_TEMP",			},
		{BT_PUB_BM_STATE,			"BT_PUB_BM_STATE",			},	
		{BT_PUB_PLAN_TEST_NUM,			"BT_PUB_PLAN_TEST_NUM",			},	
		{BT_PUB_PLAN_TEST_TIME1,		"BT_PUB_PLAN_TEST_TIME1",		},
		{BT_PUB_PLAN_TEST_TIME2,		"BT_PUB_PLAN_TEST_TIME2",		},	
		{BT_PUB_PLAN_TEST_TIME3,		"BT_PUB_PLAN_TEST_TIME3",		},	
		{BT_PUB_PLAN_TEST_TIME4,		"BT_PUB_PLAN_TEST_TIME4",		},	
		{BT_PUB_PLAN_TEST_TIME5,		"BT_PUB_PLAN_TEST_TIME5",		},
		{BT_PUB_PLAN_TEST_TIME6,		"BT_PUB_PLAN_TEST_TIME6",		},
		{BT_PUB_PLAN_TEST_TIME7,		"BT_PUB_PLAN_TEST_TIME7",		},
		{BT_PUB_PLAN_TEST_TIME8,		"BT_PUB_PLAN_TEST_TIME8",		},
		{BT_PUB_PLAN_TEST_TIME9,		"BT_PUB_PLAN_TEST_TIME9",		},
		{BT_PUB_PLAN_TEST_TIME10,		"BT_PUB_PLAN_TEST_TIME10",		},
		{BT_PUB_PLAN_TEST_TIME11,		"BT_PUB_PLAN_TEST_TIME11",		},
		{BT_PUB_PLAN_TEST_TIME12,		"BT_PUB_PLAN_TEST_TIME12",		},
		{BT_PUB_LOWEST_CAPA_FOR_BT,		"BT_PUB_LOWEST_CAPA_FOR_BT",	},

		{BT_PUB_EXP_CURR_LMT,			"BT_PUB_EXP_CURR_LMT",			},
		{BT_PUB_BATT_DISCH_SET,			"BT_PUB_BATT_DISCH_SET",		},
		{BT_PUB_NOM_VOLT24,				"BT_PUB_NOM_VOLT24",			},
		{BT_PUB_BST_VOLT24,				"BT_PUB_BST_VOLT24",			},
		{BT_PUB_TEST_VOLT24,			"BT_PUB_TEST_VOLT24",			},
		{BT_PUB_TEST_END_VOLT24,		"BT_PUB_TEST_END_VOLT24",		},
		{BT_PUB_HI_TEMP_VOLT24,			"BT_PUB_HI_TEMP_VOLT24",		},
		{BT_PUB_HI_TEMP_VOLT,			"BT_PUB_HI_TEMP_VOLT",			},
		//{BT_PUB_RECONNECT_TEMP,			"BT_PUB_RECONNECT_TEMP",		},
		{BT_PUB_HI_TEMP_ACTION,			"BT_PUB_HI_TEMP_ACTION",		},
		{BT_PUB_CURR_LMT_ENB,			"BT_PUB_CURR_LMT_ENB",			},
		//{DC_PUB_LOAD_CURR,			"DC_PUB_LOAD_CURR",			},
		//{DC_PUB_CURR,					"DC_PUB_CURR",				},
		{SPLIT_PUB_MODE,				"SPLIT_PUB_MODE",				},
		{SPLIT_PUB_SLV_CURR_LMT,		"SPLIT_PUB_SLV_CURR_LMT",		},
		{SPLIT_PUB_DELTA_VOLT,			"SPLIT_PUB_DELTA_VOLT",			},
		{SPLIT_PUB_PROP_COEF,			"SPLIT_PUB_PROP_COEF",			},
		{SPLIT_PUB_INTERGRAL_TIME,		"SPLIT_PUB_INTERGRAL_TIME",		},
		
		// /*Energy Saving Parameter*/
		{GC_PUB_LC_ENB,				"GC_PUB_LC_ENB",			},
		{GC_PUB_MPCL_ENB,			"GC_PUB_MPCL_ENB",			},
		{GC_PUB_ES_STEP,			"GC_PUB_ES_STEP",			},	
		{GC_PUB_ES_PEAK_POWER,			"GC_PUB_ES_PEAK_POWER",			},
		{GC_PUB_ES_NOW_RATE,			"GC_PUB_ES_NOW_RATE",			},
		{GC_PUB_ES_THRESHOLD,			"GC_PUB_ES_THRESHOLD",			},
		{GC_PUB_ES_BATT_DISCHG_ENB,		"GC_PUB_ES_BATT_DISCHG_ENB",		},	
		{GC_PUB_ES_DSL_CTL_ENB,			"GC_PUB_ES_DSL_CTL_ENB",		},
		{GC_PUB_LVD_QUANTITY,			"GC_PUB_LVD_QUANTITY",			},


		{SMDU_PUB_NUM,				"SMDU_PUB_NUM",				},
		{SMDU_PUB_COMM_INTERRUPT1,		"SMDU_PUB_COMM_INTERRUPT1",		},
		{SMDU_PUB_COMM_INTERRUPT2,		"SMDU_PUB_COMM_INTERRUPT2",		},
		{SMDU_PUB_COMM_INTERRUPT3,		"SMDU_PUB_COMM_INTERRUPT3",		},
		{SMDU_PUB_COMM_INTERRUPT4,		"SMDU_PUB_COMM_INTERRUPT4",		},
		{SMDU_PUB_COMM_INTERRUPT5,		"SMDU_PUB_COMM_INTERRUPT5",		},
		{SMDU_PUB_COMM_INTERRUPT6,		"SMDU_PUB_COMM_INTERRUPT6",		},
		{SMDU_PUB_COMM_INTERRUPT7,		"SMDU_PUB_COMM_INTERRUPT7",		},
		{SMDU_PUB_COMM_INTERRUPT8,		"SMDU_PUB_COMM_INTERRUPT8",		},
		{MS_PUB_DOOR_CLOSE,			"MS_PUB_DOOR_CLOSE",			},
		{MS_PUB_SWITCH_OPEN,			"MS_PUB_SWITCH_OPEN",			},
		{MS_PUB_AUTOMATIC,			"MS_PUB_AUTOMATIC",			},
		{MS_PUB_RELAY_TRIP,			"MS_PUB_RELAY_TRIP",			},
		{MS_PUB_CLOSE_CTL,			"MS_PUB_CLOSE_CTL",			},
		{MS_PUB_OPEN_CTL,			"MS_PUB_OPEN_CTL",			},
		{MS_PUB_RESET_CTL,			"MS_PUB_RESET_CTL",			},
		{MS_PUB_BLOCK_CONDITION,		"MS_PUB_BLOCK_CONDITION",		},
		
		/*LVD Group signal*/
		{LVD_NEED_AC_FAIL,			"LVD_NEED_AC_FAIL",			},
		{LVD_HTD_POINT,				"LVD_HTD_POINT",			},
		{LVD_HTD_RECON_POINT,			"LVD_HTD_RECON_POINT",			},
		//{LVD_HTD_TEMP_SENSOR,			"LVD_HTD_TEMP_SENSOR",			},
		{LVD_ESNA_LVD_SYNCH,			"LVD_ESNA_LVD_SYNCH",			},
		{LVD3_ENABLE,				"LVD3_ENABLE",				},
		{LVD3_RELAY_NO,				"LVD3_RELAY_NO",			},

		/*Energy Saving Alarm*/
		{BT_PUB_PROH_BATT_CH_SET,		"BT_PUB_PROH_BATT_CH_SET",		},
		{BT_PUB_BATT_TEST_FAIL_ALM,		"BT_PUB_BATT_TEST_FAIL_ALM",		},

		/*Mppt parameter*/
		{BT_PUB_NOM_VOLT_MPPT,			"BT_PUB_NOM_VOLT_MPPT",		},
		{BT_PUB_NOM_VOLT_RECT,			"BT_PUB_NOM_VOLT_RECT",		},
		{BT_PUB_BST_VOLT_RECT,			"BT_PUB_BST_VOLT_RECT",		},
		{BT_PUB_BST_VOLT_MPPT,			"BT_PUB_BST_VOLT_MPPT",		},
		{BT_PUB_LOWER_CAPACITY,			"BT_PUB_LOWER_CAPACITY",	},
		{BT_PUB_MAX_CHARGE_CURRENT,		"BT_PUB_MAX_CHARGE_CURRENT",	},


		{GC_PUB_OVER_PEAK_POWER_SET,		"GC_PUB_OVER_PEAK_POWER_SET",		},	

		{AC_PUB_TOTAL_POWER,			"AC_PUB_TOTAL_POWER",			},
		{DSL_PUB_TEST_ENB,			"DSL_PUB_TEST_ENB",			},
		{DSL_PUB_CTL_INH,			"DSL_PUB_CTL_INH",			},
		{DSL_PUB_TEST_DELAY,			"DSL_PUB_TEST_DELAY",			},
		{DSL_PUB_STATUS,			"DSL_PUB_STATUS",			},
		{DSL_PUB_START_CTL,			"DSL_PUB_START_CTL",			},
		{DSL_PUB_STOP_CTL,			"DSL_PUB_STOP_CTL",			},
		{DSL_PUB_LAST_TEST_DATE,		"DSL_PUB_LAST_TEST_DATE",		},
		{DSL_PUB_TEST_STA,			"DSL_PUB_TEST_STA",			},
		{DSL_PUB_TEST_RESULT,			"DSL_PUB_TEST_RESULT",			},
		{DSL_PUB_TEST_START_CTL,		"DSL_PUB_TEST_START_CTL",		},
		{DSL_PUB_TEST_NUM,			"DSL_PUB_TEST_NUM",			},

		{DSL_PUB_TEST_TIME1,			"DSL_PUB_TEST_TIME1",			},
		{DSL_PUB_TEST_TIME2,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME3,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME4,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME5,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME6,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME7,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME8,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME9,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME10,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME11,			"DSL_PUB_TEST_TIME2",			},
		{DSL_PUB_TEST_TIME12,			"DSL_PUB_TEST_TIME2",			},

		/*LargeDU signals*/

		{LDU_PUB_EXISTENCE,			"LDU_PUB_EXISTENCE",			},
		{LDU_LVD1_PRI_ENB,			"LDU_LVD1_PRI_ENB",			},
		{LDU_LVD2_PRI_ENB,			"LDU_LVD2_PRI_ENB",			},
		{LDU_LVD1_PRI_MODE,			"LDU_LVD1_PRI_MODE",			},
		{LDU_LVD2_PRI_MODE,			"LDU_LVD2_PRI_MODE",			},
		{LDU_LVD1_PRI_VOLT,			"LDU_LVD1_PRI_VOLT",			},
		{LDU_LVD2_PRI_VOLT,			"LDU_LVD2_PRI_VOLT",			},
		{LDU_LVD1_PRI_VOLT24,			"LDU_LVD1_PRI_VOLT24",			},
		{LDU_LVD2_PRI_VOLT24,			"LDU_LVD2_PRI_VOLT24",			},
		{LDU_LVD1_PRI_DEPEND,			"LDU_LVD1_PRI_DEPEND",			},
		{LDU_LVD2_PRI_DEPEND,			"LDU_LVD2_PRI_DEPEND",			},
		{LDU_LVD1_PRI_RECON_VOLT24,		"LDU_LVD1_PRI_RECON_VOLT24",		},
		{LDU_LVD2_PRI_RECON_VOLT24,		"LDU_LVD2_PRI_RECON_VOLT24",		},
		{LDU_LVD1_PRI_RECON_VOLT,		"LDU_LVD1_PRI_RECON_VOLT",		},
		{LDU_LVD2_PRI_RECON_VOLT,		"LDU_LVD2_PRI_RECON_VOLT",		},
		{LDU_LVD1_GROUP_PRI_DELAY,		"LDU_LVD1_GROUP_PRI_DELAY",		},
		{LDU_LVD2_GROUP_PRI_DELAY,		"LDU_LVD2_GROUP_PRI_DELAY",		},
		{LDU_LVD1_GROUP_PRI_TIME,		"LDU_LVD1_GROUP_PRI_TIME",		},
		{LDU_LVD2_GROUP_PRI_TIME,		"LDU_LVD2_GROUP_PRI_TIME",		},

		{LDU_DC_PUB_VOLT,			"LDU_DC_PUB_VOLT",			},
		{LDU_DC_PUB_TOTAL_LOAD,			"LDU_DC_PUB_TOTAL_LOAD",		},
		{LDU_DC_PUB_TOTAL_DCDCURRENT,		"LDU_DC_PUB_TOTAL_DCDCURRENT",		},
		{LDU_DC_PUB_COMM_STATE1,		"LDU_DC_PUB_COMM_STATE1",		},
		{LDU_DC_PUB_COMM_STATE2,		"LDU_DC_PUB_COMM_STATE2",		},
		{LDU_DC_PUB_COMM_STATE3,		"LDU_DC_PUB_COMM_STATE3",		},
		{LDU_DC_PUB_COMM_STATE4,		"LDU_DC_PUB_COMM_STATE4",		},
		{LDU_DC_PUB_COMM_STATE5,		"LDU_DC_PUB_COMM_STATE5",		},
		{LDU_DC_PUB_COMM_STATE6,		"LDU_DC_PUB_COMM_STATE6",		},
		{LDU_DC_PUB_COMM_STATE7,		"LDU_DC_PUB_COMM_STATE7",		},
		{LDU_DC_PUB_COMM_STATE8,		"LDU_DC_PUB_COMM_STATE8",		},
		{LDU_DC_PUB_COMM_STATE9,		"LDU_DC_PUB_COMM_STATE9",		},
		{LDU_DC_PUB_COMM_STATE10,		"LDU_DC_PUB_COMM_STATE10",		},
		
		{LDU_PUB_NUM,				"LDU_PUB_NUM",				},
		{LDU_PUB_COMM_INTERRUPT1,		"LDU_PUB_COMM_INTERRUPT1",		},
		{LDU_PUB_COMM_INTERRUPT2,		"LDU_PUB_COMM_INTERRUPT2",		},
		{LDU_PUB_COMM_INTERRUPT3,		"LDU_PUB_COMM_INTERRUPT3",		},
		{LDU_PUB_COMM_INTERRUPT4,		"LDU_PUB_COMM_INTERRUPT4",		},
		{LDU_PUB_COMM_INTERRUPT5,		"LDU_PUB_COMM_INTERRUPT5",		},
		{LDU_PUB_COMM_INTERRUPT6,		"LDU_PUB_COMM_INTERRUPT6",		},
		{LDU_PUB_COMM_INTERRUPT7,		"LDU_PUB_COMM_INTERRUPT7",		},
		{LDU_PUB_COMM_INTERRUPT8,		"LDU_PUB_COMM_INTERRUPT8",		},
		{LDU_PUB_COMM_INTERRUPT9,		"LDU_PUB_COMM_INTERRUPT9",		},
		{LDU_PUB_COMM_INTERRUPT10,		"LDU_PUB_COMM_INTERRUPT10",		},

		{LDU1_PUB_TEMPERATURE_ALM1,		"LDU1_PUB_TEMPERATURE_ALM1",		},
		{LDU1_PUB_TEMPERATURE_ALM2,		"LDU1_PUB_TEMPERATURE_ALM2",		},
		{LDU1_PUB_TEMPERATURE_ALM3,		"LDU1_PUB_TEMPERATURE_ALM3",		},
		{LDU2_PUB_TEMPERATURE_ALM1,		"LDU2_PUB_TEMPERATURE_ALM1",		},
		{LDU2_PUB_TEMPERATURE_ALM2,		"LDU2_PUB_TEMPERATURE_ALM2",		},
		{LDU2_PUB_TEMPERATURE_ALM3,		"LDU2_PUB_TEMPERATURE_ALM3",		},
		{LDU3_PUB_TEMPERATURE_ALM1,		"LDU3_PUB_TEMPERATURE_ALM1",		},
		{LDU3_PUB_TEMPERATURE_ALM2,		"LDU3_PUB_TEMPERATURE_ALM2",		},
		{LDU3_PUB_TEMPERATURE_ALM3,		"LDU3_PUB_TEMPERATURE_ALM3",		},
		{LDU4_PUB_TEMPERATURE_ALM1,		"LDU4_PUB_TEMPERATURE_ALM1",		},
		{LDU4_PUB_TEMPERATURE_ALM2,		"LDU4_PUB_TEMPERATURE_ALM2",		},
		{LDU4_PUB_TEMPERATURE_ALM3,		"LDU4_PUB_TEMPERATURE_ALM3",		},
		{LDU5_PUB_TEMPERATURE_ALM1,		"LDU5_PUB_TEMPERATURE_ALM1",		},
		{LDU5_PUB_TEMPERATURE_ALM2,		"LDU5_PUB_TEMPERATURE_ALM2",		},
		{LDU5_PUB_TEMPERATURE_ALM3,		"LDU5_PUB_TEMPERATURE_ALM3",		},
		{LDU6_PUB_TEMPERATURE_ALM1,		"LDU6_PUB_TEMPERATURE_ALM1",		},
		{LDU6_PUB_TEMPERATURE_ALM2,		"LDU6_PUB_TEMPERATURE_ALM2",		},
		{LDU6_PUB_TEMPERATURE_ALM3,		"LDU6_PUB_TEMPERATURE_ALM3",		},
		{LDU7_PUB_TEMPERATURE_ALM1,		"LDU7_PUB_TEMPERATURE_ALM1",		},
		{LDU7_PUB_TEMPERATURE_ALM2,		"LDU7_PUB_TEMPERATURE_ALM2",		},
		{LDU7_PUB_TEMPERATURE_ALM3,		"LDU7_PUB_TEMPERATURE_ALM3",		},
		{LDU8_PUB_TEMPERATURE_ALM1,		"LDU8_PUB_TEMPERATURE_ALM1",		},
		{LDU8_PUB_TEMPERATURE_ALM2,		"LDU8_PUB_TEMPERATURE_ALM2",		},
		{LDU8_PUB_TEMPERATURE_ALM3,		"LDU8_PUB_TEMPERATURE_ALM3",		},
		{LDU9_PUB_TEMPERATURE_ALM1,		"LDU9_PUB_TEMPERATURE_ALM1",		},
		{LDU9_PUB_TEMPERATURE_ALM2,		"LDU9_PUB_TEMPERATURE_ALM2",		},
		{LDU9_PUB_TEMPERATURE_ALM3,		"LDU9_PUB_TEMPERATURE_ALM3",		},
		{LDU10_PUB_TEMPERATURE_ALM1,		"LDU10_PUB_TEMPERATURE_ALM1",		},
		{LDU10_PUB_TEMPERATURE_ALM2,		"LDU10_PUB_TEMPERATURE_ALM2",		},
		{LDU10_PUB_TEMPERATURE_ALM3,		"LDU10_PUB_TEMPERATURE_ALM3",		},

		{GC_PUB_LduTEMP_NUM,			"GC_PUB_LduTEMP_NUM",			},

		{HYBRID_PUB_RUNNING_MODE,		"HYBRID_PUB_RUNNING_MODE",		},
		{HYBRID_PUB_DICHARGE_END_CAP,		"HYBRID_PUB_DICHARGE_END_CAP",		},
		{HYBRID_PUB_RUN_DG,			"HYBRID_PUB_RUN_DG",			},
		{HYBRID_PUB_DG1_RUNNING,		"HYBRID_PUB_DG1_RUNNING",		},
		{HYBRID_PUB_DG2_RUNNING,		"HYBRID_PUB_DG2_RUNNING",		},
		{HYBRID_PUB_RUN_ON_OVERTEMP,		"HYBRID_PUB_RUN_ON_OVERTEMP",		},
		{HYBRID_PUB_RUN_ON_OVERTEMP_ENABLE,	"HYBRID_PUB_RUN_ON_OVERTEMP_ENABLE",	},
		{HYBRID_PUB_RUN_ON_OVERTEMP_DURATION,	"HYBRID_PUB_RUN_ON_OVERTEMP_DURATION",	},
		{HYBRID_PUB_SELECTED_DI,		"HYBRID_PUB_SELECTED_DI",		},
		{HYBRID_PUB_HIGH_LOAD_ALARM,		"HYBRID_PUB_HIGH_LOAD_ALARM",		},
		{HYBRID_PUB_HIGH_LOAD_ALARM_SETTING,	"HYBRID_PUB_HIGH_LOAD_ALARM_SETTING",	},
		{HYBRID_PUB_FIXED_DISCHARGE_DURATION,	"HYBRID_PUB_FIXED_DISCHARGE_DURATION",	},
		{HYBRID_PUB_FIXED_START_TIME,		"HYBRID_PUB_FIXED_START_TIME",		},
		{HYBRID_PUB_EQUALISING_START_TIME,	"HYBRID_PUB_EQUALISING_START_TIME",	},
		{HYBRID_PUB_DG1_FAILURE,		"HYBRID_PUB_DG1_FAILURE",		},
		{HYBRID_PUB_DG2_FAILURE,		"HYBRID_PUB_DG2_FAILURE",		},
		{HYBRID_PUB_DG_FAILURE_INTERVAL,	"HYBRID_PUB_DG_FAILURE_INTERVAL",	},
		{HYBRID_PUB_GRID_ON,			"HYBRID_PUB_GRID_ON",	},

		{GC_PUB_POWERSPLIT_MODE,		"GC_PUB_POWERSPLIT_MODE"},
		{GC_PUB_BATTGROUP_BATTTEMP,		"GC_PUB_BATTGROUP_BATTTEMP"},
		{GC_PUB_BATTGROUP_BTRM,		"GC_PUB_BATTGROUP_BTRM"},
		{GC_PUB_BATTGROUP_BATTTEMP_HIGH_LIMIT,	"GC_PUB_BATTGROUP_BATTTEMP_HIGH_LIMIT"},
		{GC_PUB_BATTGROUP_BATTTEMP_LOW_LIMIT,	"GC_PUB_BATTGROUP_BATTTEMP_LOW_LIMIT"},
		{GC_PUB_BATTGROUP_BATTTEMP_HIGH_ALARM,	"GC_PUB_BATTGROUP_BATTTEMP_HIGH_ALARM"},
		{GC_PUB_SYSTEM_ENVTEMP_HIGH_ALARM,	"GC_PUB_SYSTEM_ENVTEMP_HIGH_ALARM"},
		{GC_PUB_BATTGROUP_BATTTEMP_VERYHIGH_ALARM,	"GC_PUB_BATTGROUP_BATTTEMP_VERYHIGH_ALARM"},

		//added by Jimmy 2011/11/25
		{GC_PUB_BATTGROUP_BTRM_HIGH_ALARM,	"GC_PUB_BATTGROUP_BTRM_HIGH_ALARM"},
		{GC_PUB_BATTGROUP_BTRM_VERYHIGH_ALARM,	"GC_PUB_BATTGROUP_BTRM_VERYHIGH_ALARM"},
		{GC_PUB_SMTEMP_ACTUAL_NUM,"GC_PUB_SMTEMP_ACTUAL_NUM"},
		//{GC_PUB_CFG_SMTEMP_LOST,"GC_PUB_CFG_SMTEMP_LOST"},
		{GC_PUB_CFG_SMTEMP_NUM,	"GC_PUB_CFG_SMTEMP_NUM"},

		//added by Jimmy 2012/04/05
		{GC_PUB_DCEM_ACTUAL_NUM,		"GC_PUB_DCEM_ACTUAL_NUM"},	
		{GC_PUB_CFG_DCEM_NUM,			"GC_PUB_CFG_DCEM_NUM"},

		{GC_PUB_LI_BATT_ACTUAL_NUM,		"GC_PUB_LI_BATT_ACTUAL_NUM"},
		{GC_PUB_LI_BATT_LOST_STAT,		"GC_PUB_LI_BATT_LOST_STAT"},
		{GC_PUB_LI_BATT_LOST_CLEAR_STAT,	"GC_PUB_LI_BATT_LOST_CLEAR_STAT"},
		{GC_PUB_CFG_LI_BATT_NUM,		"GC_PUB_CFG_LI_BATT_NUM"},
		{GC_PUB_IS_LI_BATT_TYPE,		"GC_PUB_IS_LI_BATT_TYPE"},
		{GC_PUB_LIGROUP_TOTAL_CURR,		"GC_PUB_LIGROUP_TOTAL_CURR"},
		{GC_PUB_LIGROUP_AVG_VOLT,		"GC_PUB_LIGROUP_AVG_VOLT"},
		{GC_PUB_LIGROUP_AVG_TEMP,		"GC_PUB_LIGROUP_AVG_TEMP"},
		{GC_PUB_LIGROUP_INST_BATT_NUM,		"GC_PUB_LIGROUP_INST_BATT_NUM"},
		{GC_PUB_LIGROUP_DISCONN_BATT_NUM,	"GC_PUB_LIGROUP_DISCONN_BATT_NUM"},
		{GC_PUB_LIGROUP_NOREPLY_BATT_NUM,	"GC_PUB_LIGROUP_NOREPLY_BATT_NUM"},
		{GC_PUB_LIGROUP_INVENTORY_UPDATE_FLAG,	"GC_PUB_LIGROUP_INVENTORY_UPDATE_FLAG"},


		{GC_PUB_BATTGROUP_LI_AVGTEMP,		"GC_PUB_BATTGROUP_LI_AVGTEMP"},
		{GC_PUB_BATTGROUP_INST_BATT_NUM,	"GC_PUB_BATTGROUP_INST_BATT_NUM"},
		{GC_PUB_BATTGROUP_DISCONN_BATT_NUM,	"GC_PUB_BATTGROUP_DISCONN_BATT_NUM"},
		{GC_PUB_BATTGROUP_NOREPLY_BATT_NUM,	"GC_PUB_BATTGROUP_NOREPLY_BATT_NUM"},
		{GC_PUB_BATTGROUP_INVENTORY_UPDATE_FLAG,"GC_PUB_BATTGROUP_INVENTORY_UPDATE_FLAG"},
		{GC_PUB_BATT_ADJUSTVOLT_GAIN,		"GC_PUB_BATT_ADJUSTVOLT_GAIN"},
		{GC_EES_SYSTEM_MODE,			"GC_EES_SYSTEM_MODE"},
		{GC_RECT_CYCLE,					"GC_RECT_CYCLE"},
		{GC_PUB_BATT_PREDICT_EN,		"GC_PUB_BATT_PREDICT_EN"},
		{GC_PUB_BATT_PREDICT_DELAY,		"GC_PUB_BATT_PREDICT_DELAY"},
		{RT_PUB_HVSD_VOLT_DIFF_24V,		"RT_PUB_HVSD_VOLT_DIFF_24V"},
		{GC_PUB_SMDU_IB_MODE,			"GC_PUB_SMDU_IB_MODE"},
		{GC_PUB_NEED_CALC_BATT_CURR,	"GC_PUB_NEED_CALC_BATT_CURR"},
		{GC_PUB_TOTAL_LOAD_CURR,		"GC_PUB_TOTAL_LOAD_CURR"},
		{GC_PUB_TOTAL_MB_LOADS,			"GC_PUB_TOTAL_MB_LOADS"},
		{GC_PUB_TOTAL_SMDU_LOADS,		"GC_PUB_TOTAL_SMDU_LOADS"},
		{GC_PUB_TOTAL_SMDUP_LOADS,		"GC_PUB_TOTAL_SMDUP_LOADS"},
		{GC_PUB_TOTAL_EIB_LOADS,		"GC_PUB_TOTAL_EIB_LOADS"},
		{GC_PUB_LOAD_ENABLE,			"GC_PUB_LOAD_ENABLE"},		
		{GC_BCL_ALARM_POINT_VOLT_MODE,	"GC_BCL_ALARM_POINT_VOLT_MODE"},	
		{GC_BCL_NORMAL_POINT_VOLT_MODE,	"GC_BCL_NORMAL_POINT_VOLT_MODE"},
		{GC_BCL_ALARM_POINT_CURR_MODE,		"GC_BCL_ALARM_POINT_CURR_MODE"},
		{GC_BCL_NORMAL_POINT_CURR_MODE,		"GC_BCL_NORMAL_POINT_CURR_MODE"},

		{GC_BATT_CURR_UP_LIMIT,		"GC_BATT_CURR_UP_LIMIT"},
		{GC_BATT_CURR_STABLE_UP_COEFF,	"GC_BATT_CURR_STABLE_UP_COEFF"},
		{GC_BATT_CURR_STABLE_DN_COEFF,	"GC_BATT_CURR_STABLE_DN_COEFF"},
		{GC_VOLT_ADJUST_CHANGE_PACE_POINT_COEFF,"GC_VOLT_ADJUST_CHANGE_PACE_POINT_COEFF"},
		{GC_VOLT_ADJUST_SLOW_COEFF,		"GC_VOLT_ADJUST_SLOW_COEFF"},
		{GC_VOLT_ADJUST_FAST_COEFF,		"GC_VOLT_ADJUST_FAST_COEFF"},
		{GC_VOLT_ADJUST_MIN_STEP,		"GC_VOLT_ADJUST_MIN_STEP"},
		{GC_VOLT_ADJUST_MAX_STEP,		"GC_VOLT_ADJUST_MAX_STEP"},
		{GC_VOLT_ADJUST_CYCLE_NUM,		"GC_VOLT_ADJUST_CYCLE_NUM"},

		{GC_PUB_BMS_CHARGE_CURRENT,		"GC_PUB_BMS_CHARGE_CURRENT"},
		{GC_PUB_BMS_NUMBER,			"GC_PUB_BMS_NUMBER"},

		{GC_PUB_TMP_CMP_ENABLE_BY_SENSOR_FAIL,		"GC_PUB_TMP_CMP_ENABLE_BY_SENSOR_FAIL"},
		{GC_PUB_TMP_CMP_THRED_FUNC_ENABLE,		"GC_PUB_TMP_CMP_THRED_FUNC_ENABLE"},
		{GC_PUB_OFF_TEMP,							"GC_PUB_OFF_TEMP"},
		{GC_PUB_ON_TEMP,							"GC_PUB_ON_TEMP"},
		{GC_PUB_TOTAL_SMDUE_LOADS,					"GC_PUB_TOTAL_SMDUE_LOADS"},
		
		{MAX_SIG_NUM,					"",								},
	};

	pPriCfg->iSigNum = pPrimaryPriCfg->iSigNum;
	ASSERT(pPrimaryPriCfg->iSigNum);

	//TRACE("GEN_CTL Debug: before NEW(GC_SIG_INFO, pPriCfg->iSigNum) pPriCfg->iSigNum = %d \n", pPriCfg->iSigNum);
	pPriCfg->pSig = NEW(GC_SIG_INFO, pPriCfg->iSigNum);
	TRACE("GEN_CTL Debug: After NEW(GC_SIG_INFO, pPriCfg->iSigNum) pPriCfg->iSigNum = %d \n", pPriCfg->iSigNum);

	GC_ASSERT(pPriCfg->pSig,
		ERR_CTL_NO_MEMORY,
		"Failed to NEW pPriCfg->pSig! \n");

	j = 0;
	while(Sig[j].iIndex < MAX_SIG_NUM)
	{
		char	szStrOut1[256] = "";
		strcat(szStrOut1, Sig[j].szName);
		strcat(szStrOut1, " is not be found!\n");

		TRACE("\nj =%d Sig[j].iIndex = %d", j, Sig[j].iIndex);
		GC_ASSERT((Sig[j].iIndex <= pPriCfg->iSigNum),
			ERR_CTL_CFG_GROUP_SIG,
			"The Public signal number in gc_index_def.h is more than gen_ctl.cfg!\n");

		for(i = 0; i< pPrimaryPriCfg->iSigNum; i++)
		{
			if(GC_MATCHED == strcmp(Sig[j].szName, 
								(pPrimaryPriCfg->pSig + i)->pName))
			{
				(pPriCfg->pSig + Sig[j].iIndex)->iEquipId 
					= (pPrimaryPriCfg->pSig + i)->iEquipId;

				(pPriCfg->pSig + Sig[j].iIndex)->iSignalId 
					= (pPrimaryPriCfg->pSig + i)->iId;

				(pPriCfg->pSig + Sig[j].iIndex)->iSignalType 
					= (pPrimaryPriCfg->pSig + i)->iType;
				
				//DELETE((pPrimaryPriCfg->pSig + i)->pName);

				break;
			}
		}

		GC_ASSERT((i < pPrimaryPriCfg->iSigNum),
			ERR_CTL_CFG_GROUP_SIG,
			szStrOut1);
		j++;
	}

	for(i = 0; i< pPrimaryPriCfg->iSigNum; i++)
	{
		DELETE((pPrimaryPriCfg->pSig + i)->pName);
	}
	DELETE(pPrimaryPriCfg->pSig);
	return;
}

/*==========================================================================*
 * FUNCTION : ConvertCfgData
 * PURPOSE  : Convert data from pPrimaryPriCfg to pPriCfg, and destroy 
		      pPrimaryPriCfg
 * CALLS    : 	StuffRectSig, StuffRectPriSig, StuffBattFusePriSig
	            StuffBattPriSig, StuffEibSig, StuffLvdSig, StuffSig,
 * CALLED BY: GC_PriCfgInit
 * ARGUMENTS: PRIMARY_PRI_CFG  *pPrimaryPriCfg : 
 *            GC_PRI_CFG*   pPriCfg      : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:38
 *==========================================================================*/
static void ConvertCfgData(PRIMARY_PRI_CFG *pPrimaryPriCfg, GC_PRI_CFG* pPriCfg)
{
	StuffRectPriSig(pPrimaryPriCfg, pPriCfg);
	StuffS1RectPriSig(pPrimaryPriCfg, pPriCfg);
	StuffS2RectPriSig(pPrimaryPriCfg, pPriCfg);
	StuffS3RectPriSig(pPrimaryPriCfg, pPriCfg);
	StuffCTPriSig(pPrimaryPriCfg, pPriCfg);
	StuffBattPriSig(pPrimaryPriCfg, pPriCfg);
	StuffLvdPriSig(pPrimaryPriCfg, pPriCfg);
	StuffBattFusePriSig(pPrimaryPriCfg, pPriCfg);
	StuffEibPriSig(pPrimaryPriCfg, pPriCfg);
#ifdef GC_SUPPORT_MPPT
	StuffMpptPriSig(pPrimaryPriCfg, pPriCfg);
#endif	
#ifdef GC_SUPPORT_BMS
	StuffBMSPriSig(pPrimaryPriCfg, pPriCfg);
#endif	

	StuffSig(pPrimaryPriCfg, pPriCfg);

	return;
}


/*==========================================================================*
 * FUNCTION : GC_PriCfgDestroy
 * PURPOSE  : To destroy g_pGcDaTa->PriCfg
 * CALLS    : 
 * CALLED BY: ExitGC
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-19 16:08
 *==========================================================================*/
void	GC_PriCfgDestroy(void)
{
	DELETE(g_pGcData->PriCfg.pEibPriSig);
	DELETE(g_pGcData->PriCfg.pLvdPriSig);
	DELETE(g_pGcData->PriCfg.pRectPriSig);
	DELETE(g_pGcData->PriCfg.pS1RectPriSig);
	DELETE(g_pGcData->PriCfg.pS2RectPriSig);
	DELETE(g_pGcData->PriCfg.pS3RectPriSig);
	DELETE(g_pGcData->PriCfg.pBattPriSig);
	DELETE(g_pGcData->PriCfg.pBattFusePriSig);
	DELETE(g_pGcData->PriCfg.pSig);
	DELETE(g_pGcData->PriCfg.pBattParam);
	DELETE(g_pGcData->PriCfg.pEngySavOutput);
	DELETE(g_pGcData->PriCfg.pReduceConsumptionAct);
	DELETE(g_pGcData->PriCfg.pLmtMaxPowerAct);
	DELETE(g_pGcData->PriCfg.pEngySavMode);
	DELETE(g_pGcData->PriCfg.pEngySavSchedule);
	DELETE(g_pGcData->PriCfg.pPowerSplitInput);
	DELETE(g_pGcData->PriCfg.pEstopEshutdownInput);
	DELETE(g_pGcData->PriCfg.pLvdOnVeryHiTemp);
	DELETE(g_pGcData->PriCfg.pFixedRelayOutput);
	DELETE(g_pGcData->PriCfg.pCTPriSig);

#ifdef GC_SUPPORT_MPPT
	DELETE(g_pGcData->PriCfg.pMpptPriSig);	
#endif
	return;
}

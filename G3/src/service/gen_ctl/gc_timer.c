/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_timer.c
 *  CREATOR  : Frank Cao                DATE: 2004-10-08 10:32
 *  VERSION  : V1.00
 *  PURPOSE  : To provide a group of interface function for timer function.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_timer.h"
#include	"gen_ctl.h"
#include	"gc_index_def.h"
#include	"gc_sig_value.h"
#include	"gc_batt_mgmt.h"
#include	"gc_float_charge.h"
#include	"gc_run_info.h"

static void TimeoutCheck(time_t tNow);
static void MsTimersInit(void);
static void MsTimersDestroy(void);

/*==========================================================================*
 * FUNCTION : GC_GetStateOfSecTimer
 * PURPOSE  : To get the state of the timer
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int iTimerId :
 * RETURN   : The aSecTimer's State
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-08 10:55
 *==========================================================================*/
BYTE GC_GetStateOfSecTimer(int iTimerId)
{
	TRACE("iTimerId = %d\n", iTimerId);
	ASSERT((iTimerId < MAX_GC_SEC_TMR_NUM) 
		&& (iTimerId >= 0));
	return g_pGcData->SecTimer.aSecTimer[iTimerId].byState;
}


/*==========================================================================*
 * FUNCTION : GC_SuspendSecTimer
 * PURPOSE  : To suspend the timer
 * CALLS    : GC_SaveData
 * CALLED BY: 
 * ARGUMENTS: int iTimerId : 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-08 11:03
 *==========================================================================*/
void GC_SuspendSecTimer(int iTimerId)
{
	
	ASSERT((iTimerId < MAX_GC_SEC_TMR_NUM) 
			&& (iTimerId >= 0));

	if(SEC_TMR_SUSPENDED != g_pGcData->SecTimer.aSecTimer[iTimerId].byState)
	{
		g_pGcData->SecTimer.aSecTimer[iTimerId].byState = SEC_TMR_SUSPENDED;
		if(g_pGcData->SecTimer.abNeedSave[iTimerId])
		{
			GC_SaveData();
		}
	}
	return;
}



/*==========================================================================*
 * FUNCTION : GC_SetSecTimer
 * PURPOSE  : To set the timer, the state turn to SEC_TMR_TIME_IN
 * CALLS    : GC_SaveData
 * CALLED BY: 
 * ARGUMENTS: int                  iTimerId   : 
 *            int                  iInterval  : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-13 13:53
 *==========================================================================*/
void GC_SetSecTimer(int iTimerId, 
					int iInterval,
					GC_GET_TMR_INTERVAL_PROC pfnGetInterval,
					int iIdx)
{
	SIG_ENUM			enumTestState;
	DWORD				dwReduceTimes;

	GC_SINGLE_SEC_TIMER	*pTmr;
	ASSERT((iTimerId < MAX_GC_SEC_TMR_NUM) 
			&& (iTimerId >= 0));

	pTmr = &(g_pGcData->SecTimer.aSecTimer[iTimerId]);
	pTmr->tStartTime = g_pGcData->SecTimer.tTimeNow;
	pTmr->pfnGetTmrInterval = pfnGetInterval;
	pTmr->iIdx = iIdx;


	if(pfnGetInterval)
	{
		pTmr->iInterval = pfnGetInterval(iIdx);
	}
	else
	{
		pTmr->iInterval = iInterval;
	}

	
	enumTestState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_TEST_STATE);
	dwReduceTimes = GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_TEST_REDUCE_TIMES);
	if(enumTestState)	//test state
	{
		if(pTmr->iInterval > SECONDS_PER_HOUR)
		{
			pTmr->iInterval /= dwReduceTimes;
			//TRACE("GC_TIMER: iTimerId = %d,  iInterval = %d.\n", iTimerId, iInterval);
		}
	}

	pTmr->byState = SEC_TMR_TIME_IN;
	if(g_pGcData->SecTimer.abNeedSave[iTimerId])
	{
		GC_SaveData();
	}

	return;
}



/*==========================================================================*
 * FUNCTION : TimeoutCheck
 * PURPOSE  : To scan all of timer, if someone is timeout, modify the state
 * CALLS    : GC_SaveData
 * CALLED BY: GC_MaintainSecTimers
 * ARGUMENTS: time_t  tNow : 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-09 16:41
 *==========================================================================*/
static void TimeoutCheck(time_t tNow)
{
	int i;
	BOOL bSave = FALSE;
	for(i = 0; i < MAX_GC_SEC_TMR_NUM; i++)
	{
		GC_SINGLE_SEC_TIMER	*pTmr = &(g_pGcData->SecTimer.aSecTimer[i]);

		if(SEC_TMR_TIME_IN == pTmr->byState)
		{
			time_t tDelta = tNow - pTmr->tStartTime;

			if(ABS(tDelta) > pTmr->iInterval)
			{
				pTmr->byState = SEC_TMR_TIME_OUT;
				if(g_pGcData->SecTimer.abNeedSave[i])
				{
					bSave = TRUE;
				}
			}

#ifndef _GC_TEST
			if(pTmr->pfnGetTmrInterval)
			{
				SIG_ENUM			enumTestState;
				DWORD				dwReduceTimes;

				pTmr->iInterval = (pTmr->pfnGetTmrInterval)(pTmr->iIdx);
				
				enumTestState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_TEST_STATE);
				dwReduceTimes = GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_TEST_REDUCE_TIMES);

				if(enumTestState)	//test state
				{
					if(pTmr->iInterval > SECONDS_PER_HOUR)
					{
						pTmr->iInterval /= dwReduceTimes;

						//TRACE("GC_TIMER: iTimerId = %d,  iInterval = %d,dwReduceTimes=%d.\n", i, pTmr->iInterval,dwReduceTimes);
					}
				}
			}
#endif
		}
	}

	if(bSave)
	{
		GC_SaveData();	
	}
	return;
}



/*==========================================================================*
 * FUNCTION : GC_MaintainSecTimers
 * PURPOSE  : 1. This function shall be called periodically. the function compare
              Now Time, Last time with Max Delta, if (NowTime - LastTime) is 
			  greater than MaxDelta, that means the clock has been modified and
			  need to proof StartTime
			  2. Scan all of SecTimers, if some one timeout, modify the state to
			  TIME_OUT.
			  3.return Noe Time

 * CALLS    : time, GC_SaveData, 
 * CALLED BY: RefreshData
 * ARGUMENTS: time_t  lLastTime : 
 *            long    lMaxDelta : 
 * RETURN   : time_t : return Noe Time
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-09 16:45
 *==========================================================================*/
time_t GC_MaintainSecTimers(time_t tLastTime, 
							long lMaxDelta)
{
	long lDeltaTm;
	time_t tNow = time(NULL);
	if(tLastTime || lMaxDelta)
	{
		lDeltaTm = tNow - tLastTime;

		if(ABS(lDeltaTm) > lMaxDelta) //means the clock has been modified.
		{
			int i;
			for(i = 0; i < MAX_GC_SEC_TMR_NUM; i++)
			{
				g_pGcData->SecTimer.aSecTimer[i].tStartTime += lDeltaTm;
			}
			GC_SaveData();

			GC_BattHisTimeProof(lDeltaTm);
		}

		TimeoutCheck(tNow);
	}

	return tNow;
}


/*==========================================================================*
 * FUNCTION : CreateSecTimers
 * PURPOSE  : Load SecTimers Data, and set initial state of SECTimers
 * CALLS    : GC_LoadData
 * CALLED BY: GC_TimersInit
 * RETURN   : GC_SEC_TIMER_GROUP* : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-13 13:39
 *==========================================================================*/
//static GC_SEC_TIMER_GROUP* CreateSecTimers()
//{
//	GC_SEC_TIMER_GROUP* secTimer;
//
//	secTimer = NEW(GC_SEC_TIMER_GROUP, 1);
//	if(secTimer)
//	{
//		if(!(GC_LoadData()))
//		{
//			int i;
//			for (i = 0; i < MAX_GC_SEC_TMR_NUM; i++)
//			{
//				secTimer.aSecTimer[i].byState = SEC_TMR_SUSPENDED;
//			}
//		}
//	}
//	return secTimer;
//}



/*==========================================================================*
 * FUNCTION : MsTimersInit
 * PURPOSE  : To set MS Timers used in General Controller. the timers are 
              supplied by run_timer.c
 * CALLS    : Timer_Set
 * CALLED BY: GC_TimersInit
 * ARGUMENTS: 
 * RETURN   :
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-10 14:36
 *==========================================================================*/
static void MsTimersInit()
{
	int	iRst;

	iRst = Timer_Set(g_pGcData->hThreadSelf,
			ID_TM_REFRESH_DATA, 
			GC_INTERVAL_REFRESH_DATA * MS_PER_SEC,
			NULL,
			0);
	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_TIMER, 
			"Timer_Set ID_TM_REFRESH_DATA error!\n");

	//﮵�صĵ�ع���ʱ��Ҫ��ȴ�ͳ��ض̣��������޸���ʱ��
	if(!IsWorkIn_LiBattMode())
	{
		iRst = Timer_Set(g_pGcData->hThreadSelf,
			ID_TM_BATT_MGMT, 
			GC_INTERVAL_BATT_MGMT * MS_PER_SEC,
			NULL,
			0);
	}
	else
	{
		//added by Jimmy 2012.08.27,﮵��ģʽ����Ϊ6��1��
		iRst = Timer_Set(g_pGcData->hThreadSelf,
			ID_TM_BATT_MGMT, 
			GC_INTERVAL_LI_BATT_MGMT * MS_PER_SEC,
			NULL,
			0);
	}
	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_TIMER,
			"Timer_Set ID_TM_BATT_MGMT error!\n");

	iRst = Timer_Set(g_pGcData->hThreadSelf,
			ID_TM_ENERGY_SAVING, 
			g_pGcData->PriCfg.iEnergySavingPeriod, 
			NULL, 
			0);
	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_TIMER,
			"Timer_Set ID_TM_ENERGY_SAVING error!\n");

	iRst = Timer_Set(g_pGcData->hThreadSelf,
			ID_TM_DSL_TEST, 
			GC_INTERVAL_DSL_TEST * MS_PER_SEC,
			NULL,
			0);
	GC_ASSERT((iRst == ERR_OK),
			ERR_CTL_TIMER,
			"Timer_Set ID_TM_DSL_TEST error!\n");
	

	iRst = Timer_Set(g_pGcData->hThreadSelf,
			ID_TM_RECT_REDUND, 
			GC_INTERVAL_RECT_REDUND * MS_PER_SEC,
			NULL,
			0);

	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_TIMER,
			"Timer_Set ID_TM_RECT_REDUND error!\n");
	
	iRst = Timer_Set(g_pGcData->hThreadSelf,
			ID_TM_MAIN_SWITCH, 
			GC_INTERVAL_MAIN_SWITCH * MS_PER_SEC,
			NULL,
			0);

	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_TIMER,
			"Timer_Set ID_TM_MAIN_SWITCH error!\n");

	return;
}


/*==========================================================================*
 * FUNCTION : MsTimersDestroy
 * PURPOSE  : To destroy MsTimers
 * CALLS    : Timer_Kill
 * CALLED BY: GC_TimersDestroy
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-13 10:20
 *==========================================================================*/
static void MsTimersDestroy()
{
	Timer_Kill(g_pGcData->hThreadSelf, ID_TM_REFRESH_DATA);
	Timer_Kill(g_pGcData->hThreadSelf, ID_TM_BATT_MGMT);
	Timer_Kill(g_pGcData->hThreadSelf, ID_TM_ENERGY_SAVING);
	Timer_Kill(g_pGcData->hThreadSelf, ID_TM_DSL_TEST);
	Timer_Kill(g_pGcData->hThreadSelf, ID_TM_RECT_REDUND);
	Timer_Kill(g_pGcData->hThreadSelf, ID_TM_MAIN_SWITCH);
	return;
}


/*==========================================================================*
 * FUNCTION : GC_TimersInit
 * PURPOSE  : To initialize SecTimers and MsTimers
 * CALLS    : MsTimersInit, CreatSecTimers
 * CALLED BY: InitGC
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-12 09:54
 *==========================================================================*/
void GC_TimersInit()
{
	int	i;

	MsTimersInit();

	//GC_SaveData(g_pGcData->SecTimer);
	g_pGcData->SecTimer.tTimeNow
		= GC_MaintainSecTimers(0, 0);

	//Second Timer
	for (i = 0; i < MAX_GC_SEC_TMR_NUM; i++)
	{
		g_pGcData->SecTimer.aSecTimer[i].byState = SEC_TMR_SUSPENDED;
		g_pGcData->SecTimer.aSecTimer[i].pfnGetTmrInterval = NULL;
	}

	return;
}

/*==========================================================================*
 * FUNCTION : GC_TimersDestroy
 * PURPOSE  : To destroy SecTimers and MsTimers
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-13 10:12
 *==========================================================================*/
void GC_TimersDestroy()
{
	MsTimersDestroy();
	return;
}




/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_engy_sav.c
 *  CREATOR  : Frank Cao                DATE: 2004-12-09 09:31
 *  VERSION  : V1.00
 *  PURPOSE  : Energy Saving
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#include	"gc_index_def.h"
#include	"gen_ctl.h"
#include	"gc_sig_value.h"
#include	"gc_batt_mgmt.h"

#include	"gc_engy_sav.h"

static void GetEngySavAct(OUT ENGY_SAV_ACT *pEngySavAct,
						  IN time_t tTimeNow);
static SIG_ENUM GetModeForDay(int iMonth, int iDay);
static RATE_MAX_POWER GetRateMaxPower(int iHour, SIG_ENUM ModeForDay);
static GC_REDUCE_CONSUMPTION_ACTION GetReduConsumpAct(SIG_ENUM RateType);
static BOOL IsExceed(float fMaxPower);
static BOOL IsExceedWithThreshold(float fMaxPower);
static float ReduceConsumption(GC_REDUCE_CONSUMPTION_ACTION ReduConsumpAct);
static float LmtPeakPower(ENGY_SAV_ACT EngySavAct);
static float DslOnHandling(BOOL bExceedWithThreshold);
static float DisableHandling(void);
static float ExceedHandling(float fPowerDiff);
static float NotExceedHandling(float fPowerDiff);
static void ReduceConsumptionCtl(GC_REDUCE_CONSUMPTION_ACTION ReduConsumpAct);
static void AllNotExceedActCtl(void);
static BOOL OneNotExceedActCtl(void);
static BOOL OneExceedActCtl(void);
static float GetMaxBattCurr(void);
static float GetMinBattCurr(void);


/*==========================================================================*
 * FUNCTION : GC_EngySavInit
 * PURPOSE  : Initialize g_pGcData->RunInfo.Bt.fExpBattCurr1 and 
              g_pGcData->RunInfo.Bt.fExpBattCurr2
 * CALLS    : 
 * CALLED BY: Init_GC
 * ARGUMENTS: 
 * RETURN   :
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 13:30
 *==========================================================================*/
void	GC_EngySavInit(void)
{
	g_pGcData->RunInfo.Bt.fExpBattCurr1
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
	g_pGcData->RunInfo.Bt.fExpBattCurr2 
		= g_pGcData->RunInfo.Bt.fExpBattCurr1;

	return;
}


/*==========================================================================*
 * FUNCTION : GC_EngySav
 * PURPOSE  : Energy Saving Function
 * CALLS    : GC_GetFloatValue GC_GetEnumValue GetEngySavAct 
              ReduceConsumption LmtPeakPower
 * CALLED BY: ServiceMain
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-10 10:03
 *==========================================================================*/
void	GC_EngySav(void)
{
	ENGY_SAV_ACT	EngySavAct;

	//According to now time and Total Power, get the action for Lower 
	//Consumptiom and Limit Max Power function
	GetEngySavAct(&EngySavAct, g_pGcData->SecTimer.tTimeNow);
#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif
	if(EngySavAct.bExceed)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						GC_PUB_OVER_PEAK_POWER_SET,
						GC_ALARM,
						TRUE);
	}
	else
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						GC_PUB_OVER_PEAK_POWER_SET,
						GC_NORMAL,
						TRUE);
	}

	if(STATE_AUTO != g_pGcData->RunInfo.enumAutoMan)
	//Manually
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_PROH_BATT_CH_SET,
						GC_NORMAL,
						TRUE);	

		return;
	}
#ifdef GC_SUPPORT_MPPT
	if(stState != GC_MPPT_DISABLE)
	{
		//No EnergySaving in MPPT.
		return;
	}
#endif

	//Excute Lower Consumption action and calculate fExpBattCurr1
	g_pGcData->RunInfo.Bt.fExpBattCurr1
		= ReduceConsumption(EngySavAct.ReduConsumpAct);

	//Excute Limit Max Power action and calculate fExpBattCurr2
	g_pGcData->RunInfo.Bt.fExpBattCurr2 = LmtPeakPower(EngySavAct);


	g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		= MIN(g_pGcData->RunInfo.Bt.fExpBattCurr1, 
			g_pGcData->RunInfo.Bt.fExpBattCurr2);

	if(g_pGcData->RunInfo.Bt.fCurrLmtSetPoint <= 0)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_PROH_BATT_CH_SET,
						GC_ALARM,
						TRUE);
	}
	else if(g_pGcData->RunInfo.Bt.fCurrLmtSetPoint > 0.005)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_PROH_BATT_CH_SET,
						GC_NORMAL,
						TRUE);	
	}

	return;
}

/*==========================================================================*
 * FUNCTION : GetEngySavAct
 * PURPOSE  : According to now time an Total Power, get the action for Lower 
              Consumption and if Total Power exceed Max Power
 * CALLS    : GetModeForDay GetRateMaxPower GetReduConsumpAct IsExceed 
              IsExceedWithThreshold
 * CALLED BY: GC_EngySav
 * ARGUMENTS: OUT ENGY_SAV_ACT  *pEngySavAct : 
 *            IN time_t         tTimeNow     : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-10 10:03
 *==========================================================================*/
static void GetEngySavAct(OUT ENGY_SAV_ACT *pEngySavAct,
						  IN time_t tTimeNow)
{
	struct tm		tmLocal;
	time_t			tLocalTime;

	SIG_ENUM		ModeForDay;
	RATE_MAX_POWER	RateMaxPower;

	tLocalTime = tTimeNow + (SECONDS_PER_HOUR / 2)
			* GC_GetLongValue(TYPE_OTHER, 0, GC_PUB_TIME_ZONE);

	gmtime_r(&tLocalTime, &tmLocal);

	ModeForDay = GetModeForDay((tmLocal.tm_mon + 1), tmLocal.tm_mday);
	RateMaxPower = GetRateMaxPower(tmLocal.tm_hour, ModeForDay);

	pEngySavAct->fPowerDiff 
		= g_pGcData->RunInfo.Ac.fTotalPower - RateMaxPower.fMaxPower;

	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_ES_NOW_RATE,
					RateMaxPower.enumRateType,
					TRUE);

	GC_SetFloatValue(TYPE_OTHER, 
					0, 
					GC_PUB_ES_PEAK_POWER,
					(FLOAT_EQUAL(RateMaxPower.fMaxPower, ENGY_SAV_MAX_POWER) 
					? 0.0 : RateMaxPower.fMaxPower));


	pEngySavAct->ReduConsumpAct 
		= GetReduConsumpAct(RateMaxPower.enumRateType);

	if(!(g_pGcData->RunInfo.Ac.bPowerValid))
	{
		pEngySavAct->bExceed = FALSE;
		pEngySavAct->bExceedWithThreshold = FALSE;
		return;
	}

	pEngySavAct->bExceed
		= IsExceed(RateMaxPower.fMaxPower);

	pEngySavAct->bExceedWithThreshold
		= IsExceedWithThreshold(RateMaxPower.fMaxPower);

	return;
}


/*==========================================================================*
 * FUNCTION : ReduceConsumption
 * PURPOSE  : Perform Lower Consumption control, calculate ExpBattCurr1
 * CALLS    : ReduceConsumptionCtl GC_GetFloatValue GC_GetEnumValue
 * CALLED BY: GC_EngySav
 * ARGUMENTS: GC_REDUCE_CONSUMPTION_ACTION  ReduConsumpAct : 
 * RETURN   : float : ExpBattCurr1
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 13:39
 *==========================================================================*/
static float ReduceConsumption(GC_REDUCE_CONSUMPTION_ACTION ReduConsumpAct)
{
	if(GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LC_ENB))
	{
		ReduceConsumptionCtl(ReduConsumpAct);
		return ReduConsumpAct.bProhibitBattChg ? 
			0.0 : GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
	}
	else
	{
		//According to Low Rate
		ReduceConsumptionCtl(*(g_pGcData->PriCfg.pReduceConsumptionAct 
			+ REDUCE_CONSUMPTION_TYPE_LOW));
		return GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);	
	}
}


/*==========================================================================*
 * FUNCTION : LmtPeakPower
 * PURPOSE  : Perform Limit Max Power control, calculate ExpBattCurr2
 * CALLS    : GC_GetEnumValue GC_GetFloatValue DslOnHandling ExceedHandling
              NotExceedHandling
 * CALLED BY: GC_EngySav
 * ARGUMENTS: BOOL  bExceed              : If exceed peak power
 *            BOOL  bExceedWithThreshold : If exceed (peak power - threshold) 
 * RETURN   : static float : ExpBattCurr2
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 13:44
 *==========================================================================*/
static float LmtPeakPower(ENGY_SAV_ACT EngySavAct)
{
	static BOOL	s_bLast = FALSE;

	if(GC_DISABLED 
			== GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPCL_ENB))
	{
		return DisableHandling();
	}


	if(g_pGcData->RunInfo.Ac.bDslValid 
		&& (GC_DIESEL_ON == g_pGcData->RunInfo.Ac.bDslState))
	{
		return DslOnHandling(EngySavAct.bExceedWithThreshold);
	}
	

	if(EngySavAct.bExceed)
	{
		s_bLast = EngySavAct.bExceed;
		return ExceedHandling(EngySavAct.fPowerDiff);
	}
	
	if(s_bLast)
	{
		g_pGcData->RunInfo.Bt.fExpBattCurr2 
			= g_pGcData->RunInfo.Bt.fCurrLmtSetPoint;
	}
	s_bLast = EngySavAct.bExceed;

	if(!EngySavAct.bExceedWithThreshold)
	{
		return NotExceedHandling(EngySavAct.fPowerDiff);
	}

	return g_pGcData->RunInfo.Bt.fExpBattCurr2;
}

/*==========================================================================*
 * FUNCTION : GetModeForDay
 * PURPOSE  : According to the date, get the mode of Energy Saving schedule
              for day.
 * CALLS    : 
 * CALLED BY: GetEngySavAct
 * ARGUMENTS: int  iMonth : 
 *            int  iDay   : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-10 10:03
 *==========================================================================*/
static SIG_ENUM GetModeForDay(int iMonth, int iDay)
{
	GC_ENGY_SAV_SCHEDULE	*pSchedule;

	ASSERT((iMonth > 0) && (iDay > 0) && (iMonth < 13) && (iDay < 32));

	pSchedule = g_pGcData->PriCfg.pEngySavSchedule;
	return (pSchedule + iDay - 1)->aenumMode[iMonth - 1];
}


/*==========================================================================*
 * FUNCTION : GetRateMaxPower
 * PURPOSE  : According to now hour and the mode for day, get the Rate Type
              and maximum power
 * CALLS    : 
 * CALLED BY: GetEngySavAct
 * ARGUMENTS: int       iHour      : 
 *            SIG_ENUM  ModeForDay : 
 * RETURN   : static RATE_MAX_POWER : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-10 10:03
 *==========================================================================*/
static RATE_MAX_POWER GetRateMaxPower(int iHour, SIG_ENUM ModeForDay)
{
	ASSERT((iHour >= 0) && (iHour < 24) && (ModeForDay < ENGY_SAV_MODE_NUM));
	return (g_pGcData->PriCfg.pEngySavMode + iHour)->aHourDef[ModeForDay];

}

/*==========================================================================*
 * FUNCTION : GetReduConsumpAct
 * PURPOSE  : According to the Rate Type, get action for Lower Consumption
 * CALLS    : 
 * CALLED BY: GetEngySavAct
 * ARGUMENTS: SIG_ENUM  RateType : 
 * RETURN   : static GC_REDUCE_CONSUMPTION_ACTION : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-10 10:03
 *==========================================================================*/
static GC_REDUCE_CONSUMPTION_ACTION GetReduConsumpAct(SIG_ENUM RateType)
{
	ASSERT(RateType < g_pGcData->PriCfg.iReduceConsumptionActNum);
	return *(g_pGcData->PriCfg.pReduceConsumptionAct + RateType);
}

/*==========================================================================*
 * FUNCTION : IsExceed
 * PURPOSE  : According to Total Power and Max Power, judge if Total Power 
              exceed Max Power
 * CALLS    : 
 * CALLED BY: GetEngySavAct
 * ARGUMENTS: float  fMaxPower : 
 * RETURN   : static GC_LIMIT_MAX_POWER_ACTION : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-10 10:03
 *==========================================================================*/
static BOOL IsExceed(float fMaxPower)
{
	return (g_pGcData->RunInfo.Ac.fTotalPower > fMaxPower);
}

/*==========================================================================*
 * FUNCTION : IsExceedWithThreshold
 * PURPOSE  : According to Total Power, Threshold and Max Power, judge if 
              Total Power exceed Max Power reduce Threshold
 * CALLS    : GC_GetFloatValue
 * CALLED BY: GetEngySavAct
 * ARGUMENTS: fMaxPower
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 13:54
 *==========================================================================*/
static BOOL IsExceedWithThreshold(float fMaxPower)
{
	float fThreshold;

	fThreshold = GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_ES_THRESHOLD);
	return (g_pGcData->RunInfo.Ac.fTotalPower 
				> (fMaxPower - fThreshold));
}


/*==========================================================================*
 * FUNCTION : ReduceConsumptionCtl
 * PURPOSE  : Perform Lower Consumption control
 * CALLS    : DxiSetData
 * CALLED BY: ReduceConsumption
 * ARGUMENTS: GC_REDUCE_CONSUMPTION_ACTION  ReduConsumpAct : 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 13:58
 *==========================================================================*/
static void ReduceConsumptionCtl(GC_REDUCE_CONSUMPTION_ACTION ReduConsumpAct)
{
	int					iRst;
	GC_SIG_INFO	*pOut;
	VAR_VALUE_EX		SigVal;

	SigVal.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	SigVal.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	SigVal.pszSenderName = "GEN_CTRL";

	pOut = g_pGcData->PriCfg.pEngySavOutput + 0;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = ReduConsumpAct.enumOutput1;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				sizeof(VAR_VALUE_EX),
				&SigVal,
				0);
		}
	}
	
	pOut = g_pGcData->PriCfg.pEngySavOutput + 1;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = ReduConsumpAct.enumOutput2;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				sizeof(VAR_VALUE_EX),
				&SigVal,
				0);
		}
	}

	pOut = g_pGcData->PriCfg.pEngySavOutput + 2;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = ReduConsumpAct.enumOutput3;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				sizeof(VAR_VALUE_EX),
				&SigVal,
				0);
		}
	}

	return;
}


/*==========================================================================*
 * FUNCTION : DslOnHandling
 * PURPOSE  : Handling when diesel is startup
 * CALLS    : GC_SetEnumValue AllNotExceedActCtl GC_GetFloatValue
 * CALLED BY: LmtPeakPower
 * ARGUMENTS: bExceedWithThreshold
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 14:00
 *==========================================================================*/
static float DslOnHandling(BOOL bExceedWithThreshold)
{
	AllNotExceedActCtl();

	if((GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_ES_DSL_CTL_ENB))
		&& (GC_DIESEL_INHABIT_OFF==GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_CTL_INH,GC_DIESEL_INVALID))
		&& (GC_DIESEL_TEST_OFF == GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_TEST_STA,GC_DIESEL_INVALID)))
	{
		if(!bExceedWithThreshold)
		{
			GC_SetEnumValue(TYPE_OTHER, 
							0,
							DSL_PUB_STOP_CTL,
							GC_DIESEL_CTL,
							TRUE);
			GC_LOG_OUT(APP_LOG_INFO, 
				"MPCL controls the diesel off.\n");
			
		}
	}
	return GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
}
	

/*==========================================================================*
 * FUNCTION : ExceedHandling
 * PURPOSE  : Handling when Total Power exceed Max Power
 * CALLS    : OneExceedActCtl GC_GetFloatValue GC_GetEnumValue 
 * CALLED BY: LmtPeakPower
 * ARGUMENTS: BOOL  bExceedWithThreshold : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 14:00
 *==========================================================================*/
#define		GC_MIN_BATT_CURR_LMT	(-0.50)
static float ExceedHandling(float fPowerDiff)
{
	if(OneExceedActCtl())
	{
		g_pGcData->RunInfo.Bt.fExpBattCurr2 
			= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);	
		return g_pGcData->RunInfo.Bt.fExpBattCurr2;
	}

	if((GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_ES_DSL_CTL_ENB))
		&& (GC_DIESEL_INHABIT_OFF==GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_CTL_INH,GC_DIESEL_INVALID))
		&& g_pGcData->RunInfo.Ac.bDslValid
		&& (GC_DIESEL_TEST_OFF == GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_TEST_STA,GC_DIESEL_INVALID)))
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						DSL_PUB_START_CTL,
						GC_DIESEL_CTL,
						TRUE);
		g_pGcData->RunInfo.Bt.fExpBattCurr2 
			= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);	

		GC_LOG_OUT(APP_LOG_INFO, 
			"MPCL controls the diesel on.\n");

		return g_pGcData->RunInfo.Bt.fExpBattCurr2;	
	}

	if(GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_ES_BATT_DISCHG_ENB))
	{
		float	fMaxBattCurr = GetMaxBattCurr();
		float	fDelta;
		float	fRatedCap;
		BOOL   bRTN;
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, 0, BT_PRI_RATED_CAP,&fRatedCap);
		if(bRTN == FALSE)
			return g_pGcData->RunInfo.Bt.fExpBattCurr2;

		if((g_pGcData->RunInfo.Bt.fExpBattCurr2 * fRatedCap) > fMaxBattCurr)
		{
			g_pGcData->RunInfo.Bt.fExpBattCurr2 
				= fMaxBattCurr /fRatedCap;
		}

		fDelta = (1000.0 * fPowerDiff * GC_RECT_EFFICIENCY
					/ g_pGcData->RunInfo.fSysVolt) 
					/ (g_pGcData->EquipInfo.iCfgQtyOfBatt 
					* fRatedCap);

		if(fDelta < 0.0001)
		{
			fDelta = 0.0001;
		}

		g_pGcData->RunInfo.Bt.fExpBattCurr2 -= fDelta;

		if(g_pGcData->RunInfo.Bt.fExpBattCurr2 < GC_MIN_BATT_CURR_LMT)
		{
			return (GC_MIN_BATT_CURR_LMT);
		}
		else
		{
			return g_pGcData->RunInfo.Bt.fExpBattCurr2;
		}
	}

	return GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
}


/*==========================================================================*
 * FUNCTION : NotExceedHandling
 * PURPOSE  : Handling when Total Power not exceed Max Power
 * CALLS    : GC_GetEnumValue OneNotExceedActCtl 
 * CALLED BY: LmtPeakPower
 * ARGUMENTS: BOOL  bExceedWithThreshold : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 14:01
 *==========================================================================*/
static float NotExceedHandling(float fPowerDiff)
{
	if(GC_ENABLED != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_ES_BATT_DISCHG_ENB))
	{
		g_pGcData->RunInfo.Bt.fExpBattCurr2 
			= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
	}

	if(g_pGcData->RunInfo.Bt.fExpBattCurr2 
		>= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT))
	{
		
		OneNotExceedActCtl();

		g_pGcData->RunInfo.Bt.fExpBattCurr2 
			= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);

		return GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
	}

	{

		float	fMaxBattCurr = GetMaxBattCurr();
		float	fMinBattCurr = GetMinBattCurr();
		float	fRatedCap;
		BOOL bRTN;

		bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, 0, BT_PRI_RATED_CAP,&fRatedCap);
		if(bRTN == TRUE)
		{
			float	fDelta = (800.0 * fPowerDiff * GC_RECT_EFFICIENCY
				/ g_pGcData->RunInfo.fSysVolt) 
				/ (g_pGcData->EquipInfo.iCfgQtyOfBatt 
				* fRatedCap);

			if((g_pGcData->RunInfo.Bt.fExpBattCurr2 * fRatedCap) > (fMaxBattCurr + 0.03 * fRatedCap))
			{
				g_pGcData->RunInfo.Bt.fExpBattCurr2 
					= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
			}
			else if((g_pGcData->RunInfo.Bt.fExpBattCurr2 * fRatedCap) < fMinBattCurr)
			{
				g_pGcData->RunInfo.Bt.fExpBattCurr2 
					= fMinBattCurr /fRatedCap- fDelta;
			}
			else
			{
				g_pGcData->RunInfo.Bt.fExpBattCurr2 -= fDelta;
			}

		}		

	}
	
	if(g_pGcData->RunInfo.Bt.fExpBattCurr2 
		< GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT))

	{
		return g_pGcData->RunInfo.Bt.fExpBattCurr2;
	}

	g_pGcData->RunInfo.Bt.fExpBattCurr2 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);

	return g_pGcData->RunInfo.Bt.fExpBattCurr2;
}


static float DisableHandling(void)
{
	g_pGcData->RunInfo.Bt.fExpBattCurr2 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
	
	OneNotExceedActCtl();

	return GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
}



/*==========================================================================*
 * FUNCTION : AllNotExceedActCtl
 * PURPOSE  : Perform all of not exceed actions
 * CALLS    : DxiSetData
 * CALLED BY: DslOnHandling
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 14:10
 *==========================================================================*/
static void AllNotExceedActCtl(void)
{
	int							iRst;
	GC_SIG_INFO			*pOut;
	GC_LIMIT_MAX_POWER_ACTION	*pAct;
	VAR_VALUE_EX				SigVal;

	SigVal.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	SigVal.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	SigVal.pszSenderName = "GEN_CTRL";

	pAct = g_pGcData->PriCfg.pLmtMaxPowerAct + 1;//not exceed action

	pOut = g_pGcData->PriCfg.pEngySavOutput + 3;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput1;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				sizeof(VAR_VALUE_EX),
				&SigVal,
				0);
		}
	}
	
	pOut = g_pGcData->PriCfg.pEngySavOutput + 4;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput2;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				sizeof(VAR_VALUE_EX),
				&SigVal,
				0);
		}
	}

	pOut = g_pGcData->PriCfg.pEngySavOutput + 5;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput3;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				sizeof(VAR_VALUE_EX),
				&SigVal,
				0);
		}
	}

	GC_LOG_OUT(APP_LOG_INFO, 
		"All of LIMIT_MAX_POWER_OUTPUT act by Notexceed Action.\n");

	return;
}


/*==========================================================================*
 * FUNCTION : OneNotExceedActCtl
 * PURPOSE  : Perform one of Not Exceed action 
 * CALLS    : DxiGetData DxiSetData
 * CALLED BY: NotExceedHandling
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 14:12
 *==========================================================================*/
static BOOL OneNotExceedActCtl(void)
{
	int							iRst;
	GC_SIG_INFO			*pOut;
	GC_LIMIT_MAX_POWER_ACTION	*pAct;
	SIG_BASIC_VALUE				*pSigBasicVal;
	int							iBufLen;
	VAR_VALUE_EX				SigVal;

	SigVal.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	SigVal.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	SigVal.pszSenderName = "GEN_CTRL";	
	
	pAct = g_pGcData->PriCfg.pLmtMaxPowerAct + 1;//not exceed action

	pOut = g_pGcData->PriCfg.pEngySavOutput + 5;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput3;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				&iBufLen,
				&pSigBasicVal,
				0);
			GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"DxiGetData Energy Saving Output error! \n");

			if(SIG_VALUE_IS_VALID(pSigBasicVal))
			{
				if(SigVal.varValue.enumValue 
					!= pSigBasicVal->varValue.enumValue)
				{
					iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
						pOut->iEquipId,
						DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
						sizeof(VAR_VALUE_EX),
						&SigVal,
						0);
				
					GC_LOG_OUT(APP_LOG_INFO, 
						"LIMIT_MAX_POWER_OUTPUT3 acts by Notexceed Action.\n");

					return TRUE;
				}
			}
		}
	}
	
	pOut = g_pGcData->PriCfg.pEngySavOutput + 4;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput2;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				&iBufLen,
				&pSigBasicVal,
				0);
			GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"DxiGetData Energy Saving Output error! \n");

			if(SIG_VALUE_IS_VALID(pSigBasicVal))
			{
				if(SigVal.varValue.enumValue != pSigBasicVal->varValue.enumValue)
				{
					iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
						pOut->iEquipId,
						DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
						sizeof(VAR_VALUE_EX),
						&SigVal,
						0);
			
					GC_LOG_OUT(APP_LOG_INFO, 
						"LIMIT_MAX_POWER_OUTPUT2 acts by Notexceed Action.\n");

					return TRUE;
				}
			}
		}
	}

	pOut = g_pGcData->PriCfg.pEngySavOutput + 3;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput1;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				&iBufLen,
				&pSigBasicVal,
				0);
			GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"DxiGetData Energy Saving Output error! \n");

			if(SIG_VALUE_IS_VALID(pSigBasicVal))
			{
				if(SigVal.varValue.enumValue != pSigBasicVal->varValue.enumValue)
				{
					iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
						pOut->iEquipId,
						DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
						sizeof(VAR_VALUE_EX),
						&SigVal,
						0);
					GC_LOG_OUT(APP_LOG_INFO, 
						"LIMIT_MAX_POWER_OUTPUT1 acts by Notexceed Action.\n");

					return TRUE;
				}
			}
		}
	}
	return FALSE;
}



/*==========================================================================*
 * FUNCTION : OneExceedActCtl
 * PURPOSE  : Perform one of Exceed action
 * CALLS    : DxiGetData DxiSetData
 * CALLED BY: ExceedHandling
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-14 14:15
 *==========================================================================*/
static BOOL OneExceedActCtl(void)
{
	int							iRst;
	GC_SIG_INFO			*pOut;
	GC_LIMIT_MAX_POWER_ACTION	*pAct;
	SIG_BASIC_VALUE*			pSigBasicVal;
	int							iBufLen;
	VAR_VALUE_EX				SigVal;

	SigVal.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	SigVal.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	SigVal.pszSenderName = "GEN_CTRL";	

	pAct = g_pGcData->PriCfg.pLmtMaxPowerAct;//exceed action

	pOut = g_pGcData->PriCfg.pEngySavOutput + 3;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput1;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				&iBufLen,
				&pSigBasicVal,
				0);
			GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"DxiGetData Energy Saving Output error! \n");

			if(SIG_VALUE_IS_VALID(pSigBasicVal))
			{
				if(SigVal.varValue.enumValue 
					!= pSigBasicVal->varValue.enumValue)
				{
					iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
						pOut->iEquipId,
						DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
						sizeof(VAR_VALUE_EX),
						&SigVal,
						0);
					
					GC_LOG_OUT(APP_LOG_INFO, 
						"LIMIT_MAX_POWER_OUTPUT1 acts by Exceed Action.\n");

					return TRUE;
				}
			}
		}
	}
	
	pOut = g_pGcData->PriCfg.pEngySavOutput + 4;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput2;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				&iBufLen,
				&pSigBasicVal,
				0);
			GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"DxiGetData Energy Saving Output error! \n");

			if(SIG_VALUE_IS_VALID(pSigBasicVal))
			{
				if(SigVal.varValue.enumValue 
					!= pSigBasicVal->varValue.enumValue)
				{
					iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
						pOut->iEquipId,
						DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
						sizeof(VAR_VALUE_EX),
						&SigVal,
						0);
					
					GC_LOG_OUT(APP_LOG_INFO, 
						"LIMIT_MAX_POWER_OUTPUT2 acts by Exceed Action.\n");

					return TRUE;
				}
			}
		}
	}

	pOut = g_pGcData->PriCfg.pEngySavOutput + 5;
	if(pOut->bEnabled)
	{
		SigVal.varValue.enumValue = pAct->enumOutput3;
		if(ENGY_SAV_NO_ACT != SigVal.varValue.enumValue)
		{
			iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
				pOut->iEquipId,
				DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
				&iBufLen,
				&pSigBasicVal,
				0);
			GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"DxiGetData Energy Saving Output error! \n");

			if(SIG_VALUE_IS_VALID(pSigBasicVal))
			{
				if(SigVal.varValue.enumValue 
					!= pSigBasicVal->varValue.enumValue)
				{
					iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
						pOut->iEquipId,
						DXI_MERGE_SIG_ID(pOut->iSignalType, pOut->iSignalId),
						sizeof(VAR_VALUE_EX),
						&SigVal,
						0);
					GC_LOG_OUT(APP_LOG_INFO, 
						"LIMIT_MAX_POWER_OUTPUT3 acts by Exceed Action.\n");

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}


static	float GetMaxBattCurr(void)
{
	int		i;
	float	fMaxCurr = -10000000.0;
	BOOL	bRst = TRUE;
	for(i = 0; i < g_pGcData->EquipInfo.iCfgQtyOfBatt; i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i])
		{
			bRst = FALSE;
			fMaxCurr = MAX(g_pGcData->RunInfo.Bt.afBattCurr[i], fMaxCurr);
		}
	}

	if(bRst)
	{
		return 0.0;
	}

	return fMaxCurr;
}


static	float GetMinBattCurr(void)
{
	int		i;
	float	fMinCurr = 10000000.0;
	BOOL	bRst = TRUE;

	for(i = 0; i < g_pGcData->EquipInfo.iCfgQtyOfBatt; i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i])
		{
			bRst = FALSE;
			fMinCurr = MIN(g_pGcData->RunInfo.Bt.afBattCurr[i], fMinCurr);
		}
	}

	if(bRst)
	{
		return 0.0;
	}

	return fMinCurr;

}




/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_batt_mgmt.h
 *  CREATOR  : Frank Cao                DATE: 2004-09-13 14:15
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef _BATT_MGMT_H_
#define _BATT_MGMT_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"
#include	"gc_timer.h"
#include	"gc_init_equip.h"

#define	GEN_CTL_DATA_FILE	"gen_ctl.dat"


#define SMBAT_TYPEID	304
#define CSUBAT_TYPEID	307

#define	TEMP_USAGE_DISABLED				0
#define	TEMP_USAGE_AMBIENT				1
#define	TEMP_USAGE_BATTERY				2

#define	mCURRENTMODE	0	
#define	mVOLTAGEMODE	1	

#define	HI_TEMP_ACTION_DISABLED			0
#define	HI_TEMP_ACTION_LOWER_VOLT		1
#define	HI_TEMP_ACTION_DISCONNECT		2

#define RECTIFIER_DOUBLE_POWER_TYPE		0

#define GC_SUPPORT_ESNA_COMP_MODE	//for ESNA CR demand,add a new comp temp mode that does not have to do with temp alarm

enum	BATT_MGMT_STATE
{
	ST_FLOAT = 0,
	ST_SHORT_TEST,
	ST_BC_FOR_TEST,
	ST_MAN_TEST,
	ST_PLAN_TEST,
	ST_AC_FAIL_TEST,
	ST_AC_FAIL,
	ST_MAN_BC,
	ST_AUTO_BC,
	ST_CYCLIC_BC,
	ST_POWER_SPLIT_BC,
	ST_POWER_SPLIT_BT,
	ST_PROTECTION,
};

enum	VOLT_TYPE
{
	TYPE_FLOAT_VOLT = 0,
	TYPE_BOOST_VOLT,
	TYPE_TEST_VOLT,
	TYPE_POWER_SPLIT,
	TYPE_VERY_HI_TEMP,
};

/* define Hybrid alarm relay */
enum HYBRID_ALARM_RELAYS_ENUM              /* NOTE: not change their values! */
{
	IB2_ALARM_RELAY_NONE = 0,
	IB2_ALARM_RELAY_GENERATOR_ALARM = 13,	
	IB2_ALARM_RELAY_BATTERY_ALARM,	
	IB2_ALARM_RELAY_RECTIFIER_ALARM,	
	IB2_ALARM_RELAY_SYSTEM_ALARM,
	IB2_ALARM_RELAY_RUNONOVERTEMP,
	IB2_ALARM_RELAY_SIXTH,
	IB2_ALARM_RELAY_DG1,
	IB2_ALARM_RELAY_DG2,
};
//changed by SongXu to DI1-8 moving from system to IB. 20160810 
enum HYBRID_DI_ENUM              /* NOTE: not change their values! */
{
	IB2_DI_NONE = 0,
	IB2_DI1_FIRST = 1,	
	IB2_DI2_SECOND,	
	IB2_DI3_THIRD,	
	IB2_DI4_FOURTH,
	IB2_DI5_FIFTH,
	IB2_DI6_SIXTH,
	IB2_DI7_SEVENTH,
	IB2_DI8_EIGHTH,
	NCU_DI1_FIRST = 191,	
	NCU_DI2_SECOND,	
	NCU_DI3_THIRD,	
	NCU_DI4_FOURTH,
};

enum VOLTAGE_TYPE              /* NOTE: not change their values! */
{
	VOLTAGE_48V = 0,
	VOLTAGE_24V = 1,	
};

enum POWER_INPUT_TYPE              /* NOTE: not change their values! */
{
	POWER_INPUT_AC = 0,
	POWER_INPUT_DIESEL = 1,	
};


struct	tagGC_BATTCURR_INFO_TYPE		
{
	float		fSumOfNegCurr;
	float		fMaxNegAdjust;
	float		fMinPosAdjust;
	float		fMinPosDiff ;
	float		fMaxNegDiffToCurrUpLimit;
	float		fMinPosToHalfStepDiff;
	BOOL		bOverCurrFlag ;
	BOOL		bAllBattUnderLowLmt ;
	BOOL		bBattOverHighLmt ;
	BOOL		bBattCurrLmtAlarm;
	BOOL		bBattCurrLmtNormal;


};
typedef struct tagGC_BATTCURR_INFO_TYPE GC_BATTCURR_INFO_TYPE;

#define	GC_RECT_ON			0
#define	GC_RECT_OFF			1

#define GC_CT_ON			1
#define GC_CT_OFF			0

#define GC_NORMAL			0
#define GC_ALARM			1
#define GC_LDU_UNDTEMP_ALM		1
#define GC_LDU_HITEMP_ALM		2

#define GC_ENABLED			1
#define GC_DISABLED			0
#define GC_EQUIP_INVALID		2
#define GC_CYC_VALID_ENABLE		0
#define GC_PRELIMIT_OVER		0
#define GC_PRELIMIT_RUN			1
#define GC_PRELIMIT_RUNTOOVER		2



#define GC_CURRLIMIT_DISABLE		0
#define GC_CURRLIMIT_CURRMODE		1
#define GC_CURRLIMIT_VOLTMODE		2

#define GC_VOLTADJUST_MINVALUE_24V	23.5
#define GC_VOLTADJUST_MINVALUE_48V	47
#define GC_VOLTADJUST_DELTA		0.5

#define GC_CURRLIMITALARM_DELTA		0.5
#define GC_CURRLIMITNORMAL_DELTA	0.3

#define GC_SYSTEMVOLT_JUDGE		35
#define GC_BATTCELLNUM_24V		12
#define GC_BATTCELLNUM_48V		24

#define GC_BATT_ADJUST_CYCLE_NUM	2

#define GC_BATT_ADJUST_SLOW_COEFF	0.5
#define GC_BATT_ADJUST_FAST_COEFF	01


#define GC_BattADJUST_CELLCOEFF		0.15

#define GC_VOLT_MIN_STEP		0.001
#define GC_VOLT_MAX_STEP		0.04


#define  GC_VOLTADJUST_OK		0
#define  GC_VOLTADJUST_PROCESSING	1

#define GC_VOLTADJUSTOK_TIME		5

#define	CTL_FLOAT			0
#define	CTL_BOOST			1

#define	CTL_TEST_START		1
#define	CTL_TEST_STOP		0

#define DSL_NO_RUNNING		0
#define DSL_RUNNING			1

#define RECT_RUN_MODE_DERATING		0
#define RECT_RUN_MODE_FULL_CAPACITY	1

//#define	DIFFERENCE_OF_TEST_VOLTAGE	0.2

#ifndef SUB_DIR_LOG_FILE
#define SUB_DIR_LOG_FILE			"log"
#endif

#ifndef ENV_ACU_ROOT_DIR
#define ENV_ACU_ROOT_DIR			"ACU_ROOT_DIR"	// to use shell env variable.
#endif

#define	GEN_CTL_DATA_RECORDS		1
#define	GEN_CTL_DATA_FREQUENCY		(5.0)

#define	GC_CURR_LMT_SET_POINT_UPPER	(0.975)
#define	GC_CURR_LMT_SET_POINT_LOWER	(0.970)

#define	GC_VOLT_LMT_SET_POINT_UPPER	(0.95)
#define	GC_VOLT_LMT_SET_POINT_LOWER	(0.90)
#define	GC_VOLT_LMT_SET_POINT_SLOW	(0.50)
#define	GC_BCL_ALARM_VOLT_MODE		(0.85)
#define	GC_BCL_NORMAL_VOLT_MODE		(0.80)

//#define	GC_MAX_CURR_LMT	(g_pGcData->RunInfo.Rt.fRatedVolt > 35.0 ? 121.0 : 106.7)
//Some of 24V rectifiers Maximal Current Limitation is 111.0% by a bug, Here we do not consider it. 
#define	GC_MAX_CURR_LMT	(121.0)
#define	GC_MAX_CURR_LMT_CONV	(116.0) //(116.0)
#define	GC_MAX_CURR_LMT_CONV_24V	(116.0) //(116.0)

#define	GC_MIN_CURR_LMT	(0.01)
#define GC_MIN_CURR_LMT_IN_NA		(10.0)
#define GC_MIN_CURR_LMT_IN_CONV		(10.0)//(50.0)

#define	GC_CT_MAX_CURR_LMT		(31.3)

#define	GC_MIN_CURR_LMT_NA	(10.0)

#define	DELAY_AFTER_PRE_CURR_LMT	50 //35

#ifdef GC_SUPPORT_MPPT
#define  GC_MPPT_MIN_CURRENT_LIMIT		(1.0)
#endif

//#define	SAMP_ID_SM_BAT	4
#define GC_MAX_CURR_LIMIT_VALUE (12000.0)

/* Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
#define DISL_PWR_LMT_INPUT_NONE		0
#define DISL_PWR_LMT_INPUT_DI1		1
#define DISL_PWR_LMT_INPUT_DI2		2
#define DISL_PWR_LMT_INPUT_DI3		3
#define DISL_PWR_LMT_INPUT_DI4		4
#define DISL_PWR_LMT_INPUT_DI5		5
#define DISL_PWR_LMT_INPUT_DI6		6
#define DISL_PWR_LMT_INPUT_DI7		7
#define DISL_PWR_LMT_INPUT_DI8		8

#define SYS_DI_ON					0
#define SYS_DI_OFF					1

#define	SYS_DI_LOW					0
#define	SYS_DI_HIGH					1
/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */

//Added by YangGuoxin,2/13/2007; for Gustaf's CR
#define ENUM_EMEA_MODE				0
#define ENUM_OTHER_MODE				1
//End by YangGuoxi.

//#define HYBRID_TEST_MODE		0

#define GC_HYBRID_GRID_ON		0		//relay on means grid on
#define GC_HYBRID_GRID_OFF		1		//relay off means grid off

#define GC_HYBRID_DISABLED		0
#define GC_HYBRID_ENABLED		1
#define GC_HYBRID_CAPACITY_MODE		1
#define GC_HYBRID_FIXED_MODE		2

#define GC_HYBRID_RELAY_STATUS_OPEN		0
#define GC_HYBRID_RELAY_STATUS_CLOSE		1
#define GC_NORMAL_OPEN			0
#define GC_NORMAL_CLOSE			1

#define	HYBRID_IB_GROUP_EQUIP_ID		1
#define	HYBRID_IB_GROUP_EQUIP_ID_IB		202

#define	HYBRID_PUB_DG_SELECTED_NOTHING		-1
#define	HYBRID_PUB_DG1_SELECTED		0
#define	HYBRID_PUB_DG2_SELECTED		1
#define	HYBRID_PUB_DG2DG1_SELECTED	2
#define	HYBRID_PUB_RUNNING_OVERTEMP	3
#define HYBRID_PUB_DO1			13
/*End*/

void GC_BattMgmtInit(void);
void GC_BattMgmtDestroy(void);
void GC_BattMgmt(void);
void GC_MasterBattMgmt(void);
void GC_RectVoltCtl(int iVoltType, BOOL bNeedTempComp);
void GC_CurrentLmt(void);
void GC_PreCurrLmt(int iQtyOfRect,BOOL bAcFail,BOOL bLVDReconnect);
BOOL GC_GetBattFuseAlm(void);
int	GC_GetAlarmNum(int iAlmLevel);
BOOL GC_AlmJudgmentForBT(void);
BOOL GC_AlmJudgmentForBC(void);
BOOL GC_AlmJudgmentForFCtoBC(void);
void GC_LoadData(void);
void GC_SaveData(void);
void GC_BasicMaintain(void);
void GC_HandlingOnErr(SIG_ENUM enumState, const char *szLog);
int	GC_GetCurrLmtPeriod(int iIdx);
int GC_GetPreCurrLmtDelay(int iIdx);
void GC_RegulateVolt(IN BOOL bNeedRegulate);
float GC_GetRegulateVolt(float fMidVoltage);
void GC_EstopShutdown(void);
BOOL GC_IsCurrentLimit(void);
BOOL GC_IsPowerLmt(void);
BOOL GC_IsOverVolt(void);
BOOL GC_IsUnderVolt(void);
BOOL GC_IsCurrLimitUnderVolt(void);
float GC_GetSettingVolt(int iSigId);
void GC_SysAlmLightCtl(void);
BOOL GC_IsSmduInterrupt(void);
BOOL GC_IsVeryHiBattTemp(void);
void GC_DcVoltCtl(float fVolt);
void GC_PreCurrlmtForAcFail(void);
void GC_HybridMgmtTimerInit(void);

BOOL GC_IsDislPwrLmtMode(void);		// Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU
BOOL GC_IsLVDDisconnect(void);
SIG_ENUM GC_HybridGetSelectedDI(void);

BOOL GC_IsNeedVoltAdjust(void);
void CurrentLimitThroughVolt(BOOL bNeedMinOutputFlag,int iSigId, BOOL bNeedTempComp);
void GC_DcVoltCtlForTest(float fVolt, BOOL bStart);
#ifdef GC_SUPPORT_RELAY_TEST
void GC_AutoRelayTest(void);
#endif

#ifdef GC_SUPPORT_MPPT
void	 GC_PreCurrLmt_Mppt(int iQtyOfRect);
#endif
void GC_CurrlmtCtl_Slave(float fCtlValue);

#ifdef GC_SUPPORT_FIAMM
void GC_HandlingFiammBatt(SIG_ENUM enumState, const char *szLog);
#endif

#endif //_BATT_MGMT_H_

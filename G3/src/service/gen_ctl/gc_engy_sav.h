/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_engy_sav.h
 *  CREATOR  : Frank Cao                DATE: 2004-12-09 09:30
 *  VERSION  : V1.00
 *  PURPOSE  : Energy Saving
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#ifndef _GC_ENGY_SAVING_H_
#define _GC_ENGY_SAVING_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define	GC_DIESEL_CTL	1

#define	GC_DIESEL_INVALID	2

#define	GC_DIESEL_ON	1
#define	GC_DIESEL_OFF	0

#define	GC_DIESEL_INHABIT_OFF	0
#define	GC_DIESEL_INHABIT_ON	1

#define	GC_DIESEL_TEST_OFF	0
#define	GC_DIESEL_TEST_ON	1


#define	GC_RECT_EFFICIENCY	0.95

enum REDUCE_CONSUMPTION_TYPE
{
	REDUCE_CONSUMPTION_TYPE_PEAK = 0,
	REDUCE_CONSUMPTION_TYPE_HIGH,
	REDUCE_CONSUMPTION_TYPE_FLAT,
	REDUCE_CONSUMPTION_TYPE_LOW
};

struct	tagENGY_SAV_ACT		
{
	GC_REDUCE_CONSUMPTION_ACTION	ReduConsumpAct;
	BOOL							bExceed;
	BOOL							bExceedWithThreshold;
	float							fPowerDiff;
};
typedef struct tagENGY_SAV_ACT ENGY_SAV_ACT;

void	GC_EngySavInit(void);

void	GC_EngySav(void);

#endif //_GC_ENGY_SAVING_H_

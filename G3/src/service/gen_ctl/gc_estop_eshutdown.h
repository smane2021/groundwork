/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : gc_estop_eshutdown.h
 *  CREATOR  : Frank Cao                DATE: 2008-10-24 13:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#ifndef _GC_ESTOP_ESHUTDOWN_H_
#define _GC_ESTOP_ESHUTDOWN_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define	EMERGENCY_SHUTDOWN_INPUT_NUM	2
#define	ACTION_HYSTERESIS_TIMES			1

#define	PARAM_STATE_DISABLED			0
#define	PARAM_STATE_ESTOP				1
#define	PARAM_STATE_ESHUTDOWN			2


struct	tagRECT_ESTOP_INFO
{
	BOOL		bComm;
	BOOL		bAcFail;
	BOOL		bCommRecycle;
	BOOL		bAcFailRecycle;
};
typedef struct tagRECT_ESTOP_INFO RECT_ESTOP_INFO;

void GC_EstopShutdown(void);
void GC_RestoreEstop(void);

#endif //_GC_ESTOP_ESHUTDOWN_H_

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : test_log.c
 *  CREATOR  : Frank Cao                DATE: 2004-09-19 11:45
 *  VERSION  : V1.00
 *  PURPOSE  : battery test log interface functions
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"batt_test_log.h"
#include	"gen_ctl.h"
#include	"gc_index_def.h"
#include	"gc_sig_value.h"

int BattEquipIDlist[MAX_NUM_SOLUTION_BATT_EQUIP]=
{
	116,117,//battery1-2						 //0-1
	118,119,120,121,122,123,124,125,//EIB1-4 battery1-2	 //2-9
	126,127,128,129,//SMDU 1 battery1-4				 //10-13	
	130,131,132,133,//SMDU 2 battery1-4				 //14-17	
	134,135,136,137,//SMDU 3 battery1-4				 //18-21
	138,139,140,141,//SMDU 4 battery1-4				 //22-25	
	142,143,144,145,//SMDU 5 battery1-4				 //26-29	
	146,147,148,149,//SMDU 6 battery1-4				 //30-33	
	150,151,152,153,//SMDU 7 battery1-4				 //34-37	
	154,155,156,157,//SMDU 8 battery1-4				 //38-41	
	206,207,208,209,//EIB 1-4 battery3				 //42-45
	272,273,274,275,276,277,278,279,280,281,//SMbatt 1-10  //46-55
	282,283,284,285,286,287,288,289,290,291,//SMbatt 11-20 //56-65
	319,320,321,322,323,324,325,326,327,328,//LDUbatt 1-10 //66-75
	329,330,331,332,333,334,335,336,337,338,//LDUbatt 11-20 //76-85
	659,660,661,662,663,664,665,666,//SMDU1-8 batt 5	  //86-93
	680,681,682,683,684,685,686,687,688,689,//SMBRC batt 1-10	  //94-103
	690,691,692,693,694,695,696,697,698,699//SMBRC batt 1-10	  //104-113
};

/*==========================================================================*
* FUNCTION : GC_IsBattSigValid
* PURPOSE  : To judge if a batt signal is configured
* CALLS    : DxiGetData
* CALLED BY: InitBattInfo
* ARGUMENTS: BYTE  iEquipType : 
*            int   iEquipIdx   : 
*            int   iSigIdx     : 
* RETURN   : TRUE: the signal is in ACU solution configuration 
* COMMENTS : 
* CREATOR  :                DATE: 2004-10-20 11:32
*==========================================================================*/
static BOOL GC_IsBattSigValid(int iEquipID,int iSigIdx)
{
	int	iRst;
	int	iBufLen;
	SIG_BASIC_VALUE*	pSigValue;


	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,
		DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalType,
		(g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalID),
		&iBufLen,
		&pSigValue,
		0);

	if(iRst != ERR_OK)
	{	
		return FALSE;
	}

	if(SIG_VALUE_IS_VALID(pSigValue) && SIG_VALUE_IS_CONFIGURED(pSigValue))
	{
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
* FUNCTION : GC_IsEIBBattSigValid
* PURPOSE  : To judge if a batt signal is configured
* CALLS    : DxiGetData
* CALLED BY: InitBattInfo
* ARGUMENTS: BYTE  iEquipType : 
*            int   iEquipIdx   : 
*            int   iSigIdx     : 
* RETURN   : TRUE: the signal is in ACU solution configuration 
* COMMENTS : 
* CREATOR  :                DATE: 2004-10-20 11:32
*==========================================================================*/
static BOOL GC_IsEIBBattSigValid(int iEquipID,int iSigIdx)
{
	int	iRst;
	int	iBufLen;
	SIG_BASIC_VALUE*	pSigValue;


	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,
		DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalType,
		(g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalID),
		&iBufLen,
		&pSigValue,
		0);

	if(iRst != ERR_OK)
	{	
		return FALSE;
	}

	if(SIG_VALUE_IS_VALID(pSigValue) 
		&& SIG_VALUE_IS_CONFIGURED(pSigValue) 
		&& SIG_VALUE_NEED_DISPLAY(pSigValue))
	{
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
* FUNCTION : GC_GetFloatDataOfBatt
* PURPOSE  : To judge if a batt signal is configured
* CALLS    : DxiGetData
* CALLED BY: InitBattInfo
* ARGUMENTS: BYTE  iEquipType : 
*            int   iEquipIdx   : 
*            int   iSigIdx     : 
* RETURN   : TRUE: the signal is in ACU solution configuration 
* COMMENTS : 
* CREATOR  :                DATE: 2004-10-20 11:32
*==========================================================================*/
static float GC_GetFloatDataOfBatt(int iEquipID,int iSigIdx)
{
	int	iRst;
	int	iBufLen;
	SIG_BASIC_VALUE*	pSigValue;
	char			strOut[256];

	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,
		DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalType,
		(g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalID),
		&iBufLen,
		&pSigValue,
		0);

	if(iRst != ERR_OK)
	{
		sprintf(strOut, 
			" GC_GetFloatDataOfBatt error! EQUIPID: %d,  S_IDX: %d\n",
			iEquipID, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}

	if(VAR_FLOAT != pSigValue->ucType)
	{
		sprintf(strOut, 
			"Signal type error,  EQUIPID: %d, S_IDX: %d %d %s\n",
			iEquipID, iSigIdx,pSigValue->ucType, " GC_GetFloatDataOfBatt");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);	
	}

	return pSigValue->varValue.fValue;
}


/*==========================================================================*
 * FUNCTION : TestLogAddRecord
 * PURPOSE  : To provide a interface to add a battery test record
 * CALLS    : 
 * CALLED BY: GC_TestLogStart, GC_TestLogEnd, GC_TestLogHandling
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-19 17:21
 *==========================================================================*/
static void TestLogAddRecord(void)
{
	int			i, j;
	int			iOffset;
	utime_t		utCurrTime;
	float		fValue;
	BYTE		*pBattTestData;

	pBattTestData = (BYTE*)(g_pGcData->RunInfo.Bt.TestLog.pTestRecord)
				+ g_pGcData->RunInfo.Bt.TestLog.iRecordNo
				* g_pGcData->EquipInfo.BattTestLogInfo.iSizeOfBattTestRcd;

	iOffset = 0;
	
	/*Current Time*/
	utCurrTime = g_pGcData->SecTimer.tTimeNow;
	memcpy((pBattTestData + iOffset), 
			&utCurrTime, 
			sizeof(utCurrTime));
	iOffset += sizeof(utCurrTime);

	/*DC Volt*/
	fValue = g_pGcData->RunInfo.fSysVolt;
	memcpy((pBattTestData + iOffset),
			&fValue,
			sizeof(float));
	iOffset += sizeof(float);

	
	for(i = 0; i < MAX_NUM_TEMPERATURE; i++)
	{
		if(g_pGcData->EquipInfo.BattTestLogInfo.abTemp[i])
		{
			fValue = GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_TEMPERATURE1 + i);
			memcpy((pBattTestData + iOffset), 
					&fValue, 
					sizeof(float));
			iOffset += sizeof(float);
		}
	}

	for(i = 0; i < MAX_NUM_SOLUTION_BATT_EQUIP; i++)
	{
		//jump largeDU Battery
		//��ز��Լ�¼TR begin
		//if((i >= 46 && i <= 65) || i >= 86) //exclude SM
		//{
		//	continue;
		//}
		//��ز��Լ�¼TR end

		/*Battery Current*/
		if(g_pGcData->EquipInfo.BattTestLogInfo.aBattInfo[i].bCurr)
		{
			fValue = g_pGcData->RunInfo.Bt.afBattCurr[i];
			//printf("%d afBattCurr is %f\n", i, fValue);
			memcpy((pBattTestData + iOffset), 
					&fValue, 
					sizeof(float));
			iOffset += sizeof(float);
		}
        
		/*Battery Voltage*/
		if(g_pGcData->EquipInfo.BattTestLogInfo.aBattInfo[i].bVolt)
		{
			fValue = GC_GetFloatDataOfBatt(BattEquipIDlist[i], BT_PRI_VOLT);
			//printf("%d Volt is %f\n", i, fValue);
			memcpy((pBattTestData + iOffset), 
					&fValue, 
					sizeof(float));
			iOffset += sizeof(float);
		}

		/*Battery Capacity*/
		if(g_pGcData->EquipInfo.BattTestLogInfo.aBattInfo[i].bCap)
		{
			fValue = GC_GetFloatDataOfBatt(BattEquipIDlist[i], BT_PRI_CAP);	
			//printf("%d bCap is %f\n", i, fValue);
			memcpy((pBattTestData + iOffset), 
				&fValue, 
				sizeof(float));
			iOffset += sizeof(float);
		}
	}

	for(i = 0; i < MAX_NUM_SMBAT_OR_SMBRC; i++)
	{
	    for(j = 0; j < MAX_BLOCK_NUM; j++)
	    {
		if(g_pGcData->EquipInfo.BattTestLogInfo.aBlockInfo[i].abSMBlockVolt[j])
		{
		    if(g_pGcData->EquipInfo.BattTestLogInfo.iSMBAT_SMBRC == SMBAT_TYPE)
		    {
			fValue = GC_GetFloatValue(TYPE_BATT_UNIT, (i + g_pGcData->EquipInfo.BattTestLogInfo.iSMBATBeginNo), BT_PRI_BLK1_VOLT + j);
		    }
		    else if(g_pGcData->EquipInfo.BattTestLogInfo.iSMBAT_SMBRC == SMBRC_TYPE)
		    {
			fValue = GC_GetFloatValue(TYPE_BATT_UNIT, (i + g_pGcData->EquipInfo.BattTestLogInfo.iSMBRCBeginNo), BT_PRI_BLK1_VOLT + j);
		    }
		    else
		    {//�������������
			fValue = 0;
		    }
		    //printf("[%d] [%d] block voltage is %f\n", i, j, fValue);
		    memcpy((pBattTestData + iOffset), &fValue, sizeof(float));
		    iOffset += sizeof(float);
		}
	    }
	}

	for(i = 0; i < MAX_NUM_EIB_EQUIP; i++)
	{
		for(j = 0; j < MAX_NUM_OF_BLK_PER_EIB; j++)
		{
			if(g_pGcData->EquipInfo.BattTestLogInfo.aEibInfo[i].abBlockVolt[j])
			{
				fValue = GC_GetFloatValue(TYPE_EIB_UNIT, 
										i, 
										EIB_BLOCK1_VOLTAGE + j);
				memcpy((pBattTestData + iOffset), 
					&fValue, 
					sizeof(float));
				iOffset += sizeof(float);
			}
			
		}
	}

	ASSERT(iOffset == g_pGcData->EquipInfo.BattTestLogInfo.iSizeOfBattTestRcd);

	if(g_pGcData->RunInfo.Bt.TestLog.iRecordNo < (MAX_RECORD_PER_TEST - 1))
	{
		g_pGcData->RunInfo.Bt.TestLog.iRecordNo++;
	}
	return;
}


/*==========================================================================*
 * FUNCTION : BattTestLogInfoInit
 * PURPOSE  : To initialize the battery test log information
 * CALLS    : 
 * CALLED BY: GC_EquipInfoInit
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-09-29 15:26
 *==========================================================================*/
void	BattTestLogInfoInit(void)
{
	int		i, j, k;

	BATT_TEST_LOG_INFO*		pBattTestLogInfo 
		= &(g_pGcData->EquipInfo.BattTestLogInfo);

	for(i = 0; i < MAX_NUM_SMBAT_OR_SMBRC; i++)
	{
	    for(k = 0; k < MAX_BLOCK_NUM; k++)
	    {
		pBattTestLogInfo->aBlockInfo[i].abSMBlockVolt[k] = FALSE;
	    }
	}
	pBattTestLogInfo->iSMBAT_SMBRC = NONE;

	pBattTestLogInfo->iSizeOfBattTestRcd = 0;
	pBattTestLogInfo->iMaxSizeOfBattTestRcd	 = 0;

	/*Size of Current Time, Size of total voltage*/
	pBattTestLogInfo->iSizeOfBattTestRcd	
		= sizeof(utime_t) + sizeof(float);

	pBattTestLogInfo->iMaxSizeOfBattTestRcd	
		= sizeof(utime_t) + sizeof(float);

	for(i = 0; i < MAX_NUM_TEMPERATURE ; i++)
	{
		if((GC_IsSigValid(TYPE_OTHER, 0, GC_PUB_TEMPERATURE1 + i))
			&& (GC_DISABLED != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_TEMP1_USAGE + i)))
		{
			pBattTestLogInfo->abTemp[i] = TRUE;
			pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
		}
		else
		{
			pBattTestLogInfo->abTemp[i] = FALSE;
		}
		pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);
	}
	
	for(i = 0; i < MAX_NUM_BATT_EQUIP; i++)
	{
		pBattTestLogInfo->aBattInfo[i].bCurr = FALSE;
		pBattTestLogInfo->aBattInfo[i].bCap = FALSE;
		pBattTestLogInfo->aBattInfo[i].bVolt = FALSE;
		pBattTestLogInfo->aExistBattInfo[i].bTemp = FALSE;
		pBattTestLogInfo->aExistBattInfo[i].iQtyBlock = 0;
	}
	for(i = 0; i < MAX_NUM_SOLUTION_BATT_EQUIP; i++)
	{
		if(GC_IsBattSigValid(BattEquipIDlist[i],BT_PRI_CURR))
		{
			pBattTestLogInfo->aBattInfo[i].bCurr = TRUE;
			pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
			pBattTestLogInfo->aBattInfo[i].bCap = TRUE;
			pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
		}
		//Add 2*sizeof(float)
		pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);
		pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);

		if(GC_IsEIBBattSigValid(BattEquipIDlist[i],BT_PRI_VOLT))
		{
			pBattTestLogInfo->aBattInfo[i].bVolt = TRUE;
			pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
		}
		pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);
	
	}
int battInfoNum;
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(GC_IsSigValid(TYPE_BATT_UNIT, i, BT_PRI_TEMP))
		{
			pBattTestLogInfo->aExistBattInfo[i].bTemp = TRUE;
		}
		//else if(i > 45 && i <= 65)//SM Battery
		//{
		//	pBattTestLogInfo->aBattInfo[i].bTemp = TRUE;
		//}
		else if((g_pGcData->EquipInfo.aiEquipIdBtPri[i]>=272)&&(g_pGcData->EquipInfo.aiEquipIdBtPri[i]<=291))//SM Battery
		{
			pBattTestLogInfo->aExistBattInfo[i].bTemp = TRUE;
		}
		else
		{
			pBattTestLogInfo->aExistBattInfo[i].bTemp = FALSE;
		}

		for(j = 0; j < MAX_BLOCK_NUM; j++)
		{
			if(GC_IsSigValid(TYPE_BATT_UNIT, i, BT_PRI_BLK1_VOLT + j))
			{
				pBattTestLogInfo->aExistBattInfo[i].iQtyBlock++;
				if((g_pGcData->EquipInfo.aiEquipIdBtPri[i] >= 272) && (g_pGcData->EquipInfo.aiEquipIdBtPri[i] <= 291))//�����SMBat
				{
					pBattTestLogInfo->iSMBAT_SMBRC = SMBAT_TYPE;
					pBattTestLogInfo->aBlockInfo[i - pBattTestLogInfo->iSMBATBeginNo].abSMBlockVolt[j] = TRUE;
					pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
				}
				else if((g_pGcData->EquipInfo.aiEquipIdBtPri[i] >= 680) && (g_pGcData->EquipInfo.aiEquipIdBtPri[i] <= 699))//�����SMBRC
				{
					pBattTestLogInfo->iSMBAT_SMBRC = SMBRC_TYPE;
					pBattTestLogInfo->aBlockInfo[i - pBattTestLogInfo->iSMBRCBeginNo].abSMBlockVolt[j] = TRUE;
					pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
				}
				else
				{}
			}
			else
			{
				break;
			}
		}
	}
	
	//for(i = 0; i < GC_Get_Total_BattNum(); i++)
	//{
	//	if(GC_IsSigValid(TYPE_BATT_UNIT, i, BT_PRI_CURR))
	//	{
	//		pBattTestLogInfo->aBattInfo[i].bCurr = TRUE;
	//		pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);

	//		pBattTestLogInfo->aBattInfo[i].bCap = TRUE;
	//		pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
	//	}
	//	else
	//	{
	//		pBattTestLogInfo->aBattInfo[i].bCurr = FALSE;
	//		pBattTestLogInfo->aBattInfo[i].bCap = FALSE;
	//	}

	//	//Add 2*sizeof(float)
	//	pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);
	//	pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);

	//	//Exclude the battery 1 voltage,battery 2 voltage
	//	if(GC_IsEIBSigValid(TYPE_BATT_UNIT, i, BT_PRI_VOLT))
	//	{
	//		pBattTestLogInfo->aBattInfo[i].bVolt = TRUE;
	//		pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
	//	}
	//	else
	//	{
	//		pBattTestLogInfo->aBattInfo[i].bVolt = FALSE;
	//	}

	//	pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);

	//	if(GC_IsSigValid(TYPE_BATT_UNIT, i, BT_PRI_TEMP))
	//	{
	//		pBattTestLogInfo->aBattInfo[i].bTemp = TRUE;
	//	}
	//	//else if(i > 45 && i <= 65)//SM Battery
	//	//{
	//	//	pBattTestLogInfo->aBattInfo[i].bTemp = TRUE;
	//	//}
	//	else if((g_pGcData->EquipInfo.aiEquipIdBtPri[i]>=272)&&(g_pGcData->EquipInfo.aiEquipIdBtPri[i]<=291))//SM Battery
	//	{
	//		pBattTestLogInfo->aBattInfo[i].bTemp = TRUE;
	//	}
	//	else
	//	{
	//		pBattTestLogInfo->aBattInfo[i].bTemp = FALSE;
	//	}

	//	pBattTestLogInfo->aBattInfo[i].iQtyBlock = 0;
	//
	//	for(j = 0; j < MAX_BLOCK_NUM; j++)
	//	{
	//		if(GC_IsSigValid(TYPE_BATT_UNIT, i, BT_PRI_BLK1_VOLT + j))
	//		{
	//		    //printf("%d BT_PRI_BLK_VOLT is valid\n", i);
	//			pBattTestLogInfo->aBattInfo[i].iQtyBlock++;
	//			if((g_pGcData->EquipInfo.aiEquipIdBtPri[i] >= 272) && (g_pGcData->EquipInfo.aiEquipIdBtPri[i] <= 291))
	//			//if(i >= 46 && i <= 65)//�����SMBat
	//			{
	//			 //   if(g_pGcData->EquipInfo.aiEquipIdBtPri[i] == 272)
	//			 //   {
	//				//pBattTestLogInfo->iSMBATBeginNo = i;
	//				////printf("pBattTestLogInfo->iSMBATBeginNo is %d\n", pBattTestLogInfo->iSMBATBeginNo);
	//			 //   }
	//			    pBattTestLogInfo->iSMBAT_SMBRC = SMBAT_TYPE;
	//			    pBattTestLogInfo->aBlockInfo[i - pBattTestLogInfo->iSMBATBeginNo].abSMBlockVolt[j] = TRUE;
	//			    pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
	//			}
	//			else if((g_pGcData->EquipInfo.aiEquipIdBtPri[i] >= 680) && (g_pGcData->EquipInfo.aiEquipIdBtPri[i] <= 699))
	//			//else if(i >= 94 && i <= 113)//�����SMBRC
	//			{
	//			    /*if(g_pGcData->EquipInfo.aiEquipIdBtPri[i] == 680)
	//			    {
	//				pBattTestLogInfo->iSMBRCBeginNo = i;
	//				printf("pBattTestLogInfo->iSMBRCBeginNo is %d\n", pBattTestLogInfo->iSMBRCBeginNo);
	//			    }*/
	//			    pBattTestLogInfo->iSMBAT_SMBRC = SMBRC_TYPE;
	//			    pBattTestLogInfo->aBlockInfo[i - pBattTestLogInfo->iSMBRCBeginNo].abSMBlockVolt[j] = TRUE;
	//			    pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
	//			}
	//			else
	//			{}
	//		}
	//		else
	//		{
	//			break;
	//		}
	//	}
	//	//printf("\npBattTestLogInfo->aBattInfo[%d].bTemp = %d;\n",i,  pBattTestLogInfo->aBattInfo[i].bTemp);
	//}

	for(i = 0; i < MAX_NUM_SMBAT_OR_SMBRC; i++)
	{
	    for(k = 0; k < MAX_BLOCK_NUM; k++)
	    {
		pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);
	    }
	}

	for(i = 0; i < MAX_NUM_EIB_EQUIP; i++)
	{
		if(GC_EQUIP_EXIST == GC_GetEnumValue(TYPE_EIB_UNIT, i, EIB_EXIST_STATE))
		{
			for(j = 0; j < MAX_NUM_OF_BLK_PER_EIB; j++)
			{
				if(GC_IsEIBSigValid(TYPE_EIB_UNIT, i, EIB_BLOCK1_VOLTAGE + j))
				{
					pBattTestLogInfo->aEibInfo[i].abBlockVolt[j] = TRUE;
					pBattTestLogInfo->iSizeOfBattTestRcd += sizeof(float);
					//printf("EIBBattery %d Block %d is true\n",i,j);	

				}
				else
				{
					pBattTestLogInfo->aEibInfo[i].abBlockVolt[j] = FALSE;
					//printf("EIBBattery %d Block %d is false\n",i,j);	
				}


				pBattTestLogInfo->iMaxSizeOfBattTestRcd	+= sizeof(float);
			}
		}
		else
		{
			for(j = 0; j < MAX_NUM_OF_BLK_PER_EIB; j++)
			{
				pBattTestLogInfo->aEibInfo[i].abBlockVolt[j] = FALSE;
					
			}
		}
	}
#define MAX_SIZE_BATTERY_LOG (300*1024/80)

	pBattTestLogInfo->iMaxSizeOfBattTestRcd = MAX_SIZE_BATTERY_LOG;

	/*printf("**************TEMPERATURE*********************\n");
	for(i = 0; i < MAX_NUM_TEMPERATURE ; i++)
	{
	    printf("abTemp[%d] is %d\n", i, pBattTestLogInfo->abTemp[i]);
	}
	printf("**************Battery*********************\n");
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
	    printf("[%d] bCurr is %d	bCap is %d  bVolt is %d\n", i, 
		pBattTestLogInfo->aBattInfo[i].bCurr, pBattTestLogInfo->aBattInfo[i].bCap, pBattTestLogInfo->aBattInfo[i].bVolt);
	}
	printf("**************EIB*********************\n");
	for(i = 0; i < MAX_NUM_EIB_EQUIP; i++)
	{
	    for(j = 0; j < MAX_NUM_OF_BLK_PER_EIB; j++)
	    {
		printf("[%d][%d] abBlockVolt is %d\n", i, j, pBattTestLogInfo->aEibInfo[i].abBlockVolt[j]);
	    }
	}
	for(i = 0; i < MAX_NUM_SMBAT_OR_SMBRC; i++)
	{
	    for(j = 0; j < MAX_BLOCK_NUM; j++)
	    {
		printf("[%d][%d] abSMBlockVolt is %d\n", i, j, pBattTestLogInfo->aBlockInfo[i].abSMBlockVolt[j]);
	    }
	}*/

	//printf("pBattTestLogInfo->iSizeOfBattTestRcd is %d\n", pBattTestLogInfo->iSizeOfBattTestRcd);
	//printf("pBattTestLogInfo->iMaxSizeOfBattTestRcd is %d\n", pBattTestLogInfo->iMaxSizeOfBattTestRcd);

	return;
}

/*==========================================================================*
 * FUNCTION : TestLogInit
 * PURPOSE  : To initialize battery test log 
              data
 * CALLS    : NEW
 * CALLED BY: GC_BattMgmtInit
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-19 15:26
 *==========================================================================*/
void	TestLogInit(void)
{

	BattTestLogInfoInit();

	if(g_pGcData->RunInfo.Bt.TestLog.pTestRecord)
	{
		DELETE(g_pGcData->RunInfo.Bt.TestLog.pTestRecord); 
	}

	g_pGcData->RunInfo.Bt.TestLog.pTestRecord 
		= NEW(BYTE, 
		(((g_pGcData->EquipInfo.BattTestLogInfo.iSizeOfBattTestRcd  
			* MAX_RECORD_PER_TEST - 1) 
			/ SIZE_STORAGE_UNIT + 1) 
			* SIZE_STORAGE_UNIT));

	GC_ASSERT(g_pGcData->RunInfo.Bt.TestLog.pTestRecord,
				ERR_CTL_NO_MEMORY,
				"Failed to apply memery for pTestRecord!\n");
	

	/*iMaxRecords is maximum records number in storage interfcae*/
	g_pGcData->RunInfo.Bt.TestLog.iMaxRecords 
		= (g_pGcData->EquipInfo.BattTestLogInfo.iMaxSizeOfBattTestRcd 	//g_pGcData->EquipInfo.BattTestLogInfo.iSizeOfBattTestRcd 
				* MAX_RECORD_PER_TEST				//History data
				+ sizeof(BATT_TEST_LOG_INFO)		//Battery info
				+ 2 * SIZE_STORAGE_UNIT)			//head and tail
			* MAX_TEST_LOG 
			/ SIZE_STORAGE_UNIT 
			+ 1;
	
	//printf("g_pGcData->RunInfo.Bt.TestLog.iMaxRecords is %d\n", g_pGcData->RunInfo.Bt.TestLog.iMaxRecords);

	g_pGcData->RunInfo.Bt.TestLog.fWritefrequencyPerday
		= 1;

	return;
}



/*==========================================================================*
 * FUNCTION : GC_TestLogDestroy
 * PURPOSE  : To release the memory assigned in GC_InitBattData
 * CALLS    : DELETE
 * CALLED BY: GC_DestroyBattMgmt
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-08 14:55
 *==========================================================================*/
void	GC_TestLogDestroy(void)
{
	if(g_pGcData->RunInfo.Bt.TestLog.pTestRecord)
	{
		DELETE(g_pGcData->RunInfo.Bt.TestLog.pTestRecord); 
	}
	return;
}


/*==========================================================================*
 * FUNCTION : GC_TestLogStart
 * PURPOSE  : To provide a battery test log interface for a battery test start.
 * CALLS    : TestLogAddRecord
 * CALLED BY: 
 * ARGUMENTS: BYTE  iStartReason : 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-19 15:25
 *==========================================================================*/
void	GC_TestLogStart(int iStartReason)
{
	char*	aszTestStartReason[] =
	{
		"Planned Test",			//Start test for it is battery best plan time
		"Manual Test",			//Start test manually 
		"AC Fail Test",			//Start AC Fail test for AC fail
		"Master Test",			//Start test for Master Power start test

		"Other",
	};

	char	szBuffer[100];

	BT_LOG_SUMMARY	*pSummary = &(g_pGcData->RunInfo.Bt.TestLog.Summary);

	sprintf(szBuffer, 
			"Battery Test Start for %s.  \n",
			aszTestStartReason[iStartReason]);
	GC_LOG_OUT(APP_LOG_INFO, szBuffer);

	TestLogInit();

	/*pSummary->fBattRatedCap 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_RATED_CAP);*/
	pSummary->fBattRatedCap 
		= GC_GetFloatValue(TYPE_BATT_UNIT, 0, BT_PRI_RATED_CAP);


	pSummary->iStartReason = iStartReason;
	pSummary->tStartTime = g_pGcData->SecTimer.tTimeNow;
	pSummary->iEndReason = END_OTHER;
	pSummary->tEndTime = g_pGcData->SecTimer.tTimeNow;
	pSummary->iTestResult = RESULT_OTHER;
	pSummary->iQtyBatt = MAX_NUM_SOLUTION_BATT_EQUIP;//GC_Get_Total_BattNum();
	pSummary->iQtyRecord = 0;
	pSummary->iSizeRecord = g_pGcData->EquipInfo.BattTestLogInfo.iSizeOfBattTestRcd;

	g_pGcData->RunInfo.Bt.TestLog.iRecordNo = 0;

#ifdef	GEN_CTL_FOR_CHN
	{
		int		i;

		for(i = 0; i < GC_Get_Total_BattNum(); i++)
		{
			g_pGcData->RunInfo.Bt.afDisCap[i] = 0;
		}
	}
#endif

	TestLogAddRecord();

	g_pGcData->RunInfo.Bt.TestLog.fLastVolt 
			= g_pGcData->RunInfo.fSysVolt;

	return;
}



/*==========================================================================*
 * FUNCTION : GC_TestLogHandling
 * PURPOSE  : 
 * CALLS    : TestLogAddRecord
 * CALLED BY: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-20 12:10
 *==========================================================================*/
void	GC_TestLogHandling(void)
{
	if((ABS(g_pGcData->RunInfo.fSysVolt 
			- g_pGcData->RunInfo.Bt.TestLog.fLastVolt))
		> GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_TEST_THRESHOLD))
	{
		g_pGcData->RunInfo.Bt.TestLog.fLastVolt 
			= g_pGcData->RunInfo.fSysVolt;
		TestLogAddRecord();
	}
	return;
}


/*==========================================================================*
 * FUNCTION : GC_TestLogEnd
 * PURPOSE  : To provide a battery test log interface for a battery test end.
 * CALLS    : TestLogAddRecord, DAT_StorageCreate, DAT_StorageWriteRecord, 
              DAT_StorageClose
 * CALLED BY: 
 * ARGUMENTS: BYTE  iEndReason  : 
 *            BYTE  byTestRrsult : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-20 15:00
 *==========================================================================*/
void	GC_TestLogEnd(int iEndReason, int iTestResult)
{
	int		i, j, iIdxBuf;
	char*	aszEndReason[] = 
	{
		"Manual",				//Stop test manually 
		"Alarm",				//Stop test for alarm
		"Timeout",				//Stop test for test time-out
		"Capacity",				//Stop test for capacity condition
		"Voltage",				//Stop test for voltage condition
		"AC_Fail",				//Stop test for AC fail
		"AC_Restore",			//Stop AC fail test for AC restore
		"AC_FailTestDisabled",	//Stop AC fail test for disabled
		"MasterStop",			//Stop test for Master Power stop test
		"StateTurnTo_MAN",		//Stop a PowerSplit BT for Auto/Man turn to Man
		"StateTurnTo_AUTO",		//Stop PowerSplit Man-BT for Auto/Man turn to Auto
		"SystemLowLoad",		//Stop battery test if the system load less than 3A

		"Other",

	};
	char*	aszTestResult[] = 
	{
		"None",				//No test result
		"Normal",			//Battery is OK
		"BadBattery",		//Battery is bad
		"PowerSplitTest",	//It's a Power Split Test
		"Other",
	};

	BOOL	bRst;
	HANDLE	hTestLog;
	char	szBuffer[100];
	BYTE	byBuffer[4 * SIZE_STORAGE_UNIT] = "Head of a Battery Test";

	sprintf(szBuffer, 
			"Battery Test End for %s, test result is %s. \n",
			aszEndReason[iEndReason],
			aszTestResult[iTestResult]);
	GC_LOG_OUT(APP_LOG_INFO, szBuffer);	


	/*If test result is NONE, test log will not be stored, but...*/
	if(RESULT_NONE == iTestResult)
	{
		//If the test duration is over 10 minutes, the test log will be recorded.
		if((g_pGcData->SecTimer.tTimeNow - g_pGcData->RunInfo.Bt.TestLog.Summary.tStartTime) < 600)
		{
		    printf("RESULT_NONE == iTestResult\n");
			return;
		}
	}

	TestLogAddRecord();

	g_pGcData->RunInfo.Bt.TestLog.Summary.iEndReason = iEndReason;
	g_pGcData->RunInfo.Bt.TestLog.Summary.tEndTime 
		= g_pGcData->SecTimer.tTimeNow;
	g_pGcData->RunInfo.Bt.TestLog.Summary.iTestResult = iTestResult;
	g_pGcData->RunInfo.Bt.TestLog.Summary.iQtyRecord 
		= g_pGcData->RunInfo.Bt.TestLog.iRecordNo;

	hTestLog = DAT_StorageCreate(BATT_TEST_LOG_FILE, 
					SIZE_STORAGE_UNIT, 
					g_pGcData->RunInfo.Bt.TestLog.iMaxRecords, 
					g_pGcData->RunInfo.Bt.TestLog.fWritefrequencyPerday);

	printf("g_pGcData->RunInfo.Bt.TestLog.iMaxRecords = %d\n", g_pGcData->RunInfo.Bt.TestLog.iMaxRecords);


	ASSERT(hTestLog);
	if(!hTestLog)
	{
	    //printf("Failed to create test log His_data!\n");
		GC_LOG_OUT(APP_LOG_WARNING, "Failed to create test log His_data!\n");
		return;
	}
	

	memcpy(byBuffer, "Head of a Battery Test", 23);
	memcpy(byBuffer + 64, 
			&(g_pGcData->RunInfo.Bt.TestLog.Summary),
			sizeof(BT_LOG_SUMMARY));
	bRst = DAT_StorageWriteRecord(hTestLog, SIZE_STORAGE_UNIT, byBuffer);
	ASSERT(bRst);
	if(!bRst)
	{
	    //printf("Failed to write a test log record!\n");
		GC_LOG_OUT(APP_LOG_WARNING, "Failed to write a test log record!\n");
		return;
	}

	iIdxBuf = 0;

	for(i = 0; i < MAX_NUM_TEMPERATURE; i++)
	{
		byBuffer[iIdxBuf] = (BYTE)(g_pGcData->EquipInfo.BattTestLogInfo.abTemp[i]);
		iIdxBuf ++;
	}
	for(i = 0; i < MAX_NUM_SOLUTION_BATT_EQUIP; i++)
	{

		BATT_EQUIP_INFO	*pBattInfo = &(g_pGcData->EquipInfo.BattTestLogInfo.aBattInfo[i]);
		byBuffer[iIdxBuf + 0] = (BYTE)(pBattInfo->bCurr);
		byBuffer[iIdxBuf + 1] = (BYTE)(pBattInfo->bVolt);
		byBuffer[iIdxBuf + 2] = (BYTE)(pBattInfo->bCap);
		iIdxBuf += 3;
	}
	for(i = 0; i < MAX_NUM_SMBAT_OR_SMBRC; i++)
	{
	    for(j = 0; j < MAX_BLOCK_NUM; j++)
	    {
		byBuffer[iIdxBuf] = (BYTE)(g_pGcData->EquipInfo.BattTestLogInfo.aBlockInfo[i].abSMBlockVolt[j]);
		iIdxBuf++;
	    }
	}

	for(i = 0; i < MAX_NUM_EIB_EQUIP; i++)
	{
		EIB_EQUIP_INFO	*pEibInfo = &(g_pGcData->EquipInfo.BattTestLogInfo.aEibInfo[i]);
		for(j = 0; j < MAX_NUM_OF_BLK_PER_EIB; j++)
		{		
			byBuffer[iIdxBuf] = (BYTE)(pEibInfo->abBlockVolt[j]);
			iIdxBuf++;
		}
	}
	/*for(i = 0 ; i <250; i++)
	{
	
		printf("byBuffer[%d] = %d ",i,  byBuffer[i]);
		if(i % 5 == 0)
		{
			printf("\n");
		}
	}*/


	bRst = DAT_StorageWriteRecord(hTestLog, 
				(SIZE_STORAGE_UNIT * 4), 
				byBuffer);
	ASSERT(bRst);
	if(!bRst)
	{
	    printf("Failed to write a test log record!\n");
		GC_LOG_OUT(APP_LOG_WARNING, "Failed to write a test log record!\n");
		return;
	}
	bRst = DAT_StorageWriteRecord(hTestLog, 
				((DWORD)((g_pGcData->EquipInfo.BattTestLogInfo.iSizeOfBattTestRcd * 
						g_pGcData->RunInfo.Bt.TestLog.iRecordNo - 1) 
						/ SIZE_STORAGE_UNIT) + 1) * SIZE_STORAGE_UNIT, 
				g_pGcData->RunInfo.Bt.TestLog.pTestRecord);

	ASSERT(bRst);
	if(!bRst)
	{
	    printf("Failed to write a test log record!\n");
		GC_LOG_OUT(APP_LOG_WARNING, "Failed to write a test log record!\n");
		return;
	}

	memcpy(byBuffer, "Tail of a Battery Test", 23);
	memcpy(byBuffer + 64, 
			&(g_pGcData->RunInfo.Bt.TestLog.Summary),
			sizeof(BT_LOG_SUMMARY));
	bRst = DAT_StorageWriteRecord(hTestLog, 
				SIZE_STORAGE_UNIT,
				byBuffer);
	ASSERT(bRst);
	if(!bRst)
	{
	    printf("Failed to write a test log record!\n");
		GC_LOG_OUT(APP_LOG_WARNING, "Failed to write a test log record!\n");
		return;
	}

	bRst = DAT_StorageClose(hTestLog);
	ASSERT(bRst);
	if(!bRst)
	{
	    printf("Failed to close a test log!\n");
		GC_LOG_OUT(APP_LOG_WARNING, "Failed to close a test log!\n");
		return;
	}

	//printf("Success to log the test!\n");
	return;
}




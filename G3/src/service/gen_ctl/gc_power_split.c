/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_power_split.c
 *  CREATOR  : Frank Cao                DATE: 2004-12-21 13:47
 *  VERSION  : V1.00
 *  PURPOSE  : Power Split
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include	"gc_index_def.h"
#include	"gen_ctl.h"
#include	"gc_batt_cap.h"
#include	"gc_batt_mgmt.h"
#include	"gc_pri_cfg.h"
#include	"gc_sig_value.h"
#include	"gc_lvd.h"
#include	"gc_power_split.h"
#include	"gc_estop_eshutdown.h"

static SIG_ENUM GetMasterInput(DWORD iInputType);
static void PowerSplitStateMgmt(void);
static void PowerSplitFC(void);
static void PowerSplitBC(void);
static void PowerSplitBT(void);
static void PowerSplitAcFail(void);
static void PowerSplitManBT(void);
static void PowerSplitManBC(void);
static void PowerSplitBcStart(int iNextState, 
							const char* szLogOut);
static void PowerSplitBcEnd(int iNextState,
							const char* szLogOut);
static void PowerSplitBtStart(int iNextState, 
							const char* szLogOut,
							int iStartReason);
static void	PowerSplitBtEnd(int iNextState, 
							const char* szLogOut,
							int iEndReason);
static void PowerSplitFcCurrLmt(void);
static void PowerSplitBcCurrLmt(void);
static void PowerSplitCurrLmt(void);

/*==========================================================================*
 * FUNCTION : GetMasterInput
 * PURPOSE  : Get the status of inputs from Master Power
 * CALLS    : DxiGetData
 * CALLED BY: GC_PowerSplit
 * ARGUMENTS: SIG_ENUM  enumInputType : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 19:13
 *==========================================================================*/
static SIG_ENUM GetMasterInput(DWORD enumInputType)
{
	GC_SIG_INFO	*pInput;
	SIG_BASIC_VALUE			*pSigValue;
	int						iRst;
	int						iBufLen;

	pInput = g_pGcData->PriCfg.pPowerSplitInput + enumInputType;
	
	if(!(pInput->bEnabled))
	{
		return GC_MASTER_INPUT_INVALID;
	}

	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					pInput->iEquipId,
					DXI_MERGE_SIG_ID(pInput->iSignalType, 
									pInput->iSignalId),
					&iBufLen,
					&pSigValue,
					0);
	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_GET_VLAUE, 
			"Get Master Input error!\n");

	if(!(SIG_VALUE_IS_VALID(pSigValue)))
	{
		return GC_MASTER_INPUT_INVALID;
	}

	return pSigValue->varValue.enumValue;
}



/*==========================================================================*
 * FUNCTION : PowerSplitStateMgmt
 * PURPOSE  : Power Split state management
 * CALLS    : GC_GetEnumValue PowerSplitFC PowerSplitAcFail PowerSplitManBT
              PowerSplitBT PowerSplitManBC PowerSplitBC
 * CALLED BY: GC_PowerSplit
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 19:15
 *==========================================================================*/
static void PowerSplitStateMgmt(void)
{
	BYTE	byState;

	byState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	switch (byState)
	{
		case ST_FLOAT:
			PowerSplitFC();
			break;

		case ST_AC_FAIL:
			PowerSplitAcFail();
			break;

		case ST_MAN_TEST:
			PowerSplitManBT();
			break;

		case ST_POWER_SPLIT_BT:
			PowerSplitBT();
			break;

		case ST_MAN_BC:
			PowerSplitManBC();
			break;

		case ST_POWER_SPLIT_BC:
			PowerSplitBC();
			break;

		default:
			GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE, 
						ST_FLOAT, 
						TRUE);
			break;
	}
	return;
}


/*==========================================================================*
 * FUNCTION : PowerSplitFC
 * PURPOSE  : Handling of Float Charge state
 * CALLS    : GC_SetEnumValue PowerSplitBcStart PowerSplitBtStart 
              GC_GetStateOfSecTimer GC_SuspendSecTimer GC_RectVoltCtl
              PowerSplitFcCurrLmt
 * CALLED BY: PowerSplitStateMgmt
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 19:17
 *==========================================================================*/
static void PowerSplitFC(void)
{
	if(g_pGcData->RunInfo.Rt.bAcFail)
	//All of rectifiers AC failure
	{
		//Battery Management State
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						ST_AC_FAIL,
						TRUE);	
	
		//Event Log
		GC_LOG_OUT(APP_LOG_INFO, "AC failure, turn to AC_Fail.\n");
		return;
	}

	

	if(STATE_AUTO != g_pGcData->RunInfo.enumAutoMan)
	//Manually
	{
		//AC not fail
		//Manually turn to boost charge state
		if(CTL_BOOST 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BOOST_CTL))
		{
			PowerSplitBcStart(ST_MAN_BC,
					"Manual BC, turn to Manual BC.\n");
			return;
		}

		//Manually start battery test			
		if(CTL_TEST_START 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
		{
			PowerSplitBtStart(ST_MAN_TEST,
					"Manual Test, turn to Manual Test.\n",
					START_MANUAL_TEST);
			return;
		}
		return;
	}

	//Automatically
	if(GC_MASTER_INPUT_ALARM == GetMasterInput(GC_MASTER_TEST))
	{
		PowerSplitBtStart(ST_POWER_SPLIT_BT,
			"Master Power Test, turn to Power Split BT.\n",
			START_MASTER_TEST);
		return;		
	}

	if(GC_MASTER_INPUT_ALARM == GetMasterInput(GC_MASTER_BOOST_CHARGE))
	{
		PowerSplitBcStart(ST_POWER_SPLIT_BC,
			"Master Power BC, turn to Power Split BC.\n");
		return;		
	}
	
	GC_RectVoltCtl(TYPE_POWER_SPLIT, FALSE);

	PowerSplitFcCurrLmt();

	return;
}




/*==========================================================================*
 * FUNCTION : PowerSplitBC
 * PURPOSE  : Handling of PowerSplitBC State
 * CALLS    : PowerSplitBcEnd GC_GetEnumValue PowerSplitBcEnd
              PowerSplitBtStart GC_RectVoltCtl PowerSplitBcCurrLmt
 * CALLED BY: PowerSplitStateMgmt
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:02
 *==========================================================================*/
static void PowerSplitBC(void)
{
	if(g_pGcData->RunInfo.Rt.bAcFail)
	//All of rectifiers AC failure
	{
		PowerSplitBcEnd(ST_AC_FAIL,
			"AC failure, turn to AC_Fail.\n");
		return;
	}


	//Manually start battery test			
	if(CTL_TEST_START 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
	{
		PowerSplitBcEnd(ST_MAN_TEST,
				NULL);
		PowerSplitBtStart(ST_MAN_TEST,
				"Manual Test, turn to Manual Test.\n",
				START_MANUAL_TEST);
		return;
	}
	
	
	if(STATE_AUTO != g_pGcData->RunInfo.enumAutoMan)
	//Manually
	{
		/*GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);*/

		PowerSplitBcEnd(ST_FLOAT,
			"Autoturn to Man, turn to FC.\n");
		return;
	}

	//Automatically
	if(GC_MASTER_INPUT_ALARM == GetMasterInput(GC_MASTER_TEST))
	{
		PowerSplitBcEnd(ST_POWER_SPLIT_BT,
				NULL);
		PowerSplitBtStart(ST_POWER_SPLIT_BT,
			"Master Power Test, turn to Power Split BT.\n",
			START_MASTER_TEST);
		return;		
	}

	if(GC_MASTER_INPUT_NORMAL == GetMasterInput(GC_MASTER_BOOST_CHARGE))
	{
		PowerSplitBcEnd(ST_FLOAT,
			"Master Power FC, turn to FC.\n");
		return;		
	}

	GC_RectVoltCtl(TYPE_BOOST_VOLT, FALSE);

	PowerSplitBcCurrLmt();

	return;

}


/*==========================================================================*
 * FUNCTION : PowerSplitBT
 * PURPOSE  : Handling of PowerSplitBT State
 * CALLS    : PowerSplitBtEnd GC_RectVoltCtl
 * CALLED BY: PowerSplitStateMgmt
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:04
 *==========================================================================*/
static void PowerSplitBT(void)
{
	if(g_pGcData->RunInfo.Rt.bAcFail)
	//All of rectifiers AC failure
	{
		PowerSplitBtEnd(ST_AC_FAIL, 
			"AC failure, turn to AC_Fail State.\n",
			END_AC_FAIL);
		return;
	}


	if(STATE_AUTO != g_pGcData->RunInfo.enumAutoMan)
	//Manually
	{
		PowerSplitBtEnd(ST_FLOAT, 
			"Auto turn to Man, turn to FC.\n",
			END_TURN_TO_MAN);
		return;
	}

	if(GC_MASTER_INPUT_NORMAL == GetMasterInput(GC_MASTER_TEST))
	{
		PowerSplitBtEnd(ST_FLOAT,
			"Master Power test stop, turn to FC.\n",
			END_MASTER_STOP);
		return;		
	}

	GC_RectVoltCtl(TYPE_TEST_VOLT, FALSE);

	GC_TestLogHandling();

	return;
}



/*==========================================================================*
 * FUNCTION : PowerSplitAcFail
 * PURPOSE  : Handling of PowerSplitAcFail State
 * CALLS    : GC_SetEnumValue GC_LOG_OUT
 * CALLED BY: PowerSplitStateMgmt
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:06
 *==========================================================================*/
static void PowerSplitAcFail(void)
{
	if(!(g_pGcData->RunInfo.Rt.bAcFail))
	//AC Restore
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						ST_FLOAT,
						TRUE);
		
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BOOST_CTL,
						CTL_FLOAT,
						TRUE);

		GC_LOG_OUT(APP_LOG_INFO, "AC Power restore, turn to FC.\n");
	}

	return;
}



/*==========================================================================*
 * FUNCTION : PowerSplitManBT
 * PURPOSE  : Handling of PowerSplitManBT State
 * CALLS    : PowerSplitBtEnd GC_GetEnumValue
 * CALLED BY: PowerSplitStateMgmt
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:07
 *==========================================================================*/
static void PowerSplitManBT(void)
{
	if(g_pGcData->RunInfo.Rt.bAcFail)
	//All of rectifiers AC failure
	{
		PowerSplitBtEnd(ST_AC_FAIL, 
			"AC failure, turn to AC Fail.\n",
			END_AC_FAIL);
		return;
	}

	//Manually start battery test			
	if(CTL_TEST_STOP 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
	{
		PowerSplitBtEnd(ST_FLOAT,
				"Manually stop, turn to FC.\n",
				END_MANUAL);
		return;
	}

	if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
	//Automatically
	{
		PowerSplitBtEnd(ST_FLOAT, 
			"Auto/Man turn to Auto, State turn to FC!\n",
			END_TURN_TO_AUTO);
		return;
	}

	GC_TestLogHandling();

	return;
}


/*==========================================================================*
 * FUNCTION : PowerSplitManBC
 * PURPOSE  : Handling of PowerSplitManBC State
 * CALLS    : GC_GetEnumValue
 * CALLED BY: PowerSplitStateMgmt
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:08
 *==========================================================================*/
static void PowerSplitManBC(void)
{
	if(g_pGcData->RunInfo.Rt.bAcFail)
	//All of rectifiers AC failure
	{
		PowerSplitBcEnd(ST_AC_FAIL,
			"AC failure, turn to AC Fail.\n");
		return;
	}

	//Manually Float Charge			
	if(CTL_FLOAT 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BOOST_CTL))
	{
		PowerSplitBcEnd(ST_FLOAT,
				"Manually FC, turn to FC.\n");
		return;
	}

	//Manually Battery Test			
	if(CTL_TEST_START 
		== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
	{
		PowerSplitBcEnd(ST_MAN_TEST, NULL);
		PowerSplitBtStart(ST_MAN_TEST, 
			"Manually Battery Test, turn to Manual BT.\n",
			START_MANUAL_TEST);
		return;
	}

	if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
	//Automatically
	{
		PowerSplitBcEnd(ST_FLOAT, 
			"Man turn to Auto, turn to FC.\n");
		return;
	}

	return;
}


/*==========================================================================*
 * FUNCTION : PowerSplitBcStart
 * PURPOSE  : Handling of Power Slpit Boost Charge start
 * CALLS    : GC_RectVoltCtl GC_SetEnumValue GC_SetEnumValue
 * CALLED BY: PowerSplitFC
 * ARGUMENTS: int          iNextState : 
 *            const char*  szLogOut   : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:09
 *==========================================================================*/
static void PowerSplitBcStart(int iNextState, 
					const char* szLogOut)
{

	//Control rectifiers volt to Boost Voltage
	GC_RectVoltCtl(TYPE_BOOST_VOLT, FALSE);

	//Manual Control Signal handling
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BOOST_CTL,
					CTL_BOOST,
					TRUE);	

	//Set Battery Management State
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BM_STATE,
					iNextState,
					TRUE);	
	
	//Event Log
	if(szLogOut)
	{
		GC_LOG_OUT(APP_LOG_INFO, szLogOut);
	}

	//Alarm Handling
	//switch (iNextState)
	//{
	//case ST_MAN_BC:
	//	GC_SetEnumValue(TYPE_OTHER, 
	//					0, 
	//					BT_PUB_MAN_BC_SET,
	//					GC_ALARM,
	//					TRUE);
	//	break;

	//case ST_POWER_SPLIT_BC:
	//	GC_SetEnumValue(TYPE_OTHER, 
	//					0, 
	//					BT_PUB_MASTER_BC_SET,
	//					GC_ALARM,
	//					TRUE);
	//	break;

	//default:
	//	/*ASSERT(FALSE);*/
	//	break;

	//}
	return;
}


/*==========================================================================*
 * FUNCTION : PowerSplitBcEnd
 * PURPOSE  : Handling of Power Slpit Boost Charge end
 * CALLS    : GC_RectVoltCtl GC_SetEnumValue GC_GetEnumValue
 * CALLED BY: PowerSplitBC PowerSplitManBC
 * ARGUMENTS: int          iNextState : 
 *            const char*  szLogOut   : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:12
 *==========================================================================*/
static void PowerSplitBcEnd(int iNextState,
						   const char* szLogOut)
{

	//Control rectifiers volt to Float Voltage
	GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);

	//Manual Control Signal handling
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BOOST_CTL,
					CTL_FLOAT,
					TRUE);		

	//Battery Management State
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BM_STATE,
					iNextState,
					TRUE);	
	
	//Event Log
	if(szLogOut)
	{
		GC_LOG_OUT(APP_LOG_INFO, szLogOut);
	}

	//Alarm Handling
	/*if(GC_NORMAL
		!= GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_MASTER_BC_SET))
	{
		GC_SetEnumValue(TYPE_OTHER,
						0, 
						BT_PUB_MASTER_BC_SET, 
						GC_NORMAL,
						TRUE);
	}
	
	if(GC_NORMAL 
		!= GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_MAN_BC_SET))
	{
		GC_SetEnumValue(TYPE_OTHER,
						0, 
						BT_PUB_MAN_BC_SET, 
						GC_NORMAL,
						TRUE);
	}*/

	return;
}

/*==========================================================================*
 * FUNCTION : PowerSplitBtStart
 * PURPOSE  : Handling on start of a power split battery test
 * CALLS    : GC_RectVoltCtl GC_SetEnumValue GC_TestLogStart
 * CALLED BY: PowerSplitFC PowerSplitBC 
 * ARGUMENTS: int          iNextState   : 
 *            const char*  szLogOut     : 
 *            int          iStartReason : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:13
 *==========================================================================*/
static void PowerSplitBtStart(int iNextState, 
					const char* szLogOut,
					int iStartReason)
{
	//Control rectifiers volt to Test Voltage
	GC_RectVoltCtl(TYPE_TEST_VOLT, FALSE);

	//Modify the Battery Management State to iNextState
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BM_STATE,
					iNextState,
					TRUE);

	//Set BT_PUB_TEST_CTL Signal to CTL_TEST_START
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_TEST_CTL,
					CTL_TEST_START,
					TRUE);

	//Set Battery Test Alarm Value
	//switch (iNextState)
	//{
	//case ST_MAN_TEST:
	//	GC_SetEnumValue(TYPE_OTHER, 
	//				0, 
	//				BT_PUB_MAN_TEST_SET,
	//				GC_ALARM,
	//				TRUE);
	//	break;

	//case ST_POWER_SPLIT_BT:
	//	GC_SetEnumValue(TYPE_OTHER, 
	//					0, 
	//					BT_PUB_MASTER_BT_SET,
	//					GC_ALARM,
	//					TRUE);
	//	break;

	//default:
	//	/*ASSERT(FALSE);*/
	//	break;

	//}

	//Event Log
	if(szLogOut)
	{
		GC_LOG_OUT(APP_LOG_INFO, szLogOut);
	}

	//Battery Test Log
	GC_TestLogStart(iStartReason);

	return;
}



/*==========================================================================*
 * FUNCTION : PowerSplitBtEnd
 * PURPOSE  : Handling on end of a power split battery test 
 * CALLS    : GC_SetEnumValue GC_RectVoltCtl GC_TestLogEnd GC_GetEnumValue
 * CALLED BY: PowerSplitBT  PowerSplitManBT
 * ARGUMENTS: int          iNextState : 
 *            const char*  szLogOut   : 
 *            int          iEndReason : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:14
 *==========================================================================*/
static void	PowerSplitBtEnd(int iNextState, 
					const char* szLogOut,
					int iEndReason)
{
	//Modify the Battery Management State
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_BM_STATE,
					iNextState,
					TRUE);

	//Set Manual Control Signal
	if(ST_FLOAT == iNextState)
	{
		//Control rectifiers volt to PowerSplit Voltage
		GC_RectVoltCtl(TYPE_POWER_SPLIT, FALSE);

		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BOOST_CTL,
						CTL_FLOAT,
						TRUE);	
	}

	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_TEST_CTL,
					CTL_TEST_STOP,
					TRUE);
	
	//Event Log
	if(szLogOut)
	{
		GC_LOG_OUT(APP_LOG_INFO, szLogOut);
	}
	
	//Test Log
	GC_TestLogEnd(iEndReason, RESULT_POWER_SPLIT);

	//Alarm Handling
	/*if(GC_NORMAL 
		!= GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_MASTER_BT_SET))
	{
		GC_SetEnumValue(TYPE_OTHER,
						0, 
						BT_PUB_MASTER_BT_SET, 
						GC_NORMAL,
						TRUE);
	}

	if(GC_NORMAL 
		!=GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_MAN_TEST_SET))
	{
		GC_SetEnumValue(TYPE_OTHER,
						0, 
						BT_PUB_MAN_TEST_SET, 
						GC_NORMAL,
						TRUE);
	}*/

	return;
}


/*==========================================================================*
 * FUNCTION : PowerSplitCurrLmt
 * PURPOSE  : Current limitation on Power Split float charge state
 * CALLS    : GC_GetFloatValue GC_SetFloatValue
 * CALLED BY: PowerSplitFC
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:15
 *==========================================================================*/
static void PowerSplitCurrLmt(void)
{
	BOOL	bInDeadErea = FALSE;
	/*float	fBtRatedCap 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_RATED_CAP);*/
	float	fRtRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	float	fRtLmtForSlv 
		= GC_GetFloatValue(TYPE_OTHER, 0, SPLIT_PUB_SLV_CURR_LMT);	

	if(!(g_pGcData->RunInfo.Rt.iQtyOnWorkRect))
	{
		return;
	}

	//If undervoltage, then try to relax 5 percent
	if(GC_IsUnderVolt())
	{
		g_pGcData->RunInfo.Rt.fCurrLmt += 5;
	}
	else
	{
		float		fMinVolt;
		float		fDeltaLmt;

		if((g_pGcData->RunInfo.Rt.fCurrLmt 
			* g_pGcData->RunInfo.Rt.iQtyOnWorkRect 
			* fRtRatedCurr / 100)
			> g_pGcData->RunInfo.Rt.fSumOfCurr * 1.05)
		//This section is for find the point quickly
		{
			g_pGcData->RunInfo.Rt.fCurrLmt 
				= 100.0 * g_pGcData->RunInfo.Rt.fSumOfCurr 
					/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect
					* fRtRatedCurr);
		}

		fMinVolt = GC_GetSettingVolt(BT_PUB_NOM_VOLT) 
			- GC_GetFloatValue(TYPE_OTHER, 0, SPLIT_PUB_DELTA_VOLT);

		if((g_pGcData->RunInfo.fSysVolt > (fMinVolt - 0.04))
			&& (g_pGcData->RunInfo.fSysVolt < (fMinVolt + 0.04)))
		{
			fDeltaLmt = 0.0;
			bInDeadErea = TRUE;
		}
		else
		{
			fDeltaLmt = ((g_pGcData->RunInfo.fLastVolt 
				- g_pGcData->RunInfo.fSysVolt) / 100
				+ (fMinVolt - g_pGcData->RunInfo.fSysVolt) 
				* GC_INTERVAL_BATT_MGMT 
				/ GC_GetDwordValue(TYPE_OTHER, 0, SPLIT_PUB_INTERGRAL_TIME)) 
				* GC_GetFloatValue(TYPE_OTHER, 0, SPLIT_PUB_PROP_COEF);
			
			if((fDeltaLmt < 0.8) && (fDeltaLmt > -0.8))
			{
				fDeltaLmt = fDeltaLmt * 0.3;
				if((fDeltaLmt < 0.1) && (fDeltaLmt > 0))
				{
					fDeltaLmt = 0.1;
				}
				if((fDeltaLmt > -0.1) && (fDeltaLmt < 0))
				{
					fDeltaLmt = -0.1;
				}
			}

			if(fDeltaLmt > 5.0)
			{
				fDeltaLmt = 5.0;
			}	
			
			if(fDeltaLmt < -5.0)
			{
				fDeltaLmt = -5.0;
			}
		}

		g_pGcData->RunInfo.Rt.fCurrLmt += fDeltaLmt;

		if(g_pGcData->RunInfo.Rt.fCurrLmt < fRtLmtForSlv)
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = fRtLmtForSlv;
		}
	}

	if(g_pGcData->RunInfo.Rt.fCurrLmt > GC_MAX_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
	}

	if(g_pGcData->RunInfo.Rt.fCurrLmt < GC_MIN_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MIN_CURR_LMT;
	}
	
	//TRACEX("Control retifiers current limit, Value: %f \n",
	//		g_pGcData->RunInfo.Rt.fCurrLmt);

	if(!bInDeadErea)
	{
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF,
					g_pGcData->RunInfo.Rt.fCurrLmt);
	}

	g_pGcData->RunInfo.fLastVolt = g_pGcData->RunInfo.fSysVolt;

	return;
}

/*==========================================================================*
 * FUNCTION : PowerSplitFcCurrLmt
 * PURPOSE  : Current limitation on Power Split float charge state
 * CALLS    : GC_GetFloatValue GC_SetFloatValue
 * CALLED BY: PowerSplitFC
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:15
 *==========================================================================*/
static void PowerSplitFcCurrLmt(void)
{
	int		i;
	float	fBtRatedCap; 
		
	float	fRtLmtForSlv 
		= GC_GetFloatValue(TYPE_OTHER, 0, SPLIT_PUB_SLV_CURR_LMT);
	
	// If battery is charging, discharging or rect current limit is less than
	// CurrLmtForSlv, bBattNormal shall be FALSE
	BOOL	bBattNormal = TRUE;
		
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		fBtRatedCap
			= GC_GetFloatValue(TYPE_BATT_UNIT, i ,BT_PRI_RATED_CAP);

		if(g_pGcData->RunInfo.Bt.abValid[i])
		{
			float	fMinCurr = 0.02 * fBtRatedCap;
			if((g_pGcData->RunInfo.Bt.afBattCurr[i] > fMinCurr)
				|| (g_pGcData->RunInfo.Bt.afBattCurr[i] < (-fMinCurr)))
			{
				bBattNormal = FALSE;
			}
		}
	}

	if(g_pGcData->RunInfo.Rt.fCurrLmt 
		< fRtLmtForSlv)
	{
		bBattNormal = FALSE;
	}

	//bBattNormal is TRUE, that means the Slave is in High Load state, GC shall 
	//make System Voltage near 53.1V by adjusting rectifers current limitation 
	if(bBattNormal)
	{
		PowerSplitCurrLmt();
	}
	else
	{
		GC_CurrentLmt();
	}

	return;
}


/*==========================================================================*
 * FUNCTION : PowerSplitBcCurrLmt
 * PURPOSE  : Current limitation on Power Split boost charge state
 * CALLS    : GC_GetFloatValue GC_SetFloatValue
 * CALLED BY: PowerSplitBC
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 20:15
 *==========================================================================*/
static void PowerSplitBcCurrLmt(void)
{
	GC_CurrentLmt();
	return;
}

/*==========================================================================*
 * FUNCTION : GC_PowerSplitInit
 * PURPOSE  : To initialize the Power Split
 * CALLS    : GC_GetSysVolt
 * CALLED BY: InitGC
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-30 16:38
 *==========================================================================*/
void GC_PowerSplitInit(void)
{
	if(g_pGcData->PriCfg.bSlaveMode)
	{
		float	fCurrLmt = GC_GetFloatValue(TYPE_OTHER, 
											0,
											BT_PUB_CURR_LMT);
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE, ST_FLOAT, TRUE);
		GC_SetFloatValue(TYPE_OTHER, 
						0,
						BT_PUB_EXP_CURR_LMT,
						fCurrLmt);

		g_pGcData->RunInfo.fLastVolt = GC_GetSysVolt();
	}
	return;
}


/*==========================================================================*
 * FUNCTION : PowerSplitSwitchOnAllRect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-09-30 10:48
 *==========================================================================*/
static void PowerSplitSwitchOnAllRect(void)
{
	// controlled by SwitchOnAllRect() in gc_rect_redund.c
	/*if(GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_ENB))
	{
		return;
	}*/	

	if(g_pGcData->RunInfo.Rt.bRectHVSD)
	{
		return;
	}

	int		i;
	if(g_pGcData->RunInfo.Rt.iQtyAcOffRect)
	{
		char	strOut[256];
		/*int		iExpectedQtyOfRect 
					= g_pGcData->RunInfo.Rt.iQtyOnWorkRect 
						+ g_pGcData->RunInfo.Rt.iQtyAcOffRect;*/
		
		sprintf(strOut, 
				"Switch-on all of rectifers. QtyOnWorkRect = %d, QtyAcOffRect = %d.\n", 
				g_pGcData->RunInfo.Rt.iQtyOnWorkRect,
				g_pGcData->RunInfo.Rt.iQtyAcOffRect);
		GC_LOG_OUT(APP_LOG_INFO, strOut);

		for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
		{
			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							RT_PUB_AC_ON_OFF_CTL_SELF + i,
							GC_RECT_ON,
							TRUE);

			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							RT_PUB_DC_ON_OFF_CTL_SELF + i,
							GC_RECT_ON,
							TRUE);
		}

	}
	return;
}


#define	MAX_NUM_POWER_SPLIT_LVD		3

/*==========================================================================*
 * FUNCTION : PowerSplitLvd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-09-30 10:50
 *==========================================================================*/
static void PowerSplitLvd(void)
{
	int			i;

	SIG_ENUM	sigPowerSplitLVDType = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_POWERSPLIT_MODE);
#define POWERSPLIT_LVD_MASTER			0
#define POWERSPLIT_LVD_SLAVE			1


	if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
	//Automatically
	{
		/*Start For LVD Master mode*/

		//if(sigPowerSplitLVDType == POWERSPLIT_LVD_MASTER)
		if(sigPowerSplitLVDType == POWERSPLIT_LVD_SLAVE)
		{
			GC_LvdRun();
			return;
		}

		for(i = 0; i < MAX_NUM_POWER_SPLIT_LVD; i++)
		{
			int		iLvdEquipNo = i / 2;
			int		iLvdNo = i % 2;

			if(g_pGcData->RunInfo.Lvd.abValid[i])
			{
				SIG_ENUM		enumMasterInput;

				enumMasterInput = GetMasterInput((DWORD)(GC_MASTER_LVD1 + i));
				
				/*if(GC_MASTER_INPUT_INVALID == enumMasterInput)
				{
					continue;
				}*/

				if(GC_ALARM == enumMasterInput)
				{
					if(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i])
					{
						char	strOut[512];
						GC_SetEnumValue(TYPE_LVD_UNIT, 
									iLvdEquipNo, 
									iLvdNo + LVD1_PRI_CTL, 
									GC_DISCONNECTED, 
									TRUE);

						sprintf(strOut,
								"LVD%d disconnect for Power Split, AC state is %s. DC voltage is %.2f.\n",
								i + 1,
								(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
								g_pGcData->RunInfo.fSysVolt);

						GC_LOG_OUT(APP_LOG_INFO, strOut);
					}
				}		
				else
				{

					if(GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i])
					{
						char	strOut[512];
						GC_SetEnumValue(TYPE_LVD_UNIT, 
									iLvdEquipNo, 
									iLvdNo + LVD1_PRI_CTL, 
									GC_CONNECTED, 
									TRUE);

						sprintf(strOut,
								"LVD%d re-connect for Power Split, AC state is %s. DC voltage is %.2f.\n",
								i + 1,
								(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
								g_pGcData->RunInfo.fSysVolt);

						GC_LOG_OUT(APP_LOG_INFO, strOut);

					}
				}
			}
		}
	}
	return;
}





/*==========================================================================*
 * FUNCTION : GC_PowerSplit
 * PURPOSE  : interface function
 * CALLS    : GC_GetEnumValue GetMasterInput DxiSetData PowerSplitStateMgmt
              GC_BattCapCalc
 * CALLED BY: ServiceMain
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-27 19:10
 *==========================================================================*/
void GC_PowerSplit(void)
{
	PowerSplitSwitchOnAllRect();

	PowerSplitLvd();

	if((g_pGcData->RunInfo.Rt.bValid) 
		&& (BOOL)(GC_GetDwordValue(TYPE_OTHER,
								0,
								RT_PUB_COMM_QTY)))
	{
		PowerSplitStateMgmt();
	}
	else
	{
		if(ST_FLOAT != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		{
			GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE, ST_FLOAT, TRUE);
			GC_LOG_OUT(APP_LOG_WARNING, "No valid retifier, turn to FC.\n");
		}
	}

	GC_BattCapCalc();

	return;
}





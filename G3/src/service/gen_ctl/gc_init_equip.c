/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_init.c
 *  CREATOR  : Frank Cao                DATE: 2004-09-09 14:51
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "gc_index_def.h"
#include "gc_init_equip.h"

#include "gen_ctl.h"

#include "pub_list.h"
#include "gc_rect_list.h"
#include "gc_sig_value.h"

static void ACInfoInit(void);
/*==========================================================================*
 * FUNCTION : GC_EquipInfoInit
 * PURPOSE  : Get Equipment Information by DxiGetData(), and initialize the
              g_pGcData->EquipInfo
 * CALLS    : DxiGetData
 * CALLED BY: InitGC
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-17 09:26
 *==========================================================================*/
void	GC_EquipInfoInit()
{
	int			i, iIdx;
	int			iRst;
	int			iBufLen;
	EQUIP_INFO	*pEquipList;

	//Get Quantity of all of equipments
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM, 
					0, 
					0, 
					&iBufLen, 
					&(g_pGcData->EquipInfo.iQtyOfEquip), 
					0);
	GC_ASSERT((iRst == ERR_OK),
			ERR_CTL_GET_EQUIP_NUM,
			"DxiGetData VAR_ACU_EQUIPS_NUM error!\n");

	//Get point of Equipment List
	iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
					0, 
					0, 
					&iBufLen, 
					&pEquipList, 
					0);
	GC_ASSERT((iRst == ERR_OK),
			ERR_CTL_GET_EQUIP_LIST,
			"DxiGetData VAR_ACU_EQUIPS_LIST error!\n");

	//g_pGcData->EquipInfo.bCfgAc = FALSE;
	g_pGcData->EquipInfo.iCfgQtyOfBatt = 0;
	g_pGcData->EquipInfo.iCfgQtyOfRect = 0;
	g_pGcData->EquipInfo.iCfgQtyOfS1Rect = 0;
	g_pGcData->EquipInfo.iCfgQtyOfS2Rect = 0;
	g_pGcData->EquipInfo.iCfgQtyOfS3Rect = 0;
	g_pGcData->EquipInfo.iCfgQtyOfLvd = 0;
	g_pGcData->EquipInfo.iCfgQtyOfBattFuse = 0;
	// ��ʼ��SMBRC��SMBAT�������
	
	g_pGcData->EquipInfo.iCfgQtyOfComBAT = 0;
	g_pGcData->EquipInfo.iCfgQtyOfEIBBAT1 = 0;
	g_pGcData->EquipInfo.iCfgQtyOfSmduBAT1 = 0;
	g_pGcData->EquipInfo.iCfgQtyOfEIBBAT2 = 0;
	g_pGcData->EquipInfo.iCfgQtyOfSmduBAT2 = 0;
	g_pGcData->EquipInfo.iCfgQtyOfLargeDUBAT = 0;
	g_pGcData->EquipInfo.iCfgQtyOfSMBAT = 0;
	g_pGcData->EquipInfo.iCfgQtyOfSMBRC = 0;
	//initial Li Batt num
	g_pGcData->EquipInfo.iCfgQtyOfLiBatt = 0;
	g_pGcData->EquipInfo.iCfgQtyOfCt = 0;
#ifdef GC_SUPPORT_MPPT
	g_pGcData->EquipInfo.iCfgQtyOfMppt = 0;
#endif
	g_pGcData->EquipInfo.iCfgQtyOfSMDUEBAT= 0;

	//ȷ��SOLUTION�����˶��ٸ��豸
	
	//Scan Equipment List, to initialize g_pGcData->EquipInfo
	for(i = 0; i < g_pGcData->EquipInfo.iQtyOfEquip; i++)
	{
		int	iMajorId = GET_EQUIP_TYPE_ID_MAJOR((pEquipList + i)->iEquipTypeID);
		int	iSubId = GET_EQUIP_TYPE_ID_SUB((pEquipList + i)->iEquipTypeID);

		if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdRect))//g_pGcData->PriCfg.iEquipTypeIdRect=200,See gen_ctl.cfg [RECT_EQUIP_TYPE_ID]
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfRect;
			g_pGcData->EquipInfo.aiEquipIdRtPri[iIdx]
					= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfRect++;		
		}
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdBatt))
			&& (iSubId != 0))// && (iSubId != 5))//Exclude largedu Battery for V2
		{
#define EQUIP_TYPEID_LIGROUP	308  //ע������ų���Li��
#define EQUIP_TYPEID_LIBATT	309	
#define EQUIP_TYPEID_SMBAT	304
#define EQUIP_TYPEID_SMBRC	306
#define EQUIP_TYPEID_BATT	301
#define EQUIP_TYPEID_LARGEDUBATT    305
#define EQUIP_TYPEID_FIAMMBATTERY 310
			if((pEquipList + i)->iEquipTypeID == EQUIP_TYPEID_LIGROUP)
			{
				//�ų���
			}
			else if((pEquipList + i)->iEquipTypeID == EQUIP_TYPEID_FIAMMBATTERY)
			{
				//�ų���
			}
			else
			{
				iIdx = g_pGcData->EquipInfo.iCfgQtyOfBatt;
				g_pGcData->EquipInfo.aiEquipIdBtPri[iIdx]
						= (pEquipList + i)->iEquipID;
				g_pGcData->EquipInfo.iCfgQtyOfBatt++;	
				g_pGcData->RunInfo.Bt.bIsA_LiBatt[iIdx] = FALSE; //�����жϵ�������Ƿ�ΪLi���

				g_pGcData->RunInfo.Bt.iEquipTypeOfBatt[iIdx]= (pEquipList + i)->iEquipTypeID;//�����жϵ�ش洢���򣬽����loadallEquip.run��ɾ��ĵ����������

				if(((pEquipList + i)->iEquipID >= 118) && ((pEquipList + i)->iEquipID <= 125))
				{
				    g_pGcData->EquipInfo.iCfgQtyOfEIBBAT1++;
				}
				if(((pEquipList + i)->iEquipID >= 126) && ((pEquipList + i)->iEquipID <= 157))
				{
				    g_pGcData->EquipInfo.iCfgQtyOfSmduBAT1++;
				}
				if(((pEquipList + i)->iEquipID >= 206) && ((pEquipList + i)->iEquipID <= 209))
				{
				    g_pGcData->EquipInfo.iCfgQtyOfEIBBAT2++;
				}
				if(((pEquipList + i)->iEquipID >= 659) && ((pEquipList + i)->iEquipID <= 666))
				{
				    g_pGcData->EquipInfo.iCfgQtyOfSmduBAT2++;
				}
				if(((pEquipList + i)->iEquipID >= 3000) && ((pEquipList + i)->iEquipID <= 3019))
				{
				    g_pGcData->EquipInfo.iCfgQtyOfSMDUEBAT++;
				}
				if((pEquipList + i)->iEquipTypeID == EQUIP_TYPEID_LIBATT)
				{
					g_pGcData->EquipInfo.iCfgQtyOfLiBatt++;
					g_pGcData->RunInfo.Bt.bIsA_LiBatt[iIdx] = TRUE;
				}
				if((pEquipList + i)->iEquipTypeID == EQUIP_TYPEID_SMBAT)
				{
				    g_pGcData->EquipInfo.iCfgQtyOfSMBAT++;
				    if(g_pGcData->EquipInfo.aiEquipIdBtPri[iIdx] == 272)
				    {
					g_pGcData->EquipInfo.BattTestLogInfo.iSMBATBeginNo = iIdx;

				    }
				}
				if((pEquipList + i)->iEquipTypeID == EQUIP_TYPEID_SMBRC)
				{
				    g_pGcData->EquipInfo.iCfgQtyOfSMBRC++;
				    if(g_pGcData->EquipInfo.aiEquipIdBtPri[iIdx] == 680)
				    {
					g_pGcData->EquipInfo.BattTestLogInfo.iSMBRCBeginNo = iIdx;
				    }
				}
				if((pEquipList + i)->iEquipTypeID == EQUIP_TYPEID_BATT)
				{
				    g_pGcData->EquipInfo.iCfgQtyOfComBAT++;
				}
				if((pEquipList + i)->iEquipTypeID == EQUIP_TYPEID_LARGEDUBATT)
				{
				    g_pGcData->EquipInfo.iCfgQtyOfLargeDUBAT++;
				}
				TRACE("(pEquipList + %d)->iEquipID = %d\n",i, (pEquipList + i)->iEquipID);
			}

		}
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdBattFuse))
			&& (iSubId != 0) && (iSubId != 3))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfBattFuse;
			g_pGcData->EquipInfo.aiEquipIdBtFusePri[iIdx]
					= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfBattFuse++;		
		}
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdLvd))
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfLvd;

			if((pEquipList + i)->iEquipID >= 309 && (pEquipList + i)->iEquipID <= 318)
			{
				g_pGcData->EquipInfo.aiEquipIdLvdPri[iIdx] = 186;	// for LargeDU LVD, it get the signal from LVD Group
			}
			else
			{
				g_pGcData->EquipInfo.aiEquipIdLvdPri[iIdx]
						= (pEquipList + i)->iEquipID;
			}
			g_pGcData->EquipInfo.iCfgQtyOfLvd++;		
		}
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdEib))
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfEib;
			g_pGcData->EquipInfo.aiEquipIdEibPri[iIdx]
					= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfEib++;		
		}
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdS1Rect))
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfS1Rect;
			g_pGcData->EquipInfo.aiEquipIdS1RtPri[iIdx]
					= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfS1Rect++;			
		}
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdS2Rect))
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfS2Rect;
			g_pGcData->EquipInfo.aiEquipIdS2RtPri[iIdx]
					= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfS2Rect++;			
		}
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdS3Rect))
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfS3Rect;
			g_pGcData->EquipInfo.aiEquipIdS3RtPri[iIdx]
					= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfS3Rect++;			
		}
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdCT))
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfCt;
			g_pGcData->EquipInfo.aiEquipIdCtPri[iIdx]
			= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfCt++;			
		}
#ifdef GC_SUPPORT_MPPT
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdMppt))
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfMppt;
			g_pGcData->EquipInfo.aiEquipIdMtPri[iIdx]
			= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfMppt++;			
		}
#endif
#ifdef GC_SUPPORT_MPPT
		else if((iMajorId 
			== GET_EQUIP_TYPE_ID_MAJOR(g_pGcData->PriCfg.iEquipTypeIdBMS))
			&& (iSubId != 0))
		{
			iIdx = g_pGcData->EquipInfo.iCfgQtyOfBMS;
			g_pGcData->EquipInfo.aiEquipIdBMSPri[iIdx]
			= (pEquipList + i)->iEquipID;
			g_pGcData->EquipInfo.iCfgQtyOfBMS++;			
		}
#endif
		/*else if(iSubId == 0)
		{
			g_pGcData->EquipInfo.aiEquipId[iMajorId] 
				= (pEquipList + i)->iEquipID;		
		}*/
	}
	g_SiteInfo.stBattNum.iCfgQtyOfComBAT = g_pGcData->EquipInfo.iCfgQtyOfComBAT;
	g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT1 = g_pGcData->EquipInfo.iCfgQtyOfEIBBAT1;
	g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT1 = g_pGcData->EquipInfo.iCfgQtyOfSmduBAT1;
	g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT2 = g_pGcData->EquipInfo.iCfgQtyOfEIBBAT2;
	g_SiteInfo.stBattNum.iCfgQtyOfSMBAT = g_pGcData->EquipInfo.iCfgQtyOfSMBAT;
	g_SiteInfo.stBattNum.iCfgQtyOfLargeDUBAT = g_pGcData->EquipInfo.iCfgQtyOfLargeDUBAT;
	g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT2 = g_pGcData->EquipInfo.iCfgQtyOfSmduBAT2;
	g_SiteInfo.stBattNum.iCfgQtyOfSMBRC = g_pGcData->EquipInfo.iCfgQtyOfSMBRC;
	g_SiteInfo.stBattNum.iCfgQtyOfSMDUEBAT = g_pGcData->EquipInfo.iCfgQtyOfSMDUEBAT;
	/*
	printf("g_pGcData->EquipInfo.iCfgQtyOfComBAT is %d\n", g_pGcData->EquipInfo.iCfgQtyOfComBAT);
	printf("g_pGcData->EquipInfo.iCfgQtyOfEIBBAT1 is %d\n", g_pGcData->EquipInfo.iCfgQtyOfEIBBAT1);
	printf("g_pGcData->EquipInfo.iCfgQtyOfSmduBAT1 is %d\n", g_pGcData->EquipInfo.iCfgQtyOfSmduBAT1);
	printf("g_pGcData->EquipInfo.iCfgQtyOfEIBBAT2 is %d\n", g_pGcData->EquipInfo.iCfgQtyOfEIBBAT2);
	printf("g_pGcData->EquipInfo.iCfgQtyOfSMBAT is %d\n", g_pGcData->EquipInfo.iCfgQtyOfSMBAT);
	printf("g_pGcData->EquipInfo.iCfgQtyOfLargeDUBAT is %d\n", g_pGcData->EquipInfo.iCfgQtyOfLargeDUBAT);
	printf("g_pGcData->EquipInfo.iCfgQtyOfSmduBAT2 is %d\n", g_pGcData->EquipInfo.iCfgQtyOfSmduBAT2);
	printf("g_pGcData->EquipInfo.iCfgQtyOfSMBRC is %d\n", g_pGcData->EquipInfo.iCfgQtyOfSMBRC);
	printf("g_pGcData->EquipInfo.iCfgQtyOfSMDUEBAT is %d\n",g_pGcData->EquipInfo.iCfgQtyOfSMDUEBAT);
	printf("g_pGcData->EquipInfo.iCfgQtyOfBatt is %d\n",g_pGcData->EquipInfo.iCfgQtyOfBatt);*/

	//Ŀǰ��Ϊ�̶���114����ص�������־
	g_SiteInfo.stBattNum.iCfgQtyOfComBAT = 2;
	g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT1 = 8;
	g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT1 = 32;
	g_SiteInfo.stBattNum.iCfgQtyOfEIBBAT2 = 4;
	g_SiteInfo.stBattNum.iCfgQtyOfSMBAT = 20;
	g_SiteInfo.stBattNum.iCfgQtyOfLargeDUBAT = 20;
	g_SiteInfo.stBattNum.iCfgQtyOfSmduBAT2 = 8;
	g_SiteInfo.stBattNum.iCfgQtyOfSMBRC = 20;

	//printf("\n*******the num of batt is %d!!!!!!",g_pGcData->EquipInfo.iCfgQtyOfBatt);
	TRACE("\n g_pGcData->EquipInfo.iCfgQtyOfBatt = %d\n", g_pGcData->EquipInfo.iCfgQtyOfBatt);
	GC_ASSERT((g_pGcData->EquipInfo.iCfgQtyOfBatt <= MAX_NUM_BATT_EQUIP),
		ERR_CTL_BM_BATT_NUM,
		"Too many battery strings!\n");
	GC_ASSERT((g_pGcData->EquipInfo.iCfgQtyOfRect <= MAX_NUM_RECT_EQUIP),
		ERR_CTL_BM_RECT_NUM,
		"Too many rectifiers!\n");
	GC_ASSERT((g_pGcData->EquipInfo.iCfgQtyOfLvd <= MAX_NUM_LVD_EQUIP),
		ERR_CTL_BM_LVD_NUM,
		"Too many LVD Equipment!\n");
	GC_ASSERT((g_pGcData->EquipInfo.iCfgQtyOfBattFuse <= MAX_NUM_BATT_FUSE_EQUIP),
		ERR_CTL_BM_BATT_FUSE_NUM,
		"Too many Battery Fuse equipment!\n");
	GC_ASSERT((g_pGcData->EquipInfo.iCfgQtyOfEib <= MAX_NUM_EIB_EQUIP),
		ERR_CTL_BM_EIB_NUM,
		"Too many EIB equipment!\n");	
	GC_ASSERT((g_pGcData->EquipInfo.iCfgQtyOfS1Rect <= MAX_NUM_RECT_EQUIP),
		ERR_CTL_BM_RECT_NUM,
		"Slave1 has too many rectifiers!\n");
	GC_ASSERT((g_pGcData->EquipInfo.iCfgQtyOfS2Rect <= MAX_NUM_RECT_EQUIP),
		ERR_CTL_BM_RECT_NUM,
		"Slave2 has too many rectifiers!\n");
	GC_ASSERT((g_pGcData->EquipInfo.iCfgQtyOfS3Rect <= MAX_NUM_RECT_EQUIP),
		ERR_CTL_BM_RECT_NUM,
		"Slave3 has Too many rectifiers!\n");


	/*BattTestLogInfoInit();*/
	ACInfoInit();
	return;
}





/*==========================================================================*
 * FUNCTION : ACInfoInit
 * PURPOSE  : To initialize the battery information
 * CALLS    : 
 * CALLED BY: GC_EquipInfoInit
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-09 16:36
 *==========================================================================*/
static void ACInfoInit(void)
{
	g_pGcData->EquipInfo.AcInfo.bTotalPower = 
		GC_IsSigValid(TYPE_OTHER, 0, AC_PUB_TOTAL_POWER);
	g_pGcData->EquipInfo.AcInfo.bDslStatus = 
		GC_IsSigValid(TYPE_OTHER, 0, DSL_PUB_STATUS);
	g_pGcData->EquipInfo.AcInfo.bDslCtl = 
		GC_IsSigValid(TYPE_OTHER, 0, DSL_PUB_START_CTL);

	
	g_pGcData->EquipInfo.AcInfo.bTotalPower = TRUE;
	g_pGcData->EquipInfo.AcInfo.bDslStatus = TRUE;
	g_pGcData->EquipInfo.AcInfo.bDslCtl = TRUE;
	



	return;
}


#define	GC_GET_DATA_FAIL	1
#define	GC_GET_DATA_SUCCESS	0
/*==========================================================================*
 * FUNCTION : GC_IsSigValid
 * PURPOSE  : To judge if a signal is configured
 * CALLS    : DxiGetData
 * CALLED BY: InitBattInfo
 * ARGUMENTS: BYTE  iEquipType : 
 *            int   iEquipIdx   : 
 *            int   iSigIdx     : 
 * RETURN   : TRUE: the signal is in ACU solution configuration 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-20 11:32
 *==========================================================================*/
BOOL	GC_IsSigValid(int iEquipType,
					   int iEquipIdx,
					   int iSigIdx)
{
	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;
	
	//char				strOut[256];

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);
	
	if(iRst != ERR_OK)
	{	
		//No the signal in standard config file.
		return FALSE;
		/*sprintf(strOut, 
				"GC_GetSigVal error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
				iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);*/
	}

	if(SIG_VALUE_IS_VALID(pSigValue) && SIG_VALUE_IS_CONFIGURED(pSigValue))
	{
		return TRUE;
	}

	return FALSE;

	//switch(iEquipType)
	//{
	//case	TYPE_RECT_UNIT:
	//	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	//			g_pGcData->EquipInfo.aiEquipIdRtPri[iEquipIdx],
	//			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pRectPriSig + iSigIdx)->iSignalType,
	//					(g_pGcData->PriCfg.pRectPriSig + iSigIdx)->iSignalID),
	//			&iBufLen,
	//			&pSigStru,
	//			0);
	//	break;

	//case	TYPE_BATT_UNIT:
	//	iRst = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
	//			g_pGcData->EquipInfo.aiEquipIdBtPri[iEquipIdx],
	//			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattPriSig + iSigIdx)
	//									->iSignalType,
	//							(g_pGcData->PriCfg.pBattPriSig + iSigIdx)
	//									->iSignalID),
	//			&iBufLen,
	//			&pSigStru,
	//			0);
	//	break;

	//case	TYPE_BATT_FUSE_UNIT:
	//	iRst = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
	//			g_pGcData->EquipInfo.aiEquipIdBtFusePri[iEquipIdx],
	//			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattFusePriSig + iSigIdx)
	//									->iSignalType,
	//							(g_pGcData->PriCfg.pBattFusePriSig + iSigIdx)
	//									->iSignalID),
	//			&iBufLen,
	//			&pSigStru,
	//			0);
	//	break;

	//case	TYPE_OTHER:
	//	/*iEquipTypeId = (g_pGcData->PriCfg.pSig + iSigIdx)->iEquipTypeId;
	//	iMajorId = GET_EQUIP_TYPE_ID_MAJOR(iEquipTypeId);*/
	//	iRst = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
	//			(g_pGcData->PriCfg.pSig + iSigIdx)->iEquipId,
	//			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pSig + iSigIdx)->iSignalType,
	//					(g_pGcData->PriCfg.pSig + iSigIdx)->iSignalId),
	//			&iBufLen,
	//			&pSigStru,
	//			0);
	//	break;

	//default:
	//	break;
	//}
	//return (iRst == GC_GET_DATA_SUCCESS) ? TRUE : FALSE;
}
/*==========================================================================*
* FUNCTION : GC_IsEIBSigValid
* PURPOSE  : To judge if a EIB Block signal is configured
* CALLS    : DxiGetData
* CALLED BY: InitBattInfo
* ARGUMENTS: BYTE  iEquipType : 
*            int   iEquipIdx   : 
*            int   iSigIdx     : 
* RETURN   : TRUE: the signal is in ACU+ solution configuration 
* COMMENTS : Special for EIB Block voltage.The block voltage may not display according to the user setting, so don't log it.
* CREATOR  : Marco Yang                DATE: 2011-11-11 11:59
*==========================================================================*/
BOOL	GC_IsEIBSigValid(int iEquipType,
		      int iEquipIdx,
		      int iSigIdx)
{
	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;



	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);

	if(iRst != ERR_OK)
	{		
		return FALSE;	
	}

	if(SIG_VALUE_IS_VALID(pSigValue) 
			&& SIG_VALUE_IS_CONFIGURED(pSigValue) 
			&& SIG_VALUE_NEED_DISPLAY(pSigValue))
	{
		return TRUE;
	}

	return FALSE;

	
}
/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-5 16:30:09
*  DESCRIPTION   : Judge if a Battery Unit a Li Batt or Traditional Batt
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
BOOL GC_Is_ABatt_Li_Type(int iIndx)
{
	if(iIndx < 0 || iIndx >= MAX_NUM_BATT_EQUIP)
	{
		return FALSE;
	}
	return g_pGcData->RunInfo.Bt.bIsA_LiBatt[iIndx];
}
BOOL	GC_IsBattValid(int iEquipType,
					   int iEquipIdx,
					   int iSigIdx)
{

	SIG_BASIC_VALUE*	pSigValue;
	int			iBufLen;
	int			iRst;

	ASSERT(iEquipIdx < MAX_NUM_BATT_EQUIP);		

	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
		g_pGcData->EquipInfo.aiEquipIdBtPri[iEquipIdx],			
		DXI_MERGE_SIG_ID(0, 100),		
		&iBufLen,			
		&pSigValue,			
		0);

	if(iRst != ERR_OK)
	{	
		return FALSE;
		
	}
	else if(!pSigValue->varValue.enumValue)  //0 Existence  other: not existence
	{
		//printf("Battery iEquipIdx %d is valid\n", iEquipIdx); 
		return TRUE;
	}

	return FALSE;	
}

BOOL	GC_IsLargeDUBatt(int iEquipIdx)
{
#define	LARGEDU_BATTERY_TYPE			305
#define	SM_BATT_TYPE				304

	int					iRst;
	int					iBufLen, iTimeOut;
	EQUIP_INFO* pEquipInfo;
	ASSERT(iEquipIdx < MAX_NUM_BATT_EQUIP);		

	iRst = DxiGetData(VAR_A_EQUIP_INFO,
		g_pGcData->EquipInfo.aiEquipIdBtPri[iEquipIdx],			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		iTimeOut);		
	
	if(iRst != ERR_OK)
	{	
		return FALSE;
		
	}
	else if(pEquipInfo->iEquipTypeID == LARGEDU_BATTERY_TYPE || pEquipInfo->iEquipTypeID == SM_BATT_TYPE)
	{
		return TRUE;
	}

	return FALSE;

	
}

/*==========================================================================*
 * FUNCTION : BattInfoInit
 * PURPOSE  : To initialize the battery information
 * CALLS    : 
 * CALLED BY: GC_EquipInfoInit
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-19 15:26
 *==========================================================================*/
static void	BattInfoInit()
{
	int		i, j;

	/*If no battery, return*/
	if(!g_pGcData->EquipInfo.iCfgQtyOfBatt)
	{
		return;
	}

	
	

	
	return;
}

UINT GC_Get_Total_BattNum(void)
{
	if (g_pGcData->EquipInfo.iCfgQtyOfBatt <= MAX_NUM_BATT_EQUIP)
	{
		//printf("\nThe right now batt total is %d",g_pGcData->EquipInfo.iCfgQtyOfBatt);
		return (UINT)(g_pGcData->EquipInfo.iCfgQtyOfBatt);
	}
	else
	{
		return (UINT)MAX_NUM_BATT_EQUIP;
	}
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_float_charge.c
 *  CREATOR  : Frank Cao                DATE: 2004-10-28 16:45
 *  VERSION  : V1.00
 *  PURPOSE  : To provide Float Charge State handling interface functions
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gc_batt_mgmt.h"
#include	"gen_ctl.h"
#include	"gc_sig_value.h"
#include	"gc_float_charge.h"
#include	"gc_boost_charge.h"
#include	"gc_discharge.h"

static BOOL	IsPreBcTime(void);
static BOOL	IsPlanTestTime(void);
static BOOL	IsCyclicStartTime(void);
static void      Set_LastPlanTime();
static BOOL JudgeBattCapaForBT(void);

/*==========================================================================*
 * FUNCTION : IsUnderHiTemp
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-11-04 16:23
 *==========================================================================*/
static BOOL IsUnderHiTemp(void)
{
	/*int		i;


	for(i = 0; i < MAX_NUM_TEMPERATURE - MAX_NUM_LargeDU_TEMP; i++)
	{
		float	fVeryHiTemp = GC_GetFloatValue(TYPE_OTHER, 
										0,
										GC_PUB_VERY_HI_TEMP1_PARAM + i) - 5;

		SIG_ENUM	enumUsage = GC_GetEnumValue(TYPE_OTHER, 
								0,
								GC_PUB_TEMP1_USAGE + i);
		if((enumUsage == TEMP_USAGE_BATTERY)
			&& (GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_TEMPERATURE1 + i)
				> fVeryHiTemp))
		{
			return FALSE;
		}
	}

	return TRUE;*/

	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_BATTTEMP_HIGH_ALARM))
	{
		return TRUE;
	}

	return FALSE;
}
/*==========================================================================*
* FUNCTION : IsUnderHiBTRM
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Jimmy Wu                DATE: 2012-02-16 16:23
*==========================================================================*/
static BOOL IsUnderHiBTRM(void)
{
	/*int		i;


	for(i = 0; i < MAX_NUM_TEMPERATURE - MAX_NUM_LargeDU_TEMP; i++)
	{
	float	fVeryHiTemp = GC_GetFloatValue(TYPE_OTHER, 
	0,
	GC_PUB_VERY_HI_TEMP1_PARAM + i) - 5;

	SIG_ENUM	enumUsage = GC_GetEnumValue(TYPE_OTHER, 
	0,
	GC_PUB_TEMP1_USAGE + i);
	if((enumUsage == TEMP_USAGE_BATTERY)
	&& (GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_TEMPERATURE1 + i)
	> fVeryHiTemp))
	{
	return FALSE;
	}
	}

	return TRUE;*/

	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_BTRM_HIGH_ALARM))
	{
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : GC_FloatCharge
 * PURPOSE  : The handling of Float Charge State
 * CALLS    : GC_GetEnumValue GC_RectVoltCtl GC_CurrentLmt 
 *            GC_CmpCapAndFcToBcCap GC_CmpBattCurrAndFcToBcCurr 
 *            GC_BoostChargeStart GC_BattTestStart GC_GetStateOfSecTimer
 *            IsPreBcTime IsPlanTestTime 
 * CALLED BY: StateMgmt
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-08 09:13
 *==========================================================================*/
void GC_FloatCharge()
{
	static SIG_ENUM		s_enumLastState = STATE_AUTO;
	BOOL                  bStartBoost     =FALSE;
	SIG_ENUM enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
	SIG_ENUM enumGridState = GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get 

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif

#ifdef GC_SUPPORT_MPPT
	if((stState == GC_DISABLED && g_pGcData->RunInfo.Rt.bAcFail)
		|| (stState ==  GC_MPPT_MPPT_RECT && g_pGcData->RunInfo.Rt.bAcFail && g_pGcData->RunInfo.Mt.bNight)
		|| (stState ==  GC_MPPT_MPPT && g_pGcData->RunInfo.Mt.bNight) )
#else
	if(g_pGcData->RunInfo.Rt.bAcFail)
#endif
	//All of rectifiers AC failure
	{
		if(GC_DISABLED 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_AC_FAIL_TEST_ENB))
		//Ac-fail test disabled
		{
			//Battery Management State
			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							BT_PUB_BM_STATE,
							ST_AC_FAIL,
							TRUE);	
	
			//Event Log
			GC_LOG_OUT(APP_LOG_INFO, "AC failure, turn to AC_Fail.\n");
		}
		else
		{

			GC_BattTestStart(ST_AC_FAIL_TEST, 
						"AC failure, turn to AC Fail Test state.\n",
						START_AC_FAIL_TEST);
		}
		return;
	}
	//AC not fail
	else
	{
		//Manually turn to boost charge state
		if(CTL_BOOST 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BOOST_CTL))
		{
			//2010-11-11:In Auto mode and start boost charge manual, don't care about the AUTO_BC_ENB.
			if(enumState != GC_HYBRID_DISABLED && STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
			{
				//Do nothing;
			}
			else
			{	
				if(GC_IsNeedVoltAdjust() == FALSE ) 
				{	

#ifdef GC_SUPPORT_MPPT
					if(stState != GC_MPPT_DISABLE)
					{
						GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect + g_pGcData->RunInfo.Mt.iQtyOnWorkRect , FALSE, FALSE);
					}
					else
					{
						GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);
					}
#else
					GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);

#endif		
				}
				GC_BoostChargeStart(ST_MAN_BC,
					"Manual BC, turn to Manual BC.\n");

			}
			
			return;
		}
#ifdef GC_SUPPORT_MPPT
	if(stState  ==  GC_MPPT_DISABLE && CTL_TEST_START 
					== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
#else
		//Manually start battery test			
		if(CTL_TEST_START 
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL))
#endif
		{
			GC_BattTestStart(ST_MAN_TEST,
					"Manual Test, turn to Manual Test.\n",
					START_MANUAL_TEST);
			return;
		}

		if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
		//Automatically
		{

			//added by Jimmy for ESNA Mode Specially 
			if(IsWorkIn_LiBattMode())//Li���ģʽ�²������²��͵�ѹ
			{
				GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);//FALSE means no temperature compensation				
				GC_CurrentLmt();
			}
			else //������ԭ���߼�
			{
				static	BOOL	s_bHiTempLowerVolt = FALSE;
				s_enumLastState = STATE_AUTO;

				if((HI_TEMP_ACTION_LOWER_VOLT 
					== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_HI_TEMP_ACTION))
					&& (GC_IsVeryHiBTRM()))
				{
					if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
					{
						g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

						CurrentLimitThroughVolt(FALSE ,BT_PUB_HI_TEMP_VOLT, FALSE);
						s_bHiTempLowerVolt = TRUE;

					}
					else
					{
						GC_RectVoltCtl(TYPE_VERY_HI_TEMP, FALSE);
						s_bHiTempLowerVolt = TRUE;
						GC_CurrentLmt();

					}
					return;
				}

				else if(s_bHiTempLowerVolt)	// not consider change of BT_PUB_HI_TEMP_ACTION
				{
				
					if((HI_TEMP_ACTION_LOWER_VOLT 
						!= GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_HI_TEMP_ACTION))
						|| (!GC_IsVeryHiBTRM()))
					{
						s_bHiTempLowerVolt = FALSE;

					}
					else
					{
						if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
						{
							g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

							CurrentLimitThroughVolt(FALSE ,BT_PUB_HI_TEMP_VOLT, FALSE);
							
						}
						else
						{
							GC_RectVoltCtl(TYPE_VERY_HI_TEMP, FALSE);
							GC_CurrentLmt();
							
						}
					}
					return;
				}

				int iESNAMode = 0;
	#ifdef GC_SUPPORT_ESNA_COMP_MODE			
				iESNAMode = GC_GetEnumValue(TYPE_OTHER,0,BT_PUB_ESNA_COMP_MODE_ENABLE); //ESNA mode Enable

	#endif	
				if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
				{	
			
					BOOL bFlag;
					if(iESNAMode == 1)
					{
						bFlag = TRUE;
					}
					else //traditional comp temp method
					{
						if(GC_IsUnderVolt()
							|| GC_IsOverVolt())
						{

							bFlag = FALSE;//FALSE means no temperature compensation				
						}
						else
						{
							bFlag = TRUE;//TRUE means temperature compensation
						}
					}

					CurrentLimitThroughVolt(FALSE ,BT_PUB_NOM_VOLT, bFlag);
				}
				else
				{

					if(iESNAMode == 1)
					{
						GC_RectVoltCtl(TYPE_FLOAT_VOLT, TRUE);//TRUE means temperature compensation
					}
					else //traditional comp temp method
					{
						if(GC_IsUnderVolt()
							|| GC_IsOverVolt())
						{

							GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);//FALSE means no temperature compensation				
						}
						else
						{
				
							GC_RectVoltCtl(TYPE_FLOAT_VOLT, TRUE);//TRUE means temperature compensation
						}
					}
					
					GC_CurrentLmt();
#ifdef GC_SUPPORT_MPPT
				
					GC_PreCurrLmt_Mppt(g_pGcData->RunInfo.Mt.iQtyOfRect);
#endif
				}
			}
			
			//Automatically Boost Charge
			if(GC_ENABLED 
				== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_AUTO_BC_ENB))

			{

				if(!GC_AlmJudgmentForFCtoBC() 
					&& (SEC_TMR_TIME_IN != GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY)))
				{

					if(g_pGcData->RunInfo.Bt.bOverFc2BcCurr)
					{
						if(GC_IsNeedVoltAdjust() == FALSE ) 
						{	

#ifdef GC_SUPPORT_MPPT
							if(stState != GC_MPPT_DISABLE)
							{
								GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect + g_pGcData->RunInfo.Mt.iQtyOnWorkRect , FALSE, FALSE);
							}
							else
							{
								GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);
							}
#else
							GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);

#endif		
						}

						GC_BoostChargeStart(ST_AUTO_BC,
							"For battery current, turn to Automatic BC.\n");
						return;			
					}

					if(GC_CmpCapAndFcToBcCap())
					{
						if(GC_IsNeedVoltAdjust() == FALSE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
						{	
#ifdef GC_SUPPORT_MPPT
							if(stState != GC_MPPT_DISABLE)
							{
								GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect + g_pGcData->RunInfo.Mt.iQtyOnWorkRect , FALSE, FALSE);
							}
							else
							{
								GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);
							}
#else
							GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);

#endif						
						}
						GC_BoostChargeStart(ST_AUTO_BC,
							"For battery capacity, turn to Automatic BC.\n");
						return;			
					}
				}
			}
			

			if(enumState != GC_HYBRID_DISABLED && enumGridState == GC_HYBRID_GRID_OFF)
			{
				return;
			}	

			//Short Test
			if(GC_ENABLED 
				== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_SHORT_TEST_ENB))
			{

				if(!GC_AlmJudgmentForBT())
				{
					if(SEC_TMR_TIME_OUT 
						== GC_GetStateOfSecTimer(SEC_TMR_ID_SHORT_TEST_INTERVAL))
					{
						GC_ShortTestStart();
						return;
					}
				}
			}

			

			//Cyclic Boost Charge		
			if(GC_ENABLED 
				== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CYC_BC_ENB))
			{	
				if(GC_IsGCSigExist(TYPE_OTHER, 0, BT_PUB_CYC_START_TIME))
		              {
			                  if(IsCyclicStartTime() 
					    	&& GC_CYC_VALID_ENABLE==GC_GetEnumValue(TYPE_OTHER,0, BT_PUB_CYC_VALID_ENABLE))
			                  {
						bStartBoost=TRUE;
			                  }
			                  else
			                  {
			                     bStartBoost=FALSE;

			                  }
		              }
		              else
		              {
			                 if(SEC_TMR_TIME_OUT == GC_GetStateOfSecTimer(SEC_TMR_ID_CYCLIC_BC_INTERVAL))
			                  {
			                     bStartBoost=TRUE;
			                  }
			                  else
			                  {
			                     bStartBoost=FALSE;

			                  }
		              }

				if(!GC_AlmJudgmentForFCtoBC() && bStartBoost )
				{
					if(GC_IsNeedVoltAdjust() == FALSE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
					{	

#ifdef GC_SUPPORT_MPPT
						if(stState != GC_MPPT_DISABLE)
						{
							GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect + g_pGcData->RunInfo.Mt.iQtyOnWorkRect , FALSE, FALSE);
						}
						else
						{
							GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);
						}
#else
						GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE, FALSE);

#endif							
					}
					GC_BoostChargeStart(ST_CYCLIC_BC,
						"Time is out, turn to Cyclic BC.\n");
					Set_LastPlanTime();
					return;						
					
				}
			}

			//Planned Test		
			/*if(GC_ENABLED 
				== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_PLAN_TEST_ENB))*/
			if(GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_PLAN_TEST_NUM))
			{

				//BC for Planned Test
				if(GC_ENABLED 
					== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_PRE_BC_ENB))
				{
					if(!GC_AlmJudgmentForFCtoBC())
					{
						if(SEC_TMR_TIME_IN 
							!= GC_GetStateOfSecTimer(SEC_TMR_ID_AFTER_BC))
						{
							if(IsPreBcTime())
							{
								GC_BoostChargeStart(ST_BC_FOR_TEST,
									"It's Pre-BC hour, turn to Pre-BC.\n");
								return;	
							}
						}
					}
				}

				//Planned Test
				if(!GC_AlmJudgmentForBT())
				{

					if(IsPlanTestTime())
					{
						if((GC_DISABLED == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_PRE_BC_ENB))
							&& !JudgeBattCapaForBT())
						{

							GC_LOG_OUT(APP_LOG_INFO, "It's plan test time, but do NOT test because battery's capacity is low.\n");
						}
						else
						{
							
							GC_BattTestStart(ST_PLAN_TEST,
								"It's plan time, turn to Planned Test.\n",
								START_PLANNED_TEST);
						}
						return;				
					}
				}
			}		
		}
		else
		//Manually
		{
			if (STATE_AUTO == s_enumLastState)
			{
				GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);//FALSE means no temperature compensation

				//When it go to manual state, open all rectifiers and release the current limit point, changed by Marco Yang,2011-5-31
				int	i;	
				for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
				{

					GC_SetEnumValue(TYPE_OTHER, 
						0, 
						RT_PUB_AC_ON_OFF_CTL_SELF + i,
						GC_RECT_ON,
						TRUE);

					GC_SetEnumValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_ON_OFF_CTL_SELF + i,
						GC_RECT_ON,
						TRUE);
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						RT_PUB_CURR_LMT_CTL_SELF + i,
						GC_MAX_CURR_LMT);
					
				}
				g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					CT_PUB_DC_ON_OFF_CTL,
					GC_RECT_ON,
					TRUE);

				//GC_SetFloatValue(TYPE_OTHER, 
				//	0,
				//	CT_PUB_CTL_CURR,
				//	GC_MAX_CURR_LMT_CONV);//31.3);
				GC_SendCTCurrentLimit(GC_MAX_CURR_LMT_CONV);
						
				//End:When it go to manual state, open all rectifiers and release the current limit point, changed by Marco Yang,2011-5-31

				s_enumLastState = STATE_MAN;

				GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_TEMP_COMPEN_SET,
						GC_NORMAL,
						FALSE);	

				TRACE("GEN_CTL: Control rectifier to float voltage.\n");
			}

			////Added by Samson Fan, 2009-11-17, �ֶ�ģʽ��ÿһȦ���·���ѹ����
			//int i;
			//float fRectCtrlVolt;
			//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
			//{
			//	fRectCtrlVolt = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DC_VOLT_CTL_24V);
			//	GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_DC_VOLT_CTL_24V, fRectCtrlVolt);
			//	
			//	for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
			//	{
			//		if(GC_EQUIP_EXIST == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1))
			//		{
			//			GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_DC_VOLT_CTL + i,	fRectCtrlVolt);	
			//		}		
			//	}
			//}
			//else
			//{
			//	fRectCtrlVolt = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DC_VOLT_CTL);
			//	GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_DC_VOLT_CTL,	fRectCtrlVolt);	

			//	for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
			//	{
			//		if(GC_EQUIP_EXIST == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1))
			//		{
			//			GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_DC_VOLT_CTL + i,	fRectCtrlVolt);	
			//		}		
			//	}
			//}
			////End
		}
	}

	return;
}


/*==========================================================================*
 * FUNCTION : IsPlanTestTime
 * PURPOSE  : To judge if it's time for a Planned Test
 * CALLS    : gmtime_r
 * CALLED BY: GC_FloatCharge
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-11 11:29
 *==========================================================================*/
static BOOL	IsPlanTestTime()
{
	int			i;
	int			iPlanNum;
	struct tm	tmNow;
	struct tm	tmLast;
	
	gmtime_r(&(g_pGcData->RunInfo.Bt.tmLastPlanTest), &tmLast);	
	gmtime_r(&(g_pGcData->SecTimer.tTimeNow), &tmNow);

	//To prevent multiple Plan-tests in one hour.
	if((tmNow.tm_year == tmLast.tm_year) 
		&& (tmNow.tm_mon == tmLast.tm_mon)
		&& (tmNow.tm_mday == tmLast.tm_mday) 
		&& (tmNow.tm_hour == tmLast.tm_hour))
	{
		//If no hour change, then return FALSE.
		return FALSE;
	}

	iPlanNum = GC_GetDwordValue(TYPE_OTHER,
								0, 
								BT_PUB_PLAN_TEST_NUM);

	for(i = 0; i < iPlanNum; i++)
	{
		SIG_TIME timePlan 
			= GC_GetTimeValue(TYPE_OTHER, 0, BT_PUB_PLAN_TEST_TIME1 + i);
		
		struct tm tmPlan;
		
		gmtime_r(&timePlan, &tmPlan);
		
		if((tmNow.tm_mon == tmPlan.tm_mon)
			&& (tmNow.tm_mday == tmPlan.tm_mday)
			&& (tmNow.tm_hour == tmPlan.tm_hour))
		{
			g_pGcData->RunInfo.Bt.tmLastPlanTest 
				= g_pGcData->SecTimer.tTimeNow;
			GC_SaveData();

			return TRUE;

		}
	}

	return FALSE;
}

/*==========================================================================*
 * FUNCTION : IsCyclicTestTime
 * PURPOSE  : To judge if it's time for a cyclic Charge
 * CALLS    : gmtime_r
 * CALLED BY: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : raoliang                DATE: 2016-11-22 
 *==========================================================================*/
static BOOL	IsCyclicStartTime()
{
	
	struct tm	tmNow;
	struct tm	tmLast;
	struct tm	tmPlan;
	BOOL		bTimeOut=TRUE;
	SIG_TIME       LastPlanTime=GC_GetTimeValue(TYPE_OTHER,0, BT_PUB_CYC_LASTPLAN_TIME);//default value 0
	gmtime_r(&LastPlanTime, &tmLast);	
	gmtime_r(&(g_pGcData->SecTimer.tTimeNow), &tmNow);

	DWORD iCyclicDays = GC_GetDwordValue(TYPE_OTHER,
							0, 
							BT_PUB_CYC_BC_INTERVAL);
	//To prevent multiple Plan-tests in one hour. if we do not restored this time ,next time up in an hour will enter bc again
	if((tmNow.tm_year == tmLast.tm_year) 
		&& (tmNow.tm_mon == tmLast.tm_mon)
		&& (tmNow.tm_mday == tmLast.tm_mday) 
		&& (tmNow.tm_hour == tmLast.tm_hour))
	{
		//If no hour change, then return FALSE.
		return FALSE;
	}
	SIG_TIME timePlan
		  = GC_GetTimeValue(TYPE_OTHER, 0, BT_PUB_CYC_START_TIME);
	gmtime_r(&timePlan, &tmPlan);	
	//printf("setted:time now[%d] time panl[%d] %d-%d-%d:%d-%d-%d\n",g_pGcData->SecTimer.tTimeNow,timePlan,tmPlan.tm_year,tmPlan.tm_mon,tmPlan.tm_mday,tmPlan.tm_hour,tmPlan.tm_min,tmPlan.tm_sec);
	if(g_pGcData->SecTimer.tTimeNow > timePlan
		&& (g_pGcData->SecTimer.tTimeNow-timePlan)%(iCyclicDays*SECONDS_PER_DAY) < SECONDS_PER_HOUR )
	{
		return TRUE;
	}
	return FALSE;
}

/*==========================================================================*
 * FUNCTION : Set_LastPlanTime
 * PURPOSE  : To judge if it's time for a cyclic Charge
 * CALLS    : gmtime_r
 * CALLED BY: GC_FloatCharge
 * RETURN   : only called after BoostChargeStart  ST_CYCLIC_BC Succefully
 * COMMENTS : 
You should Set_LastPlanTime only if Boost charge Start  succefully,
Time reach does not mean You start Boost charge succefully, Valid EQ Sig can also control Boost charge 
so call Set_LastPlanTime  only if Boost charge Start  succefully,
 * CREATOR  : raoliang                DATE: 2016-11-22 
 *==========================================================================*/
static void Set_LastPlanTime()
{
	struct tm	tmNow;
	SIG_TIME       LastPlanTime=0;
	//if old cyc solution ,donot need update LastPlanTime
	if(!GC_IsGCSigExist(TYPE_OTHER, 0, BT_PUB_CYC_START_TIME))
		return;
	/* */
	gmtime_r(&(g_pGcData->SecTimer.tTimeNow), &tmNow);
	tmNow.tm_min=0;
	tmNow.tm_sec=0;
	LastPlanTime=mktime(&tmNow);	
	GC_SetDateValue(TYPE_OTHER, 0, BT_PUB_CYC_LASTPLAN_TIME,LastPlanTime);
	//printf("Boost charge Start!!! Update LastPlanTime[%d]\n",LastPlanTime);
	return;
}


/*==========================================================================*
 * FUNCTION : IsPreBcTime
 * PURPOSE  : To judge if it's time for a Boost Charge for Test
 * CALLS    : gmtime_r GC_GetDwordValue
 * CALLED BY: GC_Float_Charge
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-11 11:31
 *==========================================================================*/
static BOOL	IsPreBcTime()
{
	int				i;
	int				iBcDuration;
	time_t			tAfterPreBc;	
	int				iPlanNum;
	struct tm		tmNow; 
	struct tm		tmfterPreBc;	

	static int		s_iLastHour = -1;

	gmtime_r(&(g_pGcData->SecTimer.tTimeNow), &tmNow);

	//To prevent multiple Pre-BC in one hour.
	if(tmNow.tm_hour == s_iLastHour)
	{
		//If no hour change, then return FALSE.
		return FALSE;
	}
	s_iLastHour = tmNow.tm_hour;

	iBcDuration = GC_GetDwordValue(TYPE_OTHER,
								0, 
								BT_PUB_CYC_BC_DURATION);
	tAfterPreBc = g_pGcData->SecTimer.tTimeNow 
				+ SECONDS_PER_MIN * iBcDuration
				+ SECONDS_PER_HOUR * 2;
	
	gmtime_r(&tAfterPreBc, &tmfterPreBc);	
	iPlanNum = GC_GetDwordValue(TYPE_OTHER,
								0, 
								BT_PUB_PLAN_TEST_NUM);


	for(i = 0; i < iPlanNum; i++)
	{
		SIG_TIME timePlan 
			= GC_GetTimeValue(TYPE_OTHER, 0, BT_PUB_PLAN_TEST_TIME1 + i);
		
		struct tm tmPlan;

		gmtime_r(&timePlan, &tmPlan);

		if((tmfterPreBc.tm_mon == tmPlan.tm_mon)
			&& (tmfterPreBc.tm_mday == tmPlan.tm_mday)
			&& (tmfterPreBc.tm_hour == tmPlan.tm_hour))
		{
			return TRUE;
		}
	}
	return FALSE;

}


/*==========================================================================*
* FUNCTION : BattCapaForBT
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : TRUE: All Battery capacity >= 99%
FALSE: At least one battery's capacity < 99%
* COMMENTS : 
* CREATOR  : Samson Fan               DATE: 2009-12-07 09:21
*==========================================================================*/
static BOOL JudgeBattCapaForBT(void)
{
	int iLowestCapaForBt = GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_LOWEST_CAPA_FOR_BT);	
	char szLogOut[128];

	int i;
	for(i = 0; i < g_pGcData->EquipInfo.iCfgQtyOfBatt; i++)
	{
		float fRatedCapa;
		BOOL bRTN;
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCapa);
		if(bRTN ==FALSE)
			continue;
		if(g_pGcData->RunInfo.Bt.abValid[i])
		{
			if(g_pGcData->RunInfo.Bt.afBattCap[i] < fRatedCapa * iLowestCapaForBt / 100)
			{
				sprintf(szLogOut, "g_pGcData->RunInfo.Bt.afBattCap[%d] = %f\n", i, g_pGcData->RunInfo.Bt.afBattCap[i]);
				GC_LOG_OUT(APP_LOG_INFO, szLogOut);
				return FALSE;
			}
		}
	}

	return TRUE;
}

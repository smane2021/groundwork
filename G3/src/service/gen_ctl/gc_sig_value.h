/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_sig_value.h
 *  CREATOR  : Frank Cao                DATE: 2004-10-20 13:52
 *  VERSION  : V1.00
 *  PURPOSE  : Get / Set signal value interface functions
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_SIG_VALUE_H_
#define _GC_SIG_VALUE_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

int	GC_GetSigVal(int iEquipType, 
				 int iEquipIdx,
				 int iSigIdx, 
				 SIG_BASIC_VALUE** pSigValue);

SIG_ENUM GC_GetEnumValue(int iEquipType, 
						 int iEquipIdx, 
						 int iSigIdx);
BOOL GC_GetEnumValueWithReturn(int iEquipType, 
			       int iEquipIdx, 
			       int iSigIdx,
			       SIG_ENUM* pValue);
SIG_ENUM GC_GetAlarmValue(int iEquipType, 
				 int iEquipIdx, 
				 int iSigIdx);

float GC_GetFloatValue(int iEquipType, 
					   int iEquipIdx, 
					   int iSigIdx);
BOOL GC_GetFloatValueWithStateReturn(int iEquipType, 
				     int iEquipIdx, 
				     int iSigIdx,
				     float * pReturnValue );

DWORD GC_GetDwordValue(int iEquipType,
					   int iEquipIdx, 
					   int iSigIdx);
BOOL GC_GetDwordValueWithStateReturn(int iEquipType, 
				     int iEquipIdx, 
				     int iSigIdx,
				      int iResetFlag,
				     DWORD * dwReturnValue);

long GC_GetLongValue(int iEquipType,
					 int iEquipIdx, 
					 int iSigIdx);

SIG_TIME GC_GetTimeValue(int iEquipType, 
						 int iEquipIdx, 
						 int iSigIdx);

void GC_SetSigVal(int iEquipType,
				  int iEquipIdx,
				  int iSigIdx,
				  VAR_VALUE_EX SigVal);

void GC_SetEnumValue(int iEquipType, 
					 int iEquipIdx,
					 int iSigIdx,
					 SIG_ENUM enumValue,
					 BOOL bSendDirectly);

void GC_SetFloatValue(int iEquipType, 
					  int iEquipIdx,
					  int iSigIdx,
					  float fValue);

void GC_SetDwordValue(int iEquipType, 
					  int iEquipIdx,
					  int iSigIdx,
					  DWORD dwValue);

void GC_SetDateValue(int iEquipType, 
					 int iEquipIdx,
					 int iSigIdx,
					 time_t dtValue);

SIG_ENUM GC_IsGCEquipExist(int iEquipType, 
				 int iEquipIdx, 
				 int iSigIdx,
				 int RTN);
BOOL GC_IsGCSigExist(int iEquipType, 
		     int iEquipIdx, 
		     int iSigIdx);
#endif	/*_GC_SIG_VALUE_H_*/


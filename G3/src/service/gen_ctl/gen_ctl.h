/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU Plus Rack_mounted
 *
 *  FILENAME : gen_ctl.h
 *  CREATOR  : Frank Cao                DATE: 2006-05-09 15:58
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#ifndef _GEN_CTL_H_
#define _GEN_CTL_H_

#include	<setjmp.h>

#include	"basetypes.h"
#include	"gc_timer.h"
#include	"gc_run_info.h"
#include	"gc_pri_cfg.h"
#include	"gc_init_equip.h"
#include	"gc_batt_mgmt.h"

#define	STATE_AUTO			0
#define	STATE_MAN			1

#define	MODE_MASTER			1
#define	MODE_SLAVE			2
#define	MODE_STANDALONE		0

/*General Controller Data: In the ServiceMain function, a TYPE_GEN_CTL_DATA 
  variant shall be defined, which shall be initialized by the InitGC. A globel
  variant g_pGcData shall point to TYPE_GEN_CTL_DATA variant, so all of 
  General Controller sub functions access the TYPE_GEN_CTL_DATA variant thru
  g_pGcData*/
struct tagGEN_CTL_DATA
{
	jmp_buf				JmpBuf;

	/*Handle of General Controller Service thread*/
	HANDLE				hThreadSelf;

	SIG_ENUM			enumMasterSlaveMode;

	//added by Jimmy for auto-config flag
	BOOL				bAutoConfig;

	/*Equipments Information get thru DxiGetData from solution config*/
	GC_EQUIP_INFO		EquipInfo;

	/*Include all of information of gen_ctl.cfg*/
	GC_PRI_CFG			PriCfg;

	/*Some of middle value and calculation result*/
	GC_RUN_INFO			RunInfo;


	/*Second Timers Data*/
	GC_SEC_TIMER_GROUP	SecTimer;
};
typedef struct tagGEN_CTL_DATA TYPE_GEN_CTL_DATA;

extern TYPE_GEN_CTL_DATA*	g_pGcData;

//#define GC_SetExitFlag() g_pGcData->bExitFlag = TRUE


#define  GC_ASSERT(Rst, ErrCode, OutString) \
	if (!(Rst)) GC_ErrHandler((ErrCode), (OutString))

/*{AppLogOut("GEN CTRL", APP_LOG_ERROR, "%s, Error Code: %d.\n", \
	(OutString), (ErrCode)); longjmp(g_pGcData->JmpBuf, (ErrCode));}*/

#define  GC_LOG_OUT(iLevel, OutString) \
	AppLogOut("GEN CTRL", (iLevel), "%s", OutString)

DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs);
void GC_ErrHandler(int iErrCode, char *szOutString);
void	TestLogInit(void);
//int*	get_iTest();
#endif	/*_GEN_CTL_H_*/


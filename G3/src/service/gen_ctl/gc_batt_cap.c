/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_batt_cap.c
 *  CREATOR  : Frank Cao                DATE: 2004-11-05 14:07
 *  VERSION  : V1.00
 *  PURPOSE  : 	1. Battery Type No. Management.
                2. Battery Capacity Prediction.
				3. Battery Capacity Regulation.
				4. Discharge Time Prediction
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gc_batt_mgmt.h"
#include	"gen_ctl.h"
#include	"gc_sig_value.h"
#include	"gc_batt_cap.h"

static void		MaintainBattParam(void);
static float	GetDischCoef(float fCurr, float fRatedCap, float *pfTimeCoef);
static float    GetDeltaCap(float fCurr, float fRatedCap, float *pfTimeCoef);
static BOOL BattDischTimePredict(float BattCurr ,float DischTime,float BattCap,float* PredictResult);
//to refresh SEC_TMR_ID_RESET_CAP timer
static void TimerResetCap(void);

/*==========================================================================*
 * FUNCTION : GC_BattCapCalc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-02-23 09:26
 *==========================================================================*/
void GC_BattCapCalc(void)
{
	int				i;
	BOOL			bSave,bRTN;
	//float			fBattCap;
	float			fMinCurr;
	float			fRatedCap;
	float			afDischCurve[10];
	float			fDischCoef,fDischCoefSetting;
	float			fDeltaCap;
	float			fDiffRate;
	float			fTotalCap;
	float			fTotalRatedCap;
	float			fDischTime;
	float			fLoadCurr;
	float			fDischTimeVolt;
	BOOL			bResult;

	TimerResetCap();	
	
	MaintainBattParam();

	fRatedCap = g_pGcData->RunInfo.Bt.afBattRatedCap[0];

	

	bSave = FALSE;

	for(i = 0; i < 10; i++)
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
				    0,
				    BT_PUB_DISCH_TIME01 + i,
				   &afDischCurve[i]);
		if(bRTN == FALSE)
			return;
	}

	bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
		0,
		BT_PUB_CAP_COEF,
		&fDischCoefSetting);
	if(bRTN == FALSE)
		return;


	if(g_pGcData->RunInfo.Bt.bNeedRegulateCap)
	{
		g_pGcData->RunInfo.Bt.bNeedRegulateCap = FALSE;
		
		for(i = 0; i < GC_Get_Total_BattNum(); i++)
		{
			//if(g_pGcData->RunInfo.Bt.abValid[i])
			//{
				if(!(FLOAT_EQUAL(g_pGcData->RunInfo.Bt.afBattCap[i],
								g_pGcData->RunInfo.Bt.afBattRatedCap[i])))
				{
					//added by Jimmy for Li Batt CR
					if(GC_Is_ABatt_Li_Type(i))
					{
						//���������Bridge Card�ϴ���������ﲻ����	
					}
					else
					{
						//g_pGcData->RunInfo.Bt.afBattCap[i] = fRatedCap;
						g_pGcData->RunInfo.Bt.afBattCap[i] = g_pGcData->RunInfo.Bt.afBattRatedCap[i];
					
						GC_SetFloatValue(TYPE_BATT_UNIT,
							i,
							BT_PRI_CAP,
							g_pGcData->RunInfo.Bt.afBattRatedCap[i]);
					}

					bSave = TRUE;
				}
			//}
		}
	}

	fTotalCap = 0.0;
	fTotalRatedCap = 0.0;
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i])
		{
			float	fBattCurr = g_pGcData->RunInfo.Bt.afBattCurr[i];

			if(fBattCurr >= 0)
			//Charging
			{
				float fCapCoef = 0.01 * fDischCoefSetting;
				fDeltaCap = fBattCurr 
							* GC_INTERVAL_BATT_MGMT 
							* fCapCoef
							/ SECONDS_PER_HOUR;
			}
			//Discharging
			else									
			{	
				
				//fDischCoef 
				//	= GetDischCoef(fBattCurr,
				//				g_pGcData->RunInfo.Bt.afBattRatedCap[i],
				//				afDischCurve);
				

				/*fDeltaCap = fBattCurr 
							* GC_INTERVAL_BATT_MGMT 
							/ SECONDS_PER_HOUR 
							/ fDischCoef;*/
				fDeltaCap  = GetDeltaCap(fBattCurr,
					g_pGcData->RunInfo.Bt.afBattRatedCap[i],
					afDischCurve);
				fDeltaCap  = - fDeltaCap ;
				
				//ignore very small discharge current
				fMinCurr = MIN_CURR_COEF * g_pGcData->RunInfo.Bt.afBattRatedCap[i];

				if(fBattCurr > (0 - fMinCurr))
				{
					BYTE	byState;
					byState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);
					switch (byState)
					{
					case ST_FLOAT:
					case ST_CYCLIC_BC:
					case ST_MAN_BC:
					case ST_AUTO_BC:
					case ST_BC_FOR_TEST:
					case ST_POWER_SPLIT_BC:

						if((GC_GetAlarmNum(ALARM_LEVEL_CRITICAL) == 0)
							&& (GC_GetAlarmNum(ALARM_LEVEL_MAJOR) == 0))
						{
							//fDeltaCap = MINI_COEF * fRatedCap;
							fDeltaCap = MINI_COEF * g_pGcData->RunInfo.Bt.afBattRatedCap[i];
							fDeltaCap  = - fDeltaCap ;
						}
						else
						{
							fDeltaCap = 0;	
						}
						break;
					default:
						break;
					}
				}
			}

			//added by Jimmy for Li Batt CR
			if(GC_Is_ABatt_Li_Type(i))
			{
				//���������Bridge Card�ϴ���������ﲻ����	
				bRTN= 
					GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_CAP,&(g_pGcData->RunInfo.Bt.afBattCap[i]));
				if(bRTN == FALSE)
					return;

			}
			else
			{	

				g_pGcData->RunInfo.Bt.afBattCap[i] += fDeltaCap;


				if(g_pGcData->RunInfo.Bt.afBattCap[i] < 0)
				{
					g_pGcData->RunInfo.Bt.afBattCap[i] = 0;
				}
				else if(g_pGcData->RunInfo.Bt.afBattCap[i] > g_pGcData->RunInfo.Bt.afBattRatedCap[i])
				{
					g_pGcData->RunInfo.Bt.afBattCap[i] = g_pGcData->RunInfo.Bt.afBattRatedCap[i];
				}
				GC_SetFloatValue(TYPE_BATT_UNIT, 
					i, 
					BT_PRI_CAP, 
					g_pGcData->RunInfo.Bt.afBattCap[i]);
			}


			fTotalCap += g_pGcData->RunInfo.Bt.afBattCap[i];
			fTotalRatedCap += g_pGcData->RunInfo.Bt.afBattRatedCap[i];

			//for to save data
			fDiffRate 
				= (g_pGcData->RunInfo.Bt.afBattCap[i] 
					- g_pGcData->RunInfo.Bt.FlashData.afBattCap[i]) 
					/ g_pGcData->RunInfo.Bt.afBattRatedCap[i];
			if(ABS(fDiffRate) > GC_DIFF_RATE)
			{
				bSave = TRUE;
			}
		}
	}


	if(bSave)
	{
		GC_SaveData();
	}

	//Predict discharge time in minute
	fLoadCurr = g_pGcData->RunInfo.Rt.fSumOfCurr 
		- g_pGcData->RunInfo.Bt.fSumOfCurr;

	if(fLoadCurr < MIN_LOAD_CURR)
	{
		fDischTime = MAX_DISCH_TIME*SECONDS_PER_HOUR;
	}
	else
	{
		//fDischCoef = GetDischCoef(-fLoadCurr,
		//						fTotalRatedCap,
		//						afDischCurve);
		
		//fDischTime = fTotalCap * fDischCoef / fLoadCurr;

		fDeltaCap  = GetDeltaCap(-fLoadCurr,
			fTotalRatedCap,
			afDischCurve);
		fDischTime = fTotalCap /fDeltaCap*10;
	}

	SIG_ENUM enumBattPredictEn;
	bResult = GC_GetEnumValueWithReturn(TYPE_OTHER, 0, GC_PUB_BATT_PREDICT_EN,&enumBattPredictEn);
	if((bResult == TRUE)&&(enumBattPredictEn == GC_ENABLED ))
	{
		bResult = BattDischTimePredict(fLoadCurr ,fDischTime, fTotalRatedCap,&fDischTimeVolt);
		if(bResult == TRUE)
		{
			g_pGcData->RunInfo.BtPredict.AdjustCoeff =  fDischTimeVolt/fDischTime;
		}
		fDischTime = g_pGcData->RunInfo.BtPredict.AdjustCoeff*fDischTime;

	}
	fDischTime = fDischTime/SECONDS_PER_HOUR;
	if(fDischTime > MAX_DISCH_TIME)
	{
		fDischTime = MAX_DISCH_TIME;
	}
	
	if(!isnan(fDischTime))
	{
		GC_SetFloatValue(TYPE_OTHER, 0, BT_PUB_PREDICT_DISCH_TIME, fDischTime);
	}
	

	return;
}


/*==========================================================================*
 * FUNCTION : GetDischCoef
 * PURPOSE  : Calculate Discharge coefficient according to fCurrRate
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: float  fCurrRate   : 
 *            float  *pfTimeCoef : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-10 14:32
 *==========================================================================*/
static float GetDischCoef(float fCurr, float fRatedCap, float *pfTimeCoef)
{
	int 		i;
	float 		fCoef;
	float		fDischCurr;

	float 		afCurrRate[GC_CURVE_POINT_NUM] 
					= {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};

	float 		afCoef[GC_CURVE_POINT_NUM];

	fDischCurr = 0.0 - fCurr;

	for(i = 0; i < GC_CURVE_POINT_NUM; i++)
	{
		afCoef[i] = afCurrRate[i] * pfTimeCoef[i];
	}

	if(fDischCurr > (fRatedCap * afCurrRate[GC_CURVE_POINT_NUM - 1]))
	{
		return afCoef[GC_CURVE_POINT_NUM - 1];
	}

	else if(fDischCurr < (fRatedCap * afCurrRate[0]))
	{
		return afCoef[0];
	}
	else
	{
		for(i = 1; i < CURVE_POINT_NUM - 1; i++)
		{
			if(fDischCurr <= (fRatedCap * afCurrRate[i]))
			{
				break;
			}
		}
	}

	fCoef = afCoef[i - 1] 
			+ (fDischCurr - fRatedCap * afCurrRate[i - 1]) 
			* (afCoef[i] - afCoef[i - 1])
			/ (fRatedCap * afCurrRate[i] - fRatedCap * afCurrRate[i - 1]);

	if(fCoef < 0)
	{
		fCoef = -fCoef; 
	}

	return fCoef;
}

/*==========================================================================*
* FUNCTION : GetDeltaCap
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float  fCurrRate   : 
*            float  *pfTimeCoef : 
* RETURN   : static float : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-11-10 14:32
*==========================================================================*/
static float GetDeltaCap(float fCurr, float fRatedCap, float *pfTimeCoef)
{
	int 		i,iBattCurrPrecentBase;
	float 		fCoef,fBattCurrPercent,fdTime;
	float		fDischCurr;

	float 		afCurrRate[GC_CURVE_POINT_NUM] 
	= {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};

	fDischCurr = 0.0 - fCurr;

	if( fRatedCap == 0)
	{
		return 0;
	}
	fBattCurrPercent =  fDischCurr / fRatedCap*10;
	iBattCurrPrecentBase = (int)fBattCurrPercent;

	if(iBattCurrPrecentBase < 1)									
	{
		fdTime = 10 / fBattCurrPercent;		/*������*ʱ��=��������*/
	}
	else if(iBattCurrPrecentBase  > 9)
	{

		fdTime = pfTimeCoef[GC_CURVE_POINT_NUM-1] * 10 / fBattCurrPercent;	/*������*ʱ��=��������*/
	}
	else
	{
		
		fdTime = pfTimeCoef[iBattCurrPrecentBase-1] 
			+ (pfTimeCoef[iBattCurrPrecentBase] - pfTimeCoef[iBattCurrPrecentBase-1])
			* (fBattCurrPercent - (float)iBattCurrPrecentBase);		/*�ŵ�ʱ��:���ŵ����߲�ֵ����*/
	}

	if(fdTime < 0.05)							/*��������*/
	{
		fdTime = 0.05;							/*��Сȡ0.05Сʱ*/
	}

	return	fRatedCap/ fdTime / 360;				/*�ŵ������仯��*/
	
}

/*==========================================================================*
 * FUNCTION : MaintainBattParam
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-26 10:36
 *==========================================================================*/
static	void MaintainBattParam()
{
	int				i;
	DWORD			dwBattTypeNo;
	GC_BATT_PARAM	*pBattParam;

	dwBattTypeNo = GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_TYPE_NO);
	GC_ASSERT(((dwBattTypeNo - 1) < (DWORD)(g_pGcData->PriCfg.iBattTypeNum)),
			ERR_CTL_CFG_BATT_TYPE_NO,
			"Battery Type No. exceeds the number defined gen_ctl.cfg!\n");

	if(g_pGcData->RunInfo.Bt.dwBattTypeNo != dwBattTypeNo)
	{
		g_pGcData->RunInfo.Bt.dwBattTypeNo = dwBattTypeNo;

		pBattParam = &(g_pGcData->PriCfg.pBattParam[dwBattTypeNo - 1]);

		//GC_SetFloatValue(TYPE_OTHER, 
		//				0,
		//				BT_PUB_RATED_CAP, 
		//				pBattParam->fRatedCap);

		for(i = 0; i < GC_Get_Total_BattNum(); i++)
		{
			GC_SetFloatValue(TYPE_BATT_UNIT, 
						i, 
						BT_PRI_RATED_CAP, 
						pBattParam->fRatedCap);
		}


		GC_SetFloatValue(TYPE_OTHER, 
						0,
						BT_PUB_OVER_CURR, 
						pBattParam->fOverCurr);

		GC_SetFloatValue(TYPE_OTHER, 
						0,
						BT_PUB_CURR_LMT, 
						pBattParam->fCurrLmt);

		GC_SetFloatValue(TYPE_OTHER, 
						0,
						DC_PUB_UNDER_VOLT, 
						pBattParam->fUnderVoltage);

		GC_SetFloatValue(TYPE_OTHER, 
						0,
						DC_PUB_VERY_HIGH_VOLT, 
						pBattParam->fOverVoltage);

		GC_SetFloatValue(TYPE_OTHER, 
						0,
						DC_PUB_VERY_UNDER_VOLT, 
						pBattParam->fVeryUnderVoltage);

		GC_SetFloatValue(TYPE_OTHER, 
						0,
						DC_PUB_HIGH_VOLT, 
						pBattParam->fHighVoltage);

		GC_SetFloatValue(TYPE_OTHER, 
						0,
						BT_PUB_CAP_COEF, 
						pBattParam->fCapCoef);

		//Modify battery type param of gen_ctl.cfg 12/19/2005
		//Add Battery name, FC Voltage, BC Voltage, Undervoltage1, 
		//UnderVoltage2, Over Voltage1,Over Voltage2, LVD1, LVD2.

		GC_SetFloatValue(TYPE_OTHER, 
						0,
						BT_PUB_NOM_VOLT, 
						pBattParam->fFCVoltage);

		GC_SetFloatValue(TYPE_OTHER, 
						0,
						BT_PUB_BST_VOLT, 
						pBattParam->fBCVoltage);
		/*
		GC_SetFloatValue(TYPE_OTHER, 
						0,
						LVD1_PUB_VOLT, 
						pBattParam->fLVD1);
		GC_SetFloatValue(TYPE_OTHER, 
						0,
						LVD2_PUB_VOLT, 
						pBattParam->fLVD2);*/
		
		for(i = 0; i < 10; i++)
		{
			GC_SetFloatValue(TYPE_OTHER, 
							0,
							BT_PUB_DISCH_TIME01 + i, 
							pBattParam->afDischCurve[i]);
		}
	}

}

/*==========================================================================*
 * FUNCTION : TimerResetCap
 * PURPOSE  : to refresh SEC_TMR_ID_RESET_CAP timer
 * CALLS    : 
 * CALLED BY: GC_RunInfoRefresh
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-21 15:19
 *==========================================================================*/
static void TimerResetCap()
{
	GC_RUN_INFO_RECT	*pRectRunInfo;

	//For reset battery capacity
	if(SEC_TMR_TIME_OUT
		== GC_GetStateOfSecTimer(SEC_TMR_ID_RESET_CAP))
	{
		g_pGcData->RunInfo.Bt.bNeedRegulateCap = TRUE;
		GC_SuspendSecTimer(SEC_TMR_ID_RESET_CAP);
		return;
	}	

	if(ST_FLOAT != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
	//Not in Float Charge, it's not possible to reset capacity.
	{
		GC_SuspendSecTimer(SEC_TMR_ID_RESET_CAP);
		return;
	}

	if(((GC_ENABLED
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_AUTO_BC_ENB))
		|| (GC_ENABLED
			== GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CYC_BC_ENB)))
			&&(!(g_pGcData->PriCfg.bSlaveMode))) 
	{
		GC_SuspendSecTimer(SEC_TMR_ID_RESET_CAP);
		return;
	}

	pRectRunInfo = &(g_pGcData->RunInfo.Rt);
	if((pRectRunInfo->fCurrLmt * pRectRunInfo->iQtyOnWorkRect
		* pRectRunInfo->fRatedCurr / 100)
		< pRectRunInfo->fSumOfCurr * 1.05)
	// Current limitation
	{
		GC_SuspendSecTimer(SEC_TMR_ID_RESET_CAP);
		return;
	}

	if(!(g_pGcData->RunInfo.Bt.bZeroCurr))
	//Battery current is not 0
	{
		GC_SuspendSecTimer(SEC_TMR_ID_RESET_CAP);
		return;
	}

	if(SEC_TMR_TIME_IN
		!= GC_GetStateOfSecTimer(SEC_TMR_ID_RESET_CAP))
	{
		GC_SetSecTimer(SEC_TMR_ID_RESET_CAP, 
						INTERVAL_RESET_CAP,
						NULL,
						0);
		return;
	}
	return;
}

int GC_GetPredictDuration(int iIdx)
{
	UNUSED(iIdx);
	if(GC_IsGCSigExist(TYPE_OTHER, 
				0,
				GC_PUB_BATT_PREDICT_DELAY))
	{
		return SECONDS_PER_MIN 
			* GC_GetDwordValue(TYPE_OTHER, 
			0,
			GC_PUB_BATT_PREDICT_DELAY);
	}
	else
	{
		return SECONDS_PER_MIN * 40;
	}
}
/*==========================================================================*
* FUNCTION : BattDischTimePredict
* PURPOSE  :
* CALLS    :
* CALLED BY:
* ARGUMENTS:
* RETURN   :
* COMMENTS :
* CREATOR  :
*==========================================================================*/
static BOOL BattDischTimePredict(float BattCurr ,float DischTime,float BattCap,float* PredictResult)
{
	float fBusVolt, fVoltPredict, ftemp,ftemp1;
	float fRealTimeDelta,fCurveTimeDelta;
	SIG_ENUM	enumState;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	static int  istep =0;
	static time_t	tTimeLast;
	time_t	tTimeNow;
	int iBattCellNum,i;
	BATTPREDICT_DIVISION  PredictCoeff[BATT_PREDICT_COEFF_NUM]={
		{2.150,	1.005},	{2.100,	1.01},	{2.050,	1.02},
		{2.000,	1.05},	{1.950,	1.10},	{1.930,	1.15},
		{1.900,	1.25},	{1.850,	1.50},	{1.800,	1.70},
		{1.750,	2.00},	{1.700,	2.60},	{1.650,	3.20},
		};
	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		iBattCellNum =  BATT_24V_CELL_NUM ;
	}
	else
	{
		iBattCellNum =	BATT_48V_CELL_NUM ;
	}

	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);
	if(enumState != ST_PLAN_TEST)//��ʱ����ͬʱ���е��Ԥ��
	{
		istep =0;
		GC_SuspendSecTimer(SEC_TMR_ID_BATT_DISCHARGE_TIME);
		return FALSE;
	}

	if(g_pGcData->RunInfo.Bt.bDischarge == FALSE) //��Ҫ��طŵ�
	{
		istep =0;
		GC_SuspendSecTimer(SEC_TMR_ID_BATT_DISCHARGE_TIME);
		return FALSE;
	}
	
	if(SEC_TMR_SUSPENDED 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_BATT_DISCHARGE_TIME))//��Ҫ��طŵ�һ��ʱ������Ԥ��
	{
		istep =0;
		g_pGcData->RunInfo.BtPredict.BattCurrMin = 1000000;
		g_pGcData->RunInfo.BtPredict.BattCurrMax  = 0;
		GC_SetSecTimer(SEC_TMR_ID_BATT_DISCHARGE_TIME,
			0,
			GC_GetPredictDuration,
			0);
		return FALSE;
	}

	g_pGcData->RunInfo.BtPredict.LVDVolt = 0;
	float fLvdVolt;
	BOOL bRTN;
	if (GC_EQUIP_EXIST != GC_IsGCEquipExist(TYPE_OTHER, 0, LDU_PUB_EXISTENCE,GC_EQUIP_INVALID))
	{
		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{	
			if(g_pGcData->RunInfo.Lvd.abValid[i * 2])
			{
				//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
				if(stVoltLevelState)//24V
				{
					bRTN = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_VOLT24 ,
						&fLvdVolt );
					
				}
				else
				{
					bRTN  = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_VOLT ,
						&fLvdVolt);

				}
				if((bRTN == TRUE)&&(fLvdVolt > g_pGcData->RunInfo.BtPredict.LVDVolt))
					g_pGcData->RunInfo.BtPredict.LVDVolt = fLvdVolt;
							
			}
			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
			{
				//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
				if(stVoltLevelState)//24V
				{
					bRTN = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_VOLT24 +1 ,
						&fLvdVolt );

				}
				else
				{
					bRTN  = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_VOLT +1,
						&fLvdVolt);

				}
				if((bRTN == TRUE)&&(fLvdVolt > g_pGcData->RunInfo.BtPredict.LVDVolt))
					g_pGcData->RunInfo.BtPredict.LVDVolt = fLvdVolt;
							
			}		
		}
	}
	else //LDU
	{
		
			//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
			if(stVoltLevelState)//24V
			{
				bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
					0,
					LDU_LVD1_PRI_VOLT24 ,
					&fLvdVolt );
			}
			else
			{
				bRTN  = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
					0,
					LDU_LVD1_PRI_VOLT ,
					&fLvdVolt);

			}

			if((bRTN == TRUE)&&(fLvdVolt > g_pGcData->RunInfo.BtPredict.LVDVolt))
				g_pGcData->RunInfo.BtPredict.LVDVolt = fLvdVolt;

			//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
			if(stVoltLevelState)//24V
			{
				bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
					0,
					LDU_LVD1_PRI_VOLT24+ 1  ,
					&fLvdVolt );
			}
			else
			{
				bRTN  = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
					0,
					LDU_LVD1_PRI_VOLT+ 1,
					&fLvdVolt);

			}
			if((bRTN == TRUE)&&(fLvdVolt > g_pGcData->RunInfo.BtPredict.LVDVolt))
				g_pGcData->RunInfo.BtPredict.LVDVolt = fLvdVolt;
		

	}
	if( g_pGcData->RunInfo.BtPredict.LVDVolt==0)
	{
		return FALSE;
	}
	float fCellVoltofLVD;
	fCellVoltofLVD = g_pGcData->RunInfo.BtPredict.LVDVolt/iBattCellNum;
	for(i = 0;i< BATT_PREDICT_COEFF_NUM;i++)
	{
		if(fCellVoltofLVD>=PredictCoeff[i].fCellVolt)
		{
			if(i==0)
			{
				return FALSE;
			}
			g_pGcData->RunInfo.BtPredict.LVDPredictDivision = PredictCoeff[i-1].fDivCoeff 
				 + (fCellVoltofLVD-PredictCoeff[i-1].fCellVolt)
				 /(PredictCoeff[i].fCellVolt-PredictCoeff[i-1].fCellVolt)
				 *(PredictCoeff[i].fDivCoeff-PredictCoeff[i-1].fDivCoeff);
			break;
		}
	}
	if(i>=BATT_PREDICT_COEFF_NUM)
	{
		return FALSE;
	}


	if(SEC_TMR_TIME_OUT 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_BATT_DISCHARGE_TIME))
	{
		g_pGcData->RunInfo.BtPredict.BattCurrMin = MIN(g_pGcData->RunInfo.BtPredict.BattCurrMin,BattCurr);
		g_pGcData->RunInfo.BtPredict.BattCurrMax = MAX(g_pGcData->RunInfo.BtPredict.BattCurrMax,BattCurr);
		if((g_pGcData->RunInfo.BtPredict.BattCurrMax -g_pGcData->RunInfo.BtPredict.BattCurrMin)>(0.1*BattCap))
		{
			istep =0;//����ص��������ϴ�ʱ������Ԥ��
		}
		if(istep==0)//��¼��ǰĸ�ŵ�ѹ��Ŀǰʱ��
		{
			g_pGcData->RunInfo.BtPredict.BusVoltLast = g_pGcData->RunInfo.fSysVolt;
			tTimeLast  = time(NULL);
			istep++;
		}
		else if(istep==1)
		{
			fBusVolt = g_pGcData->RunInfo.fSysVolt;
			if((g_pGcData->RunInfo.BtPredict.BusVoltLast-fBusVolt)>BATT_PREDICT_CELL_VOLT_DELTA*iBattCellNum)//Ԥ��ʱ���ε�ѹ��Ҫ��ֵ0.5V
			{
				tTimeNow  = time(NULL);
				g_pGcData->RunInfo.BtPredict.PredictSlop = (g_pGcData->RunInfo.BtPredict.BusVoltLast -fBusVolt)/(tTimeNow-tTimeLast);
				ftemp = (g_pGcData->RunInfo.fSysVolt-BATT_PREDICT_CELL_VOLT_DELTA*iBattCellNum)/iBattCellNum;
				for(i = 0;i< BATT_PREDICT_COEFF_NUM;i++)
				{
					if(ftemp>=PredictCoeff[i].fCellVolt)
					{
						if(i==0)
						{
							return FALSE;
						}
						g_pGcData->RunInfo.BtPredict.PredictDivision = PredictCoeff[i].fDivCoeff;
						g_pGcData->RunInfo.BtPredict.NextVolt = PredictCoeff[i].fCellVolt*iBattCellNum;
						break;
					}
				}
				if(i>=BATT_PREDICT_COEFF_NUM)
				{
					return FALSE;
				}
				g_pGcData->RunInfo.BtPredict.TimetoNextVolt= ( fBusVolt - g_pGcData->RunInfo.BtPredict.NextVolt)
					/(g_pGcData->RunInfo.BtPredict.PredictSlop*g_pGcData->RunInfo.BtPredict.PredictDivision)
					*g_pGcData->RunInfo.BtPredict.PredictCR;
				g_pGcData->RunInfo.BtPredict.TimetoLVDVolt= ( fBusVolt -g_pGcData->RunInfo.BtPredict.LVDVolt)
					/(g_pGcData->RunInfo.BtPredict.PredictSlop*g_pGcData->RunInfo.BtPredict.PredictDivision)
					*g_pGcData->RunInfo.BtPredict.PredictCR;

				g_pGcData->RunInfo.BtPredict.BusVoltLast = g_pGcData->RunInfo.fSysVolt;
				tTimeLast = tTimeNow;
				g_pGcData->RunInfo.BtPredict.TimeCurveLast = DischTime;
				istep++;
			}
			else
			{
				return FALSE;
			}

		}
		else if(istep == 2)
		{
			fBusVolt = g_pGcData->RunInfo.fSysVolt;
			if(fBusVolt <g_pGcData->RunInfo.BtPredict.NextVolt)
			{
				tTimeNow  = time(NULL);
				g_pGcData->RunInfo.BtPredict.PredictSlop = (g_pGcData->RunInfo.BtPredict.BusVoltLast-fBusVolt )/(tTimeNow-tTimeLast);
				ftemp= (g_pGcData->RunInfo.fSysVolt-BATT_PREDICT_CELL_VOLT_DELTA*iBattCellNum)/iBattCellNum;
				for(i=0;i<BATT_PREDICT_COEFF_NUM;i++)
				{
					if(ftemp>=PredictCoeff[i].fCellVolt)
					{
						if(i==0)
						{
							return FALSE;
						}
						g_pGcData->RunInfo.BtPredict.PredictDivision = PredictCoeff[i].fDivCoeff;
						g_pGcData->RunInfo.BtPredict.NextVolt = PredictCoeff[i].fCellVolt*iBattCellNum;
							break;
					}
				}
				if(i>=BATT_PREDICT_COEFF_NUM)
				{
					return FALSE;
				}

				fRealTimeDelta = (float)(tTimeNow - tTimeLast);
				fCurveTimeDelta = DischTime - g_pGcData->RunInfo.BtPredict.TimeCurveLast;
				ftemp = fCurveTimeDelta - fRealTimeDelta;
				if(ftemp<0)
				{
					ftemp = -ftemp;
				}
				ftemp1 = g_pGcData->RunInfo.BtPredict.TimetoNextVolt - fRealTimeDelta;
				if(ftemp1<0)
				{
					ftemp1 = -ftemp1;
				}
				if(ftemp>ftemp1)
				{
					*PredictResult = g_pGcData->RunInfo.BtPredict.TimetoLVDVolt;
				}

				g_pGcData->RunInfo.BtPredict.PredictCR = fRealTimeDelta / g_pGcData->RunInfo.BtPredict.TimetoNextVolt;

				g_pGcData->RunInfo.BtPredict.TimetoNextVolt= (fBusVolt -g_pGcData->RunInfo.BtPredict.NextVolt)
					/(g_pGcData->RunInfo.BtPredict.PredictSlop*g_pGcData->RunInfo.BtPredict.PredictDivision)
					*g_pGcData->RunInfo.BtPredict.PredictCR;
				g_pGcData->RunInfo.BtPredict.TimetoLVDVolt= (fBusVolt -g_pGcData->RunInfo.BtPredict.LVDVolt)
					/(g_pGcData->RunInfo.BtPredict.PredictSlop*g_pGcData->RunInfo.BtPredict.PredictDivision)
					*g_pGcData->RunInfo.BtPredict.PredictCR;

				g_pGcData->RunInfo.BtPredict.BusVoltLast = g_pGcData->RunInfo.fSysVolt;
				tTimeLast = tTimeNow;
				g_pGcData->RunInfo.BtPredict.TimeCurveLast = DischTime;

				if(ftemp<=ftemp1)
				{
					return FALSE;	
				}

			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}
			
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
* FUNCTION : GC_BattPredictInit
* PURPOSE  : initdata
CRC12 code, if success, return TRUE.
* CALLS    : Crc12, fopen, fread
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : 
* COMMENTS : 
* CREATOR  :                DATE: 
*==========================================================================*/
void GC_BattPredictInit(void)
{	
	g_pGcData->RunInfo.BtPredict.BattCurrMin = 1000000;
	g_pGcData->RunInfo.BtPredict.BattCurrMax  = 0;
	g_pGcData->RunInfo.BtPredict.PredictCR =1;
	g_pGcData->RunInfo.BtPredict.AdjustCoeff = 1;
	
}
/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_lvd.h
 *  CREATOR  : Frank Cao                DATE: 2004-11-03 11:10
 *  VERSION  : V1.00
 *  PURPOSE  : To provide LVD interface function
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_LVD_H_
#define _GC_LVD_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define	GC_CONNECTED	0
#define	GC_DISCONNECTED	1

#define	GC_ALL_CONNECTED	0
#define	GC_PART_CONNECTED	1
#define	GC_ALL_DISCONNECTED	2


#define	GC_BY_VOLT		0
#define	GC_BY_TIME		1

#ifdef   GC_SUPPORT_HIGHTEMP_DISCONNECT
#define	GC_RECONN_DELTA_BETWEEN_AMBINE_BATT			(7.0)
#define	GC_MAX_DELTA_BETWEEN_AMBINE_BATT			(10.0)
#define GC_OB_LVD2						(1)
#endif

enum	LVD_INDEX
{
	LVD_OUT1 = 0,
	LVD_OUT2,
	LVD_OUT3,
};

void GC_LvdInit(void);
void GC_LvdRun(void);

int GC_GetLvdTime(int iIdx);
int GC_GetReconnectDelay(int iIdx);

#endif //_GC_LVD_H_

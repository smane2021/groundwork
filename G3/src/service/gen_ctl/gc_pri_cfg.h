/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_get_pri_cfg.h
 *  CREATOR  : Frank Cao                DATE: 2004-10-17 15:43
 *  VERSION  : V1.00
 *  PURPOSE  : Read and parse private configuration file, initialize
 *             g_GcData->PriCfg
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_GET_PRI_CFG_
#define _GC_GET_PRI_CFG_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define	GC_MATCHED				0
#define	GC_SPLITTER				('\t')
#define	GC_MAX_NUM_PER_FIELD	50

enum	_GC_CFG_SIG_DEF
{
	GC_CFG_RECT_PRI_SIG = 0,
	GC_CFG_S1_RECT_PRI_SIG,
	GC_CFG_S2_RECT_PRI_SIG,
	GC_CFG_S3_RECT_PRI_SIG,
	GC_CFG_CT_PRI_SIG,
	GC_CFG_BATT_PRI_SIG,
	GC_CFG_LVD_PRI_SIG,
	GC_CFG_BATT_FUSE_PRI_SIG,
	GC_CFG_EIB_PRI_SIG,
#ifdef GC_SUPPORT_MPPT
	GC_CFG_MPPT_PRI_SIG,
#endif

#ifdef GC_SUPPORT_BMS
	GC_CFG_BMS_PRI_SIG,
#endif

	GC_CFG_SIG,

	GC_CFG_SIG_DEF_NUM,
};

enum	_GC_CFG_DATA_TYPE
{
	GC_CFG_BATT_PARAM = 0,
	GC_CFG_ENGY_SAV_OUTPUT,
	GC_CFG_REDUCE_CONSUMPTION_ACTION,
	GC_CFG_LIMIT_MAX_POWER_ACTION,
	GC_CFG_ENGY_SAV_MODE,
	GC_CFG_ENGY_SAV_SCHEDULE,
	GC_CFG_POWER_SPLIT_INPUT,
	GC_CFG_EMERG_SHUTDOWN_INPUT,
	GC_CFG_LVD_VERY_HI_TEMP,
	GC_CFG_FIXED_RELAY_OUTPUT,

	GC_CFG_NUM,
};

/*Battery Parameter*/
struct _GC_BATT_PARAM_
{
//#define GC_CFG_MAX_BATT_NAME_LEN			32
	float				fRatedCap;
	float				fOverCurr;
	float				fCurrLmt;
	float				fCapCoef;
	float				fFCVoltage;
	float				fBCVoltage;
	float				fUnderVoltage;
	float				fVeryUnderVoltage;
	float				fOverVoltage;
	float				fHighVoltage;
	/*float				fLVD1;
	float				fLVD2;
	char				szBatteryName[GC_CFG_MAX_BATT_NAME_LEN];*/
	float				afDischCurve[10];
};
typedef struct _GC_BATT_PARAM_ GC_BATT_PARAM;

/*Signal Type and ID*/
struct	tagGC_SIGNAL_ID_TYPE		
{
	int				iEquipTypeId;		//Equipment Type Id
	int				iSignalType;		//Signal Type
	int				iSignalID;			//Signal ID
};
typedef struct tagGC_SIGNAL_ID_TYPE GC_SIGNAL_ID_TYPE;


struct _GC_CFG_INDEX
{
	int		iIndex;
	char*	szName;
};
typedef struct _GC_CFG_INDEX GC_CFG_INDEX;

struct _GC_CFG_RELAY_INDEX
{
	int		iEquipID;
	int		iSignalType;
	int		iSignalID;
	int		iAlarmID;
	
};
typedef struct _GC_CFG_RELAY_INDEX GC_CFG_RELAY_INDEX;



#define	ENGY_SAV_ON			1
#define	ENGY_SAV_OFF		0
#define	ENGY_SAV_NO_ACT		(-1)

enum	_ENGY_SAV_RATE_TYPE
{
	ENGY_SAV_RATE_PEAK = 0,
	ENGY_SAV_RATE_HIGH,
	ENGY_SAV_RATE_FLAT,
	ENGY_SAV_RATE_LOW,
};

enum	_ENGY_SAV_DAY_MODE
{
	ENGY_SAV_MODE_A = 0,
	ENGY_SAV_MODE_B,
	ENGY_SAV_MODE_C,
	ENGY_SAV_MODE_D,
	ENGY_SAV_MODE_E,
	ENGY_SAV_MODE_F,
	ENGY_SAV_MODE_G,
	ENGY_SAV_MODE_H,

	ENGY_SAV_MODE_NUM,
};

#define	ENGY_SAV_MAX_POWER	100000.0

struct	tagRATE_MAX_POWER
{
	SIG_ENUM		enumRateType;
	float			fMaxPower;
};
typedef struct tagRATE_MAX_POWER RATE_MAX_POWER;

struct	tagGC_SIG_INFO
{
	BOOL		bEnabled;
	int			iEquipId;
	int			iSignalType;
	int			iSignalId;
};
typedef struct tagGC_SIG_INFO GC_SIG_INFO;

struct	tagREDUCE_CONSUMPTION_ACTION		
{
	SIG_ENUM		enumOutput1;			//Output 1 Action
	SIG_ENUM		enumOutput2;			//Output 2 Action
	SIG_ENUM		enumOutput3;			//Output 3 Action
	BOOL			bProhibitBattChg;		//Prohibit Battery Charge
};
typedef struct tagREDUCE_CONSUMPTION_ACTION GC_REDUCE_CONSUMPTION_ACTION;

struct	tagLIMIT_MAX_POWER_ACTION		
{
	SIG_ENUM		enumOutput1;			//Output 1 Action
	SIG_ENUM		enumOutput2;			//Output 2 Action
	SIG_ENUM		enumOutput3;			//Output 3 Action
};
typedef struct tagLIMIT_MAX_POWER_ACTION GC_LIMIT_MAX_POWER_ACTION;

#define	MONTHS_PER_YEAR		12

struct	tagENGY_SAV_MODE
{
	RATE_MAX_POWER	aHourDef[ENGY_SAV_MODE_NUM];
};
typedef struct tagENGY_SAV_MODE GC_ENGY_SAV_MODE;

struct	tagENGY_SAV_SCHEDULE
{
	SIG_ENUM	aenumMode[MONTHS_PER_YEAR];
};
typedef struct tagENGY_SAV_SCHEDULE GC_ENGY_SAV_SCHEDULE;

struct _GC_PRI_CFG
{
	/*The Equipment Type ID*/
	int							iEquipTypeIdRect;
	int							iEquipTypeIdBatt;
	int							iEquipTypeIdLvd;
	int							iEquipTypeIdBattFuse;
	int							iEquipTypeIdEib;
	int							iEquipTypeIdS1Rect;
	int							iEquipTypeIdS2Rect;
	int							iEquipTypeIdS3Rect;
	int							iEquipTypeIdCT;
#ifdef GC_SUPPORT_MPPT
	int							iEquipTypeIdMppt;
#endif

#ifdef GC_SUPPORT_BMS
	int							iEquipTypeIdBMS;
#endif

	/*Signal Type and signal ID, gc_sig_id_def.h defines the order*/
	int							iLvdPriSigNum;
	GC_SIGNAL_ID_TYPE			*pLvdPriSig;

	int							iBattFusePriSigNum;
	GC_SIGNAL_ID_TYPE			*pBattFusePriSig;

	int							iRectPriSigNum;
	GC_SIGNAL_ID_TYPE			*pRectPriSig;
	
	int							iS1RectPriSigNum;
	GC_SIGNAL_ID_TYPE			*pS1RectPriSig;

	int							iS2RectPriSigNum;
	GC_SIGNAL_ID_TYPE			*pS2RectPriSig;

	int							iS3RectPriSigNum;
	GC_SIGNAL_ID_TYPE			*pS3RectPriSig;

	int							iBattPriSigNum;
	GC_SIGNAL_ID_TYPE			*pBattPriSig;

	int							iEibPriSigNum;
	GC_SIGNAL_ID_TYPE			*pEibPriSig;

	int					iCTPriSig;
	GC_SIGNAL_ID_TYPE			*pCTPriSig;

#ifdef GC_SUPPORT_MPPT
	int					iMpptPriSigNum;
	GC_SIGNAL_ID_TYPE			*pMpptPriSig;
#endif
#ifdef GC_SUPPORT_BMS
	int					iBMSPriSigNum;
	GC_SIGNAL_ID_TYPE			*pBMSPriSig;
#endif

	int							iSigNum;
	GC_SIG_INFO					*pSig;

	/*Battery Type Configuration*/
	int							iBattTypeNum;
	GC_BATT_PARAM				*pBattParam;
	
	/*Energy Saving Output*/
	int							iEnergySavingOutputNum;
	GC_SIG_INFO					*pEngySavOutput;
	
	/*Reduce Consumption Action*/
	int							iReduceConsumptionActNum;
	GC_REDUCE_CONSUMPTION_ACTION	*pReduceConsumptionAct;

	/*Reduce Consumption Action*/
	int							iLmtMaxPowerActNum;
	GC_LIMIT_MAX_POWER_ACTION	*pLmtMaxPowerAct;

	/*Energy Saving Mode*/
	int							iEngySavModeNum;
	GC_ENGY_SAV_MODE			*pEngySavMode;

	/*Energy Saving Schedule*/
	int							iEngySavScheduleNum;
	GC_ENGY_SAV_SCHEDULE		*pEngySavSchedule;

	/*Power Split Input*/
	int							iPowerSplitInputNum;	
	GC_SIG_INFO					*pPowerSplitInput;

	/*Emergency Shut-down Input*/
	int							iEstopEshutdownInputNum;	
	GC_SIG_INFO					*pEstopEshutdownInput;
	
	int							iLvdNumOnVeryHiTemp;	
	GC_SIG_INFO					*pLvdOnVeryHiTemp;

	int							iSigNumFixedRelayOutput;	
	GC_SIG_INFO					*pFixedRelayOutput;

	/*Power Split Mode*/
	BOOL						bSlaveMode;

	BOOL						bMainSwitchEnb;
	
	/*Period of Energy Saving*/
	int							iEnergySavingPeriod;

};
typedef struct _GC_PRI_CFG GC_PRI_CFG;

/*Equipment Type*/
struct	tagPRIMARY_UNIT_SIG_TYPE_ID		
{
	char*			pName;				//Name of Signal
	int				iType;				//Signal Type
	int				iId;				//Signal Id
};
typedef struct tagPRIMARY_UNIT_SIG_TYPE_ID PRIMARY_UNIT_SIG_TYPE_ID;

/*Equipment Type Id Signal Id and Signal Type, this is for rectifier unit 
  signals and battery unit signals*/
struct	tagPRIMARY_SIG_ID_TYPE		
{
	char*			pName;				//Name of Signal
	int				iEquipTypeId;		//Equipment Type Id
	int				iType;				//Signal Type
	int				iId;				//Signal Id
};
typedef struct tagPRIMARY_SIG_ID_TYPE PRIMARY_SIG_ID_TYPE;

/*Equipment Id Signal Id and Signal Type, this is for the all of signals
  but expect rectifier unit signals and battery unit signals*/
struct	tagPRIMARY_SIG_INFO		
{
	char*			pName;				//Name of Signal
	int				iEquipId;			//Equipment Id
	int				iType;				//Signal Type
	int				iId;				//Signal Id
};
typedef struct tagPRIMARY_SIG_INFO PRIMARY_SIG_INFO;

struct _PRIMARY_PRI_CFG
{
	/*Signal Type and signal ID, the order depend on gen_ctl.cfg*/
	int					iRectPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pRectPriSig;

	int					iS1RectPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pS1RectPriSig;
	
	int					iS2RectPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pS2RectPriSig;
	
	int					iS3RectPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pS3RectPriSig;

	int			iCTPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pCTPriSig;

	int					iBattPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pBattPriSig;

	int					iLvdPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pLvdPriSig;

	int					iBattFusePriSigNum;
	PRIMARY_SIG_ID_TYPE	*pBattFusePriSig;

	int					iEibPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pEibPriSig;

	int					iSigNum;
#ifdef GC_SUPPORT_MPPT
	int					iMpptPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pMpptPriSig;
#endif

#ifdef GC_SUPPORT_BMS
	int					iBMSPriSigNum;
	PRIMARY_SIG_ID_TYPE	*pBMSPriSig;
#endif

	PRIMARY_SIG_INFO	*pSig;

};
typedef struct _PRIMARY_PRI_CFG PRIMARY_PRI_CFG;

void	GC_PriCfgInit(void);
void	GC_PriCfgDestroy(void);

#endif //_GC_GET_PRI_CFG_


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_rect_list.c
 *  CREATOR  : Frank Cao                DATE: 2004-09-19 10:44
 *  VERSION  : V1.00
 *  PURPOSE  : To provide a group interface functions to create, maintain 
               and destory the AC-on/off rectifier List
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include	"pub_list.h"
#include	"gc_rect_list.h"
#include	"gen_ctl.h"
#include	"gc_batt_mgmt.h"
#include	"gc_index_def.h"
#include	"gc_sig_value.h"

static BOOL	GC_UpdateRectTime(IN GC_RECT_INFO *pRectRunInfo);
/*==========================================================================*
 * FUNCTION : GC_InitRectList
 * PURPOSE  : To initialize the AC-on rectifier list and AC-off rectifier list
 * CALLS    : List_Create, GC_MaintainRectList
 * CALLED BY: GC_Init
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-27 21:21
 *==========================================================================*/
void	GC_InitRectList()
{
	//if(!(g_pGcData->EquipInfo.iCfgQtyOfRect))
	//{
	//	return;	
	//}

	//g_pGcData->RunInfo.Rt.iQtyOfRectSelf = 4 * MAX_NUM_RECT_EQUIP;

	g_pGcData->RunInfo.Rt.hListOnWorkRect = List_Create(sizeof(GC_RECT_INFO));	

	GC_ASSERT(g_pGcData->RunInfo.Rt.hListOnWorkRect,
				ERR_CTL_LIST_CREATE, 
				"List_Create hListOnWorkRect error!!!\n");

	g_pGcData->RunInfo.Rt.iQtyOnWorkRect = 0;

	g_pGcData->RunInfo.Rt.hListOffWorkRect = List_Create(sizeof(GC_RECT_INFO));
	
	GC_ASSERT(g_pGcData->RunInfo.Rt.hListOffWorkRect,
				ERR_CTL_LIST_CREATE, 
				"List_Create hListOffWorkRect error!!!\n");

	g_pGcData->RunInfo.Rt.iQtyOffWorkRect = 0;

	GC_MaintainRectList();
	InitRectTime();

	return;
}


#ifdef GC_SUPPORT_MPPT
/*==========================================================================*
* FUNCTION : GC_InitMpptList
* PURPOSE  : To initialize the AC-on rectifier list and AC-off rectifier list
* CALLS    : List_Create, GC_MaintainRectList
* CALLED BY: GC_Init
* RETURN   :  
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-09-27 21:21
*==========================================================================*/
void	GC_InitMpptList()
{
	


	/*GC_SetFloatValue(TYPE_OTHER, 
		0,
		MT_PUB_MAX_USED_CAP,
		0.0);		

	GC_SetFloatValue(TYPE_OTHER, 
		0,
		MT_PUB_MIN_USED_CAP,
		110.00);		*/

	g_pGcData->RunInfo.Mt.hListOnWorkRect = List_Create(sizeof(GC_MPPT_INFO));	

	GC_ASSERT(g_pGcData->RunInfo.Mt.hListOnWorkRect,
		ERR_CTL_LIST_CREATE, 
		"List_Create Mppt hListOnWorkRect error!!!\n");

	g_pGcData->RunInfo.Mt.iQtyOnWorkRect = 0;

	g_pGcData->RunInfo.Mt.hListOffWorkRect = List_Create(sizeof(GC_MPPT_INFO));

	GC_ASSERT(g_pGcData->RunInfo.Mt.hListOffWorkRect,
		ERR_CTL_LIST_CREATE, 
		"List_Create Mppt hListOffWorkRect error!!!\n");

	g_pGcData->RunInfo.Mt.iQtyOffWorkRect = 0;

	GC_MaintainMpptList();

	return;
}
#endif
/*==========================================================================*
 * FUNCTION : SearchAndRefresh
 * PURPOSE  : Scan the hList, if find a record whose dwSerialNo is equal to
              RectifierInfo.dwSerialNo, refresh the record and return TRUE.
 * CALLS    : List_GotoHead, List_Get, List_Set, List_GotoNext
 * CALLED BY: GC_MaintainRectList
 * ARGUMENTS: GC_RECT_INFO  RectifierInfo : 
 *            HANDLE     hList         : 
 * RETURN   : TRUE: successful 
              FALSE: not find RectifierInfo
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 17:10
 *==========================================================================*/
BOOL SearchAndRefresh(GC_RECT_INFO RectifierInfo, HANDLE hList)
{
	GC_RECT_INFO	RtInfo;
	BOOL			bRst;
	
	List_GotoHead(hList);
	do
	{
		if(List_Get(hList, (void*)(&RtInfo)))
		{
			if((RtInfo.dwSerialNo == RectifierInfo.dwSerialNo)
				&& (RtInfo.dwHiSerialNo == RectifierInfo.dwHiSerialNo)
				&& (RtInfo.iRectEquipType == RectifierInfo.iRectEquipType))
			{
				RectifierInfo.dwSelfOperTime = RtInfo.dwSelfOperTime;
				RectifierInfo.fOperTime = RtInfo.fOperTime;
				bRst = List_Set(hList, (void*)(&RectifierInfo));
				
				GC_ASSERT(bRst,
					ERR_CTL_LIST_SET, 
					"List_Set error!!!\n");		
				
				return TRUE;
			}
		}

	}while(List_GotoNext(hList));

	return	FALSE;
}

BOOL UpdateAndRefresh(GC_RECT_INFO RectifierInfo, HANDLE hList)
{
	GC_RECT_INFO	RtInfo;
	BOOL			bRst;

	List_GotoHead(hList);
	do
	{
		if(List_Get(hList, (void*)(&RtInfo)))
		{
			if((RtInfo.dwSerialNo == RectifierInfo.dwSerialNo)
				&& (RtInfo.dwHiSerialNo == RectifierInfo.dwHiSerialNo)
				&& (RtInfo.iRectEquipType == RectifierInfo.iRectEquipType))
			{
				bRst = List_Set(hList, (void*)(&RectifierInfo));

				GC_ASSERT(bRst,
					ERR_CTL_LIST_SET, 
					"List_Set error!!!\n");		

				return TRUE;
			}
		}

	}while(List_GotoNext(hList));

	return	FALSE;
}

/*==========================================================================*
 * FUNCTION : GC_MaintainRectList
 * PURPOSE  : To maintain the two lists--Swictch_on rectifier list and 
			  Swictch_off rectifier list 
 * CALLS    : List_GotoHead, List_GotoNext, 
              List_Delete, SearchAndRefresh, List_Insert,
 * CALLED BY: GC_InitRectList, RefreshData
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-15 16:57
 *==========================================================================*/
void	GC_MaintainRectList()
{
	int					i, j;
	BOOL				bRst;
	GC_RECT_INFO		RectifierInfo;
	GC_RUN_INFO_RECT	*pRt = &(g_pGcData->RunInfo.Rt);

	GC_RECT_INFO_IDX	aRectIdx[] =
	{
		{TYPE_RECT_UNIT,	RT_PRI_RECT_ID,		RT_PRI_RECT_HI_ID,	RT_PRI_OPER_TIME,	RT_PRI_AC_FAIL,	RT_PRI_AC_ON_OFF_STA,	RT_PRI_DC_ON_OFF_STA,	RT_PRI_FAULT,	RT_PRI_PROTCTION,	},
		{TYPE_SLAVE1_RECT,	S1_PRI_RECT_ID,		S1_PRI_RECT_HI_ID,	S1_PRI_OPER_TIME,	S1_PRI_AC_FAIL,	S1_PRI_AC_ON_OFF_STA,	S1_PRI_DC_ON_OFF_STA,	S1_PRI_FAULT,	S1_PRI_PROTCTION,	},
		{TYPE_SLAVE2_RECT,	S2_PRI_RECT_ID,		S2_PRI_RECT_HI_ID,	S2_PRI_OPER_TIME,	S2_PRI_AC_FAIL,	S2_PRI_AC_ON_OFF_STA,	S2_PRI_DC_ON_OFF_STA,	S2_PRI_FAULT,	S2_PRI_PROTCTION,	},
		{TYPE_SLAVE3_RECT,	S3_PRI_RECT_ID,		S3_PRI_RECT_HI_ID,	S3_PRI_OPER_TIME,	S3_PRI_AC_FAIL,	S3_PRI_AC_ON_OFF_STA,	S3_PRI_DC_ON_OFF_STA,	S3_PRI_FAULT,	S3_PRI_PROTCTION,	},
	};

	int					iQtyCommRect = 0;
	int					iQtyOnWorkRect = 0;
	int					iQtyOffWorkRect = 0;

	int					iQtySelfCommRect = 0;
	int					iQtySelfOnWorkRect = 0;
	int					iQtySelfOffWorkRect = 0;

	GC_RECT_INFO	QueueAcOnRect[MAX_NUM_RECT_EQUIP * MAX_RECT_TYPE_NUM];
	GC_RECT_INFO	QueueAcOffRect[MAX_NUM_RECT_EQUIP * MAX_RECT_TYPE_NUM];

	pRt->iQtyAcOffRect = 0;
	pRt->iQtyNoOutputRect = 0;

	/*Calculate the rect number of AC Failure/Rect failure*/
	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		for(j = 0; j < pRt->Slave[i].iQtyOfRect; j++)
		{
			if(GC_IsRectComm(aRectIdx[i].iRectType, j))
			{
				
				if(GC_ALARM == GC_GetEnumValue(aRectIdx[i].iRectType,
												j,
												aRectIdx[i].iAcFailSid)
					|| GC_ALARM == GC_GetEnumValue(aRectIdx[i].iRectType,
												j,
												aRectIdx[i].iFaultSid))
				{
					pRt->iQtyNoOutputRect++;
				}
			}
		}
	}

	/*Create and stuff the QueueAcOnRect[]/QueueAcOffRect[], if some rectifer 
	 is not in AC-on list / AC-off list, insert them into the list*/
	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		for(j = 0; j < pRt->Slave[i].iQtyOfRect; j++)
		{
			RectifierInfo.iRectEquipType = i;
			RectifierInfo.dwSerialNo 
				= GC_GetDwordValue(aRectIdx[i].iRectType, j, aRectIdx[i].iSerialNoSid);
			RectifierInfo.dwHiSerialNo 
				= GC_GetDwordValue(aRectIdx[i].iRectType, j, aRectIdx[i].iHiSerialNoSid);
			RectifierInfo.dwOperTime 
				= GC_GetDwordValue(aRectIdx[i].iRectType, j, aRectIdx[i].iRunTimeSid);
			RectifierInfo.iRectNo = j;
			
			
			if(GC_IsRectComm(aRectIdx[i].iRectType, j))
			{
				BOOL bValid;

                                     if((GC_NORMAL == GC_GetEnumValue(aRectIdx[i].iRectType,
                                                                                             j,
                                                                                             aRectIdx[i].iAcFailSid))
                                               && (GC_NORMAL != GC_GetEnumValue(aRectIdx[i].iRectType,
                                                                                                                j,
                                                                                                                aRectIdx[i].iAcOnOffStaSid)))
                                                        /*|| (GC_NORMAL != GC_GetEnumValue(aRectIdx[i].iRectType,
                                                                                                                j,
                                                                                                                aRectIdx[i].iDcOnOffStaSid)))) *///??��?��|?��?��??��AC?a����??��?DC��?��|??��?��|?��??o?����?Y?��|???a???��??����??2??����1��1DC?��??��??                                                                                          
                                     {
                                               pRt->iQtyAcOffRect++;
                                               
                                     }
                                     else
                                     {
                                     }


				if(pRt->bAcFail)
				{
					bValid = (GC_NORMAL == GC_GetEnumValue(aRectIdx[i].iRectType,
												j,
												aRectIdx[i].iFaultSid));
				}
				else
				{
					bValid = (GC_NORMAL == GC_GetEnumValue(aRectIdx[i].iRectType,
												j,
												aRectIdx[i].iAcOnOffStaSid))
						&& (GC_NORMAL == GC_GetEnumValue(aRectIdx[i].iRectType,
												j,
												aRectIdx[i].iFaultSid))
						//Remove the protect status, if the rectifier derated, the rectifier have output, but in proecte status
                                                        //Remove the protect status, if the rectifier derated, the rectifier have output, but in protect status
                                                        /*&& (GC_NORMAL == GC_GetEnumValue(aRectIdx[i].iRectType,
                                                                                                                j,
                                                                                                                aRectIdx[i].iProtectionSid))*/
                                                        && (GC_NORMAL == GC_GetEnumValue(aRectIdx[i].iRectType,
                                                                                                                j,
                                                                                                                aRectIdx[i].iAcFailSid))
                                                        && (GC_NORMAL == GC_GetEnumValue(aRectIdx[i].iRectType,
                                                                                                                j,
                                                                                                                aRectIdx[i].iDcOnOffStaSid));
                                     }

				iQtyCommRect++;
				
				if( i == RECT_TYPE_SELF)
				{
					iQtySelfCommRect++;
				}

				if(bValid)
				{
					QueueAcOnRect[iQtyOnWorkRect] = RectifierInfo;
					iQtyOnWorkRect++;

					if( i == RECT_TYPE_SELF)
					{
						iQtySelfOnWorkRect++;
					}

					/*Search RectifierInfo in g_pGcData->RunInfo.Rt.hListOnWorkRect, 
					if not find then insert RectifierInfo into the list*/
					if(!(SearchAndRefresh(RectifierInfo, 
						g_pGcData->RunInfo.Rt.hListOnWorkRect)))
					{
						GC_UpdateRectTime(&RectifierInfo);
						bRst = List_Insert(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
										(void*)(&RectifierInfo));
		
						GC_ASSERT(bRst,
								ERR_CTL_LIST_INSERT,
								"List_InsertT error!!!\n");					

						g_pGcData->RunInfo.Rt.iQtyOnWorkRect++;
					}
				}
				else
				{
					QueueAcOffRect[iQtyOffWorkRect] = RectifierInfo;
					iQtyOffWorkRect++;

					if( i == RECT_TYPE_SELF)
					{
						iQtySelfOffWorkRect++;
					}


					/*Search RectifierInfo in hListOffWorkRect, 
					if not find then insert RectifierInfo into the list*/				
					if(!SearchAndRefresh(RectifierInfo, 
						g_pGcData->RunInfo.Rt.hListOffWorkRect))
					{
						GC_UpdateRectTime(&RectifierInfo);
						bRst = List_Insert(g_pGcData->RunInfo.Rt.hListOffWorkRect,
									(void*)(&RectifierInfo));
				
						GC_ASSERT(bRst,
								ERR_CTL_LIST_INSERT,
								"List_InsertT error!!!\n");					

						pRt->iQtyOffWorkRect++;
					}
				}
			}
		}
	}

	/*Scan the AC-on list, if someone is not in QueueAcOnRect, 
	then remove it from the AC-on list */
	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);
	while(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
					(void*)(&RectifierInfo)))
	{
		BOOL	bFind = FALSE;
		for (i = 0; i < iQtyOnWorkRect; i++)
		{
			if((RectifierInfo.dwSerialNo == QueueAcOnRect[i].dwSerialNo) 
				&& (RectifierInfo.dwHiSerialNo == QueueAcOnRect[i].dwHiSerialNo)
				&& (RectifierInfo.iRectEquipType == QueueAcOnRect[i].iRectEquipType))
			{

				QueueAcOnRect[i].fOperTime = RectifierInfo.fOperTime;
				QueueAcOnRect[i].dwSelfOperTime = RectifierInfo.dwSelfOperTime;
				bRst = List_Set(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
							(void*)(&(QueueAcOnRect[i])));
	
				GC_ASSERT(bRst,
						ERR_CTL_LIST_SET, 
						"List_Set error!!!\n");		

				bFind = TRUE;
				break;
			}
		}

		if(bFind)
		{
			if(!(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect)))
			{
				break;
			}
		}
		else
		{
			bRst = List_Delete(g_pGcData->RunInfo.Rt.hListOnWorkRect);

			GC_ASSERT(bRst,
					ERR_CTL_LIST_DELETE, 
					"List_Delete error!!!\n");						

			g_pGcData->RunInfo.Rt.iQtyOnWorkRect--;
		}
	}	

	/*Scan the AC-off list, if someone is not in QueueAcOffRect, 
	then remove it from the AC-off list */
	List_GotoHead(pRt->hListOffWorkRect);
	while(List_Get(pRt->hListOffWorkRect, 
					(void*)(&RectifierInfo)))
	{
		BOOL	bFind = FALSE;
		for (i = 0; i < iQtyOffWorkRect; i++)
		{
			if((RectifierInfo.dwSerialNo == QueueAcOffRect[i].dwSerialNo)
				&& (RectifierInfo.dwHiSerialNo == QueueAcOffRect[i].dwHiSerialNo))
			{
				QueueAcOffRect[i].fOperTime = RectifierInfo.fOperTime;
				QueueAcOffRect[i].dwSelfOperTime = RectifierInfo.dwSelfOperTime;
				bRst = List_Set(pRt->hListOffWorkRect,
					(void*)(&(QueueAcOffRect[i])));

				GC_ASSERT(bRst,
						ERR_CTL_LIST_SET, 
						"List_Set error!!!\n");
				bFind = TRUE;
				break;
			}
		}

		if(bFind)
		{
			if(!(List_GotoNext(pRt->hListOffWorkRect)))
			{
				break;
			}		
		}
		else
		{
			bRst = List_Delete(pRt->hListOffWorkRect);
			
			GC_ASSERT(bRst,
					ERR_CTL_LIST_DELETE, 
					"List_Delete error!!!\n");
			
			pRt->iQtyOffWorkRect--;
		}
	}

	//Set self rect number
	GC_SetDwordValue(TYPE_OTHER,
					0,
					RT_PUB_COMM_QTY, 
					(DWORD)iQtySelfCommRect);

	GC_SetDwordValue(TYPE_OTHER, 
					0,
					RT_PUB_VAILD_QTY, 
					(DWORD)iQtySelfOnWorkRect);

	return;
}

static BOOL	GC_UpdateRectTime(IN GC_RECT_INFO *pRectRunInfo)
{
//Update running time;
	int			i, j;
	GC_RECT_INFO		RectifierInfo;
	GC_RUN_INFO_RECT	*pRt = &(g_pGcData->RunInfo.Rt);

	/*Scan all rectifiers*/

	
	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
			(void*)(&RectifierInfo)))
		{
			if((pRectRunInfo->dwSerialNo == RectifierInfo.dwSerialNo)
				&& (pRectRunInfo->dwHiSerialNo == RectifierInfo.dwHiSerialNo)
				&& (pRectRunInfo->iRectEquipType == RectifierInfo.iRectEquipType))
			{
				pRectRunInfo->dwSelfOperTime = RectifierInfo.dwSelfOperTime;
				pRectRunInfo->fOperTime = RectifierInfo.fOperTime;	

				return TRUE;
			}

		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));

	List_GotoHead(g_pGcData->RunInfo.Rt.hListOffWorkRect);
	do
	{
		GC_RECT_INFO	RectifierInfo;

		if(List_Get(g_pGcData->RunInfo.Rt.hListOffWorkRect, 
			(void*)(&RectifierInfo)))
		{
			if((pRectRunInfo->dwSerialNo == RectifierInfo.dwSerialNo)
				&& (pRectRunInfo->dwHiSerialNo == RectifierInfo.dwHiSerialNo)
				&& (pRectRunInfo->iRectEquipType == RectifierInfo.iRectEquipType))
			{
				pRectRunInfo->dwSelfOperTime = RectifierInfo.dwSelfOperTime;
				pRectRunInfo->fOperTime = RectifierInfo.fOperTime;	

				return TRUE;
			}	
		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOffWorkRect));
	
	return;
}
/*==========================================================================*
 * FUNCTION : GC_DestroyRectList
 * PURPOSE  : To destory the rect lists
 * CALLS    : List_Destroy, 
 * CALLED BY: ExitGC
 * ARGUMENTS: void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-27 11:30
 *==========================================================================*/
void	GC_DestroyRectList(void)
{
	if(g_pGcData->RunInfo.Rt.hListOnWorkRect)
	{
		List_Destroy(g_pGcData->RunInfo.Rt.hListOnWorkRect);
	}

	if(g_pGcData->RunInfo.Rt.hListOffWorkRect)
	{
		List_Destroy(g_pGcData->RunInfo.Rt.hListOffWorkRect);
	}

	return;
}
/*==========================================================================*
 * FUNCTION : GC_DestroyRectList
 * PURPOSE  : To destory the rect lists
 * CALLS    : List_Destroy, 
 * CALLED BY: ExitGC
 * ARGUMENTS: void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-27 11:30
 *==========================================================================*/
void	GC_DestroyMpptList(void)
{
	if(g_pGcData->RunInfo.Mt.hListOnWorkRect)
	{
		List_Destroy(g_pGcData->RunInfo.Mt.hListOnWorkRect);
	}
	
	if(g_pGcData->RunInfo.Mt.hListOffWorkRect)
	{
		List_Destroy(g_pGcData->RunInfo.Mt.hListOffWorkRect);
	}
	return;
}

#ifdef GC_SUPPORT_MPPT
/*==========================================================================*
* FUNCTION : SearchAndRefreshMppt
* PURPOSE  : Scan the hList, if find a record whose dwSerialNo is equal to
RectifierInfo.dwSerialNo, refresh the record and return TRUE.
* CALLS    : List_GotoHead, List_Get, List_Set, List_GotoNext
* CALLED BY: GC_MaintainRectList
* ARGUMENTS: GC_RECT_INFO  RectifierInfo : 
*            HANDLE     hList         : 
* RETURN   : TRUE: successful 
FALSE: not find RectifierInfo
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-09-16 17:10
*==========================================================================*/
BOOL SearchAndRefreshMppt(GC_MPPT_INFO RectifierInfo, HANDLE hList)
{
	GC_MPPT_INFO		RtInfo;
	BOOL			bRst;

	List_GotoHead(hList);
	do
	{
		if(List_Get(hList, (void*)(&RtInfo)))
		{
			if((RtInfo.dwSerialNo == RectifierInfo.dwSerialNo)
				&& (RtInfo.dwHiSerialNo == RectifierInfo.dwHiSerialNo))
			{
				bRst = List_Set(hList, (void*)(&RectifierInfo));

				GC_ASSERT(bRst,
					ERR_CTL_LIST_SET, 
					"List_Set error!!!\n");		

				return TRUE;
			}
		}
	}while(List_GotoNext(hList));

	return	FALSE;
}

/*==========================================================================*
* FUNCTION : GC_MaintainMpptList
* PURPOSE  : To maintain the two lists--Swictch_on rectifier list and 
Swictch_off rectifier list 
* CALLS    : List_GotoHead, List_GotoNext, 
List_Delete, SearchAndRefresh, List_Insert,
* CALLED BY: GC_InitRectList, RefreshData
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-09-15 16:57
*==========================================================================*/
void	GC_MaintainMpptList()
{
	int			i, j;
	BOOL			bRst;
	GC_MPPT_INFO		RectifierInfo;
	GC_RUN_INFO_MPPT	*pMppt = &(g_pGcData->RunInfo.Mt);

	
	//TYPE_MPPT_UNIT,	MT_PRI_RECT_ID,		MT_PRI_RECT_HI_ID,	MT_PRI_OPER_TIME,	MT_PRI_NIGHT_STATUS,	MT_PRI_DC_ON_OFF_STA,	MT_PRI_FAULT,	MT_PRI_PROTCTION
	
	int					iQtyCommMppt = 0;
	int					iQtyOnWorkMppt = 0;
	int					iQtyOffWorkMppt = 0;

	GC_MPPT_INFO	QueueDcOnRect[MAX_NUM_MPPT_EQUIP];
	GC_MPPT_INFO	QueueDcOffRect[MAX_NUM_MPPT_EQUIP];

	pMppt->iQtyNoOutputRect = 0;
	pMppt->iQtyDcOffRect = 0;

	/*Calculate the rect number of AC Failure/Rect failure*/

	for(j = 0; j < pMppt->iQtyOfRect; j++)
	{
		if(GC_IsRectComm(TYPE_MPPT_UNIT, j))
		{
			if(GC_ALARM == GC_GetEnumValue(TYPE_MPPT_UNIT,
						j,
						MT_PRI_NIGHT_STATUS)
				|| GC_ALARM == GC_GetEnumValue(TYPE_MPPT_UNIT,
						j,
						MT_PRI_FAULT))
			{
				pMppt->iQtyNoOutputRect++;
			}
		}
	}
	

	/*Create and stuff the QueueAcOnRect[]/QueueAcOffRect[], if some rectifer 
	is not in AC-on list / AC-off list, insert them into the list*/

	for(j = 0; j < pMppt->iQtyOfRect; j++)
	{

		RectifierInfo.dwSerialNo 
			= GC_GetDwordValue(TYPE_MPPT_UNIT, j, MT_PRI_RECT_ID);
		RectifierInfo.dwHiSerialNo 
			= GC_GetDwordValue(TYPE_MPPT_UNIT, j, MT_PRI_RECT_HI_ID);
		RectifierInfo.dwOperTime 
			= GC_GetDwordValue(TYPE_MPPT_UNIT, j, MT_PRI_OPER_TIME);
		RectifierInfo.iRectNo = j;



		if(GC_IsRectComm(TYPE_MPPT_UNIT, j))
		{
			BOOL bValid;

			if((GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT,
							j,
							MT_PRI_NIGHT_STATUS))
				|| (GC_NORMAL != GC_GetEnumValue(TYPE_MPPT_UNIT,
								j,
								MT_PRI_DC_ON_OFF_STA)))											
			{
				pMppt->iQtyDcOffRect++;
			}

			if(pMppt->bNight)
			{
				bValid = FALSE;
			}
			else
			{
				bValid =(GC_NORMAL == GC_GetEnumValue(TYPE_MPPT_UNIT,
								j,
								MT_PRI_FAULT))
					&& (GC_NORMAL == GC_GetEnumValue(TYPE_MPPT_UNIT,
									j,
									MT_PRI_DC_ON_OFF_STA));
				//printf("MPPT[%d] = %d,%d\n", j, GC_GetEnumValue(TYPE_MPPT_UNIT,	j, MT_PRI_FAULT), GC_GetEnumValue(TYPE_MPPT_UNIT,j,MT_PRI_NIGHT_STATUS));
			}

			iQtyCommMppt++;

			if(bValid)
			{
				QueueDcOnRect[iQtyOnWorkMppt] = RectifierInfo;
				iQtyOnWorkMppt++; 

				/*Search RectifierInfo in g_pGcData->RunInfo.Rt.hListOnWorkRect, 
				if not find then insert RectifierInfo into the list*/
				if(!(SearchAndRefreshMppt(RectifierInfo, 
					g_pGcData->RunInfo.Mt.hListOnWorkRect)))
				{
					 
					bRst = List_Insert(g_pGcData->RunInfo.Mt.hListOnWorkRect, 
						(void*)(&RectifierInfo));

					GC_ASSERT(bRst,
						ERR_CTL_LIST_INSERT,
						"List_InsertT error!!!\n");					

					pMppt->iQtyOnWorkRect++;
				}
			}
			else
			{
				QueueDcOffRect[iQtyOffWorkMppt] = RectifierInfo;
				iQtyOffWorkMppt++; 

				/*Search RectifierInfo in hListOffWorkRect, 
				if not find then insert RectifierInfo into the list*/				
				if(!SearchAndRefreshMppt(RectifierInfo, 
					g_pGcData->RunInfo.Mt.hListOffWorkRect))
				{
					bRst = List_Insert(g_pGcData->RunInfo.Mt.hListOffWorkRect,
						(void*)(&RectifierInfo));

					GC_ASSERT(bRst,
						ERR_CTL_LIST_INSERT,
						"List_InsertT error!!!\n");					

					pMppt->iQtyOffWorkRect++;
				}
			}
		}
	}
	
	//printf("iQtyOnWorkMppt = %d, iQtyOnWorkMppt =%d\n",iQtyOnWorkMppt , iQtyOnWorkMppt);
	//printf("pMppt->iQtyOnWorkRect = %d, pMppt->iQtyOffWorkRect =%d\n",pMppt->iQtyOnWorkRect , pMppt->iQtyOffWorkRect);
	/*Scan the DC-on list, if someone is not in QueueAcOnRect, 
	then remove it from the AC-on list */
	List_GotoHead(g_pGcData->RunInfo.Mt.hListOnWorkRect);
	while(List_Get(g_pGcData->RunInfo.Mt.hListOnWorkRect, 
		(void*)(&RectifierInfo)))
	{
		BOOL	bFind = FALSE;
		for (i = 0; i < iQtyOnWorkMppt; i++)
		{
			if((RectifierInfo.dwSerialNo == QueueDcOnRect[i].dwSerialNo) 
				&& (RectifierInfo.dwHiSerialNo == QueueDcOnRect[i].dwHiSerialNo))
			{

				bRst = List_Set(g_pGcData->RunInfo.Mt.hListOnWorkRect, 
					(void*)(&(QueueDcOnRect[i])));

				GC_ASSERT(bRst,
					ERR_CTL_LIST_SET, 
					"List_Set error!!!\n");		

				bFind = TRUE;
				break;
			}
		}

		if(bFind)
		{
			if(!(List_GotoNext(g_pGcData->RunInfo.Mt.hListOnWorkRect)))
			{
				break;
			}
		}
		else
		{
			bRst = List_Delete(g_pGcData->RunInfo.Mt.hListOnWorkRect);

			GC_ASSERT(bRst,
				ERR_CTL_LIST_DELETE, 
				"List_Delete error!!!\n");						

			pMppt->iQtyOnWorkRect--;
		}
	}	

	/*Scan the DC-off list, if someone is not in QueueAcOffRect, 
	then remove it from the AC-off list */
	List_GotoHead(pMppt->hListOffWorkRect);
	while(List_Get(pMppt->hListOffWorkRect, 
		(void*)(&RectifierInfo)))
	{
		BOOL	bFind = FALSE;
		for (i = 0; i < iQtyOffWorkMppt; i++)
		{
			if((RectifierInfo.dwSerialNo == QueueDcOffRect[i].dwSerialNo)
				&& (RectifierInfo.dwHiSerialNo == QueueDcOffRect[i].dwHiSerialNo))
			{
				//QueueDcOffRect[i].fOperTime = RectifierInfo.fOperTime;
				//QueueDcOffRect[i].dwSelfOperTime = RectifierInfo.dwSelfOperTime;
				bRst = List_Set(pMppt->hListOffWorkRect,
					(void*)(&(QueueDcOffRect[i])));

				GC_ASSERT(bRst,
					ERR_CTL_LIST_SET, 
					"List_Set error!!!\n");
				bFind = TRUE;
				break;
			}
		}

		if(bFind)
		{
			if(!(List_GotoNext(pMppt->hListOffWorkRect)))
			{
				break;
			}		
		}
		else
		{
			bRst = List_Delete(pMppt->hListOffWorkRect);

			GC_ASSERT(bRst,
				ERR_CTL_LIST_DELETE, 
				"List_Delete error!!!\n");

			pMppt->iQtyOffWorkRect--;
		}
	}

	//Set self rect number
	GC_SetDwordValue(TYPE_OTHER,
		0,
		MT_PUB_COMM_QTY, 
		(DWORD)iQtyCommMppt);

	GC_SetDwordValue(TYPE_OTHER, 
		0,
		MT_PUB_VAILD_QTY, 
		(DWORD)iQtyOnWorkMppt);

	return;
}
#endif
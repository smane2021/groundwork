/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : gc_estop_eshutdown.c
 *  CREATOR  : Frank Cao                DATE: 2008-10-24 13:44
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gc_batt_mgmt.h"
#include	"gc_lvd.h"
#include	"gen_ctl.h"
#include	"gc_sig_value.h"
#include	"gc_estop_eshutdown.h"


#define		NOT_CARE_RECT_ESTOP_STATE

#ifdef NOT_CARE_RECT_ESTOP_STATE
/*==========================================================================*
 * FUNCTION : GetRectEstopState
 * PURPOSE  : Get Estop state from rectifiers
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-08-17 14:13
 *==========================================================================*/
static BOOL GetRectEstopState(void)
{
	int							i, j;
	BOOL						bAllCommRecycle;
	BOOL						bAllAcFailRecycle;
	GC_RUN_INFO_RECT			*pRt = &(g_pGcData->RunInfo.Rt);

	static BOOL					s_bFirstRun = TRUE;
	static RECT_ESTOP_INFO		s_EStopInfo[MAX_NUM_RECT_EQUIP * MAX_RECT_TYPE_NUM];

	if(s_bFirstRun)
	{
		s_bFirstRun = FALSE;
	
		for(j = 0; j < MAX_RECT_TYPE_NUM; j++)
		{
			for(i = 0; i < MAX_NUM_RECT_EQUIP; i++)
			{
				s_EStopInfo[j * MAX_NUM_RECT_EQUIP + i].bComm 
					= pRt->Slave[j].abComm[i];
				switch(j)
				{
				case RECT_TYPE_SELF:
					s_EStopInfo[j * MAX_NUM_RECT_EQUIP + i].bAcFail
						= GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_FAIL);
					break;
				case RECT_TYPE_SLAVE1:
					s_EStopInfo[j * MAX_NUM_RECT_EQUIP + i].bAcFail
						= GC_IsGCEquipExist(TYPE_SLAVE1_RECT, i, S1_PRI_AC_FAIL,FALSE);
					break;
				case RECT_TYPE_SLAVE2:
					s_EStopInfo[j * MAX_NUM_RECT_EQUIP + i].bAcFail
						= GC_IsGCEquipExist(TYPE_SLAVE2_RECT, i, S2_PRI_AC_FAIL,FALSE);
					break;
				case RECT_TYPE_SLAVE3:
					s_EStopInfo[j * MAX_NUM_RECT_EQUIP + i].bAcFail
						= GC_IsGCEquipExist(TYPE_SLAVE3_RECT, i, S3_PRI_AC_FAIL,FALSE);
					break;
				default:
					break;
				}
				s_EStopInfo[j * MAX_NUM_RECT_EQUIP + i].bCommRecycle = FALSE;
				s_EStopInfo[j * MAX_NUM_RECT_EQUIP + i].bAcFailRecycle = FALSE;
			}
		}
	}
	
	if(g_pGcData->RunInfo.EStopEshutdow.enumEStopState)
	{

		bAllCommRecycle = TRUE;
		bAllAcFailRecycle = TRUE;

		for(i = 0; i < pRt->Slave[RECT_TYPE_SELF].iQtyOfRect; i++)
		{
			if(pRt->Slave[RECT_TYPE_SELF].abComm[i] && (!s_EStopInfo[i].bComm))
			{
				s_EStopInfo[i].bCommRecycle = TRUE;
			}
			
			if((GC_NORMAL == GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_FAIL))
				&& (!s_EStopInfo[i].bAcFail))
			{
				s_EStopInfo[i].bAcFailRecycle = TRUE;
			}

			if(!(s_EStopInfo[i].bCommRecycle))
			{
				bAllCommRecycle = FALSE;
			}

			if(!(s_EStopInfo[i].bAcFailRecycle))
			{
				bAllAcFailRecycle = FALSE;
			}
		}
	}

	for(i = 0; i < pRt->Slave[RECT_TYPE_SELF].iQtyOfRect; i++)
	{
		s_EStopInfo[i].bComm = pRt->Slave[RECT_TYPE_SELF].abComm[i];
		s_EStopInfo[i].bAcFail = GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_FAIL);
	}

	if(bAllCommRecycle || bAllAcFailRecycle)
	{
		for(i = 0; i < pRt->Slave[RECT_TYPE_SELF].iQtyOfRect; i++)
		{
			s_EStopInfo[i].bCommRecycle = FALSE;
			s_EStopInfo[i].bAcFailRecycle = FALSE;
		}
		return TRUE;
	}

	return FALSE;

}
#endif

static SIG_ENUM GetDInputState(void)
{
	SIG_BASIC_VALUE	*pSigValue;
	int				iRst, i;
	int				iBufLen;
	SIG_ENUM		enumValue[EMERGENCY_SHUTDOWN_INPUT_NUM];
	static int		s_iCounter = 0;
	SIG_ENUM		enumResult;
	


	if(g_pGcData->PriCfg.iEstopEshutdownInputNum != EMERGENCY_SHUTDOWN_INPUT_NUM)
	{
		return GC_INVALID_SIG;
	}

	for(i = 0; i < EMERGENCY_SHUTDOWN_INPUT_NUM; i++)
	{
		GC_SIG_INFO	*pInputCfg = g_pGcData->PriCfg.pEstopEshutdownInput + i;

		if(!(pInputCfg->bEnabled))
		{
			return GC_INVALID_SIG;
		}

		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					pInputCfg->iEquipId,
					DXI_MERGE_SIG_ID(pInputCfg->iSignalType,
									pInputCfg->iSignalId),
					&iBufLen,
					&pSigValue,
					0);	

		GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"Get Emergency Shut-down input error!\n");

		if(!(SIG_VALUE_IS_VALID(pSigValue)))
		{
			return GC_INVALID_SIG;
		}
		
		enumValue[i] = pSigValue->varValue.enumValue;
	}

	enumResult = GC_ALARM;

	for(i = 0; i < EMERGENCY_SHUTDOWN_INPUT_NUM; i++)
	{
		if(GC_NORMAL == enumValue[i])
		{
			enumResult = GC_NORMAL;
			break;
		}
	}
	
	if(GC_ALARM == enumResult) 
	//2 inputs are all Alarm
	{
		if(s_iCounter > ACTION_HYSTERESIS_TIMES)
		{
			return GC_ALARM;
		}
		else
		{
			s_iCounter++;
		}
	}
	else
	{
		s_iCounter = 0;
		return GC_NORMAL;
	}

	return GC_NORMAL;
}



/*==========================================================================*
 * FUNCTION : GetEstopEshutdownInput
 * PURPOSE  : To get 2 emergency shutdown inputs by 1 times hysteresis, put 
              the result into g_pGcData->RunInfo.enumEstopEshutdownInput
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-05-09 14:38
 *==========================================================================*/
static void GetEstopEshutdownInput(void)
{
	GC_ESTOP_ESHUTDOWN*	pEstopEshutdown = &(g_pGcData->RunInfo.EStopEshutdow);
	
	
	pEstopEshutdown->enumEstopEshutdownParam 
		= GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_ESTOP_ESHUTDOWN);

	pEstopEshutdown->enumEstopEshutdownInput = GetDInputState();

#ifndef NOT_CARE_RECT_ESTOP_STATE
	pEstopEshutdown->enumRectEStopState = GetRectEstopState();
#endif

	return;
}


static void ControlToShutdown(void)
{
	DWORD		i;

	
	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						RT_PUB_AC_ON_OFF_CTL_SELF + i,
						GC_RECT_OFF,
						TRUE);
	}
	GC_SetEnumValue(TYPE_OTHER, 
			0, 
			MT_PUB_DC_ON_OFF_CTL,
			GC_RECT_OFF,
			TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
						0, 
						CT_PUB_DC_ON_OFF_CTL,
						GC_RECT_OFF,
						TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_ESTOP_ESHUTDOWN_ALARM,
					GC_ALARM,
					TRUE);

	//To judge if there is LVD disconnected.
	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{
		if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
			&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0]))
		{
			GC_SetEnumValue(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_CTL, 
						GC_DISCONNECTED, 
						TRUE);
		}

		if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
			&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1]))
		{
			GC_SetEnumValue(TYPE_LVD_UNIT, 
						i,
						LVD2_PRI_CTL, 
						GC_DISCONNECTED, 
						TRUE);
		}
	}


	return;
}


static void ControlToEstop(void)
{
	DWORD		i;

	g_pGcData->RunInfo.EStopEshutdow.enumEStopState = TRUE;


	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						RT_PUB_ESTOP_CTL + i,
						GC_ALARM,
						TRUE);
	}
	if(GC_IsGCSigExist(TYPE_OTHER, 0, MT_PUB_ESTOP_CTL))
	{
		GC_SetEnumValue(TYPE_OTHER, 
			0, 
			MT_PUB_ESTOP_CTL,
			GC_ALARM,
			TRUE);
	}

	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					CT_PUB_ESTOP_CTL,
					GC_ALARM,
					TRUE);
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_ESTOP_ESHUTDOWN_ALARM,
					GC_ALARM,
					TRUE);

	//To judge if there is LVD disconnected.
	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{
		if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
			&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0]))
		{
			GC_SetEnumValue(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_CTL, 
						GC_DISCONNECTED, 
						TRUE);
		}

		if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
			&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1]))
		{
			GC_SetEnumValue(TYPE_LVD_UNIT, 
						i,
						LVD2_PRI_CTL, 
						GC_DISCONNECTED, 
						TRUE);
		}
	}


	return;
}


static void RecycleShutdown(void)
{
	int i;

	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		GC_SetEnumValue(TYPE_OTHER, 
			0, 
			RT_PUB_AC_ON_OFF_CTL_SELF + i,
			GC_RECT_ON,
			TRUE);
	}


	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					CT_PUB_DC_ON_OFF_CTL,
					GC_RECT_ON,
					TRUE);
	GC_SetEnumValue(TYPE_OTHER, 
		0, 
		MT_PUB_DC_ON_OFF_CTL,
		GC_RECT_ON,
		TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_ESTOP_ESHUTDOWN_ALARM,
					GC_NORMAL,
					TRUE);



	return;
}


static void RecycleEstop(BOOL bNeedCtlLvd)
{
	int			i;

	/*if(g_pGcData->RunInfo.EStopEshutdow.enumEStopState)
	{
		GC_SetEnumValue(TYPE_OTHER, 
					0, 
					CT_PUB_ESTOP_CTL,
					GC_NORMAL,
					TRUE);
	}*/

	if(bNeedCtlLvd)
	{
		//To judge if there is LVD disconnected.
		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{
			if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
				&& (GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0]))
			{
				GC_SetEnumValue(TYPE_LVD_UNIT, 
							i,
							LVD1_PRI_CTL, 
							GC_CONNECTED, 
							TRUE);
			}

			if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
				&& (GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1]))
			{
				GC_SetEnumValue(TYPE_LVD_UNIT, 
							i,
							LVD2_PRI_CTL, 
							GC_CONNECTED, 
							TRUE);
			}
		}
	}
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_ESTOP_ESHUTDOWN_ALARM,
					GC_NORMAL,
					TRUE);

	g_pGcData->RunInfo.EStopEshutdow.enumEStopState = FALSE;

	return;
}



#define	ESTOP_UNLOCK		0
#define	ESTOP_LOCK			1

/*==========================================================================*
 * FUNCTION : GC_EstopShutdown
 * PURPOSE  : To execute emergency shutdown
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-05-09 14:43
 *==========================================================================*/
void GC_EstopShutdown(void)
{
	static SIG_ENUM		s_enumLastInput = GC_NORMAL;
	static SIG_ENUM		s_enumLastState = ESTOP_UNLOCK;

	GetEstopEshutdownInput();


	//invalid input
	if(GC_INVALID_SIG == g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownInput)
	{
		RecycleEstop(FALSE);

		s_enumLastInput = g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownInput;
		s_enumLastState = ESTOP_UNLOCK;
		return;
	}

	//disabled
	if(PARAM_STATE_DISABLED 
		== g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownParam)
	{
		RecycleEstop(FALSE);
		s_enumLastState = ESTOP_UNLOCK;
	}
	
	//E-stop
	else if(g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownParam == PARAM_STATE_ESTOP)
	{	
		if(GC_ALARM == g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownInput)
		{
			ControlToEstop();
			if(ESTOP_UNLOCK == s_enumLastState)
			{
				GC_LOG_OUT(APP_LOG_WARNING, "Emergency Stop!!!\n");
			}
			s_enumLastState = ESTOP_LOCK;
		}

#ifdef NOT_CARE_RECT_ESTOP_STATE
		else 
		{
			RecycleEstop(FALSE);

			if(ESTOP_LOCK == s_enumLastState)
			{
				GC_LOG_OUT(APP_LOG_INFO, "End emergency Stop.\n");
			}

			s_enumLastState = ESTOP_UNLOCK;
		}
#else		
		//all rectifiers unlocked
		else if(!g_pGcData->RunInfo.EStopEshutdow.enumRectEStopState) 
		{
			RecycleEstop(TRUE);

			if(ESTOP_LOCK == s_enumLastState)
			{
				GC_LOG_OUT(APP_LOG_INFO, "End emergency Stop.\n");
			}

			s_enumLastState = ESTOP_UNLOCK;
		}
#endif
	}


	//E-shutdown
	else if(g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownParam == PARAM_STATE_ESHUTDOWN)
	{
		if(GC_ALARM == g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownInput)
		{
			ControlToShutdown();
			
			if(GC_NORMAL == s_enumLastInput)
			{
				GC_LOG_OUT(APP_LOG_WARNING, "Emergency shut-down!!!\n");
			}
		}
		else
		{
			if(GC_NORMAL != s_enumLastInput)
			{
				RecycleShutdown();

				GC_LOG_OUT(APP_LOG_INFO, "End emergency shut-down.\n");
			}	
		}
	}

	s_enumLastInput = g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownInput;

	return;
}


void GC_EstopShutStateInit(void)
{
	g_pGcData->RunInfo.EStopEshutdow.enumEStopState = FALSE;
	return;
}



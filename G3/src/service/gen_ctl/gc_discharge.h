/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_discharge.h
 *  CREATOR  : Frank Cao                DATE: 2004-10-29 10:33
 *  VERSION  : V1.00
 *  PURPOSE  : To provide Battery Test  and AC Fail State handling 
 *             interface functions
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_DISCHARGE_H_
#define _GC_DISCHARGE_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define	MODE_ENTIRE_LOAD_TEST	0
#define	MODE_CONST_CURR_TEST	1

void GC_AcFail(void);

void GC_PlanManTest(SIG_ENUM BmState);

void GC_AcFailTest(void);

void GC_ShortTest(void);

void GC_BattTestEnd(int iNextState,
					const char* szLogOut,
					int iEndReason,
					int iTestResult,
					BOOL bTurnAuto);
void GC_BattTestStart(int iNextState, 
					  const char* szLogOut,
					  int iStartReason);

void GC_ShortTestStart(void);
void GC_ShortTestIntervalTimerCheck(void);
int GC_GetBtDuration(int iIdx);
int GC_GetShortTestDuration(int iIdx);
int GC_GetShortTestInterval(int iIdx);
#endif //_GC_DISCHARGE_H_

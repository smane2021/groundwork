/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU Plus Rack_mounted
 *
 *  FILENAME : gc_timer.h
 *  CREATOR  : Frank Cao                DATE: 2006-05-09 15:02
 *  VERSION  : V1.00
 *  PURPOSE  : To provide a group of interface function for timer function.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _PUB_TIMER_H_
#define _PUB_TIMER_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"


#define	GC_INTERVAL_REFRESH_DATA_DN		3
#define	GC_INTERVAL_BATT_MGMT_DN		6

#define	GC_INTERVAL_REFRESH_DATA		5
#define	GC_INTERVAL_BATT_MGMT			10
//added by Jimmy 2012.08.27 for Li-Ion Batt CR
#define	GC_INTERVAL_LI_BATT_MGMT		6
#define	GC_INTERVAL_RECT_REDUND			30
#define	GC_INTERVAL_DSL_TEST			5
#define	GC_INTERVAL_MAIN_SWITCH			5

#define SECONDS_PER_MIN					60
#define MINUTES_PER_HOUR				60
//#ifdef _DEBUG
//#define HOURS_PER_DAY					1
//#else
#define HOURS_PER_DAY					24
//#endif
#define MS_PER_SEC						1000
#define	DAYS_PER_YEAR					365
#define	DAYS_PER_MONTH					30
#define SECONDS_PER_HOUR				(SECONDS_PER_MIN * MINUTES_PER_HOUR)
#define SECONDS_PER_DAY					(HOURS_PER_DAY * MINUTES_PER_HOUR * SECONDS_PER_MIN)

#define	INTERVAL_AFTER_BC				(24 * SECONDS_PER_HOUR)
#define	INTERVAL_RESET_CAP				(3 * SECONDS_PER_HOUR)


#define GC_TIMER_IS_STOP(TimerID)			(GC_GetStateOfSecTimer(TimerID) == SEC_TMR_SUSPENDED)
#define GC_TIMER_IS_RUNNING(TimerID)		(GC_GetStateOfSecTimer(TimerID) == SEC_TMR_TIME_IN)
#define GC_TIMER_IS_OUT(TimerID)			(GC_GetStateOfSecTimer(TimerID) == SEC_TMR_TIME_OUT)


enum	GC_MS_TIMER_ID
{
	ID_TM_REFRESH_DATA = 0,
	ID_TM_BATT_MGMT,
	ID_TM_RECT_REDUND,
	ID_TM_ENERGY_SAVING,
	ID_TM_DSL_TEST,
	ID_TM_MAIN_SWITCH,

	MAX_TIMER_NUM
};

enum _TIMER_STATE
{
	SEC_TMR_SUSPENDED = 0,
	SEC_TMR_TIME_IN,
	SEC_TMR_TIME_OUT,
};

enum	_GC_SEC_TIMER_ID
{
	SEC_TMR_ID_SHORT_TEST_INTERVAL = 0,		//Short Test Interval
	SEC_TMR_ID_SHORT_TEST_DURATION,			//Short Test Duration
	SEC_TMR_ID_BATT_TEST_DURATION,			//Battery Test Duration
	SEC_TMR_ID_STABLE_BC,					//Stable Boost Charge
	SEC_TMR_ID_CYCLIC_BC_INTERVAL,			//Cyclic Boost Charge Interval
	SEC_TMR_ID_CYCLIC_BC_DURATION,			//Cyclic Boost Charge Duration
	SEC_TMR_ID_AFTER_BC,					//for to start a Pre-BC
	SEC_TMR_ID_RESET_CAP,					//for to Reset Battery Capcity
	SEC_TMR_ID_BC_PROTECT,					//Boost Charge Protection
	SEC_TMR_ID_FC_TO_BC_DELAY,				//for current, FC to BC Delay
	SEC_TMR_ID_CURR_LMT_INTERVAL,			//current limit period
	SEC_TMR_ID_CONST_TEST_INTERVAL,			//Constant test control period
	SEC_TMR_ID_RECT_CURR,					//Rcteifier current calc delay
	SEC_TMR_ID_GET_LOAD_INTERVAL,			//Get Load Current period
	SEC_TMR_ID_SWITCH_ON_DELAY,				//After switch off a rectifier, 
	//										//if under-voltage, delay 300s 
	//										//then switch on all of rectifiers

	SEC_TMR_ID_LVD1_RECONNECT = 20,			//for LVD reconnection
	SEC_TMR_ID_LVD1_DISCH = 70,				//for LVD disconnection by time
	SEC_TMR_ID_RECT_CYCLE =	120,				//Rectifier Cycling period
	SEC_TMR_ID_SWITCH_OFF_DELAY,			//Rectifier Redundancy Switch Off Delay
	SEC_TMR_ID_SWITCH_DELAY,				//Cycling Switch Delay
	SEC_TMR_ID_PRE_CURR_LMT_DELAY,			//Delay after Pre-Current limit
	SEC_TMR_ID_OSCILLATE_PERIOD,			//If Rectifier Redundancy deactivated 5 times in the period, 
											//Rectifier Redundancy Enabled shall be set to NO
	SEC_TMR_ID_ALL_RECT_DRYING,

	SEC_TMR_ID_MANUAL_TO_AUTO,

	SEC_TMR_ID_DSL_TEST_DELAY,				//Delay before a diesel test

	SEC_TMR_ID_MAIN_SWITCH_30S,				//Main Switch 30s Timer
	SEC_TMR_ID_MAIN_SWITCH_5S,				//Main Switch 5s Timer

	SEC_TMR_ID_BOOTING_DELAY,			//DC voltage calcate deley when restart.

	//Timer for Hybrid system

	SEC_TMR_ID_HYBRID_CAPACITY_CHARGE,
	SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE,
	SEC_TMR_ID_HYBRID_FIXEDDAILY_CHARGE,
	SEC_TMR_ID_HYBRID_FIXEDDAILY_DISCHARGE,
	SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION,
	SEC_TMR_ID_HYBRID_EQUALISM_CYCLE,
	SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION,
	SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP,
	SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL,
	SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL,

	//For ESNA LVD Board synch
	SEC_TMR_ID_ESNA_LVD_SYNCH_INTERVAL,

	//For Relay test function
	SEC_TMR_ID_RELAY_TEST_INTERVAL,
	SEC_TMR_ID_RELAY_TEST_CHANGESTEATE,


	//For MPPT
	SEC_TMR_ID_MPPT_LOST_INTERVAL,

	//For BattPredict
	SEC_TMR_ID_BATT_DISCHARGE_TIME,
	SEC_TMR_ID_PRE_CURR_LMT_OF_LVD,


	MAX_GC_SEC_TMR_NUM,
};

typedef int (*GC_GET_TMR_INTERVAL_PROC)(int iIdx);

struct tagGC_TIMER
{		
	time_t						tStartTime;
	int							iInterval;
	GC_GET_TMR_INTERVAL_PROC	pfnGetTmrInterval;
	int							iIdx;

	/*The Timer State, include Suspended, Time-in, Time-out*/
	BYTE						byState;
};
typedef struct tagGC_TIMER	GC_SINGLE_SEC_TIMER;

struct tagGC_SEC_TIMER_GROUP
{
	GC_SINGLE_SEC_TIMER		aSecTimer[MAX_GC_SEC_TMR_NUM];
	time_t					tTimeNow;
	BOOL					abNeedSave[MAX_GC_SEC_TMR_NUM];
};
typedef struct tagGC_SEC_TIMER_GROUP GC_SEC_TIMER_GROUP;

//initialize timer data
void GC_TimersInit(void);

//destroy timer data
void GC_TimersDestroy(void);

//Get the timer state
BYTE GC_GetStateOfSecTimer(int iTimerId);	

//Suspend the timer
void GC_SuspendSecTimer(int iTimerId);

//to set the timer with the Duration
void GC_SetSecTimer(int iTimerId,
					int	iInterval,
					GC_GET_TMR_INTERVAL_PROC pfnGetInterval,
					int iIdx);

//to maintain all of timers
time_t GC_MaintainSecTimers(time_t lLastTime, 
							long lMaxDelta);

#endif //_PUB_TIMER_H_

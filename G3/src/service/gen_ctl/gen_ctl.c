/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU Plus Rack_mounted
 *
 *  FILENAME : gen_ctl.c
 *  CREATOR  : Frank Cao                DATE: 2006-05-09 13:30
 *  VERSION  : V1.00
 *  PURPOSE  : It's entrance of General Controller 
 *
 *
 *  HISTORY  : 
 *
 *==========================================================================*/
#include	<setjmp.h>
#include	"gen_ctl.h"
#include	"gc_index_def.h"
#include	"gc_rect_list.h"
#include	"gc_batt_mgmt.h"
//#include	"gc_engy_sav.h"
//#include	"gc_dsl_test.h"
#include	"gc_rect_redund.h"
#include	"gc_timer.h"
#include	"gc_sig_value.h"
#include	"gc_init_equip.h"
#include	"gc_pri_cfg.h"
#include	"gc_power_split.h"
#include	"gc_estop_eshutdown.h"

#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>

#ifdef	_GC_TEST
#include	"gc_picket.h"
#endif

#define	MASTER_MODE		0
#define	SLAVE_MODE		1

static void	InitGC(void);
static void	ExitGC(void);
static void CheckPriCfgChange(void);

/*All data of General Controller is in a structure of TYPE_GEN_CTL_DATA,
and this pointer points to it. */
TYPE_GEN_CTL_DATA	*g_pGcData;
TYPE_GEN_CTL_DATA	g_GcData;

/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : To provide the standard interface function for SCU Plus main
			  module
 * CALLS    : GC_Init, PowerSplit, GC_BattMgmt
 * CALLED BY: Main Module of SCU Plus
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE:  2006��5��9�� 13:40:10
 *==========================================================================*/
#define		GET_SERVICE_OF_GC_NAME	"gen_ctl.so"
DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs)
{
	UNUSED(pArgs);
	int		iRst;
	BOOL	bExitFlag;
	static BOOL bCANScanCount = 0;

	//Sleep(10000);//?
	g_pGcData = &g_GcData;
	memset(g_pGcData, 0, sizeof(TYPE_GEN_CTL_DATA));
	APP_SERVICE		*GC_AppService = NULL;
	GC_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_GC_NAME);

	iRst = setjmp(g_pGcData->JmpBuf);

	if(iRst)
	{
		return	iRst;
	}
	g_pGcData->bAutoConfig = TRUE;//��ʼ��
	InitGC();
	GC_RunInfoRefresh();
	GC_RunInfoRefresh();
	GC_RunInfoRefresh();

	//Following is a message cycle
	bExitFlag = FALSE;

	//�˴���ʾSSL��SNMPV3״̬begin
	VAR_VALUE_EX	sigValue;
	sigValue.nSendDirectly = TRUE;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = "GC";
	if(g_SiteInfo.iSSLFlag == NO_SSL)
	{
	    sigValue.varValue.enumValue = 0;
	}
	else
	{
	    sigValue.varValue.enumValue = 1;
	}
	DxiSetData(VAR_A_SIGNAL_VALUE,
	    1, // Equip IP
	    DXI_MERGE_SIG_ID(0, 183),// signal type and signal ID
	    sizeof(VAR_VALUE_EX),
	    &sigValue,
	    0);
	//�˴���ʾSSL��SNMPV3״̬end

#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread GC Service was created, Thread Id = %d.\n", gettid());
#endif

	while(!bExitFlag)
	{
		int		iRst,itemp;
		RUN_THREAD_MSG	msg;
		
		iRst = RunThread_GetMessage(g_pGcData->hThreadSelf, &msg, FALSE, 0);
		if(iRst == ERR_OK)
		{
			

			BOOL	bEStopEShutdownState = FALSE;
			if(((PARAM_STATE_ESHUTDOWN == g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownParam) 
			    && (GC_ALARM == g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownInput))
			   || ((PARAM_STATE_ESTOP == g_pGcData->RunInfo.EStopEshutdow.enumEstopEshutdownParam) 
					&& g_pGcData->RunInfo.EStopEshutdow.enumEStopState))
			{

				GC_HandlingOnErr(ST_FLOAT, "Emergency shut-down or Estop, turn to FC.\n");
				bEStopEShutdownState = TRUE;	
			}

			switch(msg.dwMsg)
			{
			case MSG_CAN_SAMPLER_FINISH_SCAN:
				bCANScanCount  =  3;
				break;
			case MSG_QUIT:
				bExitFlag = TRUE;
				break;

			case MSG_TIMER:
				GC_ASSERT((msg.dwParam1 < MAX_TIMER_NUM), 
						ERR_CTL_GET_MESSAGE, 
						"Invalid Timer Message Parameter1!\n");

				switch(msg.dwParam1)
				{

				case ID_TM_REFRESH_DATA:

#ifdef _GC_TEST //defined when make test version
					{
						BOOL	bRst;
						bRst = GC_Picket();
						if(!bRst)
						{
							abort();
						}
					}
#endif //_GC_TEST
					GC_RunInfoRefresh();
					break;

				case ID_TM_BATT_MGMT:
					if(bCANScanCount  < 3)
					{
						bCANScanCount++;
					}
					else 
					{
						GC_BasicMaintain();

						if(!bEStopEShutdownState)
						{

							switch(g_pGcData->enumMasterSlaveMode)
							{
							case MODE_STANDALONE:

								if(g_pGcData->PriCfg.bSlaveMode)
								{
									GC_PowerSplit();
								}
								else
								{
									GC_BattMgmt();
								}
								break;
							case MODE_MASTER:
								GC_MasterBattMgmt();
								break;
							case MODE_SLAVE:
							default:
								break;
							}
						}

					}
					break;


				case ID_TM_RECT_REDUND:
					if(bCANScanCount  >=  3)
					{
						if(!(g_pGcData->PriCfg.bSlaveMode) 
							&& (g_pGcData->enumMasterSlaveMode != MODE_SLAVE)
							&& (!bEStopEShutdownState))
						{
							GC_RectRedund();
							GC_RectHVSD();
						}
					}
					break;

				case ID_TM_ENERGY_SAVING:
					if(bCANScanCount  >=  3)
					{
						if(!(g_pGcData->PriCfg.bSlaveMode) 
							&& (g_pGcData->enumMasterSlaveMode != MODE_SLAVE)
							&& (!bEStopEShutdownState))
						{
							GC_EngySav();
						}
					}
					break;

				case ID_TM_DSL_TEST:
					if(bCANScanCount  >=  3)
					{
						if(!(g_pGcData->PriCfg.bSlaveMode) 
							&& (g_pGcData->enumMasterSlaveMode != MODE_SLAVE)
							&& (!bEStopEShutdownState))
						{
							GC_DslTest();
						}
					}
					break;

				case ID_TM_MAIN_SWITCH:
					if(bCANScanCount  >= 3)
					{
						if(!(g_pGcData->PriCfg.bSlaveMode) 
							&& (g_pGcData->enumMasterSlaveMode != MODE_SLAVE)
							&& (!bEStopEShutdownState))
						{
							GC_MainSwitch();
						}
					}
					break;

				default:
					break;

				}//switch(msg.dwParam1)
				break;
			//added by Jimmy Wu, for re-initial Batt Test Info 
			case MSG_AUTOCONFIG: 
				if(ID_AUTOCFG_START == msg.dwParam1)
				{
					g_pGcData->bAutoConfig = TRUE;
				}
				else if(ID_AUTOCFG_REFRESH_DATA == msg.dwParam1)
				{
					//printf("\n\n********Auto Initial Batt********\n\n");
					Sleep(10000);//Set a 10s delay for sanning devices
					g_pGcData->bAutoConfig = FALSE;
					TestLogInit();
				}
				break;
			default:
				break;
			}//switch(msg.dwMsg)
		}
		else
		{
			if(g_pGcData->enumMasterSlaveMode == MODE_SLAVE)
			{
				//No any code here.
			}
			else
			{
				GC_PreCurrlmtForAcFail();				
			}
			
			GC_EstopShutdown();
			GC_SysAlmLightCtl();
#ifdef GC_SUPPORT_RELAY_TEST
			GC_AutoRelayTest();
#endif
			Sleep(1000);
		}

		//To tell the thread manager I'm living.
		RunThread_Heartbeat(g_pGcData->hThreadSelf);	
		if(GC_AppService != NULL)
		{
			GC_AppService->bReadyforNextService = TRUE;
		}
	}//while(bExitFlag)


	ExitGC();
	GC_DestroyMpptList();  
	//DELETE(g_pGcData);

	GC_LOG_OUT(APP_LOG_INFO, "*** The General Controller service exit! ***\n");

	return	0;
}


/*==========================================================================*
 * FUNCTION : GC_CheckConfigStatus
 * PURPOSE  : clear the private data and log data when the config solution
 *            is changed.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao         DATE: 2006-05-09 22:05
 *==========================================================================*/
static void CheckPriCfgChange(void)
{
	// the config changed, remove all data.
	if(DXI_GetConfigStatus(NULL) != CONFIG_STATUS_NOT_CHANGED)
	{
		DAT_StorageDeleteRecord(GEN_CTL_DATA);// remove private data
		DAT_StorageDeleteRecord(BATT_TEST_LOG_FILE);// remove battery test log
		DAT_StorageDeleteRecord(DSL_TEST_LOG);// remove diesel test log
	}
}


/*==========================================================================*
 * FUNCTION : GC_Init
 * PURPOSE  : 1. Read and parse private configuration file, initilize the g_pGcData
			  2. Initlize the timers
 * CALLS    : 
 * CALLED BY: ServiceMain
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-5-9 13:51:30
 *==========================================================================*/
static void	InitGC()
{
	g_pGcData->hThreadSelf = RunThread_GetId(NULL);	
	// check the config status, remove all data if cfg changed.
	CheckPriCfgChange();

	GC_PriCfgInit();
	GC_EquipInfoInit();
	GC_InitRectList();
	GC_TimersInit();
	GC_LoadData();
	GC_EngySavInit();
	GC_RectRedundInit();
	GC_DslTestInit();
	GC_PowerSplitInit();
	GC_MainSwitchInit();
	GC_BattMgmtInit();
	GC_EstopShutStateInit();
	GC_HybridMgmtTimerInit();
#ifdef GC_SUPPORT_MPPT
	GC_InitMpptList();
#endif
	GC_BattPredictInit();
	return;
}


/*==========================================================================*
 * FUNCTION : ExitGC
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-05-09 13:36
 *==========================================================================*/
static void	ExitGC(void)
{
	GC_BattMgmtDestroy();
	GC_TimersDestroy();
	GC_DestroyRectList();
	GC_PriCfgDestroy();
	return;
}


/*==========================================================================*
 * FUNCTION : GC_ErrHandler
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int   iErrCode     : 
 *            char  *szOutString : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-01-07 13:36
 *==========================================================================*/
void GC_ErrHandler(int iErrCode, char *szOutString)
{
#ifdef	_DEBUG
	printf("GC: %s, Error Code: %d.\n***General Controller Exit!!!***\n", 
			szOutString, 
			iErrCode);
#else
	AppLogOut("GEN CTRL", 
			APP_LOG_ERROR, 
			"%s, Error Code: %d.\n***General Controller Exit!!!***\n", 
			szOutString, 
			iErrCode); 
#endif
	longjmp(g_pGcData->JmpBuf, iErrCode);
}


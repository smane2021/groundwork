/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_lvd.h
 *  CREATOR  : Frank Cao                DATE: 2004-11-03 11:10
 *  VERSION  : V1.00
 *  PURPOSE  : To provide LVD interface function
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_MAIN_SWITCH_H_
#define _GC_MAIN_SWITCH_H_

#include "stdsys.h"
#include "public.h"
#include "basetypes.h"

#define	GC_MAIN_SWITCH_RECORD_NUM	3

#define	INTERVAL_MAIN_SWITCH_30S	30
#define	INTERVAL_MAIN_SWITCH_5S		5

void GC_MainSwitchInit(void);
void GC_MainSwitch(void);

#endif //_GC_MAIN_SWITCH_H_

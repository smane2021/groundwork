/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_main_switch.c
 *  CREATOR  : Frank Cao                DATE: 2005-03-02 18:35
 *  VERSION  : V1.00
 *  PURPOSE  : To provide Main Switch interface function
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gen_ctl.h"
#include	"gc_main_switch.h"
#include	"gc_sig_value.h"
#include	"gc_batt_mgmt.h"

static void MainSwitchOnManual(void);
static void MainSwitchOnOpen(void);
static void MainSwitchOnClose(void);
static void MainSwitchCloseCtl(void);
static BOOL MainSwitchNeedBlock(time_t tNow);
static void MainSwitchTimeCheck(time_t tNow);

/*==========================================================================*
 * FUNCTION : GC_MainSwitchInit
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-02 18:36
 *==========================================================================*/
void GC_MainSwitchInit(void)
{
	BOOL	*p_bNeedSave = g_pGcData->SecTimer.abNeedSave;

	GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S);
	p_bNeedSave[SEC_TMR_ID_MAIN_SWITCH_30S] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_5S);
	p_bNeedSave[SEC_TMR_ID_MAIN_SWITCH_5S] = FALSE;

	g_pGcData->RunInfo.Ms.bBlockCondition = FALSE;
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					MS_PUB_BLOCK_CONDITION, 
					FALSE, 
					TRUE);
	g_pGcData->RunInfo.Ms.iHead = 0;
	g_pGcData->RunInfo.Ms.iLength = 0;

	return;
}


/*==========================================================================*
 * FUNCTION : GC_MainSwitch
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-02 18:36
 *==========================================================================*/
void GC_MainSwitch(void)
{
	GC_RUN_INFO_MS	*pMsInfo = &(g_pGcData->RunInfo.Ms);
	
	if(!(g_pGcData->PriCfg.bMainSwitchEnb))
	{
		return;
	}

	MainSwitchTimeCheck(g_pGcData->SecTimer.tTimeNow);

	if((!(pMsInfo->bInputValid)) 
		|| (STATE_MAN == g_pGcData->RunInfo.enumAutoMan))
	{
		GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S);
		GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_5S);
		return;
	}

	if(!(pMsInfo->bAuto))
	{
		MainSwitchOnManual();
		return;
	}

	if(pMsInfo->bSwitchOpen)
	{
		MainSwitchOnOpen();
	}
	else
	{
		MainSwitchOnClose();
	}
	return;
}

/*==========================================================================*
 * FUNCTION : MainSwitchOnManual
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-04 20:00
 *==========================================================================*/
static void MainSwitchOnManual(void)
{
	GC_RUN_INFO_MS	*pMsInfo = &(g_pGcData->RunInfo.Ms);

	GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S);
	GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_5S);
	if((!(g_pGcData->RunInfo.Ms.bSwitchOpen))
		&& (!(g_pGcData->RunInfo.Ms.bRelayTrip)))
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						MS_PUB_BLOCK_CONDITION, 
						FALSE, 
						TRUE);
		pMsInfo->iHead = 0;
		pMsInfo->iLength = 0;

		if(g_pGcData->RunInfo.Ms.bBlockCondition)
		{
			g_pGcData->RunInfo.Ms.bBlockCondition = FALSE;
			GC_LOG_OUT(APP_LOG_INFO, 
				"Main-switch block condition is set FALSE.\n");
		}
	}
	return;
}


/*==========================================================================*
 * FUNCTION : MainSwitchOnOpen
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-04 19:39
 *==========================================================================*/
static void MainSwitchOnOpen(void)
{
	GC_RUN_INFO_MS	*pMsInfo = &(g_pGcData->RunInfo.Ms);

	if((!(pMsInfo->bBlockCondition)) 
		&& (pMsInfo->bDoorClose) 
		&& (pMsInfo->bRelayTrip))
	{
		if(SEC_TMR_SUSPENDED
			== GC_GetStateOfSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S))
		{
			GC_SetSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S, 
							INTERVAL_MAIN_SWITCH_30S,
							NULL,
							0);
		}
		else if(SEC_TMR_TIME_OUT
			== GC_GetStateOfSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S))
		{
			GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S);
			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							MS_PUB_RESET_CTL, 
							TRUE, 
							TRUE);
			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							MS_PUB_OPEN_CTL, 
							TRUE, 
							TRUE);
			GC_SetSecTimer(SEC_TMR_ID_MAIN_SWITCH_5S, 
							INTERVAL_MAIN_SWITCH_5S,
							NULL,
							0);
		}
	}
	else
	{
		GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S);
	}

	MainSwitchCloseCtl();

	return;
}



/*==========================================================================*
 * FUNCTION : MainSwitchOnClose
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-04 19:39
 *==========================================================================*/
static void MainSwitchOnClose(void)
{
	GC_SetEnumValue(TYPE_OTHER, 0, MS_PUB_CLOSE_CTL, FALSE, FALSE);
	GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_30S);
	MainSwitchCloseCtl();

	return;	
}



/*==========================================================================*
 * FUNCTION : MainSwitchCloseCtl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-04 19:40
 *==========================================================================*/
static void MainSwitchCloseCtl(void)
{
	if(SEC_TMR_TIME_OUT 
		!= GC_GetStateOfSecTimer(SEC_TMR_ID_MAIN_SWITCH_5S))
	{
		return;
	}

	GC_SuspendSecTimer(SEC_TMR_ID_MAIN_SWITCH_5S);
	
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					MS_PUB_RESET_CTL, 
					FALSE, 
					TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					MS_PUB_OPEN_CTL, 
					FALSE, 
					TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					MS_PUB_CLOSE_CTL, 
					TRUE, 
					TRUE);

	GC_LOG_OUT(APP_LOG_INFO, 
		"Make a closing Main-switch action.\n");

	if(MainSwitchNeedBlock(g_pGcData->SecTimer.tTimeNow))
	{
		g_pGcData->RunInfo.Ms.bBlockCondition = TRUE;
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						MS_PUB_BLOCK_CONDITION, 
						TRUE, 
						TRUE);
		GC_LOG_OUT(APP_LOG_INFO, 
			"Main-switch block condition is set TRUE.\n");

	}
	return;

}

/*==========================================================================*
 * FUNCTION : MainSwitchNeedBlock
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: time_t  tNow : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-04 19:40
 *==========================================================================*/
static BOOL MainSwitchNeedBlock(time_t tNow)
{
	int				iDiff;
	GC_RUN_INFO_MS	*pMsInfo = &(g_pGcData->RunInfo.Ms);

	pMsInfo->tmCloseTime[pMsInfo->iHead] = tNow;
	if(pMsInfo->iHead >= (GC_MAIN_SWITCH_RECORD_NUM - 1))
	{
		pMsInfo->iHead = 0;
	}
	else
	{
		pMsInfo->iHead++;
	}

	if(pMsInfo->iLength < GC_MAIN_SWITCH_RECORD_NUM)
	{
		pMsInfo->iLength++;
	}

	if(pMsInfo->iLength < GC_MAIN_SWITCH_RECORD_NUM)
	{
		return FALSE;
	}

	iDiff = tNow - pMsInfo->tmCloseTime[pMsInfo->iHead];
	if((iDiff > 0 ) && (iDiff < 180))
	{
		return TRUE;
	}
	return FALSE;

}


/*==========================================================================*
 * FUNCTION : MainSwitchTimeCheck
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: time_t  tNow : 
 * RETURN   : static : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-04 19:38
 *==========================================================================*/
static void MainSwitchTimeCheck(time_t tNow)
{
	static time_t		s_tLastTime = -1;
	long lDeltaTm;

	if(s_tLastTime == -1)
	{
		s_tLastTime = tNow;
		return;
	}

	lDeltaTm = tNow - s_tLastTime - GC_INTERVAL_MAIN_SWITCH;

	if(ABS(lDeltaTm) > 5)
	//means the clock has been modified.
	{
		int i;
		for(i = 0; i < GC_MAIN_SWITCH_RECORD_NUM; i++)
		{
			g_pGcData->RunInfo.Ms.tmCloseTime[i] += lDeltaTm;
		}
	}

	s_tLastTime = tNow;

	return;
}


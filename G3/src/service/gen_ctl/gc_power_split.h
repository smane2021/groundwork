/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_power_split.h
 *  CREATOR  : Frank Cao                DATE: 2004-12-21 11:43
 *  VERSION  : V1.00
 *  PURPOSE  : Power Split
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_POWER_SPLIT_H_
#define _GC_POWER_SPLIT_H_

enum	MASTER_INPUT_TYPE
{
	GC_MASTER_LVD1 = 0,
	GC_MASTER_LVD2,
	GC_MASTER_LVD3,
	GC_MASTER_TEST,
	GC_MASTER_BOOST_CHARGE,
};

#define	GC_MASTER_INPUT_NORMAL		0
#define	GC_MASTER_INPUT_ALARM		1
#define	GC_MASTER_INPUT_INVALID		-1

void GC_PowerSplit(void);
void GC_PowerSplitInit(void);

#endif //_GC_POWER_SPLIT_H_

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_sig_value.c
 *  CREATOR  : Frank Cao                DATE: 2004-10-20 13:55
 *  VERSION  : V1.00
 *  PURPOSE  : Get / Set signal value interface functions
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include	"gc_index_def.h"
#include	"gc_sig_value.h"
#include	"gen_ctl.h"
#include	"data_exchange.h"

//#define		GC_SIG_DEBUG

/*==========================================================================*
 * FUNCTION : GC_GetSigVal
 * PURPOSE  : Using the DxiGetData interface function, get a signal value
 * CALLS    : DxiGetData
 * CALLED BY: the functions that neeed get a signal value
 * ARGUMENTS: BYTE        iEquipType : 
 *            int         iEquipIdx   : 
 *            int         iSigIdx     : 
 *            SIG_BASIC_VALUE*  pSigValue   : the return value
 * RETURN   : 0: successful, other: failed 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 11:23
 *==========================================================================*/
int	GC_GetSigVal(int iEquipType, 
				 int iEquipIdx,
				 int iSigIdx, 
				 SIG_BASIC_VALUE** ppSigValue)
{
	int	iRst = ERR_CTL_GET_VLAUE;
	int	iBufLen;

	ASSERT(iEquipType < MAX_EQUIP_TYPE_NUM);

	switch(iEquipType)
	{
	case	TYPE_RECT_UNIT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdRtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pRectPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pRectPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_SLAVE1_RECT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdS1RtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pS1RectPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pS1RectPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_SLAVE2_RECT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdS2RtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pS2RectPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pS2RectPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_SLAVE3_RECT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdS3RtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pS3RectPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pS3RectPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_BATT_UNIT:
		/*TRACE(" \nTYPE_BATT_UNIT=[%d,%d [%d]\n", iEquipIdx, iSigIdx, g_pGcData->EquipInfo.aiEquipIdBtPri[iEquipIdx]);
		TRACE(" \nTYPE_BATT_UNIT= [%d]\n", (g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalType);
		TRACE(" \nTYPE_BATT_UNIT= [%d]\n", (g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalID);*/
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdBtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_BATT_FUSE_UNIT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdBtFusePri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattFusePriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pBattFusePriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_EIB_UNIT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdEibPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pEibPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pEibPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_LVD_UNIT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdLvdPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pLvdPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pLvdPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_OTHER:
		/*TRACE(" \nTYPE_OTHER=[%d,%d [%d]\n", iEquipIdx, iSigIdx, (g_pGcData->PriCfg.pSig + iSigIdx)->iEquipId);
		TRACE(" \nTYPE_OTHER= [%d]\n", (g_pGcData->PriCfg.pSig + iSigIdx)->iSignalType);
		TRACE(" \nTYPE_OTHER= [%d]\n", (g_pGcData->PriCfg.pSig + iSigIdx)->iSignalId);*/

		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			(g_pGcData->PriCfg.pSig + iSigIdx)->iEquipId,
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pSig + iSigIdx)->iSignalType, 
							(g_pGcData->PriCfg.pSig + iSigIdx)->iSignalId),
			&iBufLen,
			ppSigValue,
			0);
		break;

	case	TYPE_CT_UNIT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdCtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pCTPriSig + iSigIdx)->iSignalType,
			(g_pGcData->PriCfg.pCTPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);
		
		break;
#ifdef GC_SUPPORT_MPPT
	case	10://TYPE_MPPT_UNIT:
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdMtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pMpptPriSig + iSigIdx)->iSignalType,
			(g_pGcData->PriCfg.pMpptPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);

		break;
#endif
#ifdef GC_SUPPORT_BMS
	case	TYPE_BMS_UNIT://TYPE_MPPT_UNIT:
		printf("Get Value of  iSigIdx = %d, %d, %d, %d\n", iSigIdx, g_pGcData->EquipInfo.aiEquipIdBMSPri[iEquipIdx], (g_pGcData->PriCfg.pBMSPriSig + iSigIdx)->iSignalType,(g_pGcData->PriCfg.pBMSPriSig + iSigIdx)->iSignalID);
		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdBMSPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBMSPriSig + iSigIdx)->iSignalType,
			(g_pGcData->PriCfg.pBMSPriSig + iSigIdx)->iSignalID),
			&iBufLen,
			ppSigValue,
			0);

		break;
#endif
	default:
		break;
	
	}
	return iRst;
}


/*==========================================================================*
 * FUNCTION : GC_GetEnumValue
 * PURPOSE  : To get a ENUM signal value
 * CALLS    : GC_GetSigVal
 * CALLED BY: the functions that need to get a enum signal value
 * ARGUMENTS: BYTE  iEquipType : 
 *            int   iEquipIdx   : 
 *            int   iSigIdx     : 
 * RETURN   : SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 11:29
 *==========================================================================*/
SIG_ENUM GC_GetEnumValue(int iEquipType, 
				 int iEquipIdx, 
				 int iSigIdx)
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);

	if(iRst != ERR_OK)
	{
		sprintf(strOut, 
			"GC_GetEnumValue error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}

	if(VAR_ENUM != pSigValue->ucType) 
		//&& (0 != pSigValue->ucType))//for alarm signal
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetEnumValue");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);
	}
	
	return pSigValue->varValue.enumValue;
}
/*==========================================================================*
* FUNCTION : GC_GetEnumValueWithReturn
* PURPOSE  : To get a ENUM signal value
* CALLS    : GC_GetSigVal
* CALLED BY: the functions that need to get a enum signal value
* ARGUMENTS: BYTE  iEquipType : 
*            int   iEquipIdx   : 
*            int   iSigIdx     : 
* RETURN   : SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-14 11:29
*==========================================================================*/
BOOL GC_GetEnumValueWithReturn(int iEquipType, 
			 int iEquipIdx, 
			 int iSigIdx,
			 SIG_ENUM* pValue)
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);

	if(iRst != ERR_OK)
	{
		sprintf(strOut, 
			"GC_GetEnumValueWithReturn error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		AppLogOut("GEN CTRL", 
			APP_LOG_ERROR, 
			"%s, Error Code: %d.", 
			strOut, 
			ERR_CTL_GET_VLAUE);
		return FALSE;
	}

	if(VAR_ENUM != pSigValue->ucType) 
		//&& (0 != pSigValue->ucType))//for alarm signal
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetEnumValueWithReturn");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);
	}
	*pValue = pSigValue->varValue.enumValue;
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : GC_GetEnumValue
 * PURPOSE  : To get a ENUM signal value
 * CALLS    : GC_GetSigVal
 * CALLED BY: the functions that need to get a enum signal value
 * ARGUMENTS: BYTE  iEquipType : 
 *            int   iEquipIdx   : 
 *            int   iSigIdx     : 
 * RETURN   : SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 11:29
 *==========================================================================*/
SIG_ENUM GC_GetAlarmValue(int iEquipType, 
				 int iEquipIdx, 
				 int iSigIdx)
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	SIG_ENUM		enumValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);

	if(iRst != ERR_OK)
	{
		sprintf(strOut, 
			"GC_GetEnumValue error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}

	if(VAR_ENUM != pSigValue->ucType) 
		//&& (0 != pSigValue->ucType))//for alarm signal
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetAlarmValue");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);
	}

	enumValue = pSigValue->varValue.enumValue;

	if(pSigValue->ucSigType == SIG_TYPE_ALARM)
	{
		//TRACE("Enter alarm suppression!!!\n");
		if(!ALARM_IS_ACTIVING(pSigValue))
		{
			enumValue = 0;
		}
	}
	
	return enumValue;
}

/*==========================================================================*
 * FUNCTION : GC_GetFloatValue
 * PURPOSE  : To get a float signal value
 * CALLS    : GC_GetSigVal
 * CALLED BY: the functions that need to get a float signal value
 * ARGUMENTS: BYTE  iEquipType : 
 *            int   iEquipIdx   : 
 *            int   iSigIdx     : 
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 11:31
 *==========================================================================*/
float GC_GetFloatValue(int iEquipType, 
				 int iEquipIdx, 
				 int iSigIdx)
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);
	
	if(iRst != ERR_OK)
	{
		sprintf(strOut, 
			"GC_GetFloatValue error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}

	if(VAR_FLOAT != pSigValue->ucType)
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetFloatValue");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);	
	}
	
	return pSigValue->varValue.fValue;
}
/*==========================================================================*
* FUNCTION : GC_GetFloatValue
* PURPOSE  : To get a float signal value
* CALLS    : GC_GetSigVal
* CALLED BY: the functions that need to get a float signal value
* ARGUMENTS: BYTE  iEquipType : 
*            int   iEquipIdx   : 
*            int   iSigIdx     : 
* RETURN   : float : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-14 11:31
*==========================================================================*/
BOOL GC_GetFloatValueWithStateReturn(int iEquipType, 
		       int iEquipIdx, 
		       int iSigIdx,
		      float * pReturnValue )
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);


	if (iRst == ERR_DXI_INVALID_EQUIP_ID)
	{
		return FALSE;
	}
	

	if(iRst != ERR_OK)
	{
		sprintf(strOut, 
			"GC_GetFloatValue error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);

	}

	if(VAR_FLOAT != pSigValue->ucType)
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetFloatValueWithStateReturn");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);	
	}
	*pReturnValue = pSigValue->varValue.fValue;
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : GC_GetDwordValue
 * PURPOSE  : To get a DWORD signal value
 * CALLS    : GC_GetSigVal
 * CALLED BY: the functions that need to get a DWORD signal value
 * ARGUMENTS: BYTE  iEquipType : 
 *            int   iEquipIdx   : 
 *            int   iSigIdx     : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 11:32
 *==========================================================================*/
DWORD GC_GetDwordValue(int iEquipType, 
				 int iEquipIdx, 
				 int iSigIdx)
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);
	if(iRst != ERR_OK)
	{	
		sprintf(strOut, 
				"GC_GetDwordValue error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
				iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}

	if(VAR_UNSIGNED_LONG != pSigValue->ucType) 
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetDwordValue");		
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);
	}
	
	return pSigValue->varValue.ulValue;
}

/*==========================================================================*
* FUNCTION : GC_GetDwordValueWithStateRetrun
* PURPOSE  : To get a DWORD signal value
* CALLS    : GC_GetSigVal
* CALLED BY: the functions that need to get a DWORD signal value
* ARGUMENTS: BYTE  iEquipType : 
*            int   iEquipIdx   : 
*            int   iSigIdx     : 
* RETURN   : DWORD : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-14 11:32
*==========================================================================*/
BOOL GC_GetDwordValueWithStateReturn(int iEquipType, 
		       int iEquipIdx, 
		       int iSigIdx,
		       int iResetFlag,
		       DWORD * dwReturnValue)
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);
	if (iRst == ERR_DXI_INVALID_EQUIP_ID)
	{
		return FALSE;
	}
	if((iRst != ERR_OK)&&(iResetFlag ==TRUE ))
	{	
		sprintf(strOut, 
			"GC_GetDwordValueWithStateReturn error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}
	else if((iRst != ERR_OK)&&(iResetFlag ==FALSE ))
	{
		sprintf(strOut, 
			"GC_GetDwordValueWithStateReturn error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		AppLogOut("GEN CTRL", 
			APP_LOG_ERROR, 
			"%s, Error Code: %d.", 
			strOut, 
			ERR_CTL_GET_VLAUE);
		return FALSE;

	}

	if(VAR_UNSIGNED_LONG != pSigValue->ucType) 
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetDwordValueWithStateReturn");		
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);
	}
	*dwReturnValue = pSigValue->varValue.ulValue;
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : GC_GetLongValue
 * PURPOSE  : To get a Long signal value
 * CALLS    : GC_GetSigVal
 * CALLED BY: the functions that need to get a DWORD signal value
 * ARGUMENTS: BYTE  iEquipType : 
 *            int   iEquipIdx   : 
 *            int   iSigIdx     : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 11:32
 *==========================================================================*/
long GC_GetLongValue(int iEquipType, 
				 int iEquipIdx, 
				 int iSigIdx)
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);
	if(iRst != ERR_OK)
	{	
		sprintf(strOut, 
				"GC_GetLongValue error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
				iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}	

	if(VAR_LONG != pSigValue->ucType) 
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetLongValue");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);
	}
	
	return pSigValue->varValue.lValue;
}


/*==========================================================================*
 * FUNCTION : GC_GetTimeValue
 * PURPOSE  : To get a Data-Time signal value
 * CALLS    : GC_GetSigVal
 * CALLED BY: the functions that need to get a Data-Time signal value
 * ARGUMENTS: BYTE  iEquipType : 
 *            int   iEquipIdx   : 
 *            int   iSigIdx     : 
 * RETURN   : SIG_TIME : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 11:32
 *==========================================================================*/
SIG_TIME GC_GetTimeValue(int iEquipType, 
				 int iEquipIdx, 
				 int iSigIdx)
{
	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);
	if(iRst != ERR_OK)
	{	
		sprintf(strOut, 
				"GC_GetTimeValue error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
				iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}	

	if(VAR_DATE_TIME != pSigValue->ucType)
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetTimeValue");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);
	}
	
	return pSigValue->varValue.dtValue;
}



/*==========================================================================*
 * FUNCTION : GC_SetSigVal
 * PURPOSE  : Using the DxiSetData interface function, set a signal value 
 * CALLS    : DxiSetData
 * CALLED BY: the functions need to set a value
 * ARGUMENTS: BYTE            iEquipType : 
 *            int             iEquipIdx   : 
 *            int             iSigIdx     : 
 *            VAR_VALUE_EX  SigVal      : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-15 10:48
 *==========================================================================*/
void GC_SetSigVal(int iEquipType, 
				  int iEquipIdx, 
				  int iSigIdx, 
				  VAR_VALUE_EX SigVal)

{
	int	iRst = ERR_CTL_SET_VALUE;
	/*int	iEquipTypeId;
	int	iMajorId;*/
	
	ASSERT(iEquipType < MAX_EQUIP_TYPE_NUM);

	/*TRACE("After calling, iEquipType = %d, iEquipIdx = %d, iSigIdx = %d,\n", 
		iEquipType, 
		iEquipIdx,
		iSigIdx);*/

	switch(iEquipType)
	{
	case	TYPE_OTHER:
		//TRACE("GC:***iEquipId = %d,  iSignalType = %d, iSignalId = %d, iRst = ***\n",
		//		(g_pGcData->PriCfg.pSig + iSigIdx)->iEquipId,
		//		(g_pGcData->PriCfg.pSig + iSigIdx)->iSignalType,
		//		(g_pGcData->PriCfg.pSig + iSigIdx)->iSignalId
		//		);

		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			(g_pGcData->PriCfg.pSig + iSigIdx)->iEquipId,
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pSig + iSigIdx)->iSignalType, 
						(g_pGcData->PriCfg.pSig + iSigIdx)->iSignalId),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
#ifdef GC_SIG_DEBUG
		if(iRst)
		{
			//TRACE("GC:***iEquipId = %d,  iSignalType = %d, iSignalId = %d, iRst = %d***\n",
			//	(g_pGcData->PriCfg.pSig + iSigIdx)->iEquipId,
			//	(g_pGcData->PriCfg.pSig + iSigIdx)->iSignalType,
			//	(g_pGcData->PriCfg.pSig + iSigIdx)->iSignalId,
			//	iRst);
		}
#endif

		break;

	case	TYPE_RECT_UNIT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdRtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pRectPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pRectPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
		break;

	case	TYPE_SLAVE1_RECT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdS1RtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pS1RectPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pS1RectPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
		break;

	case	TYPE_SLAVE2_RECT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdS2RtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pS2RectPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pS2RectPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
		break;

	case	TYPE_SLAVE3_RECT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdS3RtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pS3RectPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pS3RectPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
		break;

	case	TYPE_LVD_UNIT:
		/*TRACE("Before execting, iEquipType = %d, iEquipIdx = %d, iSigIdx = %d, EquipId = %d, SigType = %d, SigId = %d, Value = %d\n", 
			iEquipType, 
			iEquipIdx,
			iSigIdx,
			g_pGcData->EquipInfo.aiEquipIdLvdPri[iEquipIdx],
			(g_pGcData->PriCfg.pLvdPriSig + iSigIdx)->iSignalType,
			(g_pGcData->PriCfg.pLvdPriSig + iSigIdx)->iSignalID,
			SigVal.varValue.enumValue);*/

		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdLvdPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pLvdPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pLvdPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);

		//TRACE("After execting, iRst = %d, \n", iRst);
		break;

	case	TYPE_BATT_UNIT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdBtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalType, 
						(g_pGcData->PriCfg.pBattPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
		break;

	case	TYPE_BATT_FUSE_UNIT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdBtFusePri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBattFusePriSig + iSigIdx)->iSignalType, 
						(g_pGcData->PriCfg.pBattFusePriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
		break;

	case	TYPE_EIB_UNIT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdEibPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pEibPriSig + iSigIdx)->iSignalType,
						(g_pGcData->PriCfg.pEibPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
		break;
#ifdef GC_SUPPORT_MPPT
	case	10://TYPE_MPPT_UNIT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdMtPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pMpptPriSig + iSigIdx)->iSignalType,
			(g_pGcData->PriCfg.pMpptPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);		
		break;
#endif
#ifdef GC_SUPPORT_BMS
	case	TYPE_BMS_UNIT://TYPE_MPPT_UNIT:
		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			g_pGcData->EquipInfo.aiEquipIdBMSPri[iEquipIdx],
			DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pBMSPriSig + iSigIdx)->iSignalType,
			(g_pGcData->PriCfg.pBMSPriSig + iSigIdx)->iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);		
		break;
#endif



	default:
		break;
	}
#ifdef	_GC_TEST
	g_pGcData->RunInfo.SetResult.iEquipType = iEquipType;
	g_pGcData->RunInfo.SetResult.iEquipIdx = iEquipIdx;
	g_pGcData->RunInfo.SetResult.iSigIdx = iSigIdx;
	g_pGcData->RunInfo.SetResult.SigVal = SigVal;
	g_pGcData->RunInfo.SetResult.iResult = iRst;
#endif

	return;
}


/*==========================================================================*
 * FUNCTION : GC_SetEnumValue
 * PURPOSE  : To set a ENUM value
 * CALLS    : GC_SetSigVal
 * CALLED BY: The functions that need to set a ENUM signal value
 * ARGUMENTS: BYTE      iEquipType : 
 *            int       iEquipIdx   : 
 *            int       iSigIdx     : 
 *            SIG_ENUM  enumValue   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 11:56
 *==========================================================================*/
void GC_SetEnumValue(int iEquipType, 
					 int iEquipIdx,
					 int iSigIdx,
					 SIG_ENUM enumValue,
					 BOOL bSendDirectly)
{
	VAR_VALUE_EX	sigValue;

	sigValue.nSendDirectly = (int)bSendDirectly;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	if(iEquipType == TYPE_LVD_UNIT 
		&& g_pGcData->RunInfo.Lvd.bLVD3Enable
		&& ((!g_SiteInfo.bLoadAllEquip && iEquipIdx == LVD3_EQUIP_ID_SEQUENCE)||(g_SiteInfo.bLoadAllEquip && iEquipIdx == LVD3_EQUIP_ID_485_SEQUENCE)))//19 is fixed for LVD3
	{
		sigValue.pszSenderName = LVD3_SENDER_NAME;
	}
	else
	{
		sigValue.pszSenderName = "GEN_CTRL";
	}
	sigValue.varValue.enumValue = enumValue;

	/*TRACE("Before calling, iEquipType = %d, iEquipIdx = %d, iSigIdx = %d,\n", 
		iEquipType, 
		iEquipIdx,
		iSigIdx);*/

	GC_SetSigVal(iEquipType, 
				 iEquipIdx,
				 iSigIdx,
				 sigValue);
	return;
}



/*==========================================================================*
 * FUNCTION : GC_SetFloatValue
 * PURPOSE  : To set a float value
 * CALLS    : GC_SetSigVal
 * CALLED BY: The functions that need to set a float signal value
 * ARGUMENTS: BYTE   iEquipType : 
 *            int    iEquipIdx   : 
 *            int    iSigIdx     : 
 *            flaot  fValue      : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 13:27
 *==========================================================================*/
void GC_SetFloatValue(int iEquipType, 
				 int iEquipIdx,
				 int iSigIdx,
				 float fValue)
{
	VAR_VALUE_EX	sigValue;

	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = "GEN_CTRL";
	sigValue.varValue.fValue = fValue;

	GC_SetSigVal(iEquipType, 
				 iEquipIdx,
				 iSigIdx,
				 sigValue);

	return;
}



/*==========================================================================*
 * FUNCTION : GC_SetDwordValue
 * PURPOSE  : To set a DWORD value
 * CALLS    : GC_SetSigVal
 * CALLED BY: The functions that need to set a DWORD signal value
 * ARGUMENTS: BYTE   iEquipType : 
 *            int    iEquipIdx   : 
 *            int    iSigIdx     : 
 *            DWORD  dwValue     : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 13:27
 *==========================================================================*/
void GC_SetDwordValue(int iEquipType, 
				 int iEquipIdx,
				 int iSigIdx,
				 DWORD dwValue)
{
	VAR_VALUE_EX	sigValue;

	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = "GEN_CTRL";
	sigValue.varValue.ulValue = dwValue;

	GC_SetSigVal(iEquipType, 
				 iEquipIdx,
				 iSigIdx,
				 sigValue);
	return;
}


/*==========================================================================*
 * FUNCTION : GC_SetDwordValue
 * PURPOSE  : To set a Date-time value
 * CALLS    : GC_SetSigVal
 * CALLED BY: The functions that need to set a Date-time signal value
 * ARGUMENTS: BYTE   iEquipType : 
 *            int    iEquipIdx   : 
 *            int    iSigIdx     : 
 *            DWORD  dwValue     : 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-14 13:27
 *==========================================================================*/
void GC_SetDateValue(int iEquipType, 
				 int iEquipIdx,
				 int iSigIdx,
				 time_t dtValue)
{
	VAR_VALUE_EX	sigValue;

	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = "GEN_CTRL";
	sigValue.varValue.dtValue = dtValue;

	GC_SetSigVal(iEquipType, 
				 iEquipIdx,
				 iSigIdx,
				 sigValue);
	return;
}


//G3_OPT [loader], by Lin.Tao.Thomas
SIG_ENUM GC_IsGCEquipExist(int iEquipType, 
					   int iEquipIdx, 
					   int iSigIdx,
					   int RTN)
{

	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);

	//not in the quip list of G3 solution
	if (iRst == ERR_DXI_INVALID_EQUIP_ID)
		return ((SIG_ENUM)RTN);

	if(iRst != ERR_OK)
	{
		sprintf(strOut, 
			"GC_GetEnumValue error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}

	if(VAR_ENUM != pSigValue->ucType) 
	{
		sprintf(strOut, 
			"Signal type error, E_TYPE: %d, E_IDX: %d, S_IDX: %d %d %s\n",
			iEquipType, iEquipIdx, iSigIdx,pSigValue->ucType, "GC_GetEnumValue");
		GC_ASSERT(FALSE, ERR_CTL_SIG_TYPE, strOut);
	}

	return  pSigValue->varValue.enumValue;
}

//G3_OPT [loader], by Lin.Tao.Thomas
BOOL GC_IsGCSigExist(int iEquipType, 
					 int iEquipIdx, 
					 int iSigIdx)
{

	char				strOut[256];

	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;

	iRst = GC_GetSigVal(iEquipType, iEquipIdx, iSigIdx, &pSigValue);

	//not in the quip list of G3 solution
	if (iRst == ERR_DXI_INVALID_EQUIP_ID)
		return FALSE;

	//not in the signal list of G3 solution
	if (iRst == ERR_DXI_NOT_FIND_SIGNAL)
	
		return FALSE;

	if(iRst != ERR_OK)
	{
		sprintf(strOut, 
			"GC_GetSigVal error! E_TYPE: %d, E_IDX: %d, S_IDX: %d\n",
			iEquipType, iEquipIdx, iSigIdx);
		GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
	}
	

	return SIG_VALUE_IS_CONFIGURED(pSigValue);
}


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_picket.h
 *  CREATOR  : Frank Cao                DATE: 2004-12-02 14:40
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#ifndef _GC_PICKET_H_
#define _GC_PICKET_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"


BOOL GC_Picket(void);
//BOOL GC_Picket2(void);
#endif //_GC_PICKET_H_

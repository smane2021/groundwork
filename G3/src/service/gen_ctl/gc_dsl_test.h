/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_dsl_test.h
 *  CREATOR  : Frank Cao                DATE: 2004-12-17 11:05
 *  VERSION  : V1.00
 *  PURPOSE  : Diesel Test
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_DSL_TEST_H_
#define _GC_DSL_TEST_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

enum	DSL_TEST_START_REASON
{
	DSL_TEST_FOR_PLAN = 0,
	DSL_TEST_MANUALLY,

	DSL_TEST_OTHER
};

#define	GC_DSL_TEST_START		1
#define	GC_DSL_TEST_STOP		0

#define	DSL_TEST_MAX_RECORDS	12 
#define	DSL_TEST_FREQUENCY		0.05

//[0=Normal]/[1=Manual Stop]/[2=Time is up]/
//[3=In Man State]/[4=Low Batt Vol]/[5=High Water Temp]
//[6=Low Oil Press]/[7=Low Fuel Level]/[8=Diesel Failure]
#define	DSL_EQUIP_INVALID	255 

void GC_DslTestInit(void);

void GC_DslTest(void);
int GC_GetDslTestDelay(int iIdx);
#endif //_GC_DSL_TEST_H_

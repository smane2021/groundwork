/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : pub_list.c
 *  CREATOR  : Frank Cao                DATE: 2004-09-16 09:09
 *  VERSION  : V1.00
 *  PURPOSE  : To provide a group of interface function which fulfil Double
 *             Links List
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"pub_list.h"


/*==========================================================================*
 * FUNCTION : List_Create
 * PURPOSE  : Creat a list
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: int  iRecordSize : The size od a record
 * RETURN   : HANDLE : Handle of the List
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:26
 *==========================================================================*/
HANDLE List_Create (int iRecordSize)
{
	HANDLE	hList = NULL;

	hList = NEW(PUB_LIST, 1);
	if(NULL != hList)
	{
		((PUB_LIST*)hList)->dwSize = iRecordSize;
		((PUB_LIST*)hList)->pHead = NULL;
		((PUB_LIST*)hList)->pCurr = NULL;
	}
	return hList;
}


/*==========================================================================*
 * FUNCTION : List_Destroy
 * PURPOSE  : Destroy the list
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : Handle of the List
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:27
 *==========================================================================*/
void List_Destroy(HANDLE hList)
{
	LIST_NODE	*p;
	LIST_NODE	*q;

	if(NULL != hList)
	{
		p = ((PUB_LIST*)hList)->pHead;
		while(p)
		{
			q = p;
			p = p->pNext;
            
			if(q->pRecord)
			{
				DELETE(q->pRecord);
			}
			DELETE(q);
		}
		DELETE((PUB_LIST*)hList);
	}
	return;
}


/*==========================================================================*
 * FUNCTION : List_Insert
 * PURPOSE  : Only permit to insert a record at head
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList    : Handle of the List
 *            void    *pRecord : Point to record content
 * RETURN   : TRUE : successful
              FALSE: failed
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:28
 *==========================================================================*/
BOOL List_Insert(HANDLE hList,
				 const void *pRecord)
{
	LIST_NODE	*p;

	if((NULL == hList) || (NULL == pRecord))
	{
		return FALSE;
	}
	
	p = NEW(LIST_NODE, 1);
	if(NULL == p)
	{
		return FALSE;
	}

	p->pRecord = NEW(char, ((PUB_LIST*)hList)->dwSize);
	if(NULL == p->pRecord)
	{
		return FALSE;
	}
	
	memcpy(p->pRecord, pRecord, ((PUB_LIST*)hList)->dwSize);
	p->pNext = ((PUB_LIST*)hList)->pHead;
	p->pPrev = NULL;

	if(NULL != ((PUB_LIST*)hList)->pHead)
	{
		(((PUB_LIST*)hList)->pHead)->pPrev = p;
	}
	((PUB_LIST*)hList)->pHead = p;
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : List_Delete
 * PURPOSE  : delete the current record
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : Handle of the List
 * RETURN   : TRUE : successful
              FALSE: not delete
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:32
 *==========================================================================*/
BOOL List_Delete(HANDLE hList)
{
	LIST_NODE* p;
	LIST_NODE* q;
	LIST_NODE* r;

	if(NULL == hList)
	{
		return FALSE;
	}
	if(NULL == ((PUB_LIST*)hList)->pHead)
	{
		return FALSE;
	}
	
	if(((PUB_LIST*)hList)->pCurr == ((PUB_LIST*)hList)->pHead)
	{
		((PUB_LIST*)hList)->pHead = (((PUB_LIST*)hList)->pCurr)->pNext;
	}

	p = ((PUB_LIST*)hList)->pCurr;
	ASSERT(p);

	q = p->pNext;
	r = p->pPrev;

	((PUB_LIST*)hList)->pCurr = q;

	if(r)
	{
		r->pNext = q;
	}

	if(q)
	{
		q->pPrev = r;
	}

	if(p->pRecord)
	{
		DELETE(p->pRecord);
	}
	DELETE(p);
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : List_Get
 * PURPOSE  : copy a current record to pOut
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : 
 *            void*   pOut  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:32
 *==========================================================================*/
BOOL List_Get(HANDLE hList,
			 void*	pOut)
{
	if(NULL == pOut)
	{
		return FALSE;
	}

	if(NULL == hList)
	{
		return FALSE;
	}

	if(NULL == ((PUB_LIST*)hList)->pCurr)
	{
		return FALSE;
	}

	if(NULL == (((PUB_LIST*)hList)->pCurr)->pRecord)
	{
		return FALSE;
	}

	memcpy(pOut, 
		(((PUB_LIST*)hList)->pCurr)->pRecord,
		((PUB_LIST*)hList)->dwSize);
	return TRUE;

}


/*==========================================================================*
 * FUNCTION : List_Put
 * PURPOSE  : to modify a record
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : 
 *            void*   pOut  : 
 * RETURN   : BOOL : TRUE: successful, else fail
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 17:18
 *==========================================================================*/
BOOL List_Set(HANDLE hList,
			 const void* pOut)
{
	if(NULL == pOut)
	{
		return FALSE;
	}

	if(NULL == hList)
	{
		return FALSE;
	}

	if(NULL == ((PUB_LIST*)hList)->pCurr)
	{
		return FALSE;
	}

	if(NULL == (((PUB_LIST*)hList)->pCurr)->pRecord)
	{
		return FALSE;
	}

	memcpy((((PUB_LIST*)hList)->pCurr)->pRecord, 
			pOut, 
			((PUB_LIST*)hList)->dwSize);
	return TRUE;

}


/*==========================================================================*
 * FUNCTION : List_Head
 * PURPOSE  : To move pCurr to head of list
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : 
 * RETURN   : TURE: successful
              FALSE: hList is NULL
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:33
 *==========================================================================*/
BOOL List_GotoHead(HANDLE hList)
{
	if(NULL == hList)
	{
		return FALSE;
	}
	((PUB_LIST*)hList)->pCurr = ((PUB_LIST*)hList)->pHead;
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : List_GotoTail
 * PURPOSE  : To move the pCurr to tail of the list
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : 
 * RETURN   : TURE: successful
              FALSE: hList is NULL 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 17:33
 *==========================================================================*/
BOOL List_GotoTail(HANDLE hList)
{
	if(NULL == hList)
	{
		return FALSE;
	}
	List_GotoHead(hList);
	do	{} while(List_GotoNext(hList));
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : List_Next
 * PURPOSE  : To move the currrent point to next item of the list, expect 
              pCurr point to the tail
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : 
 * RETURN   : TRUE: move to prov, otherwise not move
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:34
 *==========================================================================*/
BOOL List_GotoNext(HANDLE hList)
{
	if(NULL == hList)
	{
		return FALSE;
	}
	if(((PUB_LIST*)hList)->pCurr)
	{
		if((((PUB_LIST*)hList)->pCurr)->pNext)
		{
			((PUB_LIST*)hList)->pCurr = (((PUB_LIST*)hList)->pCurr)->pNext;
			return TRUE;
		}
	}
	return FALSE;
}


/*==========================================================================*
 * FUNCTION : List_Prev
 * PURPOSE  : To move current point to previous item of the list, expect 
              pCurr point to the head
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : 
 * RETURN   : BOOL : TRUE: move to prov, otherwise not move
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:35
 *==========================================================================*/
BOOL List_GotoPrev(HANDLE hList)
{
	if(NULL == hList)
	{
		return FALSE;
	}
	if(((PUB_LIST*)hList)->pCurr)
	{
		if((((PUB_LIST*)hList)->pCurr)->pPrev)
		{
			((PUB_LIST*)hList)->pCurr = (((PUB_LIST*)hList)->pCurr)->pPrev;
			return TRUE;
		}
	}
	return FALSE;
}


/*==========================================================================*
 * FUNCTION : List_IsEmpty
 * PURPOSE  : To judge if the list is empty
 * CALLS    : 
 * CALLED BY: The functions that need to use List
 * ARGUMENTS: HANDLE  hList : 
 * RETURN   : BOOL : TRUE: the hList is empty, otherwise not empty
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-16 11:35
 *==========================================================================*/
BOOL List_IsEmpty(HANDLE hList)
{
	if(NULL == hList)
	{
		return FALSE;
	}
	if(((PUB_LIST*)hList)->pHead)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

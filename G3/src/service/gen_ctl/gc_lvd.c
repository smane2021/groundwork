/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_lvd.c
 *  CREATOR  : Frank Cao                DATE: 2004-11-03 11:20
 *  VERSION  : V1.00
 *  PURPOSE  : To provide LVD interface function
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gen_ctl.h"
#include	"gc_batt_mgmt.h"
//#include	"gc_rect_redund.h"
#include	"gc_sig_value.h"
#include	"gc_lvd.h"
static int GC_GetReconnectDelay_LargeDU(int iIdx);
static int GC_GetLvdTime_LargeDU(int iIdx);
static float GC_GetHDTemperature(void);
static void GC_SynchLVDBoard();

/*==========================================================================*
 * FUNCTION : IsUnderReconnectTemp
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-11-04 16:24
 *==========================================================================*/
static BOOL IsUnderReconnectTemp(void)
{
	/*int		i;

	float	fReconectTemp = GC_GetFloatValue(TYPE_OTHER, 
		0,
		BT_PUB_RECONNECT_TEMP);

	for(i = 0; i < MAX_NUM_TEMPERATURE - MAX_NUM_LargeDU_TEMP; i++)
	{
		SIG_ENUM	enumUsage = GC_GetEnumValue(TYPE_OTHER, 
			0,
			GC_PUB_TEMP1_USAGE + i);
		if((enumUsage == TEMP_USAGE_BATTERY)
			&& (GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_TEMPERATURE1 + i)
	> fReconectTemp))
		{
			return FALSE;
		}
	}

	return TRUE;*/



}



/*==========================================================================*
 * FUNCTION : BattLvdCtl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SIG_ENUM  enumVal : 
 * RETURN   : static : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-11-04 16:24
 *==========================================================================*/
static void BattLvdCtl(SIG_ENUM	enumVal)
{
	int		i;

	VAR_VALUE_EX		SigValue;
	SIG_BASIC_VALUE*	pSigValue;

	int					iRst = ERR_CTL_GET_VLAUE;
	int					iBufLen;
	char				strOut[512];

	SigValue.nSendDirectly = (int)TRUE;
	SigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	SigValue.pszSenderName = "GEN_CTRL";
	SigValue.varValue.enumValue = enumVal;

	for(i = 0; i < g_pGcData->PriCfg.iLvdNumOnVeryHiTemp; i++)
	{
		GC_SIG_INFO*	pBattLvdSig = g_pGcData->PriCfg.pLvdOnVeryHiTemp + i;
		if(!(pBattLvdSig->bEnabled))
		{
			continue;
		}

		iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
			pBattLvdSig->iEquipId,
			DXI_MERGE_SIG_ID(pBattLvdSig->iSignalType, pBattLvdSig->iSignalId),
			&iBufLen,
			&pSigValue,
			0);
		
		if(iRst != ERR_OK)
		{
			sprintf(strOut, 
				"Error Battery LVD control signal in GC private config file! \n");
			GC_ErrHandler(ERR_CTL_GET_VLAUE, strOut);
		}

		if(pSigValue->varValue.enumValue != enumVal)
		{
			DxiSetData(VAR_A_SIGNAL_VALUE,
				pBattLvdSig->iEquipId,
				DXI_MERGE_SIG_ID(pBattLvdSig->iSignalType, pBattLvdSig->iSignalId),
				sizeof(VAR_VALUE_EX),
				&SigValue,
				0);
		}
	}

	return;
}



/*==========================================================================*
 * FUNCTION : Reconnection
 * PURPOSE  : Handling in disconnection state
 * CALLS    : GC_GetFloatValue GC_GetStateOfSecTimer GC_SuspendSecTimer
              GC_SetSecTimer DxiSetData
 * CALLED BY: GC_LvdRun
 * ARGUMENTS: int  iIdx : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-03 16:06
 *==========================================================================*/
static void Reconnection(int iIdx)
{
	BOOL	bReconnect = FALSE,bRTN;
	int		iLvdEquipNo = iIdx / 2;
	int		iLvdNo = iIdx % 2;
	float	fReconnectVolt,fReconnectTemp;
	static int iVoltAdjustlimitCount[MAX_NUM_LVD_EQUIP*2];//0:timedelay 1:voltadjustwait 2:reconnect
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif

#ifdef   GC_SUPPORT_HIGHTEMP_DISCONNECT
	float fBatteryTemp = GC_GetFloatValue(TYPE_OTHER, 
		0, 
		GC_PUB_BATTGROUP_BATTTEMP);
	float fAmbineTemp = GC_GetFloatValue(TYPE_OTHER, 
		0, 
		GC_PUB_AMBINE_TEMPERATURE);

	float fTempDelta = GC_GetDwordValue(TYPE_OTHER, 
		0, 
		GC_PUB_TEMP_DELTA);


	if(iIdx == GC_OB_LVD2 && ABS(fBatteryTemp - fAmbineTemp) > (10 - fTempDelta))
	{
		return;
	}
#endif   GC_SUPPORT_HIGHTEMP_DISCONNECT

	if(GC_DISABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, 
					iLvdEquipNo,
					LVD1_PRI_ENB + iLvdNo,GC_DISABLED))
	{
		return;
	}

	bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, LVD_HTD_RECON_POINT,&fReconnectTemp);
/* Added by Victor Fan for CR# 0188-07-ACU, 2007-08-30 */
	if( (iLvdEquipNo <= 19) && (GC_ENABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, iLvdEquipNo, LVD1_HTD_ENB  + iLvdNo,GC_EQUIP_INVALID))
		&& (bRTN == TRUE)&& (GC_GetHDTemperature() >fReconnectTemp))
	{
		return;
	}
/* Ended by Victor Fan for CR# 0188-07-ACU, 2007-08-30 */

	if(g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx])
	//AC was failure when the LVD was disconnected
	{
		if(!g_pGcData->RunInfo.Rt.bAcFail)
		//but now AC is not failure
		{
			bReconnect = TRUE;
		}
	}

	//AC was not failure when the LVD was disconnected
	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
							iLvdEquipNo,
							LVD1_PRI_RECON_VOLT24 + iLvdNo,
							&fReconnectVolt );
	}
	else
	{
		bRTN  = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
							iLvdEquipNo,
							LVD1_PRI_RECON_VOLT + iLvdNo,
							&fReconnectVolt);
	
	}

	if(((bRTN  == TRUE )&&(g_pGcData->RunInfo.fSysVolt > fReconnectVolt))
	    ||(iVoltAdjustlimitCount[iIdx]>=1))
	{
		bReconnect = TRUE;

	}

	if(bReconnect)
	{

		if(SEC_TMR_SUSPENDED 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iIdx))
		{

			GC_SetSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iIdx, 
							0,
							GC_GetReconnectDelay,
							iIdx);
			iVoltAdjustlimitCount[iIdx]= 0;
		}

		else if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iIdx))
		{
			char	strOut[512];
			if(GC_IsNeedVoltAdjust() == TRUE)  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
			{
				g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;
				CurrentLimitThroughVolt(TRUE ,BT_PUB_NOM_VOLT, FALSE);
				iVoltAdjustlimitCount[iIdx]++;
			}
			else
			{
				iVoltAdjustlimitCount[iIdx]++;
#ifdef GC_SUPPORT_MPPT		
				
				if(stState != GC_MPPT_DISABLE)
				{
					GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect + g_pGcData->RunInfo.Mt.iQtyOnWorkRect , FALSE,TRUE);
				}
				else
				{
					GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE,TRUE);
				}
#else
				GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE,TRUE);
	
#endif			
			}
			
			if(iVoltAdjustlimitCount[iIdx] >=2)
			{
				
				GC_SetEnumValue(TYPE_LVD_UNIT, 
					iLvdEquipNo, 
					LVD1_PRI_CTL + iLvdNo, 
					GC_CONNECTED, 
					TRUE);

				GC_SuspendSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iIdx);
				iVoltAdjustlimitCount[iIdx]= 0;

				sprintf(strOut,
					"LVD%d re-connect, AC state is %s. DC voltage is %.2f.\n",
					iIdx + 1,
					(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
					g_pGcData->RunInfo.fSysVolt);

				GC_LOG_OUT(APP_LOG_INFO, strOut);

			}
			
		}
	}
	else
	{
		GC_SuspendSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iIdx);
	}

	return;
}

/*==========================================================================*
 * FUNCTION : Reconnection_LargeDU
 * PURPOSE  : Handling in disconnection state
 * CALLS    : GC_GetFloatValue GC_GetStateOfSecTimer GC_SuspendSecTimer
              GC_SetSecTimer DxiSetData
 * CALLED BY: GC_LvdRun
 * ARGUMENTS: int  iIdx : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Marco Yang                DATE: 2010-03-10 16:06
 *==========================================================================*/
static void Reconnection_LargeDU(int iIdx)
{
	BOOL	bReconnect = FALSE;
	int		iLvdEquipNo = iIdx / 2;
	int		iLvdNo = iIdx % 2;
	float	fReconnectVolt;
	int	i;
	BOOL	bDisConnected = FALSE,bRTN;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{		
		
		if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + iLvdNo])
		{
			if(GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + iLvdNo])
			{
				bDisConnected = TRUE;
				break;	
			}			
		}		
	}
	if(!bDisConnected)
	{
		return;
	}

	if(g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx])
	//AC was failure when the LVD was disconnected
	{
		if(!g_pGcData->RunInfo.Rt.bAcFail)
		//but now AC is not failure
		{
			bReconnect = TRUE;
		}
	}

	//AC was not failure when the LVD was disconnected


	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
			0,
			LDU_LVD1_PRI_RECON_VOLT24 + iLvdNo,
			&fReconnectVolt);
	}
	else
	{
		bRTN  = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
			0,
			LDU_LVD1_PRI_RECON_VOLT + iLvdNo,
			&fReconnectVolt);

	}
	

	if((bRTN == TRUE)&&(g_pGcData->RunInfo.fSysVolt > fReconnectVolt))
	{
		bReconnect = TRUE;		
	}
	

	if(bReconnect)
	{
		if(SEC_TMR_SUSPENDED 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iLvdNo))
		{

			GC_SetSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iLvdNo, 
							0,
							GC_GetReconnectDelay_LargeDU,
							iLvdNo);
			GC_LOG_OUT(APP_LOG_INFO, "SEC_TMR_SUSPENDED\n");
		}

		else if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iLvdNo))
		{
			char	strOut[512];
			
			GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, FALSE,TRUE);
			
			for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
			{		
				if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + iLvdNo])
				{
					if(GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + iLvdNo])
					{
						g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i * 2 + iLvdNo] = FALSE;
						
						GC_SetEnumValue(TYPE_LVD_UNIT, 
							i, 
							LVD1_PRI_CTL + iLvdNo, 
							GC_CONNECTED, 
							TRUE);	
					}			
				}		
			}

			

			GC_SuspendSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iLvdNo);

			sprintf(strOut,
				"LVD%d re-connect, AC state is %s. DC voltage is %.2f.\n",
				iLvdNo,
				(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
				g_pGcData->RunInfo.fSysVolt);

			GC_LOG_OUT(APP_LOG_INFO, strOut);
		}
		else
		{
			//Null
		}
	}
	else
	{
		//GC_LOG_OUT(APP_LOG_INFO, "bReconnect is false\n");
		GC_SuspendSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iLvdNo);
	}

	return;
}
/*==========================================================================*
 * FUNCTION : Disconnection
 * PURPOSE  : Handling in connection state
 * CALLS    : GC_GetEnumValue GC_SetSecTimer GC_GetFloatValue DxiSetData
              GC_GetStateOfSecTimer
 * CALLED BY: GC_LvdRun
 * ARGUMENTS: int  iIdx : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-03 16:07
 *==========================================================================*/
static void Disconnection(int iIdx)
{
	static int		s_aiCounter[MAX_NUM_LVD_EQUIP * 2 ];
	int			iDependency;
	int			iLvdEquipNo = iIdx / 2;
	int			iLvdNo = iIdx % 2;
	float			fLvdVolt;
	float			fReconnectVolt,fReconnectTemp;
	BOOL			bRTN;
	int			iLVD3Sequence = LVD3_EQUIP_ID_SEQUENCE;

	SIG_ENUM		enumState;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);


	GC_SuspendSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iIdx);

	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);
	if((enumState == ST_SHORT_TEST)
		|| (enumState == ST_MAN_TEST)
		|| (enumState == ST_PLAN_TEST))
	{
		s_aiCounter[iIdx] = 0;
		GC_SuspendSecTimer(SEC_TMR_ID_LVD1_DISCH + iIdx);

		return;
	}
	if(GC_DISABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, 
						iLvdEquipNo,
						LVD1_PRI_ENB + iLvdNo,GC_DISABLED))
	{
		s_aiCounter[iIdx] = 0;

		return;
	}

	if(SEC_TMR_TIME_IN 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_OF_LVD))  // ����Ԥ�������ܵ���ģ���������������ظ����µ����⣬����Ԥ����ʱ���µ�
	{
		s_aiCounter[iIdx] = 0;
		g_pGcData->RunInfo.bLVDPreLmtToCurrLmtFlag = TRUE;
		g_pGcData->RunInfo.fSysLoadofPreCurrLimit = g_pGcData->RunInfo.fSysLoad;
		return;
	}
// dependency check
	// 0 indicates No Dependedcy.	
	//iDependency = GC_IsGCEquipExist(TYPE_LVD_UNIT, 
	//						iLvdEquipNo,
	//						LVD1_PRI_DEPEND + iLvdNo,GC_DISABLED);
	//if(iDependency) 
	//{
	//	if((iLvdNo != (iDependency - 1))	//depend on itself
	//		&& (g_pGcData->RunInfo.Lvd.abValid[iLvdEquipNo * 2 + iDependency - 1])
	//		&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[iLvdEquipNo * 2 + iDependency - 1]))
	//	{
	//		s_aiCounter[iIdx] = 0;

	//		return;
	//	}
	//}
	iDependency = GC_IsGCEquipExist(TYPE_LVD_UNIT, 
							iLvdEquipNo,
							LVD1_PRI_DEPEND + iLvdNo,GC_DISABLED);

	/*if(iLvdEquipNo == LVD3_EQUIP_ID_SEQUENCE && iLvdNo == 0 )
	{
		if(g_pGcData->RunInfo.Rt.bAcFail)
		{
			s_aiCounter[iIdx]++;
		}
		else
		{
			s_aiCounter[iIdx] = 0;
		}

		if(s_aiCounter[iIdx] >= 3)
		{
			char	strSpOut[512];

			
			GC_SetEnumValue(TYPE_LVD_UNIT, 
						iLvdEquipNo,
						LVD1_PRI_CTL + iLvdNo, 
						GC_DISCONNECTED, 
						TRUE);
			
			g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx]
				= g_pGcData->RunInfo.Rt.bAcFail;
			
			s_aiCounter[iIdx] = 0;

			sprintf(strSpOut,
				"LVD%d disconnect for AC Failure. DC voltage is %.2f.\n",
				iIdx + 1,
				g_pGcData->RunInfo.fSysVolt);
			GC_LOG_OUT(APP_LOG_INFO, strSpOut);
		}
		return;
	}*/
	//printf("iLvdEquipNo= %d, iLvdNo= %d, iDependency = %d\n",iLvdEquipNo, iLvdNo, iDependency);
	if(iDependency) 
	{
		if(iLvdEquipNo == OBLVD_EQUIP_ID_SEQUENCE)
		{
			if(iLvdNo != (iDependency - 1))				//depend on itself
			{
				if(iDependency == 3)//Depend on LVD3
				{
					if(g_SiteInfo.bLoadAllEquip)
					{
						iLVD3Sequence = LVD3_EQUIP_ID_485_SEQUENCE;
					}
					else
					{
						iLVD3Sequence = LVD3_EQUIP_ID_SEQUENCE;
					}
					if((g_pGcData->RunInfo.Lvd.abValid[iLVD3Sequence*2])
						&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[iLVD3Sequence * 2 ]))
					{
						s_aiCounter[iIdx] = 0;
						return;
					}
					
				}
				else
				{
					if((g_pGcData->RunInfo.Lvd.abValid[OBLVD_EQUIP_ID_SEQUENCE * 2 + iDependency - 1])
						&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[OBLVD_EQUIP_ID_SEQUENCE * 2 + iDependency - 1]))
					{
						s_aiCounter[iIdx] = 0;
						return;
					}
				}
			}


		}
		else if((g_SiteInfo.bLoadAllEquip && iLvdEquipNo == LVD3_EQUIP_ID_485_SEQUENCE)
			|| (!g_SiteInfo.bLoadAllEquip && iLvdEquipNo == LVD3_EQUIP_ID_SEQUENCE))
		{
			//printf("iLvdEquipNo = %d iDependency = %d\n",iLvdEquipNo, iDependency);
			if(iDependency != 3 )				//depend on itself
			{
				//printf("g_pGcData->RunInfo.Lvd.abValid[iDependency - 1]) = %d, g_pGcData->RunInfo.Lvd.aenumState[iDependency - 1] = %d\n",g_pGcData->RunInfo.Lvd.abValid[iDependency - 1], g_pGcData->RunInfo.Lvd.aenumState[iDependency - 1]);
				if((g_pGcData->RunInfo.Lvd.abValid[iDependency - 1])
					&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[iDependency - 1]))
				{
					//printf("Equip %d Return;\n",iLvdEquipNo);
					s_aiCounter[iIdx] = 0;
					return;
				}
				
			}
		}
		else
		{
			if((iLvdNo != (iDependency - 1))	//depend on itself
				&& (g_pGcData->RunInfo.Lvd.abValid[iLvdEquipNo * 2 + iDependency - 1])
				&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[iLvdEquipNo * 2 + iDependency - 1]))
			{
				s_aiCounter[iIdx] = 0;

				return;
			}
		}
	}

#ifdef   GC_SUPPORT_HIGHTEMP_DISCONNECT
	float fBatteryTemp = GC_GetFloatValue(TYPE_OTHER, 
		0, 
		GC_PUB_BATTGROUP_BATTTEMP);
	float fAmbineTemp = GC_GetFloatValue(TYPE_OTHER, 
		0, 
		GC_PUB_AMBINE_TEMPERATURE);

	if(iIdx == GC_OB_LVD2 && ABS(fBatteryTemp - fAmbineTemp) > GC_MAX_DELTA_BETWEEN_AMBINE_BATT)
	{
		if(s_aiCounter[iIdx] > 5)
		{
			char	strOut[512];
			
			g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[iIdx] = g_pGcData->RunInfo.fSysLoad;

			GC_SetEnumValue(TYPE_LVD_UNIT, 
				iLvdEquipNo,
				LVD1_PRI_CTL + iLvdNo, 
				GC_DISCONNECTED, 
				TRUE);


			g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx]
			= g_pGcData->RunInfo.Rt.bAcFail;

			s_aiCounter[iIdx] = 0;

			sprintf(strOut,
				"LVD%d disconnect for the delta between ambine temp and batt temp, AC state is %s. DC voltage is %.2f.\n",
				iIdx + 1,
				(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
				g_pGcData->RunInfo.fSysVolt);
			GC_LOG_OUT(APP_LOG_INFO, strOut);
		}
		else
		{
			s_aiCounter[iIdx]++;
		}

		return;
	}
#endif  // GC_SUPPORT_HIGHTEMP_DISCONNECT

		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, LVD_HTD_POINT,&fReconnectTemp);
/* Added by Victor Fan for CR# 0188-07-ACU, 2007-08-30 */
	//printf("iLvdEquipNo = %d , GC_IsGCEquipExist(TYPE_LVD_UNIT, iLvdEquipNo, LVD1_HTD_ENB  + iLvdNo,GC_EQUIP_INVALID)= %d,bRTN = %d\n",iLvdEquipNo, GC_IsGCEquipExist(TYPE_LVD_UNIT, iLvdEquipNo, LVD1_HTD_ENB  + iLvdNo,GC_EQUIP_INVALID),bRTN);
	if((iLvdEquipNo <= 19) && (GC_ENABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, iLvdEquipNo, LVD1_HTD_ENB  + iLvdNo,GC_EQUIP_INVALID))
		&&((bRTN == TRUE)&&(GC_GetHDTemperature() > fReconnectTemp)))
	{
		if(s_aiCounter[iIdx] > 5)
		{
			char	strOut[512];

			
			g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[iIdx] = g_pGcData->RunInfo.fSysLoad;
			GC_SetEnumValue(TYPE_LVD_UNIT, 
						iLvdEquipNo,
						LVD1_PRI_CTL + iLvdNo, 
						GC_DISCONNECTED, 
						TRUE);
			
			g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx]
				= g_pGcData->RunInfo.Rt.bAcFail;
			
			s_aiCounter[iIdx] = 0;

			sprintf(strOut,
				"LVD%d disconnect for High Temperature, AC state is %s. DC voltage is %.2f.\n",
				iIdx + 1,
				(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
				g_pGcData->RunInfo.fSysVolt);
			GC_LOG_OUT(APP_LOG_INFO, strOut);
		}
		else
		{
			s_aiCounter[iIdx]++;

		}

		return;
	}
/* Ended by Victor Fan for CR# 0188-07-ACU, 2007-08-30 */

/* Changed by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
	if(!GC_IsDislPwrLmtMode() && GC_DISABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_ENABLE))
	{
		
		if((!(g_pGcData->RunInfo.Rt.bAcFail))
			&& (GC_ENABLED == GC_IsGCEquipExist(TYPE_OTHER, 0, LVD_NEED_AC_FAIL,GC_EQUIP_INVALID)))
		{
			s_aiCounter[iIdx] = 0;

			return;
		}
		
		/*Start Modified by Marco Yang, Don't consider the current condition, 2010-4-5*/
		//Discharge and AC failure check
		//if((!g_pGcData->RunInfo.Bt.bDischarge) 
		//	&& (!g_pGcData->RunInfo.Rt.bAcFail))
		//not discharge and not AC failure
		//{
		//	s_aiCounter[iIdx] = 0;
		//	return;
		//}
		/*End Modified by Marco Yang, Don't consider the current condition, 2010-4-5*/

		if((GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_SET))
			&& (g_pGcData->RunInfo.Rt.fCurrLmt < 100.0)
			&& (!GC_IsPowerLmt()))
		//Rectifer current limit not relax
		{
			s_aiCounter[iIdx] = 0;

			return;
		}
	}
/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */

	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if (stVoltLevelState)//24V
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
			iLvdEquipNo,
			LVD1_PRI_VOLT24 + iLvdNo,
			&fLvdVolt );
	}
	else
	{
		bRTN  = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
			iLvdEquipNo,
			LVD1_PRI_VOLT + iLvdNo,
			&fLvdVolt);

	}


	if((bRTN == TRUE)&&(g_pGcData->RunInfo.fSysVolt < fLvdVolt))
	{

		if(s_aiCounter[iIdx] > 3)
		{
			char	strOut[512];
			g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[iIdx] = g_pGcData->RunInfo.fSysLoad;
			GC_SetEnumValue(TYPE_LVD_UNIT, 
						iLvdEquipNo,
						LVD1_PRI_CTL + iLvdNo, 
						GC_DISCONNECTED, 
						TRUE);			

			g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx]
				= g_pGcData->RunInfo.Rt.bAcFail;
			s_aiCounter[iIdx] = 0;

			sprintf(strOut,
				"LVD%d disconnect for voltage, AC state is %s. DC voltage is %.2f.\n",
				iIdx + 1,
				(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
				g_pGcData->RunInfo.fSysVolt);
			GC_LOG_OUT(APP_LOG_INFO, strOut);
		}
		else
		{
			s_aiCounter[iIdx]++;
		}

		return;
	}
	
	s_aiCounter[iIdx] = 0;

	if(GC_BY_VOLT 
		== GC_IsGCEquipExist(TYPE_LVD_UNIT, 
					iLvdEquipNo,
					LVD1_PRI_MODE + iLvdNo,GC_EQUIP_INVALID))
	//By Voltage
	{

		return;
	}
	else if(GC_EQUIP_INVALID 
		== GC_IsGCEquipExist(TYPE_LVD_UNIT, 
		iLvdEquipNo,
		LVD1_PRI_MODE + iLvdNo,GC_EQUIP_INVALID))
	{

		return;
	}
	
	//By Time
	
	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if (stVoltLevelState)//24V
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
			iLvdEquipNo,
			LVD1_PRI_RECON_VOLT24 + iLvdNo,
			&fReconnectVolt );
	}
	else
	{
		bRTN  = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
			iLvdEquipNo,
			LVD1_PRI_RECON_VOLT + iLvdNo,
			&fReconnectVolt);

	}



	if((SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_DISCH + iIdx))
		&&((bRTN==TRUE)&& (g_pGcData->RunInfo.fSysVolt < fReconnectVolt)))
	{
		char	strOut[512];
		g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[iIdx] = g_pGcData->RunInfo.fSysLoad;
		
		GC_SetEnumValue(TYPE_LVD_UNIT, 
						iLvdEquipNo,
						LVD1_PRI_CTL + iLvdNo, 
						GC_DISCONNECTED, 
						TRUE);	
		
		g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx] 
				= g_pGcData->RunInfo.Rt.bAcFail;

		sprintf(strOut,
				"LVD%d disconnect for discharge time, AC state is %s. DC voltage is %.2f.\n",
				iIdx + 1,
				(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
				g_pGcData->RunInfo.fSysVolt);

		GC_LOG_OUT(APP_LOG_INFO, strOut);

		return;
	}

	return;
}

/*==========================================================================*
 * FUNCTION : Disconnection_LargeDU
 * PURPOSE  : Handling in connection state
 * CALLS    : GC_GetEnumValue GC_SetSecTimer GC_GetFloatValue DxiSetData
              GC_GetStateOfSecTimer
 * CALLED BY: GC_LvdRun
 * ARGUMENTS: int  iIdx : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-03 16:07
 *==========================================================================*/
static void Disconnection_LargeDU(int iIdx)
{
	static int		s_aiCounter[MAX_NUM_LVD_EQUIP * 2] ;
	int				iDependency;
	int				iLvdEquipNo = iIdx / 2;
	int				iLvdNo = iIdx % 2;
	float			fLvdVolt;
	float			fReconnectVolt;
	int			i;
	BOOL			bConnected = FALSE,bRTN ;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);


	SIG_ENUM		enumState;
	
	GC_SuspendSecTimer(SEC_TMR_ID_LVD1_RECONNECT + iIdx);

	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);	
	
	
	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{		
		if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + iLvdNo])
		{
			if(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + iLvdNo])
			{
				bConnected = TRUE;
				break;	
			}			
		}		
	}
	if(!bConnected)
	{
		return;
	}



	if((enumState == ST_SHORT_TEST)
		|| (enumState == ST_MAN_TEST)
		|| (enumState == ST_PLAN_TEST))
	{
		s_aiCounter[iIdx] = 0;
		GC_SuspendSecTimer(SEC_TMR_ID_LVD1_DISCH + iIdx);
		return;
	}

	if(GC_DISABLED 
		== GC_IsGCEquipExist(TYPE_OTHER, 
						0,
						LDU_LVD1_PRI_ENB + iLvdNo,GC_DISABLED))
	{
		s_aiCounter[iIdx] = 0;
		return;
	}
	
	if(SEC_TMR_TIME_IN 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_OF_LVD))  // ����Ԥ�������ܵ���ģ���������������ظ����µ����⣬����Ԥ����ʱ���µ�
	{
		s_aiCounter[iIdx] = 0;
		g_pGcData->RunInfo.bLVDPreLmtToCurrLmtFlag = TRUE;
		g_pGcData->RunInfo.fSysLoadofPreCurrLimit = g_pGcData->RunInfo.fSysLoad;
		return;
	}
/* Changed by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
	if(!GC_IsDislPwrLmtMode() && GC_DISABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_ENABLE))
	{
		if((!(g_pGcData->RunInfo.Rt.bAcFail))
			&& (GC_ENABLED == GC_IsGCEquipExist(TYPE_OTHER, 0, LVD_NEED_AC_FAIL,GC_EQUIP_INVALID)))
		{
			s_aiCounter[iIdx] = 0;
			return;
		}
		
		/*Start Modified by Marco Yang, Don't consider the current condition, 2010-4-5*/
		//Discharge and AC failure check
		//if((!g_pGcData->RunInfo.Bt.bDischarge) 
		//	&& (!g_pGcData->RunInfo.Rt.bAcFail))
		//not discharge and not AC failure
		//{
		//	s_aiCounter[iIdx] = 0;
		//	return;
		//}
		/*End Modified by Marco Yang, Don't consider the current condition, 2010-4-5*/

		if((GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_SET))
			&& (g_pGcData->RunInfo.Rt.fCurrLmt < 100.0)
			&& (!GC_IsPowerLmt()))
		//Rectifer current limit not relax
		{
			s_aiCounter[iIdx] = 0;
			return;
		}
	}
/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */

	// dependency check
	// 0 indicates No Dependedcy.	
	iDependency = GC_IsGCEquipExist(TYPE_OTHER, 
							0,
							LDU_LVD1_PRI_DEPEND  + iLvdNo,GC_DISABLED );
	if(iDependency) 
	{
		if((iLvdNo != (iDependency - 1))	//depend on itself
			&& (g_pGcData->RunInfo.Lvd.abValid[iLvdEquipNo * 2 + iDependency - 1])
			&& (GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[iLvdEquipNo * 2 + iDependency - 1]))
		{
			s_aiCounter[iIdx] = 0;
			return;
		}
	}

	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
			0,
			LDU_LVD1_PRI_VOLT24  + iLvdNo,
			&fLvdVolt );
	}
	else
	{
		bRTN  = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
			0,
			LDU_LVD1_PRI_VOLT  + iLvdNo,
			&fLvdVolt);

	}


	if((bRTN == TRUE)&&(g_pGcData->RunInfo.fSysVolt < fLvdVolt))
	{
		if(s_aiCounter[iIdx] > 3)
		{
			char	strOut[512];
			for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
			{		
				if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + iLvdNo])
				{
					if(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + iLvdNo])
					{
						g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i * 2 + iLvdNo] = TRUE;
						g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[i * 2 + iLvdNo] = g_pGcData->RunInfo.fSysLoad;
						GC_SetEnumValue(TYPE_LVD_UNIT, 
							i,
							LVD1_PRI_CTL + iLvdNo, 
							GC_DISCONNECTED, 
							TRUE);	
						
					}			
				}		
			}

			


			g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx]
				= g_pGcData->RunInfo.Rt.bAcFail;
			s_aiCounter[iIdx] = 0;

			sprintf(strOut,
				"LVD%d disconnect for voltage, AC state is %s. DC voltage is %.2f.\n",
				iIdx + 1,
				(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
				g_pGcData->RunInfo.fSysVolt);
			GC_LOG_OUT(APP_LOG_INFO, strOut);
		}
		else
		{
			s_aiCounter[iIdx]++;
		}
		return;
	}
	
	s_aiCounter[iIdx] = 0;

	if(GC_BY_VOLT 
		== GC_IsGCEquipExist(TYPE_OTHER, 
						0,
						LDU_LVD1_PRI_MODE  + iLvdNo,GC_EQUIP_INVALID))
	//By Voltage
	{
		return;
	}
	else if(GC_EQUIP_INVALID 
		==  GC_IsGCEquipExist(TYPE_OTHER, 
		0,
		LDU_LVD1_PRI_MODE  + iLvdNo,GC_EQUIP_INVALID))
	{
		return;
	}
	
	//By Time
	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
			0,
			LDU_LVD1_PRI_RECON_VOLT24  + iLvdNo,
			&fReconnectVolt );
	}
	else
	{
		bRTN  = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
			0,
			LDU_LVD1_PRI_RECON_VOLT + iLvdNo,
			&fReconnectVolt);

	}
	
	if((SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_DISCH + iLvdNo))
		&& ((bRTN== TRUE)&&(g_pGcData->RunInfo.fSysVolt < fReconnectVolt)))
	{
		char	strOut[512];

		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{		
			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + iLvdNo])
			{
				if(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + iLvdNo])
				{
					g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i * 2 + iLvdNo] = TRUE;
					g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[i * 2 + iLvdNo] = g_pGcData->RunInfo.fSysLoad;
					GC_SetEnumValue(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_CTL + iLvdNo, 
						GC_DISCONNECTED, 
						TRUE);	
					break;	
				}			
			}		
		}				
		
		g_pGcData->RunInfo.Lvd.abLastAcFail[iIdx] 
				= g_pGcData->RunInfo.Rt.bAcFail;

		sprintf(strOut,
				"LVD%d disconnect for discharge time, AC state is %s. DC voltage is %.2f.\n",
				iIdx + 1,
				(g_pGcData->RunInfo.Rt.bAcFail ? "failure" : "normal"),
				g_pGcData->RunInfo.fSysVolt);

		GC_LOG_OUT(APP_LOG_INFO, strOut);
		return;
	}
	return;
}
/*==========================================================================*
 * FUNCTION : ConnectAll
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-09-25 14:54
 *==========================================================================*/
static void ConnectAll(void)
{

	int	i;
	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{
		if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0] && GC_DISABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, 
					i,
					LVD1_PRI_ENB, GC_DISABLED))
		{
			g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0] 
				= GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD1_PRI_CTL,GC_EQUIP_INVALID);

			GC_SuspendSecTimer(SEC_TMR_ID_LVD1_DISCH + i * 2 + 0);
			GC_SetEnumValue(TYPE_LVD_UNIT, 
							i, 
							LVD1_PRI_CTL, 
							GC_CONNECTED, 
							FALSE);
		}
	
		if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1] && GC_DISABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, 
					i,
					LVD1_PRI_ENB + 1,GC_DISABLED))
		{
			g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1] 
				= GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD2_PRI_CTL,GC_EQUIP_INVALID);

			GC_SuspendSecTimer(SEC_TMR_ID_LVD1_DISCH + i * 2 + 1);
			GC_SetEnumValue(TYPE_LVD_UNIT, 
							i, 
							LVD2_PRI_CTL, 
							GC_CONNECTED, 
							FALSE);

		}
	}
	return;
}


#define		GC_LVD_MAX_TIMES	4
/*==========================================================================*
 * FUNCTION : GC_LvdRun
 * PURPOSE  : LVD interface function
 * CALLS    : GC_GetEnumValue GetLvdState Disconnection Reconnection
              GetLvdState
 * CALLED BY: GC_BattMgmt
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-03 14:46
 *==========================================================================*/
void GC_LvdRun(void)
{
	
	int				i;
	static	BOOL	s_bErrCurrLast = FALSE;
	static	BOOL	s_bHiTempDisconnected = FALSE;
	BOOL			bImbalProtEnb 
						= GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_IMBAL_PROT_ENB);

	g_pGcData->RunInfo.Lvd.bLVD3Enable = GC_GetEnumValue(TYPE_OTHER, 0, LVD3_ENABLE);
	if(g_pGcData->RunInfo.Bt.bErrCurr)
	{
		if(GC_ENABLED == bImbalProtEnb)
		{
			if(!s_bErrCurrLast)
			{
				GC_LOG_OUT(APP_LOG_WARNING, 
					"Abnormal load current! LVD function shall be disabled.\n");
			}
			ConnectAll();
			s_bErrCurrLast = g_pGcData->RunInfo.Bt.bErrCurr;
			return;

		}
		s_bErrCurrLast = g_pGcData->RunInfo.Bt.bErrCurr;
	}
	else
	{
		if(s_bErrCurrLast)
		{
			GC_LOG_OUT(APP_LOG_INFO, 
				"Abnormal load current end! LVD fuction shall run normally.\n");
		}

		s_bErrCurrLast = g_pGcData->RunInfo.Bt.bErrCurr;	
	}	

	//Discharge check
	if(!(g_pGcData->RunInfo.Bt.bDischarge))
	//not discharge
	{
		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{
			GC_SuspendSecTimer(SEC_TMR_ID_LVD1_DISCH + 2 * i + 0);
			GC_SuspendSecTimer(SEC_TMR_ID_LVD1_DISCH + 2 * i + 1);
		}	
	}
	else
	{
		//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
		if (GC_EQUIP_EXIST != GC_IsGCEquipExist(TYPE_OTHER, 0, LDU_PUB_EXISTENCE,GC_EQUIP_INVALID))
		//if(GC_EQUIP_EXIST != GC_GetEnumValue(TYPE_OTHER, 0, LDU_PUB_EXISTENCE))
		{
			for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
			{
				if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0]
					&& (SEC_TMR_SUSPENDED 
						== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_DISCH + i * 2 + 0)))
				{
					GC_SetSecTimer(SEC_TMR_ID_LVD1_DISCH + i * 2 + 0, 
								0,
								GC_GetLvdTime,
								i * 2 + 0);
				}

				if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1]
					&& (SEC_TMR_SUSPENDED 
						== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_DISCH + i * 2 + 1)))
				{

					GC_SetSecTimer(SEC_TMR_ID_LVD1_DISCH + i * 2 + 1, 
								0,
								GC_GetLvdTime,
								i * 2 + 1);
				}
			}
		}
		else// LargeDU mode ,there are only 2 timer.
		{
			if(SEC_TMR_SUSPENDED 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_DISCH + 0))
			{
				GC_SetSecTimer(SEC_TMR_ID_LVD1_DISCH + 0, 
							0,
							GC_GetLvdTime_LargeDU,
							0);
			}

			if(SEC_TMR_SUSPENDED 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_LVD1_DISCH + 1))
			{

				GC_SetSecTimer(SEC_TMR_ID_LVD1_DISCH + 1, 
							0,
							GC_GetLvdTime_LargeDU,
							1);
			}

		}
	}

	if( SEC_TMR_TIME_OUT 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_ESNA_LVD_SYNCH_INTERVAL) 
		&& GC_ENABLED == GC_IsGCEquipExist(TYPE_OTHER, 
		0,
		LVD_ESNA_LVD_SYNCH,
		GC_EQUIP_INVALID))
	{
	
		GC_SynchLVDBoard();
		GC_SuspendSecTimer(SEC_TMR_ID_ESNA_LVD_SYNCH_INTERVAL);
		//printf("Go to here----2\n");
	}


	if(STATE_MAN == g_pGcData->RunInfo.enumAutoMan)
	{
		return;
	}


	if (GC_EQUIP_EXIST !=GC_IsGCEquipExist(TYPE_OTHER, 0, LDU_PUB_EXISTENCE,GC_EQUIP_INVALID))  //G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	{
		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{
			
			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
			{

				if(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0])
				{
					g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i * 2 + 0] = FALSE;
					Disconnection(i * 2 + 0);
				}
				else
				{
					g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i * 2 + 0] = TRUE;
					Reconnection(i * 2 + 0);
				}
			}

			if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
			{
				if(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1])
				{
					g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i * 2 + 1] = FALSE;
					Disconnection(i * 2 + 1);
				}
				else
				{
					g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i * 2 + 1] = TRUE;
					Reconnection(i * 2 + 1);
				}
			}
		}		
	}
	else
	{
			//LargeDU mode ,only 2 level LVD. 
			Disconnection_LargeDU(0);
			Disconnection_LargeDU(1);
			
			Reconnection_LargeDU(0);
			Reconnection_LargeDU(1);
		
	}
	return;
}

/*==========================================================================*
 * FUNCTION : GC_LvdInit
 * PURPOSE  : Initialize the LVD run information
 * CALLS    : 
 * CALLED BY: GC_BattMgmtInit
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-03 16:03
 *==========================================================================*/
void GC_LvdInit()
{
	int		i;
	BOOL	*p_bNeedSave = g_pGcData->SecTimer.abNeedSave;
	for(i = 0; i < MAX_NUM_LVD_EQUIP * 2; i++)
	{
		g_pGcData->RunInfo.Lvd.abLastAcFail[i] = FALSE;

		g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i] = FALSE;
		g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[i] = 0;

		GC_SuspendSecTimer(SEC_TMR_ID_LVD1_RECONNECT + i);
		p_bNeedSave[SEC_TMR_ID_LVD1_RECONNECT + i] = FALSE;

		GC_SuspendSecTimer(SEC_TMR_ID_LVD1_DISCH + i);
		p_bNeedSave[SEC_TMR_ID_LVD1_DISCH + i] = FALSE;

	}
	g_pGcData->RunInfo.Lvd.bLVD3Enable = FALSE;
	
	return;
}


/*==========================================================================*
* FUNCTION : GC_SynchLVDBoard
* PURPOSE  : Synchronize with ESNA LVD Board after starting.
* CALLS    : 
* CALLED BY: GC_BattMgmtInit
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-11-03 16:03
*==========================================================================*/
static void GC_SynchLVDBoard()
{
	int i;
	float fReconnectVolt;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	//printf("LVD1 status :%d;LVD1 status :%d\n", g_pGcData->RunInfo.Lvd.abValid[0], g_pGcData->RunInfo.Lvd.abValid[1]);
	if((g_pGcData->RunInfo.Lvd.abValid[0]) && 
		GC_ENABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, 
		0,
		LVD1_PRI_ENB,GC_EQUIP_INVALID))
	{
		//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
		if(stVoltLevelState)
		{
			fReconnectVolt = GC_GetFloatValue(TYPE_LVD_UNIT, 
				0,
				LVD1_PRI_VOLT24 + 0);
		}
		else
		{
			fReconnectVolt = GC_GetFloatValue(TYPE_LVD_UNIT, 
				0,
				LVD1_PRI_VOLT + 0);

		}

		if(g_pGcData->RunInfo.fSysVolt < fReconnectVolt)
		{
			GC_SetEnumValue(TYPE_LVD_UNIT, 
				0,
				LVD1_PRI_CTL, 
				GC_DISCONNECTED, 
				TRUE);
			
			
		}
		else
		{
			GC_SetEnumValue(TYPE_LVD_UNIT, 
				0,
				LVD1_PRI_CTL, 
				GC_CONNECTED, 
				TRUE);
			
		}

			
	}
	if((g_pGcData->RunInfo.Lvd.abValid[1]) && 
		GC_ENABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, 
		0,
		LVD1_PRI_ENB + 1,
		GC_EQUIP_INVALID))
	{
		//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
		if(stVoltLevelState)//24V
		{
			fReconnectVolt = GC_GetFloatValue(TYPE_LVD_UNIT, 
				0,
				LVD1_PRI_VOLT24 + 1);
		}
		else
		{
			fReconnectVolt = GC_GetFloatValue(TYPE_LVD_UNIT, 
				0,
				LVD1_PRI_VOLT + 1);

		}

		if(g_pGcData->RunInfo.fSysVolt < fReconnectVolt)
		{
			GC_SetEnumValue(TYPE_LVD_UNIT, 
				0,
				LVD1_PRI_CTL + 1, 
				GC_DISCONNECTED, 
				TRUE);
			
		}
		else
		{
			GC_SetEnumValue(TYPE_LVD_UNIT, 
				0,
				LVD1_PRI_CTL + 1, 
				GC_CONNECTED, 
				TRUE);
			
		}
	}		
	
	
	return;
}
/*==========================================================================*
 * FUNCTION : GC_GetLvdTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdx : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-04-08 11:04
 *==========================================================================*/
int GC_GetLvdTime(int iIdx)
{
	int		iLvdEquipNo = iIdx / 2;
	int		iLvdNo = iIdx % 2;
	

	ASSERT((iLvdEquipNo >= 0) && (iLvdEquipNo < MAX_NUM_LVD_EQUIP));	
	ASSERT((iLvdNo >= 0) && (iLvdNo <= 1));	
	
	return SECONDS_PER_MIN 
		* (int)GC_GetDwordValue(TYPE_LVD_UNIT, 
								iLvdEquipNo, 
								LVD1_PRI_TIME + iLvdNo);
}

/*==========================================================================*
 * FUNCTION : GC_GetLvdTime_LargeDU
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdx : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-04-08 11:04
 *==========================================================================*/
static int GC_GetLvdTime_LargeDU(int iIdx)
{
	
	ASSERT((iIdx >= 0) && (iIdx <= 1));	
	
	return SECONDS_PER_MIN 
		* (int)GC_GetDwordValue(TYPE_OTHER, 
						0, 
						LDU_LVD1_GROUP_PRI_TIME + iIdx);
}


/*==========================================================================*
 * FUNCTION : GC_GetReconnectDelay
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdx : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-04-08 11:04
 *==========================================================================*/
int GC_GetReconnectDelay(int iIdx)
{
	int		iLvdEquipNo = iIdx / 2;
	int		iLvdNo = iIdx % 2;
	ASSERT((iLvdEquipNo >= 0) && (iLvdEquipNo < MAX_NUM_LVD_EQUIP));	
	ASSERT((iLvdNo >= 0) && (iLvdNo <= 1));	

	return SECONDS_PER_MIN 
		* (int)GC_GetDwordValue(TYPE_LVD_UNIT, 
								iLvdEquipNo, 
								LVD1_PRI_DELAY + iLvdNo);
}


/*==========================================================================*
 * FUNCTION : GC_GetReconnectDelay_LargeDU
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdx : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-04-08 11:04
 *==========================================================================*/
static int GC_GetReconnectDelay_LargeDU(int iIdx)
{
	
	int		iLvdNo = iIdx % 2;
	
	ASSERT((iLvdNo >= 0) && (iLvdNo <= 1));	

	return SECONDS_PER_MIN 
		* (int)GC_GetDwordValue(TYPE_OTHER, 
								0, 
								LDU_LVD1_GROUP_PRI_DELAY + iLvdNo);
}


/*==========================================================================*
 * FUNCTION : GC_GetHDTemperature
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdx : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-04-08 11:04
 *==========================================================================*/
static float GC_GetHDTemperature(void)
{
	//This function is using BTRM sensor instead of Comp Temp, 
	//Jimmy CR Temp Strategy, 2012-02-06
	float				fTempForBTRM;
	SIG_BASIC_VALUE*		pSigValue;
	int				iTempNoForBTRM;
	int				iRst;
	SIG_ENUM			enumTempUsage = 0;
	SIG_ENUM			enumTempNotConnectAlm = 0;
	SIG_ENUM			enumTempFaultAlm = 0;	
	BOOL				bBTRMsation = FALSE;	

	iTempNoForBTRM = GC_GetEnumValue(TYPE_OTHER,
								0,
								GC_PUB_NO_FOR_BTRM);
	//ASSERT(iTempNoForCompen >= 0 && iTempNoForCompen <= MAX_NUM_TEMPERATURE);
	//int iNotLduTempNum = MAX_NUM_TEMPERATURE - MAX_NUM_LargeDU_TEMP;
	
	//if((iTempNoForCompen > 0) && (iTempNoForCompen <= iNotLduTempNum))	//Not LargeDU Temp
	//{
	//	enumTempUsage = GC_GetEnumValue(TYPE_OTHER, 
	//						0,
	//						GC_PUB_TEMP1_USAGE + iTempNoForCompen - 1);

	//	enumTempNotConnectAlm = GC_GetEnumValue(TYPE_OTHER, 
	//						0,
	//						GC_PUB_TEMP1_NOT_CONNECT_ALM + iTempNoForCompen - 1);

	//	enumTempFaultAlm = GC_GetEnumValue(TYPE_OTHER, 
	//						0,
	//						GC_PUB_TEMP1_FAULT_ALM + iTempNoForCompen - 1);


	//	if((enumTempUsage != TEMP_USAGE_DISABLED)
	//		&& (enumTempNotConnectAlm == GC_NORMAL)
	//		&& (enumTempFaultAlm == GC_NORMAL))
	//	{
	//		bCompensation = TRUE;
	//	}
	//}
	//else if ((iTempNoForCompen > iNotLduTempNum) && (iTempNoForCompen <= MAX_NUM_TEMPERATURE)) //LargeDU temp
	//{
	//	int iLduTempNum = GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LduTEMP_NUM);
	//	if(iTempNoForCompen - iNotLduTempNum <= iLduTempNum)
	//	{
	//		bCompensation = TRUE;
	//	}
	//}

	if(iTempNoForBTRM == 0)
	{
		bBTRMsation = FALSE;
	}
	else
	{
		bBTRMsation = TRUE;
	}

	if(bBTRMsation)
	{

		iRst = GC_GetSigVal(TYPE_OTHER, 
						0, 
						GC_PUB_BATTGROUP_BTRM, 
						&pSigValue);
		GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"GC_GetSigVal GC_PUB_BATTGROUP_BATTTEMP error!\n");
		
		if(SIG_VALUE_IS_VALID(pSigValue))
		{
			fTempForBTRM = GC_GetFloatValue(TYPE_OTHER, 
									0, 
									GC_PUB_BATTGROUP_BTRM);			
		}
		else
		{
			fTempForBTRM = -273.0;
		}
			
	}
	else
	{
		fTempForBTRM = -273.0;
	}	
	return fTempForBTRM;	
}

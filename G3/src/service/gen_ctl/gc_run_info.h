/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_run_info.h
 *  CREATOR  : Frank Cao                DATE: 2004-10-20 16:04
 *  VERSION  : V1.00
 *  PURPOSE  : to provide run infomation handling interface
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_RUN_INFO_H_
#define _GC_RUN_INFO_H_

#include	"basetypes.h"
#include	"gc_init_equip.h"
//#include	"gc_test_log.h"
#include	"gc_batt_history.h"
#include	"gc_timer.h"
#include	"gc_main_switch.h"
#include	"gc_index_def.h"
#include	"gc_rect_list.h"


#define MIN_CURR_COEF			0.005
//#define	MAX_NUM_RECT_COMM_BREAK	4

#define	GC_MAX_BATT_TEMP		150.0
#define	GC_MIN_BATT_TEMP		(-100.0)

#define GC_MAX_DC_VOLT			60.0
#define GC_MIN_DC_VOLT			17.0

#define	GC_INVALID_SIG			(-1)

#define	GC_EQUIP_EXIST			0
#define	GC_EQUIP_INEXIST		1
#define	GC_EQUIP_INVALID		2

#define	GC_BT_USED_BY_BM		0
#define	GC_BT_NOUSED_BY_BM		1
#define	GC_BT_INVALIAD			2


#define GC_MPPT_DISABLE			0
#define GC_MPPT_MPPT_RECT		1
#define GC_MPPT_MPPT			2
#define GC_RECT_FIRST			0
#define GC_MPPT_FIRST			1
#define GC_CALCULATE_CAPACITY_DELTA 		1.075 //Add /1.075 to avoid the value > 100, 1.075 provide by Alfred


#define	GC_BCL_ALARM_CURR_MODE		(0.95)
#define	GC_BCL_NORMAL_CURR_MODE		(0.90)

struct	tagGC_FLASH_DATA
{
	SIG_ENUM		enumBattMgmtState;

	GC_SINGLE_SEC_TIMER	tmrShortTestInterval;
	GC_SINGLE_SEC_TIMER	tmrCyclicBcDuration;
	GC_SINGLE_SEC_TIMER	tmrCyclicBcInterval;
	
	//Batteries Capcity
	float			afBattCap[MAX_NUM_BATT_EQUIP];

	time_t			tmLastDslTest;
	time_t			tmLastPlanTest;

	//Batteries History
	DWORD			dwShallowTime;
	DWORD			dwShallowTimes;
	DWORD			dwMidTime;
	DWORD			dwMidTimes;
	DWORD			dwDeepTime;
	DWORD			dwDeepTimes;
	DWORD			adwHighTempTime[MAX_NUM_BATT_EQUIP];
	DWORD			adwLowTempTime[MAX_NUM_BATT_EQUIP];

};
typedef struct tagGC_FLASH_DATA GC_FLASH_DATA;


struct	tagGC_SLAVE_INFO_RECT
{
	int			iQtyOfRect;
	BOOL		abComm[MAX_NUM_RECT_EQUIP];
	float		fRatedCurr;
	float		fRatedCurrA[MAX_NUM_RECT_EQUIP]; //Support for mixed rectifier.
};
typedef struct tagGC_SLAVE_INFO_RECT GC_RUN_INFO_SLAVE;


struct	tagGC_RUN_INFO_RECT
{
	//If SCU communication interrupt, all value of rectifiers shall invalid
	BOOL				bValid;
	//All of rectifiers AC failure
	BOOL				bAcFail;
	BOOL				bMasterAcFail;
	//There is rectifier that communication interrupts
	BOOL				bNoRespond;
	BOOL				bMasterNoRespond;
	int				iQtyOfRect;


	float				fSumOfCurr;
	float				fVolt;
	float				fCurrLmt;
	float				fMaxCurrLmtPoint;
	
	HANDLE				hListOnWorkRect;
	HANDLE				hListOffWorkRect;
	int				iQtyOnWorkRect;
	int				iQtyOffWorkRect;
	//Qty Of rectfiers which are communicating, not AC-fail but AC relay is OFF
	int				iQtyAcOffRect; 
	int				iQtyNoOutputRect;
	float				fRatedVolt;

	//The total rectifier rated current which rectifiers are on
	float				fSumOnRatedCurrent;
	float				fRatedCurr;

	BOOL			bRectHVSD;		//If bRectHVSD, Can not open all rect directly.

	GC_RUN_INFO_SLAVE	Slave[MAX_RECT_TYPE_NUM]; //include the master
};
typedef struct tagGC_RUN_INFO_RECT GC_RUN_INFO_RECT;

struct	tagGC_CONV_INFO_CONV
{
	//There is rectifier that communication interrupts
	BOOL				bNoRespond;
	int				iQtyOfRect;
	float				fSumOfCurr;

	int				iQtyOnWorkRect;
	int				iQtyOffWorkRect;
	//Qty Of converters which are communicating, not AC-fail but AC relay is OFF
	int				iQtyAcOffRect; 
	int				iQtyNoOutputRect;
	float				fAverVoltage;
	float				fSumOnRatedCurrent;

	
};
typedef struct tagGC_CONV_INFO_CONV GC_CONV_INFO_CONV;


#ifdef GC_SUPPORT_MPPT
struct	tagGC_RUN_INFO_MPPT
{
	//If SCU communication interrupt, all value of rectifiers shall invalid
	BOOL				bValid;
	//All of rectifiers AC failure

	//There is rectifier that communication interrupts
	BOOL				bNoRespond;
	int				iQtyOfRect;

	float				fSumOfCurr;
	float				fVolt;
	float				fCurrLmt;
	float				fMaxCurrLmtPoint;

	HANDLE				hListOnWorkRect;
	HANDLE				hListOffWorkRect;
	int				iQtyOnWorkRect;
	int				iQtyOffWorkRect;
	//Qty Of rectfiers which are communicating, not AC-fail but AC relay is OFF
	int				iQtyNoOutputRect;
	int				iQtyDcOffRect;
	float				fRatedVolt;

	//The total rectifier rated current which rectifiers are on
	float				fSumOnRatedCurrent;
	float				fRatedCurr;
	BOOL				abComm[MAX_NUM_MPPT_EQUIP];
	BOOL				bNight;
};
typedef struct tagGC_RUN_INFO_MPPT GC_RUN_INFO_MPPT;
#endif

struct	tagGC_SIG_RUN_INFO
{
	BOOL		bValid;
	VAR_VALUE	Value;
};
typedef struct tagGC_SIG_RUN_INFO GC_SIG_RUN_INFO;


struct	tagGC_RUN_INFO_BATT
{
	//This defines the type : Li or Traditional
	//added by Jimmy 2012/04/05
	int			iSysBattType; // 0 = Traditional, 1 = Li Batt
	BOOL			bIsA_LiBatt[MAX_NUM_BATT_EQUIP]; //TRUE = Li Batt, FALSE = Traditional Batt
	float			fAvgTemp; //�����洢Li��ص�ƽ���¶ȣ���ʱ�ò��ϣ�������

	int			iBattNum;
	BOOL			bErrCurr;
	BOOL			abUsedByBattMgmt[MAX_NUM_BATT_EQUIP];
	BOOL			abValid[MAX_NUM_BATT_EQUIP];
	//GC_SIG_RUN_INFO	GroupTemp;
	int				iQtyValidBatt;
	BOOL			bDischarge;
	BOOL			bZeroCurr;
	BOOL			bOverFc2BcCurr;
	TEST_LOG		TestLog;
#ifdef	_GEN_CTL_FOR_CHN
	float			afDisCap[MAX_NUM_BATT_EQUIP];
	float			fTotalDisCap;
#endif
	float			afBattCurr[MAX_NUM_BATT_EQUIP];
	float			fSumOfCurr;
	float			fCurrLmtSetPoint;
	float			fExpBattCurr1;				//for Lower Consumption
	float			fExpBattCurr2;				//for Limit Max Power
	int				iValidVoltNum;
	float			fVolt;
	DWORD			dwBattTypeNo;
	BOOL			bNeedRegulateCap;
	float			afBattCap[MAX_NUM_BATT_EQUIP];
	float			afBattRatedCap[MAX_NUM_BATT_EQUIP];
	DWORD			dwBcProtectCounter;
	//For Battery History
	STATE_RECORD	DischState;	
	STATE_RECORD	aTempState[MAX_NUM_BATT_EQUIP];
	DWORD			dwShallowTime;
	DWORD			dwShallowTimes;
	DWORD			dwMidTime;
	DWORD			dwMidTimes;
	DWORD			dwDeepTime;
	DWORD			dwDeepTimes;
	DWORD			adwHighTempTime[MAX_NUM_BATT_EQUIP];
	DWORD			adwLowTempTime[MAX_NUM_BATT_EQUIP];

	time_t			tmLastPlanTest;
	int			iCmpCapDelay;
	int			iVoltAdjustState;
	int			iEquipTypeOfBatt[MAX_NUM_BATT_EQUIP];
	int			iBattCurrlimitFlag;    //��ѹģʽ��BCL�澯��־
	
	GC_FLASH_DATA	FlashData;
};
typedef struct tagGC_RUN_INFO_BATT GC_RUN_INFO_BATT;

struct	tagGC_RUN_INFO_BATTPREDICT
{
	float BattCurrMin;
	float BattCurrMax;
	float BusVoltLast;
	float NextVolt;
	float LVDVolt;
	float TimetoNextVolt;
	float TimetoLVDVolt;
	float TimeCurveLast;
	float PredictSlop;
	float PredictDivision;
	float LVDPredictDivision;
	float PredictCR;
	float AdjustCoeff;
};
typedef struct tagGC_RUN_INFO_BATTPREDICT GC_RUN_INFO_BATTPREDICT;

struct	tagGC_RUN_INFO_LVD
{
	BOOL		abValid[MAX_NUM_LVD_EQUIP * 2];
	SIG_ENUM	aenumState[MAX_NUM_LVD_EQUIP * 2];
	BOOL		abLastAcFail[MAX_NUM_LVD_EQUIP * 2];
	BOOL		bLoadLVDFlag[MAX_NUM_LVD_EQUIP * 2];
	float		fLoadCurrBeforeLVD[MAX_NUM_LVD_EQUIP * 2];
	BOOL		bLVD3Enable;

};
typedef struct tagGC_RUN_INFO_LVD GC_RUN_INFO_LVD;


struct	tagGC_DSL_TEST_INFO
{
	time_t		tStartTime;
	time_t		tEndTime;
	SIG_ENUM	enumStartReason;
	SIG_ENUM	enumTestResult;
};
typedef struct tagGC_DSL_TEST_INFO GC_DSL_TEST_INFO;


/*struct	tagGC_RUN_INFO_AC
{
	BOOL				bPowerValid;
	BOOL				bDslValid;
	BOOL				bDslState;
	float				fTotalPower;
	BOOL				bLastDslState;
	BOOL				bLastDslTestState;
	GC_DSL_TEST_INFO	DslTestInfo;
	time_t				tmLastDslTest;
	int					iAcNum;
};
typedef struct tagGC_RUN_INFO_AC GC_RUN_INFO_AC;*/


struct	tagGC_RUN_INFO_MS
{
	BOOL				bBlockCondition;
	BOOL				bInputValid;
	BOOL				bDoorClose;
	BOOL				bSwitchOpen;
	BOOL				bAuto;
	BOOL				bRelayTrip;
	time_t				tmCloseTime[GC_MAIN_SWITCH_RECORD_NUM];
	int					iHead;
	int					iLength;
};
typedef struct tagGC_RUN_INFO_MS GC_RUN_INFO_MS;

struct	tagGC_RUN_INFO_HYBRID
{
	int				iLastRunDG;
	int				iCurrentRunningDG;
	int				iLastCloseDG;
	BOOL				bDieselFailure[2];

	int				iLastGridOnState;

	time_t				tmLastDischargeTime;
	time_t				tmLastEqualisingTime;
};
typedef struct tagGC_RUN_INFO_HYBRID GC_RUN_INFO_HYBRID;

#ifdef	_GC_TEST
struct _GC_SET_RESULT
{
	int				iEquipType;
	int				iEquipIdx;
	int				iSigIdx;
	VAR_VALUE_EX	SigVal;
	int				iResult;
};
typedef struct _GC_SET_RESULT GC_SET_RESULT;
#endif


struct _GC_ESTOP_ESHUTDOWN
{
	SIG_ENUM			enumEstopEshutdownParam;
	SIG_ENUM			enumEstopEshutdownInput;
	SIG_ENUM			enumEStopState;
#ifdef NOT_CARE_RECT_ESTOP_STATE
	SIG_ENUM			enumRectEStopState;
#endif
	//SIG_ENUM			enumConvertEStopState;
};

typedef struct _GC_ESTOP_ESHUTDOWN GC_ESTOP_ESHUTDOWN;



struct	tagGC_RUN_INFO_AC
{
	BOOL				bPowerValid;
	BOOL				bDslValid;
	BOOL				bDslState;
	float				fTotalPower;
	BOOL				bLastDslState;
	BOOL				bLastDslTestState;
	GC_DSL_TEST_INFO	DslTestInfo;
	time_t				tmLastDslTest;
};
typedef struct tagGC_RUN_INFO_AC GC_RUN_INFO_AC;


struct _GC_RUN_INFO
{
	SIG_ENUM			enumAutoMan;
	float				fSysVolt;
	float				fLastVolt;
	float				fSysLoad;
	float				fSysLoadofPreCurrLimit;
	BOOL				bLVDPreLmtToCurrLmtFlag;
	BOOL				bPreLmtForAcFail;
	//BOOL				bSlaveMode;
	GC_RUN_INFO_RECT	Rt;
	GC_RUN_INFO_BATT	Bt;
	GC_RUN_INFO_LVD		Lvd;
//	GC_RUN_INFO_DC		Dc;
	GC_RUN_INFO_AC		Ac;
	GC_CONV_INFO_CONV	Conv;
#ifdef GC_SUPPORT_MPPT
	GC_RUN_INFO_MPPT	Mt;
#endif


	GC_ESTOP_ESHUTDOWN	EStopEshutdow;
	GC_RUN_INFO_MS		Ms;

	GC_RUN_INFO_HYBRID	HyBrid;

	GC_RUN_INFO_BATTPREDICT BtPredict;

#ifdef	_GC_TEST
	GC_SET_RESULT		SetResult;
#endif
};

typedef struct _GC_RUN_INFO GC_RUN_INFO;

void	GC_RunInfoRefresh(void);
BOOL	GC_IsRectComm(int iRectType, int iRectNo);
float	GC_GetSysVolt(void);
float	GC_GetRectValidCapacity(int iRectType, int iRectNo);
BOOL	InitRectTime(void);
BOOL	PrintRectTime(void);
BOOL IsMultiRectFail(void);
BOOL IsRectFail(void);
BOOL IsWorkIn_LiBattMode(void);
void GC_SendCTCurrentLimit(IN float fCurrentLimit);
#endif //_GC_RUN_INFO_H_


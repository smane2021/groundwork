/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_picket.c
 *  CREATOR  : Frank Cao                DATE: 2004-12-02 15:00
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include	"gc_index_def.h"

#ifdef _GC_TEST	//
#include	"gc_picket.h"
#include	"../../mainapp/equip_mon/equip_mon.h"

static BOOL GcTestEquipSignal(IN SITE_INFO *pSite, IN char *pArgs);
static BOOL Site_DisplaySignals(IN SITE_INFO *pSite);
static BOOL Equip_DisplaySignals(IN EQUIP_INFO *pEquip);
static BOOL Equip_DisplayAlarmSignals(IN EQUIP_INFO *pEquip);
static BOOL Equip_DisplayControlSignals(IN EQUIP_INFO *pEquip);
static BOOL Equip_DisplaySettingSignals(IN EQUIP_INFO *pEquip);
static BOOL Equip_DisplayAlarmSignals(IN EQUIP_INFO *pEquip);
static BOOL Equip_DisplaySamplingSignals(IN EQUIP_INFO *pEquip);
static int ReadPicketFile(const char *szFileName, char **a_strInput);
extern SITE_INFO  g_SiteInfo;

BOOL GC_Picket(void)
{
	int		i;
	char	*szFileName = "picket.txt";
	char	*a_strInput[256]; 

	ReadPicketFile(szFileName, a_strInput);

	i = 0;
	while(a_strInput[i])
	{
		if(!GcTestEquipSignal(&g_SiteInfo, a_strInput[i]))
		{
			printf("***** %d: %s ***** \n", i, a_strInput[i]);
			return FALSE;
		}
		DELETE(a_strInput[i]);
		i++;
	}

	return TRUE;
}

//BOOL GC_Picket2(void)
//{
//	int		i;
//	char*	a_strInput[] = 
//	{
//		"2 2 1 48	Rated Voltage",
//		"2 2 2 50	Rated Current",
//		"3 0 1 0	Rectifier on/off",
//		"2 0 8 7	Number of rectifiers",
//		"3 0 2 53.5	Voltage",
//		"3 0 3 38	Origin Current",
//		"3 0 8 4001	Rectifier ID",
//		"3 0 10 0	Communication Interrupt count",
//		"4 0 2 53.3	Voltage",
//		"4 0 3 40	Origin Current",
//		"4 0 8 4002	Rectifier ID",
//		"5 0 2 53.6	Voltage",
//		"5 0 3 32	Origin Current",
//		"5 0 8 4003	Rectifier ID",
//		"5 0 10 1	Communication Interrupt count",
//		"6 0 2 53.2	Voltage",
//		"6 0 3 40	Origin Current",
//		"6 0 8 4004	Rectifier ID",
//		"7 0 2 53.2	Voltage",
//		"7 0 3 10	Origin Current",
//		"7 0 8 4005	Rectifier ID",
//		"8 0 2 53.2	Voltage",
//		"8 0 3 40	Origin Current",
//		"8 0 8 4006	Rectifier ID",
//		"9 0 2 53.2	Voltage",
//		"9 0 3 40	Origin Current",
//		"9 0 8 4007	Rectifier ID",
//
//		"27 2 20 2	BC to FC Delay",
//		"28 0 1 9	Battery Current",
//		//"28 0 2 900	Battery Capacity",
//		//"29 0 1 50	Battery Current",
//		//"29 0 2 800	Battery Capacity",
//		"30 0 1 9	Battery Current",
//		//"30 0 2 900	Battery Capacity",
//		"30 0 3 53.7	Voltage",
//		0,
//	};
//
//
//	i = 0;
//	while(a_strInput[i])
//	{
//		if(!GcTestEquipSignal(&g_SiteInfo, a_strInput[i]))
//		{
//			printf("***** %d: %s ***** \n", i, a_strInput[i]);
//			return FALSE;
//		}
//		i++;
//	}
//
//	return TRUE;
//}
//



static BOOL GcTestEquipSignal(IN SITE_INFO *pSite,
							  IN char *pArgs)
{
	EQUIP_INFO *pEquip;
	int			nEquipId;
	int			nSigType;
	int			nSigId;
	SAMPLE_SIG_VALUE *pSig;

	float		fNewValue = 0;
	int			nArgs;

	nArgs = sscanf(pArgs, "%d %d %d %f", 
		&nEquipId, &nSigType, &nSigId, &fNewValue);

	if (nArgs < 3)
	{
		printf( "--->Invalid args: %s\n", pArgs);
		return FALSE;
	}

	if (nEquipId < 0)	// show all equipment of the site
	{
		Site_DisplaySignals(pSite);
		return TRUE;
	}

	pEquip = Init_GetEquipRefById(pSite, nEquipId);
	if (pEquip == NULL)
	{
		printf( "--->Invalid equipment ID: %d\n", nEquipId);
		return FALSE;
	}

	if (nSigType < 0)	// show all types of the equipment
	{
		Equip_DisplaySignals(pEquip);
		return TRUE;
	}


	if (nSigId < 0)	// show all signal of the type
	{
		switch (nSigType)
		{
		case SIG_TYPE_SAMPLING:
			return Equip_DisplaySamplingSignals(pEquip);

		case SIG_TYPE_CONTROL:
			return Equip_DisplayControlSignals(pEquip);

		case SIG_TYPE_SETTING:
			return Equip_DisplaySettingSignals(pEquip);

		case SIG_TYPE_ALARM:
			return Equip_DisplayAlarmSignals(pEquip);
		}
		
		printf( "--->No such signal type %d.\n", nSigType);
		return FALSE;
	}

	// show one signal
	pSig = (SAMPLE_SIG_VALUE*)Init_GetEquipSigRefById(pEquip, nSigType, nSigId);
	if (pSig == NULL)
	{
		printf( "--->No such signal type %d or ID %d in equipment %s(%d).\n",
			nSigType, nSigId,
			EQP_GET_EQUIP_NAME0(pEquip),
			pEquip->iEquipID);
		return FALSE;
	}

	printf("Equipment %s(%d) displaying signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID);

	if(nSigType == SIG_TYPE_ALARM)
	{
		/*printf("%d: Alarm name: %s\n",
			(pSig)->pStdSig->iSigID,
			EQP_GET_SIGV_NAME0((pSig)));*/

		(pSig)->bv.varValue.lValue = (long)fNewValue;

		SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);

		return TRUE;
	}

	if (nArgs == 3)
	{
		if ((pSig)->bv.ucType == VAR_FLOAT)
		{
			printf("ID=%4d  *** %s ***  %c= %1.3f(Raw=%1.3f) %-8s\n",
				(pSig)->pStdSig->iSigID, 
				EQP_GET_SIGV_NAME0((pSig)),
				SIG_VALUE_IS_VALID(pSig) ? ' ' : 'x',
				(pSig)->bv.varValue.fValue,
				(pSig)->bv.fRawData,
				(pSig)->pStdSig->szSigUnit);
		}
		else
		{
			printf("ID=%4d *** %s ***%c= %ld(Raw=%1.3f) %-8s\n",
				(pSig)->pStdSig->iSigID, 
				EQP_GET_SIGV_NAME0((pSig)),
				SIG_VALUE_IS_VALID(pSig) ? ' ' : 'x',
				(pSig)->bv.varValue.lValue,
				(pSig)->bv.fRawData,
				(pSig)->pStdSig->szSigUnit);
		}
	}
	else if (nArgs == 4)
	{
		EQUIP_PROCESS_CMD	cmd;

		if ((pSig)->bv.ucType == VAR_FLOAT)
		{
			/*printf("***Set \"%s\" %f *** ID=%4d  %c= %1.3f(Raw=%1.3f) %-8s\n",
				EQP_GET_SIGV_NAME0((pSig)),
				fNewValue,
				(pSig)->pStdSig->iSigID, 
				SIG_VALUE_IS_VALID(pSig) ? ' ' : 'x',
				(pSig)->bv.varValue.fValue,
				(pSig)->bv.fRawData,
				(pSig)->pStdSig->szSigUnit);*/

				(pSig)->bv.varValue.fValue = fNewValue;
		}
		else
		{
			/*printf("***Set \"%s\" %d *** ID=%4d  %c= %ld(Raw=%1.3f) %-8s\n",
				EQP_GET_SIGV_NAME0((pSig)),
				(int)fNewValue,
				(pSig)->pStdSig->iSigID, 
				SIG_VALUE_IS_VALID(pSig) ? ' ' : 'x',
				(pSig)->bv.varValue.lValue,
				(pSig)->bv.fRawData,
				(pSig)->pStdSig->szSigUnit);*/

				(pSig)->bv.varValue.lValue = (int)fNewValue;
		}

		// set sig is valid
		SIG_STATE_SET(pSig, VALUE_STATE_IS_VALID);
		TRACE( "Set signal value to %1.3f.\n", fNewValue);

		// 2. notify equipment manager to process data
		// to notify the data processor
		cmd.nCmd   = PROCESS_CMD_SIG_DATA_CHANGED;
		cmd.pEquip = pEquip;
		Queue_Put(pSite->hqProcessCommand, &cmd, FALSE);
	}

	return TRUE;
}

#define DISPLAY_SIG_A(i, pSig)											\
	if ((pSig)->bv.ucType == VAR_FLOAT)									\
	{																	\
		printf("[%4d]: ID=%4d(SID=%2d,CH=%2d) %-32s%c= %1.3f(Raw=%1.3f) %-8s\n",\
			(i)+1, (pSig)->pStdSig->iSigID,								\
			pSig->sv.iSamplerID, pSig->sv.iSamplerChannel,				\
			EQP_GET_SIGV_NAME0((pSig)),									\
			SIG_VALUE_IS_VALID((pSig)) ? ' ' : 'x',						\
			(pSig)->bv.varValue.fValue,									\
			(pSig)->bv.fRawData,										\
			(pSig)->pStdSig->szSigUnit);								\
	}																	\
	else																\
	{																	\
		printf("[%4d]: ID=%4d(SID=%2d,CH=%2d) %-32s%c= %ld(Raw=%1.3f) %-8s\n",\
			(i)+1, (pSig)->pStdSig->iSigID,								\
			pSig->sv.iSamplerID, pSig->sv.iSamplerChannel,				\
			EQP_GET_SIGV_NAME0((pSig)),									\
			SIG_VALUE_IS_VALID((pSig)) ? ' ' : 'x',						\
			(pSig)->bv.varValue.lValue,									\
			(pSig)->bv.fRawData,										\
			(pSig)->pStdSig->szSigUnit);								\
	}


#define DISPLAY_ALL_SIG(i, nSig, pSig)				\
	for ((i) = 0; (i) < (nSig); (i)++, (pSig)++)	\
	{												\
		DISPLAY_SIG_A((i), (pSig));					\
	}


static BOOL Equip_DisplaySamplingSignals(IN EQUIP_INFO *pEquip)
{
	int					i;
	int					nSig  = pEquip->pStdEquip->iSampleSigNum;
	SAMPLE_SIG_VALUE	*pSig = pEquip->pSampleSigValue;

	printf("\n\n%s(%d) has %d sampling signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		nSig);

	DISPLAY_ALL_SIG(i, nSig, pSig);

	return TRUE;
}


static BOOL Equip_DisplaySettingSignals(IN EQUIP_INFO *pEquip)
{
	int					i;
	int					nSig  = pEquip->pStdEquip->iSetSigNum;
	SET_SIG_VALUE		*pSig = pEquip->pSetSigValue;

	printf("\n\n%s(%d) has %d setting signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		nSig);

	DISPLAY_ALL_SIG(i, nSig, pSig);

	return TRUE;
}


static BOOL Equip_DisplayControlSignals(IN EQUIP_INFO *pEquip)
{
	int					i;
	int					nSig  = pEquip->pStdEquip->iCtrlSigNum;
	CTRL_SIG_VALUE		*pSig = pEquip->pCtrlSigValue;

	printf("\n\n%s(%d) has %d control signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		nSig);

	DISPLAY_ALL_SIG(i, nSig, pSig);

	return TRUE;
}


static BOOL Equip_DisplayAlarmSignals(IN EQUIP_INFO *pEquip)
{
	int nLevel, i;
	ALARM_SIG_VALUE		*pSig, *pHead;
	char	strTime[32];

	printf("\n\n%s(%d) has %d active alarms:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		pEquip->iAlarmCount[0]);
	
	// lock the equipment, must get the lock.
	if (Mutex_Lock(pEquip->hSyncLock, MAX_TIME_WAITING_EQUIP_LOCK) == ERR_MUTEX_OK)
	{			
		for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
		{
			pHead = &pEquip->pActiveAlarms[nLevel];
			pSig  = pHead->next;

			printf("\nLevel %d has %d active alarms:\n",
				nLevel, pEquip->iAlarmCount[nLevel]);

			// the link is a bilink.
			for (i = 1; pSig != pHead; pSig = pSig->next, i++)
			{
				printf("EQUIP %4d: %-32s start at %s, %s.\n",
					i, EQP_GET_SIGV_NAME0((pSig)),
					TimeToString(pSig->sv.tmStartTime, TIME_CHN_FMT,
						strTime, sizeof(strTime)),
					SIG_VALUS_IS_SUPPRESSED(pSig) ? "suppressed" : "active");

			}
		}

		Mutex_Unlock(pEquip->hSyncLock);
	}

	else
	{
		printf( "Fails on trying to lock equipment.\n");
	}

	printf("\n\n%s(%d) has %d delaying alarms:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID,
		pEquip->nDelayingAlarms);

	pHead = &pEquip->pDelayingAlarms[0];
	pSig  = pHead->next;
	for (i = 1; pSig != pHead; pSig = pSig->next, i++)
	{
		printf("EQUIP %4d: %-32s is delaying, will active in %d seconds.\n",
			i, EQP_GET_SIGV_NAME0((pSig)),
			pSig->sv.nDelayingTime);
	}

	return TRUE;
}

static BOOL Equip_DisplaySignals(IN EQUIP_INFO *pEquip)
{
	printf("\n\nEquipment %s(%d) displaying signals:\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID);

	Equip_DisplaySamplingSignals(pEquip);
	Equip_DisplaySettingSignals(pEquip);
	Equip_DisplayControlSignals(pEquip);
	Equip_DisplayAlarmSignals(pEquip);

	printf("Equipment %s(%d) end displaying signals.\n",
		EQP_GET_EQUIP_NAME0(pEquip),
		pEquip->iEquipID);

	return TRUE;
}


static BOOL Site_DisplaySignals(IN SITE_INFO *pSite)
{
	int				i, nLevel;
	EQUIP_INFO		*pEquip;
	char			strTime[32];

	printf("\n\n\nSite has %d active alarms, %s:\n",
		pSite->iAlarmCount[0],
		TimeToString(time(NULL), TIME_CHN_FMT, strTime, sizeof(strTime)));
	
	for (nLevel = ALARM_LEVEL_OBSERVATION; nLevel < ALARM_LEVEL_MAX; nLevel++)
	{
		printf("EQUIP site Level %d has %d active alarms:\n",
			nLevel, pSite->iAlarmCount[nLevel]);
	}

	// proccess the equipment alarms if its data is changed
	pEquip = pSite->pEquipInfo;
	for (i = 0; i < pSite->iEquipNum; i++, pEquip++)
	{
		Equip_DisplaySignals(pEquip);
	}


	return TRUE;
}

/**********************************************************************/
#define	ERR_FILE_OPEN	1

static int ReadPicketFile(const char *szFileName, char **a_strInput)
{
	FILE *fp;
	
	int  i;
	
	char aBuf[256];
	
	if((fp = fopen(szFileName, "r")) == NULL) 
	{
		return ERR_FILE_OPEN;
	}
	
	i = 0;
	while(1) 
	{ 
		memset(aBuf, 0x00, 256);
		if(fgets(aBuf, 255, fp) == NULL)
		{
			a_strInput[i] = 0;
			fclose(fp);
			return 0; 
        }
		else
		{
			int	iLen = strlen(aBuf);
			if(iLen)
			{
				if((aBuf[0] == '/') 
					|| (aBuf[0] == '\r') 
					|| (aBuf[0] == '\n')
					|| (aBuf[0] == ' '))
				{
					continue;
				}
				a_strInput[i] = NEW(char, iLen + 1);
				ASSERT(a_strInput[i]);
				strcpy(a_strInput[i], aBuf);
				i++;
			}
		}
	} 
}

#endif



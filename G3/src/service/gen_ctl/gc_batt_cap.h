/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_batt_cap.h
 *  CREATOR  : Frank Cao                DATE: 2004-11-05 14:05
 *  VERSION  : V1.00
 *  PURPOSE  : 	1. Battery Type No. Management.
                2. Battery Capacity Prediction.
				3. Battery Capacity Regulation.
				4. Discharge Time Prediction
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_BATT_CAP_H_
#define _GC_BATT_CAP_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define	GC_CURVE_POINT_NUM	10
#define	GC_DIFF_RATE		(0.05)
#define CURVE_POINT_NUM		10
#define	MAX_DISCH_TIME		(24 * 15)
#define	MIN_LOAD_CURR		0.01
#define	RT_CURR_LMT_100		1.0

#define	MINI_COEF			0.0000001

#define	BATT_PREDICT_COEFF_NUM          12

#define	BATT_PREDICT_CELL_VOLT_DELTA         0.025


#define	BATT_24V_CELL_NUM          12
#define	BATT_48V_CELL_NUM          24


 struct tagGC_BATTPREDICT_DIVISION
 {
	 float fCellVolt;
	 float fDivCoeff;
 };

typedef struct tagGC_BATTPREDICT_DIVISION BATTPREDICT_DIVISION;
void	GC_BattCapCalc(void);
void GC_BattPredictInit(void);

#endif //_GC_BATT_CAP_H_

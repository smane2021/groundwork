/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_rect_list.h
 *  CREATOR  : Frank Cao                DATE: 2004-09-19 10:43
 *  VERSION  : V1.00
 *  PURPOSE  : To provide a group interface functions to create, maintain 
               and destory the AC-on/off rectifier List
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_RECT_LIST_H_
#define _GC_RECT_LIST_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

enum RECT_TYPE_ORDER
{
	RECT_TYPE_SELF = 0,
	RECT_TYPE_SLAVE1,
	RECT_TYPE_SLAVE2,
	RECT_TYPE_SLAVE3,

	MAX_RECT_TYPE_NUM,
};

/*Rectifer Info*/
struct	tagRECT_INFO		
{
	int			iRectEquipType;			//0: Self Rectifers,
										//1: Slave 1 Rectifers
										//2: Slave 2 Rectifers
										//3: Slave 3 Rectifers

	DWORD		dwSerialNo;				//Rectifier's Serial No.
	DWORD		dwHiSerialNo;			//High Rectifier's Serial No.
	int		iRectNo;					//Rectifier's No.
	DWORD		dwOperTime;				//Operation Time
	float		fOperTime;				//For controller to calculate the running time.
	DWORD		dwSelfOperTime;				//For controller to calculate the running time.
};
typedef struct tagRECT_INFO GC_RECT_INFO;

#ifdef GC_SUPPORT_MPPT
/*Rectifer Info*/
struct	tagMPPT_INFO		
{

	DWORD		dwSerialNo;				//Rectifier's Serial No.
	DWORD		dwHiSerialNo;			//High Rectifier's Serial No.
	int		iRectNo;					//Rectifier's No.
	DWORD		dwOperTime;				//Operation Time
};
typedef struct tagMPPT_INFO GC_MPPT_INFO;
void	GC_MaintainMpptList(void);
#endif

struct	tagRECT_INFO_IDX		
{
	int			iRectType;
	int			iSerialNoSid;
	int			iHiSerialNoSid;
	int			iRunTimeSid;
	int			iAcFailSid;
	int			iAcOnOffStaSid;
	int			iDcOnOffStaSid;
	int			iFaultSid;
	int			iProtectionSid;
};
typedef struct tagRECT_INFO_IDX GC_RECT_INFO_IDX;


struct	tagCONV_INFO_ID		
{
	int			iNoResponseSid;
	int			iAcOnOffStaSid;
	int			iFaultSid;

};
typedef struct tagRECT_INFO_IDX GC_CONV_INFO_ID;

/*To initialize the AC-on rectifier list and AC-off rectifier list*/
void	GC_InitRectList(void);

/*To maintain the two lists--Swictch_on rectifier list and 
  Swictch_off rectifier list */
void	GC_MaintainRectList(void);

/*To destory the rect lists*/
void	GC_DestroyRectList(void);
BOOL SearchAndRefresh(GC_RECT_INFO RectifierInfo, HANDLE hList);
BOOL UpdateAndRefresh(GC_RECT_INFO RectifierInfo, HANDLE hList);
void	GC_DestroyMpptList(void);
void	GC_InitMpptList();
#endif	/*_GC_RECT_LIST_H_*/


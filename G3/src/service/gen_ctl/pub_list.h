/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : pub_list.h
 *  CREATOR  : Frank Cao                DATE: 2004-09-15 19:37
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _PUB_LIST_H_
#define _PUB_LIST_H_

#include	"stdsys.h"
#include	"public.h"

typedef struct _LIST_NODE	LIST_NODE;

struct _LIST	//Simple List
{				
	DWORD			dwSize;		//size of an element or a record of this Link
	char			*pBuffer;	//the buffer to hold elements
	LIST_NODE		*pHead;		//the first char of the head position
	LIST_NODE		*pCurr;		//Current Point
};				
typedef struct _LIST	PUB_LIST;

struct _LIST_NODE	
{				
	LIST_NODE		*pNext;
	LIST_NODE		*pPrev;
	void			*pRecord;
};

//Creat a list
HANDLE List_Create(int iRecordSize);

//Destroy the list
void List_Destroy(HANDLE hList);

//Only permit to insert a record at head
BOOL List_Insert(HANDLE hList,
				 const void *pRecord);	

//copy current record to pOut
BOOL List_Get(const HANDLE hList,
			 void	*pOut);

//to modify current record with pOut
BOOL List_Set(const HANDLE hList,
			 const void	*pOut);

//delete the current record
BOOL List_Delete(HANDLE hList);

//To move the currrent point to head of list
BOOL List_GotoHead(HANDLE hList);

//To move the currrent point to tail of the list
BOOL List_GotoTail(HANDLE hList);

//To move the currrent point to next item of the list, expect 
//pCurr point to the tail
BOOL List_GotoNext(HANDLE hList);

//To move current point to previous item of the list, expect 
//pCurr point to the head
BOOL List_GotoPrev(HANDLE hList);

//To judge if the list is empty
BOOL List_IsEmpty(HANDLE hList);

#endif //_PUB_LIST_H_

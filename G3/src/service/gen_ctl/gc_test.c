/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_test.c
 *  CREATOR  : Frank Cao                DATE: 2004-09-23 11:46
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include	<signal.h>
#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"
#include	"gc_index_def.h"
#include	"pub_list.h"
#include	"gen_ctl.h"
#include	"gc_batt_mgmt.h"
#include	"gc_rect_list.h"
#include	"cfg_model.h"
#include	"data_exchange.h"
#include	"gc_picket.h"
#include	"../../mainapp/config_mgmt/cfg_mgmt.h"
#include	"../../mainapp/equip_mon/equip_mon.h"
static int Main_Init(void);
static int Main_Exit(IN int nCurrentStatus);
static void Main_SignalHandler(IN int nSig);
static DWORD Main_RunThreadEventHandler(
	IN DWORD	dwThreadEvent,
	IN HANDLE	hThread,
	IN const char *pszThreadName);
static void Main_ExitAbnormally(int nRunningState);
static void Main_HandleStopSignals(void);

#define SYS_IS_INITIALIZING 0
#define SYS_IS_RUNNING		1
#define SYS_IS_EXITING		2

#define SYS_IS_INTERRUPTED	-1	// killed/stopped by user
#define SYS_IS_KILLED		-2	// 10 Ctrl-C will kill the system.
#define SYS_IS_ABNORMAL		-3	// abnormal for threads
#define SYS_IS_FAULT		-4

#define GC_TEST_MAIN	"GC_TEST"

#define DEF_INIT_EXIT_MODULE(moduleName, bNecessary, errCode,				\
							 initProc, initArg1, initArg2,					\
	  						 exitProc, exitArg1, exitArg2)					\
{																			\
	(char *)(moduleName), (bNecessary),	(DWORD)(errCode),					\
	(INIT_MODULE_PROC)(initProc), {(void *)(initArg1), (void *)(initArg2)},	\
	(EXIT_MODULE_PROC)(exitProc), {(void *)(exitArg1), (void *)(exitArg2)},	\
}

#define MAX_INIT_EXIT_ARGS		2

typedef BOOL (*INIT_MODULE_PROC)(void *pArg1, void *pArg2);
typedef void (*EXIT_MODULE_PROC)(void *pArg1, void *pArg2);
// the init and exit modules shall be called in main.
struct _SYS_INIT_EXIT_MODULE
{
	char				*pszModuleName;
	BOOL				bMandatoryModule;	// must be init OK.
	DWORD				dwErrorCode;		// return errorcode on init error.

	INIT_MODULE_PROC	pfnInit;
	void				*pInitArgs[MAX_INIT_EXIT_ARGS];

	EXIT_MODULE_PROC	pfnExit;
	void				*pExitArgs[MAX_INIT_EXIT_ARGS];
};
typedef struct _SYS_INIT_EXIT_MODULE	SYS_INIT_EXIT_MODULE;

//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO			g_SiteInfo;
SERVICE_MANAGER		g_ServiceManager;
TIME_SRV_INFO		g_sTimeSrvInfo;  
char				g_szACUConfigDir[MAX_FILE_PATH] 
						= "/home/c13487/scup/src/config";
static int s_nSystemRunningState = SYS_IS_RUNNING;

static SYS_INIT_EXIT_MODULE	s_SysInitExitModule[] =
{
	DEF_INIT_EXIT_MODULE("Solution Configuration",	TRUE,	ERR_LOADING_CONFIG,
						EquipMonitoring_Init,		&g_SiteInfo,	NULL, 
						EquipMonitoring_Unload,		&g_SiteInfo,	NULL),
	DEF_INIT_EXIT_MODULE("Data Management Interface",FALSE,	ERR_INIT_DATA_MGMT,		
						DAT_IniMngHisData,			NULL,			NULL,
						DAT_ClearResource,			NULL,			NULL),
	DEF_INIT_EXIT_MODULE("Data Exchange Interface",	TRUE,	ERR_INIT_DXI,	
						InitDataExchangeModule,		NULL,			NULL,
						DestroyDataExchangeModule,	NULL,			NULL),
};


/*==========================================================================*
 * FUNCTION : Main_InitModules
 * PURPOSE  : init the modules by normal sequence.
 * CALLS    : 
 * CALLED BY: Main_Init
 * ARGUMENTS:   void : 
 * RETURN   : static int : if any mandatory module fails on init, 
 *                         return its errcode, or return OK.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-12-03 11:13
 *==========================================================================*/
static int Main_InitModules(void)
{
	int						i;
	BOOL					bResult;
	SYS_INIT_EXIT_MODULE	*pModule;
	int						nResult = ERR_MAIN_OK;

	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "Initializing system modules...\n");

	for (i = 1; i <= ITEM_OF(s_SysInitExitModule); i++)
	{
		pModule = &s_SysInitExitModule[i-1];

		if (pModule->pfnInit != NULL)
		{
			AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "%d: Initializing %s...\n",
				i, pModule->pszModuleName);

			bResult = pModule->pfnInit(
				pModule->pInitArgs[0],
				pModule->pInitArgs[1]);

			AppLogOut(GC_TEST_MAIN, 
				bResult ? APP_LOG_INFO : 
					pModule->bMandatoryModule ? APP_LOG_ERROR : APP_LOG_WARNING, 
				"%d: %s is %s successfully initialized.\n",
				i, pModule->pszModuleName,
				bResult ? "now" : "NOT");

			if (!bResult && pModule->bMandatoryModule)
			{
				nResult = pModule->dwErrorCode;
				break;
			}
		}
	}

	AppLogOut(GC_TEST_MAIN, (nResult == ERR_MAIN_OK) ? APP_LOG_INFO : APP_LOG_ERROR,
		"System modules are %s successully initialized.\n",
		(nResult == ERR_MAIN_OK) ? "now" : "NOT");

	return nResult;
}


/*==========================================================================*
 * FUNCTION : Main_DestroyModules
 * PURPOSE  : Destroy the module by reverse sequence.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : always OK
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-12-03 11:15
 *==========================================================================*/
static int Main_DestroyModules(void)
{
	int						i;
	SYS_INIT_EXIT_MODULE	*pModule;

	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "Destroying system modules...\n");

	// call destroy function by reverse sequence
	for (i = ITEM_OF(s_SysInitExitModule); i > 0; i--)
	{
		pModule = &s_SysInitExitModule[i-1];

		if (pModule->pfnExit != NULL)
		{
			AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "%d: Destroying %s...\n",
				i, pModule->pszModuleName);

			pModule->pfnExit(
				pModule->pExitArgs[0],
				pModule->pExitArgs[1]);

			AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "%d: %s is successfully destroyed.\n",
				i, pModule->pszModuleName);
		}
	}

	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "System modules are destroyed.\n");

	return ERR_MAIN_OK;
}

static BOOL Main_InstallSigHandler(BOOL bInstall)
{
	int	i;
	int pHandledSignals[] = 
	{	
		SIGTERM, SIGINT, SIGKILL, SIGQUIT, // Kill signal or Ctrl+C
		SIGPIPE,							// broken pipe.
		SIGSEGV,							// segment fault error.
	};

	//int pIgnoredSignals[] =
	//{
	//	SIGINT,
	//}

	UNUSED(bInstall);

	// 0. init msg handler.
	for (i = 0; i < ITEM_OF(pHandledSignals); i++)
	{
		signal(pHandledSignals[i], Main_SignalHandler);
	}

	return TRUE;
}

static int Main_Init()
{
	int		nResult;

#ifdef _GC_TEST
	int		nBlock, nId;
	size_t  ulBytes;
#endif //_GC_TEST

	s_nSystemRunningState = SYS_IS_INITIALIZING;

	// 1. starting msg.
	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "+----------------------------------+\n");
	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "|   ACU system now is starting...  |\n");
	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "+----------------------------------+\n");

	
	//if (!WatchDog_Open())
	//{
	//	AppLogOut(GC_TEST_MAIN, APP_LOG_ERROR,
	//		"Fails on opening and enabling the watch-dog.\n");
	//}


	// 0. init msg handler.
	Main_InstallSigHandler(TRUE);


	// 2. init thread mamager
	RunThread_ManagerInit(Main_RunThreadEventHandler);	// must be at 1st


	// 3. init sys modules.
	nResult = Main_InitModules();


#ifdef _GC_TEST
	nBlock = MEM_GET_INFO(&nId, &ulBytes);
	AppLogOut("MAIN", APP_LOG_INFO, "Total allocated %d mem blocks, %u bytes, "
		"%d time NEW/RENEW calls.\n",
		nBlock, ulBytes, nId);
#endif //_GC_TEST

	return nResult;
}

/*==========================================================================*
 * FUNCTION : Main_Exit
 * PURPOSE  : exit system accoring to the current status.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN OUT MAIN_ARGS  *THIS          : 
 *            IN int            nCurrentStatus : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-17 21:33
 *==========================================================================*/
static int Main_Exit(IN int nCurrentStatus)
{

	//s_nSystemRunningState = SYS_IS_EXITING;	// other value is already sett!
	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "+----------------------------------+\n");
	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "|   ACU system now is quiting...   |\n");
	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "+----------------------------------+\n");

	// destroy sys modules
	Main_DestroyModules();

	// stop the  thread manager.
	RunThread_ManagerExit(10000, TRUE);	// must be at last

	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "+----------------------------------+\n");
	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "|   ACU system exited %s  |\n",
		(nCurrentStatus == SYS_IS_INTERRUPTED) ? "normally.  " : "abnormally." );
	AppLogOut(GC_TEST_MAIN, APP_LOG_INFO, "+----------------------------------+\n\n\n");

	//// disable and close watch dog
	//WatchDog_Close((nCurrentStatus == SYS_IS_INTERRUPTED) 
	//	? TRUE		// for user interrupt, stop and disable dog
	//	: FALSE);	// for abnormally stop, do NOT disabel dog to make dog reset system

	return nCurrentStatus;
}

//static void TestGC(void);

/*==========================================================================*
 * FUNCTION : TestGC
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-23 11:55
 *==========================================================================*/
DWORD TestGC(void * pThreadArgs)
{
	SERVICE_ARGUMENTS	*pArgs;
	UNUSED(pThreadArgs);
	printf("General Controller start!!!\n");
	pArgs = NULL;
	ServiceMain(pArgs);
	return 0;
}


int main(void);

/*==========================================================================*
 * FUNCTION : main
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-23 11:51
 *==========================================================================*/
int main()
{
	HANDLE	hGcTestThread;
	BOOL	bExit = FALSE;
	int		nExitStatus;

	nExitStatus = Main_Init();
	hGcTestThread 
		= RunThread_Create("GenCtlTthread",
							(RUN_THREAD_START_PROC)TestGC,
							NULL,
							NULL,
							RUN_THREAD_FLAG_HAS_MSG);

	ASSERT(hGcTestThread);

	if (nExitStatus != ERR_MAIN_OK)
	{
		AppLogOut(GC_TEST_MAIN, APP_LOG_ERROR,
			"Init system got error code %d(%x), system exit.\n",
			nExitStatus, nExitStatus);
		nExitStatus = SYS_IS_ABNORMAL;
	}

	else 
	{
		while(!bExit)
		{
			if(getchar() != 'q')
			{
				//g_pGcData->bExitFlag = TRUE;
				Sleep(15000);
				bExit = TRUE;
			}
		}
	}

	// 3. cleanup
	Main_Exit(nExitStatus);

	return 0;
}

int UpdateSCUPTime(time_t* pTimeRet)
{
	return stime(pTimeRet);;
}

BOOL ConstructLangText(LANG_TEXT*	pLangText,
					   int			iMaxLenForFull,
					   int			iMaxLenForAbbr,
					   char*		pFullName0,
					   char*		pFullName1,
					   char*		pAbbrName0,
					   char*		pAbbrName1)


{
	char* pUseString;


	if (pLangText)
	{
		//	pLangText->iResourceID = 20;
		pLangText->iMaxLenForFull = iMaxLenForFull;	
		pLangText->iMaxLenForAbbr = iMaxLenForAbbr;	

		pUseString = NEW(char, pLangText->iMaxLenForFull);
		strncpyz(pUseString, pFullName0, pLangText->iMaxLenForFull);
		pLangText->pFullName[0] = pUseString;	

		pUseString = NEW(char, pLangText->iMaxLenForFull);
		strncpyz(pUseString, pFullName1, pLangText->iMaxLenForFull);
		pLangText->pFullName[1] = pUseString;

		pUseString = NEW(char, pLangText->iMaxLenForAbbr);
		strncpyz(pUseString, pAbbrName0, pLangText->iMaxLenForAbbr);
		pLangText->pAbbrName[0] = pUseString;	

		pUseString = NEW(char, pLangText->iMaxLenForAbbr);
		strncpyz(pUseString, pAbbrName1, pLangText->iMaxLenForAbbr);
		pLangText->pAbbrName[1] = pUseString;
	}

	return TRUE;
}

static void Main_SignalHandler(IN int nSig)
{
	TRACEX("Received a signal #%d.\n", nSig);

	switch (nSig)
	{
	case SIGSEGV:
		Main_ExitAbnormally(s_nSystemRunningState);	// never returns
		s_nSystemRunningState = SYS_IS_FAULT;	
		break;

	case SIGTERM:	
	case SIGKILL:
	case SIGQUIT:
		Main_HandleStopSignals();
		break;

	case SIGINT:	// CTRL+C, the number of signal will be sent to app 
					// when ctrl+c is pressed in ACU board, ignore it.
	case SIGPIPE:	// pipe broken, continue to run
	default:
		break;
	}
}
static DWORD Main_RunThreadEventHandler(
	IN DWORD	dwThreadEvent,
	IN HANDLE	hThread,
	IN const char *pszThreadName)
{
	UNUSED(pszThreadName);

	if (THREAD_EVENT_NO_RESPONSE == dwThreadEvent)
	{
		AppLogOut( GC_TEST_MAIN, APP_LOG_ERROR,
			"Thread %s(%p) is no response, it will be killed, "
			"and the system will be restarted!\n",
			pszThreadName, hThread);

		s_nSystemRunningState = SYS_IS_ABNORMAL;

		//continue this thread, but we need quit system
		//return THREAD_CONTINUE_THIS;
	}

	return THREAD_CONTINUE_RUN;
}
static void Main_ExitAbnormally(int nRunningState)
{
	char	*pStates[] = {"initializing", "running", "exiting"};

	if ((nRunningState < 0) || (nRunningState > ITEM_OF(pStates)))
	{
		nRunningState = SYS_IS_EXITING;
	}

	AppLogOut(GC_TEST_MAIN, APP_LOG_ERROR, 
		"PANIC: System received SIGSEGV(segment fault) signal "
		"while %s, system exit.\n",
		pStates[nRunningState]);


	_exit(-1);	// do NOT cleanup if fails exit().
}
static void Main_HandleStopSignals(void)
{
	static int		nKillCount = 0;
	static time_t	tmLastSignaled = 0;
	time_t			tmNow = time(NULL);

	if (tmNow - tmLastSignaled > 10)	// 10 seconds
	{
		nKillCount = 0;
	}

	tmLastSignaled = tmNow;

	nKillCount++;

	switch (nKillCount)
	{
	case 1:
#define SYS_PREQUIT_MSG	"Received a quit command, please send "\
			"command again to stop system.\n"
		AppLogOut(GC_TEST_MAIN, APP_LOG_WARNING, "%s", SYS_PREQUIT_MSG);
		printf("%s", SYS_PREQUIT_MSG);

		break;

	case 2:
#define SYS_INTR_MSG	"Received two quit commands, system is now quiting...\n"
		AppLogOut(GC_TEST_MAIN, APP_LOG_WARNING, "%s", SYS_INTR_MSG);

		printf("%s", SYS_INTR_MSG);
		s_nSystemRunningState = SYS_IS_INTERRUPTED;

		break;

	case 10:
#define SYS_KILL_MSG	"System aborted due to too many quit commands Ctrl-C.\n"
		printf("%s", SYS_KILL_MSG);
		AppLogOut(GC_TEST_MAIN, APP_LOG_WARNING, "%s", SYS_KILL_MSG);

		// exit with no cleanup.
		s_nSystemRunningState = SYS_IS_KILLED;

		_exit(-1);
		break;

	default:
		if (nKillCount >= 15)
		{
			Main_ExitAbnormally(s_nSystemRunningState);
		}

		printf( "System is quiting, please wait... \n" );
	}
}

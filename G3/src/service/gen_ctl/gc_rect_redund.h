/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_rect_redund.h
 *  CREATOR  : Frank Cao                DATE: 2004-09-15 15:26
 *  VERSION  : V1.00
 *  PURPOSE  : Rectifier Redundancy
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_RECT_REDUND_H_
#define _GC_RECT_REDUND_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define		GC_NO_RECT					-1
#define		DELAY_AFTER_SWITCH_OFF		60
#define		DELTA_DIFFERENCE			60

#define		GC_RECT_REDUND_NORMAL		0
#define		GC_RECT_REDUND_ACTIVE		1
#define		GC_RECT_REDUND_DRYING		2


//[0=LT93]/[1ID119=GT93]/[2=GT95]/[3=GT96]/[4=GT97]/[5=GT98]/[6=GT99]
#define		GC_RECT_INVALID		255

void	GC_RectRedund(void);
void	GC_RectRedundInit(void);

void	GC_RectHVSD(void);

int GC_GetCycPeriod(int iIdx);
int GC_GetSwitchOffDelay(int iIdx);
//int GC_GetPreCurrLmtDelay(int iIdx);

#endif //_GC_RECT_REDUND_H_




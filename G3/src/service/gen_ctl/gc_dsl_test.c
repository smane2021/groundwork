/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_dsl_test.c
 *  CREATOR  : Frank Cao                DATE: 2004-12-17 13:16
 *  VERSION  : V1.00
 *  PURPOSE  : Diesel Test
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gen_ctl.h"
#include	"gc_batt_mgmt.h"
#include	"gc_sig_value.h"
#include	"gc_dsl_test.h"
#include	"gc_engy_sav.h"

static void DslTestLog(void);
static BOOL	IsDslTestTime(void);


/*==========================================================================*
 * FUNCTION : GC_DslTestInit
 * PURPOSE  : To initialize data of doesel test
 * CALLS    : 
 * CALLED BY: InitGC
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-20 17:08
 *==========================================================================*/
void GC_DslTestInit(void)
{
	g_pGcData->RunInfo.Ac.bLastDslTestState = GC_DSL_TEST_STOP;
	g_pGcData->RunInfo.Ac.bLastDslState = GC_DIESEL_OFF;

	GC_SuspendSecTimer(SEC_TMR_ID_DSL_TEST_DELAY);
	g_pGcData->SecTimer.abNeedSave[SEC_TMR_ID_DSL_TEST_DELAY] = FALSE;

	return;
}



/*==========================================================================*
 * FUNCTION : GC_DslTest
 * PURPOSE  : To provide interface function for diesel test
 * CALLS    : GC_GetEnumValue DslTestLog GC_SetEnumValue
 * CALLED BY: ServiceMain
 * ARGUMENTS:   
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-20 17:09
 *==========================================================================*/
void GC_DslTest(void)
{
	static	BOOL	bAutoFlag = FALSE;
	GC_RUN_INFO_AC	*pRunInfoAc = &(g_pGcData->RunInfo.Ac);
	BOOL			bDslTestState;
	//printf("pRunInfoAc->bDslValid = %d, pRunInfoAc->bLastDslState = %d, pRunInfoAc->bLastDslTestState = %d,bDslTestState= %d\n", pRunInfoAc->bDslValid, pRunInfoAc->bLastDslState, pRunInfoAc->bLastDslTestState,bDslTestState);
	if(!(pRunInfoAc->bDslValid))
	{
		pRunInfoAc->bLastDslTestState = GC_DSL_TEST_STOP;
		pRunInfoAc->bLastDslState = GC_DIESEL_OFF;
		GC_SuspendSecTimer(SEC_TMR_ID_DSL_TEST_DELAY);
		
		return;	
	}


	bDslTestState = GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_TEST_STA,GC_DIESEL_INVALID);
	if(bDslTestState == GC_DIESEL_INVALID )
	{
		return;
	}

	if((GC_DIESEL_OFF == pRunInfoAc->bLastDslState)
		&& (GC_DIESEL_ON == pRunInfoAc->bDslState))
	{
		GC_SetSecTimer(SEC_TMR_ID_DSL_TEST_DELAY,
					0,
					GC_GetDslTestDelay,
					0);
	}

	if(GC_DSL_TEST_START == pRunInfoAc->bLastDslTestState)
	{
		if(GC_DSL_TEST_STOP == bDslTestState)
		{
			DslTestLog();
			GC_SetDateValue(TYPE_OTHER, 
							0,
							DSL_PUB_LAST_TEST_DATE,
							g_pGcData->SecTimer.tTimeNow);
			g_pGcData->RunInfo.Ac.tmLastDslTest = g_pGcData->SecTimer.tTimeNow;
			GC_SaveData();
		}
	}

	else if(GC_DSL_TEST_START == bDslTestState)
	{
		if(bAutoFlag)
		{
			bAutoFlag = FALSE;
			pRunInfoAc->DslTestInfo.enumStartReason 
				= DSL_TEST_FOR_PLAN;
		}
		else
		{
			pRunInfoAc->DslTestInfo.enumStartReason 
				= DSL_TEST_MANUALLY;
		}
		pRunInfoAc->DslTestInfo.tStartTime
				= g_pGcData->SecTimer.tTimeNow;		
	}

	else if(STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
	//Automatically
	{
		if((GC_ENABLED == GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_TEST_ENB,GC_EQUIP_INVALID))
			&&(!(pRunInfoAc->bDslState)))
		{
			if(SEC_TMR_TIME_IN 
				!= GC_GetStateOfSecTimer(SEC_TMR_ID_DSL_TEST_DELAY))
			{
				if(IsDslTestTime())
				{
					GC_SetEnumValue(TYPE_OTHER, 
								0,
								DSL_PUB_TEST_START_CTL, 
								GC_DSL_TEST_START,
								TRUE);
					bAutoFlag = TRUE;
				
					GC_LOG_OUT(APP_LOG_INFO, 
						"It's diesel test hour, try to start a test.\n");
				}
			}
		}
	}


	pRunInfoAc->bLastDslTestState = bDslTestState;
	pRunInfoAc->bLastDslState = pRunInfoAc->bDslState;
	return;	
}



/*==========================================================================*
 * FUNCTION : DslTestLog
 * PURPOSE  : to write a log into diesel test log
 * CALLS    : GC_LOG_OUT GC_GetEnumValue DAT_StorageCreate 
              DAT_StorageWriteRecord DAT_StorageClose
 * CALLED BY: GC_DslTest
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-20 17:12
 *==========================================================================*/
static void DslTestLog(void)
{
	HANDLE	hTestLog;
	BOOL	bRst;
	char	szBuffer[100];

	sprintf(szBuffer, 
			"Diesel Test End, test result code is %d. \n",
			(int)(GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_TEST_RESULT,DSL_EQUIP_INVALID)));
	GC_LOG_OUT(APP_LOG_INFO, szBuffer);

	g_pGcData->RunInfo.Ac.DslTestInfo.tEndTime 
		= g_pGcData->SecTimer.tTimeNow;
	g_pGcData->RunInfo.Ac.DslTestInfo.enumTestResult 
		= GC_IsGCEquipExist(TYPE_OTHER, 0, DSL_PUB_TEST_RESULT,DSL_EQUIP_INVALID);

	hTestLog = DAT_StorageCreate(DSL_TEST_LOG, 
					sizeof(GC_DSL_TEST_INFO), 
					DSL_TEST_MAX_RECORDS, 
					DSL_TEST_FREQUENCY);
	
	ASSERT(hTestLog);
	if(!hTestLog)
	{
		GC_LOG_OUT(APP_LOG_INFO, "Failed to create diesel test log!\n");
		return;
	}

	bRst = DAT_StorageWriteRecord(hTestLog, 
								sizeof(GC_DSL_TEST_INFO), 
								&(g_pGcData->RunInfo.Ac.DslTestInfo));

	ASSERT(bRst);
	if(!bRst)
	{
		GC_LOG_OUT(APP_LOG_INFO, "Failed to write diesel test log!\n");
		return;
	}

	bRst = DAT_StorageClose(hTestLog);

	ASSERT(bRst);
	if(!bRst)
	{
		GC_LOG_OUT(APP_LOG_INFO, "Failed to close diesel test log!\n");
		return;
	}

	return;
}



/*==========================================================================*
 * FUNCTION : IsDslTestTime
 * PURPOSE  : To jadge if it is time for diesel test
 * CALLS    : gmtime_r 
 * CALLED BY: GC_DslTest
 * ARGUMENTS:   
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-20 17:14
 *==========================================================================*/
static BOOL	IsDslTestTime(void)
{
	int			i;
	DWORD			iPlanNum;
	struct tm	tmLast;
	struct tm	tmNow;

	gmtime_r(&(g_pGcData->RunInfo.Ac.tmLastDslTest), &tmLast);
	gmtime_r(&(g_pGcData->SecTimer.tTimeNow), &tmNow);

	if((tmNow.tm_year == tmLast.tm_year) 
		&& (tmNow.tm_mon == tmLast.tm_mon)
		&& (tmNow.tm_mday == tmLast.tm_mday) 
		&& (tmNow.tm_hour == tmLast.tm_hour))
	{
		//If no hour change, then return FALSE.
		return FALSE;
	}

	BOOL bRTN;

	bRTN = GC_GetDwordValueWithStateReturn(TYPE_OTHER,
								0, 
								DSL_PUB_TEST_NUM,
								TRUE,
								&iPlanNum);
	if(bRTN == FALSE)
	{
		return FALSE;
	}
	for(i = 0; i < (int)iPlanNum; i++)
	{
		SIG_TIME timePlan 
			= GC_GetTimeValue(TYPE_OTHER, 0, DSL_PUB_TEST_TIME1 + i);
		
		struct tm tmPlan;
		
		gmtime_r(&timePlan, &tmPlan);
		
		if((tmNow.tm_mon == tmPlan.tm_mon)
			&& (tmNow.tm_mday == tmPlan.tm_mday)
			&& (tmNow.tm_hour == tmPlan.tm_hour))
		{
			g_pGcData->RunInfo.Ac.tmLastDslTest 
				= g_pGcData->SecTimer.tTimeNow;
			GC_SaveData();
			return TRUE;
		}
	}
	return FALSE;
}

int GC_GetDslTestDelay(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_HOUR 
		* GC_GetDwordValue(TYPE_OTHER, 
						0,
						DSL_PUB_TEST_DELAY);
}



/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_batt_history.h
 *  CREATOR  : Frank Cao                DATE: 2004-10-13 17:15
 *  VERSION  : V1.00
 *  PURPOSE  : battery log interface functions
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_BATT_HISTORY_H_
#define _GC_BATT_HISTORY_H_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define	SHALLOW_DISCH_VOLT	50.0
#define	MIDDLE_DISCH_VOLT	46.8
#define	DEEP_DISCH_VOLT		42.0
#define	BATT_HIGH_TEMP		30.0
#define	BATT_LOW_TEMP		10.0

enum	BATT_DISCH_STATE
{
	DISCH_STATE_NORMAL = 0,
	DISCH_STATE_SHALLOW,
	DISCH_STATE_MIDDLE,
	DISCH_STATE_DEEP,
};

enum	BATT_TEMP_STATE
{
	BATT_TEMP_NORMAL = 0,
	BATT_TEMP_HIGH,
	BATT_TEMP_LOW,
};

struct	_STATE_RECORD_
{
	BYTE		byState;
	time_t		tChangeTime;
};
typedef struct _STATE_RECORD_ STATE_RECORD;

void	GC_BattHisInit(void);
void	GC_BattHisRefresh(void);
void	GC_BattHisTimeProof(long lDeltaTm);


#endif	/*_GC_BATT_HISTORY_H_*/


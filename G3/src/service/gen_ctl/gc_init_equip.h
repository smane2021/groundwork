/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_init.h
 *  CREATOR  : Frank Cao                DATE: 2004-09-09 14:37
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _GC_INIT_EQUIP_
#define	_GC_INIT_EQUIP_

#include	"stdsys.h"
#include	"public.h"
#include	"basetypes.h"

#define	MAX_NUM_RECT_EQUIP			160//120
#define MAX_NUM_CT_EQUIP			60
#define MAX_NUM_MPPT_EQUIP			30
#define MAX_NUM_BMS_EQUIP			16
//2010-6-9, Add 1 battery every SMDU Unit.Now there is 5 batteries for 1 SMDU.
//#define	MAX_NUM_BATT_EQUIP			94//86//66//46 exclude largedu Battery for V2
#ifdef SUPPORT_CSU

#define	MAX_NUM_BATT_EQUIP			135//115//86//66//46 exclude largedu Battery for V2��20��SMBRC�ĵ��
#define	MAX_NUM_BATT_FUSE_EQUIP			9

#else

//#define	MAX_NUM_BATT_EQUIP			114//86//66//46 exclude largedu Battery for V2��20��SMBRC�ĵ��
#define		MAX_NUM_BATT_EQUIP			194	//174//114+60 ������60��Li���;Jimmy2012/04/05
#define		MAX_NUM_CAN_BATT_EQUIP			114	
#define		MAX_NUM_485_BATT_EQUIP			60

#define		MAX_NUM_SOLUTION_BATT_EQUIP		114

#define		MAX_NUM_SMBAT_OR_SMBRC			20

#define NONE	0
#define SMBAT_TYPE  1
#define SMBRC_TYPE  2

#define	MAX_NUM_BATT_FUSE_EQUIP			9

#endif


#define	MAX_NUM_LVD_EQUIP			20//19  Add LVD3 equipment ,so change from 19 to 20
#define	MAX_NUM_EIB_EQUIP			4
#define	MAX_NUM_TEMPERATURE			7//10//7    //7 -> 10, Add 3 largeDU temperature//10 -> 7 remove largeDU temperature
#define MAX_NUM_LargeDU_TEMP			3
#define	MAX_NUM_OF_BLK_PER_EIB			8
#define MAX_BLOCK_NUM				24
#define	MAX_LDU_TEMPER_NUM			30
#define LVD3_EQUIP_ID_SEQUENCE			9 //Fixed for LVD
#define LVD3_EQUIP_ID_485_SEQUENCE		19//If load all equipment, then fixed for LVD
#define OBLVD_EQUIP_ID_SEQUENCE			0



#define	GET_EQUIP_TYPE_ID_MAJOR(a)	((int)((a) / 100))
#define	GET_EQUIP_TYPE_ID_SUB(a)	((int)((a) % 100))


struct	tagBATT_EQUIP_INFO		
{
	BOOL		bCurr;			//Battery Current of a string
	BOOL		bVolt;			//Battery Voltage of a string
	BOOL		bCap;			//Battery Capacity of a string
	BOOL		bLargeBat;		//if it is largedu battery , bLargeBat is true
};
typedef struct tagBATT_EQUIP_INFO BATT_EQUIP_INFO;

struct	tagEXIST_BATT_EQUIP_INFO		
{
	
	BOOL		bTemp;			//Temperature of a string
	int		iQtyBlock;		//Quantity of block
	
};
typedef struct tagEXIST_BATT_EQUIP_INFO EXIST_BATT_EQUIP_INFO;

struct	tagBLOCK_EQUIP_INFO		
{
    BOOL		abSMBlockVolt[MAX_BLOCK_NUM];	//����SMBAT��SMBRC�Ŀ��ѹ
};
typedef struct tagBLOCK_EQUIP_INFO BLOCK_EQUIP_INFO;

struct	tagEIB_EQUIP_INFO		
{
	BOOL		abBlockVolt[MAX_NUM_OF_BLK_PER_EIB];	//Is the Block 1 Voltage exist
};
typedef struct tagEIB_EQUIP_INFO EIB_EQUIP_INFO;


struct	tagAC_EQUIP_INFO		
{
	BOOL		bDslStatus;		//Diesel Start/Stop state
	BOOL		bTotalPower;	//Total Power
	BOOL		bDslCtl;		//Diesel Start/Stop Control
};
typedef struct tagAC_EQUIP_INFO AC_EQUIP_INFO;

struct	tagBATT_TEST_LOG_INFO		
{
	/*Battery signals Information*/
	BATT_EQUIP_INFO		aBattInfo[MAX_NUM_BATT_EQUIP];
	EXIST_BATT_EQUIP_INFO	aExistBattInfo[MAX_NUM_BATT_EQUIP];
	/*EIB signals Information*/
	EIB_EQUIP_INFO		aEibInfo[MAX_NUM_EIB_EQUIP];
	/*Temperature information*/
	BOOL				abTemp[MAX_NUM_TEMPERATURE];
	/*Size of battery test record, depends on BattInfo*/
	int					iSizeOfBattTestRcd;

	/*Size of max battery test record, just for allocate flash space*/
	int					iMaxSizeOfBattTestRcd;
	//�����Ƿ���SMBAT����SMBRC���ѹ�ṹ
	BLOCK_EQUIP_INFO	aBlockInfo[MAX_NUM_SMBAT_OR_SMBRC];

	int			iSMBAT_SMBRC;	//������SMBAT����SMBRC

	int			iSMBATBeginNo; //SMBAT��ص���ʼ��ţ���Ϊ�п���ɾ��ĳЩ����豸������SMBAT��ص���ʼ��Ų��ǹ̶���

	int			iSMBRCBeginNo; //SMBRC��ص���ʼ��ţ���Ϊ�п���ɾ��ĳЩ����豸������SMBRC��ص���ʼ��Ų��ǹ̶���
};
typedef struct tagBATT_TEST_LOG_INFO BATT_TEST_LOG_INFO;


struct	tagGC_EQUIP_INFO
{
	/*The Qty of all of equipments*/
	int					iQtyOfEquip;

	/*The Qty of all of samplers*/
	//int					iQtyOfSampler;

	/*Following 4 variant are initialized by GC_InitQuipInfo, they are 
	  depend on Solution Configration*/
	int					iCfgQtyOfBatt;
	//added by Jimmy for judging if Li cfg is exsisted
	int					iCfgQtyOfLiBatt;
	int					iCfgQtyOfComBAT;
	int					iCfgQtyOfEIBBAT1;
	int					iCfgQtyOfSmduBAT1;
	int					iCfgQtyOfEIBBAT2;
	int					iCfgQtyOfSmduBAT2;
	int					iCfgQtyOfLargeDUBAT;
	int					iCfgQtyOfSMBAT;//��¼SMBAT Battery����
	int					iCfgQtyOfSMBRC;//��¼SMBRC Battery����
	int					iCfgQtyOfSMDUEBAT;

	int					iCfgQtyOfRect;
	int					iCfgQtyOfS1Rect;
	int					iCfgQtyOfS2Rect;
	int					iCfgQtyOfS3Rect;
	int					iCfgQtyOfCt;
	int					iCfgQtyOfBattFuse;
	int					iCfgQtyOfLvd;
	int					iCfgQtyOfEib;
#ifdef GC_SUPPORT_MPPT
	int					iCfgQtyOfMppt;
#endif
#ifdef GC_SUPPORT_MPPT
	int					iCfgQtyOfBMS;
#endif
	/*Following defines the Equipment ID for every Equipment*/
	int					aiEquipIdRtPri[MAX_NUM_RECT_EQUIP];
	int					aiEquipIdS1RtPri[MAX_NUM_RECT_EQUIP];
	int					aiEquipIdS2RtPri[MAX_NUM_RECT_EQUIP];
	int					aiEquipIdS3RtPri[MAX_NUM_RECT_EQUIP];
	int					aiEquipIdCtPri[MAX_NUM_CT_EQUIP];
	int					aiEquipIdBtPri[MAX_NUM_BATT_EQUIP];
	int					aiEquipIdLvdPri[MAX_NUM_LVD_EQUIP];
	int					aiEquipIdBtFusePri[MAX_NUM_BATT_FUSE_EQUIP];
	int					aiEquipIdEibPri[MAX_NUM_EIB_EQUIP];
#ifdef GC_SUPPORT_MPPT
	int					aiEquipIdMtPri[MAX_NUM_MPPT_EQUIP];
#endif
#ifdef GC_SUPPORT_MPPT
	int					aiEquipIdBMSPri[MAX_NUM_MPPT_EQUIP];
#endif
	/*int					aiEquipId[MAX_NUM_OF_EQUIP_TYPE];*/

	/*Battery test log Information*/
	BATT_TEST_LOG_INFO	BattTestLogInfo;

	/*AC Information*/
	AC_EQUIP_INFO		AcInfo;

};
typedef struct	tagGC_EQUIP_INFO GC_EQUIP_INFO;


void	GC_EquipInfoInit(void);
BOOL	GC_IsSigValid(int iEquipType,
					  int iEquipIdx,
					  int iSigIdx);
BOOL	GC_IsEIBSigValid(int iEquipType,
			 int iEquipIdx,
			 int iSigIdx);
BOOL	GC_Is_ABatt_Li_Type(int iIndx);//added by Jimmy 2012/04/05
UINT	GC_Get_Total_BattNum(void); //added by Jimmy 2012/04/11
#endif //_GC_INIT_EQUIP_

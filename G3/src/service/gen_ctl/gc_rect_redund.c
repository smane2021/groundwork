/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : gc_rect_redund.c
 *  CREATOR  : Frank Cao                DATE: 2004-09-15 15:29
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "gc_rect_redund.h"
#include "gen_ctl.h"
#include "gc_timer.h"
#include "gc_sig_value.h"
#include "gc_index_def.h"
#include "gc_batt_mgmt.h"
#include "gc_rect_list.h"
#include "pub_list.h"
#include "gc_run_info.h"
#include "gc_estop_eshutdown.h"


static void RedundSwitch(void);
static void CyclingSwitch(void);
static int GetMaxRuntimeNo(int *iRectType);
static int 	FindLowEffiMaxRuntimeRect(int *iRectType);
static int GetMinRuntimeNo(int *iRectType);
static int GetFstInOnList(int *iRectType);
static int GetFstInOffList(int *iRectType);
static BOOL	IsSwitchTime(void);
static void SwitchOnAllRect(BOOL bNeedPreCurrentLimit);
static void SwitchOnOneRect(int iRectNo, int iRectType);
static void SwitchOffOneRect(int iRectNo, int iRectType);
static char *GetRectIdStr(DWORD dwRectId, DWORD *dwRectHiId,
							 char *pStrOut);
static BOOL IsDiselRunning(void);
static BOOL IsRectHaveOtherAlarm(void);
static BOOL IsChargeOrDischarge(void);
static BOOL IsOverEnergeSavingPoint(void);

static int		GC_GetRectDryingTime(int iIdx);
static BOOL 	NeedOpenOneRect(void);
static BOOL 	NeedCloseOneRect(void);

static void SetRectHVSDstatus(int iRectId, BOOL bAlarm);
static void SetSlaveRectHVSDstatus(int iRectId, BOOL bAlarm,int iSlaveID);


/*==========================================================================*
 * FUNCTION : GC_RectRedundInit
 * PURPOSE  : To initialize Rectifier Redundancy Timer
 * CALLS    : GC_GetDwordValue GC_SetSecTimer GC_SuspendSecTimer
 * CALLED BY: InitGC
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-15 14:41
 *==========================================================================*/
void GC_RectRedundInit(void)
{
	BOOL	*p_bNeedSave = g_pGcData->SecTimer.abNeedSave;

	GC_SuspendSecTimer(SEC_TMR_ID_RECT_CYCLE);
	p_bNeedSave[SEC_TMR_ID_RECT_CYCLE] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);
	p_bNeedSave[SEC_TMR_ID_SWITCH_OFF_DELAY] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_DELAY);
	p_bNeedSave[SEC_TMR_ID_SWITCH_DELAY] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY);
	p_bNeedSave[SEC_TMR_ID_PRE_CURR_LMT_DELAY] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_RECT_CURR);
	p_bNeedSave[SEC_TMR_ID_RECT_CURR] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_ON_DELAY);
	p_bNeedSave[SEC_TMR_ID_SWITCH_ON_DELAY] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD);
	p_bNeedSave[SEC_TMR_ID_OSCILLATE_PERIOD] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_ALL_RECT_DRYING);
	p_bNeedSave[SEC_TMR_ID_ALL_RECT_DRYING] = FALSE;

	g_pGcData->RunInfo.Rt.bRectHVSD = FALSE;
	SetRectHVSDstatus(-1, FALSE);

	return;
}


/*==========================================================================*
 * FUNCTION : GC_RectRedund
 * PURPOSE  : Rectifer Redundancy
 * CALLS    : GC_GetEnumValue RedundSwitch CyclingSwitch
 * CALLED BY: ServiceMain
 * RETURN   :
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-15 14:41
 *==========================================================================*/
void GC_RectRedund()
{
	
	static	int			s_iRectRedundCounter = 0;
	static	int			s_iBattCurrCounter = 0;
	static	int			s_iOscilateTimes = 0;
	static	BOOL		s_bLastESActive = FALSE;
	static	BOOL		s_bAllRectDryingFlag = FALSE;
	static	SIG_ENUM	s_enumLastRedundEnb = GC_DISABLED;
	//printf("----------IN--------------\n");
#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif
	SIG_ENUM enumRedundEnb = GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_ENB);

	if((enumRedundEnb == GC_ENABLED) && (s_enumLastRedundEnb == GC_DISABLED))
	{
		
		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_OSCILL, GC_NORMAL, TRUE);
	}
	else if((enumRedundEnb == GC_DISABLED) && (s_enumLastRedundEnb == GC_ENABLED))
	{
		
		SwitchOnAllRect(FALSE);		//Not PreCurrenLimit
	}
#ifdef GC_SUPPORT_MPPT
	else if(stState != GC_MPPT_DISABLE)
	{
		//No ECO in MPPT.
		return;
	}
#endif
	if((enumRedundEnb == GC_ENABLED) && GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_ENABLE))
	{
		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_MAX_CURR_LMT_ENABLE, GC_DISABLED, TRUE);;
	}
	s_enumLastRedundEnb = enumRedundEnb;
	
	if(SEC_TMR_TIME_OUT == GC_GetStateOfSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD))
	{
		GC_SuspendSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD);
		s_iOscilateTimes = 0;
	}

	if(s_iOscilateTimes >= 5)
	{
		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_OSCILL, GC_ALARM, TRUE);
		GC_SuspendSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD);
		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_ENB, GC_DISABLED, TRUE);
		s_iOscilateTimes = 0;
		GC_LOG_OUT(APP_LOG_INFO, "Redundency control oscillated, disable the function. \n");
	}
			
	
	if((STATE_AUTO != g_pGcData->RunInfo.enumAutoMan) //Manually, 
		|| (GC_ENABLED != enumRedundEnb)	//The rectifiers shall be opened in BattMgmt
		|| g_pGcData->RunInfo.Rt.bRectHVSD)	//GC close some HVSD rect ,so do NOT redundancy
	{
		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_ES_ACTIVE_SET, GC_NORMAL, TRUE);
		s_bLastESActive = FALSE;

		//s_iRectRedundCounter = 0;

		//printf("g_pGcData->RunInfo.enumAutoMan= %d, enumRedundEnb= %d, g_pGcData->RunInfo.Rt.bRectHVSD = %d\n", g_pGcData->RunInfo.enumAutoMan, enumRedundEnb, g_pGcData->RunInfo.Rt.bRectHVSD);
		return;
	}
	
	
	//Rectifier redunacy can be valid only in float status and no current limit
	//So Current limit,Disel is running, Rectifier have other alarm is not redunace
	if((g_pGcData->RunInfo.Bt.bErrCurr 
		&& (GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_IMBAL_PROT_ENB)))
		|| g_pGcData->RunInfo.Rt.bAcFail
		|| IsRectHaveOtherAlarm()
		|| GC_GetBattFuseAlm()
		|| GC_IsLVDDisconnect())
	{
		//Open all of rectifiers
		/*printf("SwitchOnAllRect no Pre-Limit-Current, for Alarm. iQtyAcOffRect = %d \n",
			g_pGcData->RunInfo.Rt.iQtyAcOffRect);*/
		SwitchOnAllRect(FALSE);		// Don't PreCurrenLimit in Redunce

		//Suspend timer
		GC_SuspendSecTimer(SEC_TMR_ID_RECT_CYCLE);
		GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);

		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_ES_ACTIVE_SET, GC_NORMAL, TRUE);
		
		if(SEC_TMR_SUSPENDED == GC_GetStateOfSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD))
		{
			GC_SetSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD, 4 * SECONDS_PER_HOUR, NULL, 0);
		}

		if(s_bLastESActive)
		{
			s_iOscilateTimes++;
		}
		s_bLastESActive = FALSE;
		//printf("g_pGcData->RunInfo.Rt.bAcFail= %d, IsRectHaveOtherAlarm= %d, GC_GetBattFuseAlm = %d\n", g_pGcData->RunInfo.Rt.bAcFail, IsRectHaveOtherAlarm(), GC_GetBattFuseAlm());
		return;
	}

	if(IsChargeOrDischarge())
	{
		if(s_iBattCurrCounter < 2)
		{
			s_iBattCurrCounter++;	
		}
		else if (s_iBattCurrCounter >= 2)
		{	
			s_iBattCurrCounter = 5;
			SwitchOnAllRect(TRUE);		// PreCurrenLimit

			//Suspend timer
			GC_SuspendSecTimer(SEC_TMR_ID_RECT_CYCLE);
			GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);

			GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_ES_ACTIVE_SET, GC_NORMAL, TRUE);
			
			if(SEC_TMR_SUSPENDED == GC_GetStateOfSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD))
			{
				GC_SetSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD, 4 * SECONDS_PER_HOUR, NULL, 0);
			}

			if(s_bLastESActive)
			{
				s_iOscilateTimes++;
			}
			
			s_bLastESActive = FALSE;
			//printf("IsChargeOrDischarge= %d, \n", IsChargeOrDischarge());
	
		}
		return;
	}
	else
	{
		if(s_iBattCurrCounter>0)
		{
			s_iBattCurrCounter--;
			return;
		}	
	}


	if(((GC_IsUnderVolt()) 
			&& (SEC_TMR_TIME_IN 
				!= GC_GetStateOfSecTimer(SEC_TMR_ID_SWITCH_ON_DELAY)))
		|| GC_IsPowerLmt()
		|| IsOverEnergeSavingPoint()
		//|| ST_FLOAT != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) 
		|| IsDiselRunning())
	{
		if(s_iRectRedundCounter < 2)
		{
			s_iRectRedundCounter++;	
		}
		else
		{
			SwitchOnAllRect(FALSE);		//Not PreCurrenLimit

			//Suspend timer
			GC_SuspendSecTimer(SEC_TMR_ID_RECT_CYCLE);
			GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);

			GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_ES_ACTIVE_SET, GC_NORMAL, TRUE);
			
			if(SEC_TMR_SUSPENDED == GC_GetStateOfSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD))
			{
				GC_SetSecTimer(SEC_TMR_ID_OSCILLATE_PERIOD, 4 * SECONDS_PER_HOUR, NULL, 0);
			}
			if(s_bLastESActive)
			{
				s_iOscilateTimes++;
			}			
			s_bLastESActive = FALSE;
			//printf("GC_IsUnderVolt= %d, \n", GC_IsUnderVolt());

			return;
		}
	}
	else
	{
		s_iRectRedundCounter = 0;
	}


	if(g_pGcData->RunInfo.Rt.iQtyAcOffRect)
	{
		if(!(GC_TIMER_IS_RUNNING(SEC_TMR_ID_PRE_CURR_LMT_DELAY)))
		{
			GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_ES_ACTIVE_SET, GC_ALARM, TRUE);
			s_bLastESActive = TRUE;
		}
	}
	else
	{
		GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_ES_ACTIVE_SET, GC_NORMAL, TRUE);
		s_bLastESActive = FALSE;
	}

	//if(SEC_TMR_SUSPENDED == GC_GetStateOfSecTimer(SEC_TMR_ID_RECT_CYCLE))
	//{
	//	GC_SetSecTimer(SEC_TMR_ID_RECT_CYCLE, 0, GC_GetCycPeriod, 0);
	//}

	//Modified by Samson, 2009-12-07
	//change to new redundancy strategy

	//if(SEC_TMR_SUSPENDED == GC_GetStateOfSecTimer(SEC_TMR_ID_SWITCH_DELAY))
	//{
	//      RedundSwitch();
	//}
	//
	//CyclingSwitch();

	if(GC_GetStateOfSecTimer(SEC_TMR_ID_RECT_CYCLE) == SEC_TMR_TIME_OUT)
	{
		GC_SetSecTimer(SEC_TMR_ID_RECT_CYCLE, 0, GC_GetCycPeriod, 0);

		int iRectDryingTime = GC_GetRectDryingTime(0);

		if(iRectDryingTime > 0)
		{
			s_bAllRectDryingFlag = TRUE;

			GC_SetSecTimer(SEC_TMR_ID_ALL_RECT_DRYING, iRectDryingTime, NULL, 0);

			GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);

			TRACE("Start to drying. Switch ON all rect.\n");
			SwitchOnAllRect(FALSE);

			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				RT_PUB_ES_ACTIVE_SET,
				GC_RECT_REDUND_DRYING,
				TRUE);
			//printf("Drying now\n");
			return;
		}
		else
		{

			GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);
			SwitchOnAllRect(FALSE);
			
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				RT_PUB_ES_ACTIVE_SET,
				GC_RECT_REDUND_NORMAL,
				TRUE);
			return;
		}

	}

	if(s_bAllRectDryingFlag)
	{
		if(GC_GetStateOfSecTimer(SEC_TMR_ID_ALL_RECT_DRYING) == SEC_TMR_TIME_OUT)
		{
			TRACE("Dry complete.\n");

			s_bAllRectDryingFlag = FALSE;
			GC_SuspendSecTimer(SEC_TMR_ID_ALL_RECT_DRYING);
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				RT_PUB_ES_ACTIVE_SET,
				GC_RECT_REDUND_NORMAL,
				TRUE);
		}
		//printf("s_bAllRectDryingFlag = %d now\n", s_bAllRectDryingFlag);
		return;
	}

	if(NeedOpenOneRect())
	{
		TRACE("Need Open Rect. Switch ON all rect.\n");
		SwitchOnAllRect(FALSE);

		GC_SuspendSecTimer(SEC_TMR_ID_RECT_CYCLE);
		//printf("NeedOpenOneRect\n");
		return;
	}

	if(NeedCloseOneRect())
	{
		//printf("Need Close Rect. Delaying.\n");
		if(GC_TIMER_IS_STOP(SEC_TMR_ID_SWITCH_OFF_DELAY))
		{
			GC_SetSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY, 0, GC_GetSwitchOffDelay, 0);
		}
		else if(GC_TIMER_IS_RUNNING(SEC_TMR_ID_PRE_CURR_LMT_DELAY))
		{
			//When the system in AC Failure pre-currentlimit, do not start ECO. Test the 2900U AC Failure
			//function ,the battery current maybe zero in pre-currentlimit state, then start ECO after finishing the pre-current limit,
			//It is not correct.
			GC_SetSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY, 0, GC_GetSwitchOffDelay, 0);
		}
		else if(GC_TIMER_IS_OUT(SEC_TMR_ID_SWITCH_OFF_DELAY))
		{
			int iToCloseRectType;
			int iRectNo = FindLowEffiMaxRuntimeRect(&iToCloseRectType);

			if(iRectNo != GC_NO_RECT)
			{
				//printf("Close Rectifier:Type=%d, No=%d\n", iToCloseRectType, iRectNo);
				SwitchOffOneRect(iRectNo, iToCloseRectType);

				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					RT_PUB_ES_ACTIVE_SET,
					GC_RECT_REDUND_ACTIVE,
					TRUE);

				//�򿪺�ɼ�ʱ��
				if(GC_TIMER_IS_STOP(SEC_TMR_ID_RECT_CYCLE))
				{
					GC_SetSecTimer(SEC_TMR_ID_RECT_CYCLE, 0, GC_GetCycPeriod, 0);
				}
				//printf("need to close rectifier\n");
			}
			GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);


		}
	}
	else
	{
		TRACE("Need NOT Close Rect. \n");
		GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);
		//printf("Not need to close rectifier\n");
	}

	/* End */




	//printf("----------OUT--------------\n");
	return;
}


/*==========================================================================*
 * FUNCTION : RedundSwitch
 * PURPOSE  : Redundant Rectifier Switch ON/OFF
 * CALLS    : GetMaxRuntimeNo GetMinRuntimeNo GC_GetFloatValue 
              GC_SetEnumValue GC_GetStateOfSecTimer GC_SetSecTimer
 * CALLED BY: GC_RectRedund
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-16 15:14
 *==========================================================================*/
//#define	FLUCTUATION_RATE	0.10
static void RedundSwitch(void)
{
	static	int s_iRedundSwitchCounter = 0;

	float	fActualCap/*, fMinRedundancy, fMaxRedundancy*/;
	float	fRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	float	fBestEfficiency 
		= GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_BEST_EFFICIENCY);
	//float	fSwitchOnOneLoad;
	float	fSwitchOnAllLoad;
	float	fSwitchOffOneLoad;
	float	fRectTotalCurr = g_pGcData->RunInfo.Rt.fSumOfCurr;
	int	iRectType;

	fActualCap = fRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect;
	/*fMinRedundancy = (100.0 * fRectTotalCurr / fBestEfficiency
			- fRectTotalCurr) / fRatedCurr;
	fMaxRedundancy = fMinRedundancy + 1.10;
	fSwitchOnOneLoad = fMinRedundancy * fRatedCurr + fRectTotalCurr;
	fSwitchOnAllLoad = fRatedCurr * (fMinRedundancy - 1.0) + fRectTotalCurr;
	fSwitchOffOneLoad = fMaxRedundancy * fRatedCurr + fRectTotalCurr;*/

	fSwitchOffOneLoad = 100.0 * fRectTotalCurr / fBestEfficiency			//best capacity
		+ fRectTotalCurr 
			* GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_ES_FLUCT_RANGE) / 100	//Fluctuation load
		+ fRatedCurr;														//One rectifer capacity

	fSwitchOnAllLoad = 100.0 * fRectTotalCurr / fBestEfficiency;			//best capacity

	/*printf("fActualCap = %.02f, fSwitchOffOneLoad = %.02f, fSwitchOnAllLoad = %.02f\n", 
				fActualCap, fSwitchOffOneLoad, fSwitchOnAllLoad);*/

	/*ASSERT((fSwitchOffOneLoad > fSwitchOnOneLoad) 
		&& (fSwitchOnOneLoad > fSwitchOnAllLoad));*/

	if(fActualCap > fSwitchOffOneLoad)
	{
		s_iRedundSwitchCounter = 0;
		if(SEC_TMR_SUSPENDED 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY))
		{
			//SEC_TMR_ID_SWITCH_OFF_DELAY Unit : Second
			GC_SetSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY, 
							0,
							GC_GetSwitchOffDelay,
							0);		
		}

		if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY))
		{
			//int iMaxRuntimeNo = GetMaxRuntimeNo(&iRectType);
			int iMaxRuntimeNo = FindLowEffiMaxRuntimeRect(&iRectType);

			if(iMaxRuntimeNo != GC_NO_RECT)
			{
				SwitchOffOneRect(iMaxRuntimeNo, iRectType);
				printf("SwitchOffOneRect, iMaxRuntimeNo = %d\n", iMaxRuntimeNo);
			}
			else
			{
				printf("GC_NO_RECTSwitchOffOneRect, iMaxRuntimeNo = %d\n", iMaxRuntimeNo);
			}

			GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);
		}
	}
	else
	{
		GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_OFF_DELAY);	

		if(fActualCap < fSwitchOnAllLoad)
		{
			if(s_iRedundSwitchCounter > 2)
			{
				SwitchOnAllRect(FALSE);
				/*printf("SwitchOnAllRect, cause fActualCap < fSwitchOnAllLoad.\n");*/

				GC_SuspendSecTimer(SEC_TMR_ID_RECT_CYCLE);
			}
			else
			{
				s_iRedundSwitchCounter++;
			}
		}
		//else if(fActualCap < fSwitchOnOneLoad)
		//{
		//	int iMinRuntimeNo = GetMinRuntimeNo();

		//	if(iMinRuntimeNo != GC_NO_RECT)
		//	{
		//		SwitchOnOneRect(iMinRuntimeNo);
		//		//printf("SwitchOnOneRect, iMinRuntimeNo = %d\n", iMinRuntimeNo);
		//	}

		//	s_iRedundSwitchCounter = 0;	
		//}
	}
	return;
}


/*==========================================================================*
 * FUNCTION : CyclingSwitch
 * PURPOSE  : Rectifier Cycling
 * CALLS    : GetFstInOnList GetFstInOffList GC_GetStateOfSecTimer
              GC_SetEnumValue GC_SetSecTimer GC_SuspendSecTimer
 * CALLED BY: GC_RectRedund
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-16 15:15
 *==========================================================================*/
static void CyclingSwitch(void)
{
	int iRectType;
	if(SEC_TMR_SUSPENDED == GC_GetStateOfSecTimer(SEC_TMR_ID_SWITCH_DELAY))
	{
		if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_RECT_CYCLE))
		{
			if(IsSwitchTime())
			{
				int iFstInOnList = GetFstInOnList(&iRectType);
				int iFstInOffList = GetFstInOffList(&iRectType);

				if((iFstInOnList != GC_NO_RECT) 
					&& (iFstInOffList != GC_NO_RECT))
				{
					SwitchOnOneRect(iFstInOffList, iRectType);
					//printf("SwitchOnOneRect in CyclingSwitch, iFstInOffList = %d\n", iFstInOffList);
				
					GC_SetSecTimer(SEC_TMR_ID_SWITCH_DELAY, 
									0,
									GC_GetSwitchOffDelay,
									0);

					GC_SuspendSecTimer(SEC_TMR_ID_RECT_CYCLE);

					return;
				}

			}
		}
	}

	if(SEC_TMR_TIME_OUT == GC_GetStateOfSecTimer(SEC_TMR_ID_SWITCH_DELAY))
	{
		int iFstInOnList = GetFstInOnList(&iRectType);

		if(iFstInOnList != GC_NO_RECT)
		{
			SwitchOffOneRect(iFstInOnList, iRectType);
			//printf("SwitchOffOneRect in CyclingSwitch, iFstInOnList = %d\n", iFstInOnList);

		}
		GC_SuspendSecTimer(SEC_TMR_ID_SWITCH_DELAY);
	}

	return;
}



/*==========================================================================*
 * FUNCTION : GetMaxRuntimeNo
 * PURPOSE  : Get number of rectifier whose runtime is maximum in switch-on 
              rectifier list
 * CALLS    : List_GotoHead List_Get List_GotoNext
 * CALLED BY: RedundSwitch
 * ARGUMENTS: 
 * RETURN   : int 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-16 15:20
 *==========================================================================*/
static int GetMaxRuntimeNo(int *iRectType)
{
	DWORD			dwMaxRuntime = 0;
	int				iRectNo = GC_NO_RECT;
	GC_RECT_INFO	RectifierInfo;

	//List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);

	//do
	//{
	//	if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
	//				(void*)(&RectifierInfo)))
	//	{
	//		if(dwMaxRuntime <= RectifierInfo.dwOperTime)
	//		{
	//			iRectNo = RectifierInfo.iRectNo;
	//			dwMaxRuntime = RectifierInfo.dwOperTime;
	//			*iRectType = RectifierInfo.iRectEquipType;
	//		}
	//		printf("dwMaxRuntime = [Rectifier%d].dwOperTime; %d\n",RectifierInfo.iRectNo, RectifierInfo.dwOperTime);
	//	}

	//}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));	

	//Change to the running time calculated by controllers.
	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);

	do
	{
		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
					(void*)(&RectifierInfo)))
		{
			if(dwMaxRuntime <= RectifierInfo.dwSelfOperTime)
			{
				iRectNo = RectifierInfo.iRectNo;
				dwMaxRuntime = RectifierInfo.dwSelfOperTime;
				*iRectType = RectifierInfo.iRectEquipType;
			}
			printf("dwMaxRuntime = [Rectifier%d].dwSelfOperTime; %d\n",RectifierInfo.iRectNo, RectifierInfo.dwSelfOperTime);
		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));	

	return iRectNo;
}


static int 	FindLowEffiMaxRuntimeRect(int *iRectType)
{
	DWORD			dwMaxRuntime = 0;
	int				iRectEffiType;
	int				iMinRectEffiType = 255;
	int				iRectNo = GC_NO_RECT;
	GC_RECT_INFO	RectifierInfo;
	BOOL				bExistSlaveRect = FALSE;

	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);

	do
	{
		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, (void*)(&RectifierInfo)))
		{
			if(RectifierInfo.iRectEquipType == RECT_TYPE_SELF)
			{
				continue;
				//iRectEffiType = (int)GC_GetEnumValue(TYPE_RECT_UNIT , RectifierInfo.iRectNo, RT_PRI_EFFICIENCY_NO);
			}
			else
			{
				iRectEffiType = (int)GC_IsGCEquipExist(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType,
					RectifierInfo.iRectNo, S1_PRI_EFFICIENCY_NO,GC_RECT_INVALID);
				if(iRectEffiType == GC_RECT_INVALID)
				{
					continue;
				}
			}

			TRACE("RectNo = %d, EffiType = %d, Runtime = %d\n", RectifierInfo.iRectNo, iRectEffiType, RectifierInfo.dwOperTime);

			if ((iRectEffiType < iMinRectEffiType)
				|| ((iRectEffiType == iMinRectEffiType) && (RectifierInfo.dwSelfOperTime > dwMaxRuntime)))
			{
				iMinRectEffiType = iRectEffiType;
				dwMaxRuntime = RectifierInfo.dwSelfOperTime;

				iRectNo = RectifierInfo.iRectNo;
				*iRectType = RectifierInfo.iRectEquipType;
				TRACE("Select Rect:Type=%d, No=%d\n", *iRectType, iRectNo);
				bExistSlaveRect = TRUE;
			}
		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));	

	if(bExistSlaveRect)
	{
		return iRectNo;
	}

	//Close slave rect first. If there is no slave rect that can not be closed, close master rect.

	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);

	do
	{
		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, (void*)(&RectifierInfo)))
		{
			if(RectifierInfo.iRectEquipType == RECT_TYPE_SELF)
			{
				iRectEffiType = (int)GC_GetEnumValue(TYPE_RECT_UNIT , RectifierInfo.iRectNo, RT_PRI_EFFICIENCY_NO);
			}
			else
			{
				continue;
				//iRectEffiType = (int)GC_GetEnumValue(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType, RectifierInfo.iRectNo, S1_PRI_EFFICIENCY_NO);
			}

			//printf("RectNo = %d, EffiType = %d, Runtime = %d\n", RectifierInfo.iRectNo, iRectEffiType, RectifierInfo.dwSelfOperTime);

			if ((iRectEffiType < iMinRectEffiType)
				|| ((iRectEffiType == iMinRectEffiType) && (RectifierInfo.dwSelfOperTime > dwMaxRuntime)))
			{
				iMinRectEffiType = iRectEffiType;
				dwMaxRuntime = RectifierInfo.dwSelfOperTime;

				iRectNo = RectifierInfo.iRectNo;
				*iRectType = RectifierInfo.iRectEquipType;
				TRACE("Select Rect:Type=%d, No=%d\n", *iRectType, iRectNo);
			}
		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));	

	return iRectNo;

}

/*==========================================================================*
 * FUNCTION : GetMinRuntimeNo
 * PURPOSE  : Get number of rectifier whose runtime is minimum in switch-off 
              rectifier list
 * CALLS    : List_GotoHead List_Get List_GotoNext
 * CALLED BY: RedundSwitch
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-16 15:24
 *==========================================================================*/
static int GetMinRuntimeNo(int *iRectType)
{
	DWORD			dwMinRuntime = 0xFFFFFFFF;
	int				iRectNo = GC_NO_RECT;
	GC_RECT_INFO	RectifierInfo;

	int			aiRTFaultSid[] = {RT_PRI_FAULT, 
									S1_PRI_FAULT,
									S2_PRI_FAULT,
									S3_PRI_FAULT,};

	int			aiRTProtectionSid[] = {RT_PRI_PROTCTION, 
								S1_PRI_PROTCTION,
								S2_PRI_PROTCTION,
								S3_PRI_PROTCTION,};

	int			aiRTFailSid[] = {RT_PRI_AC_FAIL, 
							S1_PRI_AC_FAIL,
							S2_PRI_AC_FAIL,
							S3_PRI_AC_FAIL,};


	List_GotoHead(g_pGcData->RunInfo.Rt.hListOffWorkRect);

	do
	{
		if(List_Get(g_pGcData->RunInfo.Rt.hListOffWorkRect, 
					(void*)(&RectifierInfo)))
		{
			if((GC_NORMAL == GC_IsGCEquipExist(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType,
											RectifierInfo.iRectNo,
											aiRTFailSid[RectifierInfo.iRectEquipType],
											GC_RECT_INVALID))
				&& (GC_NORMAL == GC_IsGCEquipExist(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType,
											RectifierInfo.iRectNo,
											aiRTProtectionSid[RectifierInfo.iRectEquipType],
											GC_RECT_INVALID))
				&& (GC_NORMAL == GC_IsGCEquipExist(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType,
											RectifierInfo.iRectNo,
											aiRTFailSid[RectifierInfo.iRectEquipType],
											GC_RECT_INVALID)))
			{
				if(dwMinRuntime > RectifierInfo.dwSelfOperTime)
				{
					iRectNo = RectifierInfo.iRectNo;
					dwMinRuntime = RectifierInfo.dwSelfOperTime;
					*iRectType = RectifierInfo.iRectEquipType;
				}
			}
		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOffWorkRect));	

	return iRectNo;
}


/*==========================================================================*
 * FUNCTION : GetFstInOnList
 * PURPOSE  : Get the number of first rectifier in switch-on rectifier list
 * CALLS    : List_GotoTail List_Get
 * CALLED BY: CyclingSwitch
 * ARGUMENTS:  
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-16 15:25
 *==========================================================================*/
static int GetFstInOnList(int *iRectType)
{
	GC_RECT_INFO	RectifierInfo;

	if(List_GotoTail(g_pGcData->RunInfo.Rt.hListOnWorkRect))
	{
		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
					(void*)(&RectifierInfo)))
		{
			*iRectType = RectifierInfo.iRectEquipType;
			return RectifierInfo.iRectNo;
		}	
	}
	return GC_NO_RECT;
}


/*==========================================================================*
 * FUNCTION : GetFstInOffList
 * PURPOSE  : Get the number of first rectifier in switch-off rectifier list
 * CALLS    : List_GotoTail List_Get
 * CALLED BY: CyclingSwitch
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-16 15:35
 *==========================================================================*/
static int GetFstInOffList(int *iRectType)
{
	GC_RECT_INFO	RectifierInfo;

	int			aiRTFaultSid[] = {RT_PRI_FAULT, 
								S1_PRI_FAULT,
								S2_PRI_FAULT,
								S3_PRI_FAULT,};

	int			aiRTProtectionSid[] = {RT_PRI_PROTCTION, 
								S1_PRI_PROTCTION,
								S2_PRI_PROTCTION,
								S3_PRI_PROTCTION,};

	int			aiRTFailSid[] = {RT_PRI_AC_FAIL, 
							S1_PRI_AC_FAIL,  //[ID51=Normal]/[ID95=Low]/[ID52=Failure]
							S2_PRI_AC_FAIL,
							S3_PRI_AC_FAIL,};


	List_GotoTail(g_pGcData->RunInfo.Rt.hListOffWorkRect);

	do
	{
		if(List_Get(g_pGcData->RunInfo.Rt.hListOffWorkRect, 
					(void*)(&RectifierInfo)))
		{
			if((GC_NORMAL == GC_IsGCEquipExist(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType,
											RectifierInfo.iRectNo,
											aiRTFaultSid[RectifierInfo.iRectEquipType],
											GC_RECT_INVALID))
				&& (GC_NORMAL == GC_IsGCEquipExist(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType,
											RectifierInfo.iRectNo,
											aiRTProtectionSid[RectifierInfo.iRectEquipType],
											GC_RECT_INVALID))
				&& (GC_NORMAL ==GC_IsGCEquipExist(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType,
											RectifierInfo.iRectNo,
											aiRTFailSid[RectifierInfo.iRectEquipType],
											GC_RECT_INVALID)))
			{
				*iRectType = RectifierInfo.iRectEquipType;
				return RectifierInfo.iRectNo;
			}
		}
	}while(List_GotoPrev(g_pGcData->RunInfo.Rt.hListOffWorkRect));

	return GC_NO_RECT;
}


/*==========================================================================*
 * FUNCTION : IsSwitchTime
 * PURPOSE  : To judge if it's time for a switch rectifier
 * CALLS    : gmtime_r GC_GetdWordValue
 * CALLED BY: CyclingSwitch
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-11 11:29
 *==========================================================================*/
static BOOL	IsSwitchTime(void)
{
	/*struct tm		tmNow;
	time_t			tLocalTime;

	tLocalTime = g_pGcData->SecTimer.tTimeNow + (SECONDS_PER_HOUR / 2)
			* GC_GetLongValue(TYPE_OTHER, 0, GC_PUB_TIME_ZONE);

	gmtime_r(&tLocalTime, &tmNow);

	return (int)GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_ACTIVE_CLK) 
				== tmNow.tm_hour;*/
	return TRUE;
}


/*==========================================================================*
 * FUNCTION : GC_GetCycPeriod
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdx : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-19 14:40
 *==========================================================================*/
int GC_GetCycPeriod(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_HOUR
		* (int)GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_REDU_CLC_PERD);
}



/*==========================================================================*
 * FUNCTION : GC_GetSwitchOffDelay
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdx : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-19 14:40
 *==========================================================================*/
int GC_GetSwitchOffDelay(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN 
		* GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_AC_OFF_DLY);
}


int GC_GetRectCurrCalcDelay(int iIdx)
{
	UNUSED(iIdx);
	// the maxium of rcterfiers is 100, total delay time 18+5=23,that < 30S
	// return g_pGcData->RunInfo.Rt.iQtyOfRectSelf * 2 + 5;
	return (int)(g_pGcData->RunInfo.Rt.iQtyOfRect * 0.18 + 5);
}

static int GC_GetRectDryingTime(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN
		* (int)GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_RECT_DRYING_TIME);
}

/*==========================================================================*
 * FUNCTION : SwitchOnAllRect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-19 14:40
 *==========================================================================*/
static void SwitchOnAllRect(BOOL bNeedPreCurrentLimit)
{
	static	BOOL	s_LastAcFailState = FALSE;
	int				i;
	
	if((g_pGcData->RunInfo.Rt.bAcFail && (s_LastAcFailState == FALSE))
		|| (g_pGcData->RunInfo.Rt.iQtyAcOffRect))
	{
		char	strOut[256];
		int iExpectedQtyOfRect 
			= g_pGcData->RunInfo.Rt.iQtyOnWorkRect 
				+ g_pGcData->RunInfo.Rt.iQtyAcOffRect - g_pGcData->RunInfo.Rt.iQtyNoOutputRect; 
		
		if(/*(GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_PCL_ENB))
			&& */bNeedPreCurrentLimit)
		{
			GC_PreCurrLmt(iExpectedQtyOfRect, FALSE, FALSE);
			/*printf("GC_PreCurrLmt in SwitchOnAllRect, time = %d, iQtyOnWorkRect = %d, iQtyAcOffRect = %d\n",
				time(NULL),
				g_pGcData->RunInfo.Rt.iQtyOnWorkRect, 
				g_pGcData->RunInfo.Rt.iQtyAcOffRect);*/
			
			/*GC_SetSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY, 
						0,
						GC_GetPreCurrLmtDelay,
						0);	*/
		}


		sprintf(strOut, 
				"Switch-on all of rectifers. QtyOnWorkRect = %d, QtyAcOffRect = %d.\n", 
				g_pGcData->RunInfo.Rt.iQtyOnWorkRect,
				g_pGcData->RunInfo.Rt.iQtyAcOffRect);
		GC_LOG_OUT(APP_LOG_INFO, strOut);

		for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
		{

			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							RT_PUB_AC_ON_OFF_CTL_SELF + i,
							GC_RECT_ON,
							TRUE);

			GC_SetEnumValue(TYPE_OTHER, 
							0, 
							RT_PUB_DC_ON_OFF_CTL_SELF + i,
							GC_RECT_ON,
							TRUE);
		}

		GC_SetSecTimer(SEC_TMR_ID_RECT_CURR,
						0,
						GC_GetRectCurrCalcDelay,
						0);	
	}

	s_LastAcFailState = g_pGcData->RunInfo.Rt.bAcFail;

	return;
}

/*==========================================================================*
 * FUNCTION : SwitchOnOneRect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iRectNo : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-19 14:40
 *==========================================================================*/
static void SwitchOnOneRect(int iRectNo, int iRectType)
{

	int iExpectedQtyOfRect = g_pGcData->RunInfo.Rt.iQtyOnWorkRect + 1;

	int			aiRTRectIDSid[] = {RT_PRI_RECT_ID, 
									S1_PRI_RECT_ID,
									S2_PRI_RECT_ID,
									S3_PRI_RECT_ID,};
	int			aiRTRectIDHiId[] = {RT_PRI_RECT_HI_ID, 
									S1_PRI_RECT_HI_ID,
									S2_PRI_RECT_HI_ID,
									S3_PRI_RECT_HI_ID,};

	int			aiRTRectACONSid[] = {RT_PRI_AC_ON_OFF_CTL, 
									S1_PRI_AC_ON_OFF_CTL,
									S2_PRI_AC_ON_OFF_CTL,
									S3_PRI_AC_ON_OFF_CTL,};
	int			aiRTRectDCONSid[] = {RT_PRI_DC_ON_OFF_CTL, 
									S1_PRI_DC_ON_OFF_CTL,
									S2_PRI_DC_ON_OFF_CTL,
									S3_PRI_DC_ON_OFF_CTL,};


	if(SEC_TMR_TIME_IN 
		!= GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY))
	{
		char	strOut[256];
		char	strRectId[100];
		//if(GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_PCL_ENB))
		//{
		//	GC_PreCurrLmt(iExpectedQtyOfRect, FALSE);


		//	/*GC_SetSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY, 
		//				0,
		//				GC_GetPreCurrLmtDelay,
		//				0);*/	
		//}
		DWORD dwRectHighID = GC_GetDwordValue(TYPE_RECT_UNIT + iRectType, 
			iRectNo,
			aiRTRectIDHiId[TYPE_RECT_UNIT + iRectType]);

		GetRectIdStr(GC_GetDwordValue(TYPE_RECT_UNIT + iRectType, 
									iRectNo,
									aiRTRectIDSid[TYPE_RECT_UNIT + iRectType]),
					&dwRectHighID,
					strRectId);

		sprintf(strOut, 
				"Switch-on the slave %d rectifer %d, Rectifier ID = %s. \n", 
				TYPE_RECT_UNIT + iRectType,
				iRectNo + 1,
				strRectId);

		GC_LOG_OUT(APP_LOG_INFO, strOut);
        
		GC_SetEnumValue(TYPE_RECT_UNIT + iRectType, 
						iRectNo,
						aiRTRectACONSid[TYPE_RECT_UNIT + iRectType],
						GC_RECT_ON,
						TRUE);

		GC_SetEnumValue(TYPE_RECT_UNIT + iRectType, 
						iRectNo,
						aiRTRectDCONSid[TYPE_RECT_UNIT + iRectType],
						GC_RECT_ON,
						TRUE);

		GC_SetSecTimer(SEC_TMR_ID_RECT_CURR,
						0,
						GC_GetRectCurrCalcDelay,
						0);	
	}
	return;
}


/*==========================================================================*
 * FUNCTION : SwitchOffOneRect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iRectNo : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-19 14:40
 *==========================================================================*/
static void SwitchOffOneRect(int iRectNo, int iRectType)
{

	char	strOut[256];
	char	strRectId[100];

	int			aiRTRectIDSid[] = {RT_PRI_RECT_ID, 
									S1_PRI_RECT_ID,
									S2_PRI_RECT_ID,
									S3_PRI_RECT_ID,};
	int			aiRTRectIDHiId[] = {RT_PRI_RECT_HI_ID, 
						S1_PRI_RECT_HI_ID,
						S2_PRI_RECT_HI_ID,
						S3_PRI_RECT_HI_ID,};

	int			aiRTRectACONSid[] = {RT_PRI_AC_ON_OFF_CTL, 
									S1_PRI_AC_ON_OFF_CTL,
									S2_PRI_AC_ON_OFF_CTL,
									S3_PRI_AC_ON_OFF_CTL,};

	DWORD dwRectHighID = GC_GetDwordValue(TYPE_RECT_UNIT + iRectType, 
						iRectNo,
						aiRTRectIDHiId[TYPE_RECT_UNIT + iRectType]);
	GetRectIdStr(GC_GetDwordValue(TYPE_RECT_UNIT + iRectType, 
									iRectNo,
									aiRTRectIDSid[iRectType]),
					&dwRectHighID,
					strRectId);


	sprintf(strOut, 
		"Switch-off the slave %d rectifer %d, Rectifier ID = %s. \n", 
		TYPE_RECT_UNIT + iRectType,
		iRectNo + 1,
		strRectId);

	GC_LOG_OUT(APP_LOG_INFO, strOut);

	GC_SetEnumValue(TYPE_RECT_UNIT + iRectType, 
					iRectNo,
					aiRTRectACONSid[iRectType],
					GC_RECT_OFF,
					TRUE);

	/*GC_SetEnumValue(TYPE_RECT_UNIT, 
					iRectNo,
					RT_PRI_DC_ON_OFF_CTL,
					GC_RECT_OFF,
					TRUE);*/
	
	//GC_SetEnumValue(TYPE_OTHER, 0, RT_PUB_ES_ACTIVE_SET, GC_ALARM, TRUE);
	

	GC_SetSecTimer(SEC_TMR_ID_RECT_CURR,
					0,
					GC_GetRectCurrCalcDelay,
					0);	

	GC_SetSecTimer(SEC_TMR_ID_SWITCH_ON_DELAY,
					DELAY_AFTER_SWITCH_OFF,
					NULL,
					0);	


	return;
}

static char *GetRectIdStr(DWORD dwRectId, DWORD *dwRectHiId,
							 char *pStrOut)
{
	sprintf(pStrOut, "%02u%02d%02d%05d", 
			*dwRectHiId,
			((int)((dwRectId >> 24) & 0x1F)),
			((int)((dwRectId >> 16) & 0x0F)),
			((int)(dwRectId & 0xffff)));

	return pStrOut;
}



static BOOL IsDiselRunning(void)
{
	/*
	if( GC_IsSigValid(TYPE_OTHER, 0, DSL_PUB_STATUS) 
		  && DSL_RUNNING == GC_GetEnumValue(TYPE_OTHER, 0, DSL_PUB_STATUS))
	{
		return TRUE;
	}
	return FALSE;
	*/
	return FALSE;
}


static BOOL IsRectHaveOtherAlarm(void)
{
	int			i, j ;
	
	for(j = 0; j < MAX_RECT_TYPE_NUM; j++)
	{
		for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[j].iQtyOfRect; i++)
		{
			if( j == RECT_TYPE_SELF)
			{
				if((GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT  , i, RT_PRI_OVER_TEMP))
					|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT  , i, RT_PRI_AC_FAIL))
					|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT , i, RT_PRI_FAULT))
					|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_OVER_VOLT))
					//|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT + j, i, RT_PRI_PROTCTION))
					|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_FAN_FAULT))
					//|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_POWER_LMT_RECT))
					|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_NO_RESPOND_STATE)))
				{
					return TRUE;
				}
			}
			else
			{
				if((GC_NORMAL != GC_IsGCEquipExist(TYPE_RECT_UNIT + j  , i, S1_PRI_OVER_TEMP,GC_RECT_INVALID))
					|| (GC_NORMAL != GC_IsGCEquipExist(TYPE_RECT_UNIT + j  , i, S1_PRI_AC_FAIL,GC_RECT_INVALID))
					|| (GC_NORMAL != GC_IsGCEquipExist(TYPE_RECT_UNIT  + j, i, S1_PRI_FAULT,GC_RECT_INVALID))
					|| (GC_NORMAL != GC_IsGCEquipExist(TYPE_RECT_UNIT  + j, i, S1_PRI_OVER_VOLT,GC_RECT_INVALID))
					//|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT + j, i, RT_PRI_PROTCTION))
					|| (GC_NORMAL != GC_IsGCEquipExist(TYPE_RECT_UNIT  + j, i, S1_PRI_FAN_FAULT,GC_RECT_INVALID))
					//|| (GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_POWER_LMT_RECT))
					|| (GC_NORMAL != GC_IsGCEquipExist(TYPE_RECT_UNIT  + j, i, S1_PRI_NO_RESPOND_STATE,GC_RECT_INVALID)))
				{
					return TRUE;
				}
			}
		}
	}

	return FALSE;
	
}



#define	MIN_DISCHARGE_CURR_COEF		(-0.003)
#define	MIN_CHARGE_CURR_COEF		0.005
/*==========================================================================*
 * FUNCTION : IsChargeOrDischarge
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-05-14 16:07
 *==========================================================================*/
#define	MIN_CHARGEO_GURRENT		(5.0)
#define MIN_DISCHARGE_CURRENT		(-2.0)
static BOOL IsChargeOrDischarge(void)
{
	int			i;
	static		int  s_iDelayTimes = 0;
    
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(!(g_pGcData->RunInfo.Bt.abValid[i]) || !(g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i]))
		{
			continue;
		}
		else
		{
			float fRatedCap;
			BOOL bRTN;
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i ,BT_PRI_RATED_CAP,&fRatedCap);
			if(bRTN == FALSE)
			{
				continue;
			}
			float fMinDischargeCurr = MIN_DISCHARGE_CURR_COEF* fRatedCap;
			float fMinChargeCurr = MIN_CHARGE_CURR_COEF* fRatedCap;
			if(fMinChargeCurr < MIN_CHARGEO_GURRENT)
			{
				fMinChargeCurr = MIN_CHARGEO_GURRENT;
			}

			if(fMinDischargeCurr > MIN_DISCHARGE_CURRENT)
			{
				fMinDischargeCurr = MIN_DISCHARGE_CURRENT;
			}


			//Battery current
			if((g_pGcData->RunInfo.Bt.afBattCurr[i] < fMinDischargeCurr)
				|| (g_pGcData->RunInfo.Bt.afBattCurr[i] > fMinChargeCurr))
			{
				/*printf("Is in Charge Or Discharge!!!\n");*/
				//s_iDelayTimes++;
				//if(s_iDelayTimes < 6)
				//{
				//	return FALSE;
				//}
				//else
				//{					
					return TRUE;
				//}
				
			}
		}
	}
	s_iDelayTimes = 0;
	return FALSE;
}

#define	SIZE_OF_DEAD_AREA	5
/*==========================================================================*
 * FUNCTION : IsOverEnergeSavingPoint
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-05-14 16:52
 *==========================================================================*/
static BOOL IsOverEnergeSavingPoint(void)
{
	static	BOOL	s_bOver = TRUE;

	float	fHighLimit = g_pGcData->RunInfo.Rt.fRatedCurr
							* g_pGcData->RunInfo.Rt.iQtyOfRect
							* (GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_ES_SYSTEM_POINT) 
								+ SIZE_OF_DEAD_AREA)
							/ 100;
	float	fLowLimit = g_pGcData->RunInfo.Rt.fRatedCurr
							* g_pGcData->RunInfo.Rt.iQtyOfRect
							* GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_ES_SYSTEM_POINT) 
							/ 100;

	if(g_pGcData->RunInfo.Rt.fSumOfCurr > fHighLimit)
	{
		s_bOver = TRUE;
	}
	else if(g_pGcData->RunInfo.Rt.fSumOfCurr < fLowLimit)
	{
		s_bOver = FALSE;	
	}
	
	/*if(s_bOver)
	{
		printf("Is Over Energe Saving Point!!! Rt.fSumOfCurr = %.02f, fHighLimit = %.02f, fLowLimit = %.02f\n",
			g_pGcData->RunInfo.Rt.fSumOfCurr,
			fHighLimit,
			fLowLimit);
	}*/

	return s_bOver;
}

static BOOL 	NeedOpenOneRect(void)
{
	static	int s_iRedundSwitchCounter = 0;

	float	fActualCap/*, fMinRedundancy, fMaxRedundancy*/;
	float	fRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	float	fBestEfficiency 
		= GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_BEST_EFFICIENCY);
	//float	fSwitchOnOneLoad;
	float	fSwitchOnAllLoad;
	float	fSwitchOffOneLoad;
	float	fRectTotalCurr = g_pGcData->RunInfo.Rt.fSumOfCurr;
	int		iRectType;

	fActualCap = fRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect;

	fSwitchOffOneLoad = 100.0 * fRectTotalCurr / fBestEfficiency			//best capacity
		+ fRectTotalCurr 
		* GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_ES_FLUCT_RANGE) / 100	//Fluctuation load
		+ fRatedCurr;														//One rectifer capacity

	fSwitchOnAllLoad = 100.0 * fRectTotalCurr / fBestEfficiency;			//best capacity

	return 	fActualCap < fSwitchOnAllLoad;

}

static BOOL 	NeedCloseOneRect(void)
{
	static	int s_iRedundSwitchCounter = 0;

	float	fActualCap/*, fMinRedundancy, fMaxRedundancy*/;
	float	fRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	float	fBestEfficiency 
		= GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_BEST_EFFICIENCY);
	//float	fSwitchOnOneLoad;
	float	fSwitchOnAllLoad;
	float	fSwitchOffOneLoad;
	float	fRectTotalCurr = g_pGcData->RunInfo.Rt.fSumOfCurr;
	int		iRectType;

	fActualCap = fRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect;

	fSwitchOffOneLoad = 100.0 * fRectTotalCurr / fBestEfficiency			//best capacity
		+ fRectTotalCurr 
		* GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_ES_FLUCT_RANGE) / 100	//Fluctuation load
		+ fRatedCurr;														//One rectifier capacity

	fSwitchOnAllLoad = 100.0 * fRectTotalCurr / fBestEfficiency;			//best capacity

	return	fActualCap > fSwitchOffOneLoad;
}


void GC_RectHVSD(void)
{
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	if(GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_HVSD_ENABLE) != GC_ENABLED)
	{
		return;
	}
	
	// No Battery test mode
	SIG_ENUM	eBMState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);
	if((eBMState == ST_SHORT_TEST)
		|| (eBMState == ST_MAN_TEST)
		|| (eBMState == ST_PLAN_TEST)
		|| (eBMState == ST_AC_FAIL_TEST)
		|| (eBMState == ST_POWER_SPLIT_BT))
	{
		return;
	}

	int		i;
	int		j;
	int		iExpectedQtyOfRect;
	static	BOOL	s_bLastHVSD = FALSE;
	static	int		s_iHVSDRectID = -1;

	float	fRectCtrlVolt ;
	float	fHVSDVoltDiff ;
	BOOL bRTN;


	//if(g_pGcData->RunInfo.Rt.fRatedVolt < GC_SYSTEMVOLT_JUDGE)
	if(stVoltLevelState)//24V
	{
		fRectCtrlVolt = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DC_VOLT_CTL_24V);

		 if(GC_IsGCSigExist(TYPE_OTHER, 0, RT_PUB_HVSD_VOLT_DIFF_24V))	
		{
			fHVSDVoltDiff = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_HVSD_VOLT_DIFF_24V);
	
		 }
		 else
		 {
			fHVSDVoltDiff = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_HVSD_VOLT_DIFF);//����ACU+����

		 }
	}
	else
	{
		fRectCtrlVolt = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DC_VOLT_CTL);
		fHVSDVoltDiff = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_HVSD_VOLT_DIFF);
	}
	

	GC_RUN_INFO_RECT	*pRtRunInfo = &(g_pGcData->RunInfo.Rt);

	// Calculate System voltage for more times before HVSD handle
	g_pGcData->RunInfo.fSysVolt = GC_GetSysVolt();
	GC_SetFloatValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT, g_pGcData->RunInfo.fSysVolt);

	if((pRtRunInfo->iQtyOnWorkRect > 0)
		&& (g_pGcData->RunInfo.fSysVolt - fRectCtrlVolt >= fHVSDVoltDiff))
	{
		TRACE("[GC] Detect the voltage difference between bus bar and control!\n");
		if ((!s_bLastHVSD) && (s_iHVSDRectID < 0))
		{
			GC_LOG_OUT(APP_LOG_WARNING, "The system voltage is much more than rectifier control volt, rect maybe fail.\n");
		}

		// Switch on all rectifiers before close HVSD rect
		if((!pRtRunInfo->bRectHVSD)
			&& (g_pGcData->RunInfo.Rt.iQtyAcOffRect > 0))
		{
			SwitchOnAllRect(TRUE);
			GC_LOG_OUT(APP_LOG_INFO, "Switch on all before close HVSD rect.\n");
		}

		//Handle Rect HVSD
		float fRectVolt;
		char	strOut[256];
		char	strRectId[100];
		DWORD	dwRectId, dwRectHiId;
		SIG_ENUM	eRectACStatus;
		
		for(j = 0;j < MAX_RECT_TYPE_NUM;j++)//��������ģ��
		{
			for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[j].iQtyOfRect; i++)			
			{
				if(pRtRunInfo->Slave[j].abComm[i])
				{
					g_pGcData->RunInfo.fSysVolt = GC_GetSysVolt();
					if(j == 0)
					{
						fRectVolt = GC_GetFloatValue(TYPE_RECT_UNIT, i, RT_PRI_OUT_VOLT);
						eRectACStatus = GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_ON_OFF_STA);
					}
					else
					{
						fRectVolt = GC_GetFloatValue(TYPE_RECT_UNIT + j, i, S1_PRI_OUT_VOLT);
						eRectACStatus = GC_IsGCEquipExist(TYPE_RECT_UNIT + j, i, S1_PRI_AC_ON_OFF_STA,GC_RECT_INVALID);
						if(eRectACStatus ==GC_RECT_INVALID)
						{
							continue;
						}
					}

					
					if((g_pGcData->RunInfo.fSysVolt - fRectVolt >= fHVSDVoltDiff)
						&& (eRectACStatus == GC_NORMAL))
					{
						if(s_iHVSDRectID == i)
						{
							TRACE("[GC] Close Rect%d because of HVSD.\n", i);
							if(j == 0)
							{
								GC_SetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_ON_OFF_CTL,
									GC_RECT_OFF, TRUE);
								SetRectHVSDstatus(i, TRUE);

								dwRectId = GC_GetDwordValue(TYPE_RECT_UNIT, i, RT_PRI_RECT_ID);
								dwRectHiId = GC_GetDwordValue(TYPE_RECT_UNIT, i, RT_PRI_RECT_HI_ID);

							}
							else
							{
								GC_SetEnumValue(TYPE_RECT_UNIT + j, i, S1_PRI_AC_ON_OFF_CTL,
									GC_RECT_OFF, TRUE);
								SetSlaveRectHVSDstatus(i, TRUE,j);

								dwRectId = GC_GetDwordValue(TYPE_RECT_UNIT + j, i, S1_PRI_RECT_ID);
								dwRectHiId = GC_GetDwordValue(TYPE_RECT_UNIT + j, i, S1_PRI_RECT_HI_ID);

							}
							
							GetRectIdStr(dwRectId, &dwRectHiId, strRectId);
							sprintf(strOut, 
								"Switch-off rect %d, Rect ID = %s. SysVolt = %.1fV, rect report volt = %.1fV.\n", 
								i + 1, strRectId, g_pGcData->RunInfo.fSysVolt, fRectVolt);
							GC_LOG_OUT(APP_LOG_INFO, strOut);

							pRtRunInfo->bRectHVSD = TRUE;
							s_bLastHVSD = TRUE;
						}
						else
						{
							s_iHVSDRectID = i;
						}

						//Only close one rect in a cycle
						break;
					}
				}
			}

		}
		

	}
	else
	{
		if(s_bLastHVSD)
		{
			GC_LOG_OUT(APP_LOG_INFO, "System voltage is normal after close HVSD rect.\n");
		}
		s_bLastHVSD = FALSE;
		s_iHVSDRectID = -1;
	}

	// һ��ģ�����Ź���ַ�������û���������ģ�飬���HVSD��־��ΪFalse
	if(GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_RECT_INFO_CHANGED) > 0)
	{
		
		pRtRunInfo->bRectHVSD = FALSE;
	//	printf("Clear HVSD flag because rect info changed.\n");

		SetRectHVSDstatus(-1, FALSE);
		SetSlaveRectHVSDstatus(-1, FALSE,-1);

		//�ڷǴӻ�ģʽ�£���Ҫ����ñ�־
		SIG_ENUM eRunMode = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MASTER_SLAVE_MODE);
		if(eRunMode != MODE_SLAVE)
		{
			GC_SetDwordValue(TYPE_OTHER, 0, RT_PUB_RECT_INFO_CHANGED, 0);
		}
	}
	// ������õ�ģ�����Ϊ0��������û������ͨѶ�жϸ澯
	else if(GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_CFG_RECT_NUM) == 0)
	{
		
		pRtRunInfo->bRectHVSD = FALSE;
		printf("Clear HVSD flag because cfg rect num is 0.\n");

		SetRectHVSDstatus(-1, FALSE);
		SetSlaveRectHVSDstatus(-1, FALSE,-1);

	}

}


static void SetRectHVSDstatus(int iRectId, BOOL bAlarm)
{
	if(iRectId < 0)		//Set All
	{
		int i;
		for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect; i++)
		{
			GC_SetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_HVSD_STATUS, bAlarm, TRUE);
		}

	}
	else
	{
		GC_SetEnumValue(TYPE_RECT_UNIT, iRectId, RT_PRI_HVSD_STATUS, bAlarm, TRUE);
	}
}

static void SetSlaveRectHVSDstatus(int iRectId, BOOL bAlarm,int iSlaveID)
{
	if(iRectId < 0 && iSlaveID < 0)		//Set All
	{
		int i;
		int j;
		for(j = 1;j < MAX_RECT_TYPE_NUM;j++)
		{
			for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[j].iQtyOfRect; i++)
			{
				if(j == 0)
				{
					GC_SetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_HVSD_STATUS, bAlarm, TRUE);

				}
				else
				{
					GC_SetEnumValue(TYPE_RECT_UNIT + j, i, S1_PRI_HVSD_STATUS, bAlarm, TRUE);
				}
				
			}

		}
		

	}
	else
	{
		GC_SetEnumValue(TYPE_RECT_UNIT + iSlaveID, iRectId, S1_PRI_HVSD_STATUS, bAlarm, TRUE);
	}
}
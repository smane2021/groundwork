/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU Plus Rack_mounted
 *
 *  FILENAME : gc_batt_mgmt.c
 *  CREATOR  : Frank Cao                DATE: 2006-05-09 16:00
 *  VERSION  : V1.00
 *  PURPOSE  : To provide interface function of Battery Management
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include	"gc_index_def.h"
#include	"gc_batt_mgmt.h"
#include	"gen_ctl.h"
#include	"pub_list.h"
#include	"gc_rect_list.h"
#include	"gc_batt_history.h"
#include	"gc_sig_value.h"
#include	"gc_float_charge.h"
#include	"gc_boost_charge.h"
#include	"gc_discharge.h"
#include	"gc_lvd.h"
#include	"gc_batt_cap.h"
#include	"gc_estop_eshutdown.h"


static void StateMgmt(void);
static void	OpenAllRect(void);
static void BattMgmtTimerInit(void);
static float GetTempCompDelta(void);
static void CmpRtBtCurr(void);

/*Hybrid function*/
static BOOL GC_HybridState(void);
static void GC_HybridCapacityMode(void);
static BOOL GC_HybridFixedtimeMode(void);
static void GC_HybridEqualisCharge(void);
static BOOL GC_HybridIsUnderVol(void);
static BOOL GC_HybridIsOverTemp(void);
static BOOL GC_HybridIsRunOverTemp(void);
static void GC_HybridResetCapacity(void);
static void GC_HybridAlarmOutput(IN BOOL bClear);
static void GC_HybridGridOn(void);
static void GC_HybridOpenDiesel(IN BOOL bRunOverTemp);
static void GC_HybridCloseDiesel(void);
static int GC_GetHybridCapacityDischarge(int iIdx);
static int GC_GetHybridFixedDischarge(int iIdx);
static int GC_GetHybridDieselROOTDuration(IN int iIdx);
static int GC_GetHybridEqualismDuration(IN int iIdx);
static BOOL GC_IsHybridEqualismTime(void);
static int GC_GetHybridEqualismDuration(IN int iIdx);
static BOOL GC_HybridChargeCapCmp(void);
static BOOL GC_HybridCmpHighLoadAlarm(void);
static BOOL	GC_IsHybridStartDischargeTime(void);
static BOOL GC_HybridDischCapCmp(void);
static void GC_HyrbridRelayOutputCtrl(int iCtrlValue, int iRelayNo);
static void GC_HybridAlarmRelayAction(IN int iCurrRelayNo,
								   IN BOOL bAlarmStart);
static SIG_ENUM GC_HyBridGetRelayStatus(int iRelayNo);
static SIG_ENUM GC_HyBridGetDIStatus(int iRelayNo);
static int GC_GetHybridDieselFailureInterval(IN int iIdx);
static void GC_HybridRefreshHybridAlarm(void);
static BOOL GC_HybridGetDieselState(IN int iDG);

static int GC_ControlAllRelay(IN BOOL bClear);
static BOOL GC_RelayControl(IN int iRelayNum, IN int iStatus, IN BOOL bClear);
static SIG_ENUM GC_GetRelayStatus(IN int iEquipID, IN int iSignalType, IN int iSIgnalID);
static void GC_RaiseRelayAlarm(IN int iRelayNum, IN int iAarlm);
static void GC_ControlVoltage(IN int iState);
static void GC_SendMTCurrentLmt(IN float fCurrentLmt);
void GC_CurrentLmt_Mppt_Min(void);

static float GC_GetVoltCheck(int iSigId,float fVolt);
static float GC_GetRegulateVolt_MPPT(float fAimVoltage);
//static float GetMaxCurrDiff(void);
//static float Filter_Down(float fLastOutput,
//						 float fInput, 
//						 float* pfFIFO,
//						 float* pfSort_Buffer,
//						 int* piFIFO_Head,
//						 int iSizeOfBuffer,
//						 int iSizeOfMiddle);
static void GC_DcVoltCtl_Mppt(float fVolt);
#ifdef	GC_JFFS2
static WORD Crc12(const BYTE* pbyBuff, int iLen);
#endif


#define	SYS_ALM_LIGHT_ON	1
#define	SYS_ALM_LIGHT_OFF	0

#define	SIZE_BATT_CURR_BUF	3
#define	SIZE_BATT_CURR_MID	1

static int	GC_GetCurrLmtPeriod_BMS(int iIdx)
{
	UNUSED(iIdx);	
	return  10;
}

static int GC_GetSysAlmLightValue()
{
#define GC_SYS_ALARM_FUNC_SIGID		499
	int iInited = 0;
	static SIG_BASIC_VALUE *pSigSysAlarmFunc = NULL;
	int iVarSubID,iBufLen,iError;
	if(iInited == 0)
	{
		iInited=1;
		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, GC_SYS_ALARM_FUNC_SIGID);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			1,
			iVarSubID,
			&iBufLen,
			&pSigSysAlarmFunc,
			0);
		if(iError != ERR_DXI_OK)
		{
			GC_LOG_OUT( APP_LOG_ERROR, 
				"Get Signal sys alarm function signal point error!\n");
		}	
	}
	if(pSigSysAlarmFunc != NULL)
		return pSigSysAlarmFunc->varValue.enumValue;
	else
		return 1;//1 is the default value of this signal
}

//added for sys alarm light judge as setting signal
static BOOL GC_SysAlmLightJudge(void)
{
	int iSysAlmLightValue;
	iSysAlmLightValue = GC_GetSysAlmLightValue();

	if(iSysAlmLightValue == 0)//
	{
		if((GC_GetAlarmNum(ALARM_LEVEL_CRITICAL) > 0))
		{
			return TRUE;
		}
	}
	else if(iSysAlmLightValue == 1)
	{
		if((GC_GetAlarmNum(ALARM_LEVEL_CRITICAL) > 0) 
			|| (GC_GetAlarmNum(ALARM_LEVEL_MAJOR) > 0))
		{
			return TRUE;
		}
	}
	else
	{
		if((GC_GetAlarmNum(ALARM_LEVEL_CRITICAL) > 0) 
			|| (GC_GetAlarmNum(ALARM_LEVEL_MAJOR) > 0)
			|| (GC_GetAlarmNum(ALARM_LEVEL_OBSERVATION) > 0))
		{
			return TRUE;
		}
	}
	return FALSE;

}

void GC_SysAlmLightCtl(void)
{
	BOOL bSysValue = GC_SysAlmLightJudge();

	if( bSysValue)
	{
		GC_SetEnumValue(TYPE_OTHER, 
					0,
					GC_PUB_SYS_ALM_LIGHT_CTL, 
					SYS_ALM_LIGHT_ON,
					TRUE);
	}
	else
	{
		GC_SetEnumValue(TYPE_OTHER, 
					0,
					GC_PUB_SYS_ALM_LIGHT_CTL, 
					SYS_ALM_LIGHT_OFF,
					TRUE);
	
	}
	return;

}


static float GC_GetSystemLoadForPrelimit(void)
{
	float ftemp = g_pGcData->RunInfo.fSysLoad;
	int i;

	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_EES_SYSTEM_MODE))
	{
		if( TRUE == GC_GetEnumValue(TYPE_OTHER, 0, GC_EES_SYSTEM_MODE))
		{
			
				for(i = 0;i<MAX_NUM_LVD_EQUIP*2;i++)
				{
					if(g_pGcData->RunInfo.Lvd.bLoadLVDFlag[i] == TRUE)
					{
						if(ftemp<g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[i])
							ftemp=g_pGcData->RunInfo.Lvd.fLoadCurrBeforeLVD[i];

					}
				}
			

		}	
	}
	return ftemp;
}
/*==========================================================================*
* FUNCTION : GC_GetBattCapForPrelimit
* PURPOSE  : Get min batt cap if all batt don't used in batt manage,the cap is 50000
* CALLS    : 
* CALLED BY: 
* RETURN   : : 
* COMMENTS : 
* CREATOR  :                 DATE: 2004-09-15 12:20
*==========================================================================*/
static float GC_GetBattCapForPrelimit(void)
{
	int i;
	float fBattRatedCap =  50000;
	float ftemp = 0;
	BOOL bRTN;

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i]&& g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&ftemp);
			if(bRTN == FALSE)
			{
				continue; 
			}
			if(fBattRatedCap > ftemp )
			{
				fBattRatedCap = ftemp;		
			}
		}
	}
	return  fBattRatedCap;
}

static BOOL CheckRectAcFail(BOOL bLast)
{
    BOOL        bAcFail = TRUE;
    int         i;
    int         iQtyCommRect;

    iQtyCommRect = (int)GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_COMM_QTY);

    //All rectifiers communication fail
    if(!iQtyCommRect)
    {
        if(RECTIFIER_DOUBLE_POWER_TYPE 
            == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_POWER_TYPE))
        {
            return bLast;
        }
        else
        {
            if(g_pGcData->RunInfo.Bt.bDischarge)
            {
                return TRUE;
            }
            else
            {
                return bLast;
            }
        }
    }

    for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[0].iQtyOfRect; i++)
    {
        if(g_pGcData->RunInfo.Rt.Slave[0].abComm[i])
        {   
            if(GC_NORMAL 
                == GC_GetEnumValue(TYPE_RECT_UNIT, i, RT_PRI_AC_FAIL))
            {
                bAcFail = FALSE;
                break;
            }
        }
    }
    if(bAcFail)
    {
	    for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
	    {
		    if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID) 
			    && GC_NORMAL == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_MAINS_FAILURE_SLAVE1 + i - 1))
		    {					
			    bAcFail = FALSE;
			    break;
		    }		
	    }
    }

    return bAcFail;
}

/*==========================================================================*
 * FUNCTION : GC_GetSourceCurrForPrelimit
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: GC_PreCurrLmt
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank SongXu                DATE: 2017-03-02 12:20
 *==========================================================================*/
static float GC_GetSourceCurrForPrelimit()
{
#define GC_SOURCE_VALUE_SIGID		240
	static int iInited = 0;
	static SIG_BASIC_VALUE *pSigSourceValueSig = NULL;
	static EQUIP_INFO	*pEquip = NULL;
	int iVarSubID,iBufLen,iError;
	if(iInited == 0)
	{
		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, GC_SOURCE_VALUE_SIGID);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			1,
			iVarSubID,
			&iBufLen,
			&pSigSourceValueSig,
			0);
		if(iError != ERR_DXI_OK)
		{
			GC_LOG_OUT( APP_LOG_ERROR, 
				"Get Signal Source Value point error!\n");
			iInited = -1;
			return 0;
		}	
		iInited = 1;
	}
	if(iInited == 1 && pSigSourceValueSig != NULL)
	{
		if (SIG_VALUE_IS_VALID(pSigSourceValueSig))
		{
			return pSigSourceValueSig->varValue.fValue;
		}
	}

	return 0;


}
/*==========================================================================*
 * FUNCTION : GC_PreCurrLmt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: GC_ShortTest
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-15 12:20
 *==========================================================================*/
void	 GC_PreCurrLmt(int iQtyOfRect, BOOL bAcFail,BOOL bLVDReconnect)
{
	float	fRectRatedCurr;
	float	fLoadCurrForLimit,fBattCapForLimit,fSoureCurrForLimit = 0;
	float	fCurrLmtOfRt;
	int	i;

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif

	if((GC_CURRLIMIT_DISABLE == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB))
		|| (!g_pGcData->RunInfo.Bt.iQtyValidBatt))
	{
		
		GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				 g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint);

		GC_CurrlmtCtl_Slave(g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint);
#ifdef GC_SUPPORT_MPPT
		GC_SendMTCurrentLmt(g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint);
		g_pGcData->RunInfo.Mt.fCurrLmt = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;
#endif
		
		g_pGcData->RunInfo.Rt.fCurrLmt = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;
		return;
	}
	

	//Get rectifier reated current
	fRectRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;

	fLoadCurrForLimit =GC_GetSystemLoadForPrelimit();
	fBattCapForLimit =GC_GetBattCapForPrelimit();
	fSoureCurrForLimit =GC_GetSourceCurrForPrelimit();

	int iIdx = 0;
	//calculate and set rectifier current limit
	if(iQtyOfRect)
	{
		//add by Jimmy 2012.06.25~{T$o?=o?=o?=o?=R2o?=o?=o?=o?=o?=o?=R;o?=o?=o?=o?=o?=o?=o?~}?
		//~{o.5o?=o?=o?=o?=o?=o?=o?=o?=V1o?=o?=J9o?=o?=o?=d5%o?=o?=o?=o?=o?=c7~}?
		if(IsWorkIn_LiBattMode())
		{
			//~{W"o?=b#,o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=J9o?=o?~}?~{o?=o?=N*o?=o?=N;o?=o?=o?=o?=o?=O5o?=X2o?=M~} .
			fCurrLmtOfRt = 100.0 * (fLoadCurrForLimit
				+ GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_LIBATT))
				/ (fRectRatedCurr * iQtyOfRect);
			
		}
		else
		{
			SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
			if(stState)
			{
				fCurrLmtOfRt = 100.0 * (fLoadCurrForLimit - fSoureCurrForLimit
						+ 0.50 * GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT)* fBattCapForLimit - g_pGcData->RunInfo.Mt.fSumOfCurr)//* GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_RATED_CAP))
						/ (fRectRatedCurr * iQtyOfRect);
				
			}
			else
			{
				fCurrLmtOfRt = 100.0 * (fLoadCurrForLimit - fSoureCurrForLimit
						+ 0.50 * GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT)* fBattCapForLimit)//* GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_RATED_CAP))
						/ (fRectRatedCurr * iQtyOfRect);
				
			}

			if(bAcFail)	//~{o?=o?=o?=o?=M#o?=o?=~}
			{
				//for(iIdx = 0; iIdx < iQtyOfRect; iIdx++)
				//{
				//	int iAcFail = GC_GetEnumValue(TYPE_RECT_UNIT, iIdx, RT_PRI_AC_FAIL);
				//	if (2 != iAcFail || g_pGcData->RunInfo.Bt.fSumOfCurr > 0)			//~{o?=N:o?=R;o?=o?=D#o?=o?=~} 2: AC~{C;o?=o?=J'o?=\#o?=o?=o?=o?=X5o?=o?=o?=o?=o?=o?=Y7E5g!#~}
				//	{
				//		fCurrLmtOfRt = g_pGcData->RunInfo.Rt.fCurrLmt;				//~{o?=o?=o?=o?=o?=c2;o?=o?=~}

				//		break;
				//	}
				//}

				GC_RUN_INFO_RECT *pRtRunInfo = &(g_pGcData->RunInfo.Rt);
				pRtRunInfo->bAcFail = CheckRectAcFail(pRtRunInfo->bAcFail);
				if(stState)
				{
					if (FALSE == pRtRunInfo->bAcFail)
					{
						fCurrLmtOfRt = g_pGcData->RunInfo.Rt.fCurrLmt;
						
					}
				}
				else
				{
					if ((FALSE == pRtRunInfo->bAcFail) || g_pGcData->RunInfo.Bt.fSumOfCurr > 0)
					{
						fCurrLmtOfRt = g_pGcData->RunInfo.Rt.fCurrLmt;
						
					}
				}
			}

		}
	}
	else
	{
		fCurrLmtOfRt = GC_MAX_CURR_LMT; 	
	}

	if(fCurrLmtOfRt > GC_MAX_CURR_LMT)
	{
		fCurrLmtOfRt = GC_MAX_CURR_LMT;
	}
	/* Added by Marco Yang, 2011-04-27, for NA's requirement:Limit the system max output */
	if(fCurrLmtOfRt > g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint)
	{
		fCurrLmtOfRt = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;
		
	}
	/* End by Marco Yang, 2011-04-27, for NA's requirement:Limit the system max output */

	if(fCurrLmtOfRt < GC_MIN_CURR_LMT)
	{
		fCurrLmtOfRt = GC_MIN_CURR_LMT;
	}
	
/* Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
	if(GC_IsDislPwrLmtMode()) 
	{
		float fDislPwrLmtPoint = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DISL_PWR_LMT_POINT);
		if(fCurrLmtOfRt > fDislPwrLmtPoint)
		{
			fCurrLmtOfRt = fDislPwrLmtPoint;
		}
		
	}
/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */

	if(ABS(fCurrLmtOfRt - g_pGcData->RunInfo.Rt.fCurrLmt) >= 4)
	{
		char	strOut[256];
		
		GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_STATUS_CHANGED,
					GC_ALARM,
					TRUE);
	}
#ifdef GC_SUPPORT_MPPT
//#define  GC_MPPT_MIN_CURRENT_LIMIT		(10.0)
	if(stState != GC_MPPT_DISABLE)
	{
		
		
		GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF, 
				fCurrLmtOfRt);

		g_pGcData->RunInfo.Rt.fCurrLmt = fCurrLmtOfRt;
			
		
		
	}
	else
	{
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF, 
			fCurrLmtOfRt);
		
		/*GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SLAVE1, 
			fCurrLmtOfRt);
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SLAVE2, 
			fCurrLmtOfRt);
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SLAVE3, 
			fCurrLmtOfRt);*/
		
		GC_CurrlmtCtl_Slave(fCurrLmtOfRt);
		g_pGcData->RunInfo.Rt.fCurrLmt = fCurrLmtOfRt;
	}
#else
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		RT_PUB_CURR_LMT_CTL_SELF, 
		fCurrLmtOfRt);
	/*GC_SetFloatValue(TYPE_OTHER, 
		0,
		RT_PUB_CURR_LMT_CTL_SLAVE1, 
		fCurrLmtOfRt);
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		RT_PUB_CURR_LMT_CTL_SLAVE2, 
		fCurrLmtOfRt);
	GC_SetFloatValue(TYPE_OTHER, 
		0,
		RT_PUB_CURR_LMT_CTL_SLAVE3, 
		fCurrLmtOfRt);*/
	
	GC_CurrlmtCtl_Slave(fCurrLmtOfRt);
	g_pGcData->RunInfo.Rt.fCurrLmt = fCurrLmtOfRt;
#endif
	
	if(bAcFail)
	{
		GC_SetSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY, 
					0,
					GC_GetPreCurrLmtDelay,
					0);
		if( bLVDReconnect)
		{
			GC_SetSecTimer(SEC_TMR_ID_PRE_CURR_LMT_OF_LVD, 
				0,
				GC_GetPreCurrLmtDelay,
				0);

		}
	}
	else
	{
		GC_SetSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY, 
					DELAY_AFTER_PRE_CURR_LMT,
					NULL,
					0);
		if( bLVDReconnect)
		{
			GC_SetSecTimer(SEC_TMR_ID_PRE_CURR_LMT_OF_LVD, 
				DELAY_AFTER_PRE_CURR_LMT,
				NULL,
				0);

		}
	}

	return;
}

#ifdef GC_SUPPORT_MPPT
void	 GC_PreCurrLmt_Mppt(int iQtyOfRect)
{
	float	fRectRatedCurr;
	float	fCurrLmtOfRt;
	int	i;
	float fLoadCurrForLimit =GC_GetSystemLoadForPrelimit();
	float fBattCapForLimit =GC_GetBattCapForPrelimit();
	SIG_ENUM	stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);

	

	if(!g_pGcData->RunInfo.Mt.bNight)
	{
		return;
	}

	if((GC_CURRLIMIT_DISABLE == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB))
		|| (!g_pGcData->RunInfo.Bt.iQtyValidBatt))
	{
		GC_SendMTCurrentLmt(g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint);
		g_pGcData->RunInfo.Mt.fCurrLmt =g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;
		return;
	}


	if(stState == GC_MPPT_MPPT)//Mppt mode
	{
		fCurrLmtOfRt = 100.0 * (fLoadCurrForLimit 
				+ 0.50 * GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT)* fBattCapForLimit )
				/ (g_pGcData->RunInfo.Mt.fRatedCurr * iQtyOfRect);
		if(fCurrLmtOfRt > GC_MAX_CURR_LMT)
		{
			fCurrLmtOfRt = GC_MAX_CURR_LMT;
		}
		
		if(fCurrLmtOfRt < GC_MPPT_MIN_CURRENT_LIMIT)
		{
			fCurrLmtOfRt = GC_MPPT_MIN_CURRENT_LIMIT;
		}
	}
	else
	{
		
			
		fCurrLmtOfRt = 100.0 * (0.50 * GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT)* fBattCapForLimit - (g_pGcData->RunInfo.Rt.fSumOfCurr - fLoadCurrForLimit))
						/ (g_pGcData->RunInfo.Mt.fRatedCurr * iQtyOfRect);
		//fCurrLmtOfRt = GC_MPPT_MIN_CURRENT_LIMIT;
		
		if(fCurrLmtOfRt > GC_MAX_CURR_LMT)
		{
			fCurrLmtOfRt = GC_MAX_CURR_LMT;
		}
		
		if(fCurrLmtOfRt < GC_MPPT_MIN_CURRENT_LIMIT)
		{
			fCurrLmtOfRt = GC_MPPT_MIN_CURRENT_LIMIT;
		}
			
		
	}

	if(ABS(fCurrLmtOfRt - g_pGcData->RunInfo.Mt.fCurrLmt) >= 4)
	{
		char	strOut[256];
		
	}
	GC_SendMTCurrentLmt(fCurrLmtOfRt);
	g_pGcData->RunInfo.Mt.fCurrLmt = fCurrLmtOfRt;
	//printf("Pre Current Limit %f\n", g_pGcData->RunInfo.Mt.fCurrLmt);		
	
	
	return;
}
#endif
/*==========================================================================*
 * FUNCTION : StateMgmt
 * PURPOSE  : State Management
 * CALLS    : GC_FloatCharge, GC_ShortTest, GC_PlanManTest
 *            GC_AcFailTest, GC_AcFail, GC_ManAutoBC, GC_CycPreBC
 * CALLED BY: GC_BattMgmt
 * ARGUMENTS:  
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-13 15:21
 *==========================================================================*/
static void StateMgmt()
{
	SIG_ENUM	enumState;

	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);
	switch (enumState)
	{
		case ST_FLOAT:
			GC_FloatCharge();
			break;

		case ST_SHORT_TEST:
			GC_ShortTest();
			break;

		case ST_MAN_TEST:
		case ST_PLAN_TEST:
			GC_PlanManTest(enumState);
			break;

		case ST_AC_FAIL_TEST:
			GC_AcFailTest();
			break;

		case ST_AC_FAIL:
			GC_AcFail();

			break;
		case ST_MAN_BC:
		case ST_AUTO_BC:
			GC_ManAutoBC(enumState);

			break;
		case ST_CYCLIC_BC:
		case ST_BC_FOR_TEST:
			GC_CycPreBC();
			break;

		case ST_PROTECTION:
		default:
			GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE, ST_FLOAT, TRUE);
			break;
	}

	return;
}



/*==========================================================================*
 * FUNCTION : BattMgmtStateInit
 * PURPOSE  : Initialize the Battery Management State, Battery Manangement 
              Alarm and Auto / Man state
 * CALLS    : GC_GetEnumValue GC_SetEnumValue
 * CALLED BY: GC_BattMgmtInit
 * ARGUMENTS: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-05 14:52
 *==========================================================================*/
static void BattMgmtStateInit(void)
{
	float		fVolt;
	SIG_ENUM	enumState;
	//initialize the state
	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	switch(enumState)
	{
	case ST_MAN_BC:
	case ST_AUTO_BC:
	case ST_CYCLIC_BC:
	case ST_BC_FOR_TEST:
		//signal BT_PUB_BOOST_CTL handling
#ifdef GC_SUPPORT_MPPT

		GC_ControlVoltage(BT_PUB_BST_VOLT);
		//fVolt = GC_GetSettingVolt(BT_PUB_NOM_VOLT);
		//GC_DcVoltCtl(fVolt);
#else
		fVolt = GC_GetSettingVolt(BT_PUB_BST_VOLT);
		GC_DcVoltCtl(fVolt);
#endif


		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BOOST_CTL,
						CTL_BOOST,
						TRUE);
		GC_LOG_OUT(APP_LOG_INFO, 
			"General Controller start, Turn to BC.\n");
		break;

	case ST_MAN_TEST:
		GC_SetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN, STATE_AUTO, TRUE);
		//Here not need break;
	case ST_FLOAT:
	case ST_PLAN_TEST:
	case ST_AC_FAIL_TEST:
	case ST_AC_FAIL:
	case ST_SHORT_TEST:
	default:
#ifdef GC_SUPPORT_MPPT

		GC_ControlVoltage(BT_PUB_NOM_VOLT);
		//fVolt = GC_GetSettingVolt(BT_PUB_NOM_VOLT);
		//GC_DcVoltCtl(fVolt);
#else
		fVolt = GC_GetSettingVolt(BT_PUB_NOM_VOLT);
		GC_DcVoltCtl(fVolt);
#endif
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						ST_FLOAT,
						TRUE);

		//signal BT_PUB_BOOST_CTL handling

		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BOOST_CTL,
						CTL_FLOAT,
						TRUE);

		GC_LOG_OUT(APP_LOG_INFO, 
			"General Controller start, Turn to FC.\n");
		break;
	}

	//signal CTL_TEST_STOP handling
	GC_SetEnumValue(TYPE_OTHER, 
					0, 
					BT_PUB_TEST_CTL,
					CTL_TEST_STOP,
					TRUE);


	//Initialize the GC Alarms
	GC_SetEnumValue(TYPE_OTHER,
					0, 
					BT_PUB_BAD_BATT_SET, 
					GC_NORMAL,
					TRUE);

	GC_SetEnumValue(TYPE_OTHER,
					0, 
					BT_PUB_DISCH_IMB_SET, 
					GC_NORMAL,
					TRUE);
	return;

}



/*==========================================================================*
 * FUNCTION : GC_BattMgmtInit
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-22 11:26
 *==========================================================================*/
void GC_BattMgmtInit()
{
	float		fCurrLmt; 
	SIG_ENUM	enumVal;
	ULONG		ulVal;
	int		i;

	/*If no battery, return*/
	/*if(!g_pGcData->EquipInfo.iCfgQtyOfBatt)
	{
		return;
	}*/

	BattMgmtTimerInit();

	g_pGcData->RunInfo.Bt.TestLog.pTestRecord = NULL;
	TestLogInit();
	g_pGcData->RunInfo.Rt.fRatedVolt = 48.0;

	g_pGcData->RunInfo.bPreLmtForAcFail = FALSE;
	g_pGcData->RunInfo.bLVDPreLmtToCurrLmtFlag = FALSE;
	g_pGcData->RunInfo.fSysLoadofPreCurrLimit = 0;
	
	BattMgmtStateInit();

	g_pGcData->RunInfo.Bt.dwBattTypeNo 
		= GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_TYPE_NO);
	g_pGcData->RunInfo.Bt.bNeedRegulateCap = FALSE;

	g_pGcData->RunInfo.Bt.dwBcProtectCounter = 0;

	g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);	
	g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

	g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_NORMAL;

	/*GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_RATED_CURR, 100.0);
	GC_SetFloatValue(TYPE_OTHER, 0, RT_PUB_RATED_VOLT, 53.5);*/

	GC_BattHisInit();

	GC_LvdInit();

	GC_RunInfoRefresh();

	g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint = GC_MAX_CURR_LMT;
	g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;

	GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF, 
					GC_MAX_CURR_LMT);

	for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
	{
		if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
		{

			GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF + i,
			GC_MAX_CURR_LMT);				

		}		
	}
	
	//No battery
	/*if(0 == g_pGcData->RunInfo.Bt.iQtyValidBatt)
	{	
		GC_SetEnumValue(TYPE_OTHER, 
						0, 
						BT_PUB_BM_STATE,
						ST_FLOAT,
						TRUE);
	}*/

	GC_SetEnumValue(TYPE_OTHER, 
					0,
					SPLIT_PUB_MODE,
					g_pGcData->PriCfg.bSlaveMode,
					TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
					0,
					BT_PUB_ABNOR_BATT_CURR_SET, 
					GC_NORMAL,
					TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
					0,
					GC_PUB_ABNOR_LOAD_SET, 
					GC_NORMAL,
					TRUE);

	fCurrLmt = GC_GetFloatValue(TYPE_OTHER, 
								0,
								BT_PUB_CURR_LMT);

	GC_SetFloatValue(TYPE_OTHER, 
					0,
					BT_PUB_EXP_CURR_LMT,
					fCurrLmt);

	//Set rectifer parameter
	ulVal = GC_GetDwordValue(TYPE_OTHER, 
							0,
							RT_PUB_SEQ_START_INTERVAL);
	GC_SetDwordValue(TYPE_OTHER, 
					0,
					RT_PUB_SEQ_START_INTERVAL,
					ulVal);

	ulVal = GC_GetDwordValue(TYPE_OTHER, 
							0,
							RT_PUB_OV_RESTART_INTERVAL);
	GC_SetDwordValue(TYPE_OTHER, 
					0,
					RT_PUB_OV_RESTART_INTERVAL,
					ulVal);

	ulVal = GC_GetDwordValue(TYPE_OTHER, 
							0,
							RT_PUB_WALKIN_TIME);
	GC_SetDwordValue(TYPE_OTHER, 
					0,
					RT_PUB_WALKIN_TIME,
					ulVal);	

	enumVal = GC_GetEnumValue(TYPE_OTHER, 
							0,
							RT_PUB_OV_RESTART_ENB);
	GC_SetEnumValue(TYPE_OTHER, 
					0,
					RT_PUB_OV_RESTART_ENB,
					enumVal,
					TRUE);	
	
	enumVal = GC_GetEnumValue(TYPE_OTHER, 
							0,
							RT_PUB_WALKIN_ENB);
	GC_SetEnumValue(TYPE_OTHER, 
					0,
					RT_PUB_WALKIN_ENB,
					enumVal,
					TRUE);	

	enumVal = GC_GetEnumValue(TYPE_OTHER, 
							0,
							RT_PUB_AC_OV_RESTART_ENB);
	GC_SetEnumValue(TYPE_OTHER, 
					0,
					RT_PUB_AC_OV_RESTART_ENB,
					enumVal,
					TRUE);	

	GC_SetFloatValue(TYPE_OTHER, 
					0,
					BT_PUB_LOWER_CAPACITY, 
					100.0);
	
	g_pGcData->RunInfo.Bt.iCmpCapDelay = 0;

	g_pGcData->RunInfo.Bt.bErrCurr = FALSE;


	return;
}


/*==========================================================================*
 * FUNCTION : GC_BattMgmtDestroy
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-08 15:12
 *==========================================================================*/
void GC_BattMgmtDestroy()
{
	/*If no battery, return*/
	/*if(!g_pGcData->EquipInfo.iCfgQtyOfBatt)
	{
		return;
	}*/

	GC_TestLogDestroy();

	//Not execute GC_TestLogDestroy();
	
	//Not execute BattMgmtStateDestroy();

	//Not execute GC_BattHisInitDestroy();

	//Not execute GC_LvdDestroy();


	return;	
}


/*==========================================================================*
 * FUNCTION : GC_BattMgmt
 * PURPOSE  : Battery Management
 * CALLS    : StateMgmt, GC_BattCapCalc, BcProtet, GC_LvdRun, GC_BattHisRefresh
 *			  
 * CALLED BY: ServiceMain
 * ARGUMENTS:  
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-09-13 15:08
 *==========================================================================*/
void GC_BattMgmt()
{
	BOOL	bImbalProtEnb
				= GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_IMBAL_PROT_ENB);

	BOOL	bFiammBattery = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_FIAMM_BATTERY);
	//DWORD	dwBMSNumber ;//= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_BMS_NUMBER);
	//BOOL bRTN = GC_GetDwordValueWithStateReturn(TYPE_OTHER, 0, GC_PUB_BMS_NUMBER, TRUE, &dwBMSNumber);//
	
#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif

	if(SEC_TMR_TIME_IN == GC_GetStateOfSecTimer(SEC_TMR_ID_BOOTING_DELAY))
	{
		//Do nothing.
	}
	else
	{
		//Wait all battery current is active(About 200 Seconds), then calculate the load current.
		CmpRtBtCurr();
	}

	//CmpRtBtCurr();

	GC_BattCapCalc();
	/*if(!(g_pGcData->RunInfo.Bt.iQtyValidBatt))
	{
		GC_HandlingOnErr(ST_FLOAT, "No valid battery, turn to FC.\n");
		return;
	}*/

	if(bFiammBattery)
	{
		GC_HandlingFiammBatt(ST_FLOAT, "Change to Fiamm Battery.\n");
		return;
	}
	/*if(bRTN && dwBMSNumber > 0)
	{
		GC_HandlingBMS(ST_FLOAT, "Change to BMS.\n");
		return;
	}*/
	//error current
	if(g_pGcData->RunInfo.Bt.bErrCurr && (GC_ENABLED == bImbalProtEnb))
	{

		GC_HandlingOnErr(ST_PROTECTION, "DC Current error, turn to PROTECT.\n");
	}
	//no valid rectifier and no rectifier AC failure
#ifdef GC_SUPPORT_MPPT
	else if(((stState == GC_MPPT_DISABLE) &&  (!(g_pGcData->RunInfo.Rt.bValid)) && (!(g_pGcData->RunInfo.Rt.bAcFail)))
		|| ((stState != GC_MPPT_DISABLE) && (!(g_pGcData->RunInfo.Rt.bValid)) && (!(g_pGcData->RunInfo.Rt.bAcFail)) && (!(g_pGcData->RunInfo.Mt.bValid)) && ((g_pGcData->RunInfo.Mt.bNight))))
	{

		GC_HandlingOnErr(ST_FLOAT, "No valid retifier and solar converter, turn to Battery Discharge.\n");
	}
#else
	else if((!(g_pGcData->RunInfo.Rt.bValid)) && (!(g_pGcData->RunInfo.Rt.bAcFail)))
	{

		GC_HandlingOnErr(ST_FLOAT, "No valid retifier, turn to FC.\n");
	}
#endif
	else
	{

		StateMgmt();
		GC_BcProtect();
	}
	GC_HybridState();
	GC_LvdRun();
	GC_BattHisRefresh();

	return;
}


void GC_MasterBattMgmt()
{
	BOOL	bImbalProtEnb
				= GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_IMBAL_PROT_ENB);
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);

	CmpRtBtCurr();
	GC_BattCapCalc();

	//error current
	if(g_pGcData->RunInfo.Bt.bErrCurr && (GC_ENABLED == bImbalProtEnb))
	{
		GC_HandlingOnErr(ST_PROTECTION, "DC Current error, turn to PROTECT.\n");
	}
	//no valid rectifier and no rectifier AC failure
#ifdef GC_SUPPORT_MPPT
	else if(((stState == GC_MPPT_DISABLE) &&  (!(g_pGcData->RunInfo.Rt.bValid)) && (!(g_pGcData->RunInfo.Rt.bAcFail)))
		|| ((stState != GC_MPPT_DISABLE) && (!(g_pGcData->RunInfo.Rt.bValid)) && (!(g_pGcData->RunInfo.Rt.bAcFail)) && (!(g_pGcData->RunInfo.Mt.bValid)) && (!(g_pGcData->RunInfo.Mt.bNight))))
	{
		GC_HandlingOnErr(ST_FLOAT, "No valid retifier, turn to FC.\n");
	}
#else
	else if((!(g_pGcData->RunInfo.Rt.bValid)) && (!(g_pGcData->RunInfo.Rt.bAcFail)))
	{
		GC_HandlingOnErr(ST_FLOAT, "No valid retifier, turn to FC.\n");
	}
#endif
	else
	{
		StateMgmt();
		GC_BcProtect();
	}

	GC_HybridState();	
	GC_LvdRun();
	GC_BattHisRefresh();

	return;
}


/*==========================================================================*
 * FUNCTION : Filter_Down
 * PURPOSE  : Filter the input down-change only.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: float  fInput        : 
 *            float  *pfFIFO       : 
 *            int    *piFIFO_Head  : 
 *            int    iSizeOfBuffer : 
 *            int    iSizeOfMiddle : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-12-23 16:40
 *==========================================================================*/
static float Filter_Down(float fLastOutput,
						 float fInput, 
						 float* pfFIFO,
						 float* pfSort_Buffer,
						 int* piFIFO_Head,
						 int iSizeOfBuffer,
						 int iSizeOfMiddle)

{
	int		i , j;
	float	fSum;
    
	if(fInput > fLastOutput)
	{
		for (i = 0; i < iSizeOfBuffer; i++)
		{
			*(pfFIFO + i) = fInput;
		}
		return fInput;
	}

	ASSERT((*piFIFO_Head) < iSizeOfBuffer);
	
	//Put the input into the array
	*(pfFIFO + (*piFIFO_Head)) = fInput;
	(*piFIFO_Head)++;
	if((*piFIFO_Head) >= iSizeOfBuffer)
	{
		*piFIFO_Head = 0;
	}

    //Put the array into buffer
	for (i = 0; i < iSizeOfBuffer; i++)
	{
       *(pfSort_Buffer + i) = *(pfFIFO + i);
	}

    //effervescing sort
    for (i = 0; i < (iSizeOfBuffer - 1); i++)
	{
		for (j = 0; j < (iSizeOfBuffer - 1 - i); j++)
		{
			if((*(pfSort_Buffer + j)) > ( *(pfSort_Buffer + j + 1)))
			{
				float fTemp = *(pfSort_Buffer + j + 1);
				*(pfSort_Buffer + j + 1) = *(pfSort_Buffer + j);
				*(pfSort_Buffer +j) = fTemp;
			}
		}
	}

    //middle average
    fSum = 0;
    for (i = 0; i < iSizeOfMiddle; i++)
	{
		fSum +=(*(pfSort_Buffer + ((iSizeOfBuffer - iSizeOfMiddle) / 2 + i)));
    }

    return (fSum / iSizeOfMiddle);
}




/*********************************************************************************
*  
*  FUNCTION NAME : GC_IsNeedABCL
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-8-28 10:59:03
*  DESCRIPTION   : to judge if need to start ABCL or cease
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
//static BOOL GC_IsNeedABCL(float* pfBattCurr)
//{
//	//~{o?=o?=o?=o?=~}1~{o?=o?=o?=\5o?=o?=o?=~}>0.5*ABCL*No
//	float fLiTotalCurr = g_pGcData->RunInfo.Bt.fSumOfCurr;
//	float fLiBattCurrLmtSetPoint = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_LIBATT);
//	if(fLiTotalCurr > 0.5*fLiBattCurrLmtSetPoint*g_pGcData->RunInfo.Bt.iQtyValidBatt)
//	{
//		return TRUE;
//	}
//	//~{o?=o?=o?=o?=~}3:~{o?=P5o?=X7E5o?=o?=o?=_5o?=X5o?=o?=o?=o?=o?~}?
//	for(i = 0; i < GC_Get_Total_BattNum(); i++)
//	{
//		if(g_pGcData->RunInfo.Bt.abValid[i])
//		{
//			if(pfBattCurr[i] < 0 || pfBattCurr[i] > fLiBattCurrLmtSetPoint*1.2)
//			{
//				return TRUE;
//			}
//		}
//	}
//	return FALSE;
//}
/*********************************************************************************
*  
*  FUNCTION NAME : GC_CurrentLimit_LiBatt
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-5-7 18:27:10
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
void GC_CurrentLimit_LiBatt(void)
{
	int		i, j;
	float	fSumOfNegCurr = 0.0001;
	float	fMinPosDiff = 100000.0;
	//float	fBtRatedCap;
	float	fRtRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	//BOOL	bInDeadArea;
	//BOOL	bAllBattUnderLowLmt = TRUE;
	BOOL	bBattOverHighLmt = FALSE;
	static float fSlaveCurrLmt = 0.0;

	static BOOL		s_bFirstRun = TRUE;
	static float	s_afFIFO[MAX_NUM_BATT_EQUIP][SIZE_BATT_CURR_BUF];
	static int		s_aiHead[MAX_NUM_BATT_EQUIP];
	static float	s_afBattCurrOut[MAX_NUM_BATT_EQUIP];
	float			afSortBuf[SIZE_BATT_CURR_BUF];
	BOOL		bExceedCurr = FALSE,bRTN;
	float		fDislPwrLmtPoint = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DISL_PWR_LMT_POINT);

	
	//dwSafeModeDelayTimes = 6 * GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_SAFE_MODE_LI_DELAY);
	////~{o?=o?=J1o?=o?=N'~} 1~{o?=o?=~}30min
	//if(dwSafeModeDelayTimes < 6)
	//{
	//	dwSafeModeDelayTimes = 6;
	//}
	//else if(dwSafeModeDelayTimes > 180)
	//{
	//	dwSafeModeDelayTimes = 180;
	//}

	//if(!g_pGcData->RunInfo.Bt.iQtyValidBatt && dwUsedDelayTimes < dwSafeModeDelayTimes) //~{N4o?=o?=b5=o~} ~{5o?=o?~}?,~{o?=o?=o?=k02H+D#J=~}
	//{
	//	dwUsedDelayTimes++;
	//	//~{M7~}3~{o?=N#o?=~}30s~{o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=^#o?=o?=o?=V$o?=o?=o?=X9o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=R2o?=o?=a1;o?=o?=o?=o?=o?=o?=o?=Y?o?=J<o?=o?=o?=o?=H~} ~{D#J=o?=^!o?=~}
	//	if(dwUsedDelayTimes < 3)
	//	{
	//		GC_SafeMode_CurrLimit_LiBatt(TRUE);
	//	}
	//	else
	//	{	
	//		GC_SafeMode_CurrLimit_LiBatt(FALSE);
	//	}
	//	return;
	//}
	//else
	//{
	//	//~{V;R*o?=o?=b5=o?=o?=o?=~}or~{o?=o?=J1o?=o?=o?=o?=o?=K3o?=o?=o?=H+D#J=~}
	//	//dwUsedDelayTimes = dwSafeModeDelayTimes;		
	//}

	if(s_bFirstRun)
	{

		for(i = 0; i < GC_Get_Total_BattNum(); i++)
		{
			if(!(g_pGcData->RunInfo.Bt.abValid[i]))
			{
				for(j = 0; j < SIZE_BATT_CURR_BUF; j++)
				{
					 bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT,
						i,
						BT_PRI_CURR,
						&(s_afFIFO[i][j]));
					 if(bRTN == FALSE)
						 continue;
				}
				s_aiHead[i] = 0;
				s_afBattCurrOut[i] = g_pGcData->RunInfo.Bt.afBattCurr[i];
			}
		}
		s_bFirstRun = FALSE;
	}

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i])
		{

			s_afBattCurrOut[i] = Filter_Down(s_afBattCurrOut[i],
				g_pGcData->RunInfo.Bt.afBattCurr[i],
				&(s_afFIFO[i][0]),
				afSortBuf,
				&(s_aiHead[i]),
				SIZE_BATT_CURR_BUF,
				SIZE_BATT_CURR_MID);
			//TRACE("GC:**** i = %d, BattCurrInput[i] = %.02f, BattCurrOut[i] = %.02f, ***\n", 
			//	i,
			//	g_pGcData->RunInfo.Bt.afBattCurr[i],
			//	s_afBattCurrOut[i]);
		}
	}

	if(!(g_pGcData->RunInfo.Rt.iQtyOnWorkRect))
	{
		return;
	}
	//~{T$o?=o?=o?=o?=W4L,R;V1o?=o?=o?=o?=~},~{V1o?=o?=o?=o?=J1o?=o?=N*V9o?=o?=o?=o?=o?=o?=o?=o?=o?=H%o?=o?~}?imer
	//if(SEC_TMR_TIME_IN 
	//	== GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY))
	//{
	//	return;
	//}
	//~{o.5o?=o?~}?0s~{o?=o?=R;o?=N#o?=o?=o?=o?=o?=H%o?=o?=~}timer
	//if(SEC_TMR_TIME_IN 
	//	== GC_GetStateOfSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL))
	//{
	//	return;
	//}

	//GC_SetSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL, 0, GC_GetCurrLmtPeriod, 0);

	//~{W"o?=b#,o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=J9o?=o?~}?~{o?=o?=N*o?=o?=N;o?=o?=o?=o?=o?=O5o?=X2o?=M~} 
	//g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
	//	= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);
	g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_LIBATT);


	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			//fBtRatedCap= g_pGcData->RunInfo.Bt.afBattRatedCap[i];

			float fDiffer;
			//if(s_afBattCurrOut[i] > (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
			//	* GC_CURR_LMT_SET_POINT_LOWER))
			//	/** fBtRatedCap * GC_CURR_LMT_SET_POINT_LOWER))*/
			//{
			//	bAllBattUnderLowLmt = FALSE;
			//}

			fDiffer = (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
				 * GC_CURR_LMT_SET_POINT_LOWER)
				- s_afBattCurrOut[i];
			fMinPosDiff = MIN(fMinPosDiff, fDiffer);

			if (s_afBattCurrOut[i] > (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
				 * GC_CURR_LMT_SET_POINT_UPPER))
			{
				fDiffer = (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
					 * GC_CURR_LMT_SET_POINT_UPPER)
					- s_afBattCurrOut[i];
				fSumOfNegCurr += fDiffer;
				bBattOverHighLmt = TRUE;
			}
		}
		else
		{
			//do nothing
		}
	}
	BOOL bNeedReleaseCurr = FALSE;
	//~{o?=o?=o?=o?=~}1~{o?=o?=o?=\5o?=o?=o?=~}<0.5*ABCL*No,~{o?=o?=o?=o?=o?=^5o?=X=o?=o?=o?~}?
	float fLiTotalCurr = g_pGcData->RunInfo.Bt.fSumOfCurr;	
	if((!g_pGcData->RunInfo.Bt.iQtyValidBatt) || 
		(fLiTotalCurr < 0.5*g_pGcData->RunInfo.Bt.fCurrLmtSetPoint*g_pGcData->RunInfo.Bt.iQtyValidBatt))
	{
		
		bNeedReleaseCurr = TRUE;
	}
	//~{o?=o?=W<o?=o?=~}ABCL
	float fStandardCurrLmt = 100.0 * (g_pGcData->RunInfo.fSysLoad 
		+ g_pGcData->RunInfo.Bt.iQtyValidBatt*g_pGcData->RunInfo.Bt.fCurrLmtSetPoint)
		/ (g_pGcData->RunInfo.Rt.fRatedCurr * g_pGcData->RunInfo.Rt.iQtyOfRect);
	//~{o?=o?=o?=o?=~}2~{o?=o?=o?=P5o?=o?~}?isconnected
	BOOL bHasBattDisconn = FALSE;
	DWORD dwDisBattNum= GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LIGROUP_DISCONN_BATT_NUM);
	if(dwDisBattNum > 0)
	{
		//~{o?=P5o?=o?~}?isconn~{o?=K#o?=o?=J5o?=o?=o?=o?=o?=o?=o?=o?=o?=~}Load+1
		bHasBattDisconn = TRUE;
		fStandardCurrLmt = 100.0 * (g_pGcData->RunInfo.fSysLoad 
			+ 1*g_pGcData->RunInfo.Bt.fCurrLmtSetPoint)
			/ (g_pGcData->RunInfo.Rt.fRatedCurr * g_pGcData->RunInfo.Rt.iQtyOfRect);
	}
	
	if((!bBattOverHighLmt) && (bNeedReleaseCurr) && (!bHasBattDisconn)) //~{o?=o?=o?=o?=o?=o?=o?=o?=~}
	{
		//ABCL not activate
		bExceedCurr = FALSE;
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_ABCL_IS_ACTIVE,GC_NORMAL,TRUE);
		if(g_pGcData->RunInfo.Rt.fCurrLmt < fStandardCurrLmt)
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = fStandardCurrLmt;
		}
		else
		{
			g_pGcData->RunInfo.Rt.fCurrLmt += 5.0;//~{C?o?=o?=o?=o?=o?=o?=~}5%~{o?=o?=V1o?=o?=H+o?=o?=o?=E?o?=~}
		}
	}
	else
	{
		//ABCL activate
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_ABCL_IS_ACTIVE,GC_ALARM,TRUE);

		if(bHasBattDisconn)
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = fStandardCurrLmt;
		}
		else if((fSumOfNegCurr > 0) && (fMinPosDiff > 0))
			//Not exceed battery current limit, need to relax rectifers current limit
		{
			float	fDelta;

			fDelta = 100.0 * fMinPosDiff 
				/ (fRtRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect);

			if(fDelta < 2)
			{
				fDelta = fDelta * 0.5;
				if(fDelta < 0.1)
				{
					fDelta = 0.1;
				}
			}
			g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;
		}
		else if((fSumOfNegCurr < 0) //~{G7Q9o?=o?=J1o?=o?=o?=o?=o?=o?=R*o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=R*o?=o?=~}
			/*&& (g_pGcData->RunInfo.fSysVolt > (1.0 + GC_GetSettingVolt(DC_PUB_UNDER_VOLT)))*/)
		//Exceed battery current limit, need to reduce rectifers current limit
		{

			float	fDelta;
			//float	fGain;

			if((g_pGcData->RunInfo.Rt.fCurrLmt 
				* g_pGcData->RunInfo.Rt.iQtyOnWorkRect 
				* fRtRatedCurr / 100) 
				> g_pGcData->RunInfo.Rt.fSumOfCurr * 1.05)
			{
				g_pGcData->RunInfo.Rt.fCurrLmt 
					= 100.0 * g_pGcData->RunInfo.Rt.fSumOfCurr 
					/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect
					* fRtRatedCurr);
			}

			//In order to avoid  under voltage by current limt. add fGain, if the system voltage is close to under voltage, 
			//reduce the current limit point.
			//fGain = 0.2 * (g_pGcData->RunInfo.fSysVolt - (1.0 
			//	+ GC_GetSettingVolt(DC_PUB_UNDER_VOLT)));

			//if (fGain > 1.0)
			//{
			//	fGain = 1.0;
			//}

			//fDelta = 100.0 * fGain * fSumOfNegCurr / (g_pGcData->RunInfo.Rt.iQtyOnWorkRect * fRtRatedCurr);
			fDelta = 100.0 * fSumOfNegCurr / (g_pGcData->RunInfo.Rt.iQtyOnWorkRect * fRtRatedCurr);

			g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;
			bExceedCurr = TRUE;

		}
		else
		{			
			//~{o?=o?=J1o?=o?=o?=P?o?=o?=o?=o?=o?=o?=P5o?=o?=H~} ~{o?=o?=o?=o?=o?=K#o?=o?=o?=R*o?=o?=o?=B=o?=o?=o?=~}safemode(~{o?=o?=C;o?=o?=C4K2o?=o?=o?=o?=_<o?=o?=o?=o?=o?=o?=H2o?=o?=o?=o?=o?~}?
			//if(g_pGcData->RunInfo.Bt.iQtyValidBatt 
			//	&& g_pGcData->RunInfo.Rt.fCurrLmt >= 0.9 * g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint)
			//{
			//	GC_SafeMode_CurrLimit_LiBatt();
			//	return;
			//}
		}
	}

	if(g_pGcData->RunInfo.Rt.fCurrLmt > GC_MAX_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
	}

	/* Added by Marco Yang, 2011-04-27, for NA's requirement:Limit the system max output */
	if(g_pGcData->RunInfo.Rt.fCurrLmt  > g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt  = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;
	}
	/* End by Marco Yang, 2011-04-27, for NA's requirement:Limit the system max output */

	if(g_pGcData->RunInfo.Rt.fCurrLmt < GC_MIN_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MIN_CURR_LMT;
	}

	/* Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
	if(GC_IsDislPwrLmtMode()) 
	{
		if(g_pGcData->RunInfo.Rt.fCurrLmt > fDislPwrLmtPoint)
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = fDislPwrLmtPoint;
		}
	}
	/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */

	if((g_pGcData->RunInfo.Rt.bMasterNoRespond || GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CAN_FAILURE)) && g_pGcData->enumMasterSlaveMode == MODE_MASTER)//master mode & (can failure | all rectifiers no response)
	{
		//release current limit
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF,
			GC_MAX_CURR_LMT);

		//for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		//{
		//	if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
		//	{
		//		GC_SetFloatValue(TYPE_OTHER, 
		//			0,
		//			RT_PUB_CURR_LMT_CTL_SELF + i,
		//			GC_MAX_CURR_LMT);
		//	}		
		//}
		GC_CurrlmtCtl_Slave(GC_MAX_CURR_LMT);
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
		return;
	}
	else if(GC_CURRLIMIT_DISABLE == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB))
	{
		/* Changed by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		if(GC_IsDislPwrLmtMode()) 
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				fDislPwrLmtPoint);
			g_pGcData->RunInfo.Rt.fCurrLmt = fDislPwrLmtPoint;
		}
		else
		{
			/*GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF,
			GC_MAX_CURR_LMT);*/
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint);
			g_pGcData->RunInfo.Rt.fCurrLmt = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;

		}
		/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		return;

	}
	else
	{
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF,
			g_pGcData->RunInfo.Rt.fCurrLmt);
	}

	float fCurrentLimitx;
	int   iCmpResult = 0;
	if(!bExceedCurr && (ST_FLOAT == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) || ST_AUTO_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		) //not exceed current limit
	{

		fCurrentLimitx = 100 * (g_pGcData->RunInfo.fSysLoad + g_pGcData->RunInfo.Bt.fSumOfCurr)/(g_pGcData->RunInfo.Rt.iQtyOnWorkRect * fRtRatedCurr);//fSumOfNegCurr

		if(fCurrentLimitx > 5.0 && fCurrentLimitx < 25.0)
		{
			fCurrentLimitx = fCurrentLimitx - 3.0; 

		}
		iCmpResult = GC_CmpMasterSlaveCurr();
		if( iCmpResult != 0)
		{
			fSlaveCurrLmt += iCmpResult;

		}
		else
		{
			fSlaveCurrLmt += 0.0;
		}

		if(fSlaveCurrLmt > 3.0 )
		{
			fSlaveCurrLmt = 3.0;
		}
		else if(fSlaveCurrLmt < -3.0)
		{
			fSlaveCurrLmt = -3.0;
		}


		fCurrentLimitx = fCurrentLimitx + fSlaveCurrLmt;


		//for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		//{
		//	if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
		//	{
		//		GC_SetFloatValue(TYPE_OTHER, 
		//			0,
		//			RT_PUB_CURR_LMT_CTL_SELF + i,
		//			fCurrentLimitx);
		//	}		
		//}
		GC_CurrlmtCtl_Slave(fCurrentLimitx);

	}
	else 
	{
		//for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		//{
		//	if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
		//	{
		//		GC_SetFloatValue(TYPE_OTHER, 
		//			0,
		//			RT_PUB_CURR_LMT_CTL_SELF + i,
		//			g_pGcData->RunInfo.Rt.fCurrLmt);
		//	}		
		//}
		GC_CurrlmtCtl_Slave(g_pGcData->RunInfo.Rt.fCurrLmt);
	}



	TRACE("GC_CurrentLmt Li Batt: fCurrLmt = %f, time = %d fCurrentLimitx = %f,g_pGcData->RunInfo.Rt.fCurrLmt = %f\n", g_pGcData->RunInfo.Rt.fCurrLmt, time(NULL),fCurrentLimitx,g_pGcData->RunInfo.Rt.fCurrLmt);

	return;
}
/*********************************************************************************
*  
*  FUNCTION NAME : BattCurrStatistic
*  CREATOR       : 
*  CREATED DATE  : 
*  DESCRIPTION   : 
*  CHANGE HISTORY: 
*  
***********************************************************************************/
void  BattCurrStatistic(GC_BATTCURR_INFO_TYPE* pBattInfo,int mode)
{
	int		i, j;
	float		fSumOfNegCurr = 0.0001;
	float		fMinPosDiff = 100000.0;
	float		fMinPosToHalfStepDiff = 100000.0;
	float		fMaxNegDiff = 0;
	float		fMaxNegDiffToCurrUpLimit = 0;
	float		afSortBuf[SIZE_BATT_CURR_BUF];
	float		fOverCurrLimit;
	float		fMinPosAdjust = 0;
	float		fMaxNegAdjust = 0;
	float		fBtRatedCap;

	static BOOL	s_bFirstRun = TRUE;
	static float	s_afFIFO[MAX_NUM_BATT_EQUIP][SIZE_BATT_CURR_BUF];
	static int		s_aiHead[MAX_NUM_BATT_EQUIP];
	static float	s_afBattCurrOut[MAX_NUM_BATT_EQUIP];

	BOOL		bRTN;
	BOOL		bOverCurrFlag = FALSE;
	BOOL		bAllBattUnderLowLmt = TRUE;
	BOOL		bBattOverHighLmt = FALSE;
	BOOL		bBattCurrLmtAlarm = FALSE;
	BOOL		bBattCurrLmtNormal = TRUE;

	float		Uplimit, Dnlimit,fBattCurrUpLimit;
	float		fBCLAlarmlimit,fBCLNormallimit ;

	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_BCL_ALARM_POINT_VOLT_MODE))
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_BCL_ALARM_POINT_VOLT_MODE,&fBCLAlarmlimit);
		if(bRTN == FALSE)
		{
			fBCLAlarmlimit = GC_BCL_ALARM_VOLT_MODE;
		}
	}
	else
	{
		fBCLAlarmlimit = GC_BCL_ALARM_VOLT_MODE;
	}
	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_BCL_NORMAL_POINT_VOLT_MODE))
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_BCL_NORMAL_POINT_VOLT_MODE,&fBCLNormallimit);
		if(bRTN == FALSE)
		{
			fBCLNormallimit = GC_BCL_NORMAL_VOLT_MODE;
		}
	}
	else
	{
		fBCLNormallimit = GC_BCL_NORMAL_VOLT_MODE;
	}
	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_BATT_CURR_UP_LIMIT))
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_BATT_CURR_UP_LIMIT,&fBattCurrUpLimit);
		if(bRTN == FALSE)
		{
			fBattCurrUpLimit = g_pGcData->RunInfo.Bt.fCurrLmtSetPoint;
		}
	}
	else
	{
		fBattCurrUpLimit = g_pGcData->RunInfo.Bt.fCurrLmtSetPoint;
	}

	if(mode == mCURRENTMODE)
	{
		Uplimit = GC_CURR_LMT_SET_POINT_UPPER;
		Dnlimit = GC_CURR_LMT_SET_POINT_LOWER;
	}
	else
	{
		if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_BATT_CURR_STABLE_UP_COEFF))
		{

			bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_BATT_CURR_STABLE_UP_COEFF,&Uplimit);
			if(bRTN == FALSE)
			{
				Uplimit = GC_VOLT_LMT_SET_POINT_UPPER;
			}
		}
		else
		{
			Uplimit = GC_VOLT_LMT_SET_POINT_UPPER;
		}
		if(fBattCurrUpLimit< (Uplimit * g_pGcData->RunInfo.Bt.fCurrLmtSetPoint))
		{
			Uplimit = fBattCurrUpLimit/g_pGcData->RunInfo.Bt.fCurrLmtSetPoint;
		}
		if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_BATT_CURR_STABLE_DN_COEFF))
		{
			bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_BATT_CURR_STABLE_DN_COEFF,&Dnlimit);
			if(bRTN == FALSE)
			{

				Dnlimit = GC_VOLT_LMT_SET_POINT_LOWER;
			}
		}
		else
		{
			Dnlimit = GC_VOLT_LMT_SET_POINT_LOWER;
		}

	}

	if(s_bFirstRun)
	{
		for(i = 0; i < GC_Get_Total_BattNum(); i++)
		{

			if(g_pGcData->RunInfo.Bt.abValid[i])
			{
				for(j = 0; j < SIZE_BATT_CURR_BUF; j++)
				{
					bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT,
						i,
						BT_PRI_CURR,
						&(s_afFIFO[i][j]));
					if(bRTN == FALSE)
						continue;
				}
				s_aiHead[i] = 0;
				s_afBattCurrOut[i] = g_pGcData->RunInfo.Bt.afBattCurr[i];
			}
		}
		s_bFirstRun = FALSE;
	}

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i])
		{

			s_afBattCurrOut[i] = Filter_Down(s_afBattCurrOut[i],
				g_pGcData->RunInfo.Bt.afBattCurr[i],
				&(s_afFIFO[i][0]),
				afSortBuf,
				&(s_aiHead[i]),
				SIZE_BATT_CURR_BUF,
				SIZE_BATT_CURR_MID);
		}
	}

	//g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
	//	= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);     //~{N*o?=K<o?=o?=o?=~}energysave~{o?=o?=o?=o?=o?=o?=o?=o?=o?=o2;o?=o?=o?=T<o?=H!o?=o?=o?=C5o?=o?=o?=o?=o?=o?=c#,o?=o?=o?=o?=o?~}?nergysave~{o?=o?=H!~}
	fOverCurrLimit = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_OVER_CURR);
	

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			fBtRatedCap= g_pGcData->RunInfo.Bt.afBattRatedCap[i];
			//the control set value is expect value 
			//* GC_CURR_LMT_SET_POINT_LOWER to GC_CURR_LMT_SET_POINT_UPPER
			float	fDiffer;

			if(s_afBattCurrOut[i]
			> (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint
			   * fBtRatedCap *Dnlimit))
			{
				bAllBattUnderLowLmt = FALSE;
			}
			if(s_afBattCurrOut[i]
			> (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint
			* fBtRatedCap *fBCLAlarmlimit))
			{
				
				bBattCurrLmtAlarm = TRUE;
			}
			if(s_afBattCurrOut[i]
				> (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint
					* fBtRatedCap *fBCLNormallimit))
			{
				
				bBattCurrLmtNormal = FALSE;
			}

	fDiffer 
		= (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		* fBtRatedCap * Dnlimit)
		- s_afBattCurrOut[i];
	if(fMinPosDiff > fDiffer)
	{
		fMinPosDiff =  fDiffer;
		if(fBtRatedCap !=0)
		{
			fMinPosAdjust = fMinPosDiff / fBtRatedCap;
		}
	}
	
	fDiffer 
		= (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		* fBtRatedCap * GC_VOLT_LMT_SET_POINT_SLOW )
		- s_afBattCurrOut[i];
	if(fMinPosToHalfStepDiff > fDiffer)
	{
		fMinPosToHalfStepDiff =  fDiffer;
	}

	if (s_afBattCurrOut[i]
	> (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		* fBtRatedCap * Uplimit))
	{
		fDiffer 
			= (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
			* fBtRatedCap * Uplimit)
			- s_afBattCurrOut[i];
		fSumOfNegCurr += fDiffer;
		bBattOverHighLmt = TRUE;
		if(fMaxNegDiff> fDiffer)
		{
			fMaxNegDiff = fDiffer;
			if(fBtRatedCap !=0)
			{
				fMaxNegAdjust  = fMaxNegDiff / fBtRatedCap;
			}
			fMaxNegDiffToCurrUpLimit = (fBattCurrUpLimit * fBtRatedCap )- s_afBattCurrOut[i];
		}

	}
	if(s_afBattCurrOut[i] > (fOverCurrLimit* fBtRatedCap ))
	{
		bOverCurrFlag = TRUE;
	}

		}
	}
	pBattInfo->bAllBattUnderLowLmt= bAllBattUnderLowLmt;		//~{o?=G7o?=o?=o?=o?=P5o?=X5o?=o?=o?=o?=o?=P!o?=Z3o?=g76N'o?=o?=o?=o?=o?=Z4K7o?=N'o?=Z2o?=o?=Y5o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=~}
	pBattInfo->bBattOverHighLmt = bBattOverHighLmt;			//~{o?=G7o?=o?=P5o?=X5o?=o?=o?=o?=o?=o?=Z3o?=g76N'o?=o?=o?=o?=o?=o?=~}
	pBattInfo->bOverCurrFlag = bOverCurrFlag;			//~{o?=G7o?=o?=P5o?=X9o?=o?=o?~}?
	pBattInfo->fMinPosDiff = fMinPosDiff;				//~{o?=o?=g76N'o?=o?=o?=o?=o?=o?=o?=o?=D?G0o?=o?=X5o?=o?=o?=o?=o?=o?=o?=P!o?=D2o?=V5~}
	pBattInfo->fSumOfNegCurr = fSumOfNegCurr;			//~{o?=o?=o?=P3o?=o?=o?=o?=o?=g76N'o?=o?=o?=^5D5o?=X5o?=o?=o?=~}-~{o?=o?=g76N'o?=o?=o?=o?=V~} ~{o?=o?=~}
	pBattInfo->fMaxNegAdjust = fMaxNegAdjust;			//~{o?=o?=o?=P5o?=X5o?=o?=o?=o?=P3o?=o?=o?=o?=o?=g76N'o?=o?=o?=o?=o?=o?=o?=D2o?=V5~}			
	pBattInfo->fMinPosAdjust = fMinPosAdjust;			//~{o?=o?=o?=P5o?=X5o?=o?=o?=o?=o?=P!o?=Z3o?=g76N'o?=o?=o?=o?=o?=o?=P!o?=D2o?=V5~}
	pBattInfo->fMaxNegDiffToCurrUpLimit = fMaxNegDiffToCurrUpLimit;	//~{o?=o?=o?=P3o?=o?=o?=o?=o?=g76N'o?=o?=o?=^5D5o?=X5o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=V~}?
	pBattInfo->fMinPosToHalfStepDiff = fMinPosToHalfStepDiff;	//~{o?=o?=o?=P5o?=X5o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=R;o?=o?=V5V~} ~{o?=o?=o?=o?=o?=P!V~}?
	pBattInfo->bBattCurrLmtAlarm = bBattCurrLmtAlarm;		//~{o?=o?=Q9o?=o?=o?=o?=D#J=o?=o?=o?=P5o?=X5o?=o?=o?=o?=o?=o?=o?=o?=o?=V~}?
	pBattInfo->bBattCurrLmtNormal = bBattCurrLmtNormal;		//~{o?=o?=Q9o?=o?=o?=o?=D#J=o?=o?=o?=o?=o?=P5o?=X5o?=o?=o?=P!o?=o?=o?=o?=V~}?
}

/********************************************************/
/* Max(LVDVolt ~{o?=o?=~} 47)					*/
/********************************************************/

float GetMinAdjustVolt(void)
{
	float fLvdVolt, fMaxLvdVolt;
	BOOL bRTN;
	fMaxLvdVolt = 0;
	int i;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	if (GC_EQUIP_EXIST !=GC_IsGCEquipExist(TYPE_OTHER, 0, LDU_PUB_EXISTENCE,GC_EQUIP_INVALID))
	{
		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{
			if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
			  &&(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0])
			 &&( GC_ENABLED  == GC_IsGCEquipExist(TYPE_LVD_UNIT, i,LVD1_PRI_ENB ,GC_DISABLED)))
			{
				//if(g_pGcData->RunInfo.Rt.fRatedVolt < GC_SYSTEMVOLT_JUDGE)
				if(stVoltLevelState)//24V
				{
					bRTN = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_VOLT24 ,
						&fLvdVolt );
				}
				else
				{
					bRTN  = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_VOLT,
						&fLvdVolt);

				}
			
				if((bRTN == TRUE)&&(fMaxLvdVolt <fLvdVolt))
				{
					fMaxLvdVolt =fLvdVolt;
				}
			}

			if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
			   &&(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1])
			   &&( GC_ENABLED  == GC_IsGCEquipExist(TYPE_LVD_UNIT, i,LVD1_PRI_ENB+1 ,GC_DISABLED)))
			{
				//if(g_pGcData->RunInfo.Rt.fRatedVolt < GC_SYSTEMVOLT_JUDGE)
				if(stVoltLevelState)//24V
				{
					bRTN = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_VOLT24+1 ,
						&fLvdVolt );
				}
				else
				{
					bRTN  = GC_GetFloatValueWithStateReturn(TYPE_LVD_UNIT, 
						i,
						LVD1_PRI_VOLT+1,
						&fLvdVolt);

				}
			
				if((bRTN == TRUE)&&(fMaxLvdVolt <fLvdVolt))
				{
					fMaxLvdVolt =fLvdVolt;
				}
			}
		}
	}
	else
	{
		
		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{
			if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
				&&(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0])
				&&( GC_ENABLED == GC_IsGCEquipExist(TYPE_OTHER, 0,LDU_LVD1_PRI_ENB,GC_EQUIP_INVALID)))
			{
				//if(g_pGcData->RunInfo.Rt.fRatedVolt < GC_SYSTEMVOLT_JUDGE)
				if(stVoltLevelState)//24V
				{
					bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
						0,
						LDU_LVD1_PRI_VOLT24,
						&fLvdVolt );
				}
				else
				{
					bRTN  = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
						0,
						LDU_LVD1_PRI_VOLT ,
						&fLvdVolt);

				}
			}
			break;
		}

		if((bRTN == TRUE)&&(fMaxLvdVolt <fLvdVolt))
		{
			fMaxLvdVolt =fLvdVolt;
		}

		for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
		{
			if((g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
				&&(GC_CONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1])
				&&( GC_ENABLED == GC_IsGCEquipExist(TYPE_OTHER, 0,LDU_LVD2_PRI_ENB,GC_EQUIP_INVALID)))
			{
				//if(g_pGcData->RunInfo.Rt.fRatedVolt < GC_SYSTEMVOLT_JUDGE)
				if(stVoltLevelState)//24V
				{
					bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
						0,
						LDU_LVD2_PRI_VOLT24,
						&fLvdVolt );
				}
				else
				{
					bRTN  = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 
						0,
						LDU_LVD2_PRI_VOLT,
						&fLvdVolt);

				}
			}
			break;
		}


		if((bRTN == TRUE)&&(fMaxLvdVolt <fLvdVolt))
		{
			fMaxLvdVolt =fLvdVolt;
		}
	}
	fMaxLvdVolt = fMaxLvdVolt + GC_VOLTADJUST_DELTA;
	//if(g_pGcData->RunInfo.Rt.fRatedVolt < GC_SYSTEMVOLT_JUDGE)
	if(stVoltLevelState)//24V
	{
		if(fMaxLvdVolt <GC_VOLTADJUST_MINVALUE_24V)
			fMaxLvdVolt = GC_VOLTADJUST_MINVALUE_24V;
	}
	else
	{	
		if(fMaxLvdVolt <GC_VOLTADJUST_MINVALUE_48V)
			fMaxLvdVolt =GC_VOLTADJUST_MINVALUE_48V;
	}
	return  fMaxLvdVolt;	
}
float AdjustVoltCompareWithLimit(float InputValue ,float Uplimit, float Dnlimit)
{
	if(InputValue >0)
	{
			if(InputValue>Uplimit)
			{
				InputValue =Uplimit ;
			}
			else if	(InputValue <Dnlimit)
			{
				InputValue = Dnlimit;	
			}
	}
	else
	{
			InputValue = -InputValue;
			if(InputValue>Uplimit)
			{
				InputValue =Uplimit ;
			}
			else if	(InputValue <Dnlimit)
			{
				InputValue = Dnlimit;	
			}
			InputValue = -InputValue;

	}
	return  InputValue;
}
/************************************************************************/
/*Vs = Vsp + (Is ~{o?~}? Ia) x G x N						*/
/* = Set Voltage to rectifiers						*/
/*Vsp = Previous Set Voltage						*/
/*Is = Set charge current limit in ~{o?=o?=~}C10~{o?=o?=~}				*/
/*Ia = Actual charge current in ~{o?=o?=~}C10~{o?=o?=~}					*/
/*G = Gain factor. Typical value for Lead Acid batteries = 0.15V/cell	*/
/*N = Number of battery cells.						*/
/************************************************************************/

void CurrentLimitThroughVolt(BOOL bNeedMinOutputFlag,int iSigId, BOOL bNeedTempComp)
{
	float fRectNeedOutputVolt,fBusVolt,fGain,fLowVolt,fSystemVolt;
	float fVoltTemp,fVoltStepLimitUp,fVoltStepLimitDn;
	int iBattNum;
	static int iVoltAdjustTime = 0;
	static int iVoltPosAdjustCount = 0;
	static int iLowVoltFlag = 0;
	//static int PreLimitFlag = GC_PRELIMIT_OVER;
	BOOL bRTN, bTempCompActive;
		
	GC_BATTCURR_INFO_TYPE BattInfo;

	float fSlowCoeff,fFastCoeff,fMinStep,fMaxStep;
	int   iAdjustCycleNum;



	
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);


		


	bTempCompActive  = FALSE;

	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_VOLT_ADJUST_SLOW_COEFF))
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0,GC_VOLT_ADJUST_SLOW_COEFF,&fSlowCoeff);
		if(bRTN == FALSE)
		{
			fSlowCoeff= GC_BATT_ADJUST_SLOW_COEFF;
		}
	}
	else
	{
		fSlowCoeff= GC_BATT_ADJUST_SLOW_COEFF;
	}

	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_VOLT_ADJUST_FAST_COEFF))
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_VOLT_ADJUST_FAST_COEFF,&fFastCoeff);
		if(bRTN == FALSE)
		{
			fFastCoeff= GC_BATT_ADJUST_FAST_COEFF;
		}
	}
	else
	{
		fFastCoeff= GC_BATT_ADJUST_FAST_COEFF;
	}

	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_VOLT_ADJUST_MIN_STEP))
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_VOLT_ADJUST_MIN_STEP,&fMinStep);
		if(bRTN == FALSE)
		{
			fMinStep = GC_VOLT_MIN_STEP;
		}
	}
	else
	{
		fMinStep = GC_VOLT_MIN_STEP;
	}

	if(GC_IsGCSigExist(TYPE_OTHER, 0, GC_VOLT_ADJUST_MAX_STEP))
	{
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_VOLT_ADJUST_MAX_STEP,&fMaxStep);
		if(bRTN == FALSE)
		{
			fMaxStep = GC_VOLT_MIN_STEP;
		}
	}
	else
	{
		fMaxStep = GC_VOLT_MIN_STEP;
	}

	if(GC_IsGCSigExist(TYPE_OTHER, 0,GC_VOLT_ADJUST_CYCLE_NUM))
	{
		iAdjustCycleNum =(int)GC_GetDwordValue(TYPE_OTHER, 0,GC_VOLT_ADJUST_CYCLE_NUM);
	}
	else
	{
		iAdjustCycleNum = GC_BATT_ADJUST_CYCLE_NUM;
	}


	BattCurrStatistic(&BattInfo,mVOLTAGEMODE);

	if((BattInfo.bOverCurrFlag == TRUE)||(bNeedMinOutputFlag == TRUE))
	{
		iLowVoltFlag = 2;
		iVoltAdjustTime = 0;
		iVoltPosAdjustCount = 0;
		fRectNeedOutputVolt = 	GetMinAdjustVolt();
		g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;

		g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_ALARM;

	}
	else
	{
		
		bRTN = GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_PUB_BATT_ADJUSTVOLT_GAIN,&fGain);
		if(bRTN == FALSE)
		{
			fGain = GC_BattADJUST_CELLCOEFF;
		}

		//if(g_pGcData->RunInfo.Rt.fRatedVolt < GC_SYSTEMVOLT_JUDGE)
		if(stVoltLevelState)//24V
		{
			iBattNum = GC_BATTCELLNUM_24V;
			fBusVolt  = GC_GetFloatValue(TYPE_OTHER,
				0,
				RT_PUB_DC_VOLT_CTL_24V);		
		}
		else
		{
			iBattNum = GC_BATTCELLNUM_48V;
			fBusVolt =GC_GetFloatValue(TYPE_OTHER,
				0,
				RT_PUB_DC_VOLT_CTL);
		}
		fVoltStepLimitUp = fMaxStep*iBattNum;	
		fVoltStepLimitDn = fMinStep *iBattNum;	

		fSystemVolt = GC_GetSysVolt();
		if((fBusVolt < fSystemVolt)&&(fSystemVolt< GC_GetSettingVolt(iSigId))
		   &&(iLowVoltFlag > 0)) //~{o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=B5o?=o?=Q9o?=s#,;X5o?=o?=o?=o?=o?=o?=o?=o?=B5o?=X7E5g!#o?=o?=o?=o?=o?=o?=D8o?=E5o?=Q9o?=o?=o?=o?=N~} ~{D8o?=E5o?=Q9o?=o?=o?=o?=o?=o?=o?=H2o?=o?=o?=o?=o?=o?=V5o?=o?=Z5D2o?=o?=o?=~}
		{	
		   fBusVolt = fSystemVolt;
		}
		if(iLowVoltFlag  >0)
		{
			iLowVoltFlag--;
		}


		fRectNeedOutputVolt = fBusVolt;

		if(g_pGcData->RunInfo.Bt.iVoltAdjustState == GC_VOLTADJUST_PROCESSING)
		{

			if(BattInfo.fMaxNegDiffToCurrUpLimit<-1)
			{
				fRectNeedOutputVolt = 	GetMinAdjustVolt();
				iLowVoltFlag = 2;
				iVoltPosAdjustCount = 0;
				iVoltAdjustTime = 0;
				g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_ALARM;
				

			}
			else if(BattInfo.fSumOfNegCurr<0)
			{
				fVoltTemp = BattInfo.fMaxNegAdjust*fGain*iBattNum;
				fVoltTemp = AdjustVoltCompareWithLimit(fVoltTemp,fVoltStepLimitUp,fVoltStepLimitDn);	
				fRectNeedOutputVolt = fBusVolt+ fVoltTemp  ;
				iVoltPosAdjustCount = 0;
				iVoltAdjustTime = 0;
				g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_ALARM;
				

			}
			else if(BattInfo.fMinPosDiff>0.5)
			{
				if(iVoltPosAdjustCount == 0)
				{
					iVoltPosAdjustCount =iAdjustCycleNum -1;

					if(BattInfo.fMinPosToHalfStepDiff>0)
					{
						fVoltTemp = BattInfo.fMinPosAdjust *fGain*iBattNum*fFastCoeff;
					}
					else
					{
						fVoltTemp = BattInfo.fMinPosAdjust *fGain*iBattNum*fSlowCoeff;
					}

					fVoltTemp = AdjustVoltCompareWithLimit(fVoltTemp,fVoltStepLimitUp,fVoltStepLimitDn);
					fRectNeedOutputVolt = fBusVolt+fVoltTemp;
				}
				else
				{
					if(iVoltPosAdjustCount >0)
						iVoltPosAdjustCount --;

				}
				
				
				fVoltTemp =  GC_GetSettingVolt(iSigId);
				if(iSigId ==BT_PUB_NOM_VOLT || iSigId == BT_PUB_BST_VOLT)
				{
					if((bNeedTempComp == TRUE)
						&& (!(g_pGcData->RunInfo.Rt.bNoRespond))
						&& (!GC_IsSmduInterrupt()))
					{
						fVoltTemp += GetTempCompDelta();
						bTempCompActive = TRUE;

					}
					fVoltTemp += GC_GetRegulateVolt(fVoltTemp);


				}
			
				
				if(((fVoltTemp-fRectNeedOutputVolt)> GC_CURRLIMITALARM_DELTA)||
				    (BattInfo.bBattCurrLmtAlarm == TRUE))
				{
					
					g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_ALARM;
				}
				else if(((fVoltTemp-fRectNeedOutputVolt)<= GC_CURRLIMITNORMAL_DELTA)&&
					(BattInfo.bBattCurrLmtNormal == TRUE))
				{
					
					g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_NORMAL;	
				}

				if(fRectNeedOutputVolt >fVoltTemp)
				{
					fRectNeedOutputVolt =fVoltTemp;
					if(CompareBattCurrAndBcToFcCurr())
					{
						if(iVoltAdjustTime < GC_VOLTADJUSTOK_TIME)
						{
							iVoltAdjustTime ++;
						}
						else
						{
							iVoltAdjustTime = 0;
							g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_OK;

						}
					
					}
					else
					{
						iVoltAdjustTime = 0;
					}
				}
				else
				{
					iVoltAdjustTime = 0;
				}
			}
			else
			{
				iVoltAdjustTime = 0;
				iVoltPosAdjustCount = 0;
				
			}
		}
		else
		{
			g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_NORMAL;	
			iVoltAdjustTime = 0;
			iVoltPosAdjustCount = 0;
			if(BattInfo.fSumOfNegCurr <0)
			{
				
				fRectNeedOutputVolt = 	GetMinAdjustVolt();
				iLowVoltFlag = 2;
				g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;
				g_pGcData->RunInfo.Bt.iBattCurrlimitFlag  = GC_ALARM;
			}
			else
			{
				fVoltTemp =  GC_GetSettingVolt(iSigId);
				if(iSigId ==BT_PUB_NOM_VOLT)
				{
					if((bNeedTempComp == TRUE)
						&& (!(g_pGcData->RunInfo.Rt.bNoRespond))
						&& (!GC_IsSmduInterrupt()))
					{
						fVoltTemp += GetTempCompDelta();
						bTempCompActive = TRUE;

					}
					fVoltTemp += GC_GetRegulateVolt(fVoltTemp);


				}
				if(fRectNeedOutputVolt < (fVoltTemp - fVoltStepLimitUp))
				{
					g_pGcData->RunInfo.Bt.iVoltAdjustState = GC_VOLTADJUST_PROCESSING;
					fRectNeedOutputVolt = fRectNeedOutputVolt + fVoltStepLimitUp ;
				}
				else
				{
					fRectNeedOutputVolt = fVoltTemp;
				}
				
				
			}
			
			
			
		}
		fLowVolt = GetMinAdjustVolt();
		if(fRectNeedOutputVolt < fLowVolt)
		{
			fRectNeedOutputVolt = fLowVolt;
			iLowVoltFlag = 2;
		}

	}

	GC_DcVoltCtl(fRectNeedOutputVolt);
	if(BattInfo.fSumOfNegCurr>0)
	{


	GC_SetFloatValue(TYPE_OTHER, 
						0,
						RT_PUB_CURR_LMT_CTL_SELF,
			GC_MAX_CURR_LMT);
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
		
	}
	if(bTempCompActive == FALSE)
	{
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			BT_PUB_TEMP_COMPEN_SET,
			GC_NORMAL,
			FALSE);	
	}
	
}

BOOL GC_IsNeedVoltAdjust(void)
{
	int	iCurrentLimitMode;
	SIG_ENUM  enumState;

	if(IsWorkIn_LiBattMode())
	{	
		return FALSE;
	}
	if((GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_LC_ENB))||
	   (GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPCL_ENB)))
	{	
		return FALSE;
	}
#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
	if(stState  !=  GC_MPPT_DISABLE)
	{
		return FALSE;
	}
#endif

	enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
	iCurrentLimitMode = (int) GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB);
	
	if(iCurrentLimitMode == GC_CURRLIMIT_VOLTMODE)
	{
		if(g_pGcData->enumMasterSlaveMode == MODE_STANDALONE)
		{
			if((g_pGcData->PriCfg.bSlaveMode == FALSE)&&(enumState== GC_HYBRID_DISABLED))
			{
				return TRUE;
			}
		}
	}

	return FALSE;

}




/*==========================================================================*
 * FUNCTION : GC_CurrentLmt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-26 10:03
 *==========================================================================*/
void GC_CurrentLmt(void)
{
	
	//~{o.5o?=o?=o?=o?=o?=o?=o?=o?=V1o?=o?=J9o?=o?=o?=d5%o?=o?=o?=o?=o?=c7~}?
	if(IsWorkIn_LiBattMode())
	{
		GC_CurrentLimit_LiBatt();
		return;
	}
	int		i, j;
	float	fRtRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	BOOL	bInDeadArea;
	static float fSlaveCurrLmt = 0.0;

	BOOL		bExceedCurr = FALSE;
	GC_BATTCURR_INFO_TYPE BattInfo;

	float	fLoadCurrLimitPoint;
	BOOL    bLoadCurrLimitFlag= FALSE;
	float fDislPwrLmtPoint = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DISL_PWR_LMT_POINT);

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
	if(stState != GC_MPPT_DISABLE )//Rect-MPPT
	{
		GC_CurrentLmt_Mppt();
		return;
	}
#endif
	

	BattCurrStatistic(&BattInfo,mCURRENTMODE);

	if(!(g_pGcData->RunInfo.Rt.iQtyOnWorkRect))
	{
		return;
	}

	if(SEC_TMR_TIME_IN 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY))
	{
		return;
	}

	if(SEC_TMR_TIME_IN 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL))
	{
		return;
	}



	GC_SetSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL, 0, GC_GetCurrLmtPeriod, 0);
	
	if(BattInfo.bAllBattUnderLowLmt || BattInfo.bBattOverHighLmt)
	{
		bInDeadArea = FALSE;
	}
	else
	{
		bInDeadArea = TRUE;	
	}
	
	if(g_pGcData->RunInfo.bLVDPreLmtToCurrLmtFlag == TRUE)
	{
		fLoadCurrLimitPoint = 100.0 *g_pGcData->RunInfo.fSysLoadofPreCurrLimit /(fRtRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect);
	}

	//printf("BattInfo.fSumOfNegCurr = %f,BattInfo.fMinPosDiff = %f\n",BattInfo.fSumOfNegCurr,BattInfo.fMinPosDiff );
	//If undervoltage(47V/23.5V), then try to relax 5 percent
	if((g_pGcData->RunInfo.bLVDPreLmtToCurrLmtFlag == TRUE)
	   &&(g_pGcData->RunInfo.Rt.fCurrLmt < fLoadCurrLimitPoint) )
	{
		bLoadCurrLimitFlag = TRUE;
		g_pGcData->RunInfo.Rt.fCurrLmt  =  fLoadCurrLimitPoint;
	}
	else if(GC_IsCurrLimitUnderVolt())
	{
		
		g_pGcData->RunInfo.Rt.fCurrLmt += 5.0;
	}
	else if(bInDeadArea)	
	{
		;
	}
	else
	{
		if((BattInfo.fSumOfNegCurr > 0) && (BattInfo.fMinPosDiff > 0))
		//Not exceed battery current limit, need to relax rectifers current limit
		{
			float	fDelta;

			fDelta = 100.0 * 0.90 * BattInfo.fMinPosDiff 
					/ (fRtRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect);

			if(fDelta < 2)
			{
				fDelta = fDelta * 0.5;
				if(fDelta < 0.1)
				{
					fDelta = 0.1;
				}
			}
			g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;
			//printf("(BattInfo.fSumOfNegCurr > 0) && (BattInfo.fMinPosDiff > 0) g_pGcData->RunInfo.Rt.fCurrLmt = %f  fDelta = %f\n", g_pGcData->RunInfo.Rt.fCurrLmt, fDelta);
		}
		else if((BattInfo.fSumOfNegCurr < 0)
			/*&& (g_pGcData->RunInfo.fSysVolt > (1.0 + GC_GetSettingVolt(DC_PUB_UNDER_VOLT)))*/)
		//Robert raised a TR to remove the DC_PUB_UNDER_VOLT judgment
		//Exceed battery current limit, need to reduce rectifers current limit
		{
			
			float	fDelta;
			//float	fGain;

			if((g_pGcData->RunInfo.Rt.fCurrLmt 
				* g_pGcData->RunInfo.Rt.iQtyOnWorkRect 
				* fRtRatedCurr / 100) 
				> g_pGcData->RunInfo.Rt.fSumOfCurr * 1.05)
			{
				g_pGcData->RunInfo.Rt.fCurrLmt 
					= 100.0 * g_pGcData->RunInfo.Rt.fSumOfCurr 
						/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect
							* fRtRatedCurr);
			}
			
			//In order to avoid  under voltage by current limt. add fGain, if the system voltage is close to under voltage, 
			//reduce the current limit point.
			//Robert raised a TR to remove the DC_PUB_UNDER_VOLT judgment
			/*fGain = 0.2 * (g_pGcData->RunInfo.fSysVolt - (1.0 
				+ GC_GetSettingVolt(DC_PUB_UNDER_VOLT)));

			if (fGain > 1.0)
			{
				fGain = 1.0;
			}*/
			//Robert raised a TR to remove the DC_PUB_UNDER_VOLT judgment
			//fDelta = 100.0 * fGain * BattInfo.fSumOfNegCurr
			//		/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect * fRtRatedCurr);

			fDelta = 100.0 * BattInfo.fSumOfNegCurr
				/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect * fRtRatedCurr);

			g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;
			bExceedCurr = TRUE;
			//printf("(BattInfo.fSumOfNegCurr < 0) g_pGcData->RunInfo.Rt.fCurrLmt = %f  fDelta = %f\n",g_pGcData->RunInfo.Rt.fCurrLmt, fDelta);

		}
	}

	if(g_pGcData->RunInfo.Rt.fCurrLmt > GC_MAX_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
	}

/* Added by Marco Yang, 2011-04-27, for NA's requirement:Limit the system max output */
	if(g_pGcData->RunInfo.Rt.fCurrLmt  > g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt  = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;
	}
/* End by Marco Yang, 2011-04-27, for NA's requirement:Limit the system max output */

	if(g_pGcData->RunInfo.Rt.fCurrLmt < GC_MIN_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MIN_CURR_LMT;
	}

/* Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
	if(GC_IsDislPwrLmtMode()) 
	{
		if(g_pGcData->RunInfo.Rt.fCurrLmt > fDislPwrLmtPoint)
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = fDislPwrLmtPoint;
		}

	}
/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */

	if((g_pGcData->RunInfo.Rt.bMasterNoRespond || GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CAN_FAILURE)) && g_pGcData->enumMasterSlaveMode == MODE_MASTER)//master mode & (can failure | all rectifiers no response)
	{
		//release current limit
		GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				GC_MAX_CURR_LMT);

		/*for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						RT_PUB_CURR_LMT_CTL_SELF + i,
						GC_MAX_CURR_LMT);
			}		
		}*/
		GC_CurrlmtCtl_Slave(GC_MAX_CURR_LMT);
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
		g_pGcData->RunInfo.bLVDPreLmtToCurrLmtFlag = FALSE;
		return;
	}
	else if(GC_CURRLIMIT_DISABLE == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB))
	{
		/* Changed by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		if(GC_IsDislPwrLmtMode()) 
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				fDislPwrLmtPoint);
			g_pGcData->RunInfo.Rt.fCurrLmt = fDislPwrLmtPoint;
		}
		else
		{
			/*GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				GC_MAX_CURR_LMT);*/
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				 g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint);
			g_pGcData->RunInfo.Rt.fCurrLmt = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;
			
		}
		/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		if(g_pGcData->enumMasterSlaveMode != MODE_MASTER)
		{
			g_pGcData->RunInfo.bLVDPreLmtToCurrLmtFlag = FALSE;
			return;
		}
		else
		{
			//Do nothing;
		}
		
	}
	else
	{
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF,
					g_pGcData->RunInfo.Rt.fCurrLmt);
	}

	float fCurrentLimitx;
	int   iCmpResult = 0;
	if(((!bExceedCurr)&&(bLoadCurrLimitFlag == FALSE))
	   || GC_CURRLIMIT_DISABLE == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB)) //not exceed current limit
	{

		fCurrentLimitx = 100 * (g_pGcData->RunInfo.fSysLoad + g_pGcData->RunInfo.Bt.fSumOfCurr)/(g_pGcData->RunInfo.Rt.iQtyOnWorkRect * fRtRatedCurr);//fSumOfNegCurr
		
		if(fCurrentLimitx > 5.0 && fCurrentLimitx < 25.0)
		{
			fCurrentLimitx = fCurrentLimitx - 3.0; 

		}
		iCmpResult = GC_CmpMasterSlaveCurr();
		if( iCmpResult != 0)
		{
			fSlaveCurrLmt += iCmpResult;
			
		}
		else
		{
			fSlaveCurrLmt += 0.0;
		}
		
		if(fSlaveCurrLmt > 3.0 )
		{
			fSlaveCurrLmt = 3.0;
		}
		else if(fSlaveCurrLmt < -3.0)
		{
			fSlaveCurrLmt = -3.0;
		}
		
		
		fCurrentLimitx = fCurrentLimitx + fSlaveCurrLmt;



		/*for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						RT_PUB_CURR_LMT_CTL_SELF + i,
						fCurrentLimitx);
			}		
		}*/
		GC_CurrlmtCtl_Slave(fCurrentLimitx);
		
	}
	else 
	{

		/*for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{
					GC_SetFloatValue(TYPE_OTHER, 
						0,
						RT_PUB_CURR_LMT_CTL_SELF + i,
						g_pGcData->RunInfo.Rt.fCurrLmt);
			}		
		}*/
		GC_CurrlmtCtl_Slave(g_pGcData->RunInfo.Rt.fCurrLmt);
		fSlaveCurrLmt = 0.0;
	}
	g_pGcData->RunInfo.bLVDPreLmtToCurrLmtFlag = FALSE;
	

	//printf("GC_CurrentLmt fCurrLmt = %f, time = %d fCurrentLimitx = %f,g_pGcData->RunInfo.Rt.fCurrLmt = %f\n", g_pGcData->RunInfo.Rt.fCurrLmt, time(NULL),fCurrentLimitx,g_pGcData->RunInfo.Rt.fCurrLmt);

	return;
}

#ifdef GC_SUPPORT_MPPT
/*==========================================================================*
* FUNCTION : GC_CurrentLmt_Mppt
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-26 10:03
*==========================================================================*/
void GC_CurrentLmt_Mppt(void)
{
	int		i, j;
	float	fSumOfNegCurr = 0.0001;
	float	fMinPosDiff = 100000.0;
	float	fBtRatedCap;
	float	fRtRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	BOOL	bInDeadArea;
	BOOL	bAllBattUnderLowLmt = TRUE;
	BOOL	bBattOverHighLmt = FALSE;
	static float fSlaveCurrLmt = 0.0;

	static BOOL		s_bFirstRun = TRUE;
	static float	s_afFIFO[MAX_NUM_BATT_EQUIP][SIZE_BATT_CURR_BUF];
	static int		s_aiHead[MAX_NUM_BATT_EQUIP];
	static float	s_afBattCurrOut[MAX_NUM_BATT_EQUIP];
	float			afSortBuf[SIZE_BATT_CURR_BUF];
	BOOL		bExceedCurr = FALSE,bRTN;

	SIG_ENUM	stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_WAY);
	float		fDislPwrLmtPoint = GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_DISL_PWR_LMT_POINT);

	if(stState == GC_MPPT_FIRST)
	{
		fRtRatedCurr = g_pGcData->RunInfo.Mt.fRatedCurr;
	}

	if(s_bFirstRun)
	{
		for(i = 0; i < GC_Get_Total_BattNum(); i++)
		{

			if(!(g_pGcData->RunInfo.Bt.abValid[i]))
			{
				for(j = 0; j < SIZE_BATT_CURR_BUF; j++)
				{
					bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT,
						i,
						BT_PRI_CURR,
						&(s_afFIFO[i][j]) );
					if(bRTN == FALSE)
						continue;
				}
				s_aiHead[i] = 0;
				s_afBattCurrOut[i] = g_pGcData->RunInfo.Bt.afBattCurr[i];
			}
		}
		s_bFirstRun = FALSE;
	}

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{

		if(g_pGcData->RunInfo.Bt.abValid[i])
		{

			s_afBattCurrOut[i] = Filter_Down(s_afBattCurrOut[i],
				g_pGcData->RunInfo.Bt.afBattCurr[i],
				&(s_afFIFO[i][0]),
				afSortBuf,
				&(s_aiHead[i]),
				SIZE_BATT_CURR_BUF,
				SIZE_BATT_CURR_MID);
			//TRACE("GC:**** i = %d, BattCurrInput[i] = %.02f, BattCurrOut[i] = %.02f, ***\n", 
			//	i,
			//	g_pGcData->RunInfo.Bt.afBattCurr[i],
			//	s_afBattCurrOut[i]);
		}
	}
	if(!(g_pGcData->RunInfo.Rt.iQtyOnWorkRect) && !(g_pGcData->RunInfo.Mt.iQtyOnWorkRect))
	{
		return;
	}

	//if(SEC_TMR_TIME_IN 
	//	== GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY))
	//{
	//	return;
	//}

	if(SEC_TMR_TIME_IN 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL))
	{
		return;
	}


	GC_SetSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL, 0, GC_GetCurrLmtPeriod, 0);


	g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);


	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			fBtRatedCap= g_pGcData->RunInfo.Bt.afBattRatedCap[i];
			float	fDiffer;

			if(s_afBattCurrOut[i]
				> (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint
					* fBtRatedCap * GC_CURR_LMT_SET_POINT_LOWER))
			{
				bAllBattUnderLowLmt = FALSE;
			}

			fDiffer = (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
				* fBtRatedCap * GC_CURR_LMT_SET_POINT_LOWER)
				- s_afBattCurrOut[i];
			fMinPosDiff = MIN(fMinPosDiff, fDiffer);

			if (s_afBattCurrOut[i]
					> (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
						* fBtRatedCap * GC_CURR_LMT_SET_POINT_UPPER))
			{
				fDiffer 
					= (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
					* fBtRatedCap * GC_CURR_LMT_SET_POINT_UPPER)
					- s_afBattCurrOut[i];
				fSumOfNegCurr += fDiffer;
				bBattOverHighLmt = TRUE;
			}
		}
	}

	if(bAllBattUnderLowLmt || bBattOverHighLmt)
	{
		bInDeadArea = FALSE;
	}
	else
	{
		bInDeadArea = TRUE;	
	}


	//If undervoltage(47V/23.5V), then try to relax 5 percent
	/*if(GC_IsCurrLimitUnderVolt())
	{

		if(stState == GC_RECT_FIRST)
		{
			if(FLOAT_EQUAL(g_pGcData->RunInfo.Rt.fCurrLmt, GC_MAX_CURR_LMT) || g_pGcData->RunInfo.Rt.bAcFail || !g_pGcData->RunInfo.Rt.iQtyOnWorkRect)
			{
				//Only release it when rectifier output max.
				g_pGcData->RunInfo.Mt.fCurrLmt += 5.0;
			}
			g_pGcData->RunInfo.Rt.fCurrLmt += 5.0;
		}
		else 
		{	
			if(FLOAT_EQUAL(g_pGcData->RunInfo.Mt.fCurrLmt, GC_MAX_CURR_LMT)|| g_pGcData->RunInfo.Mt.bNight || !g_pGcData->RunInfo.Mt.iQtyOnWorkRect)
			{
				g_pGcData->RunInfo.Rt.fCurrLmt += 5.0;				
			}
			g_pGcData->RunInfo.Mt.fCurrLmt += 5.0;
		}

	}
	else*/ if(bInDeadArea)
	{
	}
	else
	{
		//printf("fSumOfNegCurr = %f, fMinPosDiff = %f\n ", fSumOfNegCurr, fMinPosDiff);
		if((fSumOfNegCurr > 0) && (fMinPosDiff > 0))
			//Not exceed battery current limit, need to relax rectifers current limit
		{
			float	fDelta;
			
			
			if(stState == GC_RECT_FIRST)
			{
				fDelta = 100.0 * 0.90 * fMinPosDiff 
					/ (g_pGcData->RunInfo.Rt.fRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect);


				if(fDelta < 2)
				{
					fDelta = fDelta * 0.5;
					if(fDelta < 0.1)
					{
						fDelta = 0.1;
					}
				}	
				if(FLOAT_EQUAL(g_pGcData->RunInfo.Rt.fCurrLmt, GC_MAX_CURR_LMT) || g_pGcData->RunInfo.Rt.bAcFail || !g_pGcData->RunInfo.Rt.iQtyOnWorkRect)
				{
					fDelta = 100.0 * 0.90 * fMinPosDiff 
						/ (g_pGcData->RunInfo.Mt.fRatedCurr * g_pGcData->RunInfo.Mt.iQtyOnWorkRect);


					if(fDelta < 2)
					{
						fDelta = fDelta * 0.5;
						if(fDelta < 0.1)
						{
							fDelta = 0.1;
						}
					}	
					g_pGcData->RunInfo.Mt.fCurrLmt += fDelta;
				}

				g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;
				
				//printf("fDelta = %f\n", fDelta);
				//printf("GC_RECT_FIRST  ----2---- g_pGcData->RunInfo.Mt.fCurrLmt = %f,g_pGcData->RunInfo.Rt.fCurrLmt = %f\n", g_pGcData->RunInfo.Mt.fCurrLmt,g_pGcData->RunInfo.Rt.fCurrLmt);
			}
			else 
			{	
				fDelta = 100.0 * 0.90 * fMinPosDiff 
					/ (g_pGcData->RunInfo.Mt.fRatedCurr * g_pGcData->RunInfo.Mt.iQtyOnWorkRect);


				if(fDelta < 2)
				{
					fDelta = fDelta * 0.5;
					if(fDelta < 0.1)
					{
						fDelta = 0.1;
					}
				}
				if(FLOAT_EQUAL(g_pGcData->RunInfo.Mt.fCurrLmt, GC_MAX_CURR_LMT) || g_pGcData->RunInfo.Mt.bNight || !g_pGcData->RunInfo.Mt.iQtyOnWorkRect)
				{
					fDelta = 100.0 * 0.90 * fMinPosDiff 
						/ (g_pGcData->RunInfo.Rt.fRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect);


					if(fDelta < 2)
					{
						fDelta = fDelta * 0.5;
						if(fDelta < 0.1)
						{
							fDelta = 0.1;
						}
					}	
					g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;				
				}
				g_pGcData->RunInfo.Mt.fCurrLmt += fDelta;
				//printf("fDelta = %f\n", fDelta);
				//printf("Other----2--- g_pGcData->RunInfo.Mt.fCurrLmt = %f,g_pGcData->RunInfo.Rt.fCurrLmt = %f\n", g_pGcData->RunInfo.Mt.fCurrLmt,g_pGcData->RunInfo.Rt.fCurrLmt);
			}
		
		}
		else if((fSumOfNegCurr < 0)
			/*&& (g_pGcData->RunInfo.fSysVolt > (1.0 + GC_GetSettingVolt(DC_PUB_UNDER_VOLT)))*/)
		//Exceed battery current limit, need to reduce rectifers current limit
		{

			float	fDelta;
			//float	fGain;
			
			if((g_pGcData->RunInfo.Rt.fCurrLmt 
				* g_pGcData->RunInfo.Rt.iQtyOnWorkRect 
				* g_pGcData->RunInfo.Rt.fRatedCurr / 100) 
			> g_pGcData->RunInfo.Rt.fSumOfCurr * 1.05 || (g_pGcData->RunInfo.Mt.fCurrLmt 
						* g_pGcData->RunInfo.Mt.iQtyOnWorkRect 
						* g_pGcData->RunInfo.Mt.fRatedCurr / 100) 
					> g_pGcData->RunInfo.Mt.fSumOfCurr * 1.05)
			{
				g_pGcData->RunInfo.Rt.fCurrLmt 
					= 100.0 * (g_pGcData->RunInfo.Rt.fSumOfCurr +  g_pGcData->RunInfo.Mt.fSumOfCurr)
					/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect
					* g_pGcData->RunInfo.Rt.fRatedCurr + g_pGcData->RunInfo.Mt.iQtyOnWorkRect
					* g_pGcData->RunInfo.Mt.fRatedCurr );
				
			}
			//g_pGcData->RunInfo.Mt.fCurrLmt = g_pGcData->RunInfo.Rt.fCurrLmt;
			//printf("Over g_pGcData->RunInfo.Rt.fCurrLmt = %f\n", g_pGcData->RunInfo.Rt.fCurrLmt);
			

			//In order to avoid  under voltage by current limt. add fGain, if the system voltage is close to under voltage, 
			//reduce the current limit point.
			/*fGain = 0.2 * (g_pGcData->RunInfo.fSysVolt - (1.0 
				+ GC_GetSettingVolt(DC_PUB_UNDER_VOLT)));

			if (fGain > 1.0)
			{
				fGain = 1.0;
			}

			fDelta = 100.0 * fGain * fSumOfNegCurr
				/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect *  g_pGcData->RunInfo.Rt.fRatedCurr + g_pGcData->RunInfo.Mt.iQtyOnWorkRect * g_pGcData->RunInfo.Mt.fRatedCurr );
			*/
			fDelta = 100.0 * fSumOfNegCurr
				/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect *  g_pGcData->RunInfo.Rt.fRatedCurr + g_pGcData->RunInfo.Mt.iQtyOnWorkRect * g_pGcData->RunInfo.Mt.fRatedCurr );
			//printf("Mppt g_pGcData->RunInfo.Mt.fCurrLmt = %f g_pGcData->RunInfo.Rt.fCurrLmt = %f fDelta = %f\n", g_pGcData->RunInfo.Mt.fCurrLmt, g_pGcData->RunInfo.Rt.fCurrLmt, fDelta);
			if(g_pGcData->RunInfo.Mt.fCurrLmt + fDelta < 0)
			{
				g_pGcData->RunInfo.Mt.fCurrLmt = 0;
			
				g_pGcData->RunInfo.Rt.fCurrLmt += fDelta + g_pGcData->RunInfo.Mt.fCurrLmt + fDelta;

			}
			else if(g_pGcData->RunInfo.Rt.fCurrLmt + fDelta < 0)
			{
				g_pGcData->RunInfo.Rt.fCurrLmt = 0;
			
				g_pGcData->RunInfo.Mt.fCurrLmt += fDelta + g_pGcData->RunInfo.Rt.fCurrLmt + fDelta;

			}
			else
			{
					
				g_pGcData->RunInfo.Mt.fCurrLmt += fDelta;
					
				g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;
				
			}
			
			
			//printf("Over GC_RECT_FIRST  ----2---- g_pGcData->RunInfo.Mt.fCurrLmt = %f,g_pGcData->RunInfo.Rt.fCurrLmt = %f\n", g_pGcData->RunInfo.Mt.fCurrLmt,g_pGcData->RunInfo.Rt.fCurrLmt);
			
			bExceedCurr = TRUE;

		}
	}

	//printf("fSumOfNegCurr = %f, fMinPosDiff = %f, g_pGcData->RunInfo.Rt.fCurrLmt= %f\n",fSumOfNegCurr, fMinPosDiff, g_pGcData->RunInfo.Rt.fCurrLmt);
	if(g_pGcData->RunInfo.Rt.fCurrLmt > GC_MAX_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
	}

	if(g_pGcData->RunInfo.Rt.fCurrLmt < GC_MIN_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MIN_CURR_LMT;
	}

	//printf("fSumOfNegCurr = %f, fMinPosDiff = %f, g_pGcData->RunInfo.Rt.fCurrLmt= %f\n",fSumOfNegCurr, fMinPosDiff, g_pGcData->RunInfo.Rt.fCurrLmt);
	if(g_pGcData->RunInfo.Mt.fCurrLmt > GC_MAX_CURR_LMT)
	{
		g_pGcData->RunInfo.Mt.fCurrLmt = GC_MAX_CURR_LMT;
	}

	if(g_pGcData->RunInfo.Mt.fCurrLmt < GC_MIN_CURR_LMT)
	{
		g_pGcData->RunInfo.Mt.fCurrLmt = GC_MIN_CURR_LMT;
	}
	if(GC_IsDislPwrLmtMode()) 
	{
		
		if(g_pGcData->RunInfo.Rt.fCurrLmt > fDislPwrLmtPoint)
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = fDislPwrLmtPoint;
		}

		if(g_pGcData->RunInfo.Mt.fCurrLmt > fDislPwrLmtPoint)
		{
			g_pGcData->RunInfo.Mt.fCurrLmt = fDislPwrLmtPoint;
		}
	}


	if((g_pGcData->RunInfo.Rt.bMasterNoRespond && g_pGcData->RunInfo.Mt.bNoRespond)|| GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CAN_FAILURE)) //master mode & (can failure | all rectifiers no response)
	{
		//release current limit
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF,
			GC_MAX_CURR_LMT);
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
		//release current limit
		GC_SendMTCurrentLmt(GC_MAX_CURR_LMT);
		g_pGcData->RunInfo.Mt.fCurrLmt = GC_MAX_CURR_LMT;
		return;
	}
	else if(GC_CURRLIMIT_DISABLE == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB))
	{
		/* Changed by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		if(GC_IsDislPwrLmtMode()) 
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				fDislPwrLmtPoint);
			g_pGcData->RunInfo.Rt.fCurrLmt = fDislPwrLmtPoint;
			GC_SendMTCurrentLmt(fDislPwrLmtPoint);
			g_pGcData->RunInfo.Mt.fCurrLmt = fDislPwrLmtPoint;
		}
		else
		{
			GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				GC_MAX_CURR_LMT);
			g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
			GC_SendMTCurrentLmt(GC_MAX_CURR_LMT);
			g_pGcData->RunInfo.Mt.fCurrLmt = GC_MAX_CURR_LMT;
				

		}
		/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
		//printf("return -----4\n");
		return;

	}
	else
	{
		//printf("return ---2 RunInfo.Rt.fCurrLmt= %f  g_pGcData->RunInfo.Mt.fCurrLmt = %f\n", g_pGcData->RunInfo.Rt.fCurrLmt, g_pGcData->RunInfo.Mt.fCurrLmt);
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF,
			g_pGcData->RunInfo.Rt.fCurrLmt);
		
		GC_SendMTCurrentLmt(g_pGcData->RunInfo.Mt.fCurrLmt);
		
	}	

	return;
}
void GC_CurrentLmt_Mppt_Min(void)
{
	int		i, j;
	float	fSumOfNegCurr = 0.0001;
	float	fMinPosDiff = 100000.0;
	float	fBtRatedCap;
	float	fRtRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	BOOL	bInDeadArea;
	BOOL	bAllBattUnderLowLmt = TRUE;
	BOOL	bBattOverHighLmt = FALSE;
	static float fSlaveCurrLmt = 0.0;

	static BOOL		s_bFirstRun = TRUE;
	static float	s_afFIFO[MAX_NUM_BATT_EQUIP][SIZE_BATT_CURR_BUF];
	static int		s_aiHead[MAX_NUM_BATT_EQUIP];
	static float	s_afBattCurrOut[MAX_NUM_BATT_EQUIP];
	float			afSortBuf[SIZE_BATT_CURR_BUF];
	BOOL		bExceedCurr = FALSE,bRTN;
	float		fCurrLmt = 0.0;

	SIG_ENUM	stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_WAY);

	if(stState == GC_MPPT_FIRST)
	{
		fRtRatedCurr = g_pGcData->RunInfo.Mt.fRatedCurr;
	}

	if(s_bFirstRun)
	{
		for(i = 0; i < GC_Get_Total_BattNum(); i++)
		{

			if(!(g_pGcData->RunInfo.Bt.abValid[i]))
			{
				for(j = 0; j < SIZE_BATT_CURR_BUF; j++)
				{
					bRTN = GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT,
						i,
						BT_PRI_CURR,
						&(s_afFIFO[i][j]));
					
				}
				s_aiHead[i] = 0;
				s_afBattCurrOut[i] = g_pGcData->RunInfo.Bt.afBattCurr[i];
			}
		}
		s_bFirstRun = FALSE;
	}

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{

		if(g_pGcData->RunInfo.Bt.abValid[i])
		{

			s_afBattCurrOut[i] = Filter_Down(s_afBattCurrOut[i],
				g_pGcData->RunInfo.Bt.afBattCurr[i],
				&(s_afFIFO[i][0]),
				afSortBuf,
				&(s_aiHead[i]),
				SIZE_BATT_CURR_BUF,
				SIZE_BATT_CURR_MID);
			//TRACE("GC:**** i = %d, BattCurrInput[i] = %.02f, BattCurrOut[i] = %.02f, ***\n", 
			//	i,
			//	g_pGcData->RunInfo.Bt.afBattCurr[i],
			//	s_afBattCurrOut[i]);
		}
	}

	if(!(g_pGcData->RunInfo.Mt.iQtyOnWorkRect))
	{
		return;
	}	

	if(SEC_TMR_TIME_IN 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL))
	{
		return;
	}


	GC_SetSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL, 0, GC_GetCurrLmtPeriod, 0);


	g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		= GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT);


	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			fBtRatedCap= g_pGcData->RunInfo.Bt.afBattRatedCap[i];
			float	fDiffer;

			if(s_afBattCurrOut[i]
	> (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint
		* fBtRatedCap * GC_CURR_LMT_SET_POINT_LOWER))
	{
		bAllBattUnderLowLmt = FALSE;
	}

	fDiffer = (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		* fBtRatedCap * GC_CURR_LMT_SET_POINT_LOWER)
		- s_afBattCurrOut[i];
	fMinPosDiff = MIN(fMinPosDiff, fDiffer);

	if (s_afBattCurrOut[i]
	> (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
		* fBtRatedCap * GC_CURR_LMT_SET_POINT_UPPER))
	{
		fDiffer 
			= (g_pGcData->RunInfo.Bt.fCurrLmtSetPoint 
			* fBtRatedCap * GC_CURR_LMT_SET_POINT_UPPER)
			- s_afBattCurrOut[i];
		fSumOfNegCurr += fDiffer;
		bBattOverHighLmt = TRUE;
	}
		}
	}

	if(bAllBattUnderLowLmt || bBattOverHighLmt)
	{
		bInDeadArea = FALSE;
	}
	else
	{
		bInDeadArea = TRUE;	
	}

	//printf("fMinPosDiff = %f fSumOfNegCurr = %f\n",fMinPosDiff, fSumOfNegCurr);
	//If undervoltage(47V/23.5V), then try to relax 5 percent
	if(GC_IsCurrLimitUnderVolt())
	{
		g_pGcData->RunInfo.Mt.fCurrLmt += 5.0;	
	}
	else if(bInDeadArea)
	{
	}
	else
	{
		if((fSumOfNegCurr > 0) && (fMinPosDiff > 0))
			//Not exceed battery current limit, need to relax rectifers current limit
		{
			float	fDelta;		
				
			fDelta = 100.0 * 0.90 * fMinPosDiff 
				/ (g_pGcData->RunInfo.Mt.fRatedCurr * g_pGcData->RunInfo.Mt.iQtyOnWorkRect);


			if(fDelta < 2)
			{
				fDelta = fDelta * 0.5;
				if(fDelta < 0.1)
				{
					fDelta = 0.1;
				}
			}
			g_pGcData->RunInfo.Mt.fCurrLmt += fDelta;		
			

		}
		else if((fSumOfNegCurr < 0)
			/*&& (g_pGcData->RunInfo.fSysVolt > (1.0 + GC_GetSettingVolt(DC_PUB_UNDER_VOLT)))*/)
			//Exceed battery current limit, need to reduce rectifers current limit
		{

			float	fDelta;
			//float	fGain;
			//g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;

			if((g_pGcData->RunInfo.Mt.fCurrLmt 
						* g_pGcData->RunInfo.Mt.iQtyOnWorkRect 
						* g_pGcData->RunInfo.Mt.fRatedCurr / 100) 
					> g_pGcData->RunInfo.Mt.fSumOfCurr * 1.05)
			{
				fCurrLmt
					= 100.0 * (g_pGcData->RunInfo.Rt.fSumOfCurr +  g_pGcData->RunInfo.Mt.fSumOfCurr)
					/ (g_pGcData->RunInfo.Mt.iQtyOnWorkRect	* g_pGcData->RunInfo.Mt.fRatedCurr );

			}
			g_pGcData->RunInfo.Mt.fCurrLmt = fCurrLmt;			
			
			/*fGain = 0.2 * (g_pGcData->RunInfo.fSysVolt - (1.0 
				+ GC_GetSettingVolt(DC_PUB_UNDER_VOLT)));

			if (fGain > 1.0)
			{
				fGain = 1.0;
			}*/

			/*fDelta = 100.0 * fGain * fSumOfNegCurr
				/ (g_pGcData->RunInfo.Mt.iQtyOnWorkRect * g_pGcData->RunInfo.Mt.fRatedCurr );*/
			fDelta = 100.0 * fSumOfNegCurr 
				/ (g_pGcData->RunInfo.Mt.iQtyOnWorkRect * g_pGcData->RunInfo.Mt.fRatedCurr );

			g_pGcData->RunInfo.Mt.fCurrLmt += fDelta;

			

			bExceedCurr = TRUE;

		}
	}
	
	if(g_pGcData->RunInfo.Mt.fCurrLmt > GC_MAX_CURR_LMT)
	{
		g_pGcData->RunInfo.Mt.fCurrLmt = GC_MAX_CURR_LMT;
	}

	if(g_pGcData->RunInfo.Mt.fCurrLmt < GC_MIN_CURR_LMT)
	{
		g_pGcData->RunInfo.Mt.fCurrLmt = GC_MIN_CURR_LMT;
	}
	


	if(GC_CURRLIMIT_DISABLE == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB))
	{			

		GC_SendMTCurrentLmt(GC_MAX_CURR_LMT);
		g_pGcData->RunInfo.Mt.fCurrLmt = GC_MAX_CURR_LMT;	
		return;

	}
	else
	{
		GC_SendMTCurrentLmt(g_pGcData->RunInfo.Mt.fCurrLmt);	
	}	

	return;
}
#endif
#ifdef GC_SUPPORT_BMS
/*==========================================================================*
* FUNCTION : GC_CurrentLmt_BMS
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-10-26 10:03
*==========================================================================*/
void GC_CurrentLmt_BMS(void)
{
	
	int		i, j;
	float	fRtRatedCurr = g_pGcData->RunInfo.Rt.fRatedCurr;
	BOOL	bInDeadArea;
	float fMinPosDiff = 5000.0, fSumOfNegCurr = 0.0, fBtCurrent, fBatteryRatedCurrent;
	BOOL bProtect = FALSE;

	BOOL		bExceedCurr = FALSE, bRTN, bAllBattUnderLowLmt = TRUE, bBattOverHighLmt = FALSE;

	if(!(g_pGcData->RunInfo.Rt.iQtyOnWorkRect))
	{
		printf("-------------8---------\n");
		return;
	}

	//if(SEC_TMR_TIME_IN 
	//	== GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY))
	//{
	//	printf("-------------9---------\n");
	//	return;
	//}

	if(SEC_TMR_TIME_IN 
		== GC_GetStateOfSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL))
	{
		printf("-------------10---------\n");
		return;
	}


	GC_SetSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL, 0, GC_GetCurrLmtPeriod_BMS, 0);

	
	bRTN= GC_GetFloatValueWithStateReturn(TYPE_OTHER, 0, GC_PUB_BMS_CHARGE_CURRENT, &fBatteryRatedCurrent);

	if(bRTN == FALSE)
	{
		printf("-------------11---------\n");
		return;
	}

	for(i = 0; i < MAX_NUM_BMS_EQUIP; i++)
	{
		bRTN = GC_GetEnumValueWithReturn(TYPE_BMS_UNIT, i, BMS_PRI_PROTECT_STA, &bProtect);//
		if(bRTN && bProtect)
		{			
			break;
		}
		else if(bRTN == FALSE)
		{
			printf("-------------12---------\n");
			return;
		}
		bRTN= GC_GetFloatValueWithStateReturn(TYPE_BMS_UNIT, i, BMS_PRI_CURRENT, &fBtCurrent);//
		if(bRTN == FALSE)
		{
			printf("-------------13---------\n");
			return;
		}
		float	fDiffer;

		if(fBtCurrent > fBatteryRatedCurrent *0.97)
		{
			bAllBattUnderLowLmt = FALSE;
		}
		
		if (fBtCurrent > fBatteryRatedCurrent)
		{
			fDiffer 
				= fBatteryRatedCurrent	- fBtCurrent;
			fSumOfNegCurr += fDiffer;
			printf("fSumOfNegCurr = %f\n", fSumOfNegCurr);
			bBattOverHighLmt = TRUE;
		}
		else
		{
			fDiffer = fBatteryRatedCurrent - fBtCurrent;
			fMinPosDiff = MIN(fMinPosDiff, fDiffer);
			printf("fDiffer = %f, fMinPosDiff = %f \n", fDiffer, fMinPosDiff);

		}

		
	}

	GC_SetSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL, 0, GC_GetCurrLmtPeriod, 0);
	
	if(bProtect)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt 
					= 100*(g_pGcData->RunInfo.fSysLoad + fBatteryRatedCurrent * 0.5) 
						/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect
							* fRtRatedCurr);
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF,
					g_pGcData->RunInfo.Rt.fCurrLmt);
		printf("-------------14----g_pGcData->RunInfo.Rt.fCurrLmt = %f-----\n", g_pGcData->RunInfo.Rt.fCurrLmt);
		return;
	}
	if(bAllBattUnderLowLmt || bBattOverHighLmt)
	{
		bInDeadArea = FALSE;
	}
	else
	{
		bInDeadArea = TRUE;	
	}
	

	if(bInDeadArea)	
	{
		;
	}
	else
	{
		if(fSumOfNegCurr < 0)
		{
			
			float	fDelta;

			if((g_pGcData->RunInfo.Rt.fCurrLmt 
				* g_pGcData->RunInfo.Rt.iQtyOnWorkRect 
				* fRtRatedCurr / 100) 
				> g_pGcData->RunInfo.Rt.fSumOfCurr * 1.05)
			{
				g_pGcData->RunInfo.Rt.fCurrLmt 
					= 100.0 * g_pGcData->RunInfo.Rt.fSumOfCurr 
						/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect
							* fRtRatedCurr);
			}
			
			fDelta = 100.0 * fSumOfNegCurr
				/ (g_pGcData->RunInfo.Rt.iQtyOnWorkRect * fRtRatedCurr);

			g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;
			bExceedCurr = TRUE;
		}
		else if((fMinPosDiff > 0))
		//Not exceed battery current limit, need to relax rectifers current limit
		{
			float	fDelta;

			fDelta = 100.0 * 0.90 * fMinPosDiff 
					/ (fRtRatedCurr * g_pGcData->RunInfo.Rt.iQtyOnWorkRect);

			if(fDelta < 2)
			{
				fDelta = fDelta * 0.5;
				if(fDelta < 0.1)
				{
					fDelta = 0.1;
				}
			}
			g_pGcData->RunInfo.Rt.fCurrLmt += fDelta;
		}

	}

	if(g_pGcData->RunInfo.Rt.fCurrLmt > GC_MAX_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
	}
	if(g_pGcData->RunInfo.Rt.fCurrLmt < GC_MIN_CURR_LMT)
	{
		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MIN_CURR_LMT;
	}

	if(g_pGcData->RunInfo.Rt.bMasterNoRespond || GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CAN_FAILURE)) //master mode & (can failure | all rectifiers no response)
	{
		//release current limit
		GC_SetFloatValue(TYPE_OTHER, 
				0,
				RT_PUB_CURR_LMT_CTL_SELF,
				GC_MAX_CURR_LMT);

		g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
		printf("-------------15---------\n");
		return;
	}
	else if(GC_CURRLIMIT_DISABLE == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_CURR_LMT_ENB))
	{			
		GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF,
			 g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint);
		g_pGcData->RunInfo.Rt.fCurrLmt = g_pGcData->RunInfo.Rt.fMaxCurrLmtPoint;			
		
	}
	else
	{
		GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF,
					g_pGcData->RunInfo.Rt.fCurrLmt);
	}
	printf("-------------16---------\n");
	return;
}

#endif
/*==========================================================================*
 * FUNCTION : GetBattHiTempAlm
 * PURPOSE  : If there is one or more Battery Over Temperature alarm, 
			  return TRUE, otherwise return FALSE.
 * CALLS    : GC_GetEnumValue
 * CALLED BY: GC_CycPreBC GC_ManAutoBC
 * RETURN   : BOOL
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-05 10:40
 *==========================================================================*/
static BOOL GetBattHiTempAlm(void)
{
	//int		i;

	//for(i = 0; i < MAX_NUM_TEMPERATURE - MAX_NUM_LargeDU_TEMP; i++)
	//{
	//	SIG_ENUM	enumTempUsage = GC_GetEnumValue(TYPE_OTHER, 
	//							0,
	//							GC_PUB_TEMP1_USAGE + i);
	//	if((enumTempUsage == TEMP_USAGE_BATTERY)
	//		&& (GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
	//							0,
	//							GC_PUB_HI_TEMP1_ALM + i)))
	//	{
	//		return TRUE;
	//	}
	//}

	////LargeDUBattery Hi temperature alarm
	//for(i = 0; i < MAX_LDU_TEMPER_NUM; i++)
	//{		
	//	//printf("LargeDU Temperature%d status is %d\n", i, GC_GetEnumValue(TYPE_OTHER, 
	//	//						0,
	//	//						LDU1_PUB_TEMPERATURE_ALM1 + i));
	//	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
	//							0,
	//							LDU1_PUB_TEMPERATURE_ALM1 + i))
	//	{		

	//		return TRUE;
	//	}	
	//
	//}

	//return FALSE;

	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_BATTTEMP_HIGH_ALARM) 
		|| GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_BATTTEMP_VERYHIGH_ALARM) )
	{
		return TRUE;
	}

	return FALSE;


}




/*==========================================================================*
* FUNCTION : GetBattHiBTRMAlm
* PURPOSE  : If there is one or more Battery Over Temperature alarm, 
return TRUE, otherwise return FALSE.
* CALLS    : GC_GetEnumValue
* CALLED BY: GC_CycPreBC GC_ManAutoBC
* RETURN   : BOOL
* COMMENTS : 
* CREATOR  : Jimmy Wu                DATE: 2012-02-16 10:40
*==========================================================================*/
static BOOL GetBattHiBTRMAlm(void)
{
	//int		i;

	//for(i = 0; i < MAX_NUM_TEMPERATURE - MAX_NUM_LargeDU_TEMP; i++)
	//{
	//	SIG_ENUM	enumTempUsage = GC_GetEnumValue(TYPE_OTHER, 
	//							0,
	//							GC_PUB_TEMP1_USAGE + i);
	//	if((enumTempUsage == TEMP_USAGE_BATTERY)
	//		&& (GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
	//							0,
	//							GC_PUB_HI_TEMP1_ALM + i)))
	//	{
	//		return TRUE;
	//	}
	//}

	////LargeDUBattery Hi temperature alarm
	//for(i = 0; i < MAX_LDU_TEMPER_NUM; i++)
	//{		
	//	//printf("LargeDU Temperature%d status is %d\n", i, GC_GetEnumValue(TYPE_OTHER, 
	//	//						0,
	//	//						LDU1_PUB_TEMPERATURE_ALM1 + i));
	//	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
	//							0,
	//							LDU1_PUB_TEMPERATURE_ALM1 + i))
	//	{		

	//		return TRUE;
	//	}	
	//
	//}

	//return FALSE;

	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_BTRM_HIGH_ALARM) 
		|| GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_BTRM_VERYHIGH_ALARM) )
	{
		return TRUE;
	}

	return FALSE;


}




#define	HYSTERESIS_TIMES_VOLT_CTL	1
/*==========================================================================*
 * FUNCTION : GC_RectVoltCtl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE  byVolt      : 
 *            BOOL  bNeedTempComp : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-06 11:33
 *==========================================================================*/
void GC_RectVoltCtl(int iVoltType, BOOL bNeedTempComp)
{
	float		fVoltOfRt;
	float		fVoltOfMppt = 0.0;
	SIG_ENUM	stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
	BOOL		bTempCompActive = FALSE;
	
	switch (iVoltType)
	{
	case TYPE_FLOAT_VOLT:
			if(stState == GC_MPPT_MPPT_RECT || stState == GC_MPPT_MPPT)
			{
				if(g_pGcData->RunInfo.Mt.bNight)
				{
					fVoltOfRt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_MPPT);
					if(bNeedTempComp)
					{
						fVoltOfRt += GetTempCompDelta();
						bTempCompActive = TRUE;
					}
					fVoltOfRt += GC_GetRegulateVolt(fVoltOfRt);
				}
 				else 
 				{
					fVoltOfRt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_RECT);
					if(bNeedTempComp)
					{
						fVoltOfRt += GetTempCompDelta();
						bTempCompActive = TRUE;
					}
				}
				
				fVoltOfMppt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_MPPT);
				if(bNeedTempComp)
				{
					fVoltOfMppt += GetTempCompDelta();
					bTempCompActive = TRUE;
				}	
				fVoltOfMppt += GC_GetRegulateVolt_MPPT(fVoltOfMppt);
			}		
			else
			{	
				fVoltOfRt = GC_GetSettingVolt(BT_PUB_NOM_VOLT);
				if(bNeedTempComp)
				{
					fVoltOfRt += GetTempCompDelta();
					bTempCompActive = TRUE;
				}
				fVoltOfRt += GC_GetRegulateVolt(fVoltOfRt);
			}
		
		break;

	case TYPE_BOOST_VOLT: 		
		if(stState == GC_MPPT_MPPT_RECT || stState == GC_MPPT_MPPT)
		{
			if(g_pGcData->RunInfo.Mt.bNight)
			{
				fVoltOfRt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BST_VOLT_MPPT);
			}
			else
			{
				fVoltOfRt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BST_VOLT_RECT);
			}
			if(bNeedTempComp)
			{
				fVoltOfRt += GetTempCompDelta();
				bTempCompActive = TRUE;
			}
			fVoltOfRt += GC_GetRegulateVolt(fVoltOfRt);

			fVoltOfMppt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BST_VOLT_MPPT);
			if(bNeedTempComp)
			{
				fVoltOfMppt += GetTempCompDelta();
				bTempCompActive = TRUE;
			}	
			fVoltOfMppt += GC_GetRegulateVolt_MPPT(fVoltOfMppt);
		}		
		else
		{	
			fVoltOfRt = GC_GetSettingVolt(BT_PUB_BST_VOLT);//115,2,17
			if(bNeedTempComp)
			{
				fVoltOfRt += GetTempCompDelta();					
				bTempCompActive = TRUE;
			}
			fVoltOfRt += GC_GetRegulateVolt(fVoltOfRt);
		}
		break;

	case TYPE_TEST_VOLT:
 		fVoltOfRt = GC_GetSettingVolt(BT_PUB_TEST_VOLT);

		break;

	case TYPE_POWER_SPLIT:
 		fVoltOfRt = GC_GetSettingVolt(BT_PUB_NOM_VOLT);
		fVoltOfRt += GC_GetFloatValue(TYPE_OTHER, 
									0,
									SPLIT_PUB_DELTA_VOLT);

		break;

	case TYPE_VERY_HI_TEMP:
 		fVoltOfRt = GC_GetSettingVolt(BT_PUB_HI_TEMP_VOLT);

		break;

	default:
		if(stState == GC_MPPT_MPPT_RECT || stState == GC_MPPT_MPPT)
		{
			if(g_pGcData->RunInfo.Mt.bNight)
			{
				fVoltOfRt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_MPPT);
			}
			else
			{
				fVoltOfRt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_RECT);
			}
			fVoltOfMppt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_MPPT);	
		}
		else
		{			
			fVoltOfRt = GC_GetSettingVolt(BT_PUB_NOM_VOLT);
			fVoltOfRt += GC_GetRegulateVolt(fVoltOfRt);
		}
		break;
	}
	
	if(stState == GC_MPPT_MPPT_RECT || stState == GC_MPPT_MPPT)
	{
		GC_DcVoltCtl(fVoltOfRt);
		GC_DcVoltCtl_Mppt(fVoltOfMppt);
	}
	else
	{
		if(iVoltType == TYPE_TEST_VOLT)
		{
			GC_DcVoltCtlForTest(fVoltOfRt, FALSE);
		}
		else
		{
			GC_DcVoltCtl(fVoltOfRt);
		}
	}

	if(bTempCompActive == FALSE)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_TEMP_COMPEN_SET,
						GC_NORMAL,
						FALSE);	
	}
	return;
}


#define		MAX_FUSE_NUM_PER_EQUIP		4
/*==========================================================================*
 * FUNCTION : GC_GetBattFuseAlm
 * PURPOSE  : If there is one or more Battery Fuse alarm, 
			  return TRUE, otherwise return FALSE.
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-5-31 13:59
 *==========================================================================*/
BOOL	GC_GetBattFuseAlm()
{
	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;
	int					i, j;
	
	for(i = 0; i < MAX_NUM_BATT_FUSE_EQUIP; i++)
	{
		for(j = 0; j < MAX_BATT_FUSE_PRI_SIG_NUM; j++)
		{
			

			iRst = GC_GetSigVal(TYPE_BATT_FUSE_UNIT, 
							i,
							BF_PRI_STATE1 + j,
							&pSigValue);
			if (iRst == ERR_DXI_INVALID_EQUIP_ID)
			{
				continue;
			}
			
			GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal BF_PRI_STATE1 error!\n");

				

			if(!(SIG_VALUE_IS_VALID(pSigValue)))
			{
				
				continue;
			}

			if(GC_ALARM 
				== pSigValue->varValue.enumValue)
			{
				
				return TRUE;
			}
		}
	}
	//LargeDUBattery fuse alarm
	for(i = 66; i < 86; i++)
	{		

		iRst = GC_GetSigVal(TYPE_BATT_UNIT, 
						i,
						BT_PRI_LDU_FUSE_ALARM,
						&pSigValue);
	
		
		//printf("Battery %d is %d\n", i, pSigValue->varValue.enumValue);

		if( iRst || !(SIG_VALUE_IS_VALID(pSigValue)))
		{
			//printf("Battery %d is not OK-------\n", i);
			continue;
		}
		
		
		if(GC_NORMAL 
			!= pSigValue->varValue.enumValue)
		{
			//printf("Battery %d is OK-------\n", i);
			return TRUE;
		}
	
	}
	return FALSE;
}


BOOL GC_IsSmduInterrupt(void)
{
	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;
	int					i;
	DWORD				dwSmduNum;
	BOOL bRTN;
	bRTN = GC_GetDwordValueWithStateReturn(TYPE_OTHER, 0, SMDU_PUB_NUM,TRUE,&dwSmduNum);

	if(bRTN == FALSE)
	{
		return FALSE;
	}


	for(i = 0; i < (int)dwSmduNum; i++)
	{
		iRst = GC_GetSigVal(TYPE_OTHER, 
							0,
							SMDU_PUB_COMM_INTERRUPT1 + i,
							&pSigValue);
		if (iRst == ERR_DXI_INVALID_EQUIP_ID)
		{
			continue;
		}

		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal SMDU_PUB_COMM_INTERRUPT1 error!\n");
	
		if(!(SIG_VALUE_IS_VALID(pSigValue)))
		{
			continue;
		}

		if(GC_NORMAL 
			!= pSigValue->varValue.enumValue)
		{
			return TRUE;
		}
	}

	return FALSE;
}

BOOL GC_IsLargeDUDCInterrupt(void)
{
	int					iRst;
	SIG_BASIC_VALUE*	pSigValue;
	int					i;
	DWORD				dwLduNum; 

	BOOL bRTN;
	bRTN = GC_GetDwordValueWithStateReturn(TYPE_OTHER, 0, LDU_PUB_NUM,TRUE,&dwLduNum);

	if(bRTN == FALSE)
	{
		return FALSE;
	}
	
	for(i = 0; i < (int)dwLduNum; i++)
	{
		iRst = GC_GetSigVal(TYPE_OTHER, 
							0,
							LDU_PUB_COMM_INTERRUPT1 + i,
							&pSigValue);
		if (iRst == ERR_DXI_INVALID_EQUIP_ID)
		{
			continue;
		}
		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal LDU_PUB_COMM_INTERRUPT1 error!\n");
		if(!(SIG_VALUE_IS_VALID(pSigValue)))
		{
			continue;
		}

		if(GC_NORMAL 
			!= pSigValue->varValue.enumValue)
		{
			return TRUE;
		}
	}

	return FALSE;
}


BOOL GC_IsSlaveInterrupt(void)
{
	int					iRst;
	SIG_BASIC_VALUE*			pSigValue;
	int					i;
	
	
	for(i = 0; i < MAX_RECT_TYPE_NUM - 1; i++)
	{
		iRst = GC_GetSigVal(TYPE_OTHER, 
							0,
							RT_PUB_COMM_INTERRUPT_SLAVE1 + i,
							&pSigValue);
		if (iRst == ERR_DXI_INVALID_EQUIP_ID)
		{
			continue;
		}
		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal RT_PUB_ALL_INTERRUPT_SELF error!\n");
		
		if(!(SIG_VALUE_IS_VALID(pSigValue)))
		{
			continue;
		}

		//printf("Slave %d status is %d\n", i, pSigValue->varValue.ulValue);
		if(GC_NORMAL 
			!= pSigValue->varValue.ulValue)
		{
			return TRUE;
		}
	}

	return FALSE;
}

BOOL GC_IsSlaveMainsFail(void)
{
	int					iRst;
	SIG_BASIC_VALUE*			pSigValue;
	int					i;
	
	
	for(i = 0; i < RECT_TYPE_SLAVE3 ; i++)
	{
		iRst = GC_GetSigVal(TYPE_OTHER, 
							0,
							RT_PUB_MAINS_FAILURE_SLAVE1 + i,
							&pSigValue);

		if (iRst == ERR_DXI_INVALID_EQUIP_ID)
		{
			continue;
		}
		GC_ASSERT((iRst == ERR_OK), 
							ERR_CTL_GET_VLAUE, 
							"GC_GetSigVal RT_PUB_ALL_INTERRUPT_SELF error!\n");
		
		if(!(SIG_VALUE_IS_VALID(pSigValue)))
		{
			continue;
		}

		
		if(GC_NORMAL 
			!= pSigValue->varValue.enumValue)
		{
			
			
			return TRUE;
		}
	}

	return FALSE;
}

BOOL GC_IsLVDDisconnect(void)
{	
	
	int	i;

	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{
		if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
		{
			if(GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0])
			{
				return TRUE; 
			}
			
		}

		if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
		{
			if(GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1])
			{
				return TRUE;
			}
			
		}
	}
	return FALSE;

}
/*==========================================================================*
 * FUNCTION : GC_LoadData
 * PURPOSE  : To load the GC Data from file, and check with 
			  CRC12 code, if success, return TRUE.
 * CALLS    : Crc12, fopen, fread
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-13 13:37
 *==========================================================================*/
void	GC_LoadData()
{
	int		i,j,k;
	float			fRatedCap;
	GC_FLASH_DATA	*pFlashData = &(g_pGcData->RunInfo.Bt.FlashData);
	GC_FLASH_DATA	ReadFlashData;
	HANDLE			hGenCtlData;
	BOOL			bRst,bRTN;

#ifndef	_GC_TEST
	hGenCtlData = DAT_StorageOpen(GEN_CTL_DATA);
	if(hGenCtlData)
	{
		int		iStartRecordNo = 1;
		int		iRecords = 1;
		bRst 
			= DAT_StorageReadRecords(hGenCtlData,
									&iStartRecordNo,	//piStartRecordNo, 
									&iRecords,			//piRecords, 
									&ReadFlashData,			//pBuff,
									FALSE,				//bActiveAlarm,
									FALSE);				//bAscending

		if(bRst && (iRecords > 0))
		{
			pFlashData->enumBattMgmtState = ReadFlashData.enumBattMgmtState ;

			GC_SINGLE_SEC_TIMER	*pTmr = g_pGcData->SecTimer.aSecTimer;
			if(MODE_SLAVE == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MASTER_SLAVE_MODE))
			{
				GC_SetEnumValue(TYPE_OTHER, 
								0,
								BT_PUB_BM_STATE,
								ST_FLOAT,
								TRUE);
			}
			else
			{
				GC_SetEnumValue(TYPE_OTHER, 
								0,
								BT_PUB_BM_STATE,
								pFlashData->enumBattMgmtState,
								TRUE);
			}

			//Second Timer
			pFlashData->tmrShortTestInterval = ReadFlashData.tmrShortTestInterval;
			pFlashData->tmrCyclicBcDuration = ReadFlashData.tmrCyclicBcDuration;

			pTmr[SEC_TMR_ID_SHORT_TEST_INTERVAL]
					= pFlashData->tmrShortTestInterval;
			pTmr[SEC_TMR_ID_SHORT_TEST_INTERVAL].pfnGetTmrInterval 
					= GC_GetShortTestInterval;

			pTmr[SEC_TMR_ID_CYCLIC_BC_DURATION]
					= pFlashData->tmrCyclicBcDuration;
			pTmr[SEC_TMR_ID_CYCLIC_BC_DURATION].pfnGetTmrInterval 
					= GC_GetIntervalCycBcDuration;

			for(i = 0,j=0,k=0; i < GC_Get_Total_BattNum(); i++)
			{
				if((g_pGcData->RunInfo.Bt.iEquipTypeOfBatt[i]>=SMBAT_TYPEID)
				&&(g_pGcData->RunInfo.Bt.iEquipTypeOfBatt[i]<=CSUBAT_TYPEID))
				{
					if(ReadFlashData.afBattCap[k+MAX_NUM_CAN_BATT_EQUIP] == -1)
					{
						bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&ReadFlashData.afBattCap[k+MAX_NUM_CAN_BATT_EQUIP]);
						if(bRTN == FALSE)
						{
							ReadFlashData.afBattCap[k+MAX_NUM_CAN_BATT_EQUIP] = 50000;
						}
					}
					pFlashData->afBattCap[i] = ReadFlashData.afBattCap[k+MAX_NUM_CAN_BATT_EQUIP];
					pFlashData->adwHighTempTime[i]= ReadFlashData.adwHighTempTime[k+MAX_NUM_CAN_BATT_EQUIP];
					pFlashData->adwLowTempTime[i]= ReadFlashData.adwLowTempTime[k+MAX_NUM_CAN_BATT_EQUIP];
					k++;
				}
				else
				{
					pFlashData->afBattCap[i] = ReadFlashData.afBattCap[j];
					pFlashData->adwHighTempTime[i]= ReadFlashData.adwHighTempTime[j];
					pFlashData->adwLowTempTime[i]= ReadFlashData.adwLowTempTime[j];
					j++;
				}
			}
			
			for(i = 0; i < GC_Get_Total_BattNum(); i++)
			{

				//addded by Jimmy for Li-Batt CR
				if(GC_Is_ABatt_Li_Type(i))
				{
					//~{o?=o?=o?=o?=o?=o?=o?=o?=o?~}?ridge Card~{o?=O4o?=o?=o?=o?=o?=o?=o?=o?=o?=o2;o?=o?=o?=o?~}?
				}
				else
				{				
					//Batteries Capcity
					g_pGcData->RunInfo.Bt.afBattCap[i] = pFlashData->afBattCap[i];
					GC_SetFloatValue(TYPE_BATT_UNIT, 
						i,
						BT_PRI_CAP,
						pFlashData->afBattCap[i]);
				}

				//if((g_pGcData->EquipInfo.BattTestLogInfo.aBattInfo[i].bTemp) || (i > 45 && i <= 65))
				if((g_pGcData->EquipInfo.BattTestLogInfo.aExistBattInfo[i].bTemp) 
				   || (g_pGcData->EquipInfo.aiEquipIdBtPri[i] >= 272 
				      && g_pGcData->EquipInfo.aiEquipIdBtPri[i] <= 291 ))
				{
					//High Temperature Time
					g_pGcData->RunInfo.Bt.adwHighTempTime[i] 
						= pFlashData->adwHighTempTime[i];
					GC_SetDwordValue(TYPE_BATT_UNIT, 
							i, 
							BT_PRI_HIGH_TEMP_TIME,
							(DWORD)(pFlashData->adwHighTempTime[i] 
									/ SECONDS_PER_MIN));

					//Low Temperature Time
					g_pGcData->RunInfo.Bt.adwLowTempTime[i] 
						= pFlashData->adwLowTempTime[i];
					GC_SetDwordValue(TYPE_BATT_UNIT, 
							i, 
							BT_PRI_LOW_TEMP_TIME,
							(DWORD)(pFlashData->adwLowTempTime[i] 
									/ SECONDS_PER_MIN));
				}
			}

			//Shallow Time
			pFlashData->dwShallowTime =ReadFlashData.dwShallowTime;
			g_pGcData->RunInfo.Bt.dwShallowTime 
				= pFlashData->dwShallowTime;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_SHALLOW_TIME,
							(DWORD)(pFlashData->dwShallowTime / SECONDS_PER_MIN));

			//Shallow Times
			pFlashData->dwShallowTimes =ReadFlashData.dwShallowTimes;
			g_pGcData->RunInfo.Bt.dwShallowTimes 
				= pFlashData->dwShallowTimes;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_SHALLOW_TIMES,
							pFlashData->dwShallowTimes);

			//Mid Time
			pFlashData->dwMidTime =ReadFlashData.dwMidTime;
			g_pGcData->RunInfo.Bt.dwMidTime 
				= pFlashData->dwMidTime;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_MID_TIME,
							(DWORD)(pFlashData->dwMidTime / SECONDS_PER_MIN));

			//Mid Times
			pFlashData->dwMidTimes =ReadFlashData.dwMidTimes;
			g_pGcData->RunInfo.Bt.dwMidTimes 
				= pFlashData->dwMidTimes;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_MID_TIMES,
							pFlashData->dwMidTimes);

			//Deep Time
			pFlashData->dwDeepTime =ReadFlashData.dwDeepTime;
			g_pGcData->RunInfo.Bt.dwDeepTime 
				= pFlashData->dwDeepTime;	
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_DEEP_TIME,
							(DWORD)(pFlashData->dwDeepTime / SECONDS_PER_MIN));

			//Deep Times
			pFlashData->dwDeepTimes =ReadFlashData.dwDeepTimes;
			g_pGcData->RunInfo.Bt.dwDeepTimes 
				= pFlashData->dwDeepTimes;
			GC_SetDwordValue(TYPE_OTHER, 
							0, 
							BT_PUB_DEEP_TIMES,
							pFlashData->dwDeepTimes);

			pFlashData->tmLastPlanTest =ReadFlashData.tmLastPlanTest;
			pFlashData->tmLastDslTest =ReadFlashData.tmLastDslTest;

			g_pGcData->RunInfo.Bt.tmLastPlanTest = pFlashData->tmLastPlanTest;
			g_pGcData->RunInfo.Ac.tmLastDslTest = pFlashData->tmLastDslTest;
			
			GC_SetDateValue(TYPE_OTHER, 
							0,
							DSL_PUB_LAST_TEST_DATE,
							pFlashData->tmLastDslTest);
		}

		bRst = DAT_StorageClose(hGenCtlData);

		if(!bRst)
		{				
			GC_LOG_OUT(APP_LOG_WARNING, "Failed to close GenCtlData log!\n");
		}

		if(bRst && (iRecords > 0))
		{
			return;
		}
	}

#endif

	
	//Failed to load, following is initializing Flash Data 
	GC_SINGLE_SEC_TIMER	*pTmr = g_pGcData->SecTimer.aSecTimer;

	GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE, ST_FLOAT, TRUE);
	
	//Second Timer
	pTmr[SEC_TMR_ID_SHORT_TEST_INTERVAL].byState = SEC_TMR_SUSPENDED;
	pTmr[SEC_TMR_ID_CYCLIC_BC_DURATION].byState = SEC_TMR_SUSPENDED;

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		
		//~{o?=o?=o?=o?=o?=o?=o?=o?=o?~}?ridge Card~{o?=O4o?=o?=o?=o?=o?=o?=o?=o?=o?=o2;o?=o?=o?=o?~}?
		bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
		if(bRTN == FALSE)
			continue;

		
		//addded by Jimmy for Li-Batt CR
		if(GC_Is_ABatt_Li_Type(i))
		{
			//~{o?=o?=o?=o?=o?=o?=o?=o?=o?~}?ridge Card~{o?=O4o?=o?=o?=o?=o?=o?=o?=o?=o?=o2;o?=o?=o?=o?~}?
		}
		else
		{
			//Batteries Capcity
			g_pGcData->RunInfo.Bt.afBattCap[i] = fRatedCap;
			GC_SetFloatValue(TYPE_BATT_UNIT, i, BT_PRI_CAP, fRatedCap);
		}
		

		//if((g_pGcData->EquipInfo.BattTestLogInfo.aBattInfo[i].bTemp) || (i > 45 && i <= 65))
		if((g_pGcData->EquipInfo.BattTestLogInfo.aExistBattInfo[i].bTemp) 
		   || (g_pGcData->EquipInfo.aiEquipIdBtPri[i] >= 272 
		       && g_pGcData->EquipInfo.aiEquipIdBtPri[i] <= 291 ))
		{
			//High Temperature Time
			g_pGcData->RunInfo.Bt.adwHighTempTime[i] = 0;
			GC_SetDwordValue(TYPE_BATT_UNIT, i, BT_PRI_HIGH_TEMP_TIME, 0);

			//Low Temperature Time
			g_pGcData->RunInfo.Bt.adwLowTempTime[i] = 0;
			GC_SetDwordValue(TYPE_BATT_UNIT, i, BT_PRI_LOW_TEMP_TIME, 0);
		}
	}

	//Shallow Time
	g_pGcData->RunInfo.Bt.dwShallowTime = 0;
	GC_SetDwordValue(TYPE_OTHER, 0, BT_PUB_SHALLOW_TIME, 0);

	//Shallow Times
	g_pGcData->RunInfo.Bt.dwShallowTimes = 0;
	GC_SetDwordValue(TYPE_OTHER, 0, BT_PUB_SHALLOW_TIMES, 0);

	//Mid Time
	g_pGcData->RunInfo.Bt.dwMidTime = 0;
	GC_SetDwordValue(TYPE_OTHER, 0, BT_PUB_MID_TIME, 0);

	//Mid Times
	g_pGcData->RunInfo.Bt.dwMidTimes = 0;
	GC_SetDwordValue(TYPE_OTHER, 0, BT_PUB_MID_TIMES, 0);

	//Deep Time
	g_pGcData->RunInfo.Bt.dwDeepTime = 0;
	GC_SetDwordValue(TYPE_OTHER, 0, BT_PUB_DEEP_TIME,0);

	//Deep Times
	g_pGcData->RunInfo.Bt.dwDeepTimes = 0;
	GC_SetDwordValue(TYPE_OTHER, 0, BT_PUB_DEEP_TIMES, 0);

	g_pGcData->RunInfo.Bt.tmLastPlanTest = 0;
	g_pGcData->RunInfo.Ac.tmLastDslTest = 0;

	return;
}


/*==========================================================================*
 * FUNCTION : GC_SaveData
 * PURPOSE  : To Save the GC Data with CRC12 code into file.
 * CALLS    : fopen, fclose, fwrite, Crc12
 * CALLED BY: GC_SuspendSecTimer, GC_SetSecTimer, TimeoutCheck, 
 * ARGUMENTS: 
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-13 13:33
 *==========================================================================*/
void GC_SaveData()
{
	int		i,j,k;
	GC_FLASH_DATA	*pFlashData = &(g_pGcData->RunInfo.Bt.FlashData);

	GC_FLASH_DATA	WriteFlashData;

	HANDLE	hGenCtlData;
	BOOL	bRst;

	hGenCtlData = DAT_StorageCreate(GEN_CTL_DATA, 
					sizeof(GC_FLASH_DATA), 
					GEN_CTL_DATA_RECORDS, 
					GEN_CTL_DATA_FREQUENCY);
	
	if(!hGenCtlData)
	{
		GC_LOG_OUT(APP_LOG_WARNING, 
			"Failed to create GenCtlData log!\n");
		return;
	}

	pFlashData->enumBattMgmtState 
		= GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);
	WriteFlashData.enumBattMgmtState = pFlashData->enumBattMgmtState;
	
	//Second Timer
	pFlashData->tmrShortTestInterval
		= g_pGcData->SecTimer.aSecTimer[SEC_TMR_ID_SHORT_TEST_INTERVAL];

	WriteFlashData.tmrShortTestInterval = pFlashData->tmrShortTestInterval;

	pFlashData->tmrCyclicBcDuration
		= g_pGcData->SecTimer.aSecTimer[SEC_TMR_ID_CYCLIC_BC_DURATION];

	WriteFlashData.tmrCyclicBcDuration =pFlashData->tmrCyclicBcDuration;
	for(i = 0,j=0,k=0; i < GC_Get_Total_BattNum(); i++)
	{
		//Batteries Capcity
		pFlashData->afBattCap[i] 
			= g_pGcData->RunInfo.Bt.afBattCap[i];
				
		//High Temperature Time
		 pFlashData->adwHighTempTime[i]
			= g_pGcData->RunInfo.Bt.adwHighTempTime[i];

		//Low Temperature Time
		pFlashData->adwLowTempTime[i]
			= g_pGcData->RunInfo.Bt.adwLowTempTime[i];
		if((g_pGcData->RunInfo.Bt.iEquipTypeOfBatt[i] >=SMBAT_TYPEID)
		  &&(g_pGcData->RunInfo.Bt.iEquipTypeOfBatt[i] <=CSUBAT_TYPEID))
		{
			WriteFlashData.afBattCap[k+MAX_NUM_CAN_BATT_EQUIP]= pFlashData->afBattCap[i];
			WriteFlashData.adwHighTempTime[k+MAX_NUM_CAN_BATT_EQUIP]= pFlashData->adwHighTempTime[i];
			WriteFlashData.adwLowTempTime[k+MAX_NUM_CAN_BATT_EQUIP]= pFlashData->adwLowTempTime[i];
			k++;
		}
		else
		{
			WriteFlashData.afBattCap[j]= pFlashData->afBattCap[i];
			WriteFlashData.adwHighTempTime[j]= pFlashData->adwHighTempTime[i];
			WriteFlashData.adwLowTempTime[j]= pFlashData->adwLowTempTime[i];
			j++;

		}
	}
	if(k==0)
	{
		for(k =0; k < MAX_NUM_485_BATT_EQUIP; k++)
		{
			WriteFlashData.afBattCap[k+MAX_NUM_CAN_BATT_EQUIP] = -1;
			WriteFlashData.adwHighTempTime[k+MAX_NUM_CAN_BATT_EQUIP]= 0;
			WriteFlashData.adwLowTempTime[k+MAX_NUM_CAN_BATT_EQUIP]= 0;
		}
	}
	

	//Shallow Time
	pFlashData->dwShallowTime 
		= g_pGcData->RunInfo.Bt.dwShallowTime;
	WriteFlashData.dwShallowTime =pFlashData->dwShallowTime;

	//Shallow Times
	pFlashData->dwShallowTimes 
		= g_pGcData->RunInfo.Bt.dwShallowTimes;

	WriteFlashData.dwShallowTimes =pFlashData->dwShallowTimes;


	//Mid Time
	pFlashData->dwMidTime 
		= g_pGcData->RunInfo.Bt.dwMidTime;

	WriteFlashData.dwMidTime =pFlashData->dwMidTime;
	
	//Mid Times
	pFlashData->dwMidTimes
		= g_pGcData->RunInfo.Bt.dwMidTimes;

	WriteFlashData.dwMidTimes =pFlashData->dwMidTimes;

	//Deep Time
	pFlashData->dwDeepTime = g_pGcData->RunInfo.Bt.dwDeepTime;

	WriteFlashData.dwDeepTime =pFlashData->dwDeepTime;
			
	//Deep Times
	pFlashData->dwDeepTimes = g_pGcData->RunInfo.Bt.dwDeepTimes;

	WriteFlashData.dwDeepTimes =pFlashData->dwDeepTimes;

	pFlashData->tmLastPlanTest = g_pGcData->RunInfo.Bt.tmLastPlanTest;
	pFlashData->tmLastDslTest = g_pGcData->RunInfo.Ac.tmLastDslTest;

	WriteFlashData.tmLastPlanTest =pFlashData->tmLastPlanTest;
	WriteFlashData.tmLastDslTest  =pFlashData->tmLastDslTest;


	bRst = DAT_StorageWriteRecord(hGenCtlData, 
								sizeof(GC_FLASH_DATA), 
								(void*)(&WriteFlashData));

	if(!bRst)
	{
		GC_LOG_OUT(APP_LOG_WARNING, 
			"Failed to write GenCtlData log!\n");
		return;
	}

	bRst = DAT_StorageClose(hGenCtlData);

	if(!bRst)
	{
		GC_LOG_OUT(APP_LOG_WARNING, 
			"Failed to close GenCtlData log!\n");
		return;
	}

	return;
}



/*==========================================================================*
 * FUNCTION : GC_GetAlarmNum
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAlmLevel : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-05 09:55
 *==========================================================================*/
int	GC_GetAlarmNum(int iAlmLevel)
{
	int		iRst;
	int		iBufLen;
	int		iActiveAlarmNum;

	iRst = DxiGetData(VAR_ACTIVE_ALARM_NUM,
					iAlmLevel,
					0,
					&iBufLen,
					&iActiveAlarmNum,
					0);
	GC_ASSERT((iRst == ERR_OK), 
			ERR_CTL_GET_ALARM_NUM,
			"DxiGetData VAR_ACTIVE_ALARM_NUM error!\n");

	return iActiveAlarmNum;
}



/*==========================================================================*
 * FUNCTION : GC_AlmJudgmentForBC
 * PURPOSE  : To judge if the alarm condition is fulfilled for end a boost 
              charge
 * CALLS    : 
 * CALLED BY: GC_CycPreBC GC_ManAutoBC GC_NeedAutoBC GC_FloatCharge
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-17 10:50
 *==========================================================================*/
BOOL GC_AlmJudgmentForBC()
{
	static	int	s_iCounter = 0;
	char		szLogOut[128];
	SIG_ENUM enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
	SIG_ENUM enumGridState = GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get 

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif

	if(enumState == GC_HYBRID_ENABLED && enumGridState == GC_HYBRID_GRID_OFF)
	{
		return FALSE;
	}

#ifdef GC_SUPPORT_MPPT
	if(stState == GC_MPPT_DISABLE)
	{
		if((g_pGcData->RunInfo.Rt.bNoRespond )
			|| (g_pGcData->RunInfo.Rt.bAcFail)
			|| GetBattHiTempAlm() 
			|| GC_GetBattFuseAlm()
			|| GC_IsSmduInterrupt()
			|| GC_IsSlaveInterrupt()
			|| GC_IsSlaveMainsFail()
			|| GC_IsLargeDUDCInterrupt())
		{			
			if(s_iCounter > 2)
			{
				//sprintf(szLogOut, "rect:%d,batt temp:%d, Batt fuse:%d,smdu inte:%d,Slave:%d,Slave mains:%d", g_pGcData->RunInfo.Rt.bNoRespond, 
				//GetBattHiTempAlm(), GC_GetBattFuseAlm(), GC_IsSmduInterrupt(), GC_IsSlaveInterrupt(), GC_IsSlaveMainsFail());
				//GC_LOG_OUT(APP_LOG_INFO, szLogOut);
				return TRUE;
			}
			else
			{
				s_iCounter++;
			}
		}
		else
		{
			s_iCounter = 0;
		}
		return FALSE;
	}
	else  if(stState == GC_MPPT_MPPT)
	{
		if(g_pGcData->RunInfo.Mt.bNoRespond
			|| g_pGcData->RunInfo.Mt.bNight
			|| GetBattHiTempAlm() 
			|| GC_GetBattFuseAlm()
			|| GC_IsSmduInterrupt())
		{
			/*printf("g_pGcData->RunInfo.Rt.bMasterNoRespond = %d, g_pGcData->RunInfo.Mt.bNoRespond= %d,g_pGcData->RunInfo.Rt.bAcFail= %d,g_pGcData->RunInfo.Mt.bNight= %d \n", g_pGcData->RunInfo.Rt.bMasterNoRespond, g_pGcData->RunInfo.Mt.bNoRespond,
				g_pGcData->RunInfo.Rt.bMasterAcFail,g_pGcData->RunInfo.Mt.bNight);
			//printf("GetBattHiTempAlm() = %d, GC_GetBattFuseAlm()= %d,GC_IsSmduInterrupt()= %d,GC_IsSlaveInterrupt()= %d,GC_IsSlaveMainsFail()= %d,GC_IsLargeDUDCInterrupt()= %d\n", GetBattHiTempAlm(), GC_GetBattFuseAlm(),GC_IsSmduInterrupt(),GC_IsSlaveInterrupt(),GC_IsSlaveMainsFail(),GC_IsLargeDUDCInterrupt());*/
			if(s_iCounter > 2)
			{
				//sprintf(szLogOut, "rect:%d,batt temp:%d, Batt fuse:%d,smdu inte:%d,Slave:%d,Slave mains:%d", g_pGcData->RunInfo.Rt.bNoRespond, 
				//GetBattHiTempAlm(), GC_GetBattFuseAlm(), GC_IsSmduInterrupt(), GC_IsSlaveInterrupt(), GC_IsSlaveMainsFail());
				//GC_LOG_OUT(APP_LOG_INFO, szLogOut);
				return TRUE;
			}
			else
			{
				s_iCounter++;
			}
		}
		else
		{
			s_iCounter = 0;
		}
		return FALSE;
	}
	else
	{
		/*printf("g_pGcData->RunInfo.Rt.bMasterNoRespond = %d, g_pGcData->RunInfo.Mt.bNoRespond= %d,g_pGcData->RunInfo.Rt.bAcFail= %d,g_pGcData->RunInfo.Mt.bNight= %d \n", g_pGcData->RunInfo.Rt.bMasterNoRespond, g_pGcData->RunInfo.Mt.bNoRespond,
			g_pGcData->RunInfo.Rt.bMasterAcFail,g_pGcData->RunInfo.Mt.bNight);
		//printf("GetBattHiTempAlm() = %d, GC_GetBattFuseAlm()= %d,GC_IsSmduInterrupt()= %d,GC_IsSlaveInterrupt()= %d,GC_IsSlaveMainsFail()= %d,GC_IsLargeDUDCInterrupt()= %d\n", GetBattHiTempAlm(), GC_GetBattFuseAlm(),GC_IsSmduInterrupt(),GC_IsSlaveInterrupt(),GC_IsSlaveMainsFail(),GC_IsLargeDUDCInterrupt());*/

		if((g_pGcData->RunInfo.Rt.bMasterNoRespond && g_pGcData->RunInfo.Mt.bNoRespond)
			|| (g_pGcData->RunInfo.Rt.bMasterNoRespond && g_pGcData->RunInfo.Mt.bNight)
			||(g_pGcData->RunInfo.Rt.bMasterAcFail && g_pGcData->RunInfo.Mt.bNoRespond)
			||(g_pGcData->RunInfo.Rt.bMasterAcFail && g_pGcData->RunInfo.Mt.bNight)
			|| GetBattHiTempAlm() 
			|| GC_GetBattFuseAlm()
			|| GC_IsSmduInterrupt())
		{
			//printf("g_pGcData->RunInfo.Rt.bMasterNoRespond = %d, g_pGcData->RunInfo.Mt.bNoRespond= %d,g_pGcData->RunInfo.Rt.bAcFail= %d,g_pGcData->RunInfo.Mt.bNight= %d \n", g_pGcData->RunInfo.Rt.bMasterNoRespond, g_pGcData->RunInfo.Mt.bNoRespond,
			//	g_pGcData->RunInfo.Rt.bMasterAcFail,g_pGcData->RunInfo.Mt.bNight);
			//printf("GetBattHiTempAlm() = %d, GC_GetBattFuseAlm()= %d,GC_IsSmduInterrupt()= %d,GC_IsSlaveInterrupt()= %d,GC_IsSlaveMainsFail()= %d,GC_IsLargeDUDCInterrupt()= %d\n", GetBattHiTempAlm(), GC_GetBattFuseAlm(),GC_IsSmduInterrupt(),GC_IsSlaveInterrupt(),GC_IsSlaveMainsFail(),GC_IsLargeDUDCInterrupt());
			if(s_iCounter > 2)
			{
				//sprintf(szLogOut, "rect:%d,batt temp:%d, Batt fuse:%d,smdu inte:%d,Slave:%d,Slave mains:%d", g_pGcData->RunInfo.Rt.bNoRespond, 
				//GetBattHiTempAlm(), GC_GetBattFuseAlm(), GC_IsSmduInterrupt(), GC_IsSlaveInterrupt(), GC_IsSlaveMainsFail());
				//GC_LOG_OUT(APP_LOG_INFO, szLogOut);
				return TRUE;
			}
			else
			{
				s_iCounter++;
			}
		}
		else
		{
			s_iCounter = 0;
		}
		return FALSE;
	}
#else
	if((g_pGcData->RunInfo.Rt.bNoRespond )
		|| (g_pGcData->RunInfo.Rt.bAcFail)
		|| GetBattHiTempAlm() 
		|| GC_GetBattFuseAlm()
		|| GC_IsSmduInterrupt()
		|| GC_IsSlaveInterrupt()
		|| GC_IsSlaveMainsFail()
		|| GC_IsLargeDUDCInterrupt())
	{			
		if(s_iCounter > 2)
		{
			//sprintf(szLogOut, "rect:%d,batt temp:%d, Batt fuse:%d,smdu inte:%d,Slave:%d,Slave mains:%d", g_pGcData->RunInfo.Rt.bNoRespond, 
			//GetBattHiTempAlm(), GC_GetBattFuseAlm(), GC_IsSmduInterrupt(), GC_IsSlaveInterrupt(), GC_IsSlaveMainsFail());
			//GC_LOG_OUT(APP_LOG_INFO, szLogOut);
			return TRUE;
		}
		else
		{
			s_iCounter++;
		}
	}
	else
	{
		s_iCounter = 0;
	}
	return FALSE;
#endif

	
}


/*==========================================================================*
* FUNCTION : GC_AlmJudgmentForFCtoBC
* PURPOSE  : To judge if the alarm condition is fulfilled for FC to BC
				This condition should be very strict, so it is not necessary to wait for 3 times
* CALLS    : 
* CALLED BY: GC_FloatCharge
* RETURN   :  
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2004-11-17 10:50
*==========================================================================*/
BOOL GC_AlmJudgmentForFCtoBC()
{
	char		szLogOut[128];

	if((g_pGcData->RunInfo.Rt.bNoRespond || g_pGcData->RunInfo.Mt.bNoRespond)
		|| (g_pGcData->RunInfo.Rt.bAcFail && g_pGcData->RunInfo.Mt.bNight)
		|| GetBattHiTempAlm() 
		|| GC_GetBattFuseAlm()
		|| GC_IsSmduInterrupt()
		|| GC_IsSlaveInterrupt()
		|| GC_IsSlaveMainsFail()
		|| GC_IsLargeDUDCInterrupt())
	{
		return TRUE;
	}
	else
	{

		return FALSE;
	}
}



/*==========================================================================*
 * FUNCTION : GC_IsVeryHiBattTemp
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-11-04 16:24
 *==========================================================================*/
BOOL GC_IsVeryHiBattTemp(void)
{
	//int		i;

	//for(i = 0; i < MAX_NUM_TEMPERATURE; i++)
	//{
	//	SIG_ENUM	enumTempUsage = GC_GetEnumValue(TYPE_OTHER, 
	//		0,
	//		GC_PUB_TEMP1_USAGE + i);
	//	if((enumTempUsage == TEMP_USAGE_BATTERY)
	//		&& (GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
	//		0,
	//		GC_PUB_VERY_HI_TEMP1_ALM + i)))
	//	{
	//		return TRUE;
	//	}
	//}

	
	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
							0,
							GC_PUB_BATTGROUP_BATTTEMP_VERYHIGH_ALARM))
	{
		return TRUE;
	}
	
	return FALSE;
}

/*==========================================================================*
* FUNCTION : GC_IsVeryHiBTRM
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Jimmy Wu                DATE: 2012-02-16 16:23
*==========================================================================*/
BOOL GC_IsVeryHiBTRM(void)
{
	//int		i;

	//for(i = 0; i < MAX_NUM_TEMPERATURE; i++)
	//{
	//	SIG_ENUM	enumTempUsage = GC_GetEnumValue(TYPE_OTHER, 
	//		0,
	//		GC_PUB_TEMP1_USAGE + i);
	//	if((enumTempUsage == TEMP_USAGE_BATTERY)
	//		&& (GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
	//		0,
	//		GC_PUB_VERY_HI_TEMP1_ALM + i)))
	//	{
	//		return TRUE;
	//	}
	//}


	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_BTRM_VERYHIGH_ALARM))
	{
		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : GC_AlmJudgmentForBT
 * PURPOSE  : To judge if the alarm condition is fulfilled for end a Battery 
              Test
 * CALLS    : 
 * CALLED BY: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-11-17 10:50
 *==========================================================================*/
BOOL GC_AlmJudgmentForBT()
{
	static	int s_iCounter = 0;

	if((GC_GetAlarmNum(ALARM_LEVEL_CRITICAL) > 0) 
		|| (GC_GetAlarmNum(ALARM_LEVEL_MAJOR) > 0))
	{
		if(s_iCounter > 2)
		{
			return TRUE;
		}
		else
		{
			s_iCounter++;
		}
	}
	else
	{
		s_iCounter = 0;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : OpenAllRect
 * PURPOSE  : If there is retifier DC_ON_OFF_STATE is Off, then open all of 
              rectifiers. If there is retifier AC_ON_OFF_STATE is Off, then 
			  open all of rectifiers.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-08 15:08
 *==========================================================================*/
static void	OpenAllRect(void)
{
	int					i, j;
	GC_RUN_INFO_RECT	*pRt = &(g_pGcData->RunInfo.Rt);
	int	enumMode =  GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MODE);


	GC_RECT_INFO_IDX	aRectIdx[] =
	{
		{TYPE_RECT_UNIT,	RT_PRI_RECT_ID,		RT_PRI_RECT_HI_ID,	RT_PRI_OPER_TIME,	RT_PRI_AC_FAIL,	RT_PRI_AC_ON_OFF_STA,	RT_PRI_DC_ON_OFF_STA,	RT_PRI_FAULT,	RT_PRI_PROTCTION,	},
		{TYPE_SLAVE1_RECT,	S1_PRI_RECT_ID,		S1_PRI_RECT_HI_ID,	S1_PRI_OPER_TIME,	S1_PRI_AC_FAIL,	S1_PRI_AC_ON_OFF_STA,	S1_PRI_DC_ON_OFF_STA,	S1_PRI_FAULT,	S1_PRI_PROTCTION,	},
		{TYPE_SLAVE2_RECT,	S2_PRI_RECT_ID,		S2_PRI_RECT_HI_ID,	S2_PRI_OPER_TIME,	S2_PRI_AC_FAIL,	S2_PRI_AC_ON_OFF_STA,	S2_PRI_DC_ON_OFF_STA,	S2_PRI_FAULT,	S2_PRI_PROTCTION,	},
		{TYPE_SLAVE3_RECT,	S3_PRI_RECT_ID,		S3_PRI_RECT_HI_ID,	S3_PRI_OPER_TIME,	S3_PRI_AC_FAIL,	S3_PRI_AC_ON_OFF_STA,	S3_PRI_DC_ON_OFF_STA,	S3_PRI_FAULT,	S3_PRI_PROTCTION,	},
	};

	if(GC_GetDwordValue(TYPE_OTHER, 0, CT_PUB_QTY_SELF))
	{
		if(enumMode == ENUM_OTHER_MODE)
		{
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				CT_PUB_DC_ON_OFF_CTL,
				GC_RECT_ON,
				TRUE);
		}

		if(GC_DISABLED == GC_GetEnumValue(TYPE_OTHER, 0, CT_PUB_MAX_CURR_LMT_ENABLE))
		{
			GC_SendCTCurrentLimit(GC_MAX_CURR_LMT_CONV);
		}
	}

	//controlled by SwitchOnAllRect() in gc_rect_redund.c
	if(GC_ENABLED == GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_REDU_ENB))
	{	
		return;
	}

	if(g_pGcData->enumMasterSlaveMode == MODE_SLAVE)
	{
		return;
	}
	

	if(enumMode == ENUM_EMEA_MODE)
	{
		return;
	}

	//Check all of communicating rectifiers	
	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		for(j = 0; j < pRt->Slave[i].iQtyOfRect; j++)
		{
			if(pRt->Slave[i].abComm[j])
			{
				if(GC_RECT_OFF 
					== GC_GetEnumValue(aRectIdx[i].iRectType, j, aRectIdx[i].iDcOnOffStaSid))
				{
					GC_SetEnumValue(TYPE_OTHER, 
									0, 
									RT_PUB_DC_ON_OFF_CTL_SELF + i,
									GC_RECT_ON,
									TRUE);
					break;
				}
			}
		}
	}

	//Check all of communicating rectifiers	
	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		for(j = 0; j < pRt->Slave[i].iQtyOfRect; j++)
		{
			if(pRt->Slave[i].abComm[j])
			{
				if(GC_RECT_OFF 
					== GC_GetEnumValue(aRectIdx[i].iRectType, j, aRectIdx[i].iAcOnOffStaSid))
				{
					GC_SetEnumValue(TYPE_OTHER, 
									0, 
									RT_PUB_AC_ON_OFF_CTL_SELF + i,
									GC_RECT_ON,
									TRUE);
					break;
				}
			}
		}
	}


	return;
}


#ifdef	GC_JFFS2
/*==========================================================================*
 * FUNCTION : Crc12
 * PURPOSE  : To calculate and return the CRC12 code of pbyBuff with length 
		      of iLen
 * CALLS    : 
 * CALLED BY: GC_SaveData GC_LoadData
 * ARGUMENTS: static BYTE*  pbyBuff : 
 *            int           iLen    : 
 * RETURN   : CRC12 code 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-09 17:29
 *==========================================================================*/
static WORD Crc12(const BYTE* pbyBuff, int iLen)
{
	WORD	wCRC;
	int 	i, iByteOffset, iBitOffset;

	wCRC = 0;
	iByteOffset = 0;
	iBitOffset = 0;
	while(iByteOffset < iLen)
	{
		if(pbyBuff[iByteOffset] & (0x80 >> iBitOffset))
		{
			wCRC |= 0x01;
		}
		if(wCRC >= 0x1000)
		{
			wCRC ^= 0x180D;
		}

		wCRC <<= 1;
		iBitOffset++;
		if(iBitOffset == 8)
		{
			iBitOffset = 0;
			iByteOffset++;
		}
	}

	/* to handle last 12 zero*/
	for(i = 0; i < 12; i++)
	{
		if(wCRC >= 0x1000)
			wCRC ^= 0x180D;
		wCRC <<= 1;
	}
	wCRC >>= 1;
	return wCRC;
}
#endif


/*==========================================================================*
 * FUNCTION : BattMgmtTimerInit
 * PURPOSE  : Initialize the sec-timers used by Battery management 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-12-15 14:18
 *==========================================================================*/
static void BattMgmtTimerInit(void)
{
	BOOL	*p_bNeedSave = g_pGcData->SecTimer.abNeedSave;

	p_bNeedSave[SEC_TMR_ID_SHORT_TEST_INTERVAL] = TRUE;

	GC_SuspendSecTimer(SEC_TMR_ID_STABLE_BC);
	p_bNeedSave[SEC_TMR_ID_STABLE_BC] = FALSE;

	p_bNeedSave[SEC_TMR_ID_CYCLIC_BC_DURATION] = TRUE;

	GC_SuspendSecTimer(SEC_TMR_ID_SHORT_TEST_DURATION); 
	p_bNeedSave[SEC_TMR_ID_SHORT_TEST_DURATION] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_BATT_TEST_DURATION);
	p_bNeedSave[SEC_TMR_ID_BATT_TEST_DURATION] = FALSE;

	//Suspend After Boost Charge Timer
	GC_SuspendSecTimer(SEC_TMR_ID_AFTER_BC);
	p_bNeedSave[SEC_TMR_ID_AFTER_BC] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_RESET_CAP);
	p_bNeedSave[SEC_TMR_ID_RESET_CAP] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_BC_PROTECT);
	p_bNeedSave[SEC_TMR_ID_BC_PROTECT] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_FC_TO_BC_DELAY);
	p_bNeedSave[SEC_TMR_ID_FC_TO_BC_DELAY] = FALSE;	

	GC_SuspendSecTimer(SEC_TMR_ID_CURR_LMT_INTERVAL);
	p_bNeedSave[SEC_TMR_ID_CURR_LMT_INTERVAL] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_CONST_TEST_INTERVAL);
	p_bNeedSave[SEC_TMR_ID_CONST_TEST_INTERVAL] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_GET_LOAD_INTERVAL);
	p_bNeedSave[SEC_TMR_ID_GET_LOAD_INTERVAL] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY);
	p_bNeedSave[SEC_TMR_ID_PRE_CURR_LMT_DELAY] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_BOOTING_DELAY);
	p_bNeedSave[SEC_TMR_ID_BOOTING_DELAY] = FALSE;
	if(!GC_IsGCSigExist(TYPE_OTHER, 0, BT_PUB_CYC_START_TIME))
	{
		GC_SetSecTimer(SEC_TMR_ID_CYCLIC_BC_INTERVAL,
				0,
				GC_GetIntervalCycBcInterval,
				0);
	}
	
	p_bNeedSave[SEC_TMR_ID_CYCLIC_BC_INTERVAL] = FALSE;


	GC_SuspendSecTimer(SEC_TMR_ID_ESNA_LVD_SYNCH_INTERVAL);
	p_bNeedSave[SEC_TMR_ID_ESNA_LVD_SYNCH_INTERVAL] = FALSE;

	
	GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL);
	p_bNeedSave[SEC_TMR_ID_RELAY_TEST_INTERVAL] = FALSE;	

	GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE);
	p_bNeedSave[SEC_TMR_ID_RELAY_TEST_CHANGESTEATE] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_MPPT_LOST_INTERVAL);
	p_bNeedSave[SEC_TMR_ID_MPPT_LOST_INTERVAL] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_BATT_DISCHARGE_TIME);
	p_bNeedSave[SEC_TMR_ID_BATT_DISCHARGE_TIME] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_PRE_CURR_LMT_OF_LVD);
	p_bNeedSave[SEC_TMR_ID_PRE_CURR_LMT_OF_LVD] = FALSE;

	return;
}
/*==========================================================================*
 * FUNCTION : GC_HybridMgmtTimerInit
 * PURPOSE  : Initialize the sec-timers used by Hybrid system
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Marco Yang                DATE: 2011-1-19 14:18
 *==========================================================================*/
void GC_HybridMgmtTimerInit(void)
{
	BOOL	*p_bNeedSave = g_pGcData->SecTimer.abNeedSave;
		

	//Suspend Hybrid Timer
	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_CAPACITY_CHARGE);
	p_bNeedSave[SEC_TMR_ID_HYBRID_CAPACITY_CHARGE] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE);
	p_bNeedSave[SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_FIXEDDAILY_CHARGE);
	p_bNeedSave[SEC_TMR_ID_HYBRID_FIXEDDAILY_CHARGE] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_FIXEDDAILY_DISCHARGE);
	p_bNeedSave[SEC_TMR_ID_HYBRID_FIXEDDAILY_DISCHARGE] = FALSE;	

	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP);
	p_bNeedSave[SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION);
	p_bNeedSave[SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION);
	p_bNeedSave[SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP] = FALSE;


	GC_SetSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE,
		0,
		GC_GetIntervalCycBcInterval,
		0);
	p_bNeedSave[SEC_TMR_ID_HYBRID_EQUALISM_CYCLE] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL);
	p_bNeedSave[SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL] = FALSE;

	GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL);
	p_bNeedSave[SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL] = FALSE;

	g_pGcData->RunInfo.HyBrid.tmLastEqualisingTime = 0;
	g_pGcData->RunInfo.HyBrid.tmLastEqualisingTime = 0;

	g_pGcData->RunInfo.HyBrid.iLastGridOnState  = GC_HYBRID_GRID_ON;
	g_pGcData->RunInfo.HyBrid.iLastRunDG   = IB2_ALARM_RELAY_DG1;
	g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG1;


	return;
}


#define	 HYSTERESIS_TIMES_MAN_TO_AUTO		2
/*==========================================================================*
 * FUNCTION : GC_BasicMaintain
 * PURPOSE  : Get AutoMan State, handling on Man to Auto, Open all 
              rectifiers on Auto
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-01-03 11:50
 *==========================================================================*/
void GC_BasicMaintain(void)
{
	static	SIG_ENUM	s_enumLastAutoMan = STATE_AUTO;
	static	int			s_iUnderVoltCounter = 0;

	int			aiRTCurrentLimitSid[] = {RT_PRI_CURR_LMT, 
									S1_PRI_CURR_LMT,
									S2_PRI_CURR_LMT,
									S3_PRI_CURR_LMT,};


	//Below is for rectifies current limitation smoothly change from MAN to AUTO.
	if((STATE_MAN == s_enumLastAutoMan) 
		&& (STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
		&& (SEC_TMR_TIME_IN != GC_GetStateOfSecTimer(SEC_TMR_ID_PRE_CURR_LMT_DELAY)))
	{
		//Calculate sum of rectifies current limit
		float fSumOfCurrLmt = 0.0;
		List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);
		do
		{
			GC_RECT_INFO	RectifierInfo;

			if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, 
						(void*)(&RectifierInfo)))
			{
				fSumOfCurrLmt += GC_GetFloatValue(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType, 
												RectifierInfo.iRectNo, 
												aiRTCurrentLimitSid[RectifierInfo.iRectEquipType]);
			}
		}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));	

		if(g_pGcData->RunInfo.Rt.iQtyOnWorkRect)
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = fSumOfCurrLmt
						 / g_pGcData->RunInfo.Rt.iQtyOnWorkRect;
		}
		else
		{
			g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;		
		}
	}

	if (STATE_AUTO == g_pGcData->RunInfo.enumAutoMan)
	{
		OpenAllRect();
		s_iUnderVoltCounter = 0;
	}
	else if(GC_IsUnderVolt())
	//If undervoltage, then AutoMan state turn to Auto
	{
		if(s_iUnderVoltCounter > HYSTERESIS_TIMES_MAN_TO_AUTO)
		{
			GC_SetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN, STATE_AUTO, TRUE);
			s_iUnderVoltCounter = 0;
		}
		else
		{
			s_iUnderVoltCounter ++;
		}
	}
	else
	{
		s_iUnderVoltCounter = 0;
	}

	s_enumLastAutoMan = g_pGcData->RunInfo.enumAutoMan;

	return;

}


#define	TEMP_COMP_VOLT_LIMIT			(2.0)
#define	TEMP_COMP_LOW_LIMIT			(-5.0)
#define	TEMP_COMP_HIGH_LIMIT			(40.0)
#define	TEMP_COMP_MIN_LOW_LIMIT			(-40.0)
#define	TEMP_COMP_MAX_HIGH_LIMIT		(100.0)
#define	TEMP_COMP_DEAD_AREA_HIGH_LIMIT	(0.02)
#define	TEMP_COMP_DEAD_AREA_LOW_LIMIT	(0.01)
/*==========================================================================*
 * FUNCTION : GetTempCompDelta
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-04 14:27
 *==========================================================================*/
static float GetTempCompDelta(void)
{
	float				fTempForCompen;
	float				fNomTemp;
	float				fFactor;
	float				fDelta = 0.0;
	SIG_BASIC_VALUE*		pSigValue;
	int				iTempNoForCompen;
	int				iRst;
	SIG_ENUM			enumTempUsage = 0;
	SIG_ENUM			enumTempNotConnectAlm = 0;
	SIG_ENUM			enumTempFaultAlm = 0;
	float				fDeltaLmt;
	float				fFloatVolt;
	float				fHighVolt;
	float				fLowVolt;
	BOOL				bCompensation = FALSE;
	float				fHighTempSigValue,fLowTempSigValue;
	SIG_ENUM stVoltLevelState  = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);
	SIG_ENUM enumState = 0;
//#define GC_SUPPORT_ESNA_COMP_MODE	//for ESNA CR demand,add a new comp temp mode that does not have to do with temp alarm
#define ENABLE_ESNA_MODE	1
	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);


	iTempNoForCompen = GC_GetEnumValue(TYPE_OTHER,
								0,
								GC_PUB_NO_FOR_COMPEN);
	//ASSERT(iTempNoForCompen >= 0 && iTempNoForCompen <= MAX_NUM_TEMPERATURE);

	////int iNotLduTempNum = MAX_NUM_TEMPERATURE - MAX_NUM_LargeDU_TEMP;
	//
	//if((iTempNoForCompen > 0) && (iTempNoForCompen <= iNotLduTempNum))	//Not LargeDU Temp
	//{
	//	enumTempUsage = GC_GetEnumValue(TYPE_OTHER, 
	//						0,
	//						GC_PUB_TEMP1_USAGE + iTempNoForCompen - 1);

	//	enumTempNotConnectAlm = GC_GetEnumValue(TYPE_OTHER, 
	//						0,
	//						GC_PUB_TEMP1_NOT_CONNECT_ALM + iTempNoForCompen - 1);

	//	enumTempFaultAlm = GC_GetEnumValue(TYPE_OTHER, 
	//						0,
	//						GC_PUB_TEMP1_FAULT_ALM + iTempNoForCompen - 1);


	//	if((enumTempUsage != TEMP_USAGE_DISABLED)
	//		&& (enumTempNotConnectAlm == GC_NORMAL)
	//		&& (enumTempFaultAlm == GC_NORMAL))
	//	{
	//		bCompensation = TRUE;
	//	}
	//}
	//else if ((iTempNoForCompen > iNotLduTempNum) && (iTempNoForCompen <= MAX_NUM_TEMPERATURE)) //LargeDU temp
	//{
	//	int iLduTempNum = GC_GetDwordValue(TYPE_OTHER, 0, GC_PUB_LduTEMP_NUM);
	//	if(iTempNoForCompen - iNotLduTempNum <= iLduTempNum)
	//	{
	//		bCompensation = TRUE;
	//	}
	//}


	static BOOL s_OverOffTempAgo = FALSE;

	SIG_ENUM stTempCompEnable  = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_TMP_CMP_ENABLE_BY_SENSOR_FAIL);
	SIG_ENUM stThredFuncEnable  = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_TMP_CMP_THRED_FUNC_ENABLE);
	float fOffTemp = GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_OFF_TEMP);
	float fOnTemp = GC_GetFloatValue(TYPE_OTHER, 0, GC_PUB_ON_TEMP);

	//printf(" GetTempCompDelta():stTempCompEnable=%d,stThredFuncEnable=%d,fOffTemp=%f,fOnTemp=%f~~~~----0\n",stTempCompEnable,stThredFuncEnable,fOffTemp,fOnTemp);



	if(iTempNoForCompen == 0)
	{
		bCompensation = FALSE;
	}
	else
	{
		bCompensation = TRUE;
	}


	if(bCompensation)
	{

		iRst = GC_GetSigVal(TYPE_OTHER, 
						0, 
						GC_PUB_BATTGROUP_BATTTEMP, 
						&pSigValue);		


		GC_ASSERT((iRst == ERR_OK), 
				ERR_CTL_GET_VLAUE, 
				"GC_GetSigVal GC_PUB_TEMPERATURE1 error!\n");
		if (iRst == ERR_DXI_INVALID_EQUIP_ID)
		{
			fDelta = 0.0;
			return fDelta; 
		}
		if(SIG_VALUE_IS_VALID(pSigValue))
		{
			int iESNAMode_Enable = 0;
			float	fESNA_HighVolt = 0.0;
			float	fESNA_LowVolt = 0.0;
#ifdef GC_SUPPORT_ESNA_COMP_MODE
			iESNAMode_Enable = GC_GetEnumValue(TYPE_OTHER,0,BT_PUB_ESNA_COMP_MODE_ENABLE);
			if(iESNAMode_Enable == 1)
			{
				fESNA_HighVolt = GC_GetSettingVolt(BT_PUB_ESNA_COMP_HI_VOLT);//GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_ESNA_COMP_HI_VOLT); 
				fESNA_LowVolt = GC_GetSettingVolt(BT_PUB_ESNA_COMP_LOW_VOLT);//GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_ESNA_COMP_LOW_VOLT);
			}
#endif
			fTempForCompen = GC_GetFloatValue(TYPE_OTHER, 
									0, 
									GC_PUB_BATTGROUP_BATTTEMP);
		
			if(fTempForCompen < TEMP_COMP_MIN_LOW_LIMIT)
			{
				fDelta = 0.0;
				GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_TEMP_COMPEN_SET,
						GC_NORMAL,
						TRUE);
				return fDelta;				
			}
			else if(fTempForCompen > TEMP_COMP_MAX_HIGH_LIMIT)
			{
				fDelta = 0.0;
				GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_TEMP_COMPEN_SET,
						GC_NORMAL,
						TRUE);
						
				return fDelta;	
			}
			fNomTemp = GC_GetFloatValue(TYPE_OTHER,
				0,
				BT_PUB_NOM_TEMP);

			if(enumState == ST_MAN_BC || 
				enumState == ST_AUTO_BC ||
				enumState == ST_CYCLIC_BC )
			{


				fFactor = GC_GetFloatValue(TYPE_OTHER,
					0, 
					BT_PUB_EQ_COMPEN_FACTOR);

			}
			else
			{
				fFactor = GC_GetFloatValue(TYPE_OTHER,
					0, 
					BT_PUB_COMPEN_FACTOR);
			}

			//HPM CR 2018-9-5
			if( 0 == stTempCompEnable )
			{
				fDelta = 0.0;
				GC_SetEnumValue(TYPE_OTHER, 
									0,
									BT_PUB_TEMP_COMPEN_SET,
									GC_NORMAL,
									TRUE);
				return fDelta;	
			}

			//printf("GetTempCompDelta():fTempForCompen=%f,fNomTemp=%f,fFactor=%f~~~~--2\n",fTempForCompen,fNomTemp,fFactor);

			//HPM CR 2018-9-5
			if( 1 == stThredFuncEnable )
			{				
				if( fTempForCompen>= fOffTemp )
				{
					s_OverOffTempAgo = TRUE;
					fDelta = 0.0;
					GC_SetEnumValue(TYPE_OTHER, 
									0,
									BT_PUB_TEMP_COMPEN_SET,
									GC_NORMAL,
									TRUE);
					return fDelta;	
				}
				else if( ( fTempForCompen> fOnTemp ) && ( fTempForCompen < fOffTemp ) )
				{
					if(s_OverOffTempAgo)
					{
						fDelta = 0.0;
						GC_SetEnumValue(TYPE_OTHER, 
										0,
										BT_PUB_TEMP_COMPEN_SET,
										GC_NORMAL,
										TRUE);
						return fDelta;	
					}
				}
				else
				{
					if(s_OverOffTempAgo)
					{
						s_OverOffTempAgo = FALSE;
					}
				}
			}


			//Modified by Jimmy 2011-12-23 for ESNA temp CR		
			if(iESNAMode_Enable == ENABLE_ESNA_MODE && fESNA_HighVolt > 0.0 && fESNA_LowVolt > 0.0)
			{
				fDelta = (fNomTemp - fTempForCompen) * fFactor / 1000;

				fFloatVolt = GC_GetSettingVolt(BT_PUB_NOM_VOLT);


				if((fDelta + fFloatVolt) > (fESNA_HighVolt))
				{
					fDelta = fESNA_HighVolt - fFloatVolt;
				}

				if((fDelta + fFloatVolt) < (fESNA_LowVolt))
				{
					fDelta = fESNA_LowVolt - fFloatVolt;
				}

				//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0) //if user set wrong range ,here limit the range
				if(1 == stVoltLevelState)//24V
				{
					if(fDelta > 1.0)
					{
						fDelta = 1.0;
					}
					else if(fDelta < -1.0)
					{
						fDelta = -1.0;
					}
				}
				else
				{
					if(fDelta > 2.5)
					{
						fDelta = 2.5;
					}
					else if(fDelta < -2.5)
					{
						fDelta = -2.5;
					}
				}
				

			}
			else
			{


				fHighTempSigValue = GC_GetFloatValue(TYPE_OTHER, 
					0, 
					GC_PUB_BATTGROUP_BATTTEMP_HIGH_LIMIT); 
				fLowTempSigValue = GC_GetFloatValue(TYPE_OTHER, 
					0, 
					GC_PUB_BATTGROUP_BATTTEMP_LOW_LIMIT);


				if(fTempForCompen > fHighTempSigValue)
				{
					fTempForCompen = fHighTempSigValue;				
				}
				if(fTempForCompen < fLowTempSigValue)
				{
					fTempForCompen = fLowTempSigValue;				
				}

				fDelta = (fNomTemp - fTempForCompen) * fFactor / 1000;

				//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
				if(1 == stVoltLevelState)//24V
				{
					fDeltaLmt = TEMP_COMP_VOLT_LIMIT / 2;				
				}
				else
				{
					fDeltaLmt = TEMP_COMP_VOLT_LIMIT;

				}

				if(enumState == ST_MAN_BC || 
				   enumState == ST_AUTO_BC ||
				   enumState == ST_CYCLIC_BC )
				{
					fFloatVolt = GC_GetSettingVolt(BT_PUB_BST_VOLT);
					fHighVolt = 60;
					fLowVolt = 40;

					if((fDelta + fFloatVolt) > (fHighVolt - 0.2))
					{
						fDelta = fHighVolt - fFloatVolt - 0.2;
					}

					if((fDelta + fFloatVolt) < (fLowVolt + 0.2))
					{
						fDelta = fLowVolt + 0.2 - fFloatVolt;
					}
					if(fDelta > fDeltaLmt)
					{
						fDelta = fDeltaLmt;
					}

					if(fDelta < (0.0 - fDeltaLmt))
					{
						fDelta = 0.0 - fDeltaLmt;
					}
				}
				else
				{

					fFloatVolt = GC_GetSettingVolt(BT_PUB_NOM_VOLT);
					fHighVolt = GC_GetSettingVolt(DC_PUB_HIGH_VOLT);
					fLowVolt = GC_GetSettingVolt(DC_PUB_UNDER_VOLT);

					if((fDelta + fFloatVolt) > (fHighVolt - 0.2))
					{
						fDelta = fHighVolt - fFloatVolt - 0.2;
					}

					if((fDelta + fFloatVolt) < (fLowVolt + 0.2))
					{
						fDelta = fLowVolt + 0.2 - fFloatVolt;
					}
					if(fDelta > fDeltaLmt)
					{
						fDelta = fDeltaLmt;
					}

					if(fDelta < (0.0 - fDeltaLmt))
					{
						fDelta = 0.0 - fDeltaLmt;
					}
				}
			}
			//printf("fTempForCompen = %f, fDelta = %f, fNomTemp = %f, fDeltaLmt = %f, fFloatVolt = %f, fHighVolt = %f, fLowVolt = %f", fTempForCompen, fDelta, fNomTemp, fDeltaLmt, fFloatVolt, fHighVolt, fLowVolt);
		}
		else
		{
			fDelta = 0.0;
		}
	}
	else
	{
		fDelta = 0.0;
	}

/*
	static int 
	
	if( fTempForCompen > fOffTemp)
	{
		fDelta = 0.0;
	}
	else if( (fTempForCompen < fOffTemp) && (fTempForCompen > fOnTemp) )
	{
		
	}
	else
	{
		
	}
*/



	if(ABS(fDelta) > TEMP_COMP_DEAD_AREA_HIGH_LIMIT)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_TEMP_COMPEN_SET,
						GC_ALARM,
						TRUE);
	}
	else if(ABS(fDelta) < TEMP_COMP_DEAD_AREA_LOW_LIMIT)
	{
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						BT_PUB_TEMP_COMPEN_SET,
						GC_NORMAL,
						TRUE);
	}
	
	return fDelta;
}



/*==========================================================================*
 * FUNCTION : GetMaxCurrDiff
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-11-11 10:49
 *==========================================================================*/
static float GetMaxCurrDiff(void)
{
	//float		fMaxDiffer;

    
	//if(g_pGcData->RunInfo.Rt.iQtyOnWorkRect > 2)
	//{
	//	if(FLOAT_EQUAL(100.0, GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_RATED_CURR)))
	//	{
	//		fMaxDiffer = 2.5 * g_pGcData->RunInfo.Rt.iQtyOnWorkRect;
	//	}
	//	else
	//	{
	//		fMaxDiffer = 1.5 * g_pGcData->RunInfo.Rt.iQtyOnWorkRect;
	//	}
	//}
	//else
	//{
	//	if(g_pGcData->RunInfo.Rt.iQtyOnWorkRect)
	//	{
	//		if(FLOAT_EQUAL(100.0, GC_GetFloatValue(TYPE_OTHER, 0, RT_PUB_RATED_CURR)))
	//		{
	//			fMaxDiffer = 5.0 * g_pGcData->RunInfo.Rt.iQtyOnWorkRect;
	//		}
	//		else
	//		{
	//			fMaxDiffer = 3.0 * g_pGcData->RunInfo.Rt.iQtyOnWorkRect;
	//		}
	//	}
	//	else		//No valid rectifier
	//	{
	//		fMaxDiffer = 8.0;
	//	}
	//}
	int	i;
	float	fMaxBatRatedCurr = 0.0;
	
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{
		if(fMaxBatRatedCurr  < g_pGcData->RunInfo.Bt.afBattRatedCap[i])
		{
			fMaxBatRatedCurr = g_pGcData->RunInfo.Bt.afBattRatedCap[i];
		}
	}

	//return (0.005 * fMaxBatRatedCurr);
	return MAX(g_pGcData->RunInfo.Rt.iQtyOnWorkRect, 
				MAX(0.005 * fMaxBatRatedCurr, 5));
}



/*==========================================================================*
 * FUNCTION : GC_GetSettingVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iSigId : 
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-09-29 17:46
 *==========================================================================*/
float GC_GetSettingVolt(int iSigId)
{
	float		fVolt;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		fVolt = GC_GetFloatValue(TYPE_OTHER, 0, iSigId + 1);
	}
	else
	{
		fVolt = GC_GetFloatValue(TYPE_OTHER, 0, iSigId);
	}
	return fVolt;
}

/*==========================================================================*
* FUNCTION : GC_GetVoltCheck
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int iSigId,float fVolt: 
* RETURN   : float : 
* COMMENTS : 
* CREATOR  :                DATE: 2013-09-29 17:46
*==========================================================================*/
static float GC_GetVoltCheck(int iSigId,float fVolt)
{
	float	fVoltOutput,fUpLimit,fDnLimit;
	int iRst,iBufLen;
	CTRL_SIG_INFO *pCtrlSigInfo;

	fVoltOutput =  fVolt;

	iRst = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
		(g_pGcData->PriCfg.pSig + iSigId)->iEquipId,
		DXI_MERGE_SIG_ID((g_pGcData->PriCfg.pSig + iSigId)->iSignalType, 
		(g_pGcData->PriCfg.pSig + iSigId)->iSignalId),
		&iBufLen,
		&pCtrlSigInfo,
		0);
	if(iRst != ERR_DXI_OK )
	{
		return fVoltOutput;
	}

	fUpLimit =pCtrlSigInfo->fMaxValidValue;
	fDnLimit =pCtrlSigInfo->fMinValidValue;
	
	if(fVoltOutput>fUpLimit)
	{
		fVoltOutput=fUpLimit;
	}
	if(fVoltOutput<fDnLimit)
	{
		fVoltOutput=fDnLimit;
	}

	return fVoltOutput;
	
}



void GC_DcVoltCtl(float fVolt)
{
#define DELTA_OF_SLAVE_VOLTAGE			(0.5)

	int	i;
	float fDelta = 0.0;
	float fVoltOutput;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	if((g_pGcData->RunInfo.Rt.bMasterNoRespond || GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CAN_FAILURE)) && g_pGcData->enumMasterSlaveMode == MODE_MASTER)//master mode & (can failure | all rectifiers no response)
	{
		fDelta = 0.0;
	}
	else
	{
		fDelta = DELTA_OF_SLAVE_VOLTAGE;
	}
	
	
	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{
				fVoltOutput =GC_GetVoltCheck(RT_PUB_DC_VOLT_CTL+i,fVolt+fDelta);
				GC_SetFloatValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_VOLT_CTL + i,
						fVoltOutput);  //Control the slave's voltage +Delta. 	

			}		
		}
		fVoltOutput =GC_GetVoltCheck(RT_PUB_DC_VOLT_CTL_24V,fVolt);
		GC_SetFloatValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_VOLT_CTL_24V,
						fVoltOutput);

		
	}
	else
	{
		for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{
				fVoltOutput =GC_GetVoltCheck(RT_PUB_DC_VOLT_CTL+i,fVolt+fDelta);
				GC_SetFloatValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_VOLT_CTL + i,
						fVoltOutput);  //Control the slave's voltage +Delta. 	

			}		
		}
		fVoltOutput =GC_GetVoltCheck(RT_PUB_DC_VOLT_CTL,fVolt);
		GC_SetFloatValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_VOLT_CTL,
						fVoltOutput);	

		
	}
	return;
}
#ifdef GC_SUPPORT_MPPT
static void GC_DcVoltCtl_Mppt(float fVolt)
{
#define DELTA_OF_SLAVE_VOLTAGE_MPPT			(0.0)


	int	i;
	float fDelta = 0.0;
	float fVoltOutput;

	//if((g_pGcData->RunInfo.Mt.bNoRespond || GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CAN_FAILURE)) && g_pGcData->enumMasterSlaveMode == MODE_MASTER)//master mode & (can failure | all rectifiers no response)
	if((g_pGcData->RunInfo.Mt.bNoRespond || GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_CAN_FAILURE)) || GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE) == GC_MPPT_MPPT)//master mode & (can failure | all rectifiers no response)

	{
		fDelta = 0.0;
	}
	else
	{
		fDelta = DELTA_OF_SLAVE_VOLTAGE_MPPT;
	}
	fVolt += fDelta;

	fVoltOutput =GC_GetVoltCheck(MT_PUB_DC_VOLT_CTL,fVolt);

	//printf("Sending fVolt = %f\n", fVolt);
	GC_SetFloatValue(TYPE_OTHER, 
			0, 
			MT_PUB_DC_VOLT_CTL,
			fVoltOutput);	
	return;
}

static void GC_ControlVoltage(IN int iState)
{
	float fVolt;
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
	if(stState == GC_MPPT_MPPT_RECT)//Rect-MPPT
	{
		if(iState == BT_PUB_BST_VOLT)//Boost 
		{
			if(g_pGcData->RunInfo.Mt.bNight)
			{
				fVolt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BST_VOLT_MPPT);
			}
			else
			{
				fVolt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BST_VOLT_RECT);
			}
			GC_DcVoltCtl(fVolt);
			fVolt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BST_VOLT_MPPT);
			GC_DcVoltCtl_Mppt(fVolt);
		}
		else if(iState == BT_PUB_NOM_VOLT)//Float
		{
			if(g_pGcData->RunInfo.Mt.bNight)
			{
				fVolt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_MPPT);
			}
			else
			{
				fVolt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_RECT);
			}
			GC_DcVoltCtl(fVolt);
			fVolt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_MPPT);
			GC_DcVoltCtl_Mppt(fVolt);
		}
	}
	else if(stState == GC_MPPT_MPPT)//MPPT
	{
		if(iState == BT_PUB_BST_VOLT)//Boost 
		{
			fVolt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_BST_VOLT_MPPT);
			GC_DcVoltCtl_Mppt(fVolt);
		}
		else if(iState == BT_PUB_NOM_VOLT)//Float
		{
			fVolt = GC_GetFloatValue(TYPE_OTHER, 0, BT_PUB_NOM_VOLT_MPPT);
			GC_DcVoltCtl_Mppt(fVolt);
		}
	}
	else//48V
	{
		fVolt = GC_GetSettingVolt(iState);
		GC_DcVoltCtl(fVolt);
	}
	return;
}
#endif

void GC_DcVoltCtlForTest(float fVolt, BOOL bStart)
{
	int	i;
	BOOL    bExistSlave = FALSE;
	float   fVoltOutput;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{
				fVoltOutput =GC_GetVoltCheck(RT_PUB_DC_VOLT_CTL+i,fVolt);
				GC_SetFloatValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_VOLT_CTL + i,
						fVoltOutput);  //Control the slave's voltage +Delta. 	
				bExistSlave = TRUE;

			}		
		}
		if(bExistSlave && bStart)
		{
			Sleep(10000);
		}
		fVoltOutput =GC_GetVoltCheck(RT_PUB_DC_VOLT_CTL_24V,fVolt);
		GC_SetFloatValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_VOLT_CTL_24V,
						fVoltOutput);

		
	}
	else
	{

		for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{
				fVoltOutput =GC_GetVoltCheck(RT_PUB_DC_VOLT_CTL + i,fVolt);
				GC_SetFloatValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_VOLT_CTL + i,
						fVoltOutput);  //Control the slave's voltage +Delta. 
				bExistSlave = TRUE;

			}		
		}
		if(bExistSlave && bStart)
		{
			Sleep(10000);
		}
		fVoltOutput =GC_GetVoltCheck(RT_PUB_DC_VOLT_CTL,fVolt);
		GC_SetFloatValue(TYPE_OTHER, 
						0, 
						RT_PUB_DC_VOLT_CTL,
						fVoltOutput);	
	 

		
	}
	return;
}

#define	AMNORMAL_CURR_ERROR			5.0
#define	AMNORMAL_CURR_ERROR_COEF	0.005
#define	HYSTERESIS_TIMES_ABNOR_LOAD	6
/*==========================================================================*
 * FUNCTION : CmpRtBtCurr
 * PURPOSE  : According to battery current and rectifier current, judge if 
              load cureent is abnormal
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-09 16:28
 *==========================================================================*/
static void CmpRtBtCurr(void)
{
	static BOOL s_bLast = FALSE;
	static int	s_iCounter = 0;
	int			i;
	BOOL		bDisconnect = FALSE;
	float		fSumCurr = 0;

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
#endif

	//To judge if there is LVD disconnected.
	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{
		if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 0])
		{
			if(GC_ENABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD1_PRI_ENB,GC_EQUIP_INVALID))
			{
				if(GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 0])
				{
					bDisconnect = TRUE;
					break;
				}
			}
		}

		if(g_pGcData->RunInfo.Lvd.abValid[i * 2 + 1])
		{
			if(GC_ENABLED == GC_IsGCEquipExist(TYPE_LVD_UNIT, i, LVD2_PRI_ENB,GC_EQUIP_INVALID))
			{
				if(GC_DISCONNECTED == g_pGcData->RunInfo.Lvd.aenumState[i * 2 + 1])
				{
					bDisconnect = TRUE;
					break;
				}
			}
		}
	}

	//If there is LVD disconnected, "Abnormal load current" should be normal,
	//because "Abnormal load current" would make LVD connected.
	if(bDisconnect)
	{
		g_pGcData->RunInfo.Bt.bErrCurr = FALSE;
		GC_SetEnumValue(TYPE_OTHER,
						0,
						GC_PUB_ABNOR_LOAD_SET, 
						GC_NORMAL,
						TRUE);
		s_iCounter = 0;
		return;
	}


#ifdef GC_SUPPORT_MPPT
	if(stState != GC_MPPT_DISABLE)
	{
		fSumCurr = g_pGcData->RunInfo.Rt.fSumOfCurr + g_pGcData->RunInfo.Mt.fSumOfCurr
			- g_pGcData->RunInfo.Bt.fSumOfCurr;
	}
	else
	{
		fSumCurr = g_pGcData->RunInfo.Rt.fSumOfCurr
			- g_pGcData->RunInfo.Bt.fSumOfCurr;

	}
#else
	fSumCurr = g_pGcData->RunInfo.Rt.fSumOfCurr
		- g_pGcData->RunInfo.Bt.fSumOfCurr;

#endif

	if(fSumCurr < (0.0 - GetMaxCurrDiff()))
	{
		if(s_iCounter >= HYSTERESIS_TIMES_ABNOR_LOAD)
		{
			g_pGcData->RunInfo.Bt.bErrCurr = TRUE;
			GC_SetEnumValue(TYPE_OTHER, 
							0,
							GC_PUB_ABNOR_LOAD_SET, 
							GC_ALARM,
							TRUE);

			if(!s_bLast)
			{
				GC_LOG_OUT(APP_LOG_WARNING, 
					"Imbalance current checked, raise Imbalance Current alarm.\n");
				s_bLast = TRUE;
			}
		}
		else
		{
			s_iCounter++;
		}
	}
	else 
	{
		s_iCounter = 0;

		g_pGcData->RunInfo.Bt.bErrCurr = FALSE;	
		
		GC_SetEnumValue(TYPE_OTHER, 
						0,
						GC_PUB_ABNOR_LOAD_SET, 
						GC_NORMAL,
						TRUE);

		if(s_bLast)
		{
			GC_LOG_OUT(APP_LOG_INFO, 
				"No imbalance current! Imbalance Current alarm end.\n");
			s_bLast = FALSE;
		}
	}

	return;
}

/*==========================================================================*
 * FUNCTION : GC_HandlingOnErr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2005-03-09 17:02
 *==========================================================================*/
void GC_HandlingOnErr(SIG_ENUM enumState, const char *szLog)
{
	int		i;

	if((STATE_MAN == g_pGcData->RunInfo.enumAutoMan)
		&& (ST_MAN_TEST != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		&& (ST_AC_FAIL_TEST != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)))
	{
		if(ST_AC_FAIL != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		{
			return;
		}
	}

	if(GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) != enumState) 
	{
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE, enumState, TRUE);		
		GC_SetEnumValue(TYPE_OTHER,	0, BT_PUB_BOOST_CTL, CTL_FLOAT, TRUE);	
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL, CTL_TEST_STOP, TRUE);

		GC_LOG_OUT(APP_LOG_WARNING, szLog);
	}

	GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);
	
	g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
	GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF, 
					g_pGcData->RunInfo.Rt.fCurrLmt);
#ifdef GC_SUPPORT_MPPT
	//g_pGcData->RunInfo.Mt.fCurrLmt = GC_MAX_CURR_LMT;
	//GC_SendMTCurrentLmt(g_pGcData->RunInfo.Mt.fCurrLmt);

#endif

	/*for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
	{
		if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
		{

			GC_SetFloatValue(TYPE_OTHER, 
			0,
			RT_PUB_CURR_LMT_CTL_SELF + i,
			g_pGcData->RunInfo.Rt.fCurrLmt);				

		}		
	}*/
	GC_CurrlmtCtl_Slave(g_pGcData->RunInfo.Rt.fCurrLmt);
	//~{o?=o?=o?=o?=o?=M;o?=o?=X1U2o?=o?=o?=~}?
	
	return;
}

void GC_HandlingFiammBatt(SIG_ENUM enumState, const char *szLog)
{
	int		i;

	if((STATE_MAN == g_pGcData->RunInfo.enumAutoMan)
		&& (ST_MAN_TEST != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		&& (ST_AC_FAIL != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		&& (ST_AC_FAIL_TEST != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)))
	{
		return;
	}

	if(GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) != enumState) 
	{
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE, enumState, TRUE);		
		GC_SetEnumValue(TYPE_OTHER,	0, BT_PUB_BOOST_CTL, CTL_FLOAT, TRUE);	
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL, CTL_TEST_STOP, TRUE);

		GC_LOG_OUT(APP_LOG_WARNING, szLog);
	}

	GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);
	
	g_pGcData->RunInfo.Rt.fCurrLmt = GC_MAX_CURR_LMT;
	GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF, 
					g_pGcData->RunInfo.Rt.fCurrLmt);	
	
	return;
}
#ifdef GC_SUPPORT_BMS
void GC_HandlingBMS(SIG_ENUM enumState, const char *szLog)
{
	int		i;

	printf("------1--------\n");
	if((STATE_MAN == g_pGcData->RunInfo.enumAutoMan)
		&& (ST_MAN_TEST != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		&& (ST_AC_FAIL != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		&& (ST_AC_FAIL_TEST != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)))
	{
		return;
	}
	printf("------2--------\n");
	if(GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) != enumState) 
	{
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE, enumState, TRUE);		
		GC_SetEnumValue(TYPE_OTHER,	0, BT_PUB_BOOST_CTL, CTL_FLOAT, TRUE);	
		GC_SetEnumValue(TYPE_OTHER, 0, BT_PUB_TEST_CTL, CTL_TEST_STOP, TRUE);

		GC_LOG_OUT(APP_LOG_WARNING, szLog);
	}
	printf("------3--------\n");
	GC_RectVoltCtl(TYPE_FLOAT_VOLT, FALSE);	
	GC_CurrentLmt_BMS();	
	printf("------4--------\n");
	return;
}
#endif
//#define MAX_REGULATE_VOLT					(0.03)
#define MIN_SEND_VOLTAGE					(42.0)
#define MAX_NEED_REGULATE_VOLT				(0.1)
#define MAX_NEED_REGULATE_MINUS_VOLT		(-0.1)
#define MIN_SEND_VOLTAGE_24V				(21.0)
#define MAX_NEED_REGULATE_VOLT_24V			(0.1)
#define MAX_NEED_REGULATE_MINUS_VOLT_24V	(-0.1)

/*==========================================================================*
 * FUNCTION : GC_GetRegulateVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: float  fAimVoltage : 
 * RETURN   : float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2007-03-08 21:18
 *==========================================================================*/
static float GC_GetRegulateVolt_MPPT(float fAimVoltage)
{
	float		fRegulateVolt = 0.0;	
	float		fSysVoltage = 0.0;
	float		fRectVoltage = 0.0;
	float		fSendVoltage = 0.0;
	float		fDiffer = 0.0;
	static	float	s_fReturnVolt = 0.0;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);


	fSysVoltage = GC_GetFloatValue(TYPE_OTHER,
								0,
								GC_PUB_SYS_VOLT);

	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	SIG_ENUM	stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
	fRectVoltage = GC_GetFloatValue(TYPE_OTHER,
								0,
								MT_PUB_DC_VOLT_CTL);	

	fSendVoltage = fRectVoltage + (fAimVoltage - fSysVoltage) * 0.8;
	/*fDiffer = fSendVoltage - fAimVoltage;*/
	fDiffer = (fAimVoltage - fSysVoltage) * 0.8;


	if(//GC_IsCurrentLimit() 
		/*||*/ GC_IsPowerLmt() 
		//|| ((g_pGcData->RunInfo.Rt.fRatedVolt > 35.0) && (fSendVoltage < MIN_SEND_VOLTAGE))
		//|| ((g_pGcData->RunInfo.Rt.fRatedVolt < 35.0) && (fSendVoltage < MIN_SEND_VOLTAGE_24V))
		|| ((0 == stVoltLevelState) && (fSendVoltage < MIN_SEND_VOLTAGE))
		|| ((1 == stVoltLevelState) && (fSendVoltage < MIN_SEND_VOLTAGE_24V))
		|| (STATE_AUTO != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN))
		|| ((ST_FLOAT != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
			&& (ST_AUTO_BC != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)) 
			&& (ST_CYCLIC_BC != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
			&& (ST_MAN_BC != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))))
		//|| ((ABS(fSysVoltage - fAimVoltage)) < MAX_REGULATE_VOLT))
	{
		fRegulateVolt = 0.0;
		s_fReturnVolt = 0.0;		
	}
	else if(ST_AUTO_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) || ST_CYCLIC_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)|| ST_MAN_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
	{
		if(fSysVoltage < fAimVoltage - 0.5)
		{
			fRegulateVolt = 0.0;
			s_fReturnVolt = 0.0;	
		}
		else
		{
			fRegulateVolt = fDiffer;
		}
	}
	else
	{

		fRegulateVolt = fDiffer;
	}		
	//if(g_pGcData->RunInfo.Rt.fRatedVolt > 35.0)
	if(stVoltLevelState == 0)//48V
	{
		if(fRegulateVolt > MAX_NEED_REGULATE_VOLT)
		{
			fRegulateVolt = MAX_NEED_REGULATE_VOLT;
		}
		else if(fRegulateVolt < MAX_NEED_REGULATE_MINUS_VOLT)
		{
			fRegulateVolt = MAX_NEED_REGULATE_MINUS_VOLT;
		}
	}
	else
	{
		if(fRegulateVolt > MAX_NEED_REGULATE_VOLT_24V)
		{
			fRegulateVolt = MAX_NEED_REGULATE_VOLT_24V;
		}
		else if(fRegulateVolt < MAX_NEED_REGULATE_MINUS_VOLT_24V)
		{
			fRegulateVolt = MAX_NEED_REGULATE_MINUS_VOLT_24V;
		}	
	}

	s_fReturnVolt += fRegulateVolt;

	//if(g_pGcData->RunInfo.Rt.fRatedVolt > 35.0)
	if(stVoltLevelState == 0)//48V
	{
		if(s_fReturnVolt > 5 * MAX_NEED_REGULATE_VOLT)
		{
			s_fReturnVolt = 5 * MAX_NEED_REGULATE_VOLT;
		}
		else if(s_fReturnVolt < 5 * MAX_NEED_REGULATE_MINUS_VOLT)
		{
			s_fReturnVolt = 5 * MAX_NEED_REGULATE_MINUS_VOLT;
		}
	}
	else
	{
		if(s_fReturnVolt > 5 * MAX_NEED_REGULATE_VOLT_24V)
		{
			s_fReturnVolt = 5 *  MAX_NEED_REGULATE_VOLT_24V;
		}
		else if(s_fReturnVolt < 5 * MAX_NEED_REGULATE_MINUS_VOLT_24V)
		{
			s_fReturnVolt = 5 * MAX_NEED_REGULATE_MINUS_VOLT_24V;
		}	
	}

	return s_fReturnVolt;
}
float GC_GetRegulateVolt(float fAimVoltage)
{
	float		fRegulateVolt = 0.0;	
	float		fSysVoltage = 0.0;
	float		fRectVoltage = 0.0;
	float		fSendVoltage = 0.0;
  	float		fDiffer = 0.0;
	static	float	s_fReturnVolt = 0.0;
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);


	fSysVoltage = GC_GetFloatValue(TYPE_OTHER,
								0,
								GC_PUB_SYS_VOLT);

	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		fRectVoltage = GC_GetFloatValue(TYPE_OTHER,
								0,
								RT_PUB_DC_VOLT_CTL_24V);
	}
	else
	{
		fRectVoltage = GC_GetFloatValue(TYPE_OTHER,
								0,
								RT_PUB_DC_VOLT_CTL);
	
	}


	fSendVoltage = fRectVoltage + (fAimVoltage - fSysVoltage) * 0.8;
	/*fDiffer = fSendVoltage - fAimVoltage;*/
	fDiffer = (fAimVoltage - fSysVoltage) * 0.8;


	if(//GC_IsCurrentLimit() 
		/*||*/ GC_IsPowerLmt() 
		//|| ((g_pGcData->RunInfo.Rt.fRatedVolt > 35.0) && (fSendVoltage < MIN_SEND_VOLTAGE))
		//|| ((g_pGcData->RunInfo.Rt.fRatedVolt < 35.0) && (fSendVoltage < MIN_SEND_VOLTAGE_24V))
		|| ((0 == stVoltLevelState) && (fSendVoltage < MIN_SEND_VOLTAGE))
		|| ((1 == stVoltLevelState) && (fSendVoltage < MIN_SEND_VOLTAGE_24V))
		|| (STATE_AUTO != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_AUTO_MAN))
		|| ((ST_FLOAT != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)) 
		&& (ST_AUTO_BC != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)) 
		&& (ST_CYCLIC_BC != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
		&& (ST_MAN_BC != GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))))
		//|| ((ABS(fSysVoltage - fAimVoltage)) < MAX_REGULATE_VOLT))
	{
		fRegulateVolt = 0.0;
		s_fReturnVolt = 0.0;		
	}
	else if(ST_AUTO_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE) || ST_CYCLIC_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE)|| ST_MAN_BC == GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE))
	{
		if(fSysVoltage < fAimVoltage - 0.5)
		{
			fRegulateVolt = 0.0;
			s_fReturnVolt = 0.0;	
		}
		else
		{
			fRegulateVolt = fDiffer;
		}
		
	}
	else
	{

		fRegulateVolt = fDiffer;
	}		
	//if(g_pGcData->RunInfo.Rt.fRatedVolt > 35.0)
	if(stVoltLevelState == 0)//48V
	{
		if(fRegulateVolt > MAX_NEED_REGULATE_VOLT)
		{
			fRegulateVolt = MAX_NEED_REGULATE_VOLT;
		}
		else if(fRegulateVolt < MAX_NEED_REGULATE_MINUS_VOLT)
		{
			fRegulateVolt = MAX_NEED_REGULATE_MINUS_VOLT;
		}
	}
	else
	{
		if(fRegulateVolt > MAX_NEED_REGULATE_VOLT_24V)
		{
			fRegulateVolt = MAX_NEED_REGULATE_VOLT_24V;
		}
		else if(fRegulateVolt < MAX_NEED_REGULATE_MINUS_VOLT_24V)
		{
			fRegulateVolt = MAX_NEED_REGULATE_MINUS_VOLT_24V;
		}	
	}

	s_fReturnVolt += fRegulateVolt;

	//if(g_pGcData->RunInfo.Rt.fRatedVolt > 35.0)
	if(stVoltLevelState == 0)//48V
	{
		if(s_fReturnVolt > 5 * MAX_NEED_REGULATE_VOLT)
		{
			s_fReturnVolt = 5 * MAX_NEED_REGULATE_VOLT;
		}
		else if(s_fReturnVolt < 5 * MAX_NEED_REGULATE_MINUS_VOLT)
		{
			s_fReturnVolt = 5 * MAX_NEED_REGULATE_MINUS_VOLT;
		}
	}
	else
	{
		if(s_fReturnVolt > 5 * MAX_NEED_REGULATE_VOLT_24V)
		{
			s_fReturnVolt = 5 *  MAX_NEED_REGULATE_VOLT_24V;
		}
		else if(s_fReturnVolt < 5 * MAX_NEED_REGULATE_MINUS_VOLT_24V)
		{
			s_fReturnVolt = 5 * MAX_NEED_REGULATE_MINUS_VOLT_24V;
		}	
	}

	return s_fReturnVolt;
}


/*==========================================================================*
 * FUNCTION : JudgeRectLost
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-09-11 16:54
 *==========================================================================*/
//static void JudgeRectLost(void)
//{
//#define MIN_LOST_RECT_NUMBER		1
//#define	RECT_LOST_DELAY_TIMES		10
//	int			iQty = GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_QTY_SELF);
//	int			iCfgQty	= GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_CFG_RECT_NUM);
//	static int	s_iDelay = 0;
//
//	if(s_iDelay < RECT_LOST_DELAY_TIMES)
//	{
//		s_iDelay++;
//		return;
//	}
//
//	if(iQty > iCfgQty)
//	{
//		//Update RT_PUB_CFG_RECT_NUM
//		GC_SetDwordValue(TYPE_OTHER,
//					0,
//					RT_PUB_CFG_RECT_NUM, 
//					(DWORD)iQty);
//	}
//	
//	if(iCfgQty - iQty >= MIN_LOST_RECT_NUMBER)
//	{
//		//Raise alarm
//		GC_SetEnumValue(TYPE_OTHER, 
//						0,
//						RT_PUB_CFG_RECT_LOST,
//						GC_ALARM,
//						TRUE);
//	}
//	else
//	{
//		//Restore normal
//		GC_SetEnumValue(TYPE_OTHER, 
//						0,
//						RT_PUB_CFG_RECT_LOST,
//						GC_NORMAL,
//						TRUE);
//	}
//	return;
//	
//}

/*==========================================================================*
 * FUNCTION : GC_IsCurrentLimit
 * PURPOSE  : To judge if the rectifiers is in current limit state
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-06-12 09:41
 *==========================================================================*/
BOOL GC_IsCurrentLimit(void)
{
	int			i;
	
	for(i = 0; i < g_pGcData->RunInfo.Rt.Slave[RECT_TYPE_SELF].iQtyOfRect; i++)
	{

		if(GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT , i, RT_PRI_CURR_LMT_SET))
		{
			//printf("Is Current Limit!!!\n");
			return TRUE;
		}	
		
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : GC_IsPowerLmt
 * PURPOSE  : To judge if the rectifiers is in power limit state
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-06-12 09:41
 *==========================================================================*/
BOOL GC_IsPowerLmt(void)
{
	int			aiPowerLmtSid[] = {RT_PRI_POWER_LMT_RECT, 
									S1_PRI_POWER_LMT_RECT,
									S2_PRI_POWER_LMT_RECT,
									S3_PRI_POWER_LMT_RECT,};

	int			i, j;

	for(i = 0; i < MAX_RECT_TYPE_NUM; i++)
	{
		for(j = 0; j < g_pGcData->RunInfo.Rt.Slave[i].iQtyOfRect; j++)
		{
			if(GC_NORMAL != GC_GetEnumValue(TYPE_RECT_UNIT + i, j, aiPowerLmtSid[i]))
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : GC_IsUnderVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-06-22 16:43
 *==========================================================================*/
BOOL GC_IsUnderVolt(void)
{

	if((GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_UNDER_VOLT_ALM))
		|| (GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_UNDER_VOLT_ALM_24V)))
	{
		return TRUE;
	}

	return FALSE;
}
/*==========================================================================*
* FUNCTION : GC_IsCurrLimitUnderVolt
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2006-06-22 16:43
*==========================================================================*/
BOOL GC_IsCurrLimitUnderVolt(void)
{
	SIG_ENUM stVoltLevelState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_VOLT_LEVEL);

	/*if((GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_UNDER_VOLT_ALM))
		|| (GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_UNDER_VOLT_ALM_24V)))
	{
		return TRUE;
	}*/
	//if(g_pGcData->RunInfo.Rt.fRatedVolt < 35.0)
	if(stVoltLevelState)//24V
	{
		if( g_pGcData->RunInfo.fSysVolt <= 23.5)
		{
			return TRUE;
		}
	}
	else
	{
		if( g_pGcData->RunInfo.fSysVolt <= 47.0)
		{
			return TRUE;
		}
	}

	
	return FALSE;
}


/*==========================================================================*
 * FUNCTION : GC_IsOverVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-06-23 11:12
 *==========================================================================*/
BOOL GC_IsOverVolt(void)
{
	if((GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_OVER_VOLT_ALM))
		|| (GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_OVER_VOLT_ALM_24V))
		|| (GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_HIGH_VOLT_ALM))
		|| (GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_HIGH_VOLT_ALM_24V)))
	{
		return TRUE;
	}
	return FALSE;
}


#define	MIN_CURR_LMT_INTERCAL	30

int	GC_GetCurrLmtPeriod(int iIdx)
{
	UNUSED(iIdx);	
	return  MIN_CURR_LMT_INTERCAL;
}


//int GC_GetPreCurrLmtDelay(int iIdx)
//{
//	UNUSED(iIdx);
//	DWORD	dwInterval;
//	dwInterval = g_pGcData->RunInfo.Rt.Slave[RECT_TYPE_SELF].iQtyOfRect
//		* GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_SEQ_START_INTERVAL);
//	
//	return  dwInterval + DELAY_AFTER_PRE_CURR_LMT;
//}

/**************************************************************
~{T$o?=o?=o?=o?=o?=o?=D3o?=o?=o?=J1o?=o?=o?=_<o?=o?=o?=o?=o?~}?
~{T$o?=o?=o?=o?=o?=o?=o?=o?=o?=J1o?=o?~}?~{o?=o?=~} ~{o?=o?=D#o?=o?=o?=o?=o?~}?-1 ~{o?=o?=~}* ~{D#o?=o?=o?=o?=o?=o?=o?=o?=J1o?=o?=~}
+ WORKIN~{J1o?=o?=~} * 1.15 
1.15~{o?=o?=o?=o?=V5o?=o?=o?=o?=D#o?=o?=o?=o?=~} ~{o?=o?=o?~}?0%~{o?=o?=o?=o?=n#~} ~{o?=o?=o?=G8o?=~}0.05~{T$o?=o?=~}
~{N*o?=o?=S-o?=O2o?=o?=o?=o?=o?=o?=J1o?=o?=F%o?=o?=o?=o?=o?=b#~} ~{H!~}
~{T$o?=o?=o?=o?=o?=o?=o?=o?=o?=J1o?=o?~}? ~{o?=o?=~} DELAY_AFTER_PRE_CURR_LMT~{o?=o?=o?=o?=o?=o?=o?=V~}?
***************************************************************/
int GC_GetPreCurrLmtDelay(int iIdx)
{
	UNUSED(iIdx);

#define WALKIN_IS_ENABLE	1
#define WALKIN_COEF		(1.15)
#define INTERVAL_OFFSET		20

	DWORD		dwInterval = 0;
	SIG_ENUM	enumVal;
	int	iRectTemp = 0;
	iRectTemp = (g_pGcData->RunInfo.Rt.Slave[RECT_TYPE_SELF].iQtyOfRect);
	iRectTemp = MAX(iRectTemp, 0);		//~{D?o?=D2o?=R*o?=o?=o?=o?=o?=o?=o?~}?
	dwInterval = iRectTemp * GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_SEQ_START_INTERVAL);
	dwInterval += INTERVAL_OFFSET;

	enumVal = GC_GetEnumValue(TYPE_OTHER, 0,RT_PUB_WALKIN_ENB);
	if (WALKIN_IS_ENABLE == enumVal)
	{
		dwInterval += (GC_GetDwordValue(TYPE_OTHER, 0, RT_PUB_WALKIN_TIME) * WALKIN_COEF);
	}

	return MAX(DELAY_AFTER_PRE_CURR_LMT, dwInterval);
	//return  dwInterval + DELAY_AFTER_PRE_CURR_LMT;
}
/*==========================================================================*
 * FUNCTION : GC_PreCurrlmtForAcFail
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-08-17 16:17
 *==========================================================================*/
void GC_PreCurrlmtForAcFail(void)
{
	static	int		s_iCnt = 15;	

	BOOL	bFiammBattery = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_FIAMM_BATTERY);
	//DWORD	dwBMSNumber ;
	//BOOL bRTN = GC_GetDwordValueWithStateReturn(TYPE_OTHER, 0, GC_PUB_BMS_NUMBER, TRUE, &dwBMSNumber);//
	//
	//if(bRTN && dwBMSNumber > 0)
	//{		
	//	return;
	//}

#ifdef GC_SUPPORT_MPPT
	SIG_ENUM stState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_MPPT_RUNNING_MODE);
	if(stState)
	{
		if(g_pGcData->RunInfo.Mt.bNight)
		{
			//s_iCnt = 0;
			GC_PreCurrLmt_Mppt(g_pGcData->RunInfo.Mt.iQtyOfRect);
		}
		
	}
#endif
	if(g_pGcData->RunInfo.Rt.bAcFail)
	{
		if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
		{
			s_iCnt = 0;
			CurrentLimitThroughVolt(TRUE ,BT_PUB_NOM_VOLT, FALSE);
		}
		else
		{
			s_iCnt = 0;
			GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect, TRUE, FALSE);
		}
		
	}
	else
	{
		if(GC_IsNeedVoltAdjust() == TRUE )  ////ST_MAN_TEST,ST_PLAN_TEST,ST_AC_FAIL_TEST,
		{
			s_iCnt = 15;
		}
		else if(s_iCnt < 15)
		{
			GC_PreCurrLmt(g_pGcData->RunInfo.Rt.iQtyOfRect, TRUE, FALSE);
			s_iCnt++;
		}
	}
	
	return;
}


/*==========================================================================*
 * FUNCTION : GC_CurrlmtCtl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: float  fCtlValue : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-08-17 16:18
 *==========================================================================*/
void GC_CurrlmtCtl_Slave(float fCtlValue)
{
	
	int		i;
	static	int	iSendTimes = 0;
	static	time_t	s_tmLast = 0;
	time_t	s_tmCurrent = time(NULL);

	if(s_tmCurrent - s_tmLast >= 3 || s_tmCurrent - s_tmLast < 0)
	{
		iSendTimes = 0;
	}
	s_tmLast = s_tmCurrent;		

	if(!iSendTimes)
	{
		for(i = RECT_TYPE_SLAVE1; i < MAX_RECT_TYPE_NUM; i++)
		{
			if(GC_EQUIP_EXIST == GC_IsGCEquipExist(TYPE_OTHER, 0, RT_PUB_SLAVE1_EXIST + i - RECT_TYPE_SLAVE1,GC_EQUIP_INVALID))
			{
				GC_SetFloatValue(TYPE_OTHER, 
					0,
					RT_PUB_CURR_LMT_CTL_SELF + i,
					fCtlValue);
			}		
		}

		
	}
	iSendTimes++;
	if(iSendTimes >= 5)
	{
		iSendTimes = 0;
	}
	return;
}


/*==========================================================================*
 * FUNCTION : GC_CmpMasterSlaveCurr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Marco Yang                DATE: 2010-02-07 16:18
 *==========================================================================*/
int GC_CmpMasterSlaveCurr()
{ 
#define		GC_SLAVE_ADD_LMT		1
#define		GC_SLAVE_DECREASE_LMT		-1
#define		GC_SLAVE_EQUAL_LMT		0

	int				iRectLimit = GC_SLAVE_EQUAL_LMT;
	BOOL				bExceedLoadshare = FALSE;
	GC_RECT_INFO			RectifierInfo;
	BOOL				bExistSlaveRect = FALSE;
	float				fMaxCurr = 0.0, fMinCurr = 1000, fTempCurr = 0.0;
	float				fCmpRatio1 = 0.0, fCmpRatio2 = 0.0, fAverageCurr = 0.0;
	float				fMaxMasterCurr = 0.0, fMaxSlaveCurr = 0.0;

	List_GotoHead(g_pGcData->RunInfo.Rt.hListOnWorkRect);

	do
	{
		if(List_Get(g_pGcData->RunInfo.Rt.hListOnWorkRect, (void*)(&RectifierInfo)))
		{
			if(RectifierInfo.iRectEquipType == RECT_TYPE_SELF)
			{
				fTempCurr = GC_GetFloatValue(TYPE_RECT_UNIT , RectifierInfo.iRectNo, RT_PRI_OUT_CURR);

				if(fMaxMasterCurr < fTempCurr)
				{
					fMaxMasterCurr = fTempCurr;
				}
				
			}
			else
			{
				bExistSlaveRect  = TRUE;
				fTempCurr = GC_GetFloatValue(TYPE_RECT_UNIT + RectifierInfo.iRectEquipType, RectifierInfo.iRectNo, S1_PRI_OUT_CURR);
				
				if(fMaxSlaveCurr < fTempCurr)
				{
					fMaxSlaveCurr = fTempCurr;
				}
			}

			if(fMaxCurr < fTempCurr)
			{
				fMaxCurr = fTempCurr;
			}

			if(fMinCurr > fTempCurr)
			{
				fMinCurr = fTempCurr;
			}		
			
		}

	}while(List_GotoNext(g_pGcData->RunInfo.Rt.hListOnWorkRect));	

	if(bExistSlaveRect)
	{
		fAverageCurr = g_pGcData->RunInfo.Rt.fSumOfCurr/g_pGcData->RunInfo.Rt.iQtyOnWorkRect;
		fCmpRatio1 = abs(fMaxCurr - fAverageCurr)/g_pGcData->RunInfo.Rt.fRatedCurr;
		fCmpRatio2 = abs(fMinCurr - fAverageCurr)/g_pGcData->RunInfo.Rt.fRatedCurr;

		if(fCmpRatio1 > 0.04 || fCmpRatio2 > 0.04)
		{
			if(fMaxMasterCurr - fMaxSlaveCurr > 0.5)	//Master's rectifier output is big, add the slave's output current.
			{
				 
				return  GC_SLAVE_ADD_LMT;
			}
			else if(fMaxSlaveCurr -  fMaxMasterCurr> 0.5)
			{
				 
				return GC_SLAVE_DECREASE_LMT;
			}		
			
		}
	}	

	return GC_SLAVE_EQUAL_LMT;
}

/* Added by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
BOOL GC_IsDislPwrLmtMode()
{
	SIG_ENUM	enumState, enumDislDI;
	BOOL bDislMode = FALSE;

	enumState = GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_DISL_PWR_LMT_ENB);
	enumDislDI = GC_GetEnumValue(TYPE_OTHER, 0, RT_PUB_DISL_DI_INPUT);

	if((GC_ENABLED == enumState) && (DISL_PWR_LMT_INPUT_NONE != enumDislDI)
		&& (SYS_DI_HIGH == GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_DI1 + enumDislDI - 1)))
	{
		bDislMode = TRUE;
	}

	//	TRACE("****GC**** It %s in Diesel Power Limit Mode.\n",(bDislMode?"IS":"IS NOT"));	//FXS debug
	return bDislMode;
}
/* Ended by Victor Fan, 2007-08-27, for CR# 0189-07-ACU */
BOOL GC_HybridClearAlarm()
{
	//Clear the hybrid alarm
	GC_SetEnumValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_DG1_RUNNING,
		GC_NORMAL,
		TRUE);
	GC_SetEnumValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_DG2_RUNNING,
		GC_NORMAL,
		TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_RUN_ON_OVERTEMP,
		GC_NORMAL,
		TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_HIGH_LOAD_ALARM,
		GC_NORMAL,
		TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_DG1_FAILURE,
		GC_NORMAL,
		TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_DG2_FAILURE,
		GC_NORMAL,
		TRUE);

	GC_SetEnumValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_GRID_ON,
		GC_NORMAL,
		TRUE);

	//Clear DO statuse
	GC_HybridAlarmOutput(TRUE);

	//Clear Boost charge
	SIG_ENUM enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	if(ST_AUTO_BC == enumState 
		|| ST_MAN_BC == enumState  
		|| ST_CYCLIC_BC == enumState )
	{
		GC_BoostChargeEnd(ST_FLOAT,
			FALSE,
			"Because of disable Hybrid function, turn to FC.\n");
	}
}

/*Added by YangGuoxin,2011-1-18, for HybridSystem*/

SIG_ENUM GC_HybridGetSelectedDI()
{
	SIG_ENUM enumSelectedDI = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_SELECTED_DI);	
	SIG_ENUM enumGridState = GC_HyBridGetDIStatus(IB2_DI1_FIRST + enumSelectedDI);//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_FIRST + enumSelectedDI);//Get Grid state
//changed by SongXu to DI1-8 moving from system to IB. 20160810
	SIG_ENUM enumSelectedDIState;

	if(enumSelectedDI < IB2_DI8_EIGHTH)
	{
		enumSelectedDIState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_DI_STATE_1 + enumSelectedDI);
	}
	else
	{
		enumSelectedDIState = GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_SYS_DI_STATE_9 + enumSelectedDI - 8);
	}
	return (enumGridState == enumSelectedDIState) ? TRUE : FALSE;
}
static BOOL GC_HybridState()
{
////#ifdef HYBRID_TEST_MODE
//	g_pGcData->RunInfo.Bt.bDischarge = TRUE;
////#endif
	static		int iStartTimes = 0;
	SIG_ENUM	enumState, enumGridState;
	static int		iDisableTimes = 0;
	
	
	enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
	enumGridState = GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get Grid state
	static	SIG_ENUM	enumLastState = GC_HYBRID_DISABLED;

	//printf("enumState = %d, enumGridState = %d \n", enumState, enumGridState);
	if(iStartTimes < 50)
	{
		iStartTimes++;
		return;
	}

	if(STATE_AUTO != g_pGcData->RunInfo.enumAutoMan)
	{
		return;
	}

#ifdef GC_SUPPORT_RELAY_TEST	
	 if(GC_DISABLED != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_RELAY_TEST))
	 {
		 //need clear timer
		 if(SEC_TMR_SUSPENDED 
			 != GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE)) //?~{o?=o?=o?=o?=o?=J1o?=o?=o?=o?~}?n~{o?=o?=o?=o?=C4o?=l#?~}
		 {
			 GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE);			
		 }
		return;
	 }
#endif
	if(enumState == GC_HYBRID_DISABLED)

	{
		//need clear timer
		if(SEC_TMR_SUSPENDED 
			!= GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE)) //?~{o?=o?=o?=o?=o?=J1o?=o?=o?=o?~}?n~{o?=o?=o?=o?=C4o?=l#?~}
		{
			GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE);			
		}
		//Need clear other status.
		//1.Diesel
		//2.Boost charge / Discharge.Clear alarm
		if(enumLastState == GC_HYBRID_ENABLED)
		{
			GC_HybridCloseDiesel();
			GC_HybridClearAlarm();
		}

		//Because DO may control failure, so just clear 2 times.
		if(iDisableTimes >= 2)
		{
			enumLastState = GC_HYBRID_DISABLED;
		}
		else
		{
			iDisableTimes++;
		}
		return TRUE;
	}
	else if(enumLastState == GC_HYBRID_DISABLED)
	{
		//printf("enumLastState == GC_HYBRID_DISABLED\n");
		//GC_HybridOpenDiesel(FALSE);
		/*GC_SetSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION,
							0,
							GC_GetHybridEqualismDuration,
							0);*/
		enumLastState = GC_HYBRID_ENABLED;	
		iDisableTimes = 0;
	}
	GC_HybridRefreshHybridAlarm();
	GC_HybridAlarmOutput(FALSE);

	if(enumGridState == GC_HYBRID_GRID_ON)
	{
		
		GC_HybridGridOn();
		return TRUE;
	}
	else if(enumState == GC_HYBRID_CAPACITY_MODE)// capacity mode
	{
		
	
		GC_HybridCapacityMode();
		return TRUE;
	}
	else if(enumState == GC_HYBRID_FIXED_MODE)//fixed mode
	{
		//printf("Into GC_HYBRID_FIXED_MODE\n");

		GC_HybridFixedtimeMode();
		return TRUE;
	}
	else
	{
		return FALSE;
	}	
}
static void GC_HybridInitState()
{
	g_pGcData->RunInfo.HyBrid.iLastGridOnState = GC_HYBRID_GRID_OFF;
	g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG_SELECTED_NOTHING;
	g_pGcData->RunInfo.HyBrid.bDieselFailure[0] = FALSE;
	g_pGcData->RunInfo.HyBrid.bDieselFailure[1] = FALSE;
	g_pGcData->RunInfo.HyBrid.iLastRunDG = GC_DISABLED;

	SIG_ENUM	enumState, enumBattState;
	enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
	enumBattState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	if(enumState == GC_HYBRID_CAPACITY_MODE && enumBattState != ST_AUTO_BC )
	{
		//Close diesel.
		GC_HybridCloseDiesel();
	}


}
static void GC_HybridCapacityMode()
{
	SIG_ENUM	enumGridState, enumState;


	enumGridState =  GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get Grid state
	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	if(enumGridState == GC_HYBRID_GRID_OFF 
		&& g_pGcData->RunInfo.HyBrid.iLastGridOnState != GC_HYBRID_GRID_OFF)//Before state is on ,now first time is off, start discharge
	{
		
		g_pGcData->RunInfo.HyBrid.iLastGridOnState = GC_HYBRID_GRID_OFF;
		//GC_HybridOpenDiesel(FALSE);	
		GC_HybridCloseDiesel();

		return;
	}

	if(ST_CYCLIC_BC == enumState 
		|| SEC_TMR_SUSPENDED 
			!= GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION))//in Equalism state
	{
		
		GC_HybridEqualisCharge();
		return;
	}
	else if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE)
		&& GC_IsHybridEqualismTime())//time in to start equalism charge
	{
		//Start equalism charge
		GC_HybridOpenDiesel(FALSE);
		if(ST_CYCLIC_BC != enumState )
		{
			GC_BoostChargeStart(ST_CYCLIC_BC,
				"Time to turn to CYCLE BC.\n");
		}
		
		GC_SetSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION,
							0,
							GC_GetHybridEqualismDuration,
							0);

		return;
	}
	else 
	{
		
		if(GC_HybridIsOverTemp() && (GC_ENABLED ==GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_ON_OVERTEMP_ENABLE)))//Over temperature
		{
			//printf("GC_HybridIsOverTemp HYBRID_PUB_RUN_ON_OVERTEMP_ENABLE GC_HybridIsRunOverTemp = %d\n", GC_HybridIsRunOverTemp());
			if(!GC_HybridIsRunOverTemp())//Diesel is not on or diesel is not in overtemperature mode
			{
				//Open diesel, Set state
				GC_HybridOpenDiesel(TRUE);
				g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_RUNNING_OVERTEMP;
				if(SEC_TMR_SUSPENDED 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION))
				{
					GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION,
							0,
							GC_GetHybridDieselROOTDuration,
							0);
				}

				return;
			}			
		}
		//~{o?=o?=o?=o?=o?=o?=o?=N~} ~{o?=o?=o?=o?=o?=M;o?=~},~{o?=o?=o?=M;o?=J'o?=o?=~},~{o?=o?=o?=Z>o?=o?=o?=~},~{o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=~}.~{W*N*o?=o?=o?=o?=~}.
		//~{o?=o?=o?=o?=o?=o?=o?=N~} Both,2~{o?=o?=o?=M;o?=o?=o?=J'o?=o?=~},~{o?=R4o?=o?=Z>o?=o?=o?=~},~{o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=~}.
		SIG_ENUM    enumSelectedDG = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_DG);
		if((enumSelectedDG == HYBRID_PUB_DG1_SELECTED && GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG1_FAILURE))
			|| (enumSelectedDG == HYBRID_PUB_DG2_SELECTED && GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
						0,
						HYBRID_PUB_DG2_FAILURE))
			|| (enumSelectedDG == HYBRID_PUB_DG2DG1_SELECTED && GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
										0,
										HYBRID_PUB_DG1_FAILURE) 
			&&	GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
						0,
						HYBRID_PUB_DG2_FAILURE)))
		{
			if(ST_AUTO_BC == enumState)
			{
				GC_BoostChargeEnd(ST_FLOAT,
					FALSE,
					"End Boost charge, turn to FC.\n");
			}
			return;
		}

		//If the battery is over temperature, then stop boost charge.
		if(GC_IsVeryHiBTRM())
		{
			if(ST_AUTO_BC == enumState)
			{
				GC_BoostChargeEnd(ST_FLOAT,
					FALSE,
					"Over temperature,end Boost charge, turn to FC.\n");
			}
			return;
		}

		if(GC_HybridIsUnderVol())//Under voltage 
		{

			GC_HybridOpenDiesel(FALSE);
			if(ST_AUTO_BC != enumState )
			{
				GC_BoostChargeStart(ST_AUTO_BC,
					"Under voltage, turn to AUTO BC.\n");
			}
			
			return;
		}

		if(GC_HybridDischCapCmp())//Fullfill capacity condition to stop 
		{

			//Open diesel
			//Go to boost charge	
			GC_HybridOpenDiesel(FALSE);
			if(ST_AUTO_BC != enumState )
			{
				GC_BoostChargeStart(ST_AUTO_BC,
					"Because of capacity is low, turn to AUTO BC.\n");
			}
			
			return;
		}

		if(GC_HybridIsRunOverTemp())//Generator is run on over temperature
		{
			if(SEC_TMR_TIME_OUT 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION))
			{
				//Close diesel,stop boost charge
				GC_HybridCloseDiesel();
				//Clear the alarm of Over temp				
				g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG_SELECTED_NOTHING;
				if(ST_AUTO_BC == enumState)
				{
					GC_BoostChargeEnd(ST_FLOAT,
						FALSE,
						"Because of diesel off, turn to FC.\n");
				}
				GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION);
				return;
				
			}
			else
			{
				//Continue run on over temperature
				return;
			}
		}

		//Diesel is on but not in boost charge or charge current is zero, then go to boost charge
		if(GC_HybridChargeCapCmp() && (ST_AUTO_BC != enumState))
		{
			//Close diesel, clear the capacity to 100%
			GC_HybridCloseDiesel();
			//GC_HybridResetCapacity();
			return;		
		}
		
		
		return;
	}

	return;
}


static BOOL GC_HybridFixedtimeMode()
{
	SIG_ENUM	enumGridState,enumState;
	enumGridState =  GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get Grid state
	enumState =  GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	if(enumGridState == GC_HYBRID_GRID_OFF 
		&& g_pGcData->RunInfo.HyBrid.iLastGridOnState != GC_HYBRID_GRID_OFF)//Before state is on ,now first time is off, start discharge
	{
		g_pGcData->RunInfo.HyBrid.iLastGridOnState = GC_HYBRID_GRID_OFF;
		//GC_HybridOpenDiesel(FALSE);	
		GC_HybridCloseDiesel();

		//printf("GC_HYBRID_GRID_OFFg_pGcData->RunInfo.HyBrid.iLastGridOnState = %d\n", g_pGcData->RunInfo.HyBrid.iLastGridOnState);
		if(SEC_TMR_SUSPENDED 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_FIXEDDAILY_DISCHARGE)) //?~{o?=o?=o?=o?=o?=J1o?=o?=o?=o?~}?n~{o?=o?=o?=o?=C4o?=l#?~}
		{
			GC_SetSecTimer(SEC_TMR_ID_HYBRID_FIXEDDAILY_DISCHARGE,
				0,
				GC_GetHybridFixedDischarge,
				0);
		}

		return TRUE;
	}	

	if(ST_CYCLIC_BC == enumState)//in Equalism state
	{

		GC_HybridEqualisCharge();
		return TRUE;
	}
	else if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE) 
			&& GC_IsHybridEqualismTime())//time in to start equalism charge
	{

		//Start equalism charge
		GC_HybridOpenDiesel(FALSE);
		if(ST_CYCLIC_BC != enumState )
		{
			GC_BoostChargeStart(ST_CYCLIC_BC,
					"Time to turn to CYCLE BC.\n");
		}
		GC_SetSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION,
							0,
							GC_GetHybridEqualismDuration,
							0);
		GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE);
		return TRUE;
	}
	else 
	{
		
		if(GC_HybridIsOverTemp() && (GC_ENABLED ==GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_ON_OVERTEMP_ENABLE)))//Over temperature
		{
			if(!GC_HybridIsRunOverTemp())//Diesel is not on or diesel is not in overtemperature mode
			{
				//Open diesel, Set state

				GC_HybridOpenDiesel(TRUE);
				g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_RUNNING_OVERTEMP;
				if(SEC_TMR_SUSPENDED 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION))
				{
					GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION,
							0,
							GC_GetHybridDieselROOTDuration,
							0);
				}

				return;
			}			
		}
		//~{o?=o?=o?=o?=o?=o?=o?=N~} ~{o?=o?=o?=o?=o?=M;o?=~},~{o?=o?=o?=M;o?=J'o?=o?=~},~{o?=o?=o?=Z>o?=o?=o?=~},~{o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=~}.~{W*N*o?=o?=o?=o?=~}.
		//~{o?=o?=o?=o?=o?=o?=o?=N~} Both,2~{o?=o?=o?=M;o?=o?=o?=J'o?=o?=~},~{o?=R4o?=o?=Z>o?=o?=o?=~},~{o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=~}.
		SIG_ENUM    enumSelectedDG = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_DG);
		if((enumSelectedDG == HYBRID_PUB_DG1_SELECTED && GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG1_FAILURE))
			|| (enumSelectedDG == HYBRID_PUB_DG2_SELECTED && GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG2_FAILURE))
			|| (enumSelectedDG == HYBRID_PUB_DG2DG1_SELECTED && GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG1_FAILURE) 
			&&	GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG2_FAILURE)))
		{
			if(ST_AUTO_BC == enumState)
			{
				GC_BoostChargeEnd(ST_FLOAT,
					FALSE,
					"End Boost charge, turn to FC.\n");
			}
			return;
		}
		//If the battery is over temperature, then stop boost charge.
		if(GC_IsVeryHiBTRM())
		{
			if(ST_AUTO_BC == enumState)
			{
				GC_BoostChargeEnd(ST_FLOAT,
					FALSE,
					"Over temperature,end Boost charge, turn to FC.\n");
			}
			return;
		}
		if(GC_HybridIsUnderVol())//Under voltage 
		{
			//printf("GC_HybridIsUnderVol = %d\n", GC_HybridIsUnderVol());
			//Open diesel
			GC_HybridOpenDiesel(FALSE);
			//Go to boost charge	
			if(ST_AUTO_BC != enumState )
			{
				GC_BoostChargeStart(ST_AUTO_BC,
						"Under voltage, turn to AUTO BC.\n");
			}
			return TRUE;
		}

		if(GC_HybridIsRunOverTemp())//Generator is run on over temperature
		{
			if(SEC_TMR_TIME_OUT 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION))
			{
				//close diesel,stop boost charge
				GC_HybridCloseDiesel();
				g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG_SELECTED_NOTHING;

				GC_BoostChargeEnd(ST_FLOAT,
					FALSE,
					"Because of diesel off, turn to FC.\n");
				GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION);
				return TRUE;
				
			}
			else
			{
				//Continue run on over temperature
				return TRUE;
			}
		}
	
		if(GC_HybridDischCapCmp() && !(GC_HybridIsRunOverTemp()))//Fullfill capacity condition to stop 
		{
			//Open diesel
			GC_HybridOpenDiesel(FALSE);
			//Go to boost charge	
			if(ST_AUTO_BC != enumState )
			{
				GC_BoostChargeStart(ST_AUTO_BC,
						"Because of capacity is low, turn to AUTO BC.\n");
			}
			return TRUE;
		}
		
		if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_FIXEDDAILY_DISCHARGE))//Discharge time out
		{
			GC_HybridOpenDiesel(FALSE);
			//Go to boost charge	
			if(ST_AUTO_BC != enumState )
			{
				GC_BoostChargeStart(ST_AUTO_BC,
					"Because of capacity is low, turn to AUTO BC.\n");
			}
			GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_FIXEDDAILY_DISCHARGE);

			if(GC_HybridCmpHighLoadAlarm())//Fullfill the high load alarm
			{
				//Raise High load alarm
				GC_SetEnumValue(TYPE_OTHER, 
						0,
						HYBRID_PUB_HIGH_LOAD_ALARM,
						GC_ALARM,
						TRUE);
			}
			return TRUE;
		}	
	
		//printf("Into else\n");
		if(GC_IsHybridStartDischargeTime())//Time == period start timer, start discharge
		{
			//Clear High load alarm
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_HIGH_LOAD_ALARM,
				GC_NORMAL,
				TRUE);

			//Close diesel, start discharge.
			GC_HybridCloseDiesel();
			if(ST_AUTO_BC == enumState )
			{
				GC_BoostChargeEnd(ST_FLOAT,
					FALSE,
					"Because of diesel off, turn to FC.\n");
			}

			GC_SetSecTimer(SEC_TMR_ID_HYBRID_FIXEDDAILY_DISCHARGE,
				0,
				GC_GetHybridFixedDischarge,
				0);

			return TRUE;
		}

		
	}

	return TRUE;
}
static void GC_HybridEqualisCharge()
{
	SIG_ENUM	enumState;
	enumState =  GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);
	static int	s_iDelayTimes = 0 ;

	if(SEC_TMR_TIME_OUT == GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION))//time out
	{
		//Close diesel
		GC_HybridCloseDiesel();
		GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION);
		GC_SetSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE,
				0,
				GC_GetIntervalCycBcInterval,
				0);
		if(ST_CYCLIC_BC == enumState)
		{
			GC_BoostChargeEnd(ST_FLOAT,
					FALSE,
					"End Boost charge, turn to FC.\n");	
		}
		s_iDelayTimes = 0;
		return;
	}
	if(ST_CYCLIC_BC != enumState && s_iDelayTimes > 3)
	{
		GC_HybridCloseDiesel();	
		s_iDelayTimes = 0;
		GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE_DURATION);
		GC_SetSecTimer(SEC_TMR_ID_HYBRID_EQUALISM_CYCLE,
				0,
				GC_GetIntervalCycBcInterval,
				0);
		return;
	}
	if(GC_HybridIsRunOverTemp())//Generator is run on over temperature
	{
		
		if(SEC_TMR_TIME_OUT 
				== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION))
		{
			//Clear the alarm of Over temp		
			//Close diesel,stop boost charge
			g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG_SELECTED_NOTHING;
			GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_RUN_ON_OVERTEMP,
					GC_NORMAL,
					TRUE);
			GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION);
			return;
			
		}
		else
		{
			if(SEC_TMR_SUSPENDED 
				== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL_ON_OVERTEMP_DURATION))
			{
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_RUN_ON_OVERTEMP,
					GC_NORMAL,
					TRUE);
			}
			//Continue run on over temperature
			return;
		}
	}
	s_iDelayTimes++ ;
	return ;
}


static BOOL GC_HybridIsUnderVol()
{
	if((GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_VERY_UNDER_VOLT_ALM_24V))
		|| (GC_NORMAL != GC_GetEnumValue(TYPE_OTHER, 0, GC_PUB_VERY_UNDER_VOLT_ALM)))

	{
		return TRUE;
	}

	//if(g_pGcData->RunInfo.Rt.bAcFail)

	//{
	//	return TRUE;
	//}

	return FALSE;
}

static BOOL GC_HybridIsOverTemp()
{
	/*int		i;

	for(i = 0; i < MAX_NUM_TEMPERATURE - MAX_NUM_LargeDU_TEMP; i++)
	{
		SIG_ENUM	enumTempUsage = GC_GetEnumValue(TYPE_OTHER, 
								0,
								GC_PUB_TEMP1_USAGE + i);
		if((enumTempUsage != TEMP_USAGE_DISABLED)
			&& ((GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
								0,
								GC_PUB_HI_TEMP1_ALM + i)) 
			|| (GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
								0,
								GC_PUB_VERY_HI_TEMP1_ALM + i))))
		{
			return TRUE;
		}
	}

	return FALSE;*/

	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_SYSTEM_ENVTEMP_HIGH_ALARM)
	 ||GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_BATTGROUP_BATTTEMP_VERYHIGH_ALARM))
	{
		return TRUE;
	}

	return FALSE;
}

static BOOL GC_HybridIsRunOverTemp()
{
	//printf("(GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RUNONOVERTEMP) == GC_HYBRID_RELAY_STATUS_OPEN) ? 1: 0 = %d\n", (GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RUNONOVERTEMP) == GC_HYBRID_RELAY_STATUS_OPEN) ? 0 : 1);
	if(GC_ENABLED !=GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_ON_OVERTEMP_ENABLE))
	{
		return FALSE;
	}
	if(GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_ON_OVERTEMP))
	{
		return TRUE;
	}
	return g_pGcData->RunInfo.HyBrid.iCurrentRunningDG == HYBRID_PUB_RUNNING_OVERTEMP ? TRUE : FALSE ;
	//return (GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RUNONOVERTEMP) == GC_HYBRID_RELAY_STATUS_OPEN) ? 1: 0;
}

static void GC_HybridResetCapacity()
{
	int		i;
	BOOL		bSave = FALSE;
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{		
		if(!(FLOAT_EQUAL(g_pGcData->RunInfo.Bt.afBattCap[i],
						g_pGcData->RunInfo.Bt.afBattRatedCap[i])))
		{
			//added by Jimmy for Li Batt CR
			if(GC_Is_ABatt_Li_Type(i))
			{
				//~{o?=o?=o?=o?=o?=o?=o?=o?=o?~}?ridge Card~{o?=O4o?=o?=o?=o?=o?=o?=o?=o?=o?=o2;o?=o?=o?=o?~}?
			}
			else
			{
				g_pGcData->RunInfo.Bt.afBattCap[i] = g_pGcData->RunInfo.Bt.afBattRatedCap[i];
				GC_SetFloatValue(TYPE_BATT_UNIT,
					i,
					BT_PRI_CAP,
					g_pGcData->RunInfo.Bt.afBattRatedCap[i]);
			}

			bSave = TRUE;
		}		
	}

	if(bSave)
	{
		GC_SaveData();
	}

	return;
}
static BOOL GC_HybridGetRelay1Alarm()
{
	//Diesel alarm
	if(GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_DG1_FAILURE)
		||GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_DG2_FAILURE))
	{
		return TRUE;
	}
	return FALSE;
}
static BOOL GC_HybridGetRelay2Alarm()
{
	//Battery alarm
	if(GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 
		0,
		HYBRID_PUB_HIGH_LOAD_ALARM)
		||GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
			0,
			GC_PUB_BATT_TEMP_FAULT)
		||GetBattHiBTRMAlm()
		||GC_GetBattFuseAlm()
		||GC_IsUnderVolt()
		||GC_IsOverVolt()
		||GC_IsLVDDisconnect())
	{
		return TRUE;
	}
	return FALSE;
}
static BOOL GC_HybridGetRelay3Alarm()
{
	//Rectifier alarm
	if(IsMultiRectFail()
		|| IsRectFail())
	{
		return TRUE;
	}
	return FALSE;
}
static BOOL GC_HybridGetRelay4Alarm()
{
	//System alarm
	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_AMB_TEMP_FAULT)
		||GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_AMB_TEMP_HIGH))
	{
		return TRUE;
	}
	return FALSE;
}
static void GC_HybridAlarmOutput(IN BOOL bClear)
{
	SIG_ENUM	enumFailsafe;
	enumFailsafe = GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_FAILSAFE);

	if(!bClear && GC_HybridGetRelay1Alarm())//Mains fail
	{
		
		if(enumFailsafe == GC_NORMAL_OPEN)
		{
			if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_GENERATOR_ALARM))
			{
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_GENERATOR_ALARM, GC_HYBRID_RELAY_STATUS_CLOSE);
			}
		}
		else
		{
			if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_GENERATOR_ALARM))
			{
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_GENERATOR_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
			}
		}

	}
	else
	{
		if(bClear)
		{
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_GENERATOR_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
		}
		else
		{
			if(enumFailsafe == GC_NORMAL_OPEN)
			{
				if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_GENERATOR_ALARM))
				{
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_GENERATOR_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
				}
			}
			else
			{
				if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_GENERATOR_ALARM))
				{
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_GENERATOR_ALARM, GC_HYBRID_RELAY_STATUS_CLOSE);
				}
			}
		}
	}

	if(!bClear && GC_HybridGetRelay2Alarm())//Battery alarms
	{
		
		if(enumFailsafe == GC_NORMAL_OPEN)
		{
			if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_BATTERY_ALARM))
			{
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_BATTERY_ALARM, GC_HYBRID_RELAY_STATUS_CLOSE);
			}
		}
		else
		{
			if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_BATTERY_ALARM))
			{
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_BATTERY_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
			}
		}

	}
	else
	{
		if(bClear)
		{
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_BATTERY_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
		}
		else
		{
			if(enumFailsafe == GC_NORMAL_OPEN)
			{
				if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_BATTERY_ALARM))
				{
					//Execute
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_BATTERY_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
				}
			}
			else
			{
				if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_BATTERY_ALARM))
				{
					//Execute
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_BATTERY_ALARM, GC_HYBRID_RELAY_STATUS_CLOSE);
				}
			}
		}
	}


	if(!bClear && GC_HybridGetRelay3Alarm())//Rectifier alarms
	{	
		
		if(enumFailsafe == GC_NORMAL_OPEN)
		{
			if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RECTIFIER_ALARM))
			{
				//Execute
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RECTIFIER_ALARM, GC_HYBRID_RELAY_STATUS_CLOSE);
			}
		}
		else
		{
			if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RECTIFIER_ALARM))
			{
				//Execute
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RECTIFIER_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
			}
		}

	}
	else
	{
		if(bClear)
		{
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RECTIFIER_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
		}
		else
		{	
			if(enumFailsafe == GC_NORMAL_OPEN)
			{
				if(GC_HYBRID_RELAY_STATUS_OPEN  != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RECTIFIER_ALARM))
				{
					//Execute
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RECTIFIER_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
				}
			}
			else
			{
				if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RECTIFIER_ALARM))
				{
					//Execute
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RECTIFIER_ALARM, GC_HYBRID_RELAY_STATUS_CLOSE);
				}
			}
		}
	}

	if(!bClear && GC_HybridGetRelay4Alarm())//System alarms
	{	
		
		if(enumFailsafe == GC_NORMAL_OPEN)
		{
			if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_SYSTEM_ALARM))
			{
				//Execute
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_SYSTEM_ALARM, GC_HYBRID_RELAY_STATUS_CLOSE);
			}
		}
		else
		{
			if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_SYSTEM_ALARM))
			{
				//Execute
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_SYSTEM_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
			}
		}

	}
	else
	{
		if(bClear)
		{
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_SYSTEM_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
		}
		else
		{
			if(enumFailsafe == GC_NORMAL_OPEN)
			{
				if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_SYSTEM_ALARM))
				{
					//Execute
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_SYSTEM_ALARM, GC_HYBRID_RELAY_STATUS_OPEN);
				}
			}
			else
			{
				if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_SYSTEM_ALARM))
				{
					//Execute
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_SYSTEM_ALARM, GC_HYBRID_RELAY_STATUS_CLOSE);
				}
			}
		}
	}

	//relay 5, relay 6, relay 7, relay 8 will cotrol in the open diesel function.
	return;

}

static void GC_HybridGridOn()
{		
	SIG_ENUM	enumState, enumGridState;
	enumGridState =  GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get Grid state
	enumState = GC_GetEnumValue(TYPE_OTHER, 0, BT_PUB_BM_STATE);

	if(enumGridState == GC_HYBRID_GRID_OFF)
	{
		return;
	}
	else//Grid is on	
	{
		GC_HybridCloseDiesel();


		if(g_pGcData->RunInfo.HyBrid.iLastGridOnState != GC_HYBRID_GRID_ON)//Before state is off, now first time is on, start charge
		{		
			if(enumState != ST_AUTO_BC)
			{
				//Go to boost charge			
				GC_BoostChargeStart(ST_AUTO_BC,
						"Grid is on, turn to AUTO BC.\n");
			}
			g_pGcData->RunInfo.HyBrid.iLastGridOnState = GC_HYBRID_GRID_ON;
		}

		//Suspend the timer
		if(SEC_TMR_TIME_IN 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE))
		{
			GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_CAPACITY_DISHARGE);
		}
		
	}
	return;
}

static void GC_HybridOpenDiesel(IN BOOL bRunOverTemp)
{
	SIG_ENUM	enumSelectedDG;
	enumSelectedDG = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_DG);//Get which DG will be selected



	//Clear GC RUNNING ALARM
	//Check the relay status, if status is closed, do nothing, else close it
	//printf("GC_HybridOpenDiesel 1 IB2_ALARM_RELAY_DG1 = %d, IB2_ALARM_RELAY_DG2 = %d g_pGcData->RunInfo.HyBrid.iLastCloseDG = %d\n", GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1), GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2), g_pGcData->RunInfo.HyBrid.iLastCloseDG);
	SIG_ENUM	enumFailsafe;
	enumFailsafe = GC_GetEnumValue(TYPE_OTHER, 
					0,
					GC_PUB_FAILSAFE);

	//printf("enumFailsafe %d\n", enumFailsafe);
	if(enumFailsafe == GC_NORMAL_OPEN)
	{
		//Get diesel state
		if(bRunOverTemp)//ENABLE
		{
			//Open diesel, and set stat is run over temp, raise the run on over temp alarm		
			if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RUNONOVERTEMP))
			{
				//Execute
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RUNONOVERTEMP, GC_HYBRID_RELAY_STATUS_CLOSE);
						
			}
			//Raise alarm
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_RUN_ON_OVERTEMP,
				GC_ALARM,
				TRUE);
		}

		if( enumSelectedDG == HYBRID_PUB_DG1_SELECTED)//DG1
		{
			if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
			{
				if(SEC_TMR_SUSPENDED 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL))
				{
					GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL,
						0,
						GC_GetHybridDieselFailureInterval,
						0);
				}
				
				//Execute
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1, GC_HYBRID_RELAY_STATUS_CLOSE);
			}
			g_pGcData->RunInfo.HyBrid.iLastRunDG = IB2_ALARM_RELAY_DG1;
			g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG1_SELECTED;
			//Raise DG1 alarm
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG1_RUNNING,
				GC_ALARM,
				TRUE);
			
			
		}
		else if(enumSelectedDG == HYBRID_PUB_DG2_SELECTED)//DG2
		{

			if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
			{
				
				//Execute
				if(SEC_TMR_SUSPENDED 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL))
				{
					GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL,
						0,
						GC_GetHybridDieselFailureInterval,
						0);
				}
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_CLOSE);
			}
			g_pGcData->RunInfo.HyBrid.iLastRunDG = IB2_ALARM_RELAY_DG2;
			g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG2_SELECTED;
			//Raise DG2 alarm
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG2_RUNNING,
				GC_ALARM,
				TRUE);

			
		}
		else//Both
		{
			if(g_pGcData->RunInfo.HyBrid.bDieselFailure[0] && g_pGcData->RunInfo.HyBrid.bDieselFailure[1])
			{
				g_pGcData->RunInfo.HyBrid.iLastCloseDG = GC_DISABLED;
			}
			else if(g_pGcData->RunInfo.HyBrid.bDieselFailure[0])
			{
				g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG1;
			}
			else if(g_pGcData->RunInfo.HyBrid.bDieselFailure[1])
			{
				g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG2;
			}
			else
			{
				if(g_pGcData->RunInfo.HyBrid.iLastCloseDG == GC_DISABLED)
				{
					g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG1;
				}
			}
			//printf("Into both status g_pGcData->RunInfo.HyBrid.iLastCloseDG = %d\n", g_pGcData->RunInfo.HyBrid.iLastCloseDG);
			//Check the relay status, if status is closed, do nothing, else close it
			if(g_pGcData->RunInfo.HyBrid.iLastCloseDG == IB2_ALARM_RELAY_DG1)
			{
				if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
				{
					
					//Execute
					if(SEC_TMR_SUSPENDED 
						== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL))
					{
						GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL,
							0,
							GC_GetHybridDieselFailureInterval,
							0);
					}
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_CLOSE);				

				}
				g_pGcData->RunInfo.HyBrid.iLastRunDG = IB2_ALARM_RELAY_DG2;
				g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG2_SELECTED;
				//Raise DG1 alarm
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG2_RUNNING,
					GC_ALARM,
					TRUE);
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG1_RUNNING,
					GC_NORMAL,
					TRUE);
				
			}
			else if(g_pGcData->RunInfo.HyBrid.iLastCloseDG == IB2_ALARM_RELAY_DG2)
			{
				if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
				{
					
					//Execute
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1, GC_HYBRID_RELAY_STATUS_CLOSE);
					if(SEC_TMR_SUSPENDED 
						== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL))
					{
						GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL,
							0,
							GC_GetHybridDieselFailureInterval,
							0);
					}				

				}
				g_pGcData->RunInfo.HyBrid.iLastRunDG = IB2_ALARM_RELAY_DG1;
				g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG1_SELECTED;
				//Raise DG2 alarm
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG1_RUNNING,
					GC_ALARM,
					TRUE);
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG2_RUNNING,
					GC_NORMAL,
					TRUE);
				
			}
		}
	}
	else
	{
		//Get diesel state
		if(bRunOverTemp)//ENABLE
		{
			//Open diesel, and set stat is run over temp, raise the run on over temp alarm		
			if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RUNONOVERTEMP))
			{
				//Execute
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RUNONOVERTEMP, GC_HYBRID_RELAY_STATUS_OPEN);
						
			}
			//Raise alarm
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_RUN_ON_OVERTEMP,
					GC_ALARM,
					TRUE);
		}

		if( enumSelectedDG == HYBRID_PUB_DG1_SELECTED)//DG1
		{
			if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
			{
				//Execute
				if(SEC_TMR_SUSPENDED 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL))
				{
					GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL,
						0,
						GC_GetHybridDieselFailureInterval,
						0);
				}
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1, GC_HYBRID_RELAY_STATUS_OPEN);		

			}
			g_pGcData->RunInfo.HyBrid.iLastRunDG = IB2_ALARM_RELAY_DG1;
			g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG1_SELECTED;
			//Raise DG1 alarm
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG1_RUNNING,
				GC_ALARM,
				TRUE);
			
				
		}
		else if(enumSelectedDG == HYBRID_PUB_DG2_SELECTED)//DG2
		{

			if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
			{
				
				//Execute
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_OPEN);
				if(SEC_TMR_SUSPENDED 
					== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL))
				{
					GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL,
						0,
						GC_GetHybridDieselFailureInterval,
						0);
				}
			}
			g_pGcData->RunInfo.HyBrid.iLastRunDG = IB2_ALARM_RELAY_DG2;
			g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG2_SELECTED;
			//Raise DG2 alarm
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG2_RUNNING,
				GC_ALARM,
				TRUE);
			
			
		}
		else//Both
		{
			
			if(g_pGcData->RunInfo.HyBrid.bDieselFailure[0] && g_pGcData->RunInfo.HyBrid.bDieselFailure[1])
			{
				g_pGcData->RunInfo.HyBrid.iLastCloseDG = GC_DISABLED;
			}
			else if(g_pGcData->RunInfo.HyBrid.bDieselFailure[0])
			{
				g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG1;
			}
			else if(g_pGcData->RunInfo.HyBrid.bDieselFailure[1])
			{
				g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG2;
			}
			else
			{
				if(g_pGcData->RunInfo.HyBrid.iLastCloseDG == GC_DISABLED)
				{
					g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG1;
				}
			}
			
			//Check the relay status, if status is closed, do nothing, else close it
			if(g_pGcData->RunInfo.HyBrid.iLastCloseDG == IB2_ALARM_RELAY_DG1)
			{
				if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
				{
					//Execute
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_OPEN);
					if(SEC_TMR_SUSPENDED 
						== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL))
					{
						GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL,
							0,
							GC_GetHybridDieselFailureInterval,
							0);
					}
				}
				g_pGcData->RunInfo.HyBrid.iLastRunDG = IB2_ALARM_RELAY_DG2;
				g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG2_SELECTED;
				//Raise DG1 alarm
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG2_RUNNING,
					GC_ALARM,
					TRUE);
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG1_RUNNING,
					GC_NORMAL,
					TRUE);
				
				
			}
			else if(g_pGcData->RunInfo.HyBrid.iLastCloseDG == IB2_ALARM_RELAY_DG2)
			{
				if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
				{
					//Execute
					
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1, GC_HYBRID_RELAY_STATUS_OPEN);
					if(SEC_TMR_SUSPENDED 
						== GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL))
					{
						GC_SetSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL,
							0,
							GC_GetHybridDieselFailureInterval,
							0);
					}

				}
				g_pGcData->RunInfo.HyBrid.iLastRunDG = IB2_ALARM_RELAY_DG1;
				g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG1_SELECTED;
				//Raise DG2 alarm
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG1_RUNNING,
					GC_ALARM,
					TRUE);
				GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG2_RUNNING,
					GC_NORMAL,
					TRUE);
				
			
				
			}
		}
	}
	


	//Clear other alarm?
	if(enumSelectedDG == HYBRID_PUB_DG1_SELECTED )
	{
		//Close DG2; Clear the alarm	
		if(enumFailsafe == GC_NORMAL_OPEN)
		{
			if (GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
			{
				
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_OPEN );
			}
		}
		else 
		{
			if (GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
			{
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_CLOSE );
			}
		}
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG2_RUNNING,
			GC_NORMAL,
			TRUE);
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG2_FAILURE,
			GC_NORMAL,
			TRUE);
	}
	else if(enumSelectedDG == HYBRID_PUB_DG2_SELECTED)
	{
		//Close DG1; Clear the alarm	
		if(enumFailsafe == GC_NORMAL_OPEN)
		{
			if (GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
			{
				
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1, GC_HYBRID_RELAY_STATUS_OPEN );
			}
		}
		else 
		{
			if (GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
			{
				GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1, GC_HYBRID_RELAY_STATUS_CLOSE );
			}
		}

		GC_SetEnumValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG1_RUNNING,
			GC_NORMAL,
			TRUE);
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG1_FAILURE,
			GC_NORMAL,
			TRUE);
	}
	else if(enumSelectedDG == HYBRID_PUB_DG2DG1_SELECTED)
	{
		//How to do?
		
		if(g_pGcData->RunInfo.HyBrid.iCurrentRunningDG == HYBRID_PUB_DG2_SELECTED)
		{
			//Close DG1; Clear the alarm	
			if(enumFailsafe == GC_NORMAL_OPEN)
			{
				if (GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
				{
					
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1, GC_HYBRID_RELAY_STATUS_OPEN );
				}
			}
			else 
			{
				if (GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
				{
					
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1, GC_HYBRID_RELAY_STATUS_CLOSE );
				}
			}

		}
		else if(g_pGcData->RunInfo.HyBrid.iCurrentRunningDG == HYBRID_PUB_DG1_SELECTED)
		{			
			//Close DG2; Clear the alarm	
			if(enumFailsafe == GC_NORMAL_OPEN)
			{
				if (GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
				{
					
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_OPEN );
				}
			}
			else 
			{
				if (GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
				{
					
					GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_CLOSE );
				}
			}

		}
	}
	
}

static void GC_HybridRefreshHybridAlarm()
{
#define   MAX_FAILURE_TIMES			6
	SIG_ENUM	enumState, enumGridState;
	enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);//Get Mode
	enumGridState = GC_HybridGetSelectedDI();//GC_GetEnumValue(TYPE_OTHER, 0, IB2_DI1_GRID_STATE);//Get Grid state
	static int iDG1Running = 0;
	static int iDG2Running = 0;
	//~{W"o?=M#o?=o?=o?=o?=]=o?=o?=o?=M#o?=o?=o?=o?=o?=P6o?=o?=M;o?=o?=o?=o?=o?=o?=G7o?=I9o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=f2;o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=o?=N*o?=o?=o?=o?=O5M3G7Q9o?=o?=o?=P6O#o?=~}by Jimmy Wu 20130530
	//printf("Failure state is %d %d\n",  GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL), GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL));
	BOOL bDieselFailed = GC_HybridIsUnderVol() || g_pGcData->RunInfo.Rt.bAcFail;
	if(SEC_TMR_TIME_OUT == GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL))
	{
		if( bDieselFailed  && GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_DG1_RUNNING))
		{
			//Raise DG1 alarm
			GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG1_FAILURE,
					GC_ALARM,
					TRUE);
			g_pGcData->RunInfo.HyBrid.bDieselFailure[0] = TRUE;
			

		}
		else
		{
			//Clear DG1 alarm
			GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG1_FAILURE,
					GC_NORMAL,
					TRUE);
			g_pGcData->RunInfo.HyBrid.bDieselFailure[0] = FALSE;
			iDG1Running = 0;
			iDG2Running = 0;
		}
		GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL);

	}
	else if(SEC_TMR_TIME_IN != GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL1_FAILURE_INTERVAL))
	{

		//SIG_ENUM    enumSelectedDG = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_DG);
		//SIG_ENUM    enumSelectedDG = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_DG1_RUNNING);
		if(bDieselFailed)
		{	
			
			if(GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_DG1_RUNNING) )
			{
				if(iDG1Running > MAX_FAILURE_TIMES)
				{
					GC_SetEnumValue(TYPE_OTHER, 
						0,
						HYBRID_PUB_DG1_FAILURE,
						GC_ALARM,
						TRUE);
					g_pGcData->RunInfo.HyBrid.bDieselFailure[0] = TRUE;
				}
				iDG1Running++;
			}
			
		}
		else
		{
			iDG1Running = 0;
			iDG2Running = 0;
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG1_FAILURE,
				GC_NORMAL,
				TRUE);
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG2_FAILURE,
				GC_NORMAL,
				TRUE);
			g_pGcData->RunInfo.HyBrid.bDieselFailure[0] = FALSE;
			g_pGcData->RunInfo.HyBrid.bDieselFailure[1] = FALSE;
		}
	}
	if(SEC_TMR_TIME_OUT == GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL))
	{
		if(bDieselFailed && GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_DG2_RUNNING))
		{
			//Raise DG2 alarm
			GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG2_FAILURE,
					GC_ALARM,
					TRUE);	
			g_pGcData->RunInfo.HyBrid.bDieselFailure[1] = TRUE;
			
		}
		else
		{
			//Clear DG2 alarm
			GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_DG2_FAILURE,
					GC_NORMAL,
					TRUE);
			g_pGcData->RunInfo.HyBrid.bDieselFailure[1] = FALSE;
		}
		GC_SuspendSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL);
	}
	else if(SEC_TMR_TIME_IN == GC_GetStateOfSecTimer(SEC_TMR_ID_HYBRID_DIESEL2_FAILURE_INTERVAL))
	{
		if(bDieselFailed )
		{	
			
			if(GC_ALARM == GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_DG2_RUNNING) )
			{
				if(iDG2Running > MAX_FAILURE_TIMES)
				{
					GC_SetEnumValue(TYPE_OTHER, 
						0,
						HYBRID_PUB_DG2_FAILURE,
						GC_ALARM,
						TRUE);
					g_pGcData->RunInfo.HyBrid.bDieselFailure[1] = TRUE;
				}
				iDG2Running++;
			}
			
		}
		else
		{
			iDG1Running = 0;
			iDG2Running = 0;
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG1_FAILURE,
				GC_NORMAL,
				TRUE);
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG2_FAILURE,
				GC_NORMAL,
				TRUE);
			g_pGcData->RunInfo.HyBrid.bDieselFailure[0] = FALSE;
			g_pGcData->RunInfo.HyBrid.bDieselFailure[1] = FALSE;
		}
	}

	if(enumGridState == GC_HYBRID_GRID_ON && enumState != GC_HYBRID_DISABLED)
	{
		if(GC_ALARM != GC_GetEnumValue(TYPE_OTHER, 
								0,
								HYBRID_PUB_GRID_ON))
		{
			GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_GRID_ON,
					GC_ALARM,
					TRUE);	
		}
		
	}
	else
	{
		if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
								0,
								HYBRID_PUB_GRID_ON))
		{
			GC_SetEnumValue(TYPE_OTHER, 
					0,
					HYBRID_PUB_GRID_ON,
					GC_NORMAL,
					TRUE);	
		}
	}

	return;
}
static BOOL GC_HybridGetDieselState(IN int iDG)
{
	SIG_ENUM	enumFailsafe = GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_FAILSAFE);
	//1--Close  0 ---Open
	if(enumFailsafe == GC_NORMAL_OPEN)
	{
		return (GC_HyBridGetRelayStatus(iDG) == GC_HYBRID_RELAY_STATUS_OPEN) ? 1 : 0;
	}
	else
	{
		return (GC_HyBridGetRelayStatus(iDG) == GC_HYBRID_RELAY_STATUS_CLOSE) ? 1 : 0;
	}
}
static void GC_HybridCloseDiesel()
{
	SIG_ENUM	enumFailsafe = GC_GetEnumValue(TYPE_OTHER, 
					0,
					GC_PUB_FAILSAFE);
	SIG_ENUM	enumState = GC_GetEnumValue(TYPE_OTHER, 0, HYBRID_PUB_RUNNING_MODE);

	//Clear GC RUNNING ALARM
	//Check the relay status, if status is closed, do nothing, else close it
	//printf("1 IB2_ALARM_RELAY_DG1 = %d, IB2_ALARM_RELAY_DG2 = %d\n", GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1), GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2));
	if(GC_HYBRID_DISABLED == enumState)
	{
		//Execute
		GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1,  GC_HYBRID_RELAY_STATUS_OPEN);
		g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG1;
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG1_RUNNING,
			GC_NORMAL,
			TRUE);

		//Execute
		GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_OPEN);	
		g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG2;
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_DG2_RUNNING,
			GC_NORMAL,
			TRUE);

		GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RUNONOVERTEMP, GC_HYBRID_RELAY_STATUS_OPEN);					
	}
	else if(enumFailsafe == GC_NORMAL_OPEN)
	{
		if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
		{
			//Execute
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1,  GC_HYBRID_RELAY_STATUS_OPEN);
			g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG1;
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG1_RUNNING,
				GC_NORMAL,
				TRUE);
		}

		if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
		{
			//Execute
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_OPEN);	
			g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG2;
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG2_RUNNING,
				GC_NORMAL,
				TRUE);
		}
		//Close diesel, and set stat is run over temp, raise the run on over temp alarm		
		if(GC_HYBRID_RELAY_STATUS_OPEN != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RUNONOVERTEMP))
		{
			//Execute
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RUNONOVERTEMP, GC_HYBRID_RELAY_STATUS_OPEN);					
		}
	}
	else
	{
		if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1))
		{
			//Execute
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG1,  GC_HYBRID_RELAY_STATUS_CLOSE);
			g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG1;
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG1_RUNNING,
				GC_NORMAL,
				TRUE);
		}

		if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2))
		{
			//Execute
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_DG2, GC_HYBRID_RELAY_STATUS_CLOSE);	
			g_pGcData->RunInfo.HyBrid.iLastCloseDG = IB2_ALARM_RELAY_DG2;
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				HYBRID_PUB_DG2_RUNNING,
				GC_NORMAL,
				TRUE);
		}

		//Close diesel, and set stat is run over temp, raise the run on over temp alarm		
		if(GC_HYBRID_RELAY_STATUS_CLOSE != GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_RUNONOVERTEMP))
		{
			//Execute
			GC_HybridAlarmRelayAction(IB2_ALARM_RELAY_RUNONOVERTEMP, GC_HYBRID_RELAY_STATUS_CLOSE);					
		}
	}
	

	g_pGcData->RunInfo.HyBrid.iCurrentRunningDG = HYBRID_PUB_DG_SELECTED_NOTHING;
	GC_SetEnumValue(TYPE_OTHER, 
			0,
			HYBRID_PUB_RUN_ON_OVERTEMP,
			GC_NORMAL,
			TRUE);
	g_pGcData->RunInfo.HyBrid.bDieselFailure[0] = FALSE;
	g_pGcData->RunInfo.HyBrid.bDieselFailure[1] = FALSE;
	//printf("1 g_pGcData->RunInfo.HyBrid.iLastCloseDG = %d IB2_ALARM_RELAY_DG1 = %d, IB2_ALARM_RELAY_DG2 = %d\n", g_pGcData->RunInfo.HyBrid.iLastCloseDG, GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG1), GC_HyBridGetRelayStatus(IB2_ALARM_RELAY_DG2));
	return;

}

static int GC_GetHybridFixedDischarge(int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_HOUR
		* GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_FIXED_DISCHARGE_DURATION);
}
static int GC_GetHybridDieselROOTDuration(IN int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN
		* GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_RUN_ON_OVERTEMP_DURATION);
}

static int GC_GetHybridEqualismDuration(IN int iIdx)
{
	UNUSED(iIdx);
	return SECONDS_PER_MIN
		* GC_GetDwordValue(TYPE_OTHER, 0, BT_PUB_CYC_BC_DURATION);
}

static int GC_GetHybridDieselFailureInterval(IN int iIdx)
{
	UNUSED(iIdx);
	return  GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_DG_FAILURE_INTERVAL);
}
/*==========================================================================*
 * FUNCTION : IsHybridStartTime
 * PURPOSE  : To judge if it's time for a Hybrid cycle start discharge
 * CALLS    : gmtime_r
 * CALLED BY: GC_FloatCharge
 * RETURN   : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-11 11:29
 *==========================================================================*/
static BOOL GC_IsHybridEqualismTime()
{
	
	struct tm	tmNow;
	struct tm	tmLast;
	
	
	gmtime_r(&(g_pGcData->RunInfo.HyBrid.tmLastEqualisingTime), &tmLast);	
	gmtime_r(&(g_pGcData->SecTimer.tTimeNow), &tmNow);

	
	//To prevent multiple Plan-tests in one hour.
	if((tmNow.tm_year == tmLast.tm_year) 
		&& (tmNow.tm_mon == tmLast.tm_mon)
		&& (tmNow.tm_mday == tmLast.tm_mday) 
		&& (tmNow.tm_hour == tmLast.tm_hour))
	{
		
		//If no hour change, then return FALSE.
		return FALSE;
	}

	DWORD dwStartTime
		= GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_EQUALISING_START_TIME);
	
	if(tmNow.tm_hour == dwStartTime)
	{
		g_pGcData->RunInfo.HyBrid.tmLastEqualisingTime 
			= g_pGcData->SecTimer.tTimeNow;
		GC_SaveData();
		return TRUE;
	}

	return FALSE;
}


static BOOL GC_HybridChargeCapCmp()
{
	int		i;
	float		fRatedCap;
	BOOL		bValidBattery = FALSE,bRTN;
	char		szLogOut[128];
#define HYBRID_DELTA			(0.98)	//For battery capacity stop diesel.

	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{		
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			

			bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
			if(bRTN == FALSE)
				continue;

			float	fBattCap ;

			bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_CAP,&fBattCap);
			if(bRTN == FALSE)
				continue;
					
			//printf("Battery Unit is %f fRatedCap = %f\n", fBattCap, fRatedCap);
			if(fBattCap < fRatedCap * HYBRID_DELTA)
			{
				//printf("Battery Unit is %f fRatedCap = %f\n", fBattCap, fRatedCap);
				//GC_LOG_OUT(APP_LOG_INFO, szLogOut);

				return FALSE;
			}
			bValidBattery = TRUE;
		}
	}
	if(bValidBattery)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
static BOOL GC_HybridGetBattTemp()
{
	if(GC_ALARM == GC_GetEnumValue(TYPE_OTHER, 
			0,
			GC_PUB_RELAY_TEST))
	{

	}
	return FALSE;
}
static BOOL GC_HybridCmpHighLoadAlarm()
{
	int		i;
	float		fTestEndCap, fBattCap,fRatedCap;
	float fHighLoadAlarm = GC_GetFloatValue(TYPE_OTHER, 0, HYBRID_PUB_HIGH_LOAD_ALARM_SETTING);
	BOOL bRTN;
	
	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{		
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
			if(bRTN == FALSE)
				continue;
			fTestEndCap = fHighLoadAlarm *fRatedCap/ 100;	

			float	fBattCap; 

			bRTN =  GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i,BT_PRI_CAP,&fBattCap);
			if(bRTN == FALSE)
				continue;		
			if(fBattCap < fTestEndCap)
			{
				//sprintf(szLogOut, "Battery Unit is %f\n", fBattCap);
				//GC_LOG_OUT(APP_LOG_INFO, szLogOut);

				return TRUE;
			}
		}
	}
	return FALSE;
}
static BOOL	GC_IsHybridStartDischargeTime()
{
	int			i;
	struct tm	tmNow;
	struct tm	tmLast;
	
	gmtime_r(&(g_pGcData->RunInfo.HyBrid.tmLastDischargeTime), &tmLast);	
	gmtime_r(&(g_pGcData->SecTimer.tTimeNow), &tmNow);


	//To prevent multiple Plan-tests in one hour.
	if((tmNow.tm_year == tmLast.tm_year) 
		&& (tmNow.tm_mon == tmLast.tm_mon)
		&& (tmNow.tm_mday == tmLast.tm_mday) 
		&& (tmNow.tm_hour == tmLast.tm_hour))
	{
		//If no hour change, then return FALSE.
		return FALSE;
	}


	DWORD dwStartTime
		= GC_GetDwordValue(TYPE_OTHER, 0, HYBRID_PUB_FIXED_START_TIME);
	
	if(tmNow.tm_hour == dwStartTime)
	{
		g_pGcData->RunInfo.HyBrid.tmLastDischargeTime 
			= g_pGcData->SecTimer.tTimeNow;
		GC_SaveData();

		return TRUE;
	}


	return FALSE;
}

/*==========================================================================*
 * FUNCTION : GC_HybridDischCapCmp
 * PURPOSE  : To judge if discharge capacity is greater than set point
 * CALLS    : GC_GetFloatValue
 * CALLED BY: GC_PlanManTest GC_AcFailTest
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2004-10-31 17:52
 *==========================================================================*/
static BOOL GC_HybridDischCapCmp()
{
	int		i;
	float		fTestEndCap,fRatedCap;
	char		szLogOut[128];
	BOOL		bRTN;

	if(g_pGcData->RunInfo.Bt.iCmpCapDelay < 30)	//Because there is fault turn to boost charge when controller is restarting, delay 80 second.
	{
		return FALSE;
	}


	for(i = 0; i < GC_Get_Total_BattNum(); i++)
	{		
		if(g_pGcData->RunInfo.Bt.abValid[i] && g_pGcData->RunInfo.Bt.abUsedByBattMgmt[i])
		{
			bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_RATED_CAP,&fRatedCap);
			if(bRTN == FALSE)
				continue;

			fTestEndCap = GC_GetFloatValue(TYPE_OTHER, 0, HYBRID_PUB_DICHARGE_END_CAP)
				* fRatedCap/ 100;

			float	fBattCap ;
			bRTN= GC_GetFloatValueWithStateReturn(TYPE_BATT_UNIT, i, BT_PRI_CAP,&fBattCap);

			if(bRTN == FALSE)
				continue;

			if(fBattCap < fTestEndCap)
			{
				//printf("Battery Unit %d is %f fTestEndCap = %d\n", i, fBattCap, fTestEndCap);
				//GC_LOG_OUT(APP_LOG_INFO, szLogOut);

				return TRUE;
			}
		}
	}
	return FALSE;
}
static void GC_HyrbridRelayOutputCtrl(int iCtrlValue, int iRelayNo)
{
	int iError;
	int iBufLen;
	int iVarID, iVarSubID;
	int iTimeOut;
	VAR_VALUE_EX varValueEx;

	memset(&varValueEx, 0x0, sizeof(varValueEx));
	varValueEx.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	varValueEx.nSenderType	 = SERVICE_OF_LOGIC_CONTROL;
	varValueEx.pszSenderName = "GEN_CTRL";
	varValueEx.varValue.enumValue = iCtrlValue;
	
	iError			= 0;
	iBufLen			= sizeof(VAR_VALUE_EX);
	iTimeOut		= 0;

	
	if(iRelayNo < IB2_ALARM_RELAY_DG2 + 1)
	{
		iVarID			= HYBRID_IB_GROUP_EQUIP_ID;//IB2
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, iRelayNo );
	}

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
			iVarID,			
			iVarSubID,		
			iBufLen,			
			(void *)&(varValueEx),			
			iTimeOut);

	if (iError == ERR_DXI_INVALID_DATA_VALUE)
	{
		return ;
	}

	return ;
}
static void GC_HybridAlarmRelayAction(IN int iCurrRelayNo,
								   IN BOOL bAlarmStart)
{	
	int iCtrlValue;
	iCtrlValue = (bAlarmStart) ? 1 : 0;
	

	GC_HyrbridRelayOutputCtrl(iCtrlValue, iCurrRelayNo);

	return;
}


static SIG_ENUM GC_HyBridGetRelayStatus(int iRelayNo)
{

	int iError;
	int iBufLen;
	int iVarID, iVarSubID;
	int iTimeOut;

	iError			= 0;
	iBufLen			= sizeof(VAR_VALUE);
	iTimeOut		= 200;

	//1.Get data
	SIG_BASIC_VALUE *pSigVal;

	if(iRelayNo < IB2_ALARM_RELAY_DG2 + 1)
	{
		iVarID			= HYBRID_IB_GROUP_EQUIP_ID;//IB2
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_CONTROL, iRelayNo);
	}

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iVarID,			
			iVarSubID,		
			&iBufLen,			
			&pSigVal,			
			iTimeOut);	

	if (iError != ERR_OK)
	{
		return FALSE;
	}
 
	if (!SIG_VALUE_IS_VALID(pSigVal))
	{
		return FALSE;
	}
	return pSigVal->varValue.enumValue;
 
}

static SIG_ENUM GC_HyBridGetDIStatus(int iRelayNo)
{

	int iError;
	int iBufLen;
	int iVarID, iVarSubID;
	int iTimeOut;

	iError			= 0;
	iBufLen			= sizeof(VAR_VALUE);
	iTimeOut		= 200;

	//1.Get data
	SIG_BASIC_VALUE *pSigVal;

//changed by SongXu to DI1-8 moving from system to IB. 20160810
	if(iRelayNo < IB2_DI8_EIGHTH + 1)
	{
		iVarID			= HYBRID_IB_GROUP_EQUIP_ID_IB;//IB2.
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, iRelayNo );
	}
	else
	{
		iVarID			= HYBRID_IB_GROUP_EQUIP_ID;//System.
		iVarSubID		= DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, iRelayNo+ NCU_DI1_FIRST - 9 );
	}
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iVarID,			
			iVarSubID,		
			&iBufLen,			
			&pSigVal,			
			iTimeOut);	

	if (iError != ERR_OK)
	{
		return FALSE;
	}
 
	if (!SIG_VALUE_IS_VALID(pSigVal))
	{
		return FALSE;
	}

	return pSigVal->varValue.enumValue;
	
 
}


/*Ended by YangGuoxin,2011-1-18, for HybridSystem*/

#ifdef GC_SUPPORT_RELAY_TEST
#define		MAX_RELAY_NO			74
#define		MAX_IB1_RELAY_NUM		12
enum _RELAY_TEST_MODE
{
	MODE_INDIVIDUAL	= 1,
	MODE_AUTOMATIC	= 2,
	
};
GC_CFG_RELAY_INDEX	Sig[MAX_RELAY_NO] =
{
	{1,	1,	22,	199	},	//NCU
	{1,	1,	23,	200	},
	{1,	1,	24,	201	},
	{1,	1,	25,	202	},
	{202,	1,	1,	21	},	//IB1
	{202,	1,	2,	22	},
	{202,	1,	3,	23	},
	{202,	1,	4,	24	},
	{202,	1,	5,	25	},
	{202,	1,	6,	26	},
	{202,	1,	7,	27	},
	{202,	1,	8,	28	},
	



	{203,	1,	1,	21	},	//IB2
	{203,	1,	2,	22	},
	{203,	1,	3,	23	},
	{203,	1,	4,	24	},
	{203,	1,	5,	25	},
	{203,	1,	6,	26	},
	{203,	1,	7,	27	},
	{203,	1,	8,	28	},
	{204,	1,	1,	21	},	//IB3
	{204,	1,	2,	22	},
	{204,	1,	3,	23	},
	{204,	1,	4,	24	},
	{204,	1,	5,	25	},
	{204,	1,	6,	26	},
	{204,	1,	7,	27	},
	{204,	1,	8,	28	},
	{205,	1,	1,	21	},	//IB4
	{205,	1,	2,	22	},
	{205,	1,	3,	23	},
	{205,	1,	4,	24	},
	{205,	1,	5,	25	},
	{205,	1,	6,	26	},
	{205,	1,	7,	27	},
	{205,	1,	8,	28	},

	{197,	1,	1,	21	},	//EIB1
	{197,	1,	2,	22	},
	{197,	1,	3,	23	},
	{197,	1,	4,	24	},
	{197,	1,	5,	25	},

	{198,	1,	1,	21	},	//EIB2
	{198,	1,	2,	22	},
	{198,	1,	3,	23	},
	{198,	1,	4,	24	},
	{198,	1,	5,	25	},
	{199,	1,	1,	21	},	//EIB3
	{199,	1,	2,	22	},
	{199,	1,	3,	23	},
	{199,	1,	4,	24	},
	{199,	1,	5,	25	},
	{200,	1,	1,	21	},	//EIB4
	{200,	1,	2,	22	},
	{200,	1,	3,	23	},
	{200,	1,	4,	24	},
	{200,	1,	5,	25	},

	{266,	1,	1,	21	},	//SMIO3
	{266,	1,	2,	22	},
	{266,	1,	3,	23	},
	{267,	1,	1,	21	},	//SMIO4
	{267,	1,	2,	22	},
	{267,	1,	3,	23	},
	{268,	1,	1,	21	},	//SMIO5
	{268,	1,	2,	22	},
	{268,	1,	3,	23	},
	{269,	1,	1,	21	},	//SMIO6
	{269,	1,	2,	22	},
	{269,	1,	3,	23	},
	{270,	1,	1,	21	},	//SMIO7
	{270,	1,	2,	22	},
	{270,	1,	3,	23	},		
	{271,	1,	1,	21	},	//SMIO8
	{271,	1,	2,	22	},
	{271,	1,	3,	23	},		
};
static int GC_GetRelayTestDelay()
{
	return  (int)GC_GetDwordValue(TYPE_OTHER, 
				0, 
				GC_PUB_RELAY_TEST_TIME);
}
void GC_AutoRelayTest()
{	
	static int i_sRelayNO = 0;
	static int i_sLastRelayMode = GC_DISABLED;
	static int i_sLastSystemState = -1;
	static time_t t_sLastRunningTime = 0;
	static time_t	iRelayTimer[MAX_RELAY_NO] = { 0 };
	static int i_sIndivTimer[MAX_RELAY_NO] = { 0 };
	int	iRelayTime = 0;
	int	i = 0;
	int	iReturn = 0;
	static int	iTimers = 0, iRetryTimers = 0, iSleepTimers = -1, iInitialTimes = 0;

	
	if(iInitialTimes < 150)
	{
		iInitialTimes++;
		return;
	}
	

	if(i_sLastRelayMode == -1)
	{
		i_sLastSystemState = g_pGcData->RunInfo.enumAutoMan;

	}
	int	iCurrentMode =  GC_GetEnumValue(TYPE_OTHER, 
						0,
						GC_PUB_RELAY_TEST);
	if(iInitialTimes == 150 && iCurrentMode != GC_DISABLED )//First start.
	{
		iInitialTimes++;
		if(STATE_MAN != g_pGcData->RunInfo.enumAutoMan)
		{		
			GC_ControlAllRelay(FALSE);
		}
		GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL);
		GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE);
		i_sRelayNO = 0;
		
		for( i = 0; i < MAX_RELAY_NO; i++)
		{
			iRelayTimer[i] = 0;
			i_sIndivTimer[i] = 0;
			GC_RaiseRelayAlarm(i, GC_NORMAL);
		}
		GC_SetEnumValue(TYPE_OTHER, 
			0,
			GC_PUB_RELAY_TESTING, 
			GC_NORMAL,
			TRUE);
		return;

	}

	if(STATE_MAN == g_pGcData->RunInfo.enumAutoMan 
		|| GC_DISABLED == iCurrentMode)
	{
		//Clear state,Clear alarm,Clear timer;
		if(i_sLastRelayMode != GC_DISABLED)
		{	
			GC_ControlAllRelay(TRUE);
			GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL);
			GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE);
			i_sRelayNO = 0;
			i_sLastRelayMode = GC_DISABLED;
			t_sLastRunningTime = 0;
			for( i = 0; i < MAX_RELAY_NO; i++)
			{
				iRelayTimer[i] = 0;
				i_sIndivTimer[i] = 0;
				GC_RaiseRelayAlarm(i, GC_NORMAL);
			}
			GC_SetEnumValue(TYPE_OTHER, 
				0,
				GC_PUB_RELAY_TESTING, 
				GC_NORMAL,
				TRUE);
		}
		i_sLastSystemState = STATE_MAN;
		iSleepTimers = -1;
		iTimers = 0;
		iRetryTimers = 0;
		return;
	}
	
		
	//Clear all relay state.
	if((i_sLastRelayMode == GC_DISABLED) 
		|| (i_sLastSystemState == STATE_MAN) 
		|| (i_sLastRelayMode != iCurrentMode))
	{
		if(iSleepTimers == 0)
		{		
			GC_ControlAllRelay(FALSE);
		}
		if(((i_sLastRelayMode == GC_DISABLED) 
			&& iCurrentMode != GC_DISABLED && iSleepTimers < 10) || iSleepTimers == -1)
		{
			iSleepTimers++;
			i_sLastRelayMode == GC_DISABLED;
			return;
		}
		i_sLastRelayMode = iCurrentMode;
		i_sRelayNO = 0;
		t_sLastRunningTime = 0;
		for( i = 0; i < MAX_RELAY_NO; i++)
		{
			iRelayTimer[i] = 0;
			i_sIndivTimer[i] = 0;
			GC_RaiseRelayAlarm(i, GC_NORMAL);
		}
		iSleepTimers = 0;
		iTimers = 0;
		iRetryTimers = 0;
	}	


	i_sLastSystemState = g_pGcData->RunInfo.enumAutoMan;
	i_sLastRelayMode = iCurrentMode;
	GC_SetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_RELAY_TESTING, 
		GC_ALARM,
		TRUE);

	if(iCurrentMode == MODE_AUTOMATIC)
	{
		if(i_sRelayNO < MAX_RELAY_NO)
		{
			if(GC_AlmJudgmentForBT())
			{
				//Keep the current state, but have no any new action.
				if( SEC_TMR_SUSPENDED 
					!= GC_GetStateOfSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL)) 
				{
					GC_RelayControl(i_sRelayNO, GC_HYBRID_RELAY_STATUS_CLOSE, FALSE);
					GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL);
					
				}

				GC_SetEnumValue(TYPE_OTHER, 
					0, 
					GC_PUB_RELAY_TEST,
					GC_DISABLED,
					TRUE);

				return;
			}
			
			if(SEC_TMR_SUSPENDED 
				== GC_GetStateOfSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL))
			{
				iReturn = GC_RelayControl(i_sRelayNO, GC_HYBRID_RELAY_STATUS_CLOSE, FALSE);
				if(iReturn == TRUE)
				{
					GC_SetSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL, 
						0,
						GC_GetRelayTestDelay,
						0);
				}
				else if(iReturn == FALSE)
				{
					if(iRetryTimers < 5)
					{
						iRetryTimers++;
					}
					else
					{
						i_sRelayNO++;
						iRetryTimers = 0;
					}
				}
				else//Return -1
				{
					i_sRelayNO++;
					iRetryTimers = 0;
				}

			}	

			if( SEC_TMR_TIME_OUT 
				== GC_GetStateOfSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL)) 
			{
				if(iTimers % 5 == 0)
				{
					GC_RelayControl(i_sRelayNO, GC_HYBRID_RELAY_STATUS_OPEN, FALSE);
				}
				iTimers++;
				
				if(iTimers > 30 || GC_GetRelayStatus(Sig[i_sRelayNO].iEquipID, Sig[i_sRelayNO].iSignalType, Sig[i_sRelayNO].iSignalID) == GC_HYBRID_RELAY_STATUS_OPEN)//Check status
				{				
					GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_INTERVAL);
					GC_RaiseRelayAlarm(i_sRelayNO, GC_NORMAL);
					i_sRelayNO++;
					iTimers = 0;
				}
			}

		}
		else
		{
			GC_ControlAllRelay(TRUE);
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				GC_PUB_RELAY_TEST,
				GC_DISABLED,
				TRUE);
		}
	}
	else	//Individual
	{
		if(GC_AlmJudgmentForBT())
		{
			GC_ControlAllRelay(TRUE);
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				GC_PUB_RELAY_TEST,
				GC_DISABLED,
				TRUE);
			return;
		}

		time_t tCurrentTime = time(NULL);
		if(t_sLastRunningTime == 0)//Init
		{
			t_sLastRunningTime = tCurrentTime;
		}
		else if(ABS(t_sLastRunningTime - tCurrentTime) > GC_INTERVAL_BATT_MGMT)
		{
			//Check time change
			for( i = 0; i < MAX_RELAY_NO; i++ )
			{
				if(iRelayTimer[i] != 0 )
				{
					iRelayTimer[i] = tCurrentTime - (t_sLastRunningTime - iRelayTimer[i]);  
				}
			}			
		}
		t_sLastRunningTime = tCurrentTime;		
		iRelayTime = GC_GetRelayTestDelay();
		if(SEC_TMR_SUSPENDED 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE))
		{
			GC_SetSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE, 
				600,
				NULL,
				0);
		}

		for(i = 0; i < MAX_RELAY_NO; i++)
		{
			if(GC_GetRelayStatus(Sig[i].iEquipID, Sig[i].iSignalType, Sig[i].iSignalID) == GC_HYBRID_RELAY_STATUS_CLOSE)
			{
				if(iRelayTimer[i] == 0)
				{
					iRelayTimer[i] = tCurrentTime;
					
					GC_SetSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE, 
						600,
						NULL,
						0);
					GC_RaiseRelayAlarm(i, GC_ALARM);	
				}
			}
		}

		for(i = 0; i < MAX_RELAY_NO; i++)
		{
			if(iRelayTimer[i] != 0 && (tCurrentTime - iRelayTimer[i] > iRelayTime))
			{
				if(i_sIndivTimer[i] % 5 == 0)
				{
					GC_RelayControl(i, GC_HYBRID_RELAY_STATUS_OPEN, FALSE);
				}
				i_sIndivTimer[i]++;
				if(i_sIndivTimer[i] > 30 || GC_GetRelayStatus(Sig[i].iEquipID, Sig[i].iSignalType, Sig[i].iSignalID) == GC_HYBRID_RELAY_STATUS_OPEN)//Check status
				{				
					iRelayTimer[i] = 0;

					GC_SetSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE, 
						600,
						NULL,
						0);
					GC_RaiseRelayAlarm(i, GC_NORMAL);
					i_sIndivTimer[i] = 0;
				}

				
					
			}
		}

		if(SEC_TMR_TIME_OUT 
			== GC_GetStateOfSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE))
		{
			//Change to disable, Clear alarm
			GC_SetEnumValue(TYPE_OTHER, 
				0, 
				GC_PUB_RELAY_TEST,
				GC_DISABLED,
				TRUE);

			GC_SuspendSecTimer(SEC_TMR_ID_RELAY_TEST_CHANGESTEATE);
		}

	}
	return;
}

static  int GC_ControlAllRelay(IN BOOL bClear)
{
	int	i = 0;
	while( i < MAX_RELAY_NO)
	{
	
		GC_RelayControl(i, GC_HYBRID_RELAY_STATUS_OPEN, bClear);
		i++;
	}
	return 0; 
}

static  BOOL GC_RelayControl(IN int iRelayNum, IN int iStatus, IN BOOL bClear)
{
	SIG_BASIC_VALUE *pSigVal;
	VAR_VALUE_EX SigVal;
	int iError;
	int iBufLen;
	int iTimeOut = 200;
	int iValue;

	ASSERT(iRelayNum < MAX_RELAY_NO);
	//printf("Sig[iRelayNum].iEquipID = %d, Sig[iRelayNum].iSignalID = %d\n",Sig[iRelayNum].iEquipID, Sig[iRelayNum].iSignalID);
	if(Sig[iRelayNum].iEquipID == 1 )//System
	{
		//printf("Control relay %d, S\n",iRelayNum);
		if(Sig[iRelayNum].iSignalID < 22)
		{
			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				Sig[iRelayNum].iEquipID,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 78 ),		
				&iBufLen,			
				&pSigVal,			
				iTimeOut);
			
			if(iError != ERR_OK || !SIG_VALUE_IS_VALID(pSigVal) || pSigVal->varValue.ulValue == 0)//No IB board
			{
				//Is not valid equipment.
				//printf("return 0\n");
				return -1;
			}

			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						1,			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 79 ),		
						&iBufLen,			
						&pSigVal,			
						iTimeOut);
			
			if(iError != ERR_OK)  
			{
				//Is not valid signal.
				//printf("return 1\n");
				return -1;
			}
			
			else if(pSigVal->varValue.enumValue == 0)//IB1 Type,then skip
			{	
				if(Sig[iRelayNum].iSignalID >= 17 && Sig[iRelayNum].iSignalID <= 20)
				{
					//printf("return 2\n");
					return -1;
				}
			}
		}
		
	}
	else
	{
		iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			Sig[iRelayNum].iEquipID,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 100 ),		
			&iBufLen,			
			&pSigVal,			
			iTimeOut);
		if(iError != ERR_OK || !SIG_VALUE_IS_VALID(pSigVal) || pSigVal->varValue.enumValue == 1)
		{
			//Is not valid equipment.
			//printf("return 3\n");
			return -1;
		}
		
		if(Sig[iRelayNum].iEquipID == 202|| Sig[iRelayNum].iEquipID ==203 || Sig[iRelayNum].iEquipID == 204 || Sig[iRelayNum].iEquipID == 205)
		{
			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
					201,			
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 1 ),//IB Type		
					&iBufLen,			
					&pSigVal,			
					iTimeOut);
		
			if(iError != ERR_OK )
			{
				//Is not valid equipment.
				//printf("return4\n");
				return -1;
			}
			else if(pSigVal->varValue.enumValue == 0)//IB1 Type,then skip
			{
				if(Sig[iRelayNum].iSignalID >= 5 && Sig[iRelayNum].iSignalID <= 8)
				{
					//IB 1 type ,then skip relay 5 to 8
					//printf("return 6\n");
					return -1;
				}
			}
		}
	}
	
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				Sig[iRelayNum].iEquipID,			
				DXI_MERGE_SIG_ID(Sig[iRelayNum].iSignalType, Sig[iRelayNum].iSignalID ),		
				&iBufLen,			
				&pSigVal,			
				iTimeOut);

	if (iError == ERR_OK && SIG_VALUE_IS_VALID(pSigVal)  )
	{

		SigVal.nSendDirectly = TRUE;
		SigVal.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		SigVal.pszSenderName = "GEN_CTRL";

		//if( iRelayNum < MAX_IB1_RELAY_NUM) //Failsafe is valid for IB1.
		//{
		//	if(bClear)
		//	{
		//		SigVal.varValue.enumValue = iStatus;
		//	}
		//	else
		//	{

		//		SIG_ENUM	enumFailsafe = GC_GetEnumValue(TYPE_OTHER, 
		//							0,
		//							GC_PUB_FAILSAFE_NA);

		//		SigVal.varValue.enumValue = (enumFailsafe == GC_DISABLED) ? iStatus : 
		//			((iStatus == GC_HYBRID_RELAY_STATUS_CLOSE) ? GC_HYBRID_RELAY_STATUS_OPEN : GC_HYBRID_RELAY_STATUS_CLOSE);
		//	}
		//}
		//else
		{			
			SigVal.varValue.enumValue = iStatus;
		}
		
		iError = DxiSetData(VAR_A_SIGNAL_VALUE,
			Sig[iRelayNum].iEquipID,
			DXI_MERGE_SIG_ID(Sig[iRelayNum].iSignalType, 
				Sig[iRelayNum].iSignalID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
		if(iError == ERR_OK)
		{
			iValue = (iStatus == GC_HYBRID_RELAY_STATUS_CLOSE) ? 1 : 0;
			GC_RaiseRelayAlarm(iRelayNum, iValue);
			
			//printf("return TRUE\n");
			return TRUE;
		}		
	}
	//printf("return FALSE\n");
	return FALSE;
}
static void GC_RaiseRelayAlarm(IN int iRelayNum, IN int iAarlm)
{
	VAR_VALUE_EX SigVal;
	SigVal.varValue.enumValue = iAarlm;
	SigVal.nSendDirectly = TRUE;
	SigVal.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	SigVal.pszSenderName = "GEN_CTRL";

	DxiSetData(VAR_A_SIGNAL_VALUE,
			Sig[iRelayNum].iEquipID,
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 
				Sig[iRelayNum].iAlarmID),
			sizeof(VAR_VALUE_EX),
			&SigVal,
			0);
	return;
}
static SIG_ENUM GC_GetRelayStatus(IN int iEquipID, IN int iSignalType, IN int iSIgnalID)
{
	int iError;
	int iBufLen;
	int iTimeOut;

	iError			= 0;
	iTimeOut		= 200;

	//1.Get data
	SIG_BASIC_VALUE *pSigVal, *pOtherSigVal;

	SIG_ENUM	enumFailsafe = GC_GetEnumValue(TYPE_OTHER, 
		0,
		GC_PUB_FAILSAFE_NA);

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		DXI_MERGE_SIG_ID(iSignalType, iSIgnalID),		
		&iBufLen,			
		&pSigVal,			
		iTimeOut);	

	if (iError != ERR_OK)
	{
		return -1;
	}

	if (!SIG_VALUE_IS_VALID(pSigVal))
	{
		return -1;
	}
	if( iEquipID == 1) //Failsafe is valid for IB1.
	{	
		if(iSIgnalID < 22)
		{
			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				iEquipID,			
				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 78 ),		
				&iBufLen,			
				&pOtherSigVal,			
				iTimeOut);

			if(iError != ERR_OK || !SIG_VALUE_IS_VALID(pOtherSigVal) || pOtherSigVal->varValue.ulValue == 0)//No IB board
			{
				//Is not valid equipment.
				return -1;
			}
			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipID,			
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 79 ),		
						&iBufLen,			
						&pOtherSigVal,			
						iTimeOut);
			
			if(iError != ERR_OK)  
			{
				return -1;
			}			
			else if(pOtherSigVal->varValue.enumValue == 0)//IB1 Type,then skip
			{	
				if(iSIgnalID >= 17 && iSIgnalID <= 20)
				{
					return -1;
				}
			}
		}


		//return ((enumFailsafe == GC_DISABLED) ? pSigVal->varValue.enumValue : 
		//	((pSigVal->varValue.enumValue == GC_HYBRID_RELAY_STATUS_CLOSE) ? GC_HYBRID_RELAY_STATUS_OPEN : GC_HYBRID_RELAY_STATUS_CLOSE));
		return pSigVal->varValue.enumValue; 
	}
	else
	{	
		return pSigVal->varValue.enumValue;
	}
}
#endif

static void GC_SendMTCurrentLmt(IN float fCurrentLmt)
{
	int iQtyOfMppt = GC_GetDwordValue(TYPE_OTHER, 0, MT_PUB_QTY_SELF);
	if(iQtyOfMppt)
	{
		GC_SetFloatValue(TYPE_OTHER, 
				0,
				MT_PUB_CURR_LMT_CTL, 
				fCurrentLmt);		
	}
	return;
}

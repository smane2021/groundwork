/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_default_screen.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:55
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_DEFAULT_SCREEN_H__041008
#define __LCD_DEFAULT_SCREEN_H__041008

#ifdef  __cplusplus
extern "C" {
#endif

	struct tagRectFillStru
	{
		int x;
		int y;

		int nWidth;
	};
	typedef struct tagRectFillStru RECT_FILL_STRU;

static int DisCurTimeOnScreen(char* pLangCode);

static int DisplayDefaultScreen(MENU_DISPLAY* pMenuDisplay);

static int Compare_RECT_FILL_STRU(const RECT_FILL_STRU *p1,
								  const RECT_FILL_STRU *p2);

static void MakeRectStruSort(RECT_FILL_STRU* pRectFillStru, int nFillStruNum);


#ifdef __cplusplus
}
#endif
#endif //__LCD_DEFAULT_SCREEN_H__041008

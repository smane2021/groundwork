/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : test_lcd_module.c
*  CREATOR  : HULONGWEN                DATE: 2004-10-19 11:29
*  VERSION  : V1.00
*  PURPOSE  : To test user_manage.so
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>

#include "lcd_public.h"

char g_szACUConfigDir[MAX_FILE_PATH] = "/home/hulw/work/src/config";

BOOL InitSiteInfo(void);
void InitUserManageInfo(void);

static void LCDTestFunc(void)
{
		ServiceInit(0, NULL);

		ServiceMain(NULL);
}

/*==========================================================================*
* FUNCTION : main
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int    argc    : 
*            INT char  *argv[] : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-09-30 16:38
*==========================================================================*/
int main(IN int argc, IN char *argv[])
{

	printf("Test LCD module start!\r\n");

	InitSiteInfo();


//	extern int Timer_ManagerInit(IN int nTimerResolution);

//	Timer_ManagerInit(20);

	extern int InitDataExchangeModule();
	//At first init data exchange module
	InitDataExchangeModule();

		extern BOOL InitUserManage(void);
	InitUserManage();

	InitUserManageInfo();


	if(argv[1])
	{
		if(strcmp(argv[1], "-user") == 0)
		{
			ServiceInit(0, NULL);

			ServiceMain(NULL);
		}
	}

	else
	{
		RunThread_Create("LCD Test thread",
		LCDTestFunc,
		(void*)NULL,
		(DWORD*)NULL,
		0);

		while (getchar() != 'q')
		{
		}

	}

	return 0;
}

//���ڴ�׮��data_exchange.c��Ҫ�õ���ȫ�ֱ����ͺ���

//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO  g_SiteInfo;

//need to be defined in Miscellaneous Function Module
TIME_SRV_INFO g_sTimeSrvInfo;

//need to be defined in equip management Module
int Equip_Control(int nEquipID, int nSigType, int nSigID, 
				  VAR_VALUE *pCtrlValue, DWORD dwTimeout)
{
	SIG_BASIC_VALUE* pSigValue = NULL;
	int				 nBufLen;

	ASSERT(pCtrlValue);

	DxiGetData(VAR_A_SIGNAL_VALUE,
			nEquipID,			
			DXI_MERGE_SIG_ID(nSigType, nSigID),		
			&nBufLen,			
			&(pSigValue),			
			0);

	ASSERT(pSigValue);

	memcpy(&(pSigValue->varValue), pCtrlValue, sizeof(VAR_VALUE));
		

	return TRUE;
}

//need to be defined in Miscellaneous Function Module
int UpdateACUTime (time_t* pTimeRet)
{
	return stime(pTimeRet);
}

BOOL ConstructLangText(LANG_TEXT*	pLangText,
					   int			iMaxLenForFull,
					   int			iMaxLenForAbbr,
					   char*		pFullName0,
					   char*		pFullName1,
					   char*		pAbbrName0,
					   char*		pAbbrName1)


{
	char* pUseString;


	if (pLangText)
	{
		//	pLangText->iResourceID = 20;
		pLangText->iMaxLenForFull = iMaxLenForFull;	
		pLangText->iMaxLenForAbbr = iMaxLenForAbbr;	

		pUseString = NEW(char, pLangText->iMaxLenForFull);
		strncpyz(pUseString, pFullName0, pLangText->iMaxLenForFull);
		pLangText->pFullName[0] = pUseString;	

		pUseString = NEW(char, pLangText->iMaxLenForFull);
		strncpyz(pUseString, pFullName1, pLangText->iMaxLenForFull);
		pLangText->pFullName[1] = pUseString;

		pUseString = NEW(char, pLangText->iMaxLenForAbbr);
		strncpyz(pUseString, pAbbrName0, pLangText->iMaxLenForAbbr);
		pLangText->pAbbrName[0] = pUseString;	

		pUseString = NEW(char, pLangText->iMaxLenForAbbr);
		strncpyz(pUseString, pAbbrName1, pLangText->iMaxLenForAbbr);
		pLangText->pAbbrName[1] = pUseString;
	}

	return TRUE;
}

//Init site info
BOOL InitSiteInfo(void)
{
#ifdef _DEBUG
	printf("filename: test_data_exchange.c [InitSiteInfo]\n\r");
#endif 	/*_DEBUG	*/

	g_SiteInfo.LangInfo.szLocaleLangCode = "zh";

	g_SiteInfo.iSiteID = 15;

	ConstructLangText(&(g_SiteInfo.langSiteName),
		32,
		16,
		"Advanced controller unit team",
		"Advanced controller unit ��Ŀ��",
		"ACU team",
		"ACU ��Ŀ��");

	ConstructLangText(&(g_SiteInfo.langSiteLocation),
		64,
		32,
		"On the dept. of monitor",
		"����¥��ز�",
		"On the dept. of monitor",
		"����¥��ز�");

	ConstructLangText(&(g_SiteInfo.langDescription),
		64,
		32,
		"Has strong function",
		"ACU���ܺ�ǿ���",
		"Has strong function",
		"ACU���ܺ�ǿ���");


	{
		//four standard equip types

		STDEQUIP_TYPE_INFO*	pStdEquipTypeInfo;	

		SAMPLE_SIG_INFO	*pSampleSigInfo;
		CTRL_SIG_INFO	*pCtrlSigInfo;
		SET_SIG_INFO	*pSetSigInfo;
		ALARM_SIG_INFO	*pAlarmSigInfo;

		g_SiteInfo.iStdEquipTypeNum = 5;	

		pStdEquipTypeInfo = NEW(STDEQUIP_TYPE_INFO,g_SiteInfo.iStdEquipTypeNum);
		ZERO_POBJS(pStdEquipTypeInfo, g_SiteInfo.iStdEquipTypeNum);

		g_SiteInfo.pStdEquipTypeInfo = pStdEquipTypeInfo;

		//�����The first equip is battery group
		pStdEquipTypeInfo->iTypeID = 3;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"Battery group",
			"������",
			"Bat grp",
			"�����");


		//battery equip has three sample signals
		pStdEquipTypeInfo->iSampleSigNum = 3;

		pSampleSigInfo = NEW(SAMPLE_SIG_INFO,pStdEquipTypeInfo->iSampleSigNum);
		ZERO_POBJS(pSampleSigInfo, pStdEquipTypeInfo->iSampleSigNum);

		pStdEquipTypeInfo->pSampleSigInfo = pSampleSigInfo;
		pSampleSigInfo->iSigID = 1;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery total current",
			"�����ܵ���",
			"Bat cur",
			"����ܵ���");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "6.0";
		pSampleSigInfo->szSigUnit = "A";


		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 100;
		pSampleSigInfo->fMaxValidValue = 1000;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 2;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery average voltage",
			"����ƽ����ѹ",
			"Bat avr vol",
			"��ؾ�ѹ");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "4.1";
		pSampleSigInfo->szSigUnit = "V";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_NONE;

		pSampleSigInfo->fMinValidValue = 43;
		pSampleSigInfo->fMaxValidValue = 58;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 7;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery temparature",
			"�����¶�",
			"Bat temp",
			"�����");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "5.1";
		pSampleSigInfo->szSigUnit = "degC";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = -40;
		pSampleSigInfo->fMaxValidValue = 80;

		//Batt group has one alarm signals
		pStdEquipTypeInfo->iAlarmSigNum = 1;

		pAlarmSigInfo = NEW(ALARM_SIG_INFO,pStdEquipTypeInfo->iAlarmSigNum);

		ZERO_POBJS(pAlarmSigInfo, pStdEquipTypeInfo->iAlarmSigNum);

		pStdEquipTypeInfo->pAlarmSigInfo = pAlarmSigInfo;

		pAlarmSigInfo->iSigID = 1;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"Battery temperature hight",
			"��س���",
			"Hight bat temp",
			"��س���");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_MAJOR;

		//ģ��The second equip is rectifier
		pStdEquipTypeInfo++;

		pStdEquipTypeInfo->iTypeID = 2;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"Rectifier group",
			"����ģ��",
			"Rect grp",
			"ģ��");

		//Rectifier equip has two control signals
		pStdEquipTypeInfo->iCtrlSigNum = 2;

		pCtrlSigInfo = NEW(CTRL_SIG_INFO,pStdEquipTypeInfo->iCtrlSigNum);

		ZERO_POBJS(pCtrlSigInfo, pStdEquipTypeInfo->iCtrlSigNum);

		pStdEquipTypeInfo->pCtrlSigInfo = pCtrlSigInfo;

		pCtrlSigInfo->iSigID = 1;

		pCtrlSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pCtrlSigInfo->pSigName,
			32,
			16,
			"Rectifier current limit control",
			"����ģ�����������",
			"curr lmt ctrl",
			"ģ������");


		pCtrlSigInfo->fMinValidValue = 0;
		pCtrlSigInfo->fMaxValidValue = 110;

		pCtrlSigInfo->iSigValueType = VAR_UNSIGNED_LONG;
		pCtrlSigInfo->szValueDisplayFmt = "2";
		pCtrlSigInfo->szSigUnit = "%";

		pCtrlSigInfo->fControlStep = 2;

		pCtrlSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pCtrlSigInfo->byAuthorityLevel = ADMIN_LEVEL;

		pCtrlSigInfo++;

		pCtrlSigInfo->iSigID = 2;

		pCtrlSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pCtrlSigInfo->pSigName,
			32,
			16,
			"Rectifier voltage control",
			"����ģ���ѹ����",
			"Rect vol ctrl",
			"ģ���ѹ");

		pCtrlSigInfo->iSigValueType = VAR_FLOAT;

		pCtrlSigInfo->fMinValidValue = 45;
		pCtrlSigInfo->fMaxValidValue = 58;

		pCtrlSigInfo->szValueDisplayFmt = ".1";
		pCtrlSigInfo->szSigUnit = "V";

		pCtrlSigInfo->fControlStep = 0.1;

		pCtrlSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pCtrlSigInfo->byAuthorityLevel = OPERATOR_LEVEL;

		//alarm signals
		//Rectifier equip has two alarm signals
		pStdEquipTypeInfo->iAlarmSigNum = 2;

		pAlarmSigInfo = NEW(ALARM_SIG_INFO,pStdEquipTypeInfo->iAlarmSigNum);

		ZERO_POBJS(pAlarmSigInfo, pStdEquipTypeInfo->iAlarmSigNum);

		pStdEquipTypeInfo->pAlarmSigInfo = pAlarmSigInfo;

		pAlarmSigInfo->iSigID = 1;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"Rectifier AC failure",
			"ģ�齻��ͣ��",
			"AC failure",
			"����ͣ��");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_CRITICAL;

		pAlarmSigInfo++;

		pAlarmSigInfo->iSigID = 2;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"Multiple Rectifier Failure",
			"˫(��)ģ�����",
			"Rect Failure",
			"ģ�����");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_MAJOR;

		//����������The third equip is general controller

		pStdEquipTypeInfo++;

		pStdEquipTypeInfo->iTypeID = 7;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"ACU base controller",
			"ACU����������",
			"Base controller",
			"����������");

		//GC equip has three set signals
		pStdEquipTypeInfo->iSetSigNum = 3;

		pSetSigInfo = NEW(SET_SIG_INFO,pStdEquipTypeInfo->iSetSigNum);

		ZERO_POBJS(pSetSigInfo, pStdEquipTypeInfo->iSetSigNum);

		pStdEquipTypeInfo->pSetSigInfo = pSetSigInfo;
		pSetSigInfo->iSigID = 1;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Constant Current Test Enabled",
			"��غ�����������",
			"Constant curr",
			"������������");

		pSetSigInfo->iSigValueType = VAR_ENUM;

		pSetSigInfo->iStateNum = 2;
		pSetSigInfo->pStateText = NEW(LANG_TEXT*, 2);
		pSetSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[0],
			32,
			16,
			"Test Enabled",
			"��������",
			"Enabled",
			"����");
		pSetSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[1],
			32,
			16,
			"Test Disabled",
			"���Բ�����",
			"Disable",
			"������");

		pSetSigInfo->szValueDisplayFmt = "8";

		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;
		

		pSetSigInfo->fMinValidValue = 0;
		pSetSigInfo->fMaxValidValue = 1;

		pSetSigInfo++;

		pSetSigInfo->iSigID = 2;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Planned Test Enabled",
			"��ض�ʱ��������",
			"Planned Test",
			"��ʱ��������");

		pSetSigInfo->iSigValueType = VAR_ENUM;

		pSetSigInfo->iStateNum = 2;
		pSetSigInfo->pStateText = NEW(LANG_TEXT*, 2);
		pSetSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[0],
			32,
			16,
			"Test Enabled",
			"��������",
			"Enabled",
			"����");
		pSetSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[1],
			32,
			16,
			"Test Disabled",
			"���Բ�����",
			"Disable",
			"������");

		pSetSigInfo->szValueDisplayFmt = "8";

		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;


		pSetSigInfo->fMinValidValue = 0;
		pSetSigInfo->fMaxValidValue = 1;

		pSetSigInfo++;

		pSetSigInfo->iSigID = 7;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Battery Test End Time",
			"��ز�����ֹʱ��",
			"Test end time",
			"������ֹʱ��");

		pSetSigInfo->iSigValueType = VAR_UNSIGNED_LONG;

		pSetSigInfo->szValueDisplayFmt = "5";
		pSetSigInfo->szSigUnit = "����";
		pSetSigInfo->fSettingStep = 9;
		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSetSigInfo->fMinValidValue = 60;
		pSetSigInfo->fMaxValidValue = 360;		

		//Base controller has one alarm signals
		pStdEquipTypeInfo->iAlarmSigNum = 1;

		pAlarmSigInfo = NEW(ALARM_SIG_INFO,pStdEquipTypeInfo->iAlarmSigNum);

		ZERO_POBJS(pAlarmSigInfo, pStdEquipTypeInfo->iAlarmSigNum);

		pStdEquipTypeInfo->pAlarmSigInfo = pAlarmSigInfo;

		pAlarmSigInfo->iSigID = 1;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"Battery test failure",
			"��ز���ʧ��",
			"Battest fail",
			"��ز���ʧ��");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_OBSERVATION;

		//���The fourth standard equip is battery
		pStdEquipTypeInfo++;

		pStdEquipTypeInfo->iTypeID = 4;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"Battery ",
			"���",
			"Battery",
			"���");


		//battery equip has five sample signals
		pStdEquipTypeInfo->iSampleSigNum = 5;

		pSampleSigInfo = NEW(SAMPLE_SIG_INFO,pStdEquipTypeInfo->iSampleSigNum);
		ZERO_POBJS(pSampleSigInfo, pStdEquipTypeInfo->iSampleSigNum);

		pStdEquipTypeInfo->pSampleSigInfo = pSampleSigInfo;

		pSampleSigInfo->iSigID = 1;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery  current",
			"��ص���",
			"Bat cur",
			"��ص���");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "6.0";
		pSampleSigInfo->szSigUnit = "A";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 100;
		pSampleSigInfo->fMaxValidValue = 1000;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 2;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery average voltage",
			"���ƽ����ѹ",
			"Bat avr vol",
			"��ؾ�ѹ");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "4.1";
		pSampleSigInfo->szSigUnit = "V";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 43;
		pSampleSigInfo->fMaxValidValue = 58;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 7;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery temparature",
			"����¶�",
			"Bat temp",
			"�����");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "5.1";
		pSampleSigInfo->szSigUnit = "degC";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = -40;
		pSampleSigInfo->fMaxValidValue = 80;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 20;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery  current",
			"��ؽڵ���",
			"Bat cell cur",
			"��ؽڵ���");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "3.1";
		pSampleSigInfo->szSigUnit = "AN";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 100;
		pSampleSigInfo->fMaxValidValue = 1000;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 30;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"Battery average��cell voltage",
			"��ؽڵ�ѹ",
			"Bat avr cell vol",
			"��ؽ�ѹ");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = "4.1";
		pSampleSigInfo->szSigUnit = "VOL";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 43;
		pSampleSigInfo->fMaxValidValue = 58;

		//Battery has one alarm signals
		pStdEquipTypeInfo->iAlarmSigNum = 1;

		pAlarmSigInfo = NEW(ALARM_SIG_INFO,pStdEquipTypeInfo->iAlarmSigNum);

		ZERO_POBJS(pAlarmSigInfo, pStdEquipTypeInfo->iAlarmSigNum);

		pStdEquipTypeInfo->pAlarmSigInfo = pAlarmSigInfo;

		pAlarmSigInfo->iSigID = 1;

		pAlarmSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pAlarmSigInfo->pSigName,
			32,
			16,
			"No battery ballance",
			"��ز�����",
			"No bat ballance",
			"��ز�����");

		pAlarmSigInfo->iAlarmLevel = ALARM_LEVEL_CRITICAL;

		//ϵͳThe fifth standard equip is system stdEquip
		pStdEquipTypeInfo++;

		pStdEquipTypeInfo->iTypeID = 100;

		pStdEquipTypeInfo->pTypeName = NEW(LANG_TEXT,1);
		ConstructLangText(pStdEquipTypeInfo->pTypeName,
			32,
			16,
			"System group",
			"ϵͳ��",
			"System group",
			"ϵͳ��");


		//battery equip has five sample signals
		pStdEquipTypeInfo->iSampleSigNum = 4;

		pSampleSigInfo = NEW(SAMPLE_SIG_INFO,pStdEquipTypeInfo->iSampleSigNum);
		ZERO_POBJS(pSampleSigInfo, pStdEquipTypeInfo->iSampleSigNum);

		pStdEquipTypeInfo->pSampleSigInfo = pSampleSigInfo;

		pSampleSigInfo->iSigID = 1;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"System Voltage",
			"ϵͳ��ѹ",
			"System Voltage",
			"ϵͳ��ѹ");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = ".1";
		pSampleSigInfo->szSigUnit = "V";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 42;
		pSampleSigInfo->fMaxValidValue = 60;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 2;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"System Load",
			"ϵͳ����",
			"System Load",
			"ϵͳ����");

		pSampleSigInfo->iSigValueType = VAR_FLOAT;
		pSampleSigInfo->szValueDisplayFmt = ".0";
		pSampleSigInfo->szSigUnit = "A";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 50;
		pSampleSigInfo->fMaxValidValue = 6000;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 3;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"System Power",
			"ϵͳ����",
			"System Power",
			"ϵͳ����");

		pSampleSigInfo->iSigValueType = VAR_UNSIGNED_LONG;
		pSampleSigInfo->szValueDisplayFmt = "3";
		pSampleSigInfo->szSigUnit = "%";

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 0;
		pSampleSigInfo->fMaxValidValue = 100;

		pSampleSigInfo++;

		pSampleSigInfo->iSigID = 4;

		pSampleSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSampleSigInfo->pSigName,
			32,
			16,
			"System status",
			"ϵͳ״̬",
			"Bat cell cur",
			"ϵͳ״̬");

		pSampleSigInfo->iSigValueType = VAR_ENUM;
		pSampleSigInfo->szValueDisplayFmt = "3";

pSampleSigInfo->iStateNum = 2;
		pSampleSigInfo->pStateText = NEW(LANG_TEXT*, 2);
		pSampleSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSampleSigInfo->pStateText[0],
			32,
			16,
			"No Alarm",
			"����",
			"No Alarm",
			"����");
		pSampleSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSampleSigInfo->pStateText[1],
			32,
			16,
			"�澯",
			"���Բ�����",
			"Alarm",
			"�澯");

		pSampleSigInfo->byValueDisplayAttr = DISPLAY_LCD;

		pSampleSigInfo->fMinValidValue = 0;
		pSampleSigInfo->fMaxValidValue = 1;

		pStdEquipTypeInfo->iSetSigNum = 2;

		pSetSigInfo = NEW(SET_SIG_INFO,pStdEquipTypeInfo->iSetSigNum);

		ZERO_POBJS(pSetSigInfo, pStdEquipTypeInfo->iSetSigNum);

		pStdEquipTypeInfo->pSetSigInfo = pSetSigInfo;
		pSetSigInfo->iSigID = 5;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Auto/Man State",
			"���Զ�״̬",
			"Auto/Man State",
			"���Զ�״̬");

		pSetSigInfo->iSigValueType = VAR_ENUM;

		pSetSigInfo->iStateNum = 2;
		pSetSigInfo->pStateText = NEW(LANG_TEXT*, 2);
		pSetSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[0],
			32,
			16,
			"Auto",
			"�Զ�",
			"Auto",
			"�Զ�");
		pSetSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[1],
			32,
			16,
			"Manual",
			"�ֶ�",
			"Manual",
			"�ֶ�");

		pSetSigInfo->szValueDisplayFmt = "4";

		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;
		

		pSetSigInfo->fMinValidValue = 0;
		pSetSigInfo->fMaxValidValue = 1;

		pSetSigInfo++;

		pSetSigInfo->iSigID = 6;

		pSetSigInfo->pSigName = NEW(LANG_TEXT,1);
		ConstructLangText(pSetSigInfo->pSigName,
			32,
			16,
			"Battery Status",
			"��ع���״̬",
			"Battery Status",
			"��ع���״̬");

		pSetSigInfo->iSigValueType = VAR_ENUM;

		pSetSigInfo->iStateNum = 3;
		pSetSigInfo->pStateText = NEW(LANG_TEXT*, pSetSigInfo->iStateNum);
		pSetSigInfo->pStateText[0] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[0],
			32,
			16,
			"Float",
			"����",
			"Float",
			"����");
		pSetSigInfo->pStateText[1] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[1],
			32,
			16,
			"BC",
			"����",
			"BC",
			"����");

		pSetSigInfo->pStateText[2] = NEW(LANG_TEXT, 1);
		ConstructLangText(pSetSigInfo->pStateText[2],
			32,
			16,
			"Test",
			"����",
			"Test",
			"����");

		pSetSigInfo->szValueDisplayFmt = "4";

		pSetSigInfo->byValueDisplayAttr = DISPLAY_LCD;


		pSetSigInfo->fMinValidValue = 0;
		pSetSigInfo->fMaxValidValue = 2;
		
	}

	//Has six equip:battery,rectifier and general controller, two Batt, system
	{
		EQUIP_INFO				*pEquipInfo;	

		SAMPLE_SIG_VALUE	*pSampleSigValue;	 
		CTRL_SIG_VALUE		*pCtrlSigValue;	 
		SET_SIG_VALUE		*pSetSigValue;	 
		ALARM_SIG_VALUE		*pAlarmSigValue;	
		ALARM_SIG_VALUE		*pAlarmSigValue1;	

		g_SiteInfo.iEquipNum = 6;	

		pEquipInfo = NEW(EQUIP_INFO,g_SiteInfo.iEquipNum);

		ZERO_POBJS(pEquipInfo, g_SiteInfo.iEquipNum);


		g_SiteInfo.pEquipInfo = pEquipInfo;

		//�����
		pEquipInfo->iEquipID = 51;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"Battery group",
			"������",
			"Bat grp",
			"�����");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 0;

		pSampleSigValue = NEW(SAMPLE_SIG_VALUE,pEquipInfo->pStdEquip->iSampleSigNum);
		ZERO_POBJS(pSampleSigValue, pEquipInfo->pStdEquip->iSampleSigNum);

		pEquipInfo->pSampleSigValue = pSampleSigValue;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 0;

		pSampleSigValue->bv.varValue.fValue = 500;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 1;

		pSampleSigValue->bv.varValue.fValue = 52.7;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 2;

		pSampleSigValue->bv.varValue.fValue = 25;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		//active alarm
		//major alarm
		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2003 - 1900;
			startTime.tm_mon = 9 - 1;
			startTime.tm_mday = 27;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_MAJOR)->next = pAlarmSigValue;
		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		pAlarmSigValue->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_MAJOR) ;

		printf("pAlarmSigValue = %d pEquipInfo = %d \r\n ", pAlarmSigValue, pAlarmSigValue->pEquipInfo);

		//ģ��
		pEquipInfo++;

		pEquipInfo->iEquipID = 2;

		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"Rectifier group",
			"����ģ��",
			"Rect grp",
			"ģ��");


		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 1;

		pCtrlSigValue = NEW(CTRL_SIG_VALUE,pEquipInfo->pStdEquip->iCtrlSigNum);
		ZERO_POBJS(pCtrlSigValue, pEquipInfo->pStdEquip->iCtrlSigNum);

		pEquipInfo->pCtrlSigValue = pCtrlSigValue;

		pCtrlSigValue->pStdSig = pEquipInfo->pStdEquip->pCtrlSigInfo + 0;

		pCtrlSigValue->bv.varValue.lValue = 90;

		pCtrlSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pCtrlSigValue++;

		pCtrlSigValue->pStdSig = pEquipInfo->pStdEquip->pCtrlSigInfo + 1;

		pCtrlSigValue->bv.varValue.fValue = 53.5;

		pCtrlSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		//active alarm

		//major alarm
		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;


			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2004 - 1900;
			
			startTime.tm_mon = 5 - 1;
			startTime.tm_mday = 9;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_MAJOR)->next = pAlarmSigValue;
		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 1;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		pAlarmSigValue->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_MAJOR) ;

		//critical alarm
		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2004 - 1900;
			startTime.tm_mon = 5 - 1;
			startTime.tm_mday = 8;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_CRITICAL)->next = pAlarmSigValue;


		//Obervation alarm
		pAlarmSigValue1 = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2003 - 1900;
			startTime.tm_mon = 06 - 1;
			startTime.tm_mday = 21;

			pAlarmSigValue1->sv.tmStartTime = mktime(&startTime);
		}

		pAlarmSigValue1->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue1->pEquipInfo = pEquipInfo;

		pAlarmSigValue->next = pAlarmSigValue1;

		pAlarmSigValue1->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_CRITICAL);


		//����������
		pEquipInfo++;

		pEquipInfo->iEquipID = 58;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,		
			32,
			16,
			"ACU base controller",
			"ACU����������",
			"Base controller",
			"����������");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 2;

		pSetSigValue = NEW(SET_SIG_VALUE,pEquipInfo->pStdEquip->iSetSigNum);
		ZERO_POBJS(pSetSigValue, pEquipInfo->pStdEquip->iSetSigNum);

		pEquipInfo->pSetSigValue = pSetSigValue;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 0;

		pSetSigValue->bv.varValue.enumValue = 1;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSetSigValue++;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 1;

		pSetSigValue->bv.varValue.enumValue = 1;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSetSigValue++;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 2;

		pSetSigValue->bv.varValue.ulValue = 320;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		//active alarm
		//observation alarm
		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2003 - 1900;
			startTime.tm_mon = 06 - 1;
			startTime.tm_mday = 19;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_OBSERVATION)->next = pAlarmSigValue;
		pAlarmSigValue->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_OBSERVATION) ;

		
		pEquipInfo++;
		
		//���1
		pEquipInfo->iEquipID = 100;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"Battery 1",
			"���1",
			"Bat 1",
			"���1");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 3;

		pSampleSigValue = NEW(SAMPLE_SIG_VALUE,pEquipInfo->pStdEquip->iSampleSigNum);
		ZERO_POBJS(pSampleSigValue, pEquipInfo->pStdEquip->iSampleSigNum);

		pEquipInfo->pSampleSigValue = pSampleSigValue;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 0;

		pSampleSigValue->bv.varValue.fValue = 500;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 1;

		pSampleSigValue->bv.varValue.fValue = 52.7;

		//��Ч֮
	//	pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 2;

		pSampleSigValue->bv.varValue.fValue = 25;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 3;

		pSampleSigValue->bv.varValue.fValue = 150;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 4;

		pSampleSigValue->bv.varValue.fValue = 53.7;

		//��Ч֮
	//	pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		//critical alarm

		pAlarmSigValue = NEW(ALARM_SIG_VALUE,1);

		{
			struct tm startTime;

			memset(&startTime, 0, sizeof(startTime));

			startTime.tm_year = 2003 - 1900;
			startTime.tm_mon = 9 - 1;
			startTime.tm_mday = 28;

			pAlarmSigValue->sv.tmStartTime = mktime(&startTime);
		}

		pAlarmSigValue->pStdSig = pEquipInfo->pStdEquip->pAlarmSigInfo + 0;
		pAlarmSigValue->pEquipInfo = pEquipInfo;
		(pEquipInfo->pActiveAlarms + ALARM_LEVEL_CRITICAL)->next = pAlarmSigValue;
		pAlarmSigValue->next = (pEquipInfo->pActiveAlarms + ALARM_LEVEL_CRITICAL) ;

		pEquipInfo++;


		//���2
		pEquipInfo->iEquipID = 103;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"Battery 2",
			"���2",
			"Bat 2",
			"���2");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 3;

		pSampleSigValue = NEW(SAMPLE_SIG_VALUE,pEquipInfo->pStdEquip->iSampleSigNum);
		ZERO_POBJS(pSampleSigValue, pEquipInfo->pStdEquip->iSampleSigNum);

		pEquipInfo->pSampleSigValue = pSampleSigValue;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 0;

		pSampleSigValue->bv.varValue.fValue = 400;

		//��Ч֮
	//	pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 1;

		pSampleSigValue->bv.varValue.fValue = 53.0;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 2;

		pSampleSigValue->bv.varValue.fValue = 28;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 3;

		pSampleSigValue->bv.varValue.fValue = 250;

		//��Ч֮
	//	pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 4;

		pSampleSigValue->bv.varValue.fValue = 55.7;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pEquipInfo++;
		//ϵͳ�豸
		pEquipInfo->iEquipID = 1;
		pEquipInfo->pEquipName = NEW(LANG_TEXT,1);
		ConstructLangText(pEquipInfo->pEquipName,
			32,
			16,
			"ACU Site",
			"ACUվ",
			"ACU Site",
			"ACUվ");

		pEquipInfo->pStdEquip = g_SiteInfo.pStdEquipTypeInfo + 4;

		pSampleSigValue = NEW(SAMPLE_SIG_VALUE,pEquipInfo->pStdEquip->iSampleSigNum);
		ZERO_POBJS(pSampleSigValue, pEquipInfo->pStdEquip->iSampleSigNum);

		pEquipInfo->pSampleSigValue = pSampleSigValue;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 0;

		pSampleSigValue->bv.varValue.fValue = 53.5;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 1;

		pSampleSigValue->bv.varValue.fValue = 500;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 2;

		pSampleSigValue->bv.varValue.ulValue = 70;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

		pSampleSigValue++;

		pSampleSigValue->pStdSig = pEquipInfo->pStdEquip->pSampleSigInfo + 3;

		pSampleSigValue->bv.varValue.enumValue = 0;

		pSampleSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSetSigValue = NEW(SET_SIG_VALUE,pEquipInfo->pStdEquip->iSetSigNum);
		ZERO_POBJS(pSetSigValue, pEquipInfo->pStdEquip->iSetSigNum);

		pEquipInfo->pSetSigValue = pSetSigValue;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 0;

		pSetSigValue->bv.varValue.enumValue = 1;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;


		pSetSigValue++;

		pSetSigValue->pStdSig = pEquipInfo->pStdEquip->pSetSigInfo + 1;

		pSetSigValue->bv.varValue.enumValue = 2;

		pSetSigValue->bv.usStateMask = VALUE_STATE_IS_VALID;

	}

	//Active alarm count of site
	g_SiteInfo.iAlarmCount[ALARM_LEVEL_NONE] = 6;
	g_SiteInfo.iAlarmCount[ALARM_LEVEL_OBSERVATION] = 1;
	g_SiteInfo.iAlarmCount[ALARM_LEVEL_MAJOR] = 2;
	g_SiteInfo.iAlarmCount[ALARM_LEVEL_CRITICAL] = 3;

	return TRUE;
}

BOOL ConstructLangTextInclueRes(LANG_TEXT*	pLangText,
					   int			iMaxLenForFull,
					   int			iMaxLenForAbbr,
					   char*		pFullName0,
					   char*		pFullName1,
					   char*		pAbbrName0,
					   char*		pAbbrName1,
					   int			iResourceID)

{
	char* pUseString;


	if (pLangText)
	{
		//	pLangText->iResourceID = 20;
		pLangText->iMaxLenForFull = iMaxLenForFull;	
		pLangText->iMaxLenForAbbr = iMaxLenForAbbr;	

		pUseString = NEW(char, pLangText->iMaxLenForFull);
		strncpyz(pUseString, pFullName0, pLangText->iMaxLenForFull);
		pLangText->pFullName[0] = pUseString;	

		pUseString = NEW(char, pLangText->iMaxLenForFull);
		strncpyz(pUseString, pFullName1, pLangText->iMaxLenForFull);
		pLangText->pFullName[1] = pUseString;

		pUseString = NEW(char, pLangText->iMaxLenForAbbr);
		strncpyz(pUseString, pAbbrName0, pLangText->iMaxLenForAbbr);
		pLangText->pAbbrName[0] = pUseString;	

		pUseString = NEW(char, pLangText->iMaxLenForAbbr);
		strncpyz(pUseString, pAbbrName1, pLangText->iMaxLenForAbbr);
		pLangText->pAbbrName[1] = pUseString;

		pLangText->iResourceID = iResourceID;
	}

	return TRUE;
}

int LoadLangResourseConfig(IN char *szLangResourceFile, LANG_FILE *pLangFile)
{
	pLangFile->iLangTextNum = 24;

	pLangFile->pLangText = NEW(LANG_TEXT,pLangFile->iLangTextNum);

	ConstructLangTextInclueRes(pLangFile->pLangText,
		32,
		16,
		"Please select user",
		"��ѡ���û�",
		"select user",
		"ѡ���û�",
		PASSWORD_DLG_USER_ID);

	ConstructLangTextInclueRes(pLangFile->pLangText + 1,
		32,
		16,
		"Please input password",
		"����������",
		"Input password",
		"��������",
		PASSWORD_DLG_PWD_ID);

	ConstructLangTextInclueRes(pLangFile->pLangText + 2,
		32,
		16,
		"Site ID",
		"��վID",
		"Site ID",
		"��վID",
		SYS_SITE_ID_SET_ID);

	ConstructLangTextInclueRes(pLangFile->pLangText + 3,
		32,									 
		16,									 
		"Language",							 
		"��������",							 
		"Language",							 
		"��������",							 
		SYS_LANGUAGE_SET_ID);				 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 4,
		32,									 
		16,									 
		"System Date",						 
		"ϵͳ����",							 
		"System Date",						 
		"ϵͳ����",							 
		SYS_DATE_SET_ID);					 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 5,
		32,									
		16,									 
		"System Time",						 
		"ϵͳʱ��",							 
		"System Time",						 
		"ϵͳʱ��",	
		SYS_TIME_SET_ID);					 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 6,
		32,									 
		16,									 
		"IP Address",						 
		"IP��ַ",							   
		"IP Address",						 
		"IP��ַ",							   
		SYS_IP_ADDR_SET_ID);				 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 7,
		32,									 
		16,									 
		"Sub Mask",							 
		"��������",							 
		"Sub Mask",							 
		"��������",							 
		SYS_NETMASK_SET_ID);				 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 8,
		32,									 
		16,									 
		"Default Gateway",					 
		"Ĭ������",							 
		"Default Gateway",					 
		"Ĭ������",							 
		SYS_GATEWAY_SET_ID);				 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 9,
		32,									 
		16,									 
		"Main Menu",						 
		"���˵�",							  
		"Main Menu",						 
		"���˵�",							  
		MAIN_MENU_LANG_ID);					 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 10,
		32,									 
		16,									 
		"Runtime Info",						 
		"������Ϣ",							 
		"Runtime Info",						 
		"������Ϣ",							 
		MAIN_RUNINFO_LANG_ID);				 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 11,
		32,									 
		16,									 
		"Parameter Set",					 
		"��������",							 
		"Parameter Set",					 
		"��������",							 
		MAIN_SET_LANG_ID);					 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 12,
		32,									 
		16,									 
		"Device Control",					 
		"�豸����",							 
		"Device Control",					 
		"�豸����",							 
		MAIN_CONTROL_LANG_ID);				 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 13,
		32,									 
		16,									 
		"System set",						 
		"ϵͳ����",							 
		"System set",						 
		"ϵͳ����",							 
		SYSTEM_SET_LANG_ID);				 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 14,
		32,									 
		16,									 
		"English",							 
		"Ӣ��",							   
		"English",							 
		"Ӣ��",							   
		ENGLISH_LANGUAGE_LANG_ID);			 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 15,
		32,									 
		16,									 
		"Chinese",							 
		"����",							   
		"Chinese",							 
		"����",							   
		LOCAL_LANGUAGE_LANG_ID);			 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 16,
		32,									 
		16,									 
		"Prompt Info",						 
		"��ʾ��Ϣ",							 
		"Prompt Info",						 
		"��ʾ��Ϣ",							 
		HEADING_PROMPT_INFO_LANG_ID);		 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 17,
		32,									 
		16,									 
		"Error password",					 
		"�������",							 
		"Error password",					 
		"�������",							 
		ERR_PASSWORD_PROMPT_LANG_ID);		 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 18,
		32,									 
		16,									 
		"Any key return",					 
		"���������",						
		"Any key return",					 
		"���������",						
		ANY_KEY_RET_PROMPT_LANG_ID);		 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 19,
		32,									 
		16,									 
		"Acknowledge info",					 
		"ȷ����Ϣ",							 
		"Acknowledge info",					 
		"ȷ����Ϣ",							 
		HEADING_ACKNOWLEDGE_INFO_LANG_ID);	 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 20,
		32,									 
		16,									 
		"ENT key confirm",					 
		"ENT��ȷ��",						  
		"ENT key confirm",					 
		"ENT��ȷ��",						  
		ENT_KEY_ACKNOWLEDGE_LANG_ID);		 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 21,
		32,									 
		16,									 
		"Other key cancel",					 
		"������ȡ��",						
		"Other key cancel",					 
		"������ȡ��",						
		OTHER_KEY_RET_PROMPT_LANG_ID);		 
											 
	ConstructLangTextInclueRes(pLangFile->pLangText + 22,
		32,
		16,
		"No Privilege",
		"���㹻Ȩ��",
		"No Privilege",
		"���㹻Ȩ��",
		NOT_ENOUGH_PRIVILEGE_LANG_ID);

	ConstructLangTextInclueRes(pLangFile->pLangText + 23,
		32,
		16,
		"No item info",
		"��������Ϣ",
		"No item info",
		"��������Ϣ",
		NO_ITEM_INFO_LANG_ID);

	return 0;
}

/*==========================================================================*
* FUNCTION : GetLCDLangText
* PURPOSE  : get Lang Text structure by ResourceID
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int         iResourceID : key value
*			  IN int		 iLen        : Lang Text list length
*            IN LANG_TEXT  *pLangText   : Lang Text list to search
* RETURN   : LANG_TEXT * : the searched element, NULL for fail
* COMMENTS : we think the list has ascending order ..
* CREATOR  : LinTao                   DATE: 2004-09-14 09:37
*==========================================================================*/
LANG_TEXT *GetLCDLangText(IN int iResourceID, IN int iLen, IN LANG_TEXT *pLangTextList)
{
	int i, head = 0, tail = iLen-1;

	ASSERT(pLangTextList);

	if (iLen <= 0)
	{
		return NULL;
	}

	do
	{
		i = (head + tail)/2;

		if (pLangTextList[i].iResourceID > iResourceID)
		{
			tail = i - 1;
		}

		else
		{
			head = i + 1;
		}
	}
	while(pLangTextList[i].iResourceID != iResourceID && head <= tail);

	if (pLangTextList[i].iResourceID == iResourceID)
	{
		return &pLangTextList[i];
	}

	else
	{
		return NULL;
	}
}

extern USER_INFO_STRU g_UserManage[];
extern int			  g_nUserNumber;

void InitUserManageInfo(void)
{
	printf("filename: test_lcd_module.c [InitUserManageInfo]\r\n");

TRACEX("InitUserManageInfo\n");
	//user1, should be administrator 
	g_UserManage[0].byLevel = ADMIN_LEVEL;

	strncpyz(g_UserManage[0].szUserName, ADMIN_DEFAULT_NAME,
		sizeof(g_UserManage[0].szUserName));

	strncpyz(g_UserManage[0].szPassword, ADMIN_DEFAULT_PASSWORD,
		sizeof(g_UserManage[0].szPassword));
TRACEX("InitUserManageInfo\n");


	//user2, hulw
	g_UserManage[1].byLevel = OPERATOR_LEVEL;

	strncpyz(g_UserManage[1].szUserName, "hulw",
		sizeof(g_UserManage[1].szUserName));

	strncpyz(g_UserManage[1].szPassword, "hello",
		sizeof(g_UserManage[1].szPassword));

	g_nUserNumber = 2;
TRACEX("InitUserManageInfo\n");

}


/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_main.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:55
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_MAIN_H__041008
#define __LCD_MAIN_H__041008



#ifdef  __cplusplus
extern "C" {
#endif


#include "lcd_interface.h"

#define ONE_HOUR_MSECOND		(60*60*1000)
#define GETKEY_TIME_INTERVAL		50//ms
#define HANDLEIDLE_TIME_INTERVAL	80//ms
#define PROMPT_INFO_TIMEOUT			5000//ms

#define MINI_NCU_MAX_HIS_ALARM_COUNT			500
#define MINI_NCU_MAX_ACT_ALARM_COUNT			200


//please keep the same with struct tagSiteInfo in cfg_model.h
#define LUI_PRODUCTINFO_NUM_I2C					8	//I2C Device
//RS485, including SMIO*8, SMAC*4, SMBAT*12, Salve1~3
#define LUI_PRODUCTINFO_NUM_RS485				PRODUCT_INFO_485PRODUCTINFO_MAX
#define LUI_PRODUCTINFO_NUM_CAN_SMDUP			8//SCUPlus Device
#define LUI_PRODUCTINFO_NUM_CAN_CONVERTER		60//Converter
#define LUI_PRODUCTINFO_NUM_CAN_SMTEMP			8//SMTemp
//#define LUI_PRODUCTINFO_NUM_CAN_LiBatt		60//LiBatt
//#define LUI_PRODUCTINFO_NUM_CAN_LiBridge		1//CANLiBridge
#define LUI_PRODUCTINFO_NUM_CAN_MPPT			30
#define LUI_PRODUCTINFO_NUM_CAN_RECT			120
#define LUI_PRODUCTINFO_NUM_CAN_SMDU			8
#define LUI_PRODUCTINFO_NUM_CAN_SMDUH			8
#define LUI_PRODUCTINFO_NUM_CAN_DCEM			8


enum	LUI_PRODUCTINFO_INDEX_ENUM
{
	LUI_PRODUCTINFO_START_I2C = 1,
	LUI_PRODUCTINFO_END_I2C = LUI_PRODUCTINFO_START_I2C + LUI_PRODUCTINFO_NUM_I2C - 1,
	LUI_PRODUCTINFO_START_RS485,
	LUI_PRODUCTINFO_END_RS485 = LUI_PRODUCTINFO_START_RS485 + LUI_PRODUCTINFO_NUM_RS485 - 1,
	LUI_PRODUCTINFO_START_CAN_SMDUP,
	LUI_PRODUCTINFO_END_CAN_SMDUP = LUI_PRODUCTINFO_START_CAN_SMDUP + LUI_PRODUCTINFO_NUM_CAN_SMDUP - 1,
	LUI_PRODUCTINFO_START_CAN_CONVERTER,
	LUI_PRODUCTINFO_END_CAN_CONVERTER = LUI_PRODUCTINFO_START_CAN_CONVERTER + LUI_PRODUCTINFO_NUM_CAN_CONVERTER - 1,
	LUI_PRODUCTINFO_START_CAN_SMTEMP,
	LUI_PRODUCTINFO_END_CAN_SMTEMP = LUI_PRODUCTINFO_START_CAN_SMTEMP + LUI_PRODUCTINFO_NUM_CAN_SMTEMP - 1,
	LUI_PRODUCTINFO_START_CAN_MPPT,
	LUI_PRODUCTINFO_END_CAN_MPPT = LUI_PRODUCTINFO_START_CAN_MPPT + LUI_PRODUCTINFO_NUM_CAN_MPPT - 1,
	LUI_PRODUCTINFO_START_CAN_RECT,
	LUI_PRODUCTINFO_END_CAN_RECT = LUI_PRODUCTINFO_START_CAN_RECT + LUI_PRODUCTINFO_NUM_CAN_RECT - 1,
	LUI_PRODUCTINFO_START_CAN_SMDU,
	LUI_PRODUCTINFO_END_CAN_SMDU = LUI_PRODUCTINFO_START_CAN_SMDU + LUI_PRODUCTINFO_NUM_CAN_SMDU - 1,
	LUI_PRODUCTINFO_START_CAN_SMDUH,
	LUI_PRODUCTINFO_END_CAN_SMDUH = LUI_PRODUCTINFO_START_CAN_SMDUH + LUI_PRODUCTINFO_NUM_CAN_SMDUH - 1,
	LUI_PRODUCTINFO_START_CAN_DCEM,
	LUI_PRODUCTINFO_END_CAN_DCEM = LUI_PRODUCTINFO_START_CAN_DCEM + LUI_PRODUCTINFO_NUM_CAN_DCEM - 1,
	LUI_PRODUCTINFO_TOTAL_NUM
};


enum	DIVIDED_SUB_MENU_POSITION
{
	DIVIDED_MENU_SIBLING = 0,
	DIVIDED_MENU_CHILDREN = 1,
	DIVIDED_MENU_UNDER_FIX = 2,

};

/* config file names */
//#define CONFIG_FILE_LCD_PRIVATE		"private/lcd/lcd_private.cfg"
#define CONFIG_FILE_LCD_PRIVATE		"private/lcd_mini/lcd_private.cfg"
#define RESOURCE_FILE_LCD_PRIVATE	"lcd_private.res"

	//Config sections name
//Private config
#define SIGNAL_DIS_ON_DEF_SCREEN_INFO_128		"[SIGNAL_DIS_ON_DEF_SCREEN_INFO_128]"
#define SIGNAL_DIS_ON_DEF_SCREEN_INFO_64		"[SIGNAL_DIS_ON_DEF_SCREEN_INFO_64]"
#define SIGNAL_DIS_ON_DEF_SCREEN_INFO_32		"[SIGNAL_DIS_ON_DEF_SCREEN_INFO_32]"
#define SIGNAL_DIS_ON_DEF_SCREEN_INFO_32_SLAVE	"[SIGNAL_DIS_ON_DEF_SCREEN_INFO_32_SLAVE]"

#define SIGNALS_OF_DIVIDED_SUB_MENU_ITEM		"[SIGNALS_OF_DIVIDED_SUB_MENU_ITEM]"
#define SIGNALS_OF_DIVIDED_SUB_MENU_INFO		"[SIGNALS_OF_DIVIDED_SUB_MENU_INFO]"

#define INFO_OF_CHANGE_MENU_ITEM_RESOUCE		"[CHANGED_MENU_ITEM_RESOURCE]"

//Language Resource
#define SELF_DEFINE_LANGUAGE_ITEM_NUMBER		"[SELF_DEFINE_LANGUAGE_ITEM_NUMBER]"
#define SELF_DEFINE_LANGUAGE_ITEM_INFO			"[SELF_DEFINE_LANGUAGE_ITEM_INFO]"

#define	RES_ID_VIRTUAL_GROUP_EQUIP_NAME				"[VIRTUAL_GROUP_EQUIP_NAME]"

#define			EIB_NUM_MAX		4
#define			MAX_SLAVE_NUM	3
typedef	struct tagActEquipNum
{
	int		nRectNum;
	int		anSlaveRectNum[MAX_SLAVE_NUM];
	int		nConvterNum;
	int		nSMDUNum;
	int		nMBBattNum;
	int		anEIBBattNum[EIB_NUM_MAX];
}ACT_EQUIP_NUM;


#define		DIVIDED_SUB_MENU_CHARS_MAX		512
typedef struct tagDividedSubMenuItemCfg
{
	int		nSubMenuItemIndex;

	int		nResouceID;

	int		nEquipID;
	int		nSigType;

	char	caSigID[DIVIDED_SUB_MENU_CHARS_MAX];

}DIVIDED_SUB_MENU_ITEM_CFG;

typedef struct tagDividedSubMenuItemCfgReader
{
	int						nDividedSubMenuItemNum;

	DIVIDED_SUB_MENU_ITEM_CFG*	pDividedSubMenuItemCfg;

}DIVIDED_SUB_MENU_ITEM_CFG_READER;


typedef struct tagDividedSubMenuInfoCfg
{
	int		nSubMenuInfoIndex;

	int		nEquipID;
	int		nSigType;

	int		nPositionFlag;

	char	caMenuItemIndex[DIVIDED_SUB_MENU_CHARS_MAX];

}DIVIDED_SUB_MENU_INFO_CFG;

typedef struct tagDividedSubMenuInfoCfgReader
{
	int						nDividedSubMenuInfoNum;

	DIVIDED_SUB_MENU_INFO_CFG*	pDividedSubMenuInfoCfg;

}DIVIDED_SUB_MENU_INFO_CFG_READER;


typedef struct tagChangeMenuItemResouceCfg
{
	int		nIndex;

	int		nEquipID;
	int		nSigType;

	int		nResouceID;

}CHANGE_MENU_ITEM_RESOURCE_CFG;

typedef struct tagChangeMenuItemResouceCfgReader
{
	int	nChgMenuItemResNum;

	CHANGE_MENU_ITEM_RESOURCE_CFG	*pChgMenuItemResCfg;

}CHANGE_MENU_ITEM_RESOURCE_CFG_READER;


typedef	struct	tagVirtualGroupEquipNameCfg
{
	int		nIndex;
	int		nEquipTypeId;
	int		nResouceID;
}VIRTUAL_GROUP_EQUIP_NAME_CFG;

typedef	struct	tagVirtualGroupName
{
	int		nVirGroupEquipNameResNum;
	VIRTUAL_GROUP_EQUIP_NAME_CFG	*pVirGroupEquipNameResCfg;
}VIRTUAL_GROUP_EQUIP_NAME_CFG_READER;


#define MINI_NCU_LANGUAGE_MAX_NUM				15
#define MINI_NCU_LANG_TEXT_MAX_LEN				64
typedef	struct	tagLangSupportTableItem
{
	char*		pszLangCode;
	char		szLocalAbbrName[MINI_NCU_LANG_TEXT_MAX_LEN];
	char		szLocalPrompt1[MINI_NCU_LANG_TEXT_MAX_LEN];
	char		szLocalPrompt2[MINI_NCU_LANG_TEXT_MAX_LEN];
}LANG_SUPPORT_TABLE_ITEM;

typedef	struct	tagLangSupportTable
{
	int							nLangSupportNum;
	LANG_SUPPORT_TABLE_ITEM		staLangSupportTableItem[MINI_NCU_LANGUAGE_MAX_NUM];
}LANG_SUPPORT_TABLE;

//Used to parse private config 
typedef struct tagLcdPrivateCfgLoader
{
	LANG_FILE					stLCDLangFile;
	
	//Frank Wu,20160127, for MiniNCU
	LANG_SUPPORT_TABLE			stLCDLangSupportTable;

	DEFAULT_SCREEN_CFG_READER	stDefaultScreenCfgReader128;
	DEFAULT_SCREEN_CFG_READER	stDefaultScreenCfgReader64;
	DEFAULT_SCREEN_CFG_READER	stDefaultScreenCfgReader32;//just for non-slave mode
	DEFAULT_SCREEN_CFG_READER	stDefaultScreenCfgReader32_slave;//just for slave mode

	DIVIDED_SUB_MENU_ITEM_CFG_READER	stDividedSubMenuItemCfgReader;
	DIVIDED_SUB_MENU_INFO_CFG_READER	stDividedSubMenuInfoCfgReader;

	CHANGE_MENU_ITEM_RESOURCE_CFG_READER stChgMenuItemResCfgReader;

	VIRTUAL_GROUP_EQUIP_NAME_CFG_READER		stVirGroupEquipNameCfgReader;
}LCD_PRIVATE_CFG_LOADER;



//Used to parse equips and signals
//The structure will be convenient to build menu
typedef struct tagSignaInfo
{
	int					iSampleSigNum;	 
	SAMPLE_SIG_INFO**	ppSampleSigInfo;	

	int 				iCtrlSigNum;	 
	CTRL_SIG_INFO**		ppCtrlSigInfo;	

	int					iSetSigNum;	 
	SET_SIG_INFO**		ppSetSigInfo;	 

	int					iAlarmSigNum;	 
	ALARM_SIG_INFO**	ppAlarmSigInfo; 

}SIGNAL_INFO;

typedef struct tagStdEquipInfo
{
	int				nEquipType;
	SIGNAL_INFO		stSignalInfo;

	int				nActEquipNum;	
	EQUIP_INFO**	ppActualEquip;	

}STANDARD_EQUIP_INFO;

typedef struct tagClassEquipInfo
{
	int			nStdEquipNum;
	STANDARD_EQUIP_INFO**	ppStandardEquipInfo;

	int			nGroupEquipType;

}CLASS_EQUIP_INFO;

typedef struct tagParseEquipInfo
{
	int			nClassEquipNum;
	CLASS_EQUIP_INFO* pClassEquipInfo;

	int			nStdEquipNum;				
	STANDARD_EQUIP_INFO *pStdEquipInfo;	//Record the StdEquipNodeList

}PARSE_EQUIP_INFO;


//////////////////////////////////////////////////////////////////////////

typedef struct tagDispActEquipInfo
{
	SIGNAL_INFO	*pSignalInfo;
	EQUIP_INFO	*pEquipInfo;

}DISP_ACTUAL_EQUIP_NODE;

typedef struct tagDispClassEquipInfo
{
	int			nDispActEquipNum;
	DISP_ACTUAL_EQUIP_NODE* pDispActEquipNode;

	int			nGroupEquipType;
	EQUIP_INFO	*pGroupEquipInfo;

}DISP_CLASS_EQUIP_NODE;

typedef struct tagParseMenuNode
{
	int			nDispClassEquipNum[SIG_TYPE_MAX];	//SIG_TYPE_SAMPLING,SIG_TYPE_CONTROL,SIG_TYPE_SETTING,SIG_TYPE_ALARM
	DISP_CLASS_EQUIP_NODE	*pDispClassEquipNode[SIG_TYPE_MAX];

}PARSE_MENU_NODE;

typedef struct tagAlarmSetEquipNode
{
	int			nAlarmSetEquipNum;
	STANDARD_EQUIP_INFO**	ppStandardEquipInfo;

}ALARM_SET_EQUIP_NODE;



#define MAX_PENDING_ACTIVE_ALARM_REPORT 64 

typedef struct	//	The active alarm value manager
{				
	ALARM_SIG_VALUE_EX_LCD 	*pAlarmList;	//	The alarm list, must be the first field!

	int				nAlarmCount;	//	The total alarm count

	// max active alarm numbers in queue, see MAX_PENDING_TRAP_SEND
	HANDLE			hActiveAlarmReportQueue;

	BOOL			bAllAlarmAckFlag;


}ACTIVE_ALARM_MANAGER;			

void* AddHistoryAlarmItems(MENU_ITEM* pEquipItem);

DWORD LUI_GetDwordSigValue(int nEquipId, int nSignalType, int nSignalId);

int RefreshSubMenuStatus(SUB_MENU *pSubMenu, BOOL bRecursive);
extern int LCD_SwitchToNextIndex(BOOL bNeedInit, BOOL bForceSwitch, int iSwitchInterval, int iMaxIndex);
extern int DisNavigationInfo(MENU_DISPLAY* pMenuDisplay);
extern char* LCD_QuerySubmenuItemName(int nMenuItemID);
//extern int UTF8toGB2312(char *inbuf,int inlen,char *outbuf,int outlen);
extern PRODUCT_INFO* QueryProductInfo(int nDeviceID);
extern BOOL LUI_IsNeedRebootForSignal(int iEquipID, 
									  int iSigType, 
									  int iSigID);

#ifdef __cplusplus
}
#endif

#endif //__LCD_MAIN_H__041008





/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_menu_screen.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:56
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_MENU_SCREEN_H__041008
#define __LCD_MENU_SCREEN_H__041008

#ifdef  __cplusplus
extern "C" {
#endif

static int DisplaySubmenuItem(int nMenuItemID,
							  int x,
							  int y,
							  char* pLangCode, 
							  BOOL bCurItemFlag);

static int DisplaySubmenus(MENU_DISPLAY* pMenuDisplay);


#ifdef __cplusplus
}
#endif

#endif //__LCD_MENU_SCREEN_H__041008

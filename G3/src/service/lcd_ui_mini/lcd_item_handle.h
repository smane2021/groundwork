 /*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_item_handle.h
*  CREATOR  : HULONGWEN                DATE: 2004-11-19 11:17
*  VERSION  : V1.00
*  PURPOSE  : Get or set the data of all the menu items
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_ITEM_HANDLE_H__041008
#define __LCD_ITEM_HANDLE_H__041008

#ifdef  __cplusplus
extern "C" {
#endif

#define GET_PROMPT_INFO				0
#define GET_VALUE_FROM_SYSTEM		1
#define GET_VALUE_FROM_EDITING		2

	typedef struct tagDisValuePreInfo
	{
		int			iSigValueType;
		VAR_VALUE	vdData;
		char		*szValueDisplayFmt;		
		char		*szSigUnit;	

		float		fMinValidValue;		
		float		fMaxValidValue;		


		int			iStateNum;				/* state count for Enumeration sig value */
		LANG_TEXT	**pStateText;

		int					iSectionNum;			//For VAR_MULTI_SECT type
		SECTION_DATA_STRU*	pSectionDataStru;//Used to set ip and date, time

		char*		pString; //for VAR_STRING and VAR_PHONE_NO
	}DIS_VALUE_PRE_INFO;


	//Item Handle Function
	typedef int (*DIS_VALUE_GET_PRE_INFO)(MENU_ITEM* pMenuItem, 
		DIS_VALUE_PRE_INFO* pDisValuePreInfo,
		int nGetType);

	typedef int (*ITEM_SET_FUNC)(MENU_ITEM* pMenuItem);

	typedef struct tagItemHandleFunc
	{
		int			nItemHandleID;
		DIS_VALUE_GET_PRE_INFO	*pfItemHandleGetFunc;
		ITEM_SET_FUNC		*pfItemHandleSetFunc;
	}ITEM_HANDLE_FUNC;

	extern ITEM_HANDLE_FUNC g_aItemHandleFunc[];

	extern const int ITEM_HANDLE_FUNC_NUM;



	extern int SetSignalInfo(MENU_ITEM* pMenuItem);
	extern int GetSignalPreInfo(MENU_ITEM* pMenuItem, DIS_VALUE_PRE_INFO* pDisValuePreInfo,	int nGetType);

	extern int SetAlarmLevelGetPreInfo(MENU_ITEM* pMenuItem, 
		DIS_VALUE_PRE_INFO* pDisValuePreInfo,
		int nGetType);
	extern int SetAlarmLevelSetFunc(MENU_ITEM* pMenuItem);

	extern int SetAlarmRelayGetPreInfo(MENU_ITEM* pMenuItem, 
		DIS_VALUE_PRE_INFO* pDisValuePreInfo,
		int nGetType);
	extern int SetAlarmRelaySetFunc(MENU_ITEM* pMenuItem);

	extern int GetProductPreInfo(MENU_ITEM* pMenuItem, DIS_VALUE_PRE_INFO* pDisValuePreInfo, int nGetType);



#ifdef __cplusplus
}
#endif

#endif //__LCD_ITEM_HANDLE_H__041008

/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_ack_dlg.c
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:57
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include "lcd_public.h"

#include "lcd_ack_dlg.h"
#include "lcd_main.h"

#define ACK_SLEEP_TIMEOUT 100//ms
#define Delay_Reboot_Time 2  //s

/*==========================================================================*
* FUNCTION : DisAckScreenInfo
* PURPOSE  : Display prompt info and acknowledge info on screen
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char*  pHeadingInfo : The first line info
*            char*  pPromptInfo1 : The second line info
*            char*  nPromptInfo2 : The third line info
*            char*  nPromptInfo3 : The fourth line info
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-06 10:10
*==========================================================================*/
static int DisAckScreenInfo(char* pHeadingInfo, 
							char* pPromptInfo1, 
							char* nPromptInfo2,
							char* nPromptInfo3)
{
	char*		pDisBuf;

	char*		pLangCode = GetCurrentLangCode();

	int			nFontHeight;
	int			nMaxRow;
	int			nInfoHeight;

	int			i;

	int nDrawWidth;

	int nX,nY;


	ASSERT(pLangCode);

	nFontHeight = GetFontHeightFromCode(pLangCode);
	//nInfoHeight = SCREEN_HEIGHT / 4;
	nInfoHeight = nFontHeight;
	nMaxRow = SCREEN_HEIGHT/nFontHeight;

	//Frank Wu,20160127, for MiniNCU
	//for(i = 0; i < 4; i++)
	for(i = 0; i < nMaxRow; i++)
	{
		nX = 0/*((i == 0) ? 0 : ARROW_WIDTH * 2)*/;
		//nY = ((int)(nMaxRow * i/4)) * nFontHeight;
		nY = i * nInfoHeight;

		//clear the blank before info
		ClearBar( 0,
			nY,
			nX,
			nInfoHeight,//nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);

		//Display one line of info
		pDisBuf = ((i == 0) ? pHeadingInfo
			: ((i == 1) ? pPromptInfo1 : 
		((i == 2) ? nPromptInfo2 : nPromptInfo3)));

		DisString(pDisBuf, 
			(int)strlen(pDisBuf),  
			nX,//Except the first line, front blank
			nY,
			pLangCode, 
			FLAG_DIS_NORMAL_TYPE);

		nDrawWidth = 
			GetWidthOfString(pDisBuf, (int)strlen(pDisBuf), pLangCode);

		//clear the blank after info
		ClearBar( nX + nDrawWidth,
			nY,
			SCREEN_WIDTH - (nX + nDrawWidth),
			nInfoHeight,//nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);
	}

	return ERR_LCD_OK;
}

static int DisAckScreenInfoByLang(char* pHeadingInfo, 
							char* pPromptInfo1, 
							char* nPromptInfo2,
							char* nPromptInfo3,
							char* pszLangCode,
							BOOL bJustOneLine)
{
	char*		pDisBuf;

	char*		pLangCode = NULL;

	int			nFontHeight;
	int			nMaxRow;
	int			nInfoHeight;

	int			i;

	int nDrawWidth;

	int nX,nY;

	if(NULL == pszLangCode)
	{
		pLangCode = GetCurrentLangCode();
	}
	else
	{
		pLangCode = pszLangCode;
	}

	ASSERT(pLangCode);

	nFontHeight = GetFontHeightFromCode(pLangCode);
	//nInfoHeight = SCREEN_HEIGHT / 4;
	nInfoHeight = nFontHeight;
	nMaxRow = SCREEN_HEIGHT/nFontHeight;

	if(bJustOneLine)
	{
		//display the text in the center of screen
		pDisBuf = pHeadingInfo;
		
		nDrawWidth = 
			GetWidthOfString(pDisBuf, (int)strlen(pDisBuf), pLangCode);

		nY = (SCREEN_HEIGHT - nFontHeight)/2;
		
		//clear the top of text area
		ClearBar(0, 0, SCREEN_WIDTH, nY, FLAG_DIS_NORMAL_TYPE);
		
		//clear the left of text area
		ClearBar(0, nY, (SCREEN_WIDTH - nDrawWidth)/2, nFontHeight, FLAG_DIS_NORMAL_TYPE);
			
		//display text
		DisString(pDisBuf, 
			(int)strlen(pDisBuf),  
			(SCREEN_WIDTH - nDrawWidth)/2,
			nY,
			pLangCode, 
			FLAG_DIS_NORMAL_TYPE);

		//clear the right of text area
		ClearBar((SCREEN_WIDTH + nDrawWidth)/2, nY, (SCREEN_WIDTH - nDrawWidth)/2, nFontHeight, FLAG_DIS_NORMAL_TYPE);

		//clear the bottom of text area
		ClearBar(0, nY + nFontHeight, SCREEN_WIDTH, nY, FLAG_DIS_NORMAL_TYPE);
	}
	else
	{
		//Frank Wu,20160127, for MiniNCU
		//for(i = 0; i < 4; i++)
		for(i = 0; i < nMaxRow; i++)
		{
			nX = 0/*((i == 0) ? 0 : ARROW_WIDTH * 2)*/;
			//nY = ((int)(nMaxRow * i/4)) * nFontHeight;
			nY = i * nInfoHeight;

			//clear the blank before info
			ClearBar( 0,
				nY,
				nX,
				nInfoHeight,//nFontHeight, 
				FLAG_DIS_NORMAL_TYPE);

			//Display one line of info
			pDisBuf = ((i == 0) ? pHeadingInfo
				: ((i == 1) ? pPromptInfo1 : 
				((i == 2) ? nPromptInfo2 : nPromptInfo3)));

			DisString(pDisBuf, 
				(int)strlen(pDisBuf),  
				nX,//Except the first line, front blank
				nY,
				pLangCode, 
				FLAG_DIS_NORMAL_TYPE);

			nDrawWidth = 
				GetWidthOfString(pDisBuf, (int)strlen(pDisBuf), pLangCode);

			//clear the blank after info
			ClearBar( nX + nDrawWidth,
				nY,
				SCREEN_WIDTH - (nX + nDrawWidth),
				nInfoHeight,//nFontHeight, 
				FLAG_DIS_NORMAL_TYPE);
		}
	}


	return ERR_LCD_OK;
}
/*==========================================================================*
* FUNCTION : DoModal_AcknowlegeDlg_ID_Reboot
* PURPOSE  : Display prompt info and acknowledge info,
*			  support sleep to timeout and key press quit modes
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  nHeadingInfoID : The first line info ID
*            int  nPromptInfoID1 : The second line info ID
*            int  nPromptInfoID2 : The third line info ID
*            int  nPromptInfoID3 : The fourth line info ID
*            int  nTimeout       : Sleep to timeout, then quit
* RETURN   : int : 
* COMMENTS : if ID == -1, Do not display
* CREATOR  : john                DATE: 2016-07-29 09:48
*==========================================================================*/
int DoModal_AcknowlegeDlg_ID_Reboot(int nHeadingInfoID, 
							 int nPromptInfoID1, 
							 int nPromptInfoID2,
							 int nPromptInfoID3,
							 int nTimeout)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	int nLangID;
	char* pDisBuf;

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	ASSERT(pLCDLangFile);

	int i = 0;

	for(i = 0; i < 4; i++)
	{
		switch(i)
		{
		case 0:
			nLangID = nHeadingInfoID;
			pDisBuf = szHeadingInfo;

			break;

		case 1:
			nLangID = nPromptInfoID1;
			pDisBuf = szPromptInfo1;

			break;

		case 2:
			nLangID = nPromptInfoID2;
			pDisBuf = szPromptInfo2;

			break;

		case 3:
			nLangID = nPromptInfoID3;
			pDisBuf = szPromptInfo3;

			break;

		default:
			break;
		}

		if(nLangID != -1)
		{
			pLangText = GetLCDLangText(
				nLangID, 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);

			if(pLangText)
			{
				strncpyz(pDisBuf, 
					pLangText->pAbbrName[nCurrentLangFlag],
					MAX_CHARS_OF_LINE);

			}
		}
	}
	
	 

	return DoModal_AcknowlegeDlg_INFO_Reboot(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		nTimeout);

}

/*==========================================================================*
* FUNCTION : DoModal_AcknowlegeDlg_ID
* PURPOSE  : Display prompt info and acknowledge info,
*			  support sleep to timeout and key press quit modes
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  nHeadingInfoID : The first line info ID
*            int  nPromptInfoID1 : The second line info ID
*            int  nPromptInfoID2 : The third line info ID
*            int  nPromptInfoID3 : The fourth line info ID
*            int  nTimeout       : Sleep to timeout, then quit
* RETURN   : int : 
* COMMENTS : if ID == -1, Do not display
* CREATOR  : HULONGWEN                DATE: 2004-12-07 14:40
*==========================================================================*/
int DoModal_AcknowlegeDlg_ID(int nHeadingInfoID, 
							 int nPromptInfoID1, 
							 int nPromptInfoID2,
							 int nPromptInfoID3,
							 int nTimeout)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	int nLangID;
	char* pDisBuf;

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	ASSERT(pLCDLangFile);

	int i = 0;

	for(i = 0; i < 4; i++)
	{
		switch(i)
		{
		case 0:
			nLangID = nHeadingInfoID;
			pDisBuf = szHeadingInfo;

			break;

		case 1:
			nLangID = nPromptInfoID1;
			pDisBuf = szPromptInfo1;

			break;

		case 2:
			nLangID = nPromptInfoID2;
			pDisBuf = szPromptInfo2;

			break;

		case 3:
			nLangID = nPromptInfoID3;
			pDisBuf = szPromptInfo3;

			break;

		default:
			break;
		}

		if(nLangID != -1)
		{
			pLangText = GetLCDLangText(
				nLangID, 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);

			if(pLangText)
			{
				strncpyz(pDisBuf, 
					pLangText->pAbbrName[nCurrentLangFlag],
					MAX_CHARS_OF_LINE);

			}
		}
	}

	return DoModal_AcknowlegeDlg_INFO(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		nTimeout);

}

int DoModal_AcknowlegeDlg_ID_ByLang(int nHeadingInfoID, 
							 int nPromptInfoID1, 
							 int nPromptInfoID2,
							 int nPromptInfoID3,
							 int nTimeout,
							 char *pszLangCode,
							 BOOL bJustOneLine,
							 BOOL bNeedProcessEvent)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	int nLangID;
	char* pDisBuf;

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	ASSERT(pLCDLangFile);

	int i = 0;

	for(i = 0; i < 4; i++)
	{
		switch(i)
		{
		case 0:
			nLangID = nHeadingInfoID;
			pDisBuf = szHeadingInfo;

			break;

		case 1:
			nLangID = nPromptInfoID1;
			pDisBuf = szPromptInfo1;

			break;

		case 2:
			nLangID = nPromptInfoID2;
			pDisBuf = szPromptInfo2;

			break;

		case 3:
			nLangID = nPromptInfoID3;
			pDisBuf = szPromptInfo3;

			break;

		default:
			break;
		}

		if(nLangID != -1)
		{
			pLangText = GetLCDLangText(
				nLangID, 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);

			if(pLangText)
			{
				strncpyz(pDisBuf, 
					pLangText->pAbbrName[nCurrentLangFlag],
					MAX_CHARS_OF_LINE);

			}
		}
	}

	return DoModal_AcknowlegeDlg_INFO_ByLang(szHeadingInfo, 
				szPromptInfo1, 
				szPromptInfo2,
				szPromptInfo3,
				nTimeout,
				pszLangCode,
				bJustOneLine,
				bNeedProcessEvent);

}
/*==========================================================================*
* FUNCTION : DoModal_AcknowlegeDlg_INFO_Reboot
* PURPOSE  : personally to control the reboot function
*			
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char*  pHeadingInfo : The first line info
*            char*  pPromptInfo1 : The second line info
*            char*  nPromptInfo2 : The third line info
*            char*  nPromptInfo3 : The fourth line info
*            int    nTimeout     : Sleep to timeout, then quit, 
*									if -1, key press quit mode
* RETURN   : int : should return ID_CANCEL, ID_RETURN, ID_GOTO_DEF_SCREEN, or ID_TIMEOUT_RETURN
* COMMENTS : 
* CREATOR  : JOHN               DATE: 2016-07-27 15:01
*==========================================================================*/
int DoModal_AcknowlegeDlg_INFO_Reboot(char* pHeadingInfo, 
							   char* pPromptInfo1, 
							   char* nPromptInfo2,
							   char* nPromptInfo3,
							   int	nTimeout)
{
	UCHAR		byKeyValue;
	int			iProcTime;

	time_t	tmTimeout = time(NULL);
	time_t	tmNow;

	int			nRet = ID_CANCEL; 

	BOOL bRefreshAtOnce = TRUE;

	if(IS_HIGH_SCREEN)
	{
		ClearScreen();
	}

	DisAckScreenInfo(pHeadingInfo, pPromptInfo1, 
		nPromptInfo2, nPromptInfo3);
	

	for(;;)
	{
		byKeyValue = GetKeyEx();
		
		iProcTime = GETKEY_TIME_INTERVAL;

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();

			switch( byKeyValue )
			{
			case VK_ESCAPE:  
				{
					return ID_CANCEL;	
				}				

			case VK_UP:  
			case VK_DOWN:
				{
					// No Operation
				}
				break;

			case VK_ENTER:   
				{		
						tmNow = time(NULL);
						if ((tmNow - tmTimeout) >= Delay_Reboot_Time)
						{
							printf("*********tmNow - tmTimeout=*****%d\n",tmNow - tmTimeout);
							return ID_RETURN;
						}else
						{
							printf("*wait please||tmNow - tmTimeout=*%d\n",tmNow - tmTimeout);
							break;
						}
								 	 
				}
			
			default:
				break;
			}

		}
		else
		{
			iProcTime += HANDLEIDLE_TIME_INTERVAL;

			nRet = HandleIdle();

			if (nRet == ID_GOTO_DEF_SCREEN)
			{
				return ID_GOTO_DEF_SCREEN;
			}
			//Because the LCD is initted, so display info again.
			else if (nRet == ID_INIT_LCD_JUST_NOW)
			{
				bRefreshAtOnce = TRUE;
			}
		}

		if (bRefreshAtOnce)
		{
			DisAckScreenInfo(pHeadingInfo, pPromptInfo1, 
					nPromptInfo2, nPromptInfo3);

			bRefreshAtOnce = FALSE;
		}

		
		if (nTimeout != -1)
		{
			nTimeout -= iProcTime;

			if (nTimeout > ACK_SLEEP_TIMEOUT)
			{
				Sleep(ACK_SLEEP_TIMEOUT);
				nTimeout -= ACK_SLEEP_TIMEOUT;
			}
			else if(nTimeout > 0)
			{
				Sleep((DWORD)nTimeout);

				return ID_TIMEOUT_RETURN;
			}
			else
			{
				return ID_TIMEOUT_RETURN;
			}
		}
	}
}




/*==========================================================================*
* FUNCTION : DoModal_AcknowlegeDlg_INFO
* PURPOSE  : Display prompt info and acknowledge info,
*			  support sleep to timeout and key press quit modes
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char*  pHeadingInfo : The first line info
*            char*  pPromptInfo1 : The second line info
*            char*  nPromptInfo2 : The third line info
*            char*  nPromptInfo3 : The fourth line info
*            int    nTimeout     : Sleep to timeout, then quit, 
*									if -1, key press quit mode
* RETURN   : int : should return ID_CANCEL, ID_RETURN, ID_GOTO_DEF_SCREEN, or ID_TIMEOUT_RETURN
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-07 14:33
*==========================================================================*/
int DoModal_AcknowlegeDlg_INFO(char* pHeadingInfo, 
							   char* pPromptInfo1, 
							   char* nPromptInfo2,
							   char* nPromptInfo3,
							   int	nTimeout)
{
	UCHAR		byKeyValue;
	int			iProcTime;

	int			nRet = ID_CANCEL; 

	BOOL bRefreshAtOnce = TRUE;

	if(IS_HIGH_SCREEN)
	{
		ClearScreen();
	}

	DisAckScreenInfo(pHeadingInfo, pPromptInfo1, 
		nPromptInfo2, nPromptInfo3);

	
	for(;;)
	{
		byKeyValue = GetKeyEx();
		
		iProcTime = GETKEY_TIME_INTERVAL;

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();

			switch( byKeyValue )
			{
			case VK_ESCAPE:  
				{
					return ID_CANCEL;	
				}				

			case VK_UP:  
			case VK_DOWN:
				{
					// No Operation
				}
				break;

			case VK_ENTER:   
				{
					return ID_RETURN;	
				}
			
			default:
				break;
			}

		}
		else
		{
			iProcTime += HANDLEIDLE_TIME_INTERVAL;

			nRet = HandleIdle();

			if (nRet == ID_GOTO_DEF_SCREEN)
			{
				return ID_GOTO_DEF_SCREEN;
			}
			//Because the LCD is initted, so display info again.
			else if (nRet == ID_INIT_LCD_JUST_NOW)
			{
				bRefreshAtOnce = TRUE;
			}
		}

		if (bRefreshAtOnce)
		{
			DisAckScreenInfo(pHeadingInfo, pPromptInfo1, 
					nPromptInfo2, nPromptInfo3);

			bRefreshAtOnce = FALSE;
		}

		
		if (nTimeout != -1)
		{
			nTimeout -= iProcTime;

			if (nTimeout > ACK_SLEEP_TIMEOUT)
			{
				Sleep(ACK_SLEEP_TIMEOUT);
				nTimeout -= ACK_SLEEP_TIMEOUT;
			}
			else if(nTimeout > 0)
			{
				Sleep((DWORD)nTimeout);

				return ID_TIMEOUT_RETURN;
			}
			else
			{
				return ID_TIMEOUT_RETURN;
			}
		}
	}
}


int DoModal_AcknowlegeDlg_INFO_ByLang(char* pHeadingInfo, 
							   char* pPromptInfo1, 
							   char* nPromptInfo2,
							   char* nPromptInfo3,
							   int	nTimeout,
							   char *pszLangCode,
							   BOOL bJustOneLine,
							   BOOL bNeedProcessEvent)
{
	UCHAR		byKeyValue;
	int			iProcTime;

	int			nRet = ID_CANCEL; 

	BOOL bRefreshAtOnce = TRUE;

	if(IS_HIGH_SCREEN)
	{
		ClearScreen();
	}

	DisAckScreenInfoByLang(pHeadingInfo, pPromptInfo1, 
		nPromptInfo2, nPromptInfo3,
		pszLangCode,
		bJustOneLine);


	for(;;)
	{
		byKeyValue = GetKeyEx();

		iProcTime = GETKEY_TIME_INTERVAL;


		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();

			switch( byKeyValue )
			{
			case VK_ESCAPE:  
				{
					return ID_CANCEL;	
				}				

			case VK_UP:  
			case VK_DOWN:
				{
					// No Operation
				}
				break;

			case VK_ENTER:   
				{
					return ID_RETURN;	
				}

			default:
				break;
			}

		}
		else
		{
			if(bNeedProcessEvent)
			{
				iProcTime += HANDLEIDLE_TIME_INTERVAL;

				nRet = HandleIdle();

				if (nRet == ID_GOTO_DEF_SCREEN)
				{
					return ID_GOTO_DEF_SCREEN;
				}
				//Because the LCD is initted, so display info again.
				else if (nRet == ID_INIT_LCD_JUST_NOW)
				{
					bRefreshAtOnce = TRUE;
				}
			}
		}

		if (bRefreshAtOnce)
		{
			DisAckScreenInfoByLang(pHeadingInfo, pPromptInfo1, 
				nPromptInfo2, nPromptInfo3,
				pszLangCode, 
				bJustOneLine);

			bRefreshAtOnce = FALSE;
		}


		if (nTimeout != -1)
		{
			nTimeout -= iProcTime;

			if (nTimeout > ACK_SLEEP_TIMEOUT)
			{
				Sleep(ACK_SLEEP_TIMEOUT);
				nTimeout -= ACK_SLEEP_TIMEOUT;
			}
			else if(nTimeout > 0)
			{
				Sleep((DWORD)nTimeout);

				return ID_TIMEOUT_RETURN;
			}
			else
			{
				return ID_TIMEOUT_RETURN;
			}
		}
	}
}

void PromptSysIsRebooting(void)
{
	DisAckScreenInfoByLang("SYS is rebooting", "", "", "",
		LCD_FIXED_LANGUAGE_EN,
		TRUE);
}

void PromptAppClosed(void)
{
	DisAckScreenInfoByLang("App Closed", "", "", "",
		LCD_FIXED_LANGUAGE_EN,
		TRUE);
}


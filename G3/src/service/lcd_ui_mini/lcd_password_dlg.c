/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_password_dlg.c
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:48
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include "lcd_password_dlg.h"


#include "lcd_public.h"

#include "lcd_item_handle.h"


extern int DisplaySignals(SUB_MENU* pSubMenu,  
				   int		nGetValueType);

extern int HandleSignalScreenEnter(SUB_MENU* pSubMenu, 
								   MENU_DISPLAY* pMenuDisplay,
								   GLOBAL_VAR_OF_LCD* pThis);

extern int HandleSignalScreenEscape(SUB_MENU* pSubMenu);

extern int HandleSignalScreenUpOrDown(SUB_MENU* pSubMenu, STACK_PVOID* pStack, BOOL bUp, GLOBAL_VAR_OF_LCD* pThis);


/*==========================================================================*
 * FUNCTION : DoModal_PasswordDlg
 * PURPOSE  : Password screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*       pMenuDisplay : 
 *            GLOBAL_VAR_OF_LCD*  pThis        : 
 * RETURN   : int : return ID_CANCEL or authority level or password error info
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 17:19
 *==========================================================================*/
int DoModal_PasswordDlg(MENU_DISPLAY* pMenuDisplay, GLOBAL_VAR_OF_LCD* pThis)
{
	UCHAR byKeyValue;

	SUB_MENU  *pSubMenu;

	int nGetValueType = GET_VALUE_FROM_SYSTEM;

	BOOL bRefreshAtOnce = TRUE;

	int nIDRet = ID_VIEWING;

	ASSERT(pMenuDisplay->pPasswordMenu->pCurMenu);

	pSubMenu = pMenuDisplay->pPasswordMenu->pCurMenu;

	pSubMenu->nCur = 0;

	ClearScreen();
	DisplaySignals(pSubMenu, nGetValueType);

	for(;;)
	{
		byKeyValue = GetKeyEx();

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();
			switch( byKeyValue )
			{				

			case VK_ESCAPE:  
				{
					if(HandleSignalScreenEscape(pSubMenu) 
						== ID_GOTO_RPEVIOUS)
					{
						return ID_CANCEL;
					}
					else
					{
						nIDRet = ID_VIEWING;
					}
				}
				break;

			case VK_UP:  
			case VK_DOWN:
				{
					nIDRet = HandleSignalScreenUpOrDown
						(pSubMenu, NULL, (byKeyValue == VK_UP), pThis);
					
				}
				break;

			case VK_ENTER:   
				{
					nIDRet = HandleSignalScreenEnter
						(pSubMenu, pMenuDisplay,pThis);

                    if(nIDRet != ID_VIEWING && nIDRet != ID_EDITING)
					{
						//Return authority level or password error info
						return nIDRet;
					}
				}
				break;

			default:
				break;
			}
			if(nIDRet == ID_VIEWING)
			{
				nGetValueType = GET_VALUE_FROM_SYSTEM;
			}
			else if(nIDRet == ID_EDITING)
			{
				nGetValueType = GET_VALUE_FROM_EDITING;
			}
			//others IDRet handle				

			bRefreshAtOnce = TRUE;

		}
		else
		{
			int nRetHandleIdle = HandleIdle();

			if(nRetHandleIdle == ID_GOTO_DEF_SCREEN)
			{
				//Set to origin status
				pSubMenu->pItems[pSubMenu->nCur].cMenuStatus = 
					MD_SHOW_STATUS;

				return ID_GOTO_DEF_SCREEN;
			}
			else if(nRetHandleIdle == ID_INIT_LCD_JUST_NOW)
			{
				bRefreshAtOnce = TRUE;
			}
		}

		ASSERT(pThis);

		if(pThis->cDataRefreshFlag == NEED_REFRESH_DATA || bRefreshAtOnce)
		{
			DisplaySignals(pSubMenu, nGetValueType);

			bRefreshAtOnce = FALSE;

			pThis->cDataRefreshFlag = NEED_NOT_REFRESH_DATA;
		}

	}

}



/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_language_screen.h
*  CREATOR  : HULONGWEN                DATE: 2005-01-20 16:37
*  VERSION  : V1.00
*  PURPOSE  : ACU language select
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_LANGUAGE_SCREEN_H__050120
#define __LCD_LANGUAGE_SCREEN_H__050120

#ifdef  __cplusplus
extern "C" {
#endif

	struct tagLangSelScreen
	{
		int			nLangNum;
		LANG_TEXT**  ppLangText;   //

		int			nLangEquipID;
		int			nLangSigType;
		int			nLangSigID;

		int	        nCurItem;      //��ǰ�˵��� -- �ڲ�ʹ��


		//Frank Wu,20160127, for MiniNCU
		//for all languages
		int	        nCurItemForAllLang;
		int			nLangNumForAllLang;
		void*		pvLangInfoForAllLang;//LANG_SUPPORT_TABLE_ITEM *
	};

	typedef struct tagLangSelScreen LANG_SEL_SCREEN;
	

#ifdef __cplusplus
}
#endif
#endif //__LCD_LANGUAGE_SCREEN_H__050120

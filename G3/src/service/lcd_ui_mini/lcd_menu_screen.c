  /*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_menu_screen.c
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:48
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include "lcd_public.h"
#include "lcd_menu_screen.h"
#include "lcd_ack_dlg.h"
#include "lcd_main.h"


#define MAIN_OFFSET_WIDTH	16

BYTE g_byAuthorityLevel = BROWSER_LEVEL;

extern MENU_ITEM* g_pActiveAlarmMenuItem;
extern MENU_ITEM* g_pHistoryAlarmMenuItem;


extern int DoModal_PasswordDlg(MENU_DISPLAY* pMenuDisplay, 
							   GLOBAL_VAR_OF_LCD* pThis);

extern int DoModal_AlarmsScreen(MENU_DISPLAY* pMenuDisplay, 
								GLOBAL_VAR_OF_LCD* pThis, 
								int nMenuType);


extern int DoModal_SignalsScreen(MENU_DISPLAY* pMenuDisplay,
								 GLOBAL_VAR_OF_LCD* pThis);


extern	void* AddHistoryAlarmItems(MENU_ITEM* pEquipItem);


extern int RefreshSubMenuStatus(SUB_MENU *pSubMenu, BOOL bRecursive);






static void HandleSelfTestMenu(int nLangResID, GLOBAL_VAR_OF_LCD *pThis);


extern int	GetVirtualGroupNameResID(int nEquipTypeId);



/*==========================================================================*
 * FUNCTION : DisplaySubmenuItem
 * PURPOSE  : Display submenu item on menu screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    nMenuItemID  : submenu ID
 *            int    x            : x offset
 *            int    y            : y offset
 *            char*  pLangCode    : language code
 *            BOOL   bCurItemFlag : display cursor according to it
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 16:34
 *==========================================================================*/
static int DisplaySubmenuItem(int nMenuItemID,
							  int x,
							  int y,
							  char* pLangCode, 
							  BOOL bCurItemFlag)
{
	LANG_TEXT* pLangText;

	char*	pDisBuf = NULL;
#ifdef DISP_RECT_POSITION_NO
	char	acDisBuf[80];
#endif
	int nBufLen;

	int nEquipID = -1;
	EQUIP_INFO* pEquipInfo = NULL;

	int nEquipTypeID = -1;
	STDEQUIP_TYPE_INFO* pStdEquipInfo = NULL;

	int nDeviceID = -1;
	PRODUCT_INFO sProductInfo;
	ZERO_POBJS(&sProductInfo, 1);

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	int nFontHeight = GetFontHeightFromCode(pLangCode);

	int nDrawWidth;

	int i;

	//Get lang text accoring to menu ID
	if((LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_FIXED_LANG_RES)
		|| (LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_DIVIDED_SUB_MENU)
		|| (LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_CHANGED_MENU_ITEM))
	{
		pLangText = GetLCDLangText(
			LCD_SPLIT_LANG_RES_ID(nMenuItemID), 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		ASSERT(pLangText);
		pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_VIRTUAL_GROUP_EQUIP)
	{
		nEquipTypeID = LCD_SPLIT_STD_EQUIP_ID(nMenuItemID);

		//�Ȳ���LCD˽������
		int nVirGroupResId = GetVirtualGroupNameResID(nEquipTypeID);
		if(nVirGroupResId > 0)
		{
			pLangText = GetLCDLangText(
				nVirGroupResId, 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);

			ASSERT(pLangText);
			pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
		}
		//���û�����ã���ȡ X01(����201,301��)�ı�׼�豸������
		else
		{
			nEquipTypeID += 1;

			DxiGetData(VAR_A_STD_EQUIP_INFO,
				nEquipTypeID,			
				0,		
				&nBufLen,			
				&pStdEquipInfo,			
				0);

			ASSERT(pStdEquipInfo);

			pLangText = pStdEquipInfo->pTypeName;

			ASSERT(pLangText);
			pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
		}
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_ACTUAL_EQUIP)
	{
		nEquipID = LCD_SPLIT_ACT_EQUIP_ID(nMenuItemID);

		DxiGetData(VAR_A_EQUIP_INFO,
			nEquipID,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0);

		ASSERT(pEquipInfo);

		pLangText = pEquipInfo->pEquipName;

		ASSERT(pLangText);
		pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_STDANDARD_EQUIP)
	{
		nEquipTypeID = LCD_SPLIT_STD_EQUIP_ID(nMenuItemID);

		DxiGetData(VAR_A_STD_EQUIP_INFO,
			nEquipTypeID,			
			0,		
			&nBufLen,			
			&pStdEquipInfo,			
			0);

		ASSERT(pStdEquipInfo);

		pLangText = pStdEquipInfo->pTypeName;

		ASSERT(pLangText);
		pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_BARCODE_DEVICE)
	{
		LANG_TEXT  sDeviceName;

		nDeviceID = LCD_SPLIT_BAR_DEVICE_ID(nMenuItemID);
		
		PRODUCT_INFO*	pProductInfo = NULL;

		pProductInfo = QueryProductInfo(nDeviceID);

		if('\0' != pProductInfo->szDeviceAbbrName[nCurrentLangFlag])
		{
			pDisBuf = pProductInfo->szDeviceAbbrName[nCurrentLangFlag];
		}
		else
		{
			pDisBuf = "Unknow Device";
		}
		
		
#if 0
		int nMaxDeviceNum = DXI_GetDeviceNum();
		int nFirstRectID = DXI_GetFirstRectDeviceID();

		if(nDeviceID == 1)
		{
			pDisBuf = MINI_NCU_DEV_NAME;
		}
		else if(nDeviceID <= nMaxDeviceNum)
		{
			DXI_GetPIByDeviceID(nDeviceID, &sProductInfo);

			pDisBuf = sProductInfo.szDeviceAbbrName[nCurrentLangFlag];
		}
		//I2C Devices
		else if((nDeviceID >= nFirstRectID + 290) && (nDeviceID < nFirstRectID + 298))
		{
			pDisBuf = g_SiteInfo.stI2CProductInfo[nDeviceID - nFirstRectID - 290].szDeviceAbbrName[nCurrentLangFlag];
		}
		else if((nDeviceID >= nFirstRectID + 298) && (nDeviceID < nFirstRectID + 306))
		{
			pDisBuf = g_SiteInfo.stCANSMDUPProductInfo[nDeviceID - nFirstRectID - 298].szDeviceAbbrName[nCurrentLangFlag];
		}
		//Rs485
		else if((nDeviceID >= nFirstRectID + 306) && (nDeviceID < nFirstRectID + 368))
		{
			pDisBuf = g_SiteInfo.st485ProductInfo[nDeviceID - nFirstRectID - 306].szDeviceAbbrName[nCurrentLangFlag];
		}
		//Convertor
		else if(nDeviceID >= nFirstRectID + 368)
		{
			pDisBuf = g_SiteInfo.stCANConverterProductInfo[nDeviceID - nFirstRectID - 368].szDeviceAbbrName[nCurrentLangFlag];
			
		}
		else
		{
			pDisBuf = "Unknow Device";
		}
#endif
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_CUSTOMIZE_MENU)
	{
		int nPageID = LCD_SPLIT_ACT_EQUIP_ID(nMenuItemID);

		USER_DEF_PAGES *pUserDefPages = NULL;

		DxiGetData(VAR_USER_DEF_PAGES,
			0,			
			0,		
			&nBufLen,			
			&pUserDefPages,			
			0);

		ASSERT(pUserDefPages);
		PAGE_NAME *pPageName = pUserDefPages->pPageInfo->pPageName;
		for(i = 0; 
			(i < pUserDefPages->pPageInfo->iPageInfoNum) && pPageName;
			i++, pPageName++)
		{
			if(pPageName->iPageID == nPageID)
			{
				break;
			}
		}
		ASSERT(i < pUserDefPages->pPageInfo->iPageInfoNum); //һ��Ҫ�ҵ���
		ASSERT(pPageName);

		//Frank Wu,20160127, for MiniNCU
		//pDisBuf = pPageName->pAbbrName[nCurrentLangFlag];
		if(pPageName->iResourceID < 0)
		{
			pDisBuf = pPageName->pAbbrName[nCurrentLangFlag];
		}
		else
		{
			pLangText = GetLCDLangText(
				pPageName->iResourceID,
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);

			ASSERT(pLangText);
			pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
		}

	}

	else
	{
		DEBUG_LCD_FILE_FUN_LINE_HEX("SubMenuItem_ID", nMenuItemID);
	}

	ASSERT(pDisBuf);

#ifdef DISP_RECT_POSITION_NO
	if((nEquipID >= RECT_EQUIP_ID_MIN) && (nEquipID <= RECT_EQUIP_ID_MAX))
	{
		int		iPosNo = LUI_GetDwordSigValue(nEquipID, SIG_TYPE_SAMPLING, SIG_ID_POSTION_NO);
		char	acPosNo[10];

		//When SCU+ start, PositionNo maybe is 0, which does not need disply
		if((iPosNo > 0) && (iPosNo < 1000))
		{
			snprintf(acPosNo, sizeof(acPosNo), "%03d", iPosNo);

			snprintf(acDisBuf, sizeof(acDisBuf), "%s(%s)", pLangText->pAbbrName[nCurrentLangFlag], acPosNo);

			pDisBuf = acDisBuf;
		}
	}
#endif

	nDrawWidth = GetWidthOfString(pDisBuf, (int)strlen(pDisBuf), pLangCode);

	//Display string
	DisString(pDisBuf, 
		(int)strlen(pDisBuf),  
		x,
		y,
		pLangCode, 
		FLAG_DIS_NORMAL_TYPE);

	//clear the front blank
	if(bCurItemFlag)
	{
		ClearBar(ARROW_WIDTH,
			y,
			x - ARROW_WIDTH,
			nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);

		//Display the right arrow
		DisSpecifiedFlag(ASCII_RIGHT_ARROW,
				0, 
				y);
	}
	else
	{
		ClearBar(0,
			y,
			x,
			nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);
	}

	//clear the back blank
	if(x + nDrawWidth < SCREEN_WIDTH - ARROW_WIDTH)
	{
	ClearBar( x + nDrawWidth,
			y,
			SCREEN_WIDTH - ARROW_WIDTH - (x + nDrawWidth),
			nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);
	}


	return ERR_LCD_OK;

}



/*==========================================================================*
 * FUNCTION : DisplaySubmenus
 * PURPOSE  : Display submenu screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*  pMenuDisplay : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 17:12
 *==========================================================================*/
static int DisplaySubmenus(MENU_DISPLAY* pMenuDisplay)
{
	ASSERT(pMenuDisplay->pMainMenu->pCurMenu);

	char*		pLangCode = GetCurrentLangCode();
	ASSERT(pLangCode);

	MENU_ITEM*	pItems;

	SUB_MENU*	pSubMenu;
	pSubMenu = pMenuDisplay->pMainMenu->pCurMenu;

	int			i;
	int			nFontHeight;
	int			nMaxRow;

	nFontHeight = GetFontHeightFromCode(pLangCode);
	nMaxRow = SCREEN_HEIGHT/nFontHeight;

	if(pSubMenu->nDispItems <= 0)
	{
#define PROMPT_NO_ITEMS_TIMEOUT 1500
		DoModal_AcknowlegeDlg_ID(HEADING_PROMPT_INFO_LANG_ID, 
			NO_ITEM_INFO_LANG_ID, 
			ESC_OR_ENT_RET_PROMPT_LANG_ID,
			-1,
			PROMPT_NO_ITEMS_TIMEOUT);

		return ID_GOTO_RPEVIOUS;
	}


	//Arrange the top and bottom menu items to be displayed
	if(pSubMenu->nCur < pSubMenu->nTop)
	{
		pSubMenu->nTop		= pSubMenu->nCur;
		pSubMenu->nBottom	= pSubMenu->nTop + (nMaxRow - 2);	//ע�⣺�����Ѿ�ռ��һ�У�����Ҫ��2

	}

	if(pSubMenu->nCur > pSubMenu->nBottom)
	{
		pSubMenu->nBottom	= pSubMenu->nCur;
		pSubMenu->nTop		= pSubMenu->nBottom - (nMaxRow - 2);

	}

	if(pSubMenu->nBottom != pSubMenu->nTop + (nMaxRow - 2))
	{
		pSubMenu->nBottom = pSubMenu->nTop + (nMaxRow - 2);
	}

	//Display the father item name
	DisplaySubmenuItem(pSubMenu->nSubMenuID,
		0, 
		0, 
		pLangCode,
		FALSE);


	int	nHasDisplayLine = 1;	//�Ѿ���ʾ��һ�б���

	for(i = pSubMenu->nTop; 
		(i <= pSubMenu->nBottom) && (i < pSubMenu->nDispItems); 
		i++, nHasDisplayLine++)
	{
		//Display sub items in turn
		ASSERT(pSubMenu->pDispItems[i] >= 0);
		ASSERT(pSubMenu->pDispItems[i] < pSubMenu->nItems);

		pItems = pSubMenu->pItems + pSubMenu->pDispItems[i];
		if(pItems == NULL)
		{
			break;
		}

		DisplaySubmenuItem(pItems->nMenuItemID, 
				MAIN_OFFSET_WIDTH, 
				(i - pSubMenu->nTop + 1) * nFontHeight, 
				pLangCode,
				i == pSubMenu->nCur);

	}

	//�������δ��ʾ�Ĳ���
	if(nHasDisplayLine < nMaxRow)
	{
		ClearBar(0,
			nHasDisplayLine * nFontHeight,
			SCREEN_WIDTH - ARROW_WIDTH,
			SCREEN_HEIGHT - nHasDisplayLine * nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);
	}

	//Display progress bar
	DisplayProgressBar(pSubMenu->nDispItems,
			pSubMenu->nCur,
			pSubMenu->nTop,
			pSubMenu->nBottom);

	return ERR_LCD_OK;
}

int GetInputUserAndPwd(MENU_DISPLAY* pMenuDisplay, GLOBAL_VAR_OF_LCD* pThis)
{
	int nIdRet = DoModal_PasswordDlg(pMenuDisplay, pThis);

	if(nIdRet == ID_ERR_PASSWORD)
	{
#define PROMPT_ERR_PASSWORD_TIMEOUT 2000
		//Prompt error password
		DoModal_AcknowlegeDlg_ID(HEADING_PROMPT_INFO_LANG_ID, 
			ERR_PASSWORD_PROMPT_LANG_ID, 
			ESC_OR_ENT_RET_PROMPT_LANG_ID, -1, 
			PROMPT_ERR_PASSWORD_TIMEOUT);

		return ID_CANCEL;
	}
	else if(nIdRet >= BROWSER_LEVEL
		&& nIdRet <= ADMIN_LEVEL)
	{
		g_byAuthorityLevel = nIdRet;

		pThis->cPasswordFlag = NEED_NOT_PASSWORD_VALIDATE;

		return ID_OK;
	}
	else
	{
		return ID_CANCEL;
	}
}

/*==========================================================================*
 * FUNCTION : HandleEnter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*       pMenuDisplay : 
 *            GLOBAL_VAR_OF_LCD*  pThis        : 
 * RETURN   : static int : Attention the return of ID_GOTO_RPEVIOUS(DoModal_SignalsScreen() or DoModal_AlarmsScreen()) 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-03-17 14:52
 *==========================================================================*/
static int HandleEnter(MENU_DISPLAY* pMenuDisplay, GLOBAL_VAR_OF_LCD* pThis)
{
	//because should change the pMenuDisplay->pMainMenu->pCurMenu, use **
	SUB_MENU  **ppSubMenu;
	STACK_PVOID* pStack;
	MENU_ITEM *pItem;
	void* pHistoryAlarmNew = NULL;

	ASSERT(pMenuDisplay->pMainMenu->pCurMenu);

	ppSubMenu = &(pMenuDisplay->pMainMenu->pCurMenu);
	pStack = &(pMenuDisplay->pMainMenu->stack);

	pItem = &(*ppSubMenu)->pItems[(*ppSubMenu)->pDispItems[(*ppSubMenu)->nCur]];

	if(pItem == g_pHistoryAlarmMenuItem)
	{
		//if pHistoryAlarmNew != NULL, it needs to be deleted later
		pHistoryAlarmNew = AddHistoryAlarmItems(g_pHistoryAlarmMenuItem);
	}

	if(pItem->pvItemData != NULL 
		&& ((SUB_MENU *)pItem->pvItemData)->nItems != 0
		&& ((SUB_MENU *)pItem->pvItemData)->pItems != NULL)
	{
		SUB_MENU*	pItemSubMenu;

		pItemSubMenu = (SUB_MENU *)pItem->pvItemData; /* SUB_MENUָ�� */

		pItemSubMenu->nCur = 0;

		ASSERT(pItemSubMenu->pItems + pItemSubMenu->nCur);

		ASSERT(pThis);

		if(pItem->cValidateFlag == MENU_ITEM_NEED_VALIDATE
			&& pThis->cPasswordFlag == NEED_PASSWORD_VALIDATE)
		{
			//Need enter into password dialog
			int nIdRet = GetInputUserAndPwd(pMenuDisplay, pThis);

			if (nIdRet != ID_OK)
			{
				return nIdRet;
			}
		}

		STACK_PUSH(pStack, (PVOID)(*ppSubMenu) );  /* Save current menu */
		*ppSubMenu = pItemSubMenu;

		if(!(IS_SUBMENU_ITEM(pItemSubMenu->pItems + pItemSubMenu->nCur)))
		{
			//Display active and history alarm screen
			char cMenuType = 
				(pItemSubMenu->pItems + pItemSubMenu->nCur)->cMenuType;

			if (cMenuType == MT_ACTIVE_ALARM_DISPLAY
				|| cMenuType ==  MT_HISTORY_ALARM_DISPLAY)
			{
				int nRet;

				pThis->cAlarmPopFlag = NO_ALARM_POP;

				//should return ID_GOTO_RPEVIOUS or ..
				nRet = DoModal_AlarmsScreen(pMenuDisplay, pThis, cMenuType);

				if (cMenuType ==  MT_HISTORY_ALARM_DISPLAY)
				{
					//after the view of history alarm, need delete pHistoryAlarmNew
					if (pHistoryAlarmNew)
					{
						DELETE_ITEM(pHistoryAlarmNew);
						//pHistoryAlarmNew = NULL;
					}
				}

				return nRet;
			}
			else
			{
				//by HULONGWEN should return ID_GOTO_RPEVIOUS or ..
				return DoModal_SignalsScreen(pMenuDisplay, pThis);
			}
		}
		else
		{

		}
	}
	else if (IS_SELF_TEST_ITEM(pItem)) 
	{
		HandleSelfTestMenu(LCD_SPLIT_LANG_RES_ID(pItem->nMenuItemID), pThis);
	}
	else
	{
		int nHeadingInfoID	= -1;
		int nPromptInfoID1	= -1;
		int nPromptInfoID2	= -1;
		int nPromptInfoID3	= -1;

		//prompt has no items
		if (pItem == g_pActiveAlarmMenuItem)
		{
			//If no active alarm
			nHeadingInfoID	= ACTIVE_ALARM_DISP_LANG_ID;
			nPromptInfoID1 = NO_ACTIVE_ALARM_PROMPT_LANG_ID;
		}
		else if(pItem == g_pHistoryAlarmMenuItem)
		{
			//If no history alarm
			nHeadingInfoID	= HISTORY_ALARM_DISP_LANG_ID;
			nPromptInfoID1 = NO_HISTORY_ALARM_PROMPT_LANG_ID;
		}
		else
		{
			nHeadingInfoID	= HEADING_PROMPT_INFO_LANG_ID;
			nPromptInfoID1	= NO_ITEM_INFO_LANG_ID;
			nPromptInfoID2	= ESC_OR_ENT_RET_PROMPT_LANG_ID;

		}

		return DoModal_AcknowlegeDlg_ID(nHeadingInfoID, 
			nPromptInfoID1, 
			nPromptInfoID2,
			nPromptInfoID3,
			PROMPT_NO_ITEMS_TIMEOUT);

	}

	return ID_OK;
}


/*==========================================================================*
 * FUNCTION : DoModal_MenuScreen
 * PURPOSE  : Menu screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*       pMenuDisplay : 
 *            GLOBAL_VAR_OF_LCD*  pThis        : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 17:18
 *==========================================================================*/
int DoModal_MenuScreen(MENU_DISPLAY* pMenuDisplay, GLOBAL_VAR_OF_LCD* pThis)
{
	UCHAR byKeyValue;

	SUB_MENU  **ppSubMenu;

	STACK_PVOID* pStack;

	BOOL bRefreshAtOnce = TRUE;

	ASSERT(pMenuDisplay->pMainMenu->pCurMenu);

	ppSubMenu = &(pMenuDisplay->pMainMenu->pCurMenu);

	(*ppSubMenu)->nCur = 0;

	pStack	= &(pMenuDisplay->pMainMenu->stack);

	//////////////////////////////////////////////////////////////////////////
	//RefreshSubMenuStatus(*ppSubMenu, TRUE);
	RefreshSubMenuStatus(*ppSubMenu, FALSE);
	//////////////////////////////////////////////////////////////////////////
	
	ClearScreen();
	DisplaySubmenus(pMenuDisplay);

	for(;;)
	{
		byKeyValue = GetKeyEx();

		//Add the code to meet the demand of Rect num change
		if ((*ppSubMenu)->nDispItems == 0)
		{
			byKeyValue = VK_ESCAPE;
		}
		else if ((*ppSubMenu)->nCur >= (*ppSubMenu)->nDispItems)//͵������
		{
			(*ppSubMenu)->nCur -= 1;

			bRefreshAtOnce = TRUE;
		}

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();

			switch( byKeyValue )
			{
			case VK_ESCAPE:  
				{
					if (STACK_IS_EMPTY(pStack))  /* �Ƕ���˵����˳�*/
					{
						return ID_GOTO_RPEVIOUS;
					}
					else
					{
						*ppSubMenu = (SUB_MENU *)STACK_POP(pStack);
					}
				}
				break;

			case VK_UP:  
			case VK_DOWN:
				{
					if( (*ppSubMenu)->nItems > 1 )
					{
						(*ppSubMenu)->nCur = (byKeyValue == VK_UP)
							? PrevSubMenuItem( *ppSubMenu, 1)
							: NextSubMenuItem( *ppSubMenu ,1);
					}
				}
				break;

			case VK_ENTER:   
				{
					int nRetID = HandleEnter(pMenuDisplay, pThis);
					if (nRetID == ID_GOTO_RPEVIOUS)//return from DoModal_SignalsScreen() or DoModal_AlarmsScreen()
					{
						*ppSubMenu = (SUB_MENU *)STACK_POP(pStack);
					}
					//others IDRet handle

					//Refresh submenu display status when press enter
					RefreshSubMenuStatus(*ppSubMenu, FALSE);
				}
				break;
			case VK_ENTER_UP:
				{
					DisNavigationInfo(pMenuDisplay);
				}
				break;
			default:
				break;
			}
			
			bRefreshAtOnce = TRUE;
		}
		else
		{
			int nRetHandleIdle = HandleIdle();

			if(nRetHandleIdle == ID_GOTO_DEF_SCREEN)
			{
				//The following two sentences must be called 
				//to resume the current menu and stack
				pMenuDisplay->pMainMenu->pCurMenu = 
					pMenuDisplay->pMainMenu->pRootMenu;

				STACK_SET_EMPTY(pStack);

				//return to default screen
				return ID_GOTO_DEF_SCREEN;
			}
			else if(nRetHandleIdle == ID_INIT_LCD_JUST_NOW)
			{
				bRefreshAtOnce = TRUE;
			}
		}		

		if(pThis->cDataRefreshFlag == NEED_REFRESH_DATA || bRefreshAtOnce)
		{
			bRefreshAtOnce = FALSE;
			pThis->cDataRefreshFlag = NEED_NOT_REFRESH_DATA;

			if(DisplaySubmenus(pMenuDisplay) == ID_GOTO_RPEVIOUS)
			{
				//NO ITEMS to display
				*ppSubMenu = (SUB_MENU *)STACK_POP(pStack);
				bRefreshAtOnce = TRUE;
			}
		}

	}

}


static void HandleSelfTestMenu(int nLangResID, GLOBAL_VAR_OF_LCD* pThis)
{
	char*		pLangCode = GetCurrentLangCode();
	UCHAR		ucKey;
	BOOL		bRefreshAtOnce;

	ClearScreen();
	AlarmLedCtrl(LED_RUN, FALSE);

	char *pPromtInfo = "This is Self Test";
	DisString(pPromtInfo, strlen(pPromtInfo), 
		0, 48, pLangCode, FLAG_DIS_NORMAL_TYPE);


	char *pDisplayInfo;
	char *pDate = "2008-09-25";
	char *pVoltage = "53.5 V";
	char *pCurrent = "1080 A";
	char *pBattMgnState = "Float Charge";
	char *pManAuto = "Auto";
	char *pAlarm = "Normal";

	char *pMainMenu = "Main Menu";
	char *pStatus = "Status";
	char *pMaintain = "Maintain";
	char *pParaSet = "Parameter Set";
	char *pFastSet = "Fast Set";

	while(1)
	{
		RunThread_Heartbeat(RunThread_GetId(NULL));
		ucKey = GetKey();
		if(ucKey != VK_NO_KEY)
		{
			KeyPressForeOperation();

			bRefreshAtOnce = TRUE;

			switch(ucKey)
			{
			case VK_ESCAPE:
				return ;

#define		ALINE_LEFT		8
#define		ALINE_CENTER	((SCREEN_WIDTH - strlen(pDisplayInfo) * BASE_HEIGHT) / 2)
#define		ALINE_RIGHT		(SCREEN_WIDTH - (strlen(pDisplayInfo) + 1) * BASE_HEIGHT)

#define		LINE(nY)		((nY - 1) * 16)

			case VK_UP:		//Default Screen
				ClearScreen();

				pDisplayInfo = pDate;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					ALINE_RIGHT, LINE(1), pLangCode, FLAG_DIS_NORMAL_TYPE);

				pDisplayInfo = pBattMgnState;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					ALINE_LEFT, LINE(3), pLangCode, FLAG_DIS_NORMAL_TYPE);

				pDisplayInfo = pVoltage;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					ALINE_LEFT, LINE(5), pLangCode, FLAG_DIS_NORMAL_TYPE);

				pDisplayInfo = pCurrent;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					ALINE_RIGHT, LINE(5), pLangCode, FLAG_DIS_NORMAL_TYPE);

				pDisplayInfo = pManAuto;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					ALINE_LEFT, LINE(7), pLangCode, FLAG_DIS_NORMAL_TYPE);

				pDisplayInfo = pAlarm;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					ALINE_RIGHT, LINE(7), pLangCode, FLAG_DIS_NORMAL_TYPE);

				break;

			case VK_DOWN:	//Main Menu
				ClearScreen();

				pDisplayInfo = pMainMenu;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					0, LINE(1), pLangCode, FLAG_DIS_NORMAL_TYPE);

				pDisplayInfo = pStatus;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					16, LINE(2), pLangCode, FLAG_DIS_NORMAL_TYPE);

				DisSpecifiedFlag(ASCII_RIGHT_ARROW,	0, LINE(2));

				pDisplayInfo = pMaintain;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					16, LINE(3), pLangCode, FLAG_DIS_NORMAL_TYPE);

				pDisplayInfo = pParaSet;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					16, LINE(4), pLangCode, FLAG_DIS_NORMAL_TYPE);

				pDisplayInfo = pFastSet;
				DisString(pDisplayInfo, strlen(pDisplayInfo), 
					16, LINE(5), pLangCode, FLAG_DIS_NORMAL_TYPE);

				break;

			case VK_ENTER:
				ClearScreen();

				//Light(FALSE);

				//pDisplayInfo = pDate;
				//DisString(pDisplayInfo, strlen(pDisplayInfo), 
				//	ALINE_RIGHT, 0, pLangCode, FLAG_DIS_NORMAL_TYPE);

				//pDisplayInfo = pVoltage;
				//DispBigSizeString(pDisplayInfo, strlen(pDisplayInfo), 
				//	((SCREEN_WIDTH - strlen(pDisplayInfo) * BASE_HEIGHT * 2) / 2), 16, pLangCode, FLAG_DIS_NORMAL_TYPE);

				//pDisplayInfo = pCurrent;
				//DispBigSizeString(pDisplayInfo, strlen(pDisplayInfo), 
				//	((SCREEN_WIDTH - strlen(pDisplayInfo) * BASE_HEIGHT * 2) / 2), 48, pLangCode, FLAG_DIS_NORMAL_TYPE);

				//pDisplayInfo = pBattMgnState;
				//DispBigSizeString(pDisplayInfo, strlen(pDisplayInfo), 
				//	((SCREEN_WIDTH - strlen(pDisplayInfo) * BASE_HEIGHT * 2) / 2), 80, pLangCode, FLAG_DIS_NORMAL_TYPE);

				//pDisplayInfo = pManAuto;
				//DisString(pDisplayInfo, strlen(pDisplayInfo), 
				//	ALINE_LEFT, 112, pLangCode, FLAG_DIS_NORMAL_TYPE);

				//pDisplayInfo = pAlarm;
				//DisString(pDisplayInfo, strlen(pDisplayInfo), 
				//	ALINE_RIGHT, 112, pLangCode, FLAG_DIS_NORMAL_TYPE);

				break;

			default:
				break;
			}
		}

		if(bRefreshAtOnce || pThis->cDataRefreshFlag == NEED_REFRESH_DATA)
		{
			bRefreshAtOnce = FALSE;
			pThis->cDataRefreshFlag = NEED_NOT_REFRESH_DATA;

			if(nLangResID == MAIN_SELF_TEST1_LANG_ID)
			{
				//�Բ��Բ˵�1�Ĵ���
//				char caDisplay[] = "��ʾ����";
//				DispBigSizeString(caDisplay, strlen(caDisplay), 0, 0, pLangCode, FLAG_DIS_NORMAL_TYPE);
			}
			else if(nLangResID == MAIN_SELF_TEST2_LANG_ID)
			{
				//�Բ��Բ˵�2�Ĵ���
				DisString("�Բ��Բ˵�", 10, 0, 64, pLangCode, FLAG_DIS_NORMAL_TYPE);
				SelfTestFunctionForDriver();
			}
		}
	}

}



extern LCD_PRIVATE_CFG_LOADER	g_stLcdPrivateCfgLoader;

int	GetVirtualGroupNameResID(int nEquipTypeId)
{
	
	VIRTUAL_GROUP_EQUIP_NAME_CFG *pCfg = g_stLcdPrivateCfgLoader.stVirGroupEquipNameCfgReader.pVirGroupEquipNameResCfg;
	int	iCfgNum = g_stLcdPrivateCfgLoader.stVirGroupEquipNameCfgReader.nVirGroupEquipNameResNum;
	int i;
	for(i = 0; (i < iCfgNum) && (pCfg); i++, pCfg++)
	{
		if(pCfg->nEquipTypeId == nEquipTypeId)
		{
			return pCfg->nResouceID;
		}
	}

	return - 1;
}
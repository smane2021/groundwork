/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : lcd_main.c
 *  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:48
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include "lcd_public.h"

#include "lcd_main.h" 

#include "lcd_ack_dlg.h"
#include	"../../mainapp/equip_mon/equip_mon.h"

#include "lcd_item_handle.h"

//if __ACTIVE_ALARM_POP_NO_KEY_PRESSED, active alarm screen will pop up 
//when no key was pressed after several minutes, ifndef, active alarm screen 
//will be poped up immediately.
#define __ACTIVE_ALARM_POP_NO_KEY_PRESSED

#define __SYNC_ALARM_LIST_TIMELY

// Display all menu item for debug
//#define __DISPLAY_ALL_MENU_ITEM



//����������ò˵�����1��Ϊ0
// Main Menu
#define RUNNING_INFO_MENU_NUM			1	//Must be 1
#define MAINTENANCE_MENU_NUM			1	//Must be 1
#define PARAMETER_SET_MENU_NUM			1	//Must be 1
#define SLAVE_PARAMETER_SET_MENU_NUM	1	//Must be 1
#define ENERGY_SAVIING_MENU_NUM			1	//Can be 1 or 0
#define CUSTOMIZE_MENU_NUM				1	//Can be 1 or 0
#define SELF_TEST_MENU1_NUM				0	//Can be 1 or 0
#define SELF_TEST_MENU2_NUM				0	//Can be 1 or 0
#define MAIN_MENU_ITEM_NUM			(RUNNING_INFO_MENU_NUM + MAINTENANCE_MENU_NUM \
	+ PARAMETER_SET_MENU_NUM + CUSTOMIZE_MENU_NUM \
	+ ENERGY_SAVIING_MENU_NUM \
	+ SELF_TEST_MENU1_NUM + SELF_TEST_MENU2_NUM )

//In RunningInfo SubMenu
#define ACTIVE_ALARM_MENU_NUM			1	//Must be 1
#define HISTORY_ALARM_MENU_NUM			1	//Must be 1
#define BARCODE_MENU_NUM				1	//Can be 1 or 0

//In Maintain SubMenu
#define MAN_AUTO_SET_IN_CTRL_MENU		1	//Can be 1 or 0

//In ParameterSetting SubMenu
#define ALARM_SET_MENU_NUM				1	//Can be 1 or 0
#define COMMUNICATION_SET_MENU_NUM		1	//Can be 1 or 0
#define SYSTEM_PARA_SET_NUM				1	//Must be 1

//In ALARM_SET SubMenu
#define ALARM_LEVEL_SET_MENU_NUM		1	//Can be 1 or 0
#define ALARM_RELAY_SET_MENU_NUM		1	//Can be 1 or 0
#define ALARM_CONTROL_SET_MENU_NUM		1	//�澯�������澯������
#define IB_ALARM_VOLT_SET_MENU_NUM		1	//��������IB���DI�ĸ澯��ƽ
#define ALARM_SET_MENU_ITEM_NUM		(ALARM_LEVEL_SET_MENU_NUM \
	+ ALARM_RELAY_SET_MENU_NUM \
	+ ALARM_CONTROL_SET_MENU_NUM \
	+ IB_ALARM_VOLT_SET_MENU_NUM)

#define RECT_TWINKLE_OFF_TIMEOUT	2 
#define RECT_TWINKLE_START_EQUIPID	3
#define CONVERTER_TWINKLE_START_EQUIPID 211


#define WORK_AS_SLAVE_ENABLE //Mark ACU+ can be set as slave mode

//Used to save some global variable
static GLOBAL_VAR_OF_LCD	g_gvThisService;

//The whole menu needed to display on screen
static MENU_DISPLAY			g_mdMenuDisplay;

//Current language flag
static int					g_nCurrentLangFlag = ENGLISH_LANGUAGE_FLAG;

int	g_iWorkMode = WORK_MODE_SEPERATE;//Ĭ�϶�������ģʽ added by Jimmy Wu 20110706
BOOL g_bNeedSwithToNextPage;//Frank Wu,20160127, for MiniNCU
BOOL g_bNeedSwithToFirstPage;//Frank Wu,20160127, for MiniNCU

//Used to parse equips and signals and Menu Node from g_SiteInfo
static PARSE_EQUIP_INFO		g_stParseEquipInfo;
static PARSE_MENU_NODE		g_stParseMenuNode;

static	ACT_EQUIP_NUM		g_s_ActEquipNum;
//static int				g_s_nActRectNum = 0;
//static int				g_s_nActConverterNum = 0;
//static int				g_s_nMBBattNum = 0;
//
//#define					EIB_NUM_MAX	4
//static int				g_s_nEIBBattNum[EIB_NUM_MAX] = {0, 0, 0, 0};
//
//static int				g_s_SMDU_Num = 0;

////Used to Read LCD private configuration
//Include LCD language file info and default screen item info
LCD_PRIVATE_CFG_LOADER	g_stLcdPrivateCfgLoader;

//The content of Active or history Alarm Menu Item will change frequently,
//Need to save it to go to it next time.
MENU_ITEM* g_pActiveAlarmMenuItem = NULL;
MENU_ITEM* g_pHistoryAlarmMenuItem = NULL;

//define the unique active alarm manager.
static ACTIVE_ALARM_MANAGER	g_s_ActiveAlarmManager =
{	
	NULL,	// not any alarm
	-1,		// -1: indicate the manager is not initialized yet.
	NULL,
	FALSE
};
//Frank Wu,20160127, for MiniNCU
static char*	g_s_ppLangCode[] = {
	LCD_FIXED_LANGUAGE_EN,
	LCD_FIXED_LANGUAGE_ZH,
	LCD_FIXED_LANGUAGE_TW,
	LCD_FIXED_LANGUAGE_DE,
	LCD_FIXED_LANGUAGE_ES,
	LCD_FIXED_LANGUAGE_FR,
	LCD_FIXED_LANGUAGE_IT,
	LCD_FIXED_LANGUAGE_RU,
	LCD_FIXED_LANGUAGE_TR,
	LCD_FIXED_LANGUAGE_SV,
	LCD_FIXED_LANGUAGE_PT,
};

#define MINI_NCU_LANGUAGE_LIST_PTR						(g_s_ppLangCode)
#define MINI_NCU_LANGUAGE_LIST_ITEM(iArgIndex)			(g_s_ppLangCode[iArgIndex])
#define MINI_NCU_LANGUAGE_LIST_NUM						(sizeof(g_s_ppLangCode)/sizeof(g_s_ppLangCode[0]))

#define ACTIVE_ALARM_LIST_INITIALIZED()		(g_s_ActiveAlarmManager.nAlarmCount >= 0)


extern int DoModal_AlarmsScreen(MENU_DISPLAY* pMenuDisplay,
								GLOBAL_VAR_OF_LCD* pThis,
								int nMenuType);

extern int DoModal_LanguageScreen(MENU_DISPLAY* pMenuDisplay,
								  GLOBAL_VAR_OF_LCD* pThis);

//Frank Wu,20160127, for MiniNCU
extern int DoModal_LanguageScreenMiniNCU(MENU_DISPLAY* pMenuDisplay,
										 GLOBAL_VAR_OF_LCD* pThis);
extern int	GetVirtualGroupNameResID(int nEquipTypeId);

static int ParsedDefScreenCfgTableProc(char *szBuf, 
				DEFAULT_SCREEN_ITEM_CFG *pDefaultScreenItemCfg);

static int ParsedLcdLangFileTableProc(IN char *szBuf,
									  OUT LANG_TEXT *pStructData);

static int LoadLCDPrivateConfigProc(void *pCfg, void *pLoadToBuf);

static int LoadLCDPrivateConfig(LCD_PRIVATE_CFG_LOADER* pLcdPrivateCfgLoader);



static int ParseEquipsAndSignals(PARSE_EQUIP_INFO* pParseEquipInfo);

static int ParseMainMenuNode(PARSE_MENU_NODE* pParseMenuNode);


//Frank Wu,20160127, for MiniNCU
static int CreateMainMenuMiniNCU(MENU_STRU** ppMenu);
static int CreateCustomSubMenu(MENU_ITEM* pParentMenuItem, 
							   USER_DEF_PAGES *pUserDefPages,
							   int nPageID);
static int QueryCustomizeMenu(void** pCustomizeMenuItem);
static int ClearLCDPrivateConfigLangFile(LANG_FILE *pstLCDLangFile);

static int CreateMenus(MENU_DISPLAY* pMenuDisplay);
static int CreateMainMenu(MENU_STRU** ppMenu);
static int CreateDefaultMenu(MENU_STRU** ppMenu);
static int CreatePasswordMenu(MENU_STRU** ppMenu);

static int CreateRunInfoMenu(MENU_ITEM* pMenuItem);
static int CreateSignalCtrlMenu(MENU_ITEM* pMenuItem);
static int CreateSignalSetMenu(MENU_ITEM* pMenuItem);
static int CreateCustomizeMenu(MENU_ITEM* pMenuItem);

static int ConstructEquipSubMenu(SUB_MENU* pEquipSubMenu,
								 int nSigType);
static int CreateSignalSubMenu(OUT SUB_MENU** ppSubMenu,
							   EQUIP_INFO* pEquipInfo, 
							   SIGNAL_INFO* pSignalInfo,
							   int nSigType);

static int AddSiteInventoryItems(MENU_ITEM* pEquipItem);
static int AddAlarmControlItems(MENU_ITEM* pEquipItem);
static int AddAlarmSetItems(MENU_ITEM* pEquipItem);
static int AddCommSetItems(MENU_ITEM* pEquipItem);
static int AddSysSetItems(MENU_ITEM* pEquipItem);


static int AddActiveAlarmItems(MENU_ITEM* pEquipItem);


static int DestroyParseMemory(void);
static int DestroyMenus(MENU_DISPLAY* pMenuDisplay);

static void FreeLangText(LANG_TEXT *pLangText);
static int LoadLCDPrivateConfig(LCD_PRIVATE_CFG_LOADER* pLcdPrivateCfgLoader);
static int DestroyLCDPrivateConfig(LCD_PRIVATE_CFG_LOADER* pLcdPrivateCfgLoader);

static int TM_FUNC_PasswordValid(HANDLE hself, int idTimer, void *par);
static int TM_FUNC_SwitchDefScreen(HANDLE hself, int idTimer, void *par);
static int TM_FUNC_AlarmPopAtOnce(HANDLE hself, int idTimer, void *par);
static int TM_FUNC_InitLcd(HANDLE hself, int idTimer, void *par);
static int TM_FUNC_RefreshData(HANDLE hself, int idTimer, void *par);


static int InitLCDTimers(void);
static int InitLCDService(void);
static int DestroyLCDService(void);

static ALARM_SIG_VALUE_EX_LCD*
	FindPreviousAlarmInList(ALARM_SIG_VALUE_EX_LCD* pFindAlarm);

static ALARM_SIG_VALUE_EX_LCD*
	FindPreAlarmInListIncudeTime(ALARM_SIG_VALUE* pFindAlarm);

//Alarm list handle funcs
static int AddActiveAlarmToList(ALARM_SIG_VALUE_EX_LCD* pAddAlarm);
static int DeleteActiveAlarmFromList(ALARM_SIG_VALUE_EX_LCD* pDelAlarm);
static int InitActiveAlarmManager(void);
static int DestroyActiveAlarmManager(void);

static BOOL LedAndBuzzerCtrlViaAlarmList(void);


static int DelMenuRecursively(SUB_MENU* pSubMenu);

		SUB_MENU* GetSubMenuFromID(SUB_MENU* pSubMenu, int nSubMenuID);
static MENU_ITEM* FindMenuItemByID(SUB_MENU* pSubMenu, int nMenuItemID);
static MENU_ITEM* FindParentMenuItemByID(SUB_MENU* pRootSubMenu, int nMenuItemID, OUT int *pItemIndex);
static int GetEquipIDThroughDeviceID(int iDeviceID);

static int ReadLcdVar(char *pVarName);
int WriteLcdVar(char *pVarName, int nValue);

EQUIP_INFO* LCD_GetEquipInfo(int iEquipID);


BOOL LUI_IsNeedRebootForSignal(int iEquipID, 
							   int iSigType, 
							   int iSigID)
{
	SIG_ITEM		staRebootSigList[] = {
		{1, 2, 180},//Rectifier Expansion
	};
	SIG_ITEM		*pSigItem = NULL;
	int				i;

	pSigItem = staRebootSigList;
	for(i = 0; i < ITEM_OF(staRebootSigList); i++, pSigItem++)
	{
		if( (iEquipID == pSigItem->iEquipID)
			&& (iSigType == pSigItem->iSigType)
			&& (iSigID == pSigItem->iSigID) )
		{
			return TRUE;
		}
	}

	return FALSE;
}


int LCD_SwitchToNextIndex(BOOL bNeedInit,
						BOOL bForceSwitch,
						int iSwitchInterval,
						int iMaxIndex)
{
	static time_t			s_tmLastSwitchTime = 0;
	static int				s_iCurIndex = 0;
	time_t					tmCurTime;

	tmCurTime = time(NULL);

	if(bNeedInit)//init
	{
		s_iCurIndex = 0;
		s_tmLastSwitchTime = tmCurTime;
	}
	
	if(bForceSwitch)
	{
		s_iCurIndex++;
	}
	
	if(iSwitchInterval > 0)
	{
		if((tmCurTime - s_tmLastSwitchTime >= iSwitchInterval)
			|| (tmCurTime - s_tmLastSwitchTime < 0))
		{
			s_tmLastSwitchTime = tmCurTime;
			s_iCurIndex++;
		}
	}

	s_iCurIndex %= iMaxIndex;

	return s_iCurIndex;
}


//Frank Wu,20160127, for MiniNCU

static int DisNavigationInfoItem(SUB_MENU** ppSubMenu, int iNum)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";
	char*			pszName = NULL;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();
	LANG_TEXT* pLangText;

	int				iCurItemLevel;
	int				iaSwitchTable[iNum];
	int				iSwitchTableIndex;
	int				i;
	SUB_MENU*		pSubMenu;

	pLangText = GetLCDLangText(
		HEADING_PROMPT_INFO_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);
	if(pLangText)
	{
		snprintf(szHeadingInfo, sizeof(szHeadingInfo),
			"%s",
			pLangText->pAbbrName[nCurrentLangFlag]);
	}


	for(i = 0; i < iNum; i++)
	{
		iaSwitchTable[i] = i;
	}

	//switch to next page
	iSwitchTableIndex = LCD_SwitchToNextIndex(g_bNeedSwithToFirstPage, g_bNeedSwithToNextPage, -1, ITEM_OF(iaSwitchTable));
	iCurItemLevel = iaSwitchTable[iSwitchTableIndex];

	g_bNeedSwithToFirstPage = FALSE;
	g_bNeedSwithToNextPage = FALSE;

	pSubMenu = ppSubMenu[iCurItemLevel];
	if(NULL != pSubMenu)
	{
		pszName = LCD_QuerySubmenuItemName(pSubMenu->nSubMenuID);
		if(NULL != pszName)
		{
			snprintf(szPromptInfo1, sizeof(szPromptInfo1),
				"L%d %s %s",
				iCurItemLevel + 1,
				pszName,
				(iCurItemLevel == iNum - 1)? "*": "->");
		}
	}

	return DoModal_AcknowlegeDlg_INFO(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		PROMPT_INFO_TIMEOUT);
}



int DisNavigationInfo(MENU_DISPLAY* pMenuDisplay)
{
	SUB_MENU  *pSubMenu;
	pSubMenu = pMenuDisplay->pMainMenu->pCurMenu;

	STACK_PVOID* pStack;
	pStack	= &(pMenuDisplay->pMainMenu->stack);

	SUB_MENU*	paMenuStackList[20];
	int			iMenuStackListItemNum;
	int			i;
	int			nIDAlarmPrompt;

	iMenuStackListItemNum = STACK_DEPTH(pStack);
	for(i = iMenuStackListItemNum - 1; i >= 0; i--)
	{
		paMenuStackList[i] = (SUB_MENU*)STACK_POP(pStack);
	}

	for(i = 0; i < iMenuStackListItemNum; i++)
	{
		STACK_PUSH(pStack, (PVOID)paMenuStackList[i]);
	}

	paMenuStackList[iMenuStackListItemNum] = pSubMenu;
	iMenuStackListItemNum++;

	g_bNeedSwithToFirstPage = TRUE;
	g_bNeedSwithToNextPage = FALSE;

	for(;;)
	{
		nIDAlarmPrompt = DisNavigationInfoItem(paMenuStackList, iMenuStackListItemNum);

		if (nIDAlarmPrompt == ID_RETURN)
		{
			g_bNeedSwithToNextPage = TRUE;
		}
		else if (nIDAlarmPrompt == ID_CANCEL)
		{
			break;
		}
		else if (nIDAlarmPrompt == ID_GOTO_DEF_SCREEN)
		{
			break;
		}
		else if(nIDAlarmPrompt == ID_TIMEOUT_RETURN)
		{
			break;
		}
	}
	
	g_bNeedSwithToFirstPage = TRUE;
	g_bNeedSwithToNextPage = FALSE;

	return ID_GOTO_RPEVIOUS;
}


char* LCD_QuerySubmenuItemName(int nMenuItemID)
{
	LANG_TEXT* pLangText;

	char*	pDisBuf = NULL;
#ifdef DISP_RECT_POSITION_NO
	char	acDisBuf[80];
#endif
	int nBufLen;

	int nEquipID = -1;
	EQUIP_INFO* pEquipInfo = NULL;

	int nEquipTypeID = -1;
	STDEQUIP_TYPE_INFO* pStdEquipInfo = NULL;

	int nDeviceID = -1;
	PRODUCT_INFO sProductInfo;
	ZERO_POBJS(&sProductInfo, 1);

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	char* pLangCode = GetLangCode(nCurrentLangFlag);

	ASSERT(pLCDLangFile);

	int nFontHeight = GetFontHeightFromCode(pLangCode);

	int nDrawWidth;

	int i;

	//Get lang text accoring to menu ID
	if((LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_FIXED_LANG_RES)
		|| (LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_DIVIDED_SUB_MENU)
		|| (LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_CHANGED_MENU_ITEM))
	{
		pLangText = GetLCDLangText(
			LCD_SPLIT_LANG_RES_ID(nMenuItemID), 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		ASSERT(pLangText);
		pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_VIRTUAL_GROUP_EQUIP)
	{
		nEquipTypeID = LCD_SPLIT_STD_EQUIP_ID(nMenuItemID);

		//�Ȳ���LCD˽������
		int nVirGroupResId = GetVirtualGroupNameResID(nEquipTypeID);
		if(nVirGroupResId > 0)
		{
			pLangText = GetLCDLangText(
				nVirGroupResId, 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);

			ASSERT(pLangText);
			pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
		}
		//���û�����ã���ȡ X01(����201,301��)�ı�׼�豸������
		else
		{
			nEquipTypeID += 1;

			DxiGetData(VAR_A_STD_EQUIP_INFO,
				nEquipTypeID,			
				0,		
				&nBufLen,			
				&pStdEquipInfo,			
				0);

			ASSERT(pStdEquipInfo);

			pLangText = pStdEquipInfo->pTypeName;

			ASSERT(pLangText);
			pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
		}
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_ACTUAL_EQUIP)
	{
		nEquipID = LCD_SPLIT_ACT_EQUIP_ID(nMenuItemID);

		DxiGetData(VAR_A_EQUIP_INFO,
			nEquipID,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0);

		ASSERT(pEquipInfo);

		pLangText = pEquipInfo->pEquipName;

		ASSERT(pLangText);
		pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_STDANDARD_EQUIP)
	{
		nEquipTypeID = LCD_SPLIT_STD_EQUIP_ID(nMenuItemID);

		DxiGetData(VAR_A_STD_EQUIP_INFO,
			nEquipTypeID,			
			0,		
			&nBufLen,			
			&pStdEquipInfo,			
			0);

		ASSERT(pStdEquipInfo);

		pLangText = pStdEquipInfo->pTypeName;

		ASSERT(pLangText);
		pDisBuf = pLangText->pAbbrName[nCurrentLangFlag];
	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_BARCODE_DEVICE)
	{
		LANG_TEXT  sDeviceName;

		nDeviceID = LCD_SPLIT_BAR_DEVICE_ID(nMenuItemID);
		int nMaxDeviceNum = DXI_GetDeviceNum();
		int nFirstRectID = DXI_GetFirstRectDeviceID();

		if(nDeviceID == 1)
		{
			pDisBuf = "ACU+";
		}
		else if(nDeviceID <= nMaxDeviceNum)
		{
			DXI_GetPIByDeviceID(nDeviceID, &sProductInfo);

			pDisBuf = sProductInfo.szDeviceAbbrName[nCurrentLangFlag];
		}
		//I2C Devices
		else if((nDeviceID >= nFirstRectID + 290) && (nDeviceID < nFirstRectID + 298))
		{
			pDisBuf = g_SiteInfo.stI2CProductInfo[nDeviceID - nFirstRectID - 290].szDeviceAbbrName[nCurrentLangFlag];
		}
		else if((nDeviceID >= nFirstRectID + 298) && (nDeviceID < nFirstRectID + 306))
		{
			pDisBuf = g_SiteInfo.stCANSMDUPProductInfo[nDeviceID - nFirstRectID - 298].szDeviceAbbrName[nCurrentLangFlag];
		}
		//Rs485
		else if((nDeviceID >= nFirstRectID + 306) && (nDeviceID < nFirstRectID + 368))
		{
			pDisBuf = g_SiteInfo.st485ProductInfo[nDeviceID - nFirstRectID - 306].szDeviceAbbrName[nCurrentLangFlag];
		}
		//Convertor
		else if(nDeviceID >= nFirstRectID + 368)
		{
			pDisBuf = g_SiteInfo.stCANConverterProductInfo[nDeviceID - nFirstRectID - 368].szDeviceAbbrName[nCurrentLangFlag];

		}
		else
		{
			pDisBuf = "Unknow Device";
		}

	}
	else if(LCD_SPLIT_PREFIX(nMenuItemID) == PREFIX_CUSTOMIZE_MENU)
	{
		int nPageID = LCD_SPLIT_ACT_EQUIP_ID(nMenuItemID);

		USER_DEF_PAGES *pUserDefPages = NULL;

		DxiGetData(VAR_USER_DEF_PAGES,
			0,			
			0,		
			&nBufLen,			
			&pUserDefPages,			
			0);

		ASSERT(pUserDefPages);
		PAGE_NAME *pPageName = pUserDefPages->pPageInfo->pPageName;
		for(i = 0; 
			(i < pUserDefPages->pPageInfo->iPageInfoNum) && pPageName;
			i++, pPageName++)
		{
			if(pPageName->iPageID == nPageID)
			{
				break;
			}
		}
		ASSERT(i < pUserDefPages->pPageInfo->iPageInfoNum); //һ��Ҫ�ҵ���
		ASSERT(pPageName);

		pDisBuf = pPageName->pAbbrName[nCurrentLangFlag];

	}

	else
	{
		DEBUG_LCD_FILE_FUN_LINE_HEX("SubMenuItem_ID", nMenuItemID);
	}

	ASSERT(pDisBuf);

#ifdef DISP_RECT_POSITION_NO
	if((nEquipID >= RECT_EQUIP_ID_MIN) && (nEquipID <= RECT_EQUIP_ID_MAX))
	{
		int		iPosNo = LUI_GetDwordSigValue(nEquipID, SIG_TYPE_SAMPLING, SIG_ID_POSTION_NO);
		char	acPosNo[10];

		//When SCU+ start, PositionNo maybe is 0, which does not need disply
		if((iPosNo > 0) && (iPosNo < 1000))
		{
			snprintf(acPosNo, sizeof(acPosNo), "%03d", iPosNo);

			snprintf(acDisBuf, sizeof(acDisBuf), "%s(%s)", pLangText->pAbbrName[nCurrentLangFlag], acPosNo);

			pDisBuf = acDisBuf;
		}
	}
#endif

	nDrawWidth = GetWidthOfString(pDisBuf, (int)strlen(pDisBuf), pLangCode);

	//Display string



	return pDisBuf;

}

static BOOL UpdateActEquipNum(void)
{
	BOOL bNeedRefresh = FALSE;

	int nBufLen;
	SIG_BASIC_VALUE		*pSigValue;

	//Rectifier Number
	if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID),	
		SIG_ID_ACTUAL_RECT_NUM,
		&nBufLen,			
		&pSigValue,			
		0))
	{
		if(pSigValue->varValue.ulValue != g_s_ActEquipNum.nRectNum)
		{
			g_s_ActEquipNum.nRectNum = pSigValue->varValue.ulValue;
			bNeedRefresh = TRUE;
		}
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get Rectifier number.");
	}

	//Slave Rect
	int i;
	for(i = 0; i < MAX_SLAVE_NUM; i++)
	{
		if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
			DXI_GetEquipIDFromStdEquipID(SLAVE1_EQUIP_TYPE + 100 * i),	
			SIG_ID_ACTUAL_SLAVE_RECT_NUM,
			&nBufLen,			
			&pSigValue,			
			0))
		{
			if(pSigValue->varValue.ulValue != g_s_ActEquipNum.anSlaveRectNum[i])
			{
				g_s_ActEquipNum.anSlaveRectNum[i] = pSigValue->varValue.ulValue;
				bNeedRefresh = TRUE;
			}
		}
		else
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get Slave Rect number.");
			DEBUG_LCD_FILE_FUN_LINE_INT("Slave", i);
		}
	}


	//Converter Number
	if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
		CONVERTER_GROUP_EQUIP_ID,	
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_CONVERTER_NUM),
		&nBufLen,			
		&pSigValue,			
		0))
	{
		if(pSigValue->varValue.ulValue != g_s_ActEquipNum.nConvterNum)
		{
			g_s_ActEquipNum.nConvterNum = pSigValue->varValue.ulValue;
			bNeedRefresh = TRUE;
		}
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get Converter number.");
	}

	////Main Board Battery Number
	if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
		BATT_GROUP_EQUIP_ID,	
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SIG_ID_MB_BATT_NUM),
		&nBufLen,			
		&pSigValue,			
		0))
	{
		if(pSigValue->varValue.ulValue != g_s_ActEquipNum.nMBBattNum)
		{
			g_s_ActEquipNum.nMBBattNum = pSigValue->varValue.ulValue;
			bNeedRefresh = TRUE;
		}
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get MainBoard Battery number.");
	}

	//EIB Battery Number
	for (i = 0; i < EIB_NUM_MAX; i++)
	{
		if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
			EIB1_EQUIP_ID + i,	
			DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SIG_ID_EIB_BATT_NUM),
			&nBufLen,			
			&pSigValue,			
			0))
		{
			if(pSigValue->varValue.ulValue != g_s_ActEquipNum.anEIBBattNum[i])
			{
				g_s_ActEquipNum.anEIBBattNum[i] = pSigValue->varValue.ulValue;
				bNeedRefresh = TRUE;

				break;
			}
		}
		else
		{
			//	DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get EIB1 Battery number.");
		}
	}

	//SMDU Number
	if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(SMDU_GROUP_EQUIP_TYPE),	
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_SMDU_NUM),
		&nBufLen,			
		&pSigValue,			
		0))
	{
		if(pSigValue->varValue.ulValue != g_s_ActEquipNum.nSMDUNum)
		{
			g_s_ActEquipNum.nSMDUNum = pSigValue->varValue.ulValue;
			bNeedRefresh = TRUE;
		}
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get SMDU number.");
	}

	return bNeedRefresh;
}










static BOOL ChangeEquipMenuNumToActual(void)
{
	BOOL bEquipChanged = UpdateActEquipNum();
	BOOL bSignalChanged = FALSE;

	int nBufLen;
	SIG_BASIC_VALUE		*pSigValue;


	//SMDU CONFIG CHANGE
#define		SMDU_CONFIG_NOT_CHANGED		0
#define		SMDU_CONFIG_CHANGED			1

	if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(SMDU_GROUP_EQUIP_TYPE),	
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_SMDU_CONFIG_CHANGE),
		&nBufLen,			
		&pSigValue,			
		0))
	{
		if(pSigValue->varValue.enumValue == SMDU_CONFIG_CHANGED)
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("SMDU's config has changed.");

			bSignalChanged = TRUE;

			VAR_VALUE value;
			value.enumValue = SMDU_CONFIG_NOT_CHANGED;

			SetSignalValueInterface(
				DXI_GetEquipIDFromStdEquipID(SMDU_GROUP_EQUIP_TYPE),	
				SIG_TYPE_SAMPLING,
				SIG_ID_SMDU_CONFIG_CHANGE, 
				value,
				SERVICE_OF_LOGIC_CONTROL,
				EQUIP_CTRL_SEND_URGENTLY,
				0);	
			Sleep(2000);
		}
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get the signal of SMDU config changed.");
	}

	// Signal Display status changed Message
#define	WAIT_TIME_CHECKING_LCD_MESSAGE	10

	MESSAGE_TO_UI	stMessageToLcd;
	if(ERR_QUEUE_OK == Queue_Get(g_SiteInfo.hLcdMessageQueue, 
		&stMessageToLcd, 
		FALSE, 
		WAIT_TIME_CHECKING_LCD_MESSAGE))
	{
		bSignalChanged = TRUE;
	}

	if(bEquipChanged)
	{
		RefreshSubMenuStatus(g_mdMenuDisplay.pMainMenu->pRootMenu, TRUE);
	}
	else if(bSignalChanged)
	{
		RefreshSubMenuStatus(g_mdMenuDisplay.pMainMenu->pCurMenu, FALSE);
	}

	return bEquipChanged || bSignalChanged;
}

PRODUCT_INFO* QueryProductInfo(int nDeviceID)
{
	static PRODUCT_INFO		s_stProductInfo;
	PRODUCT_INFO*			pProductInfo = NULL;
	
	int nMaxDeviceNum = DXI_GetDeviceNum();

	memset(&s_stProductInfo, 0, sizeof(s_stProductInfo));
	pProductInfo = &s_stProductInfo;

	if(nDeviceID < 1)
	{
		pProductInfo = pProductInfo;
	}
	else if(nDeviceID == 1)//Power System
	{
		pProductInfo = DXI_GetDeviceProductInfo();
	}
	else if(nDeviceID <= nMaxDeviceNum)
	{
		DXI_GetPIByDeviceID(nDeviceID, &s_stProductInfo);
		pProductInfo = &s_stProductInfo;
	}
	else 
	{
		int nProductInfoIndex = nDeviceID - nMaxDeviceNum;
		int nOffset;

		if((nProductInfoIndex >= 1) 
			&& (nProductInfoIndex <= LUI_PRODUCTINFO_TOTAL_NUM))
		{
			if((nProductInfoIndex >= LUI_PRODUCTINFO_START_I2C) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_I2C))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_I2C;
				pProductInfo = &g_SiteInfo.stI2CProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_RS485) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_RS485))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_RS485;
				pProductInfo = &g_SiteInfo.st485ProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_SMDUP) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_SMDUP))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_SMDUP;
				pProductInfo = &g_SiteInfo.stCANSMDUPProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_CONVERTER) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_CONVERTER))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_CONVERTER;
				pProductInfo = &g_SiteInfo.stCANConverterProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_SMTEMP) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_SMTEMP))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_SMTEMP;
				pProductInfo = &g_SiteInfo.stCANSMTempProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_MPPT) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_MPPT))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_MPPT;
				pProductInfo = &g_SiteInfo.stCANMpptProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_RECT) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_RECT))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_RECT;
				pProductInfo = &g_SiteInfo.stCANRectProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_SMDU) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_SMDU))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_SMDU;
				pProductInfo = &g_SiteInfo.stCANSMDUProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_SMDUH) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_SMDUH))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_SMDUH;
				pProductInfo = &g_SiteInfo.stCANSMDUHProductInfo[nOffset];
			}
			else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_DCEM) 
				&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_DCEM))
			{
				nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_DCEM;
				pProductInfo = &g_SiteInfo.stCANDCEMProductInfo[nOffset];
			}
		}
	}
	
	return pProductInfo;
}




static BOOL	IsBarcodeDisplay(int nDeviceID)
{
	BOOL bDisplay = FALSE;
#if 0
	int	iError;
	int	iEquipID = 1;//SMDU Sampler Status
	int	iSignalType = 2;
	int	iSignalID = 183;
	int	iBufLen;
	int	iSMDUSamplerStatus;
	int	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	SIG_BASIC_VALUE* pSigValue;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		5000);
	if(iError != ERR_DXI_OK)
	{
		iSMDUSamplerStatus = 0;

	}
	else
	{
		iSMDUSamplerStatus = pSigValue->varValue.enumValue;

	}
#endif

	PRODUCT_INFO*	pProductInfo = NULL;

	if(1 == nDeviceID)
	{
		bDisplay = TRUE;
	}
	else
	{
		pProductInfo = QueryProductInfo(nDeviceID);

		if(pProductInfo->bSigModelUsed)
		{
			bDisplay = TRUE;
		}
		else
		{
			bDisplay = FALSE;
		}
	}

	return bDisplay;



#if 0
	int nFirstRect = DXI_GetFirstRectDeviceID();
	//int nRealRect = DXI_GetRectRealNumber();

	//Power System
	if(nDeviceID == 1)
	{
		bDisplay = TRUE;
	}
	//Rectifier
	else if((nDeviceID >= nFirstRect) && (nDeviceID < nFirstRect + 100))
	{
		bDisplay = (nDeviceID - nFirstRect < g_s_ActEquipNum.nRectNum);
	}
	//SMDU
	else if((nDeviceID >= nFirstRect + 100) && (nDeviceID < nFirstRect + 110))
	{
		if(iSMDUSamplerStatus == 0)
		{
			if((nDeviceID >= nFirstRect + 100) && (nDeviceID < nFirstRect + 108))
			{
				bDisplay = (nDeviceID - nFirstRect - 100 < g_s_ActEquipNum.nSMDUNum);
			}

		}
		else
		{
			if((nDeviceID >= nFirstRect + 102) && (nDeviceID < nFirstRect + 110))
			{
				bDisplay = (nDeviceID - nFirstRect - 102 < g_s_ActEquipNum.nSMDUNum);
			}

		}

	}

	//Slave1 Rect
	else if((nDeviceID >= nFirstRect + 110) && (nDeviceID < nFirstRect + 170))
	{
		bDisplay = (nDeviceID - nFirstRect - 110 < g_s_ActEquipNum.anSlaveRectNum[0]);
	}
	//Slave2 Rect
	else if((nDeviceID >= nFirstRect + 170) && (nDeviceID < nFirstRect + 230))
	{
		bDisplay = (nDeviceID - nFirstRect - 170 < g_s_ActEquipNum.anSlaveRectNum[1]);
	}
	//Slave3 Rect
	else if((nDeviceID >= nFirstRect + 230) && (nDeviceID < nFirstRect + 290))
	{
		bDisplay = (nDeviceID - nFirstRect - 230 < g_s_ActEquipNum.anSlaveRectNum[2]);
	}
	//I2C
	else if((nDeviceID >= nFirstRect + 290) && (nDeviceID < nFirstRect + 298))
	{
		bDisplay = g_SiteInfo.stI2CProductInfo[nDeviceID - nFirstRect - 290].bSigModelUsed;
	}
	else if((nDeviceID >= nFirstRect + 298) && (nDeviceID < nFirstRect + 306))
	{
		bDisplay = g_SiteInfo.stCANSMDUPProductInfo[nDeviceID - nFirstRect - 298].bSigModelUsed;
	}
	//RS484
	else if((nDeviceID >= nFirstRect + 306) && (nDeviceID < nFirstRect + 368))
	{
		bDisplay = g_SiteInfo.st485ProductInfo[nDeviceID - nFirstRect - 306].bSigModelUsed;
	}
	//Convertor
	else if(nDeviceID >= nFirstRect + 368)
	{
		bDisplay = g_SiteInfo.stCANConverterProductInfo[nDeviceID - nFirstRect - 368].bSigModelUsed;

	}
	//Other
	else
	{
		bDisplay = FALSE;
	}
	return bDisplay;
#endif

}

static BOOL	GetConvIDThruDeviceID(int nDeviceID)
{
	int nFirstRect = DXI_GetFirstRectDeviceID();
		
	//Convertor
	if(nDeviceID >= nFirstRect + 368 && nDeviceID < nFirstRect + 368 + 48)
	{
		return nDeviceID - nFirstRect - 368 + 211;
	}
	else
	{
		return -1;
	}
	
}
static BOOL IsMenuItemDisplay(MENU_ITEM* pMenuItem)
{
#ifdef __DISPLAY_ALL_MENU_ITEM
	return TRUE;
#endif
	ASSERT(pMenuItem);

	BOOL bDisplay = TRUE;

	SUB_MENU *pSubMenu;

	int	iError;
	int nBufLen;
	SIG_BASIC_VALUE		*pSigValue;

	int nEquipID, nSigType, nSigID;
	int nDeviceID;

	int	nPrefixID;

	EQUIP_INFO* pEquipInfo;

	//begin added by Jimmy for shielding setting signals for Slave ACU+	
#ifdef WORK_AS_SLAVE_ENABLE

	#if 0
		if(WORK_MODE_SLAVE == g_iWorkMode)
		{
			switch(pMenuItem->cMenuType)
			{
			case MT_SUBMENU:
				nPrefixID = LCD_SPLIT_PREFIX(pMenuItem->nMenuItemID);
				nEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);
				//printf("\n*****The curr Prefix and ID are: %d , %d \n",nPrefixID,nEquipID);
				if(nPrefixID == PREFIX_FIXED_LANG_RES )
				{
					if((pMenuItem->nMenuItemID != LCD_MERGE_FIXED_LANG_ID(MAIN_CUSTOMIZE_LANG_ID))
						/*&& (pMenuItem->nMenuItemID != LCD_MERGE_FIXED_LANG_ID(MAIN_RUNINFO_LANG_ID))*/// 201108����ʾ��������Ϣ��
					  )
					{
						return FALSE;
					}
					else
					{
						
					}
				}
				else if(nPrefixID == PREFIX_CUSTOMIZE_MENU)// �Զ���˵�����ʾ				
				{
					if(pMenuItem->nMenuItemID != LCD_MERGE_CUSTOMIZE_MENU_ID(MAIN_QUICK_SETTING_LANG_ID))
					{
						return FALSE;
					}
				}	
				else if(nPrefixID == PREFIX_DIVIDED_SUB_MENU)
				{
					return FALSE;
				}
				break;
			default:			
				break;
			}
		
		}
	#else
		if(WORK_MODE_SLAVE == g_iWorkMode)
		{
			if(MT_SUBMENU == pMenuItem->cMenuType)
			{
				if( pMenuItem->nMenuItemID == LCD_MERGE_FIXED_LANG_ID(MAIN_SET_LANG_ID) )//����ʾ�Ǵӻ�ģʽ�µ����ò˵�
				{
					return FALSE;
				}
			}
		}
		else
		{
				if( pMenuItem->nMenuItemID == LCD_MERGE_ACTUAL_EQUIP_ID(0, MAIN_SLAVE_SET_LANG_ID))//����ʾ�ӻ�ģʽ�µ����ò˵�
				{
				 
				return FALSE;
				}
		}
	#endif
#endif
	//end Add for slave display shield
	switch(pMenuItem->cMenuType)
	{
	case MT_SUBMENU:
		{
			pSubMenu = (SUB_MENU *)(pMenuItem->pvItemData);

			if(pSubMenu == NULL)
			{
				return TRUE;
			}

			nPrefixID = LCD_SPLIT_PREFIX(pMenuItem->nMenuItemID);
			nEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

			if(nPrefixID == PREFIX_FIXED_LANG_RES)
			{
				//�����û��Զ���˵����ͽ��ܲ˵����澯��ƽ
				//���û����������ʾ
				if((pMenuItem->nMenuItemID == LCD_MERGE_FIXED_LANG_ID(MAIN_CUSTOMIZE_LANG_ID))
					|| (pMenuItem->nMenuItemID == LCD_MERGE_FIXED_LANG_ID(MAIN_ENERGY_SAVING_LANG_ID))
					|| (pMenuItem->nMenuItemID == LCD_MERGE_FIXED_LANG_ID(ALARM_VOLTAGE_IB_LANG_ID)))
				{
					bDisplay = (pSubMenu->nDispItems > 0);
				}
				else
				{
					bDisplay = TRUE;
				}
			}

			else if(nPrefixID == PREFIX_ACTUAL_EQUIP)
			{

				// Rectifier
				if((nEquipID >= RECT_EQUIP_ID_MIN) && (nEquipID <= RECT_EQUIP_ID_MAX))
				{
					bDisplay = (nEquipID - RECT_EQUIP_ID_MIN < g_s_ActEquipNum.nRectNum);
				}
				// Converter
				else if((nEquipID >= CONVERTER_EQUIP_ID_MIN) && (nEquipID <= CONVERTER_EQUIP_ID_MAX))
				{
					bDisplay = (nEquipID - CONVERTER_EQUIP_ID_MIN < g_s_ActEquipNum.nConvterNum);
				}
				//2009-12-26 �Ƿ���ʾȡ���ڹ���״̬λ
				//// Battery 1:
				else if(nEquipID == BATTERY1_EQUIP_ID)
				{
					bDisplay = (g_s_ActEquipNum.nMBBattNum >= 1);
				}
				//// Battery 2
				else if(nEquipID == BATTERY2_EQUIP_ID)
				{
					bDisplay = (g_s_ActEquipNum.nMBBattNum >= 2);
				}
				// EIB Battery 1 ~ 2
				else if((nEquipID >= EIB1_BATTERY1_EQUIP_ID) && (nEquipID <= EIB4_BATTERY2_EQUIP_ID))
				{
					ASSERT((EIB4_BATTERY2_EQUIP_ID - EIB1_BATTERY1_EQUIP_ID + 1) == EIB_NUM_MAX * 2);
					bDisplay = ((nEquipID - EIB1_BATTERY1_EQUIP_ID) % 2 < g_s_ActEquipNum.anEIBBattNum[(nEquipID - EIB1_BATTERY1_EQUIP_ID) / 2]);
				}
				// EIB Battery 3
				else if((nEquipID >= EIB1_BATTERY3_EQUIP_ID) && (nEquipID <= EIB4_BATTERY3_EQUIP_ID))
				{
					bDisplay = g_s_ActEquipNum.anEIBBattNum[nEquipID - EIB1_BATTERY3_EQUIP_ID] >= 3;
				}
				else	//Other Actual Equip
				{
					pEquipInfo = LCD_GetEquipInfo(nEquipID);
					if(pEquipInfo != NULL)
					//if(ERR_DXI_OK == DxiGetData(VAR_A_EQUIP_INFO,
					//	nEquipID,
					//	0,
					//	&nBufLen,			
					//	&pEquipInfo,			
					//	0))
					{
						bDisplay = pEquipInfo->bWorkStatus;
					}
					else
					{
						bDisplay = TRUE;
					}

				}
			}
			else if(nPrefixID == PREFIX_VIRTUAL_GROUP_EQUIP)
			{
				// Rectifier Group VIRTUAL_GROUP_EQUIP
				if(nEquipID == RECT_GROUP_EQUIP_TYPE)
				{
					bDisplay = TRUE;
				}
				else
				{
					bDisplay = (pSubMenu->nDispItems > 0);
				}
			}
			else if(nPrefixID == PREFIX_BARCODE_DEVICE)
			{
				nDeviceID = nEquipID;
				bDisplay = IsBarcodeDisplay(nDeviceID);
			}
			else
			{
				bDisplay = (pSubMenu->nDispItems > 0);
			}
		}
		break;

	case MT_SIGNAL_DISPLAY:		//NOT break, continue
	case MT_SIGNAL_SET:			//NOT break, continue
	case MT_SIGNAL_CONTROL:		//NOT break, continue
	case MT_SIGNAL_CUSTOMIZE:
		{
			DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID, nEquipID, nSigType, nSigID);

			iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				nEquipID,			
				DXI_MERGE_SIG_ID(nSigType, nSigID),
				&nBufLen,			
				(void *)&pSigValue,			
				0);

			if (iError == ERR_DXI_OK)
			{
				bDisplay = SIG_VALUE_IS_CONFIGURED(pSigValue) && SIG_VALUE_NEED_DISPLAY(pSigValue);
			}
			else
			{	
				//DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get the value of signal.");
				//DEBUG_LCD_FILE_FUN_LINE_HEX("SignalID", pMenuItem->nMenuItemID);
				//Frank
				//printf("IsMenuItemDisplay failed, nEquipID=%d,nSigType=%d, nSigID=%d\n",nEquipID, nSigType, nSigID);
				bDisplay = TRUE;
			}

			//�����ź��������豸�Ƿ����
			// Rectifier
			if((nEquipID >= RECT_EQUIP_ID_MIN) && (nEquipID <= RECT_EQUIP_ID_MAX))
			{
				bDisplay &= (nEquipID - RECT_EQUIP_ID_MIN < g_s_ActEquipNum.nRectNum);
			}
			// Converter
			else if((nEquipID >= CONVERTER_EQUIP_ID_MIN) && (nEquipID <= CONVERTER_EQUIP_ID_MAX))
			{
				bDisplay &= (nEquipID - CONVERTER_EQUIP_ID_MIN < g_s_ActEquipNum.nConvterNum);
			}
			else if(nEquipID == BATTERY1_EQUIP_ID)
			{
				bDisplay &= (g_s_ActEquipNum.nMBBattNum > 0);
			}
			else if(nEquipID == BATTERY2_EQUIP_ID)
			{
				bDisplay &= (g_s_ActEquipNum.nMBBattNum == 2);
			}
			else if((nEquipID >= EIB1_BATTERY1_EQUIP_ID) && (nEquipID <= EIB4_BATTERY2_EQUIP_ID))
			{
				bDisplay &= ((nEquipID - EIB1_BATTERY1_EQUIP_ID) % 2 < g_s_ActEquipNum.anEIBBattNum[(nEquipID - EIB1_BATTERY1_EQUIP_ID) / 2]);
			}
			else if((nEquipID >= EIB1_BATTERY3_EQUIP_ID) && (nEquipID <= EIB4_BATTERY3_EQUIP_ID))
			{
				bDisplay &= g_s_ActEquipNum.anEIBBattNum[nEquipID - EIB1_BATTERY3_EQUIP_ID] >= 3;
			}
			else
			{
				pEquipInfo = LCD_GetEquipInfo(nEquipID);
				if(pEquipInfo != NULL)
				//if(ERR_DXI_OK == DxiGetData(VAR_A_EQUIP_INFO,
				//	nEquipID,
				//	0,
				//	&nBufLen,			
				//	&pEquipInfo,			
				//	0))
				{
					bDisplay &= pEquipInfo->bWorkStatus;
				}
			}
		}
		break;

	case MT_BARCODE:
		{
			DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID, nDeviceID, nSigType, nSigID);
			bDisplay = IsBarcodeDisplay(nDeviceID);
		}
		break;

	case MT_ALARM_LEVEL_SET:
	case MT_ALARM_RELAY_SET:
		//���еĵ�ظ澯����ֻ��ʾһ�飬��SM BATT���ź��ر�࣬���ܺ϶�Ϊһ������ֻ�ö���ʾ������
		//{
		//	DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID, nEquipID, nSigType, nSigID);
		//	if((GET_EQUIP_TYPE_ID_MAJOR(nEquipID) == GET_EQUIP_TYPE_ID_MAJOR(BATT_GROUP_EQUIP_TYPE))
		//		&& (GET_EQUIP_TYPE_ID_SUB(nEquipID) > 1))	//Other Battery
		//	{
		//		bDisplay = FALSE;
		//	}
		//}
		//break;

	case MT_ACTIVE_ALARM_DISPLAY:
	case MT_HISTORY_ALARM_DISPLAY:
	case MT_PASSWORD_SELECT:
	case MT_SELF_TEST:
		{
			bDisplay = TRUE;
		}
		break;

	case MT_SELF_DEF_SET:
		{
			//�ų���ؼƻ�������������
			DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID, nEquipID, nSigType, nSigID);
			if((nEquipID == BATT_GROUP_EQUIP_ID) && (nSigType == SIG_TYPE_SETTING))
			{
				if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipID,			
					DXI_MERGE_SIG_ID(nSigType, nSigID),
					&nBufLen,			
					(void *)&pSigValue,			
					0))
				{
					bDisplay = SIG_VALUE_IS_CONFIGURED(pSigValue) && SIG_VALUE_NEED_DISPLAY(pSigValue);
				}
				else
				{
					bDisplay = TRUE;
				}
			}
			//���⴦��"������Ļ��С"
			else if((LCD_SPLIT_PREFIX(pMenuItem->nMenuItemID) == PREFIX_FIXED_LANG_RES)
				&& (LCD_SPLIT_LANG_RES_ID(pMenuItem->nMenuItemID) == CHANGE_LCD_HEIGHT_SET_ID))
			{
				//�����״̬�����
				if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
					1,				//PowerSystem			
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 38),//38		Test State Set
					&nBufLen,			
					(void *)&pSigValue,			
					0))
				{
					bDisplay = pSigValue->varValue.enumValue;
				}
				else
				{
					bDisplay = FALSE;
				}
			}
			//���⴦��"������Ļ�Ƕ�"
			else if((LCD_SPLIT_PREFIX(pMenuItem->nMenuItemID) == PREFIX_FIXED_LANG_RES)
				&& (LCD_SPLIT_LANG_RES_ID(pMenuItem->nMenuItemID) == SET_LCD_ROTATION_SET_ID))
			{
				bDisplay = IS_HIGH_SCREEN;
			}
			//����
			else
			{
				bDisplay = TRUE;
			}
		}
		break;

	default:
		bDisplay = TRUE;
		break;

	}
	return bDisplay;
}


int RefreshSubMenuStatus(SUB_MENU *pSubMenu, BOOL bRecursive)
{
	//Assume bRecursive is TRUE
	//UNUSED(bRecursive);

	if((pSubMenu == NULL)
		|| (pSubMenu->pItems->cMenuType == MT_ACTIVE_ALARM_DISPLAY)
		|| (pSubMenu->pItems->cMenuType == MT_HISTORY_ALARM_DISPLAY)
		|| (pSubMenu->pItems->cMenuType == MT_PASSWORD_SELECT))
	{
		return ERR_LCD_OK;
	}

	MENU_ITEM* pMenuItem;
	int i;

	//ˢ��ÿһ���˵������ʾ״̬
	for(i = 0; i < pSubMenu->nItems; i++)
	{
		pMenuItem = pSubMenu->pItems + i;
		ASSERT(pMenuItem);

		if(bRecursive && IS_SUBMENU_ITEM(pMenuItem))
		//if(IS_SUBMENU_ITEM(pMenuItem))
		{
			SUB_MENU *pSubSubMenu = (SUB_MENU *)(pMenuItem->pvItemData);
			RefreshSubMenuStatus(pSubSubMenu, bRecursive);
		}
		pMenuItem->bDisplayFlag = IsMenuItemDisplay(pMenuItem);
	}
	//begin By Jimmy Wu for entering Slave mode menu 20110707
#ifdef WORK_AS_SLAVE_ENABLE	
#define		SYS_EQUIP_ID		1
#define		SIG_TYPE_SETTING	2
#define		WORK_MODE_SIG_ID	180	 
	if(!bRecursive) //����ÿ���ظ�ˢ��ֻ��ˢ�µ�ǰʱ��ˢһ��
	{			
		SIG_ENUM sig_Ret = GetEnumSigValue(SYS_EQUIP_ID, SIG_TYPE_SETTING,WORK_MODE_SIG_ID,"Work Mode");
		if(sig_Ret >= WORK_MODE_SEPERATE && sig_Ret <= WORK_MODE_SLAVE)
		{
			g_iWorkMode = sig_Ret;
		}
		else
		{
			g_iWorkMode = WORK_MODE_SEPERATE; //Ĭ�϶���ģʽ
		}
		//printf("\n**********Now the Work Mode is %d \n",g_iWorkMode);		
	}
	//end adding for Slave mode access point
#endif	

	//������֯��ʾ�Ĳ˵���

	pSubMenu->nDispItems = 0;

	for(i = 0; i < pSubMenu->nItems; i++)
	{
		pMenuItem = pSubMenu->pItems + i;

		if(pMenuItem->bDisplayFlag)
		{
			*(pSubMenu->pDispItems + pSubMenu->nDispItems++) = i;
		}
	}

	//������ʾ�ĵط���Ϊ-1
	for(i = pSubMenu->nDispItems; i < pSubMenu->nItems; i++)
	{
		*(pSubMenu->pDispItems + i) = -1;
	}

	return ERR_LCD_OK;
}


EQUIP_INFO* LCD_GetEquipInfo(int iEquipID)
{
	int nBufLen;
	EQUIP_INFO *pEquipInfo;

	if(ERR_DXI_OK == DxiGetData(VAR_A_EQUIP_INFO,
		iEquipID,
		0,
		&nBufLen,			
		&pEquipInfo,			
		0))
	{
		return pEquipInfo;
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT get Equip Info");
		DEBUG_LCD_FILE_FUN_LINE_INT("nEquipID" ,iEquipID);

		return NULL;
	}
}

/*==========================================================================*
 * FUNCTION : GetLangCode
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nLaguageFlag : 
 * RETURN   : char* : 
 * COMMENTS : 
 * CREATOR  :                  DATE: 2006-06-13 09:05
 *==========================================================================*/
char* GetLangCode(int nLaguageFlag)
{
	char*	pLangCode;

	//static char* s_pLocalLangCode = NULL;//This is an error for three langs
	static char* s_pLocalLangCode = NULL;
	static char* s_pLocal2LangCode = NULL;

	if(nLaguageFlag == LOCAL_LANGUAGE_FLAG)
	{
		//In order to enhance efficiency, only get the local code one time
		if (s_pLocalLangCode == NULL)
		{
			int		nBufLen;

			DxiGetData(VAR_ACU_PUBLIC_CONFIG,
				LOCAL_LANGUAGE_CODE, 
				0, 
				&nBufLen,
				&(s_pLocalLangCode),
				0);
		}

		pLangCode = s_pLocalLangCode;
	}
	else if(nLaguageFlag == LOCAL2_LANGUAGE_FLAG)
	{
		//In order to enhance efficiency, only get the local code one time
		if (s_pLocal2LangCode == NULL)
		{
			int		nBufLen;

			DxiGetData(VAR_ACU_PUBLIC_CONFIG,
				LOCAL2_LANGUAGE_CODE, 
				0, 
				&nBufLen,
				&(s_pLocal2LangCode),
				0);
		}

		pLangCode = s_pLocal2LangCode;

	}
	else if(nLaguageFlag == ENGLISH_LANGUAGE_FLAG)
	{
		pLangCode = LCD_FIXED_LANGUAGE;//english is fixed language
	}
	else
	{
		pLangCode = "";
	}

	return pLangCode;
}
/*==========================================================================*
* FUNCTION : GetCurrentLangCode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : char* : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:20
*==========================================================================*/
char* GetCurrentLangCode(void)
{
	return GetLangCode(g_nCurrentLangFlag);	
}

/*==========================================================================*
* FUNCTION : GetCurrentLangFlag
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:20
*==========================================================================*/
BOOL GetCurrentLangFlag(void)
{
	return g_nCurrentLangFlag;
}


/*==========================================================================*
* FUNCTION : SetCurrentLangFlag
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BOOL  bLangFlag : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:20
*==========================================================================*/
BOOL SetCurrentLangFlag(int iLangFlag)
{
	g_nCurrentLangFlag = iLangFlag;

	return TRUE;
}

//if bOnlyChangeFlag != TRUE, need to save the flag to flash
BOOL SetCurrentLang(int nCurrentLangValue, BOOL bOnlyChangeFlag)
{
	int	nChangeLangFlag;

	if (!bOnlyChangeFlag)
	{
		int nEquipID, nSigType, nSigID;

		VAR_VALUE value;

		nEquipID = DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);
		DXI_SPLIT_SIG_ID(SIG_ID_LANGUAGE_SUPPORT, nSigType, nSigID);

		value.enumValue = nCurrentLangValue;

		SetSignalValueInterface(nEquipID, 
			nSigType, nSigID, value, 
			SERVICE_OF_LOGIC_CONTROL,
			EQUIP_CTRL_SEND_URGENTLY,
			0);		
	}

	if(nCurrentLangValue == ENGLISH_LANGUAGE_FLAG)
	{
		nChangeLangFlag = ENGLISH_LANGUAGE_FLAG;
	}
	else if(nCurrentLangValue == LOCAL_LANGUAGE_FLAG)
	{
		nChangeLangFlag = LOCAL_LANGUAGE_FLAG;
	}
	else
	{
		nChangeLangFlag = LOCAL2_LANGUAGE_FLAG;
	}
	SetCurrentLangFlag(nChangeLangFlag);

	TRACE("SetCurrentLangFlag is %d\n\n", nChangeLangFlag);

	return TRUE;
}



BOOL SetBuzzerBeepThredID(HANDLE hThreadID)
{
	g_gvThisService.hThreadBuzzerBeep = hThreadID;

	return TRUE;
}
/*==========================================================================*
* FUNCTION : GetLCDLangFile
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : LANG_FILE* : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:20
*==========================================================================*/
LANG_FILE* GetLCDLangFile(void)
{
	return &(g_stLcdPrivateCfgLoader.stLCDLangFile);
}


/*==========================================================================*
* FUNCTION : GetDefMenuCfgReader
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : DEFAULT_SCREEN_CFG_READER* : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:20
*==========================================================================*/
DEFAULT_SCREEN_CFG_READER* GetDefMenuCfgReader(void)
{
	if(IS_LOW_SCREEN)
	{
#ifdef WORK_AS_SLAVE_ENABLE	

		SIG_ENUM sig_Ret = GetEnumSigValue(SYS_EQUIP_ID, SIG_TYPE_SETTING,WORK_MODE_SIG_ID,"Work Mode");
		if(sig_Ret >= WORK_MODE_SEPERATE && sig_Ret <= WORK_MODE_SLAVE)
		{
			g_iWorkMode = sig_Ret;
		}
		else
		{
			g_iWorkMode = WORK_MODE_SEPERATE; //Ĭ�϶���ģʽ
		}
		
		if(WORK_MODE_SLAVE == g_iWorkMode)
		{
			return &(g_stLcdPrivateCfgLoader.stDefaultScreenCfgReader32_slave);
		}
		
#endif
	
		return &(g_stLcdPrivateCfgLoader.stDefaultScreenCfgReader32);
	}
	else
	{
		if(IS_HIGH_SCREEN)
		{
			return &(g_stLcdPrivateCfgLoader.stDefaultScreenCfgReader128);
		}
		else
		{
			return &(g_stLcdPrivateCfgLoader.stDefaultScreenCfgReader64);
		}
	}
}

















/*==========================================================================*
* FUNCTION : CreateMenus
* PURPOSE  : Create menus
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_DISPLAY*  pMenuDisplay : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:03
*==========================================================================*/
static int CreateMenus(MENU_DISPLAY* pMenuDisplay)
{

	int nError = ERR_LCD_OK;

	MENU_STRU* pMenu = NULL;

	ASSERT(pMenuDisplay);

	// 2.Create Default Menu  //It doesn't execute
	nError = CreateDefaultMenu(&pMenu);
	//It's unuseful
	pMenuDisplay->pDefaultMenu = pMenu;


	// 3.Create Main Menu
	//Frank Wu,20160127, for MiniNCU
	//nError = CreateMainMenu(&pMenu);
	nError = CreateMainMenuMiniNCU(&pMenu);
	pMenuDisplay->pMainMenu = pMenu;


	nError = CreatePasswordMenu(&pMenu);
	pMenuDisplay->pPasswordMenu = pMenu;

	//by HULONGWEN need feed dog
	return nError;
}


















/* define splitter character used by LCD config files */
#define SPLITTER		 ('\t')

/*==========================================================================*
* FUNCTION : ParsedDefScreenCfgTableProc
* PURPOSE  : Parse default screen config table
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char                     *szBuf                 : a line info
*            DEFAULT_SCREEN_ITEM_CFG  *pDefaultScreenItemCfg : to save cfg
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 17:24
*==========================================================================*/
static int ParsedDefScreenCfgTableProc(char *szBuf, 
									   DEFAULT_SCREEN_ITEM_CFG 
									   *pDefaultScreenItemCfg)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pDefaultScreenItemCfg);

	/* 0.jump Sequence id field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 1.Standard Equip ID for Signal */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

//#if 0//Do not check if pField is a invalid string
//	if ((*pField < '0') || (*pField > '9'))//maybe dis fixed string 
//	{
//		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
//			"[%s]--ParsedLcdLangFileTableProc: ERROR: "
//			"Standard Equip ID is not a number!\n\r", __FILE__);
//
//		return 1;    /* not a num, error */
//	}
//#endif

	pDefaultScreenItemCfg->nStdEquipID = atoi(pField);

	/* 2.Signal Type for Signal */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

//#if 0 //Do not check if pField is a invalid string
//	if ((*pField < '0') || (*pField > '9'))
//	{
//		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
//			"[%s]--ParsedLcdLangFileTableProc: ERROR: Signal Type is not "
//			"a number!\n\r", __FILE__);
//
//		return 2;    /* not a num, error */
//	}
//#endif

	pDefaultScreenItemCfg->nSigType = atoi(pField);

	/* 3.Signal ID  for Signal */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

//#if 0 //Do not check if pField is a invalid string
//
//	if ((*pField < '0') || (*pField > '9'))
//	{
//		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
//			"[%s]--ParsedLcdLangFileTableProc: ERROR: Signal ID is not "
//			"a number!\n\r", __FILE__);
//
//		return 3;    /* not a num, error */
//	}
//#endif 

	pDefaultScreenItemCfg->nSigID = atoi(pField);

	/* 4. x offset for Signal*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: "
			"x offset for Signal is not a number!\n\r", __FILE__);

		return 4;    /* not a num, error */
	}

	//pDefaultScreenItemCfg->x = atoi(pField);
	pDefaultScreenItemCfg->x = (atoi(pField) - 1) * BASE_HEIGHT;

	/* 5. y offset for Signal */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: "
			"y offset for Signal is not a number!\n\r", __FILE__);

		return 5;    /* not a num, error */
	}
	//pDefaultScreenItemCfg->y = atoi(pField);
	pDefaultScreenItemCfg->y = (atoi(pField) - 1) * GetFontHeightFromCode(GetCurrentLangCode());

	/* 8. display Big Size info or NOT */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: "
			"fixed prompt info language ID is not "
			"a number!\n\r", __FILE__);

		return 8;    /* not a num, error */
	}
	pDefaultScreenItemCfg->nDispBigSizeFlag = atoi(pField);

	/* 6. display prompt or value flag for Signal */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: "
			"display pompt or value flag for Signal is not "
			"a number!\n\r", __FILE__);

		return 6;    /* not a num, error */
	}
	pDefaultScreenItemCfg->nDisPromptOrValueFlag = atoi(pField);	

	if (pDefaultScreenItemCfg->nDisPromptOrValueFlag == GET_FIXED_PROMPT)
	{
		/* 7. display fixed prompt info language ID*/
		szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
		if ((*pField < '0') || (*pField > '9'))
		{
			AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParsedLcdLangFileTableProc: ERROR: "
				"fixed prompt info language ID is not "
				"a number!\n\r", __FILE__);

			return 7;    /* not a num, error */
		}

		pDefaultScreenItemCfg->nPromptInfoLangID = atoi(pField);
		
		
		/* 8. Equip ID */
		pDefaultScreenItemCfg->nEquipID = -1;
		
		szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
		if(*pField != '\0')
		{
			if ((*pField < '0') || (*pField > '9'))
			{
				AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
					"[%s]--ParsedLcdLangFileTableProc: ERROR: "
					"Equip ID is not "
					"a number!\n\r", __FILE__);

				return 8;    /* not a num, error */
			}
			pDefaultScreenItemCfg->nEquipID = atoi(pField);
		}
		
	}
	else
	{
		BOOL bParseFail = FALSE;
		
		/* 7. display fixed prompt info language ID*/
		if( !bParseFail )
		{
			szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
			if(*pField != '\0')//exist
			{
				if ((*pField < '0') || (*pField > '9'))
				{
					AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
						"[%s]--ParsedLcdLangFileTableProc: ERROR: "
						"fixed prompt info language ID is not "
						"a number!\n\r", __FILE__);

					return 7;    /* not a num, error */
				}

				pDefaultScreenItemCfg->nPromptInfoLangID = atoi(pField);
			}
			else
			{
				bParseFail = TRUE;
			}
		}

		/* 8. Equip ID */
		pDefaultScreenItemCfg->nEquipID = -1;
		
		if( !bParseFail )
		{
			szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
			if(*pField != '\0')
			{
				if ((*pField < '0') || (*pField > '9'))
				{
					AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
						"[%s]--ParsedLcdLangFileTableProc: ERROR: "
						"Equip ID is not "
						"a number!\n\r", __FILE__);

					return 8;    /* not a num, error */
				}
				pDefaultScreenItemCfg->nEquipID = atoi(pField);
			}
			else
			{
				bParseFail = TRUE;
			}
		}

	}
	

	return ERR_LCD_OK;
}


static int ParsedDividedSubMenuItemProc(char *szBuf,
				 DIVIDED_SUB_MENU_ITEM_CFG	*pDividedSubMenuItemCfg)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pDividedSubMenuItemCfg);

	/* 1.SubMenuItem Index */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pDividedSubMenuItemCfg->nSubMenuItemIndex = atoi(pField);

	/* 2.SubMenuItem Resouce ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pDividedSubMenuItemCfg->nResouceID = atoi(pField);

	/* 3.Equip ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pDividedSubMenuItemCfg->nEquipID = atoi(pField);

	/* 4.Signal Type for Signal */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pDividedSubMenuItemCfg->nSigType = atoi(pField);

	/* 5.Signal IDs String */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	strncpy(pDividedSubMenuItemCfg->caSigID, pField, DIVIDED_SUB_MENU_CHARS_MAX);

	return ERR_LCD_OK;
}

static int ParsedDividedSubMenuInfoProc(char *szBuf,
				 DIVIDED_SUB_MENU_INFO_CFG	*pDividedSubMenuInfoCfg)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pDividedSubMenuInfoCfg);

	/* 1.SubMenuItem Index */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pDividedSubMenuInfoCfg->nSubMenuInfoIndex = atoi(pField);

	/* 2.Equip ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pDividedSubMenuInfoCfg->nEquipID = atoi(pField);

	/* 3.Signal Type for Signal */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pDividedSubMenuInfoCfg->nSigType = atoi(pField);

	/* 4.SubMenu position flag */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pDividedSubMenuInfoCfg->nPositionFlag = atoi(pField);

	/* 5.Signal IDs String */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	strncpy(pDividedSubMenuInfoCfg->caMenuItemIndex, pField, DIVIDED_SUB_MENU_CHARS_MAX);

	return ERR_LCD_OK;
}


static int ParsedChgMenuItemResProc(char *szBuf,
				CHANGE_MENU_ITEM_RESOURCE_CFG	*pChgMenuItemResCfg)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pChgMenuItemResCfg);

	/* 1.Index */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pChgMenuItemResCfg->nIndex = atoi(pField);

	/* 2.Equip ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pChgMenuItemResCfg->nEquipID = atoi(pField);

	/* 3.SigType */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pChgMenuItemResCfg->nSigType = atoi(pField);

	/* 4.ResouceID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pChgMenuItemResCfg->nResouceID = atoi(pField);

	return ERR_LCD_OK;
}

static int ParsedVirGroupEqpNameResProc(char *szBuf,
		VIRTUAL_GROUP_EQUIP_NAME_CFG	*pVirGroupEquipNameResCfg)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pVirGroupEquipNameResCfg);

	/* 1.Index */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pVirGroupEquipNameResCfg->nIndex = atoi(pField);

	/* 2.Equip Type ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pVirGroupEquipNameResCfg->nEquipTypeId = atoi(pField);

	/* 3.ResouceID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	pVirGroupEquipNameResCfg->nResouceID = atoi(pField);

	return ERR_LCD_OK;
}


/*==========================================================================*
* FUNCTION : ParsedLcdLangFileTableProc
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char        *szBuf       : 
*            OUT LANG_TEXT  *pStructData : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:20
*==========================================================================*/
static int ParsedLcdLangFileTableProc(IN char *szBuf, 
									  OUT LANG_TEXT *pStructData)
{
	char *pField;

	/* used as buffer */
	int iMaxLenForFull, iMaxLenForAbbr;
	char *szText;

	ASSERT(szBuf);
	ASSERT(pStructData);

	// 1.jump Sequence id field    [SELF_DEFINE_LANGUAGE_ITEM_INFO](1 column)
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	// 2.1RES ID field              [SELF_DEFINE_LANGUAGE_ITEM_INFO](2 column)
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: Res ID is not "
			"a number!\n\r", __FILE__);

		return 1;    /* not a num, error */
	}
	pStructData->iResourceID = atoi(pField);

	// 2.2.Max length of full name field,set a standard length
	iMaxLenForFull = 32;
	pStructData->iMaxLenForFull = iMaxLenForFull;	

	// 3.1Max length of abbr name field  [SELF_DEFINE_LANGUAGE_ITEM_INFO](3 column)
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: Max length(abbr) "
			"is not a number!\n\r", __FILE__);

		return 3;    /* not a num, error */
	}
	iMaxLenForAbbr = atoi(pField);
	pStructData->iMaxLenForAbbr = iMaxLenForAbbr;

	// 3.2.Allocate full english name field */
	pStructData->pFullName[0] = NEW(char, iMaxLenForFull + 1);
	szText = pStructData->pFullName[0];
	if (szText == NULL)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: no memory to load "
			"English Full Name info!\n\r", __FILE__);

		return 4;
	}
	strncpyz(szText, "", iMaxLenForFull + 1);

	// 4.1 Abbr english name field /  [SELF_DEFINE_LANGUAGE_ITEM_INFO](4 column)
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: abbr english name "
			"is NULL!\n\r", __FILE__);

		return 5;
	}
	pStructData->pAbbrName[0] = NEW(char, iMaxLenForAbbr + 1);
	szText = pStructData->pAbbrName[0];
	if (szText == NULL)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: no memory to load "
			"Abbr English Name info!\n\r", __FILE__);

		return 5;
	}
	//Frank Wu,20160127, for MiniNCU
	strncpyz(szText, pField, iMaxLenForAbbr + 1);
	//char *pszGB2312Str;
	//int iRet;
	//iRet = LCD_ConvertUTF8toGB2312(pField, strlen(pField), &pszGB2312Str);
	//printf("iRet=%d, iResourceID=%d, pField=%s, pszGB2312Str=%s\n", iRet, pStructData->iResourceID, pField, pszGB2312Str);
	//strncpyz(szText, pszGB2312Str, iMaxLenForAbbr + 1);

	// 4.2.Allocate full locale name field / 
	pStructData->pFullName[1] = NEW(char, iMaxLenForFull + 1);
	szText = pStructData->pFullName[1];
	if (szText == NULL)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: no memory to load "
			"Locale Full Name info!\n\r", __FILE__);

		return 6;
	}
	strncpyz(szText, "", iMaxLenForFull + 1);

	// 5.Get abbr locale name field */  [SELF_DEFINE_LANGUAGE_ITEM_INFO](5 column)
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: abbr locale name "
			"is NULL!\n\r", __FILE__);

		return 7;
	}
	pStructData->pAbbrName[1] = NEW(char, iMaxLenForAbbr + 1);
	szText = pStructData->pAbbrName[1];
	if (szText == NULL)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedLcdLangFileTableProc: ERROR: no memory to load "
			"Locale Abbr Name info!\n\r", __FILE__);

		return 7;
	}
	//Frank Wu,20160127, for MiniNCU
	strncpy(szText, pField, (size_t)iMaxLenForAbbr + 1);
	//iRet = LCD_ConvertUTF8toGB2312(pField, strlen(pField), &pszGB2312Str);
	//printf("iRet=%d, iResourceID=%d, pField=%s, pszGB2312Str=%s\n", iRet, pStructData->iResourceID, pField, pszGB2312Str);
	//strncpyz(szText, pszGB2312Str, iMaxLenForAbbr + 1);

	return ERR_LCD_OK;
}

/*==========================================================================*
* FUNCTION : LoadLCDPrivateConfigProc
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void  *pCfg       : 
*            void  *pLoadToBuf : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:20
*==========================================================================*/
static int LoadLCDPrivateConfigProc(void *pCfg, void *pLoadToBuf)
{
	LCD_PRIVATE_CFG_LOADER *pBuf;

	//Has N tables
	CONFIG_TABLE_LOADER loader[8];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (LCD_PRIVATE_CFG_LOADER *)pLoadToBuf;


	/* load default screen configuration for 128 X 128 */
	DEF_LOADER_ITEM(&loader[0],
		NULL/*SIGNAL_DIS_ON_DEF_SCREEN_NUM*/,
		&(pBuf->stDefaultScreenCfgReader128.nCfgItemNum), 
		SIGNAL_DIS_ON_DEF_SCREEN_INFO_128, 
		&(pBuf->stDefaultScreenCfgReader128.pDefaultScreenItemCfg), 
		ParsedDefScreenCfgTableProc);

	/* load default screen configuration for 128 X 64 */
	DEF_LOADER_ITEM(&loader[1],
		NULL/*SIGNAL_DIS_ON_DEF_SCREEN_NUM*/,
		&(pBuf->stDefaultScreenCfgReader64.nCfgItemNum), 
		SIGNAL_DIS_ON_DEF_SCREEN_INFO_64, 
		&(pBuf->stDefaultScreenCfgReader64.pDefaultScreenItemCfg), 
		ParsedDefScreenCfgTableProc);

	/* load default screen configuration for 128 X 32 */
	DEF_LOADER_ITEM(&loader[6],
		NULL/*SIGNAL_DIS_ON_DEF_SCREEN_NUM*/,
		&(pBuf->stDefaultScreenCfgReader32.nCfgItemNum), 
		SIGNAL_DIS_ON_DEF_SCREEN_INFO_32, 
		&(pBuf->stDefaultScreenCfgReader32.pDefaultScreenItemCfg), 
		ParsedDefScreenCfgTableProc);

	DEF_LOADER_ITEM(&loader[7],
		NULL/*SIGNAL_DIS_ON_DEF_SCREEN_NUM*/,
		&(pBuf->stDefaultScreenCfgReader32_slave.nCfgItemNum), 
		SIGNAL_DIS_ON_DEF_SCREEN_INFO_32_SLAVE, 
		&(pBuf->stDefaultScreenCfgReader32_slave.pDefaultScreenItemCfg), 
		ParsedDefScreenCfgTableProc);

	/* load divided sub menu item configuration */
	DEF_LOADER_ITEM(&loader[2],
		NULL,
		&(pBuf->stDividedSubMenuItemCfgReader.nDividedSubMenuItemNum), 
		SIGNALS_OF_DIVIDED_SUB_MENU_ITEM, 
		&(pBuf->stDividedSubMenuItemCfgReader.pDividedSubMenuItemCfg), 
		ParsedDividedSubMenuItemProc);


	/* load divided sub menu info configuration */
	DEF_LOADER_ITEM(&loader[3],
		NULL,
		&(pBuf->stDividedSubMenuInfoCfgReader.nDividedSubMenuInfoNum), 
		SIGNALS_OF_DIVIDED_SUB_MENU_INFO, 
		&(pBuf->stDividedSubMenuInfoCfgReader.pDividedSubMenuInfoCfg), 
		ParsedDividedSubMenuInfoProc);

	/* load divided sub menu info configuration */
	DEF_LOADER_ITEM(&loader[4],
		NULL,
		&(pBuf->stChgMenuItemResCfgReader.nChgMenuItemResNum), 
		INFO_OF_CHANGE_MENU_ITEM_RESOUCE, 
		&(pBuf->stChgMenuItemResCfgReader.pChgMenuItemResCfg), 
		ParsedChgMenuItemResProc);

	/* load divided sub menu info configuration */
	DEF_LOADER_ITEM(&loader[5],
		NULL,
		&(pBuf->stVirGroupEquipNameCfgReader.nVirGroupEquipNameResNum), 
		RES_ID_VIRTUAL_GROUP_EQUIP_NAME, 
		&(pBuf->stVirGroupEquipNameCfgReader.pVirGroupEquipNameResCfg), 
		ParsedVirGroupEqpNameResProc);



	if (Cfg_LoadTables(pCfg,sizeof(loader)/sizeof(*loader),loader) 
		!= ERR_CFG_OK)
	{
		return ERR_LCD_FAILURE;
	}

	return ERR_LCD_OK;
}

/*==========================================================================*
 * FUNCTION : LoadLCDPrivateResourceProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pCfg       : 
 *            void  *pLoadToBuf : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  :                  DATE: 2006-05-10 10:04
 *==========================================================================*/
static int LoadLCDPrivateResourceProc(void *pCfg, void *pLoadToBuf)
{
	LCD_PRIVATE_CFG_LOADER *pBuf;

	//Only one table
	CONFIG_TABLE_LOADER loader[1];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (LCD_PRIVATE_CFG_LOADER *)pLoadToBuf;

	/* load LCD language file table */
	DEF_LOADER_ITEM(&loader[0],
		NULL/*SELF_DEFINE_LANGUAGE_ITEM_NUMBER*/,
		&(pBuf->stLCDLangFile.iLangTextNum), 
		SELF_DEFINE_LANGUAGE_ITEM_INFO, 
		&(pBuf->stLCDLangFile.pLangText), 
		ParsedLcdLangFileTableProc);

	if (Cfg_LoadTables(pCfg,sizeof(loader)/sizeof(*loader),loader) 
		!= ERR_CFG_OK)
	{
		return ERR_LCD_FAILURE;
	}

	return ERR_LCD_OK;
}

#ifdef _SUPPORT_THREE_LANGUAGE
/*==========================================================================*
 * FUNCTION : LoadLCDPrivateResourceProc2
 * PURPOSE  : This function is for second resource file for scu plus
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pCfg       : 
 *            void  *pLoadToBuf : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : lixidong                 DATE: 2006-05-10 10:04
 *==========================================================================*/
static int LoadLCDPrivateResourceProc2(void *pCfg, void *pLoadToBuf)
{
	LCD_PRIVATE_CFG_LOADER *pBuf;
	LANG_TEXT	*pLangText;
	LANG_TEXT	*pTempLangText;
	LANG_TEXT	*pLangText_Use;
	int	i,iLangTextNum;

	//Has two tables
	CONFIG_TABLE_LOADER loader[1];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (LCD_PRIVATE_CFG_LOADER *)pLoadToBuf;

	/* load LCD language file table */
	DEF_LOADER_ITEM(&loader[0],
		NULL/*SELF_DEFINE_LANGUAGE_ITEM_NUMBER*/,
		//&(pBuf->stLCDLangFile.iLangTextNum), 
		&iLangTextNum,
		SELF_DEFINE_LANGUAGE_ITEM_INFO, 
		//&(pBuf->stLCDLangFile.pLangText),
		&pLangText,
		ParsedLcdLangFileTableProc);

	if (Cfg_LoadTables(pCfg,sizeof(loader)/sizeof(*loader),loader) 
		!= ERR_CFG_OK)
	{
		return ERR_LCD_FAILURE;
	}

	if(iLangTextNum != pBuf->stLCDLangFile.iLangTextNum)
	{
		if(iLangTextNum > pBuf->stLCDLangFile.iLangTextNum)
		{
			iLangTextNum = pBuf->stLCDLangFile.iLangTextNum;
		}
		AppLogOut("LCD_MAIN", APP_LOG_ERROR, 
			"There are different signals in two lcd private resource file.");
	}
	pTempLangText = pLangText;
	pLangText_Use = pBuf->stLCDLangFile.pLangText;
	for(i = 0; i < iLangTextNum; i++, pLangText++, pLangText_Use++)
	{
		pLangText_Use->pAbbrName[2] = NEW_strdup(pLangText->pAbbrName[1]);
		pLangText_Use->pFullName[2] = NEW_strdup(pLangText->pFullName[1]);
		DELETE_ITEM(pLangText->pAbbrName[0]);
		DELETE_ITEM(pLangText->pAbbrName[1]);
		DELETE_ITEM(pLangText->pFullName[0]);
		DELETE_ITEM(pLangText->pFullName[1]);
	}

	DELETE_ITEM(pTempLangText);

	return ERR_LCD_OK;
}
#endif


/*==========================================================================*
 * FUNCTION : *CreateLangResourceFileName
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const char  *szFileName : format: xxx.cfg
 *            const char  *szLangCode : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LinTao                   DATE: 2004-09-17 20:03
 *==========================================================================*/
static char *CreateLangResourceFileName(const char *szFileName, const char *szLangCode)
{
	static char szLRFileName[256]; /* the returned lang resource file name */
	const char *p;   
	size_t iLen;

	if (szFileName == NULL || szLangCode == NULL)
	{
		return NULL;
	}

	/* init */
	szLRFileName[0] = '\0';

	/* get the head: config/lang/xx/ */
	strcpy(szLRFileName, "lang/");
	strcat(szLRFileName, szLangCode);
	strcat(szLRFileName, "/");

	/* get the name: stdxx_xx.res */
	p = szFileName;
	iLen = 0;
	while (*p != '\0' && *p != '.')
	{
		p++;
		iLen++;
	}

	if (*p == '\0')				/* lack of '.', invalid format */
	{
		return NULL;
	}
				
	strncat(szLRFileName, szFileName, iLen);
	strcat(szLRFileName, "_");
	strcat(szLRFileName, szLangCode);
	strcat(szLRFileName, ".res");

	return szLRFileName;
}





/*==========================================================================*
* FUNCTION : LoadLCDPrivateConfig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: LCD_PRIVATE_CFG_LOADER*  pLcdPrivateCfgLoader : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:19
*==========================================================================*/
static int LoadLCDPrivateConfig(LCD_PRIVATE_CFG_LOADER* pLcdPrivateCfgLoader)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	char*	pLangCode = "";
	int		nBufLen;

	ASSERT(pLcdPrivateCfgLoader);

	//Load LCD private config
	Cfg_GetFullConfigPath(CONFIG_FILE_LCD_PRIVATE, 
		szCfgFileName, MAX_FILE_PATH);


	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadLCDPrivateConfigProc,
		pLcdPrivateCfgLoader);

	if (ret != ERR_CFG_OK)
	{
		return ret;
	}

	
	DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		LOCAL_LANGUAGE_CODE, 
		0, 
		&nBufLen,
		&(pLangCode),
		0);



	//Frank Wu,20160127, for MiniNCU
	LANG_SUPPORT_TABLE*			pstLCDLangSupportTable = &pLcdPrivateCfgLoader->stLCDLangSupportTable;
	LANG_SUPPORT_TABLE_ITEM*	pstLCDLangSupportTableItem = NULL;
	LANG_TEXT*					pLangText = NULL;
	LANG_FILE*					pLCDLangFile = NULL;
	char*						pTempLangCode = NULL;
	char						szLangFilePath[MAX_FILE_PATH];
	int							i;
	int							iLangCount;
	struct stat					stFileStat;

	memset(pstLCDLangSupportTable, 0, sizeof(*pstLCDLangSupportTable));

	iLangCount = LOCAL_LANGUAGE_FLAG + 1;

/*

1.  ˳��ɨ�裬ɨ�跶ΧΪ�ṹ��g_s_ppLangCode����֧�ֵ����ԣ�������������ṹ�����������Զ�֧�֣�
     1) ���ȣ�Ӣ��ض�֧��
     2) ��Σ��������Է���EN֮����ԶΪ�ڶ������ϵͳ������Ĭ����ʾ
     3) ʣ�µĸ���CONFIG_DEFAULT_DIRĿ¼�µ�������g_s_ppLangCodeȡ������mini��config_defaultĿ¼����es��zh��tw��de��fr��it��tr��ru������Ϊtr��ruȥ�������mini֧�ֵ�
	  ���Թ�7�֣���En��es��zh��tw��de��fr��it
2.  ����ESNAֻ֧�� En��Es��Fr�����miniҲ֧�����������ԣ�����mini��web�Ϳ���ֱ����esna���ⲿ���޸��ˣ�ֻ��Ҫ�޸�LCDҲ֧��3�����Լ��ɡ����������Ҫ���ɿ��䣬Ҳ���㡣
3.  ESNA��LCD����ʾ������˳���ǹ̶��ģ�En����ǰ�棬����Ķ��ǹ̶��ģ�ֻ�Ǳ������Ե�¼ʱ�ᷴ��ѡ�С���mini��û�з���ѡ�У�����������Զ��ʾ�ڵڶ���(En֮��)������ϵͳ����
     ��lcd��Ĭ����ʾ��������(ǰ����ϵͳ�����ļ���22�����ź���Ϊ��Ĭ����ʾ��������)��
4.  �������Զ������g_stLcdPrivateCfgLoaderȫ�ֽṹ��stLCDLangSupportTable��Ա�е�staLangSupportTableItem�����

*/


	//load all language texts for language selecting screen
	for(i = 0; i < MINI_NCU_LANGUAGE_LIST_NUM; i++)
	{
		pTempLangCode = MINI_NCU_LANGUAGE_LIST_ITEM(i);
		
		if(0 == stricmp(pTempLangCode, LCD_FIXED_LANGUAGE_EN))//English
		{
			pstLCDLangSupportTableItem = &pstLCDLangSupportTable->staLangSupportTableItem[ENGLISH_LANGUAGE_FLAG];
			
			pstLCDLangSupportTableItem->pszLangCode = pTempLangCode;
			strncpyz(pstLCDLangSupportTableItem->szLocalAbbrName,
				"English",
				sizeof(pstLCDLangSupportTableItem->szLocalAbbrName));
		}
		else if(0 == stricmp(pTempLangCode, pLangCode))//local language
		{
			//printf("pTempLangCode=%s########\n",pTempLangCode);
			pstLCDLangSupportTableItem = &pstLCDLangSupportTable->staLangSupportTableItem[LOCAL_LANGUAGE_FLAG];

			pstLCDLangSupportTableItem->pszLangCode = pTempLangCode;
			strncpyz(pstLCDLangSupportTableItem->szLocalAbbrName,
				pTempLangCode,
				sizeof(pstLCDLangSupportTableItem->szLocalAbbrName));
		}
		else//other languages, load from CONFIG_DEFAULT_DIR
		{	
			//For mini and esna code merge, 2018-5-15
			if( (pTempLangCode == LCD_FIXED_LANGUAGE_ES)  || (pTempLangCode == LCD_FIXED_LANGUAGE_FR) )
			{
				pLCDLangFile = &pLcdPrivateCfgLoader->stLCDLangFile;

				MakeFullFileName(szLangFilePath, getenv(ENV_ACU_ROOT_DIR), 
					CONFIG_DEFAULT_DIR, CreateLangResourceFileName(RESOURCE_FILE_LCD_PRIVATE, pTempLangCode));

				ret = stat(szLangFilePath, &stFileStat);
				if(0 == ret)
				{
					//load language config
					ret = Cfg_LoadConfigFile(
						szLangFilePath, 
						LoadLCDPrivateResourceProc,
						pLcdPrivateCfgLoader);
					if (ERR_CFG_OK == ret)
					{
						pstLCDLangSupportTableItem = &pstLCDLangSupportTable->staLangSupportTableItem[iLangCount];

						pstLCDLangSupportTableItem->pszLangCode = pTempLangCode;
						strncpyz(pstLCDLangSupportTableItem->szLocalAbbrName,
							pTempLangCode,
							sizeof(pstLCDLangSupportTableItem->szLocalAbbrName));

						//language name
						pLangText = GetLCDLangText(
							LOCAL_LANGUAGE_NAME_LANG_ID, 
							pLCDLangFile->iLangTextNum, 
							pLCDLangFile->pLangText);
						if((NULL != pLangText) && (LOCAL_LANGUAGE_NAME_LANG_ID == pLangText->iResourceID))
						{
							strncpyz(pstLCDLangSupportTableItem->szLocalAbbrName,
								pLangText->pAbbrName[LOCAL_LANGUAGE_FLAG],
								sizeof(pstLCDLangSupportTableItem->szLocalAbbrName));
						}

						//prompt1 string
						pLangText = GetLCDLangText(
							HEADING_ACKNOWLEDGE_INFO_LANG_ID, 
							pLCDLangFile->iLangTextNum, 
							pLCDLangFile->pLangText);
						if((NULL != pLangText) && (HEADING_ACKNOWLEDGE_INFO_LANG_ID == pLangText->iResourceID))
						{
							strncpyz(pstLCDLangSupportTableItem->szLocalPrompt1,
								pLangText->pAbbrName[LOCAL_LANGUAGE_FLAG],
								sizeof(pstLCDLangSupportTableItem->szLocalPrompt1));
						}

						//prompt2 string
						pLangText = GetLCDLangText(
							REBOOT_TO_VALIDATE_LANG_ID, 
							pLCDLangFile->iLangTextNum, 
							pLCDLangFile->pLangText);
						if((NULL != pLangText) && (REBOOT_TO_VALIDATE_LANG_ID == pLangText->iResourceID))
						{
							strncpyz(pstLCDLangSupportTableItem->szLocalPrompt2,
								pLangText->pAbbrName[LOCAL_LANGUAGE_FLAG],
								sizeof(pstLCDLangSupportTableItem->szLocalPrompt2));
						}

						iLangCount++;
					}

					//unload language config
					ClearLCDPrivateConfigLangFile(pLCDLangFile);
				}
			}
		}
		//printf("LoadLCDPrivateConfig():  i = %d  , iLangCount = %d!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n",i,iLangCount);
	}

	pstLCDLangSupportTable->nLangSupportNum = iLangCount;


	//Load LCD private language resource path according to language code 
	Cfg_GetFullConfigPath(
		CreateLangResourceFileName(RESOURCE_FILE_LCD_PRIVATE, pLangCode),
		szCfgFileName, MAX_FILE_PATH);

	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadLCDPrivateResourceProc,
		pLcdPrivateCfgLoader);

	if (ret != ERR_CFG_OK)
	{
		return ret;
	}
	else
	{
		pLCDLangFile = &pLcdPrivateCfgLoader->stLCDLangFile;

		//language name
		pLangText = GetLCDLangText(
			LOCAL_LANGUAGE_NAME_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if((NULL != pLangText) && (LOCAL_LANGUAGE_NAME_LANG_ID == pLangText->iResourceID))
		{
			pstLCDLangSupportTableItem = &pstLCDLangSupportTable->staLangSupportTableItem[LOCAL_LANGUAGE_FLAG];
			strncpyz(pstLCDLangSupportTableItem->szLocalAbbrName,
				pLangText->pAbbrName[LOCAL_LANGUAGE_FLAG],
				sizeof(pstLCDLangSupportTableItem->szLocalAbbrName));
		}
	}
	
	

	///////////////////////////////////////////////////////////////////////////
	//for scu plus it need another local languge
#ifdef _SUPPORT_THREE_LANGUAGE
	DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		LOCAL2_LANGUAGE_CODE, 
		0, 
		&nBufLen,
		&(pLangCode),
		0);

	//Load LCD private language resource2 path according to language code 
	Cfg_GetFullConfigPath(
		CreateLangResourceFileName(RESOURCE_FILE_LCD_PRIVATE, pLangCode),
		szCfgFileName, MAX_FILE_PATH);

	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadLCDPrivateResourceProc2,
		pLcdPrivateCfgLoader);

	if (ret != ERR_CFG_OK)
	{
		return ret;
	}
#endif
	///////////////////////////////////////////////////////////////////////////
	return ERR_CFG_OK;
}





/*==========================================================================*
 * FUNCTION : Compare_Sampling_Sig_Info
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: const SAMPLE_SIG_INFO**  p1 : 
 *            const SAMPLE_SIG_INFO**  p2 : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : lixidong                 DATE: 2006-05-10 10:56
 *==========================================================================*/
static int Compare_Sampling_Sig_Info(const SAMPLE_SIG_INFO** p1,
									 const SAMPLE_SIG_INFO** p2)
{
	if((*p1)->iValueDisplayID > (*p2)->iValueDisplayID)
	{
		return 1;
	}

	if((*p1)->iValueDisplayID < (*p2)->iValueDisplayID)
	{
		return -1;
	}

	return 0;
}

static int Compare_Control_Sig_Info(const CTRL_SIG_INFO** p1,
									const CTRL_SIG_INFO** p2)
{
	if((*p1)->iValueDisplayID > (*p2)->iValueDisplayID)
	{
		return 1;
	}

	if((*p1)->iValueDisplayID < (*p2)->iValueDisplayID)
	{
		return -1;
	}

	return 0;
}

static int Compare_Setting_Sig_Info(const SET_SIG_INFO** p1,
									const SET_SIG_INFO** p2)
{
	if((*p1)->iValueDisplayID > (*p2)->iValueDisplayID)
	{
		return 1;
	}

	if((*p1)->iValueDisplayID < (*p2)->iValueDisplayID)
	{
		return -1;
	}

	return 0;
}



static void MakeSigDisplayOrderSort(void* ppSigInfo, int nSigmNum, int nSigType)
{
	int(*fCompareFunc)(const void *, const void *);

	if (nSigType == SIG_TYPE_SAMPLING)
	{

		fCompareFunc = 
			(int(*)(const void *, const void *))Compare_Sampling_Sig_Info;
	}
	else if (nSigType == SIG_TYPE_CONTROL)
	{

		fCompareFunc = 
			(int(*)(const void *, const void *))Compare_Control_Sig_Info;
	}
	else if (nSigType == SIG_TYPE_SETTING)
	{

		fCompareFunc = 
			(int(*)(const void *, const void *))Compare_Setting_Sig_Info;
	}
	else
	{
		return;
	}

	qsort(ppSigInfo, (size_t)nSigmNum, 4,
		fCompareFunc);
}

static int Compare_ClassEquipType(const DISP_CLASS_EQUIP_NODE *p1,
								   const DISP_CLASS_EQUIP_NODE *p2)
{
	if(p1->nGroupEquipType > p2->nGroupEquipType)
	{
		return 1;
	}

	if(p1->nGroupEquipType < p2->nGroupEquipType)
	{
		return -1;
	}

	return 0;
}

static int Compare_ActEquipID(const DISP_ACTUAL_EQUIP_NODE *p1,
								const DISP_ACTUAL_EQUIP_NODE *p2)
{
	if(p1->pEquipInfo->iEquipID > p2->pEquipInfo->iEquipID)
	{
		return 1;
	}

	if(p1->pEquipInfo->iEquipID < p2->pEquipInfo->iEquipID)
	{
		return -1;
	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : ClassNodeExist
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: PARSE_MENU_NODE*  pParseMenuNode : 
 *            int               nEquipType     : 
 * RETURN   : static CLASS_EQUIP_NODE* : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-09-19 10:59
 *==========================================================================*/
static CLASS_EQUIP_INFO* ClassNodeExist(PARSE_EQUIP_INFO* pParseEquipInfo, int nEquipType)
{
	ASSERT(pParseEquipInfo);	

	CLASS_EQUIP_INFO*	pClassEquipInfo = pParseEquipInfo->pClassEquipInfo;

	int i;

	for(i = 0; (i < pParseEquipInfo->nClassEquipNum) && (pClassEquipInfo != NULL);
		i++, pClassEquipInfo++)
	{
		if((pClassEquipInfo->nStdEquipNum > 0) 
			&& (GET_EQUIP_TYPE_ID_MAJOR(pClassEquipInfo->nGroupEquipType) == GET_EQUIP_TYPE_ID_MAJOR(nEquipType)))
		{
			return pClassEquipInfo;

		}
	}

	return NULL;
}

static STANDARD_EQUIP_INFO* FindStdEquipNode(PARSE_EQUIP_INFO* pParseEquipInfo, int nEquipType)
{
	ASSERT(pParseEquipInfo);

	STANDARD_EQUIP_INFO* pStdEquipInfo;
	pStdEquipInfo = pParseEquipInfo->pStdEquipInfo;

	int i;
	for(i = 0; (i < pParseEquipInfo->nStdEquipNum) && (pStdEquipInfo != NULL);
		i++, pStdEquipInfo++)
	{
		if(pStdEquipInfo->nEquipType == nEquipType)
		{
			return pStdEquipInfo;
		}
	}

	return NULL;
}

static int GetSameClassEquipNumber(STANDARD_EQUIP_INFO	*pStandardEquipInfoList,
				int	nStandardEquipNum, int nEquipType)
{
	ASSERT(pStandardEquipInfoList);

	STANDARD_EQUIP_INFO	*pStandardEquipInfo;
	pStandardEquipInfo = pStandardEquipInfoList;

	int	nEquipNum = 0;
	int i;
	for(i = 0; (i < nStandardEquipNum) && (pStandardEquipInfo != NULL);
		i++, pStandardEquipInfo++)
	{
		if(GET_EQUIP_TYPE_ID_MAJOR(pStandardEquipInfo->nEquipType)
			== GET_EQUIP_TYPE_ID_MAJOR(nEquipType))
		{
			nEquipNum++;
		}
	}
	return nEquipNum;	
}

static int GetSameStdEquipNumber(EQUIP_INFO	*pActualEquipList,
								   int	nActualEquipNum, int nStdEquipType)
{
	ASSERT(pActualEquipList);

	EQUIP_INFO *pActEquip;
	pActEquip = pActualEquipList;

	int	nEquipNum = 0;
	int i;
	for(i = 0; (i < nActualEquipNum) && (pActEquip != NULL);
		i++, pActEquip++)
	{
		if(pActEquip->iEquipTypeID == nStdEquipType)
		{
			nEquipNum++;
		}
	}
	return nEquipNum;	
}

/*==========================================================================*
 * FUNCTION : ReadStandardEquipInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: STANDARD_EQUIP_NODE_INFO                     *pStandardEquipNode : 
 *            STDEQUIP_TYPE_INFO *pStandardEquipList; int  nStandardEquipNum   : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-09-18 16:50
 *==========================================================================*/
static int 	ReadStandardEquipInfo(STANDARD_EQUIP_INFO	*pStandardEquipInfo,
				  STDEQUIP_TYPE_INFO	*pStandardEquipList,
				  int	nStandardEquipNum)
{
	ASSERT(pStandardEquipInfo);
	ASSERT(pStandardEquipList);

	int			i, j, k;

	SAMPLE_SIG_INFO	*pSampleSigInfo;	 
	CTRL_SIG_INFO	*pCtrlSigInfo;	 
	SET_SIG_INFO	*pSetSigInfo;	 
	ALARM_SIG_INFO	*pAlarmSigInfo; 


	STDEQUIP_TYPE_INFO	*pStandardEquip;
	pStandardEquip = pStandardEquipList;

	int				nSigNum;
	SIGNAL_INFO		*pSignalInfo;


	//�ѱ�׼�豸���ź�ȫ������
	for(i = 0;
		(i < nStandardEquipNum) && (pStandardEquip != NULL) && (pStandardEquipInfo != NULL);
		i++, pStandardEquip++, pStandardEquipInfo++)
	{
		pStandardEquipInfo->nEquipType = pStandardEquip->iTypeID;

		pSignalInfo = &(pStandardEquipInfo->stSignalInfo);

		for(j = SIG_TYPE_SAMPLING; j <= SIG_TYPE_ALARM; j++)
		{
			//�Ѹ��豸�µ�X�źŵ�ָ����뵽pSignalInfo�С�
			switch (j)
			{
			case SIG_TYPE_SAMPLING:
				{
					nSigNum = pStandardEquip->iSampleSigNum;

					pSignalInfo->iSampleSigNum = 0;

					pSignalInfo->ppSampleSigInfo = NEW(SAMPLE_SIG_INFO*, nSigNum);
					LOGOUT_NO_MEMORY(pSignalInfo->ppSampleSigInfo);
					ZERO_POBJS(pSignalInfo->ppSampleSigInfo, nSigNum);

					pSampleSigInfo = pStandardEquip->pSampleSigInfo;
					for(k = 0;
						pSampleSigInfo != NULL && k < nSigNum;
						k++, pSampleSigInfo++)
					{
						if(pSampleSigInfo->byValueDisplayAttr & DISPLAY_LCD)
						{
							pSignalInfo->ppSampleSigInfo[pSignalInfo->iSampleSigNum] = pSampleSigInfo;
							pSignalInfo->iSampleSigNum++;
						}
					}

					MakeSigDisplayOrderSort(pSignalInfo->ppSampleSigInfo,
						pSignalInfo->iSampleSigNum,
						SIG_TYPE_SAMPLING);

				}
				break;

			case SIG_TYPE_CONTROL:	
				{
					nSigNum = pStandardEquip->iCtrlSigNum;

					pSignalInfo->iCtrlSigNum = 0;

					pSignalInfo->ppCtrlSigInfo = NEW(CTRL_SIG_INFO*, nSigNum);
					LOGOUT_NO_MEMORY(pSignalInfo->ppCtrlSigInfo);
					ZERO_POBJS(pSignalInfo->ppCtrlSigInfo, nSigNum);

					pCtrlSigInfo = pStandardEquip->pCtrlSigInfo;
					for(k = 0;
						pCtrlSigInfo != NULL && k < nSigNum;
						k++, pCtrlSigInfo++)
					{
						if(pCtrlSigInfo->byValueDisplayAttr & DISPLAY_LCD)
						{
							pSignalInfo->ppCtrlSigInfo[pSignalInfo->iCtrlSigNum] = pCtrlSigInfo;
							pSignalInfo->iCtrlSigNum++;
						}
					}

					MakeSigDisplayOrderSort(pSignalInfo->ppCtrlSigInfo,
						pSignalInfo->iCtrlSigNum,
						SIG_TYPE_CONTROL);

				}
				break;

			case SIG_TYPE_SETTING:		
				{
					nSigNum = pStandardEquip->iSetSigNum;

					pSignalInfo->iSetSigNum = 0;

					pSignalInfo->ppSetSigInfo = NEW(SET_SIG_INFO*, nSigNum);
					LOGOUT_NO_MEMORY(pSignalInfo->ppSetSigInfo);
					ZERO_POBJS(pSignalInfo->ppSetSigInfo, nSigNum);

					pSetSigInfo = pStandardEquip->pSetSigInfo;
					for(k = 0;
						pSetSigInfo != NULL && k < nSigNum;
						k++, pSetSigInfo++)
					{
						if(pSetSigInfo->byValueDisplayAttr & DISPLAY_LCD)
						{
							pSignalInfo->ppSetSigInfo[pSignalInfo->iSetSigNum] = pSetSigInfo;
							pSignalInfo->iSetSigNum ++;
						}
					}

					MakeSigDisplayOrderSort(pSignalInfo->ppSetSigInfo,
						pSignalInfo->iSetSigNum,
						SIG_TYPE_SETTING);

				}
				break;

			case SIG_TYPE_ALARM:		
				{			
					nSigNum = pStandardEquip->iAlarmSigNum;

					pSignalInfo->iAlarmSigNum = 0;

					pSignalInfo->ppAlarmSigInfo = NEW(ALARM_SIG_INFO*, nSigNum);
					LOGOUT_NO_MEMORY(pSignalInfo->ppAlarmSigInfo);
					ZERO_POBJS(pSignalInfo->ppAlarmSigInfo, nSigNum);

					pAlarmSigInfo = pStandardEquip->pAlarmSigInfo;
					for(k = 0;
						pAlarmSigInfo != NULL && k < nSigNum;
						k++, pAlarmSigInfo++)
					{
						pSignalInfo->ppAlarmSigInfo[pSignalInfo->iAlarmSigNum] = pAlarmSigInfo;
						pSignalInfo->iAlarmSigNum ++;
					}

					//MakeSigDisplayOrderSort(pSignalInfo->pAlarmSigInfo, 
					//pSignalInfo->iAlarmSigNum, SIG_TYPE_ALARM);
				}
				break;

			default:
				{
				}
				break;

			}
		}
	}

	return ERR_LCD_OK;
}



/*==========================================================================*
 * FUNCTION : ClassifyStandardEquip
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: CLASS_EQUIP_NODE*   pClassEquipNode     : 
 *            STDEQUIP_TYPE_INFO  *pStandardEquipList : 
 *            int                 nStandardEquipNum   : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-09-19 09:04
 *==========================================================================*/
static int ClassifyStandardEquip(PARSE_EQUIP_INFO* pParseEquipInfo,
				STANDARD_EQUIP_INFO	*pStandardEquipInfoList,
				int	nStandardEquipNum)
{
	ASSERT(pParseEquipInfo);
	pParseEquipInfo->nClassEquipNum = 0;

	ASSERT(pStandardEquipInfoList);

	int i;
	int	nEquipType;
	int	nSameClassEquipNum;

	CLASS_EQUIP_INFO*	pClassEquipInfo;

	STANDARD_EQUIP_INFO *pStandardEquipInfo;
	pStandardEquipInfo = pStandardEquipInfoList;

	// Loop the pStandardEquipInfoList
	for(i = 0; 
		(i < nStandardEquipNum) && (pStandardEquipInfo != NULL);
		i++, pStandardEquipInfo++)
	{
		nEquipType = pStandardEquipInfo->nEquipType;
		pClassEquipInfo = ClassNodeExist(pParseEquipInfo, nEquipType);

		//if do not exist, add these std equips to a class equip node
		if(pClassEquipInfo == NULL)
		{
			pClassEquipInfo = pParseEquipInfo->pClassEquipInfo + pParseEquipInfo->nClassEquipNum;
            pParseEquipInfo->nClassEquipNum++;

			nSameClassEquipNum = GetSameClassEquipNumber(pStandardEquipInfoList, nStandardEquipNum, nEquipType);
			ASSERT(nSameClassEquipNum > 0);

			pClassEquipInfo->ppStandardEquipInfo = NEW(STANDARD_EQUIP_INFO*, nSameClassEquipNum);
			LOGOUT_NO_MEMORY(pClassEquipInfo->ppStandardEquipInfo);
			ZERO_POBJS(pClassEquipInfo->ppStandardEquipInfo, nSameClassEquipNum);

			pClassEquipInfo->nStdEquipNum = 1;
			pClassEquipInfo->ppStandardEquipInfo[0] = pStandardEquipInfo;
	
			pClassEquipInfo->nGroupEquipType = GET_EQUIP_TYPE_ID_MAJOR(pStandardEquipInfo->nEquipType) * 100;


		}
		else	//Add this std equip to an exist node
		{
			pClassEquipInfo->ppStandardEquipInfo[pClassEquipInfo->nStdEquipNum] = pStandardEquipInfo;
			pClassEquipInfo->nStdEquipNum++;
		}

	}

	return ERR_LCD_OK;
}

static int 	ClassifyActualEquip(PARSE_EQUIP_INFO *pParseEquipInfo,
				EQUIP_INFO *pActualEquipList,
				int nActualEquipNum)
{
	ASSERT(pParseEquipInfo);
	ASSERT(pActualEquipList);

	int i;
	int	nSameStdEquipNum;

	// ����һ��ÿһ��׼�豸�¶�Ӧ���ж��ٸ�ʵ���豸��������pStdEquipNode->ppActualEquip�Ŀռ�
	STANDARD_EQUIP_INFO *pStdEquipInfo;
	pStdEquipInfo = pParseEquipInfo->pStdEquipInfo;
	
	for(i = 0; (i < pParseEquipInfo->nStdEquipNum) && (pStdEquipInfo != NULL);
		i++, pStdEquipInfo++)
	{
		pStdEquipInfo->nActEquipNum = 0;	//������

		nSameStdEquipNum = GetSameStdEquipNumber(pActualEquipList, nActualEquipNum, pStdEquipInfo->nEquipType);
		if(nSameStdEquipNum > 0)
		{
			pStdEquipInfo->ppActualEquip = NEW(EQUIP_INFO*, nSameStdEquipNum);
			LOGOUT_NO_MEMORY(pStdEquipInfo->ppActualEquip);
			ZERO_POBJS(pStdEquipInfo->ppActualEquip, nSameStdEquipNum);

		}
		else
		{
			pStdEquipInfo->ppActualEquip = NULL;
		}
	}


	//��ʵ���豸�ֵ�����pStdEquipNode->ppActualEquip����
	EQUIP_INFO *pActEquip;
	pActEquip = pActualEquipList;

	for(i = 0; (i < nActualEquipNum) && (pActEquip != NULL);
		i++, pActEquip++)
	{
		pStdEquipInfo = FindStdEquipNode(pParseEquipInfo, pActEquip->iEquipTypeID);
		ASSERT(pStdEquipInfo);

		pStdEquipInfo->ppActualEquip[pStdEquipInfo->nActEquipNum++] = pActEquip;
	}

	return ERR_LCD_OK;
}



/*==========================================================================*
* FUNCTION : ParseEquipsAndSignals
* PURPOSE  : Parse equips and signals from site info
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: PARSE_MENU_NODE*  pParseMenuNode : To save info after parsed
*            int               nDispType      : Display on LCD or other modes
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 17:32
*==========================================================================*/
static int ParseEquipsAndSignals(PARSE_EQUIP_INFO* pParseEquipInfo)
{

	ASSERT(pParseEquipInfo);
	ZERO_POBJS(pParseEquipInfo, 1);

	int			nError	= ERR_DXI_OK;

	int			nInterfaceType;
	int			nVarID = 0;			//Data ID
	int			nVarSubID = 0;		//Data sub ID
	int			nBufLen;
	int			nTimeOut = 5000;	//Time out


	// 1. Get the number of standard equipments which defined in BasicStandard.cfg
	nInterfaceType = VAR_STD_EQUIPS_NUM;

	int		nStdEquipNum;
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&nStdEquipNum,			
		nTimeOut);

	if(ERR_DXI_OK != nError)
	{
		return ERR_LCD_DXI_GET_DATA_FAILURE;
	}

	if(nStdEquipNum <= 0)
	{
		return ERR_LCD_OK;
	}

	// 2. Get the list of standard equipments
	nInterfaceType = VAR_STD_EQUIPS_LIST;

	STDEQUIP_TYPE_INFO	*pStandardEquipList;
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pStandardEquipList,			
		nTimeOut);

	if(ERR_DXI_OK != nError)
	{
		return ERR_LCD_DXI_GET_DATA_FAILURE;
	}


	// 3. �ѱ�׼�豸�������źŵ���Ϣ����pStandardEquipInfo��
	STANDARD_EQUIP_INFO	*pStandardEquipInfo;
	pStandardEquipInfo = NEW(STANDARD_EQUIP_INFO, nStdEquipNum);
	LOGOUT_NO_MEMORY(pStandardEquipInfo);
	ZERO_POBJS(pStandardEquipInfo, nStdEquipNum);

	pParseEquipInfo->pStdEquipInfo = pStandardEquipInfo;
	pParseEquipInfo->nStdEquipNum = nStdEquipNum;

	ReadStandardEquipInfo(pStandardEquipInfo, pStandardEquipList, nStdEquipNum);

	// 4. �Ա�׼�豸�����ı�׼�豸��������з���
	CLASS_EQUIP_INFO*	pClassEquipInfo;
	pClassEquipInfo = NEW(CLASS_EQUIP_INFO, nStdEquipNum);	//ʵ���ϲ���Ҫ��ô��
	LOGOUT_NO_MEMORY(pClassEquipInfo);
	ZERO_POBJS(pClassEquipInfo, nStdEquipNum);

	pParseEquipInfo->pClassEquipInfo = pClassEquipInfo;

	ClassifyStandardEquip(pParseEquipInfo, pStandardEquipInfo, nStdEquipNum);

	// 5. Get the number of actual equipments
	nInterfaceType = VAR_ACU_EQUIPS_NUM;

	int	nActEquipNum;
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&nActEquipNum,			
		nTimeOut);

	if(ERR_DXI_OK != nError)
	{
		return ERR_LCD_DXI_GET_DATA_FAILURE;
	}

	if(nActEquipNum <= 0)
	{
		return ERR_LCD_OK;
	}

	// 6. Get the list of actual equipments
	nInterfaceType = VAR_ACU_EQUIPS_LIST;

	EQUIP_INFO* pActEquipList;
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pActEquipList,			
		nTimeOut);

	if(ERR_DXI_OK != nError)
	{
		return ERR_LCD_DXI_GET_DATA_FAILURE;
	}

	// 7. ��ʵ���豸�ֵ�����STANDARD_EQUIP_NODE_INFO����
	ClassifyActualEquip(pParseEquipInfo, pActEquipList, nActEquipNum);

	return ERR_LCD_OK;
}


static int GetDispEquipNumOfClassEquip(CLASS_EQUIP_INFO *pClassEquip, int nSigType)
{

	ASSERT(pClassEquip);

	STANDARD_EQUIP_INFO* pStdEquipInfo;
	SIGNAL_INFO* pSignalInfo;

	int i;
	int nDispEquipNum = 0;

	for(i = 0; i < pClassEquip->nStdEquipNum; i++)
	{
		pStdEquipInfo = pClassEquip->ppStandardEquipInfo[i];

		//if ClassifyStandardEquip() has executed, pStdEquipInfo should not equal to NULL
		ASSERT(pStdEquipInfo != NULL);

		pSignalInfo = &(pStdEquipInfo->stSignalInfo);
		switch(nSigType)
		{
		case SIG_TYPE_SAMPLING:
			if(pSignalInfo->iSampleSigNum > 0)
			{
				nDispEquipNum += pStdEquipInfo->nActEquipNum;
			}
			break;

		case SIG_TYPE_CONTROL:
			if(pSignalInfo->iCtrlSigNum > 0)
			{
				nDispEquipNum += pStdEquipInfo->nActEquipNum;
			}
			break;

		case SIG_TYPE_SETTING:
			if(pSignalInfo->iSetSigNum > 0)
			{
				nDispEquipNum += pStdEquipInfo->nActEquipNum;
			}
			break;

		case SIG_TYPE_ALARM:
			if(pSignalInfo->iAlarmSigNum > 0)
			{
				nDispEquipNum += pStdEquipInfo->nActEquipNum;
			//	nDispEquipNum ++;
			}
			break;

		default:
			break;

		}
	}

	return nDispEquipNum;

}


static int GetDispClassEquipNum(PARSE_EQUIP_INFO *pParseEquipInfo, int nSigType)
{
	ASSERT(pParseEquipInfo);

	int nClassEquipNum = 0;

	int i;
	CLASS_EQUIP_INFO *pClassEquip = pParseEquipInfo->pClassEquipInfo;
	for(i = 0; 
		(i < pParseEquipInfo->nClassEquipNum) && (pClassEquip != NULL);
		i++, pClassEquip++)
	{
		if(GetDispEquipNumOfClassEquip(pClassEquip, nSigType) > 0)
		{
			nClassEquipNum++;
		}
	}

	return nClassEquipNum;
}

static int ParseClassEquipNode(PARSE_MENU_NODE *pParseMenuNode,
				CLASS_EQUIP_INFO *pClassEquipInfo,
				int nSigType)
{
	//���pClassEquipInfo������Ҫ��ʾ���豸����ӵ�pParseMenuNode��

	ASSERT(pParseMenuNode);
	ASSERT(pClassEquipInfo);

	int nDispActEquipNum;
	int i, j;

	nDispActEquipNum = GetDispEquipNumOfClassEquip(pClassEquipInfo, nSigType);
	if(nDispActEquipNum <= 0)
	{
		return ERR_LCD_OK;
	}

	DISP_CLASS_EQUIP_NODE *pDispClassEquipNode;
	pDispClassEquipNode = pParseMenuNode->pDispClassEquipNode[nSigType] + pParseMenuNode->nDispClassEquipNum[nSigType]++;

	pDispClassEquipNode->pDispActEquipNode = NEW(DISP_ACTUAL_EQUIP_NODE, nDispActEquipNum);
	LOGOUT_NO_MEMORY(pDispClassEquipNode->pDispActEquipNode);
	ZERO_POBJS(pDispClassEquipNode->pDispActEquipNode, nDispActEquipNum);

	//�ȰѸ������㣬���ں����++
	pDispClassEquipNode->nDispActEquipNum = 0;

	DISP_ACTUAL_EQUIP_NODE *pDispActEquipNode;

	STANDARD_EQUIP_INFO *pStdEquipInfo;
	pStdEquipInfo = pClassEquipInfo->ppStandardEquipInfo[0];
	ASSERT(pStdEquipInfo);

	for(i = 0; 
		(i < pClassEquipInfo->nStdEquipNum) && (pStdEquipInfo != NULL);
		i++, pStdEquipInfo++)
	{
		//��¼���豸
		if(GET_EQUIP_TYPE_ID_SUB(pStdEquipInfo->nEquipType) == 0)
		{
			//���豸����ֻ����һ��
			ASSERT(pStdEquipInfo->ppActualEquip[0] != NULL);
			ASSERT(pDispClassEquipNode->pGroupEquipInfo == NULL);

			pDispClassEquipNode->nGroupEquipType = pStdEquipInfo->nEquipType;
			pDispClassEquipNode->pGroupEquipInfo = pStdEquipInfo->ppActualEquip[0];

		}

		switch(nSigType)
		{
		case SIG_TYPE_SAMPLING:
			if(pStdEquipInfo->stSignalInfo.iSampleSigNum > 0)
			{
				for(j = 0; j < pStdEquipInfo->nActEquipNum; j++)
				{
					pDispActEquipNode = pDispClassEquipNode->pDispActEquipNode + pDispClassEquipNode->nDispActEquipNum++;
					pDispActEquipNode->pSignalInfo = &(pStdEquipInfo->stSignalInfo);
					pDispActEquipNode->pEquipInfo = pStdEquipInfo->ppActualEquip[j];

				}
			}
			break;

		case SIG_TYPE_CONTROL:
			if(pStdEquipInfo->stSignalInfo.iCtrlSigNum > 0)
			{
				for(j = 0; j < pStdEquipInfo->nActEquipNum; j++)
				{
					pDispActEquipNode = pDispClassEquipNode->pDispActEquipNode + pDispClassEquipNode->nDispActEquipNum++;
					pDispActEquipNode->pSignalInfo = &(pStdEquipInfo->stSignalInfo);
					pDispActEquipNode->pEquipInfo = pStdEquipInfo->ppActualEquip[j];

				}
			}
			break;

		case SIG_TYPE_SETTING:
			if(pStdEquipInfo->stSignalInfo.iSetSigNum > 0)
			{
				for(j = 0; j < pStdEquipInfo->nActEquipNum; j++)
				{
					pDispActEquipNode = pDispClassEquipNode->pDispActEquipNode + pDispClassEquipNode->nDispActEquipNum++;
					pDispActEquipNode->pSignalInfo = &(pStdEquipInfo->stSignalInfo);
					pDispActEquipNode->pEquipInfo = pStdEquipInfo->ppActualEquip[j];

				}
			}
			break;

		case SIG_TYPE_ALARM:
			if(pStdEquipInfo->stSignalInfo.iAlarmSigNum > 0)
			{
				for(j = 0; j < pStdEquipInfo->nActEquipNum; j++)
				{
					pDispActEquipNode = pDispClassEquipNode->pDispActEquipNode + pDispClassEquipNode->nDispActEquipNum++;
					pDispActEquipNode->pSignalInfo = &(pStdEquipInfo->stSignalInfo);
					pDispActEquipNode->pEquipInfo = pStdEquipInfo->ppActualEquip[j];

				}
			}
			break;

		default:
			break;

		}
	}

	//Check
	ASSERT(pDispClassEquipNode->nDispActEquipNum == nDispActEquipNum);

	//Order
	qsort(pDispClassEquipNode->pDispActEquipNode,
		(size_t)pDispClassEquipNode->nDispActEquipNum,
		sizeof(DISP_ACTUAL_EQUIP_NODE),
		(int(*)(const void *, const void *))Compare_ActEquipID);

	return ERR_LCD_OK;
}

static int ParseMainMenuNode(PARSE_MENU_NODE* pParseMenuNode)
{
	ASSERT(pParseMenuNode);
	ZERO_POBJS(pParseMenuNode, 1);

	int nSigType, j;

	PARSE_EQUIP_INFO *pParseEquipInfo = &g_stParseEquipInfo;

	CLASS_EQUIP_INFO *pClassEquipInfo;

	int nDispClassEquipNum;


	for(nSigType = SIG_TYPE_SAMPLING; nSigType <= SIG_TYPE_ALARM; nSigType++)
	{
		nDispClassEquipNum = GetDispClassEquipNum(pParseEquipInfo, nSigType);
		if(nDispClassEquipNum <= 0)
		{
			continue;
		}

		pParseMenuNode->pDispClassEquipNode[nSigType] = NEW(DISP_CLASS_EQUIP_NODE, nDispClassEquipNum);
		LOGOUT_NO_MEMORY(pParseMenuNode->pDispClassEquipNode[nSigType]);
		ZERO_POBJS(pParseMenuNode->pDispClassEquipNode[nSigType], nDispClassEquipNum);

		//�Խ�������ÿһ��ClassEquip���з�����������ʾ���豸�ŵ�pParseMenuNode��
		pClassEquipInfo = pParseEquipInfo->pClassEquipInfo;
		for(j = 0; 
			(j < pParseEquipInfo->nClassEquipNum) && (pClassEquipInfo != NULL);
			j++, pClassEquipInfo++)
		{
			ParseClassEquipNode(pParseMenuNode, pClassEquipInfo, nSigType);
		}

		ASSERT(pParseMenuNode->nDispClassEquipNum[nSigType] == nDispClassEquipNum);

		//Order
		qsort(pParseMenuNode->pDispClassEquipNode[nSigType],
			(size_t)pParseMenuNode->nDispClassEquipNum[nSigType],
			sizeof(DISP_CLASS_EQUIP_NODE),
			(int(*)(const void *, const void *))Compare_ClassEquipType);

	}

	return ERR_LCD_OK;
}








static int ConstructEquipSubMenu(SUB_MENU* pEquipSubMenu,
								 int nSigType)
{
	//ǰ�᣺���Ӳ˵���������Ϣ������������������õȣ��Ѿ������ã�
	//		������Ҫ�����Ӳ˵���ã���������һ�����ź����Ӳ˵���
	ASSERT(pEquipSubMenu);

	int				*pDispItems;

	MENU_ITEM*		pEquipItem;
	MENU_ITEM*		pSubEquipItem;

	//EQUIP_INFO*		pEquipInfo;
	//SIGNAL_INFO*	pSignalInfo;

	SUB_MENU*		pSignalSubMenu;
	SUB_MENU*		pVirGroupEquipSubMenu;

	int i, k;
	int iResult;

	PARSE_MENU_NODE *pParseMenuNode = &g_stParseMenuNode;

	int		nClassEquipNum;
	nClassEquipNum = pParseMenuNode->nDispClassEquipNum[nSigType];
	ASSERT(pEquipSubMenu->nItems >= nClassEquipNum);


	if(nSigType == SIG_TYPE_SAMPLING)
	{
		//ǰ���и�����ǰ�澯��
		pEquipItem = pEquipSubMenu->pItems + ACTIVE_ALARM_MENU_NUM;
	}
	else if(nSigType == SIG_TYPE_CONTROL)
	{
		//ǰ���и������Զ����ơ�
		pEquipItem = pEquipSubMenu->pItems + MAN_AUTO_SET_IN_CTRL_MENU;
	}
	else if(nSigType == SIG_TYPE_SETTING)
	{
		//ǰ���и����澯�������á�
		pEquipItem = pEquipSubMenu->pItems + ALARM_SET_MENU_NUM;
	}
	else
	{
		pEquipItem = pEquipSubMenu->pItems;
	}

	DISP_CLASS_EQUIP_NODE *pDispClassEquip;
	pDispClassEquip = pParseMenuNode->pDispClassEquipNode[nSigType];

	int		nDispActEquipNum;
	EQUIP_INFO		*pDispActEquip;
	SIGNAL_INFO*	pSignalInfo;

	for(i = 0; 
		(i < pParseMenuNode->nDispClassEquipNum[nSigType])
			&& (pDispClassEquip != NULL) && (pEquipItem != NULL);
		i++, pDispClassEquip++, pEquipItem++)
	{
		nDispActEquipNum = pDispClassEquip->nDispActEquipNum;

		ASSERT(nDispActEquipNum > 0);

		if(nDispActEquipNum == 1)	//Single equip
		{
			pDispActEquip = pDispClassEquip->pDispActEquipNode->pEquipInfo;
			ASSERT(pDispActEquip);

			pSignalInfo = pDispClassEquip->pDispActEquipNode->pSignalInfo;
			ASSERT(pSignalInfo);

			iResult = CreateSignalSubMenu(&pSignalSubMenu, pDispActEquip, pSignalInfo, nSigType);
			if(iResult)		//Failure
			{
				return iResult;
			}
			//�������õ��ź����Ӳ˵�pSignalSubMenu���ӵ��豸�˵���
			INIT_SUBMENU_ITEM(pEquipItem, 
				LCD_MERGE_ACTUAL_EQUIP_ID(nSigType, pDispActEquip->iEquipID),
				MENU_ITEM_NEED_NOT_VALIDATE, 
				MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
				pSignalSubMenu);

		}
		else //multiple equips
		{
			//����һ���������豸�Ӳ˵�����nDispActEquipNum���˵���
			pVirGroupEquipSubMenu = NEW(SUB_MENU, 1);
			LOGOUT_NO_MEMORY(pVirGroupEquipSubMenu);
			ZERO_POBJS(pVirGroupEquipSubMenu, 1);

			pSubEquipItem = NEW(MENU_ITEM, nDispActEquipNum);
			LOGOUT_NO_MEMORY(pSubEquipItem);
			ZERO_POBJS(pSubEquipItem, nDispActEquipNum);

			pDispItems = NEW(int, nDispActEquipNum);
			LOGOUT_NO_MEMORY(pDispItems);
			ZERO_POBJS(pDispItems, nDispActEquipNum);

			//��ʼ���������豸�Ӳ˵�
			//	����������������������
			//	��Rectifier		    ��
			//	��	Rect1			��
			//	��	Rect2			��
			//	��	Rect3			��
			//	����������������������
			INIT_SUB_MENU(pVirGroupEquipSubMenu,
				LCD_MERGE_VIRTUAL_GROUP_EQUIP_ID(nSigType, pDispClassEquip->nGroupEquipType),
				nDispActEquipNum,
				pSubEquipItem,
				pDispItems);

			//�����������豸�Ӳ˵����ӵ�Ҫ�������Ӳ˵�(��������Ϣ)���豸��
			//				  	����������������������
			//	Rect	<---	��Rectifier		    ��
			//					��	Rect1			��
			//					��	Rect2			��
			//					��	Rect3			��
			//					����������������������
			INIT_SUBMENU_ITEM(pEquipItem, 
				LCD_MERGE_VIRTUAL_GROUP_EQUIP_ID(nSigType, pDispClassEquip->nGroupEquipType),
				MENU_ITEM_NEED_NOT_VALIDATE, 
				MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
				pVirGroupEquipSubMenu);	

			//������������豸�Ӳ˵��£���ÿ��ʵ���豸�����ź����Ӳ˵�
			for(k = 0; k < pDispClassEquip->nDispActEquipNum; k++, pSubEquipItem++)
			{
				pDispActEquip = (pDispClassEquip->pDispActEquipNode + k)->pEquipInfo;
				ASSERT(pDispActEquip);

				pSignalInfo = (pDispClassEquip->pDispActEquipNode + k)->pSignalInfo;
				ASSERT(pSignalInfo);

				iResult = CreateSignalSubMenu(&pSignalSubMenu,
					pDispActEquip, pSignalInfo, nSigType);
				if(iResult)
				{
					return iResult;
				}

				//����ʵ���豸���ź����Ӳ˵����ӵ��������豸�Ӳ˵��Ĳ˵�����
				//				  	����������������������
				//	Rect1	<---	��Rect Volt		    ��
				//					��		53.5 V		��
				//					��Rect Curr			��
				//					��		30.5 A		��
				//					����������������������
				INIT_SUBMENU_ITEM(pSubEquipItem, 
					LCD_MERGE_ACTUAL_EQUIP_ID(nSigType, pDispActEquip->iEquipID), 
					MENU_ITEM_NEED_NOT_VALIDATE, 
					MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
					pSignalSubMenu);
			}

		}
	}

	return ERR_LCD_OK;

}


//Frank Wu,20160127, for MiniNCU
static int ConstructEquipSubMenuMiniNCU(MENU_ITEM* pEquipItem, int nSigType, int nGroupEquipType)
{
	//ǰ�᣺���Ӳ˵���������Ϣ������������������õȣ��Ѿ������ã�
	//		������Ҫ�����Ӳ˵���ã���������һ�����ź����Ӳ˵���
	ASSERT(pEquipSubMenu);

	int				*pDispItems;
	MENU_ITEM*		pSubEquipItem;

	//EQUIP_INFO*		pEquipInfo;
	//SIGNAL_INFO*	pSignalInfo;

	SUB_MENU*		pSignalSubMenu;
	SUB_MENU*		pVirGroupEquipSubMenu;

	int i, k;
	int iResult;

	PARSE_MENU_NODE *		pParseMenuNode = &g_stParseMenuNode;
	DISP_CLASS_EQUIP_NODE *	pDispClassEquip;
	int						nClassEquipNum;
	
	pDispClassEquip = pParseMenuNode->pDispClassEquipNode[nSigType];
	nClassEquipNum = pParseMenuNode->nDispClassEquipNum[nSigType];


	int		nDispActEquipNum;
	EQUIP_INFO		*pDispActEquip;
	SIGNAL_INFO*	pSignalInfo;

	for(i = 0; 
		(i < nClassEquipNum) && (pDispClassEquip != NULL) && (pEquipItem != NULL);
		i++, pDispClassEquip++)
	{
		if(nGroupEquipType != pDispClassEquip->nGroupEquipType)
		{
			continue;
		}

		nDispActEquipNum = pDispClassEquip->nDispActEquipNum;

		ASSERT(nDispActEquipNum > 0);

		if(nDispActEquipNum == 1)	//Single equip
		{
			pDispActEquip = pDispClassEquip->pDispActEquipNode->pEquipInfo;
			ASSERT(pDispActEquip);

			pSignalInfo = pDispClassEquip->pDispActEquipNode->pSignalInfo;
			ASSERT(pSignalInfo);

			iResult = CreateSignalSubMenu(&pSignalSubMenu, pDispActEquip, pSignalInfo, nSigType);
			if(iResult)		//Failure
			{
				return iResult;
			}
			//�������õ��ź����Ӳ˵�pSignalSubMenu���ӵ��豸�˵���
			INIT_SUBMENU_ITEM(pEquipItem, 
				LCD_MERGE_ACTUAL_EQUIP_ID(nSigType, pDispActEquip->iEquipID),
				MENU_ITEM_NEED_NOT_VALIDATE, 
				MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
				pSignalSubMenu);

		}
		else //multiple equips
		{
			//����һ���������豸�Ӳ˵�����nDispActEquipNum���˵���
			pVirGroupEquipSubMenu = NEW(SUB_MENU, 1);
			LOGOUT_NO_MEMORY(pVirGroupEquipSubMenu);
			ZERO_POBJS(pVirGroupEquipSubMenu, 1);

			pSubEquipItem = NEW(MENU_ITEM, nDispActEquipNum);
			LOGOUT_NO_MEMORY(pSubEquipItem);
			ZERO_POBJS(pSubEquipItem, nDispActEquipNum);

			pDispItems = NEW(int, nDispActEquipNum);
			LOGOUT_NO_MEMORY(pDispItems);
			ZERO_POBJS(pDispItems, nDispActEquipNum);

			//��ʼ���������豸�Ӳ˵�
			//	����������������������
			//	��Rectifier		    ��
			//	��	Rect1			��
			//	��	Rect2			��
			//	��	Rect3			��
			//	����������������������
			INIT_SUB_MENU(pVirGroupEquipSubMenu,
				LCD_MERGE_VIRTUAL_GROUP_EQUIP_ID(nSigType, pDispClassEquip->nGroupEquipType),
				nDispActEquipNum,
				pSubEquipItem,
				pDispItems);

			//�����������豸�Ӳ˵����ӵ�Ҫ�������Ӳ˵�(��������Ϣ)���豸��
			//				  	����������������������
			//	Rect	<---	��Rectifier		    ��
			//					��	Rect1			��
			//					��	Rect2			��
			//					��	Rect3			��
			//					����������������������
			INIT_SUBMENU_ITEM(pEquipItem, 
				LCD_MERGE_VIRTUAL_GROUP_EQUIP_ID(nSigType, pDispClassEquip->nGroupEquipType),
				MENU_ITEM_NEED_NOT_VALIDATE, 
				MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
				pVirGroupEquipSubMenu);	

			//������������豸�Ӳ˵��£���ÿ��ʵ���豸�����ź����Ӳ˵�
			for(k = 0; k < pDispClassEquip->nDispActEquipNum; k++, pSubEquipItem++)
			{
				pDispActEquip = (pDispClassEquip->pDispActEquipNode + k)->pEquipInfo;
				ASSERT(pDispActEquip);

				pSignalInfo = (pDispClassEquip->pDispActEquipNode + k)->pSignalInfo;
				ASSERT(pSignalInfo);

				iResult = CreateSignalSubMenu(&pSignalSubMenu,
					pDispActEquip, pSignalInfo, nSigType);
				if(iResult)
				{
					return iResult;
				}

				//����ʵ���豸���ź����Ӳ˵����ӵ��������豸�Ӳ˵��Ĳ˵�����
				//				  	����������������������
				//	Rect1	<---	��Rect Volt		    ��
				//					��		53.5 V		��
				//					��Rect Curr			��
				//					��		30.5 A		��
				//					����������������������
				INIT_SUBMENU_ITEM(pSubEquipItem, 
					LCD_MERGE_ACTUAL_EQUIP_ID(nSigType, pDispActEquip->iEquipID), 
					MENU_ITEM_NEED_NOT_VALIDATE, 
					MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
					pSignalSubMenu);
			}

		}
	}

	return ERR_LCD_OK;

}


/*==========================================================================*
 * FUNCTION : CreateSignalSubMenu
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: EQUIP_INFO*   pEquipInfo  : 
 *            SIGNAL_INFO*  pSignalInfo : 
 * RETURN   : static SUB_MENU* : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng                DATE: 2008-07-08 17:33
 *==========================================================================*/
static int CreateSignalSubMenu(OUT SUB_MENU** ppSubMenu,
							   EQUIP_INFO* pEquipInfo, 
							   SIGNAL_INFO* pSignalInfo,
							   int nSigType)
{
	MENU_ITEM*	pSignalItem;
	int			*pDispItems;
	int			i;
	int			nSigNum;
	char		cMenuType;
	char		cValidateFlag;
	char		cAcknowlegeFlag;

	switch(nSigType)
	{
	case SIG_TYPE_SAMPLING:
		nSigNum = pSignalInfo->iSampleSigNum;
		cMenuType = MT_SIGNAL_DISPLAY;
		cValidateFlag = MENU_ITEM_NEED_NOT_VALIDATE;
		cAcknowlegeFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE;
		break;

	case SIG_TYPE_CONTROL:
		nSigNum = pSignalInfo->iCtrlSigNum;
		cMenuType = MT_SIGNAL_CONTROL;
		cValidateFlag	= MENU_ITEM_NEED_VALIDATE;
		cAcknowlegeFlag	= MENU_ITEM_NEED_ACKNOWLEDGE;
		break;

	case SIG_TYPE_SETTING:
		nSigNum = pSignalInfo->iSetSigNum;
		cMenuType = MT_SIGNAL_SET;
		cValidateFlag	= MENU_ITEM_NEED_VALIDATE;
		cAcknowlegeFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE;
		break;

	case SIG_TYPE_ALARM:
		nSigNum = pSignalInfo->iAlarmSigNum;
		cMenuType = MT_ALARM_LEVEL_SET;
		cValidateFlag	= MENU_ITEM_NEED_VALIDATE;
		cAcknowlegeFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE;
		break;

	default:
		DEBUG_LCD_FILE_FUN_LINE_STRING("Unknow SigType");
		*ppSubMenu = NULL;
		return ERR_LCD_FAILURE;
	}

	if(nSigNum > 0)
	{
		*ppSubMenu = NEW(SUB_MENU, 1);
		LOGOUT_NO_MEMORY(*ppSubMenu);
		ZERO_POBJS(*ppSubMenu, 1);

		pSignalItem = NEW(MENU_ITEM, nSigNum);
		LOGOUT_NO_MEMORY(pSignalItem);
		ZERO_POBJS(pSignalItem, nSigNum);

		pDispItems = NEW(int, nSigNum);
		LOGOUT_NO_MEMORY(pDispItems);
		ZERO_POBJS(pDispItems, nSigNum);

		INIT_SUB_MENU(*ppSubMenu,
				LCD_MERGE_ACTUAL_EQUIP_ID(nSigType, pEquipInfo->iEquipID), 
				nSigNum,
				pSignalItem,
				pDispItems);

		int		nSigID;
		void*	pSigInfoPointer;

		switch(nSigType)
		{
		case SIG_TYPE_SAMPLING:
			for(i = 0; i < nSigNum && pSignalItem != NULL; i++, pSignalItem++)
			{
				ASSERT(pSignalInfo->ppSampleSigInfo[i]);
				nSigID = pSignalInfo->ppSampleSigInfo[i]->iSigID;
				pSigInfoPointer = pSignalInfo->ppSampleSigInfo[i];

				INIT_SIGNAL_ITEM(pSignalItem, 
					cMenuType,
					DXI_MERGE_UNIQUE_SIG_ID(pEquipInfo->iEquipID, nSigType, nSigID),
					cValidateFlag, 
					cAcknowlegeFlag,
					pSigInfoPointer);
			}
			break;

		case SIG_TYPE_CONTROL:
			for(i = 0; i < nSigNum && pSignalItem != NULL; i++, pSignalItem++)
			{
				ASSERT(pSignalInfo->ppCtrlSigInfo[i]);
				nSigID = pSignalInfo->ppCtrlSigInfo[i]->iSigID;
				pSigInfoPointer = pSignalInfo->ppCtrlSigInfo[i];

				INIT_SIGNAL_ITEM(pSignalItem, 
					cMenuType,
					DXI_MERGE_UNIQUE_SIG_ID(pEquipInfo->iEquipID, nSigType, nSigID),
					cValidateFlag, 
					cAcknowlegeFlag,
					pSigInfoPointer);
			}
			break;

		case SIG_TYPE_SETTING:
			for(i = 0; i < nSigNum && pSignalItem != NULL; i++, pSignalItem++)
			{
				ASSERT(pSignalInfo->ppSetSigInfo[i]);
				nSigID = pSignalInfo->ppSetSigInfo[i]->iSigID;
				pSigInfoPointer = pSignalInfo->ppSetSigInfo[i];

				INIT_SIGNAL_ITEM(pSignalItem, 
					cMenuType,
					DXI_MERGE_UNIQUE_SIG_ID(pEquipInfo->iEquipID, nSigType, nSigID),
					cValidateFlag, 
					cAcknowlegeFlag,
					pSigInfoPointer);
			}
			break;

		case SIG_TYPE_ALARM:
			for(i = 0; i < nSigNum && pSignalItem != NULL; i++, pSignalItem++)
			{
				ASSERT(pSignalInfo->ppAlarmSigInfo[i]);
				nSigID = pSignalInfo->ppAlarmSigInfo[i]->iSigID;
				pSigInfoPointer = pSignalInfo->ppAlarmSigInfo[i];

				INIT_SIGNAL_ITEM(pSignalItem, 
					cMenuType,
					DXI_MERGE_UNIQUE_SIG_ID(pEquipInfo->iEquipID, nSigType, nSigID),
					cValidateFlag, 
					cAcknowlegeFlag,
					pSigInfoPointer);
			}
			break;

		default:
			DEBUG_LCD_FILE_FUN_LINE_STRING("Unknow SigType");
			*ppSubMenu = NULL;
			return ERR_LCD_FAILURE;
		}
	}
	else
	{
		*ppSubMenu = NULL;
		return ERR_LCD_OK;
	}

	return ERR_LCD_OK;
}




static int AddAlarmSetSigSubMenu(MENU_ITEM *pParentMenuItem, STANDARD_EQUIP_INFO *pStdEquip, int nSigType)
{
	ASSERT(pParentMenuItem);
	ASSERT(pStdEquip);

	SUB_MENU*	pSubMenu;
	MENU_ITEM*	pMenuItem;
	int			*pDispItems;

	SIGNAL_INFO	*pSignalInfo;
	pSignalInfo = &(pStdEquip->stSignalInfo);

	int			nMenuItemNum = pSignalInfo->iAlarmSigNum;

	//����������pStdEquipΪ��Ҫ��ʾ�ı�׼�豸���澯�ź���Ӧ�ô���0
	ASSERT(nMenuItemNum > 0);

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItem = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItem);
	ZERO_POBJS(pMenuItem, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_STD_EQUIP_ID(nSigType, pStdEquip->nEquipType),
		nMenuItemNum,
		pMenuItem,
		pDispItems);

	INIT_SUBMENU_ITEM(pParentMenuItem, 
		LCD_MERGE_STD_EQUIP_ID(nSigType, pStdEquip->nEquipType),
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	int i;

	char	cMenuType;

	if(nSigType == SIG_TYPE_ALARM_LEVEL_SET)
	{
		cMenuType = MT_ALARM_LEVEL_SET;
	}
	else if(nSigType == SIG_TYPE_ALARM_RELAY_SET)
	{
		cMenuType = MT_ALARM_RELAY_SET;
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("Unknow nSigType \n");
		cMenuType = MT_SIGNAL_DISPLAY;	//Error
	}


	ALARM_SIG_INFO *pAlarmSigInfo;
	for(i = 0; i < nMenuItemNum; i++, pMenuItem++)
	{
		pAlarmSigInfo = pSignalInfo->ppAlarmSigInfo[i];
		ASSERT(pAlarmSigInfo);

		INIT_SIGNAL_ITEM(pMenuItem,
			cMenuType,
			DXI_MERGE_UNIQUE_SIG_ID(pStdEquip->nEquipType, nSigType, pAlarmSigInfo->iSigID),
            MENU_ITEM_NEED_VALIDATE,
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			pAlarmSigInfo);
	}

	return ERR_LCD_OK;
}


static int AddAlarmLevelOrRelaySetMenu(MENU_ITEM *pParentMenuItem, 
								ALARM_SET_EQUIP_NODE *pAlarmSetEquipNode,
								int	nSigType)
{
	SUB_MENU*	pSubMenu;
	MENU_ITEM*	pMenuItem;
	int			*pDispItems;

	int			nMenuItemNum = pAlarmSetEquipNode->nAlarmSetEquipNum;

	if(nMenuItemNum <= 0)
	{
		return ERR_LCD_OK;
	}

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItem = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItem);
	ZERO_POBJS(pMenuItem, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	int nMenuItemID;
	if(nSigType == SIG_TYPE_ALARM_LEVEL_SET)
	{
		nMenuItemID = LCD_MERGE_FIXED_LANG_ID(ALARM_LEVEL_SET_LANG_ID);
	}
	else if(nSigType == SIG_TYPE_ALARM_RELAY_SET)
	{
		nMenuItemID = LCD_MERGE_FIXED_LANG_ID(ALARM_RELAY_SET_LANG_ID);
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("Unknow nSigType \n");
		INIT_SUBMENU_ITEM(pParentMenuItem, 
			nMenuItemID, 
			MENU_ITEM_NEED_NOT_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			NULL);	
		return ERR_LCD_FAILURE;
	}

	INIT_SUB_MENU(pSubMenu,
		nMenuItemID,
		nMenuItemNum,
		pMenuItem,
		pDispItems);

	INIT_SUBMENU_ITEM(pParentMenuItem, 
		nMenuItemID, 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	


	int i;
	STANDARD_EQUIP_INFO *pStdEquip;

	for(i = 0;
		(i < nMenuItemNum) && (pMenuItem) && pStdEquip;
		i++, pMenuItem++)
	{
		pStdEquip = pAlarmSetEquipNode->ppStandardEquipInfo[i];
		AddAlarmSetSigSubMenu(pMenuItem, pStdEquip, nSigType);
	}

	return ERR_LCD_OK;
}


#define ALARM_CONTROL_ITEM_DISP_FORMAT	""
static int AddAlarmControlSetMenu(MENU_ITEM *pParentMenuItem, 
								ALARM_SET_EQUIP_NODE *pAlarmSetEquipNode)
{
	ASSERT(pParentMenuItem);
	UNUSED(pAlarmSetEquipNode);

	//For clear history alarm  control
	//static LANG_TEXT*		s_ClearHistoryAlarmTextArray[1];


	static SELF_DEFINE_INFO s_AlarmControlItemInfo[] = 
	{
		//Alarm voice
		DEF_SELF_DEFINE_INFO(ALARM_VOICE_CTRL_ID, 
			NULL,
			VAR_ENUM, 
			OPERATOR_LEVEL, 
			ALARM_CONTROL_ITEM_DISP_FORMAT, 
			1, 
			5,
			0,
			6,
			NULL,//Will set text array in GetAlarmVoiceCtrlPreInfo()
			NULL),

		//outgoing alarm
		DEF_SELF_DEFINE_INFO(OUTGOING_ALARM_CTRL_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			ALARM_CONTROL_ITEM_DISP_FORMAT, 
			1, 
			1,
			0,
			0,
			NULL,//Will set text array in GetOutgoingAlarmItemPreInfo()
			NULL),

		//clear history alarm
		DEF_SELF_DEFINE_INFO(CLEAR_HIS_ALARM_CTRL_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			ALARM_CONTROL_ITEM_DISP_FORMAT, 
			0, 
			0,
			0,
			1,
			NULL,
			NULL),

	};


#define ALARM_CONTROL_ITEM_NUMBER \
	(sizeof(s_AlarmControlItemInfo)/sizeof(SELF_DEFINE_INFO))

	//alarm control item number
	int		nSigNum = ALARM_CONTROL_ITEM_NUMBER;

	SUB_MENU*	pSubMenu;
	MENU_ITEM*	pSignalItem;
	int			*pDispItems;

	char		cMenuType = MT_SELF_DEF_SET;

	int i;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pSignalItem = NEW(MENU_ITEM, nSigNum);
	LOGOUT_NO_MEMORY(pSignalItem);
	ZERO_POBJS(pSignalItem, nSigNum);

	pDispItems = NEW(int, nSigNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nSigNum);

	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_FIXED_LANG_ID(ALARM_CONTROL_LANG_ID),
		nSigNum,
		pSignalItem,
		pDispItems);

	for(i = 0; i < (int)ALARM_CONTROL_ITEM_NUMBER; i++)
	{
		int nAckFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE;

		if (s_AlarmControlItemInfo[i].iSigID == CLEAR_HIS_ALARM_CTRL_ID
			|| s_AlarmControlItemInfo[i].iSigID == OUTGOING_ALARM_CTRL_ID)
		{
			nAckFlag = MENU_ITEM_NEED_ACKNOWLEDGE;
		}

		//Add each alarm ctrl set item
		INIT_SIGNAL_ITEM(&(pSignalItem[i]), 
			cMenuType,
			LCD_MERGE_FIXED_LANG_ID(s_AlarmControlItemInfo[i].iSigID),
			MENU_ITEM_NEED_VALIDATE, 
			nAckFlag,
			&(s_AlarmControlItemInfo[i]));
	}

	INIT_SUBMENU_ITEM(pParentMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(ALARM_CONTROL_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	return ERR_LCD_OK;
}

static int AddIBAlarmLevelSetMenu(MENU_ITEM *pParentMenuItem, 
								ALARM_SET_EQUIP_NODE *pAlarmSetEquipNode)
{
	// ע�⣺Ŀǰֻ֧��һ��IB��������
	//   �����������õĸ澯�ߵ͵�ƽֻ�Ե�һ��IB����Ч��������
	ASSERT(pParentMenuItem);
	UNUSED(pAlarmSetEquipNode);

	int	nInterfaceType = VAR_SET_SIG_NUM_OF_EQUIP;
	int	nVarID = DXI_GetEquipIDFromStdEquipID(IB_EQUIP_TYPE);
		
	int nVarSubID = 0;
	int nBufLen;
	int nTimeOut = 5000;
	int nError;

	// Get the number of setting signals of IB
	int	nMenuItemNum;
	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&nMenuItemNum,			
		nTimeOut);

	if((ERR_DXI_OK != nError) || (nMenuItemNum <= 0))
	{
		INIT_SUBMENU_ITEM(pParentMenuItem, 
			LCD_MERGE_FIXED_LANG_ID(ALARM_VOLTAGE_IB_LANG_ID), 
			MENU_ITEM_NEED_NOT_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			NULL);

		return ERR_LCD_DXI_GET_DATA_FAILURE;
	}


	SUB_MENU*	pSubMenu;
	MENU_ITEM*	pMenuItem;
	int			*pDispItems;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItem = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItem);
	ZERO_POBJS(pMenuItem, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_FIXED_LANG_ID(ALARM_VOLTAGE_IB_LANG_ID),
		nMenuItemNum,
		pMenuItem,
		pDispItems);

	INIT_SUBMENU_ITEM(pParentMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(ALARM_VOLTAGE_IB_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	// Get the setting signals of IB
	SET_SIG_INFO *pSetSigInfo = NULL;

	nInterfaceType = VAR_SET_SIG_STRU_OF_EQUIP;

	DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pSetSigInfo,			
		nTimeOut);

	int i;
	for(i = 0;
		(i < nMenuItemNum) && (pMenuItem) && (pSetSigInfo);
		i++, pMenuItem++, pSetSigInfo++)
	{
		INIT_SIGNAL_ITEM(pMenuItem,
			MT_SIGNAL_SET,
			DXI_MERGE_UNIQUE_SIG_ID(nVarID, SIG_TYPE_SETTING, pSetSigInfo->iSigID),
			MENU_ITEM_NEED_VALIDATE,
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			pSetSigInfo);
	}

	return ERR_LCD_OK;
}




/*==========================================================================*
* FUNCTION : AddAlarmSetItems
* PURPOSE  : Add alarm set items to param set sub menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*  pEquipItem : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:13
*==========================================================================*/
static int AddAlarmSetItems(MENU_ITEM* pParentMenuItem)
{
	int nError = ERR_LCD_OK;

	SUB_MENU*	pSubMenu;
	MENU_ITEM*	pMenuItem;
	int			*pDispItems;

	int			nMenuItemNum = ALARM_SET_MENU_ITEM_NUM;

	/* Alarm set menu -- SUB_MENU */
	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItem = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItem);
	ZERO_POBJS(pMenuItem, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_FIXED_LANG_ID(ALARM_PARAM_SET_LANG_ID),
		ALARM_SET_MENU_ITEM_NUM,
		pMenuItem,
		pDispItems);

	INIT_SUBMENU_ITEM(pParentMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(ALARM_PARAM_SET_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	


	//���ݱ�׼�豸��������ռ�
	PARSE_EQUIP_INFO *pParseEquipInfo = &g_stParseEquipInfo;
	int nStdEquipNum = pParseEquipInfo->nStdEquipNum;

	ALARM_SET_EQUIP_NODE stAlarmSetEquipNode;
	ALARM_SET_EQUIP_NODE *pAlarmSetEquipNode = &stAlarmSetEquipNode;
	ZERO_POBJS(pAlarmSetEquipNode, 1);

	pAlarmSetEquipNode->ppStandardEquipInfo = 
		NEW(STANDARD_EQUIP_INFO*, nStdEquipNum);
	LOGOUT_NO_MEMORY(pAlarmSetEquipNode->ppStandardEquipInfo);
	ZERO_POBJS(pAlarmSetEquipNode->ppStandardEquipInfo, nStdEquipNum);

	//��������Ҫ��ʾ�ı�׼�豸
	int i;
	STANDARD_EQUIP_INFO	*pStdEquipInfo;

	for(i = 0; i < pParseEquipInfo->nStdEquipNum; i++)
	{
		pStdEquipInfo = pParseEquipInfo->pStdEquipInfo + i;

		ASSERT(pStdEquipInfo);

		//�ٳ� ���Դ�� Ҫ���豸
		
		if ((301 == pStdEquipInfo->nEquipType)
			||(302 == pStdEquipInfo->nEquipType)
			||(303 == pStdEquipInfo->nEquipType)
			||(304 == pStdEquipInfo->nEquipType)
			||(306 == pStdEquipInfo->nEquipType)
			||(307 == pStdEquipInfo->nEquipType)
			||(401 == pStdEquipInfo->nEquipType)
			||(402 == pStdEquipInfo->nEquipType)
			||(403 == pStdEquipInfo->nEquipType)
			||(501 == pStdEquipInfo->nEquipType)
			||(502 == pStdEquipInfo->nEquipType)
			||(503 == pStdEquipInfo->nEquipType)
			||(504 == pStdEquipInfo->nEquipType)
			||(601 == pStdEquipInfo->nEquipType)
			||(602 == pStdEquipInfo->nEquipType)
			//||(603 == pStdEquipInfo->nEquipType)//ldu LVD �澯����Ҫ��
			||(702 == pStdEquipInfo->nEquipType)
			||(703 == pStdEquipInfo->nEquipType)
			||(900 == pStdEquipInfo->nEquipType)
			||(901 == pStdEquipInfo->nEquipType)
			||((pStdEquipInfo->nEquipType <= 1208) && (pStdEquipInfo->nEquipType >= 1000))
			||((pStdEquipInfo->nEquipType <= 2101) && (pStdEquipInfo->nEquipType >= 1500))
			||(500 == pStdEquipInfo->nEquipType))
		{
			continue;
		}

		if(pStdEquipInfo->stSignalInfo.iAlarmSigNum > 0)
		{
			pAlarmSetEquipNode->ppStandardEquipInfo[pAlarmSetEquipNode->nAlarmSetEquipNum++]
				= pStdEquipInfo;
		}
	}

	if(pAlarmSetEquipNode->nAlarmSetEquipNum <= 0)
	{
		return ERR_LCD_OK;
	}

	//���Ӳ˵�

	pMenuItem = pSubMenu->pItems;

#if ALARM_LEVEL_SET_MENU_NUM
	AddAlarmLevelOrRelaySetMenu(pMenuItem, pAlarmSetEquipNode, SIG_TYPE_ALARM_LEVEL_SET);
	pMenuItem++;
#endif

#if ALARM_RELAY_SET_MENU_NUM
	AddAlarmLevelOrRelaySetMenu(pMenuItem, pAlarmSetEquipNode, SIG_TYPE_ALARM_RELAY_SET);
	pMenuItem++;
#endif

#if IB_ALARM_VOLT_SET_MENU_NUM
	AddIBAlarmLevelSetMenu(pMenuItem, pAlarmSetEquipNode);
	pMenuItem++;
#endif

#if ALARM_CONTROL_SET_MENU_NUM
	AddAlarmControlSetMenu(pMenuItem, pAlarmSetEquipNode);
	pMenuItem++;
#endif



	//�ͷſռ�
	DELETE_ITEM(pAlarmSetEquipNode->ppStandardEquipInfo);


	return nError;
}

/*==========================================================================*
* FUNCTION : AddActiveAlarmItems
* PURPOSE  : Add active alarm display items to run info sub menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*  pEquipItem : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:10
*==========================================================================*/
static int AddActiveAlarmItems(MENU_ITEM* pEquipItem)
{
	int nError = ERR_LCD_OK;

	SUB_MENU*	pSubMenu = NULL;
	MENU_ITEM*	pSignalItem;
	int			*pDispItems;

	int			nSigNum = 0;

	int			i;

	char		cMenuType = MT_ACTIVE_ALARM_DISPLAY;

	ASSERT(pEquipItem);

	//Delete old submenu and menu items
	if(pEquipItem->pvItemData != NULL)
	{
		DelMenuRecursively((SUB_MENU*)(pEquipItem->pvItemData));
	}


	nSigNum = g_s_ActiveAlarmManager.nAlarmCount;

	if (nSigNum > 0)
	{
		if(nSigNum > MINI_NCU_MAX_ACT_ALARM_COUNT)
		{
			nSigNum = MINI_NCU_MAX_ACT_ALARM_COUNT;
		}


		ALARM_SIG_VALUE_EX_LCD 	*pAlarmValue = 
			g_s_ActiveAlarmManager.pAlarmList;


		pSubMenu = NEW(SUB_MENU, 1);
		LOGOUT_NO_MEMORY(pSubMenu);
		ZERO_POBJS(pSubMenu, 1);

		pSignalItem = NEW(MENU_ITEM, nSigNum);
		LOGOUT_NO_MEMORY(pSignalItem);
		ZERO_POBJS(pSignalItem, nSigNum);

		pDispItems = NEW(int, nSigNum);
		LOGOUT_NO_MEMORY(pDispItems);
		ZERO_POBJS(pDispItems, nSigNum);

		INIT_SUB_MENU(pSubMenu,
			LCD_MERGE_FIXED_LANG_ID(ACTIVE_ALARM_DISP_LANG_ID), 
			nSigNum,
			pSignalItem,
			pDispItems);

		for(i = 0; i < nSigNum && pAlarmValue != NULL;
			i++, pSignalItem++, pAlarmValue = pAlarmValue->next)
		{
			//Add each set item
			INIT_SIGNAL_ITEM(pSignalItem, 
				cMenuType,
				DXI_MERGE_UNIQUE_SIG_ID(
				pAlarmValue->bvAlarm.pEquipInfo->iEquipID, 
				SIG_TYPE_ACTIVE_ALARM, pAlarmValue->bvAlarm.pStdSig->iSigID),
				MENU_ITEM_NEED_NOT_VALIDATE, 
				MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
				pAlarmValue);

		}

	}
	else
	{
		pSubMenu = NULL;
	}


	INIT_SUBMENU_ITEM(pEquipItem, 
		LCD_MERGE_FIXED_LANG_ID(ACTIVE_ALARM_DISP_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	LedAndBuzzerCtrlViaAlarmList();//Control led and buzzer

	return nError;

}



#define	PRODUCT_INFO_DIS_FORMAT		"14"

static int CreateBarcodeSigSubMenu(MENU_ITEM* pEquipItem, int nDeviceID)
{
	static SELF_DEFINE_INFO		s_ProductInfoItemInfo[] =
	{
		//module name		
		DEF_SELF_DEFINE_INFO(PRODUCT_INFO_NAME_LANG_ID,
			NULL,
			VAR_STRING,
			BROWSER_LEVEL,
			PRODUCT_INFO_DIS_FORMAT,
			0,
			0,
			0,
			0,
			NULL,
			NULL),

		//part number
		DEF_SELF_DEFINE_INFO(PRODUCT_INFO_PART_NUMBER_LANG_ID,
			NULL,
			VAR_STRING,
			BROWSER_LEVEL,
			PRODUCT_INFO_DIS_FORMAT,
			0,
			0,
			0,
			0,
			NULL,
			NULL),

		//hardware version
		DEF_SELF_DEFINE_INFO(PRODUCT_INFO_HWVERSION_LANG_ID,
			NULL,
			VAR_STRING,
			BROWSER_LEVEL,
			PRODUCT_INFO_DIS_FORMAT,
			0,
			0,
			0,
			0,
			NULL,
			NULL),

		//software version
		DEF_SELF_DEFINE_INFO(PRODUCT_INFO_SWVERSION_LANG_ID,
			NULL,
			VAR_STRING,
			BROWSER_LEVEL,
			PRODUCT_INFO_DIS_FORMAT,
			0,
			0,
			0,
			0,
			NULL,
			NULL),

		//serial number
		DEF_SELF_DEFINE_INFO(PRODUCT_INFO_SERIAL_LANG_ID,
			NULL,
			VAR_STRING,
			BROWSER_LEVEL,
			PRODUCT_INFO_DIS_FORMAT,
			0,
			0,
			0,
			0,
			NULL,
			NULL),
	};


	int	nSigNum = sizeof(s_ProductInfoItemInfo) / sizeof(SELF_DEFINE_INFO);

	SUB_MENU	*pSubMenu;
	MENU_ITEM	*pMenuItems;
	int			*pDispItems;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItems = NEW(MENU_ITEM, nSigNum);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, nSigNum);

	pDispItems = NEW(int, nSigNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nSigNum);


	//��ʼ���Ӳ˵�
	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_BARCODE_DEVICE_ID(nDeviceID),
		nSigNum, pMenuItems, pDispItems);

	INIT_SUBMENU_ITEM(pEquipItem,
		LCD_MERGE_BARCODE_DEVICE_ID(nDeviceID),
		MENU_ITEM_NEED_NOT_VALIDATE,
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);

	//��ʼ���˵���
	int	i;
	for(i = 0; i < nSigNum; i++)
	{
		INIT_BARCODE_ITEM(pMenuItems + i,
			LCD_MERGE_BARCODE_SIGNAL_ID(s_ProductInfoItemInfo[i].iSigID, nDeviceID),
			&(s_ProductInfoItemInfo[i]));
	}

	return ERR_LCD_OK;
}



static int AddSiteInventoryItems(MENU_ITEM* pParentMenuItem)
{
	ASSERT(pParentMenuItem);

	int nDeviceNum;
	nDeviceNum = DXI_GetDeviceNum();
	
	nDeviceNum += LUI_PRODUCTINFO_TOTAL_NUM;


	//�����Ӳ˵�
	SUB_MENU	*pSubMenu;
	MENU_ITEM	*pMenuItems;
	int			*pDispItems;

	//Create site inventory menu  -- SUB_MENU
	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);


	pMenuItems = NEW(MENU_ITEM, nDeviceNum);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, nDeviceNum);

	pDispItems = NEW(int, nDeviceNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nDeviceNum);

	//��ʼ������Ʒ��Ϣ���Ӳ˵�
	INIT_SUB_MENU(pSubMenu, 
		LCD_MERGE_FIXED_LANG_ID(PRODUCT_INFO_LANG_ID),
		nDeviceNum, pMenuItems, pDispItems);


	INIT_SUBMENU_ITEM(pParentMenuItem,
		LCD_MERGE_FIXED_LANG_ID(PRODUCT_INFO_LANG_ID),
		MENU_ITEM_NEED_NOT_VALIDATE,
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);


	//����ÿһ���豸��Barcode�ź��Ӳ˵�
	int	i;
	for(i = 0; i < nDeviceNum; i++)
	{
		//nDeviceID�Ǵ�1��ʼ��
		CreateBarcodeSigSubMenu(pMenuItems + i, i + 1);	
	}

	return ERR_LCD_OK;


}


/*==========================================================================*
* FUNCTION : AddHistoryAlarmItems
* PURPOSE  : Add history alarm display items to run info sub menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*  pEquipItem : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:10
*==========================================================================*/
void* AddHistoryAlarmItems(MENU_ITEM* pEquipItem)
{

	SUB_MENU*	pSubMenu = NULL;
	MENU_ITEM*	pSignalItem;
	int			*pDispItems;

	int			nSigNum = 0;
	int			i;

	char		cMenuType = MT_HISTORY_ALARM_DISPLAY;

	HANDLE		hHistoricalAlarm = NULL;

	HIS_ALARM_RECORD* pAlarmRecord = NULL;

	ASSERT(pEquipItem);

	if(pEquipItem->pvItemData != NULL)
	{
		DelMenuRecursively((SUB_MENU*)(pEquipItem->pvItemData));
	}


	hHistoricalAlarm =	DAT_StorageOpen(HIST_ALARM_LOG);

	// read the alarm as historical alarm.
	if (hHistoricalAlarm != NULL)
	{
		int iStartPos = 1;

		//pAlarmRecord should be deleted by the func which called this func
		pAlarmRecord = NEW(HIS_ALARM_RECORD, MAX_HIS_ALARM_COUNT);
		LOGOUT_NO_MEM_RETURN_NULL(pAlarmRecord);
		ZERO_POBJS(pAlarmRecord, MAX_HIS_ALARM_COUNT);

		nSigNum = MAX_HIS_ALARM_COUNT;

		if (!DAT_StorageReadRecords(hHistoricalAlarm, &iStartPos,
			&nSigNum, pAlarmRecord, 0, FALSE))
		{
			TRACEX("Readding historical alarm data ... FAILED.\n");

			nSigNum = 0;
		}
		
		DAT_StorageClose(hHistoricalAlarm);
	}

	if (nSigNum != 0)
	{
		if(nSigNum > MINI_NCU_MAX_HIS_ALARM_COUNT)
		{
			nSigNum = MINI_NCU_MAX_HIS_ALARM_COUNT;
		}

		HIS_ALARM_RECORD* pAlarmSigValue  = pAlarmRecord;

		pSubMenu = NEW(SUB_MENU, 1);
		LOGOUT_NO_MEM_RETURN_NULL(pSubMenu);
		ZERO_POBJS(pSubMenu, 1);

		pSignalItem = NEW(MENU_ITEM, nSigNum);
		LOGOUT_NO_MEM_RETURN_NULL(pSignalItem);
		ZERO_POBJS(pSignalItem, nSigNum);

		pDispItems = NEW(int, nSigNum);
		LOGOUT_NO_MEM_RETURN_NULL(pDispItems);
		ZERO_POBJS(pDispItems, nSigNum);

		INIT_SUB_MENU(pSubMenu,
			LCD_MERGE_FIXED_LANG_ID(HISTORY_ALARM_DISP_LANG_ID), 
			nSigNum,
			pSignalItem,
			pDispItems);

		for(i = 0; i < nSigNum; i++, pSignalItem++, pAlarmSigValue++)
		{
			//Add each set item
			INIT_SIGNAL_ITEM(pSignalItem, 
				cMenuType,
				//by HULONGWEN, �����Ż�����Ψһ����ʵʱ�澯�����ص��������Գ�����Ӱ��
				//OK����SIG_TYPE_ALARM�ĳ�SIG_TYPE_HISTORY_ALARM�����Ա�֤ID���ظ���
				DXI_MERGE_UNIQUE_SIG_ID(pAlarmSigValue->iEquipID, 
				SIG_TYPE_HISTORY_ALARM, pAlarmSigValue->iAlarmID),
				MENU_ITEM_NEED_NOT_VALIDATE, 
				MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
				pAlarmSigValue);
		}

	}
	else
	{
		DELETE_ITEM(pAlarmRecord);
		pSubMenu = NULL;
	}

	//Assign the equip ID, not standard equip ID
	INIT_SUBMENU_ITEM(pEquipItem, 
		LCD_MERGE_FIXED_LANG_ID(HISTORY_ALARM_DISP_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	


	return pAlarmRecord;
}

#define CLEAR_HISTORY_ALARM_DIS_FORMAT	"2"
#define SYS_KEYPAD_VOICE_SET_ID_FORMAT	"4"
#define OUT_GOING_ALARM_DIS_FORMAT		"2"
#define ALARM_VOICE_DIS_FORMAT			"8"
/*==========================================================================*
* FUNCTION : AddAlarmControlItems
* PURPOSE  : Add alarm control items to alarm set sub menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*  pEquipItem : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:09
*==========================================================================*/
static int AddAlarmControlItems(MENU_ITEM* pEquipItem)
{
	int nError = ERR_LCD_OK;

	//For clear history alarm  control
	static LANG_TEXT*		s_ClearHistoryAlarmTextArray[1];


	static SELF_DEFINE_INFO s_AlarmControlItemInfo[] = 
	{
		//Alarm voice
		DEF_SELF_DEFINE_INFO(ALARM_VOICE_CTRL_ID, 
			NULL,
			VAR_ENUM, 
			OPERATOR_LEVEL, 
			ALARM_VOICE_DIS_FORMAT, 
			1, 
			5,
			0,
			6,
			NULL,//Will set text array in GetAlarmVoiceCtrlPreInfo()
			NULL),

		//Keypad voice enabled
		//DEF_SELF_DEFINE_INFO(KEYPAD_VOICE_SET_ID,
		//	NULL,
		//	VAR_ENUM,
		//	OPERATOR_LEVEL,
		//	SYS_KEYPAD_VOICE_SET_ID_FORMAT,
		//	1,
		//	1,
		//	0,
		//	2,
		//	NULL,
		//	NULL),


		//outgoing alarm
		DEF_SELF_DEFINE_INFO(OUTGOING_ALARM_CTRL_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			OUT_GOING_ALARM_DIS_FORMAT, 
			1, 
			1,
			0,
			0,
			NULL,//Will set text array in GetOutgoingAlarmItemPreInfo()
			NULL),

		//clear history alarm
		DEF_SELF_DEFINE_INFO(CLEAR_HIS_ALARM_CTRL_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			CLEAR_HISTORY_ALARM_DIS_FORMAT, 
			0, 
			0,
			0,
			1,
			s_ClearHistoryAlarmTextArray,
			NULL),

	};


#define ALARM_CONTROL_ITEM_NUMBER \
	(sizeof(s_AlarmControlItemInfo)/sizeof(SELF_DEFINE_INFO))

	//alarm control item number
	int		nSigNum = ALARM_CONTROL_ITEM_NUMBER;

	SUB_MENU*	pSubMenu;
	MENU_ITEM*	pSignalItem;
	int			*pDispItems;

	char		cMenuType = MT_SELF_DEF_SET;

	int i;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pSignalItem = NEW(MENU_ITEM, nSigNum);
	LOGOUT_NO_MEMORY(pSignalItem);
	ZERO_POBJS(pSignalItem, nSigNum);

	pDispItems = NEW(int, nSigNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nSigNum);

	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_FIXED_LANG_ID(ALARM_CONTROL_LANG_ID),
		nSigNum,
		pSignalItem,
		pDispItems);

	for(i = 0; i < (int)ALARM_CONTROL_ITEM_NUMBER; i++)
	{
		int nAckFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE;

		if (s_AlarmControlItemInfo[i].iSigID == CLEAR_HIS_ALARM_CTRL_ID
			|| s_AlarmControlItemInfo[i].iSigID == OUTGOING_ALARM_CTRL_ID)
		{
			nAckFlag = MENU_ITEM_NEED_ACKNOWLEDGE;
		}

		//Add each alarm ctrl set item
		INIT_SIGNAL_ITEM(&(pSignalItem[i]), 
			cMenuType,
			LCD_MERGE_FIXED_LANG_ID(s_AlarmControlItemInfo[i].iSigID),
			MENU_ITEM_NEED_VALIDATE, 
			nAckFlag,
			&(s_AlarmControlItemInfo[i]));
	}

	INIT_SUBMENU_ITEM(pEquipItem, 
		LCD_MERGE_FIXED_LANG_ID(ALARM_CONTROL_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	return nError;
}





static int AddManAutoSetInCtrlMenu(MENU_ITEM* pParentMenuItem)
{
	ASSERT(pParentMenuItem);

	SUB_MENU		*pSubMenu;
	MENU_ITEM*		pMenuItems;
	int				*pDispItems;

	int				nMenuItemNum;

	nMenuItemNum = MAN_AUTO_SET_IN_CTRL_MENU;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItems = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	INIT_SUB_MENU(pSubMenu, 
		LCD_MERGE_FIXED_LANG_ID(MAN_AUTO_SET_LANG_ID),
		nMenuItemNum, pMenuItems, pDispItems);

	INIT_SUBMENU_ITEM(pParentMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(MAN_AUTO_SET_LANG_ID), 
		MENU_ITEM_NEED_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	


	//��ȡ���Զ������źŵ�ָ��
	int nError;
	int nBufLen;
	VOID *pSigInfoPointer;

	int	nEquipID, nSigType, nSigID;

	nEquipID = POWER_SYSTEM_EQUIP_ID;
	DXI_SPLIT_SIG_ID(SIG_ID_AUTO_MAN_STATE, nSigType, nSigID);

	nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
		POWER_SYSTEM_EQUIP_ID,			
		SIG_ID_AUTO_MAN_STATE,		
		&nBufLen,			
		&pSigInfoPointer,			
		0);

	INIT_SIGNAL_ITEM(pMenuItems, 
		MT_SIGNAL_SET,
		DXI_MERGE_UNIQUE_SIG_ID(POWER_SYSTEM_EQUIP_ID, SIG_TYPE_SETTING, nSigID),
		MENU_ITEM_NEED_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		nError == ERR_DXI_OK ? pSigInfoPointer : NULL);

	return ERR_LCD_OK;
}













/*==========================================================================*
* FUNCTION : CreateRunInfoMenu
* PURPOSE  : Create run info sub menu of main menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: SUB_MENU*  pSubMenu : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:07
*==========================================================================*/
static int CreateRunInfoMenu(MENU_ITEM* pRunInfoMenuItem)
{
	int				nEquipNum;
	int				nMenuItemNum;
	int				*pDispItems;

	MENU_ITEM*		pMenuItems;
	MENU_ITEM*		pEquipItem;

	SUB_MENU* pSubMenu;

	nEquipNum = g_stParseMenuNode.nDispClassEquipNum[SIG_TYPE_SAMPLING];

	nMenuItemNum = nEquipNum + ACTIVE_ALARM_MENU_NUM
		+ HISTORY_ALARM_MENU_NUM + BARCODE_MENU_NUM;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItems = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	//In order to assign the Father ID of items
	//��ʼ����������Ϣ���Ӳ˵�
	INIT_SUB_MENU(pSubMenu, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_RUNINFO_LANG_ID),
		nMenuItemNum, pMenuItems, pDispItems);

	//����������Ϣ���Ӳ˵����ӵ������˵����Ĳ˵���
	ASSERT(pRunInfoMenuItem);
	INIT_SUBMENU_ITEM(pRunInfoMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_RUNINFO_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);

	// 1. Active alarm
	//����ǰ�澯���Ӳ˵�
	pEquipItem	= pMenuItems;
	g_pActiveAlarmMenuItem = pEquipItem;
	AddActiveAlarmItems(pEquipItem);

	// 2. Equipment
	//���조�����źš��Ӳ˵��е��豸�˵���

	ConstructEquipSubMenu(pSubMenu, SIG_TYPE_SAMPLING);

	// 3. History alarm
	pEquipItem = pMenuItems + nEquipNum + 1;
	g_pHistoryAlarmMenuItem = pEquipItem;
	//History alarm will be added when enter into the view of history alarm
	
	INIT_SUBMENU_ITEM(pEquipItem, 
			LCD_MERGE_FIXED_LANG_ID(HISTORY_ALARM_DISP_LANG_ID), 
			MENU_ITEM_NEED_NOT_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			NULL);
		
	// 4. Barcode Menu item
#if BARCODE_MENU_NUM
	pEquipItem++;
	AddSiteInventoryItems(pEquipItem);
#endif

	return ERR_LCD_OK;
}


/*==========================================================================*
* FUNCTION : CreateSignalCtrlMenu
* PURPOSE  : Create signal control sub menu of main menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: SUB_MENU*  pSubMenu : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:07
*==========================================================================*/
static int CreateSignalCtrlMenu(MENU_ITEM* pSigCtrlMenuItem)
{
	int				nEquipNum;
	int				nMenuItemNum;
	int				*pDispItems;

	MENU_ITEM*		pMenuItems;

	SUB_MENU	*pSubMenu;

	nEquipNum = g_stParseMenuNode.nDispClassEquipNum[SIG_TYPE_CONTROL];

	nMenuItemNum = nEquipNum + MAN_AUTO_SET_IN_CTRL_MENU;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItems = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	INIT_SUB_MENU(pSubMenu, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_CONTROL_LANG_ID),
		nMenuItemNum, pMenuItems, pDispItems);

	ASSERT(pSigCtrlMenuItem);
	INIT_SUBMENU_ITEM(pSigCtrlMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_CONTROL_LANG_ID), 
		MENU_ITEM_NEED_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

#if MAN_AUTO_SET_IN_CTRL_MENU
	AddManAutoSetInCtrlMenu(pMenuItems);
#endif

	ConstructEquipSubMenu(pSubMenu, SIG_TYPE_CONTROL);

	return ERR_LCD_OK;
}



/*==========================================================================*
* FUNCTION : CreateSignalSetMenu
* PURPOSE  : Create signal set sub menu of main menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: SUB_MENU*  pSubMenu : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:07
*==========================================================================*/
static int CreateSignalSetMenu(MENU_ITEM* pSigSetMenuItem)
{
	int				nEquipNum;
	int				nMenuItemNum;
	int				*pDispItems;

	MENU_ITEM*		pMenuItems;
	MENU_ITEM*		pEquipItem;

	SUB_MENU*		pSubMenu;

	nEquipNum = g_stParseMenuNode.nDispClassEquipNum[SIG_TYPE_SETTING];

	nMenuItemNum = nEquipNum + ALARM_SET_MENU_NUM
		+ COMMUNICATION_SET_MENU_NUM + SYSTEM_PARA_SET_NUM;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItems = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	//In order to assign the Father ID of items
	INIT_SUB_MENU(pSubMenu, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_SET_LANG_ID),
		nMenuItemNum, pMenuItems, pDispItems);

	ASSERT(pSigSetMenuItem);
	INIT_SUBMENU_ITEM(pSigSetMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_SET_LANG_ID), 
		MENU_ITEM_NEED_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	pEquipItem	= pMenuItems;

	// 1. Alarm param set
#if ALARM_SET_MENU_NUM
//	pEquipItem++;
	AddAlarmSetItems(pEquipItem);
#endif

	// 2. Equip Setting
	ConstructEquipSubMenu(pSubMenu, SIG_TYPE_SETTING);


	pEquipItem	= pMenuItems + nEquipNum + ALARM_SET_MENU_NUM;

	// 4. Communication Set
#if COMMUNICATION_SET_MENU_NUM
	AddCommSetItems(pEquipItem);
	pEquipItem++;
#endif

	// 3. System param set
	AddSysSetItems(pEquipItem);

	return ERR_LCD_OK;
}



//Frank Wu,20160127, for MiniNCU
static int CreateSignalSetMenuMiniNCU(MENU_ITEM* pSigSetMenuItem)
{
	int				nMenuItemNum;
	int				*pDispItems;

	MENU_ITEM*		pMenuItems;
	MENU_ITEM*		pMenuItem;

	SUB_MENU*		pSubMenu;
	int				iCustomizeMenuNum = 0;
	USER_DEF_PAGES *pUserDefPages = NULL;
	int				i, nPageID;

	iCustomizeMenuNum = QueryCustomizeMenu(&pUserDefPages);

	nMenuItemNum = iCustomizeMenuNum + COMMUNICATION_SET_MENU_NUM + SYSTEM_PARA_SET_NUM;

	if(nMenuItemNum <= 0)
	{
		INIT_SUBMENU_ITEM(pSigSetMenuItem, 
			LCD_MERGE_FIXED_LANG_ID(MAIN_SET_LANG_ID), 
			MENU_ITEM_NEED_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			NULL);

		return ERR_LCD_OK;
	}

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItems = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	//In order to assign the Father ID of items
	INIT_SUB_MENU(pSubMenu, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_SET_LANG_ID),
		nMenuItemNum, pMenuItems, pDispItems);

	ASSERT(pSigSetMenuItem);
	INIT_SUBMENU_ITEM(pSigSetMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_SET_LANG_ID), 
		MENU_ITEM_NEED_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	pMenuItem	= pMenuItems;

	/* 1. Customize Menu */
	for(i = 0; i < iCustomizeMenuNum; i++, pMenuItem++)
	{
		nPageID = (pUserDefPages->pPageInfo->pPageName + i)->iPageID;
		CreateCustomSubMenu(pMenuItem, pUserDefPages, nPageID);
	}

#if COMMUNICATION_SET_MENU_NUM
	// 2. Communication Set
	AddCommSetItems(pMenuItem);
	pMenuItem++;
#endif

#if SYSTEM_PARA_SET_NUM
	// 3. System param set
	AddSysSetItems(pMenuItem);
	pMenuItem++;
#endif

	return ERR_LCD_OK;
}

static int CreateSlaveSignalSetMenuMiniNCU(MENU_ITEM* pParentMenuItem)
{
	int				nMenuItemNum;
	int				*pDispItems;

	MENU_ITEM*		pMenuItems;
	MENU_ITEM*		pMenuItem;

	SUB_MENU*		pSubMenu;
	int				i;

	SIG_ITEM		staSlaveSigList[] = {
		{1, 2, 180},//Rectifier Expansion
		{1, 2, 91},//Slave Address
	};
	SIG_ITEM		*pSigItem = NULL;
	
	int		nError;
	int		nEquipID, nSigType, nSigID;
	int		nVarID,	nVarSubID, nBufLen;
	void	*pSigInfoPointer;
	int		nTimeOut = 5000;

	nMenuItemNum = ITEM_OF(staSlaveSigList);

	//��ʼ�����Ӳ˵�
	if(nMenuItemNum > 0)
	{
		pSubMenu = NEW(SUB_MENU, 1);
		LOGOUT_NO_MEMORY(pSubMenu);
		ZERO_POBJS(pSubMenu, 1);

		pMenuItems = NEW(MENU_ITEM, nMenuItemNum);
		LOGOUT_NO_MEMORY(pMenuItems);
		ZERO_POBJS(pMenuItems, nMenuItemNum);

		pDispItems = NEW(int, nMenuItemNum);
		LOGOUT_NO_MEMORY(pDispItems);
		ZERO_POBJS(pDispItems, nMenuItemNum);

		INIT_SUB_MENU(pSubMenu, 
			LCD_MERGE_ACTUAL_EQUIP_ID(0, MAIN_SLAVE_SET_LANG_ID),
			nMenuItemNum, pMenuItems, pDispItems);

		INIT_SUBMENU_ITEM(pParentMenuItem, 
			LCD_MERGE_ACTUAL_EQUIP_ID(0, MAIN_SLAVE_SET_LANG_ID),
			MENU_ITEM_NEED_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			pSubMenu);

		//���ź�����뵽���Ӳ˵���
		pSigItem = staSlaveSigList;
		for(i = 0; i < nMenuItemNum; i++, pMenuItems++, pSigItem++)
		{
			nEquipID = pSigItem->iEquipID;
			nSigType = pSigItem->iSigType;
			nSigID = pSigItem->iSigID;

			nVarID = nEquipID;
			nVarSubID = DXI_MERGE_SIG_ID(nSigType, nSigID);

			nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
				nVarID,			
				nVarSubID,		
				&nBufLen,			
				&pSigInfoPointer,			
				nTimeOut);

			if(SIG_TYPE_CONTROL == nSigType)
			{
				INIT_SIGNAL_ITEM(pMenuItems, 
					MT_SIGNAL_CUSTOMIZE,
					DXI_MERGE_UNIQUE_SIG_ID(nEquipID, nSigType, nSigID),
					MENU_ITEM_NEED_VALIDATE, 
					MENU_ITEM_NEED_ACKNOWLEDGE,
					nError == ERR_DXI_OK ? pSigInfoPointer : NULL);
			}
			else
			{
				INIT_SIGNAL_ITEM(pMenuItems, 
					MT_SIGNAL_CUSTOMIZE,
					DXI_MERGE_UNIQUE_SIG_ID(nEquipID, nSigType, nSigID),
					MENU_ITEM_NEED_VALIDATE, 
					MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
					nError == ERR_DXI_OK ? pSigInfoPointer : NULL);
			}
		}
	}

	return ERR_LCD_OK;
}



static int CreateEnergySavingMenu(MENU_ITEM* pParentMenuItem)
{
	ASSERT(pParentMenuItem);
	INIT_SUBMENU_ITEM(pParentMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_ENERGY_SAVING_LANG_ID),
		MENU_ITEM_NEED_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		NULL);

	//Menu Items are added in CreateDividedMenu()

	return ERR_LCD_OK;
}


static int CreateCustomSubMenu(MENU_ITEM* pParentMenuItem, 
							   USER_DEF_PAGES *pUserDefPages,
							   int nPageID)
{
	ASSERT(pParentMenuItem);


	int		nError;
	int		nEquipID, nSigType, nSigID;
	int		nVarID,	nVarSubID, nBufLen;
	void	*pSigInfoPointer;
	int		nTimeOut = 5000;

	SUB_MENU*		pSubMenu;
	int				*pDispItems;
	MENU_ITEM		*pMenuItems;

	int		iSigItemNum;
	iSigItemNum = pUserDefPages->pSigItemInfo->iSigItemNum;
	if(iSigItemNum <= 0)
	{
		INIT_SUBMENU_ITEM(pParentMenuItem, 
			LCD_MERGE_CUSTOMIZE_MENU_ID(nPageID),
			MENU_ITEM_NEED_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			NULL);
		return ERR_LCD_FAILURE;
	}

	int		*piSigItem;
	piSigItem = NEW(int, iSigItemNum);
	LOGOUT_NO_MEMORY(piSigItem);
	ZERO_POBJS(piSigItem, iSigItemNum);
	//memset(piSigItem, 0xFF, nMenuItemNum * sizeof(int));	//ȫ����Ϊ-1

	//�ҵ���Page�µ�����Item�����浽piSigItem��ָ����ڴ���
	int		i;
	int		nMenuItemNum = 0;
	for(i = 0; i < iSigItemNum; i++)
	{
		if((pUserDefPages->pSigItemInfo->pSigItem + i)->iPageID == nPageID)
		{
			*(piSigItem + nMenuItemNum) = i;
			nMenuItemNum++;
		}
	}

	//��ʼ�����Ӳ˵�
	if(nMenuItemNum > 0)
	{
		pSubMenu = NEW(SUB_MENU, 1);
		LOGOUT_NO_MEMORY(pSubMenu);
		ZERO_POBJS(pSubMenu, 1);

		pMenuItems = NEW(MENU_ITEM, nMenuItemNum);
		LOGOUT_NO_MEMORY(pMenuItems);
		ZERO_POBJS(pMenuItems, nMenuItemNum);

		pDispItems = NEW(int, nMenuItemNum);
		LOGOUT_NO_MEMORY(pDispItems);
		ZERO_POBJS(pDispItems, nMenuItemNum);

		INIT_SUB_MENU(pSubMenu, 
			LCD_MERGE_CUSTOMIZE_MENU_ID(nPageID),
			nMenuItemNum, pMenuItems, pDispItems);

		INIT_SUBMENU_ITEM(pParentMenuItem, 
			LCD_MERGE_CUSTOMIZE_MENU_ID(nPageID),
			MENU_ITEM_NEED_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			pSubMenu);

		//���ź�����뵽���Ӳ˵���
		for(i = 0; (i < nMenuItemNum) && (*(piSigItem + i) >= 0); i++, pMenuItems++)
		{
			nEquipID = (pUserDefPages->pSigItemInfo->pSigItem + *(piSigItem + i))->iEquipID;
			nSigType = (pUserDefPages->pSigItemInfo->pSigItem + *(piSigItem + i))->iSigType;
			nSigID = (pUserDefPages->pSigItemInfo->pSigItem + *(piSigItem + i))->iSigID;

			nVarID = nEquipID;
			nVarSubID = DXI_MERGE_SIG_ID(nSigType, nSigID);

			nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
				nVarID,			
				nVarSubID,		
				&nBufLen,			
				&pSigInfoPointer,			
				nTimeOut);

			if(SIG_TYPE_CONTROL == nSigType)
			{
				INIT_SIGNAL_ITEM(pMenuItems, 
					MT_SIGNAL_CUSTOMIZE,
					DXI_MERGE_UNIQUE_SIG_ID(nEquipID, nSigType, nSigID),
					MENU_ITEM_NEED_VALIDATE, 
					MENU_ITEM_NEED_ACKNOWLEDGE,
					nError == ERR_DXI_OK ? pSigInfoPointer : NULL);
			}
			else
			{
				INIT_SIGNAL_ITEM(pMenuItems, 
					MT_SIGNAL_CUSTOMIZE,
					DXI_MERGE_UNIQUE_SIG_ID(nEquipID, nSigType, nSigID),
					MENU_ITEM_NEED_VALIDATE, 
					MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
					nError == ERR_DXI_OK ? pSigInfoPointer : NULL);
			}
		}


	}
	else
	{
		INIT_SUBMENU_ITEM(pParentMenuItem, 
			LCD_MERGE_CUSTOMIZE_MENU_ID(nPageID),
			MENU_ITEM_NEED_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			NULL);
		return ERR_LCD_FAILURE;

	}

	DELETE_ITEM(piSigItem);
	return ERR_LCD_OK;

}

static int QueryCustomizeMenu(void** pCustomizeMenuItem)
{
	USER_DEF_PAGES *pUserDefPages;

	int				nInterfaceType = VAR_USER_DEF_PAGES;		
	int				nVarID = 0;
	int				nVarSubID = 0;
	int				nBufLen;
	int				nTimeOut = 0;
	int				nError = ERR_DXI_OK;
	int				iCustomizeMenuNum = 0;

	*pCustomizeMenuItem = NULL;

	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pUserDefPages,			
		nTimeOut);
	if(nError == ERR_DXI_OK)
	{
		iCustomizeMenuNum = pUserDefPages->pPageInfo->iPageInfoNum;
		*pCustomizeMenuItem = pUserDefPages;
	}


	return iCustomizeMenuNum;
}


static int CreateCustomizeMenu(MENU_ITEM* pCustomizeMenuItem)
{
	ASSERT(pCustomizeMenuItem);

	USER_DEF_PAGES *pUserDefPages;

	int				nInterfaceType = VAR_USER_DEF_PAGES;		
	int				nVarID = 0;
	int				nVarSubID = 0;
	int				nBufLen;
	int				nTimeOut = 0;
	int				nError = ERR_DXI_OK;

	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pUserDefPages,			
		nTimeOut);
	if(nError != ERR_DXI_OK)
	{
		INIT_SUBMENU_ITEM(pCustomizeMenuItem, 
			LCD_MERGE_FIXED_LANG_ID(MAIN_CUSTOMIZE_LANG_ID), 
			MENU_ITEM_NEED_NOT_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			NULL);

		return ERR_LCD_FAILURE;
	}

	int				nMenuItemNum;
	int				*pDispItems;

	MENU_ITEM*		pMenuItems;

	SUB_MENU*		pSubMenu;

	nMenuItemNum = pUserDefPages->pPageInfo->iPageInfoNum;
	if(nMenuItemNum <= 0)
	{
		INIT_SUBMENU_ITEM(pCustomizeMenuItem, 
			LCD_MERGE_FIXED_LANG_ID(MAIN_CUSTOMIZE_LANG_ID), 
			MENU_ITEM_NEED_NOT_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			NULL);

		return ERR_LCD_FAILURE;
	}

	if(nMenuItemNum == 1)
	{
		CreateCustomSubMenu(pCustomizeMenuItem,
			pUserDefPages,
			pUserDefPages->pPageInfo->pPageName->iPageID);

		return ERR_LCD_OK;
	}

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItems = NEW(MENU_ITEM, nMenuItemNum);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, nMenuItemNum);

	pDispItems = NEW(int, nMenuItemNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItemNum);

	INIT_SUB_MENU(pSubMenu, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_CUSTOMIZE_LANG_ID),
		nMenuItemNum, pMenuItems, pDispItems);

	INIT_SUBMENU_ITEM(pCustomizeMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(MAIN_CUSTOMIZE_LANG_ID), 
		MENU_ITEM_NEED_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	int i, nPageID;

	for(i = 0; i < nMenuItemNum; i++, pMenuItems++)
	{
		nPageID = (pUserDefPages->pPageInfo->pPageName + i)->iPageID;
		CreateCustomSubMenu(pMenuItems, pUserDefPages, nPageID);
	}

	return ERR_LCD_OK;
}



#define		DIVIDED_SUB_MENU_SPLITTER	','

static DIVIDED_SUB_MENU_ITEM_CFG* FindDividedSubMenuItem(LCD_PRIVATE_CFG_LOADER *pLcdPrivateCfgLoader, int nMenuItemIndex)
{
	ASSERT(pLcdPrivateCfgLoader);

	DIVIDED_SUB_MENU_ITEM_CFG	*pSubMenuItem;
	int j;

	for(j = 0; j < pLcdPrivateCfgLoader->stDividedSubMenuItemCfgReader.nDividedSubMenuItemNum; j++)
	{
		pSubMenuItem = pLcdPrivateCfgLoader->stDividedSubMenuItemCfgReader.pDividedSubMenuItemCfg + j;
		if(pSubMenuItem->nSubMenuItemIndex == nMenuItemIndex)
		{
			return pSubMenuItem;
		}
	}

	return NULL;
}

static int CheckDividedSubMenuCfg(LCD_PRIVATE_CFG_LOADER *pLcdPrivateCfgLoader)
{
	ASSERT(pLcdPrivateCfgLoader);

	int		i;
	int		nCfgItemNum;

	int			nBufLen;
	EQUIP_INFO	*pEquipInfo;
	VOID		*pSigStru;

	char		*pMenuItemIndex;
	char		caMenuItemIndex[DIVIDED_SUB_MENU_CHARS_MAX];
	char		*pSigID;
	char		caSigID[DIVIDED_SUB_MENU_CHARS_MAX];

	char		*pBuf;

	int			nMenuItemIndex;
	int			nSigID;

	LANG_FILE* pLCDLangFile = GetLCDLangFile();
	LANG_TEXT* pLangText;

	DIVIDED_SUB_MENU_ITEM_CFG	*pSubMenuItem;
	DIVIDED_SUB_MENU_INFO_CFG	*pSubMenuInfo;

	// 1. ������õĲ˵���
	nCfgItemNum = pLcdPrivateCfgLoader->stDividedSubMenuItemCfgReader.nDividedSubMenuItemNum;
	for(i = 0; i < nCfgItemNum;	i++)
	{
		//���pSubMenuInfo�Ƿ����
		pSubMenuItem = pLcdPrivateCfgLoader->stDividedSubMenuItemCfgReader.pDividedSubMenuItemCfg + i;
		if(!pSubMenuItem)
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("pSubMenuItem NOT Exist!");
			return ERR_LCD_DIVIDED_SUB_MENU_CFG_OTHER;
		}

		//�����ԴID
		pLangText = GetLCDLangText(
			pSubMenuItem->nResouceID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if(!pLangText)
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("pLangText NOT Exist!");
			DEBUG_LCD_FILE_FUN_LINE_INT("nLangResouceID", pSubMenuItem->nResouceID);
			return ERR_LCD_DIVIDED_SUB_MENU_CFG_RESOUCE_ID;
		}

		//����豸ID
		pEquipInfo = LCD_GetEquipInfo(pSubMenuItem->nEquipID);
		if(pEquipInfo == NULL)
		//if(ERR_DXI_OK != DxiGetData(VAR_A_EQUIP_INFO,
		//	pSubMenuItem->nEquipID,			
		//	0,		
		//	&nBufLen,			
		//	&pEquipInfo,			
		//	0))
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("pLangText NOT Exist!");
			DEBUG_LCD_FILE_FUN_LINE_INT("nEquipID", pSubMenuItem->nEquipID);
			return ERR_LCD_DIVIDED_SUB_MENU_CFG_EQUIP_ID;
		}

		//����ź�����
		if((pSubMenuItem->nSigType < SIG_TYPE_SAMPLING)
			|| (pSubMenuItem->nSigType >= SIG_TYPE_MAX))
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("nSigType NOT Exist!");
			DEBUG_LCD_FILE_FUN_LINE_INT("nSigType", pSubMenuItem->nSigType);
			return ERR_LCD_DIVIDED_SUB_MENU_CFG_SIG_TYPE;
		}

		//����ź�ID

		//��ΪCfg_SplitStringEx()���SPLITTER����NULL������Ҫ�ȿ���һ��
		strncpy(caSigID, pSubMenuItem->caSigID, DIVIDED_SUB_MENU_CHARS_MAX);
		pSigID = caSigID;
		pBuf = NULL;
		while((pSigID = Cfg_SplitStringEx(pSigID, &pBuf, DIVIDED_SUB_MENU_SPLITTER))
			&& (pSigID != pBuf))
		{
			nSigID = atoi(pBuf);
			if(nSigID != -1)
			{
				if(ERR_DXI_OK != DxiGetData(VAR_A_SIGNAL_INFO_STRU,
					pSubMenuItem->nEquipID,	
					DXI_MERGE_SIG_ID(pSubMenuItem->nSigType, nSigID),
					&nBufLen,
					&pSigStru,
					0))
				{
					DEBUG_LCD_FILE_FUN_LINE_STRING("Signal NOT Exist!");
					DEBUG_LCD_FILE_FUN_LINE_INT("nEquipID", pSubMenuItem->nEquipID);
					DEBUG_LCD_FILE_FUN_LINE_INT("nSigType", pSubMenuItem->nSigType);
					DEBUG_LCD_FILE_FUN_LINE_INT("nSigID", nSigID);
					return ERR_LCD_DIVIDED_SUB_MENU_CFG_SIGNAL_ID;
				}
			}
		}
	}


	// 2. ����Ӳ˵��е��豸ID���˵���ID�Ƿ����
	nCfgItemNum = pLcdPrivateCfgLoader->stDividedSubMenuInfoCfgReader.nDividedSubMenuInfoNum;

	for(i = 0; i < nCfgItemNum;	i++)
	{
		//���pSubMenuInfo�Ƿ����
		pSubMenuInfo = pLcdPrivateCfgLoader->stDividedSubMenuInfoCfgReader.pDividedSubMenuInfoCfg + i;
		if(!pSubMenuInfo)
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("pSubMenuInfo NOT Exist!");
			return ERR_LCD_DIVIDED_SUB_MENU_CFG_OTHER;
		}

		if(pSubMenuInfo->nPositionFlag == DIVIDED_MENU_UNDER_FIX)
		{
			//�����ԴID
			pLangText = GetLCDLangText(
				pSubMenuInfo->nEquipID, 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);
			if(!pLangText)
			{
				DEBUG_LCD_FILE_FUN_LINE_STRING("pLangText NOT Exist!");
				DEBUG_LCD_FILE_FUN_LINE_INT("nLangResouceID", pSubMenuItem->nResouceID);
				return ERR_LCD_DIVIDED_SUB_MENU_CFG_RESOUCE_ID;
			}
		}
		else
		{
			//����豸ID
			pEquipInfo = LCD_GetEquipInfo(pSubMenuInfo->nEquipID);
			if(pEquipInfo == NULL)
			//if(ERR_DXI_OK != DxiGetData(VAR_A_EQUIP_INFO,
			//	pSubMenuInfo->nEquipID,			
			//	0,		
			//	&nBufLen,			
			//	&pEquipInfo,			
			//	0))
			{
				DEBUG_LCD_FILE_FUN_LINE_STRING("pLangText NOT Exist!");
				DEBUG_LCD_FILE_FUN_LINE_INT("nEquipID", pSubMenuItem->nEquipID);
				return ERR_LCD_DIVIDED_SUB_MENU_CFG_EQUIP_ID;
			}

			//����ź�����
			if((pSubMenuInfo->nSigType < SIG_TYPE_SAMPLING)
				|| (pSubMenuInfo->nSigType >= SIG_TYPE_MAX))
			{
				DEBUG_LCD_FILE_FUN_LINE_STRING("nSigType NOT Exist!");
				DEBUG_LCD_FILE_FUN_LINE_INT("nSigType", pSubMenuItem->nSigType);
				return ERR_LCD_DIVIDED_SUB_MENU_CFG_SIG_TYPE;
			}
		}

		//������õĲ˵����Ƿ����

		strncpy(caMenuItemIndex, pSubMenuInfo->caMenuItemIndex, DIVIDED_SUB_MENU_CHARS_MAX);
		pMenuItemIndex = caMenuItemIndex;
		pBuf = NULL;
		while((pMenuItemIndex = Cfg_SplitStringEx(pMenuItemIndex, &pBuf, DIVIDED_SUB_MENU_SPLITTER))
			&& (pMenuItemIndex != pBuf))
		{
			nMenuItemIndex = atoi(pBuf);

			if(!FindDividedSubMenuItem(pLcdPrivateCfgLoader, nMenuItemIndex))
			{
				DEBUG_LCD_FILE_FUN_LINE_STRING("DIVIDED_SUB_MENU_ITEM NOT Exist!");
				DEBUG_LCD_FILE_FUN_LINE_INT("nMenuItemIndex", nMenuItemIndex);
				return ERR_LCD_DIVIDED_SUB_MENU_CFG_MENU_ITEM;
			}
		}
	}

	return ERR_LCD_DIVIDED_SUB_MENU_CFG_OK;
}


static BOOL SignalHasUsedInDividedSubMenuItem(LCD_PRIVATE_CFG_LOADER *pLcdPrivateCfgLoader,int nEquipID, int nSigType, int nSigID)
{
	ASSERT(pLcdPrivateCfgLoader);
	ASSERT(nSigID > 0);

	int i;
	char	caCfgIDs[DIVIDED_SUB_MENU_CHARS_MAX + 2];

	char	caSigID[DIVIDED_SUB_MENU_CHARS_MAX];
	snprintf(caSigID, sizeof(caSigID), ",%d,", nSigID);

	int	nCfgMenuItem;
	nCfgMenuItem = pLcdPrivateCfgLoader->stDividedSubMenuItemCfgReader.nDividedSubMenuItemNum;

	DIVIDED_SUB_MENU_ITEM_CFG *pCfgMenuItem;
	pCfgMenuItem = pLcdPrivateCfgLoader->stDividedSubMenuItemCfgReader.pDividedSubMenuItemCfg;

	for(i = 0;
		(i < nCfgMenuItem) && pCfgMenuItem;
		i++, pCfgMenuItem++)
	{
		if((pCfgMenuItem->nEquipID == nEquipID)
			&& (pCfgMenuItem->nSigType == nSigType))
		{
			snprintf(caCfgIDs, sizeof(caCfgIDs), ",%s,", pCfgMenuItem->caSigID);

			if(strstr(caCfgIDs, caSigID))
			{
				return TRUE;
			}
		}

	}

	return FALSE;

}

static int CompleteDividedSubMenuItemInfo(LCD_PRIVATE_CFG_LOADER *pLcdPrivateCfgLoader,
										  DIVIDED_SUB_MENU_INFO_CFG *pCfgMenuInfo)
{
	ASSERT(pLcdPrivateCfgLoader);
	ASSERT(pCfgMenuInfo);


	int		nItemID;
	char	*pBuf;
	char	*pItemID;
	char	caItemID[DIVIDED_SUB_MENU_CHARS_MAX];

	char	caOtherID[DIVIDED_SUB_MENU_CHARS_MAX];

	DIVIDED_SUB_MENU_ITEM_CFG *pCfgMenuItem;


	PARSE_EQUIP_INFO	*pParseEquipInfo = &g_stParseEquipInfo;
	STANDARD_EQUIP_INFO	*pStdEquipInfo;

	int		i;
	int		nBufLen;
	EQUIP_INFO			*pEquipInfo;
	int		nEquipID;
	int		nEquipType;

	SAMPLE_SIG_INFO		*pSmpSigInfo;
	CTRL_SIG_INFO		*pCtrlSigInfo;
	SET_SIG_INFO		*pSetSigInfo;
	ALARM_SIG_INFO		*pAlarmSigInfo;

	strncpy(caItemID, pCfgMenuInfo->caMenuItemIndex, DIVIDED_SUB_MENU_CHARS_MAX);
	pItemID = caItemID;
	pBuf = NULL;

	while((pItemID = Cfg_SplitStringEx(pItemID, &pBuf, DIVIDED_SUB_MENU_SPLITTER))
		&& (pItemID != pBuf))
	{
		nItemID = atoi(pBuf);

		pCfgMenuItem = FindDividedSubMenuItem(pLcdPrivateCfgLoader, nItemID);
		ASSERT(pCfgMenuItem);

		if(strcmp(pCfgMenuItem->caSigID, "-1") == 0)	//��ȣ�����Ҫ��ȫ
		{
			pCfgMenuItem->caSigID[0] = NULL;	//�����

			nEquipID = pCfgMenuItem->nEquipID;

			pEquipInfo = LCD_GetEquipInfo(nEquipID);
			//DxiGetData(VAR_A_EQUIP_INFO,			
			//	nEquipID,	
			//	0,
			//	&nBufLen,			
			//	&pEquipInfo,			
			//	0);

			nEquipType = pEquipInfo->iEquipTypeID;

			//�ӽ������ı�׼�豸��Ϣ��ȡ�ź�
			pStdEquipInfo = pParseEquipInfo->pStdEquipInfo;
			for(i = 0; 
				(i < pParseEquipInfo->nStdEquipNum) && pStdEquipInfo;
				i++, pStdEquipInfo++)
			{
				if(pStdEquipInfo->nEquipType == nEquipType)
				{
					break;
				}
			}
			ASSERT(i < pParseEquipInfo->nStdEquipNum);

			switch(pCfgMenuItem->nSigType)
			{
			case SIG_TYPE_SAMPLING:
				for(i = 0; i < pStdEquipInfo->stSignalInfo.iSampleSigNum; i++)
				{
					pSmpSigInfo = pStdEquipInfo->stSignalInfo.ppSampleSigInfo[i];
					if(!SignalHasUsedInDividedSubMenuItem(pLcdPrivateCfgLoader,
						pCfgMenuItem->nEquipID, pCfgMenuItem->nSigType, pSmpSigInfo->iSigID))
					{
						snprintf(caOtherID, sizeof(caOtherID), "%d,", pSmpSigInfo->iSigID);
						strcat(pCfgMenuItem->caSigID, caOtherID);
					}
				}
				break;

			case SIG_TYPE_CONTROL:
				for(i = 0; i < pStdEquipInfo->stSignalInfo.iCtrlSigNum; i++)
				{
					pCtrlSigInfo = pStdEquipInfo->stSignalInfo.ppCtrlSigInfo[i];
					if(!SignalHasUsedInDividedSubMenuItem(pLcdPrivateCfgLoader,
						pCfgMenuItem->nEquipID, pCfgMenuItem->nSigType, pCtrlSigInfo->iSigID))
					{
						snprintf(caOtherID, sizeof(caOtherID), "%d,", pCtrlSigInfo->iSigID);
						strcat(pCfgMenuItem->caSigID, caOtherID);
					}
				}
				break;

			case SIG_TYPE_SETTING:
				for(i = 0; i < pStdEquipInfo->stSignalInfo.iSetSigNum; i++)
				{
					pSetSigInfo = pStdEquipInfo->stSignalInfo.ppSetSigInfo[i];
					if(!SignalHasUsedInDividedSubMenuItem(pLcdPrivateCfgLoader,
						pCfgMenuItem->nEquipID, pCfgMenuItem->nSigType, pSetSigInfo->iSigID))
					{
						snprintf(caOtherID, sizeof(caOtherID), "%d,", pSetSigInfo->iSigID);
						strcat(pCfgMenuItem->caSigID, caOtherID);
					}
				}
				break;

			case SIG_TYPE_ALARM:
				for(i = 0; i < pStdEquipInfo->stSignalInfo.iAlarmSigNum; i++)
				{
					pAlarmSigInfo = pStdEquipInfo->stSignalInfo.ppAlarmSigInfo[i];
					if(!SignalHasUsedInDividedSubMenuItem(pLcdPrivateCfgLoader,
						pCfgMenuItem->nEquipID, pCfgMenuItem->nSigType, pAlarmSigInfo->iSigID))
					{
						snprintf(caOtherID, sizeof(caOtherID), "%d,", pAlarmSigInfo->iSigID);
						strcat(pCfgMenuItem->caSigID, caOtherID);
					}
				}
				break;

			default:
				DEBUG_LCD_FILE_FUN_LINE_STRING("Unkonw SIG_TYPE.");
				break;
			}

			//�����һ��','ȥ��
			if(pCfgMenuItem->caSigID[strlen(pCfgMenuItem->caSigID) - 1] == ',')
			{
				pCfgMenuItem->caSigID[strlen(pCfgMenuItem->caSigID) - 1] = NULL;
			}

			break;	//�Ѿ��ҵ�һ��"-1"�ˣ��˳�

		}

	}

	return ERR_LCD_OK;
}



static int CreateDividedSubMenuItem(MENU_ITEM *pParentMenuItem, DIVIDED_SUB_MENU_ITEM_CFG *pCfgMenuItem)
{
	SUB_MENU	*pNewSubMenu;
	MENU_ITEM	*pNewMenuItem;
	int			nMenuItem;
	int			*pDispItems;

	nMenuItem = Cfg_GetSplitStringCount(pCfgMenuItem->caSigID, DIVIDED_SUB_MENU_SPLITTER);

	pNewSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pNewSubMenu);
	ZERO_POBJS(pNewSubMenu, 1);

	pNewMenuItem = NEW(MENU_ITEM, nMenuItem);
	LOGOUT_NO_MEMORY(pNewMenuItem);
	ZERO_POBJS(pNewMenuItem, nMenuItem);

	pDispItems = NEW(int, nMenuItem);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nMenuItem);

	INIT_SUB_MENU(pNewSubMenu,
		LCD_MERGE_DIVIDED_SUB_MENU_ID(pCfgMenuItem->nResouceID), 
		nMenuItem,
		pNewMenuItem,
		pDispItems);

	INIT_SUBMENU_ITEM(pParentMenuItem, 
		LCD_MERGE_DIVIDED_SUB_MENU_ID(pCfgMenuItem->nResouceID), 
		MENU_ITEM_NEED_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pNewSubMenu);	

	int		nSigID;
	char	*pBuf;
	char	*pSigID;
	int		nBufLen;
	VOID	*pSigStru;

	int		cMenuType;
	int		cValidateFlag;
	int		cAcknowlegeFlag;
	switch(pCfgMenuItem->nSigType)
	{
	case SIG_TYPE_SAMPLING:
		cMenuType = MT_SIGNAL_DISPLAY;
		cValidateFlag	= MENU_ITEM_NEED_NOT_VALIDATE;
		cAcknowlegeFlag	= MENU_ITEM_NEED_NOT_ACKNOWLEDGE;
		break;

	case SIG_TYPE_CONTROL:
		cMenuType = MT_SIGNAL_CONTROL;
		cValidateFlag	= MENU_ITEM_NEED_VALIDATE;
		cAcknowlegeFlag	= MENU_ITEM_NEED_ACKNOWLEDGE;
		break;

	case SIG_TYPE_SETTING:
		cMenuType = MT_SIGNAL_SET;
		cValidateFlag	= MENU_ITEM_NEED_VALIDATE;
		cAcknowlegeFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE;
		break;

	case SIG_TYPE_ALARM:
		cMenuType = MT_ALARM_LEVEL_SET;
		cValidateFlag	= MENU_ITEM_NEED_VALIDATE;
		cAcknowlegeFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE;
		break;

	default:
		DEBUG_LCD_FILE_FUN_LINE_STRING("Unknow SigType");
		return ERR_LCD_FAILURE;
	}


	int		i = 0;
	pSigID = pCfgMenuItem->caSigID;
	pBuf = NULL;
	while((pSigID = Cfg_SplitStringEx(pSigID, &pBuf, DIVIDED_SUB_MENU_SPLITTER))
		&& (pSigID != pBuf))
	{
		nSigID = atoi(pBuf);
		ASSERT(nSigID > 0);		//��ʱ�ź�ID����Ϊ"-1"

		DxiGetData(VAR_A_SIGNAL_INFO_STRU,
			pCfgMenuItem->nEquipID,	
			DXI_MERGE_SIG_ID(pCfgMenuItem->nSigType, nSigID),
			&nBufLen,
			&pSigStru,
			0);

		INIT_SIGNAL_ITEM(pNewMenuItem + i, 
			cMenuType,
			DXI_MERGE_UNIQUE_SIG_ID(pCfgMenuItem->nEquipID, pCfgMenuItem->nSigType, nSigID),
			cValidateFlag, 
			cAcknowlegeFlag,
			pSigStru);

		i++;

	}
	ASSERT(nMenuItem == i);

	return ERR_LCD_OK;
}


static int CreateDividedMenu(SUB_MENU *pRootSubMenu, LCD_PRIVATE_CFG_LOADER *pLcdPrivateCfgLoader)
{
	ASSERT(pRootSubMenu);
	ASSERT(pLcdPrivateCfgLoader);

	int	i, j;
	int nCfgInfoNum;
	int	nMenuItemIndex;

	char	*pBuf;
	char	*pMenuItemIndex;

	DIVIDED_SUB_MENU_INFO_CFG	*pCfgSubMenuInfo;
	DIVIDED_SUB_MENU_ITEM_CFG	*pCfgSubMenuItem;

	MENU_ITEM	*pModifiedMenuItem;
	int			nModifiedMenuItemID;
	SUB_MENU	*pOldSubMenu;

	SUB_MENU	*pNewSubMenu;
	MENU_ITEM	*pNewMenuItem;
	int			nMenuItem;
	int			*pDispItems;

	int			nPosiFlag;

	nCfgInfoNum = pLcdPrivateCfgLoader->stDividedSubMenuInfoCfgReader.nDividedSubMenuInfoNum;
	pCfgSubMenuInfo = pLcdPrivateCfgLoader->stDividedSubMenuInfoCfgReader.pDividedSubMenuInfoCfg;

	for(i = 0; 
		(i < nCfgInfoNum) && pCfgSubMenuInfo;
		i++, pCfgSubMenuInfo++)
	{
		//���ź�IDΪ"-1"�Ĳ�ȫ
		CompleteDividedSubMenuItemInfo(pLcdPrivateCfgLoader, pCfgSubMenuInfo);

		nPosiFlag = pCfgSubMenuInfo->nPositionFlag;
		if((nPosiFlag == DIVIDED_MENU_CHILDREN)
			|| (nPosiFlag == DIVIDED_MENU_UNDER_FIX))
		{
			if(nPosiFlag == DIVIDED_MENU_CHILDREN)
			{
				nModifiedMenuItemID = LCD_MERGE_ACTUAL_EQUIP_ID(pCfgSubMenuInfo->nSigType, pCfgSubMenuInfo->nEquipID);
			}
			else	//DIVIDED_MENU_UNDER_FIX
			{
				nModifiedMenuItemID = LCD_MERGE_FIXED_LANG_ID(pCfgSubMenuInfo->nEquipID);
			}

			pModifiedMenuItem = FindMenuItemByID(pRootSubMenu, nModifiedMenuItemID);
			if(pModifiedMenuItem)
			{
				nMenuItem = Cfg_GetSplitStringCount(pCfgSubMenuInfo->caMenuItemIndex, DIVIDED_SUB_MENU_SPLITTER);

				if(nMenuItem == 1)
				{
					nMenuItemIndex = atoi(pCfgSubMenuInfo->caMenuItemIndex);

					pCfgSubMenuItem = FindDividedSubMenuItem(pLcdPrivateCfgLoader, nMenuItemIndex);
					ASSERT(pCfgSubMenuItem);

					pOldSubMenu = (SUB_MENU*)(pModifiedMenuItem->pvItemData);
					if(pOldSubMenu)
					{
						DelMenuRecursively(pOldSubMenu);
					}

					CreateDividedSubMenuItem(pModifiedMenuItem, pCfgSubMenuItem);
				}
				else
				{
					pNewSubMenu = NEW(SUB_MENU, 1);
					LOGOUT_NO_MEMORY(pNewSubMenu);
					ZERO_POBJS(pNewSubMenu, 1);

					pNewMenuItem = NEW(MENU_ITEM, nMenuItem);
					LOGOUT_NO_MEMORY(pNewMenuItem);
					ZERO_POBJS(pNewMenuItem, nMenuItem);

					pDispItems = NEW(int, nMenuItem);
					LOGOUT_NO_MEMORY(pDispItems);
					ZERO_POBJS(pDispItems, nMenuItem);

					INIT_SUB_MENU(pNewSubMenu,
						pModifiedMenuItem->nMenuItemID, 
						nMenuItem,
						pNewMenuItem,
						pDispItems);

					//�����µ��Ӳ˵���
					j = 0;
					pMenuItemIndex = pCfgSubMenuInfo->caMenuItemIndex;
					pBuf = NULL;
					while((pMenuItemIndex = Cfg_SplitStringEx(pMenuItemIndex, &pBuf, DIVIDED_SUB_MENU_SPLITTER))
						&& (pMenuItemIndex != pBuf))
					{
						nMenuItemIndex = atoi(pBuf);

						pCfgSubMenuItem = FindDividedSubMenuItem(pLcdPrivateCfgLoader, nMenuItemIndex);
						ASSERT(pCfgSubMenuItem);

						CreateDividedSubMenuItem(pNewMenuItem + j++, pCfgSubMenuItem);

					}
					ASSERT(nMenuItem == j);

					//�Ѿɵ�ɾ�������µ���������
					pOldSubMenu = (SUB_MENU*)(pModifiedMenuItem->pvItemData);

					if(pOldSubMenu)
					{
						DelMenuRecursively(pOldSubMenu);
					}

					pModifiedMenuItem->pvItemData = (VOID*)pNewSubMenu;

				}

			}
			else
			{
				DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT find Menu Item.");
			}
		}
		else if(nPosiFlag == DIVIDED_MENU_SIBLING)
		{
			int		nCurMenuItemIndex;
			int		nCfgItemNum;

			nModifiedMenuItemID = LCD_MERGE_ACTUAL_EQUIP_ID(pCfgSubMenuInfo->nSigType, pCfgSubMenuInfo->nEquipID);
			pModifiedMenuItem = FindParentMenuItemByID(pRootSubMenu, nModifiedMenuItemID, &nCurMenuItemIndex);
			if(pModifiedMenuItem)
			{
				pOldSubMenu = (SUB_MENU*)(pModifiedMenuItem->pvItemData);

				nCfgItemNum = Cfg_GetSplitStringCount(pCfgSubMenuInfo->caMenuItemIndex, DIVIDED_SUB_MENU_SPLITTER);

				nMenuItem = nCfgItemNum + pOldSubMenu->nItems - 1;

				pNewSubMenu = NEW(SUB_MENU, 1);
				LOGOUT_NO_MEMORY(pNewSubMenu);
				ZERO_POBJS(pNewSubMenu, 1);

				pNewMenuItem = NEW(MENU_ITEM, nMenuItem);
				LOGOUT_NO_MEMORY(pNewMenuItem);
				ZERO_POBJS(pNewMenuItem, nMenuItem);

				pDispItems = NEW(int, nMenuItem);
				LOGOUT_NO_MEMORY(pDispItems);
				ZERO_POBJS(pDispItems, nMenuItem);

				INIT_SUB_MENU(pNewSubMenu,
					pModifiedMenuItem->nMenuItemID, 
					nMenuItem,
					pNewMenuItem,
					pDispItems);

				//�Ѿɵ��Ӳ˵��У�ǰ��Ĳ˵������
				if(nCurMenuItemIndex > 0)
				{
					memcpy(pNewSubMenu->pItems, pOldSubMenu->pItems, nCurMenuItemIndex * sizeof(MENU_ITEM));
				}

				//�����µ��Ӳ˵���
				j = 0;
				pMenuItemIndex = pCfgSubMenuInfo->caMenuItemIndex;
				pBuf = NULL;
				while((pMenuItemIndex = Cfg_SplitStringEx(pMenuItemIndex, &pBuf, DIVIDED_SUB_MENU_SPLITTER))
					&& (pMenuItemIndex != pBuf))
				{
					nMenuItemIndex = atoi(pBuf);

					pCfgSubMenuItem = FindDividedSubMenuItem(pLcdPrivateCfgLoader, nMenuItemIndex);
					ASSERT(pCfgSubMenuItem);

					CreateDividedSubMenuItem(pNewSubMenu->pItems + nCurMenuItemIndex + j++, pCfgSubMenuItem);

				}
				ASSERT(nCfgItemNum == j);

				//�Ѿɵ��Ӳ˵��У�����Ĳ˵���ҲҪ������
				for(j = nCurMenuItemIndex + 1; j < pOldSubMenu->nItems; j++)
				{
					memcpy((pNewSubMenu->pItems + nCfgItemNum - 1 + j),
						(pOldSubMenu->pItems + j),
						sizeof(MENU_ITEM));
				}

				//�ѾɵĲ˵�����ָ����Ӳ˵�ɾ��
				DelMenuRecursively((pOldSubMenu->pItems + nCurMenuItemIndex)->pvItemData);

				//�Ѿɵ��Ӳ˵�ɾ��
				DELETE_ITEM(pOldSubMenu->pItems);
				DELETE_ITEM(pOldSubMenu->pDispItems);
				DELETE_ITEM(pOldSubMenu);

				pModifiedMenuItem->pvItemData = (VOID*)pNewSubMenu;
			}
			else
			{
				DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT find Menu Item.");
			}

		}
		else
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("PRI_CFG:Divided sub menu info:Position Flag ERROR!");
			// NOT surport by now
		}
	}

	return ERR_LCD_OK;
}


static int ChangeMenuItemResource(SUB_MENU *pRootSubMenu, LCD_PRIVATE_CFG_LOADER *pLcdPrivateCfgLoader)
{
	ASSERT(pRootSubMenu);
	ASSERT(pLcdPrivateCfgLoader);

	CHANGE_MENU_ITEM_RESOURCE_CFG *pChgMenuItemResCfg;
	pChgMenuItemResCfg = pLcdPrivateCfgLoader->stChgMenuItemResCfgReader.pChgMenuItemResCfg;

	int nCfgNum = pLcdPrivateCfgLoader->stChgMenuItemResCfgReader.nChgMenuItemResNum;

	int nMenuItemID;
	MENU_ITEM *pModifyMenuItem;

	LANG_TEXT* pLangText;
	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	int i;
	for(i = 0;
		(i < nCfgNum) && pChgMenuItemResCfg;
		i++, pChgMenuItemResCfg++)
	{
		nMenuItemID = LCD_MERGE_ACTUAL_EQUIP_ID(pChgMenuItemResCfg->nSigType, pChgMenuItemResCfg->nEquipID);
		pModifyMenuItem = FindMenuItemByID(pRootSubMenu, nMenuItemID);

		pLangText = GetLCDLangText(
			pChgMenuItemResCfg->nResouceID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		if(pModifyMenuItem && pLangText)
		{
			pModifyMenuItem->nMenuItemID = LCD_MERGE_CHANGE_MENU_ITEM_ID(pChgMenuItemResCfg->nResouceID);
		}
		else
		{
			DEBUG_LCD_FILE_FUN_LINE_STRING("Can NOT Find Menu item or Resouce.");
		}
	}

	return ERR_LCD_OK;
}

/*==========================================================================*
 * FUNCTION : LUI_GetDwordSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: nEquipId    : 
 *            nSignalType : 
 *            nSignalId   : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-10-18 10:09
 *==========================================================================*/
DWORD LUI_GetDwordSigValue(int nEquipId, int nSignalType, int nSignalId)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);

	return pSigValue->varValue.ulValue;

}



/*==========================================================================*
* FUNCTION : CreateDefaultMenu
* PURPOSE  : The default menu is blank
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_STRU**  ppMenu : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:06
*==========================================================================*/
static int CreateDefaultMenu(MENU_STRU** ppMenu)
{
	UNUSED(ppMenu);
	int nError = ERR_LCD_OK;

	return nError;
}


/*==========================================================================*
* FUNCTION : CreateMainMenu
* PURPOSE  : Create main menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_STRU**  ppMenu : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:06
*==========================================================================*/
static int CreateMainMenu(MENU_STRU** ppMenu)
{
	// 1. Only parse equips and signals info for LCD display
	ParseEquipsAndSignals(&g_stParseEquipInfo);

	ParseMainMenuNode(&g_stParseMenuNode);

	FeedDogOfLCDThread();

	// 2. Create the Main Sub menu

	int					nError = ERR_LCD_OK;

	static MENU_STRU	s_MainMenuStru;

	SUB_MENU*			pSubMenu;
	MENU_ITEM*			pMenuItem;
	int*				pDispItems;

	/* System main menu -- SUB_MENU */
	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItem = NEW(MENU_ITEM, MAIN_MENU_ITEM_NUM);
	LOGOUT_NO_MEMORY(pMenuItem);
	ZERO_POBJS(pMenuItem, MAIN_MENU_ITEM_NUM);

	pDispItems = NEW(int, MAIN_MENU_ITEM_NUM);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, MAIN_MENU_ITEM_NUM);

	INIT_MENU_STRU(&s_MainMenuStru, pSubMenu);
	INIT_SUB_MENU(pSubMenu,
				LCD_MERGE_FIXED_LANG_ID(MAIN_MENU_LANG_ID),
				MAIN_MENU_ITEM_NUM,
				pMenuItem,
				pDispItems);

	// 3. ������������Ϣ���Ӳ˵�
	CreateRunInfoMenu(pMenuItem);

	// 5. �������������á��Ӳ˵�
	pMenuItem++;
	CreateSignalSetMenu(pMenuItem);


	// 6. Others
	/* Energy Saving Menu */
#if ENERGY_SAVIING_MENU_NUM
	pMenuItem++;
	CreateEnergySavingMenu(pMenuItem);
#endif

	// 4. ����������������Ӳ˵�
	pMenuItem++;
	CreateSignalCtrlMenu(pMenuItem);

	/* Customize Menu */
#if CUSTOMIZE_MENU_NUM
	pMenuItem++;
	CreateCustomizeMenu(pMenuItem);
#endif

#if SELF_TEST_MENU1_NUM
	pMenuItem++;
	INIT_SELF_TEST_MENU_ITEM(pMenuItem,
		LCD_MERGE_FIXED_LANG_ID(MAIN_SELF_TEST1_LANG_ID));
#endif

#if SELF_TEST_MENU2_NUM
	pMenuItem++;
	INIT_SELF_TEST_MENU_ITEM(pMenuItem,
		LCD_MERGE_FIXED_LANG_ID(MAIN_SELF_TEST2_LANG_ID));
#endif

	// 7. Init the MENU_STRU
	//Init menu stack, be used to save the previous menu
	STACK_INIT( &(s_MainMenuStru.stack), 
		s_MainMenuStru.pStackBuff, 
		MAX_SUB_MENU_LEVEL );

	//root menu
	s_MainMenuStru.pCurMenu   = s_MainMenuStru.pRootMenu;
	//has no last selected menu
	s_MainMenuStru.pLastItem     = NULL;                   
	//Set the current menu to the start
	s_MainMenuStru.pCurMenu->nCur = 0;           

	ASSERT(ppMenu);
	*ppMenu = &s_MainMenuStru;



	// 8. Special Handle for some divided sub menu
	nError = CheckDividedSubMenuCfg(&g_stLcdPrivateCfgLoader);
	if(nError == ERR_LCD_DIVIDED_SUB_MENU_CFG_OK)
	{
		CreateDividedMenu(s_MainMenuStru.pRootMenu, &g_stLcdPrivateCfgLoader);
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("CheckDividedSubMenuCfg() ERROR!");
		DEBUG_LCD_FILE_FUN_LINE_HEX("Error Code", nError);
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]-- ERROR: LCD PRIVATE CFG:DIVIDED SUB MENU. Error code is %d!\n\r", __FILE__, nError);
	}

	// 9. Special Handle for some menu item should change resource
	ChangeMenuItemResource(s_MainMenuStru.pRootMenu, &g_stLcdPrivateCfgLoader);

	// 10. Release the memory malocated in Parse... Func
	DestroyParseMemory();

	return nError;
}


//Frank Wu,20160127, for MiniNCU
static int CreateMainMenuMiniNCU(MENU_STRU** ppMenu)
{
	// 1. Only parse equips and signals info for LCD display
	ParseEquipsAndSignals(&g_stParseEquipInfo);

	ParseMainMenuNode(&g_stParseMenuNode);

	FeedDogOfLCDThread();

	// 2. Create the Main Sub menu

#define MINI_NCU_RECTIFIER_INFO_MENU_NUM	1

#define MINI_NCU_MAIN_MENU_ITEM_NUM			(ACTIVE_ALARM_MENU_NUM + HISTORY_ALARM_MENU_NUM \
	+ BARCODE_MENU_NUM + PARAMETER_SET_MENU_NUM \
	+ SLAVE_PARAMETER_SET_MENU_NUM \
	+ MINI_NCU_RECTIFIER_INFO_MENU_NUM )


	int					nError = ERR_LCD_OK;
	static MENU_STRU	s_MainMenuStru;
	SUB_MENU*			pSubMenu;
	MENU_ITEM*			pMenuItems;
	MENU_ITEM*			pMenuItem;
	int*				pDispItems;


	/* System main menu -- SUB_MENU */
	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pMenuItems = NEW(MENU_ITEM, MINI_NCU_MAIN_MENU_ITEM_NUM);
	LOGOUT_NO_MEMORY(pMenuItems);
	ZERO_POBJS(pMenuItems, MINI_NCU_MAIN_MENU_ITEM_NUM);

	pDispItems = NEW(int, MINI_NCU_MAIN_MENU_ITEM_NUM);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, MINI_NCU_MAIN_MENU_ITEM_NUM);

	INIT_MENU_STRU(&s_MainMenuStru, pSubMenu);
	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_FIXED_LANG_ID(MAIN_MENU_LANG_ID),
		MINI_NCU_MAIN_MENU_ITEM_NUM,
		pMenuItems,
		pDispItems);

	pMenuItem	= pMenuItems;

#if ACTIVE_ALARM_MENU_NUM
	// 1. Active alarm
	//����ǰ�澯���Ӳ˵�
	g_pActiveAlarmMenuItem = pMenuItem;
	AddActiveAlarmItems(pMenuItem);
	pMenuItem++;
#endif

#if HISTORY_ALARM_MENU_NUM
	// 2. History alarm
	g_pHistoryAlarmMenuItem = pMenuItem;
	//History alarm will be added when enter into the view of history alarm

	INIT_SUBMENU_ITEM(pMenuItem, 
		LCD_MERGE_FIXED_LANG_ID(HISTORY_ALARM_DISP_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		NULL);
	pMenuItem++;
#endif

	// 3. Barcode Menu item
#if BARCODE_MENU_NUM
	AddSiteInventoryItems(pMenuItem);
	pMenuItem++;
#endif

#if MINI_NCU_RECTIFIER_INFO_MENU_NUM
	// 4. ������ģ����Ϣ���Ӳ˵�
	ConstructEquipSubMenuMiniNCU(pMenuItem, SIG_TYPE_SAMPLING, RECT_GROUP_EQUIP_TYPE);
	pMenuItem++;
#endif

#if PARAMETER_SET_MENU_NUM
	// 5. ��������ģʽ�¡��������á��Ӳ˵�
	CreateSignalSetMenuMiniNCU(pMenuItem);
	pMenuItem++;
#endif

#if SLAVE_PARAMETER_SET_MENU_NUM
	// 5. �����ӻ�ģʽ�¡��������á��Ӳ˵�
	CreateSlaveSignalSetMenuMiniNCU(pMenuItem);
	pMenuItem++;
#endif

	// 6. Init the MENU_STRU
	//Init menu stack, be used to save the previous menu
	STACK_INIT( &(s_MainMenuStru.stack), 
		s_MainMenuStru.pStackBuff, 
		MAX_SUB_MENU_LEVEL );

	//root menu
	s_MainMenuStru.pCurMenu = s_MainMenuStru.pRootMenu;
	//has no last selected menu
	s_MainMenuStru.pLastItem = NULL;
	//Set the current menu to the start
	s_MainMenuStru.pCurMenu->nCur = 0;

	ASSERT(ppMenu);
	*ppMenu = &s_MainMenuStru;

	// 8. Special Handle for some divided sub menu
	nError = CheckDividedSubMenuCfg(&g_stLcdPrivateCfgLoader);
	if(nError == ERR_LCD_DIVIDED_SUB_MENU_CFG_OK)
	{
		CreateDividedMenu(s_MainMenuStru.pRootMenu, &g_stLcdPrivateCfgLoader);
	}
	else
	{
		DEBUG_LCD_FILE_FUN_LINE_STRING("CheckDividedSubMenuCfg() ERROR!");
		DEBUG_LCD_FILE_FUN_LINE_HEX("Error Code", nError);
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]-- ERROR: LCD PRIVATE CFG:DIVIDED SUB MENU. Error code is %d!\n\r", __FILE__, nError);
	}

	// 9. Special Handle for some menu item should change resource
	ChangeMenuItemResource(s_MainMenuStru.pRootMenu, &g_stLcdPrivateCfgLoader);


	// 10. Release the memory malocated in Parse... Func
	DestroyParseMemory();

	return nError;
}



/*==========================================================================*
* FUNCTION : CreatePasswordMenu
* PURPOSE  : Create password menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_STRU**  ppMenu : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:03
*==========================================================================*/
static int CreatePasswordMenu(MENU_STRU** ppMenu)
{
	int					nError = ERR_LCD_OK;

	static LANG_TEXT	s_UserStateText[USERS_MAX_NUMBER];

	static LANG_TEXT*	s_UserStateTextArray[USERS_MAX_NUMBER];

	static SELF_DEFINE_INFO s_PwdItemInfo[] = 
	{
		DEF_SELF_DEFINE_INFO( PASSWORD_DLG_USER_ID, 
			NULL,
			VAR_ENUM, 
			BROWSER_LEVEL, 
			"-4", //-4 is to show the maximum length of user name(if length >= 4)
			1, 
			0,
			0,
			USERS_MAX_NUMBER,
			s_UserStateTextArray,
			NULL),

		DEF_SELF_DEFINE_INFO( PASSWORD_DLG_PWD_ID, 
			NULL,
			VAR_STRING,
			BROWSER_LEVEL, 
			"", 
			0, 
			0,
			0,
			0, 
			NULL,
			NULL)
	};
	/* ������ */
	static MENU_ITEM s_PwdMenuItem[] =
	{
		DEF_SIGNAL_ITEM( MT_PASSWORD_SELECT, 
			LCD_MERGE_FIXED_LANG_ID(PASSWORD_DLG_USER_ID),
			MENU_ITEM_NEED_NOT_VALIDATE,
			0, 
			&(s_PwdItemInfo[0])),

		DEF_SIGNAL_ITEM( MT_PASSWORD_SELECT,
			LCD_MERGE_FIXED_LANG_ID(PASSWORD_DLG_PWD_ID), 
			MENU_ITEM_NEED_NOT_VALIDATE, 
			0, 
			&(s_PwdItemInfo[1]) )

	};
	static int s_PwdMenuDispItem[] = {0, 1};

	/* password -- SUB_MENU */
	static SUB_MENU		s_PasswordMenu = 
		DEF_SUB_MENU(LCD_MERGE_FIXED_LANG_ID(PASSWORD_SCREEN_MENU_ID),
		s_PwdMenuItem, s_PwdMenuDispItem);

	static MENU_STRU	s_PwdMenuStru = DEF_MENU_STRU(&s_PasswordMenu) ;

	int i;
	for(i = 0; i < USERS_MAX_NUMBER; i++)
	{
		s_UserStateTextArray[i] = &(s_UserStateText[i]);
	}

	//root menu
	s_PwdMenuStru.pCurMenu   = s_PwdMenuStru.pRootMenu;

	//has no last selected menu
	s_PwdMenuStru.pLastItem     = NULL;                   

	//Set the current menu to the start
	s_PwdMenuStru.pCurMenu->nCur = 0;           


	ASSERT(ppMenu);

	*ppMenu = &s_PwdMenuStru;


	return nError;
}


/*==========================================================================*
* FUNCTION : DelMenuRecursively
* PURPOSE  : Delete the sub menu structure Recursivel
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: SUB_MENU*  pSubMenu : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:02
*==========================================================================*/
static int DelMenuRecursively(SUB_MENU* pSubMenu)
{
	MENU_ITEM   *pItems, *pMenuItem;
	int			nItemNum;
	int			i;
	int *pDispItems;

	ASSERT(pSubMenu);

	pItems = pSubMenu->pItems;
	nItemNum = pSubMenu->nItems;
	pDispItems = pSubMenu->pDispItems;

	pMenuItem = pItems;

	for (i = 0; i < nItemNum; i++, pMenuItem++)
	{
		if(pMenuItem->cMenuType == MT_SUBMENU)
		{
			if(pMenuItem->pvItemData != NULL)
			{
				DelMenuRecursively((SUB_MENU*)(pMenuItem->pvItemData));
			}
		}
	}


	DELETE_ITEM(pItems);

	DELETE_ITEM(pDispItems);

	DELETE_ITEM(pSubMenu);

	return ERR_LCD_OK;
}


static int DestroyParseMemory(void)
{
	// Delete the memory of Parse...
	PARSE_MENU_NODE		*pParseMenuNode = &g_stParseMenuNode;
	PARSE_EQUIP_INFO	*pParseEquipInfo = &g_stParseEquipInfo;

	// 1. Delete g_stParseMenuNode
	DISP_CLASS_EQUIP_NODE *pDispClassEquipNode;

	int i, nSigType;

	for(nSigType = SIG_TYPE_SAMPLING; nSigType <= SIG_TYPE_ALARM; nSigType++)
	{
		pDispClassEquipNode = pParseMenuNode->pDispClassEquipNode[nSigType];
		ASSERT(pDispClassEquipNode);

		for(i = 0; i < pParseMenuNode->nDispClassEquipNum[nSigType]; i++)
		{
			DELETE_ITEM((pParseMenuNode->pDispClassEquipNode[nSigType] + i)->pDispActEquipNode);
		}
		DELETE_ITEM(pDispClassEquipNode);
	}

	// 2. Delete g_stParseEquipInfo

	// 2.1 Delete g_stParseEquipInfo.pClassEquipInfo
	CLASS_EQUIP_INFO	*pClassEquipInfo;

	for(i = 0; i < pParseEquipInfo->nClassEquipNum;	i++)
	{
		pClassEquipInfo = pParseEquipInfo->pClassEquipInfo + i;
		ASSERT(pClassEquipInfo);

		DELETE_ITEM(pClassEquipInfo->ppStandardEquipInfo);
	}
	DELETE_ITEM(pParseEquipInfo->pClassEquipInfo);

	// 2.2 Delete g_stParseEquipInfo.pStdEquipInfo
	STANDARD_EQUIP_INFO	*pStdEquipInfo;
	SIGNAL_INFO* pSignalInfo;
	for(i = 0; i < pParseEquipInfo->nStdEquipNum; i++)
	{
		pStdEquipInfo = pParseEquipInfo->pStdEquipInfo + i;
		ASSERT(pStdEquipInfo);

		pSignalInfo = &(pStdEquipInfo->stSignalInfo);
		ASSERT(pSignalInfo);

		DELETE_ITEM(pSignalInfo->ppSampleSigInfo);
		DELETE_ITEM(pSignalInfo->ppCtrlSigInfo);
		DELETE_ITEM(pSignalInfo->ppSetSigInfo);
		DELETE_ITEM(pSignalInfo->ppAlarmSigInfo);

		DELETE_ITEM(pStdEquipInfo->ppActualEquip);
	}

	DELETE_ITEM(pParseEquipInfo->pStdEquipInfo);

	return ERR_LCD_OK;

}




/*==========================================================================*
* FUNCTION : DestroyMenus
* PURPOSE  : Destroy menus
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_DISPLAY*  pMenuDisplay : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:02
*==========================================================================*/
static int DestroyMenus(MENU_DISPLAY* pMenuDisplay)
{
	int nError = ERR_LCD_OK;


	DelMenuRecursively(pMenuDisplay->pMainMenu->pRootMenu);

	return nError;
}


/*==========================================================================*
* FUNCTION : FreeLangText
* PURPOSE  : Free language text structure
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: LANG_TEXT  *pLangText : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:01
*==========================================================================*/
static void FreeLangText(LANG_TEXT *pLangText)
{
	int i;

	for (i = 0; i < MAX_LANG_TYPE; i++)
	{
		DELETE_ITEM(pLangText->pFullName[i]);

		DELETE_ITEM(pLangText->pAbbrName[i]);
	}

	return;
}

static int ClearLCDPrivateConfigLangFile(LANG_FILE *pstLCDLangFile)
{

	int			nError = ERR_LCD_OK;
	int			iLangTextNum;	
	LANG_TEXT	*pLangText;
	int			j;

	/* free Lang Texts in each Lang File */
	iLangTextNum = pstLCDLangFile->iLangTextNum;
	pLangText = pstLCDLangFile->pLangText;

	for (j = 0; j < iLangTextNum; j++)
	{
		FreeLangText(&(pLangText[j]));	
	}

	/* now free Lang File */
	DELETE_ITEM(pLangText);

	//init
	pstLCDLangFile->iLangTextNum = 0;
	pstLCDLangFile->pLangText = NULL;

	return nError;
}


/*==========================================================================*
* FUNCTION : DestroyLCDPrivateConfig
* PURPOSE  : Delete the config loader buffer
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: LCD_PRIVATE_CFG_LOADER*  pLcdPrivateCfgLoader : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:00
*==========================================================================*/
static int DestroyLCDPrivateConfig(LCD_PRIVATE_CFG_LOADER* pLcdPrivateCfgLoader)
{
	ASSERT(pLcdPrivateCfgLoader);

	int			nError = ERR_LCD_OK;

	int			iLangTextNum;	
	LANG_TEXT	*pLangText;

	/* free Lang Texts in each Lang File */
	iLangTextNum = pLcdPrivateCfgLoader->stLCDLangFile.iLangTextNum;
	pLangText = pLcdPrivateCfgLoader->stLCDLangFile.pLangText;
	ASSERT(pLangText);

	int			j;
	for (j = 0; j < iLangTextNum; j++)
	{
		FreeLangText(&(pLangText[j]));	
	}

	/* now free Lang File */
	DELETE_ITEM(pLangText);


	DELETE_ITEM(pLcdPrivateCfgLoader->stDefaultScreenCfgReader128.pDefaultScreenItemCfg);
	DELETE_ITEM(pLcdPrivateCfgLoader->stDefaultScreenCfgReader64.pDefaultScreenItemCfg);

	DELETE_ITEM(pLcdPrivateCfgLoader->stDividedSubMenuItemCfgReader.pDividedSubMenuItemCfg);
	DELETE_ITEM(pLcdPrivateCfgLoader->stDividedSubMenuInfoCfgReader.pDividedSubMenuInfoCfg);

	DELETE_ITEM(pLcdPrivateCfgLoader->stChgMenuItemResCfgReader.pChgMenuItemResCfg);

	DELETE_ITEM(pLcdPrivateCfgLoader->stVirGroupEquipNameCfgReader.pVirGroupEquipNameResCfg);

	return nError;
}

//Use the array to save time info
TIMER_ENTRY g_s_lcdTimeEntry[] = 
{
	{NULL, NULL, TM_ID_PASSWORD_VALID, INTERVAL_PASSWORD_VALID,
		TM_FUNC_PasswordValid, 
		(DWORD)(&(g_gvThisService.nTimerIDPasswordValid)), 0},

	{NULL, NULL, TM_ID_SWITCH_DEF_SCREEN, INTERVAL_SWITCH_DEF_SCREEN,
	TM_FUNC_SwitchDefScreen, 
	(DWORD)(&(g_gvThisService.nTimerIDSwitchDefScreen)), 0},

	{NULL, NULL, TM_ID_ALARM_POP_AT_ONCE, INTERVAL_ALARM_POP_AT_ONCE,
	TM_FUNC_AlarmPopAtOnce, 
	(DWORD)(&(g_gvThisService.nTimerIDAlarmPopAtOnce)), 0},

	{NULL, NULL, TM_ID_INIT_LCD, INTERVAL_INIT_LCD,
	TM_FUNC_InitLcd, 
	(DWORD)(&(g_gvThisService.nTimerIDInitLCD)), 0},

	{NULL, NULL, TM_ID_REFRESH_DATA, INTERVAL_REFRESH_DATA,
	TM_FUNC_RefreshData,
	(DWORD)(&(g_gvThisService.nTimerIDDataRefresh)), 0},
};

/*==========================================================================*
* FUNCTION : TM_FUNC_PasswordValid
* PURPOSE  : When time up, the function will be excute, set corresponding flag
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hself   : 
*            int     idTimer : 
*            void    *par    : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:42
*==========================================================================*/
static int TM_FUNC_PasswordValid(HANDLE hself, int idTimer, void *par)
{
	GLOBAL_VAR_OF_LCD* pGlobalVarOfLCD = (GLOBAL_VAR_OF_LCD*)par;

	UNUSED(hself);
	UNUSED(idTimer);

	ASSERT(pGlobalVarOfLCD);

	pGlobalVarOfLCD->cPasswordFlag = NEED_PASSWORD_VALIDATE;

	return ERR_LCD_OK;
}

/*==========================================================================*
* FUNCTION : TM_FUNC_SwitchDefScreen
* PURPOSE  : When time up, the function will be excute, set corresponding flag
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hself   : 
*            int     idTimer : 
*            void    *par    : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:42
*==========================================================================*/
static int TM_FUNC_SwitchDefScreen(HANDLE hself, int idTimer, void *par)
{
	GLOBAL_VAR_OF_LCD* pGlobalVarOfLCD = (GLOBAL_VAR_OF_LCD*)par;

	UNUSED(hself);
	UNUSED(idTimer);

	ASSERT(pGlobalVarOfLCD);

	pGlobalVarOfLCD->cSwitchDefScreenFlag = NEED_SWITCH_TO_DEF_SCREEN;

	return ERR_LCD_OK;
}

/*==========================================================================*
* FUNCTION : TM_FUNC_AlarmPopAtOnce
* PURPOSE  : When time up, the function will be excute, set corresponding flag
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hself   : 
*            int     idTimer : 
*            void    *par    : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:42
*==========================================================================*/
static int TM_FUNC_AlarmPopAtOnce(HANDLE hself, int idTimer, void *par)
{
	GLOBAL_VAR_OF_LCD* pGlobalVarOfLCD = (GLOBAL_VAR_OF_LCD*)par;

	UNUSED(hself);
	UNUSED(idTimer);

	ASSERT(pGlobalVarOfLCD);

	pGlobalVarOfLCD->cAlarmPopAtOnceFlag = NEED_ALARM_POP_AT_ONCE;

	return ERR_LCD_OK;
}

/*==========================================================================*
* FUNCTION : TM_FUNC_InitLcd
* PURPOSE  : When time up, the function will be excute, set corresponding flag
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hself   : 
*            int     idTimer : 
*            void    *par    : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:41
*==========================================================================*/
static int TM_FUNC_InitLcd(HANDLE hself, int idTimer, void *par)
{
	GLOBAL_VAR_OF_LCD* pGlobalVarOfLCD = (GLOBAL_VAR_OF_LCD*)par;

	UNUSED(hself);
	UNUSED(idTimer);

	ASSERT(pGlobalVarOfLCD);

	pGlobalVarOfLCD->cInitLCDFlag = NEED_INIT_LCD;

	return ERR_LCD_OK;
}

/*==========================================================================*
* FUNCTION : TM_FUNC_RefreshData
* PURPOSE  : When time up, the function will be excute, set corresponding flag
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hself   : 
*            int     idTimer : 
*            void    *par    : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:41
*==========================================================================*/
static int TM_FUNC_RefreshData(HANDLE hself, int idTimer, void *par)
{
	GLOBAL_VAR_OF_LCD* pGlobalVarOfLCD = (GLOBAL_VAR_OF_LCD*)par;

	UNUSED(hself);
	UNUSED(idTimer);

	ASSERT(pGlobalVarOfLCD);

	pGlobalVarOfLCD->cDataRefreshFlag = NEED_REFRESH_DATA;

	return ERR_LCD_OK;
}


/*==========================================================================*
* FUNCTION : InitLCDTimers
* PURPOSE  : Init timers, has five timers
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:41
*==========================================================================*/
static int InitLCDTimers(void)
{
	int nError = ERR_LCD_OK;

	int i;

	for (i = 0; i < ITEM_OF(g_s_lcdTimeEntry); i++)
	{
		if(Timer_Set(g_gvThisService.hThreadSelf,
			g_s_lcdTimeEntry[i].idTimer, 
			g_s_lcdTimeEntry[i].nInterval,
			g_s_lcdTimeEntry[i].pfnAction,
			(DWORD)&g_gvThisService)
			!= ERR_OK)
		{
			AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
				"[%s]--InitLCDTimers: ERROR: timer exists or set failure! "
				"Timer ID is %d", 
				__FILE__, g_s_lcdTimeEntry[i].idTimer);

			nError = ERR_LCD_TIMER_SET_FAILURE;
		}

		*((int*)(g_s_lcdTimeEntry[i].dwParam)) = g_s_lcdTimeEntry[i].idTimer;	
	}

	return nError;
}

/*==========================================================================*
* FUNCTION : DestroyLCDTimers
* PURPOSE  : Destroy timers
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:41
*==========================================================================*/
static int DestroyLCDTimers(void)
{
	int i;

	for (i = 0; i < ITEM_OF(g_s_lcdTimeEntry); i++)
	{
		Timer_Kill(g_gvThisService.hThreadSelf, g_s_lcdTimeEntry[i].idTimer);
	}

	return ERR_LCD_OK;
}

/*==========================================================================*
* FUNCTION : ServiceNotification
* PURPOSE  : Regist alarm report function(_ALARM_OCCUR_MASK, _ALARM_CLEAR_MASK)
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hService        : 
*            int     nMsgType        : 
*            int     nTrapDataLength : 
*            void*   lpTrapData      : 
*            void*   lpParam         :  
*            BOOL    bUrgent         : 
* RETURN   : int : 0 for success, 1 for error(defined in DXI module)
* COMMENTS : 
* CREATOR  : HULONGWEN                   DATE: 2004-10-22 17:23
*==========================================================================*/
static int ServiceNotification(HANDLE hService, 
							   int nMsgType, 
							   int nTrapDataLength, 
							   void *lpTrapData,	
							   void *lpParam, 
							   BOOL bUrgent)
{
	UNUSED(hService);
	UNUSED(nTrapDataLength);
	UNUSED(lpTrapData);
	UNUSED(bUrgent);

	/*TRACEX("LCD receive a trap, code is 0x%x.\n", 
		nMsgType);*/

	if ((nMsgType & (_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK)) != 0)
	{
		//Because the trap send info include active or deactive status and 
		//alarm acknowledge info,
		//so extend the alarm signal value structure
		ALARM_SIG_VALUE_EX_LCD	stAlarmSigValue;

		ACTIVE_ALARM_MANAGER* pAlarmManager = (ACTIVE_ALARM_MANAGER*)lpParam;


		if (nTrapDataLength != sizeof(ALARM_SIG_VALUE))
		{
			return 0;
		}

		memcpy(&(stAlarmSigValue.bvAlarm), lpTrapData, 
			sizeof(ALARM_SIG_VALUE));

		stAlarmSigValue.nAlarmMsgType = nMsgType;

		stAlarmSigValue.bAlarmAck = FALSE;//not be acknowledged

		if (pAlarmManager)
		{
			//Put the report info to alarm queue
			Queue_Put(pAlarmManager->hActiveAlarmReportQueue, &stAlarmSigValue, FALSE);
		}

		return 1;
	}

	return 0;
}

/*==========================================================================*
* FUNCTION : InitLCDService
* PURPOSE  : Init lcd driver, config, menu, timers, alarm report func
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:40
*==========================================================================*/
static int InitLCDService(void)
{

	int		nError = ERR_LCD_OK;

	int			iRet;

	// 1. Load lcd private config,resource file
	if (LoadLCDPrivateConfig(&g_stLcdPrivateCfgLoader) != ERR_CFG_OK)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
				"Fails on loading LCD private configuration.\n");

	}

	// 2. Init timers
	InitLCDTimers();

	// 3. CreateMenu
	nError = CreateMenus(&g_mdMenuDisplay);

	// 4. Init Sub Menu display status
	RefreshSubMenuStatus(g_mdMenuDisplay.pMainMenu->pRootMenu, TRUE);

	g_gvThisService.cPasswordFlag = NEED_PASSWORD_VALIDATE;

	/* 5. register in the DXI module to receive Alarms */
	iRet = DxiRegisterNotification("LCD service", 
		ServiceNotification,
		&g_s_ActiveAlarmManager, // it shall be changed to the actual arg
		_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 
		g_gvThisService.hThreadSelf,
		_REGISTER_FLAG);

	InitActiveAlarmManager();//Get the active alarm list after register occur and clear alarm report 

	return nError;
}


/*==========================================================================*
* FUNCTION : DestroyLCDService
* PURPOSE  : Destroy all the initted object
*			  lcd driver, font lib, menu, config, timer, alarm report func
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:38
*==========================================================================*/
static int DestroyLCDService(void)
{

	int		nError = ERR_LCD_OK;

	//Frank Wu,20160127, for MiniNCU
	//char*	ppLangCode[SYS_LANG_NUM_ONE_TIME];

	int		nBufLen;


	CloseLcdDriver();
	//Frank Wu,20160127, for MiniNCU
#if 0
	ppLangCode[0] = LCD_FIXED_LANGUAGE;

	nError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		LOCAL_LANGUAGE_CODE, 
		0, 
		&nBufLen,
		&(ppLangCode[1]),
		0);

	if(nError != ERR_DXI_OK)
	{
		ppLangCode[1] = "";
	}
	
	//UnRegisterFontLib
	RegisterFontLib(SYS_LANG_NUM_ONE_TIME, ppLangCode, FLAG_UNREGIST_FONT_LIB);
#else
	RegisterFontLib(MINI_NCU_LANGUAGE_LIST_NUM, MINI_NCU_LANGUAGE_LIST_PTR, FLAG_UNREGIST_FONT_LIB);
#endif

	DestroyMenus(&g_mdMenuDisplay);

	DestroyLCDPrivateConfig(&g_stLcdPrivateCfgLoader);

	DestroyLCDTimers();


	nError = DxiRegisterNotification("LCD service", 
		ServiceNotification,
		&g_s_ActiveAlarmManager, // it shall be changed to the actual arg
		_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK, 
		g_gvThisService.hThreadSelf,
		_CANCEL_FLAG);

	DestroyActiveAlarmManager();

	return nError;
}

static BOOL	LUI_StopSplash(int nWaitSec)
{
	char	szCmdLine[MAX_FILE_PATH];
	char *	pszStopSplash = "stopsplash";

	snprintf(szCmdLine, sizeof(szCmdLine), "%s/bin/%s %d",
		getenv(ENV_ACU_ROOT_DIR), pszStopSplash, nWaitSec);
	//system(szCmdLine);
	_SYSTEM(szCmdLine);

	return TRUE;
}



/*==========================================================================*
* FUNCTION : LCDSplashFunc
* PURPOSE  : The LCD splash thread function, 
*			  init LCD, init font lib, display welcome screen
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pArgs : 
* RETURN   : static DWORD : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:17
*==========================================================================*/
static DWORD LCDSplashFunc(void* pArgs)
{
	GLOBAL_VAR_OF_LCD* pThis = (GLOBAL_VAR_OF_LCD*)pArgs;

	ASSERT(pThis);		

//	DisWelcomeScreen(0,0);//display wellcome screen here

	//if LCD ServiceMain() is called to start, return the thread.
	for(;!(pThis->bLCDServiceMainStartFlag);)
	{
		//Can display �����ڴ�
		RunThread_Heartbeat(pThis->hThreadSplash);
	}

	//	printf("splash Thread exit!\r\n");


	return 0;
}



/*==========================================================================*
* FUNCTION : ServiceInit
* PURPOSE  : LCD service init function, create splash thread
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int   argc   : 
*            IN char  **argv : 
* RETURN   : DWORD : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:14
*==========================================================================*/
DWORD ServiceInit(IN int argc,	
				  IN char **argv)
{
	//Frank Wu,20160127, for MiniNCU
	//char*	ppLangCode[SYS_LANG_NUM_ONE_TIME];

	int		nBufLen;

	UNUSED(argc);//by HULONGWEN, ��ʱ��
	UNUSED(argv);
    // 1.Clear buffer
	memset(&g_gvThisService,		0, sizeof(g_gvThisService));

	memset(&g_mdMenuDisplay,		0, sizeof(g_mdMenuDisplay));

	memset(&g_stParseMenuNode,		0, sizeof(g_stParseMenuNode));

	memset(&g_stLcdPrivateCfgLoader,0, sizeof(g_stLcdPrivateCfgLoader));

	//Frank Wu,20160127, for MiniNCU

	RegisterFontLib(MINI_NCU_LANGUAGE_LIST_NUM, MINI_NCU_LANGUAGE_LIST_PTR, FLAG_REGIST_FONT_LIB);

	return 0;

}


/*==========================================================================*
* FUNCTION : ServiceMain
* PURPOSE  : LCD serice main function, init and destroy service and enter 
*			  into LCD operations
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: SERVICE_ARGUMENTS  *pArgs : 
* RETURN   : DWORD : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:15
*==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{
	UNUSED(pArgs);

	HANDLE			hSelf = RunThread_GetId(NULL);

	g_gvThisService.hThreadSelf = hSelf;


	LUI_StopSplash(0);
	Sleep(2000);//wait for LUI_StopSplash

	int iInitRet = InitLCD();//init lcd driver
	int iMaxInitCount = 12;//1min
	
	//make sure kill the splash
	while(ERR_LCD_OK != iInitRet)
	{
		Sleep(4000);

		if(iMaxInitCount > 0)
		{
			iMaxInitCount--;

			_SYSTEM("killall acu_splash");
			_SYSTEM("killall acu_splash");
			Sleep(1000);
			iInitRet = InitLCD();//init lcd driver
		}
		else
		{
			break;
		}
	}

	InitLCDService();
	

	int	nLastBright = ReadLcdVar("lcd_bright");
	if(nLastBright != -1)	//Read OK
	{
		SetLCDBright(nLastBright);
	}

	Light(TRUE);
	AlarmLedCtrl(LED_RUN, TRUE);
	AlarmLedCtrl(LED_YEL, FALSE);
	AlarmLedCtrl(LED_RED, FALSE);
	BuzzerBeepCtrl(FALSE, 0);
	
	
	//Frank Wu,20160127, for MiniNCU
	//All the LCD handle around here and loop around here
	//DoModal_LanguageScreen(&g_mdMenuDisplay, &g_gvThisService);
	DoModal_LanguageScreenMiniNCU(&g_mdMenuDisplay, &g_gvThisService);

	//Exit LCD thread

	//Stop the buzzer beep thread
	RunThread_Stop(g_gvThisService.hThreadBuzzerBeep,
		3000,
		TRUE);

	//Should exit, delete all the pointers
	DestroyLCDService();

	return ERR_LCD_OK;

}

/*==========================================================================*
 * FUNCTION : GetAlarmVoiceBuzzerBeepTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2006-06-12 14:52
 *==========================================================================*/
static int GetAlarmVoiceBuzzerBeepTime(void)
{
	int nAlarmVoiceTime = -1;

	int nBufLen;

	SIG_BASIC_VALUE* pSigValue = NULL;

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
		SIG_ID_ALARM_VOICE_SET,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		if (pSigValue == NULL)
		{
			return nAlarmVoiceTime;
		}

		switch(pSigValue->varValue.enumValue)
		{
		case 0:
			nAlarmVoiceTime = -1;
			break;

		case 1:
			nAlarmVoiceTime = 0;
			break;

		case 2:
			nAlarmVoiceTime = 3 * 60 * 1000;//3min
			break;

		case 3:
			nAlarmVoiceTime = 10 * 60 * 1000;//10min
			break;

		case 4:
			nAlarmVoiceTime = 1 * 60 * 60 * 1000;//1hour
			break;

		case 5:
			nAlarmVoiceTime = 4 * 60 * 60 * 1000;//4hour
			break;

		default:
			break;
		}

	}

	return nAlarmVoiceTime;
}

static BOOL LedAndBuzzerCtrlViaAlarmList(void)
{
	int nAlarmLevel = ALARM_LEVEL_NONE;

	int nObeserAlarmNum = 0;
	int nMajorAlarmNum = 0;
	int nCriticalAlarmNum = 0;

	BOOL bBuzzerBeep = FALSE;

	ALARM_SIG_VALUE_EX_LCD	*p = g_s_ActiveAlarmManager.pAlarmList;

	/*int nSigType;
	int nSigID;*/
	VAR_VALUE vdValue;


	while( p != NULL )
	{
		nAlarmLevel = p->bvAlarm.pStdSig->iAlarmLevel;

		if (nAlarmLevel == ALARM_LEVEL_OBSERVATION)
		{
			nObeserAlarmNum++;
		}
		else if (nAlarmLevel == ALARM_LEVEL_MAJOR)
		{
			nMajorAlarmNum++;

			if (!(p->bAlarmAck))
			{
				bBuzzerBeep = TRUE;
			}
		}
		else if (nAlarmLevel == ALARM_LEVEL_CRITICAL)
		{
			nCriticalAlarmNum++;

			if (!(p->bAlarmAck))
			{
				bBuzzerBeep = TRUE;
			}
		}

		p = p->next;

	}

	GLOBAL_VAR_OF_LCD *pThis = &g_gvThisService;
	pThis->nOAnum = nObeserAlarmNum;
	pThis->nCAnum = nCriticalAlarmNum;
	pThis->nMAnum = nMajorAlarmNum;

	//If observeation alarm, light yellow leb
	if (nObeserAlarmNum > 0)
	{
		AlarmLedCtrl(LED_YEL, TRUE);
	}
	else
	{
		AlarmLedCtrl(LED_YEL, FALSE);
	}

	//If major or critical alarm, light red leb
	if (nMajorAlarmNum > 0 || nCriticalAlarmNum > 0)
	{
		AlarmLedCtrl(LED_RED, TRUE);
	}
	else
	{
		AlarmLedCtrl(LED_RED, FALSE);
	}


#define SYSTME_ALARM_LED_ON 1
#define SYSTME_ALARM_LED_OFF 0

	//If observation, major or critical alarm, light system alarm LED
	vdValue.enumValue = 
		(nObeserAlarmNum > 0 || nMajorAlarmNum > 0 || nCriticalAlarmNum > 0)
		? SYSTME_ALARM_LED_ON
		: SYSTME_ALARM_LED_OFF;

	if (bBuzzerBeep)
	{
		int nAlarmVoiceTime = GetAlarmVoiceBuzzerBeepTime();

		if (nAlarmVoiceTime != 0)
		{
			BuzzerBeepCtrl(TRUE, nAlarmVoiceTime);
			BuzzerBeepCtrl(TRUE, nAlarmVoiceTime);
			
		}
			Light(TRUE);
			Timer_Reset(g_gvThisService.hThreadSelf,
			g_gvThisService.nTimerIDSwitchDefScreen);
			g_gvThisService.cSwitchDefScreenFlag = NEED_NOT_SWITCH_TO_DEF_SCREEN;

	}
	else
	{
		BuzzerBeepCtrl(FALSE, 0);
	}

	return TRUE;
}

static int HandleAlarmReport(MENU_DISPLAY* pMenuDisplay, 
							 GLOBAL_VAR_OF_LCD* pThis)
{
	ALARM_SIG_VALUE_EX_LCD stAlarmSigValue;

	char cAlarmOccurFlag = NO_ALARM_REPORT;	//New alarm occur
	char cAlarmClearFlag = NO_ALARM_REPORT;	

	//Add to use stop BuzzerBeep
	int nAlarmVoiceTime = GetAlarmVoiceBuzzerBeepTime();
	if (nAlarmVoiceTime == 0)
	{	
		BuzzerBeepCtrl(FALSE, 0);

	}

#define WAIT_TIME_ACTIVE_ALARM_QUEUE_GET	500		// ms. 

	while (Queue_GetCount(
		g_s_ActiveAlarmManager.hActiveAlarmReportQueue, NULL) > 0)
	{
		RunThread_Heartbeat(pThis->hThreadSelf);	//heartbeat

		//Get the alarm info from queue
		if ((Queue_Get(g_s_ActiveAlarmManager.hActiveAlarmReportQueue, 
			&stAlarmSigValue, FALSE, 
			WAIT_TIME_ACTIVE_ALARM_QUEUE_GET) == ERR_QUEUE_OK))
		{

			if ((stAlarmSigValue.nAlarmMsgType & _ALARM_OCCUR_MASK)
				== _ALARM_OCCUR_MASK)
			{
				cAlarmOccurFlag = ALARM_OCCUR_REPORT;

				g_s_ActiveAlarmManager.bAllAlarmAckFlag = FALSE;//Has new alarm 

				AddActiveAlarmToList(&stAlarmSigValue);
			}
			else if ((stAlarmSigValue.nAlarmMsgType & _ALARM_CLEAR_MASK)
				== _ALARM_CLEAR_MASK)
			{
				cAlarmClearFlag = ALARM_CLEAR_REPORT;

				DeleteActiveAlarmFromList(&stAlarmSigValue);
			}
		}
	}

	//if no alarm occur or clear
	if (cAlarmOccurFlag == NO_ALARM_REPORT 
		&& cAlarmClearFlag == NO_ALARM_REPORT)
	{
		return ERR_LCD_OK;
	}

	//Because has new alarm occur or clear, orgnize alarm menu again

	AddActiveAlarmItems(g_pActiveAlarmMenuItem);

	//Handle alarm pop
	if (cAlarmOccurFlag
		&& (pThis->cAlarmPopFlag & ALARM_DIS_STATUS) == 0
		&& (pThis->cAlarmPopFlag & ALARM_POP_STATUS) == 0)
	{

#ifdef __ACTIVE_ALARM_POP_NO_KEY_PRESSED

		//No user operate the keyboard, so pop to alarm screen
		if (pThis->cAlarmPopAtOnceFlag == NEED_ALARM_POP_AT_ONCE)
		{
			if(g_pActiveAlarmMenuItem->pvItemData != NULL)
			{
				pThis->cAlarmPopFlag = ALARM_POP_STATUS;//don't call several times

				DoModal_AlarmsScreen(pMenuDisplay,
					pThis, 
					MT_ACTIVE_ALARM_DISPLAY);

				pThis->cAlarmPopFlag = NO_ALARM_POP;

			}	

			return ID_INIT_LCD_JUST_NOW;//Use to refresh screen because of return
		}

#else

#define ENTER_INTO_ALARM_SCREEN_TIMEOUT 5000

		int nAckDlgRet;

		pThis->cAlarmPopFlag = ALARM_POP_STATUS;//don't call several times

		nAckDlgRet = DoModal_AcknowlegeDlg_ID(
			ACTIVE_ALARM_DISP_LANG_ID, 
			ENT_KEY_ACKNOWLEDGE_LANG_ID, 
			ESC_KEY_CANCEL_PROMPT_LANG_ID, 
			-1,
			ENTER_INTO_ALARM_SCREEN_TIMEOUT);


		//Prompt active alarm generate
		if( nAckDlgRet == ID_TIMEOUT_RETURN || nAckDlgRet == ID_RETURN)
		{
			if(g_pActiveAlarmMenuItem->pvItemData != NULL)
			{
				DoModal_AlarmsScreen(pMenuDisplay,
					pThis, 
					MT_ACTIVE_ALARM_DISPLAY);

			}	
		}

		pThis->cAlarmPopFlag = NO_ALARM_POP;

		return ID_INIT_LCD_JUST_NOW;//Use to refresh screen because of return

#endif

	}

	return ERR_LCD_OK;
}

SUB_MENU* GetSubMenuFromID(SUB_MENU* pSubMenu, int nSubMenuID)
{
	MENU_ITEM   *pItems, *pMenuItem;
	int			nItemNum;

	int			i;

	ASSERT(pSubMenu);

	pItems = pSubMenu->pItems;
	nItemNum = pSubMenu->nItems;

	pMenuItem = pItems;

	for (i = 0; i < nItemNum; i++, pMenuItem++)
	{
		if(pMenuItem->cMenuType == MT_SUBMENU)
		{
			SUB_MENU* pCurSubMenu = (SUB_MENU*)(pMenuItem->pvItemData);
			if(pCurSubMenu != NULL)
			{
				if (pCurSubMenu->nSubMenuID == nSubMenuID)
				{
					return pCurSubMenu;
				}
				else
				{
					SUB_MENU* pSubMenu = 
						GetSubMenuFromID(pCurSubMenu, nSubMenuID);
					if (pSubMenu != NULL)
					{
						return pSubMenu;
					}
				}
			}
		}
	}


	return NULL;
}


static MENU_ITEM* FindMenuItemByID(SUB_MENU* pSubMenu, int nMenuItemID)
{
	ASSERT(pSubMenu);

	MENU_ITEM   *pItems, *pMenuItem;
	int			nItemNum;
	int			i;

	pItems = pSubMenu->pItems;
	nItemNum = pSubMenu->nItems;

	for (i = 0; i < nItemNum; i++, pItems++)
	{
		if(pItems->nMenuItemID == nMenuItemID)
		{
			return pItems;
		}
		if(pItems->cMenuType == MT_SUBMENU)
		{
			SUB_MENU* pCurSubMenu = (SUB_MENU*)(pItems->pvItemData);
			if(pCurSubMenu != NULL)
			{
				pMenuItem = FindMenuItemByID(pCurSubMenu, nMenuItemID);
				if (pMenuItem != NULL)
				{
					return pMenuItem;
				}
			}
		}
	}

	return NULL;
}

static MENU_ITEM* FindParentMenuItemByID(SUB_MENU* pRootSubMenu, int nMenuItemID, OUT int *pItemIndex)
{
	ASSERT(pRootSubMenu);

	MENU_ITEM   *pItems, *pMenuItem;
	int			nItemNum;
	int			i, j;

	SUB_MENU* pCurSubMenu;

	pItems = pRootSubMenu->pItems;
	nItemNum = pRootSubMenu->nItems;

	for (i = 0; i < nItemNum; i++, pItems++)
	{
		if(pItems->cMenuType == MT_SUBMENU)
		{
			pCurSubMenu = (SUB_MENU*)(pItems->pvItemData);

			if(pCurSubMenu)
			{
				for(j = 0; j < pCurSubMenu->nItems; j++)
				{
					if((pCurSubMenu->pItems + j)->nMenuItemID == nMenuItemID)
					{
						*pItemIndex = j;
						return pItems;
					}
				}

				//��������Ӳ˵�û���ҵ��������¼��Ӳ˵�����
				pMenuItem = FindParentMenuItemByID(pCurSubMenu, nMenuItemID, pItemIndex);
				if(pMenuItem)
				{
					return pMenuItem;
				}
			}

		}
	}

	return NULL;
}

static int GetEquipIDThroughDeviceID(int iDeviceID)
{
	//special deal to rect 
	int nMaxDeviceNum = DXI_GetDeviceNum();
	int nProductInfoIndex = iDeviceID - nMaxDeviceNum;
	int nOffset,iEquipID;

	if((nProductInfoIndex >= 1) 
		&& (nProductInfoIndex <= LUI_PRODUCTINFO_TOTAL_NUM))
	{
		if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_RECT) 
		&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_RECT))
		{
			nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_RECT;
			iEquipID = nOffset + RECT_TWINKLE_START_EQUIPID;
		}
		else if((nProductInfoIndex >= LUI_PRODUCTINFO_START_CAN_CONVERTER) 
			&& (nProductInfoIndex <= LUI_PRODUCTINFO_END_CAN_CONVERTER))
		{
			nOffset = nProductInfoIndex - LUI_PRODUCTINFO_START_CAN_CONVERTER;
			iEquipID =  nOffset + CONVERTER_TWINKLE_START_EQUIPID;
		}

	}
	return iEquipID;

}


static BOOL RectTwinkle(SUB_MENU  *pSubMenu, BOOL bTwinkleOn)
{
	VAR_VALUE vdValue;
	int nSigType;
	int nSigID;

	static time_t	s_tmTimeout_led = 0/*time(NULL)*/;
	time_t	tmNow_led;
	tmNow_led = time(NULL);


	if(bTwinkleOn == RECT_TWINKLE_OFF)	//Close all rect twinkle
	{

		if((tmNow_led-s_tmTimeout_led)>=RECT_TWINKLE_OFF_TIMEOUT)
		{
			s_tmTimeout_led = tmNow_led;	
		}
		else
		{
			return TRUE;
		}
		vdValue.enumValue = RECT_TWINKLE_OFF;

		DXI_SPLIT_SIG_ID(SIG_ID_ALL_RECT_TWINKLE, nSigType, nSigID);

		SetSignalValueInterface(
			DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID), 
			nSigType, 
			nSigID,
			vdValue,
			SERVICE_OF_LOGIC_CONTROL,
			EQUIP_CTRL_SEND_DIRECTLY,
			0);//Don't wait
		

		DXI_SPLIT_SIG_ID(SIG_ID_ALL_CONV_TWINKLE, nSigType, nSigID);
		SetSignalValueInterface(
			DXI_GetEquipIDFromStdEquipID(CONV_GROUP_STD_EQUIP_ID), 
			nSigType, 
			nSigID,
			vdValue,
			SERVICE_OF_LOGIC_CONTROL,
			EQUIP_CTRL_SEND_DIRECTLY,
			0);//Don't wait
		
		return TRUE;
	}

	if (pSubMenu == NULL)
	{
		return TRUE;
	}

	//��Ҫ������	
	int nEquipID = -1;
	int nDeviceId;
	DEVICE_INFO*  pDeviceInfo;
	EQUIP_INFO *  pEquipInfo = NULL;
	int nBufLen;

	if(LCD_SPLIT_PREFIX(pSubMenu->nSubMenuID) == PREFIX_ACTUAL_EQUIP)
	{
		nEquipID = LCD_SPLIT_ACT_EQUIP_ID(pSubMenu->nSubMenuID);
	}
	else if(LCD_SPLIT_PREFIX(pSubMenu->nSubMenuID) == PREFIX_BARCODE_DEVICE)
	{
		nDeviceId = LCD_SPLIT_BAR_DEVICE_ID(pSubMenu->nSubMenuID);
		
		pDeviceInfo =  DXI_GetDeviceByID(nDeviceId);
		if((nEquipID = GetConvIDThruDeviceID(nDeviceId)) > 0)
		{
			//Do nothing;
		}
		else if((pDeviceInfo != NULL) && (pDeviceInfo->nRelatedEquip != 0))
		{
			pEquipInfo = pDeviceInfo->ppRelatedEquip[0];
			if(pEquipInfo)
			{
				nEquipID = pEquipInfo->iEquipID;
			}
		}
		else
		{
			nEquipID = GetEquipIDThroughDeviceID(nDeviceId);	
		}

	}
		
	if(nEquipID > 0)
	{
		pEquipInfo = LCD_GetEquipInfo(nEquipID);
		
		if(pEquipInfo != NULL)
		//if (DxiGetData(VAR_A_EQUIP_INFO,			
		//	nEquipID,	
		//	0,
		//	&nBufLen,			
		//	&pEquipInfo,			
		//	0) == ERR_DXI_OK)
		{
			if (pEquipInfo->iEquipTypeID == RECT_STD_EQUIP_ID)
			{
				vdValue.enumValue = RECT_TWINKLE_ON;

				DXI_SPLIT_SIG_ID(SIG_ID_RECT_TWINKLE, nSigType, nSigID);

				SetSignalValueInterface(nEquipID, 
					nSigType, 
					nSigID,
					vdValue,
					SERVICE_OF_LOGIC_CONTROL,
					EQUIP_CTRL_SEND_DIRECTLY,
					0);//Don't wait
			}
			else if(pEquipInfo->iEquipTypeID == CONV_STD_EQUIP_ID)
			{
				vdValue.enumValue = RECT_TWINKLE_ON;

				DXI_SPLIT_SIG_ID(SIG_ID_CONV_TWINKLE, nSigType, nSigID);

				SetSignalValueInterface(nEquipID, 
					nSigType, 
					nSigID,
					vdValue,
					SERVICE_OF_LOGIC_CONTROL,
					EQUIP_CTRL_SEND_DIRECTLY,
					0);//Don't wait
			}
		}
	}

	return TRUE;
}


static BOOL HandleRectLedTwinkle(void)
{
	static SUB_MENU* pLastSubMenu = NULL;

	SUB_MENU  *pSubMenu = g_mdMenuDisplay.pMainMenu->pCurMenu;

	if (g_gvThisService.cAlarmPopFlag)//if alarm pop or display, let the led STOP twinking
	{
		if(g_gvThisService.cDisAlarmNumFlag == YES_DIS_ALARM_NUM)
		{
			RectTwinkle(NULL, RECT_TWINKLE_OFF);
		}
		return TRUE;
	}

	//printf("HandleRectLedTwinkle ----1\n");
	if (pLastSubMenu == pSubMenu)
	{
		return TRUE;
	}

	//printf("HandleRectLedTwinkle ----2\n");
	//let all the rect twinkle off, ���������ֻҪ��ǰ�˵���old�˵����ͷ��˿���
	RectTwinkle(NULL, RECT_TWINKLE_OFF);

	pLastSubMenu = pSubMenu;

	RectTwinkle(pSubMenu, RECT_TWINKLE_ON);

	return TRUE;
}


#define SYNC_ACTIVE_ALARM_TIMEOUT	60//1min
static BOOL SyncActiveALarmList(void)
{
	static time_t	s_tmTimeout = 0/*time(NULL)*/;
	time_t	tmNow;

	BOOL bNeedOrgnizeMenuAgain = FALSE;

	tmNow = time(NULL);
	if ((tmNow - s_tmTimeout) >= SYNC_ACTIVE_ALARM_TIMEOUT)
	{
		int			nSigNum = 0;

		int			i;

		ALARM_SIG_VALUE* pAlarmSigValue;

		s_tmTimeout = tmNow;

		if (DXI_GetActiveAlarmItems(&nSigNum, 
			&pAlarmSigValue)
			== ERR_DXI_OK)
		{
			ALARM_SIG_VALUE_EX_LCD	*p = g_s_ActiveAlarmManager.pAlarmList;

			ALARM_SIG_VALUE_EX_LCD	*pPrevAlarm;	// the previuos alarm of the current.

			//Because the nAlarmMsgType will not be used,
			//Use it to save the exist flag
			while( p != NULL )
			{
				p->nAlarmMsgType = FALSE;

				p = p->next;

			}

			for (i = 0; i < nSigNum; i++)
			{
				// to check the alarm has been added or not
				pPrevAlarm = FindPreAlarmInListIncudeTime(&(pAlarmSigValue[nSigNum - i - 1]));
				if (pPrevAlarm != NULL)
				{
					p = pPrevAlarm->next;

					p->nAlarmMsgType = TRUE;
				}	
				else//if not exist, add it
				{
					ALARM_SIG_VALUE_EX_LCD stAlarmSigValue;

					memcpy(&(stAlarmSigValue.bvAlarm), 
						&(pAlarmSigValue[nSigNum - i - 1]), 
						sizeof(ALARM_SIG_VALUE));

					stAlarmSigValue.nAlarmMsgType = TRUE;

					stAlarmSigValue.bAlarmAck = FALSE;

					AddActiveAlarmToList(&stAlarmSigValue);

					bNeedOrgnizeMenuAgain = TRUE;
				}
			}

			DELETE_ITEM(pAlarmSigValue);

			pPrevAlarm = (ALARM_SIG_VALUE_EX_LCD *)&g_s_ActiveAlarmManager;
			p = pPrevAlarm->next;

			while (p != NULL)
			{
				//if it is not exist in equip mon module, delete it from list, 
				//to keep LCD module has the same alarms with equip-mon module
				if (p->nAlarmMsgType == FALSE)
				{
					pPrevAlarm->next = p->next;

					DELETE_ITEM(p);	// this alarm will be deleted.

					g_s_ActiveAlarmManager.nAlarmCount--;

					p = pPrevAlarm;

					bNeedOrgnizeMenuAgain = TRUE;

				}

				// to process the next alarm.
				pPrevAlarm = p;
				p		   = p->next;
			}

			if (bNeedOrgnizeMenuAgain)
			{
				//Because has new alarm occur or clear, orgnize alarm menu again

				AddActiveAlarmItems(g_pActiveAlarmMenuItem);

			}
		}
	}

	else if (tmNow < s_tmTimeout)
	{
		s_tmTimeout = tmNow;
	}

	return TRUE;
}

static BOOL HandleSpecRequireWhenIdle(void)
{
	ChangeEquipMenuNumToActual();//Because rect num maybe changed


	HandleRectLedTwinkle();//When a rect is viewed or ctrlled, should LED twinkle

#ifdef __SYNC_ALARM_LIST_TIMELY
	SyncActiveALarmList();//to keep LCD module has the same alarms with equip-mon module
#endif

	return TRUE;
}

/*==========================================================================*
* FUNCTION : HandleIdle
* PURPOSE  : When no key pressed, execute this function 
*			  Handle the operation of the flag changing
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:09
*==========================================================================*/
int HandleIdle(void)
{
	int nRetID = ID_OK;

	MENU_DISPLAY* pMenuDisplay	= &g_mdMenuDisplay;
	GLOBAL_VAR_OF_LCD* pThis	= &g_gvThisService;


	//Feed dog at first
	FeedDogOfLCDThread();

	if (!THREAD_IS_RUNNING(pThis->hThreadSelf))
	{
		//printf("!THREAD_IS_RUNNING(pThis->hThreadSelf)\n");
		//When alarm screen is pop up, Need change ALARM_POP_STATUS to NO_ALARM_POP
		//ensure to exit LCD thread
		pThis->cAlarmPopFlag = NO_ALARM_POP;

		return ID_GOTO_DEF_SCREEN;
	}

	//Maybe return for refreshing screen, return ID_INIT_LCD_JUST_NOW
	if (HandleAlarmReport(pMenuDisplay, pThis) != ERR_LCD_OK)
	{
		return ID_INIT_LCD_JUST_NOW;
	}

	//init lcd driver
	if (pThis->cInitLCDFlag == NEED_INIT_LCD)
	{
		int iInitRet = InitLCD();
		SetLCDRotation(GetLCDRotation());

		pThis->cInitLCDFlag = NEED_NOT_INIT_LCD;

		return  ID_INIT_LCD_JUST_NOW;
	}

	HandleSpecRequireWhenIdle();

	//����Auto Config����Ϣ
	RUN_THREAD_MSG msg;
	if(RunThread_GetMessage(RunThread_GetId(NULL), &msg, FALSE, 0) == ERR_THREAD_OK)
	{
		if(msg.dwMsg == MSG_AUTOCONFIG)
		{
			switch(msg.dwParam1)
			{
			case ID_AUTOCFG_START:
				TRACE("*****LCD*****START AUTO CONFIG!!!\n");		//FXS debug
				pThis->cAutoConfigStatus = IS_BACK_TO_DEFAULT_SCRREN;
				break;

			case ID_AUTOCFG_REFRESH_DATA:
				TRACE("*****LCD*****AUTOCFG_REFRESH_DATA!\n");		//FXS debug
				pThis->cAutoConfigStatus = OK_AUTO_CONFIG;
				break;

			default:
				break;
			}
			return ID_GOTO_DEF_SCREEN;

		}
		else if(msg.dwMsg == MSG_FIND_EQUIP)
		{
			TRACE("*****LCD***** Recv Msg: Find Equip!!!\n");		//FXS debug
			RefreshSubMenuStatus(pMenuDisplay->pMainMenu->pRootMenu, TRUE);
		}
	}
	if((pThis->cAutoConfigStatus == IS_BACK_TO_DEFAULT_SCRREN))
	{

		TRACE("*****LCD*****cAutoConfigStatus is IS_BACK_TO_DEFAULT_SCRREN and return ID_GOTO_DEF_SCREEN.\n");		//FXS debug
		return ID_GOTO_DEF_SCREEN;
	}

	//Do not occupied too long cpu time slice 
	Sleep(HANDLEIDLE_TIME_INTERVAL);

	//Switch to default screen, need to be placed on the end of the function
	if((pThis->cSwitchDefScreenFlag == NEED_SWITCH_TO_DEF_SCREEN)
		&& (pThis->cAutoConfigStatus == OK_AUTO_CONFIG))
	{
		Light(FALSE);		//close back light to protect lcd screen

		//Return to default screen
		return ID_GOTO_DEF_SCREEN;
	}


	return nRetID;
}


/*==========================================================================*
* FUNCTION : KeyPressForeOperation
* PURPOSE  : When a key is pressed, excute the following operation first.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:38
*==========================================================================*/
void KeyPressForeOperation(void)
{
	//Feed dog at first
	FeedDogOfLCDThread();

	//Open back light
	Light(TRUE);

	extern int g_nKeyPressBuzzerBeepTime;

	int nBufLen;
	SIG_BASIC_VALUE* pSigValue;
	
	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
		SIG_ID_KEYPAD_VOICE_SET,
		&nBufLen,			
		&pSigValue,			
		0) == ERR_DXI_OK)
	{
		ASSERT(pSigValue);	
		if(pSigValue->varValue.enumValue == 0)		//if keypad voice set is on ,open it. else do nothing
		{
			BuzzerBeepCtrl(TRUE, g_nKeyPressBuzzerBeepTime);
		}
		else
		{
			BuzzerBeepCtrl(FALSE, 0);
		}

	}


	if (!(g_s_ActiveAlarmManager.bAllAlarmAckFlag))//To improved efficiency, use bAllAlarmAckFlag
	{
		ALARM_SIG_VALUE_EX_LCD* p = g_s_ActiveAlarmManager.pAlarmList;

		while( p != NULL )
		{
			p->bAlarmAck = TRUE;

			p = p->next;

		}

	}

	//old buzzer beep mode
	//BuzzerBeep(TRUE);

	////Sleep time decide the buzzer sound
	//Sleep(20);

	////Close beep
	//BuzzerBeep(FALSE);

	//Reset the three timers
	Timer_Reset(g_gvThisService.hThreadSelf,
		g_gvThisService.nTimerIDAlarmPopAtOnce);
	g_gvThisService.cAlarmPopAtOnceFlag = NEED_NOT_ALARM_POP_AT_ONCE;

	Timer_Reset(g_gvThisService.hThreadSelf,
		g_gvThisService.nTimerIDSwitchDefScreen);
	g_gvThisService.cSwitchDefScreenFlag = NEED_NOT_SWITCH_TO_DEF_SCREEN;

	Timer_Reset(g_gvThisService.hThreadSelf,
		g_gvThisService.nTimerIDPasswordValid);
}


/*==========================================================================*
* FUNCTION : FeedDogOfLCDThread
* PURPOSE  : Used to feed dog of LCD thread
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 18:38
*==========================================================================*/
BOOL FeedDogOfLCDThread(void)
{
	/* feed watch dog */
	//RunThread_Heartbeat(g_gvThisService.hThreadSelf);
	RunThread_Heartbeat(RunThread_GetId(NULL));

	return TRUE;
}

/*==========================================================================*
* FUNCTION : GetLCDLangText
* PURPOSE  : get Lang Text structure by ResourceID
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int         iResourceID : key value
*			  IN int		 iLen        : Lang Text list length
*            IN LANG_TEXT  *pLangText   : Lang Text list to search
* RETURN   : LANG_TEXT * : the searched element, NULL for fail
* COMMENTS : we think the list has ascending order ..
* CREATOR  : HULONGWEN                   DATE: 2004-09-14 09:37
*==========================================================================*/
LANG_TEXT *GetLCDLangText(IN int iResourceID, 
						  IN int iLen, 
						  IN LANG_TEXT *pLangTextList)
{
	LANG_TEXT* pLangText;
	static	LANG_TEXT s_stubLangText;

	extern LANG_TEXT *GetLangText(IN int iResourceID, 
		IN int iLen, 
		IN LANG_TEXT *pLangTextList);

	pLangText = GetLangText(iResourceID, 
		iLen, 
		pLangTextList);

	if (pLangText == NULL)
	{
		pLangText = &s_stubLangText;

		s_stubLangText.iResourceID = -1;
		s_stubLangText.iMaxLenForFull = 0;
		s_stubLangText.iMaxLenForAbbr = 0;

		s_stubLangText.pFullName[0] = "";
		s_stubLangText.pFullName[1] = "";
		s_stubLangText.pAbbrName[0] = "";
		s_stubLangText.pAbbrName[1] = "";
	}

	return pLangText;
}


static ALARM_SIG_VALUE_EX_LCD*
FindPreviousAlarmInList(ALARM_SIG_VALUE_EX_LCD* pFindAlarm)
{
	ALARM_SIG_VALUE_EX_LCD	*p;				// The current timer being processed
	ALARM_SIG_VALUE_EX_LCD	*pPrevAlarm;	// the previuos timer of the current.

	pPrevAlarm = (ALARM_SIG_VALUE_EX_LCD *)&g_s_ActiveAlarmManager;
	p = pPrevAlarm->next;

	while (p != NULL)
	{
		if (p->bvAlarm.pStdSig == pFindAlarm->bvAlarm.pStdSig
			&& p->bvAlarm.pEquipInfo == pFindAlarm->bvAlarm.pEquipInfo)
		{
			return pPrevAlarm;
		}

		// to process the next timer.
		pPrevAlarm = p;
		p		   = p->next;
	}

	return NULL;
}

static ALARM_SIG_VALUE_EX_LCD*
FindPreAlarmInListIncudeTime(ALARM_SIG_VALUE* pFindAlarm)
{
	ALARM_SIG_VALUE_EX_LCD	*p;				// The current timer being processed
	ALARM_SIG_VALUE_EX_LCD	*pPrevAlarm;	// the previuos timer of the current.

	pPrevAlarm = (ALARM_SIG_VALUE_EX_LCD *)&g_s_ActiveAlarmManager;
	p = pPrevAlarm->next;

	while (p != NULL)
	{
		if (p->bvAlarm.pStdSig == pFindAlarm->pStdSig
			&& p->bvAlarm.pEquipInfo == pFindAlarm->pEquipInfo
			&& p->bvAlarm.sv.tmStartTime == pFindAlarm->sv.tmStartTime)
		{
			return pPrevAlarm;
		}

		// to process the next timer.
		pPrevAlarm = p;
		p		   = p->next;
	}

	return NULL;
}

static int AddActiveAlarmToList(ALARM_SIG_VALUE_EX_LCD* pAddAlarm)
{
	int nError = ERR_LCD_OK;

	if (!ACTIVE_ALARM_LIST_INITIALIZED())
	{
		InitActiveAlarmManager();
	}

	if (FindPreviousAlarmInList(pAddAlarm) == NULL)
	{
		ALARM_SIG_VALUE_EX_LCD*	p = NEW(ALARM_SIG_VALUE_EX_LCD, 1);

		if(p)
		{
			memcpy(p, pAddAlarm, sizeof(ALARM_SIG_VALUE_EX_LCD));
			// add the alarm to the head of the manager
			p->next	= g_s_ActiveAlarmManager.pAlarmList;

			g_s_ActiveAlarmManager.pAlarmList	= p;

			g_s_ActiveAlarmManager.nAlarmCount++;

			nError = ERR_LCD_OK;
		}
		else
		{
			TRACE("[Timer_Set] -- Out of memory on adding alarm.\n");
			nError = ERR_LCD_ADD_ACTIVE_ALARM_FAILURE;
		}
	}
	return nError;
}

static int DeleteActiveAlarmFromList(ALARM_SIG_VALUE_EX_LCD* pDelAlarm)
{
	ALARM_SIG_VALUE_EX_LCD	*p;				// The current alarm being processed
	ALARM_SIG_VALUE_EX_LCD	*pPrevAlarm;	// the previuos alarm of the current.


	// to check the alarm has been added or not
	pPrevAlarm = FindPreviousAlarmInList(pDelAlarm);
	if (pPrevAlarm != NULL)
	{
		p                = pPrevAlarm->next;	// remove from link
		pPrevAlarm->next = p->next;

		DELETE_ITEM(p);	// this alarm will be deleted.

		g_s_ActiveAlarmManager.nAlarmCount--;
	}

	return (pPrevAlarm != NULL) ? ERR_LCD_OK: ERR_LCD_ACTIVE_ALARM_NOT_FOUND;
}


static int InitActiveAlarmManager(void)
{
	int nError = ERR_LCD_OK;

	int			nSigNum = 0;
	int			i;

	ALARM_SIG_VALUE_EX_LCD	stAlarmSigValue;

	ALARM_SIG_VALUE* pAlarmSigValue;

	if(!ACTIVE_ALARM_LIST_INITIALIZED()) 
	{
		// 1. init the trap send queue
		g_s_ActiveAlarmManager.hActiveAlarmReportQueue = Queue_Create(
			MAX_PENDING_ACTIVE_ALARM_REPORT,
			sizeof(ALARM_SIG_VALUE_EX_LCD),
			5);

		if (g_s_ActiveAlarmManager.hActiveAlarmReportQueue == NULL)
		{
			AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
				"Fails on creating the alarm report queue.\n");

			return ERR_LCD_QUEUE_CREATE_FAILURE;

		}
	}

	g_s_ActiveAlarmManager.nAlarmCount	    = 0;

	g_s_ActiveAlarmManager.pAlarmList	    = NULL;

	if (ERR_DXI_OK == DXI_GetActiveAlarmItems(&nSigNum, &pAlarmSigValue))
	{
		for (i = 0; i < nSigNum; i++)
		{
			memcpy(&(stAlarmSigValue.bvAlarm), 
				&(pAlarmSigValue[nSigNum - i - 1]), 
				sizeof(ALARM_SIG_VALUE));

			stAlarmSigValue.nAlarmMsgType = _ALARM_OCCUR_MASK;

			stAlarmSigValue.bAlarmAck = FALSE;

			//Put the report info to alarm queue
			Queue_Put(g_s_ActiveAlarmManager.hActiveAlarmReportQueue,
				&stAlarmSigValue, FALSE);

		}

		DELETE_ITEM(pAlarmSigValue);
	}


	return nError;
}

static int DestroyActiveAlarmManager(void)
{
	int nError = ERR_LCD_OK;

	ALARM_SIG_VALUE_EX_LCD	*p, *t;	

	p = g_s_ActiveAlarmManager.pAlarmList;

	while( p != NULL )
	{
		t = p;

		p = p->next;

		DELETE_ITEM(t);
	}

	g_s_ActiveAlarmManager.pAlarmList  = NULL;
	g_s_ActiveAlarmManager.nAlarmCount = -1;	// not initialized flag

	if (g_s_ActiveAlarmManager.hActiveAlarmReportQueue != NULL)
	{
		Queue_Destroy(g_s_ActiveAlarmManager.hActiveAlarmReportQueue);

		g_s_ActiveAlarmManager.hActiveAlarmReportQueue = NULL;

	}

	return nError;
}


UCHAR GetKeyEx(void)
{
	UCHAR byKeyValue;

	static UCHAR s_LastKey = VK_NO_KEY;
	static int	s_iSameKeyCount = 0;
	static int	s_iNoKeyCount = 0;
	GLOBAL_VAR_OF_LCD *pThis = &g_gvThisService;

	int nInterfaceType = VAR_LCDKEY_THRUYDN23;		
	int nVarID = 0;
	int nVarSubID = 0;
	int nBufLen;
	int nTimeOut = 0;

	Sleep(GETKEY_TIME_INTERVAL);
	
	byKeyValue = GetKey();

	if (byKeyValue != VK_NO_KEY)
	{
		s_iNoKeyCount = 0;

		if(byKeyValue == s_LastKey)
		{
			s_iSameKeyCount++;
		}
		else
		{
			s_LastKey = byKeyValue;
			s_iSameKeyCount = 0;
		}

		if(s_iSameKeyCount > 20)
		{
			pThis->nValueStepMultiple = 10;
		}
		else if(s_iSameKeyCount > 10)
		{
			pThis->nValueStepMultiple = 2;
		}
		else
		{
			pThis->nValueStepMultiple = 1;
		}

		//TRACE("nValueStepMultiple = %d.\n", pThis->nValueStepMultiple);

		return byKeyValue;
	}
	else
	{

		if (ERR_DXI_OK == DxiGetData(nInterfaceType,
				nVarID,			
				nVarSubID,		
				&nBufLen,			
				&byKeyValue,			
				nTimeOut))
		{
			if (byKeyValue != VK_NO_KEY)
			{
				return byKeyValue;
			}
		}
	}


	s_iNoKeyCount++;

	//�˲�
	if(s_iNoKeyCount > 3)
	{
		s_LastKey = VK_NO_KEY;
		s_iSameKeyCount = 0;
		pThis->nValueStepMultiple = 1;
	}

	return VK_NO_KEY;
}



#define	VAR_VALUE_LEN	10
#define	LCD_VAR_FILE_NAME	"/home/lcd_bright.log"

static int ReadLcdVar(char *pVarName)
{
	UNUSED(pVarName);

	int iVarFd;

	char sbuf[VAR_VALUE_LEN];
	memset(sbuf, 0, VAR_VALUE_LEN);

	iVarFd = open(LCD_VAR_FILE_NAME, O_RDONLY | O_CREAT);
	if(iVarFd <=0)
	{
		TRACE("failed to open %s!\n", LCD_VAR_FILE_NAME);
	}
	read(iVarFd, sbuf, VAR_VALUE_LEN);
	close (iVarFd);

	int	nValue = -1;
	sscanf(sbuf, "%d", &nValue);

	TRACE("LCD var value is %d.\n", nValue);

	return nValue;
}


int WriteLcdVar(char *pVarName, int nValue)
{
	UNUSED(pVarName);

	int iVarFd;

	iVarFd = open(LCD_VAR_FILE_NAME, O_WRONLY | O_CREAT);
	if(iVarFd <=0)
	{
		TRACE("failed to open %s!\n", LCD_VAR_FILE_NAME);
	}

	char sbuf[VAR_VALUE_LEN];
	snprintf(sbuf, sizeof(sbuf), "%d\r\n", nValue);

	write(iVarFd, sbuf, VAR_VALUE_LEN);
	close (iVarFd);

	return ERR_LCD_OK;
}



#define SYS_SITE_ID_SET_ID_DIS_FORMAT	"3"

#define SYS_LANGUAGE_SET_ID_DIS_FORMAT	"6"
#define SYS_TIME_ZONE_SET_ID_DIS_FORMAT "9"
#define SYS_DATE_SET_ID_DIS_FORMAT		"10"
#define SYS_TIME_SET_ID_DIS_FORMAT		"8"
#define SYS_RELOAD_DEF_CONFIG_SET_ID_DIS_FORMAT	"4"

/*==========================================================================*
* FUNCTION : AddSysSetItems
* PURPOSE  : Add system param set items to param set sub menu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*  pEquipItem : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-04 19:08
*==========================================================================*/
static int AddSysSetItems(MENU_ITEM* pEquipItem)
{
	int nError = ERR_LCD_OK;

	static SECTION_DATA_STRU	s_SectionDataStru_DATE[3] =
	{
		DEF_SECTION_DATA_STRU(1970, 1970, 2038, 1, "04", '-'),
			DEF_SECTION_DATA_STRU(01, 01, 12, 1, "02", '-'),
			DEF_SECTION_DATA_STRU(01, 01, 31, 1, "02", '\0'),
	};

	static SECTION_DATA_STRU	s_SectionDataStru_TIME[3] =
	{
		DEF_SECTION_DATA_STRU(00, 00, 23, 1, "02", ':'),
			DEF_SECTION_DATA_STRU(00, 00, 59, 1, "02", ':'),
			DEF_SECTION_DATA_STRU(00, 00, 59, 1, "02", '\0'),
	};

	static SELF_DEFINE_INFO s_SystemSetItemInfo[] = 
	{

#ifndef GET_PART_LANG_INFO_FROM_SYS_RES
		//Language set
		DEF_SELF_DEFINE_INFO(SYS_LANGUAGE_SET_ID, 
			NULL,
			VAR_ENUM, 
			BROWSER_LEVEL, 
			SYS_LANGUAGE_SET_ID_DIS_FORMAT, 
			1, 
			SYS_LANG_NUM_ONE_TIME - 1,
			0,
			SYS_LANG_NUM_ONE_TIME,
			s_SysLangSetTextArray,
			NULL),
#else
		//Language set
		DEF_SELF_DEFINE_INFO(SYS_LANGUAGE_SET_ID, 
			NULL,
			VAR_ENUM, 
			BROWSER_LEVEL, 
			SYS_LANGUAGE_SET_ID_DIS_FORMAT, 
			1, 
			SYS_LANG_NUM_ONE_TIME - 1,
			0,
			SYS_LANG_NUM_ONE_TIME,
			NULL,
			NULL),
#endif
		////ACU Time Zone
		//DEF_SELF_DEFINE_INFO(SYS_TIME_ZONE_SET_ID, 
		//	NULL,
		//	VAR_LONG, 
		//	OPERATOR_LEVEL, 
		//	SYS_TIME_ZONE_SET_ID_DIS_FORMAT, 
		//	1, 
		//	26,
		//	-24,
		//	0,
		//	NULL,
		//	NULL),

		//ACU Date
		DEF_SELF_DEFINE_INFO(SYS_DATE_SET_ID, 
			NULL,
			VAR_MULTI_SECT, 
			OPERATOR_LEVEL, 
			SYS_DATE_SET_ID_DIS_FORMAT, 
			1, 
			2,
			0,
			sizeof(s_SectionDataStru_DATE)/sizeof(SECTION_DATA_STRU),
			NULL,
			s_SectionDataStru_DATE),

		//ACU Time
		DEF_SELF_DEFINE_INFO(SYS_TIME_SET_ID, 
			NULL,
			VAR_MULTI_SECT, 
			OPERATOR_LEVEL, 
			SYS_TIME_SET_ID_DIS_FORMAT, 
			1, 
			2,
			0,
			sizeof(s_SectionDataStru_TIME)/sizeof(SECTION_DATA_STRU),
			NULL,
			s_SectionDataStru_TIME),

		//Keypad voice enabled
		DEF_SELF_DEFINE_INFO(KEYPAD_VOICE_SET_ID,
			NULL,
			VAR_ENUM,
			OPERATOR_LEVEL,
			SYS_KEYPAD_VOICE_SET_ID_FORMAT,
			1,
			1,
			0,
			2,
			NULL,
			NULL),
#if 0//Frank Wu, for mini NCU
#ifdef CHANGE_LCD_HEIGHT
		//Change the LCD height between 128*64 and 128*128
		DEF_SELF_DEFINE_INFO(CHANGE_LCD_HEIGHT_SET_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			"", 
			1, 
			1,
			0,
			2,
			NULL,
			NULL),

#endif

#ifdef SET_LCD_ROTATION
		//Change the LCD rotation:0, 90, 180, 270 deg
		DEF_SELF_DEFINE_INFO(SET_LCD_ROTATION_SET_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			"", 
			1, 
			3,
			0,
			4,
			NULL,
			NULL),

#endif
#endif
		//Reload default config
		DEF_SELF_DEFINE_INFO(RELOAD_DEF_CONFIG_SET_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			SYS_RELOAD_DEF_CONFIG_SET_ID_DIS_FORMAT, 
			0, 
			0,
			0,
			1,
			NULL,
			NULL),
#if 0//Frank Wu, for mini NCU
		//Download config file
		DEF_SELF_DEFINE_INFO(DOWNLOAD_CONFIG_LANG_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			"", 
			0, 
			0,
			0,
			1,
			NULL,
			NULL),
#endif
		//Auto Config
		DEF_SELF_DEFINE_INFO(AUTO_CONFIG_LANG_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			"", 
			0, 
			0,
			0,
			1,
			NULL,
			NULL),
		//clear history alarm
		DEF_SELF_DEFINE_INFO(CLEAR_HIS_ALARM_CTRL_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			"", 
			0, 
			0,
			0,
			1,
			NULL,
			NULL),
	};


#define SYSTEM_SET_ITEM_NUMBER	(sizeof(s_SystemSetItemInfo)/sizeof(SELF_DEFINE_INFO))

	//Previous system set items number
	int			nSigNum = SYSTEM_SET_ITEM_NUMBER;

	int			i;


	SUB_MENU*	pSubMenu;
	MENU_ITEM*	pSignalItem;
	int			*pDispItems;

	char		cMenuType = MT_SELF_DEF_SET;


	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pSignalItem = NEW(MENU_ITEM, nSigNum);
	LOGOUT_NO_MEMORY(pSignalItem);
	ZERO_POBJS(pSignalItem, nSigNum);

	pDispItems = NEW(int, nSigNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nSigNum);

	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_FIXED_LANG_ID(SYSTEM_SET_LANG_ID), 
		nSigNum,
		pSignalItem,
		pDispItems);

	for(i = 0; i < nSigNum && pSignalItem != NULL;
		i++, pSignalItem++)
	{
		int nAckFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE;

		if (s_SystemSetItemInfo[i].iSigID == SYS_IP_ADDR_SET_ID
			|| s_SystemSetItemInfo[i].iSigID == SYS_NETMASK_SET_ID
			|| s_SystemSetItemInfo[i].iSigID == SYS_GATEWAY_SET_ID
			//|| s_SystemSetItemInfo[i].iSigID == RELOAD_DEF_CONFIG_SET_ID
			|| s_SystemSetItemInfo[i].iSigID == DOWNLOAD_CONFIG_LANG_ID
			|| s_SystemSetItemInfo[i].iSigID == CLEAR_HIS_ALARM_CTRL_ID
			/*|| s_SystemSetItemInfo[i].iSigID == AUTO_CONFIG_LANG_ID*/)
		{
			nAckFlag = MENU_ITEM_NEED_ACKNOWLEDGE;
		}

		//Add each set item
		INIT_SIGNAL_ITEM(pSignalItem, 
			cMenuType,
			LCD_MERGE_FIXED_LANG_ID(s_SystemSetItemInfo[i].iSigID),
			MENU_ITEM_NEED_VALIDATE, 
			nAckFlag,
			&(s_SystemSetItemInfo[i]));
	}

	INIT_SUBMENU_ITEM(pEquipItem, 
		LCD_MERGE_FIXED_LANG_ID(SYSTEM_SET_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	return nError;
}

//�����´���ŵ��������Ӱ��IDE���Զ���ȫ����

/*==========================================================================*
* FUNCTION : AddCommSetItems
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*  pEquipItem : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2006-11-03 16:03
*==========================================================================*/
static int AddCommSetItems(MENU_ITEM* pEquipItem)
{
	int nError = ERR_LCD_OK;

	static SECTION_DATA_STRU	s_SectionDataStru_IPADDR[4] =
	{
		DEF_SECTION_DATA_STRU(192, 1, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(168, 0, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(192, 0, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(2, 1, 255, 1, "1", '\0'),
	};

	static SECTION_DATA_STRU	s_SectionDataStru_NETMASK[4] =
	{
		DEF_SECTION_DATA_STRU(255, 1, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(255, 0, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(255, 0, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(0, 0, 255, 1, "1", '\0'),
	};

	static SECTION_DATA_STRU	s_SectionDataStru_GATEWAY[4] =
	{
		DEF_SECTION_DATA_STRU(192, 1, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(168, 0, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(192, 0, 255, 1, "1", '.'),
			DEF_SECTION_DATA_STRU(1, 1, 255, 1, "1", '\0'),
	};


#define SYS_IP_ADDR_SET_ID_DIS_FORMAT	"8"
#define SYS_NETMASK_SET_ID_DIS_FORMAT	"8"
#define SYS_GATEWAY_SET_ID_DIS_FORMAT	"8"
	static SELF_DEFINE_INFO s_YND23SetItemInfo[] = 
	{

		//DHCP
		DEF_SELF_DEFINE_INFO(DHCP_SET_LANG_ID, 
			NULL,
			VAR_ENUM, 
			ENGINEER_LEVEL, 
			"", 
			1, 
			1,
			0,
			2,
			NULL,
			NULL),

		//IP address
		DEF_SELF_DEFINE_INFO(SYS_IP_ADDR_SET_ID, 
			NULL,
			VAR_MULTI_SECT, 
			ENGINEER_LEVEL, 
			SYS_IP_ADDR_SET_ID_DIS_FORMAT, 
			1, 
			3,
			0,
			sizeof(s_SectionDataStru_IPADDR)/sizeof(SECTION_DATA_STRU),
			NULL,
			s_SectionDataStru_IPADDR),

		//Netmask
		DEF_SELF_DEFINE_INFO(SYS_NETMASK_SET_ID, 
			NULL,
			VAR_MULTI_SECT, 
			ENGINEER_LEVEL, 
			SYS_NETMASK_SET_ID_DIS_FORMAT, 
			1, 
			3,
			0,
			sizeof(s_SectionDataStru_NETMASK)/sizeof(SECTION_DATA_STRU),
			NULL,
			s_SectionDataStru_NETMASK),

		//Gateway
		DEF_SELF_DEFINE_INFO(SYS_GATEWAY_SET_ID, 
			NULL,
			VAR_MULTI_SECT, 
			ENGINEER_LEVEL, 
			SYS_GATEWAY_SET_ID_DIS_FORMAT, 
			1, 
			3,
			0,
			sizeof(s_SectionDataStru_GATEWAY)/sizeof(SECTION_DATA_STRU),
			NULL,
			s_SectionDataStru_GATEWAY),
#if 0 //Frank Wu, for mini NCU

			//Self Address
			DEF_SELF_DEFINE_INFO(SYS_SITE_ID_SET_ID, 
				NULL,
				VAR_LONG, 
				ENGINEER_LEVEL, 
				"3", 
				1, 
				255,
				1,
				0,
				NULL,
				NULL),

			//CNT mode
			DEF_SELF_DEFINE_INFO(YDN_PORT_MODE_SET_ID, 
				NULL,
				VAR_ENUM, 
				ENGINEER_LEVEL, 
				"8", 
				1, 
				2,
				0,
				YDN23_PORT_TYPE_NUM,
				NULL,
				NULL),

			//Port Para
			DEF_SELF_DEFINE_INFO(YDN_PORT_PARAM_SET_ID, 
				NULL,
				VAR_ENUM, 
				ENGINEER_LEVEL, 
				"12", 
				1, 
				5,
				0,
				YDN23_PORT_PARAM_NUM,
				NULL,
				NULL),

			//Call back Enable
			DEF_SELF_DEFINE_INFO(YDN_CALL_BACK_ENB_SET_ID, 
				NULL,
				VAR_ENUM, 
				ENGINEER_LEVEL, 
				"3", 
				1, 
				1,
				0,
				2,
				NULL,
				NULL),

			//Call back times
			DEF_SELF_DEFINE_INFO(YDN_CALL_BACK_NUM_SET_ID, 
				NULL,
				VAR_UNSIGNED_LONG, 
				ENGINEER_LEVEL, 
				"3", 
				1, 
				5,
				0,
				0,
				NULL,
				NULL),

			//Call back interval
			DEF_SELF_DEFINE_INFO(YDN_CALL_BACK_INTERVAL_SET_ID, 
				NULL,
				VAR_UNSIGNED_LONG, 
				ENGINEER_LEVEL, 
				"3", 
				1, 
				300,
				0,
				0,
				NULL,
				NULL),
			//Call back Phone No1
			DEF_SELF_DEFINE_INFO(YDN_PHONE_NO1_SET_ID, 
				NULL,
				VAR_PHONE_NO, 
				ENGINEER_LEVEL, 
				"8", 
				1, 
				0,
				0,
				0,
				NULL,
				NULL),

			//Call back Phone No2
			DEF_SELF_DEFINE_INFO(YDN_PHONE_NO2_SET_ID, 
				NULL,
				VAR_PHONE_NO, 
				ENGINEER_LEVEL, 
				"8", 
				1, 
				0,
				0,
				0,
				NULL,
				NULL),

			//Call back Phone No3
			DEF_SELF_DEFINE_INFO(YDN_PHONE_NO3_SET_ID, 
				NULL,
				VAR_PHONE_NO, 
				ENGINEER_LEVEL, 
				"8", 
				1, 
				0,
				0,
				0,
				NULL,
				NULL),
#endif
	};

#define YDN_SET_ITEM_NUMBER		(sizeof(s_YND23SetItemInfo)/sizeof(SELF_DEFINE_INFO))

	//Previous system set items number
	int			nSigNum = YDN_SET_ITEM_NUMBER;
	int			i;


	SUB_MENU*	pSubMenu;
	MENU_ITEM*	pSignalItem;
	int			*pDispItems;

	pSubMenu = NEW(SUB_MENU, 1);
	LOGOUT_NO_MEMORY(pSubMenu);
	ZERO_POBJS(pSubMenu, 1);

	pSignalItem = NEW(MENU_ITEM, nSigNum);
	LOGOUT_NO_MEMORY(pSignalItem);
	ZERO_POBJS(pSignalItem, nSigNum);

	pDispItems = NEW(int, nSigNum);
	LOGOUT_NO_MEMORY(pDispItems);
	ZERO_POBJS(pDispItems, nSigNum);

	INIT_SUB_MENU(pSubMenu,
		LCD_MERGE_FIXED_LANG_ID(COMMUNICATION_SET_LANG_ID), 
		nSigNum,
		pSignalItem,
		pDispItems);

	//Assign the equip ID, not standard equip ID
	INIT_SUBMENU_ITEM(pEquipItem, 
		LCD_MERGE_FIXED_LANG_ID(COMMUNICATION_SET_LANG_ID), 
		MENU_ITEM_NEED_NOT_VALIDATE, 
		MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
		pSubMenu);	

	for(i = 0; 
		i < nSigNum && pSignalItem != NULL;
		i++, pSignalItem++)
	{
		//Add each set item
		INIT_SIGNAL_ITEM(pSignalItem, 
			MT_SELF_DEF_SET,
			LCD_MERGE_FIXED_LANG_ID(s_YND23SetItemInfo[i].iSigID),
			MENU_ITEM_NEED_VALIDATE, 
			MENU_ITEM_NEED_NOT_ACKNOWLEDGE,
			&(s_YND23SetItemInfo[i]));
	}

	return nError;
}










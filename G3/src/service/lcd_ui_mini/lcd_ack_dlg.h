/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_ack_dlg.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:55
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_ACK_DLG_H__041008
#define __LCD_ACK_DLG_H__041008

#ifdef  __cplusplus
extern "C" {
#endif


/*==========================================================================*
* FUNCTION : DoModal_AcknowlegeDlg_ID
* PURPOSE  : Display prompt info and acknowledge info,
*			  support sleep to timeout and key press quit modes
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  nHeadingInfoID : The first line info ID
*            int  nPromptInfoID1 : The second line info ID
*            int  nPromptInfoID2 : The third line info ID
*            int  nPromptInfoID3 : The fourth line info ID
*            int  nTimeout       : Sleep to timeout, then quit
* RETURN   : int : 
* COMMENTS : if ID == -1, Do not display
* CREATOR  : HULONGWEN                DATE: 2004-12-07 14:40
*==========================================================================*/
int DoModal_AcknowlegeDlg_ID(int nHeadingInfoID, 
							 int nPromptInfoID1, 
							 int nPromptInfoID2,
							 int nPromptInfoID3,
							 int nTimeout);

/*==========================================================================*
* FUNCTION : DoModal_AcknowlegeDlg_INFO
* PURPOSE  : Display prompt info and acknowledge info,
*			  support sleep to timeout and key press quit modes
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char*  pHeadingInfo : The first line info
*            char*  pPromptInfo1 : The second line info
*            char*  nPromptInfo2 : The third line info
*            char*  nPromptInfo3 : The fourth line info
*            int    nTimeout     : Sleep to timeout, then quit, 
*									if -1, key press quit mode
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2004-12-07 14:33
*==========================================================================*/
int DoModal_AcknowlegeDlg_INFO(char* pHeadingInfo, 
							   char* pPromptInfo1, 
							   char* nPromptInfo2,
							   char* nPromptInfo3,
							   int	nTimeout);

extern int DoModal_AcknowlegeDlg_ID_ByLang(int nHeadingInfoID, 
										   int nPromptInfoID1, 
										   int nPromptInfoID2,
										   int nPromptInfoID3,
										   int nTimeout,
										   char *pszLangCode,
										   BOOL bJustOneLine,
										   BOOL bNeedProcessEvent);


extern int DoModal_AcknowlegeDlg_INFO_ByLang(char* pHeadingInfo, 
											 char* pPromptInfo1, 
											 char* nPromptInfo2,
											 char* nPromptInfo3,
											 int	nTimeout,
											 char *pszLangCode,
											 BOOL bJustOneLine,
											 BOOL bNeedProcessEvent);


extern void PromptSysIsRebooting(void);
extern void PromptAppClosed(void);


#ifdef __cplusplus
}
#endif

#endif //__LCD_ACK_DLG_H__041008

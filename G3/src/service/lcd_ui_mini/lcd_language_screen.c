/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_language_screen.c
*  CREATOR  : HULONGWEN                DATE: 2005-01-20 16:37
*  VERSION  : V1.00
*  PURPOSE  : ACU language select
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include "lcd_public.h"
#include "lcd_language_screen.h"
#include "lcd_main.h"

//#include "lcd_item_handle.h"
#ifdef _DEBUG
//#define		_DEBUG_LANG_SCREEN		1
#endif
extern int DoModal_DefaultScreen(MENU_DISPLAY* pMenuDisplay,
								 GLOBAL_VAR_OF_LCD* pThis);

extern int DoModal_AcknowlegeDlg_INFO(char* pHeadingInfo, 
									  char* pPromptInfo1, 
									  char* nPromptInfo2,
									  char* nPromptInfo3,
									  int	nTimeout);


//Frank Wu,20160127, for MiniNCU
//#define LANGUAGE_SCREEN_TIMEOUT		(30)//30s
#define LANGUAGE_SCREEN_TIMEOUT			(INTERVAL_SCREEN_TIMEOUT/1000)//ms->s

static LANG_SEL_SCREEN s_LangSelScreenInfo;


//Frank Wu,20160127, for MiniNCU
static BOOL SetCurrentLangMiniNCU(LANG_SEL_SCREEN* pLangSelInfo, BOOL bOnlyChangeFlag)
{
#define ENTER_TO_REBOOT_TIMEOUT 5000

	int							nCurrentLangValue = pLangSelInfo->nCurItemForAllLang;
	LANG_SUPPORT_TABLE_ITEM*	psLangSupportTableList = (LANG_SUPPORT_TABLE_ITEM*)pLangSelInfo->pvLangInfoForAllLang;
	LANG_SUPPORT_TABLE_ITEM*	psLangSupportTableItem = NULL;
	char*						pszPrompt1 = "Acknowledge Info";
	char*						pszPrompt2 = "Reboot Validate";
	BOOL						bJustOneLine = TRUE;


	if((ENGLISH_LANGUAGE_FLAG == nCurrentLangValue)
		|| (LOCAL_LANGUAGE_FLAG == nCurrentLangValue))
	{
		return SetCurrentLang(nCurrentLangValue, bOnlyChangeFlag);
	}
	else if(nCurrentLangValue < pLangSelInfo->nLangNumForAllLang)
	{

		psLangSupportTableItem = &psLangSupportTableList[nCurrentLangValue];

		if(('\0' != psLangSupportTableItem->szLocalPrompt1[0])
			|| ('\0' != psLangSupportTableItem->szLocalPrompt2[0]))
		{
			pszPrompt1 = psLangSupportTableItem->szLocalPrompt1;
			pszPrompt2 = psLangSupportTableItem->szLocalPrompt2;
			bJustOneLine = FALSE;
		}

		if(ID_RETURN == DoModal_AcknowlegeDlg_INFO_ByLang(pszPrompt1, pszPrompt2, "", "",
							ENTER_TO_REBOOT_TIMEOUT,
							psLangSupportTableItem->pszLangCode,
							bJustOneLine,
							FALSE))
		{
			PromptSysIsRebooting();
			
			DXI_ChangeLanguage(psLangSupportTableItem->pszLangCode);

			Sleep(ONE_HOUR_MSECOND);
		}
	}

	return FALSE;
}



/*==========================================================================*
 * FUNCTION : LoadLangSelScreenInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: LANG_SEL_SCREEN*  pLangSelInfo : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  :                  DATE: 2006-06-13 08:52
 *==========================================================================*/
static BOOL LoadLangSelScreenInfo(LANG_SEL_SCREEN* pLangSelInfo, GLOBAL_VAR_OF_LCD* pThis) 
{
	int nBufLen;
	int nError = ERR_LCD_OK;

	void* pSigInfo;

	ASSERT(pLangSelInfo);

	memset(pLangSelInfo, 0, sizeof(LANG_SEL_SCREEN));

	pLangSelInfo->nLangEquipID = 
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);

	DXI_SPLIT_SIG_ID(SIG_ID_LANGUAGE_SUPPORT, 
		pLangSelInfo->nLangSigType, 
		pLangSelInfo->nLangSigID);

	nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,			
			pLangSelInfo->nLangEquipID,	
			SIG_ID_LANGUAGE_SUPPORT,
			&nBufLen,			
			&pSigInfo,			
			0);

	if (nError == ERR_DXI_OK)
	{
		SIG_BASIC_VALUE*	pSigValue;

		ASSERT(pSigInfo);

		pLangSelInfo->nLangNum		= ((SET_SIG_INFO*)pSigInfo)->iStateNum;
		pLangSelInfo->ppLangText	= ((SET_SIG_INFO*)pSigInfo)->pStateText;

#ifdef _DEBUG_LANG_SCREEN
		//printf("pLangSelInfo->nLangEquipID is %d\n", pLangSelInfo->nLangEquipID);
		//printf("Current Lang Num is %d\n", pLangSelInfo->nLangNum);				
		//printf("pLangSelInfo->nLangSigType is %d\n", pLangSelInfo->nLangSigType);
		//printf("pLangSelInfo->nLangSigID is %d\n", pLangSelInfo->nLangSigID);
#endif

		if (DxiGetData(VAR_A_SIGNAL_VALUE,
			pLangSelInfo->nLangEquipID,	
			SIG_ID_LANGUAGE_SUPPORT,
			&nBufLen,			
			&pSigValue,			
			0) == ERR_DXI_OK)
		{
			pLangSelInfo->nCurItem = pSigValue->varValue.enumValue;

			SetCurrentLang(pSigValue->varValue.enumValue, TRUE);

			//TRACE("\n\nSetCurrentLang is %d\n\n",pSigValue->varValue.enumValue);

#ifdef _DEBUG_LANG_SCREEN
		//printf("pLangSelInfo->nCurItem is %d\n\n", pLangSelInfo->nCurItem);			
#endif
		}
		else
		{
			return FALSE;
		}

	}
	else
	{
		return FALSE;
	}
	
	return TRUE;
}


//Frank Wu,20160127, for MiniNCU
static BOOL LoadLangSelScreenInfoMiniNCU(LANG_SEL_SCREEN* pLangSelInfo, GLOBAL_VAR_OF_LCD* pThis) 
{
	int nBufLen;
	int nError = ERR_LCD_OK;

	void* pSigInfo;

	ASSERT(pLangSelInfo);

	memset(pLangSelInfo, 0, sizeof(LANG_SEL_SCREEN));

	pLangSelInfo->nLangEquipID = 
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);

	DXI_SPLIT_SIG_ID(SIG_ID_LANGUAGE_SUPPORT, 
		pLangSelInfo->nLangSigType, 
		pLangSelInfo->nLangSigID);

	nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,			
		pLangSelInfo->nLangEquipID,	
		SIG_ID_LANGUAGE_SUPPORT,
		&nBufLen,			
		&pSigInfo,			
		0);

	if (nError == ERR_DXI_OK)
	{
		SIG_BASIC_VALUE*	pSigValue;

		ASSERT(pSigInfo);

		pLangSelInfo->nLangNum		= ((SET_SIG_INFO*)pSigInfo)->iStateNum;
		pLangSelInfo->ppLangText	= ((SET_SIG_INFO*)pSigInfo)->pStateText;

#ifdef _DEBUG_LANG_SCREEN
		//printf("pLangSelInfo->nLangEquipID is %d\n", pLangSelInfo->nLangEquipID);
		//printf("Current Lang Num is %d\n", pLangSelInfo->nLangNum);				
		//printf("pLangSelInfo->nLangSigType is %d\n", pLangSelInfo->nLangSigType);
		//printf("pLangSelInfo->nLangSigID is %d\n", pLangSelInfo->nLangSigID);
#endif

		if (DxiGetData(VAR_A_SIGNAL_VALUE,
			pLangSelInfo->nLangEquipID,	
			SIG_ID_LANGUAGE_SUPPORT,
			&nBufLen,			
			&pSigValue,			
			0) == ERR_DXI_OK)
		{
			pLangSelInfo->nCurItem = pSigValue->varValue.enumValue;

			//printf("LoadLangSelScreenInfoMiniNCU--1----:pLangSelInfo->nCurItem =%d~~~~~~~~~~~\n",pLangSelInfo->nCurItem);

			SetCurrentLang(pSigValue->varValue.enumValue, TRUE);

			//printf("LoadLangSelScreenInfoMiniNCU--2--:pLangSelInfo->nCurItem =%d~~~~~~~~~~~\n",pLangSelInfo->nCurItem);
		}
		else
		{
			return FALSE;
		}

	}
	else
	{
		return FALSE;
	}

	extern LCD_PRIVATE_CFG_LOADER	g_stLcdPrivateCfgLoader;

	pLangSelInfo->nCurItemForAllLang = pLangSelInfo->nCurItem;
	//pLangSelInfo->nLangNumForAllLang = g_stLcdPrivateCfgLoader.stLCDLangSupportTable.nLangSupportNum-2;  �����2��ʵ����ѧ
	pLangSelInfo->nLangNumForAllLang = g_stLcdPrivateCfgLoader.stLCDLangSupportTable.nLangSupportNum;//Fengel 2018-5-16
	pLangSelInfo->pvLangInfoForAllLang = (void *)g_stLcdPrivateCfgLoader.stLCDLangSupportTable.staLangSupportTableItem;

	return TRUE;
}


#define FONT_HEIGHT_COMPROMISE 16//����

/*==========================================================================*
 * FUNCTION : DisplayLanguageScreen
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: LANG_SEL_SCREEN*  pLangSelInfo : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  :                  DATE: 2006-06-13 09:11
 *==========================================================================*/
static BOOL DisplayLanguageScreen(LANG_SEL_SCREEN* pLangSelInfo)
{
	char szDisBuf[MAX_CHARS_OF_LINE];

	ASSERT(pLangSelInfo);

#ifdef _SUPPORT_THREE_LANGUAGE
	snprintf(szDisBuf, sizeof(szDisBuf), "  %-14s", //SCREEN_WIDTH/ARROW_WIDTH - 2
			pLangSelInfo->ppLangText[pLangSelInfo->nCurItem]->pAbbrName[2]);

	 DisString(szDisBuf, 
			(int)strlen(szDisBuf),  
			0,
			FONT_HEIGHT_COMPROMISE,
			GetLangCode(LOCAL2_LANGUAGE_FLAG), 
			FLAG_DIS_NORMAL_TYPE);      //For scuplus
#endif
	
	snprintf(szDisBuf, sizeof(szDisBuf), "  %-14s", //SCREEN_WIDTH/ARROW_WIDTH - 2
		pLangSelInfo->ppLangText[pLangSelInfo->nCurItem]->pAbbrName[1]);

    DisString(szDisBuf, 
		(int)strlen(szDisBuf),  
		0,
		SCREEN_HEIGHT / 64 * FONT_HEIGHT_COMPROMISE,
		GetLangCode(LOCAL_LANGUAGE_FLAG), 
		FLAG_DIS_NORMAL_TYPE);

	snprintf(szDisBuf, sizeof(szDisBuf), "  %-14s", //SCREEN_WIDTH/ARROW_WIDTH - 2
		pLangSelInfo->ppLangText[pLangSelInfo->nCurItem]->pAbbrName[0]);

    DisString(szDisBuf, 
		(int)strlen(szDisBuf),  
		0,
		(SCREEN_HEIGHT / 64 + 1) * FONT_HEIGHT_COMPROMISE,
		GetLangCode(ENGLISH_LANGUAGE_FLAG), 
		FLAG_DIS_NORMAL_TYPE);

	DisplayProgressArrow(pLangSelInfo->nLangNum,
		pLangSelInfo->nCurItem,
		0,
		0);

	return TRUE;
}

//Frank Wu,20160127, for MiniNCU
static BOOL DisplayLanguageScreenMiniNCU(LANG_SEL_SCREEN* pLangSelInfo)
{
	char								szDisBuf[MAX_CHARS_OF_LINE];
	char*								pszLangCode;
	char*								pszLangName;
	int									nDrawWidth;
	int									nFontHeight;
	LANG_SUPPORT_TABLE_ITEM*			psLangSupportTableList = NULL;
	ASSERT(pLangSelInfo);

	//pLangSelInfo->pvLangInfoForAllLang֧����������,pLangSelInfo->nCurItemForAllLang=0��ʾ��ʾEn��Ϊ1��ʾ��ʾ�������ԣ�����1��ʾ֧�ֵ�����
	//DisplayLanguageScreenMiniNCU���������Ű�������ѡ�����ʾ��ǰӦ����ʾ�����ԡ�
	psLangSupportTableList = (LANG_SUPPORT_TABLE_ITEM*)pLangSelInfo->pvLangInfoForAllLang;
	pszLangCode = psLangSupportTableList[pLangSelInfo->nCurItemForAllLang].pszLangCode;
	pszLangName = psLangSupportTableList[pLangSelInfo->nCurItemForAllLang].szLocalAbbrName;

	//printf("pLangSelInfo->nCurItemForAllLang= %d , pszLangCode :%s~~~~~~~~~~~~~~~~~~~~\n",pLangSelInfo->nCurItemForAllLang,pszLangCode);
	
	snprintf(szDisBuf, sizeof(szDisBuf), "%s", pszLangName);

	nDrawWidth = GetWidthOfString(szDisBuf, (int)strlen(szDisBuf), pszLangCode);
	nFontHeight = GetFontHeightFromCode(pszLangCode);

	//display char '->'
	DisSpecifiedFlag(ASCII_RIGHT_ARROW, 0, (SCREEN_HEIGHT - nFontHeight)/2);

	//clear text area
	ClearBar(ARROW_WIDTH,
		(SCREEN_HEIGHT - nFontHeight)/2,
		SCREEN_WIDTH - ARROW_WIDTH - ARROW_WIDTH,
		nFontHeight,
		FLAG_DIS_NORMAL_TYPE);
		
	//display text
	DisString(szDisBuf, 
		(int)strlen(szDisBuf),  
		(SCREEN_WIDTH - ARROW_WIDTH - nDrawWidth)/2,
		(SCREEN_HEIGHT - nFontHeight)/2,
		pszLangCode,
		FLAG_DIS_NORMAL_TYPE);

	//display progress bar
	DisplayProgressBar(pLangSelInfo->nLangNumForAllLang,
		pLangSelInfo->nCurItemForAllLang,
		pLangSelInfo->nCurItemForAllLang,
		pLangSelInfo->nCurItemForAllLang);

	return TRUE;
}



enum LANG_SCREEN_WAIT_ENUM
	{
		LANG_SCREEN_WAIT_GO_ON = 0,
		LANG_SCREEN_WAIT_TIME_OUT,
	};


/*==========================================================================*
 * FUNCTION : DoModal_DefaultScreen
 * PURPOSE  : All the handle on default screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*       pMenuDisplay : 
 *            GLOBAL_VAR_OF_LCD*  pThis        : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 14:48
 *==========================================================================*/
int DoModal_LanguageScreen(MENU_DISPLAY* pMenuDisplay,
						  GLOBAL_VAR_OF_LCD* pThis)
{
	UCHAR byKeyValue;

	BOOL bRefreshAtOnce = TRUE;

	LANG_SEL_SCREEN* pLangSelInfo = &s_LangSelScreenInfo;


	int nError = ERR_LCD_OK;

	time_t	tmTimeout = time(NULL);
	time_t	tmNow;
	//1.Init
	LoadLangSelScreenInfo(pLangSelInfo, pThis);	

	ClearScreen();

	//2.Display language screen
	//DisplayLanguageScreen(pLangSelInfo);
	
	//3.User must choose one type of language
	for(;;)
	{
		byKeyValue = GetKeyEx();

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();

			switch( byKeyValue )
			{
			case VK_ENTER:
				{
					if (SetCurrentLang(pLangSelInfo->nCurItem, FALSE) == TRUE)
					{
					}
					else
					{
						nError = ERR_LCD_FAILURE;

						goto __EXIT_LANGUAGE_SCREEN;
					}
					
				}
				//Needn't break, call DoModal_DefaultScreen
			case VK_ESCAPE:  
				{
					goto __ENTER_DEFAULT_SCREEN;
				}
				break;
			case VK_UP:  
				{
					if (pLangSelInfo->nLangNum > 1 )
					{
						pLangSelInfo->nCurItem = 
							((pLangSelInfo->nCurItem - 1) >= 0)
							? pLangSelInfo->nCurItem - 1
							: pLangSelInfo->nLangNum - 1;

						bRefreshAtOnce = TRUE;
					}
				}

				break;
			case VK_DOWN:
				{
					if (pLangSelInfo->nLangNum > 1 )
					{
						pLangSelInfo->nCurItem = 
							((pLangSelInfo->nCurItem + 1) 
							< pLangSelInfo->nLangNum)
							? pLangSelInfo->nCurItem + 1
							: 0;

						bRefreshAtOnce = TRUE;
					}
				}

				break;
			
			default:
				break;
			}

		}
		else
		{
			int nRetHandleIdle = HandleIdle();

			if (nRetHandleIdle == ID_INIT_LCD_JUST_NOW)
			{
				ClearScreen();
				bRefreshAtOnce = TRUE;
			}
			else if (nRetHandleIdle == ID_GOTO_DEF_SCREEN)
			{
				if (!THREAD_IS_RUNNING(pThis->hThreadSelf))
				{
					//LCD thread should quit
					nError = ERR_LCD_OK;

					goto __EXIT_LANGUAGE_SCREEN;
				}

			}
		}

		ASSERT(pThis);
		//Change to another language
		if(bRefreshAtOnce/* || pThis->cDataRefreshFlag == NEED_REFRESH_DATA */)
		{
			tmTimeout = time(NULL);//Reset the time to current time

			DisplayLanguageScreen(pLangSelInfo);

			bRefreshAtOnce = FALSE;

			/*pThis->cDataRefreshFlag = NEED_NOT_REFRESH_DATA;*/
		}


		tmNow = time(NULL);
		if ((tmNow - tmTimeout) >= LANGUAGE_SCREEN_TIMEOUT)
		{
			goto __ENTER_DEFAULT_SCREEN;
		}
		//Maybe modify time by user interface
		else if (tmNow < tmTimeout)
		{
			tmTimeout = tmNow;
		}

	}

__ENTER_DEFAULT_SCREEN:
	//4.All the LCD handle around here and loop around here
	DoModal_DefaultScreen(pMenuDisplay, pThis); //Start from Deault Screen

	ClearScreen();

	nError = ERR_LCD_OK;

__EXIT_LANGUAGE_SCREEN:

	DoModal_AcknowlegeDlg_INFO("", 
		"  App Closed!", 
		"",
		"",
		-1);

	return nError;

}


//Frank Wu,20160127, for MiniNCU
int DoModal_LanguageScreenMiniNCU(MENU_DISPLAY* pMenuDisplay,
								  GLOBAL_VAR_OF_LCD* pThis)
{
	UCHAR byKeyValue;

	BOOL bRefreshAtOnce = TRUE;

	LANG_SEL_SCREEN* pLangSelInfo = &s_LangSelScreenInfo;


	int nError = ERR_LCD_OK;

	time_t	tmTimeout = time(NULL);
	time_t	tmNow;


	//1.Init
	LoadLangSelScreenInfoMiniNCU(pLangSelInfo, pThis);	

	
	ClearScreen();

	//2.Display language screen
	//DisplayLanguageScreenMiniNCU(pLangSelInfo);

	//3.User must choose one type of language
	for(;;)
	{
		byKeyValue = GetKeyEx();

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();

			switch( byKeyValue )
			{
			case VK_ENTER:
				{
					if (SetCurrentLangMiniNCU(pLangSelInfo, FALSE))
					{
						goto __ENTER_DEFAULT_SCREEN;
					}
					
					ClearScreen();
					
					break;
				}
				//Needn't break, call DoModal_DefaultScreen
			case VK_ESCAPE:  
				{
					goto __ENTER_DEFAULT_SCREEN;
				}
				break;
			case VK_UP:  
				{
					if (pLangSelInfo->nLangNumForAllLang > 1 )
					{
						pLangSelInfo->nCurItemForAllLang = 
							((pLangSelInfo->nCurItemForAllLang - 1) >= 0)
							? pLangSelInfo->nCurItemForAllLang - 1
							: pLangSelInfo->nLangNumForAllLang - 1;

					}
				}

				break;
			case VK_DOWN:
				{
					if (pLangSelInfo->nLangNumForAllLang > 1 )
					{
						pLangSelInfo->nCurItemForAllLang = 
							((pLangSelInfo->nCurItemForAllLang + 1) 
							< pLangSelInfo->nLangNumForAllLang)
							? pLangSelInfo->nCurItemForAllLang + 1
							: 0;

					}
				}

				break;

			default:
				break;
			}
			bRefreshAtOnce = TRUE;
		}
		else
		{
			//int nRetHandleIdle = HandleIdle();
			//int nRetHandleIdle = -1;//do nothing in Language select screen
			
			int nRetHandleIdle = ID_GOTO_DEF_SCREEN;
			
			if (nRetHandleIdle == ID_INIT_LCD_JUST_NOW)
			{
				ClearScreen();
				bRefreshAtOnce = TRUE;
			}
			else if (nRetHandleIdle == ID_GOTO_DEF_SCREEN)
			{
				if (!THREAD_IS_RUNNING(pThis->hThreadSelf))
				{
					//LCD thread should quit
					nError = ERR_LCD_OK;

					goto __EXIT_LANGUAGE_SCREEN;
				}

			}
		}

		ASSERT(pThis);
		//Change to another language
		if(bRefreshAtOnce/* || pThis->cDataRefreshFlag == NEED_REFRESH_DATA */)
		{
			tmTimeout = time(NULL);//Reset the time to current time

			/*
				ϵͳ������������ѡ������У�ѡ����ʲô���Խ��룬��һ������������ѡ������Ĭ����ʾ�������ԡ�

				����web��¼����ѡ��EN����󣬲������¼���´�������LCD��Ȼ��ʾ��һ�ε�ǰ����

				��ˣ�ֻ����LCD�ϲ�����ǰ���ԣ���һ��������LCD����ѡ�����Ż����ϴ�ѡ�������Ĭ����ʾ
				
			*/	
			DisplayLanguageScreenMiniNCU(pLangSelInfo);

			bRefreshAtOnce = FALSE;

			/*pThis->cDataRefreshFlag = NEED_NOT_REFRESH_DATA;*/
		}


		tmNow = time(NULL);
		if ((tmNow - tmTimeout) >= LANGUAGE_SCREEN_TIMEOUT)
		{
			goto __ENTER_DEFAULT_SCREEN;
		}
		//Maybe modify time by user interface
		else if (tmNow < tmTimeout)
		{
			tmTimeout = tmNow;
		}

	}

__ENTER_DEFAULT_SCREEN:
	//4.All the LCD handle around here and loop around here
	DoModal_DefaultScreen(pMenuDisplay, pThis); //Start from Deault Screen

	ClearScreen();

	nError = ERR_LCD_OK;

__EXIT_LANGUAGE_SCREEN:

	PromptAppClosed();

	return nError;

}




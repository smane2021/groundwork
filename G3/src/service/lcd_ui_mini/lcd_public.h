/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_public.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:55
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_PUBLIC_H__041008
#define __LCD_PUBLIC_H__041008

#ifdef  __cplusplus
extern "C" {
#endif

//#define	DISP_RECT_POSITION_NO
#define GET_PART_LANG_INFO_FROM_SYS_RES

#define CHANGE_LCD_HEIGHT
#define SET_LCD_ROTATION

#define IS_HIGH_SCREEN (SCREEN_HEIGHT == 128)
#define IS_LOW_SCREEN	(SCREEN_HEIGHT == 32)





//EQUIP_TYPE_ID
#define		RECT_GROUP_EQUIP_TYPE	200
#define		BATT_GROUP_EQUIP_TYPE	300
#define		BATTERY_EQUIP_TYPE		301
#define		EIB_BATTERY_EQUIP_TYPE	303

#define		SMDU_GROUP_EQUIP_TYPE	1000

#define		SLAVE1_EQUIP_TYPE		1600

#define		IB_EQUIP_TYPE			801	//��IB��DI�澯����ŵ��澯������������ʱ��Ҫ�õ�

//EQUIP_ID
#define		POWER_SYSTEM_EQUIP_ID	1

#define		RECT_GROUP_EQUIP_ID		2
#define		RECT_EQUIP_ID_MIN		3
#define		RECT_EQUIP_ID_MAX		102

#define		BATT_GROUP_EQUIP_ID		115

#define		BATTERY1_EQUIP_ID		116
#define		BATTERY2_EQUIP_ID		117

#define		EIB1_BATTERY1_EQUIP_ID		118

#define		EIB4_BATTERY2_EQUIP_ID		125

#define		EIB1_BATTERY3_EQUIP_ID		206
#define		EIB4_BATTERY3_EQUIP_ID		209

#define		EIB1_EQUIP_ID				197
#define		EIB2_EQUIP_ID				198
#define		EIB3_EQUIP_ID				199
#define		EIB4_EQUIP_ID				200


#define		CONVERTER_GROUP_EQUIP_ID	210
#define		CONVERTER_EQUIP_ID_MIN		211
#define		CONVERTER_EQUIP_ID_MAX		258


//Signal ID
#define		SIG_ID_POSTION_NO			34

#define		SIG_ID_MB_BATT_NUM			64
#define		SIG_ID_EIB_BATT_NUM			7

#define		SIG_ID_SMDU_NUM				1
#define		SIG_ID_SMDU_CONFIG_CHANGE	2

#define		SIG_ID_CONVERTER_NUM		8
#define		SIG_ID_RECTGROUP_MAX_CURRENT		33
#define		SIG_ID_RECTGROUP_MIN_CURRENT		34





#define	GET_EQUIP_TYPE_ID_MAJOR(EquipTypeID)	((int)((EquipTypeID) / 100))
#define	GET_EQUIP_TYPE_ID_SUB(EquipTypeID)	((int)((EquipTypeID) % 100))







//#include "lcd_interface.h"
#include "version_manage.h"
enum TIMER_ID_LCD_ENUM
{
	TM_ID_PASSWORD_VALID = 100,
	TM_ID_SWITCH_DEF_SCREEN,
	TM_ID_ALARM_POP_AT_ONCE,
	TM_ID_INIT_LCD,
	TM_ID_REFRESH_DATA,
};

//#define INTERVAL_PASSWORD_VALID		(4	* 60 * 1000 - 10)	// 4min
#define INTERVAL_PASSWORD_VALID		(8	* 60 * 1000 - 10)	// 8min
#define INTERVAL_SWITCH_DEF_SCREEN	(8	* 60 * 1000 - 20)	// 8min
#define INTERVAL_ALARM_POP_AT_ONCE	(2	* 60 * 1000 - 30)	// 2min
#define INTERVAL_INIT_LCD			(10 * 60 * 1000 - 40)	// 10min
//#define INTERVAL_REFRESH_DATA		(1	* 1000)				// 1second
#define INTERVAL_REFRESH_DATA		(2	* 1000)				// 2second

#define INTERVAL_SCREEN_TIMEOUT		(5	* 60 * 1000)		//5min

#define	DEBUG_LCD_INFO(Info) \
	TRACE("****LCD**** %s.\n", Info)

#define	DEBUG_LCD_INFO_STRING(Info, StringVar) \
	TRACE("****LCD**** %s %s.\n", Info, StringVar)

#define DEBUG_LCD_FILE_FUN_LINE_STRING(StringVar) \
	TRACE("****LCD**** [%s]-[%s]-[%d]: %s.\n", \
	__FILE__, __FUNCTION__, __LINE__, StringVar)

#define DEBUG_LCD_FILE_FUN_LINE_INT(VarName, VarValue) \
	TRACE("****LCD**** [%s]-[%s]-[%d]: %s = %d.\n", \
	__FILE__, __FUNCTION__, __LINE__, VarName, VarValue)

#define DEBUG_LCD_FILE_FUN_LINE_HEX(VarName, VarValue) \
	TRACE("****LCD**** [%s]-[%s]-[%d]: %s = 0x%X.\n", \
	__FILE__, __FUNCTION__, __LINE__, VarName, VarValue)

//#define  LCD_LOG_OUT(iLevel, OutString) \
//	AppLogOut(g_szCurUser, (iLevel), "%s", (OutString))

#define LOGOUT_NO_MEMORY(p) \
	if(!(p)) {AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, \
				"[%s]-[%s]-[%d]: There is no enough memory!\n", \
				__FILE__, __FUNCTION__, __LINE__); \
				return ERR_LCD_NO_MEMORY;}

#define LOGOUT_NO_MEM_RETURN_NULL(p) \
	if(!(p)) {AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, \
				"[%s]-[%s]-[%d]: There is no enough memory!\n", \
				__FILE__, __FUNCTION__, __LINE__); \
				return NULL;}

#define LOGOUT_NO_MEM_RETURN(p) \
	if(!(p)) {AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, \
				"[%s]-[%s]-[%d]: There is no enough memory!\n", \
				__FILE__, __FUNCTION__, __LINE__); \
				return;}

#define DELETE_ITEM(p) if((p)) { SAFELY_DELETE(p);  (p) = NULL; }


#define NextSubMenuItem(pSubMenu, nStep)   \
	( (((pSubMenu)->nCur + nStep) < (pSubMenu)->nDispItems) \
	? ((pSubMenu)->nCur + nStep) : 0 )

/*#define PrevSubMenuItem(pSubMenu, nStep)   \
//	( (((pSubMenu)->nCur - (nStep)) >= 0) \
//	? ((pSubMenu)->nCur - (nStep)) : ((pSubMenu)->nItems-1) )
*/
//#define PrevSubMenuItem(pSubMenu, nStep)   \
//	( (((pSubMenu)->nCur - (nStep)) >= 0) \
//	? ((pSubMenu)->nCur - (nStep)) \
//	: ((pSubMenu)->nItems -  \
//	(((pSubMenu)->nItems % nStep) ? ((pSubMenu)->nItems % nStep) : nStep)))

#define PrevSubMenuItem(pSubMenu, nStep)   \
	( (((pSubMenu)->nCur - (nStep)) >= 0) \
	? ((pSubMenu)->nCur - (nStep)) \
	: ((pSubMenu)->nDispItems -  \
	(((pSubMenu)->nDispItems % nStep) ? ((pSubMenu)->nDispItems % nStep) : nStep)))



//This SIG_TYPE_EX_FOR_LCD should be different from SIG_TYPE which defined in cfg_model.h
enum SIG_TYPE_EX_FOR_LCD
{
	SIG_TYPE_BARCODE			= SIG_TYPE_MAX + 1,
	SIG_TYPE_ALARM_LEVEL_SET,
	SIG_TYPE_ALARM_RELAY_SET,

	SIG_TYPE_ACTIVE_ALARM,
	SIG_TYPE_HISTORY_ALARM,
};
//This enum sig is for define work mode of slave/master addedd by Jimmy Wu 20110706
enum ENUM_WORK_MODE_FOR_SLAVE 
{
	WORK_MODE_SEPERATE = 0,
	WOKE_MODE_MASTER,
	WORK_MODE_SLAVE,
};



//not change their values! because the pAbbrName of LANG_TEXT, element 0 is English
enum LANGUAGE_FLAG_ENUM
{
	ENGLISH_LANGUAGE_FLAG = 0,
	LOCAL_LANGUAGE_FLAG,
	LOCAL2_LANGUAGE_FLAG,	 //Scu plus add another local language
};

enum RETURN_VALUE_IS_VALID_ENUM
{
	RET_VALID_DATA = 0,
	RET_INVALID_DATA = 1		//For example, Communication interrupt

};

enum MENU_ITEM_STATUS_ENUM
{
	MD_SHOW_STATUS = 0,
	MD_EDIT_STATUS,
};

enum MENU_ITEM_ACKNOWLEDGE_ENUM
{
	MENU_ITEM_NEED_NOT_ACKNOWLEDGE = 0,
	MENU_ITEM_NEED_ACKNOWLEDGE,
};


enum MENU_ITEM_VALIDATE_ENUM
{
	MENU_ITEM_NEED_NOT_VALIDATE = 0,
	MENU_ITEM_NEED_VALIDATE,
};

enum PASSWORD_VALIDATE_ENUM
{
	NEED_PASSWORD_VALIDATE = 0,
	NEED_NOT_PASSWORD_VALIDATE,
};

enum SWITCH_DEF_SCREEN_ENUM
{
	NEED_NOT_SWITCH_TO_DEF_SCREEN= 0,
	NEED_SWITCH_TO_DEF_SCREEN,
};

enum ALARM_POP_AT_ONCE_ENUM
{
	NEED_NOT_ALARM_POP_AT_ONCE = 0,
	NEED_ALARM_POP_AT_ONCE,
};

enum INIT_LCD_ENUM
{
	NEED_NOT_INIT_LCD = 0,
	NEED_INIT_LCD,
};

enum REFRESH_DATA_ENUM
{
	NEED_NOT_REFRESH_DATA = 0,
	NEED_REFRESH_DATA,
};

enum RECT_TWINKLE_ENUM
{
	RECT_TWINKLE_OFF = 0,
	RECT_TWINKLE_ON = 1,
};

enum ALARM_REPORT_ENUM
{
	NO_ALARM_REPORT = 0,
	ALARM_OCCUR_REPORT,
	ALARM_CLEAR_REPORT,
};

enum ALARM_POP_ENUM
{
	NO_ALARM_POP = 0,
	ALARM_POP_STATUS = 1,
	ALARM_DIS_STATUS = 2,

};

enum DIS_ALARM_NUM_FLAG
{
	NO_DIS_ALARM_NUM = 0,
	YES_DIS_ALARM_NUM = 1,
};

enum AUTO_CONFIG_STATUS_ENUM
{
	OK_AUTO_CONFIG = 0,
	IS_BACK_TO_DEFAULT_SCRREN,
	IS_WAIT_FOR_CONFIG,	

	IS_READY_TO_RECREATE,
	IS_RECREATE_MENU,
	IS_OVER_CREATE,
};


//Can not conflict with AUTHORITY_LEVEL
//Because of the password menu return 
enum ID_RET_FLAG
{
	ID_OK       = AUTHORITY_LEVEL_MAX +1, 
	ID_CANCEL			,//No operation, resume old status
	ID_RETURN           ,

	ID_GOTO_RPEVIOUS	,//Return to previous menu screen
	ID_GOTO_DEF_SCREEN	,//TIme out, need to goto default screen

	ID_INIT_LCD_JUST_NOW,//LCD is initialed,should refresh data immediately

	ID_VIEWING			,
	ID_EDITING			,

	ID_ERR_PASSWORD		,

	ID_TIMEOUT_RETURN	,

};

//enum REFRESH_ENUM_FLAG
//{
//	ALL_REFRESH_FLAG	= 0x01,
//	PART_REFRESH_FLAG	= 0x02,
//	ARROW_REFRESH_FLAG	= 0x04,
//	DATA_REFRESH_FLAG	= 0x08,
//};




enum MENU_ITEM_LANG_ID_ENUM
{
	//�������ENUM������ֵӦ��lcd_private_zh.res���źŵ�RES_ID���Ӧ

	MAIN_MENU_LANG_ID = 1,
	MAIN_RUNINFO_LANG_ID,
	MAIN_CONTROL_LANG_ID,
	MAIN_SET_LANG_ID,
	MAIN_ENERGY_SAVING_LANG_ID,
	MAIN_QUICK_SETTING_LANG_ID,
	MAIN_CUSTOMIZE_LANG_ID,
	MAIN_SELF_TEST1_LANG_ID,
	MAIN_SELF_TEST2_LANG_ID,
	MAN_AUTO_SET_LANG_ID,

	PASSWORD_DLG_USER_ID = 11,
	PASSWORD_DLG_PWD_ID,

	MAIN_SLAVE_SET_LANG_ID = 13,

	ACTIVE_ALARM_DISP_LANG_ID = 21,
	HISTORY_ALARM_DISP_LANG_ID,
	NO_ACTIVE_ALARM_PROMPT_LANG_ID,
	NO_HISTORY_ALARM_PROMPT_LANG_ID,

	HEADING_ACKNOWLEDGE_INFO_LANG_ID = 31,
	ENT_KEY_ACKNOWLEDGE_LANG_ID,
	ESC_KEY_CANCEL_PROMPT_LANG_ID,

	HEADING_PROMPT_INFO_LANG_ID,
	ERR_PASSWORD_PROMPT_LANG_ID,
	ESC_OR_ENT_RET_PROMPT_LANG_ID,

	NOT_ENOUGH_PRIVILEGE_LANG_ID,
	NO_ITEM_INFO_LANG_ID,

	SWITCH_TO_NEXT_EQUIP_LANG_ID,
	SWITCH_TO_PREVIOUS_EQUIP_LANG_ID,

	SIGNAL_CANNOT_BE_SET_LANG_ID,
	SIGNAL_CANNOT_BE_CONTROL_LANG_ID,

	CONFLICT_SETTING_LANG_ID,

	CONTROL_OR_SETTING_FAILED_LANG_ID,

	HARDWARE_PROTECT_STATUS_LANG_ID,

	REBOOT_PROMPT_LANG_ID,

	APP_IS_LANG_ID,
	AUTO_CONFIGING_LANG_ID,
	COPYING_FILE_LANG_ID,
	PLEASE_WAIT_LANG_ID,

	SWITCH_TO_SET_LANG_ID,

	DHCP_IS_OPEN_LANG_ID,
	CAN_NOT_SET_LANG_ID,

	DOWNLOAD_COMPLETE_LANG_ID,
	REBOOT_TO_VALIDATE_LANG_ID,

	PRODUCT_INFO_LANG_ID = 61,			//�⼸���źŵ�ID����С��256����Ϊֻ����һ���ֽ�
	PRODUCT_INFO_NAME_LANG_ID,
	PRODUCT_INFO_PART_NUMBER_LANG_ID,
	PRODUCT_INFO_HWVERSION_LANG_ID,
	PRODUCT_INFO_SWVERSION_LANG_ID,
	PRODUCT_INFO_SERIAL_LANG_ID,
	
	LOCAL_LANGUAGE_NAME_LANG_ID = 80,

	ALARM_PARAM_SET_LANG_ID = 81,

	ALARM_LEVEL_SET_LANG_ID,
	NO_ALARM_LANG_ID,
	OBSERVE_ALARM_LANG_ID,
	MAJOR_ALARM_LANG_ID,
	CRITICAL_ALARM_LANG_ID,

	ALARM_RELAY_SET_LANG_ID,
	ALARM_NO_RELAY_LANG_ID,
	ALARM_RELAY1_LANG_ID,
	ALARM_RELAY2_LANG_ID,
	ALARM_RELAY3_LANG_ID,
	ALARM_RELAY4_LANG_ID,
	ALARM_RELAY5_LANG_ID,
	ALARM_RELAY6_LANG_ID,
	ALARM_RELAY7_LANG_ID,
	ALARM_RELAY8_LANG_ID,
	ALARM_RELAY9_LANG_ID,
	ALARM_RELAY10_LANG_ID,
	ALARM_RELAY11_LANG_ID,
	ALARM_RELAY12_LANG_ID,
	ALARM_RELAY13_LANG_ID,

	ALARM_CONTROL_LANG_ID,
	ALARM_VOICE_CTRL_ID,
	OUTGOING_ALARM_CTRL_ID,
	CLEAR_HIS_ALARM_CTRL_ID,
	YES_HANDLE_LANGUAGE_LANG_ID,
	NO_HANDLE_LANGUAGE_LANG_ID,

	ALARM_VOLTAGE_IB_LANG_ID,

	SYSTEM_SET_LANG_ID = 121,
	SYS_LANGUAGE_SET_ID,
	SYS_TIME_ZONE_SET_ID,
	SYS_DATE_SET_ID,
	SYS_TIME_SET_ID,
	RELOAD_DEF_CONFIG_SET_ID,
	KEYPAD_VOICE_SET_ID,
	DOWNLOAD_CONFIG_LANG_ID,
	AUTO_CONFIG_LANG_ID,

	COMMUNICATION_SET_LANG_ID = 141,

	DHCP_SET_LANG_ID,
	SYS_IP_ADDR_SET_ID,
	SYS_NETMASK_SET_ID,
	SYS_GATEWAY_SET_ID,

	SYS_SITE_ID_SET_ID,
	YDN_PORT_MODE_SET_ID,
	YDN_PORT_PARAM_SET_ID,
	YDN_CALL_BACK_ENB_SET_ID,
	YDN_CALL_BACK_NUM_SET_ID,
	YDN_CALL_BACK_INTERVAL_SET_ID,
	YDN_PHONE_NO1_SET_ID,
	YDN_PHONE_NO2_SET_ID,
	YDN_PHONE_NO3_SET_ID,


	DHCP_OPEN_LANG_ID = 161,
	DHCP_CLOSE_LANG_ID,
	DHCP_ERROR_LANG_ID,

	CNT_MODE_RS232,
	CNT_MODE_MODEM,
	CNT_MODE_ETHERNET,

	PORT_PARAM_5050,
	PORT_PARAM_2400,
	PORT_PARAM_4800,
	PORT_PARAM_9600,
	PORT_PARAM_19200,
	PORT_PARAM_38400,






//////////////////////////////////////////////////////////////////////////

	ENGLISH_LANGUAGE_LANG_ID,
	LOCAL_LANGUAGE_LANG_ID,


//////////////////////////////////////////////////////////////////////////


	ACU_SERIAL_NUMBER_LANG_ID = 301,
	ACU_HARDWARE_VERSION_LANG_ID,
	ACU_SOFTWARE_VERSION_LANG_ID,
	ACU_ETHADDR_LANG_ID,
	ACU_FILE_SYSTEM_VERSION_LANG_ID,
	ACU_PRODUCT_INFO_LANG_ID,
	ACU_CONFIG_VERSION_ID,





	CHANGE_LCD_HEIGHT_SET_ID = 501,
	CHANGE_LCD_HEIGHT_128_64_LANG_ID,
	CHANGE_LCD_HEIGHT_128_128_LANG_ID,

	SET_LCD_ROTATION_SET_ID = 504,
	SET_LCD_ROTATION_0_LANG_ID,
	SET_LCD_ROTATION_90_LANG_ID,
	SET_LCD_ROTATION_180_LANG_ID,
	SET_LCD_ROTATION_270_LANG_ID,

	OTHER_USE_ID = 800,
	PASSWORD_SCREEN_MENU_ID,
	SIG_TIME_TYPE_SET_ID,
	

};


enum MENU_ITEM_TYPE_ENUM
{
	MT_SUBMENU = 1,

	MT_SIGNAL_DISPLAY,
	MT_SIGNAL_SET,
	MT_SIGNAL_CONTROL,

	MT_SIGNAL_CUSTOMIZE,		//�û��Զ����ź����ID�������ź���ͬ

	MT_SELF_DEF_SET,

	//	MT_ALARM_DISPLAY
	MT_ACTIVE_ALARM_DISPLAY,
	MT_HISTORY_ALARM_DISPLAY,

	MT_ALARM_LEVEL_SET,
	MT_ALARM_RELAY_SET,

	MT_PASSWORD_SELECT,

	MT_BARCODE,				//ֻ������ʾ�����ܱ༭

	MT_SELF_TEST,			//�Բ��Բ˵�������Բ⡢���Ե�

};

//Sub menu ID has three types
enum PREFIX_MENU_ITEM_ID
{
	//do not suggest use 0xFF, because -1 is ffffffff
	PREFIX_FIXED_LANG_RES		= 0xFE,		//���磺���˵���������Ϣ������������û��Զ���˵�
	PREFIX_STDANDARD_EQUIP		= 0xFD,		//���磺�澯���������µ��豸�����׼�����ļ���Ӧ
	PREFIX_ACTUAL_EQUIP			= 0xFC,		//���磺��Դϵͳ������ģ���飬����ģ��1��������
	PREFIX_VIRTUAL_GROUP_EQUIP	= 0xFB,		//���磺����ģ�飨����ģ��1~n����һ����
											//���磺��أ����1~n����һ���������ǡ�����顱��Ҳ���ǡ����1����

	PREFIX_BARCODE_DEVICE		= 0xFA,		//BARCODE�豸������Ʒ��Ϣ���µġ�����ģ��1��
	PREFIX_BARCODE_SIGNAL		= 0xF9,		//BARCODE�źţ�����Ʒ��Ϣ���µġ�����ģ��1���µġ�Ӳ���汾��

	PREFIX_CUSTOMIZE_MENU		= 0xF8,		//�û��Զ���˵�����Զ���˵����µġ����������

	PREFIX_DIVIDED_SUB_MENU		= 0xF7,		//��������Ӳ˵�������ѵ��������ò����ֳ�N��С�Ӳ˵�

	PREFIX_CHANGED_MENU_ITEM	= 0xF6,		//��Щ���豸��������Ҫ���⴦��

	PREFIX_SELF_TEST_MENU		= 0xF0,		//�����Բ��Բ˵���

};

//////////////////////////////////////////////////////////////////////////
//
//	MENU_ITEM(�˵���)��ID����
//		���ݽṹ��	int			nMenuItemID;	(4���ֽ�)
//		
//		�̶��˵���		Prefix + 0x00 + LangResID(WORD)
//		ʵ���豸��		Prefix + �ź����� + EquipID
//		�������豸��	Prefix + �ź����� + EquipTypeID
//		//ע��ͬһ��ʵ���豸�ڡ�������Ϣ���͡�������������������á����ǲ�һ���ġ�
//
//		�źţ�			�豸ID(WORD) + �ź�����(BYTE) + �ź�ID(BYTE)	//Ŀǰ�ź�ID���ᳬ��256
//		BARCODE�豸��	Prefix + iSigTypeEx + EquipID(WORD)				//PrefixΪPREFIX_STDANDARD_EQUIP
//		BARCODE�źţ�	EquipID(WORD) + iSigTypeEx + SigResID		
//		�Զ���˵��	Prefix + 0x00 + PageID
//
//////////////////////////////////////////////////////////////////////////
//
//	SUB_MENU�Ӳ˵���ID ������һ��Ĳ˵����ID��ͬ
//
//////////////////////////////////////////////////////////////////////////


#define LCD_MERGE_FIXED_LANG_ID(LangResID) \
	(MAKELONG((LangResID), MAKEWORD((00),(PREFIX_FIXED_LANG_RES))))

#define LCD_MERGE_ACT_EQUIP_ID(SubType, EquipID) \
	(MAKELONG((EquipID), MAKEWORD((SubType),(PREFIX_ACTUAL_EQUIP))))

#define LCD_MERGE_ACTUAL_EQUIP_ID(SigType, EquipID) \
	(MAKELONG((EquipID), MAKEWORD((SigType),(PREFIX_ACTUAL_EQUIP))))

#define LCD_MERGE_STD_EQUIP_ID(SigType, EquipTypeID) \
	(MAKELONG((EquipTypeID), MAKEWORD((SigType),(PREFIX_STDANDARD_EQUIP))))

#define LCD_MERGE_VIRTUAL_GROUP_EQUIP_ID(SigType, EquipTypeID) \
	(MAKELONG((EquipTypeID), MAKEWORD((SigType),(PREFIX_VIRTUAL_GROUP_EQUIP))))

#define LCD_MERGE_BARCODE_DEVICE_ID(DeviceID) \
	(MAKELONG((DeviceID),MAKEWORD((SIG_TYPE_BARCODE),(PREFIX_BARCODE_DEVICE))))

#define LCD_MERGE_BARCODE_SIGNAL_ID(SigResID, DeviceID) \
	(DXI_MERGE_UNIQUE_SIG_ID(DeviceID, SIG_TYPE_BARCODE, SigResID))

#define LCD_MERGE_CUSTOMIZE_MENU_ID(nPageID) \
	(MAKELONG((nPageID), MAKEWORD((00),(PREFIX_CUSTOMIZE_MENU))))

#define LCD_MERGE_DIVIDED_SUB_MENU_ID(nResouceID) \
	(MAKELONG((nResouceID), MAKEWORD((00),(PREFIX_DIVIDED_SUB_MENU))))

#define LCD_MERGE_CHANGE_MENU_ITEM_ID(nResouceID) \
	(MAKELONG((nResouceID), MAKEWORD((00),(PREFIX_CHANGED_MENU_ITEM))))




#define LCD_SPLIT_PREFIX(MenuID) (HIBYTE(HIWORD((MenuID))))

#define LCD_SPLIT_LANG_RES_ID(MenuID) (LOWORD((MenuID)))

#define LCD_SPLIT_STD_EQUIP_ID(MergeMenuID) (LOWORD((MergeMenuID)))

#define LCD_SPLIT_ACT_EQUIP_ID(MergeMenuID) (LOWORD((MergeMenuID)))

#define LCD_SPLIT_BARCODE_DEVICE_ID_EQUIP(MenuID) (LOWORD((MenuID)))

#define LCD_SPLIT_BARCODE_SIGNAL_ID_EQUIP(MenuID) (HIWORD((MenuID)))

#define LCD_SPLIT_BARCODE_SIGNAL_ID_SIGNAL(MenuID) (LOBYTE(LOWORD(MenuID)))


//////////////////////////////////////////////////////////////////////////

#define LCD_SPLIT_BAR_DEVICE_ID(MenuID) (LOWORD((MenuID)))

#define LCD_SPLIT_DEVICE_ID(MenuID) (LOBYTE(HIWORD((MenuID))))
//////////////////////////////////////////////////////////////////////////






//Menu item struct is used to save menu item information
typedef struct tagMenuItem
{
	char		cMenuType;		//Menu item type 

	//To ensure the nonrecurring menu item ID
	//Fixed menu item ID:	LCD_MERGE_FIXED_LANG_ID
	//StdEquip menu item ID:LCD_MERGE_STD_EQUIP_ID
	//Equip menu item ID:	LCD_MERGE_ACT_EQUIP_ID
	//Signal menu item ID:	DXI_MERGE_UNIQUE_SIG_ID
	int			nMenuItemID;	//If it is a signal's id, high 16: signalid, low 16bit: signal type

	VAR_VALUE	vdData;			//Current data value(float, enum, int, char)

	int			nCursorOffset;	//Value display offset on lcd screen 
								
	char		cValidateFlag;	//Need to verified with password or not when ENTER is pressed
	char		cAcknowlegeFlag;//Need to be Acknowledged hint when ENTER is pressed
		
	BOOL		bDisplayFlag;	//Whether need to be displayed or not
	char		cMenuStatus;	//Menu item status, to see MENU_ITEM_STATUS_ENUM
	int		nCurrentEditPos;	//��ǰ���ڱ༭��λ�ã��Զ��ֶε��ź���Ч���������ڣ�ʱ�䣬IP���ַ�����

	void*		pvItemData;		//if nType=MT_SUBMENU, sub menu, 
								//(SUB_MENU*)pvItemData is sub menu pointer
								//if cMenuType = others, signal or alarm or self define item
}MENU_ITEM;


#define IS_SUBMENU_ITEM(pItem)  ((pItem)->cMenuType == MT_SUBMENU)
#define IS_BARCODE_ITEM(pItem)  ((pItem)->cMenuType == MT_BARCODE)
#define IS_SELF_TEST_ITEM(pItem)  ((pItem)->cMenuType == MT_SELF_TEST)

#define DEF_SUBMENU_ITEM(itemID, validateflag, ackflag,submenu) \
	{ MT_SUBMENU, (itemID), {0}, 0, (validateflag), (ackflag), TRUE, MD_SHOW_STATUS, TRUE, (PVOID)(submenu)  }

#define DEF_SIGNAL_ITEM(SigType, itemID, validateflag, ackflag,itemdata ) \
	{ (SigType), (itemID), {0}, 0, (validateflag), (ackflag), TRUE, MD_SHOW_STATUS, TRUE, (PVOID)(itemdata) }

#define DEF_BARCODE_ITEM(itemID, itemdata ) \
	{ MT_BARCODE, (itemID), {0}, 0, MENU_ITEM_NEED_NOT_VALIDATE, MENU_ITEM_NEED_NOT_ACKNOWLEDGE, \
		TRUE, MD_SHOW_STATUS, 0, (PVOID)(itemdata) }


#define INIT_SUBMENU_ITEM(p, itemID, validateflag, ackflag, itemdata) \
	((p)->cMenuType = MT_SUBMENU, \
	(p)->nMenuItemID = (itemID), \
	(p)->vdData.lValue = 0, \
	(p)->nCursorOffset = 0, \
	(p)->cValidateFlag = (validateflag), \
	(p)->cAcknowlegeFlag = (ackflag), \
	(p)->bDisplayFlag = TRUE, \
	(p)->cMenuStatus = MD_SHOW_STATUS, \
	(p)->nCurrentEditPos = 0, \
	(p)->pvItemData = (PVOID)(itemdata))

#define INIT_SELF_TEST_MENU_ITEM(p, itemID) \
	((p)->cMenuType = MT_SELF_TEST, \
	(p)->nMenuItemID = (itemID), \
	(p)->vdData.lValue = 0, \
	(p)->nCursorOffset = 0, \
	(p)->cValidateFlag = MENU_ITEM_NEED_NOT_VALIDATE, \
	(p)->cAcknowlegeFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE, \
	(p)->bDisplayFlag = TRUE, \
	(p)->cMenuStatus = MD_SHOW_STATUS, \
	(p)->nCurrentEditPos = 0, \
	(p)->pvItemData = NULL)


#define INIT_SIGNAL_ITEM(p, SigType,itemID, validateflag, ackflag, itemdata) \
	((p)->cMenuType = (SigType), \
	(p)->nMenuItemID = (itemID), \
	(p)->vdData.lValue = 0, \
	(p)->nCursorOffset = 0, \
	(p)->cValidateFlag = (validateflag), \
	(p)->cAcknowlegeFlag = (ackflag), \
	(p)->bDisplayFlag = TRUE, \
	(p)->cMenuStatus = MD_SHOW_STATUS, \
	(p)->nCurrentEditPos = 0, \
	(p)->pvItemData = (PVOID)(itemdata))

#define INIT_BARCODE_ITEM(p, itemID, itemdata) \
	((p)->cMenuType = MT_BARCODE, \
	(p)->nMenuItemID = (itemID), \
	(p)->vdData.lValue = 0, \
	(p)->nCursorOffset = 0, \
	(p)->cValidateFlag = MENU_ITEM_NEED_NOT_VALIDATE, \
	(p)->cAcknowlegeFlag = MENU_ITEM_NEED_NOT_ACKNOWLEDGE, \
	(p)->bDisplayFlag = TRUE, \
	(p)->cMenuStatus = MD_SHOW_STATUS, \
	(p)->nCurrentEditPos = 0, \
	(p)->pvItemData = (PVOID)(itemdata))



typedef struct tagSubMenu
{
	int			nSubMenuID;	//The mother ID of sub menu, used to display prompt info

	int         nItems;		//Actual items number    
	MENU_ITEM*	pItems;		//Items, is array pointer

	int			nDispItems;	//ʵ����ʾ�Ĳ˵��������nDispItems <= nItems;
	int*		pDispItems;	//ָ��һ�����飬�����鱣��ʵ����ʾ�Ĳ˵������

	int	        nCur;		//Curret item sequence
	int			nTop;
	int			nBottom;
}SUB_MENU;

#define DEF_SUB_MENU( submenuID, MenuItemsArray, DispItemsIndexArray)   \
	{ (submenuID), sizeof(MenuItemsArray)/sizeof((MenuItemsArray)[0]), (MenuItemsArray),\
		sizeof(MenuItemsArray)/sizeof((MenuItemsArray)[0]),DispItemsIndexArray,	0, 0, 0 }

#define INIT_SUB_MENU(p, submenuid,itemnum, pitems, pDisplayItems) \
	((p)->nSubMenuID = (submenuid), \
	(p)->nItems = (itemnum), \
	(p)->pItems = pitems, \
	(p)->nDispItems = (itemnum), \
	(p)->pDispItems = (pDisplayItems), \
	(p)->nCur = (0), \
	(p)->nTop = (0), \
	(p)->nBottom = (0))


#define INIT_MENU_NO_CHG_CUR(p, submenuid,itemnum, pitems, pDisplayItems) \
	((p)->nSubMenuID = (submenuid), \
	(p)->nItems = (itemnum), \
	(p)->pItems = pitems, \
	(p)->nDispItems = (itemnum), \
	(p)->pDispItems = (pDisplayItems), \
	(p)->nTop = (0), \
	(p)->nBottom = (0))

#define MAX_SUB_MENU_LEVEL  5		//The maximum allowed menu levels
struct tagMenuStru
{
	SUB_MENU        *pRootMenu; //Root menu

	SUB_MENU        *pCurMenu;  //Current sub menu
	MENU_ITEM       *pLastItem; 

	STACK_PVOID     stack;      //Menu stack, save previous sub menu
	PVOID           pStackBuff[MAX_SUB_MENU_LEVEL];//menu stack pointer
};
typedef struct tagMenuStru MENU_STRU;

#define DEF_MENU_STRU(psubMenu) \
	{	(psubMenu), (SUB_MENU*)NULL, (MENU_ITEM*)NULL,	{0, 0, 0}, {0}	}

#define INIT_MENU_STRU(p, rootMenu) ((p)->pRootMenu = (rootMenu))

typedef struct tagMenuDisplay
{
	MENU_STRU* pDefaultMenu;
	MENU_STRU* pMainMenu;		
	MENU_STRU* pPasswordMenu;	
}MENU_DISPLAY;



typedef struct tagGlobalVarofLCD
{
	char cPasswordFlag;			//The flag show the validity of password 
	char cSwitchDefScreenFlag;	//Need to switch to default screen
								//and close backlight to protect screen
	char cAlarmPopAtOnceFlag;	//Need to pop alarm when alarm was generated
	char cInitLCDFlag;			//Need to init LCD
	char cDataRefreshFlag;		//Need to refresh data of screen

	char cAlarmPopFlag;			//Alarm screen is pop now 

	//begin by hulw 061219 In alarm screen, LED flash for rectifier alarm
	char cDisAlarmNumFlag;			//Whether the alarm num is being displayed
	//end

	char	cAutoConfigStatus;


	HANDLE	hThreadSplash;
	HANDLE	hThreadBuzzerBeep;

	BOOL	bLCDServiceMainStartFlag;

	HANDLE	hThreadSelf;
	int		nTimerIDPasswordValid;
	int		nTimerIDSwitchDefScreen;
	int		nTimerIDAlarmPopAtOnce;
	int		nTimerIDInitLCD;
	int		nTimerIDDataRefresh;

	int		nValueStepMultiple;			//һֱ����ͬһ����ʱ�������ļ��ٱ���

	int		nOAnum;
	int		nCAnum;
	int		nMAnum;

}GLOBAL_VAR_OF_LCD;


typedef struct tagSectionDatastru
{
	UINT	uiValue;

	UINT	uiMinValue;
	UINT	uiMaxValue;

	UINT	uiSettingStep;

	char	*szValueDisplayFmt;		/* display format */ 

	char	cSeparateFlag;
}SECTION_DATA_STRU;

#define DEF_SECTION_DATA_STRU( value, minValue, maxValue, settingStep, valueDisplayFmt, separateFlag)   \
	{ (value), (minValue), (maxValue), (settingStep), (valueDisplayFmt), (separateFlag)}


#define MAX_STRING_LENGTH 14//only can input <=14 chars, the last char should be '\0'

/* for self define info definition */
typedef struct tagSelfDefineInfo	
{
	int				iSigID;				// NOTE: MUST BE THE FIRST  ITEM! DO NOT MOVE!
	LANG_TEXT		*pSigName;			// NOTE: MUST BE THE SECOND ITEM! DO NOT MOVE!
	int				iSigValueType;		/*	1: Long
											2: Float 
											3: Unsigned Long  
											4: Date/Time
											5: Enumeration  
											6: String
										*/	
	char			*szSigUnit;	 
	VAR_VALUE		defaultValue;	
	BYTE			byValueDisplayAttr;	// display attribute, use macro 
	BYTE			byAuthorityLevel;	// authority level needed to set current sig 
	BOOL			bCanBeCtrlledorSet;	// Can be controlled or set
	int				iEquipAndSigID;		// EquipId, SigType, SigId
	char			*szValueDisplayFmt;	// display format */ 
	float			fSettingStep;	
	float			fMaxValidValue;
	float			fMinValidValue;

	//For enum type -- pStateText, multiSect type -- pSectionDataStru
	int				iStateNum;			/* state count for Enumeration sig value */
	LANG_TEXT		**pStateText;	    /* text description for enumeration sig value, pointer array */		
	
	//For VAR_MULTI_SECT type
	SECTION_DATA_STRU*	pSectionDataStru;//Used to set ip and date, time
	
	//for VAR_STRING and VAR_PHONE_NO
	char			szString[MAX_STRING_LENGTH + 1];
}SELF_DEFINE_INFO;

#define DEF_SELF_DEFINE_INFO(sigID, sigName, sigValueType, authorityLevel, \
	valueDisplayFmt, settingStep, maxValidValue, minValidValue, \
	stateNum, stateText, sectionDataStru) \
	{ (sigID), (sigName), (sigValueType), (""), {0}, \
	(DISPLAY_LCD), (authorityLevel), \
	(TRUE), (0), (valueDisplayFmt), (settingStep), (maxValidValue), (minValidValue), \
	(stateNum), (stateText), (sectionDataStru), ""}


#define INIT_SELF_DEFINE_INFO(p, sigID, sigName, sigValueType, authorityLevel, \
	valueDisplayFmt, settingStep, maxValidValue, minValidValue, \
	stateNum, stateText, sectionDataStru) \
	((p)->iSigID = (sigID),  \
	(p)->pSigName = (sigName),  \
	(p)->iSigValueType = (sigValueType),  \
	(p)->byAuthorityLevel = (authorityLevel), \
	(p)->bCanBeCtrlledorSet = TRUE, \
	(p)->szValueDisplayFmt = (valueDisplayFmt),  \
	(p)->fSettingStep = (settingStep),  \
	(p)->fMaxValidValue = (maxValidValue),  \
	(p)->fMinValidValue = (minValidValue), \
	(p)->iStateNum = (stateNum),  \
	(p)->pStateText = (stateText),  \
	(p)->pSectionDataStru = (sectionDataStru))


enum DEF_SCREEN_DIS_INFO_ENUM
{
	GET_SIGNAL_PROMPT = 0,	//need to be GET_PROMPT_INFO
	GET_SIGNAL_VALUE  = 1,	//need to be GET_VALUE_FROM_SYSTEM
	GET_FIXED_PROMPT  = 2,
};

typedef struct tagDefaultScreenItemCfg
{
	short	x;
	short	y;

	int		nStdEquipID;
	int		nSigType;
	int		nSigID;

	int		nDisPromptOrValueFlag;//0:sig prompt, 1:sig value, 2 fix prompt

	int		nPromptInfoLangID;

	int		nDispBigSizeFlag;	//0:Normal Size,	1:Big Size
	
	int		nEquipID;

}DEFAULT_SCREEN_ITEM_CFG;

typedef struct tagDefaultScreenCfgReader
{
	int						nCfgItemNum;

	DEFAULT_SCREEN_ITEM_CFG*	pDefaultScreenItemCfg;

}DEFAULT_SCREEN_CFG_READER;








#ifdef _SUPPORT_THREE_LANGUAGE
#define SYS_LANG_NUM_ONE_TIME	3  //In scu plus,it need three languages
#else
#define SYS_LANG_NUM_ONE_TIME	2
#endif

#define YDN23_PORT_TYPE_NUM		3
#define YDN23_PORT_PARAM_NUM	6
#define	YDN23_CALL_BACK_ENB_NUM	2

/* for alarm sig value */
typedef struct tagAlarmSigValueExLCD ALARM_SIG_VALUE_EX_LCD;
struct tagAlarmSigValueExLCD
{
	ALARM_SIG_VALUE_EX_LCD	*next;		 /* NOTE: MUST BE THE FIRST  ITEM! basic value */
	
	ALARM_SIG_VALUE	bvAlarm;					 
	
	int				nAlarmMsgType;
	BOOL			bAlarmAck;	
};

extern LANG_TEXT *GetLCDLangText(IN int iResourceID, 
	IN int iLen, 
	IN LANG_TEXT *pLangTextList);


char* GetCurrentLangCode(void);

char* GetLangCode(int nLaguageFlag);

BOOL GetCurrentLangFlag(void);

BOOL SetCurrentLangFlag(int iLangFlag);

BOOL SetCurrentLang(int nCurrentLangValue, BOOL bOnlyChangeFlag);


LANG_FILE* GetLCDLangFile(void);

DWORD ServiceInit(IN int argc,		// arg num.
				IN char **argv);		// args


DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs);

int HandleIdle(void);

void KeyPressForeOperation(void);

BOOL FeedDogOfLCDThread(void);

LANG_TEXT *GetLCDLangText(IN int iResourceID, 
	IN int iLen, 
	IN LANG_TEXT *pLangTextList);


int SetSignalValueInterface(int nEquipID, 
						int nSigType, 
						int nSigID,
						VAR_VALUE	vdData,
						int	nSenderType,
						int nSendDirectly,
						int	nTimeout);

BOOL ConvertTime(time_t* pTime, BOOL bUTCToLocal);
BOOL GetLocalTimeOfACU(time_t* pTime);
BOOL SetLocalTimeOfACU(time_t* pTime);


int SetLCDRotation(int iRotationAngle);


UCHAR GetKeyEx(void);

int RefreshSubMenuStatus(SUB_MENU* pSubMenu, BOOL bRecursive);




#ifdef __cplusplus
}
#endif

#endif //__LCD_PUBLIC_H__041008

 /*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_default_screen.c
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:47
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

//Frank Wu,20160127, for MiniNCU
#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/resource.h>		/* setrlimit */
#include <sys/types.h>			/* socket, bind, accept */
#include <sys/socket.h>			/* socket, bind, accept, setsockopt, */
#include <netdb.h>
#include <net/route.h>			/*rtentry*/
#include <net/if.h>				/*ifreq */


#include "stdsys.h"  
#include "public.h" 
#include "app_conf.h"

#include "lcd_public.h"
#include "lcd_default_screen.h"

#include "lcd_item_handle.h"

#include "lcd_main.h"
extern g_iWorkMode; //add by Jimmy 2011/10/28

extern int DoModal_MenuScreen(MENU_DISPLAY* pMenuDisplay, 
							  GLOBAL_VAR_OF_LCD* pThis);

extern DEFAULT_SCREEN_CFG_READER* GetDefMenuCfgReader(void);

extern int ConvertValue_ForDefMenu(MENU_ITEM*	pMenuItem,
							int			nBufLength, 
							char*		pDisBuf,
							int			nGetType);

extern int DoModal_AcknowlegeDlg_INFO(char* pHeadingInfo, 
							   char* pPromptInfo1, 
							   char* nPromptInfo2,
							   char* nPromptInfo3,
							   int	nTimeout);

extern int DoModal_AcknowlegeDlg_ID(int nHeadingInfoID, 
							 int nPromptInfoID1, 
							 int nPromptInfoID2,
							 int nPromptInfoID3,
							 int nTimeout);


extern int WriteLcdVar(char *pVarName, int nValue);


int	HandleAutoConfig(MENU_DISPLAY* pMenuDisplay,
			 GLOBAL_VAR_OF_LCD* pThis);

#define DEFAULT_SCREEN_DEFAULT_ITEM			1

static int g_s_nScreenItemNum = 1;
static int g_s_nCurScreenItem = DEFAULT_SCREEN_DEFAULT_ITEM;


//ALl the first line
#define CURRENT_TIME_DIS_FORMAT	"15"//SCREEN_WIDTH/8

#define CHINESE_LANG_CODE		"zh"

#define SWITCH_DATE_TIME_INTERVAL 3//times of INTERVAL_REFRESH_DATA

#define RECT_BASE_HEIGHT     8

/*==========================================================================*
 * FUNCTION : DisCurTimeOnScreen
 * PURPOSE  : Display current time on default screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char*  pLangCode : current language code
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 14:36
 *==========================================================================*/
static int DisCurTimeOnScreen(char* pLangCode)
{
	int		nError = ERR_LCD_OK;

	time_t	tCurtime;
	struct tm tmCurTime;

	char	szDisBuf[MAX_CHARS_OF_LINE];
	char	szTempBuf[MAX_CHARS_OF_LINE];
	char	szDisplayFormat[MAX_CHARS_OF_LINE];

	static int s_SwitchDateTimeFlag = 1;
	static int s_SwitchTakeCount = 0;


	ASSERT(pLangCode);

	GetLocalTimeOfACU(&tCurtime);

	if(++s_SwitchTakeCount >= SWITCH_DATE_TIME_INTERVAL)
	{
		s_SwitchTakeCount = 0;

		s_SwitchDateTimeFlag = (s_SwitchDateTimeFlag == 0) ? 1 : 0;
	}


	if(s_SwitchDateTimeFlag == 0)
	{
		strftime(szTempBuf, sizeof(szTempBuf), "%H:%M:%S", 
			gmtime_r(&tCurtime, &tmCurTime));
	}
	else
	{
		////According to SCU
		//if(strcasecmp(pLangCode, CHINESE_LANG_CODE) == 0)
		//{
		//	strftime(szTempBuf, sizeof(szTempBuf), "%Y-%m-%d", 
		//		gmtime_r(&tCurtime, &tmCurTime));
		//}
		//else
		//{
		//	strftime(szTempBuf, sizeof(szTempBuf), "%b.%d.%Y", 
		//		gmtime_r(&tCurtime, &tmCurTime));
		//}
		strftime(szTempBuf, sizeof(szTempBuf), "%Y-%m-%d", 
			gmtime_r(&tCurtime, &tmCurTime));

	}

	snprintf(szDisplayFormat, sizeof(szDisplayFormat), 
		"%%%ss", CURRENT_TIME_DIS_FORMAT);

	snprintf(szDisBuf, sizeof(szDisBuf), szDisplayFormat, szTempBuf);

	DisString(szDisBuf, 
		(int)strlen(szDisBuf),  
		0,
		0,
		pLangCode, 
		FLAG_DIS_NORMAL_TYPE);	

	return nError;
}

static int Compare_RECT_FILL_STRU(const RECT_FILL_STRU *p1,
								  const RECT_FILL_STRU *p2)
{
	if(p1->y > p2->y)
	{
		return 1;
	}

	if(p1->y < p2->y)
	{
		return -1;
	}

	if(p1->x > p2->x)
	{
		return 1;
	}

	if(p1->x < p2->x)
	{
		return -1;
	}


	return 0;
}


//insert sort
static void MakeRectStruSort(RECT_FILL_STRU* pRectFillStru, int nFillStruNum)
{
		qsort(pRectFillStru, (size_t)nFillStruNum, sizeof(*pRectFillStru),
			(int(*)(const void *, const void *))Compare_RECT_FILL_STRU);
}



#define MAX_DEFAULT_SCREEN_DIS_NUM 20

/*==========================================================================*
 * FUNCTION : DisplayDefaultScreen
 * PURPOSE  : Display default screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*  pMenuDisplay : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 14:41
 *==========================================================================*/
static int DisplayDefaultScreen(MENU_DISPLAY* pMenuDisplay)
{
	DEFAULT_SCREEN_ITEM_CFG*		pDefaultScreenItemCfg;

	DEFAULT_SCREEN_CFG_READER*	pDefMenuCfgReader = GetDefMenuCfgReader();

	int			i, j, nMenuItemNum;

	MENU_ITEM	stMenuItem;
	void*		pSigInfo;

	int			nBufLen;
	int			nEquipId, nSignalType, nSignalID;

	char		szDisBuf[MAX_CHARS_OF_LINE];

	char		cDispType = FLAG_DIS_NORMAL_TYPE;

	char*		pLangCode = GetCurrentLangCode();

	int			nFontHeight = GetFontHeightFromCode(pLangCode);

	RECT_FILL_STRU	stRectFillStru[MAX_DEFAULT_SCREEN_DIS_NUM];
	int				nActualFillStruNum = 0;

	int nDrawWidth;

	RECT_FILL_STRU* pRectFillStru;

	int			nGetType = GET_VALUE_FROM_SYSTEM;

	UNUSED(pMenuDisplay);

	ASSERT(pLangCode);

	memset(stRectFillStru, 0, sizeof(stRectFillStru));

	if (g_s_nCurScreenItem == 0)
	{
		//Display time at first line
		DisCurTimeOnScreen(pLangCode);

		//Collect the not-be-filled area
		for(j = 0; j < nFontHeight / RECT_BASE_HEIGHT; 
			j++, nActualFillStruNum++)
		{
			stRectFillStru[nActualFillStruNum].x = 0;
			stRectFillStru[nActualFillStruNum].y = j * RECT_BASE_HEIGHT;

			stRectFillStru[nActualFillStruNum].nWidth = SCREEN_WIDTH;

		}
	}


	ASSERT(pDefMenuCfgReader);

	pDefaultScreenItemCfg = pDefMenuCfgReader->pDefaultScreenItemCfg;

	nMenuItemNum = pDefMenuCfgReader->nCfgItemNum;

	//Display the configurable signals in turn.
	for(i = 0; i < nMenuItemNum && pDefaultScreenItemCfg != NULL; 
		i++, pDefaultScreenItemCfg++)
	{
		if ((pDefaultScreenItemCfg->y)/SCREEN_HEIGHT != g_s_nCurScreenItem)
		{
			continue;//Don't display on this screen
		}

		if (pDefaultScreenItemCfg->nDisPromptOrValueFlag != GET_FIXED_PROMPT)
		{
			nEquipId = 
				DXI_GetEquipIDFromStdEquipID(pDefaultScreenItemCfg->nStdEquipID);

			if(nEquipId == -1)
			{
				continue;//No this equipment
			}
			
			if(pDefaultScreenItemCfg->nEquipID > 0)
			{
				nEquipId = pDefaultScreenItemCfg->nEquipID;
			}
			
			/////////add by jimmy to avoid display batt info
			//if(g_iWorkMode == WORK_MODE_SLAVE)
			//{
			//	if(nEquipId == 115)
			//	{
			//		continue; 
			//	}
			//}
			//////////////////
			nSignalType = pDefaultScreenItemCfg->nSigType;
			nSignalID = pDefaultScreenItemCfg->nSigID;

			if (DxiGetData(VAR_A_SIGNAL_INFO_STRU,
				nEquipId,			
				DXI_MERGE_SIG_ID(nSignalType, nSignalID),
				&nBufLen,			
				&pSigInfo,			
				0) != ERR_DXI_OK)
			{
				continue;
			}

			ASSERT(pSigInfo);

			memset(&stMenuItem, 0, sizeof(stMenuItem));

			INIT_SIGNAL_ITEM(&stMenuItem, 
				0,
				DXI_MERGE_UNIQUE_SIG_ID(nEquipId, nSignalType, nSignalID),
				FALSE, 
				FALSE,
				pSigInfo);

			nGetType = pDefaultScreenItemCfg->nDisPromptOrValueFlag;

			if(ConvertValue_ForDefMenu(&stMenuItem,
				sizeof(szDisBuf), 
				szDisBuf,
				nGetType)
				== RET_INVALID_DATA)
			{
				cDispType = FLAG_DIS_REVERSE_TYPE;
				continue;
			}
			else
			{
				cDispType = FLAG_DIS_NORMAL_TYPE;
			}
		}
		else
		{
			LANG_TEXT* pLangText;

			int nCurrentLangFlag = GetCurrentLangFlag();

			LANG_FILE* pLCDLangFile = GetLCDLangFile();

			ASSERT(pLCDLangFile);

			pLangText = GetLCDLangText(
				pDefaultScreenItemCfg->nPromptInfoLangID, 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);

			ASSERT(pLangText);

			strncpyz(szDisBuf, 
				pLangText->pAbbrName[nCurrentLangFlag],
				sizeof(szDisBuf));

			cDispType = FLAG_DIS_NORMAL_TYPE;
		}

		//Do not use assert, let program operation, but do not display 
		if(pDefaultScreenItemCfg->x < 0
			|| pDefaultScreenItemCfg->y < nFontHeight//The first row display time
			|| pDefaultScreenItemCfg->y % RECT_BASE_HEIGHT != 0
			|| pDefaultScreenItemCfg->x >= SCREEN_WIDTH
			|| ((pDefaultScreenItemCfg->y) % SCREEN_HEIGHT) 
			> SCREEN_HEIGHT - nFontHeight
			/*|| pDefaultScreenItemCfg->y > SCREEN_HEIGHT - nFontHeight*/)
		{
			continue;
		}

		DisString(szDisBuf, 
			(int)strlen(szDisBuf),  
			pDefaultScreenItemCfg->x,
			(pDefaultScreenItemCfg->y) % SCREEN_HEIGHT,
			pLangCode, 
			cDispType);

		nDrawWidth = 
			GetWidthOfString(szDisBuf, (int)strlen(szDisBuf), pLangCode);

		//Collect the not-be-filled area
		for(j = 0; j < nFontHeight / RECT_BASE_HEIGHT;
			j++, nActualFillStruNum++)
		{
			stRectFillStru[nActualFillStruNum].x = pDefaultScreenItemCfg->x;
			
			stRectFillStru[nActualFillStruNum].y = 
				(pDefaultScreenItemCfg->y % SCREEN_HEIGHT) 
				+ j * RECT_BASE_HEIGHT;

			stRectFillStru[nActualFillStruNum].nWidth = nDrawWidth;

		}

	}

	//If only one screen, do not display progress arrow
	DisplayProgressArrow(g_s_nScreenItemNum,
		g_s_nCurScreenItem,
		0,
		0);

	//Collect the not-be-filled area, top arrow
	for(j = 0; j < nFontHeight / RECT_BASE_HEIGHT; j++, nActualFillStruNum++)
	{
		stRectFillStru[nActualFillStruNum].x = SCREEN_WIDTH - ARROW_WIDTH;
		stRectFillStru[nActualFillStruNum].y = j * RECT_BASE_HEIGHT;

		stRectFillStru[nActualFillStruNum].nWidth = ARROW_WIDTH;

	}
	//Collect the not-be-filled area, bottom arrow
	for(j = 0; j < nFontHeight / RECT_BASE_HEIGHT; j++, nActualFillStruNum++)
	{
		stRectFillStru[nActualFillStruNum].x = SCREEN_WIDTH - ARROW_WIDTH;

		stRectFillStru[nActualFillStruNum].y = 
			SCREEN_HEIGHT - nFontHeight + j * RECT_BASE_HEIGHT;

		stRectFillStru[nActualFillStruNum].nWidth = ARROW_WIDTH;

	}

	//Clear all the not-be-filled area
	MakeRectStruSort(stRectFillStru, nActualFillStruNum);

	pRectFillStru = stRectFillStru;

	for(i = 0/*nFontHeight*/; 
		i < SCREEN_HEIGHT && pRectFillStru != NULL;
		i += RECT_BASE_HEIGHT)
	{
		BOOL bRowFirstItem = TRUE;

		while(pRectFillStru && pRectFillStru->y == i)
		{
			if(bRowFirstItem)
			{
				ClearBar(0, i, pRectFillStru->x, 
					RECT_BASE_HEIGHT, FLAG_DIS_NORMAL_TYPE);
			}

			if((pRectFillStru + 1) != NULL)
			{
				if((pRectFillStru + 1)->y == i)
				{
					if((pRectFillStru + 1)->x > 
						pRectFillStru->x + pRectFillStru->nWidth)
					{
						ClearBar(pRectFillStru->x + pRectFillStru->nWidth, 
							i, 
							(pRectFillStru + 1)->x - 
							(pRectFillStru->x + pRectFillStru->nWidth), 
							RECT_BASE_HEIGHT, 
							FLAG_DIS_NORMAL_TYPE);
					}
				}
				else
				{
					ClearBar(pRectFillStru->x + pRectFillStru->nWidth, 
						i, 
						SCREEN_WIDTH - 
						(pRectFillStru->x + pRectFillStru->nWidth), 
						RECT_BASE_HEIGHT, 
						FLAG_DIS_NORMAL_TYPE);
				}
			}
			else
			{
				break;
			}

			pRectFillStru++;

			bRowFirstItem = FALSE;
		}

		//Have no filled items
		if (bRowFirstItem)
		{
			ClearBar(0, 
				i, 
				SCREEN_WIDTH, 
				RECT_BASE_HEIGHT, 
				FLAG_DIS_NORMAL_TYPE);
		}
	}
	

	return ERR_LCD_OK;
}

static int GetScreenNumVarConfig(void)
{
	int i;
	int nMaxScreen = 1;
	int nScreen;
	DEFAULT_SCREEN_CFG_READER*	pDefMenuCfgReader = GetDefMenuCfgReader();

	if (pDefMenuCfgReader == NULL)
	{
		return 1;
	}

	for (i = 0; i < pDefMenuCfgReader->nCfgItemNum; i++)
	{
		nScreen =
			((pDefMenuCfgReader->pDefaultScreenItemCfg[i]).y)/SCREEN_HEIGHT;
		if (nMaxScreen < nScreen + 1)
		{
			nMaxScreen = nScreen + 1;
		}
	}

	 return nMaxScreen; 
}





static BOOL DisAcuVersionsOnScreen(void)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "Fail to Get";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "Ver Info by DXI";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	ACU_PRODUCT_INFO sAcuProductInfo;

	int		nBufLen;

	ASSERT(pLCDLangFile);

	memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));

	nBufLen = sizeof(sAcuProductInfo);

	if (DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		ACU_PRODUCT_INFO_GET, 
		0, 
		&nBufLen,
		&(sAcuProductInfo),
		0) == ERR_DXI_OK)
	{
		//��Ʒ��Ϣ
		pLangText = GetLCDLangText(
			ACU_PRODUCT_INFO_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		if(pLangText)
		{
			snprintf(szHeadingInfo, sizeof(szHeadingInfo),
				"%-10.10s%.6s",
				pLangText->pAbbrName[nCurrentLangFlag],
				sAcuProductInfo.szProductNo);

		}

		//ϵ�к�
		pLangText = GetLCDLangText(
			ACU_SERIAL_NUMBER_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		if(pLangText)
		{
			snprintf(szPromptInfo1, sizeof(szPromptInfo1),
				"%-10.10s%.6s",
				pLangText->pAbbrName[nCurrentLangFlag],
				sAcuProductInfo.szACUSerialNo);

		}

		if (strlen(sAcuProductInfo.szACUSerialNo) > 6)
		{
			snprintf(szPromptInfo2, sizeof(szPromptInfo2),
				"%s", sAcuProductInfo.szACUSerialNo + 6);
		}

		//����汾
		pLangText = GetLCDLangText(
			ACU_SOFTWARE_VERSION_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		if(pLangText)
		{

			snprintf(szPromptInfo3, sizeof(szPromptInfo3),
				"%-9.9s%.7s",
				pLangText->pAbbrName[nCurrentLangFlag],
				sAcuProductInfo.szSWRevision);
		}

	}

#define DISPLAY_SN_AND_VERSION_TIMEOUT	5000
	return DoModal_AcknowlegeDlg_INFO(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		DISPLAY_SN_AND_VERSION_TIMEOUT);

}



static BOOL DisAcuOtherInfoOnScreen(void)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	ACU_PRODUCT_INFO sAcuProductInfo;

	int		nBufLen;

	ASSERT(pLCDLangFile);

	memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));

	nBufLen = sizeof(sAcuProductInfo);

	if (DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		ACU_PRODUCT_INFO_GET, 
		0, 
		&nBufLen,
		&(sAcuProductInfo),
		0) == ERR_DXI_OK)
	{
		//Ӳ���汾
		pLangText = GetLCDLangText(
			ACU_HARDWARE_VERSION_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		if(pLangText)
		{
			snprintf(szHeadingInfo, sizeof(szHeadingInfo),
				"%-10.10s%.6s",
				pLangText->pAbbrName[nCurrentLangFlag],
				sAcuProductInfo.szHWRevision);

		}

		//MAC��ַ
		pLangText = GetLCDLangText(
			ACU_ETHADDR_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		if(pLangText)
		{
			snprintf(szPromptInfo1, sizeof(szPromptInfo1),
				"%-10.10s%.6s",
				pLangText->pAbbrName[nCurrentLangFlag],
				sAcuProductInfo.szEthaddr);

		}

		if (strlen(sAcuProductInfo.szEthaddr) > 6)
		{
			snprintf(szPromptInfo2, sizeof(szPromptInfo2),
				"%s", sAcuProductInfo.szEthaddr + 6);
		}

		//�ļ�ϵͳ�汾
		pLangText = GetLCDLangText(
			ACU_FILE_SYSTEM_VERSION_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		if(pLangText)
		{
			snprintf(szPromptInfo3, sizeof(szPromptInfo3),
				"%-10.10s%.6s",
				pLangText->pAbbrName[nCurrentLangFlag],
				sAcuProductInfo.szFileSystemVersion);

		}

	}

	return DoModal_AcknowlegeDlg_INFO(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		DISPLAY_SN_AND_VERSION_TIMEOUT);

}

static BOOL DisAcuThirdInfoOnScreen(void)
{
	char	szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char	szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char	szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char	szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	LANG_TEXT*	pLangText;
	
	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE*	pLCDLangFile = GetLCDLangFile();	//get lcd private cfg.

	ACU_PRODUCT_INFO	sAcuProductInfo;			//ACU_PRODUCT_INFO struct which get from data exhchange module

	int		nBufLen;
	
	ASSERT(pLCDLangFile);

	memset(&sAcuProductInfo,0,sizeof(sAcuProductInfo));

	nBufLen = sizeof(sAcuProductInfo);

	if(DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		ACU_PRODUCT_INFO_GET, 
		0, 
		&nBufLen,
		&(sAcuProductInfo),
		0) == ERR_DXI_OK)
	{
		//scup_product_info_get already which include cfg version
		pLangText = GetLCDLangText(
			ACU_CONFIG_VERSION_ID,
			pLCDLangFile->iLangTextNum,
			pLCDLangFile->pLangText);

		if(pLangText)
		{
			snprintf(szHeadingInfo,sizeof(szHeadingInfo),
				"%-16.16s",
				pLangText->pAbbrName[nCurrentLangFlag]);

			snprintf(szPromptInfo1,sizeof(szPromptInfo1),
				"%-16.16s",
				sAcuProductInfo.szCfgFileVersion);
		}

	}
	return DoModal_AcknowlegeDlg_INFO(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		DISPLAY_SN_AND_VERSION_TIMEOUT);
}

//Frank Wu,20160127, for MiniNCU
static BOOL DisMiniNCUSelfInfoOnScreen(int iPageIndex)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	ACU_PRODUCT_INFO sAcuProductInfo;

	int		nBufLen;
	int		iError;
	
	char	*pszHeadingInfoName = NULL;
	char	*pszHeadingInfoValue = NULL;
	char	*pszPromptInfo1Name = NULL;
	char	*pszPromptInfo1Value = NULL;

	ASSERT(pLCDLangFile);

	if(0 == iPageIndex)
	{
		//query data
		memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
		nBufLen = sizeof(sAcuProductInfo);

		if (DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			ACU_PRODUCT_INFO_GET, 
			0, 
			&nBufLen,
			&(sAcuProductInfo),
			0) == ERR_DXI_OK)
		{
			pszHeadingInfoValue = sAcuProductInfo.szPartNumber;
			pszPromptInfo1Value = sAcuProductInfo.szACUSerialNo;
		}
	
		//output
		//�ͺ�
		pszHeadingInfoName = "";
		snprintf(szHeadingInfo, sizeof(szHeadingInfo),
			"%s%s",
			pszHeadingInfoName,
			pszHeadingInfoValue);

		//ϵ�к�
		pszPromptInfo1Name = "SN";
		snprintf(szPromptInfo1, sizeof(szPromptInfo1),
			"%s:%s",
			pszPromptInfo1Name,
			pszPromptInfo1Value);
	}
	else if(1 == iPageIndex)
	{
		//query data
		memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
		nBufLen = sizeof(sAcuProductInfo);

		if (DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			ACU_PRODUCT_INFO_GET, 
			0, 
			&nBufLen,
			&(sAcuProductInfo),
			0) == ERR_DXI_OK)
		{
			pszHeadingInfoValue = sAcuProductInfo.szHWRevision;
			pszPromptInfo1Value = sAcuProductInfo.szSWRevision;
		}
	
		//output
		//Ӳ���汾
		pszHeadingInfoName = "HW Ver";
		pLangText = GetLCDLangText(
			ACU_HARDWARE_VERSION_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if(pLangText)
		{
			pszHeadingInfoName = pLangText->pAbbrName[nCurrentLangFlag];
		}
		snprintf(szHeadingInfo, sizeof(szHeadingInfo),
			"%s:%s",
			pszHeadingInfoName,
			pszHeadingInfoValue);

		//����汾
		pszPromptInfo1Name = "SW Ver";
		pLangText = GetLCDLangText(
			ACU_SOFTWARE_VERSION_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if(pLangText)
		{
			pszPromptInfo1Name = pLangText->pAbbrName[nCurrentLangFlag];
		}
		snprintf(szPromptInfo1, sizeof(szPromptInfo1),
			"%s:%s",
			pszPromptInfo1Name,
			pszPromptInfo1Value);
	}
	else if(2 == iPageIndex)
	{
		//query data
		memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
		nBufLen = sizeof(sAcuProductInfo);

		if (DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			ACU_PRODUCT_INFO_GET, 
			0, 
			&nBufLen,
			&(sAcuProductInfo),
			0) == ERR_DXI_OK)
		{
			pszHeadingInfoValue = sAcuProductInfo.szCfgFileVersion;
			pszPromptInfo1Value = sAcuProductInfo.szFileSystemVersion;
		}
	
		//output
		//���ð汾
		pszHeadingInfoName = "Cfg Ver";
		pLangText = GetLCDLangText(
			ACU_CONFIG_VERSION_ID,
			pLCDLangFile->iLangTextNum,
			pLCDLangFile->pLangText);
		if(pLangText)
		{
			pszHeadingInfoName = pLangText->pAbbrName[nCurrentLangFlag];
		}
		snprintf(szHeadingInfo, sizeof(szHeadingInfo),
			"%s:%s",
			pszHeadingInfoName,
			pszHeadingInfoValue);

		//�ļ�ϵͳ�汾
		pszPromptInfo1Name = "FileSys";
		pLangText = GetLCDLangText(
			ACU_FILE_SYSTEM_VERSION_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if(pLangText)
		{
			pszPromptInfo1Name = pLangText->pAbbrName[nCurrentLangFlag];
		}
		snprintf(szPromptInfo1, sizeof(szPromptInfo1),
			"%s:%s",
			pszPromptInfo1Name,
			pszPromptInfo1Value);
	}
	else if(3 == iPageIndex)
	{
		//query data
		char	szTempIp[64];
		ULONG	ulIP;

		memset(szTempIp, 0, sizeof(szTempIp));
		iError = DxiGetData(VAR_ACU_NET_INFO,
			NET_INFO_IP,			
			0,		
			&nBufLen,			
			&ulIP,			
			0);
		if(ERR_DXI_OK == iError)
		{
			inet_ntop(AF_INET, (void*)&ulIP, szTempIp, sizeof(szTempIp));
		}

		//output
		pszHeadingInfoName = "IP";
		//if(strlen(pszHeadingInfoName) + strlen(szTempIp) <= 16)
		//{
		//	//IP����+��ַ
		//	snprintf(szHeadingInfo, sizeof(szHeadingInfo),
		//		"%s:%s",
		//		pszHeadingInfoName,
		//		szTempIp);
		//}
		//else
		{
			//IP����
			snprintf(szHeadingInfo, sizeof(szHeadingInfo),
				"%s:",
				pszHeadingInfoName);
				
			//IP��ֵַ
			snprintf(szPromptInfo1, sizeof(szPromptInfo1),
				"%14s",
				szTempIp);
		}
	}
	else if(4 == iPageIndex)
	{
		//query data
		memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));
		nBufLen = sizeof(sAcuProductInfo);

		if (DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			ACU_PRODUCT_INFO_GET, 
			0, 
			&nBufLen,
			&(sAcuProductInfo),
			0) == ERR_DXI_OK)
		{
			pszPromptInfo1Value = sAcuProductInfo.szEthaddr;
		}

		//output
		//MAC��ַ
		pszHeadingInfoName = "MAC";
		if(NULL == pszPromptInfo1Value)
		{
			//IP����+ NULL
			snprintf(szHeadingInfo, sizeof(szHeadingInfo),
				"%s:%s",
				pszHeadingInfoName,
				pszPromptInfo1Value);
		}
		else
		{
			//00:09:F7:8F:18:32
			char	szMACHead[32];//00:09:F7:
			char	szMACTail[32];//8F:18:32

			strncpyz(szMACHead, pszPromptInfo1Value, 9 + 1);
			strncpyz(szMACTail, pszPromptInfo1Value + 9, 9 + 1);
			
			//MAC:  00:09:F7:
			//      8F:18:32
			//IP����
			snprintf(szHeadingInfo, sizeof(szHeadingInfo),
				"%s:%10s",
				pszHeadingInfoName,
				szMACHead);

			//IP��ֵַ
			snprintf(szPromptInfo1, sizeof(szPromptInfo1),
				"%13s",
				szMACTail);
		}
	}

	return DoModal_AcknowlegeDlg_INFO(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		DISPLAY_SN_AND_VERSION_TIMEOUT);
}



/*==========================================================================*
 * FUNCTION : DoModal_DefaultScreen
 * PURPOSE  : All the handle on default screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*       pMenuDisplay : 
 *            GLOBAL_VAR_OF_LCD*  pThis        : 
 * RETURN   : int : if return, return ERR_LCD_OK
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 14:48
 *==========================================================================*/
int DoModal_DefaultScreen(MENU_DISPLAY* pMenuDisplay,
						  GLOBAL_VAR_OF_LCD* pThis)
{
	ASSERT(pMenuDisplay);
	ASSERT(pThis);

	UCHAR byKeyValue;

	BOOL bRefreshAtOnce = TRUE;

	BOOL bBrightChanged = FALSE;

	STACK_PVOID* pStack;

	pStack	= &(pMenuDisplay->pMainMenu->stack);

	g_s_nScreenItemNum = GetScreenNumVarConfig();

	ClearScreen();

	DisplayDefaultScreen(pMenuDisplay);

	for(;;)
	{
		byKeyValue = GetKeyEx();

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();

			switch(byKeyValue)
			{
			case VK_UP:  
				{
					if (g_s_nScreenItemNum > 1 )
					{
						g_s_nCurScreenItem = 
							((g_s_nCurScreenItem - 1) >= 0)
							? g_s_nCurScreenItem - 1
							: g_s_nScreenItemNum - 1;

						bRefreshAtOnce = TRUE;
					}
				}
				break;

			case VK_DOWN:
				{
					if (g_s_nScreenItemNum > 1 )
					{
						g_s_nCurScreenItem = 
							((g_s_nCurScreenItem + 1) < g_s_nScreenItemNum)
							? g_s_nCurScreenItem + 1
							: 0;

						bRefreshAtOnce = TRUE;
					}
				}
				break;

			case VK_ESCAPE:  
				{
					if(g_s_nCurScreenItem != DEFAULT_SCREEN_DEFAULT_ITEM)
					{
						g_s_nCurScreenItem = DEFAULT_SCREEN_DEFAULT_ITEM;
						bRefreshAtOnce = TRUE;
						break;
					}
					else
					{
						//�������ڲ��Բ����󣺰�n��ESC(n>5)���ٰ�ENT����Ҫ�������˵�����
						//int nRet = DisAcuVersionsOnScreen();
						//if(nRet == ID_CANCEL)	//ESC
						//{
						//	nRet = DisAcuOtherInfoOnScreen();
						//	if(nRet == ID_CANCEL)
						//	{
						//		DisAcuThirdInfoOnScreen();
						//	}
						//	break;
						//}
						//else if (nRet != ID_RETURN)
						//{
						//	bRefreshAtOnce = TRUE;
						//	break;
						//}
						//else	//if ENTER pressed, DoModal_MenuScreen()
						//{
						//	// NO BREAK
						//}
#if 0
						if(DisAcuVersionsOnScreen() == ID_RETURN)
						{
							if(DisAcuOtherInfoOnScreen() == ID_RETURN)
							{
								DisAcuThirdInfoOnScreen();
							}
						}
#endif
						
						if((ID_RETURN == DisMiniNCUSelfInfoOnScreen(0))
							&& (ID_RETURN == DisMiniNCUSelfInfoOnScreen(1))
							&& (ID_RETURN == DisMiniNCUSelfInfoOnScreen(2))
							&& (ID_RETURN == DisMiniNCUSelfInfoOnScreen(3))
							&& (ID_RETURN == DisMiniNCUSelfInfoOnScreen(4))
							)
						{
							//none
						}
						
						bRefreshAtOnce = TRUE;
						
						break;

					}
				}
				//break;

			case VK_ENTER:
				{
					//Save the Bright Value to LCD Env Var.
					if(bBrightChanged)
					{
						TRACE("Save the LCD Bright Value:%d.\n", GetLCDBright());
						bBrightChanged = FALSE;
						WriteLcdVar(NULL, GetLCDBright());
					}

					//If ENT key be pressed, enter into menu screen
					DoModal_MenuScreen(pMenuDisplay, pThis);

					//�����ڲ˵�������������Ļ�߶ȣ���Ҫ���¼���
					g_s_nScreenItemNum = GetScreenNumVarConfig();

					bRefreshAtOnce = TRUE;

					ClearScreen();

				}
				break;

			case VK_ENTER_UP:	//BACK_LIGHT_INC
				{
					AdjustBright(TRUE);
					bBrightChanged = TRUE;
				}
				break;

			case VK_ENTER_DOWN:	//BACK_LIGHT_DEC
				{
					AdjustBright(FALSE);
					bBrightChanged = TRUE;
				}
				break;

			case VK_UP_DOWN:	//Logout
				{
					ASSERT(pThis);
					pThis->cPasswordFlag = NEED_PASSWORD_VALIDATE;
				}
				break;

			case VK_ENTER_ESC:	//Reboot
				{
#define ENTER_TO_REBOOT_TIMEOUT 5000

					if (DoModal_AcknowlegeDlg_ID_Reboot(
						HEADING_ACKNOWLEDGE_INFO_LANG_ID, 
						REBOOT_PROMPT_LANG_ID,
						ENT_KEY_ACKNOWLEDGE_LANG_ID, 
						ESC_KEY_CANCEL_PROMPT_LANG_ID, 
						ENTER_TO_REBOOT_TIMEOUT) == ID_RETURN)
					{
						PromptSysIsRebooting();
						
						DXI_RebootACUorSCU(TRUE, TRUE);
						
						Sleep(ONE_HOUR_MSECOND);
					}
				}
				break;


			default:
				break;
			}

		}
		else
		{
			int nRetHandleIdle = HandleIdle();

			if (nRetHandleIdle == ID_INIT_LCD_JUST_NOW)
			{
				bRefreshAtOnce = TRUE;
			}
			else if (nRetHandleIdle == ID_GOTO_DEF_SCREEN)
			{
				g_s_nCurScreenItem = DEFAULT_SCREEN_DEFAULT_ITEM;

				if (!THREAD_IS_RUNNING(pThis->hThreadSelf))
				{
					//LCD thread should quit
					return ERR_LCD_OK;
				}

			}

			// if App is Auto config, display and wait
			HandleAutoConfig(pMenuDisplay, pThis);
		}


		if(pThis->cDataRefreshFlag == NEED_REFRESH_DATA || bRefreshAtOnce)
		{
			pThis->cDataRefreshFlag = NEED_NOT_REFRESH_DATA;
			bRefreshAtOnce = FALSE;

			DisplayDefaultScreen(pMenuDisplay);

		}
		
	}

}

int	HandleAutoConfig(MENU_DISPLAY* pMenuDisplay,
					 GLOBAL_VAR_OF_LCD* pThis)
{
	int		iWaitTimes = 0;
	if(pThis->cAutoConfigStatus == IS_BACK_TO_DEFAULT_SCRREN)
	{
		pThis->cAutoConfigStatus = IS_WAIT_FOR_CONFIG;
		TRACE("*****LCD*****cAutoConfigStatus = IS_WAIT_FOR_CONFIG\n");		//FXS debug

		do	//wait for WJ
		{
			Timer_Reset(pThis->hThreadSelf,	pThis->nTimerIDAlarmPopAtOnce);
			pThis->cAlarmPopAtOnceFlag = NEED_NOT_ALARM_POP_AT_ONCE;

			AlarmLedCtrl(LED_YEL, TRUE);
			AlarmLedCtrl(LED_RED, FALSE);
			//DoModal_AcknowlegeDlg_ID(
			//	-1,//blank
			//	APP_IS_LANG_ID,
			//	AUTO_CONFIGING_LANG_ID,
			//	-1,//blank
			//	1000);
			DoModal_AcknowlegeDlg_ID_ByLang(
				APP_IS_LANG_ID,
				AUTO_CONFIGING_LANG_ID,
				-1,//blank
				-1,//blank
				1000,
				GetCurrentLangCode(),
				FALSE,
				TRUE);

			AlarmLedCtrl(LED_YEL, FALSE);
			AlarmLedCtrl(LED_RED, TRUE);
			//DoModal_AcknowlegeDlg_ID(
			//	-1,
			//	-1,
			//	PLEASE_WAIT_LANG_ID,
			//	-1,
			//	1000);
				
			DoModal_AcknowlegeDlg_ID_ByLang(
				PLEASE_WAIT_LANG_ID,
				-1,
				-1,
				-1,
				1000,
				GetCurrentLangCode(),
				TRUE,
				TRUE);
		}
		while(pThis->cAutoConfigStatus == IS_WAIT_FOR_CONFIG);
		
		//Wait 30 seconds, wait to scan all equipments.
		do	//wait for WJ
		{
			Timer_Reset(pThis->hThreadSelf,	pThis->nTimerIDAlarmPopAtOnce);
			pThis->cAlarmPopAtOnceFlag = NEED_NOT_ALARM_POP_AT_ONCE;

			AlarmLedCtrl(LED_YEL, TRUE);
			AlarmLedCtrl(LED_RED, FALSE);
			//DoModal_AcknowlegeDlg_ID(
			//	-1,//blank
			//	APP_IS_LANG_ID,
			//	AUTO_CONFIGING_LANG_ID,
			//	-1,//blank
			//	500);
			DoModal_AcknowlegeDlg_ID_ByLang(
				APP_IS_LANG_ID,
				AUTO_CONFIGING_LANG_ID,
				-1,//blank
				-1,//blank
				500,
				GetCurrentLangCode(),
				FALSE,
				TRUE);


			AlarmLedCtrl(LED_YEL, FALSE);
			AlarmLedCtrl(LED_RED, TRUE);
			//DoModal_AcknowlegeDlg_ID(
			//	-1,
			//	-1,
			//	PLEASE_WAIT_LANG_ID,
			//	-1,
			//	500);
			DoModal_AcknowlegeDlg_ID_ByLang(
				PLEASE_WAIT_LANG_ID,
				-1,
				-1,
				-1,
				500,
				GetCurrentLangCode(),
				TRUE,
				TRUE);
				
			iWaitTimes++;
		}
		while(iWaitTimes < 30);

		//Set LEDs after auto config, 2009-11-02
		AlarmLedCtrl(LED_YEL, pThis->nOAnum > 0);
		AlarmLedCtrl(LED_RED, (pThis->nCAnum > 0) || (pThis->nMAnum > 0));

		RefreshSubMenuStatus(pMenuDisplay->pMainMenu->pRootMenu, TRUE);
	}
	return ERR_LCD_OK;
}

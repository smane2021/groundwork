/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_signals_screen.c
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:48
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include "lcd_public.h"

#include "lcd_signals_screen.h"

#include "lcd_item_handle.h"

#include "lcd_ack_dlg.h"


#define CTRL_EXP_SPLITTER "\\n"//Used to analyze control expression


//the maximum unit chars of signal
#define MAX_CHARS_OF_UNIT			5
#define CHARS_OF_UNIT_FOR_DEF_MENU	1//display unit exactly

#define SWITCH_TO_NEW_EQUIP_TIMEOUT 1000


extern BYTE g_byAuthorityLevel;

extern int GetInputUserAndPwd(MENU_DISPLAY* pMenuDisplay, GLOBAL_VAR_OF_LCD* pThis);

extern	SUB_MENU* GetSubMenuFromID(SUB_MENU* pSubMenu, int nSubMenuID);



static int ConvertValueToString(MENU_ITEM*	pMenuItem,
								int			nBufLength, 
								char*		pDisBuf, 
								int			nGetType,
								int			nMaxCharsOfUnit);

static float ValueStep(float fSetValue,float fMaxValue, float fMinValue, 
					   float fSettingStep, BOOL bStepFlag);






static void* GetItemHandleFunc(int nItemID, BOOL bGetPreInfoFunc)
{
	int i = 0;
	int nItemNum = ITEM_HANDLE_FUNC_NUM;

	for(i = 0; i < nItemNum; i++)
	{
		if(g_aItemHandleFunc[i].nItemHandleID == nItemID)
		{
			if(bGetPreInfoFunc)
			{
				return (void*)g_aItemHandleFunc[i].pfItemHandleGetFunc;
			}
			else
			{
				return (void*)g_aItemHandleFunc[i].pfItemHandleSetFunc;
			}
		}
	}

	return NULL;
}

//ignoring the case of the characters
static int strcasefind(char *s1, int c1 )
{
	int  i = 0;
	char p;

	if ('a' <= c1 && c1 <= 'z')
		c1 += 'A' - 'a';

	while (s1[i] != '\0')
	{
		p = s1[i];

		if ('a' <= p && p <= 'z')
			p += 'A' - 'a';

		if (p == c1)
		{
			return i;
		}

		i++;
	}

	return -1;
}



/*==========================================================================*
 * FUNCTION : AmendFloat
 * PURPOSE  : �ֶ���������
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: float  fValue    : 
 *            char*  szDispFmt : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-12-17 13:23
 *==========================================================================*/
static float AmendFloat(float fValue, char* szDispFmt)
{

	//�����ʽ�������¼��֣��򲻴���
	if(strcmp(szDispFmt, ".0")
		&& strcmp(szDispFmt, ".1")
		&& strcmp(szDispFmt, ".2")
		&& strcmp(szDispFmt, ".3"))
	{
		return fValue;
	}

	BOOL bNegative = FALSE;
	if(fValue < 0)
	{
		bNegative = TRUE;
		fValue = 0 - fValue;
	}

	int nDecimal = atoi(szDispFmt + 1);
	int nMultiple = pow(10, nDecimal + 1);			//".0"���10��".1"���100

	int nCvtIntValue = (int)(fValue * nMultiple);
	int nOnes = nCvtIntValue % 10;
	if(nOnes < 5)
	{
		nCvtIntValue = nCvtIntValue - nOnes;
	}
	else	//��λ
	{
		nCvtIntValue = nCvtIntValue - nOnes + 10;
	}
	if(nCvtIntValue == 0)
	{
		return 0;
	}

	fValue = (float)nCvtIntValue / nMultiple;

	return bNegative ? (-1) * fValue : fValue;
	
}



/*==========================================================================*
* FUNCTION : ConvertValueToString
* PURPOSE  : Convert value to string, include prompt info and value info
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*  pMenuItem       : 
*            int         nBufLength      : 
*            char*       pDisBuf         : 
*            int         nGetType        : 
*            int         nMaxCharsOfUnit : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 17:20
*==========================================================================*/
static int ConvertValueToString(MENU_ITEM*	pMenuItem,
								int			nBufLength, 
								char*		pDisBuf, 
								int			nGetType,
								int			nMaxCharsOfUnit)
{
	ASSERT(pMenuItem);

	LANG_TEXT*	pLangText;

	int			nRetDataFlag = RET_VALID_DATA;

	int			nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE*	pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	void*		pDisSigInfo;
	pDisSigInfo = pMenuItem->pvItemData;
	ASSERT(pDisSigInfo);

	if(nGetType == GET_PROMPT_INFO)
	{
		if(LCD_SPLIT_PREFIX(pMenuItem->nMenuItemID) == PREFIX_FIXED_LANG_RES)
		{
			pLangText = GetLCDLangText(
				LCD_SPLIT_LANG_RES_ID(pMenuItem->nMenuItemID), 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);
		}
		else if(IS_BARCODE_ITEM(pMenuItem))
		{
			pLangText = GetLCDLangText(
				LCD_SPLIT_BARCODE_SIGNAL_ID_SIGNAL(pMenuItem->nMenuItemID), 
				pLCDLangFile->iLangTextNum, 
				pLCDLangFile->pLangText);
		}
		else
		{
			//because for all the signal info and self-define info, the language info's offset is
			//the same offset(sizeof(int))
			pLangText = ((SAMPLE_SIG_INFO*)pDisSigInfo)->pSigName;
		}

		ASSERT(pLangText);

		strncpyz(pDisBuf, 
			pLangText->pAbbrName[nCurrentLangFlag],
			nBufLength);

	}
	else if(nGetType == GET_VALUE_FROM_SYSTEM 
		|| nGetType == GET_VALUE_FROM_EDITING)
	{
		DIS_VALUE_PRE_INFO	stDisValuePreInfo;
		char				szDisplayFormat[MAX_CHARS_OF_LINE];

		if (pMenuItem->cMenuType == MT_SELF_DEF_SET
			|| pMenuItem->cMenuType == MT_PASSWORD_SELECT)
		{
			SELF_DEFINE_INFO* pSelfDefineInfo;

			pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
			ASSERT(pSelfDefineInfo);


			DIS_VALUE_GET_PRE_INFO	pGetPreInfoFunc;
			pGetPreInfoFunc = (DIS_VALUE_GET_PRE_INFO)GetItemHandleFunc(pSelfDefineInfo->iSigID, TRUE);
			ASSERT(pGetPreInfoFunc);

			//execute get function
			nRetDataFlag = pGetPreInfoFunc(pMenuItem, &stDisValuePreInfo, nGetType);

		}
		
		else if (pMenuItem->cMenuType == MT_ALARM_LEVEL_SET)
		{
			nRetDataFlag = SetAlarmLevelGetPreInfo(pMenuItem, 
				&stDisValuePreInfo, 
						nGetType);
		}

		else if (pMenuItem->cMenuType == MT_ALARM_RELAY_SET)
		{
			nRetDataFlag = SetAlarmRelayGetPreInfo(pMenuItem, 
				&stDisValuePreInfo, 
				nGetType);
		}

		else if(pMenuItem->cMenuType == MT_BARCODE)
		{
			nRetDataFlag = GetProductPreInfo(pMenuItem, 
				&stDisValuePreInfo, 
				nGetType);
		}

		else //MT_SIGNAL_DISPLAY || MT_SIGNAL_SET || MT_SIGNAL_CONTROL
		{
			nRetDataFlag = GetSignalPreInfo(pMenuItem, 
				&stDisValuePreInfo, 
				nGetType);
		}

		ASSERT(stDisValuePreInfo.szValueDisplayFmt);

		switch (stDisValuePreInfo.iSigValueType)
		{
		case VAR_LONG:
			{
				if(strcmp(stDisValuePreInfo.szSigUnit, "") != 0)
				{
					//maybe 16������ʾ��ʽ
					if (strcasefind(stDisValuePreInfo.szValueDisplayFmt, 'X') >= 0)
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%s%%%ds" , 
							stDisValuePreInfo.szValueDisplayFmt, 
							nMaxCharsOfUnit);
					}
					else
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%sd%%%ds" , 
							stDisValuePreInfo.szValueDisplayFmt, 
							nMaxCharsOfUnit);
					}

					snprintf(pDisBuf, (size_t)nBufLength,
						szDisplayFormat, 
						stDisValuePreInfo.vdData.lValue, 
						stDisValuePreInfo.szSigUnit);
				}
				else
				{
					//maybe 16������ʾ��ʽ
					if (strcasefind(stDisValuePreInfo.szValueDisplayFmt, 'X') >= 0)
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%s" , 
							stDisValuePreInfo.szValueDisplayFmt);
					}
					else
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%sd" , 
							stDisValuePreInfo.szValueDisplayFmt);
					}

					snprintf(pDisBuf, (size_t)nBufLength,
						szDisplayFormat, 
						stDisValuePreInfo.vdData.lValue);
				}

			}
			break;

		case VAR_FLOAT:			
			{

				if(strcmp(stDisValuePreInfo.szSigUnit, "") != 0)	//�е�λ
				{
					//maybe 16������ʾ��ʽ
					if (strcasefind(stDisValuePreInfo.szValueDisplayFmt, 'X') >= 0)
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%s%%%ds" , 
							stDisValuePreInfo.szValueDisplayFmt, 
							nMaxCharsOfUnit);
					}
					else
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%sf%%%ds" , 
							stDisValuePreInfo.szValueDisplayFmt, 
							nMaxCharsOfUnit);
					}

					//�Ը������������������snprintf�������벻׼������
					stDisValuePreInfo.vdData.fValue = AmendFloat(stDisValuePreInfo.vdData.fValue, stDisValuePreInfo.szValueDisplayFmt);

					snprintf(pDisBuf, (size_t)nBufLength,
						szDisplayFormat, 
						stDisValuePreInfo.vdData.fValue, 
						stDisValuePreInfo.szSigUnit);

				}
				else	//�޵�λ
				{
					//maybe 16������ʾ��ʽ
					if (strcasefind(stDisValuePreInfo.szValueDisplayFmt, 'X') >= 0)
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%s" , 
							stDisValuePreInfo.szValueDisplayFmt);
					}
					else
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%sf" , 
							stDisValuePreInfo.szValueDisplayFmt);
					}

					stDisValuePreInfo.vdData.fValue = AmendFloat(stDisValuePreInfo.vdData.fValue, stDisValuePreInfo.szValueDisplayFmt);

					snprintf(pDisBuf, (size_t)nBufLength,
						szDisplayFormat, 
						stDisValuePreInfo.vdData.fValue);
				}

			}
			break;
		case VAR_UNSIGNED_LONG:		
			{
				if(strcmp(stDisValuePreInfo.szSigUnit, "") != 0)
				{
					//maybe 16������ʾ��ʽ
					if (strcasefind(stDisValuePreInfo.szValueDisplayFmt, 'X') >= 0)
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%s%%%ds" , 
							stDisValuePreInfo.szValueDisplayFmt, 
							nMaxCharsOfUnit);
					}
					else
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%sd%%%ds" , 
							stDisValuePreInfo.szValueDisplayFmt, 
							nMaxCharsOfUnit);
					}

					snprintf(pDisBuf, (size_t)nBufLength,
						szDisplayFormat, 
						stDisValuePreInfo.vdData.ulValue, 
						stDisValuePreInfo.szSigUnit);
				}
				else
				{
					//maybe 16������ʾ��ʽ
					if (strcasefind(stDisValuePreInfo.szValueDisplayFmt, 'X') >= 0)
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%s" , 
							stDisValuePreInfo.szValueDisplayFmt);
					}
					else
					{
						snprintf(szDisplayFormat, sizeof(szDisplayFormat),
							"%%%sd" , 
							stDisValuePreInfo.szValueDisplayFmt);
					}

					snprintf(pDisBuf, (size_t)nBufLength,
						szDisplayFormat, 
						stDisValuePreInfo.vdData.ulValue);
				}

			}
			break;

		case VAR_ENUM:		
			{		
				pLangText = (LANG_TEXT*)(stDisValuePreInfo.pStateText[stDisValuePreInfo.vdData.enumValue]);
		
				ASSERT(pLangText);
	
				snprintf(szDisplayFormat, sizeof(szDisplayFormat),
					"%%%ss" , 
					stDisValuePreInfo.szValueDisplayFmt);

				snprintf(pDisBuf, (size_t)nBufLength,
					szDisplayFormat, 
					pLangText->pAbbrName[nCurrentLangFlag]);

			}
			break;

		case VAR_DATE_TIME:		
			{
				DEBUG_LCD_FILE_FUN_LINE_STRING("Handle VAR_DATE_TIME...");

				time_t tm;
				struct tm tmTime;

				tm = (time_t)(stDisValuePreInfo.vdData.dtValue);

				ConvertTime(&tm, TRUE);

				strftime(pDisBuf, (size_t)nBufLength, "%y%m%d %H:%M:%S", 
					gmtime_r(&(tm), &tmTime));

				//if(strcmp(stDisValuePreInfo.szSigUnit, "") != 0)	//�е�λ
				//{
				//	strcat(pDisBuf, stDisValuePreInfo.szSigUnit);
				//}

			}
			break;

		case VAR_PHONE_NO:		// Same Handle, NO break;
		case VAR_STRING:
			{	
				ASSERT(stDisValuePreInfo.pString);

				snprintf(szDisplayFormat, sizeof(szDisplayFormat),
					"%%%ss" ,
					stDisValuePreInfo.szValueDisplayFmt);

				snprintf(pDisBuf, (size_t)nBufLength,
					szDisplayFormat, 
					stDisValuePreInfo.pString);

			}
			break;

		case VAR_MULTI_SECT:		
			{		
				char		szTempBuf[MAX_CHARS_OF_LINE];
				SECTION_DATA_STRU*	pSectionDataStru;
				pSectionDataStru = stDisValuePreInfo.pSectionDataStru;

				memset(pDisBuf, 0, (size_t)nBufLength);

				int i;
				for(i = 0; 
					(i < stDisValuePreInfo.iStateNum) && (pSectionDataStru != NULL); 
					i++, pSectionDataStru++)
				{
					snprintf(szDisplayFormat, sizeof(szDisplayFormat),
						"%%%sd%c" ,
						pSectionDataStru->szValueDisplayFmt,
						pSectionDataStru->cSeparateFlag);

					snprintf(szTempBuf, sizeof(szTempBuf),
						szDisplayFormat, 
						pSectionDataStru->uiValue);

					strcat(pDisBuf, szTempBuf);

				}

			}
			break;

		default:
			{
			}
			break;

		}


	}

	return nRetDataFlag;
}

static BOOL IsStringType(MENU_ITEM* pMenuItem, 
						 SELF_DEFINE_INFO** ppSelfDefineInfo)
{
	ASSERT(pMenuItem);

	if (pMenuItem->cMenuType == MT_SELF_DEF_SET
		|| pMenuItem->cMenuType == MT_PASSWORD_SELECT)
	{
		SELF_DEFINE_INFO* pSelfDefineInfo;
		pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
		ASSERT(pSelfDefineInfo);

		if(pSelfDefineInfo->iSigValueType == VAR_STRING)
		{
			if(ppSelfDefineInfo)
			{
				*ppSelfDefineInfo = pSelfDefineInfo;
			}
			return TRUE;
		}
	}
	return FALSE;
}


static BOOL IsPhoneNoType(MENU_ITEM* pMenuItem, 
						 SELF_DEFINE_INFO** ppSelfDefineInfo)
{
	ASSERT(pMenuItem);

	if (pMenuItem->cMenuType == MT_SELF_DEF_SET)
	{
		SELF_DEFINE_INFO* pSelfDefineInfo;
		pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
		ASSERT(pSelfDefineInfo);

		if(pSelfDefineInfo->iSigValueType == VAR_PHONE_NO)
		{
			if(ppSelfDefineInfo)
			{
				*ppSelfDefineInfo = pSelfDefineInfo;
			}
			return TRUE;
		}
	}
	return FALSE;
}


static BOOL IsMultiSectType(MENU_ITEM* pMenuItem, 
							SELF_DEFINE_INFO** ppSelfDefineInfo)
{
	ASSERT(pMenuItem);

	if(pMenuItem->cMenuType == MT_SELF_DEF_SET)
	{
		SELF_DEFINE_INFO* pSelfDefineInfo;
		pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
		ASSERT(pSelfDefineInfo);

		if(pSelfDefineInfo->iSigValueType == VAR_MULTI_SECT)
		{
			if(ppSelfDefineInfo)
			{
				*ppSelfDefineInfo = pSelfDefineInfo;
			}
			return TRUE;
		}
	}
	return FALSE;
}

/*==========================================================================*
* FUNCTION : DisplaySignalItem
* PURPOSE  : Display a signal item 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*  pMenuItem     : 
*            int         x             : 
*            int         y             : 
*            char*       pLangCode     : 
*            int         nGetValueType : 
*            BOOL        bCurItemFlag  : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 17:28
*==========================================================================*/
static int DisplaySignalItem(MENU_ITEM* pMenuItem,
							 int x,
							 int y,
							 char* pLangCode, 
							 int nGetValueType,
							 BOOL bCurItemFlag)
{
	//by HULONGWEN, assume x = 0

	char			szDisBuf[MAX_CHARS_OF_LINE];
	int				nDrawWidth;
	int				nFontHeight = GetFontHeightFromCode(pLangCode);
	char			cDispType = FLAG_DIS_NORMAL_TYPE;

	int				nItemDisXoffset = 0;

	ASSERT(pMenuItem);

	//At first display the signal prompt name
	ConvertValueToString(pMenuItem, 
						sizeof(szDisBuf),
						szDisBuf,
						GET_PROMPT_INFO,
						MAX_CHARS_OF_UNIT);

	nDrawWidth = 
		GetWidthOfString(szDisBuf, (int)strlen(szDisBuf), pLangCode);

	DisString(szDisBuf,
			(int)strlen(szDisBuf),
			x,
			y,
			pLangCode,
			cDispType);

	ClearBar(x + nDrawWidth,
			y,
			SCREEN_WIDTH - ARROW_WIDTH -  (x + nDrawWidth),
			nFontHeight,
			FLAG_DIS_NORMAL_TYPE);

	//Then display the signal value
	if(RET_INVALID_DATA == ConvertValueToString(pMenuItem, 
								sizeof(szDisBuf),
								szDisBuf,
								nGetValueType,
								MAX_CHARS_OF_UNIT)
		|| FLAG_IS_EQUAL(pMenuItem->cMenuStatus, MD_EDIT_STATUS))
	{
		cDispType = FLAG_DIS_REVERSE_TYPE;
	}
	else
	{
		cDispType = FLAG_DIS_NORMAL_TYPE;
	}

	nDrawWidth = GetWidthOfString(szDisBuf, (int)strlen(szDisBuf), pLangCode);

	//Edit characters: left order	
	//TO let the username and password order
	if(IsStringType(pMenuItem, NULL)
		|| pMenuItem->cMenuType == MT_PASSWORD_SELECT)
	{
		nItemDisXoffset = ARROW_WIDTH * 2;

		//clear the back blank
		ClearBar(x + nItemDisXoffset + nDrawWidth,
			y + nFontHeight,
			SCREEN_WIDTH - ARROW_WIDTH - (x + nItemDisXoffset + nDrawWidth),  //refresh region,determined by the rect
			nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);
	}
	else //others: right order
	{
		nItemDisXoffset = SCREEN_WIDTH - ARROW_WIDTH -  nDrawWidth;

		if(nItemDisXoffset < 0)
		{
			nItemDisXoffset = 0;
		}

	}	

	//signal display, needn't to draw right arrow
	if((pMenuItem->cMenuType == MT_SIGNAL_DISPLAY)
		||(pMenuItem->cMenuType == MT_BARCODE))
	{
		bCurItemFlag = FALSE;
	}
	//clear the front blank and draw right arrow
	if(bCurItemFlag)
	{
		ClearBar(ARROW_WIDTH,
			y + nFontHeight,
			x + nItemDisXoffset - ARROW_WIDTH,
			nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);

		if(x + nItemDisXoffset < ARROW_WIDTH)
		{
			//Too long, adopt or mode
			ChangeDrawMode(IOC_OR_MODE);
		}

		//Display the right arrow
		DisSpecifiedFlag(ASCII_RIGHT_ARROW,
			0, 
			y + nFontHeight);

		if(x + nItemDisXoffset < ARROW_WIDTH)
		{
			//resume to copy mode
			ChangeDrawMode(IOC_COPY_MODE);
		}
	}
	else
	{
		ClearBar(0,
			y + nFontHeight,
			x + nItemDisXoffset,
			nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);
	}

	//�ڱ༭״̬��������ַ������߶��ֶεģ���ֻ������ʾ���ڱ༭����һ���֡�
	if((pMenuItem->cMenuStatus == MD_EDIT_STATUS)
		&& (IsMultiSectType(pMenuItem, NULL) || IsStringType(pMenuItem, NULL)))
	{
		int nHasDrawWidth;
		//int	nHasDrawStringLen;
		//int	nReverseStart, nReverseEnd;

		if(IsStringType(pMenuItem, NULL))
		{
			ASSERT(pMenuItem->nCurrentEditPos <= strlen(szDisBuf));

			//ǰ��������ʾ
			DisString(szDisBuf,
				pMenuItem->nCurrentEditPos,
				nItemDisXoffset,
				y + nFontHeight,
				pLangCode,
				FLAG_DIS_NORMAL_TYPE);

			nHasDrawWidth = GetWidthOfString(szDisBuf, pMenuItem->nCurrentEditPos, pLangCode);

			//���淴����ʾ
			DisString(szDisBuf + pMenuItem->nCurrentEditPos,
				(int)strlen(szDisBuf) - pMenuItem->nCurrentEditPos,
				nItemDisXoffset + nHasDrawWidth,
				y + nFontHeight,
				pLangCode,
				FLAG_DIS_REVERSE_TYPE);

		}
		else //���ֶ�
		{
			SELF_DEFINE_INFO* pSelfDefineInfo;
			pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
			ASSERT(pSelfDefineInfo);
			ASSERT(pMenuItem->nCurrentEditPos < pSelfDefineInfo->iStateNum);

			SECTION_DATA_STRU*	pSectionDataStru;
			pSectionDataStru = pSelfDefineInfo->pSectionDataStru;
			ASSERT(pSectionDataStru);


#if 0
			int i;
			int nStrOffset = 0;
			char *pReverseStart = szDisBuf;
			
			//ǰ��������ʾ
			DisString(szDisBuf,
				(int)strlen(szDisBuf),
				nItemDisXoffset,
				y + nFontHeight,
				pLangCode,
				FLAG_DIS_NORMAL_TYPE);

			//�м䷴����ʾ
			char cSeparate;
			for(i = 0; i < pMenuItem->nCurrentEditPos; i++)
			{
				cSeparate = (pSectionDataStru + i)->cSeparateFlag;
				pReverseStart = strchr(szDisBuf + nStrOffset, cSeparate) + 1;
				nStrOffset = (pReverseStart - szDisBuf)/sizeof(char);
			}
			nHasDrawWidth = GetWidthOfString(szDisBuf, nStrOffset, pLangCode);
			DisString(pReverseStart,
				(int)strlen(pReverseStart),
				nItemDisXoffset + nHasDrawWidth,
				y + nFontHeight,
				pLangCode,
				FLAG_DIS_REVERSE_TYPE);


			//���������ʾ
			if(pMenuItem->nCurrentEditPos < pSelfDefineInfo->iStateNum - 1)
			{
				cSeparate = (pSectionDataStru + pMenuItem->nCurrentEditPos)->cSeparateFlag;

				pReverseStart = strchr(szDisBuf + nStrOffset, cSeparate);
				nStrOffset = (pReverseStart - szDisBuf)/sizeof(char);
				nHasDrawWidth = GetWidthOfString(szDisBuf, nStrOffset, pLangCode);
				DisString(pReverseStart,
					(int)strlen(pReverseStart),
					nItemDisXoffset + nHasDrawWidth,
					y + nFontHeight,
					pLangCode,
					FLAG_DIS_NORMAL_TYPE);

			}
#else
			int i;
			int nStrOffset = 0;
			char *pReverseStart = NULL, *pReverseEnd = NULL;
			char *pStrStart = NULL, *pStrEnd = NULL;

			pStrStart = szDisBuf;
			pStrEnd = szDisBuf + strlen(szDisBuf);

			//�༭�ο�ʼλ��
			char cSeparate;
			pReverseStart = pStrStart;
			for(i = 0; i < pMenuItem->nCurrentEditPos; i++)
			{
				cSeparate = (pSectionDataStru + i)->cSeparateFlag;
				pReverseStart = strchr(pStrStart + nStrOffset, cSeparate) + 1;
				nStrOffset = (pReverseStart - pStrStart)/sizeof(char);
			}

			//�༭�ν���λ��
			pReverseEnd = pStrEnd;
			if(pMenuItem->nCurrentEditPos < pSelfDefineInfo->iStateNum - 1)
			{
				cSeparate = (pSectionDataStru + pMenuItem->nCurrentEditPos)->cSeparateFlag;
				pReverseEnd = strchr(pStrStart + nStrOffset, cSeparate);
				nStrOffset = (pReverseEnd - pStrStart)/sizeof(char);
			}

			//�༭��֮ǰ������ʾ
			if(pReverseStart != pStrStart)
			{
				DisString(pStrStart,
					(pReverseStart - pStrStart)/sizeof(char),
					nItemDisXoffset,
					y + nFontHeight,
					pLangCode,
					FLAG_DIS_NORMAL_TYPE);
			}

			//�༭�η�����ʾ
			nHasDrawWidth = GetWidthOfString(pStrStart, (pReverseStart - pStrStart)/sizeof(char), pLangCode);
			DisString(pReverseStart,
				(pReverseEnd - pReverseStart)/sizeof(char),
				nItemDisXoffset + nHasDrawWidth,
				y + nFontHeight,
				pLangCode,
				FLAG_DIS_REVERSE_TYPE);

			//�༭��֮��������ʾ
			if(pReverseEnd != pStrEnd)
			{
				nHasDrawWidth = GetWidthOfString(pStrStart, (pReverseEnd - pStrStart)/sizeof(char), pLangCode);
				DisString(pReverseEnd,
					(pStrEnd - pReverseEnd)/sizeof(char),
					nItemDisXoffset + nHasDrawWidth,
					y + nFontHeight,
					pLangCode,
					FLAG_DIS_NORMAL_TYPE);
			}

#endif
		}
		
	}
	else
	{
		DisString(szDisBuf,
			(int)strlen(szDisBuf),
			nItemDisXoffset,
			y + nFontHeight,
			pLangCode,
			cDispType);
	}


	return ERR_LCD_OK;

}

/*==========================================================================*
* FUNCTION : DisplaySignals
* PURPOSE  : Display all signals on a screen
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: SUB_MENU*  pSubMenu      : 
*            int        nGetValueType : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 17:28
*==========================================================================*/
int DisplaySignals(SUB_MENU* pSubMenu,  
				   int		nGetValueType)
{
	ASSERT(pSubMenu);


	if(pSubMenu->nDispItems <= 0)
	{
#define PROMPT_NO_ITEMS_TIMEOUT 1500
		DoModal_AcknowlegeDlg_ID(HEADING_PROMPT_INFO_LANG_ID, 
			NO_ITEM_INFO_LANG_ID, 
			ESC_OR_ENT_RET_PROMPT_LANG_ID,
			-1,
			PROMPT_NO_ITEMS_TIMEOUT);

		return ID_GOTO_RPEVIOUS;
	}

	if(pSubMenu->nCur >= pSubMenu->nDispItems)
	{
		pSubMenu->nCur = pSubMenu->nDispItems - 1;
	}

	char*		pLangCode = GetCurrentLangCode();
	ASSERT(pLangCode);


	int			i;
	MENU_ITEM*	pItems;

	int			nFontHeight;
	int			nMaxRow;
	int			nSigNumOfScreen;

	nFontHeight = GetFontHeightFromCode(pLangCode);
	nMaxRow = SCREEN_HEIGHT/nFontHeight;
	nSigNumOfScreen = nMaxRow/A_SIGNAL_OCCUPIED_LINES;

	MENU_ITEM* pCurMenuItem;
	pCurMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[pSubMenu->nCur]]);
	ASSERT(pCurMenuItem);

	//����nTop��nBottom
	if(FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_SIGNAL_DISPLAY)
		||FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_BARCODE))
	{		
		if(pSubMenu->nCur != pSubMenu->nTop)
		{
			pSubMenu->nTop = pSubMenu->nCur;
			pSubMenu->nBottom = pSubMenu->nTop + (nSigNumOfScreen - 1);
		}

	}
	else if(FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_SIGNAL_SET)
		|| FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_SIGNAL_CONTROL)
		|| FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_ALARM_LEVEL_SET)
		|| FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_ALARM_RELAY_SET)
		|| FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_SELF_DEF_SET)
		|| FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_PASSWORD_SELECT)
		|| FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_SIGNAL_CUSTOMIZE))
	{
		if(pSubMenu->nCur < pSubMenu->nTop)
		{
			pSubMenu->nTop = pSubMenu->nCur;
			pSubMenu->nBottom = pSubMenu->nTop + (nSigNumOfScreen - 1);
		}

		if(pSubMenu->nCur > pSubMenu->nBottom)
		{
			pSubMenu->nBottom = pSubMenu->nCur;
			pSubMenu->nTop = pSubMenu->nBottom - (nSigNumOfScreen - 1);
		}

	}

	if(pSubMenu->nBottom != pSubMenu->nTop + (nSigNumOfScreen - 1))
	{
		pSubMenu->nBottom = pSubMenu->nTop + (nSigNumOfScreen - 1);
	}

	
	//��������Ҫ������ʾ(���¾���)
	int nStartY = 0;
	if(LCD_SPLIT_LANG_RES_ID(pSubMenu->nSubMenuID) == PASSWORD_SCREEN_MENU_ID)
	{
		nStartY = (SCREEN_HEIGHT - pSubMenu->nItems * A_SIGNAL_OCCUPIED_LINES * nFontHeight) / 2;
		if(nStartY < 0)
		{
			nStartY = 0;
		}
		if(nStartY > 0)
		{
			ClearBar(0, 0, SCREEN_WIDTH, nStartY, FLAG_DIS_NORMAL_TYPE);
		}
	}

	//��ʼ��ʾ
	int	nHasDisplayLine = 0;

	for(i = pSubMenu->nTop; 
		(i <= pSubMenu->nBottom) && (i < pSubMenu->nDispItems);
		i++, nHasDisplayLine += A_SIGNAL_OCCUPIED_LINES)
	{
		ASSERT(pSubMenu->pDispItems[i] >= 0);
		ASSERT(pSubMenu->pDispItems[i] < pSubMenu->nItems);

		pItems = pSubMenu->pItems + pSubMenu->pDispItems[i];
		if(pItems == NULL)
		{
			break;
		}

		DisplaySignalItem(pItems, 
			0, 
			(i - pSubMenu->nTop) * nFontHeight * A_SIGNAL_OCCUPIED_LINES + nStartY, 
			pLangCode,
			nGetValueType,
			i == pSubMenu->nCur);

	}

	//�������δ��ʾ�Ĳ���
	if(nHasDisplayLine * nFontHeight  + nStartY < SCREEN_HEIGHT)
	{
		ClearBar(0,
			nHasDisplayLine * nFontHeight + nStartY,
			SCREEN_WIDTH - ARROW_WIDTH,
			SCREEN_HEIGHT - nHasDisplayLine * nFontHeight - nStartY, 
			FLAG_DIS_NORMAL_TYPE);
	}

	DisplayProgressBar(pSubMenu->nDispItems,
		pSubMenu->nCur,
		pSubMenu->nTop,
		pSubMenu->nBottom);

	return ERR_LCD_OK;
}

/*==========================================================================*
 * FUNCTION : HandleSignalScreenUpOrDown
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SUB_MENU**  ppSubMenu : 
 *            BOOL        bUp       : 
 * RETURN   : int : Need return ID_EDITING or ID_VIEWING
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-03-17 14:03
 *==========================================================================*/
int HandleSignalScreenUpOrDown(SUB_MENU* pSubMenu, STACK_PVOID* pStack, BOOL bUp, GLOBAL_VAR_OF_LCD* pThis)
{
	
	ASSERT(pSubMenu);
	UNUSED(pStack);
	ASSERT(pThis);

	int nRetID = ID_VIEWING;

	int	nMultiple;

	MENU_ITEM* pCurMenuItem;
	pCurMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[pSubMenu->nCur]]);
	ASSERT(pCurMenuItem);

	if(FLAG_IS_EQUAL(pCurMenuItem->cMenuStatus, MD_SHOW_STATUS))
	{
		if(FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_SIGNAL_DISPLAY)
			|| FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_BARCODE))
		{
			if(pSubMenu->nItems > SIGNAL_NUM_PER_SCREEN)
			{
				pSubMenu->nCur = bUp
					? PrevSubMenuItem( pSubMenu, SIGNAL_NUM_PER_SCREEN)
					: NextSubMenuItem( pSubMenu, SIGNAL_NUM_PER_SCREEN);
			}
		}
		else /*MT_SIGNAL_SET || MT_SIGNAL_CONTROL || MT_SELF_DEF_SET|| MT_ALARM_LEVEL_SET || MT_PASSWORD_SELECT and etc*/
		{
			if( pSubMenu->nItems > 1 )
			{
				pSubMenu->nCur = bUp
					? PrevSubMenuItem( pSubMenu, 1)
					: NextSubMenuItem( pSubMenu ,1);
			}
		}
	}
	else if(FLAG_IS_EQUAL(pCurMenuItem->cMenuStatus, MD_EDIT_STATUS))
	{
		nRetID = ID_EDITING;

		int		iSigValueType;			
		float	fMaxValue, fMinValue;
		float	fSettingStep;

		char*	pString = NULL;
		SET_SIG_INFO* pSetSigInfo;
		CTRL_SIG_INFO* pCtrlSigInfo;
		SELF_DEFINE_INFO* pSelfDefineInfo;

		int nEquipId, nSignalType, nSignalID;

		DXI_SPLIT_UNIQUE_SIG_ID(pCurMenuItem->nMenuItemID, \
					nEquipId, \
					nSignalType, \
					nSignalID);


		switch(pCurMenuItem->cMenuType) 
		{
		case MT_SIGNAL_SET:				//Same Handle,continue
		case MT_SIGNAL_CUSTOMIZE:
			if(nSignalType==SIG_TYPE_CONTROL)
			{	
				pCtrlSigInfo = (CTRL_SIG_INFO*)(pCurMenuItem->pvItemData);
				ASSERT(pCtrlSigInfo);
				iSigValueType = pCtrlSigInfo->iSigValueType;

				fMaxValue = pCtrlSigInfo->fMaxValidValue;
				fMinValue = pCtrlSigInfo->fMinValidValue;
				fSettingStep = pCtrlSigInfo->fControlStep;

			}
			else
			{	
				pSetSigInfo = (SET_SIG_INFO*)(pCurMenuItem->pvItemData);
				ASSERT(pSetSigInfo);
				iSigValueType = pSetSigInfo->iSigValueType;

				fMaxValue = pSetSigInfo->fMaxValidValue;
				fMinValue = pSetSigInfo->fMinValidValue;
				fSettingStep = pSetSigInfo->fSettingStep;	
			}
			//Changed by Marco Yang, 2011/5/6
			//Do some special for the Max Current Limit
			if(FLOAT_EQUAL(12000.0 ,fMaxValue) == TRUE )
			{
				if (pSetSigInfo->iSigID == 46)//For max current limit, do some special,Rectifier
				{
					int nBufLen;
					SIG_BASIC_VALUE		*pSigValue;

					if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
						RECT_GROUP_EQUIP_ID,	
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECTGROUP_MAX_CURRENT),
						&nBufLen,			
						&pSigValue,			
						0))
					{
						if(pSigValue->varValue.fValue > fMinValue)
						{				
							fMaxValue = pSigValue->varValue.fValue;					
						}
					}

					if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
						RECT_GROUP_EQUIP_ID,	
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECTGROUP_MIN_CURRENT),
						&nBufLen,			
						&pSigValue,			
						0))
					{
						if(pSigValue->varValue.fValue < fMaxValue)
						{				
							fMinValue = pSigValue->varValue.fValue;					
						}

					}

				}
				else if (pSetSigInfo->iSigID == 51)//For max current limit, do some special,Converter 
				{
					int nBufLen;
					SIG_BASIC_VALUE		*pSigValue;

					if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
						CONVERTER_GROUP_EQUIP_ID,	
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECTGROUP_MAX_CURRENT),
						&nBufLen,			
						&pSigValue,			
						0))
					{
						if(pSigValue->varValue.fValue > fMinValue)
						{				
							fMaxValue = pSigValue->varValue.fValue;					
						}
					}

					if(ERR_DXI_OK == DxiGetData(VAR_A_SIGNAL_VALUE,
						CONVERTER_GROUP_EQUIP_ID,	
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECTGROUP_MIN_CURRENT),
						&nBufLen,			
						&pSigValue,			
						0))
					{
						if(pSigValue->varValue.fValue < fMaxValue)
						{				
							fMinValue = pSigValue->varValue.fValue;					
						}

					}

				}
			}
			
			//End by Marco Yang, 2011/5/6

			break;

		case MT_SIGNAL_CONTROL:
			pCtrlSigInfo = (CTRL_SIG_INFO*)(pCurMenuItem->pvItemData);
			ASSERT(pCtrlSigInfo);

			iSigValueType = pCtrlSigInfo->iSigValueType;

			fMaxValue = pCtrlSigInfo->fMaxValidValue;
			fMinValue = pCtrlSigInfo->fMinValidValue;
			fSettingStep = pCtrlSigInfo->fControlStep;

			break;

		case MT_SELF_DEF_SET:
		case MT_PASSWORD_SELECT:
			pSelfDefineInfo = 
				(SELF_DEFINE_INFO*)(pCurMenuItem->pvItemData);
			ASSERT(pSelfDefineInfo);

			iSigValueType = pSelfDefineInfo->iSigValueType;

			fMaxValue = pSelfDefineInfo->fMaxValidValue;
			fMinValue = pSelfDefineInfo->fMinValidValue;
			fSettingStep = pSelfDefineInfo->fSettingStep;			

			pString = pSelfDefineInfo->szString;

			break;	

		case MT_ALARM_LEVEL_SET:

			iSigValueType = VAR_ENUM;

			fMaxValue = ALARM_LEVEL_MAX - 1;
			fMinValue = 0;
			fSettingStep = 1;

			break;

		case MT_ALARM_RELAY_SET:

			iSigValueType = VAR_ENUM;

			fMaxValue = MAX_RELAY_NUM;
			fMinValue = 0;
			fSettingStep = 1;

			break;

		default:
			DEBUG_LCD_FILE_FUN_LINE_INT("Unkonw cMenuType:", pCurMenuItem->cMenuType);
			break;
		}//switch(pCurMenuItem->cMenuType) 


		nMultiple = pThis->nValueStepMultiple;

		switch (iSigValueType)
		{
		case VAR_FLOAT:
			{
				pCurMenuItem->vdData.fValue = 
					ValueStep((float)(pCurMenuItem->vdData.fValue), 
					fMaxValue, fMinValue, fSettingStep * nMultiple, bUp);

			}
			break;

		case VAR_LONG:
			{
				pCurMenuItem->vdData.lValue = 
					(long)(ValueStep((float)(pCurMenuItem->vdData.lValue), 
					fMaxValue, fMinValue, fSettingStep * nMultiple, bUp));
			}
			break;

		case VAR_UNSIGNED_LONG:
			{
				pCurMenuItem->vdData.ulValue = 
					(ULONG)(ValueStep((float)(pCurMenuItem->vdData.ulValue), 
					fMaxValue, fMinValue, fSettingStep * nMultiple, bUp));
			}
			break;

		//case VAR_DATE_TIME:
		//	{
		//	}
		//	break;

		case VAR_ENUM:
			{
				fSettingStep = 1;

				pCurMenuItem->vdData.enumValue = 
					(SIG_ENUM)(ValueStep(
					(float)(pCurMenuItem->vdData.enumValue), 
					fMaxValue, fMinValue, fSettingStep, bUp));
			}
			break;	

		case VAR_STRING:
			{
				char* pCurChar;

				int nOffset = 0;

				pCurChar = &(pSelfDefineInfo->szString[pCurMenuItem->nCurrentEditPos]);

				nOffset = CHAR_OF_CHARACTER_SET - 
					strlen(strchr(s_pCharacterSet, *pCurChar));

				nOffset  = !bUp ? (nOffset - 1) : (nOffset + 1);

				if(nOffset >= (int)CHAR_OF_CHARACTER_SET)
				{
					nOffset = 0;
				}
				else if(nOffset < 0)
				{
					nOffset = CHAR_OF_CHARACTER_SET - 1;
				}

				*pCurChar = s_pCharacterSet[nOffset];

			}
			break;

		case VAR_PHONE_NO:
			{
				char* pCurChar;

				int nOffset = 0;

				pCurChar = &(pSelfDefineInfo->szString[pCurMenuItem->nCurrentEditPos]);

				nOffset = CHAR_OF_CHARACTER_SET - 
					strlen(strchr(s_pCharacterSet, *pCurChar));

				nOffset  = !bUp ? (nOffset - 1) : (nOffset + 1);

#define		NUMBERIC_CAHR_OF_CHARACTER_SET	10
				if(nOffset > (int)NUMBERIC_CAHR_OF_CHARACTER_SET)
				{
					nOffset = 0;
				}
				else if(nOffset < 0)
				{
					nOffset = NUMBERIC_CAHR_OF_CHARACTER_SET;
				}

				*pCurChar = s_pCharacterSet[nOffset];

				//int nLength;
				//ASSERT(pString);

				//nLength = strlen(pString);

				//if(bUp)
				//{
				//	if(pCurMenuItem->nCursorOffset < (MAX_CHAR_NUM_PER_LINE - 2))	
				//	{
				//		pCurMenuItem->nCursorOffset++;
				//	}
				//	else if(pCurMenuItem->nCursorOffset == (MAX_CHAR_NUM_PER_LINE - 2))
				//	{
				//		if(nLength < MAX_STRING_LENGTH)
				//		{
				//			pString[nLength] = '0';
				//			pString[nLength + 1] = 0;
				//		}
				//	}
				//	else
				//	{
				//		if(nLength < MAX_STRING_LENGTH)
				//		{
				//			pString[nLength] = '0';
				//			pString[nLength + 1] = 0;
				//			pCurMenuItem->nCursorOffset--;
				//		}					
				//	}
				//}
				//else
				//{
				//	if(nLength)	
				//	{
				//		if((MAX_CHAR_NUM_PER_LINE - nLength - 1)
				//			== pCurMenuItem->nCursorOffset)
				//		{
				//			pCurMenuItem->nCursorOffset++;
				//			pString[nLength - 1] = 0;
				//		}
				//		else
				//		{
				//			pCurMenuItem->nCursorOffset--;
				//		}
				//	}
				//}
			}
			break;	

		case VAR_MULTI_SECT:
			{
				ASSERT(pSelfDefineInfo);
				ASSERT(pCurMenuItem->nCurrentEditPos < pSelfDefineInfo->iStateNum);
				
				SECTION_DATA_STRU*	pSectionDataStru;
				pSectionDataStru = pSelfDefineInfo->pSectionDataStru 
					+ pCurMenuItem->nCurrentEditPos;
				ASSERT(pSectionDataStru);

				pSectionDataStru->uiValue = 
					(UINT)ValueStep((float)(pSectionDataStru->uiValue), 
					(float)(pSectionDataStru->uiMaxValue),
					(float)(pSectionDataStru->uiMinValue),
					(float)(pSectionDataStru->uiSettingStep) * nMultiple,
					bUp);
			}
			break;	

		default:
			{
				DEBUG_LCD_FILE_FUN_LINE_INT("Unkonw SigValueType:", iSigValueType);
			}
			break;

		}//switch (iSigValueType)
	}

	return nRetID;
}

/*==========================================================================*
* FUNCTION : ValueStep
* PURPOSE  : Get new value considering the setting step
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float  fSetValue    : 
*            float  fMaxValue    : 
*            float  fMinValue    : 
*            float  fSettingStep : 
*            BOOL   bStepFlag    : 
* RETURN   : static float : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 17:30
*==========================================================================*/
static float ValueStep(float fSetValue,
					   float fMaxValue,
					   float fMinValue,
					   float fSettingStep,
					   BOOL bStepFlag)
{

	if(bStepFlag)
	{
		if( fSetValue < fMaxValue )
		{
			if((fMaxValue - fSetValue) > fSettingStep)
			{
				fSetValue = fSettingStep * (fSetValue / fSettingStep + 1.000);
			}
			else
			{
				fSetValue = fMaxValue;
			}

		}
		else //when arrived maximum, turn to minimum
		{
			fSetValue = fMinValue;
		}

		if(fSetValue < fMinValue)
		{
			fSetValue = fMinValue;
		}
	}
	else
	{
		if( fSetValue > fMinValue )
		{
			if( (fSetValue - fMinValue) > fSettingStep )
			{
				fSetValue = fSettingStep * (fSetValue / fSettingStep - 1.000);
			}
			else
			{
				fSetValue = fMinValue;
			}

		}
		else //when arrived minimum, turn to maximum
		{
			fSetValue = fMaxValue;
		}

		if(fSetValue > fMaxValue)
		{
			fSetValue = fMaxValue;
		}
	}

	return fSetValue;
}


/*==========================================================================*
 * FUNCTION : HandleSignalScreenEscape
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SUB_MENU**  ppSubMenu : 
 * RETURN   : int : Need return ID_GOTO_RPEVIOUS or ID_VIEWING
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-03-17 14:00
 *==========================================================================*/
int HandleSignalScreenEscape(SUB_MENU* pSubMenu)
{
	MENU_ITEM* pCurMenuItem;
	int nIDRet = ID_VIEWING;

	ASSERT(pSubMenu);

	pCurMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[pSubMenu->nCur]]);
	ASSERT(pCurMenuItem);

	if(FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_SIGNAL_DISPLAY)
		||FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_BARCODE))
	{		
		nIDRet = ID_GOTO_RPEVIOUS;
	}
	else//MT_SIGNAL_SET || MT_SIGNAL_CONTROL || MT_SELF_DEF_SET|| MT_ALARM_LEVEL_SET || MT_PASSWORD_SELECT
	{
		if(FLAG_IS_EQUAL(pCurMenuItem->cMenuStatus, MD_SHOW_STATUS))
		{
			nIDRet = ID_GOTO_RPEVIOUS;
		}
		else if(FLAG_IS_EQUAL(pCurMenuItem->cMenuStatus, MD_EDIT_STATUS))
		{
			pCurMenuItem->cMenuStatus = MD_SHOW_STATUS;

			nIDRet = ID_VIEWING;
		}
	}

	return nIDRet;
}

static BYTE GetAuthorityLevel(MENU_ITEM* pMenuItem)
{
	BYTE byAuthorityLevel = OPERATOR_LEVEL;

	int nEquipId, nSignalType, nSignalID;

	CTRL_SIG_INFO*		pCtrlSigInfo;
	SET_SIG_INFO*		pSetSigInfo;	 
	SELF_DEFINE_INFO*	pSelfDefineInfo;

	void*				pDisSigInfo;

	ASSERT(pMenuItem);

	pDisSigInfo = pMenuItem->pvItemData;

	ASSERT(pDisSigInfo);

	if(pMenuItem->cMenuType == MT_SELF_DEF_SET)
	{
		pSelfDefineInfo = (SELF_DEFINE_INFO*)pDisSigInfo;

		byAuthorityLevel = pSelfDefineInfo->byAuthorityLevel;

	}
	else
	{
		DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID, 
			nEquipId, 
			nSignalType, 
			nSignalID);

		if(nSignalType == SIG_TYPE_CONTROL)
		{
			pCtrlSigInfo = (CTRL_SIG_INFO*)pDisSigInfo;

			byAuthorityLevel = pCtrlSigInfo->byAuthorityLevel;
		}
		else if(nSignalType == SIG_TYPE_SETTING)
		{
			pSetSigInfo = (SET_SIG_INFO*)pDisSigInfo;

			byAuthorityLevel = pSetSigInfo->byAuthorityLevel;
		}
	}

	return byAuthorityLevel;
}

static int DisNotBeCtrlledOrsetInfo(int nSignalType)
{
	int nPromptInfoID1;

	if (nSignalType == SIG_TYPE_CONTROL)
	{
		nPromptInfoID1 = SIGNAL_CANNOT_BE_CONTROL_LANG_ID;
	}
	else
	{
		nPromptInfoID1 = SIGNAL_CANNOT_BE_SET_LANG_ID;
	}

#define SIGNAL_NOT_CONTROL_PROMPT_TIMEOUT 1500//1s

	return DoModal_AcknowlegeDlg_ID(
		HEADING_PROMPT_INFO_LANG_ID, 
		nPromptInfoID1, 
		ESC_OR_ENT_RET_PROMPT_LANG_ID, -1, 
		SIGNAL_NOT_CONTROL_PROMPT_TIMEOUT);

}

static int DisSetOrCtrlAckInfo(MENU_ITEM* pCurMenuItem)
{
	char* pHeadingInfo = "";
	char* pPromptInfo1 = "";
	char* pPromptInfo2 = "";
	char* pPromptInfo3 = "";

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	char szCtrlExpressionBuf[MAX_CHARS_OF_LINE * 2];

	ASSERT(pCurMenuItem);

	if(pCurMenuItem->cMenuType == MT_SIGNAL_SET
		|| pCurMenuItem->cMenuType == MT_SIGNAL_CONTROL)//set or control signal
	{
		int				iCtrlExpression = 0;
		CTRL_EXPRESSION	*pCtrlExpression = NULL;

		int		i;

		int		iSigValueType;
		float	fCurValue;

		if(pCurMenuItem->cMenuType == MT_SIGNAL_SET)
		{
			SET_SIG_INFO* pSetSig = (SET_SIG_INFO*)pCurMenuItem->pvItemData;

			iCtrlExpression = pSetSig->iCtrlExpression;
			pCtrlExpression = pSetSig->pCtrlExpression;
			iSigValueType	= pSetSig->iSigValueType;
		}
		else
		{
			CTRL_SIG_INFO* pCtrlSig = (CTRL_SIG_INFO*)pCurMenuItem->pvItemData;

			iCtrlExpression = pCtrlSig->iCtrlExpression;
			pCtrlExpression = pCtrlSig->pCtrlExpression;
			iSigValueType	= pCtrlSig->iSigValueType;
		}

		fCurValue = (iSigValueType == VAR_FLOAT)  
			? pCurMenuItem->vdData.fValue 
			: ((iSigValueType != VAR_UNSIGNED_LONG) 
			? (float)(pCurMenuItem->vdData.lValue)
			: (float)(pCurMenuItem->vdData.ulValue));

		for (i = 0; i < iCtrlExpression; i++, pCtrlExpression++)
		{
			if (fCurValue >= pCtrlExpression->fThreshold)
			{
				break;
			}
		}

		if (i < iCtrlExpression)
		{
			if (pCtrlExpression->pText != NULL)
			{
				strncpyz(szCtrlExpressionBuf, 
					pCtrlExpression->pText->pAbbrName[nCurrentLangFlag],
					sizeof(szCtrlExpressionBuf));
			}
			else
			{
				szCtrlExpressionBuf[0] = '\0';
			}

			if (szCtrlExpressionBuf[0] == '\0')
			{
				pHeadingInfo = "";
			}
			else//if has "\n", display the config string two line
			{
				char* p = strstr(szCtrlExpressionBuf, CTRL_EXP_SPLITTER);

				pHeadingInfo = szCtrlExpressionBuf;

				if (p != NULL)
				{

					pPromptInfo1 = p + strlen(CTRL_EXP_SPLITTER);//The start of the second line

					*p = '\0';//Set the first line end char
				}
				else
				{
					pPromptInfo1 = "";
				}

			}

		}
		else
		{
			if(pCurMenuItem->cAcknowlegeFlag != MENU_ITEM_NEED_ACKNOWLEDGE)
			{
				return ID_RETURN;
			}
		}

	}
	else
	{
		if (pCurMenuItem->cAcknowlegeFlag != MENU_ITEM_NEED_ACKNOWLEDGE)
		{
			return ID_RETURN;
		}
	}

	if (pHeadingInfo[0] == '\0')//if "", display the ack prompt info
	{
		pLangText = GetLCDLangText(
			HEADING_ACKNOWLEDGE_INFO_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);

		if(pLangText)
		{
			pHeadingInfo = pLangText->pAbbrName[nCurrentLangFlag];

		}
	}

	pLangText = GetLCDLangText(
		ENT_KEY_ACKNOWLEDGE_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	if(pLangText)
	{
		pPromptInfo2 = pLangText->pAbbrName[nCurrentLangFlag];

	}

	//if(pCurMenuItem->cMenuType == MT_SIGNAL_SET
	//	|| pCurMenuItem->cMenuType == MT_SIGNAL_CONTROL)//set or control signal
	//{
	//	int nEquipId;
	//	int nSignalType;
	//	int nSignalID;
	//	
	//	DXI_SPLIT_UNIQUE_SIG_ID(pCurMenuItem->nMenuItemID, 
	//		nEquipId, 
	//		nSignalType, 
	//		nSignalID);

	//	if(nEquipId)

	//}



	pLangText = GetLCDLangText(
		ESC_KEY_CANCEL_PROMPT_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	if(pLangText)
	{
		pPromptInfo3 = pLangText->pAbbrName[nCurrentLangFlag];

	}

	if (pPromptInfo1[0] == '\0')//if "", the following displayed item move to front
	{
		pPromptInfo1 = pPromptInfo2;

		pPromptInfo2 = pPromptInfo3;

		pPromptInfo3 = "";
	}

	return DoModal_AcknowlegeDlg_INFO(
		pHeadingInfo,
		pPromptInfo1,
		pPromptInfo2,
		pPromptInfo3,
		-1);
}



static int FindNextEquip(SUB_MENU *pSubMenu, BOOL bOnlySameType, BOOL bJumpToTop)
{
	ASSERT(pSubMenu);

	MENU_ITEM *pMenuItem;
	pMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[pSubMenu->nCur]]);

	BOOL bFind = FALSE;
	int i;
	int nBufLen;

	int nEquipID, nSrcEquipID;
	EQUIP_INFO *pEquipInfo, *pSrcEquipInfo;

	//��ȡҪ���ҵ��豸�������Ϣ
	nSrcEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

	if(ERR_DXI_OK != DxiGetData(VAR_A_EQUIP_INFO,
		nSrcEquipID,
		0,
		&nBufLen,			
		&pSrcEquipInfo,			
		0))
	{
		return -1;
	}


	//��ʼ������һ���豸
	for(i = pSubMenu->nCur + 1; i < pSubMenu->nDispItems; i++)
	{
		pMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[i]]);
		nEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

		if(ERR_DXI_OK == DxiGetData(VAR_A_EQUIP_INFO,
			nEquipID,
			0,
			&nBufLen,			
			&pEquipInfo,			
			0))
		{
			if(bOnlySameType)
			{
				if(pEquipInfo->iEquipTypeID	== pSrcEquipInfo->iEquipTypeID)
				{
					bFind = TRUE;
					break;
				}
			}
			else
			{
				bFind = TRUE;
				break;
			}
		}
	}

	if(bFind)
	{
		return i;
	}

	//���û�ҵ�����ͷ��ʼ��
	if(bJumpToTop)
	{
		for(i = 0; i < pSubMenu->nCur; i++)
		{
			pMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[i]]);
			nEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

			if(ERR_DXI_OK == DxiGetData(VAR_A_EQUIP_INFO,
				nEquipID,
				0,
				&nBufLen,			
				&pEquipInfo,			
				0))
			{
				if(bOnlySameType)
				{
					if(pEquipInfo->iEquipTypeID	== pSrcEquipInfo->iEquipTypeID)
					{
						bFind = TRUE;
						break;
					}
				}
				else
				{
					bFind = TRUE;
					break;
				}
			}
		}
		if(bFind)
		{
			return i;
		}
	}

	return -1;
}

static int FindPrevEquip(SUB_MENU *pSubMenu, BOOL bOnlySameType, BOOL bJumpToTop)
{
	ASSERT(pSubMenu);

	MENU_ITEM *pMenuItem;
	pMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[pSubMenu->nCur]]);

	BOOL bFind = FALSE;
	int i;
	int nBufLen;

	int nEquipID, nSrcEquipID;
	EQUIP_INFO *pEquipInfo, *pSrcEquipInfo;

	//��ȡҪ���ҵ��豸�������Ϣ
	nSrcEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

	if(ERR_DXI_OK != DxiGetData(VAR_A_EQUIP_INFO,
		nSrcEquipID,
		0,
		&nBufLen,			
		&pSrcEquipInfo,			
		0))
	{
		return -1;
	}


	//��ʼ������һ���豸
	for(i = pSubMenu->nCur - 1; i >= 0; i--)
	{
		pMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[i]]);
		nEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

		if(ERR_DXI_OK == DxiGetData(VAR_A_EQUIP_INFO,
			nEquipID,
			0,
			&nBufLen,			
			&pEquipInfo,			
			0))
		{
			if(bOnlySameType)
			{
				if(pEquipInfo->iEquipTypeID	== pSrcEquipInfo->iEquipTypeID)
				{
					bFind = TRUE;
					break;
				}
			}
			else
			{
				bFind = TRUE;
				break;
			}
		}
	}

	if(bFind)
	{
		return i;
	}

	//���û�ҵ�����ͷ��ʼ��
	if(bJumpToTop)
	{
		for(i = pSubMenu->nDispItems - 1; i > pSubMenu->nCur; i--)
		{
			pMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[i]]);
			nEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

			if(ERR_DXI_OK == DxiGetData(VAR_A_EQUIP_INFO,
				nEquipID,
				0,
				&nBufLen,			
				&pEquipInfo,			
				0))
			{
				if(bOnlySameType)
				{
					if(pEquipInfo->iEquipTypeID	== pSrcEquipInfo->iEquipTypeID)
					{
						bFind = TRUE;
						break;
					}
				}
				else
				{
					bFind = TRUE;
					break;
				}
			}
		}
		if(bFind)
		{
			return i;
		}
	}

	return -1;
}



static BOOL SwitchNextOrPreviousEquip(MENU_DISPLAY* pMenuDisplay, 
									  BOOL bNext)
{
	//Switch to next or previous equip, only to multiple equips

	STACK_PVOID* pStack;
	pStack = &(pMenuDisplay->pMainMenu->stack);
	//Certainlly the pStack is not empty
	ASSERT(pStack);

	SUB_MENU *pParentSubMenu;
	pParentSubMenu = (SUB_MENU *)STACK_TOP(pStack);
	ASSERT(pParentSubMenu);

	SUB_MENU* pSubMenu;
	pSubMenu = pMenuDisplay->pMainMenu->pCurMenu;
	ASSERT(pSubMenu);

	MENU_ITEM *pMenuItem;

	int nBufLen;
	int nEquipID;
	int nDeivceID;
	EQUIP_INFO* pEquipInfo;
	int nNewCur = -1;
	SUB_MENU *pNewSubMenu;

	if((LCD_SPLIT_PREFIX(pSubMenu->nSubMenuID) == PREFIX_ACTUAL_EQUIP)
		|| (LCD_SPLIT_PREFIX(pSubMenu->nSubMenuID) == PREFIX_BARCODE_DEVICE))
	{
		if(LCD_SPLIT_PREFIX(pParentSubMenu->nSubMenuID) == PREFIX_VIRTUAL_GROUP_EQUIP)
		{
			nNewCur = bNext ? FindNextEquip(pParentSubMenu, TRUE, TRUE) : FindPrevEquip(pParentSubMenu, TRUE, TRUE);
		}
		else if(pParentSubMenu->nSubMenuID == LCD_MERGE_FIXED_LANG_ID(PRODUCT_INFO_LANG_ID))
		{
			nNewCur = bNext ? FindNextEquip(pParentSubMenu, FALSE, TRUE) : FindPrevEquip(pParentSubMenu, FALSE, TRUE);
		}

		if(nNewCur < 0)
		{
			return FALSE;
		}

		pParentSubMenu->nCur = nNewCur;
		pMenuItem = &(pParentSubMenu->pItems[pParentSubMenu->pDispItems[pParentSubMenu->nCur]]);
		pMenuDisplay->pMainMenu->pCurMenu = (SUB_MENU*)(pMenuItem->pvItemData);

		pNewSubMenu = pMenuDisplay->pMainMenu->pCurMenu;
		pNewSubMenu->nCur = pSubMenu->nCur;
		if(pNewSubMenu->nCur >= pNewSubMenu->nDispItems)
		{
			pNewSubMenu->nCur = pNewSubMenu->nDispItems - 1;
		}

		int nCurrentLangFlag = GetCurrentLangFlag();

		LANG_FILE* pLCDLangFile = GetLCDLangFile();
		ASSERT(pLCDLangFile);

		char* pHeadingInfo = "";

		LANG_TEXT *pLangText;

		pLangText = GetLCDLangText(
			SWITCH_TO_NEXT_EQUIP_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if(pLangText)
		{
			pHeadingInfo = pLangText->pAbbrName[nCurrentLangFlag];
		}

		char szPromptInfo1[MAX_CHARS_OF_LINE] = "";

		if(LCD_SPLIT_PREFIX(pParentSubMenu->nSubMenuID) == PREFIX_VIRTUAL_GROUP_EQUIP)
		{
			nEquipID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

			if(ERR_DXI_OK == DxiGetData(VAR_A_EQUIP_INFO,
				nEquipID,
				0,
				&nBufLen,			
				&pEquipInfo,			
				0))
			{
				if (pEquipInfo->pEquipName)
				{
					snprintf(szPromptInfo1, sizeof(szPromptInfo1),
						"  %s", pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag]);
				}

			}
		}
		else if(pParentSubMenu->nSubMenuID == LCD_MERGE_FIXED_LANG_ID(PRODUCT_INFO_LANG_ID))
		{
			nDeivceID = LCD_SPLIT_ACT_EQUIP_ID(pMenuItem->nMenuItemID);

			PRODUCT_INFO stPI;
			if(ERR_DXI_OK == DXI_GetPIByDeviceID(nDeivceID, &stPI))
			{
				snprintf(szPromptInfo1, sizeof(szPromptInfo1),
						"  %s", stPI.szDeviceAbbrName[nCurrentLangFlag]);
			}
		}


		//DoModal_AcknowlegeDlg_INFO(
		//	"",
		//	pHeadingInfo, 
		//	szPromptInfo1, 
		//	"",
		//	SWITCH_TO_NEW_EQUIP_TIMEOUT);
		DoModal_AcknowlegeDlg_INFO(
			pHeadingInfo, 
			szPromptInfo1, 
			"",
			"",
			SWITCH_TO_NEW_EQUIP_TIMEOUT);

		return TRUE;

	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : HandleSignalScreenEnter
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SUB_MENU**          ppSubMenu : 
 *			  MENU_DISPLAY* pMenuDisplay	:
 *            GLOBAL_VAR_OF_LCD*  pThis     : 
 * RETURN   : int : Need return ID_VIEWING or ID_EDITING. If in password screen, 
 *					maybe return user level or password error info etc. 
 * COMMENTS : Pay attention to the change of menu status cMenuStatus
 * CREATOR  : HULONGWEN                DATE: 2005-03-17 10:48
 *==========================================================================*/
int HandleSignalScreenEnter(SUB_MENU* pSubMenu, 
							MENU_DISPLAY* pMenuDisplay, 
							GLOBAL_VAR_OF_LCD* pThis)
{
	int			nRetID = ID_VIEWING;
	MENU_ITEM*	pCurMenuItem;

	ASSERT(pSubMenu);
	ASSERT(pThis);

	pCurMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[pSubMenu->nCur]]);
	ASSERT(pCurMenuItem);

	if(FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_SIGNAL_DISPLAY)
		||FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_BARCODE))
	{		
		//����һ���豸
		SwitchNextOrPreviousEquip(pMenuDisplay, TRUE);
	}
	else /*MT_SIGNAL_SET || MT_SIGNAL_CONTROL || MT_SELF_DEF_SET|| MT_ALARM_LEVEL_SET || MT_PASSWORD_SELECT*/
	{		
		//change the current menu item from Show status  to Edit status
		if(FLAG_IS_EQUAL(pCurMenuItem->cMenuStatus, MD_SHOW_STATUS))
		{
			// check the hardware switch status, if the HW SWITCH is ON, the control 
			// will be rejected.
			if ((!FLAG_IS_EQUAL(pCurMenuItem->cMenuType, MT_PASSWORD_SELECT))
				&& GetSiteInfo()->bSettingChangeDisabled)
			{
#define PROMPT_HW_STATUS_TIMEOUT 1500
				//prompt hardware switch status
				DoModal_AcknowlegeDlg_ID(HEADING_PROMPT_INFO_LANG_ID, 
					HARDWARE_PROTECT_STATUS_LANG_ID, 
					ESC_OR_ENT_RET_PROMPT_LANG_ID, -1, 
					PROMPT_HW_STATUS_TIMEOUT);

				return ID_VIEWING;

			}

			if((pCurMenuItem->cMenuType == MT_SIGNAL_SET)
				|| (pCurMenuItem->cMenuType == MT_SIGNAL_CONTROL)//set or control signal
				|| (pCurMenuItem->cMenuType == MT_SIGNAL_CUSTOMIZE))
			{
				SIG_BASIC_VALUE*	pSigValue = NULL;

				int nBufLen;

				int nEquipId, nSignalType, nSignalID;

				DXI_SPLIT_UNIQUE_SIG_ID(pCurMenuItem->nMenuItemID, 
					nEquipId, 
					nSignalType, 
					nSignalID);

				if (DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,			
					DXI_MERGE_SIG_ID(nSignalType, nSignalID),
					&nBufLen,			
					&pSigValue,			
					0) == ERR_DXI_OK)
				{
					SET_SIG_INFO* pSetSig = (SET_SIG_INFO*)pCurMenuItem->pvItemData;
					CTRL_SIG_INFO* pCtrlSig = (CTRL_SIG_INFO*)pCurMenuItem->pvItemData;

					if ((nSignalType == SIG_TYPE_CONTROL 
							&& (!(SIG_VALUE_IS_CONTROLLABLE(pSigValue)) 
							|| !CTRL_SIG_IS_CTRL_ON_UI(pCtrlSig, CONTROL_LCD)))
						|| (nSignalType == SIG_TYPE_SETTING 
							&& (!(SIG_VALUE_IS_SETTABLE(pSigValue)) 
							|| !SET_SIG_IS_SET_ON_UI(pSetSig, SET_LCD))))
					{
						DisNotBeCtrlledOrsetInfo(nSignalType);

						return ID_VIEWING;
					}

					//����Ǹ����������ڴ����ʵ��ֵ����Ϊ��ʾֵ
					int iSigValueType;
					char *pDispFmt;
					if((pCurMenuItem->cMenuType == MT_SIGNAL_SET)
						/*|| (pCurMenuItem->cMenuType == MT_SIGNAL_CUSTOMIZE)*/)
					{
						iSigValueType = pSetSig->iSigValueType;
						pDispFmt = pSetSig->szValueDisplayFmt;
					}
					else if(pCurMenuItem->cMenuType == MT_SIGNAL_CONTROL)
					{
						iSigValueType = pCtrlSig->iSigValueType;
						pDispFmt = pCtrlSig->szValueDisplayFmt;
					}
					else if(pCurMenuItem->cMenuType == MT_SIGNAL_CUSTOMIZE)
					{
						if(SIG_TYPE_CONTROL == nSignalType)
						{
							iSigValueType = pCtrlSig->iSigValueType;
							pDispFmt = pCtrlSig->szValueDisplayFmt;
						}
						else //if(SIG_TYPE_SETTING == nSignalType)
						{
							iSigValueType = pSetSig->iSigValueType;
							pDispFmt = pSetSig->szValueDisplayFmt;
						}
					}

					if(iSigValueType == VAR_FLOAT)
					{
						pCurMenuItem->vdData.fValue = AmendFloat(pCurMenuItem->vdData.fValue, pDispFmt);
					}

				}

			}			
			else
			{
				SELF_DEFINE_INFO* pSelfDefineInfo = NULL;

				if (pCurMenuItem->cMenuType == MT_SELF_DEF_SET)
				{
					pSelfDefineInfo = pCurMenuItem->pvItemData;

					ASSERT(pSelfDefineInfo);

					if (!pSelfDefineInfo->bCanBeCtrlledorSet)
					{
						DisNotBeCtrlledOrsetInfo(SIG_TYPE_SETTING);

						return ID_VIEWING;
					}
				}

				//To the VAR_STRING type, should init the string to "0"
				if(IsStringType(pCurMenuItem, &pSelfDefineInfo))
				{
					memset(pSelfDefineInfo->szString, 
						0, 
						sizeof(pSelfDefineInfo->szString));

					pSelfDefineInfo->szString[0] = '0';
				}
				else if(IsPhoneNoType(pCurMenuItem, &pSelfDefineInfo))
				{
					memset(pSelfDefineInfo->szString, 
						0, 
						sizeof(pSelfDefineInfo->szString));

					pSelfDefineInfo->szString[0] = '0';
					//pCurMenuItem->nCursorOffset 
					//	= MAX_CHAR_NUM_PER_LINE - (strlen(pSelfDefineInfo->szString) + 1);
				}
			}

			//If meet the demand, change the menu status and the return ID
			pCurMenuItem->cMenuStatus = MD_EDIT_STATUS;
			pCurMenuItem->nCurrentEditPos = 0;
			nRetID = ID_EDITING;			
		}
		//Acknowledge the set or control operation
		//and change the current menu item from Edit status to Show status
		else if(FLAG_IS_EQUAL(pCurMenuItem->cMenuStatus, MD_EDIT_STATUS))
		{
			int nAckRet;
			int nExecuteRet = ERR_LCD_OK;

			//������ַ������߶��ֶεģ���������һ���༭��
			if (pCurMenuItem->cMenuType == MT_SELF_DEF_SET
				|| pCurMenuItem->cMenuType == MT_PASSWORD_SELECT)
			{
				SELF_DEFINE_INFO* pSelfDefineInfo;
				pSelfDefineInfo = (SELF_DEFINE_INFO*)(pCurMenuItem->pvItemData);
				ASSERT(pSelfDefineInfo);

				if(pSelfDefineInfo->iSigValueType == VAR_STRING)
				{
					if(pSelfDefineInfo->szString[pCurMenuItem->nCurrentEditPos] != ' ')	//blank char
					{
						pCurMenuItem->nCurrentEditPos++;
						pSelfDefineInfo->szString[pCurMenuItem->nCurrentEditPos] = ' ';

						return ID_EDITING;
					}
				}
				else if(pSelfDefineInfo->iSigValueType == VAR_PHONE_NO)
				{
					if(pSelfDefineInfo->szString[pCurMenuItem->nCurrentEditPos] != ' ')	//blank char
					{
						pCurMenuItem->nCurrentEditPos++;
						pSelfDefineInfo->szString[pCurMenuItem->nCurrentEditPos] = ' ';

						return ID_EDITING;
					}
				}
				else if(pSelfDefineInfo->pSectionDataStru)		//Multi Section
				{
					if(pCurMenuItem->nCurrentEditPos < pSelfDefineInfo->iStateNum - 1)
					{
						pCurMenuItem->nCurrentEditPos++;
						return ID_EDITING;
					}
				}
			}

			//�������
			if(pCurMenuItem->cValidateFlag == MENU_ITEM_NEED_VALIDATE)
			{
				if (pThis->cPasswordFlag == NEED_PASSWORD_VALIDATE)
				{
					//Need enter into password dialog
					int nIdRet = GetInputUserAndPwd(pMenuDisplay, pThis);

					if (nIdRet != ID_OK)
					{
						return ID_EDITING;
					}
				}

				if(GetAuthorityLevel(pCurMenuItem) > g_byAuthorityLevel)
				{
#define PROMPT_NO_ENOUGH_PRIVILEGE_TIMEOUT 1500
					//prompt has no enough priviledge
					DoModal_AcknowlegeDlg_ID(HEADING_PROMPT_INFO_LANG_ID, 
						NOT_ENOUGH_PRIVILEGE_LANG_ID, 
						ESC_OR_ENT_RET_PROMPT_LANG_ID, -1, 
						PROMPT_NO_ENOUGH_PRIVILEGE_TIMEOUT);
					
					return ID_EDITING;
				}

			}
			

			nAckRet = DisSetOrCtrlAckInfo(pCurMenuItem);
			if (nAckRet != ID_RETURN)//Need return, nAckRet maybe ID_CANCEL or ID_GOTO_DEF_SCREEN
			{
				return ID_EDITING;
			}

			//����ִ�к���
			if(pCurMenuItem->cMenuType == MT_SELF_DEF_SET
				|| pCurMenuItem->cMenuType == MT_PASSWORD_SELECT)
			{
				SELF_DEFINE_INFO* pSelfDefineInfo;

				pSelfDefineInfo = (SELF_DEFINE_INFO*)(pCurMenuItem->pvItemData);
				ASSERT(pSelfDefineInfo);

				ITEM_SET_FUNC	pItemSetFunc;
				pItemSetFunc = (ITEM_SET_FUNC)GetItemHandleFunc(pSelfDefineInfo->iSigID, FALSE);
				ASSERT(pItemSetFunc);

				//execute set function
				nExecuteRet = pItemSetFunc(pCurMenuItem);

			}
			else if (pCurMenuItem->cMenuType == MT_ALARM_LEVEL_SET)
			{
				nExecuteRet = SetAlarmLevelSetFunc(pCurMenuItem);
			}
			else if (pCurMenuItem->cMenuType == MT_ALARM_RELAY_SET)
			{
				nExecuteRet = SetAlarmRelaySetFunc(pCurMenuItem);
			}	
			else
			{
				nExecuteRet = SetSignalInfo(pCurMenuItem);
			}


#define SIGNAL_SETTING_FAILURE_TIMEOUT 1500

			//Password screen, return directly
			if (pCurMenuItem->cMenuType == MT_PASSWORD_SELECT)
			{
				//Should change the pwd menu status to show status
				pCurMenuItem->cMenuStatus = MD_SHOW_STATUS;

				return nExecuteRet;
			}
			else if (nExecuteRet == ERR_EQP_CTRL_SUPPRESSED)
			{
				DoModal_AcknowlegeDlg_ID(
					HEADING_PROMPT_INFO_LANG_ID, 
					CONFLICT_SETTING_LANG_ID, 
					ESC_OR_ENT_RET_PROMPT_LANG_ID, -1, 
					SIGNAL_SETTING_FAILURE_TIMEOUT);

				return ID_EDITING;//Set again
			}
			else if (nExecuteRet != ERR_DXI_OK && nExecuteRet != ERR_LCD_OK)
			{
				DoModal_AcknowlegeDlg_ID(
					HEADING_PROMPT_INFO_LANG_ID, 
					CONTROL_OR_SETTING_FAILED_LANG_ID, 
					ESC_OR_ENT_RET_PROMPT_LANG_ID, -1, 
					SIGNAL_SETTING_FAILURE_TIMEOUT);
			}

			//If meet the demand, change the menu status and the return ID
			pCurMenuItem->cMenuStatus = MD_SHOW_STATUS;	
			nRetID = ID_VIEWING;			
		}
	}

	return nRetID;
}


static int HandleSignalScreenEnterUp(SUB_MENU* pSubMenu, 
							MENU_DISPLAY* pMenuDisplay, 
							GLOBAL_VAR_OF_LCD* pThis)
{
	ASSERT(pSubMenu);
	ASSERT(pMenuDisplay);
	ASSERT(pThis);

	int nRetID = ID_VIEWING;

	MENU_ITEM *pCurMenuItem;
	pCurMenuItem = &(pSubMenu->pItems[pSubMenu->pDispItems[pSubMenu->nCur]]);
	ASSERT(pCurMenuItem);

	if(pCurMenuItem->cMenuStatus != MD_SHOW_STATUS)
	{
		return nRetID;
	}
	SUB_MENU *pNewSubMenu;
	int nSubMenuID;

	if(pCurMenuItem->cMenuType == MT_ALARM_LEVEL_SET)
	{
		nSubMenuID = LCD_MERGE_STD_EQUIP_ID(SIG_TYPE_ALARM_RELAY_SET, LCD_SPLIT_STD_EQUIP_ID(pSubMenu->nSubMenuID));
	}
	else if(pCurMenuItem->cMenuType == MT_ALARM_RELAY_SET)
	{
		nSubMenuID = LCD_MERGE_STD_EQUIP_ID(SIG_TYPE_ALARM_LEVEL_SET, LCD_SPLIT_STD_EQUIP_ID(pSubMenu->nSubMenuID));
	}
	else
	{
		return nRetID;
	}

	pNewSubMenu = GetSubMenuFromID(pMenuDisplay->pMainMenu->pRootMenu, nSubMenuID);
	if(!pNewSubMenu)
	{
		return nRetID;
	}

	pNewSubMenu->nCur = pSubMenu->nCur;
	pNewSubMenu->nTop = pSubMenu->nTop;
	pNewSubMenu->nBottom = pSubMenu->nBottom;

	pMenuDisplay->pMainMenu->pCurMenu = pNewSubMenu;


	//��ʾ
	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	char* pHeadingInfo = "";
	char* pPromptInfo1 = "";
	char* pPromptInfo2 = "";

	LANG_TEXT *pLangText;

	pLangText = GetLCDLangText(
		HEADING_PROMPT_INFO_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);
	if(pLangText)
	{
		pHeadingInfo = pLangText->pAbbrName[nCurrentLangFlag];
	}

	pLangText = GetLCDLangText(
		SWITCH_TO_SET_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);
	if(pLangText)
	{
		pPromptInfo1 = pLangText->pAbbrName[nCurrentLangFlag];
	}

	if(pCurMenuItem->cMenuType == MT_ALARM_LEVEL_SET)
	{
		pLangText = GetLCDLangText(
			ALARM_RELAY_SET_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
	}
	else
	{
		pLangText = GetLCDLangText(
			ALARM_LEVEL_SET_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
	}
	if(pLangText)
	{
		pPromptInfo2 = pLangText->pAbbrName[nCurrentLangFlag];
	}


	DoModal_AcknowlegeDlg_INFO(
		pHeadingInfo, 
		pPromptInfo1, 
		pPromptInfo2, 
		"",
		SWITCH_TO_NEW_EQUIP_TIMEOUT);

	return nRetID;
}




/*==========================================================================*
 * FUNCTION : DoModal_SignalsScreen
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*       pMenuDisplay : 
 *            GLOBAL_VAR_OF_LCD*  pThis        : 
 * RETURN   : int : should return ID_GOTO_RPEVIOUS
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-03-17 19:03
 *==========================================================================*/
int DoModal_SignalsScreen(MENU_DISPLAY* pMenuDisplay, GLOBAL_VAR_OF_LCD* pThis)
{
	ASSERT(pMenuDisplay->pMainMenu->pCurMenu);

	UCHAR byKeyValue;

	int nGetValueType = GET_VALUE_FROM_SYSTEM;

	BOOL bRefreshAtOnce = TRUE;

	int nIDRet = ID_VIEWING;

	SUB_MENU* pPreSubMenu = NULL;

	SUB_MENU  *pSubMenu;
	pSubMenu = pMenuDisplay->pMainMenu->pCurMenu;

	STACK_PVOID* pStack;
	pStack	= &(pMenuDisplay->pMainMenu->stack);

	//////////////////////////////////////////////////////////////////////////
	RefreshSubMenuStatus(pSubMenu, FALSE);
	//////////////////////////////////////////////////////////////////////////

	ClearScreen();
	if(DisplaySignals(pSubMenu, nGetValueType) == ID_GOTO_RPEVIOUS)
	{
		return ID_GOTO_RPEVIOUS;
	}

	for(;;)
	{
		byKeyValue = GetKeyEx();

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();

			switch( byKeyValue )
			{
			case VK_ESCAPE:  
				{
					if(HandleSignalScreenEscape(pSubMenu) == ID_GOTO_RPEVIOUS)
					{
						return ID_GOTO_RPEVIOUS;
					}
					else
					{
						nIDRet = ID_VIEWING;
					}

				}

				break;

			case VK_UP:  
			case VK_DOWN:
				{
					nIDRet = HandleSignalScreenUpOrDown(
						pSubMenu, pStack, (byKeyValue == VK_UP), pThis);						
				}

				break;

			case VK_ENTER:   
				{
					nIDRet = HandleSignalScreenEnter(pSubMenu, 
						pMenuDisplay, pThis);

					//���ܸĹ�pSubMenu:�ڲɼ��ź����ﰴENTER
					pSubMenu = pMenuDisplay->pMainMenu->pCurMenu;
				}

				break;

			case VK_ENTER_UP:
				{
					DisNavigationInfo(pMenuDisplay);
				}
				break;

			default:
				break;
			}

			if(nIDRet == ID_VIEWING)
			{
				nGetValueType = GET_VALUE_FROM_SYSTEM;
			}
			else if(nIDRet == ID_EDITING)
			{
				nGetValueType = GET_VALUE_FROM_EDITING;
			}
			
			//others IDRet handle	
			bRefreshAtOnce = TRUE;

		}
		else
		{
			int nRetHandleIdle = HandleIdle();

			if(nRetHandleIdle == ID_GOTO_DEF_SCREEN)
			{
				pSubMenu->pItems[pSubMenu->pDispItems[pSubMenu->nCur]].cMenuStatus = MD_SHOW_STATUS;

				return ID_GOTO_RPEVIOUS;
			}
			else if(nRetHandleIdle == ID_INIT_LCD_JUST_NOW)
			{
				bRefreshAtOnce = TRUE;
			}
		}

		ASSERT(pThis);

		if(pThis->cDataRefreshFlag == NEED_REFRESH_DATA || bRefreshAtOnce)
		{
			bRefreshAtOnce = FALSE;
			pThis->cDataRefreshFlag = NEED_NOT_REFRESH_DATA;

			if(DisplaySignals(pSubMenu, nGetValueType) == ID_GOTO_RPEVIOUS)
			{
				return ID_GOTO_RPEVIOUS;
			}

		}

	}

}

int ConvertValue_ForDefMenu(MENU_ITEM*	pMenuItem,
							int			nBufLength, 
							char*		pDisBuf,
							int			nGetType)
{
	return ConvertValueToString(pMenuItem, 
		nBufLength,
		pDisBuf, 
		nGetType,
		CHARS_OF_UNIT_FOR_DEF_MENU);;
}

/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_alarms_screen.c
*  CREATOR  : HULONGWEN                DATE: 2004-11-25 15:12
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

#include "lcd_public.h"

#include "lcd_alarms_screen.h"

#include "lcd_ack_dlg.h"



#define A_ALARM_OCCUPIED_LINES	(SCREEN_HEIGHT / 16)

#define ALARM_PROMPT_TIMEOUT 1000//1s
static DWORD  LCD_GetRectHighSNValue(IN int iEquipID);
static BOOL LCD_GetRectHighValid(IN int iEquipID);
static int LCD_GetEquipName(IN OUT char *szEquipName);
static char *LCD_GetEquipNameFromInterface(IN int iEquipID,IN int iLanguage,IN int iPosition);

extern MENU_ITEM* g_pActiveAlarmMenuItem;
extern MENU_ITEM* g_pHistoryAlarmMenuItem;

extern BOOL g_bNeedSwithToNextPage;//Frank Wu,20160127, for MiniNCU
extern BOOL g_bNeedSwithToFirstPage;


/*==========================================================================*
 * FUNCTION : GetAlarmLevelLangText
 * PURPOSE  : Get alarm level desciption test according to its level
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nAlarmLevel : alarm level
 * RETURN   : static LANG_TEXT* : Ret LANG_TEXT pointer
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-07 17:26
 *==========================================================================*/
static LANG_TEXT* GetAlarmLevelLangText(int nAlarmLevel)
{
	LANG_TEXT*	pLangText;

	int			nAlarmLevelLangID;

	LANG_FILE*	pLCDLangFile = GetLCDLangFile();

	if (nAlarmLevel == ALARM_LEVEL_OBSERVATION)
	{
		nAlarmLevelLangID = OBSERVE_ALARM_LANG_ID;
	}
	else if (nAlarmLevel == ALARM_LEVEL_MAJOR)
	{
		nAlarmLevelLangID = MAJOR_ALARM_LANG_ID;
	}
	else if (nAlarmLevel == ALARM_LEVEL_CRITICAL)
	{
		nAlarmLevelLangID = CRITICAL_ALARM_LANG_ID;
	}
	else
	{
		return NULL;
	}

	ASSERT(pLCDLangFile);

	pLangText = GetLCDLangText(
		nAlarmLevelLangID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	return pLangText;
}

/*==========================================================================*
 * FUNCTION : DisplayAlarmItem
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_ITEM*  pMenuItem : Item info
 *            int         x         : x offset on screen
 *            int         y         : y offset on screen
 *            char*       pLangCode : Current language code
 *            int         nSequence   : The sequence ofa alarm
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-08 10:15
 *==========================================================================*/
static int DisplayAlarmItem(MENU_ITEM* pMenuItem,
							int x,
							int y,
							char* pLangCode,
							int	nSequence)
{
	//by HULONGWEN, assume x = 0

	char szDisBuf[MAX_CHARS_OF_LINE];

	int nDrawWidth;

	int nFontHeight = GetFontHeightFromCode(pLangCode);

	int nCurrentLangFlag = GetCurrentLangFlag();


	char cDispType = FLAG_DIS_NORMAL_TYPE;

	int i;

	ALARM_SIG_VALUE* pALarmSigValue;
	HIS_ALARM_RECORD* pHisAlarmRecord;

	EQUIP_INFO* pEquipInfo = NULL;
	ALARM_SIG_INFO* pAlarmSigInfo = NULL;

	time_t tmStart;
	struct tm tmAlarmTime;
	char	*pszEquipName;

	ASSERT(pMenuItem);


	if (pMenuItem->cMenuType == MT_ACTIVE_ALARM_DISPLAY)
	{
		//begin by hulw 061219 In alarm screen, LED flash for rectifier alarm
		int nSigType;
		int nSigID;
		VAR_VALUE vdValue;

		MENU_ITEM* pLastMenuItem = NULL;

		pALarmSigValue = 
			&(((ALARM_SIG_VALUE_EX_LCD*)pMenuItem->pvItemData)->bvAlarm);

		ASSERT(pALarmSigValue);

		pEquipInfo = pALarmSigValue->pEquipInfo;

		pAlarmSigInfo = pALarmSigValue->pStdSig;

		ASSERT(pEquipInfo != NULL && pAlarmSigInfo != NULL);

		tmStart = pALarmSigValue->sv.tmStartTime;

		if (pLastMenuItem != pMenuItem)
		{
			vdValue.enumValue = RECT_TWINKLE_OFF;

			DXI_SPLIT_SIG_ID(SIG_ID_ALL_RECT_TWINKLE, nSigType, nSigID);

			SetSignalValueInterface(
				DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID), 
				nSigType, 
				nSigID,
				vdValue,
				SERVICE_OF_LOGIC_CONTROL,
				EQUIP_CTRL_SEND_DIRECTLY,
				0);//Don't wait


			DXI_SPLIT_SIG_ID(SIG_ID_ALL_CONV_TWINKLE, nSigType, nSigID);

			SetSignalValueInterface(
				DXI_GetEquipIDFromStdEquipID(CONV_GROUP_STD_EQUIP_ID), 
				nSigType, 
				nSigID,
				vdValue,
				SERVICE_OF_LOGIC_CONTROL,
				EQUIP_CTRL_SEND_DIRECTLY,
				0);//Don't wait

			pLastMenuItem = pMenuItem;

			if (pEquipInfo->iEquipTypeID == RECT_STD_EQUIP_ID)
			{
				vdValue.enumValue = RECT_TWINKLE_ON;

				DXI_SPLIT_SIG_ID(SIG_ID_RECT_TWINKLE, nSigType, nSigID);

				SetSignalValueInterface(pEquipInfo->iEquipID, 
					nSigType, 
					nSigID,
					vdValue,
					SERVICE_OF_LOGIC_CONTROL,
					EQUIP_CTRL_SEND_DIRECTLY,
					0);//Don't wait
			}
			else if(pEquipInfo->iEquipTypeID == CONV_STD_EQUIP_ID)
			{
				vdValue.enumValue = RECT_TWINKLE_ON;

				DXI_SPLIT_SIG_ID(SIG_ID_CONV_TWINKLE, nSigType, nSigID);

				SetSignalValueInterface(pEquipInfo->iEquipID, 
					nSigType, 
					nSigID,
					vdValue,
					SERVICE_OF_LOGIC_CONTROL,
					EQUIP_CTRL_SEND_DIRECTLY,
					0);//Don't wait
			}

		}

		
		//end
	}
	else if (pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY)
	{
		int nError;
		int nBufLen;

		pHisAlarmRecord = (HIS_ALARM_RECORD*)pMenuItem->pvItemData;

		ASSERT(pHisAlarmRecord);

		nError = DxiGetData(VAR_A_EQUIP_INFO,			
			pHisAlarmRecord->iEquipID,	
			0,
			&nBufLen,			
			&pEquipInfo,			
			0);

		nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,			
			pHisAlarmRecord->iEquipID,	
			DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, pHisAlarmRecord->iAlarmID),
			&nBufLen,			
			&pAlarmSigInfo,			
			0);

		if (pEquipInfo == NULL || pAlarmSigInfo == NULL)
		{
			return ERR_LCD_DXI_GET_DATA_FAILURE;
		}

		tmStart = pHisAlarmRecord->tmStartTime;
	}		

	for(i = 0; i < 4; i++)
	{
		switch(i)
		{
		case 0:
			//0. display the alarm owner name
			//To rect, need to display serial NO, but active alarm need to display Position No
			if (pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY
				&& (pHisAlarmRecord->szSerialNumber)[0] != '\0'
				&& (pHisAlarmRecord->iEquipID < 3 || pHisAlarmRecord->iEquipID > 102))
			{
				snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
					pHisAlarmRecord->szSerialNumber);

			}
			else if (pEquipInfo->iEquipTypeID != RECT_STD_EQUIP_ID)
			{
				snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
					pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag]);
			}

			else if(pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY)
			{
				int nBufLen;
				SIG_BASIC_VALUE* pSigValue = NULL;

				if (DxiGetData(VAR_A_SIGNAL_VALUE,
					pEquipInfo->iEquipID,			
					SIG_ID_RECT_SN,
					&nBufLen,			
					&pSigValue,			
					0) == ERR_DXI_OK)
				{
					
					pszEquipName = LCD_GetEquipNameFromInterface(pHisAlarmRecord->iEquipID, nCurrentLangFlag, pHisAlarmRecord->iPosition);
					if(pszEquipName)
					{			
						snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
							pszEquipName);

						DELETE(pszEquipName);
						pszEquipName = NULL;
					}
					else
					{
						snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
							pszEquipName);
					}
					
				}
			}
#ifdef DISP_RECT_POSITION_NO
			else
			{
				int nBufLen;
				SIG_BASIC_VALUE* pSigValue = NULL;

				int		iPosNo = LUI_GetDwordSigValue(pEquipInfo->iEquipID,
													SIG_TYPE_SAMPLING,
													SIG_ID_POSTION_NO);
				if((iPosNo > 0) && (iPosNo < 1000))
				{
					snprintf(szDisBuf,
							sizeof(szDisBuf),
							"%d %s(%03d)", 
							nSequence + 1,
							pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag],
							iPosNo);
				}
				else
				{
					snprintf(szDisBuf,
							sizeof(szDisBuf),
							"%d %s", 
							nSequence + 1,
							pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag]);
				
				}

			}
#else
			else
			{
				int nBufLen;
				SIG_BASIC_VALUE* pSigValue = NULL;

				if (DxiGetData(VAR_A_SIGNAL_VALUE,
					pEquipInfo->iEquipID,			
					SIG_ID_RECT_SN,
					&nBufLen,			
					&pSigValue,			
					0) == ERR_DXI_OK)
				{
					if (pSigValue != NULL)
					{
						//Modify by YangGuoxin.3/16/2006
						//if(!LCD_GetRectHighValid(pEquipInfo->iEquipID))
						//{
						//	snprintf(szDisBuf, sizeof(szDisBuf), "%d 2%02d%01X%06d", 
						//		nSequence + 1,
						//	//	(((BYTE*)&(pSigValue->varValue))[0] & 0x1F),
						//	//	(((BYTE*)&(pSigValue->varValue))[1] & 0x0F),
						//		((pSigValue->varValue.ulValue) >> 24) & 0x1F,
						//		((pSigValue->varValue.ulValue) >> 16) & 0x0F,
						//		LOWORD((DWORD)(pSigValue->varValue.ulValue)));
						//}
						//else
						//{
						//	snprintf(szDisBuf, sizeof(szDisBuf), "%d %02u%02d%02d%05d", 
						//		nSequence + 1,
						//		(unsigned int)LCD_GetRectHighSNValue(pEquipInfo->iEquipID),
						//	//	(((BYTE*)&(pSigValue->varValue))[0] & 0x1F),
						//	//	(((BYTE*)&(pSigValue->varValue))[1] & 0x0F),
						//		((pSigValue->varValue.ulValue) >> 24) & 0x1F,
						//		((pSigValue->varValue.ulValue) >> 16) & 0x0F,
						//		LOWORD((DWORD)(pSigValue->varValue.ulValue)));
						//	TRACE("\n%d = %s\n", pEquipInfo->iEquipID, szDisBuf);
						//}
						snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
							pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag]);
						
						//End by YangGuoxin;3/16/2006
					}
				}
			}
#endif

			break;

		case 1:
			//1. display the alarm name
			snprintf(szDisBuf, sizeof(szDisBuf), "%s", 				
				pAlarmSigInfo->pSigName->pAbbrName[nCurrentLangFlag]);
					break;

		case 2:
			//2.Start time
			ConvertTime(&tmStart, TRUE);

			strftime(szDisBuf, sizeof(szDisBuf), "%y%m%d %H:%M:%S", 
				gmtime_r(&(tmStart), &tmAlarmTime));

			break;

		case 3:
			{
				//For active alarm:alarm level
				if(pMenuItem->cMenuType == MT_ACTIVE_ALARM_DISPLAY)
				{
					LANG_TEXT*	pLangText;

					pLangText = GetAlarmLevelLangText(
						pALarmSigValue->pStdSig->iAlarmLevel);

					if (pLangText != NULL)
					{
						snprintf(szDisBuf, sizeof(szDisBuf), 
							"%s", pLangText->pAbbrName[nCurrentLangFlag]);
					}
					else
					{
						strcpy(szDisBuf, "");
					}
				}
				//For history alarm: end time
				else if (pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY)
				{
					time_t tmEnd;

					tmEnd = pHisAlarmRecord->tmEndTime;

					ConvertTime(&tmEnd, TRUE);

					strftime(szDisBuf, sizeof(szDisBuf), "%y%m%d %H:%M:%S", 
						gmtime_r(&(tmEnd), &tmAlarmTime));
				}
			}
			break;

		default:
			break;
		}


		nDrawWidth = 
			GetWidthOfString(szDisBuf, (int)strlen(szDisBuf), pLangCode);

		DisString(szDisBuf, 
			(int)strlen(szDisBuf),  
			x,
			y + i * nFontHeight * SCREEN_HEIGHT / 64,
			pLangCode, 
			cDispType);

		ClearBar(x + nDrawWidth,
			y + i * nFontHeight * SCREEN_HEIGHT / 64,
			SCREEN_WIDTH  - ARROW_WIDTH - (x + nDrawWidth),
			nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);
	}

	return ERR_LCD_OK;

}


//Frank Wu,20160127, for MiniNCU
static int DisplayAlarmItemMiniNCU(MENU_ITEM* pMenuItem,
							int x,
							int y,
							char* pLangCode,
							int	nSequence)
{
	//by HULONGWEN, assume x = 0

	char szDisBuf[MAX_CHARS_OF_LINE];

	int nDrawWidth;

	int nFontHeight = GetFontHeightFromCode(pLangCode);

	int nCurrentLangFlag = GetCurrentLangFlag();


	char cDispType = FLAG_DIS_NORMAL_TYPE;

	int i;

	ALARM_SIG_VALUE* pALarmSigValue;
	HIS_ALARM_RECORD* pHisAlarmRecord;

	EQUIP_INFO* pEquipInfo = NULL;
	ALARM_SIG_INFO* pAlarmSigInfo = NULL;

	time_t tmStart;
	struct tm tmAlarmTime;
	char	*pszEquipName;


	ASSERT(pMenuItem);


	if (pMenuItem->cMenuType == MT_ACTIVE_ALARM_DISPLAY)
	{
		//begin by hulw 061219 In alarm screen, LED flash for rectifier alarm
		int nSigType;
		int nSigID;
		VAR_VALUE vdValue;

		MENU_ITEM* pLastMenuItem = NULL;

		pALarmSigValue = 
			&(((ALARM_SIG_VALUE_EX_LCD*)pMenuItem->pvItemData)->bvAlarm);

		ASSERT(pALarmSigValue);

		pEquipInfo = pALarmSigValue->pEquipInfo;

		pAlarmSigInfo = pALarmSigValue->pStdSig;

		ASSERT(pEquipInfo != NULL && pAlarmSigInfo != NULL);

		tmStart = pALarmSigValue->sv.tmStartTime;

		if (pLastMenuItem != pMenuItem)
		{
			vdValue.enumValue = RECT_TWINKLE_OFF;

			DXI_SPLIT_SIG_ID(SIG_ID_ALL_RECT_TWINKLE, nSigType, nSigID);

			SetSignalValueInterface(
				DXI_GetEquipIDFromStdEquipID(RECT_GROUP_STD_EQUIP_ID), 
				nSigType, 
				nSigID,
				vdValue,
				SERVICE_OF_LOGIC_CONTROL,
				EQUIP_CTRL_SEND_DIRECTLY,
				0);//Don't wait


			DXI_SPLIT_SIG_ID(SIG_ID_ALL_CONV_TWINKLE, nSigType, nSigID);

			SetSignalValueInterface(
				DXI_GetEquipIDFromStdEquipID(CONV_GROUP_STD_EQUIP_ID), 
				nSigType, 
				nSigID,
				vdValue,
				SERVICE_OF_LOGIC_CONTROL,
				EQUIP_CTRL_SEND_DIRECTLY,
				0);//Don't wait

			pLastMenuItem = pMenuItem;

			if (pEquipInfo->iEquipTypeID == RECT_STD_EQUIP_ID)
			{
				vdValue.enumValue = RECT_TWINKLE_ON;

				DXI_SPLIT_SIG_ID(SIG_ID_RECT_TWINKLE, nSigType, nSigID);

				SetSignalValueInterface(pEquipInfo->iEquipID, 
					nSigType, 
					nSigID,
					vdValue,
					SERVICE_OF_LOGIC_CONTROL,
					EQUIP_CTRL_SEND_DIRECTLY,
					0);//Don't wait
			}
			else if(pEquipInfo->iEquipTypeID == CONV_STD_EQUIP_ID)
			{
				vdValue.enumValue = RECT_TWINKLE_ON;

				DXI_SPLIT_SIG_ID(SIG_ID_CONV_TWINKLE, nSigType, nSigID);

				SetSignalValueInterface(pEquipInfo->iEquipID, 
					nSigType, 
					nSigID,
					vdValue,
					SERVICE_OF_LOGIC_CONTROL,
					EQUIP_CTRL_SEND_DIRECTLY,
					0);//Don't wait
			}

		}


		//end
	}
	else if (pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY)
	{
		int nError;
		int nBufLen;

		pHisAlarmRecord = (HIS_ALARM_RECORD*)pMenuItem->pvItemData;

		ASSERT(pHisAlarmRecord);

		nError = DxiGetData(VAR_A_EQUIP_INFO,			
			pHisAlarmRecord->iEquipID,	
			0,
			&nBufLen,			
			&pEquipInfo,			
			0);

		nError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,			
			pHisAlarmRecord->iEquipID,	
			DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, pHisAlarmRecord->iAlarmID),
			&nBufLen,			
			&pAlarmSigInfo,			
			0);

		if (pEquipInfo == NULL || pAlarmSigInfo == NULL)
		{
			return ERR_LCD_DXI_GET_DATA_FAILURE;
		}

		tmStart = pHisAlarmRecord->tmStartTime;
	}		


	int				iStartItemID;
	int				iaSwitchTable[] = {0, 2};
	int				iSwitchTableIndex;

	//switch to next page of one alarm item
	iSwitchTableIndex = LCD_SwitchToNextIndex(g_bNeedSwithToFirstPage, g_bNeedSwithToNextPage, -1, ITEM_OF(iaSwitchTable));
	iStartItemID = iaSwitchTable[iSwitchTableIndex];
	
	g_bNeedSwithToFirstPage = FALSE;
	g_bNeedSwithToNextPage = FALSE;

	//for(i = 0; i < 4; i++)
	for(i = iStartItemID; i < iStartItemID + 2; i++)
	{
		switch(i)
		{
		case 0:
			//0. display the alarm owner name
			//To rect, need to display serial NO, but active alarm need to display Position No
			if (pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY
				&& (pHisAlarmRecord->szSerialNumber)[0] != '\0'
				&& (pHisAlarmRecord->iEquipID < 3 || pHisAlarmRecord->iEquipID > 102))
			{
				snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
					pHisAlarmRecord->szSerialNumber);

			}
			else if (pEquipInfo->iEquipTypeID != RECT_STD_EQUIP_ID)
			{
				snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
					pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag]);
			}

			else if(pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY)
			{
				int nBufLen;
				SIG_BASIC_VALUE* pSigValue = NULL;

				if (DxiGetData(VAR_A_SIGNAL_VALUE,
					pEquipInfo->iEquipID,			
					SIG_ID_RECT_SN,
					&nBufLen,			
					&pSigValue,			
					0) == ERR_DXI_OK)
				{

					pszEquipName = LCD_GetEquipNameFromInterface(pHisAlarmRecord->iEquipID, nCurrentLangFlag, pHisAlarmRecord->iPosition);
					if(pszEquipName)
					{			
						snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
							pszEquipName);

						DELETE(pszEquipName);
						pszEquipName = NULL;
					}
					else
					{
						snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
							pszEquipName);
					}

				}
			}
#ifdef DISP_RECT_POSITION_NO
			else
			{
				int nBufLen;
				SIG_BASIC_VALUE* pSigValue = NULL;

				int		iPosNo = LUI_GetDwordSigValue(pEquipInfo->iEquipID,
					SIG_TYPE_SAMPLING,
					SIG_ID_POSTION_NO);
				if((iPosNo > 0) && (iPosNo < 1000))
				{
					snprintf(szDisBuf,
						sizeof(szDisBuf),
						"%d %s(%03d)", 
						nSequence + 1,
						pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag],
						iPosNo);
				}
				else
				{
					snprintf(szDisBuf,
						sizeof(szDisBuf),
						"%d %s", 
						nSequence + 1,
						pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag]);

				}

			}
#else
			else
			{
				int nBufLen;
				SIG_BASIC_VALUE* pSigValue = NULL;

				if (DxiGetData(VAR_A_SIGNAL_VALUE,
					pEquipInfo->iEquipID,			
					SIG_ID_RECT_SN,
					&nBufLen,			
					&pSigValue,			
					0) == ERR_DXI_OK)
				{
					if (pSigValue != NULL)
					{
						//Modify by YangGuoxin.3/16/2006
						//if(!LCD_GetRectHighValid(pEquipInfo->iEquipID))
						//{
						//	snprintf(szDisBuf, sizeof(szDisBuf), "%d 2%02d%01X%06d", 
						//		nSequence + 1,
						//	//	(((BYTE*)&(pSigValue->varValue))[0] & 0x1F),
						//	//	(((BYTE*)&(pSigValue->varValue))[1] & 0x0F),
						//		((pSigValue->varValue.ulValue) >> 24) & 0x1F,
						//		((pSigValue->varValue.ulValue) >> 16) & 0x0F,
						//		LOWORD((DWORD)(pSigValue->varValue.ulValue)));
						//}
						//else
						//{
						//	snprintf(szDisBuf, sizeof(szDisBuf), "%d %02u%02d%02d%05d", 
						//		nSequence + 1,
						//		(unsigned int)LCD_GetRectHighSNValue(pEquipInfo->iEquipID),
						//	//	(((BYTE*)&(pSigValue->varValue))[0] & 0x1F),
						//	//	(((BYTE*)&(pSigValue->varValue))[1] & 0x0F),
						//		((pSigValue->varValue.ulValue) >> 24) & 0x1F,
						//		((pSigValue->varValue.ulValue) >> 16) & 0x0F,
						//		LOWORD((DWORD)(pSigValue->varValue.ulValue)));
						//	TRACE("\n%d = %s\n", pEquipInfo->iEquipID, szDisBuf);
						//}
						snprintf(szDisBuf, sizeof(szDisBuf),"%d %s", nSequence + 1,
							pEquipInfo->pEquipName->pAbbrName[nCurrentLangFlag]);

						//End by YangGuoxin;3/16/2006
					}
				}
			}
#endif

			break;

		case 1:
			//1. display the alarm name
			snprintf(szDisBuf, sizeof(szDisBuf), "%s", 				
				pAlarmSigInfo->pSigName->pAbbrName[nCurrentLangFlag]);
			break;

		case 2:
			//2.Start time
			ConvertTime(&tmStart, TRUE);

			strftime(szDisBuf, sizeof(szDisBuf), "%y%m%d %H:%M:%S", 
				gmtime_r(&(tmStart), &tmAlarmTime));

			break;

		case 3:
			{
				//For active alarm:alarm level
				if(pMenuItem->cMenuType == MT_ACTIVE_ALARM_DISPLAY)
				{
					LANG_TEXT*	pLangText;

					pLangText = GetAlarmLevelLangText(
						pALarmSigValue->pStdSig->iAlarmLevel);

					if (pLangText != NULL)
					{
						snprintf(szDisBuf, sizeof(szDisBuf), 
							"%s", pLangText->pAbbrName[nCurrentLangFlag]);
					}
					else
					{
						strcpy(szDisBuf, "");
					}
				}
				//For history alarm: end time
				else if (pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY)
				{
					time_t tmEnd;

					tmEnd = pHisAlarmRecord->tmEndTime;

					ConvertTime(&tmEnd, TRUE);

					strftime(szDisBuf, sizeof(szDisBuf), "%y%m%d %H:%M:%S", 
						gmtime_r(&(tmEnd), &tmAlarmTime));
				}
			}
			break;

		default:
			break;
		}


		nDrawWidth = 
			GetWidthOfString(szDisBuf, (int)strlen(szDisBuf), pLangCode);

		DisString(szDisBuf, 
			(int)strlen(szDisBuf),  
			x,
			y + (i - iStartItemID)* nFontHeight ,
			pLangCode, 
			cDispType);

		ClearBar(x + nDrawWidth,
			y + (i - iStartItemID) * nFontHeight,
			SCREEN_WIDTH  - ARROW_WIDTH - (x + nDrawWidth),
			nFontHeight, 
			FLAG_DIS_NORMAL_TYPE);
	}

	return ERR_LCD_OK;

}

/*==========================================================================*
 * FUNCTION : DisplayAlarms
 * PURPOSE  : Display active or history alarm
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SUB_MENU*  pSubMenu : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-08 10:18
 *==========================================================================*/
static int DisplayAlarms(SUB_MENU* pSubMenu)
{
	ASSERT(pSubMenu);

	char*		pLangCode = GetCurrentLangCode();
	ASSERT(pLangCode);

	int			i;

	MENU_ITEM*	pItems;
	MENU_ITEM* pCurMenuItem;


	int			nFontHeight;
	int			nMaxRow;
	int			nSigNumOfScreen;

	nFontHeight = GetFontHeightFromCode(pLangCode);
	nMaxRow = SCREEN_HEIGHT/nFontHeight;

	//Calcute alarm number on screen
	nSigNumOfScreen = nMaxRow/A_ALARM_OCCUPIED_LINES;

	pCurMenuItem = &(pSubMenu->pItems[pSubMenu->nCur]);

	ASSERT(pCurMenuItem);

	if(pSubMenu->nCur != pSubMenu->nTop)
	{
		pSubMenu->nTop		= pSubMenu->nCur;
		pSubMenu->nBottom	= pSubMenu->nTop + (nSigNumOfScreen - 1);
	}


	if(pSubMenu->nBottom != pSubMenu->nTop + (nSigNumOfScreen - 1))
	{
		pSubMenu->nBottom	= pSubMenu->nTop + (nSigNumOfScreen - 1);
	}

	pItems = pSubMenu->pItems + pSubMenu->nTop;

	for(i = pSubMenu->nTop; (i <= pSubMenu->nBottom && i < pSubMenu->nItems); 
		i++, pItems++)
	{
		//Frank Wu,20160127, for MiniNCU
		//DisplayAlarmItem(pItems, 
		DisplayAlarmItemMiniNCU(pItems,
			0, 
			(i - pSubMenu->nTop) * nFontHeight * A_ALARM_OCCUPIED_LINES, 
			pLangCode,
			i);

	}

	//Clear blank rect
	if(pSubMenu->nBottom >= pSubMenu->nItems)
	{
		int nClearY = (pSubMenu->nItems - pSubMenu->nTop) 
			* nFontHeight * A_ALARM_OCCUPIED_LINES;

		ClearBar( 0,
			nClearY,
			SCREEN_WIDTH - ARROW_WIDTH,
			SCREEN_HEIGHT - nClearY, 
			FLAG_DIS_NORMAL_TYPE);
	}

	//display progress bar
	DisplayProgressBar(pSubMenu->nItems,
		pSubMenu->nCur,
		pSubMenu->nTop,
		pSubMenu->nBottom);

	return ERR_LCD_OK;
}

/*==========================================================================*
 * FUNCTION : DisALarmNumPromptInfo
 * PURPOSE  : Display alarm number prompt info
 *			  Include OA\MA\CA
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SUB_MENU*  pSubMenu : 
 * RETURN   : static : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-08 10:21
 *==========================================================================*/
static int DisALarmNumPromptInfo(SUB_MENU* pSubMenu, int nTimeOut)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	int nObeserAlarmNum = 0;
	int nMajorAlarmNum = 0;
	int nCriticalAlarmNum = 0;

	int nAlarmCount = pSubMenu->nItems;
	MENU_ITEM* pMenuItem = pSubMenu->pItems;

	int nAlarmLevel = ALARM_LEVEL_NONE;

	int i = 0;

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	ALARM_SIG_INFO* pAlarmSigInfo = NULL;
	HIS_ALARM_RECORD* pHisAlarmRecord = NULL;

	ASSERT(pLCDLangFile);

	for(i = 0; i < nAlarmCount; i++, pMenuItem++)
	{
		if(pMenuItem->cMenuType == MT_ACTIVE_ALARM_DISPLAY)
		{
			pAlarmSigInfo = ((ALARM_SIG_VALUE_EX_LCD*)pMenuItem->pvItemData)
				->bvAlarm.pStdSig;

			nAlarmLevel = pAlarmSigInfo->iAlarmLevel;
		}
		else if(pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY)
		{
			int nBufLen;

			pHisAlarmRecord = (HIS_ALARM_RECORD*)pMenuItem->pvItemData;

			ASSERT(pHisAlarmRecord);

			DxiGetData(VAR_A_SIGNAL_INFO_STRU,			
				pHisAlarmRecord->iEquipID,	
				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, pHisAlarmRecord->iAlarmID),
				&nBufLen,			
				&pAlarmSigInfo,			
				0);

			nAlarmLevel = pHisAlarmRecord->byAlarmLevel;
		}		

		if (nAlarmLevel == ALARM_LEVEL_OBSERVATION)
		{
			nObeserAlarmNum++;
		}
		else if (nAlarmLevel == ALARM_LEVEL_MAJOR)
		{
			nMajorAlarmNum++;
		}
		else if (nAlarmLevel == ALARM_LEVEL_CRITICAL)
		{
			nCriticalAlarmNum++;
		}
	}

	//If no alarm, quit. by HULONGWEN use nAlarmCount = 0?
	if (nObeserAlarmNum == 0
		&& nMajorAlarmNum == 0
		&& nCriticalAlarmNum == 0)
	{
		return ID_CANCEL;
	}

	if(pSubMenu->pItems[0].cMenuType == MT_ACTIVE_ALARM_DISPLAY)
	{

		pLangText = GetLCDLangText(
			ACTIVE_ALARM_DISP_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
	}
	else if(pSubMenu->pItems[0].cMenuType == MT_HISTORY_ALARM_DISPLAY)
	{
		pLangText = GetLCDLangText(
			HISTORY_ALARM_DISP_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
	}

	if(pLangText)
	{
		strncpyz(szHeadingInfo, 
			pLangText->pAbbrName[nCurrentLangFlag],
			sizeof(szPromptInfo1));

	}

	pLangText = GetLCDLangText(
		OBSERVE_ALARM_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	if(pLangText)
	{
		snprintf(szPromptInfo1, sizeof(szPromptInfo1),
			"%-11.11s:%3d",
			pLangText->pAbbrName[nCurrentLangFlag],
			nObeserAlarmNum);
	}

	pLangText = GetLCDLangText(
		MAJOR_ALARM_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	if(pLangText)
	{
		snprintf(szPromptInfo2, sizeof(szPromptInfo2),
			"%-11.11s:%3d",
			pLangText->pAbbrName[nCurrentLangFlag],
			nMajorAlarmNum);
	}

	pLangText = GetLCDLangText(
		CRITICAL_ALARM_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	if(pLangText)
	{
		snprintf(szPromptInfo3, sizeof(szPromptInfo3),
			"%-11.11s:%3d",
			pLangText->pAbbrName[nCurrentLangFlag],
			nCriticalAlarmNum);
	}

	return DoModal_AcknowlegeDlg_INFO(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		nTimeOut);

}

static int DisALarmNumPromptInfoMiniNCU(SUB_MENU* pSubMenu, int nTimeOut)
{
	char szHeadingInfo[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo1[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo2[MAX_CHARS_OF_LINE] = "";
	char szPromptInfo3[MAX_CHARS_OF_LINE] = "";

	int nObeserAlarmNum = 0;
	int nMajorAlarmNum = 0;
	int nCriticalAlarmNum = 0;

	int nAlarmCount = pSubMenu->nItems;
	MENU_ITEM* pMenuItem = pSubMenu->pItems;

	int nAlarmLevel = ALARM_LEVEL_NONE;

	int i = 0;

	LANG_TEXT*	pLangText;

	int nCurrentLangFlag = GetCurrentLangFlag();

	LANG_FILE* pLCDLangFile = GetLCDLangFile();

	ALARM_SIG_INFO* pAlarmSigInfo = NULL;
	HIS_ALARM_RECORD* pHisAlarmRecord = NULL;

	ASSERT(pLCDLangFile);

	for(i = 0; i < nAlarmCount; i++, pMenuItem++)
	{
		if(pMenuItem->cMenuType == MT_ACTIVE_ALARM_DISPLAY)
		{
			pAlarmSigInfo = ((ALARM_SIG_VALUE_EX_LCD*)pMenuItem->pvItemData)
				->bvAlarm.pStdSig;

			nAlarmLevel = pAlarmSigInfo->iAlarmLevel;
		}
		else if(pMenuItem->cMenuType == MT_HISTORY_ALARM_DISPLAY)
		{
			int nBufLen;

			pHisAlarmRecord = (HIS_ALARM_RECORD*)pMenuItem->pvItemData;

			ASSERT(pHisAlarmRecord);

			DxiGetData(VAR_A_SIGNAL_INFO_STRU,			
				pHisAlarmRecord->iEquipID,	
				DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, pHisAlarmRecord->iAlarmID),
				&nBufLen,			
				&pAlarmSigInfo,			
				0);

			nAlarmLevel = pHisAlarmRecord->byAlarmLevel;
		}		

		if (nAlarmLevel == ALARM_LEVEL_OBSERVATION)
		{
			nObeserAlarmNum++;
		}
		else if (nAlarmLevel == ALARM_LEVEL_MAJOR)
		{
			nMajorAlarmNum++;
		}
		else if (nAlarmLevel == ALARM_LEVEL_CRITICAL)
		{
			nCriticalAlarmNum++;
		}
	}

	//If no alarm, quit. by HULONGWEN use nAlarmCount = 0?
	if (nObeserAlarmNum == 0
		&& nMajorAlarmNum == 0
		&& nCriticalAlarmNum == 0)
	{
		return ID_CANCEL;
	}

	if(pSubMenu->pItems[0].cMenuType == MT_ACTIVE_ALARM_DISPLAY)
	{
		pLangText = GetLCDLangText(
			ACTIVE_ALARM_DISP_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
	}
	else if(pSubMenu->pItems[0].cMenuType == MT_HISTORY_ALARM_DISPLAY)
	{
		pLangText = GetLCDLangText(
			HISTORY_ALARM_DISP_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
	}

	if(pLangText)
	{
		strncpyz(szHeadingInfo, 
			pLangText->pAbbrName[nCurrentLangFlag],
			sizeof(szHeadingInfo));

	}


	int				iaSwitchTable[] = {1, 2, 3};
	int				iSwitchTableIndex;
	int				iCurPageID;

	//switch to next page of one alarm item
	iSwitchTableIndex = LCD_SwitchToNextIndex(g_bNeedSwithToFirstPage, FALSE, 1, ITEM_OF(iaSwitchTable));
	iCurPageID = iaSwitchTable[iSwitchTableIndex];
	g_bNeedSwithToFirstPage = FALSE;

	if(1 == iCurPageID)
	{
		pLangText = GetLCDLangText(
			OBSERVE_ALARM_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if(pLangText)
		{
			snprintf(szPromptInfo1, sizeof(szPromptInfo1),
				"%-12s:%3d",
				pLangText->pAbbrName[nCurrentLangFlag],
				nObeserAlarmNum);
		}
	}
	else if(2 == iCurPageID)
	{
		pLangText = GetLCDLangText(
			MAJOR_ALARM_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if(pLangText)
		{
			snprintf(szPromptInfo1, sizeof(szPromptInfo1),
				"%-12s:%3d",
				pLangText->pAbbrName[nCurrentLangFlag],
				nMajorAlarmNum);
		}
	}
	else
	{
		pLangText = GetLCDLangText(
			CRITICAL_ALARM_LANG_ID, 
			pLCDLangFile->iLangTextNum, 
			pLCDLangFile->pLangText);
		if(pLangText)
		{
			snprintf(szPromptInfo1, sizeof(szPromptInfo1),
				"%-12s:%3d",
				pLangText->pAbbrName[nCurrentLangFlag],
				nCriticalAlarmNum);
		}
	}


	return DoModal_AcknowlegeDlg_INFO(szHeadingInfo, 
		szPromptInfo1, 
		szPromptInfo2,
		szPromptInfo3,
		nTimeOut);

}



/*==========================================================================*
 * FUNCTION : DoModal_AlarmsScreen
 * PURPOSE  : Display alarm info on screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_DISPLAY*       pMenuDisplay : 
 *            GLOBAL_VAR_OF_LCD*  pThis        : 
 * RETURN   : int : should return ID_GOTO_RPEVIOUS
 * COMMENTS : Before return ID_GOTO_RPEVIOUS, should call pThis->cAlarmPopFlag = NO_ALARM_POP;
 * CREATOR  : HULONGWEN                DATE: 2004-12-08 10:23
 *==========================================================================*/
int DoModal_AlarmsScreen(MENU_DISPLAY* pMenuDisplay, 
						 GLOBAL_VAR_OF_LCD* pThis, 
						 int nMenuType)
{
	UCHAR byKeyValue;

	SUB_MENU  **ppSubMenu;

	BOOL bRefreshAtOnce = FALSE;

	int nIDAlarmPrompt; 
	int nAlarmPageID;


	UNUSED(pMenuDisplay);

	if (nMenuType == MT_ACTIVE_ALARM_DISPLAY)
	{
		ASSERT(g_pActiveAlarmMenuItem->pvItemData);

		ppSubMenu = (SUB_MENU**)(&((g_pActiveAlarmMenuItem->pvItemData)));

	}
	else if (nMenuType == MT_HISTORY_ALARM_DISPLAY)
	{
		ASSERT(g_pHistoryAlarmMenuItem->pvItemData);

		ppSubMenu = (SUB_MENU**)(&((g_pHistoryAlarmMenuItem->pvItemData)));
	}


	ClearScreen();


	pThis->cAlarmPopFlag |= ALARM_DIS_STATUS;

	//begin by hulw 061219 In alarm screen, LED flash for rectifier alarm. 
	pThis->cDisAlarmNumFlag = YES_DIS_ALARM_NUM;
	
	
	g_bNeedSwithToFirstPage = TRUE;

	//Because the alarm number maybe refresh,adopt loop mode
	//Display alarm number prompt info first
	for(;;)
	{
		if ((*ppSubMenu) == NULL
			|| (*ppSubMenu)->nItems == 0)
		{
			pThis->cAlarmPopFlag = NO_ALARM_POP;

			pThis->cDisAlarmNumFlag = NO_DIS_ALARM_NUM;

			return ID_GOTO_RPEVIOUS;
		}

		nIDAlarmPrompt = 
			//Frank Wu,20160127, for MiniNCU
			//DisALarmNumPromptInfo(*ppSubMenu, ALARM_PROMPT_TIMEOUT);
			DisALarmNumPromptInfoMiniNCU(*ppSubMenu, ALARM_PROMPT_TIMEOUT);

		if (nIDAlarmPrompt == ID_RETURN)
		{
			pThis->cDisAlarmNumFlag = NO_DIS_ALARM_NUM;

			break;//go on
		}
		else if (nIDAlarmPrompt == ID_CANCEL)
		{
			pThis->cAlarmPopFlag = NO_ALARM_POP;

			pThis->cDisAlarmNumFlag = NO_DIS_ALARM_NUM;

			return ID_GOTO_RPEVIOUS;
		}
		else if (nIDAlarmPrompt == ID_GOTO_DEF_SCREEN
			&& (pThis->cAlarmPopFlag & ALARM_POP_STATUS) == 0)
		{
			pThis->cAlarmPopFlag = NO_ALARM_POP;

			pThis->cDisAlarmNumFlag = NO_DIS_ALARM_NUM;

			return ID_GOTO_RPEVIOUS;
		}
		/*else if (nIDAlarmPrompt == ID_TIMEOUT_RETURN)
		{
			continue;
		}*/
		
	}

	//end by hulw

	//Because the alarm info maybe modified in HandleIdle() 
	if ((*ppSubMenu) == NULL
		|| (*ppSubMenu)->nItems == 0)
	{
		pThis->cAlarmPopFlag = NO_ALARM_POP;

		return ID_GOTO_RPEVIOUS;
	}

	ClearScreen();

	g_bNeedSwithToFirstPage = TRUE;
	g_bNeedSwithToNextPage = FALSE;

	DisplayAlarms(*ppSubMenu);


	for(;;)
	{
		byKeyValue = GetKeyEx();

		if( byKeyValue != VK_NO_KEY )
		{
			KeyPressForeOperation();
			

			switch( byKeyValue )
			{
			case VK_ESCAPE:  
				{
					pThis->cAlarmPopFlag = NO_ALARM_POP;

					return ID_GOTO_RPEVIOUS;

				}

				break;

			case VK_UP:  
			case VK_DOWN:
				{
					if( (*ppSubMenu)->nItems > 1 )
					{
						(*ppSubMenu)->nCur = (byKeyValue == VK_UP)
							? PrevSubMenuItem( *ppSubMenu, 1)
							: NextSubMenuItem( *ppSubMenu ,1);
					}		

					g_bNeedSwithToFirstPage = TRUE;
				}

				break;

			case VK_ENTER:   
				{
					g_bNeedSwithToNextPage = TRUE;
				}

				break;
			case VK_ENTER_UP:
				{
					DisNavigationInfo(pMenuDisplay);
					
					g_bNeedSwithToFirstPage = TRUE;
				}
				break;
			default:
				break;
			}
			
			bRefreshAtOnce = TRUE;
		}
		else
		{
			int nRetHandleIdle = HandleIdle();

			if(nRetHandleIdle == ID_GOTO_DEF_SCREEN)
			{
				pThis->cAlarmPopFlag = NO_ALARM_POP;

				return ID_GOTO_RPEVIOUS;
			}
			else if(nRetHandleIdle == ID_INIT_LCD_JUST_NOW)
			{
				bRefreshAtOnce = TRUE;
			}

			//Because the alarm info maybe modified in HandleIdle() 
			if ((*ppSubMenu) == NULL
				|| (*ppSubMenu)->nItems == 0)
			{
				pThis->cAlarmPopFlag = NO_ALARM_POP;

				return ID_GOTO_RPEVIOUS;
			}
		}

		ASSERT(pThis);

		if(pThis->cDataRefreshFlag == NEED_REFRESH_DATA || bRefreshAtOnce)
		{
			DisplayAlarms(*ppSubMenu);

			bRefreshAtOnce = FALSE;

			pThis->cDataRefreshFlag = NEED_NOT_REFRESH_DATA;
		}

	}

}

static BOOL LCD_GetRectHighValid(IN int iEquipID)
{
	SIG_BASIC_VALUE*		pSigValue;
	int iBufLen;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECT_HI_SN),
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (iError == ERR_DXI_OK)
	{
		 return SIG_VALUE_IS_CONFIGURED(pSigValue);
	}
	return FALSE;
 
}
static DWORD  LCD_GetRectHighSNValue(IN int iEquipID)
{
	SIG_BASIC_VALUE*		pSigValue = NULL;
	int iBufLen;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECT_HI_SN),
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (pSigValue && (iError == ERR_DXI_OK))
	{
#ifdef _DEBUG
		if(pSigValue->varValue.ulValue == 0)
		{
			pSigValue->varValue.ulValue = 2;
		}
#endif
		return pSigValue->varValue.ulValue;

	}
	return 1;
}

static char *LCD_GetEquipNameFromInterface(IN int iEquipID,IN int iLanguage,IN int iPosition)
{

	char	*szEquipName = NULL;
	EQUIP_INFO		*pEquipInfo = NULL;
	int nError, nBufLen, iPos;

	nError = DxiGetData(VAR_A_EQUIP_INFO,			
		iEquipID,	
		0,
		&nBufLen,			
		&pEquipInfo,			
		0);

	szEquipName = NEW(char, 64);
	if(szEquipName == NULL)
	{
		return NULL;
	}
	memset(szEquipName, 0, 64);

	
	if(nError == ERR_DXI_OK)
	{		
		if(pEquipInfo->iEquipTypeID == 201 || pEquipInfo->iEquipTypeID == 1101)
		{

			strncpyz(szEquipName, pEquipInfo->pEquipName->pAbbrName[iLanguage], 64);
			iPos = LCD_GetEquipName(pEquipInfo->pEquipName->pAbbrName[iLanguage]);
			snprintf(szEquipName + iPos, 5, "#%d", iPosition);
		}				
		else
		{
			strncpyz(szEquipName, pEquipInfo->pEquipName->pAbbrName[iLanguage], 64);
		}


		return szEquipName;
		
	}
	else
	{
		SAFELY_DELETE(szEquipName);
		return NULL;
	}

	return NULL;

}
static int LCD_GetEquipName(IN OUT char *szEquipName)
{
#define IS_NUMBER(c)								(((c)>=(0x30) && (c)<=(0x39)) || (c) == 0x23)
	char *p=szEquipName;
	/*char pfinal[30]="";*/
	int i=0;
	while(*p)
	{
		if(IS_NUMBER(*p)) 
		{
			p=p++;
			break;
		}
		else
		{
			//pfinal[i]=*p;
			p++;
			i++;
		}
	}
	//p=strdup(pfinal);
	return i;
}
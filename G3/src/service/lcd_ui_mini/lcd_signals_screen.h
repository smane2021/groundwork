/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_signals_screen.h
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:47
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __LCD_SIGNALS_SCREEN_H__041008
#define __LCD_SIGNALS_SCREEN_H__041008

#ifdef  __cplusplus
extern "C" {
#endif


//Used to edit password string and other strings
static const char s_pCharacterSet[] = 
	" 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
#define CHAR_OF_CHARACTER_SET (sizeof(s_pCharacterSet) - 1)

#define A_SIGNAL_OCCUPIED_LINES	2

#define CHAR_HEIGHT     (BASE_HEIGHT * 2)
#define SIGNAL_HEIGHT	(CHAR_HEIGHT * A_SIGNAL_OCCUPIED_LINES)
#define SIGNAL_NUM_PER_SCREEN	(SCREEN_HEIGHT / SIGNAL_HEIGHT)



#ifdef __cplusplus
}
#endif

#endif //__LCD_SIGNALS_SCREEN_H__041008

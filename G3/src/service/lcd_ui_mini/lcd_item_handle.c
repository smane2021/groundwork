   /*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_item_handle.c
*  CREATOR  : HULONGWEN                DATE: 2004-11-19 11:17
*  VERSION  : V1.00
*  PURPOSE  : Get or set the data of all the menu items
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  
#include "lcd_public.h"
#include "lcd_item_handle.h"
#include "../ydn23/ydn.h"
#include "../eem_soc/esr.h"
#include "lcd_main.h"


static UINT g_sMonthLimit[12] = 
{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

//If it is a leap year
#define    IS_LEAP_YEAR(iYear) ((!((iYear)%4)&&((iYear)%100)) || !((iYear)%400))

#define TM_YEAR_START	1900

#define NEED_TO_GET_ORIGIN_DATA(nGetType, cMenuStatus)  \
	((nGetType) == GET_VALUE_FROM_SYSTEM)

//Siganl set time out
#define SET_TIME_OUT	12000 //6s

static char g_szCurUser[64] = "";

#define ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo) \
	((pDisValuePreInfo)->iSigValueType = (pSelfDefineInfo)->iSigValueType, \
	(pDisValuePreInfo)->fMinValidValue = (pSelfDefineInfo)->fMinValidValue, \
	(pDisValuePreInfo)->fMaxValidValue = (pSelfDefineInfo)->fMaxValidValue, \
	(pDisValuePreInfo)->iStateNum = (pSelfDefineInfo)->iStateNum, \
	(pDisValuePreInfo)->pStateText = (pSelfDefineInfo)->pStateText, \
	(pDisValuePreInfo)->szValueDisplayFmt = (pSelfDefineInfo)->szValueDisplayFmt, \
	(pDisValuePreInfo)->szSigUnit = (pSelfDefineInfo)->szSigUnit, \
	(pDisValuePreInfo)->iSectionNum = (pSelfDefineInfo)->iStateNum, \
	(pDisValuePreInfo)->pSectionDataStru = (pSelfDefineInfo)->pSectionDataStru, \
	(pDisValuePreInfo)->pString = (pSelfDefineInfo)->szString) \



extern int DoModal_AcknowlegeDlg_INFO(char* pHeadingInfo, 
									  char* pPromptInfo1, 
									  char* nPromptInfo2,
									  char* nPromptInfo3,
									  int	nTimeout);


extern int DoModal_AcknowlegeDlg_ID(int nHeadingInfoID, 
							 int nPromptInfoID1, 
							 int nPromptInfoID2,
							 int nPromptInfoID3,
							 int nTimeout);




static int GetYdnPriCfg(OUT YDN_COMMON_CONFIG* pYdnPriCfg);
static int SetYdnPriCfg(IN YDN_COMMON_CONFIG* pYdnPriCfg, IN int iModifyType);
static SIG_ENUM GetEnumByPortParm(const char* pPortParam);

static DWORD  LCD_GetRectHighSNValue(IN int iEquipID);
static BOOL LCD_GetRectHighValid(IN int iEquipID);






static int GetUserPreInfo(MENU_ITEM* pMenuItem, 
						  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						  int nGetType);
static int SetUserInfo(MENU_ITEM* pMenuItem);


static int GetPasswordPreInfo(MENU_ITEM* pMenuItem, 
							  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							  int nGetType);
static int SetPasswordInfo(MENU_ITEM* pMenuItem);


static int GetSysLanguageInfo(MENU_ITEM* pMenuItem, 
							  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							  int nGetType);
static int SetSysLanguageInfo(MENU_ITEM* pMenuItem);


static int GetTimeZonePreInfo(MENU_ITEM* pMenuItem, 
							  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							  int nGetType);
static int SetTimeZoneItem(MENU_ITEM* pMenuItem);


static int GetSystemDate(MENU_ITEM* pMenuItem, 
							 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							 int nGetType);
static int SetSystemDate(MENU_ITEM* pMenuItem);


static int GetSystemTimeInfo(MENU_ITEM* pMenuItem, 
							 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							 int nGetType);
static int SetSystemTimeInfo(MENU_ITEM* pMenuItem);


static int ReloadDefConfigGetPreInfo(MENU_ITEM* pMenuItem, 
							 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							 int nGetType);
static int ReloadDefConfigSetFunc(MENU_ITEM* pMenuItem);


static int GetKeypadVoiceInfo(MENU_ITEM* pMenuItem, 
							  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							  int nGetType);
static int SetKeypadVoiceInfo(MENU_ITEM* pMenuItem);


static int GetClearHisAlarmItemPreInfo(MENU_ITEM* pMenuItem, 
									   DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									   int nGetType);
static int SetClearHisAlarmItem(MENU_ITEM* pMenuItem);


static int GetOutgoingAlarmItemPreInfo(MENU_ITEM* pMenuItem, 
									   DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									   int nGetType);
static int SetOutgoingAlarmItem(MENU_ITEM* pMenuItem);


static int GetAlarmVoiceCtrlPreInfo(MENU_ITEM* pMenuItem, 
									DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									int nGetType);
static int SetAlarmVoiceItem(MENU_ITEM* pMenuItem);


static int DownloadConfigGetPreInfo(MENU_ITEM* pMenuItem, 
									DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									int nGetType);
static int DownloadConfigSetFunc(MENU_ITEM* pMenuItem);


static int AutoConfigGetPreInfo(MENU_ITEM* pMenuItem, 
									DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									int nGetType);
static int AutoConfigSetFunc(MENU_ITEM* pMenuItem);


static int ChangeHeightGetPreInfo(MENU_ITEM* pMenuItem, 
								  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
								  int nGetType);
static int ChangeHeightSetFunc(MENU_ITEM* pMenuItem);


static int SetLcdRotationGetPreInfo(MENU_ITEM* pMenuItem, 
									DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									int nGetType);
static int SetLcdRotationSetFunc(MENU_ITEM* pMenuItem);



//////////////////////////////////////////////////////////////////////////

static int DHCPGetPreInfo(MENU_ITEM* pMenuItem, 
						  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						  int nGetType);
static int DHCPSetFunc(MENU_ITEM* pMenuItem);


static int GetSysIpAddress(MENU_ITEM* pMenuItem, 
						   DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						   int nGetType);
static int SetSysIpAddress(MENU_ITEM* pMenuItem);


static int GetSysNetmask(MENU_ITEM* pMenuItem, 
						 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						 int nGetType);
static int SetSysNetmask(MENU_ITEM* pMenuItem);


static int GetSysDefGateway(MENU_ITEM* pMenuItem, 
							DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							int nGetType);
static int SetSysDefGateway(MENU_ITEM* pMenuItem);




static int GetSiteIDPreInfo(MENU_ITEM* pMenuItem, 
							DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							int nGetType);
static int SetSiteIDInfo(MENU_ITEM* pMenuItem);


static int GetYDN23SlefAddr(MENU_ITEM* pMenuItem,
							DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							int nGetType);
static int SetYDN23SlefAddr(MENU_ITEM* pMenuItem);


static int GetYDN23PortType(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType);
static int SetYDN23PortType(MENU_ITEM* pMenuItem);


static int GetYDN23PortParam(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType);
static int SetYDN23PortParam(MENU_ITEM* pMenuItem);


static int GetYDN23CallBackEnb(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType);
static int SetYDN23CallBackEnb(MENU_ITEM* pMenuItem);


static int GetYDN23CallBackNum(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType);
static int SetYDN23CallBackNum(MENU_ITEM* pMenuItem);


static int GetYDN23CallBackInterval(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType);
static int SetYDN23CallBackInterval(MENU_ITEM* pMenuItem);


static int GetYDN23Phone1(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType);
static int SetYDN23Phone1(MENU_ITEM* pMenuItem);


static int GetYDN23Phone2(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType);
static int SetYDN23Phone2(MENU_ITEM* pMenuItem);


static int GetYDN23Phone3(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType);
static int SetYDN23Phone3(MENU_ITEM* pMenuItem);



//static int GetSigValForFastSet(MENU_ITEM* pMenuItem, 
//					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
//					  int nGetType);
//static int SetSigValForFastSet(MENU_ITEM* pMenuItem);


static int SigTimeSetGetPreInfo(MENU_ITEM* pMenuItem, 
								DIS_VALUE_PRE_INFO* pDisValuePreInfo,
								int nGetType);
static int SigTimeSetSetFunc(MENU_ITEM* pMenuItem);



YDN_COMMON_CONFIG g_YdnPriCfg;



ITEM_HANDLE_FUNC	g_aItemHandleFunc[] = 
	{
		{	PASSWORD_DLG_USER_ID,		GetUserPreInfo,				SetUserInfo},
		{	PASSWORD_DLG_PWD_ID,		GetPasswordPreInfo,			SetPasswordInfo},

		{	SYS_LANGUAGE_SET_ID,		GetSysLanguageInfo,			SetSysLanguageInfo},
		{	SYS_DATE_SET_ID,			GetSystemDate,				SetSystemDate},
		{	SYS_TIME_SET_ID,			GetSystemTimeInfo,			SetSystemTimeInfo},
		{	SYS_IP_ADDR_SET_ID,			GetSysIpAddress,			SetSysIpAddress},
		{	SYS_NETMASK_SET_ID,			GetSysNetmask,				SetSysNetmask},
		{	SYS_GATEWAY_SET_ID,			GetSysDefGateway,			SetSysDefGateway},

		{	CLEAR_HIS_ALARM_CTRL_ID,	GetClearHisAlarmItemPreInfo,SetClearHisAlarmItem},
		{	OUTGOING_ALARM_CTRL_ID,		GetOutgoingAlarmItemPreInfo,SetOutgoingAlarmItem},
		{	ALARM_VOICE_CTRL_ID,		GetAlarmVoiceCtrlPreInfo,	SetAlarmVoiceItem},

		{	SYS_TIME_ZONE_SET_ID,		GetTimeZonePreInfo,			SetTimeZoneItem},
		{	RELOAD_DEF_CONFIG_SET_ID,	ReloadDefConfigGetPreInfo,	ReloadDefConfigSetFunc},
		{	KEYPAD_VOICE_SET_ID,		GetKeypadVoiceInfo,			SetKeypadVoiceInfo},
		{	DOWNLOAD_CONFIG_LANG_ID,	DownloadConfigGetPreInfo,	DownloadConfigSetFunc},
		{	AUTO_CONFIG_LANG_ID,		AutoConfigGetPreInfo,		AutoConfigSetFunc},

		{	CHANGE_LCD_HEIGHT_SET_ID,	ChangeHeightGetPreInfo,		ChangeHeightSetFunc},
		{	SET_LCD_ROTATION_SET_ID,	SetLcdRotationGetPreInfo,	SetLcdRotationSetFunc},

		{	DHCP_SET_LANG_ID,			DHCPGetPreInfo,				DHCPSetFunc},
		//{	SYS_SITE_ID_SET_ID,			GetSiteIDPreInfo,			SetSiteIDInfo},
		{	 SYS_SITE_ID_SET_ID,		GetYDN23SlefAddr,			SetYDN23SlefAddr},
		{	YDN_PORT_MODE_SET_ID,		GetYDN23PortType,			SetYDN23PortType},
		{	YDN_PORT_PARAM_SET_ID,		GetYDN23PortParam,			SetYDN23PortParam},
		{	YDN_CALL_BACK_ENB_SET_ID,	GetYDN23CallBackEnb,		SetYDN23CallBackEnb},
		{	YDN_CALL_BACK_NUM_SET_ID,	GetYDN23CallBackNum,		SetYDN23CallBackNum},
		{	YDN_CALL_BACK_INTERVAL_SET_ID,GetYDN23CallBackInterval,	SetYDN23CallBackInterval},
		{	YDN_PHONE_NO1_SET_ID,		GetYDN23Phone1,				SetYDN23Phone1},
		{	YDN_PHONE_NO2_SET_ID,		GetYDN23Phone2,				SetYDN23Phone2},
		{	YDN_PHONE_NO3_SET_ID,		GetYDN23Phone3,				SetYDN23Phone3},

		{	SIG_TIME_TYPE_SET_ID,		SigTimeSetGetPreInfo,		SigTimeSetSetFunc},		//���ڼƻ����Ե�ʱ������
	};

const int ITEM_HANDLE_FUNC_NUM = (sizeof(g_aItemHandleFunc)/sizeof(ITEM_HANDLE_FUNC));


/*==========================================================================*
* FUNCTION : GetSignalPreInfo
* PURPOSE  : Get a signal info before it will be displayed
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 16:02
*==========================================================================*/
int GetSignalPreInfo(MENU_ITEM* pMenuItem, 
					 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					 int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	int					nRetDataFlag = RET_VALID_DATA;

	SIG_BASIC_VALUE*	pSigValue = NULL;

	int nBufLen;
	int nEquipId, nSignalType, nSignalID;

	void*				pDisSigInfo;
	pDisSigInfo = pMenuItem->pvItemData;
	ASSERT(pDisSigInfo);

	DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID, 
		nEquipId, 
		nSignalType, 
		nSignalID);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		if (DxiGetData(VAR_A_SIGNAL_VALUE,
			nEquipId,			
			DXI_MERGE_SIG_ID(nSignalType, nSignalID),
			&nBufLen,			
			&pSigValue,			
			0) == ERR_DXI_OK)
		{
			ASSERT(pSigValue);	

			if(!SIG_VALUE_IS_VALID(pSigValue) || !SIG_VALUE_IS_CONFIGURED(pSigValue))
			{
				//if ret invalid data, should reverse display
				nRetDataFlag = RET_INVALID_DATA;
			}


			memcpy(&(pMenuItem->vdData),
				&(pSigValue->varValue),
				sizeof(VAR_VALUE));
		}
		else
		{
			nRetDataFlag = RET_INVALID_DATA;
		}		
	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));


	switch (nSignalType)
	{
	case SIG_TYPE_SAMPLING:
		{
			SAMPLE_SIG_INFO* pSigInfo = (SAMPLE_SIG_INFO*)pDisSigInfo;

			pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;

			pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

			pDisValuePreInfo->iStateNum = pSigInfo->iStateNum;
			pDisValuePreInfo->pStateText = pSigInfo->pStateText;

			pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

			pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
			pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;
		}
		break;

	case SIG_TYPE_CONTROL:	
		{
			CTRL_SIG_INFO* pSigInfo = (CTRL_SIG_INFO*)pDisSigInfo;

			pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;

			pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

			pDisValuePreInfo->iStateNum = pSigInfo->iStateNum;
			pDisValuePreInfo->pStateText = pSigInfo->pStateText;

			pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

			pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
			pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;
		}
		break;

	case SIG_TYPE_SETTING:		
		{
			SET_SIG_INFO* pSigInfo = (SET_SIG_INFO*)pDisSigInfo;

			pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;

			pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

			pDisValuePreInfo->iStateNum = pSigInfo->iStateNum;
			pDisValuePreInfo->pStateText = pSigInfo->pStateText;

			pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

			pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
			pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;
		}
		break;

	case SIG_TYPE_ALARM:		
		{			

		}
		break;

	default:
		{
		}
		break;

	}

	//Enum����Ҫ�������⴦�����ڵ�ֵ��Enum��������ȡ���Ĳ���0������1
	//���������������Ҫ��������
	if((pDisValuePreInfo->iSigValueType == VAR_ENUM)
		&& (pDisValuePreInfo->iStateNum == 1))
	{
//		ASSERT(pDisValuePreInfo->vdData.enumValue == 1);	//��һ���������ź�û�д���
		pDisValuePreInfo->vdData.enumValue = 0;
	}

	//The special handle of some signals 
	//
	// The signal of rect S/N
	if (pSigValue != NULL
		&& (((SAMPLE_SIG_VALUE *)pSigValue)->pStdSig->iSigID == 
		DXI_SPLIT_SIG_MID_ID(SIG_ID_RECT_SN)) &&
		(pSigValue->ucSigType == DXI_SPLIT_SIG_MID_TYPE(SIG_ID_RECT_SN)))
	{
		EQUIP_INFO* pEquipInfo = NULL;

		if (DxiGetData(VAR_A_EQUIP_INFO,			
			nEquipId,	
			0,
			&nBufLen,			
			&pEquipInfo,			
			0) == ERR_DXI_OK)
		{
			if ((pEquipInfo->iEquipTypeID == RECT_STD_EQUIP_ID)
				|| (pEquipInfo->iEquipTypeID == RECT_STD_EQUIP_ID_SLAVE1)
				|| (pEquipInfo->iEquipTypeID == RECT_STD_EQUIP_ID_SLAVE2)
				|| (pEquipInfo->iEquipTypeID == RECT_STD_EQUIP_ID_SLAVE3))

			{
				static char s_szRectSN[20];

				pDisValuePreInfo->szSigUnit = NULL;

				pDisValuePreInfo->iSigValueType = VAR_STRING;
				if(LCD_GetRectHighValid(nEquipId))
				{
					snprintf(s_szRectSN, sizeof(s_szRectSN), "%02u%02d%02d%05d", 
						(unsigned int)(LCD_GetRectHighSNValue(nEquipId)),
						((pDisValuePreInfo->vdData.ulValue) >> 24) & 0x1F,
						((pDisValuePreInfo->vdData.ulValue) >> 16) & 0x0F,
						LOWORD((DWORD)(pDisValuePreInfo->vdData.ulValue)));

					pDisValuePreInfo->pString = s_szRectSN;

					pDisValuePreInfo->szValueDisplayFmt = "11";
				}
				else
				{
					snprintf(s_szRectSN, sizeof(s_szRectSN), "2%02d%01X%06d", 
						((pDisValuePreInfo->vdData.ulValue) >> 24) & 0x1F,
						((pDisValuePreInfo->vdData.ulValue) >> 16) & 0x0F,
						LOWORD((DWORD)(pDisValuePreInfo->vdData.ulValue)));

					pDisValuePreInfo->pString = s_szRectSN;

					pDisValuePreInfo->szValueDisplayFmt = "10";
				}
			}
		}

	}

	//The set signals of datetime set
	if (pDisValuePreInfo->iSigValueType == VAR_DATE_TIME 
		&& nSignalType == SIG_TYPE_SETTING)
	{

#define MAX_SUPPORT_TIME_SET	36

		static SECTION_DATA_STRU	s_A_SectionDataStru_TimeSet[] =
		{
			DEF_SECTION_DATA_STRU(01, 01, 12, 1, "02", '-'),
				DEF_SECTION_DATA_STRU(01, 01, 31, 1, "02", ' '),
#ifdef TIME_SET_INCLUDE_MIN
				DEF_SECTION_DATA_STRU(00, 00, 23, 1, "02", ':'),
				DEF_SECTION_DATA_STRU(00, 00, 59, 1, "02", '\0'),
#else
				DEF_SECTION_DATA_STRU(00, 00, 23, 1, "02", '\0'),
#endif//TIME_SET_INCLUDE_MIN
		};

#define TIME_SET_SECTION_NUM sizeof(s_A_SectionDataStru_TimeSet)/sizeof(SECTION_DATA_STRU)

		static SECTION_DATA_STRU s_SectionDataStru_TimeSet[MAX_SUPPORT_TIME_SET * TIME_SET_SECTION_NUM];

		static SELF_DEFINE_INFO s_SigTimeSetItemInfo[MAX_SUPPORT_TIME_SET];

		static int s_nSigTimeSetNum = 0;

		SELF_DEFINE_INFO*	pSigTimeSetItemInfo = NULL;
		SECTION_DATA_STRU*	pSectionDataStru	= NULL;

		time_t tSetTime;
		struct tm		tmSettime;

		if (s_nSigTimeSetNum >= MAX_SUPPORT_TIME_SET)
		{
			return nRetDataFlag;
		}

		pSectionDataStru = s_SectionDataStru_TimeSet + TIME_SET_SECTION_NUM * s_nSigTimeSetNum;

		memcpy(pSectionDataStru, s_A_SectionDataStru_TimeSet, 
			sizeof(s_A_SectionDataStru_TimeSet));

		pSigTimeSetItemInfo = s_SigTimeSetItemInfo + s_nSigTimeSetNum;

		s_nSigTimeSetNum++;

#define SIG_TIME_SET_ID_DIS_FORMAT	"11"
		INIT_SELF_DEFINE_INFO(pSigTimeSetItemInfo,
			SIG_TIME_TYPE_SET_ID,
			((SET_SIG_INFO*)pDisSigInfo)->pSigName,
			VAR_MULTI_SECT, 
			((SET_SIG_INFO*)pDisSigInfo)->byAuthorityLevel, 
			SIG_TIME_SET_ID_DIS_FORMAT, 
			1, 
			TIME_SET_SECTION_NUM - 1,
			0,
			TIME_SET_SECTION_NUM,
			NULL,
			pSectionDataStru);

		pMenuItem->cMenuType = MT_SELF_DEF_SET;
		pMenuItem->pvItemData = pSigTimeSetItemInfo;

		pDisValuePreInfo->iSigValueType = pSigTimeSetItemInfo->iSigValueType;

		pDisValuePreInfo->szValueDisplayFmt = 
			pSigTimeSetItemInfo->szValueDisplayFmt;

		pDisValuePreInfo->iStateNum = pSigTimeSetItemInfo->iStateNum;


		pDisValuePreInfo->pSectionDataStru = 
			pSigTimeSetItemInfo->pSectionDataStru;

		pDisValuePreInfo->szValueDisplayFmt = SIG_TIME_SET_ID_DIS_FORMAT;

		tSetTime = (time_t)(pMenuItem->vdData.ulValue);

		ConvertTime(&tSetTime, TRUE);

		gmtime_r(&tSetTime, &tmSettime);

		(pSectionDataStru + 0)->uiValue	= tmSettime.tm_mon + 1;
		(pSectionDataStru + 1)->uiValue	= tmSettime.tm_mday;

		(pSectionDataStru + 2)->uiValue	= tmSettime.tm_hour;

#ifdef TIME_SET_INCLUDE_MIN
		(pSectionDataStru + 3)->uiValue	= tmSettime.tm_min;
#endif//TIME_SET_INCLUDE_MIN


		(pSectionDataStru + 1)->uiMaxValue = 
			g_sMonthLimit[(pSectionDataStru + 0)->uiValue - 1];

	}

	return nRetDataFlag;
}



int SetSignalValueInterface(int nEquipID, 
							int nSigType, 
							int nSigID,
							VAR_VALUE	vdData,
							int	nSenderType,
							int nSendDirectly,
							int	nTimeout)
{
	int nRet;

	if( LUI_IsNeedRebootForSignal(nEquipID, nSigType, nSigID) )
	{
		nRet = DoModal_AcknowlegeDlg_ID(
			HEADING_PROMPT_INFO_LANG_ID, 
			REBOOT_TO_VALIDATE_LANG_ID,
			-1,
			-1,
			PROMPT_INFO_TIMEOUT);
		if(ID_RETURN != nRet)
		{
			return ERR_LCD_OK;
		}
	}


	VAR_VALUE_EX	varValueEx;

	memset(&varValueEx, 0, sizeof(varValueEx));

	varValueEx.nSendDirectly = nSendDirectly;

	if (nSenderType == SERVICE_OF_LOGIC_CONTROL)
	{
		varValueEx.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		varValueEx.pszSenderName = "";
	}
	else
	{
		varValueEx.nSenderType = SERVICE_OF_USER_INTERFACE;
		varValueEx.pszSenderName = g_szCurUser;
	}

	memcpy(&(varValueEx.varValue), &(vdData), sizeof(VAR_VALUE));

	//////////////////////////////////////////////////////////////////////////
	//����Enum������һ�����⴦��
	//��Enum�ǵ�ֵ����ʱ��ȡֵ����0������1
	int nBufLen;

	VOID			*pSigInfo;
	//	SAMPLE_SIG_INFO	*pSamSigInfo;
	CTRL_SIG_INFO	*pCtrlSigInfo;
	SET_SIG_INFO	*pSetSigInfo;
	//	ALARM_SIG_INFO	*pAlmSigInfo;


	if(DxiGetData(VAR_A_SIGNAL_INFO_STRU,
		nEquipID,			
		DXI_MERGE_SIG_ID(nSigType, nSigID),		
		&nBufLen,			
		&pSigInfo,			
		2000) == ERR_DXI_OK)
	{
		switch(nSigType)
		{
		case SIG_TYPE_SAMPLING:
			//Nothing
			break;

		case SIG_TYPE_CONTROL:
			pCtrlSigInfo = (CTRL_SIG_INFO*)pSigInfo;
			if((pCtrlSigInfo->iSigValueType == VAR_ENUM)
				&& (pCtrlSigInfo->iStateNum == 1)
				&& (varValueEx.varValue.enumValue == 0))
			{
				varValueEx.varValue.enumValue = 1;
			}

			break;

		case SIG_TYPE_SETTING:
			pSetSigInfo = (SET_SIG_INFO*)pSigInfo;
			if((pSetSigInfo->iSigValueType == VAR_ENUM)
				&& (pSetSigInfo->iStateNum == 1)
				&& (varValueEx.varValue.enumValue == 0))
			{
				varValueEx.varValue.enumValue = 1;
			}

			break;
		case SIG_TYPE_ALARM:
			//Nothing
			break;

		default:
			break;
		}

	}

	//////////////////////////////////////////////////////////////////////////

	nRet = DxiSetData(VAR_A_SIGNAL_VALUE,
		nEquipID,			
		DXI_MERGE_SIG_ID(nSigType, nSigID),		
		sizeof(VAR_VALUE_EX),			
		&(varValueEx),			
		nTimeout);
	//printf("nEquipID=%d, nSigType=%d, nSigID=%d, nRet=%x, lValue=%d, fvalue=%f\n", nEquipID, nSigType, nSigID, nRet,
	//varValueEx.varValue.lValue, varValueEx.varValue.fValue);
	if(nRet == ERR_DXI_OK)
	{
		if( LUI_IsNeedRebootForSignal(nEquipID, nSigType, nSigID) )
		{
			PromptSysIsRebooting();
			_SYSTEM("reboot");
			Sleep(60*60*1000);
		}
	}

	return nRet;
}

int SetSignalInfo(MENU_ITEM* pMenuItem)
{
	int nEquipID, nSigType, nSigID;

	ASSERT(pMenuItem);


	DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID, nEquipID, nSigType, nSigID);

	return SetSignalValueInterface(nEquipID, nSigType, nSigID, pMenuItem->vdData, 
		SERVICE_OF_USER_INTERFACE, 
		EQUIP_CTRL_SEND_URGENTLY,
		SET_TIME_OUT);

}


/*==========================================================================*
* FUNCTION : SetAlarmLevelGetPreInfo
* PURPOSE  : Get alarm level info before it will be displayed
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 16:02
*==========================================================================*/
int SetAlarmLevelGetPreInfo(MENU_ITEM* pMenuItem, 
							DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	LANG_FILE* pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	//For alarm level set
	static LANG_TEXT*		s_AlarmLevelSetTextArray[ALARM_LEVEL_MAX];

	//No alarm 
	s_AlarmLevelSetTextArray[0] = GetLCDLangText(
		NO_ALARM_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//Observation alarm 
	s_AlarmLevelSetTextArray[1] = GetLCDLangText(
		OBSERVE_ALARM_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//Major alarm
	s_AlarmLevelSetTextArray[2] = GetLCDLangText(
		MAJOR_ALARM_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//Critical alarm
	s_AlarmLevelSetTextArray[3] = GetLCDLangText(
		CRITICAL_ALARM_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//Special handle
	pDisValuePreInfo->iSigValueType = VAR_ENUM;
	pDisValuePreInfo->iStateNum = ALARM_LEVEL_MAX;
	pDisValuePreInfo->pStateText = s_AlarmLevelSetTextArray;
	pDisValuePreInfo->szValueDisplayFmt = "2";

	ALARM_SIG_INFO*	pAlarmSigInfo;
	pAlarmSigInfo = (ALARM_SIG_INFO*)(pMenuItem->pvItemData);
	ASSERT(pAlarmSigInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		pMenuItem->vdData.enumValue = 
			pAlarmSigInfo->iAlarmLevel;
	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));

	return RET_VALID_DATA;
}

/*==========================================================================*
 * FUNCTION : SetAlarmLevelSetFunc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_ITEM*  pMenuItem : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-09-25 14:43
 *==========================================================================*/
int SetAlarmLevelSetFunc(MENU_ITEM* pMenuItem)
{
	int nEquipTypeID, nSigType, nSigID;

	SET_A_SIGNAL_INFO_STU stSetASignalInfo;

	ASSERT(pMenuItem && pMenuItem->pvItemData);

	DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID,
		nEquipTypeID,
		nSigType, 
		nSigID);

	stSetASignalInfo.byModifyType = MODIFY_ALARM_LEVEL;
	stSetASignalInfo.bModifyAlarmLevel = pMenuItem->vdData.dtValue;
	memcpy(stSetASignalInfo.cModifyUser, g_szCurUser, 1 + strlen(g_szCurUser));

	return DxiSetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
		nEquipTypeID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, nSigID),		
		sizeof(SET_A_SIGNAL_INFO_STU),			
		&stSetASignalInfo,			
		0);
}


/*==========================================================================*
* FUNCTION : SetAlarmRelayGetPreInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2006-10-29 14:27
*==========================================================================*/
int SetAlarmRelayGetPreInfo(MENU_ITEM* pMenuItem, 
							DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	LANG_FILE*			pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	//For alarm level set
	static LANG_TEXT*	s_AlarmRelaySetTextArray[MAX_RELAY_NUM + 1];

	int i;
	for(i = 0; i <= MAX_RELAY_NUM; i ++)
	{
		s_AlarmRelaySetTextArray[i] = GetLCDLangText(ALARM_NO_RELAY_LANG_ID + i,
			pLCDLangFile->iLangTextNum,
			pLCDLangFile->pLangText);

	}

	//Special handle
	pDisValuePreInfo->iSigValueType = VAR_ENUM;
	pDisValuePreInfo->iStateNum = MAX_RELAY_NUM + 1;
	pDisValuePreInfo->pStateText = s_AlarmRelaySetTextArray;
	pDisValuePreInfo->szValueDisplayFmt = "2";

	ALARM_SIG_INFO*		pAlarmSigInfo;
	pAlarmSigInfo = (ALARM_SIG_INFO*)(pMenuItem->pvItemData);
	ASSERT(pAlarmSigInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		pMenuItem->vdData.enumValue = 
			pAlarmSigInfo->iAlarmRelayNo;
	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));

	return RET_VALID_DATA;
}


/*==========================================================================*
 * FUNCTION : SetAlarmRelaySetFunc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: MENU_ITEM*  pMenuItem : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-09-25 14:42
 *==========================================================================*/
int SetAlarmRelaySetFunc(MENU_ITEM* pMenuItem)
{
	int nEquipTypeID, nSigType, nSigID;

	SET_A_SIGNAL_INFO_STU stSetASignalInfo;

	ASSERT(pMenuItem && pMenuItem->pvItemData);

	DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID,
		nEquipTypeID,
		nSigType,
		nSigID);

	stSetASignalInfo.byModifyType = MODIFY_ALARM_RELAY;
	stSetASignalInfo.bModifyAlarmRelay = pMenuItem->vdData.dtValue;
	memcpy(stSetASignalInfo.cModifyUser, g_szCurUser, 1 + strlen(g_szCurUser));

	return DxiSetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
		nEquipTypeID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_ALARM, nSigID),		
		sizeof(SET_A_SIGNAL_INFO_STU),			
		&stSetASignalInfo,			
		0);
}




int GetProductPreInfo(MENU_ITEM* pMenuItem, 
				   DIS_VALUE_PRE_INFO* pDisValuePreInfo,
				   int nGetType)
{
	UNUSED(nGetType);

	int nCurrentLangFlag = GetCurrentLangFlag();

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	//Read Product Info
	int	nDeviceId = LCD_SPLIT_BARCODE_SIGNAL_ID_EQUIP(pMenuItem->nMenuItemID);
	int	nSigResID = LCD_SPLIT_BARCODE_SIGNAL_ID_SIGNAL(pMenuItem->nMenuItemID);

	PRODUCT_INFO*	pProductInfo = NULL;

	pProductInfo = QueryProductInfo(nDeviceId);

	//PRODUCT_INFO	sProductInfo;
	//PRODUCT_INFO	*pProductInfo = NULL;

	//memset(&sProductInfo, 0, sizeof(sProductInfo));
	//pProductInfo = &sProductInfo;

	////I2C Devices
	//else if((nDeviceId >= nFirstRectID + 290) && (nDeviceId < nFirstRectID + 298))
	//{
	//	pProductInfo = g_SiteInfo.stI2CProductInfo + (nDeviceId - nFirstRectID - 290);
	//}
	//else if((nDeviceId >= nFirstRectID + 298) && (nDeviceId < nFirstRectID + 306))
	//{
	//	pProductInfo = g_SiteInfo.stCANSMDUPProductInfo + (nDeviceId - nFirstRectID - 298);
	//}
	////Rs485
	//else if((nDeviceId >= nFirstRectID + 306) && (nDeviceId < nFirstRectID + 368))
	//{

	//	pProductInfo = g_SiteInfo.st485ProductInfo + (nDeviceId - nFirstRectID - 306);
	//}
	////Convertor
	//else if(nDeviceId >= nFirstRectID + 368)
	//{
	//	pProductInfo = g_SiteInfo.stCANConverterProductInfo + (nDeviceId - nFirstRectID - 368);

	//}
	ASSERT(pProductInfo);


	switch(nSigResID)
	{
	case PRODUCT_INFO_NAME_LANG_ID:
		if(pProductInfo->szDeviceAbbrName[nCurrentLangFlag][0]!='\0')
		{
			strncpy(pDisValuePreInfo->pString, pProductInfo->szDeviceAbbrName[nCurrentLangFlag], 16);
		}
		else
		{
			strcpy(pDisValuePreInfo->pString, "N/A");
		}

		break;

	case PRODUCT_INFO_PART_NUMBER_LANG_ID:
		if(pProductInfo->szPartNumber[0]!='\0')
		{
			strncpy(pDisValuePreInfo->pString, pProductInfo->szPartNumber, 16);
			
			//remove all char " "in the head or tail
			char szBuf[sizeof(pProductInfo->szPartNumber) + 1];
			strncpy(szBuf, pProductInfo->szPartNumber, sizeof(szBuf));
			strncpy(pDisValuePreInfo->pString, Cfg_RemoveWhiteSpace(szBuf), 16);
		}
		else
		{
			strcpy(pDisValuePreInfo->pString, "N/A");
		}

		break;

	case PRODUCT_INFO_HWVERSION_LANG_ID:
		if(pProductInfo->szHWVersion[0]!='\0')
		{
			strncpy(pDisValuePreInfo->pString, pProductInfo->szHWVersion, 16);
		}
		else
		{
			strcpy(pDisValuePreInfo->pString, "N/A");
		}

		break;

	case PRODUCT_INFO_SWVERSION_LANG_ID:
		if(pProductInfo->szSWVersion[0]!='\0')
		{
			strncpy(pDisValuePreInfo->pString, pProductInfo->szSWVersion, 16);
		}
		else
		{
			strcpy(pDisValuePreInfo->pString, "N/A");
		}

		break;

	case PRODUCT_INFO_SERIAL_LANG_ID:
		if(pProductInfo->szSerialNumber[0]!='\0')
		{
			strncpy(pDisValuePreInfo->pString, pProductInfo->szSerialNumber, 16);
		}
		else
		{
			strcpy(pDisValuePreInfo->pString, "N/A");
		}
		break;

	default:
		strcpy(pDisValuePreInfo->pString, "N/A");
		break;

	}


	return ERR_LCD_OK;
}

/*==========================================================================*
* FUNCTION : GetKeypadVoiceInfo
* PURPOSE  : Get Keypad voice enable in lcd
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : caihao                DATE: 2005-12-26 15:04
*==========================================================================*/
static int GetKeypadVoiceInfo(MENU_ITEM* pMenuItem, 
							  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							  int nGetType)
{	
	int	nRetDataFlag = RET_VALID_DATA;
	
	SELF_DEFINE_INFO*	pSelfDefineInfo;

	int nBufLen;
	void* pSigInfo;

	ASSERT(pMenuItem);

	ASSERT(pDisValuePreInfo);

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);

	pSigInfo = NULL;

	if(DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
		STD_ID_ACU_SYSTEM_EQUIP,
		SIG_ID_KEYPAD_VOICE_SET,
		&nBufLen,
		&pSigInfo,
		0) != ERR_DXI_OK)
	{
		return	nRetDataFlag;
	}

	pSelfDefineInfo->iStateNum	= ((SET_SIG_INFO*)pSigInfo)->iStateNum;
	pSelfDefineInfo->pStateText	= ((SET_SIG_INFO*)pSigInfo)->pStateText;

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		SIG_BASIC_VALUE* pSigValue;

		if (DxiGetData(VAR_A_SIGNAL_VALUE,
			DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
			SIG_ID_KEYPAD_VOICE_SET,
			&nBufLen,			
			&pSigValue,			
			0) == ERR_DXI_OK)
		{
			//ASSERT(pSigValue);
			if(!pSigValue) 
			{
				return RET_INVALID_DATA;
			}

			if(!SIG_VALUE_IS_VALID(pSigValue))
			{
				//if ret invalid data, should reverse display
				nRetDataFlag = RET_INVALID_DATA;
			}


			memcpy(&(pMenuItem->vdData),
				&(pSigValue->varValue), 
				sizeof(VAR_VALUE));

		}
	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData),
		sizeof(VAR_VALUE));

	return nRetDataFlag;

}

/*==========================================================================*
* FUNCTION : GetUserPreInfo
* PURPOSE  : Get user info before it will be displayed
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 15:54
*==========================================================================*/
static int GetUserPreInfo(MENU_ITEM* pMenuItem, 
						  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						  int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;

	USER_INFO_STRU*		pUserInfo;
	int					nUserNum = 0;

	SELF_DEFINE_INFO*	pSelfDefineInfo;

	int					i = 0;
	LANG_TEXT*			pLangText;

	UNUSED(nGetType);

	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);


	nUserNum = GetUserNum(GET_ACTUAL_USER_NUM)-1;

	GetUserInfo(&pUserInfo);

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);

	pSelfDefineInfo->iStateNum = nUserNum;

	pSelfDefineInfo->fMinValidValue = 0;
	pSelfDefineInfo->fMaxValidValue = nUserNum - 1;


	//To keep the user operation consistent to signal
	for(i = 0; i < nUserNum; i++, pUserInfo++)
	{
		ASSERT(pUserInfo);
		if((strcmp(pUserInfo->szUserName,SUPER_ADMIN_NAME) == 0))
		{
			i--;
			continue;//remove emersSadmin display on lcd
		}
		pLangText = pSelfDefineInfo->pStateText[i];
		 

		ASSERT(pLangText);

		pLangText->pAbbrName[0] = pUserInfo->szUserName;
		pLangText->pAbbrName[1] = pUserInfo->szUserName;

		//scu plus add for mult-lang
		//pLangText->pAbbrName[2] = pUserInfo->szUserName;

	}

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		pMenuItem->vdData.enumValue = pSelfDefineInfo->defaultValue.enumValue;
	}

	//Prevent to cause beyond the mark when deletting user info
	if (pMenuItem->vdData.enumValue >= pDisValuePreInfo->iStateNum)
	{
		pMenuItem->vdData.enumValue = 0;
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));

	return nRetDataFlag;
}

/*==========================================================================*
* FUNCTION : GetPasswordPreInfo
* PURPOSE  : Get password info before it will be displayed
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 16:00
*==========================================================================*/
static int GetPasswordPreInfo(MENU_ITEM* pMenuItem, 
							  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							  int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;

	SELF_DEFINE_INFO*	 pSelfDefineInfo;

	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	//by HULONGWEN To VAR_STRING type, pDisValuePreInfo->vdData unused temporary  

	//Default display "*"
	if (NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		strcpy(pSelfDefineInfo->szString, "*");

	}
	else
	{
	}

	return nRetDataFlag;
}

/*==========================================================================*
* FUNCTION : GetSiteIDPreInfo
* PURPOSE  : Get site ID info before it will be displayed
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 16:00
*==========================================================================*/
static int GetSiteIDPreInfo(MENU_ITEM* pMenuItem, 
							DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;


	SELF_DEFINE_INFO*	pSelfDefineInfo;


	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);


	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		int		nSiteID = 0;

		int		nBufLen;


		if (DxiGetData(VAR_ACU_PUBLIC_CONFIG,
			SITE_ID,			
			0,		
			&nBufLen,			
			&nSiteID,			
			0) != ERR_DXI_OK)
		{
			nSiteID = 0;

			nRetDataFlag = RET_INVALID_DATA;
		}

		pMenuItem->vdData.ulValue = nSiteID;

	}
	else
	{

	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));


	return nRetDataFlag;

}

/*==========================================================================*
* FUNCTION : GetSysLanguageInfo
* PURPOSE  : Get system language info before it will be displayed
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 16:00
*==========================================================================*/
static int GetSysLanguageInfo(MENU_ITEM* pMenuItem, 
							  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							  int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	int		nRetDataFlag = RET_VALID_DATA;
	int		nCurrentLangFlag = GetCurrentLangFlag();

	int nBufLen;
	void* pSigInfo;

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	//LANG_FILE*	pLCDLangFile = GetLCDLangFile();
	//ASSERT(pLCDLangFile);
#ifndef GET_PART_LANG_INFO_FROM_SYS_RES
	static LANG_TEXT*		s_SysLangSetTextArray[SYS_LANG_NUM_ONE_TIME];

	//Unused
	pSelfDefineInfo->iStateNum = SYS_LANG_NUM_ONE_TIME;

	//English
	pLangText = GetLCDLangText(
		ENGLISH_LANGUAGE_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	pSelfDefineInfo->pStateText[0] = pLangText;

	//Local language
	pLangText = GetLCDLangText(
		LOCAL_LANGUAGE_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	pSelfDefineInfo->pStateText[1] = pLangText;

	pDisValuePreInfo->pStateText = s_SysLangSetTextArray;
#else
	//All these from system resource
	DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,			
		STD_ID_ACU_SYSTEM_EQUIP,	
		SIG_ID_LANGUAGE_SUPPORT,
		&nBufLen,			
		&pSigInfo,			
		0);

	pSelfDefineInfo->iStateNum		= ((SET_SIG_INFO*)pSigInfo)->iStateNum;
	pSelfDefineInfo->pStateText	= ((SET_SIG_INFO*)pSigInfo)->pStateText;

#endif

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		pMenuItem->vdData.enumValue = nCurrentLangFlag;
	}
	else
	{
		;
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));


	return nRetDataFlag;
}


BOOL ConvertTime(time_t* pTime, BOOL bUTCToLocal)
{
	SIG_BASIC_VALUE* pSigValue;

	int nBufLen;

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
		SIG_ID_TIME_ZONE_SET,
		&nBufLen,			
		&pSigValue,			
		0) != ERR_DXI_OK)
	{	
		return FALSE;
	}

	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{	
		int nTimeOffset = (pSigValue->varValue.lValue) * 3600 / 2;

		if (bUTCToLocal)
		{
			*pTime += nTimeOffset;
		}
		else//Local time to UTC time
		{
			*pTime -= nTimeOffset;
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

BOOL GetLocalTimeOfACU(time_t* pTime)
{
	ASSERT(pTime);

	int nBufLen;

	if (DxiGetData(VAR_TIME_SERVER_INFO,
		SYSTEM_TIME,			
		0,		
		&nBufLen,			
		pTime,			
		0) != ERR_DXI_OK)
	{
		return FALSE;
	}

	ConvertTime(pTime, TRUE);

	return TRUE;
}

BOOL SetLocalTimeOfACU(time_t* pTime)
{
	ASSERT(pTime);

	ConvertTime(pTime, FALSE);

	//set system time
	if (DxiSetData(VAR_TIME_SERVER_INFO,
		SYSTEM_TIME,			
		0,		
		sizeof(*pTime),			
		pTime,			
		0) != ERR_DXI_OK)
	{
		return FALSE;
	}

	return TRUE;
}



#define DATE_HANDLE_FUNC 0
#define TIME_HANDLE_FUNC 1

/*==========================================================================*
* FUNCTION : GetDateOrTimePreInfo
* PURPOSE  : Get date or time info before it will be displayed
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
*            int                  nDateOrTimeType  : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 16:01
*==========================================================================*/
static int GetDateOrTimePreInfo(MENU_ITEM* pMenuItem, 
								DIS_VALUE_PRE_INFO* pDisValuePreInfo,
								int nGetType,
								int nDateOrTimeType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	int					nRetDataFlag = RET_VALID_DATA;

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	SECTION_DATA_STRU*	pSectionDataStru;
	pSectionDataStru = pSelfDefineInfo->pSectionDataStru;
	ASSERT(pSectionDataStru + 0 != NULL
		&& pSectionDataStru + 1 != NULL
		&& pSectionDataStru + 2 != NULL);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		time_t	tCurtime;

		struct tm		tmCurtime;


		pMenuItem->vdData.enumValue = 0;//year

		if (!GetLocalTimeOfACU(&tCurtime))
		{
			nRetDataFlag = RET_INVALID_DATA;
		}

		gmtime_r(&tCurtime, &tmCurtime);

		if(nDateOrTimeType == DATE_HANDLE_FUNC)
		{
			(pSectionDataStru + 0)->uiValue	= tmCurtime.tm_year 
				+ TM_YEAR_START;
			(pSectionDataStru + 1)->uiValue	= tmCurtime.tm_mon + 1;
			(pSectionDataStru + 2)->uiValue	= tmCurtime.tm_mday;
		}
		else if(nDateOrTimeType == TIME_HANDLE_FUNC)
		{
			(pSectionDataStru + 0)->uiValue	= tmCurtime.tm_hour;
			(pSectionDataStru + 1)->uiValue	= tmCurtime.tm_min;
			(pSectionDataStru + 2)->uiValue	= tmCurtime.tm_sec;
		}

	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData),
		sizeof(VAR_VALUE));


	if(nDateOrTimeType == DATE_HANDLE_FUNC)
	{
		(pSectionDataStru + 2)->uiMaxValue = 
			g_sMonthLimit[(pSectionDataStru + 1)->uiValue - 1];

		//If a leap year and is Feb.
		if(((pSectionDataStru + 1)->uiValue == 2)
			&& (IS_LEAP_YEAR((pSectionDataStru + 0)->uiValue)))
		{
			(pSectionDataStru + 2)->uiMaxValue = 29;
		}
	}

	return nRetDataFlag;
}


static int GetSystemDate(MENU_ITEM* pMenuItem, 
						 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						 int nGetType)
{
	return GetDateOrTimePreInfo(pMenuItem, 
		pDisValuePreInfo,
		nGetType,
		DATE_HANDLE_FUNC);
}

static int GetSystemTimeInfo(MENU_ITEM* pMenuItem, 
							 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							 int nGetType)
{
	return GetDateOrTimePreInfo(pMenuItem, 
		pDisValuePreInfo,
		nGetType,
		TIME_HANDLE_FUNC);
}

/*==========================================================================*
* FUNCTION : GetSysNetPreInfo
* PURPOSE  : Get system net info before it will be displayed
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
*            DIS_VALUE_PRE_INFO*  pDisValuePreInfo : 
*            int                  nGetType         : 
*            int                  nGetLanType      : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : HULONGWEN                DATE: 2005-01-11 16:01
*==========================================================================*/
static int GetSysNetPreInfo(MENU_ITEM* pMenuItem, 
							DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							int nGetType,
							int nGetLanType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	//���DHCP���ܴ򿪣���������
	int	nBufLen, nDHCP;

	//SIG_BASIC_VALUE*	pSigValue = NULL;
	//if((pMenuItem->cMenuStatus == MD_EDIT_STATUS)
	//	&& (DxiGetData(VAR_A_SIGNAL_VALUE,
	//		1,			
	//		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 7),	//���Զ�����
	//		&nBufLen,			
	//		&pSigValue,			
	//		0) == ERR_DXI_OK)
	//	&& (pSigValue->varValue.enumValue == 0))	//�Զ�
	

	if((pMenuItem->cMenuStatus == MD_EDIT_STATUS)
		&& (DxiGetData(VAR_APP_DHCP_INFO,
			0,
			0,
			&nBufLen,
			&nDHCP,
			0) == ERR_DXI_OK)
		&& (nDHCP == APP_DHCP_ON))
	{
		pMenuItem->cMenuStatus = MD_SHOW_STATUS;

		DoModal_AcknowlegeDlg_ID(
			HEADING_PROMPT_INFO_LANG_ID, 
			DHCP_IS_OPEN_LANG_ID, 
			CAN_NOT_SET_LANG_ID,
			ESC_OR_ENT_RET_PROMPT_LANG_ID, 
			1000);
		return RET_VALID_DATA;
	}

	int	nRetDataFlag = RET_VALID_DATA;

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	SECTION_DATA_STRU*	pSectionDataStru;
	pSectionDataStru = pSelfDefineInfo->pSectionDataStru;

	ASSERT(pSectionDataStru + 0 != NULL
		&& pSectionDataStru + 1 != NULL
		&& pSectionDataStru + 2 != NULL
		&& pSectionDataStru + 3 != NULL);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		ULONG	nAddress;
		//struct in_addr* pIn;

		int		nBufLen;

		pMenuItem->vdData.enumValue = 0;//The first section

		if (DxiGetData(VAR_ACU_NET_INFO,
			nGetLanType,			
			0,		
			&nBufLen,			
			&nAddress,			
			0) != ERR_DXI_OK)
		{
			nRetDataFlag = RET_INVALID_DATA;
		}

		//pIn = (struct in_addr*)(&(nAddress));

		//(pSectionDataStru + 0)->uiValue	= pIn->S_un.S_un_b.s_b1;
		//(pSectionDataStru + 1)->uiValue	= pIn->S_un.S_un_b.s_b2;
		//(pSectionDataStru + 2)->uiValue	= pIn->S_un.S_un_b.s_b3;
		//(pSectionDataStru + 3)->uiValue	= pIn->S_un.S_un_b.s_b4;

		(pSectionDataStru + 0)->uiValue	= ((UCHAR*)&nAddress)[0];
		(pSectionDataStru + 1)->uiValue	= ((UCHAR*)&nAddress)[1];
		(pSectionDataStru + 2)->uiValue	= ((UCHAR*)&nAddress)[2];
		(pSectionDataStru + 3)->uiValue	= ((UCHAR*)&nAddress)[3];

	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData),
		sizeof(VAR_VALUE));

	return nRetDataFlag;
}


static int GetSysIpAddress(MENU_ITEM* pMenuItem, 
						   DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						   int nGetType)
{
	return GetSysNetPreInfo(pMenuItem, 
		pDisValuePreInfo,
		nGetType,
		NET_INFO_IP);

}

static int GetSysNetmask(MENU_ITEM* pMenuItem, 
						 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						 int nGetType)
{
	return GetSysNetPreInfo(pMenuItem, 
		pDisValuePreInfo,
		nGetType,
		NET_INFO_NETMASK);
}

static int GetSysDefGateway(MENU_ITEM* pMenuItem, 
							DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							int nGetType)
{
	return GetSysNetPreInfo(pMenuItem, 
		pDisValuePreInfo,
		nGetType,
		NET_INFO_DEFAULT_GATEWAY);
}

static int GetClearHisAlarmItemPreInfo(MENU_ITEM* pMenuItem, 
									   DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									   int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	int					nRetDataFlag = RET_VALID_DATA;

	LANG_FILE* pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	pSelfDefineInfo->iStateNum = 1;
	
	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	static LANG_TEXT*	s_pTextArray[1];

	//Yes option
	s_pTextArray[0] = GetLCDLangText(
		YES_HANDLE_LANGUAGE_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	pDisValuePreInfo->pStateText = s_pTextArray;

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		pMenuItem->vdData.enumValue = 0;
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));


	return nRetDataFlag;
}

static int GetOutgoingAlarmItemPreInfo(MENU_ITEM* pMenuItem, 
									   DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									   int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	int					nRetDataFlag = RET_VALID_DATA;

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	int nBufLen;
	void* pSigInfo;

	DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,			
		STD_ID_ACU_SYSTEM_EQUIP,	
		SIG_ID_ALARM_OUTGOING_BLOCKED,
		&nBufLen,			
		&pSigInfo,			
		0);

	pSelfDefineInfo->iStateNum		= ((SET_SIG_INFO*)pSigInfo)->iStateNum;
	pSelfDefineInfo->pStateText	= ((SET_SIG_INFO*)pSigInfo)->pStateText;

	pDisValuePreInfo->iSigValueType = pSelfDefineInfo->iSigValueType;
	pDisValuePreInfo->iStateNum = pSelfDefineInfo->iStateNum;
	pDisValuePreInfo->pStateText = pSelfDefineInfo->pStateText;

	pDisValuePreInfo->szValueDisplayFmt = pSelfDefineInfo->szValueDisplayFmt;

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		SIG_BASIC_VALUE* pSigValue;

		if (DxiGetData(VAR_A_SIGNAL_VALUE,
			DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
			SIG_ID_ALARM_OUTGOING_BLOCKED,
			&nBufLen,			
			&pSigValue,			
			0) == ERR_DXI_OK)
		{
			ASSERT(pSigValue);	

			if(!SIG_VALUE_IS_VALID(pSigValue))
			{
				//if ret invalid data, should reverse display
				nRetDataFlag = RET_INVALID_DATA;
			}

			memcpy(&(pMenuItem->vdData), 
				&(pSigValue->varValue), 
				sizeof(VAR_VALUE));

		}
	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));


	return nRetDataFlag;


}


static int GetAlarmVoiceCtrlPreInfo(MENU_ITEM* pMenuItem, 
									DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;


	SELF_DEFINE_INFO*	pSelfDefineInfo;


	int nBufLen;
	void* pSigInfo;


	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);


	if (DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,			
		STD_ID_ACU_SYSTEM_EQUIP,	
		SIG_ID_ALARM_VOICE_SET,
		&nBufLen,			
		&pSigInfo,			
		0) != ERR_DXI_OK)
	{
		return nRetDataFlag;
	}

	pSelfDefineInfo->iStateNum	= ((SET_SIG_INFO*)pSigInfo)->iStateNum;
	pSelfDefineInfo->pStateText	= ((SET_SIG_INFO*)pSigInfo)->pStateText;

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		SIG_BASIC_VALUE* pSigValue;

		if (DxiGetData(VAR_A_SIGNAL_VALUE,
			DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
			SIG_ID_ALARM_VOICE_SET,
			&nBufLen,			
			&pSigValue,			
			0) == ERR_DXI_OK)
		{
			ASSERT(pSigValue);	

			if(!SIG_VALUE_IS_VALID(pSigValue))
			{
				//if ret invalid data, should reverse display
				nRetDataFlag = RET_INVALID_DATA;
			}


			memcpy(&(pMenuItem->vdData),
				&(pSigValue->varValue), 
				sizeof(VAR_VALUE));

		}
	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData),
		sizeof(VAR_VALUE));


	return nRetDataFlag;

}

static int GetTimeZonePreInfo(MENU_ITEM* pMenuItem, 
							  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
							  int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;


	SELF_DEFINE_INFO*	pSelfDefineInfo;


	int nBufLen;

	static char s_szTimeZone[20];

	int nCurTimeZone;


	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);


	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);


	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		SIG_BASIC_VALUE* pSigValue;

		if (DxiGetData(VAR_A_SIGNAL_VALUE,
			DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
			SIG_ID_TIME_ZONE_SET,
			&nBufLen,			
			&pSigValue,			
			0) == ERR_DXI_OK)
		{
			ASSERT(pSigValue);	

			if(!SIG_VALUE_IS_VALID(pSigValue))
			{
				//if ret invalid data, should reverse display
				nRetDataFlag = RET_INVALID_DATA;
			}


			memcpy(&(pMenuItem->vdData),
				&(pSigValue->varValue), 
				sizeof(VAR_VALUE));

		}
	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData),
		sizeof(VAR_VALUE));

	nCurTimeZone = pMenuItem->vdData.lValue;

	if (nCurTimeZone == 0)
	{
		snprintf(s_szTimeZone, sizeof(s_szTimeZone), "GMT");
	}
	else 
	{
		snprintf(s_szTimeZone, sizeof(s_szTimeZone), "GMT%s%02d:%02d",
			(nCurTimeZone > 0) ? "+" : "-",
			abs(nCurTimeZone / 2), 
			((nCurTimeZone % 2) == 0) ? 00 : 30);
	}

	//Special handle
	pDisValuePreInfo->szSigUnit = NULL;

	pDisValuePreInfo->iSigValueType = VAR_STRING;

	pDisValuePreInfo->pString = s_szTimeZone;

	pDisValuePreInfo->szValueDisplayFmt = "9";



	return nRetDataFlag;

}



static int SigTimeSetGetPreInfo(MENU_ITEM* pMenuItem, 
								DIS_VALUE_PRE_INFO* pDisValuePreInfo,
								int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;


	SELF_DEFINE_INFO*	pSelfDefineInfo;

	SECTION_DATA_STRU*	pSectionDataStru = NULL;


	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);

	pSectionDataStru = pSelfDefineInfo->pSectionDataStru;

	// maofuhua changed. 2005-3-26
//	ASSERT(pSectionDataStru + 0 != NULL
//		&& pSectionDataStru + 1 != NULL
//		&& pSectionDataStru + 2 != NULL
//#ifdef TIME_SET_INCLUDE_MIN
//		&& pSectionDataStru + 3 != NULL
//#endif//TIME_SET_INCLUDE_MIN
//		);


#ifdef TIME_SET_INCLUDE_MIN
	ASSERT(pSectionDataStru + 0 != NULL
		&& pSectionDataStru + 1 != NULL
		&& pSectionDataStru + 2 != NULL
		&& pSectionDataStru + 3 != NULL);
#else
	ASSERT(pSectionDataStru + 0 != NULL
		&& pSectionDataStru + 1 != NULL
		&& pSectionDataStru + 2 != NULL);
#endif//TIME_SET_INCLUDE_MIN

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		time_t	tSetTime = 0;

		struct tm		tmSettime;

		SIG_BASIC_VALUE*	pSigValue = NULL;
		int		nBufLen;

		int nEquipId, nSignalType, nSignalID;

		DXI_SPLIT_UNIQUE_SIG_ID(pMenuItem->nMenuItemID, 
			nEquipId, 
			nSignalType, 
			nSignalID);


		if (DxiGetData(VAR_A_SIGNAL_VALUE,
			nEquipId,			
			DXI_MERGE_SIG_ID(nSignalType, nSignalID),
			&nBufLen,			
			&pSigValue,			
			0) == ERR_DXI_OK)
		{
			ASSERT(pSigValue);	

			if(!SIG_VALUE_IS_VALID(pSigValue))
			{
				//if ret invalid data, should reverse display
				nRetDataFlag = RET_INVALID_DATA;
			}

			pSelfDefineInfo->bCanBeCtrlledorSet = 
				SIG_VALUE_IS_SETTABLE(pSigValue);


			tSetTime = (time_t)(pSigValue->varValue.dtValue);	

		}
		else
		{

			nRetDataFlag = RET_INVALID_DATA;
		}		


		ConvertTime(&tSetTime, TRUE);

		gmtime_r(&tSetTime, &tmSettime);

		(pSectionDataStru + 0)->uiValue	= tmSettime.tm_mon + 1;
		(pSectionDataStru + 1)->uiValue	= tmSettime.tm_mday;

		(pSectionDataStru + 2)->uiValue	= tmSettime.tm_hour;

#ifdef TIME_SET_INCLUDE_MIN
		(pSectionDataStru + 3)->uiValue	= tmSettime.tm_min;
#endif//TIME_SET_INCLUDE_MIN


		pMenuItem->vdData.enumValue = 0;//month

	}
	else
	{
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData),
		sizeof(VAR_VALUE));

	(pSectionDataStru + 1)->uiMaxValue = 
		g_sMonthLimit[(pSectionDataStru + 0)->uiValue - 1];


	return nRetDataFlag;
}



static int GetYDN23SlefAddr(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;

	SELF_DEFINE_INFO*	pSigInfo;
	//SET_SIG_INFO*		pSetSigInfo;

	ASSERT(pMenuItem);
	pSigInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSigInfo);
	ASSERT(pDisValuePreInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		if(GetYdnPriCfg(&g_YdnPriCfg))
		{
			return RET_INVALID_DATA;
		}
		else
		{	
			pMenuItem->vdData.ulValue = g_YdnPriCfg.byADR;
		}
	}

	memcpy(&(pDisValuePreInfo->vdData), &(pMenuItem->vdData), sizeof(VAR_VALUE));
	
	pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;
	pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

	pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

	pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
	pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;

	return nRetDataFlag;
}


static int GetYDN23PortType(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	int					i, nRetDataFlag = RET_VALID_DATA;

	SELF_DEFINE_INFO*	pSigInfo;
	LANG_FILE*			pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	//For alarm level set
	static LANG_TEXT*	s_YdnCntModeTextArray[YDN23_PORT_TYPE_NUM];

	
	for(i = 0; i < YDN23_PORT_TYPE_NUM; i++)
	{
		s_YdnCntModeTextArray[i] = GetLCDLangText(CNT_MODE_RS232 + i,
								pLCDLangFile->iLangTextNum,
								pLCDLangFile->pLangText);
		
	}

	pSigInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSigInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		if(GetYdnPriCfg(&g_YdnPriCfg))
		{
			return RET_INVALID_DATA;
		}
		else
		{	
			pMenuItem->vdData.enumValue = g_YdnPriCfg.iMediaType;
		}
	}

	memcpy(&(pDisValuePreInfo->vdData), &(pMenuItem->vdData), sizeof(VAR_VALUE));
	
	pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;
	pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

	pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

	pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
	pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;
	
	pDisValuePreInfo->iStateNum = YDN23_PORT_TYPE_NUM;
	pDisValuePreInfo->pStateText = s_YdnCntModeTextArray;

	return nRetDataFlag;
}


static int GetYDN23PortParam(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	int					i, nRetDataFlag = RET_VALID_DATA;

	SELF_DEFINE_INFO*	pSigInfo;
	LANG_FILE*			pLCDLangFile = GetLCDLangFile();

	//For alarm level set
	static LANG_TEXT*	s_YdnPortParamTextArray[YDN23_PORT_PARAM_NUM];


	ASSERT(pLCDLangFile);
	ASSERT(pMenuItem);

	for(i = 0; i < YDN23_PORT_PARAM_NUM; i++)
	{
		s_YdnPortParamTextArray[i] = GetLCDLangText(PORT_PARAM_5050 + i,
								pLCDLangFile->iLangTextNum,
								pLCDLangFile->pLangText);
		
	}

	pSigInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSigInfo);
	ASSERT(pDisValuePreInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		if(GetYdnPriCfg(&g_YdnPriCfg))
		{
			return RET_INVALID_DATA;
		}
		else
		{	
			pMenuItem->vdData.enumValue 
				= GetEnumByPortParm(g_YdnPriCfg.szCommPortParam);
			
			if(pMenuItem->vdData.enumValue < 0)
			{
				return RET_INVALID_DATA;
			}
		}
	}

	pDisValuePreInfo->vdData.enumValue = pMenuItem->vdData.enumValue;
	
	pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;
	pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

	pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

	pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
	pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;
	
	pDisValuePreInfo->iStateNum = YDN23_PORT_PARAM_NUM;
	pDisValuePreInfo->pStateText = s_YdnPortParamTextArray;

	return nRetDataFlag;
}

static int GetYDN23CallBackEnb(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	int					i, nRetDataFlag = RET_VALID_DATA;

	SELF_DEFINE_INFO*	pSigInfo;
	LANG_FILE*			pLCDLangFile = GetLCDLangFile();

	//For alarm level set
	static LANG_TEXT*	s_YdnCallBackEnbTextArray[YDN23_CALL_BACK_ENB_NUM];


	ASSERT(pLCDLangFile);
	ASSERT(pMenuItem);

	for(i = 0; i < YDN23_CALL_BACK_ENB_NUM; i++)
	{
		s_YdnCallBackEnbTextArray[YDN23_CALL_BACK_ENB_NUM - i - 1] 
				= GetLCDLangText(YES_HANDLE_LANGUAGE_LANG_ID + i,
								pLCDLangFile->iLangTextNum,
								pLCDLangFile->pLangText);
		
	}

	pSigInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSigInfo);
	ASSERT(pDisValuePreInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		if(GetYdnPriCfg(&g_YdnPriCfg))
		{
			return RET_INVALID_DATA;
		}
		else
		{	
			pMenuItem->vdData.enumValue = g_YdnPriCfg.bReportInUse;
		}
	}

	memcpy(&(pDisValuePreInfo->vdData), &(pMenuItem->vdData), sizeof(VAR_VALUE));
	
	pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;
	pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

	pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

	pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
	pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;
	
	pDisValuePreInfo->iStateNum = YDN23_CALL_BACK_ENB_NUM;
	pDisValuePreInfo->pStateText = s_YdnCallBackEnbTextArray;

	return nRetDataFlag;
}


static int GetYDN23CallBackNum(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;
	SELF_DEFINE_INFO*	pSigInfo;

	ASSERT(pMenuItem);

	pSigInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSigInfo);
	ASSERT(pDisValuePreInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		if(GetYdnPriCfg(&g_YdnPriCfg))
		{
			return RET_INVALID_DATA;
		}
		else
		{	
			pMenuItem->vdData.ulValue = g_YdnPriCfg.iMaxAttempts;
		}
	}

	memcpy(&(pDisValuePreInfo->vdData), &(pMenuItem->vdData), sizeof(VAR_VALUE));
	
	pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;
	pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

	pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

	pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
	pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;

	return nRetDataFlag;
}

static int GetYDN23CallBackInterval(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	int					nRetDataFlag = RET_VALID_DATA;
	SELF_DEFINE_INFO*	pSigInfo;

	ASSERT(pMenuItem);

	pSigInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSigInfo);
	ASSERT(pDisValuePreInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		if(GetYdnPriCfg(&g_YdnPriCfg))
		{
			return RET_INVALID_DATA;
		}
		else
		{	
			pMenuItem->vdData.ulValue = g_YdnPriCfg.iAttemptElapse / 1000;
		}
	}

	memcpy(&(pDisValuePreInfo->vdData), &(pMenuItem->vdData), sizeof(VAR_VALUE));
	
	pDisValuePreInfo->szSigUnit = "s";
	pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

	pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

	pDisValuePreInfo->fMinValidValue = pSigInfo->fMinValidValue;
	pDisValuePreInfo->fMaxValidValue = pSigInfo->fMaxValidValue;

	return nRetDataFlag;
}

static int GetYDN23PhoneNo(MENU_ITEM* pMenuItem,
						  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
						  int nGetType,
						  int nIndex)	//nIndex = 1, 2, 3
{
	int					nRetDataFlag = RET_VALID_DATA;
	SELF_DEFINE_INFO*	pSigInfo;

	ASSERT(pMenuItem);

	pSigInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSigInfo);
	ASSERT(pDisValuePreInfo);

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		if(GetYdnPriCfg(&g_YdnPriCfg))
		{
			return RET_INVALID_DATA;
		}
		else
		{
			memcpy(pSigInfo->szString, 
				g_YdnPriCfg.szAlarmReportPhoneNumber[nIndex - 1], 
				strlen(g_YdnPriCfg.szAlarmReportPhoneNumber[nIndex - 1]) + 1);
			pDisValuePreInfo->pString = pSigInfo->szString;
		}
	}
	else
	{
		pDisValuePreInfo->pString = pSigInfo->szString;
	}

	pDisValuePreInfo->szSigUnit = pSigInfo->szSigUnit;
	pDisValuePreInfo->iSigValueType = pSigInfo->iSigValueType;

	pDisValuePreInfo->szValueDisplayFmt = pSigInfo->szValueDisplayFmt;

	return nRetDataFlag;
}

static int GetYDN23Phone1(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	return GetYDN23PhoneNo(pMenuItem, pDisValuePreInfo, nGetType, 1);
}

static int GetYDN23Phone2(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	return GetYDN23PhoneNo(pMenuItem, pDisValuePreInfo, nGetType, 2);
}

static int GetYDN23Phone3(MENU_ITEM* pMenuItem,
					  DIS_VALUE_PRE_INFO* pDisValuePreInfo,
					  int nGetType)
{
	return GetYDN23PhoneNo(pMenuItem, pDisValuePreInfo, nGetType, 3);
}

/*==========================================================================*
* FUNCTION :  RemoveSpace
* PURPOSE  :  remove space in string
* CALLS    : 
* CALLED BY:  Web_PageRefresh 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wan Kun              DATE: 2006-08-29 11:08
*==========================================================================*/
static char *RemoveSpace(char *buf)
{
#define IS_SPACE(c)								((c)==(0x20))
	char *p=buf;
	char pfinal[30]="";
	int i=0;
	while(*p)
	{
		if(IS_SPACE(*p)) p=p++;
		pfinal[i]=*p;
		p++;
		i++;
	}
	p=strdup(pfinal);
	return p;
}

static int SetYDN23PhoneNo(MENU_ITEM* pMenuItem, int nIndex)	//nIndex = 1, 2, 3
{
	ASSERT(pMenuItem);

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	if(GetYdnPriCfg(&g_YdnPriCfg))
	{
		return RET_INVALID_DATA;
	}
	else
	{	
		memcpy(g_YdnPriCfg.szAlarmReportPhoneNumber[nIndex - 1], 
			RemoveSpace(pSelfDefineInfo->szString),
			strlen(RemoveSpace(pSelfDefineInfo->szString)));

	}

	ULONG	nFlag;
	if(nIndex == 1)
	{
		nFlag = YDN_CFG_REPORT_NUMBER_1;
	}
	else if(nIndex == 2)
	{
		nFlag = YDN_CFG_REPORT_NUMBER_2;
	}
	else if(nIndex == 3)
	{
		nFlag = YDN_CFG_REPORT_NUMBER_3;
	}
	else
	{
		nFlag = NULL;
	}

	return SetYdnPriCfg(&g_YdnPriCfg, YDN_CFG_W_MODE | nFlag);

}


static int SetYDN23Phone1(MENU_ITEM* pMenuItem)
{
	return SetYDN23PhoneNo(pMenuItem, 1);
}

static int SetYDN23Phone2(MENU_ITEM* pMenuItem)
{
	return SetYDN23PhoneNo(pMenuItem, 2);
}

static int SetYDN23Phone3(MENU_ITEM* pMenuItem)
{
	return SetYDN23PhoneNo(pMenuItem, 3);
}





//Should return ID_VIEWING
static int SetUserInfo(MENU_ITEM* pMenuItem)
{
	int nRet = ID_VIEWING;

	SELF_DEFINE_INFO*	pSelfDefineInfo;	

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	pSelfDefineInfo->defaultValue.enumValue = pMenuItem->vdData.enumValue;

	UNUSED(pMenuItem);

	return nRet;
}

//If password is right, return the level of the user,
//otherwise return ID_ERR_PASSWORD
static int SetPasswordInfo(MENU_ITEM* pMenuItem)
{
	SELF_DEFINE_INFO*	pSelfDefineInfo;

	char*				pUserName;
	char*				pUserPassword;

	USER_INFO_STRU		sUserInfo;

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	ASSERT(pSelfDefineInfo->iSigValueType == VAR_STRING);

	pUserPassword = pSelfDefineInfo->szString;
	//trim
	int nLen = strlen(pUserPassword);
	if(*(pUserPassword + nLen - 1) == ' ')
	{
		*(pUserPassword + nLen - 1) = NULL;
	}

	//The item of user info
	pMenuItem--;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	ASSERT(pSelfDefineInfo->iSigValueType == VAR_ENUM);

	pUserName = pSelfDefineInfo->pStateText[pMenuItem->vdData.enumValue]->
		pAbbrName[0];

	FindUserInfo(pUserName, &sUserInfo);

	if(strcmp(sUserInfo.szPassword, pUserPassword) == 0)
	{
		snprintf(g_szCurUser, sizeof(g_szCurUser), "LCD: %s", pUserName);

		pSelfDefineInfo->defaultValue.enumValue = pMenuItem->vdData.enumValue;

		return sUserInfo.byLevel;
	}
	else
	{
		return ID_ERR_PASSWORD;
	}

}

static int SetSiteIDInfo(MENU_ITEM* pMenuItem)
{
	return DxiSetData(VAR_ACU_PUBLIC_CONFIG,
		SITE_ID,			
		0,		
		sizeof(pMenuItem->vdData.lValue),			
		&(pMenuItem->vdData.lValue),			
		SET_TIME_OUT);

}

static int SetSysLanguageInfo(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int	nCurLangFlag = GetCurrentLangFlag();
	int	nChangeLangFlag;
	nChangeLangFlag = pMenuItem->vdData.enumValue;

	if(nCurLangFlag != nChangeLangFlag)
	{
		//SetCurrentLangFlag(nChangeLangFlag);
		SetCurrentLang(pMenuItem->vdData.enumValue, FALSE);
	}

	return ERR_LCD_OK;
}


static int SetDateOrTime(MENU_ITEM* pMenuItem, int nDateOrTimeType)
{
	SELF_DEFINE_INFO*	pSelfDefineInfo;

	SECTION_DATA_STRU*	pSectionDataStru;

	time_t	tCurtime;


	struct tm		tmCurtime;

	GetLocalTimeOfACU(&tCurtime);

	ASSERT(pMenuItem);

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);

	pSectionDataStru = pSelfDefineInfo->pSectionDataStru;

	ASSERT(pSectionDataStru + 0 != NULL
		&& pSectionDataStru + 1 != NULL
		&& pSectionDataStru + 2 != NULL);

	gmtime_r(&tCurtime, &tmCurtime);

	if(nDateOrTimeType == DATE_HANDLE_FUNC)
	{
		tmCurtime.tm_year = (pSectionDataStru + 0)->uiValue - TM_YEAR_START;
		tmCurtime.tm_mon  = (pSectionDataStru + 1)->uiValue - 1;
		tmCurtime.tm_mday = (pSectionDataStru + 2)->uiValue;
	}
	else if(nDateOrTimeType == TIME_HANDLE_FUNC)
	{
		tmCurtime.tm_hour = (pSectionDataStru + 0)->uiValue;
		tmCurtime.tm_min  = (pSectionDataStru + 1)->uiValue;
		tmCurtime.tm_sec = (pSectionDataStru + 2)->uiValue;
	}

	tCurtime = mktime(&tmCurtime);

	if (SetLocalTimeOfACU(&tCurtime))
	{
		return ERR_LCD_OK;
	}
	else
	{
		return ERR_LCD_FAILURE;
	}

}

/*==========================================================================*
* FUNCTION :SetKeypadVoiceInfo
* PURPOSE  : Set Keypad voice enable in lcd
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: MENU_ITEM*           pMenuItem        : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : caihao                DATE: 2005-12-26 15:04
*==========================================================================*/
static int SetKeypadVoiceInfo(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int nEquipID, nSigType, nSigID;
	nEquipID = DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);
	DXI_SPLIT_SIG_ID(SIG_ID_KEYPAD_VOICE_SET, nSigType, nSigID);

	return SetSignalValueInterface(nEquipID, 
		nSigType, nSigID, pMenuItem->vdData, 
		SERVICE_OF_USER_INTERFACE, 
		EQUIP_CTRL_SEND_URGENTLY,
		SET_TIME_OUT);
}

static int SetSystemDate(MENU_ITEM* pMenuItem)
{
	return SetDateOrTime(pMenuItem, DATE_HANDLE_FUNC);
}


static int SetSystemTimeInfo(MENU_ITEM* pMenuItem)
{
	return SetDateOrTime(pMenuItem, TIME_HANDLE_FUNC);
}

static int SigTimeSetSetFunc(MENU_ITEM* pMenuItem)
{
	SELF_DEFINE_INFO*	pSelfDefineInfo;

	SECTION_DATA_STRU*	pSectionDataStru;

	time_t	tSetTime = 0;

	struct tm		tmSetTime;


	ASSERT(pMenuItem);

	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);

	ASSERT(pSelfDefineInfo);

	pSectionDataStru = pSelfDefineInfo->pSectionDataStru;


#ifdef TIME_SET_INCLUDE_MIN
	ASSERT(pSectionDataStru + 0 != NULL
		&& pSectionDataStru + 1 != NULL
		&& pSectionDataStru + 2 != NULL
		&& pSectionDataStru + 3 != NULL);
#else
	ASSERT(pSectionDataStru + 0 != NULL
		&& pSectionDataStru + 1 != NULL
		&& pSectionDataStru + 2 != NULL);
#endif//TIME_SET_INCLUDE_MIN

	GetLocalTimeOfACU(&tSetTime);	//for system time set
	gmtime_r(&tSetTime, &tmSetTime);
	
	//tmSetTime.tm_year = 100;//year: 2000
	tmSetTime.tm_mon  = (pSectionDataStru + 0)->uiValue - 1;
	tmSetTime.tm_mday = (pSectionDataStru + 1)->uiValue;
	tmSetTime.tm_hour = (pSectionDataStru + 2)->uiValue;
#ifdef TIME_SET_INCLUDE_MIN
	tmSetTime.tm_min  = (pSectionDataStru + 3)->uiValue;

	tmSetTime.tm_sec = 30;

	tmSetTime.tm_year = 71;
#else
	tmSetTime.tm_min = 7;

	tmSetTime.tm_sec = 30;

	tmSetTime.tm_year = 71;//1971YY xMM xDD xHour 7Min 30Sec��To solve the precision of EEM time

#endif//TIME_SET_INCLUDE_MIN	


	tSetTime = mktime(&tmSetTime);

	ConvertTime(&tSetTime, FALSE);


	pMenuItem->vdData.dtValue = tSetTime;
	
	return SetSignalInfo(pMenuItem);
}

static int SetSysNetInfo(MENU_ITEM* pMenuItem, int nSetLanType)
{
	ASSERT(pMenuItem);

	//���DHCP���ܴ򿪣���������
	int	nBufLen, nDHCP;
	if((DxiGetData(VAR_APP_DHCP_INFO,
			0,
			0,
			&nBufLen,
			&nDHCP,
			0) == ERR_DXI_OK)
		&& (nDHCP == APP_DHCP_ON))
	{
		DoModal_AcknowlegeDlg_ID(
			HEADING_PROMPT_INFO_LANG_ID, 
			DHCP_IS_OPEN_LANG_ID, 
			CAN_NOT_SET_LANG_ID,
			ESC_OR_ENT_RET_PROMPT_LANG_ID, 
			1000);
		return ERR_LCD_OK;
	}

	ULONG	nAddress;

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	SECTION_DATA_STRU*	pSectionDataStru;
	pSectionDataStru = pSelfDefineInfo->pSectionDataStru;
	ASSERT(pSectionDataStru + 0 != NULL
		&& pSectionDataStru + 1 != NULL
		&& pSectionDataStru + 2 != NULL
		&& pSectionDataStru + 3 != NULL);


	((UCHAR*)&nAddress)[0] = (pSectionDataStru + 0)->uiValue;
	((UCHAR*)&nAddress)[1] = (pSectionDataStru + 1)->uiValue;
	((UCHAR*)&nAddress)[2] = (pSectionDataStru + 2)->uiValue;
	((UCHAR*)&nAddress)[3] = (pSectionDataStru + 3)->uiValue;


	//set system net info
	return DxiSetData(VAR_ACU_NET_INFO,
		nSetLanType,			
		0,		
		sizeof(nAddress),			
		&nAddress,			
		0);

}

static int SetSysIpAddress(MENU_ITEM* pMenuItem)
{
	return SetSysNetInfo(pMenuItem, NET_INFO_IP);
}

static int SetSysNetmask(MENU_ITEM* pMenuItem)
{
	return SetSysNetInfo(pMenuItem, NET_INFO_NETMASK);
}

static int SetSysDefGateway(MENU_ITEM* pMenuItem)
{
	return SetSysNetInfo(pMenuItem, NET_INFO_DEFAULT_GATEWAY);
}

static int SetClearHisAlarmItem(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int nError = ERR_LCD_OK;

	if (pMenuItem->vdData.enumValue == 0)//Only this selection
	{
		//clear history alarms
		BOOL	bOK = DAT_StorageDeleteRecord(HIST_ALARM_LOG);

		nError = bOK ? ERR_LCD_OK : ERR_LCD_FAILURE;
		
		AppLogOut(LCD_UI_TASK, APP_LOG_WARNING, 
					"Emptying History alarm on LCD %s.\n",
					bOK ? "OK" : "FAILED");

	}
	else
	{
		//by HULONGWEN
	}

	return nError;
}

static int SetOutgoingAlarmItem(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int nEquipID, nSigType, nSigID;

	nEquipID = DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);
	DXI_SPLIT_SIG_ID(SIG_ID_ALARM_OUTGOING_BLOCKED, nSigType, nSigID);

	return SetSignalValueInterface(nEquipID, 
		nSigType, nSigID, pMenuItem->vdData, 
		SERVICE_OF_USER_INTERFACE, 
		EQUIP_CTRL_SEND_URGENTLY,
		SET_TIME_OUT);

}

static int SetAlarmVoiceItem(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int nEquipID, nSigType, nSigID;

	nEquipID = DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);
	DXI_SPLIT_SIG_ID(SIG_ID_ALARM_VOICE_SET, nSigType, nSigID);

	return SetSignalValueInterface(nEquipID, 
		nSigType, nSigID, pMenuItem->vdData, 
		SERVICE_OF_USER_INTERFACE, 
		EQUIP_CTRL_SEND_URGENTLY,
		SET_TIME_OUT);	 
}

static int SetTimeZoneItem(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int nEquipID, nSigType, nSigID;

	nEquipID = DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP);
	DXI_SPLIT_SIG_ID(SIG_ID_TIME_ZONE_SET, nSigType, nSigID);

	return SetSignalValueInterface(nEquipID, 
		nSigType, nSigID, pMenuItem->vdData, 
		SERVICE_OF_USER_INTERFACE, 
		EQUIP_CTRL_SEND_URGENTLY,
		SET_TIME_OUT);

}


static int ReloadDefConfigGetPreInfo(MENU_ITEM* pMenuItem, 
									 DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									 int nGetType)
{
	//Has the same selection with clear his alarm item
	return GetClearHisAlarmItemPreInfo(pMenuItem, pDisValuePreInfo, nGetType);
}


static int ReloadDefConfigSetFunc(MENU_ITEM* pMenuItem)
{
	int iRet;

	ASSERT(pMenuItem);

	if (pMenuItem->vdData.enumValue == 0)//Only this selection
	{
		iRet = DoModal_AcknowlegeDlg_ID(
			HEADING_PROMPT_INFO_LANG_ID, 
			REBOOT_TO_VALIDATE_LANG_ID,
			-1,
			-1,
			PROMPT_INFO_TIMEOUT);
		if(ID_RETURN != iRet)
		{
			return ERR_LCD_OK;
		}


		//DoModal_AcknowlegeDlg_ID(
		//	APP_IS_LANG_ID,
		//	COPYING_FILE_LANG_ID,
		//	PLEASE_WAIT_LANG_ID,
		//	-1,
		//	1);

		//Reload default config
		if (DXI_ReloadDefaultConfig() == ERR_DXI_OK)
		{
			//DoModal_AcknowlegeDlg_INFO(
			//	"", 
   //             "SYS is Rebooting", 
			//	"",
			//	"",
			//	-1);
			PromptSysIsRebooting();
			Sleep(60*60*1000);
		}
		else
		{
			//DoModal_AcknowlegeDlg_INFO(
			//	"Reload", 
			//	"Default Config", 
			//	"Fail!!!!",
			//	"",
			//	5000);
			DoModal_AcknowlegeDlg_INFO_ByLang(
				"Fail!!!!", 
				"",
				"", 
				"",
				PROMPT_INFO_TIMEOUT,
				GetLangCode(LOCAL_LANGUAGE_FLAG),
				TRUE,
				FALSE);
		}
	}
	else
	{
		//by HULONGWEN
	}

	return ERR_LCD_OK;

}



static int SetYDN23SlefAddr(MENU_ITEM* pMenuItem)
{
	//char	strOut[256];

	if(GetYdnPriCfg(&g_YdnPriCfg))
	{
		return RET_INVALID_DATA;
	}
	else
	{
		/*sprintf(strOut, 
				"'Self Addr' is modified to %ul from %d.\n", 
				pMenuItem->vdData.ulValue,
				g_YdnPriCfg.byADR);

		LCD_LOG_OUT(APP_LOG_INFO, strOut);*/

		g_YdnPriCfg.byADR = pMenuItem->vdData.ulValue;
	}

	return SetYdnPriCfg(&g_YdnPriCfg, YDN_CFG_W_MODE | YDN_CFG_ADR);
}

#define	PORT_TYPE_ETHERNET	2
#define	PORT_TYPE_MODEM		1
#define	PORT_TYPE_RS232		0

static int SetYDN23PortType(MENU_ITEM* pMenuItem)
{
	//char	strOut[256];

	if(GetYdnPriCfg(&g_YdnPriCfg))
	{
		return RET_INVALID_DATA;
	}
	else
	{	
		if(g_YdnPriCfg.iMediaType == pMenuItem->vdData.ulValue)
		{
			return 0;
		}
		else
		{
			if(PORT_TYPE_ETHERNET == g_YdnPriCfg.iMediaType)
			{
				//sprintf(strOut, 
				//	"'Port Type' is modified to %ul from %d.\n", 
				//	pMenuItem->vdData.ulValue,
				//	g_YdnPriCfg.iMediaType);
				//LCD_LOG_OUT(APP_LOG_INFO, strOut);

				g_YdnPriCfg.iMediaType = pMenuItem->vdData.ulValue;

				//sprintf(strOut, 
				//	"'Port Param' is modified to %s.\n", 
				//	"9600,n,8,1");
				//LCD_LOG_OUT(APP_LOG_INFO, strOut);

				memcpy(g_YdnPriCfg.szCommPortParam, "9600,n,8,1", (1 + strlen("9600,n,8,1")));
			}
			else
			{
				if(PORT_TYPE_ETHERNET == pMenuItem->vdData.ulValue)
				{
					//sprintf(strOut, 
					//	"'Port Param' is modified to %s.\n", 
					//	"5050");
					//LCD_LOG_OUT(APP_LOG_INFO, strOut);
					memcpy(g_YdnPriCfg.szCommPortParam, "5050", (1 + strlen("5050")));
				}
				//sprintf(strOut, 
				//	"'Port Type' is modified to %ul from %d.\n", 
				//	pMenuItem->vdData.ulValue,
				//	g_YdnPriCfg.iMediaType);
				//LCD_LOG_OUT(APP_LOG_INFO, strOut);

				g_YdnPriCfg.iMediaType = pMenuItem->vdData.ulValue;
			}
		}
	}

	return SetYdnPriCfg(&g_YdnPriCfg, 
		YDN_CFG_W_MODE | YDN_CFG_MEDIA_TYPE | YDN_CFG_MEDIA_PORT_PARAM);
}


static int SetYDN23PortParam(MENU_ITEM* pMenuItem)
{
	char*	pParam[YDN23_PORT_PARAM_NUM] = 
	{
		"5050",
		"2400,n,8,1",
		"4800,n,8,1",
		"9600,n,8,1",
		"19200,n,8,1",
		"38400,n,8,1",
	};

	if(GetYdnPriCfg(&g_YdnPriCfg))
	{
		return RET_INVALID_DATA;
	}
	else
	{	
		if(PORT_TYPE_ETHERNET == g_YdnPriCfg.iMediaType)
		{
			if(pMenuItem->vdData.enumValue != 0) 
			{
				return RET_INVALID_DATA;			
			}
			else
			{
				memcpy(g_YdnPriCfg.szCommPortParam, "5050", (1 + strlen("5050")));
			}
		}
		else
		{
			if((pMenuItem->vdData.enumValue < 1) 
				|| (pMenuItem->vdData.enumValue > 5))
			{
				return RET_INVALID_DATA;			
			}
			else
			{
				memcpy(g_YdnPriCfg.szCommPortParam, 
					pParam[pMenuItem->vdData.enumValue],
					(1 + strlen(pParam[pMenuItem->vdData.enumValue])));
			}
		}
	}

	return SetYdnPriCfg(&g_YdnPriCfg, YDN_CFG_W_MODE | YDN_CFG_MEDIA_PORT_PARAM);

}


static int SetYDN23CallBackEnb(MENU_ITEM* pMenuItem)
{
	/*char	strOut[256];*/

	if(GetYdnPriCfg(&g_YdnPriCfg))
	{
		return RET_INVALID_DATA;
	}
	else
	{	
		if(g_YdnPriCfg.bReportInUse == pMenuItem->vdData.enumValue)
		{
			return 0;
		}
		else
		{
			/*sprintf(strOut, 
				"'Alarm Report Enabled' is modified to %d from %d.\n", 
				pMenuItem->vdData.enumValue,
				g_YdnPriCfg.bReportInUse);

			LCD_LOG_OUT(APP_LOG_MILESTONE, strOut);*/
			g_YdnPriCfg.bReportInUse = pMenuItem->vdData.enumValue;
		}
	}

	return SetYdnPriCfg(&g_YdnPriCfg, YDN_CFG_W_MODE | YDN_CFG_REPORT_IN_USE);
}


static int SetYDN23CallBackNum(MENU_ITEM* pMenuItem)
{
	if(GetYdnPriCfg(&g_YdnPriCfg))
	{
		return RET_INVALID_DATA;
	}
	else
	{	
		if(g_YdnPriCfg.iMaxAttempts == pMenuItem->vdData.ulValue)
		{
			return 0;
		}
		else
		{
			g_YdnPriCfg.iMaxAttempts = pMenuItem->vdData.ulValue;
		}
	}

	return SetYdnPriCfg(&g_YdnPriCfg, YDN_CFG_W_MODE | YDN_CFG_MAX_ATTEMPTS);
}



static int SetYDN23CallBackInterval(MENU_ITEM* pMenuItem)
{
	if(GetYdnPriCfg(&g_YdnPriCfg))
	{
		return RET_INVALID_DATA;
	}
	else
	{	
		if(g_YdnPriCfg.iAttemptElapse == (pMenuItem->vdData.ulValue * 1000))
		{
			return 0;
		}
		else
		{
			g_YdnPriCfg.iAttemptElapse = pMenuItem->vdData.ulValue * 1000;
		}
	}

	return SetYdnPriCfg(&g_YdnPriCfg, YDN_CFG_W_MODE | YDN_CFG_ATTEMPT_ELAPSE);
}

static BOOL LCD_GetRectHighValid(IN int iEquipID)
{
	SIG_BASIC_VALUE*		pSigValue;
	int iBufLen;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECT_HI_SN),
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (iError == ERR_DXI_OK)
	{
		 return SIG_VALUE_IS_CONFIGURED(pSigValue);
	}
	return FALSE;
 
}
static DWORD  LCD_GetRectHighSNValue(IN int iEquipID)
{
	SIG_BASIC_VALUE*		pSigValue = NULL;
	int iBufLen;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECT_HI_SN),
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (pSigValue &&(iError == ERR_DXI_OK))
	{
#ifdef _DEBUG
		if(pSigValue->varValue.ulValue == 0)
		{
			pSigValue->varValue.ulValue = 2;
		}
#endif
		return pSigValue->varValue.ulValue;

	}
	return 1;
}


/*==========================================================================*
 * FUNCTION : GetYdnPriCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT COMMON_CONFIG*  pYdnPriCfg : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-11-16 16:48
 *==========================================================================*/
static int GetYdnPriCfg(OUT YDN_COMMON_CONFIG* pYdnPriCfg)
{
    APP_SERVICE*	pYdnService = NULL;
    
	ASSERT(pYdnPriCfg);

	pYdnService = ServiceManager_GetService(SERVICE_GET_BY_LIB, "ydn23.so");

    if(pYdnService && pYdnService->pfnServiceConfig)
    {
		return pYdnService->pfnServiceConfig(pYdnService->hServiceThread,
					pYdnService->bServiceRunning,
					&pYdnService->args, 
					0x0,
					0,
					(void *)pYdnPriCfg);
	}

    return -1;
}


/*==========================================================================*
 * FUNCTION : SetYdnPriCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN COMMON_CONFIG*  pYdnPriCfg  : 
 *            IN int             iModifyType : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2006-11-16 16:48
 *==========================================================================*/
static int SetYdnPriCfg(IN YDN_COMMON_CONFIG* pYdnPriCfg, IN int iModifyType)
{


	//ASSERT(szBuffer);
	COMMON_CONFIG		stEEMCommonConfig ;
	APP_SERVICE		*stAppService = NULL;
	int			iEEMModifyType = 0;
	int			iReturn = 0;

	char			szTempUserName[40];
   
	printf("iModifyType = %d\n", iModifyType);
	if(iModifyType)
	{
   
		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,"eem_soc.so");
		if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		{
			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
									stAppService->bServiceRunning,
									&stAppService->args, 
									0x0,
									0,
								(void *)&stEEMCommonConfig);

			
			if(iReturn == ERR_SERVICE_CFG_OK)
			{
				stEEMCommonConfig.iProtocolType = 4;
				stEEMCommonConfig.iMediaType = 2;//tcp/ip
				strncpyz(stEEMCommonConfig.szCommPortParam,"2000",5);
				

				iEEMModifyType = iEEMModifyType | ESR_CFG_W_MODE;
				iEEMModifyType = iEEMModifyType | ESR_CFG_ALL | ESR_CFG_PROTOCOL_TYPE |ESR_CFG_MEDIA_TYPE | ESR_CFG_MEDIA_PORT_PARAM;
				iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
					stAppService->bServiceRunning,
					&stAppService->args, 
					iEEMModifyType,
					0,
					(void *)&stEEMCommonConfig);// == ERR_SERVICE_CFG_OK)
				printf("iReturn = %d\n", iReturn);
				if(iReturn != ERR_SERVICE_CFG_OK)
				{
				
					return iReturn;
				}
			}
			else
			{
				printf("ERR_SERVICE_CFG_OK = %d\n", iReturn);
				return iReturn;
			}

		}
	}


	
	APP_SERVICE*	pYdnService = NULL;

	ASSERT(pYdnPriCfg);

	if(iModifyType)
	{
		pYdnService = ServiceManager_GetService(SERVICE_GET_BY_LIB, "ydn23.so");

		if(pYdnService && pYdnService->pfnServiceConfig)
		{
			memcpy(pYdnPriCfg->cModifyUser, g_szCurUser, 1 + strlen(g_szCurUser));
			pYdnPriCfg->iProtocolType = 4; 
			iReturn =  pYdnService->pfnServiceConfig(pYdnService->hServiceThread,
							pYdnService->bServiceRunning,
							&pYdnService->args,
							iModifyType,
							0,
							(void *)pYdnPriCfg);
			printf("YDN return %d\n", iReturn);
			return iReturn;

		}
	}
	printf("-1\n");
	return -1;
}


static SIG_ENUM GetEnumByPortParm(const char* pPortParam)
{
	int		i;
	char*	pParam[YDN23_PORT_PARAM_NUM] = 
	{
		"5050",
		"2400,n,8,1",
		"4800,n,8,1",
		"9600,n,8,1",
		"19200,n,8,1",
		"38400,n,8,1",
	};

	for(i = 0; i < YDN23_PORT_PARAM_NUM; i++)
	{
		//TRACE("****** pParam[i] = %s ******\n", pParam[i]);
		if(!(memcmp(pParam[i], pPortParam, 5)))
		{
			//TRACE("*** pParam[i] = %s ***\n *** pPortParam = %s ***\n *** i = %d\n\n",
			//	pParam[i], pPortParam, i);
			return i;
		}
	}

	return 3;
}


static int ChangeHeightGetPreInfo(MENU_ITEM* pMenuItem, 
				DIS_VALUE_PRE_INFO* pDisValuePreInfo,
				int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	LANG_FILE*	pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	static LANG_TEXT*		s_ChangeHeightTextArray[2];

	//128*64
	s_ChangeHeightTextArray[0] = GetLCDLangText(
		CHANGE_LCD_HEIGHT_128_64_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//128*128
	s_ChangeHeightTextArray[1] = GetLCDLangText(
		CHANGE_LCD_HEIGHT_128_128_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);
	pDisValuePreInfo->pStateText = s_ChangeHeightTextArray;

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		pMenuItem->vdData.enumValue = SCREEN_HEIGHT / 64 - 1;	//0 or 1
	}
	else
	{
		;
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));

	return RET_VALID_DATA;
}


static int ChangeHeightSetFunc(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);
	int nHeight;

	if (pMenuItem->vdData.enumValue == 0)	//128*64
	{
		nHeight = 64;
		SetScreenHeight(nHeight);
	}
	else if(pMenuItem->vdData.enumValue == 1)	//128*64
	{
		nHeight = 128;
		SetScreenHeight(nHeight);
	}

	ClearScreen();

	//DEBUG_LCD_FILE_FUN_LINE_STRING("Do NOT write Env Var for debug");
	//return ERR_LCD_OK;

	//���浽���������У��Ա��´�����ʱʹ�ã��������������á�
	char caSysCmd[256];
	snprintf(caSysCmd, sizeof(caSysCmd), "%s %d %s %d",ENV_VAR_EXE_FILE, ENV_VAR_WRITE_FLAG, ENV_VAR_LCD_HEIGHT, nHeight / 64);
	//DEBUG_LCD_INFO_STRING("The command of SET LCD Rotation is ", caSysCmd);
	system(caSysCmd);

	return ERR_LCD_OK;
}

static int SetLcdRotationGetPreInfo(MENU_ITEM* pMenuItem, 
				DIS_VALUE_PRE_INFO* pDisValuePreInfo,
				int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	LANG_FILE*	pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	static LANG_TEXT*		s_SetLcdRotaionTextArray[4];
	//0
	s_SetLcdRotaionTextArray[0] = GetLCDLangText(
		SET_LCD_ROTATION_0_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//90
	s_SetLcdRotaionTextArray[1] = GetLCDLangText(
		SET_LCD_ROTATION_90_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//180
	s_SetLcdRotaionTextArray[2] = GetLCDLangText(
		SET_LCD_ROTATION_180_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//270
	s_SetLcdRotaionTextArray[3] = GetLCDLangText(
		SET_LCD_ROTATION_270_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);
	pDisValuePreInfo->pStateText = s_SetLcdRotaionTextArray;

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		pMenuItem->vdData.enumValue = GetLCDRotation() / 90;	//0 ~ 3
	}
	else
	{
		;
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));

	return RET_VALID_DATA;
	
}


static int SetLcdRotationSetFunc(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int	nReturn = ERR_LCD_OK;
	int nRotation = pMenuItem->vdData.enumValue * 90;

	//If the SCREEN_HEIGHT is 64, LCD Rotation can not be set
	if(!IS_HIGH_SCREEN)
	{
		nRotation = 0;
		nReturn = ERR_LCD_FAILURE;
	}

	int	iRoll;

	iRoll = nRotation/90;

	VAR_VALUE_EX		sigValue;

	//sigValue.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_USER_INTERFACE;
	sigValue.pszSenderName ="LCD_UI";
	sigValue.varValue.enumValue = iRoll;
	int	iRst;
	int	nEquipId = 1;
	int			nSignalType = 2;
	int			nSignalId = 186;

	iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
		nEquipId,
		DXI_MERGE_SIG_ID(nSignalType, nSignalId),
		sizeof(VAR_VALUE_EX),
		&sigValue,
		0);

	SetLCDRotation(nRotation);

	ClearScreen();

	//DEBUG_LCD_FILE_FUN_LINE_STRING("Do NOT write Env Var for debug");
	//return nReturn;

	//���浽���������У��Ա��´�����ʱʹ�ã��������������á�
	char caSysCmd[256];
	snprintf(caSysCmd, sizeof(caSysCmd), "%s %d %s %d",ENV_VAR_EXE_FILE, ENV_VAR_WRITE_FLAG, ENV_VAR_LCD_ROTATION, nRotation / 90);
	//DEBUG_LCD_INFO_STRING("The command of SET LCD Rotation is ", caSysCmd);
	system(caSysCmd);

	return nReturn;
}


static int DHCPGetPreInfo(MENU_ITEM* pMenuItem, 
				DIS_VALUE_PRE_INFO* pDisValuePreInfo,
				int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	int	nRetDataFlag = RET_VALID_DATA;


	LANG_FILE*	pLCDLangFile = GetLCDLangFile();
	ASSERT(pLCDLangFile);

	SELF_DEFINE_INFO*	pSelfDefineInfo;
	pSelfDefineInfo = (SELF_DEFINE_INFO*)(pMenuItem->pvItemData);
	ASSERT(pSelfDefineInfo);

	static LANG_TEXT*		s_DHCPTextArray[3];
	//Close
	s_DHCPTextArray[0] = GetLCDLangText(
		DHCP_CLOSE_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//Open
	s_DHCPTextArray[1] = GetLCDLangText(
		DHCP_OPEN_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	//Error
	s_DHCPTextArray[2] = GetLCDLangText(
		DHCP_ERROR_LANG_ID, 
		pLCDLangFile->iLangTextNum, 
		pLCDLangFile->pLangText);

	ASSIGN_DIS_VALUE_PRE_INFO(pDisValuePreInfo, pSelfDefineInfo);
	pDisValuePreInfo->pStateText = s_DHCPTextArray;

	if(NEED_TO_GET_ORIGIN_DATA(nGetType, pMenuItem->cMenuStatus))
	{
		int nDHCP = APP_DHCP_OFF;

		int nBufLen;

		if(DxiGetData(VAR_APP_DHCP_INFO,
			0,
			0,
			&nBufLen,
			&nDHCP,
			0) == ERR_DXI_OK)
		{
			//if(nDHCP != APP_DHCP_ON)
			//{
			//	nDHCP = APP_DHCP_OFF;
			//}
			pMenuItem->vdData.enumValue = nDHCP;
		}
		else
		{
			nRetDataFlag = RET_INVALID_DATA;
		}
	}
	else
	{
		;
	}

	memcpy(&(pDisValuePreInfo->vdData), 
		&(pMenuItem->vdData), 
		sizeof(VAR_VALUE));

	return nRetDataFlag;

}

static int DHCPSetFunc(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int nDHCP = pMenuItem->vdData.enumValue;

	int nRet = DxiSetData(VAR_APP_DHCP_INFO,
					0,
					0,
					sizeof(nDHCP),
					&nDHCP,
					0);

	int nBufLen;
	VAR_VALUE_EX	varValueEx;

	memset(&varValueEx, 0, sizeof(varValueEx));

	varValueEx.nSendDirectly = EQUIP_CTRL_SEND_URGENTLY;
	varValueEx.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	varValueEx.pszSenderName = "";

	if(DxiGetData(VAR_APP_DHCP_INFO,
		0,
		0,
		&nBufLen,
		&nDHCP,
		0) == ERR_DXI_OK)
	{
		varValueEx.varValue.enumValue = (nDHCP == APP_DCHP_ERR);

		DxiSetData(VAR_A_SIGNAL_VALUE,
			POWER_SYSTEM_EQUIP_ID,			
			DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 54),//54	DHCP Failure		
			sizeof(VAR_VALUE_EX),			
			&(varValueEx),			
			0);
	}

	return nRet;
}

static int DownloadConfigGetPreInfo(MENU_ITEM* pMenuItem, 
									DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	//Has the same selection with clear his alarm item
	return GetClearHisAlarmItemPreInfo(pMenuItem, pDisValuePreInfo, nGetType);

}

static int DownloadConfigSetFunc(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int	nReturn = ERR_LCD_OK;
	unsigned int nSysRet;

#define USB_MOUNT_DIR			"/usb/"
#define	USB_CONFIG_FILE_NAME1	"app_cfg.tar"
#define	USB_CONFIG_FILE_NAME2	"app_cfg.tar.gz"

	char szCommand[128];

	snprintf(szCommand, sizeof(szCommand), "/bin/mount /dev/sda1 %s", USB_MOUNT_DIR);

	nSysRet = system(szCommand);
	TRACE("Ret Value of system(mount) is %02X.\n", nSysRet);
	if(nSysRet >> 8 != 0)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_WARNING, 
			"Download Config File Failure! Maybe NO udisk inserted.\n");

		DoModal_AcknowlegeDlg_INFO(
			"",
			"Please insert",
			"    Udisk!",
			"",
			5000);

		return ERR_LCD_FAILURE;
	}

	char szUSBConfigFile[MAX_FILE_PATH];
	BOOL bFileExist = FALSE;

	if(snprintf(szUSBConfigFile, sizeof(szUSBConfigFile), "%s%s", USB_MOUNT_DIR, USB_CONFIG_FILE_NAME1)
		&& (access(szUSBConfigFile, F_OK) == 0))	//return Zero if SUCCESS (File is Exist)
	{
		snprintf(szCommand, sizeof(szCommand), "tar -xf %s -C /app/ \n", szUSBConfigFile);
		bFileExist = TRUE;
	}
	else if(snprintf(szUSBConfigFile, sizeof(szUSBConfigFile), "%s%s", USB_MOUNT_DIR, USB_CONFIG_FILE_NAME2)
		&& (access(szUSBConfigFile, F_OK) == 0))
	{
		snprintf(szCommand, sizeof(szCommand), "tar -xzf %s -C /app/ \n", szUSBConfigFile);
		bFileExist = TRUE;
	}

	if(bFileExist)
	{
		TRACE("Decompress cfg in USB cmd: %s", szCommand);

		nSysRet = system(szCommand);
		TRACE("Ret Value of system(tar) is %02X.\n", nSysRet);

		if(nSysRet >> 8 != 0)
		{
			nReturn = ERR_LCD_FAILURE;

			AppLogOut(LCD_UI_TASK, APP_LOG_WARNING, 
				"Download Config File Failure! Decompress error, the config file may be invalid.\n");
		}
		else
		{
			AppLogOut(LCD_UI_TASK, APP_LOG_WARNING, 
				"Download Config File by USB Success.\n");

			DoModal_AcknowlegeDlg_ID(
				HEADING_PROMPT_INFO_LANG_ID, 
				DOWNLOAD_COMPLETE_LANG_ID, 
				REBOOT_TO_VALIDATE_LANG_ID,
				-1, 
				2000);
		}
	}
	else	//Either USB_CONFIG_FILE_NAME1 or USB_CONFIG_FILE_NAME2 are NOT exist
	{
		DoModal_AcknowlegeDlg_INFO(
			"",
			"CanNOT Find File",
			USB_CONFIG_FILE_NAME1,
			USB_CONFIG_FILE_NAME2,
			5000);

		nReturn = ERR_LCD_FAILURE;

		AppLogOut(LCD_UI_TASK, APP_LOG_WARNING, 
			"Download Config File Failure! Can NOT find config file[%s] or [%s].\n", USB_CONFIG_FILE_NAME1, USB_CONFIG_FILE_NAME2);
	}


	snprintf(szCommand, sizeof(szCommand), "/bin/umount %s", USB_MOUNT_DIR);
	system(szCommand);

	return nReturn;

}

static int AutoConfigGetPreInfo(MENU_ITEM* pMenuItem, 
									DIS_VALUE_PRE_INFO* pDisValuePreInfo,
									int nGetType)
{
	ASSERT(pMenuItem);
	ASSERT(pDisValuePreInfo);

	//Has the same selection with clear his alarm item
	return GetClearHisAlarmItemPreInfo(pMenuItem, pDisValuePreInfo, nGetType);

}

static int AutoConfigSetFunc(MENU_ITEM* pMenuItem)
{
	ASSERT(pMenuItem);

	int	nReturn = ERR_LCD_OK;
	int nRet;

#if 0
	DEBUG_LCD_FILE_FUN_LINE_STRING("Do NOT execute Auto Config function for debug.");
	HANDLE hCmdThread = RunThread_Create("Auto Config",
		(RUN_THREAD_START_PROC)DIX_AutoConfigMain,
		(void*)NULL,
		(DWORD*)NULL,
		0);
#endif

	nRet = DoModal_AcknowlegeDlg_ID(
		HEADING_PROMPT_INFO_LANG_ID, 
		REBOOT_TO_VALIDATE_LANG_ID,
		-1,
		-1,
		PROMPT_INFO_TIMEOUT);
	if(ID_RETURN != nRet)
	{
		return ERR_LCD_OK;
	}

	DXI_AutoConfigWithReboot();

	PromptSysIsRebooting();
	Sleep(60*60*1000);

	return nReturn;

}

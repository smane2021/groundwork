/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_suicide_timer.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 16:22
 *  VERSION  : V1.00
 *  PURPOSE  : to kill a process itself after given seconds.
 *				!!!FOR WEB-CGI USE ONLY!!!
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <unistd.h>
#include <sys/time.h>			/* select */
#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>

#include "cgi_suicide_timer.h"


// for test only.
//#define _DEBUG_SUICIDE_TIMER

static int					s_bInitOK			  = 0;
static __sighandler_t		s_pOldTimerHandler	  = NULL;

static int					s_nSuicideWaitSeconds = 300;	// 5min.
static int					s_nElapsedSeconds     = 0;

static SUICIDE_CLEAR_PROC	s_pfnSuicideClearProc = NULL;
static void					*s_lpSuicideClearParam= NULL;

/*==========================================================================*
 * FUNCTION : SuicideTimer_AlarmHander
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN int     nSigNum : 
 *            siginfo_t  *extra  : 
 *            void       *cruft  : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 18:40
 *==========================================================================*/
static void SuicideTimer_AlarmHander(int nSigNum, siginfo_t *extra, void *cruft)
{
	UNUSED(nSigNum);
	UNUSED(extra);
	UNUSED(cruft);

	s_nElapsedSeconds += CGI_SUICIDE_TIMER_RESOLUTION;

#ifdef _DEBUG_SUICIDE_TIMER
	printf("Now is %d, elapsed %d\n",
		time(NULL), s_nElapsedSeconds);
#endif

	if (s_nElapsedSeconds >= s_nSuicideWaitSeconds)
	{
		int nRet = 0;

#ifdef _DEBUG
		FILE *fp = fopen("/var/suicided_cgi.txt", "a+t");
		if (fp != NULL)
		{
			time_t	tmNow = time(NULL);
			//fprintf( fp, "PID:%d suicided at %s",  // no CR.
			//	getpid(), ctime(&tmNow));
			fclose(fp);
		}
#endif

		if (s_pfnSuicideClearProc != NULL)
		{
			nRet = s_pfnSuicideClearProc(s_lpSuicideClearParam);
		}

#ifdef _DEBUG_SUICIDE_TIMER
		printf( "Exited in timer.\n");
#endif

		exit(nRet);	// error?
	}
}


/*==========================================================================*
 * FUNCTION : SuicideTimer_Init
 * PURPOSE  : Init the timer for process suicide after given seconds.
 *            The callback can be called before suicide.
 * CALLS    : exit.
 * CALLED BY: 
 * ARGUMENTS: IN int                 nSuicideWaitSeconds : wait seconds.
 *            IN SUICIDE_CLEAR_PROC  pfnClearProc        : clean proc
 *                 if pfnClearProc not NULL, the process will exit with
 *                 the return value of the pfnClearProc.
 *            IN void                *lpClearParam       : arg for clean proc
 * RETURN   : int : 0: for OK.
 * COMMENTS : This function ONLY be used ONCE in a process, because of using
 *            static variables and one timer.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 16:18
 *==========================================================================*/
int SuicideTimer_Init(IN int				nSuicideWaitSeconds, 
					  IN SUICIDE_CLEAR_PROC	pfnClearProc,
					  IN void				*lpClearParam)
{
	struct itimerval itv;
	int    nResult;

	s_nSuicideWaitSeconds = nSuicideWaitSeconds;
	s_pfnSuicideClearProc = pfnClearProc;
	s_lpSuicideClearParam = lpClearParam;

	if (s_bInitOK)
	{
		return 0;
	}

#ifdef _DEBUG_SUICIDE_TIMER
	printf("Process will suicide after %d seconds.\n",
		s_nSuicideWaitSeconds);
#endif

	// set timer alarm handler, and save the old one
	s_pOldTimerHandler = (__sighandler_t)signal(SIGALRM, 
		(__sighandler_t)SuicideTimer_AlarmHander);

	// start the timer
	/* Value to put into `it_value' when the timer expires.  */
	itv.it_interval.tv_sec	= CGI_SUICIDE_TIMER_RESOLUTION;
	itv.it_interval.tv_usec = 0;

	/* Time to the next timer expiration.  */
	itv.it_value.tv_sec		= itv.it_interval.tv_sec;
	itv.it_value.tv_usec	= itv.it_interval.tv_usec;

	nResult = setitimer( ITIMER_REAL, &itv, NULL );
	if( nResult != 0 )
	{
#ifdef _DEBUG_SUICIDE_TIMER
		perror( "setitimer( ITIMER_REAL, ...)" );
#endif
		return errno;
	}

	s_bInitOK = 1;

	return 0;
}


/*==========================================================================*
 * FUNCTION : SuicideTimer_Reset
 * PURPOSE  : reset the timer to 0. 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : CGI shall call SuicideTimer_Reset periodly if the CGI is doing
 *            a thing need the time than the nSuicideWaitSeconds.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 18:14
 *==========================================================================*/
void SuicideTimer_Reset(void)
{
	s_nElapsedSeconds = 0;
}


/*==========================================================================*
 * FUNCTION : SuicideTimer_Destroy
 * PURPOSE  : destroy the timer.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 21:04
 *==========================================================================*/
void SuicideTimer_Destroy(void)
{
	if (!s_bInitOK)
	{
		return;
	}

	setitimer( ITIMER_REAL, NULL, NULL ); 

	// set timer alarm handler, and save the old one
	signal(SIGALRM, 
		(__sighandler_t)s_pOldTimerHandler);

	s_bInitOK = 0;
}


#ifdef _DEBUG_SUICIDE_TIMER
int ClearProc(char *lpMsg)
{
	printf("%s: Cleaning....\n",
		lpMsg);
	return 1;
}

int main(int argc, char *argv[])
{
	int i;
	if (argc != 2)
	{
		printf("Usage: %s waittime\n", argv[0]);
		return 0;
	}

	i = atoi(argv[1]);
	if (i <= 0)
	{
		i = 60;
	}

	SuicideTimer_Init(i, 
		(SUICIDE_CLEAR_PROC)ClearProc,
		(void *)"Test");

	// the sleep will be interrupted, please use Sleep instead of sleep.
	printf("%d: Sleep 10, this sleep call maybe interrupted by timer.\n",
		time(NULL)); 
	sleep(10);	

	printf("%d: Press enter to quit now.\n", 
		time(NULL));
	getchar();
	
	SuicideTimer_Destroy();

	printf("%d: Sleep 10 after destorying the timer.\n",
		time(NULL)); 
	sleep(10);	


	printf("%d: Normally exit in main.\n",
		time(NULL));

	return 0;
}
#endif





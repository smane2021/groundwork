/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"
#include "cgi_pub.h"
#include "cgivars.h"


//#define WEB_DEFAULT_DIR			"/scup/www/html/cgi-bin/eng"		/*save english language html file*/
//#define WEB_LOCAL_DIR			"/scup/www/html/cgi-bin/loc"		/*save local language html file*/
//#define WEB_TEMPLATE_DIR		"/scup/www/html/cgi-bin/loc"		/*store template file*/
//
//#define WEB_CFG_NAME			"CfgName"
//#define WEB_CFG_VERSION			"CfgVersion"
//#define WEB_CFG_DATE			"CfgDate"
//#define WEB_CFG_DEFAULT			"default language"
//#define WEB_CFG_LOCAL			"Local language"
//#define WEB_CFG_ENGINEER		"CfgEngineer"
//#define WEB_CFG_DESCRIPTION		"CfgDescription"
//
//#define WEB_NUMBER_OF_CONFIG_INFORMATION	"[NUMBER_OF_CONFIG_INFORMATION]"
//#define WEB_CONFIG_INFORMATION				"[CONFIG_INFORMATION]"
//#define WEB_PAGES_NUMBER					"[NUM_OF_PAGES]"
//
//#define MAKE_PRIVATE_VAR_FIELD(var)				("[" var "]")
//
//#define	ERR_WEB_OK					0
//#define CONFIG_FILE_WEB_PRIVATE		"web/Web_Resource.res"
//
//#define SPLITTER					0x09
//
//
//typedef struct tagWebGeneralResourceInfo
//{
//	char		szResourceID[32];
//	char		*szLocal;
//	char		*szDefault;
//}WEB_GENERAL_RESOURCE_INFO;
//
//
//typedef struct tagWebTotalResourceInfo
//{
//	int							iNumber;
//	char						szFileName[64];
//	WEB_GENERAL_RESOURCE_INFO	*stWebPrivate;
//}WEB_PRIVATE_RESOURCE_INFO;
//
//typedef struct tagWebResourceInfo
//{
//	char	CfgName[32];
//	float	CfgVersion;
//	char	szCfgTime[32];
//	char	szCfgDefault[32];
//	char	szCfgLocal[32];
//	char	szCfgDescription[128];
//}WEB_RESOURCE_INFO;
//
//typedef struct tagWebHeadResourceInfo
//{
//	int					iNumber;
//	WEB_RESOURCE_INFO	*stWebHeadInfo;
//}WEB_HEAD_RESOURCE_INFO;
//
//
//
//WEB_PRIVATE_RESOURCE_INFO		*pWebCfgInfo = NULL;
//WEB_HEAD_RESOURCE_INFO			*pWebHeadInfo = NULL;
//
//int								iPagesNumber = 0;
//
//static const char szTemplateFile[128][64] = {"control_cgi.htm:Number","control_cgi.htm",
//				"data_sampler.htm:Number",		"data_sampler.htm",
//				"dialog.htm:Number",			"dialog.htm",
//				"editsignalname.js:Number",		"editsignalname.js",
//				"equip_data.htm:Number",		"equip_data.htm",
//				"j01_tree_maker.js:Number",		"j01_tree_maker.js",
//				"j02_tree_view.js:Number",		"j02_tree_view.js",
//				"j04_gfunc_def.js:Number",		"j04_gfunc_def.js",
//				"j05_signal_def.js:Number",		"j05_signal_def.js",
//				"j07_equiplist_def.js:Number",	"j07_equiplist_def.js",
//				"j30_table_script.js:Number",	"j30_table_script.js",
//				"j31_menu_script.js:Number",	"j31_menu_script.js",
//				"j31_table_script.js:Number",	"j31_table_script.js",
//				"j50_event_log.js:Number",		"j50_event_log.js",
//				"j76_device_def.htm:Number",	"j76_device_def.htm",
//				"j77_equiplist_def.htm:Number",	"j77_equiplist_def.htm",
//				"login.htm:Number",				"login.htm",
//				"p01_main_frame.htm:Number",	"p01_main_frame.htm",
//				"p02_tree_view.htm:Number",		"p02_tree_view.htm",
//				"p03_main_menu.htm:Number",		"p03_main_menu.htm",
//				"p04_main_title.htm:Number",	"p04_main_title.htm",
//				"p05_equip_sample.htm:Number",	"p05_equip_sample.htm",
//				"p06_equip_control.htm:Number",	"p06_equip_control.htm",
//				"p07_equip_setting.htm:Number",	"p07_equip_setting.htm",
//				"p08_alarm_frame.htm:Number",		"p08_alarm_frame.htm",
//				"p09_alarm_title.htm:Number",		"p09_alarm_title.htm",
//				"p10_alarm_show.htm:Number",		"p10_alarm_show.htm",
//				"p11_network_config.htm:Number",	"p11_network_config.htm",
//				"p12_nms_config.htm:Number",		"p12_nms_config.htm",
//				"p13_esr_config.htm:Number",		"p13_esr_config.htm",
//				"p14_user_config.htm:Number",		"p14_user_config.htm",
//				"p15_filemanage_menu.htm:Number",	"p15_filemanage_menu.htm",
//				"p16_filemanage_title.htm:Number",	"p16_filemanage_title.htm",
//				"p17_login_overtime.htm:Number",	"p17_login_overtime.htm.htm",
//				"p18_filemanage_upload.htm:Number",	"p18_filemanage_upload.htm",
//				"p19_time_config.htm:Number",		"p19_time_config.htm",
//				"p20_history_frame.htm:Number",		"p20_history_frame.htm",
//				"p21_history_title.htm:Number",		"p21_history_title.htm",
//				"p22_history_dataquery.htm:Number",	"p22_history_dataquery.htm",
//				"p23_history_alarmquery.htm:Number","p23_history_alarmquery.htm",
//				"p24_history_logquery.htm:Number",	"p24_history_logquery.htm",
//				"p25_online_frame.htm:Number",		"p25_online_frame.htm",
//				"p26_online_title.htm:Number",		"p26_online_title.htm",
//				"p27_online_modifysystem.htm:Number",		"p27_online_modifysystem.htm",
//				"p28_online_modifydevice.htm:Number",		"p28_online_modifydevice.htm",
//				"p29_online_modifyalarm.htm:Number",		"p29_online_modifyalarm.htm",
//				"p30_acu_signal_value.htm:Number",			"p30_acu_signal_value.htm",
//				"p31_close_system.htm:Number",				"p31_close_system.htm",
//				"p32_start_system.htm:Number",				"p32_start_system.htm",
//				"p33_replace_file.htm:Number",				"p33_replace_file.htm",
//				"p34_history_batterylogquery.htm:Number",	"p34_history_batterylogquery.htm",
//				"p47_web_title.htm:Number",					"p47_web_title.htm",
//				"p79_site_map.htm:Number",					"p79_site_map.htm",
//				"p80_status_view.htm:Number",				"p80_status_view.htm"};
//
//
///*function declare*/
//static int StartTransferFile(int *iQuitCommand);
//static float GetVersionOfWebPages(IN int iLanguage);
//static char *MakeFilePath(IN char *szFileName, IN int iLanguage);
//static int WEB_ReadConfig(void);
//static char *MakeVarField(char *szBuffer);
//static char *MakePrivateVarField(IN const char *szBuffer);
//static int LoadWebPrivateConfigProc(IN int iPages, void *pCfg);
//static int ParseWebResourceInfo(IN char *szBuf, OUT WEB_RESOURCE_INFO *pResourceInfo);
//static int ParsedWebLangFileTableProc(IN char *szBuf, 
//									  OUT WEB_GENERAL_RESOURCE_INFO *pStructData);
//static void	Web_freeMemory( IN void *ptr);
//
//static int StartTransferFile(int *iQuitCommand)
//{
//	/*transfer local file, transfer english file*/
//	int			i = 0, k = 0;
//	FILE		*fp1 ;
//	char		*pHtml1 = NULL, *pHtml2 = NULL; 
//	int			iError = 0;
//	int			iReturn1 = 0, iReturn2 = 0;
//	printf("starting to transfer WebPages!============1\n");
//	iError = WEB_ReadConfig();
//	if( iError == ERR_WEB_OK && pWebCfgInfo != NULL && pWebHeadInfo != NULL)
//	{
//
//		printf("starting to transfer WebPages!============2\n");
//		//printf("cfgversion:%f\n", pWebHeadInfo->stWebHeadInfo->CfgVersion);
//
//		/*�жϰ汾��*/
//        if(pWebHeadInfo->stWebHeadInfo->CfgVersion > GetVersionOfWebPages(0) ||
//				pWebHeadInfo->stWebHeadInfo->CfgVersion > GetVersionOfWebPages(1) )
//		{
//			
//			for(i = 0; i < iPagesNumber; i++)
//			{
//				char *szFile;
//				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 2);
//				iReturn1 = LoadHtmlFile(szFile, &pHtml2);
//				iReturn2 = LoadHtmlFile(szFile, &pHtml1);
//
//				DELETE(szFile);
//				szFile = NULL;
//
//				if((iReturn1 > 0) && (iReturn2 > 0))
//				{
//					for(k = 0; k < pWebCfgInfo[i].iNumber; i++)
//					{
//						ReplaceString(&pHtml1, 
//								MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID),
//								pWebCfgInfo[i].stWebPrivate->szDefault);
//						ReplaceString(&pHtml2, 
//								MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID), 
//								pWebCfgInfo[i].stWebPrivate[k].szLocal);
//					}
//				}
//				//Save File
//				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 0); 
//				if(szFile != NULL && (fp1 = fopen(szFile,"wb")) != NULL &&
//						 pHtml1 != NULL)
//				{
//
//					fwrite(pHtml1,strlen(pHtml1), 1, fp1);
//					fclose(fp1);
//				}
//
//				DELETE(szFile);
//				szFile = NULL;
//				
//				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 1); 
//				if(szFile != NULL && (fp1 = fopen(szFile,"wb")) != NULL &&
//						 pHtml1 != NULL)
//				{
//					fwrite(pHtml2,strlen(pHtml2), 1, fp1);
//					fclose(fp1);
//				}
//				DELETE(szFile);
//				szFile = NULL;
//				
//				if(pHtml1 != NULL)
//				{
//					DELETE(pHtml1);
//					pHtml1 = NULL;
//				}
//				
//				if(pHtml2 !=  NULL)
//				{
//					DELETE(pHtml2);
//					pHtml2 = NULL;
//				}
//
//				if(*iQuitCommand == SERVICE_STOP_RUNNING)
//				{
//					break;
//				}
//
//				RunThread_Heartbeat(RunThread_GetId(NULL));
//			}
//		}
//
//		
//		
//	}
//	/*free*/
//	WEB_PRIVATE_RESOURCE_INFO	*pDelete = (WEB_PRIVATE_RESOURCE_INFO *)pWebCfgInfo;
//	for(i = 0; i < iPagesNumber && pDelete != NULL; i++, pDelete++)
//	{
//		if(pDelete->stWebPrivate->szDefault != NULL)
//		{
//			DELETE(pDelete->stWebPrivate->szDefault);
//			pDelete->stWebPrivate->szDefault = NULL;
//		}
//
//		if(pDelete->stWebPrivate->szLocal != NULL)
//		{
//			DELETE(pDelete->stWebPrivate->szLocal);
//			pDelete->stWebPrivate->szLocal = NULL;
//		}
//		
//		if(pDelete->stWebPrivate != NULL)
//		{
//			DELETE(pDelete->stWebPrivate);
//			pDelete->stWebPrivate = NULL;
//		}
//
//	}
//	if(pWebCfgInfo != NULL)
//	{
//		DELETE(pWebCfgInfo);
//		pWebCfgInfo = NULL;
//	}
//	//Web_freeMemory(&pWebCfgInfo);
//	
//	if(pWebHeadInfo != NULL)
//	{
//		if(pWebHeadInfo->stWebHeadInfo != NULL)
//		{
//			DELETE(pWebHeadInfo->stWebHeadInfo);
//			pWebHeadInfo->stWebHeadInfo = NULL;
//		}
//		DELETE(pWebHeadInfo);
//		pWebHeadInfo = NULL;
//	}
//	return SERVICE_EXIT_OK;
//}
//static void	Web_freeMemory( IN void *ptr)
//{
//	ASSERT(ptr);
//	int							i = 0;
//	int							iNum = 0;
//	WEB_PRIVATE_RESOURCE_INFO	*pDelete = (WEB_PRIVATE_RESOURCE_INFO *)ptr;
//	iNum = pDelete->iNumber;
//	if(pDelete != NULL)
//	{
//		for(i = 0; i < iNum && pDelete != NULL; i++, pDelete++ )
//		{
//			DELETE(pDelete->stWebPrivate->szDefault);
//			pDelete->stWebPrivate->szDefault = NULL;
//
//			DELETE(pDelete->stWebPrivate->szLocal);
//			pDelete->stWebPrivate->szLocal = NULL;
//		}	
//	}
//
//	DELETE(ptr);
//	ptr = NULL;
//}
//
//static char *MakeVarField(char *szBuffer)
//{
//	char	*szReturnBuffer;
//	szReturnBuffer = NEW(char, 128);
//
//	if(szReturnBuffer != NULL)
//	{
//		sprintf(szReturnBuffer, "/*[%s]*/", szBuffer);
//		return szReturnBuffer;
//	}
//	return NULL;
//}
//
//static float GetVersionOfWebPages(IN int iLanguage)
//{
//#define WEB_PAGES_VERSION_FILE		"/scup/config/private/web/cfg_version.cfg"//"/home/ygx/cfg/cfg_version.cfg"
//#define WEB_PAGES_VERSION			"[cfg_version]"
//
//	FILE	*fp = NULL;
//	char	szBuffer[128];
//	char	*pSearchValue = NULL;
//	char	szVersion[32];
//	printf("%s", "GetVersionOfWebPages");
//	if(iLanguage ==0)
//	{
//
//		//if((fp = fopen(MakeFilePath(WEB_PAGES_VERSION_FILE, 0), "r")) != NULL)
//		if((fp = fopen(WEB_PAGES_VERSION_FILE, "r")) != NULL)
//		{
//			while(!feof(fp))
//			{
//				fgets(szBuffer, sizeof(szBuffer), fp);
//				pSearchValue = strstr(szBuffer, WEB_PAGES_VERSION);
//				if(pSearchValue != NULL)
//				{
//					pSearchValue = strchr(szBuffer,':');
//					strncpyz(szVersion,pSearchValue + 1, sizeof(szVersion));
//					printf("GetVersionOfWebPages : %s\n", szVersion);
//					return atof(szVersion);
//
//				}
//			}
//		}
//	}
//	else
//	{
//		//if((fp = fopen(MakeFilePath(WEB_PAGES_VERSION_FILE, 1), "r")) != NULL)
//		if((fp = fopen(WEB_PAGES_VERSION_FILE, "r")) != NULL)
//		{
//			while(!feof(fp))
//			{
//				fgets(szBuffer, sizeof(szBuffer), fp);
//				pSearchValue = strstr(szBuffer, WEB_PAGES_VERSION);
//				if(pSearchValue != NULL)
//				{
//					pSearchValue = strchr(szBuffer,':');
//					strncpyz(szVersion,pSearchValue + 1, sizeof(szVersion));
//					printf("GetVersionOfWebPages : %s\n", szVersion);
//					return atof(szVersion);
//
//				}
//			}
//		}
//	}
//	printf("GetVersionOfWebPages : %d\n", 0);
//	return 0.0;
//}
//static char *MakeFilePath(IN char *szFileName, IN int iLanguage)
//{
//	char			*szReturnFile;
//
//	ASSERT(szFileName);
//	
//	szReturnFile = NEW(char, 128);
//	if(szReturnFile == NULL)
//	{
//		return NULL;
//	}
//
//	if(iLanguage ==0)
//	{
//		
//		sprintf(szReturnFile,"%s/%s", WEB_DEFAULT_DIR, szFileName);
//	}
//	else if(iLanguage == 1)
//	{
//		sprintf(szReturnFile,"%s/%s", WEB_LOCAL_DIR, szFileName);
//	}
//	else
//	{
//		sprintf(szReturnFile,"%s/%s", WEB_TEMPLATE_DIR, szFileName);
//	}
//	return szReturnFile;
//}
//
//
//static int WEB_ReadConfig(void)
//{
//	int		iRst;
//	void	*pProf = NULL;  
//	FILE	*pFile = NULL;
//	char	*szInFile = NULL;
//	size_t	ulFileLen;
//	
//
//	char szCfgFileName[MAX_FILE_PATH]; 
//
//	Cfg_GetFullConfigPath(CONFIG_FILE_WEB_PRIVATE, szCfgFileName, MAX_FILE_PATH);
//	TRACE("szCfgFileName : %s\n", szCfgFileName);
//	/*Debug*/	
//	//printf("%s", "Initialize, Into WEB_ReadConfig!");
//	//sprintf(szCfgFileName,"%s","/scup/config/private/web/Source.cfg");//"/home/ygx/cfg/Source.cfg");
//	/*End Debug*/
//
//	/* open file */
//	pFile = fopen(szCfgFileName, "r");
//	if (pFile == NULL)
//	{		
//		return FALSE;
//	}
//
//	ulFileLen = GetFileLength(pFile);
//
//	szInFile = NEW(char, ulFileLen + 1);
//	if (szInFile == NULL)
//	{
//		fclose(pFile);
//		return FALSE;
//	}
//
//	/* read file */
//	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
//	fclose(pFile);
//
//	if (ulFileLen == 0) 
//	{
//		/* clear the memory */
//		DELETE(szInFile);
//
//		return FALSE;
//	}
//	szInFile[ulFileLen] = '\0';  /* end with NULL */
//
//	/* create SProfile */
//	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);
//	
//	if (pProf == NULL)
//	{
//		DELETE(szInFile);
//		return FALSE;
//	}
//	//1.Read Number pf pages
//	iRst = Cfg_ProfileGetInt(pProf,
//							WEB_PAGES_NUMBER, 
//							&iPagesNumber); 
//	if (iRst != 1)
//	{
//		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
//				"There are no pages number in Resource.cfg.");
//	}
//
//	//2.Read current all command content and command numbers
//    iRst = LoadWebPrivateConfigProc(iPagesNumber, pProf);
//	
//	//printf("%d%s\n", pWebHeadInfo->iNumber, pWebHeadInfo->stWebHeadInfo->CfgName);
//
//	if (iRst != ERR_WEB_OK)
//	{
//		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
//				"Fail to get resource table.");
//
//		DELETE(pProf); 
//		DELETE(szInFile);
//		return iRst;
//	}
//
//
//   	DELETE(pProf); 
//	DELETE(szInFile);
//	return ERR_WEB_OK;
//}
//
//
//static int LoadWebPrivateConfigProc(IN int iPages, void *pCfg)
//{
// 
//	int			i = 0, k = 0;
//	char		*szNumber = NULL, *szFile = NULL;
//	//Has two tables
//	CONFIG_TABLE_LOADER loader[iPages + 1];
//	
//	pWebHeadInfo = NEW(WEB_HEAD_RESOURCE_INFO, 1);
//	
//	//loader		= NEW(CONFIG_TABLE_LOADER,  2);
//	pWebCfgInfo	= NEW(WEB_PRIVATE_RESOURCE_INFO, iPages);
//	//ASSERT(pWebCfgInfo);
//	//ASSERT(loader);
//	//ASSERT(pCfg);
//
//
//	/* load configuration information*/
//	DEF_LOADER_ITEM(&loader[0] ,
//		WEB_NUMBER_OF_CONFIG_INFORMATION,
//		&(pWebHeadInfo->iNumber), 
//		WEB_CONFIG_INFORMATION, 
//		&(pWebHeadInfo->stWebHeadInfo), 
//		ParseWebResourceInfo);
//
//
//	/* load Web language file table */
//
//	for(i = 1; i < iPages + 2; i++)
//	{
//		strncpyz(pWebCfgInfo[i-1].szFileName,szTemplateFile[k +1] , sizeof(pWebCfgInfo[i-1].szFileName));
//		szNumber = MakePrivateVarField(szTemplateFile[k++]);
//		szFile	 = MakePrivateVarField(szTemplateFile[k++]);
//		DEF_LOADER_ITEM(&loader[i],
//			szNumber,
//			&(pWebCfgInfo[i-1].iNumber), 
//			szFile,
//			&(pWebCfgInfo[i-1].stWebPrivate), 
//			ParsedWebLangFileTableProc);
//
//		DELETE(szNumber);
//		szNumber = NULL;
//		DELETE(szFile);
//		szFile = NULL;
//	}
//			 
//	if (Cfg_LoadTables(pCfg, 1 + iPages, loader) != ERR_CFG_OK)
//	{
//		printf("Fail to Cfg_LoadTables\n");
//		return ERR_LCD_FAILURE;
//	}
//	
//	
//	
//	return ERR_CFG_OK;
//}
//
//static char *MakePrivateVarField(IN const char *szBuffer)
//{
//	char	*szReturnBuffer = NULL;
//	szReturnBuffer = NEW(char, 128);
//
//	if(szReturnBuffer != NULL)
//	{
//		sprintf(szReturnBuffer, "[%s]", szBuffer);
//		return szReturnBuffer;
//	}
//	return NULL;
//}
//static int ParseWebResourceInfo(IN char *szBuf, OUT WEB_RESOURCE_INFO *pResourceInfo)
//{
//	char *pField = NULL;
//
//
//	ASSERT(szBuf);
//	ASSERT(pResourceInfo);
//
//	/* CfgName */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;    
//	}
//	strncpyz(pResourceInfo->CfgName, pField, sizeof(pResourceInfo->CfgName));
//
//	/*CfgVersion*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;  
//	}
//	pResourceInfo->CfgVersion = atof(pField);
//
//	/*CfgTime*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;    
//	}
//	strncpyz(pResourceInfo->szCfgTime, pField, sizeof(pResourceInfo->szCfgTime));
//
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;   
//	}
//	strncpyz(pResourceInfo->szCfgDefault, pField, sizeof(pResourceInfo->szCfgDefault));
//
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;  
//	}
//	strncpyz(pResourceInfo->szCfgLocal, pField, sizeof(pResourceInfo->szCfgLocal));
//
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1; 
//	}
//	strncpyz(pResourceInfo->szCfgDescription, pField, sizeof(pResourceInfo->szCfgDescription));
//
//	return ERR_WEB_OK;
//}
//
//
//static int ParsedWebLangFileTableProc(IN char *szBuf, 
//									  OUT WEB_GENERAL_RESOURCE_INFO *pStructData)
//{
//	char *pField = NULL;
//	int		iMaxLen = 0;
//	/* used as buffer */
//
//	ASSERT(szBuf);
//	ASSERT(pStructData);
//	
//	//TRACE("*******szBuf : %s\n", szBuf);
//	/*0.Jump sequence ID*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//
//	/* 1.RES ID field */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;    
//	}
//	//printf("pStructData->szResourceID : %s\n", pField);
//	strncpyz(pStructData->szResourceID, pField, sizeof(pStructData->szResourceID));
//	
//	/*2.iMaxFullLen*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;    
//	}
//	iMaxLen = atoi(pField);
//
//	/*3.iMaxAbbrLen*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
//		return 1;    
//	}
//
// 	/* 4.English full language */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
// 		return 2;    
//	}
//	pStructData->szDefault = NEW(char, iMaxLen + 1);
//	//printf("pStructData->szDefault : %s\n", pField);
//	strncpyz(pStructData->szDefault, pField, iMaxLen + 1);
//
//	/*5.English abbr language*/
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
// 		return 2;    
//	}
//
//	/* 6.local full language */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
// 		return 3;    
//	}
//	pStructData->szLocal = NEW(char, iMaxLen + 1);
//    //printf("pStructData->szLocal : %s\n", pField);
//	strncpyz(pStructData->szLocal, pField, iMaxLen + 1);
//
//	/*Jump 7.local abbr language */
//	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
//	if (pField == NULL)
//	{
// 		return 3;    
//	}
//	/*Jump 8.description */
//
//	return ERR_WEB_OK;
//}
//
//

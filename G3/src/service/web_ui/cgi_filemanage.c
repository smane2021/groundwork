/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : cgi_filemanage.c
*  CREATOR  : Yang Guoxin              DATE: 2004-12-07 14:47
*  VERSION  : V1.00
*  PURPOSE  :
*
*  HISTORY  :
*
*==========================================================================*/
#include "stdsys.h"  
#include "public.h"
#include "pubfunc.h"
#include <sys/dir.h> 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "cgi_pub.h"
#include "cgivars.h"

#define WDOG_MAJOR			241 
#define IOCTL_EN_WD	     _IOW(WDOG_MAJOR, 1, unsigned long)
#define IOCTL_DIS_WD      _IOW(WDOG_MAJOR, 2, unsigned long)
#define IOCTL_FEED_WD    _IOW(WDOG_MAJOR, 3, unsigned long)
#define IOCTL_SET_TIMER    _IOW(WDOG_MAJOR, 4, unsigned long)

/*for upload file*/
#define CGI_CRLF					"\r\n"
#define HTTP_GET					"GET"
#define HTTP_POST					"POST"
#define X_CONTENT_TYPE				"application/x-www-form-urlencoded"
#define M_CONTENT_TYPE				"multipart/form-data"


/*for cgi parameter*/
#define	WEB_CGI_GETFILELIST			"_getfilelist"
#define	WEB_CGI_CLOSE_ACU			"_close_acu"
#define WEB_CGI_START_ACU			"_start_acu"
#define WEB_CGI_REPLACE_FILE		"_replace_file"		//Replace file
#define REPLACE_FILE_TIPS			"replace_file_tips"
#define WEB_CGI_REPLACED_FILE_NAME	"_replaced_file_name"    //Replaced file name
#define WEB_CGI_REPLACE_FILE_NAME	"_replace_file_name"
#define	WEB_CGI_UPLOAD_FILE			"_upload_file"
#define WEB_CGI_CFG_FILE            "_upload_cfg"
#define WEB_CGI_LANG_FILE           "_upload_lang"

#define CLIENT_CLOSE_ACU			1
#define CLIENT_START_ACU			2
#define CLIENT_GETFILELIST			3
#define CLIENT_UPLOAD_FILE			4
#define CLIENT_REPLACE_FILE			5
#define UPLOAD_CFG_FILE             6
#define UPLOAD_LANG_FILE            7




#define WEB_UPLOAD_CONFIG_TAR		0
#define WEB_UPLOAD_LANG_TAR			1
#define WEB_UPLOAD_PROGRAM_TAR		2

#define WEB_UPLOAD_PROGRAM_PATH		"/app/service"
#define WEB_UPLOAD_LANG_PATH		"/app/config/lang"
#define WEB_UPLOAD_CONFIG_PATH		"/app/config"

#define LOC_FILE_PATH				"/app/www/html/cgi-bin/loc"
#define ENG_FILE_PATH				"/app/www/html/cgi-bin/eng"
#define VAR_PATH					"/var"
#define OBJECT_PATH					"/app"
#define CLOSE_ACU_SYSTEM_FILE		"p31_close_system.htm"
#define START_ACU_SYSTEM_FILE		"p33_replace_file.htm"		
#define REPLACE_ACU_FILE			"p33_replace_file.htm"

#define WEB_SETTINGPARAM_FILE_NAME			"SettingParam.run"

//#define ACU_DOWNLOAD_FILE			"p17_filemanage_download.htm"
#define ACU_UPLOAD_FILE				"p18_filemanage_upload.htm"
#define ACU_FILE_LIST				"file_list"

#define ACU_RETURN_RESULT			"CONTROL_RESULT"

#define WEB_ENG_FILE_PATH			"/app/www/html/cgi-bin/eng"
#define WEB_LOC_FILE_PATH			"/app/www/html/cgi-bin/loc"

#define WEB_ZIP_CFG_LANG			"app_lang.tar"//.gz
#define WEB_ZIP_CFG					"app_cfg.tar"//.gz

/*General*/
#define	FILE_NO_AUTHORITY				"3"
#define FAIL_TO_COMM_WITH_ACU		"4"

/*Used to close ACU*/
#define SUCCESS_TO_CLOSE_ACU		"1"
#define FAIL_TO_CLOSE_ACU			"2"

/*Used to start ACU*/
#define SUCCESS_TO_START_ACU		"1"
#define FAIL_TO_START_ACU			"2"

#define MAX_VAR_DIR_SIZE			9437184//6144000

#define TABSPACES					8 

#define PREFIX_DOWNLOAD_FILE		"download_"

//Move to new directory,because the software package is too large(> 6M)
#define NEW_DOWNLOAD_DIRECTORY		"/usb"

#define WEB_PLC_PAGE    "p40_cfg_plc_Popup.htm"////Added by wj 20070208
#define WEB_CFG_PATH				"config/solution/ config/standard/ config/private/ config/run/"
#define WEB_LANG_PATH				"config/lang"
#define WEB_TAR_GZ  "*.tar.gz"
#define WEB_TAR     "*.tar"

static unsigned char		*HTTPBuffer = NULL;
static unsigned char		*CONTENT_TYPE = NULL;
static unsigned char		*CONTENT_LENGTH = NULL;

static int Web_FLE_getCGIParam(OUT int *iReturnControlType, OUT char **szReturnSessionID, 
							   OUT int *iReturnLanguage, OUT char **szOutSrcFile, 
							   OUT char **szOutDesFile, OUT char **szOutFileName, OUT off_t *file_size);

//static char *Web_FLE_MakeFilesListBuffer(void);
static off_t GetListName(char *name);
static off_t list(char *name);
static char *Web_FLE_GetFileName(IN char *szHttp, IN const char *szFindName);
static int Web_Replace_File(IN char *szFileName);
static void Web_Remove_File(IN char *szFileName);
static void Web_Zip_File(void);
static void Web_Del_Zip_File(void);////Added by wj 20070208

static int Web_FLE_ModifyTime(IN char *szFileName);
static void Web_CGI_PostNoAuthority(IN int iModifyType, IN int iLanguage);
/***************queue start***************/
typedef struct tagUploadQueue
{
	unsigned char type;
	unsigned char name[255];
	unsigned char *value;
	unsigned char extend[255];
	unsigned long size;
	struct tagUploadQueue *next;
}UploadQueue;

void push(UploadQueue **pQueue, unsigned char type, unsigned char *name, 
		  unsigned char *value, unsigned char *extend, unsigned long size);
void pop(UploadQueue **pQueue);

int		IsEmpty(UploadQueue *pQueue);
static off_t PrintQueue(UploadQueue *pQueue);


UploadQueue		*pUploadQueue = NULL;
static int		serial = 0;
/***************queue end***************/
//void InitHTTPEnv();
char *Request(IN char *key,OUT char *szOut);
/***********http upload start**********/

int			Web_FLE_GetBoundary(char *boundary);
int			Web_FLE_GetFormType(char *szIn);
void		Web_FLE_GetFormName(char *szIn, char *szOut);
void		Web_FLE_GetFileExtend(char *szIn, char *szOut);
int			Web_FLE_UImport(IN char *szIn, IN unsigned long ulS, IN char  *key);
UploadQueue *Web_FLE_UExport(UploadQueue **pQueue, char *name);
int			Web_FLE_SaveToFile(char *pszPath, unsigned char *szIn, unsigned int uiLen);
/***********http upload end**********/
void Web_FLE_BinToHex(unsigned char *szIn, char *szOut, int sl);
unsigned long Web_FLE_substring(size_t iPos, char *szIn, unsigned int iDataLen, char *szKey);
static int Web_FLE_CloseACU(void);
static int Web_Get_SwichState(void);
static int Web_FLE_StartACU(void);
static char *Web_FLE_GetFileFromPath(char *szSrcFile);
//static int Web_FLE_replaceFile(IN  char *szDstFile, IN  char *szSrcFile);
static char **getPOSTvarsA(unsigned char **HTTPBuffer) ;

char	*szMatchFileName = NULL;
/*for download file*/
#define	WORK_DIR_PATH				"/app/"
#define GET_DIR_PATH_SUCCESS		0

void Web_FLE_BinToHex(unsigned char *szIn, char *szOut, int sl)
{
	int i;
	*szOut = 0x0;
	for (i = 0; i < sl; i++)
	{
		sprintf(szOut+strlen(szOut), "%2.2X", szIn[i]);
	}
}



unsigned long Web_FLE_substring(size_t iPos, char *szIn, unsigned int iDataLen, char *szKey)
{
	unsigned long i;
	int iKey = (int)strlen(szKey);

	for (i = iPos; i < iDataLen; i++)
	{
		if (!strncmp(&szIn[i], szKey, (size_t)iKey))
		{
			return i;
		}
	}
	return (unsigned long)-1L;
}


char szPathList[10240];

int main(int argc, char *argv[])
{
#define WEB_FILE_ENG_PATH		"/app/www/html/cgi-bin/eng/p31_close_system.htm"
#define WEB_FILE_LOC_PATH		"/app/www/html/cgi-bin/loc/p31_close_system.htm"

	UNUSED(argc);
	UNUSED(argv);

	int			iReturnControlType = 0, iReturnLanguage = 0;
	char		szFilePath[64];
	int			iLen = 0;
	char		*szHtml = NULL;
	char		szFileTips[128];
	char		*szSrcFile = NULL, *szDesFile = NULL;
	char		*szReturnSessionID = NULL;
	//char		*szAuthority = NULL;
	//BOOL		bReturn = FALSE;
	char		*szFileName = NULL;
	off_t		file_size = 0;
	int			iReturn = 0;
	int			iLanguage = 0, iAuthority = 0;
	//char		szVARFile[64];
#define WEB_CONFIG_PATH			"/app/config"
#define WEB_SAMPLER_PATH		"/app/sampler"

	printf("Content-type:text/html\n\n");
	//GetListName(WEB_CONFIG_PATH);
	//GetListName(WEB_SAMPLER_PATH);


	if((iReturn = Web_FLE_getCGIParam(&iReturnControlType, 
		&szReturnSessionID, 
		&iReturnLanguage,
		&szSrcFile, 
		&szDesFile, 
		&szFileName, 
		&file_size)) == TRUE)
	{
		/*language type*/

		

		if(iReturnLanguage > 0)
		{
			iLanguage =	LOCAL_LANGUAGE_NAME;
		}
		else
		{
			iLanguage = ENGLISH_LANGUAGE_NAME;
		}


		switch(iReturnControlType)
		{


		case CLIENT_CLOSE_ACU:
			{
				if(iReturnLanguage > 0)//(iLanguage ==	LOCAL_LANGUAGE_NAME)
				{
					sprintf(szFilePath, "%s/%s", WEB_LOC_FILE_PATH, CLOSE_ACU_SYSTEM_FILE);
				}
				else
				{
					sprintf(szFilePath, "%s/%s", WEB_ENG_FILE_PATH, CLOSE_ACU_SYSTEM_FILE);
				}

				iAuthority = start_session(Web_RemoveWhiteSpace(szReturnSessionID));
				if(iAuthority < 0)			//iAuthority < 0 , it is the session fault,
					//not the authority fault

				{
					print_session_error(iAuthority,iLanguage);
					return FALSE;
				}
				else if(iAuthority < 3)
				{
					Web_CGI_PostNoAuthority(iReturnControlType, iLanguage);
					return 0;
				}
				if(Web_FLE_CloseACU() == TRUE)
				{
					//Judge authority
					sleep(5);
					
					//printf("Content-type:text/html\n\n");

					if(LoadHtmlFile(szFilePath, &szHtml) > 0)
					{
						ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT), "1");
					}

					//Web_Zip_File();//removed by wj 20070209
				}
				else
				{

					if(LoadHtmlFile(szFilePath, &szHtml) > 0)
					{
						ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT), "2");
					}
				}
				break;
			}
		case CLIENT_START_ACU:
			{
				//iLen += sprintf(szFilePath + iLen, "/%s", START_ACU_SYSTEM_FILE);
				/*	if(iLanguage ==	LOCAL_LANGUAGE_NAME)
				{
				sprintf(szFilePath + iLen, "%s/%s", WEB_LOC_FILE_PATH, START_ACU_SYSTEM_FILE);
				}
				else
				{
				sprintf(szFilePath + iLen, "%s/%s", WEB_ENG_FILE_PATH, START_ACU_SYSTEM_FILE);
				}

				if(LoadHtmlFile(szFilePath, &szHtml) > 0)
				{
				ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT), "4");
				}	

				if(szHtml != NULL)
				{
				printf("%s",szHtml);
				DELETE(szHtml);
				szHtml = NULL;
				}*/

				Web_FLE_StartACU();
				break;
			}
		case CLIENT_UPLOAD_FILE:
			{

				if(iReturnLanguage > 0)//(iLanguage ==	LOCAL_LANGUAGE_NAME)
				{
					sprintf(szFilePath, "%s/%s", WEB_LOC_FILE_PATH, REPLACE_ACU_FILE);
				}
				else
				{
					sprintf(szFilePath, "%s/%s", WEB_ENG_FILE_PATH, REPLACE_ACU_FILE);
				}



				if(LoadHtmlFile(szFilePath, &szHtml) > 0)
				{
					if(Web_Get_SwichState() == 3)
					{
						ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT), 
							"12");//Fail hardware proctect

					}
					else if(Web_Get_SwichState() == 2)
					{
						ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT), 
							"2");//Fail

					}
					else
					{
						char *szFileNameTemp = Web_FLE_GetFileFromPath(szMatchFileName);

						char szFileNameTotal[128];
						sprintf(szFileNameTotal, "%s%s", PREFIX_DOWNLOAD_FILE, szFileNameTemp);

						FILE *fp1 = fopen("/var/1.htm","w");
						fwrite(szFileNameTemp,strlen(szFileNameTemp), 1, fp1);
						fclose(fp1);

						int iReturnValue = Web_Replace_File(szFileNameTotal);
						//delete
						Web_Remove_File(szFileNameTotal);

						if(iReturnValue == TRUE)
						{
							ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT),
								"1");
						}
						else
						{
							ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT), 
								"2");//Fail
						}

						

					}

					DELETE(szFileName);
					szFileName = NULL;
					
				}

				break;
			}
		case UPLOAD_CFG_FILE:  //Added by wj 20070209
			{

				Web_Del_Zip_File();

				char szDelRun[256];
#define CFG_RUN_FILE    "AcuLastSolution.run"

				char szZip[256];

				memset(szDelRun, 0, sizeof(szDelRun));
				sprintf(szDelRun, "mv -f /app/config/run/%s /var/%s", CFG_RUN_FILE,CFG_RUN_FILE);
				system(szDelRun);

				memset(szZip, 0, sizeof(szZip));
				//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_CFG_PATH,WEB_ZIP_CFG_LANG);
				sprintf(szZip, "cd /app;tar -cf /var/%s %s", WEB_ZIP_CFG, WEB_CFG_PATH);
				system(szZip);

				memset(szDelRun, 0, sizeof(szDelRun));
				sprintf(szDelRun, "mv -f /var/%s /app/config/run/%s", CFG_RUN_FILE,CFG_RUN_FILE);
				system(szDelRun);


				if(iReturnLanguage > 0)//(iLanguage ==	LOCAL_LANGUAGE_NAME)
				{
					sprintf(szFilePath, "%s/%s", WEB_LOC_FILE_PATH, REPLACE_ACU_FILE);
				}
				else
				{
					sprintf(szFilePath, "%s/%s", WEB_ENG_FILE_PATH, REPLACE_ACU_FILE);
				}



				if(LoadHtmlFile(szFilePath, &szHtml) > 0)
				{

					ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT),
						"10");
				}
				break;
			}

		case UPLOAD_LANG_FILE:  //Added by wj 20070209
			{

				Web_Del_Zip_File();
				char szZip[256];

				memset(szZip, 0, sizeof(szZip));
				//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
				sprintf(szZip, "cd /app;tar -cf /var/%s %s", WEB_ZIP_CFG_LANG, WEB_LANG_PATH);
				system(szZip);

				if(iReturnLanguage > 0)//(iLanguage ==	LOCAL_LANGUAGE_NAME)
				{
					sprintf(szFilePath, "%s/%s", WEB_LOC_FILE_PATH, REPLACE_ACU_FILE);
				}
				else
				{
					sprintf(szFilePath, "%s/%s", WEB_ENG_FILE_PATH, REPLACE_ACU_FILE);
				}



				if(LoadHtmlFile(szFilePath, &szHtml) > 0)
				{

					ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT),
						"11");
				}
				break;

			}
		}


		if(szHtml != NULL)
		{

			printf("%s",szHtml);
			DELETE(szHtml);
			szHtml = NULL;
		} 

	}
	else if(iReturn == ERR_FILE_NO_SPACE)
	{
		/*
		if(iReturnLanguage > 0)
		{
		iLen += sprintf(szFilePath, "%s", LOC_FILE_PATH);
		}
		else
		{
		iLen += sprintf(szFilePath, "%s", ENG_FILE_PATH);
		}
		sprintf(szFilePath + iLen, "/%s", ACU_UPLOAD_FILE);
		if(LoadHtmlFile(szFilePath, &szHtml) > 0)
		{
		ReplaceString(&szHtml,MAKE_VAR_FIELD(ACU_RETURN_RESULT),	"3");
		}
		if(szHtml != NULL)
		{

		PostPage(szHtml);
		DELETE(szHtml);
		szHtml = NULL;
		} 
		*/
		printf("<html><body topmargin=\"100\" leftmargin=\"0\" bgcolor=\"#ffffff\" text=\"#000000\">");
		printf("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"><tr bgcolor=\"#6699cc\">");
		printf("<td height=\"31\"><div align=\"left\"><font style=\"FONT-SIZE:14pt\" face=\"PrimaSans BT, Verdana, sans-serif\" color=\"white\">");
		printf("<b>Over space tips</b></font></div></td></tr><tr align=\"center\" valign=\"middle\"><td colspan=\"3\" bgcolor=\"#cccccc\">");
		printf("<table ><font style=\"FONT-SIZE:14pt\" face=\"PrimaSans BT, Verdana, sans-serif\" >Your upload files is over the space, please upload the correct files to Controller.");
		printf("Use Back button in IE to go back!</font></table></td></tr></table></body></html>");
	}


	if(szSrcFile != NULL)
	{
		DELETE(szSrcFile);
		szSrcFile = NULL;
	}

	if(szDesFile != NULL)
	{
		DELETE(szDesFile);
		szDesFile = NULL;
	}

	if(szFileName != NULL)
	{
		DELETE(szFileName);
		szFileName = NULL;
	}

	if(szMatchFileName != NULL)
	{
		DELETE(szMatchFileName);
		szMatchFileName = NULL;
	}
	return TRUE; 
}

static int Web_Get_SwichState(void)
{
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;
	char    *pSysState = NULL;
	//char    *pAppState = NULL;
	char	*pTemp = NULL;

#define SWICH_FILE_NAME		"/var/appinfo.log"
#define	SYS_SWICH_STATE		"JUMP_SYSTEM_RDONLY:"
#define	APP_SWICH_STATE		"JUMP_APP_RDONLY:"

	pFile = fopen(SWICH_FILE_NAME, "r");
	if (pFile == NULL)
	{	
		return 2;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return 2;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	pTemp = strstr(szInFile,SYS_SWICH_STATE);
	pSysState = pTemp+19;

	/*pTemp = strstr(szInFile,APP_SWICH_STATE);
	pAppState = pTemp+16;*/

	/*if(*pAppState == 'S')
	{
	    FILE *fp20 = fopen("/var/20.htm","w");
	    fclose(fp20);
		return 3;
	}*/

	SAFELY_DELETE(szInFile);
	szInFile = NULL;

	return TRUE;

}

static int Web_FLE_CloseACU(void)
{
	/*���͹ر�ACU �ź�,���ACU �Ƿ�ر�*/
	//system("killall -9 acu");
	int		fd;    //fifo handle
	int		iLen = 0;
	char	szBuf[1024];
	//int		iBufCount = 0;  //buf count ,used in read FIFO buf 


	/*create FIFO with our PID as part of name*/
	if((fd = open(MAIN_FIFO_NAME,O_WRONLY))<0)
	{
		return FALSE;
	}

	//time(&tp);
	iLen = sprintf(szBuf,"%10ld%2d%8d%2d%2d",(long)getpid(), FILE_MANAGE,0,0,0);

	if((write(fd,szBuf,strlen(szBuf) + 1)) < 0)
	{
		close(fd);
		return FALSE;
	}

	close(fd);
	return TRUE;
	//#define WEB_CLOSE_ACU  "/acu/stopacu"
	//	if(system(WEB_CLOSE_ACU) > 0)
	//	{
	//		return TRUE;
	//	}
	//	return FALSE;
}


#define _O_RDWR         0x0002  /* open for reading and writing */

static int Web_FLE_StartACU(void)
{
	/*��������ACU �ź�,���ACU �Ƿ�ر�*/
	//char		szSystemCommand[64];
	//sprintf(szSystemCommand, "%s", "reboot");

	//system(szSystemCommand);
	printf("\n___/sbin/reboot OK!___\n");
	//system("/sbin/reboot");
	int s_hWatchDog;
	s_hWatchDog = open("/dev/wd", _O_RDWR);
	if(s_hWatchDog < 0)
	{
		printf("error to open %d\n", s_hWatchDog);
		return ;
	}

	// init the maximum alllowed feed period in seconds.
	ioctl(s_hWatchDog, IOCTL_SET_TIMER, 0x03);

	ioctl(s_hWatchDog, IOCTL_EN_WD);

	sleep(20);

	return TRUE;

}




//static int Web_FLE_replaceFile(IN  char *szDstFile, IN  char *szSrcFile)
//{   
//
//	ASSERT(szDstFile);
//	ASSERT(szSrcFile);
//	char            szBuffer[128];
//	char			szTempFile[64];
//	char			*pSearchValue = NULL;
//
//	pSearchValue = strrchr(szSrcFile, 13);
//	if(pSearchValue != NULL)
//	{
//		*pSearchValue = '\0';
//	}
//
//	strncpyz(szTempFile, szDstFile + 3, sizeof(szTempFile));
//
//		pSearchValue = strrchr(szTempFile, 13);
//	if(pSearchValue != NULL)
//	{
//		*pSearchValue = '\0';
//	}
//	sprintf(szBuffer,"cp -rf %s  /%s",szSrcFile, szTempFile );
// 
//	FILE *fp;
//	if((fp = fopen("/acu/www/html/file.htm","wb")) != NULL)
//	{
//		fwrite(szBuffer,strlen(szBuffer), 1, fp);
//		fclose(fp);
//	}
//
//	system(szBuffer);
//	//system("cp -rf /var/BasicStandard.cfg /acu/config/standard/BasicStandard.cfg ");
//	//remove(szSrcFile);
//	///*��commȥcp����*/
//	//
//	//int		fd;    //fifo handle
//	//int		iLen = 0;
//	//char	szBuf[1024];
//	//
//	//
//	///*create FIFO with our PID as part of name*/
// //	if((fd = open(MAIN_FIFO_NAME,O_WRONLY))<0)
//	//{
//	//	return FALSE;
//	//}
//	// 
//	//
//	//iLen = sprintf(szBuf,"%10ld%2d%128s",(long)getpid(), FILE_MANAGE_REPLACEFILE,szBuffer);
//	//
//	//if((write(fd,szBuf,strlen(szBuf) + 1)) < 0)
//	//{
//	//	close(fd);
//	//	return FALSE;
//	//}
//
//	//close(fd);
//	return TRUE;
//
//	
//}



char *Request(IN char *key,OUT char *szOut)
{
	UNUSED(key);
	return szOut;
}



//��ȡ�����Ʒָ��
int Web_FLE_GetBoundary(IN char *boundary)
{
	int		iContentType = (int)strlen(M_CONTENT_TYPE);
	int		iLen = 0;
	int		i = 0;
	int		iPosition = iContentType + 11;//11 byte length just '; boundary='
	*boundary = 0x0;

	if (CONTENT_LENGTH == NULL)
		return 1;

	if (CONTENT_TYPE == NULL) 
		return 2;

	if (!strncmp(CONTENT_TYPE, M_CONTENT_TYPE, (size_t)iContentType))
	{
		*boundary++ = '-';
		*boundary++ = '-';
		iLen = (int)strlen(CONTENT_TYPE);

		for (i = iPosition; i < iLen; i++)
		{
			*boundary++ = CONTENT_TYPE[i];
		}

		*boundary = 0x0;
		return 0;
	}
	return 3;
}



/*
��ȡ������
=0������
=1���ļ���
*/
int Web_FLE_GetFormType(char *szIn)
{
	char key[255]={0};
	char *p;

	sprintf(key, "filename=%c", 34);
	p = strstr(szIn, key);

	return p != NULL;
}



//��ȡ����
void Web_FLE_GetFormName(char *szIn, char *szOut)
{
	char	key[255]={0};
	char	*p;

	sprintf(key, "name=%c", 34);
	p = strstr(szIn, key);

	*szOut = 0x0;
	if (p)
	{
		p += (int)strlen(key);

		while(*p&&*p!='"') 
			*szOut++ = *p++;
	}
}



//��Content-Type��ȡ�ļ���
void Web_FLE_GetFileExtend(IN char *szIn, OUT char *szOut)
{

	char	*szKey	= ".";
	char *p;
	p= strrchr(szIn,92);		//��ȡhttpͷ�����һ��"\"

	*szOut = 0x0;
	if (p)
	{
		p += (int)strlen(szKey);

		while( *p && *p != '"') 
			*szOut++ = *p++;
	}
}



//�������ݣ����浽����
/*sPos : ��ʼλ�� s1:���ݳ���.�м�����Ϊ��������ݣ��޷ָ���*/
void DBParse(unsigned char *szIn, unsigned long ulStartPos, unsigned long ulDataLen)
{
	unsigned char	type = 0, name[255]={0}, *value, extend[255]={0};
	unsigned long	size;
	int				iCrlf = (int)strlen(CGI_CRLF);
	unsigned		long ulHeadLength;//head length
	unsigned		long ulPos1, ulPos2;
	char			*buf = NULL;

	buf = NEW(char, (size_t)ulDataLen + 1);//(char *)malloc(ulDataLen + 1);//

	char			*tmp = NULL;

	UNUSED(size);
	memset(buf, 0x0, (size_t)(ulDataLen + 1));
	memcpy(buf, szIn + ulStartPos, (size_t)ulDataLen);

	/*get head length*/
	ulHeadLength = Web_FLE_substring((size_t)0, buf, ulDataLen, "\r\n\r\n");
	tmp = NEW(char, ulHeadLength + 1);//(char *)malloc(ulHeadLength + 1);

	memset(tmp, 0x0, ulHeadLength + 1);
	memcpy(tmp, buf, ulHeadLength);

	type = (unsigned char)Web_FLE_GetFormType(tmp);
	Web_FLE_GetFormName(tmp, (char *)name);

	ulPos1 = ulHeadLength + iCrlf + iCrlf;  /*����"\r\n"*/
	ulPos2 = ulDataLen - ulPos1;

	value = NEW(unsigned char, ulPos2 + 1);//(unsigned char *)malloc(ulPos2 + 1);
	memset(value, 0x0, ulPos2+1);
	memcpy(value, buf + ulPos1, ulPos2);

	if (type == (unsigned char)1  && ulPos2 > 0)
	{
		/*get file extend*/
		Web_FLE_GetFileExtend(tmp, (char *)extend);
	}

	//TRACE("name:%s", name);
	push(&pUploadQueue, (unsigned char)type, name, 
		(unsigned char *)value, extend, 
		(unsigned long)ulPos2);
	serial++;

	DELETE(value);
	DELETE(tmp);
	DELETE(buf);//free(buf);
}


/*
�ϴ����
szIn = stdin���������
sl = CONTENT_LENGTH
key = �����Ʒָ��boundary
*/
/*IN unsigned long ulS :data length*/
int Web_FLE_UImport(IN char *szIn, IN unsigned long ulS, IN char  *key)
{
	int				iKey =	(int)strlen(key);
	int				iCrlf = (int)strlen(CGI_CRLF);
	unsigned long	ulPos1,  ulPos2;

	/*��ȡ��һ�������Ʒָ�����λ��*/
	ulPos1 = Web_FLE_substring((size_t)0, szIn, (unsigned int)ulS, key);
	while (ulPos1 != (unsigned long)-1)
	{
		ulPos2 = Web_FLE_substring((size_t)(ulPos1 + (unsigned long)iKey), szIn, (unsigned int)ulS, key);//��һ�������Ʒָ����Ŀ�ʼλ��

		if (ulPos2 ==(unsigned long) -1) 
			break;
		/*�ѷָ���֮������ݽ��н���*/
		//printf("Content-type:text/html\n\n");
		//TRACE("[%ld][%ld][%d][%d][%d]!\n",ulPos1,ulPos2, iKey,iCrlf, ulPos2 - ulPos1 - iKey - iCrlf * 2);
		DBParse(szIn, ulPos1 + iKey + iCrlf, ulPos2 - ulPos1 - iKey - iCrlf * 2);//���������Ʒָ���е�����

		ulPos1 = Web_FLE_substring((size_t)ulPos2, szIn, (unsigned int)ulS, key);


	}

	return 1;
}

/*
�ϴ��ļ�����
����PUploadQueue�ṹ
name = ����
*/
UploadQueue *Web_FLE_UExport(UploadQueue **pQueue, char *name)
{
	UploadQueue *PQ = *pQueue;
	UploadQueue *R = NULL;

	while(PQ != NULL)
	{
		if (!strcmp(PQ->name, name))
		{
			R = PQ;
			break;
		}
		PQ= PQ->next;
	}
	return R;
}

/*
�����ļ�
xPath = ·��
xIn = ����
xl = ���ݳ���
*/
int Web_FLE_SaveToFile(IN char *pszPath, IN unsigned char *szIn, IN unsigned int uiLen)
{
	FILE *fp = fopen(pszPath, "w");
	if (fp != NULL)
	{
		fwrite(szIn, uiLen, 1, fp);
		fclose(fp);
		return 1;
	}
	else
	{
		return 0;
	}
}



/**************queue*****************/

void push(UploadQueue **pQueue, unsigned char type, unsigned char *name, 
		  unsigned char *value, unsigned char *extend, unsigned long size)
{
	//UploadQueue *tmp = (UploadQueue *)malloc(sizeof(UploadQueue));
	UploadQueue *tmp = NEW(UploadQueue, 1);
	UploadQueue *PQ = *pQueue;
	if (tmp!=NULL)
	{
		tmp->type = type;
		strcpy(tmp->name, name);
		//tmp->value = (unsigned char *)malloc(size+1);
		tmp->value = NEW(unsigned char, size + 1);

		memset(tmp->value, 0x0, size+1);
		memcpy(tmp->value, value, size);

		strcpy(tmp->extend, extend);

		tmp->size = size;
		tmp->next = NULL;

		if (*pQueue == NULL)
		{
			*pQueue = tmp;
		}
		else
		{
			while(PQ != NULL && PQ->next != NULL)
			{
				PQ = PQ->next;
			}
			PQ->next = tmp;
		}
	}
	else
	{
		TRACE("No memory available.\n");
	}
}

void pop(UploadQueue **ppQueue)
{
	UploadQueue *tmp;
	if (IsEmpty(*ppQueue)) 
	{
		return;
	}
	tmp = *ppQueue;
	*ppQueue = (*ppQueue)->next;
	DELETE(tmp->value);
	DELETE(tmp);
}

int IsEmpty(UploadQueue *pQueue)
{
	return pQueue == NULL;
}

static off_t PrintQueue(UploadQueue *pQueue)
{
	char		filePath[255]={0};
	off_t		dir_size = 0, file_size = 0;

	dir_size = list(VAR_PATH);

	UploadQueue		*pTempQueue = pQueue;
	while(pTempQueue != NULL)
	{
		if(pTempQueue->type == (unsigned char)1 && pTempQueue->size > 0)
		{
			file_size += pTempQueue->size;
		}
		pTempQueue = pTempQueue->next;
	}


	//printf("dir_size : %ld[%ld]\n", dir_size, file_size);
	if(file_size < MAX_VAR_DIR_SIZE - dir_size)
	{
		while(pQueue != NULL)
		{
			if (pQueue->type == (unsigned char)1 && pQueue->size > 0)
			{
				sprintf(filePath, "%s/%s%s", NEW_DOWNLOAD_DIRECTORY, PREFIX_DOWNLOAD_FILE,pQueue->extend);
				Web_FLE_SaveToFile(filePath, pQueue->value, pQueue->size);
			}
			pQueue = pQueue->next;
		}
	}
	else
	{
		return ERR_FILE_NO_SPACE;
	}
	return file_size; 
}


//static char *Web_FLE_MakeFilesListBuffer(void)
//{
//DIR			*pDir =NULL;
//struct	dirent *pdirent = NULL;
//int			i = 0;
//char		*szFileBuffer;
//int			iLen = 0;

//szFileBuffer = NEW(char, 1024);
//if(szFileBuffer == NULL)
//{
//	return NULL;
//}
////for(i = 0; i < 6; i++)
////{
////	if((pDir = opendir(szFilePath[i])) != NULL)
////	{
////		while ((pdirent = readdir(pDir)) != NULL) 
////		{
////			iLen += sprintf(szFileBuffer + iLen, "%d,/%s/%s/%s;",", i,
////				WORK_DIR_PATH, szFilePath[i],  pdirent->d_name);
////		}
////	}
////}

//char pn[256];
//if((pDir = opendir("/acu")) != NULL)
//{
//	while ((pdirent = readdir(pDir)) != NULL) 
//	{
//		if(pdirent->d_ino == 0) 
//		{
//			continue;
//		}

//		if ((strcmp(pdirent->d_name, ".") == 0) ||
//			(strcmp(pdirent->d_name,"..") == 0)) 
//		{
//			continue;
//		}
//		strcpy(pn, "/acu"); 
//		strcat(pn, "/"); 
//		strcat(pn, dir->d_name); 

//		iLen += sprintf(szFileBuffer + iLen, "/%s/%s/%s;", 
//			WORK_DIR_PATH, pdirent->d_name);
//	}
//}
// 
//szFileBuffer[iLen -1] = 32;
//return szFileBuffer;	
//}


static int Web_FLE_getCGIParam(OUT int *iReturnControlType, 
							   OUT char **szReturnSessionID, 
							   OUT int *iReturnLanguage, 
							   OUT char **szOutSrcFile,
							   OUT char **szOutDesFile, 
							   OUT char **szOutFileName,
							   OUT off_t *file_size)
{


	char			**postvars = NULL; /* POST request data repository */
	char			**getvars = NULL; /* GET request data repository */
	int				form_method; /* POST = 1, GET = 0 */  
	char			*val = NULL;
	unsigned long	ulLen = 0;
	int				iControlType = 0 , iLanguageType = 0;
	int				i;
	char			boundary[255]={0};
	char			*szSessionID = NULL;
	UploadQueue		*pUQ = NULL;
	char			*szSrcFile = NULL, *szDesFile = NULL;

	form_method		= getRequestMethod();

	UNUSED(ulLen);
	UNUSED(szSrcFile);
	UNUSED(szDesFile);
	UNUSED(szOutSrcFile);
	UNUSED(szOutDesFile);




	if(form_method == POST) 
	{
		getvars		= getGETvars();
		//postvars	= getPOSTvars();


		postvars = getPOSTvarsA(&HTTPBuffer);
		if(postvars == NULL )
		{
			return FALSE;
		}
	} 
	else if(form_method == GET) 
	{
		getvars = getGETvars();
	}
	else 
	{
		return FALSE;
	}



	if((val = getValue( getvars, postvars, SESSION_ID)) != NULL) // sessionId
	{
		//szSessionID = atoi(val);
		szSessionID = NEW(char, 32);
		sprintf(szSessionID, "%s", val);
		//printf("szSessionID[%s]", szSessionID);
	}

	if((val = getValue(getvars,postvars,LANGUAGE_TYPE)) != NULL)
	{

		iLanguageType = atoi(val);


	}



	//if((val = getValue(getvars,postvars,"file_name")) != NULL)
	//{
	//	TRACE("val : %s\n", val);
	//	sprintf(szMatchFileName, "%s", val);

	//}


	if((val = getValue(getvars,postvars,WEB_CGI_CLOSE_ACU)) != NULL && atoi(val) > 0)
	{
		iControlType = CLIENT_CLOSE_ACU;
	}
	else 
	{
		if((val =Web_FLE_GetFileName(HTTPBuffer, LANGUAGE_TYPE)) != NULL )
		{
			iLanguageType = atoi(val);
		}

		if((val =Web_FLE_GetFileName(HTTPBuffer, WEB_CGI_START_ACU)) != NULL&& atoi(val) > 0)
		{
			iControlType = CLIENT_START_ACU;
		}
		else if((val = getValue(getvars,postvars,WEB_CGI_START_ACU)) != NULL && atoi(val) > 0)	
		{
			//TRACE("val : %s\n", val);
			iControlType = CLIENT_START_ACU;
		}
		else if((val =Web_FLE_GetFileName(HTTPBuffer, WEB_CGI_CFG_FILE)) != NULL&& atoi(val) > 0)
		{
			iControlType = UPLOAD_CFG_FILE;
		}
		else if((val = getValue(getvars,postvars,WEB_CGI_CFG_FILE)) != NULL && atoi(val) > 0)	
		{
			//TRACE("val : %s\n", val);
			iControlType = UPLOAD_CFG_FILE;
		}
		else if((val =Web_FLE_GetFileName(HTTPBuffer, WEB_CGI_LANG_FILE)) != NULL&& atoi(val) > 0)
		{
			iControlType = UPLOAD_LANG_FILE;
		}
		else if((val = getValue(getvars,postvars,WEB_CGI_LANG_FILE)) != NULL && atoi(val) > 0)	
		{
			//TRACE("val : %s\n", val);
			iControlType = UPLOAD_LANG_FILE;
		}
		else//((val = getValue(getvars,postvars,WEB_CGI_UPLOAD_FILE)) != NULL)
		{
			off_t			tmp_file_size = 0;
			//printf("Content-type:text/html\n\n");
			Web_Del_Zip_File();//Added by wj 20070208

			CONTENT_TYPE	= (unsigned char *)getenv("CONTENT_TYPE");
			CONTENT_LENGTH	= (unsigned char *)getenv("CONTENT_LENGTH");
			iControlType = CLIENT_UPLOAD_FILE;

			szMatchFileName = Web_FLE_GetFileName(HTTPBuffer,"file_name");

			
			/*Upload file*/

			if (!Web_FLE_GetBoundary(boundary))
			{
				/*get data*/
				Web_FLE_UImport((char *)HTTPBuffer, (unsigned long)atol(CONTENT_LENGTH), boundary);
			}

			pUQ = Web_FLE_UExport(&pUploadQueue, "filedata0");//����ʾ��; filedata0�Ǳ���


			if (pUQ != NULL)
			{

				/*printf("%-10s%d\n%-10s%s\n%-10s%s\n%-10s%s\n%-10s%lu\n\n", 
				"type",		pUQ->type,		"name", pUQ->name, "value", 
				pUQ->value,	"extend",	pUQ->extend,	"size", pUQ->size); */


				/*save file*/
				//char	*szFileNameMatch = NULL;
				//if((szFileNameMatch = NEW(char, 255)) != NULL)
				//{
				//	//get file name
				//	sprintf(szFileNameMatch, "%s",pUQ->extend);
				//	*szOutFileName = szFileNameMatch;
				//}

				tmp_file_size = PrintQueue(pUploadQueue);

				if(tmp_file_size == ERR_FILE_NO_SPACE)
				{
					/*			DELETE(szFileNameMatch);
					szFileNameMatch = NULL;*/
					return ERR_FILE_NO_SPACE;
				}
				else
				{
					*file_size = tmp_file_size;
				}

			}

			for (i = 0; i < serial; i++) 
				pop(&pUploadQueue);//��ն���
			fflush(stdout);
		}
	}
	//printf("szFilePath : iReturnControlType :%d[%s]\n",iControlType, HTTPBuffer);

	*iReturnControlType	= iControlType;
	*szReturnSessionID	= szSessionID;
	*iReturnLanguage	= iLanguageType;

	if (HTTPBuffer != NULL) 
		DELETE(HTTPBuffer);
	cleanUp( getvars, postvars);


	return TRUE;  
}

static char *Web_FLE_GetFileName(IN char *szHttp, IN const char *szFindName)
{
	ASSERT(szHttp);
	ASSERT(szFindName);

	//TRACE("szFindName : %s\n", szFindName);
	//TRACE("szHttp : %256s\n", szHttp);
	char	*pSearchValue1 = NULL, *pSearchValue2 = NULL;
	int		iPosition = 0;

	/*FILE *fp7 = fopen("/var/7.htm","w");
	fwrite(szHttp,strlen(szHttp), 1,fp7);
	fclose(fp7);*/

	pSearchValue1 = strstr(szHttp,szFindName);
	//TRACE("pSearchValue1 : %s", pSearchValue1);
	if(pSearchValue1 != NULL)
	{
		pSearchValue1 = pSearchValue1 + strlen(szFindName) + 2;
		//TRACE("pSearchValue1 : %64s", pSearchValue1);
		if((pSearchValue2 = strstr(pSearchValue1, "--")) != NULL)
		{
			//TRACE("pSearchValue2 : %s", pSearchValue2);
			iPosition = pSearchValue2 - pSearchValue1;
			char	*szOutName = NEW(char, iPosition + 1);
			strncpyz(szOutName, pSearchValue1, iPosition + 1);
			return szOutName;
		}
	}
	return NULL;
}
static char *Web_FLE_GetFileFromPath(char *szSrcFile)
{
	char		*szDesFile = NULL;
	char		*szSrcCopyFile = szSrcFile;
	char		*pSearchValue = NULL;
	int			iPosition = 0;
#define MAX_FILE_LEN			128
#define GET_REVERSE_SLASH		92//'\'

	/*FILE *fp6 = fopen("/var/6.htm","w");
	fwrite(szSrcFile,strlen(szSrcFile), 1,fp6);
	fclose(fp6);*/
	szDesFile = NEW(char, MAX_FILE_LEN);
	//printf("[1]szSrcCopyFile[%s]\n", szSrcCopyFile);
	if(szDesFile != NULL && szSrcCopyFile != NULL)
	{

		pSearchValue = strrchr(szSrcCopyFile, GET_REVERSE_SLASH);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - szSrcCopyFile;
			if(iPosition > 0)
			{
				szSrcCopyFile = szSrcCopyFile + iPosition + 1;
				//printf("[2]szSrcCopyFile[%s]\n", szSrcCopyFile);
				strncpyz(szDesFile, szSrcCopyFile, MAX_FILE_LEN  );

				//printf("[3]szDesFile[%s]\n", szDesFile);
				return szDesFile;
			}
		}
		DELETE(szDesFile);
		szDesFile = NULL;
		return NULL;
	}

	DELETE(szDesFile);
	szDesFile = NULL;
	return NULL;
}


static char **getPOSTvarsA(unsigned char **HTTPBuffer) 
{
	int			i;
	int			content_length;
	char		**postvars;
	char		*postinput;
	char		**pairlist;
	int			paircount = 0;
	char		*nvpair;
	char		*eqpos;
	unsigned char		*pHTTPBuffer = NULL;

	postinput = getenv("CONTENT_LENGTH");

	if (!postinput)
		return NULL;
	if(!(content_length = atoi(postinput)))
		return NULL;
	if(!(pHTTPBuffer = NEW(char, (size_t)content_length + 1)))
	{
		return NULL;
	}
	if(!(postinput = NEW(unsigned char,(size_t)content_length + 1)))
		return NULL;
	if(!fread(pHTTPBuffer, (size_t)content_length, (size_t)1, stdin))
		return NULL;
	*HTTPBuffer = pHTTPBuffer;
	strncpyz(postinput, pHTTPBuffer, content_length + 1);
	//printf("postinput[%s]\n", postinput);
	//if((pHTTPBuffer = NEW(unsigned char, (size_t)content_length + 1)) != NULL)
	//{
	//	//sprintf(pHTTPBuffer, "%s[%d]", postinput,content_length);


	//	//printf("%us", postinput);
	//	strncpyz(pHTTPBuffer, (unsigned char *)postinput, content_length + 1);
	//	*HTTPBuffer = pHTTPBuffer;

	//}


	for(i=0;postinput[i];i++)
		if(postinput[i] == '+')
			postinput[i] = ' ';

	//pairlist = (char **) malloc(256*sizeof(char **));
	pairlist = NEW(char *,256);
	paircount = 0;
	nvpair = strtok(postinput, "&");
	//strtok_r(postinput, "&",&nvpair);
	while (nvpair) 
	{
		pairlist[paircount++] = strdup(nvpair);
		if(!(paircount%256))
			//pairlist = (char **) realloc(pairlist, (paircount+256)*sizeof(char **));
			pairlist = RENEW(char *, pairlist, (paircount+256)*sizeof(char *));
		nvpair = strtok(NULL, "&");
		//strtok_r(NULL, "&", &nvpair);
	}

	pairlist[paircount] = 0;
	//postvars = (char **) malloc((paircount*2+1)*sizeof(char **));
	postvars = NEW(char *,paircount*2 +1);
	for(i = 0;i<paircount;i++) 
	{
		if((eqpos = strchr(pairlist[i], '='))) 
		{
			*eqpos= '\0';
			unescape_url(postvars[i*2+1] = strdup(eqpos+1));
		} 
		else 
		{
			unescape_url(postvars[i*2+1] = strdup(""));
		}
		unescape_url(postvars[i*2]= strdup(pairlist[i]));
	}
	postvars[paircount*2] = 0;

	//for(i=0;pairlist[i];i++)
	//	DELETE(pairlist[i]);
	DELETE(pairlist);
	DELETE(postinput);

	return postvars;
}

//��ȡ/var Ŀ¼�ռ��С
static off_t list(char *name) 
{ 
	char		pn[255]; 
	DIR			*dp; 
	off_t		f_size,d_size; 
	int			i; 
	struct stat sbuf; 
	struct direct *dir;

	f_size = 0; 

	for (i = 0; i <= 1; i++) 
	{ 
		if ((dp = opendir(name))== NULL) 
		{ 
			return -1; 
		} 
		while ((dir = readdir(dp)) != NULL) 
		{ 
			if(dir->d_ino == 0) 
				continue; 
			strcpy(pn, name); 
			strcat(pn, "/"); 
			strcat(pn, dir->d_name); 
			if (lstat(pn, &sbuf) < 0) 
			{ 
				return -1; 
			} 
			//�ж϶������ļ��Ƿ���һ��Ŀ¼��������������(sbuf.st_mode&S_IFMT)==S_IFDIRʵ�֡�
			//�����������������˳��������ӡ�Ŀ¼�������һ��Ŀ¼���Ա��������ѭ���� 
			if (((sbuf.st_mode & S_IFMT) != S_IFLNK) &&
				((sbuf.st_mode & S_IFMT) == S_IFDIR) &&
				(strcmp(dir->d_name,".") != 0) &&
				(strcmp(dir->d_name,"..") != 0)) 
			{ 
				//����ǵ�2��forѭ����ݹ����list�������г���Ӧ����Ŀ¼��
				//ͬʱ�ۼ�Ŀ¼��ռ���̿ռ�Ĵ�С�� 
				if (i==1) 
				{ 
					d_size=list(pn); 
					f_size=f_size + d_size; 
				} 
			} 
			else 
			{ 
				if (i==0) 
				{ 
					f_size=f_size + sbuf.st_size; 
					if((strcmp(dir->d_name,".") != 0) &&
						(strcmp(dir->d_name,"..") != 0))
					{
						//printf("*********%s\n",pn); 
					}
				} 

			} 

		} 

		closedir(dp); 
	} 
	return f_size; 
}

static off_t GetListName(char *name) 
{ 
	char		pn[255]; 
	DIR			*dp; 
	off_t		f_size,d_size; 
	int			i; 
	struct stat sbuf; 
	struct direct *dir;

	f_size = 0; 

	for (i = 0; i <= 1; i++) 
	{ 
		if ((dp = opendir(name))== NULL) 
		{ 
			return -1; 
		} 
		while ((dir = readdir(dp)) != NULL) 
		{ 
			if(dir->d_ino == 0) 
				continue; 
			strcpy(pn, name); 
			strcat(pn, "/"); 
			strcat(pn, dir->d_name); 
			if (lstat(pn, &sbuf) < 0) 
			{ 
				return -1; 
			} 
			//�ж϶������ļ��Ƿ���һ��Ŀ¼��������������(sbuf.st_mode&S_IFMT)==S_IFDIRʵ�֡�
			//�����������������˳��������ӡ�Ŀ¼�������һ��Ŀ¼���Ա��������ѭ���� 
			if (((sbuf.st_mode & S_IFMT) != S_IFLNK) &&
				((sbuf.st_mode & S_IFMT) == S_IFDIR) &&
				(strcmp(dir->d_name,".") != 0) &&
				(strcmp(dir->d_name,"..") != 0)) 
			{ 
				//����ǵ�2��forѭ����ݹ����list�������г���Ӧ����Ŀ¼��
				//ͬʱ�ۼ�Ŀ¼��ռ���̿ռ�Ĵ�С�� 
				if (i==1) 
				{ 
					d_size=GetListName(pn); 
					f_size=f_size + d_size; 
				} 
			} 
			else 
			{ 
				if (i==0) 
				{ 
					f_size=f_size + sbuf.st_size; 
					if((strcmp(dir->d_name,".") != 0) &&
						(strcmp(dir->d_name,"..") != 0))
					{
						//printf("************%s\n",pn); 
						strcat(szPathList, pn);
						strcat(szPathList,";\n");
					}
				} 

			} 

		} 

		closedir(dp); 
	} 
	return f_size; 
}



static int Web_Replace_File(IN char *szFileName)
{
	char szUnzip[128];
	char	*pSearchValue = NULL;
	char	szFileModifyTime[128];

#define WEB_SOLUTION_FILE_NAME			"MonitoringSolution.cfg"

	/*FILE *fp4 = fopen("/var/4.htm","w");
	fwrite(szFileName,strlen(szFileName), 1, fp4);
	fclose(fp4);*/


	if(szFileName)
	{

		sprintf(szFileModifyTime, "/usb/%s", szFileName);
		Web_FLE_ModifyTime(szFileModifyTime);


		while((pSearchValue = strrchr(szFileName, '\n')) != NULL )
		{
			*pSearchValue = 32;
		}

		while((pSearchValue = strrchr(szFileName, '\r')) != NULL )
		{
			*pSearchValue = 32;
		}

		if((pSearchValue = strstr(szFileName, ".tar.gz")) != NULL)
		{
			sprintf(szUnzip, "tar -xzf %s/%s -C /app", NEW_DOWNLOAD_DIRECTORY, szFileName);//, szFileName);
		}
		else if((pSearchValue = strstr(szFileName, ".tar")) != NULL)
		{
			sprintf(szUnzip, "tar -xf %s/%s -C /app", NEW_DOWNLOAD_DIRECTORY, szFileName);//, szFileName);
		}
		else if((pSearchValue = strstr(szFileName, WEB_SOLUTION_FILE_NAME)) != NULL)
		{
			sprintf(szUnzip, "mv -f %s/%s /app/config/solution/%s", NEW_DOWNLOAD_DIRECTORY,szFileName, WEB_SOLUTION_FILE_NAME);
		}
		else if((pSearchValue = strstr(szFileName, WEB_SETTINGPARAM_FILE_NAME)) != NULL)
		{
		    FILE *fp5 = fopen("/var/5.htm","w");
		    fwrite(szFileName,strlen(szFileName), 1,fp5);
		    fclose(fp5);
			sprintf(szUnzip, "mv -f %s/%s /app/config/run/%s",NEW_DOWNLOAD_DIRECTORY, szFileName, WEB_SETTINGPARAM_FILE_NAME);
		}
		else
		{
			/*FILE *fp5 = fopen("/var/5.htm","w");
			fwrite(szFileName,strlen(szFileName), 1,fp5);
			fclose(fp5);*/
			return FALSE;
		}

		if(system(szUnzip) != -1)
		{
			/*FILE *fp2 = fopen("/var/3.htm","w");
			fwrite(szFileName,strlen(szFileName), 1,fp2);
			fclose(fp2);*/

			return TRUE;
		}
	}
	FILE *fp = fopen("/var/2.htm","w");
	fwrite(szFileName,strlen(szFileName), 1,fp);
	fclose(fp);

	return FALSE;
}


static void Web_Remove_File(IN char *szFileName)
{
	char szUnzip[128];
	char *pSearchValue = NULL;

	if(szFileName && (strcmp(szFileName, WEB_ZIP_CFG_LANG) != 0 || strcmp(szFileName, WEB_ZIP_CFG) != 0))// || strcmp(szFileName, WEB_SETTINGPARAM_FILE_NAME) != 0
	{

		while((pSearchValue = strrchr(szFileName, '\n')) != NULL )
		{
			*pSearchValue = 32;
		}

		while((pSearchValue = strrchr(szFileName, '\r')) != NULL )
		{
			*pSearchValue = 32;
		}
		sprintf(szUnzip, "rm -rf %s/%s", NEW_DOWNLOAD_DIRECTORY, szFileName);
		system(szUnzip);
	}
}


static void Web_Zip_File(void)
{

#define WEB_CFG_PATH				"config/solution/ config/standard/ config/private/ config/run/"
#define WEB_LANG_PATH				"config/lang"
	//#define WEB_SOLUTION_FILE_PATH		"cp -rf /app/config/solution/MonitoringSolution.cfg /var/"

	char szZip[256];

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_CFG_PATH,WEB_ZIP_CFG_LANG);
	sprintf(szZip, "cd /app;tar -cf /var/%s %s", WEB_ZIP_CFG, WEB_CFG_PATH);
	system(szZip);


	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "cd /app;tar -cf /var/%s %s", WEB_ZIP_CFG_LANG, WEB_LANG_PATH);
	system(szZip);

	//system(WEB_SOLUTION_FILE_PATH);

}

static void Web_CGI_PostNoAuthority(IN int iModifyType, IN int iLanguage)
{




#define  WEB_P31				"p31_close_system.htm"	
#define  WEB_P33				"p33_replace_file.htm"

	char szFileNameReturn[128];

	memset(szFileNameReturn, 0, sizeof(szFileNameReturn));
	if(iModifyType == CLIENT_CLOSE_ACU)
	{
		if(iLanguage == LOCAL_LANGUAGE_NAME)
		{
			sprintf(szFileNameReturn, "%s/%s", WEB_LOC_FILE_PATH, WEB_P31);
		}
		else
		{
			sprintf(szFileNameReturn, "%s/%s", WEB_ENG_FILE_PATH, WEB_P31);
		}
	}
	else
	{
		if(iLanguage == ENGLISH_LANGUAGE_NAME)
		{
			sprintf(szFileNameReturn, "%s/%s", WEB_LOC_FILE_PATH, WEB_P31);
		}
		else
		{
			sprintf(szFileNameReturn, "%s/%s", WEB_ENG_FILE_PATH, WEB_P31);
		}
	}


}

static int Web_FLE_ModifyTime(IN char *szFileName)
{
	time_t				now;
	struct utimbuf		 FileTime;


	time(&now);
	FileTime.actime = now - 100;
	FileTime.modtime = now -100;

	if(szFileName != NULL)
	{
		utime(szFileName, &FileTime);
	}
}

static void Web_Del_Zip_File(void)
{


	char szZip[256];


	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_CFG_PATH,WEB_ZIP_CFG_LANG);
	sprintf(szZip, "rm -f /var/%s", WEB_ZIP_CFG);
	system(szZip);


	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/%s", WEB_ZIP_CFG_LANG);
	system(szZip);

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/eng/%s", WEB_PLC_PAGE);
	system(szZip);

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/loc/%s", WEB_PLC_PAGE);
	system(szZip);

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/%s", WEB_TAR_GZ);
	system(szZip);

	memset(szZip, 0, sizeof(szZip));
	//sprintf(szZip, "tar cvf - %s | gzip - > /var/%s", WEB_LANG_PATH, WEB_ZIP_CFG );
	sprintf(szZip, "rm -f /var/%s", WEB_TAR);
	system(szZip);


	//system(WEB_SOLUTION_FILE_PATH);

}

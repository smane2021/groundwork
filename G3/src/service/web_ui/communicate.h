/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : communicate.h
 *  CREATOR  : Yang Guoxin              DATE: 2004-09-28 10:15
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef _COMMUNICATE_H
#define _COMMUNICATE_H
#endif

//#include "new.h"
#include "pubfunc.h"
#include "public.h"
#include "batt_test_log.h"



/*transfer time*/
#define TIME_YEAR_LEN			5
#define TIME_MONTH_LEN			3
#define TIME_LINUX_YEAR			1900
#define TIME_LINUX_MONTH		1

/*
typedef struct tagUserinfo
{
	char chUsername[16];	  //�û�����
    char chPassword[16];	//�û�����	
	BYTE byLevel;   //�û�Ȩ��
}USER_INFO;
*/
typedef struct tagEquipList
{            //get equip list from server
	int		EquipID;            //equip ID
	BYTE	EquipType;         //equip type
	char	EquipName[16];     //equip name
}EquipList;

#define SAMPLE_DATA						0
#define	CONTROL_DATA					1
#define	SETTING_DATA					2
#define	RALARM_DATA						3
#define	STATUS_DATA						4


#define EQUIP_LIST_ID					0


/*Multi Language*/
#define	ENGLISH_LANGUAGE_NAME			0
#define	LOCAL_LANGUAGE_NAME 			1
#define	LOCAL2_LANGUAGE_NAME 			2 //Added by wj for three languages 2006.5.9

#define		WEB_SUPPORT_RECTID		0

/*For Query*/
typedef struct HisReadLog
{
	time_t	tmLogTime;//char	szTime[25];
	char	szTaskName[32];
	char	szInfoLevel[16];
	DWORD	RunThread_GetId;
	char	szBuf[129];
}HIS_READ_LOG;

/*used to the object log*/
typedef struct HisMatchLog
{
	time_t	tmLogTime;//char	szTime[32];
	char	szTaskName[32];
	char	szInfoLevel[16];
	char	szInformation[129];
}HIS_MATCH_LOG;


typedef struct HisGeneralCondition
{
	int 	iEquipID;
	int		iDataType;			/*	[0] his data	[1] statistics data 
									[2] his alarm	[3] control command 
									[4] Battery test */
	time_t	tmFromTime;
	time_t	tmToTime;
}HIS_GENERAL_CONDITION;

typedef struct HisAlarmToReturn
{
	char	szEquipName[64];
	char	szAlarmName[33];
	float	fTriggerValue;
	char	szUnit[8];
	time_t	tmStartTime;
	time_t  tmEndTime;
	BYTE	byLevel;
}HIS_ALARM_TO_RETURN;

typedef struct HisDataToReturn
{
	char				szEquipName[64];						// Equipment ID 
	char				szSignalName[33];						// Signal ID
	char				szSignalVal[32];					// Signal Value 
	char				szUnit[8];
	time_t				tSignalTime;					// Signal time 
    DWORD				dwActAlarmID;	
}HIS_DATA_TO_RETURN;

typedef struct HisStatDataToRetrun
{
	char				szEquipName[64];						// Equipment ID 
	char				szSignalName[32];						// Signal ID
	time_t				tmStatTime;						// Statistic time 
	char				szCurrentVal[32];					// Current Value
	char				szAverageVal[32];					// Average Value
	char				szMinVal[32];						// Minimum Value
    time_t				tmMinValTime;					// Time of Minimum Value
	char				szMaxVal[32];						// Maximum Value
    time_t				tmMaxValTime;
	char				szUnit[8];
}HIS_STATDATA_TO_RETURN;

typedef struct HisControlToReturn
{
	char				szEquipName[64];					// Equipment ID 
	char				szSignalName[33];					// Signal ID
	time_t				tmControlTime;						// Control time 
#define MAX_CTRL_CMD_LEN			64
	char				szControlVal[MAX_CTRL_CMD_LEN];		// Control Value
	char				szControlValForSave[MAX_CTRL_CMD_LEN];		//save file
	char				szUnit[8];
#define MAX_CTRL_SENDER_NAME	64
    char				szCtrlSenderName[MAX_CTRL_SENDER_NAME];	// Control Sender ID 
	char				szSenderType[16];					// Sender Type 
	//char				szControlResult[8];					// Control Result
	int					iControlResult;
    int					iValueType;							//show time type
}HIS_CONTROL_TO_RETURN;

//////////////////////////////////////////////////////////////////////////
//removed by wj 

typedef struct tagBatteryHeadInfo
{
	char				szHeadInfo[23];
	char				szReserve[41];
	BT_LOG_SUMMARY		btSummary;
	BYTE				byNull[156];
}BATTERY_HEAD_INFO;

typedef struct tagBatteryInfo
{
	BYTE		byBatteryCurrent;
	BYTE		byBatteryVolage;
	BYTE		byBatteryCapacity;

}BATTERY_INFO;
typedef struct tagBatteryBlockInfo
{
	BYTE		byBlockVoltage[8];

}BATTERY_BLOCK_VOLTAGE_STATUS;


typedef struct tagSMBAT_SMBRC_INFO
{
    BYTE		bySMBlockVoltage[24];

}SMBAT_SMBRC_INFO;
typedef struct tagBatteryGeneralInfo
{
	BYTE			byTemperatureInfo[7];
	BATTERY_INFO		BatteryInfo[114];	// ����豸��66���ĳ�114��
	SMBAT_SMBRC_INFO	stSmbatSmbrcBlock[20];
	BATTERY_BLOCK_VOLTAGE_STATUS	stBatteryBlock[4];
	BYTE				byNull[163];
}BATTERY_GENERAL_INFO;

typedef struct tagBatteryRealInfo
{
	//time_t		tRecordTime;
	//float		fVoltage;
	float		fBatCurrent;
	float		fBatVoltage;
	float		fBatCapacity;
	float		fBatTemp;
	int			iBatteryNum;

}BATTERY_REAL_DATA;
typedef struct tagBatteryBlockVoltage
{
	float				fBatBlockVoltage[8];//EIB Block Voltage

}BATTERY_BLOCK_VOLTAGE;

typedef struct tagBATTERY_SMBLOCK_VOLTAGE
{
    float				fSMBlockVoltage[24];//EIB Block Voltage

}BATTERY_SMBLOCK_VOLTAGE;

typedef struct tagBatteryLogInfo
{
	time_t				tRecordTime;
	float				fVoltage;
	float				fTemp[10];
	
	BATTERY_REAL_DATA	pBatteryRealData[114];// ����豸��66�ĳ�114��

	BATTERY_SMBLOCK_VOLTAGE	    fBatSMBlock[20];
	
	BATTERY_BLOCK_VOLTAGE		fBatBlock[4];//EIB2
	
}BATTERY_LOG_INFO;

//end////////////////////////////////////////////////////////////////////////

typedef struct tagCommEquipList
{
	int			iEquipID;
	char		szLocEquipName[33];
	char		szEngEquipName[33];
    char		szLoc2EquipName[33];
	int			iEquipType;
	BOOL		bGroupType;
#ifdef AUTO_CONFIG
	BOOL         bWorkState;//Added by wj for Auto Config
#endif
}COMM_EQUIP_LIST;

#define MAX_EQUIP_TYPE_NUM   100
typedef struct tagEquipSort
{
	int		iNum;
	int		iEquipTypeID;
	int		iEquipID;
	int		iUnitEquipID;
	char	szEngEquipName[33];
	char	szLocEquipName[33];
    char	szLoc2EquipName[33];
}EQUIP_SORT;

/*for private configure*/
typedef struct tagWebStatus
{
	int				iEquipID;
	int				iSignalType;
	int				iSignalID;
}WEB_STATUS_CONFIG_INFO;

typedef struct tagWebStatusConfig
{
	int						iNumber;
	WEB_STATUS_CONFIG_INFO	*stWebStatusConfig;
	int						iExploreType;
	int						iNetscapeNumber;
	WEB_STATUS_CONFIG_INFO	*stNetscapeConfig;
}WEB_STATUS_CONFIG;

typedef struct tagWebGCTestLog
{
	time_t		tStartTime;
	time_t		tEndTime;
	SIG_ENUM	enumStartReason;
	SIG_ENUM	enumTestResult;
}WEB_GC_DSL_TEST_INFO;

//////////////////////////////////////////////////////////////////////////
//Added by wj for PLC Configure
//Operator	Input1		Input2		Param1		Param2		Output

typedef struct tagWebPLCConfigLine
{
    char *pszOperator;
    char *pszInput1;
    char *pszInput2;
    char *pszParam1;
    char *pszParam2;
    char *pszOutput;
    
}WEB_PLC_CFG_INFO_LINE;

typedef struct tagWebPLCConfig
{
    int						iNumber;
    WEB_PLC_CFG_INFO_LINE	*stWebPLCConfigLine;
}WEB_PLC_CFC_INFO;


typedef struct tagWebAlarmConfigLine
{
    int iAlarmSigId;
    char *pszAlarmSupExp;
    char *pszRegId;

}WEB_ALARM_CFG_INFO_LINE;

typedef struct tagWebAlarmConfig
{
    int						iNumber;
    WEB_ALARM_CFG_INFO_LINE	*stWebAlarmConfigLine;
}WEB_ALARM_CFG_INFO;

typedef struct tagWebGCConfigLine
{
	char *pszPSSigName;
	char *pszEquipId;
	char *pszSigType;
	char *pszSigId;

}WEB_GC_CFG_INFO_LINE;

typedef struct tagWebGCConfig
{
	int						iNumber;
	char                    *pMode;
	WEB_GC_CFG_INFO_LINE	*stWebGCConfigLine;
}WEB_GC_CFC_INFO;

//end////////////////////////////////////////////////////////////////////////


//int Web_SetAlarmRelay(IN char *pszBuf);
int StartWebCommunicate(int *iQuitCommand);
DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs);

/*define Web pages marco*/
 
#define WEB_PAGES_CONTROL_NUM			"[control_cgi.htm:Number]"
#define WEB_PAGES_CONTROL				"[control_cgi.htm]"
#define WEB_PAGES_DATASAMPLE_NUM		"[data_sampler.htm:Number]"
#define WEB_PAGES_DATASAMPLE			"[data_sampler.htm]"
#define WEB_PAGES_DIALOG_NUM			"[dialog.htm:Number]"
#define WEB_PAGES_DIALOG				"[dialog.htm]"
#define WEB_PAGES_EDITSIGNAL_NUM		"[editsignalname.js:Number]"
#define WEB_PAGES_EDITSIGNAL			"[editsignalname.js]"
#define WEB_PAGES_EQUIPDATA_NUM			"[equip_data.htm:Number]"
#define WEB_PAGES_EQUIPDATA				"[equip_data.htm]"
#define WEB_PAGES_J01_NUM				"[j01_tree_maker.js:Number]"
#define WEB_PAGES_JO1					"[j01_tree_maker.js]"
#define WEB_PAGES_JO2_NUM				"[j02_tree_view.js:Number]"
#define WEB_PAGES_JO2					"[j02_tree_view.js]"
#define WEB_PAGES_JO4_NUM				"[j04_gfunc_def.js:Number]"
#define WEB_PAGES_JO4					"[j04_gfunc_def.js]"
#define WEB_PAGES_JO5_NUM				"[j05_signal_def.js:Number]"
#define WEB_PAGES_JO5					"[j05_signal_def.js]"
#define WEB_PAGES_JO7_NUM				"[j07_equiplist_def.js:Number]"
#define WEB_PAGES_JO7					"[j07_equiplist_def.js]"
#define WEB_PAGES_J30_NUM				"[p36_clear_data.htm:Number]"
#define WEB_PAGES_J30					"[p36_clear_data.htm]"
#define WEB_PAGES_J31_MENU_NUM			"[j31_menu_script.js:Number]"
#define WEB_PAGES_J31_MENU				"[j31_menu_script.js]"
#define WEB_PAGES_J31_TABLE_NUM			"[j31_table_script.js:Number]"
#define WEB_PAGES_J31_TABLE				"[j31_table_script.js]"
#define WEB_PAGES_J50_NUM				"[j50_event_log.js:Number]"
#define WEB_PAGES_J50					"[j50_event_log.js]"
#define WEB_PAGES_J76_NUM				"[j76_device_def.htm:Number]"
#define WEB_PAGES_J76					"[j76_device_def.htm]"
#define WEB_PAGES_J77_NUM				"[j77_equiplist_def.htm:Number]"
#define WEB_PAGES_J77					"[j77_equiplist_def.htm]"
#define WEB_PAGES_LOGIN_NUM				"[login.htm:Number]"
#define WEB_PAGES_LOGIN					"[login.htm]"
#define WEB_PAGES_P01_NUM				"[p01_main_frame.htm:Number]"
#define WEB_PAGES_P01					"[p01_main_frame.htm]"
#define WEB_PAGES_P02_NUM				"[p02_tree_view.htm:Number]"
#define WEB_PAGES_PO2					"[p02_tree_view.htm]"
#define WEB_PAGES_PO3_NUM				"[p03_main_menu.htm:Number]"
#define WEB_PAGES_PO3					"[p03_main_menu.htm]"
#define WEB_PAGES_PO4_NUM				"[p04_main_title.htm:Number]"
#define WEB_PAGES_P04					"[p04_main_title.htm]"
#define WEB_PAGES_P05_NUM				"[p05_equip_sample.htm:Number]"
#define WEB_PAGES_P05					"[p05_equip_sample.htm]"
#define WEB_PAGES_P06_NUM				"[p06_equip_control.htm:Number]"
#define WEB_PAGES_P06					"[p06_equip_control.htm]"
#define WEB_PAGES_P07_NUM				"[p07_equip_setting.htm:Number]"
#define WEB_PAGES_P07					"[p07_equip_setting.htm]"
#define WEB_PAGES_P08_NUM				"[p08_alarm_frame.htm:Number]"
#define WEB_PAGES_P08					"[p08_alarm_frame.htm]"
#define WEB_PAGES_P09_NUM				"[p09_alarm_title.htm:Number]"
#define WEB_PAGES_P09					"[p09_alarm_title.htm]"
#define WEB_PAGES_P10_NUM				"[p10_alarm_show.htm:Number]"
#define WEB_PAGES_P10					"[p10_alarm_show.htm]"
#define WEB_PAGES_P11_NUM				"[p11_network_config.htm:Number]"
#define WEB_PAGES_P11					"[p11_network_config.htm]"
#define WEB_PAGES_P12_NUM				"[p12_nms_config.htm:Number]"
#define WEB_PAGES_P12					"[p12_nms_config.htm]"
#define WEB_PAGES_P13_NUM				"[p13_esr_config.htm:Number]"
#define WEB_PAGES_P13					"[p13_esr_config.htm]"
#define WEB_PAGES_P14_NUM				"[p14_user_config.htm:Number]"
#define WEB_PAGES_P14					"[p14_user_config.htm]"
#define WEB_PAGES_P15_NUM				"[p15_show_acutime.htm:Number]"
#define WEB_PAGES_P15					"[p15_show_acutime.htm]"
#define WEB_PAGES_P16_NUM				"[p16_filemanage_title.htm:Number]"
#define WEB_PAGES_P16					"[p16_filemanage_title.htm]"
#define WEB_PAGES_P17_NUM				"[p17_login_overtime.htm:Number]"
#define WEB_PAGES_P17					"[p17_login_overtime.htm]"
#define WEB_PAGES_P18_NUM				"[p18_restore_default.htm:Number]"
#define WEB_PAGES_P18					"[p18_restore_default.htm]"
#define WEB_PAGES_P19_NUM				"[p19_time_config.htm:Number]"
#define WEB_PAGES_P19					"[p19_time_config.htm]"
#define WEB_PAGES_P20_NUM				"[p20_history_frame.htm:Number]"
#define WEB_PAGES_P20					"[p20_history_frame.htm]"
#define WEB_PAGES_P21_NUM				"[p21_history_title.htm:Number]"
#define WEB_PAGES_P21					"[p21_history_title.htm]"
#define WEB_PAGES_P22_NUM				"[p22_history_dataquery.htm:Number]"
#define WEB_PAGES_P22					"[p22_history_dataquery.htm]"
#define WEB_PAGES_P23_NUM				"[p23_history_alarmquery.htm:Number]"
#define WEB_PAGES_P23					"[p23_history_alarmquery.htm]"
#define WEB_PAGES_P24_NUM				"[p24_history_logquery.htm:Number]"
#define WEB_PAGES_P24					"[p24_history_logquery.htm]"
#define WEB_PAGES_P25_NUM				"[p25_online_frame.htm:Number]"
#define WEB_PAGES_P25					"[p25_online_frame.htm]"
#define WEB_PAGES_P26_NUM				"[p26_online_title.htm:Number]"
#define WEB_PAGES_P26					"[p26_online_title.htm]"
#define WEB_PAGES_P27_NUM				"[p27_online_modifysystem.htm:Number]"
#define WEB_PAGES_P27					"[p27_online_modifysystem.htm]"
#define WEB_PAGES_P28_NUM				"[p28_online_modifydevice.htm:Number]"
#define WEB_PAGES_P28					"[p28_online_modifydevice.htm]"
#define WEB_PAGES_P29_NUM				"[p29_online_modifyalarm.htm:Number]"
#define WEB_PAGES_P29					"[p29_online_modifyalarm.htm]"
#define WEB_PAGES_P30_NUM				"[p30_acu_signal_value.htm:Number]"
#define WEB_PAGES_P30					"[p30_acu_signal_value.htm]"
#define WEB_PAGES_P31_NUM				"[p31_close_system.htm:Number]"
#define WEB_PAGES_P31					"[p31_close_system.htm]"
#define WEB_PAGES_P32_NUM				"[p32_start_system.htm:Number]"
#define WEB_PAGES_P32					"[p32_start_system.htm]"
#define WEB_PAGES_P33_NUM				"[p33_replace_file.htm:Number]"
#define WEB_PAGES_P33					"[p33_replace_file.htm]"
#define WEB_PAGES_P34_NUM				"[p34_history_batterylogquery.htm:Number]"
#define WEB_PAGES_P34					"[p34_history_batterylogquery.htm]"
#define WEB_PAGES_P47_NUM				"[p47_web_title.htm:Number]"
#define WEB_PAGES_P47					"[p47_web_title.htm]"
#define WEB_PAGES_P79_NUM				"[p79_site_map.htm:Number]"
#define WEB_PAGES_P79					"[p79_site_map.htm]"
#define WEB_PAGES_P80_NUM				"[p80_status_view.htm:Number]"
#define WEB_PAGES_P80					"[p80_status_view.htm]"
#define	WEB_PAGES_ALAI_NUM				"[alai_tree.js:Number]"
#define	WEB_PAGES_ALAI					"[alai_tree.js]"
#define	WEB_PAGES_J09_NUM				"[j09_alai_tree_help.js:Number]"
#define	WEB_PAGES_J09					"[j09_alai_tree_help.js]"
#define	WEB_PAGES_P35_NUM				"[p35_status_switch.htm:Number]"
#define	WEB_PAGES_P35					"[p35_status_switch.htm]"
//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.5.13
#define	WEB_PAGES_P37_NUM				"[p37_edit_config_file.htm:Number]"
#define	WEB_PAGES_P37					"[p37_edit_config_file.htm]"
#define	WEB_PAGES_P38_NUM				"[p38_title_config_file.htm:Number]"
#define	WEB_PAGES_P38					"[p38_title_config_file.htm]"
#define	WEB_PAGES_P39_NUM				"[p39_edit_config_plc.htm:Number]"
#define	WEB_PAGES_P39					"[p39_edit_config_plc.htm]"
#define	WEB_PAGES_P40_NUM				"[p40_cfg_plc_Popup.htm:Number]"
#define	WEB_PAGES_P40					"[p40_cfg_plc_Popup.htm]"
#define WEB_PAGES_P42_NUM               "[p42_edit_config_alarm.htm:Number]"
#define WEB_PAGES_P42                   "[p42_edit_config_alarm.htm]"
#define WEB_PAGES_P41_NUM               "[p41_edit_config_alarmReg.htm:Number]"
#define WEB_PAGES_P41                   "[p41_edit_config_alarmReg.htm]"
#define WEB_PAGES_P43_NUM               "[p43_ydn_config.htm:Number]"
#define WEB_PAGES_P43                   "[p43_ydn_config.htm]"
#define WEB_PAGES_P44_NUM               "[p81_user_def_page.htm:Number]"
#define WEB_PAGES_P44                   "[p81_user_def_page.htm]"
#define	WEB_PAGES_P45_NUM				"[p44_cfg_powersplite.htm:Number]"
#define	WEB_PAGES_P45					"[p44_cfg_powersplite.htm]"
#define	WEB_PAGES_P46_NUM				"[p78_get_setting_param.htm:Number]"
#define	WEB_PAGES_P46					"[p78_get_setting_param.htm]"
#define	WEB_PAGES_P48_NUM				"[p77_auto_config.htm:Number]"
#define	WEB_PAGES_P48					"[p77_auto_config.htm]"

#define WEB_PAGES_P49_NUM               "[copyright.htm:Number]"
#define WEB_PAGES_P49                   "[copyright.htm]"
#define WEB_PAGES_P50_NUM               "[global.css:Number]"
#define WEB_PAGES_P50                   "[global.css]"
#define WEB_PAGES_P51_NUM               "[header.htm:Number]"
#define WEB_PAGES_P51                   "[header.htm]"
#define WEB_PAGES_P52_NUM               "[p01_home.htm:Number]"
#define WEB_PAGES_P52                   "[p01_home.htm]"
#define	WEB_PAGES_P53_NUM				"[p01_home_index.htm:Number]"
#define	WEB_PAGES_P53					"[p01_home_index.htm]"
#define	WEB_PAGES_P54_NUM				"[p01_home_title.htm:Number]"
#define	WEB_PAGES_P54					"[p01_home_title.htm]"
#define	WEB_PAGES_P55_NUM				"[p_main_menu.html:Number]"
#define	WEB_PAGES_P55					"[p_main_menu.html]"

#define	WEB_PAGES_P56_NUM				"[excanvas.js:Number]"
#define	WEB_PAGES_P56					"[excanvas.js]"
#define	WEB_PAGES_P57_NUM				"[line_data.htm:Number]"
#define	WEB_PAGES_P57					"[line_data.htm]"
#define	WEB_PAGES_P58_NUM				"[jquery.emsMeter.js:Number]"
#define	WEB_PAGES_P58					"[jquery.emsMeter.js]"
#define	WEB_PAGES_P59_NUM				"[jquery.emsplot.js:Number]"
#define	WEB_PAGES_P59					"[jquery.emsplot.js]"
#define	WEB_PAGES_P60_NUM				"[jquery.emsplot.style.css:Number]"
#define	WEB_PAGES_P60					"[jquery.emsplot.style.css]"
#define	WEB_PAGES_P61_NUM				"[jquery.emsThermometer.js:Number]"
#define	WEB_PAGES_P61					"[jquery.emsThermometer.js]"
#define	WEB_PAGES_P62_NUM				"[jquery.min.js:Number]"
#define	WEB_PAGES_P62					"[jquery.min.js]"

#define WEB_PAGES_P12_V3_NUM				"[p12_nmsv3_config.htm:Number]"
#define WEB_PAGES_P12_V3				"[p12_nmsv3_config.htm]"

//end////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Netscape Multi_Lang Support
//Added by wj 2007-04-20
#define WEB_N_PAGES_P1_NUM               "[active.htm:Number]"
#define WEB_N_PAGES_P1                   "[active.htm]"
#define WEB_N_PAGES_P2_NUM               "[Alarm.htm:Number]"
#define WEB_N_PAGES_P2                   "[Alarm.htm]"
#define WEB_N_PAGES_P3_NUM               "[Bat_test.htm:Number]"
#define WEB_N_PAGES_P3                   "[Bat_test.htm]"
#define WEB_N_PAGES_P4_NUM               "[bat_test_log.htm:Number]"
#define WEB_N_PAGES_P4                   "[bat_test_log.htm]"
#define WEB_N_PAGES_P5_NUM               "[bottom.htm:Number]"
#define WEB_N_PAGES_P5                   "[bottom.htm]"
#define WEB_N_PAGES_P6_NUM               "[command_log.htm:Number]"
#define WEB_N_PAGES_P6                   "[command_log.htm]"
#define WEB_N_PAGES_P7_NUM               "[contents.htm:Number]"
#define WEB_N_PAGES_P7                   "[contents.htm]"
#define WEB_N_PAGES_P8_NUM               "[Control_record.htm:Number]"
#define WEB_N_PAGES_P8                   "[Control_record.htm]"
#define WEB_N_PAGES_P9_NUM               "[Data_record.htm:Number]"
#define WEB_N_PAGES_P9                   "[Data_record.htm]"
#define WEB_N_PAGES_P10_NUM               "[eemconf_general.htm:Number]"
#define WEB_N_PAGES_P10                   "[eemconf_general.htm]"
#define WEB_N_PAGES_P11_NUM               "[eemconf_leasedline.htm:Number]"
#define WEB_N_PAGES_P11                   "[eemconf_leasedline.htm]"
#define WEB_N_PAGES_P12_NUM               "[eemconf_pstn.htm:Number]"
#define WEB_N_PAGES_P12                   "[eemconf_pstn.htm]"
#define WEB_N_PAGES_P13_NUM               "[eemconf_tcpip.htm:Number]"
#define WEB_N_PAGES_P13                   "[eemconf_tcpip.htm]"
#define WEB_N_PAGES_P14_NUM               "[eemsettings.htm:Number]"
#define WEB_N_PAGES_P14                   "[eemsettings.htm]"
#define WEB_N_PAGES_P15_NUM               "[equip.htm:Number]"
#define WEB_N_PAGES_P15                   "[equip.htm]"
#define WEB_N_PAGES_P16_NUM               "[error.htm:Number]"
#define WEB_N_PAGES_P16                   "[error.htm]"
#define WEB_N_PAGES_P17_NUM               "[history_alarm.htm:Number]"
#define WEB_N_PAGES_P17                   "[history_alarm.htm]"
#define WEB_N_PAGES_P18_NUM               "[history_data.htm:Number]"
#define WEB_N_PAGES_P18                   "[history_data.htm]"
#define WEB_N_PAGES_P19_NUM               "[history_log.htm:Number]"
#define WEB_N_PAGES_P19                   "[history_log.htm]"
#define WEB_N_PAGES_P20_NUM               "[index.htm:Number]"
#define WEB_N_PAGES_P20                   "[index.htm]"
#define WEB_N_PAGES_P21_NUM               "[ipconf.htm:Number]"
#define WEB_N_PAGES_P21                   "[ipconf.htm]"
#define WEB_N_PAGES_P22_NUM               "[logmainteneance.htm:Number]"
#define WEB_N_PAGES_P22                   "[logmainteneance.htm]"
#define WEB_N_PAGES_P23_NUM               "[main.htm:Number]"
#define WEB_N_PAGES_P23                   "[main.htm]"
#define WEB_N_PAGES_P24_NUM               "[mainhigh.htm:Number]"
#define WEB_N_PAGES_P24                   "[mainhigh.htm]"
#define WEB_N_PAGES_P25_NUM               "[othermaintenance.htm:Number]"
#define WEB_N_PAGES_P25                   "[othermaintenance.htm]"
#define WEB_N_PAGES_P26_NUM               "[passwd.htm:Number]"
#define WEB_N_PAGES_P26                   "[passwd.htm]"
#define WEB_N_PAGES_P27_NUM               "[RectifierList.htm:Number]"
#define WEB_N_PAGES_P27                   "[RectifierList.htm]"
#define WEB_N_PAGES_P28_NUM               "[snmpconf.htm:Number]"
#define WEB_N_PAGES_P28                   "[snmpconf.htm]"
#define WEB_N_PAGES_P29_NUM               "[system.htm:Number]"
#define WEB_N_PAGES_P29                   "[system.htm]"
#define WEB_N_PAGES_P30_NUM               "[system_log.htm:Number]"
#define WEB_N_PAGES_P30                   "[system_log.htm]"
#define WEB_N_PAGES_P31_NUM               "[systemsettings.htm:Number]"
#define WEB_N_PAGES_P31                   "[systemsettings.htm]"
#define WEB_N_PAGES_P32_NUM               "[timecheck.js:Number]"
#define WEB_N_PAGES_P32                   "[timecheck.js]"
#define WEB_N_PAGES_P33_NUM               "[timeconf.htm:Number]"
#define WEB_N_PAGES_P33                   "[timeconf.htm]"
#define WEB_N_PAGES_P34_NUM               "[top.htm:Number]"
#define WEB_N_PAGES_P34                   "[top.htm]"
#define WEB_N_PAGES_P35_NUM               "[toplow.htm:Number]"
#define WEB_N_PAGES_P35                   "[toplow.htm]"
#define WEB_N_PAGES_P36_NUM               "[sitemap.htm:Number]"
#define WEB_N_PAGES_P36                   "[sitemap.htm]"
#define WEB_N_PAGES_P37_NUM               "[sitelanguage.htm:Number]"
#define WEB_N_PAGES_P37                   "[sitelanguage.htm]"
#define WEB_N_PAGES_P38_NUM               "[recover.htm:Number]"
#define WEB_N_PAGES_P38                   "[recover.htm]"
#define WEB_N_PAGES_P39_NUM               "[restart.htm:Number]"
#define WEB_N_PAGES_P39                   "[restart.htm]"
#define WEB_N_PAGES_P40_NUM               "[clear_bat_log.htm:Number]"
#define WEB_N_PAGES_P40                   "[clear_bat_log.htm]"
#define WEB_N_PAGES_P41_NUM               "[errorcontrol.htm:Number]"
#define WEB_N_PAGES_P41                   "[errorcontrol.htm]"





//////////////////////////////////////////////////////////////////////////




/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>



#define SET_RO_COMMUNITY				"SetROCommunity"
#define SET_RW_COMMUNITY				"SetRWCommunity"
#define SET_TRAP_COMMUNITY				"SetTrapCommunity"
#define SET_TRAP_ONE1					"SetTrapOne1"
#define SET_TRAP_ONE2					"SetTrapOne2"
#define SET_TRAP_ONE3					"SetTrapOne3"
#define SET_TRAP_ONE4					"SetTrapOne4"
#define SET_TRAP_TWO1					"SetTrapTwo1"
#define SET_TRAP_TWO2					"SetTrapTwo2"
#define SET_TRAP_TWO3					"SetTrapTwo3"
#define SET_TRAP_TWO4					"SetTrapTwo4"
#define SET_TRAP_THREE1					"SetTrapThree1"
#define SET_TRAP_THREE2					"SetTrapThree2"
#define SET_TRAP_THREE3					"SetTrapThree3"
#define SET_TRAP_THREE4					"SetTrapThree4"


static void Web_SET_PostNMSPage(IN  char *pGetBuf, IN int iLanguage);
static int Web_SET_GetCommandParam(OUT char **pBuf);
static char *Web_SET_SendConfigureCommand(IN char *szBuf);


int main(void)
{

	int							iLanguage = 0;				
	//char						*pHtml = NULL;
	//char						szExchange[8];
	char						*szGetBuf = NULL;
	char						*szReturnBuf = NULL;
	int							iAuthority = 0;
	char						*szSessName = NULL ;

	printf("Content-type:text/html\n\n");
	
	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");

	//Authority check
/*	if(Web_CheckUser(WEB_NET_PAGES_WRITE)  == FALSE)
	{
		return FALSE;
	}
*/
	/*get parameter*/
	if (Web_SET_GetCommandParam(&szGetBuf) != FALSE)
	{
		if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf)) != NULL)
		{
			//Web_SET_PostNMSPage(szReturnBuf, iLanguage);
		}
	}
	return TRUE;

}

static char *Web_SET_SendConfigureCommand(IN char *szBuf)	//iModifyPassword = 13:Modify password
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	int		iBufCount = 0;
	//TRACE("szBuf : %s\n", szBuf);
	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return NULL;
	}
	 
	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return NULL;
	}
	
	/*start buffer with pid and a blank*/
	//iLen = sprintf(szBuf1,"%10ld%2d%2d%-128s",(long)getpid(), WEB_SUPPORT_NETSCAPE,iModifyPassword, szBuf);
	iLen = sprintf(szBuf1,"%10ld%2d%2d%2d%-128s",(long)getpid(), WEB_SUPPORT_NETSCAPE,SET_NMS_SETTING, 1,szBuf);
	
	if((write(fd, (void *)szBuf1, iLen + 1))<0)
	{
		close(fd);
		return NULL;
	}
	if((fd2 = open(fifoname,O_RDONLY))<0)
	{
		return NULL;
	}

	
	if((iLen = read(fd2, szBuf2, PIPE_BUF - 1)) > 0)
	{
		Web_SET_PostNMSPage(szBuf2, 1);
	}


	close(fd2);
	close(fd);
	unlink(fifoname);

	return NULL;
}




static int Web_SET_GetCommandParam(OUT char **pBuf)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL, *val1 = NULL, *val2 = NULL, *val3 = NULL, *val4 = NULL;
	char	*szSendBuf = NULL;
	int		iLen = 0;

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}
		 
	szSendBuf = NEW(char, 256);
	if(szSendBuf == NULL)
	{
		return FALSE;
	}

	if((val = getValue(getvars,postvars,SET_RO_COMMUNITY)) != NULL)
	{
		iLen += sprintf(szSendBuf + iLen,"%s",val);

	}
	else
	{
		iLen += sprintf(szSendBuf + iLen,"%s","public");
	}


	if((val = getValue( getvars, postvars, SET_RW_COMMUNITY )) != NULL)
	{
		iLen += sprintf(szSendBuf + iLen,";%s",val);
	}
	else
	{
		iLen += sprintf(szSendBuf + iLen,";%s","private");
	}

	if((val = getValue( getvars, postvars, SET_TRAP_COMMUNITY )) != NULL)
	{
		iLen += sprintf(szSendBuf + iLen,";%s",val);
	}
	else
	{
		iLen += sprintf(szSendBuf + iLen,";%s"," ");
	}

	//get trap1 
	if( (val1 = getValue( getvars, postvars, SET_TRAP_ONE1 )) != NULL
		&& (val2 = getValue( getvars, postvars, SET_TRAP_ONE2 )) != NULL
		&& (val3 = getValue( getvars, postvars, SET_TRAP_ONE3 )) != NULL
		&& (val4 = getValue( getvars, postvars, SET_TRAP_ONE4 )) != NULL)
	{
		if(IsValidWORD(Cfg_RemoveWhiteSpace(val1))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val2))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val3))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val4)))
		{
			iLen += sprintf(szSendBuf + iLen,";%s.%s.%s.%s",val1,val2,val3,val4);
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";0.0.0.0");
		}
	}
	else
	{
		iLen += sprintf(szSendBuf + iLen,";0.0.0.0");
	}

	//get trap2 
	if((val1 = getValue( getvars, postvars, SET_TRAP_TWO1 )) != NULL
		&& (val2 = getValue( getvars, postvars, SET_TRAP_TWO2 )) != NULL
		&& (val3 = getValue( getvars, postvars, SET_TRAP_TWO3 )) != NULL
		&& (val4 = getValue( getvars, postvars, SET_TRAP_TWO4 )) != NULL)
	{
		if(IsValidWORD(Cfg_RemoveWhiteSpace(val1))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val2))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val3))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val4)))
		{
			iLen += sprintf(szSendBuf+ iLen,";%s.%s.%s.%s",val1,val2,val3,val4);
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";0.0.0.0");
		}
	}
	else
	{
		iLen += sprintf(szSendBuf + iLen,";0.0.0.0");
	}

	//get trap3 
	if((val1 = getValue( getvars, postvars, SET_TRAP_THREE1 )) != NULL
		&& (val2 = getValue( getvars, postvars, SET_TRAP_THREE2 )) != NULL
		&& (val3 = getValue( getvars, postvars, SET_TRAP_THREE3 )) != NULL
		&& (val4 = getValue( getvars, postvars, SET_TRAP_THREE4 )) != NULL)
	{
		if(IsValidWORD(Cfg_RemoveWhiteSpace(val1))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val2))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val3))
			&& IsValidWORD(Cfg_RemoveWhiteSpace(val4)))
		{
			iLen += sprintf(szSendBuf + iLen,";%s.%s.%s.%s",val1,val2,val3,val4);
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";0.0.0.0");
		}
	}
	else
	{
		iLen += sprintf(szSendBuf + iLen,";0.0.0.0");
	}

	iLen += sprintf(szSendBuf + iLen,"%s",";");

 	*pBuf = szSendBuf;
	cleanUp(getvars, postvars);

    return TRUE;  
}


static void Web_SET_PostNMSPage(IN  char *pGetBuf, IN int iLanguage)
{

//�ѷ��صĽ��д��error.htm�ļ���,������/var/netscapeĿ¼��
	//���ļ�
#define CGI_NET_PATH_ERROR_FILE			"/app/www/netscape/pages/error.htm"	
#define CGI_NET_CONTROL_VALUE			"/*[ID_RETURN]*/"
	char	*pHtml = NULL;
 	
	if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
	{
		printf("Content-type:text/html\n\n");
		ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, pGetBuf);
		
		PostPage(pHtml);
		DELETE(pHtml);
		pHtml = NULL;
	}

	
}


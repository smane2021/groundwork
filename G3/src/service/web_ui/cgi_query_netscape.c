/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query_netscape.c
 *  CREATOR  : Wan Kun              DATE: 2007-01-11 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"
#include "cgi_pub.h"
#include "cgivars.h"
#define CGI_PUB_SESSIONID				"sessionId"
#define CGI_PUB_EQUIPID					"equipID"
#define CGI_PUB_LANGUAGE				"language_type"
#define CGI_QUERY_TYPE					"_data_type"
#define CGI_QUERY_FROMTIME				"_timeFrom"
#define CGI_QUERY_TOTIME				"_timeTo"
#define CGI_QUERY_BUFFER_LEN			4
//#define RETURN_QUERY_RESULT				"return_query_result"

/*For returning data*/
#define WEB_QUE_BATTERYTEST_LOG			"battery_test_log"
#define WEB_QUE_BATTERYTEST_SUMMARY		"battery_test_summary"
#define WEB_QUE_BAT_TEST_SUM			"bat_test_sum"
#define WEB_QUE_ALARM_DATA_QUERY		"history_alarm_info"
#define WEB_QUE_HISDATA_QUERY			"history_data_info"
#define WEB_QUE_CONTRL_LOG_NETSCAPE		"control_data_info"
#define WEB_QUE_SYSTEM_LOG				"history_log_info"
//#define WEB_QUE_STATDATA_QUERY			"stat_result"
//#define WEB_QUE_DISEL_TEST				"disel_test_log"

typedef struct tagQueryParameter
{
	int			iQueryType;				/*	0 : QUERY_HIS_DATA 
											1: QUERY_STAT_DATA 
											2: QUERY_HISALARM_DATA 
											3: QUERY_CONTROLCOMMAND_DATA 
											4: QUERY_BATTERYTEST_DATA 
											5: QUERY_HISLOG_DATA*/
	char		szFromTime[32];
	char		szToTime[32];
	char		szSessionID[64];
	int			iEquipID;
	int			iLanguage;
	int			iControl;				/* = HISTORY_QUERY */

}STRU_QUERY_PARAMETER;

static int Web_QUE_sendQueryCommand(IN STRU_QUERY_PARAMETER *pCommandParam, OUT char **ppBuf);
static int Web_QUE_GetCommandParam(OUT STRU_QUERY_PARAMETER *pBuf);
static char *Web_QUE_GetBattTestLog(IN int iQueryDataType, IN char *pBuf);
static char html_path[7][128] = {"Data_record.htm",
								"Alarm.htm",
								"Control_record.htm",
								"System_log.htm",
								"Bat_test.htm",
								"bat_test_log.htm",
								"error.htm"};


int main(void)
{
	STRU_QUERY_PARAMETER	stQueryBuf;
	int						iLanguage;				
	char					*pHtml = NULL;
	//char					szExchange[8];
	//int						iAuthority = 0;
	char					*pQueryReturnData = NULL;
	int						i = 0;
	char					*szPath = NULL;

	printf("Content-type:text/html\n\n");
	/*get parameter*/
	if (Web_QUE_GetCommandParam(&stQueryBuf) == FALSE)
	{
		return FALSE;
	}

	/*language type*/
	/*--------------------
	if(stQueryBuf.iLanguage > 0)
	{
		iLanguage =	LOCAL_LANGUAGE_NAME;
	}
	else
	{
		iLanguage = ENGLISH_LANGUAGE_NAME;
	}
	----------------------*/
		/*Judge which file will be loaded*/
	
	if(stQueryBuf.iQueryType == QUERY_HIS_DATA || 
				stQueryBuf.iQueryType == QUERY_STAT_DATA)
	{
		i = 0;	// data query
	}
	else if(stQueryBuf.iQueryType == QUERY_HISALARM_DATA)
	{
		i = 1;	// alarm query
	}
	else if(stQueryBuf.iQueryType == QUERY_CONTROLCOMMAND_DATA || 
				stQueryBuf.iQueryType == QUERY_DISEL_TEST)
    {
		i = 2;	// log query
	}
	else if(stQueryBuf.iQueryType == QUERY_HISLOG_DATA)
	{
		i = 3;	// history log query
	}
	else if(stQueryBuf.iQueryType == QUERY_BATTERYTEST_DATA)
	{
		i = 4;	// battery test log query
	}
	else if(stQueryBuf.iQueryType == QUERY_BATTERYTEST_SUM)
	{
		i = 5;	// battery test log query
	}
	else if(stQueryBuf.iQueryType == QUERY_CLEAR_ALARM || stQueryBuf.iQueryType == CLEAR_HISTORY_DATA
		||stQueryBuf.iQueryType == CLEAR_HISTORY_LOG || stQueryBuf.iQueryType == CLEAR_COMMAND_LOG || stQueryBuf.iQueryType == CLEAR_BAT_TEST_LOG)
	{
		i = 6;
	}
	else 
	{
		return FALSE;
	}
	
	//szPath = Web_SET_MakeConfigurePagePath(html_path[i], iLanguage);
	
	szPath = NEW(char, 50);
	sprintf(szPath,"/app/www/netscape/pages/%s",html_path[i]);
	//printf("-------szpath is -------------%s",szPath);
	
	

	if(LoadHtmlFile(szPath, &pHtml ) <= 0 )
    {
		AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Error : Fail to load file [%s]", html_path[i]);
		DELETE(szPath);
		szPath = NULL;
        return FALSE;
    }
	
	
	DELETE(szPath);
	szPath = NULL;

	

	/*send command*/
	if(Web_QUE_sendQueryCommand(&stQueryBuf,&pQueryReturnData) == FALSE)
	{
		AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Fail to send ACU controlCommand");
		return FALSE;
	}
	




	char	szReturnResult[3];
	int		iReturnResult = 0;
	char	*pDeletePtr = NULL;
	pDeletePtr = pQueryReturnData;
	strncpyz(szReturnResult, pQueryReturnData, 3);
	iReturnResult = atoi(szReturnResult);
	//pQueryReturnData = pQueryReturnData + 2;


	

	

	if(stQueryBuf.iQueryType == QUERY_BATTERYTEST_DATA)
	{
		char	*pGetBuf = NULL;
		
		if(iReturnResult == 0)
		{
			pQueryReturnData = pQueryReturnData + 2;
			pGetBuf = Web_QUE_GetBattTestLog(0, pQueryReturnData);
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_BATTERYTEST_SUMMARY), pGetBuf);
			DELETE(pGetBuf);
			pGetBuf = NULL;

			pGetBuf = Web_QUE_GetBattTestLog(1, pQueryReturnData);
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_BATTERYTEST_LOG), pGetBuf);
			DELETE(pGetBuf);
			pGetBuf = NULL;	
			
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "6");
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}
	}
	else if(stQueryBuf.iQueryType == QUERY_BATTERYTEST_SUM)
	{
		//printf("in  QUERY_BATTERYTEST_SUM,iReturnResult= %d\n",iReturnResult);
		if(iReturnResult == 0)
		{
			pQueryReturnData = pQueryReturnData + 2;
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_BAT_TEST_SUM), 
														pQueryReturnData);

			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}
	}
	else if(stQueryBuf.iQueryType == QUERY_CONTROLCOMMAND_DATA)
	{
		if(iReturnResult == 0)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_CONTRL_LOG_NETSCAPE), 
														pQueryReturnData);

			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}
	}
	else if(stQueryBuf.iQueryType == QUERY_HISLOG_DATA)
	{
		if(iReturnResult == 0)
		{
			pQueryReturnData = pQueryReturnData + 2;
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "7");
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_SYSTEM_LOG), 
															pQueryReturnData);
			
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}
	}
	else if(stQueryBuf.iQueryType == QUERY_DISEL_TEST)
	{
		
/*		
		if(iReturnResult == 0)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "6");
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_DISEL_TEST), 
															pQueryReturnData);
			
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}*/
		//exit(1);
	}
	else if(stQueryBuf.iQueryType == QUERY_HIS_DATA)
	{
		if(iReturnResult == 1)
		{
			pQueryReturnData = pQueryReturnData + 2;
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_HISDATA_QUERY), 
															pQueryReturnData);
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															"1");
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}
	}
	else if(stQueryBuf.iQueryType == QUERY_STAT_DATA)
	{
	/*	if(iReturnResult == 1)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_STATDATA_QUERY), 
															pQueryReturnData);
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
																		"1");
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}*/
	}
	else if(stQueryBuf.iQueryType == QUERY_HISALARM_DATA)
	{
		if(iReturnResult == 0)
		{
			if(pQueryReturnData == NULL)
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															"6");
			}
			else
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_ALARM_DATA_QUERY), 
															pQueryReturnData);
			}
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}
	
	}
	else if(stQueryBuf.iQueryType == QUERY_CLEAR_ALARM || stQueryBuf.iQueryType == CLEAR_HISTORY_DATA 
		||stQueryBuf.iQueryType == CLEAR_HISTORY_LOG || stQueryBuf.iQueryType == CLEAR_COMMAND_LOG || stQueryBuf.iQueryType == CLEAR_BAT_TEST_LOG)
	{
		//printf("iReturnResult:  %d\n", iReturnResult);
		if(iReturnResult ==0)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("ID_RETURN"), "1");
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("ID_RETURN"), "0");
		}
	}
	else if(stQueryBuf.iQueryType == QUERY_DISEL_TEST)
	{
		/*if(iReturnResult == 1)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_QUE_DISEL_TEST), 
															pQueryReturnData);
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
																		"1");
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), 
															szReturnResult);
		}*/
	}
	else
	{
		//return FALSE;
	}
	/*modify page time*/
	time_t now = time(NULL);
    srand( (unsigned int)now );
	//MakeModiTime(strModiTime, (int)strlen(strModiTime));
	
	/*modify the head of the page*/
    //StuffHead(szHead, strlen(pHtml));
	/*get control result*/
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(RETURN_QUERY_RESULT), "1");
	//PostPage(szHead);
	PostPage(pHtml);
//#ifdef _DEBUG
//	FILE *fp = fopen("/acu/www/html/sh.htm","w");
//	fwrite(pHtml,strlen(pHtml), 1, fp);
//	fclose(fp);
//#endif

	DELETE(pHtml);
	pHtml = NULL;
	
	DELETE(pDeletePtr);
	pDeletePtr = NULL;
	
	return TRUE;
}


static int Web_QUE_sendQueryCommand(IN STRU_QUERY_PARAMETER *pCommandParam, 
									OUT char **ppBuf)
{

	int				fd,fd2;    //fifo handle
	int				iLen;
	char			buf[256],buf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t			mode = 0666;
	int				iBufCount = 0;
	char			*pBuf;

	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY))<0)
	{
#ifdef _DEBUG
		AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Error : Fail to open FIFO [%s]",MAIN_FIFO_NAME);
#endif
		return FALSE;
	}

	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH,(long)getpid());
	 
	if((mkfifo(fifoname,mode))<0)
	{
#ifdef _DEBUG
		AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Error : Fail to read FIFO [%s]", fifoname);
#endif
 		return FALSE;
	}
	 
	/*[0]PID [1] QUERY DATA [2]QUERY TYPE [3]Start time [4] end time [5]equip ID  [6]iLanguage*/
	
	iLen = sprintf(buf,"%10ld%2d%2d%32s%32s%8d%2d",(long)getpid(), 
												pCommandParam->iControl,
												pCommandParam->iQueryType,
												pCommandParam->szFromTime,
												pCommandParam->szToTime,
												pCommandParam->iEquipID,
												pCommandParam->iLanguage);

	//printf("buf: %s\n", buf);
	//return FALSE;

	if((write(fd,buf,(unsigned int)(iLen + 1)))<0)
	{
#ifdef _DEBUG
		AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Error : Fail to write FIFO ");
#endif
		close(fd);
		return FALSE;
	}

	if((fd2 = open(fifoname,O_RDONLY))<0)
	{
		AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Fail to open FIFO fail [%s]", MAIN_FIFO_NAME);
		return FALSE;
	}

	pBuf = NEW(char,CGI_QUERY_BUFFER_LEN * PIPE_BUF);
	ASSERT(pBuf);
	
	//FILE *fp = fopen("/acu/www/html/realtime.htm","w");
 

	while((iLen = read(fd2,buf2,PIPE_BUF - 1)) > 0)
	{
		/*fwrite(pBuf,strlen(pBuf), 1, fp);*/
		if(iBufCount >= CGI_QUERY_BUFFER_LEN)
		{
       		pBuf = RENEW(char, pBuf, (iBufCount + 1) * PIPE_BUF);
		}
		strcat(pBuf, buf2);
		iBufCount++;
	}
	//fwrite(pBuf,strlen(pBuf), 1, fp);
	//fclose(fp);
	//AppLogOut(CGI_APP_LOG_QUERY_NAME, APP_LOG_WARNING,"Info : Get string %s", pBuf);
	//printf("received data is %s",pBuf);
	*ppBuf = pBuf;

	close(fd2);
	close(fd);
	unlink(fifoname);

	return TRUE;
}
 
 
/*==========================================================================*
 * FUNCTION :  Web_QUE_GetCommandParam
 * PURPOSE  :  get control param from CGI
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  OUT STRU_CONTROL_COMMAND *pBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/
static int Web_QUE_GetCommandParam(OUT STRU_QUERY_PARAMETER *stQueryParam)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val;

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
        postvars = getPOSTvars();
		//printf("now in post proc:\n");
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
		//printf("now in get proc:\n");
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}

    stQueryParam->iControl = HISTORY_QUERY_NETSCAPE;
    
	val = getValue( getvars, postvars, CGI_PUB_EQUIPID );
	//printf("CGI_PUB_EQUIPID : %s\n",val);
    if( val != NULL )
    {
        stQueryParam->iEquipID = atoi(val) ; // 
		if((val = getValue( getvars, postvars, CGI_QUERY_TYPE )) != NULL)
		{
			/*	[0]:History data
				[1]:Stat data
				[2]:History alarm data
				[3]:Control log data
				[4]:Battery test data
				[5]:His Log data	
				[6]:Clear alarm log
				[7]:Disel log*/
			stQueryParam->iQueryType = atoi(val);
			//printf("iQueryType : %s\n",val);
		}

		if((val = getValue( getvars, postvars, CGI_QUERY_FROMTIME )) != NULL)
		{
			strncpyz(stQueryParam->szFromTime,val,sizeof(stQueryParam->szFromTime));
			//printf("szFromTime : %s\n",val);
		}
		
		if((val = getValue( getvars, postvars, CGI_QUERY_TOTIME)) !=NULL) 
		{
			strncpyz(stQueryParam->szToTime,val,sizeof(stQueryParam->szToTime));
			//printf("szToTime : %s\n",val);
		}

		if((val = getValue( getvars, postvars, SESSION_ID )) != NULL ) // sessionId
        {
			strncpyz(stQueryParam->szSessionID,val,sizeof(stQueryParam->szSessionID));  
			//printf("iSessionID : %s\n",val);
	    }
		
		if((val = getValue(getvars,postvars,LANGUAGE_TYPE)) != NULL)
		{
			stQueryParam->iLanguage = atoi(val);
			//printf("iLanguage : %s\n",val);
		}
		
   }
	
	
   cleanUp(getvars, postvars);

   return TRUE;  
}

static char *Web_QUE_GetBattTestLog(IN int iQueryDataType, IN char *pBuf)
{
	char	*pFirstPtr = NULL, *pLastPtr = NULL;
	int		iPosition = 0;
	char	*pNewBuffer = NULL;
	ASSERT(pBuf);
    
	if(iQueryDataType == 0)
	{
		pFirstPtr =  pBuf;
		pLastPtr= strchr(pBuf, 59);
		iPosition = pLastPtr - pFirstPtr;
		if(iPosition > 0)
		{
			pNewBuffer = NEW(char, iPosition + 1);
			ASSERT(pNewBuffer);

			strncpyz(pNewBuffer, pBuf, iPosition + 1);
			//printf("when select 0, then:%s",pNewBuffer);
		}
	}
	else
	{
		pFirstPtr = strchr(pBuf, 59);
		ASSERT(pFirstPtr);
		pFirstPtr = pFirstPtr + 1;
		iPosition = strlen(pFirstPtr);

		if(iPosition > 0)
		{
			pNewBuffer = NEW(char, iPosition + 1);
			ASSERT(pNewBuffer);

			strncpyz(pNewBuffer, pFirstPtr, iPosition + 1);
		}
		//printf("when select 1, then:%s",pNewBuffer);
	}
	return pNewBuffer;
}

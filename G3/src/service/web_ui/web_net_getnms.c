/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static void Web_SET_PostNMSPage(IN char *ptrBuff);
static void *Web_SET_SendConfigureCommand(IN int nCommandType);
static int Web_SET_GetCommandParam(void);
char		szUserInfo[32];

int main(void)
{
	int							nCommandType = 0;

	printf("Content-type:text/html\n\n");

	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");
	memset(szUserInfo, 0x0, sizeof(szUserInfo));

	/*get parameter*/
	if ((nCommandType = Web_SET_GetCommandParam()) > 0)
	{
		if(Web_SET_SendConfigureCommand(nCommandType) != NULL)
		{
			//Web_SET_PostNMSPage(szReturn);
		}
	}
	else
	{
	}
	
	return TRUE;

}

static void *Web_SET_SendConfigureCommand(IN int nCommandType)	
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	int		iBufCount = 0;
	int		iModifyPassword = 0;

	/*create FIFO with our PID as part of name*/

 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return -1;
	}

	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return -1;
	}
	
	/*start buffer with pid and a blank*/
	iLen = sprintf(szBuf1,"%10ld%2d%2d%-8d",(long)getpid(), WEB_SUPPORT_NETSCAPE,GET_NMS_SETTING, nCommandType);
		
	if((write(fd, (void *)szBuf1, iLen + 1))<0)
	{
		close(fd);
		return -1;
	}


	if((fd2 = open(fifoname,O_RDONLY)) < 0)
	{
 		return -1;
	}
	
	if((iLen = read(fd2, szBuf2, PIPE_BUF - 1)) > 0)
	{
		Web_SET_PostNMSPage(szBuf2);
	}

	close(fd2);
	close(fd);
	unlink(fifoname);

	return NULL;
}

static int Web_SET_GetCommandParam(void)
{

	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL;
	int		nCommandType;

    form_method = getRequestMethod();
    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}
	
	char *ptr1 = NULL;
	if((val = getValue( getvars, postvars,  CGI_GET_SETTING_TYPE)) != NULL)
	{
		nCommandType = atoi(val);
	}

	if((val = getValue( getvars, postvars,  CGI_GET_AUTH_NAME)) != NULL)
	{
		sprintf(szUserInfo,"%s", val);
	}

	cleanUp(getvars, postvars);
    return nCommandType;  
}


static char szReturnValue[4][100] = {"No used", "All alarms", "Major alarms","Critical alarms"};

static void Web_SET_PostNMSPage(IN char *ptrBuff)
{
#define DI_PUBLIC_COMMUNITY				"DI_PUBLIC_COMMUNITY"
#define DI_PUBLIC_COMMUNITY2				"DI_PUBLIC_COMMUNITY2"
#define DI_PRIVATE_COMMUNITY			"DI_PRIVATE_COMMUNITY"
#define DI_PRIVATE_COMMUNITY2			"DI_PRIVATE_COMMUNITY2"
#define DI_TRAP_LEVEL					"DI_TRAP_LEVEL"
#define DI_CURRENT_TRAP1				"DI_CURRENT_TRAP1"
#define DI_CURRENT_TRAP2				"DI_CURRENT_TRAP2"
#define DI_CURRENT_TRAP3				"DI_CURRENT_TRAP3"
#define END_OF_NMS						','
//�����ַ���

	char	*pPosition, *ptr;
	int		iPosition, iNMS, iTrapLevel;
	char	*pHtml;
	char	szExchange[128];
 
	ptr = ptrBuff;
	pPosition = strchr(ptr, END_OF_NMS);
	iPosition = pPosition - ptr;
	//NMS Number
	if(iPosition > 0 )
	{
		strncpyz(szExchange, ptr, iPosition + 1);
		iNMS = atoi(szExchange);	
		ptr = ptr + iPosition;
	}
	else
	{
		iNMS = 0;
	}
	ptr = ptr + 1;
	if(LoadHtmlFile("/var/netscape/snmpconf.htm", &pHtml) < 0 )
	{
		return NULL;
	}
	if(iNMS > 0)
	{
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;

		/*Trap Levle*/
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_TRAP_LEVEL), szExchange );
			ptr = ptr + iPosition;
		}
 
		ptr = ptr + 1;

		//Public
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PUBLIC_COMMUNITY), szExchange );
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PUBLIC_COMMUNITY2), szExchange );
			ptr = ptr + iPosition;
		
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PUBLIC_COMMUNITY), "public" );
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PUBLIC_COMMUNITY2), "public" );
		}
		ptr = ptr + 1;
		//Private
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PRIVATE_COMMUNITY), szExchange );
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PRIVATE_COMMUNITY2), szExchange );
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PRIVATE_COMMUNITY), "private" );
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PRIVATE_COMMUNITY2), "private" );
		}
		ptr = ptr + 1;

		//Address 1
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP1), szExchange );
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP1), ptr );
		}
		ptr = ptr + 1;

		//Address 2
		//printf("ptr = %d [%s]\n", iNMS, ptr);
		if(iNMS > 1 )
		{
			if(iNMS > 2)
			{
 				pPosition = strchr(ptr, END_OF_NMS);
				iPosition = pPosition - ptr;
				if(iPosition > 0 )
				{
					strncpyz(szExchange, ptr, iPosition + 1);
					ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP2), szExchange );
					ptr = ptr + iPosition;
					//printf("---------\n");
				}
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP2), ptr );
			}

		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP2), "0.0.0.0" );
		}
		ptr = ptr + 1;
		//Address 3
		//printf("ptr = %d [%s]\n", iNMS, ptr);
		if(iNMS > 2)
		{
			//pPosition = strchr(ptr, END_OF_NMS);
			//iPosition = pPosition - ptr;
			//if(iPosition > 0 )
			//{
				//strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP3), ptr );
				//ptr = ptr + iPosition;
			//	printf("/////////////////\n");
			//}
			//ptr = ptr + 1;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP3), "0.0.0.0" );
		}
	}
	else
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_TRAP_LEVEL), "0");
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PUBLIC_COMMUNITY), "public" );
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PRIVATE_COMMUNITY), "private" );
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PUBLIC_COMMUNITY2), "public" );
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PRIVATE_COMMUNITY2), "private" );
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP1), "0.0.0.0" );
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP2), "0.0.0.0" );
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_CURRENT_TRAP3), "0.0.0.0" );
	}

	if(strncmp(CGI_READ_NAME, szUserInfo, sizeof(CGI_READ_NAME)) == 0)
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_DI_DISABLED), "disabled" );
	}
	else
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_DI_DISABLED), " " );
	}

	PostPage(pHtml);
	DELETE(pHtml);
	return ;
}




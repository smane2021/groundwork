/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>



#define CGI_TIME_TIME							"Date"
#define CGI_TIME_DATE							"Time"



static void Web_SET_PostTimePage(IN  char *pGetBuf, IN int iLanguage);
static int Web_SET_GetCommandParam(OUT char **pBuf);
static char *Web_SET_SendConfigureCommand(IN char *szBuf);
static void Web_SET_PostTimePageError();


int main(void)
{

	int							iLanguage = 0;				
	//char						*pHtml = NULL;
	//char						szExchange[8];
	char						*szGetBuf = NULL;
	char						*szReturnBuf = NULL;
	int							iAuthority = 0;
	char						*szSessName = NULL ;

	printf("Content-type:text/html\n\n");
	
	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");

	/*get parameter*/
	if (Web_SET_GetCommandParam(&szGetBuf) != FALSE)
	{
		if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf)) != NULL)
		{
			//Web_SET_PostIPPage(szReturnBuf, iLanguage);
		}
	}
	else
	{

		Web_SET_PostTimePageError();
	}
	if(szGetBuf != NULL)
	{
		DELETE(szGetBuf);
		szGetBuf = NULL;
	}
	return TRUE;

}

static char *Web_SET_SendConfigureCommand(IN char *szBuf)	
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	int		iBufCount = 0;
	//TRACE("szBuf : %s\n", szBuf);
	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return NULL;
	}
	 
	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return NULL;
	}
	
	/*start buffer with pid and a blank*/
	//iLen = sprintf(szBuf1,"%10ld%2d%2d%-128s",(long)getpid(), WEB_SUPPORT_NETSCAPE,iModifyPassword, szBuf);
	
	iLen = sprintf(szBuf1,"%10ld%2d%2d%2d%-128s",(long)getpid(), WEB_SUPPORT_NETSCAPE,SET_TIME_SETTING, 1,szBuf);
	
	if((write(fd, (void *)szBuf1, iLen + 1))<0)
	{
		close(fd);
		return NULL;
	}
	if((fd2 = open(fifoname,O_RDONLY))<0)
	{
		return NULL;
	}

 
	if((iLen = read(fd2, szBuf2, PIPE_BUF - 1)) > 0)
	{
		Web_SET_PostTimePage(szBuf2, 1);
 
	}


	close(fd2);
	close(fd);
	unlink(fifoname);

	return TRUE;
}




static int Web_SET_GetCommandParam(OUT char **pBuf)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL, *val1 = NULL, *val2 = NULL, *val3 = NULL, *val4 = NULL;
	char	*szSendBuf = NULL;
	int		iLen = 0;
	int		nYear,nMonth,nDay,nHour,nMinute,nSeconds;
 

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}
		 
	szSendBuf = NEW(char, 256);
	if(szSendBuf == NULL)
	{
		return FALSE;
	}

	if((val = getValue( getvars, postvars,  CGI_TIME_TIME)) != NULL)
	{
		sscanf(val,"%d-%d-%d ", &nYear,&nMonth,&nDay);
		//printf("{%d}{%d}{%d}\n", nYear,nMonth,nDay);
		if(nYear <= 2099 && nYear >= 1977 && nMonth >= 1 && nMonth <= 12 && nDay >= 1 && nDay <= 31 )
		{
			iLen += sprintf(szSendBuf + iLen,"%s ",val);
		}
		else
		{
			return FALSE;
		}
	}
	else
	{

		return FALSE;
	}

	if((val = getValue( getvars, postvars,  CGI_TIME_DATE)) != NULL)
	{
		sscanf(val,"%d:%d:%d ", &nHour,&nMinute,&nSeconds);
		//printf("{%d}{%d}{%d}\n", nHour,nMinute,nSeconds);
		if(nHour <= 23 && nHour >= 0 && nMinute >= 0 && nMinute <= 59 && nSeconds >= 0 && nSeconds <= 59 )
		{
			iLen += sprintf(szSendBuf + iLen,"%s ",val);
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}

	*pBuf =  szSendBuf;
	//printf("%s",szSendBuf);
	//exit(1);
    return TRUE;  
}

static void Web_SET_PostTimePage(IN  char *pGetBuf, IN int iLanguage)
{

//�ѷ��صĽ��д��error.htm�ļ���,������/var/netscapeĿ¼��
	//���ļ�
#define CGI_NET_PATH_ERROR_FILE			"/app/www/netscape/pages/error.htm"	
#define CGI_NET_CONTROL_VALUE			"/*[ID_RETURN]*/"
	char	*pHtml = NULL;
 
	if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
	{
		//printf("Content-type:text/html\n\n");
		ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, pGetBuf);
		
		PostPage(pHtml);
		DELETE(pHtml);
		pHtml = NULL;
	}
	
 
	
}

static void Web_SET_PostTimePageError()
{

	//�ѷ��صĽ��д��error.htm�ļ���,������/var/netscapeĿ¼��
	//���ļ�
#define CGI_NET_PATH_ERROR_FILE			"/app/www/netscape/pages/error.htm"	
#define CGI_NET_CONTROL_VALUE			"/*[ID_RETURN]*/"
	char	*pHtml = NULL;

	if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
	{
		//printf("Content-type:text/html\n\n");
		ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, "0");

		PostPage(pHtml);
		DELETE(pHtml);
		pHtml = NULL;
	}



}


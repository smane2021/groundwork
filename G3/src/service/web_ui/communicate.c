 
/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : communicate.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-09-28 10:15
 *  VERSION  : V1.00
 *  PURPOSE  :
 *   
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include <signal.h>
#include "stdsys.h"  


#include <netinet/in.h>			/* sockaddr_in, sockaddr */
#include <arpa/inet.h>			/* inet_ntoa */
#include <sys/resource.h>		/* setrlimit */
#include <sys/types.h>			/* socket, bind, accept */
#include <sys/socket.h>			/* socket, bind, accept, setsockopt, */
#include <netdb.h>
#include <net/route.h>			/*rtentry*/
#include <net/if.h>				/*ifreq */
#include <time.h>

#include <sys/dir.h> 
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>

#include "communicate.h"
//#define EEM_SUPPORT		1
//
//#ifndef EEM_SUPPORT
//#include "../eem_soc/addydn.h"
//#else
#include "../eem_soc/esr.h"
#include "../ydn23/ydn.h"
//#endif

#include "cgi_pub.h"
#include "cgi_realtime.h"

#include "../../mainapp/config_mgmt/cfg_mgmt.h"

//#include "../../lib/data_mgmt/his_data.h"

#define _XOPEN_SOURCE /* glibc2 needs this */
#include <time.h>	// for strptime

char *strptime(const char *s, const char *format, struct tm *tm);

#define AJAX_TEST //AJAX TEST

#ifdef _DEBUG
	#ifndef _SHOW_WEB_INFO
 		#define	_SHOW_WEB_INFO
	#endif
#endif 

//#ifndef _SUPPORT_THREE_LANGUAGE
//
//    #define	_SUPPORT_THREE_LANGUAGE
//
//#endif



//---------------------CR1 Log------------------------------------
//After adding system type, the system type is too long, 
//so change the szStatus[128] to szStatus[1024],change szCtrlExpression[128] to szCtrlExpression[1024]
#define WEB_ADD_SYSTEM_TYPE			1		
//---------------------CR1 Log------------------------------------



/*define the len of the equipid stored in fifo*/



/*for querying log*/
#define BASIC_LOG_FILE			"ACU.log"
#define BASIC_BAK_LOG_FILE		"ACU.bak.log"
#define SUB_DIR_LOG_FILE		"log"

#ifndef ENV_ACU_ROOT_DIR
	#define ENV_ACU_ROOT_DIR		"ACU_ROOT_DIR"	// to use shell env variable.
#endif
static char APP_LOG_FILE[MAX_FILE_PATH] = BASIC_LOG_FILE;

#define SPLITTER					0x09

USER_INFO_STRU	gUserinfo;
EquipList		l_WEB_equipList;
char			*pWriteBuf;
int				iVarSubID = 0;
int				iBufLen = 0;
int				iTimeOut = 0;

/*signal Number*/
int			iSigNumRet = 0;
int			iSampleSigNumRet = 0;
int			iControlSigNumRet = 0;
int			iSettingSigNumRet = 0;
int			iAlarmSigNumRet = 0;
int			iRAlarmSigNumRet = 0;
static int	iStatusSigNum = 4;

//#define APP_LOG_FILE					"/opt/ACU/log/ACU.log"
/*define global var in this file*/

int iAutoCFGStartFlage = 0;
int iSlaveMode = 0;
int iSMDUCFGStatus = 0;

//float				*pStatusSigValue;

/*Read param from Main fifo*/
#define	MAX_SIGNAL_NUM_LEN				4
#define	MAX_EQUIPID_LEN					8
#define MAX_SIGNALID_LEN				8
#define MAX_SIGNALTYPE_LEN				4
#define MAX_CONTROLTYPE_LEN				2
#define MAX_LANGUAGE_TYPE_LEN			2
#define	MAX_SIGNAL_VALUE_TYPE			2
#define MAX_SIGNA_NEW_ALARM_LEVEL		5
#define MAX_NEED_TO_RETURN_STRUCTURE	2
#define MAX_MODIFY_NAME_TYPE			2
#define MAX_CONTROL_VALUE_LEN			100
#define MAX_CONTROL_USER_NAME_LEN		32


#define MAX_SIGNAL_TOTAL_NUM_LEN		5*MAX_SIGNAL_NUM_LEN
#define MAX_SIGNAL_VALUE_LEN			50
#define	MAX_ALARM_VALUE_BUF_LEN			60
#define	MAX_STATUS_VALUE_LEN			40

#define MAX_BUF_LEN					MAX_SIGNAL_VALUE_LEN*iSampleSigNumRet	+	\
									MAX_SIGNAL_VALUE_LEN*iControlSigNumRet	+	\
									MAX_SIGNAL_VALUE_LEN*iSettingSigNumRet	+	\
									MAX_ALARM_VALUE_BUF_LEN*iRAlarmSigNumRet	+	\
									MAX_STATUS_VALUE_LEN*iStatusSigNum	+ 30000	

#define MAX_SIGNAL_STRUCTURE_LEN		600		/*used for returning signal stucture*/

#define MAX_USERNAME_LEN				16
#define MAX_PASSWORD_LEN				16


#define FAULT_SAMPLER_ID				0

/*define authority */
#define NO_THIS_USER					-1
#define NO_MATCH_PASSWORD				-2

#define WAIT_FOR_START                  -3

#define AUTO_CONFIG_START				-4


#define MAX_EQUIP_LIST_DATA_SIZE		2000

//#define MAX_EQUIPLIST_BUFFER_LEN		20000




/*for get fifo head*/
#define MAX_COMM_PID_LEN				10
#define MAX_COMM_GET_TYPE				2

/*for query data*/
#define MAX_COMM_RETURN_DATA_LINE_SIZE	250
#define COMM_QUERY_TYPE_LEN				2
#define COMM_QUERY_TIME_LEN				32
#define MAX_QUERY_BATTERY_GROUP_LEN		4000
#define	MAX_QUERY_BATTERY_GROUP_NUM		48
#define MAX_HIS_DATA_NUM				1000
#define MAX_HIS_DATA_LEN				200
#define MAX_STAT_DATA_NUM				1000
#define MAX_STAT_DATA_LEN				200

#define MAX_ALARM_DATA_NUM				400
#define MAX_ALARM_DATA_LEN				200

#define MAX_CONTROL_DATA_NUM			600
#define MAX_CONTROL_DATA_LEN			480

#define MAX_DATA_LOG_NUM				1000
#define MAX_DATA_LOG_LEN				800 


#define MAX_EQUIP_NUM					1000

/*for returning alarm information*/
#define MAX_LOC_ALARM_BUFFER_LEN		1000
#define MAX_ENG_ALARM_BUFFER_LEN		1000
#define MAX_LOC2_ALARM_BUFFER_LEN		1000

/*for modiying time*/
#define WEB_MODIFY_TIMESRV_IP			1
#define WEB_MODIFY_TIME					2

/*for returning group information*/
#define MAX_LOC_GROUP_LEN				1000
#define MAX_LOC2_GROUP_LEN				1000//added by wj for three languages 2006.4.29
#define MAX_ENG_GROUP_LEN				1000

/*for status bar struc signal buffer len*/
#define MAX_STATUS_STRUC_LEN		100


#define GET_SERVICE_OF_ESR_NAME			"eem_soc.so"
#define GET_SERVICE_OF_NMSV2_NAME		"snmp_agent.so"
#define GET_SERVICE_OF_NMSV3_NAME		"snmpv3_agent.so"
#define GET_SERVICE_OF_YDN_NAME			"ydn23.so"

//for hardware protection return error
#define WEB_RETURN_PROTECTED_ERROR			5
#define WEB_RETURN_NOT_SUPPORT_V3			7


/****************start:  for multilanguage ****************/
/****************start : Macro****************/
#define WEB_DEFAULT_DIR						"/app/www/html/cgi-bin/eng"		/*save english language html file*/
#define WEB_LOCAL_DIR						"/app/www/html/cgi-bin/loc"		/*save local language html file*/
#define WEB_LOCAL2_DIR						"/app/www/html/cgi-bin/loc2"       /*save local2 language html file*///Add by wj for three languages,2006.4.27
#define WEB_TEMPLATE_DIR					"/app/www/html/template"/*store template file*/

//////////////////////////////////////////////////////////////////////////
//netscape
#define WEB_DEFAULT_DIR_N						"/app/www/netscape/pages"		/*save  language html file*/
#define WEB_TEMPLATE_DIR_N					"/app/www/netscape/template"/*store template file*/
//////////////////////////////////////////////////////////////////////////


#define WEB_LOG_DIR							"/var/download.txt"

#define WEB_CFG_NAME						"CfgName"
#define WEB_CFG_VERSION						"CfgVersion"
#define WEB_CFG_DATE						"CfgDate"
#define WEB_CFG_DEFAULT						"default language"
#define WEB_CFG_LOCAL						"Local language"
#define WEB_CFG_ENGINEER					"CfgEngineer"
#define WEB_CFG_DESCRIPTION					"CfgDescription"

#define WEB_NUMBER_OF_CONFIG_INFORMATION	"[NUMBER_OF_CONFIG_INFORMATION]"
#define WEB_CONFIG_INFORMATION				"[CONFIG_INFORMATION]"
#define WEB_PAGES_NUMBER					"[NUM_OF_PAGES]"

#define MAKE_PRIVATE_VAR_FIELD(var)			("[" var "]")

#define	ERR_WEB_OK							0
#define CONFIG_FILE_WEB_PRIVATE				"Web_Resource.res"//"lang/zh/Web_Resource.res"//"private/web/Web_Resource.res"
#define CONFIG_FILE_WEB_PRIVATE_R			"private/web/Web_Res_Ver.cfg"

#define CONFIG_FILE_WEB_N_PRIVATE				"Web_ResourceN.res"//"lang/zh/Web_Resource.res"//"private/web/Web_Resource.res"

////Added by YangGuoxin,23/8/2006
#define ADD_SUPPORT_NETSCAPE
#ifdef  ADD_SUPPORT_NETSCAPE
static BOOL IsValidDatetime(const char *pField);
static BOOL IsValidFloat(const char *pField);
static BOOL IsValidLong(const char *pField);
//static BOOL IsValidWORD(const char *pField);
#endif
////End by YangGuoxin,23/8/2006

//use to WEB_VERSION_INFO
#define	WEB_LOCAL_LANGUAGE					"[LOCAL_LANGUAGE]"
#define	WEB_LOCAL_LANGUAGE_VERSION			"[LOCAL_LANGUAGE_VERSION]"
#define	WEB_LOCAL2_LANGUAGE					"[LOCAL_LANGUAGE]"
#define	WEB_LOCAL2_LANGUAGE_VERSION			"[LOCAL_LANGUAGE_VERSION]"

#define WEB_NET_RESET_TIMER 1

#define SPLITTER							0x09
/****************end : Macro****************/
/****************start :structure ****************/
typedef struct tagWebGeneralResourceInfo
{
	char		szResourceID[33];
	char		*szLocal;
	char		*szDefault;
    char        *szLocal2;  //Add by wj for three languages,2006.4.27
}WEB_GENERAL_RESOURCE_INFO;


typedef struct tagWebTotalResourceInfo
{
	int							iNumber;
	char						szFileName[64];
	WEB_GENERAL_RESOURCE_INFO	*stWebPrivate;
}WEB_PRIVATE_RESOURCE_INFO;

typedef struct tagWebResourceInfo
{
	char	CfgName[33];
	double	CfgVersion;
	char	szCfgTime[32];
	char	szCfgDefault[33];
	char	szCfgLocal[33];
	char	szCfgDescription[128];
}WEB_RESOURCE_INFO;

typedef struct tagWebHeadResourceInfo
{
	int					iNumber;
	WEB_RESOURCE_INFO	*stWebHeadInfo;
}WEB_HEAD_RESOURCE_INFO;

/*read rect id to get rect number*/
typedef struct stWEB_RECT_CONFIG_INFO
{
	int		iEquipID;
	int		iSignalType;
	int		iSignalID;
}WEB_RECT_CONFIG_INFO;

typedef struct stWEB_VERSION_INFO
{
	float	fVersion;
	char	szLanguage[32];
    float	fVersion2;//Add by wj for three languages,2006.4.27
    char	szLanguage2[32];//Add by wj for three languages,2006.4.27

}WEB_VERSION_INFO;

/****************end	:structure ****************/

/****************start	:����****************/
WEB_PRIVATE_RESOURCE_INFO		*pWebCfgInfo = NULL;
WEB_PRIVATE_RESOURCE_INFO       *pWebCfgInfoTemp = NULL; //Add by wj for three languages,2006.4.27
WEB_HEAD_RESOURCE_INFO			*pWebHeadInfo = NULL;
WEB_RECT_CONFIG_INFO			stRectNumber;
WEB_VERSION_INFO				stWebCompareVersion;
WEB_VERSION_INFO				stWebVersionInfo;

int								iPagesNumber = 0;
int								iNPagesNumber = 0;

int                             iStartOK=0;
WEB_PRIVATE_RESOURCE_INFO		*pWebCfgInfoN = NULL;
WEB_HEAD_RESOURCE_INFO			*pWebHeadInfoN = NULL;
WEB_VERSION_INFO				stWebCompareVersionN;
WEB_VERSION_INFO				stWebVersionInfoN;

#define WEB_MAX_HTML_PAGE_NUM				83
#define WEB_PAGES_VERSION_FILE				"/app/config/private/web/Web_Res_Ver.cfg"//"/home/ygx/cfg/cfg_version.cfg"
#define WEB_NETSCAPE_PAGES_VERSION_FILE				"/app/config/private/web/Web_Res_VerN.cfg"//Added by wj for netscape multi_lang

#define WEB_PAGES_VERSION					"[cfg_version]"

static const char szTemplateFile[][64] = {"control_cgi.htm:Number","control_cgi.htm",
				"data_sampler.htm:Number",		"data_sampler.htm",
				"dialog.htm:Number",			"dialog.htm",
				"editsignalname.js:Number",		"editsignalname.js",
				"equip_data.htm:Number",		"equip_data.htm",
				"j01_tree_maker.js:Number",		"j01_tree_maker.js",
				"j02_tree_view.js:Number",		"j02_tree_view.js",
				"j04_gfunc_def.js:Number",		"j04_gfunc_def.js",
				"j05_signal_def.js:Number",		"j05_signal_def.js",
				"j07_equiplist_def.js:Number",	"j07_equiplist_def.js",
				"p36_clear_data.htm:Number",	"p36_clear_data.htm",
				"j31_menu_script.js:Number",	"j31_menu_script.js",
				"j31_table_script.js:Number",	"j31_table_script.js",
				"j50_event_log.js:Number",		"j50_event_log.js",
				"j76_device_def.htm:Number",	"j76_device_def.htm",
				"j77_equiplist_def.htm:Number",	"j77_equiplist_def.htm",
				"login.htm:Number",				"login.htm",
				"p01_main_frame.htm:Number",	"p01_main_frame.htm",
				"p02_tree_view.htm:Number",		"p02_tree_view.htm",
				"p03_main_menu.htm:Number",		"p03_main_menu.htm",
				"p04_main_title.htm:Number",	"p04_main_title.htm",
				"p05_equip_sample.htm:Number",	"p05_equip_sample.htm",
				"p06_equip_control.htm:Number",	"p06_equip_control.htm",
				"p07_equip_setting.htm:Number",	"p07_equip_setting.htm",
				"p08_alarm_frame.htm:Number",		"p08_alarm_frame.htm",
				"p09_alarm_title.htm:Number",		"p09_alarm_title.htm",
				"p10_alarm_show.htm:Number",		"p10_alarm_show.htm",
				"p11_network_config.htm:Number",	"p11_network_config.htm",
				"p12_nms_config.htm:Number",		"p12_nms_config.htm",
				"p13_esr_config.htm:Number",		"p13_esr_config.htm",
				"p14_user_config.htm:Number",		"p14_user_config.htm",
				"p15_show_acutime.htm:Number",		"p15_show_acutime.htm",
				"p16_filemanage_title.htm:Number",	"p16_filemanage_title.htm",
				"p17_login_overtime.htm:Number",	"p17_login_overtime.htm",
				"p18_restore_default.htm:Number",	"p18_restore_default.htm",
				"p19_time_config.htm:Number",		"p19_time_config.htm",
				"p20_history_frame.htm:Number",		"p20_history_frame.htm",
				"p21_history_title.htm:Number",		"p21_history_title.htm",
				"p22_history_dataquery.htm:Number",	"p22_history_dataquery.htm",
				"p23_history_alarmquery.htm:Number","p23_history_alarmquery.htm",
				"p24_history_logquery.htm:Number",	"p24_history_logquery.htm",
				"p25_online_frame.htm:Number",		"p25_online_frame.htm",
				"p26_online_title.htm:Number",		"p26_online_title.htm",
				"p27_online_modifysystem.htm:Number",		"p27_online_modifysystem.htm",
				"p28_online_modifydevice.htm:Number",		"p28_online_modifydevice.htm",
				"p29_online_modifyalarm.htm:Number",		"p29_online_modifyalarm.htm",
				"p30_acu_signal_value.htm:Number",			"p30_acu_signal_value.htm",
				"p31_close_system.htm:Number",				"p31_close_system.htm",
				"p32_start_system.htm:Number",				"p32_start_system.htm",
				"p33_replace_file.htm:Number",				"p33_replace_file.htm",
				"p34_history_batterylogquery.htm:Number",	"p34_history_batterylogquery.htm",
				"p47_web_title.htm:Number",					"p47_web_title.htm",
				"p79_site_map.htm:Number",					"p79_site_map.htm",
				"p80_status_view.htm:Number",				"p80_status_view.htm",
				"alai_tree.js:Number",						"alai_tree.js",
				"j09_alai_tree_help.js:Number",				"j09_alai_tree_help.js",
				"p35_status_switch.htm",					"p35_status_switch.htm",
				"p37_edit_config_file.htm:Number",          "p37_edit_config_file.htm",
				"p38_title_config_file.htm:Number",         "p38_title_config_file.htm",
				"p39_edit_config_plc.htm:Number",           "p39_edit_config_plc.htm",
				"p40_cfg_plc_Popup.htm:Number",             "p40_cfg_plc_Popup.htm",
				"p42_edit_config_alarm.htm:Number",         "p42_edit_config_alarm.htm",
				"p41_edit_config_alarmReg.htm:Number",      "p41_edit_config_alarmReg.htm",
				"p43_ydn_config.htm:Number",                "p43_ydn_config.htm",
				"p81_user_def_page.htm:Number",				"p81_user_def_page.htm",
				"p44_cfg_powersplite.htm:Number",			"p44_cfg_powersplite.htm",
				"p78_get_setting_param.htm:Number",			"p78_get_setting_param.htm",
				"p77_auto_config.htm:Number",				"p77_auto_config.htm",
				"copyright.htm:Number",					"copyright.htm",
				"global.css:Number",					"global.css",
				"header.htm:Number",					"header.htm",
				"p01_home.htm:Number",					"p01_home.htm",
				"p01_home_index.htm:Number",					"p01_home_index.htm",
				"p01_home_title.htm:Number",					"p01_home_title.htm",
				"p_main_menu.html:Number",					"p_main_menu.html",
				"excanvas.js:Number",					"excanvas.js",
				"line_data.htm:Number",					"line_data.htm",
				"jquery.emsMeter.js:Number",					"jquery.emsMeter.js",
				"jquery.emsplot.js:Number",					"jquery.emsplot.js",
				"jquery.emsplot.style.css:Number",				"jquery.emsplot.style.css",
				"jquery.emsThermometer.js:Number",				"jquery.emsThermometer.js",
				"jquery.min.js:Number",					"jquery.min.js",
				"p12_nmsv3_config.htm:Number",		"p12_nmsv3_config.htm"



};

//////////////////////////////////////////////////////////////////////////
//Netscape Multi_Lang Support
#define WEB_MAX_HTML_PAGE_NUM_NETSCAPE				41//netscape pages Num
static const char szTemplateFileN[][64] = {"active.htm:Number","active.htm",
						"Alarm.htm:Number",      "Alarm.htm",
						"Bat_test.htm:Number",      "Bat_test.htm",
						"bat_test_log.htm:Number",  "bat_test_log.htm",
						"bottom.htm:Number",        "bottom.htm",
						"command_log.htm:Number",   "command_log.htm",
						"contents.htm:Number",      "contents.htm",
						"Control_record.htm:Number",    "Control_record.htm",
						"Data_record.htm:Number",   "Data_record.htm",
						"eemconf_general.htm:Number",   "eemconf_general.htm",
						"eemconf_leasedline.htm:Number",    "eemconf_leasedline.htm",
						"eemconf_pstn.htm:Number",      "eemconf_pstn.htm",
						"eemconf_tcpip.htm:Number",     "eemconf_tcpip.htm",
						"eemsettings.htm:Number",       "eemsettings.htm",
						"equip.htm:Number",             "equip.htm",
						"error.htm:Number",             "error.htm",
						"history_alarm.htm:Number",     "history_alarm.htm",
						"history_data.htm:Number",      "history_data.htm",
						"history_log.htm:Number",       "history_log.htm",
						"index.htm:Number",             "index.htm",
						"ipconf.htm:Number",            "ipconf.htm",
						"logmainteneance.htm:Number",   "logmainteneance.htm",
						"main.htm:Number",              "main.htm",
						"mainhigh.htm:Number",          "mainhigh.htm",
						"othermaintenance.htm:Number",  "othermaintenance.htm",
						"passwd.htm:Number",            "passwd.htm",
						"RectifierList.htm:Number",     "RectifierList.htm",
						"snmpconf.htm:Number",          "snmpconf.htm",
						"system.htm:Number",            "system.htm",
						"System_log.htm:Number",        "System_log.htm",
						"systemsettings.htm:Number",    "systemsettings.htm",
						"timecheck.js:Number",          "timecheck.js",
						"timeconf.htm:Number",          "timeconf.htm",
						"top.htm:Number",               "top.htm",
						"toplow.htm:Number",            "toplow.htm",
						"sitemap.htm:Number",           "sitemap.htm",
						"sitelanguage.htm:Number",      "sitelanguage.htm",
						"recover.htm:Number",           "recover.htm",
						"restart.htm:Number",           "restart.htm",
						"clear_bat_log.htm:Number",     "clear_bat_log.htm",
						"errorcontrol.htm:Number",      "errorcontrol.htm"};

//////////////////////////////////////////////////////////////////////////

/***************end	:����****************/
/***************end:  for multilanguage*********************/

/*Battery test*/
typedef struct tagBatteryLog
{
	float		fCurrent;					/*Battery current*/
	float		fVoltage;					/*Battery Voltage*/
	float		fCapacity;					/*Battery capacity*/
	float		fTemperature;				/*Battery temperature*/
	float		fBlockVoltage[8];			/*Block voltage*/
}BATTERY_LOG;

typedef struct tagBatteryData
{
	BYTE		byBatteryDataInfo[256];
}BATTERY_DATA_RECORD;


static const char szUserInfo[4][32]	= {"User","HLMS","NMS","Internal Service"};
static const char szControlResult[2][16] = {"Successful","Fail"};
static const char szAlarmName[4][8] = {"NA","OA","MA","CA"};
 
//static void GetEquipList(OUT EQUIP_INFO **ppBuf);

static int Web_GetSignalNum(IN int iEquipID,
							OUT char **ppBuf);
//static BOOL SetSignal(IN int iEquipID,IN int nTypeID,IN int iSignalID,IN BOOL SetType,IN void *pbuf);
static int Web_GetAuthorityByUser(IN char *pszUserInfo,
								  IN char *pszPasswordInfo);

static int Web_GetACUInfo(OUT SITE_INFO	**ppSiteInfo, 
						  OUT DWORD *pDwGetData, 
						  OUT char szGetStr[128], 
						  OUT char **ppLangCode);

static int Web_MakeACUInfoBuffer(OUT char **ppBuf, IN int iLanguage);
static int Web_ModifyACUInfo(IN char *pNewConfigInfo, 
							 IN int iLanguage, 
							 IN int iModifyType);
static int Web_GetEquipListFromInterface(OUT EQUIP_INFO **ppEquipInfo);

static int Web_MakeEquipListBuffer(OUT char **ppBuf);

static char *Web_GetEquipNameFromInterface(IN int iEquipID,
										   IN int iLanguage);

static int Web_GetStatusData(OUT char **ppBuf);


static int Web_SendEquipList(IN char *buf,
							 IN char *filename);

static int Web_SendRealtimeData(IN char *buf,
								IN char *filename);

static int Web_MakeSampleDataBuf(IN int iLanguage, 
								 IN int iEquipID,
								 IN OUT char **ppBuf);
static int Web_GetEquipType(IN int iEquipID);

static int Web_GetSampleSignalByEquipID(IN int iEquipID, 
										OUT SAMPLE_SIG_VALUE **ppSigValue);

static int Web_GetControlSignalByEquipID(IN int iEquipID, 
										 OUT CTRL_SIG_VALUE **ppSigValue);

static int Web_GetSettingSignalByEquipID(IN int iEquipID, 
										 OUT SET_SIG_VALUE **ppSigValue);

static int Web_GetRealTimeAlarmData(OUT ALARM_SIG_VALUE **ppSigValue);

static int Web_MakeControlDataBuf(IN int iLanguage, 
								  IN int iEquipID,
								  IN OUT char **ppBuf);
static int Web_MakeHisData();

static char *Web_GetSamplerNameById(IN int iLanguage, 
									IN int iSamplerId);

static int Web_MakeSettingDataBuf(IN int iLanguage, 
								  IN int iEquipID,
								  IN OUT char **ppBuf);

static int Web_MakeRAlarmDataBuf(IN int iLanguage, 
								 IN OUT char **ppBuf);
static int Web_GetRectSNIndex(void);
static int Web_GetRectHighSNIndex(void);
static BOOL Web_GetRectHighValid(IN int iEquipID);
static DWORD  Web_GetRectHighSNValue(IN int iEquipID);
static DWORD  Web_GetRectPosition(IN int iEquipID);

static void Web_SortHisAlarmData(HIS_ALARM_TO_RETURN *pHisAlarmToReturn, int iNum);
static int Compare_His_ALARM_SIG_VALUE(const HIS_ALARM_TO_RETURN *p1,
								   const HIS_ALARM_TO_RETURN *p2);
//static int Web_MakeStatusDataBuf(IN int iEquipID,
//								 IN OUT char **ppBuf);

static int Web_GetSamSigStruofEquip(IN int iEquipID,
									OUT void **ppBuf);

static int Web_MakeSamSigStruBufofEquip(IN int iLanguage, 
										IN int iEquipID,
										OUT char **pBuf);

static int Web_GetSetSigStruofEquip(IN int iEquipID,
									OUT SET_SIG_INFO **ppBuf);

static int Web_MakeSetSigStruBufofEquip(IN int iLanguage, 
										IN int iEquipID,
										OUT char **ppBuf);

static int Web_GetConSigStruofEquip(IN int iEquipID,
									OUT void **ppBuf);

static int Web_MakeConSigStruBufofEquip(IN int iLanguage, 
										IN int iEquipID,
										OUT char **pBuf);

static int Web_GetAlarmSigStruofEquip(IN int iEquipID,
									  OUT void **ppBuf);

static int Web_MakeStatusSigStruBufofEquip(IN int iLanguage, 
										   OUT char **ppBuf);

static int Web_MakeRAlarmSigStruBufofEquip(char **ppBuf);

static int Web_SendControlCommandResult(IN char *buf, 
										IN  char *filename);

//static int Web_ModifySignalName(IN int iEquipID,
//								IN int iSignalType,
//								IN int iSignalID,
//								IN const char *szSignalName, 
//								IN int iLanguage);

static int Web_ModifyStdEquipSignalName(IN int iEquipID,
										IN int iSignalType,
										IN int iSignalID,
										IN char *szSignalName, 
										IN int iLanguage,
										IN int iModifyNameType);

static int Web_ModifySignalValue(IN int iEquipID,
								 IN int iSignalType,
								 IN int iSignalID,
								 IN VAR_VALUE_EX value);

static int Web_ModifySignalAlarmLevel(IN int iEquipID,
									  IN int iSignalType,
									  IN int iSignalID,
									  IN int iAlarmLevel,
                                      IN char *szWriteUserName);

static int Web_ModifyEquipName(IN int iEquipID,
							   IN char *pszEquipName,
							   IN int iLanguage,
							   IN int iModifyNameType);

//static int ModifyACUInformation(IN int iModifyType ,IN const char *cNewValue,IN int iLanguage);



static int Web_SendQueryData(IN char *buf, 
							 IN char *filename);

static int Web_QueryHisData(IN time_t toTime,
							IN time_t fromTime,
							IN int iEquipID,
							IN int iDataType, 
							OUT void **ppBuf);

static int Web_QueryHisAlarm(IN time_t fromTime,
							 IN time_t toTime,
							 IN int iEquipID,
							 OUT void **ppBuf);

static int Web_QueryControlCommand(IN time_t fromTime,
								   IN time_t toTime,
								   OUT void **ppBuf);
//static int QueryHisBatteryTest();
static int Web_TransferHisDataIDToName(IN void *pBuf,
									   IN int iDataLen,
									   IN int iLanguage,
									   OUT void **ppBuf);

static int Web_TransferAlarmDataIDToName(IN void *pBuf,
										 IN int iDataLen,
										 IN int iLanguage,
										 OUT void **ppBuf);

static char *Web_GetASignalName(IN int iSignalID, 
								IN int iDataType, 
								IN int iEquipID, 
								IN int iLanguage,
								OUT char **szUnit,
								IN int iStatue,
								IN char **szStatue,
								IN int *iValueType);

static char *Web_GetAEquipName(IN int iEquipID, IN int iLanguage);

static int Web_TransferStatDataIDToName(IN void *pBuf, 
										IN int iDataLen,
										IN int iLanguage,
										OUT void **ppBuf);

static int Web_TransferControlIDToName(IN HIS_CONTROL_RECORD *pBuf,
									   IN int iDataLen,
									   IN int iLanguage,
									   OUT void **ppBuf);

static void Web_TransferGetValue(IN char *szCtrlCmd, 
								 OUT int *iChannel, 
								 OUT char *szValue, 
								 OUT int *iSave);


static int Web_QueryHisLog(IN time_t fromTime, 
						   IN time_t toTime, 
						   OUT void **ppHisLog);

static int Web_MakeQueryHisDataBuffer(IN int iEquipID, 
									  IN time_t fromTime, 
									  IN time_t toTime, 
									  IN int iLanguage, 
									  OUT char **ppBuf);

static int Web_MakeQueryStatDataBuffer(IN int iEquipID, 
									   IN time_t fromTime, 
									   IN time_t toTime, 
									   IN int iLanguage, 
									   OUT char **ppBuf);

static int Web_MakeQueryAlarmDataBuffer(IN int iEquipID, 
										IN time_t fromTime, 
										IN time_t toTime, 
										IN int iLanguage, 
										IN char **ppBuf);

static int Web_MakeQueryControlDataBuffer(IN time_t fromTime, 
										  IN time_t toTime, 
										  IN int iLanguage, 
										  OUT char **ppBuf);

static int Web_MakeQueryHisLogBuffer(IN time_t fromTime,
									 IN time_t toTime,
									 IN char **ppBuf);

static int Web_fnCompareQueryCondition(IN const void *pRecord, 
									   IN const void *pCondition);
						 		
static time_t Web_transferCharToTime(IN char *pTime,IN BOOL bAscending);

static int Web_GetBarcode_Info(IN char **szProductInfo,IN int iLanguage);

static BOOL Web_CheckValid(IN const char *pField);

//////////////////////////////////////////////////////////////////////////
//removed by wj
static void Web_ManageBatteryData(IN int iRecordNum,
								  IN int iQtyBatt,						/*Quantity of  battery*/
								  IN int iQtyRecord,					/*Quantity of record*/
								  BATTERY_GENERAL_INFO stGeneralInfo, 
								  IN void *pBatteryData, 
								  OUT BATTERY_LOG_INFO **ppBatteryRealData);


static int Web_fnCompareBatteryCondition(IN const void *pRecord, 
										 IN const void *pCondition);

//end////////////////////////////////////////////////////////////////////////
BOOL ConvertTime(time_t* pTime, BOOL bUTCToLocal);

static int Web_QueryBatteryTestLog(IN int iQueryTimes, 
								   OUT BATTERY_HEAD_INFO **ppGeneralInfo,
								   OUT BATTERY_LOG_INFO **ppBatteryRealData,
								   OUT  BATTERY_GENERAL_INFO	*stOutBatteryGeneralInfo);


//////////////////////////////////////////////////////////////////////////
//Modified by wj
//static int Web_QueryBatteryTestLog(IN int iQueryTimes, 
//								   OUT BT_LOG_SUMMARY **ppGeneralInfo);
//end////////////////////////////////////////////////////////////////////////


static int Web_MakeQueryBatteryTestLogBuffer(IN int iQueryTimes, 
											 OUT char **ppBuf);

static int Web_MakeAlarmSigStruBufofEquip(IN int iLanguage, 
										  IN int iEquipID,
										  OUT char **ppBuf);

/*for configure*/
static int Web_SendConfigureData(IN char *buf, 
								 IN char *filename);

//static	int GetEquipIDFromName(char *szName);
static int Web_MakeEquipAlarmInfoBuffer(OUT char **ppReturnBuf);
static int Web_MakeGroupDeviceList(OUT char **ppReturnBuf);
static char *Web_MakeNetWorkInfoBuffer(void);
static int Web_Modify_AcuIP_Addr(IN char * szIP);
static char *Web_MakeACUUserInfoBuffer(void);
static int Web_GetUserAuthority(IN char *szUserName);

static char *Web_MakeACUTimeSrv(void);
static int Web_SetACUTimeSrv(IN int iModiyType, 
							 IN void *pModiyValue);
static int Web_GetSetUserInfo(IN char *szGetUserInfo, 
							  OUT char *szUserName, 
							  OUT char *szUserPassword, 
							  OUT char *szUserLevel);

static time_t Web_AnalyzeHisLogLine(IN char *szReadLine, 
									OUT HIS_MATCH_LOG *szAnalyzeString)
;


static int Web_ReadStatusConfig(OUT WEB_STATUS_CONFIG **stReturnStatusConfig );
static int Web_ParseWebStatusInfo(IN char *szBuf,
							  OUT WEB_STATUS_CONFIG_INFO *pStructData);

static char *Web_MakeNMSPrivateConfigureBuffer(void);
static int Web_ModifyNMSPrivateConfigure(IN char *szNmsBuffer);
static char *Web_MakeESRPrivateConfigreBuffer(void);
static int Web_GetESRModifyInfo(IN char *szBuffer, 
								OUT COMMON_CONFIG *stCommonConfig);

static int Web_ModifyESRPrivateConfigure(IN char *szBuffer);

//static int Web_GetSingalAlarmInfo(IN int iEquipID, 
//								  IN int iLanguage, 
//								  OUT char **szSignalInfo);

static int Web_GetStdEquipSingalInfo(IN int iEquipTypeID, 
									 IN int iSignalType, 
									 IN int iSignalID, 
									 IN int iLanguage, 
									 OUT char **szSignalInfo);

static int Web_MakeEquipListBufferA(OUT char **ppBuf, IN int iLanguage);
static int Web_MakeACUInfoBufferA(OUT char **ppBuf, IN int iLanguage);

/****************start : multilanguage****************/
static int StartTransferFile(int *iQuitCommand);
static double Web_GetVersionOfWebPages(void);
static int Web_WriteVersionOfWebPages(IN double fVersion);
static int Web_WriteVersionOfWebPages_r(IN double fVersion, IN char *szLanguage);
static int Web_WriteVersionOfWebPages_r2(IN double fVersion, IN char *szLanguage);//Add by wj for three languages,2006.4.27

static double Web_GetVersionOfWebPages_r(WEB_VERSION_INFO  *stWebVersionInfo_R);


static char *MakeFilePath(IN char *szFileName, IN int iLanguage);
static int WEB_ReadConfig(void);
static int WEB_ReadConfigLoc2(void);//Add by wj for three languages,2006.4.27
static char *CreateLangResourceFileName(const char *szFileName, const char *szLangCode);
static char *MakeVarField(char *szBuffer);
//static char *MakePrivateVarField(IN const char *szBuffer);
static int LoadWebPrivateConfigProc(IN int iPages, void *pCfg);
static int LoadWebPrivateConfigProc2(IN int iPages, void *pCfg);//Add by wj for three languages

static int ParseWebResourceInfo(IN char *szBuf, 
								OUT WEB_RESOURCE_INFO *pResourceInfo);
static int ParsedWebLangFileTableProc(IN char *szBuf, 
									  OUT WEB_GENERAL_RESOURCE_INFO *pStructData);
static int ParsedWebLangFileTableProc2(IN char *szBuf, 
                                       OUT WEB_GENERAL_RESOURCE_INFO *pStructData);//Add by wj for three languages,2006.4.27
static void CloseACU(void);
static void Web_ReplaceFile(IN char *szBuffer);
static int Web_TransferFormatToInt(IN char *szFormat);

static int ServiceNotification(HANDLE hService, 
							   int iMsgType, 
							   int iTrapDataLength, 
							   void *lpTrapData,	
							   void *lpParam, 
							   BOOL bUrgent);

static void Web_ClearSessionFile(void);

static BOOL Web_ClearHistoryData(IN int iClearType, IN char *szUserName);
static BOOL Web_GetProtectedStatus(void) ;

static int Web_MakeQueryDiselTestLog(IN time_t fromTime, 
									 IN time_t toTime, 
									 IN int iLanguage, 
									 IN OUT char **ppBuf);

static int Web_QueryDiselTestLog(IN time_t fromTime,
							 IN time_t toTime,
							 OUT void **ppBuf);

static char szClearName[][64]= {HIST_ALARM_LOG,
									HIS_DATA_LOG,
									//STAT_DATA_LOG,
									CTRL_CMD_LOG,
									BATT_TEST_LOG_FILE,
									//PERSISTENT_SIG_LOG,
									//ACU_RUNNING_LOG,
									DSL_TEST_LOG
                                    };

//for refresh rectifier list

static unsigned int Web_GetRectifierNumber(void);
static unsigned int Web_GetConverterNumber(void);
static int Web_MakeRectifierRefresh(IN BOOL bUrgent);
static unsigned int Web_GetACDNumber(void);
static unsigned int Web_GetDCDNumber(void);
//static unsigned int Web_GetBattNumber(void);
static unsigned int Web_GetBATTNumber(void);
static unsigned int Web_GetEIBBattNumber(int iEquipID);
static unsigned int Web_GetSMDUCFGStatus(void);
static unsigned int Web_WorkStatus(void);
static BOOL Web_GetEquipExistStatus(int iEquipID);

//for GC Config
static HANDLE s_hMutexLoadGCCFGFile = NULL;
#define WEB_GC_CFG_FILE_PATH "/app/config/private/gen_ctl/gen_ctl.cfg"
#define GC_PS_MODE     "[POWER_SPLIT_MODE]"
#define GC_PS_INFO      "[POWER_SPLIT_INPUT]"

//for GC Config
HANDLE GetMutexLoadGCConfig(void);
static int Web_MakeGCWebPage(OUT char **ppszReturn, IN int iLanguage);
static int Web_LoadGCConfigFile(char **ppszReturn);
static int Web_LoadGCPSMode(char **ppszReturn);
static int ParseGCTableProc(IN char *szBuf, OUT WEB_GC_CFG_INFO_LINE *pStructData);
static int LoadGCConfigFile(IN void *pCfg, OUT void *pLoadToBuf);
static int Web_GetGCPSMode(OUT char **ppszGCPSMode);
static int Web_ModifyGCPSMode(IN char *pszMode);
static int Web_ModifyGCPSInfo(IN char *pszGCPSInfo);

//////////////////////////////////////////////////////////////////////////
//Added by wj for PLC Config 2006.5.15

//for PLC Config
#define WEB_PLC_CFG_FILE_PATH "/app/config/private/plc/plc.cfg"
#define MAX_TIME_WAITING_LOAD_PLC_CONFIG 1000
#define SPLITTER_LINE "\r\n"
#define MAX_ITEM_BUF 150
#define SCUP_EQUIPCFG_FILE_PATH  "/app/config/standard/"
#define PLC_CMD "[PLC_CMD]"
#define ALARM_INFO "[ALARM_INFO]"
#define MAX_BUFFER  300000

//for AlarmSupExp Config
#define MAX_FILE_PATH_BUF_LEN  150


//for PLC Config
HANDLE GetMutexLoadPLCConfig(void);
static int Web_MakePlcWebPage(OUT char **ppszReturn, IN int iLanguage);
static int Web_LoadPlcConfigFile(OUT char **ppszReturn);
static int LoadPlcConfigFile(IN void *pCfg, OUT void *pLoadToBuf);
static int ParsePLCTableProc(IN char *szBuf, OUT WEB_PLC_CFG_INFO_LINE *pStructData);
static int Web_WritePLCCFGInfo(IN char *pszFileLineBuf);
static int Web_DeletePLCCFGInfo(IN char *pszFileLineBuf);

//for AlarmSupExp Config
HANDLE GetMutexLoadAlarmConfig(void);
static int Web_MakeAlarmSupExpWebPageBuf(IN char *pszBuf,OUT char **pszReturn,IN int iLanguage);
static int Web_GetStdEquipFileName(IN int istdEquipTypeId, OUT char **pszStdEquipFileName);
static int Web_GetStdEquipBuf(OUT char **pszStdEquipBuf, IN int iLanguage);
static int Web_GetEquipInfoByTypeId(IN int iEquipTypeId, OUT EQUIP_INFO **ppszEquipInfo);
static int Web_GetEquipInfoById(IN int iEquipId, OUT EQUIP_INFO **ppszEquipInfo);
static int Web_GetDispAlarmItemsBuf(IN int iEquipTypeId, IN char *pEquipTypeCFGFileName, IN int iLanguage, OUT char **ppszDispAlarmItemsBuf);
static int Web_LoadStdEquipAlarmSigConfigFile(IN char *pFilePath, IN WEB_ALARM_CFG_INFO *pstAlarmConfigInfo);
static int LoadAlarmConfigFile(IN void *pCfg, OUT void *pLoadToBuf);
static int ParseAlarmTableProc(IN char *szBuf, OUT WEB_ALARM_CFG_INFO_LINE *pStructData);
static int Web_ParseSuppressingExp(IN char *pszAlarmSupExp, OUT char **ppszResult, IN EQUIP_INFO *pszEquipInfo, IN int iLanguage);
static int Web_ParseSignal(IN int iSequnceNo, IN int iAlarmSigId, OUT char **ppszResult, IN EQUIP_INFO *pszEquipInfo, IN int iLanguage);
static int Web_ParseSignalName(IN int iLanguage, IN EQUIP_INFO *pszEquipInfo, IN int iAlarmSigId, OUT char **ppszAlarmName);
static char *Web_EditCFGLineItem(IN char *pszLineBuf, IN int iColNo, IN char *pszNewItemBuf);
static int Web_SetAlarmSuppressingExp(IN char *pszSendBuf,IN char *szUserName);
static void Web_SkipTabSpace(char **pProf);

//for AlarmRelay Config
static int Web_MakeAlarmRegWebPageBuf(IN char *pszBuf,OUT char **ppszReturn,IN int iLanguage);
static int Web_GetDispAlarmRegItemsBuf(IN int iEquipTypeId, 
                                       IN char *pEquipTypeCFGFileName, 
                                       IN int iLanguage, 
                                       OUT char **ppszDispAlarmItemsBuf);
int Web_SetAlarmRelay(IN char *pszSendBuf,IN char *szUserName);


//for YDN Setting
static char *Web_MakeYDNPrivateConfigreBuffer(void);
static int Web_ModifyYDNPrivateConfigure(IN char *szBuffer,IN char *szUserName);
static int Web_GetYDNModifyInfo(IN char *szBuffer, OUT YDN_COMMON_CONFIG *stCommonConfig);

//User Def Page
static int Web_MakeSigValOfUserDefPage(IN int iLanguage, 
									   IN int iPageID,
									   OUT char **ppBuf);
static int Web_MakeSigInfoOfUserDefPage(IN int iLanguage, 
									   IN int iPageID,
									   OUT char **ppBuf);
//for setting param
static int Web_MakeGetSetParamWebPage(void);
static int Web_GetSetParamNum(void);


//for refresh rectifier list

static unsigned int Web_GetRectifierNumber(void);
static int Web_MakeRectifierRefresh(IN BOOL bUrgent);
static void fnProcessCmdThread(void);
//////////////////////////////////////////////////////////////////////////
//netscape
static int  Web_Netscape_MultiLang();
static int  Web_Netscape_PageLang();
static int Web_GetVersionOfWebPages_N(WEB_VERSION_INFO  *stWebVersionInfo_R);
static int Web_Netscape_ReadConfig();
static int Web_Netscape_LoadWebPrivateConfigProc(IN int iPages, IN void *pCfg);
static char *MakeFilePathN(IN char *szFileName, IN int iLanguage);
static int Web_Netscape_CreatePage();
static int Web_Netscape_ChangeFlage();
static int Web_Free_ResourceN();
static int Web_WriteVersionOfWebPages_N(IN double fVersion, IN char *szLanguage,IN int iStartFlage);
static int Web_SetCurrentLangValue(IN char *ptrBuf);
static int Web_GetCurrentLangValue(void);
static int Web_GetNowLangValue(void);
static int Web_GetESRModifyInfo_net(IN char *szBuffer, OUT COMMON_CONFIG *stCommonConfig, IN int iCommandType);
static int Web_SetACUTime_net(IN void *pModiyValue);
static int Web_Recover(IN char *ptr);
static BOOL Web_ACUIsAuto();
static int Web_PageRefresh(IN int *iQuitCommand);
static int Web_CopyNeedFile();
static int Web_MakeFirstPage(char **ppBuf);
static time_t Web_Net_transferCharToTime(IN char *pTime);
static int Web_SendQueryData_Netscape(IN char *buf,  IN char *filename);
static int Web_StateMgmt_Netscape(IN char *buf, 
				  IN char *filename);
static int Web_PageRealize(EQUIP_INFO		*pEquipInfo);
static int Web_GetNetscapeRealTimeAlarmData(OUT ALARM_SIG_VALUE **ppSigValue);
static int GetSlaveMode(void);
static int Web_GetCSUNum(int iType);

static int Web_ModifyNMSV3PrivateConfigure(IN char *szNmsBuffer);
static char *Web_MakeNMSV3PrivateConfigureBuffer(void);
//////////////////////////////////////////////////////////////////////////

static int Web_RefreshEquipList(void);
static int Web_ResetNetDriver(void);
static int Web_GetEquipName(IN OUT char *szEquipName);
static char *Web_GetEquipNameFromInterface_New(IN int iEquipID,IN int iLanguage,IN int iPosition);
//static int Web_AJAX_TEST(void);
static unsigned int Web_GetRectifierIncludSlave(void);

static HANDLE s_hMutexLoadPLCCFGFile = NULL;
static HANDLE s_hMutexLoadAlarmCFGFile = NULL;
//end////////////////////////////////////////////////////////////////////////
static int StartApache(void);
int iRollFlage = 0;
int StartFlage = 0;

/****************end : multilanguage****************/
EQUIP_SORT			iEquipTypeNum[MAX_EQUIP_TYPE_NUM];
COMM_EQUIP_LIST		pEquipList[MAX_EQUIP_NUM];
static int			iMaxEquipTypeInFact = 0;
WEB_STATUS_CONFIG	*stStatusConfig;  //read status config info
time_t				g_AlarmCurrentTime;
int					iRectGroupID = 0;		//Rect group ID
unsigned int		iRectifierNumber = 0;
unsigned int		iConverterNumber = 0;
unsigned int		iACDNumber = 0;
unsigned int		iDCDNumber = 0;
unsigned int		iBATTNumber = 0;
unsigned int		iEIB1BattNum = 0;
unsigned int		iEIB2BattNum = 0;
unsigned int		iEIB3BattNum = 0;
unsigned int		iEIB4BattNum = 0;
unsigned int		iBatLCNum = 0;
unsigned int		iDCLCNum = 0;


/*==========================================================================*
 * FUNCTION :  Web_GetSignalNum
 * PURPOSE  :  get signal num
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN int iEquipID
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetSignalNum(IN int iEquipID,char **ppBuf)
{
	int		iLen = 0 ;
	int		iSigNumRet = 0;//,iSampleSigNumRet,iControlSigNumRet,iSettingSigNumRet,iAlarmSigNumRet,iRAlarmSigNumRet;
    int		iError = 0;
	int		iInterfaceType;
	char	pBuf[60];

	iInterfaceType = VAR_SAM_SIG_NUM_OF_EQUIP;
	iError += DxiGetData(iInterfaceType,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		&iSampleSigNumRet,			
		iTimeOut);

//	//TRACE("iSampleSigNumRet:%d\n",iSampleSigNumRet);

	iInterfaceType = VAR_CON_SIG_NUM_OF_EQUIP;
	iError += DxiGetData(iInterfaceType,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		&iControlSigNumRet,			
		iTimeOut);

//	//TRACE("iControlSigNumRet:%d\n",iControlSigNumRet);

	iInterfaceType = VAR_SET_SIG_NUM_OF_EQUIP;
	iError += DxiGetData(iInterfaceType,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		&iSettingSigNumRet,			
		iTimeOut);

//	//TRACE("iSettingSigNumRet:%d\n",iSettingSigNumRet);

	iInterfaceType = VAR_ACTIVE_ALARM_NUM;
	int iVarID = ALARM_LEVEL_NONE; //Alarm level
	iError += DxiGetData(iInterfaceType,
		iVarID,			
		iVarSubID,		
		&iBufLen,			
		&iRAlarmSigNumRet,			
		iTimeOut);
	
//	//TRACE("iRAlarmSigNumRet:%d\n",iRAlarmSigNumRet);

	iInterfaceType = VAR_ALARM_SIG_NUM_OF_EQUIP;
	iError += DxiGetData (iInterfaceType,
							iEquipID,			
							iVarSubID,		
							&iBufLen,			
							&iAlarmSigNumRet,			
							iTimeOut);

//	//TRACE("iAlarmSigNumRet:%d\n",iAlarmSigNumRet);
	
	if(iError == ERR_DXI_OK)
	{
		////TRACE("Web_GetSignalNum: Successfully to get signual number\n");
		iSigNumRet = iSampleSigNumRet + iControlSigNumRet +	iSettingSigNumRet + iRAlarmSigNumRet;
		////TRACE("iSigNumRet :%d\n",iSigNumRet);

		iLen += sprintf(pBuf,"%4d%4d%4d%4d%4d",
						iSampleSigNumRet,iControlSigNumRet,
						iSettingSigNumRet,iAlarmSigNumRet,iRAlarmSigNumRet);
						
		pBuf[iLen] = '\0';
		*ppBuf = pBuf;

		return iSigNumRet;
	}
	else
	{
		return FALSE;
	}

}


/*==========================================================================*
 * FUNCTION :  Web_GetAuthorityByUser
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *pszUserInfo:
				IN char *pszPasswordInfo:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:15
 *==========================================================================*/
static int Web_GetAuthorityByUser(IN char *pszUserInfo,IN char *pszPasswordInfo)
{
	USER_INFO_STRU		*pUserInfo;
	int					iError;
	int					iAuthorLevel = 0;



	pUserInfo = NEW(USER_INFO_STRU,1);
	if(pUserInfo == NULL)
	{
		return FALSE;
	}

	//strncpyz(pUserInfo->szUserName , pszUserInfo,strlen(pUserInfo->szUserName));
	iError = FindUserInfo(pszUserInfo,pUserInfo);
	
	//TRACE("-----------[%s][%s]----[%s][%s]\n", pszUserInfo, pszPasswordInfo,pUserInfo->szUserName, pUserInfo->szPassword);
	if(iError == ERR_SEC_USER_NOT_EXISTED)
	{

		DELETE(pUserInfo);
		pUserInfo = NULL;
		return NO_THIS_USER;
	}
	else
	{
		if(strcmp(pszPasswordInfo,pUserInfo->szPassword) != 0)
		{
			DELETE(pUserInfo);
			pUserInfo = NULL;
			return NO_MATCH_PASSWORD;
		}
		else
		{
			iAuthorLevel = pUserInfo->byLevel;
			DELETE(pUserInfo);
			pUserInfo = NULL;
			return iAuthorLevel;
		}
	}
}

/*==========================================================================*
 * FUNCTION :  Web_GetACUInfo
 * PURPOSE  :  get ACU Information
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT SITE_INFO	**ppSiteInfo: 
				OUT DWORD *pDwGetData:
				OUT char szGetStr[128]: 
				OUT char **ppLangCode:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
 *==========================================================================*/
static int Web_GetACUInfo(OUT SITE_INFO	**ppSiteInfo, OUT DWORD *pDwGetData, 
						  OUT char szGetStr[128], OUT char **ppLangCode)
{
	SITE_INFO			*pSiteInfo;
	//LANG_TEXT			*pLangInfo;
	DWORD	   			dwGetData;
	char	   			szInGetStr[128];
	char           		*pLocalLangCode;

	int		iError = 0;
	int		iInterfaceType = VAR_ACU_PUBLIC_CONFIG;
	int		iVarID = SITE_INFO_POINTER;

	iError += DxiGetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						&pSiteInfo,			
						iTimeOut);

	iVarID = SITE_SW_VERSION;
	iError += DxiGetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						&dwGetData,			
						iTimeOut);


	iVarID = ACU_MANUFACTURER;
	iError += DxiGetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						szInGetStr,			
						iTimeOut);

	iVarID = LOCAL_LANGUAGE_CODE;

	iError += DxiGetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						&pLocalLangCode,			
						iTimeOut);
	
	if (iError == ERR_DXI_OK)
	{
		sprintf(szGetStr,"%s", szInGetStr);
		*ppLangCode = pLocalLangCode;
		pDwGetData = &dwGetData;
		*ppSiteInfo = pSiteInfo;
		
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}


/*==========================================================================*
 * FUNCTION :  Web_MakeACUInfoBufferA
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char **ppBuf:
				IN int iLanguage:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
 *==========================================================================*/
static int Web_MakeACUInfoBufferA(OUT char **ppBuf, IN int iLanguage)
{
	SITE_INFO		*pSiteInfo = NULL;
	char			*pOutBuf = NULL;
	DWORD			dwGetData;
	char			szGetStr[128];
	char			*pLangCode = NULL;
	int				i = 1, iLen = 0;
	char			*pSearchValue = NULL;

	
	if(Web_GetACUInfo(&pSiteInfo, &dwGetData, szGetStr, &pLangCode) == FALSE)
	{
		return FALSE;
	}
#define MAX_ACU_INFO_BUFFER_LEN_A 10000	
	
	pOutBuf = NEW(char, MAX_ACU_INFO_BUFFER_LEN_A);
	ASSERT(pOutBuf);
	if(pOutBuf == NULL)
	{
		return FALSE;
	}
	if(iLanguage > 0)
	{
		
        if(iLanguage==1)
        {
            pOutBuf[iLen] = ACU_LOC_INFO_START;
		    /*Loc ACU Info*/
		    iLen += sprintf(pOutBuf + iLen + 1, "%d,\"��վ��\",\"%s\",\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[0]);
		    //iLen += sprintf(pOutBuf + iLen, "%d,\"Version\",%ld,\n",    SITE_SW_VERSION, dwGetData);
		    //iLen += sprintf(pOutBuf + iLen, "%d,\"Manufacture\",\"%s\",\n", ACU_MANUFACTURER, szGetStr);
		    //iLen += sprintf(pOutBuf + iLen, "%d,\"SiteID\", %d,\n",	  SITE_ID, pSiteInfo->iSiteID);
		    //iLen += sprintf(pOutBuf + iLen, "%d,\"Language Code\",\"%s\",\n", LOCAL_LANGUAGE_CODE, pLangCode);
		    iLen += sprintf(pOutBuf + iLen, "%d,\"��վλ��\",\"%s\",\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[1]);
		    iLen += sprintf(pOutBuf + iLen, "%d,\"��վ����\",\"%s\",\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[1]);
		    //iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);
    		
		    pSearchValue = strrchr(pOutBuf,44);  //get the last ','
		    if(pSearchValue != NULL)
		    {
			    *pSearchValue = 32;   //replace ',' with ' '
		    }
        }
        else
        {
            pOutBuf[iLen] = ACU_LOC2_INFO_START;
            /*Loc ACU Info*/
            iLen += sprintf(pOutBuf + iLen + 1, "%d,\"SiteName\",\"%s\",\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[2]);
            //iLen += sprintf(pOutBuf + iLen, "%d,\"Version\",%ld,\n",    SITE_SW_VERSION, dwGetData);
            //iLen += sprintf(pOutBuf + iLen, "%d,\"Manufacture\",\"%s\",\n", ACU_MANUFACTURER, szGetStr);
            //iLen += sprintf(pOutBuf + iLen, "%d,\"SiteID\", %d,\n",	  SITE_ID, pSiteInfo->iSiteID);
            //iLen += sprintf(pOutBuf + iLen, "%d,\"Language Code\",\"%s\",\n", LOCAL_LANGUAGE_CODE, pLangCode);
            iLen += sprintf(pOutBuf + iLen, "%d,\"Site Location\",\"%s\",\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[2]);
            iLen += sprintf(pOutBuf + iLen, "%d,\"Site Description\",\"%s\",\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[2]);
            //iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);

            pSearchValue = strrchr(pOutBuf,44);  //get the last ','
            if(pSearchValue != NULL)
            {
                *pSearchValue = 32;   //replace ',' with ' '
            }
        }
		
		
		
	}
	else
	{
	
		/*Eng ACU Infor*/
		i = 1;
		pOutBuf[iLen] = ACU_ENG_INFO_START ;
		iLen += sprintf(pOutBuf + iLen + 1, "%d,\"SiteName\",\"%s\",\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[0]);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Version\",%ld,\n",    SITE_SW_VERSION, dwGetData);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Manufacture\",\"%s\",\n", ACU_MANUFACTURER, szGetStr);
		//iLen += sprintf(pOutBuf + iLen, "%d, \"SiteID\",%d ,\n", SITE_ID, pSiteInfo->iSiteID);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"Language Code\",\"%s\",\n", LOCAL_LANGUAGE_CODE, pLangCode);
		iLen += sprintf(pOutBuf + iLen, "%d,\"Site Location\",\"%s\",\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[0]);
		iLen += sprintf(pOutBuf + iLen, "%d,\"Site Description\",\"%s\",\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[0]);
		//iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);
	
		pSearchValue = strrchr(pOutBuf,44);  //get the last ','
		
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}
	}

	*ppBuf = pOutBuf;
	return iLen;
}



/*==========================================================================*
 * FUNCTION :  Web_MakeACUInfoBuffer
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char **ppBuf:
				IN int iLanguage:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
 *==========================================================================*/
static int Web_MakeACUInfoBuffer(OUT char **ppBuf, IN int iLanguage)
{
	SITE_INFO		*pSiteInfo = NULL;
	char			*pOutBuf = NULL;
	DWORD			dwGetData;
	char			szGetStr[128];
	char			*pLangCode = NULL;
	int				i = 1, iLen = 0;
	char			*pSearchValue = NULL;

	
	if(Web_GetACUInfo(&pSiteInfo, &dwGetData, szGetStr, &pLangCode) == FALSE)
	{
		return FALSE;
	}
#define MAX_ACU_INFO_BUFFER_LEN				15000	

	pOutBuf = NEW(char, MAX_ACU_INFO_BUFFER_LEN);
	if(pOutBuf == NULL)
	{
		return FALSE;
	}
	pOutBuf[iLen] = ACU_LOC_INFO_START;
	/*Loc ACU Info*/
	iLen += sprintf(pOutBuf + iLen + 1, "%d,\"SiteName\",\"%s\",\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[0]);
	iLen += sprintf(pOutBuf + iLen, "%d,\"Site Location\",\"%s\",\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[1]);
	iLen += sprintf(pOutBuf + iLen, "%d,\"Site Description\",\"%s\",\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[1]);
	//iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);
	
	pSearchValue = strrchr(pOutBuf,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}
	if(iLanguage != -1)
	{

		*ppBuf = pOutBuf;
		return iLen;
	}
	/*Eng ACU Infor*/
	i = 1;
	pOutBuf[iLen] = ACU_ENG_INFO_START ;
	iLen += sprintf(pOutBuf + iLen + 1, "%d,\"SiteName\",\"%s\",\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[0]);
	iLen += sprintf(pOutBuf + iLen, "%d,\"Site Location\",\"%s\",\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[0]);
	iLen += sprintf(pOutBuf + iLen, "%d,\"Site Description\",\"%s\",\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[0]);
	//iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);

	pSearchValue = strrchr(pOutBuf,44);  //get the last ','
	
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

//////////////////////////////////////////////////////////////////////////
    //added by wj for three languages 2006.4.29
    /*Loc2 ACU Infor*/
#ifdef  _SUPPORT_THREE_LANGUAGE

    pOutBuf[iLen] = ACU_LOC2_INFO_START ;
    iLen += sprintf(pOutBuf + iLen + 1, "%d,\"SiteName\",\"%s\",\n", SITE_NAME, pSiteInfo->langSiteName.pFullName[2]);
    iLen += sprintf(pOutBuf + iLen, "%d,\"Site Location\",\"%s\",\n", SITE_LOCATION, pSiteInfo->langSiteLocation.pFullName[2]);
    iLen += sprintf(pOutBuf + iLen, "%d,\"Site Description\",\"%s\",\n", SITE_DESCRIPTION, pSiteInfo->langDescription.pFullName[2]);
    //iLen += sprintf(pOutBuf + iLen, "%d,\"AlarmOutgoingBlocked\",%d,\n", i++, pSiteInfo->bAlarmOutgoingBlocked);

    pSearchValue = strrchr(pOutBuf,44);  //get the last ','

    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }
#else
    pOutBuf[iLen] = ACU_LOC2_INFO_START ;
    iLen = iLen + 1; 
    iLen += sprintf(pOutBuf + iLen, "  ");
#endif
//end////////////////////////////////////////////////////////////////////////

	*ppBuf = pOutBuf;
	return iLen;
}



/*==========================================================================*
 * FUNCTION :  Web_ModifyACUInfo
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *pNewConfigInfo:
				IN int iLanguage:
				IN int iModifyType:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
 *==========================================================================*/
static int Web_ModifyACUInfo(IN char *pNewConfigInfo, IN int iLanguage, 
							 IN int iModifyType)
{
	int		iVarID, iVarSubID;
	int		iError = 0;
	int		iInterfaceType = VAR_ACU_PUBLIC_CONFIG;
	BOOL		bNeedRefreshWebPage = FALSE;

	Cfg_RemoveWhiteSpace(pNewConfigInfo);
	switch (iModifyType)
	{
		case SITE_NAME:
		{
			iVarID = SITE_NAME;
			if(iLanguage >0)
			{
				if(iLanguage == 1)
				{
				    iVarSubID = MODIFY_SITE_ENGLISH_FULL_NAME;
				    iBufLen = strlen(pNewConfigInfo);

				    iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
				}
				//////////////////////////////////////////////////////////////////////////
				//Added by wj for three languages 2006.5.9
				else
				{
				    iVarSubID = MODIFY_SITE_ENGLISH_FULL_NAME;
				    iBufLen = strlen(pNewConfigInfo);

				    iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
				}
				//end////////////////////////////////////////////////////////////////////////
		                
				

			}
			else
			{
				iVarSubID = MODIFY_SITE_ENGLISH_FULL_NAME;
				iBufLen = strlen(pNewConfigInfo);

				iError += DxiSetData(iInterfaceType,
								iVarID,			
								iVarSubID,		
								iBufLen,			
								pNewConfigInfo,			
								iTimeOut);
			}

			bNeedRefreshWebPage = TRUE;
			break;
		}
		case SITE_SW_VERSION:
		{		
			iVarID = SITE_SW_VERSION;
			int dwVersion = atoi(pNewConfigInfo);
			iBufLen = sizeof(dwVersion);

			iError += DxiSetData(iInterfaceType,
								iVarID,			
								iVarSubID,		
								iBufLen,			
								&dwVersion,			
								iTimeOut);
			break;
		}
		case ACU_MANUFACTURER:
		{		
			iVarID = ACU_MANUFACTURER;
			iBufLen = strlen(pNewConfigInfo);

			iError += DxiSetData(iInterfaceType,
								iVarID,			
								iVarSubID,		
								iBufLen,			
								pNewConfigInfo,			
								iTimeOut);
			break;
		}
		case SITE_ID:
		{		
			iVarID = SITE_ID;
			int nSiteID = atoi(pNewConfigInfo);
			iBufLen = sizeof(nSiteID);

			iError += DxiSetData(iInterfaceType,
								iVarID,			
								iVarSubID,		
								iBufLen,			
								&nSiteID,			
								iTimeOut);
			break;
		}
		case SITE_LOCATION:
		{		
			iVarID = SITE_LOCATION;
			
			if(iLanguage > 0)
			{
				if(iLanguage == 1)
				{
				    iVarSubID = MODIFY_LOCATION_LOCAL_FULL;

				    iBufLen = strlen(pNewConfigInfo);

				    iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
				}
				//////////////////////////////////////////////////////////////////////////
				//Added by wj for three languages 2006.5.9
		                
				else
				{
				    iVarSubID = MODIFY_LOCATION_LOCAL2_FULL;

				    iBufLen = strlen(pNewConfigInfo);

				    iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
				}

				//end////////////////////////////////////////////////////////////////////////
		                
				
			}
			else
			{
				iVarID = SITE_LOCATION;
				iVarSubID = MODIFY_LOCATION_ENGLISH_FULL;

				iBufLen = strlen(pNewConfigInfo);

				iError += DxiSetData(iInterfaceType,
									iVarID,			
									iVarSubID,		
									iBufLen,			
									pNewConfigInfo,			
									iTimeOut);
			}
			bNeedRefreshWebPage = TRUE;
			break;
		}
		case SITE_DESCRIPTION:
		{		
			iVarID = SITE_DESCRIPTION;
			if(iLanguage > 0)
			{
				if(iLanguage == 1)
				{
				    iVarSubID = MODIFY_DESCRIPTION_LOCAL_FULL;
				    iBufLen = strlen(pNewConfigInfo);

				    iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
				}

				//////////////////////////////////////////////////////////////////////////
				//Added by wj for three languages 2006.5.9

				else
				{
				    iVarSubID = MODIFY_DESCRIPTION_LOCAL2_FULL;
				    iBufLen = strlen(pNewConfigInfo);

				    iError += DxiSetData(iInterfaceType,
					iVarID,			
					iVarSubID,		
					iBufLen,			
					pNewConfigInfo,			
					iTimeOut);
				}

				//end////////////////////////////////////////////////////////////////////////
		                
				
			}
			else
			{
				iVarSubID = MODIFY_DESCRIPTION_ENGLISH_FULL;
				iBufLen = strlen(pNewConfigInfo);

				iError += DxiSetData(iInterfaceType,
									iVarID,			
									iVarSubID,		
									iBufLen,			
									pNewConfigInfo,			
									iTimeOut);
			}
			bNeedRefreshWebPage = TRUE;
			break;
		}
		default:
			break;
	}

	if (iError == ERR_DXI_OK)
	{
		if(bNeedRefreshWebPage == TRUE)
		{
			Web_WriteVersionOfWebPages_r(0.99, stWebCompareVersion.szLanguage);
			

		}
		return TRUE;
	}
	else
	{
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}
}

/*==========================================================================*
 * FUNCTION :  Web_GetEquipListFromInterface
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  OUT EQUIP_INFO *pEquipInfo
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:15
 *==========================================================================*/

static int Web_GetEquipListFromInterface(OUT EQUIP_INFO **ppEquipInfo)
{
	int				iError = 0;
	EQUIP_INFO		*pEquipInfo = NULL;
	int				iSBufLen = 0;
	int				iLen = 0;

	iError = DxiGetData(VAR_ACU_EQUIPS_LIST,
						0,			
						0,		
						&iSBufLen,			
						&pEquipInfo,			
						0);
	
	iLen = iSBufLen/sizeof(EQUIP_INFO);
	
	if (iError == ERR_DXI_OK   )
	{
		*ppEquipInfo = pEquipInfo;
		
		return iLen;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
 * FUNCTION :  Web_MakeEquipListBuffer
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:15
 *==========================================================================*/

static int Web_MakeEquipListBufferA(OUT char **ppBuf, IN int iLanguage)
{
	int				iLen = 0;//, iEquipNum = 0;
	char			*pMakeBuf = NULL;
	EQUIP_INFO		*pEquipInfo = NULL;
	int				i;
	int				iEquipNumber = 0;
	
	/*get equip information*/

	if((iEquipNumber = Web_GetEquipListFromInterface(&pEquipInfo)) <= 0)
	{
		return FALSE;
	}
	
	pMakeBuf = NEW(char, iEquipNumber *  100);

	for ( i = 0; i < iEquipNumber && pEquipInfo != NULL; i++, pEquipInfo++)
	{
		iLen += sprintf(pMakeBuf + iLen, "new ODevice(%d, \"%s\", \"%s\",%d)%c \n",
									pEquipInfo->iEquipID,
									pEquipInfo->pEquipName->pFullName[iLanguage],
									pEquipInfo->pEquipName->pAbbrName[iLanguage],
									pEquipInfo->iEquipTypeID,
									(i == iEquipNumber - 1)? 32 : 44 );
	}
	
	*ppBuf = pMakeBuf;
	return TRUE;
}
 
static BOOL	IsGroupNeedDisplay(int iEquipID)
{
#define EQUIP_EXIST_STATE	0
	int	iError;
	int	iSignalType = 0;
	int	iSignalID = 100; //ͨ��Existence�ź����ж��Ƿ���ʾ
	int	iBufLen;
	int	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	SIG_BASIC_VALUE* pSigValue;
	BOOL	bNeedFlag = FALSE;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		5000);
	if(iError == ERR_DXI_OK)
	{
		bNeedFlag = (pSigValue->varValue.enumValue == EQUIP_EXIST_STATE) ? TRUE:FALSE;
	}
	else
	{
		bNeedFlag = FALSE;
	}
	
	return bNeedFlag;
}

/*==========================================================================*
 * FUNCTION :  Web_MakeEquipListBuffer
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
 *==========================================================================*/
static int Web_MakeEquipListBuffer(OUT char **ppBuf)
{
	int				iLen = 0, iEquipNum = 0;
	char			*pMakeBuf = NULL;
	char			*pSearchValue = NULL;

	EQUIP_INFO		*pEquipInfo = NULL;

	//printf("\n__________INTO Web_MakeEquipListBuffer_____________\n ");
	
	//int				iRectVarID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8);
	//SIG_BASIC_VALUE	*pSigValue = NULL;
	//int				iRectNumber = 0;
	//int				iBufINLen = sizeof(SIG_BASIC_VALUE);
	

					

	/*get equip information*/

	if(Web_GetEquipListFromInterface(&pEquipInfo) <= 0)
	{
		return FALSE;
	}
	//printf("\n_____1_____INTO Web_MakeEquipListBuffer_____________\n ");
	/*get equip number*/
	int iError = DxiGetData(VAR_ACU_EQUIPS_NUM,
							0,			
							iVarSubID,		
							&iBufLen,			
							(void *)&iEquipNum,			
							iTimeOut);

	
	
	if(iError == ERR_DXI_OK)
	{
		int			i = 0, j = 0, k = 0, m = 0;
	 
		BOOL		bMatch = FALSE;
		//TRACE("iEquipNum : %d\n",iEquipNum);
	 
		for(i = 0; i < iEquipNum; i++)
		{
			for(m = 0; m < k; m++)
			{
				if(pEquipInfo[i].iEquipTypeID/100 == iEquipTypeNum[m].iEquipTypeID/100)
				{
					if(iEquipTypeNum[m].iUnitEquipID == 0)
					{
						iEquipTypeNum[m].iUnitEquipID = pEquipInfo[i].iEquipID;
					}
					bMatch = TRUE;
					 
				}
			}
			if(!bMatch)
			{
				if(pEquipInfo[i].iEquipTypeID == 200)
				{
					iRectGroupID = pEquipInfo[i].iEquipID;
				}
				iEquipTypeNum[k].iEquipID		= pEquipInfo[i].iEquipID;	
				iEquipTypeNum[k].iEquipTypeID	= pEquipInfo[i].iEquipTypeID;
				iEquipTypeNum[k].iUnitEquipID	= 0;

				strncpyz(iEquipTypeNum[k].szEngEquipName, 
							pEquipInfo[i].pEquipName->pFullName[0],
							sizeof(iEquipTypeNum[k].szEngEquipName));	

				strncpyz(iEquipTypeNum[k].szLocEquipName, 
							pEquipInfo[i].pEquipName->pFullName[1],
							sizeof(iEquipTypeNum[k].szLocEquipName));

                //////////////////////////////////////////////////////////////////////////
                //Added by wj for three languages 2006.5.9
                #ifdef _SUPPORT_THREE_LANGUAGE
                strncpyz(iEquipTypeNum[k].szLoc2EquipName, 
                            pEquipInfo[i].pEquipName->pFullName[2],
                            sizeof(iEquipTypeNum[k].szLoc2EquipName));
                #endif
                //end////////////////////////////////////////////////////////////////////////
                

				iEquipTypeNum[k].iNum		= 1;

				for (j = i + 1; j < iEquipNum; j++)
				{
					if(pEquipInfo[j].iEquipTypeID/100 == iEquipTypeNum[k].iEquipTypeID/100)
					{
						iEquipTypeNum[k].iNum++;
					}
					 
				}
				k++;
			}
			bMatch = FALSE;
		}
		iMaxEquipTypeInFact = k ;

		iRectifierNumber = Web_GetRectifierNumber();
		iACDNumber = 0;//Web_GetACDNumber();
        iDCDNumber = 0;//Web_GetDCDNumber();
        iBATTNumber = Web_GetBATTNumber();
		iEIB1BattNum = Web_GetEIBBattNumber(197);
		iEIB2BattNum = Web_GetEIBBattNumber(198);
		iEIB3BattNum = Web_GetEIBBattNumber(199);
		iEIB4BattNum = Web_GetEIBBattNumber(200);
		iBatLCNum = Web_GetCSUNum(0);
		iDCLCNum = Web_GetCSUNum(1);
		iSMDUCFGStatus = Web_GetSMDUCFGStatus();
		iConverterNumber = Web_GetConverterNumber();

	//	TRACE("\n1----------iRectifierNumber[%d]\n", iRectifierNumber);	
   //     TRACE("\n1----------iACDNumber[%d]\n", iACDNumber);
    //    TRACE("\n1----------iDCDNumber[%d]\n", iDCDNumber);
     //   TRACE("\n1----------iBATTNumber[%d]\n", iBATTNumber);
		/*Equip sort based on equip type*/ 
		k = 0;
		for(j = 0; j < iMaxEquipTypeInFact; j++)
		{
			for(i = 0; i < iEquipNum; i++)
			{
				if(pEquipInfo[i].iEquipTypeID/100 == iEquipTypeNum[j].iEquipTypeID/100)
				{
					pEquipList[k].bGroupType = pEquipInfo[i].pStdEquip->bGroupType;
					pEquipList[k].iEquipID = pEquipInfo[i].iEquipID;
					strncpyz(pEquipList[k].szLocEquipName,pEquipInfo[i].pEquipName->pFullName[1], sizeof(pEquipList[k].szLocEquipName));
					strncpyz(pEquipList[k].szEngEquipName,pEquipInfo[i].pEquipName->pFullName[0], sizeof(pEquipList[k].szEngEquipName));
                    //Added by wj for three languages 2006.5.9
                    #ifdef _SUPPORT_THREE_LANGUAGE
                    strncpyz(pEquipList[k].szLoc2EquipName,pEquipInfo[i].pEquipName->pFullName[2], sizeof(pEquipList[k].szLoc2EquipName));
                    #endif
					pEquipList[k].iEquipType = iEquipTypeNum[j].iEquipTypeID;
#ifdef AUTO_CONFIG
					pEquipList[k].bWorkState = pEquipInfo[i].bWorkStatus;
#endif
					k++;
				}
							
			}
		}

		
		pMakeBuf = NEW(char,iEquipNum * MAX_EQUIP_LIST_DATA_SIZE + 4500);
		if(pMakeBuf == NULL)
		{
			return FALSE;
		}
		

		/*Local language device list*/
		//STDEQUIP_TYPE_INFO *pStdTempEquipType = pStdEquipType;
		//for(i = 0; i < iStdEquipNum && pStdEquipType != NULL; i++, pStdEquipType++)
		//{
		//	iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%16s\",%4d,%1d) ,\n",pStdEquipType->pTypeName->pFullName[1],
		//												pStdEquipType->iTypeID,
		//												pStdEquipType->bGroupType);
		//}
		
		int			iShowRectNumber =  iRectifierNumber;
		int			iShowConvNumber =  iConverterNumber;
		int			iRectGroupNumber = 1;
		int			iConvGroupNumber = 1;
        //ACD
        int			iShowACDNumber =  iACDNumber;
        int			iACDGroupNumber = 0;
        //DCD
        int			iShowDCDNumber =  iDCDNumber;
        int			iDCDGroupNumber = 0;
        //BATT
        int			iShowBATTNumber =  iBATTNumber;
        //int			iBATTGroupNumber = 1;
		int			iShowEIB1BattNum = iEIB1BattNum;
		int			iShowEIB2BattNum = iEIB2BattNum;
		int			iShowEIB3BattNum = iEIB3BattNum;
		int			iShowEIB4BattNum = iEIB4BattNum; 

		iShowRectNumber = 100;
		iShowConvNumber = 60;
        iShowACDNumber = 0;
        iShowDCDNumber = 0;
		for(i = 0; i < iEquipNum; i++)
		{
			if(pEquipList[i].iEquipType/100 == 2 )//rect
			{
				if(pEquipList[i].bGroupType == TRUE && iRectGroupNumber > 0)
				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
														pEquipList[i].szLocEquipName,
														pEquipList[i].iEquipID,
														pEquipList[i].iEquipType);
					iRectGroupNumber--;
				}
				else if(iShowRectNumber > 0)
				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
														pEquipList[i].szLocEquipName,
														pEquipList[i].iEquipID,
														pEquipList[i].iEquipType);
					iShowRectNumber--;
				}
				/*else
				{
				}*/
			}
			else if(pEquipList[i].iEquipType/100 == 11 )//rect
			{
				if(pEquipList[i].bGroupType == TRUE && iConvGroupNumber > 0 )
				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
						pEquipList[i].szLocEquipName,
						pEquipList[i].iEquipID,
						pEquipList[i].iEquipType);
					iConvGroupNumber--;
				}
				else if(iShowConvNumber > 0)
				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
						pEquipList[i].szLocEquipName,
						pEquipList[i].iEquipID,
						pEquipList[i].iEquipType);
					iShowConvNumber--;
				}
				/*else
				{
				}*/
			}
            /*else if(pEquipList[i].iEquipType/100 == 4 )//ACD
            {
                if(pEquipList[i].bGroupType == TRUE && iACDNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d) ,\n",
                        pEquipList[i].szLocEquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iACDGroupNumber--;
                }
                else if(iShowACDNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d) ,\n",
                        pEquipList[i].szLocEquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iShowACDNumber--;
                }
                else
                {
                }
            }
            else if(pEquipList[i].iEquipType/100 == 5 )//DCD
            {
                if(pEquipList[i].bGroupType == TRUE && iDCDNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d) ,\n",
                        pEquipList[i].szLocEquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iDCDGroupNumber--;
                }
                else if(iShowDCDNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d) ,\n",
                        pEquipList[i].szLocEquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iShowDCDNumber--;
                }
                else
                {
                }
            }*/
			else
			{
//#ifdef AUTO_CONFIG
//				if(pEquipList[i].bWorkState == TRUE)
//#endif
				//{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",pEquipList[i].szLocEquipName,
						pEquipList[i].iEquipID,
						pEquipList[i].iEquipType);
				//}
				
			}
		}
			
		pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}
	
//printf("\n_____2_____INTO Web_MakeEquipListBuffer_____________\n ");
		/*English language device list*/
		pMakeBuf[iLen] = ENGLISH_START;
		iLen += 1;

		iShowRectNumber =  iRectifierNumber ;
		iShowConvNumber =  iConverterNumber;
        iRectGroupNumber = 1;
		iConvGroupNumber = 1;

		iShowRectNumber = 100;
		iShowConvNumber = 60;
        iShowACDNumber = 0;
        iShowDCDNumber = 0;
		for(i = 0; i < iEquipNum; i++)
		{
			if(pEquipList[i].iEquipType/100 == 2 )
			{
				if(pEquipList[i].bGroupType == TRUE && iRectGroupNumber > 0)
				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d), \n",pEquipList[i].szEngEquipName,
														pEquipList[i].iEquipID,
														pEquipList[i].iEquipType);
					iRectGroupNumber--;
				}
				else if(iShowRectNumber > 0)
				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d), \n",
														pEquipList[i].szEngEquipName,
														pEquipList[i].iEquipID,
														pEquipList[i].iEquipType);
					iShowRectNumber--;
				}
				else
				{
				}
			}
			else if(pEquipList[i].iEquipType/100 == 11 )//rect
			{
				if(pEquipList[i].bGroupType == TRUE && iConvGroupNumber > 0)
				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
						pEquipList[i].szEngEquipName,
						pEquipList[i].iEquipID,
						pEquipList[i].iEquipType);
					iConvGroupNumber--;
				}
				else if(iShowConvNumber > 0)
				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
						pEquipList[i].szEngEquipName,
						pEquipList[i].iEquipID,
						pEquipList[i].iEquipType);
					iShowConvNumber--;
				}
				/*else
				{
				}*/
			}
            /*else if(pEquipList[i].iEquipType/100 == 4 )
            {
                if(pEquipList[i].bGroupType == TRUE && iACDGroupNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d), \n",pEquipList[i].szEngEquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iACDGroupNumber--;
                }
                else if(iShowACDNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d), \n",
                        pEquipList[i].szEngEquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iShowACDNumber--;
                }
                else
                {
                }
            }
            else if(pEquipList[i].iEquipType/100 == 5 )
            {
                if(pEquipList[i].bGroupType == TRUE && iDCDGroupNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d), \n",pEquipList[i].szEngEquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iDCDGroupNumber--;
                }
                else if(iShowDCDNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d), \n",
                        pEquipList[i].szEngEquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iShowDCDNumber--;
                }
                else
                {
                }
            }*/
			else
			{
//#ifdef AUTO_CONFIG
//				if(pEquipList[i].bWorkState == TRUE)
//#endif
//				{
					iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d), \n",pEquipList[i].szEngEquipName,
															pEquipList[i].iEquipID,
															pEquipList[i].iEquipType);
				//}
			}
		}

//printf("\n_____3_____INTO Web_MakeEquipListBuffer_____________\n ");
        pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
        if(pSearchValue != NULL)
        {
            *pSearchValue = 32;   //replace ',' with ' '
        }
//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.4.29
        /*Loc2 language device list*///instead of eng for a while
#ifdef _SUPPORT_THREE_LANGUAGE
        pMakeBuf[iLen] = LOC2_START;
        iLen += 1;

        iShowRectNumber =  iRectifierNumber ;
        iShowRectNumber = 100;
        iRectGroupNumber = 1;


        iShowACDNumber = 0;
        iShowDCDNumber = 0;
        for(i = 0; i < iEquipNum; i++)
        {
            if(pEquipList[i].iEquipType/100 == 2 )
            {
                if(pEquipList[i].bGroupType == TRUE && iRectGroupNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d), \n",pEquipList[i].szLoc2EquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iRectGroupNumber--;
                }
                else if(iShowRectNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d), \n",
                        pEquipList[i].szLoc2EquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iShowRectNumber--;
                }
                else
                {
                }
            }
            /*else if(pEquipList[i].iEquipType/100 == 4 )
            {
                if(pEquipList[i].bGroupType == TRUE && iACDGroupNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d), \n",pEquipList[i].szLoc2EquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iACDGroupNumber--;
                }
                else if(iShowACDNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d), \n",
                        pEquipList[i].szLoc2EquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iShowACDNumber--;
                }
                else
                {
                }
            }
            else if(pEquipList[i].iEquipType/100 == 5 )
            {
                if(pEquipList[i].bGroupType == TRUE && iDCDGroupNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d), \n",pEquipList[i].szLoc2EquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iDCDGroupNumber--;
                }
                else if(iShowDCDNumber > 0)
                {
                    iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%s\",%4d,%d), \n",
                        pEquipList[i].szLoc2EquipName,
                        pEquipList[i].iEquipID,
                        pEquipList[i].iEquipType);
                    iShowDCDNumber--;
                }
                else
                {
                }
            }*/
            else
            {
                iLen += sprintf(pMakeBuf + iLen,"new ODevice(\"%s\",%4d,%d), \n",pEquipList[i].szLoc2EquipName,
                    pEquipList[i].iEquipID,
                    pEquipList[i].iEquipType);
            }
        }

        


		//pStdEquipType = pStdTempEquipType;
		//for(i = 0; i < iStdEquipNum && pStdEquipType != NULL; i++, pStdEquipType++)
		//{
		//	iLen += sprintf(pMakeBuf + iLen,"\t new ODevice(\"%16s\",%4d,%1d) ,\n",pStdEquipType->pTypeName->pFullName[0],
		//												pStdEquipType->iTypeID,
		//												pStdEquipType->bGroupType);
		//}

		pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}

#else
        pMakeBuf[iLen] = LOC2_START;
        iLen += 1;
        iLen += sprintf(pMakeBuf + iLen,"  ");
#endif

//end////////////////////////////////////////////////////////////////////////
		/*Make local language node*/
		pMakeBuf [iLen] = TREE_LOC_START;
		iLen += 1;
		m = 0;
//printf("\n_____4_____INTO Web_MakeEquipListBuffer_____________\n ");
		BOOL bEquipNOGroup = FALSE;
		iShowRectNumber =  iRectifierNumber;
        iShowACDNumber =  iACDNumber;
        iShowDCDNumber =  iDCDNumber;
        iShowBATTNumber = iBATTNumber;
		iShowEIB1BattNum = iEIB1BattNum;
		iShowEIB2BattNum = iEIB2BattNum;
		iShowEIB3BattNum = iEIB3BattNum;
		iShowEIB4BattNum = iEIB4BattNum;
		iShowConvNumber =  iConverterNumber;
    //    TRACE("\nACD=%d,DCD=%d,BATT=%d\n",iACDNumber,iDCDNumber,iBATTNumber);
		for(j = 0; j < iMaxEquipTypeInFact; j++)
		{
			bEquipNOGroup = FALSE;
			if(iEquipTypeNum[j].iEquipTypeID/100 == 26)
			{
				//printf("Skip\n");
				continue;
				
			}
			else if(iEquipTypeNum[j].iNum > 1)
			{

				 if(iEquipTypeNum[j].iEquipTypeID/100 == 2 && IsGroupNeedDisplay(2))		//Special for rect
				{
					bEquipNOGroup = TRUE;
					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
						if(pEquipList[m + i].bGroupType)
						{
							iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szLocEquipName);
							
							/*print equip group*/
						}				
					}

					for( i = 0; i < iEquipTypeNum[j].iNum && iShowRectNumber > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType)
						{		
							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);
							iShowRectNumber--;
						}
					}
				}
				else if(iEquipTypeNum[j].iEquipTypeID/100 == 11)		//Special for Converter
				{
					
					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
						if(pEquipList[m + i].bGroupType && pEquipList[m + i].bWorkState == TRUE)
						{
							iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szLocEquipName);
							bEquipNOGroup = TRUE;
							/*print equip group*/
						}				
					}

					for( i = 0; i < iEquipTypeNum[j].iNum && iShowConvNumber > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType)
						{		
							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);
							iShowConvNumber--;
						}
					}
				}
				else if(iEquipTypeNum[j].iEquipTypeID/100 == 3)		//Special for BATT
				{
					bEquipNOGroup = TRUE;
					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
						if(pEquipList[m + i].bGroupType)
						{
							iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szLocEquipName);


						}				
					}

					if(iShowBATTNumber == 3)
					{
						iShowBATTNumber = 1;
					}


					for( i = 0; i < iEquipTypeNum[j].iNum && iShowBATTNumber > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 116 || pEquipList[m + i].iEquipID == 117))
						{	

							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);
							iShowBATTNumber--;
						}
					}
					//EIB1 Batt
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowEIB1BattNum > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 118 || pEquipList[m + i].iEquipID == 119 || pEquipList[m + i].iEquipID == 206))
						{	
							
							if(Web_GetEquipExistStatus(pEquipList[m + i].iEquipID))
							{
								iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);
								iShowEIB1BattNum--;
							}
						}
					}
					//EIB2 Batt
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowEIB2BattNum > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 120 || pEquipList[m + i].iEquipID == 121 || pEquipList[m + i].iEquipID == 207))
						{	
							if(Web_GetEquipExistStatus(pEquipList[m + i].iEquipID))
							{
								iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);
								iShowEIB2BattNum--;
							}
						}
					}
					//EIB3 Batt
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowEIB3BattNum > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 122 || pEquipList[m + i].iEquipID == 123 || pEquipList[m + i].iEquipID == 208))
						{	
							if(Web_GetEquipExistStatus(pEquipList[m + i].iEquipID))
							{
								iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);
								iShowEIB3BattNum--;
							}
						}
					}
					//EIB4 Batt
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowEIB4BattNum > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 124 || pEquipList[m + i].iEquipID == 125 || pEquipList[m + i].iEquipID == 209))
						{	
							if(Web_GetEquipExistStatus(pEquipList[m + i].iEquipID))
							{
								iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);
								iShowEIB4BattNum--;
							}
						}
					}




					for( i = 0; i < iEquipTypeNum[j].iNum ; i++)
					{
						if(!pEquipList[m + i].bGroupType && pEquipList[m + i].iEquipID > 125 && pEquipList[m + i].iEquipID <= 205 && pEquipList[m + i].bWorkState == TRUE)
						{	

							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);

						}
					}

					for( i = 0; i < iEquipTypeNum[j].iNum ; i++)
					{
						if(!pEquipList[m + i].bGroupType && pEquipList[m + i].iEquipID >= 272 && pEquipList[m + i].bWorkState == TRUE  && pEquipList[m + i].iEquipID != 3000)
						{	

							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);

						}
					}

				}

				else
				{
				
					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
#ifdef AUTO_CONFIG
						if(pEquipList[m + i].bGroupType && (pEquipList[m + i].bWorkState == TRUE))
#else

						if(pEquipList[m + i].bGroupType)
#endif
						{
							iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szLocEquipName);
							bEquipNOGroup = TRUE;
							/*print equip group*/
						}				
					}

					for( i = 0; i < iEquipTypeNum[j].iNum ; i++)
					{
#ifdef AUTO_CONFIG
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].bWorkState == TRUE))
#else

						if(!pEquipList[m + i].bGroupType)
#endif
						{		
							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLocEquipName);
						}
					}
				}
				if(bEquipNOGroup == TRUE)
				{
					iLen += sprintf(pMakeBuf + iLen,"1,");
					bEquipNOGroup = FALSE; 
				}
				if(iEquipTypeNum[j].iEquipTypeID / 100 == 9)		//Special for ESNA
				{
					int iESNAConverter1 = 0;
					
					for(iESNAConverter1 = 0; iESNAConverter1 < MAX_EQUIP_NUM; iESNAConverter1++)
					{
						if(pEquipList[iESNAConverter1].iEquipType == 2600)
						{
							break;
						}
					}	
					
#ifdef AUTO_CONFIG
					if(pEquipList[iESNAConverter1].bWorkState == TRUE)
#endif

	 				/*if this group has one equip,print equip directlly*/
					{
						iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[iESNAConverter1].szLocEquipName);
					}
				}
			}
			else if(iEquipTypeNum[j].iNum == 1)
			{	
		 		/*if this group has one equip,print equip directlly*/
#ifdef AUTO_CONFIG
				if(pEquipList[m].bWorkState == 1)
#endif
				{
					iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m ].szLocEquipName);
			
				}
			}
			m += iEquipTypeNum[j].iNum;
		}
		
		iLen += sprintf(pMakeBuf + iLen,"%s", "1,");
		 
		/*Make english language node*/
	//printf("\n____6______INTO Web_MakeEquipListBuffer_____________\n ");	
		pMakeBuf[iLen] = TREE_ENG_START;
		iLen += 1;
		m = 0;
		iShowRectNumber =  iRectifierNumber;
        iShowACDNumber =  iACDNumber;
        iShowDCDNumber =  iDCDNumber;
        iShowBATTNumber = iBATTNumber;
		iShowEIB1BattNum = iEIB1BattNum;
		iShowEIB2BattNum = iEIB2BattNum;
		iShowEIB3BattNum = iEIB3BattNum;
		iShowEIB4BattNum = iEIB4BattNum;
		iShowConvNumber =  iConverterNumber;
   //     TRACE("\nACD=%d,DCD=%d,BATT=%d\n",iACDNumber,iDCDNumber,iBATTNumber);

		for(j = 0; j < iMaxEquipTypeInFact; j++)
		{
			if(iEquipTypeNum[j].iEquipTypeID / 100 == 26)//  
			{
				//printf("Skip\n");
				continue;
				
			}
			if(iEquipTypeNum[j].iNum > 1)
			{
				
				if(iEquipTypeNum[j].iEquipTypeID/100 == 2 && IsGroupNeedDisplay(2))
				{
					bEquipNOGroup = TRUE;
					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
						if(pEquipList[m + i].bGroupType)
						{
							iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szEngEquipName);
							
							/*print equip group*/
						}				
					}

					for( i = 0; i < iEquipTypeNum[j].iNum && iShowRectNumber > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType)
						{
							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);
							iShowRectNumber--;
						}
					}
				}

				else if(iEquipTypeNum[j].iEquipTypeID/100 == 11)		//Special for Converter
				{
					
					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
						if(pEquipList[m + i].bGroupType && pEquipList[m + i].bWorkState == TRUE)
						{
							iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szEngEquipName);
							bEquipNOGroup = TRUE;
							/*print equip group*/
						}				
					}

					for( i = 0; i < iEquipTypeNum[j].iNum && iShowConvNumber > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType)
						{		
							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);
							iShowConvNumber--;
						}
					}
				}
				else if(iEquipTypeNum[j].iEquipTypeID/100 == 3)
				{
					bEquipNOGroup = TRUE;
					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
						if(pEquipList[m + i].bGroupType)
						{
							iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szEngEquipName);


						}				
					}
					if(iShowBATTNumber == 3)
					{
						iShowBATTNumber = 1;
					}
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowBATTNumber > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 116 || pEquipList[m + i].iEquipID == 117))
						{	

							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);
							iShowBATTNumber--;
						}
					}
					//EIB1 Batt
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowEIB1BattNum > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 118 || pEquipList[m + i].iEquipID == 119 || pEquipList[m + i].iEquipID == 206))
						{	
							if(Web_GetEquipExistStatus(pEquipList[m + i].iEquipID))
							{
								iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);
								iShowEIB1BattNum--;
							}
						}
					}
					//EIB2 Batt
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowEIB2BattNum > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 120 || pEquipList[m + i].iEquipID == 121 || pEquipList[m + i].iEquipID == 207))
						{	
							if(Web_GetEquipExistStatus(pEquipList[m + i].iEquipID))
							{
								iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);
								iShowEIB2BattNum--;
							}
						}
					}
					//EIB3 Batt
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowEIB3BattNum > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 122 || pEquipList[m + i].iEquipID == 123 || pEquipList[m + i].iEquipID == 208))
						{	
							if(Web_GetEquipExistStatus(pEquipList[m + i].iEquipID))
							{
								iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);
								iShowEIB3BattNum--;
							}
						}
					}
					//EIB4 Batt
					for( i = 0; i < iEquipTypeNum[j].iNum && iShowEIB4BattNum > 0; i++)
					{
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].iEquipID == 124 || pEquipList[m + i].iEquipID == 125 || pEquipList[m + i].iEquipID == 209))
						{	
							if(Web_GetEquipExistStatus(pEquipList[m + i].iEquipID))
							{
								iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);
								iShowEIB4BattNum--;
							}
						}
					}


					for( i = 0; i < iEquipTypeNum[j].iNum ; i++)
					{
						if(!pEquipList[m + i].bGroupType && pEquipList[m + i].iEquipID > 125 && pEquipList[m + i].iEquipID <= 205 && pEquipList[m + i].bWorkState == TRUE && pEquipList[m + i].iEquipID != 186)
						{	

							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);

						}
					}

					for( i = 0; i < iEquipTypeNum[j].iNum ; i++)
					{
						if(!pEquipList[m + i].bGroupType && pEquipList[m + i].iEquipID >= 272 && pEquipList[m + i].bWorkState == TRUE && pEquipList[m + i].iEquipID != 3000)
						{	

							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);

						}
					}

				}
				else
				{
					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
#ifdef AUTO_CONFIG
						if(pEquipList[m + i].bGroupType && (pEquipList[m + i].bWorkState == TRUE))
#else

						if(pEquipList[m + i].bGroupType)
#endif
						{
							iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szEngEquipName);
							bEquipNOGroup = TRUE;

						}				
					}

					for( i = 0; i < iEquipTypeNum[j].iNum; i++)
					{
#ifdef AUTO_CONFIG
						if(!pEquipList[m + i].bGroupType && (pEquipList[m + i].bWorkState == TRUE))
#else

						if(!pEquipList[m + i].bGroupType)
#endif
						{
							iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szEngEquipName);

						}
					}
				}
				if(bEquipNOGroup == TRUE)
				{
					iLen += sprintf(pMakeBuf + iLen,"1,");
					bEquipNOGroup = FALSE;

				}
				if(iEquipTypeNum[j].iEquipTypeID / 100 == 9)//		//Special for ESNA
				{
					int iESNAConverter1 = 0;
					
					for(iESNAConverter1 = 0; iESNAConverter1 < MAX_EQUIP_NUM; iESNAConverter1++)
					{
						if(pEquipList[iESNAConverter1].iEquipType == 2600)
						{
							break;
						}
					}	
					
#ifdef AUTO_CONFIG
					if(pEquipList[iESNAConverter1].bWorkState == TRUE)
#endif

	 				/*if this group has one equip,print equip directlly*/
					{
						iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[iESNAConverter1].szEngEquipName);
					}
				}
			}
			else if(iEquipTypeNum[j].iNum == 1)
			{
					
#ifdef AUTO_CONFIG
				if(pEquipList[m].bWorkState == TRUE)
#endif

		 		/*if this group has one equip,print equip directlly*/
				{
					iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m ].szEngEquipName);
				}
			}
			m += iEquipTypeNum[j].iNum;
		}
		
		iLen += sprintf(pMakeBuf + iLen,"%s", "1,");
//printf("\n____7______INTO Web_MakeEquipListBuffer_____________\n ");
		*ppBuf = pMakeBuf;


//////////////////////////////////////////////////////////////////////////

        /*Make Loc2 language node*/
        #ifdef _SUPPORT_THREE_LANGUAGE
        pMakeBuf[iLen] = TREE_LOC2_START;
        iLen += 1;
        m = 0;
        iShowRectNumber =  iRectifierNumber;
        for(j = 0; j < iMaxEquipTypeInFact; j++)
        {
            if(iEquipTypeNum[j].iNum > 1)
            {

                if(iEquipTypeNum[j].iEquipTypeID/100 == 2 && IsGroupNeedDisplay(2))
                {
                    bEquipNOGroup = TRUE;
                    for( i = 0; i < iEquipTypeNum[j].iNum; i++)
                    {
                        if(pEquipList[m + i].bGroupType)
                        {
                            iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szLoc2EquipName);

                            /*print equip group*/
                        }				
                    }

                    for( i = 0; i < iEquipTypeNum[j].iNum && iShowRectNumber > 0; i++)
                    {
                        if(!pEquipList[m + i].bGroupType)
                        {
                            iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLoc2EquipName);
                            iShowRectNumber--;
                        }
                    }
                }
                else
                {
                    for( i = 0; i < iEquipTypeNum[j].iNum; i++)
                    {
                        if(pEquipList[m + i].bGroupType)
                        {
                            iLen += sprintf(pMakeBuf + iLen," 0,\"%s\",\"\",\"\",\"img/rect.ico\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",-1,",pEquipList[m + i].szLoc2EquipName);
                            bEquipNOGroup = TRUE;
                            /*print equip group*/
                        }				
                    }

                    for( i = 0; i < iEquipTypeNum[j].iNum; i++)
                    {
                        if(!pEquipList[m + i].bGroupType)
                        {
                            iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m + i].szLoc2EquipName);

                        }
                    }
                }
                if(bEquipNOGroup == TRUE)
                {
                    iLen += sprintf(pMakeBuf + iLen,"1,");
                    bEquipNOGroup = FALSE;

                }
            }
            else if(iEquipTypeNum[j].iNum == 1)
            {	

                /*if this group has one equip,print equip directlly*/
                iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/rect.ico\",4,\"parent.site_view.location=\\\"p03_main_menu.htm\\\"\",\"\",\"\",",pEquipList[m ].szLoc2EquipName);
            }
            m += iEquipTypeNum[j].iNum;
        }

        iLen += sprintf(pMakeBuf + iLen,"%s", "1,");

        #else//����λ��
        pMakeBuf[iLen] = TREE_LOC2_START;
        iLen += 1;
        iLen += sprintf(pMakeBuf + iLen,"  ");
        #endif

//end////////////////////////////////////////////////////////////////////////

	//printf("\n_______8___INTO Web_MakeEquipListBuffer_____________\n ");	

		int nInterfaceType = VAR_USER_DEF_PAGES;		
		int nVarID = 0;
		int nVarSubID = 0;
		int nBufLen;
		int nTimeOut = 0;
		int nError = ERR_DXI_OK;
		USER_DEF_PAGES *pUserDefPages = NULL;
		PAGE_NAME *pTempPageName = NULL;

		nError = DxiGetData(nInterfaceType,
			nVarID,			
			nVarSubID,		
			&nBufLen,			
			&pUserDefPages,			
			nTimeOut);

		pMakeBuf[iLen] = TREE_LOC_START;
		iLen += 1;
		//pTempPageName = pUserDefPages->pPageInfo->pPageName;
//printf("\n_______9___INTO Web_MakeEquipListBuffer_____________\n ");	
		//if(pUserDefPages->pPageInfo->iPageInfoNum > 0)
		//{
		//	for( i = 0; i < pUserDefPages->pPageInfo->iPageInfoNum; i++ ,pTempPageName++)
		//	{

		//		iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/edit.ico\",4,\"parent.site_view.view.location=\\\"p81_user_def_page.htm\\\"\",\"\",\"\",",pTempPageName->pFullName[1]);//loc

		//	}

		//	iLen += sprintf(pMakeBuf + iLen,"%s", "1,");

		//	pMakeBuf[iLen] = TREE_ENG_START;
		//	iLen += 1;
		//	pTempPageName = pUserDefPages->pPageInfo->pPageName;
		//	for( i = 0; i < pUserDefPages->pPageInfo->iPageInfoNum; i++ ,pTempPageName++)
		//	{

		//		iLen += sprintf(pMakeBuf + iLen,"2,\"%s\",\"\",\"\",\"img/edit.ico\",4,\"parent.site_view.view.location=\\\"p81_user_def_page.htm\\\"\",\"\",\"\",",pTempPageName->pFullName[0]);//eng

		//	}

		//	printf("USER_DEFINE_PAGES No.:%d",pUserDefPages->pPageInfo->iPageInfoNum);

		//	iLen += sprintf(pMakeBuf + iLen,"%s", "1,");

		//	pMakeBuf[iLen] = TREE_LOC_START;
		//	iLen += 1;
		//	pTempPageName = pUserDefPages->pPageInfo->pPageName;
		//	for( i = 0; i < pUserDefPages->pPageInfo->iPageInfoNum; i++ ,pTempPageName++)
		//	{

		//		iLen += sprintf(pMakeBuf + iLen,"new OUserDefPages(\"%s\",%d), \n",pTempPageName->pFullName[1],
		//			pTempPageName->iPageID);//loc

		//	}
		//	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
		//	if(pSearchValue != NULL)
		//	{
		//		*pSearchValue = 32;   //replace ',' with ' '
		//	}

		//	//iLen += sprintf(pMakeBuf + iLen,"%s", "1,");

		//	pMakeBuf[iLen] = TREE_ENG_START;
		//	iLen += 1;
		//	pTempPageName = pUserDefPages->pPageInfo->pPageName;
		//	for( i = 0; i < pUserDefPages->pPageInfo->iPageInfoNum; i++,pTempPageName++)
		//	{

		//		iLen += sprintf(pMakeBuf + iLen,"new OUserDefPages(\"%s\",%d), \n",pTempPageName->pFullName[0],
		//			pTempPageName->iPageID);//eng

		//	}
		//	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
		//	if(pSearchValue != NULL)
		//	{
		//		*pSearchValue = 32;   //replace ',' with ' '
		//	}

		//	printf("USER_DEFINE_PAGES No.:%d",pUserDefPages->pPageInfo->iPageInfoNum);

		//	//iLen += sprintf(pMakeBuf + iLen,"%s", "1,");


		//}
		//else
		{
			iLen += sprintf(pMakeBuf + iLen,"%s1,;%s1,;%s;%s", " "," "," "," ");

		}

		

        *ppBuf = pMakeBuf;
		//printf("\n___%s___\n",pMakeBuf);


        return iLen;
	}
	else
	{
		return FALSE;
	}
	
}


/*==========================================================================*
 * FUNCTION :  Web_GetEquipNameFromInterface
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				IN int iLanguage:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
 *==========================================================================*/
static char *Web_GetEquipNameFromInterface(IN int iEquipID,IN int iLanguage)
{

	int		i, iEquipNum = 0;
	char	*szEquipName = NULL;
	EQUIP_INFO		*pEquipInfo = NULL;
	EQUIP_INFO		*pTEquipInfo = NULL;
	char		szDisBuf[128];
	static	int			iRectSNIndex = -1;
	static	int			iRectHighSNIndex = -1;

	if(iRectSNIndex == -1)
	{
		iRectSNIndex = Web_GetRectSNIndex();
	}

	if(iRectHighSNIndex == -1)
	{
		iRectHighSNIndex = Web_GetRectHighSNIndex();
	}

	if(Web_GetEquipListFromInterface(&pEquipInfo) <= 0)
	{
		return NULL;
	}
	int iError = DxiGetData(VAR_ACU_EQUIPS_NUM,
							0,			
							iVarSubID,		
							&iBufLen,			
							(void *)&iEquipNum,			
							iTimeOut);

	szEquipName = NEW(char, 64);
	

	if(szEquipName == NULL)
	{
		return NULL;
	}
	memset(szEquipName, 0, 64);

	pTEquipInfo = pEquipInfo;
	if(iError == ERR_DXI_OK)
	{
		for(i = 0; i < iEquipNum && pTEquipInfo != NULL; i++, pTEquipInfo++)
		{
			if(iEquipID == pTEquipInfo->iEquipID)
			{
				//if(pTEquipInfo->iEquipTypeID == 201)
				//{
				//	////Modifyby YangGuoxin,3/16/2006
				//	if( !Web_GetRectHighValid(pTEquipInfo->iEquipID))// old rectifier
				//	{
				//		snprintf(szDisBuf, sizeof(szDisBuf), "2%02d%01X%06d", 
				//				//pTEquipInfo->pEquipName->pFullName[iLanguage],
				//				((pTEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue) >> 24) & 0x1F,
				//				((pTEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue) >> 16) & 0x0F,
				//				LOWORD((DWORD)(pTEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue)));

				//		strncpyz(szEquipName, szDisBuf, 64);
				//	//printf("\nold = %s\n", szEquipName);
				//	}
				//	else//new rectifier
				//	{
				//		TRACE("WEB SITE VALUE is %u\n",Web_GetRectHighSNValue(iEquipID));
				//		snprintf(szDisBuf, sizeof(szDisBuf), "%02u%02d%02d%05d", 
				//							Web_GetRectHighSNValue(iEquipID),//pTEquipInfo->pSampleSigValue[iRectHighSNIndex].bv.varValue.ulValue,
				//							((pTEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue) >> 24) & 0x1F,
				//							((pTEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue) >> 16) & 0x0F,
				//							LOWORD((DWORD)(pTEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue)));
				//		strncpyz(szEquipName, szDisBuf, 64);
				//		
				//	}
				//	//End by YangGuoxin.3/16/2006
				//	snprintf(szEquipName,sizeof(szEquipName), "Rectifier #%d", i);
				//	//printf("szEquipName[%s]\n", szEquipName);
				//}
				//else
				{
					strncpyz(szEquipName, pTEquipInfo->pEquipName->pFullName[iLanguage], 64);
				}
				

				return szEquipName;
	        }
			
		}

	}
    else
    {
        SAFELY_DELETE(szEquipName);
        
        return NULL;
    }
	
	return NULL;

}


static char *Web_GetEquipNameFromInterface_New(IN int iEquipID,IN int iLanguage,IN int iPosition)
{


	char	*szEquipName = NULL;
	int			iPos, iBufLen;
	EQUIP_INFO		*pEquipInfo = NULL;

	int iError = DxiGetData(VAR_A_EQUIP_INFO,			
		iEquipID,	
		0,
		&iBufLen,			
		&pEquipInfo,			
		0);

	szEquipName = NEW(char, 64);
	if(szEquipName == NULL)
	{
		return NULL;
	}
	memset(szEquipName, 0, 64);

	szEquipName = NEW(char, 64);


	if(szEquipName == NULL)
	{
		return NULL;
	}
	memset(szEquipName, 0, 64);

	
	if(iError == ERR_DXI_OK)
	{		
		if(iPosition != 0)
		{

			strncpyz(szEquipName, pEquipInfo->pEquipName->pFullName[iLanguage], 64);
			iPos = Web_GetEquipName(pEquipInfo->pEquipName->pFullName[iLanguage]);
			snprintf(szEquipName + iPos, 5, "#%d", iPosition);
			//printf("iPosition = %d\n", iPosition);
		}				
		else
		{
			strncpyz(szEquipName, pEquipInfo->pEquipName->pFullName[iLanguage], 64);
		}


		return szEquipName;

	}
	else
	{
		SAFELY_DELETE(szEquipName);
		return NULL;
	}

	return NULL;

}
static int Web_GetEquipName(IN OUT char *szEquipName)
{
#define IS_NUMBER(c)								(((c)>=(0x30) && (c)<=(0x39)) || (c) == 0x23)
	char *p=szEquipName;
	/*char pfinal[30]="";*/
	int i=0;
	while(*p)
	{
		if(IS_NUMBER(*p)) 
		{
			p=p++;
			break;
		}
		else
		{
			//pfinal[i]=*p;
			p++;
			i++;
		}
	}
	//p=strdup(pfinal);
	return i;
}
/*==========================================================================*
 * FUNCTION :  Web_GetStatusData
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:35
 *==========================================================================*/
static int Web_GetStatusData(OUT char **ppBuf)
{

	int					i, iLen = 0;
	int					iError = 0;
	SIG_BASIC_VALUE		*pSigValue = NULL;
	char				*pMBuf = NULL;
	int					iEquipID, iSignalID = 0, iSignalType = 0;
	int					iVarSubID = 0, iStatusSignalNumber = 0; 
		
	if(stStatusConfig != NULL)
	{
		iStatusSignalNumber = stStatusConfig->iNumber;
		pMBuf = NEW(char, iStatusSignalNumber * MAX_STATUS_VALUE_LEN);
		if(pMBuf == NULL)
		{
			return FALSE;
		}

		/*get Value*/

		for( i = 0; i < iStatusSignalNumber; i++)
		{
			iEquipID = stStatusConfig->stWebStatusConfig[i].iEquipID;
			iSignalID = stStatusConfig->stWebStatusConfig[i].iSignalID;
			iSignalType = stStatusConfig->stWebStatusConfig[i].iSignalType;
			iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
			
			//TRACE("%d[%d,%d,%d]\n", i, iEquipID, iSignalID, iSignalType);
			iError += DxiGetData(VAR_A_SIGNAL_VALUE,
								iEquipID,			
								iVarSubID,		
								&iBufLen,			
								(void *)&pSigValue,			
								0);
			iSignalType = pSigValue->ucType;
			if(iError == ERR_DXI_OK)
			{
				if( iSignalType == VAR_FLOAT)
				{
					iLen += sprintf(pMBuf + iLen, "%8.2f,%d,", 
								pSigValue->varValue.fValue,
								SIG_VALUE_IS_VALID(pSigValue));
				}
				else if(iSignalType == VAR_LONG)
				{
					iLen += sprintf(pMBuf + iLen, "%8ld,%d,", 
									pSigValue->varValue.lValue,
									SIG_VALUE_IS_VALID(pSigValue));
				}
				else if(iSignalType == VAR_UNSIGNED_LONG)
				{
					iLen += sprintf(pMBuf + iLen, "%8lu,%d,", 
									pSigValue->varValue.ulValue,
									SIG_VALUE_IS_VALID(pSigValue));
				}
				else if(iSignalType == VAR_ENUM)
				{
					iLen += sprintf(pMBuf + iLen, "%8ld,%d,", 
									pSigValue->varValue.enumValue,
									SIG_VALUE_IS_VALID(pSigValue));
				}
					
			}
			else
			{
                DELETE(pMBuf);//Added by wj 2006.07.10
                pMBuf = NULL;//Added by wj 2006.07.10
				return FALSE;
			}
		}
	}
	else
	{
		return FALSE;
	}
	pMBuf[iLen - 1] = 32;
	//TRACE("pMBuf[%s]\n",pMBuf);
	*ppBuf = pMBuf;
	//TRACE("pMBuf[%s]\n", pMBuf);
	return iLen;
}



/*==========================================================================*
 * FUNCTION :  Web_GetShowStatusData
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN void *pCfg:
				OUT WEB_STATUS_CONFIG *stStatusConfig:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
 *==========================================================================*/
static int Web_GetShowStatusData(IN void *pCfg, 
								 OUT WEB_STATUS_CONFIG *stStatusConfig)
{
#define WEB_NUMBER_OF_STATUS_CONFIG_INFORMATION		"[NUMBER_OF_CONFIG_SHOW]"
#define	WEB_STATUS_CONFIG_INFORMATION				"[CONFIG_STATUS_SHOW]"

#define	WEB_NUMBER_OF_NETSCAPE_CONFIG_SHOW			"[NUMBER_OF_NETSCAPE_CONFIG_SHOW]"
#define	WEB_FIRSTPAGE_SHOW_SIGNAL					"[SUPPORT_FIRSTPAGE_SHOW_SIGNAL]"

#define	WEB_SUPPORT_EXPLORER_TYPE					"[SUPPORT_EXPLORER_TYPE]"


	CONFIG_TABLE_LOADER			loader[2];

	DEF_LOADER_ITEM(&loader[0],
		WEB_NUMBER_OF_STATUS_CONFIG_INFORMATION,
		&(stStatusConfig->iNumber), 
		WEB_STATUS_CONFIG_INFORMATION, 
		&(stStatusConfig->stWebStatusConfig), 
		Web_ParseWebStatusInfo);

	DEF_LOADER_ITEM(&loader[1],
		WEB_NUMBER_OF_NETSCAPE_CONFIG_SHOW,
		&(stStatusConfig->iNetscapeNumber), 
		WEB_FIRSTPAGE_SHOW_SIGNAL, 
		&(stStatusConfig->stNetscapeConfig), 
		Web_ParseWebStatusInfo);

	if (Cfg_LoadTables(pCfg, 2, &loader) != ERR_CFG_OK)
	{
		TRACE("Fail to Cfg_LoadTables\n");
		return ERR_LCD_FAILURE;
	}

	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION :  Web_ReadStatusConfig
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT WEB_STATUS_CONFIG **stReturnStatusConfig:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
 *==========================================================================*/
static int Web_ReadStatusConfig(OUT WEB_STATUS_CONFIG **stReturnStatusConfig )
{
	int				iRst;
	void			*pProf = NULL;  
	FILE			*pFile = NULL;
	char			*szInFile = NULL;
	size_t			ulFileLen;

	char			szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_WEB_PRIVATE_R, szCfgFileName, MAX_FILE_PATH);
	/*Debug*/	

	/* open file */
	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{		
		return FALSE;
	}

	//TRACE("Successfully, Into fopen %s!", szCfgFileName);

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return FALSE;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);
		return FALSE;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

	if (pProf == NULL)
	{
		DELETE(szInFile);
		return FALSE;
	}

	//2.Read current all command content and command numbers
	WEB_STATUS_CONFIG		*stStatusConfig;
	stStatusConfig = NEW(WEB_STATUS_CONFIG, 1);
	iRst = Cfg_ProfileGetInt(pProf,
		"[SUPPORT_EXPLORER_TYPE]", 
		&stStatusConfig->iExploreType); 

	iRst = Web_GetShowStatusData(pProf, stStatusConfig);

	if (iRst != ERR_WEB_OK)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
			"Fail to get resource table.");

		DELETE(pProf); 
		DELETE(szInFile);
		return iRst;
	}

	*stReturnStatusConfig = stStatusConfig;


	/*//TRACE(" Web_ReadStatusConfig : stReturnStatusConfig:%d : %d : %d  :%d : %d\n", stReturnStatusConfig->iNumber, 
	stReturnStatusConfig->stWebStatusConfig[0].iEquipID,
	stReturnStatusConfig->stWebStatusConfig[1].iEquipID,
	stReturnStatusConfig->stWebStatusConfig[2].iEquipID,
	stReturnStatusConfig->stWebStatusConfig[3].iEquipID);*/

	DELETE(pProf); 
	DELETE(szInFile);
	return ERR_WEB_OK;
}



/*==========================================================================*
 * FUNCTION :  Web_ParseWebStatusInfo
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szBuf:
				OUT WEB_STATUS_CONFIG_INFO *pStructData:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
 *==========================================================================*/
static int Web_ParseWebStatusInfo(IN char *szBuf, 
								OUT WEB_STATUS_CONFIG_INFO *pStructData)
{
	char	*pField = NULL;
	char	szBuffer[33];
	/* used as buffer */
	ASSERT(szBuf);
	ASSERT(pStructData);
	/* EquipID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;    
	}
	strncpyz(szBuffer, pField, sizeof(szBuffer));
	//TRACE("pStructData->iEquipID : %s\n", szBuffer);
	pStructData->iEquipID = atoi(szBuffer);
	
	/* SignalType */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 2;    
	}
	strncpyz(szBuffer, pField, sizeof(szBuffer));
	//TRACE("pStructData->iSignalType : %s\n", szBuffer);
	pStructData->iSignalType = atoi(szBuffer);
	
	/* SignalID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 3;    
	}
	strncpyz(szBuffer, pField, sizeof(szBuffer));
	//TRACE("pStructData->iSignalID : %s\n", szBuffer);

	pStructData->iSignalID = atoi(szBuffer);

	return ERR_WEB_OK;
}

#define		GET_SERVICE_OF_WEB_UI_NAME	"web_ui_comm.so"
/*==========================================================================*
 * FUNCTION :  StartWebCommunicate
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int *iQuitCommand:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
 *==========================================================================*/
int StartWebCommunicate(IN int *iQuitCommand)
//int main(int argc,char *argv[])
{
	int				fd;
	int				iLen;
	char			buf[4096];   // fifo read buffer ,get parameter from CGI
	char			filename[FIFO_NAME_LEN];  //fifo write buffer,init according to the CGI process
	mode_t			mode = 0666;  //open fifo mode ,
	char			*ptr = NULL;
	char			szPid[8];
	char			szControlType[8];
	int				iControltype;
	BOOL			bContinueRunning = TRUE;
	int				iReturnError;
	int				iTimerSet;
	char			szCommand1[128];
	int			iWorkStatus;
	SIG_BASIC_VALUE		*pSigValue;
	int	iBufLen;
	int		iRst;
	unsigned long  ulInterval;
	APP_SERVICE		*WEB_UI_AppService = NULL;
	WEB_UI_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_WEB_UI_NAME);


	//Reset Net Driver////////////////////////////////////////////////////////////////////////

	

#define SYSTEM_EQUIP_ID	1
	//Timer reset will lead to watch dog error.Disable it.
	//iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	//    SYSTEM_EQUIP_ID,
	//    DXI_MERGE_SIG_ID(2, 450),
	//    &iBufLen,
	//    &pSigValue,
	//    0);
	//if(iRst == ERR_DXI_OK)
	//{
	//    printf("Net-Port reset value is %d\n", pSigValue->varValue.ulValue);
	//    if(pSigValue->varValue.ulValue >= 1)
	//    {
	//	ulInterval = pSigValue->varValue.ulValue * 3600000;
	//	iTimerSet = Timer_Set(RunThread_GetId(NULL),
	//	    WEB_NET_RESET_TIMER,
	//	    ulInterval,
	//	    //60000,
	//	    Web_ResetNetDriver,
	//	    NULL);
	//	if (iTimerSet == ERR_TIMER_EXISTS)
	//	{
	//	    TRACE("Alarm Report Timer has existed");
	//	}
	//	if (iTimerSet == ERR_TIMER_SET_FAIL)
	//	{
	//	    TRACE("Alarm Report Timer Failed");
	//	    AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Info : SET NET RESET TIMER FAILED!");
	//	}
	//    }
	//    else
	//    {/*nothing to do*/}
	//}
	//else
	//{/*nothing to do*/}

	//////////////////////////////////////////////////////////////////////////



 
	/*Firstly delete the MAIN_FIFO_NAME if it has been created*/
	char	szCommandStr[30] = "rm -f ";
	strcat(szCommandStr,MAIN_FIFO_NAME);
	system(szCommandStr);


#ifdef _DEBUG
	AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"Info : Communicate Initialize");
#endif

	/*Read status config show ID*/
	if(Web_ReadStatusConfig(&stStatusConfig) != ERR_WEB_OK)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Error : Fail to read web private configuration");
	}


//////////////////////////////////////////////////////////////////////////
	//Added by YangGuoxin;9/11/2006
	//mkdir /var/netscape
#define RUNNINT_AS_NETSCAPE					1
	if(stStatusConfig->iExploreType == RUNNINT_AS_NETSCAPE)
	{


	//chmod
 	char szCommand[128] = "chmod 777  ";
	strcat(szCommand,MAIN_FIFO_NAME);
	system(szCommand);
 
	sprintf(szCommand, "chmod 777 %s", HTML_SAVE_REAL_PATH);
	system(szCommand);

    sprintf(szCommand, "chmod 777 %s", HTML_SAVE_REAL2_PATH);//Added by wj for three languages 2006.4.28
    system(szCommand);
	
	sprintf(szCommand, "chmod 777 %s", HTML_SAVE_ENG_PATH);
	system(szCommand);

	sprintf(szCommand,"mkdir %s", WEB_LOC_VAR);
	system(szCommand);

	sprintf(szCommand,"chmod 777 %s", WEB_LOC_VAR);
	system(szCommand);

    sprintf(szCommand,"mkdir %s", WEB_LOC2_VAR);//Added by wj for three languages 2006.4.28
    system(szCommand);

    sprintf(szCommand,"chmod 777 %s", WEB_LOC2_VAR);//Added by wj for three languages 2006.4.28
    system(szCommand);

	sprintf(szCommand,"mkdir %s", WEB_ENG_VAR);
	system(szCommand);

	sprintf(szCommand,"chmod 777 %s", WEB_ENG_VAR);
	system(szCommand);


	sprintf(szCommand,"chmod -R 777 %s", "/app");
	system(szCommand);

		if(Web_Netscape_MultiLang() == FALSE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Error : Fail to Create Multi_Languages Web Pages");
			return FALSE;

		}

		TRACE("Netscape Web Pages Create OK !!!!\n");



		sprintf(szCommand1,"mkdir %s;chmod 777 %s", "/var/netscape","/var/netscape");
		system(szCommand1);

		HANDLE hCmdThread = RunThread_Create("Web command thread",
			(RUN_THREAD_START_PROC)fnProcessCmdThread,
			(void*)NULL,
			(DWORD*)NULL,
			0);

		Web_CopyNeedFile();
		Web_PageRefresh(iQuitCommand);
		RunThread_Stop(hCmdThread,1000, 1);
	}
	//End by YangGuoxin;9/11/2006
	else
	{


	//create fifo that all process can access
	if((mkfifo(MAIN_FIFO_NAME,mode))<0)
	{
#ifdef _DEBUG
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Error : Fail to  make main fifo [%s]", MAIN_FIFO_NAME);
#endif
		return FALSE;
	}

	//chmod
 	char szCommand[128] = "chmod 777  ";
	strcat(szCommand,MAIN_FIFO_NAME);
	system(szCommand);
 
	sprintf(szCommand, "chmod 777 %s", HTML_SAVE_REAL_PATH);
	system(szCommand);

    sprintf(szCommand, "chmod 777 %s", HTML_SAVE_REAL2_PATH);//Added by wj for three languages 2006.4.28
    system(szCommand);
	
	sprintf(szCommand, "chmod 777 %s", HTML_SAVE_ENG_PATH);
	system(szCommand);

	sprintf(szCommand,"mkdir %s", WEB_LOC_VAR);
	system(szCommand);

	sprintf(szCommand,"chmod 777 %s", WEB_LOC_VAR);
	system(szCommand);

    sprintf(szCommand,"mkdir %s", WEB_LOC2_VAR);//Added by wj for three languages 2006.4.28
    system(szCommand);

    sprintf(szCommand,"chmod 777 %s", WEB_LOC2_VAR);//Added by wj for three languages 2006.4.28
    system(szCommand);

	sprintf(szCommand,"mkdir %s", WEB_ENG_VAR);
	system(szCommand);

	sprintf(szCommand,"chmod 777 %s", WEB_ENG_VAR);
	system(szCommand);


	sprintf(szCommand,"chmod -R 777 %s", "/app");
	system(szCommand);

#ifdef _DEBUG
	Web_ClearSessionFile();
#endif 

	if((fd = open(MAIN_FIFO_NAME,O_RDONLY | O_NONBLOCK))<0)
	{
#ifdef _DEBUG
		AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Error : Fail to open main FIFO [%s]", MAIN_FIFO_NAME);
#endif 
		return FALSE;
	}

	RunThread_Heartbeat(RunThread_GetId(NULL));

	system("cp /app/www/firstpage.htm /app/www/index/");
	
#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread ACU+ WEB Service was created, Thread Id = %d.\n", gettid());
#endif	
	///* Register in the DXI module to receive Alarms */
	DxiRegisterNotification("Web Agent", 
		ServiceNotification,
		NULL, // it shall be changed to the actual arg
		(_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK)/*|_SAMPLE_MASK|_SET_MASK|_CONTROL_MASK*/, 
		RunThread_GetId(NULL),
		_REGISTER_FLAG);

	//if client was closed, ignore the error and return ;
	signal(SIGPIPE, SIG_IGN);

	/*Transfer web page*/
	iRectifierNumber =  Web_GetRectifierNumber();
	
	if(StartTransferFile(iQuitCommand) == FALSE)//modified by wj for three languages 2006.4.28
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Error : Fail to read web resource  configuration");
	}
	////TRACE("End:StartTransferFile\n");
	
    //StartApache();
	

 

	//Get rectifier number

	iRectifierNumber =  Web_GetRectifierNumber();
    iACDNumber = 0; //Web_GetACDNumber();
    iDCDNumber = 0; //Web_GetDCDNumber();
    iBATTNumber = Web_GetBATTNumber();
	iEIB1BattNum = Web_GetEIBBattNumber(197);
	iEIB2BattNum = Web_GetEIBBattNumber(198);
	iEIB3BattNum = Web_GetEIBBattNumber(199);
	iEIB4BattNum = Web_GetEIBBattNumber(200);
	iBatLCNum = Web_GetCSUNum(0);

	iDCLCNum = Web_GetCSUNum(1);
	iSMDUCFGStatus = Web_GetSMDUCFGStatus();
	iConverterNumber = Web_GetConverterNumber();
	iWorkStatus = 	Web_WorkStatus();
	//TRACE("iRectifierNumber[%d]\n", iRectifierNumber);

	RunThread_Heartbeat(RunThread_GetId(NULL));

    TRACE("Start web ok!\n");

    s_hMutexLoadPLCCFGFile = Mutex_Create(TRUE);
    s_hMutexLoadAlarmCFGFile = Mutex_Create(TRUE);
	s_hMutexLoadGCCFGFile = Mutex_Create(TRUE);


    //TRACE("s_hMutexLoadPLCCFGFile Mutex_Create OK!!!\n");
    //TRACE("s_hMutexLoadAlarmCFGFile Mutex_Create OK!!!\n");


//Test//////////////////////////////////////////////////////////////////////////

	//int nInterfaceType = VAR_USER_DEF_PAGES;		
	//int nVarID = 0;
	//int nVarSubID = 0;
	//int nBufLen;
	//int nTimeOut = 0;
	//int nError = ERR_DXI_OK;
	//USER_DEF_PAGES *pUserDefPages = NULL;

	//nError = DxiGetData(nInterfaceType,
	//	nVarID,			
	//	nVarSubID,		
	//	&nBufLen,			
	//	&pUserDefPages,			
	//	nTimeOut);

	//printf("\n_______USER_DEFINE_PAGES No.:%d_______________\n",pUserDefPages->pPageInfo->iPageInfoNum);
	//printf("\n_______USER_DEFINE_PAGES Sig No.:%d_______________\n",pUserDefPages->pSigItemInfo->iSigItemNum);

	//Web_MakeRectifierRefresh();
	//printf("\n_______Web_MakeRectifierRefresh() OK!!_______________\n");

//////////////////////////////////////////////////////////////////////////
	if(WEB_UI_AppService != NULL)
	{
		WEB_UI_AppService->bReadyforNextService = TRUE;
	}
	while (bContinueRunning)
	{
	    /*pid_t pid1;
	    int istatus = 0;
	    if((pid1 = fork()) < 0)
	    {
		printf("fork fail\n");
		istatus = -1;
	    }
	    else if(pid1 == 0)
	    {
		printf("fork success\n");
		execl("/bin/sh", "sh", "-c", "ls", (char *)0);
		_exit(127);
	    }
	    else
	    {
		while(waitpid(pid1, &istatus, 0) < 0)
		{
		    if(errno != EINTR)
		    {
			istatus = -1;
			break;
		    }
		}
	    }

	    printf("istatus is %d\n", istatus);
	    if(istatus == -1)
	    {
		printf("Error is %s\n", strerror(errno));
	    }*/
		/*tell main web_ui_comm.so living*/
		if(StartFlage == 0)
		{
		    iRollFlage++;
		    //TRACE("iRollFlage=%d\n",iRollFlage);
		    if(iRollFlage > 50)
		    {
			StartFlage = 1;

			/*if(iWorkStatus == 1)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				HANDLE hCmdThread = RunThread_Create("Auto Config",
					(RUN_THREAD_START_PROC)DIX_AutoConfigMain,
					(void*)NULL,
					(DWORD*)NULL,
					0);

				Sleep(300);

			}
			*/

		    }
		}

//#ifdef AJAX_TEST
//
//	//Web_AJAX_TEST();
//
//#endif

//printf("Web_ui:~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`1\n");

#ifdef AUTO_CONFIG

		int	iRst1;
		RUN_THREAD_MSG msg1;

		iRst1 = RunThread_GetMessage(RunThread_GetId(NULL), &msg1, FALSE, 0);

		if(iRst1 == ERR_OK)
		{
			TRACE("msg1.dwMsg : %d\n",msg1.dwMsg);
			if(msg1.dwMsg == MSG_AUTOCONFIG)
			{
				switch(msg1.dwParam1)
				{
				case ID_AUTOCFG_START:
					TRACE("WEB START AUTO CONFIG!!!\n");
					iAutoCFGStartFlage = 1;
					break;

				case ID_AUTOCFG_REFRESH_DATA:
					TRACE("WEB START AUTO CONFIG REFRESH!!!\n");
					Web_MakeRectifierRefresh(FALSE);
					TRACE("Web_MakeRectifierRefresh!!!\n");
					iAutoCFGStartFlage = 0;
					break;
				default:
					break;
				}

			}
		}

#endif
		iSlaveMode = GetSlaveMode();
//printf("Web_ui:~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`2\n");
        
		while ((WaitFiledReadable(fd, 1000) > 0) )
 		{
// printf("Web_ui:~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`3\n");
		
 			RunThread_Heartbeat(RunThread_GetId(NULL));

			if((iLen = read(fd,buf,PIPE_BUF)) > 0)
			{
				;
			}
			else
			{
				Sleep(1000);
				break;
			}
#ifdef AUTO_CONFIG
			/*int	iRst2;
			RUN_THREAD_MSG msg2;

			iRst2 = RunThread_GetMessage(RunThread_GetId(NULL), &msg2, FALSE, 0);

			printf("iRst2 = %d\n",iRst2);
			if(iRst2 == ERR_OK)
			{
			
				if(msg2.dwMsg == MSG_AUTOCONFIG)
				{
					switch(msg2.dwParam1)
					{
					case ID_AUTOCFG_START:
						TRACE("WEB START AUTO CONFIG!!!\n");
						iAutoCFGStartFlage = 1;
						break;

					case ID_AUTOCFG_REFRESH_DATA:
						TRACE("WEB START AUTO CONFIG REFRESH!!!\n");
						Web_MakeRectifierRefresh(FALSE);
						TRACE("Web_MakeRectifierRefresh!!!\n");
						iAutoCFGStartFlage = 0;
						break;
					default:
						break;
					}

				}
				
			}*/

			

#endif
//	printf("Web_ui:~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`4\n");

			iSlaveMode = GetSlaveMode();
			buf[iLen] = '\0';
#ifdef _SHOW_WEB_INFO
			//TRACE("GetBuffer: buf %s \n iLen : %d \n",buf, iLen);
#endif	
			TRACE("\nRESET Timer!!!\n");
			if(pSigValue->varValue.ulValue >= 1)
			{
			    Timer_Reset(RunThread_GetId(NULL),WEB_NET_RESET_TIMER);
			}
			
			//0-9 in buf is pid ,get the fifo name to write data .
			ptr = buf ;
			strncpyz(szPid,ptr, MAX_COMM_PID_LEN +1);
			
//	printf("Web_ui:~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`5\n");


			if (atoi(szPid) > 0)
			{
				sprintf(filename,"%s/fifo.%-d",CGI_CLIENT_FIFO_PATH,atoi(szPid));
		
				ptr = ptr + MAX_COMM_PID_LEN ;
				 
				//10-11 in buf is control type
				strncpyz(szControlType,ptr,MAX_COMM_GET_TYPE + 1);
				iControltype = atoi(szControlType);
				ptr = ptr + MAX_COMM_GET_TYPE ;

		
				switch(iControltype)
				{
					case LOG_IN:
						{
							Web_MakeHisData();
                            
							iReturnError = Web_SendEquipList(buf, filename);

							break;
						}
					case REALTIME_DATA:
						{
							iReturnError = Web_SendRealtimeData(buf,filename);
							break;
						}
					case CONTROL_COMMAND:
						{
							//TRACE("\n_____GetBuffer1: buf %s \n iLen : %d \n",buf, iLen);
							iReturnError = Web_SendControlCommandResult(buf,filename);
							break;
						}
					case HISTORY_QUERY:
						{
							iReturnError = Web_SendQueryData(buf,filename);
							break;
						}
					case CONFIGURE_MANAGE:
						{
							////TRACE("Web_SendConfigureData \n");
							//TRACE("\n_____GetBuffer2: buf %s \n iLen : %d \n",buf, iLen);
							iReturnError = Web_SendConfigureData(buf,filename);
							break;
						}
					case FILE_MANAGE:
						{

							CloseACU();
							_SYSTEM("mount -t tmpfs tmpfs /usb -o size=16M");
							break;
						}
					case FILE_MANAGE_REPLACEFILE:
						{
							Web_ReplaceFile(buf);
							break;
						}
						
					default:
						
							break;
						
				}
			}
			
//printf("Web_ui:~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`6\n");

		
		}
		RUN_THREAD_MSG msg3;
		if(RunThread_GetMessage(RunThread_GetId(NULL), &msg3, FALSE, 0) == ERR_THREAD_OK)
		{
			if(msg3.dwMsg == MSG_AUTOCONFIG)
			{
				switch(msg3.dwParam1)
				{
				case ID_AUTOCFG_START:
					TRACE("WEB START AUTO CONFIG!!!\n");
					iAutoCFGStartFlage = 1;
					break;

				case ID_AUTOCFG_REFRESH_DATA:
					TRACE("WEB START AUTO CONFIG REFRESH!!!\n");
					Web_MakeRectifierRefresh(FALSE);
					TRACE("Web_MakeRectifierRefresh!!!\n");
					iAutoCFGStartFlage = 0;
					break;
				default:
					break;
				}

			}
			
			if(msg3.dwMsg == MSG_CONIFIRM_ID)
			{
				//printf("*****   WEB WEB WEB WEB ***** Recv Msg: Find Equip!!!\n");      //FXS debug
				Web_MakeRectifierRefresh(TRUE);
				iAutoCFGStartFlage = 2;

			}
			
		}
		else if(iAutoCFGStartFlage == 3)
		{
			iAutoCFGStartFlage = 0;
		}

	
		RunThread_Heartbeat(RunThread_GetId(NULL));
//	printf("Web_ui:~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`7\n");
		
		if (*iQuitCommand == SERVICE_STOP_RUNNING)
		{
			bContinueRunning = FALSE;
		}
		 
	}
	Mutex_Destroy(s_hMutexLoadPLCCFGFile);
	Mutex_Destroy(s_hMutexLoadAlarmCFGFile);
	Mutex_Destroy(s_hMutexLoadGCCFGFile);
	if(pSigValue->varValue.ulValue >= 1)
	{
	    Timer_Kill(RunThread_GetId(NULL),WEB_NET_RESET_TIMER);
	}

	}

	if (stStatusConfig != NULL)
	{	if (stStatusConfig->stWebStatusConfig != NULL)
	{
		DELETE(stStatusConfig->stWebStatusConfig);	
		stStatusConfig->stWebStatusConfig = NULL;
	}
	if (stStatusConfig->stNetscapeConfig != NULL)
	{
		DELETE(stStatusConfig->stNetscapeConfig);	
		stStatusConfig->stNetscapeConfig = NULL;
	}
	DELETE(stStatusConfig);
	stStatusConfig = NULL;

	}
	////TRACE("Successfully!");
	close(fd);


	//Cancel the register notification. YangGuoxin 2005/6/20
	DxiRegisterNotification("Web Agent", 
		ServiceNotification,
		NULL, // it shall be changed to the actual arg
		(_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK)/*|_SAMPLE_MASK|_SET_MASK|_CONTROL_MASK*/, 
		RunThread_GetId(NULL),
		_CANCEL_FLAG);

	//remove(MAIN_FIFO_NAME);
	
    return SERVICE_EXIT_OK;
 }

/*==========================================================================*
 * FUNCTION :  Web_SendEquipList
 * PURPOSE  :  return the EquipList to the CGI
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *buf:
				IN char *filename:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:01
 *==========================================================================*/
static int Web_SendEquipList(IN char *buf,IN char *filename)
{
	char	*ptr = NULL;
	char	szUser[MAX_USERNAME_LEN + 1];
	char	szPassword[MAX_PASSWORD_LEN + 1];
	char	szLanguage[5];
	int		fd2;
	char	*pszSendBuf = NULL;
	int		iReturn, iLen = 0, iNewLen = 0, iLanguage;
	char	*pTempBuf = NULL;

	
	if((fd2 = open(filename,O_WRONLY )) < 0)
	{
		return FALSE;
			
	}
	//if client was closed, ignore the error and return ;
	signal(SIGPIPE, SIG_IGN);
	/*offset*/
	ptr = buf + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;
	
	//get password ,User name is 16 bit long ;
	strncpyz(szUser, ptr, MAX_USERNAME_LEN + 1);
	ptr = ptr + MAX_USERNAME_LEN;

	//get password ,password is 16 bit long ;
	strncpyz(szPassword, ptr, MAX_PASSWORD_LEN + 1 );
	ptr = ptr + MAX_PASSWORD_LEN;

	strncpyz(szLanguage, ptr, MAX_LANGUAGE_TYPE_LEN + 1);
	iLanguage = atoi(szLanguage);
 

	iReturn = Web_GetAuthorityByUser(Cfg_RemoveWhiteSpace(szUser),Cfg_RemoveWhiteSpace(szPassword));
	//TRACE("iReturn : [%s] [%s]\n",Cfg_RemoveWhiteSpace(szUser),Cfg_RemoveWhiteSpace(szPassword));
	
	pszSendBuf = NEW(char , 3);
	
	if(pszSendBuf == NULL)
	{
        close(fd2);
		return FALSE;

	}
   /* stop = clock();
    TRACE("ClockStart = %ld\n",start);
    TRACE("ClockStop = %ld\n",stop);
    TRACE("Clock = %d\n",((stop - start)/CLOCKS_PER_SEC));*/

    if(StartFlage == 0)
    {
        iLen = sprintf(pszSendBuf,"%2d",WAIT_FOR_START);
        pszSendBuf[iLen] = 0;
        if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1)) < 0)
        {
            DELETE(pszSendBuf);
            pszSendBuf = NULL;
            close(fd2);
            /*kill client process*/
            char			szCommand[30];
            iLen = sprintf(szCommand,"kill ");
            strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

            TRACEX("%s\n", szCommand);
            system(szCommand);

            return FALSE;
        }
    }
#ifdef AUTO_CONFIG
	else if(iAutoCFGStartFlage == 1)
	{
		iLen = sprintf(pszSendBuf,"%2d",AUTO_CONFIG_START);
		pszSendBuf[iLen] = 0;
		if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1)) < 0)
		{
			DELETE(pszSendBuf);
			pszSendBuf = NULL;
			close(fd2);
			/*kill client process*/
			char			szCommand[30];
			iLen = sprintf(szCommand,"kill ");
			strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

			TRACEX("%s\n", szCommand);
			system(szCommand);

			return FALSE;
		}
	}

#endif
	else if(iSlaveMode == 1)
	{
		iLen = sprintf(pszSendBuf,"%2d",-5);
		pszSendBuf[iLen] = 0;
		if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1)) < 0)
		{
			DELETE(pszSendBuf);
			pszSendBuf = NULL;
			close(fd2);
			/*kill client process*/
			char			szCommand[30];
			iLen = sprintf(szCommand,"kill ");
			strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

			TRACEX("%s\n", szCommand);
			system(szCommand);

			return FALSE;
		}
	}

    else
    {
	    if (iReturn == NO_THIS_USER)
	    {
    #ifdef _DEBUG
		    AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Info : Fail to get user!");
    #endif
		    //not pass,return false
		    iLen = sprintf(pszSendBuf,"%2d",NO_THIS_USER);
		    pszSendBuf[iLen] = 0;
		    if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1)) < 0)
		    {
    #ifdef _DEBUG
			    AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Info : Fail to send data!");
    #endif
			    DELETE(pszSendBuf);
			    pszSendBuf = NULL;
			    close(fd2);
			    /*kill client process*/
			    char			szCommand[30];
			    iLen = sprintf(szCommand,"kill ");
			    strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

			    TRACEX("%s\n", szCommand);
			    system(szCommand);

			    return FALSE;
		    }
    			
	    }
	    else if(iReturn == NO_MATCH_PASSWORD)
	    {
    #ifdef _DEBUG
		    AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Info : Fail to get password!");	
    #endif
		    //not pass,return false
		    iLen = sprintf(pszSendBuf,"%2d",NO_MATCH_PASSWORD);
		    pszSendBuf[iLen] = 0;
		    if((write(fd2,pszSendBuf,sizeof(pszSendBuf) + 1))<0)
		    {
			    DELETE(pszSendBuf);
			    pszSendBuf = NULL;
			    close(fd2);
			    char			szCommand[30];
			    iLen = sprintf(szCommand,"kill ");
			    strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

			    TRACEX("%s\n", szCommand);
			    system(szCommand);
			    return FALSE;
		    }
	    }
	    else
	    {
		    iLen  += sprintf(pszSendBuf,"%2d",(int)iReturn);

		    //pass,return the authority and equiplist,make the return data
		    if((iNewLen = Web_MakeEquipListBuffer(&pTempBuf)) > 0 )
		    {
			    pszSendBuf = RENEW(char,pszSendBuf,iNewLen + iLen + 1);
                TRACE("Web_MakeEquipListBuffer OK!!!\n");
    			
		    }
	 	    iLen += sprintf(pszSendBuf + iLen, "%s", pTempBuf);
		    //strncpyz(pszSendBuf + iLen,pTempBuf,iNewLen + 1);
    	 
		    SAFELY_DELETE(pTempBuf);

		    /*Send ACU Information*/
		    if((iNewLen = Web_MakeACUInfoBuffer(&pTempBuf, -1)) > 0)
		    {
			    pszSendBuf = RENEW(char, pszSendBuf, iNewLen + iLen + 1);
                TRACE("Web_MakeACUInfoBuffer!!!\n");
    			
		    }
    		 
		    iLen += sprintf(pszSendBuf + iLen, "%s", pTempBuf);
    		 
		    SAFELY_DELETE(pTempBuf);

		    /*Send equip alarm  Information*/
		    ////TRACE("Start : Web_MakeEquipAlarmInfoBuffer\n");
		    if((iNewLen = Web_MakeEquipAlarmInfoBuffer(&pTempBuf)) > 0)
		    {
			    pszSendBuf = RENEW(char, pszSendBuf, iNewLen + iLen + 1);
                //TRACE("Web_MakeEquipAlarmInfoBuffer!!!\n");
    			
		    }
    		 
		    ////TRACE("End : Web_MakeEquipAlarmInfoBuffer : Info : %s\n", pTempBuf);
		    iLen += sprintf(pszSendBuf + iLen, "%s", pTempBuf);
		    SAFELY_DELETE(pTempBuf);

		    char stTemp[5] ;
		    stTemp[0] = 59;
		    stTemp[4] = 32;
		   	//sprintf(stTemp+1,"%3d",iRectifierNumber);
			sprintf(stTemp+1,"%3d",Web_GetRectifierIncludSlave());	

		    iNewLen = 5;
		    pszSendBuf = RENEW(char, pszSendBuf, iNewLen + iLen + 1);
		    iLen += sprintf(pszSendBuf + iLen, "%s", stTemp);
    		
    	
		    /*Send Group equip Information*/
		    ////TRACE("Start : Web_MakeGroupDeviceList\n");
		    if((iNewLen = Web_MakeGroupDeviceList(&pTempBuf)) > 0)
		    {
			    pszSendBuf = RENEW(char, pszSendBuf, iNewLen + iLen + 1);
                //TRACE("Web_MakeGroupDeviceList!!!\n");
    			
		    }
    		 
		    iLen += sprintf(pszSendBuf + iLen, "%s", pTempBuf);
		    ////TRACE("End : Web_MakeGroupDeviceList : Info : %s\n", pTempBuf);

		    SAFELY_DELETE(pTempBuf);

		    



    //////////////////////////////////////////////////////////////////////////

        /* FILE *fp;
            if((fp = fopen("/var/f1.htm","wb")) !=  NULL)
            {
                fwrite(pszSendBuf,strlen(pszSendBuf), 1, fp);
                fclose(fp);
            }*/
    //////////////////////////////////////////////////////////////////////////
            

		    int iPipeLen = iLen /(PIPE_BUF - 2)  + 1;

		    ////TRACE("Successful :[%d][%d][%d][%s]\n",iLen, iPipeLen, strlen(pBuf), pBuf);
		    if(iPipeLen <= 1)
		    {
			    if((write(fd2, pszSendBuf, PIPE_BUF - 1)) < 0)
			    {
                    close(fd2);
				    return FALSE;
			    }
		    }
		    else
		    {

			    int m;
			    char szP[PIPE_BUF -1];
			    for(m = 0; m < iPipeLen; m++)
			    {
				    memset(szP, 0x0, PIPE_BUF -1);
				    strncpyz(szP, pszSendBuf + m * (PIPE_BUF - 2), PIPE_BUF - 1);
				    if((write(fd2, szP, PIPE_BUF - 1)) < 0)
				    {
					    //TRACE("Fault\n");
                        close(fd2);
					    return FALSE;
				    }

			    }

		    }    			
	    }
    }
	
	
	close(fd2);
	if(pszSendBuf != NULL)
	{
		DELETE(pszSendBuf);
		pszSendBuf = NULL;
	}
	return TRUE;
}


/*==========================================================================*
 * FUNCTION :  Web_MakeEquipAlarmInfoBuffer
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char **ppReturnBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:08
 *==========================================================================*/
static int Web_MakeEquipAlarmInfoBuffer(OUT char **ppReturnBuf)
{
	char		*szEngSendBuffer = NULL, *szLocSendBuffer = NULL;
	char        *szLoc2SendBuffer = NULL;//Add by wj for three languages 2006.4.29   

	ALARM_SIG_INFO	*pAlarmSigInfo = NULL;
	int			i, j;
	int			iError = 0;
	int			iEngLen = 0, iLocLen = 0;
    int         iLoc2Len = 0;//Add by wj for three languages 2006.4.29 
	int			iSignalNum = 0;
	int			iStdEquipNum = 0;
    char        *szsendBuffer = NULL;

	STDEQUIP_TYPE_INFO		*pStdEquipType = NULL;
	STDEQUIP_TYPE_INFO		*pTStdEquipType = NULL;
	szEngSendBuffer	= NEW(char, 2);
	szLocSendBuffer =NEW(char, 2);
    szLoc2SendBuffer =NEW(char, 2);//Add by wj for three languages 2006.4.29   
	
	if(szEngSendBuffer == NULL ||szLocSendBuffer == NULL ||szLoc2SendBuffer == NULL)
	{
        return FALSE;
	}
	
	szEngSendBuffer[iEngLen++] = ENG_EQUIPALARM_INFO_START;
	szLocSendBuffer[iLocLen++] = LOC_EQUIPALARM_INFO_START;
    szLoc2SendBuffer[iLoc2Len++] = LOC2_EQUIPALARM_INFO_START;//Add by wj for three languages 2006.4.29   


	iError = DxiGetData(VAR_STD_EQUIPS_NUM,
						0,			
						0,		
						&iBufLen,			
						(void *)&iStdEquipNum,			
						0);


	iError +=  DxiGetData(VAR_STD_EQUIPS_LIST,
						0,			
						0,		
						&iBufLen,			
						(void *)&pTStdEquipType,			
						0);
	/*Get Standard EquipList*/
	/*Local language device list*/
	////TRACE("iBufLen : %d\n", iBufLen/sizeof(STDEQUIP_TYPE_INFO));
	if(iError != ERR_DXI_OK)
	{
		return FALSE;
	}
	pStdEquipType = pTStdEquipType;

	ASSERT(pStdEquipType);
	for(i = 0; i < iStdEquipNum && pStdEquipType != NULL; i++, pStdEquipType++)
	{
		ASSERT(pStdEquipType);
		if((iSignalNum = pStdEquipType->iAlarmSigNum) > 0)
		{
			szEngSendBuffer = RENEW(char, szEngSendBuffer , 
									iEngLen + MAX_ENG_ALARM_BUFFER_LEN * iSignalNum);
			szLocSendBuffer = RENEW(char, szLocSendBuffer , 
									iLocLen  + MAX_LOC_ALARM_BUFFER_LEN * iSignalNum);
            szLoc2SendBuffer = RENEW(char, szLoc2SendBuffer , 
                                    iLoc2Len  + MAX_LOC2_ALARM_BUFFER_LEN * iSignalNum);
			
			////TRACE("-----%d--------iSignalNum : %d\n",i, iSignalNum);
			pAlarmSigInfo = pStdEquipType->pAlarmSigInfo;
			for(j = 0; j < iSignalNum && pAlarmSigInfo != NULL;j++, pAlarmSigInfo++)
			{
				ASSERT(pStdEquipType->pAlarmSigInfo);
				////TRACE("pStdEquipType->pAlarmSigInfo->pSigName->pFullName[0] : %s\n", pAlarmSigInfo->pSigName->pFullName[0]);
				/*Eng*/ 
				iEngLen += sprintf(szEngSendBuffer + iEngLen, "new OAlarm(\"%32s\", %d, %d, \"%s\", %d),\n", 
									pAlarmSigInfo->pSigName->pFullName[0],
									pAlarmSigInfo->iSigID,
									pAlarmSigInfo->iAlarmLevel,
									pStdEquipType->pTypeName->pFullName[0],
									pStdEquipType->bGroupType);
				/*Loc*/	
				iLocLen += sprintf(szLocSendBuffer + iLocLen, "new OAlarm(\"%s\", %d, %d, \"%s\", %d),\n", 
									pAlarmSigInfo->pSigName->pFullName[1],
									pAlarmSigInfo->iSigID,
									pAlarmSigInfo->iAlarmLevel,
									pStdEquipType->pTypeName->pFullName[1],
									pStdEquipType->bGroupType);
//////////////////////////////////////////////////////////////////////////
                //Add by wj for three languages 2006.4.28       
                /*Loc2*/
                #ifdef _SUPPORT_THREE_LANGUAGE
                iLoc2Len += sprintf(szLoc2SendBuffer + iLoc2Len, "new OAlarm(\"%s\", %d, %d, \"%s\", %d),\n", 
                                    pAlarmSigInfo->pSigName->pFullName[2],
                                    pAlarmSigInfo->iSigID,
                                    pAlarmSigInfo->iAlarmLevel,
                                    pStdEquipType->pTypeName->pFullName[2],
                                    pStdEquipType->bGroupType);
                #else
                iLoc2Len += sprintf(szLoc2SendBuffer + iLoc2Len, "  ");
                #endif
//end////////////////////////////////////////////////////////////////////////

			}
			
		}

	}

    /*FILE *pfile=fopen("/var/MakeEquipAlarmInfoBuffer.txt","w+");
    fwrite(szLoc2SendBuffer,(size_t)iLoc2Len,1,pfile);
    fclose(pfile);*/

	szEngSendBuffer[iEngLen - 2] =  32;
	szLocSendBuffer[iLocLen - 2] =	32;
    szLoc2SendBuffer[iLoc2Len - 2] = 32;//Add by wj for three languages 2006.4.28   
	////TRACE("********************iStdEquipNum : %d********************************************", iStdEquipNum);
	////TRACE("szLocSendBuffer %s\n", szLocSendBuffer);
	////TRACE("szEngSendBuffer : %s\n", szEngSendBuffer);
	szsendBuffer = NEW(char, (size_t)(iEngLen + iLocLen + iLoc2Len + 4));//Add by wj for three languages 2006.4.28   
    
    memmove(szsendBuffer,szLocSendBuffer,(size_t)(iLocLen+1));//Add by wj for three languages 2006.4.28
    memmove(szsendBuffer+iLocLen,szEngSendBuffer,(size_t)(iEngLen+1));//Add by wj for three languages 2006.4.28
    memmove(szsendBuffer+iLocLen+iEngLen,szLoc2SendBuffer,(size_t)(iLoc2Len+1));//Add by wj for three languages 2006.4.28

    szsendBuffer[iLocLen + iEngLen + iLoc2Len + 3]='\0';//Add by wj for three languages 2006.4.28

	if(szEngSendBuffer != NULL)
	{
		DELETE(szEngSendBuffer);
		szEngSendBuffer = NULL;
	}

    if(szLoc2SendBuffer != NULL)//Add by wj for three languages 2006.4.28   
    {
        DELETE(szLoc2SendBuffer);
        szLoc2SendBuffer = NULL;
    }

    if(szLocSendBuffer != NULL)//Add by wj for three languages 2006.4.28   
    {
        DELETE(szLocSendBuffer);
        szLocSendBuffer = NULL;
    }
	////TRACE("Unit : szLocSendBuffer : %s\n", szLocSendBuffer);

	*ppReturnBuf = szsendBuffer;

    
    /*FILE *pfile2=fopen("/var/MakeEquipAlarmInfoBuffer2.txt","w+");
    fwrite(szsendBuffer,strlen(szsendBuffer),1,pfile2);
    fclose(pfile2);*/

	return (iEngLen + iLocLen + iLoc2Len + 4);//Add by wj for three languages 2006.4.28   
	
}


/*==========================================================================*
 * FUNCTION :  Web_MakeGroupDeviceList
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char **ppReturnBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:08
 *==========================================================================*/
static int Web_MakeGroupDeviceList(OUT char **ppReturnBuf)
{

	char		*szEngGroupDeviceBuffer = NULL, *szLocGroupDeviceBuffer = NULL;
    char        *szLoc2GroupDeviceBuffer = NULL;//Added by wj for three languages 2006.4.29
	int			i;
	int			iEngLen = 0, iLocLen = 0;
    int         iLoc2Len = 0;//Added by wj for three languages 2006.4.29
	int			iStdEquipNum = 0;	
	STDEQUIP_TYPE_INFO		*pStdEquipType = NULL;
	int			iError = 0;
    char        *szsendBuffer=NULL;

	

	/*Get Standard EquipNum*/

	iError = DxiGetData(VAR_STD_EQUIPS_NUM,
						0,			
						0,		
						&iBufLen,			
						(void *)&iStdEquipNum,			
						0);
	if(iError != ERR_DXI_OK)
	{
		return FALSE;
	}
	szLocGroupDeviceBuffer = NEW(char, (size_t) iStdEquipNum * MAX_LOC_GROUP_LEN );//Added by wj for three languages 2006.4.29
	szEngGroupDeviceBuffer = NEW(char, (size_t) iStdEquipNum * MAX_ENG_GROUP_LEN);//Added by wj for three languages 2006.4.29
    szLoc2GroupDeviceBuffer = NEW(char, (size_t) iStdEquipNum * MAX_LOC2_GROUP_LEN);//Added by wj for three languages 2006.4.29

	if(szEngGroupDeviceBuffer == NULL || szLocGroupDeviceBuffer == NULL || szLoc2GroupDeviceBuffer == NULL)
	{
		return FALSE;
	}

	memset(szLocGroupDeviceBuffer, 0x0, (size_t) iStdEquipNum * MAX_LOC_GROUP_LEN );//Added by wj for three languages 2006.4.29
	memset(szEngGroupDeviceBuffer, 0x0, (size_t) iStdEquipNum * MAX_ENG_GROUP_LEN);
    memset(szLoc2GroupDeviceBuffer, 0x0, (size_t) iStdEquipNum * MAX_LOC2_GROUP_LEN);//Added by wj for three languages 2006.4.29

	
	szLocGroupDeviceBuffer[iLocLen++] = LOC_GROUP_DEVICE_START;
	szEngGroupDeviceBuffer[iEngLen++] = ENG_GROUP_DEVICE_START;
    szLoc2GroupDeviceBuffer[iLoc2Len++] = LOC2_GROUP_DEVICE_START;//Added by wj for three languages 2006.4.29

	/*Get Standard EquipList*/
	iError +=  DxiGetData(VAR_STD_EQUIPS_LIST,
						0,			
						0,		
						&iBufLen,			
						(void *)&pStdEquipType,			
						0);

    if(iError != ERR_DXI_OK)
    {
        SAFELY_DELETE(szLocGroupDeviceBuffer);
        SAFELY_DELETE(szEngGroupDeviceBuffer);
        SAFELY_DELETE(szLoc2GroupDeviceBuffer);
        return FALSE;
    }
	
	for(i = 0; i < iStdEquipNum && pStdEquipType != NULL; i++, pStdEquipType++)
	{
		iEngLen += sprintf(szEngGroupDeviceBuffer + iEngLen, "new OGroup(\"%s\",%d,%d),\n",
						pStdEquipType->pTypeName->pFullName[0],
						pStdEquipType->iTypeID,
						pStdEquipType->iTypeID);
		
		iLocLen += sprintf(szLocGroupDeviceBuffer + iLocLen, "new OGroup(\"%s\",%d,%d),\n",
						pStdEquipType->pTypeName->pFullName[1],
						pStdEquipType->iTypeID,
						pStdEquipType->iTypeID);
//////////////////////////////////////////////////////////////////////////
        //Added by wj for three languages 2006.4.29
        #ifdef _SUPPORT_THREE_LANGUAGE
        iLoc2Len += sprintf(szLoc2GroupDeviceBuffer + iLoc2Len, "new OGroup(\"%s\",%d,%d),\n",
                        pStdEquipType->pTypeName->pFullName[2],
                        pStdEquipType->iTypeID,
                        pStdEquipType->iTypeID);
        #else
        iLoc2Len += sprintf(szLoc2GroupDeviceBuffer + iLoc2Len, "  ");
        #endif
//////////////////////////////////////////////////////////////////////////


	}

    /*FILE *pfile=fopen("/var/MakeGroupDeviceList.txt","w+");
    fwrite(szLoc2GroupDeviceBuffer,(size_t)iLoc2Len,1,pfile);
    fclose(pfile);*/

	if(iLocLen > 1 && iEngLen > 1 && iLoc2Len > 1)//Added by wj for three languages 2006.4.29
	{
		szLocGroupDeviceBuffer[iLocLen - 2] = 32;
		szEngGroupDeviceBuffer[iEngLen - 2] = 32;
        szLoc2GroupDeviceBuffer[iLoc2Len - 2] = 32;//Added by wj for three languages 2006.4.29
	}

    szsendBuffer=NEW(char,(size_t)(iLocLen+iEngLen+iLoc2Len+4));

    memmove(szsendBuffer,szLocGroupDeviceBuffer,(size_t)(iLocLen+1));//Added by wj for three languages 2006.4.29
	memmove(szsendBuffer+iLocLen,szEngGroupDeviceBuffer,(size_t)(iEngLen+1));//Added by wj for three languages 2006.4.29
    memmove(szsendBuffer+iLocLen+iEngLen,szLoc2GroupDeviceBuffer,(size_t)(iLoc2Len+1));//Added by wj for three languages 2006.4.29
    szsendBuffer[iLocLen+iEngLen+iLoc2Len+3]='\0';

   
	if(szEngGroupDeviceBuffer != NULL)
	{
		DELETE(szEngGroupDeviceBuffer);
		szEngGroupDeviceBuffer = NULL;
	}
//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.4.29
    if(szLoc2GroupDeviceBuffer != NULL)
    {
        DELETE(szLoc2GroupDeviceBuffer);
        szLoc2GroupDeviceBuffer = NULL;
    }

    if(szLocGroupDeviceBuffer != NULL)
    {
        DELETE(szLocGroupDeviceBuffer);
        szLocGroupDeviceBuffer = NULL;
    }

//////////////////////////////////////////////////////////////////////////

	*ppReturnBuf = szsendBuffer;

    /*FILE *pfile2=fopen("/var/MakeGroupDeviceList2.txt","w+");
    fwrite(szsendBuffer,strlen(szsendBuffer),1,pfile2);
    fclose(pfile2);*/

	return (iEngLen + iLocLen + iLoc2Len + 4);//Added by wj for three languages 2006.4.29
}

/*==========================================================================*
 * FUNCTION :  Web_SendRealtimeData
 * PURPOSE  :  return the signal data to the CGI
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *buf:
				IN char *filename:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 15:08
 *==========================================================================*/
static int Web_SendRealtimeData(IN char *buf,IN char *filename)
{
	char	*ptr = NULL, *pBuf = NULL, *pTempBuf = NULL;
	int		fd2;            //handle of fifo
	char	szTemp[64];		//temp buffer
	int		iEquipID;
	int		iLen = 0;
	int		iNeed, iLanguage;
	int		iEIBAllBattNum = 0;


	//open the fifo
	if((fd2 = open(filename,O_WRONLY )) < 0)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open FIFO");
		return FALSE;
	}

	/*offset*/
	ptr = buf + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;

	/*getEquipId*/
	strncpyz(szTemp,ptr,MAX_EQUIPID_LEN + 1);
	iEquipID =atoi(szTemp);
 	ptr = ptr + MAX_EQUIPID_LEN;

	/*whether need to return signal structure*/
	strncpyz(szTemp,ptr,MAX_NEED_TO_RETURN_STRUCTURE + 1);
	iNeed = atoi(szTemp);
	////TRACE("iNeed:%d\n",iNeed);
	ptr = ptr + MAX_NEED_TO_RETURN_STRUCTURE;

	/*get language type*/
	strncpyz(szTemp,ptr,MAX_LANGUAGE_TYPE_LEN + 1);

	if(atoi(szTemp) > 0)
	{
        if(atoi(szTemp)==1)
        {
            iLanguage = LOCAL_LANGUAGE_NAME;
        }
        else
        {
            iLanguage = LOCAL2_LANGUAGE_NAME;
        }
		
	}
	else
	{
		iLanguage = ENGLISH_LANGUAGE_NAME;
	}
	
	/*Allocate memory*/
	pBuf = NEW(char,20 + MAX_BUF_LEN * 2);
	if(pBuf == NULL)
	{
        close(fd2);
		return FALSE;
	}
	
	
	/*get signalData according to the equipId*/
	 
	/*pBuf = NEW(char,MAX_SIGNAL_TOTAL_NUM_LEN)*/
	/*Make signal len buffer return to CGI */
	if (Web_GetSignalNum(iEquipID, &pTempBuf) > 0)
	{
		////TRACE("Web_GetSignalNum:%s\n",pTempBuf);
		iLen += sprintf(pBuf + iLen,"%s",pTempBuf);
	}
#ifdef _DEBUG
	else
	{
		iLen += sprintf(pBuf + iLen,"%4d",0);
		iLen += sprintf(pBuf + iLen,"%4d",0);
		iLen += sprintf(pBuf + iLen,"%4d",0);
		iLen += sprintf(pBuf + iLen,"%4d",0);
		iLen += sprintf(pBuf + iLen,"%4d",0);
	}
#endif
	//iLen += sprintf(pBuf + iLen, "%-4c", END_OF_SIGNUALNUM);

	/*make data to send to the CGI*/
	/*if need return data structure*/
	
	if(iNeed == NEED_RETURN_SIGNAL_STRUCTURE)
	{
		/*Construct sample signal struc*/
		////TRACE("start:Web_MakeSamSigStruBufofEquip\n");
		 
		if(( Web_MakeSamSigStruBufofEquip(iLanguage, iEquipID,&pTempBuf)) > 0)
		{
			iLen += sprintf(pBuf + iLen,"%s",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		pBuf[iLen] = END_OF_SAMPLESIG;
		iLen += 1;
		
		////TRACE("start:Web_MakeConSigStruBufofEquip\n");
		/*Construct control signal struc*/	
		if((Web_MakeConSigStruBufofEquip(iLanguage, iEquipID, &pTempBuf)) > 0)
		{
			iLen  += sprintf(pBuf + iLen,"%s",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		pBuf[iLen] = END_OF_CONTROLSIG;
		iLen += 1;

		////TRACE("start:Web_MakeSetSigStruBufofEquip\n");
		/*Construct setting signal struc*/	
		if((Web_MakeSetSigStruBufofEquip(iLanguage, iEquipID,&pTempBuf)) > 0)
		{
						
            iLen  += sprintf(pBuf + iLen,"%s",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
			

		}
		pBuf[iLen] = END_OF_SETTINGSIG;
		iLen += 1;

	 
		////TRACE("start:Web_MakeAlarmSigStruBufofEquip\n");
		/*Construct alarm signal struc*/	
		if((Web_MakeAlarmSigStruBufofEquip(iLanguage,iEquipID,&pTempBuf)) > 0)
		{
			iLen  += sprintf(pBuf + iLen," %s",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}

		
		pBuf[iLen] = END_OF_ALARMSIG;
		iLen += 1;
	 
		////TRACE("start:Web_MakeStatusSigStruBufofEquip\n");
		/*Construct status bar signal struc*/
		if((Web_MakeStatusSigStruBufofEquip(iLanguage, &pTempBuf)) > 0)
		{
			iLen += sprintf(pBuf + iLen, "%s", pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		pBuf[iLen] = END_OF_STATUSSIG;
		iLen += 1;

		////TRACE("start:Web_MakeRAlarmSigStruBufofEquip\n");
		if(Web_MakeRAlarmSigStruBufofEquip(&pTempBuf) > 0)
		{
			iLen += sprintf(pBuf + iLen, "%s", pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		
		pBuf[iLen] = END_OF_RALARM;
		iLen += 1;

		
		//char		szAlarmTime[32] = {0};
		//sprintf(szAlarmTime, "%ld", g_AlarmCurrentTime);
		
		iLen += sprintf(pBuf + iLen, "%ld%c", g_AlarmCurrentTime, 59);

        Web_MakeRectifierRefresh(FALSE);
		iLen += sprintf(pBuf + iLen, "%d%c", iRectifierNumber, 59);


		iEIBAllBattNum = iEIB1BattNum + iEIB2BattNum + iEIB3BattNum + iEIB4BattNum;
        iLen += sprintf(pBuf + iLen, "%d%c", iEIBAllBattNum, 59);
        iLen += sprintf(pBuf + iLen, "%d%c", iDCDNumber, 59);

		iLen += sprintf(pBuf + iLen, "%d%c", iAutoCFGStartFlage, 59);
		if(iAutoCFGStartFlage == 2)
		{
			iAutoCFGStartFlage = 3;
		}
		
		iLen += sprintf(pBuf + iLen, "%d%c", iSlaveMode, 59);
		iLen += sprintf(pBuf + iLen, "%d%c", iSMDUCFGStatus, 59);
		iLen += sprintf(pBuf + iLen, "%d%c", iConverterNumber, 59);//if iConverterNumber changed,we should refresh the tree_view
        iLen += sprintf(pBuf + iLen, "%d%c", iBATTNumber, 59);
		//printf("\n_______1iBATTNumber:%d_________\n",iBATTNumber);
	}
	else
	{
		/*Construct sample signal data*/
		if((Web_MakeSampleDataBuf(iLanguage, iEquipID,&pTempBuf)) > 0 )
		{
			iLen  += sprintf(pBuf + iLen,"%s",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		pBuf[iLen] = END_OF_SAMPLESIG;
		iLen += 1;

		/*Construct control signal data*/
		if((Web_MakeControlDataBuf(iLanguage, iEquipID,&pTempBuf)) > 0)
		{
			iLen  += sprintf(pBuf + iLen," %s",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		
		pBuf[iLen] = END_OF_CONTROLSIG;
		iLen += 1;

		/*Construct setting signal data*/
		if((Web_MakeSettingDataBuf(iLanguage, iEquipID,&pTempBuf)) > 0)
		{
			iLen  += sprintf(pBuf + iLen,"%s",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		pBuf[iLen] = END_OF_SETTINGSIG;
		iLen += 1;

		/*Construct realtime alarm signal data*/
		if((Web_MakeRAlarmDataBuf(iLanguage, &pTempBuf) ) > 0)
		{
			iLen  += sprintf(pBuf + iLen,"%s",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		//iLen += sprintf(pBuf + iLen, "%-4c", END_OF_RALARMSIG);
		pBuf[iLen] = END_OF_RALARMSIG;
		iLen += 1;

		/*Construct status bar signal data*/
		if((Web_GetStatusData(&pTempBuf)) > 0)
		{
			iLen  += sprintf(pBuf + iLen," %s ",pTempBuf);
			DELETE(pTempBuf);
			pTempBuf = NULL;
		}
		pBuf[iLen] = END_OF_RALARMSIG;
		iLen += 1;

		iLen += sprintf(pBuf + iLen, "%ld%c", g_AlarmCurrentTime, 59);

		//refresh rectifier
		Web_MakeRectifierRefresh(FALSE);
		////TRACE("iNeedRefreshRectifier : [%d]\n", iNeedRefreshRectifier);
		
		iLen += sprintf(pBuf + iLen, "%d%c", iRectifierNumber, 59);//if iRectifierNumber changed,we should refresh the tree_view

		iEIBAllBattNum = iEIB1BattNum + iEIB2BattNum + iEIB3BattNum + iEIB4BattNum;
		iLen += sprintf(pBuf + iLen, "%d%c", iEIBAllBattNum, 59);
        iLen += sprintf(pBuf + iLen, "%d%c", iDCDNumber, 59);

		iLen += sprintf(pBuf + iLen, "%d%c", iAutoCFGStartFlage, 59);
		if(iAutoCFGStartFlage == 2)
		{
			iAutoCFGStartFlage = 3;
		}
		iLen += sprintf(pBuf + iLen, "%d%c", iSlaveMode, 59);
		iLen += sprintf(pBuf + iLen, "%d%c", iSMDUCFGStatus, 59);
		iLen += sprintf(pBuf + iLen, "%d%c", iConverterNumber, 59);//if iConverterNumber changed,we should refresh the tree_view
        iLen += sprintf(pBuf + iLen, "%d%c", iBATTNumber, 59);
		//TRACE("\n1----------iRectifierNumber[%d]\n", iRectifierNumber);
		//printf("\n_______2iBATTNumber:%d_________\n",iBATTNumber);
		
	}
	
	int iPipeLen = iLen /(PIPE_BUF - 2)  + 1;

	//TRACE("Successful :[%d][%d][%d][%s]\n",iLen, iPipeLen, strlen(pBuf), pBuf);
	RunThread_Heartbeat(RunThread_GetId(NULL));
	if(iPipeLen <= 1)
	{
		if((write(fd2, pBuf, PIPE_BUF - 1)) < 0)
		{
            close(fd2);
			return FALSE;
		}
	}
	else
	{

		int m;
		char szP[PIPE_BUF -1];
		for(m = 0; m < iPipeLen; m++)
		{
			memset(szP, 0x0, PIPE_BUF -1);
			strncpyz(szP, pBuf + m * (PIPE_BUF - 2), PIPE_BUF - 1);
			if((write(fd2, szP, PIPE_BUF - 1)) < 0)
			{
				//TRACE("Fault\n");
                close(fd2);
				return FALSE;
			}

		}

	}

        RunThread_Heartbeat(RunThread_GetId(NULL));
	if(pBuf != NULL)
	{
		DELETE(pBuf);
		pBuf = NULL;
	}
	close(fd2);
	return TRUE;
}

//for three language
static int Web_MakeRectifierRefresh(IN BOOL bUrgent)
{
    //TRACE("into Web_MakeRectifierRefresh");
#define CGI_ENG_DEVICE_PATH				"/app/www/html/cgi-bin/eng/j02_tree_view.js"
//#define CGI_ENG_DEVICE_PATH_T			"/scup/www/html/cgi-bin/eng/j02_tree_view_te.js"//"/scup/www/html/cgi-bin/eng/j02_tree_view_t.js"
#define CGI_ENG_DEVICE_PATH_T			"/var/eng/j02_tree_view_t.js"//"/scup/www/html/cgi-bin/eng/j02_tree_view_t.js"
#define CGI_LOC_DEVICE_PATH				"/app/www/html/cgi-bin/loc/j02_tree_view.js"
//#define CGI_LOC_DEVICE_PATH_T			"/scup/www/html/cgi-bin/loc/j02_tree_view_tl.js"//"/scup/www/html/cgi-bin/loc/j02_tree_view_t.js"
#define CGI_LOC_DEVICE_PATH_T			"/var/loc/j02_tree_view_t.js"//"/scup/www/html/cgi-bin/loc/j02_tree_view_t.js"

//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.5.8    
#define CGI_LOC2_DEVICE_PATH			"/app/www/html/cgi-bin/loc2/j02_tree_view.js"
#define CGI_LOC2_DEVICE_PATH_T			"/var/loc2/j02_tree_view_t.js"//"/scup/www/html/cgi-bin/loc/j02_tree_view_t.js"
//end//////////////////////////////////////////////////////////////////////////

#define CGI_ENG_EQUIPLIST_PATH_T			"/var/eng/j77_equiplist_def.htm"
#define CGI_LOC_EQUIPLIST_PATH_T			"/var/loc/j77_equiplist_def.htm"
#define CGI_ENG_EQUIPLIST_PATH_B			"/var/eng/j77_equiplist_def_back.htm"
#define CGI_LOC_EQUIPLIST_PATH_B			"/var/loc/j77_equiplist_def_back.htm"


#define CGI_TREE_NODE					"tree_node"
#define USER_DEF_PAGES_NODE          	"user_def_pages_node"

	char		*szBuffer = NULL, *szReadBuffer = NULL;
	int			iLen = 0;
	char		*pSearchValue1 = NULL, *pSearchValue2 = NULL;
	int			iPosition = 0;
	FILE		*fp;
	char		*pHtml =  NULL;
	char		szCommandStr[128];
	int			iGetRectNumber = Web_GetRectifierNumber();
    int         iGetACDNumber = 0;//Web_GetACDNumber();
    int         iGetDCDNumber = 0;//Web_GetDCDNumber();
    int         iGetBATTNumber = Web_GetBATTNumber();
	int			iGetEIB1BattNum = Web_GetEIBBattNumber(197);
	int			iGetEIB2BattNum = Web_GetEIBBattNumber(198);
	int			iGetEIB3BattNum = Web_GetEIBBattNumber(199);
	int			iGetEIB4BattNum = Web_GetEIBBattNumber(200);
	int			iGetBatLCNum = Web_GetCSUNum(0);
	int			iGetDCLCNum = Web_GetCSUNum(1);
	int			iGetSMDUCFGStatus	= Web_GetSMDUCFGStatus();
	int			iGetConvNumber = Web_GetConverterNumber();
	static int			iRefreshTimes = FALSE;
	static int			iTempFlage = 0;



       if(iRectifierNumber != (unsigned int)iGetRectNumber/* || 
                    iACDNumber != (unsigned int)iGetACDNumber ||
                    iDCDNumber != (unsigned int)iGetDCDNumber*/ ||
                    iBATTNumber != (unsigned int)iGetBATTNumber ||
					iEIB1BattNum != (unsigned int)iGetEIB1BattNum ||
					iEIB2BattNum != (unsigned int)iGetEIB2BattNum ||
					iEIB3BattNum != (unsigned int)iGetEIB3BattNum ||
					iEIB4BattNum != (unsigned int)iGetEIB4BattNum ||
					iSMDUCFGStatus != (unsigned int)iGetSMDUCFGStatus ||
					iConverterNumber != (unsigned int)iGetConvNumber || iRefreshTimes == TRUE ||
					iGetBatLCNum != iBatLCNum || iGetDCLCNum != iDCLCNum || bUrgent)
	{
		if(iTempFlage < 5)
		{
			iRefreshTimes = TRUE;
			iTempFlage++;

		}
		else
		{
			iTempFlage=0;
			iRefreshTimes = FALSE;

		}
		//Make equiplist
		//jump 2 ";"
		sprintf(szCommandStr, "chmod 777 %s", CGI_ENG_DEVICE_PATH_T);
		system(szCommandStr);
		sprintf(szCommandStr, "chmod 777 %s", CGI_LOC_DEVICE_PATH_T);
		system(szCommandStr);

		//if(iSMDUCFGStatus == 1)
		//{
		//	sleep(3);
		//}
 
		if((iLen = Web_MakeEquipListBuffer(&szBuffer)) > 0 )
		{
			char stRectNum[4];
			int  iRectIncludSlave = Web_GetRectifierIncludSlave();
			//sprintf(stRectNum,"%d",iRectifierNumber);
			sprintf(stRectNum,"%d",iRectIncludSlave);

			//write file
			pSearchValue1 = strchr(szBuffer,59);//pSearchValue1 point to the start of eng equiplist
			if(pSearchValue1 != NULL)
			{
				pSearchValue1= pSearchValue1 + 1;
				pSearchValue2 = strchr(pSearchValue1 , 59);//pSearchValue2 point to the start of loc2 equiplist
				if(pSearchValue2 != NULL)
				{
					pSearchValue2  = pSearchValue2 + 1;
					if((pSearchValue1 = strchr(pSearchValue2, 59)) != NULL)//pSearchValue1 point to the start of loc treenode
					{ 
						pSearchValue1=pSearchValue1+1;//Add by wj for three languages 2006.5.8
						pSearchValue2 = strchr(pSearchValue1 , 59);//pSearchValue2 point to the start of eng treenode//Add by wj for three languages 2006.5.8
						if(pSearchValue2 != NULL)//Add by wj for three languages 2006.5.8
						{
						//Local
							iPosition = pSearchValue2 - pSearchValue1;
							TRACE("iPosition=%d",iPosition);
							szReadBuffer = NEW(char, iPosition + 1);
							ASSERT(szReadBuffer);
							if(szReadBuffer != NULL)
							{
								strncpyz(szReadBuffer, pSearchValue1, iPosition + 1);

								if(LoadHtmlFile(CGI_LOC_DEVICE_PATH, &pHtml ) > 0 )
								{
									//ReplaceString( &pHtml, MAKE_VAR_FIELD("TREE_RECT_NUM"), stRectNum);
									ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_TREE_NODE), szReadBuffer);

									if((fp = fopen(CGI_LOC_DEVICE_PATH_T,"wb")) !=  NULL)
									{
										fwrite(pHtml,strlen(pHtml), 1, fp);
										DELETE(pHtml);
										pHtml = NULL;
										fclose(fp);
										//printf("Web_MakeRectifierRefresh LOC OK!!!");
									}
								}

								if(LoadHtmlFile(CGI_LOC_DEVICE_PATH_T, &pHtml ) > 0 )
								{
									ReplaceString( &pHtml, MAKE_VAR_FIELD("TREE_RECT_NUM"), stRectNum);


									if((fp = fopen(CGI_LOC_DEVICE_PATH_T,"wb")) !=  NULL)
									{
										fwrite(pHtml,strlen(pHtml), 1, fp);
										DELETE(pHtml);
										pHtml = NULL;
										fclose(fp);
										//printf("Web_MakeRectifierRefresh LOC3 OK!!!");
									}
								}

								

								
								DELETE(szReadBuffer);
								szReadBuffer = NULL;
							}

							pSearchValue2  = pSearchValue2 + 1;//Add by wj for three languages 2006.5.8

							//////////////////////////////////////////////////////////////////////////
							//Add by wj for three languages 2006.5.8
							//Eng
							if((pSearchValue1 = strchr(pSearchValue2, 59)) != NULL)//pSearchValue1 point to the start of loc2 treenode
							{
								iPosition = pSearchValue1 - pSearchValue2;
								szReadBuffer = NEW(char, iPosition + 1);
								ASSERT(szReadBuffer);

								if(szReadBuffer != NULL)
								{
									strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);

									if(LoadHtmlFile(CGI_ENG_DEVICE_PATH, &pHtml ) > 0 )
									{
										ReplaceString( &pHtml, MAKE_VAR_FIELD("TREE_RECT_NUM"), stRectNum);
										ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_TREE_NODE), szReadBuffer);

										if((fp = fopen(CGI_ENG_DEVICE_PATH_T,"wb")) !=  NULL)
										{
											fwrite(pHtml,strlen(pHtml), 1, fp);
											DELETE(pHtml);
											pHtml = NULL;
											fclose(fp);
											//printf("Web_MakeRectifierRefresh ENG OK!!!");
										}
									}

									if(LoadHtmlFile(CGI_ENG_DEVICE_PATH_T, &pHtml ) > 0 )
									{
										ReplaceString( &pHtml, MAKE_VAR_FIELD("TREE_RECT_NUM"), stRectNum);


										if((fp = fopen(CGI_ENG_DEVICE_PATH_T,"wb")) !=  NULL)
										{
											fwrite(pHtml,strlen(pHtml), 1, fp);
											DELETE(pHtml);
											pHtml = NULL;
											fclose(fp);
											//printf("Web_MakeRectifierRefresh ENG3 OK!!!");
										}
									}
									DELETE(szReadBuffer);
									szReadBuffer = NULL;
								}
                               // pSearchValue2 = pSearchValue2 + iPosition + 1;
							}

							pSearchValue2 = pSearchValue2 + iPosition + 1;
							if((pSearchValue1 = strchr(pSearchValue2, 59)) != NULL)//pSearchValue1 point to the start of loc2 treenode
							{
								iPosition = pSearchValue1 - pSearchValue2;
							}

							pSearchValue2 = pSearchValue2 + iPosition + 1;
							if((pSearchValue1 = strchr(pSearchValue2, 59)) != NULL)//pSearchValue1 point to the start of loc user_def_page node
							{
								iPosition = pSearchValue1 - pSearchValue2;
								szReadBuffer = NEW(char, iPosition + 1);
								ASSERT(szReadBuffer);

								if(szReadBuffer != NULL)
								{
									strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);

									if(LoadHtmlFile(CGI_LOC_DEVICE_PATH_T, &pHtml ) > 0 )
									{
										ReplaceString( &pHtml, MAKE_VAR_FIELD(USER_DEF_PAGES_NODE), szReadBuffer);

										if((fp = fopen(CGI_LOC_DEVICE_PATH_T,"wb")) !=  NULL)
										{
											fwrite(pHtml,strlen(pHtml), 1, fp);
											DELETE(pHtml);
											pHtml = NULL;
											fclose(fp);
											//printf("Web_MakeRectifierRefresh LOC2 OK!!!");
										}
									}	
									DELETE(szReadBuffer);
									szReadBuffer = NULL;
								}
								// pSearchValue2 = pSearchValue2 + iPosition + 1;
							}

								
							pSearchValue2 = pSearchValue2 + iPosition + 1;

							if((pSearchValue1 = strchr(pSearchValue2, 59)) != NULL)//pSearchValue1 point to the start of loc user_def_page node
							{
								iPosition = pSearchValue1 - pSearchValue2;
								szReadBuffer = NEW(char, iPosition + 1);
								ASSERT(szReadBuffer);

								if(szReadBuffer != NULL)
								{
									strncpyz(szReadBuffer, pSearchValue2, iPosition + 1);

									if(LoadHtmlFile(CGI_ENG_DEVICE_PATH_T, &pHtml ) > 0 )
									{
										ReplaceString( &pHtml, MAKE_VAR_FIELD(USER_DEF_PAGES_NODE), szReadBuffer);

										if((fp = fopen(CGI_ENG_DEVICE_PATH_T,"wb")) !=  NULL)
										{
											fwrite(pHtml,strlen(pHtml), 1, fp);
											DELETE(pHtml);
											pHtml = NULL;
											fclose(fp);
											//printf("Web_MakeRectifierRefresh ENG2 OK!!!");
										}
									}	
									DELETE(szReadBuffer);
									szReadBuffer = NULL;
								}
							}

						

							DELETE(szBuffer);
							szBuffer = NULL;

							//end////////////////////////////////////////////////////////////////////////

						}//Add by wj for three languages 2006.5.8
						
						
                        
					}
				}
				
			
				
			}

			
//Added the equiplist,2011-11-29
			EQUIP_INFO		*pEquipInfo = NULL;
			int			iEquipNum;
			char			*pMakeEquipListBuf = NULL;
			int			i;
			char			szExchangeFile[64];
			


			if(Web_GetEquipListFromInterface(&pEquipInfo) <= 0)
			{
				return FALSE;
			}

			int iError = DxiGetData(VAR_ACU_EQUIPS_NUM,
				0,			
				iVarSubID,		
				&iBufLen,			
				(void *)&iEquipNum,			
				iTimeOut);

			pMakeEquipListBuf = NEW(char, 100 * iEquipNum);
			if(!pMakeEquipListBuf)
			{
				return;
			}
			memset(pMakeEquipListBuf, 0x0, 100 * iEquipNum * sizeof(char));

			int iRectifierNumber = 100 + 1;//Including Group
			int iConverterNumber = 48 + 1;//Including Group
			iLen = 0;
			for(i = 0; i < iEquipNum; i++)
			{
				if(i == iEquipNum - 1)
				{
					iLen += sprintf(pMakeEquipListBuf + iLen,"new ODevice(\"%s\",%4d,%d)\n",
						pEquipInfo[i].pEquipName->pFullName[1],
						pEquipInfo[i].iEquipID,
						pEquipInfo[i].iEquipTypeID);
				}
				else if(pEquipInfo[i].iEquipTypeID ==  200 || pEquipInfo[i].iEquipTypeID ==  201 )//rect
				{
					
					if(iRectifierNumber > 0)
					{
						iLen += sprintf(pMakeEquipListBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
							pEquipInfo[i].pEquipName->pFullName[1],
							pEquipInfo[i].iEquipID,
							pEquipInfo[i].iEquipTypeID);
						iRectifierNumber--;
					}
				}
				else if(pEquipInfo[i].iEquipTypeID == 1100 || pEquipInfo[i].iEquipTypeID == 1101)//Converter
				{
					if(iConverterNumber > 0)
					{
						iLen += sprintf(pMakeEquipListBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
							pEquipInfo[i].pEquipName->pFullName[1],
							pEquipInfo[i].iEquipID,
							pEquipInfo[i].iEquipTypeID);
						iConverterNumber--;
					}

				}
				else
				{

					iLen += sprintf(pMakeEquipListBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
						pEquipInfo[i].pEquipName->pFullName[1],
						pEquipInfo[i].iEquipID,
						pEquipInfo[i].iEquipTypeID);

				}
			}
#define WEB_HTML_EQUIP_LIST					"equip_list"	
			if(LoadHtmlFile(CGI_LOC_EQUIPLIST_PATH_B, &pHtml ) > 0 )
			{
	
				ReplaceString( &pHtml, MAKE_VAR_FIELD(WEB_HTML_EQUIP_LIST), pMakeEquipListBuf);

				if((fp = fopen(CGI_LOC_EQUIPLIST_PATH_T,"wb")) !=  NULL)
				{
					fwrite(pHtml,strlen(pHtml), 1, fp);
					DELETE(pHtml);
					pHtml = NULL;
					fclose(fp);
					
				}
			}	


			iLen = 0;
			memset(pMakeEquipListBuf, 0x0, 100 * iEquipNum * sizeof(char));
			iRectifierNumber = 100 + 1;//Including Group
			iConverterNumber = 48 + 1;//Including Group
			
			for(i = 0; i < iEquipNum; i++)
			{
				if(i == iEquipNum - 1)
				{
					iLen += sprintf(pMakeEquipListBuf + iLen,"new ODevice(\"%s\",%4d,%d)\n",
						pEquipInfo[i].pEquipName->pFullName[0],
						pEquipInfo[i].iEquipID,
						pEquipInfo[i].iEquipTypeID);
				}
				else  if(pEquipInfo[i].iEquipTypeID ==  200 || pEquipInfo[i].iEquipTypeID ==  201 )//rect
				{

					if(iRectifierNumber > 0)
					{
						iLen += sprintf(pMakeEquipListBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
							pEquipInfo[i].pEquipName->pFullName[0],
							pEquipInfo[i].iEquipID,
							pEquipInfo[i].iEquipTypeID);
						iRectifierNumber--;
					}
				}
				else if(pEquipInfo[i].iEquipTypeID == 1100 || pEquipInfo[i].iEquipTypeID == 1101)//Converter
				{
					if(iConverterNumber > 0)
					{
						iLen += sprintf(pMakeEquipListBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
							pEquipInfo[i].pEquipName->pFullName[0],
							pEquipInfo[i].iEquipID,
							pEquipInfo[i].iEquipTypeID);
						iConverterNumber--;
					}

				}
				else
				{

					iLen += sprintf(pMakeEquipListBuf + iLen,"new ODevice(\"%s\",%4d,%d) ,\n",
						pEquipInfo[i].pEquipName->pFullName[0],
						pEquipInfo[i].iEquipID,
						pEquipInfo[i].iEquipTypeID);
				}
			}


			if(LoadHtmlFile(CGI_ENG_EQUIPLIST_PATH_B, &pHtml ) > 0 )
			{

				ReplaceString( &pHtml, MAKE_VAR_FIELD(WEB_HTML_EQUIP_LIST), pMakeEquipListBuf);

				if((fp = fopen(CGI_ENG_EQUIPLIST_PATH_T,"wb")) !=  NULL)
				{
					fwrite(pHtml,strlen(pHtml), 1, fp);
					DELETE(pHtml);
					pHtml = NULL;
					fclose(fp);
					
				}
			}	

			DELETE(pMakeEquipListBuf);
			pMakeEquipListBuf = NULL;
//End, 2011-11-29
			return iRectifierNumber;
		}
		else
		{
			return iRectifierNumber;//return 0;
		}
	}
	else
	{

		return iRectifierNumber;//return 0;
	}
}


static unsigned int Web_GetRectifierNumber(void)
{
//Get Rectifier number
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8);
	int		iError = 0;


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				iRectGroupID,	//Rectifier group ID		
				iVarSubID,		
				&iBufLen,			
				(void *)&pSigValue,			
				iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		////TRACE("pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
		return pSigValue->varValue.ulValue;
	}
	else
	{
		////TRACE("pSigValue->varValue.ulValue[false]\n");
		return 0;
	}

}

static unsigned int Web_GetRectifierIncludSlave(void)
{
//Get Rectifier number
	SIG_BASIC_VALUE* pSigValue = NULL;
	int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8);
	int		iError = 0, iTotalNumber = 0;;
	BOOL		bMaster = FALSE;

	//Primary 
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				iRectGroupID,	//Rectifier group ID		
				iVarSubID,		
				&iBufLen,			
				(void *)&pSigValue,			
				iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		iTotalNumber += pSigValue->varValue.ulValue;
	}

	iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 2);
	//Explantion 1
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				639,	//Rectifier group ID		
				iVarSubID,		
				&iBufLen,			
				(void *)&pSigValue,			
				iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		iTotalNumber += pSigValue->varValue.ulValue;
	}

	//Explantion 2
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				640,	//Rectifier group ID		
				iVarSubID,		
				&iBufLen,			
				(void *)&pSigValue,			
				iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		iTotalNumber += pSigValue->varValue.ulValue;
	}
	
	////Explantion 3
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
				641,	//Rectifier group ID		
				iVarSubID,		
				&iBufLen,			
				(void *)&pSigValue,			
				iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		iTotalNumber += pSigValue->varValue.ulValue;
	}

	return iTotalNumber;
}


static unsigned int Web_GetNetscapeRectifierNumber(void)
{
	//Get Rectifier number

	// TRACE("\n Web_GetNetscapeRectifierNumber");
	int iRectGroupEquipID = 0;
	int iRectGroupID_ex  = 200;
	iRectGroupEquipID = DXI_GetEquipIDFromStdEquipID(iRectGroupID_ex);
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8);
	int		iError = 0;


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iRectGroupEquipID,	//Rectifier group Equip ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		//TRACE("pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
		return pSigValue->varValue.ulValue;
	}
	else
	{
		//TRACE("pSigValue->varValue.ulValue[false]\n");
		return 0;
	}

}

static unsigned int Web_GetConverterNumber(void)
{
	//Get Rectifier number
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8);
	int		iError = 0;
	int     iConverterGroupID=210;


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iConverterGroupID,	//Rectifier group ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		////TRACE("pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
		return pSigValue->varValue.ulValue;
	}
	else
	{
		////TRACE("pSigValue->varValue.ulValue[false]\n");
		return 0;
	}

}

static unsigned int Web_WorkStatus(void)
{
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 180);
	int		iError = 0;
	int     iSystemID=1;


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iSystemID,	//Rectifier group ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		////TRACE("pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
		return pSigValue->varValue.enumValue;
	}
	else
	{
		////TRACE("pSigValue->varValue.ulValue[false]\n");
		return 0;
	}


}
static int Web_GetCSUNum(int iType)
{
	//Get Rectifier number
#ifdef SUPPORT_CSU
	int		iVarSubID ;
	int		iSystemID = 1 ;
	int		iError = 0;
	if(iType == 0)//BattLC
	{
		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 95);

	}
	else
	{
		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 94);
	}
	SIG_BASIC_VALUE* pSigValue;

	VAR_VALUE_EX value;


	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;



	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iSystemID,	//Rectifier group ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if (iError == ERR_DXI_OK)
	{

		return pSigValue->varValue.ulValue;
	}
#endif

	return 0;

}
static unsigned int Web_GetSMDUCFGStatus(void)
{
	//Get Rectifier number
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 3);
	int		iSMDUGroupID = 106;
	int		iError = 0;
	VAR_VALUE_EX value;
	

	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iSMDUGroupID,	//Rectifier group ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		////TRACE("pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
		/*if(pSigValue->varValue.enumValue == 1)
		{
			iBufLen = sizeof(VAR_VALUE_EX);
			iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 3);
			value.varValue.enumValue  = 0;
			

			DxiSetData(VAR_A_SIGNAL_VALUE,
				iSMDUGroupID,			
				iVarSubID,		
				iBufLen,			
				&value,			
				10000);
			return 1;
		}*/
		return pSigValue->varValue.ulValue;
	}

	return 0;

}

static unsigned int Web_GetACDNumber(void)
{
    //Get ACD number
    int iACDGroupID = 103;
    SIG_BASIC_VALUE* pSigValue;
    int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1);
    int		iError = 0;


    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
        iACDGroupID,	//Rectifier group ID		
        iVarSubID,		
        &iBufLen,			
        (void *)&pSigValue,			
        iTimeOut);

    if (iError == ERR_DXI_OK)
    {
        //TRACE("GetACDNumber pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
        return pSigValue->varValue.ulValue;
    }
    else
    {
        ////TRACE("pSigValue->varValue.ulValue[false]\n");
        return 0;
    }

}

static unsigned int Web_GetDCDNumber(void)
{
    //Get DCD number
    int iDCDGroup = 109;
    SIG_BASIC_VALUE* pSigValue;
    int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1);
    int		iError = 0;


    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
        iDCDGroup,	//Rectifier group ID		
        iVarSubID,		
        &iBufLen,			
        (void *)&pSigValue,			
        iTimeOut);

    if (iError == ERR_DXI_OK)
    {
        ////TRACE("pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
        return pSigValue->varValue.ulValue;
    }
    else
    {
        ////TRACE("pSigValue->varValue.ulValue[false]\n");
        return 0;
    }

}

static int GetSlaveMode(void)
{
	//Get BATT number


	int iSystemID = 1;
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID =DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 180); //DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 63);
	int		iError = 0;


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iSystemID,	//DCD1 ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if(iError == ERR_DXI_OK)
	{
		if(pSigValue->varValue.enumValue == 2 )
		{
			return 1;
		}
	}
	else
	{

		return 0;
	}
  


	//TRACE("GetBATTNumber pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);

}
static unsigned int Web_GetBATTNumber(void)
{
    //Get BATT number

    int iBATTNumbertemp = 0;
    int iBattGroupID = 115;
    SIG_BASIC_VALUE* pSigValue;
    int		iVarSubID =DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 34); //DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 63);
    int		iError = 0;


        iError = DxiGetData(VAR_A_SIGNAL_VALUE,
            iBattGroupID,	//DCD1 ID		
            iVarSubID,		
            &iBufLen,			
            (void *)&pSigValue,			
            iTimeOut);

        if(iError == ERR_DXI_OK)
        {
            iBATTNumbertemp = pSigValue->varValue.ulValue; 
        }
        else
        {
            ////TRACE("pSigValue->varValue.ulValue[false]\n");
            return 0;
        }
        pSigValue = NULL;      

   
    //TRACE("GetBATTNumber pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
    return iBATTNumbertemp;
}

static unsigned int Web_GetEIBBattNumber(int iEquipID)
{
	//Get BATT number

	int iBATTNumbertemp = 0;
	int iBattGroupID = iEquipID;
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID =DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 7); //DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 63);
	int		iError = 0;

	EQUIP_INFO *pEquipInfo = NULL;

	iError = DxiGetData(VAR_A_EQUIP_INFO,
		iBattGroupID,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	if(pEquipInfo->bWorkStatus == FALSE)
	{

		return iBATTNumbertemp;
	}


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iBattGroupID,	//DCD1 ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if(iError == ERR_DXI_OK)
	{
		iBATTNumbertemp = pSigValue->varValue.enumValue; 
	}
	else
	{
		////TRACE("pSigValue->varValue.ulValue[false]\n");
		return 0;
	}
	pSigValue = NULL;      


	//TRACE("GetBATTNumber pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
	return iBATTNumbertemp;
}
static BOOL Web_GetEquipExistStatus(int iEquipID)
{
	int		iError = 0, iBufLen = 0;
	EQUIP_INFO *pEquipInfo = NULL;

	iError = DxiGetData(VAR_A_EQUIP_INFO,
		iEquipID,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	if(iError == ERR_DXI_OK && pEquipInfo->bWorkStatus == TRUE)
	{
		return TRUE;
	}
	return FALSE;
}
 /*==========================================================================*
 * FUNCTION :  Web_MakeSampleDataBuf
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				IN int iEquipID:
				IN OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_MakeSampleDataBuf(IN int iLanguage, IN int iEquipID,
								 IN OUT char **ppBuf)
{
	int					iNum, iLen = 0;
	char				*pMBuf = NULL;
	char				*pSamplerName = NULL;
	SAMPLE_SIG_VALUE	*pSampleSigValue = NULL;
	//char				szTime[32];
	time_t				tmSample;
	//char				szTimeValue[32];
	int					iSampleDataNum = 0;
	char				szDisBuf[64];
	int					iEquipTypeID = 0;
	//static	int iRectHighSNIndex = Web_GetRectHighSNIndex();

	/*get sample signal value,value type is var_float*/
	iSampleDataNum = Web_GetSampleSignalByEquipID(iEquipID, &pSampleSigValue);
	
	if(iSampleDataNum <= 0  )
	{
		return FALSE;
	}
	pMBuf = NEW(char,MAX_COMM_RETURN_DATA_LINE_SIZE * iSampleDataNum);
	if(pMBuf == NULL )
	{
		return FALSE;
	}

	iEquipTypeID = Web_GetEquipType(iEquipID); 

	////TRACE("iSampleSigNumRet:%d\n",iSampleSigNumRet);
 	for(iNum = 0; iNum < iSampleDataNum && pSampleSigValue != NULL ; iNum++, pSampleSigValue++)
	{	
		
		////TRACE("pSampleSigValue->pStdSig->byValueDisplayAttr=[%c][%d][%d]\n", 
		//								pSampleSigValue->pStdSig->byValueDisplayAttr, 
		//								pSampleSigValue->pStdSig->byValueDisplayAttr,
		//								DISPLAY_WEB);
		if(STD_SIG_IS_DISPLAY_ON_UI(pSampleSigValue->pStdSig,DISPLAY_WEB))
		{

			pSamplerName = Web_GetSamplerNameById(iLanguage, pSampleSigValue->sv.iSamplerID);
			if(pSamplerName == NULL )
			{
				pSamplerName = NEW(char, 16);
				if(pSamplerName != NULL)
				{
					strncpyz(pSamplerName,"--",16);
				}
				else
				{
					return FALSE;
				}
				 
			}

			
			tmSample = pSampleSigValue->bv.tmCurrentSampled;
			if(pSampleSigValue->pStdSig->iSigValueType == VAR_FLOAT)
			{
				iLen += sprintf(pMBuf + iLen,"  %f,   %ld,   %d, \"%s\", %4d,%d,%d,\n",
							pSampleSigValue->bv.varValue.fValue,
							tmSample,
							pSampleSigValue->sv.iRelatedAlarmLevel,
							pSamplerName, 
							pSampleSigValue->sv.iSamplerChannel,
							SIG_VALUE_IS_VALID(&pSampleSigValue->bv),
							SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv) 
								&& (SIG_VALUE_NEED_DISPLAY(&pSampleSigValue->bv)));
			}
			else if(pSampleSigValue->pStdSig->iSigValueType == VAR_LONG)
			{
				iLen += sprintf(pMBuf + iLen,"   %8ld,  %ld,  %d, \"%s\", %4d,%d,%d,\n",
						pSampleSigValue->bv.varValue.lValue,
						tmSample,
						pSampleSigValue->sv.iRelatedAlarmLevel,
						pSamplerName,
						pSampleSigValue->sv.iSamplerChannel,
						SIG_VALUE_IS_VALID(&pSampleSigValue->bv),
						SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv) 
							&& (SIG_VALUE_NEED_DISPLAY(&pSampleSigValue->bv)));
			}
			else if(pSampleSigValue->pStdSig->iSigValueType == VAR_UNSIGNED_LONG)
			{
				
				if(((iEquipTypeID ==201)||(iEquipTypeID == 1601) || (iEquipTypeID == 1701) || (iEquipTypeID == 1801)|| (iEquipTypeID == 1101) || (iEquipTypeID == 2501)) && pSampleSigValue->pStdSig->iSigID == 8)
				{
					//if(!Web_GetRectHighValid(iEquipID))//Old rectifier
					//{
					//	snprintf(szDisBuf, sizeof(szDisBuf), "2%02d%01X%06d", 
					//				((pSampleSigValue->bv.varValue.ulValue) >> 24) & 0x1F,
					//				((pSampleSigValue->bv.varValue.ulValue) >> 16) & 0x0F,
					//				LOWORD((DWORD)(pSampleSigValue->bv.varValue.ulValue)));
					//}
					//else//new rectifier
					//{
							snprintf(szDisBuf, sizeof(szDisBuf), "%02u%02d%02d%05d", 
											Web_GetRectHighSNValue(iEquipID),
											((pSampleSigValue->bv.varValue.ulValue) >> 24) & 0x1F,
											((pSampleSigValue->bv.varValue.ulValue) >> 16) & 0x0F,
											LOWORD((DWORD)(pSampleSigValue->bv.varValue.ulValue)));
							
							//printf("pSampleSigValue->bv.varValue.ulValue = \n____%08x____\n",pSampleSigValue->bv.varValue.ulValue);
							//printf("pSampleSigValue->bv.varValue.ulValue = \n____%u____\n",pSampleSigValue->bv.varValue.ulValue);
				
					//}
					
					////TRACE("szDisBuf[%s]\n", szDisBuf);
					
					iLen  += sprintf(pMBuf + iLen,"  \"%s\",   %ld,   %d, \"%s\",%4d,%d,%d,\n", 
								szDisBuf,
								tmSample,
								pSampleSigValue->sv.iRelatedAlarmLevel,
								pSamplerName,
								pSampleSigValue->sv.iSamplerChannel,
								SIG_VALUE_IS_VALID(&pSampleSigValue->bv),
								SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv)
									&& (SIG_VALUE_NEED_DISPLAY(&pSampleSigValue->bv)));
				}
				else
				{
					iLen  += sprintf(pMBuf + iLen,"  %8lu,   %ld,   %d, \"%s\",%4d,%d,%d,\n", 
								pSampleSigValue->bv.varValue.ulValue,
								tmSample,
								pSampleSigValue->sv.iRelatedAlarmLevel,
								pSamplerName,
								pSampleSigValue->sv.iSamplerChannel,
								SIG_VALUE_IS_VALID(&pSampleSigValue->bv),
								SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv)
									&& (SIG_VALUE_NEED_DISPLAY(&pSampleSigValue->bv)));
				}

				//DELETE(pTempBuf);
			}
			else if(pSampleSigValue->pStdSig->iSigValueType == VAR_ENUM)
			{
				iLen  += sprintf(pMBuf + iLen,"  %8ld,  %ld,   %d, \"%s\",%4d, %d,%d,\n", 
								pSampleSigValue->bv.varValue.enumValue,
								tmSample,
								pSampleSigValue->sv.iRelatedAlarmLevel,
								pSamplerName,
								pSampleSigValue->sv.iSamplerChannel,
								SIG_VALUE_IS_VALID(&pSampleSigValue->bv),
								SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv)
									&& (SIG_VALUE_NEED_DISPLAY(&pSampleSigValue->bv)));
			}
			else if(pSampleSigValue->pStdSig->iSigValueType == VAR_DATE_TIME)
			{	
				
				//TimeToString(pSampleSigValue->bv.varValue.dtValue, TIME_CHN_FMT, 
				//szTimeValue, sizeof(szTimeValue));
				iLen  += sprintf(pMBuf + iLen,"  %ld, %ld,  %d, \"%s\",%4d,%d,%d,  \n", 
								pSampleSigValue->bv.varValue.dtValue,
								tmSample,
								pSampleSigValue->sv.iRelatedAlarmLevel,
								pSamplerName,
								pSampleSigValue->sv.iSamplerChannel,
								SIG_VALUE_IS_VALID(&pSampleSigValue->bv),
								SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv)
									&& (SIG_VALUE_NEED_DISPLAY(&pSampleSigValue->bv)));
			}
			
			DELETE(pSamplerName);
			pSamplerName = NULL;
		}
	} 
    //TRACE("%s",pMBuf);
    //TRACE("Web_MakeSampleDataBuf OK!!!\n");

	if(iLen > 0)
	{
		char		*pSearchValue = NULL;
		pSearchValue = strrchr(pMBuf, 44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}
		////TRACE("pMBuf : %s", pMBuf);
		*ppBuf = pMBuf;
		return iLen;
	}
	else
	{
		DELETE(pMBuf);
		pMBuf = NULL;
		return 0;
	}
}

static int Web_GetEquipType(IN int iEquipID) 
{
	EQUIP_INFO		*pEquipInfo;

	int iError = DxiGetData(VAR_A_EQUIP_INFO,
							iEquipID,			
							0,		
							&iBufLen,			
							&pEquipInfo,			
							0);
	if (iError == ERR_DXI_OK)
	{
		 return pEquipInfo->iEquipTypeID;
	}
	else
	{
		 return 0;
	}
}

/*==========================================================================*
 * FUNCTION :  Web_GetSampleSignalByEquipID
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT SAMPLE_SIG_VALUE **ppSigValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetSampleSignalByEquipID(IN int iEquipID, 
										OUT SAMPLE_SIG_VALUE **ppSigValue)
{
		SAMPLE_SIG_VALUE *pSigValue = NULL;
		int				iSignalNum = 0;

		int				iError = DxiGetData(VAR_SAM_SIG_VALUE_OF_EQUIP,
											iEquipID,			
											iVarSubID,		
											&iBufLen,			
											(void*)&pSigValue,			
											iTimeOut);
		
		iSignalNum = iBufLen /sizeof(SAMPLE_SIG_VALUE);

		if(iError == ERR_DXI_OK)
		{
			*ppSigValue = pSigValue;
			return iSignalNum;
		}
		else
		{
			return FALSE;
		}

}



/*==========================================================================*
 * FUNCTION :  Web_GetControlSignalByEquipID
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT CTRL_SIG_VALUE **ppSigValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetControlSignalByEquipID(IN int iEquipID, 
										 OUT CTRL_SIG_VALUE **ppSigValue)
{
		CTRL_SIG_VALUE	*pCtrlSigInfo = NULL;
		int				iSignalNum = 0;

		int				iError = DxiGetData(VAR_CON_SIG_VALUE_OF_EQUIP,
											iEquipID,			
											iVarSubID,		
											&iBufLen,			
											(void*)&pCtrlSigInfo,			
											iTimeOut);

		iSignalNum = iBufLen /sizeof(CTRL_SIG_VALUE);
		
		if(iError == ERR_DXI_OK)
		{
			*ppSigValue = pCtrlSigInfo;
			return iSignalNum;
		}
		else
		{
			return FALSE;
		}
 
}



/*==========================================================================*
 * FUNCTION :  Web_GetSettingSignalByEquipID
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT SET_SIG_VALUE **ppSigValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetSettingSignalByEquipID(IN int iEquipID, 
										 OUT SET_SIG_VALUE **ppSigValue)
{
	SET_SIG_VALUE *pSetSigValue = NULL;

	int iError = 0;

	int	iSignalNum = 0;

	iError = DxiGetData(VAR_SET_SIG_VALUE_OF_EQUIP,
						iEquipID,			
						iVarSubID,		
						&iBufLen,			
						(void *)&pSetSigValue,			
						0);

	iSignalNum = iBufLen / sizeof(SET_SIG_VALUE);
	////TRACE("Web_GetSettingSignalByEquipID:iSignalNum[%d]\n", iSignalNum);
	if (iError == ERR_DXI_OK)
	{
		*ppSigValue = pSetSigValue;
		return iSignalNum;
	}
	else
	{
		return FALSE;
	}
}



/*==========================================================================*
 * FUNCTION :  Web_GetRealTimeAlarmData
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT ALARM_SIG_VALUE **ppSigValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetRealTimeAlarmData(OUT ALARM_SIG_VALUE **ppSigValue)
{
	int						iError = 0;
	ALARM_SIG_VALUE			*stAlarmSigValue = NULL;
	int						iSignalNum = 0;
		 
	if(iRAlarmSigNumRet > 0)
	{
        stAlarmSigValue = NEW(ALARM_SIG_VALUE, iRAlarmSigNumRet);
		if(stAlarmSigValue == NULL)
		{
			return FALSE;
		}
		memset(stAlarmSigValue, 0x0, sizeof(ALARM_SIG_VALUE) * iRAlarmSigNumRet);
	}
	else
	{
		return FALSE;
	}
	

	iBufLen = sizeof(ALARM_SIG_VALUE) * iRAlarmSigNumRet;//Should assign size
	
	iError += DxiGetData(VAR_ACTIVE_ALARM_INFO,
						ALARM_LEVEL_NONE,			
						0,		
						&iBufLen,			
						stAlarmSigValue,			
						0);
	 
	iSignalNum = iBufLen / sizeof(ALARM_SIG_VALUE);
	
	
	if(iError == ERR_DXI_OK)
	{
		if(stAlarmSigValue != NULL)
		{
			*ppSigValue = (ALARM_SIG_VALUE *)stAlarmSigValue;
		}
				
		return iSignalNum;
		
	}
	else
	{
		return FALSE;
	}
}

static int Web_GetNetscapeRealTimeAlarmData(OUT ALARM_SIG_VALUE **ppSigValue)
{
	int iSigNumber = 0, iGetSigNumber = 0, iSignalNum = 0;
	ALARM_SIG_VALUE			*stAlarmSigValue = NULL;
	int iError = 0;

	iError += DxiGetData(VAR_ACTIVE_ALARM_NUM,
		ALARM_LEVEL_NONE,			
		iVarSubID,		
		&iBufLen,			
		&iGetSigNumber,			
		iTimeOut);


	if(iGetSigNumber > 0)
	{
		stAlarmSigValue = NEW(ALARM_SIG_VALUE, iGetSigNumber);
		if(stAlarmSigValue == NULL)
		{
			return FALSE;
		}
		memset(stAlarmSigValue, 0x0, sizeof(ALARM_SIG_VALUE) * iGetSigNumber);
	}
	else
	{
		return FALSE;
	}


	iBufLen = sizeof(ALARM_SIG_VALUE) * iGetSigNumber;//Should assign size

	iError += DxiGetData(VAR_ACTIVE_ALARM_INFO,
		ALARM_LEVEL_NONE,			
		0,		
		&iBufLen,			
		stAlarmSigValue,			
		0);

	iSignalNum = iBufLen / sizeof(ALARM_SIG_VALUE);

	//TRACE("\niSignalNum:%d\n", iSignalNum);
	if(iError == ERR_DXI_OK)
	{
		if(stAlarmSigValue != NULL)
		{
			*ppSigValue = (ALARM_SIG_VALUE *)stAlarmSigValue;
		}

		return iSignalNum;

	}
	else
	{
		return FALSE;
	}
}


/*==========================================================================*
 * FUNCTION :  Web_MakeControlDataBuf
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				IN int iEquipID:
				IN OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_MakeControlDataBuf(IN int iLanguage, IN int iEquipID,
								  IN OUT char **ppBuf)
{
	int					iNum, iLen = 0;
	char				*pMBuf = NULL;
	char				*pSamplerName = NULL;
	//char				szTime[32];
	time_t				tmSample;
	//char				szTimeValue[32];
	int					iSignalNumber = 0;
 
	/*get control signal value, value type is var_float var_long var_unsign ,var_bool*/
	CTRL_SIG_VALUE		*pControlSigValue = NULL;
 
	iSignalNumber = Web_GetControlSignalByEquipID(iEquipID,&pControlSigValue);
	if(iSignalNumber > 0)
	{
		pMBuf = NEW(char,MAX_COMM_RETURN_DATA_LINE_SIZE * iSignalNumber);
		if(pMBuf == NULL )
		{
			return FALSE;
		}
		memset(pMBuf, 0x0, (size_t)MAX_COMM_RETURN_DATA_LINE_SIZE * iSignalNumber);
	}
	else
	{
		return FALSE;
	}


	
	for(iNum =0; iNum < iSignalNumber && pControlSigValue != NULL; iNum++, pControlSigValue++)
	{	
		if(STD_SIG_IS_DISPLAY_ON_UI(pControlSigValue->pStdSig,DISPLAY_WEB))
		{
			pSamplerName = Web_GetSamplerNameById(iLanguage, pControlSigValue->sv.iSamplerID);
			
			if(pSamplerName == NULL)
			{
				pSamplerName = NEW(char, 16);
				if(pSamplerName != NULL)
				{
					strncpyz(pSamplerName,"--",16);
				}
				else
				{
					return FALSE;
				}
				 
			}

			tmSample = pControlSigValue->bv.tmCurrentSampled;

			if(pControlSigValue->pStdSig->iSigValueType  == VAR_FLOAT)
			{
				iLen += sprintf(pMBuf + iLen,"\t %f,%ld,\"%s\",%4d,%d,%d,\n",
										pControlSigValue->bv.varValue.fValue,
										tmSample,
										pSamplerName, 
										pControlSigValue->sv.iControlChannel,
										(CTRL_SIG_IS_CTRL_ON_UI(pControlSigValue->pStdSig,CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pControlSigValue->bv) : 0,
										SIG_VALUE_IS_CONFIGURED(&pControlSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pControlSigValue->bv)));
				
			}
			else if(pControlSigValue->pStdSig->iSigValueType == VAR_LONG)
			{
				iLen  += sprintf(pMBuf + iLen,"\t %8ld, %ld,\"%s\",%4d,%d,%d,\n",
										pControlSigValue->bv.varValue.lValue,
										tmSample,
										pSamplerName,
										pControlSigValue->sv.iControlChannel,
										(CTRL_SIG_IS_CTRL_ON_UI(pControlSigValue->pStdSig,CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pControlSigValue->bv) : 0,
										SIG_VALUE_IS_CONFIGURED(&pControlSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pControlSigValue->bv)));
			}
			else if(pControlSigValue->pStdSig->iSigValueType == VAR_UNSIGNED_LONG)
			{
				iLen +=  sprintf(pMBuf + iLen,"\t %lu, %ld, \"%s\",%4d, %d,%d,\n",
								pControlSigValue->bv.varValue.ulValue,
								tmSample,
								pSamplerName ,
								pControlSigValue->sv.iControlChannel,
								(CTRL_SIG_IS_CTRL_ON_UI(pControlSigValue->pStdSig, CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pControlSigValue->bv) : 0,
								SIG_VALUE_IS_CONFIGURED(&pControlSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pControlSigValue->bv)));

			}
			else if(pControlSigValue->pStdSig->iSigValueType == VAR_ENUM)
			{
				iLen += sprintf(pMBuf + iLen,"\t %d, %ld,\"%s\", %4d,%d,%d,\n",
								(int)pControlSigValue->bv.varValue.enumValue,
								tmSample,
								pSamplerName,
								pControlSigValue->sv.iControlChannel,
								(CTRL_SIG_IS_CTRL_ON_UI(pControlSigValue->pStdSig, CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pControlSigValue->bv) : 0,
								SIG_VALUE_IS_CONFIGURED(&pControlSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pControlSigValue->bv)));
			}
			else if(pControlSigValue->pStdSig->iSigValueType == VAR_DATE_TIME)	
			{

				iLen += sprintf(pMBuf + iLen,"\t %ld, %ld,\"%s\", %4d,%d,%d,\n",
									pControlSigValue->bv.varValue.dtValue,
									tmSample,
									pSamplerName ,
									pControlSigValue->sv.iControlChannel,
									(CTRL_SIG_IS_CTRL_ON_UI(pControlSigValue->pStdSig,CONTROL_WEB)) ? SIG_VALUE_IS_CONTROLLABLE(&pControlSigValue->bv) : 0,
									SIG_VALUE_IS_CONFIGURED(&pControlSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pControlSigValue->bv)));
			}

				
			
			DELETE(pSamplerName);
			pSamplerName = NULL;
		}

	}

	if(iLen > 0)
	{
		char		*pSearchValue = NULL;
		pSearchValue = strrchr(pMBuf, 44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}
	 
		*ppBuf = pMBuf;
		////TRACE(" iSignalNumber : %d, Web_MakeControlDataBuf : %s \n", iSignalNumber, pMBuf);
		return iLen;
	}
	else
	{
		DELETE(pMBuf);
		pMBuf = NULL;
		return 0;
	}
}


/*==========================================================================*
 * FUNCTION :  Web_GetSamplerNameById
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				IN int iSamplerId:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
/*get sampler name according to the Sampler ID*/
static char *Web_GetSamplerNameById(IN int iLanguage, IN int iSamplerId)
{
	int				i;
	SAMPLER_INFO	*pSamplersInfo = NULL; 

	int			iSamplersNumRet;
	int			iVarID = 0;
	char		*szSampleName = NULL;
	
	int iError = DxiGetData(VAR_SAMPLERS_NUM,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						&iSamplersNumRet,			
						iTimeOut);

	
	iError += DxiGetData(VAR_SAMPLERS_LIST,
							iVarID,			
							iVarSubID,		
							&iBufLen,			
							(void *)&pSamplersInfo,			
							iTimeOut);

	if(iError != ERR_DXI_OK)//sampler List is null
	{
		return NULL;

	}
	else
	{
		for(i=0; i < iSamplersNumRet && pSamplersInfo != NULL; i++, pSamplersInfo++)
		{
			if(iSamplerId == pSamplersInfo->iSamplerID)
			{
				szSampleName = NEW(char, 48);
				ASSERT(szSampleName);
				if(szSampleName != NULL)
				{
					//strncpyz(szSampleName, 
					//		pSamplersInfo->pSamplerName->pFullName[iLanguage], 
					//		32);
					sprintf(szSampleName, "%d,%s,%d",
									iSamplerId,
									pSamplersInfo->pSamplerName->pFullName[iLanguage],
									pSamplersInfo->iSamplerAddr);
					return szSampleName;
				}
				else
				{
					return NULL;
				}
				
			}
		}
		return NULL;
	}

}


/*==========================================================================*
 * FUNCTION :  Web_MakeSettingDataBuf
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				IN int iEquipID:
				IN OUT  char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_MakeSettingDataBuf(IN int iLanguage, 
								  IN int iEquipID,
								  IN OUT  char **ppBuf)
{
	int					iNum, iLen = 0;
	char				*pMBuf = NULL;
	char				*pSamplerName = NULL;
	SET_SIG_VALUE		*pParamSigValue = NULL;
	//char				szTime[32];
	time_t				tmSample;
	//char				szTimeValue[32];
	int					iSignalNumber = 0;

	 
	/*get setting signal value ,*/
	iSignalNumber = Web_GetSettingSignalByEquipID(iEquipID,&pParamSigValue);
	if(iSignalNumber > 0)
	{
		pMBuf = NEW(char,MAX_COMM_RETURN_DATA_LINE_SIZE * iSignalNumber);
	}
	else
	{
		return FALSE;
	}
	if(pMBuf == NULL)
	{
		return FALSE;
	}


	for(iNum =0; iNum < iSignalNumber && pParamSigValue != NULL; iNum++,pParamSigValue++)
	{

		if(STD_SIG_IS_DISPLAY_ON_UI(pParamSigValue->pStdSig,DISPLAY_WEB))
		{

			pSamplerName = Web_GetSamplerNameById(iLanguage,pParamSigValue->sv.iSamplerID);
			if(pSamplerName == NULL )
			{
				pSamplerName = NEW(char, 16);
				if(pSamplerName != NULL)
				{
					strncpyz(pSamplerName,"--",16);
					
				}
				else
				{
					return FALSE;
				}
				 
			}
						
			tmSample = pParamSigValue->bv.tmCurrentSampled;

			if(pParamSigValue->pStdSig->iSigValueType  == VAR_FLOAT)
			{
				
				iLen  +=  sprintf(pMBuf + iLen,"\t %f, %ld, \"%s\", %4d, %d,%d,\n",
								pParamSigValue->bv.varValue.fValue,
								tmSample,
								pSamplerName,
								pParamSigValue->sv.iControlChannel,
								(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
								SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));
			}
			else if(pParamSigValue->pStdSig->iSigValueType  == VAR_LONG)
			{
				////TRACE("*****************[%d]********************\n", iNum);
				iLen  += sprintf(pMBuf + iLen,"\t %8ld, %ld, \"%s\", %4d, %d,%d,\n",
								pParamSigValue->bv.varValue.lValue,
								tmSample,
								pSamplerName,
								pParamSigValue->sv.iControlChannel,
								(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
								SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));
			}
			else if(pParamSigValue->pStdSig->iSigValueType == VAR_UNSIGNED_LONG)
			{
				
				iLen += sprintf(pMBuf + iLen,"\t %8lu, %ld, \"%s\", %4d, %d,%d,\n",
								pParamSigValue->bv.varValue.ulValue,
								tmSample,
								pSamplerName,
								pParamSigValue->sv.iControlChannel,
								(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
								SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));

				//DELETE(pTempBuf);
			}
			else if(pParamSigValue->pStdSig->iSigValueType == VAR_ENUM)
			{
				iLen += sprintf(pMBuf + iLen,"\t %d, %ld, \"%s\", %4d, %d,%d,\n",
								(int)pParamSigValue->bv.varValue.enumValue,
								tmSample,
								pSamplerName,
								pParamSigValue->sv.iControlChannel,
								(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
								SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));

			}
			else if(pParamSigValue->pStdSig->iSigValueType == VAR_DATE_TIME)
			{
				iLen += sprintf(pMBuf + iLen,"\t %ld, %ld, \"%s\", %4d, %d,%d,\n",
								pParamSigValue->bv.varValue.dtValue,
								tmSample,
								pSamplerName,
								pParamSigValue->sv.iControlChannel,
								(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
								SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));
			}
				
			
			DELETE(pSamplerName);
			pSamplerName = NULL;
		}
	}
	//TRACE("pMBuf : %s\n", pMBuf);
	if(iLen > 0)
	{
		char		*pSearchValue = NULL;
		pSearchValue = strrchr(pMBuf, 44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}

		*ppBuf = pMBuf;
		return iLen;
	}
	else
	{
		DELETE(pMBuf);
		pMBuf = NULL;
		return 0;
	}
}


/*==========================================================================*
 * FUNCTION :  Web_MakeRAlarmDataBuf
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				IN OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_MakeRAlarmDataBuf(IN int iLanguage, IN OUT char **ppBuf)
{
	int					iNum = 0, iLen = 0;
	char				*pMBuf = NULL;
	ALARM_SIG_VALUE		*pAlarmSigValue = NULL, *pDelete = NULL;
	//char				szTime[32];
	time_t				tmSample;
	int					iSignalNumber = 0;
	char				szDisBuf[128];
	static	int			iRectSNIndex = -1, iRectHighSNIndex = -1;
	int					k;

	if(iRectSNIndex == -1)
	{
		iRectSNIndex = Web_GetRectSNIndex();
	}

	if(iRectHighSNIndex == -1)
	{
		iRectHighSNIndex = Web_GetRectHighSNIndex();
	}

	if((iSignalNumber = Web_GetRealTimeAlarmData(&pAlarmSigValue)) > 0)
	{
		//Alarm sort
		DXI_MakeAlarmSigValueSort(pAlarmSigValue, iSignalNumber);

        if(iSignalNumber > 200)
        {
            iSignalNumber = 200;
        }
		pMBuf = NEW(char, MAX_COMM_RETURN_DATA_LINE_SIZE * iSignalNumber);
	}
	else
	{
		return FALSE;
	}

	if(pMBuf == NULL )
	{
		return FALSE;
	}
	memset(pMBuf, 0x0, (size_t)MAX_COMM_RETURN_DATA_LINE_SIZE * iSignalNumber);
	
	RunThread_Heartbeat(RunThread_GetId(NULL));
	
	pDelete = pAlarmSigValue;
	for(iNum =0; iNum < iSignalNumber && pAlarmSigValue != NULL; iNum++, pAlarmSigValue++)
	{	
		////TRACE("iLanguage : %d\n",iLanguage);

		//if(pAlarmSigValue->sv.tmStartTime > 0)
		//{		
		//	TimeToString(pAlarmSigValue->sv.tmStartTime, TIME_CHN_FMT, 
		//		szTime, sizeof(szTime));
		//}
		//else
		//{
		//	strncpyz(szTime,"--",sizeof(szTime));
		//}
		if(pAlarmSigValue->pStdSig->iAlarmLevel != ALARM_LEVEL_NONE)
		{
			tmSample = pAlarmSigValue->sv.tmStartTime;
			
			//if rect show rect id, else show device name
			if(pAlarmSigValue->pEquipInfo->iEquipTypeID == 201)
			{
				for(k = 0; k <pAlarmSigValue->pEquipInfo->pSampleSigValue->pStdSig->iSigID; k++)
				{
					//Modifyby YangGuoxin,3/16/2006
					if( !SIG_VALUE_IS_CONFIGURED(&pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectHighSNIndex]) )// old rectifier
					{
						//snprintf(szDisBuf, sizeof(szDisBuf), "%s[2%02d%01X%06d]", 
						//					pAlarmSigValue->pEquipInfo->pEquipName->pFullName[iLanguage],
						//					((pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue) >> 24) & 0x1F,
						//					((pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue) >> 16) & 0x0F,
						//					//(((BYTE*)&(pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue))[0] & 0x1F),
						//					//(((BYTE*)&(pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue))[1] & 0x0F),
						//					LOWORD((DWORD)(pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue)));
						snprintf(szDisBuf, sizeof(szDisBuf), "%s", 
										pAlarmSigValue->pEquipInfo->pEquipName->pFullName[iLanguage]);
											
					}
					else//new rectifier
					{
						//TRACE("VALUE is %u\n",Web_GetRectHighSNValue(pAlarmSigValue->pEquipInfo->iEquipID));

						//snprintf(szDisBuf, sizeof(szDisBuf), "%s[%02u%02d%02d%05d]", 
						//					pAlarmSigValue->pEquipInfo->pEquipName->pFullName[iLanguage],
						//					Web_GetRectHighSNValue(pAlarmSigValue->pEquipInfo->iEquipID),//pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectHighSNIndex].bv.varValue.ulValue,
						//					((pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue) >> 24) & 0x1F,
						//					((pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue) >> 16) & 0x0F,
						//					//(((BYTE*)&(pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue))[0] & 0x1F),
						//					//(((BYTE*)&(pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue))[1] & 0x0F),
						//					LOWORD((DWORD)(pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectSNIndex].bv.varValue.ulValue)));
						snprintf(szDisBuf, sizeof(szDisBuf), "%s", 
									pAlarmSigValue->pEquipInfo->pEquipName->pFullName[iLanguage]);
											
						//printf("\n%d %d %02d\n",iRectHighSNIndex,iRectSNIndex, pAlarmSigValue->pEquipInfo->pSampleSigValue[iRectHighSNIndex].bv.varValue.ulValue);

					}
					//End by YangGuoxin.3/16/2006
				}
			}
			else
			{
				snprintf(szDisBuf, sizeof(szDisBuf),"%s",pAlarmSigValue->pEquipInfo->pEquipName->pFullName[iLanguage]);
			}

			iLen += sprintf(pMBuf + iLen," \"%s\", %d, %ld,\"%16s\", \n",
									pAlarmSigValue->pStdSig->pSigName->pFullName[iLanguage],
									pAlarmSigValue->pStdSig->iAlarmLevel,
									tmSample,
									szDisBuf);//pAlarmSigValue->pEquipInfo->pEquipName->pFullName[iLanguage]); 
		}
	}

        RunThread_Heartbeat(RunThread_GetId(NULL));
        
	if(pDelete != NULL)
	{
		DELETE(pDelete);
		pDelete = NULL;
	}
	char		*pSearchValue = NULL;
	pSearchValue = strrchr(pMBuf, 44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	*ppBuf = pMBuf;
	return iLen;
}

static DWORD  Web_GetRectHighSNValue(IN int iEquipID)
{
	SIG_BASIC_VALUE*		pSigValue = NULL;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECT_HI_SN),
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (pSigValue && (iError == ERR_DXI_OK))
	{
#ifdef _DEBUG
		if(pSigValue->varValue.ulValue == 0)
		{
			pSigValue->varValue.ulValue = 2;
		}
#endif
		return pSigValue->varValue.ulValue;

	}
	return 1;
}

static DWORD  Web_GetRectPosition(IN int iEquipID)
{
	SIG_BASIC_VALUE*		pSigValue = NULL;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1),
		&iBufLen,			
		(void *)&pSigValue,			
		0);



	if (pSigValue && (iError == ERR_DXI_OK))
	{
		return pSigValue->varValue.ulValue;

	}
	return 0;
}

static int Web_GetRectSNIndex(void)
{
	SAMPLE_SIG_INFO		*pSampleSigInfo;
	int	iError = DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
							RECT_STD_EQUIP_ID,			
							SIG_ID_RECT_SN,		
							&iBufLen,			
							(void *)&pSampleSigInfo,			
							0);
	if (iError == ERR_DXI_OK)
	{
		 return (pSampleSigInfo->iValueDisplayID - 1);
	}
	
	return -1;
	
}
static int Web_GetRectHighSNIndex(void)
{
	SAMPLE_SIG_INFO		*pSampleSigInfo;
	int	iError = DxiGetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
							RECT_STD_EQUIP_ID,			
							SIG_ID_RECT_HI_SN,		
							&iBufLen,			
							(void *)&pSampleSigInfo,			
							0);
	if (iError == ERR_DXI_OK)
	{
		 return (pSampleSigInfo->iValueDisplayID - 1);
	}
	
	return -1;
}

static BOOL Web_GetRectHighValid(IN int iEquipID)
{
	SIG_BASIC_VALUE*		pSigValue;
	int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
							iEquipID,			
							DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SIG_ID_RECT_HI_SN),
							&iBufLen,			
							(void *)&pSigValue,			
							0);

 

	if (iError == ERR_DXI_OK)
	{
		 return SIG_VALUE_IS_CONFIGURED(pSigValue);
	}
	return FALSE;
 
}

static void Web_SortHisAlarmData(HIS_ALARM_TO_RETURN *pHisAlarmToReturn, int iNum)
{

	qsort(pHisAlarmToReturn, (size_t)iNum, sizeof(*pHisAlarmToReturn),
			(int(*)(const void *, const void *))Compare_His_ALARM_SIG_VALUE);
	
}

//modify sort based on end time, not start time.2005/6/14 by YangGuoxin
static int Compare_His_ALARM_SIG_VALUE(const HIS_ALARM_TO_RETURN *p1,
								   const HIS_ALARM_TO_RETURN *p2)
{
	if(p1->tmEndTime < p2->tmEndTime)
	{
		return 1;
	}

	if(p1->tmEndTime > p2->tmEndTime)
	{
		return -1;
	}

	return 0;
	
}
//End by 2005/6/14 by YangGuoxin


/*==========================================================================*
 * FUNCTION :  Web_GetSamSigStruofEquip
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID
				OUT void **ppBuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetSamSigStruofEquip(IN int iEquipID,OUT void **ppBuf)
{
	SAMPLE_SIG_INFO		*pSampleSigInfo  = NULL;
	int					iVarID , iError ;
	int					iLen = 0;


	/*get sample signal info*/
	iVarID = iEquipID; /*Equip ID*/
	iError = DxiGetData(VAR_SAM_SIG_STRU_OF_EQUIP,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						(void *)&pSampleSigInfo,			
						iTimeOut);
	
	iLen = iBufLen / sizeof(SAMPLE_SIG_INFO);
	if(iError == ERR_DXI_OK)
	{
		*ppBuf = pSampleSigInfo;
		return iLen;
	}
	else
	{
		return FALSE;
	}
		
}


/*==========================================================================*
 * FUNCTION :  Web_MakeSamSigStruBufofEquip
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				IN int iEquipID:	
				OUT void **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_MakeSamSigStruBufofEquip(IN int iLanguage, 
										IN int iEquipID,
										OUT char **ppBuf)
{
	int					i, j, iLen = 0;
	SAMPLE_SIG_INFO		*pGetBuf = NULL;
	char				*pOutputBuf = NULL;
	int					iSignalNumber = 0;
	int					iSignalType = 0, iStateNum = 0;
	int					iValueFormat = 0;
	/*get sampler signal structure*/
	iSignalNumber = Web_GetSamSigStruofEquip(iEquipID, (void *)&pGetBuf);
	
	/*make output buffer*/
	if(iSignalNumber > 0)
	{
		pOutputBuf = NEW(char, iSignalNumber * MAX_SIGNAL_STRUCTURE_LEN);
	}
	else
	{
		return FALSE;
	}

	if(pOutputBuf == NULL )
	{
		return FALSE;
	}
	 
	for(i = 0; i < iSignalNumber && pGetBuf != NULL ; i++, pGetBuf++)
	{
		iSignalType = pGetBuf->iSigValueType;
		iStateNum	= pGetBuf->iStateNum;
		
		////TRACE("STD_SIG_IS_DISPLAY_ON_UI(pGetBuf,DISPLAY_WEB)  : [%d]\n", STD_SIG_IS_DISPLAY_ON_UI(pGetBuf,DISPLAY_WEB));
		if(STD_SIG_IS_DISPLAY_ON_UI(pGetBuf,DISPLAY_WEB))
		{
			if(iSignalType == VAR_ENUM)
			{
				iLen += sprintf(pOutputBuf + iLen,"new OSample(%4d, \"%16s \t\", \"%8s\",%4d,%2d \n",
												pGetBuf->iSigID,
												pGetBuf->pSigName->pFullName[iLanguage],
												(strlen(pGetBuf->szSigUnit) > 0) ? pGetBuf->szSigUnit : "&nbsp",
												iSignalType,
												0);
				for(j = 0; j < iStateNum; j++)
				{
					iLen += sprintf(pOutputBuf + iLen,",\"%s\"", pGetBuf->pStateText[j]->pFullName[iLanguage]);
				}
				iLen += sprintf(pOutputBuf + iLen,"),\n");
			}
			else
			{
				iValueFormat = Web_TransferFormatToInt(pGetBuf->szValueDisplayFmt);
				iLen += sprintf(pOutputBuf + iLen,"new OSample(%4d, \"%16s \t\", \"%8s\",%4d,%4d), \n",
												pGetBuf->iSigID,
												pGetBuf->pSigName->pFullName[iLanguage],
												(strlen(pGetBuf->szSigUnit) > 0) ? pGetBuf->szSigUnit : "&nbsp",
												iSignalType,
												iValueFormat);
			}
		}
		/*iLen = iLen + sprintf(pOutputBuf+iLen,"\t %4d, \"%16s \t\", \"%8s\" , \n",
											pGetBuf->iSigID,
											pGetBuf->pSigName->pFullName[iLanguage],
											pGetBuf->szSigUnit);*/
		
		
	}
    //TRACE("%s\n",pOutputBuf);
    //TRACE("Web_MakeSamSigStruBufofEquip OK!!!\n");
	if(iLen > 0)
	{
		char		*pSearchValue = NULL;
		pSearchValue = strrchr(pOutputBuf, 44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}
		*ppBuf = pOutputBuf;
		////TRACE("****Web_MakeSamSigStruBufofEquip***%s\n", pOutputBuf);
		/*return buffer len*/
		return iLen;
	}
	else
	{
		DELETE(pOutputBuf);
		pOutputBuf = NULL;
		return 0;
	}
}

static int Web_TransferFormatToInt(IN char *szFormat)
{
	//ASSERT(szFormat);
	int		iValue1 = 0,iValue2 = 0, iNum;
	if(szFormat != NULL)
	{
		
		iNum = sscanf(szFormat,"%d.%d", &iValue1, &iValue2);
		if(iNum == 0)
		{
			iNum = sscanf(szFormat,".%d", &iValue2);
		}
		return iValue2;
	}
	else
	{
		return 0;
	}
}

static void Web_TransferESRCfgToFormat(IN char *szFormat, OUT int *iValidate)
{
	//ASSERT(szFormat);
	char	*pSearchValue = NULL;
	if(szFormat != NULL)
	{
		pSearchValue = strrchr(szFormat,'*');
		if(pSearchValue != NULL)
		{
			//TRACE("[%s][%s][%d]\n", pSearchValue, pSearchValue + 1, atoi(pSearchValue + 1));

			if(atoi(pSearchValue + 1) == 0)
			{
				*iValidate = 0;
			}
			else if(atoi(pSearchValue + 1) == 1)
			{
				*iValidate = 1;
			}
			else
			{
				*iValidate = -1;	
			}
			*pSearchValue = 0;

		}
	}
	else
	{
		*iValidate = -1;
	}
	
}

/*==========================================================================*
 * FUNCTION :  Web_GetSetSigStruofEquip
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID
				OUT void **pBuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetSetSigStruofEquip(IN int iEquipID,
									OUT SET_SIG_INFO **ppBuf)
{
	SET_SIG_INFO		*pSetSigInfo = NULL;
	int					iVarID,iInterfaceType,iError ;
	int					iLen = 0;

	iVarID = iEquipID; //Equip ID
    iInterfaceType = VAR_SET_SIG_STRU_OF_EQUIP;

	iError = DxiGetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						&pSetSigInfo,			
						iTimeOut);

	iLen = iBufLen/sizeof(SET_SIG_INFO);
	if(iError == ERR_DXI_OK)
	{
		*ppBuf = pSetSigInfo;
		return iLen;
	}
	else
	{
		return FALSE;
			
	}
}



/*==========================================================================*
 * FUNCTION :  Web_MakeSetSigStruBufofEquip
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				IN int iEquipID
				OUT void **ppBuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_MakeSetSigStruBufofEquip(IN int iLanguage, 
										IN int iEquipID,
										OUT char **ppBuf)
{
	int				i,	j, iLen = 0;
	SET_SIG_INFO	*pGetBuf = NULL, *pSetSigInfo = NULL;
	char			*pOutputBuf = NULL;
	int				iSignalNumber = 0, iStatusLen = 0;

#ifdef WEB_ADD_SYSTEM_TYPE
	char			szStatus[2048], szCtrlExpression[1024];
#else
	char			szStatus[128], szCtrlExpression[128];
#endif
	int				iValueFormat = 0;
	int				iStateNumber = 0, iCtrlExpression = 0;
	/*get set signal structure*/
	
	iSignalNumber = Web_GetSetSigStruofEquip(iEquipID,&pSetSigInfo);

	/*make output buffer*/
	if(iSignalNumber > 0)
	{
		pOutputBuf = NEW(char, iSignalNumber * MAX_SIGNAL_STRUCTURE_LEN);
	}
	else
	{
		return FALSE;
	}
	if(pOutputBuf == NULL )
	{
		return FALSE;
	}

	pGetBuf = pSetSigInfo;
	for(i = 0; i< iSignalNumber && pGetBuf != NULL ; i++, pGetBuf++)
	{
		if(STD_SIG_IS_DISPLAY_ON_UI(pGetBuf,DISPLAY_WEB))
		{
			iStatusLen = 0;
			iStateNumber = pGetBuf->iStateNum;
			if( pGetBuf->iSigValueType == VAR_ENUM)
			{
				for(j = 0; j < iStateNumber; j++)
				{

					iStatusLen += sprintf(szStatus + iStatusLen,",\"%s\"",pGetBuf->pStateText[j]->pFullName[iLanguage]);
					
				}
				
				 
			}
			else
			{
				sprintf(szStatus, "%s", "  ");
			}
			
			iCtrlExpression = pGetBuf->iCtrlExpression;
			iStatusLen = 0;
			if(iCtrlExpression > 0)
			{
				
				for(j = 0; j < iCtrlExpression ; j++)
				{
					
					if(pGetBuf->pCtrlExpression[j].pText != NULL)
					{
						iStatusLen += sprintf(szCtrlExpression + iStatusLen,",%8.2f,\"%s\"",
										pGetBuf->pCtrlExpression[j].fThreshold,
										pGetBuf->pCtrlExpression[j].pText->pFullName[iLanguage]);
					}
					else
					{
						iStatusLen += sprintf(szCtrlExpression + iStatusLen,",%8.2f,\"\"",
										pGetBuf->pCtrlExpression[j].fThreshold);
		
					}
					//TRACE("[%d]iStatusLen[%s]\n", i,szCtrlExpression);
				}
			}
			else
			{
				sprintf(szCtrlExpression, "%s", "  ");
			}

			////TRACE("*********%d******%s**%d*\n", i, szStatus, iLen);
			iValueFormat = Web_TransferFormatToInt(pGetBuf->szValueDisplayFmt);
			iLen = iLen + sprintf(pOutputBuf+iLen,"new OSetting(%4d, \"%16s\",\"%8s\",%4d, %d, %8.2f,%8.2f,%4d,%4d,%4d %s %s), \n",
									pGetBuf->iSigID,
									pGetBuf->pSigName->pFullName[iLanguage],
									(strlen(pGetBuf->szSigUnit) > 0)? pGetBuf->szSigUnit : "&nbsp",
									pGetBuf->iSigValueType,
									pGetBuf->byAuthorityLevel,
									pGetBuf->fMinValidValue,
									pGetBuf->fMaxValidValue,
									iValueFormat,
									iStateNumber,
									iCtrlExpression,
									szStatus,
									szCtrlExpression);
		}
		
	}
	//TRACE("pOutputBuf : %s\n", pOutputBuf);
	if(iLen > 0)
	{
		char		*pSearchValue = NULL;
		pSearchValue = strrchr(pOutputBuf, 44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}

		*ppBuf = pOutputBuf;
		/*return buffer len*/
		return iLen;
	}
	else
	{
		DELETE(pOutputBuf);
		pOutputBuf = NULL;
		return 0;
	}
}



/*==========================================================================*
 * FUNCTION :  Web_GetConSigStruofEquip
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID
				OUT void **ppBuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-11 12:08
 *==========================================================================*/

static int Web_GetConSigStruofEquip(IN int iEquipID,
									OUT void **ppBuf)
{
	CTRL_SIG_INFO		*pCtrlSigInfo = NULL;
	int					iVarID,iInterfaceType,iError ;


	iVarID = iEquipID; //�豸ID
	iInterfaceType = VAR_CON_SIG_STRU_OF_EQUIP;

	iError = DxiGetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						&pCtrlSigInfo,			
						iTimeOut);	

	int iLen = iBufLen/sizeof(CTRL_SIG_INFO);

	if(iError == ERR_DXI_OK)
	{
		*ppBuf = (void *)pCtrlSigInfo;
		return iLen;
	}
	else
	{
		
		return FALSE;
			
	}
}


/*==========================================================================*
 * FUNCTION :  Web_MakeConSigStruBufofEquip
 * PURPOSE  :   
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT void **pBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-11 11:18
 *==========================================================================*/
static int Web_MakeConSigStruBufofEquip(IN int iLanguage, 
										IN int iEquipID,
										OUT char **ppBuf)
{
	int					i = 0,	iLen = 0;
	CTRL_SIG_INFO		*pGetBuf = NULL;
	char				*pOutputBuf = NULL;
	int					iSignalNumber = 0;
	int					iStatusLen, j;
	char				szStatus[128], szCtrlExpression[128];
	int					iValueFormat = 0;
	int					iStateNumber = 0, iCtrlExpression = 0;

	/*get set signal structure*/
	iSignalNumber = Web_GetConSigStruofEquip(iEquipID,(void *)&pGetBuf);
	/*make output buffer*/
	if(iSignalNumber > 0)
	{
		pOutputBuf = NEW(char, iSignalNumber * MAX_SIGNAL_STRUCTURE_LEN);
	}
	else
	{
		return FALSE;
	}

	if(pOutputBuf == NULL )
	{
		return FALSE;
	}

	for(i = 0; i < iSignalNumber && pGetBuf != NULL; i++, pGetBuf++)
	{
		////TRACE("pGetBuf->iSigID: %d iControlSigNumRet : %d\n", iControlSigNumRet, pGetBuf->iSigID);
		if(STD_SIG_IS_DISPLAY_ON_UI(pGetBuf,DISPLAY_WEB))
		{
			iStatusLen = 0;
			iStateNumber = pGetBuf->iStateNum;
			if(pGetBuf->iSigValueType == VAR_ENUM)
			{
				for(j = 0; j < pGetBuf->iStateNum ; j++)
				{
					iStatusLen += sprintf(szStatus + iStatusLen,",\"%s\" ",pGetBuf->pStateText[j]->pFullName[iLanguage]);
				}
								
			}
			else
			{
				sprintf(szStatus, "%s"," ");
				
			}

			
			iCtrlExpression = pGetBuf->iCtrlExpression;
			iStatusLen = 0;
			if(iCtrlExpression > 0)
			{
				for(j = 0; j < iCtrlExpression ; j++)
				{
					iStatusLen += sprintf(szCtrlExpression + iStatusLen,",%8.2f,\"%s\"",
									pGetBuf->pCtrlExpression[j].fThreshold,
									pGetBuf->pCtrlExpression[j].pText->pFullName[iLanguage]);
				}
			}
			else
			{
				sprintf(szCtrlExpression, "%s", "  ");
			}


			iValueFormat = Web_TransferFormatToInt(pGetBuf->szValueDisplayFmt);
			iLen = iLen + sprintf(pOutputBuf + iLen,"new OControl(%4d, \"%-32s \", \"%8s\", %4d,%d, %8.2f,%8.2f,%4d,%4d,%4d %-48s %s), \n",
										pGetBuf->iSigID,
										pGetBuf->pSigName->pFullName[iLanguage],
										strlen(pGetBuf->szSigUnit) ? pGetBuf->szSigUnit : "&nbsp",
										pGetBuf->iSigValueType,
										pGetBuf->byAuthorityLevel,
										pGetBuf->fMinValidValue,
										pGetBuf->fMaxValidValue,
										iValueFormat,
										iStateNumber,
										iCtrlExpression,
                                        szStatus,
										szCtrlExpression);
		}
		
	}

	if(iLen > 0)
	{
		char		*pSearchValue = NULL;
		pSearchValue = strrchr(pOutputBuf, 44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}

		*ppBuf = pOutputBuf;
		
		/*return buffer len*/
		return iLen;
	}
	else
	{
		DELETE(pOutputBuf);
		pOutputBuf = NULL;
		return 0;
	}
}


/*==========================================================================*
 * FUNCTION :  Web_GetAlarmSigStruofEquip
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID:
				OUT void **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-6 11:08
 *==========================================================================*/
static int Web_GetAlarmSigStruofEquip(IN int iEquipID,
									  OUT void **ppBuf)
{
	ALARM_SIG_INFO		*pAlarmSigInfo = NULL;
	int					iVarID,iInterfaceType,iError ;
	int					iLen = 0;

	iVarID = iEquipID; //Equip ID
	iInterfaceType = VAR_ALARM_SIG_STRU_OF_EQUIP;
	
	iError = DxiGetData(iInterfaceType,
						iVarID,			
						iVarSubID,		
						&iBufLen,			
						&pAlarmSigInfo,			
						iTimeOut);
	
	iLen = iBufLen / sizeof(ALARM_SIG_INFO);
	if(iError == ERR_DXI_OK)
	{
		*ppBuf = (void *)pAlarmSigInfo;
		return iLen;
	}
	else
	{
		return FALSE;
			
	}
}



/*==========================================================================*
 * FUNCTION :  Web_MakeAlarmSigStruBufofEquip
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipID
				OUT void **pBuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-11 11:08
 *==========================================================================*/
static int Web_MakeAlarmSigStruBufofEquip(IN int iLanguage, 
										  IN int iEquipID,
										  OUT char **ppBuf)
{
	int					i,	iLen = 0;
	ALARM_SIG_INFO		*pGetBuf = NULL;
	char				*pOutputBuf = NULL;
	int					iSignalNumber = 0;

	/*get set signal structure*/
	if( (iSignalNumber = Web_GetAlarmSigStruofEquip(iEquipID,(void *)&pGetBuf)) >0)
	{
		pOutputBuf = NEW(char, iSignalNumber * MAX_SIGNAL_STRUCTURE_LEN);
	}
	else
	{
		return FALSE;
	}
	/*make output buffer*/
	
	if(pOutputBuf == NULL)
	{
		return FALSE;
	}

	int nType = 1;   //Unit signal
	for(i = 0; i< iSignalNumber && pGetBuf != NULL; i++, pGetBuf++)
	{
		iLen = iLen + sprintf(pOutputBuf + iLen,"new OAlarm(%4d, \"%16s\",%4d, %4d), \n",
													pGetBuf->iSigID,
													pGetBuf->pSigName->pFullName[iLanguage],
													nType,
													pGetBuf->iAlarmLevel);
		
	}
	
	char		*pSearchValue = NULL;
	pSearchValue = strrchr(pOutputBuf, 44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	*ppBuf = pOutputBuf;

	return iLen;
}



/*==========================================================================*
 * FUNCTION :   Web_MakeStatusSigStruBufofEquip
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iLanguage:
				IN int iEquipID:
				OUT char **ppBuf: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/
static int Web_MakeStatusSigStruBufofEquip(IN int iLanguage, 
										     OUT char **ppBuf)
{
	int			i = 0, j = 0, iLen = 0, iError = 0, iVarSubID = 0;
	int			iSignalID = 0, iSignalType = 0;
	/*get status signal structure*/	
	void		*pSigStruc = NULL;
	char		*pMBuf = NULL;
	int			iStatusSignalNum = 0;
	int			iStateNum = 0;
	int			iValueFormat = 0;
	ASSERT(stStatusConfig);

	if(stStatusConfig != NULL)
	{
		iStatusSignalNum = stStatusConfig->iNumber;
		
		pMBuf = NEW(char,  iStatusSignalNum * MAX_STATUS_STRUC_LEN);
		if(pMBuf == NULL)
		{
			return FALSE;
		}
		for( i = 0; i < iStatusSignalNum; i++)
		{
			iSignalID = stStatusConfig->stWebStatusConfig[i].iSignalID;
			iSignalType = stStatusConfig->stWebStatusConfig[i].iSignalType;
			iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

			iError += DxiGetData(VAR_A_SIGNAL_INFO_STRU,
								stStatusConfig->stWebStatusConfig[i].iEquipID,			
								iVarSubID,		
								&iBufLen,			
								(void *)&pSigStruc,			
								iTimeOut);
			if(iError == ERR_DXI_OK)
			{
				if(iSignalType == SIG_TYPE_SAMPLING)
				{
					iValueFormat = Web_TransferFormatToInt(((SAMPLE_SIG_INFO *)pSigStruc)->szValueDisplayFmt);

					if(((SAMPLE_SIG_INFO *)pSigStruc)->iSigValueType == VAR_ENUM)
					{
						iLen += sprintf(pMBuf + iLen, "new OStatus(\"%s\",\"%s\",%d,%d,",
							((SAMPLE_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],//>pFullName[iLanguage],
							((SAMPLE_SIG_INFO *)pSigStruc)->szSigUnit,
							iValueFormat,
							VAR_ENUM);
						
						iStateNum = ((SAMPLE_SIG_INFO *)pSigStruc)->iStateNum;
						for(j = 0; j < iStateNum; j++)
						{
							iLen += sprintf(pMBuf + iLen,"\"%s\",", ((SAMPLE_SIG_INFO *)pSigStruc)->pStateText[j]->pAbbrName[iLanguage]);//->pFullName[iLanguage]);
						}
						pMBuf[iLen - 1] = 32;
						iLen += sprintf(pMBuf + iLen,"),");
					}
					else
					{
						iLen += sprintf(pMBuf + iLen, "new OStatus(\"%s\",\"%s\",%d, %d),",
									((SAMPLE_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],
									((SAMPLE_SIG_INFO *)pSigStruc)->szSigUnit, 
									iValueFormat,
									((SAMPLE_SIG_INFO *)pSigStruc)->iSigValueType);
					}
				}
				else if(iSignalType == SIG_TYPE_SETTING)
				{
					iValueFormat = Web_TransferFormatToInt(((SET_SIG_INFO *)pSigStruc)->szValueDisplayFmt);
					if(((SET_SIG_INFO *)pSigStruc)->iSigValueType == VAR_ENUM)
					{
						iLen += sprintf(pMBuf + iLen, "new OStatus(\"%s\",\"%s\",%d,%d,",
							((SET_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],
							((SET_SIG_INFO *)pSigStruc)->szSigUnit, 
							iValueFormat,
							VAR_ENUM);
						
						iStateNum = ((SET_SIG_INFO *)pSigStruc)->iStateNum;
						for(j = 0; j < iStateNum; j++)
						{
							iLen += sprintf(pMBuf + iLen,"\"%s\",", ((SET_SIG_INFO *)pSigStruc)->pStateText[j]->pAbbrName[iLanguage]);
						}
						pMBuf[iLen - 1] = 32;
						iLen += sprintf(pMBuf + iLen,"),");

					}
					else
					{
						iLen += sprintf(pMBuf + iLen, "new OStatus(\"%s\",\"%s\",%d,%d),",
							((SET_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],
							((SET_SIG_INFO *)pSigStruc)->szSigUnit,
							iValueFormat,
							((SET_SIG_INFO *)pSigStruc)->iSigValueType);
					}
				}
				else if(iSignalType ==SIG_TYPE_CONTROL)
				{
					iValueFormat = Web_TransferFormatToInt(((CTRL_SIG_INFO *)pSigStruc)->szValueDisplayFmt);
					if(((CTRL_SIG_INFO *)pSigStruc)->iSigValueType == VAR_ENUM)
					{
						iLen += sprintf(pMBuf + iLen, "new OStatus(\"%s\",\"%s\",%d,%d,",
							((CTRL_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],
							((CTRL_SIG_INFO *)pSigStruc)->szSigUnit,
							iValueFormat,
							VAR_ENUM);
						
						iStateNum = ((CTRL_SIG_INFO *)pSigStruc)->iStateNum;
						for(j = 0; j < iStateNum; j++)
						{
							iLen += sprintf(pMBuf + iLen,"\"%s\",", ((CTRL_SIG_INFO *)pSigStruc)->pStateText[j]->pAbbrName[iLanguage]);
						}
						pMBuf[iLen - 1] = 32;
						iLen += sprintf(pMBuf + iLen,"),");
					}
					else
					{
						iLen += sprintf(pMBuf + iLen, "new OStatus(\"%s\",\"%s\",%d,%d),",
							((CTRL_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],
							((CTRL_SIG_INFO *)pSigStruc)->szSigUnit,
							iValueFormat,
							((CTRL_SIG_INFO *)pSigStruc)->iSigValueType);	
					}
				}

			}
			else
			{
                DELETE(pMBuf);
                pMBuf = NULL;
				return FALSE;
			}
		}
	}
	else
	{
        
		return FALSE;
	}
	
	pMBuf[iLen - 1] = 32;
    //TRACE("%s\n",pMBuf);
	*ppBuf = pMBuf;
	return iLen;

}



/*==========================================================================*
 * FUNCTION :  Web_MakeRAlarmSigStruBufofEquip
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_MakeRAlarmSigStruBufofEquip(OUT char **ppBuf)
{
	int			i = 0, iLen = 0;
	char		*pOutputBuf;
	if(iRAlarmSigNumRet <= 0)
	{
		return FALSE;
	}
	pOutputBuf = NEW(char, iRAlarmSigNumRet * 33);
	if(pOutputBuf == NULL )
	{
		return FALSE;
	}
	for(i = 0; i< iRAlarmSigNumRet ; i++)
	{
		iLen += sprintf(pOutputBuf + iLen, "new ORAlarm(), \n");
	}

	char		*pSearchValue = NULL;
	pSearchValue = strrchr(pOutputBuf, 44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	*ppBuf = pOutputBuf;
	/*return buffer len*/

	return iLen;
}

/*==========================================================================*
 * FUNCTION : str_ltrim
 * PURPOSE  : No any memery changed, return the pointer that point to 
 *            first non-space position in the string
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char*  strSrc : 
 * RETURN   : char* : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-02-25 14:55
 *==========================================================================*/
char* str_ltrim(char* strSrc)
{
	char*	pStr;
	
	if(*(pStr = strSrc) != ' ')
	{
		return strSrc;
	}
	
	while (*(++pStr) == ' ');
	
	return pStr;
}   



/*==========================================================================*
 * FUNCTION :   Web_SendControlCommandResult
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN const char *buf:
				IN const char *filename :
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/
static int Web_SendControlCommandResult(IN char *buf, 
										IN  char *filename)
{
#define		CGI_WEB_USER			"WEB:"
	int			fd2;
	char		pGetData[100];
	char		*ptr = NULL;
	int			iEquipID,iSignalType,iSignalID,iControltype,iAlarmLevel,iReturn = 0;
	char		szSignalName[33],szEquipName[33],szAcuInfo[100];
	VAR_VALUE_EX	value;
	char		*szReturnResult = NULL;
	int			iLanguage;
	char		*szSignalInfo = NULL; 
	int			iValueType = 0;
	char		szUserName[33], szWriteUserName[40];
	int			iModifyNameType = 0;
	int			iLen = 0;
	char		*pUserDefPageInfo = NULL;
	char		*pUserDefPageTemp = NULL;
	int			iBufLen = 0;
	int		iAutoCFGTemp = 0;
	
	

	/*offset*/
	ptr = buf;
	ptr = ptr + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;

	/*Get Equip ID*/
	strncpyz(pGetData,ptr,MAX_EQUIPID_LEN + 1);
	iEquipID = atoi(pGetData);
	ptr = ptr + MAX_EQUIPID_LEN;

	/*Get signal type*/
	strncpyz(pGetData,ptr,MAX_SIGNALTYPE_LEN + 1);
	iSignalType = atoi(pGetData);
	ptr = ptr + MAX_SIGNALTYPE_LEN;

	/*Get signal ID*/
	strncpyz(pGetData,ptr,MAX_SIGNALID_LEN + 1);
	iSignalID = atoi(pGetData);
	ptr =  ptr + MAX_SIGNALID_LEN;
	
	/*Get control type, refer to GetCommandParam in control_command.c */
	strncpyz(pGetData,ptr,MAX_CONTROLTYPE_LEN + 1);    
	iControltype = atoi(pGetData); 
	ptr =  ptr + MAX_CONTROLTYPE_LEN;

	/*Get language type*/
	strncpyz(pGetData,ptr,MAX_LANGUAGE_TYPE_LEN + 1);
	iLanguage = atoi(pGetData); 
	ptr =  ptr + MAX_LANGUAGE_TYPE_LEN;
    
    //TRACE("Web_SendControlCommandResult iLanguages:%d",iLanguage);

	/*Get Value type*/
	strncpyz(pGetData,ptr,MAX_SIGNAL_VALUE_TYPE + 1);
	iValueType = atoi(pGetData); 
	ptr =  ptr + MAX_SIGNAL_VALUE_TYPE;

	/*Get Alarm type*/
	strncpyz(pGetData,ptr,MAX_SIGNA_NEW_ALARM_LEVEL + 1);
	iAlarmLevel = atoi(pGetData); 
	ptr =  ptr + MAX_SIGNA_NEW_ALARM_LEVEL;

	/*Get Modify name type : 0 : full 1:abbr*/
	strncpyz(pGetData,ptr,MAX_MODIFY_NAME_TYPE + 1);
	iModifyNameType = atoi(pGetData); 
	ptr =  ptr + MAX_MODIFY_NAME_TYPE;

	/*the modify content*/
	
	strncpyz(pGetData,ptr,MAX_CONTROL_VALUE_LEN + 1);
	ptr =  ptr + MAX_CONTROL_VALUE_LEN;

	strncpyz(szUserName, ptr, MAX_CONTROL_USER_NAME_LEN + 1);
	sprintf(szWriteUserName, "%s %s", CGI_WEB_USER, str_ltrim(szUserName));
	

	switch(iControltype)
	{
		
		case MODIFY_SIGNAL_NAME:
			{
				
				strncpyz(szSignalName,pGetData,sizeof(szSignalName));
				//TRACE("MODIFY_SIGNAL_NAME---------[%s]----[%s]--", szSignalName, pGetData);
				//iReturn = Web_ModifySignalName(iEquipID, iSignalType,iSignalID,szSignalName, iLanguage);

				iReturn = Web_ModifyStdEquipSignalName(iEquipID,
														iSignalType,
														iSignalID,
														szSignalName, 
														iLanguage,
														iModifyNameType);//Modified by wj for three languages 2006.5.9
				Web_GetStdEquipSingalInfo( iEquipID, iSignalType, 0, iLanguage, &szSignalInfo);
				break;
			}
		case MODIFY_SIGNAL_VALUE:
			{
				
				value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
				value.pszSenderName = szWriteUserName;
				value.nSenderType = SERVICE_OF_USER_INTERFACE;
				
				if(iValueType == VAR_ENUM)
				{
					value.varValue.enumValue = atoi(pGetData);
				}
				else if(iValueType == VAR_FLOAT)
				{
					value.varValue.fValue = atof(pGetData);
				}
				else if(iValueType == VAR_UNSIGNED_LONG)
				{
					value.varValue.ulValue = (unsigned long)atol(pGetData);
				}
				else if(iValueType == VAR_LONG)
				{
					value.varValue.lValue = atol(pGetData);
				}
				else if(iValueType == VAR_DATE_TIME)
				{
					value.varValue.dtValue = (time_t)atol(pGetData);
				}
				else
				{
					iReturn = -1;
					break;
				}
		 		//if(iSignalType == SIG_TYPE_CONTROL )  /*control*/
				//{
				//	value.varValue.enumValue = atoi(pGetData);

				/*if(iEquipID == 1 && iSignalType == 2 && iSignalID == 180)
				{
					SIG_BASIC_VALUE*		pSigValue;
					int	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipID,			
						DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, iSignalID),
						&iBufLen,			
						(void *)&pSigValue,			
						0);

					if(pSigValue->varValue.enumValue != value.varValue.enumValue)
					{
						iAutoCFGTemp = 1;
					}



				}*/

				iReturn = Web_ModifySignalValue(iEquipID,iSignalType,iSignalID,value);

				/*if(iAutoCFGTemp == 1)
				{
					TRACE("\n into GET_AUTO_CONFIG!!!\n");
					RunThread_Heartbeat(RunThread_GetId(NULL));
					HANDLE hCmdThread = RunThread_Create("Auto Config",
						(RUN_THREAD_START_PROC)DIX_AutoConfigMain,
						(void*)NULL,
						(DWORD*)NULL,
						0);

					iAutoCFGTemp == 0;
				}*/

					
				//}
				//else if(iSignalType == SIG_TYPE_SETTING) /*setting*/
				//{
				//	value.varValue.fValue = atof(pGetData);
				//	iReturn = Web_ModifySignalValue(iEquipID,iSignalType,iSignalID,value);
				//}

				

				break;
			}
		case MODIFY_SIGNAL_ALARM_LEVEL:
			{
				
				//iAlarmLevel = atoi(pGetData);
				iReturn = Web_ModifySignalAlarmLevel(iEquipID,iSignalType,iSignalID,iAlarmLevel,szWriteUserName);
				
				Web_GetStdEquipSingalInfo( iEquipID, 3, 0, iLanguage, &szSignalInfo);
				
				break;
			}
		case MODIFY_EQUIP_NAME:
			{
				
				strncpyz(szEquipName,pGetData,sizeof(szEquipName));	 
				iReturn = Web_ModifyEquipName(iEquipID,szEquipName,iLanguage,iModifyNameType);//Modified by wj for three languages 2006.5.9
				Web_MakeEquipListBufferA(&szSignalInfo, iLanguage);
				
				break;
			}
		case MODIFY_ACU_INFO:
			{
				/*the equipID is acu id when it is in acu*/
				strncpyz(szAcuInfo,pGetData,sizeof(szAcuInfo));
				int iModifyType = iEquipID;			//special for modify ACU Info,iModifyType == iEquipID;
				iReturn = Web_ModifyACUInfo(szAcuInfo, iLanguage, iModifyType);//Modified by wj for three languages 2006.5.9
				Web_MakeACUInfoBufferA(&szSignalInfo, iLanguage);//Modified by wj for three languages 2006.5.9
				break;
			}
		case GET_EQUIP_SIGNAL_INFO:
			{
				
				//iReturn = Web_GetSingalAlarmInfo(iEquipID, iLanguage, &szSignalInfo);
				iReturn =  Web_GetStdEquipSingalInfo( iEquipID, 0, 0, iLanguage, &szSignalInfo);
				iReturn = 2;
				break;
			}
		case MODIFY_ALARM_ALL_INFO:
			{
				
				//iAlarmLevel = atoi(pGetData);
				iReturn = Web_ModifySignalAlarmLevel(iEquipID,iSignalType,iSignalID,iAlarmLevel,szWriteUserName);
				
				strncpyz(szSignalName,pGetData,sizeof(szSignalName));
				if(iReturn == TRUE || iReturn == WEB_RETURN_PROTECTED_ERROR)
				{
					iReturn = Web_ModifyStdEquipSignalName(iEquipID,
														iSignalType,
														iSignalID,
														szSignalName, 
														iLanguage,
														iModifyNameType);
				}
				else
				{
					iReturn = FALSE;
				}
				Web_GetStdEquipSingalInfo( iEquipID, 3, 0, iLanguage, &szSignalInfo);
			
				break;
			}

		case GET_ACU_INFO:
			{
				iReturn += Web_MakeACUInfoBufferA(&szSignalInfo, iLanguage);
				iReturn = 2;
				break;
			}
		case GET_DEVICE_LIST:
			{
				iReturn += Web_MakeEquipListBufferA(&szSignalInfo, iLanguage);
				iReturn = 2;
				break;
			}
		case CLEAR_HISTORY_DATA:
			{

				int		iClearType = 0;
				iClearType = atoi(pGetData);
				if( Web_GetProtectedStatus() == TRUE)
				{
					iReturn = 5;
				}
				else
                {
					iReturn += Web_ClearHistoryData(iClearType, szUserName);
				}
				break;
			}
		case RESTORE_DEFAULT_CONFIG:
			{
				DXI_ReloadDefaultConfig();
				break;
			}
		case RESTART_ACU:
			{
				DXI_RebootACUorSCU(TRUE,TRUE);
				break;
			}
		case GET_USER_DEF_PAGE:
			{
				//printf("\n___Into User Def Page___\n");
				pUserDefPageInfo = NEW(char,(101 * MAX_SIGNAL_STRUCTURE_LEN+101 * MAX_COMM_RETURN_DATA_LINE_SIZE));

				iBufLen = Web_MakeSigValOfUserDefPage(iLanguage,iEquipID,&pUserDefPageTemp);//iEquipID = iPageID

				iLen = sprintf(pUserDefPageInfo,"%s",pUserDefPageTemp);

				SAFELY_DELETE(pUserDefPageTemp);

				//pUserDefPageInfo[iBufLen - 1] = 59;

				iBufLen += Web_MakeSigInfoOfUserDefPage(iLanguage,iEquipID,&pUserDefPageTemp);//iEquipID = iPageID

				iLen += sprintf(pUserDefPageInfo + iLen,"%s",pUserDefPageTemp);

				SAFELY_DELETE(pUserDefPageTemp);

				iReturn = 6;


				break;
			}
		case SET_USER_DEF_PAGE:
			{

				value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
				value.pszSenderName = szWriteUserName;
				value.nSenderType = SERVICE_OF_USER_INTERFACE;

				if(iValueType == VAR_ENUM)
				{
					value.varValue.enumValue = atoi(pGetData);
				}
				else if(iValueType == VAR_FLOAT)
				{
					value.varValue.fValue = atof(pGetData);
				}
				else if(iValueType == VAR_UNSIGNED_LONG)
				{
					value.varValue.ulValue = (unsigned long)atol(pGetData);
				}
				else if(iValueType == VAR_LONG)
				{
					value.varValue.lValue = atol(pGetData);
				}
				else if(iValueType == VAR_DATE_TIME)
				{
					value.varValue.dtValue = (time_t)atol(pGetData);
				}
				else
				{
					iReturn = -1;
					break;
				}
				//if(iSignalType == SIG_TYPE_CONTROL )  /*control*/
				//{
				//	value.varValue.enumValue = atoi(pGetData);
				iReturn = Web_ModifySignalValue(iEquipID,2,iSignalID,value);
				//}
				//else if(iSignalType == SIG_TYPE_SETTING) /*setting*/
				//{
				//	value.varValue.fValue = atof(pGetData);
				//	iReturn = Web_ModifySignalValue(iEquipID,iSignalType,iSignalID,value);
				//}

				//printf("\n___Into User Def Page___\n");
				Sleep(1000);
				pUserDefPageInfo = NEW(char,(101 * MAX_SIGNAL_STRUCTURE_LEN+101 * MAX_COMM_RETURN_DATA_LINE_SIZE));

				iBufLen = Web_MakeSigValOfUserDefPage(iLanguage,iSignalType,&pUserDefPageTemp);//iSignalType = iPageID

				iLen = sprintf(pUserDefPageInfo,"%s",pUserDefPageTemp);

				SAFELY_DELETE(pUserDefPageTemp);

				//pUserDefPageInfo[iBufLen - 1] = 59;

				iBufLen += Web_MakeSigInfoOfUserDefPage(iLanguage,iSignalType,&pUserDefPageTemp);//iSignalType = iPageID

				iLen += sprintf(pUserDefPageInfo + iLen,"%s",pUserDefPageTemp);

				SAFELY_DELETE(pUserDefPageTemp);



				break;
			}
		default:
			{
				break;
			}

	}
	

	/*send result*/
	szReturnResult = NEW(char, 20);
	iLen = sprintf(szReturnResult,"%10d",iReturn);
	if(szSignalInfo != NULL && strlen(szSignalInfo) != 0)
	{
		szReturnResult = RENEW(char, szReturnResult, 20 + strlen(szSignalInfo));
		strcat(szReturnResult, szSignalInfo);
		
	}
	if(szSignalInfo != NULL)
	{
		DELETE(szSignalInfo);
		szSignalInfo = NULL;
	}
	if(iControltype == GET_USER_DEF_PAGE || iControltype == SET_USER_DEF_PAGE)
	{
		szReturnResult = RENEW(char, szReturnResult, 20 + strlen(pUserDefPageInfo));
		strcat(szReturnResult, pUserDefPageInfo);
		SAFELY_DELETE(pUserDefPageInfo);

	}

	if((fd2 = open(filename,O_WRONLY ))<0)
	{
		return FALSE;

	}

	int iPipeLen = strlen(szReturnResult)/(PIPE_BUF - 1)  + 1;

	if(iPipeLen <= 1)
	{
		if((write(fd2,szReturnResult,strlen(szReturnResult) + 1)) < 0)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to write FIFO when write the control result!");
			close(fd2);
			char			szCommand[30];
			iLen = sprintf(szCommand,"kill ");
			strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

			TRACEX("%s\n", szCommand);
			system(szCommand);
			return FALSE;
		}
	}
	else
	{
		int m;
		char szP[PIPE_BUF - 1];
		for(m = 0; m < iPipeLen; m++)
		{
			strncpyz(szP, szReturnResult + m * (PIPE_BUF - 2), PIPE_BUF - 1);
		
			if((write(fd2, szP, PIPE_BUF - 1)) < 0)
			{
                close(fd2);
				return FALSE;
			}
		}
	}
	
	if(szReturnResult != NULL)
	{
		DELETE(szReturnResult);
		szReturnResult  = NULL;
	}

	if(szSignalInfo != NULL)
	{
		DELETE(szSignalInfo);
		szSignalInfo = NULL;
	}
	close(fd2);
	return TRUE;
}

static BOOL Web_GetProtectedStatus(void) 
{
	SITE_INFO  *pSiteInfo = NULL;
	
	iBufLen = sizeof(SITE_INFO);
	int iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
							SITE_INFO_POINTER,			
							0,		
							&iBufLen,			
							&pSiteInfo,			
							0);

	if (iError == ERR_DXI_OK)
	{
		return pSiteInfo->bSettingChangeDisabled;
	}
	else
	{
		return FALSE;
	}
}

static BOOL Web_ClearHistoryData(IN int iClearType, IN char *szUserName)
{
	//printf("\n____Into Web_ClearHistoryData iClearType:%d__\n",iClearType);
	if ((iClearType < 0) || (iClearType >= ITEM_OF(szClearName)))	// added by maofuhua. 2005-05-10
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,
			"User [%s] try to clear non-existed data type: %d\n",
			szUserName, iClearType);
		return FALSE;
	}

	if(DAT_StorageDeleteRecord(szClearName[iClearType]) == TRUE)
	{
		//printf("\n____Into Web_ClearHistoryData OK!!___\n");
		
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"User [%s] Successfully to Clear [%s]", szUserName, szClearName[iClearType]);
		return TRUE;
	}
	else
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"User [%s] Fail to Clear [%s]", szUserName, szClearName[iClearType]);
		return FALSE;
	}
}


/*==========================================================================*
 * FUNCTION :   Web_ModifyStdEquipSignalName
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int equipID:
				IN int signalType:
				IN int signalID:
				IN const char *signalName:
				IN int iLanguage: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/
static int Web_ModifyStdEquipSignalName(IN int iEquipID,
										IN int iSignalType,
										IN int iSignalID,
										IN char *szSignalName, 
										IN int iLanguage,
										IN int iModifyNameType)
{

	ASSERT(szSignalName);
	SET_A_SIGNAL_INFO_STU		stSetASignalInfo;
	char	*szTrimText = NULL;

	int iError		= 0;
	int iBufLen		= sizeof(SET_A_SIGNAL_INFO_STU);
	int iVarSubID	= DXI_MERGE_SIG_ID(iSignalType,iSignalID);

	if(iLanguage == ENGLISH_LANGUAGE_NAME )
	{
		if(iModifyNameType == 1)		//abbr name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_ENGLISH_ABBR_NAME;
		}
		else							//full name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_ENGLISH_FULL_NAME;
		}
	}
	else if(iLanguage == LOCAL_LANGUAGE_NAME)
	{
		if(iModifyNameType == 1)		//abbr name
		{
			stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL_ABBR_NAME;
		}
		else							//full name
		{
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL_FULL_NAME;
		}
	}

    //////////////////////////////////////////////////////////////////////////
    //Added by wj for three languages 2006.5.9
    else 
    {
        if(iModifyNameType == 1)		//abbr name
        {
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL2_ABBR_NAME;
        }
        else							//full name
        {
            stSetASignalInfo.byModifyType  = (BYTE)MODIFY_SIGNAL_LOCAL2_FULL_NAME;
        }
    }

    //end////////////////////////////////////////////////////////////////////////
    

	szTrimText = Cfg_RemoveWhiteSpace(szSignalName);
	strncpyz(stSetASignalInfo.szModifyBuf,szTrimText, 
											(int)sizeof(stSetASignalInfo.szModifyBuf));
	
	iError += DxiSetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
						iEquipID,			
						iVarSubID,		
						iBufLen,			
						(void *)&stSetASignalInfo,			
						0);

	szTrimText = NULL;
	
	if (iError == ERR_DXI_OK )
	{
		return TRUE;
	}
	else
	{
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}
}


/*==========================================================================*
 * FUNCTION :   Web_ModifySignalValue
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int equipID:
				IN int signalType:
				IN int signalID:
				IN VAR_VALUE value: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/
static int Web_ModifySignalValue(IN int iEquipID,
								 IN int iSignalType,
								 IN int iSignalID,
								 IN VAR_VALUE_EX value)
{
	int			iError = -1;
	int			iBufLen = sizeof(VAR_VALUE_EX);

	//printf("\n into Web_ModifySignalValue\n");

	//time_t the_time, begin_time;

	//begin_time = time((time_t *)0);

	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
						iEquipID,			
						iVarSubID,		
						iBufLen,			
						&value,			
						550000);

	if (iError == ERR_DXI_OK )
	{
		if(iEquipID == 1 && iSignalType == 2 && iSignalID == 180)
		{
			_SYSTEM("reboot");			
		}

		//the_time = time((time_t *)0);

		//printf("all time is %lf(s)\n", difftime(the_time,begin_time));

		return TRUE;
	}
	else
	{
		if(iError == ERR_EQP_CTRL_SUPPRESSED)
		{
			return MODIFY_SIGNALVALUE_FAIL;
		}
		else if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}

}



/*==========================================================================*
 * FUNCTION :  Web_ModifySignalAlarmLevel
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int equipID:
				IN int signalType:
				IN int signalID:
				IN int alarmLevel:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/

static int Web_ModifySignalAlarmLevel(IN int iEquipID,
									  IN int iSignalType,
									  IN int iSignalID,
									  IN int iAlarmLevel,
                                      IN char *szWriteUserName)
{
	SET_A_SIGNAL_INFO_STU		stSetASignalInfo;

	int iError	= 0;
	int iBufLen = 0;
	int iVarSubID	= DXI_MERGE_SIG_ID(iSignalType,iSignalID);


    strncpyz(stSetASignalInfo.cModifyUser, 
		szWriteUserName,
		strlen(szWriteUserName) + 1);

	stSetASignalInfo.byModifyType		= MODIFY_ALARM_LEVEL;
	stSetASignalInfo.bModifyAlarmLevel	= iAlarmLevel;
	
	/*iError += DxiSetData(VAR_A_SIGNAL_INFO_STRU,
						iEquipID,			
						iVarSubID,		
						iBufLen,			
						(void *)&stSetASignalInfo,			
						iTimeOut);*/
	iBufLen = sizeof(SET_A_SIGNAL_INFO_STU);
#ifdef _SHOW_WEB_INFO
	//TRACE("iEquipID,%d, iSignalType, %d, iSignalID, %d, iAlarmLevel, %d, iBufLen \n" ,
								//iEquipID, iSignalType, iSignalID, iAlarmLevel);

#endif	
	
	

	iError += DxiSetData(VAR_A_SIGNAL_INFO_THRO_STD_EQUIP,
						iEquipID,			
						iVarSubID,		
						iBufLen,			
						(void *)&stSetASignalInfo,			
						0);
#ifdef _SHOW_WEB_INFO
		//TRACE("iError :%d\n", iError);
#endif	

	if (iError == ERR_DXI_OK )
	{
		return TRUE;
	}
	else
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Modify signal alarm level fail");
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}

}



/*==========================================================================*
 * FUNCTION :   Web_ModifyEquipName
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int equipID:
				IN char equipName[32]:
				IN int iLanguage	 :
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/
static int Web_ModifyEquipName(IN int iEquipID,
							   IN char *pszEquipName,
							   IN int iLanguage,
							   IN int iModifyNameType)
{
	ASSERT(pszEquipName);
	int			iError	= 0;
	char		*szTEquipName = pszEquipName;
	char		*szEquipName = NULL;
	int			iInterfaceType;

	szEquipName = Cfg_RemoveWhiteSpace(szTEquipName);
	iBufLen = strlen(szEquipName);
	


    if(iLanguage == 1)
    {
        if(iModifyNameType == 1)
        {
            iInterfaceType = MODIFY_EQUIP_LOCAL_ABBR_NAME;
        }
        else
        {
            iInterfaceType = MODIFY_EQUIP_LOCAL_FULL_NAME;
        }
        iError += DxiSetData(VAR_A_EQUIP_INFO,
            iEquipID,			
            iInterfaceType,		
            iBufLen,			
            (void *)szEquipName,			
            iTimeOut);
    }
    else if(iLanguage == 2)
    {
        if(iModifyNameType == 1)
        {
            iInterfaceType = MODIFY_EQUIP_LOCAL2_ABBR_NAME;
        }
        else
        {
            iInterfaceType = MODIFY_EQUIP_LOCAL2_FULL_NAME;
        }
        iError += DxiSetData(VAR_A_EQUIP_INFO,
            iEquipID,			
            iInterfaceType,		
            iBufLen,			
            (void *)szEquipName,			
            iTimeOut);
    }
	else
	{
		if(iModifyNameType == 1)
		{
			iInterfaceType = MODIFY_EQUIP_ENGLISH_ABBR_NAME;
		}
		else
		{
			iInterfaceType = MODIFY_EQUIP_ENGLISH_FULL_NAME;
		}

		iError += DxiSetData(VAR_A_EQUIP_INFO,
							iEquipID,			
							iInterfaceType,		
							iBufLen,			
							(void *)szEquipName,			
							iTimeOut);
	}

    //TRACE("| Web_ModifyEquipName:%d\n",iInterfaceType);
	
	if(iError == ERR_DXI_OK)
	{
		return TRUE;
	}
	else
	{
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}


 
}


/*==========================================================================*
 * FUNCTION :    Web_SendQueryData
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   char *buf:
				char *filename:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_SendQueryData(IN char *buf, 
							 IN char *filename)
{
	char		*ptr	= NULL;				/*pointer to buf*/
	char		*pBuf	= NULL;				/*output buffer*/
	int			fd2;				/*fd2: handle of fifo*/
	int			iEquipID;			/**/
	int			iQueryType;			/*Query type*/
	int			iLanguage;			/*language*/
	time_t		toTime = 0, fromTime = 0;	/*query time*/
	char		szGetData[64];		/*temp buffer*/
	int			iQueryTimes;

	

	//open the fifo
	if((fd2 = open(filename,O_WRONLY  )) < 0)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open FIFO");
		return FALSE;
			
	}
	
	/*offset 12 */
	ptr = buf +  MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;


	/*get QueryType*/
#ifdef _SHOW_WEB_INFO
	//TRACE("iQueryType : %s\n", ptr);
#endif	
	
	strncpyz(szGetData, ptr, COMM_QUERY_TYPE_LEN + 1);
	iQueryType = atoi(szGetData);
	//TRACE("iQueryType string is %s.\n",szGetData);
	ptr = ptr + COMM_QUERY_TYPE_LEN;
	
	/*get query time (from time)*/
	strncpyz(szGetData, ptr, COMM_QUERY_TIME_LEN + 1);
	//fromTime = Web_transferCharToTime(szGetData, TRUE);
	fromTime = (time_t)atoi(szGetData);
	//TRACE("fromTime string is %s.\n",szGetData);
	ptr = ptr + COMM_QUERY_TIME_LEN;


	/*get query time (to time)*/
	strncpyz(szGetData, ptr, COMM_QUERY_TIME_LEN + 1);
	//toTime = Web_transferCharToTime(szGetData, FALSE);
	toTime = (time_t)atoi(szGetData);
	//TRACE("toTime string is %s.\n",szGetData);
	ptr = ptr + COMM_QUERY_TIME_LEN;

//#ifdef _SHOW_WEB_INFO
	////TRACE("fromTime[%d][%s]  toTime[%d][%s]\n",fromTime, ctime(&fromTime),toTime,"ctime(&toTime)");
//#endif

	/*get query equipID */
	strncpyz(szGetData, ptr, MAX_EQUIPID_LEN + 1);
	iEquipID = atoi(szGetData);
	ptr = ptr + MAX_EQUIPID_LEN;

	/*get Language type*/
	strncpyz(szGetData, ptr, MAX_LANGUAGE_TYPE_LEN + 1);
	iLanguage = atoi(szGetData);

#ifdef _SHOW_WEB_INFO
	//TRACE("iQueryType : %d  \n", iQueryType);
#endif	
	
	//TRACE("In Web_SendQueryData iQueryType is %ld, fromTime %ld,toTime %ld\n",iQueryType, fromTime,toTime);
	int iReturn = 0;
	switch(iQueryType)
	{
		case QUERY_HIS_DATA:

			iReturn = Web_MakeQueryHisDataBuffer(iEquipID, 
									fromTime, 
									toTime, 
									iLanguage, 
									&pBuf);

			break;
		case QUERY_STAT_DATA:

			iReturn = Web_MakeQueryStatDataBuffer(iEquipID, 
									fromTime, 
									toTime, 
									iLanguage, 
									&pBuf);
			//TRACE("Web_MakeQueryStatDataBuffer[%d]\n", iReturn);
			break;
		case QUERY_HISALARM_DATA:

			iReturn = Web_MakeQueryAlarmDataBuffer(iEquipID, 
									fromTime, 
									toTime, 
									iLanguage, 
									&pBuf);
			break;

		case QUERY_CONTROLCOMMAND_DATA:

			iReturn = Web_MakeQueryControlDataBuffer(fromTime, 
										toTime, 
										iLanguage, 
										&pBuf);
			break;

		case QUERY_BATTERYTEST_DATA:
			iQueryTimes = iEquipID;			//special for Battery test, 
													//iEquipID is that you want to query 
													//what times of battery test

			TRACE("Start to Web_MakeQueryBatteryTestLogBuffer");

			printf("Start to Web_MakeQueryBatteryTestLogBuffer\n");	
			iReturn = Web_MakeQueryBatteryTestLogBuffer(iQueryTimes, &pBuf);

			TRACE("End to Web_MakeQueryBatteryTestLogBuffer");
			break;

		case QUERY_HISLOG_DATA:

			iReturn = Web_MakeQueryHisLogBuffer(fromTime, toTime, &pBuf);
			break;
		case QUERY_CLEAR_ALARM:
			iReturn  = DAT_StorageDeleteRecord(HIST_ALARM_LOG);
			break;
		case QUERY_DISEL_TEST:
			iReturn	 = Web_MakeQueryDiselTestLog(fromTime, toTime,iLanguage,&pBuf);
			break;
		default:
			break;
	}
 
	if(iReturn == FALSE)
	{
		
		pBuf = NEW(char, 3);
		sprintf(pBuf, "%2d", 2);
	}
	
	int iPipeLen = strlen(pBuf)/(PIPE_BUF - 1)  + 1;

#ifdef _SHOW_WEB_INFO
	//TRACE("iPipeLen = %d\n", iPipeLen);
#endif
	

//#ifdef _DEBUG
//	FILE *fp = fopen("/scup/www/html/query.htm","w");
//	fwrite(pBuf,strlen(pBuf), 1, fp);
//	fclose(fp);
//#endif

	if(iPipeLen <= 1)
	{
		if((write(fd2, pBuf, strlen(pBuf) + 1)) < 0)
		{
            close(fd2);
			return FALSE;
		}
	}
	else
	{
		int m;
		char szP[PIPE_BUF - 1];
		for(m = 0; m < iPipeLen; m++)
		{
			strncpyz(szP, pBuf + m * (PIPE_BUF - 2), PIPE_BUF - 1);
#ifdef _SHOW_WEB_INFO
			//TRACE("m =%d \n", m);
#endif
	
			
			if((write(fd2, szP, PIPE_BUF - 1)) < 0)
			{
                close(fd2);
				return FALSE;
			}
		}

	}

	if(pBuf != NULL)
	{
		DELETE(pBuf);
		pBuf = NULL;
	}
	close(fd2);
	return TRUE;
}



/*==========================================================================*
 * FUNCTION :    Web_QueryHisData
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN time_t toTime:
				IN time_t fromTime:
				IN int iEquipID:
				IN int iDataType:
				OUT char **ppBuf: 
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_QueryHisData(IN time_t toTime,
							IN time_t fromTime,
							IN int iEquipID,
							IN int iDataType, 
							OUT void **ppBuf)
{
	/*query parameter equip,data type (his, statistics,from time to time)*/
	int			iRecords = 6000, iResult,iStartRecordNo = -1;		//record number
	HANDLE		hHisData;
	int			iBufLen = 6000;
 

	/*Judge data type*/
	HIS_GENERAL_CONDITION *pCondition	= NEW(HIS_GENERAL_CONDITION,1);
	pCondition->tmToTime				= toTime;
	pCondition->tmFromTime				= fromTime;
	pCondition->iEquipID				= iEquipID;
	pCondition->iDataType				= iDataType;

	//TRACE("iDataType:[%d]\n", iDataType);
	if(iDataType == QUERY_STAT_DATA)
	{
		hHisData =	DAT_StorageOpen(STAT_DATA_LOG);
		if(hHisData == NULL)
		{
			return FALSE;
		}
		HIS_STAT_RECORD *pHisDataRecord = NEW(HIS_STAT_RECORD, iBufLen);
		ASSERT(pHisDataRecord);		
		memset(pHisDataRecord, 0x0, iBufLen * sizeof(HIS_STAT_RECORD));

		//TRACE("Start to DAT_StorageFindRecords\n");
		iResult = DAT_StorageFindRecords(hHisData,
									Web_fnCompareQueryCondition, 
									(void *)pCondition,
									&iStartRecordNo, 
									&iRecords, 
									(void*)pHisDataRecord, 
									FALSE,
									TRUE);
		DELETE(pCondition);
		pCondition = NULL;
//#ifdef _SHOW_WEB_INFO
		//TRACE("HIS_STAT_RECORD : iResult : [%d] iRecords[%d]\n", iResult, iRecords);
//#endif
		
        if(iResult >= 1  && iRecords > 0)
		{
			//return (HIS_DATA_RECORD *)pHisDataRecord;
			*ppBuf = (void *)pHisDataRecord;
			DAT_StorageClose(hHisData);


			return iRecords;
		}
		else
		{
			DELETE(pHisDataRecord);
			pHisDataRecord = NULL;
			DAT_StorageClose(hHisData);
			return FALSE;

		}
	}
	else
	{
		hHisData =	DAT_StorageOpen( HIS_DATA_LOG);
		if(hHisData == NULL)
		{
			return FALSE;
		}

		HIS_DATA_RECORD *pHisDataRecord = NEW(HIS_DATA_RECORD,iBufLen);
		ASSERT(pHisDataRecord);	
		memset(pHisDataRecord, 0x0, iBufLen * sizeof(HIS_DATA_RECORD));

		iResult = DAT_StorageFindRecords(hHisData,
									Web_fnCompareQueryCondition, 
									(void *)pCondition,
									&iStartRecordNo, 
									&iRecords, 
									(void*)pHisDataRecord, 
									0,
									FALSE);
#ifdef _SHOW_WEB_INFO
		//TRACE("HIS_DATA_RECORD : iResult[%d] : iRecords[%d]\n", iResult, iRecords);
#endif
		
		DELETE(pCondition);
		pCondition = NULL;

		if(iResult >= 1 && iRecords > 0)
		{
			//return (HIS_DATA_RECORD *)pHisDataRecord;
			*ppBuf = (void *)pHisDataRecord;
			DAT_StorageClose(hHisData);
			return iRecords;
		}
		else
		{
			DELETE(pHisDataRecord);
			pHisDataRecord = NULL;
			DAT_StorageClose(hHisData);
			return FALSE;

		}
	}
	
}



/*==========================================================================*
 * FUNCTION :    Web_QueryHisAlarm
 * PURPOSE  :	 Query His alarm
 * CALLS    : 
 * CALLED BY:	
 * ARGUMENTS:   IN time_t fromTime:
				IN time_t toTime:
				IN int iEquipID:
				OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_QueryHisAlarm(IN time_t fromTime,
							 IN time_t toTime,
							 IN int iEquipID,
							 OUT void **ppBuf)
{
	/*query history alarm record*/
	/*query parameter equip,data type (his, statistics,from time to time)*/
	int			iRecords = MAX_HIS_ALARM_COUNT;//400;
	int			iResult = 0,iStartRecordNo = -1;		//record number
	HANDLE		hAlarmData;
	int			iBufLen	= MAX_HIS_ALARM_COUNT;//400;
	int			iMatchRecord = 0, i;
	HIS_ALARM_RECORD	*pTempHisAlarmRecord =NULL, *pTempFullHisAlarmRecord = NULL;

	 
	hAlarmData =	DAT_StorageOpen(HIST_ALARM_LOG);

	if(hAlarmData == NULL)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open hAlarmData");
		return FALSE;
	}

	HIS_ALARM_RECORD *pHisAlarmRecord	= NEW(HIS_ALARM_RECORD,iBufLen);
	HIS_GENERAL_CONDITION *pCondition = NEW(HIS_GENERAL_CONDITION,1);
	
	ASSERT(pCondition);
	//TRACE("In Web_QueryHisAlarm toTime is %d, fromTime is %d.\n",toTime,fromTime);
	pCondition->tmToTime			= toTime;
	pCondition->tmFromTime			= fromTime;
	pCondition->iEquipID			= iEquipID;
	pCondition->iDataType			= QUERY_HISALARM_DATA;
	

	if(pHisAlarmRecord == NULL )
	{
		DAT_StorageClose(HIS_ALARM_LOG);
	
		DELETE(pCondition);
		pCondition = NULL;
		DELETE(pHisAlarmRecord);
		pHisAlarmRecord = NULL;
		

		return FALSE;
	}
	//time_t the_time = time((time_t *)0);
	//printf("\n__The Time 1 is:%ld___\n",the_time);
	iRecords = MAX_HIS_ALARM_COUNT;
	iStartRecordNo = 1;
	iResult =DAT_StorageReadRecords(hAlarmData,
									&iStartRecordNo, 
									&iRecords, 
									(void*)pHisAlarmRecord, 
									0,
									FALSE);



	//iResult = DAT_StorageFindRecords(hAlarmData,
	//								Web_fnCompareQueryCondition, 
	//								(void *)pCondition,
	//								&iStartRecordNo, 
	//								&iRecords, 
	//								(void*)pHisAlarmRecord, 
	//								0,
	//								TRUE);
	
	DELETE(pCondition);
	pCondition = NULL;
	//TRACE("Current Alarm Records is %d.iResult is %d.\n\n",iRecords,iResult);
	

	if(iResult && iRecords > 0)
	{
		HIS_ALARM_RECORD *pFullHisAlarmRecord	= NEW(HIS_ALARM_RECORD,iRecords);
		ASSERT(pFullHisAlarmRecord);
		memset(pFullHisAlarmRecord, 0x0, sizeof(HIS_ALARM_RECORD) *  iRecords);
		pTempFullHisAlarmRecord = pFullHisAlarmRecord;
		pTempHisAlarmRecord = pHisAlarmRecord;
		if(pFullHisAlarmRecord != NULL && pHisAlarmRecord != NULL)
		{

			for(i = 0; i < iRecords && pHisAlarmRecord != NULL && pFullHisAlarmRecord != NULL; i++, pHisAlarmRecord++)
			{
				if((pHisAlarmRecord->tmStartTime - fromTime) > 0 && (toTime - pHisAlarmRecord->tmStartTime) > 0 && (pHisAlarmRecord->iEquipID == iEquipID || iEquipID < 0))
				{	
					memcpy(pFullHisAlarmRecord, pHisAlarmRecord, sizeof(HIS_ALARM_RECORD));
					iMatchRecord++;
					pFullHisAlarmRecord++;
				}


				if(i == iRecords/100)
				{
					RunThread_Heartbeat(RunThread_GetId(NULL));
				}
			}
				
			
		}
	
		pFullHisAlarmRecord = pTempFullHisAlarmRecord ;
		pHisAlarmRecord = pTempHisAlarmRecord ;
		*ppBuf = (void *)pFullHisAlarmRecord;
		DELETE(pHisAlarmRecord);

		DAT_StorageClose(hAlarmData);


		//TRACE("Current Alarm Records is %d.\n\n",iRecords);
		return iMatchRecord;
	}
	else
	{
		DELETE(pHisAlarmRecord);
		pHisAlarmRecord = NULL;
		DAT_StorageClose(hAlarmData);
		return FALSE;
	}
}




/*==========================================================================*
 * FUNCTION :    Web_QueryControlCommand
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN time_t fromTime:
				IN time_t toTime:
				OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_QueryControlCommand(IN time_t fromTime,
								   IN time_t toTime, 
								   OUT void **ppBuf)
{
	
	int			iResult = 0, iStartRecordNo = -1;		//record number
	HANDLE		hHisCommand;
	int			iBufLen = MAX_CTRL_CMD_COUNT;//400;

	hHisCommand =	DAT_StorageOpen(CTRL_CMD_LOG);
	
	if(hHisCommand == NULL)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open HisCommand");
		return FALSE;
	}

	HIS_CONTROL_RECORD *pHisCommandRecord	= NEW(HIS_CONTROL_RECORD, iBufLen);
	HIS_GENERAL_CONDITION stCondition;

	stCondition.tmToTime	= toTime;
	stCondition.tmFromTime	= fromTime;
	stCondition.iEquipID	= -1;	//all equipment 
	stCondition.iDataType	= QUERY_CONTROLCOMMAND_DATA;
	


	if(pHisCommandRecord == NULL )
	{
		DAT_StorageClose(hHisCommand);
		return FALSE;
	}
	int			iRecords = MAX_CTRL_CMD_COUNT;//400;
	iResult = DAT_StorageFindRecords(hHisCommand,
									Web_fnCompareQueryCondition, 
									(void *)&stCondition,
									&iStartRecordNo, 
									&iRecords, 
									(void*)pHisCommandRecord, 
									0,
									FALSE);

#ifdef _SHOW_WEB_INFO
	//TRACE("iResult : %d  iRecords :[%d]  iStartRecordNo[%d]\n", iResult, iRecords,iStartRecordNo);
#endif
	
	if(iResult >= 1 && iRecords >= 1)
	{
		*ppBuf = (void *)pHisCommandRecord;
		DAT_StorageClose(hHisCommand);
		return iRecords;
	}
	else
	{
		DELETE(pHisCommandRecord);
		pHisCommandRecord = NULL;
		DAT_StorageClose(hHisCommand);
		return FALSE;
	}
}



/*==========================================================================*
* FUNCTION :    Web_MakeQueryBatteryTestLogBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iQueryTimes:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-6-19 11:20
*==========================================================================*/
/*==========================================================================*
 * FUNCTION :    Web_MakeQueryBatteryTestLogBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN int iQueryTimes:
				OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
 char szBatterTitle[861][32] = {"Battery1 Current",	
				"Battery1 Voltage",	
				"Battery1 Capacity",	
				"Battery2 Current",	
				"Battery2 Voltage",	
				"Battery2 Capacity",	
				"EIB1Battery1 Current",	
				"EIB1Battery1 Voltage",	
				"EIB1Battery1 Capacity",	
				"EIB1Battery2 Current",	
				"EIB1Battery2 Voltage",	
				"EIB1Battery2 Capacity",	
				"EIB2Battery1 Current",	
				"EIB2Battery1 Voltage",	
				"EIB2Battery1 Capacity",	
				"EIB2Battery2 Current",	
				"EIB2Battery2 Voltage",	
				"EIB2Battery2 Capacity",	
				"EIB3Battery1 Current",	
				"EIB3Battery1 Voltage",	
				"EIB3Battery1 Capacity",	
				"EIB3Battery2 Current",	
				"EIB3Battery2 Voltage",	
				"EIB3Battery2 Capacity",	
				"EIB4Battery1 Current",	
				"EIB4Battery1 Voltage",	
				"EIB4Battery1 Capacity",	
				"EIB4Battery2 Current",	
				"EIB4Battery2 Voltage",	
				"EIB4Battery2 Capacity",	
				"SMDU1Battery1 Current",	
				"SMDU1Battery1 Voltage",	
				"SMDU1Battery1 Capacity",	
				"SMDU1Battery2 Current",	
				"SMDU1Battery2 Voltage",	
				"SMDU1Battery2 Capacity",	
				"SMDU1Battery3 Current",	
				"SMDU1Battery3 Voltage",	
				"SMDU1Battery3 Capacity",	
				"SMDU1Battery4 Current",	
				"SMDU1Battery4 Voltage",	
				"SMDU1Battery4 Capacity",	
				"SMDU2Battery1 Current",	
				"SMDU2Battery1 Voltage",	
				"SMDU2Battery1 Capacity",	
				"SMDU2Battery2 Current",	
				"SMDU2Battery2 Voltage",	
				"SMDU2Battery2 Capacity",	
				"SMDU2Battery3 Current",	
				"SMDU2Battery3 Voltage",	
				"SMDU2Battery3 Capacity",	
				"SMDU2Battery4 Current",	
				"SMDU2Battery4 Voltage",	
				"SMDU2Battery4 Capacity",	
				"SMDU3Battery1 Current",	
				"SMDU3Battery1 Voltage",	
				"SMDU3Battery1 Capacity",
				"SMDU3Battery2 Current",	
				"SMDU3Battery2 Voltage",	
				"SMDU3Battery2 Capacity",	
				"SMDU3Battery3 Current",	
				"SMDU3Battery3 Voltage",	
				"SMDU3Battery3 Capacity",	
				"SMDU3Battery4 Current",	
				"SMDU3Battery4 Voltage",	
				"SMDU3Battery4 Capacity",	
				"SMDU4Battery1 Current",	
				"SMDU4Battery1 Voltage",	
				"SMDU4Battery1 Capacity",	
				"SMDU4Battery2 Current",	
				"SMDU4Battery2 Voltage",	
				"SMDU4Battery2 Capacity",	
				"SMDU4Battery3 Current",	
				"SMDU4Battery3 Voltage",	
				"SMDU4Battery3 Capacity",	
				"SMDU4Battery4 Current",	
				"SMDU4Battery4 Voltage",	
				"SMDU4Battery4 Capacity",	
				"SMDU5Battery1 Current",	
				"SMDU5Battery1 Voltage",	
				"SMDU5Battery1 Capacity",	
				"SMDU5Battery2 Current",	
				"SMDU5Battery2 Voltage",	
				"SMDU5Battery2 Capacity",	
				"SMDU5Battery3 Current",	
				"SMDU5Battery3 Voltage",	
				"SMDU5Battery3 Capacity",	
				"SMDU5Battery4 Current",	
				"SMDU5Battery4 Voltage",	
				"SMDU5Battery4 Capacity",	
				"SMDU6Battery1 Current",	
				"SMDU6Battery1 Voltage",	
				"SMDU6Battery1 Capacity",	
				"SMDU6Battery2 Current",	
				"SMDU6Battery2 Voltage",	
				"SMDU6Battery2 Capacity",	
				"SMDU6Battery3 Current",	
				"SMDU6Battery3 Voltage",	
				"SMDU6Battery3 Capacity",	
				"SMDU6Battery4 Current",	
				"SMDU6Battery4 Voltage",	
				"SMDU6Battery4 Capacity",	
				"SMDU7Battery1 Current",	
				"SMDU7Battery1 Voltage",	
				"SMDU7Battery1 Capacity",	
				"SMDU7Battery2 Current",	
				"SMDU7Battery2 Voltage",	
				"SMDU7Battery2 Capacity",	
				"SMDU7Battery3 Current",	
				"SMDU7Battery3 Voltage",	
				"SMDU7Battery3 Capacity",	
				"SMDU7Battery4 Current",	
				"SMDU7Battery4 Voltage",	
				"SMDU7Battery4 Capacity",	
				"SMDU8Battery1 Current",	
				"SMDU8Battery1 Voltage",	
				"SMDU8Battery1 Capacity",	
				"SMDU8Battery2 Current",	
				"SMDU8Battery2 Voltage",	
				"SMDU8Battery2 Capacity",	
				"SMDU8Battery3 Current",	
				"SMDU8Battery3 Voltage",	
				"SMDU8Battery3 Capacity",	
				"SMDU8Battery4 Current",	
				"SMDU8Battery4 Voltage",	
				"SMDU8Battery4 Capacity",	
				"EIB1Battery3 Current",	
				"EIB1Battery3 Voltage",	
				"EIB1Battery3 Capacity",	
				"EIB2Battery3 Current",	
				"EIB2Battery3 Voltage",	
				"EIB2Battery3 Capacity",	
				"EIB3Battery3 Current",	
				"EIB3Battery3 Voltage",	
				"EIB3Battery3 Capacity",	
				"EIB4Battery3 Current",	
				"EIB4Battery3 Voltage",	
				"EIB4Battery3 Capacity",//�����ӵ���豸begin
				"SMBattery1 Current",
				"SMBattery1 Voltage",
				"SMBattery1 Capacity",
				"SMBattery2 Current",
				"SMBattery2 Voltage",
				"SMBattery2 Capacity",
				"SMBattery3 Current",
				"SMBattery3 Voltage",
				"SMBattery3 Capacity",
				"SMBattery4 Current",
				"SMBattery4 Voltage",
				"SMBattery4 Capacity",
				"SMBattery5 Current",
				"SMBattery5 Voltage",
				"SMBattery5 Capacity",
				"SMBattery6 Current",
				"SMBattery6 Voltage",
				"SMBattery6 Capacity",
				"SMBattery7 Current",
				"SMBattery7 Voltage",
				"SMBattery7 Capacity",
				"SMBattery8 Current",
				"SMBattery8 Voltage",
				"SMBattery8 Capacity",
				"SMBattery9 Current",
				"SMBattery9 Voltage",
				"SMBattery9 Capacity",
				"SMBattery10 Current",
				"SMBattery10 Voltage",
				"SMBattery10 Capacity",
				"SMBattery11 Current",
				"SMBattery11 Voltage",
				"SMBattery11 Capacity",
				"SMBattery12 Current",
				"SMBattery12 Voltage",
				"SMBattery12 Capacity",
				"SMBattery13 Current",
				"SMBattery13 Voltage",
				"SMBattery13 Capacity",
				"SMBattery14 Current",
				"SMBattery14 Voltage",
				"SMBattery14 Capacity",
				"SMBattery15 Current",
				"SMBattery15 Voltage",
				"SMBattery15 Capacity",
				"SMBattery16 Current",
				"SMBattery16 Voltage",
				"SMBattery16 Capacity",
				"SMBattery17 Current",
				"SMBattery17 Voltage",
				"SMBattery17 Capacity",
				"SMBattery18 Current",
				"SMBattery18 Voltage",
				"SMBattery18 Capacity",
				"SMBattery19 Current",
				"SMBattery19 Voltage",
				"SMBattery19 Capacity",
				"SMBattery20 Current",
				"SMBattery20 Voltage",
				"SMBattery20 Capacity",//�����ӵ���豸end
				"LargDUBattery1 Current",	
				"LargDUBattery1 Voltage",	
				"LargDUBattery1 Capacity",
				"LargDUBattery2 Current",	
				"LargDUBattery2 Voltage",	
				"LargDUBattery2 Capacity",
				"LargDUBattery3 Current",	
				"LargDUBattery3 Voltage",	
				"LargDUBattery3 Capacity",
				"LargDUBattery4 Current",	
				"LargDUBattery4 Voltage",	
				"LargDUBattery4 Capacity",
				"LargDUBattery5 Current",	
				"LargDUBattery5 Voltage",	
				"LargDUBattery5 Capacity",
				"LargDUBattery6 Current",	
				"LargDUBattery6 Voltage",	
				"LargDUBattery6 Capacity",
				"LargDUBattery7 Current",	
				"LargDUBattery7 Voltage",	
				"LargDUBattery7 Capacity",
				"LargDUBattery8 Current",	
				"LargDUBattery8 Voltage",	
				"LargDUBattery8 Capacity",
				"LargDUBattery9 Current",	
				"LargDUBattery9 Voltage",	
				"LargDUBattery9 Capacity",
				"LargDUBattery10 Current",	
				"LargDUBattery10 Voltage",	
				"LargDUBattery10 Capacity",
				"LargDUBattery11 Current",	
				"LargDUBattery11 Voltage",	
				"LargDUBattery11 Capacity",
				"LargDUBattery12 Current",	
				"LargDUBattery12 Voltage",	
				"LargDUBattery12 Capacity",
				"LargDUBattery13 Current",	
				"LargDUBattery13 Voltage",	
				"LargDUBattery13 Capacity",
				"LargDUBattery14 Current",	
				"LargDUBattery14 Voltage",	
				"LargDUBattery14 Capacity",
				"LargDUBattery15 Current",	
				"LargDUBattery15 Voltage",	
				"LargDUBattery15 Capacity",
				"LargDUBattery16 Current",	
				"LargDUBattery16 Voltage",	
				"LargDUBattery16 Capacity",
				"LargDUBattery17 Current",	
				"LargDUBattery17 Voltage",	
				"LargDUBattery17 Capacity",
				"LargDUBattery18 Current",	
				"LargDUBattery18 Voltage",	
				"LargDUBattery18 Capacity",
				"LargDUBattery19 Current",	
				"LargDUBattery19 Voltage",	
				"LargDUBattery19 Capacity",
				"LargDUBattery20 Current",	
				"LargDUBattery20 Voltage",	
				"LargDUBattery20 Capacity",				//�����ӵ���豸begin
				"SMDU1Battery5 Current",
				"SMDU1Battery5 Voltage",
				"SMDU1Battery5 Capacity",
				"SMDU2Battery5 Current",
				"SMDU2Battery5 Voltage",
				"SMDU2Battery5 Capacity",
				"SMDU3Battery5 Current",
				"SMDU3Battery5 Voltage",
				"SMDU3Battery5 Capacity",
				"SMDU4Battery5 Current",
				"SMDU4Battery5 Voltage",
				"SMDU4Battery5 Capacity",
				"SMDU5Battery5 Current",
				"SMDU5Battery5 Voltage",
				"SMDU5Battery5 Capacity",
				"SMDU6Battery5 Current",
				"SMDU6Battery5 Voltage",
				"SMDU6Battery5 Capacity",
				"SMDU7Battery5 Current",
				"SMDU7Battery5 Voltage",
				"SMDU7Battery5 Capacity",
				"SMDU8Battery5 Current",
				"SMDU8Battery5 Voltage",
				"SMDU8Battery5 Capacity",
				"SMBRCBattery1 Current",
				"SMBRCBattery1 Voltage",
				"SMBRCBattery1 Capacity",
				"SMBRCBattery2 Current",
				"SMBRCBattery2 Voltage",
				"SMBRCBattery2 Capacity",
				"SMBRCBattery3 Current",
				"SMBRCBattery3 Voltage",
				"SMBRCBattery3 Capacity",
				"SMBRCBattery4 Current",
				"SMBRCBattery4 Voltage",
				"SMBRCBattery4 Capacity",
				"SMBRCBattery5 Current",
				"SMBRCBattery5 Voltage",
				"SMBRCBattery5 Capacity",
				"SMBRCBattery6 Current",
				"SMBRCBattery6 Voltage",
				"SMBRCBattery6 Capacity",
				"SMBRCBattery7 Current",
				"SMBRCBattery7 Voltage",
				"SMBRCBattery7 Capacity",
				"SMBRCBattery8 Current",
				"SMBRCBattery8 Voltage",
				"SMBRCBattery8 Capacity",
				"SMBRCBattery9 Current",
				"SMBRCBattery9 Voltage",
				"SMBRCBattery9 Capacity",
				"SMBRCBattery10 Current",
				"SMBRCBattery10 Voltage",
				"SMBRCBattery10 Capacity",
				"SMBRCBattery11 Current",
				"SMBRCBattery11 Voltage",
				"SMBRCBattery11 Capacity",
				"SMBRCBattery12 Current",
				"SMBRCBattery12 Voltage",
				"SMBRCBattery12 Capacity",
				"SMBRCBattery13 Current",
				"SMBRCBattery13 Voltage",
				"SMBRCBattery13 Capacity",
				"SMBRCBattery14 Current",
				"SMBRCBattery14 Voltage",
				"SMBRCBattery14 Capacity",
				"SMBRCBattery15 Current",
				"SMBRCBattery15 Voltage",
				"SMBRCBattery15 Capacity",
				"SMBRCBattery16 Current",
				"SMBRCBattery16 Voltage",
				"SMBRCBattery16 Capacity",
				"SMBRCBattery17 Current",
				"SMBRCBattery17 Voltage",
				"SMBRCBattery17 Capacity",
				"SMBRCBattery18 Current",
				"SMBRCBattery18 Voltage",
				"SMBRCBattery18 Capacity",
				"SMBRCBattery19 Current",
				"SMBRCBattery19 Voltage",
				"SMBRCBattery19 Capacity",
				"SMBRCBattery20 Current",
				"SMBRCBattery20 Voltage",
				"SMBRCBattery20 Capacity",//�����ӵ���豸end
				"SMBAT/BRC1 BLOCK1 Voltage",
				"SMBAT/BRC1 BLOCK2 Voltage",
				"SMBAT/BRC1 BLOCK3 Voltage",
				"SMBAT/BRC1 BLOCK4 Voltage",
				"SMBAT/BRC1 BLOCK5 Voltage",
				"SMBAT/BRC1 BLOCK6 Voltage",
				"SMBAT/BRC1 BLOCK7 Voltage",
				"SMBAT/BRC1 BLOCK8 Voltage",
				"SMBAT/BRC1 BLOCK9 Voltage",
				"SMBAT/BRC1 BLOCK10 Voltage",
				"SMBAT/BRC1 BLOCK11 Voltage",
				"SMBAT/BRC1 BLOCK12 Voltage",
				"SMBAT/BRC1 BLOCK13 Voltage",
				"SMBAT/BRC1 BLOCK14 Voltage",
				"SMBAT/BRC1 BLOCK15 Voltage",
				"SMBAT/BRC1 BLOCK16 Voltage",
				"SMBAT/BRC1 BLOCK17 Voltage",
				"SMBAT/BRC1 BLOCK18 Voltage",
				"SMBAT/BRC1 BLOCK19 Voltage",
				"SMBAT/BRC1 BLOCK20 Voltage",
				"SMBAT/BRC1 BLOCK21 Voltage",
				"SMBAT/BRC1 BLOCK22 Voltage",
				"SMBAT/BRC1 BLOCK23 Voltage",
				"SMBAT/BRC1 BLOCK24 Voltage",
				"SMBAT/BRC2 BLOCK1 Voltage",
				"SMBAT/BRC2 BLOCK2 Voltage",
				"SMBAT/BRC2 BLOCK3 Voltage",
				"SMBAT/BRC2 BLOCK4 Voltage",
				"SMBAT/BRC2 BLOCK5 Voltage",
				"SMBAT/BRC2 BLOCK6 Voltage",
				"SMBAT/BRC2 BLOCK7 Voltage",
				"SMBAT/BRC2 BLOCK8 Voltage",
				"SMBAT/BRC2 BLOCK9 Voltage",
				"SMBAT/BRC2 BLOCK10 Voltage",
				"SMBAT/BRC2 BLOCK11 Voltage",
				"SMBAT/BRC2 BLOCK12 Voltage",
				"SMBAT/BRC2 BLOCK13 Voltage",
				"SMBAT/BRC2 BLOCK14 Voltage",
				"SMBAT/BRC2 BLOCK15 Voltage",
				"SMBAT/BRC2 BLOCK16 Voltage",
				"SMBAT/BRC2 BLOCK17 Voltage",
				"SMBAT/BRC2 BLOCK18 Voltage",
				"SMBAT/BRC2 BLOCK19 Voltage",
				"SMBAT/BRC2 BLOCK20 Voltage",
				"SMBAT/BRC2 BLOCK21 Voltage",
				"SMBAT/BRC2 BLOCK22 Voltage",
				"SMBAT/BRC2 BLOCK23 Voltage",
				"SMBAT/BRC2 BLOCK24 Voltage",
				"SMBAT/BRC3 BLOCK1 Voltage",
				"SMBAT/BRC3 BLOCK2 Voltage",
				"SMBAT/BRC3 BLOCK3 Voltage",
				"SMBAT/BRC3 BLOCK4 Voltage",
				"SMBAT/BRC3 BLOCK5 Voltage",
				"SMBAT/BRC3 BLOCK6 Voltage",
				"SMBAT/BRC3 BLOCK7 Voltage",
				"SMBAT/BRC3 BLOCK8 Voltage",
				"SMBAT/BRC3 BLOCK9 Voltage",
				"SMBAT/BRC3 BLOCK10 Voltage",
				"SMBAT/BRC3 BLOCK11 Voltage",
				"SMBAT/BRC3 BLOCK12 Voltage",
				"SMBAT/BRC3 BLOCK13 Voltage",
				"SMBAT/BRC3 BLOCK14 Voltage",
				"SMBAT/BRC3 BLOCK15 Voltage",
				"SMBAT/BRC3 BLOCK16 Voltage",
				"SMBAT/BRC3 BLOCK17 Voltage",
				"SMBAT/BRC3 BLOCK18 Voltage",
				"SMBAT/BRC3 BLOCK19 Voltage",
				"SMBAT/BRC3 BLOCK20 Voltage",
				"SMBAT/BRC3 BLOCK21 Voltage",
				"SMBAT/BRC3 BLOCK22 Voltage",
				"SMBAT/BRC3 BLOCK23 Voltage",
				"SMBAT/BRC3 BLOCK24 Voltage",
				"SMBAT/BRC4 BLOCK1 Voltage",
				"SMBAT/BRC4 BLOCK2 Voltage",
				"SMBAT/BRC4 BLOCK3 Voltage",
				"SMBAT/BRC4 BLOCK4 Voltage",
				"SMBAT/BRC4 BLOCK5 Voltage",
				"SMBAT/BRC4 BLOCK6 Voltage",
				"SMBAT/BRC4 BLOCK7 Voltage",
				"SMBAT/BRC4 BLOCK8 Voltage",
				"SMBAT/BRC4 BLOCK9 Voltage",
				"SMBAT/BRC4 BLOCK10 Voltage",
				"SMBAT/BRC4 BLOCK11 Voltage",
				"SMBAT/BRC4 BLOCK12 Voltage",
				"SMBAT/BRC4 BLOCK13 Voltage",
				"SMBAT/BRC4 BLOCK14 Voltage",
				"SMBAT/BRC4 BLOCK15 Voltage",
				"SMBAT/BRC4 BLOCK16 Voltage",
				"SMBAT/BRC4 BLOCK17 Voltage",
				"SMBAT/BRC4 BLOCK18 Voltage",
				"SMBAT/BRC4 BLOCK19 Voltage",
				"SMBAT/BRC4 BLOCK20 Voltage",
				"SMBAT/BRC4 BLOCK21 Voltage",
				"SMBAT/BRC4 BLOCK22 Voltage",
				"SMBAT/BRC4 BLOCK23 Voltage",
				"SMBAT/BRC4 BLOCK24 Voltage",
				"SMBAT/BRC5 BLOCK1 Voltage",
				"SMBAT/BRC5 BLOCK2 Voltage",
				"SMBAT/BRC5 BLOCK3 Voltage",
				"SMBAT/BRC5 BLOCK4 Voltage",
				"SMBAT/BRC5 BLOCK5 Voltage",
				"SMBAT/BRC5 BLOCK6 Voltage",
				"SMBAT/BRC5 BLOCK7 Voltage",
				"SMBAT/BRC5 BLOCK8 Voltage",
				"SMBAT/BRC5 BLOCK9 Voltage",
				"SMBAT/BRC5 BLOCK10 Voltage",
				"SMBAT/BRC5 BLOCK11 Voltage",
				"SMBAT/BRC5 BLOCK12 Voltage",
				"SMBAT/BRC5 BLOCK13 Voltage",
				"SMBAT/BRC5 BLOCK14 Voltage",
				"SMBAT/BRC5 BLOCK15 Voltage",
				"SMBAT/BRC5 BLOCK16 Voltage",
				"SMBAT/BRC5 BLOCK17 Voltage",
				"SMBAT/BRC5 BLOCK18 Voltage",
				"SMBAT/BRC5 BLOCK19 Voltage",
				"SMBAT/BRC5 BLOCK20 Voltage",
				"SMBAT/BRC5 BLOCK21 Voltage",
				"SMBAT/BRC5 BLOCK22 Voltage",
				"SMBAT/BRC5 BLOCK23 Voltage",
				"SMBAT/BRC5 BLOCK24 Voltage",
				"SMBAT/BRC6 BLOCK1 Voltage",
				"SMBAT/BRC6 BLOCK2 Voltage",
				"SMBAT/BRC6 BLOCK3 Voltage",
				"SMBAT/BRC6 BLOCK4 Voltage",
				"SMBAT/BRC6 BLOCK5 Voltage",
				"SMBAT/BRC6 BLOCK6 Voltage",
				"SMBAT/BRC6 BLOCK7 Voltage",
				"SMBAT/BRC6 BLOCK8 Voltage",
				"SMBAT/BRC6 BLOCK9 Voltage",
				"SMBAT/BRC6 BLOCK10 Voltage",
				"SMBAT/BRC6 BLOCK11 Voltage",
				"SMBAT/BRC6 BLOCK12 Voltage",
				"SMBAT/BRC6 BLOCK13 Voltage",
				"SMBAT/BRC6 BLOCK14 Voltage",
				"SMBAT/BRC6 BLOCK15 Voltage",
				"SMBAT/BRC6 BLOCK16 Voltage",
				"SMBAT/BRC6 BLOCK17 Voltage",
				"SMBAT/BRC6 BLOCK18 Voltage",
				"SMBAT/BRC6 BLOCK19 Voltage",
				"SMBAT/BRC6 BLOCK20 Voltage",
				"SMBAT/BRC6 BLOCK21 Voltage",
				"SMBAT/BRC6 BLOCK22 Voltage",
				"SMBAT/BRC6 BLOCK23 Voltage",
				"SMBAT/BRC6 BLOCK24 Voltage",
				"SMBAT/BRC7 BLOCK1 Voltage",
				"SMBAT/BRC7 BLOCK2 Voltage",
				"SMBAT/BRC7 BLOCK3 Voltage",
				"SMBAT/BRC7 BLOCK4 Voltage",
				"SMBAT/BRC7 BLOCK5 Voltage",
				"SMBAT/BRC7 BLOCK6 Voltage",
				"SMBAT/BRC7 BLOCK7 Voltage",
				"SMBAT/BRC7 BLOCK8 Voltage",
				"SMBAT/BRC7 BLOCK9 Voltage",
				"SMBAT/BRC7 BLOCK10 Voltage",
				"SMBAT/BRC7 BLOCK11 Voltage",
				"SMBAT/BRC7 BLOCK12 Voltage",
				"SMBAT/BRC7 BLOCK13 Voltage",
				"SMBAT/BRC7 BLOCK14 Voltage",
				"SMBAT/BRC7 BLOCK15 Voltage",
				"SMBAT/BRC7 BLOCK16 Voltage",
				"SMBAT/BRC7 BLOCK17 Voltage",
				"SMBAT/BRC7 BLOCK18 Voltage",
				"SMBAT/BRC7 BLOCK19 Voltage",
				"SMBAT/BRC7 BLOCK20 Voltage",
				"SMBAT/BRC7 BLOCK21 Voltage",
				"SMBAT/BRC7 BLOCK22 Voltage",
				"SMBAT/BRC7 BLOCK23 Voltage",
				"SMBAT/BRC7 BLOCK24 Voltage",
				"SMBAT/BRC8 BLOCK1 Voltage",
				"SMBAT/BRC8 BLOCK2 Voltage",
				"SMBAT/BRC8 BLOCK3 Voltage",
				"SMBAT/BRC8 BLOCK4 Voltage",
				"SMBAT/BRC8 BLOCK5 Voltage",
				"SMBAT/BRC8 BLOCK6 Voltage",
				"SMBAT/BRC8 BLOCK7 Voltage",
				"SMBAT/BRC8 BLOCK8 Voltage",
				"SMBAT/BRC8 BLOCK9 Voltage",
				"SMBAT/BRC8 BLOCK10 Voltage",
				"SMBAT/BRC8 BLOCK11 Voltage",
				"SMBAT/BRC8 BLOCK12 Voltage",
				"SMBAT/BRC8 BLOCK13 Voltage",
				"SMBAT/BRC8 BLOCK14 Voltage",
				"SMBAT/BRC8 BLOCK15 Voltage",
				"SMBAT/BRC8 BLOCK16 Voltage",
				"SMBAT/BRC8 BLOCK17 Voltage",
				"SMBAT/BRC8 BLOCK18 Voltage",
				"SMBAT/BRC8 BLOCK19 Voltage",
				"SMBAT/BRC8 BLOCK20 Voltage",
				"SMBAT/BRC8 BLOCK21 Voltage",
				"SMBAT/BRC8 BLOCK22 Voltage",
				"SMBAT/BRC8 BLOCK23 Voltage",
				"SMBAT/BRC8 BLOCK24 Voltage",
				"SMBAT/BRC9 BLOCK1 Voltage",
				"SMBAT/BRC9 BLOCK2 Voltage",
				"SMBAT/BRC9 BLOCK3 Voltage",
				"SMBAT/BRC9 BLOCK4 Voltage",
				"SMBAT/BRC9 BLOCK5 Voltage",
				"SMBAT/BRC9 BLOCK6 Voltage",
				"SMBAT/BRC9 BLOCK7 Voltage",
				"SMBAT/BRC9 BLOCK8 Voltage",
				"SMBAT/BRC9 BLOCK9 Voltage",
				"SMBAT/BRC9 BLOCK10 Voltage",
				"SMBAT/BRC9 BLOCK11 Voltage",
				"SMBAT/BRC9 BLOCK12 Voltage",
				"SMBAT/BRC9 BLOCK13 Voltage",
				"SMBAT/BRC9 BLOCK14 Voltage",
				"SMBAT/BRC9 BLOCK15 Voltage",
				"SMBAT/BRC9 BLOCK16 Voltage",
				"SMBAT/BRC9 BLOCK17 Voltage",
				"SMBAT/BRC9 BLOCK18 Voltage",
				"SMBAT/BRC9 BLOCK19 Voltage",
				"SMBAT/BRC9 BLOCK20 Voltage",
				"SMBAT/BRC9 BLOCK21 Voltage",
				"SMBAT/BRC9 BLOCK22 Voltage",
				"SMBAT/BRC9 BLOCK23 Voltage",
				"SMBAT/BRC9 BLOCK24 Voltage",
				"SMBAT/BRC10 BLOCK1 Voltage",
				"SMBAT/BRC10 BLOCK2 Voltage",
				"SMBAT/BRC10 BLOCK3 Voltage",
				"SMBAT/BRC10 BLOCK4 Voltage",
				"SMBAT/BRC10 BLOCK5 Voltage",
				"SMBAT/BRC10 BLOCK6 Voltage",
				"SMBAT/BRC10 BLOCK7 Voltage",
				"SMBAT/BRC10 BLOCK8 Voltage",
				"SMBAT/BRC10 BLOCK9 Voltage",
				"SMBAT/BRC10 BLOCK10 Voltage",
				"SMBAT/BRC10 BLOCK11 Voltage",
				"SMBAT/BRC10 BLOCK12 Voltage",
				"SMBAT/BRC10 BLOCK13 Voltage",
				"SMBAT/BRC10 BLOCK14 Voltage",
				"SMBAT/BRC10 BLOCK15 Voltage",
				"SMBAT/BRC10 BLOCK16 Voltage",
				"SMBAT/BRC10 BLOCK17 Voltage",
				"SMBAT/BRC10 BLOCK18 Voltage",
				"SMBAT/BRC10 BLOCK19 Voltage",
				"SMBAT/BRC10 BLOCK20 Voltage",
				"SMBAT/BRC10 BLOCK21 Voltage",
				"SMBAT/BRC10 BLOCK22 Voltage",
				"SMBAT/BRC10 BLOCK23 Voltage",
				"SMBAT/BRC10 BLOCK24 Voltage",
				"SMBAT/BRC11 BLOCK1 Voltage",
				"SMBAT/BRC11 BLOCK2 Voltage",
				"SMBAT/BRC11 BLOCK3 Voltage",
				"SMBAT/BRC11 BLOCK4 Voltage",
				"SMBAT/BRC11 BLOCK5 Voltage",
				"SMBAT/BRC11 BLOCK6 Voltage",
				"SMBAT/BRC11 BLOCK7 Voltage",
				"SMBAT/BRC11 BLOCK8 Voltage",
				"SMBAT/BRC11 BLOCK9 Voltage",
				"SMBAT/BRC11 BLOCK10 Voltage",
				"SMBAT/BRC11 BLOCK11 Voltage",
				"SMBAT/BRC11 BLOCK12 Voltage",
				"SMBAT/BRC11 BLOCK13 Voltage",
				"SMBAT/BRC11 BLOCK14 Voltage",
				"SMBAT/BRC11 BLOCK15 Voltage",
				"SMBAT/BRC11 BLOCK16 Voltage",
				"SMBAT/BRC11 BLOCK17 Voltage",
				"SMBAT/BRC11 BLOCK18 Voltage",
				"SMBAT/BRC11 BLOCK19 Voltage",
				"SMBAT/BRC11 BLOCK20 Voltage",
				"SMBAT/BRC11 BLOCK21 Voltage",
				"SMBAT/BRC11 BLOCK22 Voltage",
				"SMBAT/BRC11 BLOCK23 Voltage",
				"SMBAT/BRC11 BLOCK24 Voltage",
				"SMBAT/BRC12 BLOCK1 Voltage",
				"SMBAT/BRC12 BLOCK2 Voltage",
				"SMBAT/BRC12 BLOCK3 Voltage",
				"SMBAT/BRC12 BLOCK4 Voltage",
				"SMBAT/BRC12 BLOCK5 Voltage",
				"SMBAT/BRC12 BLOCK6 Voltage",
				"SMBAT/BRC12 BLOCK7 Voltage",
				"SMBAT/BRC12 BLOCK8 Voltage",
				"SMBAT/BRC12 BLOCK9 Voltage",
				"SMBAT/BRC12 BLOCK10 Voltage",
				"SMBAT/BRC12 BLOCK11 Voltage",
				"SMBAT/BRC12 BLOCK12 Voltage",
				"SMBAT/BRC12 BLOCK13 Voltage",
				"SMBAT/BRC12 BLOCK14 Voltage",
				"SMBAT/BRC12 BLOCK15 Voltage",
				"SMBAT/BRC12 BLOCK16 Voltage",
				"SMBAT/BRC12 BLOCK17 Voltage",
				"SMBAT/BRC12 BLOCK18 Voltage",
				"SMBAT/BRC12 BLOCK19 Voltage",
				"SMBAT/BRC12 BLOCK20 Voltage",
				"SMBAT/BRC12 BLOCK21 Voltage",
				"SMBAT/BRC12 BLOCK22 Voltage",
				"SMBAT/BRC12 BLOCK23 Voltage",
				"SMBAT/BRC12 BLOCK24 Voltage",
				"SMBAT/BRC13 BLOCK1 Voltage",
				"SMBAT/BRC13 BLOCK2 Voltage",
				"SMBAT/BRC13 BLOCK3 Voltage",
				"SMBAT/BRC13 BLOCK4 Voltage",
				"SMBAT/BRC13 BLOCK5 Voltage",
				"SMBAT/BRC13 BLOCK6 Voltage",
				"SMBAT/BRC13 BLOCK7 Voltage",
				"SMBAT/BRC13 BLOCK8 Voltage",
				"SMBAT/BRC13 BLOCK9 Voltage",
				"SMBAT/BRC13 BLOCK10 Voltage",
				"SMBAT/BRC13 BLOCK11 Voltage",
				"SMBAT/BRC13 BLOCK12 Voltage",
				"SMBAT/BRC13 BLOCK13 Voltage",
				"SMBAT/BRC13 BLOCK14 Voltage",
				"SMBAT/BRC13 BLOCK15 Voltage",
				"SMBAT/BRC13 BLOCK16 Voltage",
				"SMBAT/BRC13 BLOCK17 Voltage",
				"SMBAT/BRC13 BLOCK18 Voltage",
				"SMBAT/BRC13 BLOCK19 Voltage",
				"SMBAT/BRC13 BLOCK20 Voltage",
				"SMBAT/BRC13 BLOCK21 Voltage",
				"SMBAT/BRC13 BLOCK22 Voltage",
				"SMBAT/BRC13 BLOCK23 Voltage",
				"SMBAT/BRC13 BLOCK24 Voltage",
				"SMBAT/BRC14 BLOCK1 Voltage",
				"SMBAT/BRC14 BLOCK2 Voltage",
				"SMBAT/BRC14 BLOCK3 Voltage",
				"SMBAT/BRC14 BLOCK4 Voltage",
				"SMBAT/BRC14 BLOCK5 Voltage",
				"SMBAT/BRC14 BLOCK6 Voltage",
				"SMBAT/BRC14 BLOCK7 Voltage",
				"SMBAT/BRC14 BLOCK8 Voltage",
				"SMBAT/BRC14 BLOCK9 Voltage",
				"SMBAT/BRC14 BLOCK10 Voltage",
				"SMBAT/BRC14 BLOCK11 Voltage",
				"SMBAT/BRC14 BLOCK12 Voltage",
				"SMBAT/BRC14 BLOCK13 Voltage",
				"SMBAT/BRC14 BLOCK14 Voltage",
				"SMBAT/BRC14 BLOCK15 Voltage",
				"SMBAT/BRC14 BLOCK16 Voltage",
				"SMBAT/BRC14 BLOCK17 Voltage",
				"SMBAT/BRC14 BLOCK18 Voltage",
				"SMBAT/BRC14 BLOCK19 Voltage",
				"SMBAT/BRC14 BLOCK20 Voltage",
				"SMBAT/BRC14 BLOCK21 Voltage",
				"SMBAT/BRC14 BLOCK22 Voltage",
				"SMBAT/BRC14 BLOCK23 Voltage",
				"SMBAT/BRC14 BLOCK24 Voltage",
				"SMBAT/BRC15 BLOCK1 Voltage",
				"SMBAT/BRC15 BLOCK2 Voltage",
				"SMBAT/BRC15 BLOCK3 Voltage",
				"SMBAT/BRC15 BLOCK4 Voltage",
				"SMBAT/BRC15 BLOCK5 Voltage",
				"SMBAT/BRC15 BLOCK6 Voltage",
				"SMBAT/BRC15 BLOCK7 Voltage",
				"SMBAT/BRC15 BLOCK8 Voltage",
				"SMBAT/BRC15 BLOCK9 Voltage",
				"SMBAT/BRC15 BLOCK10 Voltage",
				"SMBAT/BRC15 BLOCK11 Voltage",
				"SMBAT/BRC15 BLOCK12 Voltage",
				"SMBAT/BRC15 BLOCK13 Voltage",
				"SMBAT/BRC15 BLOCK14 Voltage",
				"SMBAT/BRC15 BLOCK15 Voltage",
				"SMBAT/BRC15 BLOCK16 Voltage",
				"SMBAT/BRC15 BLOCK17 Voltage",
				"SMBAT/BRC15 BLOCK18 Voltage",
				"SMBAT/BRC15 BLOCK19 Voltage",
				"SMBAT/BRC15 BLOCK20 Voltage",
				"SMBAT/BRC15 BLOCK21 Voltage",
				"SMBAT/BRC15 BLOCK22 Voltage",
				"SMBAT/BRC15 BLOCK23 Voltage",
				"SMBAT/BRC15 BLOCK24 Voltage",
				"SMBAT/BRC16 BLOCK1 Voltage",
				"SMBAT/BRC16 BLOCK2 Voltage",
				"SMBAT/BRC16 BLOCK3 Voltage",
				"SMBAT/BRC16 BLOCK4 Voltage",
				"SMBAT/BRC16 BLOCK5 Voltage",
				"SMBAT/BRC16 BLOCK6 Voltage",
				"SMBAT/BRC16 BLOCK7 Voltage",
				"SMBAT/BRC16 BLOCK8 Voltage",
				"SMBAT/BRC16 BLOCK9 Voltage",
				"SMBAT/BRC16 BLOCK10 Voltage",
				"SMBAT/BRC16 BLOCK11 Voltage",
				"SMBAT/BRC16 BLOCK12 Voltage",
				"SMBAT/BRC16 BLOCK13 Voltage",
				"SMBAT/BRC16 BLOCK14 Voltage",
				"SMBAT/BRC16 BLOCK15 Voltage",
				"SMBAT/BRC16 BLOCK16 Voltage",
				"SMBAT/BRC16 BLOCK17 Voltage",
				"SMBAT/BRC16 BLOCK18 Voltage",
				"SMBAT/BRC16 BLOCK19 Voltage",
				"SMBAT/BRC16 BLOCK20 Voltage",
				"SMBAT/BRC16 BLOCK21 Voltage",
				"SMBAT/BRC16 BLOCK22 Voltage",
				"SMBAT/BRC16 BLOCK23 Voltage",
				"SMBAT/BRC16 BLOCK24 Voltage",
				"SMBAT/BRC17 BLOCK1 Voltage",
				"SMBAT/BRC17 BLOCK2 Voltage",
				"SMBAT/BRC17 BLOCK3 Voltage",
				"SMBAT/BRC17 BLOCK4 Voltage",
				"SMBAT/BRC17 BLOCK5 Voltage",
				"SMBAT/BRC17 BLOCK6 Voltage",
				"SMBAT/BRC17 BLOCK7 Voltage",
				"SMBAT/BRC17 BLOCK8 Voltage",
				"SMBAT/BRC17 BLOCK9 Voltage",
				"SMBAT/BRC17 BLOCK10 Voltage",
				"SMBAT/BRC17 BLOCK11 Voltage",
				"SMBAT/BRC17 BLOCK12 Voltage",
				"SMBAT/BRC17 BLOCK13 Voltage",
				"SMBAT/BRC17 BLOCK14 Voltage",
				"SMBAT/BRC17 BLOCK15 Voltage",
				"SMBAT/BRC17 BLOCK16 Voltage",
				"SMBAT/BRC17 BLOCK17 Voltage",
				"SMBAT/BRC17 BLOCK18 Voltage",
				"SMBAT/BRC17 BLOCK19 Voltage",
				"SMBAT/BRC17 BLOCK20 Voltage",
				"SMBAT/BRC17 BLOCK21 Voltage",
				"SMBAT/BRC17 BLOCK22 Voltage",
				"SMBAT/BRC17 BLOCK23 Voltage",
				"SMBAT/BRC17 BLOCK24 Voltage",
				"SMBAT/BRC18 BLOCK1 Voltage",
				"SMBAT/BRC18 BLOCK2 Voltage",
				"SMBAT/BRC18 BLOCK3 Voltage",
				"SMBAT/BRC18 BLOCK4 Voltage",
				"SMBAT/BRC18 BLOCK5 Voltage",
				"SMBAT/BRC18 BLOCK6 Voltage",
				"SMBAT/BRC18 BLOCK7 Voltage",
				"SMBAT/BRC18 BLOCK8 Voltage",
				"SMBAT/BRC18 BLOCK9 Voltage",
				"SMBAT/BRC18 BLOCK10 Voltage",
				"SMBAT/BRC18 BLOCK11 Voltage",
				"SMBAT/BRC18 BLOCK12 Voltage",
				"SMBAT/BRC18 BLOCK13 Voltage",
				"SMBAT/BRC18 BLOCK14 Voltage",
				"SMBAT/BRC18 BLOCK15 Voltage",
				"SMBAT/BRC18 BLOCK16 Voltage",
				"SMBAT/BRC18 BLOCK17 Voltage",
				"SMBAT/BRC18 BLOCK18 Voltage",
				"SMBAT/BRC18 BLOCK19 Voltage",
				"SMBAT/BRC18 BLOCK20 Voltage",
				"SMBAT/BRC18 BLOCK21 Voltage",
				"SMBAT/BRC18 BLOCK22 Voltage",
				"SMBAT/BRC18 BLOCK23 Voltage",
				"SMBAT/BRC18 BLOCK24 Voltage",
				"SMBAT/BRC19 BLOCK1 Voltage",
				"SMBAT/BRC19 BLOCK2 Voltage",
				"SMBAT/BRC19 BLOCK3 Voltage",
				"SMBAT/BRC19 BLOCK4 Voltage",
				"SMBAT/BRC19 BLOCK5 Voltage",
				"SMBAT/BRC19 BLOCK6 Voltage",
				"SMBAT/BRC19 BLOCK7 Voltage",
				"SMBAT/BRC19 BLOCK8 Voltage",
				"SMBAT/BRC19 BLOCK9 Voltage",
				"SMBAT/BRC19 BLOCK10 Voltage",
				"SMBAT/BRC19 BLOCK11 Voltage",
				"SMBAT/BRC19 BLOCK12 Voltage",
				"SMBAT/BRC19 BLOCK13 Voltage",
				"SMBAT/BRC19 BLOCK14 Voltage",
				"SMBAT/BRC19 BLOCK15 Voltage",
				"SMBAT/BRC19 BLOCK16 Voltage",
				"SMBAT/BRC19 BLOCK17 Voltage",
				"SMBAT/BRC19 BLOCK18 Voltage",
				"SMBAT/BRC19 BLOCK19 Voltage",
				"SMBAT/BRC19 BLOCK20 Voltage",
				"SMBAT/BRC19 BLOCK21 Voltage",
				"SMBAT/BRC19 BLOCK22 Voltage",
				"SMBAT/BRC19 BLOCK23 Voltage",
				"SMBAT/BRC19 BLOCK24 Voltage",
				"SMBAT/BRC20 BLOCK1 Voltage",
				"SMBAT/BRC20 BLOCK2 Voltage",
				"SMBAT/BRC20 BLOCK3 Voltage",
				"SMBAT/BRC20 BLOCK4 Voltage",
				"SMBAT/BRC20 BLOCK5 Voltage",
				"SMBAT/BRC20 BLOCK6 Voltage",
				"SMBAT/BRC20 BLOCK7 Voltage",
				"SMBAT/BRC20 BLOCK8 Voltage",
				"SMBAT/BRC20 BLOCK9 Voltage",
				"SMBAT/BRC20 BLOCK10 Voltage",
				"SMBAT/BRC20 BLOCK11 Voltage",
				"SMBAT/BRC20 BLOCK12 Voltage",
				"SMBAT/BRC20 BLOCK13 Voltage",
				"SMBAT/BRC20 BLOCK14 Voltage",
				"SMBAT/BRC20 BLOCK15 Voltage",
				"SMBAT/BRC20 BLOCK16 Voltage",
				"SMBAT/BRC20 BLOCK17 Voltage",
				"SMBAT/BRC20 BLOCK18 Voltage",
				"SMBAT/BRC20 BLOCK19 Voltage",
				"SMBAT/BRC20 BLOCK20 Voltage",
				"SMBAT/BRC20 BLOCK21 Voltage",
				"SMBAT/BRC20 BLOCK22 Voltage",
				"SMBAT/BRC20 BLOCK23 Voltage",
				"SMBAT/BRC20 BLOCK24 Voltage",
				"EIB1Block1Voltage",	
				"EIB1Block2Voltage",	
				"EIB1Block3Voltage",	
				"EIB1Block4Voltage",	
				"EIB1Block5Voltage",	
				"EIB1Block6Voltage",	
				"EIB1Block7Voltage",	
				"EIB1Block8Voltage",	
				"EIB2Block1Voltage",	
				"EIB2Block2Voltage",	
				"EIB2Block3Voltage",	
				"EIB2Block4Voltage",	
				"EIB2Block5Voltage",	
				"EIB2Block6Voltage",	
				"EIB2Block7Voltage",	
				"EIB2Block8Voltage",	
				"EIB3Block1Voltage",	
				"EIB3Block2Voltage",	
				"EIB3Block3Voltage",	
				"EIB3Block4Voltage",	
				"EIB3Block5Voltage",	
				"EIB3Block6Voltage",	
				"EIB3Block7Voltage",	
				"EIB3Block8Voltage",	
				"EIB4Block1Voltage",	
				"EIB4Block2Voltage",	
				"EIB4Block3Voltage",	
				"EIB4Block4Voltage",	
				"EIB4Block5Voltage",	
				"EIB4Block6Voltage",	
				"EIB4Block7Voltage",	
				"EIB4Block8Voltage",	
				"Temperature1",	
				"Temperature2",		
				"Temperature3",		
				"Temperature4",		
				"Temperature5",		
				"Temperature6",		
				"Temperature7"};
static int Web_MakeQueryBatteryTestLogBuffer(IN int iQueryTimes, 
	OUT char **ppBuf)
{
	BATTERY_LOG_INFO		*pBatteryLogInfo	= NULL;
	BATTERY_HEAD_INFO		*stGeneralInfo = NULL;
	char					*pMakeBuf = NULL, *pFileMakeBuf = NULL;
	int						iLen = 0, iFileLen = 0;
	int						iQtyBatt, iQtyRecord;
	int						i = 0, k = 0, j = 0;
	char					szTime1[32], szTime2[32];
	int						iBatteryNum = 0;
	time_t					tmUTC;
	BATTERY_GENERAL_INFO			stOutBatteryGeneralInfo;
	int					iStatus[1024];
	BYTE					byTest[1024];


	char					szBatteryStartReason[5][128]={"Start test for it is battery best plan time",
		"Start test manually",
		"Start AC Fail test for AC fail",
		"Start test for Master Power start test",
		"Other reason"};
	char					szBatteryEndReason[12][128]={"Stop test manually",
		"Stop test for alarm",
		"Stop test for test time-out",
		"Stop test for capacity condition",
		"Stop test for voltage condition",
		"Stop test for AC fail",
		"Stop AC fail test for AC restore",
		"Stop AC fail test for disabled",
		"Stop test for Master Power stop test",
		"Stop a PowerSplit BT for Auto/Man turn to Man",
		"Stop PowerSplit Man-BT for Auto/Man turn to Auto",
		"Stop by other reason"};
	char					szBatteryTestResult[5][64]={"No test result",
		"Battery is OK",
		"Battery is bad",
		"It's a Power Split Test", 
		"Other result"};

	printf("Now inquire the battery log\n");
	if(Web_QueryBatteryTestLog(iQueryTimes, &stGeneralInfo, &pBatteryLogInfo, &stOutBatteryGeneralInfo) != TRUE)//ERR_WEB_COMM_GET_RESULT_FAIL)
	{
		return FALSE;
	}

	iQtyBatt	= stGeneralInfo->btSummary.iQtyBatt;
	iQtyRecord	= stGeneralInfo->btSummary.iQtyRecord;

	printf("iQtyBatt is %d, iQtyRecord is %d\n", iQtyBatt, iQtyRecord);

	pMakeBuf = NEW(char, 20 * MAX_QUERY_BATTERY_GROUP_LEN * MAX_QUERY_BATTERY_GROUP_NUM);
	pFileMakeBuf = NEW(char, 20 * MAX_QUERY_BATTERY_GROUP_LEN * MAX_QUERY_BATTERY_GROUP_NUM);
	if(pMakeBuf == NULL || pFileMakeBuf == NULL)
	{
		if(pBatteryLogInfo != NULL)
		{
			DELETE(pBatteryLogInfo);
			pBatteryLogInfo = NULL;
		}
		if(pMakeBuf != NULL)
		{
			DELETE(pMakeBuf);
			pMakeBuf = NULL;
		}
		if(pFileMakeBuf != NULL)
		{
			DELETE(pFileMakeBuf);
			pFileMakeBuf = NULL;
		}
		return FALSE;
	}




	iLen += sprintf(pMakeBuf + iLen,"    %d,%d,%d,%d,%d;",
		(int)stGeneralInfo->btSummary.tStartTime,//szTime1,
		(int)stGeneralInfo->btSummary.tEndTime,//szTime2,
		stGeneralInfo->btSummary.iStartReason,
		stGeneralInfo->btSummary.iEndReason,
		stGeneralInfo->btSummary.iTestResult);


	/*iFileLen += sprintf(pFileMakeBuf + iFileLen,"\t %d \t%d \t%d \t%d \t%d\n",
	(int)stGeneralInfo->btSummary.tStartTime,//szTime1,
	(int)stGeneralInfo->btSummary.tEndTime,//szTime2,
	stGeneralInfo->btSummary.iStartReason,
	stGeneralInfo->btSummary.iEndReason,
	stGeneralInfo->btSummary.iTestResult);*/

	//print test log status	
	iLen += sprintf(pMakeBuf + iLen,"%d,%d,%d,",1,1,1 );

	memset(byTest, 0x0, sizeof(byTest));
	memcpy(byTest, &stOutBatteryGeneralInfo, sizeof(byTest));
	for(i = 0; i < iQtyBatt; i++)
	{
		iLen += sprintf(pMakeBuf + iLen," %d,%d,%d,",(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCurrent == 1) ? 1 : 0, (stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryVolage == 1) ? 1 : 0 ,(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCapacity== 1) ? 1 : 0);
		iStatus[i*3 + 0] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCurrent == 1) ? 1 : 0;
		iStatus[i*3 + 1] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryVolage == 1) ? 1 : 0;
		iStatus[i*3 + 2] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCapacity == 1) ? 1 : 0;

	}

	for(i = 0; i < 20; i++)
	{
	    for(j = 0; j < 24; j++)
	    {
		iLen += sprintf(pMakeBuf + iLen,"%d,",(stOutBatteryGeneralInfo.stSmbatSmbrcBlock[i].bySMBlockVoltage[j] == 1) ? 1 : 0);
		iStatus[iQtyBatt * 3 + i * 24 + j] =(stOutBatteryGeneralInfo.stSmbatSmbrcBlock[i].bySMBlockVoltage[j] == 1) ? 1 : 0;
	    }
	}

	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 8; j++)
		{
			iLen += sprintf(pMakeBuf + iLen,"%d,",(stOutBatteryGeneralInfo.stBatteryBlock[i].byBlockVoltage[j] == 1) ? 1 : 0);
			iStatus[iQtyBatt * 3 + 480 + i*8 + j] =(stOutBatteryGeneralInfo.stBatteryBlock[i].byBlockVoltage[j] == 1) ? 1 : 0;
			//printf("Battery %d Block %d = %d",i, j, stOutBatteryGeneralInfo.stBatteryBlock[i].byBlockVoltage[j]);
			//iLen += sprintf(pMakeBuf + iLen,"%d,",(byTest[i + iQtyBatt * 3]== 1) ? 1 : 0);
			//iStatus[iQtyBatt*3 + i] =(byTest[i + iQtyBatt * 3]== 1) ? 1 : 0;
		}
	}

	for(i = 0; i < 7; i++)
	{
		if(i == 6)
		{
			iLen += sprintf(pMakeBuf + iLen,"%d;",(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0 );
		}
		else
		{
			iLen += sprintf(pMakeBuf + iLen,"%d,",(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0 );
		}
		iStatus[iQtyBatt * 3 + 480 + 32 + i] =(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0  ;

	}
	/*for(i = 0; i < 230; i++)
	{
	printf("iStatus[%d] = %d\n",i, iStatus[i]);
	}*/
	//print test result	
	for(i = 0; i < iQtyRecord ; i++)
	{	
		/*print the common information of every battery test record*/
		tmUTC = pBatteryLogInfo[i].tRecordTime;

		TimeToString(tmUTC, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));


		iLen += sprintf(pMakeBuf + iLen, "<tr><td rowspan='%d'>%d</td><td rowspan='%d'>\"+ makeSampleDateStr(%ld)+\"</td> <td rowspan='%d'>%8.2f</td>", 1, (i + 1), 1, tmUTC, 1, pBatteryLogInfo[i].fVoltage);
		iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t%6d \t%-32s \t%-8.2f ",  (i + 1), szTime1, pBatteryLogInfo[i].fVoltage);

		iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t\t%s"," ");
		for(k = 0; k < iQtyBatt; k++)		{


			if(iStatus[k*3 + 0])
			{
				iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatCurrent);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.2f \t",	
					pBatteryLogInfo[i].pBatteryRealData[k].fBatCurrent);
				//printf("fBatCurrent %d is %f\n", k, pBatteryLogInfo[i].pBatteryRealData[k].fBatCurrent);


			}
			if(iStatus[k*3 + 1])
			{
				iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatVoltage);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.2f \t",	
					pBatteryLogInfo[i].pBatteryRealData[k].fBatVoltage);
				//printf("fBatVoltage %d is %f\n", k, pBatteryLogInfo[i].pBatteryRealData[k].fBatVoltage);

			}

			if(iStatus[k*3 + 2])
			{
				iLen += sprintf(pMakeBuf + iLen, "<td>%8.0f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatCapacity);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.0f \t",	
					pBatteryLogInfo[i].pBatteryRealData[k].fBatCapacity);		
				//printf("fBatCapacity %d is %f\n", k, pBatteryLogInfo[i].pBatteryRealData[k].fBatCapacity);
			}			

		}

		for(j = 0; j < 480; j++)
		{
		    if(iStatus[iQtyBatt * 3 + j])
		    {
			iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td>", pBatteryLogInfo[i].fBatSMBlock[j / 24].fSMBlockVoltage[j % 24]);
			iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t%-22.2f ", pBatteryLogInfo[i].fBatSMBlock[j / 24].fSMBlockVoltage[j % 24]);
		    }


		}

		for(j = 0; j < 32; j++)
		{
			if(iStatus[iQtyBatt * 3 + 480 + j])
			{
				iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td>", pBatteryLogInfo[i].fBatBlock[j/8].fBatBlockVoltage[j%8]);
				iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t%-22.2f ", pBatteryLogInfo[i].fBatBlock[j/8].fBatBlockVoltage[j%8]);
			}


		}

		for(j = 0; j < 7; j++)
		{
			if(iStatus[iQtyBatt * 3 + 480 + 32 + j])
			{
				iLen += sprintf(pMakeBuf + iLen, " <td>%8.0f</td> ",pBatteryLogInfo[i].fTemp[j]);
				if(j == 0)
				{
					iFileLen += sprintf(pFileMakeBuf + iFileLen, " %-20.0f ",pBatteryLogInfo[i].fTemp[j]);
				}
				else
				{
					iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-20.0f ",pBatteryLogInfo[i].fTemp[j]);
				}

			}
		}



		iLen += sprintf(pMakeBuf + iLen, "</tr>");
		iFileLen += sprintf(pFileMakeBuf + iFileLen, "\n");

	}
	//iLen += sprintf(pMakeBuf + iLen, "");

	//TRACE("pFileMakeBuf:[%s]\n", pFileMakeBuf);

	*ppBuf = pMakeBuf;

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL)
	{

		TimeToString(stGeneralInfo->btSummary.tStartTime, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));

		TimeToString(stGeneralInfo->btSummary.tEndTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		char	szFileTitle[4096];
		iLen = sprintf(szFileTitle, "Query  Battery test log \n" \
			"Start time: %s \n" \
			"End time  : %s\n" \
			"Start reason   : %s\n" \
			"End reason     : %s\n" \
			"Test result    : %s\n\n\n" \
			"\t%6s \t%-32s \t%-16s",
			szTime1,
			szTime2,
			szBatteryStartReason[stGeneralInfo->btSummary.iStartReason],
			szBatteryEndReason[stGeneralInfo->btSummary.iEndReason],
			szBatteryTestResult[stGeneralInfo->btSummary.iTestResult],
			"Index",
			"Record time",
			"System voltage(V)");
		i = 0;						
		while(i < 861)
		{

			if(iStatus[i])
			{
				iLen += sprintf(szFileTitle + iLen, "\t%-20s ", szBatterTitle[i]);
				i++;
			}
			else
			{
				i++;
			}
		}						
		sprintf(szFileTitle + iLen, "%s ", "\n");						



		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pFileMakeBuf,strlen(pFileMakeBuf), 1, fp);
		//printf("szFileTitle is %s\n", szFileTitle);
		fclose(fp);
	}

	//printf("pMakeBuf is %s\n", pMakeBuf);
	//printf("pFileMakeBuf is %s\n", pFileMakeBuf);


	if(stGeneralInfo != NULL)
	{
		DELETE(stGeneralInfo);
		stGeneralInfo = NULL;
	}


	if(pBatteryLogInfo != NULL)
	{
		//for(i = 0; i < iQtyRecord && pBatteryLogInfo != NULL; i++)
		//{
		//	if(pBatteryLogInfo[i].pBatteryRealData != NULL)
		//	{
		//		DELETE(pBatteryLogInfo[i].pBatteryRealData);
		//		pBatteryLogInfo[i].pBatteryRealData = NULL;
		//	}
		//}
		DELETE(pBatteryLogInfo);
		pBatteryLogInfo = NULL;
	}

	//added by wankun in order to avoid mem leak
	if(pFileMakeBuf != NULL )
	{
		DELETE(pFileMakeBuf);
		pFileMakeBuf = NULL;
	}
	//ended by wankun
	return TRUE;
}




BOOL ConvertTime(time_t* pTime, BOOL bUTCToLocal)
{
	SIG_BASIC_VALUE* pSigValue;

	int nBufLen;

	if (DxiGetData(VAR_A_SIGNAL_VALUE,
		DXI_GetEquipIDFromStdEquipID(STD_ID_ACU_SYSTEM_EQUIP),			
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 25),
		&nBufLen,			
		&pSigValue,			
		0) != ERR_DXI_OK)
	{	
		//TRACE("FALSE\n");
		return FALSE;
	}

	ASSERT(pSigValue);	

	if(SIG_VALUE_IS_VALID(pSigValue))
	{
		int nTimeOffset = (pSigValue->varValue.lValue) * 3600 / 2;

		*pTime = 0;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}


static int Web_fnCompareBatteryCondition(IN const void *pRecord, 
										 IN const void *pCondition)
{
	/*Compare head*/
	UNUSED(pCondition);		
	return (stricmp(((BATTERY_HEAD_INFO *)pRecord)->szHeadInfo, 
						"Head of a Battery Test") == 0) ? TRUE : FALSE;
}
/*==========================================================================*
* FUNCTION :    Web_QueryBatteryTestLog
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iQueryTimes:
OUT BT_LOG_SUMMARY **ppGeneralInfo:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
/*Battery test Log */
static int Web_QueryBatteryTestLog(IN int iQueryTimes, 
								   OUT BATTERY_HEAD_INFO **ppGeneralInfo,
								   OUT BATTERY_LOG_INFO **ppBatteryRealData,
								   OUT  BATTERY_GENERAL_INFO	*stOutBatteryGeneralInfo)
{
	HANDLE					hBatteryTestLog = NULL;
	BATTERY_HEAD_INFO		*stBatteryHeadInfo = NULL;
	HIS_GENERAL_CONDITION	stCondition;
	BATTERY_DATA_RECORD		*pBatteryData = NULL;
	int						iBuf[10];
	int						iStartNO = -1;
	//int						iBuffLen = 0;
	int						iRecords = 10;
	int						iRecordNum = 0, iQtyBatt = 0, iQtyRecord = 0, iResult = -2;
	BATTERY_GENERAL_INFO	BatteryGeneralInfo;
	int i, j;
	int iRecordNumtemp, iStartNOtemp;


	/*for(i = 0; i < MAX_DATA_TYPES; i++)
	{
	    printf("The log name is %s\n", g_RECORD_SET_TRACK[i].DataName);
	    if(strcmp(BATT_TEST_LOG_FILE, g_RECORD_SET_TRACK[i].DataName) == 0)
	    {
		printf("iTotalSectors is %d, iMaxRecords is %d, iRecordSize is %d, iStartRecordNo is %d, bSectorEffect is %d, "\ 
		    "byCurrSectorNo is %d, iCurrWritePos is %d, iCurrReadPos is %d, byOldCurrSectorNo is %d, iDataRecordIDIndex is %d, "\
		    "byCurrSectorNo is %d, byNewSectorFlag is %d, iMaxRecordsPerSector is %d, iWritableRecords is %d, dwCurrRecordID is %d, "\
		    "byLogSectorNo is %d, byPhySectorNo is %d, byOldCurrSectorNo is %d\n", 
		    g_RECORD_SET_TRACK[i].iTotalSectors, g_RECORD_SET_TRACK[i].iMaxRecords, g_RECORD_SET_TRACK[i].iRecordSize, 
		    g_RECORD_SET_TRACK[i].iStartRecordNo, g_RECORD_SET_TRACK[i].bSectorEffect, g_RECORD_SET_TRACK[i].byCurrSectorNo, 
		    g_RECORD_SET_TRACK[i].iCurrWritePos, g_RECORD_SET_TRACK[i].iCurrReadPos, g_RECORD_SET_TRACK[i].byOldCurrSectorNo, 
		    g_RECORD_SET_TRACK[i].iDataRecordIDIndex, g_RECORD_SET_TRACK[i].byCurrSectorNo, g_RECORD_SET_TRACK[i].byNewSectorFlag,
		    g_RECORD_SET_TRACK[i].iMaxRecordsPerSector, g_RECORD_SET_TRACK[i].iWritableRecords, g_RECORD_SET_TRACK[i].dwCurrRecordID,
		    g_RECORD_SET_TRACK[i].pbyPhysicsSectorNo->byLogSectorNo, g_RECORD_SET_TRACK[i].pbyPhysicsSectorNo->byPhySectorNo,
		    g_RECORD_SET_TRACK[i].byOldCurrSectorNo);
		for (j = 0; j < MAX_DATA_TYPES; j++)
		{
		    if(g_HISDATA_QUEUE[j].iDataRecordIDIndex == g_RECORD_SET_TRACK[i].iDataRecordIDIndex)
		    {
			printf("g_HISDATA_QUEUE.bIsAlarm is %d, iHead is %d, iLength is %d, iRecordSize is %d, iMaxSize is %d\n",
			    g_HISDATA_QUEUE[j].bIsAlarm, g_HISDATA_QUEUE[j].iHead, g_HISDATA_QUEUE[j].iLength, g_HISDATA_QUEUE[j].iRecordSize,
			    g_HISDATA_QUEUE[j].iMaxSize);
		    }
		}
	    }
	}*/
	hBatteryTestLog = DAT_StorageOpen(BATT_TEST_LOG_FILE);
	
	if(hBatteryTestLog == NULL)
	{
	    //printf("Open BATT_TEST_LOG_FILE file!\n");
		return FALSE;
	}
	/*init*/

	stCondition.iEquipID	= -1;
	stCondition.iDataType	= -1;
	stCondition.tmFromTime	= 0;
	stCondition.tmToTime	= 0;

	/*find the match record*/
	//Find the battery test times
	iStartNO = -1;
	memset(iBuf, 0x0, iRecords * sizeof(int));

	iResult = DAT_StorageSearchRecords(hBatteryTestLog,
										 Web_fnCompareBatteryCondition,
										 NULL,
										 &iStartNO,
										 &iRecords,
										 (void *)iBuf,
										 FALSE,
										 FALSE,
										 TRUE);
	DAT_RecoverReadHandle(&hBatteryTestLog);

	/*for(i = 0; i < 10; i++)
	{
	    printf("iBuf[%d] is %d\n", i, iBuf[i]);
	}
	printf("iQueryTimes is %d\n", iQueryTimes);*/

	if(iResult < 0 || iRecords <= 0 )
	{
		DAT_StorageClose(hBatteryTestLog);
		printf("iResult is %d, iRecords is %d\n", iResult, iRecords);
		//printf("ERR_WEB_COMM_GET_RESULT_FAIL1!\n");

		return ERR_WEB_COMM_GET_RESULT_FAIL;
	}
	else if(iQueryTimes > iRecords)
	{
		DAT_StorageClose(hBatteryTestLog);
		//printf("ERR_WEB_COMM_GET_RESULT_FAIL2!\n");
		return ERR_WEB_COMM_GET_RESULT_FAIL;//ERR_COMM_FALSE_QUERY_TIMES;
	}
	else
	{
#ifdef _DEBUG
		int		k;
		for(k = 0; k < iRecords; k++)
		{
			//TRACE("iBuf[%d] : %d\n",k, iBuf[k]);
		}
		//iStartNO = iBuf[0] ;
		iStartNO = iBuf[iQueryTimes - 1];
		
#else
		iStartNO = iBuf[iQueryTimes - 1];
		/*iStartNOtemp = iStartNO;
		printf("iStartNOtemp is %d\n", iStartNOtemp);*/
#endif

	}


	/*get the match record head information*/
#ifdef _SHOW_WEB_INFO
	//TRACE("iStartNO:[%d]\n", iStartNO);
#endif

	stBatteryHeadInfo = NEW(BATTERY_HEAD_INFO,1);
	if(stBatteryHeadInfo == NULL)
	{
		DAT_StorageClose(hBatteryTestLog);
		//printf("ERR_WEB_COMM_GET_RESULT_FAIL3!\n");
		return ERR_WEB_COMM_GET_RESULT_FAIL;
	}
	memset(stBatteryHeadInfo, 0x0, sizeof(BATTERY_HEAD_INFO));
	iRecords = 1;
	
	iResult = DAT_StorageReadRecords(hBatteryTestLog,
										&iStartNO,
										&iRecords,
										stBatteryHeadInfo,//(void*)stBatteryHeadInfo,
										FALSE,
										TRUE);
	DAT_RecoverReadHandle(&hBatteryTestLog);


#ifdef _SHOW_WEB_INFO
	//TRACE("[DAT_StorageReadRecords] iResult :[%d][%d]\n", iResult,iRecords);
#endif
	
	
	if(iResult == FALSE )//|| iRecords <= 0)
	{
		DAT_StorageClose(hBatteryTestLog);
		//printf("ERR_WEB_COMM_GET_RESULT_FAIL4!\n");
		return ERR_WEB_COMM_GET_RESULT_FAIL;
	}

	/*Analyze the match record*/
	iQtyRecord	= stBatteryHeadInfo->btSummary.iQtyRecord;
	//changed by Yangguoxin,1/30/2007. Added 8 SMDU,extend the record space from 128 to 256
	iRecordNum	= (iQtyRecord * stBatteryHeadInfo->btSummary.iSizeRecord - 1)/256 + 1; //(iQtyRecord * stBatteryHeadInfo->btSummary.iSizeRecord - 1)/128 + 1; 
	/*iRecordNumtemp = iRecordNum;
	printf("iRecordNumtemp is %d\n", iRecordNumtemp);*/
	iQtyBatt	= stBatteryHeadInfo->btSummary.iQtyBatt;

	printf("iQtyRecord is %d iRecordNum is %d iQtyBatt is %d iSizeRecord is %d\n", iQtyRecord, iRecordNum, iQtyBatt, stBatteryHeadInfo->btSummary.iSizeRecord);
	
#ifdef _SHOW_WEB_INFO
	//TRACE("iRecordNum :%d iQtyBatt :%d iQtyRecord:%d \n", iRecordNum, iQtyBatt, iQtyRecord);
	//TRACE("stBatteryHeadInfo->szHeadInfo = [%s]\n",stBatteryHeadInfo->szHeadInfo);
	//TRACE("\t ,iStartReason %d, iEndReason %d, iTestResult %d, iSizeRecord : %d\n",
									//stBatteryHeadInfo->btSummary.iStartReason,
									//stBatteryHeadInfo->btSummary.iEndReason,
									//stBatteryHeadInfo->btSummary.iTestResult,
									//stBatteryHeadInfo->btSummary.iSizeRecord);
	//TRACE("read first record \n");
#endif 

 	/*read first record*/

	/*for(i = 0; i < 10; i++)
	{
	    printf("iBuf[%d] is %d\n", i, iBuf[i]);
	}
	printf("iQueryTimes is %d\n", iQueryTimes);*/
	iRecords = 4;
	iStartNO = iBuf[iQueryTimes - 1] + 1 ;
	/*printf("iStartNO is %d\n", iStartNO);
	printf("iRecordNum is %d\n", iRecordNum);*/

	//memset(&BatteryGeneralInfo, 0x0, 128);
	iResult = DAT_StorageReadRecords(hBatteryTestLog,
										&iStartNO,
										&iRecords,
										(void*)&BatteryGeneralInfo,
										FALSE,
										TRUE);
	DAT_RecoverReadHandle(&hBatteryTestLog);
#ifdef _SHOW_WEB_INFO
	//TRACE("[DAT_StorageReadRecords] iResult :[%d] iRecordNum [%d]\n", iResult,iRecords);
	int		mc;
	for(mc = 0; mc < iQtyBatt; mc++)
	{
		BATTERY_INFO	*pBI = &(BatteryGeneralInfo.BatteryInfo[mc]);

		//TRACE("byBatteryCapacity : [%c][%d]\n", pBI->byBatteryCapacity,pBI->byBatteryCapacity);
		//TRACE("byBatteryCurrent [%c][%d]\n", pBI->byBatteryCurrent,pBI->byBatteryCurrent);
		//TRACE("byBatteryNum:[%c][%d]\n", pBI->byBatteryNum, pBI->byBatteryNum);
		//TRACE("byBatteryTemp : [%c][%d]\n", pBI->byBatteryTemp, pBI->byBatteryTemp);
		//TRACE("byBatteryVolage : [%c][%d]\n", pBI->byBatteryVolage, pBI->byBatteryVolage);
	}
#endif

	/*printf("iRecordNum is %d\n", iRecordNum);*/
	if(iResult <= 0)
	{
		DAT_StorageClose(hBatteryTestLog);

		DELETE(stBatteryHeadInfo);
		stBatteryHeadInfo = NULL;
		//printf("ERR_WEB_COMM_GET_RESULT_FAIL5!\n");
		return ERR_WEB_COMM_GET_RESULT_FAIL;
	}

	/*read second record and next record*/
#ifdef _SHOW_WEB_INFO
	//TRACE("read second record and next record \n");
#endif 
	

	/*printf("iRecordNum is %d\n", iRecordNum);*/
	pBatteryData = NEW(BATTERY_DATA_RECORD, iRecordNum );
	if(pBatteryData == NULL)
	{
		DAT_StorageClose(hBatteryTestLog);
		DELETE(stBatteryHeadInfo);
		stBatteryHeadInfo = NULL;
		printf("ERR_WEB_NO_MEMORY!\n");
		return ERR_WEB_NO_MEMORY;
	}
	/*printf("iRecordNum is %d\n", iRecordNum);*/
	memset(pBatteryData, 0x0, iRecordNum  * sizeof(BATTERY_DATA_RECORD));
	
	/*for(i = 0; i < 10; i++)
	{
	    printf("iBuf[%d] is %d\n", i, iBuf[i]);
	}
	printf("iQueryTimes is %d\n", iQueryTimes);*/
	iStartNO = iBuf[iQueryTimes - 1] + 5;//�ӵ�5��iRecord��ʼ��
	/*iRecordNum = iRecordNumtemp;
	iStartNO = iStartNOtemp + 3;
	printf("iRecordNumtemp is %d iStartNOtemp is %d\n", iRecordNumtemp, iStartNOtemp);*/
	printf("\niStartNO = %d iRecordNum = %d\n", iStartNO, iRecordNum);
	iResult = DAT_StorageReadRecords(hBatteryTestLog,
										&iStartNO,
										&iRecordNum,
										(void *)pBatteryData,
										0,
										TRUE);
	DAT_RecoverReadHandle(&hBatteryTestLog);
	//printf("iRecordNum is %d\n", iRecordNum);
	
#ifdef _SHOW_WEB_INFO
	//TRACE("[DAT_StorageReadRecords] iResult :[%d] iRecordNum [%d]\n", iResult,iRecordNum);
#endif

	if(iResult > 0)
	{
		/*Analyze the battery data*/
		
		Web_ManageBatteryData(iRecordNum, iQtyBatt, iQtyRecord, BatteryGeneralInfo, 
									(void *)pBatteryData, ppBatteryRealData);
		
		DELETE(pBatteryData);
		pBatteryData = NULL;

		*ppGeneralInfo = stBatteryHeadInfo;
		memcpy(stOutBatteryGeneralInfo,&BatteryGeneralInfo, sizeof(BATTERY_GENERAL_INFO));
		/*free*/
		DAT_StorageClose(hBatteryTestLog);
		//printf("TRUE!\n");

		return TRUE;
	}
	else
	{
		/*free*/
		DAT_StorageClose(hBatteryTestLog);
		DELETE(stBatteryHeadInfo);
		stBatteryHeadInfo = NULL;
		//printf("FALSE!\n");
		return FALSE;
	}



}

/*==========================================================================*
 * FUNCTION :    Web_ManageBatteryData
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN int iRecordNum:
				IN int iQtyBatt:			Quantity of  battery
				IN int iQtyRecord:				Quantity of record
				IN void *pGeneralInfo: 
				IN void *pBatteryData:
				OUT BATTERY_LOG_INFO **ppBatteryRealData:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
#define FLOAT_EQUAL01(f)			((f < 0.1 && f >-0.1) ? 1 : 0)
static void Web_ManageBatteryData(IN int iRecordNum,				/*total record num*/
				  IN int iQtyBatt,					/*Quantity of  battery*/
				  IN int iQtyRecord,					/*Quantity of record*/
				  BATTERY_GENERAL_INFO stGeneralInfo, 
				  IN void *pBatteryData, 
				  OUT BATTERY_LOG_INFO **ppBatteryRealData)
{
	BATTERY_LOG_INFO		*stBatteryData = NULL;   //the largest battery group is 18.
	int						i = 0, j = 0, k = 0;
	int						iBatteryNum = 0;
	BYTE					*pfBatteryData = NULL, *pfTemBatteryData = NULL;
	BATTERY_DATA_RECORD		*pfBatteryDataInfo = (BATTERY_DATA_RECORD *) pBatteryData;
	int						iValue = 0;
	float					fValue;
	int					m;
	BYTE					byTest[256];

	pfBatteryData = NEW(BYTE, iRecordNum * 256);
	ASSERT(pfBatteryData);

	//printf("iRecordNum =%d iQtyBatt = %d,iQtyRecord = %d\n",iRecordNum, iQtyBatt,iQtyRecord);
	for(k = 0; k < iRecordNum && pfBatteryDataInfo != NULL; k++, pfBatteryDataInfo++)
	{
		memcpy(pfBatteryData + k * 256, pfBatteryDataInfo->byBatteryDataInfo, sizeof(BYTE) * 256);
	}

	pfTemBatteryData =(BYTE *) pfBatteryData;


	stBatteryData = NEW(BATTERY_LOG_INFO, iQtyRecord);
	ASSERT(stBatteryData);

	//	for(i = 0; i < iQtyBatt; i++)
	//	{
	//		
	////#ifdef _SHOW_WEB_INFO
	//		TRACE(" %d\n", stGeneralInfo.BatteryInfo[i].byBatteryCurrent);
	//		TRACE(" %d\n", stGeneralInfo.BatteryInfo[i].byBatteryVolage);
	//		TRACE(" %d\n", stGeneralInfo.BatteryInfo[i].byBatteryCapacity);
	//
	////#endif 
	//	}
	memset(byTest, 0x0, sizeof(byTest)); 
	memcpy(byTest, &stGeneralInfo, sizeof(byTest));

	for(k = 0; k < iQtyRecord; k++)
	{
		if(k > 0)
		{
			pfBatteryData = pfBatteryData + 4;			//?

		}
		/*get time*/
		memcpy((int*)&iValue, pfBatteryData, 4 * sizeof(BYTE));
		stBatteryData[k].tRecordTime = iValue;

		/*get system voltage*/
		pfBatteryData = pfBatteryData + 4;
		memcpy((void*)&fValue, pfBatteryData, 4 * sizeof(BYTE));
		stBatteryData[k].fVoltage = fValue;

		TRACE("k = %d: tRecordTime = %ul, fVoltage = %f\n", k, stBatteryData[k].tRecordTime, stBatteryData[k].fVoltage);
		/*get Temperature*/
		for(i = 0; i < 7; i++)
		{
			if(stGeneralInfo.byTemperatureInfo[i] != 0)
			{
				pfBatteryData = pfBatteryData + 4;
				memcpy((void*)&fValue, pfBatteryData, 4);
				stBatteryData[k].fTemp[i] = fValue;
			}
			else
			{
				stBatteryData[k].fTemp[i] = 0.0;
			}
		}



		/*stBatteryData[k].pBatteryRealData = NEW(BATTERY_REAL_DATA, iQtyBatt);
		ASSERT(stBatteryData[k].pBatteryRealData);*/

		for(i = 0; i < iQtyBatt; i++)
		{
			/*Battery Current*/
			if(stGeneralInfo.BatteryInfo[i].byBatteryCurrent != 0)
			{
				pfBatteryData = pfBatteryData + 4;
				memcpy((void*)&fValue, pfBatteryData, 4);
				//printf("fValue is %f\n", fValue);
				if(FLOAT_EQUAL01(fValue))
				{
					stBatteryData[k].pBatteryRealData[i].fBatCurrent = 0.0;
				}
				else
				{
					stBatteryData[k].pBatteryRealData[i].fBatCurrent = fValue;
				}

				TRACE("stBatteryData[%d].pBatteryRealData[%d].fBatCurrent = %f",k,i, stBatteryData[k].pBatteryRealData[i].fBatCurrent);
			}
			else
			{
				stBatteryData[k].pBatteryRealData[i].fBatCurrent = 0.0;
			}

			/*Battery Voltage*/
			if(stGeneralInfo.BatteryInfo[i].byBatteryVolage != 0)
			{
				pfBatteryData = pfBatteryData + 4;
				memcpy((void*)&fValue, pfBatteryData, 4);
				//printf("fValue is %f\n", fValue);
				stBatteryData[k].pBatteryRealData[i].fBatVoltage = fValue;
				if(FLOAT_EQUAL01(fValue))
				{
					stBatteryData[k].pBatteryRealData[i].fBatVoltage = 0.0;
				}
				else
				{
					stBatteryData[k].pBatteryRealData[i].fBatVoltage = fValue;
				}
				TRACE("stBatteryData[%d].pBatteryRealData[%d].fBatCurrent = %f",k,i, stBatteryData[k].pBatteryRealData[i].fBatCurrent);
			}
			else
			{
				stBatteryData[k].pBatteryRealData[i].fBatVoltage = 0.0;
			}

			/*Battery Capacity*/
			if(stGeneralInfo.BatteryInfo[i].byBatteryCapacity != 0)
			{
				pfBatteryData = pfBatteryData + 4;
				memcpy((void*)&fValue, pfBatteryData, 4);
				//printf("fValue is %f\n", fValue);
				stBatteryData[k].pBatteryRealData[i].fBatCapacity = fValue;
				TRACE("stBatteryData[%d].pBatteryRealData[%d].fBatCurrent = %f",k,i, stBatteryData[k].pBatteryRealData[i].fBatCurrent);
			}
			else
			{
				stBatteryData[k].pBatteryRealData[i].fBatCapacity = 0.0;
			}

		}
		//printf("Now the read block voltage is:\n");
		for(i = 0; i < 20; i++)
		{
		    for(j = 0; j < 24; j++)
		    {
			if(stGeneralInfo.stSmbatSmbrcBlock[i].bySMBlockVoltage[j])
			{				
			    pfBatteryData = pfBatteryData + 4;
			    memcpy((void *)&fValue, pfBatteryData, 4);
			    //printf("[%d][%d]fValue is %f\n", i, j, fValue);
			    if(FLOAT_EQUAL01(fValue))
			    {
				stBatteryData[k].fBatSMBlock[i].fSMBlockVoltage[j] = 0.0;
			    }
			    else
			    {
				stBatteryData[k].fBatSMBlock[i].fSMBlockVoltage[j] = fValue;
			    }
			}
			else
			{
			    stBatteryData[k].fBatSMBlock[i].fSMBlockVoltage[j] = 0.0;
			}
		    }
		}
		for(i = 0; i < 4; i++)
		{
			for(j = 0; j < 8; j++)
			{
				if(stGeneralInfo.stBatteryBlock[i].byBlockVoltage[j])
				{				
					pfBatteryData = pfBatteryData + 4;
					memcpy((void *)&fValue, pfBatteryData, 4);
					if(FLOAT_EQUAL01(fValue))
					{
						stBatteryData[k].fBatBlock[i].fBatBlockVoltage[j] = 0.0;
					}
					else
					{
						stBatteryData[k].fBatBlock[i].fBatBlockVoltage[j] = fValue;
					}
				}
				else
				{
					stBatteryData[k].fBatBlock[i].fBatBlockVoltage[j] = 0.0;
				}
			}
		}
		/*for(j = 0; j < 32; j++)
		{
		m = j/8;
		if(byTest[205 + j] != 0)
		{
		pfBatteryData = pfBatteryData + 4;
		memcpy((void *)&fValue, pfBatteryData, 4);
		stBatteryData[k].fBatBlock[m].fBatBlockVoltage[j] = fValue;
		TRACE("stBatteryData[%d].pBatteryRealData[%d].fBatCurrent = %f",k,i, stBatteryData[k].pBatteryRealData[i].fBatCurrent);
		}
		else
		{
		stBatteryData[k].fBatBlock[m].fBatBlockVoltage[j] = 0.0;
		}
		}*/
	}



	DELETE(pfTemBatteryData);
	pfTemBatteryData = NULL;
	*ppBatteryRealData = stBatteryData;



}


/*==========================================================================*
 * FUNCTION :    Web_TransferHisDataIDToName
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN void *pBuf:
				IN int iDataLen:
				IN int iLanguage:
				OUT void **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
/*alarm ID*/
static int Web_TransferHisDataIDToName(IN void *pBuf,
									   IN int iDataLen,
									   IN int iLanguage,
									   OUT void **ppBuf)
{
	char					*pszEquipName	= NULL;
	char					*pszSignalName	= NULL;
	HIS_DATA_RECORD			*pHisDataRecord = NULL;
	HIS_DATA_TO_RETURN		*pDataReturn	= NULL;
	char					*szUnit = NULL, *szState = NULL;
	int						iValueType = 0;
	char					szTime1[32], *pTime = NULL;
	time_t					tmUTC;

	/*get equipment alarm info by ID*/
	if(pBuf == NULL)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_TransferHisDataIDToName pBuf : FALSE\n");
#endif 
		
		return FALSE;
	}

	pHisDataRecord = (HIS_DATA_RECORD *)pBuf;

	pDataReturn = NEW(HIS_DATA_TO_RETURN, iDataLen);
	if(pDataReturn == NULL)
	{
		return FALSE;
	}
	memset(pDataReturn,0x0, (size_t)iDataLen);

	/*get signal name*/
	*ppBuf  = (void *)pDataReturn;
	while(pHisDataRecord != NULL && iDataLen > 0)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("iDataLen : %d\n", iDataLen);
#endif 

		pszEquipName = Web_GetEquipNameFromInterface((int)pHisDataRecord->iEquipID,
													iLanguage); 
	
#ifdef _SHOW_WEB_INFO
		//TRACE("pHisDataRecord->iEquipID : [%d]\n", pHisDataRecord->iEquipID);
#endif 
		
		if(pszEquipName != NULL)
		{
			strncpyz(pDataReturn->szEquipName,pszEquipName,sizeof(pDataReturn->szEquipName));
			DELETE(pszEquipName);
			pszEquipName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szEquipName,"--",sizeof(pDataReturn->szEquipName));
		}
	  
		pszSignalName =  Web_GetASignalName(LOWORD(pHisDataRecord->iSignalID),
											HIWORD(pHisDataRecord->iSignalID),
											pHisDataRecord->iEquipID,
											iLanguage,
											&szUnit,
											pHisDataRecord->varSignalVal.enumValue,//-1,
											&szState,
											&iValueType);
		if(pszSignalName != NULL)
		{
			strncpyz(pDataReturn->szSignalName,pszSignalName,sizeof(pDataReturn->szSignalName));
			DELETE(pszSignalName);
			pszSignalName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szSignalName,"--",sizeof(pDataReturn->szSignalName));
		}
		
		pDataReturn->tSignalTime	= pHisDataRecord->tmSignalTime;

		//Get signal value
		if(iValueType == VAR_LONG)
		{
			sprintf(pDataReturn->szSignalVal,"%ld", pHisDataRecord->varSignalVal.lValue);
        }
		else if(iValueType == VAR_FLOAT)
		{
			sprintf(pDataReturn->szSignalVal,"%.2f", pHisDataRecord->varSignalVal.fValue);
		}
		else if(iValueType == VAR_UNSIGNED_LONG)
		{
			sprintf(pDataReturn->szSignalVal,"%lu", pHisDataRecord->varSignalVal.ulValue);
		}
		else if(iValueType == VAR_DATE_TIME)
		{
			//Conver to local time
			
			tmUTC = pHisDataRecord->varSignalVal.dtValue;
			ConvertTime(&tmUTC, TRUE);

			TimeToString(tmUTC, TIME_CHN_FMT, 
						szTime1, 32);
			//trim
			pTime = szTime1;
			pTime = pTime + 5;
			pTime[13] = '\0';

			//show
			strncpyz(pDataReturn->szSignalVal,pTime, sizeof(pDataReturn->szSignalVal));
		}
		else 
		{
			if(szState != NULL)
			{
				sprintf(pDataReturn->szSignalVal,"%-s", szState);//pHisDataRecord->varSignalVal.enumValue);
				
			}
			else
			{
				sprintf(pDataReturn->szSignalVal,"%s", "--");
			}
		}

		if(szState != NULL)
		{
			DELETE(szState);
			szState = NULL;
		}
		if(szUnit != NULL)
		{
			strncpyz(pDataReturn->szUnit, szUnit, sizeof(pDataReturn->szUnit));
			DELETE(szUnit);
			szUnit = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szUnit, "--", sizeof(pDataReturn->szUnit));
		}

		pDataReturn->dwActAlarmID	= pHisDataRecord->iActAlarmID;

		pHisDataRecord++;
		pDataReturn++;

		iDataLen--;

	}

	
	return TRUE;
}



/*==========================================================================*
 * FUNCTION :   Web_TransferAlarmDataIDToName 
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN void *pBuf:
				IN int iDataLen:
				IN int iLanguage:
				OUT void **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_TransferAlarmDataIDToName(IN void *pBuf,
										 IN int iDataLen,
										 IN int iLanguage,
										 OUT void **ppBuf)
{
	char						*pszEquipName	= NULL;
	char						*pszSignalName	= NULL;
	char						*szUnit = NULL;
	HIS_ALARM_TO_RETURN			*pDataReturn	= NULL;
	HIS_ALARM_RECORD			*pAlarmRecord	= NULL;
	//int							iValueType = 0;
	
	ASSERT(pBuf);
	if(pBuf == NULL)
	{
		return FALSE;
	}
	pAlarmRecord = (HIS_ALARM_RECORD *)pBuf;

	/*init*/
	pDataReturn = NEW(HIS_ALARM_TO_RETURN, iDataLen);
	if(pDataReturn == NULL)
	{
		return FALSE;
	}
	memset(pDataReturn, 0x0, (size_t)iDataLen);

	*ppBuf  = (void *)pDataReturn;
	while(pAlarmRecord != NULL && iDataLen > 0)
	{
		if(pAlarmRecord->szSerialNumber[0] == '\0')
		{
			pszEquipName = Web_GetEquipNameFromInterface_New(pAlarmRecord->iEquipID, iLanguage, pAlarmRecord->iPosition); 
			if(pszEquipName != NULL)
			{
				strncpyz(pDataReturn->szEquipName,pszEquipName,
							sizeof(pDataReturn->szEquipName));
				DELETE(pszEquipName);
				pszEquipName = NULL;
			}
			else
			{
				strncpyz(pDataReturn->szEquipName,"--",
							sizeof(pDataReturn->szEquipName));
			}
		}
		else
		{
#ifdef		WEB_SUPPORT_RECTID
			pszEquipName = Web_GetEquipNameFromInterface_New(pAlarmRecord->iEquipID, iLanguage, pAlarmRecord->iPosition); 
			if(pszEquipName)
			{			
				strncpyz(pDataReturn->szEquipName, pszEquipName,
						sizeof(pDataReturn->szEquipName));
				DELETE(pszEquipName);
				pszEquipName = NULL;
			}
			else
			{
				strncpyz(pDataReturn->szEquipName,"--",
					sizeof(pDataReturn->szEquipName));
			}
#else
			strncpyz(pDataReturn->szEquipName, pAlarmRecord->szSerialNumber,
							sizeof(pDataReturn->szEquipName));
			//printf("\npAlarmRecord->szSerialNumber = %s\n", pAlarmRecord->szSerialNumber);
#endif
		}

		pszSignalName = Web_GetASignalName(pAlarmRecord->iAlarmID,		//Signal ID
											SIG_TYPE_ALARM,				//Signal type
											pAlarmRecord->iEquipID,
											iLanguage,
											&szUnit,
											-1,
											NULL,
											NULL);

		if(pszSignalName != NULL)
		{
			strncpyz(pDataReturn->szAlarmName, pszSignalName,
				sizeof(pDataReturn->szAlarmName));
			DELETE(pszSignalName);
			pszSignalName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szAlarmName, "--",
				sizeof(pDataReturn->szAlarmName));
		}

		pDataReturn->tmStartTime	= pAlarmRecord->tmStartTime;
		pDataReturn->tmEndTime		= pAlarmRecord->tmEndTime;		
		pDataReturn->fTriggerValue	= pAlarmRecord->varTrigValue.fValue;
		pDataReturn->byLevel		= pAlarmRecord->byAlarmLevel;

		if(szUnit != NULL)
		{
			strncpyz(pDataReturn->szUnit, szUnit, sizeof(pDataReturn->szUnit));
			DELETE(szUnit);
			szUnit = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szUnit, "--", sizeof(pDataReturn->szUnit));
		}
		pAlarmRecord++;
		pDataReturn++;
		iDataLen--;

	}
 	return TRUE;
}



/*==========================================================================*
 * FUNCTION :    Web_GetASignalName
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN int iSignalID:
				IN int iDataType:
				IN int iEquipID:
				IN int iLanguage:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static char *Web_GetASignalName(IN int iSignalID, 
								IN int iDataType, 
								IN int iEquipID, 
								IN int iLanguage,
								OUT char **szUnit,
								IN int iStatue,
								IN char **szStatue,
								IN int *iValueType)
{
	int		iVarSubID, iError = 0;
	iVarSubID = DXI_MERGE_SIG_ID(iDataType,iSignalID);
	char	*szSignalName = NULL;

	szSignalName = NEW(char, 33);
	if(szSignalName == NULL)
	{
		return NULL;
	}
	memset(szSignalName, 0x0, 33);
	
	if(iDataType == SIG_TYPE_SAMPLING )
	{
		SAMPLE_SIG_INFO		*pSigInfo = NULL;	
		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
						iEquipID,			
						iVarSubID,		
						&iBufLen,			
						(void *)&pSigInfo,			
						iTimeOut);

		if (iError == ERR_DXI_OK)
		{
			if(strcmp(pSigInfo->szSigUnit, "\0") != 0)
			//if(pSigInfo->szSigUnit != NULL)
			{
				*szUnit = NEW(char, 8);
				ASSERT(*szUnit);
				if(*szUnit != NULL)
				{
					strncpyz(*szUnit, pSigInfo->szSigUnit, 8);
				}
			}

			if((pSigInfo->iStateNum > 0) && (iStatue != -1) && (iStatue <= (int)pSigInfo->fMaxValidValue) && (iStatue >= (int)pSigInfo->fMinValidValue))
			{
				*szStatue = NEW(char, 32);
				iStatue = iStatue - (int)pSigInfo->fMinValidValue;
				strncpyz(*szStatue, pSigInfo->pStateText[iStatue]->pFullName[iLanguage], 32);
			}

			strncpyz(szSignalName, pSigInfo->pSigName->pFullName[iLanguage], 33);
			if(iValueType != NULL)
			{
				*iValueType = pSigInfo->iSigValueType;
			}
			return	szSignalName;
		}
		else
		{
			return NULL;
		}

	}
	else if(iDataType == SIG_TYPE_CONTROL)
	{
		CTRL_SIG_INFO	*pSigInfo = NULL;
		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
						iEquipID,			
						iVarSubID,		
						&iBufLen,			
						(void *)&pSigInfo,			
						iTimeOut);
		if (iError == ERR_DXI_OK)
		{
			if(strcmp(pSigInfo->szSigUnit, "\0") != 0)
			//if(pSigInfo->szSigUnit != NULL)
			{
				*szUnit = NEW(char, 8);
				ASSERT(*szUnit);
				if(*szUnit != NULL)
				{
					strncpyz(*szUnit, pSigInfo->szSigUnit, 8);
				}
			}

			
			if((pSigInfo->iStateNum > 0) && (iStatue != -1) && (iStatue <= (int)pSigInfo->fMaxValidValue) && (iStatue >= (int)pSigInfo->fMinValidValue))
			{
				*szStatue = NEW(char, 32);
				iStatue = iStatue - (int)pSigInfo->fMinValidValue;
				strncpyz(*szStatue, pSigInfo->pStateText[iStatue]->pFullName[iLanguage], 32);
			}

			if(iValueType != NULL)
			{
				*iValueType = pSigInfo->iSigValueType;
			}

			strncpyz(szSignalName, pSigInfo->pSigName->pFullName[iLanguage], 33);
			return	szSignalName;
		}
		else
		{
			return NULL;
		}

	}
	else if(iDataType == SIG_TYPE_SETTING)
	{
		SET_SIG_INFO	*pSigInfo = NULL;
		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
						iEquipID,			
						iVarSubID,		
						&iBufLen,			
						(void *)&pSigInfo,			
						iTimeOut);
		if (iError == ERR_DXI_OK)
		{
			

			if(strcmp(pSigInfo->szSigUnit, "\0") != 0)
			//if(pSigInfo->szSigUnit != NULL)
			{
				*szUnit = NEW(char, 8);
				ASSERT(*szUnit);
				if(*szUnit != NULL)
				{
					strncpyz(*szUnit, pSigInfo->szSigUnit, 8);
				}
			}

			if((pSigInfo->iStateNum > 0) && (iStatue != -1) && (iStatue <= (int)pSigInfo->fMaxValidValue) && (iStatue >= (int)pSigInfo->fMinValidValue))
			{
				*szStatue = NEW(char, 32);
				iStatue = iStatue - (int)pSigInfo->fMinValidValue;
				strncpyz(*szStatue, pSigInfo->pStateText[iStatue]->pFullName[iLanguage], 32);
			}

			if(iValueType != NULL)
			{
				*iValueType = pSigInfo->iSigValueType;
			}

			strncpyz(szSignalName, pSigInfo->pSigName->pFullName[iLanguage], 33);
			return	szSignalName;
		}
		else
		{
			return NULL;
		}

	}
	else if(iDataType == SIG_TYPE_ALARM)
	{
		ALARM_SIG_INFO	*pSigInfo = NULL;
		
		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
						iEquipID,			
						iVarSubID,		
						&iBufLen,			
						(void *)&pSigInfo,			
						iTimeOut);
		
		if (iError == ERR_DXI_OK)
		{
			strncpyz(szSignalName, pSigInfo->pSigName->pFullName[iLanguage], 33);

			return	szSignalName;
		}
		else
		{
			return NULL;
		}	

	}
	else
	{
		return NULL;
	}
}


static char *Web_GetAEquipName(IN int iEquipID, IN int iLanguage)
{
	EQUIP_INFO* pEquipInfo;
	int			iEquipNum = 0;
	int			i = 0;
	int iError = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	iEquipNum = iBufLen / sizeof(EQUIP_INFO);
	if(iError == ERR_DXI_OK)
	{
		for( i = 0; i < iEquipNum && pEquipInfo != NULL; i++)
		{
			if(pEquipInfo->iEquipID == iEquipID)
			{
				return pEquipInfo->pEquipName->pFullName[iLanguage];
			}
			pEquipInfo++;
		}
	}
	else
	{
		return NULL;
	}
	return NULL;
}
/*==========================================================================*
 * FUNCTION :    Web_TransferStatDataIDToName
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN void *pBuf:
				IN int iDataLen:
				IN int iLanguage:
				OUT void **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_TransferStatDataIDToName(IN void *pBuf, 
										IN int iDataLen,
										IN int iLanguage,
										OUT void **ppBuf)
{
	char						*pszEquipName		= NULL;
	char						*pszSignalName		= NULL;
	HIS_STATDATA_TO_RETURN		*pDataReturn		= NULL;
	HIS_STAT_RECORD				*pStatDataRecord	= NULL;
	char						*szUnit = NULL;
	char						*szState = NULL;
	int							iValueType = 0;

	if(pBuf == NULL)
	{
		return FALSE;
	}

	pStatDataRecord = pBuf;
	/*init*/
	pDataReturn = NEW(HIS_STATDATA_TO_RETURN, iDataLen);
	if(pDataReturn == NULL)
	{
		return FALSE;
	}
	*ppBuf  = (void *)pDataReturn;
	while(pStatDataRecord != NULL && iDataLen > 0)
	{
		pszEquipName = Web_GetEquipNameFromInterface(pStatDataRecord->iEquipID, 
																	iLanguage); 
		if(pszEquipName != NULL)
		{
			strncpyz(pDataReturn->szEquipName, 
						pszEquipName,
						sizeof(pDataReturn->szEquipName));
			DELETE(pszEquipName);
			pszEquipName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szEquipName, 
						"--",
						sizeof(pDataReturn->szEquipName));
		}
		pszSignalName = Web_GetASignalName(	LOWORD(pStatDataRecord->iSignalID), 
										HIWORD(pStatDataRecord->iSignalID),
										pStatDataRecord->iEquipID, 
										iLanguage,
										&szUnit,
										-1, 
										&szState,
										&iValueType);
		if(pszSignalName != NULL)
		{
			strncpyz(pDataReturn->szSignalName, 
					 pszSignalName,
					 sizeof(pDataReturn->szSignalName));
			DELETE(pszSignalName);
			pszSignalName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szSignalName, "--",
				sizeof(pDataReturn->szSignalName));
		}
		//TRACE("pStatDataRecord->varMaxValue.fValue:%f\n", pStatDataRecord->varMaxValue.fValue);
		//TRACE("pStatDataRecord->varMaxValue.fValue:%f\n", pStatDataRecord->varMinValue.fValue);
		
		if(iValueType == VAR_LONG)
		{
			sprintf(pDataReturn->szMaxVal, "%ld",pStatDataRecord->varMaxValue.lValue);
			sprintf(pDataReturn->szMinVal,"%ld",pStatDataRecord->varMinValue.lValue);
			sprintf(pDataReturn->szAverageVal,"%ld",pStatDataRecord->varAverageValue.lValue);
			sprintf(pDataReturn->szCurrentVal,"%ld",pStatDataRecord->varCurrentVal.lValue);
		}
		else if(iValueType == VAR_FLOAT)
		{
			//sprintf(pDataReturn->szSignalVal,"%f", pStatDataRecord->varSignalVal.fValue);
			
			sprintf(pDataReturn->szMaxVal, "%8.2f",pStatDataRecord->varMaxValue.fValue);
			sprintf(pDataReturn->szMinVal,"%8.2f",pStatDataRecord->varMinValue.fValue);
			sprintf(pDataReturn->szAverageVal,"%8.2f",pStatDataRecord->varAverageValue.fValue);
			sprintf(pDataReturn->szCurrentVal,"%8.2f",pStatDataRecord->varCurrentVal.fValue);

		}
		else if(iValueType == VAR_UNSIGNED_LONG)
		{
			//sprintf(pDataReturn->szSignalVal,"%lu", pStatDataRecord->varSignalVal.ulValue);
			sprintf(pDataReturn->szMaxVal, "%lu",pStatDataRecord->varMaxValue.ulValue);
			sprintf(pDataReturn->szMinVal,"%lu",pStatDataRecord->varMinValue.ulValue);
			sprintf(pDataReturn->szAverageVal,"%lu",pStatDataRecord->varAverageValue.ulValue);
			sprintf(pDataReturn->szCurrentVal,"%lu",pStatDataRecord->varCurrentVal.ulValue);

		}
		


		
		pDataReturn->tmMaxValTime = pStatDataRecord->tmMaxValue;
		pDataReturn->tmMinValTime = pStatDataRecord->tmMinValue;
		pDataReturn->tmStatTime	 = pStatDataRecord->tmStatTime;
		
		if(szUnit != NULL)
		{
			strncpyz(pDataReturn->szUnit, szUnit, sizeof(pDataReturn->szUnit));
		}
		else
		{
			strncpyz(pDataReturn->szUnit, "--", sizeof(pDataReturn->szUnit));
		}
		DELETE(szUnit);
		szUnit = NULL;

		pStatDataRecord++;
		pDataReturn++;
		iDataLen--;
	}
	
	return TRUE;
}



/*==========================================================================*
 * FUNCTION :   Web_TransferControlIDToName 
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN HIS_CONTROL_RECORD *pBuf:
				IN int iDataLen:
				IN int iLanguage,OUT void **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_TransferControlIDToName(IN HIS_CONTROL_RECORD *pBuf,
									   IN int iDataLen,
									   IN int iLanguage,
									   OUT void **ppBuf)
{
	char						*pszEquipName	= NULL;
	char						*pszSignalName	= NULL;
	HIS_CONTROL_TO_RETURN		*pDataReturn	= NULL;
	HIS_CONTROL_RECORD			*pControlRecord = NULL;
	char						*szUnit = NULL;
	int							iChannel = -1, iValue = -1,iSave = -1;
	//char						szValue[32];
	char						*szStatue = NULL;
	char						szValue[64];
	int							iValueType = 0;
	time_t						tmUTC;

	char						szTime1[32], *pTime = NULL;
	ASSERT(pBuf);
	if(pBuf == NULL)
	{

		////TRACE("pBuf : false");
		return FALSE;
	}

	pControlRecord = (HIS_CONTROL_RECORD *)pBuf;

	/*init*/
	pDataReturn = NEW(HIS_CONTROL_TO_RETURN,iDataLen);
	if(pDataReturn == NULL)
	{
		////TRACE("pDataReturn : false");
		return FALSE;
	}
	memset(pDataReturn, 0x0, (size_t)iDataLen);

	*ppBuf  = (void *)pDataReturn;
	while(pControlRecord != NULL && iDataLen > 0)
	{
		
		/*Transfer equip name*/
		pszEquipName = Web_GetEquipNameFromInterface_New(pControlRecord->iEquipID, iLanguage, pControlRecord->iPositionID); 
		if(pszEquipName != NULL)
		{
			strncpyz(pDataReturn->szEquipName, pszEquipName,sizeof(pDataReturn->szEquipName));
			DELETE(pszEquipName);
			pszEquipName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szEquipName, "--",sizeof(pDataReturn->szEquipName));
		}

		
		//sscanf(pControlRecord->szCtrlCmd, "%d,%d,%d", &iChannel, &iValue, &iSave);
		Web_TransferGetValue(pControlRecord->szCtrlCmd, &iChannel,szValue,&iSave);
		
#ifdef _SHOW_WEB_INFO
		//TRACE("iChannel[%d] szValue[%s] iSave[%d]\n", iChannel, szValue, iSave);
#endif 
		
		/*Transfer signal name*/
		////{{ new code. maofuhua, 2005-5-10
		szStatue = NULL;
		iValue = atoi(szValue);
		
		pszSignalName = Web_GetASignalName(LOWORD(pControlRecord->iSignalID),		//Signal ID
										HIWORD(pControlRecord->iSignalID),		//Signal type
										pControlRecord->iEquipID,
										iLanguage,
										&szUnit,
										iValue,
										&szStatue,
										&iValueType);
		


		if(pszSignalName != NULL )
		{
			strncpyz(pDataReturn->szSignalName, 
					 pszSignalName,
					 sizeof(pDataReturn->szSignalName));
			DELETE(pszSignalName);
			pszSignalName = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szSignalName, 
					 "--",
					 sizeof(pDataReturn->szSignalName));
		}
		
		pDataReturn->iValueType = 0; 
		if(szStatue != NULL)
		{
			strncpyz(pDataReturn->szControlVal, szStatue, sizeof(pDataReturn->szControlVal));
			strncpyz(pDataReturn->szControlValForSave, szStatue, sizeof(pDataReturn->szControlValForSave));
			DELETE(szStatue);
			szStatue = NULL;
		}
		else if(iValueType == VAR_DATE_TIME)
		{
			

			pDataReturn->iValueType = 1;

			tmUTC = (time_t)atoi(szValue);
		
			TimeToString(tmUTC, TIME_CHN_FMT, 
						szTime1, sizeof(szTime1));
			//trim
			pTime = szTime1;
			pTime = pTime + 5;
			pTime[8] = '\0';

			//show
			strncpyz(pDataReturn->szControlValForSave,pTime, sizeof(pDataReturn->szControlValForSave));
			strncpyz(pDataReturn->szControlVal,szValue, sizeof(pDataReturn->szControlVal));

		}
		else if(iValueType == VAR_FLOAT)
		{
			sprintf(pDataReturn->szControlValForSave, "%.2f", atof(szValue));
			sprintf(pDataReturn->szControlVal, "%8.2f", atof(szValue));
		}
		else
		{
			sprintf(pDataReturn->szControlValForSave, "%-s", szValue);
			sprintf(pDataReturn->szControlVal, "%s", szValue);
		}
		
		
		pDataReturn->tmControlTime	= pControlRecord->tmControlTime;
		//pDataReturn->fControlVal	= pControlRecord->varControlVal.fValue;	
		//strncpyz(pDataReturn->szControlVal, pControlRecord->szCtrlCmd, sizeof(pDataReturn->szControlVal));
		if(szUnit != NULL)
		{
			strncpyz(pDataReturn->szUnit, szUnit, sizeof(pDataReturn->szUnit));
			DELETE(szUnit);
			szUnit = NULL;
		}
		else
		{
			strncpyz(pDataReturn->szUnit, "--", sizeof(pDataReturn->szUnit));
		}

		/*redefine that store name , not ID */
		strncpyz(pDataReturn->szCtrlSenderName, 
				pControlRecord->szSenderName, 
				 sizeof(pDataReturn->szCtrlSenderName));
		strncpyz(pDataReturn->szSenderType, 
				 szUserInfo[pControlRecord->bySenderType - 1], 
				 sizeof(pDataReturn->szSenderType));
		//strncpyz(pDataReturn->szControlResult, 
		//		szControlResult[pControlRecord->nControlResult], 
		//		 sizeof(pDataReturn->szControlResult));
		pDataReturn->iControlResult = pControlRecord->nControlResult;

		pControlRecord++;
		pDataReturn++;
		iDataLen--;
		
	}
	
	return TRUE;
}

static void Web_TransferGetValue(IN char *szCtrlCmd, 
								 OUT int *iChannel, 
								 OUT char *szValue, 
								 OUT int *iSave)
{
	ASSERT(szCtrlCmd);
	char	*pSearchValue = NULL;
	char	*pTempCtrlCmd = szCtrlCmd;
	char	szBuffer[32];
	int		iPosition = 0;
	
	if(pTempCtrlCmd != NULL)
	{
		pSearchValue = strchr(pTempCtrlCmd, ',');
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTempCtrlCmd;

			if(iPosition > 0)
			{
				strncpyz(szBuffer, pTempCtrlCmd, iPosition + 1);
				pTempCtrlCmd = pTempCtrlCmd + iPosition;
				*iChannel = atoi(szBuffer);
			}

		}
		pTempCtrlCmd =  pTempCtrlCmd + 1;

		pSearchValue = strchr(pTempCtrlCmd, ',');
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTempCtrlCmd;

			if(iPosition > 0)
			{
				strncpyz(szValue, pTempCtrlCmd, iPosition + 1);
				pTempCtrlCmd = pTempCtrlCmd + iPosition;
				 
			}

		}
		pTempCtrlCmd =  pTempCtrlCmd + 1;

	
		//strncpyz(szBuffer, pTempCtrlCmd, (int)strlen(pTempCtrlCmd) + 1);
		strncpyz(szBuffer, pTempCtrlCmd, sizeof(szBuffer));//(int)strlen(pTempCtrlCmd) + 1);
		*iSave = atoi(szBuffer);
			

	}
}

/*==========================================================================*
 * FUNCTION :    Web_QueryHisLog
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN time_t fromTime:
				IN time_t toTime:
				OUT void **ppHisLog:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/

static int Web_QueryHisLog(IN time_t fromTime, 
						   IN time_t toTime, 
						   OUT void **ppHisLog)
{
	UNUSED(APP_LOG_FILE);
	char			*szReadLog = NULL, *szReadTempLog = NULL;
	int				iStartRecordNum = 1;
	int				iRecords = MAX_LOG_RECORD_COUNT;
	int				iRecordSize = (int)sizeof(LOG_RECORD_SIZE);
	char			szReadString[iRecordSize + 1];
	int				i, iMatchRecord = 0;
	time_t			tmReturn = 0;
	HIS_READ_LOG	pLog;
	HIS_MATCH_LOG	*pMatchLog = NULL, *pTMatchLog = NULL;

	szReadLog = NEW(char, MAX_LOG_RECORD_COUNT * iRecordSize);
	ASSERT(szReadLog);

	if(szReadLog == NULL)
	{
		return FALSE;
	}

	memset(szReadLog, 0x0, (size_t)MAX_LOG_RECORD_COUNT * iRecordSize);

	time_t		the_time,end_time;
	the_time = time((time_t *)0);
	printf("\n__The Time 1 is:%ld___\n",the_time);
	int		iResult = LOG_StorageReadRecords(&iStartRecordNum,
											&iRecords,
											(void*)szReadLog,
											FALSE,
											TRUE);
	end_time = time((time_t *)0);
	printf("\n__The Time 2 is:%ld___\n",end_time);
	printf("\n__Elapsed Time is:%lf__\n",difftime(end_time,the_time));

	pMatchLog = NEW(HIS_MATCH_LOG, iRecords);
	ASSERT(pMatchLog);

	the_time = time((time_t *)0);
	printf("\n__The Time 1 is:%ld___\n",the_time);
	if(pMatchLog != NULL)
	{
		pTMatchLog = pMatchLog;
		if(iResult == TRUE)
		{
			szReadTempLog = szReadLog;
			for(i = 0; i < iRecords && szReadTempLog != NULL; i++)
			{	
				memset(szReadString, 0x0, (size_t)iRecordSize + 1);
		
				memcpy(szReadString, szReadTempLog, (size_t)iRecordSize);
				szReadTempLog = szReadTempLog + iRecordSize;
				Web_AnalyzeHisLogLine(szReadString, pMatchLog);
				
				if((pMatchLog->tmLogTime - fromTime) > 0 && (toTime - pMatchLog->tmLogTime) > 0 )
				{	
					pMatchLog++;
					iMatchRecord++;
				}
			

				if(i == iRecords/100)
				{
					RunThread_Heartbeat(RunThread_GetId(NULL));
				}
			}
			*ppHisLog = (void *)pTMatchLog;
		}
	}
	printf("**********I'm here!!!\n");
	
	DELETE(szReadLog);
	szReadLog = NULL;
	
	if(iMatchRecord <= 0 && pMatchLog != NULL)
	{
		DELETE(pMatchLog);
		pMatchLog = NULL;
	}

	/*end_time = time((time_t *)0);
	printf("\n__The Time 2 is:%ld___\n",end_time);
	printf("\n__Elapsed Time is:%lf__\n",difftime(end_time,the_time));*/

	return iMatchRecord;
}


#define	LEN_INFO_LEVEL	6
#define	LEN_LOG_TIME	17
#define	LEN_RECORDER	12
#define	LEN_THREAD_ID	8

/*==========================================================================*
 * FUNCTION :  Web_AnalyzeHisLogLine
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szReadLine:
				OUT HIS_READ_LOG *szAnalyzeString:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/


static time_t Web_AnalyzeHisLogLine(IN char *szReadLine, 
									OUT HIS_MATCH_LOG *szAnalyzeString)
{
	ASSERT(szReadLine);
	int			iPostition = 0;
	char		*pSearchValue = NULL;
	struct tm	p_tm;
	char		szTempBuf[10];
	char		szTimeStr[32];

	/*InfoLevel*/
	strncpyz(szAnalyzeString->szInfoLevel, szReadLine, LEN_INFO_LEVEL);
	szReadLine = szReadLine + LEN_INFO_LEVEL;

	/*time*/
	pSearchValue = strchr(szReadLine, ' ');
	pSearchValue = pSearchValue + 1;
	pSearchValue = strchr(pSearchValue, ' ');
	iPostition = pSearchValue - szReadLine;

	//printf("iPostition = %d\n", iPostition);
	if(iPostition == LEN_LOG_TIME)
	{
		//Transfer time
		strncpyz(szTimeStr, szReadLine, iPostition + 1);
		strptime(szTimeStr, "%y-%m-%d %H:%M:%S", &p_tm);
		szAnalyzeString->tmLogTime = mktime(&p_tm);
	}
	else
	{
		szAnalyzeString->tmLogTime = -1;	
	}
	szReadLine += LEN_LOG_TIME + 1;
	
	/*TaskName*/
	strncpyz(szAnalyzeString->szTaskName, szReadLine, LEN_RECORDER);
	szReadLine += LEN_RECORDER + 1;

	/*Run thread ID*/
	//strncpyz(szTempBuf, szReadLine, LEN_THREAD_ID + 1);
	//szAnalyzeString->RunThread_GetId = atol(szTempBuf);
	szReadLine += LEN_THREAD_ID + 1;

	while((pSearchValue = strchr(szReadLine, 34)) != NULL )//delete "
	{
		*pSearchValue = 39;// '
	}

	while((pSearchValue = strrchr(szReadLine, '\n')) != NULL || (pSearchValue = strrchr(szReadLine, '\r')) != NULL)
	{
		*pSearchValue = ' ';
	}
	 
	strncpyz(szAnalyzeString->szInformation, szReadLine,(int)sizeof(szAnalyzeString->szInformation));

	return TRUE;
}

/*==========================================================================*
 * FUNCTION :    Web_MakeQueryHisDataBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN int iEquipID:
				IN time_t fromTime:
				IN time_t toTime:
				IN int iLanguage:
				OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_MakeQueryHisDataBuffer(IN int iEquipID, 
									  IN time_t fromTime, 
									  IN time_t toTime, 
									  IN int iLanguage, 
									  OUT char **ppBuf)
{
	int						iLen			= 0, iFileLen = 0;
	HIS_DATA_RECORD			*pReturnData	= NULL;
	HIS_DATA_TO_RETURN		*pTraiReturnData= NULL;
	HIS_DATA_TO_RETURN		*pDeletePtr		= NULL;
	char					*pMakeBuf		= NULL, *pMakeFileBuf = NULL;
	int						iBufLen			= 0;
	int						iDataLen		= 0;
	char					szTime[32], szTime2[32];

	if((iBufLen = Web_QueryHisData(toTime,fromTime, iEquipID, 
							QUERY_HIS_DATA, (void *)&pReturnData)) <= 0 )
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_MakeQueryHisDataBuffer :iBufLen : %d\n", iBufLen);
#endif 
		DELETE(pReturnData);
		pReturnData = NULL;

		return FALSE;
	}

	iDataLen = iBufLen * MAX_HIS_DATA_LEN;

	pMakeBuf = NEW(char,iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeBuf, 0x0, (size_t)iDataLen);

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);


	if(Web_TransferHisDataIDToName(pReturnData,iBufLen, iLanguage,
										(void *)&pTraiReturnData) == FALSE)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_TransferHisDataIDToName :FALSE\n");
#endif 
		DELETE(pReturnData);
		pReturnData = NULL;

		DELETE(pMakeBuf);
		pMakeBuf = NULL;

		return FALSE;
	}

	DELETE(pReturnData);
	pReturnData = NULL;

	pDeletePtr = pTraiReturnData;

	iLen += sprintf(pMakeBuf + iLen,"%2d", 1);
	int i = 1;
	int j = 500;
	while(pTraiReturnData != NULL && iBufLen > 0)
	{

		TimeToString(pTraiReturnData->tSignalTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));
		if(j > 0)
		{
			iLen += sprintf(pMakeBuf + iLen, "\"%16s\",\t\"%32s\",\t\"%32s\",\t\"%8s\",\t%ld,\t\n", 
				pTraiReturnData->szEquipName, 
				pTraiReturnData->szSignalName,
				pTraiReturnData->szSignalVal, 
				pTraiReturnData->szUnit,
				pTraiReturnData->tSignalTime);//,szTime,
			//pTraiReturnData->dwActAlarmID );
			j--;

		}

		iFileLen += sprintf(pMakeFileBuf + iFileLen, "\t%d \t%-32s \t%-32s  \t%-32s \t%-8s \t%-32s\t\n", 
												i,
												pTraiReturnData->szEquipName, 
												pTraiReturnData->szSignalName,
												pTraiReturnData->szSignalVal, 
												pTraiReturnData->szUnit,
												szTime);
												
		pTraiReturnData++;
		iBufLen--;
		i++;
	}
    char		*pSearchValue = NULL;
	
	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
	*pSearchValue = 32;   //replace ',' with ' '

	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryHisDataBuffer]******%s\n", pMakeBuf);
#endif 
	int iAccess;
	iAccess = access(WEB_LOG_DIR, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL && iFileLen > 1)
	{
		char	szFileTitle[512];

		TimeToString(fromTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "Query History Data\nQuery EquipID:[%s] \nQuery Time:from %s to %s\nTotal %d record(s) queried\n%s \t%-32s \t%-32s \t%-32s \t%-8s \t%-32s \t\n",
						(iEquipID > 0) ? Web_GetAEquipName(iEquipID, iLanguage) : "All Devices",
						szTime,
						szTime2,
						i-1,
						"Index",
						"Device Name", 
						"Signal Name", 
						"Value", 
						"Unit",
						"Time");

		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);

		if(iAccess < 0)
		{
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
			system(szCommandStr);
		}

		
	}

	Web_MakeHisData();

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;
	
	DELETE(pDeletePtr);
	pDeletePtr = NULL;
	return TRUE;
}

/*==========================================================================*
* FUNCTION :    Web_MakeHisData
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iEquipID:
IN time_t fromTime:
IN time_t toTime:
IN int iLanguage:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
*==========================================================================*/
static int Web_MakeHisData()
{
	time_t fromTime;
	time_t toTime;
	HIS_DATA_RECORD			*pReturnData	= NULL;
	HIS_DATA_RECORD			*pReturnDataTemp	= NULL;// backup of pReturnData
	int						iBufLen			= 0,iFileLen = 0;
	int				iBufLen1 = 0;  //backup of iBufLen
	int						iDataLen		= 0;
	char				*pMakeFileBuf = NULL;
	char					szTime[32];
	struct tm gmTime;
	HIS_DATA_RECORD_LOAD		*pLoadData = NULL, *pLoadData2 = NULL;//pLoadData2 is the backup of pLoadData
	HIS_DATA_RECORD_LOAD		*pLoadData1 = NULL, *pLoadData3 = NULL;//pLoadData3 is the backup of pLoadData1
	HIS_DATA_RECORD_LOAD            LoadData;
	int iBufLenLoad = 0;			//the length of pLoadData
	int iBufLenLoad1 = 0;			//the length of pLoadData1
	int i, j, k, iday;
	int l;


#define THREE_MONTH_DAY	    90

	toTime = time((time_t *)0);
	fromTime = toTime - 7776000;
	int iEquipID = 1;

	if((iBufLen = Web_QueryHisData(toTime,fromTime, iEquipID, 
		QUERY_HIS_DATA, (void *)&pReturnData)) <= 0 )
	{

		DELETE(pReturnData);
		pReturnData = NULL;

		return FALSE;
	}

	iBufLen1 = iBufLen;

	pReturnDataTemp = pReturnData;

	// find the number of element which iSignalID is 2 or 182(load current)
	while(pReturnDataTemp != NULL && iBufLen > 0)
	{
		if(pReturnDataTemp->iSignalID == 2 || pReturnDataTemp->iSignalID == 182)
		{
		    iBufLenLoad++;	
		    //printf("%ld, %.2f\n", pReturnDataTemp->tmSignalTime, pReturnDataTemp->varSignalVal.fValue);// for debug
		}
		
		pReturnDataTemp++;
		iBufLen--;

	}

	pReturnDataTemp = pReturnData;
	iBufLen = iBufLen1;

	pLoadData2 = pLoadData = NEW(HIS_DATA_RECORD_LOAD, iBufLenLoad); //create pLoadData
	pLoadData3 = pLoadData1 = NEW(HIS_DATA_RECORD_LOAD, iBufLenLoad); //create pLoadData1

	//save the element which iSignalID is 2 or 182(load current) to pLoadData
	while(pReturnDataTemp != NULL && iBufLen > 0)
	{
	    if(pReturnDataTemp->iSignalID == 2 || pReturnDataTemp->iSignalID == 182)
	    {
		gmtime_r(&(pReturnDataTemp->tmSignalTime), &gmTime);
		pLoadData->tm1Date.tm_mday = gmTime.tm_mday;
		pLoadData->tm1Date.tm_mon = gmTime.tm_mon;
		pLoadData->tm1Date.tm_year = gmTime.tm_year;
		pLoadData->floadCurrent = pReturnDataTemp->varSignalVal.fValue;
		pLoadData->tmSignalTime = pReturnDataTemp->tmSignalTime;
		pLoadData->bflag = TRUE;
		pLoadData++;
	    }
	    pReturnDataTemp++;
	    iBufLen--;
	}

	pLoadData = pLoadData2;

	//for debug begin
	/*for (l = 0; l < iBufLenLoad; l++)
	{
	    printf("[%d] year %d month %d day %d load %f\n", l, (pLoadData + l)->tm1Date.tm_year, (pLoadData + l)->tm1Date.tm_mon, 
		(pLoadData + l)->tm1Date.tm_mday, (pLoadData + l)->floadCurrent);
	}*/
	//for debug end

	iDataLen = iBufLenLoad * MAX_HIS_DATA_LEN;

	pMakeFileBuf = NEW(char, iDataLen + 100);// create pMakeFileBuf
	if(pMakeFileBuf == NULL)
	{
	    DELETE(pReturnData);
	    pReturnData = NULL;
	    return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);

	iFileLen += sprintf(pMakeFileBuf + iFileLen, "var data = [");

	iday = 0;
	// find the peak load current of the same day and record it in pMakeFileBuf
	for(i = 0; i < iBufLenLoad; i++)
	{
	    //printf("i = %d\n", i);// for debug
	    if((pLoadData + i)->bflag == TRUE)
	    {
		LoadData.tm1Date.tm_mday = (pLoadData + i)->tm1Date.tm_mday;
		LoadData.tm1Date.tm_mon = (pLoadData + i)->tm1Date.tm_mon;
		LoadData.tm1Date.tm_year = (pLoadData + i)->tm1Date.tm_year;
		LoadData.tmSignalTime = (pLoadData + i)->tmSignalTime;
		LoadData.floadCurrent = (pLoadData + i)->floadCurrent;

		//for debug begin
		printf("LoadData year %d month %d day %d load %f\n", LoadData.tm1Date.tm_year, LoadData.tm1Date.tm_mon,
		    LoadData.tm1Date.tm_mday, LoadData.floadCurrent);
		//for debug end

		// put the element belong to the same day to pLoadData1, the length is iBufLenLoad1
		iBufLenLoad1 = 0;
		for(j = 0; j < iBufLenLoad; j++)
		{
		    //printf("j = %d\n", j);// for debug
		    if(((pLoadData + j)->bflag == TRUE) &&
			(LoadData.tm1Date.tm_mday == (pLoadData + j)->tm1Date.tm_mday) &&
			(LoadData.tm1Date.tm_mon == (pLoadData + j)->tm1Date.tm_mon) &&
			(LoadData.tm1Date.tm_year == (pLoadData + j)->tm1Date.tm_year))
		    {
			(pLoadData + j)->bflag = FALSE;// if the element has been compared, set the bflag to be FALSE, in order to neglect it
			pLoadData1->tmSignalTime = (pLoadData + j)->tmSignalTime;
			pLoadData1->floadCurrent = (pLoadData + j)->floadCurrent;
			pLoadData1++;
			iBufLenLoad1++;
		    }
		}

		pLoadData1 = pLoadData3;
		//for debug begin
		/*for (l = 0; l < iBufLenLoad1; l++)
		{
		    printf("[%d] pLoadData1 tmSignalTime %ld load %f\n", l, (pLoadData1 + l)->tmSignalTime, (pLoadData1 + l)->floadCurrent);
		}*/
		//for debug end

		// find the peak load current of pLoadData1
		for(k = 0; k < iBufLenLoad1; k++)
		{
		    //printf("k = %d\n", k);// for debug
		    if(LoadData.floadCurrent < (pLoadData1 + k)->floadCurrent)
		    {
			LoadData.floadCurrent = (pLoadData1 + k)->floadCurrent;
			LoadData.tmSignalTime = (pLoadData1 + k)->tmSignalTime;
		    }
		}

		//for debug begin
		printf("peak load current tmSignalTime %ld load %f\n", LoadData.tmSignalTime, LoadData.floadCurrent);
		//for debug end

		// record the peak load current and relative date in pMakeFileBuf
		TimeToString(LoadData.tmSignalTime, TIME_HISDATA_FMT, 
		    szTime, sizeof(szTime));
		iFileLen += sprintf(pMakeFileBuf + iFileLen, "[\"%-32s\",%.2f],", 
		    szTime,
		    LoadData.floadCurrent
		    );
		//printf("iday is %d\n", iday);// for debug
		iday++;
		if(iday > THREE_MONTH_DAY)// at most record 90 days' data
		{
		    break;
		}
	    }
	}
	iFileLen += sprintf(pMakeFileBuf + iFileLen, "]");

#define WEB_HISDATA_DIR "/var/hisdata.js"
	int iAccess;
	iAccess = access(WEB_HISDATA_DIR, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_HISDATA_DIR,"wb")) != NULL && iFileLen > 1 && pReturnDataTemp != NULL)
	{

		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);

		if(iAccess < 0)
		{
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_HISDATA_DIR);
			system(szCommandStr);
		}


	}

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;

	DELETE(pReturnData);
	pReturnData = NULL;


	return TRUE;
}


/*==========================================================================*
 * FUNCTION :    Web_MakeQueryStatDataBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN int iEquipID:
				IN time_t fromTime:
				IN time_t toTime:
				IN int iLanguage:
				OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_MakeQueryStatDataBuffer(IN int iEquipID, 
									   IN time_t fromTime, 
									   IN time_t toTime, 
									   IN int iLanguage, 
									   OUT char **ppBuf)
{
	int							iLen = 0, iFileLen = 0;
	HIS_STAT_RECORD				*pReturnData		= NULL;
	HIS_STATDATA_TO_RETURN		*pTraiReturnData	= NULL;
	HIS_STATDATA_TO_RETURN		*pDeletePtr			= NULL;
	char						*pMakeBuf			= NULL, *pMakeFileBuf = NULL;
	int							iBufLen	= 0;
	char						szTime1[32],szTime2[32],szTime3[32];
	


	if((iBufLen = Web_QueryHisData(toTime, fromTime, iEquipID,
									QUERY_STAT_DATA, (void *)&pReturnData)) <= 0)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_QueryHisData : iBufLen : %d\n", iBufLen);
#endif 
	
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}

	int iDataLen = iBufLen * MAX_STAT_DATA_LEN;

	pMakeBuf = NEW(char,iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);


	if(Web_TransferStatDataIDToName(pReturnData, iBufLen, iLanguage, 
											(void *)&pTraiReturnData) == FALSE)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_TransferStatDataIDToName : false\n");
#endif 
		DELETE(pMakeBuf);
		pMakeBuf = NULL;

		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		return FALSE;
	}

	DELETE(pReturnData);
	pReturnData = NULL;

	iLen += sprintf(pMakeBuf + iLen,"%2d", 1);
	pDeletePtr = pTraiReturnData;
	while(pTraiReturnData != NULL && iBufLen > 0)
	{
		TimeToString(pTraiReturnData->tmStatTime, TIME_CHN_FMT, 
				szTime1, sizeof(szTime1));
		TimeToString(pTraiReturnData->tmMaxValTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));
		TimeToString(pTraiReturnData->tmMinValTime, TIME_CHN_FMT, 
				szTime3, sizeof(szTime3));

		iLen += sprintf(pMakeBuf + iLen,"\t \"%16s\",\t\"%16s\",\t%s,\t%s,\t%ld,\t%s,\t%ld,\t%s,\t%ld,\t\"%8s\",\t\n",	
												pTraiReturnData->szEquipName,
												pTraiReturnData->szSignalName,
												pTraiReturnData->szCurrentVal,
												pTraiReturnData->szAverageVal, 	
												pTraiReturnData->tmStatTime,//szTime1,
												pTraiReturnData->szMaxVal,
												pTraiReturnData->tmMaxValTime,//szTime2,
												pTraiReturnData->szMinVal,
												pTraiReturnData->tmMinValTime,//szTime3,
												pTraiReturnData->szUnit); 
		iFileLen += sprintf(pMakeFileBuf + iFileLen,"\t \"%16s\",\t\"%16s\",\t%s,\t%s,\t%s,\t%s,\t%s,\t%s,\t%s,\t\"%8s\",\t\n",	
												pTraiReturnData->szEquipName,
												pTraiReturnData->szSignalName,
												pTraiReturnData->szCurrentVal,
												pTraiReturnData->szAverageVal, 	
												szTime1,
												pTraiReturnData->szMaxVal,
												szTime2,
												pTraiReturnData->szMinVal,
												szTime3,
												pTraiReturnData->szUnit); 
		

		
		pTraiReturnData++;
		iBufLen--;
	}


	
    char		*pSearchValue = NULL;
	
	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryStatDataBuffer]******%s\n", pMakeBuf);
#endif 
	int iAccess;
	iAccess = access(WEB_LOG_DIR, R_OK|W_OK);
	
	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL && iFileLen > 1)
	{

		char	szFileTitle[512];

		TimeToString(fromTime, TIME_CHN_FMT, 
				szTime1, sizeof(szTime1));

		TimeToString(toTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "Query statistics data\nQuery EquipID:[%s] \nQuery time:from %s to %s\n" \
							  "%16s \t%16s \t%16s \t%s \t%16s \t%16s \t%16s \t%16s \t%16s \t%16s\t\n",
						(iEquipID > 0) ? Web_GetAEquipName(iEquipID, iLanguage) : "All device",
						szTime1,
						szTime2,
						"Device name", 
						"Signal name",
						"Current value",
						"Average value",
						"Statistics time",
						"Max value",
						"Max time",
						"Min value",
						"Min time",
						"Unit");
		fwrite(szFileTitle, strlen(szFileTitle), 1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);

		fclose(fp);
		if(iAccess < 0)
		{
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
			system(szCommandStr);
		}
	}


	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;

	DELETE(pDeletePtr);
	pDeletePtr = NULL;

	return TRUE;
}


/*==========================================================================*
 * FUNCTION :    Web_MakeQueryAlarmDataBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN int iEquipID:
				IN time_t fromTime:
				IN time_t toTime:
				IN int iLanguage:
				IN char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_MakeQueryAlarmDataBuffer(IN int iEquipID, 
										IN time_t fromTime, 
										IN time_t toTime, 
										IN int iLanguage, 
										IN char **ppBuf)
{
	int							iLen = 0, iFileLen = 0;
	HIS_ALARM_RECORD			*pReturnData		= NULL;
	HIS_ALARM_TO_RETURN			*pTraiReturnData	= NULL;	
	char						*pMakeBuf			= NULL, *pMakeFileBuf = NULL;
	int							iDataLen = 0;
	int							iBufLen = 0;
	void						*pDeletePtr = NULL;
	char						szTime1[32], szTime2[32];
	char						*szLevelName[] = {"NA", "OA", "MA", "CA"};
	int							nAlarms;
	int				iMatchNumber = 0;

	/*Query alarm*/
	//TRACE("In Web_MakeQueryAlarmDataBuffer, fromTime is %d,toTime is %d\n",fromTime,toTime);
	if(( iBufLen = Web_QueryHisAlarm(fromTime, toTime, iEquipID,
													(void *)&pReturnData)) <= 0)
	{
		if(pReturnData != NULL)
		{
			DELETE(pReturnData);
			pReturnData = NULL;
		}
		return FALSE;
	}
    TRACE("\n QueryAlarmData iBufLen: [%d]\n", iBufLen);
#ifdef _SHOW_WEB_INFO
	//TRACE("iBufLen: [%d]\n", iBufLen);
#endif 
	
	iDataLen = iBufLen * MAX_ALARM_DATA_LEN;
	pMakeBuf = NEW(char, iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeBuf, 0, (size_t)iDataLen);

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);

	/*Transfer the match language*/
	if(Web_TransferAlarmDataIDToName((void *)pReturnData, iBufLen, iLanguage,
										(void *)&pTraiReturnData) == FALSE)
	{
		//TRACE("Web_TransferAlarmDataIDToName : FALSE");

		DELETE(pReturnData);
		pReturnData = NULL;

		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		return FALSE;
	}
	
	DELETE(pReturnData);
	pReturnData = NULL;


	nAlarms = 0;
	pDeletePtr = pTraiReturnData;
	
	//sort
	//Web_SortHisAlarmData(pTraiReturnData, iBufLen);

	iLen += sprintf(pMakeBuf + iLen,"%2d", 1);
	int		i = 1;
	while(pTraiReturnData != NULL && iBufLen > 0)
	{
		TimeToString(pTraiReturnData->tmStartTime, TIME_CHN_FMT, 
				szTime1, sizeof(szTime1));
		TimeToString(pTraiReturnData->tmEndTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));
		if(iMatchNumber < 500)
		{
			iLen += sprintf(pMakeBuf + iLen,"\"%16s\",\t\"%16s\",\t%d, \t%ld,\t%ld,\t\n",
										pTraiReturnData->szEquipName,
										pTraiReturnData->szAlarmName,
										pTraiReturnData->byLevel,
										//pTraiReturnData->fTriggerValue,
										pTraiReturnData->tmStartTime,//szTime1,
										pTraiReturnData->tmEndTime);//szTime2); 
		}
		iMatchNumber++;
		// maofuhua changed the format. 2005-3-15
		nAlarms++;
		// removed the value column.
		iFileLen +=sprintf(pMakeFileBuf + iFileLen,"%5d \t%-32s \t%-32s \t%-5s \t%-15s \t%-15s\n",
										nAlarms,
										pTraiReturnData->szEquipName,
										pTraiReturnData->szAlarmName,
										szLevelName[pTraiReturnData->byLevel],
										//pTraiReturnData->fTriggerValue,
										szTime1,
										szTime2);

		pTraiReturnData++;
		iBufLen--;
	}

    char		*pSearchValue = NULL;
	
	pSearchValue = strrchr(pMakeBuf, 44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryAlarmDataBuffer]******%s\n", pMakeBuf);
#endif 
	
	int iAccess;
	iAccess = access(WEB_LOG_DIR, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL && iFileLen > 1)
	{
		
		char	szFileTitle[512];

		TimeToString(fromTime, TIME_CHN_FMT, 
				szTime1, sizeof(szTime1));
		TimeToString(toTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

		// removed the value column.
		sprintf(szFileTitle, "Query ACU plus Alarm Data\nQuery EquipID: %s\n" \
							  "Query Time: from %s to %s\n"	\
							  "Total %d alarm(s) queried.\n\n" \
							  "%5s \t%-32s \t%-32s \t%s \t%-15s \t%-15s\n",
							  (iEquipID > 0) ? Web_GetAEquipName(iEquipID, iLanguage) : "All device",
								szTime1,//ctime(&fromTime),
								szTime2,//ctime(&toTime),
								nAlarms,
								"Index",
								"Device Name",
								"Signal Name",
								"Level",
								//"Value",
								"Start Time",
								"End Time");

		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);
		if(iAccess < 0)
		{
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
			system(szCommandStr);
		}
		
		
	}

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;

	DELETE(pDeletePtr);
	pDeletePtr = NULL;
	
	//fclose(fp);

	return TRUE;
}



/*==========================================================================*
 * FUNCTION :    Web_MakeQueryControlDataBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN time_t fromTime:
				IN int toTime:
				IN int iLanguage:
				OUT char **ppBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_MakeQueryControlDataBuffer(IN time_t fromTime , 
										  IN time_t toTime, 
										  IN int iLanguage, 
										  OUT char **ppBuf)
{
	int							iLen = 0, iFileLen = 0;
	HIS_CONTROL_RECORD			*pReturnData		= NULL;
	HIS_CONTROL_TO_RETURN		*pTraiReturnData	= NULL;
	char						*pMakeBuf			= NULL, *pMakeFileBuf = NULL;
	int							iBufLen = 0;
	int							iDataLen = 0;
	HIS_CONTROL_TO_RETURN		*pDeletePtr = NULL;
	char						szTime[32],szTime2[32];

	
	if((iBufLen = Web_QueryControlCommand(fromTime, toTime, 
										(void **)&pReturnData)) <= 0)
	{
		return FALSE;
	}

	iDataLen = iBufLen * MAX_CONTROL_DATA_LEN;	
	pMakeBuf = NEW(char,iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	
	memset(pMakeBuf, 0, (size_t)iDataLen);

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);

	
	if(Web_TransferControlIDToName((void *)pReturnData, iBufLen, iLanguage, 
										(void *) &pTraiReturnData) == FALSE)
	{
		//TRACE("Web_TransferControlIDToName false\n");
		DELETE(pReturnData);
		pReturnData = NULL;
		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		DELETE(pMakeFileBuf);
		pMakeFileBuf = NULL;
		return FALSE;
	}
	

	DELETE(pReturnData);
	pReturnData = NULL;

	pDeletePtr = pTraiReturnData;

	iLen += sprintf(pMakeBuf + iLen,"%2d", 1);
	int		i = 1;
	while(pTraiReturnData != NULL && iBufLen > 0)
	{
		TimeToString(pTraiReturnData->tmControlTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		iLen += sprintf(pMakeBuf + iLen,"\"%s\",\"%s\",%d,\"%s\",\"%8s\",%ld,\"%s\",\"%-s\",%d,\n",
													pTraiReturnData->szEquipName,
													pTraiReturnData->szSignalName,
													pTraiReturnData->iValueType,
													pTraiReturnData->szControlVal,
													pTraiReturnData->szUnit,
													pTraiReturnData->tmControlTime,//szTime,
													pTraiReturnData->szCtrlSenderName,
													pTraiReturnData->szSenderType,
													pTraiReturnData->iControlResult);

		iFileLen += sprintf(pMakeFileBuf + iFileLen,"%8d \t%-32s \t%-32s \t%-32s \t%-8s \t%-32s \t%-32s \t%-32s \t%-16s \n",
													i,
													pTraiReturnData->szEquipName,
													pTraiReturnData->szSignalName,
													pTraiReturnData->szControlValForSave,
													pTraiReturnData->szUnit,
													szTime,
													pTraiReturnData->szCtrlSenderName,
													pTraiReturnData->szSenderType,
													(pTraiReturnData->iControlResult == 0) ? "Successful" :"Failure");

		pTraiReturnData++;
		iBufLen--;
		i++;
	}
		

    char		*pSearchValue;
	
	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}
	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryControlDataBuffer]******%s\n", pMakeBuf);
#endif 
	
	int iAccess;
	iAccess = access(WEB_LOG_DIR, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL && iFileLen > 1)
	{
		char	szFileTitle[512];
		TimeToString(fromTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "Query History Control Log\n" \
							"Query Time:from %s to %s\nTotal %d record(s) queried\n " \
							"\n%8s \t%-32s \t%-32s \t%-32s \t%-8s \t%-32s \t%-32s \t%-32s \t%-16s\n",
							szTime,
							szTime2,
							i-1,
							"Index",
							"Device Name", 
							"Signal Name", 
							"Value", 
							"Unit",
							"Time",
							"Sender Name",
							"Sender Type",
							"Send Result");

		fwrite(szFileTitle, strlen(szFileTitle),1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);
		if(iAccess < 0)
		{
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
			system(szCommandStr);
		}

		
	}

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;


	DELETE(pDeletePtr);
	pDeletePtr = NULL;

	return TRUE;
}


/*==========================================================================*
 * FUNCTION :    Web_MakeQueryHisLogBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN time_t fromTime:
				IN time_t toTime:
				IN char **ppBuf:
 * RETURN   :	TRUE or FALSE
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_MakeQueryHisLogBuffer(IN time_t fromTime,
									 IN time_t toTime,
									 IN char **ppBuf)
{
	int							iLen = 0,iFileLen = 0;
	HIS_MATCH_LOG				*pReturnData	= NULL;
	HIS_MATCH_LOG				*pDeletePtr		= NULL;
	char						*pMakeBuf		= NULL, *pMakeFileBuf = NULL;
	int							iMatchRecord = 0;
	char						szTime[32];
	int							i = 0;
	
	time_t		the_time,end_time;

	
	/*Get Log*/
	if((iMatchRecord = Web_QueryHisLog(fromTime, toTime,(void *)&pReturnData)) <= 0)
	{
		TRACE("Web_QueryHisLog : FALSE");

		return FALSE;
	}
	
	
	pMakeBuf = NEW(char,iMatchRecord * MAX_DATA_LOG_LEN);
	pMakeFileBuf = NEW(char, iMatchRecord * MAX_DATA_LOG_LEN);
	if(pMakeBuf == NULL || pMakeFileBuf == NULL)
	{
		if(pReturnData != NULL)
		{
			DELETE(pReturnData);
			pReturnData = NULL;
		}
		return FALSE;
	}

	pDeletePtr = pReturnData;

	/*the_time = time((time_t *)0);
	printf("\n__The Time 1 is:%ld___\n",the_time);*/
	iLen += sprintf(pMakeBuf + iLen,"%2d", 0);
	while(pReturnData != NULL && iMatchRecord > 0 )
	{
		TimeToString(pReturnData->tmLogTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		if(iMatchRecord <= 500)
		{
			iLen += sprintf(pMakeBuf + iLen,"\"%-32s\",\t\"%-16s\",\t\"%d\",\t\"%s\",\n",
				pReturnData->szTaskName,
				pReturnData->szInfoLevel,
				(int)pReturnData->tmLogTime, 
				Cfg_RemoveWhiteSpace(pReturnData->szInformation));
		}
		

		iFileLen += sprintf(pMakeFileBuf + iFileLen,"%-16s \t%-8s \t%32s \t%s \n",
										pReturnData->szTaskName,
										pReturnData->szInfoLevel,
										szTime, 
										Cfg_RemoveWhiteSpace(pReturnData->szInformation));

		iMatchRecord--;
		pReturnData++;
		i++;
	}

	char		*pSearchValue = NULL;
	
	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	int iAccess;
	iAccess = access(WEB_LOG_DIR, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL)
	{
		char	szFileTitle[512];
		char	szTime[32],szTime2[32];

		TimeToString(fromTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "Query System Log\nQuery Time:from %s to %s\nTotal %d record(s) queried\n\n",
						szTime,
						szTime2,
						i-1);
		
		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);

		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);
		if(iAccess < 0)
		{
		
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
			system(szCommandStr);
		}
	}

	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryHisLogBuffer]**[%s]****\n", pMakeBuf);
#endif 
	

	DELETE(pDeletePtr);
    DELETE(pMakeFileBuf);
    pMakeFileBuf = NULL;
	pDeletePtr = NULL;

	/*end_time = time((time_t *)0);
	printf("\n__The Time 2 is:%ld___\n",end_time);
	printf("\n__Elapsed Time is:%lf__\n",difftime(the_time,end_time));*/

	return TRUE;
}


/*==========================================================================*
 * FUNCTION :   Web_fnCompareQueryCondition
 * PURPOSE  :	Compare Condition	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   const void *pRecord:
				const void *pTCondition:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
static int Web_fnCompareQueryCondition(IN const void *pRecord, 
									   IN const void *pTCondition)
{
	HIS_GENERAL_CONDITION *pCondition = (HIS_GENERAL_CONDITION *)pTCondition;
	static unsigned int		lnHisDataCount = 0;
	static unsigned int		lnStatDataCount = 0;
	static unsigned int		lnHisAlarmCount = 0;
	static unsigned int		lnHisControlCount = 0;

	//TRACE("Come here compare data type is %d.\n", (int)pCondition->iDataType);

	if((pRecord == NULL) || (pTCondition == NULL))
	{
		return FALSE;
	}
	
	switch((int)pCondition->iDataType)
	{
		
		case QUERY_HIS_DATA:
		{	
			/*History data*/
			//TRACE("*********QUERY_HIS_DATA*************\n");
			HIS_DATA_RECORD *pRecordData	= (HIS_DATA_RECORD *)pRecord;
			lnHisDataCount++;
			if(lnHisDataCount % 200 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				lnHisDataCount = 0;
			}

			if(pRecordData->iEquipID == pCondition->iEquipID || 
												pCondition->iEquipID < 0)
			{
				if(((pRecordData->tmSignalTime - pCondition->tmFromTime) > 0) && 
					((pCondition->tmToTime - pRecordData->tmSignalTime) > 0))
				{
					
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
            break;
		}
		case QUERY_STAT_DATA:
		{
			/*Statical data*/
			//TRACE("*********QUERY_STAT_DATA*************\n");
			HIS_STAT_RECORD		*pRecordData = (HIS_STAT_RECORD *)pRecord;
			
			lnStatDataCount++;
			if(lnStatDataCount % 200 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				lnStatDataCount = 0;
			}

			if(pRecordData->iEquipID == pCondition->iEquipID || 
												pCondition->iEquipID < 0)
			{
				if((pRecordData->tmStatTime - pCondition->tmFromTime) > 0 && 
					( pCondition->tmToTime - pRecordData->tmStatTime) > 0)
				{	
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
					return FALSE;
			}
            break;

		}
		case QUERY_HISALARM_DATA:
		{
			/*Query history alarm*/
			//TRACE("*********QUERY_HISALARM_DATA*************\n");
			HIS_ALARM_RECORD	*pRecordData = (HIS_ALARM_RECORD *)pRecord;
			
			lnHisAlarmCount++;
			if(lnHisAlarmCount % 200 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				lnHisAlarmCount = 0;
			}

			/*TRACE("pCondition->iEquipID is %d.pRecordData->tmStartTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
				pCondition->iEquipID,pRecordData->tmStartTime, pCondition->tmFromTime, pCondition->tmToTime);

			TRACE("pCondition->iEquipID is %d.Time Interval is %d,Timew is %d\n",
				pCondition->iEquipID,(pRecordData->tmStartTime - pCondition->tmFromTime), 
				(pCondition->tmToTime - pRecordData->tmStartTime));*/

			if(pRecordData->iEquipID == pCondition->iEquipID || 
													pCondition->iEquipID < 0)
			{
				if((pRecordData->tmStartTime - pCondition->tmFromTime) > 0 && 
					( pCondition->tmToTime - pRecordData->tmStartTime) > 0)
				{	
                   /* TRACE("pCondition->iEquipID is %d.pRecordData->tmStartTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
                        pCondition->iEquipID,pRecordData->tmStartTime, pCondition->tmFromTime, pCondition->tmToTime);*/
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
					return FALSE;
			}
            break;
			
		}
		case QUERY_CONTROLCOMMAND_DATA:
		{
			 
			/*query control command log*/
			//TRACE("*********QUERY_CONTROLCOMMAND_DATA*************\n");
			HIS_CONTROL_RECORD		*pRecordData = (HIS_CONTROL_RECORD *)pRecord;

			lnHisControlCount++;
			if(lnHisControlCount % 200 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				lnHisControlCount = 0;
			}

            
           /* TRACE("###########pCondition->iEquipID is %d.pRecordData->tmControlTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
                pCondition->iEquipID,pRecordData->tmControlTime, pCondition->tmFromTime, pCondition->tmToTime);*/

			if(pRecordData->iEquipID == pCondition->iEquipID || 
													pCondition->iEquipID < 0)
			{
				if((pRecordData->tmControlTime - pCondition->tmFromTime) > 0 && 
					( pCondition->tmToTime - pRecordData->tmControlTime) > 0)
				{	
                    /*TRACE("@@@@@@@@@@@@@@@@pCondition->iEquipID is %d.pRecordData->tmControlTime is %d,pCondition->tmFromTime is %d, pCondition->tmFromTime is %d\n",
                        pCondition->iEquipID,pRecordData->tmControlTime, pCondition->tmFromTime, pCondition->tmToTime);*/

					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
					return FALSE;
			}
            break;
		}
		case QUERY_DISEL_TEST:
		{
			WEB_GC_DSL_TEST_INFO	*pRecordData = (WEB_GC_DSL_TEST_INFO *)pRecord;
		
			if((pRecordData->tStartTime - pCondition->tmFromTime) > 0 && 
				( pCondition->tmToTime - pRecordData->tStartTime) > 0)
			{	
				return TRUE;
			}
			else
			{
				return FALSE;
			}

            break;
			
		}
		default:

			return FALSE;
			 

	}
	
}



/*==========================================================================*
 * FUNCTION :   Web_transferCharToTime 
 * PURPOSE  :	Transfer char to time
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN char *pTime: input time need to be transfered;
 * RETURN   :	The transfered time
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-29 11:20
 *==========================================================================*/
	
static time_t Web_transferCharToTime(IN char *pTime,IN BOOL bAscending)
{
	ASSERT(pTime);
	
	//struct tm	p_tm;	
	//char		*szTime = NULL;
	////TRACE("pTime : %s\n", pTime);
	//szTime = Cfg_RemoveWhiteSpace(pTime);
	////TRACE("szTime : %s\n", szTime);
	//strptime(szTime, "%y-%m-%d", &p_tm);
	//return mktime(&p_tm);

	char		*pGetObject = NULL;
	char		szYear[5],szMonth[3],szDay[3];
	struct tm	tmReturn;
	pGetObject = Cfg_RemoveWhiteSpace(pTime);

	if(pGetObject == NULL)
	{
		return FALSE;
	}

	/*pTime example:"2004-12-12"*/
    /*get year*/
	strncpyz(szYear, pGetObject, sizeof(szYear));
	tmReturn.tm_year	= atoi(szYear) - TIME_LINUX_YEAR;
	//TRACE("szYear[%s]\n", szYear);
	pGetObject			= pGetObject + TIME_YEAR_LEN;
	
	/*get month*/
	char	*pSearchValue = NULL;
	int		iPosition = 0;
	pSearchValue = strchr(pGetObject,'-');
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - pGetObject;
		if(iPosition > 0)
		{
			strncpyz(szMonth, pGetObject, iPosition + 1);
			tmReturn.tm_mon		= atoi(szMonth) - TIME_LINUX_MONTH;
			//TRACE("szMonth[%s]\n", szMonth);
		}
		else
		{
			tmReturn.tm_mon		= 1 - TIME_LINUX_MONTH;
		}
	}
	pGetObject			= pGetObject + iPosition + 1;

	/*get day*/
	//pSearchValue = strchr(pGetObject,'-');
	//if(pSearchValue != NULL)
	//{
	//	iPosition = pSearchValue - pGetObject;

	strncpyz(szDay, pGetObject, (int)strlen(pGetObject) + 1);
	//TRACE("szDay[%s]\n", szDay);
	tmReturn.tm_mday = atoi(szDay);


	if(bAscending == TRUE)
	{
		tmReturn.tm_sec		= 0;
		tmReturn.tm_min		= 0;
		tmReturn.tm_hour	= 0;
	}
	else
	{
		tmReturn.tm_sec		= 59;
		tmReturn.tm_min		= 59;
		tmReturn.tm_hour	= 23;
	}
	tmReturn.tm_wday	= 0;
	tmReturn.tm_yday	= 0;
	tmReturn.tm_isdst	= -1;
	
	return mktime(&tmReturn);
}




/*==========================================================================*
 * FUNCTION :  Web_SendConfigureData
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *buf:
				IN char *filename:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_SendConfigureData(IN char *buf, 
								 IN char *filename)
{
	int			fd2;
	char		pGetData[100];
	char		*ptr = NULL;
	int			iLanguage = 0, iLen  = 0;;
	int			iCommandType;
	char		*pReturnBuf = NULL;
	int			iReturnValue = 0;
	char		szReturnResult[1600000];//Added by wj because buffer size is about 600k
	int			iModifyPassword = 0;
	char		szReturnProductInfo[12800];//Product Information
    int         iresult = 0;
    char        szUserName[33];
    char        szTempUserName[64];

	printf("buf is %s\n", buf);
	if((fd2 = open(filename,O_WRONLY )) > 0)
	{
		/*offset*/
		ptr = buf;
		ptr = ptr + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;
#define MAX_MODIFY_PASS_LEN				2
		/*GET IF MODIFY PASSWORD*/
		strncpyz(pGetData,ptr,MAX_MODIFY_PASS_LEN + 1);
		iModifyPassword = atoi(pGetData);
		ptr = ptr + MAX_MODIFY_PASS_LEN;
#define MAX_COMMAND_TYPE_LEN			2

#define MAX_USER_NAME_LEN              32

        strncpyz(szUserName,ptr,MAX_USER_NAME_LEN + 1);

        ptr = ptr + MAX_USER_NAME_LEN;

        TRACE("\n_____________USER_NAME______________:%s",szUserName);


		/*Get command type*/
		strncpyz(pGetData,ptr,MAX_COMMAND_TYPE_LEN + 1);
		iCommandType = atoi(pGetData);
		ptr = ptr + MAX_COMMAND_TYPE_LEN;
		
		if(iModifyPassword == SET_USER_PASSWORD)
		{
			iCommandType = SET_USER_PASSWORD;
		}
		/*Get Language type*/

		strncpyz(pGetData,ptr,MAX_LANGUAGE_TYPE_LEN + 1);
		iLanguage = atoi(pGetData);
        printf("\n_____________iLanguage______________:%d pGetData = %s\n",iLanguage, pGetData);
		ptr = ptr + MAX_LANGUAGE_TYPE_LEN;
		printf("\n______________iCommandType_____________%d\n", iCommandType);
		switch(iCommandType)
		{
			case GET_NETWORK_INFO:
				pReturnBuf = Web_MakeNetWorkInfoBuffer();
				if( Web_GetProtectedStatus() == TRUE)
				{
					sprintf(szReturnResult,"%2d%s",WEB_RETURN_PROTECTED_ERROR, pReturnBuf);
				}
				else
                {
					sprintf(szReturnResult,"%2d%s",99,pReturnBuf);
				}
#ifdef _SHOW_WEB_INFO
				//TRACE("GET_NETWORK_INFO : %s\n", szReturnResult);
#endif 
				
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}

				break;

			case SET_NETWORK_INFO:

				iReturnValue = Web_Modify_AcuIP_Addr(ptr);
				pReturnBuf = Web_MakeNetWorkInfoBuffer();
				sprintf(szReturnResult,"%2d%s",iReturnValue,pReturnBuf);
				
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}


				break;

			case SET_NMS_INFO:
				iReturnValue = Web_ModifyNMSPrivateConfigure(ptr);
				pReturnBuf = Web_MakeNMSPrivateConfigureBuffer();
				
				iLen += sprintf(szReturnResult + iLen,"%2d", iReturnValue);
				iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);
				
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				break;
			case GET_NMS_INFO:
				pReturnBuf = Web_MakeNMSPrivateConfigureBuffer();
				if(Web_GetProtectedStatus() == TRUE)
				{
					iLen += sprintf(szReturnResult + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR);
				}				
				else
				{
					iLen += sprintf(szReturnResult + iLen,"%2d", 1);
				}
				iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);
#ifdef _SHOW_WEB_INFO
				//TRACE("szReturnResult : %s\n", szReturnResult);
#endif 
				

				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}

				break;


			case GET_ESR_INFO:

				pReturnBuf = Web_MakeESRPrivateConfigreBuffer();

				
				if(pReturnBuf != NULL)
				{
					if(Web_GetProtectedStatus() == TRUE)
					{
						iLen += sprintf(szReturnResult + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
					}
					else
					{
						iLen += sprintf(szReturnResult + iLen,"%2d%s",99,pReturnBuf);
					}
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
					iLen += sprintf(szReturnResult + iLen,"%2d",1);
				}

				break;
			case SET_ESR_INFO:
				iReturnValue = Web_ModifyESRPrivateConfigure(ptr); 
				pReturnBuf = Web_MakeESRPrivateConfigreBuffer();

				iLen += sprintf(szReturnResult + iLen,"%2d", iReturnValue);
				

				if(pReturnBuf != NULL)
				{
					iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);

					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				else
				{
				}
				break;


			case GET_TIME_INFO:
				
				pReturnBuf = Web_MakeACUTimeSrv();
				if(Web_GetProtectedStatus() == TRUE)
				{
					iLen += sprintf(szReturnResult + iLen,"%2d%s",WEB_RETURN_PROTECTED_ERROR, pReturnBuf);
				}
				else
				{
					iLen +=sprintf(szReturnResult + iLen,"%2d%s",1, pReturnBuf);
				}
#ifdef _SHOW_WEB_INFO
				//TRACE("GET_TIME_INFO : %s\n", pReturnBuf);
#endif 
	
				
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				break;

			case SET_TIME_IP:
			{	
				iReturnValue = Web_SetACUTimeSrv(WEB_MODIFY_TIMESRV_IP, ptr);
				pReturnBuf = Web_MakeACUTimeSrv();
				sprintf(szReturnResult,"%2d%s",iReturnValue,pReturnBuf);
#ifdef _SHOW_WEB_INFO
				//TRACE("szReturnResult : %s\n", szReturnResult);
#endif 
				
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				break;
			}
			case SET_TIME_VALUE:
			{
				iReturnValue = Web_SetACUTimeSrv(WEB_MODIFY_TIME, ptr);
				
				pReturnBuf = Web_MakeACUTimeSrv();
				sprintf(szReturnResult,"%2d%s",iReturnValue,pReturnBuf);
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				break;

			}
			case GET_USER_INFO:
				pReturnBuf = Web_MakeACUUserInfoBuffer();

				if(Web_GetProtectedStatus() == TRUE)
				{
					iLen +=sprintf(szReturnResult + iLen,"%2d",WEB_RETURN_PROTECTED_ERROR);
				}
				else
				{
					iLen += sprintf(szReturnResult + iLen,"%2d", 0);
				}

				iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);

				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}

				////TRACE("GET_USER_INFO : s\n", szReturnResult);
				break;
			case SET_USER_INFO:
			{	
				int			iNewAuthority;
				int			iLen = 0;
				char		szName[33], szNewPassword[33], szAuthority[33];
				char		*szTrimName = NULL, *szTrimPassword = NULL;
				
				if(Web_GetSetUserInfo(ptr, szName, szNewPassword, szAuthority) == TRUE)
				{
					iNewAuthority = atoi(szAuthority);
					szTrimName = Cfg_RemoveWhiteSpace(szName);
					szTrimPassword = Cfg_RemoveWhiteSpace(szNewPassword);
#ifdef _SHOW_WEB_INFO
					//TRACE("szTrimPassword : *%s*  %d  %d  [%s]\n", szTrimPassword, strlen(szTrimPassword), iNewAuthority, szTrimName);
#endif 					
					
					
					iReturnValue = ModifyUserInfo(szTrimName, szTrimPassword, iNewAuthority);
					////TRACE("iReturnValue : %d\n", iReturnValue);
					pReturnBuf = Web_MakeACUUserInfoBuffer();
					if(iReturnValue == ERR_SEC_USER_NOT_EXISTED)
					{
						iReturnValue = 11;
                    }
					iLen += sprintf(szReturnResult + iLen,"%2d", iReturnValue);
					iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);

					if(pReturnBuf != NULL)
					{
						DELETE(pReturnBuf);
						pReturnBuf = NULL;
					}
					/*DELETE(szName);
					szName = NULL;

					DELETE(szNewPassword);
					szNewPassword = NULL;

					DELETE(szAuthority);
					szAuthority = NULL;*/
				}
				break;
			}
			case SET_USER_PASSWORD:
			{
				int			iNewAuthority;
				int			iLen = 0;
				char		szName[33], szNewPassword[33], szAuthority[33];
				char		*szTrimName = NULL, *szTrimPassword = NULL;
				
				//printf("SET_USER_PASSWORD[%d]\n", SET_USER_PASSWORD);
				if(Web_GetSetUserInfo(ptr, szName, szNewPassword, szAuthority) == TRUE)
				{
					iNewAuthority = Web_GetUserAuthority(Cfg_RemoveWhiteSpace(szName));
					szTrimName = Cfg_RemoveWhiteSpace(szName);
					szTrimPassword = Cfg_RemoveWhiteSpace(szNewPassword);
						
					iReturnValue = ModifyUserInfo(szTrimName, szTrimPassword, iNewAuthority);
					pReturnBuf = Web_MakeACUUserInfoBuffer();
					if(iReturnValue == ERR_SEC_USER_NOT_EXISTED)
					{
						iReturnValue = 11;
                    }
					iLen += sprintf(szReturnResult + iLen,"%2d", iReturnValue);
					iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);

					if(pReturnBuf != NULL)
					{
						DELETE(pReturnBuf);
						pReturnBuf = NULL;
					}
				}
				break;
			}
			case ADD_USER_INFO:
			{
				int			iNewAuthority;
				int			iLen = 0;
				char		szName[33], szNewPassword[33], szAuthority[33];
				char		*szTrimName = NULL, *szTrimPassword = NULL;
					
				if(Web_GetSetUserInfo(ptr, szName, szNewPassword, szAuthority) == TRUE)
				{
					iNewAuthority = atoi(szAuthority);
				
					szTrimName = Cfg_RemoveWhiteSpace(szName);
					szTrimPassword = Cfg_RemoveWhiteSpace(szNewPassword);
					
					iReturnValue = AddUserInfo(szTrimName,szTrimPassword, iNewAuthority);
					pReturnBuf = Web_MakeACUUserInfoBuffer();
					
					if(iReturnValue == ERR_SEC_USER_ALREADY_EXISTED)
					{
						iLen += sprintf(szReturnResult + iLen,"%2d", 9);
					}
					else if(iReturnValue == ERR_SEC_TOO_MANY_USERS)
					{
						iLen += sprintf(szReturnResult + iLen,"%2d", 10);
					}
					else
					{
						iLen += sprintf(szReturnResult + iLen,"%2d", iReturnValue);
					}
					iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);
					
					if(pReturnBuf != NULL)
					{
						DELETE(pReturnBuf);
						pReturnBuf = NULL;
					}
					
				}
				
				break;
			}
			case DELETE_USER_INFO:
			{	
				char		szName[33];
				char		*szTrimName = NULL;
				strncpyz(szName, ptr, sizeof(szName));
				szTrimName = Cfg_RemoveWhiteSpace(szName);
				//TRACE("DELETE_USER_INFODELETE_USER_INFODELETE_USER_INFODELETE_USER_INFO\n");
#ifdef _SHOW_WEB_INFO
				//TRACE("DELETE_USER_INFO---szTrimName:%s\n", szTrimName);
#endif
				
				if(szTrimName != NULL)
				{
#ifdef _SHOW_WEB_INFO
					//TRACE("DELETE_USER_INFO---szTrimName:%s\n", szTrimName);
#endif
					
					iReturnValue = DeleteUserInfo(szTrimName);
					if(iReturnValue == ERR_SEC_CANNOT_DELETE_ADMIN)
					{
						iReturnValue = 7;
					}
					else if(iReturnValue == ERR_SEC_USER_NOT_EXISTED)
					{
						iReturnValue = 11;
					}
		 
					//DELETE(szTrimName);
					//szTrimName = NULL;
				}
				else
				{
					iReturnValue = 0;
				}

				pReturnBuf = Web_MakeACUUserInfoBuffer();
				iLen += sprintf(szReturnResult + iLen,"%2d", iReturnValue);
				iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}

				break;
			}
#ifdef PRODUCT_INFO_SUPPORT
			case GET_PRODUCT_INFO:
			{
				//pReturnBuf = Web_MakeNetWorkInfoBuffer();
				//printf("\nGET_PRODUCT_INFO\n");
				
				Web_GetBarcode_Info(&pReturnBuf,iLanguage);
				sprintf(szReturnProductInfo,"%s",pReturnBuf);
			
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}

				break;
			}
#endif
		
            //////////////////////////////////////////////////////////////////////////
            //Added by wj for Config PLC private config file 2006.5.13

            case SET_PLC_CONFIG :
            {
                TRACE("SET_PLC_CONFIG:%s/n",ptr);
                
                //1����ptrд��PLC Configure File��ע�⻥�⣬ͬʱֻ����һ���˽����޸ģ��޸ĵ�ͬʱ���ܶ�ȡ��
                //2����Webҳ��p39_edit_config_plc.htm�е�PLC Configure File��Ϣ���µ���Ϣ�����滻����Web_MakePlcWebPage()ʵ��
                //3�����ز������iresult = 6��ʾ�ɹ���iresult = 5��ʾʧ��
                int itemp = 0;
                RunThread_Heartbeat(RunThread_GetId(NULL));
                iresult = Web_WritePLCCFGInfo(ptr);

                char *pszReturn = NULL;
                char *ptemp = "  ;  ;  ;  ;  ;  ";

                if(iresult == 4)
                {

                    iresult = 0;
                    pszReturn = NEW_strdup(ptemp);
                }
                else
                {
                    RunThread_Heartbeat(RunThread_GetId(NULL));
                    itemp = Web_MakePlcWebPage( &pszReturn,iLanguage);
                    if(itemp == 0 )
                    {
                        iresult = 0;
                        pszReturn = NEW_strdup(ptemp);

                    }
                    else if(itemp == 1)
                    {
                        
                    }
                    else if(itemp == ERR_NO_MEMORY)
                    {
                        iresult = 1;
                        pszReturn = NEW_strdup(ptemp);

                    }

                }

                if(iresult == 6)
                {
                    sprintf(szTempUserName,"Web:%s  SetPLC  ",szUserName);
                    AppLogOut(szTempUserName,APP_LOG_MILESTONE,ptr);
                        TRACE("\n________ptr:%s_________",ptr);
                }
                
                memset(szReturnResult,0,sizeof(szReturnResult));
                sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

                DELETE(pszReturn);

                break;
            }

            case DEL_PLC_CONFIG :
            {

                RunThread_Heartbeat(RunThread_GetId(NULL));
                //1��ɾ��PLC Configure File��ptrָ�����У�ע�⻥�⣬ͬʱֻ����һ���˽����޸ģ��޸ĵ�ͬʱ���ܶ�ȡ��
                //2����Webҳ��p39_edit_config_plc.htm�е�PLC Configure File��Ϣ���µ���Ϣ�����滻����Web_MakePlcWebPage()ʵ��
                //3�����ز������iresult = 6��ʾ�ɹ���iresult = 5��ʾʧ��,iresult = 4��ʾ�ļ�������
                int itemp = 0;
                TRACE("DEL_PLC_CONFIG:%s/n",ptr);
                iresult = Web_DeletePLCCFGInfo(ptr);

                char *pszReturn = NULL;
                char *ptemp = "  ;  ;  ;  ;  ;  ";

                if(iresult == 4)
                {
                    iresult = 0;
                    pszReturn = NEW_strdup(ptemp);
                }
                else
                {
                    RunThread_Heartbeat(RunThread_GetId(NULL));
                    itemp = Web_MakePlcWebPage( &pszReturn,iLanguage);
                    if(itemp == 0 )
                    {
                        iresult = 0;
                        pszReturn = NEW_strdup(ptemp);

                    }
                    else if(itemp == 1)
                    {

                    }
                    else if(itemp == ERR_NO_MEMORY)
                    {
                        iresult = 1;
                        pszReturn = NEW_strdup(ptemp);

                    }

                }

                if(iresult == 6)
                {
                    sprintf(szTempUserName,"Web:%s  DelPLC  ",szUserName);
                    AppLogOut(szTempUserName,APP_LOG_MILESTONE,ptr);
                        TRACE("\n________ptr:%s_________",ptr);
                }
                memset(szReturnResult,0,sizeof(szReturnResult));
                sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

                DELETE(pszReturn);

                break;
            }

            case GET_PLC_CONFIG :
            {
                //��Webҳ��p39_edit_config_plc.htm�е�PLC Configure File��Ϣ���豸��Ϣ���ź���Ϣ�����µ���Ϣ�����滻,���ڵ�һ����ʾ

                TRACE("into GET_PLC_CONFIG!!!:%s",ptr);

                iresult = 7;
                char *pszReturn = NULL;
                char *ptemp = "  ;  ;  ;  ;  ;  ";


                RunThread_Heartbeat(RunThread_GetId(NULL));
                iresult = Web_MakePlcWebPage( &pszReturn,iLanguage);
                if(iresult == 0 )
                {
                    pszReturn = NEW_strdup(ptemp);

                }
                else if(iresult == 1)
                {
                    iresult = 7;
                }
                else if(iresult == ERR_NO_MEMORY)
                {
                    iresult = 1;
                    pszReturn = NEW_strdup(ptemp);

                }


                memset(szReturnResult,0,sizeof(szReturnResult));
                sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

                TRACE("Web_MakePlcWebPage OK!!!");
                DELETE(pszReturn);
                break;
            }
            
            //end////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////
            //Added by wj for Config Alarm Suppressing Exp 2006.5.22
            case GET_ALARM_CONFIG:
            {
                TRACE("into GET_ALARM_CONFIG!!!:%s",ptr);

                iresult = 7;
                char *pszReturn = NULL;


                RunThread_Heartbeat(RunThread_GetId(NULL));
                Web_MakeAlarmSupExpWebPageBuf(ptr,&pszReturn,iLanguage);

                memset(szReturnResult,0,sizeof(szReturnResult));
                sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

                TRACE("Web_MakeAlarmSupExpWebPageBuf OK!!!");
                DELETE(pszReturn);

                break;
            }

            case SET_ALARM_CONFIG:
            {
                TRACE("into SET_ALARM_CONFIG!!!:%s",ptr);


                iresult = Web_SetAlarmSuppressingExp(ptr,szUserName);
                char *pszReturn = NULL;


                RunThread_Heartbeat(RunThread_GetId(NULL));
                Web_MakeAlarmSupExpWebPageBuf(ptr,&pszReturn,iLanguage);

                if(iresult == 6)
                {
                    sprintf(szTempUserName,"Web:%s  SetAlarm  ",szUserName);
                    AppLogOut(szTempUserName,APP_LOG_MILESTONE,ptr);
                    TRACE("\n________ptr:%s_________",ptr);
                }


                memset(szReturnResult,0,sizeof(szReturnResult));
                sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

                TRACE("Web_MakeAlarmSupExpWebPageBuf OK!!!");
                DELETE(pszReturn);

                break;
            }
                                
            //end////////////////////////////////////////////////////////////////////////
            
            //////////////////////////////////////////////////////////////////////////
            //Added by wj for Config Alarm Relay 2006.6.22
            case GET_ALARMREG_CONFIG:
                {
                    TRACE("into GET_ALARMREG_CONFIG!!!:%s",ptr);

                    iresult = 7;
                    char *pszReturn = NULL;


                    RunThread_Heartbeat(RunThread_GetId(NULL));
                    Web_MakeAlarmRegWebPageBuf(ptr,&pszReturn,iLanguage);

                    memset(szReturnResult,0,sizeof(szReturnResult));
                    sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

                    TRACE("Web_MakeAlarmRegWebPageBuf OK!!!");
                    DELETE(pszReturn);

                    break;
                }

            case SET_ALARMREG_CONFIG:
                {
                    TRACE("into SET_ALARMREG_CONFIG!!!:%s",ptr);


                    iresult = Web_SetAlarmRelay(ptr,szUserName);
                    char *pszReturn = NULL;


                    RunThread_Heartbeat(RunThread_GetId(NULL));
                    Web_MakeAlarmRegWebPageBuf(ptr,&pszReturn,iLanguage);

                    
                    memset(szReturnResult,0,sizeof(szReturnResult));
                    sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

                    TRACE("Web_MakeAlarmRegWebPageBuf OK!!!");
                    DELETE(pszReturn);

                    break;
                }

                //end////////////////////////////////////////////////////////////////////////
			case GET_GC_PS_CONFIG:
				{
					TRACE("into GET_GC_PS_CONFIG!!!:%s",ptr);
					iresult = 7;
					char *pszReturn = NULL;
					char *ptemp = "  ;  ;  ;  ;  ;  ;   ";


					RunThread_Heartbeat(RunThread_GetId(NULL));
					iresult = Web_MakeGCWebPage( &pszReturn,iLanguage);
					if(iresult == 0 )
					{
						pszReturn = NEW_strdup(ptemp);

					}
					else if(iresult == 1)
					{
						iresult = 7;
					}
					else if(iresult == ERR_NO_MEMORY)
					{
						iresult = 1;
						pszReturn = NEW_strdup(ptemp);

					}

					TRACE("___iLen:%d_______\n",strlen(pszReturn));
					TRACE("into Web_MakeGCWebPage!!!End\n");


					memset(szReturnResult,0,sizeof(szReturnResult));
					sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

					TRACE("Web_MakeGCWebPage OK!!!");
					DELETE(pszReturn);
					break;
				}

			case SET_GC_PS_MODE:
				{
					TRACE("into SET_GC_PS_MODE!!!:%s",ptr);
					int itemp = 0;
					iresult = 7;
					char *pszReturn = NULL;
					char *ptemp = "  ;  ;  ;  ;  ;  ;   ";

					iresult = Web_ModifyGCPSMode(ptr);


					if(iresult == 4)
					{
						iresult = 0;
						pszReturn = NEW_strdup(ptemp);
					}
					else
					{
						RunThread_Heartbeat(RunThread_GetId(NULL));
						itemp = Web_MakeGCWebPage( &pszReturn,iLanguage);
						if(itemp == 0 )
						{
							iresult = 0;
							pszReturn = NEW_strdup(ptemp);

						}
						else if(itemp == 1)
						{

						}
						else if(itemp == ERR_NO_MEMORY)
						{
							iresult = 1;
							pszReturn = NEW_strdup(ptemp);

						}
					}

					TRACE("into Web_MakeGCWebPage!!!End\n");


					memset(szReturnResult,0,sizeof(szReturnResult));
					sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

					TRACE("Web_MakeGCWebPage OK!!!");
					DELETE(pszReturn);
					break;
				}

			case SET_GC_PS_CONFIG:
				{
					TRACE("into SET_GC_PS_CONFIG!!!:%s",ptr);
					int itemp = 0;
					iresult = 7;
					char *pszReturn = NULL;
					char *ptemp = "  ;  ;  ;  ;  ;  ;   ";

					iresult = Web_ModifyGCPSInfo(ptr);


					if(iresult == 4)
					{
						iresult = 0;
						pszReturn = NEW_strdup(ptemp);
					}
					else
					{
						RunThread_Heartbeat(RunThread_GetId(NULL));
						itemp = Web_MakeGCWebPage( &pszReturn,iLanguage);
						if(itemp == 0 )
						{
							iresult = 0;
							pszReturn = NEW_strdup(ptemp);

						}
						else if(itemp == 1)
						{

						}
						else if(itemp == ERR_NO_MEMORY)
						{
							iresult = 1;
							pszReturn = NEW_strdup(ptemp);

						}
					}

					TRACE("into Web_MakeGCWebPage!!!End\n");


					memset(szReturnResult,0,sizeof(szReturnResult));
					sprintf(szReturnResult,"%2d%s", iresult, pszReturn);

					TRACE("Web_MakeGCWebPage OK!!!");
					DELETE(pszReturn);
					break;
				}


                //////////////////////////////////////////////////////////////////////////
                //Added by wj for YDN Setting Config
           
            case GET_YDN_CONFIG:
				{
					pReturnBuf = Web_MakeYDNPrivateConfigreBuffer();

					if(pReturnBuf != NULL)
					{
						if(Web_GetProtectedStatus() == TRUE)
						{
							iLen += sprintf(szReturnResult + iLen,"%2d%s",15,pReturnBuf);//WEB_RETURN_PROTECTED_ERROR
						}
						else
						{
							iLen += sprintf(szReturnResult + iLen,"%2d%s",99,pReturnBuf);
						}
						DELETE(pReturnBuf);
						pReturnBuf = NULL;
					}
					else
					{
						iLen += sprintf(szReturnResult + iLen,"%2d",1);
					}

					break;
				}

            case SET_YDN_CONFIG:
				{
					iReturnValue = Web_ModifyYDNPrivateConfigure(ptr,szUserName); 
					pReturnBuf = Web_MakeYDNPrivateConfigreBuffer();

					iLen += sprintf(szReturnResult + iLen,"%2d", iReturnValue);


					if(pReturnBuf != NULL)
					{
						iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);

						DELETE(pReturnBuf);
						pReturnBuf = NULL;
					}
					else
					{
					}
					break;
				}

			case GET_SETTING_PARAM:
				{
					TRACE("\n into GET_SETTING_PARAM!!!\n");



					RunThread_Heartbeat(RunThread_GetId(NULL));
					iresult = Web_MakeGetSetParamWebPage();

					TRACE("\n into GET_SETTING_PARAM!!!End %d\n",iresult);


					memset(szReturnResult,0,sizeof(szReturnResult));
					sprintf(szReturnResult,"%2d", iresult);

					TRACE("Web_MakeGetSetParamWebPage OK!!!");

					break;
				}

			case GET_AUTO_CONFIG:
				{
					/*TRACE("\n into GET_AUTO_CONFIG!!!\n");
					RunThread_Heartbeat(RunThread_GetId(NULL));
					HANDLE hCmdThread = RunThread_Create("Auto Config",
						(RUN_THREAD_START_PROC)DIX_AutoConfigMain,
						(void*)NULL,
						(DWORD*)NULL,
						0);
					close(fd2);
					return TRUE;*/
					AppLogOut(CGI_APP_LOG_CONTROL_NAME,APP_LOG_INFO,"Start Auto Config, System will be reboot!");

					
					close(fd2);

					char szFullPath[MAX_FILE_PATH];
					char szCmdLine[128];

					Cfg_GetFullConfigPath(LOADALLEQUIP_FLAGFILE, szFullPath, MAX_FILE_PATH);
					snprintf(szCmdLine, sizeof(szCmdLine), "rm -rf %s",
						szFullPath);	
					_SYSTEM(szCmdLine);
					_SYSTEM("reboot");
					
					return TRUE;

					break;
				}
			case SET_NMSV3_INFO:
				iReturnValue = Web_ModifyNMSV3PrivateConfigure(ptr);
				pReturnBuf = Web_MakeNMSV3PrivateConfigureBuffer();
				
				iLen += sprintf(szReturnResult + iLen,"%2d", iReturnValue);
				iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);
				
				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}
				break;
			case GET_NMSV3_INFO:
				pReturnBuf = Web_MakeNMSV3PrivateConfigureBuffer();
				if(Web_GetProtectedStatus() == TRUE)
				{
					iLen += sprintf(szReturnResult + iLen,"%2d", WEB_RETURN_PROTECTED_ERROR);
				}
				else if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
				{
					iLen += sprintf(szReturnResult + iLen,"%2d", WEB_RETURN_NOT_SUPPORT_V3);
				}
				else
				{
					iLen += sprintf(szReturnResult + iLen,"%2d", 1);
				}
				iLen += sprintf(szReturnResult + iLen,"%s",pReturnBuf);
				

				if(pReturnBuf != NULL)
				{
					DELETE(pReturnBuf);
					pReturnBuf = NULL;
				}

				break;
                //end////////////////////////////////////////////////////////////////////////
                
			default:
				break;
		}

       /* FILE *pf = NULL;
        pf = fopen("/var/replaceStr.txt","a+");
        fwrite(szReturnResult,strlen(szReturnResult),1,pf);
        fclose(pf);*/

#ifdef _SHOW_WEB_INFO
		//TRACE("szReturnResult : %s\n", szReturnResult);
#endif		
		if(iCommandType != GET_PRODUCT_INFO && 
            iCommandType != GET_PLC_CONFIG && 
            iCommandType != SET_PLC_CONFIG && 
            iCommandType != DEL_PLC_CONFIG &&
            iCommandType != GET_ALARM_CONFIG &&
            iCommandType != SET_ALARM_CONFIG &&
            iCommandType != GET_ALARMREG_CONFIG &&
            iCommandType != SET_ALARMREG_CONFIG &&
			iCommandType != GET_GC_PS_CONFIG && 
			iCommandType != SET_GC_PS_CONFIG &&
			iCommandType != SET_GC_PS_MODE )
		{

			if((write(fd2,szReturnResult,4095)) < 0)
			{
				AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to write FIFO when write the control result!");
				close(fd2);
				char			szCommand[30];
				iLen = sprintf(szCommand,"kill ");
				strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);

				TRACEX("%s\n", szCommand);
				system(szCommand);
				return FALSE;
			}
		}
        //////////////////////////////////////////////////////////////////////////
        //Added by wj for PLC Config
        else if(iCommandType == GET_PLC_CONFIG || 
                iCommandType == SET_PLC_CONFIG || 
                iCommandType == DEL_PLC_CONFIG ||
                iCommandType == GET_ALARM_CONFIG ||
                iCommandType == SET_ALARM_CONFIG ||
                iCommandType == GET_ALARMREG_CONFIG ||
                iCommandType == SET_ALARMREG_CONFIG ||
				iCommandType == GET_GC_PS_CONFIG ||
				iCommandType == SET_GC_PS_CONFIG ||
				iCommandType == SET_GC_PS_MODE)
        {
            int iPipeLen = strlen(szReturnResult)/(PIPE_BUF - 1)  + 2;
            if(iPipeLen <= 1)
            {
                if((write(fd2, szReturnResult, strlen(szReturnResult) + 1)) < 0)
                {
                    close(fd2);
                    return FALSE;
                }
            }
            else
            {
                int m;
                char szP[PIPE_BUF - 1];
                for(m = 0; m < iPipeLen; m++)
                {
                    strncpyz(szP, szReturnResult + m * (PIPE_BUF - 2), PIPE_BUF - 1);
                    if((write(fd2, szP, PIPE_BUF - 1)) < 0)
                    {
                        close(fd2);
                        return FALSE;
                    }
                }

            }
        }
        //end////////////////////////////////////////////////////////////////////////  
		else
		{
				int iPipeLen = strlen(szReturnProductInfo)/(PIPE_BUF - 1)  + 1;
				if(iPipeLen <= 1)
				{
					if((write(fd2, szReturnProductInfo, strlen(szReturnProductInfo) + 1)) < 0)
					{
                        close(fd2);
						return FALSE;
					}
				}
				else
				{
					int m;
					char szP[PIPE_BUF - 1];
					for(m = 0; m < iPipeLen; m++)
					{
						strncpyz(szP, szReturnProductInfo + m * (PIPE_BUF - 2), PIPE_BUF - 1);
						if((write(fd2, szP, PIPE_BUF - 1)) < 0)
						{
                            close(fd2);
							return FALSE;
						}
					}

				}
		}

		close(fd2);
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}



/*==========================================================================*
 * FUNCTION :  Web_MakeNetWorkInfoBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char * szIP:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static char *Web_MakeNetWorkInfoBuffer(void)
{
	char			*szMakeBuf;
	int				iLen = 0;
	struct in_addr	*pIn;
	int				iBufLen, iVarID, iError, iTimeOut;
	ACU_NET_INFO 	stACUNetInfo;
	
	
	/////////////////////////////////////
	//Get local IP address,add port number
	iTimeOut = 100;
	iError = ERR_DXI_OK;
	iVarID = NET_INFO_ALL;
	iBufLen = sizeof(ACU_NET_INFO);

	iError += DxiGetData(VAR_ACU_NET_INFO,
			iVarID,			
			0,		
			&iBufLen,			
			&stACUNetInfo,			
			iTimeOut);
	
	szMakeBuf = NEW(char, 128);
	if(szMakeBuf == NULL)
	{
		return NULL;
	}
	if (iError == ERR_DXI_OK)
	{
	 		 
		pIn = (struct in_addr*)(&(stACUNetInfo.ulIp));
		iLen += sprintf(szMakeBuf + iLen, "%32s,",inet_ntoa(*pIn));

		pIn = (struct in_addr*)(&(stACUNetInfo.ulMask));
		iLen += sprintf(szMakeBuf + iLen, "%32s,",inet_ntoa(*pIn));

		pIn = (struct in_addr*)(&(stACUNetInfo.ulGateway));
		iLen += sprintf(szMakeBuf + iLen, "%32s,",inet_ntoa(*pIn));

		
		return szMakeBuf;

	}
	else
	{
		return NULL;
	}
	
}



/*==========================================================================*
 * FUNCTION :  Web_Modify_AcuIP_Addr
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char * szIP:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/

static int Web_Modify_AcuIP_Addr(IN char * szIP)
{
#ifdef _SHOW_WEB_INFO
	TRACE("szIP : %s\n", szIP);
#endif	
	
	ASSERT(szIP);
	ULONG			ulIPAddr, ulMask, ulGateWay;
	ACU_NET_INFO	stACUNetInfo;
	int				iBufLen, iError;
	char			*pSearchValue = NULL, *ptr = NULL;
	int				iPosition = 0;
	char			szExchange[33];
	char			*szTrimText = NULL;

	int nDHCP = APP_DHCP_OFF;

	int nBufLen;

	if(DxiGetData(VAR_APP_DHCP_INFO,
		0,
		0,
		&nBufLen,
		&nDHCP,
		0) == ERR_DXI_OK)
	{
		if(nDHCP == APP_DHCP_ON)
		{
			return 6;
		}
	}
	
	ptr = szIP;
	/*IP*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition > 0)
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			szTrimText = Cfg_RemoveWhiteSpace(szExchange);
			if(atoi(szTrimText) == 0)
			{
				ulIPAddr = inet_addr("0.0.0.0");
			}
			else
			{
				ulIPAddr = inet_addr(szTrimText);
			}

			ptr = ptr + iPosition;
		}
	}
	ptr = ptr + 1;

	/*Mask*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition > 0)
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			szTrimText = Cfg_RemoveWhiteSpace(szExchange);
			if(atoi(szTrimText) == 0)
			{
				ulMask = inet_addr("0.0.0.0");
			}
			else
			{
				ulMask = inet_addr(szTrimText);
			}
			ptr = ptr + iPosition;
		}
	}
	ptr = ptr + 1;

	/*Gateway*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition > 0)
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			
			szTrimText = Cfg_RemoveWhiteSpace(szExchange);
#ifdef _SHOW_WEB_INFO
			TRACE("szTrimText:[%s]", szTrimText);
#endif
			
			if(atoi(szTrimText) == 0)
			{
				ulGateWay = inet_addr("0.0.0.0");
			}
			else
			{
				ulGateWay = inet_addr(szTrimText);
			}

			//ptr = ptr + iPosition;
		}
	}
	iBufLen  = sizeof(ACU_NET_INFO);

	stACUNetInfo.ulIp = ulIPAddr;
	stACUNetInfo.ulMask = ulMask;;
	stACUNetInfo.ulGateway = ulGateWay;
	stACUNetInfo.ulBroardcast= (ULONG)0;

	iError = DxiSetData(VAR_ACU_NET_INFO,
			NET_INFO_ALL,			
			0,		
			iBufLen,			
			&stACUNetInfo,			
			0);
#ifdef _SHOW_WEB_INFO
	TRACE("iError :[%d]\n", iError);
#endif
	
	if (iError == ERR_DXI_OK)
	{
		return TRUE;
	}
	else  
	{
		
		if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
		{
			return WEB_RETURN_PROTECTED_ERROR;
		}
		else
		{
			return FALSE;
		}
	}


}



/*==========================================================================*
 * FUNCTION :  Web_MakeACUUserInfoBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static char *Web_MakeACUUserInfoBuffer(void)
{
	USER_INFO_STRU		*pUserInfo = NULL;
	int					i, iLen = 0;
	int					iUserNum = 0;
	char				*szUserBuf = NULL;
	
	if((iUserNum = GetUserInfo(&pUserInfo)) > 0)
	{
		szUserBuf = NEW(char, iUserNum * 64);
		if(szUserBuf != NULL)
		{
			for(i = 0; i < iUserNum && pUserInfo != NULL; i++, pUserInfo++)
				iLen += sprintf(szUserBuf + iLen, "%d,\"%s\",%2d,", 
										i, pUserInfo->szUserName,pUserInfo->byLevel);
		}
		if(iLen > 0)
		{
#ifdef _SHOW_WEB_INFO
			//TRACE("iUserNum : %d szUserBuf : %s", iUserNum, szUserBuf);
#endif
			
			szUserBuf[iLen-1] = 32;
		}
		return szUserBuf;
	}
	return NULL;
}

static int Web_GetUserAuthority(IN char *szUserName)
{
	USER_INFO_STRU		pUserInfo ;
 	ASSERT(szUserName);

	if(FindUserInfo(szUserName, &pUserInfo) == ERR_SEC_OK)
	{
		return (int)pUserInfo.byLevel;
	}
	return 0;
}

/*==========================================================================*
 * FUNCTION :  Web_MakeESRPrivateConfigreBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/

static char *Web_MakeESRPrivateConfigreBuffer(void)
{
	COMMON_CONFIG	stCommonConfig ;
	APP_SERVICE		*stAppService = NULL;
	int				iLen = 0;
	char			*szReturn = NULL;
	int				iReturn = 0;
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);
	
	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{

		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
											stAppService->bServiceRunning,
											&stAppService->args, 
											0x0,
											0,
										(void *)&stCommonConfig);
		
		szReturn = NEW(char, 250);

		if(szReturn != NULL)
		{
			//TRACE("stAppService ---- 4\n");
			//iLen += sprintf(szReturn + iLen, "%2d", iReturn);
			if(iReturn == ERR_SERVICE_CFG_OK)
			{
				iLen += sprintf(szReturn + iLen, "\t %d,%d,%d,%d,%d,%d,%d,", 
									stCommonConfig.iProtocolType,
									stCommonConfig.iMediaType,
									stCommonConfig.iAttemptElapse/1000,
									stCommonConfig.iMaxAttempts,
									stCommonConfig.bCallbackInUse,
									stCommonConfig.bReportInUse,
									stCommonConfig.iSecurityLevel);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[1]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szCallbackPhoneNumber[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szReportIP[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szReportIP[1]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szSecurityIP[0]);
				iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szSecurityIP[1]);
				iLen += sprintf(szReturn + iLen, "%d,",		stCommonConfig.byCCID);
				iLen += sprintf(szReturn + iLen, "%d,",		stCommonConfig.iSOCID);
				iLen += sprintf(szReturn + iLen, "\"%s\"",		stCommonConfig.szCommPortParam);

				return szReturn;
		

			}
		}
		
	}
	//TRACE("return NULL\n");
	return NULL;
}



/*==========================================================================*
 * FUNCTION :  Web_ModifyESRPrivateConfigure
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szBuffer:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_ModifyESRPrivateConfigure(IN char *szBuffer)
{
	ASSERT(szBuffer);
	COMMON_CONFIG	stCommonConfig ;
	YDN_COMMON_CONFIG	stYDNCommonConfig ;
	APP_SERVICE		*stAppService = NULL;
	int				iModifyType = 0;
	int				iYDNModifyType = 0;
	int				iReturn = 0;
#ifdef _SHOW_WEB_INFO
	//TRACE("szBuffer : %s\n", szBuffer);
#endif
	

	if((iModifyType = Web_GetESRModifyInfo(szBuffer, &stCommonConfig)) != FALSE)
	{
		////TRACE("iModifyType : %x ----- 1\n", iModifyType);
		////Change ydn protocol to EEM .
		//if(stCommonConfig.iProtocolType != YDN23)
		//{
		//	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN_NAME);
		//
		//	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		//	{
		//		stYDNCommonConfig.iProtocolType = YDN23;			
		//		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		//					stAppService->bServiceRunning,
		//					&stAppService->args, 
		//					0x0,
		//					0,
		//					(void *)&stYDNCommonConfig);
		//		 if(iReturn == ERR_SERVICE_CFG_OK)
		//		 {
		//			stYDNCommonConfig.iProtocolType = EEM;
		//			iYDNModifyType = iYDNModifyType | YDN_CFG_W_MODE;
		//			iYDNModifyType = iYDNModifyType | YDN_CFG_ALL | YDN_CFG_PROTOCOL_TYPE | YDN_CFG_MEDIA_TYPE | YDN_CFG_MEDIA_PORT_PARAM;
		//			stYDNCommonConfig.iMediaType = 2;//tcp/ip
		//			strncpyz(stYDNCommonConfig.szCommPortParam,"5050",5);
		//			TRACE("stYDNCommonConfig.szCommPortParam = %s\n", stYDNCommonConfig.szCommPortParam);

		//			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
		//				stAppService->bServiceRunning,
		//				&stAppService->args, 
		//				iYDNModifyType,
		//				0,
		//				(void *)&stYDNCommonConfig);// == ERR_SERVICE_CFG_OK)
		//			if(iReturn != ERR_SERVICE_CFG_OK)
		//			{
		//				return iReturn;
		//			}
		//		 }				
		//	}
		//}


		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);
		
		if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		{
			//TRACE("iModifyType : %x ----- 2", iModifyType);
			//TRACE("stAppService->bServiceRunning : %d\n",stAppService->bServiceRunning);
			

			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
											stAppService->bServiceRunning,
											&stAppService->args, 
											iModifyType,
											0,
										(void *)&stCommonConfig);// == ERR_SERVICE_CFG_OK)

			//TRACE("iReturn : %d ----- 3\n", iReturn);
			return iReturn;
		}
	}
	return ERR_SERVICE_CFG_FAIL;
}


/*==========================================================================*
 * FUNCTION :  Web_GetESRModifyInfo
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szBuffer:
				OUT COMMON_CONFIG *stCommonConfig:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_GetESRModifyInfo(IN char *szBuffer, OUT COMMON_CONFIG *stCommonConfig)
{
	ASSERT(szBuffer);
	ASSERT(stCommonConfig);

	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	char			*pTrim = NULL;
	int				iModifyType = ESR_CFG_W_MODE;

	//Aanalyze szBuffer
	int			iValdateModify = 0;
	if((ptr = szBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iProtocolType = atoi(szExchange) ;
					//TRACE("stCommonConfig->iProtocolType :[%s][%d]\n", szExchange, stCommonConfig->iProtocolType);
					iModifyType = iModifyType | ESR_CFG_PROTOCOL_TYPE;
				}
				ptr = ptr + iPosition;
				
			}
						
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iMediaType = atoi(szExchange);
					//TRACE("stCommonConfig->iMediaType :[%s][%d]\n", szExchange, stCommonConfig->iMediaType);
					iModifyType = iModifyType | ESR_CFG_MEDIA_TYPE;
				}
				ptr = ptr + iPosition;
				
			}
						
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iAttemptElapse = 1000 * atoi(szExchange);
					//TRACE("stCommonConfig->iAttemptElapse :[%s][%d]\n", szExchange, stCommonConfig->iAttemptElapse);
					iModifyType = iModifyType | ESR_CFG_ATTEMPT_ELAPSE;
				}
				ptr = ptr + iPosition;
			}
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					stCommonConfig->iMaxAttempts = atoi(szExchange);
					//TRACE("stCommonConfig->iMaxAttempts :[%s][%d]\n", szExchange,stCommonConfig->iMaxAttempts);
					iModifyType = iModifyType | ESR_CFG_MAX_ATTEMPTS;
				}
				ptr = ptr + iPosition;
			}
		}
		ptr = ptr + 1;

 		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					if(iPosition > 2)
					{
						stCommonConfig->bCallbackInUse = TRUE;
					}
					else
					{
						stCommonConfig->bCallbackInUse = FALSE;
					}
					
					//TRACE("stCommonConfig->bCallbackInUse :[%s][%d]\n", szExchange,stCommonConfig->bCallbackInUse);
					iModifyType = iModifyType | ESR_CFG_CALLBACK_IN_USE;
				}
				ptr = ptr + iPosition;
			}
			
						
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					if(iPosition > 2)
					{
						stCommonConfig->bReportInUse = TRUE;
					}
					else
					{
						stCommonConfig->bReportInUse = FALSE;
					}
					//TRACE("stCommonConfig->bReportInUse :[%s][%d]\n", szExchange, stCommonConfig->bReportInUse);
					iModifyType = iModifyType | ESR_CFG_REPORT_IN_USE;
				}
				ptr = ptr + iPosition;
			}
									
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{

				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
                    stCommonConfig->iSecurityLevel = atoi(szExchange) + 1;
					//TRACE("stCommonConfig->iSecurityLevel :[%s][%d]\n", szExchange,stCommonConfig->iSecurityLevel);
					iModifyType = iModifyType | ESR_CFG_SECURITY_LEVEL;
				}
				ptr = ptr + iPosition;
			}
						
		}
		ptr = ptr + 1;
	
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szAlarmReportPhoneNumber[0],
								pTrim,
								sizeof(stCommonConfig->szAlarmReportPhoneNumber[0]));

					//TRACE("stCommonConfig->szAlarmReportPhoneNumber :[%s][%s]\n", szExchange,stCommonConfig->szAlarmReportPhoneNumber[0]);
					iModifyType = iModifyType | ESR_CFG_REPORT_NUMBER_1;
				}
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szAlarmReportPhoneNumber[1],
								pTrim,
								sizeof(stCommonConfig->szAlarmReportPhoneNumber[1]));
				
					//TRACE("stCommonConfig->szAlarmReportPhoneNumber :[%s][%s]\n", szExchange, stCommonConfig->szAlarmReportPhoneNumber[1]);
					iModifyType = iModifyType | ESR_CFG_REPORT_NUMBER_2;
				}
				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;


		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);

				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szCallbackPhoneNumber[0],
							pTrim,
							(int)sizeof(stCommonConfig->szCallbackPhoneNumber[0]));

					//TRACE("stCommonConfig->szCallbackPhoneNumber :[%s][%s]\n", szExchange, stCommonConfig->szCallbackPhoneNumber[0]);
					iModifyType = iModifyType | ESR_CFG_CALLBACK_NUMBER;
				}
				ptr = ptr + iPosition;

			}
			
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szReportIP[0],
								pTrim,
								sizeof(stCommonConfig->szReportIP[0]));

					//TRACE("stCommonConfig->szReportIP :[%s][%s]\n", szExchange,stCommonConfig->szReportIP[0]);
					iModifyType = iModifyType | ESR_CFG_IPADDR_1;
				}
				ptr = ptr + iPosition;

			}
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szReportIP[1],
								pTrim,
								sizeof(stCommonConfig->szReportIP[1]));

					//TRACE("stCommonConfig->szReportIP :[%s][%s]\n", szExchange,stCommonConfig->szReportIP[1]);
					iModifyType = iModifyType | ESR_CFG_IPADDR_2;
				}
				ptr = ptr + iPosition;

			}
			
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					strncpyz(stCommonConfig->szSecurityIP[0],
								pTrim,
								sizeof(stCommonConfig->szSecurityIP[0]));

					//TRACE("stCommonConfig->szSecurityIP :[%s][%s]\n", szExchange,stCommonConfig->szSecurityIP[0]);
					iModifyType = iModifyType | ESR_CFG_SECURITY_IP_1;
				}
				ptr = ptr + iPosition;
 			}
			
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					//TRACE("pTrim : %s\n", pTrim);
					strncpyz(stCommonConfig->szSecurityIP[1],
								pTrim,
								sizeof(stCommonConfig->szSecurityIP[1]));

					//TRACE("stCommonConfig->szSecurityIP :[%s][%s]\n", szExchange, stCommonConfig->szSecurityIP[0]);
					iModifyType = iModifyType | ESR_CFG_SECURITY_IP_2;
				}
				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					stCommonConfig->byCCID = (BYTE)atoi(pTrim);

					//TRACE("stCommonConfig->byCCID :[%s][%d]\n", szExchange, stCommonConfig->byCCID);
					iModifyType = iModifyType | ESR_CFG_CCID;
				}
				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				//TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					stCommonConfig->iSOCID = atoi(pTrim);
					//TRACE("stCommonConfig->iSOCID :[%s][%d]\n", szExchange, stCommonConfig->iSOCID);
					iModifyType = iModifyType | ESR_CFG_SOCID;
				}
				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;


		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
				if(iValdateModify == 1)
				{
					pTrim = Cfg_RemoveWhiteSpace(szExchange);
					sprintf(stCommonConfig->szCommPortParam, "%s", pTrim);
					//TRACE("stCommonConfig->szCommPortParam :[%s][%s]\n", szExchange, stCommonConfig->szCommPortParam);
					iModifyType = iModifyType | ESR_CFG_MEDIA_PORT_PARAM;
				}
				ptr = ptr + iPosition;
			}
			
		}
		
		return iModifyType;
	}
	return FALSE;

}


/*==========================================================================*
 * FUNCTION :  Web_MakeNMSPrivateConfigureBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:		
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static char *Web_MakeNMSPrivateConfigureBuffer(void)
{
	APP_SERVICE		*stAppService = NULL;
	int				iBufLen = 1;
	NMS_INFO		*stNmsInfo = NULL;
	char			*szNmsInfo = NULL;
	struct in_addr	inIP;
	int				i = 0, iLen = 0;
	int				iNMSNum = 0;

	if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV2_NAME);
	}
	else
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV3_NAME);
	}	


	//stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMS_NAME);
	
	//TRACE("**********stAppService \n");

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{
		//TRACE("**********stAppService->pfnServiceConfig : TRUE\n");

		if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										GET_NMS_USER_INFO,
										&iBufLen,
										(void *)&stNmsInfo) == ERR_SNP_OK)
		{
			//print serviceconfig
			
			iNMSNum =  iBufLen /sizeof(NMS_INFO);
			szNmsInfo = NEW(char, 200 * iNMSNum);
			//TRACE("**********stAppService->pfnServiceConfig : TRUE  iBufLen : %d %d\n", iBufLen, iNMSNum);
			for(i = 0; i < iNMSNum && stNmsInfo != NULL; i++, stNmsInfo++)
			{

				inIP.s_addr = stNmsInfo->ulIpAddress; 
				//memcpy(&inIP.s_addr, &stNmsInfo->ulIpAddress, sizeof(ULONG));
				iLen += sprintf(szNmsInfo + iLen,"%d,\"%s\",%d,\"%s\",\"%s\",",  
											i,
											inet_ntoa(inIP),
											stNmsInfo->nTrapLevel,
											stNmsInfo->szPublicCommunity,
											stNmsInfo->szPrivateCommunity
											);
			
			}
			if(iLen >= 1)
			{
				*(szNmsInfo + iLen - 1) = 32;
			}
			else
			{
				DELETE(szNmsInfo);
				szNmsInfo = NULL;

			}
			//TRACE("szNmsInfo : %s\n", szNmsInfo);
			return szNmsInfo;

		}
	}
	//TRACE("**********stAppService->pfnServiceConfig : FALSE\n");
	return NULL;
}


/*==========================================================================*
 * FUNCTION :  Web_ModifyNMSPrivateConfigure
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szNmsBuffer:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_ModifyNMSPrivateConfigure(IN char *szNmsBuffer)
{
#define ADD_TYPE_OF_NMS					1
#define MODIFY_TYPE_OF_NMS				2
#define DELETE_TYPE_OF_NMS				3
#define CHANGETRAP_TYPE_OF_NMS			4

	ASSERT(szNmsBuffer);
	NMS_INFO		stNmsInfo;
	APP_SERVICE		*stAppService = NULL;
	int				iBufLen = sizeof(NMS_INFO);
	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	int				iModifyType = 0;
	int				iReturn = 0;
	char			*szTrim = NULL;

//added by YangGuoxin,3/20/2007
	int				iAllBufLen = 1;
	NMS_INFO		*stAllNmsInfo = NULL;
	int				i = 0, iNMSNum = 0;
//end by YangGuoxin, 3/20/2007
	
	//�ֽⷢ�͹����Ļ�����
	//TRACE("szNmsBuffer : %s\n", szNmsBuffer);
	if((ptr = szNmsBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				iModifyType = atoi(szExchange);
				ptr = ptr + iPosition;
			}
			else
			{
				return FALSE;
			}
			
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				char		*szIP = NULL;
				szIP = Cfg_RemoveWhiteSpace(szExchange + 1);
				stNmsInfo.ulIpAddress = inet_addr(szIP);

				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				szTrim = Cfg_RemoveWhiteSpace(szExchange);
				sprintf(stNmsInfo.szPublicCommunity, szTrim, sizeof(stNmsInfo.szPublicCommunity));
				//TRACE("szPublicCommunity : %s\n", stNmsInfo.szPublicCommunity);
				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				szTrim = Cfg_RemoveWhiteSpace(szExchange);
				sprintf(stNmsInfo.szPrivateCommunity, szTrim, sizeof(stNmsInfo.szPrivateCommunity));
				//TRACE("szPrivateCommunity : %s\n", stNmsInfo.szPrivateCommunity);
				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;
		

		strncpyz(szExchange, ptr, (int)strlen(ptr) + 1);
		stNmsInfo.nTrapLevel = atoi(szExchange) - 1;

	}
	else
	{
		return FALSE;
	}


	//�޸�
	//TRACE("NMS_INFO : %d     %d %s\n  %s [%s]\n", iModifyType,   stNmsInfo.nTrapLevel, 
	//									stNmsInfo.szPrivateCommunity, 
	//									stNmsInfo.szPublicCommunity, inet_ntoa(*((struct in_addr *)(&stNmsInfo.ulIpAddress))));
	stNmsInfo.nPrivilege = 0;
	
	if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV2_NAME);
	}
	else
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
	}

	//stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMS_NAME);

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{
//added by YangGuoxin,3/20/2007
		if(iModifyType == DELETE_TYPE_OF_NMS)// delete
		{
			iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										iModifyType,
										&iBufLen,
										(void *)&stNmsInfo);// == ERR_SNP_OK)
		}
		else if(iModifyType == CHANGETRAP_TYPE_OF_NMS)// change trap
		{
			if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
											stAppService->bServiceRunning,
											&stAppService->args, 
											GET_NMS_USER_INFO,
											&iAllBufLen,
											(void *)&stAllNmsInfo) == ERR_SNP_OK)
			{
				iNMSNum =  iAllBufLen /sizeof(NMS_INFO);

			
				stAllNmsInfo->nTrapLevel = stNmsInfo.nTrapLevel;
				iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										CHANGETRAP_TYPE_OF_NMS,
										&iBufLen,
										(void *)stAllNmsInfo);

			}

		}
		else if(iModifyType == MODIFY_TYPE_OF_NMS )//modify 
		{
			if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
											stAppService->bServiceRunning,
											&stAppService->args, 
											GET_NMS_USER_INFO,
											&iAllBufLen,
											(void *)&stAllNmsInfo) == ERR_SNP_OK)
			{
				iNMSNum =  iAllBufLen /sizeof(NMS_INFO);

				for(i = 0; i < iNMSNum && stAllNmsInfo != NULL; i++, stAllNmsInfo++)
				{
					if(stAllNmsInfo->ulIpAddress == stNmsInfo.ulIpAddress)
					{
						stNmsInfo.nTrapLevel = stAllNmsInfo->nTrapLevel  ;
						iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
											stAppService->bServiceRunning,
											&stAppService->args, 
											iModifyType,
											&iBufLen,
											(void *)&stNmsInfo);
					}
				}

			}
			 
		} 
		else if(iModifyType == ADD_TYPE_OF_NMS)//add 
		{
			if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
											stAppService->bServiceRunning,
											&stAppService->args, 
											GET_NMS_USER_INFO,
											&iAllBufLen,
											(void *)&stAllNmsInfo) == ERR_SNP_OK)
			{
				iNMSNum =  iAllBufLen /sizeof(NMS_INFO);
				if(iNMSNum > 0)
				{
					stNmsInfo.nTrapLevel = stAllNmsInfo->nTrapLevel;
					iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										iModifyType,
										&iBufLen,
										(void *)&stNmsInfo);// == ERR_SNP_OK)
				}
				else
				{
					stNmsInfo.nTrapLevel = 0 ;
					iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										iModifyType,
										&iBufLen,
										(void *)&stNmsInfo);// == ERR_SNP_OK)
				}
				

			}

			
		}

		 
//end by YangGuoxin, 3/20/2007

		if(iReturn == ERR_SNP_OK)
		{
			return TRUE;//iReturn;
		}
		else if(iReturn == ERR_NMS_TOO_MANY_USERS)
		{
			return 6;
		}
		else
		{
			return FALSE;
		}

	}
	return -1;
}


/*==========================================================================*
 * FUNCTION :  Web_MakeACUTimeSrv
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/

static char *Web_MakeACUTimeSrv(void)
{
	TIME_SRV_INFO	*pTimeSvrInfo = NULL;
	int				iError = 0, iLen = 0;
	char			*szTime = NULL;
	struct in_addr	inIP;


	iBufLen = sizeof(TIME_SRV_INFO);
	iError += DxiGetData(VAR_TIME_SERVER_INFO,
						TIME_SRV_CONFIG,			
						0,		
						&iBufLen,			
						&pTimeSvrInfo,			
						0);
	
	if (iError == ERR_DXI_OK)
	{	
		//TRACE("iError : %d\n", iError);
		if((szTime = NEW(char, 128)) != NULL)
		{
			inIP.s_addr = pTimeSvrInfo->ulMainTmSrvAddr;

			iLen += sprintf(szTime + iLen, "%s%c",inet_ntoa(inIP), 59);
			
			inIP.s_addr = pTimeSvrInfo->ulBakTmSrvAddr;
			iLen += sprintf(szTime + iLen, "%s%c%ld%c",
				inet_ntoa(inIP),59, pTimeSvrInfo->dwJustTimeInterval, 59);
			iLen += sprintf(szTime + iLen, "%ld%c",
				 pTimeSvrInfo->lTimeZone, 59);


			return szTime;
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		return NULL;
	}

}


/*==========================================================================*
 * FUNCTION :  Web_SetACUTimeSrv
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iModiyType:
				IN void *pModiyValue:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_SetACUTimeSrv(IN int iModiyType, IN void *pModiyValue)
{
	int		iError = ERR_DXI_OK;
	ULONG	ulIP;
	char	szIP[33], szExchange[33];
	char	szTimeZone[16];
	time_t	tmModiy;
	int		iInterval = 0;
	char	*pSearchValue = NULL;
	int		iPosition = 0;
	char	*ptr = NULL;
	char	*szTrimText = NULL;

	ptr = (char *)pModiyValue;

	//TRACE("Web_SetACUTimeSrv : %s\n", (char *)pModiyValue);
	if(iModiyType == WEB_MODIFY_TIMESRV_IP)
	{
		//Modify IP
		iBufLen = sizeof(ULONG);

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 1)
			{
				strncpyz(szIP, ptr, iPosition + 1);
				szTrimText = Cfg_RemoveWhiteSpace(szIP);
				if((strcmp(szTrimText,"0.0.0.0") == 0) || atoi(szTrimText) == 0)
				{
					ulIP = inet_addr("0.0.0.0");
				}
				else
				{
					ulIP = inet_addr(szTrimText);
				}

				iError += DxiSetData(VAR_TIME_SERVER_INFO,
							TIME_SRV_IP,			
							0,		
							iBufLen,			
							&ulIP,			
							0);
				ptr = ptr + iPosition;
			}
		}
		ptr = ptr + 1;
		
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 1)
			{
				strncpyz(szIP, ptr, iPosition + 1);
				szTrimText = Cfg_RemoveWhiteSpace(szIP);
				
				if((strcmp(szTrimText,"0.0.0.0") == 0) || atoi(szTrimText) == 0)
				{
					ulIP = inet_addr("0.0.0.0");
				}
				else
				{
					ulIP = inet_addr(szTrimText);
				}
				
				iError += DxiSetData(VAR_TIME_SERVER_INFO,
							TIME_SRV_BACKUP_IP,			
							0,		
							iBufLen,			
							&ulIP,			
							0);

				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

			
		
		pSearchValue = strchr(ptr, 59);
		
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition >= 1)
			{
				strncpyz(szIP, ptr, iPosition + 1);
				iInterval = atoi(szIP);
				//TRACE("\niInterval %d\n", iInterval);
				iBufLen = sizeof(DWORD);
				iError += DxiSetData(VAR_TIME_SERVER_INFO,
										TIME_SYNC_INTERVAL,			
										0,		
										iBufLen,			
										&iInterval,			
										0);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition >= 1)
			{
			
				strncpyz(szIP, ptr, iPosition + 1);
				szTrimText = Cfg_RemoveWhiteSpace(szIP);
				iInterval = atoi(szTrimText);
				
				iBufLen = sizeof(LONG);
				iError += DxiSetData(VAR_TIME_SERVER_INFO,
										TIME_SYNC_TIMEZONE,			
										0,		
										iBufLen,			
										&iInterval,			
										0);
			}

		}

	}
	else if(iModiyType == WEB_MODIFY_TIME)
	{
		//�޸�ʱ��
		pSearchValue = strchr(ptr, 59);
		iBufLen = sizeof(time_t);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 1)
			{
				
				strncpyz(szExchange, ptr, iPosition + 1);
				tmModiy = (time_t)atoi(szExchange);
				iError += DxiSetData(VAR_TIME_SERVER_INFO,
									SYSTEM_TIME,			
									0,		
									iBufLen,			
									(void *)&tmModiy,			
									0);
			}
		}
		
       
	}
	else
	{
		return FALSE;
	}
	
	if (iError == ERR_DXI_OK)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
 * FUNCTION :  Web_GetStdEquipSingalInfo
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int iEquipTypeID: 
				IN int iSignalType:
				IN int iSignalID:
				IN int iLanguage:
				OUT char **szSignalInfo:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_GetStdEquipSingalInfo(IN int iEquipTypeID, 
									 IN int iSignalType, 
									 IN int iSignalID, 
									 IN int iLanguage, 
									 OUT char **szSignalInfo)
{
	UNUSED(iSignalID);

	char		*szSendBuffer = NULL;

	STDEQUIP_TYPE_INFO		*pTStdEquipType = NULL, *pStdEquipType = NULL;
	
	ALARM_SIG_INFO			*pAlarmSigInfo = NULL;
	SAMPLE_SIG_INFO			*pSampleSigInfo = NULL;
	CTRL_SIG_INFO			*pCtrlSigInfo = NULL;
	SET_SIG_INFO			*pSetSigInfo = NULL;

	int						iStdEquipTypeNum = 0;//, iSignalType = 0;

	int			i = 0, j = 0;
	int			iError = 0;
	int			iLen = 0 ;
	int			iSignalNum = 0;

	iError +=  DxiGetData(VAR_STD_EQUIPS_NUM,
						0,			
						0,		
						&iBufLen,			
						(void *)&iStdEquipTypeNum,			
						0);
	
	iError +=  DxiGetData(VAR_STD_EQUIPS_LIST,
						0,			
						0,		
						&iBufLen,			
						(void *)&pTStdEquipType,			
						0);

	if(iError != ERR_DXI_OK)
	{
		return FALSE;
	}
	pStdEquipType = pTStdEquipType;
	//TRACE("iStdEquipTypeNum : %d\n", iStdEquipTypeNum);
	szSendBuffer = NEW(char, 12);
	iLen += sprintf(szSendBuffer + iLen,"%5d", iEquipTypeID);
	iLen += sprintf(szSendBuffer + iLen,"%5d", iSignalType);

	for (i = 0; i < iStdEquipTypeNum && pStdEquipType != NULL; i++, pStdEquipType++)
	{
		if (pStdEquipType->iTypeID == iEquipTypeID)
		{
			
			pAlarmSigInfo = pStdEquipType->pAlarmSigInfo;
			iSignalNum = pStdEquipType->iAlarmSigNum;
			

			szSendBuffer	= RENEW(char, szSendBuffer, strlen(szSendBuffer) + iSignalNum * 100 + 5);

			//TRACE("iSignalNum : %d\n", iSignalNum);

			if(szSendBuffer == NULL )
			{
				return FALSE;
			}
	
			for(j = 0; j < iSignalNum && pAlarmSigInfo != NULL; j++, pAlarmSigInfo++)
			{
					iLen += sprintf(szSendBuffer + iLen, "new OAlarm(\"%s\", \"%s\",%d, %d)%c\n", 
										pAlarmSigInfo->pSigName->pFullName[iLanguage],
										pAlarmSigInfo->pSigName->pAbbrName[iLanguage],
										pAlarmSigInfo->iSigID,
										pAlarmSigInfo->iAlarmLevel,
										(j == (iSignalNum - 1)) ? 32 : 44);
				
	
			}
			pAlarmSigInfo = NULL;

			//szSendBuffer[iLen -1] =  59;
			iLen += sprintf(szSendBuffer + iLen,"%c", 59);

			
			pSampleSigInfo = pStdEquipType->pSampleSigInfo;
			iSignalNum = pStdEquipType->iSampleSigNum;
			szSendBuffer	= RENEW(char, szSendBuffer, strlen(szSendBuffer) + iSignalNum * 100 + 5);

			if(szSendBuffer == NULL)
			{
				return FALSE;
			}
			iSignalType = 0;
			for(j = 0; j < iSignalNum && pSampleSigInfo != NULL; j++, pSampleSigInfo++)
			{
				if((STD_SIG_IS_DISPLAY_ON_UI(pSampleSigInfo,DISPLAY_WEB) == TRUE) ||
					(STD_SIG_IS_DISPLAY_ON_UI(pSampleSigInfo,DISPLAY_LCD) == TRUE))
				{
					iLen += sprintf(szSendBuffer + iLen, "new OSample(\"%s\", \"%s\",%d)%c\n", 
									pSampleSigInfo->pSigName->pFullName[iLanguage],
									pSampleSigInfo->pSigName->pAbbrName[iLanguage],
									pSampleSigInfo->iSigID,
									44);//(j == (iSignalNum - 1)) ? 32 : 44);
					iSignalType++;
				}
			}
			char	*pSearchValue = strrchr(szSendBuffer, 44);  //get the last ','
			if(pSearchValue != NULL && iSignalType != 0)
			{
				*pSearchValue = 32;   //replace ',' with ' '
			}

			pSampleSigInfo = NULL;
			//szSendBuffer[iLen -1] =  59;
			iLen += sprintf(szSendBuffer + iLen,"%c", 59);

			pCtrlSigInfo = pStdEquipType->pCtrlSigInfo;
			iSignalNum = pStdEquipType->iCtrlSigNum;


			szSendBuffer	= RENEW(char, szSendBuffer, strlen(szSendBuffer) + iSignalNum * 100 + 5);
			if(szSendBuffer == NULL)
			{
				return FALSE;
			}

			iSignalType = 0;
			for(j = 0; j < iSignalNum && pCtrlSigInfo != NULL; j++, pCtrlSigInfo++)
			{
				if((STD_SIG_IS_DISPLAY_ON_UI(pCtrlSigInfo,DISPLAY_WEB) == TRUE) || 
							(STD_SIG_IS_DISPLAY_ON_UI(pCtrlSigInfo,DISPLAY_LCD) == TRUE))
				{
					iLen += sprintf(szSendBuffer + iLen, "new OControl(\"%s\",  \"%s\",%d)%c\n", 
									pCtrlSigInfo->pSigName->pFullName[iLanguage],
									pCtrlSigInfo->pSigName->pAbbrName[iLanguage],
									pCtrlSigInfo->iSigID,
									44);//(j == (iSignalNum - 1)) ? 32 : 44);
					iSignalType++;
				}
			}
			pSearchValue = strrchr(szSendBuffer, 44);  //get the last ','
			if(pSearchValue != NULL && iSignalType != 0)
			{
				*pSearchValue = 32;   //replace ',' with ' '
			}

			pCtrlSigInfo = NULL;
			//szSendBuffer[iLen -1] =  59;
			iLen += sprintf(szSendBuffer + iLen,"%c", 59);


			pSetSigInfo = pStdEquipType->pSetSigInfo;
			iSignalNum = pStdEquipType->iSetSigNum;
			szSendBuffer	= RENEW(char, szSendBuffer, strlen(szSendBuffer) + iSignalNum * 100 + 5);
			if(szSendBuffer == NULL)
			{
				return FALSE;
			}

			iSignalType = 0;
			for(j = 0; j < iSignalNum && pSetSigInfo != NULL; j++, pSetSigInfo++)
			{
				if((STD_SIG_IS_DISPLAY_ON_UI(pSetSigInfo,DISPLAY_WEB) == TRUE) || 
							(STD_SIG_IS_DISPLAY_ON_UI(pSetSigInfo,DISPLAY_LCD) == TRUE))
				{
					iLen += sprintf(szSendBuffer + iLen, "new OSetting(\"%s \",  \"%s\",%d)%c\n", 
									pSetSigInfo->pSigName->pFullName[iLanguage],
									pSetSigInfo->pSigName->pAbbrName[iLanguage],
									pSetSigInfo->iSigID,
									44);//(j == (iSignalNum - 1)) ? 32 : 44);
					iSignalType++;
				}
	
			}
			pSearchValue = strrchr(szSendBuffer, 44);  //get the last ','
			if(pSearchValue != NULL && iSignalType != 0)
			{
				*pSearchValue = 32;   //replace ',' with ' '
			}

			pSetSigInfo = NULL;
			//szSendBuffer[iLen -1] =  59;
			iLen += sprintf(szSendBuffer + iLen,"%c", 59);

			break;

		}

	}
	if(iLen >= 2)
	{
		//TRACE("szSendBuffer : %s\n", szSendBuffer);
		*szSignalInfo = szSendBuffer;
		return iLen;
	}
	else
	{
		if(szSendBuffer != NULL)
		{
			DELETE(szSendBuffer);
			szSendBuffer = NULL;
		}
		return FALSE;
	}
}

/*==========================================================================*
 * FUNCTION :  CloseACU
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static void CloseACU(void)
{
	//stop acu
	raise(SIGTERM); 
	raise(SIGTERM); 
}

static void Web_ReplaceFile(IN char *szBuffer)
{
	ASSERT(szBuffer);
	char		szFileName[128];
	char		*pSearchValue1 =  NULL, *pSearchValue2 =  NULL;
	int			iPosition = 0;
	int			iLen = 0;
	if(szBuffer)
	{
		//cp
		system(szBuffer);
		//rm
		pSearchValue1 = strchr(szBuffer, 47); //get the last space
		if(pSearchValue1 != NULL)
		{
			pSearchValue2 = strchr(pSearchValue1, 32); //get the last space
			if(pSearchValue2 != NULL)
			{
				iPosition = pSearchValue2 - pSearchValue1;
				if(iPosition > 0)
				{
					iLen = sprintf(szFileName, "%s", "rm -rf  ");
					strncpyz(szFileName + iLen, pSearchValue1, iPosition + 1);
					system(szFileName);
				}
			}
		}
	}
}
/*==========================================================================*
 * FUNCTION :  Web_GetSetUserInfo
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szGetUserInfo:
				OUT char *szUserName:
				OUT char *szUserPassword:
				OUT char *szUserLevel:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_GetSetUserInfo(IN char *szGetUserInfo, OUT char *szUserName, 
						  OUT char *szUserPassword, OUT char *szUserLevel)
{
	char		*pUserInfo = szGetUserInfo;
	char		*pSearchValue = NULL;
	int			iPosition = 0;
	//char		szUserNameInfo[33], szUserPasswordInfo[33], szUserLevelInfo[33];
	
	//TRACE("szGetUserInfo : %s\n", szGetUserInfo);
	if(pUserInfo != NULL)
	{
		pSearchValue = strchr(pUserInfo, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pUserInfo;
			if(iPosition  > 0 )
			{
				strncpyz(szUserName, pUserInfo, 
									((iPosition + 1) < 33) ? (iPosition + 1) : 33);
				//szUserName = Cfg_RemoveWhiteSpace(szUserNameInfo);
				////TRACE("szUserNameInfo : %s szUserName :%s\n", szUserNameInfo, szUserName);
				pUserInfo = pUserInfo + iPosition;

			}
			else
			{
				return FALSE;
			}
			pUserInfo = pUserInfo + 1;
		}
		else
		{
			return FALSE;
		}
		
		pSearchValue = strchr(pUserInfo, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pUserInfo;
			if(iPosition  > 0)
			{
				strncpyz(szUserPassword, pUserInfo, 
								((iPosition + 1) < 33) ? (iPosition + 1) : 33);
				//szUserPassword = Cfg_RemoveWhiteSpace(szUserPasswordInfo);
				////TRACE("szUserPasswordInfo : %s szUserPassword %s:\n", szUserPasswordInfo, szUserPassword);
				pUserInfo = pUserInfo + iPosition;
			}
			else
			{
				return FALSE;
			}
			pUserInfo = pUserInfo + 1;
		}
		else
		{
			return FALSE;
		}

		pSearchValue = strchr(pUserInfo, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pUserInfo;
			if(iPosition  > 0)
			{
				strncpyz(szUserLevel, pUserInfo, 
									((iPosition + 1) < 33) ? (iPosition + 1) : 33);
				//szUserLevel = Cfg_RemoveWhiteSpace(szUserLevelInfo);
				////TRACE("szUserLevelInfo : %s szUserLevel : %s\n", szUserLevelInfo, szUserLevel);
				//pUserInfo = pUserInfo + iPosition;
			}
			else
			{
				return FALSE;
			}
			//pUserInfo = pUserInfo + 1;
		}
		else
		{
			return FALSE;
		}
	}
	return TRUE;
}

DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{	
	DWORD dwExitCode;
	dwExitCode = StartWebCommunicate(&pArgs->nQuitCommand);
	return dwExitCode;

}

/*==========================================================================*
 * FUNCTION :  StartTransferFile
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN int *iQuitCommand:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int StartTransferFile(IN int *iQuitCommand)
{
	/*transfer local file, transfer english file*/
	int			i = 0, k = 0;
	FILE		*fp1 ;
	char		*pHtml1 = NULL, *pHtml2 = NULL;
    char        *pHtml3 = NULL;//Add by wj for three languages,2006.4.27 
	int			iError = 0;
    int         iError2=0;//Add by wj for three languages,2006.4.27
	int			iReturn1 = 0, iReturn2 = 0; 
    int         iReturn3 = 0;
	char		*szFile = NULL;
	char		*szMakeVar = NULL;
	double		fVersion; 
    double      fVersion2; //Add by wj for three languages,2006.4.27
	
#define WEB_MULILANGUAGE_ENG			"Language_type"

#define WEB_MAINPAGE_LINK_ENG			"eng/p01_main_frame.htm"
#define WEB_MAINPAGE_LINK_LOC			"loc/p01_main_frame.htm"
#define WEB_MAINPAGE_LINK_LOC2			"loc2/p01_main_frame.htm" //Add by wj for three languages,2006.4.27
#define WEB_MAINPAGE_LINK_ID			"ID_LOGIN_LINK"

#define WEB_LOGIN_LINK_ENG				"eng/login.htm"
#define WEB_LOGIN_LINK_LOC				"loc/login.htm"
#define WEB_LOGIN_LINK_LOC2				"loc2/login.htm"  //Add by wj for three languages,2006.4.27

#define WEB_LOGIN_OVERTIME_LINK_ID		"ID_LOGIN_OVERTIME_LINK"

#define WEB_LOC_FILE_UPLOAD_PAGE		"loc/p33_replace_file.htm"
#define WEB_LOC2_FILE_UPLOAD_PAGE		"loc2/p33_replace_file.htm"//Add by wj for three languages,2006.4.27
#define WEB_ENG_FILE_UPLOAD_PAGE		"eng/p33_replace_file.htm"
#define WEB_FILE_LOAD					"ID_FILE_LOAD"

#define WEB_LOGIN_OVERTIME_CANCEL		"ID_LOGIN_OVERTIME_CANCEL"
#define WEB_LOC_FILE_MAIN_PAGE			"loc/p03_main_menu.htm"
#define WEB_LOC2_FILE_MAIN_PAGE			"loc2/p03_main_menu.htm"//Add by wj for three languages,2006.4.27
#define WEB_ENG_FILE_MAIN_PAGE			"eng/p03_main_menu.htm"

//#define WEB_LOGIN_PAGES_NO				15
//#define WEB_P02_PAGES_NO				17
#define WEB_RES_FILE_LOGIN				"login.htm"
#define WEB_RES_FILE_OVERTIME			"p17_login_overtime.htm"
#define WEB_RES_FILE_REPLACE			"p31_close_system.htm"

#define WEB_TREE_VIEW_T					"ID_TREE_VIEW_T"
#define WEB_TREE_VIEW_T_ENG_VALUE		"/var/eng/j02_tree_view_t.js"
#define WEB_TREE_VIEW_T_LOC_VALUE		"/var/loc/j02_tree_view_t.js"
#define WEB_TREE_VIEW_T_LOC2_VALUE		"/var/loc2/j02_tree_view_t.js"//Add by wj for three languages,2006.4.27

#define WEB_EQUIP_LIST_ID				"ID_EQUIP_LIST_DEF"
#define WEB_EQUIP_LIST_ENG				"/var/eng/j77_equiplist_def.htm"
#define WEB_EQUIP_LIST_LOC				"/var/loc/j77_equiplist_def.htm"
#define WEB_EQUIP_LIST_LOC2				"/var/loc2/j77_equiplist_def.htm"//Add by wj for three languages,2006.4.27

#define WEB_SERIAL_NUMBER				"ID_SERIAL_NUMBER"
#define WEB_HARDWARE_VERSION			"ID_HARDWARE_VERSION"
#define WEB_SOFTWARE_VERSION			"ID_SOFTWARE_VERSION"
#define	WEB_CFG_VERSION					"ID_CFG_VERSION"
#define WEB_PART_NUMBER					"ID_PART_NUMBER"

#define WEB_ZH_LANGUAGE					"zh"
#define WEB_GB2312_CHARCODE				"gb2312"
#define WEB_UTF8_CHARCODE				"UTF-8"
#define WEB_CHARCODE_ID					"ID_CHARSET"

	//TRACE("starting to transfer WebPages!============1\n");
    //TRACE("into StartTransferFile");
	iError = WEB_ReadConfig();

#ifdef _SUPPORT_THREE_LANGUAGE 
    iError2 = WEB_ReadConfigLoc2();//Add by wj for three languages,2006.4.27
#endif
	//TRACE("starting to transfer WebPages!=====%d=======\n", iError);
    //printf("WEB_ReadConfig OK!");
	//if( iError == ERR_WEB_OK && pWebCfgInfo != NULL && pWebHeadInfo != NULL)

#ifdef _SUPPORT_THREE_LANGUAGE
	if(iError == ERR_WEB_OK && iError2 == ERR_WEB_OK)
#else
    if(iError == ERR_WEB_OK)
#endif
	{
		/*Judge version*/
		//fVersion  = pWebHeadInfo->stWebHeadInfo->CfgVersion;
		fVersion = stWebCompareVersion.fVersion;
		ACU_PRODUCT_INFO sAcuProductInfo;

		int		iBufLen;

		memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));

		iBufLen = sizeof(sAcuProductInfo);

		iError = DxiGetData(VAR_ACU_PUBLIC_CONFIG,
						ACU_PRODUCT_INFO_GET,  
						0, 
						&iBufLen,
						&(sAcuProductInfo),
						0);
		WEB_VERSION_INFO		stVersion;
		Web_GetVersionOfWebPages_r(&stVersion);
		//TRACE("fVersion = %f stVersion.fVersion = %s stVersion.szLanguage = %s\n", fVersion, stVersion.fVersion, stVersion.szLanguage);
       // if(fVersion > Web_GetVersionOfWebPages())
#ifdef _SUPPORT_THREE_LANGUAGE
		if(fVersion >  stVersion.fVersion || fVersion <  stVersion.fVersion || strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersion.szLanguage), Cfg_RemoveWhiteSpace(stVersion.szLanguage)) != 0
            || fVersion2 >  stVersion.fVersion2 || fVersion2 <  stVersion.fVersion2 || strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersion.szLanguage2), Cfg_RemoveWhiteSpace(stVersion.szLanguage2)) != 0)//Add by wj for three languages,2006.4.27
#else

		//printf("[Web_ui][Web_Resource_zh.res] version is %f, language is %s\n", fVersion, stWebCompareVersion.szLanguage);
		//printf("[Web_Res_Ver.cfg] version is %f, language is %s\n", stVersion.fVersion, stVersion.szLanguage);
        if(fVersion >  stVersion.fVersion || fVersion <  stVersion.fVersion || strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersion.szLanguage), Cfg_RemoveWhiteSpace(stVersion.szLanguage)) != 0)
#endif
        {
			TRACE("********************starting to transfer WebPages!**********************************\n");


			//////////////////////////////////////////////////////////////////////////
			/*int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 183);
			int		iSystemID = 1;
			int		iBufLen = sizeof(VAR_VALUE_EX);
			VAR_VALUE_EX	value;


			value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
			value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
			value.varValue.enumValue  = 0;



			DxiSetData(VAR_A_SIGNAL_VALUE,
				iSystemID,			
				iVarSubID,		
				iBufLen,			
				&value,			
				10000);*/
			//////////////////////////////////////////////////////////////////////////


			
			//for(i = 0; i < iPagesNumber; i++)
			for(i = 0; i < WEB_MAX_HTML_PAGE_NUM; i++)
			{
				
				
				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 3);
				//TRACE("********************[%d][%s][%d]**********************************\n",i, szFile,pWebCfgInfo[i].iNumber);
				ASSERT(szFile);
				if(szFile != NULL)
				{
					iReturn1 = LoadHtmlFile(szFile, &pHtml1);
					iReturn2 = LoadHtmlFile(szFile, &pHtml2);

#ifdef _SUPPORT_THREE_LANGUAGE
                    iReturn3 = LoadHtmlFile(szFile, &pHtml3);//Add by wj for three languages,2006.4.27
#endif
					DELETE(szFile);
					szFile = NULL;
				}

#ifdef _SUPPORT_THREE_LANGUAGE
				if((iReturn1 > 0) && (iReturn2 > 0) && (iReturn3 > 0))
#else
                if((iReturn1 > 0) && (iReturn2 > 0))
#endif
				{
					////TRACE("*****%d*****pWebCfgInfo[i].iNumber : %d\n",i, pWebCfgInfo[i].iNumber);

				    //j04_gfunc_def.js�滻�Ƿ�֧��SSL
					if(strcmp((const char *)pWebCfgInfo[i].szFileName, "j04_gfunc_def.js") == 0)
					{
					    if(g_SiteInfo.iSSLFlag == NO_SSL)
					    {
						//printf("Run the Webpages without SSL!\n");
						}
					    else
					    {
						//printf("Run the Webpages with SSL!\n");
						szMakeVar = MakeVarField("ID_SYSTEMTYPE");
						if(szMakeVar != NULL)
						{
						    ReplaceString(&pHtml1, 
							szMakeVar,
							"SSL");
						    DELETE(szMakeVar);
						    szMakeVar = NULL;
						}

						/*Loc*/
						szMakeVar = MakeVarField("ID_SYSTEMTYPE");
						if(szMakeVar != NULL)
						{
						    ReplaceString(&pHtml2, 
							szMakeVar, 
							"SSL");
						    DELETE(szMakeVar);
						    szMakeVar = NULL;
						}
					    }
					}

					for(k = 0; k < pWebCfgInfo[i].iNumber; k++)
					{
						//TRACE("szResourceID : %s\n", MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID));
						/*Eng*/
						szMakeVar = MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID);
						if(szMakeVar != NULL)
						{
							ReplaceString(&pHtml1, 
									szMakeVar,
									pWebCfgInfo[i].stWebPrivate[k].szDefault);
							DELETE(szMakeVar);
							szMakeVar = NULL;
						}
						

						/*Loc*/
						szMakeVar = MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID);
						if(szMakeVar != NULL)
						{
							ReplaceString(&pHtml2, 
									szMakeVar, 
									pWebCfgInfo[i].stWebPrivate[k].szLocal);
							DELETE(szMakeVar);
							szMakeVar = NULL;
						}


                         
                        /*Loc2*///Add by wj for three languages,2006.4.27
#ifdef _SUPPORT_THREE_LANGUAGE
                        szMakeVar = MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID);
                        if(szMakeVar != NULL)
                        {
                            ReplaceString(&pHtml3, 
                                szMakeVar, 
                                pWebCfgInfo[i].stWebPrivate[k].szLocal2);
                            DELETE(szMakeVar);
                            szMakeVar = NULL;
                        }
#endif

						
					}
					//language
					ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_MULILANGUAGE_ENG),
								"0");

					ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_MULILANGUAGE_ENG),
								"1");

                    #ifdef _SUPPORT_THREE_LANGUAGE
                    ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_MULILANGUAGE_ENG),
                                "2");//Added by wj for three languages,2006.4.27
                    #endif

					ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_MAINPAGE_LINK_ID),
								WEB_MAINPAGE_LINK_ENG);

					ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_MAINPAGE_LINK_ID),
								WEB_MAINPAGE_LINK_LOC);

                    #ifdef _SUPPORT_THREE_LANGUAGE
                    ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_MAINPAGE_LINK_ID),
                                 WEB_MAINPAGE_LINK_LOC2);//Add by wj for three languages,2006.4.27
                    #endif

					ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_LOGIN_OVERTIME_LINK_ID),
								WEB_LOGIN_LINK_ENG);

					ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_LOGIN_OVERTIME_LINK_ID),
								WEB_LOGIN_LINK_LOC);

                    #ifdef _SUPPORT_THREE_LANGUAGE
                    ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_LOGIN_OVERTIME_LINK_ID),
                                WEB_LOGIN_LINK_LOC2);//Add by wj for three languages,2006.4.27
                    #endif

					ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_FILE_LOAD),
								WEB_ENG_FILE_UPLOAD_PAGE);

					ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_FILE_LOAD),
								WEB_LOC_FILE_UPLOAD_PAGE);

                    #ifdef _SUPPORT_THREE_LANGUAGE
                    ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_FILE_LOAD),
                                WEB_LOC2_FILE_UPLOAD_PAGE);//Add by wj for three languages,2006.4.27
                    #endif


					ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_LOGIN_OVERTIME_CANCEL),
								WEB_ENG_FILE_MAIN_PAGE);

					ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_LOGIN_OVERTIME_CANCEL),
								WEB_LOC_FILE_MAIN_PAGE);

                    #ifdef _SUPPORT_THREE_LANGUAGE
                    ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_LOGIN_OVERTIME_CANCEL),
                                WEB_LOC2_FILE_MAIN_PAGE);//Add by wj for three languages,2006.4.27
                    #endif

					ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_TREE_VIEW_T),
								WEB_TREE_VIEW_T_ENG_VALUE);

					ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_TREE_VIEW_T),
								WEB_TREE_VIEW_T_LOC_VALUE);

                    #ifdef _SUPPORT_THREE_LANGUAGE
                    ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_TREE_VIEW_T),
                                WEB_TREE_VIEW_T_LOC2_VALUE);//Add by wj for three languages,2006.4.27
                    #endif

					ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_EQUIP_LIST_ID),
								WEB_EQUIP_LIST_ENG);

					ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_EQUIP_LIST_ID),
								WEB_EQUIP_LIST_LOC);

                    #ifdef _SUPPORT_THREE_LANGUAGE
                    ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_EQUIP_LIST_ID),
                                WEB_EQUIP_LIST_LOC2);//Add by wj for three languages,2006.4.27
                    #endif

					//html charcode
					if(strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersion.szLanguage), WEB_ZH_LANGUAGE) == 0)
					{
						//GB2312
						ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_CHARCODE_ID),
								WEB_GB2312_CHARCODE);

						ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_CHARCODE_ID),
								WEB_GB2312_CHARCODE);

                        #ifdef _SUPPORT_THREE_LANGUAGE
                        ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_CHARCODE_ID),
                                WEB_GB2312_CHARCODE);//WEB_UTF8_CHARCODE);//Add by wj for three languages,2006.4.27
                        #endif
					}
					else if(strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersion.szLanguage), "ru") == 0)
					{
						//GB2312
						ReplaceString(&pHtml1, 
							MAKE_VAR_FIELD(WEB_CHARCODE_ID),
							"KOI8-R");

						ReplaceString(&pHtml2, 
							MAKE_VAR_FIELD(WEB_CHARCODE_ID),
							"KOI8-R");

#ifdef _SUPPORT_THREE_LANGUAGE
						ReplaceString(&pHtml3, 
							MAKE_VAR_FIELD(WEB_CHARCODE_ID),
							"KOI8-R");//WEB_UTF8_CHARCODE);//Add by wj for three languages,2006.4.27
#endif
					}

                    #ifdef _SUPPORT_THREE_LANGUAGE
                    else if(strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersion.szLanguage2), WEB_ZH_LANGUAGE) == 0)
                    {
                        //GB2312
                        ReplaceString(&pHtml1, 
                                MAKE_VAR_FIELD(WEB_CHARCODE_ID),
                                WEB_GB2312_CHARCODE);

                        ReplaceString(&pHtml2, 
                                MAKE_VAR_FIELD(WEB_CHARCODE_ID),
                                WEB_GB2312_CHARCODE);//WEB_UTF8_CHARCODE);
    
                       
                        ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_CHARCODE_ID),
                                WEB_GB2312_CHARCODE);//Add by wj for three languages,2006.4.27

                    }
                    #endif

					else
					{
						//for other language, use the IE encoding configure,not the html file configure
						//must modify the httpd.conf configure to enable [add AddDefaultCharset off] at the same time
						//UTF-8
						ReplaceString(&pHtml1, 
								MAKE_VAR_FIELD(WEB_CHARCODE_ID),
								"");//WEB_UTF8_CHARCODE);

						ReplaceString(&pHtml2, 
								MAKE_VAR_FIELD(WEB_CHARCODE_ID),
								"");//WEB_UTF8_CHARCODE);

                        #ifdef _SUPPORT_THREE_LANGUAGE
                        ReplaceString(&pHtml3, 
                                MAKE_VAR_FIELD(WEB_CHARCODE_ID),
                                "");//WEB_UTF8_CHARCODE);//Add by wj for three languages,2006.4.27
                        #endif
					}

					//version
					if(iError == ERR_DXI_OK)
					{
						

						ReplaceString(&pHtml1, 
							MAKE_VAR_FIELD(WEB_PART_NUMBER),
							sAcuProductInfo.szPartNumber);

						ReplaceString(&pHtml2, 
							MAKE_VAR_FIELD(WEB_PART_NUMBER),
							sAcuProductInfo.szPartNumber);


						ReplaceString(&pHtml1, 
									MAKE_VAR_FIELD(WEB_SERIAL_NUMBER),
									sAcuProductInfo.szACUSerialNo);

						ReplaceString(&pHtml2, 
									MAKE_VAR_FIELD(WEB_SERIAL_NUMBER),
									sAcuProductInfo.szACUSerialNo);

                        #ifdef _SUPPORT_THREE_LANGUAGE
                        ReplaceString(&pHtml3, 
                                    MAKE_VAR_FIELD(WEB_SERIAL_NUMBER),
                                    sAcuProductInfo.szACUSerialNo);//Add by wj for three languages,2006.4.27
                        #endif
						
						ReplaceString(&pHtml1, 
									MAKE_VAR_FIELD(WEB_HARDWARE_VERSION),
									sAcuProductInfo.szHWRevision);

						ReplaceString(&pHtml2, 
									MAKE_VAR_FIELD(WEB_HARDWARE_VERSION),
									sAcuProductInfo.szHWRevision);

                        #ifdef _SUPPORT_THREE_LANGUAGE
                        ReplaceString(&pHtml3, 
                                    MAKE_VAR_FIELD(WEB_HARDWARE_VERSION),
                                    sAcuProductInfo.szHWRevision);//Add by wj for three languages,2006.4.27
                        #endif
						
						ReplaceString(&pHtml1, 
									MAKE_VAR_FIELD(WEB_SOFTWARE_VERSION),
									sAcuProductInfo.szSWRevision);

						ReplaceString(&pHtml2, 
									MAKE_VAR_FIELD(WEB_SOFTWARE_VERSION),
									sAcuProductInfo.szSWRevision);

                        #ifdef _SUPPORT_THREE_LANGUAGE
                        ReplaceString(&pHtml3, 
                                    MAKE_VAR_FIELD(WEB_SOFTWARE_VERSION),
                                    sAcuProductInfo.szSWRevision);//Add by wj for three languages,2006.4.27
                        #endif
						//Added by YangGuoxin, 12/31/2005, ����������Ϣ��ʾ
						ReplaceString(&pHtml1, 
									MAKE_VAR_FIELD(WEB_CFG_VERSION),
									sAcuProductInfo.szCfgFileVersion);

						ReplaceString(&pHtml2, 
									MAKE_VAR_FIELD(WEB_CFG_VERSION),
									sAcuProductInfo.szCfgFileVersion);

						//system name
						ReplaceString(&pHtml1, 
							MAKE_VAR_FIELD("ID_SYSTEMNAME2"),
							g_SiteInfo.langDescription.pFullName[0]);

						ReplaceString(&pHtml2, 
							MAKE_VAR_FIELD("ID_SYSTEMNAME2"),
							g_SiteInfo.langDescription.pFullName[1]);

						ReplaceString(&pHtml1, 
							MAKE_VAR_FIELD("ID_SYSTEMNAME"),
							g_SiteInfo.langDescription.pFullName[0]);

						ReplaceString(&pHtml2, 
							MAKE_VAR_FIELD("ID_SYSTEMNAME"),
							g_SiteInfo.langDescription.pFullName[1]);

						//site name
						ReplaceString(&pHtml1, 
							MAKE_VAR_FIELD("ID_SITENAME"),
							g_SiteInfo.langSiteName.pFullName[0]);

						ReplaceString(&pHtml2, 
							MAKE_VAR_FIELD("ID_SITENAME"),
							g_SiteInfo.langSiteName.pFullName[0]);

						//site location
						ReplaceString(&pHtml1, 
							MAKE_VAR_FIELD("ID_SITENAME2"),
							g_SiteInfo.langSiteLocation.pFullName[0]);

						ReplaceString(&pHtml2, 
							MAKE_VAR_FIELD("ID_SITENAME2"),
							g_SiteInfo.langSiteLocation.pFullName[1]);

						
						
						////sys
						//ReplaceString(&pHtml1, 
						//	MAKE_VAR_FIELD("ID_SYSTEMNAME_TREE"),
						//	g_SiteInfo.langSiteLocation.pFullName[0]);

						//ReplaceString(&pHtml2, 
						//	MAKE_VAR_FIELD("ID_SYSTEMNAME_TREE"),
						//	g_SiteInfo.langSiteLocation.pFullName[1]);


						


                        #ifdef _SUPPORT_THREE_LANGUAGE
                        ReplaceString(&pHtml3, 
                                    MAKE_VAR_FIELD(WEB_CFG_VERSION),//Add by wj for three languages,2006.4.27
                                    sAcuProductInfo.szCfgFileVersion);
                        #endif
						//End;Added by YangGuoxin, 12/31/2005
					}

				}
				//Save Eng File 
				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 0); 
				if(szFile != NULL && (fp1 = fopen(szFile,"wb")) != NULL &&
						 pHtml1 != NULL)
				{

					fwrite(pHtml1,strlen(pHtml1), 1, fp1);
					fclose(fp1);

				}
				if(szFile != NULL)
				{
					DELETE(szFile);
					szFile = NULL;
				}

				//Save Loc File 
				szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 1); 
				if(szFile != NULL && (fp1 = fopen(szFile,"wb")) != NULL &&
						 pHtml1 != NULL)
				{
					fwrite(pHtml2,strlen(pHtml2), 1, fp1);
					fclose(fp1);
				}

				if(strcmp((const char *)pWebCfgInfo[i].szFileName, "j04_gfunc_def.js") == 0)
				{
				    system("rm /app/www/html/cgi-bin/j04_gfunc_def.js");
				    system("cp /app/www/html/cgi-bin/eng/j04_gfunc_def.js /app/www/html/cgi-bin/");
				}

				if(szFile != NULL)
				{
					DELETE(szFile);
					szFile = NULL;
				}

                //Save Loc2 File //Add by wj for three languages,2006.4.27
                #ifdef _SUPPORT_THREE_LANGUAGE
                szFile = MakeFilePath(pWebCfgInfo[i].szFileName, 2); 
                if(szFile != NULL && (fp1 = fopen(szFile,"wb")) != NULL &&
                    pHtml1 != NULL)
                {
                    fwrite(pHtml3,strlen(pHtml3), 1, fp1);
                    fclose(fp1);
                }
                if(szFile != NULL)
                {
                    DELETE(szFile);
                    szFile = NULL;
                }
				#endif

				if(pHtml1 != NULL)
				{
					DELETE(pHtml1);
					pHtml1 = NULL;
				}
				
				if(pHtml2 !=  NULL)
				{
					DELETE(pHtml2);
					pHtml2 = NULL;
				}

                #ifdef _SUPPORT_THREE_LANGUAGE
                if(pHtml3 !=  NULL)//Add by wj for three languages,2006.4.27
                {
                    DELETE(pHtml3);
                    pHtml3 = NULL;
                }
                #endif
				
				RunThread_Heartbeat(RunThread_GetId(NULL));
				if(*iQuitCommand == SERVICE_STOP_RUNNING)
				{
					break;
				}

				
			}
			//write version
            
			TRACE("Web_WriteVersionOfWebPages_r((double)fVersion) \n");
			Web_WriteVersionOfWebPages_r((double)fVersion, stWebCompareVersion.szLanguage);

            #ifdef _SUPPORT_THREE_LANGUAGE
            Web_WriteVersionOfWebPages_r2((double)fVersion2, stWebCompareVersion.szLanguage2);//Add by wj for three languages,2006.4.27
            #endif

			char	szCommand[128];
			sprintf(szCommand,"chmod 777 %s/*", WEB_DEFAULT_DIR);
			system(szCommand);

			//szCommand[0] = NULL;
			sprintf(szCommand,"chmod 777 %s/*", WEB_LOCAL_DIR);
			system(szCommand);

            #ifdef _SUPPORT_THREE_LANGUAGE
            sprintf(szCommand,"chmod 777 %s/*", WEB_LOCAL2_DIR);
            system(szCommand);
            #endif
		}
		TRACE("StartTransferFile OK!!!");
		/*free*/
		WEB_PRIVATE_RESOURCE_INFO	*pDelete = (WEB_PRIVATE_RESOURCE_INFO *)pWebCfgInfo;
		WEB_GENERAL_RESOURCE_INFO	*pDeleteWebPrivate = NULL;
		for(i = 0; i < WEB_MAX_HTML_PAGE_NUM && pDelete != NULL; i++, pDelete++)
		{
			////TRACE("*******%d*********\n", i);
			pDeleteWebPrivate = pDelete->stWebPrivate;
			for(k = 0; k < pDelete->iNumber && pDelete->stWebPrivate != NULL; k++, pDelete->stWebPrivate++)
			{
				if(pDelete->stWebPrivate != NULL)
				{
					if(pDelete->stWebPrivate->szDefault != NULL)
					{
						DELETE(pDelete->stWebPrivate->szDefault);
						pDelete->stWebPrivate->szDefault = NULL;
					}

					if(pDelete->stWebPrivate->szLocal != NULL)
					{
						DELETE(pDelete->stWebPrivate->szLocal);
						pDelete->stWebPrivate->szLocal = NULL;
					}

                    if(pDelete->stWebPrivate->szLocal2 != NULL)
                    {
                        DELETE(pDelete->stWebPrivate->szLocal2);
                        pDelete->stWebPrivate->szLocal2 = NULL;
                    }
				

				}
			}
			if(pDeleteWebPrivate != NULL)
			{
				DELETE(pDeleteWebPrivate);
				pDeleteWebPrivate = NULL;
			}
		}
		if(pWebCfgInfo != NULL)
		{
			//DELETE(pWebCfgInfo);
			pWebCfgInfo = NULL;
		}
		//TRACE("****************************Web_freeMemory(&pWebCfgInfo)\n");
		
		if(pWebHeadInfo != NULL)
		{
			if(pWebHeadInfo->stWebHeadInfo != NULL)
			{
				DELETE(pWebHeadInfo->stWebHeadInfo);
				pWebHeadInfo->stWebHeadInfo = NULL;
			}
			DELETE(pWebHeadInfo);
			pWebHeadInfo = NULL;
		}
		return TRUE;
	}
	else
	{
		return FALSE;
	}
	//printf("StartTransferFile OK!!");
}

/*==========================================================================*
 * FUNCTION :  MakeVarField
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szBuffer:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static char *MakeVarField(IN char *szBuffer)
{
	char	*szReturnBuffer;
	szReturnBuffer = NEW(char, 128);

	if(szReturnBuffer != NULL)
	{
		sprintf(szReturnBuffer, "/*[%s]*/", szBuffer);
		return szReturnBuffer;
	}
	return NULL;
}
static int Web_WriteVersionOfWebPages_r(IN double fVersion, IN char *szLanguage)
{
	char			*pSearchValue = NULL, *str_string = NULL, szReadString[130];
	FILE			*fp;
#define FILE_CFG_VERSION			"[LOCAL_LANGUAGE_VERSION]"
#define FILE_CFG_LANGUAGE			"[LOCAL_LANGUAGE]"
#define MAX_READ_LINE_LEN			129
	
	if((fp = fopen(WEB_PAGES_VERSION_FILE, "rb+")) != NULL)
	{
		while(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
		{
			if((pSearchValue = strstr(szReadString, FILE_CFG_LANGUAGE)) != NULL)
			{
				if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
				{
					fseek(fp,(long)-strlen(szReadString), SEEK_CUR);
					if(szLanguage != NULL)
					{
						str_string = NEW(char, strlen(szReadString) + 1);
						memset(str_string,0, 4);
						sprintf(str_string,"%3s\n",Cfg_RemoveWhiteSpace(szLanguage));
						//TRACE("szLanguage[%s]\n", szLanguage);
						//strncpyz(str_string,Cfg_RemoveWhiteSpace(szLanguage),strlen(szReadString) + 1);
						fputs(str_string, fp);

						DELETE(str_string);
						str_string = NULL;
					}
					
				}
			}
			else if((pSearchValue = strstr(szReadString,FILE_CFG_VERSION)) != NULL)
			{
				if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
				{
					fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
					str_string = NEW(char, 128);
					sprintf(str_string,"%8.2f\n",fVersion);
					fputs(str_string, fp);
					
					DELETE(str_string);
					str_string = NULL;
				}	
			}
			
		}
		fclose(fp);
	}
	return TRUE;
}

static int Web_WriteVersionOfWebPages_r2(IN double fVersion, IN char *szLanguage)
{
    char			*pSearchValue = NULL, *str_string = NULL, szReadString[128];
    FILE			*fp;
#define FILE_CFG_VERSION			"[LOCAL2_LANGUAGE_VERSION]"
#define FILE_CFG_LANGUAGE			"[LOCAL2_LANGUAGE]"
#define MAX_READ_LINE_LEN			129

    if((fp = fopen(WEB_PAGES_VERSION_FILE, "rb+")) != NULL)
    {
        while(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
        {
            if((pSearchValue = strstr(szReadString,FILE_CFG_LANGUAGE)) != NULL)
            {
                if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
                {
                    fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
                    if(szLanguage != NULL)
                    {
                        str_string = NEW(char , strlen(szReadString) + 1);
                        memset(str_string,0, 4);
                        sprintf(str_string,"%3s\n",Cfg_RemoveWhiteSpace(szLanguage));
                        //TRACE("szLanguage[%s]\n", szLanguage);
                        //strncpyz(str_string,Cfg_RemoveWhiteSpace(szLanguage),strlen(szReadString) + 1);
                        fputs(str_string, fp);

                        DELETE(str_string);
                        str_string = NULL;
                    }

                }
            }
            else if((pSearchValue = strstr(szReadString,FILE_CFG_VERSION)) != NULL)
            {
                if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
                {
                    fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
                    str_string = NEW(char, 128);
                    sprintf(str_string,"%8.2f\n",fVersion);
                    fputs(str_string, fp);

                    DELETE(str_string);
                    str_string = NULL;
                }


            }

        }
        fclose(fp);
    }
    return TRUE;
}

static int Web_WriteVersionOfWebPages(IN double fVersion)
{
	UNUSED(fVersion);
	
	return TRUE;
}


static double Web_GetVersionOfWebPages_r(WEB_VERSION_INFO  *stWebVersionInfo_R)
{
    //TRACE("into Web_GetVersionOfWebPages_r");

	int		iRst;
	void	*pProf = NULL;  
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;



	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_WEB_PRIVATE_R, szCfgFileName, MAX_FILE_PATH);
	
	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{	
        TRACE("file %s open failure");
		return FALSE;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return FALSE;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);
		return FALSE;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);
	
	if (pProf == NULL)
	{
		DELETE(szInFile);
		return FALSE;
	}

	//1.Read Local language version
	iRst = Cfg_ProfileGetString(pProf,
	    WEB_LOCAL_LANGUAGE, 
	    stWebVersionInfo_R->szLanguage,
	    sizeof(stWebVersionInfo_R->szLanguage)); 
	//TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
	if (iRst != 1)
	{
	    AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
		"There are no pages number in Resource.cfg.");
	}

	char	szVersion[32];
	//0.Read Local language version
	iRst = Cfg_ProfileGetString(pProf,
							WEB_LOCAL_LANGUAGE_VERSION, 
							szVersion,
							sizeof(szVersion)); 
	stWebVersionInfo_R->fVersion = atof(szVersion);

	//TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
	if (iRst != 1)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
				"There are no pages number in Resource.cfg.");
	}

    //2.Read Local2 language version//Add by wj for three languages,2006.4.27
    //iRst = Cfg_ProfileGetString(pProf,
    //    "[LOCAL2_LANGUAGE_VERSION]", 
    //    szVersion,
    //    sizeof(szVersion)); 
    //stWebVersionInfo_R->fVersion2 = atof(szVersion);

    ////TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
    //if (iRst != 1)
    //{
    //    AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
    //        "There are no pages number in Resource.cfg.");
    //}

    ////3.Read Local2 language version//Add by wj for three languages,2006.4.27
    //iRst = Cfg_ProfileGetString(pProf,
    //    "[LOCAL2_LANGUAGE]", 
    //    stWebVersionInfo_R->szLanguage,
    //    sizeof(stWebVersionInfo_R->szLanguage2)); 
    ////TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
    //if (iRst != 1)
    //{
    //    AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
    //        "There are no pages number in Resource.cfg.");
    //}
	/*if (iRst != ERR_WEB_OK)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
				"Fail to get resource table.");

		DELETE(pProf); 
		DELETE(szInFile);
		return iRst;
	}*/


   	DELETE(pProf); 
	DELETE(szInFile);
	return ERR_WEB_OK;
}


static double Web_GetVersionOfWebPages(void)
{

	FILE	*fp = NULL;
	char	szBuffer[128];
	char	*pSearchValue = NULL;
	char	szVersion[32];
	//TRACE("%s", "GetVersionOfWebPages");

	if((fp = fopen(WEB_PAGES_VERSION_FILE, "r")) != NULL)
	{
		while(!feof(fp))
		{
			fgets(szBuffer, sizeof(szBuffer), fp);
			pSearchValue = strstr(szBuffer, WEB_PAGES_VERSION);
			if(pSearchValue != NULL)
			{
				pSearchValue = strchr(szBuffer,':');
				if(pSearchValue != NULL)
				{
					strncpyz(szVersion,pSearchValue + 1, sizeof(szVersion));
					//TRACE("GetVersionOfWebPages : %s\n", szVersion);
					fclose(fp);
					return atof(szVersion);
				}
				else
				{
					fclose(fp);
					return (double)0;
				}

			}
		}
		fclose(fp);
		return (double)0;
	}
	else
	{
		return (double)0;
	}


}
/*==========================================================================*
 * FUNCTION :  MakeFilePath
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szFileName:
				IN int iLanguage:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static char *MakeFilePath(IN char *szFileName, IN int iLanguage)
{
	char			*szReturnFile = NULL;

	ASSERT(szFileName);
	
	szReturnFile = NEW(char, 128);
	if(szReturnFile == NULL)
	{
		return NULL;
	}

	if(iLanguage ==0)			//Eng
	{
		
		sprintf(szReturnFile,"%s/%s", WEB_DEFAULT_DIR, szFileName);
	}
	else if(iLanguage == 1)		//Loc
	{
		sprintf(szReturnFile,"%s/%s", WEB_LOCAL_DIR, szFileName);
	}
    else if(iLanguage == 2)		//Loc2
    {
        sprintf(szReturnFile,"%s/%s", WEB_LOCAL2_DIR, szFileName);
    }
	else						//Template
	{
		sprintf(szReturnFile,"%s/%s", WEB_TEMPLATE_DIR, szFileName);
	}
	return szReturnFile;
}

/*==========================================================================*
 * FUNCTION :  WEB_ReadConfig
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static char *CreateLangResourceFileName(const char *szFileName, const char *szLangCode)
{
	static char szLRFileName[256]; /* the returned lang resource file name */
	const char *p;   
	size_t iLen;

	if (szFileName == NULL || szLangCode == NULL)
	{
		return NULL;
	}

	/* init */
	szLRFileName[0] = '\0';

	/* get the head: config/lang/xx/ */
	strcpy(szLRFileName, "lang/");
	strcat(szLRFileName, szLangCode);
	strcat(szLRFileName, "/");

	/* get the name: stdxx_xx.res */
	p = szFileName;
	iLen = 0;
	while (*p != '\0' && *p != '.')
	{
		p++;
		iLen++;
	}

	if (*p == '\0')				/* lack of '.', invalid format */
	{
		return NULL;
	}

	strncat(szLRFileName, szFileName, iLen);
	strcat(szLRFileName, "_");
	strcat(szLRFileName, szLangCode);
	strcat(szLRFileName, ".res");

	return szLRFileName;
}

static int WEB_ReadConfig(void)
{
	int		iRst;
	void	*pProf = NULL;  
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;
	char		*pLangCode = NULL;

	DxiGetData(VAR_ACU_PUBLIC_CONFIG,
				LOCAL_LANGUAGE_CODE, 
				0, 
				&iBufLen,
				&(pLangCode),
				0);

	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CreateLangResourceFileName(CONFIG_FILE_WEB_PRIVATE, pLangCode),//CONFIG_FILE_WEB_PRIVATE, 
							szCfgFileName, 
							MAX_FILE_PATH);
	//TRACE("szCfgFileName[%s]\n",szCfgFileName);
	/*Debug*/	
	//sprintf(szCfgFileName,"%s","/scup/config/private/web/Web_Resource.res");//"/home/ygx/cfg/Source.cfg");
	/*End Debug*/
	TRACE("szCfgFileName : %s\n", szCfgFileName);
	/* open file */
	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{	
        TRACE("Failure to open %s",szCfgFileName);
		return FALSE;
	}

	TRACE("Successfully to Open szCfgFileName : %s\n", szCfgFileName);

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return FALSE;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);

		return FALSE;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);
	
	if (pProf == NULL)
	{
		DELETE(szInFile);
		return FALSE;
	}
	//1.Read Number of pages
	iRst = Cfg_ProfileGetInt(pProf,
							WEB_PAGES_NUMBER, 
							&iPagesNumber); 
	TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
	if (iRst != 1)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
				"There are no pages number in Resource.cfg.");
	}

	//pWebHeadInfo = NEW(WEB_HEAD_RESOURCE_INFO, 1);
	//2. Get local language
	char		szVersion[32];

	iRst = Cfg_ProfileGetString(pProf,
							WEB_LOCAL_LANGUAGE_VERSION, 
							szVersion, 
							sizeof(szVersion)); 

	stWebCompareVersion.fVersion = atof(szVersion);
	//3. Get local language version
	iRst = Cfg_ProfileGetString(pProf,
							WEB_LOCAL_LANGUAGE, 
							stWebCompareVersion.szLanguage,
							sizeof(stWebCompareVersion.szLanguage)); 
	

	//2.Read current all command content and command numbers
    iRst = LoadWebPrivateConfigProc(iPagesNumber, pProf);
	
	TRACE("Successfully to  LoadWebPrivateConfigProc  \n");

	//printf("%d%s\n", pWebHeadInfo->iNumber, pWebHeadInfo->stWebHeadInfo->CfgName);

	if (iRst != ERR_WEB_OK)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
				"Fail to get resource table.");

		DELETE(pProf); 
		DELETE(szInFile);
		return iRst;
	}


   	DELETE(pProf); 
	DELETE(szInFile);
	return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :  WEB_ReadConfigLoc2
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   :  
* COMMENTS : for three languages
* CREATOR  : Wang Jing              DATE: 2006-04-27 09:30
*==========================================================================*/
static int WEB_ReadConfigLoc2(void)
{
    int		iRst;
    void	*pProf = NULL;  
    FILE	*pFile = NULL;
    char	*szInFile = NULL;
    size_t	ulFileLen;
    char	*pLangCode = NULL;

    DxiGetData(VAR_ACU_PUBLIC_CONFIG,
        LOCAL2_LANGUAGE_CODE, 
        0, 
        &iBufLen,
        &(pLangCode),
        0);

    char szCfgFileName[MAX_FILE_PATH]; 

    Cfg_GetFullConfigPath(CreateLangResourceFileName(CONFIG_FILE_WEB_PRIVATE, pLangCode),//CONFIG_FILE_WEB_PRIVATE, 
        szCfgFileName, 
        MAX_FILE_PATH);
    //TRACE("szCfgFileName[%s]\n",szCfgFileName);
    /*Debug*/	
    //sprintf(szCfgFileName,"%s","/scup/config/private/web/Web_Resource.res");//"/home/ygx/cfg/Source.cfg");
    /*End Debug*/
    TRACE("szCfgFileName : %s\n", szCfgFileName);
    /* open file */
    pFile = fopen(szCfgFileName, "r");
    if (pFile == NULL)
    {		
        TRACE("Failure to  open  %s \n",szCfgFileName);
        return FALSE;
    }

    TRACE("Successfully to read szCfgFileName : %s\n", szCfgFileName);

    ulFileLen = GetFileLength(pFile);

    szInFile = NEW(char, ulFileLen + 1);
    if (szInFile == NULL)
    {
        fclose(pFile);
        return FALSE;
    }

    /* read file */
    ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
    fclose(pFile);

    if (ulFileLen == 0) 
    {
        /* clear the memory */
        DELETE(szInFile);

        return FALSE;
    }
    szInFile[ulFileLen] = '\0';  /* end with NULL */

    /* create SProfile */
    pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

    if (pProf == NULL)
    {
        DELETE(szInFile);
        TRACE("Failure to  open  pProf \n");

        return FALSE;
    }

    //1.Read Number of pages
    iRst = Cfg_ProfileGetInt(pProf,
        WEB_PAGES_NUMBER, 
        &iPagesNumber); 

    TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);

    if (iRst != 1)
    {
        AppLogOut("WEB_READ_CFG2", APP_LOG_ERROR, 
            "There are no pages number in Resource.cfg.");
    }

    //pWebHeadInfo = NEW(WEB_HEAD_RESOURCE_INFO, 1);
    //2. Get local language
    char		szVersion[32];

    iRst = Cfg_ProfileGetString(pProf,
        WEB_LOCAL2_LANGUAGE_VERSION, 
        szVersion, 
        sizeof(szVersion)); 
    
    stWebCompareVersion.fVersion2 = atof(szVersion);
    TRACE("%f\n",stWebCompareVersion.fVersion2);

    //3. Get local language version
    iRst = Cfg_ProfileGetString(pProf,
        WEB_LOCAL2_LANGUAGE, 
        stWebCompareVersion.szLanguage2,
        sizeof(stWebCompareVersion.szLanguage2)); 

    TRACE("%s\n",stWebCompareVersion.szLanguage2);

    //2.Read current all command content and command numbers
    TRACE("Begin to  LoadWebPrivateConfigProc2  \n");

    iRst = LoadWebPrivateConfigProc2(iPagesNumber, pProf);//add by wj for three language 2006.4.27

    TRACE("Successfully to  LoadWebPrivateConfigProc2  \n");

    //printf("%d%s\n", pWebHeadInfo->iNumber, pWebHeadInfo->stWebHeadInfo->CfgName);

    if (iRst != ERR_WEB_OK)
    {
        AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
            "Fail to get resource table.");

        DELETE(pProf); 
        DELETE(szInFile);
        return iRst;
    }


    DELETE(pProf); 
    DELETE(szInFile);
    return ERR_WEB_OK;
}

/*==========================================================================*
 * FUNCTION :    LoadWebPrivateConfigProc
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN int iPages:
				IN void *pCfg:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int LoadWebPrivateConfigProc_r(IN int iPages, IN void *pCfg)
{
	UNUSED(iPages);
	CONFIG_TABLE_LOADER loader[1];

	pWebHeadInfo = NEW(WEB_HEAD_RESOURCE_INFO, 1);
	ASSERT(pWebHeadInfo);

	DEF_LOADER_ITEM(loader ,
		WEB_NUMBER_OF_CONFIG_INFORMATION,
		&(pWebHeadInfo->iNumber), 
		WEB_CONFIG_INFORMATION, 
		&(pWebHeadInfo->stWebHeadInfo), 
		ParseWebResourceInfo);

	if (Cfg_LoadTables(pCfg, 1 + WEB_MAX_HTML_PAGE_NUM, loader) != ERR_CFG_OK)
	{
		return ERR_LCD_FAILURE;
	}

	return ERR_CFG_OK;

}
static int LoadWebPrivateConfigProc(IN int iPages, IN void *pCfg)
{
	iPages = 82;
	int			i = 0, k = 0;
	//char		*szNumber = NULL, *szFile = NULL;
	CONFIG_TABLE_LOADER loader[WEB_MAX_HTML_PAGE_NUM];
	static WEB_PRIVATE_RESOURCE_INFO	s_aWebCfgInfo[WEB_MAX_HTML_PAGE_NUM];
	//pWebHeadInfo = NEW(WEB_HEAD_RESOURCE_INFO, 1);
	//WEB_HEAD_RESOURCE_INFO	pWebHeadInfo1;// = NEW(WEB_HEAD_RESOURCE_INFO, 1);
	
	//pWebCfgInfo	= NEW(WEB_PRIVATE_RESOURCE_INFO, WEB_MAX_HTML_PAGE_NUM);
	//ASSERT(pWebCfgInfo);
	pWebCfgInfo = s_aWebCfgInfo;

	

	
	k = 1;
	for(i = 1; i < WEB_MAX_HTML_PAGE_NUM + 1; i++)
	{
		strncpyz(pWebCfgInfo[i-1].szFileName,szTemplateFile[k] , sizeof(pWebCfgInfo[i-1].szFileName));
		k = k + 2;
	}

	DEF_LOADER_ITEM(&loader[0],
			WEB_PAGES_CONTROL_NUM,
			&(pWebCfgInfo[0].iNumber), 
			WEB_PAGES_CONTROL,
			&(pWebCfgInfo[0].stWebPrivate), 
			ParsedWebLangFileTableProc);
	
	DEF_LOADER_ITEM(&loader[1],
			WEB_PAGES_DATASAMPLE_NUM,
			&(pWebCfgInfo[1].iNumber), 
			WEB_PAGES_DATASAMPLE,
			&(pWebCfgInfo[1].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[2],
			WEB_PAGES_DIALOG_NUM,
			&(pWebCfgInfo[2].iNumber), 
			WEB_PAGES_DIALOG,
			&(pWebCfgInfo[2].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[3],
			WEB_PAGES_EDITSIGNAL_NUM,
			&(pWebCfgInfo[3].iNumber), 
			WEB_PAGES_EDITSIGNAL,
			&(pWebCfgInfo[3].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[4],
			WEB_PAGES_EQUIPDATA_NUM,
			&(pWebCfgInfo[4].iNumber), 
			WEB_PAGES_EQUIPDATA,
			&(pWebCfgInfo[4].stWebPrivate), 
			ParsedWebLangFileTableProc);
		
	DEF_LOADER_ITEM(&loader[5],
			WEB_PAGES_J01_NUM,
			&(pWebCfgInfo[5].iNumber), 
			WEB_PAGES_JO1,
			&(pWebCfgInfo[5].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[6],
			WEB_PAGES_JO2_NUM,
			&(pWebCfgInfo[6].iNumber), 
			WEB_PAGES_JO2,
			&(pWebCfgInfo[6].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[7],
			WEB_PAGES_JO4_NUM,
			&(pWebCfgInfo[7].iNumber), 
			WEB_PAGES_JO4,
			&(pWebCfgInfo[7].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[8],
			WEB_PAGES_JO5_NUM,
			&(pWebCfgInfo[8].iNumber), 
			WEB_PAGES_JO5,
			&(pWebCfgInfo[8].stWebPrivate), 
			ParsedWebLangFileTableProc);
	
	DEF_LOADER_ITEM(&loader[9],
			WEB_PAGES_JO7_NUM,
			&(pWebCfgInfo[9].iNumber), 
			WEB_PAGES_JO7,
			&(pWebCfgInfo[9].stWebPrivate), 
			ParsedWebLangFileTableProc);
	
	DEF_LOADER_ITEM(&loader[10],
			WEB_PAGES_J30_NUM,
			&(pWebCfgInfo[10].iNumber), 
			WEB_PAGES_J30,
			&(pWebCfgInfo[10].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[11],
			WEB_PAGES_J31_MENU_NUM,
			&(pWebCfgInfo[11].iNumber), 
			WEB_PAGES_J31_MENU,
			&(pWebCfgInfo[11].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[12],
			WEB_PAGES_J31_TABLE_NUM,
			&(pWebCfgInfo[12].iNumber), 
			WEB_PAGES_J31_TABLE,
			&(pWebCfgInfo[12].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[13],
			WEB_PAGES_J50_NUM,
			&(pWebCfgInfo[13].iNumber), 
			WEB_PAGES_J50,
			&(pWebCfgInfo[13].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[14],
			WEB_PAGES_J76_NUM,
			&(pWebCfgInfo[14].iNumber), 
			WEB_PAGES_J76,
			&(pWebCfgInfo[14].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[15],
			WEB_PAGES_J77_NUM,
			&(pWebCfgInfo[15].iNumber), 
			WEB_PAGES_J77,
			&(pWebCfgInfo[15].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[16],
			WEB_PAGES_LOGIN_NUM,
			&(pWebCfgInfo[16].iNumber), 
			WEB_PAGES_LOGIN,
			&(pWebCfgInfo[16].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[17],
			WEB_PAGES_P01_NUM,
			&(pWebCfgInfo[17].iNumber), 
			WEB_PAGES_P01,
			&(pWebCfgInfo[17].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[18],
			WEB_PAGES_P02_NUM,
			&(pWebCfgInfo[18].iNumber), 
			WEB_PAGES_PO2,
			&(pWebCfgInfo[18].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[19],
			WEB_PAGES_PO3_NUM,
			&(pWebCfgInfo[19].iNumber), 
			WEB_PAGES_PO3,
			&(pWebCfgInfo[19].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[20],
			WEB_PAGES_PO4_NUM,
			&(pWebCfgInfo[20].iNumber), 
			WEB_PAGES_P04,
			&(pWebCfgInfo[20].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[21],
			WEB_PAGES_P05_NUM,
			&(pWebCfgInfo[21].iNumber), 
			WEB_PAGES_P05,
			&(pWebCfgInfo[21].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[22],
			WEB_PAGES_P06_NUM,
			&(pWebCfgInfo[22].iNumber), 
			WEB_PAGES_P06,
			&(pWebCfgInfo[22].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[23],
			WEB_PAGES_P07_NUM,
			&(pWebCfgInfo[23].iNumber), 
			WEB_PAGES_P07,
			&(pWebCfgInfo[23].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[24],
			WEB_PAGES_P08_NUM,
			&(pWebCfgInfo[24].iNumber), 
			WEB_PAGES_P08,
			&(pWebCfgInfo[24].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[25],
			WEB_PAGES_P09_NUM,
			&(pWebCfgInfo[25].iNumber), 
			WEB_PAGES_P09,
			&(pWebCfgInfo[25].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[26],
			WEB_PAGES_P10_NUM,
			&(pWebCfgInfo[26].iNumber), 
			WEB_PAGES_P10,
			&(pWebCfgInfo[26].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[27],
			WEB_PAGES_P11_NUM,
			&(pWebCfgInfo[27].iNumber), 
			WEB_PAGES_P11,
			&(pWebCfgInfo[27].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[28],
			WEB_PAGES_P12_NUM,
			&(pWebCfgInfo[28].iNumber), 
			WEB_PAGES_P12,
			&(pWebCfgInfo[28].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[29],
			WEB_PAGES_P13_NUM,
			&(pWebCfgInfo[29].iNumber), 
			WEB_PAGES_P13,
			&(pWebCfgInfo[29].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[30],
			WEB_PAGES_P14_NUM,
			&(pWebCfgInfo[30].iNumber), 
			WEB_PAGES_P14,
			&(pWebCfgInfo[30].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[31],
			WEB_PAGES_P15_NUM,
			&(pWebCfgInfo[31].iNumber), 
			WEB_PAGES_P15,
			&(pWebCfgInfo[31].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[32],
			WEB_PAGES_P16_NUM,
			&(pWebCfgInfo[32].iNumber), 
			WEB_PAGES_P16,
			&(pWebCfgInfo[32].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[33],
			WEB_PAGES_P17_NUM,
			&(pWebCfgInfo[33].iNumber), 
			WEB_PAGES_P17,
			&(pWebCfgInfo[33].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[34],
			WEB_PAGES_P18_NUM,
			&(pWebCfgInfo[34].iNumber), 
			WEB_PAGES_P18,
			&(pWebCfgInfo[34].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[35],
			WEB_PAGES_P19_NUM,
			&(pWebCfgInfo[35].iNumber), 
			WEB_PAGES_P19,
			&(pWebCfgInfo[35].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[36],
			WEB_PAGES_P20_NUM,
			&(pWebCfgInfo[36].iNumber), 
			WEB_PAGES_P20,
			&(pWebCfgInfo[36].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[37],
			WEB_PAGES_P21_NUM,
			&(pWebCfgInfo[37].iNumber), 
			WEB_PAGES_P21,
			&(pWebCfgInfo[37].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[38],
			WEB_PAGES_P22_NUM,
			&(pWebCfgInfo[38].iNumber), 
			WEB_PAGES_P22,
			&(pWebCfgInfo[38].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[39],
			WEB_PAGES_P23_NUM,
			&(pWebCfgInfo[39].iNumber), 
			WEB_PAGES_P23,
			&(pWebCfgInfo[39].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[40],
			WEB_PAGES_P24_NUM,
			&(pWebCfgInfo[40].iNumber), 
			WEB_PAGES_P24,
			&(pWebCfgInfo[40].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[41],
			WEB_PAGES_P25_NUM,
			&(pWebCfgInfo[41].iNumber), 
			WEB_PAGES_P25,
			&(pWebCfgInfo[41].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[42],
			WEB_PAGES_P26_NUM,
			&(pWebCfgInfo[42].iNumber), 
			WEB_PAGES_P26,
			&(pWebCfgInfo[42].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[43],
			WEB_PAGES_P27_NUM,
			&(pWebCfgInfo[43].iNumber), 
			WEB_PAGES_P27,
			&(pWebCfgInfo[43].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[44],
			WEB_PAGES_P28_NUM,
			&(pWebCfgInfo[44].iNumber), 
			WEB_PAGES_P28,
			&(pWebCfgInfo[44].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[45],
			WEB_PAGES_P29_NUM,
			&(pWebCfgInfo[45].iNumber), 
			WEB_PAGES_P29,
			&(pWebCfgInfo[45].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[46],
			WEB_PAGES_P30_NUM,
			&(pWebCfgInfo[46].iNumber), 
			WEB_PAGES_P30,
			&(pWebCfgInfo[46].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[47],
			WEB_PAGES_P31_NUM,
			&(pWebCfgInfo[47].iNumber), 
			WEB_PAGES_P31,
			&(pWebCfgInfo[47].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[48],
			WEB_PAGES_P32_NUM,
			&(pWebCfgInfo[48].iNumber), 
			WEB_PAGES_P32,
			&(pWebCfgInfo[48].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[49],
			WEB_PAGES_P33_NUM,
			&(pWebCfgInfo[49].iNumber), 
			WEB_PAGES_P33,
			&(pWebCfgInfo[49].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[50],
			WEB_PAGES_P34_NUM,
			&(pWebCfgInfo[50].iNumber), 
			WEB_PAGES_P34,
			&(pWebCfgInfo[50].stWebPrivate), 
			ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[51],
			WEB_PAGES_P47_NUM,
			&(pWebCfgInfo[51].iNumber), 
			WEB_PAGES_P47,
			&(pWebCfgInfo[51].stWebPrivate), 
			ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[52],
			WEB_PAGES_P79_NUM,
			&(pWebCfgInfo[52].iNumber), 
			WEB_PAGES_P79,
			&(pWebCfgInfo[52].stWebPrivate), 
			ParsedWebLangFileTableProc);
	
	DEF_LOADER_ITEM(&loader[53],
			WEB_PAGES_P80_NUM,
			&(pWebCfgInfo[53].iNumber), 
			WEB_PAGES_P80,
			&(pWebCfgInfo[53].stWebPrivate), 
			ParsedWebLangFileTableProc);
	
	DEF_LOADER_ITEM(&loader[54],
			WEB_PAGES_ALAI_NUM,
			&(pWebCfgInfo[54].iNumber), 
			WEB_PAGES_ALAI,
			&(pWebCfgInfo[54].stWebPrivate), 
			ParsedWebLangFileTableProc);
			
	DEF_LOADER_ITEM(&loader[55],
			WEB_PAGES_J09_NUM,
			&(pWebCfgInfo[55].iNumber), 
			WEB_PAGES_J09,
			&(pWebCfgInfo[55].stWebPrivate), 
			ParsedWebLangFileTableProc);
				
	DEF_LOADER_ITEM(&loader[56],
			WEB_PAGES_P35_NUM,
			&(pWebCfgInfo[56].iNumber), 
			WEB_PAGES_P35,
			&(pWebCfgInfo[56].stWebPrivate), 
			ParsedWebLangFileTableProc);

    //////////////////////////////////////////////////////////////////////////
    //Added by wj for three languages 2006.5.13

    DEF_LOADER_ITEM(&loader[57],
        WEB_PAGES_P37_NUM,
        &(pWebCfgInfo[57].iNumber), 
        WEB_PAGES_P37,
        &(pWebCfgInfo[57].stWebPrivate), 
        ParsedWebLangFileTableProc);

    DEF_LOADER_ITEM(&loader[58],
        WEB_PAGES_P38_NUM,
        &(pWebCfgInfo[58].iNumber), 
        WEB_PAGES_P38,
        &(pWebCfgInfo[58].stWebPrivate), 
        ParsedWebLangFileTableProc);

    DEF_LOADER_ITEM(&loader[59],
        WEB_PAGES_P39_NUM,
        &(pWebCfgInfo[59].iNumber), 
        WEB_PAGES_P39,
        &(pWebCfgInfo[59].stWebPrivate), 
        ParsedWebLangFileTableProc);

    DEF_LOADER_ITEM(&loader[60],
        WEB_PAGES_P40_NUM,
        &(pWebCfgInfo[60].iNumber), 
        WEB_PAGES_P40,
        &(pWebCfgInfo[60].stWebPrivate), 
        ParsedWebLangFileTableProc);

    DEF_LOADER_ITEM(&loader[61],
        WEB_PAGES_P42_NUM,
        &(pWebCfgInfo[61].iNumber), 
        WEB_PAGES_P42,
        &(pWebCfgInfo[61].stWebPrivate), 
        ParsedWebLangFileTableProc);

    DEF_LOADER_ITEM(&loader[62],
        WEB_PAGES_P41_NUM,
        &(pWebCfgInfo[62].iNumber), 
        WEB_PAGES_P41,
        &(pWebCfgInfo[62].stWebPrivate), 
        ParsedWebLangFileTableProc);

    DEF_LOADER_ITEM(&loader[63],
        WEB_PAGES_P43_NUM,
        &(pWebCfgInfo[63].iNumber), 
        WEB_PAGES_P43,
        &(pWebCfgInfo[63].stWebPrivate), 
        ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[64],
		WEB_PAGES_P44_NUM,
		&(pWebCfgInfo[64].iNumber), 
		WEB_PAGES_P44,
		&(pWebCfgInfo[64].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[65],
		WEB_PAGES_P45_NUM,
		&(pWebCfgInfo[65].iNumber), 
		WEB_PAGES_P45,
		&(pWebCfgInfo[65].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[66],
		WEB_PAGES_P46_NUM,
		&(pWebCfgInfo[66].iNumber), 
		WEB_PAGES_P46,
		&(pWebCfgInfo[66].stWebPrivate), 
		ParsedWebLangFileTableProc);


	DEF_LOADER_ITEM(&loader[67],
		WEB_PAGES_P48_NUM,
		&(pWebCfgInfo[67].iNumber), 
		WEB_PAGES_P48,
		&(pWebCfgInfo[67].stWebPrivate), 
		ParsedWebLangFileTableProc);




	DEF_LOADER_ITEM(&loader[68],
		WEB_PAGES_P49_NUM,
		&(pWebCfgInfo[68].iNumber), 
		WEB_PAGES_P49,
		&(pWebCfgInfo[68].stWebPrivate), 
		ParsedWebLangFileTableProc);


	DEF_LOADER_ITEM(&loader[69],
		WEB_PAGES_P50_NUM,
		&(pWebCfgInfo[69].iNumber), 
		WEB_PAGES_P50,
		&(pWebCfgInfo[69].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[70],
		WEB_PAGES_P51_NUM,
		&(pWebCfgInfo[70].iNumber), 
		WEB_PAGES_P51,
		&(pWebCfgInfo[70].stWebPrivate), 
		ParsedWebLangFileTableProc);


	DEF_LOADER_ITEM(&loader[71],
		WEB_PAGES_P52_NUM,
		&(pWebCfgInfo[71].iNumber), 
		WEB_PAGES_P52,
		&(pWebCfgInfo[71].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[72],
		WEB_PAGES_P53_NUM,
		&(pWebCfgInfo[72].iNumber), 
		WEB_PAGES_P53,
		&(pWebCfgInfo[72].stWebPrivate), 
		ParsedWebLangFileTableProc);


	DEF_LOADER_ITEM(&loader[73],
		WEB_PAGES_P54_NUM,
		&(pWebCfgInfo[73].iNumber), 
		WEB_PAGES_P54,
		&(pWebCfgInfo[73].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[74],
		WEB_PAGES_P55_NUM,
		&(pWebCfgInfo[74].iNumber), 
		WEB_PAGES_P55,
		&(pWebCfgInfo[74].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[75],
		WEB_PAGES_P56_NUM,
		&(pWebCfgInfo[75].iNumber), 
		WEB_PAGES_P56,
		&(pWebCfgInfo[75].stWebPrivate), 
		ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[76],
		WEB_PAGES_P57_NUM,
		&(pWebCfgInfo[76].iNumber), 
		WEB_PAGES_P57,
		&(pWebCfgInfo[76].stWebPrivate), 
		ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[77],
		WEB_PAGES_P58_NUM,
		&(pWebCfgInfo[77].iNumber), 
		WEB_PAGES_P58,
		&(pWebCfgInfo[77].stWebPrivate), 
		ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[78],
		WEB_PAGES_P59_NUM,
		&(pWebCfgInfo[78].iNumber), 
		WEB_PAGES_P59,
		&(pWebCfgInfo[78].stWebPrivate), 
		ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[79],
		WEB_PAGES_P60_NUM,
		&(pWebCfgInfo[79].iNumber), 
		WEB_PAGES_P60,
		&(pWebCfgInfo[79].stWebPrivate), 
		ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[80],
		WEB_PAGES_P61_NUM,
		&(pWebCfgInfo[80].iNumber), 
		WEB_PAGES_P61,
		&(pWebCfgInfo[80].stWebPrivate), 
		ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[81],
		WEB_PAGES_P62_NUM,
		&(pWebCfgInfo[81].iNumber), 
		WEB_PAGES_P62,
		&(pWebCfgInfo[81].stWebPrivate), 
		ParsedWebLangFileTableProc);
	DEF_LOADER_ITEM(&loader[82],
		WEB_PAGES_P12_V3_NUM,
		&(pWebCfgInfo[82].iNumber), 
		WEB_PAGES_P12_V3,
		&(pWebCfgInfo[82].stWebPrivate), 
		ParsedWebLangFileTableProc);
    //end////////////////////////////////////////////////////////////////////////
    
					



	if (Cfg_LoadTables(pCfg, WEB_MAX_HTML_PAGE_NUM, loader) != ERR_CFG_OK)
	{
		//TRACE("Fail to Cfg_LoadTables\n");
		return ERR_LCD_FAILURE;
	}
	
	
	
	return ERR_CFG_OK;
}


//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.4.28
static int LoadWebPrivateConfigProc2(IN int iPages, IN void *pCfg)
{
    iPages = 64;
    int			i = 0, k = 0;
    //char		*szNumber = NULL, *szFile = NULL;
    CONFIG_TABLE_LOADER loader[WEB_MAX_HTML_PAGE_NUM];
    //pWebHeadInfo = NEW(WEB_HEAD_RESOURCE_INFO, 1);
    //WEB_HEAD_RESOURCE_INFO	pWebHeadInfo1;// = NEW(WEB_HEAD_RESOURCE_INFO, 1);

    pWebCfgInfoTemp = NEW(WEB_PRIVATE_RESOURCE_INFO, WEB_MAX_HTML_PAGE_NUM);
    //ASSERT(pWebHeadInfo1);
    ASSERT(pWebCfgInfoTemp);


    /* load configuration information*/
    /*DEF_LOADER_ITEM(&loader[0] ,
    WEB_NUMBER_OF_CONFIG_INFORMATION,
    &(pWebHeadInfo1.iNumber), 
    WEB_CONFIG_INFORMATION, 
    &(pWebHeadInfo1.stWebHeadInfo), 
    ParseWebResourceInfo);*/


    ///* load Web language file table */
    //////TRACE("2-----------LoadWebPrivateConfigProc...\n");
    ////for(i = 1; i < iPages + 1; i++)
    ////{
    //	strncpyz(pWebCfgInfo[i-1].szFileName,szTemplateFile[k +1] , sizeof(pWebCfgInfo[i-1].szFileName));
    //	szNumber = MakePrivateVarField(szTemplateFile[k++]);
    //	szFile	 = MakePrivateVarField(szTemplateFile[k++]);
    //	//TRACE("%d szNumber:%s\n szFile:%s\n",i, szNumber, szFile);
    //	DEF_LOADER_ITEM(&loader[i],
    //		"[p20_history_frame.htm:Number]",//szNumber,
    //		&(pWebCfgInfo[i-1].iNumber), 
    //		"[p20_history_frame.htm]",//szFile,
    //		&(pWebCfgInfo[i-1].stWebPrivate), 
    //		ParsedWebLangFileTableProc);

    ////	DELETE(szNumber);
    ////	szNumber = NULL;
    ////	DELETE(szFile);
    ////	szFile = NULL;
    ////}
    k = 1;
    for(i = 1; i < WEB_MAX_HTML_PAGE_NUM + 1; i++)
    {
        strncpyz(pWebCfgInfoTemp[i-1].szFileName,szTemplateFile[k] , sizeof(pWebCfgInfoTemp[i-1].szFileName));
        k = k + 2;
    }

    DEF_LOADER_ITEM(&loader[0],
        WEB_PAGES_CONTROL_NUM,
        &(pWebCfgInfoTemp[0].iNumber), 
        WEB_PAGES_CONTROL,
        &(pWebCfgInfoTemp[0].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[1],
        WEB_PAGES_DATASAMPLE_NUM,
        &(pWebCfgInfoTemp[1].iNumber), 
        WEB_PAGES_DATASAMPLE,
        &(pWebCfgInfoTemp[1].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[2],
        WEB_PAGES_DIALOG_NUM,
        &(pWebCfgInfoTemp[2].iNumber), 
        WEB_PAGES_DIALOG,
        &(pWebCfgInfoTemp[2].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[3],
        WEB_PAGES_EDITSIGNAL_NUM,
        &(pWebCfgInfoTemp[3].iNumber), 
        WEB_PAGES_EDITSIGNAL,
        &(pWebCfgInfoTemp[3].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[4],
        WEB_PAGES_EQUIPDATA_NUM,
        &(pWebCfgInfoTemp[4].iNumber), 
        WEB_PAGES_EQUIPDATA,
        &(pWebCfgInfoTemp[4].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[5],
        WEB_PAGES_J01_NUM,
        &(pWebCfgInfoTemp[5].iNumber), 
        WEB_PAGES_JO1,
        &(pWebCfgInfoTemp[5].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[6],
        WEB_PAGES_JO2_NUM,
        &(pWebCfgInfoTemp[6].iNumber), 
        WEB_PAGES_JO2,
        &(pWebCfgInfoTemp[6].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[7],
        WEB_PAGES_JO4_NUM,
        &(pWebCfgInfoTemp[7].iNumber), 
        WEB_PAGES_JO4,
        &(pWebCfgInfoTemp[7].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[8],
        WEB_PAGES_JO5_NUM,
        &(pWebCfgInfoTemp[8].iNumber), 
        WEB_PAGES_JO5,
        &(pWebCfgInfoTemp[8].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[9],
        WEB_PAGES_JO7_NUM,
        &(pWebCfgInfoTemp[9].iNumber), 
        WEB_PAGES_JO7,
        &(pWebCfgInfoTemp[9].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[10],
        WEB_PAGES_J30_NUM,
        &(pWebCfgInfoTemp[10].iNumber), 
        WEB_PAGES_J30,
        &(pWebCfgInfoTemp[10].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[11],
        WEB_PAGES_J31_MENU_NUM,
        &(pWebCfgInfoTemp[11].iNumber), 
        WEB_PAGES_J31_MENU,
        &(pWebCfgInfoTemp[11].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[12],
        WEB_PAGES_J31_TABLE_NUM,
        &(pWebCfgInfoTemp[12].iNumber), 
        WEB_PAGES_J31_TABLE,
        &(pWebCfgInfoTemp[12].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[13],
        WEB_PAGES_J50_NUM,
        &(pWebCfgInfoTemp[13].iNumber), 
        WEB_PAGES_J50,
        &(pWebCfgInfoTemp[13].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[14],
        WEB_PAGES_J76_NUM,
        &(pWebCfgInfoTemp[14].iNumber), 
        WEB_PAGES_J76,
        &(pWebCfgInfoTemp[14].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[15],
        WEB_PAGES_J77_NUM,
        &(pWebCfgInfoTemp[15].iNumber), 
        WEB_PAGES_J77,
        &(pWebCfgInfoTemp[15].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[16],
        WEB_PAGES_LOGIN_NUM,
        &(pWebCfgInfoTemp[16].iNumber), 
        WEB_PAGES_LOGIN,
        &(pWebCfgInfoTemp[16].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[17],
        WEB_PAGES_P01_NUM,
        &(pWebCfgInfoTemp[17].iNumber), 
        WEB_PAGES_P01,
        &(pWebCfgInfoTemp[17].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[18],
        WEB_PAGES_P02_NUM,
        &(pWebCfgInfoTemp[18].iNumber), 
        WEB_PAGES_PO2,
        &(pWebCfgInfoTemp[18].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[19],
        WEB_PAGES_PO3_NUM,
        &(pWebCfgInfoTemp[19].iNumber), 
        WEB_PAGES_PO3,
        &(pWebCfgInfoTemp[19].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[20],
        WEB_PAGES_PO4_NUM,
        &(pWebCfgInfoTemp[20].iNumber), 
        WEB_PAGES_P04,
        &(pWebCfgInfoTemp[20].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[21],
        WEB_PAGES_P05_NUM,
        &(pWebCfgInfoTemp[21].iNumber), 
        WEB_PAGES_P05,
        &(pWebCfgInfoTemp[21].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[22],
        WEB_PAGES_P06_NUM,
        &(pWebCfgInfoTemp[22].iNumber), 
        WEB_PAGES_P06,
        &(pWebCfgInfoTemp[22].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[23],
        WEB_PAGES_P07_NUM,
        &(pWebCfgInfoTemp[23].iNumber), 
        WEB_PAGES_P07,
        &(pWebCfgInfoTemp[23].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[24],
        WEB_PAGES_P08_NUM,
        &(pWebCfgInfoTemp[24].iNumber), 
        WEB_PAGES_P08,
        &(pWebCfgInfoTemp[24].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[25],
        WEB_PAGES_P09_NUM,
        &(pWebCfgInfoTemp[25].iNumber), 
        WEB_PAGES_P09,
        &(pWebCfgInfoTemp[25].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[26],
        WEB_PAGES_P10_NUM,
        &(pWebCfgInfoTemp[26].iNumber), 
        WEB_PAGES_P10,
        &(pWebCfgInfoTemp[26].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[27],
        WEB_PAGES_P11_NUM,
        &(pWebCfgInfoTemp[27].iNumber), 
        WEB_PAGES_P11,
        &(pWebCfgInfoTemp[27].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[28],
        WEB_PAGES_P12_NUM,
        &(pWebCfgInfoTemp[28].iNumber), 
        WEB_PAGES_P12,
        &(pWebCfgInfoTemp[28].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[29],
        WEB_PAGES_P13_NUM,
        &(pWebCfgInfoTemp[29].iNumber), 
        WEB_PAGES_P13,
        &(pWebCfgInfoTemp[29].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[30],
        WEB_PAGES_P14_NUM,
        &(pWebCfgInfoTemp[30].iNumber), 
        WEB_PAGES_P14,
        &(pWebCfgInfoTemp[30].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[31],
        WEB_PAGES_P15_NUM,
        &(pWebCfgInfoTemp[31].iNumber), 
        WEB_PAGES_P15,
        &(pWebCfgInfoTemp[31].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[32],
        WEB_PAGES_P16_NUM,
        &(pWebCfgInfoTemp[32].iNumber), 
        WEB_PAGES_P16,
        &(pWebCfgInfoTemp[32].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[33],
        WEB_PAGES_P17_NUM,
        &(pWebCfgInfoTemp[33].iNumber), 
        WEB_PAGES_P17,
        &(pWebCfgInfoTemp[33].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[34],
        WEB_PAGES_P18_NUM,
        &(pWebCfgInfoTemp[34].iNumber), 
        WEB_PAGES_P18,
        &(pWebCfgInfoTemp[34].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[35],
        WEB_PAGES_P19_NUM,
        &(pWebCfgInfoTemp[35].iNumber), 
        WEB_PAGES_P19,
        &(pWebCfgInfoTemp[35].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[36],
        WEB_PAGES_P20_NUM,
        &(pWebCfgInfoTemp[36].iNumber), 
        WEB_PAGES_P20,
        &(pWebCfgInfoTemp[36].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[37],
        WEB_PAGES_P21_NUM,
        &(pWebCfgInfoTemp[37].iNumber), 
        WEB_PAGES_P21,
        &(pWebCfgInfoTemp[37].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[38],
        WEB_PAGES_P22_NUM,
        &(pWebCfgInfoTemp[38].iNumber), 
        WEB_PAGES_P22,
        &(pWebCfgInfoTemp[38].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[39],
        WEB_PAGES_P23_NUM,
        &(pWebCfgInfoTemp[39].iNumber), 
        WEB_PAGES_P23,
        &(pWebCfgInfoTemp[39].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[40],
        WEB_PAGES_P24_NUM,
        &(pWebCfgInfoTemp[40].iNumber), 
        WEB_PAGES_P24,
        &(pWebCfgInfoTemp[40].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[41],
        WEB_PAGES_P25_NUM,
        &(pWebCfgInfoTemp[41].iNumber), 
        WEB_PAGES_P25,
        &(pWebCfgInfoTemp[41].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[42],
        WEB_PAGES_P26_NUM,
        &(pWebCfgInfoTemp[42].iNumber), 
        WEB_PAGES_P26,
        &(pWebCfgInfoTemp[42].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[43],
        WEB_PAGES_P27_NUM,
        &(pWebCfgInfoTemp[43].iNumber), 
        WEB_PAGES_P27,
        &(pWebCfgInfoTemp[43].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[44],
        WEB_PAGES_P28_NUM,
        &(pWebCfgInfoTemp[44].iNumber), 
        WEB_PAGES_P28,
        &(pWebCfgInfoTemp[44].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[45],
        WEB_PAGES_P29_NUM,
        &(pWebCfgInfoTemp[45].iNumber), 
        WEB_PAGES_P29,
        &(pWebCfgInfoTemp[45].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[46],
        WEB_PAGES_P30_NUM,
        &(pWebCfgInfoTemp[46].iNumber), 
        WEB_PAGES_P30,
        &(pWebCfgInfoTemp[46].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[47],
        WEB_PAGES_P31_NUM,
        &(pWebCfgInfoTemp[47].iNumber), 
        WEB_PAGES_P31,
        &(pWebCfgInfoTemp[47].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[48],
        WEB_PAGES_P32_NUM,
        &(pWebCfgInfoTemp[48].iNumber), 
        WEB_PAGES_P32,
        &(pWebCfgInfoTemp[48].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[49],
        WEB_PAGES_P33_NUM,
        &(pWebCfgInfoTemp[49].iNumber), 
        WEB_PAGES_P33,
        &(pWebCfgInfoTemp[49].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[50],
        WEB_PAGES_P34_NUM,
        &(pWebCfgInfoTemp[50].iNumber), 
        WEB_PAGES_P34,
        &(pWebCfgInfoTemp[50].stWebPrivate), 
        ParsedWebLangFileTableProc2);
    DEF_LOADER_ITEM(&loader[51],
        WEB_PAGES_P47_NUM,
        &(pWebCfgInfoTemp[51].iNumber), 
        WEB_PAGES_P47,
        &(pWebCfgInfoTemp[51].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[52],
        WEB_PAGES_P79_NUM,
        &(pWebCfgInfoTemp[52].iNumber), 
        WEB_PAGES_P79,
        &(pWebCfgInfoTemp[52].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[53],
        WEB_PAGES_P80_NUM,
        &(pWebCfgInfoTemp[53].iNumber), 
        WEB_PAGES_P80,
        &(pWebCfgInfoTemp[53].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[54],
        WEB_PAGES_ALAI_NUM,
        &(pWebCfgInfoTemp[54].iNumber), 
        WEB_PAGES_ALAI,
        &(pWebCfgInfoTemp[54].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[55],
        WEB_PAGES_J09_NUM,
        &(pWebCfgInfoTemp[55].iNumber), 
        WEB_PAGES_J09,
        &(pWebCfgInfoTemp[55].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[56],
        WEB_PAGES_P35_NUM,
        &(pWebCfgInfoTemp[56].iNumber), 
        WEB_PAGES_P35,
        &(pWebCfgInfoTemp[56].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    //////////////////////////////////////////////////////////////////////////
    //Added by wj for three languages 2006.5.13

    DEF_LOADER_ITEM(&loader[57],
        WEB_PAGES_P37_NUM,
        &(pWebCfgInfoTemp[57].iNumber), 
        WEB_PAGES_P37,
        &(pWebCfgInfoTemp[57].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[58],
        WEB_PAGES_P38_NUM,
        &(pWebCfgInfoTemp[58].iNumber), 
        WEB_PAGES_P38,
        &(pWebCfgInfoTemp[58].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[59],
        WEB_PAGES_P39_NUM,
        &(pWebCfgInfoTemp[59].iNumber), 
        WEB_PAGES_P39,
        &(pWebCfgInfoTemp[59].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[60],
        WEB_PAGES_P40_NUM,
        &(pWebCfgInfoTemp[60].iNumber), 
        WEB_PAGES_P40,
        &(pWebCfgInfoTemp[60].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[61],
        WEB_PAGES_P42_NUM,
        &(pWebCfgInfoTemp[61].iNumber), 
        WEB_PAGES_P42,
        &(pWebCfgInfoTemp[61].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[62],
        WEB_PAGES_P41_NUM,
        &(pWebCfgInfoTemp[62].iNumber), 
        WEB_PAGES_P41,
        &(pWebCfgInfoTemp[62].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    DEF_LOADER_ITEM(&loader[63],
        WEB_PAGES_P43_NUM,
        &(pWebCfgInfoTemp[63].iNumber), 
        WEB_PAGES_P43,
        &(pWebCfgInfoTemp[63].stWebPrivate), 
        ParsedWebLangFileTableProc2);

    //end////////////////////////////////////////////////////////////////////////




    if (Cfg_LoadTables(pCfg, WEB_MAX_HTML_PAGE_NUM, loader) != ERR_CFG_OK)
    {
        //TRACE("Fail to Cfg_LoadTables\n");
        return ERR_LCD_FAILURE;
    }

     TRACE("LoadWebPrivateConfigProc2 begin!!!");

//////////////////////////////////////////////////////////////////////////
     /*add by wj for delete memory 2006.4.27*/
     int itemp=0;
     int itemp2=0;

     WEB_GENERAL_RESOURCE_INFO   *ptemp=NULL;
     WEB_GENERAL_RESOURCE_INFO   *ptemp2=NULL;
    
    int icount=0;

    for(itemp=0;itemp<WEB_MAX_HTML_PAGE_NUM;itemp++)
    {
        ptemp=pWebCfgInfo[itemp].stWebPrivate;
        ptemp2=pWebCfgInfoTemp[itemp].stWebPrivate;
        if((ptemp != NULL) && (ptemp2 != NULL))
        {
            for(itemp2=0;itemp2<pWebCfgInfo[itemp].iNumber;itemp2++)
            {
                if(ptemp2->szLocal2!=NULL)
                {
                    ptemp->szLocal2=NEW_strdup(ptemp2->szLocal2);
                    icount++;
                }
                
                //printf("%s\n",ptemp->szLocal2);
                ptemp++;
                ptemp2++;
            }
        }
        
        
    }

   /* for(itemp=0;itemp<WEB_MAX_HTML_PAGE_NUM;itemp++)
    {
        ptemp=pWebCfgInfo[itemp].stWebPrivate;
        if( ptemp != NULL)
        {
            for(itemp2=0;itemp2<pWebCfgInfo[itemp].iNumber;itemp2++)
            {
                printf("%s\n",ptemp->szLocal2);
                ptemp++;
            }

        }
        
    }

    printf("transfer OK!!!");*/
    int icount2=0;
    for(itemp=0;itemp<WEB_MAX_HTML_PAGE_NUM;itemp++)
    {
        ptemp=pWebCfgInfoTemp[itemp].stWebPrivate;
        if(ptemp != NULL)
        {
            for(itemp2=0;itemp2<pWebCfgInfoTemp[itemp].iNumber;itemp2++)
            {
                if(ptemp->szLocal2!=NULL)
                {
                    DELETE(ptemp->szLocal2);
                    icount2++;
                }
                
                ptemp++;
            }

            DELETE(pWebCfgInfoTemp[itemp].stWebPrivate);
        }
        
       
    }

     DELETE(pWebCfgInfoTemp);
     TRACE("DELETE(pWebCfgInfoTemp); OK!!!");
     TRACE("icount=%d;icount2=%d\n",icount,icount2);
//end////////////////////////////////////////////////////////////////////////

     TRACE("LoadWebPrivateConfigProc2 OK!!!");


    return ERR_CFG_OK;
}
//end/////////////////////////////////////////////////////////////////


/*==========================================================================*
 * FUNCTION :    ParseWebResourceInfo
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN char *szBuf:
				OUT WEB_RESOURCE_INFO *pResourceInfo:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int ParseWebResourceInfo(IN char *szBuf, OUT WEB_RESOURCE_INFO *pResourceInfo)
{
	char *pField = NULL;


	ASSERT(szBuf);
	ASSERT(pResourceInfo);

	/* CfgName */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;    
	}
	strncpyz(pResourceInfo->CfgName, pField, sizeof(pResourceInfo->CfgName));
	//DELETE(pField);
	//pField = NULL;
	////TRACE("pField : 2 :%s\n",pField);

	/*CfgVersion*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;  
	}
	pResourceInfo->CfgVersion = atof(pField);
	////TRACE("pField : 2 :%s\n",pField);
	//DELETE(pField);
	//pField = NULL;


	/*CfgTime*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;    
	}
	strncpyz(pResourceInfo->szCfgTime, pField, sizeof(pResourceInfo->szCfgTime));
	//DELETE(pField);
	//pField = NULL;
	////TRACE("pField : 3 :%s\n",pField);

	/*Cfg default language*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;   
	}
	strncpyz(pResourceInfo->szCfgDefault, pField, sizeof(pResourceInfo->szCfgDefault));
	//DELETE(pField);
	//pField = NULL;
	////TRACE("pField : 4 :%s\n",pField);

	/*Cfg local language*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;  
	}
	strncpyz(pResourceInfo->szCfgLocal, pField, sizeof(pResourceInfo->szCfgLocal));
	//DELETE(pField);
	//pField = NULL;
	////TRACE("pField : 5 :%s\n",pField);

	/*Cfg jump CfgEngineer*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	//DELETE(pField);
	//pField = NULL;
	////TRACE("pField : 6 :%s\n",pField);

	/*Cfg local description*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1; 
	}
	strncpyz(pResourceInfo->szCfgDescription, pField, sizeof(pResourceInfo->szCfgDescription));
	//DELETE(pField);
	//pField = NULL;
	////TRACE("pField : 7 :%s\n",pField);

	return ERR_WEB_OK;
}

/*==========================================================================*
 * FUNCTION :    ParsedWebLangFileTableProc
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN char *szBuf:
				OUT WEB_GENERAL_RESOURCE_INFO *pStructData:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int ParsedWebLangFileTableProc(IN char *szBuf, 
									  OUT WEB_GENERAL_RESOURCE_INFO *pStructData)
{
	char *pField = NULL;
	int		iMaxLen = 0;
	/* used as buffer */
	
	ASSERT(szBuf);
	ASSERT(pStructData);
	
	////TRACE("szBuf : %s\n",szBuf);
	/*0.Jump sequence ID*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if(pField != NULL)
	{
		//DELETE(pField);
		//pField = NULL;
	}

	/* 1.RES ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;    
	}

	////TRACE("pStructData->szResourceID : %s\n", pField);
	strncpyz(pStructData->szResourceID, pField, sizeof(pStructData->szResourceID));

	/*2.iMaxFullLen*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;    
	}
	////TRACE("iMaxLen : %s\n", pField);
	iMaxLen = atoi(pField);

	/*3.jump iMaxAbbrLen*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
		return 1;    
	}

 	/* 4.English full language */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
 		return 2;    
	}
	////TRACE("pStructData->szDefault : %s\n", pField);
	pStructData->szDefault = NEW(char, iMaxLen + 1);
	//printf("pStructData->szDefault : %s\n", pField);
	strncpyz(pStructData->szDefault, pField, iMaxLen + 1);

	/*5.English abbr language*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
 		return 2;    
	}

	/* 6.local full language */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
 		return 3;    
	}
	////TRACE("pStructData->szLocal : %s\n", pField);
	pStructData->szLocal = NEW(char, iMaxLen + 1);
    //printf("pStructData->szLocal : %s\n", pField);
	strncpyz(pStructData->szLocal, pField, iMaxLen + 1);


	/*Jump 7.local abbr language */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField == NULL)
	{
 		return 3;    
	}
	/*Jump 8.description */
	
	return ERR_WEB_OK;
}

/*==========================================================================*
* FUNCTION :    ParsedWebLangFileTableProc2
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN char *szBuf:
OUT WEB_GENERAL_RESOURCE_INFO *pStructData:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-4-27 11:20
*==========================================================================*/
static int ParsedWebLangFileTableProc2(IN char *szBuf, 
                                      OUT WEB_GENERAL_RESOURCE_INFO *pStructData)
{
    char *pField = NULL;
    int		iMaxLen = 0;
    /* used as buffer */

    ASSERT(szBuf);
    ASSERT(pStructData);

    ////TRACE("szBuf : %s\n",szBuf);
    /*0.Jump sequence ID*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if(pField != NULL)
    {
        //DELETE(pField);
        //pField = NULL;
    }

    /* 1.jump RES ID field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
        return 1;    
    }

    ////TRACE("pStructData->szResourceID : %s\n", pField);
    strncpyz(pStructData->szResourceID, pField, sizeof(pStructData->szResourceID));

    /*2.jump iMaxFullLen*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
        return 1;    
    }
    ////TRACE("iMaxLen : %s\n", pField);
    iMaxLen = atoi(pField);

    /*3.jump iMaxAbbrLen*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
        return 1;    
    }

    /* 4.jump English full language */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
        return 2;    
    }
    ////TRACE("pStructData->szDefault : %s\n", pField);
    //pStructData->szDefault = NEW(char, iMaxLen + 1);
    //printf("pStructData->szDefault : %s\n", pField);
   // strncpyz(pStructData->szDefault, pField, iMaxLen + 1);

    /*5.jump English abbr language*/
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
        return 2;    
    }

    /* 6.local2 full language */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
        return 3;    
    }
    ////TRACE("pStructData->szLocal : %s\n", pField);
    pStructData->szLocal2 = NEW(char, iMaxLen + 1);
    //printf("pStructData->szLocal : %s\n", pField);
    strncpyz(pStructData->szLocal2, pField, iMaxLen + 1);
    //printf("pStructData->szLocal2 : %s\n", pField);


    /*Jump 7.local2 abbr language */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField == NULL)
    {
        return 3;    
    }
    /*Jump 8.description */

    return ERR_WEB_OK;
}

static int ServiceNotification(HANDLE hService, 
							   int iMsgType, 
							   int iTrapDataLength, 
							   void *lpTrapData,	
							   void *lpParam, 
							   BOOL bUrgent)
{
	UNUSED(hService);
	UNUSED(lpTrapData);
	UNUSED(bUrgent);
	UNUSED(lpParam);
	int		iRst;
	int		iBufLen;
	int		iActiveAlarmNum;


	//printf("iMsgType = %d\n", iMsgType);
	if ((iMsgType & (_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK)) != 0)
	{
		//iRst = DxiGetData(VAR_ACTIVE_ALARM_NUM,
		//			ALARM_LEVEL_CRITICAL,
		//			0,
		//			&iBufLen,
		//			&iActiveAlarmNum,
		//			0);
		//printf("CA Alarm %d\n",iActiveAlarmNum);
		//iRst = DxiGetData(VAR_ACTIVE_ALARM_NUM,
		//			ALARM_LEVEL_MAJOR,
		//			0,
		//			&iBufLen,
		//			&iActiveAlarmNum,
		//			0);
		//printf("MA Alarm %d\n",iActiveAlarmNum);
		//iRst = DxiGetData(VAR_ACTIVE_ALARM_NUM,
		//			ALARM_LEVEL_OBSERVATION,
		//			0,
		//			&iBufLen,
		//			&iActiveAlarmNum,
		//			0);
		//printf("OA Alarm %d\n",iActiveAlarmNum);
		if (iTrapDataLength != sizeof(ALARM_SIG_VALUE))
		{
			return 0;
		}
		g_AlarmCurrentTime = time(NULL);
		return 1;
	}

	return 0;
}

static void Web_ClearSessionFile(void)
{
	DIR				*pdir;
	struct dirent		*ent;
	char				path[32];
	char				*filename = NULL;
	char				filepath[64];

	sprintf(path,"%s","/var/");
	pdir = opendir(path);
	if(pdir != NULL)
	{
		while( (ent = readdir(pdir)) != NULL )
		{
			filename = ent->d_name; 
			if( strncmp(filename,"sess_",5) == 0 )
			{
				remove(filepath);
		        
			}
		} 
	}
	closedir(pdir);

}


static int Web_MakeQueryDiselTestLog(IN time_t fromTime, 
									 IN time_t toTime, 
									 IN int iLanguage, 
									 IN OUT char **ppBuf)
{

	int						iBufLen, iDataLen;
	WEB_GC_DSL_TEST_INFO	*pReturnData = NULL;
	char					*pMakeBuf = NULL, *pMakeFileBuf = NULL;
	int						iLen = 0, iFileLen = 0, iTotalRecords = 0;
	char					szTime[32],szTime2[32];

	UNUSED(iLanguage);
	char					szStartReason[2][32]={"Planned test","Manual start"};
	char					szTestResult[9][64]={"Normal",
												"Manual Stop",
												"Time is up",
												"In Man State",
												"Low Battery Voltage",
												"High Water Temperature",
												"Low Oil Pressure",
												"Low Fuel Level",
												"Diesel Failure"};

#define MAX_DISEL_DATA_LEN			120

	if(( iBufLen = Web_QueryDiselTestLog(fromTime, toTime, (void *)&pReturnData)) <= 0)
	{
		if(pReturnData != NULL)
		{
 			DELETE(pReturnData);
			pReturnData = NULL;
		}
		return FALSE;
	}

	iDataLen = iBufLen * MAX_DISEL_DATA_LEN;
	pMakeBuf = NEW(char, iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeBuf, 0, (size_t)iDataLen);

	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;

		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		return FALSE;
	}

	WEB_GC_DSL_TEST_INFO		*pTraiReturnData = pReturnData;
	
	while(pTraiReturnData != NULL && iBufLen > 0)
	{
		iLen += sprintf(pMakeBuf + iLen,"\t %d,\t %d,\t %ld, \t %ld%c\n",
										(int)pTraiReturnData->tStartTime,
										(int)pTraiReturnData->tEndTime,
										pTraiReturnData->enumStartReason,
										pTraiReturnData->enumTestResult,
										(iBufLen == 1) ? ' ':',');


		TimeToString(pTraiReturnData->tStartTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		TimeToString(pTraiReturnData->tEndTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

		iFileLen += sprintf(pMakeFileBuf + iFileLen,"%-8d \t%-32s \t%-32s \t%-16s \t%-16s\n",
										iTotalRecords + 1,
										szTime,
										szTime2,
										szStartReason[pTraiReturnData->enumStartReason],
										szTestResult[pTraiReturnData->enumTestResult]);

		iBufLen--;
		pTraiReturnData++;
		iTotalRecords++;
	}

	*ppBuf = pMakeBuf;
	int iAccess;
	iAccess = access(WEB_LOG_DIR, R_OK|W_OK);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL)
	{
		char	szFileTitle[512];
		
		TimeToString(fromTime, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "Query disel log\n" \
							"Query time:from %s to %s\nTotal %d record(s) queried\n " \
							"\n%-8s \t%-32s \t%-32s \t%-16s \t%-16s \n",
							szTime,
							szTime2,
							iTotalRecords,
							"Index",
							"Start time", 
							"End time", 
							"Start reason", 
							"Test result");

		fwrite(szFileTitle, strlen(szFileTitle),1, fp);

		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);
		if(iAccess < 0)
		{
		
			char szCommandStr[128];
			sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
			system(szCommandStr);
		}
	}


	if(pReturnData != NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
	}

	if(pMakeFileBuf != NULL)
	{
		DELETE(pMakeFileBuf);
		pMakeFileBuf = NULL;
	}
	return TRUE;
}

static int Web_QueryDiselTestLog(IN time_t fromTime,
							 IN time_t toTime,
							 OUT void **ppBuf)
{
#define WEB_DSL_TEST_MAX_RECORDS		12

	int			iRecords = WEB_DSL_TEST_MAX_RECORDS, iResult = 0;		//record number
	HANDLE		hDiselData;
	int			iBufLen = WEB_DSL_TEST_MAX_RECORDS;
	 
	hDiselData =	DAT_StorageOpen(DSL_TEST_LOG);

	if(hDiselData == NULL)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open %s", DSL_TEST_LOG);
		return FALSE;
	}

	WEB_GC_DSL_TEST_INFO *pDiselTestRecord	= NEW(WEB_GC_DSL_TEST_INFO,iBufLen);
	HIS_GENERAL_CONDITION *pCondition = NEW(HIS_GENERAL_CONDITION,1);
	
	ASSERT(pCondition);

	pCondition->tmToTime			= toTime;
	pCondition->tmFromTime			= fromTime;
	pCondition->iEquipID			= -1;
	pCondition->iDataType			= QUERY_DISEL_TEST;

	if(pDiselTestRecord == NULL )
	{
		DAT_StorageClose(hDiselData);
		
		DELETE(pCondition);
		pCondition = NULL;
		DELETE(pDiselTestRecord);
		pDiselTestRecord = NULL;
		
		return FALSE;
	}
	int		iStartRecordNum = -1;
	iResult = DAT_StorageFindRecords(hDiselData,
									Web_fnCompareQueryCondition, 
									(void *)pCondition,
									&iStartRecordNum, 
									&iRecords, 
									(void*)pDiselTestRecord, 
									0,
									TRUE);
	
	DELETE(pCondition);
	pCondition = NULL;

	if(iResult && iRecords > 0)
	{
		*ppBuf = (void *)pDiselTestRecord;
		DAT_StorageClose(hDiselData);
		return iRecords;
	}
	else
	{
		DELETE(pDiselTestRecord);
		pDiselTestRecord = NULL;
		DAT_StorageClose(hDiselData);
		return FALSE;
	}
}

//#define PRODUCT_INFO_SUPPORT
#ifdef PRODUCT_INFO_SUPPORT
/*********************************************************************************
*  
*  FUNCTION NAME : IsSMDU_Barcode_NeedDisplay
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-5-31 14:27:24
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static BOOL	IsSMDU_Barcode_NeedDisplay(int nSMDUIndex)
{
#define SMDU1_EQUIP_ID		107
#define SMDU_EXIST_STATE	0
	int	iError;
	int	iEquipID = SMDU1_EQUIP_ID + nSMDUIndex;// ��ȡSMDUʵ��EquipID
	int	iSignalType = 0;
	int	iSignalID = 100; //ͨ��Existence�ź����ж��Ƿ���ʾ
	int	iBufLen;
	int	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	SIG_BASIC_VALUE* pSigValue;
	BOOL	bNeedFlag = FALSE;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		5000);
	if(iError == ERR_DXI_OK)
	{
		bNeedFlag = (pSigValue->varValue.enumValue == SMDU_EXIST_STATE) ? TRUE:FALSE;
		//printf("\nEquipID:%d, bNeedDis: %d\n",iEquipID,bNeedFlag);
	}
	else
	{
		bNeedFlag = FALSE;
		//printf("\nErr Code is : %d\n!!!",iError);
	}

	return bNeedFlag;
}
static int Web_GetBarcode_Info(IN char **szProductInfo,IN int iLanguage)
{
#define  MAX_PRODUCT_INFO_LEN			384	

	int		iDevice = DXI_GetDeviceNum();
	int		iRectNumber = DXI_GetRectRealNumber();
	int		iRectStartDeviceID =  DXI_GetFirstRectDeviceID();
	int		iSMDUNumber;
	int		iSlave1RectNum=0;
	int		iSlave2RectNum=0;
	int		iSlave3RectNum=0;
	int		iSMDUPNumber;

    //TRACE("iDevice:%d | iRectNumber:%d | iRectStartDeviceID:%d\n",iDevice,iRectNumber,iRectStartDeviceID);

	int		i = 0, iLen = 0;
	char	*pSearchValue = NULL;
	char	*szTProductInfo = NULL;
	int iError;
	int iEquipID = 106;//SMDUGroup
	int iSignalType = 0;
	int iSignalID = 1;
	int iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	SIG_BASIC_VALUE* pSigValue;
	int j;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);

	if(iError != ERR_DXI_OK)
	{
		iSMDUNumber = 0;
		
	}
	else
	{
		iSMDUNumber = pSigValue->varValue.ulValue;

	}

	iEquipID = 642;//Slave1
	iSignalType = 0;
	iSignalID = 1;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);

	if(iError != ERR_DXI_OK)
	{
		iSMDUPNumber = 0;

	}
	else
	{
		iSMDUPNumber = pSigValue->varValue.ulValue;

	}
	
	iEquipID = 639;//Slave1
	iSignalType = 0;
	iSignalID = 2;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);
	if(iError != ERR_DXI_OK)
	{
		iSlave1RectNum = 0;

	}
	else
	{
		iSlave1RectNum = pSigValue->varValue.ulValue;
	}
	

	iEquipID = 640;//Slave2
	iSignalType = 0;
	iSignalID = 2;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);
	
	if(iError != ERR_DXI_OK)
	{
		iSlave2RectNum = 0;

	}
	else
	{
		iSlave2RectNum = pSigValue->varValue.ulValue;
	}

	iEquipID = 641;//Slave3
	iSignalType = 0;
	iSignalID = 2;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);
	
	if(iError != ERR_DXI_OK)
	{
		iSlave3RectNum = 0;

	}
	else
	{
		iSlave3RectNum = pSigValue->varValue.ulValue;
	}

	int	iSMDUSamplerStatus;

	iEquipID = 1;//SMDU Sampler Status
	iSignalType = 2;
	iSignalID = 183;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		10000);
	if(iError != ERR_DXI_OK)
	{
		iSMDUSamplerStatus = 0;

	}
	else
	{
		iSMDUSamplerStatus = pSigValue->varValue.ulValue;
	}




	

	if(!iRectStartDeviceID)
	{
		return FALSE;
	}

	if(iDevice > 0 && iRectNumber < iDevice )
	{
		PRODUCT_INFO	piProductInfo ;
		PRODUCT_INFO	*pSystemProductInfo ;
		szTProductInfo = NEW(char, iDevice * MAX_PRODUCT_INFO_LEN);
		pSystemProductInfo = DXI_GetDeviceProductInfo();

			//if(Web_CheckValid(piProductInfo.szPartNumber))
			//{
			//	TRACE("\npiProductInfo.szPartNumber=%s", piProductInfo.szPartNumber);
			iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",",
				(Web_CheckValid(pSystemProductInfo->szDeviceName[iLanguage])) ? pSystemProductInfo->szDeviceName[iLanguage]: "N/A",
				(Web_CheckValid(pSystemProductInfo->szPartNumber)) ? pSystemProductInfo->szPartNumber: "N/A",
				(Web_CheckValid(pSystemProductInfo->szHWVersion)) ? pSystemProductInfo->szHWVersion: "N/A",
				(Web_CheckValid(pSystemProductInfo->szSerialNumber)) ? pSystemProductInfo->szSerialNumber: "N/A",
				(Web_CheckValid(pSystemProductInfo->szSWVersion)) ? pSystemProductInfo->szSWVersion: "N/A");
			//}	



		//Get ACU Information
		
		//for( i = 1; i < iRectStartDeviceID; i++)
		//{
		//	if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK) 
		//	{
		//		//if(Web_CheckValid(piProductInfo.szPartNumber))
		//		//{
		//		//	TRACE("\npiProductInfo.szPartNumber=%s", piProductInfo.szPartNumber);
		//			iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",",
		//									(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
		//									(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
		//									(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
		//									(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
		//									(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
		//		//}	
		//	}
		//}
  //      TRACE("%s\n",szTProductInfo);
		//Get Rectifier number
		for(i = iRectStartDeviceID; i < iRectStartDeviceID + iRectNumber ; i++)
		{

			if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK) 
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
					iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
						(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ?piProductInfo.szDeviceName[iLanguage]: "N/A",
						(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber : "N/A",
						(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
						(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
						(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
				//}
			}
		}
        TRACE("%s\n",szTProductInfo);

	printf("[communicate.c]iSMDUNumber is %d\n", iSMDUNumber);
	if(iSMDUNumber > 0) //Jimmy ע�ͣ��������жϣ���ֹûɨ��SMDU��ʱ��Ҳȥ���㣬��ɴ���
	{	
		//	//Jimmy ע��:20120531�޸ģ���LCDһ��SMDU��ַ����ǰ������������㷨�޸ģ���
		//	//������IsSMDU_Barcode_NeedDisplay���������ж��Ƿ���ʾ

		for(j = 0; j < 8; j++) //8��SMDU
		{
		    printf("[communicate.c]smdu for cycle %d\n", j);
			if(iSMDUSamplerStatus == 1)
			{
				i = iRectStartDeviceID + 102 + j;
			}
			else
			{
				i = iRectStartDeviceID + 100 + j;
			}
			if(IsSMDU_Barcode_NeedDisplay(j))//ע�⣬������J������I
			{
			    printf("[communicate.c]smdu IsSMDU_Barcode_NeedDisplay %d\n", j);
				if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
				{
				    printf("[communicate.c]smdu DXI_GetPIByDeviceID %d\n", i);
					//if(Web_CheckValid(piProductInfo.szPartNumber))
					//{
					iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
						(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
						(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
						(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
						(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
						(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
					//}
				}
			}
		}
		//if(iSMDUSamplerStatus == 1)
		//{		
		//	//for(i = iRectStartDeviceID + 102, j = 0; i <= iDevice && j < iSMDUNumber; i++,j++)
		//	//{

		//	//	if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
		//	//	{
		//	//		//if(Web_CheckValid(piProductInfo.szPartNumber))
		//	//		//{
		//	//		iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
		//	//			(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
		//	//			(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
		//	//			(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
		//	//			(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
		//	//			(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
		//	//		//}
		//	//	}
		//	//}
		//}
		//else
		//{
		//	for(i = iRectStartDeviceID + 100, j = 0; i <= iDevice && j < iSMDUNumber; i++,j++)
		//	{

		//		if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
		//		{
		//			//if(Web_CheckValid(piProductInfo.szPartNumber))
		//			//{
		//			iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
		//				(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
		//				(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
		//				(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
		//				(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
		//				(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
		//			//}
		//		}
		//	}

		//}
	}
	else
	{
		//SMDU����Ϊ0��������
	}


		
		

		/*if(DXI_GetPIByDeviceID(i = iRectStartDeviceID + 108, &piProductInfo) == ERR_DXI_OK) 
		{
			//if(Web_CheckValid(piProductInfo.szPartNumber))
			//{
			//	TRACE("\npiProductInfo.szPartNumber=%s", piProductInfo.szPartNumber);
			iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",",
				(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
				(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
				(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
				(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
				(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
			//}	
		}*/
		//Revised by Jimmy Wu 2011/07/23, Slave Start from 110, not 118	
		for(i = iRectStartDeviceID + 110, j = 0; i <= iDevice && j < iSlave1RectNum; i++,j++)
		{

			if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
					(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
					(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
					(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
				//}
			}
		}

		for(i = iRectStartDeviceID + 170, j = 0; i <= iDevice && j < iSlave2RectNum; i++,j++)
		{

			if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
					(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
					(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
					(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
				//}
			}
		}
		for(i = iRectStartDeviceID + 230, j = 0; i <= iDevice && j < iSlave3RectNum; i++,j++)
		{

			if(DXI_GetPIByDeviceID(i, &piProductInfo) == ERR_DXI_OK)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(piProductInfo.szDeviceName[iLanguage])) ? piProductInfo.szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(piProductInfo.szPartNumber)) ? piProductInfo.szPartNumber: "N/A",
					(Web_CheckValid(piProductInfo.szHWVersion)) ? piProductInfo.szHWVersion: "N/A",
					(Web_CheckValid(piProductInfo.szSerialNumber)) ? piProductInfo.szSerialNumber: "N/A",
					(Web_CheckValid(piProductInfo.szSWVersion)) ? piProductInfo.szSWVersion: "N/A");
				//}
			}
		}

		
		//Start from 290, remarked by Jimmy Wu 2011/07/23 , the former writer mistaken by 298
		for(i = 0; i < 8; i++)
		{

			if(g_SiteInfo.stI2CProductInfo[i].bSigModelUsed == TRUE)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szDeviceName[iLanguage])) ? g_SiteInfo.stI2CProductInfo[i].szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szPartNumber)) ? g_SiteInfo.stI2CProductInfo[i].szPartNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szHWVersion)) ? g_SiteInfo.stI2CProductInfo[i].szHWVersion: "N/A",
					(Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szSerialNumber)) ? g_SiteInfo.stI2CProductInfo[i].szSerialNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stI2CProductInfo[i].szSWVersion)) ? g_SiteInfo.stI2CProductInfo[i].szSWVersion: "N/A");
				//}
			}
		}

		for(i = 0; i < 8; i++)
		{

			if(g_SiteInfo.stCANSMDUPProductInfo[i].bSigModelUsed == TRUE)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szDeviceName[iLanguage])) ? g_SiteInfo.stCANSMDUPProductInfo[i].szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANSMDUPProductInfo[i].szPartNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANSMDUPProductInfo[i].szHWVersion: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANSMDUPProductInfo[i].szSerialNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANSMDUPProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANSMDUPProductInfo[i].szSWVersion: "N/A");
				//}
			}
		}

		for(i = 0; i < 66; i++)
		{
		    printf("[communicate.c]485 for cycle %d\n", i);
			if(g_SiteInfo.st485ProductInfo[i].bSigModelUsed == TRUE)
			{
			    printf("[communicate.c]485 bSigModelUsed %d\n", i);
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szDeviceName[iLanguage])) ? g_SiteInfo.st485ProductInfo[i].szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szPartNumber)) ? g_SiteInfo.st485ProductInfo[i].szPartNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szHWVersion)) ? g_SiteInfo.st485ProductInfo[i].szHWVersion: "N/A",
					(Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szSerialNumber)) ? g_SiteInfo.st485ProductInfo[i].szSerialNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.st485ProductInfo[i].szSWVersion)) ? g_SiteInfo.st485ProductInfo[i].szSWVersion: "N/A");
				//}
			}
		}

		for(i = 0; i < 60; i++)
		{

			if(g_SiteInfo.stCANConverterProductInfo[i].bSigModelUsed == TRUE)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szDeviceName[iLanguage])) ? g_SiteInfo.stCANConverterProductInfo[i].szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANConverterProductInfo[i].szPartNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANConverterProductInfo[i].szHWVersion: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANConverterProductInfo[i].szSerialNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANConverterProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANConverterProductInfo[i].szSWVersion: "N/A");
				//}
			}
		}

		for(i = 0; i < 8; i++)
		{

			if(g_SiteInfo.stCANSMTempProductInfo[i].bSigModelUsed == TRUE)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szDeviceName[iLanguage])) ? g_SiteInfo.stCANSMTempProductInfo[i].szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANSMTempProductInfo[i].szPartNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANSMTempProductInfo[i].szHWVersion: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANSMTempProductInfo[i].szSerialNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANSMTempProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANSMTempProductInfo[i].szSWVersion: "N/A");
				//}
			}
		}
		for(i = 0; i < 60; i++)
		{

			if(g_SiteInfo.stCANLiBattProductInfo[i].bSigModelUsed == TRUE)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szDeviceName[iLanguage])) ? g_SiteInfo.stCANLiBattProductInfo[i].szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANLiBattProductInfo[i].szPartNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANLiBattProductInfo[i].szHWVersion: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANLiBattProductInfo[i].szSerialNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANLiBattProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANLiBattProductInfo[i].szSWVersion: "N/A");
				//}
			}
		}
		//��ʾBridgeCard�Ĳ�Ʒ��Ϣ����������Jimmy Added 2012/06/26
		if(g_SiteInfo.stCANLiBridgeCardProductInfo.bSigModelUsed == TRUE)
		{
			//if(Web_CheckValid(piProductInfo.szPartNumber))
			//{
			iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
				(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[iLanguage])) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szDeviceName[iLanguage]: "N/A",
				(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szPartNumber)) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szPartNumber: "N/A",
				(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szHWVersion)) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szHWVersion: "N/A",
				(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szSerialNumber)) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szSerialNumber: "N/A",
				(Web_CheckValid(g_SiteInfo.stCANLiBridgeCardProductInfo.szSWVersion)) ? g_SiteInfo.stCANLiBridgeCardProductInfo.szSWVersion: "N/A");
			//}
		}

#ifdef GC_SUPPORT_MPPT
		for(i = 0; i < 16; i++)//Can
		{

			if(g_SiteInfo.stCANMpptProductInfo[i].bSigModelUsed == TRUE)
			{
				//if(Web_CheckValid(piProductInfo.szPartNumber))
				//{
				iLen += sprintf(szTProductInfo + iLen,"\"%16s\",\"%16s\",\"%16s\",\"%32s\",\"%16s\",", 
					(Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szDeviceName[iLanguage])) ? g_SiteInfo.stCANMpptProductInfo[i].szDeviceName[iLanguage]: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szPartNumber)) ? g_SiteInfo.stCANMpptProductInfo[i].szPartNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szHWVersion)) ? g_SiteInfo.stCANMpptProductInfo[i].szHWVersion: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szSerialNumber)) ? g_SiteInfo.stCANMpptProductInfo[i].szSerialNumber: "N/A",
					(Web_CheckValid(g_SiteInfo.stCANMpptProductInfo[i].szSWVersion)) ? g_SiteInfo.stCANMpptProductInfo[i].szSWVersion: "N/A");
				//}
			}
		}
#endif	

        TRACE("%s\n",szTProductInfo);
		//printf("\n%d %d %s\n", iDevice, iRectNumber, szTProductInfo);
		pSearchValue = strrchr(szTProductInfo,44);  //get the last ','
		*pSearchValue = 32;   //replace ',' with ' '
		//TRACE("\nszTProductInfo:%s\n", szTProductInfo);

		*szProductInfo = szTProductInfo;
		return TRUE;
	}
	return FALSE;
}

static BOOL Web_CheckValid(IN const char *pField)
{
	return (pField[0] && !strstr(pField, "N/A")) ? TRUE:FALSE;
}

#endif
//////////////////////////////////////////////////////////////////////////



#define	MAX_SIZE_OF_SIG		20
#define	MAX_SIZE_OF_EQUIP	10
/*==========================================================================*
 * FUNCTION : Web_MakePlcWebPage
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT char  **ppszReturn : 
 *            IN int    iLanguage    : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2007-03-08 16:15
 *==========================================================================*/
static int Web_MakePlcWebPage(OUT char **ppszReturn, IN int iLanguage)
{
    //TRACE(" ______into Web_MakePlcWebPage________");

    int		    i, j, iFirstRectID = 0, iEquipNum = 0, iBufLen;
    FILE        *fp          = NULL;
    EQUIP_INFO  *pEquipInfo  = NULL;
	EQUIP_INFO  *pEquip = NULL;
    char        *pSearchValue= NULL;
    char        *szFile      = NULL;
    char        *pPageFile   = NULL;
    char        *pHtml       = NULL;
    char        *pszMakeVar  = NULL;
    char        *pszID       = NULL;
    char        *pszTemp     = NULL;
    char        *pszPLCInfo  = NULL;
    int         iReturn;
    int         iLen = 0, iPLCInfoLen = 0;
    int         iSamplingNum = 0, iControlNum = 0,iSettingNum = 0, iAlarmNum = 0;
    SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
    CTRL_SIG_VALUE    *pControlSigValue = NULL;
    SET_SIG_VALUE     *pSettingSigValue = NULL;
    ALARM_SIG_INFO   *pAlarmSigInfo   = NULL;
	char*		pResult = NULL;

    char        szBuf[MAX_BUFFER * 2];

	/*char*	szBuf = NEW(char, MAX_BUFFER);
	ASSERT(szBuf);*/

    //get equip information
    int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
        0,			
        0,		
        &iBufLen,			
        &pEquip,			
        0);

    //get equip number
    int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
        0,			
        0,		
        &iBufLen,			
        (void *)&iEquipNum,			
        0);

    if(iError1 || iError2 || !szBuf)
    {
        return FALSE;
    }

    iPLCInfoLen = Web_LoadPlcConfigFile(&pszPLCInfo);//get PLC Configure file Info
    if(iPLCInfoLen == -1)
    {
        return 0;
    }
    
	TRACE("\n\n***********%s-%d,  OK!!!!!***********\n\n\n", __FUNCTION__, __LINE__);

	////////////////////////////////////////

	//Stuff pHtml, result will be in iReturn
	pPageFile = "p40_cfg_plc_Popup.htm";
    szFile = MakeFilePath(pPageFile, iLanguage);

    ASSERT(szFile);
    if(szFile != NULL)
    {
        iReturn = LoadHtmlFile(szFile, &pHtml);
    }
	/////////////////////////////////////////

	//New pResult
	pEquipInfo = pEquip;
    for(i = 0; i < iEquipNum; i++, pEquipInfo++)
    {

        iSamplingNum += Web_GetSampleSignalByEquipID(pEquipInfo->iEquipID, &pSampleSigValue);
        iControlNum += Web_GetControlSignalByEquipID(pEquipInfo->iEquipID, &pControlSigValue);
        iSettingNum += Web_GetSettingSignalByEquipID(pEquipInfo->iEquipID, &pSettingSigValue);
        iAlarmNum += Web_GetAlarmSigStruofEquip(pEquipInfo->iEquipID, &pAlarmSigInfo);
    }
	
	pResult = NEW(char, MAX_SIZE_OF_EQUIP * iEquipNum 
						+ MAX_SIZE_OF_SIG * (iSamplingNum + iControlNum + iSettingNum + iAlarmNum) 
						+ iPLCInfoLen
						+ 7);
	if(!pResult)
	{
		AppLogOut("WEB", 
			APP_LOG_ERROR, 
            "[%s-%s-%d]: There is no enough memory! "
            __FILE__,
			__FUNCTION__,
			__LINE__);

        return ERR_NO_MEMORY;
	}
	TRACE("\n\n*****%s-%d,  OK!!!!! Size of pResult is %d***********\n\n\n", 
		__FUNCTION__,
		__LINE__, 
		MAX_SIZE_OF_EQUIP * iEquipNum 
			+ MAX_SIZE_OF_SIG * (iSamplingNum + iControlNum + iSettingNum + iAlarmNum) 
			+ iPLCInfoLen
			+ 7);


	//Equip info handling...
	memset(szBuf, 0, MAX_BUFFER * 2);
	iLen = 0;
	szBuf[iLen++] = '\t';
	pEquipInfo = pEquip;
	for(i = 0; i < iEquipNum; i++, pEquipInfo++)
	{
		if(pEquipInfo->pStdEquip->iTypeID == 1101 ||
			pEquipInfo->pStdEquip->iTypeID == 1600 || pEquipInfo->pStdEquip->iTypeID == 1601 ||
			pEquipInfo->pStdEquip->iTypeID == 1700 || pEquipInfo->pStdEquip->iTypeID == 1701 ||
			pEquipInfo->pStdEquip->iTypeID == 1800 || pEquipInfo->pStdEquip->iTypeID == 1801 ||
			(pEquipInfo->pStdEquip->iTypeID >= 1900 && pEquipInfo->pStdEquip->iTypeID <= 1908) ||
			pEquipInfo->pStdEquip->iTypeID == 1300 || pEquipInfo->pStdEquip->iTypeID == 1301 ||
			pEquipInfo->pStdEquip->iTypeID == 1400 || pEquipInfo->pStdEquip->iTypeID == 1401 ||
			pEquipInfo->pStdEquip->iTypeID == 2000 || pEquipInfo->pStdEquip->iTypeID == 2001 ||
			pEquipInfo->pStdEquip->iTypeID == 305 || pEquipInfo->pStdEquip->iTypeID == 306 ||
			pEquipInfo->pStdEquip->iTypeID == 2200 || pEquipInfo->pStdEquip->iTypeID == 2201 ||
			pEquipInfo->pStdEquip->iTypeID == 2202 || pEquipInfo->pStdEquip->iTypeID == 2203 ||
			pEquipInfo->pStdEquip->iTypeID == 2204 || pEquipInfo->pStdEquip->iTypeID == 2205 ||
			pEquipInfo->pStdEquip->iTypeID == 2206 || pEquipInfo->pStdEquip->iTypeID == 2207 ||
			pEquipInfo->pStdEquip->iTypeID == 2208 || pEquipInfo->pStdEquip->iTypeID == 504 ||
			pEquipInfo->pStdEquip->iTypeID == 308 || pEquipInfo->pStdEquip->iTypeID == 309 ||
			pEquipInfo->pStdEquip->iTypeID == 2400 || pEquipInfo->pStdEquip->iTypeID == 2401 ||
			pEquipInfo->pStdEquip->iTypeID == 2500 || pEquipInfo->pStdEquip->iTypeID == 2501 ||

			pEquipInfo->pStdEquip->iTypeID == 403)
		{
		    //pEquipInfo++;
		    continue;
		}
        
		iLen += sprintf(szBuf + iLen, 
							"new OEquip(%4d, \"%s\"),\n",
							pEquipInfo->iEquipID, 
							pEquipInfo->pEquipName->pAbbrName[iLanguage]);
		
	}
	
	pSearchValue = strrchr(szBuf, 44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }

    if(iReturn > 0)
    {
        pszID = "equipments";
        pszMakeVar = MakeVarField(pszID);
        if(pszMakeVar != NULL)
        {
            ReplaceString(&pHtml, pszMakeVar, szBuf);
            DELETE(pszMakeVar);
            pszMakeVar = NULL;
        }
	}

    strcpy(pResult, szBuf);

	TRACE("\n\n*****%s-%d, Equip Handling is OK! Length of pResult is %d, iLen = %d******\n\n\n", 
		__FUNCTION__, __LINE__,
		strlen(pResult), iLen);

	//Sampling Info Handling
	memset(szBuf, 0, MAX_BUFFER * 2);
	iLen = 0;
	szBuf[iLen++] = '\t';

	pEquipInfo = pEquip;
	for(i = 0; i < iEquipNum; i++, pEquipInfo++)
	{
		if( pEquipInfo->pStdEquip->iTypeID == 1101 ||
		    pEquipInfo->pStdEquip->iTypeID == 1600 || pEquipInfo->pStdEquip->iTypeID == 1601 ||
		    pEquipInfo->pStdEquip->iTypeID == 1700 || pEquipInfo->pStdEquip->iTypeID == 1701 ||
		    pEquipInfo->pStdEquip->iTypeID == 1800 || pEquipInfo->pStdEquip->iTypeID == 1801 ||
		   (pEquipInfo->pStdEquip->iTypeID >= 1900 && pEquipInfo->pStdEquip->iTypeID <= 1908) ||
		    pEquipInfo->pStdEquip->iTypeID == 1300 || pEquipInfo->pStdEquip->iTypeID == 1301 ||
		    pEquipInfo->pStdEquip->iTypeID == 1400 || pEquipInfo->pStdEquip->iTypeID == 1401 ||
		    pEquipInfo->pStdEquip->iTypeID == 2000 || pEquipInfo->pStdEquip->iTypeID == 2001 ||
		    pEquipInfo->pStdEquip->iTypeID == 305 || pEquipInfo->pStdEquip->iTypeID == 306 ||
		    pEquipInfo->pStdEquip->iTypeID == 2200 || pEquipInfo->pStdEquip->iTypeID == 2201 ||
		    pEquipInfo->pStdEquip->iTypeID == 2202 || pEquipInfo->pStdEquip->iTypeID == 2203 ||
		    pEquipInfo->pStdEquip->iTypeID == 2204 || pEquipInfo->pStdEquip->iTypeID == 2205 ||
		    pEquipInfo->pStdEquip->iTypeID == 2206 || pEquipInfo->pStdEquip->iTypeID == 2207 ||
		    pEquipInfo->pStdEquip->iTypeID == 2208 || pEquipInfo->pStdEquip->iTypeID == 504 ||
		    pEquipInfo->pStdEquip->iTypeID == 308 || pEquipInfo->pStdEquip->iTypeID == 309 ||
		    pEquipInfo->pStdEquip->iTypeID == 2400 || pEquipInfo->pStdEquip->iTypeID == 2401 ||
		    pEquipInfo->pStdEquip->iTypeID == 2500 || pEquipInfo->pStdEquip->iTypeID == 2501 ||
		    pEquipInfo->pStdEquip->iTypeID == 403)
		{
		    //pEquipInfo++;
		    continue;
		}
		iSamplingNum = Web_GetSampleSignalByEquipID(pEquipInfo->iEquipID, &pSampleSigValue);
        
		for(j = 0; j < iSamplingNum; j++)
		{
			//if(SIG_VALUE_NEED_DISPLAY(&pSampleSigValue->bv)&&STD_SIG_IS_DISPLAY_ON_UI(pSampleSigValue->pStdSig,DISPLAY_WEB)&&SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv))
			//{
			if(STD_SIG_IS_DISPLAY_ON_UI(pSampleSigValue->pStdSig,DISPLAY_WEB) || pEquipInfo->pStdEquip->iTypeID == 100)
			{
				//OSample( equipId,signalId,signalName,valueType,valueState) 
				iLen += sprintf(szBuf + iLen, "new OSample( %4d,%4d,\"%s\",%d,%d),\n",
									pEquipInfo->iEquipID,
									pSampleSigValue->pStdSig->iSigID,
									//pSampleSigValue->pStdSig->pSigName->pFullName[iLanguage],
									pSampleSigValue->pStdSig->pSigName->pAbbrName[iLanguage], 
									pSampleSigValue->bv.ucType,
									SIG_VALUE_IS_VALID(&pSampleSigValue->bv));
			}
			//}
			pSampleSigValue++;
		}
		
	}

    pSearchValue = strrchr(szBuf, 44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }
	
	if(iReturn > 0)
	{
		pszID = "equip_sample_signals";
		pszMakeVar = MakeVarField(pszID);
		
		if(pszMakeVar != NULL)
		{
            ReplaceString(&pHtml, pszMakeVar, szBuf);
            DELETE(pszMakeVar);
            pszMakeVar = NULL;
        }
	}

    szBuf[0] = ';';
    strcat(pResult, szBuf);
	TRACE("\n\n*****%s-%d, Sampling Handling is OK! Length of pResult is %d, iLen = %d******\n\n\n", 
		__FUNCTION__, __LINE__,
		strlen(pResult), iLen);
	
	//Control Info Handling
	memset(szBuf, 0, MAX_BUFFER * 2);
	iLen = 0;
	szBuf[iLen++] = '\t';

	pEquipInfo = pEquip;
	for(i = 0; i < iEquipNum; i++, pEquipInfo++)
	{
		if(pEquipInfo->pStdEquip->iTypeID == 1101 ||
			pEquipInfo->pStdEquip->iTypeID == 1600 || pEquipInfo->pStdEquip->iTypeID == 1601 ||
			pEquipInfo->pStdEquip->iTypeID == 1700 || pEquipInfo->pStdEquip->iTypeID == 1701 ||
			pEquipInfo->pStdEquip->iTypeID == 1800 || pEquipInfo->pStdEquip->iTypeID == 1801 ||
			(pEquipInfo->pStdEquip->iTypeID >= 1900 && pEquipInfo->pStdEquip->iTypeID <= 1908) ||
			pEquipInfo->pStdEquip->iTypeID == 1300 || pEquipInfo->pStdEquip->iTypeID == 1301 ||
			pEquipInfo->pStdEquip->iTypeID == 1400 || pEquipInfo->pStdEquip->iTypeID == 1401 ||
			pEquipInfo->pStdEquip->iTypeID == 2000 || pEquipInfo->pStdEquip->iTypeID == 2001 ||
			pEquipInfo->pStdEquip->iTypeID == 305 || pEquipInfo->pStdEquip->iTypeID == 306 ||
			pEquipInfo->pStdEquip->iTypeID == 2200 || pEquipInfo->pStdEquip->iTypeID == 2201 ||
			pEquipInfo->pStdEquip->iTypeID == 2202 || pEquipInfo->pStdEquip->iTypeID == 2203 ||
			pEquipInfo->pStdEquip->iTypeID == 2204 || pEquipInfo->pStdEquip->iTypeID == 2205 ||
			pEquipInfo->pStdEquip->iTypeID == 2206 || pEquipInfo->pStdEquip->iTypeID == 2207 ||
			pEquipInfo->pStdEquip->iTypeID == 2208 || pEquipInfo->pStdEquip->iTypeID == 504 ||
			pEquipInfo->pStdEquip->iTypeID == 308 || pEquipInfo->pStdEquip->iTypeID == 309 ||
			pEquipInfo->pStdEquip->iTypeID == 2400 || pEquipInfo->pStdEquip->iTypeID == 2401 ||
			pEquipInfo->pStdEquip->iTypeID == 2500 || pEquipInfo->pStdEquip->iTypeID == 2501 ||
			pEquipInfo->pStdEquip->iTypeID == 403)
		{
		    //pEquipInfo++;
		    continue;
		}
		iControlNum  = Web_GetControlSignalByEquipID(pEquipInfo->iEquipID, &pControlSigValue);
		for(j = 0; j < iControlNum; j++)
		{
			//if(SIG_VALUE_NEED_DISPLAY(&pControlSigValue->bv)&&STD_SIG_IS_DISPLAY_ON_UI(pControlSigValue->pStdSig,DISPLAY_WEB))
			//{
			//OControl( equipId,signalId,signalName,valueType,valueState) 
				iLen += sprintf(szBuf + iLen, "new OControl( %4d,%4d,\"%s\",%d,%d),\n",
										pEquipInfo->iEquipID,
										pControlSigValue->pStdSig->iSigID,
										pControlSigValue->pStdSig->pSigName->pAbbrName[iLanguage],
										pControlSigValue->bv.ucType,
										SIG_VALUE_IS_VALID(&pControlSigValue->bv));
			//}
			pControlSigValue++;
		}
	}

    pSearchValue = strrchr(szBuf, 44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }
	
	if(iReturn > 0)
	{
        pszID = "equip_control_signals";
        pszMakeVar = MakeVarField(pszID);
        if(pszMakeVar != NULL)
        {
            ReplaceString(&pHtml, pszMakeVar, szBuf);
            DELETE(pszMakeVar);
            pszMakeVar = NULL;
        }
	}

	szBuf[0] = ';';
	strcat(pResult, szBuf);
	TRACE("\n\n*****%s-%d, Control Handling is OK! Length of pResult is %d, iLen = %d******\n\n\n", 
		__FUNCTION__, __LINE__,
		strlen(pResult), iLen);

	//Setting Info Handling
	memset(szBuf, 0, MAX_BUFFER * 2);
	iLen = 0;
	szBuf[iLen++] = '\t';

	pEquipInfo = pEquip;
	for(i = 0; i < iEquipNum; i++, pEquipInfo++)
	{
		if( pEquipInfo->pStdEquip->iTypeID == 1101 ||
			pEquipInfo->pStdEquip->iTypeID == 1600 || pEquipInfo->pStdEquip->iTypeID == 1601 ||
			pEquipInfo->pStdEquip->iTypeID == 1700 || pEquipInfo->pStdEquip->iTypeID == 1701 ||
			pEquipInfo->pStdEquip->iTypeID == 1800 || pEquipInfo->pStdEquip->iTypeID == 1801 ||
			(pEquipInfo->pStdEquip->iTypeID >= 1900 && pEquipInfo->pStdEquip->iTypeID <= 1908) ||
			pEquipInfo->pStdEquip->iTypeID == 1300 || pEquipInfo->pStdEquip->iTypeID == 1301 ||
			pEquipInfo->pStdEquip->iTypeID == 1400 || pEquipInfo->pStdEquip->iTypeID == 1401 ||
			pEquipInfo->pStdEquip->iTypeID == 2000 || pEquipInfo->pStdEquip->iTypeID == 2001 ||
			pEquipInfo->pStdEquip->iTypeID == 305 || pEquipInfo->pStdEquip->iTypeID == 306 ||
			pEquipInfo->pStdEquip->iTypeID == 2200 || pEquipInfo->pStdEquip->iTypeID == 2201 ||
			pEquipInfo->pStdEquip->iTypeID == 2202 || pEquipInfo->pStdEquip->iTypeID == 2203 ||
			pEquipInfo->pStdEquip->iTypeID == 2204 || pEquipInfo->pStdEquip->iTypeID == 2205 ||
			pEquipInfo->pStdEquip->iTypeID == 2206 || pEquipInfo->pStdEquip->iTypeID == 2207 ||
			pEquipInfo->pStdEquip->iTypeID == 2208 || pEquipInfo->pStdEquip->iTypeID == 504 ||
			pEquipInfo->pStdEquip->iTypeID == 308 || pEquipInfo->pStdEquip->iTypeID == 309 ||
			pEquipInfo->pStdEquip->iTypeID == 2400 || pEquipInfo->pStdEquip->iTypeID == 2401 ||
			pEquipInfo->pStdEquip->iTypeID == 2500 || pEquipInfo->pStdEquip->iTypeID == 2501 ||
			pEquipInfo->pStdEquip->iTypeID == 403)
		{
		    //pEquipInfo++;
		    continue;
		}
		iSettingNum  = Web_GetSettingSignalByEquipID(pEquipInfo->iEquipID, &pSettingSigValue);
		for(j = 0; j < iSettingNum; j++)
		{
			if((pEquipInfo->pStdEquip->iTypeID == 100 && pSettingSigValue->pStdSig->iSigID >= 231 && pSettingSigValue->pStdSig->iSigID <= 443) ||
			   (pEquipInfo->pStdEquip->iTypeID == 300 && pSettingSigValue->pStdSig->iSigID >= 201 && pSettingSigValue->pStdSig->iSigID <= 413))
			{
				pSettingSigValue++;
				continue;
			}
			//if(SIG_VALUE_NEED_DISPLAY(&pSettingSigValue->bv)&&STD_SIG_IS_DISPLAY_ON_UI(pSettingSigValue->pStdSig,DISPLAY_WEB))
			//{
			//OSetting( equipId,signalId,signalName,valueType,valueState) 
				iLen += sprintf(szBuf + iLen, "new OSetting( %4d,%4d,\"%s\",%d,%d),\n",
										pEquipInfo->iEquipID,
										pSettingSigValue->pStdSig->iSigID,
										pSettingSigValue->pStdSig->pSigName->pAbbrName[iLanguage],
										pSettingSigValue->bv.ucType,
										SIG_VALUE_IS_VALID(&pSettingSigValue->bv));
			//}
			pSettingSigValue++;
		}
	}

    pSearchValue = strrchr(szBuf, 44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }
	
	if(iReturn > 0)
	{
		pszID = "equip_setting_signals";
        pszMakeVar = MakeVarField(pszID);
        if(pszMakeVar != NULL)
        {
            ReplaceString(&pHtml, pszMakeVar, szBuf);
            DELETE(pszMakeVar);
            pszMakeVar = NULL;
        }
	}
	szBuf[0] = ';';
	strcat(pResult, szBuf);
	TRACE("\n\n*****%s-%d, Setting Handling is OK! Length of pResult is %d, iLen = %d******\n\n\n", 
		__FUNCTION__, __LINE__,
		strlen(pResult), iLen);

	//Alarm Info Handling
	memset(szBuf, 0, MAX_BUFFER * 2);
	iLen = 0;
	szBuf[iLen++] = '\t';

	pEquipInfo = pEquip;
	//char szExchange[256];
	//FILE *fFP = fopen("/app/Alarm.list","wb") ;

	for(i = 0; i < iEquipNum; i++, pEquipInfo++)
    {
	    if( pEquipInfo->pStdEquip->iTypeID == 1101 ||
		    pEquipInfo->pStdEquip->iTypeID == 1600 || pEquipInfo->pStdEquip->iTypeID == 1601 ||
		    pEquipInfo->pStdEquip->iTypeID == 1700 || pEquipInfo->pStdEquip->iTypeID == 1701 ||
		    pEquipInfo->pStdEquip->iTypeID == 1800 || pEquipInfo->pStdEquip->iTypeID == 1801 ||
		    (pEquipInfo->pStdEquip->iTypeID >= 1900 && pEquipInfo->pStdEquip->iTypeID <= 1908) ||
		    pEquipInfo->pStdEquip->iTypeID == 1300 || pEquipInfo->pStdEquip->iTypeID == 1301 ||
		    pEquipInfo->pStdEquip->iTypeID == 1400 || pEquipInfo->pStdEquip->iTypeID == 1401 ||
		    pEquipInfo->pStdEquip->iTypeID == 2000 || pEquipInfo->pStdEquip->iTypeID == 2001 ||
		    pEquipInfo->pStdEquip->iTypeID == 305 || pEquipInfo->pStdEquip->iTypeID == 306 ||
		    pEquipInfo->pStdEquip->iTypeID == 2200 || pEquipInfo->pStdEquip->iTypeID == 2201 ||
		    pEquipInfo->pStdEquip->iTypeID == 2202 || pEquipInfo->pStdEquip->iTypeID == 2203 ||
		    pEquipInfo->pStdEquip->iTypeID == 2204 || pEquipInfo->pStdEquip->iTypeID == 2205 ||
		    pEquipInfo->pStdEquip->iTypeID == 2206 || pEquipInfo->pStdEquip->iTypeID == 2207 ||
		    pEquipInfo->pStdEquip->iTypeID == 2208 || pEquipInfo->pStdEquip->iTypeID == 504 ||
		    pEquipInfo->pStdEquip->iTypeID == 308 || pEquipInfo->pStdEquip->iTypeID == 309 ||
		    pEquipInfo->pStdEquip->iTypeID == 2400 || pEquipInfo->pStdEquip->iTypeID == 2401 ||
		    pEquipInfo->pStdEquip->iTypeID == 2500 || pEquipInfo->pStdEquip->iTypeID == 2501 ||
		    pEquipInfo->pStdEquip->iTypeID == 403)
	    {
		    //pEquipInfo++;
		    continue;
	    }
		iAlarmNum = Web_GetAlarmSigStruofEquip(pEquipInfo->iEquipID, &pAlarmSigInfo);
		for(j = 0; j < iAlarmNum; j++)
		{
			if((pEquipInfo->pStdEquip->iTypeID == 100 && pAlarmSigInfo->iSigID >= 201 && pAlarmSigInfo->iSigID <= 413) ||
				(pEquipInfo->pStdEquip->iTypeID == 300 && pAlarmSigInfo->iSigID >= 51 && pAlarmSigInfo->iSigID <= 263))
			{
				pAlarmSigInfo++;
				continue;
			}
			//OAlarm( equipId,signalId,signalName,valueType,valueState) 
			iLen += sprintf(szBuf + iLen, "new OAlarm( %4d,%4d,\"%s\",5,1),\n",
                               pEquipInfo->iEquipID,
                               pAlarmSigInfo->iSigID,
                               pAlarmSigInfo->pSigName->pAbbrName[iLanguage]);
			
			/*sprintf(szExchange, "%s\t\t%s\t\t %d\n", pEquipInfo->pEquipName->pFullName[0], pAlarmSigInfo->pSigName->pFullName[0], 
				DXI_MERGE_UNIQUE_SIG_ID(
				pEquipInfo->iEquipID, 
				SIG_TYPE_ALARM, 
				pAlarmSigInfo->iSigID));
			fwrite(szExchange, strlen(szExchange), 1, fFP);*/


			pAlarmSigInfo++;
			
		}
	}
	//fclose(fFP);

    pSearchValue = strrchr(szBuf, 44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }

	if(iReturn > 0)
	{
        pszID = "equip_alarm_signals";
        pszMakeVar = MakeVarField(pszID);
        if(pszMakeVar != NULL)
        {
            ReplaceString(&pHtml, pszMakeVar, szBuf);
            DELETE(pszMakeVar);
            pszMakeVar = NULL;
        }
    }

	szBuf[0] = ';';
	strcat(pResult, szBuf);
	TRACE("\n\n*****%s-%d, Alarm Handling is OK! Length of pResult is %d, iLen = %d******\n\n\n", 
		__FUNCTION__, __LINE__,
		strlen(pResult), iLen);

    //PLC Info...
	pSearchValue = strrchr(pszPLCInfo,44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }
	
	strcat(pResult,pszPLCInfo);
	TRACE("\n\n*****%s-%d, PLCInfo Handling is OK! Length of pResult is %d, iLen = %d******\n\n\n", 
		__FUNCTION__, __LINE__,
		strlen(pResult), iLen);
	///////////////////////
    *ppszReturn = pResult;

    char *szFileVar = NULL;

    if(iLanguage == 0)
    {
        szFileVar = "/var/eng/p40_cfg_plc_Popup.htm";
    }
    else if(iLanguage == 1)
    {
        szFileVar = "/var/loc/p40_cfg_plc_Popup.htm";
    }
    else
    {
        szFileVar = "/var/loc2/p40_cfg_plc_Popup.htm";
    }

    if(szFileVar 
		&& (fp = fopen(szFileVar,"wb")) 
		&& pHtml)
    {

        fwrite(pHtml, strlen(pHtml), 1, fp);
        fclose(fp);
    }

    if(szFile)
    {
        DELETE(szFile);
        szFile = NULL;
    }
    if(pHtml)
    {
        DELETE(pHtml);
        pHtml = NULL;
    }

	//DELETE(szBuf);
    DELETE(pszPLCInfo);

    return TRUE;
}


/*==========================================================================*
* FUNCTION :  Web_LoadPlcConfigFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_SendConfigureData 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_LoadPlcConfigFile(char **ppszReturn)
{    
    char *szFullPath = WEB_PLC_CFG_FILE_PATH;
    int i = 0;
    int iLen = 0;
    WEB_PLC_CFG_INFO_LINE *temp = NULL;

    WEB_PLC_CFC_INFO *stPLCConfigInfo = NULL;
    stPLCConfigInfo = NEW(WEB_PLC_CFC_INFO,1);
    
    char *pszResult = NULL;
    pszResult = NEW(char, MAX_LINE_SIZE*100);
    memset(pszResult,0,MAX_LINE_SIZE*100);

    if (pszResult == NULL || stPLCConfigInfo == NULL)
    {
        SAFELY_DELETE(pszResult);
        SAFELY_DELETE(stPLCConfigInfo);
        return FALSE;
    }

    if (Mutex_Lock(GetMutexLoadPLCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
        == ERR_MUTEX_OK)
    {

        if(Cfg_LoadConfigFile(szFullPath,LoadPlcConfigFile,stPLCConfigInfo)!=0)
        {
            TRACEX("____Cfg_LoadConfigFile error!_______\n");
            Mutex_Unlock(GetMutexLoadPLCConfig());
            TRACEX("____stPLCConfigInfo->iNumber[%d]_______\n",stPLCConfigInfo->iNumber);
            if(stPLCConfigInfo->iNumber>0)
            {
                TRACEX("____stPLCConfigInfo->iNumber[%d]_______\n",stPLCConfigInfo->iNumber);
                TRACEX("____Cfg_LoadConfigFile error2!_______\n");
                temp = stPLCConfigInfo->stWebPLCConfigLine;
                for(i = 0; i < stPLCConfigInfo->iNumber; i++)
                {
                    TRACEX("____Cfg_LoadConfigFile error3!_______\n");
                    SAFELY_DELETE(temp->pszOperator);
                    SAFELY_DELETE(temp->pszInput1);
                    SAFELY_DELETE(temp->pszInput2);
                    SAFELY_DELETE(temp->pszParam1);
                    SAFELY_DELETE(temp->pszParam2);
                    SAFELY_DELETE(temp->pszOutput);

                    temp++; 
                }

                SAFELY_DELETE(stPLCConfigInfo->stWebPLCConfigLine);
            }
            SAFELY_DELETE(stPLCConfigInfo);
            return -1;
        }

        Mutex_Unlock(GetMutexLoadPLCConfig());

    }


     //TRACE("Web_LoadPlcConfigFile OK!!!\n");
     pszResult[iLen++] = ';';
     
     temp = stPLCConfigInfo->stWebPLCConfigLine;
     if(stPLCConfigInfo->iNumber > 0)
     {
         for(i = 0; i < stPLCConfigInfo->iNumber; i++)
         {
             iLen += sprintf(pszResult + iLen,"\"%s\t%s\t%s\t%s\t%s\t%s\",\n",temp->pszOperator,
                 temp->pszInput1,
                 temp->pszInput2,
                 temp->pszParam1,
                 temp->pszParam2,
                 temp->pszOutput);
             temp++;
         }

     }
     else
     {
            iLen += sprintf(pszResult + iLen,"\t\t\t\t\t\t\n");
     }
     

     *ppszReturn = pszResult;

     /*FILE *pf = NULL;
     pf = fopen("/var/wj1.txt","w+");
     fwrite(pszResult,strlen(pszResult),1,pf);
     fclose(pf);*/

     
     temp = stPLCConfigInfo->stWebPLCConfigLine;
     for(i = 0; i < stPLCConfigInfo->iNumber; i++)
     {
         DELETE(temp->pszOperator);
         DELETE(temp->pszInput1);
         DELETE(temp->pszInput2);
         DELETE(temp->pszParam1);
         DELETE(temp->pszParam2);
         DELETE(temp->pszOutput);

         temp++; 
     }

     DELETE(stPLCConfigInfo->stWebPLCConfigLine);
     DELETE(stPLCConfigInfo);

     return iLen+1;

}

/*==========================================================================*
* FUNCTION :  LoadPlcConfigFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int LoadPlcConfigFile(IN void *pCfg, OUT void *pLoadToBuf)
{
    //TRACE("Into LoadPlcConfigFile!!!\n");
    CONFIG_TABLE_LOADER loader[1];
    WEB_PLC_CFC_INFO *pBuf = NULL;
    pBuf = (WEB_PLC_CFC_INFO *)pLoadToBuf;

   

    DEF_LOADER_ITEM(&loader[0],
        NULL,
        &(pBuf->iNumber), 
        PLC_CMD,
        &(pBuf->stWebPLCConfigLine), 
        ParsePLCTableProc);

    Cfg_LoadTables(pCfg, 1, loader);

    //TRACE("LoadPlcConfigFile OK!!!\n");

    return 0;

}

/*==========================================================================*
* FUNCTION :  ParsePLCTableProc
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int ParsePLCTableProc(IN char *szBuf, OUT WEB_PLC_CFG_INFO_LINE *pStructData)
{
    //TRACE("Into ParsePLCTableProc!!!\n");
//# Operator	Input1		Input2		Param1		Param2		Output
    char *pField;

    ASSERT(szBuf);
    ASSERT(pStructData);


    //TRACE("INTO ParsePLCTableProc!!!!\n");

    /* 1.Operator field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->pszOperator = NEW_strdup(pField);
    }
    else
    {
        return 1;
    }
    //TRACE("Operator field OK!!!%s\n", pStructData->pszOperator);
    /* 2.Input1 field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->pszInput1 = NEW_strdup(pField);
    }
    else
    {
        return 2;
    }
    //TRACE("Input1 field OK!!!%s\n",pStructData->pszInput1);
    /* 3.Input2 field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->pszInput2 = NEW_strdup(pField);
    }
    else
    {
        return 3;
    }
    //TRACE("Input2 field OK!!!%s\n",pStructData->pszInput2);
    /* 4.Param1 field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->pszParam1 = NEW_strdup(pField);
    }
    else
    {
        return 4;
    }
    //TRACE("Param1 field OK!!!%s\n",pStructData->pszParam1);
    /* 5.Param2 field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->pszParam2 = NEW_strdup(pField);
    }
    else
    {
        return 5;
    }
    //TRACE("Param2 field OK!!!%s\n",pStructData->pszParam2);
    /* 6.Output field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->pszOutput = NEW_strdup(pField);
    }
    else
    {
        return 6;
    }
    //TRACE("Output field OK!!!%s\n",pStructData->pszOutput);

    //TRACE("ParsePLCTableProc OK!!!\n");

    return 0;


    
}

/*==========================================================================*
* FUNCTION :  GetMutexLoadPLCConfig
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/

HANDLE GetMutexLoadPLCConfig(void)
{
    return s_hMutexLoadPLCCFGFile;
}

/*==========================================================================*
* FUNCTION :  GetMutexLoadAlarmConfig
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/

HANDLE GetMutexLoadAlarmConfig(void)
{
    return s_hMutexLoadAlarmCFGFile;
}

/*==========================================================================*
* FUNCTION :  AddStringToEndOfFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static BOOL AddStringToEndOfFile( IN FILE* pFile, IN int nWriteLen, IN char* pString)
{
    TRACE("INTO AddStringToEndOfFile\n");
    BOOL bRetFlag = TRUE;

    if (pFile != NULL)
    {
        fseek(pFile, 0, SEEK_END);

        fwrite(pString, sizeof(char), (size_t)nWriteLen, pFile);
    }
    else
    {
        bRetFlag = FALSE;
    }

    return bRetFlag;
}

/*==========================================================================*
* FUNCTION : Web_WritePLCCFGInfo
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_WritePLCCFGInfo(IN char *pszFileLineBuf)
{
    TRACE("INTO Web_WritePLCCFGInfo\n");

    char *szFullPath = WEB_PLC_CFG_FILE_PATH;
    char szWriteString[512];
    int iresult = 0;
    FILE *pFile = NULL;

    sprintf(szWriteString,"%s%s",
            SPLITTER_LINE,
            pszFileLineBuf);

    
    if (Mutex_Lock(GetMutexLoadPLCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
        == ERR_MUTEX_OK)
    {
        pFile = fopen(szFullPath, "r+");

        if (pFile == NULL)
        {
            AppLogOut("WEB", APP_LOG_ERROR, 
                "[%s]--Web_WritePLCCFGInfo: ERROR: Can not open the "
                "file: %s!\n", __FILE__, szFullPath);

            Mutex_Unlock(GetMutexLoadPLCConfig());

            return 4;

        }
        fclose(pFile);

        pFile = fopen(szFullPath, "a+");
        iresult = AddStringToEndOfFile(pFile,strlen(szWriteString),szWriteString);

        fclose(pFile);

        Mutex_Unlock(GetMutexLoadPLCConfig());
    }

    if(iresult == 1)
    {
        return 6;
    }
    else
    {
        return 5;
    }
}

/*==========================================================================*
* FUNCTION :  Web_DeletePLCCFGInfo
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/

static int Web_DeletePLCCFGInfo(IN char *pszFileLineBuf)
{
    FILE    *pFile         = NULL;
    char    *pszFile       = NULL;
    char    *pszOutFile    = NULL;
    void    *pProf         = NULL;
    long    lFileLen       = 0;
    long    lCurrPos       = 0;
    long    lFindPos       = 0;
    char    pLineBuf[MAX_LINE_SIZE];
    int     iLen           = 0;

    memset(pLineBuf,0,MAX_LINE_SIZE);

    WEB_PLC_CFG_INFO_LINE *pLineData = NULL;
    pLineData = NEW(WEB_PLC_CFG_INFO_LINE,1);
    if (pLineData == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_DeletePLCCFGInfo: "
            "ERROR: There is no enough memory! ", 
            __FILE__);

        return 5;
    }

    if(ParsePLCTableProc(pszFileLineBuf,pLineData)!=0)
    {
        SAFELY_DELETE(pLineData);
        return 5;
    }

    WEB_PLC_CFG_INFO_LINE *pTempData = NULL;
    pTempData = NEW(WEB_PLC_CFG_INFO_LINE,1);
    if (pTempData == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_DeletePLCCFGInfo: "
            "ERROR: There is no enough memory! ", 
            __FILE__);

        return 5;
    }


    if (Mutex_Lock(GetMutexLoadPLCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
        == ERR_MUTEX_OK)
    {
        pFile = fopen(WEB_PLC_CFG_FILE_PATH,"r+");
        if(pFile == NULL)
        {
            SAFELY_DELETE(pLineData);
            SAFELY_DELETE(pTempData);
            Mutex_Unlock(GetMutexLoadPLCConfig());
            return 5;
        }

        lFileLen = GetFileLength(pFile);
        TRACE("FileLen : %ld\n",lFileLen);

        pszFile = NEW(char, lFileLen + 1);
        if (pszFile == NULL)
        {
            AppLogOut("WEB", APP_LOG_ERROR, 
                "[%s]--Web_DeletePLCCFGInfo: "
                "ERROR: There is no enough memory! ", 
                __FILE__);
            fclose(pFile);
            Mutex_Unlock(GetMutexLoadPLCConfig());
            return 5;
        }
        memset(pszFile, 0, lFileLen + 1);

        pszOutFile = NEW(char, lFileLen + 1);
        if (pszOutFile == NULL)
        {
            AppLogOut("WEB", APP_LOG_ERROR, 
                "[%s]--Web_DeletePLCCFGInfo: "
                "ERROR: There is no enough memory! ", 
                __FILE__);
            fclose(pFile);
            Mutex_Unlock(GetMutexLoadPLCConfig());
            return 5;
        }
        memset(pszOutFile, 0, lFileLen + 1);

        fread(pszFile, 1, (size_t)lFileLen, pFile);
        fclose(pFile);

        pProf = Cfg_ProfileOpen(pszFile,lFileLen);
        if(Cfg_ProfileFindSection(pProf,PLC_CMD) == 0)
        {
            AppLogOut("WEB", APP_LOG_ERROR, 
                "[%s]--Web_DeletePLCCFGInfo: "
                "ERROR: Can't find Section! ", 
                __FILE__);      
            DELETE(pProf);
            Mutex_Unlock(GetMutexLoadPLCConfig());
            return 5;
        }

        lCurrPos = Cfg_ProfileTell(pProf);
        TRACE("lCurrPos : %ld\n",lCurrPos);

        memmove(pszOutFile, pszFile, lCurrPos);
        int flage = 0;
        while(Cfg_ProfileReadLine(pProf, pLineBuf, MAX_LINE_SIZE) != 0)
        {
            if(pLineBuf[0] == '#')
            {
                
                iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
            }
            else
            {
               if(ParsePLCTableProc(pLineBuf,pTempData)!=0)
                {
                    continue;
                }
                if(!strcmp(pTempData->pszOperator, pLineData->pszOperator) &&
                !strcmp(pTempData->pszInput1, pLineData->pszInput1) &&
                !strcmp(pTempData->pszInput2, pLineData->pszInput2) &&
                !strcmp(pTempData->pszParam1, pLineData->pszParam1) &&
                !strcmp(pTempData->pszParam2, pLineData->pszParam2) &&
                !strcmp(pTempData->pszOutput, pLineData->pszOutput) &&
                flage == 0)
                {
                    DELETE(pTempData->pszOperator);
                    DELETE(pTempData->pszInput1);
                    DELETE(pTempData->pszInput2);
                    DELETE(pTempData->pszParam1);
                    DELETE(pTempData->pszParam2);
                    DELETE(pTempData->pszOutput);
                    flage = 1;
                    continue;
                }
                else
                {
                    iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s\t\t%s\t\t%s\t\t%s\t\t%s\t\t%s", 
                                    pTempData->pszOperator,
                                    pTempData->pszInput1,
                                    pTempData->pszInput2,
                                    pTempData->pszParam1,
                                    pTempData->pszParam2,
                                    pTempData->pszOutput);
                    DELETE(pTempData->pszOperator);
                    DELETE(pTempData->pszInput1);
                    DELETE(pTempData->pszInput2);
                    DELETE(pTempData->pszParam1);
                    DELETE(pTempData->pszParam2);
                    DELETE(pTempData->pszOutput);
                }
            }
        }

        
        pFile = fopen(WEB_PLC_CFG_FILE_PATH,"w+");
        fwrite(pszOutFile,strlen(pszOutFile),1,pFile);
        fclose(pFile);

        Mutex_Unlock(GetMutexLoadPLCConfig());
    }

    DELETE(pLineData->pszOperator);
    pLineData->pszOperator = NULL;
    DELETE(pLineData->pszInput1);
    pLineData->pszInput1 = NULL;
    DELETE(pLineData->pszInput2);
    pLineData->pszInput2 = NULL;
    DELETE(pLineData->pszParam1);
    pLineData->pszParam1 = NULL;
    DELETE(pLineData->pszParam2);
    pLineData->pszParam2 = NULL;
    DELETE(pLineData->pszOutput);
    pLineData->pszOutput = NULL;


    DELETE(pLineData);
    pLineData = NULL;
    DELETE(pTempData);
    pTempData = NULL;
    DELETE(pszFile);
    pszFile = NULL;
    DELETE(pszOutFile);
    pszOutFile = NULL;
    DELETE(pProf);


    return 6;
}

/*==========================================================================*
* FUNCTION :  Web_MakeAlarmSupExpWebPageBuf
* PURPOSE  :  Get OStdEquip��ODispAlarmItem��ORelEquip��ORelEquipAlarms
* CALLS    : 
* CALLED BY:  Web_SendConfigureData 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/

//��ȡOStdEquip��ODispAlarmItem��ORelEquip��ORelEquipAlarms
static int Web_MakeAlarmSupExpWebPageBuf(IN char *pszBuf,OUT char **ppszReturn,IN int iLanguage)
{

    int istdEquipTypeId;
    char *pszStdEquipBuf = NULL;//new OStdEquip( equipTypeId, equipTypeName),... 
    char *pszDispAlarmItemsBuf = NULL;//new ODispAlarmItem( signalId,signalName,suppressExp),... 
    char *pszRelEquip = NULL;//new ORelEquip( equipId, equipName, sequnenceNo),... 
    char *pszRelEquipAlarms = NULL;//new ORelEquipAlarms( equipId,signalId,signalName),... 
    char *pszEquipTypeName = NULL;//StdEquipTypeName

    EQUIP_INFO *pszEquipInfo = NULL;
    EQUIP_INFO *pszEquipInfo2 = NULL;
    char *pSearchValue = NULL;
    int iLen = 0;//ORelEquip
    int iLen2 = 0;//ORelEquipAlarms
    int iLen3 = 0;//OStdEquip
    int iLen4 = 0;//ODispAlarmItem
    int iLen5 = 0;//StdEquipTypeName
    int iEquipIdTemp;
    int i;
    int i2;
    char szEquipIdTemp[5];
    char *pszStdEquipFileName = NULL;
    int *pRelEquip = NULL;
    ALARM_SIG_INFO *pAlarmSigInfoTemp = NULL; 

    //pszBuf = Web_RemoveWhiteSpace(pszBuf);
    strncpyz(szEquipIdTemp,pszBuf,5);
    istdEquipTypeId = atoi(szEquipIdTemp);


    Web_GetStdEquipFileName(istdEquipTypeId,&pszStdEquipFileName);
    //TRACE("%s\n",pszStdEquipFileName);


    iLen3 = Web_GetStdEquipBuf(&pszStdEquipBuf, iLanguage);
    //TRACE("%s\n",pszStdEquipBuf);

    Web_GetEquipInfoByTypeId(istdEquipTypeId, &pszEquipInfo);
	pszEquipTypeName = pszEquipInfo->pStdEquip->pTypeName->pFullName[iLanguage];
	iLen5 = strlen(pszEquipTypeName);

	//TRACE("____StdEquipTypeName:%s_______\n",pszEquipTypeName);

	//TRACE("____StdEquipRelevantEquipListLength:%d_______\n",pszEquipInfo->iRelevantEquipListLength);

    if(pszEquipInfo->pStdEquip->iAlarmSigNum != 0)
    {
        iLen4 = Web_GetDispAlarmItemsBuf(istdEquipTypeId, pszStdEquipFileName,iLanguage, &pszDispAlarmItemsBuf);
        //TRACE("%s\n",pszDispAlarmItemsBuf);
		
		if(pszEquipInfo->pRelevantEquipList[0] != -1)
		{
			pszRelEquip = NEW(char,pszEquipInfo->iRelevantEquipListLength*MAX_ITEM_BUF);
			pszRelEquip[iLen++] = '\t';
			pRelEquip = pszEquipInfo->pRelevantEquipList;

			pszRelEquipAlarms = NEW(char,pszEquipInfo->iRelevantEquipListLength*MAX_ITEM_BUF*100);//The MAX NUM of alarm sig is 100
			pszRelEquipAlarms[iLen2++] = '\t';

			for(i = 0;i < pszEquipInfo->iRelevantEquipListLength; i++)
			{
				iEquipIdTemp = *pRelEquip;
				if(iEquipIdTemp == -1)
				{
					continue;
				}
				Web_GetEquipInfoById(iEquipIdTemp, &pszEquipInfo2);
				iLen += sprintf(pszRelEquip + iLen, "new ORelEquip( %4d,\"%s\",%2d),\n",
					pszEquipInfo2->iEquipID,
					pszEquipInfo2->pStdEquip->pTypeName->pFullName[iLanguage],
					i+1);
				pAlarmSigInfoTemp = pszEquipInfo2->pStdEquip->pAlarmSigInfo;
				for(i2 = 0;i2 < pszEquipInfo2->pStdEquip->iAlarmSigNum; i2++)
				{

					iLen2 += sprintf(pszRelEquipAlarms + iLen2, "new ORelEquipAlarms( %4d,%3d,\"%s\"),\n",
						pszEquipInfo2->iEquipID,
						pAlarmSigInfoTemp->iSigID,
						pAlarmSigInfoTemp->pSigName->pFullName[iLanguage]);
					pAlarmSigInfoTemp++;
				}

				pRelEquip++;


			}


			Web_GetEquipInfoByTypeId(istdEquipTypeId, &pszEquipInfo2);
			iLen += sprintf(pszRelEquip + iLen, "new ORelEquip( %4d,\"%s\",%2d),\n",
				pszEquipInfo2->iEquipID,
				pszEquipInfo2->pStdEquip->pTypeName->pFullName[iLanguage],
				0);
			pAlarmSigInfoTemp = pszEquipInfo2->pStdEquip->pAlarmSigInfo;
			for(i2 = 0;i2 < pszEquipInfo2->pStdEquip->iAlarmSigNum; i2++)
			{

				iLen2 += sprintf(pszRelEquipAlarms + iLen2, "new ORelEquipAlarms( %4d,%3d,\"%s\"),\n",
					pszEquipInfo2->iEquipID,
					pAlarmSigInfoTemp->iSigID,
					pAlarmSigInfoTemp->pSigName->pFullName[iLanguage]);
				pAlarmSigInfoTemp++;
			}






			pSearchValue = strrchr(pszRelEquip,44);  //get the last ','
			if(pSearchValue != NULL)
			{
				*pSearchValue = 32;   //replace ',' with ' '
			}

			pSearchValue = strrchr(pszRelEquipAlarms,44);  //get the last ','
			if(pSearchValue != NULL)
			{
				*pSearchValue = 32;   //replace ',' with ' '
			}

			//TRACE("%s\n",pszRelEquip);
			//TRACE("%s\n",pszRelEquipAlarms);

		}
		else
		{

			pszRelEquip = NEW(char, MAX_ITEM_BUF+1);
			memset(pszRelEquip, 0, MAX_ITEM_BUF+1);
			pszRelEquip[iLen++] = '\t';

			pszRelEquipAlarms = NEW(char, MAX_ITEM_BUF*100);
			memset(pszRelEquipAlarms, ' ', MAX_ITEM_BUF*100);
			pszRelEquipAlarms[iLen2++] = '\t';


			Web_GetEquipInfoByTypeId(istdEquipTypeId, &pszEquipInfo2);
			iLen += sprintf(pszRelEquip + iLen, "new ORelEquip( %4d,\"%s\",%2d),\n",
				pszEquipInfo2->iEquipID,
				pszEquipInfo2->pStdEquip->pTypeName->pFullName[iLanguage],
				0);
			pAlarmSigInfoTemp = pszEquipInfo2->pStdEquip->pAlarmSigInfo;
			for(i2 = 0;i2 < pszEquipInfo2->pStdEquip->iAlarmSigNum; i2++)
			{

				iLen2 += sprintf(pszRelEquipAlarms + iLen2, "new ORelEquipAlarms( %4d,%3d,\"%s\"),\n",
					pszEquipInfo2->iEquipID,
					pAlarmSigInfoTemp->iSigID,
					pAlarmSigInfoTemp->pSigName->pFullName[iLanguage]);
				pAlarmSigInfoTemp++;
			}


			pSearchValue = strrchr(pszRelEquip,44);  //get the last ','
			if(pSearchValue != NULL)
			{
				*pSearchValue = 32;   //replace ',' with ' '
			}

			pSearchValue = strrchr(pszRelEquipAlarms,44);  //get the last ','
			if(pSearchValue != NULL)
			{
				*pSearchValue = 32;   //replace ',' with ' '
			}
		}


    }
    else
    {
        pszDispAlarmItemsBuf = NEW(char, MAX_ITEM_BUF+1);
        memset(pszDispAlarmItemsBuf, ' ', MAX_ITEM_BUF+1);
        iLen4 = MAX_ITEM_BUF;
        pszDispAlarmItemsBuf[MAX_ITEM_BUF] = '\0';

		pszRelEquip = NEW(char, MAX_ITEM_BUF+1);
		memset(pszRelEquip, ' ', MAX_ITEM_BUF+1);
		iLen = MAX_ITEM_BUF;
		pszRelEquip[iLen++] = '\0';

		pszRelEquipAlarms = NEW(char, MAX_ITEM_BUF+1);
		memset(pszRelEquipAlarms, ' ', MAX_ITEM_BUF+1);
		iLen2 = MAX_ITEM_BUF;
		pszRelEquipAlarms[iLen2++] = '\0';


    }
    

    char *pResult = NEW(char,iLen + iLen2 + iLen3 + iLen4 + iLen5 + 6 );
    if (pResult == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_MakeAlarmSupExpWebPageBuf: "
            "ERROR: There is no enough memory! ", 
            __FILE__);
        return ERR_NO_MEMORY;
    }
    memset(pResult,0,iLen + iLen2 + iLen3 + iLen4 + iLen5 + 6);

    strcpy(pResult,pszEquipTypeName);//StdEquipTypeName
    pszStdEquipBuf[0] = ';';
    strcat(pResult,pszStdEquipBuf);//OStdEquip
    pszDispAlarmItemsBuf[0] = ';';
    strcat(pResult,pszDispAlarmItemsBuf);//ODispAlarmItem
    pszRelEquip[0] = ';';
    strcat(pResult,pszRelEquip);//ORelEquip
    pszRelEquipAlarms[0] = ';';
    strcat(pResult,pszRelEquipAlarms);//ORelEquipAlarms

    TRACE("Web_MakeAlarmSupExpWebPageBuf:%s\n",pResult);



    *ppszReturn = pResult;

    SAFELY_DELETE(pszStdEquipBuf);
    SAFELY_DELETE(pszRelEquip);
    SAFELY_DELETE(pszRelEquipAlarms);
    SAFELY_DELETE(pszDispAlarmItemsBuf);

    return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_GetStdEquipFileName
* PURPOSE  :  Get StdEquipTypeName used StdEquipID
* CALLS    : 
* CALLED BY:  Web_MakeAlarmSupExpWebPageBuf 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
//���ݱ�׼�豸ID��ȡ��׼�����ļ���
static int Web_GetStdEquipFileName(IN int istdEquipTypeId, OUT char **ppszStdEquipFileName)
{
    STDEQUIP_TYPEMAP_INFO *pStdEquipTypeMap = NULL;
    int iEquipMapInfoNum;
    int iBufLen = 0;
    char *pszFileName = NULL;
    int i;
    int iError1 = 0;
    int iError2 = 0;


    iError1 = DxiGetData(VAR_STD_EQUIPTYPE_MAP_INFO,
                0, 
                0, 
                &iBufLen, 
                &pStdEquipTypeMap,
                0);

    iError2 = DxiGetData(VAR_STD_EQUIPTYPE_MAP_NUM,
                0, 
                0, 
                &iBufLen, 
                (void *)&iEquipMapInfoNum,
                0);

    if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
    {

        return FALSE;
    }

    for(i = 0;i < iEquipMapInfoNum; i++, pStdEquipTypeMap++)
    {

        if (!pStdEquipTypeMap)
        {

            TRACE("\n\r filename: data_exchange.c [GetEquipInfoByEquipID]\
                  pEquipInfo is not valid ");


            return ERR_DXI_INVALID_EQUIP_BUFFER;
        }
        if (pStdEquipTypeMap->iEquipTypeID == istdEquipTypeId)
        {
            break;
        }
    }
    
    *ppszStdEquipFileName = pStdEquipTypeMap->szCfgFileName;

    return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_GetStdEquipBuf
* PURPOSE  :  Get new OStdEquip( equipTypeId, equipTypeName),...
* CALLS    : 
* CALLED BY:  Web_MakeAlarmSupExpWebPageBuf 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
//��ȡnew OStdEquip( equipTypeId, equipTypeName),...
static int Web_GetStdEquipBuf(OUT char **ppszStdEquipBuf, IN int iLanguage)
{


    STDEQUIP_TYPE_INFO  *pStdEquipTypeInfo = NULL;
    STDEQUIP_TYPE_INFO  *pTempOfStdEquipTypeInfo = NULL;
    int istdEquipTypeNum;
    char *pszReturnBuf = NULL;
    char *pSearchValue = NULL;
    int i;
    int iBufLen;
    int iBufLenTemp = 0;

    /*get stdequip Info*/
    int iError1 = DxiGetData(VAR_STD_EQUIPS_LIST,
        0,			
        0,		
        &iBufLen,			
        &pStdEquipTypeInfo,			
        0);

    /*get stdequip number*/
    int iError2 = DxiGetData(VAR_STD_EQUIPS_NUM,
        0,			
        0,		
        &iBufLen,			
        (void *)&istdEquipTypeNum,			
        0);

    if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
    {

        return FALSE;
    }


    pszReturnBuf = NEW(char,istdEquipTypeNum * MAX_ITEM_BUF); 
    memset(pszReturnBuf,0,(size_t)(istdEquipTypeNum * MAX_ITEM_BUF));

    pszReturnBuf[iBufLenTemp++] = '\t';

    
    pTempOfStdEquipTypeInfo = pStdEquipTypeInfo;
    for(i = 0; i < istdEquipTypeNum; i++, pTempOfStdEquipTypeInfo++)
    {
        //new OStdEquip( equipTypeId, equipTypeName),... 
        iBufLenTemp += sprintf(pszReturnBuf + iBufLenTemp, "new OStdEquip( %4d,\"%s\"),\n",
                                pTempOfStdEquipTypeInfo->iTypeID,
                                pTempOfStdEquipTypeInfo->pTypeName->pFullName[iLanguage]);
    }

    pSearchValue = strrchr(pszReturnBuf,44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }

    *ppszStdEquipBuf = pszReturnBuf;


    return iBufLenTemp;
}

/*==========================================================================*
* FUNCTION :  Web_GetEquipInfoByTypeId
* PURPOSE  :  Get A EQUIP_INFO struct point used StdEquipID
* CALLS    : 
* CALLED BY:  Web_MakeAlarmSupExpWebPageBuf 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
//���ݱ�׼�豸����ID��ȡ����һ���豸�ṹ��ָ��
static int Web_GetEquipInfoByTypeId(IN int iEquipTypeId, OUT EQUIP_INFO **ppszEquipInfo)
{

    EQUIP_INFO *pEquipInfo = NULL;
    EQUIP_INFO *pTempEquipInfo = NULL;
    int iEquipNum;
    int iBufLen;
    int i;

    /*get equip information*/
    int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
        0,			
        0,		
        &iBufLen,			
        &pEquipInfo,			
        0);

    /*get equip number*/
    int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
        0,			
        0,		
        &iBufLen,			
        (void *)&iEquipNum,			
        0);

    if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
    {

        return FALSE;
    }

    pTempEquipInfo = pEquipInfo;
    for(i = 0; i< iEquipNum; i++, pTempEquipInfo++)
    {
        if(pTempEquipInfo->iEquipTypeID == iEquipTypeId)
        {
            break;
        }
    }

    *ppszEquipInfo = pTempEquipInfo;

    return TRUE;

}
/*==========================================================================*
* FUNCTION :  Web_GetEquipInfoById
* PURPOSE  :  Get A EQUIP_INFO struct point used EquipID
* CALLS    : 
* CALLED BY:  Web_MakeAlarmSupExpWebPageBuf 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
//�����豸ID��ȡ�豸�ṹ��ָ��
static int Web_GetEquipInfoById(IN int iEquipId, OUT EQUIP_INFO **ppszEquipInfo)
{

    EQUIP_INFO *pEquipInfo = NULL;
    EQUIP_INFO *pTempEquipInfo = NULL;
    int iEquipNum;
    int iBufLen;
    int i;

    /*get equip information*/
    int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
        0,			
        0,		
        &iBufLen,			
        &pEquipInfo,			
        0);

    /*get equip number*/
    int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
        0,			
        0,		
        &iBufLen,			
        (void *)&iEquipNum,	
        0);

    if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
    {

        return FALSE;
    }

    pTempEquipInfo = pEquipInfo;
    for(i = 0; i< iEquipNum; i++, pTempEquipInfo++)
    {
        if(pTempEquipInfo->iEquipID == iEquipId)
        {
            break;
        }
    }

    *ppszEquipInfo = pTempEquipInfo;

    return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_GetDispAlarmItemsBuf
* PURPOSE  :  Get new ODispAlarmItem( signalId,signalName,suppressExp),...
* CALLS    : 
* CALLED BY:  Web_MakeAlarmSupExpWebPageBuf 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
//��ȡnew ODispAlarmItem( signalId,signalName,suppressExp),...
static int Web_GetDispAlarmItemsBuf(IN int iEquipTypeId, 
                                    IN char *pEquipTypeCFGFileName, 
                                    IN int iLanguage, 
                                    OUT char **ppszDispAlarmItemsBuf)
{
    //TRACE("INTO Web_GetDispAlarmItemsBuf!!!\n");

    char *pCFGFilePathName = NULL;
    char *pszReturnBuf = NULL;
    char *pszAlarmName = NULL;
    char *pSearchValue = NULL;
    int iBufLen = 0;
    int i;
    WEB_ALARM_CFG_INFO_LINE *pszAlarmConfigInfoTemp = NULL;
    EQUIP_INFO *pszEquipInfo = NULL;
    char *pszParseResult = NULL;

    WEB_ALARM_CFG_INFO *stAlarmConfigInfo = NULL;
    stAlarmConfigInfo = NEW(WEB_ALARM_CFG_INFO,1);
    if (stAlarmConfigInfo == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_GetDispAlarmItemsBuf: "
            "ERROR: There is no enough memory! ", 
            __FILE__);
        return ERR_NO_MEMORY;
    }
    
    Web_GetEquipInfoByTypeId(iEquipTypeId, &pszEquipInfo);

    pCFGFilePathName = NEW(char,MAX_FILE_PATH_BUF_LEN);
    if (pCFGFilePathName == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_GetDispAlarmItemsBuf: "
            "ERROR: There is no enough memory! ", 
            __FILE__);
        return ERR_NO_MEMORY;
    }
    sprintf(pCFGFilePathName,"%s%s",SCUP_EQUIPCFG_FILE_PATH,pEquipTypeCFGFileName);

    Web_LoadStdEquipAlarmSigConfigFile(pCFGFilePathName, stAlarmConfigInfo);

    DELETE(pCFGFilePathName);




    pszReturnBuf = NEW(char, stAlarmConfigInfo->iNumber * MAX_ITEM_BUF);
    if (pszReturnBuf == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_GetDispAlarmItemsBuf: "
            "ERROR: There is no enough memory! ", 
            __FILE__);
        return ERR_NO_MEMORY;
    }
    memset(pszReturnBuf,0,stAlarmConfigInfo->iNumber * MAX_ITEM_BUF);


    pszReturnBuf[iBufLen++] = '\t';

    pszAlarmConfigInfoTemp = stAlarmConfigInfo->stWebAlarmConfigLine;
    for(i = 0; i < stAlarmConfigInfo->iNumber; i++)
    {
        //TRACE("____AlarmSupExp:%s________\n",pszAlarmConfigInfoTemp->pszAlarmSupExp);
        //TRACE("AlarmID:%d\n",pszAlarmConfigInfoTemp->iAlarmSigId);
        Web_ParseSuppressingExp(pszAlarmConfigInfoTemp->pszAlarmSupExp, &pszParseResult, pszEquipInfo, iLanguage);
        //TRACE("Web_ParseSuppressingExp:%s\n",pszParseResult);
        Web_ParseSignalName(iLanguage,pszEquipInfo, pszAlarmConfigInfoTemp->iAlarmSigId, &pszAlarmName);

        iBufLen += sprintf(pszReturnBuf + iBufLen, "new ODispAlarmItem( %3d,\"%s\",\"%s\"),\n",
                            pszAlarmConfigInfoTemp->iAlarmSigId,
                            pszAlarmName,
                            pszParseResult);

        DELETE(pszParseResult);
        pszParseResult = NULL;

        pszAlarmConfigInfoTemp++;

    }

    pSearchValue = strrchr(pszReturnBuf,44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }


    pszAlarmConfigInfoTemp = stAlarmConfigInfo->stWebAlarmConfigLine;
    for(i = 0; i < stAlarmConfigInfo->iNumber; i++)
    {
        if(pszAlarmConfigInfoTemp->pszAlarmSupExp != NULL)
        {
            DELETE(pszAlarmConfigInfoTemp->pszAlarmSupExp);
            DELETE(pszAlarmConfigInfoTemp->pszRegId);
        }
        
        pszAlarmConfigInfoTemp++;
    }

    DELETE(stAlarmConfigInfo->stWebAlarmConfigLine);
    DELETE(stAlarmConfigInfo);

    *ppszDispAlarmItemsBuf = pszReturnBuf;

    return iBufLen;

    
}

/*==========================================================================*
* FUNCTION :  Web_LoadStdEquipAlarmSigConfigFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_GetDispAlarmItemsBuf
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_LoadStdEquipAlarmSigConfigFile(IN char *pFilePath, IN WEB_ALARM_CFG_INFO *pstAlarmConfigInfo)
{    

    

    if (Mutex_Lock(GetMutexLoadAlarmConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
        == ERR_MUTEX_OK)
    {

        Cfg_LoadConfigFile(pFilePath,LoadAlarmConfigFile,pstAlarmConfigInfo);

        Mutex_Unlock(GetMutexLoadAlarmConfig());

    }

    return TRUE;

}

/*==========================================================================*
* FUNCTION :  LoadAlarmConfigFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_LoadStdEquipAlarmSigConfigFile
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int LoadAlarmConfigFile(IN void *pCfg, OUT void *pLoadToBuf)
{

    CONFIG_TABLE_LOADER loader[1];
    WEB_ALARM_CFG_INFO *pBuf = NULL;
    pBuf = (WEB_ALARM_CFG_INFO *)pLoadToBuf;



    DEF_LOADER_ITEM(&loader[0],
        NULL,
        &(pBuf->iNumber), 
        ALARM_INFO,
        &(pBuf->stWebAlarmConfigLine), 
        ParseAlarmTableProc);

    Cfg_LoadTables(pCfg, 1, loader);



    return 0;

}

/*==========================================================================*
* FUNCTION :  ParseAlarmTableProc
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  LoadAlarmConfigFile
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int ParseAlarmTableProc(IN char *szBuf, OUT WEB_ALARM_CFG_INFO_LINE *pStructData)
{
    //#Alarm ID	 Alarm Name	Alarm Name Resource ID	Alarm Level	Alarm Expression	Alarm Delay	  Suppressing Expression	Relay ID
    char *pField;

    ASSERT(szBuf);
    ASSERT(pStructData);


    //TRACE("INTO ParsePLCTableProc!!!!\n");

    /* 1.Alarm ID field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->iAlarmSigId = atoi(pField);
    }
    else
    {
        return 1;
    }

    /* 2.Alarm Name field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);


    /* 3.Alarm Name Resource ID field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    
    
    /* 4.Alarm Level field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    

    /* 5.Alarm Expression field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    

    /* 6.Alarm Delay field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);


    /* 7.Suppressing Expression field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->pszAlarmSupExp = NEW_strdup(pField);
    }
    else
    {
        return 7;
    }

    /* 8.Relay ID field */
    szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
    if (pField != NULL)
    {
        pStructData->pszRegId = NEW_strdup(pField);
    }
    else
    {
        return 8;
    }

    //TRACE("ParsePLCTableProc OK!!!\n");

    return 0;

}

/*==========================================================================*
* FUNCTION :  Web_ParseSuppressingExp
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_GetDispAlarmItemsBuf
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_ParseSuppressingExp(IN char *pszAlarmSupExp, OUT char **ppszResult, IN EQUIP_INFO *pszEquipInfo, IN int iLanguage)
{

    //TRACE("INTO Web_ParseSuppressingExp!!!\n");
    
    char *pszAlarmSupExpBuf = pszAlarmSupExp;
    char *pszTempBuf = NULL;
    char *pszSiganlBuf = NULL;
    int iBufLen = 0;
    int iSequnceNo;
    int iAlarmSigId;

    if(strcasecmp(pszAlarmSupExp,"NA")!=0)
    {
        char *pszResult = NEW(char, 256);
        if(*pszAlarmSupExp == '[')
        {
            pszAlarmSupExpBuf++;
            pszAlarmSupExpBuf = Cfg_SplitStringEx(pszAlarmSupExpBuf ,&pszTempBuf,',');
            //TRACE("%s\n",pszTempBuf);
            iSequnceNo = atoi(pszTempBuf);
            pszAlarmSupExpBuf = Cfg_SplitStringEx(pszAlarmSupExpBuf,&pszTempBuf,',');
            //TRACE("%s\n",pszTempBuf);
            pszAlarmSupExpBuf = Cfg_SplitStringEx(pszAlarmSupExpBuf,&pszTempBuf,']');
            //TRACE("%s\n",pszTempBuf);
            iAlarmSigId = atoi(pszTempBuf);
            //TRACE("____iSequnceNo:%d____iAlarmSigId:%d_____\n",iSequnceNo,iAlarmSigId);

            Web_ParseSignal(iSequnceNo, iAlarmSigId, &pszSiganlBuf, pszEquipInfo, iLanguage);
            //TRACE("Web_ParseSignal:%s\n",pszSiganlBuf);

            iBufLen += sprintf(pszResult + iBufLen, "%s",pszSiganlBuf);

            DELETE(pszSiganlBuf);
        }

        while(*pszAlarmSupExpBuf!= '\0')
        {

            pszAlarmSupExpBuf = Cfg_SplitStringEx(pszAlarmSupExpBuf,&pszTempBuf,'[');
            iBufLen += sprintf(pszResult + iBufLen, "%s",pszTempBuf);

            pszAlarmSupExpBuf = Cfg_SplitStringEx(pszAlarmSupExpBuf,&pszTempBuf,',');
            iSequnceNo = atoi(pszTempBuf);
            pszAlarmSupExpBuf = Cfg_SplitStringEx(pszAlarmSupExpBuf,&pszTempBuf,',');
            pszAlarmSupExpBuf = Cfg_SplitStringEx(pszAlarmSupExpBuf,&pszTempBuf,']');
            iAlarmSigId = atoi(pszTempBuf);
            //TRACE("____iSequnceNo:%d____iAlarmSigId:%d_____\n",iSequnceNo,iAlarmSigId);

            Web_ParseSignal(iSequnceNo, iAlarmSigId, &pszSiganlBuf, pszEquipInfo, iLanguage);
            //TRACE("Web_ParseSignal:%s\n",pszSiganlBuf);

            iBufLen += sprintf(pszResult + iBufLen, "%s",pszSiganlBuf);

            DELETE(pszSiganlBuf);
        }

        *ppszResult = pszResult;

    }
    else
    {
        *ppszResult = NEW_strdup(pszAlarmSupExp);
    }

    return TRUE;
    
    //TRACE("Web_ParseSuppressingExp OK!!!\n");
}

/*==========================================================================*
* FUNCTION :  Web_ParseSignal
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_ParseSuppressingExp
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_ParseSignal(IN int iSequnceNo, IN int iAlarmSigId, OUT char **ppszResult, IN EQUIP_INFO *pszEquipInfo, IN int iLanguage)
{
    //TRACE("INTO Web_ParseSignal!!!\n");
    int iEquipId;
    int i;
    EQUIP_INFO *pszRelatedEquipInfo = NULL;
    ALARM_SIG_INFO *ptemp = NULL;
    char *pszAlarmSigName = NULL;
    char *pszEquipName = NULL;
    char *pszReturnBuf = NEW(char,256);
    if(iSequnceNo != 0)
    {
        int itemp = iSequnceNo-1;
        iEquipId = pszEquipInfo->pRelevantEquipList[itemp];
        TRACE("iEquipId:%d",iEquipId);

        Web_GetEquipInfoById(iEquipId, &pszRelatedEquipInfo);
        TRACE(" Web_GetEquipInfoById OK!!!\n");

        ptemp = pszRelatedEquipInfo->pStdEquip->pAlarmSigInfo;

        for(i = 0; i < pszRelatedEquipInfo->pStdEquip->iAlarmSigNum; i++)
        {
            if(ptemp->iSigID == iAlarmSigId)
            {
                break;
            }

            ptemp++;
        }


        pszAlarmSigName = ptemp->pSigName->pFullName[iLanguage];
        //TRACE("pszAlarmSigName:%s",pszAlarmSigName);
        pszEquipName = pszRelatedEquipInfo->pStdEquip->pTypeName->pFullName[iLanguage];
        //TRACE("pszEquipName:%s",pszEquipName);

        sprintf(pszReturnBuf, "[%s,%s]",pszEquipName,pszAlarmSigName);

        *ppszResult = pszReturnBuf;
    }
    else
    {
        ptemp = pszEquipInfo->pStdEquip->pAlarmSigInfo;

        for(i = 0; i < pszEquipInfo->pStdEquip->iAlarmSigNum; i++)
        {
            if(ptemp->iSigID == iAlarmSigId)
            {
                break;
            }

            ptemp++;
        }


        pszAlarmSigName = ptemp->pSigName->pFullName[iLanguage];
        //TRACE("pszAlarmSigName:%s",pszAlarmSigName);
        pszEquipName = pszEquipInfo->pStdEquip->pTypeName->pFullName[iLanguage];
        //TRACE("pszEquipName:%s",pszEquipName);

        sprintf(pszReturnBuf, "[%s,%s]",pszEquipName,pszAlarmSigName);

        *ppszResult = pszReturnBuf;

    }
    //TRACE("Web_ParseSignal OK!!!\n");

}

/*==========================================================================*
* FUNCTION :  Web_ParseSignalName
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_ParseSuppressingExp
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_ParseSignalName(IN int iLanguage, IN EQUIP_INFO *pszEquipInfo, IN int iAlarmSigId, OUT char **ppszAlarmName)
{
    int i;
    EQUIP_INFO *pszTempOfEquipInfo = NULL;
    pszTempOfEquipInfo = pszEquipInfo;

    ALARM_SIG_INFO *pszTempOfAlarm = NULL;

    pszTempOfAlarm = pszTempOfEquipInfo->pStdEquip->pAlarmSigInfo;
    for(i = 0; i < pszTempOfEquipInfo->pStdEquip->iAlarmSigNum; i++)
    {
        if(pszTempOfAlarm->iSigID == iAlarmSigId)
        {
            break;
        }
        pszTempOfAlarm++;
    }

    *ppszAlarmName = pszTempOfAlarm->pSigName->pFullName[iLanguage];
    
}

/*==========================================================================*
* FUNCTION :  Web_SetAlarmSuppressingExp
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_SendConfigureData
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_SetAlarmSuppressingExp(IN char *pszSendBuf,IN char *szUserName)
{
    TRACE("\n____pszSendBuf:%s___\n",pszSendBuf);
    int iStdEquipTypeID;
    int iAlarmSigID;
    char stdEquipTypeID[5];
    char alarmSigID[4];
    char *pszAlarmSupExp = NULL;
    char *pszStdEquipFileName = NULL;
    FILE *pFile = NULL;
    long lFileLen;
    long lCurrPos;
    char *pszFile = NULL;
    char *pszOutFile = NULL;
    void *pProf = NULL;
    int flage = 0;
    char pLineBuf[MAX_LINE_SIZE];
    char *pPosition = NULL;
    int iIDBufLen = 0;
    char sAlarmIDBuf[5];
    int iAlarmSigIDTemp;
    char *pCFGFilePathName = NULL;
    int iLen = 0;
    char szTempUserName[64];

    
    pCFGFilePathName = NEW(char,MAX_FILE_PATH_BUF_LEN);
    if (pCFGFilePathName == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_SetAlarmSuppressingExp: "
            "ERROR: There is no enough memory! ", 
            __FILE__);
        return ERR_NO_MEMORY;
    }

    strncpyz(stdEquipTypeID,pszSendBuf,5);
    iStdEquipTypeID = atoi(stdEquipTypeID);
    TRACE("____StdEquipTypeID:%d___\n",iStdEquipTypeID);
    pszSendBuf = pszSendBuf + 4;

    strncpyz(alarmSigID,pszSendBuf,4);
    iAlarmSigID = atoi(alarmSigID);
    TRACE("____AlarmSigID:%d___\n",iAlarmSigID);
    pszSendBuf = pszSendBuf + 3;

    pszAlarmSupExp = Web_RemoveWhiteSpace(pszSendBuf);
    TRACE("____AlarmSupExp:%s___\n",pszAlarmSupExp);

    Web_GetStdEquipFileName(iStdEquipTypeID, &pszStdEquipFileName);
    sprintf(pCFGFilePathName,"%s%s",SCUP_EQUIPCFG_FILE_PATH,pszStdEquipFileName);
    TRACE("____StdEquipFileName:%s____\n",pCFGFilePathName);

    if (Mutex_Lock(GetMutexLoadAlarmConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
        == ERR_MUTEX_OK)
    {
        pFile = fopen(pCFGFilePathName,"r+");
        if(pFile == NULL)
        {
            Mutex_Unlock(GetMutexLoadAlarmConfig());
            return 5;
        }

        lFileLen = GetFileLength(pFile);
        TRACE("FileLen : %ld\n",lFileLen);

        pszFile = NEW(char, lFileLen + 1);
        if (pszFile == NULL)
        {
            AppLogOut("WEB", APP_LOG_ERROR, 
                "[%s]--Web_SetAlarmSuppressingExp: "
                "ERROR: There is no enough memory! ", 
                __FILE__);
            fclose(pFile);
            Mutex_Unlock(GetMutexLoadAlarmConfig());
            return 5;
        }
        memset(pszFile, 0, lFileLen + 1);

        pszOutFile = NEW(char, lFileLen + 1 + 512);//MAX_LINE_SIZE=512
        if (pszOutFile == NULL)
        {
            AppLogOut("WEB", APP_LOG_ERROR, 
                "[%s]--Web_SetAlarmSuppressingExp: "
                "ERROR: There is no enough memory! ", 
                __FILE__);
            fclose(pFile);
            Mutex_Unlock(GetMutexLoadAlarmConfig());
            return 5;
        }
        memset(pszOutFile, 0, lFileLen + 1);

        fread(pszFile, 1, (size_t)lFileLen, pFile);
        fclose(pFile);

        pProf = Cfg_ProfileOpen(pszFile,lFileLen);
        if(Cfg_ProfileFindSection(pProf,ALARM_INFO) == 0)
        {
            AppLogOut("WEB", APP_LOG_ERROR, 
                "[%s]--Web_SetAlarmSuppressingExp: "
                "ERROR: Can't find Section! ", 
                __FILE__);      
            DELETE(pProf);
            Mutex_Unlock(GetMutexLoadAlarmConfig());
            return 5;
        }

        lCurrPos = Cfg_ProfileTell(pProf);
        TRACE("lCurrPos : %ld\n",lCurrPos);

        memmove(pszOutFile, pszFile, lCurrPos);

        while(Cfg_ProfileReadLine(pProf, pLineBuf, MAX_LINE_SIZE) != 0)
        {
            if(pLineBuf[0] == '#' && flage != 1)
            {
                iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
                memset(pLineBuf,0,MAX_LINE_SIZE);
            }
            else if(pLineBuf[0] != '#' && flage != 1 && pLineBuf[0] != '[')
            {
                pPosition = strchr(pLineBuf,'\t');
                iIDBufLen = pPosition - pLineBuf;
                strncpyz(sAlarmIDBuf,pLineBuf,iIDBufLen+1);
                char *ptemp = NULL;
                ptemp = Web_RemoveWhiteSpace(sAlarmIDBuf);
                iAlarmSigIDTemp = atoi(ptemp);
                memset(sAlarmIDBuf,0,5);
                if(iAlarmSigIDTemp == iAlarmSigID)
                {
                    char *pszNewLine = NULL;
                    pszNewLine = Web_EditCFGLineItem(pLineBuf, 6, pszAlarmSupExp);
                    iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pszNewLine);
                    DELETE(pszNewLine);
                    memset(pLineBuf,0,MAX_LINE_SIZE);

                }
                else
                {
                    iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
                    memset(pLineBuf,0,MAX_LINE_SIZE);
                }
                
            }
            else if(pLineBuf[0] == '[' && flage != 1 )
            {
                iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
                memset(pLineBuf,0,MAX_LINE_SIZE);
                flage = 1;
            }
            else if(flage == 1 )
            {
                iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
                memset(pLineBuf,0,MAX_LINE_SIZE);
            }

        }

        pFile = fopen(pCFGFilePathName,"w+");
        fwrite(pszOutFile,strlen(pszOutFile),1,pFile);
        fclose(pFile);

       
        DELETE(pszFile);
        pszFile = NULL;
        DELETE(pszOutFile);
        pszOutFile = NULL;
        DELETE(pProf);

        Mutex_Unlock(GetMutexLoadAlarmConfig());
    }

    
    DELETE(pCFGFilePathName);


    return 6;
}

/*==========================================================================*
* FUNCTION :  Web_EditCFGLineItem
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_SetAlarmSuppressingExp
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static char *Web_EditCFGLineItem(IN char *pszLineBuf, IN int iColNo, IN char *pszNewItemBuf)
{

    int FPartPosition=0;
    int SPartPosition=0;
    int LPartPosition=0;
    char *temp2;

    char *result =NEW(char, MAX_LINE_SIZE);
    memset(result,0,MAX_LINE_SIZE);

    char *temp=pszLineBuf;

    int i;
    for(i = 0; i < iColNo; i++)
    {
        temp=strchr(temp,'\t');
        Web_SkipTabSpace(&temp);
    }

    FPartPosition=temp-pszLineBuf;
    strncpy(result,pszLineBuf,FPartPosition);

    SPartPosition=strlen(pszNewItemBuf);
    memmove(result+FPartPosition,pszNewItemBuf,SPartPosition);

    temp2=strchr(temp,'\t');
    if(temp2==0)
    {
        TRACE("________Web_EditCFGLineItem:%s_______\n",result);
        return result;
    }
    else
    {
        LPartPosition=strlen(temp2);
        memmove(result+FPartPosition+SPartPosition,temp2,LPartPosition);
        TRACE("________Web_EditCFGLineItem:%s_______\n",result);
        return result;
    }

   
}

/*==========================================================================*
* FUNCTION :  Web_SkipTabSpace
* PURPOSE  :  
* CALLS    : 
* CALLED BY:
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static void Web_SkipTabSpace(char **pProf)
{
    while (**pProf=='\t')
    {
        (*pProf)++;
    }
}

/*==========================================================================*
* FUNCTION :  Web_MakeAlarmRegWebPageBuf
* PURPOSE  :  
* CALLS    : 
* CALLED BY:
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_MakeAlarmRegWebPageBuf(IN char *pszBuf,OUT char **ppszReturn,IN int iLanguage)
{
    int     istdEquipTypeId;
    char    *pszStdEquipBuf = NULL;//new OStdEquip( equipTypeId, equipTypeName),... 
    char    *pszDispAlarmItemsBuf = NULL;//new ODispAlarmItem( signalId,signalName,RelayNo),... 
    char    *pszEquipTypeName = NULL;//StdEquipTypeName
    char    *pszStdEquipFileName = NULL;
    int     iLen1 = 0;
    int     iLen2 = 0;
    int     iLen3 = 0;
    EQUIP_INFO *pszEquipInfo = NULL;
    char    szEquipIdTemp[5];

    strncpyz(szEquipIdTemp,pszBuf,5);
    istdEquipTypeId = atoi(szEquipIdTemp);

    Web_GetStdEquipFileName(istdEquipTypeId,&pszStdEquipFileName);
    TRACE("%s\n",pszStdEquipFileName);

    Web_GetEquipInfoByTypeId(istdEquipTypeId, &pszEquipInfo);

    iLen1 = Web_GetStdEquipBuf(&pszStdEquipBuf, iLanguage);

    if(pszEquipInfo->pStdEquip->iAlarmSigNum != 0)
    {
        iLen2 = Web_GetDispAlarmRegItemsBuf(istdEquipTypeId, pszStdEquipFileName,iLanguage, &pszDispAlarmItemsBuf);
        TRACE("%s\n",pszDispAlarmItemsBuf);
    }
    else
    {
        pszDispAlarmItemsBuf = NEW(char, MAX_ITEM_BUF+1);
        memset(pszDispAlarmItemsBuf, ' ', MAX_ITEM_BUF+1);
        iLen2 = MAX_ITEM_BUF;
        pszDispAlarmItemsBuf[MAX_ITEM_BUF] = '\0';

    }

    pszEquipTypeName = pszEquipInfo->pStdEquip->pTypeName->pFullName[iLanguage];
    iLen3 = strlen(pszEquipTypeName);

    char *pResult = NEW(char,iLen1 + iLen2 + iLen3 + 4 );
    if (pResult == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_MakeAlarmSupExpWebPageBuf: "
            "ERROR: There is no enough memory! ", 
            __FILE__);
        return ERR_NO_MEMORY;
    }
    memset(pResult,0,iLen1 + iLen2 + iLen3 + 4);

    strcpy(pResult,pszEquipTypeName);//StdEquipTypeName
    pszStdEquipBuf[0] = ';';
    strcat(pResult,pszStdEquipBuf);//OStdEquip
    pszDispAlarmItemsBuf[0] = ';';
    strcat(pResult,pszDispAlarmItemsBuf);//ODispAlarmItem

    TRACE("Web_MakeAlarmRegWebPageBuf:%s\n",pResult);



    *ppszReturn = pResult;

    DELETE(pszStdEquipBuf);
    DELETE(pszDispAlarmItemsBuf);
    return TRUE;

}


/*==========================================================================*
* FUNCTION :  Web_GetDispAlarmRegItemsBuf
* PURPOSE  :  
* CALLS    : 
* CALLED BY:
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_GetDispAlarmRegItemsBuf(IN int iEquipTypeId, 
                                       IN char *pEquipTypeCFGFileName, 
                                       IN int iLanguage, 
                                       OUT char **ppszDispAlarmItemsBuf)
{
    TRACE("INTO Web_GetDispAlarmRegItemsBuf!!!\n");

    //char *pCFGFilePathName = NULL;
    char *pszReturnBuf = NULL;
    char *pszAlarmName = NULL;
    char *pSearchValue = NULL;
    int iBufLen = 0;
    int i;
    //WEB_ALARM_CFG_INFO_LINE *pszAlarmConfigInfoTemp = NULL;
    EQUIP_INFO *pszEquipInfo = NULL;
    char *pszParseResult = NULL;

    //WEB_ALARM_CFG_INFO *stAlarmConfigInfo = NULL;
    /*stAlarmConfigInfo = NEW(WEB_ALARM_CFG_INFO,1);
    if (stAlarmConfigInfo == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_GetDispAlarmItemsBuf: "
            "ERROR: There is no enough memory! ", 
            __FILE__);
        return ERR_NO_MEMORY;
    }*/

    Web_GetEquipInfoByTypeId(iEquipTypeId, &pszEquipInfo);

    //pCFGFilePathName = NEW(char,MAX_FILE_PATH_BUF_LEN);
    //if (pCFGFilePathName == NULL)
    //{
    //    AppLogOut("WEB", APP_LOG_ERROR, 
    //        "[%s]--Web_GetDispAlarmItemsBuf: "
    //        "ERROR: There is no enough memory! ", 
    //        __FILE__);
    //    return ERR_NO_MEMORY;
    //}
    //sprintf(pCFGFilePathName,"%s%s",SCUP_EQUIPCFG_FILE_PATH,pEquipTypeCFGFileName);

    ////Web_LoadStdEquipAlarmSigConfigFile(pCFGFilePathName, stAlarmConfigInfo);

    //DELETE(pCFGFilePathName);




    pszReturnBuf = NEW(char, pszEquipInfo->pStdEquip->iAlarmSigNum * MAX_ITEM_BUF);
    if (pszReturnBuf == NULL)
    {
        AppLogOut("WEB", APP_LOG_ERROR, 
            "[%s]--Web_GetDispAlarmItemsBuf: "
            "ERROR: There is no enough memory! ", 
            __FILE__);
        return ERR_NO_MEMORY;
    }
    memset(pszReturnBuf,0,pszEquipInfo->pStdEquip->iAlarmSigNum * MAX_ITEM_BUF);


    pszReturnBuf[iBufLen++] = '\t';

    //pszAlarmConfigInfoTemp = stAlarmConfigInfo->stWebAlarmConfigLine;
    ALARM_SIG_INFO *pTempAlarmInfo = pszEquipInfo->pStdEquip->pAlarmSigInfo;
    for(i = 0; i < pszEquipInfo->pStdEquip->iAlarmSigNum; i++)
    {
        //TRACE("____AlarmSupExp:%s________\n",pszAlarmConfigInfoTemp->pszAlarmSupExp);
        //TRACE("AlarmID:%d\n",pszAlarmConfigInfoTemp->iAlarmSigId);
        //TRACE("Web_ParseSuppressingExp:%s\n",pszParseResult);
        //Web_ParseSignalName(iLanguage,pszEquipInfo, pszAlarmConfigInfoTemp->iAlarmSigId, &pszAlarmName);

        iBufLen += sprintf(pszReturnBuf + iBufLen, "new ODispAlarmItem( %3d,\"%s\",\"%d\"),\n",
            pTempAlarmInfo->iSigID,
            pTempAlarmInfo->pSigName->pFullName[iLanguage],
            pTempAlarmInfo->iAlarmRelayNo);

        /*DELETE(pszParseResult);
        pszParseResult = NULL;*/

        pTempAlarmInfo++;

    }

    pSearchValue = strrchr(pszReturnBuf,44);  //get the last ','
    if(pSearchValue != NULL)
    {
        *pSearchValue = 32;   //replace ',' with ' '
    }

    *ppszDispAlarmItemsBuf = pszReturnBuf;

    return iBufLen;


}


/*==========================================================================*
* FUNCTION :  Web_SetAlarmRelay
* PURPOSE  :  
* CALLS    : 
* CALLED BY:
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
int Web_SetAlarmRelay(IN char *pszSendBuf,IN char *szUserName)
{

    int iStdEquipTypeID;
    int iAlarmSigID;
    char stdEquipTypeID[5];
    char alarmSigID[4];
    char *pszAlarmRelay = NULL;
    /*char *pszStdEquipFileName = NULL;
    FILE *pFile = NULL;
    long lFileLen;
    long lCurrPos;
    char *pszFile = NULL;
    char *pszOutFile = NULL;
    void *pProf = NULL;
    int flage = 0;
    char pLineBuf[MAX_LINE_SIZE];
    char *pPosition = NULL;
    int iIDBufLen = 0;
    char sAlarmIDBuf[5];
    int iAlarmSigIDTemp;
    char *pCFGFilePathName = NULL;
    int iLen = 0;*/
    int iError = 0;
    int iAlarmRelay;
    char szTempUserName[40];
    EQUIP_INFO *pszEquipInfo = NULL;


    strncpyz(stdEquipTypeID,pszSendBuf,5);
    iStdEquipTypeID = atoi(stdEquipTypeID);
    TRACE("____StdEquipTypeID:%d___\n",iStdEquipTypeID);
    pszSendBuf = pszSendBuf + 4;

    strncpyz(alarmSigID,pszSendBuf,4);
    iAlarmSigID = atoi(alarmSigID);
    TRACE("____AlarmSigID:%d___\n",iAlarmSigID);
    pszSendBuf = pszSendBuf + 3;

    pszAlarmRelay = Web_RemoveWhiteSpace(pszSendBuf);
    iAlarmRelay = atoi(pszAlarmRelay);
    TRACE("____AlarmRelay:%s___\n",pszAlarmRelay);


    Web_GetEquipInfoByTypeId(iStdEquipTypeID, &pszEquipInfo);
    TRACE("\n_________iStdEquipTypeID[%d]__________\n",pszEquipInfo->iEquipID);

    SET_A_SIGNAL_INFO_STU		stSetASignalInfo;


    int iBufLen = 0;
    int iVarSubID	= DXI_MERGE_SIG_ID(3,iAlarmSigID);


    sprintf(szTempUserName,"Web:%s  ",szUserName);
    strncpyz(stSetASignalInfo.cModifyUser,szTempUserName,strlen(szTempUserName)+1);
    
    stSetASignalInfo.byModifyType		= MODIFY_ALARM_RELAY;
    stSetASignalInfo.bModifyAlarmRelay	= iAlarmRelay;


    iBufLen = sizeof(SET_A_SIGNAL_INFO_STU);


    iError += DxiSetData(VAR_A_SIGNAL_INFO_STRU,
                         pszEquipInfo->iEquipID,			
                         iVarSubID,		
                         iBufLen,			
                         (void *)&stSetASignalInfo,			
                         0);

    if (iError == ERR_DXI_OK )
    {
        return 6;
    }
    else
    {
        AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Modify signal alarm level fail");
        if(iError == ERR_DXI_HARDWARE_SWITCH_STATUS)
        {
            return WEB_RETURN_PROTECTED_ERROR;
        }
        else
        {
            return 5;
        }
    }


  
}

//end////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
//Added by wj for YDN Setting Config
#ifndef EEM_SUPPORT
/*==========================================================================*
* FUNCTION :  Web_MakeYDNPrivateConfigreBuffer
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/

static char *Web_MakeYDNPrivateConfigreBuffer(void)
{
    YDN_COMMON_CONFIG	stCommonConfig ;
    APP_SERVICE		*stAppService = NULL;
    int				iLen = 0;
    char			*szReturn = NULL;
    int				iReturn = 0;
    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN_NAME);
    stCommonConfig.iProtocolType = YDN23;
    if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
    {

        iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
            stAppService->bServiceRunning,
            &stAppService->args, 
            0x0,
            0,
            (void *)&stCommonConfig);

        szReturn = NEW(char, 250);

        if(szReturn != NULL)
        {
            //TRACE("stAppService ---- 4\n");
            //iLen += sprintf(szReturn + iLen, "%2d", iReturn);
            if(iReturn == ERR_SERVICE_CFG_OK)
            {
                iLen += sprintf(szReturn + iLen, "\t %d,%d,%d,%d,%d,", 
                    stCommonConfig.iProtocolType,
                    stCommonConfig.iMediaType,
                    stCommonConfig.iAttemptElapse/1000,
                    stCommonConfig.iMaxAttempts,
                    stCommonConfig.bReportInUse);
                iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[0]);
                iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[1]);
                iLen += sprintf(szReturn + iLen, "\"%s\",", stCommonConfig.szAlarmReportPhoneNumber[2]);
                iLen += sprintf(szReturn + iLen, "\"%s\",",	stCommonConfig.szCommPortParam);
                iLen += sprintf(szReturn + iLen, "%d",	stCommonConfig.byADR);

                return szReturn;


            }
        }

    }
    TRACE("return NULL\n");
    return NULL;
}

/*==========================================================================*
* FUNCTION :  Web_ModifyYDNPrivateConfigure
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_ModifyYDNPrivateConfigure(IN char *szBuffer,IN char *szUserName)
{
    ASSERT(szBuffer);
    YDN_COMMON_CONFIG	stCommonConfig ;
    COMMON_CONFIG	stEEMCommonConfig ;
    APP_SERVICE		*stAppService = NULL;
    int				iModifyType = 0;
    int				iEEMModifyType = 0;
    int				iReturn = 0;
    //char            szTemp[2048];
    char            szTempUserName[40];
#ifdef _SHOW_WEB_INFO
    //TRACE("szBuffer : %s\n", szBuffer);
#endif

    sprintf(szTempUserName,"Web:%s ",szUserName);
    strncpyz(stCommonConfig.cModifyUser,szTempUserName,strlen(szTempUserName)+1);

    TRACE("\n----------- ModifyUser : %s ----- \n", stCommonConfig.cModifyUser);
    if((iModifyType = Web_GetYDNModifyInfo(szBuffer, &stCommonConfig)) != FALSE)
    {
        //TRACE("iModifyType : %x ----- 1\n", iModifyType);
	stCommonConfig.iProtocolType = 4; 
	//Change mode from EEM to YDN23
	if(stCommonConfig.iProtocolType == YDN23)
	{
		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);
		if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		{
			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
									stAppService->bServiceRunning,
									&stAppService->args, 
									0x0,
									0,
								(void *)&stEEMCommonConfig);

			
			if(iReturn == ERR_SERVICE_CFG_OK)
			{
				stEEMCommonConfig.iProtocolType = YDN23;
				stEEMCommonConfig.iMediaType = 2;//tcp/ip
				strncpyz(stEEMCommonConfig.szCommPortParam,"2000",5);
				

				iEEMModifyType = iEEMModifyType | ESR_CFG_W_MODE;
				iEEMModifyType = iEEMModifyType | ESR_CFG_ALL | ESR_CFG_PROTOCOL_TYPE |ESR_CFG_MEDIA_TYPE | ESR_CFG_MEDIA_PORT_PARAM;
				TRACE("iEEMModifyType = %d\n", iEEMModifyType);
				iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
					stAppService->bServiceRunning,
					&stAppService->args, 
					iEEMModifyType,
					0,
					(void *)&stEEMCommonConfig);// == ERR_SERVICE_CFG_OK)
				TRACE("\nEEMiReturn = %d\n", iReturn);
				if(iReturn != ERR_SERVICE_CFG_OK)
				{
					return iReturn;
				}
			}
			else
			{
				return iReturn;
			}

		}
	}


        stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_YDN_NAME);
	stCommonConfig.iProtocolType = 4; 

        if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
        {
            TRACE("iModifyType : %x ----- 2", iModifyType);
            TRACE("stAppService->bServiceRunning : %d\n",stAppService->bServiceRunning);
	   
            iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
                stAppService->bServiceRunning,
                &stAppService->args, 
                iModifyType,
                0,
                (void *)&stCommonConfig);// == ERR_SERVICE_CFG_OK)
	    TRACE("\nYDNiReturn = %d\n", iReturn);

            return iReturn;
        }
    }
    return ERR_SERVICE_CFG_FAIL;
}

/*==========================================================================*
* FUNCTION :  Web_GetYDNModifyInfo
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN char *szBuffer:
OUT COMMON_CONFIG *stCommonConfig:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2006-7-31 11:20
*==========================================================================*/
static int Web_GetYDNModifyInfo(IN char *szBuffer, OUT YDN_COMMON_CONFIG *stCommonConfig)
{
    ASSERT(szBuffer);
    ASSERT(stCommonConfig);

    char			*ptr = NULL;
    char			szExchange[64], *pSearchValue = NULL;
    int				iPosition = 0;
    char			*pTrim = NULL;
    int				iModifyType = ESR_CFG_W_MODE;

    //Aanalyze szBuffer
    int			iValdateModify = 0;
    if((ptr = szBuffer) != NULL)
    {

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    stCommonConfig->iProtocolType = atoi(szExchange) ;
                    TRACE("stCommonConfig->iProtocolType :[%s][%d]\n", szExchange, stCommonConfig->iProtocolType);
                    iModifyType = iModifyType | YDN_CFG_PROTOCOL_TYPE;
                }
                ptr = ptr + iPosition;

            }

        }
	iModifyType = iModifyType | YDN_CFG_PROTOCOL_TYPE;
        ptr = ptr + 1;

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    stCommonConfig->iMediaType = atoi(szExchange);
                    TRACE("stCommonConfig->iMediaType :[%s][%d]\n", szExchange, stCommonConfig->iMediaType);
                    iModifyType = iModifyType | YDN_CFG_MEDIA_TYPE;
                }
                ptr = ptr + iPosition;

            }

        }
        ptr = ptr + 1;

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    stCommonConfig->iAttemptElapse = 1000 * atoi(szExchange);
                    TRACE("stCommonConfig->iAttemptElapse :[%s][%d]\n", szExchange, stCommonConfig->iAttemptElapse);
                    iModifyType = iModifyType | YDN_CFG_ATTEMPT_ELAPSE;
                }
                ptr = ptr + iPosition;
            }
        }
        ptr = ptr + 1;

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    stCommonConfig->iMaxAttempts = atoi(szExchange);
                    TRACE("stCommonConfig->iMaxAttempts :[%s][%d]\n", szExchange,stCommonConfig->iMaxAttempts);
                    iModifyType = iModifyType | YDN_CFG_MAX_ATTEMPTS;
                }
                ptr = ptr + iPosition;
            }
        }
        ptr = ptr + 1;

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    if(iPosition > 2)
                    {
                        stCommonConfig->bReportInUse = TRUE;
                    }
                    else
                    {
                        stCommonConfig->bReportInUse = FALSE;
                    }
                    TRACE("stCommonConfig->bReportInUse :[%s][%d]\n", szExchange, stCommonConfig->bReportInUse);
                    iModifyType = iModifyType | YDN_CFG_REPORT_IN_USE;
                }
                ptr = ptr + iPosition;
            }

        }
        ptr = ptr + 1;

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    pTrim = Cfg_RemoveWhiteSpace(szExchange);
                    strncpyz(stCommonConfig->szAlarmReportPhoneNumber[0],
                        pTrim,
                        sizeof(stCommonConfig->szAlarmReportPhoneNumber[0]));

                    TRACE("stCommonConfig->szAlarmReportPhoneNumber :[%s][%s]\n", szExchange,stCommonConfig->szAlarmReportPhoneNumber[0]);
                    iModifyType = iModifyType | YDN_CFG_REPORT_NUMBER_1;
                }
                ptr = ptr + iPosition;
            }

        }
        ptr = ptr + 1;

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    pTrim = Cfg_RemoveWhiteSpace(szExchange);
                    strncpyz(stCommonConfig->szAlarmReportPhoneNumber[1],
                        pTrim,
                        sizeof(stCommonConfig->szAlarmReportPhoneNumber[1]));

                    TRACE("stCommonConfig->szAlarmReportPhoneNumber :[%s][%s]\n", szExchange, stCommonConfig->szAlarmReportPhoneNumber[1]);
                    iModifyType = iModifyType | YDN_CFG_REPORT_NUMBER_2;
                }
                ptr = ptr + iPosition;
            }

        }
        ptr = ptr + 1;


        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);

                if(iValdateModify == 1)
                {
                    pTrim = Cfg_RemoveWhiteSpace(szExchange);
                    strncpyz(stCommonConfig->szAlarmReportPhoneNumber[2],
                        pTrim,
                        (int)sizeof(stCommonConfig->szAlarmReportPhoneNumber[2]));

                    TRACE("stCommonConfig->szCallbackPhoneNumber :[%s][%s]\n", szExchange, stCommonConfig->szAlarmReportPhoneNumber[2]);
                    iModifyType = iModifyType | YDN_CFG_REPORT_NUMBER_3;
                }
                ptr = ptr + iPosition;

            }

        }
        ptr = ptr + 1;

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    pTrim = Cfg_RemoveWhiteSpace(szExchange);
                    sprintf(stCommonConfig->szCommPortParam, "%s", pTrim);
                    TRACE("stCommonConfig->szCommPortParam :[%s][%s]\n", szExchange, stCommonConfig->szCommPortParam);
                    iModifyType = iModifyType | YDN_CFG_MEDIA_PORT_PARAM;
                }
                ptr = ptr + iPosition;
            }

        }
        ptr = ptr + 1;

        pSearchValue = strchr(ptr, 59);
        if(pSearchValue != NULL)
        {
            iPosition = pSearchValue - ptr;
            if(iPosition > 0)
            {
                strncpyz(szExchange, ptr, iPosition + 1);
                Web_TransferESRCfgToFormat(szExchange, &iValdateModify);
                TRACE("[%s][%d]\n", szExchange, iValdateModify);
                if(iValdateModify == 1)
                {
                    pTrim = Cfg_RemoveWhiteSpace(szExchange);
                    stCommonConfig->byADR = atoi(pTrim);

                    TRACE("stCommonConfig->byADR :[%s][%d]\n", szExchange, stCommonConfig->byADR);
                    iModifyType = iModifyType | YDN_CFG_ADR;
                }
                ptr = ptr + iPosition;
            }

        }

        

        return iModifyType;
    }
    return FALSE;

}
#endif
static int StartApache(void)
{

    char			szCommand[30];
    sprintf(szCommand,"bin/startwebd &");

    TRACEX("%s\n", szCommand);
    system(szCommand);
    return TRUE;
}

/*==========================================================================*
* FUNCTION :  Web_MakeSigInfoOfUserDefPage()
* PURPOSE  :   
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iLanguage:
IN int iEquipID
OUT void **ppBuf
* RETURN   :  
* COMMENTS : 
* CREATOR  : wj               DATE: 2008-08-30 11:08
*==========================================================================*/
static int Web_MakeSigInfoOfUserDefPage(IN int iLanguage, 
										IN int iPageID,
										OUT char **ppBuf)
{
	int				i,	j, iLen = 0;
	SET_SIG_INFO	*pGetBuf = NULL, *pSetSigInfo = NULL;
	char			*pOutputBuf = NULL;
	int				iSignalNumber = 0, iStatusLen = 0;
	int				iSetSigNum = 0;

	int				nInterfaceType = VAR_USER_DEF_PAGES;		
	int				nVarID = 0;
	int				nVarSubID = 0;
	int				nBufLen;
	int				nTimeOut = 0;
	int				nError = ERR_DXI_OK;
	int				iError = ERR_DXI_OK;
	SIG_ITEM		*pSigItemTemp = NULL;

	USER_DEF_PAGES *pUserDefPages = NULL;
	EQUIP_INFO *pEquipInfo = NULL;


	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pUserDefPages,			
		nTimeOut);

	pSigItemTemp = pUserDefPages->pSigItemInfo->pSigItem;

	for(i = 0;i<pUserDefPages->pSigItemInfo->iSigItemNum;i++)
	{
		if(pSigItemTemp->iPageID == iPageID)
		{
			iSetSigNum++;
		}

		pSigItemTemp++;
	}

	if(iSetSigNum > 0 && iSetSigNum < 100)
	{
		pOutputBuf = NEW(char, iSetSigNum * MAX_SIGNAL_STRUCTURE_LEN + 4 * MAX_SIGNAL_STRUCTURE_LEN );
	}
	else if(iSetSigNum > 100)
	{
		pOutputBuf = NEW(char, 100 * MAX_SIGNAL_STRUCTURE_LEN + 4 * MAX_SIGNAL_STRUCTURE_LEN ); 
	}
	else
	{
		pOutputBuf = NEW(char, 1 * MAX_SIGNAL_STRUCTURE_LEN + 4 * MAX_SIGNAL_STRUCTURE_LEN );
	}



#ifdef WEB_ADD_SYSTEM_TYPE
	char			szStatus[2048], szCtrlExpression[2048];
#else
	char			szStatus[128], szCtrlExpression[128];
#endif
	int				iValueFormat = 0;
	int				iStateNumber = 0, iCtrlExpression = 0;
	/*get set signal structure*/

	//iSignalNumber = Web_GetSetSigStruofEquip(iEquipID,&pSetSigInfo);


	nInterfaceType = VAR_A_SIGNAL_INFO_STRU;

	pSigItemTemp = pUserDefPages->pSigItemInfo->pSigItem;

	int		iSigNumTemp = 0;

	for(i = 0;i<pUserDefPages->pSigItemInfo->iSigItemNum && iSigNumTemp < 100;i++ ,pSigItemTemp++)
	{
		if(pSigItemTemp->iPageID == iPageID)
		{
			iSigNumTemp++;
		
			nVarID = pSigItemTemp->iEquipID; //Equip ID
			nVarSubID = DXI_MERGE_SIG_ID(2,pSigItemTemp->iSigID);

			iError = DxiGetData(VAR_A_EQUIP_INFO,
				nVarID,			
				0,		
				&iBufLen,			
				&pEquipInfo,			
				0);

			if(nError == ERR_DXI_OK)
			{

				if(pEquipInfo->bWorkStatus == FALSE)
				{
					continue;
				}	
			}
			else
			{
				continue;

			}

			nError = DxiGetData(nInterfaceType,
				nVarID,			
				nVarSubID,		
				&nBufLen,			
				&pSetSigInfo,			
				iTimeOut);

			if(nError == ERR_DXI_OK)
			{
				pGetBuf = pSetSigInfo;
			}
			else
			{
				continue;
			}


			if(STD_SIG_IS_DISPLAY_ON_UI(pGetBuf,DISPLAY_WEB))
			{
				iStatusLen = 0;
				////TRACE("*********%d*****%d**%d*\n", i,pGetBuf->iSigValueType, pGetBuf->iStateNum);
				iStateNumber = pGetBuf->iStateNum;
				if( pGetBuf->iSigValueType == VAR_ENUM)
				{
					for(j = 0; j < pGetBuf->iStateNum ; j++)
					{

						////TRACE("------%d,%s",j, pGetBuf->pStateText[j]->pFullName[iLanguage]);
						iStatusLen += sprintf(szStatus + iStatusLen,",\"%s\"",pGetBuf->pStateText[j]->pFullName[iLanguage]);
						////TRACE("szStatus :%s\n",szStatus);
					}


				}
				else
				{
					sprintf(szStatus, "%s", "  ");
				}

				iCtrlExpression = pGetBuf->iCtrlExpression;
				iStatusLen = 0;
				if(iCtrlExpression > 0)
				{

					for(j = 0; j < iCtrlExpression ; j++)
					{

						if(pGetBuf->pCtrlExpression[j].pText != NULL)
						{
							iStatusLen += sprintf(szCtrlExpression + iStatusLen,",%8.2f,\"%s\"",
								pGetBuf->pCtrlExpression[j].fThreshold,
								pGetBuf->pCtrlExpression[j].pText->pFullName[iLanguage]);
						}
						else
						{
							iStatusLen += sprintf(szCtrlExpression + iStatusLen,",%8.2f,\"\"",
								pGetBuf->pCtrlExpression[j].fThreshold);

						}
						//TRACE("[%d]iStatusLen[%s]\n", i,szCtrlExpression);
					}
				}
				else
				{
					sprintf(szCtrlExpression, "%s", "  ");
				}

				////TRACE("*********%d******%s**%d*\n", i, szStatus, iLen);
				iValueFormat = Web_TransferFormatToInt(pGetBuf->szValueDisplayFmt);
				iLen = iLen + sprintf(pOutputBuf+iLen,"new OSetting(%3d, %4d, \"%16s\",\"%8s\",%4d, %d, %8.2f,%8.2f,%4d,%4d,%4d %s %s), \n",
					pSigItemTemp->iEquipID,
					pGetBuf->iSigID,
					pGetBuf->pSigName->pFullName[iLanguage],
					(strlen(pGetBuf->szSigUnit) > 0)? pGetBuf->szSigUnit : "&nbsp",
					pGetBuf->iSigValueType,
					pGetBuf->byAuthorityLevel,
					pGetBuf->fMinValidValue,
					pGetBuf->fMaxValidValue,
					iValueFormat,
					iStateNumber,
					iCtrlExpression,
					szStatus,
					szCtrlExpression);
			}
		}

	}
	//TRACE("pOutputBuf : %s\n", pOutputBuf);
	if(iLen > 0)
	{
		char		*pSearchValue = NULL;
		pSearchValue = strrchr(pOutputBuf, 44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 32;   //replace ',' with ' '
		}

		*ppBuf = pOutputBuf;
		/*return buffer len*/
		return iLen;
	}
	else
	{

		iLen = sprintf(pOutputBuf, "%s", "  ");
		*ppBuf = pOutputBuf;
		//DELETE(pOutputBuf);
		//pOutputBuf = NULL;
		return iLen;
	}
}
/*==========================================================================*
* FUNCTION :  Web_MakeSigValOfUserDefPage()
* PURPOSE  :   
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iLanguage:
IN int iEquipID:
IN OUT  char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WJ               DATE: 2004-10-6 11:08
*==========================================================================*/
static int Web_MakeSigValOfUserDefPage(IN int iLanguage, 
										IN int iPageID,
										OUT char **ppBuf)
{
	int					iNum, iLen = 0;
	char				*pMBuf = NULL;
	char				*pSamplerName = NULL;
	SET_SIG_VALUE		*pParamSigValue = NULL;
	SET_SIG_VALUE		*pSigValue = NULL;
	//char				szTime[32];
	time_t				tmSample;
	//char				szTimeValue[32];
	int					iSetSigNum = 0;
	int					i;

	int				nInterfaceType = VAR_USER_DEF_PAGES;		
	int				nVarID = 0;
	int				nVarSubID = 0;
	int				nBufLen;
	int				nTimeOut = 0;
	int				nError = ERR_DXI_OK;
	int				iError = ERR_DXI_OK;
	SIG_ITEM		*pSigItemTemp = NULL;

	USER_DEF_PAGES *pUserDefPages = NULL;
	EQUIP_INFO *pEquipInfo = NULL;


	nError = DxiGetData(nInterfaceType,
		nVarID,			
		nVarSubID,		
		&nBufLen,			
		&pUserDefPages,			
		nTimeOut);

	pSigItemTemp = pUserDefPages->pSigItemInfo->pSigItem;

	for(i = 0;i<pUserDefPages->pSigItemInfo->iSigItemNum;i++)
	{
		if(pSigItemTemp->iPageID == iPageID)
		{
			iSetSigNum++;
		}

		pSigItemTemp++;
	}


	if(iSetSigNum > 0 && iSetSigNum < 100)
	{
		pMBuf = NEW(char, iSetSigNum * MAX_COMM_RETURN_DATA_LINE_SIZE);
	}
	else if(iSetSigNum > 100)
	{
		pMBuf = NEW(char, 100 * MAX_COMM_RETURN_DATA_LINE_SIZE);
	}
	else
	{
		pMBuf = NEW(char, 1 * MAX_COMM_RETURN_DATA_LINE_SIZE);
	}



	/*get setting signal value ,*/


	int		iSigNumTemp = 0;

	nInterfaceType = VAR_A_SIGNAL_VALUE;

	pSigItemTemp = pUserDefPages->pSigItemInfo->pSigItem;

	for(i = 0;i<pUserDefPages->pSigItemInfo->iSigItemNum && iSigNumTemp < 100 ;i++ ,pSigItemTemp++)
	{
		if(pSigItemTemp->iPageID == iPageID)
		{
			iSigNumTemp++;

			nVarID = pSigItemTemp->iEquipID; //Equip ID
			nVarSubID = DXI_MERGE_SIG_ID(2,pSigItemTemp->iSigID);


			iError = DxiGetData(VAR_A_EQUIP_INFO,
				nVarID,			
				0,		
				&iBufLen,			
				&pEquipInfo,			
				0);

			if(nError == ERR_DXI_OK)
			{

				if(pEquipInfo->bWorkStatus == FALSE)
				{
					continue;
				}	
			}
			else
			{
				continue;

			}

			nError = DxiGetData(nInterfaceType,
				nVarID,			
				nVarSubID,		
				&nBufLen,			
				&pSigValue,			
				nTimeOut);

			if(nError == ERR_DXI_OK)
			{
				pParamSigValue = pSigValue;
			}
			else
			{
				continue;
			}

			if(STD_SIG_IS_DISPLAY_ON_UI(pParamSigValue->pStdSig,DISPLAY_WEB))
			{

				pSamplerName = Web_GetSamplerNameById(iLanguage,pParamSigValue->sv.iSamplerID);
				if(pSamplerName == NULL )
				{
					pSamplerName = NEW(char, 16);
					if(pSamplerName != NULL)
					{
						strncpyz(pSamplerName,"--",16);

					}
					else
					{
						return FALSE;
					}

				}

				tmSample = pParamSigValue->bv.tmCurrentSampled;

				if(pParamSigValue->pStdSig->iSigValueType  == VAR_FLOAT)
				{

					iLen  +=  sprintf(pMBuf + iLen,"\t %f, %ld, \"%s\", %4d, %d,%d,\n",
						pParamSigValue->bv.varValue.fValue,
						tmSample,
						pSamplerName,
						pParamSigValue->sv.iControlChannel,
						(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
						SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));
				}
				else if(pParamSigValue->pStdSig->iSigValueType  == VAR_LONG)
				{
					////TRACE("*****************[%d]********************\n", iNum);
					iLen  += sprintf(pMBuf + iLen,"\t %8ld, %ld, \"%s\", %4d, %d,%d,\n",
						pParamSigValue->bv.varValue.lValue,
						tmSample,
						pSamplerName,
						pParamSigValue->sv.iControlChannel,
						(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
						SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));
				}
				else if(pParamSigValue->pStdSig->iSigValueType == VAR_UNSIGNED_LONG)
				{

					iLen += sprintf(pMBuf + iLen,"\t %8lu, %ld, \"%s\", %4d, %d,%d,\n",
						pParamSigValue->bv.varValue.ulValue,
						tmSample,
						pSamplerName,
						pParamSigValue->sv.iControlChannel,
						(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
						SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));

					//DELETE(pTempBuf);
				}
				else if(pParamSigValue->pStdSig->iSigValueType == VAR_ENUM)
				{
					iLen += sprintf(pMBuf + iLen,"\t %d, %ld, \"%s\", %4d, %d,%d,\n",
						(int)pParamSigValue->bv.varValue.enumValue,
						tmSample,
						pSamplerName,
						pParamSigValue->sv.iControlChannel,
						(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
						SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));

				}
				else if(pParamSigValue->pStdSig->iSigValueType == VAR_DATE_TIME)
				{
					iLen += sprintf(pMBuf + iLen,"\t %ld, %ld, \"%s\", %4d, %d,%d,\n",
						pParamSigValue->bv.varValue.dtValue,
						tmSample,
						pSamplerName,
						pParamSigValue->sv.iControlChannel,
						(SET_SIG_IS_SET_ON_UI(pParamSigValue->pStdSig, SET_WEB)) ? SIG_VALUE_IS_SETTABLE(&pParamSigValue->bv) : 0,
						SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv) && (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv)));
				}


				DELETE(pSamplerName);
				pSamplerName = NULL;
			}
		}
	}
	if(iLen > 0)
	{
		char		*pSearchValue = NULL;
		pSearchValue = strrchr(pMBuf, 44);  //get the last ','
		if(pSearchValue != NULL)
		{
			*pSearchValue = 59;   //replace ',' with ';'
		}

		*ppBuf = pMBuf;
		return iLen;
	}
	else
	{

		iLen = sprintf(pMBuf, "%s;", "  ");
		//DELETE(pMBuf);
		//pMBuf = NULL;
		*ppBuf = pMBuf;
		return iLen;
	}
	
}

/*==========================================================================*
* FUNCTION :  Web_MakePlcWebPage
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_SendConfigureData 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_MakeGCWebPage(OUT char **ppszReturn, IN int iLanguage)
{
	TRACE(" ______into Web_MakeGCWebPage________\n");

	int		    i, iFirstRectID = 0, iEquipNum = 0, iBufLen;
	int         isigNum;
	FILE        *fp          = NULL;
	EQUIP_INFO  *pEquipInfo  = NULL;
	char        *pSearchValue= NULL;
	char        *pszSampling = NULL;
	char        *pszControl  = NULL;
	char        *pszSetting  = NULL;
	char        *pszAlarm    = NULL;
	char        *pszEquip    = NULL;
	char        *szFile      = NULL;
	char        *pPageFile   = NULL;
	char        *pHtml       = NULL;
	char        *pszMakeVar  = NULL;
	char        *pszID       = NULL;
	char        *pszTemp     = NULL;
	char        *pszGCInfo  = NULL;
	int         iReturn;
	int         iEquipLen = 0, iSamplingLen = 0, iControlLen = 0, iSettingLen = 0, iAlarmLen = 0, iPLCInfoLen = 0;
	int         iSamplingNum = 0, iControlNum = 0,iSettingNum = 0, iAlarmNum = 0;
	SAMPLE_SIG_VALUE  *pSampleSigValue  = NULL;
	CTRL_SIG_VALUE    *pControlSigValue = NULL;
	SET_SIG_VALUE     *pSettingSigValue = NULL;
	ALARM_SIG_INFO   *pAlarmSigInfo   = NULL;

	/*get equip information*/
	int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	/*get equip number*/
	int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
		0,			
		0,		
		&iBufLen,			
		(void *)&iEquipNum,			
		0);

	if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
	{
		return 0;
	}

	TRACE(" ______into Web_MakeGCWebPage________------2\n");
	iPLCInfoLen = Web_LoadGCConfigFile(&pszGCInfo);//get GC Configure file Info
	TRACE(" ______into Web_MakeGCWebPage________------3\n");
	if(iPLCInfoLen == -1)
	{
		return 0;
	}

	char *pPSMode = NULL;
	int iError;
	iError = Web_GetGCPSMode(&pPSMode);
	if(iError != 1)
	{
		return 0;
	}

	TRACE("______Web_GetGCPSMode___OUT[%s]",pPSMode);


	pszSampling = NEW(char,MAX_BUFFER * 2);
	if (pszSampling == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakePlcWebPage: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_NO_MEMORY;
	}
	memset(pszSampling,0,MAX_BUFFER * 2);

	pszControl = NEW(char,MAX_BUFFER / 2);
	if (pszControl == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakePlcWebPage: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_NO_MEMORY;
	}
	memset(pszControl,0,MAX_BUFFER / 2);

	pszSetting = NEW(char,MAX_BUFFER * 2/3);
	if (pszSetting == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakePlcWebPage: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_NO_MEMORY;
	}
	memset(pszSetting,0,MAX_BUFFER * 2/3);

	pszAlarm = NEW(char,MAX_BUFFER );
	if (pszAlarm == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakePlcWebPage: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_NO_MEMORY;
	}
	memset(pszAlarm,0,MAX_BUFFER );

	pszEquip = NEW(char,MAX_BUFFER);
	if (pszEquip == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakePlcWebPage: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_NO_MEMORY;
	}
	memset(pszEquip,0,MAX_BUFFER);

	pszSampling[iSamplingLen++] = '\t';
	pszControl[iControlLen++] = '\t';
	pszSetting[iSettingLen++] = '\t';
	pszAlarm[iAlarmLen++] = '\t';
	pszEquip[iEquipLen++] = '\t';



	for(i = 0; i < iEquipNum; i++)
	{
		if( pEquipInfo->pStdEquip->iTypeID == 1101 ||
			pEquipInfo->pStdEquip->iTypeID == 1600 || pEquipInfo->pStdEquip->iTypeID == 1601 ||
			pEquipInfo->pStdEquip->iTypeID == 1700 || pEquipInfo->pStdEquip->iTypeID == 1701 ||
			pEquipInfo->pStdEquip->iTypeID == 1800 || pEquipInfo->pStdEquip->iTypeID == 1801 ||
			(pEquipInfo->pStdEquip->iTypeID >= 1900 && pEquipInfo->pStdEquip->iTypeID <= 1908) ||
			pEquipInfo->pStdEquip->iTypeID == 1300 || pEquipInfo->pStdEquip->iTypeID == 1301 ||
			pEquipInfo->pStdEquip->iTypeID == 1400 || pEquipInfo->pStdEquip->iTypeID == 1401 ||
			pEquipInfo->pStdEquip->iTypeID == 2000 || pEquipInfo->pStdEquip->iTypeID == 2001 ||
			pEquipInfo->pStdEquip->iTypeID == 305 || pEquipInfo->pStdEquip->iTypeID == 306)
		{
			pEquipInfo++;
			continue;
		}


		iEquipLen += sprintf(pszEquip + iEquipLen, "new OEquip(%4d, \"%s\"),\n",
			pEquipInfo->iEquipID, 
			pEquipInfo->pEquipName->pAbbrName[iLanguage]);

		iSamplingNum = Web_GetSampleSignalByEquipID(pEquipInfo->iEquipID, &pSampleSigValue);

		for(isigNum = 0; isigNum < iSamplingNum; isigNum++)
		{
			if(STD_SIG_IS_DISPLAY_ON_UI(pSampleSigValue->pStdSig,DISPLAY_WEB))
			{
				//OSample( equipId,signalId,signalName,valueType,valueState) 
				iSamplingLen += sprintf(pszSampling + iSamplingLen, "new OSample( %4d,%4d,\"%s\",%d,%d),\n",
					pEquipInfo->iEquipID,
					pSampleSigValue->pStdSig->iSigID,
					pSampleSigValue->pStdSig->pSigName->pAbbrName[iLanguage],
					pSampleSigValue->bv.ucType,
					SIG_VALUE_IS_VALID(&pSampleSigValue->bv));
			}

			pSampleSigValue++;
		}

		iControlNum  = Web_GetControlSignalByEquipID(pEquipInfo->iEquipID, &pControlSigValue);

		for(isigNum = 0; isigNum < iControlNum; isigNum++)
		{
			//OControl( equipId,signalId,signalName,valueType,valueState) 
			iControlLen += sprintf(pszControl + iControlLen, "new OControl( %4d,%4d,\"%s\",%d,%d),\n",
				pEquipInfo->iEquipID,
				pControlSigValue->pStdSig->iSigID,
				pControlSigValue->pStdSig->pSigName->pAbbrName[iLanguage],
				pControlSigValue->bv.ucType,
				SIG_VALUE_IS_VALID(&pControlSigValue->bv));

			pControlSigValue++;
		}

		iSettingNum  = Web_GetSettingSignalByEquipID(pEquipInfo->iEquipID, &pSettingSigValue);

		for(isigNum = 0; isigNum < iSettingNum; isigNum++)
		{
			//OSetting( equipId,signalId,signalName,valueType,valueState) 
			iSettingLen += sprintf(pszSetting + iSettingLen, "new OSetting( %4d,%4d,\"%s\",%d,%d),\n",
				pEquipInfo->iEquipID,
				pSettingSigValue->pStdSig->iSigID,
				pSettingSigValue->pStdSig->pSigName->pAbbrName[iLanguage],
				pSettingSigValue->bv.ucType,
				SIG_VALUE_IS_VALID(&pSettingSigValue->bv));
			pSettingSigValue++;
		}

		iAlarmNum    = Web_GetAlarmSigStruofEquip(pEquipInfo->iEquipID, &pAlarmSigInfo);

		for(isigNum = 0; isigNum < iAlarmNum; isigNum++)
		{
			//OAlarm( equipId,signalId,signalName,valueType,valueState) 
			iAlarmLen += sprintf(pszAlarm + iAlarmLen, "new OAlarm( %4d,%4d,\"%s\",5,1),\n",
				pEquipInfo->iEquipID,
				pAlarmSigInfo->iSigID,
				pAlarmSigInfo->pSigName->pAbbrName[iLanguage]);

			pAlarmSigInfo++;
		}

		pEquipInfo++;

	}

	printf(" ______into Web_MakeGCWebPage________------4\n");

	pSearchValue = strrchr(pszEquip,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	pSearchValue = strrchr(pszSampling,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	pSearchValue = strrchr(pszControl,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	pSearchValue = strrchr(pszSetting,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	pSearchValue = strrchr(pszAlarm,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	pSearchValue = strrchr(pszGCInfo,44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}


	char *pResult = NEW(char,iEquipLen + iSamplingLen + iControlLen + iSettingLen + iAlarmLen + iPLCInfoLen  +strlen(pPSMode) + 8);
	strcpy(pResult,pszEquip);
	pszSampling[0] = ';';
	strcat(pResult,pszSampling);
	pszControl[0] = ';';
	strcat(pResult,pszControl);
	pszSetting[0] = ';';
	strcat(pResult,pszSetting);
	pszAlarm[0] = ';';
	strcat(pResult,pszAlarm);
	strcat(pResult,pszGCInfo);
	strcat(pResult,pPSMode);

	*ppszReturn = pResult;

	//FILE *pf = fopen("/var/replaceStr.txt","a+");
	//fwrite(pResult,strlen(pResult),1,pf);
	//fclose(pf);


	DELETE(pszSampling);
	DELETE(pszControl);
	DELETE(pszSetting);
	DELETE(pszAlarm);
	DELETE(pszEquip);
	DELETE(pszGCInfo);
	DELETE(pPSMode);

	printf(" ______into Web_MakeGCWebPage________------5\n");
	return TRUE;

}
/*==========================================================================*
* FUNCTION :  Web_LoadGCConfigFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY:   
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int Web_LoadGCConfigFile(char **ppszReturn)
{    
	char *szFullPath = WEB_GC_CFG_FILE_PATH;
	WEB_GC_CFG_INFO_LINE *temp = NULL;
	int i = 0;
	int iLen = 0;

	WEB_GC_CFC_INFO *stGCConfigInfo = NULL;
	stGCConfigInfo = NEW(WEB_GC_CFC_INFO,1);
	if (stGCConfigInfo == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakePlcConfigFile: "
			"ERROR: There is no enough memory! ", 
			__FILE__);
		return ERR_NO_MEMORY;
	}
	stGCConfigInfo->iNumber = 0;

	char *pszResult = NULL;
	pszResult = NEW(char, MAX_LINE_SIZE*100);
	memset(pszResult,0,MAX_LINE_SIZE*100);
	if (pszResult == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_MakeGCConfigFile: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_NO_MEMORY;
	}

	if (Mutex_Lock(GetMutexLoadGCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
		== ERR_MUTEX_OK)
	{

		if(Cfg_LoadConfigFile(szFullPath,LoadGCConfigFile,stGCConfigInfo)!=0)
		{
			TRACEX("____Cfg_LoadConfigFile error!_______\n");
			Mutex_Unlock(GetMutexLoadGCConfig());
			TRACEX("____stGCConfigInfo->iNumber[%d]_______\n",stGCConfigInfo->iNumber);
			if(stGCConfigInfo->iNumber>0)
			{
				TRACEX("____stGCConfigInfo->iNumber[%d]_______\n",stGCConfigInfo->iNumber);
				TRACEX("____Cfg_LoadConfigFile error2!_______\n");
				temp = stGCConfigInfo->stWebGCConfigLine;
				for(i = 0; i < stGCConfigInfo->iNumber; i++)
				{
					TRACEX("____Cfg_LoadConfigFile error3!_______\n");
					SAFELY_DELETE(temp->pszPSSigName);
					SAFELY_DELETE(temp->pszEquipId);
					SAFELY_DELETE(temp->pszSigType);
					SAFELY_DELETE(temp->pszSigId);


					temp++; 
				}

				SAFELY_DELETE(stGCConfigInfo->stWebGCConfigLine);
			}
			SAFELY_DELETE(stGCConfigInfo);
			return -1;
		}

		Mutex_Unlock(GetMutexLoadGCConfig());

	}

	//TRACE("Web_LoadPlcConfigFile OK!!!\n");
	pszResult[iLen++] = ';';

	temp = stGCConfigInfo->stWebGCConfigLine;
	if(stGCConfigInfo->iNumber > 0)
	{
		for(i = 0; i < stGCConfigInfo->iNumber; i++)
		{
			iLen += sprintf(pszResult + iLen,"\"%s\t%s\t%s\t%s\",\n",temp->pszPSSigName,
				temp->pszEquipId,
				temp->pszSigType,
				temp->pszSigId);
			temp++;
		}

	}
	else
	{
		iLen += sprintf(pszResult + iLen,"\t\t\t\t\t\t\n");
	}


	*ppszReturn = pszResult;

	/*FILE *pf = NULL;
	pf = fopen("/var/wj1.txt","w+");
	fwrite(pszResult,strlen(pszResult),1,pf);
	fclose(pf);*/


	temp = stGCConfigInfo->stWebGCConfigLine;
	for(i = 0; i < stGCConfigInfo->iNumber; i++)
	{
		SAFELY_DELETE(temp->pszPSSigName);
		SAFELY_DELETE(temp->pszEquipId);
		SAFELY_DELETE(temp->pszSigType);
		SAFELY_DELETE(temp->pszSigId);


		temp++; 
	}

	SAFELY_DELETE(stGCConfigInfo->stWebGCConfigLine);
	SAFELY_DELETE(stGCConfigInfo);

	return iLen+1;

}

/*==========================================================================*
* FUNCTION :  LoadGCConfigFile
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int LoadGCConfigFile(IN void *pCfg, OUT void *pLoadToBuf)
{
	//TRACE("Into LoadPlcConfigFile!!!\n");
	CONFIG_TABLE_LOADER loader[1];
	WEB_GC_CFC_INFO *pBuf = NULL;
	pBuf = (WEB_GC_CFC_INFO *)pLoadToBuf;



	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pBuf->iNumber), 
		GC_PS_INFO,
		&(pBuf->stWebGCConfigLine), 
		ParseGCTableProc);

	if(Cfg_LoadTables(pCfg, 1, loader)!= ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	//TRACE("LoadPlcConfigFile OK!!!\n");

	return ERR_CFG_OK;

}
/*==========================================================================*
* FUNCTION :  ParsePLCTableProc
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/
static int ParseGCTableProc(IN char *szBuf, OUT WEB_GC_CFG_INFO_LINE *pStructData)
{
	//TRACE("Into ParseGCTableProc!!!\n");
	//#InputFromMaster	EquipmentId	SignalType	SignalId
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);


	//TRACE("INTO ParsePLCTableProc!!!!\n");

	/* 1.InputFromMaster field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszPSSigName = NEW_strdup(pField);
	}
	else
	{
		return 1;
	}
	//TRACE("Operator field OK!!!%s\n", pStructData->pszOperator);
	/* 2.EquipmentId field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszEquipId = NEW_strdup(pField);
	}
	else
	{
		return 2;
	}
	//TRACE("Input1 field OK!!!%s\n",pStructData->pszInput1);
	/* 3.SignalType field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszSigType = NEW_strdup(pField);
	}
	else
	{
		return 3;
	}
	//TRACE("Input2 field OK!!!%s\n",pStructData->pszInput2);
	/* 4.SignalId field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszSigId = NEW_strdup(pField);
	}
	else
	{
		return 4;
	}


	//TRACE("ParsePLCTableProc OK!!!\n");

	return 0;



}
/*==========================================================================*
* FUNCTION :  GetMutexLoadPLCConfig
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing               DATE: 2006-05-15 11:08
*==========================================================================*/

HANDLE GetMutexLoadGCConfig(void)
{
	return s_hMutexLoadGCCFGFile;
}
/*==========================================================================*
* FUNCTION :  Web_ModifyGCPSMode
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_SendConfigureData  
* ARGUMENTS:  
* RETURN   : int:6 for success!
* COMMENTS : new function,added for CR# 0132-06-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_ModifyGCPSMode(IN char *pszMode)
{
	TRACE("________INTO__Web_ModifyGCPSMode______");
	char			*pSearchValue = NULL, *str_string = NULL, szReadString[256];
	FILE			*fp;
#define MAX_READ_LINE_LEN			256
	int flage = 0;
	if (Mutex_Lock(GetMutexLoadGCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
		== ERR_MUTEX_OK)
	{
		if((fp = fopen(WEB_GC_CFG_FILE_PATH, "rb+")) != NULL)
		{
			while(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
			{
				if((pSearchValue = strstr(szReadString,GC_PS_MODE)) != NULL)
				{
					while( flage == 0)
					{
						if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
						{
							if(szReadString[0] != '#')
							{
								fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
								if(pszMode != NULL)
								{
									str_string = NEW(char , strlen(szReadString) + 1);
									memset(str_string,0, strlen(szReadString) + 1);
									sprintf(str_string,"%s ",Cfg_RemoveWhiteSpace(pszMode));
									//TRACE("str_string[%s]\n", str_string);
									//strncpyz(str_string,Cfg_RemoveWhiteSpace(szLanguage),strlen(szReadString) + 1);
									fputs(str_string, fp);

									DELETE(str_string);
									str_string = NULL;
								}

								flage = 1;
							}

						}
					}

				}

			}
			fclose(fp);
		}
		else
		{
			Mutex_Unlock(GetMutexLoadGCConfig());
			return  4;
		}
		Mutex_Unlock(GetMutexLoadGCConfig());
	}
	else
	{
		Mutex_Unlock(GetMutexLoadGCConfig());
		return  5;
	}

	TRACE("______Web_ModifyGCPSMode__OK!!!______");
	return 6;
}
/*==========================================================================*
* FUNCTION :  Web_ModifyGCPSInfo
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_SendConfigureData  
* ARGUMENTS:  
* RETURN   : int:6 for success!
* COMMENTS : new function,added for CR# 0132-06-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/

static int Web_ModifyGCPSInfo(IN char *pszGCPSInfo)
{ 

	FILE *pFile = NULL;
	long lFileLen;
	long lCurrPos;
	char *pszFile = NULL;
	char *pszOutFile = NULL;
	void *pProf = NULL;
	int flage = 0;
	char pLineBuf[MAX_LINE_SIZE];
	char *pPosition = NULL;
	int iIDBufLen = 0;
	int iLen = 0;



	WEB_GC_CFG_INFO_LINE *pszINGCPSInfo;
	pszINGCPSInfo = NEW(WEB_GC_CFG_INFO_LINE,1);
	if (pszINGCPSInfo == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_Web_ModifyGCPSInfo: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return 5;
	}

	if(ParseGCTableProc(pszGCPSInfo, pszINGCPSInfo)!=0)
	{
		SAFELY_DELETE(pszINGCPSInfo);
		return 5;
	}

	WEB_GC_CFG_INFO_LINE *pszTempGCPSInfo;
	pszTempGCPSInfo = NEW(WEB_GC_CFG_INFO_LINE,1);
	if (pszTempGCPSInfo == NULL)
	{
		AppLogOut("WEB", APP_LOG_ERROR, 
			"[%s]--Web_Web_ModifyGCPSInfo: "
			"ERROR: There is no enough memory! ", 
			__FILE__);

		return 5;
	}


	if (Mutex_Lock(GetMutexLoadGCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
		== ERR_MUTEX_OK)
	{
		pFile = fopen(WEB_GC_CFG_FILE_PATH,"r+");
		if(pFile == NULL)
		{
			Mutex_Unlock(GetMutexLoadGCConfig());
			return 5;
		}

		lFileLen = GetFileLength(pFile);
		TRACE("FileLen : %ld\n",lFileLen);

		pszFile = NEW(char, lFileLen + 1);
		if (pszFile == NULL)
		{
			AppLogOut("WEB", APP_LOG_ERROR, 
				"[%s]--Web_SetAlarmSuppressingExp: "
				"ERROR: There is no enough memory! ", 
				__FILE__);
			fclose(pFile);
			Mutex_Unlock(GetMutexLoadGCConfig());
			return 5;
		}
		memset(pszFile, 0, lFileLen + 1);

		pszOutFile = NEW(char, lFileLen + 512);//MAX_LINE_SIZE=512
		if (pszOutFile == NULL)
		{
			AppLogOut("WEB", APP_LOG_ERROR, 
				"[%s]--Web_SetAlarmSuppressingExp: "
				"ERROR: There is no enough memory! ", 
				__FILE__);
			fclose(pFile);
			Mutex_Unlock(GetMutexLoadGCConfig());
			return 5;
		}
		memset(pszOutFile, 0, lFileLen + 1);

		fread(pszFile, 1, (size_t)lFileLen, pFile);
		fclose(pFile);

		pProf = Cfg_ProfileOpen(pszFile,lFileLen);
		if(Cfg_ProfileFindSection(pProf,GC_PS_INFO) == 0)
		{
			AppLogOut("WEB", APP_LOG_ERROR, 
				"[%s]--Web_SetAlarmSuppressingExp: "
				"ERROR: Can't find Section! ", 
				__FILE__);      
			DELETE(pProf);
			Mutex_Unlock(GetMutexLoadGCConfig());
			return 5;
		}

		lCurrPos = Cfg_ProfileTell(pProf);
		TRACE("lCurrPos : %ld\n",lCurrPos);

		memmove(pszOutFile, pszFile, lCurrPos);

		char *pLineTemp = NULL;

		while(Cfg_ProfileReadLine(pProf, pLineBuf, MAX_LINE_SIZE) != 0)
		{
			//TRACE("\n1________%s__________\n",pLineBuf);
			if(pLineBuf[0] == '#' && flage != 1)
			{
				iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
				memset(pLineBuf,0,MAX_LINE_SIZE);
			}
			else if(pLineBuf[0] != '#' && flage != 1 && pLineBuf[0] != '[')
			{
				pLineTemp = NEW_strdup(pLineBuf);
				if(ParseGCTableProc(pLineTemp,pszTempGCPSInfo)!=0)
				{
					iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s",pLineBuf);
					memset(pLineBuf,0,MAX_LINE_SIZE);
					continue;
				}

				if(!strcmp(pszINGCPSInfo->pszPSSigName,pszTempGCPSInfo->pszPSSigName))
				{

					iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s\t\t%s\t\t%s\t\t%s", 
						pszINGCPSInfo->pszPSSigName,
						pszINGCPSInfo->pszEquipId,
						pszINGCPSInfo->pszSigType,
						pszINGCPSInfo->pszSigId);
					//TRACE("2\n________%s__________\n",pszGCPSInfo);
					memset(pLineBuf,0,MAX_LINE_SIZE);

				}
				else
				{
					iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s\t\t%s\t\t%s\t\t%s", 
						pszTempGCPSInfo->pszPSSigName,
						pszTempGCPSInfo->pszEquipId,
						pszTempGCPSInfo->pszSigType,
						pszTempGCPSInfo->pszSigId);
					//TRACE("3\n________%s__________\n",pLineBuf);
					memset(pLineBuf,0,MAX_LINE_SIZE);
				}

				SAFELY_DELETE(pszTempGCPSInfo->pszEquipId);
				SAFELY_DELETE(pszTempGCPSInfo->pszPSSigName);
				SAFELY_DELETE(pszTempGCPSInfo->pszSigId);
				SAFELY_DELETE(pszTempGCPSInfo->pszSigType);
				SAFELY_DELETE(pLineTemp);

			}
			else if(pLineBuf[0] == '[' && flage != 1 )
			{
				iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
				memset(pLineBuf,0,MAX_LINE_SIZE);
				flage = 1;
			}
			else if(flage == 1 )
			{
				iLen += sprintf(pszOutFile + lCurrPos + iLen,"\r\n%s", pLineBuf);
				memset(pLineBuf,0,MAX_LINE_SIZE);
			}


		}

		pFile = fopen(WEB_GC_CFG_FILE_PATH,"w+");
		fwrite(pszOutFile,strlen(pszOutFile),1,pFile);
		fclose(pFile);


		SAFELY_DELETE(pszFile);
		pszFile = NULL;
		SAFELY_DELETE(pszOutFile);
		pszOutFile = NULL;
		SAFELY_DELETE(pProf);

		Mutex_Unlock(GetMutexLoadGCConfig());
	}

	return 6;

}
/*==========================================================================*
* FUNCTION :  Web_GetGCPSMode
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_SendConfigureData  
* ARGUMENTS:  
* RETURN   : int:6 for success!
* COMMENTS : new function,added for CR# 0132-06-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_GetGCPSMode(char **ppszGCPSMode)
{
	TRACE(" ______into Web_GetGCPSMode________------\n");
	int		iRst;
	void	*pProf = NULL;  
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;



	char *szCfgFileName = WEB_GC_CFG_FILE_PATH; 

	//Cfg_GetFullConfigPath(CONFIG_FILE_WEB_PRIVATE_R, szCfgFileName, MAX_FILE_PATH);
	if (Mutex_Lock(GetMutexLoadGCConfig(), MAX_TIME_WAITING_LOAD_PLC_CONFIG) 
		== ERR_MUTEX_OK)
	{

		pFile = fopen(szCfgFileName, "r");
		if (pFile == NULL)
		{		
			Mutex_Unlock(GetMutexLoadGCConfig());
			return FALSE;

		}

		ulFileLen = GetFileLength(pFile);

		szInFile = NEW(char, ulFileLen + 1);
		if (szInFile == NULL)
		{
			Mutex_Unlock(GetMutexLoadGCConfig());
			fclose(pFile);
			return FALSE;
		}

		/* read file */
		ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
		fclose(pFile);

		if (ulFileLen == 0) 
		{
			/* clear the memory */
			Mutex_Unlock(GetMutexLoadGCConfig());
			DELETE(szInFile);
			return FALSE;
		}
		szInFile[ulFileLen] = '\0';  /* end with NULL */

		/* create SProfile */
		pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

		if (pProf == NULL)
		{
			Mutex_Unlock(GetMutexLoadGCConfig());
			DELETE(szInFile);
			return FALSE;
		}
		char	*pszMode = NEW(char,10);
		pszMode[0] = ';';
		//1.Read Local language version
		char   *ptemp = pszMode + 1;
		iRst = Cfg_ProfileGetString(pProf,
			GC_PS_MODE, 
			ptemp,
			9); 
		*ppszGCPSMode = pszMode;

		TRACE(" ______into Web_GetGCPSMode________2[%s]------\n",*ppszGCPSMode);

		//TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iPagesNumber);
		if (iRst != 1)
		{
			AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
				"There are no GCPSMode in gen_ctl.cfg.");
			Mutex_Unlock(GetMutexLoadGCConfig());
			DELETE(pProf); 
			DELETE(szInFile);
			return FALSE;
		}

		Mutex_Unlock(GetMutexLoadGCConfig());

	}

	DELETE(pProf); 
	DELETE(szInFile);
	TRACE(" ______into Web_GetGCPSMode________OK!!!------\n");
	return TRUE;

}

/*==========================================================================*
* FUNCTION :  Web_MakeGetSetParamWebPage
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_SendConfigureData  
* ARGUMENTS:  
* RETURN   : int:1 for success!
* COMMENTS : new function,added for TR# 62-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_MakeGetSetParamWebPage(void)
{
#define WEB_SETTINGPARAM_FILE_NAME			"/var/download/SettingParam.run"
	CFG_PARAM_RUN_INFO  *pCFGParam = NULL;

	EQUIP_INFO		*pEquipInfo = NULL;
	SET_SIG_VALUE *pTempSetSigVal = NULL;
	FILE *pParamFile = NULL;
	int iEquipNumber = 0;
	int iSetSigNum = 0;
	WORD dwSum = 0;
	WORD dwUserSum = 0;
	int iparamNum = 0;
	int i = 0;
	int j = 0;
	int iParamNo = -1;
	int sigType = 2;


	iparamNum = Web_GetSetParamNum();
	if(iparamNum == 0)
	{
		return  2;
	}
	pCFGParam = NEW(CFG_PARAM_RUN_INFO,1);
	if(pCFGParam == NULL)
	{
		return  2;
	}

	strcpy(pCFGParam->sFileHeadInfo,PARAM_FILE_HEAD_INFO);
	pCFGParam->iVersion = VER_PARAM_RECORD;
	pCFGParam->pParam = NEW(PERSISTENT_SIG_RECORD,iparamNum);
	if(pCFGParam->pParam == NULL)
	{
		DELETE(pCFGParam);
		return  2;

	}

	if((iEquipNumber = Web_GetEquipListFromInterface(&pEquipInfo)) <= 0)
	{
		return 2;
	}

	for ( i = 0; i < iEquipNumber && pEquipInfo != NULL; i++, pEquipInfo++)
	{
		iSetSigNum = pEquipInfo->pStdEquip->iSetSigNum;
		pTempSetSigVal = pEquipInfo->pSetSigValue;
		for(j = 0;j < iSetSigNum; pTempSetSigVal++, j++)
		{
			if((pTempSetSigVal->pStdSig->bPersistentFlag) &&
				(pTempSetSigVal->bv.tmCurrentSampled != 0))
			{
				iParamNo++;
				pCFGParam->pParam[iParamNo].iEquipID = pEquipInfo->iEquipID;
				pCFGParam->pParam[iParamNo].iSignalID = DXI_MERGE_SIG_ID(sigType,pTempSetSigVal->pStdSig->iSigID);

				switch(pTempSetSigVal->pStdSig->iSigValueType)
				{
				case VAR_LONG:
					pCFGParam->pParam[iParamNo].varSignalVal.lValue = pTempSetSigVal->bv.varValue.lValue;
					break;
				case VAR_FLOAT:
					pCFGParam->pParam[iParamNo].varSignalVal.fValue = pTempSetSigVal->bv.varValue.fValue;
					break;
				case VAR_UNSIGNED_LONG:
					pCFGParam->pParam[iParamNo].varSignalVal.ulValue = pTempSetSigVal->bv.varValue.ulValue;
					break;
				case VAR_DATE_TIME:
					pCFGParam->pParam[iParamNo].varSignalVal.dtValue = pTempSetSigVal->bv.varValue.dtValue;
					break;
				case VAR_ENUM:
					pCFGParam->pParam[iParamNo].varSignalVal.enumValue = pTempSetSigVal->bv.varValue.enumValue;
					break;
				}

				pCFGParam->pParam[iParamNo].tmSignalTime = pTempSetSigVal->bv.tmCurrentSampled;
			}


		}

	}
	pCFGParam->iUserNum = GetUserInfo(&(pCFGParam->pUserInfo));



	APP_SERVICE		*stAppService = NULL;
	int				iNMSNum = 0;
	int			iBufLen = 0;

	if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
	{
		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV2_NAME);
	}
	else
	{
		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV3_NAME);
	}	

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{
		if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										GET_NMS_USER_INFO,
										&iBufLen,
										(void *)&pCFGParam->pNMSInfo) == ERR_SNP_OK)
		{			
			pCFGParam->iNMSNum =  iBufLen /sizeof(NMS_INFO);
		}
	}
	else
	{
		pCFGParam->iNMSNum = 0;
		pCFGParam->pNMSInfo = NULL;
	}
	
	if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
	{
		pCFGParam->iNMS3Num = 0;
		pCFGParam->pNMS3Info = NULL;
		stAppService =  NULL;
	}
	else
	{
		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
	}

	

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{
		if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										GET_NMS_V3_USER_INFO,
										&iBufLen,
										(void *)&pCFGParam->pNMS3Info) == ERR_SNP_OK)
		{			
			pCFGParam->iNMS3Num =  iBufLen /sizeof(V3NMS_INFO);	

		}
	}
	else
	{
		pCFGParam->iNMS3Num = 0;
		pCFGParam->pNMS3Info = NULL;
	}
	
	pCFGParam->iParamNum = iParamNo + 1;

	TRACE("\n_______pCFGParam->iParamNum = %d________\n",pCFGParam->iParamNum);


	int  nChrCount = pCFGParam->iParamNum * sizeof(PERSISTENT_SIG_RECORD);
	char *pFrame = (char *)pCFGParam->pParam;

	for (i = 0; i < nChrCount; i++)
	{
		dwSum += *pFrame;
		pFrame++;
	}

	nChrCount = pCFGParam->iUserNum * sizeof(USER_INFO_STRU);
	pFrame = (char *)pCFGParam->pUserInfo;
	dwUserSum = 0;
	for (i = 0; i < nChrCount; i++)
	{
		dwUserSum += *pFrame;
		pFrame++;
	}

	nChrCount = pCFGParam->iNMSNum * sizeof(NMS_INFO);
	pFrame = (char *)pCFGParam->pNMSInfo;

	for (i = 0; i < nChrCount; i++)
	{
		dwUserSum += *pFrame;
		pFrame++;
	}

	nChrCount = pCFGParam->iNMS3Num * sizeof(V3NMS_INFO);
	pFrame = (char *)pCFGParam->pNMS3Info;

	for (i = 0; i < nChrCount; i++)
	{
		dwUserSum += *pFrame;
		pFrame++;
	}


	pCFGParam->iCheckSum = (WORD)-dwSum;
	pCFGParam->iCheckUserSum = (WORD)-dwUserSum;


	/*printf("\npCFGParam->iCheckUserSum=%d\n",pCFGParam->iCheckUserSum);
	printf("\ndwSum=%d\n",dwSum);*/


	pParamFile = fopen(WEB_SETTINGPARAM_FILE_NAME,"wb+");
	fwrite(&pCFGParam->sFileHeadInfo,(size_t)ITEM_OF(pCFGParam->sFileHeadInfo),1,pParamFile);
	fwrite(&pCFGParam->iVersion,sizeof(int),1,pParamFile);
	fwrite(&pCFGParam->iParamNum,sizeof(int),1,pParamFile);
	fwrite(pCFGParam->pParam,sizeof(PERSISTENT_SIG_RECORD),pCFGParam->iParamNum,pParamFile);

	fwrite(&pCFGParam->iUserNum,sizeof(int),1,pParamFile);
	fwrite(pCFGParam->pUserInfo,sizeof(USER_INFO_STRU),pCFGParam->iUserNum,pParamFile);
	fwrite(&pCFGParam->iNMSNum,sizeof(int),1,pParamFile);
	fwrite(pCFGParam->pNMSInfo,sizeof(NMS_INFO),pCFGParam->iNMSNum,pParamFile);
	fwrite(&pCFGParam->iNMS3Num,sizeof(int),1,pParamFile);
	fwrite(pCFGParam->pNMS3Info,sizeof(V3NMS_INFO),pCFGParam->iNMS3Num,pParamFile);

	fwrite(&pCFGParam->iCheckUserSum,sizeof(int),1,pParamFile);
	fwrite(&pCFGParam->iCheckSum,sizeof(int),1,pParamFile);

	fclose(pParamFile);

	DELETE(pCFGParam->pParam);

	//Do not delete the user/SNMP/SNMPV3 information	DELETE(pCFGParam->pUserInfo);
	//DELETE(pCFGParam->pNMSInfo);
	//DELETE(pCFGParam->pNMS3Info);
	

	DELETE(pCFGParam);
	return 1;
}

/*==========================================================================*
* FUNCTION :  Web_GetSetParamNum
* PURPOSE  :  
* CALLS    : 
* CALLED BY: Web_MakeGetSetParamWebPage  
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : new function,added for TR# 62-ACU
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_GetSetParamNum(void)
{
	int iAllSetSigNum = 0;
	EQUIP_INFO		*pEquipInfo = NULL;
	int iEquipNumber = 0;
	int i = 0;

	if((iEquipNumber = Web_GetEquipListFromInterface(&pEquipInfo)) <= 0)
	{
		return FALSE;
	}

	for ( i = 0; i < iEquipNumber && pEquipInfo != NULL; i++, pEquipInfo++)
	{
		iAllSetSigNum = iAllSetSigNum + pEquipInfo->pStdEquip->iSetSigNum;
	}

	return iAllSetSigNum;
}

/*==========================================================================*
* FUNCTION :  Web_ResetNetDriver()
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
*==========================================================================*/
static int Web_ResetNetDriver(void)
{
	int iTimerSet;

	system("/home/ipreset.sh");

	TRACE("\nWeb_ResetNetDriver:NET DRIVER RESET!!!\n ");


	/*iTimerSet = Timer_Reset(RunThread_GetId("WEB_UI"),WEB_NET_RESET_TIMER);
	if(iTimerSet == ERR_OK)
	{
		TRACE("\n Web_ResetNetDriver:Reset Timer OK!!!\n");
	}*/


	return TIMER_CONTINUE_RUN;
}

//end////////////////////////////////////////////////////////////////////////

/*==========================Support Netscape================================================*/
#ifdef ADD_SUPPORT_NETSCAPE	
static int Web_SendNetscapeCommand(IN char *buf, 
				   IN char *filename)
{

	TRACE("\n_____into Web_SendNetscapeCommand____\n");
	char *ptr =  buf;
	char szValue[32];
	char szWriteUserName[40];
	VAR_VALUE_EX	value;
	int iEquipID,iSigType,iSigID;
	char szControlResult[3];
	int fd2;
	int iReturn;
	int iError;
	void		*pSigStruc = NULL;
	int iEnumValue = 0;

#define ID_LEN					3

	if((fd2 = open(filename,O_WRONLY )) < 0)
	{
		return FALSE;

	}
	//printf("\n{%s}", filename);

	ptr = ptr + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;

	char szGetID[ID_LEN + 1];
	//jump the iModifyPasswd
	ptr =  ptr + 2;

	//Get equip ID
	strncpyz(szGetID,ptr,sizeof(szGetID));
	ptr +=	ID_LEN;
	iEquipID = atoi(szGetID);


	//Get signal type
	strncpyz(szGetID,ptr,sizeof(szGetID));
	ptr +=  ID_LEN;
	iSigType = atoi(szGetID);

	//Get signal ID
	strncpyz(szGetID,ptr,sizeof(szGetID));
	ptr +=  ID_LEN;
	iSigID = atoi(szGetID);


	//Get value
	strncpyz(szValue, ptr,sizeof(szValue));

	int iAlarmLevel = atoi(szValue);
	printf("------{%d}{%d}{%d}{%s}---------\n",iEquipID,iSigID,iSigType,szValue);

	if(iSigType == 3)
	{
		//printf("------{%d}{%d}{%d}{%d}---------\n",iEquipID,iSigID,iSigType,iAlarmLevel);
		if(Web_ModifySignalAlarmLevel(iEquipID,iSigType,iSigID,iAlarmLevel,"NetscapeUser"))
		{
			strncpyz(szControlResult,"1", sizeof(szControlResult));
		}
		else
		{
			strncpyz(szControlResult,"0", sizeof(szControlResult));
		}


	}
	else
	{
		sprintf(szWriteUserName, "%s%-s",  CGI_WEB_USER, "NETSCAPE");

		printf("{%d}{%d}{%d}{%s}szWriteUserName:%s",iEquipID,iSigID,iSigType,szValue, szWriteUserName);

		value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
		value.pszSenderName = szWriteUserName;
		value.nSenderType = SERVICE_OF_USER_INTERFACE;

		iError = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
			iEquipID,			
			DXI_MERGE_SIG_ID(iSigType, iSigID),		
			&iBufLen,			
			(void *)&pSigStruc,			
			iTimeOut);
		int iValueType ;
		if (iError == ERR_DXI_OK)
		{
			LANG_TEXT	**ppStateKao;
			int		iStateNum;
			if(iSigType == 1)
			{
				iValueType = ((CTRL_SIG_INFO *)pSigStruc)->iSigValueType;
				ppStateKao =((CTRL_SIG_INFO *)pSigStruc)->pStateText;
				iStateNum = ((CTRL_SIG_INFO *)pSigStruc)->iStateNum;
			}
			else if(iSigType == 2)
			{
				iValueType = ((SET_SIG_INFO *)pSigStruc)->iSigValueType;
				ppStateKao =((SET_SIG_INFO *)pSigStruc)->pStateText;
				iStateNum = ((SET_SIG_INFO *)pSigStruc)->iStateNum;
			}



			if(iValueType == VAR_ENUM)
			{
				int i;
				for( i = 0; i < iStateNum;i++)
				{
					printf("\n{%s}{%s}",Cfg_RemoveWhiteSpace(szValue), ppStateKao[i]->pAbbrName[0]);
					if(strcasecmp(Cfg_RemoveWhiteSpace(szValue), ppStateKao[i]->pAbbrName[0]) == 0 )
					{
						iEnumValue = i;
						break;
					}

				}
				if(iStateNum == 1)
				{
					iEnumValue += 1;
				}
				printf("\niEnumValue = %d\n", iEnumValue);
			}

		}
		else
		{
			return FALSE;
		}


		printf("\niValueType :%d {%d}", iValueType, iEnumValue);
		BOOL boolInvalidValue = TRUE;
		//Judge the value type 
		if(iValueType == VAR_ENUM)
		{
			value.varValue.enumValue = iEnumValue;
		}
		else if(iValueType == VAR_FLOAT)
		{
			if(!IsValidFloat(Cfg_RemoveWhiteSpace(szValue)))
			{
				printf("\nIsValidFloat:false{%s}", Cfg_RemoveWhiteSpace(szValue));
				strncpyz(szControlResult,"0", sizeof(szControlResult));
				boolInvalidValue = FALSE;
			}
			else
			{
				value.varValue.fValue = atof(Cfg_RemoveWhiteSpace(szValue));
				printf("\nvalue.varValue.fValue = %f", value.varValue.fValue);
			}
		}
		else if(iValueType == VAR_UNSIGNED_LONG)
		{
			if(!IsValidWORD(Cfg_RemoveWhiteSpace(szValue)))
			{
				printf("\IsValidWORD:false");
				strncpyz(szControlResult,"0", sizeof(szControlResult));
				boolInvalidValue = FALSE;
			}
			else
			{
				value.varValue.ulValue = (unsigned long)atol(Cfg_RemoveWhiteSpace(szValue));
			}
		}
		else if(iValueType == VAR_LONG)
		{
			if(!IsValidLong(Cfg_RemoveWhiteSpace(szValue)))
			{
				strncpyz(szControlResult,"0", sizeof(szControlResult));

				boolInvalidValue = FALSE;
			}
			else
			{
				value.varValue.lValue = atol(Cfg_RemoveWhiteSpace(szValue));
			}
		}
		else if(iValueType == VAR_DATE_TIME)
		{
			if(!IsValidDatetime(Cfg_RemoveWhiteSpace(szValue)))
			{
				//printf("\nNo");
				strncpyz(szControlResult,"0", sizeof(szControlResult));
				boolInvalidValue = FALSE;
			}
			else
			{
				//printf("\nYes");
				value.varValue.dtValue = (time_t)Web_Net_transferCharToTime(szValue);
				//printf("\nYes %ld {%s}", value.varValue.dtValue,ctime(&value.varValue.dtValue));
			}
		}
		else
		{
			strncpyz(szControlResult,"0", sizeof(szControlResult));
			boolInvalidValue = FALSE;
		}

		if(boolInvalidValue)
		{
			iReturn = Web_ModifySignalValue(iEquipID,iSigType,iSigID,value);

			printf("\nWeb_ModifySignalValue %d", iReturn);

			printf("\niReturn=%d", iReturn);

			if(iReturn == MODIFY_SIGNALVALUE_SUPPRESSED)
			{
				if(Web_ACUIsAuto())	//ACU is auto status
				{
					strncpyz(szControlResult,"4", sizeof(szControlResult));
				}
				else  
				{
					strncpyz(szControlResult,"5", sizeof(szControlResult));
				}
			}
			else if(iReturn == WEB_RETURN_PROTECTED_ERROR)
			{
				strncpyz(szControlResult,"2", sizeof(szControlResult));
			}
			else if(iReturn == MODIFY_SIGNALVALUE_INVALID)
			{
				strncpyz(szControlResult,"0", sizeof(szControlResult));
			}
			else if(iReturn == TRUE)
			{
				strncpyz(szControlResult,"1", sizeof(szControlResult));
			}
			else
			{
				strncpyz(szControlResult,"0", sizeof(szControlResult));
			}
		}
		else
		{
		}
	}

	printf("\n%s", szControlResult);
	while((write(fd2, szControlResult, strlen(szControlResult) + 1))< 0)
	{
		printf("\nwrite:false");
		return FALSE;
	}
	printf("\nnwrite:end");
	//Sleep(10000);

	EQUIP_INFO		*pEquipInfo;
	//get the equip info
	iError = DxiGetData(VAR_A_EQUIP_INFO,
		iEquipID,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);
	if (iError == ERR_DXI_OK)
	{	
		//refresh the data after 10 seconds
		Web_PageRealize(pEquipInfo);
	}


	return TRUE;
}
static BOOL Web_ACUIsAuto()
{
	//Get Rectifier number
	SIG_BASIC_VALUE* pSigValue;
	int		iVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 7);
	int		iError = 0;


	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		1,	//ACU System Equip ID		
		iVarSubID,		
		&iBufLen,			
		(void *)&pSigValue,			
		iTimeOut);

	if (iError == ERR_DXI_OK)
	{
		////TRACE("pSigValue->varValue.ulValue[%ld]\n", pSigValue->varValue.ulValue);
		return (pSigValue->varValue.enumValue == 0) ? TRUE : FALSE;  //Auto return TRUE, other return FALSE;
	}
	else
	{
		////TRACE("pSigValue->varValue.ulValue[false]\n");
		return FALSE;
	}

}
#define GC_MAX_NUM_PER_FIELD			32

static BOOL IsValidDatetime(const char *ptrField)
{
	int	i = 0;
	int	iNumOfPoint = 0;


	if(!(*ptrField))
	{
		return FALSE;
	}

	char pField[32];
	memset(pField, 0x0,sizeof(pField));
	strncpyz(pField,ptrField,sizeof(pField));
	/*
	//2006-08-09 18:00:00
	if(*(pField + 4)== '-')
	{
	*(pField + 4) = NULL;
	if(IsValidWORD(pField))
	{
	//printf("\n----------------%s", pField);
	}
	else
	{
	return FALSE;
	}
	}
	else
	{
	return FALSE;
	}
	i  = i + 5;
	*/
	//printf("pField= {%s}\n", pField);
	i = 0;
	//Month
	if(*(pField + i +1)== '-')
	{
		*(pField + i +1) = NULL;

		if(IsValidWORD(pField + i) && atoi(pField + i) >= 0 && atoi(pField + i) <= 12)
		{
			//printf("\n----------------%s", pField + i);
		}
		else
		{
			//printf("Month");
			return FALSE;
		}
		i  = i + 2;
	}
	else if(*(pField + i + 2)== '-')
	{
		*(pField + i + 2) = NULL;

		if(IsValidWORD(pField + i) && atoi(pField + i) >= 0 && atoi(pField + i) <= 12)
		{
			//printf("\n----------------%s", pField + i);
		}
		else
		{
			//printf("Month");
			return FALSE;
		}
		i = i + 3;
	}
	else
	{
		//printf("Month");
		return FALSE;
	}

	//Day

	printf("Day pField + i = %s\n", pField + i);
	if(*(pField + i + 1)== ' ')
	{
		*(pField + i + 1) = NULL;

		if(IsValidWORD(pField + i) && atoi(pField + i) >= 1 && atoi(pField + i) <= 30)
		{
			//printf("\n----------------%s", pField + i);
		}
		else
		{
			//printf("Day");
			return FALSE;
		}
		i  = i + 2;
	}
	else if(*(pField  + i + 2)== ' ')
	{
		*(pField + i + 2) = NULL;

		if(IsValidWORD(pField + i) && atoi(pField + i) >= 1 && atoi(pField + i) <= 30)
		{
			//printf("\n----------------%s", pField + i);
		}
		else
		{
			//printf("Day");
			return FALSE;
		}
		i = i + 3;
	}
	else
	{
		//printf("Day");
		return FALSE;
	}

	//Hour
	printf("Hour pField + i = %s\n", pField + i);
	if((*(pField  + i + 1)== ' ') || (*(pField  + i + 1) == NULL))
	{
		*(pField + i + 1) = NULL;

		if(IsValidWORD(pField + i) && atoi(pField + i) >= 0 && atoi(pField + i) <= 23)
		{
			//printf("\n----------------%s", pField + i);
		}
		else
		{
			//printf("Day");
			return FALSE;
		}
		i  = i + 2;
	}
	else if((*(pField + i + 2)== ' ') || (*(pField + i + 2)== NULL))
	{
		*(pField + i + 2) = NULL;

		if(IsValidWORD(pField + i) && atoi(pField + i) >= 0 && atoi(pField + i) <= 23)
		{
			//printf("\n----------------%s", pField + i);
		}
		else
		{
			//printf("Day");
			return FALSE;
		}
		i = i + 3;
	}
	else
	{
		return FALSE;
	}
	/*	
	//Minutes
	if(*(pField + i+ 1)== ':')
	{
	*(pField + i + 1) = NULL;

	if(IsValidWORD(pField + i) && atoi(pField + i) >= 0 && atoi(pField + i) <= 59)
	{
	//printf("\n----------------%s", pField + i);
	}
	else
	{
	return FALSE;
	}
	i  = i + 2;
	}
	else if(*(pField  + i + 2)== ':')
	{
	*(pField + i + 2) = NULL;

	if(IsValidWORD(pField + i) && atoi(pField + i) >= 0 && atoi(pField + i) <= 59)
	{
	//printf("\n----------------%s", pField + i);
	}
	else
	{
	return FALSE;
	}
	i = i + 3;
	}
	else
	{
	return FALSE;
	}

	//Seconds
	if(*(pField + 1 + i)== ' ' || *(pField + 1 + i)== 0)
	{
	*(pField  + i+ 1) = NULL;

	if(IsValidWORD(pField + i) && atoi(pField + i) >= 0 && atoi(pField + i) <= 59)
	{
	//printf("\n----------------%s", pField + i);
	}
	else
	{
	return FALSE;
	}
	i  = i + 2;
	}
	else if(*(pField + 2 + i)== ' ' || *(pField + 2 + i)== 0)
	{
	*(pField  + i + 2) = NULL;

	if(IsValidWORD(pField + i) && atoi(pField + i) >= 0 && atoi(pField + i) <= 59)
	{
	//printf("\n----------------%s", pField + i);
	}
	else
	{
	return FALSE;
	}
	i = i + 3;
	}
	else
	{
	return FALSE;
	}
	*/
	return TRUE;
}



static BOOL IsValidFloat(const char *pField)
{
	int	i;
	int	iNumOfPoint = 0;

	if(!(*pField))
	{
		return FALSE;
	}

	if((*pField == '-') 
		|| (*pField == '+')
		|| (*pField == ' '))
	{
		i = 1;
	}
	else
	{
		i = 0;
	}

	while(*(pField + i))
	{
		if(*(pField + i) == '.')
		{
			iNumOfPoint++;
			if(iNumOfPoint > 1)
			{
				return FALSE;
			}
		}
		else if ((i > GC_MAX_NUM_PER_FIELD)
			|| (*(pField + i) < '0') 
			|| (*(pField + i) > '9'))
		{
			return FALSE;
		}
		i++;
	}

	return TRUE;
}

static BOOL IsValidLong(const char *pField)
{
	int	i;
	int	iNumOfPoint = 0;

	if(!(*pField))
	{
		return FALSE;
	}

	if((*pField == '-') 
		|| (*pField == '+')
		|| (*pField == ' '))
	{
		i = 1;
	}
	else
	{
		i = 0;
	}

	while(*(pField + i))
	{
		if ((i > GC_MAX_NUM_PER_FIELD)
			|| (*(pField + i) < '0') 
			|| (*(pField + i) > '9'))
		{
			return FALSE;
		}
		i++;
	}

	return TRUE;
}


//static BOOL IsValidWORD(const char *pField)
//{
//	int	i;
//
//	if(!(*pField))
//	{
//		return FALSE;
//	}
//
//	if((*pField) == '-')
//	{
//		return FALSE;
//	}
//	else if((*pField == '+')
//		|| (*pField == ' '))
//	{
//		i = 1;
//	}
//	else
//	{
//		i = 0;
//	}
//
//	while(*(pField + i))
//	{
//		if ((i > GC_MAX_NUM_PER_FIELD)
//			|| (*(pField + i) < '0') 
//			|| (*(pField + i) > '9'))
//		{
//			return FALSE;
//		}
//		i++;
//	}
//
//	return TRUE;
//}


#endif


#define MAKE_EQUIP_LIST							"equip_list"
#define MAKE_REC_LIST							"rectifier_list"
#define MAKE_EQUIP_NAME							"equip_name"
#define MAKE_SAMPLE_SIGNAL_INFO					"sample_signal_info"
#define MAKE_CONTROL_SIGNAL_INFO				"control_signal_info"
#define MAKE_SETING_SIGNAL_INFO					"seting_signal_info"
#define MAKE_ALARM_DISPLAY_INFO					"alarm_signal_display_info"
#define MAKE_ALARM_INFO							"alarm_info"
#define MAKE_FIRST_PAGE_INFO					"first_page_info"
#define MAKE_ALARM_FINAL_INFO					"alarm_final_info"
#define IS_SPACE(c)								((c)==(0x20))
#define IS_NUMBER(c)								((c)>=(0x30) && (c)<=(0x39))
#define PAGE_OK									1
#define iEquipBuffer_BUF_SIZE					100
#define iRecBuffer_BUF_SIZE						250
#define iAlarm_BUF_SIZE							250
#define ALARM_MAIN_BUF_SIZE						350
#define SAMPLE_DATA_BUF_SIZE					500
#define CONTROL_DATA_BUF						850
#define SETING_DATA_BUF							1000
#define ALARM_DATA_DISPLAY_BUF					1000
#define ALARM_DATA_BUF							1000
#define REFRESH_PERIOD							30000
#define DELAY_PERIOD							100

/*==========================================================================*
* FUNCTION :  RemoveSpace
* PURPOSE  :  remove space in string
* CALLS    : 
* CALLED BY:  Web_PageRefresh 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wan Kun              DATE: 2006-08-29 11:08
*==========================================================================*/
char *RemoveSpace(char *buf)
{
	char *p=buf;
	char pfinal[30]="";
	int i=0;
	while(*p)
	{
		if(IS_SPACE(*p)) p=p++;
		pfinal[i]=*p;
		p++;
		i++;
	}
	p=strdup(pfinal);
	return p;
}
/*==========================================================================*
* FUNCTION :  RemoveNumber
* PURPOSE  :  remove space in string
* CALLS    : 
* CALLED BY:  Web_PageRefresh 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wan Kun              DATE: 2006-08-29 11:08
*==========================================================================*/
char *RemoveNumber(char *buf)
{
	char *p=buf;
	char pfinal[30]="";
	int i=0;
	while(*p)
	{
		if(IS_NUMBER(*p)) p=p++;
		pfinal[i]=*p;
		p++;
		i++;
	}
	p=strdup(pfinal);
	return p;
}

//int   iAlarmIndex = 0;
/*==========================================================================*
* FUNCTION :  Web_PageAlarmRealize
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_PageRefresh
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wan Kun              DATE: 2006-09-08 11:08
*==========================================================================*/
static int Web_PageAlarmRealize(EQUIP_INFO *pEquipInfo,IN OUT char **ppBuf)
{
	int					iAlarmNumber = 0;
	int					iNum = 0;
	int					iLen = 0;
	int					iAlarmIndex = 0;
	int					tag_alarm1=0;
	int					tag_alarm2=0;
	int					tag_alarm3=0;
	int					iReturn = 0;
	int					iLanguage = 0;
	char				*pMBuf = NULL;
	char				szTime[32]="";
	char				AlarmSeverity[20]="";
	char				AlarmFinal[20]="";
	char				*szfile=NULL;
	char				*pHtml=NULL;
	char				*pAlarmBuf = NULL;
	time_t				tmSample;
	ALARM_SIG_VALUE		*pAlarmSigValue = NULL, *pDelete = NULL;
	FILE				*fp = NULL;

	TRACE("-----in to alarm procedure -------\n");
	iAlarmNumber = Web_GetNetscapeRealTimeAlarmData(&pAlarmSigValue);
	TRACE("-----in zero cycle --alarm number is %d-----\n", iAlarmNumber);

	//get the current language
	iLanguage = Web_GetCurrentLangValue();

	if(iAlarmNumber > 0)
	{
		pMBuf = NEW(char,iAlarm_BUF_SIZE * iAlarmNumber);

		//TRACE("-----1 --alarm number is %d-----\n", iAlarmNumber);
		//iAlarmNumber = pEquipInfo->pStdEquip->iAlarmSigNum;
		//if(iAlarmNumber > 0)
		//{
		//	pMBuf = NEW(char,MAX_COMM_RETURN_DATA_LINE_SIZE * iAlarmNumber);
		//}
		//else
		//	return 0;
		//TRACE(" ______into alarm part1________\n");
		//Alarm sort
		DXI_MakeAlarmSigValueSort(pAlarmSigValue, iAlarmNumber);
		//TRACE("-----2 --alarm number is %d-----\n", iAlarmNumber);
		if(pMBuf == NULL )
		{
			TRACE(" ______no space for pMBuf in alarm part________\n");
			return 0;
		}
		memset(pMBuf, 0x0, (size_t)iAlarm_BUF_SIZE * iAlarmNumber);

		pDelete = pAlarmSigValue;
		for(iNum =0; iNum < iAlarmNumber && pAlarmSigValue != NULL; iNum++, pAlarmSigValue++)
		{

			if(pAlarmSigValue->pStdSig->iAlarmLevel != ALARM_LEVEL_NONE)
			{

				iAlarmIndex++;
				//TRACE(" ______into alarm part2 and iAlarmIndex is %d________\n",iAlarmIndex);
				//set time of alarm
				tmSample = pAlarmSigValue->sv.tmStartTime;
				if(tmSample!=0) TimeToString(tmSample, TIME_CHN_FMT,szTime, sizeof(szTime));
				else strcpy(szTime,"--");
				//TRACE(" ______into alarm part3 and szTime is %s________\n",szTime);
				//set severity of alarm
				if(pAlarmSigValue->pStdSig->iAlarmLevel==1)
				{
					strcpy(AlarmSeverity,"OA");
					tag_alarm3=1;
				}
				else if(pAlarmSigValue->pStdSig->iAlarmLevel==2) 
				{
					strcpy(AlarmSeverity,"MA");
					tag_alarm2 = 1;
				}
				else if(pAlarmSigValue->pStdSig->iAlarmLevel==3) 
				{
					strcpy(AlarmSeverity,"CA");
					tag_alarm1 = 1;
				}
				//TRACE(" ______into alarm part4 and AlarmSeverity is %s________\n",AlarmSeverity);
				iLen +=sprintf(pMBuf + iLen, "<TR ALIGN=CENTER><TD BGCOLOR=#CCCCCC>%d</TD><TD BGCOLOR=#FF0000>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD></TR>\n",
					iAlarmIndex,
					AlarmSeverity,
					pAlarmSigValue->pStdSig->pSigName->pFullName[iLanguage],
					szTime,
					pAlarmSigValue->pEquipInfo->pEquipName->pFullName[iLanguage]);
				//TRACE(" ______into alarm part5 and each line len is %d________\n",iLen);
			}

		}

		// refresh mainhigh.htm
		szfile=NEW(char,50);
		strcpy(szfile,"/app/www/netscape/pages/mainhigh.htm");
		iReturn = LoadHtmlFile(szfile, &pHtml);
		if(iReturn > 0)
		{
			DELETE(szfile);
			szfile = NULL;

			if(tag_alarm1 == 1) strcpy(AlarmFinal,"CA");
			else if(tag_alarm2 == 1) strcpy(AlarmFinal,"MA");
			else if(tag_alarm3 == 1) strcpy(AlarmFinal,"OA");
			//TRACE("-----3 --alarm number is %d-----\n", iAlarmNumber);
			pAlarmBuf = NEW( char, ALARM_MAIN_BUF_SIZE);
			memset(pAlarmBuf, 0x0, (size_t)ALARM_MAIN_BUF_SIZE);
			sprintf(pAlarmBuf, "<TD WIDTH=\"162\"><B><script language=\"javascript\">document.write(errMsg[0]);</script></B></TD><TD BGCOLOR=#FF0000 WIDTH=85 ALIGN=CENTER><B>%s</B></TD><TD>&nbsp;</TD><TD WIDTH=200><B><script language=\"javascript\">document.write(errMsg[1]);</script></B></TD><TD ALIGN=CENTER BGCOLOR=#CCCCCC WIDTH=50><B>&nbsp;%d &nbsp;</B></TD>",
				AlarmFinal,
				iAlarmNumber);
			ReplaceString(&pHtml, MAKE_VAR_FIELD(MAKE_ALARM_FINAL_INFO),pAlarmBuf);
			szfile=NEW(char,50);
			strcpy(szfile,"/var/netscape/mainhigh.htm");
			if(szfile != NULL && (fp = fopen(szfile,"wb")) != NULL && pHtml != NULL)
			{

				fwrite(pHtml,strlen(pHtml), 1, fp);
				fclose(fp);

			}
			if(szfile != NULL)
			{
				DELETE(szfile);
				szfile = NULL;
			}
			DELETE(pAlarmBuf);
			pAlarmBuf = NULL;
			DELETE(pHtml);
			pHtml = NULL;
		}
		else
		{
			DELETE(szfile);
			szfile = NULL;
			DELETE(pHtml);
			pHtml = NULL;
		}

		if(pDelete != NULL)
		{
			DELETE(pDelete);
			pDelete = NULL;
		}
		//DELETE(pAlarmSigValue);
		//pAlarmSigValue = NULL;

		*ppBuf = pMBuf;
		//printf("\npMBuf:%s\n",pMBuf);
		//TRACE(" ______into alarm part6 and len is %d________\n",iLen);
		return iLen;
	}
	else if ( iAlarmNumber == 0 )
	{
		TRACE("-----in zero cycle --alarm number is %d-----\n", iAlarmNumber);
		//no alarm , so only return 0 for one memo size
		pMBuf = NEW(char,iAlarm_BUF_SIZE);
		if(pMBuf == NULL )
		{
			TRACE(" ______no space for pMBuf in alarm part________\n");
			return 0;
		}
		memset(pMBuf, 0x0, (size_t)iAlarm_BUF_SIZE );
		iLen = sprintf(pMBuf,"<TR><TD>--No Alarm--</TD></TR>");
		*ppBuf = pMBuf;

		// refresh mainhigh.htm
		szfile=NEW(char,50);
		strcpy(szfile,"/app/www/netscape/pages/mainhigh.htm");
		iReturn = LoadHtmlFile(szfile, &pHtml);
		if(iReturn > 0)
		{
			DELETE(szfile);
			szfile = NULL;

			strcpy(AlarmFinal,"NONE");
			TRACE("-----3 --alarm number is %d-----\n", iAlarmNumber);
			pAlarmBuf = NEW( char, ALARM_MAIN_BUF_SIZE);
			memset(pAlarmBuf, 0x0, (size_t)ALARM_MAIN_BUF_SIZE);
			sprintf(pAlarmBuf, "<TD WIDTH=\"162\"><B><script language=\"javascript\">document.write(errMsg[0]);</script></B></TD><TD BGCOLOR=#CCCCCC WIDTH=85 ALIGN=CENTER><B>%s</B></TD><TD>&nbsp;</TD><TD WIDTH=200><B><script language=\"javascript\">document.write(errMsg[1]);</script></B></TD><TD ALIGN=CENTER BGCOLOR=#CCCCCC WIDTH=50><B>&nbsp;%d &nbsp;</B></TD>",
				AlarmFinal,
				iAlarmNumber);
			ReplaceString(&pHtml, MAKE_VAR_FIELD(MAKE_ALARM_FINAL_INFO),pAlarmBuf);
			szfile=NEW(char,50);
			strcpy(szfile,"/var/netscape/mainhigh.htm");
			if(szfile != NULL && (fp = fopen(szfile,"wb")) != NULL && pHtml != NULL)
			{

				fwrite(pHtml,strlen(pHtml), 1, fp);
				fclose(fp);

			}
			if(szfile != NULL)
			{
				DELETE(szfile);
				szfile = NULL;
			}
			DELETE(pAlarmBuf);
			pAlarmBuf = NULL;
			DELETE(pHtml);
			pHtml = NULL;
		}
		else
		{
			DELETE(szfile);
			szfile = NULL;
			DELETE(pHtml);
			pHtml = NULL;
		}
		return 1;
	}
	else if (iAlarmNumber < 0 )
	{
		//abnomal status
		return FALSE;
	}

}
/*==========================================================================*
* FUNCTION :  Web_PageRealize
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_PageRefresh
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wan Kun              DATE: 2006-08-30 11:08
*==========================================================================*/
//int					tmp=0;
static int Web_PageRealize(EQUIP_INFO		*pEquipInfo)
{
	//TRACE("-----------sigal number of each model is %d name is %s----------------\n",pEquipInfo->pStdEquip->iSampleSigNum,pEquipInfo->pEquipName->pFullName[0]);
	int					iSampleDataNum = 0;
	int					iSignalNumber = 0;
	int					iSetingNumber = 0;
	int					iAlarmNumber = 0;
	int					i,iIndex = 0;
	int					iLen = 0;
	int					iReturn = 0;
	int					iEnum = 0;
	int					j = 0;
	int					iValueFormat = 0;
	int					iLanguage = 0;
	char				*pMBuf = NULL;
	char				szTime[32]="";
	char				szUnit[32]="";
	char				*pHtml = NULL;
	char				*szfile = NULL;
	char				*pNoSignal="<TR><TD><FONT SIZE=\"-1\"><script language=\"javascript\">showMsg(1)</script></FONT></TD></TR>";
	FILE				*fp = NULL;
	SAMPLE_SIG_VALUE	*pSampleSigValue = pEquipInfo->pSampleSigValue;
	CTRL_SIG_VALUE		*pControlSigValue = pEquipInfo->pCtrlSigValue;
	SET_SIG_VALUE		*pParamSigValue = pEquipInfo->pSetSigValue;
	ALARM_SIG_VALUE		*pAlarmSigValue = pEquipInfo->pAlarmSigValue;
	time_t				tmSample;
	char				szTimeValue[32];
	char				*pTimeValue = NULL;

	//if((tmp==10)&&((pEquipInfo->iEquipTypeID<200)||(pEquipInfo->iEquipTypeID>300)))
	//if((pEquipInfo->iEquipTypeID<200)||(pEquipInfo->iEquipTypeID>300))
	{	//only if ACUSystem.htm and no recitifier can enter this condition
		/*
		szfile=NEW(char,50);
		sprintf(szfile,"/acu/www/html/netscape/%s.htm",RemoveSpace(pEquipInfo->pEquipName->pFullName[0]));
		TRACE("-----------szfile is %s---------------\n",szfile);
		//*/
		szfile="/app/www/netscape/pages/system.htm";
		iReturn = LoadHtmlFile(szfile, &pHtml);
		szfile = NULL;

		//get the current language
		iLanguage = Web_GetCurrentLangValue();

		if(iReturn <= 0)
		{
			TRACE("---------can not load %s file--------------\n",szfile);
			return 0;
		}
		//TRACE("\nSampling Data!!!\n");
		//1.prepare sample data to display 
		iSampleDataNum = pEquipInfo->pStdEquip->iSampleSigNum;
		//rectifier sn manage
		char	szDisBuf[64];
		int		iEquipTypeID = pEquipInfo->iEquipTypeID;
		int		iEquipID = pEquipInfo->iEquipID;


		if(iSampleDataNum <= 0  )
		{
			//it will display no sample signal html page, add it already;
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_EQUIP_NAME),
				pEquipInfo->pEquipName->pFullName[iLanguage]);
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_SAMPLE_SIGNAL_INFO),
				pNoSignal);		
		}
		else
		{
			//display signal value
			pMBuf = NEW(char,SAMPLE_DATA_BUF_SIZE * iSampleDataNum);
			if(pMBuf == NULL )
			{
				TRACE(" ______no space for pMBuf in sample part________\n");
				return 0;
			}
			memset(pMBuf, 0x0, (size_t)SAMPLE_DATA_BUF_SIZE * iSampleDataNum);

			//TRACE("pmbuf for sample data is %s\n",pMBuf);
			for(i= 0; i < iSampleDataNum && pSampleSigValue != NULL ; i++,pSampleSigValue++)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
				//make judgement : whether display and whether configured
				if(STD_SIG_IS_DISPLAY_ON_UI(pSampleSigValue->pStdSig,DISPLAY_WEB)&&SIG_VALUE_IS_CONFIGURED(&pSampleSigValue->bv)&& (SIG_VALUE_NEED_DISPLAY(&pSampleSigValue->bv))) 
				{
					//TRACE("-----the sample signal's %d name is %s-----\n",iIndex,pSampleSigValue->pStdSig->pSigName->pFullName[0]);
					iIndex++;
					tmSample = pSampleSigValue->bv.tmCurrentSampled;
					if(tmSample!=0) TimeToString(tmSample, TIME_CHN_FMT,szTime, sizeof(szTime));
					else strcpy(szTime,"--");//if read value is zero, then display --

					//if unit is "",then display "--"
					//TRACE("------unit is %s--------\n",pSampleSigValue->pStdSig->szSigUnit);
					if (strlen(pSampleSigValue->pStdSig->szSigUnit) == 0) strcpy(szUnit,"--");
					else strcpy(szUnit,pSampleSigValue->pStdSig->szSigUnit);
					//TRACE("------after szunit is %s+++++++\n",szUnit);

					if( pSampleSigValue->pStdSig->iSigValueType == VAR_FLOAT)
					{
						//modified by wankun 12/09/2006 : add float width
						iValueFormat = Web_TransferFormatToInt( pSampleSigValue->pStdSig->szValueDisplayFmt );
						//iValueFormat = 2;
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=%s>%.*f</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD></TR>\n",
							iIndex,
							pSampleSigValue->pStdSig->pSigName->pFullName[iLanguage],
							SIG_VALUE_IS_VALID(&pSampleSigValue->bv) ? "#C0C0C0" : "#FFFFCC",
							iValueFormat,
							pSampleSigValue->bv.varValue.fValue,
							szUnit,
							szTime);
						//modified by wankun 12/09/2006 : add float width
					}
					else if( pSampleSigValue->pStdSig->iSigValueType == VAR_LONG)
					{
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=%s>%8ld</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD></TR>\n",
							iIndex,
							pSampleSigValue->pStdSig->pSigName->pFullName[iLanguage],
							SIG_VALUE_IS_VALID(&pSampleSigValue->bv) ? "#C0C0C0" : "#FFFFCC",
							pSampleSigValue->bv.varValue.lValue,
							szUnit,
							szTime);
					}
					else if( pSampleSigValue->pStdSig->iSigValueType == VAR_UNSIGNED_LONG )
					{
						if(iEquipTypeID > 200 && iEquipTypeID < 300  && pSampleSigValue->pStdSig->iSigID == 8)
						{
							if(!Web_GetRectHighValid(iEquipID))//Old rectifier
							{
								snprintf(szDisBuf, sizeof(szDisBuf), "2%02d%01X%06d", 
									(((BYTE*)&(pSampleSigValue->bv.varValue))[0] & 0x1F),
									(((BYTE*)&(pSampleSigValue->bv.varValue))[1] & 0x0F),
									LOWORD((DWORD)(pSampleSigValue->bv.varValue.ulValue)));
							}
							else//new rectifier
							{
								snprintf(szDisBuf, sizeof(szDisBuf), "%02u%02d%02d%05d", 
									Web_GetRectHighSNValue(iEquipID),
									(((BYTE*)&(pSampleSigValue->bv.varValue))[0] & 0x1F),
									(((BYTE*)&(pSampleSigValue->bv.varValue))[1] & 0x0F),
									LOWORD((DWORD)(pSampleSigValue->bv.varValue.ulValue)));
							}
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=%s>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD></TR>\n",
								iIndex,
								pSampleSigValue->pStdSig->pSigName->pFullName[iLanguage],
								SIG_VALUE_IS_VALID(&pSampleSigValue->bv) ? "#C0C0C0" : "#FFFFCC",
								szDisBuf,
								szUnit,
								szTime);
						}
						else
						{
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=%s>%8lu</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD></TR>\n",
								iIndex,
								pSampleSigValue->pStdSig->pSigName->pFullName[iLanguage],
								SIG_VALUE_IS_VALID(&pSampleSigValue->bv) ? "#C0C0C0" : "#FFFFCC",
								pSampleSigValue->bv.varValue.ulValue,
								szUnit,
								szTime);
						}
					}
					else if( pSampleSigValue->pStdSig->iSigValueType == VAR_ENUM )
					{
						iEnum = pSampleSigValue->bv.varValue.enumValue;
						//TRACE("---------iEnum is %d value is %s ----------\n",iEnum,pSampleSigValue->pStdSig->pStateText[iEnum]->pFullName[0]);
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=%s>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD></TR>\n",
							iIndex,
							pSampleSigValue->pStdSig->pSigName->pFullName[iLanguage],
							SIG_VALUE_IS_VALID(&pSampleSigValue->bv) ? "#C0C0C0" : "#FFFFCC",
							pSampleSigValue->pStdSig->pStateText[iEnum]->pFullName[iLanguage],
							szUnit,
							szTime);
					}
					else if( pSampleSigValue->pStdSig->iSigValueType == VAR_DATE_TIME )
					{
						memset(szTimeValue, 0x0, sizeof(szTimeValue));
						if(pSampleSigValue->bv.varValue.dtValue > 0)
						{
							TimeToString(pSampleSigValue->bv.varValue.dtValue, TIME_CHN_FMT,szTimeValue, sizeof(szTimeValue));
						}
						else
						{
							strncpyz(szTimeValue, "--", sizeof(szTimeValue));
						}

						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=%s>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD></TR>\n",
							iIndex,
							pSampleSigValue->pStdSig->pSigName->pFullName[iLanguage],
							SIG_VALUE_IS_VALID(&pSampleSigValue->bv) ? "#C0C0C0" : "#FFFFCC",
							szTimeValue,
							szUnit,
							szTime);
					}
				}//end ------if(STD_SIG_IS_DISPLAY_ON_UI..............
			}//end ------for(i= 0; i < iSampleDataNum.................
			//TRACE("pmbuf for sample data is %s\n",pMBuf);

			//if there is no display, then display "there are no signal at all!"
			if( strlen(pMBuf)==0) strcpy(pMBuf,pNoSignal);
			//replace the var in html file
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_EQUIP_NAME),
				pEquipInfo->pEquipName->pFullName[iLanguage]);
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_SAMPLE_SIGNAL_INFO),
				pMBuf);
			DELETE(pMBuf);
			*pMBuf = NULL;
		}//end ------if(iSampleDataNum <= 0  )
		//*/
		//TRACE("\n Control Data!!! \n");

		//2.prepare control data to display 
		iSignalNumber = pEquipInfo->pStdEquip->iCtrlSigNum;
		if(iSignalNumber <= 0  )
		{
			//it will display no control signal html page, add it lately;
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_CONTROL_SIGNAL_INFO),
				pNoSignal);
		}
		else
		{
			//display signal value
			pMBuf = NEW(char,CONTROL_DATA_BUF * iSignalNumber);
			if(pMBuf == NULL )
			{
				TRACE(" ______no space for pMBuf in control part________\n");
				return 0;
			}
			memset(pMBuf, 0x0, (size_t)CONTROL_DATA_BUF * iSignalNumber);
			//TRACE("pmbuf for control data is %s\n",pMBuf);
			iIndex = 0;
			iEnum = 0;
			iLen = 0;
			iValueFormat = 0;
			for(i= 0; i < iSignalNumber && pControlSigValue != NULL ; i++, pControlSigValue++)
			{
				//TRACE("-----the control signal's %d name is %s-----\n",iIndex,pControlSigValue->pStdSig->pSigName->pFullName[0]);

				//make judgement : whether display and whether configured
				if(STD_SIG_IS_DISPLAY_ON_UI(pControlSigValue->pStdSig,DISPLAY_WEB)&&SIG_VALUE_IS_CONFIGURED(&pControlSigValue->bv)&& (SIG_VALUE_NEED_DISPLAY(&pControlSigValue->bv))) 
				{
					iIndex++;
					tmSample = pControlSigValue->bv.tmCurrentSampled;
					if(tmSample!=0) TimeToString(tmSample, TIME_CHN_FMT,szTime, sizeof(szTime));
					else sprintf(szTime,"--");//if read value is zero, then display --
					//TRACE("-----i am here 14993   %s-----\n",szTime);
					//if unit is "",then display "--"
					if (strlen(pControlSigValue->pStdSig->szSigUnit) == 0) strcpy(szUnit,"--");
					else strcpy(szUnit,pControlSigValue->pStdSig->szSigUnit);

					if( pControlSigValue->pStdSig->iSigValueType == VAR_FLOAT)
					{
						//modified by wankun 12/09/2006 : add float width
						iValueFormat = Web_TransferFormatToInt( pControlSigValue->pStdSig->szValueDisplayFmt );
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%.*f</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"10\" name = \"InputValue1%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID1%d\" value=\"%d\"><input type=\"hidden\" name=\"Type\" value=\"1\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply%d\"><!--#endif --></TD></TR>\n",
							iIndex,
							pControlSigValue->pStdSig->pSigName->pFullName[iLanguage],
							iValueFormat,
							pControlSigValue->bv.varValue.fValue,
							szUnit,
							szTime,
							iIndex,
							iIndex,
							pControlSigValue->pStdSig->iSigID,
							pEquipInfo->iEquipID,
							iIndex);
						//modified by wankun 12/09/2006 : add float width
					}


					else if( pControlSigValue->pStdSig->iSigValueType == VAR_LONG )
					{
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%8ld</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue1%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID1%d\" value=\"%d\"><input type=\"hidden\" name=\"Type\" value=\"1\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply%d\"><!--#endif --></TD></TR>\n",
							iIndex,
							pControlSigValue->pStdSig->pSigName->pFullName[iLanguage],
							pControlSigValue->bv.varValue.lValue,
							szUnit,
							szTime,
							iIndex,
							iIndex,
							pControlSigValue->pStdSig->iSigID,
							pEquipInfo->iEquipID,
							iIndex);
					}
					else if( pControlSigValue->pStdSig->iSigValueType == VAR_UNSIGNED_LONG )
					{
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%8lu</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue1%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID1%d\" value=\"%d\"><input type=\"hidden\" name=\"Type\" value=\"1\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply%d\"><!--#endif --></TD></TR>\n",
							iIndex,
							pControlSigValue->pStdSig->pSigName->pFullName[iLanguage],
							pControlSigValue->bv.varValue.ulValue,
							szUnit,
							szTime,
							iIndex,
							iIndex,
							pControlSigValue->pStdSig->iSigID,
							pEquipInfo->iEquipID,
							iIndex);
					}
					else if( pControlSigValue->pStdSig->iSigValueType == VAR_ENUM )
					{
						iEnum = pControlSigValue->bv.varValue.enumValue;
						//TRACE("---------iEnum is %d ------\n",iEnum);
						if(pControlSigValue->pStdSig->iStateNum==1)
						{
							//in order to deal with one option's condition!
							//TRACE("---------iEnum is %d ------\n",iEnum);
							//TRACE("---------iEnum is %d value is %s ----------\n",iEnum,pControlSigValue->pStdSig->pStateText[0]->pFullName[0]);
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER><SELECT name=\"InputValue1%d\" style='width:100%'><option selected>%s</option></select></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID1%d\" value=\"%d\"><input type=\"hidden\" name=\"Type\" value=\"1\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pControlSigValue->pStdSig->pSigName->pFullName[iLanguage],
								pControlSigValue->pStdSig->pStateText[iEnum-1]->pFullName[iLanguage],
								szUnit,
								szTime,
								iIndex,
								pControlSigValue->pStdSig->pStateText[iEnum-1]->pFullName[iLanguage],
								iIndex,
								pControlSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);
						}
						else
						{
							//modified by wankun 12/09/2006, delete default item NONE
							//TRACE("---------iEnum other is %d ------\n",iEnum);
							//when iEnum>1
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER><SELECT name=\"InputValue1%d\" style='width:100%'><option selected>%s</option>",
								iIndex,
								pControlSigValue->pStdSig->pSigName->pFullName[iLanguage],
								pControlSigValue->pStdSig->pStateText[iEnum]->pFullName[iLanguage],
								szUnit,
								szTime,
								iIndex,
								pControlSigValue->pStdSig->pStateText[0]->pFullName[iLanguage]	);

							//input will add serveral options to select
							//TRACE("---------iEnum number is %d ------\n",pControlSigValue->pStdSig->iStateNum);
							for(j=1;j<pControlSigValue->pStdSig->iStateNum;j++)
							{
								iLen  += sprintf(pMBuf + iLen,"<option>%s</option>",pControlSigValue->pStdSig->pStateText[j]->pFullName[iLanguage]);
							}
							//modified by wankun 12/09/2006, delete default item NONE

							iLen  += sprintf(pMBuf + iLen,"</select></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID1%d\" value=\"%d\"><input type=\"hidden\" name=\"Type\" value=\"1\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pControlSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);

						}

					}
					else if( pControlSigValue->pStdSig->iSigValueType == VAR_DATE_TIME )
					{
						memset(szTimeValue, 0x0, sizeof(szTimeValue));
						if(pControlSigValue->bv.varValue.dtValue > 0)
						{
							TimeToString(pControlSigValue->bv.varValue.dtValue, TIME_CHN_FMT,szTimeValue, sizeof(szTimeValue));
						}
						else
						{
							strncpyz(szTimeValue, "--", sizeof(szTimeValue));
						}
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue1%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID1%d\" value=\"%d\"><input type=\"hidden\" name=\"Type\" value=\"1\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply%d\"><!--#endif --></TD></TR>\n",
							iIndex,
							pControlSigValue->pStdSig->pSigName->pFullName[iLanguage],
							szTimeValue,
							szUnit,
							szTime,
							iIndex,
							iIndex,
							pControlSigValue->pStdSig->iSigID,
							pEquipInfo->iEquipID,
							iIndex);
					}//*/

				}//end------if(STD_SIG_IS_DISPLAY_ON_UI.............................

			}//end------for(i= 0; i < iSignalNumber.........................

			//if there is no display, then display "there are no signal at all!"
			if( strlen(pMBuf)==0) strcpy(pMBuf,pNoSignal);
			//TRACE("pmbuf for control data is %s\n",pMBuf);
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_CONTROL_SIGNAL_INFO),
				pMBuf);
			DELETE(pMBuf);
			pMBuf = NULL;
		}//end------if(iSignalNumber <= 0  )
		//*/

		//TRACE("\n Setting Data!!! \n");
		//3.prepare setting data to display 
		iSetingNumber = pEquipInfo->pStdEquip->iSetSigNum;
		if(iSetingNumber <= 0  )
		{
			//it will display no control signal html page, add it lately;
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_SETING_SIGNAL_INFO),
				pNoSignal);
		}
		else
		{
			//display signal value
			pMBuf = NEW(char,SETING_DATA_BUF * iSetingNumber);
			if(pMBuf == NULL )
			{
				TRACE(" ______no space for pMBuf in seting part________\n");
				return 0;
			}
			memset(pMBuf, 0x0, (size_t)SETING_DATA_BUF * iSetingNumber);
			//TRACE("pmbuf for seting data is %s\n",pMBuf);
			iIndex = 0;
			iEnum = 0;
			iLen = 0;
			iValueFormat = 0;
			for(i= 0; i < iSetingNumber && pParamSigValue != NULL ; i++, pParamSigValue++)
			{
				//TRACE("-----the seting signal's %d name is %s-----\n",iIndex,pParamSigValue->pStdSig->pSigName->pFullName[0]);

				//make judgement : whether display and whether configured
				if(STD_SIG_IS_DISPLAY_ON_UI(pParamSigValue->pStdSig,DISPLAY_WEB)&&SIG_VALUE_IS_CONFIGURED(&pParamSigValue->bv)&& (SIG_VALUE_NEED_DISPLAY(&pParamSigValue->bv))) 
				{
					iIndex++;
					tmSample = pParamSigValue->bv.tmCurrentSampled;
					if(tmSample!=0) TimeToString(tmSample, TIME_CHN_FMT,szTime, sizeof(szTime));
					else sprintf(szTime,"--");//if read value is zero, then display --
					//TRACE("-----i am here 14993   %s-----\n",szTime);
					//if unit is "",then display "--"
					if (strlen(pParamSigValue->pStdSig->szSigUnit) == 0) strcpy(szUnit,"--");
					else strcpy(szUnit,pParamSigValue->pStdSig->szSigUnit);

					if( pParamSigValue->pStdSig->iSigValueType == VAR_FLOAT)
					{
						//modified by wankun 12/09/2006 : add float width
						iValueFormat = Web_TransferFormatToInt( pParamSigValue->pStdSig->szValueDisplayFmt );
						if(iIndex < 10)
						{
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%.*f</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue2%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply90%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
								iValueFormat,
								pParamSigValue->bv.varValue.fValue,
								szUnit,
								szTime,
								iIndex,
								iIndex,
								pParamSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);
						}
						else
						{

						
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%.*f</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue2%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply9%d\"><!--#endif --></TD></TR>\n",
							iIndex,
							pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
							iValueFormat,
							pParamSigValue->bv.varValue.fValue,
							szUnit,
							szTime,
							iIndex,
							iIndex,
							pParamSigValue->pStdSig->iSigID,
							pEquipInfo->iEquipID,
							iIndex);
						}
						//modified by wankun 12/09/2006 : add float width
					}


					else if( pParamSigValue->pStdSig->iSigValueType == VAR_LONG )
					{
						if(iIndex < 10)
						{
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%8ld</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue2%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply90%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
								pParamSigValue->bv.varValue.lValue,
								szUnit,
								szTime,
								iIndex,
								iIndex,
								pParamSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);
						}
						else
						{
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%8ld</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue2%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply9%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
								pParamSigValue->bv.varValue.lValue,
								szUnit,
								szTime,
								iIndex,
								iIndex,
								pParamSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);
						}
					}
					else if( pParamSigValue->pStdSig->iSigValueType == VAR_UNSIGNED_LONG )
					{
						if(iIndex < 10)
						{
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%8lu</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue2%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply90%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
								pParamSigValue->bv.varValue.ulValue,
								szUnit,
								szTime,
								iIndex,
								iIndex,
								pParamSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);
						}
						else
						{
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%8lu</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue2%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply9%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
								pParamSigValue->bv.varValue.ulValue,
								szUnit,
								szTime,
								iIndex,
								iIndex,
								pParamSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);
						}
					}
					else if( pParamSigValue->pStdSig->iSigValueType == VAR_ENUM )
					{
						iEnum = pParamSigValue->bv.varValue.enumValue;
						if(pParamSigValue->pStdSig->iStateNum==1)
						{
							//in order to deal with one option's condition!
							//TRACE("---------iEnum is %d value is %s ----------\n",iEnum,pParamSigValue->pStdSig->pStateText[iEnum-1]->pFullName[0]);
							if(iIndex < 10)
							{
								iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER><SELECT name=\"InputValue2%d\" style='width:100%'><option selected>%s</option></select></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply90%d\"><!--#endif --></TD></TR>\n",
									iIndex,
									pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
									pParamSigValue->pStdSig->pStateText[iEnum-1]->pFullName[iLanguage],
									szUnit,
									szTime,
									iIndex,
									pParamSigValue->pStdSig->pStateText[iEnum-1]->pFullName[iLanguage],
									iIndex,
									pParamSigValue->pStdSig->iSigID,
									pEquipInfo->iEquipID,
									iIndex);
							}
							else
							{

								iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER><SELECT name=\"InputValue2%d\" style='width:100%'><option selected>%s</option></select></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply9%d\"><!--#endif --></TD></TR>\n",
									iIndex,
									pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
									pParamSigValue->pStdSig->pStateText[iEnum-1]->pFullName[iLanguage],
									szUnit,
									szTime,
									iIndex,
									pParamSigValue->pStdSig->pStateText[iEnum-1]->pFullName[iLanguage],
									iIndex,
									pParamSigValue->pStdSig->iSigID,
									pEquipInfo->iEquipID,
									iIndex);
							}
						}
						else
						{
							//modified by wankun 12/09/2006, delete default item NONE
							//when iEnum>1
		
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER><SELECT name=\"InputValue2%d\" style='width:100%'><option selected>%s</option>",
								iIndex,
								pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
								pParamSigValue->pStdSig->pStateText[iEnum]->pFullName[iLanguage],
								szUnit,
								szTime,
								iIndex,
								pParamSigValue->pStdSig->pStateText[0]->pFullName[iLanguage]);
							//input will add serveral options to select
							for(j=1;j<pParamSigValue->pStdSig->iStateNum;j++)
							{
								iLen  += sprintf(pMBuf + iLen,"<option>%s</option>",pParamSigValue->pStdSig->pStateText[j]->pFullName[iLanguage]);
							}
							//modified by wankun 12/09/2006, delete default item NONE
							if(iIndex < 10)
							{
								iLen  += sprintf(pMBuf + iLen,"</select></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply90%d\"><!--#endif --></TD></TR>\n",
									iIndex,
									pParamSigValue->pStdSig->iSigID,
									pEquipInfo->iEquipID,
									iIndex);
							
							}
							else
							{
							iLen  += sprintf(pMBuf + iLen,"</select></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply9%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pParamSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);
							}
						}

					}
					else if( pParamSigValue->pStdSig->iSigValueType == VAR_DATE_TIME )
					{
						memset(szTimeValue, 0x0, sizeof(szTimeValue));
						if(pParamSigValue->bv.varValue.dtValue > 0)
						{
							TimeToString(pParamSigValue->bv.varValue.dtValue, TIME_CHN_FMT,szTimeValue, sizeof(szTimeValue));
							/*if(pEquipInfo->iEquipTypeID == 300 || pEquipInfo->iEquipTypeID == 700)
							{
								pTimeValue = szTimeValue;
								pTimeValue = pTimeValue + 5;
								pTimeValue[8] = NULL; 
							}*/

						}
						else
						{
							strncpyz(szTimeValue, "--", sizeof(szTimeValue));
							/*if(pEquipInfo->iEquipTypeID == 300 || pEquipInfo->iEquipTypeID == 700)
							{
								pTimeValue = "--";
							}*/
						}
						if(iIndex < 10)
						{
							iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue2%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply90%d\"><!--#endif --></TD></TR>\n",
								iIndex,
								pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
								//(pEquipInfo->iEquipTypeID != 300 && pEquipInfo->iEquipTypeID != 700) ? szTimeValue : pTimeValue ,//szTimeValue,
								szTimeValue,
								szUnit,
								szTime,
								iIndex,
								iIndex,
								pParamSigValue->pStdSig->iSigID,
								pEquipInfo->iEquipID,
								iIndex);
						}
						else
						{

						
						iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><input type=\"text\" SIZE=\"20\" MAXLENGTH=\"19\" name = \"InputValue2%d\"></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID2%d\" value=\"%d\"><input type=\"hidden\" name=\"Typex\" value=\"2\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply9%d\"><!--#endif --></TD></TR>\n",
							iIndex,
							pParamSigValue->pStdSig->pSigName->pFullName[iLanguage],
							//(pEquipInfo->iEquipTypeID != 300 && pEquipInfo->iEquipTypeID != 700) ? szTimeValue : pTimeValue ,//szTimeValue,
							szTimeValue,
							szUnit,
							szTime,
							iIndex,
							iIndex,
							pParamSigValue->pStdSig->iSigID,
							pEquipInfo->iEquipID,
							iIndex);
						}
					}//*/

				}//end------if(STD_SIG_IS_DISPLAY_ON_UI.............................

				//TRACE("\n For OK!!\n");
			}//end------for(i= 0; i < iSetingNumber.........................

			//if there is no display, then display "there are no signal at all!"
			if( strlen(pMBuf)==0) strcpy(pMBuf,pNoSignal);
			//TRACE("pmbuf for control data is %s\n",pMBuf);
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_SETING_SIGNAL_INFO),
				pMBuf);
			DELETE(pMBuf);
			pMBuf = NULL;
		}//end------if(iSignalNumber <= 0  )

		//TRACE("\nAlarm Setting!!!\n");
		//4.prepare alarm data to display
		iAlarmNumber = pEquipInfo->pStdEquip->iAlarmSigNum;
		if(iAlarmNumber <= 0  )
		{
			//it will display no alarm signal html page, add it lately;
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD( MAKE_ALARM_DISPLAY_INFO ),
				pNoSignal);
		}
		else
		{
			//display alarm signal value
			pMBuf = NEW(char,ALARM_DATA_DISPLAY_BUF * iAlarmNumber);
			if(pMBuf == NULL )
			{
				TRACE(" ______no space for pMBuf in alarm part________\n");
				return 0;
			}
			memset(pMBuf, 0x0, (size_t)ALARM_DATA_DISPLAY_BUF * iAlarmNumber);
			iIndex = 0;
			iLen = 0;
			for(i= 0; i < iAlarmNumber && pAlarmSigValue != NULL ; i++, pAlarmSigValue++)
			{
				iIndex++;
				if(iIndex < 10)
				{
					iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><SELECT name = \"InputValue3%d\"><OPTION selected>0</OPTION><OPTION>1</OPTION><OPTION>2</OPTION><OPTION>3</OPTION></SELECT></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID3%d\" value=\"%d\"><input type=\"hidden\" name=\"Typey\" value=\"3\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply500%d\"><!--#endif --></TD></TR>\n",
						iIndex,
						pAlarmSigValue->pStdSig->pSigName->pFullName[iLanguage],
						pAlarmSigValue->pStdSig->iAlarmLevel,
						iIndex,
						iIndex,
						pAlarmSigValue->pStdSig->iSigID,
						pEquipInfo->pStdEquip->iTypeID,
						iIndex);
				}
				else
				{
					iLen  += sprintf(pMBuf + iLen,"<TR><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%s</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0>%d</TD><TD ALIGN=CENTER BGCOLOR=#C0C0C0><SELECT name = \"InputValue3%d\"><OPTION selected>0</OPTION><OPTION>1</OPTION><OPTION>2</OPTION><OPTION>3</OPTION></SELECT></TD><TD ALIGN=CENTER BGCOLOR=#FFFFFF><input type=\"hidden\" name=\"SignalID3%d\" value=\"%d\"><input type=\"hidden\" name=\"Typey\" value=\"3\" ><input type=\"hidden\" name=\"EquipID\" value=\"%d\"><!--#if expr=\"($REMOTE_USER='write')||($REMOTE_USER='admin')\" --><input type=\"submit\" name=\"159\" value=\"Apply50%d\"><!--#endif --></TD></TR>\n",
						iIndex,
						pAlarmSigValue->pStdSig->pSigName->pFullName[iLanguage],
						pAlarmSigValue->pStdSig->iAlarmLevel,
						iIndex,
						iIndex,
						pAlarmSigValue->pStdSig->iSigID,
						pEquipInfo->pStdEquip->iTypeID,
						iIndex);
				
				}
				
			}
			//if there is no display, then display "there are no signal at all!"
			if( strlen(pMBuf)==0) strcpy(pMBuf,pNoSignal);
			//TRACE("pmbuf for alarm display data is %s\n",pMBuf);
			ReplaceString(&pHtml, 
				MAKE_VAR_FIELD(MAKE_ALARM_DISPLAY_INFO),
				pMBuf);
			DELETE(pMBuf);
			pMBuf = NULL;
		}//end------if(iAlarmNumber <= 0  )

		//TRACE("\nWrite File!!!!\n");
		//write data to the final html file
		szfile=NEW(char,50);
		sprintf(szfile,"/var/netscape/%s.htm",RemoveSpace(pEquipInfo->pEquipName->pFullName[0]));
		//szfile=strdup("/var/system.htm");
		if(szfile != NULL && (fp = fopen(szfile,"wb")) != NULL &&
			pHtml != NULL)
		{

			fwrite(pHtml,strlen(pHtml), 1, fp);
			fclose(fp);

		}
		DELETE(szfile);
		szfile = NULL;
		DELETE(pHtml);
		pHtml = NULL;
		//tmp++;
	}
	//tmp++;
	//TRACE("\nOK!!!\n");
}

/*==========================================================================*
* FUNCTION :  GetCurrentTime_uSec
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  Web_PageRefresh
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wan Kun              DATE: 2006-09-06 11:08
*==========================================================================*/
int GetCurrentTime_uSec()
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	return (tv.tv_sec * 1000 + tv.tv_usec/1000);
}

#define		NETSCAPE_PATH			"/var/netscape"
/*==========================================================================*
* FUNCTION :  Web_PageRefresh_AutoCFG
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  StartWebCommunicate 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing              DATE: 2007-07-23 11:08
*==========================================================================*/
static int Web_PageRefresh_AutoCFG()
{
	FILE				*fp1          = NULL;
	EQUIP_INFO			*pEquipInfo  = NULL;
	int					iEquipNum = 0, iRecNum=0,iBufLen,i,iLen1=0,iLen2=0,iLen3=0;
	int					iRecNum_New = 0;
	int					iReturn1=0,iReturn2=0,iReturn3=0;
	int					iLanguage = 0;
	char				*szfile1=NULL;
	char				*szfile2=NULL;
	char				*szfile3=NULL;
	char				*szfile4=NULL;
	char				*iEquipBuffer=NULL;
	char				*iRecBuffer=NULL;
	char				*iAlarmBuffer = NULL;
	char				*pFirstPage = NULL;
	char				*pHtml1 = NULL, *pHtml2 = NULL,*pHtml3 = NULL,*pHtml4 = NULL;
	char				aTemp[50]="";//for multi-language display "Rectifier"
	//char				*pTempBuf = NULL;
	int					temp_switch=0;	//in order to switch the recitifier units
	int					time_begin,time_end;
	int					j = 0;//count number to make judge in iRecNum scope





	/*get equip information*/
	int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	/*get equip number*/
	int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
		0,			
		0,		
		&iBufLen,			
		(void *)&iEquipNum,			
		0);	
	if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
	{
		return 0;
	}

	//get rectifier number
	iRecNum = Web_GetNetscapeRectifierNumber();
	TRACE("----------iRecNum is %d-------------\n",iRecNum);

	//get the current language
	iLanguage = Web_GetCurrentLangValue();
	TRACE("----------iLanguage is %d-------------\n",iLanguage);

	//if(iRecNum ==0) return 0;
	//iRecNum=48;

	//-----------------to generate the true equip list from data exchange interface-------------------------------------------
	//set html file name to szfile
	szfile1=NEW(char,50);
	strcpy(szfile1,"/app/www/netscape/pages/equip.htm");
	szfile2=NEW(char,50);
	strcpy(szfile2,"/app/www/netscape/pages/RectifierList.htm");
	szfile3=NEW(char,50);
	strcpy(szfile3,"/app/www/netscape/pages/active.htm");


	//load html file
	iReturn1= LoadHtmlFile(szfile1, &pHtml1);
	iReturn2= LoadHtmlFile(szfile2, &pHtml2);
	iReturn3= LoadHtmlFile(szfile3, &pHtml3);

	if((iReturn1 > 0) && (iReturn2 > 0)&& (iReturn3 > 0))
	{
		//free source of szfile1 and szfile2
		DELETE(szfile1);
		szfile1 = NULL;
		DELETE(szfile2);
		szfile2 = NULL;
		DELETE(szfile3);
		szfile3 = NULL;

		//malloc space to iEquipBuffer and iRecBuffer
		iEquipBuffer=NEW(char, (iEquipNum-iRecNum)*iEquipBuffer_BUF_SIZE);
		//clear the buffer
		memset(iEquipBuffer, 0x0, (size_t)(iEquipNum-iRecNum)*iEquipBuffer_BUF_SIZE);

		//if no rectifier, there will display no rectifier, so also need to replace html only one time!
		if(iRecNum==0)
		{
			iRecBuffer=NEW(char, iRecBuffer_BUF_SIZE);
			memset(iRecBuffer, 0x0, (size_t)iRecBuffer_BUF_SIZE);
		}
		else 
		{
			iRecBuffer=NEW(char, iRecNum*iRecBuffer_BUF_SIZE);
			memset(iRecBuffer, 0x0, (size_t)iRecNum*iRecBuffer_BUF_SIZE);
		}
		//iAlarmBuffer = NEW(char,iEquipNum*ALARM_DATA_BUF);



		if(Web_PageAlarmRealize(pEquipInfo,&iAlarmBuffer) > 0)
		{
			//TRACE("--------get alarm data ok!----------\n");
		}
		else TRACE("--------get alarm data wrong!----------\n");

		//read data from pEquipInfo and writer it to iEquipBuffer and iRecBuffer
		for(i=0;i<iEquipNum && pEquipInfo != NULL;i++)
		{
			//formation of equip list buffer and rectifier list buffer
			//TRACE("---type id is %d ---- name is %s---------\n",pEquipInfo->iEquipTypeID,pEquipInfo->pEquipName->pFullName[0]);
			if((pEquipInfo->iEquipTypeID>200)&&(pEquipInfo->iEquipTypeID<300))
			{
				//TRACE(" ______into rectifier module________\n");
				if(temp_switch==0)
				{
					//only display RecitifierUnit equip_list_display.htm
					//to display multilanguage, so add this line code to get it!
					strncpy(aTemp,pEquipInfo->pEquipName->pFullName[iLanguage],(strlen(pEquipInfo->pEquipName->pFullName[iLanguage])-1));
					TRACE("size is %d,after change RecitifierUnits is display as %s\n",strlen(pEquipInfo->pEquipName->pFullName[iLanguage]),aTemp);

					iLen1 +=sprintf(iEquipBuffer+iLen1,"<p><b><a href=\"RectifierList.htm\">%s</a></b><br></p>\n",aTemp);
					//at same time , it will output RectifierList.htm
					if(iRecNum == 0)
						//if no rectifier, there will display no rectifier on web page
						iLen2 +=sprintf(iRecBuffer+iLen2,"<p><b><a href=\"equip.htm\"><script language=\"javascript\">showMsg()</script></a></b><br></p>\n");
					else
					{
						if( j < iRecNum )// only display installed rectifier
						{
							//when i_rectifier=1,put rectifier1 to iRecBuffer
							iLen2 +=sprintf(iRecBuffer+iLen2,"<p><b><a href=\"%s.htm\">%s</a></b><br></p>\n",
								RemoveSpace(pEquipInfo->pEquipName->pFullName[0]),
								pEquipInfo->pEquipName->pFullName[iLanguage]);
							j++;
							Web_PageRealize(pEquipInfo);
						}

					}
					temp_switch=1;
				}
				//when i_rectifier>1,put other rectifier to iRecBuffer
				else if(iRecNum !=0)
				{
					if( j < iRecNum)// only display installed rectifier
					{
						iLen2 +=sprintf(iRecBuffer+iLen2,"<p><b><a href=\"%s.htm\">%s</a></b><br></p>\n",
							RemoveSpace(pEquipInfo->pEquipName->pFullName[0]),
							pEquipInfo->pEquipName->pFullName[iLanguage]);
						j++;
						Web_PageRealize(pEquipInfo);
					}
				}
			}
			else if(pEquipInfo->iEquipTypeID==100 || (pEquipInfo->iEquipTypeID>=300 && pEquipInfo->iEquipTypeID<1100) || (pEquipInfo->iEquipTypeID >= 1200 && pEquipInfo->iEquipTypeID < 1300) || (pEquipInfo->iEquipTypeID >= 1500 && pEquipInfo->iEquipTypeID < 1600))
			{
				//put other equip info into iEquipBuffer
				if(pEquipInfo->bWorkStatus == TRUE)
				{
					iLen1 +=sprintf(iEquipBuffer+iLen1,"<p><b><a href=\"%s.htm\">%s</a></b><br></p>\n",
						RemoveSpace(pEquipInfo->pEquipName->pFullName[0]),
						pEquipInfo->pEquipName->pFullName[iLanguage]);
					Web_PageRealize(pEquipInfo);

				}

				//to form the alarm buffer
				/*  modified by wk 09/13/2006
				if(Web_PageAlarmRealize(pEquipInfo,&pTempBuf) > 0)
				{
				iLen3 +=sprintf(iAlarmBuffer+iLen3,"%s",pTempBuf);
				DELETE(pTempBuf);
				pTempBuf = NULL;
				}//*/
			}
			pEquipInfo++;
		}

		//replace the html file
		ReplaceString(&pHtml1, 
			MAKE_VAR_FIELD(MAKE_EQUIP_LIST),
			iEquipBuffer);
		ReplaceString(&pHtml2, 
			MAKE_VAR_FIELD(MAKE_REC_LIST),
			iRecBuffer);
		ReplaceString(&pHtml3, 
			MAKE_VAR_FIELD(MAKE_ALARM_INFO),
			iAlarmBuffer);

		//write content to the html  file
		szfile1=NEW(char,50);
		strcpy(szfile1,"/var/netscape/equip.htm");
		szfile2=NEW(char,50);
		strcpy(szfile2,"/var/netscape/RectifierList.htm");
		szfile3=NEW(char,50);
		strcpy(szfile3,"/var/netscape/active.htm");



		if(szfile1 != NULL && (fp1 = fopen(szfile1,"wb")) != NULL &&
			pHtml1 != NULL)
		{

			fwrite(pHtml1,strlen(pHtml1), 1, fp1);
			fclose(fp1);

		}
		if(szfile2 != NULL && (fp1 = fopen(szfile2,"wb")) != NULL &&
			pHtml2 != NULL)
		{

			fwrite(pHtml2,strlen(pHtml2), 1, fp1);
			fclose(fp1);

		}
		if(szfile3 != NULL && (fp1 = fopen(szfile3,"wb")) != NULL &&
			pHtml3 != NULL)
		{

			fwrite(pHtml3,strlen(pHtml3), 1, fp1);
			fclose(fp1);

		}




		//free szfile1 and szfile2
		if(szfile1 != NULL)
		{
			DELETE(szfile1);
			szfile1 = NULL;
		}
		if(szfile2 != NULL)
		{
			DELETE(szfile2);
			szfile2 = NULL;
		}
		if(szfile3 != NULL)
		{
			DELETE(szfile3);
			szfile3 = NULL;
		}
		//free iEquipBuffer and iRecBuffer,pHtml1,pHtml2
		DELETE(iEquipBuffer);
		iEquipBuffer=NULL;
		DELETE(iRecBuffer);
		iRecBuffer=NULL;
		DELETE(iAlarmBuffer);
		iAlarmBuffer=NULL;
		DELETE(pHtml1);
		pHtml1 = NULL;
		DELETE(pHtml2);
		pHtml2 = NULL;
		DELETE(pHtml3);
		pHtml3 = NULL;
		//TRACE(" ______We can not find the source html file!________\n");
		RunThread_Heartbeat(RunThread_GetId(NULL));
	}
	else 
	{
		//free source of szfile1 and szfile2
		DELETE(szfile1);
		szfile1 = NULL;
		DELETE(szfile2);
		szfile2 = NULL;
		DELETE(szfile3);
		szfile3 = NULL;
		DELETE(pHtml1);
		pHtml1 = NULL;
		DELETE(pHtml2);
		pHtml2 = NULL;
		DELETE(pHtml3);
		pHtml3 = NULL;
		//TRACE(" ______We can not find the source html file!________\n");
		return 0;
	}

}
/*==========================================================================*
* FUNCTION :  Web_PageRefresh
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  StartWebCommunicate 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wan Kun              DATE: 2006-08-23 11:08
*==========================================================================*/
static int Web_PageRefresh(IN int *iQuitCommand)
{
	TRACE(" ______into Web_PageRefresh________\n");

	FILE				*fp1          = NULL;
	EQUIP_INFO			*pEquipInfo  = NULL;
	int					iEquipNum = 0, iRecNum=0,iBufLen,i,iLen1=0,iLen2=0,iLen3=0;
	int					iRecNum_New = 0;
	int					iReturn1=0,iReturn2=0,iReturn3=0;
	int					iLanguage = 0;
	char				*szfile1=NULL;
	char				*szfile2=NULL;
	char				*szfile3=NULL;
	char				*szfile4=NULL;
	char				*iEquipBuffer=NULL;
	char				*iRecBuffer=NULL;
	char				*iAlarmBuffer = NULL;
	char				*pFirstPage = NULL;
	char				*pHtml1 = NULL, *pHtml2 = NULL,*pHtml3 = NULL,*pHtml4 = NULL;
	char				aTemp[50]="";//for multi-language display "Rectifier"
	//char				*pTempBuf = NULL;
	int					temp_switch=0;	//in order to switch the recitifier units
	int					time_begin,time_end;
	int					j = 0;//count number to make judge in iRecNum scope

	time_begin=GetCurrentTime_uSec();



	/*get equip information*/
	int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	/*get equip number*/
	int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
		0,			
		0,		
		&iBufLen,			
		(void *)&iEquipNum,			
		0);	
	if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
	{
		return 0;
	}

	//get rectifier number
	iRecNum = Web_GetNetscapeRectifierNumber();
	TRACE("----------iRecNum is %d-------------\n",iRecNum);

	//get the current language
	iLanguage = Web_GetCurrentLangValue();
	TRACE("----------iLanguage is %d-------------\n",iLanguage);

	//if(iRecNum ==0) return 0;
	//iRecNum=48;

	//-----------------to generate the true equip list from data exchange interface-------------------------------------------
	//set html file name to szfile
	szfile1=NEW(char,50);
	strcpy(szfile1,"/app/www/netscape/pages/equip.htm");
	szfile2=NEW(char,50);
	strcpy(szfile2,"/app/www/netscape/pages/RectifierList.htm");
	szfile3=NEW(char,50);
	strcpy(szfile3,"/app/www/netscape/pages/active.htm");


	//load html file
	iReturn1= LoadHtmlFile(szfile1, &pHtml1);
	iReturn2= LoadHtmlFile(szfile2, &pHtml2);
	iReturn3= LoadHtmlFile(szfile3, &pHtml3);

	if((iReturn1 > 0) && (iReturn2 > 0)&& (iReturn3 > 0))
	{
		//free source of szfile1 and szfile2
		DELETE(szfile1);
		szfile1 = NULL;
		DELETE(szfile2);
		szfile2 = NULL;
		DELETE(szfile3);
		szfile3 = NULL;

		//malloc space to iEquipBuffer and iRecBuffer
		iEquipBuffer=NEW(char, (iEquipNum-iRecNum)*iEquipBuffer_BUF_SIZE);
		//clear the buffer
		memset(iEquipBuffer, 0x0, (size_t)(iEquipNum-iRecNum)*iEquipBuffer_BUF_SIZE);

		//if no rectifier, there will display no rectifier, so also need to replace html only one time!
		if(iRecNum==0)
		{
			iRecBuffer=NEW(char, iRecBuffer_BUF_SIZE);
			memset(iRecBuffer, 0x0, (size_t)iRecBuffer_BUF_SIZE);
		}
		else 
		{
			iRecBuffer=NEW(char, iRecNum*iRecBuffer_BUF_SIZE);
			memset(iRecBuffer, 0x0, (size_t)iRecNum*iRecBuffer_BUF_SIZE);
		}
		//iAlarmBuffer = NEW(char,iEquipNum*ALARM_DATA_BUF);



		if(Web_PageAlarmRealize(pEquipInfo,&iAlarmBuffer) > 0)
		{
			//TRACE("--------get alarm data ok!----------\n");
		}
		else TRACE("--------get alarm data wrong!----------\n");

		//read data from pEquipInfo and writer it to iEquipBuffer and iRecBuffer
		for(i=0;i<iEquipNum && pEquipInfo != NULL;i++)
		{
			//formation of equip list buffer and rectifier list buffer
			//TRACE("---type id is %d ---- name is %s---------\n",pEquipInfo->iEquipTypeID,pEquipInfo->pEquipName->pFullName[0]);
			if((pEquipInfo->iEquipTypeID>200)&&(pEquipInfo->iEquipTypeID<300))
			{
				//TRACE(" ______into rectifier module________\n");
				if(temp_switch==0)
				{
					//only display RecitifierUnit equip_list_display.htm
					//to display multilanguage, so add this line code to get it!
					strncpy(aTemp,pEquipInfo->pEquipName->pFullName[iLanguage],(strlen(pEquipInfo->pEquipName->pFullName[iLanguage])-1));
					TRACE("size is %d,after change RecitifierUnits is display as %s\n",strlen(pEquipInfo->pEquipName->pFullName[iLanguage]),aTemp);

					iLen1 +=sprintf(iEquipBuffer+iLen1,"<p><b><a href=\"RectifierList.htm\">%s</a></b><br></p>\n",aTemp);
					//at same time , it will output RectifierList.htm
					if(iRecNum == 0)
						//if no rectifier, there will display no rectifier on web page
						iLen2 +=sprintf(iRecBuffer+iLen2,"<p><b><a href=\"equip.htm\"><script language=\"javascript\">showMsg()</script></a></b><br></p>\n");
					else
					{
						if( j < iRecNum )// only display installed rectifier
						{
							//when i_rectifier=1,put rectifier1 to iRecBuffer
							iLen2 +=sprintf(iRecBuffer+iLen2,"<p><b><a href=\"%s.htm\">%s</a></b><br></p>\n",
								RemoveSpace(pEquipInfo->pEquipName->pFullName[0]),
								pEquipInfo->pEquipName->pFullName[iLanguage]);
							j++;
							Web_PageRealize(pEquipInfo);
						}

					}
					temp_switch=1;
				}
				//when i_rectifier>1,put other rectifier to iRecBuffer
				else if(iRecNum !=0)
				{
					if( j < iRecNum)// only display installed rectifier
					{
						iLen2 +=sprintf(iRecBuffer+iLen2,"<p><b><a href=\"%s.htm\">%s</a></b><br></p>\n",
							RemoveSpace(pEquipInfo->pEquipName->pFullName[0]),
							pEquipInfo->pEquipName->pFullName[iLanguage]);
						j++;
						Web_PageRealize(pEquipInfo);
					}
				}
			}
			else if((pEquipInfo->iEquipTypeID>=300 && pEquipInfo->iEquipTypeID<1100) || (pEquipInfo->iEquipTypeID >= 1200 && pEquipInfo->iEquipTypeID < 1300) || (pEquipInfo->iEquipTypeID >= 1500 && pEquipInfo->iEquipTypeID < 1600) || pEquipInfo->iEquipTypeID==100 || pEquipInfo->iEquipTypeID==200)
			{
				//put other equip info into iEquipBuffer
				if(pEquipInfo->bWorkStatus == TRUE)
				{
					iLen1 +=sprintf(iEquipBuffer+iLen1,"<p><b><a href=\"%s.htm\">%s</a></b><br></p>\n",
						RemoveSpace(pEquipInfo->pEquipName->pFullName[0]),
						pEquipInfo->pEquipName->pFullName[iLanguage]);
					Web_PageRealize(pEquipInfo);

				}

				//to form the alarm buffer
				/*  modified by wk 09/13/2006
				if(Web_PageAlarmRealize(pEquipInfo,&pTempBuf) > 0)
				{
				iLen3 +=sprintf(iAlarmBuffer+iLen3,"%s",pTempBuf);
				DELETE(pTempBuf);
				pTempBuf = NULL;
				}//*/
			}
			pEquipInfo++;
		}

		//replace the html file
		ReplaceString(&pHtml1, 
			MAKE_VAR_FIELD(MAKE_EQUIP_LIST),
			iEquipBuffer);
		ReplaceString(&pHtml2, 
			MAKE_VAR_FIELD(MAKE_REC_LIST),
			iRecBuffer);
		ReplaceString(&pHtml3, 
			MAKE_VAR_FIELD(MAKE_ALARM_INFO),
			iAlarmBuffer);

		//write content to the html  file
		szfile1=NEW(char,50);
		strcpy(szfile1,"/var/netscape/equip.htm");
		szfile2=NEW(char,50);
		strcpy(szfile2,"/var/netscape/RectifierList.htm");
		szfile3=NEW(char,50);
		strcpy(szfile3,"/var/netscape/active.htm");



		if(szfile1 != NULL && (fp1 = fopen(szfile1,"wb")) != NULL &&
			pHtml1 != NULL)
		{

			fwrite(pHtml1,strlen(pHtml1), 1, fp1);
			fclose(fp1);

		}
		if(szfile2 != NULL && (fp1 = fopen(szfile2,"wb")) != NULL &&
			pHtml2 != NULL)
		{

			fwrite(pHtml2,strlen(pHtml2), 1, fp1);
			fclose(fp1);

		}
		if(szfile3 != NULL && (fp1 = fopen(szfile3,"wb")) != NULL &&
			pHtml3 != NULL)
		{

			fwrite(pHtml3,strlen(pHtml3), 1, fp1);
			fclose(fp1);

		}




		//free szfile1 and szfile2
		if(szfile1 != NULL)
		{
			DELETE(szfile1);
			szfile1 = NULL;
		}
		if(szfile2 != NULL)
		{
			DELETE(szfile2);
			szfile2 = NULL;
		}
		if(szfile3 != NULL)
		{
			DELETE(szfile3);
			szfile3 = NULL;
		}
		//free iEquipBuffer and iRecBuffer,pHtml1,pHtml2
		DELETE(iEquipBuffer);
		iEquipBuffer=NULL;
		DELETE(iRecBuffer);
		iRecBuffer=NULL;
		DELETE(iAlarmBuffer);
		iAlarmBuffer=NULL;
		DELETE(pHtml1);
		pHtml1 = NULL;
		DELETE(pHtml2);
		pHtml2 = NULL;
		DELETE(pHtml3);
		pHtml3 = NULL;
		//TRACE(" ______We can not find the source html file!________\n");
		RunThread_Heartbeat(RunThread_GetId(NULL));
	}
	else 
	{
		//free source of szfile1 and szfile2
		DELETE(szfile1);
		szfile1 = NULL;
		DELETE(szfile2);
		szfile2 = NULL;
		DELETE(szfile3);
		szfile3 = NULL;
		DELETE(pHtml1);
		pHtml1 = NULL;
		DELETE(pHtml2);
		pHtml2 = NULL;
		DELETE(pHtml3);
		pHtml3 = NULL;
		//TRACE(" ______We can not find the source html file!________\n");
		return 0;
	}
	time_end = GetCurrentTime_uSec();
	TRACE(" ______We spent time is %d!________\n",(time_end-time_begin));

	//delay some seconds to let other thread do something!
	int iSleepTime =REFRESH_PERIOD - (time_end - time_begin);
	if ( iSleepTime > 0 ) usleep(iSleepTime);

	//to refresh web page in a main recycle
	BOOL bExitApp = FALSE; 
	while( !bExitApp )
	{

		//begin to count time
		time_begin=GetCurrentTime_uSec();

		//say hi to watchdog
		RunThread_Heartbeat(RunThread_GetId(NULL));

		//////////////////////////////////////////////////////////////////////////
		//Auto_Config
#ifdef AUTO_CONFIG
		int	iRst2;
		RUN_THREAD_MSG msg2;

		iRst2 = RunThread_GetMessage(RunThread_GetId(NULL), &msg2, FALSE, 0);

		if(iRst2 == ERR_OK)
		{
			TRACE("msg2.dwMsg : %d\n",msg2.dwMsg);
			if(msg2.dwMsg == MSG_AUTOCONFIG)
			{
				switch(msg2.dwParam1)
				{
				case ID_AUTOCFG_START:
					TRACE("WEB START AUTO CONFIG!!!\n");
					iAutoCFGStartFlage = 1;
					break;

				case ID_AUTOCFG_REFRESH_DATA:
					TRACE("WEB START AUTO CONFIG REFRESH!!!\n");
					Web_PageRefresh_AutoCFG();
					iAutoCFGStartFlage = 0;
					break;
				default:
					break;
				}

			}
		}

		if(iAutoCFGStartFlage == 1)
		{
			continue;
		}
#endif
		if(iSlaveMode == 1)
		{
			continue;
		}
		//////////////////////////////////////////////////////////////////////////

		//init the pEquipinfo and equip number
		pEquipInfo = pEquipInfo - iEquipNum;

		//load data about alarm info
		szfile3=NEW(char,50);
		strcpy(szfile3,"/app/www/netscape/pages/active.htm");
		iReturn3= LoadHtmlFile(szfile3, &pHtml3);
		if(iReturn3 <= 0)
		{
			TRACE("------error in loading active.htm in main cycle!--------\n");
			bExitApp = TRUE;
		}
		DELETE(szfile3);
		szfile3 = NULL;


		//Refresh first page

		//to get the latest iRecNum
		j=0;
		iRecNum_New = Web_GetNetscapeRectifierNumber();
		//TRACE("--------iRecNum_New is %d --------\n",iRecNum_New);
		//need to test when iRecNum changed-----------------------------------------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		if( iRecNum != iRecNum_New)
		{
			//change iRecNum to the latest value
			iRecNum = iRecNum_New;

			// restore RectifierList.htm and other web page
			szfile2=NEW(char,50);
			strcpy(szfile2,"/app/www/netscape/pages/RectifierList.htm");
			iReturn2= LoadHtmlFile(szfile2, &pHtml2);

			if(iReturn2 > 0)
			{
				DELETE(szfile2);
				szfile2 = NULL;
				if(iRecNum==0) iRecBuffer=NEW(char, 100);
				else iRecBuffer=NEW(char, iRecNum*250);
				iLen2 = 0;
				iLen3 = 0;

				for(i=0;i<iEquipNum && pEquipInfo != NULL;i++)
				{
					if((pEquipInfo->iEquipTypeID>200)&&(pEquipInfo->iEquipTypeID<300))
					{

						//TRACE("=========1=============\n");
						//only first time need to sprintf "There is no RecitifierUnit" into html
						if((iRecNum == 0) && (iLen2 == 0))
							//if no rectifier, there will display no recitifier on web page
							iLen2 +=sprintf(iRecBuffer+iLen2,"<p><b><a href=\"equip.htm\">There is no RecitifierUnit</a></b><br></p>\n");
						else if( iRecNum != 0 )
						{
							//only display installed module
							if( j < iRecNum )
							{
								//TRACE("=========3=============\n");
								//when i_rectifier=1,put rectifier1 to iRecBuffer
								iLen2 +=sprintf(iRecBuffer+iLen2,"<p><b><a href=\"%s.htm\">%s</a></b><br></p>\n",
									RemoveSpace(pEquipInfo->pEquipName->pFullName[0]),
									pEquipInfo->pEquipName->pFullName[iLanguage]);
								j++;
								Web_PageRealize(pEquipInfo);
								usleep(DELAY_PERIOD);
								if (*iQuitCommand == SERVICE_STOP_RUNNING)
								{
									bExitApp = TRUE;
									break;
								}
							}

						}
					}
					else if((pEquipInfo->iEquipTypeID>=300 && pEquipInfo->iEquipTypeID<1100) || (pEquipInfo->iEquipTypeID >= 1200 && pEquipInfo->iEquipTypeID < 1300) || (pEquipInfo->iEquipTypeID >= 1500 && pEquipInfo->iEquipTypeID < 1600) || pEquipInfo->iEquipTypeID==100 || pEquipInfo->iEquipTypeID==200)
					{
						//TRACE("=========2=============\n");
						if(pEquipInfo->bWorkStatus == TRUE )
						{
							Web_PageRealize( pEquipInfo );
						}

						usleep(DELAY_PERIOD);
						if (*iQuitCommand == SERVICE_STOP_RUNNING)
						{
							bExitApp = TRUE;
							break;
						}
						//to form data buf of active.htm
						/*  modified by wk 09/13/2006
						if(Web_PageAlarmRealize(pEquipInfo,&pTempBuf) > 0)
						{
						iLen3 +=sprintf(iAlarmBuffer+iLen3,"%s",pTempBuf);
						DELETE(pTempBuf);
						pTempBuf = NULL;
						}
						//*/
					}
					pEquipInfo++;
				}//end for(i=0;i<iEquipInfo;.........
				ReplaceString(&pHtml2, 
					MAKE_VAR_FIELD(MAKE_REC_LIST),
					iRecBuffer);
				szfile2=NEW(char,50);
				strcpy(szfile2,"/var/netscape/RectifierList.htm");
				if(szfile2 != NULL && (fp1 = fopen(szfile2,"wb")) != NULL && pHtml2 != NULL)
				{

					fwrite(pHtml2,strlen(pHtml2), 1, fp1);
					fclose(fp1);

				}
				if(szfile2 != NULL)
				{
					DELETE(szfile2);
					szfile2 = NULL;
				}
				DELETE(iRecBuffer);
				iRecBuffer=NULL;
				DELETE(pHtml2);
				pHtml2 = NULL;
			}
			else
			{
				DELETE(szfile2);
				szfile2 = NULL;
				DELETE(pHtml2);
				pHtml2 = NULL;
				bExitApp = TRUE;
				TRACE("----------load pHtml2 error in recycle--------\n");
			}

		}
		else
		{
			iLen3 = 0;
			//TRACE("===============4==============\n");
			//the number of iRecNum no change

			Web_RefreshEquipList();

			for(i=0;i<iEquipNum && pEquipInfo != NULL;i++)
			{
				if(pEquipInfo->bWorkStatus == TRUE )
				{
					Web_PageRealize( pEquipInfo );
				}


				if (*iQuitCommand == SERVICE_STOP_RUNNING)
				{
					bExitApp = TRUE;
					break;
				}

				usleep(DELAY_PERIOD);
				//to form data buf of active.htm
				/*  modified by wk 09/13/2006
				if(Web_PageAlarmRealize(pEquipInfo,&pTempBuf) > 0)
				{
				iLen3 +=sprintf(iAlarmBuffer+iLen3,"%s",pTempBuf);
				DELETE(pTempBuf);
				pTempBuf = NULL;
				}
				//*/
				pEquipInfo++;
			}
		}//end of if( iRecNum != iRecNum_New)........

		//Refresh first page
		if(Web_MakeFirstPage(&pFirstPage) > 0)
		{
			TRACE("--------get First page data ok!----------\n");
#define	WEB_NETSCAPE_MAIN_HTML			"/app/www/netscape/pages/main.htm"
			szfile4 = "/var/netscape/main.htm";
			if(LoadHtmlFile(WEB_NETSCAPE_MAIN_HTML, &pHtml4 ))
			{

				ReplaceString(&pHtml4, 
					MAKE_VAR_FIELD(MAKE_FIRST_PAGE_INFO),
					pFirstPage);			
				if((fp1 = fopen(szfile4,"wb")) != NULL && pHtml4 != NULL)
				{

					fwrite(pHtml4,strlen(pHtml4), 1, fp1);
					fclose(fp1);
				}
			}

			DELETE(pFirstPage);
			pFirstPage = NULL;
			DELETE(pHtml4);
			pHtml4 = NULL;
		}

		//form iAlarmBuffer every time
		//iAlarmBuffer = NEW(char,iEquipNum*ALARM_DATA_BUF);
		if(Web_PageAlarmRealize(pEquipInfo,&iAlarmBuffer) > 0)
		{
			//TRACE("--------get alarm data ok!----------\n");
		}
		else bExitApp = TRUE;

		//form active.htm every time
		ReplaceString(&pHtml3, 
			MAKE_VAR_FIELD(MAKE_ALARM_INFO),
			iAlarmBuffer);
		szfile3=NEW(char,50);
		strcpy(szfile3,"/var/netscape/active.htm");
		if(szfile3 != NULL && (fp1 = fopen(szfile3,"wb")) != NULL &&
			pHtml3 != NULL)
		{
			//TRACE("===============5==============\n");
			fwrite(pHtml3,strlen(pHtml3), 1, fp1);
			fclose(fp1);

		}
		if(szfile3 != NULL)
		{
			DELETE(szfile3);
			szfile3 = NULL;
		}
		DELETE(iAlarmBuffer);
		iAlarmBuffer=NULL;
		DELETE(pHtml3);
		pHtml3 = NULL;

		//caculate the rest of time 
		time_end = GetCurrentTime_uSec();
		TRACE("spent time is about %d\n",(time_end - time_begin));

		//fix bug by wankun: if user change the current time, the value of (time_end-time_begin) may be minus, so put an judge here!
		if( (time_end - time_begin) > 0)
		{
			iSleepTime =REFRESH_PERIOD - (time_end - time_begin);
		}
		else
		{
			iSleepTime = -1;
		}
		/*
		if(iSleepTime > 0)
		{
		usleep(iSleepTime);
		}
		if (*iQuitCommand == SERVICE_STOP_RUNNING)
		{
		bExitApp = TRUE;
		}
		*/


		while(iSleepTime > 0 && !bExitApp)
		{
			usleep(1000);
			if (*iQuitCommand == SERVICE_STOP_RUNNING)
			{
				bExitApp = TRUE;
				break;
			}
			iSleepTime = iSleepTime - 1000;
		}


		//Check the quit command


	}
	//*/
	return PAGE_OK;
}
/*==========================================================================*
* FUNCTION :  Web_RefreshEquipList
* PURPOSE  :  
* CALLS    : 
* CALLED BY:  StartWebCommunicate 
* ARGUMENTS:  
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wan Kun              DATE: 2006-08-23 11:08
*==========================================================================*/
static int Web_RefreshEquipList(void)
{
	FILE				*fp1          = NULL;
	EQUIP_INFO			*pEquipInfo  = NULL;
	int					iEquipNum = 0, iRecNum=0,iBufLen,i,iLen1=0,iLen2=0,iLen3=0;
	int					iRecNum_New = 0;
	int					iReturn1=0,iReturn2=0,iReturn3=0;
	int					iLanguage = 0;
	char				*szfile1=NULL;
	char				*pHtml1 = NULL;
	char				*iEquipBuffer=NULL;
	int				temp_switch = 0;
	char				aTemp[50]="";//for multi-language display "Rectifier"
	/*get equip information*/
	int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
		0,			
		0,		
		&iBufLen,			
		&pEquipInfo,			
		0);

	/*get equip number*/
	int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
		0,			
		0,		
		&iBufLen,			
		(void *)&iEquipNum,			
		0);	
	if(iError1 != ERR_DXI_OK || iError2 != ERR_DXI_OK)
	{
		return 0;
	}

	iLanguage = Web_GetCurrentLangValue();


	//if(iRecNum ==0) return 0;
	//iRecNum=48;

	//-----------------to generate the true equip list from data exchange interface-------------------------------------------
	//set html file name to szfile
	szfile1=NEW(char,50);
	strcpy(szfile1,"/app/www/netscape/pages/equip.htm");
	

	//load html file
	iReturn1= LoadHtmlFile(szfile1, &pHtml1);

	if(iReturn1 > 0)
	{
		//free source of szfile1 and szfile2
		DELETE(szfile1);
		szfile1 = NULL;

		//malloc space to iEquipBuffer and iRecBuffer
		iEquipBuffer=NEW(char, (iEquipNum-iRecNum)*iEquipBuffer_BUF_SIZE);
		//clear the buffer
		memset(iEquipBuffer, 0x0, (size_t)(iEquipNum-iRecNum)*iEquipBuffer_BUF_SIZE);

		for(i=0;i<iEquipNum && pEquipInfo != NULL;i++)
		{
			//formation of equip list buffer and rectifier list buffer
			//TRACE("---type id is %d ---- name is %s---------\n",pEquipInfo->iEquipTypeID,pEquipInfo->pEquipName->pFullName[0]);
			if((pEquipInfo->iEquipTypeID>200)&&(pEquipInfo->iEquipTypeID<300))
			{
				//TRACE(" ______into rectifier module________\n");
				if(temp_switch==0)
				{
					//only display RecitifierUnit equip_list_display.htm
					//to display multilanguage, so add this line code to get it!
					strncpy(aTemp,pEquipInfo->pEquipName->pFullName[iLanguage],(strlen(pEquipInfo->pEquipName->pFullName[iLanguage])-1));
					TRACE("size is %d,after change RecitifierUnits is display as %s\n",strlen(pEquipInfo->pEquipName->pFullName[iLanguage]),aTemp);

					iLen1 +=sprintf(iEquipBuffer+iLen1,"<p><b><a href=\"RectifierList.htm\">%s</a></b><br></p>\n",aTemp);
					//at same time , it will output RectifierList.htm

					temp_switch=1;
				}
				//when i_rectifier>1,put other rectifier to iRecBuffer

			}
			else if((pEquipInfo->iEquipTypeID>=300 && pEquipInfo->iEquipTypeID<1100) || (pEquipInfo->iEquipTypeID >= 1200 && pEquipInfo->iEquipTypeID < 1300) || (pEquipInfo->iEquipTypeID >= 1500 && pEquipInfo->iEquipTypeID < 1600) || pEquipInfo->iEquipTypeID==100 || pEquipInfo->iEquipTypeID==200)
			{
				//put other equip info into iEquipBuffer
				if(pEquipInfo->bWorkStatus == TRUE)
				{
					iLen1 +=sprintf(iEquipBuffer+iLen1,"<p><b><a href=\"%s.htm\">%s</a></b><br></p>\n",
						RemoveSpace(pEquipInfo->pEquipName->pFullName[0]),
						pEquipInfo->pEquipName->pFullName[iLanguage]);
					Web_PageRealize(pEquipInfo);

				}

				//to form the alarm buffer
				/*  modified by wk 09/13/2006
				if(Web_PageAlarmRealize(pEquipInfo,&pTempBuf) > 0)
				{
				iLen3 +=sprintf(iAlarmBuffer+iLen3,"%s",pTempBuf);
				DELETE(pTempBuf);
				pTempBuf = NULL;
				}//*/
			}
			pEquipInfo++;
		}

		//replace the html file
		ReplaceString(&pHtml1, 
			MAKE_VAR_FIELD(MAKE_EQUIP_LIST),
			iEquipBuffer);
		

		//write content to the html  file
		szfile1=NEW(char,50);
		strcpy(szfile1,"/var/netscape/equip.htm");
		


		if(szfile1 != NULL && (fp1 = fopen(szfile1,"wb")) != NULL &&
			pHtml1 != NULL)
		{

			fwrite(pHtml1,strlen(pHtml1), 1, fp1);
			fclose(fp1);

		}
		



		//free szfile1 and szfile2
		if(szfile1 != NULL)
		{
			DELETE(szfile1);
			szfile1 = NULL;
		}
		
		//free iEquipBuffer and iRecBuffer,pHtml1,pHtml2
		DELETE(iEquipBuffer);
		iEquipBuffer=NULL;
		
		DELETE(pHtml1);
		pHtml1 = NULL;
		//TRACE(" ______We can not find the source html file!________\n");
		RunThread_Heartbeat(RunThread_GetId(NULL));
	}
	

}

static int Web_CopyNeedFile()
{
	system("cp /app/www/netscape/pages/bottom.htm  /var/netscape/bottom.htm");
	system("cp /app/www/netscape/pages/contents.htm  /var/netscape/contents.htm");
	system("cp /app/www/netscape/pages/error.htm  /var/netscape/error.htm");
	system("cp /app/www/netscape/pages/errorcontrol.htm  /var/netscape/errorcontrol.htm");
	system("cp /app/www/netscape/pages/index.htm  /var/netscape/index.htm ");
	system("cp /app/www/netscape/pages/main.htm  /var/netscape/main.htm ");
	system("cp /app/www/netscape/pages/mainhigh.htm  /var/netscape/mainhigh.htm");
	system("cp /app/www/netscape/pages/top.htm   /var/netscape/top.htm");
	system("cp /app/www/netscape/pages/toplow.htm  /var/netscape/toplow.htm");
	system("cp /app/www/netscape/pages/ipconf.htm  /var/netscape/ipconf.htm");
	system("cp /app/www/netscape/pages/snmpconf.htm  /var/netscape/snmpconf.htm");
	system("cp /app/www/netscape/pages/eem*  /var/netscape/");
	system("cp /app/www/netscape/pages/sys*  /var/netscape/");
	system("cp /app/www/netscape/pages/log*  /var/netscape/");
	system("cp /app/www/netscape/pages/other*  /var/netscape/");
	system("cp /app/www/netscape/pages/time*  /var/netscape/");
	system("cp /app/www/netscape/pages/sitelanguage.htm  /var/netscape/");
	system("cp /app/www/netscape/pages/restart.htm  /var/netscape/");
	system("cp /app/www/netscape/pages/recover.htm  /var/netscape/");

	system("mkdir /var/netscape/img/; cp /app/www/netscape/pages/img/* /var/netscape/img/");
	system("cp /app/config/private/web/htaccess /var/netscape/.htaccess");
	//added by wankun 2007/03/19
	system("cp /app/www/netscape/pages/history_alarm.htm /var/netscape/history_alarm.htm");
	system("cp /app/www/netscape/pages/history_data.htm /var/netscape/history_data.htm");
	system("cp /app/www/netscape/pages/command_log.htm /var/netscape/command_log.htm");
	system("cp /app/www/netscape/pages/history_log.htm /var/netscape/history_log.htm");
	system("cp /app/www/netscape/pages/Alarm.htm /var/netscape/Alarm.htm");
	system("cp /app/www/netscape/pages/Data_record.htm /var/netscape/Data_record.htm");
	system("cp /app/www/netscape/pages/Control_record.htm /var/netscape/Control_record.htm");
	system("cp /app/www/netscape/pages/System_log.htm /var/netscape/System_log.htm");
	system("cp /app/www/netscape/pages/bat_test_log.htm /var/netscape/bat_test_log.htm");
	system("cp /app/www/netscape/pages/Bat_test.htm /var/netscape/Bat_test.htm");
	system("cp /app/www/netscape/pages/passwd.htm /var/netscape/passwd.htm");
	system("cp /app/www/netscape/pages/sitemap.htm /var/netscape/sitemap.htm");
	system("cp /app/www/netscape/pages/clear_bat_log.htm /var/netscape/clear_bat_log.htm");
	system("cp /app/www/netscape/pages/timecheck.js /var/netscape/timecheck.js");
	//ended by wankun 2007/05/10

#define VAR_EXPLOER_TYPE		"VAR_EXPLOER_TYPE"
#define VAR_FILE_PATH1			"/app/www/firstpage.htm"
#define	VAR_FILE_PATH2			"/app/www/index/firstpage.htm"

	char *pHtml = NULL;
	FILE *fp1;
	if(LoadHtmlFile(VAR_FILE_PATH1, &pHtml) > 0)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD(VAR_EXPLOER_TYPE),	"1");
		if((fp1 = fopen(VAR_FILE_PATH2,"wb")) != NULL &&   pHtml != NULL)
		{

			fwrite(pHtml,strlen(pHtml), 1, fp1);
			fclose(fp1);

		}
		DELETE(pHtml);
		pHtml = NULL;
	}

}


typedef struct tagGETALLSIGNAL
{
	int		iEquipID;
	int		iSignalID;
}GETALLLSIGNAL;


static int Web_MakeFirstPage(char **ppBuf)
{
#define MAX_NETSCAPE_STATUS_STRUC_LEN			512

	int			i = 0, j = 0, iLen = 0, iError = 0, iVarSubID = 0;
	int			iSignalID = 0, iSignalType = 0,iUCType = 0,iEquipID = 0;
	/*get status signal structure*/	
	void		*pSigStruc = NULL;
	SIG_BASIC_VALUE* pSigValue;
	char		*pMBuf = NULL;
	int			iStatusSignalNum = 0;
	int			iStateNum = 0;
	int			iValueFormat = 0;
	char		szSendValue[16];
	int			iEnumValue;
	int iLanguage = Web_GetCurrentLangValue();

	//ASSERT(stStatusConfig);

	//TRACE("\nWeb_MakeFirstPage----------------------------------1");
	if(stStatusConfig != NULL && stStatusConfig->stNetscapeConfig != NULL)
	{
		iStatusSignalNum = stStatusConfig->iNetscapeNumber;
		//TRACE("\niStatusSignalNum = %d", iStatusSignalNum);

		pMBuf = NEW(char,  iStatusSignalNum * MAX_NETSCAPE_STATUS_STRUC_LEN);
		if(pMBuf == NULL)
		{
			return FALSE;
		}

		for( i = 0; i < iStatusSignalNum; i++)
		{
				//TRACE("\nWeb_MakeFirstPage----------------------------------5");
			iEquipID = stStatusConfig->stNetscapeConfig[i].iEquipID;
			iSignalID = stStatusConfig->stNetscapeConfig[i].iSignalID;
			iSignalType = stStatusConfig->stNetscapeConfig[i].iSignalType;
			iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

			//TRACE("iSignalID = %d iSignalType=%d",iSignalID,iSignalType);

			iError = 0;
			iError += DxiGetData(VAR_A_SIGNAL_VALUE,
				iEquipID,	
				iVarSubID,		
				&iBufLen,			
				(void *)&pSigValue,			
				0);
			iBufLen = 0;
			//TRACE("\niSignalID = %d iSignalType=%d",iSignalID,iSignalType);
			iError += DxiGetData(VAR_A_SIGNAL_INFO_STRU,
				iEquipID,			
				iVarSubID,		
				&iBufLen,			
				(void *)&pSigStruc,			
				iTimeOut);




			//TRACE("\nWeb_MakeFirstPage----------------------------------2");
			if(iError == ERR_DXI_OK)
			{
				memset(szSendValue,0x0, sizeof(szSendValue));
				iValueFormat =Web_TransferFormatToInt( ((SAMPLE_SIG_INFO *)pSigStruc)->szValueDisplayFmt);
				iUCType = pSigValue->ucType;

				if( iUCType == VAR_FLOAT)
				{
					sprintf(szSendValue, "%.*f",
						iValueFormat,
						pSigValue->varValue.fValue);
				}
				else if(iUCType == VAR_LONG)
				{
					sprintf(szSendValue, "%8ld", pSigValue->varValue.lValue);
				}
				else if(iUCType == VAR_UNSIGNED_LONG)
				{
					sprintf(szSendValue, "%8lu", pSigValue->varValue.ulValue);
				}
				else if(iUCType == VAR_ENUM)
				{
					iEnumValue = pSigValue->varValue.enumValue;
				}
				else
				{
					sprintf(szSendValue, "%s", " ");
				}


				//TRACE("\nWeb_MakeFirstPage----------------------------------3");

				if(iSignalType == SIG_TYPE_SAMPLING)
				{
					//	TRACE("\nWeb_MakeFirstPage----------------------------------6");
					iLen += sprintf(pMBuf + iLen, "<TR><TD><B> %s:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</B></TD><TD></TD><TD BGCOLOR=\"#CCCCCC\" ALIGN=\"RIGHT\"><B>%s&nbsp;%s</B></TD></TR>\n",
						((SAMPLE_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],
						(iUCType == VAR_ENUM)?((SAMPLE_SIG_INFO *)pSigStruc)->pStateText[iEnumValue]->pAbbrName[iLanguage]:szSendValue,
						((SAMPLE_SIG_INFO *)pSigStruc)->szSigUnit);
					//TRACE("\nWeb_MakeFirstPage----------------------------------16");



				}
				else if(iSignalType == SIG_TYPE_SETTING)
				{
					//TRACE("\nWeb_MakeFirstPage----------------------------------7");
					//printf("\nxxx");
					//printf("%s", ((SET_SIG_INFO *)pSigStruc)->pStateText[iEnumValue]->pAbbrName[0]);
					iLen += sprintf(pMBuf + iLen, "<TR><TD><b> %s:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></TD><TD></TD><TD BGCOLOR=\"#CCCCCC\" ALIGN=\"RIGHT\"><b>%s&nbsp;%s</b></TD></TR>",
						((SET_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],
						(iUCType == VAR_ENUM)?(((SET_SIG_INFO *)pSigStruc)->pStateText[iEnumValue]->pAbbrName[iLanguage]):szSendValue,
						((SET_SIG_INFO *)pSigStruc)->szSigUnit);

				}
				else if(iSignalType ==SIG_TYPE_CONTROL)
				{
					//TRACE("\nWeb_MakeFirstPage----------------------------------8");
					iLen += sprintf(pMBuf + iLen, "<TR><TD><b> %s:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></TD><TD></TD><TD BGCOLOR=\"#CCCCCC\" ALIGN=\"RIGHT\"><b>%s&nbsp;%s</b></TD></TR>",
						((CTRL_SIG_INFO *)pSigStruc)->pSigName->pAbbrName[iLanguage],
						(iUCType == VAR_ENUM)?((CTRL_SIG_INFO *)pSigStruc)->pStateText[iEnumValue]->pAbbrName[iLanguage]:szSendValue,
						((CTRL_SIG_INFO *)pSigStruc)->szSigUnit	);


				}

			}
			else
			{
				return FALSE;
			}
		}
	}
	else
	{
		return FALSE;
	}

	pMBuf[iLen - 1] = 32;

	*ppBuf = pMBuf;


	return iLen;

}
static time_t Web_Net_transferCharToTime(IN char *pTime)
{
	ASSERT(pTime);
	struct tm	tmReturn;
	char		*pGetObject = Cfg_RemoveWhiteSpace(pTime);

	if(pGetObject == NULL)
	{
		return FALSE;
	}

	/*pTime example:"2004-12-12 06:00:00"*/
	int nYear = 1971,nMonth,nDay,nHour;
	//printf("pGetObject:%s", pGetObafject);
	sscanf(pGetObject,"%d-%d %d", &nMonth,&nDay,&nHour);

	//printf("\n get value:{%d}{%d}{%d}{%d}{%d}",nYear,nMonth,nDay,nHour,nMinute,nSeconds);
	tmReturn.tm_year	= nYear - TIME_LINUX_YEAR;
	tmReturn.tm_mon		= nMonth - TIME_LINUX_MONTH;
	tmReturn.tm_mday	 = nDay;

	tmReturn.tm_sec		= 30;//nSeconds;
	tmReturn.tm_min		= 7;//nMinute;
	tmReturn.tm_hour	= nHour;


	tmReturn.tm_wday	= 0;
	tmReturn.tm_yday	= 0;
	tmReturn.tm_isdst	= -1;

	return mktime(&tmReturn);
}


static void fnProcessCmdThread(void)
{

	HANDLE			hSelf = RunThread_GetId(NULL);

	int				fd;
	int				iLen;
	char			buf[4096];   // fifo read buffer ,get parameter from CGI
	char			filename[FIFO_NAME_LEN];  //fifo write buffer,init according to the CGI process
	mode_t			mode = 0777;  //open fifo mode ,
	char			*ptr = NULL;
	char			szPid[MAX_COMM_PID_LEN +1];//szPid[8];
	char			szControlType[8];
	int				iControltype;
	BOOL			bContinueRunning = TRUE;
	int				iReturnError;
	char			szCommand[128];

	//create fifo that all process can access
	if((mkfifo(MAIN_FIFO_NAME,mode))<0)
	{
#ifdef _DEBUG
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Error : Fail to  make main fifo [%s]", MAIN_FIFO_NAME);
#endif
		return FALSE;
	}

	sprintf(szCommand,"chmod 777  %s", MAIN_FIFO_NAME);
	system(szCommand);

	if((fd = open(MAIN_FIFO_NAME,O_RDONLY | O_NONBLOCK))<0)
	{
#ifdef _DEBUG
		AppLogOut(CGI_APP_LOG_COMM_NAME, APP_LOG_WARNING, "Error : Fail to open main FIFO [%s]", MAIN_FIFO_NAME);
#endif 
		return FALSE;
	}

	while (THREAD_IS_RUNNING(hSelf))
	{
		/*tell main web_ui_comm.so living*/
		while ((WaitFiledReadable(fd, 1000) > 0) && 
			(iLen = read(fd,buf,PIPE_BUF)) > 0)
		{

#ifdef AUTO_CONFIG
			if(iAutoCFGStartFlage == 1)
			{
				continue;
			}
#endif

			if(iSlaveMode == 1)
			{
				continue;
			}
			buf[iLen] = '\0';
			//printf("\n********************%s", buf);
#ifdef _SHOW_WEB_INFO
			//TRACE("GetBuffer: buf %s \n iLen : %d \n",buf, iLen);
#endif	

			//0-9 in buf is pid ,get the fifo name to write data .
			ptr = buf ;
			strncpyz(szPid,ptr, MAX_COMM_PID_LEN +1);

			if (atoi(szPid) > 0)
			{
				sprintf(filename,"%s/fifo.%-d",CGI_CLIENT_FIFO_PATH,atoi(szPid));

				ptr = ptr + MAX_COMM_PID_LEN ;

				//10-11 in buf is control type
				strncpyz(szControlType,ptr,MAX_COMM_GET_TYPE + 1);
				iControltype = atoi(szControlType);
				ptr = ptr + MAX_COMM_GET_TYPE ;


				switch(iControltype)
				{
				case LOG_IN:
					{

						iReturnError = Web_SendEquipList(buf, filename);

						break;
					}
				case REALTIME_DATA:
					{
						iReturnError = Web_SendRealtimeData(buf,filename);
						break;
					}
				case CONTROL_COMMAND:
					{
						iReturnError = Web_SendControlCommandResult(buf,filename);
						break;
					}
				case HISTORY_QUERY:
					{
						iReturnError = Web_SendQueryData(buf,filename);
						break;
					}
				case CONFIGURE_MANAGE:
					{
						////TRACE("Web_SendConfigureData \n");
						iReturnError = Web_SendConfigureData(buf,filename);
						break;
					}
				case FILE_MANAGE:
					{
						CloseACU();

						break;
					}
				case FILE_MANAGE_REPLACEFILE:
					{
						Web_ReplaceFile(buf);
						break;
					}
#ifdef ADD_SUPPORT_NETSCAPE				
				case WEB_SUPPORT_NETSCAPE:
					{
						//								iReturnError = Web_SendNetscapeCommand(buf,filename);
						TRACE("\n-----WEB_SUPPORT_NETSCAPE----\n");
						iReturnError = Web_StateMgmt_Netscape(buf,filename);
						break;
					}

				case HISTORY_QUERY_NETSCAPE:
					{
						TRACE("-----------in netscape-----------\n");
						iReturnError = Web_SendQueryData_Netscape(buf,filename);
						break;
					}
#endif
				default:

					break;

				}
			}


		}

		RunThread_Heartbeat(RunThread_GetId(NULL));


	}
}


#define MAX_ALARM_DATA_LEN_NETSCAPE		600
/*==========================================================================*
* FUNCTION :    Web_MakeQueryAlarmDataBuffer_Netscape
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iEquipID:
IN time_t fromTime:
IN time_t toTime:
IN int iLanguage:
IN char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wankun               DATE: 2007-03-03 11:20
*==========================================================================*/
static int Web_MakeQueryAlarmDataBuffer_Netscape(IN int iEquipID, 
						 IN time_t fromTime, 
						 IN time_t toTime, 
						 IN int iLanguage, 
						 IN char **ppBuf)
{
	int							iLen = 0, iFileLen = 0;
	HIS_ALARM_RECORD			*pReturnData		= NULL;
	HIS_ALARM_TO_RETURN			*pTraiReturnData	= NULL;	
	char						*pMakeBuf			= NULL, *pMakeFileBuf = NULL;
	int							iDataLen = 0;
	int							iBufLen = 0;
	void						*pDeletePtr = NULL;
	char						szTime1[32], szTime2[32];
	char						*szLevelName[] = {"NA", "OA", "MA", "CA"};
	int							nAlarms;
	int							iIndex = 1;

	TRACE("-----------in netscape--3-----------\n");
	/*Query alarm*/
	if(( iBufLen = Web_QueryHisAlarm(fromTime, toTime, iEquipID, (void *)&pReturnData)) <= 0)
	{
		TRACE("-----------in netscape--8-----------\n"); 
		if(pReturnData != NULL)
		{
			DELETE(pReturnData);
			pReturnData = NULL;
		}
		//display the content of no record situation
		iDataLen = 120;
		pMakeBuf = NEW(char, iDataLen);
		if(pMakeBuf == NULL)
		{
			DELETE(pReturnData);
			pReturnData = NULL;
			return FALSE;
		}
		memset(pMakeBuf, 0, (size_t)iDataLen);
		strcpy(pMakeBuf,"<TR ALIGN = CENTER><TD BGCOLOR=#CCCCCC colspan=\"6\"><script language=\"javascript\">showMsg()</script></TD></TR>");
		TRACE("-------------pMakeBuf is %s----------------\n",pMakeBuf);
		*ppBuf = pMakeBuf;
		return TRUE;
	}
#ifdef _SHOW_WEB_INFO
	TRACE("iBufLen: [%d]\n", iBufLen);
#endif 
	TRACE("iBufLen: [%d]\n", iBufLen);

	iDataLen = iBufLen * MAX_ALARM_DATA_LEN_NETSCAPE;
	pMakeBuf = NEW(char, iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeBuf, 0, (size_t)iDataLen);

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);

	/*Transfer the match language*/
	if(Web_TransferAlarmDataIDToName((void *)pReturnData, iBufLen, iLanguage,(void *)&pTraiReturnData) == FALSE)
	{
		TRACE("Web_TransferAlarmDataIDToName : FALSE");

		DELETE(pReturnData);
		pReturnData = NULL;

		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		return FALSE;
	}

	DELETE(pReturnData);
	pReturnData = NULL;


	nAlarms = 0;
	pDeletePtr = pTraiReturnData;

	//sort
	Web_SortHisAlarmData(pTraiReturnData, iBufLen);
	TRACE("iBufLen: [%d]\n", iBufLen);
	while(pTraiReturnData != NULL && iBufLen > 0)
	{
		TimeToString(pTraiReturnData->tmStartTime, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));
		TimeToString(pTraiReturnData->tmEndTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));
		iLen += sprintf(pMakeBuf + iLen,"<TR ALIGN=CENTER><TD BGCOLOR=#FF0000>%s</TD><TD BGCOLOR=#CCCCCC>%d</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD></TR>\n",
			szLevelName[pTraiReturnData->byLevel],
			iIndex,
			pTraiReturnData->szAlarmName,
			//pTraiReturnData->fTriggerValue,
			szTime1,
			szTime2,
			pTraiReturnData->szEquipName); 

		// maofuhua changed the format. 2005-3-15
		nAlarms++;
		// removed the value column.
		iFileLen +=sprintf(pMakeFileBuf + iFileLen,"%5d \t%-32s \t%-32s \t%-5s \t%-15s \t%-15s\n",
			nAlarms,
			pTraiReturnData->szEquipName,
			pTraiReturnData->szAlarmName,
			szLevelName[pTraiReturnData->byLevel],
			//pTraiReturnData->fTriggerValue,
			szTime1,
			szTime2);

		pTraiReturnData++;
		iBufLen--;
		iIndex++;
	}

	char		*pSearchValue = NULL;

	pSearchValue = strrchr(pMakeBuf, 44);  //get the last ','
	if(pSearchValue != NULL)
	{
		*pSearchValue = 32;   //replace ',' with ' '
	}

	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	TRACE("********[Web_MakeQueryAlarmDataBuffer]******%s\n", pMakeBuf);
#endif 
	TRACE("********[Web_MakeQueryAlarmDataBuffer]******%s\n", pMakeBuf);

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL && iFileLen > 1)
	{

		char	szFileTitle[512];

		TimeToString(fromTime, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));
		TimeToString(toTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		// removed the value column.
		sprintf(szFileTitle, "Query ACU Alarm Data\nQuery EquipID: %s\n" \
			"Query time: from %s to %s\n"	\
			"Total %d alarm(s) queried.\n\n" \
			"\t%5s \t%-32s \t%-32s \t%s \t%-15s \t%-15s\n",
			(iEquipID > 0) ? Web_GetAEquipName(iEquipID, iLanguage) : "All device",
			szTime1,//ctime(&fromTime),
			szTime2,//ctime(&toTime),
			nAlarms,
			"Index",
			"Device name",
			"Signal name",
			"Level",
			//"Value",
			"Start time",
			"End time");

		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);

		char szCommandStr[128];
		sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
		system(szCommandStr);


	}

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;

	DELETE(pDeletePtr);
	pDeletePtr = NULL;

	//fclose(fp);

	return TRUE;
}



#define MAX_HIS_DATA_LEN_NETSCAPE		600
/*==========================================================================*
* FUNCTION :    Web_MakeQueryHisDataBuffer_Netscape
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iEquipID:
IN time_t fromTime:
IN time_t toTime:
IN int iLanguage:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin and changed by Wankun        DATE: 2007-03-06 11:20
*==========================================================================*/
static int Web_MakeQueryHisDataBuffer_Netscape(IN int iEquipID, 
					       IN time_t fromTime, 
					       IN time_t toTime, 
					       IN int iLanguage, 
					       OUT char **ppBuf)
{
#ifndef NS_AP_HISDATA
	int						iLen			= 0, iFileLen = 0;
	HIS_DATA_RECORD			*pReturnData	= NULL;
	HIS_DATA_TO_RETURN		*pTraiReturnData= NULL;
	HIS_DATA_TO_RETURN		*pDeletePtr		= NULL;
	char					*pMakeBuf		= NULL, *pMakeFileBuf = NULL;
	int						iBufLen			= 0;
	int						iDataLen		= 0;
	char					szTime[32], szTime2[32];

	if((iBufLen = Web_QueryHisData(toTime,fromTime, iEquipID, 
		QUERY_HIS_DATA, (void *)&pReturnData)) <= 0 )
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_MakeQueryHisDataBuffer :iBufLen : %d\n", iBufLen);
#endif 
		DELETE(pReturnData);
		pReturnData = NULL;
		//display the content of no record situation
		iDataLen = 120;
		pMakeBuf = NEW(char, iDataLen);
		if(pMakeBuf == NULL)
		{
			DELETE(pReturnData);
			pReturnData = NULL;
			return FALSE;
		}
		memset(pMakeBuf, 0, (size_t)iDataLen);

		sprintf(pMakeBuf,"%2d<TR ALIGN = CENTER><TD BGCOLOR=#CCCCCC colspan=\"6\"><script language=\"javascript\">showMsg()</script></TD></TR>",1);
		TRACE("-------------pMakeBuf is %s----------------\n",pMakeBuf);
		*ppBuf = pMakeBuf;
		return TRUE;
	}
	TRACE("Web_MakeQueryHisDataBuffer :iBufLen : %d----------------\n", iBufLen);
	iDataLen = iBufLen * MAX_HIS_DATA_LEN_NETSCAPE;

	pMakeBuf = NEW(char,iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeBuf, 0x0, (size_t)iDataLen);

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);


	if(Web_TransferHisDataIDToName(pReturnData,iBufLen, iLanguage,
		(void *)&pTraiReturnData) == FALSE)
	{
#ifdef _SHOW_WEB_INFO
		//TRACE("Web_TransferHisDataIDToName :FALSE\n");
#endif 
		DELETE(pReturnData);
		pReturnData = NULL;

		DELETE(pMakeBuf);
		pMakeBuf = NULL;

		return FALSE;
	}

	DELETE(pReturnData);
	pReturnData = NULL;

	pDeletePtr = pTraiReturnData;

	iLen += sprintf(pMakeBuf + iLen,"%2d", 1);
	int i = 1;
	TRACE("i am here!\n");
	while(pTraiReturnData != NULL && iBufLen > 0)
	{

		TimeToString(pTraiReturnData->tSignalTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		iLen += sprintf(pMakeBuf + iLen, "<TR ALIGN=CENTER><TD BGCOLOR=#CCCCCC>%d</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD></TR>\n", 
			i,
			pTraiReturnData->szEquipName, 
			pTraiReturnData->szSignalName,
			pTraiReturnData->szSignalVal, 
			pTraiReturnData->szUnit,
			szTime);//,szTime,
		//pTraiReturnData->dwActAlarmID );

		iFileLen += sprintf(pMakeFileBuf + iFileLen, "\t%d \t%-32s \t%-32s  \t%-32s \t%-8s \t%-32s\t\n", 
			i,
			pTraiReturnData->szEquipName, 
			pTraiReturnData->szSignalName,
			pTraiReturnData->szSignalVal, 
			pTraiReturnData->szUnit,
			szTime);

		pTraiReturnData++;
		iBufLen--;
		i++;
	}
	/*    char		*pSearchValue = NULL;
	TRACE("i am here2!\n");
	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
	*pSearchValue = 32;   //replace ',' with ' '
	TRACE("i am here3!\n");*/
	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryHisDataBuffer]******%s\n", pMakeBuf);
#endif 

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL && iFileLen > 1)
	{
		char	szFileTitle[512];

		TimeToString(fromTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "Query history data\nQuery EquipID:[%s] \nQuery time:from %s to %s\nTotal %d record(s) queried\n%8s \t%-32s \t%-32s \t%-32s \t%-8s \t%-32s \t\n",
			(iEquipID > 0) ? Web_GetAEquipName(iEquipID, iLanguage) : "All device",
			szTime,
			szTime2,
			i-1,
			"Index",
			"Device name", 
			"Signal name", 
			"Value", 
			"Unit",
			"Time");

		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);

		char szCommandStr[128];
		sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
		system(szCommandStr);


	}

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;

	DELETE(pDeletePtr);
	pDeletePtr = NULL;
	return TRUE;
#endif
}



#define MAX_CONTROL_DATA_LEN_NETSCAPE	600
/*==========================================================================*
* FUNCTION :    Web_MakeQueryControlDataBuffer_Netscape
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN time_t fromTime:
IN int toTime:
IN int iLanguage:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Yang Guoxin and changed by Wankun              DATE: 2007-03-07 11:20
*==========================================================================*/
static int Web_MakeQueryControlDataBuffer_Netscape(IN time_t fromTime , 
						   IN time_t toTime, 
						   IN int iLanguage, 
						   OUT char **ppBuf)
{
	int							iLen = 0, iFileLen = 0;
	HIS_CONTROL_RECORD			*pReturnData		= NULL;
	HIS_CONTROL_TO_RETURN		*pTraiReturnData	= NULL;
	char						*pMakeBuf			= NULL, *pMakeFileBuf = NULL;
	int							iBufLen = 0;
	int							iDataLen = 0;
	HIS_CONTROL_TO_RETURN		*pDeletePtr = NULL;
	char						szTime[32],szTime2[32];


	if((iBufLen = Web_QueryControlCommand(fromTime, toTime, 
		(void **)&pReturnData)) <= 0)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		//display the content of no record situation
		iDataLen = 120;
		pMakeBuf = NEW(char, iDataLen);
		if(pMakeBuf == NULL)
		{
			DELETE(pReturnData);
			pReturnData = NULL;
			return FALSE;
		}
		memset(pMakeBuf, 0, (size_t)iDataLen);

		sprintf(pMakeBuf,"<TR ALIGN = CENTER><TD BGCOLOR=#CCCCCC colspan=\"9\"><script language=\"javascript\">showMsg()</script></TD></TR>");
		TRACE("-------------pMakeBuf is %s----------------\n",pMakeBuf);
		*ppBuf = pMakeBuf;
		return TRUE;
	}

	iDataLen = iBufLen * MAX_CONTROL_DATA_LEN_NETSCAPE;	
	pMakeBuf = NEW(char,iDataLen);
	if(pMakeBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		return FALSE;
	}

	memset(pMakeBuf, 0, (size_t)iDataLen);

	//Special buffer for make file 
	pMakeFileBuf = NEW(char, iDataLen);
	if(pMakeFileBuf == NULL)
	{
		DELETE(pReturnData);
		pReturnData = NULL;
		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		return FALSE;
	}
	memset(pMakeFileBuf, 0, (size_t)iDataLen);


	if(Web_TransferControlIDToName((void *)pReturnData, iBufLen, iLanguage, 
		(void *) &pTraiReturnData) == FALSE)
	{
		//TRACE("Web_TransferControlIDToName false\n");
		DELETE(pReturnData);
		pReturnData = NULL;
		DELETE(pMakeBuf);
		pMakeBuf = NULL;
		DELETE(pMakeFileBuf);
		pMakeFileBuf = NULL;
		return FALSE;
	}


	DELETE(pReturnData);
	pReturnData = NULL;

	pDeletePtr = pTraiReturnData;

	int		i = 1;
	while(pTraiReturnData != NULL && iBufLen > 0)
	{
		TimeToString(pTraiReturnData->tmControlTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		iLen += sprintf(pMakeBuf + iLen,"\t <TR ALIGN=CENTER><TD BGCOLOR=#CCCCCC>%d</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD></TR>\n",
			i,
			pTraiReturnData->szEquipName,
			pTraiReturnData->szSignalName,
			//pTraiReturnData->iValueType,
			pTraiReturnData->szControlVal,
			pTraiReturnData->szUnit,
			szTime,//pTraiReturnData->tmControlTime,
			pTraiReturnData->szCtrlSenderName,
			pTraiReturnData->szSenderType,
			(pTraiReturnData->iControlResult == 0) ? "Successfully" :"Failure");

		iFileLen += sprintf(pMakeFileBuf + iFileLen,"%8d \t%-32s \t%-32s \t%-32s \t%-8s \t%-32s \t%-32s \t%-32s \t%-16s \n",
			i,
			pTraiReturnData->szEquipName,
			pTraiReturnData->szSignalName,
			pTraiReturnData->szControlValForSave,
			pTraiReturnData->szUnit,
			szTime,
			pTraiReturnData->szCtrlSenderName,
			pTraiReturnData->szSenderType,
			(pTraiReturnData->iControlResult == 0) ? "Successfully" :"Failure");

		pTraiReturnData++;
		iBufLen--;
		i++;
	}

	/*
	char		*pSearchValue;

	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
	if(pSearchValue != NULL)
	{
	*pSearchValue = 32;   //replace ',' with ' '
	}*/
	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryControlDataBuffer]******%s\n", pMakeBuf);
#endif 


	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL && iFileLen > 1)
	{
		char	szFileTitle[512];
		TimeToString(fromTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "Query history control log\n" \
			"Query time:from %s to %s\nTotal %d record(s) queried\n " \
			"\n%8s \t%-32s \t%-32s \t%-32s \t%-8s \t%-32s \t%-32s \t%-32s \t%-16s\n",
			szTime,
			szTime2,
			i-1,
			"Index",
			"Device name", 
			"Signal name", 
			"Value", 
			"Unit",
			"Time",
			"Sender name",
			"Sender type",
			"Send result");

		fwrite(szFileTitle, strlen(szFileTitle),1, fp);
		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);

		char szCommandStr[128];
		sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
		system(szCommandStr);


	}

	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;


	DELETE(pDeletePtr);
	pDeletePtr = NULL;

	return TRUE;
}

#define MAX_DATA_LOG_LEN_NETSCAPE		800
/*==========================================================================*
* FUNCTION :    Web_MakeQueryHisLogBuffer_Netscape
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN time_t fromTime:
IN time_t toTime:
IN char **ppBuf:
* RETURN   :	TRUE or FALSE
* COMMENTS : 
* CREATOR  : Yang Guoxin and changed by wankun       DATE: 2007-03-07 11:20
*==========================================================================*/
static int Web_MakeQueryHisLogBuffer_Netscape(IN time_t fromTime,
					      IN time_t toTime,
					      IN char **ppBuf)
{
	int							iLen = 0,iFileLen = 0;
	HIS_MATCH_LOG				*pReturnData	= NULL;
	HIS_MATCH_LOG				*pDeletePtr		= NULL;
	char						*pMakeBuf		= NULL, *pMakeFileBuf = NULL;
	int							iMatchRecord = 0;
	char						szTime[32];
	int							i;
	int							j = 0;
	int							iDataLen = 0;


	/*Get Log*/
	if((iMatchRecord = Web_QueryHisLog(fromTime, toTime,(void *)&pReturnData)) <= 0)
	{
		TRACE("Web_QueryHisLog : FALSE");
		
		//display the content of no record situation
		iDataLen = 120;
		pMakeBuf = NEW(char, iDataLen);
		if(pMakeBuf == NULL)
		{
			return FALSE;
		}
		memset(pMakeBuf, 0, (size_t)iDataLen);

		sprintf(pMakeBuf,"%2d<TR ALIGN = CENTER><TD BGCOLOR=#CCCCCC colspan=\"5\"><script language=\"javascript\">showMsg()</script></TD></TR>",0);
		TRACE("-------------pMakeBuf is %s----------------\n",pMakeBuf);
		*ppBuf = pMakeBuf;
		return TRUE;

	}

	pMakeBuf = NEW(char,iMatchRecord * MAX_DATA_LOG_LEN_NETSCAPE);
	pMakeFileBuf = NEW(char, iMatchRecord * MAX_DATA_LOG_LEN);
	if(pMakeBuf == NULL || pMakeFileBuf == NULL)
	{
		if(pReturnData != NULL)
		{
			DELETE(pReturnData);
			pReturnData = NULL;
		}
		return FALSE;
	}

	pDeletePtr = pReturnData;

	iLen += sprintf(pMakeBuf + iLen,"%2d", 0);

	i = 1;
	while(pReturnData != NULL && iMatchRecord > 0 )
	{
		TimeToString(pReturnData->tmLogTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		if(iMatchRecord <= 500)
		{
			iLen += sprintf(pMakeBuf + iLen,"<TR ALIGN=CENTER><TD BGCOLOR=#CCCCCC>%d</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC>%s</TD><TD BGCOLOR=#CCCCCC ALIGN=LEFT>%s</TD></TR>\n",
			i,
			pReturnData->szTaskName,
			pReturnData->szInfoLevel,
			szTime,//(int)pReturnData->tmLogTime, 
			Cfg_RemoveWhiteSpace(pReturnData->szInformation));
			i++;

		}
		

		iFileLen += sprintf(pMakeFileBuf + iFileLen,"%-16s \t%-8s \t%32s \t%s \n",
			pReturnData->szTaskName,
			pReturnData->szInfoLevel,
			szTime, 
			Cfg_RemoveWhiteSpace(pReturnData->szInformation));

		iMatchRecord--;
		pReturnData++;
		j++;
		
	}
	/*
	char		*pSearchValue = NULL;

	pSearchValue = strrchr(pMakeBuf,44);  //get the last ','
	if(pSearchValue != NULL)
	{
	*pSearchValue = 32;   //replace ',' with ' '
	}*/

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL)
	{
		char	szFileTitle[512];
		char	szTime[32],szTime2[32];

		TimeToString(fromTime, TIME_CHN_FMT, 
			szTime, sizeof(szTime));

		TimeToString(toTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		sprintf(szFileTitle, "Query system log\nQuery time:from %s to %s\nTotal %d record(s) queried\n\n",
			szTime,
			szTime2,
			j-1);

		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);

		fwrite(pMakeFileBuf,strlen(pMakeFileBuf), 1, fp);
		fclose(fp);
		char szCommandStr[128];
		sprintf(szCommandStr, "chmod 777 %s", WEB_LOG_DIR);
		system(szCommandStr);
	}

	*ppBuf = pMakeBuf;
#ifdef _SHOW_WEB_INFO
	//TRACE("********[Web_MakeQueryHisLogBuffer]**[%s]****\n", pMakeBuf);
#endif 

	//delete the pMakeFileBuf to avoid mem leak
	DELETE(pMakeFileBuf);
	pMakeFileBuf = NULL;

	DELETE(pDeletePtr);
	pDeletePtr = NULL;

	return TRUE;
}

/*==========================================================================*
* FUNCTION :    Web_MakeQueryBatteryTestSumBuffer_Netscape
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   IN int iQueryTimes:
OUT char **ppBuf:
* RETURN   :  
* COMMENTS : 
* CREATOR  : Wankun              DATE: 2007-03-13 11:20
*==========================================================================*/
static int Web_MakeQueryBatteryTestSumBuffer_Netscape(IN int iQueryTimes, 
						      OUT char **ppBuf)
{
	BATTERY_LOG_INFO		*pBatteryLogInfo	= NULL;
	BATTERY_HEAD_INFO		*stGeneralInfo = NULL;
	char					*pMakeBuf = NULL, *pFileMakeBuf = NULL;
	int						iLen = 0, iFileLen = 0;
	int						iQtyBatt, iQtyRecord;
	int						i = 0, k = 0, j = 0;
	char					szTime1[32], szTime2[32];
	int						iBatteryNum = 0;
	time_t					tmUTC;
	BATTERY_GENERAL_INFO			stOutBatteryGeneralInfo;
	int					iStatus[200];
	int						iQueTime = 0;

	char					szBatteryStartReason[5][128]={"Start test for it is battery best plan time",
		"Start test manually",
		"Start AC Fail test for AC fail",
		"Start test for Master Power start test",
		"Other reason"};
	char					szBatteryEndReason[12][128]={"Stop test manually",
		"Stop test for alarm",
		"Stop test for test time-out",
		"Stop test for capacity condition",
		"Stop test for voltage condition",
		"Stop test for AC fail",
		"Stop AC fail test for AC restore",
		"Stop AC fail test for disabled",
		"Stop test for Master Power stop test",
		"Stop a PowerSplit BT for Auto/Man turn to Man",
		"Stop PowerSplit Man-BT for Auto/Man turn to Auto",
		"Stop by other reason"};
	char					szBatteryTestResult[5][64]={"No test result",
		"Battery is OK",
		"Battery is bad",
		"It's a Power Split Test", 
		"Other result"};


	if(iQueryTimes == 0)
	{
		//in order to display all summary info , add one variable to indicate the times of test
		iQueTime = 1;
		pFileMakeBuf = NEW(char, 10 * MAX_QUERY_BATTERY_GROUP_LEN );
		if( pFileMakeBuf == NULL )
		{
			return FALSE;
		}
		while ( Web_QueryBatteryTestLog(iQueryTimes, &stGeneralInfo, &pBatteryLogInfo, &stOutBatteryGeneralInfo) == TRUE )
		{
			TimeToString(stGeneralInfo->btSummary.tStartTime, TIME_CHN_FMT, 
				szTime1, sizeof(szTime1));

			TimeToString(stGeneralInfo->btSummary.tEndTime, TIME_CHN_FMT, 
				szTime2, sizeof(szTime2));

			//in order to add 0 at the first time and after then no 0 display!
			if ( iQueTime ==1 )
				iLen += sprintf(pFileMakeBuf + iLen,"%2d<TABLE width=500 border=1><TBODY><TR><TD><B>Battery Test&nbsp;-%d- <B><FONT size=-1><A href=\"/cgi-bin2/web_cgi_query_netscape.cgi?_data_type=5&equipID=%d\">View log</A></FONT></B><TABLE width=\"100%\"><TBODY><TR><TD><B><FONT size=-1>Test start</FONT></B></TD><TD><B><FONT size=-1>Test stop</FONT></B></TD><TD><B><FONT size=-1>Start Reason</FONT></B></TD><TD><B><FONT size=-1>Stop Reason</FONT></B></TD><TD><B><FONT size=-1>Test Result</FONT></B></TD><TR bgColor=#c0c0c0><TD><B><FONT size=-1>%s</FONT></B></TD><TD><B><FONT size=-1>%s</FONT></B></TD><TD><B><FONT size=-1>%s</FONT></B></TD><TD><B><FONT size=-1>%s</FONT></B></TD><TD><B><FONT size=-1>%s</FONT></B></TD></TR></TBODY></TABLE></B></TD></TR></TBODY></TABLE><BR>\n",
				0,
				iQueTime,
				iQueTime,
				szTime1,//(int)stGeneralInfo->btSummary.tStartTime,
				szTime2,//(int)stGeneralInfo->btSummary.tEndTime,
				szBatteryStartReason[stGeneralInfo->btSummary.iStartReason],
				szBatteryEndReason[stGeneralInfo->btSummary.iEndReason],
				szBatteryTestResult[stGeneralInfo->btSummary.iTestResult]);
			else
				iLen += sprintf(pFileMakeBuf + iLen,"<TABLE width=500 border=1><TBODY><TR><TD><B>Battery Test&nbsp;-%d- <B><FONT size=-1><A href=\"/cgi-bin2/web_cgi_query_netscape.cgi?_data_type=5&equipID=%d\">View log</A></FONT></B><TABLE width=\"100%\"><TBODY><TR><TD><B><FONT size=-1>Test start</FONT></B></TD><TD><B><FONT size=-1>Test stop</FONT></B></TD><TD><B><FONT size=-1>Start Reason</FONT></B></TD><TD><B><FONT size=-1>Stop Reason</FONT></B></TD><TD><B><FONT size=-1>Test Result</FONT></B></TD><TR bgColor=#c0c0c0><TD><B><FONT size=-1>%s</FONT></B></TD><TD><B><FONT size=-1>%s</FONT></B></TD><TD><B><FONT size=-1>%s</FONT></B></TD><TD><B><FONT size=-1>%s</FONT></B></TD><TD><B><FONT size=-1>%s</FONT></B></TD></TR></TBODY></TABLE></B></TD></TR></TBODY></TABLE><BR>\n",
				iQueTime,
				iQueTime,
				szTime1,//(int)stGeneralInfo->btSummary.tStartTime,
				szTime2,//(int)stGeneralInfo->btSummary.tEndTime,
				szBatteryStartReason[stGeneralInfo->btSummary.iStartReason],
				szBatteryEndReason[stGeneralInfo->btSummary.iEndReason],
				szBatteryTestResult[stGeneralInfo->btSummary.iTestResult]);
			iQueTime++;

			//free memory from Web_QueryBatteryTestLog-----to avoid memory leak!
			iQtyRecord	= stGeneralInfo->btSummary.iQtyRecord;

			if(pBatteryLogInfo != NULL)
			{
				
				DELETE(pBatteryLogInfo);
						
					
			}
		}


		//display no test record html page on sumary page!
		if(iQueTime == 1)
		{

			if(pBatteryLogInfo != NULL)
			{
				DELETE(pBatteryLogInfo);
				pBatteryLogInfo = NULL;
			}

			if(pFileMakeBuf != NULL )
			{
				DELETE(pFileMakeBuf);
				pFileMakeBuf = NULL;
			}
			//display the content of no record situation
			int iDataLen = 150;
			pMakeBuf = NEW(char, iDataLen);
			if(pMakeBuf == NULL)
			{
				return FALSE;
			}
			memset(pMakeBuf, 0, (size_t)iDataLen);

			sprintf(pMakeBuf,"%2d<TABLE width=500 border=1><TR ALIGN = CENTER><TD BGCOLOR=#CCCCCC><script language=\"javascript\">showMsg()</script></TD></TR></TABLE>",0);
			TRACE("-------------pMakeBuf is %s----------------\n",pMakeBuf);
			*ppBuf = pMakeBuf;
			return TRUE;
		}
		*ppBuf = pFileMakeBuf;
		return TRUE;
	}
	else
	{
		if(Web_QueryBatteryTestLog(iQueryTimes, &stGeneralInfo, &pBatteryLogInfo, &stOutBatteryGeneralInfo) != TRUE)//ERR_WEB_COMM_GET_RESULT_FAIL)
		{
			return FALSE;
		}

		iQtyBatt	= stGeneralInfo->btSummary.iQtyBatt;
		iQtyRecord	= stGeneralInfo->btSummary.iQtyRecord;

		pMakeBuf = NEW(char, (100 * MAX_QUERY_BATTERY_GROUP_LEN * MAX_QUERY_BATTERY_GROUP_NUM)*2);
		pFileMakeBuf = NEW(char, 100 * MAX_QUERY_BATTERY_GROUP_LEN * MAX_QUERY_BATTERY_GROUP_NUM);
		if(pMakeBuf == NULL || pFileMakeBuf == NULL)
		{

			if(pBatteryLogInfo != NULL)
			{
				

				DELETE(pBatteryLogInfo);
				pBatteryLogInfo=NULL;
						
			}
			if(pMakeBuf != NULL)
			{
				DELETE(pMakeBuf);
				pMakeBuf = NULL;
			}
			if(pFileMakeBuf != NULL)
			{
				DELETE(pFileMakeBuf);
				pFileMakeBuf = NULL;
			}
			return FALSE;
		}

		for(i = 0; i < iQtyBatt; i++)
		{
			iLen += sprintf(pMakeBuf + iLen," %d,%d,%d,",(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCurrent == 1) ? 1 : 0, (stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryVolage == 1) ? 1 : 0 ,(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCapacity== 1) ? 1 : 0);
			iStatus[i*3 + 0] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCurrent == 1) ? 1 : 0;
			iStatus[i*3 + 1] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryVolage == 1) ? 1 : 0;
			iStatus[i*3 + 2] =(stOutBatteryGeneralInfo.BatteryInfo[i].byBatteryCapacity == 1) ? 1 : 0;

		}
		for(i = 0; i < 32; i++)
		{
			iLen += sprintf(pMakeBuf + iLen,"%d,",(stOutBatteryGeneralInfo.stBatteryBlock[i/8].byBlockVoltage[i%4]== 1) ? 1 : 0);
			iStatus[46*3 + i] =(stOutBatteryGeneralInfo.stBatteryBlock[i/8].byBlockVoltage[i%4]== 1) ? 1 : 0;

		}
		for(i = 0; i < 7; i++)
		{
			if(i == 6)
			{
				iLen += sprintf(pMakeBuf + iLen,"%d;",(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0 );
			}
			else
			{
				iLen += sprintf(pMakeBuf + iLen,"%d,",(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0 );
			}
			iStatus[46*3 + 32 + i] =(stOutBatteryGeneralInfo.byTemperatureInfo[i] == 1) ? 1 : 0  ;

		}



		TimeToString(stGeneralInfo->btSummary.tStartTime, TIME_CHN_FMT,	szTime1, sizeof(szTime1));

		TimeToString(stGeneralInfo->btSummary.tEndTime, TIME_CHN_FMT,szTime2, sizeof(szTime2));

		iLen += sprintf(pMakeBuf + iLen,"%2d<TABLE><TBODY><TR><TD><B>Battery test log No: </B></TD><TD bgColor=#cccccc>%d</TD><TD><a id=\"linkFile\" href=\"/var/download.txt\" onclick=\"download(this)\">Right Click to Save</a></TD><TD></TD></TR><TR><TD><B>Test start: </B></TD><TD bgColor=#cccccc>%s</TD><TD><B>Start reason:</B></TD><TD bgColor=#cccccc>%s</TD></TR><TR><TD><B>Test stop:</B></TD><TD bgColor=#cccccc>%s</TD><TD><B>Stop reason:</B></TD><TD bgColor=#cccccc>%s</TD></TR><TR><TD><B>Test result:</B></TD><TD bgColor=#cccccc>%s</TD></TR></TBODY></TABLE><BR><BR>;",
			0,
			iQueryTimes,
			szTime1,//(int)stGeneralInfo->btSummary.tStartTime,
			szBatteryStartReason[stGeneralInfo->btSummary.iStartReason],
			szTime2,//(int)stGeneralInfo->btSummary.tEndTime,
			szBatteryEndReason[stGeneralInfo->btSummary.iEndReason],
			szBatteryTestResult[stGeneralInfo->btSummary.iTestResult]);


		/*iFileLen += sprintf(pFileMakeBuf + iFileLen,"\t %d \t%d \t%d \t%d \t%d\n",
		(int)stGeneralInfo->btSummary.tStartTime,//szTime1,
		(int)stGeneralInfo->btSummary.tEndTime,//szTime2,
		stGeneralInfo->btSummary.iStartReason,
		stGeneralInfo->btSummary.iEndReason,
		stGeneralInfo->btSummary.iTestResult);*/

		iLen += sprintf(pMakeBuf + iLen, "<TR><TD>%s</TD><TD>%s</TD><TD>%s</TD> ","Index","Time","System Voltage");
		i = 0;						
		while(i < 177)
		{

			if(iStatus[i])
			{
				iLen += sprintf(pMakeBuf + iLen, "<TD>%-20s</TD>", szBatterTitle[i]);
				i++;
			}
			else
			{
				i++;
			}
		}						
		sprintf(pMakeBuf + iLen, "</TR>", "\n");


		//iLen += sprintf(pMakeBuf + iLen, "");
		for(i = 0; i < iQtyRecord - 1; i++)
		{
			/*print the common information of every battery test record*/
			tmUTC = pBatteryLogInfo[i].tRecordTime;

			TimeToString(tmUTC, TIME_CHN_FMT, 
				szTime1, sizeof(szTime1));


			iLen += sprintf(pMakeBuf + iLen, "<tr><td rowspan='%d'>%d</td><td rowspan='%d'>\"+ %s+\"</td> <td rowspan='%d'>%8.2f</td>", 1, (i + 1), 1, szTime1, 1, pBatteryLogInfo[i].fVoltage);
			iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t%6d \t%-32s \t%-8.2f ",  (i + 1), szTime1, pBatteryLogInfo[i].fVoltage);

			iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t\t%s"," ");
			for(k = 0; k < iQtyBatt; k++)
			{


				if(stOutBatteryGeneralInfo.BatteryInfo[k].byBatteryCurrent)
				{
					iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatCurrent);
					iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.2f \t",	
						pBatteryLogInfo[i].pBatteryRealData[k].fBatCurrent);


				}
				if(stOutBatteryGeneralInfo.BatteryInfo[k].byBatteryVolage)
				{
					iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatVoltage);
					iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.2f \t",	
						pBatteryLogInfo[i].pBatteryRealData[k].fBatVoltage);

				}

				if(stOutBatteryGeneralInfo.BatteryInfo[k].byBatteryCapacity)
				{
					iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td> ",pBatteryLogInfo[i].pBatteryRealData[k].fBatCapacity);
					iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.2f \t",	
						pBatteryLogInfo[i].pBatteryRealData[k].fBatCapacity);		
				}			

			}

			for(j = 0; j < 32; j++)
			{
				if(stOutBatteryGeneralInfo.stBatteryBlock[j/8].byBlockVoltage[j%4])
				{
					iLen += sprintf(pMakeBuf + iLen, "<td>%8.2f</td>", pBatteryLogInfo[i].fBatBlock[j/8].fBatBlockVoltage[j%4]);
					iFileLen += sprintf(pFileMakeBuf + iFileLen, "\t%-16.2f ", pBatteryLogInfo[i].fBatBlock[j/8].fBatBlockVoltage[j%4]);
				}


			}

			for(j = 0; j < 7; j++)
			{
				if(stOutBatteryGeneralInfo.byTemperatureInfo[j])
				{
					iLen += sprintf(pMakeBuf + iLen, " <td>%8.2f</td> ",pBatteryLogInfo[i].fTemp[j]);
					iFileLen += sprintf(pFileMakeBuf + iFileLen, " \t%-16.2f ",pBatteryLogInfo[i].fTemp[j]);

				}
			}


			iLen += sprintf(pMakeBuf + iLen, "</tr>");
			iFileLen += sprintf(pFileMakeBuf + iFileLen, "\n");

		}
		//iLen += sprintf(pMakeBuf + iLen, "");

		////TRACE("pMakeBuf:[%s]\n", pMakeBuf);
		*ppBuf = pMakeBuf;

		if(stGeneralInfo != NULL)
		{
			DELETE(stGeneralInfo);
			stGeneralInfo = NULL;
		}



		if(pBatteryLogInfo != NULL)
		{
			
			DELETE(pBatteryLogInfo);
			pBatteryLogInfo=NULL;
			
		}

		if(pFileMakeBuf != NULL )
		{
			DELETE(pFileMakeBuf);
			pFileMakeBuf = NULL;
		}

	}


	

	//print test result	
	
	//iLen += sprintf(pMakeBuf + iLen, "");

	//TRACE("pFileMakeBuf:[%s]\n", pFileMakeBuf);

	*ppBuf = pMakeBuf;

	FILE *fp;
	if((fp = fopen(WEB_LOG_DIR,"wb")) != NULL)
	{

		TimeToString(stGeneralInfo->btSummary.tStartTime, TIME_CHN_FMT, 
			szTime1, sizeof(szTime1));

		TimeToString(stGeneralInfo->btSummary.tEndTime, TIME_CHN_FMT, 
			szTime2, sizeof(szTime2));

		char	szFileTitle[2048];
		iLen = sprintf(szFileTitle, "Query  Battery test log \n" \
			"Start time: %s \n" \
			"End time  : %s\n" \
			"Start reason   : %s\n" \
			"End reason     : %s\n" \
			"Test result    : %s\n\n\n" \
			"\t%6s \t%-32s \t%-16s",
			szTime1,
			szTime2,
			szBatteryStartReason[stGeneralInfo->btSummary.iStartReason],
			szBatteryEndReason[stGeneralInfo->btSummary.iEndReason],
			szBatteryTestResult[stGeneralInfo->btSummary.iTestResult],
			"Index",
			"Record time",
			"System voltage(V)");
		i = 0;						
		while(i < 177)
		{

			if(iStatus[i])
			{
				iLen += sprintf(szFileTitle + iLen, "\t%-20s ", szBatterTitle[i]);
				i++;
			}
			else
			{
				i++;
			}
		}						
		sprintf(szFileTitle + iLen, "%s ", "\n");						



		fwrite(szFileTitle,strlen(szFileTitle), 1, fp);
		fwrite(pFileMakeBuf,strlen(pFileMakeBuf), 1, fp);
		fclose(fp);
	}




	if(pBatteryLogInfo != NULL)
	{
		
		DELETE(pBatteryLogInfo);
		pBatteryLogInfo = NULL;
	}

	//added by wankun in order to avoid mem leak
	if(pFileMakeBuf != NULL )
	{
		DELETE(pFileMakeBuf);
		pFileMakeBuf = NULL;
	}
	//ended by wankun
	return TRUE;
}






/*==========================================================================*
* FUNCTION :    Web_SendQueryData_Netscape
* PURPOSE  :	 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   char *buf:
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WanKun               DATE: 2007-01-17 11:20
*==========================================================================*/
static int Web_SendQueryData_Netscape(IN char *buf,  IN char *filename)
{
	char		*ptr	= NULL;				/*pointer to buf*/
	char		*pBuf	= NULL;				/*output buffer*/
	int			fd2;				/*fd2: handle of fifo*/
	int			iEquipID;			/**/
	int			iQueryType;			/*Query type*/
	int			iLanguage;			/*language*/
	time_t		toTime = 0, fromTime = 0;	/*query time*/
	char		szGetData[64];		/*temp buffer*/
	int			iQueryTimes;



	//open the fifo
	if((fd2 = open(filename,O_WRONLY  )) < 0)
	{
		AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to open FIFO");
		return FALSE;

	}

	/*offset 12 */
	ptr = buf +  MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;


	/*get QueryType*/
#ifdef _SHOW_WEB_INFO
	//TRACE("iQueryType : %s\n", ptr);
#endif	

	strncpyz(szGetData, ptr, COMM_QUERY_TYPE_LEN + 1);
	iQueryType = atoi(szGetData);
	ptr = ptr + COMM_QUERY_TYPE_LEN;

	/*get query time (from time)*/
	strncpyz(szGetData, ptr, COMM_QUERY_TIME_LEN + 1);
	//fromTime = Web_transferCharToTime(szGetData, TRUE);
	fromTime = (time_t)atoi(szGetData);
	ptr = ptr + COMM_QUERY_TIME_LEN;


	/*get query time (to time)*/
	strncpyz(szGetData, ptr, COMM_QUERY_TIME_LEN + 1);
	//toTime = Web_transferCharToTime(szGetData, FALSE);
	toTime = (time_t)atoi(szGetData);
	ptr = ptr + COMM_QUERY_TIME_LEN;

	//#ifdef _SHOW_WEB_INFO
	//TRACE("fromTime[%d][%s]  toTime[%d][%s]\n",fromTime, ctime(&fromTime),toTime,ctime(&toTime));
	//#endif

	/*get query equipID */
	strncpyz(szGetData, ptr, MAX_EQUIPID_LEN + 1);
	iEquipID = atoi(szGetData);
	TRACE("iEquipID : %d  \n", iEquipID);
	ptr = ptr + MAX_EQUIPID_LEN;

	/*get Language type*/
	strncpyz(szGetData, ptr, MAX_LANGUAGE_TYPE_LEN + 1);
	iLanguage = atoi(szGetData);
	TRACE("iLanguage : %d  \n", iLanguage);
#ifdef _SHOW_WEB_INFO
	//TRACE("iQueryType : %d  \n", iQueryType);
#endif	


	int iReturn = 0;
	switch(iQueryType)
	{
	case QUERY_HIS_DATA:
		TRACE("-----------in netscape--QUERY_HIS_DATA-----------\n");
		iReturn = Web_MakeQueryHisDataBuffer_Netscape(iEquipID, 
			fromTime, 
			toTime, 
			iLanguage, 
			&pBuf);

		break;
	case QUERY_STAT_DATA:

		iReturn = Web_MakeQueryStatDataBuffer(iEquipID, 
			fromTime, 
			toTime, 
			iLanguage, 
			&pBuf);
		//TRACE("Web_MakeQueryStatDataBuffer[%d]\n", iReturn);
		break;
	case QUERY_HISALARM_DATA:
		TRACE("-----------in netscape--QUERY_HISALARM_DATA-----------\n");
		iReturn = Web_MakeQueryAlarmDataBuffer_Netscape(iEquipID, 
			fromTime, 
			toTime, 
			iLanguage, 
			&pBuf);
		break;

	case QUERY_CONTROLCOMMAND_DATA:
		TRACE("-----------in netscape----QUERY_QUERY_CONTROLCOMMAND_DATA-----------\n");
		iReturn = Web_MakeQueryControlDataBuffer_Netscape(fromTime, 
			toTime, 
			iLanguage, 
			&pBuf);
		break;

	case QUERY_BATTERYTEST_DATA:
		iQueryTimes = iEquipID;			//special for Battery test, 
		//iEquipID is that you want to query 
		//what times of battery test
#ifdef _SHOW_WEB_INFO
		//TRACE("Start to Web_MakeQueryBatteryTestLogBuffer");
#endif

		iReturn = Web_MakeQueryBatteryTestSumBuffer_Netscape(iQueryTimes, &pBuf);
		break;

	case QUERY_BATTERYTEST_SUM:
		iQueryTimes = iEquipID;
		iReturn = Web_MakeQueryBatteryTestSumBuffer_Netscape(iQueryTimes, &pBuf);
		break;

	case QUERY_HISLOG_DATA:
		TRACE("-----------in netscape----QUERY_HISLOG_DATA-----------\n");
		iReturn = Web_MakeQueryHisLogBuffer_Netscape(fromTime, toTime, &pBuf);
		break;
	case QUERY_CLEAR_ALARM:
		if( Web_GetProtectedStatus() == TRUE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"protect on , we can't clear history alarm log!");
			iReturn = FALSE;
		}
		else
		{
			iReturn  = DAT_StorageDeleteRecord(HIST_ALARM_LOG);
			//return success clear flag to cgi
			if(iReturn == TRUE)
			{
				pBuf = NEW(char, 3);
				sprintf(pBuf,"%2d", 0);
				AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"succeed to clear history alarm!");
			}
		}
		break;
	case CLEAR_HISTORY_DATA:
		if( Web_GetProtectedStatus() == TRUE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"protect on , we can't clear history DATA log!");
			iReturn = FALSE;
		}
		else
		{
			iReturn  = DAT_StorageDeleteRecord(HIS_DATA_LOG);
			//return success clear flag to cgi
			if(iReturn == TRUE)
			{
				pBuf = NEW(char, 3);
				sprintf(pBuf,"%2d", 0);
				AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"succeed to clear history DATA!");
			}
		}
		break;
	case CLEAR_HISTORY_LOG:
		if( Web_GetProtectedStatus() == TRUE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"protect on , we can't clear system LOG!");
			iReturn = FALSE;
		}
		else
		{
			iReturn  = DAT_StorageDeleteRecord(ACU_RUNNING_LOG);
			//return success clear flag to cgi
			if(iReturn == TRUE)
			{
				pBuf = NEW(char, 3);
				sprintf(pBuf,"%2d", 0);
				AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"succeed to clear SYSTEM LOG!");
			}
		}
		break;
	case CLEAR_COMMAND_LOG:
		TRACE("-----------in netscape----CLEAR_COMMAND_LOG-----------\n");
		if( Web_GetProtectedStatus() == TRUE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"protect on , we can't clear control command LOG!");
			iReturn = FALSE;
		}
		else
		{
			iReturn  = DAT_StorageDeleteRecord(CTRL_CMD_LOG);
			//return success clear flag to cgi
			if(iReturn == TRUE)
			{
				pBuf = NEW(char, 3);
				sprintf(pBuf,"%2d", 0);
				AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"succeed to clear control command log!");
			}
		}
		break;
	case CLEAR_BAT_TEST_LOG:
		if( Web_GetProtectedStatus() == TRUE)
		{
			AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"protect on , we can't clear battery test LOG!");
			iReturn = FALSE;
		}
		else
		{
			iReturn  = DAT_StorageDeleteRecord(BATT_TEST_LOG_FILE);
			//return success clear flag to cgi
			if(iReturn == TRUE)
			{
				pBuf = NEW(char, 3);
				sprintf(pBuf,"%2d", 0);
				AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"succeed to clear battery test log!");
			}
		}
		break;
	case QUERY_DISEL_TEST:
		iReturn	 = Web_MakeQueryDiselTestLog(fromTime, toTime,iLanguage,&pBuf);
		break;
	default:
		break;
	}

	if(iReturn == FALSE)
	{

		pBuf = NEW(char, 3);
		sprintf(pBuf, "%2d", 2);
		if(iQueryType == QUERY_CLEAR_ALARM) AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"failed to clear history alarm!");
		if(iQueryType == CLEAR_HISTORY_DATA) AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"failed to clear history DATA!");
		if(iQueryType == CLEAR_HISTORY_LOG) AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"failed to clear history LOG!");
		if(iQueryType == CLEAR_COMMAND_LOG) AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"failed to clear control command LOG!");
		if(iQueryType == CLEAR_BAT_TEST_LOG) AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_INFO,"failed to clear battery test LOG!");
	}

	int iPipeLen = strlen(pBuf)/(PIPE_BUF - 1)  + 1;

#ifdef _SHOW_WEB_INFO
	//TRACE("iPipeLen = %d\n", iPipeLen);
#endif


	//#ifdef _DEBUG
	//	FILE *fp = fopen("/acu/www/html/query.htm","w");
	//	fwrite(pBuf,strlen(pBuf), 1, fp);
	//	fclose(fp);
	//#endif

	if(iPipeLen <= 1)
	{
		if((write(fd2, pBuf, strlen(pBuf) + 1)) < 0)
		{
			return FALSE;
		}
	}
	else
	{
		int m;
		char szP[PIPE_BUF - 1];
		for(m = 0; m < iPipeLen; m++)
		{
			strncpyz(szP, pBuf + m * (PIPE_BUF - 2), PIPE_BUF - 1);
#ifdef _SHOW_WEB_INFO
			//TRACE("m =%d \n", m);
#endif


			if((write(fd2, szP, PIPE_BUF - 1)) < 0)
			{
				return FALSE;
			}
		}

	}

	TRACE("Web_SendQueryData : %s\n", pBuf);
	//if((write(fd2, pBuf, strlen(pBuf) + 1)) < 0)
	//{
	//	AppLogOut(CGI_APP_LOG_COMM_NAME,APP_LOG_WARNING,"Fail to write FIFO");
	//	close(fd2);
	//	
	//	char			szCommand[30];
	//	int iLen = sprintf(szCommand,"kill ");
	//	strncpyz(szCommand + iLen,buf, MAX_COMM_PID_LEN + 1);
	//	system(szCommand);

	//	return FALSE;
	//}

	if(pBuf != NULL)
	{
		DELETE(pBuf);
		pBuf = NULL;
	}
	close(fd2);
	return TRUE;
}



/*----------------------------------Marco Yang------------------------------------------------------*/

static char *Web_MakeNMSPrivateConfigureBuffer_net(void)
{
	APP_SERVICE		*stAppService = NULL;
	int				iBufLen = 1;
	NMS_INFO		*stNmsInfo = NULL;
	char			*szNmsInfo = NULL;
	struct in_addr	inIP;
	int				i = 0, iLen = 0;
	int				iNMSNum = 0;

	if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV2_NAME);
	}
	else
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMSV3_NAME);
	}

	//stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMS_NAME);

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{
		/*			stAppService->pfnServiceConfig(stAppService->hServiceThread,
		stAppService->bServiceRunning,
		&stAppService->args, 
		4,
		sizeof(int),
		NULL); */
		if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args, 
			GET_NMS_USER_INFO,
			&iBufLen,
			(void *)&stNmsInfo) == ERR_SNP_OK)
		{

			iNMSNum =  iBufLen /sizeof(NMS_INFO);
			szNmsInfo = NEW(char, 200 * iNMSNum);
			iLen += sprintf(szNmsInfo + iLen,"%d,",	iNMSNum);
			if(iNMSNum > 0)
			{
				inIP.s_addr = stNmsInfo->ulIpAddress;
				iLen += sprintf(szNmsInfo + iLen,"%d,%s,%s,%s,",  
					stNmsInfo->nTrapLevel,
					stNmsInfo->szPublicCommunity,
					stNmsInfo->szPrivateCommunity,
					inet_ntoa(inIP)
					);
				stNmsInfo++;
			}
			for(i = 1; i < iNMSNum && stNmsInfo != NULL; i++, stNmsInfo++)
			{

				inIP.s_addr = stNmsInfo->ulIpAddress;
				iLen += sprintf(szNmsInfo + iLen,"%s,",inet_ntoa(inIP));

			}
			if(iLen >= 1)
			{
				*(szNmsInfo + iLen - 1) = 32;
			}
			else
			{
				DELETE(szNmsInfo);
				szNmsInfo = NULL;

			}
			//printf("szNmsInfo={%s}\n", szNmsInfo);
			return szNmsInfo;

		}
	}
	return NULL;
}



static int Web_ModifyNMSPrivateConfigure_net(IN char *szNmsBuffer)
{
#define			END_OF_NMS1						';'
	//	ASSERT(szNmsBuffer);
	NMS_INFO		stNmsInfo;
	APP_SERVICE		*stAppService = NULL;
	int				iBufLen = sizeof(NMS_INFO);
	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	int				iModifyType = 0;
	int				iReturn = 0;
	char			*szTrim = NULL;
	unsigned long    ulIpAddress1, ulIpAddress2, ulIpAddress3;
	NMS_INFO		*stAllNmsInfo = NULL;
	int				iAllBufLen = 1, iNMSNum, i;


	//�ֽⷢ�͹����Ļ�����

	if((ptr = szNmsBuffer) != NULL)
	{
		pSearchValue = strchr(ptr, END_OF_NMS1);
		//printf("ptr=%s\npSearchValue=%s", ptr, pSearchValue);	
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			//printf("iPosition=%d", iPosition);
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				szTrim = Cfg_RemoveWhiteSpace(szExchange);
				//printf("\nszTrim= %s", szTrim);
				sprintf(stNmsInfo.szPublicCommunity, szTrim, sizeof(stNmsInfo.szPublicCommunity));
				//printf("szPublicCommunity : %s\n", stNmsInfo.szPublicCommunity);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, END_OF_NMS1);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				szTrim = Cfg_RemoveWhiteSpace(szExchange);
				sprintf(stNmsInfo.szPrivateCommunity, szTrim, sizeof(stNmsInfo.szPrivateCommunity));
				printf("\nszTrim= %s", szTrim);
				//TRACE("szPrivateCommunity : %s\n", stNmsInfo.szPrivateCommunity);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, END_OF_NMS1);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stNmsInfo.nTrapLevel = atoi(szExchange);
				//printf("\nnTrapLevel= %s", szExchange);
				ptr = ptr + iPosition;
			}
			else
			{
				return FALSE;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, END_OF_NMS1);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				char		*szIP = NULL;
				szIP = Cfg_RemoveWhiteSpace(szExchange );
				//printf("szIP = {%s}\n", szIP);
				ulIpAddress1 = inet_addr(szIP);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, END_OF_NMS1);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				char		*szIP = NULL;
				szIP = Cfg_RemoveWhiteSpace(szExchange);
				//printf("szIP = {%s}\n", szIP);
				ulIpAddress2 = inet_addr(szIP);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, END_OF_NMS1);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				char		*szIP = NULL;
				szIP = Cfg_RemoveWhiteSpace(szExchange);
				//printf("szIP = {%s}\n", szIP);
				ulIpAddress3 = inet_addr(szIP);
				ptr = ptr + iPosition;
			}

		}
		//ptr = ptr + 1;





	}
	else
	{
		return FALSE;
	}


	//�޸�
	printf("\nNMS_INFO : %d     %s %s\n  ", stNmsInfo.nTrapLevel, 
	stNmsInfo.szPrivateCommunity, 
	stNmsInfo.szPublicCommunity);
	stNmsInfo.nPrivilege = 0;

	if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV2_NAME);
	}
	else
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
	}
	//stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMS_NAME);
	

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{
		//added by YangGuoxin,3/20/2007
		iModifyType = DELETE_ALL_NMS_USER_INFO;//Delete all
		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args, 
			iModifyType,
			&iBufLen,
			NULL);
		printf("iReturn = %d", iReturn);
		iModifyType = ADD_NMS_USER_INFO;//Add
		stNmsInfo.ulIpAddress =  ulIpAddress1;

		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args, 
			iModifyType,
			&iBufLen,
			(void *)&stNmsInfo);
		printf("iReturn = %d", iReturn);
		if(ulIpAddress1 != ulIpAddress2)
		{
			stNmsInfo.ulIpAddress =  ulIpAddress2;
			iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
				stAppService->bServiceRunning,
				&stAppService->args, 
				iModifyType,
				&iBufLen,
				(void *)&stNmsInfo);
			printf("iReturn = %d", iReturn);
		}
		if(ulIpAddress1 != ulIpAddress3 && ulIpAddress2 != ulIpAddress3)
		{
			stNmsInfo.ulIpAddress =  ulIpAddress3;
			iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
				stAppService->bServiceRunning,
				&stAppService->args, 
				iModifyType,
				&iBufLen,
				(void *)&stNmsInfo);
			printf("iReturn = %d", iReturn);
		}

		if(iReturn == ERR_SNP_OK)
		{
			return TRUE;//iReturn;
		}
		else
		{
			return FALSE;
		}

	}
	//printf("can't get the service\n");
	return FALSE;
}


static char *Web_MakeESRPrivateConfigreBuffer_net(IN int nType)
{
	COMMON_CONFIG	stCommonConfig ;
	APP_SERVICE		*stAppService = NULL;
	int				iLen = 0;
	char			*szReturn = NULL;
	int				iReturn = 0;
	stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{

		iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
			stAppService->bServiceRunning,
			&stAppService->args, 
			0x0,
			0,
			(void *)&stCommonConfig);

		szReturn = NEW(char, 250);
		memset(szReturn,0x0,250);

		if(szReturn != NULL)
		{
			//printf("stAppService ---- 4\n");
			//iLen += sprintf(szReturn + iLen, "%2d", iReturn);
			if(iReturn == ERR_SERVICE_CFG_OK)
			{
				//printf("OK_____________%d\n", nType);
				if(nType == NET_EEM_GENEARL) //General description)
				{
					iLen += sprintf(szReturn + iLen, "\t %d,%d,%d,%d,%d,%d,%d,%d,%d,", 
						stCommonConfig.bReportInUse,	
						stCommonConfig.bCallbackInUse,
						stCommonConfig.iProtocolType,
						stCommonConfig.iMediaType,
						stCommonConfig.byCCID,
						stCommonConfig.iSOCID,
						stCommonConfig.iMaxAttempts,
						stCommonConfig.iAttemptElapse/1000,
						stCommonConfig.iSecurityLevel);
				}
				else if(nType == NET_EEM_PSTN )//pstn)
				{
					iLen += sprintf(szReturn + iLen, "\t %s,%s,%s,%s,", 
						stCommonConfig.szAlarmReportPhoneNumber[0],
						stCommonConfig.szAlarmReportPhoneNumber[1],	
						stCommonConfig.szCallbackPhoneNumber[0],
						stCommonConfig.szCommPortParam);
				}
				else if(nType == NET_EEM_TCPIP)//tcpip)
				{
					iLen += sprintf(szReturn + iLen, "\t %s,%s,%s,%s,%s,", 
						stCommonConfig.szReportIP[0],
						stCommonConfig.szReportIP[1],	
						stCommonConfig.szSecurityIP[0],
						stCommonConfig.szSecurityIP[1],
						stCommonConfig.szCommPortParam);
				}
				else if(nType == NET_EEM_LEASDLINE)//Leased line)
				{
					iLen += sprintf(szReturn + iLen, "\t %s,", 
						stCommonConfig.szCommPortParam);
				}

				printf("%s\n", szReturn);
				return szReturn;


			}
		}

	}
	printf("return NULL\n");
	return NULL;
}

static int Web_ModifyESRPrivateConfigure_net(IN char *szBuffer,IN int iCommandType)
{
	ASSERT(szBuffer);
	COMMON_CONFIG	stCommonConfig ;
	APP_SERVICE		*stAppService = NULL;
	int				iModifyType = 0;
	int				iReturn = 0;



	if((iModifyType = Web_GetESRModifyInfo_net(szBuffer, &stCommonConfig, iCommandType)) != FALSE)
	{
		stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_ESR_NAME);


		if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
		{
			//printf("iModifyType = %d\n", iModifyType);
			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
				stAppService->bServiceRunning,
				&stAppService->args, 
				iModifyType,
				0,
				(void *)&stCommonConfig);// == ERR_SERVICE_CFG_OK)
			//printf("iReturn =%d\n", iReturn);

			return iReturn;
		}
	}
	return ERR_SERVICE_CFG_FAIL;
}


static int Web_StateMgmt_Netscape(IN char *buf, 
				  IN char *filename)
{
#define WEB_NET_SUCCESS			0
#define WEB_NET_FAIL			0

	int			fd2;
	char		pGetData[100];
	char		*ptr = NULL;
	int			iLanguage = 0, iLen  = 0;;
	int			iCommandType;
	char		*pReturnBuf = NULL;
	int			iReturnValue = 0;
	int			iMainCommID = 0;

	TRACE("\n------into Web_StateMgmt_Netscape-------\n");
	if((fd2 = open(filename,O_WRONLY )) > 0)
	{
		/*offset*/

		ptr = buf;
		TRACE("Web_SendConfigureData :ptr:%s\n", ptr);

		ptr = ptr + MAX_COMM_PID_LEN + MAX_COMM_GET_TYPE;
#define MAX_MODIFY_PASS_LEN				2
		/*GET IF Main Command Type*/
		strncpyz(pGetData,ptr,MAX_MODIFY_PASS_LEN + 1);
		iMainCommID = atoi(pGetData);
		ptr = ptr + MAX_MODIFY_PASS_LEN;
#define MAX_COMMAND_TYPE_LEN			2

		/*Get command type*/
		strncpyz(pGetData,ptr,MAX_COMMAND_TYPE_LEN + 1);
		iCommandType = atoi(pGetData);
		ptr = ptr + MAX_COMMAND_TYPE_LEN;
		printf("_____%s_________iCommandType_____________%d______________%d\n", buf, iMainCommID, iCommandType);
		switch(iMainCommID)
		{
		case SET_NET_CONTROL:
			iReturnValue = Web_SendNetscapeCommand(buf,filename);
			return TRUE;
			break;
		case SET_NMS_SETTING:
			if((iReturnValue = Web_ModifyNMSPrivateConfigure_net(ptr)) == TRUE)
			{	
				pReturnBuf = "1";
			}
			else
			{
				pReturnBuf = "0";
			}
			break;
		case GET_NMS_SETTING:
			pReturnBuf = Web_MakeNMSPrivateConfigureBuffer_net();
			break;
		case SET_EEM_SETTING:
			iReturnValue = Web_ModifyESRPrivateConfigure_net(ptr,iCommandType);
			if(iReturnValue == WEB_NET_SUCCESS)
			{
				pReturnBuf = "1";
			}
			else
			{
				pReturnBuf = "0";
			}
			break;
		case GET_EEM_SETTING:
			pReturnBuf = Web_MakeESRPrivateConfigreBuffer_net(iCommandType);
			break;
		case SET_IP_SETTING:

			Web_Modify_AcuIP_Addr(ptr);
			pReturnBuf = "1";
			//printf(pReturnBuf);
			break;
		case GET_IP_SETTING:
			pReturnBuf = Web_MakeNetWorkInfoBuffer();
			//printf(pReturnBuf);
			break;
		case SET_TIME_SETTING:
			if(Web_SetACUTime_net(ptr) == TRUE)
				pReturnBuf = "1";
			else
				pReturnBuf = "0";	
			break;
		case GET_LANG_SETTING:
			if(Web_GetNowLangValue() == 1)
			{
				pReturnBuf = "1";//Local
			}
			else
			{
				pReturnBuf = "0";//English
			}
			break;
		case SET_LANG_SETTING:
			if(Web_SetCurrentLangValue(ptr) == TRUE)
			{
				pReturnBuf = "1";
			}
			else
			{
				pReturnBuf = "0";
			}
			break;
		case SET_RECOVER_RESET:
			//recover
			if(Web_Recover(ptr) ==  TRUE)
			{
				pReturnBuf = "1";
			}
			else
			{
				pReturnBuf = "0";
			}
			break;

		}


		int iPipeLen = strlen(pReturnBuf)/(PIPE_BUF - 1)  + 1;
		if(iPipeLen <= 1)
		{
			if((write(fd2, pReturnBuf, strlen(pReturnBuf) + 1)) < 0)
			{
				return FALSE;
			}
		}
		else
		{
			int m;
			char szP[PIPE_BUF - 1];
			for(m = 0; m < iPipeLen; m++)
			{
				strncpyz(szP, pReturnBuf + m * (PIPE_BUF - 2), PIPE_BUF - 1);
				if((write(fd2, szP, PIPE_BUF - 1)) < 0)
				{
					return FALSE;
				}
			}

		}

		
		if(iMainCommID == GET_NMS_SETTING)
		{
			SAFELY_DELETE(pReturnBuf);
		}
		close(fd2);
		return TRUE;
	}
	else
	{

		return FALSE;
	}

}

//////////////////////////////////////////////////////////////////////////
//Support Netscape Multi_language
//WangJing 2007-04-18
#define CONFIG_FILE_WEB_PRIVATE_N   "private/web/Web_Res_VerN.cfg"
#define START_OK            "[START_OK]"


/*==========================================================================*
* FUNCTION :    Web_Netscape_MultiLang()
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int  Web_Netscape_MultiLang()
{
	TRACE("INTO Web_Netscape_MultiLang()\n");
	int iReturn;
	iReturn = Web_GetVersionOfWebPages_N(&stWebVersionInfoN);
	if(iReturn != TRUE)
	{
		TRACE("Web_GetVersionOfWebPages_N FALSE\n");
		return FALSE;
	}

	iReturn = Web_Netscape_ReadConfig();
	if(iReturn != TRUE)
	{
		TRACE("Web_Netscape_ReadConfig FALSE\n");
		Web_Free_ResourceN();
		return FALSE;
	}

	TRACE("stWebVersionInfoN.fVersion :%f\n",stWebVersionInfoN.fVersion);
	TRACE("stWebCompareVersionN.fVersion :%f\n",stWebCompareVersionN.fVersion);
	TRACE("stWebVersionInfoN.szLanguage :%s\n",stWebVersionInfoN.szLanguage);
	TRACE("stWebCompareVersionN.szLanguage :%s\n",stWebCompareVersionN.szLanguage);
	TRACE("iStartOK:%d\n",iStartOK);


	if(iStartOK != 0 && 
		stWebVersionInfoN.fVersion >= stWebCompareVersionN.fVersion &&
		strcmp(Cfg_RemoveWhiteSpace(stWebVersionInfoN.szLanguage),Cfg_RemoveWhiteSpace(stWebCompareVersionN.szLanguage))==0 &&
		Web_Netscape_PageLang())
	{
		TRACE("NO Change In Netscape Pages\n");
		Web_Free_ResourceN();
		return TRUE;
	}
	else
	{
		TRACE("Web_Netscape_CreatePage()");

		if(Web_Netscape_CreatePage())
		{

			Web_Netscape_ChangeFlage();
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


}

/*==========================================================================*
* FUNCTION :    Web_Netscape_PageLang()
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int  Web_Netscape_PageLang()
{
	int iError;
	int iSignalType;
	int iSignalID;
	int iVarSubID;
	int iEquipID;
	SIG_BASIC_VALUE *pBefSigValue;
	SIG_BASIC_VALUE *pCurSigValue;

	//TRACE("%d[%d,%d,%d]\n", i, iEquipID, iSignalID, iSignalType);
	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 181;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	iError += DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pBefSigValue,			
		0);

	TRACE("BefSigValue %d\n",pBefSigValue->varValue.enumValue);

	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 182;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	iError += DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pCurSigValue,			
		0);

	TRACE("CurSigValue %d\n",pCurSigValue->varValue.enumValue);

	if(pBefSigValue->varValue.enumValue == pCurSigValue->varValue.enumValue)
	{
		TRACE("Web_Netscape_PageLang TRUE\n");
		return TRUE;
	}
	else
	{
		return FALSE;

	}


}
/*==========================================================================*
* FUNCTION :    Web_GetVersionOfWebPages_N(WEB_VERSION_INFO  *stWebVersionInfo_R)
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int Web_GetVersionOfWebPages_N(WEB_VERSION_INFO  *stWebVersionInfo_R)
{
	int		iRst;
	void	*pProf = NULL;  
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;



	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CONFIG_FILE_WEB_PRIVATE_N, szCfgFileName, MAX_FILE_PATH);


	TRACE("szCfgFileName :%s\n",szCfgFileName);

	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{		
		return FALSE;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return FALSE;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);
		return FALSE;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

	if (pProf == NULL)
	{
		DELETE(szInFile);
		return FALSE;
	}
	char	szVersion[32];
	//1.Read Local language version
	iRst = Cfg_ProfileGetString(pProf,
		WEB_LOCAL_LANGUAGE_VERSION, 
		szVersion,
		sizeof(szVersion)); 
	stWebVersionInfo_R->fVersion = atof(szVersion);

	TRACE("Successfully to  Cfg_ProfileGetString  %s\n", szVersion);
	if (iRst != 1)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
			"There are no pages number in Resource.cfg.");
	}

	//2.Read Local language
	iRst = Cfg_ProfileGetString(pProf,
		WEB_LOCAL_LANGUAGE, 
		stWebVersionInfo_R->szLanguage,
		sizeof(stWebVersionInfo_R->szLanguage)); 
	TRACE("Successfully to  Cfg_ProfileGetString  %s\n",stWebVersionInfo_R->szLanguage);
	if (iRst != 1)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
			"There are no pages number in Resource.cfg.");
	}
	//3.Read START_OK
	char	szStartOk[16];
	iRst = Cfg_ProfileGetString(pProf,
		START_OK, 
		szStartOk,
		sizeof(szStartOk));
	iStartOK = atoi(szStartOk);

	TRACE("Successfully to  Cfg_ProfileGetString  %s\n", szStartOk);

	if (iRst != 1)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
			"There are no pages number in Resource.cfg.");
	}
	/*if (iRst != ERR_WEB_OK)
	{
	AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
	"Fail to get resource table.");

	DELETE(pProf); 
	DELETE(szInFile);
	return iRst;
	}*/


	DELETE(pProf); 
	DELETE(szInFile);
	return TRUE;
}

/*==========================================================================*
* FUNCTION :    Web_Netscape_ReadConfig()
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int Web_Netscape_ReadConfig()
{
	int		iRst;
	void	*pProf = NULL;  
	FILE	*pFile = NULL;
	char	*szInFile = NULL;
	size_t	ulFileLen;
	char		*pLangCode = NULL;

	DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		LOCAL_LANGUAGE_CODE, 
		0, 
		&iBufLen,
		&(pLangCode),
		0);

	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(CreateLangResourceFileName(CONFIG_FILE_WEB_N_PRIVATE, pLangCode),//CONFIG_FILE_WEB_PRIVATE, 
		szCfgFileName, 
		MAX_FILE_PATH);
	TRACE("szCfgFileName[%s]\n",szCfgFileName);
	/*Debug*/	
	//sprintf(szCfgFileName,"%s","/acu/config/private/web/Web_Resource.res");//"/home/ygx/cfg/Source.cfg");
	/*End Debug*/
	//TRACE("szCfgFileName : %s\n", szCfgFileName);
	/* open file */
	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{		
		return FALSE;
	}

	//TRACE("Successfully to read szCfgFileName : %s\n", szCfgFileName);

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return FALSE;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);

		return FALSE;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);

	if (pProf == NULL)
	{
		DELETE(szInFile);
		return FALSE;
	}
	//1.Read Number of pages
	iRst = Cfg_ProfileGetInt(pProf,
		WEB_PAGES_NUMBER, 
		&iNPagesNumber); 
	TRACE("Successfully to  Cfg_ProfileGetInt  %d\n", iNPagesNumber);
	if (iRst != 1)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
			"There are no pages number in Resource.cfg.");
	}

	//pWebHeadInfo = NEW(WEB_HEAD_RESOURCE_INFO, 1);
	//2. Get local language
	char		szVersion[32];

	iRst = Cfg_ProfileGetString(pProf,
		WEB_LOCAL_LANGUAGE_VERSION, 
		szVersion, 
		sizeof(szVersion)); 

	stWebCompareVersionN.fVersion = atof(szVersion);
	TRACE("Successfully to Cfg_ProfileGetString  %s\n", szVersion);

	//3. Get local language version
	iRst = Cfg_ProfileGetString(pProf,
		WEB_LOCAL_LANGUAGE, 
		stWebCompareVersionN.szLanguage,
		sizeof(stWebCompareVersionN.szLanguage)); 

	TRACE("Successfully to Cfg_ProfileGetString  %s\n",stWebCompareVersionN.szLanguage);
	//2.Read current all command content and command numbers
	iRst = Web_Netscape_LoadWebPrivateConfigProc(iNPagesNumber, pProf);

	//TRACE("Successfully to  LoadWebPrivateConfigProc  \n");

	//printf("%d%s\n", pWebHeadInfo->iNumber, pWebHeadInfo->stWebHeadInfo->CfgName);

	if (iRst != ERR_CFG_OK)
	{
		AppLogOut("WEB_READ_CFG", APP_LOG_ERROR, 
			"Fail to get resource table.");

		DELETE(pProf); 
		DELETE(szInFile);
		return iRst;
	}


	DELETE(pProf); 
	DELETE(szInFile);
	return TRUE;
}
/*==========================================================================*
* FUNCTION :    Web_Netscape_LoadWebPrivateConfigProc(IN int iPages, IN void *pCfg)
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int Web_Netscape_LoadWebPrivateConfigProc(IN int iPages, IN void *pCfg)
{
	TRACE("INTO Web_Netscape_LoadWebPrivateConfigProc \n");

	iPages = 41;
	int			i = 0, k = 0;
	//char		*szNumber = NULL, *szFile = NULL;
	CONFIG_TABLE_LOADER loader[WEB_MAX_HTML_PAGE_NUM_NETSCAPE];
	//pWebHeadInfo = NEW(WEB_HEAD_RESOURCE_INFO, 1);
	//WEB_HEAD_RESOURCE_INFO	pWebHeadInfo1;// = NEW(WEB_HEAD_RESOURCE_INFO, 1);

	pWebCfgInfoN	= NEW(WEB_PRIVATE_RESOURCE_INFO, WEB_MAX_HTML_PAGE_NUM_NETSCAPE);
	//ASSERT(pWebHeadInfo1);
	ASSERT(pWebCfgInfoN);

	k = 1;
	for(i = 1; i < WEB_MAX_HTML_PAGE_NUM_NETSCAPE + 1; i++)
	{
		strncpyz(pWebCfgInfoN[i-1].szFileName,szTemplateFileN[k] , sizeof(pWebCfgInfoN[i-1].szFileName));
		k = k + 2;
	}

	DEF_LOADER_ITEM(&loader[0],
		WEB_N_PAGES_P1_NUM,
		&(pWebCfgInfoN[0].iNumber), 
		WEB_N_PAGES_P1,
		&(pWebCfgInfoN[0].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[1],
		WEB_N_PAGES_P2_NUM,
		&(pWebCfgInfoN[1].iNumber), 
		WEB_N_PAGES_P2,
		&(pWebCfgInfoN[1].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[2],
		WEB_N_PAGES_P3_NUM,
		&(pWebCfgInfoN[2].iNumber), 
		WEB_N_PAGES_P3,
		&(pWebCfgInfoN[2].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[3],
		WEB_N_PAGES_P4_NUM,
		&(pWebCfgInfoN[3].iNumber), 
		WEB_N_PAGES_P4,
		&(pWebCfgInfoN[3].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[4],
		WEB_N_PAGES_P5_NUM,
		&(pWebCfgInfoN[4].iNumber), 
		WEB_N_PAGES_P5,
		&(pWebCfgInfoN[4].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[5],
		WEB_N_PAGES_P6_NUM,
		&(pWebCfgInfoN[5].iNumber), 
		WEB_N_PAGES_P6,
		&(pWebCfgInfoN[5].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[6],
		WEB_N_PAGES_P7_NUM,
		&(pWebCfgInfoN[6].iNumber), 
		WEB_N_PAGES_P7,
		&(pWebCfgInfoN[6].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[7],
		WEB_N_PAGES_P8_NUM,
		&(pWebCfgInfoN[7].iNumber), 
		WEB_N_PAGES_P8,
		&(pWebCfgInfoN[7].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[8],
		WEB_N_PAGES_P9_NUM,
		&(pWebCfgInfoN[8].iNumber), 
		WEB_N_PAGES_P9,
		&(pWebCfgInfoN[8].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[9],
		WEB_N_PAGES_P10_NUM,
		&(pWebCfgInfoN[9].iNumber), 
		WEB_N_PAGES_P10,
		&(pWebCfgInfoN[9].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[10],
		WEB_N_PAGES_P11_NUM,
		&(pWebCfgInfoN[10].iNumber), 
		WEB_N_PAGES_P11,
		&(pWebCfgInfoN[10].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[11],
		WEB_N_PAGES_P12_NUM,
		&(pWebCfgInfoN[11].iNumber), 
		WEB_N_PAGES_P12,
		&(pWebCfgInfoN[11].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[12],
		WEB_N_PAGES_P13_NUM,
		&(pWebCfgInfoN[12].iNumber), 
		WEB_N_PAGES_P13,
		&(pWebCfgInfoN[12].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[13],
		WEB_N_PAGES_P14_NUM,
		&(pWebCfgInfoN[13].iNumber), 
		WEB_N_PAGES_P14,
		&(pWebCfgInfoN[13].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[14],
		WEB_N_PAGES_P15_NUM,
		&(pWebCfgInfoN[14].iNumber), 
		WEB_N_PAGES_P15,
		&(pWebCfgInfoN[14].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[15],
		WEB_N_PAGES_P16_NUM,
		&(pWebCfgInfoN[15].iNumber), 
		WEB_N_PAGES_P16,
		&(pWebCfgInfoN[15].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[16],
		WEB_N_PAGES_P17_NUM,
		&(pWebCfgInfoN[16].iNumber), 
		WEB_N_PAGES_P17,
		&(pWebCfgInfoN[16].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[17],
		WEB_N_PAGES_P18_NUM,
		&(pWebCfgInfoN[17].iNumber), 
		WEB_N_PAGES_P18,
		&(pWebCfgInfoN[17].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[18],
		WEB_N_PAGES_P19_NUM,
		&(pWebCfgInfoN[18].iNumber), 
		WEB_N_PAGES_P19,
		&(pWebCfgInfoN[18].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[19],
		WEB_N_PAGES_P20_NUM,
		&(pWebCfgInfoN[19].iNumber), 
		WEB_N_PAGES_P20,
		&(pWebCfgInfoN[19].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[20],
		WEB_N_PAGES_P21_NUM,
		&(pWebCfgInfoN[20].iNumber), 
		WEB_N_PAGES_P21,
		&(pWebCfgInfoN[20].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[21],
		WEB_N_PAGES_P22_NUM,
		&(pWebCfgInfoN[21].iNumber), 
		WEB_N_PAGES_P22,
		&(pWebCfgInfoN[21].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[22],
		WEB_N_PAGES_P23_NUM,
		&(pWebCfgInfoN[22].iNumber), 
		WEB_N_PAGES_P23,
		&(pWebCfgInfoN[22].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[23],
		WEB_N_PAGES_P24_NUM,
		&(pWebCfgInfoN[23].iNumber), 
		WEB_N_PAGES_P24,
		&(pWebCfgInfoN[23].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[24],
		WEB_N_PAGES_P25_NUM,
		&(pWebCfgInfoN[24].iNumber), 
		WEB_N_PAGES_P25,
		&(pWebCfgInfoN[24].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[25],
		WEB_N_PAGES_P26_NUM,
		&(pWebCfgInfoN[25].iNumber), 
		WEB_N_PAGES_P26,
		&(pWebCfgInfoN[25].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[26],
		WEB_N_PAGES_P27_NUM,
		&(pWebCfgInfoN[26].iNumber), 
		WEB_N_PAGES_P27,
		&(pWebCfgInfoN[26].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[27],
		WEB_N_PAGES_P28_NUM,
		&(pWebCfgInfoN[27].iNumber), 
		WEB_N_PAGES_P28,
		&(pWebCfgInfoN[27].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[28],
		WEB_N_PAGES_P29_NUM,
		&(pWebCfgInfoN[28].iNumber), 
		WEB_N_PAGES_P29,
		&(pWebCfgInfoN[28].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[29],
		WEB_N_PAGES_P30_NUM,
		&(pWebCfgInfoN[29].iNumber), 
		WEB_N_PAGES_P30,
		&(pWebCfgInfoN[29].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[30],
		WEB_N_PAGES_P31_NUM,
		&(pWebCfgInfoN[30].iNumber), 
		WEB_N_PAGES_P31,
		&(pWebCfgInfoN[30].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[31],
		WEB_N_PAGES_P32_NUM,
		&(pWebCfgInfoN[31].iNumber), 
		WEB_N_PAGES_P32,
		&(pWebCfgInfoN[31].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[32],
		WEB_N_PAGES_P33_NUM,
		&(pWebCfgInfoN[32].iNumber), 
		WEB_N_PAGES_P33,
		&(pWebCfgInfoN[32].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[33],
		WEB_N_PAGES_P34_NUM,
		&(pWebCfgInfoN[33].iNumber), 
		WEB_N_PAGES_P34,
		&(pWebCfgInfoN[33].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[34],
		WEB_N_PAGES_P35_NUM,
		&(pWebCfgInfoN[34].iNumber), 
		WEB_N_PAGES_P35,
		&(pWebCfgInfoN[34].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[35],
		WEB_N_PAGES_P36_NUM,
		&(pWebCfgInfoN[35].iNumber), 
		WEB_N_PAGES_P36,
		&(pWebCfgInfoN[35].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[36],
		WEB_N_PAGES_P37_NUM,
		&(pWebCfgInfoN[36].iNumber), 
		WEB_N_PAGES_P37,
		&(pWebCfgInfoN[36].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[37],
		WEB_N_PAGES_P38_NUM,
		&(pWebCfgInfoN[37].iNumber), 
		WEB_N_PAGES_P38,
		&(pWebCfgInfoN[37].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[38],
		WEB_N_PAGES_P39_NUM,
		&(pWebCfgInfoN[38].iNumber), 
		WEB_N_PAGES_P39,
		&(pWebCfgInfoN[38].stWebPrivate), 
		ParsedWebLangFileTableProc);

	DEF_LOADER_ITEM(&loader[39],
		WEB_N_PAGES_P40_NUM,
		&(pWebCfgInfoN[39].iNumber), 
		WEB_N_PAGES_P40,
		&(pWebCfgInfoN[39].stWebPrivate), 
		ParsedWebLangFileTableProc);


	DEF_LOADER_ITEM(&loader[40],
		WEB_N_PAGES_P41_NUM,
		&(pWebCfgInfoN[40].iNumber), 
		WEB_N_PAGES_P41,
		&(pWebCfgInfoN[40].stWebPrivate), 
		ParsedWebLangFileTableProc);


	if (Cfg_LoadTables(pCfg, WEB_MAX_HTML_PAGE_NUM_NETSCAPE, loader) != ERR_CFG_OK)
	{ 
		TRACE("Fail to Cfg_LoadTables\n");
		return ERR_LCD_FAILURE;
	}


	TRACE(" Web_Netscape_LoadWebPrivateConfigProc OK \n");
	return ERR_CFG_OK;
}
/*==========================================================================*
* FUNCTION :    MakeFilePathN(IN char *szFileName, IN int iLanguage)
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static char *MakeFilePathN(IN char *szFileName, IN int iLanguage)
{
	char			*szReturnFile = NULL;

	ASSERT(szFileName);

	szReturnFile = NEW(char, 128);
	if(szReturnFile == NULL)
	{
		return NULL;
	}

	if(iLanguage ==0)			//Eng
	{

		sprintf(szReturnFile,"%s/%s", WEB_DEFAULT_DIR_N, szFileName);
	}
	else if(iLanguage ==1)						//Template
	{
		sprintf(szReturnFile,"%s/%s", WEB_TEMPLATE_DIR_N, szFileName);
	}
	return szReturnFile;
}


/*==========================================================================*
* FUNCTION :    Web_Netscape_CreatePage()
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int Web_Netscape_CreatePage()
{
	char		*szFile = NULL;
	char		*szMakeVar = NULL;
	int iError;
	int iSignalType;
	int iSignalID;
	int iVarSubID;
	int iEquipID;
	int iLangType;
	int iReturn1;
	char *pHtml1;
	int i,k;
	FILE *fp1;


	SIG_BASIC_VALUE *pCurSigValue;

	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 182;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	iError += DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pCurSigValue,			
		0);
	iLangType = pCurSigValue->varValue.enumValue;

	for(i = 0; i < WEB_MAX_HTML_PAGE_NUM_NETSCAPE; i++)
	{


		szFile = MakeFilePathN(pWebCfgInfoN[i].szFileName, 1);
		//TRACE("********************[%d][%s][%d]**********************************\n",i, szFile,pWebCfgInfo[i].iNumber);
		ASSERT(szFile);
		if(szFile != NULL)
		{
			iReturn1 = LoadHtmlFile(szFile, &pHtml1);

			DELETE(szFile);
			szFile = NULL;
		}

		if(iReturn1 > 0)
		{
			////TRACE("*****%d*****pWebCfgInfo[i].iNumber : %d\n",i, pWebCfgInfo[i].iNumber);
			for(k = 0; k < pWebCfgInfoN[i].iNumber; k++)
			{
				////TRACE("szResourceID : %s\n", MakeVarField(pWebCfgInfo[i].stWebPrivate[k].szResourceID));

				if(iLangType == 0)
				{
					/*Eng*/
					szMakeVar = MakeVarField(pWebCfgInfoN[i].stWebPrivate[k].szResourceID);
					if(szMakeVar != NULL)
					{
						ReplaceString(&pHtml1, 
							szMakeVar,
							pWebCfgInfoN[i].stWebPrivate[k].szDefault);
						DELETE(szMakeVar);
						szMakeVar = NULL;
					}
				}
				else if(iLangType == 1)
				{
					/*Loc*/
					szMakeVar = MakeVarField(pWebCfgInfoN[i].stWebPrivate[k].szResourceID);
					if(szMakeVar != NULL)
					{
						ReplaceString(&pHtml1, 
							szMakeVar, 
							pWebCfgInfoN[i].stWebPrivate[k].szLocal);
						DELETE(szMakeVar);
						szMakeVar = NULL;
					}
				}



			}


		}
		//Save Eng File 
		szFile = MakeFilePathN(pWebCfgInfoN[i].szFileName, 0); 
		if(szFile != NULL && (fp1 = fopen(szFile,"wb")) != NULL &&
			pHtml1 != NULL)
		{

			fwrite(pHtml1,strlen(pHtml1), 1, fp1);
			fclose(fp1);

		}
		if(szFile != NULL)
		{
			DELETE(szFile);
			szFile = NULL;
		}


		if(pHtml1 != NULL)
		{
			DELETE(pHtml1);
			pHtml1 = NULL;
		}



		RunThread_Heartbeat(RunThread_GetId(NULL));

	}

	char	szCommand[128];
	sprintf(szCommand,"chmod 777 %s/*", WEB_DEFAULT_DIR_N);
	system(szCommand);

	Web_Free_ResourceN();

	return TRUE;
}


/*==========================================================================*
* FUNCTION :    Web_Netscape_ChangeFlage()
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int Web_Netscape_ChangeFlage()
{
	TRACE("INTO Web_Netscape_ChangeFlage()\n");
	int  iError;
	int iSignalType;
	int iSignalID;
	int iVarSubID;
	int iEquipID;
	SIG_BASIC_VALUE *pCurSigValue;
	SIG_BASIC_VALUE *pBefSigValue;
	VAR_VALUE_EX	value;
	int iResult;

	//TRACE("%d[%d,%d,%d]\n", i, iEquipID, iSignalID, iSignalType);

	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 182;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	iError += DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pCurSigValue,			
		0);

	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 181;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	iError += DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pBefSigValue,			
		0);

	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.pszSenderName = "web_lang_control";
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;

	value.varValue.enumValue = pCurSigValue->varValue.enumValue;

	TRACE("pCurSigValue :%d\n",pCurSigValue->varValue.enumValue);
	TRACE("pBefSigValue :%d\n",pBefSigValue->varValue.enumValue);


	//iEquipID = 1;
	//iSignalType = 2;
	//iSignalID = 44;

	if(pBefSigValue->varValue.enumValue != pCurSigValue->varValue.enumValue)
	{
		Web_ModifySignalValue(iEquipID,iSignalType,iSignalID,value);

	}

	TRACE("pCurSigValue :%d\n",pCurSigValue->varValue.enumValue);
	TRACE("pBefSigValue :%d\n",pBefSigValue->varValue.enumValue);

	if(stWebCompareVersionN.fVersion > stWebVersionInfoN.fVersion  ||
		strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersionN.szLanguage),Cfg_RemoveWhiteSpace(stWebVersionInfoN.szLanguage))!=0 ||
		iStartOK != 1)
	{
		Web_WriteVersionOfWebPages_N(stWebCompareVersionN.fVersion,stWebCompareVersionN.szLanguage,iStartOK);

	}

	return TRUE;
}
/*==========================================================================*
* FUNCTION :    Web_Free_ResourceN()
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int Web_Free_ResourceN()
{
	int i,k;
	WEB_PRIVATE_RESOURCE_INFO	*pDelete = (WEB_PRIVATE_RESOURCE_INFO *)pWebCfgInfoN;
	WEB_GENERAL_RESOURCE_INFO	*pDeleteWebPrivate = NULL;
	for(i = 0; i < WEB_MAX_HTML_PAGE_NUM_NETSCAPE && pDelete != NULL; i++, pDelete++)
	{
		////TRACE("*******%d*********\n", i);
		pDeleteWebPrivate = pDelete->stWebPrivate;
		for(k = 0; k < pDelete->iNumber && pDelete->stWebPrivate != NULL; k++, pDelete->stWebPrivate++)
		{
			if(pDelete->stWebPrivate != NULL)
			{
				if(pDelete->stWebPrivate->szDefault != NULL)
				{
					DELETE(pDelete->stWebPrivate->szDefault);
					pDelete->stWebPrivate->szDefault = NULL;
				}

				if(pDelete->stWebPrivate->szLocal != NULL)
				{
					DELETE(pDelete->stWebPrivate->szLocal);
					pDelete->stWebPrivate->szLocal = NULL;
				}


			}
		}
		if(pDeleteWebPrivate != NULL)
		{
			DELETE(pDeleteWebPrivate);
			pDeleteWebPrivate = NULL;
		}
	}
	if(pWebCfgInfoN != NULL)
	{
		DELETE(pWebCfgInfoN);
		pWebCfgInfoN = NULL;
	}
}


/*==========================================================================*
* FUNCTION :    Web_WriteVersionOfWebPages_N(IN double fVersion, IN char *szLanguage)
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int Web_WriteVersionOfWebPages_N(IN double fVersion, IN char *szLanguage,IN int iStartFlage)
{
	TRACE("INTO Web_WriteVersionOfWebPages_N()\n");
	char			*pSearchValue = NULL, *str_string = NULL, szReadString[128];
	FILE			*fp;
#define FILE_CFG_VERSION			"[LOCAL_LANGUAGE_VERSION]"
#define FILE_CFG_LANGUAGE			"[LOCAL_LANGUAGE]"
#define MAX_READ_LINE_LEN			129

	if((fp = fopen(WEB_NETSCAPE_PAGES_VERSION_FILE, "rb+")) != NULL)
	{
		while(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
		{
			if((pSearchValue = strstr(szReadString,FILE_CFG_LANGUAGE)) != NULL && 
				strcmp(Cfg_RemoveWhiteSpace(stWebCompareVersionN.szLanguage),Cfg_RemoveWhiteSpace(stWebVersionInfoN.szLanguage))!=0)
			{
				if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
				{
					fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
					if(szLanguage != NULL)
					{
						str_string = NEW(char , strlen(szReadString) + 1);
						memset(str_string,0, 4);
						sprintf(str_string,"%3s",Cfg_RemoveWhiteSpace(szLanguage));
						//TRACE("szLanguage[%s]\n", szLanguage);
						//strncpyz(str_string,Cfg_RemoveWhiteSpace(szLanguage),strlen(szReadString) + 1);
						fputs(str_string, fp);

						DELETE(str_string);
						str_string = NULL;
					}

				}
			}
			else if((pSearchValue = strstr(szReadString,FILE_CFG_VERSION)) != NULL &&
				stWebCompareVersionN.fVersion > stWebVersionInfoN.fVersion)
			{
				if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
				{
					fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
					str_string = NEW(char, 128);
					sprintf(str_string,"%8.2f",fVersion);
					fputs(str_string, fp);

					DELETE(str_string);
					str_string = NULL;
				}


			}
			else if((pSearchValue = strstr(szReadString,START_OK)) != NULL && 
				iStartOK !=1)
			{
				TRACE("Change iStartOk!\n");
				iStartOK = 1;
				if(fgets(szReadString, MAX_READ_LINE_LEN, fp) != NULL)
				{
					fseek(fp,(long)-strlen(szReadString),SEEK_CUR);
					str_string = NEW(char, 128);
					sprintf(str_string,"%d",iStartOK);
					fputs(str_string, fp);

					DELETE(str_string);
					str_string = NULL;
				}


			}


		}
		fclose(fp);
	}
	return TRUE;
}
/*==========================================================================*
* FUNCTION :    Web_SetCurrentLangValue(IN int iLanguage)
* PURPOSE  :	Netscape_Multi_Language_Support
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
char *filename:
* RETURN   :  
* COMMENTS : 
* CREATOR  : WangJing               DATE: 2007-04-18 11:20
*==========================================================================*/
static int Web_SetCurrentLangValue(IN char *ptrBuf)
{
	int iSignalType;
	int iSignalID;
	int iEquipID;

	VAR_VALUE_EX	value;


	iEquipID = 1;
	iSignalType = 2;
	iSignalID = 182;

	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.pszSenderName = "web_lang_control";
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;

	value.varValue.enumValue = atoi(ptrBuf);//iLanguage;
	//printf("ptrBuf = %s \n", ptrBuf);

	if(Web_ModifySignalValue(iEquipID,iSignalType,iSignalID,value))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
static int Web_GetCurrentLangValue(void)
{
	SIG_BASIC_VALUE *pBefSigValue;


	int iEquipID = 1;
	int iSignalType = 2;
	int iSignalID = 181;
	int iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	int iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pBefSigValue,			
		0);


	return pBefSigValue->varValue.enumValue;

}

static int Web_GetNowLangValue(void)
{
	SIG_BASIC_VALUE *pCulSigValue;


	int iEquipID = 1;
	int iSignalType = 2;
	int iSignalID = 182;
	int iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	int iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		&iBufLen,			
		(void *)&pCulSigValue,			
		0);


	return pCulSigValue->varValue.enumValue;

}


//////////////////////////////////////////////////////////////////////////


static int Web_GetESRModifyInfo_net(IN char *szBuffer, OUT COMMON_CONFIG *stCommonConfig, IN int iCommandType)
{
#define NET_EEM_CHANGE_GENERAL			(ESR_CFG_W_MODE | ESR_CFG_REPORT_IN_USE | ESR_CFG_CALLBACK_IN_USE \ 
	| ESR_CFG_PROTOCOL_TYPE \ 
		| ESR_CFG_MEDIA_TYPE \ 
		| ESR_CFG_CCID \ 
		| ESR_CFG_SOCID \ 
		| ESR_CFG_MAX_ATTEMPTS  \
		| ESR_CFG_ATTEMPT_ELAPSE \ 
		| ESR_CFG_SECURITY_LEVEL \
		| ESR_CFG_MEDIA_PORT_PARAM)

#define NET_EEM_CHANGE_LEASEDLINE		(ESR_CFG_W_MODE | ESR_CFG_MEDIA_PORT_PARAM | ESR_CFG_MEDIA_TYPE)

#define NET_EEM_CHANGE_PSTN				(ESR_CFG_W_MODE| ESR_CFG_REPORT_NUMBER_1 \
	| ESR_CFG_REPORT_NUMBER_2 \
	| ESR_CFG_CALLBACK_NUMBER \
	| ESR_CFG_MEDIA_TYPE \
	| ESR_CFG_MEDIA_PORT_PARAM)

#define NET_EEM_CHANGE_TCPIP			(ESR_CFG_W_MODE | ESR_CFG_IPADDR_1 \
	| ESR_CFG_IPADDR_2 \
	| ESR_CFG_SECURITY_IP_1 \
	| ESR_CFG_SECURITY_IP_2 \
	| ESR_CFG_MEDIA_TYPE \
	| ESR_CFG_MEDIA_PORT_PARAM)

		ASSERT(szBuffer);
	ASSERT(stCommonConfig);

	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	char			*pTrim = NULL;
	int				iValdateModify = 0;

	//printf("szBuffer={%s} %d \n", szBuffer,iCommandType);

	ptr = szBuffer;
	ptr = ptr + 1;

	if(iCommandType == NET_EEM_GENEARL)
	{
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stCommonConfig->bReportInUse = atoi(szExchange) ;
				//printf("stCommonConfig->bReportInUse = [%d]\n", stCommonConfig->bReportInUse);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1); 
				stCommonConfig->bCallbackInUse = atoi(szExchange) ;
				//printf("stCommonConfig->bCallbackInUse = [%d]\n", stCommonConfig->bCallbackInUse);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stCommonConfig->iProtocolType = atoi(szExchange) ;
				//printf("stCommonConfig->iProtocolType = [%d]\n", stCommonConfig->iProtocolType);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stCommonConfig->iMediaType = atoi(szExchange) ;
				if(stCommonConfig->iMediaType == 0) //Leased line
				{
					sprintf(stCommonConfig->szCommPortParam,"9600,n,8,1",sizeof(stCommonConfig->szCommPortParam));
				}
				else if(stCommonConfig->iMediaType == 1)//Modem
				{
					sprintf(stCommonConfig->szCommPortParam,"9600,n,8,1:1000",sizeof(stCommonConfig->szCommPortParam));
				}
				else//TCPIP
				{
					sprintf(stCommonConfig->szCommPortParam,"2000",sizeof(stCommonConfig->szCommPortParam));
				}

				//printf("stCommonConfig->iMediaType = [%d]\n", stCommonConfig->iMediaType);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stCommonConfig->byCCID = (BYTE)atoi(szExchange) ;
				//printf("stCommonConfig->byCCID = [%d]\n", stCommonConfig->byCCID);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stCommonConfig->iSOCID = atoi(szExchange) ;
				//printf("stCommonConfig->iSOCID = [%d]\n", stCommonConfig->iSOCID);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stCommonConfig->iMaxAttempts = atoi(szExchange) ;
				//printf("stCommonConfig->iMaxAttempts = [%d]\n", stCommonConfig->iMaxAttempts);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stCommonConfig->iAttemptElapse = 1000 * atoi(szExchange) ;
				//printf("stCommonConfig->iAttemptElapse = [%d]\n", stCommonConfig->iAttemptElapse);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;


		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				stCommonConfig->iSecurityLevel = atoi(szExchange) ;
				//printf("stCommonConfig->iSecurityLevel = [%d]\n", stCommonConfig->iSecurityLevel);
				ptr = ptr + iPosition;

			}

		}
		ptr = ptr + 1;

		return NET_EEM_CHANGE_GENERAL;
	}
	else if(iCommandType == NET_EEM_LEASDLINE)
	{
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(stCommonConfig->szCommPortParam, ptr, iPosition + 1);
				//				strncpyz(stCommonConfig->szCommPortParam, "19200,n,8,1", sizeof(stCommonConfig->szCommPortParam));
				stCommonConfig->iMediaType = 0 ;//Leased line
				//printf("stCommonConfig->szCommPortParam = %s\n", stCommonConfig->szCommPortParam);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;
		return NET_EEM_CHANGE_LEASEDLINE;
	}
	else if(iCommandType == NET_EEM_PSTN)
	{
		stCommonConfig->iMediaType = 1 ;//Modem
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				strncpyz(stCommonConfig->szAlarmReportPhoneNumber[0], ptr, iPosition + 1);
				//printf("stCommonConfig->szAlarmReportPhoneNumber[0] = [%s]\n", stCommonConfig->szAlarmReportPhoneNumber[0]);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				strncpyz(stCommonConfig->szAlarmReportPhoneNumber[1], ptr, iPosition + 1);
				//printf("stCommonConfig->szAlarmReportPhoneNumber[1] = [%s]\n", stCommonConfig->szAlarmReportPhoneNumber[1]);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;


		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				strncpyz(stCommonConfig->szCallbackPhoneNumber[0], ptr, iPosition + 1);
				//printf("stCommonConfig->szCallbackPhoneNumber[0] = [%s]\n", stCommonConfig->szCallbackPhoneNumber[0]);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(stCommonConfig->szCommPortParam, ptr, iPosition + 1);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;
		return NET_EEM_CHANGE_PSTN;
	}
	else if(iCommandType == NET_EEM_TCPIP)
	{
		stCommonConfig->iMediaType = 2;//TCPIP
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(stCommonConfig->szReportIP[0], ptr, iPosition + 1);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(stCommonConfig->szReportIP[1], ptr, iPosition + 1);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(stCommonConfig->szSecurityIP[0], ptr, iPosition + 1);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(stCommonConfig->szSecurityIP[1], ptr, iPosition + 1);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		//Incoming port
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(stCommonConfig->szCommPortParam, ptr, iPosition + 1);
				ptr = ptr + iPosition;
			}

		}
		ptr = ptr + 1;

		return NET_EEM_CHANGE_TCPIP;
	}

	return FALSE;

}


static int Web_SetACUTime_net(IN void *pModiyValue)
{
	int		iError,iBufLen;

	char	szExchange[33];
	time_t	tmModiy;
	char	*ptr = NULL;
	struct tm	tmReturn;

	//printf("pModiyValue= %s\n", pModiyValue);
	ptr = (char *)pModiyValue;
	strncpyz(szExchange, ptr, sizeof(szExchange));
	sscanf(szExchange,"%d-%d-%d %d:%d:%d", &tmReturn.tm_year,&tmReturn.tm_mon,&tmReturn.tm_mday, &tmReturn.tm_hour,&tmReturn.tm_min, &tmReturn.tm_sec);
	//printf("%d-%d-%d %d:%d:%d\n",tmReturn.tm_year,tmReturn.tm_mon,tmReturn.tm_mday, tmReturn.tm_hour,tmReturn.tm_min, tmReturn.tm_sec);
	if(tmReturn.tm_sec > 59)
	{
		tmReturn.tm_sec = 59;
	}
	if(tmReturn.tm_min > 59)
	{
		tmReturn.tm_min = 59;
	}
	if(tmReturn.tm_hour > 23)
	{
		tmReturn.tm_hour = 23;
	}

	tmReturn.tm_year	= tmReturn.tm_year - TIME_LINUX_YEAR;
	tmReturn.tm_mon		= tmReturn.tm_mon - TIME_LINUX_MONTH;

	tmReturn.tm_wday	= 0;
	tmReturn.tm_yday	= 0;
	tmReturn.tm_isdst	= -1;

	tmModiy = mktime(&tmReturn);

	iBufLen = sizeof(time_t);
	iError = DxiSetData(VAR_TIME_SERVER_INFO,
		SYSTEM_TIME,			
		0,		
		iBufLen,			
		(void *)&tmModiy,			
		0);

	if (iError == ERR_DXI_OK)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static int Web_Recover(IN char *ptr)
{
	//printf("%s\n", ptr);

	if(atoi(ptr) == 2)//restart
	{
		DXI_RebootACUorSCU(TRUE,TRUE);
	}
	else if(atoi(ptr) == 1)//recover setting
	{
		DXI_ReloadDefaultConfig();

	}
	return TRUE;
}

///*==========================================================================*
//* FUNCTION :  Web_AJAX_TEST()
//* PURPOSE  :  
//* CALLS    : 
//* CALLED BY: 
//* ARGUMENTS:  
//* RETURN   :  
//* COMMENTS : 
//* CREATOR  : Wang Jing              DATE: 2006-11-23 11:08
//*==========================================================================*/
//static int Web_AJAX_TEST(void)
//{
//#define MAX_XML_OF_A_EQUIP	100
//#define MAX_NUMBER_SIGNAL	100
//#define MAX_LENTH_SIGNAL	300
//#define EQUIP_LIST_XML_PATH	"/var/EquipList.xml"
//
//	TRACE("\n Start AJAX TEST! \n");
//
//	EQUIP_INFO	*pEquip = NULL;
//	int		iEquipNum = 0;
//	char		*pResult = NULL;
//	int		iLen = 0;
//	int		i = 0;
//	int		j = 0;
//	EQUIP_INFO	*pEquipTemp = NULL;
//	EQUIP_INFO	*pEquipTemp2 = NULL;
//	char *szFullPath = EQUIP_LIST_XML_PATH;
//	FILE		*pFile = NULL;
//	SAMPLE_SIG_VALUE	*pSamplerSig = NULL;
//	CTRL_SIG_VALUE		*pControlSig = NULL;
//	char		szEquipXMLName[64];
//	char		szTime1[32];
//
//
//
//	//get equip information
//	int iError1 = DxiGetData(VAR_ACU_EQUIPS_LIST,
//		0,			
//		0,		
//		&iBufLen,			
//		&pEquip,			
//		0);
//
//	//get equip number
//	int iError2 = DxiGetData(VAR_ACU_EQUIPS_NUM,
//		0,			
//		0,		
//		&iBufLen,			
//		(void *)&iEquipNum,			
//		0);
//
//	pResult = NEW(char, MAX_XML_OF_A_EQUIP * iEquipNum + 300);
//
//	iLen  +=  sprintf(pResult + iLen,"<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<!--\nl1:EquipID;\nl2:EquipNameEng;\nl3:EquipNameLoc;\nl4:Equip;\nl5:SubEquip;\nl6:GroupID;\nl7:TypeID;\nl8:SubEquipNameEng;\nl9:SubEquipNameLoc;\n-->\n<EquipList>");
//
//	pEquipTemp = pEquip;
//	for(i = 0;i < iEquipNum; i++,pEquipTemp++)
//	{
//		if(pEquipTemp->pStdEquip->bGroupType == TRUE && pEquipTemp->bWorkStatus	==TRUE)
//		{
//			iLen  +=  sprintf(pResult + iLen,"\n<l4>\n<l1>%d</l1>\n<l2>%s</l2>\n<l6>%d</l6>",pEquipTemp->iEquipID,pEquipTemp->pEquipName->pFullName[0],pEquipTemp->pStdEquip->iTypeID);
//			pEquipTemp2 = pEquip;
//
//			for(j = 0;j < iEquipNum; j++,pEquipTemp2++)
//			{
//				if(pEquipTemp2->iEquipTypeID/100 == pEquipTemp->iEquipTypeID/100 && pEquipTemp2->bWorkStatus ==TRUE && pEquipTemp2->pStdEquip->bGroupType != TRUE)
//				{
//					iLen  +=  sprintf(pResult + iLen,"\n<l5>\n<l8>%s</l8>\n<l7>%d</l7>\n</l5>",pEquipTemp2->pEquipName->pFullName[0],pEquipTemp->iEquipTypeID);
//				}
//			}
//
//			iLen  +=  sprintf(pResult + iLen,"\n</l4>");
//
//		}
//
//	}
//
//	iLen  +=  sprintf(pResult + iLen,"\n</EquipList>");
//
//
//	pFile = fopen(szFullPath, "w+");
//
//
//	fwrite(pResult,strlen(pResult),1,pFile);
//
//	fclose(pFile);
//
//	SAFELY_DELETE(pResult);
//
//	RunThread_Heartbeat(RunThread_GetId(NULL));
//
//	pResult = NEW(char, MAX_NUMBER_SIGNAL * MAX_LENTH_SIGNAL + 1000);
//	iLen = 0;
//
//
//	pEquipTemp = pEquip;
//	for(i = 0;i < iEquipNum; i++,pEquipTemp++)
//	{
//		//TRACE("\nEquip Number:%d\n",i);
//		if(pEquipTemp->bWorkStatus == TRUE)
//		{
//			iLen  +=  sprintf(pResult + iLen,"<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<Equip>\n<e22>");
//			pSamplerSig = pEquipTemp->pSampleSigValue;
//			//TRACE("\nStep 1\n");
//			for(j = 0;j < pEquipTemp->pStdEquip->iSampleSigNum;j++,pSamplerSig++)
//			{
//				iLen  +=  sprintf(pResult + iLen,"\n<e4>\n<e5>%d</e5>\n<e6>%s</e6>\n<e8>%s</e8>",pSamplerSig->pStdSig->iSigID,pSamplerSig->pStdSig->pSigName->pFullName[0],pSamplerSig->pStdSig->szSigUnit  );
//				//TRACE("\nStep 2\n");
//				if(pSamplerSig->pStdSig->iSigValueType == 1)
//				{
//					iLen  +=  sprintf(pResult + iLen,"\n<e9>%ld</e9>",pSamplerSig->bv.varValue.lValue );
//					//TRACE("\nStep 3\n");
//
//				}
//				else if(pSamplerSig->pStdSig->iSigValueType == 3)
//				{
//					iLen  +=  sprintf(pResult + iLen,"\n<e9>%ld</e9>",pSamplerSig->bv.varValue.ulValue );
//					//TRACE("\nStep 4\n");
//				}
//				else if(pSamplerSig->pStdSig->iSigValueType == 2)
//				{
//					iLen  +=  sprintf(pResult + iLen,"\n<e9>%f</e9>",pSamplerSig->bv.varValue.fValue );
//					//TRACE("\nStep 5\n");
//				}
//				else if(pSamplerSig->pStdSig->iSigValueType == 5)
//				{
//					iLen  +=  sprintf(pResult + iLen,"\n<e9>%s</e9>",pSamplerSig->pStdSig->pStateText[pSamplerSig->bv.varValue.enumValue]->pFullName[0] );
//					//TRACE("\nStep 6\n");
//				}
//				else if(pSamplerSig->pStdSig->iSigValueType == 4)
//				{
//					memset(szTime1,0,32);
//					TimeToString(pSamplerSig->bv.varValue.dtValue, TIME_CHN_FMT, 
//						szTime1, sizeof(szTime1));
//					iLen  +=  sprintf(pResult + iLen,"\n<e9>%s</e9>",szTime1 );
//					//TRACE("\nStep 7\n");
//				}
//
//				memset(szTime1,0,32);
//				TimeToString(pSamplerSig->bv.tmCurrentSampled, TIME_CHN_FMT, 
//					szTime1, sizeof(szTime1));
//				iLen  +=  sprintf(pResult + iLen,"\n<e10>%s</e10>\n</e4>",szTime1 );
//				//TRACE("\nStep 8\n");
//
//			}
//
//			iLen  +=  sprintf(pResult + iLen,"\n</e22>\n</Equip>");
//
//
//			//TRACE("\nStep 9\n");
//			memset(szEquipXMLName, 0, 64);
//			sprintf(szEquipXMLName,"/var/%s.xml",pEquipTemp->pEquipName->pFullName[0]);
//
//			//TRACE("\nStep 10\n");
//			pFile = fopen(szEquipXMLName, "w+");
//
//
//			fwrite(pResult,strlen(pResult),1,pFile);
//
//			fclose(pFile);
//			//TRACE("\nStep 11\n");
//
//		}
//
//		memset(pResult, 0, MAX_NUMBER_SIGNAL * MAX_LENTH_SIGNAL + 1000);
//		iLen = 0;
//
//		RunThread_Heartbeat(RunThread_GetId(NULL));
//
//
//	}
//
//
//
//	SAFELY_DELETE(pResult);
//
//
//	Sleep(500);
//
//	return TRUE;
//
//
//}



/*==========================================================================*
 * FUNCTION :  Web_ModifyNMSV3PrivateConfigure
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *szNmsBuffer:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static int Web_ModifyNMSV3PrivateConfigure(IN char *szNmsBuffer)
{
#define ADD_TYPE_OF_NMS					1
#define MODIFY_TYPE_OF_NMS				2
#define DELETE_TYPE_OF_NMS				3
#define CHANGETRAP_TYPE_OF_NMS			4

	ASSERT(szNmsBuffer);
	V3NMS_INFO		stNmsInfo;
	APP_SERVICE		*stAppService = NULL;
	int				iBufLen = sizeof(NMS_INFO);
	char			*ptr = NULL;
	char			szExchange[64], *pSearchValue = NULL;
	int				iPosition = 0;
	int				iModifyType = 0;
	int				iReturn = 0;
	char			*szTrim = NULL;

//added by YangGuoxin,3/20/2007
	int				iAllBufLen = 1;
	V3NMS_INFO		*stAllNmsInfo = NULL;
	int				i = 0, iNMSNum = 0;
//end by YangGuoxin, 3/20/2007
	
	//�ֽⷢ�͹����Ļ�����
	printf("szNmsBuffer : %s\n", szNmsBuffer);
	if((ptr = szNmsBuffer) != NULL)
	{

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				iModifyType = atoi(szExchange);
				ptr = ptr + iPosition;
			}
			else
			{
				return FALSE;
			}
			
		}
		printf("1--szExchange : %s\n", szExchange);
		ptr = ptr + 1;
		
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				szTrim = Cfg_RemoveWhiteSpace(szExchange);
				sprintf(stNmsInfo.szUserName, szTrim, sizeof(stNmsInfo.szUserName));		

				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;

		printf("2--szExchange : %s\n", szExchange);
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				char		*szIP = NULL;
				szIP = Cfg_RemoveWhiteSpace(szExchange + 1);
				stNmsInfo.ulTrapIpAddress = inet_addr(szIP);

				ptr = ptr + iPosition;
			}
			
		}
printf("3--szExchange : %s\n", szExchange);
		ptr = ptr + 1;

		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				szTrim = Cfg_RemoveWhiteSpace(szExchange);
				sprintf(stNmsInfo.szPrivPwd, szTrim, sizeof(stNmsInfo.szPrivPwd));
				//TRACE("szPublicCommunity : %s\n", stNmsInfo.szPublicCommunity);
				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;
printf("4--szExchange : %s\n", szExchange);
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				szTrim = Cfg_RemoveWhiteSpace(szExchange);
				sprintf(stNmsInfo.szAuthPwd, szTrim, sizeof(stNmsInfo.szAuthPwd));
				ptr = ptr + iPosition;
			}
			
		}
		ptr = ptr + 1;

printf("5--szExchange : %s\n", szExchange);
		pSearchValue = strchr(ptr, 59);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - ptr;
			if(iPosition > 0)
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				szTrim = Cfg_RemoveWhiteSpace(szExchange);
				sprintf(stNmsInfo.nTrapSecurityLevel, szTrim, sizeof(stNmsInfo.nTrapSecurityLevel));
				stNmsInfo.nTrapSecurityLevel = atoi(szExchange) - 1;
				ptr = ptr + iPosition;			
			}
			
		}
		//ptr = ptr + 1;
printf("6--szExchange : %s\n", szExchange);
		


	}
	else
	{
		return FALSE;
	}



	if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV2_NAME);
	}
	else
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
	}
	printf("\n iModifyType = %d[%d] [%d]\n",iModifyType, stAppService != NULL, stAppService->pfnServiceConfig != NULL);
	//stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMS_NAME);
	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{
//added by YangGuoxin,3/20/2007
		if(iModifyType == DELETE_TYPE_OF_NMS)// delete
		{
			iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										DELETE_NMS_V3_USER_INFO,
										&iBufLen,
										(void *)&stNmsInfo);// == ERR_SNP_OK)
			printf("DELETE_TYPE_OF_NMS iReturn = %d\n", iReturn);		
		}
		else if(iModifyType == MODIFY_TYPE_OF_NMS )//modify 
		{
			if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
											stAppService->bServiceRunning,
											&stAppService->args, 
											GET_NMS_V3_USER_INFO,
											&iAllBufLen,
											(void *)&stAllNmsInfo) == ERR_SNP_OK)
			{
				iNMSNum =  iAllBufLen /sizeof(NMS_INFO);

				for(i = 0; i < iNMSNum && stAllNmsInfo != NULL; i++, stAllNmsInfo++)
				{
					printf("stAllNmsInfo->szUserName = %s,stNmsInfo.szUserName= %s\n", stAllNmsInfo->szUserName,stNmsInfo.szUserName);
					if(strcmp(stAllNmsInfo->szUserName,stNmsInfo.szUserName))
					{
						iReturn += stAppService->pfnServiceConfig(stAppService->hServiceThread,
											stAppService->bServiceRunning,
											&stAppService->args, 
											MODIFY_NMS_V3_USER_INFO,
											&iBufLen,
											(void *)&stNmsInfo);
						printf("MODIFY_TYPE_OF_NMS iReturn = %d\n", iReturn);		
					}
				}

			}
			 
		} 
		else if(iModifyType == ADD_TYPE_OF_NMS)//add 
		{
			
			iReturn = stAppService->pfnServiceConfig(stAppService->hServiceThread,
								stAppService->bServiceRunning,
								&stAppService->args, 
								ADD_NMS_V3_USER_INFO,
								&iBufLen,
								(void *)&stNmsInfo);// == ERR_SNP_OK)	
			printf("ADD_TYPE_OF_NMS iReturn = %d\n", iReturn);		
		}

		 
//end by YangGuoxin, 3/20/2007

		if(iReturn == ERR_SNP_OK)
		{
			return TRUE;//iReturn;
		}
		else if(iReturn == ERR_NMS_TOO_MANY_USERS)
		{
			return 6;
		}
		else
		{
			return FALSE;
		}

	}
	return -1;
}


/*==========================================================================*
 * FUNCTION :  Web_MakeNMSPrivateConfigureBuffer
 * PURPOSE  :	 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:		
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2005-1-02 11:20
 *==========================================================================*/
static char *Web_MakeNMSV3PrivateConfigureBuffer(void)
{
	APP_SERVICE		*stAppService = NULL;
	int				iBufLen = 1;
	V3NMS_INFO		*stNmsInfo = NULL;
	char			*szNmsInfo = NULL;
	struct in_addr	inIP;
	int				i = 0, iLen = 0;
	int				iNMSNum = 0;

	//stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,(void *)GET_SERVICE_OF_NMS_NAME);
	if(g_SiteInfo.iSNMPV3Flag == NO_SNMPV3)
	{
	    //stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV2_NAME);
		return NULL;
	}
	else
	{
	    stAppService = ServiceManager_GetService(SERVICE_GET_BY_LIB, (void *)GET_SERVICE_OF_NMSV3_NAME);
	}

	//TRACE("**********stAppService \n");

	if(stAppService != NULL && stAppService->pfnServiceConfig != NULL)
	{
		//TRACE("**********stAppService->pfnServiceConfig : TRUE\n");

		if(stAppService->pfnServiceConfig(stAppService->hServiceThread,
										stAppService->bServiceRunning,
										&stAppService->args, 
										GET_NMS_V3_USER_INFO,
										&iBufLen,
										(void *)&stNmsInfo) == ERR_SNP_OK)
		{
			//print serviceconfig
			
			iNMSNum =  iBufLen /sizeof(V3NMS_INFO);
			szNmsInfo = NEW(char, 200 * iNMSNum);
			//TRACE("**********stAppService->pfnServiceConfig : TRUE  iBufLen : %d %d\n", iBufLen, iNMSNum);
			for(i = 0; i < iNMSNum && stNmsInfo != NULL; i++, stNmsInfo++)
			{

				inIP.s_addr = stNmsInfo->ulTrapIpAddress; 
				//memcpy(&inIP.s_addr, &stNmsInfo->ulIpAddress, sizeof(ULONG));
				iLen += sprintf(szNmsInfo + iLen,"%d,\"%s\",\" %s \",%d,\"%s\",\"%s\",",  
											i,
											stNmsInfo->szUserName,
											inet_ntoa(inIP),
											stNmsInfo->nTrapSecurityLevel,
											stNmsInfo->szPrivPwd,
											stNmsInfo->szAuthPwd
											);
			
			}
			if(iLen >= 1)
			{
				*(szNmsInfo + iLen - 1) = 32;
			}
			else
			{
				DELETE(szNmsInfo);
				szNmsInfo = NULL;

			}
			printf("szNmsInfo : %s\n", szNmsInfo);
			return szNmsInfo;

		}
	}
	//TRACE("**********stAppService->pfnServiceConfig : FALSE\n");
	return NULL;
}

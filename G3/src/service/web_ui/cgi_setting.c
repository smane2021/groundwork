/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define WEB_ADD_PRODUCT_INFO		1

#define CONFIGURE_OF_NETWORK		0
#define CONFIGURE_OF_NMS			1
#define CONFIGURE_OF_ESR			2
#define CONFIGURE_OF_USER			3
#define CONFIGURE_OF_TIME			4

#ifdef WEB_ADD_PRODUCT_INFO
	#define CONFIGURE_OF_PRODUCTINFO	5
#endif

//////////////////////////////////////////////////////////////////////////
//Added by wj for Config PLC private config file 2006.5.13

#define CONFIGURE_OF_PLC            6
#define CONFIGURE_OF_ALARMS         7
#define CONFIGURE_OF_ALARMREG       8
#define CONFIGURE_OF_YDN            9
#define CONFIGURE_OF_GCPS           10
#define CONFIGURE_OF_SETTINGPARAM   11
#define CONFIGURE_OF_AUTOCONFIG     12
//For Config PLC
#define CFG_PLC_ADD                     "addRowContent"
#define CFG_PLC_DEL                     "deleteRowId"

//For Config Alarm Suppressing Exp
#define CFG_ALARM_EQUIPTYPEID           "_stdEquipTypeId"
#define CFG_ALARM_ALARMID               "_alarmId"
#define CFG_ALARM_ALARMSUPEXP           "_alarmSuppressExp"

//For Config Alarm Reg
#define CFG_ALARM_ALARMREG           "_alarmReg"

//end////////////////////////////////////////////////////////////////////////

/*For network configure*/
#define CGI_CLIENT_IP_NAME				"_ip"
#define	CGI_CLIENT_MASK_NAME			"_mask"
#define CGI_CLIENT_GATE_NAME			"_gate"

/*for user manage*/
#define CGI_USER_NAME					"_name"
#define CGI_USER_PASSWORD				"_password1"
#define CGI_USER_LEVEL					"_authority_level"

#define USER_INFO_LIST					"_user_list"

#define WEB_CGI_TIME_SERVER1			"_time_server1"
#define WEB_CGI_TIME_SERVER2			"_time_server2"
#define WEB_CGI_TIME_INTERVAL			"_time_interval"
#define WEB_CGI_TIME_ZONE			"_time_zone_new"

#define CGI_NMS_LIST					"_nms_list"
#define CGI_ESR_LIST					"_esr_list"

#define CGI_MODIFY_CONFIGURE			"_modify_configure"
#define CGI_MODIFY_CONFIGURE_DETAIL		"_modify_configure_detail"

/*for time*/
#define CGI_TIME_TYPE					"_time_type"
#define CGI_TIME_MAIN_IP				"_time_server1"
#define CGI_TIME_BAK_IP					"_time_server2"
#define CGI_TIME_INTERVAL				"_interval_adjust"
#define CGI_TIME_ZONE					"_time_zone"
#define CGI_TIME_SET_TIME				"_submitted_time"

/*for esr*/
#define CGI_ESR_PROTOCOL_TYPE			"_protocol_type_ex"
#define CGI_ESR_PROTOCOL_MEDIA			"_protocol_media_ex"
#define CGI_ESR_MAX_ALARM_ATTEMPTS		"_esr_max_alarm_report_attempts_ex"
#define CGI_ESR_CALL_ELAPSE_TIME		"_esr_call_elapse_time_ex"
#define CGI_ESR_MAIN_REPORT_PHONE		"_main_report_phone_ex"
#define CGI_ESR_SECOND_REPORT_PHONE		"_second_report_phone_ex"
#define CGI_ESR_CALLBACK_PHONE			"_callback_phone_ex"
#define CGI_ESR_REPORT_IP1				"_report_ip1_ex"
#define CGI_ESR_REPORT_IP2				"_report_ip2_ex"
#define CGI_ESR_SECURITY_IP1			"_security_ip1_ex"
#define CGI_ESR_SECURITY_IP2			"_security_ip2_ex"
#define CGI_ESR_SECURITY_LEVEL			"_securitylevel_ex"
#define CGI_ESR_CALLBACK_INUSE			"_callbackinuse_ex"
#define CGI_ESR_REPORT_INUSE			"_reportinuse_ex"
#define CGI_ESR_CCID					"_ccid_ex"
#define CGI_ESR_SOCID					"_socid_ex"
#define	CGI_ESR_COMMANDPARAM			"_commonparam_ex"

#define CGI_PRODUCT_INFO				"acu_barcode_information"

/*for YDN*/
#define CGI_YDN_PROTOCOL_TYPE			"_protocol_type_ydn"
#define CGI_YDN_PROTOCOL_MEDIA			"_protocol_media_ydn"
#define CGI_YDN_REPORT_INUSE			"_reportinuse_ydn"
#define CGI_YDN_MAX_ALARM_ATTEMPTS		"_ydn_max_alarm_report_attempts_ex"
#define CGI_YDN_CALL_ELAPSE_TIME		"_ydn_call_elapse_time_ex"
#define CGI_YDN_MAIN_REPORT_PHONE		"_main_report_phone_ydn"
#define CGI_YDN_SECOND_REPORT_PHONE		"_second_report_phone_ydn"
#define CGI_YDN_CALLBACK_PHONE			"_callback_phone_ydn"
#define	CGI_YDN_COMMANDPARAM			"_commonparam_ydn"
#define CGI_YDN_CCID					"_ccid_ydn"

#define CGI_YDN_LIST					"_ydn_list"

//
//for GC_PS
#define CGI_PS_MODE                     "_powerSpliteMode"
#define CGI_PS_INFO                     "_powerSpliteExp"

typedef struct tagCommandInfo
{
	int		iConfigureType;		/*Configure type 0:network 1:nms 2:esr 3 : User 4:Time*/
	int		iCommandyType;		/*other than user : 0: Get 1: Set for user : 0:Get 1:Modify 2 :Add 3:Delete*/
	char	szSessionID[64];
	int		iLanguage;		
}ConfigureCommandInfo;

#ifdef WEB_ADD_PRODUCT_INFO
static char CFG_HTML_PATH[14][64] = {"p11_network_config.htm",
									"p12_nms_config.htm",
									"p13_esr_config.htm",
									"p14_user_config.htm",
									"p19_time_config.htm",
									"p30_acu_signal_value.htm",
                                    "p39_edit_config_plc.htm",
                                    "p42_edit_config_alarm.htm",
                                    "p41_edit_config_alarmReg.htm",
                                    "p13_esr_config.htm",
									"p44_cfg_powersplite.htm",
									"p78_get_setting_param.htm",
									"p77_auto_config.htm",
									"p12_nmsv3_config.htm"};
#else
static char CFG_HTML_PATH[10][64] = {"p11_network_config.htm",
									"p12_nms_config.htm",
									"p13_esr_config.htm",
									"p14_user_config.htm",
									"p19_time_config.htm",
                                    "p39_edit_config_plc.htm",
                                    "p42_edit_config_alarm.htm",
                                    "p41_edit_config_alarmReg.htm",
                                    "p43_ydn_config.htm",
									"p44_cfg_powersplite.htm"};
#endif

static void Web_SET_PostUserInfoPage(IN char *pGetBuf,IN int iLanguage);
static char *Web_SET_SendConfigureCommand(IN char *szBuf, IN int iModifyPassword);	//iModifyPassword = 13:Modify password
static int Web_SET_GetCommandParam(IN ConfigureCommandInfo *stConfigure, OUT char **pBuf);
static void Web_SET_PostNetWorkPage(IN char *pGetBuf, IN int iLanguage);
static int Web_SET_PostCFGPLCWorkPage(IN char *pGetBuf, IN int iLanguage);//Added by wj for configure PLC 2006.5.13
static int Web_SET_PostCFGALARMSWorkPage(IN char *pGetBuf, IN int iLanguage);//Added by wj for configure AlarmSupExp 2006.5.13
static int Web_SET_PostCFGALARMRegWorkPage(IN char *pGetBuf, IN int iLanguage);//Added by wj for configure AlarmSupExp 2006.5.13
static void Web_SET_PostTimeInfoPage(IN  char *pGetBuf, IN int iLanguage, IN const char *sessionId);
char *Web_RemoveWhiteSpace(IN char *pszBuf);
static void Web_SET_PostESRPage(IN char *pGetBuf, IN int iLanguage, IN int iESRType);
static void Web_SET_PostNMSPage(IN  char *pGetBuf, IN int iLanguage, IN BOOL bNMSV3);
static void Web_CGI_PostNoAuthority(IN int iModifyType, IN int iLanguage,IN BOOL boolDeleteSelf);
static void Web_SET_PostYDNPage(IN char *pGetBuf, IN int iLanguage, IN int iESRType);
static void Web_GET_PostProductInfo(IN char *pGetBuf, IN int iLanguage, IN const char *sessionId);
static int Web_SET_PostCFGGCPSWorkPage(IN char *pGetBuf, IN int iLanguage);
static int Web_SET_PostCFGGetSettingParamWorkPage(IN char *pGetBuf, IN int iLanguage);
static int Web_SET_PostAutoConfigWorkPage(IN int iLanguage);
char		szUserName[32];
char        *pUserNameTransfer;

int main(void)
{

	int							iLanguage = 0;				
	//char						*pHtml = NULL;
	//char						szExchange[8];
	char						*szGetBuf = NULL;
	ConfigureCommandInfo		stConfigureCommand;
	char						*szReturnBuf = NULL;
	int							iAuthority = 0;
	char						*szSessName = NULL ;
	char						*pTemp = NULL;

	printf("Content-type:text/html\n\n");
	
	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");

	/*get parameter*/
	
	if (Web_SET_GetCommandParam(&stConfigureCommand, &szGetBuf) != FALSE)
	{
		/*judge overtime and authority*/
		//sprintf(szSessionID,"%s",stConfigureCommand.szSessionID);
		//TRACE("iAuthority : %s", stConfigureCommand.szSessionID);
		 
		iAuthority = start_sessionA(Web_RemoveWhiteSpace(stConfigureCommand.szSessionID), &pUserNameTransfer);//start_session(Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));
		

		/*language type*/
		if(stConfigureCommand.iLanguage > 0)
		{
            if(stConfigureCommand.iLanguage == 1)
            {
                iLanguage =	LOCAL_LANGUAGE_NAME;
            }
            else
            {
                iLanguage =	LOCAL2_LANGUAGE_NAME;
            }
			
		}
		else
		{
			iLanguage = ENGLISH_LANGUAGE_NAME;
		}

		printf("\n\n");

		if(iAuthority < 0)			//iAuthority < 0 , it is the session fault,
										//not the authority fault

		{
			if(stConfigureCommand.iCommandyType != GET_NETWORK_INFO &&
					stConfigureCommand.iCommandyType != GET_NMS_INFO &&
					stConfigureCommand.iCommandyType != GET_NMSV3_INFO &&
						stConfigureCommand.iCommandyType != GET_ESR_INFO &&
							stConfigureCommand.iCommandyType != GET_TIME_INFO &&
								stConfigureCommand.iCommandyType != GET_USER_INFO && 
								 stConfigureCommand.iCommandyType != GET_PRODUCT_INFO &&
                                 stConfigureCommand.iCommandyType != GET_PLC_CONFIG &&
                                 stConfigureCommand.iCommandyType != GET_ALARM_CONFIG &&
                                 stConfigureCommand.iCommandyType != GET_ALARMREG_CONFIG &&
                                 stConfigureCommand.iCommandyType != GET_YDN_CONFIG &&
								 stConfigureCommand.iCommandyType == GET_SETTING_PARAM)
			{
				print_session_error(iAuthority,iLanguage);
				DELETE(szGetBuf);
				szGetBuf = NULL;

                if(pUserNameTransfer != NULL)
                {
                    DELETE(pUserNameTransfer);
                    pUserNameTransfer = NULL;
                }
				return FALSE;
			}
		}
		else
		{
			if(stConfigureCommand.iCommandyType == ADD_USER_INFO ||
                stConfigureCommand.iCommandyType == DELETE_USER_INFO)
			{
				if(iAuthority < 4)
				{
					Web_CGI_PostNoAuthority(stConfigureCommand.iCommandyType, iLanguage,FALSE);
					if(szGetBuf != NULL)
					{
						DELETE(szGetBuf);
						szGetBuf = NULL;
					}
                    if(pUserNameTransfer != NULL)
                    {
                        DELETE(pUserNameTransfer);
                        pUserNameTransfer = NULL;
                    }

					return 0;
				}
				else
				{
					szSessName = getUserName(Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));
					if(szSessName != NULL && strcmp(Web_RemoveWhiteSpace(szUserName), Web_RemoveWhiteSpace(szSessName)) == 0)
					{
						Web_CGI_PostNoAuthority(stConfigureCommand.iCommandyType, iLanguage,TRUE);
						if(szGetBuf != NULL)
						{
							DELETE(szGetBuf);
							szGetBuf = NULL;
						}

						if(szSessName != NULL)
						{
							DELETE(szSessName);
							szSessName = NULL;
						}
                        if(pUserNameTransfer != NULL)
                        {
                            DELETE(pUserNameTransfer);
                            pUserNameTransfer = NULL;
                        }

						return 0;
					}
					if(szSessName != NULL)
					{
						DELETE(szSessName);
						szSessName = NULL;
					}
					
				}
								 
			}
			else if( stConfigureCommand.iCommandyType == SET_USER_INFO  && iAuthority != 4 )
			{
				
				szSessName = getUserName(Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));
				
				if(szSessName == NULL || strcmp(Web_RemoveWhiteSpace(szUserName), Web_RemoveWhiteSpace(szSessName)) != 0)			 
				{
					Web_CGI_PostNoAuthority(SET_USER_INFO, iLanguage, FALSE);
					if(szSessName != NULL)
					{
						DELETE(szSessName);
						szSessName = NULL;
					}
					
					if(szGetBuf != NULL)
					{
						DELETE(szGetBuf);
						szGetBuf = NULL;
					}
					if(pUserNameTransfer != NULL)
					{
						DELETE(pUserNameTransfer);
						pUserNameTransfer = NULL;
					}
					return 0;
				}

				if(szSessName != NULL)
				{
					DELETE(szSessName);
					szSessName = NULL;
				}
				stConfigureCommand.iCommandyType = SET_USER_PASSWORD;
		
			}
			else if(iAuthority < 3 )	//5,6,7,8 is the authority level
										//1 - Browser     2 - operator
										//3 - Engineer    4-Administrator
										//Engineer and administrator have privelege
										//to control command;
										//Brower and operator have no privelege.
			{
				 if(stConfigureCommand.iCommandyType == SET_NETWORK_INFO || 
							stConfigureCommand.iCommandyType ==SET_NMS_INFO || 
							stConfigureCommand.iCommandyType ==SET_NMSV3_INFO ||
							stConfigureCommand.iCommandyType ==SET_TIME_VALUE||
							stConfigureCommand.iCommandyType ==SET_TIME_IP ||
                            stConfigureCommand.iCommandyType == SET_PLC_CONFIG ||
                            stConfigureCommand.iCommandyType == DEL_PLC_CONFIG ||
                            stConfigureCommand.iCommandyType == SET_ALARM_CONFIG ||
                            stConfigureCommand.iCommandyType == SET_ALARMREG_CONFIG ||
                            stConfigureCommand.iCommandyType == SET_YDN_CONFIG)
				{
					Web_CGI_PostNoAuthority(stConfigureCommand.iCommandyType, iLanguage, FALSE);
					
					if(szGetBuf != NULL)
					{
						DELETE(szGetBuf);
						szGetBuf = NULL;
					}
                    if(pUserNameTransfer != NULL)
                    {
                        DELETE(pUserNameTransfer);
                        pUserNameTransfer = NULL;
                    }

					return 0;
				}
				else if(stConfigureCommand.iCommandyType == SET_ESR_INFO)
				{
					Web_CGI_PostNoAuthority(SET_ESR_INFO, iLanguage,FALSE);

					if(szGetBuf != NULL)
					{
						DELETE(szGetBuf);
						szGetBuf = NULL;
					}
                    if(pUserNameTransfer != NULL)
                    {
                        DELETE(pUserNameTransfer);
                        pUserNameTransfer = NULL;
                    }

					return 0;
				}

			}

		}


		//TRACE("szGetBuf : %s\n", szGetBuf);
		switch(stConfigureCommand.iConfigureType)
		{
			
			case CONFIGURE_OF_NETWORK://Network
				{
					//if(iAuthority < 3 && stConfigureCommand.iCommandyType == SET_NETWORK_INFO)
					//{
					//	Web_SET_PostNetWorkPage(szReturnBuf, iLanguage, iAuthority);
					//}
					
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{


						Web_SET_PostNetWorkPage(szReturnBuf, iLanguage);
					}
				
				}
				break;
			case CONFIGURE_OF_NMS://NMS
				{
					szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType);
					if(szReturnBuf != NULL)
					
					{
						if(stConfigureCommand.iCommandyType  ==SET_NMSV3_INFO || stConfigureCommand.iCommandyType  == GET_NMSV3_INFO )
						{
							Web_SET_PostNMSPage(szReturnBuf,iLanguage, TRUE);
						}
						else
						{
							Web_SET_PostNMSPage(szReturnBuf,iLanguage, FALSE);	
						}
					}
				}
				break;
			case CONFIGURE_OF_ESR://ESR
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{

						
						Web_SET_PostESRPage(szReturnBuf,iLanguage,stConfigureCommand.iCommandyType);
					}
				}
				break;
			case CONFIGURE_OF_USER://User
			{
				if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
				{

					Web_SET_PostUserInfoPage(szReturnBuf,iLanguage);
						
				}
				break;
			}
			case CONFIGURE_OF_TIME://Time
				if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
				{
					//TRACE("szReturnBuf : %s\n", szReturnBuf);

					Web_SET_PostTimeInfoPage(szReturnBuf, iLanguage,Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));
				}
				break;
#ifdef WEB_ADD_PRODUCT_INFO
			case CONFIGURE_OF_PRODUCTINFO://Product info
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{

						Web_GET_PostProductInfo(szReturnBuf, iLanguage, Web_RemoveWhiteSpace(stConfigureCommand.szSessionID));
					}
				}
				break;
#endif

            //////////////////////////////////////////////////////////////////////////
            ////Added by wj for Config PLC private config file 2006.5.13
            
            case CONFIGURE_OF_PLC://Config PLC private config file
            {

              
                if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
                {
                    //TRACE("szReturnBuf : %s\n", szReturnBuf);

                    Web_SET_PostCFGPLCWorkPage(szReturnBuf, iLanguage);
                }
                break;

            }

            case CONFIGURE_OF_ALARMS://Config AlarmSupExp 
            {

               
                if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
                {
                    //TRACE("szReturnBuf : %s\n", szReturnBuf);

                    Web_SET_PostCFGALARMSWorkPage(szReturnBuf, iLanguage);
                }
                break;

            }

            case CONFIGURE_OF_ALARMREG://Config Alarm Relay
            {

                /*FILE *pf=NULL;
                pf=fopen("/var/wj3.txt","a+");
                fwrite(szGetBuf,strlen(szGetBuf),1,pf);
                fclose(pf);*/

                if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
                {
                    //TRACE("szReturnBuf : %s\n", szReturnBuf);

                    Web_SET_PostCFGALARMRegWorkPage(szReturnBuf, iLanguage);
                }
                break;

            }

            case CONFIGURE_OF_YDN://Config YDN Setting
                {


                    if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
                    {
                        //TRACE("szReturnBuf : %s\n", szReturnBuf);

                        Web_SET_PostYDNPage(szReturnBuf, iLanguage, stConfigureCommand.iCommandyType);
                    }
                    break;

                }

			case CONFIGURE_OF_GCPS://Config PLC private config file
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
						//TRACE("szReturnBuf : %s\n", szReturnBuf);
						Web_SET_PostCFGGCPSWorkPage(szReturnBuf, iLanguage);
					}
					break;

				}

			case CONFIGURE_OF_SETTINGPARAM://Get Setting Param
				{
					if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf, stConfigureCommand.iCommandyType)) != NULL)
					{
						//TRACE("szReturnBuf : %s\n", szReturnBuf);
						//return TRUE;
						Web_SET_PostCFGGetSettingParamWorkPage(szReturnBuf, iLanguage);
					}
					break;

				}

			case CONFIGURE_OF_AUTOCONFIG://Get Setting Param
				{
					Web_SET_SendConfigureCommand(szGetBuf,stConfigureCommand.iCommandyType);
					Web_SET_PostAutoConfigWorkPage(iLanguage);

					break;

				}
                
            //end////////////////////////////////////////////////////////////////////////
            default:
                break;
            

		}
		if(szReturnBuf != NULL)
		{
			DELETE(szReturnBuf);
			szReturnBuf = NULL;
		}
		if(szGetBuf != NULL)
		{
			DELETE(szGetBuf);
			szGetBuf = NULL;
		}
        if(pUserNameTransfer != NULL)
        {
            DELETE(pUserNameTransfer);
            pUserNameTransfer = NULL;
        }
	}
	return TRUE;

}

static char *Web_SET_SendConfigureCommand(IN char *szBuf, IN int iModifyPassword)	//iModifyPassword = 13:Modify password
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	//TRACE("szBuf : %s\n", szBuf);
	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return NULL;
	}
	 
	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return NULL;
	}
	
	/*start buffer with pid and a blank*/
	
	iLen = sprintf(szBuf1,"%10ld%2d%2d%-32s%-128s",(long)getpid(), CONFIGURE_MANAGE,iModifyPassword,pUserNameTransfer,szBuf);

	/*FILE *pf2=NULL;
	pf2=fopen("/var/wj2.txt","a+");
	fwrite(szBuf1,strlen(szBuf1),1,pf2);
	fclose(pf2);*/

    
#ifdef WEB_ADD_PRODUCT_INFO
	//printf("szBuf1 : %s\n", szBuf1);
	//return;
#endif

	if((write(fd, (void *)szBuf1, strlen(szBuf1) + 1))<0)
	{
		//FILE *pf2=NULL;
		//pf2=fopen("/var/wj3.txt","a+");
		////fwrite(szBuf3,strlen(szBuf3),1,pf2);
		//fclose(pf2);

		close(fd);
		return NULL;
	}

	/*FILE *fp;
	if((fp = fopen("/var/error2.txt","wb")) != NULL)
	{
		fwrite(szBuf1,strlen(szBuf1), 1, fp);
		fwrite("3333\n", 5,1,fp);

		fclose(fp);
	}*/

	if((fd2 = open(fifoname,O_RDONLY))<0)
	{
		//FILE *pf2=NULL;
		//pf2=fopen("/var/wj2.txt","a+");
		////fwrite(szBuf3,strlen(szBuf3),1,pf2);
		//fclose(pf2);

		return NULL;
	}



    szBuf3 = NEW(char,10 * PIPE_BUF);
    memset(szBuf3,0,10 * PIPE_BUF);
    int iBufCount=0;
    if(szBuf3 == NULL)
    {
        return NULL;
    }

    while((iLen = read(fd2,szBuf2,PIPE_BUF - 1)) > 0)
    {

        if(iBufCount >= 10)
        {
            szBuf3 = RENEW(char, szBuf3, (iBufCount + 1) * PIPE_BUF);
            if(szBuf3 == NULL)
            {
                return NULL;
            }

        }

        strcat(szBuf3, szBuf2);
        iBufCount++;
    }

   /* FILE *pf2=NULL;
    pf2=fopen("/var/wj2.txt","a+");
    fwrite(szBuf3,strlen(szBuf3),1,pf2);
    fclose(pf2);*/


	close(fd2);
	close(fd);
	unlink(fifoname);
	
	return szBuf3;
}




static int Web_SET_GetCommandParam(IN ConfigureCommandInfo *stConfigure, OUT char **pBuf)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL;
	char	*szSendBuf = NULL;
	int		iLen = 0;

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}

	szSendBuf = NEW(char, 1024);
	if(szSendBuf == NULL)
	{
		return FALSE;
	}

	memset(szSendBuf, 0x0, 1024);
	if((val = getValue( getvars, postvars, CGI_MODIFY_CONFIGURE_DETAIL )) != NULL)
	{
		//TRACE("val : %s\n",val);
		stConfigure->iCommandyType = atoi(val);		
	}

	if((val = getValue( getvars, postvars, CGI_MODIFY_CONFIGURE )) != NULL)
	{
		stConfigure->iConfigureType = atoi(val);
	}
	
	if((val = getValue( getvars, postvars, SESSION_ID )) != NULL ) // sessionId
    {
        //stConfigure->iSessionID = atoi(val);
	
		sprintf(stConfigure->szSessionID, "%63s",val);//, sizeof(stConfigure->szSessionID));

	}

	if((val = getValue(getvars,postvars,LANGUAGE_TYPE)) != NULL)
	{
		stConfigure->iLanguage = atoi(val);
	}


	iLen += sprintf(szSendBuf + iLen, "%2d",stConfigure->iCommandyType);
	//TRACE("szSendBuf : %s",szSendBuf);
	//TRACE("stConfigure->iCommandyType :%d\n", stConfigure->iCommandyType);
	iLen += sprintf(szSendBuf + iLen, "%2d",stConfigure->iLanguage);
	//TRACE("stConfigure->iLanguage :%d\n", stConfigure->iLanguage);

	switch(stConfigure->iConfigureType)
	{
		case CONFIGURE_OF_NETWORK://Network
		{
			if(stConfigure->iCommandyType == SET_NETWORK_INFO)
			{
				if((val = getValue( getvars, postvars, CGI_CLIENT_IP_NAME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s%c",val, 59);
				}
				if((val = getValue( getvars, postvars, CGI_CLIENT_MASK_NAME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s%c",val,59);
				}
				if((val = getValue( getvars, postvars, CGI_CLIENT_GATE_NAME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s%c",val,59);
				}
			}
			break;
		}
		case CONFIGURE_OF_NMS://NMS
			if(stConfigure->iCommandyType == SET_NMS_INFO)
			{

#define CGI_NMS_IP							"_ip"
#define CGI_NMS_PUBLIC_COMMUNITY			"_public_community"
#define CGI_NMS_PRIVATE_COMMUNITY			"_private_community"
#define	CGI_NMS_TRAP						"_trap_level"
#define CGI_NMS_OPERATE_TYPE				"_operate_type"

				if((val = getValue( getvars, postvars, CGI_NMS_OPERATE_TYPE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_IP )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_PUBLIC_COMMUNITY )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_PRIVATE_COMMUNITY )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_TRAP )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
			}
			else if(stConfigure->iCommandyType == SET_NMSV3_INFO)
			{
#define CGI_NMS_USERNAME			"_username"
#define CGI_NMS_TRAP_IP				"_trapip"
#define CGI_NMS_DES_COMMUNITY			"_des_community"
#define CGI_NMS_MD5_COMMUNITY			"_md5_community"
#define	CGI_NMS_TRAP_LEVEL			"_trap_level"
#define CGI_NMS_ENGIN_ID			"_trap_engine"

				if((val = getValue( getvars, postvars, CGI_NMS_OPERATE_TYPE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_USERNAME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_TRAP_IP )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_DES_COMMUNITY )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_NMS_MD5_COMMUNITY )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				
				if((val = getValue( getvars, postvars, CGI_NMS_TRAP_LEVEL )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				iLen += sprintf(szSendBuf + iLen, "%c",59);
			}
			break;
		case CONFIGURE_OF_ESR://ESR
			if(stConfigure->iCommandyType == SET_ESR_INFO)
			{
				if((val = getValue( getvars, postvars, CGI_ESR_PROTOCOL_TYPE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_PROTOCOL_MEDIA )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_CALL_ELAPSE_TIME )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_MAX_ALARM_ATTEMPTS )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
						
				
				if((val = getValue( getvars, postvars, CGI_ESR_CALLBACK_INUSE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_REPORT_INUSE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_SECURITY_LEVEL )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_ESR_MAIN_REPORT_PHONE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				
				if((val = getValue( getvars, postvars, CGI_ESR_SECOND_REPORT_PHONE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				
				if((val = getValue( getvars, postvars, CGI_ESR_CALLBACK_PHONE )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);
				
				if((val = getValue( getvars, postvars, CGI_ESR_REPORT_IP1 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_REPORT_IP2 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_SECURITY_IP1 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_SECURITY_IP2 )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_CCID )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val = getValue( getvars, postvars, CGI_ESR_SOCID )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);


				if((val = getValue( getvars, postvars, CGI_ESR_COMMANDPARAM )) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

			}

			break;
		case CONFIGURE_OF_USER://User
		{
			
			if(stConfigure->iCommandyType == SET_USER_INFO ||
					stConfigure->iCommandyType == ADD_USER_INFO)
			{
				if((val =  getValue( getvars, postvars, CGI_USER_NAME)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
					strncpyz(szUserName, val, sizeof(szUserName));
					
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val =  getValue( getvars, postvars, CGI_USER_PASSWORD)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

				if((val =  getValue( getvars, postvars, CGI_USER_LEVEL)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%4s",val);
				}
				iLen += sprintf(szSendBuf + iLen, "%c",59);

			}
			else if(stConfigure->iCommandyType == DELETE_USER_INFO)
			{
				if((val =  getValue( getvars, postvars, CGI_USER_NAME)) != NULL)
				{
					iLen += sprintf(szSendBuf + iLen, "%32s",val);
					strncpyz(szUserName, val, sizeof(szUserName));
				}
				
			}
			break;
		}
		case CONFIGURE_OF_TIME://Time
		{

			if(stConfigure->iCommandyType == GET_TIME_INFO)			//��ȡ����
			{
				//Get time server info
				break;
			}
			else						//���ò���
			{

				if(stConfigure->iCommandyType == SET_TIME_VALUE )	
				{
					//Set Time
					if((val =  getValue( getvars, postvars, CGI_TIME_SET_TIME)) != NULL)
					{
						iLen += sprintf(szSendBuf + iLen, "%32s;",val);
					}
					break;
				}
				else
				{
					if((val =  getValue( getvars, postvars, CGI_TIME_MAIN_IP)) != NULL)
					{
						//Main IP
						iLen += sprintf(szSendBuf + iLen, "%32s;",val);
					}
					if((val =  getValue( getvars, postvars, CGI_TIME_BAK_IP)) != NULL)
					{
						//Bak IP
						iLen += sprintf(szSendBuf + iLen, "%32s;",val);
					}
					if((val =  getValue( getvars, postvars, CGI_TIME_INTERVAL)) != NULL)
					{
						//Time Interval 
						iLen += sprintf(szSendBuf + iLen, "%16s;",val);
					}			

					if((val =  getValue( getvars, postvars, CGI_TIME_ZONE)) != NULL)
					{
						//Time Zone 

						iLen += sprintf(szSendBuf + iLen, "%16s;",val);
					}
					
				}

			}
				
			break;
		}
		case CONFIGURE_OF_PRODUCTINFO://Product Information
		{

			break;
		}

        //////////////////////////////////////////////////////////////////////////
        //Added by wj for Config PLC private config file 2006.5.13

        case CONFIGURE_OF_PLC: //Config PLC
        {

            
            if(stConfigure->iCommandyType == SET_PLC_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_PLC_ADD)) != NULL)
                {

                    iLen += sprintf(szSendBuf + iLen, "%-256s",val);
                }
            }
            else if(stConfigure->iCommandyType == DEL_PLC_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_PLC_DEL)) != NULL)
                {

                    iLen += sprintf(szSendBuf + iLen, "%-256s",val);
                }
            }
            else if(stConfigure->iCommandyType == GET_PLC_CONFIG)
            {

            }

            break;
        }
        case CONFIGURE_OF_ALARMS: //Config Alarm Suppressing Exp
        {
            if(stConfigure->iCommandyType == GET_ALARM_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_ALARM_EQUIPTYPEID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%4s",val);
                }
            }
            else if(stConfigure->iCommandyType == SET_ALARM_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_ALARM_EQUIPTYPEID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%4s",val);
                }
                //iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val =  getValue( getvars, postvars, CFG_ALARM_ALARMID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%3s",val);
                }
                //iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val =  getValue( getvars, postvars, CFG_ALARM_ALARMSUPEXP)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%-128s",val);
                }

            }
            break;
        }

        case CONFIGURE_OF_ALARMREG: //Config Alarm Relay
        {
            if(stConfigure->iCommandyType == GET_ALARMREG_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_ALARM_EQUIPTYPEID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%4s",val);
                }
            }
            else if(stConfigure->iCommandyType == SET_ALARMREG_CONFIG)
            {
                if((val =  getValue( getvars, postvars, CFG_ALARM_EQUIPTYPEID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%4s",val);
                }
                //iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val =  getValue( getvars, postvars, CFG_ALARM_ALARMID)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%3s",val);
                }
                //iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val =  getValue( getvars, postvars, CFG_ALARM_ALARMREG)) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%2s",val);
                }

            }
            break;
        }
        case CONFIGURE_OF_YDN://YDN
		{
			

            if(stConfigure->iCommandyType == SET_YDN_CONFIG)
            {
                if((val = getValue( getvars, postvars, CGI_YDN_PROTOCOL_TYPE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_PROTOCOL_MEDIA )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);
             
                if((val = getValue( getvars, postvars, CGI_YDN_CALL_ELAPSE_TIME )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_MAX_ALARM_ATTEMPTS )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_REPORT_INUSE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_MAIN_REPORT_PHONE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_SECOND_REPORT_PHONE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_CALLBACK_PHONE )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_COMMANDPARAM )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);

                if((val = getValue( getvars, postvars, CGI_YDN_CCID )) != NULL)
                {
                    iLen += sprintf(szSendBuf + iLen, "%s",val);
                }
                iLen += sprintf(szSendBuf + iLen, "%c",59);
   
            }

            break;
		}
		case CONFIGURE_OF_GCPS: //Config GC_PS
		{


			if(stConfigure->iCommandyType == SET_GC_PS_CONFIG)
			{
				if((val =  getValue( getvars, postvars, CGI_PS_INFO)) != NULL)
				{

					iLen += sprintf(szSendBuf + iLen, "%-256s",val);
				}
			}
			else if(stConfigure->iCommandyType == SET_GC_PS_MODE)
			{
				if((val =  getValue( getvars, postvars, CGI_PS_MODE)) != NULL)
				{

					iLen += sprintf(szSendBuf + iLen, "%-256s",val);
				}
			}
			else if(stConfigure->iCommandyType == GET_GC_PS_CONFIG)
			{
				;
			}

			break;
		}
		case CONFIGURE_OF_SETTINGPARAM://Product Information
		{

			break;
		}
		case CONFIGURE_OF_AUTOCONFIG://Product Information
		{

				break;
		}

        //end////////////////////////////////////////////////////////////////////////
        
		default:
			break;
	}
		 
 	*pBuf = szSendBuf;


	cleanUp(getvars, postvars);

	//TRACE("szSendBuf : %s\n", szSendBuf);
    return TRUE;  
}


static void Web_SET_PostNetWorkPage(IN char *pGetBuf, IN int iLanguage)
{
	char	szIP[33],szMask[33], szGate[33];
	char	*pHtml = NULL;
	char	*szPath = NULL;
    int		iPosition = 0;
	char	*pSearchValue = NULL;
	char	*pTGetBuf = pGetBuf;
	char	szExchange[64];
	char	*szTrimText = NULL;
	char	szReturn[3];
	BOOL    blModifySuccess = FALSE;
	memset(szReturn, 0, sizeof(szReturn));


	if(pTGetBuf != NULL)
	{

		blModifySuccess = TRUE;
		strncpyz(szReturn, pTGetBuf, sizeof(szReturn));
		/*fwrite(szReturn,strlen(szReturn), 1, fp);*/
		pTGetBuf = pTGetBuf + 2;
		pSearchValue = strchr(pTGetBuf, 44);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
			if(iPosition > 0)
			{
				strncpyz(szIP, pTGetBuf, ((iPosition + 1) < 33) ? (iPosition + 1) : 33);
				//fwrite(szIP,strlen(szIP), 1, fp);
				pTGetBuf = pTGetBuf + iPosition;
			}

		}
		pTGetBuf = pTGetBuf + 1;
		
		pSearchValue = strchr(pTGetBuf, 44);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
			if(iPosition > 0)
			{
				strncpyz(szMask, pTGetBuf, ((iPosition + 1) < 33) ?(iPosition + 1) :  33);
				//fwrite(szMask,strlen(szMask), 1, fp);
				pTGetBuf = pTGetBuf + iPosition;
			}

		}
		pTGetBuf = pTGetBuf + 1;

		pSearchValue = strchr(pTGetBuf, 44);
		if(pSearchValue != NULL)
		{
			iPosition = pSearchValue - pTGetBuf;
			if(iPosition > 0)
			{
				strncpyz(szGate, pTGetBuf, ((iPosition + 1) < 33) ?(iPosition + 1) :  33);
				//fwrite(szGate,strlen(szGate), 1, fp);
				//pTGetBuf = pTGetBuf + iPosition;
			}

		}

		//fclose(fp);
	}
	
	//pTGetBuf = pTGetBuf + iPosition;
		
	//pTGetBuf = pTGetBuf + 1;
	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[0], iLanguage);

	if(LoadHtmlFile(szPath, &pHtml ) > 0 )
    {
		

		szTrimText = Web_RemoveWhiteSpace(szIP);
		if(atoi(szTrimText) == 0)
		{
			sprintf(szExchange, "%d", inet_addr("0.0.0.0"));
		}
		else
		{
			sprintf(szExchange, "%d", inet_addr(szTrimText));
		}
		ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_IP_NAME), szExchange);
		
		szTrimText = Web_RemoveWhiteSpace(szMask);
		
		if(atoi(szTrimText) == 0)
		{
			sprintf(szExchange, "%d", inet_addr("0.0.0.0"));
		}
		else
		{
			sprintf(szExchange, "%d", inet_addr(szTrimText));
		}

		ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_MASK_NAME), szExchange);


		szTrimText = Web_RemoveWhiteSpace(szGate);
		if(atoi(szTrimText) == 0)
		{
			sprintf(szExchange, "%d", inet_addr("0.0.0.0"));
		}
		else
		{
			sprintf(szExchange, "%d", inet_addr(szTrimText));
		}
		ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CLIENT_GATE_NAME), szExchange);

		if(blModifySuccess == TRUE )
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szReturn);
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");
		}

		PostPage(pHtml);
		//printf(pHtml);
		/*Need to return fail or successfuly*/
		DELETE(pHtml);
		pHtml = NULL;
    }
	 
	
	DELETE(szPath);
	szPath = NULL;

}


static void Web_SET_PostUserInfoPage(IN char *pGetBuf,IN int iLanguage)
{
	char	*pHtml = NULL;
	char	szCommandType[3];
	char	*szPath = NULL;
	char	*pTGetBuf = pGetBuf;
	int		iCommandType = 0;
	
	if(pTGetBuf != NULL)
	{
		strncpyz(szCommandType, pTGetBuf, sizeof(szCommandType));
		pTGetBuf = pTGetBuf + 2;
		szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[3],iLanguage);
		if(LoadHtmlFile(szPath, &pHtml ) > 0)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD(USER_INFO_LIST), pTGetBuf);
			if((iCommandType = atoi(szCommandType)) == 0)
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");
			}
			else
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szCommandType);
			}
			PostPage(pHtml);
			if(pHtml != NULL)
			{
				DELETE(pHtml);
				pHtml = NULL;
			}
		}
	}

	/*Need to return fail or successfuly*/
	DELETE(szPath);
	szPath = NULL;
	

}

static void Web_SET_PostNMSPage(IN  char *pGetBuf, IN int iLanguage, IN BOOL bNMSV3)
{
	ASSERT(pGetBuf);
	
	char	*pHtml = NULL;
	char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	szResult[3];
	//Read file
	pTGetBuf = pGetBuf;
	if(bNMSV3)
	{
		szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[13], iLanguage);
	}
	else
	{
		szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[1], iLanguage);
	}
	
	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{
		strncpyz(szResult, pTGetBuf, sizeof(szResult));
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
		pTGetBuf = pTGetBuf + 2;
		ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_NMS_LIST), pTGetBuf);
		PostPage(pHtml);

		DELETE(pHtml);
		pHtml = NULL;


	}
	if(szPath != NULL)
	{
		DELETE(szPath);
		szPath = NULL;
	}
}

static void Web_SET_PostESRPage(IN char *pGetBuf, IN int iLanguage, IN int iESRType)
{
	ASSERT(pGetBuf);
	
	char	*pHtml = NULL;
	char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	szResult[3];

	//Read file
	pTGetBuf = pGetBuf;

	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[2], iLanguage);
	
	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{
		strncpyz(szResult, pTGetBuf, sizeof(szResult));
		if(iESRType == SET_ESR_INFO)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
		}

		if(atoi(szResult) != 1)
		{
			pTGetBuf = pTGetBuf + 2;
			ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_ESR_LIST), pTGetBuf);
		}
		PostPage(pHtml);
		
		DELETE(pHtml);
		pHtml = NULL;
	
	}

	DELETE(szPath);
	szPath = NULL;


}

#ifdef WEB_ADD_PRODUCT_INFO
static void Web_GET_PostProductInfo(IN char *pGetBuf, IN int iLanguage, IN const char *sessionId)
{
	UNUSED(sessionId);
	ASSERT(pGetBuf);

	char	*pHtml = NULL;
	char	*pTGetBuf = NULL;
	//char	szReturn[3];
	char	*szPath = NULL;// *pSearchValue = NULL, *szTrimText = NULL;
	//int		iPosition;
	//char	szExchange[64];
	//char	szGetInfo[1024];

	pTGetBuf = pGetBuf;
	refresFileTime();

	//printf("pTGetBuf[%s]\n", pTGetBuf);

	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[5], iLanguage);

    //printf("%s\n",szPath);


	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{


		if(pTGetBuf != NULL)
		{
		
		//	szTrimText = Web_RemoveWhiteSpace(pTGetBuf);
		//	printf("szExchange[%s]\n", szExchange);
			ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_PRODUCT_INFO), pTGetBuf);
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "2");
			PostPage(pHtml);

		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "0");
			PostPage(pHtml);
		}
		DELETE(pHtml);
		pHtml = NULL;
	}

	if(szPath != NULL)
	{
    	DELETE(szPath);
		szPath = NULL;
	}
}
#endif

static void Web_SET_PostTimeInfoPage(IN  char *pGetBuf, IN int iLanguage, IN const char *sessionId)
{
	UNUSED(sessionId);
	ASSERT(pGetBuf);
	
	char	*pHtml = NULL;
	//char	szCommandType[3];
	char	szGetInfo[33];
	char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	*pSearchValue = NULL;
	int		iPosition = 0;
	char	*szTrimText = NULL;
	char	szExchange[64];
	char	szReturn[3];

	memset(szReturn, 0, sizeof(szReturn));

	pTGetBuf = pGetBuf;
	//ResetTime(sessionId);
	refresFileTime();
	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[4], iLanguage);

	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{	
		//TRACE("TRUE\n");


		if(pTGetBuf != NULL)
		{
			strncpyz(szReturn, pGetBuf, sizeof(szReturn));
			pTGetBuf = pTGetBuf + 2;

			pSearchValue = strchr(pTGetBuf, 59);
			if(pSearchValue != NULL)
			{
				iPosition = pSearchValue - pTGetBuf;
			}
			if(iPosition > 0)
			{
				strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
				szTrimText = Web_RemoveWhiteSpace(szGetInfo);
				if((strcmp(szTrimText,"0.0.0.0") == 0) || atoi(szTrimText) == 0)
				{
					sprintf(szExchange, "%d", inet_addr("0.0.0.0"));
				}
				else
				{
					sprintf(szExchange, "%d", inet_addr(szTrimText));
				}
				//TRACE("---%s\n", szTrimText);
				ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_CGI_TIME_SERVER1), szExchange);
				pTGetBuf = pTGetBuf + iPosition;
			}
			pTGetBuf = pTGetBuf + 1;

			pSearchValue = strchr(pTGetBuf, 59);
			if(pSearchValue != NULL)
			{
				iPosition = pSearchValue - pTGetBuf;
			}
			if(iPosition > 0)
			{
				strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
				szTrimText = Web_RemoveWhiteSpace(szGetInfo);
				if((strcmp(szTrimText,"0.0.0.0") == 0) ||atoi(szTrimText) == 0)
				{
					sprintf(szExchange, "%d", inet_addr("0.0.0.0"));
				}
				else
				{
					sprintf(szExchange, "%d", inet_addr(szTrimText));
				}
				//TRACE("---%s\n", szTrimText);
				ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_CGI_TIME_SERVER2), szExchange);
				pTGetBuf = pTGetBuf + iPosition;
			}
			pTGetBuf = pTGetBuf + 1;

			pSearchValue = strchr(pTGetBuf, 59);
			if(pSearchValue != NULL)
			{
				iPosition = pSearchValue - pTGetBuf;
			}
			if(iPosition > 0)
			{
				strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
				szTrimText = Web_RemoveWhiteSpace(szGetInfo);
				ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_CGI_TIME_INTERVAL), szTrimText);
				pTGetBuf = pTGetBuf + iPosition;
			}

			pTGetBuf = pTGetBuf + 1;

			pSearchValue = strchr(pTGetBuf, 59);
			if(pSearchValue != NULL)
			{
				iPosition = pSearchValue - pTGetBuf;
			}
			if(iPosition > 0)
			{
				strncpyz(szGetInfo, pTGetBuf, iPosition + 1 );
				szTrimText = Web_RemoveWhiteSpace(szGetInfo);
				ReplaceString(&pHtml, MAKE_VAR_FIELD(WEB_CGI_TIME_ZONE), szTrimText);
			}

			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szReturn);
			PostPage(pHtml);

		
		}
		else
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "0");
			PostPage(pHtml);
		}
		DELETE(pHtml);
		pHtml = NULL;
	}
	
	DELETE(szPath);
	szPath = NULL;
}



static void Web_CGI_PostNoAuthority(IN int iModifyType, IN int iLanguage,IN BOOL boolDeleteSelf)
{

	int			i;
	char		*pHtml = NULL;
	char		szHtmlPath[128];

	if(iModifyType == SET_NETWORK_INFO)
	{
		i = 0;
	}
	else if(iModifyType == SET_NMS_INFO)
	{
		i = 1;
	}
	else if(iModifyType == SET_ESR_INFO)
	{
		i = 2;
	}
	else if(iModifyType == SET_USER_INFO || 
				iModifyType == ADD_USER_INFO ||
					 iModifyType == DELETE_USER_INFO)
	{
		i = 3;
	}
	else if(iModifyType == SET_TIME_VALUE || iModifyType == SET_TIME_IP)
	{
		i = 4;
	}
    else if(iModifyType == SET_PLC_CONFIG || iModifyType == DEL_PLC_CONFIG || iModifyType == GET_PLC_CONFIG)
    {
        i = 6;
    }
    else if(iModifyType == GET_ALARM_CONFIG || iModifyType == SET_ALARM_CONFIG)
    {
        i = 7;
    }
    else if(iModifyType == GET_ALARMREG_CONFIG || iModifyType == SET_ALARMREG_CONFIG)
    {
        i = 8;
    }
    else if(iModifyType == GET_YDN_CONFIG || iModifyType == SET_YDN_CONFIG)
    {
        i = 9;
    }
    else if(iModifyType == SET_NMS_INFO )
    {
        i = 13;
    }
	if(iLanguage > 0)
	{
        if(iLanguage == 1)
        {
            sprintf(szHtmlPath, "%s/%s", HTML_SAVE_REAL_PATH,CFG_HTML_PATH[i]);
        }
        else
        {
            sprintf(szHtmlPath, "%s/%s", HTML_SAVE_REAL2_PATH,CFG_HTML_PATH[i]);
        }
		
	}
	else
	{
		sprintf(szHtmlPath, "%s/%s", HTML_SAVE_ENG_PATH,CFG_HTML_PATH[i]);
	}

	if(LoadHtmlFile(szHtmlPath, &pHtml ) > 0)
	{
		if(iModifyType == SET_ESR_INFO || iModifyType == SET_YDN_CONFIG)
		{
			ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "8");
		}
		else
		{
			if(iModifyType == DELETE_USER_INFO && boolDeleteSelf == TRUE)
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "8");
			}
			else if(iModifyType == SET_USER_INFO )
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "6");//only user and admin can modify password
			}			 
			else
            {
				ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "4");
			}
		}
		PostPage(pHtml);

		DELETE(pHtml);
		pHtml = NULL;

					
	}

}
//////////////////////////////////////////////////////////////////////////
//Added by wj for PLC Configure
static int Web_SET_PostCFGPLCWorkPage(IN char *pGetBuf, IN int iLanguage)
{

    //FILE *pf=NULL;
    //pf=fopen("/var/wj4.txt","a+");
    ////char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    //fwrite(pGetBuf,strlen(pGetBuf),1,pf);
    //fclose(pf);


    ASSERT(pGetBuf);

    char	*pHtml = NULL;
    char	*szPath = NULL;
    char	*pTGetBuf = NULL;
    char	szResult[3];

    char        *pszSampling = NULL;
    char        *pszControl  = NULL;
    char        *pszSetting  = NULL;
    char        *pszAlarm    = NULL;
    char        *pszEquip    = NULL;
    char        *pszPLCInfo  = NULL;
    char        *pszTemp     = NULL;
    //Read file
    pTGetBuf = pGetBuf;

    strncpyz(szResult, pTGetBuf, sizeof(szResult));


    char	*ptr = NULL;
    ptr = pGetBuf + 2;

    /*OEquip*/
    char		*pPosition = strchr(ptr,59);
    int			iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszEquip = NEW(char, iPosition + 1);
        if(pszEquip == NULL)
        {
            return FALSE;
        }
        memset(pszEquip, 0x0, (size_t)iPosition);
        strncpyz(pszEquip, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszEquip,strlen(pszEquip),1,pf);


    /*OSampling*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr ;
    if(iPosition > 0)
    {
        pszSampling = NEW(char, iPosition + 1);
        if(pszSampling == NULL)
        {
            return FALSE;
        }
        memset(pszSampling, 0x0, (size_t)iPosition);
        strncpyz(pszSampling, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszSampling,strlen(pszSampling),1,pf);

    /*OControl*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszControl = NEW(char, iPosition + 1);
        if(pszControl == NULL)
        {
            return FALSE;
        }
        memset(pszControl, 0x0, (size_t)iPosition);
        strncpyz(pszControl, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;
    //fwrite(pszControl,strlen(pszControl),1,pf);

    /*OSetting*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszSetting = NEW(char, iPosition + 1);
        if(pszSetting == NULL)
        {
            return FALSE;
        }
        memset(pszSetting, 0x0, (size_t)iPosition);
        strncpyz(pszSetting, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;
    //fwrite(pszSetting,strlen(pszSetting),1,pf);


    /*OAlarm*/

    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszAlarm = NEW(char, iPosition + 1);
        if(pszAlarm == NULL)
        {
            return FALSE;
        }
        memset(pszAlarm, 0x0, (size_t)iPosition);
        strncpyz(pszAlarm, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    /*PLCConfigInfo*/

    iPosition = strlen(ptr);
    if(iPosition > 0)
    {
        pszPLCInfo = NEW(char, iPosition + 1);
        if(pszPLCInfo == NULL)
        {
            return FALSE;
        }
        memset(pszPLCInfo, 0x0, (size_t)iPosition);
        strncpyz(pszPLCInfo, ptr, iPosition + 1);

    }

    //fwrite(pszAlarm,strlen(pszAlarm),1,pf);


    szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[6], iLanguage);


    //fwrite(szPath,strlen(szPath),1,pf);

    if(LoadHtmlFile(szPath, &pHtml ) > 0)
    {
        ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equipments"), pszEquip);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_sample_signals"), pszSampling);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_control_signals"), pszControl);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_setting_signals"), pszSetting);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_alarm_signals"), pszAlarm);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("configuration_file"), pszPLCInfo);

        if(iLanguage == 0)
        {
            pszTemp = "/var/eng/p40_cfg_plc_Popup.htm";
        }
        else if(iLanguage == 1)
        {
            pszTemp = "/var/loc/p40_cfg_plc_Popup.htm";
        }
        else
        {
            pszTemp = "/var/loc2/p40_cfg_plc_Popup.htm";
        }

        ReplaceString(&pHtml, MAKE_VAR_FIELD("popupPage"),pszTemp);
        
        
        PostPage(pHtml);

        DELETE(pHtml);
        DELETE(pszEquip);
        DELETE(pszSampling);
        DELETE(pszControl);
        DELETE(pszSetting);
        DELETE(pszAlarm);
        DELETE(pszPLCInfo);

        pszSampling = NULL;
        pszControl  = NULL;
        pszSetting  = NULL;
        pszAlarm    = NULL;
        pszEquip    = NULL;
        pHtml       = NULL;

    }

    //fclose(pf);

    DELETE(szPath);
    szPath = NULL;

}
//end////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Added by wj for AlarmSupExp Config
static int Web_SET_PostCFGALARMSWorkPage(IN char *pGetBuf, IN int iLanguage)
{

    /*FILE *pf=NULL;
    pf=fopen("/var/wj4.txt","a+");
    char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    fwrite(p,strlen(p),1,pf);*/


    ASSERT(pGetBuf);

    char	*pHtml = NULL;
    char	*szPath = NULL;
    char	*pTGetBuf = NULL;
    char	szResult[3];

    char *pszStdEquipBuf        = NULL; //new OStdEquip( equipTypeId, equipTypeName),... 
    char *pszDispAlarmItemsBuf  = NULL; //new ODispAlarmItem( signalId,signalName,suppressExp),... 
    char *pszRelEquip           = NULL; //new ORelEquip( equipId, equipName, sequnenceNo),... 
    char *pszRelEquipAlarms     = NULL; //new ORelEquipAlarms( equipId,signalId,signalName),... 
    char *pszStdEquipTypeName   = NULL; //StdEquipTypeName

    //Read file
    pTGetBuf = pGetBuf;

    strncpyz(szResult, pTGetBuf, sizeof(szResult));


    char	*ptr = NULL;
    ptr = pGetBuf + 2;

    /*StdEquipTypeName*/
    char		*pPosition = strchr(ptr,59);
    int			iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszStdEquipTypeName = NEW(char, iPosition + 1);
        if(pszStdEquipTypeName == NULL)
        {
            return FALSE;
        }
        memset(pszStdEquipTypeName, 0x0, (size_t)iPosition);
        strncpyz(pszStdEquipTypeName, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;


    //fwrite(pszStdEquipTypeName,strlen(pszStdEquipTypeName),1,pf);


    /*OStdEquip*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszStdEquipBuf = NEW(char, iPosition + 1);
        if(pszStdEquipBuf == NULL)
        {
            return FALSE;
        }
        memset(pszStdEquipBuf, 0x0, (size_t)iPosition);
        strncpyz(pszStdEquipBuf, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszStdEquipBuf,strlen(pszStdEquipBuf),1,pf);


    /*ODispAlarmItem*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr ;
    if(iPosition > 0)
    {
        pszDispAlarmItemsBuf = NEW(char, iPosition + 1);
        if(pszDispAlarmItemsBuf == NULL)
        {
            return FALSE;
        }
        memset(pszDispAlarmItemsBuf, 0x0, (size_t)iPosition);
        strncpyz(pszDispAlarmItemsBuf, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszDispAlarmItemsBuf,strlen(pszDispAlarmItemsBuf),1,pf);

    /*ORelEquip*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszRelEquip = NEW(char, iPosition + 1);
        if(pszRelEquip == NULL)
        {
            return FALSE;
        }
        memset(pszRelEquip, 0x0, (size_t)iPosition);
        strncpyz(pszRelEquip, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;
    //fwrite(pszRelEquip,strlen(pszRelEquip),1,pf);


    /*ORelEquipAlarms*/

    iPosition = strlen(ptr);
    if(iPosition > 0)
    {
        pszRelEquipAlarms = NEW(char, iPosition + 1);
        if(pszRelEquipAlarms == NULL)
        {
            return FALSE;
        }
        memset(pszRelEquipAlarms, 0x0, (size_t)iPosition);
        strncpyz(pszRelEquipAlarms, ptr, iPosition + 1);

    }

    //fwrite(pszRelEquipAlarms,strlen(pszRelEquipAlarms),1,pf);


    szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[7], iLanguage);
    //fwrite(szPath,strlen(szPath),1,pf);

    if(LoadHtmlFile(szPath, &pHtml ) > 0)
    {
        ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("stdEquipments"), pszStdEquipBuf);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("dispAlarmItems"), pszDispAlarmItemsBuf);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("relEquipments"), pszRelEquip);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("relEquipAlarms"), pszRelEquipAlarms);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("stdEquipTypeName"), pszStdEquipTypeName);
        

        PostPage(pHtml);

        DELETE(pHtml);
        DELETE(pszStdEquipBuf);
        DELETE(pszDispAlarmItemsBuf);
        DELETE(pszRelEquip);
        DELETE(pszRelEquipAlarms);
        DELETE(pszStdEquipTypeName);


        pszStdEquipBuf          = NULL;
        pszDispAlarmItemsBuf    = NULL;
        pszRelEquip             = NULL;
        pszRelEquipAlarms       = NULL;
        pszStdEquipTypeName     = NULL;


    }

    //fclose(pf);

    DELETE(szPath);
    szPath = NULL;

    return TRUE;

}
//end////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Added by wj for AlarmRelay Config
static int Web_SET_PostCFGALARMRegWorkPage(IN char *pGetBuf, IN int iLanguage)
{

    /*FILE *pf=NULL;
    pf=fopen("/var/wj4.txt","a+");
    char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
    fwrite(p,strlen(p),1,pf);*/


    ASSERT(pGetBuf);

    char	*pHtml = NULL;
    char	*szPath = NULL;
    char	*pTGetBuf = NULL;
    char	szResult[3];

    char *pszStdEquipBuf        = NULL; //new OStdEquip( equipTypeId, equipTypeName),... 
    char *pszDispAlarmItemsBuf  = NULL; //new ODispAlarmItem( signalId,signalName,suppressExp),... 
   
    char *pszStdEquipTypeName   = NULL; //StdEquipTypeName

    //Read file
    pTGetBuf = pGetBuf;

    strncpyz(szResult, pTGetBuf, sizeof(szResult));


    char	*ptr = NULL;
    ptr = pGetBuf + 2;

    /*StdEquipTypeName*/
    char		*pPosition = strchr(ptr,59);
    int			iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszStdEquipTypeName = NEW(char, iPosition + 1);
        if(pszStdEquipTypeName == NULL)
        {
            return FALSE;
        }
        memset(pszStdEquipTypeName, 0x0, (size_t)iPosition);
        strncpyz(pszStdEquipTypeName, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;


    //fwrite(pszStdEquipTypeName,strlen(pszStdEquipTypeName),1,pf);


    /*OStdEquip*/
    pPosition = strchr(ptr,59);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pszStdEquipBuf = NEW(char, iPosition + 1);
        if(pszStdEquipBuf == NULL)
        {
            return FALSE;
        }
        memset(pszStdEquipBuf, 0x0, (size_t)iPosition);
        strncpyz(pszStdEquipBuf, ptr, iPosition + 1);
        ptr = ptr + iPosition ;

    }
    ptr = ptr + 1;

    //fwrite(pszStdEquipBuf,strlen(pszStdEquipBuf),1,pf);


    /*ODispAlarmItem*/
    iPosition = strlen(ptr);
    if(iPosition > 0)
    {
        pszDispAlarmItemsBuf = NEW(char, iPosition + 1);
        if(pszDispAlarmItemsBuf == NULL)
        {
            return FALSE;
        }
        memset(pszDispAlarmItemsBuf, 0x0, (size_t)iPosition);
        strncpyz(pszDispAlarmItemsBuf, ptr, iPosition + 1);

    }

    //fwrite(pszDispAlarmItemsBuf,strlen(pszDispAlarmItemsBuf),1,pf);



    szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[8], iLanguage);
    //fwrite(szPath,strlen(szPath),1,pf);

    if(LoadHtmlFile(szPath, &pHtml ) > 0)
    {
        ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("stdEquipments"), pszStdEquipBuf);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("dispAlarmItems"), pszDispAlarmItemsBuf);
        ReplaceString(&pHtml, MAKE_VAR_FIELD("stdEquipTypeName"), pszStdEquipTypeName);


        PostPage(pHtml);

        DELETE(pHtml);
        DELETE(pszStdEquipBuf);
        DELETE(pszDispAlarmItemsBuf);
        DELETE(pszStdEquipTypeName);


        pszStdEquipBuf          = NULL;
        pszDispAlarmItemsBuf    = NULL;
        pszStdEquipTypeName     = NULL;


    }

    //fclose(pf);

    DELETE(szPath);
    szPath = NULL;

    return TRUE;
}
//end////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Added by wj for YDN Setting config
static void Web_SET_PostYDNPage(IN char *pGetBuf, IN int iLanguage, IN int iYDNType)
{
    ASSERT(pGetBuf);

    char	*pHtml = NULL;
    char	*szPath = NULL;
    char	*pTGetBuf = NULL;
    char	szResult[3];

    //Read file
    pTGetBuf = pGetBuf;

    szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[9], iLanguage);

    if(LoadHtmlFile(szPath, &pHtml ) > 0)
    {
        strncpyz(szResult, pTGetBuf, sizeof(szResult));
        if(iYDNType == SET_YDN_CONFIG)
        {
            ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        }
        else
        {
            ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
        }

        if(atoi(szResult) != 1)
        {
            pTGetBuf = pTGetBuf + 2;
            ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_YDN_LIST), pTGetBuf);
        }
        PostPage(pHtml);

        DELETE(pHtml);
        pHtml = NULL;

    }

    DELETE(szPath);
    szPath = NULL;


}

static int Web_SET_PostCFGGCPSWorkPage(IN char *pGetBuf, IN int iLanguage)
{
	ASSERT(pGetBuf);

	char	*pHtml = NULL;
	char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	szResult[3];

	char        *pszSampling = NULL;
	char        *pszControl  = NULL;
	char        *pszSetting  = NULL;
	char        *pszAlarm    = NULL;
	char        *pszEquip    = NULL;
	char        *pszGCPSInfo = NULL;
	char        *pszTemp     = NULL;
	char        *pszGCPSMode = NULL;
	//Read file
	pTGetBuf = pGetBuf;

	strncpyz(szResult, pTGetBuf, sizeof(szResult));


	char	*ptr = NULL;
	ptr = pGetBuf + 2;

	/*OEquip*/
	char		*pPosition = strchr(ptr,59);
	int			iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszEquip = NEW(char, iPosition + 1);
		if(pszEquip == NULL)
		{
			return FALSE;
		}
		memset(pszEquip, 0x0, (size_t)iPosition);
		strncpyz(pszEquip, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	//fwrite(pszEquip,strlen(pszEquip),1,pf);


	/*OSampling*/
	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr ;
	if(iPosition > 0)
	{
		pszSampling = NEW(char, iPosition + 1);
		if(pszSampling == NULL)
		{
			return FALSE;
		}
		memset(pszSampling, 0x0, (size_t)iPosition);
		strncpyz(pszSampling, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	//fwrite(pszSampling,strlen(pszSampling),1,pf);

	/*OControl*/
	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszControl = NEW(char, iPosition + 1);
		if(pszControl == NULL)
		{
			return FALSE;
		}
		memset(pszControl, 0x0, (size_t)iPosition);
		strncpyz(pszControl, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;
	//fwrite(pszControl,strlen(pszControl),1,pf);

	/*OSetting*/
	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszSetting = NEW(char, iPosition + 1);
		if(pszSetting == NULL)
		{
			return FALSE;
		}
		memset(pszSetting, 0x0, (size_t)iPosition);
		strncpyz(pszSetting, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;
	//fwrite(pszSetting,strlen(pszSetting),1,pf);


	/*OAlarm*/

	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszAlarm = NEW(char, iPosition + 1);
		if(pszAlarm == NULL)
		{
			return FALSE;
		}
		memset(pszAlarm, 0x0, (size_t)iPosition);
		strncpyz(pszAlarm, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	/*GCPSConfigInfo*/

	//iPosition = strlen(ptr);
	pPosition = strchr(ptr,59);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszGCPSInfo = NEW(char, iPosition + 1);
		if(pszGCPSInfo == NULL)
		{
			return FALSE;
		}
		memset(pszGCPSInfo, 0x0, (size_t)iPosition);
		strncpyz(pszGCPSInfo, ptr, iPosition + 1);
		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	/*power_splite_mode*/
	iPosition = strlen(ptr);
	if(iPosition > 0)
	{
		pszGCPSMode = NEW(char, iPosition + 1);
		if(pszGCPSMode == NULL)
		{
			return FALSE;
		}
		memset(pszGCPSMode, 0x0, (size_t)iPosition);
		strncpyz(pszGCPSMode, ptr, iPosition + 1);

	}

	//fwrite(pszAlarm,strlen(pszAlarm),1,pf);


	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[10], iLanguage);


	//fwrite(szPath,strlen(szPath),1,pf);

	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equipments"), pszEquip);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_sample_signals"), pszSampling);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_control_signals"), pszControl);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_setting_signals"), pszSetting);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("equip_alarm_signals"), pszAlarm);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("configuration_file"), pszGCPSInfo);
		ReplaceString(&pHtml, MAKE_VAR_FIELD("power_splite_mode"), pszGCPSMode);


		PostPage(pHtml);

		DELETE(pHtml);
		DELETE(pszEquip);
		DELETE(pszSampling);
		DELETE(pszControl);
		DELETE(pszSetting);
		DELETE(pszAlarm);
		DELETE(pszGCPSInfo);
		DELETE(pszGCPSMode);

		pszSampling = NULL;
		pszControl  = NULL;
		pszSetting  = NULL;
		pszAlarm    = NULL;
		pszEquip    = NULL;
		pHtml       = NULL;

	}

	//fclose(pf);

	DELETE(szPath);
	szPath = NULL;

	return 1;
}

static int Web_SET_PostCFGGetSettingParamWorkPage(IN char *pGetBuf, IN int iLanguage)
{
	ASSERT(pGetBuf);
	//FILE *pf;
	//pf=fopen("/var/wj4.txt","a+");
	//char *p="into Web_SET_PostCFGPLCWorkPage OK!!\n";
	//fwrite(p,strlen(p),1,pf);


	char	*pHtml = NULL;
	char	*szPath = NULL;
	char	*pTGetBuf = NULL;
	char	szResult[4];

	pTGetBuf = pGetBuf;

	strncpyz(szResult, pTGetBuf, 3);

	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[11], iLanguage);

	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{
		//TRACE("LoadHtmlFile OK!!");

		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), szResult);
		//fwrite(pHtml,strlen(pHtml),1,pf);


		printf("%s",pHtml);

		DELETE(pHtml);

		pHtml  = NULL;

	}

	//fclose(pf);

	DELETE(szPath);
	szPath = NULL;

	return 1;


}

static int Web_SET_PostAutoConfigWorkPage(iLanguage)
{
	//ASSERT(pGetBuf);


	char *p="into Web_SET_PostAutoConfigWorkPage OK!!\n";



	char	*pHtml = NULL;
	char	*szPath = NULL;
	//char	*pTGetBuf = NULL;
	//char	szResult[4];

	//pTGetBuf = pGetBuf;

	//strcpyz(szResult, "1 ", 1);

	szPath = Web_SET_MakeConfigurePagePath(CFG_HTML_PATH[12], iLanguage);

	if(LoadHtmlFile(szPath, &pHtml ) > 0)
	{
		//TRACE("LoadHtmlFile OK!!");

		ReplaceString(&pHtml, MAKE_VAR_FIELD("CONTROL_RESULT"), "1");
		//fwrite(pHtml,strlen(pHtml),1,pf);


		printf("%s",pHtml);

		DELETE(pHtml);

		pHtml  = NULL;

	}

	//fclose(pf);

	DELETE(szPath);
	szPath = NULL;

	return 1;


}

//end////////////////////////////////////////////////////////////////////////



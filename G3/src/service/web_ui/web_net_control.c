/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>



/*For control & setting*/
#define CGI_SIGNAL_ID				"SignalID"
#define	CGI_SIGNAL_TYPE				"Type"
#define	CGI_SIGNAL_TYPEX			"Typex"
#define	CGI_SIGNAL_TYPEY			"Typey"
#define CGI_EQUIPMENT_ID			"EquipID"
#define CGI_INPUT_VALUE				"InputValue"



typedef struct tagCommandInfo
{
	int		iSignalID;		
	int		iSignalType;		
	int		iEquipID;
	int		iValueType;
	char	szChangeValue[32];		
}ControlCommandInfo;



static void Web_SET_PostUserInfoPage(IN char *pGetBuf,IN int iLanguage);
static int Web_SET_SendConfigureCommand(IN char *szBuf);	
static int Web_SET_GetCommandParam(IN ControlCommandInfo *stConfigure, OUT char **pBuf);
static void Web_SET_PostControlPage(IN char *ptrReturn);


int main(void)
{
	char						*szGetBuf = NULL;
	int							iReturn = 0;
	ControlCommandInfo			stConfigureCommand;
	
	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");
	/*get parameter*/
	if (Web_SET_GetCommandParam(&stConfigureCommand, &szGetBuf))
	{
		if((iReturn = Web_SET_SendConfigureCommand(szGetBuf)) >= 0)
		{
			//Web_SET_PostControlPage(iReturn);
		}
	}
	else
	{
	}

	if(szGetBuf)
	{
		DELETE(szGetBuf);
		szGetBuf = NULL;
	}
	return TRUE;

}

static int Web_SET_SendConfigureCommand(IN char *szBuf)	
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	int		iBufCount = 0;
	int		iModifyPassword = 0;

	/*create FIFO with our PID as part of name*/

 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return -1;
	}

	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return -1;
	}
	
	/*start buffer with pid and a blank*/
	iLen = sprintf(szBuf1,"%10ld%2d%2d%-128s",(long)getpid(), WEB_SUPPORT_NETSCAPE,SET_NET_CONTROL, szBuf);
		
	if((write(fd, (void *)szBuf1, iLen + 1))<0)
	{
		close(fd);
		return -1;
	}

	if((fd2 = open(fifoname,O_RDONLY)) < 0)
	{
 		return -1;
	}
	
	if((iLen = read(fd2, szBuf2, PIPE_BUF - 1)) > 0)
	{
		Web_SET_PostControlPage(szBuf2);

	}

	close(fd2);
	close(fd);
	unlink(fifoname);

	return atoi(szBuf2);
}

static int Web_SET_GetCommandParam(IN ControlCommandInfo *stConfigure, OUT char **pBuf)
{
		
	//#define HTTP_COOKIE				getenv("HTTP_COOKIE")
	//printf("Content-type:text/html\n\n");
	//char		*postinput;
	//int		content_length;
	//unsigned char		*pHTTPBuffer = NULL;

	//postinput = getenv("CONTENT_LENGTH");

	//if (!postinput)
	//	return NULL;
	//if(!(content_length = atoi(postinput)))
	//	return NULL;
	//if(!(pHTTPBuffer = NEW(char, (size_t)content_length + 1)))
	//{
	//	return NULL;
	//}
	//if(!(postinput = NEW(unsigned char,(size_t)content_length + 1)))
	//	return NULL;
	//if(!fread(pHTTPBuffer, (size_t)content_length, (size_t)1, stdin))
	//	return NULL;
	//strncpyz(postinput, pHTTPBuffer, content_length + 1);
	//printf("pHTTPBuffer = %s\n", pHTTPBuffer);
	//exit(1);



	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL;
	char	*szSendBuf = NULL;
	int		iLen = 0;

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}

	szSendBuf = NEW(char, 128);
	if(szSendBuf == NULL)
	{
		return FALSE;
	}
#define CGI_APPLY_VALUE				"159"
#define CGI_APPLY_LENGTH			sizeof("apply")
	char *ptr1 = NULL;
	if((val = getValue( getvars, postvars, CGI_APPLY_VALUE )) != NULL)
	{
        ptr1 = val + CGI_APPLY_LENGTH -1;
	}


	if((val = getValue(getvars,postvars,CGI_EQUIPMENT_ID)) != NULL)
	{
		stConfigure->iEquipID = atoi(val);
	}

	if(atoi(ptr1) > 5000)
	{
		if((val = getValue( getvars, postvars, CGI_SIGNAL_TYPEY )) != NULL)
		{
			stConfigure->iSignalType = atoi(val);
		}
	}
	else if(atoi(ptr1) > 900)
	{
		if((val = getValue( getvars, postvars, CGI_SIGNAL_TYPEX )) != NULL)
		{
			stConfigure->iSignalType = atoi(val);
		}
	}
	else
	{
		if((val = getValue( getvars, postvars, CGI_SIGNAL_TYPE )) != NULL)
		{
			stConfigure->iSignalType = atoi(val);
		}
	}

	char szSignalID[32];
	if(atoi(ptr1) > 5000)
	{
		sprintf(szSignalID, "%s%d%d",CGI_SIGNAL_ID, stConfigure->iSignalType,atoi(ptr1 + 2));
	}
	else if(atoi(ptr1) > 900)
	{
		sprintf(szSignalID, "%s%d%d",CGI_SIGNAL_ID, stConfigure->iSignalType,atoi(ptr1 + 1));
	}
	else
	{
		sprintf(szSignalID, "%s%d%d",CGI_SIGNAL_ID, stConfigure->iSignalType,atoi(ptr1));
	}

	//printf("Content-type:text/html\n\nszSignalID:{%s}", szSignalID);

	if((val = getValue( getvars, postvars, szSignalID )) != NULL)
	{
		stConfigure->iSignalID = atoi(val);		
	}


	

	char szGetValueStr[32];
	if(atoi(ptr1) > 5000)
	{
		sprintf(szGetValueStr, "%s%d%d", CGI_INPUT_VALUE,stConfigure->iSignalType, atoi(ptr1+2));
	}
	else if(atoi(ptr1) > 900)
	{
		sprintf(szGetValueStr, "%s%d%d", CGI_INPUT_VALUE,stConfigure->iSignalType, atoi(ptr1+1));
	}
	else
	{
		sprintf(szGetValueStr, "%s%d%d", CGI_INPUT_VALUE,stConfigure->iSignalType, atoi(ptr1));
	}

	//printf("Content-type:text/html\n\szGetValueStr:{%s}", szGetValueStr);
	if((val = getValue(getvars,postvars,szGetValueStr)) != NULL)
	{
		strncpyz(stConfigure->szChangeValue,val, sizeof(stConfigure->szChangeValue));
	}



	iLen += sprintf(szSendBuf + iLen, "%3d",stConfigure->iEquipID);
	iLen += sprintf(szSendBuf + iLen, "%3d",stConfigure->iSignalType);
	iLen += sprintf(szSendBuf + iLen, "%3d",stConfigure->iSignalID);
	iLen += sprintf(szSendBuf + iLen, "%s",stConfigure->szChangeValue);
	/*
	printf("Content-type:text/html\n\n [%d][%d][%d][%s]", stConfigure->iEquipID,stConfigure->iSignalType,stConfigure->iSignalID,stConfigure->szChangeValue);
	printf("\n");



	exit(1);*/
 	*pBuf = szSendBuf;


	cleanUp(getvars, postvars);
    return TRUE;  
}

static char szReturnValue[6][100] = {"Successfully to send the control.",
                            "Signal Value Invalid,Please input again.",
                            "ACU Hardware Protected.",
							"Exceed the value range,Please input again",
							"ACU is auto status,can't control",
							"This signal is suppressed by other singal,can't control"};

static void Web_SET_PostControlPage(IN char *ptrReturn)
{

//�ѷ��صĽ��д��error.htm�ļ���,������/var/netscapeĿ¼��
	//���ļ�
#define CGI_NET_PATH_ERROR_FILE			"/app/www/netscape/pages/errorcontrol.htm"	
										 
#define CGI_NET_CONTROL_VALUE			"/*[ID_RETURN]*/"
	char	*pHtml = NULL;
	
	if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
	{
		printf("Content-type:text/html\n\n");
		ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, ptrReturn);
		
		PostPage(pHtml);
		DELETE(pHtml);
		pHtml = NULL;
	}
	

	
}




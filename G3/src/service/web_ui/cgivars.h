/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgivars.h
 *  CREATOR  : Yang Guoxin              DATE: 2004-09-28 10:15
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/



#ifndef _CGIVARS_H
#define _CGIVARS_H

/* method */
#define GET		0
#define POST	1


/* function prototypes */
int getRequestMethod( void );
char **getGETvars(void );
char **getPOSTvars( void );

int cleanUp( char **getvars, char **postvars);

char *getValue( char **getVars, char **postVars, const char *var );
char hex2char(char *hex);
void unescape_url(char *url);
#endif	/* !_CGIVARS_H */

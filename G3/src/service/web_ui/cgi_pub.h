/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi-pub.h
 *  CREATOR  : Yang Guoxin                   DATE: 2004-09-28 10:13
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef _CGI_PUB_H
#define	_CGI_PUB_H


#include "stdsys.h"
#include "public.h"
#include "cgivars.h"

/*
#include "stdio.h"
#include "sys/types.h"
#include "string.h"
#include "time.h"
#define IN
#define OUT
*/
#include "unistd.h"
#include "netdb.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <utime.h>
#include <unistd.h>

#include "cgivars.h"

#include "cgi_suicide_timer.h"

#define MAKE_VAR_FIELD(var)				("/*[" var "]*/")

#define LOG_IN							0			//First Connect
#define REALTIME_DATA					1			//get realtime data
#define	CONTROL_COMMAND					2			//control
#define	HISTORY_QUERY					3			//history query
#define	CONFIGURE_MANAGE				4			//config manage
#define FILE_MANAGE						5			//file manage
#define FILE_MANAGE_REPLACEFILE			6			//file manage
//Start:Added by YangGuoxin,8/23/2006
#define WEB_SUPPORT_NETSCAPE			7





enum _NETSCAPE_CONTROL
{
	SET_NET_CONTROL	= 0,
	SET_NMS_SETTING = 8,
	GET_NMS_SETTING,
	SET_EEM_SETTING,
	GET_EEM_SETTING,
	SET_IP_SETTING,
	GET_IP_SETTING,
	GET_TIME_SETTING,
	SET_TIME_SETTING,
	GET_LANG_SETTING,
	SET_LANG_SETTING,
	SET_RECOVER_RESET,
};

enum _NETSCAPE_EEM_TYPE
{
	NET_EEM_GENEARL = 1,
	NET_EEM_PSTN,
	NET_EEM_TCPIP,
	NET_EEM_LEASDLINE,
};

//Start:Added by YangGuoxin,12/20/2006
#define WEB_SUPPORT_NET_SETTING			8
//End:Added by YangGuoxin,12/20/2006
//Start:Added by Wankun, 03/03/2007
#define HISTORY_QUERY_NETSCAPE			9
//End:Added by Wankun, 03/03/2007




#define EQUIP_SAMPLE_SIGNALS			"equip_sample_signals"
#define EQUIP_CONTROL_SIGNALS			"equip_control_signals"
#define EQUIP_SETTING_SIGNALS			"equip_setting_signals"
#define EQUIP_STATUS_SIGNALS			"equip_status_signals"
#define EQUIP_ALARM_SIGNALS				"equip_Alarm_signals"
#define EQUIP_RALARM_SIGNALS			"equip_RAlarm_signals"
#define EQUIP_LIST						"equip_list"
#define EQUIP_ALARM_DATE				"VAR_LAST_ALARM_TIME"
#define EQUIP_NEED_REFRESH_RECTIFIER	"VAR_NEED_REFRESH_RECTIFIER"
#define EQUIP_NEED_REFRESH_ACD	        "VAR_NEED_REFRESH_ACD"
#define EQUIP_NEED_REFRESH_DCD	        "VAR_NEED_REFRESH_DCD"
#define EQUIP_NEED_REFRESH_BATT	        "VAR_NEED_REFRESH_BATT"
#define CGI_NOW_TIME					"VAR_NOW_TIME"

/*CGI Param*/
#define EQUIP_NAME						"equip_name"
#define SESSION_ID						"sessionId"
#define CGI_FILE_NAME					"file_name"
#define	EQUIP_ID						"equip_ID"
#define	LANGUAGE_TYPE					"language_type"
#define ALARM_NEW_LEVEL					"signal_new_level"
#define	NEED_RETURN_STRUCTURE			"need_return_structure"
#define CONTROL_VALUE					"control_value"
#define CONTROL_TYPE					"control_type"
#define SIGNAL_TYPE						"signal_type"
#define SIGNAL_ID						"signal_id"
#define VALUE_TYPE						"value_type"
#define SIGNAL_MODIFY_NAME_TYPE			"signal_modify_type"
#define CGI_CLEAR_SESSION				"clear_session"

#define AUTO_CONFIG_START_FALGE           	"VAR_AUTO_CONFIG_START_FLAGE"//Added by wj for Auto Config
#define SLAVE_MODE_FALGE           	"VAR_SLAVE_MODE_FALGE"//Added by wj for Auto Config
#define SMDU_CFG_FALGE           	"VAR_SMDU_CFG_FALGE"//Added by wj for Auto Config
#define EQUIP_NEED_REFRESH_CONVERT	"VAR_NEED_REFRESH_CONVERTER"

#define MAIN_FIFO_NAME					"/var/fifo3"
#define CGI_CLIENT_FIFO_PATH			"/var"

/*max len of fifo file name*/
#define FIFO_NAME_LEN					32


/**/
#define QUERY_HIS_DATA				0
#define QUERY_STAT_DATA				1
#define QUERY_HISALARM_DATA			2
#define QUERY_CONTROLCOMMAND_DATA	3
#define QUERY_HISLOG_DATA			4
#define QUERY_BATTERYTEST_DATA		5
#define QUERY_CLEAR_ALARM			6
#define QUERY_DISEL_TEST			7
//Start:Added by Wankun, 03/14/2007
#define QUERY_BATTERYTEST_SUM		8
#define CLEAR_HISTORY_DATA			9
#define CLEAR_HISTORY_LOG			10
#define CLEAR_COMMAND_LOG			11
#define CLEAR_BAT_TEST_LOG			12
//End:Added by Wankun, 03/14/2007


/*command type*/
#define MODIFY_SIGNAL_NAME					0
#define MODIFY_SIGNAL_VALUE					1
#define MODIFY_SIGNAL_ALARM_LEVEL			2
#define MODIFY_EQUIP_NAME					3
#define MODIFY_ACU_INFO						4
#define GET_EQUIP_SIGNAL_INFO				5
#define MODIFY_ALARM_ALL_INFO				6
#define GET_ACU_INFO						7
#define GET_DEVICE_LIST						8
#define CLEAR_HISTORY_DATA					9
#define RESTORE_DEFAULT_CONFIG				10
#define RESTART_ACU							11
#define GET_USER_DEF_PAGE					12
#define SET_USER_DEF_PAGE					13

/*Multi Language*/
#define	ENGLISH_LANGUAGE_NAME			0
#define	LOCAL_LANGUAGE_NAME 			1
#define	LOCAL2_LANGUAGE_NAME 			2  ////Added by wj for three languages 2006.4.29

/*aim to send equip list*/
#define ENGLISH_START					59//198 ;
#define LOC2_START                      59//Added by wj for three languages 2006.4.29
#define TREE_LOC_START					59//204	;
#define TREE_LOC2_START					59//204	;//Added by wj for three languages 2006.4.29
#define TREE_ENG_START					59//201	
#define	ACU_LOC_INFO_START				59//202
#define	ACU_LOC2_INFO_START				59//202//Added by wj for three languages 2006.4.29
#define	ACU_ENG_INFO_START				59//203
#define LOC_EQUIPALARM_INFO_START		59
#define LOC2_EQUIPALARM_INFO_START		59//Added by wj for three languages 2006.4.29
#define ENG_EQUIPALARM_INFO_START		59
#define LOC_GROUP_DEVICE_START			59
#define LOC2_GROUP_DEVICE_START			59//Added by wj for three languages 2006.4.29
#define ENG_GROUP_DEVICE_START			59

/*aim to send realtime data*/
#define END_OF_SIGNUALNUM				59//197
#define END_OF_SAMPLESIG				59//198
#define	END_OF_CONTROLSIG				59//200
#define END_OF_SETTINGSIG				59//201
#define END_OF_ALARMSIG					59//202	
#define END_OF_RALARMSIG				59//203
#define END_OF_STATUSSIG				59//204
#define END_OF_RALARM					59




/*define Macro return to the cgi process*/
#define	MODIFY_SIGNALNAME_SUCCESSFUL	0
#define MODIFY_SIGNALNAME_FAIL			1
#define MODIFY_SIGNALVALUE_SUCCESSFUL	1
#define MODIFY_SIGNALVALUE_FAIL			2
#define MODIFY_SIGNALVALUE_INVALID      3
#define MODIFY_ALARMLEVEL_SUCCESSFUL	4
#define	MODIFY_ALARMLEVEL_FAIL			5
#define MODIFY_EQUIP_NAME_SUCCESSFUL	6
#define MODIFY_EQUIP_NAME_FAIL			7
#define	MODIFY_ACU_INFO_SUCCESSFUL		8
#define MODIFY_ACU_INFO_FAIL			9


//Added by YangGuoxin,9/4/2006
#define MODIFY_SIGNALVALUE_SUPPRESSED		10
//End by YangGuoxin,9/4/2006


#define NO_AUTHORITY					4      /*authority*/



/*define log file head*/
#define CGI_APP_LOG_COMM_NAME			"WEB COMM CGI"
#define CGI_APP_LOG_CONTROL_NAME		"WEB CTRL CGI"
#define CGI_APP_LOG_QUERY_NAME			"WEB QURY CGI"
#define CGI_APP_LOG_REALTIME_NAME		"WEB REAL CGI"
#define CGI_APP_LOG_CONFIGURE_NAME		"WEB COFG CGI"
#define CGI_APP_LOG_APPMAIN_NAME		"WEB MAIN CGI"
#define CGI_APP_LOG_APPPUB_NAME			"WEB PUB  CGI"


#define HTML_SAVE_REAL_PATH				"/app/www/html/cgi-bin/loc"//"/var/tmp"
#define HTML_SAVE_REAL2_PATH			"/app/www/html/cgi-bin/loc2"//"/var/tmp" //Added by wj for three languages 2006.4.28
#define HTML_SAVE_ENG_PATH				"/app/www/html/cgi-bin/eng"//"/var/tmp"

#define WEB_LOC_VAR						"/var/loc"
#define WEB_LOC2_VAR					"/var/loc2"//Added by wj for three languages 2006.4.28
#define WEB_ENG_VAR						"/var/eng"

#define	WEB_CONFIG_PATH					"/app/config"
#define WEB_RUN_CFG_PATH				"/app/config/run"

#define CGI_APP_OVERTIME_PAGE			"p17_login_overtime.htm"
/*Need return signal structure*/
#define NEED_RETURN_SIGNAL_STRUCTURE	1

#define GET_NETWORK_INFO				0
#define SET_NETWORK_INFO				1
#define GET_NMS_INFO					2
#define SET_NMS_INFO					3
#define GET_ESR_INFO					4		
#define SET_ESR_INFO					5	
#define GET_TIME_INFO					6
#define SET_TIME_VALUE					7
#define SET_TIME_IP						8
#define GET_USER_INFO					9			/*Get All user*/
#define SET_USER_INFO					10			/*Modify user info*/
#define ADD_USER_INFO					11			/*Add user info*/
#define DELETE_USER_INFO				12			/*Delete user info*/
#define SET_USER_PASSWORD				13			/*Modify user info*/

//Added by YangGuoxin; 1/19/2006
//Get prodcut info
#define GET_PRODUCT_INFO				14			
//End Added by YangGuoxin; 1/19/2006



#define CGI_GET_SETTING_TYPE			"setting_type"
#define CGI_GET_AUTH_NAME				"cgi_auth_name"
#define CGI_ADMIN_NAME					"admin"
#define CGI_READ_NAME					"read"
#define CGI_WRITE_NAME					"write"
#define CGI_DI_DISABLED					"DI_DISABLED"

//////////////////////////////////////////////////////////////////////////
//Added by wj for Config PLC private config file 2006.5.13

#define SET_PLC_CONFIG                  15
#define DEL_PLC_CONFIG                  16
#define GET_PLC_CONFIG                  17
//Added by wj for AlarmSupExp Configuration 2006.5.22
#define GET_ALARM_CONFIG                18
#define SET_ALARM_CONFIG	            19
//Added by wj for AlarmReg Configuration 2006.6.23
#define GET_ALARMREG_CONFIG	            20
#define SET_ALARMREG_CONFIG	            21
//Added by wj for YDN23 Configuration 2006.7.26
#define GET_YDN_CONFIG	            22
#define SET_YDN_CONFIG	            23
//add by wj for Power splite
#define GET_GC_PS_CONFIG                   24
#define SET_GC_PS_CONFIG                   25
#define SET_GC_PS_MODE                     26

#define GET_SETTING_PARAM                  27
#define GET_AUTO_CONFIG                    28
#define GET_NMSV3_INFO					29
#define SET_NMSV3_INFO					30
//end////////////////////////////////////////////////////////////////////////


#define SESSION_DEFAULT_DIR				"/var/sess_"
#define SPACE   (0x20)
#define TAB     (0x09)
#define WEB_IS_WHITE_SPACE(c)         ( ((c) == SPACE) || ((c) == TAB) )

char *set_session(IN char *name,IN char *pwd,IN char *strAuthority);
int start_session(IN const char *sessionId);
void kill_session(IN char *sessionId);
void print_session_error(IN const int n, IN int iLanguage);
void print_no_authority_error(IN const int n);
void clean_session_file(void);

#ifdef _CODE_FOR_MINI
int find_session_file(void);
#endif

int ReplaceString( char **ppsrc, const char *strOld, char *strNew );

//void MakeModiTime(OUT char * strModiTime,int nLen);
int LoadHtmlFile(IN const char *szFile, OUT char **pbuf);
void PostPage(IN char * pbuf);
long FileLength(IN FILE *fp );
char *CGI_PUB_GetData(IN char *ptr, IN char cSplit);
char *Web_RemoveWhiteSpace(IN char *pszBuf);
char *Web_SET_MakeConfigurePagePath(IN const char *szPrePath, IN int iLanguage);
int ResetTime(IN const char *sessionId);
int start_sessionA(IN const char *sessionId, IN char **szUserName);
int CGI_ClearProc(char *lpMsg);
void CGI_ClearSessionFile(IN char *szSessionID);
void refresFileTime(void);
char *getUserName(IN char *sessionId);
char *Cfg_RemoveWhiteSpace(IN char *pszBuf);
BOOL Web_CheckUser(IN int nPagesNeedAuthority);

enum WEB_NET_SETTING_R
{
	WEB_NET_SETTING_IP = 0,
	WEB_NET_SETTING_EEM,
	WEB_NET_SETTING_TIME,
	WEB_NET_SETTING_NMS
};

enum WEB_NET_PAGES
{
	WEB_NET_PAGES_READ,
	WEB_NET_PAGES_WRITE,
	WEB_NET_PAGES_ADMIN,
};
BOOL IsValidWORD(const char *pField);

#endif
/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : control_command.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-10-13 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"
#include "cgi_pub.h"
#include "cgivars.h"
#include "stdlib.h"

/*control type*/
#define CGI_CTL_RETURN_RESULT				"CONTROL_RESULT"
#define CGI_CTL_ALARM_SIGNAL_INFO			"non_realtime_alarm"
#define CGI_CTL_SAMPLE_SIGNAL_INFO			"_sample_signal_info"
#define CGI_CTL_CONTROL_SIGNAL_INFO			"_control_signal_info"
#define CGI_CTL_SETTING_SIGNAL_INFO			"_setting_signal_info"
#define CGI_CTL_RETURN_EQUIPID				"_return_equip_id"
#define CGI_CTL_RETURN_SIGNALTYPE			"_return_signal_type"
#define CGI_CTL_ACU_INFO					"acu_information"
#define CGI_CTL_DEVICE_LIST					"equip_list"

#define PAGE_SIG_INFO				"user_def_page_sig_info"
#define PAGE_SIG_VAL				"user_def_page_sig_val"


/*authority fail*/
#define AUTHORITY_FAIL						6

#define	HTML_SAVE_PATH						"/app/www/html/cgi-bin/loc"//"/var/tmp"  

/*file path*/


static char	html_file[9][100] = {"/p05_equip_sample.htm",			//Modify signal name
									"/p06_equip_control.htm",			//Modify signal name, Control value
									"/p07_equip_setting.htm",			//Modify signal name, Control value	
									"/p29_online_modifyalarm.htm",		//Modify alarm
									"/p28_online_modifydevice.htm",		//Modify Equipment
									"/p27_online_modifysystem.htm",		//Modify ACU Information
									"/p36_clear_data.htm",
									"/p18_restore_default.htm",
									"/p81_user_def_page.htm"};
/*return control result */
static char 	control_result[20];
char			strModiTime[50];

char			*szAlarmSignal = NULL;
char			*szSampleSignal = NULL;
char			*szControlSignal = NULL;
char			*szSettingSignal = NULL;
char			szEquipID[6];
char			szSignalType[6];
char			*szACUInfo = NULL;
char			*szDeviceList = NULL;
char			*pSigInfo = NULL;
char			*pSigVal = NULL;
typedef struct tagControlCommand
{
	int			iControl;           /*it is control command*/
	int			iLanguage;			/*language*/
	char		szSessionID[64];			/*sessionID*/
	int			iEquipID;	 
	long		iSignalType;
	long		iSignalID;
	int			iControlType;		/*[0] Modify Signal Name [1]Control value [2]Modify alarm level*/
	int			iValueType;			/*5: enum 2:float*/
	int			iAlarmNewLevel;
	int			iModifyNameType;
	char		value[100];
}STRU_CONTROL_COMMAND;

#define NAME_ERROR   "(null)"

static int Web_CTL_GetCommandParam(OUT STRU_CONTROL_COMMAND *pBuf);
static int Web_CTL_SendCommandControl(IN STRU_CONTROL_COMMAND *pCommandBuf, 
									  IN char *szUserName);
static void Web_CTL_LoadDataPtr(IN char *pBuf);
//static void StuffHead(  char *pszHead, int iLenContent );
static void Web_CTL_LoadDevicePtr(IN char *pBuf);
static void Web_CTL_LoadACUPtr(IN char *pBuf);
static void Web_CTL_LoadUserDefPage(IN char *pBuf);

/*==========================================================================*
 * FUNCTION :  main
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-15 09:20
 *==========================================================================*/
int main(void)
{
	STRU_CONTROL_COMMAND		stCmdBuf;
	int							i = 0;
	int							iLanguage = 1;				
	char						*pHtml = NULL;
	//char						szExchange[8];
	char						*szHtmlPath = NULL;
	int							iAuthority = 0;
	char						*szUserName = NULL;
	printf("Content-type:text/html\n\n");

	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");

		/*get parameter*/
	if (Web_CTL_GetCommandParam(&stCmdBuf) == FALSE)
	{		
		return FALSE;
	}


#define WEB_RETURN_NO_AUTHORITY				"3"
	/*Judge which file will be loaded*/
 	if(	stCmdBuf.iControlType== MODIFY_SIGNAL_VALUE)				/*[0] Modify Signal Name*/
	{
		 i = stCmdBuf.iSignalType;
	}
	else if(stCmdBuf.iControlType == MODIFY_SIGNAL_ALARM_LEVEL || 
			stCmdBuf.iControlType == GET_EQUIP_SIGNAL_INFO ||
			stCmdBuf.iControlType == MODIFY_SIGNAL_NAME ||
			stCmdBuf.iControlType == MODIFY_ALARM_ALL_INFO)			/*[2]Modify alarm level*/
	{
		i = MODIFY_SIGNAL_ALARM_LEVEL + 1;
	}
	else if(stCmdBuf.iControlType == MODIFY_EQUIP_NAME ||
				stCmdBuf.iControlType == GET_DEVICE_LIST)
	{
		i = MODIFY_EQUIP_NAME + 1;		
	}
	else if(stCmdBuf.iControlType == MODIFY_ACU_INFO || 
				stCmdBuf.iControlType ==  GET_ACU_INFO)
	{
		i = MODIFY_ACU_INFO + 1;
	}
	else if(stCmdBuf.iControlType == CLEAR_HISTORY_DATA)
	{
		i = 6;
	}
	else if(stCmdBuf.iControlType == RESTORE_DEFAULT_CONFIG || stCmdBuf.iControlType == RESTART_ACU)
	{
		i = 7;
	}
	else if(stCmdBuf.iControlType == GET_USER_DEF_PAGE || stCmdBuf.iControlType == SET_USER_DEF_PAGE)
	{
		i = 8;
	}

	else
	{
		//AppLogOut(CGI_APP_LOG_CONTROL_NAME,LOG_WARNING,"Fail to get correct Command Type");
		TRACE("---------------");
		return FALSE;
	}

	/*language type*/
	if(stCmdBuf.iLanguage > 0)
	{
        if(stCmdBuf.iLanguage == 1)
        {
            iLanguage =	LOCAL_LANGUAGE_NAME;
        }
        else//Added by wj for three languages 2006.4.29
        {
            iLanguage =	LOCAL2_LANGUAGE_NAME;
        }
		
	}
	else
	{
		iLanguage = ENGLISH_LANGUAGE_NAME;
	}
	/*validate modify*/
	
	if(i >= 0 && i <= 8)
	{
		
//		sprintf(szHtmlPath,"%s%s",HTML_SAVE_PATH,html_file[i]);
		szHtmlPath = Web_SET_MakeConfigurePagePath(html_file[i], iLanguage);
		if(LoadHtmlFile(szHtmlPath, &pHtml ) > 0 )
		{
			if(szHtmlPath != NULL)
			{
				DELETE(szHtmlPath);
				szHtmlPath = NULL;
			}
			/*judge overtime and authority*/

			iAuthority = start_sessionA(Web_RemoveWhiteSpace(stCmdBuf.szSessionID), &szUserName);
			//iAuthority = 4;
		
            //if(!strcmp(szUserName,NAME_ERROR))
            {

                //DELETE(pHtml);
                //pHtml = NULL;

                //DELETE(szUserName);
                //szUserName = NULL;

                //print_session_error(iAuthority,iLanguage);
                //printf("%s\n",szUserName);
                //printf("%d\n",iAuthority);
                //return TRUE;
            }

			//not the authority fault
			if(iAuthority < 0)		
			{
				//if(stCmdBuf.iControlType == GET_EQUIP_SIGNAL_INFO ||
				//		stCmdBuf.iControlType == GET_ACU_INFO ||
				//			stCmdBuf.iControlType == GET_DEVICE_LIST ||
                //            stCmdBuf.iControlType == MODIFY_SIGNAL_VALUE)
				//{
					DELETE(pHtml);
					pHtml = NULL;

					DELETE(szUserName);
					szUserName = NULL;

					print_session_error(iAuthority,iLanguage);
					return FALSE;
				//}
			}
			else
			{
				//1 - Browser     2 - operator
				//3 - Engineer    4-Administrator
				//Engineer and administrator have privelege
				//to control command;
				//Brower and operator have no privelege.
			
				if(iAuthority < 2 && 
						stCmdBuf.iControlType == MODIFY_SIGNAL_VALUE )
				{

					ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), WEB_RETURN_NO_AUTHORITY);
					PostPage(pHtml);

					DELETE(pHtml);
					pHtml = NULL;

					DELETE(szUserName);
					szUserName = NULL;

					return FALSE;
				}				
				else if(iAuthority < 3 )	
				{
	 				/*Return no authoirty*/
					//sprintf(szExchange,"%s",NO_AUTHORITY);
					if(stCmdBuf.iControlType == MODIFY_ACU_INFO ||
							stCmdBuf.iControlType == MODIFY_SIGNAL_NAME ||
							stCmdBuf.iControlType == MODIFY_SIGNAL_ALARM_LEVEL ||
							stCmdBuf.iControlType == MODIFY_EQUIP_NAME ||
							stCmdBuf.iControlType == MODIFY_ALARM_ALL_INFO ||
							stCmdBuf.iControlType == CLEAR_HISTORY_DATA ||
							stCmdBuf.iControlType == RESTORE_DEFAULT_CONFIG || 
							stCmdBuf.iControlType == RESTART_ACU)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "3");
						PostPage(pHtml);

						DELETE(pHtml);
						pHtml = NULL;

						DELETE(szUserName);
						szUserName = NULL;

						return FALSE;
					}
				}
                

			}

			
			/*resend command*/
			
			if(Web_CTL_SendCommandControl(&stCmdBuf, szUserName) != FALSE)
			{
				/*get control result*/
				
			
				if(stCmdBuf.iControlType == MODIFY_ACU_INFO || 
						stCmdBuf.iControlType ==  GET_ACU_INFO)
				{
					if(szACUInfo != NULL)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_ACU_INFO), szACUInfo);
						//ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "1");
					}
					//else
					//{
						//ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "0");
					//}
					ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), control_result);
				}
				else  if(stCmdBuf.iControlType == MODIFY_SIGNAL_ALARM_LEVEL || 
							stCmdBuf.iControlType == GET_EQUIP_SIGNAL_INFO ||
							stCmdBuf.iControlType == MODIFY_SIGNAL_NAME ||
							stCmdBuf.iControlType == MODIFY_ALARM_ALL_INFO )
				{
					ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_EQUIPID), szEquipID);
					ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_SIGNALTYPE), szSignalType);
					
					if(szAlarmSignal != NULL)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_ALARM_SIGNAL_INFO), szAlarmSignal);
					}

					if(szSampleSignal != NULL)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_SAMPLE_SIGNAL_INFO), szSampleSignal);
					}

					if(szControlSignal != NULL)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_CONTROL_SIGNAL_INFO), szControlSignal);
					}

					if(szSettingSignal != NULL)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_SETTING_SIGNAL_INFO), szSettingSignal);
					}


					if(stCmdBuf.iControlType == MODIFY_SIGNAL_NAME)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), control_result);
						
					}
					else if(stCmdBuf.iControlType == GET_EQUIP_SIGNAL_INFO)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "2");
					}

					else
					{
						//if(atoi(control_result)%2 == 0)
						//{
							ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), control_result);
						//}
						//else
						//{
						//	ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "0");
						//}
					}
				}
				else if(stCmdBuf.iControlType == MODIFY_EQUIP_NAME ||
							stCmdBuf.iControlType == GET_DEVICE_LIST)
				{
					if(szDeviceList != NULL)
					{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_DEVICE_LIST), szDeviceList);
					//	ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "1");
					}
					//else
					//{
						//ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), "0");
					//}
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), control_result);
				}
				else if(stCmdBuf.iControlType == CLEAR_HISTORY_DATA)
				{
				//	TRACE("************%s*************\n",control_result);
					ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), control_result);
				}
				else if(stCmdBuf.iControlType == MODIFY_SIGNAL_VALUE)
				{
					//if()
					//{
						ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), control_result);
					//}
				}
				else if(stCmdBuf.iControlType == GET_USER_DEF_PAGE || stCmdBuf.iControlType == SET_USER_DEF_PAGE)
				{
					ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), control_result);
					ReplaceString(&pHtml, MAKE_VAR_FIELD(PAGE_SIG_INFO), pSigInfo);
					ReplaceString(&pHtml, MAKE_VAR_FIELD(PAGE_SIG_VAL), pSigVal);

				}
				//PostPage(szHead);
				PostPage(pHtml);
				DELETE(szAlarmSignal);
				szAlarmSignal = NULL;
//#ifdef _DEBUG
//				FILE *fp;
//				if((fp = fopen("/scup/www/html/file3.htm","wb")) != NULL)
//				{
//					fwrite(pHtml,strlen(pHtml), 1, fp);
//					fclose(fp);
//				}
//#endif
			}

			else
			{
				
				/*return unknown error*/
				sprintf(control_result,"%s","0");
				ReplaceString(&pHtml, MAKE_VAR_FIELD(CGI_CTL_RETURN_RESULT), control_result);
				//PostPage(szHead);
				PostPage(pHtml);
			}
	
		}
	}
	

	if(pHtml != NULL)
	{
		DELETE(pHtml);
		pHtml = NULL;
	}

	if( pSigInfo != NULL)
	{
		DELETE( pSigInfo);
		 pSigInfo = NULL;
	}

	if(pSigVal != NULL)
	{
		DELETE(pSigVal);
		pSigVal = NULL;

	}

	if(szUserName != NULL)
	{
		DELETE(szUserName);
		szUserName = NULL;
	}

	if(szAlarmSignal != NULL)
	{
		DELETE(szAlarmSignal);
		szAlarmSignal = NULL;

	}

	if(szSampleSignal != NULL)
	{
		DELETE(szSampleSignal);
		szSampleSignal = NULL;
	}

	if(szControlSignal != NULL)
	{
		DELETE(szControlSignal);
		szControlSignal = NULL;
	}

	if(szSettingSignal != NULL)
	{
		DELETE(szSettingSignal);
		szSettingSignal = NULL;
	}

	if(szDeviceList != NULL)
	{
		DELETE(szDeviceList);
		szDeviceList = NULL;
    }

	if(szACUInfo != NULL)
	{
		DELETE(szACUInfo);
		szACUInfo = NULL;
    }
	return TRUE;
}



/*==========================================================================*
 * FUNCTION :  Web_CTL_GetCommandParam
 * PURPOSE  :  get control param from CGI
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  OUT STRU_CONTROL_COMMAND *pBuf:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/

static int Web_CTL_GetCommandParam(OUT STRU_CONTROL_COMMAND *stControlComand)
{
	char		**postvars = NULL; /* POST request data repository */
    char		**getvars = NULL; /* GET request data repository */
    int			form_method; /* POST = 1, GET = 0 */  
    char		*val;
	
    /*FILE *pf=NULL;
    fopen("/var/wj.txt","a+");*/

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
        getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			//AppLogOut(CGI_APP_LOG_CONTROL_NAME,LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		//AppLogOut(CGI_APP_LOG_CONTROL_NAME,LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}

    stControlComand->iControl = CONTROL_COMMAND;
    val = getValue( getvars, postvars, EQUIP_ID );

    //fwrite(val,sizeof(val),1,pf);
    if( val != NULL )
    {
        //pBuf->szEquipName =  val ; // EQUIP ID
		stControlComand->iEquipID = atoi(val);


		if((val = getValue( getvars, postvars, SIGNAL_TYPE )) != NULL)
		{
			stControlComand->iSignalType = atoi(val);
		}
		else
		{
			stControlComand->iSignalType = -2;
		}

		if((val = getValue( getvars, postvars, SIGNAL_ID )) != NULL)
		{
			stControlComand->iSignalID = atoi(val);
		}
		else
		{
			stControlComand->iSignalID = -2;
		}
		
		if((val = getValue( getvars, postvars, CONTROL_TYPE )) !=NULL) 
		{
			/*	0 :Modify signal name 
				1 :modify signal value 
				2 :Modify alarm level 
				3 :Modify equip name
				4 :Modify ACU Info
				5 :Get equip signal
				6 :Modify Alarm
				7: Get ACU Info
				8: Get Device list
				9: Clear data
				10:restore default config
				11:restart ACU
			*/
             //fwrite(val,sizeof(val),1,pf);
			stControlComand->iControlType = atoi(val);
		}

	    if((val = getValue( getvars, postvars, SESSION_ID )) != NULL ) // sessionId
        {
			strncpyz(stControlComand->szSessionID,val,sizeof(stControlComand->szSessionID));  
       
        }
		
		if((val = getValue(getvars,postvars,LANGUAGE_TYPE)) != NULL)
		{
			stControlComand->iLanguage = atoi(val);
		}
		/*get control value based on control type*/
		if((val = getValue(getvars,postvars,CONTROL_VALUE)) != NULL)
		{
			strncpyz(stControlComand->value,val,(int)sizeof(stControlComand->value));
		}

		

		if((val = getValue(getvars,postvars,SIGNAL_MODIFY_NAME_TYPE)) != NULL)
		{
			stControlComand->iModifyNameType = atoi(val);
		}
		else
		{
			stControlComand->iModifyNameType = -1;
		}

		if((val = getValue(getvars,postvars,VALUE_TYPE)) != NULL)
		{
			stControlComand->iValueType = atoi(val);
		}
		else
		{
			stControlComand->iValueType = -2;
		}


		if((val = getValue(getvars,postvars,ALARM_NEW_LEVEL)) != NULL)
		{
			stControlComand->iAlarmNewLevel = atoi(val);
		}
		else
		{
			stControlComand->iAlarmNewLevel = -2;
		}

		       
    }
	

	cleanUp(getvars, postvars);
    //fclose(pf);
	return TRUE;  
}



 /*==========================================================================*
 * FUNCTION :  Web_CTL_SendCommandControl
 * PURPOSE  :  send command to ACU through FIFO
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN CONTROL_COMMAND *pBuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/
static int Web_CTL_SendCommandControl(IN STRU_CONTROL_COMMAND *pCommandBuf, 
									  IN char *szUserName)
{
	int		fd,fd2;    //fifo handle
	int		iLen = 0;
	char	buf[1024],buf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*pBuf;
	int		iBufCount = 0;
	char    *pTemp;
	
 
	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return FALSE;
	}

	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return FALSE;
	}

	/*[0]PID [1] fifo type [2]equipID [3]signal type [4] signalID [5]control type  [6]iLanguage*/
	
	iLen += sprintf(buf,"%10ld%2d%8d%4ld%8ld%2d%2d%2d%5d%2d%-100s%32s",(long)getpid(), 
														pCommandBuf->iControl,
														pCommandBuf->iEquipID,
														pCommandBuf->iSignalType,
														pCommandBuf->iSignalID,
														pCommandBuf->iControlType,
														pCommandBuf->iLanguage,
														pCommandBuf->iValueType,
														pCommandBuf->iAlarmNewLevel,
														pCommandBuf->iModifyNameType,
														pCommandBuf->value,
														szUserName);


//#ifdef _DEBUG
//	FILE *fp;
//	if((fp = fopen("/scup/www/html/control.htm","wb")) != NULL)
//	{
//		fwrite(buf,strlen(buf), 1, fp);
//		fwrite("3333\n", 5,1,fp);
//		fwrite(pCommandBuf->value,strlen(pCommandBuf->value), 1, fp);
//		fwrite("\n2222\n", 5,1,fp);
//		fclose(fp);
//	}
//#endif
	//TRACE("buf : %s\n", buf);
	
	if((write(fd,buf,strlen(buf) + 1 )) < 0)
	{
		close(fd);
		
        return FALSE;
	}

	/*FILE *fp;
	if((fp = fopen("/var/error.txt","wb")) != NULL)
	{
		fwrite(buf,strlen(buf), 1, fp);
		fwrite("3333\n", 5,1,fp);

		fclose(fp);
	}*/

	/*if(pCommandBuf->iControlType == GET_USER_DEF_PAGE || pCommandBuf->iControlType == SET_USER_DEF_PAGE)
	{
		if((write(fd,buf,strlen(buf) + 1 )) < 0)
		{
			close(fd);
			return FALSE;
		}
		FILE *fp;
		if((fp = fopen("/var/error.txt","wb")) != NULL)
		{
			fwrite(buf,strlen(buf), 1, fp);
			fwrite("3333\n", 5,1,fp);
			fwrite(pCommandBuf->value,strlen(pCommandBuf->value), 1, fp);
			fwrite("\n2222\n", 5,1,fp);
			fclose(fp);
		}
	}*/


	if((fd2 = open(fifoname,O_RDONLY)) < 0)
	{
        close(fd);
        close(fd2);
		return FALSE;
	}

	
	pBuf = NEW(char, PIPE_BUF * 2);
	if(pBuf == NULL)
	{
        close(fd);
        close(fd2);
        return FALSE;
	}

	while((iLen = read(fd2,buf2,PIPE_BUF - 1)) > 0)
	{
		//printf("get info :%s \n",buf2); //print recevied data
 		if(iBufCount >= 2)
		{
			pBuf = RENEW(char, pBuf, (iBufCount + 2) * PIPE_BUF);
		}
		strcat(pBuf, buf2);
		iBufCount++;
	}
	
 	//load data
	pTemp = pBuf;

	pBuf++;

	/*FILE *pf = NULL;
	pf=fopen("/var/wj2.txt","a+");
	fwrite(pBuf,strlen(pBuf),1,pf);*/



	if(pCommandBuf->iControlType == MODIFY_SIGNAL_ALARM_LEVEL || 
			pCommandBuf->iControlType == GET_EQUIP_SIGNAL_INFO ||
			pCommandBuf->iControlType == MODIFY_SIGNAL_NAME ||
			pCommandBuf->iControlType == MODIFY_ALARM_ALL_INFO)
	{
		Web_CTL_LoadDataPtr(pBuf);
	}
	else if( pCommandBuf->iControlType == GET_ACU_INFO || 
					pCommandBuf->iControlType == MODIFY_ACU_INFO)
	{
		Web_CTL_LoadACUPtr(pBuf);
	}
	else if(pCommandBuf->iControlType == MODIFY_SIGNAL_VALUE)
	{
		strncpyz(control_result, pBuf, sizeof(control_result));
	}
	else if(pCommandBuf->iControlType == GET_USER_DEF_PAGE || pCommandBuf->iControlType == SET_USER_DEF_PAGE)
	{
		 Web_CTL_LoadUserDefPage(pBuf);

	}
	else
	{
		Web_CTL_LoadDevicePtr(pBuf);
	}


	close(fd2);
	close(fd);
	unlink(fifoname);
	DELETE(pTemp);

	return TRUE;
	
}



 
/*==========================================================================*
 * FUNCTION :  Web_CTL_LoadDataPtr
 * PURPOSE  :  Load data from FIFO
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN char *pBuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/

static void Web_CTL_LoadDataPtr(IN char *pBuf)
{
	ASSERT(pBuf);
	char		*ptr = pBuf;
	char		*pSearchValue = NULL;
	int			iPosition = 0;

    FILE *pf=NULL;
   /* pf=fopen("/var/www.txt","a+");*/

	/*Get control result*/
	strncpyz(control_result,ptr, 10);
	ptr = ptr + 9;
	
	/*Get equip ID*/
	strncpyz(szEquipID, ptr, 6);
	ptr = ptr + 5;
    
    //fwrite(szEquipID,6,1,pf);

	
	/*Get signal type*/
	strncpyz(szSignalType, ptr, 6);
	ptr = ptr + 5;

	/*Get alarm signal*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition >= 0)
		{
			szAlarmSignal = NEW(char, iPosition + 1);
			strncpyz(szAlarmSignal, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
	}
	ptr = ptr + 1;

    //fwrite(szAlarmSignal,sizeof(szAlarmSignal),1,pf);

	/*Get sample signal*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition >= 0)
		{
			szSampleSignal = NEW(char, iPosition + 1);
			strncpyz(szSampleSignal, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
	}
	ptr = ptr + 1;

    //fwrite(szSampleSignal,sizeof(szSampleSignal),1,pf);

	/*Get control signal*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition >= 0)
		{
			szControlSignal = NEW(char, iPosition + 1);
			strncpyz(szControlSignal, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
	}
	ptr = ptr + 1;

    // fwrite(szControlSignal,sizeof(szControlSignal),1,pf);

	/*Get setting signal*/
	pSearchValue = strchr(ptr, 59);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition >= 0)
		{
			szSettingSignal = NEW(char, iPosition + 1);
			strncpyz(szSettingSignal, ptr, iPosition + 1);
		}
	}

    // fwrite(szSettingSignal,sizeof(szSettingSignal),1,pf);

     //fclose(pf);

}
 
 static void Web_CTL_LoadACUPtr(IN char *pBuf)
 {
	ASSERT(pBuf);
	char		*ptr = pBuf;
	//char		*pSearchValue = NULL;
	int			iPosition = 0;

	/*Get control result*/
	strncpyz(control_result,ptr, 10);
	ptr = ptr + 10;

	iPosition = strlen(ptr);
	szACUInfo = NEW(char, iPosition + 1);
	strncpyz(szACUInfo, ptr, iPosition + 1);
	
	
 }

 static void Web_CTL_LoadDevicePtr(IN char *pBuf)
 {
	 ASSERT(pBuf);
	char		*ptr = pBuf;
	//char		*pSearchValue = NULL;
	int			iPosition = 0;

	/*Get control result*/
	strncpyz(control_result,ptr, 10);
	ptr = ptr + 9;

	iPosition = strlen(ptr);
	if(iPosition > 0)
	{
		szDeviceList = NEW(char, iPosition + 1);
		strncpyz(szDeviceList, ptr, iPosition + 1);
	}

 }

 /*==========================================================================*
 * FUNCTION :  Web_CTL_LoadUserDefPage
 * PURPOSE  :  Load data from FIFO
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN char *pBuf
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-21 11:20
 *==========================================================================*/

 static void Web_CTL_LoadUserDefPage(IN char *pBuf)
 {
	 ASSERT(pBuf);
	 char		*ptr = pBuf;
	 char		*pSearchValue = NULL;
	 int			iPosition = 0;

	 FILE *pf=NULL;
	 /* pf=fopen("/var/www.txt","a+");*/

	 /*Get control result*/
	 strncpyz(control_result,ptr, 10);
	 ptr = ptr + 9;


	 /*Get  signal info*/
	 pSearchValue = strchr(ptr, 59);
	 if(pSearchValue != NULL)
	 {
		 iPosition = pSearchValue - ptr;
		 if(iPosition >= 0)
		 {
			 pSigVal = NEW(char, iPosition + 1);
			 strncpyz(pSigVal, ptr, iPosition + 1);
			 ptr = ptr + iPosition;
		 }
	 }

	 ptr = ptr + 2;


	/* pf=fopen("/var/wj4.txt","a+");
	 fwrite(pSigVal,strlen(pSigVal),1,pf);*/

	 //fwrite(szAlarmSignal,sizeof(szAlarmSignal),1,pf);

	 /*Get signal val*/
	 iPosition = strlen(ptr);
	 if(iPosition > 0)
	 {
		 pSigInfo = NEW(char, iPosition + 1);
		 if(pSigInfo == NULL)
		 {
			 return FALSE;
		 }
		 //memset(pSigVal, 0x0, (size_t)iPosition);
		 strncpyz(pSigInfo, ptr, iPosition + 1);

	 }


	/* pf=fopen("/var/wj5.txt","a+");
	 fwrite(pSigInfo,strlen(pSigInfo),1,pf);*/

	 return TRUE;

	 // fwrite(szSettingSignal,sizeof(szSettingSignal),1,pf);

	 //fclose(pf);

 }

 
//static void StuffHead(  char *pszHead, int iLenContent )
//{
//    sprintf( pszHead,
//        "HTTP/1.1 200 OK\r\n"   // \x0d\x0a
//        "Server: BOA/1.0\r\n"
//        "Date: %s\r\n"
//        "Content-type: text/html\r\n"
//        "Content-length: %d\r\n"
//        "Last-Modified: %s\r\n"
//        "\r\n", 
//        strModiTime,
//        iLenContent,
//        strModiTime );
//
//    //return pszHead;
//} 




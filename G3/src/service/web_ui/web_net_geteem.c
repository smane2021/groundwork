/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static void Web_GET_PostEEMPage(IN int nCommandType, IN char *ptrBuff, IN int iLen);
static void *Web_SET_SendConfigureCommand(IN int nCommandType);
static int Web_SET_GetCommandParam(void);
char		szUserInfo[32];

int main(void)
{
	int							nCommandType = 0;
	printf("Content-type:text/html\n\n");

	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");
	memset(szUserInfo, 0x0, sizeof(szUserInfo));

	/*get parameter*/
	if ((nCommandType = Web_SET_GetCommandParam()) > 0)
	{
		if(Web_SET_SendConfigureCommand(nCommandType) != NULL)
		{
		}
	}
	else
	{
	}
	
	return TRUE;

}

static void *Web_SET_SendConfigureCommand(IN int nCommandType)	
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	int		iBufCount = 0;
	int		iModifyPassword = 0;

	/*create FIFO with our PID as part of name*/

 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return -1;
	}

	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return -1;
	}
	
	/*start buffer with pid and a blank*/
	iLen = sprintf(szBuf1,"%10ld%2d%2d%-8d",(long)getpid(), WEB_SUPPORT_NETSCAPE,GET_EEM_SETTING, nCommandType);
    		
	if((write(fd, (void *)szBuf1, iLen + 1))<0)
	{
		close(fd);
		return -1;
	}


	if((fd2 = open(fifoname,O_RDONLY)) < 0)
	{
 		return -1;
	}
	
	if((iLen = read(fd2, szBuf2, PIPE_BUF - 1)) > 0)
	{
		//printf("%s",szBuf2);
		//exit(1);
		Web_GET_PostEEMPage(nCommandType, szBuf2, iLen);
	}

	close(fd2);
	close(fd);
	unlink(fifoname);

	return NULL;
}

static int Web_SET_GetCommandParam(void)
{
#define CGI_GET_SETTING_TYPE			"setting_type"
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL;
	int		nCommandType;

    form_method = getRequestMethod();
    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}
	
	char *ptr1 = NULL;
	if((val = getValue( getvars, postvars,  CGI_GET_SETTING_TYPE)) != NULL)
	{
		nCommandType = atoi(val);
	}
	if((val = getValue( getvars, postvars,  CGI_GET_AUTH_NAME)) != NULL)
	{
		sprintf(szUserInfo,"%s", val);
	}
	cleanUp(getvars, postvars);
    return nCommandType;  
}


static char szReturnValue[4][100] = {"No used", "All alarms", "Major alarms","Critical alarms"};

static void Web_GET_PostEEMPage(IN int nCommandType, IN char *ptrBuff, IN int iLen)
{
#define DI_EEM_REPORTINUSE				"DI_EEM_REPORTINUSE"
#define DI_EEM_CALLBACKINUSE			"DI_EEM_CALLBACKINUSE"
#define DI_EEM_PROTOCOL					"DI_EEM_PROTOCOL"
#define DI_EEM_MEDIA					"DI_EEM_MEDIA"
#define DI_EEM_CCID						"DI_EEM_CCID"
#define DI_EEM_SOCID					"DI_EEM_SOCID"
#define DI_EEM_CALLELAPSE				"DI_EEM_CALLELAPSE"
#define DI_EEM_REPORTATTEMPS			"DI_EEM_REPORTATTEMPS"
#define DI_EEM_SECURITYLEVEL			"DI_EEM_SECURITYLEVEL"
#define END_OF_NMS						','

/*Lease line*/
#define DI_EEMLEASE_BAUD				"DI_EEMLEASE_BAUD"
#define DI_EEMLEASE_DATABIT				"DI_EEMLEASE_DATABIT"
#define DI_EEMLEASE_STOPBIT				"DI_EEMLEASE_STOPBIT"
#define DI_EEMLEASE_PARITY				"DI_EEMLEASE_PARITY"

/*TCPIP*/
#define DI_REPORT_PRIMARYIP				"DI_REPORT_PRIMARYIP"
#define DI_REPORT_PRIMARYPORT			"DI_REPORT_PRIMARYPORT"
#define DI_REPORT_SECONDARYIP			"DI_REPORT_SECONDARYIP"
#define DI_REPORT_SECONDARYPORT			"DI_REPORT_SECONDARYPORT"
#define DI_REPORT_CALLBACKIP1			"DI_REPORT_CALLBACKIP1"
#define DI_REPORT_CALLBACKPORT1			"DI_REPORT_CALLBACKPORT1"
#define DI_REPORT_CALLBACKIP2			"DI_REPORT_CALLBACKIP2"
#define DI_REPORT_CALLBACKPORT2			"DI_REPORT_CALLBACKPORT2"
#define DI_REPORT_INCOMINGPORT			"DI_REPORT_INCOMINGPORT"

/*PSTN*/
#define DI_PSTN_PRIMARY					"DI_PSTN_PRIMARY"
#define DI_PSTN_SECONDARY				"DI_PSTN_SECONDARY"
#define DI_PSTN_CALLBACK				"DI_PSTN_CALLBACK"
#define DI_PSTN_RINGSNO					"DI_PSTN_RINGSNO"
#define DI_PSTN_BAUD					"DI_PSTN_BAUD"
#define DI_PSTN_DATA					"DI_PSTN_DATA"
#define DI_PSTN_STOP					"DI_PSTN_STOP"
#define DI_PSTN_PARITY					"DI_PSTN_PARITY"
#define DI_PSTN_PHONE					"DI_PSTN_PHONE"


	char	*pPosition, *ptr;
	int		iPosition, iNMS, iTrapLevel;
	char	*pHtml;
	char	szExchange[128];
	int		iPosition2;
	char	*pPosition2;


	ptr = ptrBuff;

 
	if(nCommandType == NET_EEM_GENEARL)
	{
		if(LoadHtmlFile("/var/netscape/eemconf_general.htm", &pHtml) < 0 )
		{
			return NULL;
		}

		//Report In use
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;

		/*Trap Levle*/
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_REPORTINUSE), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_REPORTINUSE), " ");
		}
 		ptr = ptr + 1;
		
		//Callback in use
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_CALLBACKINUSE), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_CALLBACKINUSE), " ");
		}
 		ptr = ptr + 1;

		//Protocol
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_PROTOCOL), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_PROTOCOL), " ");
		}
 		ptr = ptr + 1;


		//Media
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_MEDIA), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_MEDIA), " ");
		}
 		ptr = ptr + 1;


		//CCID
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_CCID), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_CCID), " ");
		}
 		ptr = ptr + 1;

		//SOCID
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_SOCID), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_SOCID), " ");
		}
 		ptr = ptr + 1;

		//Report attemps
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_REPORTATTEMPS), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_REPORTATTEMPS), " ");
		}
 		ptr = ptr + 1;

		//Call elapse time
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_CALLELAPSE), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_CALLELAPSE), " ");
		}
 		ptr = ptr + 1;

		//Security level
		pPosition = strchr(ptr, END_OF_NMS);
		iPosition = pPosition - ptr;
		if(iPosition > 0 )
		{
			strncpyz(szExchange, ptr, iPosition + 1);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_SECURITYLEVEL), szExchange);
			ptr = ptr + iPosition;
		}
		else
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEM_SECURITYLEVEL), " ");
		}
 		ptr = ptr + 1;


	}
	else if(nCommandType == NET_EEM_PSTN)
	{
		if(LoadHtmlFile("/var/netscape/eemconf_pstn.htm", &pHtml) < 0 )
		{
			return NULL;
		}

		if(!strchr(ptr,'n'))
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PRIMARY), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_SECONDARY), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_CALLBACK), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_BAUD), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PARITY), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_DATA), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_STOP), " ");			
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PHONE), " ");
		}
		else
		{

			//Primary Phone
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PRIMARY), szExchange);
				ptr = ptr + iPosition ;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PRIMARY), " ");
			}
 			ptr = ptr + 1;

			//printf("\n---step2-----%s\n",ptr);
			//Secondary Phone
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_SECONDARY), szExchange);
				ptr = ptr + iPosition ;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_SECONDARY), " ");
			}
 			ptr = ptr + 1;

			//printf("\n---step3-----%s\n",ptr);
			//Callback Phone
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_CALLBACK), szExchange);
				ptr = ptr + iPosition;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_CALLBACK), " ");
			}
 			ptr = ptr + 1;

	/*		

			//Rings
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_RINGSNO), szExchange);
				ptr = ptr + iPosition;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_RINGSNO), " ");
			}
 			ptr = ptr + 1;
	*/
			//printf("\n---step4-----%s\n",ptr);
			
			//Baud
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_BAUD), szExchange);
				ptr = ptr + iPosition;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_BAUD), " ");
			}
 			ptr = ptr + 1;

			//printf("\n---step5----%s\n",ptr);
			
			//Parity bit
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PARITY), szExchange);
				ptr = ptr + iPosition;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PARITY), " ");
			}
 			ptr = ptr + 1;

			//printf("\n---step6----\n");
			//printf("\n---step7----%s\n",ptr);
			
			//exit(1);
			

			//Data bit
			pPosition = strchr(ptr, END_OF_NMS);
			
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_DATA), szExchange);
				ptr = ptr + iPosition;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_DATA), " ");
			}
 			ptr = ptr + 1;


			//printf("\n---step7----\n");


			//Stop bit
			pPosition = strchr(ptr, ':');
			if(pPosition)
			{
				iPosition = pPosition - ptr;


				
				if(iPosition > 0 )
				{
					strncpyz(szExchange, ptr, iPosition + 1);
					ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_STOP), szExchange);
					ptr = ptr + iPosition;
				}
				ptr = ptr + 1;
				//Phone
				pPosition = strchr(ptr, END_OF_NMS);
				iPosition = pPosition - ptr;
				if(iPosition > 0 )
				{
					strncpyz(szExchange, ptr, iPosition + 1);
					ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PHONE), szExchange);
					//ptr = ptr + iPosition;
				}
				else
				{
					ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_STOP), " ");
				}
			}
			
			else 
			{
				//Data bit
				pPosition = strchr(ptr, END_OF_NMS);
				
				iPosition = pPosition - ptr;
				if(iPosition > 0 )
				{
					strncpyz(szExchange, ptr, iPosition + 1);
					ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_STOP), szExchange);
					ptr = ptr + iPosition;
				}
				else
				{
					ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_STOP), " ");
				}
				
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_PSTN_PHONE), " ");
			}
		}

	}
	else if(nCommandType == NET_EEM_TCPIP)
	{
		if(LoadHtmlFile("/var/netscape/eemconf_tcpip.htm", &pHtml) < 0 )
		{
			return NULL;
		}

		if(!strchr(ptr, ':'))
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_PRIMARYIP), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_PRIMARYPORT), " ");

			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYIP), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYPORT), " ");

			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP1), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT1), " ");

			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP2), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT2), " ");

			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_INCOMINGPORT), " ");

			
		}
		else
		{

			//Primary IP
			pPosition = strchr(ptr, ':');
			iPosition = pPosition - ptr;
			pPosition2 = strchr(ptr, END_OF_NMS);
			if(iPosition > 0)
			{
				iPosition2 = pPosition2 - pPosition;
				
			}
			else
			{
				iPosition2 = pPosition2 - ptr;

			}

			
			if(iPosition > 0 && iPosition2 > 0)
			{
				
				memset(szExchange,0x0, sizeof(szExchange));
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_PRIMARYIP), szExchange);
				//ptr = ptr + iPosition + 1;

				memset(szExchange,0x0, sizeof(szExchange));

				strncpyz(szExchange, ptr + iPosition + 1, iPosition2);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_PRIMARYPORT), szExchange);
				ptr = ptr + iPosition2 + iPosition + 1;
			}
			else
			{
				
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_PRIMARYIP), " ");

				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_PRIMARYPORT), " ");

				ptr = ptr + iPosition2 + 1;
			}
	 		

			//SECONDARY IP
			pPosition = strchr(ptr, ':');
			iPosition = pPosition - ptr;
			pPosition2 = strchr(ptr, END_OF_NMS);

			if(iPosition > 0)
			{
				iPosition2 = pPosition2 - pPosition;

			}
			else
			{
				iPosition2 = pPosition2 - ptr;

			}
			
			if(iPosition > 0 && iPosition2 > 0)
			{
				//iPosition2 = pPosition2 - pPosition;
				memset(szExchange,0x0, sizeof(szExchange));
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYIP), szExchange);
				//ptr = ptr + iPosition + 1;

				memset(szExchange,0x0, sizeof(szExchange));
				strncpyz(szExchange, ptr + iPosition + 1, iPosition2);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYPORT), szExchange);

				ptr = ptr + iPosition2 + iPosition + 1;
				
			}
			else
			{
				//iPosition2 = pPosition2 - ptr;
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYIP), " ");

				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYPORT), " ");
				ptr = ptr + iPosition2 + 1;
			}
			//ptr = ptr + iPosition2 + iPosition + 1;

			//CALLBACK1 IP
			pPosition = strchr(ptr, ':');
			iPosition = pPosition - ptr;
			pPosition2 = strchr(ptr, END_OF_NMS);
			if(iPosition > 0)
			{
				iPosition2 = pPosition2 - pPosition;

			}
			else
			{
				iPosition2 = pPosition2 - ptr;

			}
			
			if(iPosition > 0 && iPosition2 > 0)
			{
				//iPosition2 = pPosition2 - pPosition;
				memset(szExchange,0x0, sizeof(szExchange));
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP1), szExchange);
				//ptr = ptr + iPosition + 1;
				memset(szExchange,0x0, sizeof(szExchange));

				strncpyz(szExchange, ptr + iPosition + 1, iPosition2);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT1), szExchange);
				ptr = ptr + iPosition2 + iPosition + 1;
			}
			else
			{
				//iPosition2 = pPosition2 - ptr;
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP1), " ");

				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT1), " ");
				ptr = ptr + iPosition2 + 1;
			}
			//ptr = ptr + iPosition2 + iPosition + 1;

			//CALLBACK2 IP
			pPosition = strchr(ptr, ':');
			iPosition = pPosition - ptr;
			pPosition2 = strchr(ptr, END_OF_NMS);
			if(iPosition > 0)
			{
				iPosition2 = pPosition2 - pPosition;

			}
			else
			{
				iPosition2 = pPosition2 - ptr;

			}
			
			if(iPosition > 0 && iPosition2 > 0)
			{
				//iPosition2 = pPosition2 - pPosition;
				memset(szExchange,0x0, sizeof(szExchange));
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP2), szExchange);
				//ptr = ptr + iPosition + 1;

				memset(szExchange,0x0, sizeof(szExchange));
				strncpyz(szExchange, ptr + iPosition + 1, iPosition2);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT2), szExchange);
				ptr = ptr + iPosition2 + iPosition + 1;
			}
			else
			{
				//iPosition2 = pPosition2 - ptr;
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP2), " ");

				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT2), " ");
				ptr = ptr + iPosition2 + 1;
			}
			

			////Primary Port
			//pPosition = strchr(ptr, END_OF_NMS);
			//iPosition = pPosition - ptr;
			//if(iPosition > 0 )
			//{
			//	strncpyz(szExchange, ptr, iPosition + 1);
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_PRIMARYPORT), szExchange);
			//	ptr = ptr + iPosition + 1;
			//}
			//else
			//{
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_PRIMARYPORT), " ");
			//}
 		//	//ptr = ptr + 1;
		

			////Sedondary IP
			//pPosition = strchr(ptr, ':');
			//iPosition = pPosition - ptr;
			//if(iPosition > 0 )
			//{
			//	strncpyz(szExchange, ptr, iPosition + 1);
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYIP), szExchange);
			//	ptr = ptr + iPosition + 1;
			//}
			//else
			//{
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYIP), " ");
			//}
 		//	//ptr = ptr + 1;

			////Sedondary Port
			//pPosition = strchr(ptr, END_OF_NMS);
			//iPosition = pPosition - ptr;
			//if(iPosition > 0 )
			//{
			//	strncpyz(szExchange, ptr, iPosition + 1);
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYPORT), szExchange);
			//	ptr = ptr + iPosition + 1;
			//}
			//else
			//{
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_SECONDARYPORT), " ");
			//}
 		//	//ptr = ptr + 1;


			////Callback IP1
			//pPosition = strchr(ptr, ':');
			//iPosition = pPosition - ptr;
			//if(iPosition > 0 )
			//{
			//	strncpyz(szExchange, ptr, iPosition + 1);
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP1), szExchange);
			//	ptr = ptr + iPosition + 1;
			//}
			//else
			//{
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP1), " ");
			//}
 		//	//ptr = ptr + 1;

			////Callback IP Port1
			//pPosition = strchr(ptr, END_OF_NMS);
			//iPosition = pPosition - ptr;
			//if(iPosition > 0 )
			//{
			//	strncpyz(szExchange, ptr, iPosition + 1);
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT1), szExchange);
			//	ptr = ptr + iPosition + 1;
			//}
			//else
			//{
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT1), " ");
			//}
 		//	//ptr = ptr + 1;

			////Callback IP2
			//pPosition = strchr(ptr, ':');
			//iPosition = pPosition - ptr;
			//if(iPosition > 0 )
			//{
			//	strncpyz(szExchange, ptr, iPosition + 1);
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP2), szExchange);
			//	ptr = ptr + iPosition + 1;
			//}
			//else
			//{
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKIP2), " ");
			//}
 		//	//ptr = ptr + 1;

			////Callback IP Port2
			//pPosition = strchr(ptr, END_OF_NMS);
			//iPosition = pPosition - ptr;
			//if(iPosition > 0 )
			//{
			//	strncpyz(szExchange, ptr, iPosition + 1);
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT2), szExchange);
			//	ptr = ptr + iPosition + 1;
			//}
			//else
			//{
			//	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_CALLBACKPORT2), " ");
			//}
 			//ptr = ptr + 1;


			//Incoming Port
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				memset(szExchange,0x0, sizeof(szExchange));
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_INCOMINGPORT), szExchange);
				//ptr = ptr + iPosition;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_REPORT_INCOMINGPORT), " ");
			}
 			//ptr = ptr + 1;

		}

	}
	else  
	{
		if(LoadHtmlFile("/var/netscape/eemconf_leasedline.htm", &pHtml) < 0 )
		{
			return NULL;
		}

		//printf("\n---step1-%s- %d--\n", ptr, iLen);

		
		if(iLen <= 8)
		{
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_BAUD), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_PARITY), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_DATABIT), " ");
			ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_STOPBIT), " ");

			
		}
		else
		{
			//Baud rate
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_BAUD), szExchange);
				ptr = ptr + iPosition + 1;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_BAUD), " ");
			}
 			//ptr = ptr + 1;
			//printf("\n---step2----\n");
			//Parity
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_PARITY), szExchange);
				ptr = ptr + iPosition + 1;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_PARITY), " ");
			}
 			//ptr = ptr + 1;
	//printf("\n---step3----\n");
			//Data bit
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_DATABIT), szExchange);
				ptr = ptr + iPosition + 1;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_DATABIT), " ");
			}
 			//ptr = ptr + 1;

	//printf("\n---step4----\n");
			//Stop bit
			pPosition = strchr(ptr, END_OF_NMS);
			iPosition = pPosition - ptr;
			if(iPosition > 0 )
			{
				strncpyz(szExchange, ptr, iPosition + 1);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_STOPBIT), szExchange);
				ptr = ptr + iPosition + 1;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_EEMLEASE_STOPBIT), " ");
			}
 			//ptr = ptr + 1;

	//printf("\n---step5----\n");

		}




	}
	if(strncmp(CGI_READ_NAME, szUserInfo, sizeof(CGI_READ_NAME)) == 0)
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_DI_DISABLED), "disabled" );
	}
	else
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_DI_DISABLED), " " );
	}
	PostPage(pHtml);
	DELETE(pHtml);
//�����ַ���
}




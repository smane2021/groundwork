/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_passwd_netscape.c
 *  CREATOR  : Wan Kun              DATE: 2007-04-24 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"
#include "cgi_pub.h"
#include "cgivars.h"

#define USER_READ		0
#define USER_WRITE		1
#define USER_ADMIN		2

#define CGI_USER_TYPE		"_userType"
#define CGI_USER_PASSWD		"_userPasswd"

typedef struct tagPasswd
{
	int			iPasswdType;				/*	0: passwd for read 
												1: passwd for write
												2: passwd for admin*/
	char		szPasswd[16];
}STRU_PASSWD;

static int Web_SetPasswd(IN STRU_PASSWD *pCommandParam);
static int Web_GetPasswd(OUT STRU_PASSWD *stPasswdParam);
static char html_path[128] = {"error.htm"};


int main(void)
{
	STRU_PASSWD				stPasswdBuf;
	//int						iLanguage;				
	char					*pHtml = NULL;
	//char					*pQueryReturnData = NULL;
	//int						i = 0;
	char					*szPath = NULL;

	printf("Content-type:text/html\n\n");
	

	
	szPath = NEW(char, 50);
	sprintf(szPath,"/app/www/netscape/pages/%s",html_path);
	//printf("-------szpath is -------------%s",szPath);
	
	

	if(LoadHtmlFile(szPath, &pHtml ) <= 0 )
    {
		AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Error : Fail to load file [%s]", html_path);
		DELETE(szPath);
		szPath = NULL;
        return FALSE;
    }
	
	
	DELETE(szPath);
	szPath = NULL;

	/*get passwd from html*/
	if (Web_GetPasswd(&stPasswdBuf) == FALSE)
	{
		ReplaceString(&pHtml, MAKE_VAR_FIELD("ID_RETURN"), "0");
		//return FALSE;
	}
	else
	{
		/*change passwd of system*/
		if(Web_SetPasswd(&stPasswdBuf) == FALSE)
		{
			AppLogOut(CGI_APP_LOG_QUERY_NAME,APP_LOG_WARNING,"Fail to change passwd ");
			ReplaceString(&pHtml, MAKE_VAR_FIELD("ID_RETURN"), "0");
			//return FALSE;
		}
		else
		{

			if(stPasswdBuf.iPasswdType == USER_READ)
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("ID_RETURN"), "1");
			}
			else if(stPasswdBuf.iPasswdType == USER_WRITE)
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("ID_RETURN"), "1");	
			}
			else if(stPasswdBuf.iPasswdType == USER_ADMIN)
			{
				ReplaceString(&pHtml, MAKE_VAR_FIELD("ID_RETURN"), "1");	
			}
			else
			{
				
				//return FALSE;
			}
		}//end if(Web_SetPasswd(&stPasswdBuf) == FALSE)
	}//end if (Web_GetPasswd(&stPasswdBuf) == FALSE)
	/*modify page time*/
	time_t now = time(NULL);
    srand( (unsigned int)now );
	//MakeModiTime(strModiTime, (int)strlen(strModiTime));
	
	/*modify the head of the page*/
    //StuffHead(szHead, strlen(pHtml));
	/*get control result*/
	//ReplaceString(&pHtml, MAKE_VAR_FIELD(RETURN_QUERY_RESULT), "1");
	//PostPage(szHead);
	PostPage(pHtml);
//#ifdef _DEBUG
//	FILE *fp = fopen("/acu/www/html/sh.htm","w");
//	fwrite(pHtml,strlen(pHtml), 1, fp);
//	fclose(fp);
//#endif

	DELETE(pHtml);
	pHtml = NULL;
	
		
	return TRUE;
}

/*==========================================================================*
 * FUNCTION :  Web_SetPasswd
 * PURPOSE  :  change the passwd of system
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : wankun              DATE: 2007-4-25 11:20
 *==========================================================================*/
static int Web_SetPasswd(IN STRU_PASSWD *pCommandParam)
{

	char command[64];
	//printf("we are in process of set passwd \n");
	
	//init the command string to 0
	memset(command,0, sizeof(command));
	
	if(pCommandParam->iPasswdType == USER_READ)
	{
		sprintf(command,"/var/htpasswd -b /etc/apache.passwd read %s ",pCommandParam->szPasswd);
		//printf("read -----exec command is %s \n", command);
		system(command);
	}
	else if(pCommandParam->iPasswdType == USER_WRITE)
	{
		sprintf(command,"/var/htpasswd -b /etc/apache.passwd write %s ", pCommandParam->szPasswd);
		//printf("write -----exec command is:  %s  \n", command);
		system(command);
	}
	else if(pCommandParam->iPasswdType == USER_ADMIN)
	{
		sprintf(command, "/var/htpasswd -b /etc/apache.passwd admin %s ", pCommandParam->szPasswd);
		//printf("admin -----exec command is :  %s\n", command);
		system(command);
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}
 
 
/*==========================================================================*
 * FUNCTION :  Web_GetPasswd
 * PURPOSE  :  get passwd from CGI
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : wankun              DATE: 2007-4-25 11:20
 *==========================================================================*/
static int Web_GetPasswd(OUT STRU_PASSWD *stPasswdParam)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val;

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
        postvars = getPOSTvars();
		//printf("now in post proc:\n");
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
		//printf("now in get proc:\n");
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}

        
	val = getValue( getvars, postvars, CGI_USER_TYPE );
	//printf("CGI_USER_TYPE : %s\n",val);
    if( val != NULL )
    {
        stPasswdParam->iPasswdType = atoi(val) ; // 
		//printf("iQueryType : %s\n",val);
	}

	if((val = getValue( getvars, postvars, CGI_USER_PASSWD )) != NULL)
	{
		strncpyz(stPasswdParam->szPasswd,val,sizeof(stPasswdParam->szPasswd));
		//printf("szPasswd : %s\n",val);
	}
	
	
   cleanUp(getvars, postvars);

   return TRUE;  
}



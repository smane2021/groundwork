/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : web_net_getip.c
 *  CREATOR  : Yang Guoxin              DATE: 2007-3-19 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static void Web_SET_PostIPPage(IN char *ptrBuff);
static void *Web_SET_SendConfigureCommand(IN int nCommandType);
static int Web_SET_GetCommandParam(void);
char		szUserInfo[32];

int main(void)
{
	int							nCommandType = 0;

	printf("Content-type:text/html\n\n");

	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");
	memset(szUserInfo, 0x0, sizeof(szUserInfo));

	/*get parameter*/
	if ((nCommandType = Web_SET_GetCommandParam()) > 0)
	{
	
		if(Web_SET_SendConfigureCommand(nCommandType) != NULL)
		{
			//Web_SET_PostNMSPage(szReturn);
		}
	}
	else
	{
	}
	
	return TRUE;

}

static void *Web_SET_SendConfigureCommand(IN int nCommandType)	
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	int		iBufCount = 0;
	int		iModifyPassword = 0;

	/*create FIFO with our PID as part of name*/

 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return -1;
	}

	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return -1;
	}
	
	/*start buffer with pid and a blank*/
	iLen = sprintf(szBuf1,"%10ld%2d%2d%-8d",(long)getpid(), WEB_SUPPORT_NETSCAPE,GET_IP_SETTING, nCommandType);
	
 
	if((write(fd, (void *)szBuf1, iLen + 1))<0)
	{
		close(fd);
		return -1;
	}


	if((fd2 = open(fifoname,O_RDONLY)) < 0)
	{
 		return -1;
	}
	
	if((iLen = read(fd2, szBuf2, PIPE_BUF - 1)) > 0)
	{
		Web_SET_PostIPPage(szBuf2);
 	}

	close(fd2);
	close(fd);
	unlink(fifoname);

	return NULL;
}

static int Web_SET_GetCommandParam(void)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL;
	int		nCommandType;

    form_method = getRequestMethod();
    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}
	
	char *ptr1 = NULL;
	if((val = getValue( getvars, postvars,  CGI_GET_SETTING_TYPE)) != NULL)
	{
		nCommandType = atoi(val);
	}
	if((val = getValue( getvars, postvars,  CGI_GET_AUTH_NAME)) != NULL)
	{
		sprintf(szUserInfo,"%s", val);
	}
	cleanUp(getvars, postvars);
    return nCommandType;  
}


static char szReturnValue[4][100] = {"No used", "All alarms", "Major alarms","Critical alarms"};

static void Web_SET_PostIPPage(IN char *ptrBuff)
{
#define DI_IP_ADDRESS				"DI_IP_ADDRESS"
#define DI_IP_MASK					"DI_IP_MASK"
#define DI_IP_GATEWAY				"DI_IP_GATEWAY"
#define END_OF_NMS					','
//�����ַ���

	char	*pPosition, *ptr;
	int		iPosition, iNMS, iTrapLevel;
	char	*pHtml;
	char	szExchange[128];

	ptr = ptrBuff;
	pPosition = strchr(ptr, END_OF_NMS);

 
	if(LoadHtmlFile("/var/netscape/ipconf.htm", &pHtml) < 0 )
	{
		return NULL;
	}


	pPosition = strchr(ptr, END_OF_NMS);
	iPosition = pPosition - ptr;
	//Address 1
	if(iPosition > 1)
	{
		strncpyz(szExchange, ptr, iPosition + 1);
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_IP_ADDRESS), szExchange );
		ptr = ptr + iPosition;
	}
	else
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_IP_ADDRESS), "0.0.0.0" );
	}
	ptr = ptr + 1;

	//Address 2
	pPosition = strchr(ptr, END_OF_NMS);
	iPosition = pPosition - ptr;

	if(iPosition > 1)
	{
		strncpyz(szExchange, ptr, iPosition + 1);
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_IP_MASK), szExchange );
		ptr = ptr + iPosition;
	}
	else
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_IP_MASK), "0.0.0.0" );
	}
	ptr = ptr + 1;

	//Address 3
 
	pPosition = strchr(ptr, END_OF_NMS);
	iPosition = pPosition - ptr;

	if(iPosition > 1)
	{
		strncpyz(szExchange, ptr, iPosition + 1);
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_IP_GATEWAY), szExchange );
		ptr = ptr + iPosition;
	}
	else
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_IP_GATEWAY), "0.0.0.0" );
	}

//	ptr = pPosition + 1;
	if(strncmp(CGI_READ_NAME, szUserInfo, sizeof(CGI_READ_NAME)) == 0)
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_DI_DISABLED), "disabled" );
	}
	else
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_DI_DISABLED), " " );
	}

	
	PostPage(pHtml);
	DELETE(pHtml);

}




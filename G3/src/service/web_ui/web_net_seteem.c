/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


//General
#define EEM_GENERAL_REPORTINUSESELECTION							"reportinuseselection"
#define EEM_GENERAL_CALLBACKINUSESELECTION							"callbackinuseselection"
#define EEM_GENERAL_PROTOCOLSELECTION								"protocolselection"
#define EEM_GENERAL_MEDIASELECTION									"mediaselection"
#define EEM_GENERAL_CCID											"ccid"
#define EEM_GENERAL_SOCID											"socid"
#define EEM_GENERAL_MAXALARMREPORTS									"maxalarmreports"
#define EEM_GENERAL_CALLELAPSETIME									"callelapsetime"
#define EEM_GENERAL_SECURITYLEVEL									"securitylevel"

//TCPIP
#define EEM_TCPIP_PRIP1												"prip1"
#define EEM_TCPIP_PRIP2												"prip2"
#define EEM_TCPIP_PRIP3												"prip3"
#define EEM_TCPIP_PRIP4												"prip4"
#define EEM_TCPIP_PRTCP												"prtcp"

#define EEM_TCPIP_SRIP1												"srip1"
#define EEM_TCPIP_SRIP2												"srip2"
#define EEM_TCPIP_SRIP3												"srip3"
#define EEM_TCPIP_SRIP4												"srip4"
#define EEM_TCPIP_SRTCP												"srtcp"

#define EEM_TCPIP_CBIP1												"cbip1"
#define EEM_TCPIP_CBIP2												"cbip2"
#define EEM_TCPIP_CBIP3												"cbip3"
#define EEM_TCPIP_CBIP4												"cbip4"
#define EEM_TCPIP_CBTIP												"cbtcp"

#define EEM_TCPIP_SCBIP1											"scbip1"
#define EEM_TCPIP_SCBIP2											"scbip2"
#define EEM_TCPIP_SCBIP3											"scbip3"
#define EEM_TCPIP_SCBIP4											"scbip4"
#define EEM_TCPIP_SCBTCP											"scbtcp"

#define EEM_TCPIP_ITCP												"itcp"

//PSTN
#define EEM_PSTN_PRIPHONE											"priphone"
#define EEM_PSTN_SECPHONE											"secphone"
#define EEM_PSTN_CALLBACKPHONE										"callbackphone"
#define EEM_PSTN_NUMBERRINGS										"numberrings"

#define EEM_PSTN_BAUDRATE											"baudrate"
#define EEM_PSTN_DATABITS											"databits"
#define EEM_PSTN_STOPBITS											"stopbits"
#define EEM_PSTN_PARITY												"parity"
#define EEM_PSTN_PHONE												"phone"

//Leased line
#define EEM_LEASED_BAUDRATE												"baudrate"
#define EEM_LEASED_DATABITS												"databits"
#define EEM_LEASED_STOPBITS												"stopbits"
#define EEM_LEASED_PARITY												"parity"







static void Web_SET_PostIPPage(IN  char *pGetBuf, IN int iLanguage);
static int Web_SET_GetCommandParam(OUT char **pBuf);
static char *Web_SET_SendConfigureCommand(IN char *szBuf);
static void Web_SET_PostEEMPage(IN  char *pGetBuf, IN int iLanguage);
static void Web_SET_PostEEMPageError();



int main(void)
{

	int							iLanguage = 0;				
	//char						*pHtml = NULL;
	//char						szExchange[8];
	char						*szGetBuf = NULL;
	char						*szReturnBuf = NULL;
	int							iAuthority = 0;
	char						*szSessName = NULL ;

	printf("Content-type:text/html\n\n");
	
	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");

	//Authority check
/*	if(Web_CheckUser(WEB_NET_PAGES_WRITE)  == FALSE)
	{
		return FALSE;
	}
*/

	/*get parameter*/
	if (Web_SET_GetCommandParam(&szGetBuf) != FALSE)
	{
		if((szReturnBuf = Web_SET_SendConfigureCommand(szGetBuf)) != NULL)
		{
			//Web_SET_PostIPPage(szReturnBuf, iLanguage);
		}
	}
	else
	{
		Web_SET_PostEEMPageError();

	}
	return TRUE;

}

static char *Web_SET_SendConfigureCommand(IN char *szBuf)	//iModifyPassword = 13:Modify password
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	int		iBufCount = 0;
	//TRACE("szBuf : %s\n", szBuf);
	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return NULL;
	}
	 
	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return NULL;
	}
	
	/*start buffer with pid and a blank*/
	//iLen = sprintf(szBuf1,"%10ld%2d%2d%-128s",(long)getpid(), WEB_SUPPORT_NETSCAPE,iModifyPassword, szBuf);
	iLen = sprintf(szBuf1,"%10ld%2d%2d%-128s",(long)getpid(), WEB_SUPPORT_NETSCAPE,SET_EEM_SETTING, szBuf);
	
	if((write(fd, (void *)szBuf1, iLen + 1))<0)
	{
		close(fd);
		return NULL;
	}
	if((fd2 = open(fifoname,O_RDONLY))<0)
	{
		return NULL;
	}

	
	if((iLen = read(fd2, szBuf2, PIPE_BUF - 1)) > 0)
	{
		Web_SET_PostEEMPage(szBuf2, 1);
	}


	close(fd2);
	close(fd);
	unlink(fifoname);

	return szBuf2;
}




static int Web_SET_GetCommandParam(OUT char **pBuf)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL, *val1 = NULL, *val2 = NULL, *val3 = NULL, *val4 = NULL;
	char	*szSendBuf = NULL;
	int		iLen = 0;
	int		nCommandType;

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}
		 
	szSendBuf = NEW(char, 256);
	if(szSendBuf == NULL)
	{
		return FALSE;
	}
	

//Get command type
	if((val = getValue( getvars, postvars,  CGI_GET_SETTING_TYPE)) != NULL)
	{
		nCommandType = atoi(val);
		iLen += sprintf(szSendBuf + iLen,"%2d",nCommandType);
	}

//General
	if(nCommandType == NET_EEM_GENEARL)
	{
		//Report in use
		if((val = getValue(getvars,postvars,EEM_GENERAL_REPORTINUSESELECTION)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Callback  in use
		if((val = getValue(getvars,postvars,EEM_GENERAL_CALLBACKINUSESELECTION)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Protocol selection
		if((val = getValue(getvars,postvars,EEM_GENERAL_PROTOCOLSELECTION)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));

		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Media selection
		if((val = getValue(getvars,postvars,EEM_GENERAL_MEDIASELECTION)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));

		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//CCID
		if((val = getValue(getvars,postvars,EEM_GENERAL_CCID)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//SOCID
		if((val = getValue(getvars,postvars,EEM_GENERAL_SOCID)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Max alarm reports
		if((val = getValue(getvars,postvars,EEM_GENERAL_MAXALARMREPORTS)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Call elapse time
		if((val = getValue(getvars,postvars,EEM_GENERAL_CALLELAPSETIME)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Security level
		if((val = getValue(getvars,postvars,EEM_GENERAL_SECURITYLEVEL)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

	}
//PSTN
	else if(nCommandType == NET_EEM_PSTN)
	{
		//Primary phone
		if((val = getValue(getvars,postvars,EEM_PSTN_PRIPHONE)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%s",val);
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Secondary phone
		if((val = getValue(getvars,postvars,EEM_PSTN_SECPHONE)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%s",val);
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Callback phone
		if((val = getValue(getvars,postvars,EEM_PSTN_CALLBACKPHONE)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%s",val);
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Baudrate
		if((val = getValue(getvars,postvars,EEM_PSTN_BAUDRATE)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%s",val);
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Parity
		if((val = getValue(getvars,postvars,EEM_PSTN_PARITY)) != NULL)
		{
			if(strcmp(val,"Odd") == 0)
			{
				iLen += sprintf(szSendBuf + iLen,",%c",'o');
			}
			else if(strcmp(val,"Even") == 0)
			{
				iLen += sprintf(szSendBuf + iLen,",%c",'e');
			}
			else
			{
				iLen += sprintf(szSendBuf + iLen,",%c",'n');
			}

		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,",%c",'n');
		}	
		//Data bits
		if((val = getValue(getvars,postvars,EEM_PSTN_DATABITS)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,",%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,",%d",-1);
		}

		//Stop bits
		if((val = getValue(getvars,postvars,EEM_PSTN_STOPBITS)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,",%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,",%d",-1);
		}



		//Phone
		if((val = getValue(getvars,postvars,EEM_PSTN_PHONE)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,":%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,":%d",1000);
		}		
	}
//TCPIP
	else if(nCommandType == NET_EEM_TCPIP)
	{

		//Primary report IP
		if((val1 = getValue(getvars,postvars,EEM_TCPIP_PRIP1)) != NULL
				&& (val2 = getValue(getvars,postvars,EEM_TCPIP_PRIP2)) != NULL
				&& (val3 = getValue(getvars,postvars,EEM_TCPIP_PRIP3)) != NULL 
				&& (val4 = getValue(getvars,postvars,EEM_TCPIP_PRIP4)) != NULL)
		{
			if(atoi(val1) >= 0 && atoi(val1) <= 255 
				&& atoi(val2) >= 0 && atoi(val2) <= 255 
				&& atoi(val3) >= 0 && atoi(val3) <= 255 
				&& atoi(val4) >= 0 && atoi(val4) <= 255 )
			{
				iLen += sprintf(szSendBuf + iLen,";%s.%s.%s.%s",val1,val2,val3,val4);
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//error
			return FALSE;
		}

		//Primary report Port
		if((val = getValue(getvars,postvars,EEM_TCPIP_PRTCP)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,":%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,":%d",65555);
		}	


		//Secondary report IP
		if((val1 = getValue(getvars,postvars,EEM_TCPIP_SRIP1)) != NULL
				&& (val2 = getValue(getvars,postvars,EEM_TCPIP_SRIP2)) != NULL
				&& (val3 = getValue(getvars,postvars,EEM_TCPIP_SRIP3)) != NULL 
				&& (val4 = getValue(getvars,postvars,EEM_TCPIP_SRIP4)) != NULL)
		{
			if(atoi(val1) >= 0 && atoi(val1) <= 255 
				&& atoi(val2) >= 0 && atoi(val2) <= 255 
				&& atoi(val3) >= 0 && atoi(val3) <= 255 
				&& atoi(val4) >= 0 && atoi(val4) <= 255 )
			{
				iLen += sprintf(szSendBuf + iLen,";%s.%s.%s.%s",val1,val2,val3,val4);
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//error
			return FALSE;
		}

		//Secondary report Port
		if((val = getValue(getvars,postvars,EEM_TCPIP_SRTCP)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,":%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,":%d",65555);
		}	

		//Callback report IP1
		if((val1 = getValue(getvars,postvars,EEM_TCPIP_CBIP1)) != NULL
				&& (val2 = getValue(getvars,postvars,EEM_TCPIP_CBIP2)) != NULL
				&& (val3 = getValue(getvars,postvars,EEM_TCPIP_CBIP3)) != NULL 
				&& (val4 = getValue(getvars,postvars,EEM_TCPIP_CBIP4)) != NULL)
		{
			if(atoi(val1) >= 0 && atoi(val1) <= 255 
				&& atoi(val2) >= 0 && atoi(val2) <= 255 
				&& atoi(val3) >= 0 && atoi(val3) <= 255 
				&& atoi(val4) >= 0 && atoi(val4) <= 255 )
			{
				iLen += sprintf(szSendBuf + iLen,";%s.%s.%s.%s",val1,val2,val3,val4);
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//error
			return FALSE;
		}

		//Callback report Port1
		if((val = getValue(getvars,postvars,EEM_TCPIP_CBTIP)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,":%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,":%d",65555);
		}	

		//Callback report IP2
		if((val1 = getValue(getvars,postvars,EEM_TCPIP_SCBIP1)) != NULL
				&& (val2 = getValue(getvars,postvars,EEM_TCPIP_SCBIP2)) != NULL
				&& (val3 = getValue(getvars,postvars,EEM_TCPIP_SCBIP3)) != NULL 
				&& (val4 = getValue(getvars,postvars,EEM_TCPIP_SCBIP4)) != NULL)
		{
			if(atoi(val1) >= 0 && atoi(val1) <= 255 
				&& atoi(val2) >= 0 && atoi(val2) <= 255 
				&& atoi(val3) >= 0 && atoi(val3) <= 255 
				&& atoi(val4) >= 0 && atoi(val4) <= 255 )
			{
				iLen += sprintf(szSendBuf + iLen,";%s.%s.%s.%s",val1,val2,val3,val4);
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//error
			return FALSE;
		}

		//Callback report Port2
		if((val = getValue(getvars,postvars,EEM_TCPIP_SCBTCP)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,":%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,":%d",65555);
		}	

		//Incoming  Port
		if((val = getValue(getvars,postvars,EEM_TCPIP_ITCP)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}	


	}
//Leased line
	else if(nCommandType == NET_EEM_LEASDLINE)
	{

		//Baudrate
		if((val = getValue(getvars,postvars,EEM_LEASED_BAUDRATE)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,";%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,";%d",-1);
		}

		//Parity
		if((val = getValue(getvars,postvars,EEM_LEASED_PARITY)) != NULL)
		{
			if(strcmp(val,"Odd") == 0)
			{
				iLen += sprintf(szSendBuf + iLen,",%c",'o');
			}
			else if(strcmp(val,"Even") == 0)
			{
				iLen += sprintf(szSendBuf + iLen,",%c",'e');
			}
			else
			{
				iLen += sprintf(szSendBuf + iLen,",%c",'n');
			}

		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,",%c",'n');
		}		



		//Data bits
		if((val = getValue(getvars,postvars,EEM_LEASED_DATABITS)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,",%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,",%d",-1);
		}

		//Stop bits
		if((val = getValue(getvars,postvars,EEM_LEASED_STOPBITS)) != NULL)
		{
			iLen += sprintf(szSendBuf + iLen,",%d",atoi(val));
		}
		else
		{
			iLen += sprintf(szSendBuf + iLen,",%d",-1);
		}
	}
	iLen += sprintf(szSendBuf + iLen,"%s",";");
 	*pBuf = szSendBuf;
	cleanUp(getvars, postvars);

    return TRUE;  
}

static void Web_SET_PostEEMPage(IN  char *pGetBuf, IN int iLanguage)
{

//�ѷ��صĽ��д��error.htm�ļ���,������/var/netscapeĿ¼��
	//���ļ�
#define CGI_NET_PATH_ERROR_FILE			"/app/www/netscape/pages/error.htm"	
#define CGI_NET_CONTROL_VALUE			"/*[ID_RETURN]*/"
	char	*pHtml = NULL;
 
	if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
	{
		ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, pGetBuf);
	
		PostPage(pHtml);
		DELETE(pHtml);
		pHtml = NULL;
	}
}

static void Web_SET_PostEEMPageError()
{

	//�ѷ��صĽ��д��error.htm�ļ���,������/var/netscapeĿ¼��
	//���ļ�
#define CGI_NET_PATH_ERROR_FILE			"/app/www/netscape/pages/error.htm"	
#define CGI_NET_CONTROL_VALUE			"/*[ID_RETURN]*/"
	char	*pHtml = NULL;

	if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
	{
		//printf("Content-type:text/html\n\n");
		ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, "0");

		PostPage(pHtml);
		DELETE(pHtml);
		pHtml = NULL;
	}



}


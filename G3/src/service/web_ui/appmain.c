/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : appmain.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-09-28 14:47
 *  VERSION  : V1.00
 *  PURPOSE  :When the client connect to the server, this module provide authority validate,
 *			make device page ,create session.
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"  
#include "public.h"
#include "pubfunc.h"
#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/dir.h> 
#include <unistd.h>

//#ifndef _SUPPORT_THREE_LANGUAGE
//
//    #define	_SUPPORT_THREE_LANGUAGE
//
//#endif


/*CGI var*/
#define LOG_USERNAME					"user_name"
#define LOG_PASSWORD					"user_password"

/*buffer len to save authority level*/
#define MAX_AUTHORITY_LEVEL_LEN			2

/*User authority var*/
#define USER_BROWSER					5
#define USER_OPERATOR					6
#define	USER_ENGINEER					7
#define USER_ADMINISTRATOR				8


#define HTML_SAVE_TEMPLATE_PATH			"/app/www/html/template"//"/var/template"
#define HTML_SAVE_LOC_PATH				"/app/www/html/cgi-bin/loc"//"/var/loc"
#define HTML_SAVE_LOC2_PATH				"/app/www/html/cgi-bin/loc2"//"/var/loc"
#define HTML_SAVE_ENG_PATH				"/app/www/html/cgi-bin/eng"//"/var/eng"

#define HTML_TREE_TEMPLATE_FILE			"/j02_tree_view.js"
#define HTML_EQUIP_TEMPLATE_FILE		"/j77_equiplist_def.htm"


#define HTML_TREE_FILE					"/j02_tree_view_t.js"
#define HTML_EQUIP_FILE					"/j77_equiplist_def.htm"
#define HTML_EQUIP_BACKUP_FILE				"/j77_equiplist_def_back.htm"

#define HTML_MAIN_FILE					"/p01_main_frame.htm"
#define HTML_LOGIN_FILE					"/login.htm"
	
/*must match with the client*/
#define HTML_EQUIP_LIST					"equip_list"
#define HTML_TREE_NODE					"tree_node"
#define HTML_ACU_INFO					"acu_information"
#define HTML_EQUIP_ALARM_LIST			"acu_equip_alarm"
#define HTML_GROUP_DEVICE_LIST			"acu_equip_group"
#define CONTROL_RESULT					"CONTROL_RESULT"
#define USER_DEF_PAGED_NODE				"user_def_pages_node"
#define USER_DEF_PAGED_INFO				"page_list"

#define MAX_CONNECT_NUMBER				5
/*Base64 code*/
const char DeBase64Tab[] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    62,        // '+'
    0, 0, 0,
    63,        // '/'
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61,        // '0'-'9'
    0, 0, 0, 0, 0, 0, 0,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
    13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,        // 'A'-'Z'
    0, 0, 0, 0, 0, 0,
    26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
    39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,        // 'a'-'z'
};

 
//static char *Web_APP_StuffHead( OUT char *pszHead, IN char *pCookie,IN int iLenContent );
static int Web_APP_getCGIParam(OUT char *pszUserInfo,
							   OUT char *pszPass,
							   OUT int *pnLanguage,
							   OUT int *piClearSession,
							   OUT char *szSessionID);

static char  *Web_APP_sendControlCommand(IN const char *pszUserInfo,
								 IN const char *pszPassword,
								 IN const int iLanguage, 
								 IN OUT char **ppszLocEquipList,
								 OUT char **ppszEngEquipList, 
								 OUT char **ppszLocTreeNode,
								 OUT char **ppszEngTreeNode, 
								 OUT char **ppLocACUInfo, 
								 OUT char **ppEngACUInfo, 
								 OUT char **ppszEngEquipAlarmInfo,
								 OUT char **ppszLocEquipAlarmInfo, 
								 OUT char **ppszLocGroupEquipInfo,
							     OUT char **ppszEngGroupEquipInfo,
                                 OUT char **ppszLoc2EquipList,       //Added by wj for three languages 2006.4.29
                                 OUT char **ppszLoc2TreeNode,        //Added by wj for three languages 2006.4.29
                                 OUT char **ppszLoc2ACUInfo,         //Added by wj for three languages 2006.4.29
                                 OUT char **ppszLoc2EquipAlarmInfo,  //Added by wj for three languages 2006.4.29
                                 OUT char **ppszLoc2GroupEquipInfo,
								 OUT char **ppszUserDefPagesNodeLoc,
								 OUT char **ppszUserDefPagesNodeEng,
								 OUT char **ppszUserDefPagesInfoLoc,
								 OUT char **ppszUserDefPagesInfoEng);  //Added by wj for three languages 2006.4.29

int DecodeBase64(const char* pSrc,  char* pDst, int iSrcLen);

static int Web_APP_LoadDataPtr(IN  char *pBuf, 
					   OUT char **ppszLocEquipList, 
					   OUT char **ppszEngEquipList, 
					   OUT char **ppszLocTreeNode, 
					   OUT char **ppszEngTreeNode,
					   OUT char **ppszLocACUInfo, 
					   OUT char **ppszEngACUInfo,
					   OUT char **ppszEngEquipAlarmInfo,
					   OUT char **ppszLocEquipAlarmInfo,
					   OUT char **ppszLocGroupDeviceInfo, 
					   OUT char **ppszEngGroupDeviceInfo,
                       OUT char **ppszLoc2EquipList,       //Added by wj for three languages 2006.4.29
                       OUT char **ppszLoc2TreeNode,        //Added by wj for three languages 2006.4.29
                       OUT char **ppszLoc2ACUInfo,         //Added by wj for three languages 2006.4.29
                       OUT char **ppszLoc2EquipAlarmInfo,  //Added by wj for three languages 2006.4.29
                       OUT char **ppszLoc2GroupEquipInfo,
					   OUT char **ppszUserDefPagesNodeLoc,
					   OUT char **ppszUserDefPagesNodeEng,
					   OUT char **ppszUserDefPagesInfoLoc,
					   OUT char **ppszUserDefPagesInfoEng);  //Added by wj for three languages 2006.4.29

static BOOL getFileListNumber(void);
//static void MakeModiTime(void);

char		strModiTime[128];
char		stRectNum[6];


/*==========================================================================*
 * FUNCTION :  Web_APP_StuffHead
 * PURPOSE  :  To write the info of modify time,lenContent,hashkey,strModiTime  to client
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char *pszHead:
				IN int iLenContent: 
 * RETURN   :  static char *
 * COMMENTS :	called when return the html to the client 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
 
//static char *Web_APP_StuffHead( OUT char *pszHead, IN char *pCookie,IN int iLenContent )
//{
//    sprintf( pszHead,
//			"HTTP/1.1 200 OK\r\n"   // \x0d\x0a
//			"Server: Apache/2.0.45\r\n"
//			"Date: %s\r\n"
//			"Content-type: text/html\r\n"
//			"Content-length: %d\r\n"
//			"Last-Modified: %s\r\n"
//			"Set-Cookie:%s\r\n"	
//			"\r\n\n", 
//			strModiTime,
//       		iLenContent,
//			strModiTime,
//			"mm=23");
//
//    return pszHead;
//}
 

int main(void)
{
	char		*pEngEquipHtml = NULL , *pLocEquipHtml = NULL, *pEngTreeHtml = NULL, *pLocTreeHtml = NULL;
    char        *pLoc2EquipHtml = NULL, *pLoc2TreeHtml = NULL;//Added by wj for three languages 2006.4.29
	char		szUserName[16],szPass[64];
	int			iAuthority = 0,	iLanguage = 0, iClearSession = 0;
	char		*pszAuthority = NULL;
	char		*pszLocEquipList = NULL, *pszEngEquipList = NULL, *pszLocTreeNode = NULL;
	char		*pszLocACUInfo = NULL, *pszEngACUInfo = NULL, *pszEngTreeNode = NULL;
	char		*pszEngEquipAlarmInfo = NULL,*pszLocEquipAlarmInfo = NULL;   //for alarm
	char		*pszLocGroupEquipInfo = NULL, *pszEngGroupEquipInfo = NULL;
	char        *pszUserDefPagesNodeLoc = NULL,*pszUserDefPagesNodeEng = NULL;
    char		*pszUserDefPagesInfoLoc = NULL,*pszUserDefPagesInfoEng = NULL;
	//Added by wj for three languages 2006.4.29
    char        *pszLoc2EquipList = NULL,*pszLoc2TreeNode = NULL,*pszLoc2ACUInfo = NULL,*pszLoc2EquipAlarmInfo = NULL,*pszLoc2GroupEquipInfo = NULL;
	//end////////////////////////////////////////////////////////////////////////
    FILE		*fp = NULL;
	char		szMainFile[64];			/*get the file : p01_main_frame.htm*/
	char		pEngTemplateFile[128], pLocTemplateFile[128];
    char        pLoc2TemplateFile[128];//Added by wj for three languages 2006.4.29
	char		pEquipFile[128];
	char		pExchangeFile[64];
	int			iLen = 0;
	char		szLoginFile[64], *pLoginHtml = NULL, szReturnValue[4];
	char		*pCookie = NULL;
	BOOL		boolInitLoc = FALSE, boolInitEng = FALSE;
	char		szSessionID[17] = {0};
	char		szSessionFile[64] ={0};

    
//////////////////////////////////////////////////////////////////////////
//for debug    
   /* FILE        *fpDebug=NULL;
    fpDebug=fopen("/var/appmain.txt","a+");*/

//end//////////////////////////////////////////////////////////////////////////


	
	//printf("Content-type:text/html\n\n");
	//printf("Set-cookie:mm=1\n");
	//printf("\n\n");
	//printf("<html><title>main</title>\n");
	//printf("<head>");
	//printf("<script language=JavaScript><!--\n");
	//printf("alert(document.cookie);");
	//printf("//--></script>");
	//printf("</head>");
	//printf("</html>\n");
	//return FALSE;

	memset(szUserName, 0, sizeof(szUserName));
	memset(szPass, 0, sizeof(szPass));
	if(Web_APP_getCGIParam(szUserName,szPass,&iLanguage, &iClearSession, szSessionID) == FALSE)
	{
		return FALSE;
	}


	
	
    if(iLanguage == 1)
    {
        iLanguage =	 LOCAL_LANGUAGE_NAME;
    }
    else if(iLanguage == 2)//Added by wj for three languages 2006.4.29
    {
        iLanguage =	 LOCAL2_LANGUAGE_NAME;
    }
	else if(iLanguage == 0)
	{
		iLanguage =	ENGLISH_LANGUAGE_NAME;
	}
	
	
	/*Get login html file*/
 	if(iLanguage ==	ENGLISH_LANGUAGE_NAME)
	{
		iLen = sprintf(szLoginFile,"%s%s", HTML_SAVE_ENG_PATH, HTML_LOGIN_FILE);
		szLoginFile[iLen] = '\0';

		/*get p01_main_frame.htm*/
		iLen = sprintf(szMainFile,"%s%s", HTML_SAVE_ENG_PATH, HTML_MAIN_FILE);
		szMainFile[iLen] = '\0';
	}
	else if(iLanguage == LOCAL_LANGUAGE_NAME)
	{
		iLen = sprintf(szLoginFile,"%s%s", HTML_SAVE_LOC_PATH, HTML_LOGIN_FILE);
		szLoginFile[iLen] = '\0';

				/*get p01_main_frame.htm*/
		iLen = sprintf(szMainFile,"%s%s", HTML_SAVE_LOC_PATH, HTML_MAIN_FILE);
		szMainFile[iLen] = '\0';
	}
    else if(iLanguage == LOCAL2_LANGUAGE_NAME) //Added by wj for three languages 2006.4.29
    {
        iLen = sprintf(szLoginFile,"%s%s", HTML_SAVE_LOC2_PATH, HTML_LOGIN_FILE);
        szLoginFile[iLen] = '\0';

        /*get p01_main_frame.htm*/
        iLen = sprintf(szMainFile,"%s%s", HTML_SAVE_LOC2_PATH, HTML_MAIN_FILE);
        szMainFile[iLen] = '\0';
    }


	if( LoadHtmlFile(szLoginFile, &pLoginHtml) <= 0)
	{
		return FALSE;
	}

	if(iClearSession > 0)
	{
		sprintf(szSessionFile,"%s%s",SESSION_DEFAULT_DIR, szSessionID); 
        remove(szSessionFile);

		return FALSE;
	}

	//list session file number
	if(getFileListNumber() == FALSE)
	{
		clean_session_file();
		ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), "5");
		printf("Content-type:text/html\n\n");

		PostPage(pLoginHtml);

		DELETE(pLoginHtml);
		pLoginHtml = NULL;

		return FALSE;
	}

 	/*send equipId to ACU and get realtimedata*/
	
	pszAuthority = Web_APP_sendControlCommand(szUserName, 
									  szPass, 
									  iLanguage, 
									  &pszLocEquipList, 
									  &pszEngEquipList, 
									  &pszLocTreeNode, 
									  &pszEngTreeNode,
									  &pszLocACUInfo, 
									  &pszEngACUInfo, 
									  &pszEngEquipAlarmInfo,
									  &pszLocEquipAlarmInfo, 
									  &pszLocGroupEquipInfo,
									  &pszEngGroupEquipInfo,
                                      &pszLoc2EquipList,        //Added by wj for three languages 2006.4.29
                                      &pszLoc2TreeNode,         //Added by wj for three languages 2006.4.29
                                      &pszLoc2ACUInfo,          //Added by wj for three languages 2006.4.29
                                      &pszLoc2EquipAlarmInfo,   //Added by wj for three languages 2006.4.29
                                      &pszLoc2GroupEquipInfo,
									  &pszUserDefPagesNodeLoc,
									  &pszUserDefPagesNodeEng,
									  &pszUserDefPagesInfoLoc,
									  &pszUserDefPagesInfoEng);  //Added by wj for three languages 2006.4.29

   /* FILE        *fpDebug2=NULL;
    fpDebug2=fopen("/var/appmain2.txt","a+");
    fwrite(pszLoc2EquipList,strlen(pszLoc2EquipList),1,fpDebug2);
    fwrite(pszLoc2TreeNode,strlen(pszLoc2TreeNode),1,fpDebug2);
    fwrite(pszLoc2ACUInfo,strlen(pszLoc2ACUInfo),1,fpDebug2);
    fwrite(pszLoc2EquipAlarmInfo,strlen(pszLoc2EquipAlarmInfo),1,fpDebug2);
    fwrite(pszLoc2GroupEquipInfo,strlen(pszLoc2GroupEquipInfo),1,fpDebug2);
    fclose(fpDebug2);*/



	if(pszAuthority != NULL)
	{
		iAuthority = atoi(pszAuthority);
	
		/*head*/

		if(iAuthority > 0)
		{
			/*Construct session file based on User namme and password*/
			pCookie = set_session(szUserName,szPass, pszAuthority);
			if(pCookie == NULL)
			{
				return FALSE;
			}


			DELETE(pszAuthority);
			pszAuthority = NULL;
			//time_t now = time(NULL);
			//srand( now );
			//MakeModiTime(strModiTime,(int)strlen(strModiTime));
			/*Load device_def.js*/


   			/*Load English file*/
			iLen = 0;
			iLen = sprintf(pEngTemplateFile,"%s%s", HTML_SAVE_ENG_PATH, HTML_TREE_TEMPLATE_FILE);
			pEngTemplateFile[iLen] = '\0';

			/*Load local file*/
			iLen = 0;
			iLen = sprintf(pLocTemplateFile,"%s%s", HTML_SAVE_LOC_PATH, HTML_TREE_TEMPLATE_FILE);
			pLocTemplateFile[iLen] = '\0';

//////////////////////////////////////////////////////////////////////////

            /*Load local2 file*///Added by wj for three languages 2006.4.29
            #ifdef _SUPPORT_THREE_LANGUAGE 
            iLen = 0;
            iLen = sprintf(pLoc2TemplateFile,"%s%s", HTML_SAVE_LOC2_PATH, HTML_TREE_TEMPLATE_FILE);
            pLoc2TemplateFile[iLen] = '\0';
            #endif

//end////////////////////////////////////////////////////////////////////////


			iLen = 0;
			iLen = sprintf(pEquipFile, "%s%s",HTML_SAVE_TEMPLATE_PATH, HTML_EQUIP_TEMPLATE_FILE);
			pEquipFile[iLen] = '\0';
			

			//Default language html
 			if(LoadHtmlFile(pEngTemplateFile, &pEngTreeHtml ) > 0 &&
					LoadHtmlFile(pEquipFile, &pEngEquipHtml ) > 0)
			{
				 
				/*Replace  with new information*/
				if(pszEngEquipList != NULL && 
					pszEngACUInfo != NULL && 
					pszEngEquipAlarmInfo != NULL && 
					pszEngTreeNode != NULL && 
					pszEngGroupEquipInfo != NULL)
				{
					ReplaceString(&pEngEquipHtml,MAKE_VAR_FIELD(HTML_ACU_INFO),
												pszEngACUInfo);
					ReplaceString(&pEngEquipHtml,MAKE_VAR_FIELD(HTML_EQUIP_ALARM_LIST),
												pszEngEquipAlarmInfo);

					ReplaceString(&pEngEquipHtml,MAKE_VAR_FIELD(HTML_GROUP_DEVICE_LIST),
												pszEngGroupEquipInfo);
					
					//User Define////////////////////////////////////////////////////////////////////////

					ReplaceString(&pEngTreeHtml,MAKE_VAR_FIELD(USER_DEF_PAGED_NODE),
						pszUserDefPagesNodeEng);

					ReplaceString(&pEngEquipHtml,MAKE_VAR_FIELD(USER_DEF_PAGED_INFO),
						pszUserDefPagesInfoEng);

					ReplaceString(&pEngTreeHtml,MAKE_VAR_FIELD("TREE_RECT_NUM"),
						stRectNum);
					//////////////////////////////////////////////////////////////////////////
					//Make a template in order to update the j77 after inserting the rectifier or change the position, 2011-11-29
					iLen = sprintf(pExchangeFile, "%s%s",WEB_ENG_VAR, HTML_EQUIP_BACKUP_FILE);
					pExchangeFile[iLen] = '\0';
					if((fp = fopen(pExchangeFile,"wb")) !=  NULL)
					{
						fwrite(pEngEquipHtml,strlen(pEngEquipHtml), 1, fp);
						fclose(fp);
					}
					//End ,2011-11-29.

					ReplaceString(&pEngEquipHtml,MAKE_VAR_FIELD(HTML_EQUIP_LIST),
												pszEngEquipList);

					ReplaceString(&pEngTreeHtml,MAKE_VAR_FIELD(HTML_TREE_NODE),
													pszEngTreeNode);
					
					
					
					/*free*/
					DELETE(pszEngEquipList);
					pszEngEquipList = NULL;
					DELETE(pszEngACUInfo);
					pszEngACUInfo = NULL;
					DELETE(pszEngTreeNode);
					pszEngTreeNode = NULL;
					DELETE(pszEngEquipAlarmInfo);
					pszEngEquipAlarmInfo = NULL;
					DELETE(pszEngGroupEquipInfo);
					pszEngGroupEquipInfo = NULL;

					DELETE(pszUserDefPagesNodeEng);
					pszUserDefPagesNodeEng = NULL;

					DELETE(pszUserDefPagesInfoEng);
					pszUserDefPagesInfoEng = NULL;

					/*save as new file*/
					iLen = sprintf(pExchangeFile, "%s%s",WEB_ENG_VAR, HTML_TREE_FILE);
					if((fp = fopen(pExchangeFile,"wb")) != NULL)
					{
						fwrite(pEngTreeHtml,strlen(pEngTreeHtml), 1, fp);
						fclose(fp);
					
					}
					
					iLen = sprintf(pExchangeFile, "%s%s",WEB_ENG_VAR, HTML_EQUIP_FILE);
					pExchangeFile[iLen] = '\0';
					if((fp = fopen(pExchangeFile,"wb")) !=  NULL)
					{
						fwrite(pEngEquipHtml,strlen(pEngEquipHtml), 1, fp);
						fclose(fp);
					}
					
					boolInitEng = TRUE;
				
				}
				DELETE(pEngTreeHtml);
				pEngTreeHtml = NULL;
				DELETE(pEngEquipHtml);
				pEngEquipHtml = NULL;
			}
		 

			/*Local language file*/
			if(LoadHtmlFile(pLocTemplateFile, &pLocTreeHtml ) > 0 &&
					LoadHtmlFile(pEquipFile, &pLocEquipHtml ) > 0  )
			{
				 
				if(pszLocEquipList != NULL && 
					pszLocACUInfo != NULL && 
					pszLocEquipAlarmInfo != NULL && 
					pszLocTreeNode != NULL && 
					pszLocGroupEquipInfo != NULL)
				{
		

					ReplaceString(&pLocEquipHtml,MAKE_VAR_FIELD(HTML_ACU_INFO),
													pszLocACUInfo);
					ReplaceString(&pLocEquipHtml,MAKE_VAR_FIELD(HTML_EQUIP_ALARM_LIST),
													pszLocEquipAlarmInfo);
					
					ReplaceString(&pLocEquipHtml,MAKE_VAR_FIELD(HTML_GROUP_DEVICE_LIST),
													pszLocGroupEquipInfo);
					
					
					//User Define////////////////////////////////////////////////////////////////////////

					ReplaceString(&pLocTreeHtml,MAKE_VAR_FIELD(USER_DEF_PAGED_NODE),
															pszUserDefPagesNodeLoc);

					ReplaceString(&pLocEquipHtml,MAKE_VAR_FIELD(USER_DEF_PAGED_INFO),
															pszUserDefPagesInfoLoc);

					ReplaceString(&pLocTreeHtml,MAKE_VAR_FIELD("TREE_RECT_NUM"),
															stRectNum);

		
					//////////////////////////////////////////////////////////////////////////
					//Make a template in order to update the j77 after inserting the rectifier or change the position, 2011-11-29
					iLen = sprintf(pExchangeFile, "%s%s",WEB_LOC_VAR, HTML_EQUIP_BACKUP_FILE);
					pExchangeFile[iLen] = '\0';
					if((fp = fopen(pExchangeFile,"wb")) !=   NULL )
					{
						fwrite(pLocEquipHtml, strlen(pLocEquipHtml), 1, fp);
						fclose(fp);
					}
					//End, 2011-11-29
					
					
					ReplaceString(&pLocEquipHtml,MAKE_VAR_FIELD(HTML_EQUIP_LIST),
						pszLocEquipList);
					ReplaceString(&pLocTreeHtml,MAKE_VAR_FIELD(HTML_TREE_NODE),
						pszLocTreeNode);

					/*free*/
					DELETE(pszLocEquipList);
					pszLocEquipList = NULL;
					DELETE(pszLocACUInfo);
					pszLocACUInfo = NULL;
					DELETE(pszLocTreeNode);
					pszLocTreeNode = NULL;
					DELETE(pszLocEquipAlarmInfo);
					pszLocEquipAlarmInfo = NULL;
					DELETE(pszLocGroupEquipInfo);
					pszLocGroupEquipInfo = NULL;

					DELETE(pszUserDefPagesNodeLoc);
					pszUserDefPagesNodeLoc = NULL;

					DELETE(pszUserDefPagesInfoLoc);
					pszUserDefPagesInfoLoc = NULL;
					
	
					iLen = sprintf(pExchangeFile, "%s%s",WEB_LOC_VAR, HTML_TREE_FILE);
					pExchangeFile[iLen] = '\0';

					if((fp = fopen(pExchangeFile,"wb")) !=   NULL)
					{
						fwrite(pLocTreeHtml,strlen(pLocTreeHtml), 1, fp);
						fclose(fp);	
					}
			
					iLen = sprintf(pExchangeFile, "%s%s",WEB_LOC_VAR, HTML_EQUIP_FILE);
					pExchangeFile[iLen] = '\0';
					if((fp = fopen(pExchangeFile,"wb")) != NULL )
					{
						fwrite(pLocEquipHtml, strlen(pLocEquipHtml), 1, fp);
						fclose(fp);
					}
				
					boolInitLoc = TRUE;
				}
				DELETE(pLocTreeHtml);
				pLocTreeHtml = NULL;
				DELETE(pLocEquipHtml);
				pLocEquipHtml = NULL;
			}

           /* char *temp4="begin write Local2 language file";
            fwrite(temp4,strlen(temp4),1,fpDebug);*/
//////////////////////////////////////////////////////////////////////////
//Added by wj for three languages 2006.4.29
            /*Local2 language file*/
            #ifdef _SUPPORT_THREE_LANGUAGE 
            if(LoadHtmlFile(pLoc2TemplateFile, &pLoc2TreeHtml ) > 0 &&
                LoadHtmlFile(pEquipFile, &pLoc2EquipHtml ) > 0  )
            {

               ///* char *temp3="into write Local2 language file";*/
               // fwrite(temp3,strlen(temp3),1,fpDebug);


                if(pszLoc2EquipList != NULL && 
                    pszLoc2ACUInfo != NULL && 
                    pszLoc2EquipAlarmInfo != NULL && 
                    pszLoc2TreeNode != NULL && 
                    pszLoc2GroupEquipInfo != NULL)
                {

                    //fwrite(temp3,strlen(temp3),1,fpDebug);

                    ReplaceString(&pLoc2EquipHtml,MAKE_VAR_FIELD(HTML_EQUIP_LIST),
                        pszLoc2EquipList);
                    ReplaceString(&pLoc2EquipHtml,MAKE_VAR_FIELD(HTML_ACU_INFO),
                        pszLoc2ACUInfo);
                    ReplaceString(&pLoc2EquipHtml,MAKE_VAR_FIELD(HTML_EQUIP_ALARM_LIST),
                        pszLoc2EquipAlarmInfo);

                    ReplaceString(&pLoc2EquipHtml,MAKE_VAR_FIELD(HTML_GROUP_DEVICE_LIST),
                        pszLoc2GroupEquipInfo);

                    ReplaceString(&pLoc2TreeHtml,MAKE_VAR_FIELD(HTML_TREE_NODE),
                        pszLoc2TreeNode);


                    /*free*/
                    DELETE(pszLoc2EquipList);
                    pszLoc2EquipList = NULL;
                    DELETE(pszLoc2ACUInfo);
                    pszLoc2ACUInfo = NULL;
                    DELETE(pszLoc2TreeNode);
                    pszLoc2TreeNode = NULL;
                    DELETE(pszLoc2EquipAlarmInfo);
                    pszLoc2EquipAlarmInfo = NULL;
                    DELETE(pszLoc2GroupEquipInfo);
                    pszLoc2GroupEquipInfo = NULL;


                    iLen = sprintf(pExchangeFile, "%s%s",WEB_LOC2_VAR,//HTML_SAVE_LOC2_PATH, 
                        HTML_TREE_FILE);
                    pExchangeFile[iLen] = '\0';

                    if((fp = fopen(pExchangeFile,"wb")) !=   NULL)
                    {
                       /* char *temp1="loc2/treenode write ok!!";
                        fwrite(temp1,strlen(temp1),1,fpDebug);*/

                        fwrite(pLoc2TreeHtml,strlen(pLoc2TreeHtml), 1, fp);
                        fclose(fp);	
                    }

                    iLen = sprintf(pExchangeFile, "%s%s",WEB_LOC2_VAR,//, HTML_SAVE_LOC2_PATH
                        HTML_EQUIP_FILE);
                    pExchangeFile[iLen] = '\0';
                    if((fp = fopen(pExchangeFile,"wb")) !=   NULL )
                    {
                       /* char *temp2="loc2/equiplist write ok!!";
                        fwrite(temp2,strlen(temp2),1,fpDebug);*/

                        fwrite(pLoc2EquipHtml, strlen(pLoc2EquipHtml), 1, fp);
                        fclose(fp);
                    }

                    boolInitLoc = TRUE;
                }
                DELETE(pLoc2TreeHtml);
                pLocTreeHtml = NULL;
                DELETE(pLoc2EquipHtml);
                pLocEquipHtml = NULL;
            }
            #endif
//end////////////////////////////////////////////////////////////////////////

			/*Post Login html*/
	

			//if(LoadHtmlFile(szMainFile, &pMainHtml ) > 0)
			//{
					
			//MakeModiTime();
			//Web_APP_StuffHead( szHead, pCookie, strlen(pLoginHtml) );
			//PostPage(szHead);
			printf("Content-type:text/html\n");
			printf("Set-cookie:%s", pCookie);
			printf("\n\n");
			DELETE(pCookie);
			pCookie = NULL;
			
			//printf("%s\n",pCookie);
			//printf("Set-cookie:%s\n","m1=1");
			//printf("Set-cookie:%s\n","m2=1");
			//printf("Set-cookie:%s\n","hash_key=12");
			//printf("\n\n");
			//DELETE(pCookie);
			//pCookie =NULL;

				//PostPage(pMainHtml);
				//DELETE(pMainHtml);
				//pMainHtml = NULL;
			//}
			//iLen = sprintf(szReturnValue,"%d",1);
			//szReturnValue[iLen] ='\0';
			
			//if(boolInitEng && boolInitLoc)
			//{
				ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), "1");
			//}
			//else
			//{
			//	ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), "0");
			//}
		//	printf("Content-type:text/html\n\n");

			PostPage(pLoginHtml);
			DELETE(pLoginHtml);
            pLoginHtml = NULL;

		}
		else  
		{
			if(iAuthority == -3)
            {

                iLen = sprintf(szReturnValue,"%d", 6);
                szReturnValue[iLen] = '\0';
                ///*Return value :2 incorrect password, 3 : incorrect user*/
                ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
                printf("Content-type:text/html\n\n");

                PostPage(pLoginHtml);

                DELETE(pLoginHtml);
                pLoginHtml = NULL;
                DELETE(pszAuthority);
                pszAuthority = NULL; 
            }
			else if(iAuthority == -4)
			{

				iLen = sprintf(szReturnValue,"%d", 7);
				szReturnValue[iLen] = '\0';
				///*Return value :2 incorrect password, 3 : incorrect user*/
				ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
				printf("Content-type:text/html\n\n");

				PostPage(pLoginHtml);

				DELETE(pLoginHtml);
				pLoginHtml = NULL;
				DELETE(pszAuthority);
				pszAuthority = NULL; 
			}
			else if(iAuthority == -5)
			{

				iLen = sprintf(szReturnValue,"%d", 8);
				szReturnValue[iLen] = '\0';
				///*Return value :2 incorrect password, 3 : incorrect user*/
				ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
				printf("Content-type:text/html\n\n");

				PostPage(pLoginHtml);

				DELETE(pLoginHtml);
				pLoginHtml = NULL;
				DELETE(pszAuthority);
				pszAuthority = NULL; 
			}
            else
            {
			    iLen = sprintf(szReturnValue,"%d", iAuthority + 4);
			    szReturnValue[iLen] = '\0';
			    ///*Return value :2 incorrect password, 3 : incorrect user*/
			    ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
			    printf("Content-type:text/html\n\n");

			    PostPage(pLoginHtml);

			    DELETE(pLoginHtml);
			    pLoginHtml = NULL;
			    DELETE(pszAuthority);
			    pszAuthority = NULL; 
            }
		}
			////AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_INFO," Error : Fail to get User or Password!");
		
	}
	else
	{
		//print_no_authority_error(iAuthority);
		iLen = sprintf(szReturnValue,"%d", 4 );
		/*Return value :2 incorrect password, 3 : incorrect user*/
		
		ReplaceString(&pLoginHtml, MAKE_VAR_FIELD(CONTROL_RESULT), szReturnValue);
		
		//printf(pLoginHtml);
		printf("Content-type:text/html\n\n");

		PostPage(pLoginHtml);
		
		DELETE(pLoginHtml);
		pLoginHtml = NULL;
		DELETE(pszAuthority);
		pszAuthority = NULL; 
		
		 
	}

	
	/*Delete */
	if(pszAuthority != NULL)
	{
		DELETE(pszAuthority);
		pszAuthority = NULL;
	}
		
	if(pszEngEquipList != NULL)
	{
		DELETE(pszEngEquipList);
		pszEngEquipList = NULL;
	}
	if(pszEngACUInfo != NULL)
	{
		DELETE(pszEngACUInfo);
		pszEngACUInfo = NULL;
	}
	if(pszEngTreeNode != NULL)
	{
		DELETE(pszEngTreeNode);
		pszEngTreeNode = NULL;
	}
	if(pszLocEquipList != NULL)
	{
		DELETE(pszLocEquipList);
		pszLocEquipList = NULL;
	}
	if(pszLocACUInfo != NULL)
	{
		DELETE(pszLocACUInfo);
		pszLocACUInfo = NULL;
	}
	if(pszLocTreeNode != NULL)
	{
		DELETE(pszLocTreeNode);
		pszLocTreeNode = NULL;
	}

	if(pLoginHtml != NULL)
	{
		DELETE(pLoginHtml);
		pLoginHtml = NULL;
	}
	if(pEngEquipHtml != NULL)
	{
		DELETE(pEngEquipHtml);
		pEngEquipHtml = NULL;
	}
	if(pLocEquipHtml != NULL)
	{
		DELETE(pLocEquipHtml);
		pLocEquipHtml = NULL;
	}
	if(pEngTreeHtml != NULL)
	{
		DELETE(pEngTreeHtml);
		pEngTreeHtml = NULL;
	}
	if(pLocTreeHtml != NULL)
	{
		DELETE(pLocTreeHtml);
		pLocTreeHtml = NULL;
	}

	if(pszLocEquipAlarmInfo != NULL)
	{
		DELETE(pszLocEquipAlarmInfo);
		pszLocEquipAlarmInfo = NULL;
	}
	
	if(pszEngEquipAlarmInfo != NULL)
	{
		DELETE(pszEngEquipAlarmInfo);
		pszEngEquipAlarmInfo = NULL;
	}
	return TRUE; 
	 
//////////////////////////////////////////////////////////////////////////
//for debug

    /*fclose(fpDebug);*/

//end//////////////////////////////////////////////////////////////////////////
    
}


/*==========================================================================*
 * FUNCTION :  Web_APP_getCGIParam
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	OUT char *pszUserInfo:
				OUT char *pszPass:
				OUT int *pnLanguage:
				OUT int *piClearSession:
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : 			             DATE: 2004-11-06 10:20
 *==========================================================================*/
static int Web_APP_getCGIParam(OUT char *pszUserInfo,
							   OUT char *pszPass,
							   OUT int *pnLanguage,
							   OUT int *piClearSession,
							   OUT char *szSessionID)
{
    char **postvars = NULL; /* POST request data repository */
    char **getvars = NULL; /* GET request data repository */
    int form_method; /* POST = 1, GET = 0 */  
    char *val = NULL;
	char szUserVal[64];
	/*get html send form method : POST or Get*/
    form_method = getRequestMethod();
    if(form_method == POST) 
	{
        getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		return FALSE;
	}
	
	/*get param*/

    val = getValue( getvars, postvars, LOG_USERNAME );
    if( val != NULL )
    {
		
		/*user*/
		memset(szUserVal, 0, sizeof(szUserVal));
		sprintf(szUserVal,"%s",val);

	//	if(strlen(szUserVal) == 4)
	//	{
	//		DecodeBase64(szUserVal,pszUserInfo,1);//(int)strlen(val));
	//	}
	//	else if(strlen(szUserVal) == 8)
	//	{
	//		DecodeBase64(szUserVal,pszUserInfo,2);//(int)strlen(val));
	//	}
	//	else
	//	{
			DecodeBase64(szUserVal,pszUserInfo,(int)strlen(szUserVal));
	//	}
		
    }
	memset(szUserVal, 0, sizeof(szUserVal));
	/*password*/
	val = getValue( getvars, postvars, LOG_PASSWORD );
    if( val != NULL ) 
    {
		sprintf(szUserVal,"%s",val);

		//if(strlen(szUserVal) == 4)
		//{
	//		DecodeBase64(szUserVal,pszPass,1);
	//	}
	//	else if(strlen(szUserVal) == 8)
	//	{
	//		DecodeBase64(szUserVal,pszPass,2);
	//	}
	//	else
	//	{
			DecodeBase64(szUserVal,pszPass,(int)strlen(szUserVal));
	//	}
    }
	
	
	/*language type*/
	val = getValue(getvars,postvars,LANGUAGE_TYPE);
	if(val != NULL)
	{
		*pnLanguage = atoi(val);   //Language type
	}
	
	/*clear session*/
	val = getValue(getvars,postvars,CGI_CLEAR_SESSION);


	char szVal[32];
	if(val != NULL)
	{
		
		*piClearSession = 1;//atoi(val);   //clear session
		if((val = getValue(getvars,postvars,SESSION_ID)) != NULL)
		{
			strncpyz(szSessionID, val, 17);
		}
		sprintf(szVal, "%d\n", 1);
	}
	else
	{
		*piClearSession = 0;
		sprintf(szVal, "%d\n", 0);
	}



	/*free*/
	cleanUp( getvars, postvars);

    return TRUE;  
}




/*==========================================================================*
 * FUNCTION :  Web_APP_sendControlCommand
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : 			             DATE: 2004-11-06 10:20
 *==========================================================================*/
static char  *Web_APP_sendControlCommand(IN const char *pszUserInfo,
								 IN const char *pszPassword,
								 IN const int iLanguage, 
								 IN OUT char **ppszLocEquipList,
								 OUT char **ppszEngEquipList, 
								 OUT char **ppszLocTreeNode,
								 OUT char **ppszEngTreeNode, 
								 OUT char **ppLocACUInfo, 
								 OUT char **ppEngACUInfo, 
								 OUT char **ppszEngEquipAlarmInfo,
								 OUT char **ppszLocEquipAlarmInfo, 
								 OUT char **ppszLocGroupEquipInfo,
								 OUT char **ppszEngGroupEquipInfo,
                                 OUT char **ppszLoc2EquipList,       //Added by wj for three languages 2006.4.29
                                 OUT char **ppszLoc2TreeNode,        //Added by wj for three languages 2006.4.29
                                 OUT char **ppszLoc2ACUInfo,         //Added by wj for three languages 2006.4.29
                                 OUT char **ppszLoc2EquipAlarmInfo,  //Added by wj for three languages 2006.4.29
                                 OUT char **ppszLoc2GroupEquipInfo,
								 OUT char **ppszUserDefPagesNodeLoc,
								 OUT char **ppszUserDefPagesNodeEng,
								 OUT char **ppszUserDefPagesInfoLoc,
								 OUT char **ppszUserDefPagesInfoEng)  //Added by wj for three languages 2006.4.29
{
	int				fdMain,fdProcess;    //fifo handle
	int				iLen = 0, iBufCount = 0;
	char			szBuf1[1024],szBuf2[PIPE_BUF],szFifoName[32];
 	mode_t			mode = 0666;
	char			*pBuf = NULL;
	char			*pszAuthority = NULL;
	char			*pDelete = NULL;   //Used by save the first address of pBuf, aim to delete;

	/*create FIFO with our PID as part of name*/
	/*open Main fifo to write*/ 

	if((fdMain = open(MAIN_FIFO_NAME,O_WRONLY )) < 0)
	{
		////AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_WARNING,"Error : Fail to open FIFO [%s]", MAIN_FIFO_NAME);
		//TRACE("Error : Fail to open FIFO---2\n");
		return NULL;
	}


	sprintf(szFifoName,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	
	/*make process fifo*/
	if((mkfifo(szFifoName,mode))<0)
	{
		////AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_WARNING,"Error : Fail to make FIFO [%s]", szFifoName);
		return NULL;
	}
	/*start buffer with pid and a blank*/
	iLen = sprintf(szBuf1,"%10ld%2d%16s%16s%2d",(long)getpid(), LOG_IN,pszUserInfo,pszPassword,iLanguage);
	szBuf1[iLen] = '\0';

	/*write data to web_ui_comm*/
	if((write(fdMain,szBuf1,strlen(szBuf1) + 1)) < 0)
	{
		//AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_WARNING,"Error : Fail to write FIFO %s", MAIN_FIFO_NAME);
		close(fdMain);
		return NULL;
	}

	/*open process fifo to read*/
	if((fdProcess = open(szFifoName,O_RDONLY ))<0)
	{
		//AppLogOut(CGI_APP_LOG_APPMAIN_NAME,APP_LOG_WARNING,"Fail to open FIFO [%s]", szFifoName);
        close(fdMain);
        close(fdProcess);
		return NULL;
	}

	/*allocate memory to save the data from web_ui_comm*/
	pBuf = NEW(char,10 * PIPE_BUF);
	if(pBuf == NULL)
	{
        close(fdMain);
        close(fdProcess);
        return NULL;
	}
	//FILE *fp;
	/*read data from web_ui_comm*/
	//int iGetBufferLen = 0;
	while((iLen = read(fdProcess,szBuf2,PIPE_BUF - 1)) > 0)
	{

		if(iBufCount >= 10)
		{
			pBuf = RENEW(char, pBuf, (iBufCount + 1) * PIPE_BUF);
			if(pBuf == NULL)
			{
                close(fdMain);
                close(fdProcess);
				return NULL;
			}

		}
		//strncpyz(pBuf + iBufCount * (PIPE_BUF-1),szBuf2, (int)strlen(szBuf2) + 1);
		strcat(pBuf, szBuf2);
		iBufCount++;
	}


//debug wj
   /* FILE *temp=NULL;
    temp=fopen("/var/wj.txt","w+");
    fwrite(pBuf,sizeof(char),strlen(pBuf),temp);
    fclose(temp);*/
//end

	
	
		pDelete = pBuf;

	if(strlen(pBuf) <= 0 )
	{
		DELETE(pBuf);
		pBuf = NULL;
        close(fdMain);
        close(fdProcess);
		return NULL;
	}
	pBuf++;



	

	pszAuthority = NEW(char, MAX_AUTHORITY_LEVEL_LEN + 1);
	if(pszAuthority == NULL)
	{
        DELETE(pBuf);
        pBuf = NULL;
        close(fdMain);
        close(fdProcess);
		return NULL;
	}
	strncpyz(pszAuthority, pBuf, MAX_AUTHORITY_LEVEL_LEN + 1);
	 

	/*offset, trim the authority level buffer*/
	pBuf = pBuf + MAX_AUTHORITY_LEVEL_LEN;

//#ifdef _DEBUG
//	if((fp = fopen("/scup/www/html/fffff.htm","w")) !=  NULL)
//	{
//		fwrite(pBuf,strlen(pBuf), 1, fp);
//		fclose(fp);
//	}
//#endif
	if(Web_APP_LoadDataPtr(pBuf,
				ppszLocEquipList, 
				ppszEngEquipList, 
				ppszLocTreeNode, 
				ppszEngTreeNode,
				ppLocACUInfo,
				ppEngACUInfo,
				ppszEngEquipAlarmInfo,
				ppszLocEquipAlarmInfo,
				ppszLocGroupEquipInfo,
				ppszEngGroupEquipInfo,
                ppszLoc2EquipList,       //Added by wj for three languages 2006.4.29
                ppszLoc2TreeNode,        //Added by wj for three languages 2006.4.29
                ppszLoc2ACUInfo,         //Added by wj for three languages 2006.4.29
                ppszLoc2EquipAlarmInfo,  //Added by wj for three languages 2006.4.29
                ppszLoc2GroupEquipInfo,   //Added by wj for three languages 2006.4.29
                ppszUserDefPagesNodeLoc,
				ppszUserDefPagesNodeEng,
				ppszUserDefPagesInfoLoc,
				ppszUserDefPagesInfoEng) ==  FALSE)
		return NULL;

	DELETE(pDelete);		/*free pBuf*/
	pDelete = NULL;

	close(fdMain);
	close(fdProcess);
	unlink(szFifoName);

	return pszAuthority;	
}

 

/*==========================================================================*
 * FUNCTION :  
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : 			             DATE: 2004-11-06 10:20
 *==========================================================================*/ 
int DecodeBase64(const char* pSrc,  char* pDst, int iSrcLen)
{
    int			iDstLen;            // Output char len count
    int			iValue;             // for decoding
    int			i;
 
    i = 0;
    iDstLen = 0;
	
	iSrcLen = iSrcLen + 3;
    // get 4 char ,then decode to a int and move to 3 bit
	while (i < iSrcLen)
    {
        if (*pSrc != '\r' && *pSrc!='\n')
        {
            iValue = DeBase64Tab[(int)*pSrc++] << 18;
            iValue +=(int) DeBase64Tab[(int)*pSrc++] << 12;
            *pDst++ = (iValue & 0x00ff0000) >> 16;
            iDstLen++;
 
            if (*pSrc != '=')
            {
                iValue += (int)DeBase64Tab[(int)*pSrc++] << 6;
                *pDst++ = (iValue & 0x0000ff00) >> 8;
                iDstLen++;
 
                if (*pSrc != '=')
                {
                    iValue += (int)DeBase64Tab[(int)*pSrc++];
                    *pDst++ =iValue & 0x000000ff;
                    iDstLen++;
                }
            }
 
			//TRACE("pDst[%s]\n", pDst);
            i += 4;
        }
        else        // Enter ,Jump
        {
            pSrc++;
            i++;
        }
     }
 
    // the output add tail
    *pDst = '\0';
	//TRACE("pDst[%s][%d]\n", pDst, iDstLen);
    return iDstLen;
}



/*==========================================================================*
 * FUNCTION :  
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : 			             DATE: 2004-11-06 10:20
 *==========================================================================*/
static int Web_APP_LoadDataPtr(IN  char *pBuf, 
					   OUT char **ppszLocEquipList, 
					   OUT char **ppszEngEquipList, 
					   OUT char **ppszLocTreeNode, 
					   OUT char **ppszEngTreeNode,
					   OUT char **ppszLocACUInfo, 
					   OUT char **ppszEngACUInfo,
					   OUT char **ppszEngEquipAlarmInfo,
					   OUT char **ppszLocEquipAlarmInfo,
					   OUT char **ppszLocGroupDeviceInfo, 
					   OUT char **ppszEngGroupDeviceInfo,
                       OUT char **ppszLoc2EquipList,       //Added by wj for three languages 2006.4.29
                       OUT char **ppszLoc2TreeNode,        //Added by wj for three languages 2006.4.29
                       OUT char **ppszLoc2ACUInfo,         //Added by wj for three languages 2006.4.29
                       OUT char **ppszLoc2EquipAlarmInfo,  //Added by wj for three languages 2006.4.29
                       OUT char **ppszLoc2GroupDeviceInfo,
					   OUT char **ppszUserDefPagesNodeLoc,
					   OUT char **ppszUserDefPagesNodeEng,
					   OUT char **ppszUserDefPagesInfoLoc,
					   OUT char **ppszUserDefPagesInfoEng)  //Added by wj for three languages 2006.4.29
{
	char		*pLocExchange = NULL, *pEngExchange = NULL, *pLocTreeExchange = NULL;
	char		*pEngACUInfoExchange = NULL, *pLocACUInfoExchange = NULL, *pEngTreeExchange = NULL;
	char		*pLocAlarmInfo = NULL, *pEngAlarmInfo = NULL;
	char		*pLocGroupDevice = NULL, *pEngGroupDevice = NULL;
    char		*pLoc2Exchange = NULL,*pLoc2TreeExchange = NULL,*pLoc2ACUInfoExchange = NULL,*pLoc2AlarmInfo = NULL,*pLoc2GroupDevice = NULL;//Added by wj for three languages 2006.4.29
	char        *pszUserDefPagesNodeLoc = NULL,*pszUserDefPagesNodeEng = NULL;
	char        *pszUserDefPagesInfoLoc = NULL,*pszUserDefPagesInfoEng = NULL;
	/*Local equiplist*/
	//FILE *fp;
	//if((fp = fopen("/scup/www/html/hhhhh.htm","w")) ==  NULL)
	//{
	//	return FALSE;
	//}

    //debug wj

   //FILE *temp=NULL;
   //temp=fopen("/var/wj2.txt","a+");
   //fwrite(pBuf,strlen(pBuf),1,temp);
   //fclose(temp);

    //end


	char		*ptr = pBuf;
	char		*pPosition = strchr(ptr,ENGLISH_START);
	int			iPosition = pPosition - ptr;
	
    /*Loc equiplist*/
	if(iPosition > 0)
	{
		pLocExchange = NEW(char, iPosition + 1);
		if(pLocExchange == NULL)
		{
			return FALSE;
		}
		memset(pLocExchange, 0x0, (size_t)iPosition);
		strncpyz(pLocExchange, ptr, iPosition + 1);
		*ppszLocEquipList = pLocExchange;

        //debug wj
        
        //fwrite(*ppszLocEquipList,iPosition,1,temp);

        //end

		//fwrite(pLocExchange,strlen(pLocExchange), 1, fp);

		ptr = ptr + iPosition ;

	}
	ptr = ptr + 1;

	/*English equiplist*/
	pPosition = strchr(ptr,LOC2_START);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pEngExchange = NEW(char, iPosition + 1);
		if(pEngExchange == NULL)
		{
			return FALSE;
		}
		strncpyz(pEngExchange,ptr,iPosition + 1);
		*ppszEngEquipList = pEngExchange;

        //debug wj
      
       
        //fwrite(*ppszEngEquipList,iPosition,1,temp);
       
        //end

		//fwrite(pEngExchange,strlen(pEngExchange), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;
//////////////////////////////////////////////////////////////////////////

    /*Loc2 equiplist*///Added by wj for three languages 2006.4.29
    pPosition = strchr(ptr,TREE_LOC_START);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pLoc2Exchange = NEW(char, iPosition + 1);
        if(pLoc2Exchange == NULL)
        {
            return FALSE;
        }
        strncpyz(pLoc2Exchange,ptr,iPosition + 1);
        *ppszLoc2EquipList = pLoc2Exchange;

        //debug wj
        
        //fwrite(*ppszLoc2EquipList,iPosition,1,temp);
       
        //end

        //fwrite(pEngExchange,strlen(pEngExchange), 1, fp);

        ptr = ptr + iPosition ;
    }
    ptr = ptr + 1;


//end////////////////////////////////////////////////////////////////////////

	/*Loc tree node*/
	pPosition = strchr(ptr,TREE_ENG_START);
	iPosition = pPosition - ptr;
	//printf("pLocTreeExchange : %d\n",iPosition );
	if(iPosition > 0)
	{
		pLocTreeExchange = NEW(char, iPosition + 1);
		if(pLocTreeExchange == NULL)
		{
			return FALSE;
		}

		strncpyz(pLocTreeExchange,ptr,iPosition + 1);
		*ppszLocTreeNode = pLocTreeExchange;

        //debug wj
        
        //fwrite(*ppszLocTreeNode,iPosition,1,temp);
       
        //end
		//fwrite(pLocTreeExchange,strlen(pLocTreeExchange), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;
	
	/*Eng tree node*/
	pPosition = strchr(ptr,TREE_LOC2_START);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pEngTreeExchange = NEW(char, iPosition + 1);
		if(pEngTreeExchange == NULL)
		{
			return FALSE;
		}

		strncpyz(pEngTreeExchange,ptr,iPosition + 1);
		*ppszEngTreeNode = pEngTreeExchange;
        //debug wj

        //fwrite(*ppszEngTreeNode,iPosition,1,temp);

        //end

		//fwrite(pEngTreeExchange,strlen(pEngTreeExchange), 1, fp);

		ptr = ptr + iPosition  ;
	}
	ptr = ptr + 1;
//////////////////////////////////////////////////////////////////////////
    /*Loc2 tree node*///Added by wj for three languages 2006.4.29
    pPosition = strchr(ptr,ACU_LOC_INFO_START);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pLoc2TreeExchange = NEW(char, iPosition + 1);
        if(pLoc2TreeExchange == NULL)
        {
            return FALSE;
        }

        strncpyz(pLoc2TreeExchange,ptr,iPosition + 1);
        *ppszLoc2TreeNode = pLoc2TreeExchange;

        //debug wj

        //fwrite( *ppszLoc2TreeNode,iPosition,1,temp);

        //end

        //fwrite(pEngTreeExchange,strlen(pEngTreeExchange), 1, fp);

        ptr = ptr + iPosition  ;
    }
    ptr = ptr + 1;

//end////////////////////////////////////////////////////////////////////////

//user define page node////////////////////////////////////////////////////////////////////////
	pPosition = strchr(ptr,TREE_LOC_START);//loc
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszUserDefPagesNodeLoc = NEW(char, iPosition + 1);
		if(pszUserDefPagesNodeLoc == NULL)
		{
			return FALSE;
		}
		strncpyz(pszUserDefPagesNodeLoc,ptr,iPosition + 1);
		*ppszUserDefPagesNodeLoc = pszUserDefPagesNodeLoc;

		//debug wj

		//fwrite(*ppszLoc2EquipList,iPosition,1,temp);

		//end

		//fwrite(pEngExchange,strlen(pEngExchange), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;


	pPosition = strchr(ptr,TREE_LOC_START);//eng
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszUserDefPagesNodeEng = NEW(char, iPosition + 1);
		if(pszUserDefPagesNodeEng == NULL)
		{
			return FALSE;
		}
		strncpyz(pszUserDefPagesNodeEng,ptr,iPosition + 1);
		*ppszUserDefPagesNodeEng = pszUserDefPagesNodeEng;

		//debug wj

		//fwrite(*ppszLoc2EquipList,iPosition,1,temp);

		//end

		//fwrite(pEngExchange,strlen(pEngExchange), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;

	pPosition = strchr(ptr,TREE_LOC_START);//loc
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszUserDefPagesInfoLoc = NEW(char, iPosition + 1);
		if(pszUserDefPagesInfoLoc == NULL)
		{
			return FALSE;
		}
		strncpyz(pszUserDefPagesInfoLoc,ptr,iPosition + 1);
		*ppszUserDefPagesInfoLoc = pszUserDefPagesInfoLoc;

		//debug wj

		//fwrite(*ppszLoc2EquipList,iPosition,1,temp);

		//end

		//fwrite(pEngExchange,strlen(pEngExchange), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;


	pPosition = strchr(ptr,TREE_LOC_START);//eng
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pszUserDefPagesInfoEng = NEW(char, iPosition + 1);
		if(pszUserDefPagesInfoEng == NULL)
		{
			return FALSE;
		}
		strncpyz(pszUserDefPagesInfoEng,ptr,iPosition + 1);
		*ppszUserDefPagesInfoEng = pszUserDefPagesInfoEng;

		//debug wj

		//fwrite(*ppszLoc2EquipList,iPosition,1,temp);

		//end

		//fwrite(pEngExchange,strlen(pEngExchange), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;


//////////////////////////////////////////////////////////////////////////

	/*Loc ACU Info*/
	pPosition = strchr(ptr,ACU_ENG_INFO_START);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pLocACUInfoExchange = NEW(char, iPosition + 1);
		if(pLocACUInfoExchange == NULL)
		{
			return FALSE;
		}
		strncpyz(pLocACUInfoExchange,ptr,iPosition + 1);
		*ppszLocACUInfo = pLocACUInfoExchange;

        //debug wj

        //fwrite( *ppszLocACUInfo,iPosition,1,temp);

        //end

		//fwrite(pLocACUInfoExchange,strlen(pLocACUInfoExchange), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;

	/*Eng ACU Info*/
	pPosition = strchr(ptr,ACU_LOC2_INFO_START);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pEngACUInfoExchange = NEW(char, iPosition + 1);
		if(pEngACUInfoExchange == NULL)
		{
			return FALSE;
		}
		strncpyz(pEngACUInfoExchange,ptr,iPosition + 1);
		*ppszEngACUInfo = pEngACUInfoExchange;

        //debug wj

        //fwrite( *ppszEngACUInfo,iPosition,1,temp);

        //end

		//fwrite(pEngACUInfoExchange,strlen(pEngACUInfoExchange), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;

//////////////////////////////////////////////////////////////////////////
    /*Loc2 ACU Info*///Added by wj for three languages 2006.4.29
    /*Eng ACU Info*/
    pPosition = strchr(ptr,LOC_EQUIPALARM_INFO_START);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pLoc2ACUInfoExchange = NEW(char, iPosition + 1);
        if(pLoc2ACUInfoExchange == NULL)
        {
            return FALSE;
        }
        strncpyz(pLoc2ACUInfoExchange,ptr,iPosition + 1);
        *ppszLoc2ACUInfo = pLoc2ACUInfoExchange;

        //debug wj

        //fwrite(  *ppszLoc2ACUInfo,iPosition,1,temp);

        //end

        //fwrite(pEngACUInfoExchange,strlen(pEngACUInfoExchange), 1, fp);

        ptr = ptr + iPosition ;
    }
    ptr = ptr + 1;

//////////////////////////////////////////////////////////////////////////


	/*Loc equipAlarm Info*/
	pPosition = strchr(ptr,ENG_EQUIPALARM_INFO_START);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pLocAlarmInfo = NEW(char, iPosition + 1);
		if(pLocAlarmInfo == NULL)
		{
			return FALSE;
		}
		strncpyz(pLocAlarmInfo,ptr,iPosition + 1);
		*ppszLocEquipAlarmInfo = pLocAlarmInfo;

        //debug wj

        //fwrite(  *ppszLocEquipAlarmInfo,iPosition,1,temp);

        //end
		
		//fwrite(pLocAlarmInfo,strlen(pLocAlarmInfo), 1, fp);
		
		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;
	
	/*Eng equipAlarm Info*/
	pPosition = strchr(ptr,LOC2_EQUIPALARM_INFO_START);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pEngAlarmInfo = NEW(char, iPosition + 1);
		if(pEngAlarmInfo == NULL)
		{
			return FALSE;
		}
		strncpyz(pEngAlarmInfo,ptr,iPosition + 1);
		*ppszEngEquipAlarmInfo = pEngAlarmInfo;

        //debug wj

        //fwrite(  *ppszEngEquipAlarmInfo,iPosition,1,temp);

        //end

		//fwrite(pEngAlarmInfo,strlen(pEngAlarmInfo), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;

//////////////////////////////////////////////////////////////////////////
    /*Loc2 equipAlarm Info*///Added by wj for three languages 2006.4.29

    pPosition = strchr(ptr,LOC_GROUP_DEVICE_START);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pLoc2AlarmInfo = NEW(char, iPosition + 1);
        if(pLoc2AlarmInfo == NULL)
        {
            return FALSE;
        }
        strncpyz(pLoc2AlarmInfo,ptr,iPosition + 1);
        *ppszLoc2EquipAlarmInfo = pLoc2AlarmInfo;

        //debug wj

        //fwrite( *ppszLoc2EquipAlarmInfo,iPosition,1,temp);

        //end

        //fwrite(pEngAlarmInfo,strlen(pEngAlarmInfo), 1, fp);

        ptr = ptr + iPosition ;
    }
    ptr = ptr + 1;
//end////////////////////////////////////////////////////////////////////////

    pPosition = strchr(ptr,ENG_GROUP_DEVICE_START);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {

	    strncpyz(stRectNum,ptr,iPosition + 1);

	    //debug wj

	    //fwrite( *ppszLocGroupDeviceInfo,iPosition,1,temp);

	    //end

	    //fwrite(pLocGroupDevice,strlen(pLocGroupDevice), 1, fp);

	    ptr = ptr + iPosition ;
    }
    ptr = ptr + 1;


	/*Loc Group*/
	pPosition = strchr(ptr,ENG_GROUP_DEVICE_START);
	iPosition = pPosition - ptr;
	if(iPosition > 0)
	{
		pLocGroupDevice = NEW(char, iPosition + 1);
		if(pLocGroupDevice == NULL)
		{
			return FALSE;
		}
		strncpyz(pLocGroupDevice,ptr,iPosition + 1);
		*ppszLocGroupDeviceInfo = pLocGroupDevice;

        //debug wj

        //fwrite( *ppszLocGroupDeviceInfo,iPosition,1,temp);

        //end
	
		//fwrite(pLocGroupDevice,strlen(pLocGroupDevice), 1, fp);

		ptr = ptr + iPosition ;
	}
	ptr = ptr + 1;


	/*Eng Group*/
    pPosition = strchr(ptr,LOC2_GROUP_DEVICE_START);
    iPosition = pPosition - ptr;
    if(iPosition > 0)
    {
        pEngGroupDevice = NEW(char, iPosition + 1);
        if(pEngGroupDevice == NULL)
        {
            return FALSE;
        }
        strncpyz(pEngGroupDevice,ptr,iPosition + 1);
        *ppszEngGroupDeviceInfo = pEngGroupDevice;

        //debug wj

        //fwrite( *ppszLocGroupDeviceInfo,iPosition,1,temp);

        //end

        //fwrite( *ppszEngGroupDeviceInfo,strlen(pLocGroupDevice), 1, fp);

        ptr = ptr + iPosition ;
    }
    ptr = ptr + 1;

//////////////////////////////////////////////////////////////////////////
    /*Loc2 Group*///Added by wj for three languages 2006.4.29
	if(strlen(ptr) > 0)
	{
		pLoc2GroupDevice = NEW(char, (int)strlen(ptr) + 1);
		if(pLoc2GroupDevice == NULL)
		{
			return FALSE;
		}
		strncpyz(pLoc2GroupDevice,ptr,(int)strlen(ptr) + 1);
		*ppszLoc2GroupDeviceInfo = pLoc2GroupDevice;

        //debug wj

        //fwrite( *ppszLoc2GroupDeviceInfo,sizeof(char),strlen(ptr),temp);

        //end

		//fwrite(pEngGroupDevice,strlen(pEngGroupDevice), 1, fp);
	}


	
	//fclose(fp);
	return TRUE;

    //debug wj
   // fclose(temp);
    //end

	
}

//static void MakeModiTime( void )
//{
//    time_t  ltime;
//    struct tm *gmt; 
//
//    time( &ltime );
//    gmt = gmtime( &ltime );
//    strftime( strModiTime, 
//        sizeof(strModiTime), 
//        "%a, %d %b %Y %H:%M:%S GMT", //Fri, 10 May 2002 07:27:38 GMT
//        gmt );
//}

static BOOL getFileListNumber(void)
{
#define VAR_DIR				"/var"
#define SESS_FILE_HEAD		"sess_"
	
	DIR			*dp; 
	struct direct *dir;
	int			iDirNum = 0;
	char		szFileHead[6];

	if ((dp = opendir(VAR_DIR))== NULL) 
	{ 
		return -1; 
	} 
	while ((dir = readdir(dp)) != NULL) 
	{ 
		if(dir->d_ino == 0) 
			continue; 

		strncpyz(szFileHead,dir->d_name,sizeof(szFileHead));

		if(strcmp(szFileHead, SESS_FILE_HEAD) == 0)
		{
			iDirNum++;
		}
	}
	closedir(dp);
	
	if(iDirNum >= MAX_CONNECT_NUMBER )
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}

}




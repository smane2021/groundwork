/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_suicide_timer.h
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 18:36
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __CGI_SUICIDE_TIME_H
#define __CGI_SUICIDE_TIME_H

#ifndef IN
#define IN
#endif

#ifndef UNUSED
#define UNUSED(x)	(void)(x)
#endif

#define CGI_SUICIDE_TIMER_RESOLUTION	5	// sec.

typedef int (*SUICIDE_CLEAR_PROC)(void *lpParam); 

/*==========================================================================*
 * FUNCTION : SuicideTimer_Init
 * PURPOSE  : Init the timer for process suicide after given seconds.
 *            The callback can be called before suicide.
 * CALLS    : exit.
 * CALLED BY: 
 * ARGUMENTS: IN int                 nSuicideWaitSeconds : wait seconds.
 *            IN SUICIDE_CLEAR_PROC  pfnClearProc        : clean proc
 *                 if pfnClearProc not NULL, the process will exit with
 *                 the return value of the pfnClearProc.
 *            IN void                *lpClearParam       : arg for clean proc
 * RETURN   : int : 0: for OK.
 * COMMENTS : This function ONLY be used ONCE in a process, because of using
 *            static variables and one timer.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 16:18
 *==========================================================================*/
int SuicideTimer_Init(IN int				nSuicideWaitSeconds, 
					  IN SUICIDE_CLEAR_PROC	pfnClearProc,
					  IN void				*lpClearParam);


/*==========================================================================*
 * FUNCTION : SuicideTimer_Reset
 * PURPOSE  : reset the timer to 0. 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : CGI shall call SuicideTimer_Reset periodly if the CGI is doing
 *            a thing need the time than the nSuicideWaitSeconds.
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 18:14
 *==========================================================================*/
void SuicideTimer_Reset(void);


/*==========================================================================*
 * FUNCTION : SuicideTimer_Destroy
 * PURPOSE  : destroy the timer.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua(Frank)         DATE: 2005-03-12 21:04
 *==========================================================================*/
void SuicideTimer_Destroy(void);

#endif


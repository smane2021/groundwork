/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi-realtime.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-09-28 11:38
 *  VERSION  : V1.00
 *  PURPOSE  : 
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#include "cgi_realtime.h"



//
//#define EQUIP_SAMPLE_SIGNALS			"equip_sample_signals"
//#define EQUIP_CONTROL_SIGNALS			"equip_control_signals"
//#define EQUIP_SETTING_SIGNALS			"equip_setting_signals"
//#define	EQUIP_ALARM_SIGNALS				"equip_Alarm_signals"
//#define EQUIP_RALARM_SIGNALS			"equip_ralarm_signals"
//#define EQUIP_STATUS_SIGNALS			"equip_status_signals"

#define CONTROL_RESULT					"CONTROL_RESULT"

/*defined for apache*/
#ifndef ENV_ACU_APACHE_DIR
#define ENV_ACU_APACHE_DIR				"ACU_APACHE_DIR"
#endif


                 
/*define the html path*/
#define HTML_SAMPLE_FILE				"/data_sampler.htm"				 
#define HTML_DEVICE_FILE				"/j76_device_def.htm"
//float	*pValue= NULL;

/*get the cgi parameter(equipId,sessionId)*/
/*
SAMPLEDATA			*pSampleValue;
SAMPLEDATA			*pControlValue;
SAMPLEDATA			*pSettingValue;
ALARMDATA			*pRAlarmValue;
float					*pStatus;

SAMPLESIGSTRUCT		*pSampleSigStruct;
CONTROLSIGSTRUCT	*pControlSigStrcut;
SETTINGSIGSTRUCT	*pSettingSigStruct;
ALARMSIGSTRUCT		*pAlarmSigStruct;
char				pStatusName[4][32];
*/
char		*pSampleStrucBuf = NULL, *pControlStrucBuf= NULL,*pRAlarmStrucBuf = NULL;
char		*pSettingStrucBuf= NULL, *pAlarmStrucBuf= NULL, *pStatusStrucBuf= NULL;
char		*pSampleDataBuf= NULL, *pControlDataBuf= NULL;
char		*pSettingDataBuf= NULL, *pRAlarmDatacBuf= NULL,*pStatusDataBuf= NULL;

#define MAX_DATA_LINE_SIZE   sizeof( "\t999999.99, 99999, \n" )

/*signal num iLen in buffer*/
#define MAX_SIGNUAL_NUM_BUFFER_LEN		4

/*define the number of signal*/
int		N_SAMPLE_NUM		= 0 ;     
int		N_CONTROL_NUM		= 0 ;
int		N_SETTING_NUM		= 0 ;
int		N_ALARM_NUM			= 0 ;
int		N_RALARM_NUM		= 0 ;
int		N_STATUS_NUM		= 4 ;


/*page modify time*/
char	strModiTime[50];

 static BOOL Web_REL_MakeSampleSignalSection(IN OUT char **ppBuf );
 static BOOL Web_REL_MakeControlSignalSection( IN OUT char **ppBuf);
 static BOOL Web_REL_MakeParaSignalSection(IN OUT char **ppBuf);
 static BOOL Web_REL_MakeRealtimeAlarmSection(IN OUT char **ppBuf);
 static BOOL Web_REL_MakeStatusSection( IN OUT char **ppBuf);

/* */
static int Web_REL_getCGIParam(OUT long *lCmdBuf );
static int Web_REL_sendControlCommand(IN int iEquipID, IN int iNeed,IN int iLanguage );
 //static char *StuffHead(  char *pszHead, int iLenContent );
 //static void StuffHead(  char *pszHead, int iLenContent );
 //void PostPage(IN char * pbuf);
static  int Web_REL_LoadDataPtr(IN char *pBuf,IN int iNeed);

/*Make*/
 static int Web_REL_MakeSampleStrucSection(IN OUT char **ppBuf);
 static int Web_REL_MakeControlStrucSection(IN OUT char **ppBuf);
 static int Web_REL_MakeSettingStrucSection(IN OUT char **ppBuf);
 static int Web_REL_MakeAlarmStrucSection(IN OUT char **ppBuf);
 
 static int Web_REL_MakeStatusStrucSection(IN OUT char **ppBuf);
 static int Web_REL_MakeRAlarmStrucSection(IN OUT char **ppBuf);

char *szRAlarmGetTime = NULL;
int		iRectifierRefresh = 0; //refresh rectifier
char	szRectifier[8];
char	szACD[8];
char	szDCD[8];
char	szBATT[8];
char	szAutoCFGFlage[5];
char    szSlaveModeFlage[5];
char	szSMDUCFGStatus[5];
char	szConverterNum[5];
char	szSessionID[64];
/*==========================================================================*
 * FUNCTION :   Web_REL_MakeSampleSignalSection
 * PURPOSE  :	To Make sample signal section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-08 11:20
 *==========================================================================*/
 static BOOL Web_REL_MakeSampleSignalSection(IN OUT char **ppBuf)
 {
	if(pSampleDataBuf != NULL )
	{
		ReplaceString(ppBuf, MAKE_VAR_FIELD(EQUIP_SAMPLE_SIGNALS), pSampleDataBuf );
		DELETE(pSampleDataBuf);
		pSampleDataBuf = NULL;
		
		return TRUE;
	}
	
	return FALSE;
 }
 /*==========================================================================*
 * FUNCTION :   Web_REL_MakeControlSignalSection
 * PURPOSE  :   To Make control signal section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-08 11:20
 *==========================================================================*/
static  BOOL Web_REL_MakeControlSignalSection( IN OUT char **ppBuf)
 {
	 if(pControlDataBuf != NULL)
	 {
		ReplaceString(ppBuf, MAKE_VAR_FIELD(EQUIP_CONTROL_SIGNALS), pControlDataBuf );
		DELETE(pControlDataBuf);
		pControlDataBuf = NULL;
		return TRUE;
	 }

	 return FALSE;

 }


 /*==========================================================================*
 * FUNCTION :   Web_REL_MakeParaSignalSection
 * PURPOSE  :	To Make Setting signal section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-08 11:20
 *==========================================================================*/
static  BOOL Web_REL_MakeParaSignalSection(IN OUT char **ppBuf)
 {
	if(pSettingDataBuf != NULL)
	{
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_SETTING_SIGNALS), pSettingDataBuf );
		DELETE(pSettingDataBuf);
		pSettingDataBuf = NULL;
		return TRUE;
	}

	return FALSE;
 }


 /*==========================================================================*
 * FUNCTION :   Web_REL_MakeRealtimeAlarmSection
 * PURPOSE  :	To Make Realtime Alarm signal section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-08 11:20
 *==========================================================================*/
static  int Web_REL_MakeRealtimeAlarmSection(IN OUT char **ppBuf)
 {
	 if(pRAlarmDatacBuf != NULL)
	 {
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_RALARM_SIGNALS), pRAlarmDatacBuf );
		DELETE(pRAlarmDatacBuf);
		pRAlarmDatacBuf = NULL;
		return TRUE;
	 }
	 return FALSE;
 }

 
/*==========================================================================*
 * FUNCTION :  Web_REL_MakeStatusSection
 * PURPOSE  :  To Make status section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
static int Web_REL_MakeStatusSection( IN OUT char **ppBuf)
{
	if(pStatusDataBuf != NULL)
	{
		//FILE *fp;
		//if((fp = fopen("/scup/www/html/status.htm","wb")) !=   NULL )
		//{
		//	fwrite(pStatusDataBuf, strlen(pStatusDataBuf), 1, fp);
		//	fclose(fp);
		//}
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_STATUS_SIGNALS), pStatusDataBuf );
		DELETE(pStatusDataBuf);
		pStatusDataBuf = NULL;
		return TRUE;
	}
	
	return FALSE;
}


/*==========================================================================*
 * FUNCTION :  Web_REL_MakeSampleStrucSection
 * PURPOSE  :  To Make sample signal structure section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/

static int Web_REL_MakeSampleStrucSection(IN OUT char **ppBuf)
{
	if(pSampleStrucBuf != NULL)
	{
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_SAMPLE_SIGNALS), pSampleStrucBuf );
		DELETE(pSampleStrucBuf);
		pSampleStrucBuf = NULL;
		return TRUE;
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION :  Web_REL_MakeControlStrucSection
 * PURPOSE  :  To Make control signal structure section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/

static int Web_REL_MakeControlStrucSection(IN OUT char **ppBuf)
{
	if(pControlStrucBuf != NULL)
	{
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_CONTROL_SIGNALS), pControlStrucBuf );
		DELETE(pControlStrucBuf);
		pControlStrucBuf = NULL;
		return TRUE;
	}
	
	return FALSE;
}


/*==========================================================================*
 * FUNCTION :  Web_REL_MakeSettingStrucSection
 * PURPOSE  :  To Make setting signal structure section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
static int Web_REL_MakeSettingStrucSection(IN OUT char **ppBuf)
{
	if(pSettingStrucBuf != NULL)
	{
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_SETTING_SIGNALS), pSettingStrucBuf );
		DELETE(pSettingStrucBuf);
		pSettingStrucBuf = NULL;
		return TRUE;
	}
	
	return FALSE;
}


 /*==========================================================================*
 * FUNCTION :  Web_REL_MakeAlarmStrucSection
 * PURPOSE  :  To Make setting signal structure section in ppBuf
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN OUT char **ppBuf: html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
static int Web_REL_MakeAlarmStrucSection(IN OUT char **ppBuf)
{
	if(pAlarmStrucBuf != NULL)
	{
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_ALARM_SIGNALS), pAlarmStrucBuf );
		DELETE(pAlarmStrucBuf);
		pAlarmStrucBuf = NULL;
		return TRUE;
	}
	
	return FALSE;
}
/*==========================================================================*
 * FUNCTION :  Web_REL_MakeStatusStrucSection
 * PURPOSE  :  Make status bar signal structure
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN OUT char **ppBuf:html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-28 9:20
 *==========================================================================*/
 static int Web_REL_MakeStatusStrucSection(IN OUT char **ppBuf)
 {
	if(pStatusStrucBuf != NULL)
	{
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_STATUS_SIGNALS), pStatusStrucBuf );
		DELETE(pStatusStrucBuf);
		pStatusStrucBuf = NULL;
		return TRUE;
	}

	return FALSE;
 }
/*==========================================================================*
 * FUNCTION :  Web_REL_MakeRAlarmStrucSection
 * PURPOSE  :  Make realtime signal structure
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  IN OUT char **ppBuf:html file buffer
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-28 9:20
 *==========================================================================*/
 static int Web_REL_MakeRAlarmStrucSection(IN OUT char **ppBuf)
 {
	 if(pRAlarmStrucBuf != NULL)
	 {
		ReplaceString( ppBuf, MAKE_VAR_FIELD(EQUIP_RALARM_SIGNALS), pRAlarmStrucBuf );
		DELETE(pRAlarmStrucBuf);
		pRAlarmStrucBuf = NULL;
		return TRUE;
	 }

	 return FALSE;
 }
/*==========================================================================*
 * FUNCTION :  Web_REL_getCGIParam
 * PURPOSE  :  Get parameter from CGI
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  OUT long *lCmdBuf: parameter from CGI
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-10-28 9:20
 *==========================================================================*/
static int Web_REL_getCGIParam(OUT long *lCmdBuf )
{
    char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val;
	
    form_method = getRequestMethod();
    if(form_method == POST) 
	{
        getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		return FALSE;
	}
	
	 
    lCmdBuf[0] = REALTIME_DATA;
    val = getValue( getvars, postvars, EQUIP_ID );
	if( val != NULL )
    {
		lCmdBuf[1]  = atoi(val);
		val = getValue( getvars, postvars, SESSION_ID );
        if( val != NULL ) // sessionId
        {
            strncpyz(szSessionID, val,sizeof(szSessionID));  
                
        }

		val = getValue(getvars,postvars,LANGUAGE_TYPE);
		if(val != NULL)
		{
			lCmdBuf[3] = atoi(val);
		}

		val = getValue(getvars,postvars,NEED_RETURN_STRUCTURE);
		if(val != NULL)
		{
			lCmdBuf[4] = atoi(val);
		}
       
    }

	cleanUp( getvars, postvars);

    return TRUE;  
}


/*==========================================================================*
 * FUNCTION :  Web_REL_sendControlCommand
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN long equipId:EquipID
				IN int iNeed: Whether need to return equipment structure
				IN int iLanguage:language type
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
static int Web_REL_sendControlCommand(IN int iEquipID, IN int iNeed,IN int iLanguage )
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	buf[1024],buf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*pBuf;
	char    *pTemp;
	int		iBufCount = 0;  //buf count ,used in read FIFO buf 

	memset(buf2, 0x0, PIPE_BUF);
	memset(buf, 0x0, 1024);
	memset(fifoname, 0x0, FIFO_NAME_LEN);
	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY))<0)
	{
		//AppLogOut(CGI_APP_LOG_REALTIME_NAME,APP_LOG_INFO,"Error : Fail to open FIFO %s", MAIN_FIFO_NAME);
		return FALSE;
	}
	 
	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		//AppLogOut(CGI_APP_LOG_REALTIME_NAME,APP_LOG_INFO,"Fail to make FIFO %s",fifoname);
		return FALSE;
	}
	
	/*start buffer with pid and a blank*/
	//time(&tp);
	iLen = sprintf(buf,"%10ld%2d%8d%2d%2d",(long)getpid(), REALTIME_DATA,iEquipID,iNeed,iLanguage);
	buf[iLen] = '\0';

//#ifdef _DEBUG
//	FILE *fp;
//	if((fp = fopen("/var/f.htm","wb")) !=   NULL )
//		{
//			fwrite(buf, strlen(buf), 1, fp);
//			//fwrite(pLocEquipHtml, sizeof(char), strlen(pLocEquipHtml), fp);
//			//fprintf(fp,"%s",pLocEquipHtml);
//			fclose(fp);
//		}
//#endif
	

	if((write(fd,buf,strlen(buf) + 1)) < 0)
	//if((write(fd, buf, PIPE_BUF))<0)
	{
		//AppLogOut(CGI_APP_LOG_REALTIME_NAME,APP_LOG_INFO,"Error : Fail to write FIFO [%s]", MAIN_FIFO_NAME);
		close(fd);
		return FALSE;
	}


	if((fd2 = open(fifoname,O_RDONLY)) < 0 )
	{
		//AppLogOut(CGI_APP_LOG_REALTIME_NAME,APP_LOG_INFO,"Error : Fail to write FIFO [%s]",fifoname);
        close(fd2);
		return FALSE;
	}

	pBuf = NEW(char,2*PIPE_BUF);
	if(pBuf == NULL)
	{
        close(fd);
        close(fd2);
        return FALSE;
	}
	
	
	while((iLen = read(fd2, buf2, PIPE_BUF - 1)) > 0)
	{
		//AppLogOut(CGI_APP_LOG_REALTIME_NAME, APP_LOG_INFO, "Info : get buffer :%s\n" ,buf2);
//#ifdef _DEBUG
	/*if(iNeed == FALSE)
	{
		TRACE("iLen [ %d]\n", iLen);

	}
		*/
//#endif	

		SuicideTimer_Reset();

		if(iBufCount >= 2)
		{
			pBuf = RENEW(char, pBuf, (iBufCount + 1) * PIPE_BUF);
		}
		strcat(pBuf, buf2);
		memset(buf2, 0x0, PIPE_BUF);
		iBufCount++;
	}
	//AppLogOut(CGI_APP_LOG_REALTIME_NAME,APP_LOG_INFO,"Info : get all buffer %s", pBuf);
#ifdef _DEBUG
		/*FILE *fp3;
		if((fp3 = fopen("/var/sa3.htm","w")) !=  NULL)
		{
			fwrite(pBuf,strlen(pBuf), 1, fp3);
			fclose(fp3);
		}*/
#endif
    //pTemp = pBuf + 1;
	Web_REL_LoadDataPtr(pBuf,iNeed);

	close(fd2);
	close(fd);
	unlink(fifoname);
	DELETE(pBuf);

	return TRUE;
	
}


/*==========================================================================*
 * FUNCTION :  StuffHead
 * PURPOSE  :  print the html file head
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	char *pszHead:
				int iLenContent
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-29 11:20
 *==========================================================================*/
//static void StuffHead(  char *pszHead, int iLenContent )
//{
//    sprintf( pszHead,
//        "HTTP/1.0 200 OK\r\n"   // \x0d\x0a
//        "Server: BOA/1.0\r\n"
//        "Date: %s\r\n"
//        "Content-type: text/html\r\n"
//        "Content-length: %d\r\n"
//        "Last-Modified: %s\r\n"
//        "\r\n", 
//        strModiTime,
//        iLenContent,
//        strModiTime );
//
//    //return pszHead;
//}


 
 /*==========================================================================*
 * FUNCTION :  Web_REL_LoadDataPtr
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:	IN char *pBuf: output buffer
				IN BOOL iNeed:To judge whether need to return equipment structure
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-29 11:20
 *==========================================================================*/


static  int Web_REL_LoadDataPtr(IN char *pBuf,IN int iNeed)
{
	char			*ptr;
	char			*pPosition;
	int				iPosition;
	char			szExchange[8];

	/*offset*/
	ptr = pBuf ;
 
	strncpyz(szExchange,ptr,MAX_SIGNUAL_NUM_BUFFER_LEN + 1);
	N_SAMPLE_NUM = atoi(szExchange); 
	ptr = ptr + MAX_SIGNUAL_NUM_BUFFER_LEN;
	
	//get control signal number
	strncpyz(szExchange,ptr,MAX_SIGNUAL_NUM_BUFFER_LEN + 1 );
	N_CONTROL_NUM = atoi(szExchange);
	ptr = ptr + MAX_SIGNUAL_NUM_BUFFER_LEN;
	
	//get setting signal number
	strncpyz(szExchange,ptr,MAX_SIGNUAL_NUM_BUFFER_LEN + 1);
 	N_SETTING_NUM = atoi(szExchange);
	ptr = ptr + MAX_SIGNUAL_NUM_BUFFER_LEN;
	
	strncpyz(szExchange,ptr,MAX_SIGNUAL_NUM_BUFFER_LEN + 1);
 	N_ALARM_NUM = atoi(szExchange);
	ptr = ptr + MAX_SIGNUAL_NUM_BUFFER_LEN;

	//get realtime alarm signal number
	strncpyz(szExchange,ptr,MAX_SIGNUAL_NUM_BUFFER_LEN + 1);
 	N_RALARM_NUM = atoi(szExchange);
	ptr = ptr + MAX_SIGNUAL_NUM_BUFFER_LEN;
 
	if(iNeed == NEED_RETURN_SIGNAL_STRUCTURE)
	{
        


		pPosition = strchr(ptr,END_OF_SAMPLESIG);
		iPosition = pPosition - ptr;

		/*Sample struc*/
		if(iPosition > 1)
		{
			pSampleStrucBuf = NEW(char, iPosition + 1);
			if(pSampleStrucBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pSampleStrucBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}

        
		ptr = ptr + 1;
		
		/*Control struc*/
		pPosition = strchr(ptr,END_OF_CONTROLSIG);
		iPosition = pPosition - ptr;
		if(iPosition > 1)
		{
			pControlStrucBuf = NEW(char, iPosition + 1);
			if(pControlStrucBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pControlStrucBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;

		/*Setting struc*/
		pPosition = strchr(ptr,END_OF_SETTINGSIG);
		iPosition = pPosition - ptr;
		if(iPosition > 1)
		{
			pSettingStrucBuf = NEW(char, iPosition + 1);
			if(pSettingStrucBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pSettingStrucBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;

		/*Alarm struc*/
		pPosition = strchr(ptr,END_OF_ALARMSIG);
		iPosition = pPosition - ptr;
		if(iPosition > 1)
		{
			pAlarmStrucBuf = NEW(char, iPosition + 1);
			if(pAlarmStrucBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pAlarmStrucBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;

		/*Status struc*/
		pPosition = strchr(ptr,END_OF_STATUSSIG);
		iPosition = pPosition - ptr;
		if(iPosition > 1)
		{
			pStatusStrucBuf = NEW(char, iPosition + 1);
			if(pStatusStrucBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pStatusStrucBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;

		/*real alarm struc*/
		pPosition = strchr(ptr,END_OF_RALARM);
		iPosition = pPosition - ptr;
		if(iPosition > 1)
		{
			pRAlarmStrucBuf = NEW(char, iPosition + 1);
			if(pRAlarmStrucBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pRAlarmStrucBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;

		/* alarm get time*/
		pPosition = strchr(ptr,END_OF_RALARM);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			szRAlarmGetTime = NEW(char, iPosition + 1);
			if(szRAlarmGetTime == NULL)
			{
				return FALSE;
			}
			strncpyz(szRAlarmGetTime, ptr, iPosition + 1);
			//ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

        /* need refresh rectifier*/
        pPosition = strchr(ptr,59);
        iPosition = pPosition - ptr;
        if(iPosition >= 1)
        {
            strncpyz(szRectifier, ptr, iPosition + 1);//szRectifier used to judge whether need to refresh the tree_view
            ptr = ptr + iPosition;

        }
        ptr = ptr + 1;

        /* need refresh ACD*/
        pPosition = strchr(ptr,59);
        iPosition = pPosition - ptr;
        if(iPosition >= 1)
        {
            strncpyz(szACD, ptr, iPosition + 1);//szRectifier used to judge whether need to refresh the tree_view
            ptr = ptr + iPosition;

        }
        ptr = ptr + 1;

        /* need refresh DCD*/
        pPosition = strchr(ptr,59);
        iPosition = pPosition - ptr;
        if(iPosition >= 1)
        {
            strncpyz(szDCD, ptr, iPosition + 1);//szRectifier used to judge whether need to refresh the tree_view
            ptr = ptr + iPosition;

        }
        ptr = ptr + 1;
		/* Auto Config*/
		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			strncpyz(szAutoCFGFlage, ptr, iPosition + 1);//szRectifier used to judge whether need to logout web
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

		if(iPosition >= 1)
		{
			strncpyz(szSlaveModeFlage, ptr, iPosition + 1);//szRectifier used to judge whether need to logout web
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

		/*SMDUCFG Status*/
		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			strncpyz(szSMDUCFGStatus, ptr, iPosition + 1);//szRectifier used to judge whether need to logout web
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

		/*Converter Number*/
		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			strncpyz(szConverterNum, ptr, iPosition + 1);//szRectifier used to judge whether need to logout web
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

        /* need refresh BATT*/
        pPosition = strchr(ptr,59);
        iPosition = pPosition - ptr;
        if(iPosition >= 1)
        {
            strncpyz(szBATT, ptr, iPosition + 1);//szRectifier used to judge whether need to refresh the tree_view
            //ptr = ptr + iPosition;

        }


		return TRUE;

	}
	else
	{
        
		/*Sample data*/
		pPosition = strchr(ptr,END_OF_SAMPLESIG);
		iPosition = pPosition - ptr;
		if(iPosition > 1)
		{
			//printf("pSampleDataBuf : %s", ptr);
			pSampleDataBuf = NEW(char, iPosition + 1);
			if(pSampleDataBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pSampleDataBuf, ptr, iPosition + 1);
			//printf("pSampleDataBuf : %s",pSampleDataBuf);
			ptr = ptr + iPosition;
		}

        
		ptr = ptr + 1;
		//printf("pSampleDataBuf : %s", ptr);

		/*Control data*/
		pPosition = strchr(ptr,END_OF_CONTROLSIG);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			pControlDataBuf = NEW(char, iPosition + 1);
			if(pControlDataBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pControlDataBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;


		/*Setting data*/
		pPosition = strchr(ptr,END_OF_SETTINGSIG);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			pSettingDataBuf = NEW(char, iPosition + 1);
			if(pSettingDataBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pSettingDataBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;

		/*RAlarm data*/
		pPosition = strchr(ptr,END_OF_RALARMSIG);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			pRAlarmDatacBuf = NEW(char, iPosition + 1);
			if(pRAlarmDatacBuf == NULL)
			{
				return FALSE;
			}
			
			strncpyz(pRAlarmDatacBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;


		/*Status data*/
		//printf("ptr :%s",ptr);
		pPosition = strchr(ptr,END_OF_STATUSSIG);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			pStatusDataBuf = NEW(char, iPosition + 1);
			if(pStatusDataBuf == NULL)
			{
				return FALSE;
			}
		
			strncpyz(pStatusDataBuf, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}

		ptr = ptr + 1;

		/* alarm get time*/
		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			szRAlarmGetTime = NEW(char, iPosition + 1);
			if(szRAlarmGetTime == NULL)
			{
				return FALSE;
			}
			strncpyz(szRAlarmGetTime, ptr, iPosition + 1);
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

		/* need refresh rectifier*/
		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			strncpyz(szRectifier, ptr, iPosition + 1);//szRectifier used to judge whether need to refresh the tree_view
			ptr = ptr + iPosition;

		}
        ptr = ptr + 1;

        /* need refresh ACD*/
        pPosition = strchr(ptr,59);
        iPosition = pPosition - ptr;
        if(iPosition >= 1)
        {
            strncpyz(szACD, ptr, iPosition + 1);//szRectifier used to judge whether need to refresh the tree_view
            ptr = ptr + iPosition;

        }
        ptr = ptr + 1;

        /* need refresh DCD*/
        pPosition = strchr(ptr,59);
        iPosition = pPosition - ptr;
        if(iPosition >= 1)
        {
            strncpyz(szDCD, ptr, iPosition + 1);//szRectifier used to judge whether need to refresh the tree_view
            ptr = ptr + iPosition;

        }
        ptr = ptr + 1;

		/* Auto Config*/
		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			strncpyz(szAutoCFGFlage, ptr, iPosition + 1);//szRectifier used to judge whether need to logout web
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			strncpyz(szSlaveModeFlage, ptr, iPosition + 1);//szRectifier used to judge whether need to logout web
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

		/*SMDUCFG Status*/
		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			strncpyz(szSMDUCFGStatus, ptr, iPosition + 1);//szRectifier used to judge whether need to logout web
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

		/*Converter Number*/
		pPosition = strchr(ptr,59);
		iPosition = pPosition - ptr;
		if(iPosition >= 1)
		{
			strncpyz(szConverterNum, ptr, iPosition + 1);//szRectifier used to judge whether need to logout web
			ptr = ptr + iPosition;

		}
		ptr = ptr + 1;

        /* need refresh BATT*/
        pPosition = strchr(ptr,59);
        iPosition = pPosition - ptr;
        if(iPosition >= 1)
        {
            strncpyz(szBATT, ptr, iPosition + 1);//szRectifier used to judge whether need to refresh the tree_view
            //ptr = ptr + iPosition;

        }
        //ptr = ptr + 1;

		return TRUE;
	}	 
}





/*==========================================================================*
 * FUNCTION :  main
 * PURPOSE  :  
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:  
 * RETURN   :  
 * COMMENTS : 
 * CREATOR  : Yang Guoxin               DATE: 2004-09-28 21:20
 *==========================================================================*/
int main(void)
{

	char	*pHtml = NULL,*pStrucHtml = NULL;
    //char	szHead[1024];
	int		iNeedReturnSignStruc = 0, iLanguage = 0;
	long	lCmdBuf[5];
	//char	szTempbuf[20];
	int		i;
	int		iLen = 0;
	char	szHtmlPath[64];
	//char	szEquipName[32];
	int		iEquipID = 0;
	printf("Content-type:text/html\n\n");

	// Register suicide timer. maofuhua, 2005-3-12
	// you can call SuicideTimer_Reset() to avoid timeout when the CGI is running.
	// or you need increase the wait time.
	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");


	for(i = 0; i < 5; i++)
	{
		lCmdBuf[i] = 0;
	}

	/*get equipId and sessionId,	then manage sessionId ,the equipId will be transmit if there are no overtime*/
	/*get parameter*/
	/*lCmdBuf[0] :realtimedata ,lCmdBuf[1]: device ID lCmdBuf[2]sessionId [3]need to return signalStruc*/
	
 	if(Web_REL_getCGIParam(lCmdBuf) == FALSE)
 	{
		TRACE("HELLO!");
		return FALSE;
 	}
	/*deal with overtime*/
	
	

	//printf("%ld\n", lCmdBuf[2]);
//#else
	//start_session(szTempbuf);
//#endif	

	iEquipID = lCmdBuf[1];
 

	/*If the Signal Struc need resend*/
	if(lCmdBuf[4] > 0)
	{
		iNeedReturnSignStruc = 1;
	}
	else
	{
		iNeedReturnSignStruc = 0;
	}


	if(lCmdBuf[3] > 0)
	{
        if(lCmdBuf[3] == 1)
        {
            iLanguage =	LOCAL_LANGUAGE_NAME;
        }
        else
        {
            iLanguage =	LOCAL2_LANGUAGE_NAME;
        }
		
	}
	else
	{
		iLanguage = ENGLISH_LANGUAGE_NAME ;
	}

	/*Load device_def.js*/
	if(iLanguage ==	LOCAL_LANGUAGE_NAME)
	{
		iLen = sprintf(szHtmlPath, "%s%s", HTML_SAVE_REAL_PATH, HTML_SAMPLE_FILE);
	}
	else if(iLanguage == ENGLISH_LANGUAGE_NAME)
	{
		iLen = sprintf(szHtmlPath, "%s%s", HTML_SAVE_ENG_PATH, HTML_SAMPLE_FILE);
	}
	else
    {
        iLen = sprintf(szHtmlPath, "%s%s", HTML_SAVE_REAL2_PATH, HTML_SAMPLE_FILE);
    }
	szHtmlPath[iLen] = '\0';
	 
    if(LoadHtmlFile(szHtmlPath, &pHtml ) > 0 )
    {
	

		 
		/*send equipId to ACU and get realtimedata*/
#ifdef _DEBUG
		//AppLogOut(CGI_APP_LOG_REALTIME_NAME,APP_LOG_WARNING,"Sending realtime command");
#endif
		if(Web_REL_sendControlCommand(iEquipID,iNeedReturnSignStruc,iLanguage) == FALSE)
		{
			/*if fail to send control command,send the old page to the client*/
			//AppLogOut(CGI_APP_LOG_REALTIME_NAME,APP_LOG_WARNING,"Error : Fail to send realtime command");
			PostPage(pHtml);
			DELETE(pHtml);
			pHtml = NULL;

			return FALSE;
		}
	   
		
		time_t now = time(NULL);
		srand((unsigned int)now);
		//MakeModiTime(strModiTime,sizeof(strModiTime));
		/*ReplaceString( &pHtml, MAKE_VAR_FIELD(UPS_AGENT_TIME), szHead );*/
		/*return signal buffer*/
	 
		if(iNeedReturnSignStruc)
		{
			//sprintf(szTempbuf,"%ld",lCmdBuf[2]);
			start_session(szSessionID);
			
			if(iLanguage ==	LOCAL_LANGUAGE_NAME)
			{
				iLen = sprintf(szHtmlPath, "%s%s", HTML_SAVE_REAL_PATH, HTML_DEVICE_FILE);
			}
			else if(iLanguage ==ENGLISH_LANGUAGE_NAME)
			{
				iLen = sprintf(szHtmlPath, "%s%s", HTML_SAVE_ENG_PATH, HTML_DEVICE_FILE);
			}
            else
            {
                iLen = sprintf(szHtmlPath, "%s%s", HTML_SAVE_REAL2_PATH, HTML_DEVICE_FILE);
            }

			if(LoadHtmlFile(szHtmlPath,&pStrucHtml) > 0)
			{
				/*free */
		
				Web_REL_MakeSampleStrucSection(&pStrucHtml);
				Web_REL_MakeControlStrucSection(&pStrucHtml);
				Web_REL_MakeSettingStrucSection(&pStrucHtml);
				Web_REL_MakeAlarmStrucSection(&pStrucHtml);
				Web_REL_MakeStatusStrucSection(&pStrucHtml);
				Web_REL_MakeRAlarmStrucSection(&pStrucHtml);

				if(szRAlarmGetTime != NULL)
				{
					ReplaceString( &pStrucHtml, MAKE_VAR_FIELD(EQUIP_ALARM_DATE), szRAlarmGetTime);
					DELETE(szRAlarmGetTime);
					szRAlarmGetTime = NULL;
				}
				else
				{
					ReplaceString( &pStrucHtml, MAKE_VAR_FIELD(EQUIP_ALARM_DATE), "0");
				}

                //refresh rectifier
                ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_RECTIFIER), szRectifier);
                ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_ACD), szACD);
                ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_DCD), szDCD);
                ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_BATT), szBATT);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(AUTO_CONFIG_START_FALGE), szAutoCFGFlage);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(SLAVE_MODE_FALGE), szSlaveModeFlage);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(SMDU_CFG_FALGE), szSMDUCFGStatus);
				ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_CONVERT), szConverterNum);
				
				//ReplaceString( pStrucHtml, MAKE_VAR_FIELD(CONTROL_RESULT), "true");
                /*FILE *fp2;
                if((fp2 = fopen("/var/sa.htm","w")) !=  NULL)
                {
                    fwrite(pStrucHtml,strlen(pStrucHtml), 1, fp2);
                    fclose(fp2);
                }*/

				PostPage(pStrucHtml);
			
				DELETE(pStrucHtml);
				pStrucHtml = NULL;
			}
		}
		else
		{
			//sprintf(szTempbuf,"%ld",lCmdBuf[2]);
			CGI_ClearSessionFile(szSessionID);

			Web_REL_MakeSampleSignalSection(&pHtml);
			Web_REL_MakeControlSignalSection(&pHtml);
			Web_REL_MakeParaSignalSection(&pHtml);
			Web_REL_MakeRealtimeAlarmSection(&pHtml);
 			Web_REL_MakeStatusSection(&pHtml);
			
			if(szRAlarmGetTime != NULL)
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_ALARM_DATE), szRAlarmGetTime);
				DELETE(szRAlarmGetTime);
				szRAlarmGetTime = NULL;
			}
			else
			{
				ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_ALARM_DATE), "0");
			}
		
			//refresh time
			char szTime[32];
			sprintf(szTime, "%ld", now);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_NOW_TIME), szTime);


			//refresh rectifier
			ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_RECTIFIER), szRectifier);
            ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_ACD), szACD);
            ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_DCD), szDCD);
            ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_BATT), szBATT);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(AUTO_CONFIG_START_FALGE), szAutoCFGFlage);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(SLAVE_MODE_FALGE), szSlaveModeFlage);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(SMDU_CFG_FALGE), szSMDUCFGStatus);
			ReplaceString( &pHtml, MAKE_VAR_FIELD(EQUIP_NEED_REFRESH_CONVERT), szConverterNum);
			

			if(iEquipID == 1)
			{
				Sleep(2000);
			}   
			else
			{
				Sleep(1000);
			}
			//StuffHead( szHead, (int)strlen(pHtml) );
//#ifdef _DEBUG
			/*FILE *fp3;
			if((fp3 = fopen("/var/file2.htm","wb")) != NULL)
			{
				fwrite(pHtml,strlen(pHtml), 1, fp3);
				fclose(fp3);
			}*/
//#endif
			/*output the page*/	
			//PostPage(szHead);

            /*FILE *fp2;
            if((fp2 = fopen("/var/sa2.htm","w")) !=  NULL)
            {
                fwrite(pHtml,strlen(pHtml), 1, fp2);
                fclose(fp2);
            }*/
			PostPage(pHtml);

			DELETE( pHtml );
			pHtml = NULL;

		}
	}
	
	if(pSampleStrucBuf != NULL)
	{
		DELETE(pSampleStrucBuf);
		pSampleStrucBuf = NULL;
	}
	
	if(pControlStrucBuf != NULL)
	{
		DELETE(pControlStrucBuf);
		pControlStrucBuf = NULL;
	}
	
	if(pSettingStrucBuf != NULL)
	{
		DELETE(pSettingStrucBuf);
		pSettingStrucBuf = NULL;
	}
	
	if(pAlarmStrucBuf != NULL)
	{
		DELETE(pAlarmStrucBuf);
		pAlarmStrucBuf = NULL;
	}
	if(pStatusStrucBuf != NULL)
	{
		DELETE(pStatusStrucBuf);
		pStatusStrucBuf = NULL;
	}
	if(pRAlarmStrucBuf != NULL)
	{
		DELETE(pRAlarmStrucBuf);
		pRAlarmStrucBuf = NULL;
	}
	if(pSampleDataBuf != NULL)
	{
		DELETE(pSampleDataBuf);
		pSampleDataBuf = NULL;
	}
		
	if(pSettingDataBuf != NULL)
	{
		DELETE(pSettingDataBuf);
		pSettingDataBuf = NULL;
	}
		
	if(pControlDataBuf != NULL)
	{
		DELETE(pControlDataBuf);
		pControlDataBuf = NULL;
	}
	if(pRAlarmDatacBuf != NULL)
	{
		DELETE(pRAlarmDatacBuf);
		pRAlarmDatacBuf = NULL;
	}
	if(pStatusDataBuf != NULL)
	{
		DELETE(pStatusDataBuf);
		pStatusDataBuf = NULL;
	}

	if(pHtml != NULL)
	{
		DELETE(pHtml);
		pHtml = NULL;
	}
	 
	/*modify the head of the page*/
	/* Log( "OK" );*/
	return TRUE;
}


 

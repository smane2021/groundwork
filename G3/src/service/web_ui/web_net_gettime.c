/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : web_net_getip.c
 *  CREATOR  : Yang Guoxin              DATE: 2007-3-19 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static void Web_SET_PostTimePage(void);
static int Web_SET_GetCommandParam(void);
char		szUserInfo[32];

int main(void)
{
	int						nCommandType = 0;

	printf("Content-type:text/html\n\n");
	memset(szUserInfo, 0x0, sizeof(szUserInfo));

	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");

	/*get parameter*/
	Web_SET_GetCommandParam();
	Web_SET_PostTimePage();
	
	return TRUE;

}

static int Web_SET_GetCommandParam(void)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method; /* POST = 1, GET = 0 */  
    char	*val = NULL;
	int		nCommandType;

    form_method = getRequestMethod();
    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}
	
	char *ptr1 = NULL;
	if((val = getValue( getvars, postvars,  CGI_GET_AUTH_NAME)) != NULL)
	{
		sprintf(szUserInfo,"%s", val);
	}

	cleanUp(getvars, postvars);
    return nCommandType;  
}

static void Web_SET_PostTimePage(void)
{
#define DI_TIME_DAY					"DI_TIME_DAY"
#define DI_TIME_TIME				"DI_TIME_TIME"

	char	*pHtml, *ptr,*ptrTime;
	char	szTime[64];
	time_t	timeCurrent = time(NULL);
	char	szExchange[32];

	if(LoadHtmlFile("/var/netscape/timeconf.htm", &pHtml) < 0 )
	{
		return NULL;
	}


	TimeToString(timeCurrent, TIME_CHN_FMT, 
				szTime, sizeof(szTime));

	ptrTime = szTime;
	ptr = strchr(ptrTime, ' ');
	int iPosition = ptr - ptrTime;
	//printf("%d\n", iPosition);
	strncpyz(szExchange, ptrTime, iPosition + 1);

	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_TIME_DAY), szExchange );

	ptrTime = ptrTime + iPosition;
	ReplaceString( &pHtml, MAKE_VAR_FIELD(DI_TIME_TIME), ptr );

	if(strncmp(CGI_READ_NAME, szUserInfo, sizeof(CGI_READ_NAME)) == 0)
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_DI_DISABLED), "disabled" );
	}
	else
	{
		ReplaceString( &pHtml, MAKE_VAR_FIELD(CGI_DI_DISABLED), " " );
	}

	PostPage(pHtml);

	DELETE(pHtml);

}




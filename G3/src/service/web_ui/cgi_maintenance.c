static char *Web_GetData(IN char *ptr, IN char cSplit)
{
	ASSERT(ptr);

	int		iPosition = 0;
	char	*pSearchValue = NULL;
	char	*szPtr = NULL;

	pSearchValue = strchr(ptr, cSplit);
	if(pSearchValue != NULL)
	{
		iPosition = pSearchValue - ptr;
		if(iPosition >= 1)
		{
			szPtr = NEW(char, iPosition + 1);
			if(szPtr == NULL)
			{
				return NULL;
			}
			strncpyz(szPtr, ptr, iPosition + 1);
			ptr = ptr + iPosition;
		}
		ptr = ptr + 1;
		return szPtr;
	}
	return NULL;
}
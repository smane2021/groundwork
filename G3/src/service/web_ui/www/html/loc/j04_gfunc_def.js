//<!-- Copyright (c) 2020, Vertiv Tech Co. Ltd. by maofuhua-->

var isIE = (navigator.userAgent.toLowerCase().indexOf("msie") != -1) ? true : false;
var appVer = parseFloat(navigator.appVersion);
var varNeedTimeZone = false; //Not need time zone now;
var varSystemType = "/*[ID_SYSTEMTYPE]*/";
//var varSystemType = "SSL";
var stvarSystemType ="SSL"
	
function errorTrap(msg,url,line)
{
	/*var s = "Web browser reported an error: \"" + msg + "\"," +
		"\nAt " + url + " (line: " + line + ")." +
		"\n\nYou should close the browswer, and restart a new browser to try again.";

	alert(s);*/
}

onerror = errorTrap;

// another version, use to language="JavaScript1.2"
function validateIP(what) 
{
	if(what.search(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) == -1)
		return false;

	var myArray = what.split(/\./);
	var i;
	for( i = 0; i < 4; i ++ ) {
		if( !isNumeric(myArray[i]) )
			return false;

		var t=atoi(myArray[i]);
		if( (t<0) || (t>255) )
			return false;
	}

	return true;
}

// convert string 'nnn.nnn.nnn.nnn' to long format IP 
function convertIP( what )
{
	if( !validateIP( what ) )
		return 0;

	var myArray = what.split(/\./);
	var ip = 0, i;
	for( i = 0; i < 4; i ++ ) {
	   ip <<= 8;
	   ip += atoi(myArray[i]);
	}

	return ip;
}

function atoi( str )
{
	if( !isNumeric(str) )
		return 0;

	var s1 = "", i, c;

	// skip 0 before a decimal string  
	for( i = 0; i < str.length; i ++ ) {
		c = "" + str.charAt(i);
		if( c != "0" )
			break;
	}

	if( i == str.length )
		return 0;
	for( ; i < str.length; i ++ )
		s1 = s1 + str.charAt(i);

	return parseInt( s1 );
}

function strwid( s ) {// get the width of str in single byte
	var i, l = s.length;
	for( i = 0; i < s.length; i ++ ) {
		if( s.charCodeAt(i) > 255 )
			l ++;
	}
	return l;
}

function stringIP( ip ) // convert long ip to string as 'nnn.nnn.nnn.nnn' 
{
	if( isNaN(ip) )
		ip = atoi(ip);
	var strIP="" + ((ip)&0xff) + "." + ((ip>>8)&0xff) + "." + ((ip>>16)&255) + "." + ((ip>>24)&255);
	return strIP;
}

// if found one of char of str2 in the str1, return the first index
 // else return -1  
function findOneOf( str1, str2 )
{
	var idx, i;
	for( i=0; i < str2.length; i ++ )
	{
		idx = str1.indexOf( "" + str2.charAt(i) );

		if( idx >= 0 )
			return idx;
	}
	return -1;
}

function alltrim( obj)
{
	var trimmedstring="",startpos=0, endpos=obj.value.length - 1;
	// find start position of string 	
	while (startpos<=obj.value.length && obj.value.substring(startpos, startpos+1)==" ") {
		startpos++;
	}
	// null string 
	if (endpos == -1) {
		endpos=0;
	}
	
	// find end position of string 
	while (endpos >= 0 && obj.value.substring(endpos,endpos+1)==" ") {
		endpos--;
	}
	// replace value with trimmed string 
	obj.value=obj.value.substring(startpos,endpos+1);
	return obj;
}

function splitString( str, splitor )
{
	var a = new Array();
	var n = 0, c;
	
	a[n]="";
	for( var i=0; i < str.length; i ++ )
	{
		c = str.charAt(i);
		if( c != splitor )
		{
			a[n] +=c;
		}
		else
		{
			n ++;
			a[n]="";
		}
	}
	return a;
}


// istime - validate time format (hh:mm:ss) 			 
// note: requires function alltrim() in file alltrim.cfm 
// it also requires isNumeric() in file isdate.cfm 
function isTime(obj){
 if( obj.value == "" )
	return false;

	obj=alltrim(obj);
	var a = splitString( obj.value, ":" );
	if( a.length > 3 )
		return false;

	var hours=a[0];
	var minutes= a.length>=2 ? a[1] : "0";
	var seconds= a.length> 2 ? a[2] : "0";

	if( (hours.length > 2) || (hours.length == 0) ||
		(minutes.length > 2) || (minutes.length == 0) ||
		(seconds.length > 2) || (seconds.length == 0) )
	return false;	 

	// Hours must be between 0 and 24  
	var t = atoi(hours);
	if (isNumeric(hours) &&  t >= 0 && t < 24) {
		if (isNumeric(minutes)) {
			  minutes=atoi(minutes);
			  if (minutes >=0 && minutes <=59) {
				// seconds between 0 and 59 
				t = atoi(seconds);
				if (isNumeric(seconds) && t >= 0 && t<=59) {
				  return true;
				}
			  }
		}
	 }
 
 return false;
}


function isNumeric(strval) {
	var  c;
	for (var i=0; i<strval.length;i++) {
		c =strval.charAt(i);
		if (c < "0" || c > "9") {
			return false;
		}
	}
	return true;
}

function isLeapYear( year )
{
	var y = 0;
	if( (year%4 == 0) && !(year%400 != 0 && year%100==0) )
		y = 1;
	return y;
}

// isdate - validate date format (yyyy/mm/dd) 
var monthDay = new Array( 31,28,31,30,31,30,31,31,30,31,30,31 );
function isDate(obj) {
	var month, day, year;
	var retval = false;
	if (obj.value!="")
	{
		//check  of string 
		var a = splitString( obj.value, "/" );
		if( a.length == 3 && a[0] !="" && a[1]!="" && a[2]!="") {
			if( a[0].length > 4 || a[1].length > 2 || a[2].length > 2 )
				return false;	 

			// Month must be between 1 and 12  
			month=a[1]; 
			var t = atoi(month);
			if (isNumeric(month) &&  t> 0 && t <= 12) {
				month=t;
				year=a[0];
				// limit year to range 1900 - 2100 
				if (isNumeric(year) && atoi(year) >= 1900 && atoi(year)<=9999) {
					day=a[2]; 
					if (isNumeric(day)) {
						day=atoi(day);
						t = monthDay[month-1];
						if( month == 2 )
							t += isLeapYear(year);
						if ( day > 0 && day <= t ) {
							retval=true;
						}
					}
				}
			 }
		}
	}

	return retval;
}


//
// getCurtDate() -- get the current date with format ''yyyy/mm/dd" 
// getCurTime() -- get the current time with format "hh:mm:ss"
 
 function getCurDate(nSec) {
	if( isNaN(nSec) ) {
		nSec = atoi(nSec);
		if( isNaN(nSec) ) 
			nSec = 0;
	}
	var s="/";
	var d = new Date();
	if( nSec != 0 ) {
		d.setTime( nSec*1000 );
	}
	var dd=d.getDate();
	var sd=(dd<10) ? "0"+dd : ""+dd;
	var m=d.getMonth() + 1;
	var sm = (m<10) ? "0"+m : ""+m;
	var strDate = ""+d.getFullYear() + s + sm + s + sd;
	return strDate;
 }

function getCurTime(nSec) {
	if( isNaN(nSec) ) {
		nSec = atoi(nSec);
		if( isNaN(nSec) ) 
			nSec = 0;
	}
	var c=":";
	var d = new Date();
	if( nSec != 0 ) {
		d.setTime( nSec*1000 );
	}
	var s=d.getSeconds();
	var ss=(s<10) ? "0"+s : ""+s;
	var m=d.getMinutes();
	var sm=(m<10) ? "0"+m : ""+m;
	var h=d.getHours();
	var sh=(h<10) ? "0"+h : ""+h;
	var strTime = ""+sh + c + sm + c + ss;
	return strTime;    
}

function getCurUTCTime(nSec) {
	if( isNaN(nSec) ) {
		nSec = atoi(nSec);
		if( isNaN(nSec) ) 
			nSec = 0;
	}
	var c=":";
	var d = new Date();
	if( nSec != 0 ) {
		d.setTime( nSec*1000 );
	}
	var s=d.getUTCSeconds();
	var ss=(s<10) ? "0"+s : ""+s;
	var m=d.getUTCMinutes();
	var sm=(m<10) ? "0"+m : ""+m;
	var h=d.getUTCHours();
	var sh=(h<10) ? "0"+h : ""+h;
	var strTime = ""+sh + c + sm + c + ss;
	return strTime;    
}

function getSampleCurDate(nSec) 
{
	if( isNaN(nSec) ) {
		nSec = atoi(nSec);
		if( isNaN(nSec) ) 
			nSec = 0;
	}
	var s="-";
	var d = new Date();
	if( nSec != 0 ) {
		d.setTime( nSec*1000 );
	}
	var dd=d.getDate();
	var sd=(dd<10) ? "0"+dd : ""+dd;
	var m=d.getMonth() + 1;
	var sm = (m<10) ? "0"+m : ""+m;
	var strDate = ""+d.getFullYear() + s + sm + s + sd;
	return strDate;
}

function getSampleCurUTCDate(nSec) 
{
	if( isNaN(nSec) ) {
		nSec = atoi(nSec);
		if( isNaN(nSec) ) 
			nSec = 0;
	}
	var s="-";
	var d = new Date();
	if( nSec != 0 ) {
		d.setTime( nSec*1000 );
	}
	var dd=d.getUTCDate();
	var sd=(dd<10) ? "0"+dd : ""+dd;
	var m=d.getUTCMonth() + 1;
	var sm = (m<10) ? "0"+m : ""+m;
	var strDate = ""+d.getUTCFullYear() + s + sm + s + sd;
	return strDate;
}

function makeSampleDateStr(nSec)
{

	
	if(varNeedTimeZone)//Need time zone
	{
		return "" + getSampleCurDate(nSec) + " " + getCurTime(nSec);
	}
	else			  //Not need time zone
	{
		return "" + getSampleCurUTCDate(nSec) + " " + getCurUTCTime(nSec);;
	}
}

function makeQueryDateStr(nSec, boolLast)
{
	if(!boolLast)
	{
		return "" + getSampleCurDate(nSec) + " " + "00:00:00";
	}
	else
	{
		return "" + getSampleCurDate(nSec) + " " + "23:59:59";
	}
} 



function getStrTime(nSec)
{
	return "" + getCurDate(nSec) + " " + getCurTime(nSec);
}

// get the GMT time in seconds
// NOTE: must call isDate and isTime before calling getSeconds! maofuhua. 2001/03/29
function getSeconds( strDate, strTime )
{
	var ad = splitString( strDate, "/" );
	var at = splitString( strTime, ":" );
	if( at.length < 1 ) at[0] = "0";
	if( at.length < 2 ) at[1] = "0";
	if( at.length < 3 ) at[2] = "0";

	if(varNeedTimeZone)  //Need time zone
	{
		var d = new Date( atoi(ad[0]), atoi(ad[1])-1, atoi(ad[2]), 
			atoi(at[0]), atoi(at[1]), atoi(at[2]) );
		var n = Math.floor(d.getTime() / 1000);
	}	
	else	//not need time zone
	{
		//var d = new Date.UTC( atoi(ad[0]), atoi(ad[1])-1, atoi(ad[2]), 
		//	atoi(at[0]), atoi(at[1]), atoi(at[2]) );
		//var n = Math.floor(d / 1000);
		
		var d = new Date( atoi(ad[0]), atoi(ad[1])-1, atoi(ad[2]), 
			atoi(at[0]), atoi(at[1]), atoi(at[2]) );
		var n = Math.floor(d.getTime() / 1000 - new Date().getTimezoneOffset() * 60);

		
	}  
	return n;
}

function getCookieVal (offset) 
{
  var endstr = document.cookie.indexOf (";", offset);
  if (endstr == -1)
	endstr = document.cookie.length;
  return unescape(document.cookie.substring(offset, endstr));
}


function GetCookie(name) {
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	//alert(document.cookie.length);
	//alert(document.cookie);
	var i = 0;
	while (i < clen) {
	var j = i + alen;
	if (document.cookie.substring(i, j) == arg)
	  return getCookieVal (j);
	i = document.cookie.indexOf(" ", i) + 1;
	if (i == 0) break; 
	}
	return null;
}

function SetCookie (name,value,expires,path,domain,secure) {
  document.cookie = name + "=" + escape (value) +
	((expires) ? "; expires=" + expires.toGMTString() : "") +
	((path) ? "; path=" + path : "") +
	((domain) ? "; domain=" + domain : "") +
	((secure) ? "; secure" : "");
}

function DeleteCookie(name,path,domain) {
  if (GetCookie(name)) {
	document.cookie = name + "=" +
	  ((path) ? "; path=" + path : "") +
	  ((domain) ? "; domain=" + domain : "") +
	  "; expires=Sat, 01 Jan 2000 08:29:00 GMT";
  }
}

function CookieIsEnabled(bCanSave) {
	// navigator.cookieEnabled: in NS 4 is undefined.
	var ck = "MFH_TestCookEnabled_ZH", v = "TestCookie";
	var expire = null;
	
	if( bCanSave ) {
		expire = new Date();
		expire.setTime( new Date().getTime()+30*86400000);
	}
	
	SetCookie( ck, v, expire );
	
	if(GetCookie(ck) != v)
		return false;

	DeleteCookie( ck );

	return true;
}

function getElement( id ) {
	var x;

	if (document.getElementById )
		x = document.getElementById(id);
	else if (document.all)
		x = document.all[id];
	else if (document.layers ) {
		x = document.layers[id];
	}
	
	return x;
}

function cvrtStr(src) {
	var c, i, s = "";
	for( i = 0; i < src.length; i ++ ) {
		c = src.charAt(i);
		if( (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9') )
			c = '_';
		s += c;
	}
	return s;
}

function insertYear(o)
{
	var i = 0;
	var j = 1990;
	var t = document.getElementById(o);
	
	if(t!=null)
	{	
		for (i = 0 ; i< 50 ; i++)
		{
					
			document.writeln("<option>" + (j++) + "</option>");
		
		}
		now = new Date();
		var numYear = now.getYear();
	 	t.selectedIndex = numYear-1990;
	}

}

function insertMonth(o)
{
	var i = 1;
	var t = document.getElementById(o);
	
	if(t!=null)
	{	
		for (i = 1 ; i< 13 ; i++)
		{
					
			document.writeln("<option>" + i + "</option>");
		
		}
		now = new Date();
		var numMonth = now.getMonth();
	 	t.selectedIndex = numMonth;
	 }
}
function insertDay(o)
{
	var i = 1;
	 
	var t = document.getElementById(o);
	
	if(t == null)
	{
		 
		return false;
	}
	
	if ( parseInt(o.substr(4))==1)
	{
		 
		var t1 = parseInt(this._year1.selectedIndex + 1990);
		var t2 = parseInt(this._month1.selectedIndex);	
	}
	else if( parseInt(o.substr(4))==2)
	{
		 
		var t1 = parseInt(this._year2.selectedIndex + 1990);
		var t2 = parseInt(this._month2.selectedIndex);
	}

	
	var numDays=new Array(31,28,31,30,31,30,31,31,30,31,30,31);
  	
  	day = numDays[t2];
  	if(t2==1 && t1%4==0) ++day;
  	 
	for (i = 1 ; i< day+1 ; i++)
	{
					
		document.writeln("<option>" + i + "</option>");
		
	}
	now = new Date();
	var numDay = now.getDate();
	t.selectedIndex = numDay - 1;
}

function trim(s)
{
    if (s == null)
    {
        return s;
    }

    var i;
    var beginIndex = 0;
    var endIndex = s.length - 1;

    for (i=0; i<s.length; i++)
    {
        if (s.charAt(i) == ' ' || s.charAt(i) == '��')
        {
            beginIndex++;
        }
        else
        {
            break;
        }
    }

    for (i = s.length - 1; i >= 0; i--)
    {
        if (s.charAt(i) == ' ' || s.charAt(i) == '��')
        {
            endIndex--;
        }
        else
        {
            break;
        }
    }

    if (endIndex < beginIndex)
    {
        return "";
    }

    return s.substring(beginIndex, endIndex + 1);
}
 


function checkDateByMask(dt,msg,fm,type)
{

//Define synax
var N=1;
var format=new Array(1);
format[0]="yyyy-MM-dd hh:mm:ss";


 

   

var b=false;
for(var i=0;i<N;i++){

     if(format[i].toLowerCase()==fm.toLowerCase()){
            b=true;break;
     }
}
if(!b){
     return getErrorMsg(errPar);
}


if(dt.length!=fm.length){
     var dt4=dt.replace(/[^0-9]/g,",")
     var dtarr=dt4.split(",");
     var dt3="";
     var dtlen=0;
     for(var i=0;i<dtarr.length;i++){
         var len=dtarr[i].length;
         dtlen=dtlen+len+1;
         if(len<1)
                dt3=dt3+"00"+dtarr[i];
         else if(len<2)
                dt3=dt3+"0"+dtarr[i];
         else
                dt3=dt3+dtarr[i];
                
         dt3=dt3+dt.substr(dtlen-1,1);
         }
     dt=dt3;
}

if(dt.length!=fm.length){         
     return getErrorMsg(errFormat);
}
else{
     var dt1=dt.replace(/[0-9]/g,"0");
     var dt2=fm.replace(/[ymdhs]/gi,"0");
     //alert(dt1+"\n"+dt2);
     if(dt1!=dt2){
           return getErrorMsg(errFormat);
     }
}     
         

try{
     fm=fm.replace(/Y/g,"y").replace(/D/g,"d");
     var iyyyy=fm.indexOf("yyyy");
     var iyy=fm.indexOf("yy");
     var imm=fm.indexOf("MM");
     var idd=fm.indexOf("dd");
     var ihh=fm.indexOf("hh");
     var imi=fm.indexOf("mm");
     var iss=fm.indexOf("ss");
    
     var newdt=new Date(); 
     
     var year="";
     //Year    
     try{
         var isyear=false;
         if(iyyyy>-1){                
            year=dt.substr(iyyyy,4);
            isyear=true;
         }
         else if(iyy>-1){
            year=dt.substr(iyy,2);
            isyear=true;
         }
         if(isyear){
            if(type=="1"){//
               year=parseInt(year)+1911;
            }
            newdt.setFullYear(year);
         }   
     }
     catch(e1){
			getErrorMsg(errYear+e1.toString());
			return false; 
     }
     
     //Month
     try{     
         if(imm>-1){
             if(dt.substr(imm,2)>"12"||dt.substr(imm,2)<"01"){
                 getErrorMsg(errMonth);
                 return false;
             }
             //----------------------------------------------------
				//Don't modify. Solve the February problem
             newdt.setMonth(dt.substr(imm,2)-1);
             newdt.setMonth(dt.substr(imm,2)-1);
             //----------------------------------------------------
         }
     }
     catch(e1){
          getErrorMsg(errMonth+e1.toString());
          return false;
     }
     
     //Day
     try{     
         if(idd>-1){
             if(dt.substr(idd,2)>"31"||dt.substr(idd,2)<"01"){
                 getErrorMsg(errDay);
                 return false;
             }
             newdt.setDate(dt.substr(idd,2));  
         }
     }
     catch(e1){
         getErrorMsg(errDay);
         return false;
     }
     
     //Hour
     try{
         if(ihh>-1){
             if(dt.substr(ihh,2)>"23"){
                 getErrorMsg(errHour);
                 return false;
             }
             newdt.setHours(dt.substr(ihh,2));
         }
     }
     catch(e1){
         getErrorMsg(errHour);
         return false;
     }
     
     //Minute
     try{
         if(imi>-1){
             if(dt.substr(imi,2)>"59"){
                getErrorMsg(errMinute);
                return false;
             }
             newdt.setMinutes(dt.substr(imi,2));
         }
     }
     catch(e1){
         getErrorMsg(errMinute);
          return false;
     }
     
     //Second
     try{
         if(iss>-1){
             if(dt.substr(iss,2)>"59"){
                 getErrorMsg(errSecond);
                  return false;
             }
             newdt.setSeconds(dt.substr(iss,2));
         }         
     }
     catch(e1){
			getErrorMsg(errSecond);
			return false;
     }
          
     //Year
     if(iyyyy>-1){
          if(newdt.getFullYear()!=year){
                 getErrorMsg(errYear); 
                  return false;
          }
     }
     else if(iyy>-1){
          if(newdt.getFullYear()!=year){
                 getErrorMsg(errYear); 
                  return false;
          }
     }
     
     //Month
     if(imm>-1){
          if(newdt.getMonth()!=(dt.substr(imm,2)-1)){
                 getErrorMsg(errDay); 
                  return false;
          }
     }
     
     //Day       
     if(idd>-1){
          if(newdt.getDate()!=dt.substr(idd,2)){
                 getErrorMsg(errDay); 
                  return false;
          } 
     }    
            

     //Hour
     if(ihh>-1){
          if(newdt.getHours()!=dt.substr(ihh,2)){
                 getErrorMsg(errMinute); 
                  return false;
          } 
     }
     
     //Minute
     if(imi>-1){
          if(newdt.getMinutes()!=dt.substr(imi,2)){
                 getErrorMsg(errSecond); 
                  return false;
          } 
     }
     
     //Second
     if(iss>-1){          
          if(newdt.getSeconds()!=dt.substr(iss,2)){
                 getErrorMsg(errSecond); 
                  return false;
          }
     } 
     //return newdt;
     return Math.floor(newdt.getTime() / 1000 - new Date().getTimezoneOffset() * 60);
}  
catch(e){
     getErrorMsg(e.toString()); 
      return false;
}

            
}

function getErrorMsg(msg)
{
     alert(msg);
     return false;
 }

 
function checkDate(s)
{

    return checkDateByMask(s,"","yyyy-MM-dd hh:mm:ss","");
}

function download(obj)
{
	//window.location='/download.txt'
	//var downloadFile = "http://" + location.host +"/download.txt";
	//alert(downloadFile);
	alert(obj.href);
	var win=window.open(obj.href,'_blank','top=1200px,left=1200px');
	win.document.execCommand('saveAs');
	win.opener=null;
	win.close();
	event.returnValue=false;
}

function GetAllLength(s)
{
	return  s.replace(/[^\x00-\xff]/g,"aa").length
}

function getIPFromLocation(varLocation)
{
//alert(varSystemType);
//alert(stvarSystemType);
	var varLocation1 = "" + varLocation;//window.location;
	 if(varSystemType == stvarSystemType)
	 {
	    var varLocation2= varLocation1.substring(8, varLocation1.length); 
	 }
	 else
	 {
	    var varLocation2= varLocation1.substring(7, varLocation1.length); 
	 }
	
	var varLength = findOneOf(varLocation2, "/");
	var varIP =varLocation2.substring(0, varLength);
	
	return varIP;
	
}


function DeleteCookieT(name) 
{
  if (GetCookie(name)) 
  {
	var exp = new Date();
	var cval=GetCookie(name);
    exp.setTime(exp.getTime() - 1000);
    SetCookie(name, cval, exp,"/");
  }
 }
 //BODY��ɫ����
 var setGradient = (function(){  
     //private variables;  
     var p_dCanvas = document.createElement('canvas');  
     var p_useCanvas =  !!( typeof(p_dCanvas.getContext) == 'function');  
     var p_dCtx = p_useCanvas?p_dCanvas.getContext('2d'):null;  
     var p_isIE = /*@cc_on!@*/false;  
       
     //test if toDataURL() is supported by Canvas since Safari may not support it  
     try{ p_dCtx.canvas.toDataURL() }  
     catch(err){ p_useCanvas = false; };  
   
     if(p_useCanvas){  
         return function (dEl , sColor1 , sColor2 , bRepeatY ){  
             if(typeof(dEl) == 'string') dEl =  document.getElementById(dEl);  
             if(!dEl) return false;  
             var nW = dEl.offsetWidth;  
             var nH = dEl.offsetHeight;  
             p_dCanvas.width = nW;  
             p_dCanvas.height = nH;  
   
             var dGradient;  
             var sRepeat;  
             // Create gradients  
             if(bRepeatY){  
                 dGradient = p_dCtx.createLinearGradient(0,0,nW,0);  
                 sRepeat = 'repeat-y';  
             }else{  
                 dGradient = p_dCtx.createLinearGradient(0,0,0,nH);  
                 sRepeat = 'repeat-x';  
             }         
               
             dGradient.addColorStop(0,sColor1);  
             dGradient.addColorStop(1,sColor2);                
               
             p_dCtx.fillStyle = dGradient ;   
             p_dCtx.fillRect(0,0,nW,nH);  
             var sDataUrl = p_dCtx.canvas.toDataURL('image/png');  
               
             with(dEl.style){  
                 backgroundRepeat = sRepeat;  
                 backgroundImage = 'url(' + sDataUrl + ')';  
                 backgroundColor = sColor2;      
             };  
        }  
     }else if(p_isIE){  
         p_dCanvas = p_useCanvas = p_dCtx =  null;         
         return function (dEl , sColor1 , sColor2 , bRepeatY){  
             if(typeof(dEl) == 'string') dEl =  document.getElementById(dEl);  
             if(!dEl) return false;  
             dEl.style.zoom = 1;  
             var sF = dEl.currentStyle.filter;  
             dEl.style.filter += ' ' + ['progid:DXImageTransform.Microsoft.gradient( GradientType=',  +(!!bRepeatY),',enabled=true,startColorstr=',sColor1,', endColorstr=',sColor2,')'].join('');  
         };  
     }else{  
         p_dCanvas = p_useCanvas = p_dCtx =  null;  
         return function(dEl , sColor1 , sColor2  ){  
             if(typeof(dEl) == 'string') dEl =  document.getElementById(dEl);  
             if(!dEl) return false;  
             with(dEl.style){ backgroundColor = sColor2; };  
             //alert('your browser does not support gradient effet');  
         }  
     }  
 })();
 setGradient(document.body, '#fefefe','#dfe4ea',0); 
 
 
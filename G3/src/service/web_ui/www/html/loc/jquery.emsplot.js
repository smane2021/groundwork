
(function($){
	$.lineParameter = {
		title			: null,		 			//线条的标题
		titleClass  	: "plot-title",
		background  	: "#fffdf6", 			//画布的颜色
		gridColor   	: ["#ddd","#ccc"],		//格子线的颜色
		gridOutColor	: "#333",	 			//画布外框的颜色
		gridLineWidth   : [1,2],
		scaleColor 		: "#999",	 			//刻度的颜色
		scaleLine   	: 5,		 			//刻度线的长度
		xShow       	: true,		 			//是否显示横坐标,true:显示,false:隐藏
		xTitle			: null,		 			//X坐标的标题
		xTitleClass 	: "xaxis-title",		//X坐标文字样式
		xHeight 		: 40,		 				//X坐标在目标DIV中的高度
		xClass  		: "emsPlot-axis emsPlot-xaxis",  //X坐标文字样式
		xlist 			: null,		 			//x坐标数值,如果在前端设置值,则默认平分画布
		yShow			: true,		 			//设置方法同上xShow
		yTitle			: null,		 			//Y坐标的标题
		yTitleClass		: "yaxis-title",
		yWidth  		: 40,		 				//Y坐标在目标DIV中的宽度
		yAxisClass  	: "emsPlot-axis emsPlot-yaxis",  //Y坐标文字样式
		ylist 			: null,		 			//y坐标数值,方法同上xlist
		titleHeight 	: 20,		 			//顶部标题在目标DIV中的高度
		rightSpace  	: 15,        			//画布右侧所右的宽度
		lineColor   	: ["#3b87c6","#ddd"],   //线条的颜色
		lineWidth   	: [5,1],		  		//线条的宽度
		pointsColor 	: ["#ff9966","#ffcc99"],//数据点的颜色
		pointsRadius	: 6, 		 			//数据点的半径
		midLine			: [50,80],		 			//中间线
		midLineColor	: [["#b7d548","#fff"],["#cc3333","#fff"]],  	//中间线颜色
		midLineWidth	: [[5,1],[5,1]],
		midAlpha		: [0.8,0.5],
		dateJoint		: ".",					//日期连接符
		toPercent		: [false,1]			    //是否转换成百分比,如果为true,且为为1和0则转换
	};
	$.setLineParameter = function(opts){
		if(opts)
		{
			this.title  	  = opts.title 		  	? opts.title 		    : $.lineParameter.title;
			this.titleClass   = opts.titleClass   	? opts.titleClass     	: $.lineParameter.titleClass;
			this.background   = opts.background   	? opts.background     	: $.lineParameter.background;
			this.gridColor    = opts.gridColor    	? opts.gridColor      	: $.lineParameter.gridColor;
			this.gridOutColor = opts.gridOutColor 	? opts.gridOutColor   	: $.lineParameter.gridOutColor;
			this.gridLineWidth= opts.gridLineWidth	? opts.gridLineWidth  	: $.lineParameter.gridLineWidth;			
			this.scaleColor   = opts.scaleColor   	? opts.scaleColor     	: $.lineParameter.scaleColor;
			this.scaleLine    = opts.scaleLine    	? opts.scaleLine      	: $.lineParameter.scaleLine;
			this.xShow        = opts.xShow==false 	? opts.xShow          	: $.lineParameter.xShow;			
			this.xTitle       = opts.xTitle 	  	? opts.xTitle 	    	: $.lineParameter.xTitle;						
			this.xTitleClass  =	opts.xTitleClass  	? opts.xTitleClass    	: $.lineParameter.xTitleClass
			this.xHeight  	  = opts.xHeight      	? opts.xHeight        	: $.lineParameter.xHeight;
			this.xClass  	  = opts.xClass       	? opts.xClass         	: $.lineParameter.xClass;
			this.xlist        = opts.xlist        	? opts.xlist          	: $.lineParameter.xlist;
			this.xExtent      = opts.xExtent      	? opts.xExtent        	: $.lineParameter.xExtent;
			this.yShow        = opts.yShow==false 	? opts.yShow 		    : $.lineParameter.yShow;						
			this.yTitle       = opts.yTitle 	  	? opts.yTitle 	    	: $.lineParameter.yTitle;						
			this.yTitleClass  = opts.yTitleClass  	? opts.yTitleClass    	: $.lineParameter.yTitleClass;
			this.yWidth   	  = opts.yWidth   		? opts.yWidth     		: $.lineParameter.yWidth;
			this.yAxisClass   = opts.yAxisClass   	? opts.yAxisClass     	: $.lineParameter.yAxisClass;
			this.ylist        = opts.ylist        	? opts.ylist          	: $.lineParameter.ylist;			
			this.yExtent      = opts.yExtent      	? opts.yExtent        	: $.lineParameter.yExtent;
			this.titleHeight  = opts.titleHeight  	? opts.titleHeight    	: $.lineParameter.titleHeight;
			this.rightSpace   = opts.rightSpace   	? opts.rightSpace     	: $.lineParameter.rightSpace;
			this.lineColor    = opts.lineColor    	? opts.lineColor      	: $.lineParameter.lineColor;
			this.lineWidth    = opts.lineWidth    	? opts.lineWidth      	: $.lineParameter.lineWidth;
			this.pointsColor  = opts.pointsColor  	? opts.pointsColor    	: $.lineParameter.pointsColor;
			this.pointsRadius = opts.pointsRadius 	? opts.pointsRadius   	: $.lineParameter.pointsRadius;
			this.midLine	  = opts.midLine	  	? opts.midLine			: $.lineParameter.midLine;
			this.midLineColor = opts.midLineColor 	? opts.midLineColor		: $.lineParameter.midLineColor;		
			this.midLineWidth = opts.midLineWidth 	? opts.midLineWidth		: $.lineParameter.midLineWidth;	
			this.midAlpha	  = opts.midAlpha		? opts.midAlpha			: $.lineParameter.midAlpha;
			this.dateJoint 	  = opts.dateJoint 		? opts.dateJoint		: $.lineParameter.dateJoint;
			this.toPercent 	  = opts.toPercent 		? opts.toPercent		: $.lineParameter.toPercent;
		}
	};	
	$.emsplot = function(target,data,options)
	{
		var t = $("#"+target);
		t.css({"position":"relative"});//强制设置成relative
		var tName = target;
		if(options && $.isHaveParameter(options))
		{
			options = new $.setLineParameter(options);
		}
		else
		{
			options = $.lineParameter;
		}
		$.drawAxis(t,tName,data,options);
	}
	$.drawAxis = function(t,name,data,options)
	{
		
		//初始化DIV(清除上贞绘制的内容)
		var cnum = $(t).children();
		if(cnum.length>0)
		{
			cnum.remove();
		};
		
		var container = new $.getTargetSize(t);
		var cvs,ctx;
		var canvas = document.createElement("canvas");	//背景
		var xcanvas = document.createElement("canvas"); //x坐标
		var ycanvas = document.createElement("canvas"); //y坐标
		var mcanvas = document.createElement("canvas"); //主要绘图区域
		$.canvasForIE(canvas);
		$.canvasForIE(xcanvas);
		$.canvasForIE(ycanvas);
		$.canvasForIE(mcanvas);
		canvas.id = name+"_grid";
		xcanvas.id = name+"_xaxis";
		ycanvas.id = name+"_yaxis";
		mcanvas.id = name+"_main";
		t.append(canvas);
		t.append(xcanvas);
		t.append(ycanvas);
		t.append(mcanvas);
		
		//优化变量
		var y = options.ylist;
		var yw = options.yWidth;
		var x = options.xlist;
		var xh = options.xHeight;
		
		
		//x  = [["2010/9/10",45],["2010/10/26",65],["2011/1/1",20],["2011/2/1",85],["2011/3/15",52]];
		
		var xInt,yInt,cw,ch;
		if(!$.checkAxisText(y))
		{
			yInt = data.length;
			ch = Math.floor((container.height-xh-options.titleHeight)/yInt)*yInt;
			var ymin=ch;
			var ymax=ch;
			for(var i=0; i<data.length; i++)
			{
					ymin= Math.min(ymin,data[i][1]);
					ymax = Math.max(ymax,data[i][1]);
			}
			var yHeight = Math.ceil(ymax/yInt)*yInt;
			var newYList = new Array();
			var yScale = Math.ceil((yHeight/yInt)/10)*10;
			for(var n =0; n<=data.length+1; n++)
			{
				newYList.push(n*yScale);
			}
			y=newYList;
		}
		if(!$.checkAxisText(x))
		{
			xInt = data.length
			cw = Math.floor((container.width-yw-options.rightSpace)/xInt)*xInt;
			var xmin=cw;
			var xmax=cw;
			for(var i=0; i<data.length; i++)
			{
					xmin= Math.min(xmin,data[i][0]);
					xmax = Math.max(xmax,data[i][0]);
			}
			var xWidth = Math.ceil(xmax/xInt)*xInt;
			var newXList = new Array();
			var xScale = Math.ceil((xWidth/xInt)/10)*10;
			for(var n =0; n<=data.length+1; n++)
			{
				newXList.push(n*xScale);
			}
			x=newXList;
		}		
		xInt = $.checkAxisText(x) ? x.length-1 : x.length;
		yInt = $.checkAxisText(y) ? y.length-1 : y.length;
		cw = Math.floor((container.width-yw-options.rightSpace)/(x.length*5))*(x.length*5);
		ch = Math.floor((container.height-xh-options.titleHeight)/yInt)*yInt;
		$("#"+canvas.id).attr({"width":cw+1,"height":ch+1});
		$("#"+canvas.id).css({"position":"absolute","left":yw,"bottom":xh});
		cvs = $("canvas",t)[0];
		ctx = cvs.getContext("2d");
		ctx.lineCap = "round";
		cw = cw - 15;
		
		//格式化X坐标时间为date对象,并保存在newDate中
		var newDate = [];

		for(var j=0; j<x.length; j++)
		{	
			newDate.push([new Date(Date.parse(x[j])),0]);
		}

		
		//获取总天数
		function getDaysInMonth(year,month) {
				var month = parseInt(month,10)+1;
				var temp = new Date(year,month,0)
				return temp.getDate();
		}	
		var totalDays =0 ;
		for(i=0; i<newDate.length; i++)
		{
			var year = newDate[i][0].getFullYear();
			var month = newDate[i][0].getMonth();
			totalDays += getDaysInMonth(year,month);
		}
		var xStartDate = newDate[0][0].getTime();
		var xEndDateYear = newDate[newDate.length-1][0].getFullYear();
		var xEndDateMonth = newDate[newDate.length-1][0].getMonth();
		var xEndDate = new Date(Date.UTC(xEndDateYear,xEndDateMonth,getDaysInMonth(xEndDateYear,xEndDateMonth))).getTime()

		//处理数据,保证显示数据在X坐标范围之内,超出范围的则不绘制
		var newData = [];
		for(var j=0; j<data.length; j++)
		{	
			newData.push([new Date(Date.parse(data[j][0])),data[j][1]]);  //转换数据时间为Date对象
		}
		
		var si=0,ei = newData.length;
		for(var j=0; j<newData.length; j++)
		{
			if(newData[j][0].getTime()<xStartDate)
			{
				newData.splice(j,1);
				j--
			}
		}
		
		
		for(var j=0; j<newData.length; j++)
		{
			if(newData[j][0].getTime()>xEndDate)
			{
				ei = j-1;
				break;
			}
			else if(newData[j][0].getTime()==xEndDate)
			{
				ei = j;
				break;
			}
			
		}
		newData = newData.slice(0,ei+1);
		for(var i = 0; i<newData.length; i++)
		{
			if(i>0)
			{
				//判断前一个数据是否为当前天的前一天的数据，如果是则不处理，如果不是则添加前一天的数据为前一项的数据
				if(newData[i][0].getTime() - newData[i-1][0].getTime() != 86400000) //一天为24*60*60*1000=86400000毫秒,传入的数据都是0时0分0秒0毫秒的状态
				{	
					newData.splice(i,0,[new Date(newData[i][0].getTime() - 86400000),newData[i-1][1]]);
				}
			}
		}
		//转换成百分比
		if(options.toPercent[0]==true)
		{
			for(var i =0; i<newData.length; i++)
			{
			 	newData[i][1] = (100*newData[i][1]/options.toPercent[1]).toFixed(1);
			}
		}
		/*
		if(newData[0][0].getTime()>newDate[0][0].getTime())
		{
			newData.unshift([newDate[0][0],newData[0][1]]);	
		}
		*/
		
	
		
		
		//计算当前数据坐标天数
		var nowMonth,nowDay,nowYear,currentDays,prevDays;
		var startYear = newDate[0][0].getFullYear();
		var startMonth = newDate[0][0].getMonth()+1;
		$.getCurrent = function(i,data){
			currentDays = 0;
			prevDays=0;
			nowMonth = data[i][0].getMonth()+1;
			nowYear	 = data[i][0].getFullYear();
			if(nowYear == startYear)
			{
				for(var j=startMonth; j<nowMonth; j++)
				{
					prevDays +=  getDaysInMonth(startYear,j);
				}
			}
			else
			{
				for(var j=startMonth; j<=12; j++)
				{
					prevDays +=  getDaysInMonth(startYear,j);
				}
				for(var j=1; j<nowMonth; j++)
				{
					prevDays +=  getDaysInMonth(startYear,j);
				}
			}
			currentDays = prevDays + data[i][0].getDate();
			return currentDays;
		}
		
		//draw grids
		var ogc = options.gridColor;
		var ogw = options.gridLineWidth
		ctx.strokeStyle = ogc[1];
		ctx.lineWidth = ogw[1];
		var gridX;
		
		for(var i=0; i<newDate.length; i++)
		{
			gridX = Math.round(($.getCurrent(i,newDate)/totalDays)*cw);
			if(i!=0)
			{
			ctx.beginPath();
			ctx.moveTo(gridX,0);
			ctx.lineTo(gridX,ch);
			ctx.stroke();
			}
		}
		ctx.save();		
		ctx.restore();
		ctx.strokeStyle = ogc[0];
		ctx.lineWidth = ogw[0];
		var gridX2;
		for(var i=0; i<newDate.length; i++)
		{
			gridX2 = ($.getCurrent(i,newDate)/totalDays)*cw;
			if(i>=1)
			{
				gridX3 = ($.getCurrent(i-1,newDate)/totalDays)*cw;
			}
			else
			{
			 	gridX3 = 0;
			}
			if(i!=0)
			{
				for(var j=0; j<10; j++)
				{
					ctx.beginPath();
					ctx.moveTo(Math.round(gridX2-((gridX2-gridX3)/10)*j)+0.5,0);
					ctx.lineTo(Math.round(gridX2-((gridX2-gridX3)/10)*j)+0.5,ch);
					ctx.stroke();
				}
				if(i==newDate.length-1)
				{
				 	for(var j=0; j<10; j++)
					{
						ctx.beginPath();
						ctx.moveTo(Math.round(gridX2+((gridX2-gridX3)/10)*j)+0.5,0);
						ctx.lineTo(Math.round(gridX2+((gridX2-gridX3)/10)*j)+0.5,ch);
						ctx.stroke();
					}
				}
			}
		}
		ctx.save();
		ctx.restore();
		
		//draw Xaxis text list
		var xText = document.createElement("div");
		xText.id = name+"_xtext";
		xText.className = options.xClass;
		xText.style.display = options.xShow==true ? "" : "none";
		t.append(xText);
		
		for(var i=0; i<newDate.length; i++)
		{	
			
			var gm = ((newDate[i][0].getMonth()+1) == 0) ? 12 : (newDate[i][0].getMonth()+1);
			

			gm = gm<10 ? "0"+gm : gm;
			var gd = newDate[i][0].getDate();
			var value = gm+options.dateJoint+gd;
			
			textX = ($.getCurrent(i,newDate)/totalDays)*cw;
			


			gridX = Math.round(($.getCurrent(i,newDate)/totalDays)*cw);
			if(i!=0)
			{
$("#"+xText.id).append("<div style='position: absolute;left:"+(textX-15+yw)+"px;bottom:"+(xh-4*options.scaleLine)+"px;text-align:center;width:30px'>"+value+"</div>");
			}
		}
		
		//plot title
		if(options.title!=null)
		{
			var plotTitle = document.createElement("div");
			plotTitle.id = name+"_plotTitle";
			plotTitle.className = options.titleClass;
			plotTitle.innerHTML = options.title;	
			t.append(plotTitle);
			$("#"+plotTitle.id).css({"width":t.width()})
		}
		//x title
		if(options.xTitle!=null)
		{
			var xTitle = document.createElement("div");
			xTitle.id = name+"_xAxisTitle";
			xTitle.className = options.xTitleClass;
			xTitle.innerHTML = options.xTitle;	
			t.append(xTitle);
			$("#"+xTitle.id).css({"width":t.width()})
		}
		//y title
		if(options.yTitle!=null)
		{
			var yTitle = document.createElement("div");
			yTitle.id = name+"_yAxisTitle";
			yTitle.className = options.yTitleClass;
			yTitle.innerHTML = "<span>"+options.yTitle+"</span>";	
			t.append(yTitle);
			$("#"+yTitle.id).css({"height":t.height(),"padding-top":(t.height()-$("span","#"+yTitle.id).height())/2})
		}
		
		//draw Yaxis text list
		var yAxisText = document.createElement("div");
		yAxisText.id = name+"_ytext"
		yAxisText.className = options.yAxisClass;
		yAxisText.style.display = options.yShow==true ? "" : "none";
		t.append(yAxisText);		
		for(var i=0; i<y.length; i++)
		{
			var bUnit = (y[i]!=0) ? "%" : "";
			if(i!=0 && i!=y.length-1)
			{
				$("#"+yAxisText.id).append("<div style='position: absolute;left:"+(yw-options.scaleLine-30)+"px;bottom:"+(xh+ch*y[i]/(y[y.length-1]-y[0])-0.5)+"px;text-align:right;width:30px'>"+y[i]+bUnit+"</div>");	
			}
			
		}
		
		if($.checkAxisText(x))
		{
			var gridXRealScale = parseFloat(cw/(x.length-1)) //109
			for(var i=0; i<data.length; i++)
			{
				for(var j = 0; j <x.length; j++)
				{
					var xMax = Math.max(data[i][0],x[j])
					if(xMax > data[i][0])
					{
					var xPos = (j>=1) ? j : 1;
					break;
					}
				}
				var pointX = [xPos-1]*gridXRealScale+((data[i][0]-x[xPos-1])/(x[xPos]-x[xPos-1]))*gridXRealScale;
				data[i][0]=pointX;
			}
		}
		if($.checkAxisText(y))
		{
			var gridYRealScale = parseFloat(ch/(y.length-1)) //109
			for(var i=0; i<data.length; i++)
			{
				
				for(var j = 0; j <y.length; j++)
				{
					var yMax = Math.max(data[i][1],y[j])
					if(yMax > data[i][1])
					{
					var yPos = (j>=1) ? j : 1;
					break;
					}
				}
				var pointY = [yPos-1]*gridYRealScale+((data[i][1]-y[yPos-1])/(y[yPos]-y[yPos-1]))*gridYRealScale;
				data[i][1]=pointY;
			}
		}
		if(options.toPercent[1]==0 || options.toPercent[1]==-1 || newData.length==0)
		{
		  t.append("<div style='position:absolute;left:195px; top:60px; width:150px; text-align:center; height:25px; line-height:25px; color:#666; overflow:hidden;'>/*[ID_NO_DATA]*/</div>")
		  return;
		}
		else
		{
			//draw mid line
			var omc = options.midLineColor;	
			var omw	= options.midLineWidth;
			var omv = options.midLine;
			
			var oma = options.midAlpha;
			for(var i =0; i<omv.length; i++)
			{
				if(i==0)
				{
					omv[i] = 100*omv[i]/options.toPercent[1]
				}
				ctx.lineWidth = omw[i][0];
				ctx.strokeStyle = omc[i][0];
				ctx.globalAlpha = oma[i];
				ctx.beginPath();
				ctx.moveTo(omw[i][0],Math.round(ch-ch*((omv[i]-y[0])/(y[y.length-1]-y[0])))+0.5);
				ctx.lineTo(container.width-60,Math.round(ch-ch*((omv[i]-y[0])/(y[y.length-1]-y[0])))+0.5)
				ctx.stroke();
				ctx.lineWidth = omw[i][1];
				ctx.strokeStyle = omc[i][1];
				ctx.beginPath();
				ctx.moveTo(omw[i][0],Math.round(ch-ch*((omv[i]-y[0])/(y[y.length-1]-y[0])))+0.5);
				ctx.lineTo(container.width-60,Math.round(ch-ch*((omv[i]-y[0])/(y[y.length-1]-y[0])))+0.5)
				ctx.closePath();
				ctx.stroke();
				ctx.save();
				ctx.restore();
			}
			ctx.globalAlpha = 1;
	
			//draw line
			var olc = options.lineColor;
			var olw = options.lineWidth;
			var vMax;
			var k = false;
			ctx.strokeStyle = olc[0];
			ctx.lineWidth = olw[0];
			ctx.beginPath();
			for(var i=0; i<newData.length; i++)
			{
				var cd = $.getCurrent(i,newData)
				cd = newData[i][0].getDate()>1 ? cd - 0.5 : cd
	
				var newX = cd/totalDays*cw;
				if(i==0)
				{
					ctx.moveTo(newX+0.5,Math.round(ch-ch*(newData[i][1]-y[0])/(y[y.length-1]-y[0]))+0.5);
				}
				else
				{	
					ctx.lineTo(newX+0.5,Math.round(ch-ch*(newData[i][1]-y[0])/(y[y.length-1]-y[0]))+0.5);
				}
			}
			ctx.stroke();
			ctx.strokeStyle = olc[1];
			ctx.lineWidth =olw[1];
			ctx.beginPath();
			for(var i=0; i<newData.length; i++)
			{
				var cd = $.getCurrent(i,newData)
				cd = newData[i][0].getDate()>1 ? cd - 0.5 : cd
				var newX = cd/totalDays*cw;
				if(i==0)
				{
					ctx.moveTo(newX+0.5,Math.round(ch-ch*(newData[i][1]-y[0])/(y[y.length-1]-y[0]))+0.5);
				}
				else
				{	
					ctx.lineTo(newX+0.5,Math.round(ch-ch*(newData[i][1]-y[0])/(y[y.length-1]-y[0]))+0.5);
				}
			}
			ctx.stroke();
			ctx.save();
			ctx.restore();
			
			//draw max point
			var maxy = 0,ix;
			for(var i=0; i<newData.length; i++)
			{
				maxy = Math.max(maxy,newData[i][1]);
			}
			for(var i=0; i<newData.length; i++)
			{
				if(i==0)
				{
					if(newData[0][1]== maxy)
					{
						ix = i;
					}
				}
				else {
					if(newData[i][1]== maxy && newData[i-1][1]!=maxy)
					{
						ix = i;
					}
					else if(newData[i][1]== maxy && newData[i-1][1]==maxy)
					{
						ix = i-1;	
					}
				}
			}
			var maxd = $.getCurrent(ix,newData)
				maxd = newData[ix][0].getDate()>1 ? maxd - 0.5 : maxd
			var maxX = maxd/totalDays*cw
			var maxY = Math.round(ch-ch*(newData[ix][1]-y[0])/(y[y.length-1]-y[0]))-0.5;
			var opc = options.pointsColor;
			var gradient = ctx.createRadialGradient(maxX,maxY,options.pointsRadius,maxX,maxY,3);
			gradient.addColorStop(0,opc[0]);
			gradient.addColorStop(1,opc[1]);
			ctx.fillStyle = gradient;
			ctx.beginPath();
			ctx.arc(maxX,maxY,options.pointsRadius,0,Math.PI*2,true);
			ctx.fill();
			ctx.save();
			ctx.restore();
		}
	};
	$.canvasForIE = function(ele)
	{
		if(document.all)
		{
			ele = window.G_vmlCanvasManager.initElement(ele);
		}
		return ele;
	};
	$.isHaveParameter = function(obj)
	{
		var i = 0;
		for(var p in obj){
		i++;
		if(i>0){break;}
		}
		if(i>0){
		return true;
		}
		else 
		{
		return false;
		};
	};
	$.getTargetSize = function(t){
		this.width = t.width() || 800;
		this.height = t.height() || 600;
	};
	$.checkAxisText = function(axis)
	{
		if(axis)
		{
			if(document.all)
			{
				if(axis[axis.length-1]==null)
				{
					axis.pop(axis[axis.length-1])
				}
			}
			if(axis.length>=2 && axis[0] != null)
			{
				for(var i =0; i<axis.length; i++)
				{
					if(axis[i]==null)	
					{
						return false;
					}
				}				
				return true;	
			}
		}
		else
		{
			return false;	
		}
	}
})(jQuery);

//把同一天的多个数据保留最大值，其他的值删除
function dataSelect(data)
{
	var arr = []
	for(var i =0; i<data.length; i++)
	{
		//选出一天中有多个点的数组
		var temp = data[i]
		arr[i] = []
		for(var j=0; j<data.length; j++)
		{
			if(data[j][0]==temp[0])
			{
				arr[i].push(j);
			}
		}
		if(arr[i].length>1)
		{
			var t = data[arr[i][0]][1]
			//选出一天中多个点的最大值
			for(var k=0; k<arr[i].length; k++)
			{
				t = Math.max(t,data[arr[i][k]][1]);
			}
			
			//保留一个最大值,把其他相同的数据给替换成空
			var n = 0;
			for(var s=0; s<arr[i].length; s++)
			{
				if(data[arr[i][s]][1] == t)
				{
					n++;
				}
				if(data[arr[i][s]][1]!=t)
				{
					data.splice(arr[i][s],1,"")
				}
				else 
				{
					if(n>1)
					{
						data.splice(arr[i][s],1,"")
					}
				}
			}
			
		}
	}
	
	for(var i=0; i<data.length; i++)
	{
		if(data[i]=="")
		{
			data.splice(i,1);
			i--
		}
	}
	
	return data;
}

function getCurrentDate(n,t){
	var o =  new Date(Date.parse(t));
	var y = o.getFullYear();
	var m = o.getMonth()+1-n;
	var d = o.getDate();
	if(m<=0)
	{
		y = y -1;	
	}
	else if(m>12)
	{
		y = y+1;	
	}
	m = (m<=0) ? 12-Math.abs(m) : m;
	var date = y+"/"+m+"/"+1;
	return date;
}
function getDaysInMonth(year,month) {
	var month = parseInt(month,10)+1;
	var temp = new Date(year+"/"+month+"/0");
	return temp.getDate();
}	
function getData(x,data,status){
//alert(data);
	
	//格式化X坐标时间为date对象,并保存在newDate中
	var newDate = [];
	for(var j=0; j<x.length; j++)
	{	
		newDate.push([new Date(Date.parse(x[j])),0]);
	}
	var xStartDate = newDate[0][0].getTime();
	var xEndTime = newDate[newDate.length-1][0];
	//下行重设数据最后一项为当月最后一天
	xEndTime.setDate(getDaysInMonth(newDate[newDate.length-1][0].getFullYear(),newDate[newDate.length-1][0].getMonth()+1));
	var xEndDate = xEndTime.getTime();
	//处理数据,保证显示数据在X坐标范围之内,超出范围的则不绘制
	var newData = [];
	for(var j=0; j<data.length; j++)
	{	
		newData.push([new Date(Date.parse(data[j][0])),data[j][1]]);  //转换数据时间为Date对象
	}

	var si=0,ei = newData.length;
	for(var j=0; j<newData.length; j++)
	{
		if(newData[j][0].getTime()<xStartDate)
		{
			newData.splice(j,1);
			j--
		}
	}
	for(var j=0; j<newData.length; j++)
	{
		if(newData[j][0].getTime()>xEndDate)
		{
			ei = j;
			break;
		}
	}
	newData = newData.slice(0,ei);

	if(newData.length==0)
	{
		return -1;	
	}
	else
	{
		var m = 0,a=0,mt;
		for(var i =0; i<newData.length; i++)
		{
			m = Math.max(newData[i][1],m);
			a += newData[i][1];
		}
		
		for(var i =0; i<newData.length; i++)
		{
			if(newData[i][1]==m)
			{
				mt = newData[i][0].getFullYear()+"."+(newData[i][0].getMonth()+1)+"."+newData[i][0].getDate()+"  "
				+newData[i][0].getHours()+":"+newData[i][0].getMinutes()+":"+newData[i][0].getSeconds();
			}
		}
		
		if(status==0)
		{
			return a/newData.length;//返回平均值
		}
		else if(status==1)
		{
			return m; //返回最大值
		}
		else if(status==2)
		{
			return mt; //返回最大值时间
		}
	}
}
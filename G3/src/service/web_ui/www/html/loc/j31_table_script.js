//<!-- Copyright (c) 2020, Vertiv Tech Co. Ltd. by maofuhua-->
//var isIE = (navigator.userAgent.toLowerCase().indexOf("msie") != -1) ? true : false;
var isIE = true;
var isNS = !isIE;
var appVer = parseFloat(navigator.appVersion);

//
//TBALE function need IE 4.0, Netscape 6.0 or later
//

function getElement( id ) {
	var x;

	if (document.getElementById )
		x = document.getElementById(id);
	else if (document.all)
		x = document.all[id];
	else if (document.layers ) {
		x = document.layers[id];
	}
	
	return x;
}

function getTableCell( t, r, c ) {
	var tr = t.rows.item(r);
	if( tr != null )
		return tr.cells.item(c);
	return null;
}

//////////
// for Internet Explorer 4
//
function getTableRow_IE( t, r) {
	return t.rows.item(r);
}

function getRowCell_IE( r, c ) {
	return r.cells.item(c);
}

function insertCell_IE( r, c ) {
   return r.insertCell(c);
}

function insertRow_IE( t, nr ) {
   return t.insertRow(nr);
}

function deleteRow_IE( t, nr ) {
	var r = getTableRow(t, nr);
	if( r != null ) {
		t.deleteRow( nr );
		return true;
	}
	return false;
}

function setCell_IE(r, idx, value, clr, align, bkgrd, bdr ) {
	var c = getRowCell( r, idx );
	if( c == null )	{
		c = insertCell( r, idx );
	}

	if( c.style.color != clr )
		c.style.color = clr;

	if( c.style.background != bkgrd )
		c.style.background = bkgrd;
		c.style.fontSize ="10pt";
	if( c.last == null ) 
	{
		c.style.borderRight = bdr;
		c.style.borderBottom = "#f9f9f9 solid 1px";
		c.align = align;
	}
	
	//if( c.last != value ) {
		c.innerHTML = value;
		c.last = value;
		 
	//}
	

	return c;
}
//
// end for Internet Explorer 4
//////////

//////////
// for Netscape 6
//
function getTableRow_NS( t, r ) {
	var rows = t.getElementsByTagName( "TR" );
	var tr = rows != null ? rows.item(r) : null;

	return tr;
}


function getRowCell_NS( r, c ) {
	var cells = r.getElementsByTagName( "TD" );
	var td = cells != null ? cells.item(c) : null;
	return td;
}


function insertCell_NS( r, c ) {
   var cell = document.createElement("TD");
   r.appendChild(cell);

   return cell;
}

function insertRow_NS( t, nr ) {
   var row = document.createElement("TR");
   t.appendChild(row);

   return  row;
}


function deleteRow_NS( t, nr ) {
	var r = getTableRow(t, nr);
	if( r != null ) {
		t.removeChild( r );
		return true;
	}
	return false;
}

function setCell_NS(r, idx, value, clr, align, bkgrd, bdr ) {
	var c = getRowCell( r, idx ); // r.cells(idx);
	if( c == null )	{
		c = insertCell( r, idx );//r.insertCell(idx);
	}

	if( c.style.color != clr )
		c.style.color = clr;

	if( c.last == null ) {
		c.style.border = bdr;
		c.align = align;
		c.style.background = bkgrd;
	}
	//c.style.background = bkgrd;
	//alert(c.last);
	//alert(value);
	if( c.last != value ) {
		c.innerHTML = value;
		c.last = value;
	}
	
	
	return c;
}

//
// end for Netscape 6
///////////

var getTableRow  = isIE ? getTableRow_IE  : getTableRow_NS;
var getRowCell   = isIE ? getRowCell_IE   : getRowCell_NS;
var insertCell   = isIE ? insertCell_IE   : insertCell_NS;
var insertRow    = isIE ? insertRow_IE    : insertRow_NS;
var deleteRow    = isIE ? deleteRow_IE    : deleteRow_NS;
var setCell      = isIE ? setCell_IE      : setCell_NS;

var oldAlarmNum = 0;
function deleteTableRows( t, fromRow ) {
	while( deleteRow(t, fromRow) );
	var alarmTable = getElement("ralarmTab");
	if(alarmTable)
	{
		var	alarmList = alarmTable.getElementsByTagName("tr");
		var alarmStatus = getElement("alarmValue");
		if(alarmStatus)
		{
			if(alarmList.length<2)
			{
				alarmStatus.value = 0;
			}
		}
		oldAlarmNum = alarmList.length;
		
	}
}

///// end public table api

///////////
// internal functions
var NBSP = "-";
var TIME_SLOW_THRESHOLD  = 2500; /* ms */
var TIME_QUICK_THRESHOLD = 2000;
var ROWS_PER_UPDATE = 5;
 

var newRow =0;
function setRow( tab, dataCvrt, totalRows, curRow, colToUpdate, startRow,makeRow, iAlarmType ) 
{

	if(newRow == 2 || curRow == 0)
	{
	       newRow = 0; 
	}
	
	var r, t = tab.table;
	r = getTableRow( t, makeRow+1 + startRow ); //there is a headline, so curRow+1
		
	if( r == null )
		r = insertRow( t, makeRow+1 + startRow );

	var colFrom, colTo;
	if( colToUpdate != null ) {	// to update one col
		colFrom = colToUpdate;
		colTo   = colToUpdate+1;
	}
	else {	// update all cols of data item
		colFrom = 0;
		colTo   = tab.subCols;
	}
	
	var bkgrd = tab.backColor[curRow%2+1];
	var varAlign ;//
	
	
	
	
	
	var mc, sc, di;
	
	
	for( mc = 0; mc < tab.mainCols; mc ++ ) 
	{
		if(mc != totalRows)
		{
			//alert(curRow);
			di = dataCvrt( curRow + totalRows*mc );
			

			
			
			if(iAlarmType == 1)//alarm
			{		
				for( sc = colFrom; sc < colTo; sc ++ ) 
				{
					if(sc == 1)
					{
						if(di != null && di[3] <= 2 &&  di[3] >= 0)
						{
							//bkgrd = tab.backColor[di[3] + 1];
							if(curRow%2==0)
							{
								
								bkgrd = "#b8cce4 url('img/listAlarm.gif') 90% 3px no-repeat";
								
							}
							else
							{
								
								bkgrd = "#dce6f1 url('img/listAlarm2.gif') 90% 3px no-repeat";
							}
								
						}
						else
						{
							bkgrd = tab.backColor[1];
						}
					}
					else
					{
						//bkgrd = tab.backColor[4];
						if(curRow%2==0)
						{
							
							bkgrd = "#b8cce4";
						}
						else
						{
							bkgrd = "#dce6f1";
						}
						//delete last col border
						if((sc+1)%colTo==0)
						{
							tab.borderStyle = "0px solid #f9f9f9"; //no border	
						}
						else
						{
							tab.borderStyle  = "1px solid #f9f9f9";
						}
						
						
					}
					
					if(sc == 0 )
					{
						varAlign="" + "center";
					}
					else
					{
						varAlign="" + "left";
					}
					
					setCell( r, mc*tab.subCols + sc, (sc == 0) ? (makeRow + 1) : di[sc*2],//(di ? di[sc*2] : NBSP) ,//
						tab.textColor[di ? di[sc*2+1] : 1],
						varAlign, bkgrd, tab.borderStyle );
				}
			}
			else
			{
				
				for( sc = colFrom; sc < colTo; sc ++ ) 
				{
					/*
					if(sc == 1 || sc == 3 || sc == 4)		//Name && Unit Align = right 
					{
						varAlign="" + "left";
					}
					else if(sc == 0 )
					{
						varAlign="" + "center";
					}
					else		//else Align = right
					{
						varAlign="" + "left";
					}
					*/
					if(sc%colTo==0){
						varAlign = "" + "center";
					}
					else
					{
						varAlign = "" + "left";
					}
					if(di[sc*2+1] == 4)			//invalidate signal
					{
						bkgrd = "#f0f0f0";
					}
					else
					{
						//bkgrd = "#f0f0f0";
						if(newRow%2==0)
						{
							
							bkgrd = "#b8cce4";
						}
						else
						{
							bkgrd = "#dce6f1";
						}
						//delete last col border
						if((sc+1)%colTo==0)
						{
							tab.borderStyle = "0px solid #f9f9f9"; //no border	
						}
						else
						{
							tab.borderStyle  = "1px solid #f9f9f9";
						}
						

						//bkgrd = tab.backColor[1];
					}
					
					setCell( r, mc*tab.subCols + sc, (sc == 0) ? (makeRow + 1) : di[sc*2],//(di ? di[sc*2] : NBSP) ,//
						tab.textColor[di ? di[sc*2+1] : 0],
						varAlign, bkgrd, tab.borderStyle );
				}
				
				newRow++;
			}
			
			
		}
		else
		{
			setCell( r, mc*tab.subCols + sc, (NBSP),
						tab.textColor[0],"center", bkgrd, tab.borderStyle );
		}
	}
	
		var alarmStatus = getElement("alarmValue");
	var alarmTable = getElement("ralarmTab");
	if(alarmTable && alarmStatus)
	{
		alarmStatus.value = 1;
		var	alarmList = alarmTable.getElementsByTagName("tr");
		if(alarmList.length>oldAlarmNum)
		{
			alarmStatus.value = 2;
		}
		
		if(alarmList.length>1 && alarmList.length==oldAlarmNum)
		{
			alarmStatus.value = 1;
		}
		oldAlarmNum = alarmList.length;	
	}
	


	return mc; // the data number drawn in this row
}

function createTableHead( tab, colNames, colWidth ) {
	var r = getTableRow( tab.table, 0 ); // head
	if( r == null ) {
		r = insertRow( tab.table, 0 );
		if( r == null )
		    return false;
	}

	var cc, c, cols = tab.mainCols * tab.subCols;
	
	//setCell(r,0,"�豸���",tab.textColor[0], 
	//	    "center", tab.backColor[0], tab.borderStyle );
	
	
	
	for( cc = 0; cc < cols; cc ++ ) {
		c = setCell( r, cc, colNames[cc%tab.subCols], "white",//tab.textColor[0], 
		    "center", tab.backColor[0], tab.borderStyle );
		if( c == null )
		    return false;
		//c.style.fontWeight = "bold";
		//c.style.color="8pt";
		//c.style.fontsize="1pt";
		
		c.style.borderBottom = "1px solid #f9f9f9";
		c.style.fontSize = "12px";
		c.style.textAlign ="left";
		if(cc==0){
			c.style.textAlign ="center";		
		}
		
		if((cc+1)%cols==0)
		{
			c.style.borderRight = "0px solid #f9f9f9";
		}
		else
		{
			c.style.borderRight = "1px solid #f9f9f9";
			
		}
		
		
		c.width = colWidth[cc%tab.subCols];
	}

	return true;
}

/* fit the slower computer. */
function updateTableSlowMethod(tab, dataLength, colToUpdate, startRow, dataCvrt,iAlarmType ) {
	if( tab.updating )
	{
		//alert("updateTableSlowMethod");
		return;
	}
	tab.updating = true;
	var startTime = new Date().getTime();

	if( startRow == null )
	    startRow = 0;

	var i = 0, n, curRow = 0;
	var sigRows = Math.ceil(dataLength*1.0/tab.mainCols);
	
	var iUpdateColumn = 0;
	this.setRowA = function thisSetRow() 
	{
		
		if( i < dataLength ) 
		{
			for( n = 0; n < ROWS_PER_UPDATE && i < dataLength; n ++, curRow ++ ) 
			{
				if(dataCvrt(curRow) != -1)
				{
				//	alert(dataCvrt(curRow));
				//	alert("startRow " + startRow);
				//	alert("sigRows " + sigRows);
				//	alert("curRow " + curRow);
				//	alert("i" + i);
				//	alert("tab." + tab.mainCols + "  tab.subCols" + tab.subCols);
					//alert("iUpdateColumn  " + iUpdateColumn);
					i += setRow( tab, dataCvrt, sigRows, curRow, colToUpdate, startRow, iUpdateColumn,iAlarmType);
					
					iUpdateColumn++;
				}
				else
				{
					i ++;//= setRow( tab, dataCvrt, sigRows, curRow, 0, startRow );
				}
				
			}
			
			window.setTimeout( "this.setRowA();", 0 );
		}
		else 
			window.setTimeout( "this.removeSpilth();", 0 );
	}

	this.removeSpilth = function thisRemoveSpilth() {
		// delete the left rows if type changed
		curRow ++;
		curRow += startRow;
		if( colToUpdate == null )
			deleteTableRows(tab.table, curRow);

		var elapsed = new Date().getTime() - startTime;
		if(elapsed < TIME_QUICK_THRESHOLD)
			tab.quickSpeed = true;

		tab.updating = false;
	}

	window.setTimeout( "this.setRowA();", 0 );
}

/* fit the quicker computer. */
function updateTableQuickMethod(tab, dataLength, colToUpdate, startRow, dataCvrt, iAlarmType ) {

      

	if( tab.updating )
		return;
	tab.updating = true;
	var startTime = new Date().getTime();

	if( startRow == null )
	    startRow = 0;

	var i, curRow = 0;
	var sigRows = Math.ceil(dataLength*1.0/tab.mainCols);
	
	var iUpdateColumn = 0;
	for( i = 0; i < dataLength ; curRow ++) 
	{
		if(dataCvrt(i) != -1)
		{
			i += setRow( tab, dataCvrt, sigRows, curRow, colToUpdate, startRow, iUpdateColumn, iAlarmType);
			iUpdateColumn++;
		}
		else
		{
			i++;
		}
	}
	// delete the left rows if  type changed
	curRow ++;
	curRow += startRow;
	if( colToUpdate == null ) 
	{
		deleteTableRows(tab.table, curRow);
	}

	var elapsed = new Date().getTime() - startTime;
	if(elapsed > TIME_SLOW_THRESHOLD )
		tab.quickSpeed = false;

	tab.updating = false;
}

// end internals
///////////


///////////
// global functions

// create the table object, but the table must can be found in HTML file!
function createTable( tabName, mainCols, headerNames, colWidths,
	    textColor, backColor, borderStyle,iType ) {
	if( (headerNames.length < 1) || (colWidths.length != headerNames.length) || (mainCols < 1) )
	    return null;
	
	var tabBody = getElement( tabName ); //the handle of table
	if( tabBody == null )
	    return null;

	deleteTableRows(tabBody, 0);

 
	var defTxtClr;
	var defBkClr ;
	if(iType == 1)		// alarm
	{
		
		defTxtClr   = new Array( "black", "black",  "black","black","black");//"#ffcc00",  "#cc3366", "red" );
		defBkClr    = new Array( "#3d85c4","yellow", "#cc3366", "red","white" );//( "#0099CC", "white", "#f0f0f0" );
	}
	else				// not alarm
	{
		defTxtClr   = new Array( "white", "blue",  "#4d4d4d",  "red", "#999999" );
		defBkClr    = new Array( "#3d85c4", "white", "white");//( "#0099CC", "white", "#f0f0f0" );
	}
	
	
	var defBdrStyle = "1px solid #f9f9f9";

        //var defBdrStyle = " 8pt #000066 ";
        
	var tabObj = new Object;

	tabObj.tableName  = tabName;
	tabObj.table	  = tabBody;
	tabObj.mainCols   = mainCols;
	tabObj.subCols	  = headerNames.length;
	tabObj.quickSpeed = false;
	tabObj.updating   = false;
	
	
	tabObj.textColor  = (textColor) ? textColor : defTxtClr;
	tabObj.backColor  = (backColor) ? backColor : defBkClr;
	tabObj.borderStyle= (borderStyle) ? borderStyle : defBdrStyle;
	
	if( !createTableHead( tabObj, headerNames, colWidths ) )
	    tabObj = null;
	

	return tabObj;		
}

function updateTable( tab, dataLength, colToUpdate, startRow, dataCvrt,iAlarmType ) {
	 if( tab.quickSpeed )
		updateTableQuickMethod( tab, dataLength, colToUpdate, startRow, dataCvrt,iAlarmType );
	 else
	 	updateTableSlowMethod( tab, dataLength, colToUpdate, startRow, dataCvrt, iAlarmType );
}

//2011.10.18
//formatTable

function tableFormat(obj) {
	var trlist = document.getElementById(obj).getElementsByTagName("tr");
	var tdlist = document.getElementById(obj).getElementsByTagName("td");

	var nowRows = trlist.length;
	var nowCols = tdlist.length/trlist.length;

	/*
	var oldBackground = null;
	var oldColor = null;
	for(var i=0; i<nowRows;i++)
	{
		trlist[i].onmouseover = function(){
			oldBackground = this.style.background;
			oldColor = this.style.color;
			this.style.background = "#ff9600";
			this.style.color = "#fff";
			
		}
		trlist[i].onmouseout = function(){
		
			this.style.background = oldBackground;
			this.style.color = oldColor;
		}
	
	}
	*/
		for(var i=0; i<nowRows; i++)
		{
		
			trlist[i].style.borderRight ="#f9f9f9 solid 1px";
			
			trlist[i].style.textAlign = "left";
			if(i%2==0){
			trlist[i].style.background ="#dce6f1";
			}
			else
			{
			trlist[i].style.background ="#b8cce4";
			
			}
			
			
		}
		//reset firstTr
		trlist[0].style.background = "#3d85c4";
		
		for(var j=0; j<tdlist.length; j++){
		
			tdlist[j].style.borderRight = "#f9f9f9 solid 1px";
			tdlist[j].style.borderBottom = "#f9f9f9 solid 1px";
			tdlist[j].style.fontWeight = "bold";
			tdlist[j].style.textAlign = "left";
			tdlist[j].style.textIndent = "10px";
			//first td
			if(j<=nowCols-1)
			{
				tdlist[j].style.color = "#fff";
				tdlist[j].style.borderBottom = "#f9f9f9 solid 1px";
			}

			if(j%nowCols==0)
			{
				tdlist[j].style.textAlign = "center";
				tdlist[j].style.textIndent = "0px";
			}

			//last td
			if((j+1)%nowCols==0)
			{	
				
				tdlist[j].style.borderRight = "none";
				tdlist[j].style.color = "#fff";
				
			}
			
		
		}
	


}



// end globals
//////////////

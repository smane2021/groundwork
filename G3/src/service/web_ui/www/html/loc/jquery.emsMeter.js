
//
//
(function($){
	$.defaultParameter = {
		title			: null,	 			//线条的标题
		value 			: [0],				//当前指针指向
		meterValue		: null,				//表盘的数值
		meterTextClass	: "meterText",		//表盘数值的样式名称
		canvasClass		: "meterCanvas",  	//画布样式名称
		outScale		: false,			//是否显示外围刻度、边框及数值
		outSpace  		: 4,				//刻度距离外边框的距离,所设值的大小下面的smallScale
		scaleWidth 		: 2, 				//小刻度的宽度
		scaleSmallColor : "#333", 			//小刻度的颜色
		smallScale  	: 10,				//小刻度的线长的长度
		scaleBigColor	: "#333",			//大刻度的颜色
		bigScale  		: 15,				//大刻度的线长的长度	
		outLineColor	: "#325fa2",		//外线框的颜色
		fillColor 		: "#fff",		    //外线框所包范围的填充颜色
		radius			: 180,				//外线框的半径
		outLineWidth	: 10,				//外线框的线条宽度
		originWidth		: 1,				//原点半圆的线条宽度
		originRadius	: 2,				//原点半圆的半径
		originFill  	: true,				//原点半圆是否需要填充
		originFillColor : "#333",			//原点半圆的线条及内填充颜色
		originBottom	: 15,				//原点距离DIV底部的高度
		finger	  		: 100, 				//指针距离外边框的距离,越小说明指针越靠近边框
		fingerNum		: 1,				//指针的个数，默认为1个
		fingerWidth 	: 2,				//指针的线条宽度
		fingerColor 	: ["#333","#333"],	//指针的线条颜色
		alarmValue		: null,				//警示区间数据
		alarmRadius		: [90,50],			//警示区域显示半径
		alarmColor		: ["#f26a26","#f8cc38","#b7d548","#f8cc38","#f26a26"],	//警示区间默认显示颜色,默认为五个,可在前端自行设置或更改
		alarmHide		: [],				//默认为全部显示,可在前端设置不显示的警示值的序号（即alarmValue数组的序号）
		alarmUnit		: "",
		toRotate		: 0,				//逆时针旋转角度
		centerText		: false,				//表盘中心是否显示指针值，默认不显示
		toPercent		: [false,1]			
		
	};
	$.setParameter = function(opts){
		if(opts)
		{
			this.title  	 		= opts.title 		    ? opts.title 		    : $.defaultParameter.title;
			this.value				= opts.value			? opts.value			: $.defaultParameter.value;			
			this.canvasClass 		= opts.canvasClass  	? opts.canvasClass    	: $.defaultParameter.canvasClass;
			this.outScale			= opts.outScale			? opts.outScale			: $.defaultParameter.outScale;
			this.scaleSmallColor	= opts.scaleSmallColor	? opts.scaleSmallColor	: $.defaultParameter.scaleSmallColor;
			this.scaleBigColor		= opts.scaleBigColor	? opts.scaleBigColor	: $.defaultParameter.scaleBigColor;
			this.scaleWidth			= opts.scaleWidth		? opts.scaleWidth		: $.defaultParameter.scaleWidth;
			this.outLineColor		= opts.outLineColor		? opts.outLineColor     : $.defaultParameter.outLineColor;
			this.fillColor			= opts.fillColor		? opts.fillColor		: $.defaultParameter.fillColor;
			this.radius      		= opts.radius		  	? opts.radius			: $.defaultParameter.radius;
			this.outLineWidth		= opts.outLineWidth		? opts.outLineWidth		: $.defaultParameter.outLineWidth;
			this.meterTextClass 	= opts.meterTextClass 	? opts.meterTextClass   : $.defaultParameter.meterTextClass;
			this.originWidth		= opts.originWidth		? opts.originWidth		: $.defaultParameter.originWidth;
			this.originRadius		= opts.originRadius		? opts.originRadius		: $.defaultParameter.originRadius;
			this.originFill			= opts.originFill==false? opts.originFill		: $.defaultParameter.originFill;
			this.originFillColor	= opts.originFillColor	? opts.originFillColor	: $.defaultParameter.originFillColor;
			this.originBottom 		= opts.originBottom 	? opts.originBottom   	: $.defaultParameter.originBottom;
			this.outSpace			= opts.outSpace  		? opts.outSpace  		: $.defaultParameter.outSpace;
			this.smallScale	  		= opts.smallScale   	? opts.smallScale     	: $.defaultParameter.smallScale;
			this.bigScale     		= opts.bigScale     	? opts.bigScale       	: $.defaultParameter.bigScale;
			this.finger				= opts.finger			? opts.finger			: $.defaultParameter.finger;
			this.fingerNum			= opts.fingerNum		? opts.fingerNum		: $.defaultParameter.fingerNum;
			this.fingerWidth		= opts.fingerWidth      ? opts.fingerWidth		: $.defaultParameter.fingerWidth;
			this.fingerColor		= opts.fingerColor		? opts.fingerColor		: $.defaultParameter.fingerColor;
			this.meterValue			= opts.meterValue		? opts.meterValue		: $.defaultParameter.meterValue;
			this.alarmValue			= opts.alarmValue		? opts.alarmValue		: $.defaultParameter.alarmValue;
			this.alarmRadius		= opts.alarmRadius		? opts.alarmRadius		: $.defaultParameter.alarmRadius;
			this.alarmColor			= opts.alarmColor		? opts.alarmColor		: $.defaultParameter.alarmColor;
			this.alarmHide		 	= opts.alarmHide		? opts.alarmHide		: $.defaultParameter.alarmHide;
			this.alarmUnit		 	= opts.alarmUnit		? opts.alarmUnit		: $.defaultParameter.alarmUnit;
			this.toRotate			= opts.toRotate			? opts.toRotate			: $.defaultParameter.toRotate;
			this.centerText			= opts.centerText==true	? opts.centerText		: $.defaultParameter.centerText;
			this.toPercent			= opts.toPercent 		? opts.toPercent		: $.defaultParameter.toPercent;
		}
	}; 
	$.emsMeter = function(target,options)
	{		
		var t = $("#"+target);
		t.css({"position":"relative"});//强制设置成relative
		var tName = target;
		if(options && $.isHaveParameter(options))
		{
			options = new $.setParameter(options);
		}
		else
		{
			options = $.defaultParameter;
		};
		$.drawMeter(t,tName,options);
	};
	$.drawMeter = function(t,name,options)
	{
		
		for(var i=0; i<options.value.length; i++)
		{
			if(options.value[i]==-1)
			{
				return;	
			}
		}
		for(var j=0; j<options.alarmValue.length; j++)
		{
			if(options.alarmValue[j]==-1)
			{
				return;
			}
		}
		//初始化DIV(清除上贞绘制的内容)
		var cnum = $(t).children();
		if(cnum.length>0)
		{
			cnum.remove();
		};
		
		
		if(options.toPercent[1]==0)
		{
			return 	
		}
		
		if(options.toPercent[0]==true)
		{
			for(var i=0; i<options.value.length; i++)
			{
				options.value[i] = 100*options.value[i]/options.toPercent[1];
			}
		}
		
		
		var container = new $.getTargetSize(t);
		var w = container.width;
		var h = container.height;
		var m = options.meterValue;
		var av = options.alarmValue;
		var b = options.originBottom;
		var r = options.radius;
		var rotate = options.toRotate;
		var outshow = options.outScale;
		var ar = options.alarmRadius;
		//指针指数
		var sn = m.length;
		var value = new Array();
		for(var i =0; i<options.fingerNum; i++)
		{
		   for(var j= 0; j<options.value.length; j++)
		   {
		   		if(options.value[j]>=m[0] && options.value[j]<=m[sn-1])
				{
					value[i] = -(m[sn-1]-options.value[i])* (30/(m[sn-1]-m[0]));
				}
				else if(options.value[j]<m[0])
				{
					value[i] = -(m[sn-1]-m[0])* (30/(m[sn-1]-m[0]));
				}
				else if(options.value[j]>m[sn-1])
				{
					value[i] = -(m[sn-1]-m[sn-1])* (30/(m[sn-1]-m[0]));
				}
		   }
		}
		

		
		//生成画布,设置id,大小,样式
		var canvas = document.createElement("canvas");
		$.canvasForIE(canvas);
		//canvas.id = name+"_canvas";
		t.append(canvas);
		$(canvas).addClass(options.canvasClass);
		$(canvas).attr({"width":w,"height":h});
		var ctx = canvas.getContext('2d'); 
		
		//初始化上下文对象,清理画布,确定原点坐标为正中央(默认)
		ctx.save();  
		ctx.clearRect(0,0,w,h); 
		if(rotate==0)
		{
		ctx.translate(w/2,h-b); 
		}
		else if(rotate==90)
		{
			ctx.translate(w-b,h/2); 	
		}
		ctx.lineCap = "round";
		ctx.rotate(-rotate*Math.PI/180);  
		
		//画外框的半圆,并设置阴影
		if(outshow==true)
		{
			ctx.lineWidth = options.outLineWidth; 
			ctx.strokeStyle = options.outLineColor;
			ctx.fillStyle = options.fillColor;
			ctx.beginPath();  
			//ctx.arc(0,0,options.radius,-25,Math.PI+25,true); //直接用数值计算,不好控制
			ctx.arc(0,0,options.radius,5*Math.PI/180,175*Math.PI/180,true); //转换成角度计算
			ctx.closePath();
			ctx.stroke(); 
			ctx.fill();
		}
		ctx.save();
		if(outshow==true)
		{
			//画小刻度
			ctx.strokeStyle = options.scaleSmallColor;
			ctx.lineWidth = options.scaleWidth;  
			for (i=0;i<=(sn*(sn-1));i++){  
				if (i%sn!=0) {  
					ctx.beginPath(); 
					ctx.moveTo(options.radius-options.smallScale,0);  
					ctx.lineTo(options.radius-options.outSpace,0);  
					ctx.stroke();  
				};
				ctx.rotate(-Math.PI/(sn*(sn-1)));  
			}; 
			ctx.save(); 
			ctx.restore();
		}
		
		
		if(outshow==true)
		{
			//画大刻度 
			ctx.strokeStyle = options.scaleBigColor;
			ctx.lineWidth = options.scaleWidth* (options.scaleWidth<=1 ? 2 : 1.5); 
			//画两端的大刻度
			ctx.beginPath(); 
			ctx.rotate(Math.PI);
			ctx.moveTo(options.radius-options.bigScale,0);  
			ctx.lineTo(options.radius-options.outSpace,0); 
			//画中间的大刻度
			for (var i=0;i<=(sn>2 ? sn-2 : sn-1);i++){  
				ctx.rotate(Math.PI/(sn-1));
				ctx.moveTo(options.radius-options.bigScale,0);  
				ctx.lineTo(options.radius-options.outSpace,0);  
				ctx.stroke(); 
			};
			ctx.save();
			ctx.restore();
		}
		
		//警示区间
		if($.isHaveParameter(av))
		{
			ctx.strokeStyle = "#666";
			ctx.lineCap = "butt";
			 for(var i=0; i<100; i++)
			 {
				ctx.rotate(100*i*Math.PI/180);
				var gradient = ctx.createRadialGradient(0,240,Math.PI*i,0,0,0);
				gradient.addColorStop(1,'rgba(200,200,200,1)');
				gradient.addColorStop(0,'rgba(230,230,230,1)');
			}
			ctx.fillStyle = gradient;
			ctx.lineWidth = 3;
			ctx.beginPath(); 
			ctx.arc(0,0,ar[0]+5,3*Math.PI/180,177*Math.PI/180,true);
			ctx.stroke();
			ctx.fill();
			ctx.save();
			var optsAlarm = new Array();
			for(var i=0; i<av.length; i++)
			{
				optsAlarm[i] = av[i]<m[0] ? m[0] : av[i];
				optsAlarm[i] = av[i]>m[sn-1] ? m[sn-1] : av[i];
				optsAlarm[i] = -(m[sn-1]-av[i])* (30/(m[sn-1]-m[0]));
				ctx.fillStyle = options.alarmColor[i] ? options.alarmColor[i] : $.defaultParameter.alarmColor[i];
				ctx.beginPath(); 
				ctx.moveTo(0,0);
				i== 0 ? ctx.arc(0,0,ar[0],Math.PI,optsAlarm[i]*Math.PI/30,false) : ctx.arc(0,0,ar[0],optsAlarm[i-1]*Math.PI/30,optsAlarm[i]*Math.PI/30,false)
				ctx.fill();
				ctx.save();
			};
			var nr1 = ar[0];
			var nr2 = ar[1];
			var oy = ar[1]*Math.sin(6*Math.PI/180)-1;
			ctx.fillStyle="#bbb";
			ctx.lineWidth = 2;
			ctx.beginPath(); 
			ctx.arc(0,0,nr2+6,5*Math.PI/180,175*Math.PI/180,true);
			ctx.fill();
			
			ctx.beginPath(); 
			ctx.moveTo(nr2,oy);
			ctx.lineTo(ar[0]+6,oy)
			ctx.stroke();
			
			ctx.beginPath(); 
			ctx.moveTo(-ar[0]-6,oy);
			ctx.lineTo(-ar[1],oy)
			ctx.stroke();
			
			ctx.strokeStyle = "#666";//options.fillColor; 
			ctx.fillStyle = (options.toRotate==90) ? "#eff1f4" : "#e2e6ec";

			ctx.beginPath(); 
			ctx.arc(0,0,nr2,6*Math.PI/180,174*Math.PI/180,true);
			ctx.stroke();
			ctx.beginPath(); 
			ctx.arc(0,1,nr2,8*Math.PI/180,172*Math.PI/180,true);
			ctx.fill();
			ctx.save(); 
			ctx.restore();
		};
		ctx.globalCompositeOperation = "source-over";
		//画内圆中心点
		ctx.save(); 
		ctx.lineCap = "butt";
		ctx.lineWidth = options.originWidth; 
		ctx.strokeStyle = options.originFillColor;
		ctx.beginPath(); 
		ctx.arc(0,0,options.originRadius,0,Math.PI*2,true);
		if(options.originFill==true)
		{
			ctx.closePath();
			ctx.fillStyle = options.originFillColor;
			ctx.fill();
		};
		ctx.stroke();
		ctx.save();  
		//画指针

		for(var i=0; i<options.fingerNum; i++)
		{
			ctx.lineWidth = options.fingerWidth;  
			ctx.strokeStyle = options.fingerColor[i];  
			ctx.beginPath(); 
			ctx.rotate(value[i]* Math.PI/30); 
			ctx.moveTo(options.originRadius,0);  
			ctx.lineTo(options.radius-options.finger,0);
			//arrow
			ctx.moveTo(options.radius-options.finger-8,-2.5);
			ctx.lineTo(options.radius-options.finger-8,2.5);
			ctx.lineTo(options.radius-options.finger,0);
			ctx.closePath();
			ctx.stroke();  
			ctx.restore(); 
			ctx.save();
		}
		//中心数值
		if(options.centerText == true)
		{
			t.append("<div id='centerText' style='position:absolute;left:"+(options.alarmRadius[0]+options.alarmRadius[1]+20)+"px;top:"+(options.alarmRadius[0]+options.alarmRadius[1]-7)+"px;font-size:12px;font-family:Arial;font-weight:bold;color:#0066cc;cursor:pointer;' title='"+options.value[0].toFixed(1)+"'>"+options.value[0].toFixed(1)+"</div>");
		}
		
		
		var ew = new Array();
		var eh = new Array();
		var aText = new Array();
		var L,T;
		if(outshow==true)
		{
		//刻度数值	
		var midNum  = (sn%2==1) ? (sn-1)/2 : sn/2; //计算居中序数,以0开始
		for(var i =0; i<sn; i++)
		{
			aText[i] = document.createElement("div");
			t.append(aText[i]);	
			$(aText[i]).html(m[i].toPrecision());
			$(aText[i]).addClass(options.meterTextClass);
			ew[i] = $(aText[i]).outerWidth(true);
			eh[i] = $(aText[i]).outerHeight(true);
			dgre = ((180/(sn-1))*i*Math.PI)/180; 
			//alert(180*Math.asin(0.5)/Math.PI)	//利用数值计算,0.5的反正弦值
			//alert(Math.sin(30*Math.PI/180)) //利用角度计算,30度的正弦值
			sw = options.bigScale + options.outSpace;
			var Lx,Ty;
			if(rotate==0)
			{
				L = (w/2-r*Math.cos(dgre))+sw*Math.cos(dgre);
				T = h-b-(r-sw)*Math.sin(dgre);
				if(sn%2==1 ? i<midNum : i<midNum-1)
				{
					Lx = 0;	
					Ty = 8;
				}
				else if(sn%2==1 ? i==midNum : (i==midNum-1 || i==midNum))
				{
					Lx = sn>2 ? ew[i]/2 : ( i==0 ? 0 : ew[i]);
					Ty = sn>2 ? 0 : 8;
				}
				else
				{
					Lx = ew[i];
					Ty = 8;
				};
				$(aText[i]).css({"left":L-Lx,"top":T-Ty});
			}
			else if(rotate == 90)
			{
				r = options.radius +15;
				b = b;
				m = m;
				var mid = (m[m.length-1]-m[0])/2+m[0];
				if(m[i]<mid)
				{
					L = w - b - r*Math.sin(dgre)-ew[i]/2;	
					T = h/2+r*Math.cos(dgre)-eh[i]/2
				}
				else if(m[i]>mid)
				{
					L = w - b -Math.abs(r*Math.sin(Math.PI-dgre))-ew[i]/2;
					T = h/2-Math.abs(r*Math.cos(Math.PI-dgre))-eh[i]/2;
				}
				else
				{
					L = w - b - r-ew[i]/2;
					T = h/2-eh[i]/2;
				}
				$(aText[i]).css({"left":L,"top":T});
			}
		};
		}
		ctx.save();
		ctx.restore();
		
		//警示数值
		if($.isHaveParameter(av))
		{
			av.unshift(m[0])
			var s = 5;
			ar[0] +=12;
			sh = new Array();
			for(var i =0; i<av.length; i++)
			{
				aText[i] = document.createElement("a");
				$(aText[i]).attr({"href":"javascript:void(0)","title":av[i]})
				t.append(aText[i]);	
				var sunit = av[i] ==0 ? "" : options.alarmUnit;
				$(aText[i]).html(av[i].toPrecision()+sunit);
				$(aText[i]).addClass(options.meterTextClass);
				ew[i] = $(aText[i]).outerWidth(true) ;
				eh[i] = $(aText[i]).outerHeight(true);
				var aL,aT;
				var d = (av[i]-av[0])/(av[av.length-1]-av[0])*180*Math.PI/180;
				var mid = (av[av.length-1]-av[0])/2+av[0];
				if(rotate==0)
				{
					if(i==0)
					{
						aL = w/2 - ar[0]-ew[0];
						aT = h - b-eh[0]+3;
					}
					else if(i==av.length-1)
					{
						aL = w/2 + ar[0];
						aT = h - b-eh[0]+3;
					}
					else
					{
						if(av[i]<mid)
						{
							aL = w/2 - ar[0]*Math.cos(d)-ew[i];	
							aT = h - ar[0]*Math.sin(d) - b-eh[i]/2;
						}
						else if(av[i]>mid)
						{
							aL = w/2 + Math.abs(ar[0]*Math.cos(d));	
							aT = h - ar[0]*Math.sin(d) - b-eh[i]/2;
						}
						else
						{
							aL = w/2 - ar[0]*Math.cos(d)-ew[i]/2;	
							aT = h - ar[0]*Math.sin(d) - b-eh[i]/1.3;
						}
					}
				}
				else if(rotate==90)
				{
					if(i==0)
					{
						aL = w - b-ew[i]+7;
						aT = h/2+ar[0]-eh[i]/3;
					}
					else if(i==av.length-1)
					{
						aL = w - b-ew[i]+7;
						aT = h/2-ar[0]-eh[i]/1.5;
					}
					else
					{
						if(av[i]<mid)
						{
						aL = w - b - ar[0]*Math.sin(d)-ew[i];	
						aT = h/2+(ar[0]+7)*Math.cos(d)-eh[i];
						}
						else if(av[i]>mid)
						{
							aL = w - b -Math.abs(ar[0]*Math.sin(Math.PI-d))-ew[i];
							aT = h/2-Math.abs(ar[0]*Math.cos(Math.PI-d))-eh[i]/2;
						}
						else
						{
							aL = w - b - ar[0]-ew[i];
							aT = h/2-eh[i]/2;
						}
					}
				}
				$(aText[i]).css({"left":aL,"top":aT});	
			};
			//隐藏不需要显示的警示值
			if(options.alarmHide.length!=0)
			{
				var an = $("a",t);
				for(var k=0; k<options.alarmHide.length; k++)
				{
					if(k<an.length)
					{
					$(an[options.alarmHide[k]+1]).hide();
					}
				}
			}
			ctx.save();
			ctx.restore();
		}
	};
	$.canvasForIE = function(ele)
	{
		if(document.all)
		{
			ele = window.G_vmlCanvasManager.initElement(ele);
		};
		return ele;
	};
	$.isHaveParameter = function(obj)
	{
		var i = 0;
		for(var p in obj){
		i++;
		if(i>0){break;}
		};
		if(i>0){
		return true;
		}
		else 
		{
		return false;
		};
	};
	$.getTargetSize = function(t){
		this.width = t.width();
		this.height = t.height();
	};
})(jQuery);
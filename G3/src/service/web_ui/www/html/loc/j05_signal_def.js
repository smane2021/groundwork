//<!-- Copyright (c) 2020, Vertiv Tech Co. Ltd.-->

//2004-9-3 Marco


function OSample( signalId,szName, szUnit) 
{
	var o = new Object;
	
	
	
	o.szSignalId = signalId;
	o.szName = szName;
	o.szUnit = (szUnit != "") ? szUnit : ".";

	o.fValue = -1;
	o.bWaring = false;
	
	o.szTime = -1;
	o.szSampler = -1;
	o.szChannel = -1;
	return o;
}


function OControl( signalId, szName, szUnit,nType,minVal, maxVal) 
{
	var o = new Object;

	o.szName = szName;
	o.signalId = signalId;

	o.fValue = -1;
	o.szTime = -1;
	o.bWarning = false;
	
	o.szUnit =szUnit;
	
	o.nType=nType;

	
	o.szSampler = -1;
	o.szChannel = -1;
	
	
	if (nType == 0 )   
	{
		o.szOptions = new Array();
		o.szOptions[0] = "/*[ʹ��]*/";
		o.szOptions[1] = "/*[��ֹ]*/";
		o.nOption = o.szOptions.length ;
	}
	else
	{
		o.nOption = 0 ;
	}
	
	o.minVal = minVal;
	o.maxVal = maxVal;
	return o;
}


function OSetting( signalId, szName, szUnit, nType,minVal, maxVal  ) {
	var o = new Object;

	o.szName = szName;
	o.signalId = signalId;
	o.szUnit = szUnit;
	o.nType  = nType; 

	if( nType == 0 ) {
		o.nMinVal = minVal;
		o.nMaxVal = maxVal;
	}
	else {	 //
		o.szOptions = new Array();

		var i, j = 4;/* 4th param */
		//for( i = 0; j <  OParam.arguments.length; i ++, j ++ )
		///o.szOptions[i] = OParam.arguments[j];

		o.nMinVal = 0;
		//o.nMaxVal = o.szOptions.length;

		if( o.nMaxVal < 1 )
		alert( "Param " + szName +" config is incorrected!" );
	}

	o.fValue = -1;
	o.szTime = -1;
	o.bWarning = false;
	
	o.szSampler = -1;
	o.szChannel = -1;
	
	return o;
}

 
function OAlarm( signalId, szName, nType,nLevel ) 
{
	var o = new Object;

	o.signalId = signalId;
	o.szName = szName;
	o.nType = nType ;        // 
	o.nLevel = nLevel;	// 0: no-def, 1: notify, 2: warning, 3: severe


//	o.nBeginTime = 0;
//	o.nEndTime	 = 0;
//	o.bValue = false;

	return o;
}

 
 
function ORAlarm() 
{
	var o = new Object;

	
	o.szName = -1;
	o.nLevel = -1;	// 0: na 1: ba, 2: ma 4: ca 
	o.szTime = -1;
	o.szDevice = -1;
	
	
	return o;
}

 
function OStatus(szName,szUnit)
{
	var o = new Object;
	
	o.szName = szName;
	o.fValue = -1;
	o.szUnit = szUnit;
	
	return o;
}



 
function OEQUIP( equipId, equipName, equipType,
		 equipSamples, equipControls, equipSettings ,equipAlarm ,rAlarm ,equipStatus) 
{
	 
	var o = new Object;
 
 	o.szID  = equipId;
 	o.szName = equipName;
 	o.szType = equipType;
 

	o.pSamples = equipSamples;
	o.pControls  = equipControls;
	o.pSettings  = equipSettings;
	o.pAlarms =  equipAlarm;
	o.pRAlarms = rAlarm;
	o.pStatus = equipStatus;
	//o.pDevice = equipDevice;

	o.nSamples = (o.pSamples) ? o.pSamples.length : 0;
	o.nControls = (o.pControls) ? o.pControls.length : 0;
	o.nSettings = (o.pSettings) ? o.pSettings.length : 0;
	o.nAlarm    = (o.pAlarms )  ? o.pAlarms.length :0;
	o.nSignals = (o.nASamples + o.nControls + o.nSettings + o.nAlarm );
	//o.nDevice = (o.pDevice )  ? o.pDevice.length :0;
	
	//o.nRAlarm   = (o.pRAlarm) ? o.pRAlarm.length : 0;

	
	return o;
}

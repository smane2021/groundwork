
(function($){
	$.tm_defaultParameter = {
		title		: null,	 		//温度计标题
		value 		: 0,			//当前温度
		redValue	: null,			//红色警示温度
		width		: 200,			//短形框的长度,温度计的长度= 300-2倍半径
		height		: 22,			//温度计的高度
		radius		: 8,			//温度计的半径
		xy			: [5,5],		//作图的起始点
		scale		: [-50,-25,0,25,50,75], //温度计刻度数值,最好设置比预估的温度最大值大一刻度
		scaleColor	: ["#999"],
		rectColor	: ["#999","#eee"],
		gdColor		: [["#fff","#ccc"],["#444","#ccc"],["#2396e0","#a6ecbf"]],
		show		: true
	};
	$.tm_setParameter = function(opts){
		if(opts)
		{
			this.title  	 		= opts.title 		    ? opts.title 		    : $.tm_defaultParameter.title;
			this.value				= opts.value			? opts.value			: $.tm_defaultParameter.value;	
			this.redValue			= opts.redValue			? opts.redValue			: $.tm_defaultParameter.redValue;	
			
			this.width				= opts.width			? opts.width			: $.tm_defaultParameter.width;		
			this.height				= opts.height			? opts.height			: $.tm_defaultParameter.height;		
			this.radius				= opts.radius			? opts.radius			: $.tm_defaultParameter.radius;
			this.xy					= opts.xy				? opts.xy				: $.tm_defaultParameter.xy;
			this.scale				= opts.scale			? opts.scale			: $.tm_defaultParameter.scale;
			this.scaleColor			= opts.scaleColor		? opts.scaleColor		: $.tm_defaultParameter.scaleColor
			this.rectColor			= opts.rectColor		? opts.rectColor		: $.tm_defaultParameter.rectColor
			this.gdColor			= opts.gdColor			? opts.gdColor			: $.tm_defaultParameter.gdColor
			this.show				= opts.show==false		? opts.show				: $.tm_defaultParameter.show;			
		}
	}; 
	$.emsThermometer = function(target,options)
	{		
		var t = $("#"+target);
		t.css({"position":"relative"});//强制设置成relative
		var tName = target;
		if(options && $.isHaveParameter(options))
		{
			options = new $.tm_setParameter(options);
		}
		else
		{
			options = $.tm_defaultParameter;
		};
		
		if(options.show==true)
		{
		$.drawThermometer(t,tName,options);
		}
		
	};
	$.drawThermometer = function(t,name,options)
	{
		if(options.value==-1)
		{
			return;	
		}
		//初始化DIV(清除上贞绘制的内容)
		var cnum = $(t).children();
		if(cnum.length>0)
		{
			cnum.remove();
		};
		
		var container = new $.getTargetSize(t);
		var w = container.width;
		var h = container.height;
		
		//生成画布,设置id,大小,样式
		var canvas = document.createElement("canvas");
		$.canvasForIE(canvas);
		t.append(canvas);
		$(canvas).addClass(options.canvasClass);
		$(canvas).attr({"width":w,"height":h});
		ctx = canvas.getContext('2d'); 
		ctx.lineCap = "round";
		
		var ow = options.width;
		var oh = options.height;
		var r = options.radius;
		var axis = options.xy;
		var os = options.scale;
		
		if(options.value<os[0])
		{
			options.value = os[0];	
		}
		else if(options.value>os[os.length-1])
		{
			options.value = os[os.length-1]	
		}
		
		var v = ((options.value-os[0])/(os[os.length-1]-os[0]))*(ow-10)
		var rv = ((options.redValue-os[0])/(os[os.length-1]-os[0]))*(ow-10)		
		//画刻度
		ctx.lineWidth = 1;
		ctx.strokeStyle= options.scaleColor[0];
		ctx.beginPath();
		var j = Math.round((ow-10)/(options.scale.length-1))
		var text = [];
		var dw = [];
		for(var i =0; i<=options.scale.length-1; i++)
		{
			//数值
			text[i] = document.createElement("span");
			$(text[i]).html(options.scale[i]);
			t.append($(text[i]));
			$(text[i]).addClass("thermometerText");
			dw[i] = $(text[i]).outerWidth(true)
			//alert(dw[i]);
			$(text[i]).css({"position":"absolute","left":axis[0]+6+i*j,"top":axis[1]+55});
			//大刻度
			ctx.moveTo(axis[0]+5+i*j+0.5,axis[1]+30)
			ctx.lineTo(axis[0]+5+i*j+0.5,axis[1]+40)
			ctx.stroke();
		}
		for(var i =0; i<=(options.scale.length-1)*2; i++)
		{
			//小刻度
			ctx.moveTo(axis[0]+5+i*0.5*j+0.5,axis[1]+30)
			ctx.lineTo(axis[0]+5+i*0.5*j+0.5,axis[1]+35)
			ctx.stroke();
		}
		ctx.save();
		ctx.restore();
		
		//画圆角矩形
		ctx.lineWidth = 2;
		ctx.strokeStyle= options.rectColor[0];
		$.drawRoundRect(axis[0],axis[1],ow,oh,r,90,options.gdColor[0],[0,1]);
		ctx.stroke();
		ctx.strokeStyle= options.rectColor[1];
		$.drawRoundRect(axis[0]+5,axis[1]+5,ow-10,oh-11,r-3,90,options.gdColor[1],[0,1]);
		ctx.stroke();
		$.drawRoundRect(axis[0]+5,axis[1]+5,ow-10,oh-11,r-3,180,options.gdColor[2],[0,1]);
		var f = [];
		if(v<4)
		{
			f = [true,true,true,true];
		}
		else if(v >=4)
		{
			f = [false,true,true,false];
		}
		if(v<ow-10)
		{
		$.drawRoundRect(axis[0]+5+v,axis[1]+5,ow-10-v,oh-11,r-3,90,options.gdColor[1],[0,1],f);
		}
		var redf = []
		if(options.value > 73)
		{
			redf = [false,true,true,false]
		}
		else
		{
			redf = [false,false,false,false]
		}
		if(options.value > options.redValue)
		{
			$.drawRoundRect(axis[0]+5+rv,axis[1]+5,v-rv,oh-11,r-3,90,["#ff3333"],[0,1],redf);	
		}
		ctx.save();
		ctx.restore();
	};
	//x,y为圆角矩形左上角的坐标;w,h为矩形的宽、高,radius为圆角的半径;color为渐变的颜色数组;angle为渐变的角度
	$.drawRoundRect = function(x,y,w,h,radius,angle,color,pos,fp)
	{	
		if(fp==undefined){ 
		fp = [true,true,true,true]; 
		}
		ctx.beginPath();	
		ctx.moveTo(x,y+radius);
		ctx.lineTo(x,y+h-radius);
		fp[0]==true ? ctx.quadraticCurveTo(x,y+h,x+radius,y+h) : ctx.lineTo(x,y+h);
		ctx.lineTo(x+w-radius,y+h);
		fp[1]==true ? ctx.quadraticCurveTo(x+w,y+h,x+w,y+h-radius) : ctx.lineTo(x+w,y+h);
		ctx.lineTo(x+w,y+radius)
		fp[2]==true ? ctx.quadraticCurveTo(x+w,y,x+w-radius,y) : ctx.lineTo(x+w,y);
		ctx.lineTo(x+radius,y);
		fp[3]==true ? ctx.quadraticCurveTo(x,y,x,y+radius) : ctx.lineTo(x,y);
		if(color != undefined && angle !=undefined)
		{
			if(angle==90)
			{
				x2 = x;	
				y2 = y+h;
			}
			else if(angle == 180)
			{
				x2 = x+w;
				y2 = y;
			}
			var gradient = ctx.createLinearGradient(x,y,x2,y2);
			var j = 1/color.length;
			for(var i = 0 ; i<color.length; i++)
			{
				if(pos==undefined)
				{
					gradient.addColorStop(i*j,color[i]);
				}
				else
				{
					gradient.addColorStop(pos[i],color[i]);
				}
			}
			ctx.fillStyle= gradient;
			ctx.fill();
		}
	}
	$.canvasForIE = function(ele)
	{
		if(document.all)
		{
			ele = window.G_vmlCanvasManager.initElement(ele);
		};
		return ele;
	};
	$.isHaveParameter = function(obj)
	{
		var i = 0;
		for(var p in obj){
		i++;
		if(i>0){break;}
		};
		if(i>0){
		return true;
		}
		else 
		{
		return false;
		};
	};
	$.getTargetSize = function(t){
		this.width = t.width();
		this.height = t.height();
	};
	
	
})(jQuery);
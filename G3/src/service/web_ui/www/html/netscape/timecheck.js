var errFormat="/*[ID_INFO1]*/\n"+"yyyy-MM-dd hh:mm:ss";
var errYear="/*[ID_INFO2]*/";
var errMonth="/*[ID_INFO3]*/";
var errDay="/*[ID_INFO4]*/";
var errHour="/*[ID_INFO5]*/";
var errMinute="/*[ID_INFO6]*/";
var errSecond="/*[ID_INFO7]*/";		
var timeFrom;
var timeTo;
function getSampleCurDate(nSec) 
{
	if( isNaN(nSec) ) {
		nSec = atoi(nSec);
		if( isNaN(nSec) ) 
			nSec = 0;
	}
	var s="-";
	var d = new Date();
	if( nSec != 0 ) {
		d.setTime( nSec*1000 );
	}
	var dd=d.getDate();
	var sd=(dd<10) ? "0"+dd : ""+dd;
	var m=d.getMonth() + 1;
	var sm = (m<10) ? "0"+m : ""+m;
	var strDate = ""+d.getFullYear() + s + sm + s + sd;
	return strDate;
}

function makeQueryDateStr(nSec, boolLast)
{
	if(!boolLast)
	{
		return "" + getSampleCurDate(nSec) + " " + "00:00:00";
	}
	else
	{
		return "" + getSampleCurDate(nSec) + " " + "23:59:59";
	}
} 

function initBody()
{
 
	document.Form1.fromTime.value = makeQueryDateStr(0,false);//makeSampleDateStr(0);
	document.Form1.toTime.value =  makeQueryDateStr(0,true);//makeSampleDateStr(0);
	 
}	
 

function getErrorMsg(msg)
{
     alert(msg);
     return false;
 }

function checkDateByMask(dt,msg,fm,type)
{

//Define synax
var N=1;
var format=new Array(1);
format[0]="yyyy-MM-dd hh:mm:ss";
var b=false;
for(var i=0;i<N;i++){

     if(format[i].toLowerCase()==fm.toLowerCase()){
            b=true;break;
     }
}
if(!b){
     return getErrorMsg(errPar);
}


if(dt.length!=fm.length){
     var dt4=dt.replace(/[^0-9]/g,",")
     var dtarr=dt4.split(",");
     var dt3="";
     var dtlen=0;
     for(var i=0;i<dtarr.length;i++){
         var len=dtarr[i].length;
         dtlen=dtlen+len+1;
         if(len<1)
                dt3=dt3+"00"+dtarr[i];
         else if(len<2)
                dt3=dt3+"0"+dtarr[i];
         else
                dt3=dt3+dtarr[i];
                
         dt3=dt3+dt.substr(dtlen-1,1);
         }
     dt=dt3;
}

if(dt.length!=fm.length){         
     return getErrorMsg(errFormat);
}
else{
     var dt1=dt.replace(/[0-9]/g,"0");
     var dt2=fm.replace(/[ymdhs]/gi,"0");
     //alert(dt1+"\n"+dt2);
     if(dt1!=dt2){
           return getErrorMsg(errFormat);
     }
}     
         

//try{
     fm=fm.replace(/Y/g,"y").replace(/D/g,"d");
     var iyyyy=fm.indexOf("yyyy");
     var iyy=fm.indexOf("yy");
     var imm=fm.indexOf("MM");
     var idd=fm.indexOf("dd");
     var ihh=fm.indexOf("hh");
     var imi=fm.indexOf("mm");
     var iss=fm.indexOf("ss");
    
     var newdt=new Date(); 
     
     var year="";
     //Year    
     //try{
         var isyear=false;
         if(iyyyy>-1){                
            year=dt.substr(iyyyy,4);
            isyear=true;
         }
         else if(iyy>-1){
            year=dt.substr(iyy,2);
            isyear=true;
         }
         if(isyear){
            if(type=="1"){//
               year=parseInt(year)+1911;
            }
            newdt.setFullYear(year);
         }   
     //}
     //catch(e1){
	//		getErrorMsg(errYear+e1.toString());
	//		return false; 
    // }
     
     //Month
     //try{     
         if(imm>-1){
             if(dt.substr(imm,2)>"12"||dt.substr(imm,2)<"01"){
                 getErrorMsg(errMonth);
                 return false;
             }
             //----------------------------------------------------
				//Don't modify. Solve the February problem
             newdt.setMonth(dt.substr(imm,2)-1);
             newdt.setMonth(dt.substr(imm,2)-1);
             //----------------------------------------------------
         }
     //}
     //catch(e1){
     //     getErrorMsg(errMonth+e1.toString());
     //     return false;
     //}
     
     //Day
     //try{     
         if(idd>-1){
             if(dt.substr(idd,2)>"31"||dt.substr(idd,2)<"01"){
                 getErrorMsg(errDay);
                 return false;
             }
             newdt.setDate(dt.substr(idd,2));  
         }
     //}
     //catch(e1){
     //    getErrorMsg(errDay);
     //    return false;
     //}
     
     //Hour
     //try{
         if(ihh>-1){
             if(dt.substr(ihh,2)>"23"){
                 getErrorMsg(errHour);
                 return false;
             }
             newdt.setHours(dt.substr(ihh,2));
         }
     //}
     //catch(e1){
     //    getErrorMsg(errHour);
     //    return false;
     //}
     
     //Minute
     //try{
         if(imi>-1){
             if(dt.substr(imi,2)>"59"){
                getErrorMsg(errMinute);
                return false;
             }
             newdt.setMinutes(dt.substr(imi,2));
         }
     //}
     //catch(e1){
     //    getErrorMsg(errMinute);
     //     return false;
     //}
     
     //Second
     //try{
         if(iss>-1){
             if(dt.substr(iss,2)>"59"){
                 getErrorMsg(errSecond);
                  return false;
             }
             newdt.setSeconds(dt.substr(iss,2));
         }         
     //}
     //catch(e1){
	//		getErrorMsg(errSecond);
	//		return false;
    //}
          
     //Year
     if(iyyyy>-1){
          if(newdt.getFullYear()!=year){
                 getErrorMsg(errYear); 
                  return false;
          }
     }
     else if(iyy>-1){
          if(newdt.getFullYear()!=year){
                 getErrorMsg(errYear); 
                  return false;
          }
     }
     
     //Month
     if(imm>-1){
          if(newdt.getMonth()!=(dt.substr(imm,2)-1)){
                 getErrorMsg(errDay); 
                  return false;
          }
     }
     
     //Day       
     if(idd>-1){
          if(newdt.getDate()!=dt.substr(idd,2)){
                 getErrorMsg(errDay); 
                  return false;
          } 
     }    
            

     //Hour
     if(ihh>-1){
          if(newdt.getHours()!=dt.substr(ihh,2)){
                 getErrorMsg(errMinute); 
                  return false;
          } 
     }
     
     //Minute
     if(imi>-1){
          if(newdt.getMinutes()!=dt.substr(imi,2)){
                 getErrorMsg(errSecond); 
                  return false;
          } 
     }
     
     //Second
     if(iss>-1){          
          if(newdt.getSeconds()!=dt.substr(iss,2)){
                 getErrorMsg(errSecond); 
                  return false;
          }
     } 
     //return newdt;
     return Math.floor(newdt.getTime() / 1000 - new Date().getTimezoneOffset() * 60);
//}  
//catch(e){
//     getErrorMsg(e.toString()); 
//      return false;
//}

            
}

function checkDate(s)
{
	
    return checkDateByMask(s,"","yyyy-MM-dd hh:mm:ss","");
}

function getTime()
{
	var varFromTime;
	var varToTime;
	
	
	varFromTime = document.Form1.fromTime.value;
	varToTime = document.Form1.toTime.value;
	
	if((timeFrom = checkDate(varFromTime)) == false)
	{
		//alert('formtime is wrong');
		return false;
	}
	//alert(timeFrom);	
	if((timeTo = checkDate(varToTime)) == false)
	{
		//alert('totime is wrong!');
		return false;
	}
	
	if(timeFrom > timeTo)
	{
		alert("wrong seqence of time!");//End time should be big than start time
		return false;
	}
	
	
}
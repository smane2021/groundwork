 /*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi-realtime.h
 *  CREATOR  : Yang Guoxin              DATE: 2004-09-28 10:15
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef _CGI_REALTIME_H 
#define	_CGI_REALTIME_H 
#endif

#include "stdsys.h"  
#include "public.h"
#include "pubfunc.h"
#include "cgi_pub.h" 

typedef struct sampleData
{	
	float			value;
	char			time[32];
	int				status;
	char			sampler[16];
	char			samplerChannel[4];
}SAMPLEDATA;

typedef struct alarmData
{
	char			alarmName[16];
	unsigned short	nLevel;
	char			time[32];
	char			alarmDevice[16];
}ALARMDATA;

typedef struct sampleSigStruct
{
	int			signalID;
	char		szSigName[16];
	char		szUnit[4];
}SAMPLESIGSTRUCT;


typedef struct controlSigStruct
{ 
	int			signalID;
	char		szSigName[16];
	char		szUnit[4];
	int			nType;
	float		minVal;
	float		maxVal;

}CONTROLSIGSTRUCT;

typedef struct settingSigStruct
{
	int			signalID;
	char		szSigName[16];
	char		szUnit[4];
	int			nType;
	float		minVal;
	float		maxVal;
		
}SETTINGSIGSTRUCT;

typedef struct alarmSigStruct
{
	int			signalID;
	char		szSigName[16];
	int			nType;
	int			nLevel;

}ALARMSIGSTRUCT;


/*
#define LOG_IN							0			//First Connect
#define REALTIME_DATA					1			//get realtime data
#define	CONTROL_COMMAND					2			//control
#define	HISTORY_QUERY					3			//history query
#define	CONFIGURE_MANAGE				4			//config manage
#define FILE_MANAGE						5			//file manage
*/

/*used to get data from buffer */
#define SAMPLE_DATA						0
#define	CONTROL_DATA					1
#define	SETTING_DATA					2
#define	RALARM_DATA						3
#define	STATUS_DATA						4

#define	SAMPLE_SIGNAL_STRUC				5
#define	CONTROL_SIGNAL_STRUC			6
#define SETTING_SIGNAL_STRUC			7
#define	ALARM_SIGNAL_STRUC				8
#define STATUS_SIGNAL_STRUC				9

/*define the buffer len to store general signal in fifo*/
#define MAX_DATA_TYPE_LEN				2
#define MAX_DATA_BUFFER_LEN				4             // The section used to store the signal number in FIFO
#define	MAX_DATA_SIGNAL_VALUE_LEN		8
#define MAX_DATA_SIGANL_TIME_LEN		32
#define MAX_DATA_SIGNAL_STATUS_LEN		1
#define MAX_DATA_SAMPLER_LEN			16
#define	MAX_DATA_SAMPLER_CHANNEL_LEN	4



/*define the buffer len to store alarm  in fifo*/
#define MAX_ALARM_NAME_LEN				8
#define MAX_ALARM_LEVLE_LEN				2
#define MAX_ALARM_VALUE_LEN				8
#define MAX_ALARM_TIME_LEN				32
#define	MAX_ALARM_DEVICENAME_LEN		8

#define MAX_TEMP_BUFFER_LEN				100	/*used to allocate temp buffer in loaddataptr*/

/*define the buffer len to store status signal  in fifo*/
#define MAX_STATUS_SIGNAL_LEN			8

#define MAX_SIGNAL_INT_LEN				4
#define MAX_SIGNAL_FLOAT_LEN			8
#define MAX_SIGNAL_NAME_LEN				16
#define MAX_SIGNAL_UNIT_LEN				4
#define	MAX_STATUS_SIGNAL_STRUC			8



/* Make data_sampler.js */






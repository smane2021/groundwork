/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : cgi_query.c
 *  CREATOR  : Yang Guoxin              DATE: 2004-11-25 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"
#include "public.h"
#include "pubfunc.h"

#include "cgi_pub.h"
#include "cgivars.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>



#define CGI_SET_LANG							"lang"




static void Web_SET_PostLangPage(IN  char *pGetBuf, IN int iLanguage);
static int Web_SET_GetCommandParam(void);
static char *Web_SET_SendConfigureCommand(IN int nCommandType);


int main(void)
{

	int							nCommandType = 0;

	printf("Content-type:text/html\n\n");
	
	SuicideTimer_Init(60,	// after 60 sec, the CGI will suicide.	
		(SUICIDE_CLEAR_PROC)CGI_ClearProc,	
		(void *)"clear param");
	/*get parameter*/
	if ((nCommandType = Web_SET_GetCommandParam()) > 0)
	{
	
		if(Web_SET_SendConfigureCommand(nCommandType) != NULL)
		{
			//Web_SET_PostNMSPage(szReturn);
		}
	}
	else
	{
	}
	
	return TRUE;

}

static char *Web_SET_SendConfigureCommand(IN int nCommandType)	
{
	int		fd,fd2;    //fifo handle
	int		iLen;
	char	szBuf1[1024],szBuf2[PIPE_BUF],fifoname[FIFO_NAME_LEN];
	mode_t	mode = 0666;
	char	*szBuf3 = NULL;
	int		iBufCount = 0;
	//TRACE("szBuf : %s\n", szBuf);
	/*create FIFO with our PID as part of name*/
 	if((fd = open(MAIN_FIFO_NAME,O_WRONLY)) < 0)
	{
		return NULL;
	}
	 
	sprintf(fifoname,"%s/fifo.%ld",CGI_CLIENT_FIFO_PATH, (long)getpid());
	 
	if((mkfifo(fifoname,mode)) < 0)
	{
		return NULL;
	}
	
	/*start buffer with pid and a blank*/
	//iLen = sprintf(szBuf1,"%10ld%2d%2d%-128s",(long)getpid(), WEB_SUPPORT_NETSCAPE,iModifyPassword, szBuf);
	
	iLen = sprintf(szBuf1,"%10ld%2d%2d%2d%d",(long)getpid(), WEB_SUPPORT_NETSCAPE,SET_RECOVER_RESET, 1,nCommandType);
	
	if((write(fd, (void *)szBuf1, iLen + 1))<0)
	{
		close(fd);
		return NULL;
	}
	if((fd2 = open(fifoname,O_RDONLY))<0)
	{
		return NULL;
	}

 
	if((iLen = read(fd2, szBuf2, PIPE_BUF - 1)) > 0)
	{
		Web_SET_PostLangPage(szBuf2, 1);
 
	}


	close(fd2);
	close(fd);
	unlink(fifoname);

	return TRUE;
}




static int Web_SET_GetCommandParam(void)
{
	char	**postvars = NULL; /* POST request data repository */
    char	**getvars = NULL; /* GET request data repository */
    int		form_method,nCommandType; /* POST = 1, GET = 0 */  
	char	*val;

    form_method = getRequestMethod();

    if(form_method == POST) 
	{
		getvars = getGETvars();
        postvars = getPOSTvars();
		if(postvars == NULL )
		{
			AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get CGI POST param");
			return FALSE;
		}
    } 
	else if(form_method == GET) 
	{
        getvars = getGETvars();
    }
	else 
	{
		AppLogOut("cgi_query",APP_LOG_WARNING,"Fail to get post method!");
		return FALSE;
	}
	
	if((val = getValue( getvars, postvars,  CGI_GET_SETTING_TYPE)) != NULL)
	{
		nCommandType = atoi(val);
		 
	}

	cleanUp(getvars, postvars);
    return nCommandType;  
}

static void Web_SET_PostLangPage(IN  char *pGetBuf, IN int iLanguage)
{

//�ѷ��صĽ��д��error.htm�ļ���,������/var/netscapeĿ¼��
	//���ļ�
#define CGI_NET_PATH_ERROR_FILE			"/app/www/netscape/pages/error.htm"	
#define CGI_NET_CONTROL_VALUE			"/*[ID_RETURN]*/"
	char	*pHtml = NULL;
 
	if(LoadHtmlFile(CGI_NET_PATH_ERROR_FILE, &pHtml ) > 0)
	{
		printf("Content-type:text/html\n\n");
		ReplaceString(&pHtml, CGI_NET_CONTROL_VALUE, pGetBuf);
		
		PostPage(pHtml);
		DELETE(pHtml);
		pHtml = NULL;
	}
	
 
	
}


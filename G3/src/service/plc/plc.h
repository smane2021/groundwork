/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : plc.h
 *  CREATOR  : LIXIDONG                 DATE: 2004-11-23 16:46
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __PLC_H
#define __PLC_H

//////////////////////////////////////////////////////////////////////
//Register table length
#define			PLC_REG_NUM				100

//Plc register struct
struct	tagPLC_REG_STRU		
{
	BOOL		bValid;				//1: Valid; 0: Invalid
	BYTE		byRegValueType;		//Signal Value Type   -Refer to cfg_model.h
	VAR_VALUE	varValue;
};
typedef struct tagPLC_REG_STRU tPLC_REG_STRU;

/*PLC  Data: In the ServiceMain function, a TYPE_PLC_DATA 
  variant shall be defined, which shall be initialized by the PLC_Init. */
//PLC Signal struct Definition
struct	tagPLC_SIG		
{
	BOOL		blReg;				//1: Register; 0: Signal
	BOOL		bValValid;          //1: Valid     0: Invalid
	int			iRegIdx;			//Register Index
	int			iEquipID;			//Equipment ID
	int			iSignalID;			//Signal ID
	BYTE		bySignalType;		//Signal Type		  -Refer to cfg_model.h
	BYTE		bySigValueType;	//Signal Value Type   -Refer to cfg_model.h
};
typedef struct tagPLC_SIG tPLC_SIG;

//Delay record struct Definition
// According to the Delay Command Parameter 1 and PLC Calc Period, */
// pInputRcd shall be assigned appropriate size to record the Input.*/
struct tagDELAY		
{	
	BYTE*			pInputRcd;
	int				iRcdAryHead;	//The delay record array head
	int				iRcdOutPos;	//The delay record current output position
	//int			iRcdAryLength;	//The delay record array Length
	int				iRcdArySize;	//The delay record array size
};
typedef struct tagDELAY tDELAY;

typedef struct tagPlcCommand tPLC_COMMAND;
//To define the function point for PLC Command
typedef int (*PLC_CMD_PROC)(tPLC_COMMAND *pArgs);

//PLC Command Type Definition
struct tagPlcCommand
{
	PLC_CMD_PROC	pfnPlcCmd;		//The function point of PLC Command; 
	tPLC_SIG		sigInput1;		//The First Input
	tPLC_SIG		sigInput2;		//The Second Input
	tPLC_SIG		sigOutput;		//The Output
	float			fParameter1;	//The First Parameter
	float			fParameter2;	//The Second Parameter
	
	/* If the command is Delay, the Plc module shall New a tDELAY 
	 and assign to pDelay ,otherwise pDelay = NULL*/
	tDELAY		*   pDelay;
};

struct tagPLC_TIMER
{		
	time_t		tStartTime;
	int			iInterval;

	/*The Timer State, include Suspended, Time-in, Time-out*/
	BYTE		byState;
};
typedef struct tagPLC_TIMER	PLC_SEC_TIMER;


//According to cfg, assign appropriate size for PLC Command Table
struct tagPLC_DATA
{
	/*Handle of PLC Service thread*/
	HANDLE				hThreadSelf;
	int					iCalPeriod;
	int					iCmdNums;
	int					iDelayRegs;
	tPLC_SIG			*g_pCtrlSig;
	tPLC_COMMAND		*pPlcCmdTbl;
	BYTE				*pbyRegister;
	//tPLC_REG_STRU		*pPlcRegSt;
	BOOL				bTimerInited;
    
	/*Second Timers Data*/
	PLC_SEC_TIMER		PLC_SecTimer;
};
typedef struct tagPLC_DATA TYPE_PLC_DATA;

enum	PLC_TIMER_ID
{
	PLC_TM_ID_SCAN_CMD = 6999,
	MAX_PLC_TIMER_NUM
};


/////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  :  The interface function for ACU main module
 * CALLS    : PLC_Init
 * CALLED BY: Main Module of ACU
 * ARGUMENTS: SERVICE_ARGUMENTS  *pArgs : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-28 17:06
 *==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs);

#endif //_DEBUG_MAINTN_H


/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : plc.c
 *  CREATOR  : LIXIDONG                 DATE: 2004-11-23 16:46
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

#include "plc.h"

#ifdef _DEBUG
//#define _DEBUG_PLC_		1
//#define _PLC_TEST_DS_		1
#endif

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//PLC private configration file name
#define			PLC_CFG				"private/plc/plc.cfg"

//File section definiton
#define			PLC_CALC_PERIOD		"[CALC_PERIOD]"
#define			PLC_CMD_SECTION		"[PLC_CMD]"
#define			PLC_CMD_CTRL_SIG	"[SIGNAL_DEF]"

///////////////////////////////////////////////////////////////////////////////
#define			PLC_SPLITTER		(0x09) //('\t')
#define			MAX_COMMAND_NUMS	100

///////////////////////////////////////////////////////////////////////////////
//The shortest calculate period
#define			MIN_CALC_PERIOD		5              //5s
#define			MAX_CALC_PERIOD		60              //5s

///////////////////////////////////////////////////////////////////////////////
#define		PLC_R					'R' //(0x52)	'R':register
#define		PLC_S					'S' //(0x53)	'S':signal
#define		PLC_P					'P' //(0x50)	'P':parameter
#define		PLC_EMPTY				'_'
#define		SPACE					(0x20)
#define		IS_REGISTER(c)			((c) == PLC_R)
#define		IS_SIGNAL(c)			((c) == PLC_S)
#define		IS_EMPTY(c)				(((c) == PLC_EMPTY) || (c) == SPACE)
#define		IS_PARAMETER(c)			((c) == PLC_P)
#define		COMMA_SPLIT				0x2c	//','
#define		LEFT_BRAC				'('
#define		RIGHT_BRAC				')'
#define		IS_LEFT_BRACKET(c)		((c) == LEFT_BRAC) 
#define		IS_RIGHT_BRACKET(c)		((c) == RIGHT_BRAC)

///////////////////////////////////////////////////////////////////////////////
#define		PLC_GET_DATA_FAIL		1
#define		PLC_GET_DATA_SUCCESS	0

///////////////////////////////////////////////////////////////////////////////
//Logic calculate modes
#define		PLC_SET					"SET"
#define		PLC_AND					"AND"
#define		PLC_OR					"OR"
#define		PLC_NOT					"NOT"
#define		PLC_XOR					"XOR"
#define		PLC_GT					"GT"
#define		PLC_LT					"LT"
#define		PLC_DS					"DS"
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Define global variable in plc
static TYPE_PLC_DATA			*g_pPlcData;
static TYPE_PLC_DATA			g_PlcData;

///////////////////////////////////////////////////////////////////////////////
//Declare Funcitons
//Read configuraion information from plc private configuraion file
static int PLC_ReadConfig(void);

//Read command lines from plc private configuraion file
static int LoadPLCCommand(IN void *pCfg);

//Parse command line information and initiate tPLC_COMMAND
static int ParsePLCCmdTableProc(IN char *szBuf, OUT tPLC_COMMAND *pCmd);

static int ParsePLCCtrlSig(IN char *szBuf, OUT tPLC_SIG *pSig);

//Clear some resource before exit program
static void ExitPLC(void);

//Main processing thread function
static void PLC_CalcFunc(void);

///Initiate all information
static BOOL PLC_Init(void);

static BOOL GreaterThan(tPLC_COMMAND *pArgs);
static BOOL LessThan(tPLC_COMMAND *pArgs);
static BOOL SetCalc(tPLC_COMMAND *pArgs);
static BOOL DelayCalc(tPLC_COMMAND *pArgs);

static BOOL CheckCalLogic(tPLC_COMMAND *pPlcCmd, int iCmdLine);
//Check current signal exist or not
static BOOL	SignalExisted(tPLC_SIG *pSig, tPLC_COMMAND *pPlcCmd, 
						  int iSigPos, int iCmdLine);

static BOOL GetSignalValue(tPLC_SIG *pSignal, SIG_BASIC_VALUE *pValue);
//Check plc signals cfg is right or not
static int PLC_CheckSigCfg(void);

//Initiate all timer in plc
static BOOL PLC_InitTimers(void);

//Delete all timer in plc
static void PLC_DestroyTimers(void);

//Raise PLC Config Error;
static BOOL RaiseConfigError(void);

///////////////////////////////////////////////////////////////////////////////
//Declare all calculation functions
static void GetCallBackFunc(tPLC_COMMAND *pPlcCmd, char *pFieldStr);

static SIG_ENUM GC_GetEnumValue(IN int iEquipID, IN int iSignalType, IN int iSIgnalID);
/*==========================================================================
	The process describe the flow
	1.Clear the registers to 0
	2.Read PLC private configuration file, compile the configuration file and 
	  stuff the PLC Command Table
	3.Check Configuration File is correct or not
	4.Recycle to Calc periodically (the period is configurable)
	  Scan the Commands Tab take a command to execute
	5.Exit Handling
==========================================================================*/

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//Start to define functions

#define		GET_SERVICE_OF_PLC_NAME	"plc.so"

/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  :  The interface function for ACU main module
 * CALLS    : PLC_Init
 * CALLED BY: Main Module of ACU
 * ARGUMENTS: SERVICE_ARGUMENTS  *pArgs : 
 * RETURN   : DWORD : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-28 17:06
 *==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS	*pArgs)
{
	BOOL				bExitFlag;
	int					iRst;
	RUN_THREAD_MSG		msg;
	int					iCounters;
	APP_SERVICE		*PLC_AppService = NULL;
	PLC_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_PLC_NAME);

	iCounters = 0;
	/*g_pPlcData = NEW(TYPE_PLC_DATA, 1);

	if (g_pPlcData == NULL)
	{

#ifdef _DEBUG_PLC_
		printf("Fail to allocate memory for TYPE_PLC_DATA.\n");
#endif
		AppLogOut("PLC_SERVICE", APP_LOG_ERROR, 
			"Fail to allocate memory for TYPE_PLC_DATA");
		return ERR_PLC_NO_MEMORY;
	}*/

	g_pPlcData = &g_PlcData;


	//Init value
	g_pPlcData->bTimerInited = FALSE;

	g_pPlcData->pbyRegister = NEW(BYTE, PLC_REG_NUM);

	if (g_pPlcData->pbyRegister == NULL)
	{
#ifdef _DEBUG_PLC_
		printf("Fail to allocate memory for pbyRegister.\n");
#endif
		//DELETE(g_pPlcData);
		AppLogOut("PLC_SERVICE", APP_LOG_ERROR, 
			"Fail to allocate memory for register");
		return ERR_PLC_NO_MEMORY;
	}

	g_pPlcData->hThreadSelf = RunThread_GetId(NULL);	

	if (!PLC_Init())
	{
#ifdef _DEBUG_PLC_
		printf("Fail to execute initiating PLC.\n");
#endif
		AppLogOut("PLC_SERVICE", APP_LOG_ERROR, 
			"Fail to initiate.PLC service will exit.");
		ExitPLC();
		return ERR_PLC_FAIL_INIT;
	}

#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread PLC Service was created, Thread Id = %d.\n", gettid());
#endif

	bExitFlag = FALSE;
	while(!bExitFlag)
	{

		//To tell the thread manager I'm living.
		RunThread_Heartbeat(g_pPlcData->hThreadSelf);	
		Sleep(1000);

		iRst = RunThread_GetMessage(g_pPlcData->hThreadSelf, &msg, FALSE, 0);

		if (!THREAD_IS_RUNNING(g_pPlcData->hThreadSelf))
		{

#ifdef _DEBUG_PLC_
		printf("PLC thread is not running, plc will exit.\n");
#endif
			bExitFlag = TRUE;
			break;
		}

#ifdef _DEBUG_PLC_
		iCounters++;
		if (iCounters%50 == 0)
		{
			printf("Current iCounters is %d\n", iCounters);
		}		
#endif
		if(iRst == ERR_OK)
		{
			switch(msg.dwMsg)
			{
			case MSG_TIMER:
#ifdef GC_SUPPORT_RELAY_TEST
#define SIG_ID_RELAY_TEST	222
				if(GC_GetEnumValue(1, SIG_TYPE_SETTING, SIG_ID_RELAY_TEST) == 0)//Relay test, Disable
				{
					PLC_CalcFunc();
				}
				
#else
				PLC_CalcFunc();
#endif
				break;

			case MSG_QUIT:
				bExitFlag = TRUE;
				break;
			

			default:
#ifdef _DEBUG_PLC_
				printf("Invalid Message Parameter1!\n");
#endif
				bExitFlag = FALSE;
				break;
			}//switch(msg.dwMsg)
		}
		if(PLC_AppService != NULL)
		{
			PLC_AppService->bReadyforNextService = TRUE;
		}
	}//while(bExitFlag)

	//TRACE("Start Stopping PLC.\n");
	ExitPLC();

//#ifdef _DEBUG_PLC_
	//TRACE("The PLC service has exited Successfully!\n");
//#endif
	return	0;
}

/*==========================================================================*
 * FUNCTION : RecoverSigValid
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pPlcCmd : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-03 16:26
 *==========================================================================*/
static void RecoverOutRegValid(tPLC_COMMAND *pPlcCmd)
{
    if (pPlcCmd->sigOutput.blReg)
	{
		pPlcCmd->sigOutput.bValValid = TRUE;
	}
	return;
}

/*==========================================================================*
 * FUNCTION : SetSigInvlaid
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pPlcCmd : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-04-03 16:26
 *==========================================================================*/
static void SetOutRegInvalid(tPLC_COMMAND *pPlcCmd)
{
	if (pPlcCmd->sigOutput.blReg)
	{
		pPlcCmd->sigOutput.bValValid = FALSE;
	}
}

/*==========================================================================*
 * FUNCTION : PLC_Init
 * PURPOSE  : 
 * CALLS    : PLC_ReadConfig
 * CALLED BY: 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:21
 *==========================================================================*/
static BOOL PLC_Init(void)
{
	int i, iError;
	i			 =	0;
	iError		 =	0;

	//1.Clear the registers to 0
	for(i = 0; i < PLC_REG_NUM; i++)
	{
		*(g_pPlcData->pbyRegister + i) = 0;
	}

	//2.Read PLC private configuration file
	 iError = PLC_ReadConfig();
	if (iError != ERR_PLC_OK)
	{
#ifdef _DEBUG_PLC_
	printf("Fail to read plc private configuration!\n");
#endif
		ExitPLC();
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR,
			"Fail to read configuration.");	

		return FALSE;
	}

	//3.Check configuration file
	if (PLC_CheckSigCfg() != ERR_PLC_OK)
	{
#ifdef _DEBUG_PLC_
	printf("Some errors in private configuration file!\n");
#endif
		ExitPLC();
		AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR,
			"Some command line errors in plc.cfg.");
		RaiseConfigError();

		return FALSE;
	}
	//4.Start timer
	if (!PLC_InitTimers())
	{
#ifdef _DEBUG_PLC_
	printf("Fail to initiate plc private timer!\n");
#endif
		ExitPLC();
		AppLogOut("PLC_INI_TIMER", APP_LOG_ERROR,
			"Fail to initiate plc timer.");
		return FALSE;
	}

	g_pPlcData->bTimerInited = TRUE;

#ifdef _DEBUG_PLC_
	printf("Succeed Init PLC_InitTimers\n");
#endif

	return TRUE;
}


/*==========================================================================*
 * FUNCTION : SignalExisted
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_SIG      *pSig    : 
 *            tPLC_COMMAND  *pPlcCmd : 
 *            int           iSigPos  : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-22 12:00
 *==========================================================================*/
static BOOL	SignalExisted(tPLC_SIG *pSig, tPLC_COMMAND *pPlcCmd, 
						  int iSigPos, int iCmdLine)
{
	int		iRst;
	int		iBufLen;
	void	*pSigStru;

	iRst = PLC_GET_DATA_FAIL;
	iRst = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
				pSig->iEquipID,
				DXI_MERGE_SIG_ID(pSig->bySignalType, pSig->iSignalID),
				&iBufLen,
				&pSigStru,
				0);

	//1.Check signl is existed or not
	if ((iRst != PLC_GET_DATA_SUCCESS))
	{
		if (iSigPos < 2)
		{
			AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
					"In plc.cfg, the %d line command error,"
					"input%d signal is not existed.", 
					iCmdLine, (iSigPos + 1));
		}
		else
		{
			AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
					"In plc.cfg, the %d line command error, "
					"Output signal is not existed.",
					iCmdLine);
		}
				
		return FALSE;
	}
	else
	{
		//If it is alarm signal, it will return directly
		if (pSig->bySignalType == SIG_TYPE_ALARM)
		{
			return TRUE;
		}

		//2.Check signal value type is right or not
		if (((pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)GreaterThan) ||
			(pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)LessThan)) && 
			iSigPos < 2)
		{
			switch (((SET_SIG_INFO *)pSigStru)->iSigValueType)
			{
			case VAR_FLOAT:
				pSig->bySigValueType = VAR_FLOAT;
				break;
			case VAR_UNSIGNED_LONG:
				pSig->bySigValueType = VAR_UNSIGNED_LONG;
				break;
			case VAR_LONG:
				pSig->bySigValueType = VAR_LONG;
				break;
			default:
				AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
					"In plc.cfg, the %d line command error,"
					"LT or GT input%d signal value type is not float,long or ulong type.", 
					iCmdLine, (iSigPos + 1));
				return FALSE;
				break;
			}			
		}
		else
		{
			if (((SET_SIG_INFO *)pSigStru)->iSigValueType != VAR_ENUM)
			{
				if (iSigPos < 2)
				{
					AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
						"In plc.cfg, the %d line command error,"
						"input%d signal value type is not enum type.", 
						iCmdLine, (iSigPos + 1));
				}
				else
				{
					AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
						"In plc.cfg, the %d line command error, "
						"Output signal value type is not enum type..",
						iCmdLine);
				}
				return FALSE;
			}
		}
	}

    return TRUE;	
}

/*==========================================================================*
 * FUNCTION : IsControlSigExisted
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_SIG      *pSig    : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-22 12:00
 *==========================================================================*/
static BOOL	IsControlSigExisted(tPLC_SIG *pSig)
{
	int		iRst;
	int		iBufLen;
	void	*pSigStru;

	iRst = PLC_GET_DATA_FAIL;
	iRst = DxiGetData(VAR_A_SIGNAL_INFO_STRU,
				pSig->iEquipID,
				DXI_MERGE_SIG_ID(pSig->bySignalType, pSig->iSignalID),
				&iBufLen,
				&pSigStru,
				0);

	//1.Check signl is existed or not
	if ((iRst != PLC_GET_DATA_SUCCESS))
	{
		AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
					"In plc.cfg [SIGNAL_DEF] field, the config error, "
					"Control signal is not existed.");
				
		return FALSE;
	}
	else
	{
		//2.Check signal value type is right or not
		if (((SET_SIG_INFO *)pSigStru)->iSigValueType != VAR_ENUM)
		{
			AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
						"In plc.cfg [SIGNAL_DEF] field, the config error, "
						"Contorl signal value type is not enum type..");

			return FALSE;
		}
	}

    return TRUE;	
}

/*==========================================================================*
 * FUNCTION : PLC_CheckSigCfg
 * PURPOSE  : All Output signal value must be VAR_ENUM type.All Input signal
 *			  value must VAR_ENUM except that LT and GT Input signal value 
 *			  must be VAR_FLOAT type.Not limit signal type(sampling,alarm or
 *			  setting etc.)
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :	static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-30 15:52
 *==========================================================================*/
static int PLC_CheckSigCfg(void)
{
	int i, iSigPos;
	tPLC_COMMAND *pPlcCmd;
	tPLC_SIG	 *pSig;
	//Check control signal
	if (!IsControlSigExisted(g_pPlcData->g_pCtrlSig))
	{
        return ERR_PLC_INVALID_SIG_CFG;
	}

	pPlcCmd = g_pPlcData->pPlcCmdTbl;
	for(i = 0; i < g_pPlcData->iCmdNums; i++)
	{
		//1.Check signal cfg is right or not
		for (iSigPos = 0; iSigPos < 3; iSigPos++)
		{
			switch (iSigPos)
			{
			case 0:
				pSig = &(pPlcCmd->sigInput1);
				break;
			case 1:
				pSig = &(pPlcCmd->sigInput2);
				break;
			case 2:
				pSig = &(pPlcCmd->sigOutput);
				break;
			default:
				break;
			}	
			
			//It's not a register and a signal cfg EquipID isn't zero
			if (!pSig->blReg)
			{
				//2.Check signal is existed or not
				if (pSig->iEquipID != 0)
				{
					if (!SignalExisted(pSig, pPlcCmd, iSigPos, (i + 1)))
					{					
						return ERR_PLC_INVALID_SIG_CFG;
					}
					else
					{
						//Output signal shouldn't be alarm signal
						if ((iSigPos == 2) && (pSig->bySignalType == SIG_TYPE_ALARM))
						{
							AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
							"In plc.cfg, the %d line command error,"
							"Output signal shouldn't be alarm signal type.", 
							(i + 1));
							return ERR_PLC_INVALID_SIG_CFG;							
						}
					}
				}
			}
			else //It's a register,check register id
			{	
				if (((pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)GreaterThan) ||
					(pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)LessThan)) && 
					iSigPos < 2)
				{
					AppLogOut("PLC_CHK_CFG", APP_LOG_ERROR, 
							"In plc.cfg, the %d line command error,"
							"LT or GT input%d signal shouldn't be Register type.", 
							(i + 1), (iSigPos + 1));
					return ERR_PLC_INVALID_SIG_CFG;
				}			
		
				if ((pSig->iRegIdx >= PLC_REG_NUM) ||
					(pSig->iRegIdx < 0))
				{
					AppLogOut("PLC_CHK_SIG", APP_LOG_ERROR, 
						"The %d lines command register id over limitation.", (i + 1));
					return ERR_PLC_INVALID_SIG_CFG;
				}				
			}
			
		}//End internal signal recycle	

		//2.Check different calculation logic relation
		if (!CheckCalLogic(pPlcCmd, (i + 1)))
		{
			return ERR_PLC_CAL_LOGIC;
		}

		pPlcCmd++;
	} //End command line recycle

	return ERR_PLC_OK;
}
/*==========================================================================*
 * FUNCTION : PLC_CalcFunc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :	void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-26 10:08
 *==========================================================================*/
static void PLC_CalcFunc(void)
{
	int iCounts;
	tPLC_COMMAND *pCurCmd;
	
	//User want to manually control
	SIG_BASIC_VALUE sgCtlvalue;

	if (!GetSignalValue(g_pPlcData->g_pCtrlSig, &sgCtlvalue))
	{
		return ;
	}
	else
	{	
		//User needs to control manually
        if (sgCtlvalue.varValue.enumValue)
		{
			return;
		}
	}

	if (g_pPlcData->iCmdNums == 0)
	{
		return;
	}

	pCurCmd = g_pPlcData->pPlcCmdTbl;

	for (iCounts = 0; iCounts < g_pPlcData->iCmdNums; iCounts++)
	{
		if (pCurCmd->pfnPlcCmd(pCurCmd))
		{
			SetOutRegInvalid(pCurCmd);
		}
		else
		{
			RecoverOutRegValid(pCurCmd);
		}

		pCurCmd++;
	}
	return;
}

/*==========================================================================*
 * FUNCTION : PLC_InitTimers
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :	BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-28 15:26
 *==========================================================================*/
static BOOL PLC_InitTimers(void)
{
	int	 iRst;

	//In plc.cfg PERIOD unit is (s), but at function Timer_Set,nInternal unit 
	//is ms, so (g_pPlcData->iCalPeriod)*1000  s->ms
	if ((g_pPlcData->iCalPeriod < MIN_CALC_PERIOD)
		|| (g_pPlcData->iCalPeriod > MAX_CALC_PERIOD))
	{

#ifdef _DEBUG_PLC_
	printf("Calculate period configuration slop over.\n");
#endif
		AppLogOut("PLC_INI_TIMER", APP_LOG_ERROR, 
			"Calculate period configuration slop over.");
		return FALSE;
	}

	iRst = Timer_Set(g_pPlcData->hThreadSelf, 
		PLC_TM_ID_SCAN_CMD, 
		(g_pPlcData->iCalPeriod)*1000,      
		NULL, 0);	

	if (iRst != ERR_TIMER_OK)
	{
		return FALSE;
	}

	return TRUE;
}
/*==========================================================================*
 * FUNCTION : PLC_DestroyTimers
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :	void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-28 15:26
 *==========================================================================*/
static void PLC_DestroyTimers(void)
{
	Timer_Kill(g_pPlcData->hThreadSelf,	PLC_TM_ID_SCAN_CMD);
	return;
}
/*==========================================================================*
 * FUNCTION : ExitPLC
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   :   void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-26 10:08
 *==========================================================================*/
static void ExitPLC(void)
{
	int iCounts;
	tPLC_COMMAND		*pPlcCmd;
	
	//Avoid clear resource repeatly
	if (g_pPlcData == NULL)
	{
		return;
	}

	iCounts = 0;
	pPlcCmd = g_pPlcData->pPlcCmdTbl;
	
	//Delete timer
	if (g_pPlcData->bTimerInited)
	{
		PLC_DestroyTimers();
	}

	//Delete all delay Calc memory
	if (pPlcCmd != NULL)
	{
		for (iCounts = 0; iCounts < g_pPlcData->iCmdNums; iCounts++)
		{
			if (pPlcCmd->pDelay)
			{
				if (pPlcCmd->pDelay->pInputRcd)
				{
					DELETE(pPlcCmd->pDelay->pInputRcd);
					pPlcCmd->pDelay->pInputRcd = NULL;
				}
				DELETE(pPlcCmd->pDelay);
				pPlcCmd->pDelay = NULL;
			}
			pPlcCmd++;
		}
	}

	//Clear command table	
	if (g_pPlcData->pPlcCmdTbl)
	{
		DELETE(g_pPlcData->pPlcCmdTbl);
		g_pPlcData->pPlcCmdTbl = NULL;
	}	

	//Delete register pointer
	if (g_pPlcData->pbyRegister)
	{
		DELETE(g_pPlcData->pbyRegister);
		g_pPlcData->pbyRegister = NULL;
	}
	
	//Delete control signal
	if (g_pPlcData->g_pCtrlSig)
	{
        DELETE(g_pPlcData->g_pCtrlSig);
		g_pPlcData->g_pCtrlSig = NULL;
	}
	//Delete global variable pointer
	//if (g_pPlcData)
	//{
	//	DELETE(g_pPlcData);
	//	g_pPlcData = NULL;
	//}

	return;
}

/*==========================================================================*
 * FUNCTION : LoadPLCCommand
 * PURPOSE  : 
 * CALLS    : ParsetPLCCmdTableProc
 * CALLED BY: PLC_ReadConfig
 * ARGUMENTS: IN void   *pCfg       : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 10:34
 *==========================================================================*/
static int LoadPLCControlSignal(IN void *pCfg)
{
	int		iControlSigNum;
	CONFIG_TABLE_LOADER loader;
	
	g_pPlcData->g_pCtrlSig = NULL;
	
	DEF_LOADER_ITEM(&loader, NULL, &(iControlSigNum), PLC_CMD_CTRL_SIG, 
		&(g_pPlcData->g_pCtrlSig), ParsePLCCtrlSig);

	if (Cfg_LoadTables(pCfg, 1, &loader) != ERR_CFG_OK)
	{
#ifdef _DEBUG_PLC_
		printf("Fail to LoadPLCControlSignal\n");
#endif
		return ERR_CFG_FAIL;
	}

#ifdef _DEBUG_PLC_
	printf("iControlSigNum is %d in Cfg_LoadTables\n", iControlSigNum);
#endif

	return ERR_CFG_OK;	
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//Start read private configuration functions definition
/*==========================================================================*
 * FUNCTION : PLC_ReadConfig
 * PURPOSE  : 
 * CALLS    : Cfg_ProfileOpen,Cfg_ProfileGetInt,LoadPLCCommand
 * CALLED BY: 
 * ARGUMENTS: IN const char  *szFileName : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 09:16
 *==========================================================================*/
static int PLC_ReadConfig(void)
{
	int  iRst;
	void *pProf;  
	FILE *pFile;
	char *szInFile;
	size_t ulFileLen;

	char szCfgFileName[MAX_FILE_PATH]; 

	Cfg_GetFullConfigPath(PLC_CFG, szCfgFileName, MAX_FILE_PATH);

	/* open file */
	pFile = fopen(szCfgFileName, "r");
	if (pFile == NULL)
	{	
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Fail to open private file %s.Please check file exist or not.", 
				szCfgFileName);
		return ERR_CFG_FILE_OPEN;
	}

	ulFileLen = GetFileLength(pFile);

	szInFile = NEW(char, ulFileLen + 1);
	if (szInFile == NULL)
	{
		fclose(pFile);
		return ERR_CFG_NO_MEMORY;
	}

	/* read file */
	ulFileLen = fread(szInFile, sizeof(char), ulFileLen, pFile);
	fclose(pFile);

	if (ulFileLen == 0) 
	{
		/* clear the memory */
		DELETE(szInFile);

		return ERR_CFG_FILE_READ;
	}
	szInFile[ulFileLen] = '\0';  /* end with NULL */

	/* create SProfile */
	pProf = Cfg_ProfileOpen(szInFile, (int)ulFileLen);
	
	if (pProf == NULL)
	{
		DELETE(szInFile);
		return ERR_CFG_FILE_READ;
	}
	
	//1.Read calculation period
	iRst = Cfg_ProfileGetInt(pProf,
							PLC_CALC_PERIOD, 
							&(g_pPlcData->iCalPeriod)); 
	if (iRst != 1)
	{
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"CalPeriod:Error calculation period format in plc.cfg.");
	}

#ifdef _DEBUG_PLC_
	printf("g_pPlcData->iCalPeriod is %d!\n", g_pPlcData->iCalPeriod);
#endif
	
	iRst = LoadPLCControlSignal(pProf);
	if (iRst != ERR_CFG_OK)
	{
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Fail to load cfg contorl signal.");

		DELETE(pProf); 
		DELETE(szInFile);
		return iRst;
	}

	//2.Read current all command content and command numbers

    iRst = LoadPLCCommand(pProf);
	if (iRst != ERR_CFG_OK)
	{
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Fail to load cfg contorl signal.");

		DELETE(pProf); 
		DELETE(szInFile);
		return iRst;
	}

#ifdef _DEBUG_PLC_
	printf("g_pPlcData->iCmdNums is %d!, iRst is %d\n", g_pPlcData->iCmdNums,
		iRst);
#endif

   	DELETE(pProf); 
	DELETE(szInFile);
	return ERR_CFG_OK;
}

/*==========================================================================*
 * FUNCTION : *PLC_RemoveBracket
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pszBuf : 
 * RETURN   : char : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 19:16
 *==========================================================================*/
static char *PLC_RemoveBracket(char *pszBuf)
{
    char    *p;
    char    *pFirst;
	
	pFirst = pszBuf;
	p = pszBuf;

    while (*p)
    {
        if (IS_LEFT_BRACKET(*p))
        {
			pFirst = p + 1;;       
        }

		if(IS_RIGHT_BRACKET(*p))  //delete right bracket 
		{
			*p = 0;
			break;
		}    
        p++;
    }

    return pFirst;
}

/*==========================================================================*
 * FUNCTION : ParseSigField
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ParsePLCCmdTableProc
 * ARGUMENTS: tPLC_COMMAND  *pCmd   : 
 *            char          *pField : Format:S(m,n,k)
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 15:39
 *==========================================================================*/
static BOOL ParseSigField(tPLC_SIG *pSignal, char *pField)
{
	char * pszBuff;
	char *pBuf;
	
	pBuf = NULL;
	pszBuff = PLC_RemoveBracket(pField);
	if (pszBuff == NULL)
	{
		return FALSE;
	}

#ifdef _DEBUG_PLC_
	printf("pField is %s\n", pszBuff);
#endif
	pSignal->blReg = 0;
	pSignal->iRegIdx = 0;

	//Equipment ID
	pszBuff = Cfg_SplitStringEx(pszBuff, &pBuf, COMMA_SPLIT);
	if (pBuf == NULL)
	{
		return FALSE;
	}
	pSignal->iEquipID = atoi(pBuf);
	
	//Signal ID
	pszBuff = Cfg_SplitStringEx(pszBuff, &pBuf, COMMA_SPLIT);
	if (pBuf == NULL)
	{
		return FALSE;
	}
	pSignal->iSignalID = atoi(pBuf);

	//Signal type
	pszBuff = Cfg_SplitStringEx(pszBuff, &pBuf, COMMA_SPLIT);

	if (pBuf == NULL)
	{
		return FALSE;
	}
	pSignal->bySignalType = atoi(pBuf);

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ParseRegField
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ParsePLCCmdTableProc
 * ARGUMENTS: tPLC_SIG		*pSig  : 
 *            char          *pField : Format:R(n)
 *			  int			iSegPos
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 15:40
 *==========================================================================*/
static BOOL ParseRegField(tPLC_SIG *pSig, char *pField)
{
	char * pszBuff;
	
	pszBuff = PLC_RemoveBracket(pField);

	if (pszBuff == NULL)
	{
		return FALSE;
	}

	pSig->blReg = TRUE;
	pSig->iRegIdx = atoi(pszBuff);
	if ((pSig->iRegIdx > PLC_REG_NUM) || 
		(pSig->iRegIdx < 0))
	{
		return FALSE;
	}
	pSig->bValValid = TRUE;

	pSig->iEquipID = 0;
	pSig->bySignalType = 0;
	pSig->iSignalID = 0;
	

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : SetWhiteSigField
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ParsePLCCmdTableProc
 * ARGUMENTS: tPLC_SIG *pSignal   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 15:40
 *==========================================================================*/
static void SetWhiteSigField(tPLC_SIG *pSignal)
{
	pSignal->blReg = 0;
	pSignal->iRegIdx = 0;
	pSignal->iEquipID = 0;
	pSignal->bySignalType = 0;
	pSignal->iSignalID = 0;

	return;
}
/*==========================================================================*
 * FUNCTION : ParseParaField
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: ParsePLCCmdTableProc
 * ARGUMENTS: float  *pfValue : 
 *            char   *pField  : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 19:07
 *==========================================================================*/
static BOOL ParseParaField(float *pfValue, char *pField)
{
	char * pszBuff;

	pszBuff = PLC_RemoveBracket(pField);

	if (pszBuff == NULL)
	{
		return FALSE;
	}

	*pfValue = (float)atof(pszBuff);
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : ParsePLCCtrlSig
 * PURPOSE  : parsing [SIGNAL_DEF] 
 * CALLS    : Cfg_SplitStringEx
 * CALLED BY: LoadPLCCommand
 * ARGUMENTS: IN char              *szBuf       : line data
 *            OUT tPLC_SIG	       *pSig :        to store parsed data
 * RETURN   : int : (-1) loaded failed , zero for success
 * COMMENTS : 
 * CREATOR  : LIXIDONG                   DATE: 2004-11-25 09:49
 *==========================================================================*/
static int ParsePLCCtrlSig(IN char *szBuf, OUT tPLC_SIG *pSig)
{
	char	*pField;
	int		iTemNum;

	if ((szBuf == NULL) || (pSig == NULL))
	{
		return (-1);
	}

	/* 1.The function point of PLC signal name field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);

	/* 2.The EquipID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);
	iTemNum = atoi(pField);
	pSig->iEquipID = iTemNum;	

#ifdef _DEBUG_PLC_
	printf("pSig->iEquipID is %d\n", pSig->iEquipID);
#endif

	/* 3.The Signal Type field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);
	iTemNum = atoi(pField);
	pSig->bySignalType = (BYTE)iTemNum;

#ifdef _DEBUG_PLC_
	printf("pSig->bySignalType is %u\n", pSig->bySignalType);
#endif

	/* 4.The Signal ID field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);
	iTemNum = atoi(pField);
	pSig->iSignalID = iTemNum;

#ifdef _DEBUG_PLC_
	printf("pSig->iSignalID is %u\n", pSig->iSignalID);
#endif

	return 0;
}
/*==========================================================================*
 * FUNCTION : ParsetPLCCmdTableProc
 * PURPOSE  : parsing [PLC_CMD] table of MainConfig.run file
 * CALLS    : Cfg_SplitStringEx
 * CALLED BY: LoadPLCCommand
 * ARGUMENTS: IN char              *szBuf       : line data
 *            OUT tPLC_COMMAND	   *pStructData : to store parsed data
 * RETURN   : int : (-1) loaded failed , zero for success
 * COMMENTS : 
 * CREATOR  : LIXIDONG                   DATE: 2004-11-25 09:49
 *==========================================================================*/
static int ParsePLCCmdTableProc(IN char *szBuf, OUT tPLC_COMMAND *pCmd)
{
	char	*pField;
	tDELAY  *pDelay;
	int	    iSize, i;

	if ((szBuf == NULL) || (pCmd == NULL))
	{
		return (-1);
	}

	/* 1.The function point of PLC Command field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);
	GetCallBackFunc(pCmd, pField);	

	/* 2.The First Input field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);

	if (IS_SIGNAL(*pField))
	{		
		if (!ParseSigField(&(pCmd->sigInput1), pField))
		{

#ifdef _DEBUG_PLC_
			printf("Signal Input1:Error signal format in plc.cfg.\n");
#endif
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Signal Input1:Error signal format in plc.cfg.");
			return (-1);
		}
	}
	else if(IS_REGISTER(*pField))
	{
		if (!ParseRegField(&(pCmd->sigInput1), pField))
		{
#ifdef _DEBUG_PLC_
			printf("Register Input1:Error signal format in plc.cfg.\n");
#endif
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Register Input1:Error register format in plc.cfg.");
			return (-1);
		}
	}
	else if (IS_EMPTY(*pField))
	{
		SetWhiteSigField(&(pCmd->sigInput1)); //set zero
	}
	else
	{
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"Input1:Error format in plc.cfg.");
		return (-1);
	}

	/* 3.The Second Input field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);
	if (IS_SIGNAL(*pField))
	{	
		if (!ParseSigField(&(pCmd->sigInput2), pField))
		{
#ifdef _DEBUG_PLC_
			printf("Signal Input2:Error signal format in plc.cfg.\n");
#endif
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Signal Input2:Error signal format in plc.cfg.");
			return (-1);
		}
	}
	else if(IS_REGISTER(*pField))
	{
		if (!ParseRegField(&(pCmd->sigInput2), pField))
		{
#ifdef _DEBUG_PLC_
			printf("Register Input2:Error signal format in plc.cfg.\n");
#endif
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Register Input2:Error register format in plc.cfg.");
			return (-1);
		}
	}
	else if (IS_EMPTY(*pField))
	{
		SetWhiteSigField(&(pCmd->sigInput2)); //set zero
	}
	else
	{
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"Input2:Error format in plc.cfg.");
		return (-1);
	}

	/* 4.First Parameter field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);
	if (IS_PARAMETER(*pField))
	{
		if (!ParseParaField(&(pCmd->fParameter1), pField))
		{
#ifdef _DEBUG_PLC_
			printf("Param1:Error parameter format in plc.cfg.\n");
#endif
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"Param1:Error parameter format in plc.cfg.");
			return (-1);
		}
	}
	else if (IS_EMPTY(*pField))
	{
		if ((pCmd->pfnPlcCmd == (PLC_CMD_PROC)GreaterThan) ||
			(pCmd->pfnPlcCmd == (PLC_CMD_PROC)LessThan) ||
			(pCmd->pfnPlcCmd == (PLC_CMD_PROC)SetCalc) ||
			(pCmd->pfnPlcCmd == (PLC_CMD_PROC)DelayCalc))
		{
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"The command line logic error,"
				"GT,LT,SET and DS Parameter1 can't be empty");
			return (-1);
		}//empty
	}
	else
	{
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"Param1:Error parameter format in plc.cfg.");
			return (-1);
	}

	/* 5.Scecond Parameter field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);
	if (IS_PARAMETER(*pField))
	{
		if (!ParseParaField(&(pCmd->fParameter2), pField))
		{
#ifdef _DEBUG_PLC_
			printf("Param2:Error parameter format in plc.cfg.\n");
#endif
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"Param2:Error parameter format in plc.cfg.");
			return (-1);
		}
	}
	else if (IS_EMPTY(*pField))
	{
		if ((pCmd->pfnPlcCmd == (PLC_CMD_PROC)GreaterThan) ||
			(pCmd->pfnPlcCmd == (PLC_CMD_PROC)LessThan))
		{
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"The command line logic error,"
				"GT,LT Parameter2 can't be empty.");
			return (-1);
		}//empty //empty
	}
	else
	{
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"Param2:Error parameter format in plc.cfg.");
			return (-1);
	}

	/* 6.The Output field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, PLC_SPLITTER);
	if (IS_SIGNAL(*pField))
	{		
		if (!ParseSigField(&(pCmd->sigOutput), pField))
		{
#ifdef _DEBUG_PLC_
			printf("Output field:Error signal format in plc.cfg.\n");
#endif
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Output field:Error signal format in plc.cfg.");
			return (-1);
		}
	}
	else if(IS_REGISTER(*pField))
	{
		if (!ParseRegField(&(pCmd->sigOutput), pField))
		{
#ifdef _DEBUG_PLC_
			printf("Output field:Error register format in plc.cfg.\n");
#endif
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
				"Output:Error register format in plc.cfg.");
			return (-1);
		}
	}
	else if (IS_EMPTY(*pField))
	{
		SetWhiteSigField(&(pCmd->sigOutput)); //set zero
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"Output:Any calculation's output can't be empty."
			"It must be signal or register.");
		return (-1);
	}
	else
	{
		AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"Output:Error format in plc.cfg.");
		return (-1);
	}
	
	//Process delay information
	i = 0;
	if (pCmd->pDelay)
	{
		pDelay = pCmd->pDelay;
		
        iSize  = (pCmd->fParameter1 - 1)/g_pPlcData->iCalPeriod + 1;

		pDelay->pInputRcd = NEW(BYTE, iSize);
		if (pDelay->pInputRcd == NULL)
		{
			AppLogOut("PLC_READ_CFG", APP_LOG_ERROR, 
			"No enough memory.");
		}
		else
		{
			//Set all first value is zero
			while( i < iSize)
			{
				*(pDelay->pInputRcd + i) = 0;
				i++;
			}
		}
		pDelay->iRcdOutPos = 0;
		pDelay->iRcdArySize = iSize;
	}

	return 0;
}
/*==========================================================================*
 * FUNCTION : LoadPLCCommand
 * PURPOSE  : 
 * CALLS    : ParsetPLCCmdTableProc
 * CALLED BY: PLC_ReadConfig
 * ARGUMENTS: IN void   *pCfg       : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 10:34
 *==========================================================================*/
static int LoadPLCCommand(IN void *pCfg)
{
	CONFIG_TABLE_LOADER loader;

	g_pPlcData->pPlcCmdTbl = NULL;

	DEF_LOADER_ITEM(&loader, NULL, &(g_pPlcData->iCmdNums), PLC_CMD_SECTION, 
		&(g_pPlcData->pPlcCmdTbl), ParsePLCCmdTableProc);

	if (Cfg_LoadTables(pCfg, 1, &loader) != ERR_CFG_OK)
	{
#ifdef _DEBUG_PLC_
		printf("Fail to LoadPLCCommand\n");
#endif
		return ERR_CFG_FAIL;
	}

#ifdef _DEBUG_PLC_
	printf("g_pPlcData->iCmdNums is %d in Cfg_LoadTables\n", g_pPlcData->iCmdNums);
#endif

	return ERR_CFG_OK;	
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//Start logic functions definition
/*==========================================================================*
 * FUNCTION : GetSignalValue
 * PURPOSE  : 
 * CALLS    : DxiGetData
 * CALLED BY: All logic functions
 * ARGUMENTS: tPLC_COMMAND  *pCmd   : 
 *            SIG_BASIC_VALUE		*pValue
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 15:39
 *==========================================================================*/
static BOOL GetSignalValue(tPLC_SIG *pSignal, SIG_BASIC_VALUE *pValue)
{
	int iError;
	int iInterfaceType;
	int iBufLen;
	int iVarID, iVarSubID;
	int iTimeOut;

	iError			= 0;
	iBufLen			= sizeof(VAR_VALUE);
	iInterfaceType	= VAR_A_SIGNAL_VALUE;
	iTimeOut		= 200;

	//1.Get data
	SIG_BASIC_VALUE * pSigVal;

	iVarID			= pSignal->iEquipID;

	iVarSubID		= DXI_MERGE_SIG_ID(pSignal->bySignalType, 
		pSignal->iSignalID);

	iError = DxiGetData(iInterfaceType,
			iVarID,			
			iVarSubID,		
			&iBufLen,			
			&pSigVal,			
			iTimeOut);	

	if (iError != ERR_PLC_OK)
	{
		return FALSE;
	}

    /*Added by YangGuoxin:In order to judge the rectifier number. 8/19/2006*/
    SIG_BASIC_VALUE* pRectSigValue;
    int iRectGroupEquipID = DXI_GetEquipIDFromStdEquipID(200);

    if(iVarID > iRectGroupEquipID && iVarID <= iRectGroupEquipID + 100)
    {

        iError = DxiGetData(VAR_A_SIGNAL_VALUE,
            iRectGroupEquipID,	//Rectifier group Equip ID		
            DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 8),		
            &iBufLen,			
            (void *)&pRectSigValue,			
            iTimeOut);

        if (iError != ERR_PLC_OK)
        {
            return FALSE;
        }
        else
        {
            /*exceed the valid rectifier number*/
            if(iVarID > pRectSigValue->varValue.ulValue + iRectGroupEquipID)
            {
                return FALSE;
            }
        }
    }
    /*End by YangGuoxin. 8/19/2006*/

    /*Added by wj:In order to judge the ACD number. 8/19/2006*/
    //SIG_BASIC_VALUE* pACDSigValue;
    //int iACDGroupEquipID = DXI_GetEquipIDFromStdEquipID(400);

    //if(iVarID > iACDGroupEquipID && iVarID <= iACDGroupEquipID + 5)
    //{

    //    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
    //        iACDGroupEquipID,	//ACD group Equip ID		
    //        DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1),		
    //        &iBufLen,			
    //        (void *)&pACDSigValue,			
    //        iTimeOut);

    //    if (iError != ERR_PLC_OK)
    //    {
    //        return FALSE;
    //    }
    //    else
    //    {
    //        /*exceed the valid ACD number*/
    //        if(iVarID > pACDSigValue->varValue.ulValue + iACDGroupEquipID)
    //        {
    //            return FALSE;
    //        }
    //    }
    //}
    /*End by wj. 8/19/2006*/

    /*Added by WJ:In order to judge the DCD number. 8/19/2006*/
    //SIG_BASIC_VALUE* pDCDSigValue;
    //int iDCDGroupEquipID = DXI_GetEquipIDFromStdEquipID(500);

    //if(iVarID > iDCDGroupEquipID && iVarID <= iDCDGroupEquipID + 10)
    //{

    //    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
    //        iDCDGroupEquipID,	//Rectifier group Equip ID		
    //        DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 1),		
    //        &iBufLen,			
    //        (void *)&pDCDSigValue,			
    //        iTimeOut);

    //    if (iError != ERR_PLC_OK)
    //    {
    //        return FALSE;
    //    }
    //    else
    //    {
    //        /*exceed the valid rectifier number*/
    //        if(iVarID > pDCDSigValue->varValue.ulValue + iDCDGroupEquipID)
    //        {
    //            return FALSE;
    //        }
    //    }
    //}
    /*End by WJ. 8/19/2006*/

    ///*Added by WJ:In order to judge the Batt number. 8/19/2006*/
    //SIG_BASIC_VALUE* pBattSigValue;
    //int iBattGroupEquipID = DXI_GetEquipIDFromStdEquipID(300);

    //if(iVarID > iBattGroupEquipID && iVarID <= iBattGroupEquipID + 20)
    //{

    //    iError = DxiGetData(VAR_A_SIGNAL_VALUE,
    //        iBattGroupEquipID,	//Rectifier group Equip ID		
    //       // DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 63),	
		  //  DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, 40),
    //        &iBufLen,			
    //        (void *)&pBattSigValue,			
    //        iTimeOut);

    //    if (iError != ERR_PLC_OK)
    //    {
    //        return FALSE;
    //    }
    //    else
    //    {
    //        /*exceed the valid rectifier number*/
    //        if(iVarID > pBattSigValue->varValue.ulValue + iBattGroupEquipID)
    //        {
    //            return FALSE;
    //        }
    //    }
    //}
    /*End by WJ. 8/19/2006*/


	if (!SIG_VALUE_IS_VALID(pSigVal))
	{
		return FALSE;
	}

	memmove(pValue, pSigVal, (unsigned)sizeof(SIG_BASIC_VALUE));

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : SetSignalValue
 * PURPOSE  : 
 * CALLS    : DxiGetData
 * CALLED BY: All logic functions
 * ARGUMENTS: tPLC_COMMAND  *pCmd   : 
 *            SIG_BASIC_VALUE		*pValue
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 15:39
 *==========================================================================*/
static BOOL SetSignalValue(tPLC_SIG *pSignal, SIG_BASIC_VALUE *pValue)
{
	int iError;
	int iInterfaceType;
	int iBufLen;
	int iVarID, iVarSubID;
	int iTimeOut;
	char *pszPlcName = "PLC";
	VAR_VALUE_EX varValueEx;

	memset(&varValueEx, 0, sizeof(varValueEx));
	varValueEx.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
	varValueEx.nSenderType	 = SERVICE_OF_LOGIC_CONTROL;
	varValueEx.pszSenderName = pszPlcName;

	memmove(&(varValueEx.varValue), &(pValue->varValue), 
		(size_t)sizeof(VAR_VALUE));

	iError			= 0;
	iBufLen			= sizeof(VAR_VALUE_EX);
	iInterfaceType	= VAR_A_SIGNAL_VALUE;
	iTimeOut		= 0;
	iVarID			= pSignal->iEquipID;;

	iVarSubID		= DXI_MERGE_SIG_ID(pSignal->bySignalType, 
		pSignal->iSignalID);

	iError = DxiSetData(iInterfaceType,
			iVarID,			
			iVarSubID,		
			iBufLen,			
			(void *)&(varValueEx),			
			iTimeOut);

	if (iError == ERR_DXI_INVALID_DATA_VALUE)
	{
		return FALSE;
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : SetCalc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pArgs : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:53
 *==========================================================================*/
static BOOL SetCalc(tPLC_COMMAND *pArgs)
{
	SIG_BASIC_VALUE value;
	int iRegID;

	(value.varValue).enumValue = pArgs->fParameter1;	

	//SIG_TYPE_CONTROL
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = pArgs->fParameter1;
	}
	else
	{	
		if (!(SetSignalValue(&(pArgs->sigOutput), &value)))
		{
			return FALSE;
		}
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : AndCalc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pArgs : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:53
 *==========================================================================*/
static BOOL AndCalc(tPLC_COMMAND *pArgs)
{

	SIG_BASIC_VALUE valueIn1, valueIn2, valueOut;
	BOOL bValidSig1, bValidSig2;
	int	iRegID;

	bValidSig1 = FALSE;
	bValidSig2 = FALSE;

	if (pArgs->sigInput1.blReg)
	{
		if (pArgs->sigInput1.bValValid)
		{
			iRegID = pArgs->sigInput1.iRegIdx;
			valueIn1.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
			bValidSig1 = TRUE;
		}		
	}
	else
	{
		if (GetSignalValue(&(pArgs->sigInput1), &valueIn1))
		{	
			bValidSig1 = TRUE;		
		}
	}

	if (pArgs->sigInput2.blReg)
	{
		if (pArgs->sigInput2.bValValid)
		{
			iRegID = pArgs->sigInput2.iRegIdx;
			valueIn2.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
			bValidSig2 = TRUE;
		}
	}
	else
	{	
		if (GetSignalValue(&(pArgs->sigInput2), &valueIn2))
		{	
			bValidSig2 = TRUE;
		}
	}

	if (((!bValidSig1) && (!bValidSig2)) ||
		((!bValidSig1) && (bValidSig2) && (valueIn2.varValue.enumValue)) ||
		((bValidSig1) && (!bValidSig2) && (valueIn1.varValue.enumValue)))
	{
		return FALSE;
	}
	

	//2.And input1's value and input2's value
	valueOut.varValue.enumValue = valueIn1.varValue.enumValue && 
		valueIn2.varValue.enumValue;

	//3.Set output
	//SIG_TYPE_CONTROL
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = valueOut.varValue.enumValue;
	}
	else
	{
		if (!(SetSignalValue(&(pArgs->sigOutput), &valueOut)))
		{
			return FALSE;
		}
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : OrCalc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pArgs : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:54
 *==========================================================================*/
static BOOL OrCalc(tPLC_COMMAND *pArgs)
{
	
	SIG_BASIC_VALUE valueIn1, valueIn2, valueOut;
	BOOL bValidSig1, bValidSig2;
	int	iRegID;

	bValidSig1 = FALSE;
	bValidSig2 = FALSE;

	if (pArgs->sigInput1.blReg)
	{
		if (pArgs->sigInput1.bValValid)
		{
			iRegID = pArgs->sigInput1.iRegIdx;
			valueIn1.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
			bValidSig1 = TRUE;
		}
	}
	else
	{
		if (GetSignalValue(&(pArgs->sigInput1), &valueIn1))
		{	
			bValidSig1 = TRUE;	
		}
	}

	if (pArgs->sigInput2.blReg)
	{
		if (pArgs->sigInput2.bValValid)
		{
			iRegID = pArgs->sigInput2.iRegIdx;
			valueIn2.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
			bValidSig2 = TRUE;
		}
	}
	else
	{	
		if (GetSignalValue(&(pArgs->sigInput2), &valueIn2))
		{	
			bValidSig2 = TRUE;
		}
	}

	if (((!bValidSig1) && (!bValidSig2)) ||
		((!bValidSig1) && (bValidSig2) && (!(valueIn2.varValue.enumValue))) ||
		((bValidSig1) && (!bValidSig2) && (!(valueIn1.varValue.enumValue))))
	{
		return FALSE;
	}
	

	//2.And input1's value and input2's value
	valueOut.varValue.enumValue = valueIn1.varValue.enumValue || 
		valueIn2.varValue.enumValue;

	//3.Set output
	//SIG_TYPE_CONTROL
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = valueOut.varValue.enumValue;
	}
	else
	{
		if (!(SetSignalValue(&(pArgs->sigOutput), &valueOut)))
		{
			return FALSE;
		}
	}

	return TRUE;
	
}

/*==========================================================================*
 * FUNCTION : NotCalc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pArgs : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:54
 *==========================================================================*/
static BOOL NotCalc(tPLC_COMMAND *pArgs)
{
	SIG_BASIC_VALUE valueIn1, valueOut;
	int iRegID;

	if (pArgs->sigInput1.blReg)
	{
		if (pArgs->sigInput1.bValValid)
		{
			iRegID = pArgs->sigInput1.iRegIdx;
			//valueOut.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
			valueIn1.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		if (!GetSignalValue(&(pArgs->sigInput1), &valueIn1))
		{
			return FALSE;
		}	
	}

	//2.And input1's value and input2's value
	valueOut.varValue.enumValue = !(valueIn1.varValue.enumValue);

	//3.Set output
	//SIG_TYPE_CONTROL	
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = valueOut.varValue.enumValue;
	}
	else
	{
		if (!(SetSignalValue(&(pArgs->sigOutput), &valueOut)))
		{
			return FALSE;
		}
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : XorCalc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pArgs : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:54
 *==========================================================================*/
static BOOL XorCalc(tPLC_COMMAND *pArgs)
{
	SIG_BASIC_VALUE valueIn1, valueIn2, valueOut;
	int	iRegID;

	if (pArgs->sigInput1.blReg)
	{
		if (pArgs->sigInput1.bValValid)
		{
			iRegID = pArgs->sigInput1.iRegIdx;
			valueIn1.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		if (!GetSignalValue(&(pArgs->sigInput1), &valueIn1))
		{
			return FALSE;
		}	
	}

	if (pArgs->sigInput2.blReg)
	{
		if (pArgs->sigInput2.bValValid)
		{
			iRegID = pArgs->sigInput2.iRegIdx;
			valueIn2.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
		}
		else
		{
			return FALSE;
		}
	}
	else
	{	
		if (!GetSignalValue(&(pArgs->sigInput2), &valueIn2))
		{
			return FALSE;
		}	
	}

	//2.XOR input1's value and input2's value (A(!B) + (!A)B)
	valueOut.varValue.enumValue = 
		(valueIn1.varValue.enumValue && (!(valueIn2.varValue.enumValue))) || 
		((!(valueIn1.varValue.enumValue)) && (valueIn2.varValue.enumValue));

	//3.Set output
	//SIG_TYPE_CONTROL
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = valueOut.varValue.enumValue;
	}
	else
	{
		if (!(SetSignalValue(&(pArgs->sigOutput), &valueOut)))
		{
			return FALSE;
		}
	}

	return TRUE;	
}

/*==========================================================================*
 * FUNCTION : GreaterThan
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pArgs : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:55
 *==========================================================================*/
static BOOL GreaterThan(tPLC_COMMAND *pArgs)
{
	SIG_BASIC_VALUE valueIn1, valueIn2, valueOut;
	int iRegID;
	BOOL	bSigExisted2;

	bSigExisted2 = FALSE;
	//1.Get GreaterThan input1's value
	if (!GetSignalValue(&(pArgs->sigInput1), &valueIn1))
	{
		return FALSE;
	}	
	
	//If input2 existed
	if ((pArgs->sigInput2.iEquipID != 0) || 
		(pArgs->sigInput2.iSignalID != 0))
	{
		bSigExisted2 = TRUE;
		if (!GetSignalValue(&(pArgs->sigInput2), &valueIn2))
		{
			return FALSE;
		}
	}
		
	//3.Compare GreaterThan value	
	switch(pArgs->sigInput1.bySigValueType)
	{
	case VAR_FLOAT:
		if (bSigExisted2)
		{
			if (valueIn1.varValue.fValue > valueIn2.varValue.fValue)
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.fValue 
				< (valueIn2.varValue.fValue - pArgs->fParameter2))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		else 
		{
			if (valueIn1.varValue.fValue > pArgs->fParameter1)
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.fValue 
				< (pArgs->fParameter1 - pArgs->fParameter2))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		break;
	case VAR_LONG:
		if (bSigExisted2)
		{
			if (valueIn1.varValue.lValue > valueIn2.varValue.lValue)
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.lValue 
				<= (valueIn2.varValue.lValue - (long)(pArgs->fParameter2)))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		else 
		{
			if (valueIn1.varValue.lValue > (long)(pArgs->fParameter1))
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.lValue 
				<= ((long)(pArgs->fParameter1) - (long)(pArgs->fParameter2)))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		break;
	case VAR_UNSIGNED_LONG:
		if (bSigExisted2)
		{
			if (valueIn1.varValue.ulValue > valueIn2.varValue.ulValue)
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.ulValue 
				<= (valueIn2.varValue.ulValue 
				- (unsigned long)(pArgs->fParameter2)))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		else 
		{
			if (valueIn1.varValue.ulValue > (unsigned long)(pArgs->fParameter1))
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.ulValue 
				<= ((unsigned long)(pArgs->fParameter1) 
				- (unsigned long)(pArgs->fParameter2)))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		break;
	default:
		return FALSE;
		break;
	}
	
	//3.Set output
	//SIG_TYPE_CONTROL
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = valueOut.varValue.enumValue;
	}
	else
	{
		if (!(SetSignalValue(&(pArgs->sigOutput), &valueOut)))
		{
			return FALSE;
		}
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : LessThan
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pArgs : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:56
 *==========================================================================*/
static BOOL LessThan(tPLC_COMMAND *pArgs)
{
	SIG_BASIC_VALUE valueIn1, valueIn2, valueOut;
	int iRegID;
	BOOL	bSigExisted2;

	bSigExisted2 = FALSE;
	//1.Get LessThan input1's value
	if (!GetSignalValue(&(pArgs->sigInput1), &valueIn1))
	{
		return FALSE;
	}	
	
	//If input2 existed
	if ((pArgs->sigInput2.iEquipID != 0) || 
		(pArgs->sigInput2.iSignalID != 0))
	{
		bSigExisted2 = TRUE;
		if (!GetSignalValue(&(pArgs->sigInput2), &valueIn2))
		{
			return FALSE;
		}
	}
		
	//3.Compare LessThan value	
	switch(pArgs->sigInput1.bySigValueType)
	{
	case VAR_FLOAT:
		if (bSigExisted2)
		{
			if (valueIn1.varValue.fValue < valueIn2.varValue.fValue)
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.fValue 
				> (valueIn2.varValue.fValue + pArgs->fParameter2))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		else 
		{
			if (valueIn1.varValue.fValue < pArgs->fParameter1)
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.fValue 
				> (pArgs->fParameter1 + pArgs->fParameter2))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		break;
	case VAR_LONG:
		if (bSigExisted2)
		{
			if (valueIn1.varValue.lValue < valueIn2.varValue.lValue)
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.lValue 
				>= (valueIn2.varValue.lValue + (long)(pArgs->fParameter2)))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		else 
		{
			if (valueIn1.varValue.lValue < (long)(pArgs->fParameter1))
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.lValue 
				>= ((long)(pArgs->fParameter1) + (long)(pArgs->fParameter2)))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		break;
	case VAR_UNSIGNED_LONG:
		if (bSigExisted2)
		{
			if (valueIn1.varValue.ulValue < valueIn2.varValue.ulValue)
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.ulValue 
				>= (valueIn2.varValue.ulValue 
				+ (unsigned long)(pArgs->fParameter2)))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		else 
		{
			if (valueIn1.varValue.ulValue < (unsigned long)(pArgs->fParameter1))
			{
				valueOut.varValue.enumValue = 1;
			}
			else if (valueIn1.varValue.ulValue 
				>= ((unsigned long)(pArgs->fParameter1) 
				+ (unsigned long)(pArgs->fParameter2)))
			{
				valueOut.varValue.enumValue = 0;
			}
			else
			{
				return FALSE; //Do nothing
			}
		}
		break;
	default:
		return FALSE;
		break;
	}
	
	//3.Set output
	//SIG_TYPE_CONTROL
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = valueOut.varValue.enumValue;
	}
	else
	{
		if (!(SetSignalValue(&(pArgs->sigOutput), &valueOut)))
		{
			return FALSE;
		}
	}

	return TRUE;
	/*
	SIG_BASIC_VALUE valueIn1, valueOut;
	int iRegID;

	if (pArgs->sigInput1.blReg)
	{
		iRegID = pArgs->sigInput1.iRegIdx;
		valueIn1.varValue.fValue = *(g_pPlcData->pbyRegister + iRegID);
	}
	else
	{
		if (!GetSignalValue(&(pArgs->sigInput1), &valueIn1))
		{
			return FALSE;
		}
	}
	

	//2.GreaterThan input1's value 
	if (valueIn1.varValue.fValue < pArgs->fParameter1)
	{
		valueOut.varValue.enumValue = 1;
	}
	else if (valueIn1.varValue.fValue > 
		(pArgs->fParameter1 - pArgs->fParameter2))
	{
		valueOut.varValue.enumValue = 0;
	}
	else
	{
		return FALSE; //Do nothing
	}
	
	//3.Set output
	//SIG_TYPE_CONTROL
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = valueOut.varValue.enumValue;
	}
	else
	{
		if (!(SetSignalValue(&(pArgs->sigOutput), &valueOut)))
		{		
			return FALSE;
		}
	}
	
	return TRUE;
	*/ //Old calculation
}

/*==========================================================================*
 * FUNCTION : DelayCalc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pArgs : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 13:56
 *==========================================================================*/
static BOOL DelayCalc(tPLC_COMMAND *pArgs)
{
	SIG_BASIC_VALUE valueIn1, valueOut;
	BYTE *pVal;
	int	iPos;
	int iRegID;

	if (pArgs->sigInput1.blReg)
	{
		if (pArgs->sigInput1.bValValid)
		{
			iRegID = pArgs->sigInput1.iRegIdx;
			valueIn1.varValue.enumValue = *(g_pPlcData->pbyRegister + iRegID);
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		if (!GetSignalValue(&(pArgs->sigInput1), &valueIn1))
		{
			return FALSE;
		}	
	}	

	//2.Set output from current register firstly
	//SIG_TYPE_CONTROL
	pVal = pArgs->pDelay->pInputRcd;
	valueOut.varValue.enumValue = *(pVal + pArgs->pDelay->iRcdOutPos);

#ifdef _PLC_TEST_DS_
		printf("Current delay cmd size is %d\n", pArgs->pDelay->iRcdArySize);
		printf("Current delay cmd pos is %d\n", pArgs->pDelay->iRcdOutPos);
		printf("Current delay value is %d\n", valueOut.varValue.enumValue);
#endif
	if (pArgs->sigOutput.blReg)
	{
		iRegID = pArgs->sigOutput.iRegIdx;
		*(g_pPlcData->pbyRegister + iRegID) = valueOut.varValue.enumValue;
	}
	else
	{
		if (!(SetSignalValue(&(pArgs->sigOutput), &valueOut)))
		{		
			return FALSE;
		}
	}
	

	//3.Put current acquisite value into current register 	
	iPos = pArgs->pDelay->iRcdOutPos;
	*(pVal + iPos) = valueIn1.varValue.enumValue;

	if (iPos >= (pArgs->pDelay->iRcdArySize - 1))
	{
		pArgs->pDelay->iRcdOutPos = 0;
	}
	else
	{
		pArgs->pDelay->iRcdOutPos = iPos + 1;
	}

#ifdef _PLC_TEST_DS_
		printf("Next delay cmd size is %d\n", pArgs->pDelay->iRcdArySize);
		printf("Next delay cmd pos is %d\n", pArgs->pDelay->iRcdOutPos);
		printf("Input new value is %d\n", valueIn1.varValue.enumValue);
#endif
	return TRUE;
}

/*==========================================================================*
 * FUNCTION : *GetCallBackFunc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  *pFieldStr : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-11-25 14:45
 *==========================================================================*/
static void GetCallBackFunc(tPLC_COMMAND *pPlcCmd, char *pFieldStr)
{
	PLC_CMD_PROC plcCurrFunc;

#ifdef _DEBUG_PLC_
	printf("In GetCallBackFunc, pFieldStr is %s\n", pFieldStr);
#endif

	if (strcmp(pFieldStr, PLC_SET) == 0)
	{
		plcCurrFunc = (PLC_CMD_PROC)SetCalc;
	}
	else if (strcmp(pFieldStr, PLC_AND) == 0)
	{
		plcCurrFunc = (PLC_CMD_PROC)AndCalc;
	}
	else if(strcmp(pFieldStr, PLC_OR) == 0)
	{
		plcCurrFunc = (PLC_CMD_PROC)OrCalc;

#ifdef _DEBUG_PLC_
		printf("Come to GetCallBackFunc - Or.\n");
#endif
	}
	else if (strcmp(pFieldStr, PLC_NOT) == 0)
	{
		plcCurrFunc = (PLC_CMD_PROC)NotCalc;
	}
	else if(strcmp(pFieldStr, PLC_XOR) == 0)
	{
        plcCurrFunc = (PLC_CMD_PROC)XorCalc;
	}
	else if (strcmp(pFieldStr, PLC_GT) == 0)
	{
		plcCurrFunc = (PLC_CMD_PROC)GreaterThan;
	}
	else if (strcmp(pFieldStr, PLC_LT) == 0)
	{
		plcCurrFunc = (PLC_CMD_PROC)LessThan;
	}
	else if (strcmp(pFieldStr, PLC_DS) == 0)
	{
		pPlcCmd->pfnPlcCmd = (PLC_CMD_PROC)DelayCalc;
		pPlcCmd->pDelay = NEW(tDELAY, 1);
		return;
		
	}
	else
	{
		return;
	}	

	pPlcCmd->pfnPlcCmd = plcCurrFunc;

#ifdef _DEBUG_PLC_
		printf("pPlcCmd->pfnPlcCmd is %d.\n", pPlcCmd->pfnPlcCmd);
#endif
	pPlcCmd->pDelay = NULL;	
	return;
}

/*==========================================================================*
 * FUNCTION : IsValidSignalCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_SIG  *pSig : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-22 14:44
 *==========================================================================*/
static BOOL IsValidSignalCfg(tPLC_SIG *pSig)
{
	if (pSig->blReg)   //It is register
	{
		return TRUE;
	}
	else   
	{
		if ((pSig->iEquipID == 0)
			&& (pSig->bySignalType == 0)
			&& (pSig->iSignalID == 0))
		{   
			//It's not a signal 
			return FALSE;
		}
	}	

	return TRUE;
}
/*==========================================================================*
 * FUNCTION : CheckCalLogic
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: tPLC_COMMAND  *pPlcCmd : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-22 13:33
 *==========================================================================*/
static BOOL CheckCalLogic(tPLC_COMMAND *pPlcCmd, int iCmdLine)
{
	if (pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)SetCalc)
	{
		//The SET calculation need pPlcCmd pPlcCmd->fParameter1 and
		//pPlcCmd->sigOutput must existed
		//fParameter1 has been checked in ParsePLCCmdTableProc
		if (!IsValidSignalCfg(&(pPlcCmd->sigOutput)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line SET Output is not correct.", iCmdLine);
			return FALSE;
		}
	}
	else if (pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)AndCalc)
	{
		//The AND calculation need pPlcCmd pPlcCmd->sigInput1,2 and
		//pPlcCmd->sigOutput must existed
		if (!IsValidSignalCfg(&(pPlcCmd->sigInput1)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line AND Input1 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigInput2)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line AND Input2 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigOutput)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line AND Output is not correct.", iCmdLine);
			return FALSE;
		}
	}
	else if(pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)OrCalc)
	{
		//The OR calculation need pPlcCmd pPlcCmd->sigInput1,2 and
		//pPlcCmd->sigOutput must existed
		if (!IsValidSignalCfg(&(pPlcCmd->sigInput1)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line OR Input1 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigInput2)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line OR Input2 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigOutput)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line OR Output is not correct.", iCmdLine);
			return FALSE;
		}
	}
	else if (pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)NotCalc)
	{
		//The NOT calculation need pPlcCmd pPlcCmd->sigInput1 and
		//pPlcCmd->sigOutput must existed
		if (!IsValidSignalCfg(&(pPlcCmd->sigInput1)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line NOT Input1 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigOutput)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line NOT Output is not correct.", iCmdLine);
			return FALSE;
		}
	}
	else if(pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)XorCalc)
	{
        //The XOR calculation need pPlcCmd pPlcCmd->sigInput1,2 and
		//pPlcCmd->sigOutput must existed
		if (!IsValidSignalCfg(&(pPlcCmd->sigInput1)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line XOR Input1 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigInput2)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line XOR Input2 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigOutput)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line XOR Output is not correct.", iCmdLine);
			return FALSE;
		}
	}
	else if (pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)GreaterThan)
	{
		//The GT calculation need pPlcCmd pPlcCmd->sigInput1,
		//pPlcCmd->fParameter1,pPlcCmd->fParameter2,pPlcCmd->sigOutput must existed
		//pPlcCmd->fParameter1 and pPlcCmd->fParameter2 have been checked in
		//ParsePLCCmdTableProc;
		if (!IsValidSignalCfg(&(pPlcCmd->sigInput1)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line GT Input1 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigOutput)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line GT Output is not correct.", iCmdLine);
			return FALSE;
		}
	}
	else if (pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)LessThan)
	{
		//The LT calculation need pPlcCmd pPlcCmd->sigInput1,
		//pPlcCmd->fParameter1,pPlcCmd->fParameter2,pPlcCmd->sigOutput must existed
		//pPlcCmd->fParameter1 and pPlcCmd->fParameter2 have been checked in
		//ParsePLCCmdTableProc;
		if (!IsValidSignalCfg(&(pPlcCmd->sigInput1)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line LT Input1 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigOutput)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line LT Output is not correct.", iCmdLine);
			return FALSE;
		}
	}
	else if (pPlcCmd->pfnPlcCmd == (PLC_CMD_PROC)DelayCalc)
	{
		//The LT calculation need pPlcCmd pPlcCmd->sigInput1,
		//pPlcCmd->fParameter1,pPlcCmd->sigOutput must existed
		//pPlcCmd->fParameter1 have been checked in
		//ParsePLCCmdTableProc;
		if (!IsValidSignalCfg(&(pPlcCmd->sigInput1)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line DS Input1 is not correct.", iCmdLine);
			return FALSE;
		}

		if (!IsValidSignalCfg(&(pPlcCmd->sigOutput)))
		{
			AppLogOut("PLC_CHK_LOGI", APP_LOG_ERROR, 
				"The %d command line DS Output is not correct.", iCmdLine);
			return FALSE;
		}		
	}

	return TRUE;
}
/*==========================================================================*
* FUNCTION : RaiseConfigError
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: tPLC_SIG  *pSig : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2005-02-22 14:44
*==========================================================================*/
static BOOL RaiseConfigError()
{
	int iEquipID;
	int iSignalType;
	int iSignalID;
	int iVarSubID;
	int iBufLen;
	int iError;
	VAR_VALUE_EX value;
	SIG_BASIC_VALUE* pSigValue;

	iBufLen = sizeof(VAR_VALUE_EX);

	value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	value.varValue.enumValue  = 1;
	value.pszSenderName = "PLC";


	iEquipID = 1;
	iSignalType = 0;
	iSignalID = 55;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);

	iError = DxiSetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		iVarSubID,		
		iBufLen,			
		&value,			
		10000);

	return TRUE;
}
/*==========================================================================*
* FUNCTION : GC_GetEnumValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:	IN int iEquipID:
		IN int iSignalType:
		IN int iSIgnalID:
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Marco Yang                 DATE: 2011-12-05 10:24
*==========================================================================*/
static SIG_ENUM GC_GetEnumValue(IN int iEquipID, IN int iSignalType, IN int iSIgnalID)
{

	int iError;
	int iBufLen;
	int iTimeOut;

	iError			= 0;
	iTimeOut		= 200;

	//1.Get data
	SIG_BASIC_VALUE *pSigVal;

	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipID,			
		DXI_MERGE_SIG_ID(iSignalType, iSIgnalID),		
		&iBufLen,			
		&pSigVal,			
		iTimeOut);	

	if (iError != ERR_OK)
	{
		return FALSE;
	}

	if (!SIG_VALUE_IS_VALID(pSigVal))
	{
		return FALSE;
	}
	return pSigVal->varValue.enumValue;

}
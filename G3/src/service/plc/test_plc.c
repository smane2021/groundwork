/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : test_plc.c
 *  CREATOR  : LIXIDONG                 DATE: 2004-11-23 16:46
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"
#include "public.h"

//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO  g_SiteInfo;

//need to be defined in Miscellaneous Function Module
TIME_SRV_INFO g_sTimeSrvInfo;

//need to be defined in equip management Module
BOOL Equip_Control(int nEquipID, int nSigType, int nSigID, 
				   VAR_VALUE varCtrlValue)
{
	UNUSED(nEquipID);
	UNUSED(nSigType);
	UNUSED(nSigID);
	UNUSED(varCtrlValue);

	return TRUE;
}

//need to be defined in Miscellaneous Function Module
int UpdateACUTime (time_t* pTimeRet)
{
	return stime(pTimeRet);
}

char		g_szACUConfigDir[256];


///////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : main     
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2004-10-19 12:57
 *==========================================================================*/
//int main(int argc, char *argv[])
int main(void)
{
	int iCounters;
	DWORD dwRult;
	iCounters = 0;
	printf("Start PLC_Init.\n");
	dwRult = ServiceMain(NULL);
	printf("End PLC_Init.\n");

	while(iCounters < 100000000)
	{
		Sleep(1000);
		iCounters++;
	}
	return 0;

}






#include "stdsys.h"
#include "public.h"

#include "default_store.h"

#include "snmp_public.h"

#include "asn1.h"
#include "snmp_api.h"
#include "snmp_impl.h"

static SNMP_INFO_STRUCT	g_sSnmpInfostru;

static BOOL g_sbSnmpdStartSuccessfully = FALSE;

extern int
snmp_call_callbacks(int major, int minor, void *caller_arg);

extern void vacm_parse_simple(const char *token, char *confline);

extern void	snmpd_parse_config_trap2sink(const char *word, char *cptr);

#define RO_COMMUNITY_TOCKEN		"rocommunity"
#define RO_V6_COMMUNITY_TOCKEN		"rocommunity6"
#define RW_COMMUNITY_TOCKEN		"rwcommunity"
#define RW_V6_COMMUNITY_TOCKEN		"rwcommunity6"
#define TRAP2_SINK_TOCKEN		"trap2sink"
//#define TRAP2_V6_SINK_TOCKEN		"trap2sink6"

/* config file names */
#define CONFIG_FILE_SNMP_PRIVATE	  "private/snmp/snmp_private.cfg"
#define CONFIG_FILE_SNMPD_CONF		  "/var/snmpd.conf"
#define CONFIG_FILE_V6_CONF		  "udp:161,udp6:161"

//Config sections name
#define MAP_OF_MIB_SIGNAL_INFO	"[MAP_OF_MIB_SIGNALS]"
#define MAP_OF_MIB_TABLE_INFO	"[MAP_OF_MIB_TABLES]"
#define MAP_OF_SNMP_CFG_VER_INFO	"[CONFIG_INFORMATION]"

#define SNMP_NMS_NUMBER					"[SNMP_NMS_NUMBER]"
#define SNMP_NMS_INFO					"[SNMP_NMS_INFO]"

/* define splitter charater used by SNMP config files */
#define SPLITTER		 ('\t')

#define MAX_TIME_WAITING_NMS_INFO_CHANGE 2000

#define SNMP_CALLBACK_APPLICATION 1
#define SNMPD_CALLBACK_PRE_UPDATE_CONFIG 9

static int UpdateNmsInfoToFile(int	nNmsNum,
			       int	nV3NmsNum,
							   NMS_INFO* pNmsInfos, 
							    V3NMS_INFO* pV3NmsInfos, 
							   BOOL bUpdatePriCfg);
static int DeleteAllNmsInfo(void);
static int DeleteAllV3NmsInfo(void);



SNMP_INFO_STRUCT* GetSnmpInfoRef(void)
{
	return &g_sSnmpInfostru;
}

void SetSnmpdStartFlag(BOOL bFlag)
{
	g_sbSnmpdStartSuccessfully = bFlag;
}
/*==========================================================================*
 * FUNCTION : ServiceNotification
 * PURPOSE  : Get the report alarm info and put it into queue
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hService        : 
 *            int     nMsgType        : 
 *            int     nTrapDataLength : 
 *            void*   lpTrapData      : 
 *            void*   lpParam         :  
 *            BOOL    bUrgent         : 
 * RETURN   : int : 0 for success, 1 for error(defined in DXI module)
 * COMMENTS : 
 * CREATOR  : HULONGWEN                   DATE: 2004-10-22 17:23
 *==========================================================================*/
int ServiceNotification(HANDLE hService, 
						int nMsgType, 
						int nTrapDataLength, 
						void *lpTrapData,	
						void *lpParam, 
						BOOL bUrgent)
{
	UNUSED(hService);
	UNUSED(bUrgent);

	if ((nMsgType & (_ALARM_OCCUR_MASK | _ALARM_CLEAR_MASK)) != 0)
	{
		//Because the trap send info include active or deactive status,
		//so extend the alarm signal value structure
		ALARM_SIG_VALUE_EXT	stAlarmSigValue;
		SNMP_INFO_STRUCT* pSnmpInfoStru = (SNMP_INFO_STRUCT*)lpParam;

		ASSERT(pSnmpInfoStru);

		if (nTrapDataLength != sizeof(ALARM_SIG_VALUE))
		{
			return 0;
		}

		memcpy(&(stAlarmSigValue.stAlarmSigValue), lpTrapData, 
			sizeof(ALARM_SIG_VALUE));

		stAlarmSigValue.nAlarmMsgType = nMsgType;

		pSnmpInfoStru->ulAlarmSequenceID += 1;

		stAlarmSigValue.ulAlarmSequenceID = pSnmpInfoStru->ulAlarmSequenceID;

		//Put the report info to alarm queue
		Queue_Put(pSnmpInfoStru->hTrapSendQueue, &stAlarmSigValue, FALSE);
		

	}

	return 0;
}

static BOOL ParseMIB_OID_Proc(char* szSrc,OUT _OID* pOID, OUT int* piIndexMethod)
{
	char *pField = szSrc + 1;
	char *pSubField;
	int iSubFieldCount = 0;
	int iCurIndex = 1;
	if (*pField == '\0')
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParseMIB_OID_Proc: ERROR: Relevant Equip List "
			"is NULL!\n", __FILE__);

		return FALSE;    
	}
	iSubFieldCount = Cfg_GetSplitStringCount(pField, '.');
	if(iSubFieldCount > MAX_SNMP_OID_LENTH)
	{
		iSubFieldCount = MAX_SNMP_OID_LENTH;
	}
	if (iSubFieldCount > MAX_SNMP_OID_LENTH)
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParseMIB_OID_Proc: ERROR: Too Long bit!/n/r",__FILE__);
		return 1;
	}
	pOID->iLenth = iSubFieldCount;
	memset(pOID->_oids, 0, MAX_SNMP_OID_LENTH);	
	do
	{
		pField = Cfg_SplitStringEx(pField, &pSubField, '.');
		if ((*pSubField <= '0') || (*pSubField > '9'))
		{
			if(*pSubField == 'x' || *pSubField == 'y' || *pSubField == 'z')
			{
				if(piIndexMethod != NULL)
				{
					*piIndexMethod = (int)(*pSubField - 'x') + 1;
				}
				pOID->iLenth--;
				break;
			}
			AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParseMIB_OID_Proc: ERROR:  Sub Oid "
				"is invalid\n!", __FILE__);
			return FALSE;
		} 
		sscanf(pSubField,"%d",&pOID->_oids[iCurIndex-1]);
	}
	while (iCurIndex++ < iSubFieldCount);
	return TRUE;
}
static SNMP_TABLE_ITEM_INFO* GetTableInfoFromID(int iTableID)
{
	int i = 0;
	SNMP_INFO_STRUCT* pSNMPInfo =  GetSnmpInfoRef();
	for(; i < pSNMPInfo->nSnmpTableNum; i++)
	{
		if(pSNMPInfo->pSnmpTableInfos[i].nTableID == iTableID)
		{
			return &pSNMPInfo->pSnmpTableInfos[i];
		}
	}
	return NULL;
}
/*==========================================================================*
 * FUNCTION : ParsedMIB_SigMapProc
 * PURPOSE  : Parse mib node map config table
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char          *szBuf        : a line info
 *            SNMP_NOD_ITEM *pSnmpNodItem : to save cfg
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Jimmy Wu                DATE: 2014-03-31
 *==========================================================================*/
static int ParsedMIB_SigMapProc(char *szBuf, SNMP_SIG_ITEM_INFO *pSnmpSigItem)
{
	char *pField;
	char *pSubField;
	ASSERT(szBuf);
	ASSERT(pSnmpSigItem);

	/* 0.oid of a Signal*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ( *pField != '.')
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedMIB_SigMapProc: ERROR: MIB signal oid is not"
			"started with a dot!\n\r", __FILE__);

		return 1;    /* not a num, error */
	}
	else if(!ParseMIB_OID_Proc(pField,&(pSnmpSigItem->oid_this),NULL))
	{
		return 1;
	}

	/* 1.Signal Name, just Jump */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 2.Table ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pSnmpSigItem->pTableInfo = NULL;
	}
	else if ((*pField < '0') || (*pField > '9'))
	{
		pSnmpSigItem->pTableInfo = NULL;
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedMIB_SigMapProc: ERROR: MIB nTableID is not"
			"a number!\n\r", __FILE__);

		return 1;    /* not a num, error */
	}
	else
	{
		pSnmpSigItem->pTableInfo  = GetTableInfoFromID(atoi(pField));
		if(pSnmpSigItem->pTableInfo == NULL)
		{
			AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParsedMIB_SigMapProc: ERROR: such table ID"
				"not found!\n\r", __FILE__);
			return 1;
		}
	}
	
	/* 3.Equip Type ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if(Cfg_IsEmptyField(pField) || (strcmp(pField,"-1") == 0))
	{
		pSnmpSigItem->bMultiple = FALSE;
		pSnmpSigItem->nStdEquipID = -1;
	}
	else if ((*pField < '0') || (*pField > '9'))
	{
		if(*pField == '$' && pSnmpSigItem->pTableInfo != NULL)
		{
			//parse equip list
			pField += 1;
			pSnmpSigItem->bMultiple = TRUE;
			int iSubFieldCount = Cfg_GetSplitStringCount(pField, ',');
			if(iSubFieldCount > pSnmpSigItem->pTableInfo->nEquipTypeNums)
			{
				AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
					"[%s]--ParsedMIB_SigMapProc: ERROR: too many relative "
					"Equip Indexes\n!", __FILE__);
				return 1;
			}
			pSnmpSigItem->piIndexList = NEW(int, iSubFieldCount);
			if (pSnmpSigItem->piIndexList == NULL)
			{
				AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
					"[%s]--ParsedMIB_SigMapProc: ERROR: no memory!/n/r",__FILE__);
				return 1;
			}
			pSnmpSigItem->nEquipIndexNum = iSubFieldCount;
			memset(pSnmpSigItem->piIndexList, 0, sizeof(int)*iSubFieldCount);
			int iCurIndex = 1;
			do
			{
				pField = Cfg_SplitStringEx(pField, &pSubField, ',');
				if ((*pSubField < '0') || (*pSubField > '9'))
				{
					AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
						"[%s]--ParsedMIB_SigMapProc: ERROR: Relevant "
						"Equip List is invalid\n!", __FILE__);
					return 1;
				} 
				sscanf(pSubField,"%d",&pSnmpSigItem->piIndexList[iCurIndex-1]);

			}
			while (iCurIndex++ < iSubFieldCount);

		}
		else
		{
			AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParsedMIB_SigMapProc: ERROR: MIB nStdEquipID is not"
				" a number!\n\r", __FILE__);

			return 1;    /* not a num, error */
		}

	}
	else
	{
		pSnmpSigItem->bMultiple = FALSE;
		pSnmpSigItem->nStdEquipID  = atoi(pField);
	}
	
	/* 4.Signal Type ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		if(strcmp(pField,"NA") == 0 || strcmp(pField,"-1") == 0)
		{
			pSnmpSigItem->nSigType = -1;
		}
		else
		{
			AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParsedMIB_SigMapProc: ERROR: MIB nSigType is not"
				"a number!\n\r", __FILE__);
			return 1;    /* not a num, error */
		}

	}
	else
	{
		pSnmpSigItem->nSigType  = atoi(pField);
	}

	/* 5.Signal ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		if(strcmp(pField,"NA") == 0 || strcmp(pField,"-1") == 0)
		{
			pSnmpSigItem->nSigID = -1;
		}
		else
		{
			AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParsedMIB_SigMapProc: ERROR: MIB nSigID is not"
				"a number!\n\r", __FILE__);
			return 1;    /* not a num, error */
		}

	}
	else
	{
		pSnmpSigItem->nSigID  = atoi(pField);
	}

	/* 6.Value Type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (strcmp(pField,"INT") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_INT;
	}
	else if (strcmp(pField,"FLOAT") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_FLOAT;
	}
	else if (strcmp(pField,"INDEX") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_INDEX;
	}
	else if (strcmp(pField,"EQUIPNAME") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_EQUIP_NAME;
	}
	else if (strcmp(pField,"SIGNAME") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_SIGNAL_NAME;
	}
	else if (strcmp(pField,"PRODNUM") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_PRDCT_NUM;
	}
	else if (strcmp(pField,"HWVER") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_HW_VER;
	}
	else if (strcmp(pField,"SWVER") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_SW_VER;
	}
	else if (strcmp(pField,"SNNUM") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_SERIAL_NUM;
	}
	else if (strcmp(pField,"SPECIAL") == 0) //SPecial signal
	{
		if(pSnmpSigItem->nSigID > SNMP_VT_MAX - SNMP_VT_MANIFACTURER || pSnmpSigItem->nSigID < 1)
		{
			AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParsedMIB_SigMapProc: ERROR: wrong 'SP' signal ID : %d!\n\r", __FILE__, pSnmpSigItem->nSigID);

			return 1;    /* not a num, error */
		}
		pSnmpSigItem->nValueType = SNMP_VT_MANIFACTURER + pSnmpSigItem->nSigID - 1;
	}
	else if (strcmp(pField, "ALRELAY") == 0)
	{
		pSnmpSigItem->nValueType = SNMP_VT_ALARMRELAY;
	}
	else
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedMIB_SigMapProc: ERROR: Unknown Signal Type!\n\r", __FILE__);

		return 1;    /* not a num, error */
	}
	/* 7.Access attribute */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if(strcmp(pField,"R") == 0)
	{
		pSnmpSigItem->nAccessAttr = RONLY;
	}
	else if (strcmp(pField,"RW") == 0 || strcmp(pField,"W") == 0)
	{
		pSnmpSigItem->nAccessAttr = RWRITE;
	}
	else
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedMIB_SigMapProc: ERROR: Unknown Access attribute!\n\r", __FILE__);

		return 1;    /* not a num, error */
	}
	return ERR_SNP_OK;
}
static int ParsedMIB_TableMapProc(char *szBuf, SNMP_TABLE_ITEM_INFO *pSnmpTableItem)
{
	char *pField,*pSubField;

	ASSERT(szBuf);
	ASSERT(pSnmpTableItem);
	/* 0.Table ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedMIB_TableMapProc: ERROR: MIB nTableID is not"
			"a number!\n\r", __FILE__);

		return 1;    /* not a num, error */
	}
	else
	{
		pSnmpTableItem->nTableID  = atoi(pField);
	}

	/* 1.Table Name, just Jump */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	///* 2.is equip class */
	//szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	//if (*pField == 'Y' && *(pField + 1) == '\0')
	//{
	//	pSnmpTableItem->bIsEquipType = TRUE;
	//}
	//else if (*pField == 'N' && *(pField + 1) == '\0')
	//{
	//	pSnmpTableItem->bIsEquipType = FALSE;
	//}
	//else
	//{
	//	AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
	//		"[%s]--ParsedMIB_TableMapProc: ERROR: MIB Is EquipClass should be Y or N!\n\r", __FILE__);

	//	return 1;    /* not a num, error */
	//}

	/* 3.oid of a Signal*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ( *pField != '.')
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedMIB_TableMapProc: ERROR: MIB index oid is not"
			"started with a dot!\n\r", __FILE__);

		return 1;    /* not a num, error */
	}
	else if(!ParseMIB_OID_Proc(pField,&(pSnmpTableItem->oid_index),&(pSnmpTableItem->iIndexMethod)))
	{
		return 1;
	}

	/* 4.EquipTypeID List*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (Cfg_IsEmptyField(pField))
	{
		pSnmpTableItem->nEquipTypeNums = 0;
		pSnmpTableItem->pEquipTypeIDList = NULL;
	}
	else if (strcmp(pField,"ALL") == 0 || 
		strcmp(pField,"ALARM") == 0)
	{
		pSnmpTableItem->nEquipTypeNums = (strcmp(pField,"ALARM") == 0) ? -1 : -2;
		pSnmpTableItem->pEquipTypeIDList = NULL;
	}
	else
	{
		pSnmpTableItem->nEquipTypeNums = 0;
		pSnmpTableItem->pEquipTypeIDList = NULL;
		if (*pField == '\0')
		{
			AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParsedMIB_TableMapProc: ERROR: Relevant Equip List "
				"is NULL!\n", __FILE__);

			return 1;    
		}
		int iSubFieldCount = Cfg_GetSplitStringCount(pField, ',');
		pSnmpTableItem->pEquipTypeIDList = NEW(int, iSubFieldCount);
		if (pSnmpTableItem->pEquipTypeIDList == NULL)
		{
			AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
				"[%s]--ParsedMIB_TableMapProc: ERROR: no memory!/n/r",__FILE__);
			return 1;
		}
		pSnmpTableItem->nEquipTypeNums = iSubFieldCount;
		memset(pSnmpTableItem->pEquipTypeIDList, 0, sizeof(int)*iSubFieldCount);
		int iCurIndex = 1;
		do
		{
			pField = Cfg_SplitStringEx(pField, &pSubField, ',');
			if ((*pSubField < '0') || (*pSubField > '9'))
			{
				AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
					"[%s]--ParsedMIB_TableMapProc: ERROR: Relevant "
					"Equip List is invalid\n!", __FILE__);
				return 1;
			} 
			sscanf(pSubField,"%d",&pSnmpTableItem->pEquipTypeIDList[iCurIndex-1]);

		}
		while (iCurIndex++ < iSubFieldCount);
	}
	return ERR_SNP_OK;
}
static int Parsed_SNMP_CFGPri_MapProc(char *szBuf, SNMP_PRICFG_INFO *pSnmpPriCfgInfo)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pSnmpPriCfgInfo);

	/* 0.Name,   Jump */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 1. Version, that is what we want*/
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	strncpyz(pSnmpPriCfgInfo->szSNMPpriCfgVer,pField,MAX_PRICFG_STR_LEN);
	return ERR_SNP_OK;
}
/*==========================================================================*
 * FUNCTION : ParsedNmsInfoTableProc
 * PURPOSE  : Parse nms info config table
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char     *szBuf    : a line info
 *            NMS_INFO *pNmsInfo : to save cfg
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-01-04 17:24
 *==========================================================================*/
static int ParsedNmsInfoTableProc(IN char *szBuf, 
									  OUT NMS_INFO *pNmsInfo)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pNmsInfo);

	/* 0.jump Sequence id field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);

	/* 1.NMS IP */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedNmsInfoTableProc: ERROR: NMS IP "
			"is NULL!\n\r", __FILE__);

		return 1;
	}
	pNmsInfo->ulIpAddress = inet_addr(pField);

	/* 2.Public Community */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedNmsInfoTableProc: ERROR: Public Community "
			"is NULL!\n\r", __FILE__);

		return 2;
	}
	strncpyz(pNmsInfo->szPublicCommunity, pField, 
		sizeof(pNmsInfo->szPublicCommunity));

	/* 3.Private Community */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (*pField == '\0')
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedNmsInfoTableProc: ERROR: Private Community "
			"is NULL!\n\r", __FILE__);

		return 3;
	}
	strncpyz(pNmsInfo->szPrivateCommunity, pField,
		sizeof(pNmsInfo->szPrivateCommunity));


	/* 4.TRAP level field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ParsedNmsInfoTableProc: ERROR: TRAP level is not "
			"a number!\n\r", __FILE__);

		return 4;    /* not a num, error */
	}
	pNmsInfo->nTrapLevel = atoi(pField);


	return ERR_SNP_OK;
}
//#define _SNMP_TEST_M
#ifdef _SNMP_TEST_M
static void printParse(void)
{
	SNMP_INFO_STRUCT* pSnmpInfoStruct = GetSnmpInfoRef();
	SNMP_TABLE_ITEM_INFO* pTableInfo;
	SNMP_SIG_ITEM_INFO* pSigItemInfo;
	int i = 0,j;
	//先打印cfg信息
	printf("\n*************************************************************");
	printf("\nThis is Cfg Version Info");
	printf("\n*Index\t\tVersion");
	for(i = 0;i < pSnmpInfoStruct->nSnmpPriCfgNum;i++)
	{
		printf("\n%d\t\t%s",i,pSnmpInfoStruct->pSnmpPriCfgInfos[i].szSNMPpriCfgVer);
	}
	
	printf("\nThis is Table Info");
	printf("\nTableID\t\tIndexMethod\t\tSubIndexOid\t\tEquipTypeList");
	for(i = 0; i < pSnmpInfoStruct->nSnmpTableNum; i++)
	{
		pTableInfo = &pSnmpInfoStruct->pSnmpTableInfos[i];
		printf("\n%d\t",pTableInfo->nTableID);
		char ch = (pTableInfo->iIndexMethod == 1)?'x':((pTableInfo->iIndexMethod == 2)?'y':'z');
		printf("%c\t",ch);
		for(j = 0; j < pTableInfo->oid_index.iLenth;j++)
		{
			printf("%d.",pTableInfo->oid_index._oids[j]);
		}
		printf("\t");
		if(pTableInfo->nEquipTypeNums < 0)
		{
			//printf("ALARM");
			printf((pTableInfo->nEquipTypeNums == -1)?"ALARM":"ALL");
		}
		else if(pTableInfo->nEquipTypeNums == 0)
		{
			printf("NA");
		}
		else
		{
			for(j = 0; j < pTableInfo->nEquipTypeNums; j++)
			{
				printf("%d,",pTableInfo->pEquipTypeIDList[j]);
			}
		}
	}

	printf("\nThis is SigItem Info");
	printf("\nIndex\t\tSubOid\t\tTableID\t\tEquipID/Index\t\tSigType\t\tSigID\t\tValueType\t\tAccessType");
	for(i = 0; i < pSnmpInfoStruct->nSnmpSigNum; i++)
	{
		pSigItemInfo = &pSnmpInfoStruct->pSnmpSigInfos[i];
		printf("\n%d\t", i+1);
		for(j = 0; j < pSigItemInfo->oid_this.iLenth;j++)
		{
			printf("%d.",pSigItemInfo->oid_this._oids[j]);
		}
		if(pSigItemInfo->pTableInfo)
		{
			printf("\t%d\t",pSigItemInfo->pTableInfo->nTableID);
		}
		else
		{
			printf("\tNA\t");
		}
		if(pSigItemInfo->bMultiple)
		{
			printf("$");
			for(j = 0; j < pSigItemInfo->nEquipIndexNum; j++)
			{
				printf("%d,",pSigItemInfo->piIndexList[j]);
			}
		}
		else
		{
			printf("%d",pSigItemInfo->nStdEquipID);
		}
		printf("\t%d",pSigItemInfo->nSigType);
		printf("\t%d",pSigItemInfo->nSigID);
		printf("\t%d",pSigItemInfo->nValueType);
		printf("\t%d",pSigItemInfo->nAccessAttr);
	}
	printf("\n**************************************************************\n");
}
#endif
/*==========================================================================*
 * FUNCTION : LoadSNMPPrivateConfigProc
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void  *pCfg       : 
 *            void  *pLoadToBuf : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 19:20
 *==========================================================================*/
static int LoadSNMPPrivateConfigProc(void *pCfg, void *pLoadToBuf)
{
	SNMP_INFO_STRUCT *pBuf;

#ifdef GET_NMS_INFO_FROM_CONFIG_FILE

	//Has one tables
	CONFIG_TABLE_LOADER loader[4];

#else

	//Has two tables
	CONFIG_TABLE_LOADER loader[3];

#endif
	

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	pBuf = (SNMP_INFO_STRUCT *)pLoadToBuf;


	/* load default screen configuration */
	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pBuf->nSnmpPriCfgNum), 
		MAP_OF_SNMP_CFG_VER_INFO, 
		&(pBuf->pSnmpPriCfgInfos), 
		Parsed_SNMP_CFGPri_MapProc);

	DEF_LOADER_ITEM(&loader[1],
		NULL,
		&(pBuf->nSnmpTableNum), 
		MAP_OF_MIB_TABLE_INFO, 
		&(pBuf->pSnmpTableInfos), 
		ParsedMIB_TableMapProc);

	DEF_LOADER_ITEM(&loader[2],
		NULL,
		&(pBuf->nSnmpSigNum), 
		MAP_OF_MIB_SIGNAL_INFO, 
		&(pBuf->pSnmpSigInfos), 
		ParsedMIB_SigMapProc);



#ifdef GET_NMS_INFO_FROM_CONFIG_FILE

	/* load SNMP language file table */
	DEF_LOADER_ITEM(&loader[3],
		SNMP_NMS_NUMBER,
		&(pBuf->nNmsNum), 
		SNMP_NMS_INFO, 
		&(pBuf->pNmsInfos), 
		ParsedNmsInfoTableProc);
#endif


	if (Cfg_LoadTables(pCfg,sizeof(loader)/sizeof(*loader),loader) 
		!= ERR_CFG_OK)
	{
		return ERR_SNP_READ_CFG;
	}

#ifdef _SNMP_TEST_M
	//TTTTTTTTTTEST Only
	printParse();
#endif



	return ERR_SNP_OK;
}

/*==========================================================================*
 * FUNCTION : LoadSNMPPrivateConfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SNMP_PRIVATE_CFG_LOADER*  pSnmpPrivateCfgLoader : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 19:19
 *==========================================================================*/
static int LoadSNMPPrivateConfig(SNMP_INFO_STRUCT* pSnmpInfoStru)
{
	int ret;
	char szCfgFileName[MAX_FILE_PATH]; 

	ASSERT(pSnmpInfoStru);

	Cfg_GetFullConfigPath(CONFIG_FILE_SNMP_PRIVATE, 
		szCfgFileName, 
		MAX_FILE_PATH);


	ret = Cfg_LoadConfigFile(
		szCfgFileName, 
		LoadSNMPPrivateConfigProc,
		pSnmpInfoStru);

	if (ret != ERR_SNP_OK)
	{
		//return ret;
		//说明用户配置错误，从config_default文件夹读取
		//snmp配置告警
		SetDwordSigValue(1, SIG_TYPE_SAMPLING, 206, 1, "SNMP set Config Error Alarm");

		snprintf(szCfgFileName, (size_t)MAX_FILE_PATH,
			"/app/config_default/%s",
			CONFIG_FILE_SNMP_PRIVATE);

		ret = Cfg_LoadConfigFile(
			szCfgFileName, 
			LoadSNMPPrivateConfigProc,
			pSnmpInfoStru);
		return ret;
	}

	return ERR_SNP_OK;
}

//Free all the snmp private config
static int DestroySNMPPrivateConfig(void)
{
	int			nError = ERR_SNP_OK;

	DELETE_ITEM(g_sSnmpInfostru.pSnmpSigInfos);
	DELETE_ITEM(g_sSnmpInfostru.pSnmpTableInfos);

#ifdef GET_NMS_INFO_FROM_CONFIG_FILE

	DELETE_ITEM(g_sSnmpInfostru.pNmsInfos);

#endif

	return nError;
}


/*==========================================================================*
 * FUNCTION : Snmp_TrapSendTask
 * PURPOSE  : The main task function used to send trap to nmss
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN SNMP_INFO_STRUCT  *pParam : the snmp info 
 * RETURN   : static DWORD : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 10:52
 *==========================================================================*/
static DWORD Snmp_TrapSendTask(IN SNMP_INFO_STRUCT *pParam)
{
	HANDLE			hSelf = RunThread_GetId(NULL);

	ALARM_SIG_VALUE_EXT stAlarmSigValue;

	time_t	tCurTime = 0;
	
	int	nAlarmLevel;

	int i;
	char string_Trap2SinkBuf[256];

	int				nNmsNum;
	NMS_INFO*		pNmsInfos;


	unsigned long nalarmTrapNo = 0;	

	char powerMIB_timestr[32];

	int nalarmStatusChange = 0;

	int nalarmSeverity = 0;

	char powerMIB_DescripStr[256];

	int nalarmType	= 0;

	//The variables used to send v2 trap
	oid objid_snmptrap[]	= { 1,3,6,1,6,3,1,1,4,1,0}; /* snmpTrapOID.0 */
	//oid demo_trap[]			= { 1,3,6,1,4,1,6302,2,1,0,1}; /*demo-trap */
	oid alarm_trap[]			= { 1,3,6,1,4,1,6302,2,1,5,1}; /*demo-trap */
	oid activeAlm_trap[]			= { 1,3,6,1,4,1,6302,2,1,5,2}; /*demo-trap */
	oid ceasedAlm_trap[]			= { 1,3,6,1,4,1,6302,2,1,5,3}; /*demo-trap */


	struct variable_list var_trap;
	struct variable_list var_trap1;

	oid objid_alarmTrapNo[]			= { 1,3,6,1,4,1,6302,2,1,4,1,1};
	oid objid_alarmTime[]			= { 1,3,6,1,4,1,6302,2,1,4,1,2};
	oid objid_alarmStatusChange[]	= { 1,3,6,1,4,1,6302,2,1,4,1,3};
	oid objid_alarmSeverity[]		= { 1,3,6,1,4,1,6302,2,1,4,1,4};
	oid objid_alarmDescription[]	= { 1,3,6,1,4,1,6302,2,1,4,1,5};
	oid objid_alarmType[]			= { 1,3,6,1,4,1,6302,2,1,4,1,6};


	struct variable_list var_alarmTrapNo;
	struct variable_list var_alarmTime;
	struct variable_list var_alarmStatusChange;
	struct variable_list var_alarmSeverity;
	struct variable_list var_alarmDescription;
	struct variable_list var_alarmType;

	/* trap definition objects */

	var_trap.next_variable = &var_alarmTrapNo; /* next variable */
	var_trap.name = objid_snmptrap; /* snmpTrapOID.0 */
	var_trap.name_length = sizeof(objid_snmptrap)/sizeof(oid); /* number of sub-ids */
	var_trap.type = ASN_OBJECT_ID;
	//var_trap.val.objid = demo_trap; /* demo-trap objid */
	//var_trap.val_len = sizeof(demo_trap); /* length in bytes (not number of subids!) */


	/* additional objects */

	var_alarmTrapNo.next_variable = &var_alarmTime; /* next variable */
	var_alarmTrapNo.name = objid_alarmTrapNo; /* snmpTrapOID.0 */
	var_alarmTrapNo.name_length = sizeof(objid_alarmTrapNo)/sizeof(oid); /* number of sub-ids */
	var_alarmTrapNo.type = ASN_COUNTER;
	var_alarmTrapNo.val.integer= &nalarmTrapNo; /* demo-trap objid */
	var_alarmTrapNo.val_len = sizeof(long); /* length in bytes (not number of subids!) */

	var_alarmTime.next_variable = &var_alarmStatusChange; /* next variable */
	var_alarmTime.name = objid_alarmTime; /* snmpTrapOID.0 */
	var_alarmTime.name_length = sizeof(objid_alarmTime)/sizeof(oid); /* number of sub-ids */
	var_alarmTime.type = ASN_OCTET_STR;
	var_alarmTime.val.string= powerMIB_timestr; /* demo-trap objid */
	var_alarmTime.val_len = 11/*strlen(powerMIB_timestr)*/; /* length in bytes (not number of subids!) */


	var_alarmStatusChange.next_variable = &var_alarmSeverity; /* next variable */
	var_alarmStatusChange.name = objid_alarmStatusChange; /* snmpTrapOID.0 */
	var_alarmStatusChange.name_length = sizeof(objid_alarmStatusChange)/sizeof(oid); /* number of sub-ids */
	var_alarmStatusChange.type = ASN_INTEGER;
	var_alarmStatusChange.val.integer= (long*)(&nalarmStatusChange); /* demo-trap objid */
	var_alarmStatusChange.val_len = sizeof(long); /* length in bytes (not number of subids!) */

	var_alarmSeverity.next_variable = &var_alarmDescription; /* next variable */
	var_alarmSeverity.name = objid_alarmSeverity; /* snmpTrapOID.0 */
	var_alarmSeverity.name_length = sizeof(objid_alarmSeverity)/sizeof(oid); /* number of sub-ids */
	var_alarmSeverity.type = ASN_INTEGER;
	var_alarmSeverity.val.integer= (long*)(&nalarmSeverity); /* demo-trap objid */
	var_alarmSeverity.val_len = sizeof(long); /* length in bytes (not number of subids!) */

	var_alarmDescription.next_variable = &var_alarmType; /* next variable */
	var_alarmDescription.name = objid_alarmDescription; /* snmpTrapOID.0 */
	var_alarmDescription.name_length = sizeof(objid_alarmDescription)/sizeof(oid); /* number of sub-ids */
	var_alarmDescription.type = ASN_OCTET_STR;
	var_alarmDescription.val.string= powerMIB_DescripStr; /* demo-trap objid */
//	var_alarmDescription.val_len = strlen(powerMIB_DescripStr); /* length in bytes (not number of subids!) */

	var_alarmType.next_variable = NULL; /* next variable */
	var_alarmType.name = objid_alarmType; /* snmpTrapOID.0 */
	var_alarmType.name_length = sizeof(objid_alarmType)/sizeof(oid); /* number of sub-ids */
	var_alarmType.type = ASN_INTEGER;
	var_alarmType.val.integer= (long*)(&nalarmType); /* demo-trap objid */
	var_alarmType.val_len = sizeof(long); /* length in bytes (not number of subids!) */



	struct variable_list var_alarmTime1;
	struct variable_list var_alarmSeverity1;
	struct variable_list var_alarmDescription1;
	struct variable_list var_alarmType1;

	var_trap1.next_variable = &var_alarmTime1; /* next variable */
	var_trap1.name = objid_snmptrap; /* snmpTrapOID.0 */
	var_trap1.name_length = sizeof(objid_snmptrap)/sizeof(oid); /* number of sub-ids */
	var_trap1.type = ASN_OBJECT_ID;

	var_alarmTime1.next_variable = &var_alarmSeverity1; /* next variable */
	var_alarmTime1.name = objid_alarmTime; /* snmpTrapOID.0 */
	var_alarmTime1.name_length = sizeof(objid_alarmTime)/sizeof(oid); /* number of sub-ids */
	var_alarmTime1.type = ASN_OCTET_STR;
	var_alarmTime1.val.string= powerMIB_timestr; /* demo-trap objid */
	var_alarmTime1.val_len = 11/*strlen(powerMIB_timestr)*/; /* length in bytes (not number of subids!) */

	var_alarmSeverity1.next_variable = &var_alarmDescription1; /* next variable */
	var_alarmSeverity1.name = objid_alarmSeverity; /* snmpTrapOID.0 */
	var_alarmSeverity1.name_length = sizeof(objid_alarmSeverity)/sizeof(oid); /* number of sub-ids */
	var_alarmSeverity1.type = ASN_INTEGER;
	var_alarmSeverity1.val.integer= (long*)(&nalarmSeverity); /* demo-trap objid */
	var_alarmSeverity1.val_len = sizeof(long); /* length in bytes (not number of subids!) */

	var_alarmDescription1.next_variable = &var_alarmType1; /* next variable */
	var_alarmDescription1.name = objid_alarmDescription; /* snmpTrapOID.0 */
	var_alarmDescription1.name_length = sizeof(objid_alarmDescription)/sizeof(oid); /* number of sub-ids */
	var_alarmDescription1.type = ASN_OCTET_STR;
	var_alarmDescription1.val.string= powerMIB_DescripStr; /* demo-trap objid */
	//	var_alarmDescription.val_len = strlen(powerMIB_DescripStr); /* length in bytes (not number of subids!) */

	var_alarmType1.next_variable = NULL; /* next variable */
	var_alarmType1.name = objid_alarmType; /* snmpTrapOID.0 */
	var_alarmType1.name_length = sizeof(objid_alarmType)/sizeof(oid); /* number of sub-ids */
	var_alarmType1.type = ASN_INTEGER;
	var_alarmType1.val.integer= (long*)(&nalarmType); /* demo-trap objid */
	var_alarmType1.val_len = sizeof(long); /* length in bytes (not number of subids!) */

	//extern void send_v3trap ( struct variable_list *vars);
	extern void send_v3trap(netsnmp_variable_list *vars, char *context); //Fengel modify 2017-8-9

	
	ASSERT(pParam);

	while (!g_sbSnmpdStartSuccessfully)
	{
		RunThread_Heartbeat(hSelf);	//heartbeat

		Sleep(250);
	}

	//wait time on get from snmp trap queue.
#define WAIT_TIME_SNMP_TRAP_QUEUE_GET	500		// ms. 

	// main loop of send trap
	while (THREAD_IS_RUNNING(hSelf))
	{
		RunThread_Heartbeat(hSelf);	//heartbeat

		//Get the alarm info from queue
		if ((Queue_Get(pParam->hTrapSendQueue, &stAlarmSigValue, FALSE, 
			WAIT_TIME_SNMP_TRAP_QUEUE_GET) == ERR_QUEUE_OK))
		{
		    //printf("nNmsNum is %d\n", g_sSnmpInfostru.nNmsNum);
		    //printf("nTrapLevel is %d\n", (g_sSnmpInfostru.pNmsInfos)[0].nTrapLevel);
			//by hulw 070319, to solve the issue of memory leak when update snmp trap session list.
			//For all the NMSs, just have one set for trap alarm level

			if((g_sSnmpInfostru.nNmsNum >= 1) || (g_sSnmpInfostru.nV3NmsNum >= 1))
			{
				if ((g_sSnmpInfostru.nTrapLevel != ALARM_LEVEL_NONE)
					&&(g_sSnmpInfostru.nTrapLevel <= stAlarmSigValue.stAlarmSigValue.pStdSig->iAlarmLevel))
				{
					//Send trap to nms
					nalarmTrapNo = stAlarmSigValue.ulAlarmSequenceID;//
					
					//GetTimeString(tCurTime, powerMIB_timestr); //Fengel modify
					
					nAlarmLevel = stAlarmSigValue.stAlarmSigValue.pStdSig->iAlarmLevel;

					nalarmSeverity = GetAlarmSeverity(nAlarmLevel);

					GetAlarmDescription(&(stAlarmSigValue.stAlarmSigValue), 
						powerMIB_DescripStr);

					var_alarmDescription.val_len = strlen(powerMIB_DescripStr); /* length in bytes (not number of subids!) */
					var_alarmDescription1.val_len = strlen(powerMIB_DescripStr);

					nalarmType	= DXI_MERGE_UNIQUE_SIG_ID(
						stAlarmSigValue.stAlarmSigValue.pEquipInfo->iEquipID, 
						SIG_TYPE_ALARM, 
						stAlarmSigValue.stAlarmSigValue.pStdSig->iSigID);//

					
					if ((stAlarmSigValue.nAlarmMsgType & _ALARM_OCCUR_MASK)
						== _ALARM_OCCUR_MASK)
					{
						tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmStartTime;
						GetTimeString(tCurTime, powerMIB_timestr);

						nalarmStatusChange = ALARM_STATUS_ACTIVATED;
						//发两次
						var_trap.val.objid = alarm_trap; 
						var_trap.val_len = sizeof(alarm_trap); 
						
						//send_v3trap(&var_trap);
						send_v3trap(&var_trap,NULL);  //Fengel modify 

						var_trap1.val.objid = activeAlm_trap; 
						var_trap1.val_len = sizeof(activeAlm_trap); 
						send_v3trap(&var_trap1,NULL);
						//send_v3trap(&var_trap1);
					}
					else if ((stAlarmSigValue.nAlarmMsgType & _ALARM_CLEAR_MASK)
						== _ALARM_CLEAR_MASK)
					{		
						tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmEndTime;
						GetTimeString(tCurTime, powerMIB_timestr);
						nalarmStatusChange = ALARM_STATUS_DEACTIVATED;
						//发两次
						var_trap.val.objid = alarm_trap; 
						var_trap.val_len = sizeof(alarm_trap); 
						send_v3trap(&var_trap,NULL);
						//send_v3trap(&var_trap);

						var_trap1.val.objid = ceasedAlm_trap; 
						var_trap1.val_len = sizeof(ceasedAlm_trap); 
						send_v3trap(&var_trap1,NULL);
						//send_v3trap(&var_trap1);
					}											
					//else
					//{
					//	tCurTime = stAlarmSigValue.stAlarmSigValue.sv.tmStartTime;

					//	nalarmStatusChange = ALARM_STATUS_INVALIDATE;
					//}
				}
			}

			Sleep(20);

		}
		else
		{
			Sleep(50);
		}
	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : UpdateNmsInfoToFile
 * PURPOSE  : Update the NMS info, to snmp agent sink and private config
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int        nNmsNum       : nms num
 *            NMS_INFO*  pNmsInfos     : nms info
 *            BOOL       bUpdatePriCfg : whether update private config
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:00
 *==========================================================================*/
static int UpdateNmsInfoToFile(int	nNmsNum, 
			       int	nV3NmsNum,
							   NMS_INFO* pNmsInfos, 
							   V3NMS_INFO* pV3NmsInfos, 
							   BOOL bUpdatePriCfg)
{
	int i;

	char sztrapSecuritylevel[16];
	char szV6IP[128];

	char snmpdcfg[] = 
	    "view all included .1\r\n"\
	    "access readgroup \"\" any authPriv exact all all none\r\n";
	    //"createUser zhao MD5 \"11111111\" DES \"22222222\"\r\n"\
	    //"group readgroup usm zhao\r\n";
	    //"trapsess -v 3 -l noauthnoPriv -a MD5 -A 11111111 -x DES -X 22222222 -u zhao -e 0x8000189e80c2a496209b99a247 142.100.5.9:162\r\n"\

	//by hulw 070319, to solve the issue of memory leak when update snmp trap session list.
	//For all the NMSs, just have one set for trap alarm level
	//trap sink will be updated in trap send task

	FILE			*fp = NULL;

	fp = fopen(CONFIG_FILE_SNMPD_CONF, "wt");
	if (fp == NULL)
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_WARNING, 
			"Fails on creating snmpd.conf file.\n");
		return ERR_SNP_OPEN_FILE;
	}

	fprintf(fp, snmpdcfg);

	for (i = 0; i < nNmsNum; i++)
	{
	    if((pNmsInfos[i]).bIPV6 == FALSE)//for IPV4
	    {
		//rocommunity
		fprintf(fp, "%s %s %s\n",
		    RO_COMMUNITY_TOCKEN,
		    (pNmsInfos[i]).szPublicCommunity,
		    inet_ntoa(*((struct in_addr *)&((pNmsInfos[i]).ulIpAddress))));

		//rwcommunity
		fprintf(fp, "%s %s %s\n",
		    RW_COMMUNITY_TOCKEN,
		    (pNmsInfos[i]).szPrivateCommunity,
		    inet_ntoa(*((struct in_addr *)&((pNmsInfos[i]).ulIpAddress))));

		//trap2sink
		if((pNmsInfos[i]).nTrapEnable == 1)
		{
		    fprintf(fp, "%s %s public\n",
			TRAP2_SINK_TOCKEN,
			inet_ntoa(*((struct in_addr *)&((pNmsInfos[i]).ulIpAddress))));	
		}
	    }
	    else//for IPV6
	    {
		inet_ntop(AF_INET6, (const void *)(&((pNmsInfos[i]).unIPV6Address)), szV6IP, 128);
		printf("V6 IP is %s\n", szV6IP);

		//rocommunity6
		fprintf(fp, "%s %s %s\n",
		    RO_V6_COMMUNITY_TOCKEN,
		    (pNmsInfos[i]).szPublicCommunity,
		    szV6IP);

		//rwcommunity
		fprintf(fp, "%s %s %s\n",
		    RW_V6_COMMUNITY_TOCKEN,
		    (pNmsInfos[i]).szPrivateCommunity,
		    szV6IP);

		//trap2sink
		if((pNmsInfos[i]).nTrapEnable == 1)
		{
		    fprintf(fp, "%s udp6:[%s]:162 public\n",
			TRAP2_SINK_TOCKEN,
			szV6IP);	
		}
	    }
	}

	//If has no nms info, to prevent all nms access the snmpd service
	if (nNmsNum == 0)
	{
		//rocommunity
		fprintf(fp, "%s %s %s\n",
			RO_COMMUNITY_TOCKEN,
			"self",
			"127.0.0.1");
	}

	for (i = 0; i < nV3NmsNum; i++)
	{
	    fprintf(fp, "createUser %s MD5 \"%s\" DES \"%s\"\r\n",
		(pV3NmsInfos[i]).szUserName,
		(pV3NmsInfos[i]).szAuthPwd,
		(pV3NmsInfos[i]).szPrivPwd);

	    fprintf(fp, "group readgroup usm %s\r\n",
		(pV3NmsInfos[i]).szUserName);

	    if((pV3NmsInfos[i]).nV3TrapEnable == 1)
	    {
		if((pV3NmsInfos[i]).nTrapSecurityLevel == 0)
		{
		    strcpy(sztrapSecuritylevel, "noauthnoPriv");
		}
		else if((pV3NmsInfos[i]).nTrapSecurityLevel == 1)
		{
		    strcpy(sztrapSecuritylevel, "authnoPriv");
		}
		else
		{
		    strcpy(sztrapSecuritylevel, "authPriv");
		}
		if((pV3NmsInfos[i]).bIPV6 == FALSE)
		{
		    fprintf(fp, "trapsess -v 3 -l %s -a MD5 -A %s -x DES -X %s -u %s %s:162\r\n",
		    sztrapSecuritylevel, 
		    (pV3NmsInfos[i]).szAuthPwd, 
		    (pV3NmsInfos[i]).szPrivPwd,
		    (pV3NmsInfos[i]).szUserName,
		    //(pV3NmsInfos[i]).szEngineID,
		    inet_ntoa(*((struct in_addr *)&((pV3NmsInfos[i]).ulTrapIpAddress))));
		}
		else
		{
		    inet_ntop(AF_INET6, (const void *)(&((pV3NmsInfos[i]).ulTrapIpV6Address)), szV6IP, 128);
		    fprintf(fp, "trapsess -v 3 -l %s -a MD5 -A %s -x DES -X %s -u %s %s:162\r\n",
			sztrapSecuritylevel, 
			(pV3NmsInfos[i]).szAuthPwd, 
			(pV3NmsInfos[i]).szPrivPwd,
			(pV3NmsInfos[i]).szUserName,
			//(pV3NmsInfos[i]).szEngineID,
			szV6IP);
		}
	    }
	}

	//here we use the 3rd engine type.The format of the SNMP engine ID is defined in RFC 5343.
	//IPV4 address encoded engine ID
	//80 00 1F 88 01 ip ip ip ip
	//IPV6 address encoded engine ID
	//80 00 1F 88 02 ip ip ip ip ip ip ip ip ip ip ip ip ip ip ip ip
	//MAC address encoded engine ID
	//80 00 1F 88 03 xx xx xx xx xx xx
	//Text String encoded engine ID
	//80 00 1F 88 04 text string
	//if(nV3NmsNum != 0)
	{
	    fprintf(fp, "%s\n", "engineIDType 3");
	}

	//Added by Hrishikesh Oak for CR - Improve the SNMP system group OID values
	fprintf(fp, "syscontact dcpower.tac@Vertiv.com\n");
	fprintf(fp, "sysname NetSure Control Unit\n");
	fprintf(fp, "syslocation Global\n");

	// 4. Write done
	fclose(fp);

	AppLogOut(SNMP_UI_TASK, APP_LOG_UNUSED,//APP_LOG_UNUSED, 
		"The snmpd.conf is updated done.\n");

	extern int		reconfig;

	reconfig = 1;//To refresh the nms info, should be set to 1. used in the 
	//function of receive() of snmpd.c


	if (bUpdatePriCfg)
	{
#ifdef GET_NMS_INFO_FROM_CONFIG_FILE
		//by HULONGWEN, update snmp_private.cfg
#else
		g_nNmsUserNumber = nNmsNum;
		g_V3nNmsUserNumber = nV3NmsNum;
		g_nTrapLevel = g_sSnmpInfostru.nTrapLevel;
		//printf("Before update to flash, g_nNmsUserNumber is %d g_V3nNmsUserNumber is %d g_nTrapLevel is %d\n", g_nNmsUserNumber, g_V3nNmsUserNumber, g_nTrapLevel);
		UpdateUserInfoToFlash();
#endif

	}

	return ERR_SNP_OK;
}

static BOOL AddCurrentAlarmsToQueue(HANDLE hTrapSendQueue)
{
	int				nActiveAlarmNum = 0;
	ALARM_SIG_VALUE* pActiveAlarmSigValue = NULL;
	ALARM_SIG_VALUE* pAlarmSigValue = NULL;

	if (DXI_GetActiveAlarmItems(&nActiveAlarmNum, 
		&pActiveAlarmSigValue)
		== ERR_DXI_OK)
	{
		int i;

		ALARM_SIG_VALUE_EXT	stAlarmSigValue;

		pAlarmSigValue = pActiveAlarmSigValue;

		for (i = 0; i < nActiveAlarmNum; i++, pActiveAlarmSigValue++)
		{
			memcpy(&(stAlarmSigValue.stAlarmSigValue),
				pActiveAlarmSigValue, 
				sizeof(ALARM_SIG_VALUE));

			stAlarmSigValue.nAlarmMsgType = _ALARM_OCCUR_MASK;

			g_sSnmpInfostru.ulAlarmSequenceID += 1;

			stAlarmSigValue.ulAlarmSequenceID = 
				g_sSnmpInfostru.ulAlarmSequenceID;

			//Put the report info to alarm queue
			Queue_Put(hTrapSendQueue, &stAlarmSigValue, FALSE);
			
		}

		DELETE_ITEM(pAlarmSigValue);

		return TRUE;
	}

	return FALSE;
}

#define		GET_SERVICE_OF_SNMP_NAME	"snmp_agent.so"
#define		GET_SERVICE_OF_SNMPV3_NAME	"snmpv3_agent.so"
/*==========================================================================*
 * FUNCTION : ServiceMain
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: SERVICE_ARGUMENTS *  pArgs : 
 * RETURN   : DWORD : service exit code, defined in app_service.h
 * COMMENTS : 
 * CREATOR  : HULONGWEN                   DATE: 2004-10-21 19:30
 *==========================================================================*/
DWORD ServiceMain(SERVICE_ARGUMENTS *pArgs)
{
	int			iRet;
	int			nError = SERVICE_EXIT_OK;
	APP_SERVICE		*SNMP_AppService = NULL;
	SNMP_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_SNMPV3_NAME);
	if(SNMP_AppService == NULL)
	{
		SNMP_AppService = ServiceManager_GetService(SERVICE_GET_BY_LIB,GET_SERVICE_OF_SNMP_NAME);
	}
#define SNMPD_PARA_NUM 7 
//#define SNMPD_PARA_NUM	3
	char *argv[SNMPD_PARA_NUM];

	//char szCfgFileName[MAX_FILE_PATH]; 

	UNUSED(pArgs);

	printf("SNMP V3 Agent is starting...\n");
	TRACEX("SNMP Agent is starting...\n");

	memset(&g_sSnmpInfostru, 0, sizeof(g_sSnmpInfostru));

	g_sSnmpInfostru.hThreadThis = RunThread_GetId(NULL);

	//Feed dog first
	RunThread_Heartbeat(g_sSnmpInfostru.hThreadThis);

	g_sSnmpInfostru.hNmsInfoSyncLock = Mutex_Create(TRUE);
	if (g_sSnmpInfostru.hNmsInfoSyncLock == NULL)
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"[%s]--ServiceMain: ERROR: Snmp nms info mutex creat error",  
			__FILE__);

			goto __SNMP_NMS_MUTEX;
	}

	//Load snmp private config
	if (LoadSNMPPrivateConfig(&g_sSnmpInfostru) != ERR_SNP_OK)
	{
		goto __SNMP_RPIVATE_CONFIG;
		
	}
	if(SNMP_AppService != NULL)
	{
		SNMP_AppService->bReadyforNextService = TRUE;
	}
#ifndef GET_NMS_INFO_FROM_CONFIG_FILE

	printf("Now initial the nms structure!\n");
	g_sSnmpInfostru.nNmsNum = g_nNmsUserNumber;
	g_sSnmpInfostru.nV3NmsNum = g_V3nNmsUserNumber;
	g_sSnmpInfostru.pNmsInfos = g_NmsUserManage;
	g_sSnmpInfostru.pV3NmsInfos = g_V3NmsUserManage;
	g_sSnmpInfostru.nTrapLevel = g_nTrapLevel;

#endif

#ifdef G3_OPT_DEBUG_THREAD
	AppLogOut("G3 DEBUG THREAD", APP_LOG_UNUSED,
		"Thread SNMP V3 Service was created, Thread Id = %d.\n", gettid());
#endif

	RunThread_Heartbeat(g_sSnmpInfostru.hThreadThis);

	UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum,
		g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, FALSE);

	/* 3.register in the DXI module to receive Alarms */
	iRet = DxiRegisterNotification("SNMP Agent", 
		ServiceNotification,
		&g_sSnmpInfostru, // it shall be changed to the actual arg
		_ALARM_OCCUR_MASK|_ALARM_CLEAR_MASK/*|_SAMPLE_MASK|_SET_MASK|_CONTROL_MASK*/, 
		g_sSnmpInfostru.hThreadThis,
		_REGISTER_FLAG);

	if (iRet != 0)
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR,
			"Fails on calling DxiRegisterNotification.\n");

		goto __REGISTER_NOTIFYCATION;
	}

	// 1. init the trap send queue
	g_sSnmpInfostru.hTrapSendQueue = Queue_Create(
		MAX_PENDING_TRAP_SEND,
		sizeof(ALARM_SIG_VALUE_EXT),
		5);

	if (g_sSnmpInfostru.hTrapSendQueue == NULL)
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"Fails on creating the trap send queue.\n");

		goto __QUEUE_CREATE;

	}

	AddCurrentAlarmsToQueue(g_sSnmpInfostru.hTrapSendQueue);
	
	// create the trap send task.
	g_sSnmpInfostru.hTrapSendThread = RunThread_Create("SNMP_TRAP",
		(RUN_THREAD_START_PROC)Snmp_TrapSendTask,
		(void *)(&g_sSnmpInfostru),
		NULL,
		0);

	if (g_sSnmpInfostru.hTrapSendThread == NULL)
	{
		AppLogOut(SNMP_UI_TASK, APP_LOG_ERROR, 
			"Fails on creating snmp trap send task.\n");

		goto __THREAD_CREATE;
	}

	RunThread_Heartbeat(g_sSnmpInfostru.hThreadThis);

	argv[0] = "snmpd";	//no use 
	argv[1] = "udp:161,udp6:161";
	argv[2] = "-L";		//Do not open a log file; use stdout/stderr instead.
	argv[3] = "-C";		//Do not read any configuration files except the one  option
	argv[4] = "-c";		//-c file, Read file as a configuration file.
	argv[5] = CONFIG_FILE_SNMPD_CONF;
	argv[6] = "-f";
	//argv[6] = CONFIG_FILE_V6_CONF;

	//argv[0] = "snmpd";	//no use 
	//argv[1] = "-C";		//Do not read any configuration files except the one  option
	//argv[2] = "-f";		//Do not fork

	//Call the snmp agent main function
extern int snmpd(int argc, char *argv[]);


	snmpd(SNMPD_PARA_NUM, argv);//snmpd() is snmp agent entry function


	//quit handle	

	TRACEX("SNMP Agent exited.\n");

	//Stop the snmp trap send thread
	RunThread_Stop(g_sSnmpInfostru.hTrapSendThread,
			3000,
			TRUE);


__THREAD_CREATE:
	if (g_sSnmpInfostru.hTrapSendQueue != NULL)
	{
		Queue_Destroy(g_sSnmpInfostru.hTrapSendQueue);
	}


__QUEUE_CREATE:
	// unresigster
	DxiRegisterNotification("SNMP Agent", 
		ServiceNotification,
		&g_sSnmpInfostru, // it shall be changed to the actual arg
		_ALARM_OCCUR_MASK|_ALARM_CLEAR_MASK/*|_SAMPLE_MASK|_SET_MASK|_CONTROL_MASK*/, 
		g_sSnmpInfostru.hThreadThis,
		_CANCEL_FLAG);


__REGISTER_NOTIFYCATION:
	DestroySNMPPrivateConfig();

__SNMP_RPIVATE_CONFIG:
	if (g_sSnmpInfostru.hNmsInfoSyncLock != NULL) 
	{ 
		Mutex_Destroy(g_sSnmpInfostru.hNmsInfoSyncLock);

		g_sSnmpInfostru.hNmsInfoSyncLock = NULL;
	}


__SNMP_NMS_MUTEX:
	nError = ERR_SNP_INIT_HANDLE;

	return nError;

}



void *ServiceInit(IN int argc, IN char **argv)//args
{
	UNUSED(argc);
	UNUSED(argv);

	TRACEX("SNMP Agent is initializing...\n");

	return NULL;
}

static int NmsV3UserExist(char* username)
{
    int i;

    for (i = 0; i < g_sSnmpInfostru.nV3NmsNum;i++)
    {
	if(strcmp((const char*)(g_sSnmpInfostru.pV3NmsInfos)[i].szUserName, (const char*)username) == 0)
	{
	    return i;
	}
    }

    return -1;
}

/*==========================================================================*
* FUNCTION : NmsV6UserExist
* PURPOSE  : Find nms user accoding to its key word: ip address, this is the V6 version
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: unsigned long  ulIpAddress : 
* RETURN   : static int : -1, not found, others: offset
* COMMENTS : 
* CREATOR  : Zhao Zicheng                DATE: 2014-08-24
*==========================================================================*/
static int NmsV6UserExist(IN6_ADDR  ulV6IpAddress)
{
	int i;

	for (i = 0; i < g_sSnmpInfostru.nNmsNum;i++)
	{
	    if((g_sSnmpInfostru.pNmsInfos)[i].bIPV6 == TRUE)
	    {
		if(((g_sSnmpInfostru.pNmsInfos)[i].unIPV6Address.in6_u.u6_addr32[0] == ulV6IpAddress.in6_u.u6_addr32[0]) &&
		    ((g_sSnmpInfostru.pNmsInfos)[i].unIPV6Address.in6_u.u6_addr32[1] == ulV6IpAddress.in6_u.u6_addr32[1]) &&
		    ((g_sSnmpInfostru.pNmsInfos)[i].unIPV6Address.in6_u.u6_addr32[2] == ulV6IpAddress.in6_u.u6_addr32[2]) &&
		    ((g_sSnmpInfostru.pNmsInfos)[i].unIPV6Address.in6_u.u6_addr32[3] == ulV6IpAddress.in6_u.u6_addr32[3]))
		{
		    return i;
		}
	    }
	}

	return -1;
}


/*==========================================================================*
 * FUNCTION : NmsUserExist
 * PURPOSE  : Find nms user accoding to its key word: ip address
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: unsigned long  ulIpAddress : 
 * RETURN   : static int : -1, not found, others: offset
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:37
 * MODIFY   : Zhao Zicheng	       DATE: 2014-08-24
 *==========================================================================*/
static int NmsUserExist(unsigned long    ulIpAddress)
{
	int i;

	for (i = 0; i < g_sSnmpInfostru.nNmsNum;i++)
	{
	    if((g_sSnmpInfostru.pNmsInfos)[i].bIPV6 == FALSE)
	    {
		if ((g_sSnmpInfostru.pNmsInfos)[i].ulIpAddress == ulIpAddress)
		{
		    return i;
		}
	    }
	}

	return -1;
}

static int AddV3NmsInfo(V3NMS_INFO* pV3NmsInfos)
{
    int nError;

    ASSERT(pV3NmsInfos);

    nError = ERR_SNP_OK;

    if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
	MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
    {
	if (g_sSnmpInfostru.nV3NmsNum < V3_NMS_USERS_MAX_NUMBER)
	{
	    if (NmsV3UserExist(pV3NmsInfos->szUserName) == -1)
	    {
#ifdef GET_NMS_INFO_FROM_CONFIG_FILE

		NMS_INFO* pNmsInfoOld;

		pNmsInfoOld = g_sSnmpInfostru.pNmsInfos;
		g_sSnmpInfostru.pNmsInfos = 
		    NEW(NMS_INFO, g_sSnmpInfostru.nNmsNum + 1);

		memcpy(g_sSnmpInfostru.pNmsInfos, pNmsInfoOld, 
		    sizeof(NMS_INFO) * g_sSnmpInfostru.nNmsNum);

		DELETE_ITEM(pNmsInfoOld);

		/*RENEW(NMS_INFO, g_sSnmpInfostru.pNmsInfos,
		g_sSnmpInfostru.nNmsNum + 1);*/
#endif


		memcpy((g_sSnmpInfostru.pV3NmsInfos) + g_sSnmpInfostru.nV3NmsNum,
		    pV3NmsInfos, sizeof(V3NMS_INFO));

		g_sSnmpInfostru.nV3NmsNum += 1;

		//by HULONGWEN, should save to FLASH 
		UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum, 
		    g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, TRUE);

	    }
	    else
	    {
		nError = ERR_NMS_USER_ALREADY_EXISTED;
	    }
	}
	else
	{
	    nError = ERR_NMS_TOO_MANY_USERS;
	}

	Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
    }
    else
    {
	nError = ERR_NMS_MUTEX_TIMEOUT;
    }

    return nError;
}


/*==========================================================================*
 * FUNCTION : AddNmsInfo
 * PURPOSE  : Add a nms info 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: NMS_INFO*  pNmsInfo : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:38
 *==========================================================================*/
static int AddNmsInfo(NMS_INFO* pNmsInfo)
{
	int nError;

	ASSERT(pNmsInfo);

	nError = ERR_SNP_OK;

	if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
		MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
	{
		if (g_sSnmpInfostru.nNmsNum < NMS_USERS_MAX_NUMBER)
		{
			if(((pNmsInfo->bIPV6 == FALSE) && (NmsUserExist(pNmsInfo->ulIpAddress) == -1)) ||
			    ((pNmsInfo->bIPV6 == TRUE) && (NmsV6UserExist(pNmsInfo->unIPV6Address) == -1)))
			{
#ifdef GET_NMS_INFO_FROM_CONFIG_FILE

				NMS_INFO* pNmsInfoOld;

				pNmsInfoOld = g_sSnmpInfostru.pNmsInfos;
				g_sSnmpInfostru.pNmsInfos = 
					NEW(NMS_INFO, g_sSnmpInfostru.nNmsNum + 1);

				memcpy(g_sSnmpInfostru.pNmsInfos, pNmsInfoOld, 
					sizeof(NMS_INFO) * g_sSnmpInfostru.nNmsNum);

				DELETE_ITEM(pNmsInfoOld);

				/*RENEW(NMS_INFO, g_sSnmpInfostru.pNmsInfos,
				g_sSnmpInfostru.nNmsNum + 1);*/
#endif


				memcpy((g_sSnmpInfostru.pNmsInfos) + g_sSnmpInfostru.nNmsNum,
					pNmsInfo, sizeof(NMS_INFO));

				g_sSnmpInfostru.nNmsNum += 1;
	
				//by HULONGWEN, should save to FLASH 
				UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum, 
				    g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, TRUE);

				}
			else
			{
				nError = ERR_NMS_USER_ALREADY_EXISTED;
			}
		}
		else
		{
			nError = ERR_NMS_TOO_MANY_USERS;
		}

		Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
	}
	else
	{
		nError = ERR_NMS_MUTEX_TIMEOUT;
	}

	return nError;
}

static int ModifyV3NmsInfo(V3NMS_INFO* pV3NmsInfos)
{

    int nError;

    int nFind;

    ASSERT(pV3NmsInfos);

    nError = ERR_SNP_OK;

    if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
	MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
    {

	nFind = NmsV3UserExist(pV3NmsInfos->szUserName);

	if (nFind >= 0)
	{
	    memcpy((g_sSnmpInfostru.pV3NmsInfos) + nFind,
		pV3NmsInfos, sizeof(V3NMS_INFO));

	    //by HULONGWEN, should save to FLASH 
	    UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum, 
		g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, TRUE);
	}
	else
	{
	    nError = ERR_SNP_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
    }
    else
    {
	nError = ERR_NMS_MUTEX_TIMEOUT;
    }

    return nError;
}

/*==========================================================================*
 * FUNCTION : ModifyNmsInfo
 * PURPOSE  : Modify a nms info
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: NMS_INFO*  pNmsInfo : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:39
 *==========================================================================*/
static int ModifyNmsInfo(NMS_INFO* pNmsInfo)
{

	int nError;

	int nFind;

	ASSERT(pNmsInfo);

	nError = ERR_SNP_OK;

	if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
		MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
	{
		if(pNmsInfo->bIPV6 == FALSE)
		{
		    nFind = NmsUserExist(pNmsInfo->ulIpAddress);
		}
		else
		{
		    nFind = NmsV6UserExist(pNmsInfo->unIPV6Address);
		}

		if (nFind >= 0)
		{
			memcpy((g_sSnmpInfostru.pNmsInfos) + nFind,
					pNmsInfo, sizeof(NMS_INFO));

			//by HULONGWEN, should save to FLASH 
			UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum, 
			    g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, TRUE);
		}
		else
		{
			nError = ERR_SNP_USER_NOT_EXISTED;
		}

		Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
	}
	else
	{
		nError = ERR_NMS_MUTEX_TIMEOUT;
	}

	return nError;
}
static int ModifyNmsTrapInfo(int* pNmsInfo)
{
	int nError;

	int nFind;
	
	int i;
	
	NMS_INFO* pNmsTrapInfo;

	ASSERT(pNmsInfo);

	nError = ERR_SNP_OK;
	

	if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
		MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
	{
		g_sSnmpInfostru.nTrapLevel = *pNmsInfo;
		g_nTrapLevel = g_sSnmpInfostru.nTrapLevel;
		printf("Before update to flash, g_nNmsUserNumber is %d g_V3nNmsUserNumber is %d g_nTrapLevel is %d\n", g_nNmsUserNumber, g_V3nNmsUserNumber, g_nTrapLevel);
		UpdateUserInfoToFlash();
		Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
	}
	else
	{
		nError = ERR_NMS_MUTEX_TIMEOUT;
	}

	return nError;
}

static int DeleteV3NmsInfo(char* username)
{
    int nError;

    int nFind;

    nError = ERR_SNP_OK;

    if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
	MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
    {

	nFind = NmsV3UserExist(username);

	if (nFind >= 0)
	{
	    memmove((g_sSnmpInfostru.pV3NmsInfos) + nFind, 
		(g_sSnmpInfostru.pV3NmsInfos) + nFind + 1, 
		sizeof(V3NMS_INFO)* (g_sSnmpInfostru.nV3NmsNum - nFind - 1));

	    memset(&((g_sSnmpInfostru.pV3NmsInfos)[g_sSnmpInfostru.nV3NmsNum - 1]), 
		0, 
		sizeof(V3NMS_INFO));

	    (g_sSnmpInfostru.nV3NmsNum)--;

	    //by HULONGWEN, should save to FLASH 
	    UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum,
		g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, TRUE);
	}
	else
	{
	    nError = ERR_SNP_USER_NOT_EXISTED;
	}

	Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
    }
    else
    {
	nError = ERR_NMS_MUTEX_TIMEOUT;
    }

    return nError;

}

/*==========================================================================*
 * FUNCTION : DeleteNmsInfo
 * PURPOSE  : Delete a nms info
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: unsigned long  ulIpAddress : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:39
 *==========================================================================*/
static int DeleteNmsInfo(NMS_INFO* pNmsInfo)
{
	int nError;

	int nFind;

	nError = ERR_SNP_OK;

	if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
		MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
	{

	    if(pNmsInfo->bIPV6 == FALSE)
	    {
		nFind = NmsUserExist(pNmsInfo->ulIpAddress);
	    }
	    else
	    {
		nFind = NmsV6UserExist(pNmsInfo->unIPV6Address);
	    }

		if (nFind >= 0)
		{
			memmove((g_sSnmpInfostru.pNmsInfos) + nFind, 
				(g_sSnmpInfostru.pNmsInfos) + nFind + 1, 
				sizeof(NMS_INFO)* (g_sSnmpInfostru.nNmsNum - nFind - 1));

			memset(&((g_sSnmpInfostru.pNmsInfos)[g_sSnmpInfostru.nNmsNum - 1]), 
				0, 
				sizeof(NMS_INFO));

			(g_sSnmpInfostru.nNmsNum)--;

			//by HULONGWEN, should save to FLASH 
				UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum,
					g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, TRUE);
		}
		else
		{
			nError = ERR_SNP_USER_NOT_EXISTED;
		}

		Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
	}
	else
	{
		nError = ERR_NMS_MUTEX_TIMEOUT;
	}

	return nError;

}


/*==========================================================================*
 * FUNCTION : ServiceConfig
 * PURPOSE  : Call the function to add or delete or modify nms info
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE             hServiceThread    : 
 *            BOOL               bServiceIsRunning : 
 *            SERVICE_ARGUMENTS  *pArgs            : 
 *            int                nVarID            : 
 *            int                *nBufLen          : 
 *            void               *pDataBuf         : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:52
 *==========================================================================*/
int ServiceConfig(HANDLE hServiceThread,
				  BOOL bServiceIsRunning,
				  SERVICE_ARGUMENTS *pArgs, 
				  int nVarID, 
				  int *nBufLen, 
				  void *pDataBuf)
{
	int nError = ERR_SNP_OK;

	UNUSED(hServiceThread);
	UNUSED(pArgs);
	UNUSED(bServiceIsRunning);//Should be used, by HULONGWEN

	if (nVarID == GET_NMS_USER_INFO)
	{
		*nBufLen = g_sSnmpInfostru.nNmsNum * sizeof(NMS_INFO);

		(*((NMS_INFO**)pDataBuf)) = g_sSnmpInfostru.pNmsInfos;
	}
	else if (nVarID == ADD_NMS_USER_INFO)
	{
		nError = AddNmsInfo((NMS_INFO*)pDataBuf);
	}
	else if (nVarID == MODIFY_NMS_USER_INFO)
	{
		nError = ModifyNmsInfo((NMS_INFO*)pDataBuf);
	}
	else if (nVarID == DELETE_NMS_USER_INFO)
	{
		nError = DeleteNmsInfo((NMS_INFO*)pDataBuf);
	}
	else if (nVarID == MODIFY_NMS_TRAP_INFO)
	{
		nError = ModifyNmsTrapInfo((int*)pDataBuf);
	}
	else if(nVarID == GETTRAP_TYPE_OF_NMS)
	{
	    //g_sSnmpInfostru.nTrapLevel = 0;
	    //printf("nTrapLevel is %d\n", g_sSnmpInfostru.nTrapLevel);
	    //printf("Before get the value of nTrapLevel\n");
	    //printf("the address of pointer is %x\n", pDataBuf);
	    (*((int*)pDataBuf)) = g_sSnmpInfostru.nTrapLevel;
	    //printf("Get the value of nTrapLevel\n");
	}
	else if (nVarID == DELETE_ALL_NMS_USER_INFO)
	{
		nError = DeleteAllNmsInfo();
	}
	// SNMPV3
	else if(nVarID == GET_NMS_V3_USER_INFO)
	{
	    *nBufLen = g_sSnmpInfostru.nV3NmsNum * sizeof(V3NMS_INFO);

	    (*((V3NMS_INFO**)pDataBuf)) = g_sSnmpInfostru.pV3NmsInfos;
	}
	else if (nVarID == ADD_NMS_V3_USER_INFO)
	{
	    nError = AddV3NmsInfo((V3NMS_INFO*)pDataBuf);
	}
	else if (nVarID == MODIFY_NMS_V3_USER_INFO)
	{
	    nError = ModifyV3NmsInfo((V3NMS_INFO*)pDataBuf);
	}
	else if (nVarID == DELETE_NMS_V3_USER_INFO)
	{
	    nError = DeleteV3NmsInfo((char *)pDataBuf);
	}
	else if(nVarID == DELETE_ALL_NMS_V3_USER_INFO)
	{
	    nError = DeleteAllV3NmsInfo();
	}


	return nError;
}

/*==========================================================================*
 * FUNCTION : GetTimeString
 * PURPOSE  : Get a DateAndTime string according to time_t struct 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: time_t  tCurtime : 
 *            char*   string   : return the needed string
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:40
 *==========================================================================*/
int GetTimeString(time_t tCurtime, char* string)
{
	struct tm		tmCurtime;
	char			tmp;
#define TM_YEAR_START	1900

	gmtime_r(&(tCurtime), &tmCurtime);

	*(string +1 ) = 0;
	*((short*)string) = tmCurtime.tm_year + TM_YEAR_START;
	tmp = *(string);
	*(string) = *(string + 1);
	*(string + 1) = tmp;
	*(string + 2) = tmCurtime.tm_mon + 1;
	*(string + 3) = tmCurtime.tm_mday;
	*(string + 4) = tmCurtime.tm_hour;
	*(string + 5) = tmCurtime.tm_min;
	*(string + 6) = tmCurtime.tm_sec;
	*(string + 7) = 0;
	*(string + 8) = '+';
	*(string + 9) = 0;
	*(string + 10) = 0;

	return ERR_SNP_OK;
}

/*==========================================================================*
 * FUNCTION : GetAlarmSeverity
 * PURPOSE  : Convert the actual alarm level to trap send severity
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nAlarmLevel : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:41
 *==========================================================================*/
int GetAlarmSeverity(int nAlarmLevel)
{
	int nAlarmSeverity = INVALID_ALARM_SEVERITY;

	if (nAlarmLevel == ALARM_LEVEL_OBSERVATION)
	{
		nAlarmSeverity = OBSERVATION_ALARM_SEVERITY;
	}
	else if (nAlarmLevel == ALARM_LEVEL_MAJOR)
	{
		nAlarmSeverity = MAJOR_ALARM_SEVERITY;
	}
	else if (nAlarmLevel == ALARM_LEVEL_CRITICAL)
	{
		nAlarmSeverity = CRITICAL_ALARM_SEVERITY;
	}

	return nAlarmSeverity;
}

/*==========================================================================*
 * FUNCTION : GetAlarmDescription
 * PURPOSE  : Get alarm description: alarm name + its owner
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ALARM_SIG_VALUE*  pAlarmSigValue : 
 *            char*             string         : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:42
 *==========================================================================*/
int GetAlarmDescription(ALARM_SIG_VALUE* pAlarmSigValue, char* string)
{
	sprintf(string, "%s, its owner: %s", 
			pAlarmSigValue->pStdSig->pSigName->pFullName[0],
			pAlarmSigValue->pEquipInfo->pEquipName->pAbbrName[0]);

	return ERR_SNP_OK;
}

/*==========================================================================*
 * FUNCTION : FeedSnmpThreadDog
 * PURPOSE  : Feed snmp agent thread dog
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2005-01-11 11:43
 *==========================================================================*/
int FeedSnmpThreadDog(void)
{
	SNMP_INFO_STRUCT* pSnmpInfoStru;

	pSnmpInfoStru = GetSnmpInfoRef();

	ASSERT(pSnmpInfoStru);

	
	if (!THREAD_IS_RUNNING(pSnmpInfoStru->hThreadThis))
	{
		return 1;
	}

	RunThread_Heartbeat(pSnmpInfoStru->hThreadThis);

	return 0;
}

static int DeleteAllV3NmsInfo(void)
{
    int nError;

    int nFind;
    int nNMS;

    nError = ERR_SNP_OK;
    nNMS   = g_sSnmpInfostru.nV3NmsNum;
    if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
	MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
    {
	for (nFind = 0; nFind < nNMS; nFind++)
	{
	    /*			 memmove((g_sSnmpInfostru.pNmsInfos) + nFind, 
	    (g_sSnmpInfostru.pNmsInfos) + nFind + 1, 
	    sizeof(NMS_INFO)* (g_sSnmpInfostru.nNmsNum - nFind - 1));
	    */
	    memset(&((g_sSnmpInfostru.pV3NmsInfos)[nNMS - nFind]), 
		0, 
		sizeof(V3NMS_INFO));

	    (g_sSnmpInfostru.nV3NmsNum)--;

	}
	UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum,
	    g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, TRUE);

	Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
    }
    else
    {
	nError = ERR_NMS_MUTEX_TIMEOUT;
    }

    return nError;

}

static int DeleteAllNmsInfo(void)
{
    int nError;

	int nFind;
	int nNMS;

    nError = ERR_SNP_OK;
	nNMS   = g_sSnmpInfostru.nNmsNum;
    if (Mutex_Lock(g_sSnmpInfostru.hNmsInfoSyncLock,
        MAX_TIME_WAITING_NMS_INFO_CHANGE) == ERR_MUTEX_OK)
    {
		for (nFind = 0; nFind < nNMS; nFind++)
		{
/*			 memmove((g_sSnmpInfostru.pNmsInfos) + nFind, 
                (g_sSnmpInfostru.pNmsInfos) + nFind + 1, 
                sizeof(NMS_INFO)* (g_sSnmpInfostru.nNmsNum - nFind - 1));
*/
            memset(&((g_sSnmpInfostru.pNmsInfos)[nNMS - nFind]), 
                0, 
                sizeof(NMS_INFO));

            (g_sSnmpInfostru.nNmsNum)--;

		}
		UpdateNmsInfoToFile(g_sSnmpInfostru.nNmsNum, g_sSnmpInfostru.nV3NmsNum,
							g_sSnmpInfostru.pNmsInfos, g_sSnmpInfostru.pV3NmsInfos, TRUE);

		Mutex_Unlock(g_sSnmpInfostru.hNmsInfoSyncLock);
    }
    else
    {
        nError = ERR_NMS_MUTEX_TIMEOUT;
    }

    return nError;

}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : main_board.c
 *  CREATOR  : Frank Cao                DATE: 2008-07-25 16:50
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 * 
 *==========================================================================*/
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "main_board.h"



#define		_MAIN_BOARD_DEBUG	 0

#define MB_DEVICE_NAME					"/dev/imx_sampler"
#define MB_COEF_FILE_NAME				"/home/adjcoeff.log"
#define OB_COEF_FILE_NAME				"/home/OB_adjcoeff.log"
#define MB_OB_DEVICE_NAME				"/dev/imx_adc_sampleri2c"
#define MB_LED_DEVICE_NAME				"/dev/led"

#define  MB_LOG_OUT(iLevel, OutString) \
    AppLogOut("MB_SAMP", (iLevel), "%s", OutString)

//the alarm value of fuse or back
#define	LOAD_FUSE_ALARM_THRESHOLD			10
#define BATT_FUSE_ALARM_THRESHOLD			0.4
#define LVD_BACK_ALARM_THRESHOLD			10

#define LOAD_FUSE_ALARM_THRESHOLD_DELTA			5
#define BATT_FUSE_ALARM_THRESHOLD_DELTA			0.05
#define LVD_BACK_ALARM_THRESHOLD_DELTA			5

union unSAMPLING_VALUE
{
    float	fValue;
    int		iValue;
    UINT	uiValue;
    ULONG	ulValue;
    DWORD	dwValue;
};
typedef union unSAMPLING_VALUE SAMPLING_VALUE;
/*****************************************************************/
/*ģ���ں궨�� */
/*****************************************************************/
#define MB_FALG_RUNNING			0xa5a5

#define MB_BATT_GROUP_EQUIP_ID		115
#define MB_RECT_GROUP_EQUIP_ID		2
#define MB_POWER_SYS_EQUIP_ID		1
#define MB_DCDISTRIBUTION_EQUIP_ID	176
#define MB_CT_GROUP_EQUIP_ID		210
#define MB_MPPT_GROUP_EQUIP_ID		1350

#define MB_ESNA_CT_GROUP_EQUIP_ID	3000
#define MB_ESNA_CT_GROUP_CURRENT_ID	501
#define MB_ESNA_CT_GROUP_EFFICY_ID	504

#define MB_BATT1_EQUIP_ID		116
#define MB_BATT2_EQUIP_ID		117
#define MB_DC_EQUIP_ID			176
#define MB_LVD_EQUIP_ID			187
#define MB_RECT_AC_EQUIP_ID		104

#define MB_RATED_VOLT_SIG_ID		21

#define MB_LOAD_CURR_RANGE_ID		1
#define MB_LOAD_CURR_MV_ID		2
#define MB_BATT1_CURR_RANGE_ID		1
#define MB_BATT1_CURR_MV_ID		2
#define MB_BATT2_CURR_RANGE_ID		1
#define MB_BATT2_CURR_MV_ID		2
#define MB_LVD1_STATE_ID		1
#define MB_LVD2_STATE_ID		2
#define MB_MAINS_FAIL_ID		26


#define MB_BATT_NUM_ID					64
#define MB_NA_TYPE_ID					71
#define MB_IS_CALCULATE_CURRENT_ID	73
#define MB_BATTERY_CHANGE_ID			34
#define MB_DCD_LOAD_SHUNT_NUMBER_ID	3
#define MB_DCD_LOAD_CURRENT_ID		1
#define	MB_RCT_GROUP_CURRENT_ID		1
#define	MB_RCT_GROUP_VOLT_ID			2
#define	MB_CT_GROUP_CURRENT_ID		1
#define	MB_MPPT_GROUP_CURRENT_ID		1
#define	MB_CT_GROUP_VOLT_ID			2

#define MB_BATT_EXIST					0
#define MB_BATT_NOT_EXIST				1

#define	MB_BATT_TYPE_NO_BATT			0
#define	MB_BATT_TYPE_1_BATT			1
#define	MB_BATT_TYPE_2_BATT			2
#define	MB_BATT_TYPE_SPECIAL			3	// for NA system, 1 battery string, voltage = v1 - v2 

#define MB_IS_NA						1

/*****************************************************************/
/*ģ����ö�������� */
/*****************************************************************/
//MB �ɼ��� ͨ������
enum MB_SAMP_CHANNEL
{
    MB_CH_DC_VOLT = 1,		//DC Voltage
    MB_CH_DC_LOAD_CURR,		//DC Load Current
    MB_CH_BATT1_VOLT,		//Battery 1 Voltage
    MB_CH_BATT2_VOLT,		//Battery 2 Voltage
    MB_CH_BATT1_CURR,		//Battery 1 Current
    MB_CH_BATT2_CURR,		//Battery 2 Current
    MB_CH_TEMPERATURE1,		//Temperature 1
    MB_CH_TEMPERATURE2,		//Temperature 2
    MB_CH_SPD,				//SPD state
    MB_CH_LOAD_CURR_COM_VOLT,	//DC Load Current Common
    MB_CH_BATT1_CURR_COM_VOLT,	//Battery 1 Current Common
    MB_CH_BATT2_CURR_COM_VOLT,	//Battery 2 Current Common
    MB_CH_BATT1_ST,			//Battery 1 Statue
    MB_CH_BATT2_ST,			//Battery 2 Statue
    MB_CH_ABS_BATT_CURR1,
    MB_CH_ABS_BATT_CURR2,

    MB_CH_PASSWORD_JUMPER = 31,
    MB_CH_OS_READONLY_JUMPER,
    MB_CH_APP_READONLY_JUMPER,
    MB_CH_5V_DC_POWER_JUMPER,

    MB_CH_IB01_EXIST_ST = 51,		//IB01 Exist Status 0-not exist     1-exist
    MB_CH_IB01_DI1_ST,
    MB_CH_IB01_DI2_ST,
    MB_CH_IB01_DI3_ST,
    MB_CH_IB01_DI4_ST,	    
};

#ifdef _CODE_FOR_MINI
//MB_DEVICE_NAME ������������ȡ�źŶ���
enum MB_SAMPLING_DATA_IDX
{
    MB_IDX_GND = 0,
    MB_IDX_DC_VOLT_REF,
    MB_IDX_CURR_REF,
    MB_IDX_I_GND,
    MB_IDX_LOAD_CURR,
    MB_IDX_BATT1_CURR,
    MB_IDX_BATT2_CURR,
    MB_IDX_NO_USED1,		//MB_IDX_BATT2_CURR_COM_VOLT,
    MB_IDX_TEMPERATURE1,
    MB_IDX_TEMPERATURE2,
    MB_IDX_DC_VOLT,
    MB_IDX_BATT1_VOLT,
    MB_IDX_NO_USED2,		//MB_IDX_BATT2_VOLT,
    MB_IDX_NO_USED3,

    MB_IDX_LOADFUSE1,
    MB_IDX_BATTFUSE1,
    MB_IDX_BATTFUSE2,
    MB_IDX_BATTFUSE3,		//MB_IDX_BATT1_CURR_COM_VOLT,
    MB_IDX_BATTFUSE4,		//MB_IDX_LOAD_CURR_COM_VOLT,
    MB_IDX_LVD1_ST, 		//MB_IDX_LOADFUSE2,
    MB_IDX_LVD2_ST, 		//MB_IDX_NTC_TEMP,

    //MB_IDX_DIGTAL_INPUT,

    SIZE_OF_MB_ANALOG_CHN,
};
#else

//MB_DEVICE_NAME ������������ȡ�źŶ���
enum MB_SAMPLING_DATA_IDX
{
    MB_IDX_GND = 0,
    MB_IDX_DC_VOLT_REF,
    MB_IDX_CURR_REF,
    MB_IDX_I_GND,
    MB_IDX_LOAD_CURR,
    MB_IDX_BATT1_CURR,
    MB_IDX_BATT2_CURR,
    MB_IDX_LOAD_CURR_COM_VOLT,
    MB_IDX_BATT1_CURR_COM_VOLT,
    MB_IDX_BATT2_CURR_COM_VOLT,
    MB_IDX_TEMPERATURE1,
    MB_IDX_TEMPERATURE2,
    MB_IDX_DC_VOLT,
    MB_IDX_BATT1_VOLT,
    MB_IDX_BATT2_VOLT,
    MB_IDX_NO_USED,

    MB_IDX_LOADFUSE1,
    MB_IDX_LOADFUSE2,
    MB_IDX_BATTFUSE1,
    MB_IDX_BATTFUSE2,
    MB_IDX_NTC_TEMP,

    //MB_IDX_DIGTAL_INPUT,

    SIZE_OF_MB_ANALOG_CHN,
};
#endif


//�����ź�У׼ϵ��
enum MB_ADJUST_COEF_INFO
{
    MB_ADJUST_COEF_REF_VOLT = 0,
    MB_ADJUST_COEF_REF_CURR,
    MB_ADJUST_COEF_BUS_VOLT,
    MB_ADJUST_COEF_TEMP1,
    MB_ADJUST_COEF_TEMP2,
    MB_ADJUST_COEF_BATT_VOLT1,
    MB_ADJUST_COEF_BATT_VOLT2,

    //MB_ADJUST_COEF_LOADFUSE1,
    //MB_ADJUST_COEF_LOADFUSE2,

    //MB_ADJUST_COEF_BATTFUSE1,
    //MB_ADJUST_COEF_BATTFUSE2,

    MAX_NUM_MB_COEF,
};


//�������ͨ������
enum MB_CTL_CHANNEL
{   
    //MB_LED_DEVICE_NAME
    /*MB_CTL_CH_LED_RED,		
    MB_CTL_CH_LED_GREEN,		
    MB_CTL_CH_LED_YELLOW,		
    MB_CTL_CH_LED_COR_GREEN,	
    MB_CTL_CH_BEEP,		
    MB_CTL_CH_LCD_BACKLIGHT,		
    MB_CTL_CH_BEEP100,		
    MB_CTL_CH_SYS_ALARM,*/	

    //MB_IB01_DEVICE_NAME
    MB_CTL_CH_ALM_LIGHT = 1,	//Alarm light control
    MB_CTL_CH_BUS_ADJUST,	//Adjust the bus voltage
    
    MB_CTL_CH_IB01_DO1 = 51,	//DO1
    MB_CTL_CH_IB01_DO2,		//DO2
    MB_CTL_CH_IB01_DO3,		//DO3
    MB_CTL_CH_IB01_DO4,		//DO4

    

};
//MB_OB_DEVICE_NAME OB����������ȡ�źŶ���
enum OB_SAMPLING_DATA_IDX
{
    OB_IDX_FUEL_SENSOR1 = 0,
    OB_IDX_FUEL_SENSOR2,
    OB_IDX_LVD_AUX1,
    OB_IDX_LVD_AUX2,
    OB_IDX_TEMPERATURE,
    OB_IDX_REF_CAL,  //?
    OB_IDX_LOADFUSE1,
    OB_IDX_LOADFUSE2,
    OB_IDX_LOADFUSE3,
    OB_IDX_LOADFUSE4,
    OB_IDX_LOADFUSE5,
    OB_IDX_LOADFUSE6,
    OB_IDX_LOADFUSE7,
    OB_IDX_LOADFUSE8,
    OB_IDX_LOADFUSE9,
    OB_IDX_LOADFUSE10,

    OB_IDX_BATTFUSE1,
    OB_IDX_BATTFUSE2,
    OB_IDX_BATTFUSE3,
    OB_IDX_BATTFUSE4,


    OB_IDX_BREF,  // 2.5V�ο�
    OB_IDX_GND,   // GND

    SIZE_OF_OB_CHN,
};
//OB���ź�У׼ϵ��
enum OB_ADJUST_COEF_INFO
{
    OB_ADJUST_COEF_REF_VOLT = 0,
    OB_ADJUST_COEF_FUEL1,
    OB_ADJUST_COEF_FUEL2,
    OB_ADJUST_COEF_TEMP1,

    OB_ADJUST_COEF_LOADFUSE1,
    OB_ADJUST_COEF_LOADFUSE2,
    OB_ADJUST_COEF_LOADFUSE3,
    OB_ADJUST_COEF_LOADFUSE4,
    OB_ADJUST_COEF_LOADFUSE5,
    OB_ADJUST_COEF_LOADFUSE6,
    OB_ADJUST_COEF_LOADFUSE7,
    OB_ADJUST_COEF_LOADFUSE8,
    OB_ADJUST_COEF_LOADFUSE9,
    OB_ADJUST_COEF_LOADFUSE10,
    OB_ADJUST_COEF_BATTFUSE1,
    OB_ADJUST_COEF_BATTFUSE2,
    OB_ADJUST_COEF_BATTFUSE3,
    OB_ADJUST_COEF_BATTFUSE4,    

    OB_ADJUST_COEF_LVD_AUX1,
    OB_ADJUST_COEF_LVD_AUX2,
    MAX_NUM_OB_COEF,
};


/*****************************************************************/
/*ģ����ȫ�ֱ������� */
/*****************************************************************/
int				g_iRunningFlag;

static    float			s_afMbAdjustCoef[MAX_NUM_MB_COEF];
static    float			s_afOBAdjustCoef[MAX_NUM_OB_COEF];

//�������߳�
#define   MAGIC_COUNT_MAX      5		    //ħ�����ִ���


HANDLE				g_hMainBoardFilterThread = NULL;
DWORD	MainBoard_FilterTask(void);
static BOOL MB_IsLvd3Valid(void);
/*==========================================================================*
 * FUNCTION : MB_StrExtract
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char*      strSource      : 
 *            IN OUT char*  strDestination : 
 *            IN char       cSeparator     : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:44
 *==========================================================================*/
static int MB_StrExtract(IN char* strSource,
						 IN OUT char* strDestination, 
						 IN char cSeparator)
{
	int i = 0;
	
	if((!strSource) || (!strDestination))
	{
		return 0;
	}

    while(strSource[i] && (strSource[i] != cSeparator))
    {
        strDestination[i] = strSource[i];
        i++;
    }

    strDestination[i] = 0;
    
    if(strSource[i] && i)
    {
        return i + 1;
    }
	
    return 0;
}

#define SIZE_OF_MB_COEF_FILE		200
#define SIZE_OF_A_MB_COEF		20
/*==========================================================================*
 * FUNCTION : MB_ReadCoef
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-28 11:05
 *==========================================================================*/

#define	    COEFF_VALUE_UP	1.5
#define	    COEFF_VALUE_DOWN	0.5

static void MB_ReadCoef(void)
{
	int			iFileHandle;
	int			i;
	char		strReadBuf[SIZE_OF_MB_COEF_FILE];
	char		strExtracted[SIZE_OF_A_MB_COEF];
	float		ftemp;

	ZERO_POBJS(strReadBuf, SIZE_OF_MB_COEF_FILE);

	for(i = 0; i < MAX_NUM_MB_COEF; i++)
	{
		s_afMbAdjustCoef[i] = 1.00;
	}

 
	iFileHandle = open(MB_COEF_FILE_NAME, O_RDWR);
    
	if(iFileHandle > 0)
	{
		if(read(iFileHandle, strReadBuf, SIZE_OF_MB_COEF_FILE))
		{
			int		iPosition = 0;

			for(i = 0; i < MAX_NUM_MB_COEF; i++)
			{
				char* pchReadBuf = strReadBuf + iPosition;
				iPosition += MB_StrExtract(pchReadBuf, 
										strExtracted,
										',');
				ftemp = atof(strExtracted);
				if((ftemp > COEFF_VALUE_DOWN) && (ftemp < COEFF_VALUE_UP))
				{
				    s_afMbAdjustCoef[i] = ftemp;
				}

				//TRACE("afMbAdjustCoef[%d] = %f\n", i, s_afMbAdjustCoef[i]);
			}
		}
	}

	ZERO_POBJS(strReadBuf, SIZE_OF_MB_COEF_FILE);

	for(i = 0; i < MAX_NUM_OB_COEF; i++)
	{
		s_afOBAdjustCoef[i] = 1.00;
	}	

	iFileHandle = open(OB_COEF_FILE_NAME, O_RDWR);

	if(iFileHandle > 0)
	{
		if(read(iFileHandle, strReadBuf, SIZE_OF_MB_COEF_FILE))
		{
			int		iPosition = 0;

			for(i = 0; i < MAX_NUM_OB_COEF; i++)
			{
				char* pchReadBuf = strReadBuf + iPosition;
				iPosition += MB_StrExtract(pchReadBuf, 
					strExtracted,
					',');

				ftemp = atof(strExtracted);
				if((ftemp > COEFF_VALUE_DOWN) && (ftemp < COEFF_VALUE_UP))
				{
				    s_afOBAdjustCoef[i] = ftemp;
				}

				//TRACE("afMbAdjustCoef[%d] = %f\n", i, s_afMbAdjustCoef[i]);
			}
		}
	}


	return;
}



/*==========================================================================*
 * FUNCTION : MB_GetFloatSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nEquipId    : 
 *            int  nSignalType : 
 *            int  nSignalId   : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:44
 *==========================================================================*/
static float MB_GetFloatSigValue(int nEquipId, int nSignalType, int nSignalId)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		AppLogOut("MAINBORD SAMP", 
			APP_LOG_ERROR, 
			"MB_GetFloatSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
	}
	
	return pSigValue->varValue.fValue;

}

/*==========================================================================*
 * FUNCTION : MB_GetFloatSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nEquipId    : 
 *            int  nSignalType : 
 *            int  nSignalId   : 
 * RETURN   : static float : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:44
 *==========================================================================*/
static int MB_GetESNAConverterStatus(void)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	int		nEquipId = MB_ESNA_CT_GROUP_EQUIP_ID, nSignalType = SIG_TYPE_SAMPLING, nSignalId = MB_ESNA_CT_GROUP_CURRENT_ID;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		return FALSE; 
	}
	
	return TRUE;

}
/*==========================================================================*
 * FUNCTION : MB_GetDwordSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nEquipId    : 
 *            int  nSignalType : 
 *            int  nSignalId   : 
 * RETURN   : static DWORD : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:45
 *==========================================================================*/
static DWORD MB_GetDwordSigValue(int nEquipId, int nSignalType, int nSignalId)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		AppLogOut("CAN SAMP", 
			APP_LOG_ERROR, 
			"MB_GetDwordSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
	}	
	
	return pSigValue->varValue.ulValue;

}


/*==========================================================================*
 * FUNCTION : MB_GetEnumSigValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nEquipId    : 
 *            int  nSignalType : 
 *            int  nSignalId   : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:45
 *==========================================================================*/
static SIG_ENUM MB_GetEnumSigValue(int nEquipId, int nSignalType, int nSignalId)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		AppLogOut("MAINBOARD SAMP", 
			APP_LOG_ERROR, 
			"MB_GetEnumSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
	}	
	
	return pSigValue->varValue.enumValue;

}

static SIG_ENUM MB_SetEnumSigValue(int nEquipId, int nSignalType, int nSignalId, SIG_ENUM enValue)
{
	VAR_VALUE_EX	sigValue;

	sigValue.nSendDirectly = (int)TRUE;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = "MB Sample";
	sigValue.varValue.enumValue = enValue;

	int iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			nEquipId,
			DXI_MERGE_SIG_ID(nSignalType, nSignalId),
			sizeof(VAR_VALUE_EX),
			&sigValue,
			0);
	return	iRst;
}
static SIG_ENUM MB_SetDwordSigValue(int nEquipId, int nSignalType, int nSignalId, DWORD dwValue)
{
	VAR_VALUE_EX	sigValue;

	sigValue.nSendDirectly = (int)TRUE;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = "MB Sample";
	sigValue.varValue.ulValue = dwValue;

	int iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
			nEquipId,
			DXI_MERGE_SIG_ID(nSignalType, nSignalId),
			sizeof(VAR_VALUE_EX),
			&sigValue,
			0);
	return	iRst;
}
 
/*==========================================================================*
 * FUNCTION : MB_IsLvdSigValid
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iIdxLvdEquip : 
 *            int  iIdxLvdSig   : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:45
 *==========================================================================*/
#define	 EQUIP_EXIST_ID		100  
#define	 LVD1_ENABLE_ID		3
#define	 LVD2_ENABLE_ID		8

#define	 MB_LVD_DISABLE		0
#define	 MB_LVD_ENABLE		1

#define	 MB_LVD_EXIST		0
#define	 MB_LVD_NOT_EXIST	1

static BOOL	MB_IsLvdSigValid(int iIdxLvdEquip, int iIdxLvdSig)
{
	char				strOut[256];
	
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	int			iEquipId;
	int			iSetLvdEnableId;

	//Equip ID
	iEquipId = MB_LVD_EQUIP_ID + iIdxLvdEquip;	

	//Check if the device is exist
	//If not exist, return FALSE
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipId,
	    DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, EQUIP_EXIST_ID),
	    &iBufLen,
	    &pSigValue,
	    0);

	if(iRst != ERR_OK)
	{
	    return FALSE;
	}

	if(pSigValue->varValue.enumValue == MB_LVD_NOT_EXIST)
	{
	    return FALSE;
	}
			  

	//LVD Enable Setting ID
	if(iIdxLvdSig == 1)
	{
	    iSetLvdEnableId = LVD2_ENABLE_ID;
	}
	else
	{
	    iSetLvdEnableId = LVD1_ENABLE_ID;
	}	
	//Check if the lvd is enabled
	//If disabled, return FALSE
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipId,
	    DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, iSetLvdEnableId),
	    &iBufLen,
	    &pSigValue,
	    0);	

	if(pSigValue->varValue.enumValue == MB_LVD_DISABLE)
	{
	    return FALSE;
	}
			
        //20130925 ����,ɾ��
	////Check the lvd state
	////If not valid, return FALSE
	//iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	//				iEquipId,
	//				DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, iIdxLvdSig + MB_LVD1_STATE_ID),
	//				&iBufLen,
	//				&pSigValue,
	//				0);
	//
	//if(iRst != ERR_OK)
	//{
	//	sprintf(strOut, 
	//		"GC_GetEnumValue error! EquipId: %d, SigType: 0, SigId: %d\n",
	//		MB_LVD_EQUIP_ID + iIdxLvdEquip,
	//		iIdxLvdSig + MB_LVD1_STATE_ID);

	//	MB_LOG_OUT(ERR_CTL_GET_VLAUE, strOut);
	//}

	//return SIG_VALUE_IS_VALID(pSigValue);

	return TRUE;
}


#define	MAX_NUM_LVD_EQUIP			9
#define	MB_SIG_ALARM				1
#define	MB_SIG_NORMAL				0
#define	MB_CLEAR_VOLT_BENCHMARK		(40.0)

/*==========================================================================*
 * FUNCTION : MB_IsAllLvdDisconnect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-01-16 14:15
 *==========================================================================*/
static BOOL MB_IsAllLvdDisconnect(void)
{
	BOOL	bAllLvdDisconnect = TRUE;
	BOOL	bLvdValid = FALSE;
	int		i;

	for(i = 0; i < MAX_NUM_LVD_EQUIP; i++)
	{
		if(MB_IsLvdSigValid(i, 0))
		{
			bLvdValid = TRUE;
			if(MB_SIG_NORMAL
				== MB_GetEnumSigValue(MB_LVD_EQUIP_ID + i, 
									SIG_TYPE_SAMPLING,
									MB_LVD1_STATE_ID))
			{
				bAllLvdDisconnect = FALSE;
				break;
			}
		}

		if(MB_IsLvdSigValid(i, 1))
		{
			bLvdValid = TRUE;
			if(MB_SIG_NORMAL
				== MB_GetEnumSigValue(MB_LVD_EQUIP_ID + i, 
									SIG_TYPE_SAMPLING,
									MB_LVD2_STATE_ID))
			{
				bAllLvdDisconnect = FALSE;
				break;
			}
		}
	}
	//Add LVD3 disconnect information, because LVD3 is a standalone LVD3, just do it as below.
	if(MB_IsLvd3Valid())
	{
		//printf("Go into MB_IsLvd3Valid\n");
		bLvdValid = TRUE;
		if(MB_SIG_NORMAL
				== MB_GetEnumSigValue(3510, 
						SIG_TYPE_SAMPLING,
						MB_LVD1_STATE_ID))
		{
			//printf("bAllLvdDisconnect \n");
			bAllLvdDisconnect = FALSE;			
		}
	}
	
	if(!bLvdValid)
	{
		bAllLvdDisconnect = FALSE;
	}
	//printf("MB_IsAllLvdDisconnect():bLvdValid = %d, bAllLvdDisconnect = %d~~~\n", bLvdValid, bAllLvdDisconnect);
	return bAllLvdDisconnect;
}

static BOOL MB_IsLvd3Valid(void)
{
	char			strOut[256];
	
	SIG_BASIC_VALUE*	pSigValue;
	int			iRst;
	int			iBufLen;
	int			iEquipId;
	int			iSetLvdEnableId;

	iEquipId = 3510;//LVD 3 equipment ID

	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipId,
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, EQUIP_EXIST_ID),
		&iBufLen,
		&pSigValue,
		0);

	if(iRst != ERR_OK)
	{
		return FALSE;
	}

	if(pSigValue->varValue.enumValue == MB_LVD_NOT_EXIST)
	{
		return FALSE;
	}

	//Check if the lvd is enabled
	//If disabled, return FALSE
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipId,
	    DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, LVD1_ENABLE_ID),
	    &iBufLen,
	    &pSigValue,
	    0);	

	if(iRst != ERR_OK || pSigValue->varValue.enumValue == MB_LVD_DISABLE)
	{
		return FALSE;
	}	
	
	return TRUE;
}
/*==========================================================================*
 * FUNCTION : MB_NeedClearBusVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-01-16 11:56
 *==========================================================================*/
static BOOL MB_NeedClearBusVolt(float fBusVolt)
{
	float	fClearVoltBenchmark;
	float	fRatedVolt = MB_GetFloatSigValue(MB_RECT_GROUP_EQUIP_ID, 
											SIG_TYPE_SAMPLING,
									MB_RATED_VOLT_SIG_ID);
	
	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"MainBoard Get SysVolt Level");

	fClearVoltBenchmark 
		= ((1 == stVoltLevelState)) ? (MB_CLEAR_VOLT_BENCHMARK / 2) : MB_CLEAR_VOLT_BENCHMARK;

	if(MB_IsAllLvdDisconnect())
	{
		if(MB_SIG_NORMAL
			!= MB_GetEnumSigValue(MB_RECT_AC_EQUIP_ID, 
									SIG_TYPE_ALARM,
									MB_MAINS_FAIL_ID))
		{
			return  TRUE;
		}

		//if(g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue < fClearVoltBenchmark)
		if(fBusVolt < fClearVoltBenchmark)
		{
			return TRUE;
		}
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : MB_NeedClearBattVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iBattVoltIdx : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:55
 *==========================================================================*/
static BOOL MB_NeedClearBattVolt(int iBattVoltIdx)
{
	float	fClearVoltBenchmark;
	float	fRatedVolt = MB_GetFloatSigValue(MB_RECT_GROUP_EQUIP_ID, 
											SIG_TYPE_SAMPLING,
											MB_RATED_VOLT_SIG_ID);
	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"Mainboard Get SysVolt Level");
	fClearVoltBenchmark 
		= (1 == stVoltLevelState) ? (MB_CLEAR_VOLT_BENCHMARK / 2) : MB_CLEAR_VOLT_BENCHMARK;

	if(MB_IsAllLvdDisconnect())
	{
		if(g_SiteInfo.MbRoughDataShareForI2C[iBattVoltIdx].fValue < fClearVoltBenchmark)
		{
			return TRUE;
		}
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : MB_VoltageHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-01-16 14:01
 *==========================================================================*/
static void MB_VoltageHandle(void)
{
	//float	fRatedVolt = MB_GetFloatSigValue(MB_RECT_GROUP_EQUIP_ID, 
	//										SIG_TYPE_SAMPLING,
	//										MB_RATED_VOLT_SIG_ID);
	
	int	iBattNum = MB_GetDwordSigValue(MB_BATT_GROUP_EQUIP_ID,
											SIG_TYPE_SETTING,
											MB_BATT_NUM_ID);
	int	iNAType = MB_GetDwordSigValue(MB_BATT_GROUP_EQUIP_ID,
								SIG_TYPE_SETTING,
								MB_NA_TYPE_ID);
	//20130806LKF ĸ�ŵ�ѹ�Ƶ�����MainBoard_FilterTask()�д���
	//g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue =
	//		ABS(g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue);

	
	if(iNAType == MB_IS_NA)
	{
		float	fVolt;
/*����MiniNCU��ֻ��һ·��ز���������ڱ���ģʽ�µĵ�ز�������
�����ã���Ϊ����ģʽ�µĵ�ص�ѹΪ������ز�����֮�
��ôminiNCU�ڱ���ģʽ�µĵ�ص�ѹΪ��һ·��ص�ѹ�ɼ����ֵ��
����ֻ��һ�����*/
#ifndef _CODE_FOR_MINI				
		//Battery voltage = batt1 volt - batt2 volt
		fVolt = g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue
				- g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].fValue;
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue = ABS(fVolt);
#endif
	}
	else
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue =
				ABS(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue);
				
#ifndef _CODE_FOR_MINI			
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].fValue =
				ABS(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].fValue);
#endif
	}

	//20130806LKF ĸ�ŵ�ѹ�Ƶ�����MainBoard_FilterTask()�д���
	//if(g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue < 0.5)
	//{
	//	g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue = 0.0;
	//}

	if(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue < 0.5)
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue = 0.0;
	}

#ifndef _CODE_FOR_MINI	
	if(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].fValue < 0.5)
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].fValue = 0.0;
	}
#endif

	//20130806LKF ĸ�ŵ�ѹ�Ƶ�����MainBoard_FilterTask()�д���
	//if(MB_NeedClearBusVolt())
	//{
	//	g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue = 0.0;
	//}
	
	
	if(MB_NeedClearBattVolt(MB_BATT1_VOLT))
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue = 0.0;
	}
	
#ifndef _CODE_FOR_MINI	
	if(MB_NeedClearBattVolt(MB_BATT2_VOLT))
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].fValue = 0.0;
	}
#endif
	
	return;
}


static void MB_NonBatteryHandle(void)
{


	float	fDelta = 0.0;

	int	iNAType = MB_GetDwordSigValue(MB_BATT_GROUP_EQUIP_ID,
											SIG_TYPE_SETTING,
											MB_IS_CALCULATE_CURRENT_ID);
	int	iLoadShunt = MB_GetEnumSigValue(MB_DCDISTRIBUTION_EQUIP_ID,
								SIG_TYPE_SETTING,
								MB_DCD_LOAD_SHUNT_NUMBER_ID);
	int iIsSMDU_EIBMode = MB_GetEnumSigValue(MB_POWER_SYS_EQUIP_ID,
								SIG_TYPE_SETTING,
								452);//added by Jimmy for SMDU-EIB Shunt Setting CR 2013.07.16
	float	fESNAConverterCurrent = 0.0, fESNAConverterEfficeny = 0.0;

	float fLoad = 0.0;

		float fRect = MB_GetFloatSigValue(MB_RECT_GROUP_EQUIP_ID,
								SIG_TYPE_SAMPLING,
								MB_RCT_GROUP_CURRENT_ID);

		float fConverter = MB_GetFloatSigValue(MB_CT_GROUP_EQUIP_ID,
							SIG_TYPE_SAMPLING,
							MB_CT_GROUP_CURRENT_ID);
		float fMPPTCurr = MB_GetFloatSigValue(MB_MPPT_GROUP_EQUIP_ID,
							SIG_TYPE_SAMPLING,
							MB_MPPT_GROUP_CURRENT_ID);
		
		float	fRectAveVolt = MB_GetFloatSigValue(MB_RECT_GROUP_EQUIP_ID,
			SIG_TYPE_SAMPLING,
			MB_RCT_GROUP_VOLT_ID);
			
		BOOL    b485Mode = MB_GetEnumSigValue(1, SIG_TYPE_SETTING, 180);

		float	fAllSlaveRectCurrent = 0;

		if(b485Mode == 1)
		{
			BOOL    bSlave1WorkStatus = MB_GetEnumSigValue(639, SIG_TYPE_SAMPLING, 100);
			BOOL    bSlave2WorkStatus = MB_GetEnumSigValue(640, SIG_TYPE_SAMPLING, 100);
			BOOL    bSlave3WorkStatus = MB_GetEnumSigValue(641, SIG_TYPE_SAMPLING, 100);

			float   fSlave1RectAllCurrent = MB_GetFloatSigValue(639,
				SIG_TYPE_SAMPLING,
				3);
			float   fSlave2RectAllCurrent = MB_GetFloatSigValue(640,
				SIG_TYPE_SAMPLING,
				3);
			float   fSlave3RectAllCurrent = MB_GetFloatSigValue(641,
				SIG_TYPE_SAMPLING,
				3);

			if(bSlave1WorkStatus == 0)
			{
				fAllSlaveRectCurrent = fAllSlaveRectCurrent + fSlave1RectAllCurrent;

			}

			if(bSlave2WorkStatus == 0)
			{
				fAllSlaveRectCurrent = fAllSlaveRectCurrent + fSlave2RectAllCurrent;

			}

			if(bSlave3WorkStatus == 0)
			{
				fAllSlaveRectCurrent = fAllSlaveRectCurrent + fSlave3RectAllCurrent;

			}

		}
		


		/* 2013-5-20 lkf unused variable
		float	fCtAveVolt = MB_GetFloatSigValue(MB_CT_GROUP_EQUIP_ID,
			SIG_TYPE_SAMPLING,
			MB_CT_GROUP_VOLT_ID); */
		if(fRectAveVolt > 35.0)		
		{
			fDelta = 0.5;
		}
		else
		{
			fDelta = 2.0;
		}

		//if(MB_GetESNAConverterStatus())
		//{
		//	fESNAConverterCurrent = MB_GetFloatSigValue(MB_ESNA_CT_GROUP_EQUIP_ID,
		//					SIG_TYPE_SAMPLING,
		//					MB_ESNA_CT_GROUP_CURRENT_ID);
		//	fESNAConverterEfficeny = MB_GetFloatSigValue(MB_ESNA_CT_GROUP_EQUIP_ID,
		//					SIG_TYPE_SETTING,
		//					MB_ESNA_CT_GROUP_EFFICY_ID);
		//}
	if(iNAType && iLoadShunt && (!iIsSMDU_EIBMode))
	{
		fLoad = MB_GetFloatSigValue(MB_DCDISTRIBUTION_EQUIP_ID,
							SIG_TYPE_SAMPLING,
							MB_DCD_LOAD_CURRENT_ID);
		//0.5, Because the ESNAConvert IS 24V output!
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue = fAllSlaveRectCurrent + fRect + fMPPTCurr - fLoad - fConverter * fDelta;// - 0.5 * (fESNAConverterCurrent * fESNAConverterEfficeny/100);
	}
	else if(iNAType && iIsSMDU_EIBMode)
	{
		fLoad = MB_GetFloatSigValue(MB_DCDISTRIBUTION_EQUIP_ID,
							SIG_TYPE_SAMPLING,
							G_TOTAL_LOAD_CURRENT_ID);
		//0.5, Because the ESNAConvert IS 24V output!
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue = fAllSlaveRectCurrent + fRect + fMPPTCurr - fLoad - fConverter * fDelta;
	}

	//Clear to zero.
	if(ABS(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue) < ((0.003) * MB_GetFloatSigValue(MB_DC_EQUIP_ID, SIG_TYPE_SETTING, MB_LOAD_CURR_RANGE_ID)))
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue = 0.0;
	}

	return;
}
#define	COM_VOLT_BENCHMARK	(0.5)
/*==========================================================================*
 * FUNCTION : MB_CurrentHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:45
 *==========================================================================*/
static void MB_CurrentHandle(void)
{
	float	fCurr;
	float	fComVolt;

	fCurr = g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR].fValue;
	fComVolt = g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR_COM_VOLT].fValue;
	/*if((ABS(fComVolt) > COM_VOLT_BENCHMARK)
		|| ((fCurr < 0) 
			&&((fCurr > (-1.0)) 
				|| (fCurr > ((-0.003) 
					* MB_GetFloatSigValue(MB_DC_EQUIP_ID, SIG_TYPE_SETTING, MB_LOAD_CURR_RANGE_ID))))))
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR].fValue = 0.0;	
	}	*/
	
#ifdef _CODE_FOR_MINI
	if( fCurr < (0.003) * MB_GetFloatSigValue(MB_DC_EQUIP_ID, SIG_TYPE_SETTING, MB_LOAD_CURR_RANGE_ID) )	
#else
	if((ABS(fComVolt) > COM_VOLT_BENCHMARK)
		|| (fCurr < (0.003) 
		* MB_GetFloatSigValue(MB_DC_EQUIP_ID, SIG_TYPE_SETTING, MB_LOAD_CURR_RANGE_ID)))	
#endif
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR].fValue = 0.0;	
	}	


	fCurr = g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue;
	fComVolt = g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR_COM_VOLT].fValue;
	
#ifdef _CODE_FOR_MINI
	if(  (ABS(fCurr) < 1.0) 
		|| (ABS(fCurr) < (0.003
				* MB_GetFloatSigValue(MB_BATT1_EQUIP_ID, SIG_TYPE_SETTING, MB_BATT1_CURR_RANGE_ID))))
#else
	if((ABS(fComVolt) > COM_VOLT_BENCHMARK)
		|| ((ABS(fCurr) < 1.0) 
			|| (ABS(fCurr) < (0.003
				* MB_GetFloatSigValue(MB_BATT1_EQUIP_ID, SIG_TYPE_SETTING, MB_BATT1_CURR_RANGE_ID)))))
#endif
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue = 0.0;	
	}

	
	fCurr = g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue;
	fComVolt = g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR_COM_VOLT].fValue;

#ifdef _CODE_FOR_MINI
	if( (ABS(fCurr) < 1.0) 
		|| (ABS(fCurr) < (0.003
				* MB_GetFloatSigValue(MB_BATT2_EQUIP_ID, SIG_TYPE_SETTING, MB_BATT1_CURR_RANGE_ID))))
#else
	if((ABS(fComVolt) > COM_VOLT_BENCHMARK)
		|| ((ABS(fCurr) < 1.0) 
			|| (ABS(fCurr) < (0.003
				* MB_GetFloatSigValue(MB_BATT2_EQUIP_ID, SIG_TYPE_SETTING, MB_BATT1_CURR_RANGE_ID)))))
#endif
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue = 0.0;	
	}

#ifndef _CODE_FOR_MINI
	if(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR_COM_VOLT].fValue > COM_VOLT_BENCHMARK)
	{
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue = 0.0;
	}
#endif

//printf("MB_CurrentHandle():  g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue=%f~~~~\n",g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);

//2018-10-23 for system requirements
#ifdef _CODE_FOR_MINI
	if(MB_IsAllLvdDisconnect())
	{
		//printf("MB_CurrentHandle():  True~~~~~\n");
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue = 0.0;	
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue = 0.0;
	}
#endif
//printf("MB_CurrentHandle()-1:  g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue=%f~~~~\n",g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);


	return;
}


/*==========================================================================*
 * FUNCTION : MB_BattStatusHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:45
 *==========================================================================*/
static void MB_BattStatusHandle(void)
{
	static int siBattNum = 0;  //���ڷ�ֹ�ظ���MB_BATT_NUM_ID���ø�ֵ

	int	iBattNum = MB_GetDwordSigValue(MB_BATT_GROUP_EQUIP_ID,
								SIG_TYPE_SETTING,
								MB_BATT_NUM_ID);

	int	iNAType = MB_GetDwordSigValue(MB_BATT_GROUP_EQUIP_ID,
								SIG_TYPE_SETTING,MB_IS_CALCULATE_CURRENT_ID);

	/* 2013-5-20 lkf unused
	int	iLoadShuntNumber = MB_GetEnumSigValue(MB_DCDISTRIBUTION_EQUIP_ID,
								SIG_TYPE_SETTING,
								MB_DCD_LOAD_SHUNT_NUMBER_ID);	*/						
	if(iNAType) 
	{
		
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_ST].iValue 
			= MB_BATT_EXIST;
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_ST].iValue 
			= MB_BATT_NOT_EXIST;

		MB_SetDwordSigValue(MB_BATT_GROUP_EQUIP_ID,SIG_TYPE_SAMPLING, MB_BATTERY_CHANGE_ID, (DWORD)1);

		siBattNum = iBattNum;
		if(siBattNum != 1)
		{
		    MB_SetEnumSigValue(MB_BATT_GROUP_EQUIP_ID,SIG_TYPE_SETTING, MB_BATT_NUM_ID, (SIG_ENUM)1);
		}
		return;
	}


	switch (iBattNum)
	{
	case	MB_BATT_TYPE_NO_BATT:
		/*Changed by YangGuoxin. 2010-4-20*/
		/*if(iLoadShuntNumber)
		{
			g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_ST].iValue 
				= MB_BATT_EXIST;
			g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_ST].iValue 
				= MB_BATT_NOT_EXIST;
			MB_SetEnumSigValue(MB_BATT_GROUP_EQUIP_ID,SIG_TYPE_SAMPLING, MB_BATTERY_CHANGE_ID, 1);
		}
		else*/
		{

			g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_ST].iValue 
				= MB_BATT_NOT_EXIST;
			g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_ST].iValue 
				= MB_BATT_NOT_EXIST;
			MB_SetEnumSigValue(MB_BATT_GROUP_EQUIP_ID,SIG_TYPE_SAMPLING, MB_BATTERY_CHANGE_ID, 0);
		}
		break;
	case	MB_BATT_TYPE_1_BATT:
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_ST].iValue 
			= MB_BATT_EXIST;
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_ST].iValue 
			= MB_BATT_NOT_EXIST;
		MB_SetEnumSigValue(MB_BATT_GROUP_EQUIP_ID,SIG_TYPE_SAMPLING, MB_BATTERY_CHANGE_ID, 1);
		break;
	case	MB_BATT_TYPE_2_BATT:

		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_ST].iValue 
			= MB_BATT_EXIST;
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_ST].iValue 
			= MB_BATT_EXIST;
		MB_SetEnumSigValue(MB_BATT_GROUP_EQUIP_ID,SIG_TYPE_SAMPLING, MB_BATTERY_CHANGE_ID, 2);
		break;
	//case	MB_BATT_TYPE_SPECIAL:
	//	g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_ST].iValue 
	//		= MB_BATT_EXIST;
	//	g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_ST].iValue 
	//		= MB_BATT_NOT_EXIST;
		
		break;
	default:
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_ST].iValue 
			= MB_BATT_EXIST;
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_ST].iValue 
			= MB_BATT_EXIST;
		MB_SetEnumSigValue(MB_BATT_GROUP_EQUIP_ID,SIG_TYPE_SAMPLING, MB_BATTERY_CHANGE_ID, 2);
		break;
	}
	return;
}

/*==========================================================================*
* FUNCTION : BattCurrentFilter
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
       i (3 Current Sample Channel): 0-Load 1-Batt1 Curr 2-Batt2
       s_iMagicCount : The Sample Counter
       fCurrShuntCoeff : shunt current coneff
	*pfCurrSampleValue : the sampled value
* RETURN   : 
       *pfCurrSampleValue : the value after filter
* COMMENTS : 
* CREATOR  : Kane Lian               DATE: 2014-03-14 10:52
*==========================================================================*/

//��ص���ħ�����ֹ��˴���(���ڸ��ص�������ص���)
#define   MAGIC_COUNT_MAX_BATTCURR      10         
//�����βɼ�����ԭ����ֵ�仯���ȳ����趨��Χʱ,��Ҫ��ȷ�ϴ���,���ҷ�2������
//��1������:�仯���ȳ���������*BATTCURR_CHANGE_COEFF,����BATTCURR_CHANGE_CONFIR_CNTȷ�ϴ�����,�ò���ֵ����ħ����˴���
#define	  BATTCURR_CHANGE_COEFF		0.005	    //����������һ�α仯����������*��ϵ��ʱ���Ҿ���ȷ�ϴ����������ħ����˴���
#define	  BATTCURR_CHANGE_MIN_VALUE	2	    //��ͱ仯���Ⱦ���ֵ(������*BATTCURR_CHANGE_COEFF ���ܵ��ڱ�ֵ)
#define	  BATTCURR_CHANGE_CONFIR_CNT	4	    //���仯���ȳ��������趨��Χ���������ﵽ���趨��������ò���ֵ����ħ����˴���
//��1������:�仯���ȳ���������*BATTCURR_CHANGE_COEFF_2,����BATTCURR_CHANGE_CONFIR_CNT_2ȷ�ϴ�����,�ò���ֱֵ����Ч
#define	  BATTCURR_CHANGE_COEFF_2	0.01	    //����������һ�α仯����������*��ϵ��ʱ���Ҿ���ȷ�ϴ�������������Ч
#define	  BATTCURR_CHANGE_MIN_VALUE_2	5	    //��ͱ仯���Ⱦ���ֵ(������*BATTCURR_CHANGE_COEFF_2 ���ܵ��ڱ�ֵ)
#define	  BATTCURR_CHANGE_CONFIR_CNT_2	5	    //���仯���ȳ��������趨��Χ���������ﵽ���趨��������ò���ֵ������Ч

#define	  _BattCurrentFilter_Print_Enable   0	    //��ӡ���Կ���

 static void BattCurrentFilter(int i, int s_iMagicCount, float fCurrShuntCoeff, float *pfCurrSampleValue)
{
    static float s_f32MagicSum_MB_BattCurr[3] = {0};         //ħ�����ִ������� - ���帺�ص�������ص���1����ص���2
    static float s_f32MagicAvg_MB_BattCurr[3] = {0};         //ħ�����ִ������� - ���帺�ص�������ص���1����ص���2

    static int  s_iChangeOverRange1[3] = {0};		 //����һ�βɼ��ģ����帺�ص�������ص���1����ص���2���仯����1���趨��Χʱ����Ҫȷ��
    static int  s_iChangeOverRange2[3] = {0};		 //����һ�βɼ��ģ����帺�ص�������ص���1����ص���2���仯����2���趨��Χʱ��Ҳ��Ҫȷ��

    float fDelta, fChangeRange1, fChangeRange2;

#if _BattCurrentFilter_Print_Enable
    //test
    static float f32test[6]= {0};
    //test end
#endif

    if(i > 2)
    {
	return;
    }

    //���㱾�β�����ԭ����ֵ�仯��Χ
    fDelta = *pfCurrSampleValue - s_f32MagicAvg_MB_BattCurr[i];
    if(fDelta < 0)
    {
	fDelta = -fDelta;
    }

#if _BattCurrentFilter_Print_Enable
//test
    if(i == 2)
    {
	if(fDelta > 12)
	{
	    f32test[0] = 10000;
	    f32test[1] = -1;    
	    f32test[2] = 10000;
	    f32test[3] = -1;   
	    f32test[4] = 0;
	    f32test[5] = 0;
	}

	if(*pfCurrSampleValue > f32test[1])
	{
	    f32test[1] = *pfCurrSampleValue;
	}
	if(*pfCurrSampleValue < f32test[0])
	{
	    f32test[0] = *pfCurrSampleValue;
	}

	f32test[4] = f32test[1]- f32test[0];
    }
//test end
#endif

    //�����βɼ�����ԭ����ֵ�仯���ȳ����趨��Χʱ,��Ҫ��ȷ�ϴ���,���ҷ�2������
    //���仯����������*BATTCURR_CHANGE_COEFF_2ʱ,ֱ�Ӹ�ֵ,�������ħ�����ִ���
    fChangeRange1 = fCurrShuntCoeff * BATTCURR_CHANGE_COEFF;	//��1�����ȷ�Χ			
    if(fChangeRange1 < BATTCURR_CHANGE_MIN_VALUE)
    {
	fChangeRange1 = BATTCURR_CHANGE_MIN_VALUE;
    }

    fChangeRange2 = fCurrShuntCoeff * BATTCURR_CHANGE_COEFF_2; //��2�����ȷ�Χ	
    if(fChangeRange2 < BATTCURR_CHANGE_MIN_VALUE_2)
    {
	fChangeRange2 = BATTCURR_CHANGE_MIN_VALUE_2;
    }
    //������2���趨��Χ��ȷ�Ϻ�ֱ�Ӹ�ֵ
    if(fDelta > fChangeRange2)  
    {	
	s_iChangeOverRange2[i]++;
	s_iChangeOverRange1[i]++;
	if(s_iChangeOverRange2[i] >= BATTCURR_CHANGE_CONFIR_CNT_2)  //ȷ�ϴ���
	{
	    if(s_iMagicCount < MAGIC_COUNT_MAX_BATTCURR)
	    {
		s_f32MagicSum_MB_BattCurr[i] = (s_iMagicCount+1) * (*pfCurrSampleValue);
	    }
	    else
	    {
		s_f32MagicSum_MB_BattCurr[i] = MAGIC_COUNT_MAX_BATTCURR * (*pfCurrSampleValue);
	    }		
	    s_f32MagicAvg_MB_BattCurr[i] = (*pfCurrSampleValue);
	    s_iChangeOverRange2[i] = 0;
	    s_iChangeOverRange1[i] = 0;
	}	
    }   
    //���򣬽���ħ�����ִ���
    else
    {	
	s_iChangeOverRange2[i] = 0;
	//������1���趨��Χ��ȷ�Ϻ�Ž���ħ�����ִ���
	if(fDelta > fChangeRange1)  
	{
	    s_iChangeOverRange1[i]++;
	    if(s_iChangeOverRange1[i] >= BATTCURR_CHANGE_CONFIR_CNT)  //ȷ�ϴ���
	    {
		s_iChangeOverRange1[i] = 0;
	    }
	}
	//����,ֱ�ӽ���ħ�����ִ���
	else
	{
	    s_iChangeOverRange1[i] = 0;
	}

	//ħ�����ֹ���
	if(s_iChangeOverRange1[i] == 0)
	{
	    s_f32MagicSum_MB_BattCurr[i] += (*pfCurrSampleValue);
	    if(s_iMagicCount < MAGIC_COUNT_MAX_BATTCURR)
	    {
		s_f32MagicAvg_MB_BattCurr[i] = s_f32MagicSum_MB_BattCurr[i] / (s_iMagicCount+1);        
	    }    
	    else
	    {
		s_f32MagicSum_MB_BattCurr[i] -= s_f32MagicAvg_MB_BattCurr[i];
		s_f32MagicAvg_MB_BattCurr[i] = s_f32MagicSum_MB_BattCurr[i] / MAGIC_COUNT_MAX_BATTCURR;   
	    }		
	}
    }

#if _BattCurrentFilter_Print_Enable
    ////test
    if(i == 2)
    {
	if(s_f32MagicAvg_MB_BattCurr[i] > f32test[3])
	{
	    f32test[3] = s_f32MagicAvg_MB_BattCurr[i];
	}
	if(s_f32MagicAvg_MB_BattCurr[i] < f32test[2])
	{
	    f32test[2] = s_f32MagicAvg_MB_BattCurr[i];
	}

	f32test[5] = f32test[3]- f32test[2];

	printf("\n CURRRENT%d : %.2f - Max: %.2f  Min: %.2f Delta=%.2f    After Filter:  %.2f - Max: %.2f  Min: %.2f  Delta=%.2f\n",i, *pfCurrSampleValue, f32test[0], f32test[1], f32test[4],s_f32MagicAvg_MB_BattCurr[i], f32test[2], f32test[3],  f32test[5]);
	}
    //test end
#endif

    *pfCurrSampleValue = s_f32MagicAvg_MB_BattCurr[i];
}

/*==========================================================================*
 * FUNCTION : MB_Sample
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-29 10:52
 *==========================================================================*/
#define _DEFINED_T08_OLD_BOARD   0   //1-�ɰ�  0-�°�

#if _DEFINED_T08_OLD_BOARD  

//* ������T08֮ǰ�ľɰ�
#define MB_BENCHMARK_MILLI_VOLT		(75.0)  //���°�������Ϊ50

//#define MB_BUS_VOLT_RATE		(25*2)
#define MB_BUS_VOLT_RATE		(21*2)  //��ȱ10K���裬��ѹ��������12K

#define MB_BATT_VOLT_RATE		(26*2)
#define	MB_TEMERATURE_RATE		(2)
#define MB_CURR_COMM_VOLT_RATE		(26*2)

#define MB_LOAD_FUSE_RATE		(26*2)
#define	MB_BATT_FUSE_RATE		(26*2)

#define OB_LOAD_FUSE_RATE		(26*2.5)  //���°�������Ϊ*2
#define	OB_BATT_FUSE_RATE		(16*2.5)  //���°�������Ϊ*2
#define	OB_TEMERATURE_RATE		(2.5)     //���°�������Ϊ*2
#define	OB_FUEL_SENSOR_RATE		(2.5)     //���°�������Ϊ*2
#define	OB_LVD_AUX_RATE			(26*2.5)  //���°�������Ϊ*2

#define	OB_FUEL_RESISTOR_VAL		150   

#else   //�°�

//������T09���Ժ�ĵ���
#define MB_BENCHMARK_MILLI_VOLT		(50.0)
#define MB_BUS_VOLT_RATE		(25*2)
#define MB_BATT_VOLT_RATE		(26*2)
#define	MB_TEMERATURE_RATE		(2)
#define MB_CURR_COMM_VOLT_RATE		(26*2)
#define MB_LOAD_FUSE_RATE		(26*2)

#ifdef _CODE_FOR_MINI
#define	MB_BATT_FUSE_RATE		(26*2)	//for mini controller it should be 26*2
#else
#define	MB_BATT_FUSE_RATE		(16*2)
#endif

#define OB_LOAD_FUSE_RATE		(26*2) //���°�������Ϊ*2
#define	OB_BATT_FUSE_RATE		(16*2)
#define	OB_TEMERATURE_RATE		(2)
#define	OB_FUEL_SENSOR_RATE		(2)
#define	OB_LVD_AUX_RATE			(26*2)

#define	OB_FUEL_RESISTOR_VAL		120   //OB����λ���׼��ѹ��3V����Ϊ2.5V,�������̷�Χ�ﲻ��20mA������С��������


#endif  //END: #if _DEFINED_T08_OLD_BOARD 

#define IB01_RETURN_BYTE_NUM		1   //IB01���������ֽ���


/*�����ź���Դ��ȡ���е�����źŸ���*/
#define MAX_MB(a, b) (((a) > (b)) ? (a) : (b))
#define MB_SIZE_MAX  MAX_MB(SIZE_OF_OB_CHN, SIZE_OF_MB_ANALOG_CHN)   //ȡ����źŸ���

union unMainBoardSanmpleValue
{
    BYTE	abyReadBuf[MB_SIZE_MAX * 2];
    WORD	awRoughData[MB_SIZE_MAX];	

    BYTE	byDI;
    BYTE	byUnused1;
    BYTE	byUnused2;
    BYTE	byUnused3;
};
typedef union unMainBoardSanmpleValue MB_SAMPLE_VALUE;





static void MB_Sample(void)
{
	//static int			g_SiteInfo.iMbHandle = -1;
	int i;

	MB_SAMPLE_VALUE		unMbSampVal;
	float	ftemp, fShuntCurrRange, fTemperatureTemp;

	// Lin.Jinye.Jean : Add 2017-10-11 10:45:05  magic Logic handler temperature  
	static float  s_fOldTemperature1Val =-273.0;
	static float  s_fOldTemperature1Sum =0.0;
	static float  s_fOldTemperature2Val =-273.0;
	static float  s_fOldTemperature2Sum =0.0 ;
	static float  s_fOldTemperature3Val =-273.0 ;
	static float  s_fOldTemperature3Sum =0.0 ;


	HANDLE hLocalThread;

	SAMPLING_VALUE Digit_1,Digit_0;

	static int s_iMagicCount = 0;
	
#ifdef _CODE_FOR_MINI	
	static float s_f32MagicSum_MB[4] = {0};         //ħ�����ִ������� - ��������˿
	static float s_f32MagicAvg_MB[4] = {0};         //ħ�����ִ������� - ��������˿	
#else	
	static float s_f32MagicSum_MB[2] = {0};         //ħ�����ִ������� - ��������˿
	static float s_f32MagicAvg_MB[2] = {0};         //ħ�����ִ������� - ��������˿
#endif

	static float s_f32MagicSum_OB[4] = {0};         //ħ�����ִ������� - OB������˿
	static float s_f32MagicAvg_OB[4] = {0};         //ħ�����ִ������� - OB������˿

	static int s_iMagicCount_BattCurr = 0;
	
	hLocalThread = RunThread_GetId(NULL);

	

#if	_MAIN_BOARD_DEBUG

  printf(" MB_DC_VOLT = %f V \n", g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue );

#endif
	//printf("\n /dev/imx_adc_sampleri2c = %d ", g_SiteInfo.iOBHandle);
	    

	ZERO_POBJS(unMbSampVal.abyReadBuf, MB_SIZE_MAX * 2);

	Digit_1.iValue	= 1;
	Digit_0.iValue  = 0;

	if(g_SiteInfo.iMbHandle <= 0)
	{
		g_SiteInfo.iMbHandle = open(MB_DEVICE_NAME, O_RDWR);
		if(g_SiteInfo.iMbHandle <= 0)
		{
			//MB_LOG_OUT(APP_LOG_ERROR, "Main board driver can not be opened!\n");
			return;
		}
	}
    
	
	/* Step 1 : Main board */
	if(read(g_SiteInfo.iMbHandle, unMbSampVal.abyReadBuf, SIZE_OF_MB_ANALOG_CHN * 2)
			== (SIZE_OF_MB_ANALOG_CHN * 2))
	{

		//The following arithmetic is according to "Definition of Driver Interface in NGMC - HF"

		fShuntCurrRange = MB_GetFloatSigValue(MB_DC_EQUIP_ID, SIG_TYPE_SETTING, MB_LOAD_CURR_RANGE_ID);
		g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR].fValue 
			= (float)MB_BENCHMARK_MILLI_VOLT
				* (float)(unMbSampVal.awRoughData[MB_IDX_LOAD_CURR] 
				- unMbSampVal.awRoughData[MB_IDX_I_GND]) 
				* fShuntCurrRange
				/ (MB_GetFloatSigValue(MB_DC_EQUIP_ID, SIG_TYPE_SETTING, MB_LOAD_CURR_MV_ID)
				* (unMbSampVal.awRoughData[MB_IDX_CURR_REF] - unMbSampVal.awRoughData[MB_IDX_I_GND])) 
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_CURR];
		
		BattCurrentFilter(0, s_iMagicCount_BattCurr, fShuntCurrRange, &(g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR].fValue));

#if	_MAIN_BOARD_DEBUG		
		printf("MB: MB_LOAD_CURR: %d, %.02f\n", 
				unMbSampVal.awRoughData[MB_IDX_LOAD_CURR], 
				g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR].fValue);
#endif

		fShuntCurrRange = MB_GetFloatSigValue(MB_BATT1_EQUIP_ID, SIG_TYPE_SETTING, MB_BATT1_CURR_RANGE_ID);
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue 
			= (float)MB_BENCHMARK_MILLI_VOLT
				* (float)(unMbSampVal.awRoughData[MB_IDX_BATT1_CURR] 
				- unMbSampVal.awRoughData[MB_IDX_I_GND]) 
				* fShuntCurrRange
				/ (MB_GetFloatSigValue(MB_BATT1_EQUIP_ID, SIG_TYPE_SETTING, MB_BATT1_CURR_MV_ID)
				* (unMbSampVal.awRoughData[MB_IDX_CURR_REF] - unMbSampVal.awRoughData[MB_IDX_I_GND]))
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_CURR];
				

		BattCurrentFilter(1, s_iMagicCount_BattCurr, fShuntCurrRange, &(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue));
		g_SiteInfo.MbRoughDataShareForI2C[ABS_BATT_CURR1].fValue = ABS(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);

#if	_MAIN_BOARD_DEBUG
		printf("MB: MB_BATT1_CURR: %d, %.02f\n", 
				unMbSampVal.awRoughData[MB_IDX_BATT1_CURR], 
				g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);
#endif
		fShuntCurrRange = MB_GetFloatSigValue(MB_BATT2_EQUIP_ID, SIG_TYPE_SETTING, MB_BATT2_CURR_RANGE_ID);
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue 
			=  (float)MB_BENCHMARK_MILLI_VOLT
				* (float)(unMbSampVal.awRoughData[MB_IDX_BATT2_CURR] 
				- unMbSampVal.awRoughData[MB_IDX_I_GND]) 
				* fShuntCurrRange
				/ (MB_GetFloatSigValue(MB_BATT2_EQUIP_ID, SIG_TYPE_SETTING, MB_BATT2_CURR_MV_ID)
				* (unMbSampVal.awRoughData[MB_IDX_CURR_REF] - unMbSampVal.awRoughData[MB_IDX_I_GND]))
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_CURR];


		BattCurrentFilter(2, s_iMagicCount_BattCurr, fShuntCurrRange, &(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue));
		g_SiteInfo.MbRoughDataShareForI2C[ABS_BATT_CURR2].fValue = ABS(g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue);

#if	_MAIN_BOARD_DEBUG1
		printf("MB: MB_BATT2_CURR: %d, %.02f\n", 
				unMbSampVal.awRoughData[MB_IDX_BATT2_CURR], 
				g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue);
#endif
		/* 20130806LKF ĸ�ŵ�ѹ�Ƶ�����MainBoard_FilterTask()�д���
		g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue 
			= MB_BUS_VOLT_RATE
				* (unMbSampVal.awRoughData[MB_IDX_DC_VOLT] 
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afMbAdjustCoef[MB_ADJUST_COEF_BUS_VOLT]
				/ (unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT]
				- unMbSampVal.awRoughData[MB_IDX_GND]);
#if	_MAIN_BOARD_DEBUG
		printf("MB: MB_DC_VOLT: %d, %.02f\n", 
				unMbSampVal.awRoughData[MB_IDX_DC_VOLT], 
				g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue);
#endif
		*/		    

		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue 
			= (float)MB_BATT_VOLT_RATE
				* (float)(unMbSampVal.awRoughData[MB_IDX_BATT1_VOLT] 
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afMbAdjustCoef[MB_ADJUST_COEF_BATT_VOLT1]
				/ (float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT];
#if	_MAIN_BOARD_DEBUG
		printf("MB: MB_BATT1_VOLT: %d, %.02f\n\n", 
				unMbSampVal.awRoughData[MB_IDX_BATT1_VOLT], 
				g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_VOLT].fValue);
#endif

#ifdef _CODE_FOR_MINI
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].iValue = -9999;
#else
		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].fValue 
			=  (float)MB_BATT_VOLT_RATE
				* (float)(unMbSampVal.awRoughData[MB_IDX_BATT2_VOLT] 
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afMbAdjustCoef[MB_ADJUST_COEF_BATT_VOLT2]
				/ (float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT];
#if	_MAIN_BOARD_DEBUG
				printf("MB: MB_BATT2_VOLT: %d, %.02f\n\n", 
				    unMbSampVal.awRoughData[MB_IDX_BATT2_VOLT], 
				    g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_VOLT].fValue);
#endif
#endif

		//g_SiteInfo.MbRoughDataShareForI2C[MB_TEMPERATURE1].fValue 
		fTemperatureTemp = ((float)(unMbSampVal.awRoughData[MB_IDX_TEMPERATURE1] 
				- unMbSampVal.awRoughData[MB_IDX_GND]) 
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT]
				/ (float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF] 
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* MB_TEMERATURE_RATE - 1.3923)
				* s_afMbAdjustCoef[MB_ADJUST_COEF_TEMP1] 
				* 1000 / 5.1;
#if	_MAIN_BOARD_DEBUG
				printf("MB: MB_TEMPERATURE1: %d, %.02f\n\n", 
				    unMbSampVal.awRoughData[MB_IDX_TEMPERATURE1], 
				    g_SiteInfo.MbRoughDataShareForI2C[MB_TEMPERATURE1].fValue);
#endif

		TmepSmoothingAlgorithm(&s_fOldTemperature1Val,&fTemperatureTemp,&s_fOldTemperature1Sum,0);

		//printf("mainBoardTemp-1: fTemperatureTemp=%f\n",fTemperatureTemp);
		
		g_SiteInfo.MbRoughDataShareForI2C[MB_TEMPERATURE1].fValue = fTemperatureTemp;
	
		//g_SiteInfo.MbRoughDataShareForI2C[MB_TEMPERATURE2].fValue 
		fTemperatureTemp = (((float)(unMbSampVal.awRoughData[MB_IDX_TEMPERATURE2] 
				- unMbSampVal.awRoughData[MB_IDX_GND]) 
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT]
				/ (float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]
				- unMbSampVal.awRoughData[MB_IDX_GND]))
				* MB_TEMERATURE_RATE - 1.3923) 
				* s_afMbAdjustCoef[MB_ADJUST_COEF_TEMP2] 
				* 1000 / 5.1;
#if	_MAIN_BOARD_DEBUG
				printf("MB: MB_TEMPERATURE2: %d, %.02f\n\n", 
				    unMbSampVal.awRoughData[MB_IDX_TEMPERATURE2], 
				    g_SiteInfo.MbRoughDataShareForI2C[MB_TEMPERATURE2].fValue);
#endif

		TmepSmoothingAlgorithm(&s_fOldTemperature2Val,&fTemperatureTemp,&s_fOldTemperature2Sum,1);
		
		g_SiteInfo.MbRoughDataShareForI2C[MB_TEMPERATURE2].fValue = fTemperatureTemp;

#ifndef _CODE_FOR_MINI
		g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR_COM_VOLT].fValue
			= (float)MB_CURR_COMM_VOLT_RATE * (float)(unMbSampVal.awRoughData[MB_IDX_LOAD_CURR_COM_VOLT] 
				- unMbSampVal.awRoughData[MB_IDX_GND]) 
				/ (float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]				
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT];

#if	_MAIN_BOARD_DEBUG
				printf("MB: MB_LOAD_CURR_COM_VOLT: %d, %.02f\n\n", 
				    unMbSampVal.awRoughData[MB_IDX_LOAD_CURR_COM_VOLT], 
				    g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR_COM_VOLT].fValue);
#endif

		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR_COM_VOLT].fValue
			= (float)MB_CURR_COMM_VOLT_RATE * (float)(unMbSampVal.awRoughData[MB_IDX_BATT1_CURR_COM_VOLT] 
				- unMbSampVal.awRoughData[MB_IDX_GND]) 
				/ (float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT];

		g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR_COM_VOLT].fValue
			= (float)MB_CURR_COMM_VOLT_RATE * (float)(unMbSampVal.awRoughData[MB_IDX_BATT2_CURR_COM_VOLT] 
				- unMbSampVal.awRoughData[MB_IDX_GND]) 
				/ (float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF] 
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT];
#endif


//	printf("MbSample: g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue=%f~~~\n ",g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);
//	printf("MbSample: g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue=%f~~~\n ",g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);
//	printf("MbSample: g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR].fValue=%f~~~\n ",g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);


			
#ifdef _CODE_FOR_MINI
				//LOAD FUSE, number reduce to 1 for Mini controller
				for(i = 0; i < 1; i++)
#else
				//LOAD FUSE
				for(i = 0; i < 2; i++)
#endif
				{					
					ftemp = (float)MB_LOAD_FUSE_RATE 
						* (float)(unMbSampVal.awRoughData[MB_IDX_LOADFUSE1 + i] - unMbSampVal.awRoughData[MB_IDX_GND])
						//* s_afMbAdjustCoef[MB_ADJUST_COEF_LOADFUSE1 + i] 
					/	 
						(float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]
					- unMbSampVal.awRoughData[MB_IDX_GND])
					* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT];

					if(ftemp < 0)
					{
					    ftemp = -ftemp;
					}
					if(ftemp > LOAD_FUSE_ALARM_THRESHOLD)
					{
					    g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_FUSE1_ST+i].fValue = Digit_1.fValue;
					}
					else if(ftemp < LOAD_FUSE_ALARM_THRESHOLD - LOAD_FUSE_ALARM_THRESHOLD_DELTA)
					{
					    g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_FUSE1_ST+i].fValue = Digit_0.fValue;
					}
				} 
#if	_MAIN_BOARD_DEBUG
				printf("MB: MB_LOAD_FUSE1_ST: %d, %.02f\n\n", 
				    unMbSampVal.awRoughData[MB_IDX_LOADFUSE1], 
				    g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_FUSE1_ST].fValue);
#endif

#ifdef _CODE_FOR_MINI
				//BATT FUSE number increase to 4 for mini controller
				for(i = 0; i < 4; i++)
#else
				//BATT FUSE
				for(i = 0; i < 2; i++)
#endif
				{
					ftemp = (float)MB_BATT_FUSE_RATE 
						* (float)(unMbSampVal.awRoughData[MB_IDX_BATTFUSE1 + i] - unMbSampVal.awRoughData[MB_IDX_GND])
						//* s_afMbAdjustCoef[MB_ADJUST_COEF_BATTFUSE1 + i]
					/	 
						(float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]
					- unMbSampVal.awRoughData[MB_IDX_GND])
					* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT];

					//ħ�����ִ���
					s_f32MagicSum_MB[i] += ftemp;
					if(s_iMagicCount < MAGIC_COUNT_MAX)
					{
					    s_f32MagicAvg_MB[i] = s_f32MagicSum_MB[i] / (s_iMagicCount+1);        
					}    
					else
					{
					    s_f32MagicSum_MB[i] -= s_f32MagicAvg_MB[i];
					    s_f32MagicAvg_MB[i] = s_f32MagicSum_MB[i] / MAGIC_COUNT_MAX;   
					}

					//printf("\nMB: MB_BATT_FUSE%d : %f vs %f \n", i, ftemp, s_f32MagicAvg_MB[i]);


					ftemp = s_f32MagicAvg_MB[i];

					if(ftemp < 0)
					{
					    ftemp = -ftemp;
					}
					if(ftemp > BATT_FUSE_ALARM_THRESHOLD)
					{
					    g_SiteInfo.MbRoughDataShareForI2C[MB_BATT_FUSE1_ST+i].fValue = Digit_1.fValue;
					}
					else if(ftemp < BATT_FUSE_ALARM_THRESHOLD - BATT_FUSE_ALARM_THRESHOLD_DELTA)
					{
					    g_SiteInfo.MbRoughDataShareForI2C[MB_BATT_FUSE1_ST+i].fValue = Digit_0.fValue;
					}
#if	_MAIN_BOARD_DEBUG
					printf("\n\nMB: MB_BATT_FUSE%d_ST: %d, %.02f  MB_IDX_DC_VOLT_REF=%d, MB_IDX_GND=%d\n", i,
					    unMbSampVal.awRoughData[MB_IDX_BATTFUSE1+i], 
					    ftemp, unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF], unMbSampVal.awRoughData[MB_IDX_GND]);
#endif
				}

#ifdef _CODE_FOR_MINI
			//LVD aux 
			//mini controller transfer this function from OB to MB
			g_SiteInfo.MbRoughDataShareForI2C[OB_EXIST_ST].iValue = 0;   //����?
			for(i = 0; i < 2; i++)
			{				
				ftemp
					= (float)OB_LVD_AUX_RATE 
					* (float)(unMbSampVal.awRoughData[MB_IDX_LVD1_ST + i] - unMbSampVal.awRoughData[MB_IDX_GND])
					* s_afOBAdjustCoef[OB_ADJUST_COEF_LVD_AUX1 + i]
				/	 
					(float)(unMbSampVal.awRoughData[MB_IDX_DC_VOLT_REF]
				- unMbSampVal.awRoughData[MB_IDX_GND])
				* s_afOBAdjustCoef[OB_ADJUST_COEF_REF_VOLT];

#if	_MAIN_BOARD_DEBUG
				printf("MB: OB_LVD_AUX1_ST: %d, %.02f\n\n", 
				    unMbSampVal.awRoughData[MB_IDX_LVD1_ST + i], 
				    ftemp);
#endif

				if(ftemp < 0)
				{
				    ftemp = -ftemp;
				}
				if(ftemp > LVD_BACK_ALARM_THRESHOLD)
				{
				    g_SiteInfo.MbRoughDataShareForI2C[OB_LVD_AUX1_ST+i].fValue = Digit_1.fValue;
				}
				else if(ftemp < LVD_BACK_ALARM_THRESHOLD - LVD_BACK_ALARM_THRESHOLD_DELTA)
				{
				    g_SiteInfo.MbRoughDataShareForI2C[OB_LVD_AUX1_ST+i].fValue = Digit_0.fValue;
				}
				
			}


		MB_VoltageHandle();		

		MB_CurrentHandle();

		MB_BattStatusHandle();

		MB_NonBatteryHandle();

		if(s_iMagicCount_BattCurr < MAGIC_COUNT_MAX_BATTCURR)
		{
		    s_iMagicCount_BattCurr++;
		}
	}//read(g_SiteInfo.iMbHandle...


	RunThread_Heartbeat(hLocalThread);
	
#else	
		MB_VoltageHandle();		

		MB_CurrentHandle();

		MB_BattStatusHandle();

		MB_NonBatteryHandle();

		if(s_iMagicCount_BattCurr < MAGIC_COUNT_MAX_BATTCURR)
		{
		    s_iMagicCount_BattCurr++;
		}
	}//read(g_SiteInfo.iMbHandle...


	RunThread_Heartbeat(hLocalThread);

	/* Step 2: OB */
	if(g_SiteInfo.iOBHandle > 0)
	{
		if(read(g_SiteInfo.iOBHandle, unMbSampVal.abyReadBuf, SIZE_OF_OB_CHN * 2)
			== (SIZE_OF_OB_CHN * 2))
		{
			g_SiteInfo.MbRoughDataShareForI2C[OB_EXIST_ST].iValue = 0;   //���óɴ���		

			//LOAD FUSE
			for(i = 0; i < 10; i++)
			{
					ftemp = (float)OB_LOAD_FUSE_RATE 
						* (float)(unMbSampVal.awRoughData[OB_IDX_LOADFUSE1 + i] - unMbSampVal.awRoughData[OB_IDX_GND])
						* s_afOBAdjustCoef[OB_ADJUST_COEF_LOADFUSE1 + i]
						/	 
						(float)(unMbSampVal.awRoughData[OB_IDX_BREF]
						- unMbSampVal.awRoughData[OB_IDX_GND])
						* s_afOBAdjustCoef[OB_ADJUST_COEF_REF_VOLT];

						if(ftemp < 0)
						{
						    ftemp = -ftemp;
						}
						if(ftemp > LOAD_FUSE_ALARM_THRESHOLD)
						{
						    g_SiteInfo.MbRoughDataShareForI2C[OB_LOAD_FUSE1_ST+i].fValue = Digit_1.fValue;
						}
						else if(ftemp < LOAD_FUSE_ALARM_THRESHOLD - LOAD_FUSE_ALARM_THRESHOLD_DELTA)
						{
						    g_SiteInfo.MbRoughDataShareForI2C[OB_LOAD_FUSE1_ST+i].fValue = Digit_0.fValue;
						}
			}
#if	_MAIN_BOARD_DEBUG
			printf("MB: OB_LOAD_FUSE1_ST: %d, %.02f\n\n", 
			    unMbSampVal.awRoughData[OB_IDX_LOADFUSE1], 
			    g_SiteInfo.MbRoughDataShareForI2C[OB_LOAD_FUSE1_ST].fValue);
#endif

			//BATT FUSE
			for(i = 0; i < 4; i++)
			{
			    ftemp = (float)OB_BATT_FUSE_RATE 
					* (float)(unMbSampVal.awRoughData[OB_IDX_BATTFUSE1 + i] - unMbSampVal.awRoughData[OB_IDX_GND])
					* s_afOBAdjustCoef[OB_ADJUST_COEF_BATTFUSE1 + i]
				    /	 
					(float)(unMbSampVal.awRoughData[OB_IDX_BREF]
				- unMbSampVal.awRoughData[OB_IDX_GND])
				* s_afOBAdjustCoef[OB_ADJUST_COEF_REF_VOLT];

				//ħ�����ִ���
				s_f32MagicSum_OB[i] += ftemp;
				if(s_iMagicCount < MAGIC_COUNT_MAX)
				{
				    s_f32MagicAvg_OB[i] = s_f32MagicSum_OB[i] / (s_iMagicCount+1);        
				}    
				else
				{
				    s_f32MagicSum_OB[i] -= s_f32MagicAvg_OB[i];
				    s_f32MagicAvg_OB[i] = s_f32MagicSum_OB[i] / MAGIC_COUNT_MAX;   
				}

				ftemp = s_f32MagicAvg_OB[i];

				if(ftemp < 0)
				{
				    ftemp = -ftemp;
				}
				if(ftemp > BATT_FUSE_ALARM_THRESHOLD)
				{
				    g_SiteInfo.MbRoughDataShareForI2C[OB_BATT_FUSE1_ST+i].fValue = Digit_1.fValue;
				}
				else if(ftemp < BATT_FUSE_ALARM_THRESHOLD - BATT_FUSE_ALARM_THRESHOLD_DELTA)
				{
				    g_SiteInfo.MbRoughDataShareForI2C[OB_BATT_FUSE1_ST+i].fValue = Digit_0.fValue;
				}

#if	_MAIN_BOARD_DEBUG
				printf("\nMB: OB_BATT_FUSE%d_ST: %d, %.02f\n",  i,
				    unMbSampVal.awRoughData[OB_IDX_BATTFUSE1+i], 
				    ftemp);
#endif
			}


			//FUEL SENSOR
			for(i = 0; i < 2; i++)
			{
				g_SiteInfo.MbRoughDataShareForI2C[OB_FUEL_SENSOR1+i].fValue 
					= (float)OB_FUEL_SENSOR_RATE 
					* (float)(unMbSampVal.awRoughData[OB_IDX_FUEL_SENSOR1 + i] - unMbSampVal.awRoughData[OB_IDX_GND])
					* s_afOBAdjustCoef[OB_ADJUST_COEF_FUEL1 + i]
				/	 
					(float)(unMbSampVal.awRoughData[OB_IDX_BREF]
				- unMbSampVal.awRoughData[OB_IDX_GND])
				/ OB_FUEL_RESISTOR_VAL * 1000
				* s_afOBAdjustCoef[OB_ADJUST_COEF_REF_VOLT];
			}
#if	_MAIN_BOARD_DEBUG
			printf("MB: OB_FUEL_SENSOR1: %d, %.02f\n\n", 
			    unMbSampVal.awRoughData[OB_IDX_FUEL_SENSOR1], 
			    g_SiteInfo.MbRoughDataShareForI2C[OB_FUEL_SENSOR1].fValue);
#endif

			//LVD aux
			for(i = 0; i < 2; i++)
			{				
				ftemp
					= (float)OB_LVD_AUX_RATE 
					* (float)(unMbSampVal.awRoughData[OB_IDX_LVD_AUX1 + i] - unMbSampVal.awRoughData[OB_IDX_GND])
					* s_afOBAdjustCoef[OB_ADJUST_COEF_LVD_AUX1 + i]
				/	 
					(float)(unMbSampVal.awRoughData[OB_IDX_BREF]
				- unMbSampVal.awRoughData[OB_IDX_GND])
				* s_afOBAdjustCoef[OB_ADJUST_COEF_REF_VOLT];

#if	_MAIN_BOARD_DEBUG
				printf("MB: OB_LVD_AUX1_ST: %d, %.02f\n\n", 
				    unMbSampVal.awRoughData[OB_IDX_LVD_AUX1], 
				    ftemp);
#endif

				if(ftemp < 0)
				{
				    ftemp = -ftemp;
				}
				if(ftemp > LVD_BACK_ALARM_THRESHOLD)
				{
				    g_SiteInfo.MbRoughDataShareForI2C[OB_LVD_AUX1_ST+i].fValue = Digit_1.fValue;
				}
				else if(ftemp < LVD_BACK_ALARM_THRESHOLD - LVD_BACK_ALARM_THRESHOLD_DELTA)
				{
				    g_SiteInfo.MbRoughDataShareForI2C[OB_LVD_AUX1_ST+i].fValue = Digit_0.fValue;
				}
				
			}

			//temperature
			//g_SiteInfo.MbRoughDataShareForI2C[OB_TEMP1].fValue 
			fTemperatureTemp = ((float)OB_TEMERATURE_RATE 
				* (unMbSampVal.awRoughData[OB_IDX_TEMPERATURE] - unMbSampVal.awRoughData[OB_IDX_GND])
				* s_afOBAdjustCoef[OB_ADJUST_COEF_REF_VOLT]
				/	 
				(float)(unMbSampVal.awRoughData[OB_IDX_BREF] - unMbSampVal.awRoughData[OB_IDX_GND])
				-1.3923)				
				* s_afOBAdjustCoef[OB_ADJUST_COEF_TEMP1]
				* 1000 / 5.1;	
#if	_MAIN_BOARD_DEBUG
				printf("MB: OB_TEMP1: %d, %.02f\n\n", 
				    unMbSampVal.awRoughData[OB_IDX_TEMPERATURE], 
				    g_SiteInfo.MbRoughDataShareForI2C[OB_TEMP1].fValue);
#endif
				TmepSmoothingAlgorithm(&s_fOldTemperature3Val,&fTemperatureTemp,&s_fOldTemperature3Sum,2);

			g_SiteInfo.MbRoughDataShareForI2C[OB_TEMP1].fValue = fTemperatureTemp;

		}
	}
	else
	{
		g_SiteInfo.MbRoughDataShareForI2C[OB_EXIST_ST].iValue = 1;
	}//if(g_SiteInfo.iOBHandle > 0)
	
#endif

	RunThread_Heartbeat(hLocalThread);

	/* Step 3: IB01 */
	if(g_SiteInfo.iIB01Handle > 0)
	{
		if(read(g_SiteInfo.iIB01Handle, unMbSampVal.abyReadBuf, IB01_RETURN_BYTE_NUM)
			== (IB01_RETURN_BYTE_NUM))
		{
			//printf(" IB01 = %2x \n", unMbSampVal.byDI);

			if(unMbSampVal.byDI & 0x0020)
			{
				g_SiteInfo.MbRoughDataShareForI2C[IB01_EXIST_ST].ulValue = 0;
			}
			else
			{
				g_SiteInfo.MbRoughDataShareForI2C[IB01_EXIST_ST].ulValue = 1;
			}

			//if(unMbSampVal.byDI & 0x0020) //����
			//{
#ifdef _CODE_FOR_MINI
				for(i = 0; i < 2; i++)
				{
					if(unMbSampVal.byDI & (0x0001 << i))
					{
						g_SiteInfo.MbRoughDataShareForI2C[IB01_DI1_ST + i].ulValue = 0;//change to 0,primary is 1 value reverse
					}
					else
					{
						g_SiteInfo.MbRoughDataShareForI2C[IB01_DI1_ST + i].ulValue = 1;//change to 1,primary is 0 value reverse
					}
#if	_MAIN_BOARD_DEBUG
				printf("MB: IB01_DI%d: %d\n", 
				    i,g_SiteInfo.MbRoughDataShareForI2C[IB01_DI1_ST + i].ulValue);
#endif
				}
#else
				for(i = 0; i < 5; i++)
				{
					if(unMbSampVal.byDI & (0x0001 << i))
					{
						g_SiteInfo.MbRoughDataShareForI2C[IB01_DI1_ST + i].ulValue = 1;
					}
					else
					{
						g_SiteInfo.MbRoughDataShareForI2C[IB01_DI1_ST + i].ulValue = 0;
					}
				}
#endif
			//}
		}
	}

	if(s_iMagicCount < MAGIC_COUNT_MAX)
	{
	    s_iMagicCount++;
	}


	return;
}

#define MB_CH_END_FLAG			-1

/*==========================================================================*
 * FUNCTION : MB_StuffChannel
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ENUMSIGNALPROC  EnumProc : 
 *            LPVOID  lpvoid   : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:46
 *==========================================================================*/
struct	tagMB_CHANNEL_ROUGH_IDX
{
    int		iChannel;
    int		iRoughData;
};

typedef struct tagMB_CHANNEL_ROUGH_IDX MB_CHN_TO_ROUGH_DATA;

static void MB_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
					 LPVOID lpvoid)				//Parameter of the callback function
{
	int		i;

	MB_CHN_TO_ROUGH_DATA		MbGroupSig[] =
	{
		/* MB */
		{MB_CH_DC_VOLT,			MB_DC_VOLT,			},
		{MB_CH_DC_LOAD_CURR,		MB_LOAD_CURR,			},	
		{MB_CH_BATT1_VOLT,		MB_BATT1_VOLT,			},
		{MB_CH_BATT2_VOLT,		MB_BATT2_VOLT,			},
		{MB_CH_BATT1_CURR,		MB_BATT1_CURR,			},
		{MB_CH_BATT2_CURR,		MB_BATT2_CURR,			},
		{MB_CH_TEMPERATURE1,		MB_TEMPERATURE1,		},
		{MB_CH_TEMPERATURE2,		MB_TEMPERATURE2,		},
		
#ifndef _CODE_FOR_MINI		
		{MB_CH_LOAD_CURR_COM_VOLT,	MB_LOAD_CURR_COM_VOLT,	},
		{MB_CH_BATT1_CURR_COM_VOLT,	MB_BATT1_CURR_COM_VOLT,	},
		{MB_CH_BATT2_CURR_COM_VOLT,	MB_BATT2_CURR_COM_VOLT,	},
#endif

		{MB_CH_BATT1_ST,		MB_BATT1_ST,			},
		{MB_CH_BATT2_ST,		MB_BATT2_ST,			},

		{MB_CH_ABS_BATT_CURR1,		ABS_BATT_CURR1,			},
		{MB_CH_ABS_BATT_CURR2,		ABS_BATT_CURR2,			},


		{MB_CH_PASSWORD_JUMPER,		MB_PASSWORD_JUMPER,		},
		{MB_CH_OS_READONLY_JUMPER,	MB_OS_READONLY_JUMPER,	},
		{MB_CH_APP_READONLY_JUMPER,	MB_APP_READONLY_JUMPER,	},
		{MB_CH_5V_DC_POWER_JUMPER,	MB_5V_DC_POWER_JUMPER,	},

		/* IB01 */
#ifndef _CODE_FOR_MINI
		{MB_CH_IB01_EXIST_ST,		IB01_EXIST_ST,		},
#endif
		{MB_CH_IB01_DI1_ST,		IB01_DI1_ST,		},
		{MB_CH_IB01_DI2_ST,		IB01_DI2_ST,		},
#ifndef _CODE_FOR_MINI
		{MB_CH_IB01_DI3_ST,		IB01_DI3_ST,		},
		{MB_CH_IB01_DI4_ST,		IB01_DI4_ST,		},
		{MB_CH_SPD,			IB01_SPD_ST,		},
#endif
		{MB_CH_END_FLAG,		MB_CH_END_FLAG,				},
	};

//	printf("MbStuff: g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue=%f~~~\n ",g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);
//	printf("MbStuff: g_SiteInfo.MbRoughDataShareForI2C[MB_BATT2_CURR].fValue=%f~~~\n ",g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);
//	printf("MbStuff: g_SiteInfo.MbRoughDataShareForI2C[MB_LOAD_CURR].fValue=%f~~~\n ",g_SiteInfo.MbRoughDataShareForI2C[MB_BATT1_CURR].fValue);
	

	i = 0;
	while(MbGroupSig[i].iChannel != MB_CH_END_FLAG)
	{
		EnumProc(MbGroupSig[i].iChannel, 
				(g_SiteInfo.MbRoughDataShareForI2C[MbGroupSig[i].iRoughData].fValue), 
				lpvoid );
		i++;
	}

	return;
}


/*==========================================================================*
 * FUNCTION : Query
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE hComm    : 
 *            int                                   iUnitNo  : 
 *            ENUMSIGNALPROC                        EnumProc : 
 *            LPVOID  lpvoid   : 
 * RETURN   : DLLExport BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:46
 *==========================================================================*/
#define FOR_DATA_EXCHANGE_TEST 0
DLLExport BOOL Query(HANDLE hComm,				//Handle of the port, unused here
					 int iUnitNo,				//Sampler address, unused here
					 ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
					 LPVOID lpvoid)				//Parameter of the callback function
{
	UNUSED(hComm);
	UNUSED(iUnitNo);
	static BOOL			s_bFirstRun = TRUE;

#if FOR_DATA_EXCHANGE_TEST
	ACU_VPN_INFO szVPNinfox;
	SMS_PHONE_INFO szSmsInfo;
	int iBufLength;
#endif
	/*TRACE("********Main Board DEBUG: Query once!\n");*/
	if(s_bFirstRun)
	{		
		g_SiteInfo.iMbHandle = -1;
		
#ifdef _CODE_FOR_MINI		
		g_SiteInfo.iOBHandle = 1;	// in order to keep consistence, we assume it's exist in mini controller,actually it's not exist
#else
		g_SiteInfo.iOBHandle = -1;
#endif
		g_SiteInfo.iIB01Handle = -1;
		MB_ReadCoef();

		g_SiteInfo.iMbHandle = open(MB_DEVICE_NAME, O_RDWR);
#ifdef _CODE_FOR_MINI
		//g_SiteInfo.iOBHandle = open(MB_OB_DEVICE_NAME, O_RDWR);
#else
		g_SiteInfo.iOBHandle = open(MB_OB_DEVICE_NAME, O_RDWR);	
#endif
		g_SiteInfo.iIB01Handle = open(MB_IB01_DEVICE_NAME, O_RDWR);	

#if FOR_DATA_EXCHANGE_TEST
		
		szVPNinfox.iVPNEnable = 1;
		szVPNinfox.iRemotePort = 2;
		szVPNinfox.ulRemoteIp = 1111;

		if(DxiSetData(VAR_VPN_INFO, VPN_INFO_ALL, 0, sizeof(ACU_VPN_INFO), &szVPNinfox, 0) != 0)
		{
		    printf("\n\n Set VPN FAIL! \n");
		}

		if(DxiGetData(VAR_VPN_INFO, VPN_INFO_ALL, 0,  &iBufLength,  &szVPNinfox, 0) == 0)
		{ 
	    
		printf("\n\nszVPNinfox.iRemotePort = %d \n", szVPNinfox.iRemotePort);
		printf("\n\nszVPNinfox.iVPNEnable = %d \n", szVPNinfox.iVPNEnable);
		printf("\n\nszVPNinfox.ulRemoteIp = %d \n", szVPNinfox.ulRemoteIp);
		}

		szSmsInfo.iAlarmLevel = 2;
		sprintf(szSmsInfo.szPhoneNumber1, "%s", "1892344444");
		memset(szSmsInfo.szPhoneNumber2, 0, sizeof(szSmsInfo.szPhoneNumber2));
		memset(szSmsInfo.szPhoneNumber3, 0, sizeof(szSmsInfo.szPhoneNumber3));

		if(DxiSetData(VAR_SMS_PHONE_INFO, SMS_INFO_PHONE_ALL, 0, sizeof(SMS_PHONE_INFO), &szSmsInfo, 0) != 0)
		{
		    printf("\n\n Set SMS FAIL! \n");
		}

		DxiGetData(VAR_SMS_PHONE_INFO, SMS_INFO_PHONE_ALL, 0,  &iBufLength,  &szSmsInfo, 0); 
		
		printf("\n\nszSmsInfo.iAlarmLevel = %d \n", szSmsInfo.iAlarmLevel);
		printf("\n\nszSmsInfo.szPhoneNumber1 = %s \n", szSmsInfo.szPhoneNumber1);
		printf("\n\nszSmsInfo.szPhoneNumber2 = %s \n", szSmsInfo.szPhoneNumber2);
		
#endif	

		//Create the Slave thread
		g_hMainBoardFilterThread
		    = RunThread_Create("MainBoardSlaveThread",
		    (RUN_THREAD_START_PROC)MainBoard_FilterTask,//RS485_RunAsSlave,
		    NULL,
		    NULL,
		    0);	

		if (!g_hMainBoardFilterThread)
		{
		    AppLogOut("MAINBOARD_SAM", 
			APP_LOG_ERROR,
			"Failed to create the MainBoard_Filter thread\n");

		    ASSERT(TRUE);
		}
	}
	
	s_bFirstRun = FALSE;
	
	g_iRunningFlag = MB_FALG_RUNNING;

	MB_Sample();

	MB_StuffChannel(EnumProc, lpvoid);

    return TRUE;
}

#define CFG_BUF_SIZE   0x100

//дУ׼ϵ�����ļ��У������ļ�ϵͳ���ļ���boardtest.c���ĺ���SaveCoeff
static int SaveCoeff(unsigned int pAddress, float Fin)
{
    int adj_fd;
    int len,i;
    unsigned char *sbuf;
    unsigned char	Digit[6] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
    unsigned int	temp1,temp2;

    sbuf= malloc(CFG_BUF_SIZE);
    if(!sbuf)
    {
	printf("fail to malloc\n");
	return -1;
    }

    memset(sbuf,0,CFG_BUF_SIZE);
    adj_fd = open("/home/adjcoeff.log",O_RDWR);	//O_WRONLY

    if(adj_fd <=0)
    {
	free(sbuf);
	printf("failed to open /home/adjcoeff.log!\n");
	return -1;
    }
    read(adj_fd,sbuf,CFG_BUF_SIZE);
    close (adj_fd);

    //len=sizeof(u_char);
    Fin *= 10000;
    if(Fin - (unsigned int)Fin >= 0.5)
	Fin++;									/*4��5��*/
    if(Fin > 655350)								/*��������*/
	Fin = 655350;

    temp1 = (unsigned int)Fin;					/*ת��Ϊ����*/
    for(i = 0;i < 6;i++)						/*����6λ���֣�4λС��*/
    {
	temp2 = temp1 % 10;
	Digit[5 - i] = (unsigned char)temp2;	       /*����*/
	temp1 = (temp1 - temp2) / 10;	
	if(temp1 == 0)
	    break;
    }
    for(i = 0;i < 6;i++)						/*��ʾ����ASCII��*/
    {
	Digit[i]  = Digit[i] + 0x30;		             /*ASCII��*/

    }

    Digit[0] = Digit[1];
    Digit[1] =  '.' ;           				              /*С����*/


    //len=sizeof(u_char);
    //sprintf(sbuf+pAddress*len,"2.136");
    for(i = 0;i < 6;i++)						/*д�뻺����*/
    {
	//sprintf(sbuf+pAddress+i,Digit[i]);
	sprintf((char *)(sbuf+pAddress+i),"%c",Digit[i]);


    }
    adj_fd = open("/home/adjcoeff.log",O_RDWR);	//O_WRONLY
    if(adj_fd <=0)
    {
	free(sbuf);
	printf("failed to open /home/adjcoeff.log!\n");
	return -1;
    }


    len =strlen((const char *)sbuf);
    if (write(adj_fd,sbuf, (unsigned int)len)!=len)
    {	
	free(sbuf);
	close (adj_fd);
	printf("write log file error!");
	return -1;
    }
    close (adj_fd);	
    free(sbuf);
    return 0;

}

#define	MAX_SIZE_OF_CTL_CMD_STR 128

#define          DCRefAdress                   14  //ĸ��У׼ϵ�����ļ��е�ƫ�Ƶ�ַ
/*==========================================================================*
 * FUNCTION : Control
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hComm   : 
 *            int     iUnitNo : 
 *            char*   strCmd  : 
 * RETURN   : DLLExport BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:48
 *==========================================================================*/

#define IOC_SET_DO1  	              _IOW(DIDO_MAJOR, 1, unsigned long)
#define IOC_SET_DO2  	              _IOW(DIDO_MAJOR, 2, unsigned long)
#define IOC_SET_DO3  	              _IOW(DIDO_MAJOR, 3, unsigned long)
#define IOC_SET_DO4  	              _IOW(DIDO_MAJOR, 4, unsigned long)
#define IOCTL_SET_SLED                 _IOW(DIDO_MAJOR, 9, unsigned long)


struct	tagMB_CTL_CMD
{
    int	iCmdNo;			//rather is Control Channel No.
    float	fParam;
};
typedef struct tagMB_CTL_CMD	MB_CTL_CMD;

DLLExport BOOL Control(HANDLE hComm,	//Handle of the port, unused here
					   int iUnitNo,		//Sampler address, unused here
                       char* strCmd)	//String of the control command
{
	UNUSED(hComm);
	UNUSED(iUnitNo);

	/*
	union PARA_CONTROL
	{
		float	f_value;
		int		d_value;
	};
	union PARA_CONTROL param1;*/

	
    char			strExtracted[MAX_SIZE_OF_CTL_CMD_STR] = {0};
    int				iPosition;
    float	ftemp;

	MB_CTL_CMD		CtlCmd;

	int				iOutput;
	//TRACE("************CAN DEBUG: a command: %s!\n", strCmd);
    
	//To provent not be initialized
	if(MB_FALG_RUNNING != g_iRunningFlag)
	{
		return FALSE;
	}

    //Extract the command No.
    iPosition = MB_StrExtract(strCmd, strExtracted, ',');

	if((iPosition <= 0) || (iPosition > 10))
	{
		return FALSE;	
	}

    CtlCmd.iCmdNo = atoi(strExtracted);
 
	//Extract the parameter
	iPosition = MB_StrExtract(strCmd + iPosition, strExtracted, ',' );
	
	if((iPosition <= 0) || (iPosition > 10))
	{
		return FALSE;	
	}
	CtlCmd.fParam = atof(strExtracted);
    
	switch(CtlCmd.iCmdNo)
	{
	case MB_CTL_CH_ALM_LIGHT:
		iOutput = (FLOAT_EQUAL0(CtlCmd.fParam)) ? 0 : 1;
		ioctl(g_SiteInfo.iIB01Handle, IOCTL_SET_SLED, iOutput);
		break;	

	case MB_CTL_CH_BUS_ADJUST:	//У׼ĸ�ŵ�ѹ
	    printf("Now adjust the bus voltage!\n");
	    printf("CtlCmd.fParam is %f\n", CtlCmd.fParam);
	    printf("MB_DC_VOLT is %f\n", g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue);
	    printf("MB_ADJUST_COEF_BUS_VOLT is %f\n", s_afMbAdjustCoef[MB_ADJUST_COEF_BUS_VOLT]);
	    ftemp = CtlCmd.fParam / g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue * s_afMbAdjustCoef[MB_ADJUST_COEF_BUS_VOLT];
	    printf("ftemp is %f\n", ftemp);
	    if((ftemp >= 0.95) && (ftemp <= 1.05))
	    {
			s_afMbAdjustCoef[MB_ADJUST_COEF_BUS_VOLT] = ftemp;
	
			if(SaveCoeff(DCRefAdress, ftemp) == 0)//��У׼ϵ���浽"/home/adjcoeff.log"��
			{
				printf("Success to save the MB_ADJUST_COEF_BUS_VOLT\n");
			}
			else
			{
				printf("Fail to save the MB_ADJUST_COEF_BUS_VOLT\n");
			}
	    }
	    break;	

	case MB_CTL_CH_IB01_DO1:
	    iOutput = (FLOAT_EQUAL0(CtlCmd.fParam)) ? 0 : 1;
	    ioctl(g_SiteInfo.iIB01Handle, IOC_SET_DO1, iOutput);			
	    break;

	case MB_CTL_CH_IB01_DO2:
	    iOutput = (FLOAT_EQUAL0(CtlCmd.fParam)) ? 0 : 1;
	    ioctl(g_SiteInfo.iIB01Handle, IOC_SET_DO2, iOutput);			
	    break;

	case MB_CTL_CH_IB01_DO3:
	    iOutput = (FLOAT_EQUAL0(CtlCmd.fParam)) ? 0 : 1;
	    ioctl(g_SiteInfo.iIB01Handle, IOC_SET_DO3, iOutput);			
	    break;

	case MB_CTL_CH_IB01_DO4:
	    iOutput = (FLOAT_EQUAL0(CtlCmd.fParam)) ? 0 : 1;
	    ioctl(g_SiteInfo.iIB01Handle, IOC_SET_DO4, iOutput);			
	    break;


	default:
		break;
	}
	

    return TRUE;
}


/*==========================================================================*
 * FUNCTION : GetProductInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: HANDLE  hComm   : 
 *            int     nUnitNo : 
 *            void    *pPI    : 
 * RETURN   : DLLExport BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-19 09:48
 *==========================================================================*/
DLLExport BOOL GetProductInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);

	PRODUCT_INFO  *pInfo;

	pInfo = (PRODUCT_INFO*)pPI;

	pInfo->bSigModelUsed = TRUE;

	return TRUE;
}

/*=============================================================================*
* FUNCTION: MainBoard_Filter
* PURPOSE  : filter the sampling data
* RETURN   : void
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
     DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : liankf        DATE: 
*==========================================================================*/
#define BUS_VOLT_ACCURACY	0.5		//��������Ҫ��
#define OVER_ACCURACY_CNT_MAX   3		//����һ�β���ֵ�仯����BUS_VOLT_ACCURACY�Ĵ����ﵽn�κ���Ч
#define BUS_VOLT_AVERAGE_FILTER_NUM	5	//����ƽ��ֵ���˷���ƽ������

extern BOOL	g_bExitNotification;
DWORD	MainBoard_FilterTask(void)
{
    HANDLE	hself;
    MB_SAMPLE_VALUE	unMbSampVal_FilterTask;
    hself	= RunThread_GetId(NULL);
    
    static float s_f32LastValue = 0;            //��һ�β���ֵ
    static int	s_iOverAccuracyCnt = 0;		//����һ�β���ֵ�仯����BUS_VOLT_ACCURACY�Ĵ���

    static float s_f32AverateFilterBuf[BUS_VOLT_AVERAGE_FILTER_NUM] = {0};
    static int s_iAverateCnt = 0;
    int i;
    float   f32Sum;

    SAMPLING_VALUE_MB_ROUGH	MbRoughDataBusVolt;
    float f32BusVoltBuf;

    while(!g_bExitNotification)	
    {	
	if(g_SiteInfo.iMbHandle <= 0)
	{
	    g_SiteInfo.iMbHandle = open(MB_DEVICE_NAME, O_RDWR);
	    if(g_SiteInfo.iMbHandle <= 0)
	    {
		//MB_LOG_OUT(APP_LOG_ERROR, "Main board driver can not be opened!\n");
		continue;
	    }
	}
	

	ZERO_POBJS(unMbSampVal_FilterTask.abyReadBuf, SIZE_OF_MB_ANALOG_CHN * 2);

	if(read(g_SiteInfo.iMbHandle, unMbSampVal_FilterTask.abyReadBuf, SIZE_OF_MB_ANALOG_CHN * 2)
	    == (SIZE_OF_MB_ANALOG_CHN * 2))
	{
	    
	    MbRoughDataBusVolt.fValue
		= (float)MB_BUS_VOLT_RATE
		* (unMbSampVal_FilterTask.awRoughData[MB_IDX_DC_VOLT] 
		- unMbSampVal_FilterTask.awRoughData[MB_IDX_GND])
		    * s_afMbAdjustCoef[MB_ADJUST_COEF_BUS_VOLT]
		/ (unMbSampVal_FilterTask.awRoughData[MB_IDX_DC_VOLT_REF]
		- unMbSampVal_FilterTask.awRoughData[MB_IDX_GND])
		* s_afMbAdjustCoef[MB_ADJUST_COEF_REF_VOLT];	    
	    
		//����ֵ����һ�α仯���ȳ���BUS_VOLT_ACCURACY������Ҫȷ��1��
		if((ABS(MbRoughDataBusVolt.fValue - s_f32LastValue)) > BUS_VOLT_ACCURACY)
		{
		    f32BusVoltBuf = g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue;
		    if(s_iOverAccuracyCnt == 0)
		    {
			s_iOverAccuracyCnt = 1;			
		    }
		    else
		    {
			s_iOverAccuracyCnt++;
			if(s_iOverAccuracyCnt >= OVER_ACCURACY_CNT_MAX)
			{

			    s_f32LastValue = MbRoughDataBusVolt.fValue;
			    s_iOverAccuracyCnt = 0;

			    //��ƽ��ֵ��������ֵ����ʼ��Ϊ��ֵ
			    for(i = 0; i < BUS_VOLT_AVERAGE_FILTER_NUM; i++)
			    {
				s_f32AverateFilterBuf[i] = MbRoughDataBusVolt.fValue;
			    }
			    s_iAverateCnt = BUS_VOLT_AVERAGE_FILTER_NUM;

			    f32BusVoltBuf = ABS(MbRoughDataBusVolt.fValue);
			}			
		    }		    
		}
		//���򣬲���ƽ��ֵ����
		else
		{
		    s_f32LastValue = MbRoughDataBusVolt.fValue;
		    s_iOverAccuracyCnt = 0;
		    
		    if(s_iAverateCnt < BUS_VOLT_AVERAGE_FILTER_NUM)
		    {
			s_f32AverateFilterBuf[s_iAverateCnt] = MbRoughDataBusVolt.fValue;
			s_iAverateCnt++;
		    }
		    else
		    {
			for(i = 0; i < (BUS_VOLT_AVERAGE_FILTER_NUM - 1); i++)
			{
			    s_f32AverateFilterBuf[i] = s_f32AverateFilterBuf[i+1];
			}
			s_f32AverateFilterBuf[i] = MbRoughDataBusVolt.fValue;
			s_iAverateCnt = BUS_VOLT_AVERAGE_FILTER_NUM;
		    }

		    f32Sum = 0;
		    for(i = 0; i < s_iAverateCnt; i++)
		    {
			f32Sum += s_f32AverateFilterBuf[i];
		    }
		    
		    f32Sum /= s_iAverateCnt;
		    
		    //��������
		    f32BusVoltBuf = ABS(f32Sum);
		}	    

		//printf("\n\n MbRoughDataBusVolt = %fv   f32BusVoltBuf = %fv ", MbRoughDataBusVolt.fValue, f32BusVoltBuf);
	    

	    if(f32BusVoltBuf < 0.5)
	    {
		f32BusVoltBuf = 0.0;
	    }
	    if(MB_NeedClearBusVolt(f32BusVoltBuf))
	    {
		f32BusVoltBuf = 0.0;
	    }	    
	    
	    //��ֵ����MB_StuffChannel()�����ɼ���ͨ��
	    g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue = f32BusVoltBuf;

	    
	}	

	RunThread_Heartbeat(hself);	//Heartbeat
	Sleep(1000);
	
    }//End will

  
    return 0;
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : main_board.h
 *  CREATOR  : Frank Cao                DATE: 2008-07-25 16:48
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __MAIN_BOARD_H
#define __MAIN_BOARD_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "public.h"

#define MB_IB01_DEVICE_NAME				"/dev/dido"
#define DIDO_MAJOR					240 

#define G_TOTAL_LOAD_CURRENT_ID			3
#define	MB_RCT_GROUP_CURRENT_ID			1
#define	MB_RCT_GROUP_VOLT_ID			2
#endif


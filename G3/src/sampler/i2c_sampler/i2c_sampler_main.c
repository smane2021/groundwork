/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC_HF
*
*  FILENAME : i2c_sampler_main.c
*  CREATOR  : Kenn Wan                DATE: 2008-06-19 16:57
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  : 2013-4-28 Rewrite by Liankaifeng 
*
*==========================================================================*/



#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include <conio.h>
#include <math.h>
#include <time.h>


#include "i2c_sampler_main.h"

#define I2C_DEVICE_NAME					"/dev/enpc_i2c"


#define _I2C_SAMPLER_DEBUG	0

#define I2C_BUF_NUM						200

#define  I2C_LOG_OUT(iLevel, OutString) \
	AppLogOut("I2C_SAMP", (iLevel), "%s", OutString)



union PARA_CONTROL
{
	float	f_value;
	int	d_value;
};

union unSAMPLING_VALUE
{
    float	fValue;
    int		iValue;
    UINT	uiValue;
    ULONG	ulValue;
    DWORD	dwValue;
};
typedef union unSAMPLING_VALUE SAMPLING_VALUE;
/*****************************************************************/
/*ģ���ڹ���������ö�١��궨�� */
/*****************************************************************/
union STRTOFLOAT
{
	float f_value;       // ������
	BYTE  by_value[4];   // �ַ���
};

union unI2C_VALUE
{
	float	fValue;
	int		iValue;	
};
typedef union unI2C_VALUE I2C_VALUE;

enum I2C_IB_TYPE_DEFINE
{
	IB_TYPE_IB1 = 0,
	IB_TYPE_IB2,
	IB_TYPE_NA,
};

/*���ñ��ź�λ����Ŷ���*/
#define EQUIP_IP_SYSTEM							1
#define	SYSTEM_SAMPLING_ID_VOLTAGE		  		1

#define EQUIP_IP_BATTERY1						116
#define EQUIP_IP_BATTERY2						117
#define	BATTERY_SAMPLING_ID_VOLTAGE		  		1

#define EQUIP_IP_EIB1							197
#define EQUIP_IP_EIB2							198
#define EQUIP_IP_EIB3							199
#define EQUIP_IP_EIB4							200
#define	EIB_SETTING_ID_BLOCK_DIFF	  			10
#define	EIB_SETTING_ID_BLOCK_DIFF_MID  			11
#define	EIB_SETTING_ID_BLOCK_USENUM	  			12

#define	EIB_SETTING_ID_SHUNT1_CURRENT			1
#define	EIB_SETTING_ID_SHUNT1_VOLT				2
#define	EIB_SETTING_ID_SHUNT2_CURRENT			3
#define	EIB_SETTING_ID_SHUNT2_VOLT				4
#define	EIB_SETTING_ID_SHUNT3_CURRENT			5
#define	EIB_SETTING_ID_SHUNT3_VOLT				6
#define	EIB_SETTING_ID_BATT_NUM		  			7
#define	EIB_SETTING_ID_LOAD_NUM		  			8
#define	EIB_SETTING_ID_VOLTAGE_TYPE				9

#define EQUIP_ID_FUELGROUP					700
#define	FUELGROUP_EXIST_ST_TYPE					100

#define EQUIP_ID_OBFUEL1					1500
#define EQUIP_ID_OBFUEL2					1501

#define IB_NUM_SOLUTION							4  //Solution�ж����IB�����
#define EIB_NUM_SOLUTION						4  //Solution�ж����EIB�����

#define I2C_SCAN_BOARDS_CMD_CHD                 9  // special channel for the control command of scanning board 

/*IB �����ź� */
enum I2C_SAMP_IBGROUP_CHANNEL
{
	I2C_CH_IB_NUM = 47,		//
	I2C_CH_IB_TYPE,				// 0-IB1 1-IB2 2-NA
	I2C_CH_IB_EXIST,			// 0-EXIST 1-NOT EXIST
	I2C_CH_EIB_EXIST,			// 0-EXIST 1-NOT EXIST
};

/*IB �� */
enum I2C_SAMP_IB_CHANNEL	
{
	I2C_CH_IB_1_STATE = 51,		// 0-ON 1-OFF
	I2C_CH_IB_1_FAILURE,			// 0-ON 1-OFF	
	I2C_CH_IB_1_DI1 = 56,		// 0-OFF 1-ON
	I2C_CH_IB_1_DI2,				// 0-OFF 1-ON
	I2C_CH_IB_1_DI3,				// 0-OFF 1-ON
	I2C_CH_IB_1_DI4,				// 0-OFF 1-ON
	I2C_CH_IB_1_DI5,				// 0-OFF 1-ON
	I2C_CH_IB_1_DI6,				// 0-OFF 1-ON
	I2C_CH_IB_1_DI7,				// 0-OFF 1-ON
	I2C_CH_IB_1_DI8,				// 0-OFF 1-ON
	I2C_CH_IB_1_SERIALNO1,	// serial no
	I2C_CH_IB_1_SERIALNO2,	// serial no
	I2C_CH_IB_1_SERIALNO3,	// serial no
	I2C_CH_IB_1_BARCODE1,		// barcode no
	I2C_CH_IB_1_BARCODE2,		// barcode no
	I2C_CH_IB_1_BARCODE3,		// barcode no
	I2C_CH_IB_1_BARCODE4,		// barcode no	
	I2C_CH_IB_1_TEMPERATURE1 = 79,	// 
	I2C_CH_IB_1_TEMPERATURE2,	// 
};
enum I2C_CTRL_IB_CHANNEL	
{
	I2C_CTRL_CH_IB_1_DO1 = 51,		// 0-ON 1-OFF
	I2C_CTRL_CH_IB_1_DO2,				
	I2C_CTRL_CH_IB_1_DO3,		
	I2C_CTRL_CH_IB_1_DO4,							
	I2C_CTRL_CH_IB_1_DO5,				
	I2C_CTRL_CH_IB_1_DO6,				
	I2C_CTRL_CH_IB_1_DO7,				
	I2C_CTRL_CH_IB_1_DO8,				
	I2C_CTRL_CH_IB_MAX = 80,
};

#define	I2C_CHANNEL_IB_SPACE		30   //ÿ��IB�������ñ��з���Ĳɼ��ź�/�����źſռ�

/*EIB �� */
enum I2C_SAMP_EIB_CHANNEL	
{
	I2C_CH_EIB_1_STATE = 171,		// 0-ON 1-OFF
	I2C_CH_EIB_1_FAILURE,			// 0-ON 1-OFF	
	I2C_CH_EIB_1_VOLT1 = 176,	
	I2C_CH_EIB_1_VOLT2,
	I2C_CH_EIB_1_VOLT3,
	I2C_CH_EIB_1_VOLT4,
	I2C_CH_EIB_1_VOLT5,
	I2C_CH_EIB_1_VOLT6,
	I2C_CH_EIB_1_VOLT7,
	I2C_CH_EIB_1_VOLT8,
	I2C_CH_EIB_1_BLOCKNUM = 185,  //BLOCK IN USE	
	I2C_CH_EIB_1_CURR1 = 186,	
	I2C_CH_EIB_1_CURR2,
	I2C_CH_EIB_1_CURR3,
	I2C_CH_EIB_1_LOADCURR1 = 189,	
	I2C_CH_EIB_1_LOADCURR2,
	I2C_CH_EIB_1_LOADCURR3,
	I2C_CH_EIB_1_BATTCURR1,
	I2C_CH_EIB_1_BATTCURR2,
	I2C_CH_EIB_1_BATTCURR3,
	I2C_CH_EIB_1_TEMPERATURE1 = 195,	
	I2C_CH_EIB_1_TEMPERATURE2,

	I2C_CH_EIB_ABSCURRENT1,
	I2C_CH_EIB_ABSCURRENT2,
	I2C_CH_EIB_ABSCURRENT3,

	I2C_CH_EIB_1_BADBLOCKNUM = 204,  //BAD BATTERY BLOCK 	

	I2C_CH_EIBBATT_1_BATTSTATE1 = 205,	
	I2C_CH_EIBBATT_1_BATTSTATE2,	
	I2C_CH_EIBBATT_1_BATTSTATE3,
};
enum I2C_CTRL_EIB_CHANNEL	
{
	I2C_CTRL_CH_EIB_1_DO1 = 171,		// 0-ON 1-OFF
	I2C_CTRL_CH_EIB_1_DO2,				
	I2C_CTRL_CH_EIB_1_DO3,		
	I2C_CTRL_CH_EIB_1_DO4,	

	I2C_CTRL_CH_EIB_1_SHUNT1_CURR = 175,
	I2C_CTRL_CH_EIB_1_SHUNT3_VOLT = 180,
	I2C_CTRL_CH_EIB_1_BATT_NUM = 181,
	I2C_CTRL_CH_EIB_1_VOLT_TYPE = 182,
	I2C_CTRL_CH_EIB_1_LOAD_NUM = 183,


	I2C_CTRL_CH_EIB_1_DO5 = 184,		
	I2C_CTRL_CH_EIB_MAX = 210,
};
#define	I2C_CHANNEL_EIB_SPACE		40     //ÿ��EIB�������ñ��з���Ĳɼ��ź�/�����źſռ�

/*ԭʼ�ɼ����� IB�� (IB1 or IB2) */
enum I2C_IB_ROUGH_DATA_IDX
{	
	I2C_IB_1_DI1 = 0,				// 0-OFF 1-ON
	I2C_IB_1_DI2,				// 0-OFF 1-ON
	I2C_IB_1_DI3,				// 0-OFF 1-ON
	I2C_IB_1_DI4,				// 0-OFF 1-ON
	I2C_IB_1_DI5,				// 0-OFF 1-ON
	I2C_IB_1_DI6,				// 0-OFF 1-ON
	I2C_IB_1_DI7,				// 0-OFF 1-ON
	I2C_IB_1_DI8,				// 0-OFF 1-ON
	I2C_IB_1_TEMPERATURE1,		//
	I2C_IB_1_TEMPERATURE2,		// 
	I2C_IB_1_STATE,				// 0-ON 1-OFF
	I2C_IB_1_FAILURE,			// 0-ON 1-OFF	
	I2C_IB_1_DEV_ADDR,          //��Ӧ��i2c�豸��ַ
	I2C_IB_1_EXIST,				//0-������ 1-����
	I2C_IB_1_TYPE,				// 0-IB1  1-IB2
	I2C_IB_1_COMMFAIL_CNT,  //ͨ���жϼ���
	I2C_IB_1_COMMFAIL_FLAG,  //ͨ���жϱ�־   0-�ɹ� 1-�ж�
	I2C_IB_1_SIG_MAX,	
};
/*ԭʼ�ɼ����� EIB��*/
enum I2C_EIB_ROUGH_DATA_IDX
{	
	I2C_EIB_1_VOLT1 = 0,	
	I2C_EIB_1_VOLT2,
	I2C_EIB_1_VOLT3,
	I2C_EIB_1_VOLT4,
	I2C_EIB_1_VOLT5,
	I2C_EIB_1_VOLT6,
	I2C_EIB_1_VOLT7,
	I2C_EIB_1_VOLT8,
	I2C_EIB_1_TEMPERATURE1,		//
	I2C_EIB_1_TEMPERATURE2,		//
	I2C_EIB_1_BLOCKNUM,  //BLOCK IN USE	
	I2C_EIB_1_CURRENT1,    
	I2C_EIB_1_CURRENT2,
	I2C_EIB_1_CURRENT3,
	I2C_EIB_1_LOADCURR1,	
	I2C_EIB_1_LOADCURR2,
	I2C_EIB_1_LOADCURR3,
	I2C_EIB_1_BATTCURR1,
	I2C_EIB_1_BATTCURR2,
	I2C_EIB_1_BATTCURR3,	
	I2C_EIB_1_BADBLOCKNUM,  //BAD BATTERY BLOCK 	
	I2C_EIB_1_STATE,		// 0-ON 1-OFF
	I2C_EIB_1_FAILURE,			// 0-ON 1-OFF	

	I2C_EIB_1_BATTSTATE1,
	I2C_EIB_1_BATTSTATE2,
	I2C_EIB_1_BATTSTATE3,

	I2C_EIB_ABSBATTCURR1,
	I2C_EIB_ABSBATTCURR2,
	I2C_EIB_ABSBATTCURR3,


	I2C_EIB_1_DEV_ADDR,		//��Ӧ��i2c�豸��ַ
	I2C_EIB_1_EXIST,   //0-NOT EXIST 1-EXIST
	I2C_EIB_1_COMMFAIL_CNT,  //ͨ���жϼ���
	I2C_EIB_1_COMMFAIL_FLAG,  //ͨ���жϱ�־   0-�ɹ� 1-�ж�
	I2C_EIB_1_SIG_MAX,	
};
/*ԭʼ�ɼ����� I2C�� */
enum I2C_GROUP_ROUGH_DATA_IDX
{
	I2C_GROUP_IB_NUM = 0,
	I2C_GROUP_IB_TYPE,				// 0-IB1 1-IB2 2-NA
	I2C_GROUP_IB_EXIST,			// 0-NOT EXIST   1-EXIST	

	I2C_GROUP_EIB_NUM,

	I2C_REAL_IB1_NUM,      //real exist num
	I2C_REAL_IB2_NUM,	   //real exist num	
	I2C_REAL_EIB_NUM,	   //real exist num
	I2C_GROUP_SIG_MAX,
};

#define	I2C_ROUGH_IB_MAX_NUM		4
#define	I2C_ROUGH_EIB_MAX_NUM		4

typedef struct 
{
	SAMPLING_VALUE			fRoughIBData[I2C_ROUGH_IB_MAX_NUM][I2C_IB_1_SIG_MAX];
	SAMPLING_VALUE			fRoughEIBData[I2C_ROUGH_EIB_MAX_NUM][I2C_EIB_1_SIG_MAX];
	SAMPLING_VALUE			fRoughGroupData[I2C_GROUP_SIG_MAX];
}_I2C_SAMP_DATA;

static _I2C_SAMP_DATA s_I2cSampleRoughData;



/*************************************************************************************************/
/*����ɼ�ģ�͵Ķ���*/
/*************************************************************************************************/

/*������֧�ֵĸ������뼰�䷵�����ݵĽ�����ʽ*****************************/

/*���������ö�ٶ���*/
enum I2C_COMMAND_CODE_DEF
{
	I2C_COMMAND_CODE_00 = 0x00,   //��ȡЭ��汾
	I2C_COMMAND_CODE_02 = 0x02,
	I2C_COMMAND_CODE_03 = 0x03,
	I2C_COMMAND_CODE_04 = 0x04,
	I2C_COMMAND_CODE_05 = 0x05,
	I2C_COMMAND_CODE_06 = 0x06,
};
/*����֧�ֵ�������ĸ����Ķ���*/
#define	IB1_CMD_NUM		1   //IB1��������
#define	IB2_CMD_NUM		1   //IB2��������
#define	EIB_CMD_NUM		3   //EIB��������

/*�豸�����������͵Ķ���*/
enum I2C_RETURN_DATA_SCALE_TYPE
{	
	I2C_SCALE_TYPE_NORMAL = 1, //1:1  ע��-ö������1��ʼ
	I2C_SCALE_TYPE_DI_BIT,    //λ���� bit->int
	I2C_SCALE_TYPE_MAX,
};
/*�豸�������ݽ�����ʽ�Ķ���*/
typedef struct
{
	int		nType;					// ��������: enum I2C_RETURN_DATA_SCALE_TYPE
	int		nOffset;				// ������ʼ��ַ���ڱ����ݰ��е���ʼλ��,��ͷ�ַ���ʼ��0��
	int		nScaleBit;				// ����DI-BIT����,��ֵ��Ӧ����λ�ţ��������ͣ���ֵ��Ӧ���Ǳ�����ϵ,��1��ʾ�Ŵ�1���ȡ�  
	int     nRoughDataNo;           // ��Ӧ��Rough�����е�λ��
}I2C_RETURN_DATA_INFO;

static I2C_RETURN_DATA_INFO s_ReturnDataFormat_IB1_CMD_02[] = 
{
	{I2C_SCALE_TYPE_DI_BIT, 0, 0, I2C_IB_1_DI1},
	{I2C_SCALE_TYPE_DI_BIT, 0, 1, I2C_IB_1_DI2},
	{I2C_SCALE_TYPE_DI_BIT, 0, 2, I2C_IB_1_DI3},
	{I2C_SCALE_TYPE_DI_BIT, 0, 3, I2C_IB_1_DI4},
	{0, 0, 0, 0},
};

static I2C_RETURN_DATA_INFO s_ReturnDataFormat_IB2_CMD_02[] = 
{
	{I2C_SCALE_TYPE_DI_BIT, 0, 0, I2C_IB_1_DI1},
	{I2C_SCALE_TYPE_DI_BIT, 0, 1, I2C_IB_1_DI2},
	{I2C_SCALE_TYPE_DI_BIT, 0, 2, I2C_IB_1_DI3},
	{I2C_SCALE_TYPE_DI_BIT, 0, 3, I2C_IB_1_DI4},
	{I2C_SCALE_TYPE_DI_BIT, 0, 4, I2C_IB_1_DI5},
	{I2C_SCALE_TYPE_DI_BIT, 0, 5, I2C_IB_1_DI6},
	{I2C_SCALE_TYPE_DI_BIT, 0, 6, I2C_IB_1_DI7},
	{I2C_SCALE_TYPE_DI_BIT, 0, 7, I2C_IB_1_DI8},
	{I2C_SCALE_TYPE_NORMAL, 1, 1, I2C_IB_1_TEMPERATURE1},
	{I2C_SCALE_TYPE_NORMAL, 5, 1, I2C_IB_1_TEMPERATURE2},
	{0, 0, 0, 0},
};

static I2C_RETURN_DATA_INFO s_ReturnDataFormat_EIB_CMD_04[] = 
{
	{I2C_SCALE_TYPE_NORMAL, 0, 1, I2C_EIB_1_VOLT1},
	{I2C_SCALE_TYPE_NORMAL, 4, 1, I2C_EIB_1_VOLT2},
	{I2C_SCALE_TYPE_NORMAL, 8, 1, I2C_EIB_1_VOLT3},
	{I2C_SCALE_TYPE_NORMAL, 12, 1, I2C_EIB_1_VOLT4},
	{I2C_SCALE_TYPE_NORMAL, 16, 1, I2C_EIB_1_VOLT5},
	{I2C_SCALE_TYPE_NORMAL, 20, 1, I2C_EIB_1_VOLT6},
	{I2C_SCALE_TYPE_NORMAL, 24, 1, I2C_EIB_1_VOLT7},
	{I2C_SCALE_TYPE_NORMAL, 28, 1, I2C_EIB_1_VOLT8},
	{0, 0, 0, 0},
};
static I2C_RETURN_DATA_INFO s_ReturnDataFormat_EIB_CMD_05[] = 
{
	{I2C_SCALE_TYPE_NORMAL, 0, 1, I2C_EIB_1_CURRENT1},
	{I2C_SCALE_TYPE_NORMAL, 4, 1, I2C_EIB_1_CURRENT2},
	{I2C_SCALE_TYPE_NORMAL, 8, 1, I2C_EIB_1_CURRENT3},
	{0, 0, 0, 0},
};
static I2C_RETURN_DATA_INFO s_ReturnDataFormat_EIB_CMD_06[] = 
{
	{I2C_SCALE_TYPE_NORMAL, 0, 1, I2C_EIB_1_TEMPERATURE1},
	{I2C_SCALE_TYPE_NORMAL, 4, 1, I2C_EIB_1_TEMPERATURE2},
	{0, 0, 0, 0},
};

/*����������뼰�䷵�����ݳ��Ⱥͽ�����ʽ�Ķ���*/
typedef struct
{
	int nCommandCode;
	BYTE nReturnByteNum;
	I2C_RETURN_DATA_INFO *pReturnDataInfo;
}I2C_COMMAND_DATA_INFO;

/*IB1 ����֧�ֵ�������Ͷ�Ӧ�������ݽ�����ʽ */
static I2C_COMMAND_DATA_INFO s_CommandAndReturnData_IB1[IB1_CMD_NUM] =
{
	{I2C_COMMAND_CODE_02, 1, s_ReturnDataFormat_IB1_CMD_02},
};
/*IB2 ����֧�ֵ�������Ͷ�Ӧ�������ݽ�����ʽ */
static I2C_COMMAND_DATA_INFO s_CommandAndReturnData_IB2[IB2_CMD_NUM] =
{
	{I2C_COMMAND_CODE_02, 9, s_ReturnDataFormat_IB2_CMD_02},
};
/*EIB ����֧�ֵ�������Ͷ�Ӧ�������ݽ�����ʽ */
static I2C_COMMAND_DATA_INFO s_CommandAndReturnData_EIB[EIB_CMD_NUM] =
{
	{I2C_COMMAND_CODE_04, 32, s_ReturnDataFormat_EIB_CMD_04},
	{I2C_COMMAND_CODE_05, 12, s_ReturnDataFormat_EIB_CMD_05},
	{I2C_COMMAND_CODE_06, 8, s_ReturnDataFormat_EIB_CMD_06},
};

/*����ɼ�ģ�͵Ķ���*************************************************************/

/*�������Ͷ���*/
enum I2C_DEVICE_TYPE
{
	I2C_DEVICE_TYPE_IB1 = 0,
	I2C_DEVICE_TYPE_IB2,
	I2C_DEVICE_TYPE_NA,  //Ϊ���� enum I2C_IB_TYPE_DEFINE ����
	I2C_DEVICE_TYPE_EIB,	
	I2C_DEVICE_TYPE_MAX,
};

/*���ֵ�����ȡ���е�����������*/
#define I2C_MAX(a, b) (((a) > (b)) ? (a) : (b))
#define I2C_CMD_NUM_MAX1  I2C_MAX(IB1_CMD_NUM, IB2_CMD_NUM)
#define I2C_CMD_NUM_MAX  I2C_MAX(I2C_CMD_NUM_MAX1, EIB_CMD_NUM)   //ȡ���������

/*�ɼ�ģ�Ͷ���*/
typedef struct
{
	BYTE nDeviceType;
	BYTE nStartAddr;
	BYTE nCommandNum;
	I2C_COMMAND_DATA_INFO *pCommandAndReturnData;
}I2C_SAMPLE_MODEL;

/*IB1 ��ɼ�ģ�� */
static I2C_SAMPLE_MODEL s_SampleModel_IB1 =
{
	I2C_DEVICE_TYPE_IB1, 	
	0x10,
	IB1_CMD_NUM,
	s_CommandAndReturnData_IB1,
};
/*IB2 ��ɼ�ģ�� */
static I2C_SAMPLE_MODEL s_SampleModel_IB2 =
{
	I2C_DEVICE_TYPE_IB2, 	
	0x18,
	IB2_CMD_NUM,
	s_CommandAndReturnData_IB2,
};
/*EIB ��ɼ�ģ�� */
static I2C_SAMPLE_MODEL s_SampleModel_EIB =
{
	I2C_DEVICE_TYPE_EIB, 	
	0x20,
	EIB_CMD_NUM,
	s_CommandAndReturnData_EIB,
};

/*ͬ���豸�ĵ�ַ���*/
#define		I2C_DEVICE_ADDR_STEP 			2    
/*���ְ������������� */
#define	MAX_IB1_NUM   4
#define	MAX_IB2_NUM   4
#define	MAX_EIB_NUM   4


/*************************************************************************************************/
/*************************************************************************************************/

typedef struct
{
	BYTE	byStatusIBRelay[IB_NUM_SOLUTION];
	BYTE	byStatusEIBRelay[EIB_NUM_SOLUTION];
	union STRTOFLOAT BatShuntPara[EIB_NUM_SOLUTION][6];
	int     BattNum[EIB_NUM_SOLUTION];
	int     VoltageType[EIB_NUM_SOLUTION];
	int     LoadNum[EIB_NUM_SOLUTION];
	BYTE	bNeedScanBoards;
}DATASTRUCT_CONTROLL;

static DATASTRUCT_CONTROLL s_I2cControllStatus;


static	BOOL			g_bDriverIsOpen = FALSE;				// indicate the driver opened or not
static	int			g_iI2Cfd = -1;							// handle of I2C equip
static	int			g_iSynTimer = 0;
static	BOOL			g_LVDAlarmOrNot = FALSE;



/*************************************************************************************************/
/*  OB��*/
/*************************************************************************************************/

#define EQUIP_ID_OBLVD				187
#define	OBLVD_SETTING_ID_CONTACTORTYPE  	14

enum _OBLVD_CONTACT_TYPE_IDX
{
    OBLVDTONTACTTYPE_BISTABLE = 0,    //˫��
    OBLVDTONTACTTYPE_STABLE,	    //����
    OBLVDTONTACTTYPE_STABLEWITHSAMPLE,  //���ȴ��ز�
};

static int s_OBLVDContactType = OBLVDTONTACTTYPE_BISTABLE;   //�Ӵ������� 0-˫�� 1-����  2-���ȴ��ز�


static	int CalCheckSum(BYTE nCommand, BYTE nByteCount, BYTE *pReiveData);
 static	BOOL SetDev(int nAddr, int nCmdNo, int nData);

//added by Jimmy for SMDU-EIB CR 2013.07.06
static BOOL g_bIsSMDUEIBMode = FALSE;
static BOOL Is_SMDU_EIBMode(void);
static BOOL Is_SMDU_EIBModeValid();
static void ReDispatchShuntReadings(int EIBNO);
static float g_fTotalEIBLoads[4];
static BOOL	bSignalSMDUEIBModeValid = TRUE;

static BOOL Is_SMDU_EIBMode()
{
#define SYS_GROUP_EQUIPID 1
	if(GetEnumSigValue(SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		452,
		"CAN_SAMP") > 0)
	{
		return TRUE;
	}
	return FALSE;
}

static void ReDispatchShuntReadings(int EIBNO)
{
	//printf("\n---------Entering EIB-i2c ReDispatching--------------\n");
	int iBattNum = 0;
#define EQUIP_ID_EIB1	197
#define SIG_ID_BATTNUM 7
#define SIG_ID_LOADNUM 8
#define SIG_ID_SHUNT1_SET_AS 16
	//��SMDU-EIBģʽ��ǿ�ưѸ�������Ϊ��Ϊ0
	if(GetDwordSigValue(EQUIP_ID_EIB1 + EIBNO, 
		SIG_TYPE_SETTING,
		SIG_ID_LOADNUM,
		"I2C_SAMP") != 0)
	{
		SetDwordSigValue(EQUIP_ID_EIB1 + EIBNO, 
			SIG_TYPE_SETTING,
			SIG_ID_LOADNUM,
			0,
			"I2C_SAMP");
	}
	int i = 0;	
	SAMPLING_VALUE		NoValid;
	SAMPLING_VALUE  Digit_1,Digit_0;
	Digit_1.iValue	=    1;
	Digit_0.iValue	=    0;
	NoValid.iValue = -9999;
	SIG_ENUM		enumValue = 0;

	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR1].fValue = NoValid.fValue;
	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR2].fValue = NoValid.fValue;
	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR3].fValue = NoValid.fValue;

	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR1].fValue = NoValid.fValue;
	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR2].fValue = NoValid.fValue;
	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR3].fValue = NoValid.fValue;

	//battery number is 0, set the battery work status channel to NOT
	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE1].fValue = Digit_1.fValue;
	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE2].fValue = Digit_1.fValue;
	s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE3].fValue = Digit_1.fValue;


	for(i =0;i < 3;i++)
	{
		enumValue = GetEnumSigValue(EQUIP_ID_EIB1 + EIBNO, 
				SIG_TYPE_SETTING,
				SIG_ID_SHUNT1_SET_AS + i,
				"I2C_SAMP");
		if(enumValue == 1)//general
		{
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR1 + i].fValue 
				= s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1 + i].fValue;
			//do nothing
		}
		else if(enumValue == 2)//����
		{
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR1 + i].fValue 
				= s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1 + i].fValue;
			g_fTotalEIBLoads[EIBNO] += s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR1 + i].fValue;//�����ܵ���
		}
		else if(enumValue == 3)//���
		{
			//ע����������˳���ŷŵ�
			/*s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE1 + iBattNum].fValue = Digit_0.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR1 + iBattNum].fValue
				= s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1 + i].fValue;;*/
			//Change to not sequence, just as the shunt sequence.
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE1 + i].fValue = Digit_0.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR1 + i].fValue
				= s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1 + i].fValue;

			iBattNum++;
		}
	}
	if(GetDwordSigValue(EQUIP_ID_EIB1 + EIBNO, 
		SIG_TYPE_SETTING,
		SIG_ID_BATTNUM,
		"I2C_SAMP") != iBattNum)
		{
		SetDwordSigValue(EQUIP_ID_EIB1 + EIBNO, 
					SIG_TYPE_SETTING,
					SIG_ID_BATTNUM,
					iBattNum,
					"I2C_SAMP");
		}

}

/*==========================================================================*
* FUNCTION : CalCheckSum
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  nCommand    : 
*            BYTE  nByteCount  : 
*            BYTE  *pReiveData : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-13 14:40
*==========================================================================*/
static	int CalCheckSum(BYTE nCommand, BYTE nByteCount, BYTE *pReiveData)
{
	WORD sum,check;
	BYTE i;

	sum = nCommand;
	sum += nByteCount;
	for(i = 0; i < nByteCount; i++)
	{
		sum += pReiveData[i];
	}
	check = ((WORD)pReiveData[nByteCount + 1] << 8) + (WORD)pReiveData[nByteCount];
	//TRACE("check = %2x  sum= %2x\n",check,sum);
	sum = ~sum + 1;
	//TRACE("check = %2x  sum= %2x\n",check,sum); 
	if(check == sum)
		return TRUE;
	else
		return FALSE;
}
/*==========================================================================*
* FUNCTION : OpenI2CDriver
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-13 14:46
*==========================================================================*/
static	int OpenI2CDriver( void )
{
	//TRACE("==========================NOW enter the OpenI2CDriver function of I2C world======================\n");
	//open physical device
	g_iI2Cfd = open( I2C_DEVICE_NAME, O_RDWR);
	if( g_iI2Cfd < 0 )
	{
		//printf(" open I2C device error! \n");
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to open I2C device!\n");
		g_bDriverIsOpen = FALSE;
		return FALSE;
	}
	g_bDriverIsOpen = TRUE;

	return TRUE;
}
/*==========================================================================*
* FUNCTION : DestoryI2Cdev
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   :  
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-13 14:41
*==========================================================================*/
/*static	void DestoryI2Cdev(void)
{
	ioctl(g_iI2Cfd, 0, 0);
	close( g_iI2Cfd );
	g_iI2Cfd = -1;
	g_bDriverIsOpen = FALSE;	
}*/
/*==========================================================================*
* FUNCTION : ChageOBACData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    EIBNO  : 
*            float  *pData : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-10-28 15:50
*==========================================================================*/
#define ResetI2C							0x800			// reset the driver of I2C
static	void vResetI2C(void)
{
	if( !g_bDriverIsOpen ) 
		OpenI2CDriver();

	ioctl(g_iI2Cfd, ResetI2C, 0);
	close(g_iI2Cfd);
	g_iI2Cfd = -1;
	g_bDriverIsOpen = FALSE;

}
/*==========================================================================*
* FUNCTION : GetI2CData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  nAddr      : 
*            BYTE  nCommand   : 
*            BYTE  *pBuffer   : 
*            BYTE  nByteCount : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-13 14:41
*==========================================================================*/
//#define I2C_SLAVE_G3						(0x0703)
#define I2C_SLAVE_G3						(0x0706)
static	int GetI2CData( BYTE nAddr, BYTE nCommand, BYTE *pBuffer, BYTE nByteCount )
{
	// to be coded

	int nRet = 0;
	int nStatusCheck = 0;
	//int iI2Cfd = -1;

	if( !g_bDriverIsOpen ) 
	{	
		if(!OpenI2CDriver()) 
		{
			return FALSE;
		}			
	}	
	
	ioctl( g_iI2Cfd, I2C_SLAVE_G3, nAddr);				// transfer nAddr to driver

	pBuffer[0] = nByteCount;						// transfer nByteCount to driver
	pBuffer[1] = nCommand;							// transfer nCommand to driver

	nRet = read( g_iI2Cfd, pBuffer, (nByteCount+2));


	//return length should be equal to nByteCount+2(check number)
	if( nRet == 2 )
	{
		nStatusCheck = CalCheckSum( (BYTE)nCommand, (BYTE)nByteCount, pBuffer);

		if( nStatusCheck )
		{
			return TRUE;			
		}
		else
		{
			//close the handle of I2C device
			//DestoryI2Cdev();
			return FALSE;			
		}
	}
	else
	{
		//close the handle of I2C device
		//DestoryI2Cdev();
		return FALSE;		
	}
}
static BOOL Is_SMDU_EIBModeValid()
{
	SIG_BASIC_VALUE*	pSigValue;
	int			iRst;
	int			iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					SYS_GROUP_EQUIPID,
					DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, 452),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		return FALSE;
	}	
	
	return TRUE;
}
/*==========================================================================*
* FUNCTION : ClearCommData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Kane.Lian                 DATE: 2013-04-25 
*==========================================================================*/
static void ClearCommData(void)
{
	BYTE i;

	I2C_VALUE I2cValueNone;

	I2cValueNone.iValue = -9999;


	for(i = 0; i < I2C_ROUGH_IB_MAX_NUM; i++)
	{

		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DI1].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DI2].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DI3].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DI4].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DI5].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DI6].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DI7].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DI8].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_TEMPERATURE1].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_TEMPERATURE2].fValue = I2cValueNone.fValue;

		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DEV_ADDR].iValue = 0x00;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_TYPE].iValue = IB_TYPE_NA;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_EXIST].iValue = FALSE;

		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_CNT].iValue = 0;
		s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_FLAG].iValue = 0;




	}

	for(i = 0; i < I2C_ROUGH_EIB_MAX_NUM; i++)
	{
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_VOLT1].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_VOLT2].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_VOLT3].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_VOLT4].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_VOLT5].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_VOLT6].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_VOLT7].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_VOLT8].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_BLOCKNUM].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_CURRENT1].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_CURRENT2].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_CURRENT3].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_LOADCURR1].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_LOADCURR2].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_LOADCURR3].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_BATTCURR1].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_BATTCURR2].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_BATTCURR3].fValue = I2cValueNone.fValue;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_BADBLOCKNUM].fValue = I2cValueNone.fValue;


		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_DEV_ADDR].iValue = 0x00;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_EXIST].iValue = FALSE;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_COMMFAIL_CNT].iValue = 0;
		s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_COMMFAIL_FLAG].iValue = 0;
	}



	s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_NUM].iValue = 0;
	s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_EIB_NUM].iValue = 0;
	s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_TYPE].iValue = IB_TYPE_NA;  //�鸳ֵ��Ҫ�� enum I2C_IB_TYPE_DEFINE �ж����ֵ
	s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_EXIST].iValue = FALSE;

	s_I2cSampleRoughData.fRoughGroupData[I2C_REAL_IB1_NUM].iValue = 0;
	s_I2cSampleRoughData.fRoughGroupData[I2C_REAL_IB2_NUM].iValue = 0;
	s_I2cSampleRoughData.fRoughGroupData[I2C_REAL_EIB_NUM].iValue = 0;
}

/*==========================================================================*
* FUNCTION : ScanI2CBoards
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float  *pData : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Kane.Lian                 DATE: 2013-04-25 
*==========================================================================*/
#define		I2C_GET_PROTOCOL_VER_CMD  I2C_COMMAND_CODE_00						//��ȡi2c�豸��Э��汾��������



static	void ScanI2CBoards( void )
{
	BYTE i;
	BYTE				Frame[I2C_BUF_NUM]={0};
	int					nRet = 0;
	BYTE	byIBIndex = 0;
	BYTE	byEIBIndex = 0;

	ClearCommData();  //������I2C���ڱ�־


	/*ɨ��IB2��*/	
	for(i = 0; i < MAX_IB2_NUM; i++)
	{
		nRet = GetI2CData((BYTE)(s_SampleModel_IB2.nStartAddr + i * I2C_DEVICE_ADDR_STEP), 
			(BYTE)(I2C_GET_PROTOCOL_VER_CMD), Frame, (BYTE)(1) );

		if (nRet)
		{			
			if(Frame[0])
			{
				s_I2cSampleRoughData.fRoughGroupData[I2C_REAL_IB2_NUM].iValue++;

				if(byIBIndex < I2C_ROUGH_IB_MAX_NUM)
				{
					s_I2cSampleRoughData.fRoughIBData[byIBIndex][I2C_IB_1_DEV_ADDR].iValue = s_SampleModel_IB2.nStartAddr + i * I2C_DEVICE_ADDR_STEP;
					s_I2cSampleRoughData.fRoughIBData[byIBIndex][I2C_IB_1_TYPE].iValue = I2C_DEVICE_TYPE_IB2;
					s_I2cSampleRoughData.fRoughIBData[byIBIndex][I2C_IB_1_EXIST].iValue = TRUE;
					byIBIndex++;
				}	

				s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_TYPE].iValue = IB_TYPE_IB2; //�鸳ֵ��Ҫ�� enum I2C_IB_TYPE_DEFINE �ж����ֵ
			}
		}

		//TRACE("---------s_CommData_IB2[%d].ExistFlag = %d ------------\n ", i, s_CommData_IB2[i].ExistFlag);
	}

	Sleep(100);

	/*ɨ��IB1��*/
	for(i = 0; i < MAX_IB1_NUM; i++)
	{
		nRet = GetI2CData((BYTE)(s_SampleModel_IB1.nStartAddr + i * I2C_DEVICE_ADDR_STEP), 
			(BYTE)(I2C_GET_PROTOCOL_VER_CMD), Frame, (BYTE)(1) );

		if (nRet)
		{			
			if(Frame[0])
			{
				s_I2cSampleRoughData.fRoughGroupData[I2C_REAL_IB1_NUM].iValue++;

				if(byIBIndex < I2C_ROUGH_IB_MAX_NUM)
				{
					s_I2cSampleRoughData.fRoughIBData[byIBIndex][I2C_IB_1_DEV_ADDR].iValue = s_SampleModel_IB1.nStartAddr + i * I2C_DEVICE_ADDR_STEP;
					s_I2cSampleRoughData.fRoughIBData[byIBIndex][I2C_IB_1_TYPE].iValue = I2C_DEVICE_TYPE_IB1;
					s_I2cSampleRoughData.fRoughIBData[byIBIndex][I2C_IB_1_EXIST].iValue = TRUE;
					byIBIndex++;
				}

				//�鸳ֵ��Ҫ�� enum I2C_IB_TYPE_DEFINE �ж����ֵ
				if(s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_TYPE].iValue == IB_TYPE_NA)
				{
					s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_TYPE].iValue = IB_TYPE_IB1;
				}
			}
		}
	}

	s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_NUM].iValue = byIBIndex;		

	if(byIBIndex > 0)
	{
		s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_EXIST].iValue = TRUE;
	}


	Sleep(100);


	/*ɨ��EIB�� */
	for(i = 0; i < MAX_EIB_NUM; i++)
	{
		nRet = GetI2CData((BYTE)(s_SampleModel_EIB.nStartAddr + i * I2C_DEVICE_ADDR_STEP), 
			(BYTE)(I2C_GET_PROTOCOL_VER_CMD), Frame, (BYTE)(1) );
		/*nRet = GetI2CData((BYTE)(2), (BYTE)(0), Frame, (BYTE)(1) );*/

		if (nRet)
		{			
			if(Frame[0])
			{
				s_I2cSampleRoughData.fRoughGroupData[I2C_REAL_EIB_NUM].iValue++;

				if(byEIBIndex < I2C_ROUGH_EIB_MAX_NUM)
				{
					s_I2cSampleRoughData.fRoughEIBData[byEIBIndex][I2C_EIB_1_DEV_ADDR].iValue = s_SampleModel_EIB.nStartAddr + i * I2C_DEVICE_ADDR_STEP;
					s_I2cSampleRoughData.fRoughEIBData[byEIBIndex][I2C_EIB_1_EXIST].iValue = TRUE;
					byEIBIndex++;
				}
			}
		}				

		//TRACE("---------s_CommData_EIB[%d].ExistFlag = %d ------------\n ", i, s_CommData_EIB[i].ExistFlag);
	}

	s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_EIB_NUM].iValue = byEIBIndex;

	
	Sleep(100);


}

/*==========================================================================*
* FUNCTION : ReadEIBSetSignal
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  EIBNO : 0-EIB1 1-EIB2 ....
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-10-28 14:16
*==========================================================================*/


static	void ReadEIBSetSignal(int EIBNO)
{
	int					nVarID = 0;				// Equipment's id
	int					nVarSubID = 0;			// setting signal id
	SIG_BASIC_VALUE		*pSigValue;
	int					iInterfaceType = VAR_A_SIGNAL_VALUE;
	int					iError = ERR_DXI_OK;
	int					nTimeOut = 0;
	int					nBufLen;

	if(EIBNO >= MAX_EIB_NUM)
	{
		return;
	}

	nVarID = EQUIP_IP_EIB1 + EIBNO;

	// read setting (signal voltage type) of EIB
	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_VOLTAGE_TYPE);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_VoltageType[EIBNO] = pSigValue->varValue.enumValue;
		s_I2cControllStatus.VoltageType[EIBNO] = pSigValue->varValue.enumValue;
	}
	else 
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB Voltage Type!\n");
	}

	// read setting signal (battery number) of EIB
	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_BATT_NUM);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_BattNum[EIBNO] = pSigValue->varValue.enumValue;
		s_I2cControllStatus.BattNum[EIBNO] = pSigValue->varValue.enumValue;
	}
	else 
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB battery number!\n");
	}

	// read setting signal (loader number) of EIB
	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_LOAD_NUM);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_LoadNum[EIBNO] = pSigValue->varValue.enumValue;
		s_I2cControllStatus.LoadNum[EIBNO] = pSigValue->varValue.enumValue;
	}
	else 
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB loader number!\n");
	}

}
/*==========================================================================*
* FUNCTION : ReadShuntPara
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  EIBNO : 0-EIB1 1-EIB2 ....
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-10-28 13:36
*==========================================================================*/

static	void ReadShuntPara(int EIBNO)
{
	int					nVarID = 0;				// Equipment's id
	int					nVarSubID = 0;			// setting signal id
	SIG_BASIC_VALUE		*pSigValue;
	int					iInterfaceType = VAR_A_SIGNAL_VALUE;
	int					iError = ERR_DXI_OK;
	int					nTimeOut = 0;
	int					nBufLen;

	if(EIBNO >= MAX_EIB_NUM)
	{
		return;
	}

	nVarID = EQUIP_IP_EIB1 + EIBNO;

	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_SHUNT1_CURRENT);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_BatShuntPara[EIBNO*6+0].f_value = pSigValue->varValue.fValue;
		s_I2cControllStatus.BatShuntPara[EIBNO][0].f_value = pSigValue->varValue.fValue;
	}
	else
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB Shunt1 Parameter!\n");
	}

	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_SHUNT1_VOLT);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_BatShuntPara[EIBNO*6+1].f_value = pSigValue->varValue.fValue;
		s_I2cControllStatus.BatShuntPara[EIBNO][1].f_value = pSigValue->varValue.fValue;
	}
	else
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB Shunt1 Parameter!\n");
	}

	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_SHUNT2_CURRENT);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_BatShuntPara[EIBNO*6+2].f_value = pSigValue->varValue.fValue;
		s_I2cControllStatus.BatShuntPara[EIBNO][2].f_value = pSigValue->varValue.fValue;
	}
	else
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB Shunt2 Parameter!\n");
	}

	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_SHUNT2_VOLT);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_BatShuntPara[EIBNO*6+3].f_value = pSigValue->varValue.fValue;
		s_I2cControllStatus.BatShuntPara[EIBNO][3].f_value = pSigValue->varValue.fValue;
	}
	else 
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB Shunt2 Parameter!\n");
	}

	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_SHUNT3_CURRENT);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_BatShuntPara[EIBNO*6+4].f_value = pSigValue->varValue.fValue;
		s_I2cControllStatus.BatShuntPara[EIBNO][4].f_value = pSigValue->varValue.fValue;
	}
	else 
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB Shunt3 Parameter!\n");
	}

	nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_SHUNT3_VOLT);
	iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
	if( iError == ERR_DXI_OK )
	{
		//g_BatShuntPara[EIBNO*6+5].f_value = pSigValue->varValue.fValue;
		s_I2cControllStatus.BatShuntPara[EIBNO][5].f_value = pSigValue->varValue.fValue;
	}
	else 
	{
		I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB Shunt3 Parameter!\n");
	}
}

// ������8���ַ��ĸ�������ʽ�ַ���ת���ɸ�����
/*==========================================================================*
* FUNCTION : FixFloatDat
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char*  sStr : 
* RETURN   : float : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-15 14:55
*==========================================================================*/
static	float FixFloatDat( char* sStr )
{
	int i;
	union STRTOFLOAT floatvalue;

	// ����δ���岿�����ݣ����豸����Ϊ�ո񣬱�ʾ�źŲ����ڡ�ǰ�û��п��Խ����ź�ɾ��
	/*if( sStr[0]==0x20 )
	return -9999.0f;*/

	for( i=0; i<4; i++ )
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
		//TRACE("------------------------1----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 
			&floatvalue.by_value[3-i]);
#else
		int	c;
		//TRACE("------------------------2----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 	&c);
		floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//TRACE("------------------------3-----%x-----------------------\n",floatvalue.by_value[i]);
		//floatvalue.by_value[3-i] = sStr[i];
		floatvalue.by_value[i] = sStr[i];
		//sscanf((const char *)sStr+i, "%x", &floatvalue.by_value[i]);

#endif
	}

	return floatvalue.f_value;
}

/*==========================================================================*
* FUNCTION : Fix_Data
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   :  
* COMMENTS : 
* CREATOR  : kane.lian                 DATE: 2013-04-26
*==========================================================================*/
static	void Fix_Data( int nUnitNo, BYTE nDeviceType, BYTE *Frame, I2C_COMMAND_DATA_INFO * pCommandDataInfo)
{
	BYTE byTemp;
	float ftemp;

	SAMPLING_VALUE	NoValid;
	SAMPLING_VALUE  Digit_1,Digit_0;
	SAMPLING_VALUE	Digit_2;

	I2C_RETURN_DATA_INFO *pReturnData;

	Digit_1.iValue	=     1;
	Digit_0.iValue  =    0;
	Digit_2.iValue	=    2;
	NoValid.iValue = -9999;

	pReturnData = &pCommandDataInfo->pReturnDataInfo[0];
	
	while(pReturnData->nType != 0)
	{
		ftemp = NoValid.fValue;

		switch(pReturnData->nType)
		{
		case I2C_SCALE_TYPE_DI_BIT:	
			byTemp = (0x01 << pReturnData->nScaleBit);
			if(Frame[pReturnData->nOffset] & byTemp)
			{
				ftemp = Digit_1.fValue;	
			}
			else
			{
				ftemp = Digit_0.fValue;	
			}

			break;

		case I2C_SCALE_TYPE_NORMAL:
			ftemp = FixFloatDat( (char *)Frame+pReturnData->nOffset);
			break;

		default:
			break;

		}


		if((nDeviceType == I2C_DEVICE_TYPE_IB1) 
			|| (nDeviceType == I2C_DEVICE_TYPE_IB2))
		{
			s_I2cSampleRoughData.fRoughIBData[nUnitNo][pReturnData->nRoughDataNo].fValue = ftemp;
		}
		else if(nDeviceType == I2C_DEVICE_TYPE_EIB) 
		{
			s_I2cSampleRoughData.fRoughEIBData[nUnitNo][pReturnData->nRoughDataNo].fValue = ftemp;	
		}


		pReturnData++;

	} //END: while

}

/*==========================================================================*
* FUNCTION : fGetMaxMinDiff
* PURPOSE  : 
* CALLS    : 
* CALLED BY: RepairEIBData
* ARGUMENTS: float * fSource   : 
*            int iLen	       : 

* RETURN   : float  Max - Min: 
* COMMENTS : Modified by Jimmy as for TR of EIB bad battery block alarm issue
* CREATOR  : 
*==========================================================================*/
static	float fGetMaxMinDiff(float * fSource, float fBase,int iLen,int iStart, int iLenOfC)
{
	float	fMaxValue = 0.0;
	float	fMinValue = 9999.9;
	float	fTemp = 0.0;
	int	i;
	if(iStart >= iLen)
	{
		return 0.0;
	}
	for(i=iStart;i<iStart + iLenOfC;i++) 
	{
		fTemp = ABS(fSource[i]);//use abs to campare, in case system volt is +- negative by Jimmy 2011-09-29
		if(fMaxValue < fTemp)
		{
			fMaxValue = fTemp;
		}
		if(fMinValue > fTemp)
		{
			fMinValue = fTemp;
		}
	}
	fTemp = ABS(fBase);//use abs to campare, in case system volt is +- negative by Jimmy 2011-09-29
	if(ABS(fTemp - fMaxValue) > 2.0 || ABS(fTemp - fMinValue) > 2.0) //if fBase is lagger than block volt, then ignore it.
	{		
		if(fMaxValue < fTemp)
		{
			fMaxValue = fTemp;
		}
		if(fMinValue > fTemp)
		{
			fMinValue = fTemp;
		}
		return fMaxValue - fMinValue;		
	}
	else
	{
		return fMaxValue - fMinValue;
	}

}



/*==========================================================================*
* FUNCTION : RepairEIBData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    EIBNO  : 
*            float  *pData : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-10-28 15:50
*==========================================================================*/


static	void RepairEIBData(int EIBNO, BYTE nCommandCode)
{
	//int					i = 0;
	int					j = 0;
	int					k = 0;

	int					iError = 0;
	int					nVarID = 0;			//Equip id
	int					nVarSubID = 0;			//setting signal id
	int					nBufLen;
	int					nTimeOut = 0;

	float				Voltage[4] = {0.0};
	float				Vol_Diff[4] = {0.0};
	float				Vol_tmp[8] ={0.0};
	float				Voltage1[4] = {0.0};
	//New mode fo declearing battery block if failure or well 
	float				Vol_Diff1[4] = {0.0};
	float				Mother_Volt = 0.0; 
	int					EquipID[4] = { EQUIP_IP_EIB1, EQUIP_IP_EIB2, EQUIP_IP_EIB3, EQUIP_IP_EIB4};
	UINT				uBlockUsed	= 8; //added by Jimmy to tell the used block num 

	float				tempS = 0.0;
	int					tempD = 0;

	int					iInterfaceType = VAR_A_SIGNAL_VALUE;
	
	SAMPLING_VALUE				NoValid;
	SIG_BASIC_VALUE				*pSigValue;

	SAMPLING_VALUE  Digit_1,Digit_0;

#if	_I2C_SAMPLER_DEBUG
	static int s_test1 = 0;
#endif

	Digit_1.iValue	=    1;
	Digit_0.iValue  =    0;

	NoValid.iValue = -9999;

	/*1. Block Voltage ����*/
	if(nCommandCode == I2C_COMMAND_CODE_04)  //For Voltage
	{
		// Get Equip Battery1 voltage
		nVarID = EQUIP_IP_BATTERY1;    
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, BATTERY_SAMPLING_ID_VOLTAGE);
		iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
		if( iError == ERR_DXI_OK )
		{
			Voltage[EIBNO] = pSigValue->varValue.fValue;
		}
		else
		{
			I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB battery Voltage!\n");
		}

		//Get Equip  Power system voltage
		nVarID = EQUIP_IP_SYSTEM;		
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, SYSTEM_SAMPLING_ID_VOLTAGE);
		iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
		if( iError == ERR_DXI_OK )
		{
			Mother_Volt = pSigValue->varValue.fValue;
		}
		else
		{
			I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read System Voltage for EIB!\n");	
		}

		// Get Equip Battery2 voltage
		nVarID = EQUIP_IP_BATTERY2;		
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, BATTERY_SAMPLING_ID_VOLTAGE);
		iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
		if( iError == ERR_DXI_OK )
		{
			Voltage1[EIBNO] = pSigValue->varValue.fValue;
		}
		else
		{
			I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB battery Voltage!\n");
		}

		// Get Equip EIB Block voltage differenc
		nVarID = EquipID[EIBNO];
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_BLOCK_DIFF);
		iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
		if( iError == ERR_DXI_OK )
		{
			Vol_Diff[EIBNO] = pSigValue->varValue.fValue;			
		}
		else
		{
			I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB battery difference Voltage!\n");
		}

		// Get Equip EIB BLOCK USE IN NUM
		nVarID = EquipID[EIBNO];
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_BLOCK_USENUM);
		iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
		if( iError == ERR_DXI_OK )
		{			
			uBlockUsed = pSigValue->varValue.ulValue;		
			if(uBlockUsed < 0 || uBlockUsed > 8)
			{
				uBlockUsed = 8;				
			}
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BLOCKNUM].iValue = uBlockUsed;	
		}
		else
		{
			I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read used block num!\n");		
		}


		// Get Equip EIB Block voltage differenc for Mid mode
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, EIB_SETTING_ID_BLOCK_DIFF_MID);	
		iError = DxiGetData(iInterfaceType, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
		if( iError == ERR_DXI_OK )
		{
			Vol_Diff1[EIBNO] = pSigValue->varValue.fValue;
		}
		else 
		{
			I2C_LOG_OUT(APP_LOG_WARNING, "Failed to read EIB battery difference Voltage!\n");
		}
		//Clear the battery voltage when the voltages of  the battery1 and battery2 are 0 
		if((Voltage[EIBNO] <= 0.1)&&(Voltage1[EIBNO] <= 0.1))
		{
			for( k=0;k<8;k++) 
			{
				s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT1+k].fValue = 0.0;	
			} 
		}



		for( k=0;k<8;k++) 
		{
			Vol_tmp[k] = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT1+k].fValue;			
		}


		//if( g_VoltageType[EIBNO] == 0)   //48V
		if(s_I2cControllStatus.VoltageType[EIBNO] == 0)
		{
			//voltage type is 48 Block4	
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT2].fValue = Vol_tmp[1] - Vol_tmp[0];		
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT3].fValue = Vol_tmp[2] - Vol_tmp[1];
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT4].fValue = Vol_tmp[3] - Vol_tmp[2];
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT6].fValue = Vol_tmp[5] - Vol_tmp[4];
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT7].fValue = Vol_tmp[6] - Vol_tmp[5];
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT8].fValue = Vol_tmp[7] - Vol_tmp[6];

			// calculate one block battery voltage
			Voltage[EIBNO] = Mother_Volt / 4;			
		}	

		//else if (g_VoltageType[EIBNO] == 2)  //24V 
		else if(s_I2cControllStatus.VoltageType[EIBNO] == 2)
		{
			// voltage type is 24 Block2
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT2].fValue 
				= s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT2].fValue - s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT1].fValue;		
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT4].fValue 
				= s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT4].fValue - s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT3].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT6].fValue 
				= s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT6].fValue - s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT5].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT8].fValue 
				= s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT8].fValue - s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT7].fValue;

			// calculate one block battery voltage
			//Voltage[i] = Voltage[i] / 2;
			Voltage[EIBNO] = Mother_Volt / 2;
		}
		//else if (g_VoltageType[EIBNO] == 1)  //mid point
		else if(s_I2cControllStatus.VoltageType[EIBNO] == 1)
		{
			//For mid point type,keep raw data			
			Voltage[EIBNO] = Mother_Volt / 2;
		}

		// Brian asked me to show positive value on display, so I use the abs value instead, Jimmy 2011-09-30
		for(k =0; k < 8; k++)
		{
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT1 + k].fValue = ABS(s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT1 + k].fValue);
		}
		//-------------END OF MODIFYING by Jimmy Wu------------------------------------*/

		// To synchronize the Battery voltage and battery block voltages from EIB  are valid at same time
		if(g_iSynTimer < 15 )
		{
			g_iSynTimer++;
			//*(pData + start[i] + 28) = Digit_0.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BADBLOCKNUM].fValue = Digit_0.fValue;
		}
		else
		{

			//-----------BINGIN INTRODUCED OF MODIFYING by Jimmy Wu----------------------
			//* 
			//*  DATE          : 2011-9-28 11:08:29
			//*  CHANGE SOURCE : Brian's TR: EIB bad block battery issue
			//*  REMARK        : 
			//*  Modify the way to alarm bad block		
			//printf("\n****the bus voltage is %f !***\n", Voltage[i]);
			//printf("\n****the block Num is %d !***\n", uBlockUsed);
			//if(g_VoltageType[EIBNO] == 0) //48V block4
			if(s_I2cControllStatus.VoltageType[EIBNO] == 0)
			{	
				//max 2 strings
				for(k =0;k < 2; k++)
				{
					//tempS = fGetMaxMinDiff(pData + start[i],Voltage[i],uBlockUsed,k*4,4); //changed by Jimmy for TR
					tempS = fGetMaxMinDiff(&s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT1].fValue, Voltage[EIBNO], uBlockUsed, k*4, 4); 

					if(tempS >= Vol_Diff[EIBNO])
					{
						//*(pData + start[i] + 28) = Digit_1.fValue;
						s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BADBLOCKNUM].fValue = Digit_1.fValue;
						//printf(" BAD BATTERY BLOCK !!!! \n");
						break;
					}
					else if(tempS < Vol_Diff[EIBNO]*0.9)
					{
						//*(pData + start[i] + 28) = Digit_0.fValue;
						s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BADBLOCKNUM].fValue = Digit_0.fValue;
					}
				}				

			}
			//else if(g_VoltageType[EIBNO] == 1) //mid point
			else if(s_I2cControllStatus.VoltageType[EIBNO] == 1)
			{
				tempD = 0;
				for( k = 0; k< uBlockUsed; k++)
				{
					//tempS = ABS(*(pData + start[i] + k)) - ABS(Voltage[i]); //use abs, edited by Jimmy 2011-09-29
					tempS = ABS(s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT1+k].fValue) - ABS(Voltage[EIBNO]); //use abs, edited by Jimmy 2011-09-29

					if( tempS < 0) tempS = - tempS;
					if( tempS > Vol_Diff1[EIBNO])
					{
						//*(pData + start[i] + 28) = Digit_1.fValue;
						s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BADBLOCKNUM].fValue = Digit_1.fValue;
						break;
					}
					else if( tempS < (Vol_Diff1[EIBNO]*0.9))
					{// only 10% �ĸ澯�ز�
						tempD++;
					}
				}
				// only if all 8 channels is  right, it will set back this alarm to normal
				if( tempD == uBlockUsed) 
					//*(pData + start[i] + 28) = Digit_0.fValue;
					s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BADBLOCKNUM].fValue = Digit_0.fValue;

			}
			//else if (g_VoltageType[i] == 2) //24V block2
			else if(s_I2cControllStatus.VoltageType[EIBNO] == 2)
			{
				//max 4 strings
				for(k =0;k < 4; k++)
				{
					//tempS = fGetMaxMinDiff(pData + start[i],Voltage[i],uBlockUsed,k*2,2); //changed by Jimmy for TR
					tempS = fGetMaxMinDiff(&s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_VOLT1].fValue, Voltage[EIBNO], uBlockUsed, k*2, 2); 
					if(tempS >= Vol_Diff[EIBNO])
					{
						//*(pData + start[i] + 28) = Digit_1.fValue;
						s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BADBLOCKNUM].fValue = Digit_1.fValue;
						break;
					}
					else if(tempS < Vol_Diff[EIBNO]*0.9)
					{
						//*(pData + start[i] + 28) = Digit_0.fValue;
						s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BADBLOCKNUM].fValue = Digit_0.fValue;
					}
				}	
			}
			//else if (g_VoltageType[EIBNO] == 3)
			else if(s_I2cControllStatus.VoltageType[EIBNO] == 3)
			{	// do nothing, set alarm back to normal
				//*(pData + start[i] + 28) = Digit_0.fValue;
				s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BADBLOCKNUM].fValue = Digit_0.fValue;
			}

			//-------------END OF MODIFYING by Jimmy Wu------------------------------------

		}  //END: if(g_iSynTimer < 15 ) else


	} //END : if(nCommandCode == I2C_COMMAND_CODE_04)  //For Voltage



	/*2. Current ����*/
	else if(nCommandCode == I2C_COMMAND_CODE_05)  //For Current
	{
		j = EIBNO*6;

		// caculate the current value by shunt parameter
		//tempS = g_BatShuntPara[j+0].f_value/g_BatShuntPara[j+1].f_value;
		tempS = s_I2cControllStatus.BatShuntPara[EIBNO][0].f_value/s_I2cControllStatus.BatShuntPara[EIBNO][1].f_value;

		//added by Jimmy 2013.12.20,ȥ����Ư
		if(fabs(s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1].fValue) < 0.005 * s_I2cControllStatus.BatShuntPara[EIBNO][1].f_value)
		{
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1].fValue = 0.0;
		}

		//*(pData + start[i]+10) = (*(pData + start[i]+10) )* tempS;  
		s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1].fValue *= tempS;
		s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_ABSBATTCURR1].fValue = ABS(s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1].fValue);

		//tempS = g_BatShuntPara[j+2].f_value/g_BatShuntPara[j+3].f_value;
		tempS = s_I2cControllStatus.BatShuntPara[EIBNO][2].f_value/s_I2cControllStatus.BatShuntPara[EIBNO][3].f_value;

		//added by Jimmy 2013.12.20,ȥ����Ư
		if(fabs(s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT2].fValue) < 0.005 * s_I2cControllStatus.BatShuntPara[EIBNO][3].f_value)
		{
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT2].fValue = 0.0;
		}

		//*(pData + start[i]+11) = (*(pData + start[i]+11)) * tempS;
		s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT2].fValue *= tempS;
		s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_ABSBATTCURR2].fValue = ABS(s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT2].fValue);

		//tempS = g_BatShuntPara[j+4].f_value/g_BatShuntPara[j+5].f_value;
		tempS = s_I2cControllStatus.BatShuntPara[EIBNO][4].f_value/s_I2cControllStatus.BatShuntPara[EIBNO][5].f_value;
		//*(pData + start[i]+12) = (*(pData + start[i]+12)) * tempS;
		
		//added by Jimmy 2013.12.20,ȥ����Ư
		if(fabs(s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT3].fValue) < 0.005 * s_I2cControllStatus.BatShuntPara[EIBNO][5].f_value)
		{
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT3].fValue = 0.0;
		}
		
		s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT3].fValue *= tempS;
		s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_ABSBATTCURR3].fValue = ABS(s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT3].fValue);
		// ------------------set 3 channels current to the right value----------------------------------------------
		if(g_bIsSMDUEIBMode || bSignalSMDUEIBModeValid)
		{
			ReDispatchShuntReadings(EIBNO);
			return;
		}


		
		//if( g_BattNum[EIBNO] == 0)
		if(s_I2cControllStatus.BattNum[EIBNO] == 0)
		{				
			/*
			*(pData + start[i]+16) = NoValid.fValue;
			*(pData + start[i]+17) = NoValid.fValue;
			*(pData + start[i]+18) = NoValid.fValue;
			//battery number is 0, set the battery work status channel
			*(pData + start[i]+29) = Digit_1.fValue;
			*(pData + start[i]+30) = Digit_1.fValue;
			*(pData + start[i]+31) = Digit_1.fValue;
			*/

			
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR1].fValue = NoValid.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR2].fValue = NoValid.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR3].fValue = NoValid.fValue;

			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE1].fValue = Digit_1.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE2].fValue = Digit_1.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE3].fValue = Digit_1.fValue;


		}
		//else if ( g_BattNum[i] == 1 )
		else if(s_I2cControllStatus.BattNum[EIBNO] == 1)
		{
			/*
			*(pData + start[i]+16) = *(pData + start[i]+10);
			*(pData + start[i]+17) = NoValid.fValue;
			*(pData + start[i]+18) = NoValid.fValue;
			//set work status of battery 
			*(pData + start[i]+29) = Digit_0.fValue;
			*(pData + start[i]+30) = Digit_1.fValue;
			*(pData + start[i]+31) = Digit_1.fValue; */

			
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR1].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR2].fValue = NoValid.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR3].fValue = NoValid.fValue;

			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE1].fValue = Digit_0.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE2].fValue = Digit_1.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE3].fValue = Digit_1.fValue;
		}
		//else if ( g_BattNum[i] == 2)
		else if(s_I2cControllStatus.BattNum[EIBNO] == 2)
		{
			/*
			*(pData + start[i]+16) = *(pData + start[i]+10);
			*(pData + start[i]+17) = *(pData + start[i]+11);
			*(pData + start[i]+18) = NoValid.fValue;
			//set work status of battery 
			*(pData + start[i]+29) = Digit_0.fValue;
			*(pData + start[i]+30) = Digit_0.fValue;
			*(pData + start[i]+31) = Digit_1.fValue; */


			
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR1].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR2].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT2].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR3].fValue = NoValid.fValue;

			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE1].fValue = Digit_0.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE2].fValue = Digit_0.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE3].fValue = Digit_1.fValue;
		}
		//else if ( g_BattNum[EIBNO] == 3)
		else if(s_I2cControllStatus.BattNum[EIBNO] == 3)
		{
			/*
			*(pData + start[i]+16) = *(pData + start[i]+10);
			*(pData + start[i]+17) = *(pData + start[i]+11);
			*(pData + start[i]+18) = *(pData + start[i]+12);
			//set work status of battery 
			*(pData + start[i]+29) = Digit_0.fValue;
			*(pData + start[i]+30) = Digit_0.fValue;
			*(pData + start[i]+31) = Digit_0.fValue;*/

			
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR1].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR2].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT2].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTCURR3].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT3].fValue;

			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE1].fValue = Digit_0.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE2].fValue = Digit_0.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_BATTSTATE3].fValue = Digit_0.fValue;
		}



		//if( g_LoadNum[EIBNO] == 0)
		if(s_I2cControllStatus.LoadNum[EIBNO] == 0)
		{
			/*
			*(pData + start[i]+13) = NoValid.fValue;
			*(pData + start[i]+14) = NoValid.fValue;
			*(pData + start[i]+15) = NoValid.fValue; */
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR1].fValue = NoValid.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR2].fValue = NoValid.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR3].fValue = NoValid.fValue;

		}
		//else if ( g_LoadNum[i] == 1 )
		else if(s_I2cControllStatus.LoadNum[EIBNO] == 1)
		{
			/*
			*(pData + start[i]+13) = *(pData + start[i]+12);
			*(pData + start[i]+14) = NoValid.fValue;
			*(pData + start[i]+15) = NoValid.fValue;*/
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR1].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT3].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR2].fValue = NoValid.fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR3].fValue = NoValid.fValue;
		}
		//else if ( g_LoadNum[i] == 2)
		else if(s_I2cControllStatus.LoadNum[EIBNO] == 2)
		{
			/*
			*(pData + start[i]+13) = *(pData + start[i]+12);
			*(pData + start[i]+14) = *(pData + start[i]+11);
			*(pData + start[i]+15) = NoValid.fValue;*/
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR1].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT3].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR2].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT2].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR3].fValue = NoValid.fValue;
		}
		//else if ( g_LoadNum[i] == 3)
		else if(s_I2cControllStatus.LoadNum[EIBNO] == 3)
		{
			/*
			*(pData + start[i]+13) = *(pData + start[i]+12);
			*(pData + start[i]+14) = *(pData + start[i]+11);
			*(pData + start[i]+15) = *(pData + start[i]+10);*/
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR1].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT3].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR2].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT2].fValue;
			s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_LOADCURR3].fValue = s_I2cSampleRoughData.fRoughEIBData[EIBNO][I2C_EIB_1_CURRENT1].fValue;
		}

	}  //END: else if(nCommandCode == I2C_COMMAND_CODE_05)  //For Current


}
/*==========================================================================*
* FUNCTION : I2C_Read
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-13 14:49
*==========================================================================*/


static BOOL I2C_Read(void)
{
	BYTE i,j;
	int nRet;
	BYTE Frame[I2C_BUF_NUM]={0};    // �������ݻ�����

#if	_I2C_SAMPLER_DEBUG
	HANDLE hLocalThread;

	hLocalThread = RunThread_GetId(NULL);
#endif



	/*step1: ��ѯIB */
	for(i = 0; i < I2C_ROUGH_IB_MAX_NUM; i++)
	{
		if(s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_EXIST].iValue == TRUE)
		{
			if(s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_TYPE].iValue == I2C_DEVICE_TYPE_IB2)
			{
				nRet = GetI2CData(s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DEV_ADDR].iValue, 
					s_SampleModel_IB2.pCommandAndReturnData[0].nCommandCode, Frame, s_SampleModel_IB2.pCommandAndReturnData[0].nReturnByteNum);

				if (nRet)
				{
					Fix_Data(i, s_SampleModel_IB2.nDeviceType,  Frame,  &s_SampleModel_IB2.pCommandAndReturnData[0]);		
					s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_CNT].iValue = 0;	
					s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_FLAG].iValue = 0;								
				}
				else
				{
					s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_CNT].iValue++;	
					if(s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_CNT].iValue > 5)
					{
						s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_FLAG].iValue = 1;
					}

					vResetI2C();
				}
			}
			else if(s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_TYPE].iValue == I2C_DEVICE_TYPE_IB1)
			{
				nRet = GetI2CData(s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_DEV_ADDR].iValue, 
					s_SampleModel_IB1.pCommandAndReturnData[0].nCommandCode, Frame, s_SampleModel_IB1.pCommandAndReturnData[0].nReturnByteNum);

				if (nRet)
				{
					Fix_Data(i, s_SampleModel_IB1.nDeviceType,  Frame,  &s_SampleModel_IB1.pCommandAndReturnData[0]);	
					s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_CNT].iValue = 0;	
					s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_FLAG].iValue = 0;							
				}
				else
				{
					s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_CNT].iValue++;	
					if(s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_CNT].iValue > 5)
					{
						s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_FLAG].iValue = 1;
					}

					vResetI2C();
				}
			}

			Sleep( 100 );
		}
	}


	/* ˢ�²��� */
	for(i = 0; i < s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_EIB_NUM].iValue; i++)
	{
		ReadEIBSetSignal(i);
		ReadShuntPara(i);
	}

	/*step2: ��ѯEIB */

	for(i = 0; i < I2C_ROUGH_EIB_MAX_NUM; i++)
	{
		if(s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_EXIST].iValue == TRUE)
		{
			for(j = 0; j < s_SampleModel_EIB.nCommandNum; j++)
			{
				nRet = GetI2CData(s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_DEV_ADDR].iValue, 
					s_SampleModel_EIB.pCommandAndReturnData[j].nCommandCode, Frame, s_SampleModel_EIB.pCommandAndReturnData[j].nReturnByteNum);
				if (nRet)
				{
					Fix_Data(i, s_SampleModel_EIB.nDeviceType,  Frame,  &s_SampleModel_EIB.pCommandAndReturnData[j]);	
					RepairEIBData(i, s_SampleModel_EIB.pCommandAndReturnData[j].nCommandCode);
					s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_COMMFAIL_CNT].iValue = 0;	
					s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_COMMFAIL_FLAG].iValue = 0;
				}			
				else
				{
					s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_COMMFAIL_CNT].iValue++;	
					if(s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_COMMFAIL_CNT].iValue > 5)
					{
						s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_COMMFAIL_FLAG].iValue = 1;
					}

					vResetI2C();
				}		
			}

			Sleep( 100 );
		}
	}

	

#if	_I2C_SAMPLER_DEBUG

	RunThread_Heartbeat(hLocalThread);

/*
	printf(" s_I2cControllStatus.BattNum = %d \n", s_I2cControllStatus.BattNum[0]);
	printf(" I2C_EIB_1_CURRENT1 = %f \n", s_I2cSampleRoughData.fRoughEIBData[0][I2C_EIB_1_CURRENT1].fValue);
	printf(" I2C_EIB_1_BATTCURR1 = %f \n", s_I2cSampleRoughData.fRoughEIBData[0][I2C_EIB_1_BATTCURR1].fValue);

	printf(" I2C_EIB_1_VOLT1 = %f \n", s_I2cSampleRoughData.fRoughEIBData[0][I2C_EIB_1_VOLT1].fValue);
	printf(" I2C_EIB_1_VOLT2 = %f \n", s_I2cSampleRoughData.fRoughEIBData[0][I2C_EIB_1_VOLT2].fValue);
*/	
#endif

}


static SIG_ENUM FuelGetManagementUnitNum(void)
{
    //INT32 iFuelUnitNum = 0;
    INT32 iFuelGroupEquipId = 700;
    INT32 iFuelUnitNumSigID = 2;
    INT32 iFuelUnitNumSigType = 2;

    //G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
    static BOOL bInit = FALSE;
    static BOOL bConfigured = TRUE;

    if (!bInit)
    {
	EQUIP_INFO* pEquipInfo;
	int  nBufLen;

	if (DxiGetData(VAR_A_EQUIP_INFO,
	    iFuelGroupEquipId,			
	    0,		
	    &nBufLen,			
	    &pEquipInfo,			
	    0) == ERR_DXI_INVALID_EQUIP_ID)
	{
	    bConfigured = FALSE;

	    //give WARNING msg
	    AppLogOut("I2C_OB_PORT", APP_LOG_WARNING, 
		"Fuel equipment not configured in the solution.");
	}

	bInit = TRUE;
    }

    if (!bConfigured)   //if not configured, the Unit number is zero
    {
	return 0;
    }
    //G3_OPT end

    return GetEnumSigValue(iFuelGroupEquipId,
	iFuelUnitNumSigType,
	iFuelUnitNumSigID,
	"Fuel Get Unit Number");

    
}

static BOOL TwoFloatIsEqual(float fv1, float fv2)
{
    float ftemp;

    ftemp = ABS(fv1 - fv2);

    if(ftemp < 0.0001)
    {
	return TRUE;
    }

    return FALSE;
    
}

/*==========================================================================*
* FUNCTION : MB_GetFloatSigValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  nEquipId    : 
*            int  nSignalType : 
*            int  nSignalId   : 
* RETURN   : static float : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-02-19 09:44
*==========================================================================*/
static float I2C_GetFloatSigValue(int nEquipId, int nSignalType, int nSignalId)
{
    SIG_BASIC_VALUE*	pSigValue;
    int					iRst;
    int					iBufLen;

    iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	nEquipId,
	DXI_MERGE_SIG_ID(nSignalType, nSignalId),
	&iBufLen,
	&pSigValue,
	0);

    if(iRst != ERR_OK)
    {
	AppLogOut("I2C SAMP", 
	    APP_LOG_ERROR, 
	    "I2C_GetFloatSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
	    nEquipId,
	    nSignalType,
	    nSignalId); 
    }

    return pSigValue->varValue.fValue;

}
/*==========================================================================*
* FUNCTION : FuelGetEquipIsChange
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static void FuelGetEquipIsChange(INT32* pBeEquipChange)
{
    static INT32 siOldEquipNum = 0;
    SIG_ENUM	iEquipNum;

    iEquipNum = FuelGetManagementUnitNum();

    if (siOldEquipNum != iEquipNum)
    {
	siOldEquipNum = iEquipNum;
	(*pBeEquipChange) = TRUE;
	return;
    }
    

    (*pBeEquipChange) = FALSE;	

    return;
}
/*==========================================================================*
* FUNCTION : FuelEquipStatusChangeProc
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static void FuelEquipStatusChangeProc(void)
{
    static INT32 iBeEquipInfoChange = FALSE;
    static	DWORD	s_dwChangeForWeb= 0;

    if (iBeEquipInfoChange)
    {
	s_dwChangeForWeb =
	    GetDwordSigValue(106,   //SMDU_GROUP_EQUIPID
	    0,   //SIG_TYPE_SAMPLING
	    2+1,  //CFG_CHANGED_SIG_ID
	    "I2CSAMPLER");

	//for Web
	s_dwChangeForWeb++;
	if(s_dwChangeForWeb >= 255)
	{
	    s_dwChangeForWeb = 0;
	}

	//printf("\n $$$$$$$$$ s_bDetectDIPChange == TRUE \n");

	SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
	    0,		//SIG_TYPE_SAMPLING
	    2 + 1,	//CFG_CHANGED_SIG_ID
	    s_dwChangeForWeb,
	    "I2CSAMPLER");
	//for	LCD
	SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
	    0,		//SIG_TYPE_SAMPLING
	    2,		//CFG_CHANGED_SIG_ID
	    1,
	    "I2CSAMPLER");

	iBeEquipInfoChange = FALSE;
    }
    else
    {
	FuelGetEquipIsChange(&iBeEquipInfoChange);
    }

    return;
}
/*==========================================================================*
* FUNCTION : I2cStuffChn
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
/* OB */
/* 20130531 LKF	Ϊ�˼���ACU+�������ļ����Ѵ�����ɼ���OB���ź���䵽I2C�ɼ�����ͨ���� */
enum OB_SAMP_CHANNEL
{ 
    MB_CH_OB_EXIST_ST = 1 ,		//OB exist state: 0-not exist  1- exist
    MB_CH_OB_FAIL_ST,			//OB comm state: 0-fail  1- ok

    MB_CH_FUEL1_EXIST_ST,
    MB_CH_FUEL2_EXIST_ST,    

    MB_CH_OB_LOAD_FUSE1_ST = 6,		//OB Load Fuse 1
    MB_CH_OB_LOAD_FUSE2_ST ,			
    MB_CH_OB_LOAD_FUSE3_ST ,			
    MB_CH_OB_LOAD_FUSE4_ST ,			
    MB_CH_OB_LOAD_FUSE5_ST ,			
    MB_CH_OB_LOAD_FUSE6_ST ,			
    MB_CH_OB_LOAD_FUSE7_ST ,			
    MB_CH_OB_LOAD_FUSE8_ST ,			
    MB_CH_OB_LOAD_FUSE9_ST ,			
    MB_CH_OB_LOAD_FUSE10_ST ,			
    MB_CH_OB_BATT_FUSE1_ST = 16,			//OB BATT Fuse 1
    MB_CH_OB_BATT_FUSE2_ST ,	
    MB_CH_OB_BATT_FUSE3_ST ,	
    MB_CH_OB_BATT_FUSE4_ST ,	
    MB_CH_OB_LVD1_STATE  = 20,	//  OB LVD State
    MB_CH_OB_LVD2_STATE ,

    MB_CH_OB_TEMP1 = 25,

    MB_CH_FUEL1_FAIL_ST = 31,
    MB_CH_FUEL2_FAIL_ST,

    MB_CH_OB_LVD_AUX1_ST = 33,			//OB  LVD  aux
    MB_CH_OB_LVD_AUX2_ST,

    MB_CH_OB_LOAD_FUSE11_ST ,			
    MB_CH_OB_LOAD_FUSE12_ST ,
    MB_CH_OB_BATT_FUSE5_ST,
    MB_CH_OB_BATT_FUSE6_ST,


    MB_CH_OB_FUEL_SENSOR1,  //39
    MB_CH_OB_FUEL_SENSOR2,
#ifdef _CODE_FOR_MINI
    MB_CH_OB_BATT_FUSE7_ST,	//mini controller increase two batt fuse channel
    MB_CH_OB_BATT_FUSE8_ST,
#endif
    
};

struct	tagI2C_CHN_ROUGH_DATA_IDX
{
	int		iChannel;
	int		iRoughData;
};

typedef struct tagI2C_CHN_ROUGH_DATA_IDX I2C_CHN_TO_ROUGH_DATA;
#define I2C_CH_END_FLAG			(-1)

#define	   OB_FUELSENSOR_X1_SIGID	59
#define	   OB_FUELSENSOR_Y1_SIGID	60
#define	   OB_FUELSENSOR_X2_SIGID	61
#define	   OB_FUELSENSOR_Y2_SIGID	62

#define OB_RECT_GROUP_EQUIP_ID		2
#define OB_RATED_VOLT_SIG_ID		21
#define OB_CLEAR_VOLT_BENCHMARK		30	//���ڸõ�ѹ��Ϊ����µ絼��ĸ�Ų�����0

//Added for temprature filtering by Song Xu 20160520 
#define TEMPFILTER_MAGIC_COUNT_MAX	10
struct tempFilterMagic
{
	float tfSum;
	float tfLast;
	float tfAver;
	int tfCount;
};
typedef struct tempFilterMagic tfMagic;



static void I2cStuffChn(ENUMSIGNALPROC EnumProc,		//Callback function for stuffing channels
						LPVOID lpvoid)				//Parameter of the callback function
{
	int i = 0;
	int j = 0;
	int nDeviceNo;
	SIG_ENUM enumFuelUnitNum;
	I2C_VALUE I2cValueNone, I2cValue_1, I2cValue_0, I2cValueCommFail;
	static int s_iFuelGroupExistFlag = 0;    //����λ��״̬��RS485��ֵ�����źŴ���ǰ����������6/7�Ĵ���״̬

	float fFuelSensorX1, fFuelSensorX2, fFuelSensorY1, fFuelSensorY2, fFuelSampleValue;
	I2C_VALUE I2cValueTemp;

	//Added for temprature filtering by Song Xu 20160520 
	float ftemp;
	static tfMagic itfMagic[I2C_ROUGH_IB_MAX_NUM*2]={
	};


	int					iError = 0;
	int					nBufLen;
	SIG_BASIC_VALUE				*pSigValue;
	
	
	I2C_CHN_TO_ROUGH_DATA		I2C_IB_Sig[] =
	{
		//{I2C_CH_IB_1_STATE,		I2C_IB_1_STATE,		},
		{I2C_CH_IB_1_FAILURE,	I2C_IB_1_FAILURE,	},		
		{I2C_CH_IB_1_DI1,	I2C_IB_1_DI1,			},
		{I2C_CH_IB_1_DI2,	I2C_IB_1_DI2,			},			
		{I2C_CH_IB_1_DI3,	I2C_IB_1_DI3,			},			
		{I2C_CH_IB_1_DI4,	I2C_IB_1_DI4,			},			
		{I2C_CH_IB_1_DI5,	I2C_IB_1_DI5,			},			
		{I2C_CH_IB_1_DI6,	I2C_IB_1_DI6,			},
		{I2C_CH_IB_1_DI7,	I2C_IB_1_DI7,			},
		{I2C_CH_IB_1_DI8,	I2C_IB_1_DI8,			},
		{I2C_CH_IB_1_TEMPERATURE1,	I2C_IB_1_TEMPERATURE1,	},
		{I2C_CH_IB_1_TEMPERATURE2,	I2C_IB_1_TEMPERATURE2,	},

		{I2C_CH_END_FLAG,	I2C_CH_END_FLAG,		},	
	};

	I2C_CHN_TO_ROUGH_DATA		I2C_EIB_Sig[] =
	{		
		{I2C_CH_EIB_1_STATE,		I2C_EIB_1_STATE,			},
		{I2C_CH_EIB_1_FAILURE,		I2C_EIB_1_FAILURE,			},
		{I2C_CH_EIB_1_VOLT1,		I2C_EIB_1_VOLT1,			},
		{I2C_CH_EIB_1_VOLT2,		I2C_EIB_1_VOLT2,			},
		{I2C_CH_EIB_1_VOLT3,		I2C_EIB_1_VOLT3,			},
		{I2C_CH_EIB_1_VOLT4,		I2C_EIB_1_VOLT4,			},
		{I2C_CH_EIB_1_VOLT5,		I2C_EIB_1_VOLT5,			},
		{I2C_CH_EIB_1_VOLT6,		I2C_EIB_1_VOLT6,			},
		{I2C_CH_EIB_1_VOLT7,		I2C_EIB_1_VOLT7,			},
		{I2C_CH_EIB_1_VOLT8,		I2C_EIB_1_VOLT8,			},

		{I2C_CH_EIB_1_BLOCKNUM,		I2C_EIB_1_BLOCKNUM,			},
		{I2C_CH_EIB_1_CURR1,		I2C_EIB_1_CURRENT1,			},
		{I2C_CH_EIB_1_CURR2,		I2C_EIB_1_CURRENT2,			},
		{I2C_CH_EIB_1_CURR3,		I2C_EIB_1_CURRENT3,			},
		{I2C_CH_EIB_1_LOADCURR1,	I2C_EIB_1_LOADCURR1,			},
		{I2C_CH_EIB_1_LOADCURR2,	I2C_EIB_1_LOADCURR2,			},
		{I2C_CH_EIB_1_LOADCURR3,	I2C_EIB_1_LOADCURR3,			},
		{I2C_CH_EIB_1_BATTCURR1,	I2C_EIB_1_BATTCURR1,			},
		{I2C_CH_EIB_1_BATTCURR2,	I2C_EIB_1_BATTCURR2,			},
		{I2C_CH_EIB_1_BATTCURR3,	I2C_EIB_1_BATTCURR3,			},
		{I2C_CH_EIB_1_BADBLOCKNUM,	I2C_EIB_1_BADBLOCKNUM,			},

		{I2C_CH_EIBBATT_1_BATTSTATE1,	I2C_EIB_1_BATTSTATE1,			},
		{I2C_CH_EIBBATT_1_BATTSTATE2,	I2C_EIB_1_BATTSTATE2,			},
		{I2C_CH_EIBBATT_1_BATTSTATE3,	I2C_EIB_1_BATTSTATE3,			},

		{I2C_CH_EIB_1_TEMPERATURE1,	I2C_EIB_1_TEMPERATURE1,		},
		{I2C_CH_EIB_1_TEMPERATURE2,	I2C_EIB_1_TEMPERATURE2,		},
		
		{I2C_CH_EIB_ABSCURRENT1,	I2C_EIB_ABSBATTCURR1,		},
		{I2C_CH_EIB_ABSCURRENT2,	I2C_EIB_ABSBATTCURR2,		},
		{I2C_CH_EIB_ABSCURRENT3,	I2C_EIB_ABSBATTCURR3,		},


		{I2C_CH_END_FLAG,			I2C_CH_END_FLAG,	},

	};

	/* OB */
	/* 20130531 LKF	Ϊ�˼���ACU+�������ļ����Ѵ�����ɼ���OB���ź���䵽I2C�ɼ�����ͨ���� */

	I2C_CHN_TO_ROUGH_DATA		I2C_MB_Sig[] =
	{
#ifdef _CODE_FOR_MINI
		{MB_CH_OB_LOAD_FUSE1_ST,	MB_LOAD_FUSE1_ST,	},
	    {MB_CH_OB_LOAD_FUSE12_ST,	MB_LOAD_FUSE2_ST,	},
	    {MB_CH_OB_BATT_FUSE1_ST,	MB_BATT_FUSE1_ST,	},
	    {MB_CH_OB_BATT_FUSE2_ST,	MB_BATT_FUSE2_ST,	},
	    {MB_CH_OB_BATT_FUSE3_ST,	MB_BATT_FUSE3_ST,	},
	    {MB_CH_OB_BATT_FUSE4_ST,	MB_BATT_FUSE4_ST,	},
#else
	    {MB_CH_OB_LOAD_FUSE11_ST,	MB_LOAD_FUSE1_ST,	},
	    {MB_CH_OB_LOAD_FUSE12_ST,	MB_LOAD_FUSE2_ST,	},
	    {MB_CH_OB_BATT_FUSE5_ST,	MB_BATT_FUSE1_ST,	},
	    {MB_CH_OB_BATT_FUSE6_ST,	MB_BATT_FUSE2_ST,	},
#endif
	    {I2C_CH_END_FLAG,		I2C_CH_END_FLAG,	},
	};

	I2C_CHN_TO_ROUGH_DATA		I2C_OB_Sig[] =
	{	 
#ifdef _CODE_FOR_MINI	
		{MB_CH_OB_LOAD_FUSE11_ST,	OB_LOAD_FUSE1_ST,	},
#else	    
	    {MB_CH_OB_LOAD_FUSE1_ST,	OB_LOAD_FUSE1_ST,	},
#endif
	    {MB_CH_OB_LOAD_FUSE2_ST,	OB_LOAD_FUSE2_ST,	},
	    {MB_CH_OB_LOAD_FUSE3_ST,	OB_LOAD_FUSE3_ST,	},
	    {MB_CH_OB_LOAD_FUSE4_ST,	OB_LOAD_FUSE4_ST,	},
	    {MB_CH_OB_LOAD_FUSE5_ST,	OB_LOAD_FUSE5_ST,	},
	    {MB_CH_OB_LOAD_FUSE6_ST,	OB_LOAD_FUSE6_ST,	},
	    {MB_CH_OB_LOAD_FUSE7_ST,	OB_LOAD_FUSE7_ST,	},
	    {MB_CH_OB_LOAD_FUSE8_ST,	OB_LOAD_FUSE8_ST,	},
		
#ifdef _CODE_FOR_MINI	 
		{MB_CH_OB_BATT_FUSE5_ST,	OB_BATT_FUSE1_ST,	},
	    {MB_CH_OB_BATT_FUSE6_ST,	OB_BATT_FUSE2_ST,	},
#else
	    {MB_CH_OB_BATT_FUSE1_ST,	OB_BATT_FUSE1_ST,	},
	    {MB_CH_OB_BATT_FUSE2_ST,	OB_BATT_FUSE2_ST,	},
#endif
	    //{MB_CH_OB_LVD1_STATE,	OB_LVD1_STATE,		},
	    //{MB_CH_OB_LVD2_STATE,	OB_LVD2_STATE,		},

	    {MB_CH_OB_TEMP1,		OB_TEMP1,		},

	    //{MB_CH_OB_LVD_AUX1_ST,	OB_LVD_AUX1_ST,		},
	    //{MB_CH_OB_LVD_AUX2_ST,	OB_LVD_AUX2_ST,		},
	    {MB_CH_OB_LOAD_FUSE9_ST,   OB_LOAD_FUSE9_ST,	},
	    {MB_CH_OB_LOAD_FUSE10_ST,   OB_LOAD_FUSE10_ST,	},
		
#ifdef _CODE_FOR_MINI
	    {MB_CH_OB_BATT_FUSE7_ST,	OB_BATT_FUSE3_ST,	},
	    {MB_CH_OB_BATT_FUSE8_ST,	OB_BATT_FUSE4_ST,	},
#else
	    {MB_CH_OB_BATT_FUSE3_ST,	OB_BATT_FUSE3_ST,	},
	    {MB_CH_OB_BATT_FUSE4_ST,	OB_BATT_FUSE4_ST,	},
#endif
	    //{MB_CH_OB_FUEL_SENSOR1,	OB_FUEL_SENSOR1,	},
	    //{MB_CH_OB_FUEL_SENSOR2,	OB_FUEL_SENSOR2,	},

	    {MB_CH_OB_FAIL_ST,		OB_EXIST_ST,	},
	    {MB_CH_OB_EXIST_ST,		OB_EXIST_ST,	},

	    //{MB_CH_FUEL1_EXIST_ST,	OB_EXIST_ST,	},
	    //{MB_CH_FUEL2_EXIST_ST,	OB_EXIST_ST,	},
	    

	    {I2C_CH_END_FLAG,		I2C_CH_END_FLAG,	},
	};

	I2cValueNone.iValue = -9999;
	I2cValueCommFail.iValue = -9998;
	I2cValue_1.iValue	=	 1;
	I2cValue_0.iValue	=	 0;

	//IB
	nDeviceNo = 0;
	for(i = 0; i < I2C_ROUGH_IB_MAX_NUM; i++)
	{		
		if((s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_EXIST].iValue == TRUE)
			&& (s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_COMMFAIL_FLAG].iValue == 0))
		{
			s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_FAILURE].iValue = 0;
			s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_STATE].iValue = 0;
//Added for temperature filtering by Song Xu 20160520
			j = 0;			
			while(I2C_IB_Sig[j].iChannel != I2C_CH_END_FLAG)
			{
				if(I2C_IB_Sig[j].iChannel==I2C_CH_IB_1_TEMPERATURE1)
				{
//					printf("before IB%d tempe%f  ",i,s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue);
					ftemp=s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue;
					itfMagic[i*2].tfSum+=ftemp;
					if(itfMagic[i*2].tfCount<TEMPFILTER_MAGIC_COUNT_MAX)
					{
						itfMagic[i*2].tfAver=itfMagic[i*2].tfSum/(++itfMagic[i*2].tfCount);
					}
					else
					{
						itfMagic[i*2].tfSum-=itfMagic[i*2].tfAver;
						itfMagic[i*2].tfAver=itfMagic[i*2].tfSum/TEMPFILTER_MAGIC_COUNT_MAX;
					}
					//�����²����2�ȣ�������֮ǰ�����ݽ���ֱ�Ӵ���
					if(itfMagic[i*2].tfCount>0&&fabs(ftemp-itfMagic[i*2].tfLast)>2)
					{
						itfMagic[i*2].tfAver=ftemp;
						itfMagic[i*2].tfCount=1;
						itfMagic[i*2].tfSum=ftemp;
					}
					else
					{
						s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue=itfMagic[i*2].tfAver;
					}
					itfMagic[i*2].tfLast=ftemp;
//					printf("after IB%d tempe%f count%d sum%f aver%f\n",i,s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue,\
//						itfMagic[i*2].tfCount,itfMagic[i*2].tfSum,itfMagic[i*2].tfAver);
				}
				if(I2C_IB_Sig[j].iChannel==I2C_CH_IB_1_TEMPERATURE2)
				{
//					printf("before IB%d tempe%f ",i,s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue);
					ftemp=s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue;
					itfMagic[i*2+1].tfSum+=ftemp;
					if(itfMagic[i*2+1].tfCount<TEMPFILTER_MAGIC_COUNT_MAX)
					{
						itfMagic[i*2+1].tfAver=itfMagic[i*2+1].tfSum/(++itfMagic[i*2+1].tfCount);
					}
					else
					{
						itfMagic[i*2+1].tfSum-=itfMagic[i*2+1].tfAver;
						itfMagic[i*2+1].tfAver=itfMagic[i*2+1].tfSum/TEMPFILTER_MAGIC_COUNT_MAX;
					}
					//�����²����2�ȣ�������֮ǰ�����ݽ���ֱ�Ӵ���
					if(itfMagic[i*2+1].tfCount>0&&fabs(ftemp-itfMagic[i*2+1].tfLast)>2)
					{
						itfMagic[i*2+1].tfAver=ftemp;
						itfMagic[i*2+1].tfCount=1;
						itfMagic[i*2+1].tfSum=ftemp;
					}
					else
					{
						s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue=itfMagic[i*2+1].tfAver;
					}
					itfMagic[i*2+1].tfLast=ftemp;
					s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue=itfMagic[i*2+1].tfAver;
//					printf("after IB%d tempe%f count %d sum%f aver%f\n",i,s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue,\
//						itfMagic[i*2+1].tfCount,itfMagic[i*2+1].tfSum,itfMagic[i*2+1].tfAver);
				}
				j++;
			}
//Added for temperature filtering by Song Xu end


			j = 0;			
			while(I2C_IB_Sig[j].iChannel != I2C_CH_END_FLAG)
			{
				EnumProc(I2C_IB_Sig[j].iChannel + i * I2C_CHANNEL_IB_SPACE, 
					s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_Sig[j].iRoughData].fValue, 
					lpvoid );
				j++;
			}

			//��һ��IB���óɲ�����
			//As Brian's requirement, display IB2, the DO don't display on System.
			if(nDeviceNo == 0)
			{
#ifdef _CODE_FOR_MINI
				EnumProc(I2C_CH_IB_1_STATE + i * I2C_CHANNEL_IB_SPACE, 
				I2cValue_1.fValue, 
				lpvoid );
#else
			    EnumProc(I2C_CH_IB_1_STATE + i * I2C_CHANNEL_IB_SPACE, 
				I2cValue_0.fValue, 
				lpvoid );
#endif
			}
			else
			{
			    EnumProc(I2C_CH_IB_1_STATE + i * I2C_CHANNEL_IB_SPACE, 
				I2cValue_0.fValue, 
				lpvoid );
			}

			nDeviceNo++;
			
		}	
		else
		{
			if(s_I2cSampleRoughData.fRoughIBData[i][I2C_IB_1_EXIST].iValue == TRUE)   //ͨѶ�ж�
			{
				//ͨѶ״̬�����1��ͨѶ��
				EnumProc(I2C_CH_IB_1_FAILURE + i * I2C_CHANNEL_IB_SPACE, 
					I2cValue_1.fValue, 
					lpvoid );

						
				//��һ��IB���óɲ�����
				if(nDeviceNo == 0)
				{
#ifdef _CODE_FOR_MINI
					EnumProc(I2C_CH_IB_1_STATE + i * I2C_CHANNEL_IB_SPACE, 
					I2cValue_1.fValue, 
					lpvoid );
#else
				    EnumProc(I2C_CH_IB_1_STATE + i * I2C_CHANNEL_IB_SPACE, 
					I2cValue_0.fValue, 
					lpvoid );
#endif
				}
				else  //�����Ĵ���״̬�����0������		
				{
				    EnumProc(I2C_CH_IB_1_STATE + i * I2C_CHANNEL_IB_SPACE, 
					I2cValue_0.fValue, 
					lpvoid );
				}


				nDeviceNo++;

				j = 0;
				while(I2C_IB_Sig[j].iChannel != I2C_CH_END_FLAG)
				{
				    if((I2C_IB_Sig[j].iChannel != I2C_CH_IB_1_FAILURE)
					&& (I2C_IB_Sig[j].iChannel != I2C_CH_IB_1_STATE))
				    {
					EnumProc(I2C_IB_Sig[j].iChannel + i * I2C_CHANNEL_IB_SPACE, 
					    I2cValueCommFail.fValue, 
					    lpvoid );
				    }

				    j++;
				}
			}
			else  //������
			{
				//ͨѶ״̬�������Чֵ
				EnumProc(I2C_CH_IB_1_FAILURE + i * I2C_CHANNEL_IB_SPACE, 
					I2cValueNone.fValue, 
					lpvoid );

				//����״̬�����1��������
				EnumProc(I2C_CH_IB_1_STATE + i * I2C_CHANNEL_IB_SPACE, 
					I2cValue_1.fValue, 
					lpvoid );

				j = 0;
				while(I2C_IB_Sig[j].iChannel != I2C_CH_END_FLAG)
				{
				    if((I2C_IB_Sig[j].iChannel != I2C_CH_IB_1_FAILURE)
					&& (I2C_IB_Sig[j].iChannel != I2C_CH_IB_1_STATE))
				    {
					EnumProc(I2C_IB_Sig[j].iChannel + i * I2C_CHANNEL_IB_SPACE, 
					    I2cValueNone.fValue, 
					    lpvoid );
				    }

				    j++;
				}
			}
			
		}
	}

	// IB - NUM
	EnumProc(I2C_CH_IB_NUM, 
	    s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_NUM].fValue, 
	    lpvoid );

	EnumProc(I2C_CH_IB_TYPE, 
	    s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_TYPE].fValue, 
	    lpvoid );

	//�������һ��IB�壬����ʾIB��
	if(s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_IB_NUM].iValue > 1)
	{
	    EnumProc(I2C_CH_IB_EXIST, 
		I2cValue_0.fValue, 
		lpvoid );
	}
	else
	{
	    EnumProc(I2C_CH_IB_EXIST, 
		I2cValue_1.fValue,  
		lpvoid );
	}

	// EIB
	nDeviceNo = 0;
	for(i = 0; i < I2C_ROUGH_EIB_MAX_NUM; i++)
	{		
		if((s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_EXIST].iValue == TRUE)
			&& (s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_COMMFAIL_FLAG].iValue == 0))
		{
			s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_FAILURE].iValue = 0;
			s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_STATE].iValue = 0;

			j = 0;
			while(I2C_EIB_Sig[j].iChannel != I2C_CH_END_FLAG)
			{
				EnumProc(I2C_EIB_Sig[j].iChannel + i * I2C_CHANNEL_EIB_SPACE, 
					s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_Sig[j].iRoughData].fValue, 
					lpvoid );

				j++;
			}

			nDeviceNo++;
		}	
		else
		{
			if(s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_EXIST].iValue == TRUE)   //ͨѶ�ж�
			{
				//ͨѶ״̬�����1��ͨѶ��
				EnumProc(I2C_CH_EIB_1_FAILURE + i * I2C_CHANNEL_EIB_SPACE, 
					I2cValue_1.fValue, 
					lpvoid );

				//����״̬�����0������
				EnumProc(I2C_CH_EIB_1_STATE + i * I2C_CHANNEL_EIB_SPACE, 
					I2cValue_0.fValue, 
					lpvoid );

				//��ش�����״̬������ԭ��״̬
				EnumProc(I2C_CH_EIBBATT_1_BATTSTATE1 + i * I2C_CHANNEL_EIB_SPACE, 
				    s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_BATTSTATE1].fValue, 
				    lpvoid );
				EnumProc(I2C_CH_EIBBATT_1_BATTSTATE2 + i * I2C_CHANNEL_EIB_SPACE, 
				    s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_BATTSTATE2].fValue,
				    lpvoid );
				EnumProc(I2C_CH_EIBBATT_1_BATTSTATE3 + i * I2C_CHANNEL_EIB_SPACE, 
				    s_I2cSampleRoughData.fRoughEIBData[i][I2C_EIB_1_BATTSTATE3].fValue,
				    lpvoid );

				nDeviceNo++;

				j = 0;
				while(I2C_EIB_Sig[j].iChannel != I2C_CH_END_FLAG)
				{
				    if((I2C_EIB_Sig[j].iChannel != I2C_CH_EIB_1_FAILURE)
					&& (I2C_EIB_Sig[j].iChannel != I2C_CH_EIB_1_STATE)
					&& (I2C_EIB_Sig[j].iChannel != I2C_CH_EIBBATT_1_BATTSTATE1)
					&& (I2C_EIB_Sig[j].iChannel != I2C_CH_EIBBATT_1_BATTSTATE2)
					&& (I2C_EIB_Sig[j].iChannel != I2C_CH_EIBBATT_1_BATTSTATE3) )
				    {
					EnumProc(I2C_EIB_Sig[j].iChannel + i * I2C_CHANNEL_EIB_SPACE, 
					    I2cValueCommFail.fValue, 
					    lpvoid );
				    }

				    j++;
				}			
				
			}
			else  //������
			{
				//ͨѶ״̬�������Чֵ
				EnumProc(I2C_CH_EIB_1_FAILURE + i * I2C_CHANNEL_EIB_SPACE, 
					I2cValueNone.fValue, 
					lpvoid );

				//����״̬�����1��������
				EnumProc(I2C_CH_EIB_1_STATE + i * I2C_CHANNEL_EIB_SPACE, 
					I2cValue_1.fValue, 
					lpvoid );

				//��ش�����״̬�����1��������
				EnumProc(I2C_CH_EIBBATT_1_BATTSTATE1 + i * I2C_CHANNEL_EIB_SPACE, 
				    I2cValue_1.fValue, 
				    lpvoid );
				EnumProc(I2C_CH_EIBBATT_1_BATTSTATE2 + i * I2C_CHANNEL_EIB_SPACE, 
				    I2cValue_1.fValue, 
				    lpvoid );
				EnumProc(I2C_CH_EIBBATT_1_BATTSTATE3 + i * I2C_CHANNEL_EIB_SPACE, 
				    I2cValue_1.fValue, 
				    lpvoid );	

				j = 0;
				while(I2C_EIB_Sig[j].iChannel != I2C_CH_END_FLAG)
				{
				    if((I2C_EIB_Sig[j].iChannel != I2C_CH_EIB_1_FAILURE)
					&& (I2C_EIB_Sig[j].iChannel != I2C_CH_EIB_1_STATE)
					&& (I2C_EIB_Sig[j].iChannel != I2C_CH_EIBBATT_1_BATTSTATE1)
					&& (I2C_EIB_Sig[j].iChannel != I2C_CH_EIBBATT_1_BATTSTATE2)
					&& (I2C_EIB_Sig[j].iChannel != I2C_CH_EIBBATT_1_BATTSTATE3) )
				    {
					EnumProc(I2C_EIB_Sig[j].iChannel + i * I2C_CHANNEL_EIB_SPACE, 
					    I2cValueNone.fValue, 
					    lpvoid );
				    }

				    j++;
				}			
			}
			
		}		
	}

	//EIB��
	if(s_I2cSampleRoughData.fRoughGroupData[I2C_GROUP_EIB_NUM].iValue > 0)
	{
	    EnumProc(I2C_CH_EIB_EXIST, 
		I2cValue_0.fValue, 
		lpvoid );
	}
	else
	{
	    EnumProc(I2C_CH_EIB_EXIST, 
		I2cValue_1.fValue, 
		lpvoid );
	}
	

	/*OB*/
	if(g_SiteInfo.iOBHandle > 0)
	{
	    i = 0;
	    while(I2C_MB_Sig[i].iChannel != I2C_CH_END_FLAG)
	    {
		EnumProc(I2C_MB_Sig[i].iChannel, 
		    (g_SiteInfo.MbRoughDataShareForI2C[I2C_MB_Sig[i].iRoughData].fValue), 
		    lpvoid );
		i++;
	    }	  

	    i = 0;
	    while(I2C_OB_Sig[i].iChannel != I2C_CH_END_FLAG)
	    {
		EnumProc(I2C_OB_Sig[i].iChannel, 
		    (g_SiteInfo.MbRoughDataShareForI2C[I2C_OB_Sig[i].iRoughData].fValue), 
		    lpvoid );
		i++;
	    }	  

	    //LVD״̬ �� �زɸ澯 ����
	    if(s_OBLVDContactType == OBLVDTONTACTTYPE_STABLE)  //����ģʽ
	    {
		EnumProc((MB_CH_OB_LVD1_STATE),			
			(g_SiteInfo.MbRoughDataShareForI2C[OB_LVD1_STATE].fValue),		    
			lpvoid );			

		EnumProc((MB_CH_OB_LVD2_STATE),			
		    (g_SiteInfo.MbRoughDataShareForI2C[OB_LVD2_STATE].fValue),		    
		    lpvoid );		    
		
		EnumProc((MB_CH_OB_LVD_AUX1_ST),			
		    I2cValue_0.fValue,
		    lpvoid );		

		EnumProc((MB_CH_OB_LVD_AUX2_ST),			
		    I2cValue_0.fValue,
		    lpvoid );		

	    }
	    else   //˫�ȼ����ȴ��ز�ģʽ
	    {
		EnumProc((MB_CH_OB_LVD1_STATE),			
		    (g_SiteInfo.MbRoughDataShareForI2C[OB_LVD_AUX1_ST].fValue),		    
		    lpvoid );	
		

		EnumProc((MB_CH_OB_LVD2_STATE),			
		    (g_SiteInfo.MbRoughDataShareForI2C[OB_LVD_AUX2_ST].fValue),		    
		    lpvoid );	
		
		//LVD�زɸ澯����
		if(g_LVDAlarmOrNot == TRUE)
		{
		    /* 2013-9-24 ���θ���ĸ�ŵ�ѹ��FAILURE�Ĵ����Ļ�ͬACU+����
		    float fClearVoltBenchmark;
		    float fRatedVolt = I2C_GetFloatSigValue(OB_RECT_GROUP_EQUIP_ID, 
					    SIG_TYPE_SAMPLING,
					    OB_RATED_VOLT_SIG_ID);
		    fClearVoltBenchmark 
			= (fRatedVolt < 35.0) ? (OB_CLEAR_VOLT_BENCHMARK / 2) : OB_CLEAR_VOLT_BENCHMARK;

		    //LVD1
		    if(g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue < fClearVoltBenchmark) //�������ˣ���ر������²ɲ���ֵ
		    {
			EnumProc((MB_CH_OB_LVD_AUX1_ST),			
			    I2cValue_0.fValue,
			    lpvoid );
		    }		    
		    else 
		    */
		    if(g_SiteInfo.MbRoughDataShareForI2C[OB_LVD_AUX1_ST].iValue == 
			g_SiteInfo.MbRoughDataShareForI2C[OB_LVD1_STATE].iValue )
		    {
			EnumProc((MB_CH_OB_LVD_AUX1_ST),			
			    I2cValue_0.fValue,
			    lpvoid );	
		    }
		    else
		    {
			EnumProc((MB_CH_OB_LVD_AUX1_ST),			
			    I2cValue_1.fValue,
			    lpvoid );	
		    }
		    
		    //LVD2  2013-9-24 ���θ���ĸ�ŵ�ѹ��FAILURE�Ĵ����Ļ�ͬACU+����
		    /*if(g_SiteInfo.MbRoughDataShareForI2C[MB_DC_VOLT].fValue < fClearVoltBenchmark)  //�������ˣ���ر������²ɲ���ֵ
		    {
			EnumProc((MB_CH_OB_LVD_AUX2_ST),			
			    I2cValue_0.fValue,
			    lpvoid );
		    }
		    else 
		    */
		    if(g_SiteInfo.MbRoughDataShareForI2C[OB_LVD_AUX2_ST].iValue == 
			g_SiteInfo.MbRoughDataShareForI2C[OB_LVD2_STATE].iValue )
		    {
			EnumProc((MB_CH_OB_LVD_AUX2_ST),			
			    I2cValue_0.fValue,
			    lpvoid );	
		    }
		    else
		    {
			EnumProc((MB_CH_OB_LVD_AUX2_ST),			
			    I2cValue_1.fValue,
			    lpvoid );	
		    }
		}
	    }
	   

	    //ֻ�л�ȡ����λ���źŴ��ڵ�����£��Ŵ�������6/7�Ĵ���״̬
	    if(s_iFuelGroupExistFlag == 0)
	    {
		iError = DxiGetData(VAR_A_SIGNAL_VALUE, EQUIP_ID_FUELGROUP, DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, FUELGROUP_EXIST_ST_TYPE), &nBufLen, &pSigValue, 0);
		if( iError == ERR_DXI_OK )
		{
		    if(SIG_VALUE_IS_VALID(pSigValue) && SIG_VALUE_IS_CONFIGURED(pSigValue))
		    {
			if(pSigValue->varValue.enumValue == 0)
			{
			    s_iFuelGroupExistFlag = 1;
			}
		    }		    
		}
	    }

	    if(s_iFuelGroupExistFlag)  //��λ���źŴ���	    
	    {
		//OB����λ�����ź��ж� 
		enumFuelUnitNum = FuelGetManagementUnitNum();

		if(g_SiteInfo.MbRoughDataShareForI2C[OB_EXIST_ST].iValue != 0)
		{
		    enumFuelUnitNum = 0;
		}
    	    
		for (i = 0; ((i < enumFuelUnitNum) && (enumFuelUnitNum <= 2)); i++)
		{
		    EnumProc((MB_CH_FUEL1_EXIST_ST + i),			
			I2cValue_0.fValue,		    
			lpvoid );
		    EnumProc((MB_CH_FUEL1_FAIL_ST + i),			
			I2cValue_0.fValue,
			lpvoid );	
		    
		    //��λ�ź���Ҫ���㴦��
		    iError = DxiGetData(VAR_A_SIGNAL_VALUE, EQUIP_ID_OBFUEL1+i, DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, OB_FUELSENSOR_X1_SIGID), &nBufLen, &pSigValue, 0);
		    if( iError == ERR_DXI_OK )
		    {
			fFuelSensorX1 = pSigValue->varValue.fValue;
		    }
		    else
		    {
			fFuelSensorX1 = 0;
		    }
		    iError = DxiGetData(VAR_A_SIGNAL_VALUE, EQUIP_ID_OBFUEL1+i, DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, OB_FUELSENSOR_Y1_SIGID), &nBufLen, &pSigValue, 0);
		    if( iError == ERR_DXI_OK )
		    {
			fFuelSensorY1 = pSigValue->varValue.fValue;
		    }
		    else
		    {
			fFuelSensorY1 = 0;
		    }
		    iError = DxiGetData(VAR_A_SIGNAL_VALUE, EQUIP_ID_OBFUEL1+i, DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, OB_FUELSENSOR_X2_SIGID), &nBufLen, &pSigValue, 0);
		    if( iError == ERR_DXI_OK )
		    {
			fFuelSensorX2 = pSigValue->varValue.fValue;
		    }
		    else
		    {
			fFuelSensorX2 = 20;
		    }
		    iError = DxiGetData(VAR_A_SIGNAL_VALUE, EQUIP_ID_OBFUEL1+i, DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, OB_FUELSENSOR_Y2_SIGID), &nBufLen, &pSigValue, 0);
		    if( iError == ERR_DXI_OK )
		    {
			fFuelSensorY2 = pSigValue->varValue.fValue;
		    }
		    else
		    {
			fFuelSensorY2 = 20;
		    }

		    fFuelSampleValue = g_SiteInfo.MbRoughDataShareForI2C[OB_FUEL_SENSOR1+i].fValue;
		    if(fFuelSampleValue > fFuelSensorX2)
		    {
			fFuelSampleValue = fFuelSensorX2;
		    }		    
		    
		    if(fFuelSampleValue < fFuelSensorX1)
		    {
			fFuelSampleValue = fFuelSensorX1;
		    }

		    if(!TwoFloatIsEqual(fFuelSensorX1, fFuelSensorX2))
		    {
			I2cValueTemp.fValue = (fFuelSampleValue - fFuelSensorX1)/ (fFuelSensorX2 - fFuelSensorX1) 
			    * (fFuelSensorY2 - fFuelSensorY1) + fFuelSensorY1;
		    }
		    else
		    {
			I2cValueTemp.iValue = -9999;
		    }

		    EnumProc((MB_CH_OB_FUEL_SENSOR1 + i),			
			(I2cValueTemp.fValue),		    
			lpvoid );
		    
		}
		for (; i < 2; i++)
		{
		    EnumProc((MB_CH_FUEL1_EXIST_ST + i),			
			I2cValueNone.fValue,
			lpvoid );
		    EnumProc((MB_CH_FUEL1_FAIL_ST + i),			
			I2cValueNone.fValue,
			lpvoid );
		    EnumProc((MB_CH_OB_FUEL_SENSOR1 + i),			
			I2cValueNone.fValue,		    
			lpvoid );
		}

		FuelEquipStatusChangeProc();

	    }
	    else
	    {
		for (i = 0; i < 2; i++)
		{
		    EnumProc((MB_CH_FUEL1_EXIST_ST + i),			
			I2cValueNone.fValue,
			lpvoid );
		    EnumProc((MB_CH_FUEL1_FAIL_ST + i),			
			I2cValueNone.fValue,
			lpvoid );
		    EnumProc((MB_CH_OB_FUEL_SENSOR1 + i),			
			I2cValueNone.fValue,		    
			lpvoid );
		}
	    }

	    
	}  //END: if(g_SiteInfo.iOBHandle > 0)
	else
	{
	    i = 0;
	    while(I2C_MB_Sig[i].iChannel != I2C_CH_END_FLAG)
	    {
		EnumProc(I2C_MB_Sig[i].iChannel, 
		    (g_SiteInfo.MbRoughDataShareForI2C[I2C_MB_Sig[i].iRoughData].fValue), 
		    lpvoid );
		i++;
	    }

	    i = 0;
	    while(I2C_OB_Sig[i].iChannel != I2C_CH_END_FLAG)
	    {
		EnumProc(I2C_OB_Sig[i].iChannel, 
		    I2cValueNone.fValue, 
		    lpvoid );
		i++;
	    }

	    for (i = 0; i < 2; i++)
	    {
		EnumProc((MB_CH_FUEL1_EXIST_ST + i),			
		    I2cValueNone.fValue,
		    lpvoid );
		EnumProc((MB_CH_FUEL1_FAIL_ST + i),			
		    I2cValueNone.fValue,
		    lpvoid );
		EnumProc((MB_CH_OB_FUEL_SENSOR1 + i),			
		    I2cValueNone.fValue,		    
		    lpvoid );
	    }
	}

#ifdef _CODE_FOR_MINI
	EnumProc(MB_CH_OB_LOAD_FUSE12_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE11_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE2_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE3_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE4_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE5_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE6_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE7_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE8_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE9_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_LOAD_FUSE10_ST,I2cValueNone.fValue, lpvoid );

	EnumProc(MB_CH_OB_BATT_FUSE5_ST,I2cValueNone.fValue, lpvoid );
	EnumProc(MB_CH_OB_BATT_FUSE6_ST,I2cValueNone.fValue, lpvoid );
#endif



#if _I2C_SAMPLER_DEBUG	
	printf(" OB_LVD1_STATE = %d \n", g_SiteInfo.MbRoughDataShareForI2C[OB_LVD1_STATE].iValue);
	printf(" OB_LVD2_STATE = %d \n", g_SiteInfo.MbRoughDataShareForI2C[OB_LVD2_STATE].iValue);
#endif	
	
	
}
/*==========================================================================*
* FUNCTION : GetReConfigCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL :TRUE means Receive Reconfig Cmd from web 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-10 16:55
*==========================================================================*/
#define	    I2C_SYS_RECONFIG_SIG_ID	    70
#define	    SYS_GROUP_EQUIPID		    1
static BOOL GetReConfigCmd(void)
{
    if(GetDwordSigValue(SYS_GROUP_EQUIPID, 
	SIG_TYPE_SETTING,
	I2C_SYS_RECONFIG_SIG_ID,
	"I2C_SAMP") > 0)
    {
	SetDwordSigValue(SYS_GROUP_EQUIPID, 
	    SIG_TYPE_SETTING,
	    I2C_SYS_RECONFIG_SIG_ID,
	    0,
	    "I2C_SAMP");

	return TRUE;
    }

    return FALSE;
}
static	BOOL I2c_ResetRelays_AfterScanBoards()
{

	static BOOL s_bFirstRun=TRUE;
	if(s_bFirstRun==FALSE)
	{
		return  FALSE;	
	}
	//printf("Reset DO s \n");
	BYTE	byStatusIBRelay[IB_NUM_SOLUTION];
	BYTE	byStatusEIBRelay[EIB_NUM_SOLUTION];
	BYTE	byTemp=0;
	int nDeviceNo=0,nCmdNo=0,nDeviceAddr=0,nCmdCode=0;
	//IB  1~4
	for(nDeviceNo=0;nDeviceNo<IB_NUM_SOLUTION;nDeviceNo++)
	{	
		nDeviceAddr = s_I2cSampleRoughData.fRoughIBData[nDeviceNo][I2C_IB_1_DEV_ADDR].iValue;
		nCmdCode = 0x82;
		for(nCmdNo=I2C_CTRL_CH_IB_1_DO1;nCmdNo<=I2C_CTRL_CH_IB_1_DO8;nCmdNo++)
		{
			byTemp = 0x01 << (nCmdNo - I2C_CTRL_CH_IB_1_DO1);
			byStatusIBRelay[nDeviceNo] &= (~byTemp);	  //��� 
			SetDev(nDeviceAddr, nCmdCode, byStatusIBRelay[nDeviceNo]);
		}	
	}

	/* EIB */
	for(nDeviceNo=0;nDeviceNo<IB_NUM_SOLUTION;nDeviceNo++)
	{
		nDeviceAddr = s_I2cSampleRoughData.fRoughEIBData[nDeviceNo][I2C_EIB_1_DEV_ADDR].iValue;
		nCmdCode = 0x82;
		//EIB Do 1~4 
		for(nCmdNo=I2C_CTRL_CH_EIB_1_DO1;nCmdNo< I2C_CTRL_CH_EIB_1_DO4;nCmdNo++)
		{
			byTemp = 0x01 << (nCmdNo - I2C_CTRL_CH_EIB_1_DO1);	
			byStatusEIBRelay[nDeviceNo] &= (~byTemp);	  //��� 
			SetDev(nDeviceAddr, nCmdCode, byStatusEIBRelay[nDeviceNo]);
		}
		//for EIB DO5
		nCmdNo=  I2C_CTRL_CH_EIB_1_DO5;
		byTemp = 0x01 << 4;
		byStatusEIBRelay[nDeviceNo] &= (~byTemp);	  //��� 
		SetDev(nDeviceAddr, nCmdCode, byStatusEIBRelay[nDeviceNo]);
	}
	 s_bFirstRun=FALSE;
	return ;
}
/*==========================================================================*
* FUNCTION : Query
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE                  hComm    : 
*            int                     nUnitNo  : sampler address  used to as file handle 
*			  ENUMSIGNALPROC          EnumProc : Enum function 
*			  LPVOID lpvoid			  pointer  : 
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : Kenn                 DATE: 2008-07-08 15:46
*==========================================================================*/
DLLExport BOOL Query(HANDLE hComm, 
					 int nUnitNo,  
					 ENUMSIGNALPROC EnumProc,   
					 LPVOID lpvoid)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	//int				i = 0;
	int				iError = 0;
	int				nVarID = 0;			//Equip id
	int				nVarSubID = 0;			//setting signal id
	int				nBufLen;
	int				nTimeOut = 0;
	SIG_BASIC_VALUE			*pSigValue;
	
	g_bIsSMDUEIBMode = Is_SMDU_EIBMode();
	bSignalSMDUEIBModeValid = Is_SMDU_EIBModeValid();

	float fAllEIBLoad = 0.0;//ͳ������EIB����
	int i = 0;
	for(i =0;i < 4;i++)
	{
		g_fTotalEIBLoads[i] = 0.0;
	}

	static BOOL			s_bFirstRun = TRUE;

	if(s_bFirstRun)
	{		
		s_I2cControllStatus.bNeedScanBoards = TRUE;

		nVarID = EQUIP_ID_OBLVD;    
		nVarSubID = DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, OBLVD_SETTING_ID_CONTACTORTYPE);
		iError = DxiGetData(VAR_A_SIGNAL_VALUE, nVarID, nVarSubID, &nBufLen, &pSigValue, nTimeOut);
		if( iError == ERR_DXI_OK )
		{
		    s_OBLVDContactType = pSigValue->varValue.enumValue;
		}	
	}
//	else
//	{
//		I2c_ResetRelays_AfterScanBoards();	
//	}
	s_bFirstRun = FALSE;

	//found  reconfig
	if(GetReConfigCmd())
	{
	    TRACE("\n	######  RecvReConfigCmdForI2CSample  #### \n");

	    s_I2cControllStatus.bNeedScanBoards = TRUE;	    
	}

	// ɨ����Ӵ���
	if( s_I2cControllStatus.bNeedScanBoards ) 
	{
		ScanI2CBoards();

		s_I2cControllStatus.bNeedScanBoards = FALSE;
	}

	// ʵʱ���ݲɼ�
	I2C_Read();


	// �����ϱ�
	I2cStuffChn(EnumProc, lpvoid);

	//added by Jimmy for SMDU-EIB Shunt Setting CR2013.07.06
	if(g_bIsSMDUEIBMode)
	{
		for(i =0;i < 4;i++)
		{
			fAllEIBLoad += g_fTotalEIBLoads[i] ;
		}
			//�ܸ��ص�������
#define EUIP_ID_EIBGRP 196
	SetFloatSigValue(EUIP_ID_EIBGRP, 
			SIG_TYPE_SAMPLING,
			1,
			fAllEIBLoad,
			"I2C_SAMP");
	}

	return TRUE;
}

/*==========================================================================*
* FUNCTION : GetProductInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hComm    : 
*            int     nUnitNo  : 
*            void    *pPI     : 
*            int     nEquipId : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-10-30 16:55
*==========================================================================*/
BOOL GetProductInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	UNUSED(hComm);

	BYTE Frame[I2C_BUF_NUM]={0};    // �������ݻ�����
	//BYTE Address[13] = {I2C_EIB_ADDRESS,I2C_EIB2_ADDRESS,I2C_EIB3_ADDRESS,I2C_EIB4_ADDRESS,0,I2C_IB_ADDRESS,I2C_IB2_ADDRESS,I2C_IB3_ADDRESS,I2C_IB4_ADDRESS,I2C_I2B_ADDRESS,I2C_I2B2_ADDRESS,I2C_I2B3_ADDRESS,I2C_I2B4_ADDRESS};
	int i;
	PRODUCT_INFO  *pInfo;
	int				nRet = 0;
	//float		fTemp[1024];
	BYTE	 	cAddress1;
	BOOL	bIBFlag = FALSE;
	BOOL	bEIBFlag = FALSE;


	pInfo = (PRODUCT_INFO*)pPI;



	//memset(pInfo,0,sizeof(PRODUCT_INFO));
	//check if support Product Info
	if ( (nUnitNo < 197) || (nUnitNo==201) || (nUnitNo >205))
	{
		pInfo->bSigModelUsed = FALSE;
		return FALSE;
	}

	//IB should get product info two times, one for IB2, the other for IB1
	if( (nUnitNo <= 205)&&( nUnitNo >=202))
	{	//IB boards
		switch(nUnitNo)
		{
		case 202:
			if(s_I2cSampleRoughData.fRoughIBData[0][I2C_IB_1_EXIST].iValue == 1)  //����
			{
				cAddress1 = s_I2cSampleRoughData.fRoughIBData[0][I2C_IB_1_DEV_ADDR].iValue;
				bIBFlag = TRUE;
			}			
			break;
		case 203:
			if(s_I2cSampleRoughData.fRoughIBData[1][I2C_IB_1_EXIST].iValue == 1)  //����
			{
				cAddress1 = s_I2cSampleRoughData.fRoughIBData[1][I2C_IB_1_DEV_ADDR].iValue;
				bIBFlag = TRUE;
			}			
			break;
		case 204:
			if(s_I2cSampleRoughData.fRoughIBData[2][I2C_IB_1_EXIST].iValue == 1)  //����
			{
				cAddress1 = s_I2cSampleRoughData.fRoughIBData[2][I2C_IB_1_DEV_ADDR].iValue;
				bIBFlag = TRUE;
			}			
			break;
		case 205:
			if(s_I2cSampleRoughData.fRoughIBData[3][I2C_IB_1_EXIST].iValue == 1)  //����
			{
				cAddress1 = s_I2cSampleRoughData.fRoughIBData[3][I2C_IB_1_DEV_ADDR].iValue;
				bIBFlag = TRUE;
			}			
			break;
		default:
			break;

		}
		/*
		Change For: To settle the read I2C product infomation error sometime, 
		Program will read 3  times at most when reading error
		BY:liuxinglian
		DATE:2009-05-25
		*/
		if(bIBFlag == TRUE)
		{
			i = 0;
			while(i <= 5)
			{			
				if(GetI2CData(cAddress1, 1, Frame,36))
				{
					break;
				}			
				i++;
			}
			if(i > 5)
			{
				pInfo->bSigModelUsed = FALSE;
				return FALSE;
			}
		}
		else
		{
		    pInfo->bSigModelUsed = FALSE;
		    return FALSE;
		}
		
	}
	else
	{	//EIB boards
		switch(nUnitNo)
		{
		case 197:
			if(s_I2cSampleRoughData.fRoughEIBData[0][I2C_EIB_1_EXIST].iValue == 1)  //����
			{
				cAddress1 = s_I2cSampleRoughData.fRoughEIBData[0][I2C_EIB_1_DEV_ADDR].iValue;
				bEIBFlag = TRUE;
			}
			
			break;
		case 198:
			if(s_I2cSampleRoughData.fRoughEIBData[1][I2C_EIB_1_EXIST].iValue == 1)  //����
			{
				cAddress1 = s_I2cSampleRoughData.fRoughEIBData[1][I2C_EIB_1_DEV_ADDR].iValue;
				bEIBFlag = TRUE;
			}
			break;
		case 199:
			if(s_I2cSampleRoughData.fRoughEIBData[2][I2C_EIB_1_EXIST].iValue == 1)  //����
			{
				cAddress1 = s_I2cSampleRoughData.fRoughEIBData[2][I2C_EIB_1_DEV_ADDR].iValue;
				bEIBFlag = TRUE;
			}
			break;
		case 200:
			if(s_I2cSampleRoughData.fRoughEIBData[3][I2C_EIB_1_EXIST].iValue == 1)  //����
			{
				cAddress1 = s_I2cSampleRoughData.fRoughEIBData[3][I2C_EIB_1_DEV_ADDR].iValue;
				bEIBFlag = TRUE;
			}
			break;
		default:
			break;

		}
		/*
		Change For: To settle the read I2C product infomation error sometime;
		BY:liuxinglian
		DATE:2009-05-25
		*/
		if(bEIBFlag == TRUE)
		{
			i = 0;
			while(i <= 5)
			{
				if( GetI2CData(cAddress1, 1, Frame,36))
				{
					break;
				}

				i++;
			}
			if(i > 5)
			{
				pInfo->bSigModelUsed = FALSE;
				return FALSE;
			}
		}
		else
		{
		    pInfo->bSigModelUsed = FALSE;
		    return FALSE;
		}
	}

	//Product Info frame analyse
	pInfo->bSigModelUsed = TRUE;

	TRACE("I2C equip Info:%s\n", Frame);

	//1.Serial number process
	for (i = 0; i < 11; i++)
	{
		pInfo->szSerialNumber[i] = Frame[i+8];
	}

	//2.HW version process - the last three character
	//printf("\n PartNumber process = %s\n", (char *)Frame+13+iLen-6);
	for (i = 0; i < 3; i++)
	{
		//pInfo->szHWVersion[2-i] = Frame[i];
		pInfo->szHWVersion[i] = Frame[i];
	}

	//3.PartNumber process
	//printf("\n PartNumber process = %s\n", (char *)Frame+13+22);
	for (i = 0; i < 16; i++)   //16 to 9
	{
		pInfo->szPartNumber[i] = 0;

	}
	for (i = 0; i < 9; i++)   //16 to 9
	{
		pInfo->szPartNumber[i] = Frame[i+19];

	}

	//4.SW version process -- using another frame
	for (i = 0; i < 4; i++)
	{
		//pInfo->szSWVersion[3-i] = Frame[i+4];
		pInfo->szSWVersion[i] = Frame[i+4];
	}


	//#ifdef _DEBUG_PRODUCT_INFO
	TRACE("I2C equip PartNumber:%s\n", pInfo->szPartNumber);
	TRACE("I2C equip SW version:%s\n", pInfo->szSWVersion);
	TRACE("I2C equip HW version:%s\n", pInfo->szHWVersion);
	TRACE("I2C equip Serial Number:%s\n", pInfo->szSerialNumber);
	//#endif //_DEBUG_PRODUCT_INFO


	return TRUE;
}
/*==========================================================================*
* FUNCTION : SetI2CData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  nAddr      : 
*            BYTE  *pBuffer   : 
*            BYTE  nByteCount : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-13 15:21
*==========================================================================*/
static	int SetI2CData( BYTE nAddr, BYTE *pBuffer, BYTE nByteCount )
{
    int nRet = 0;
   
    if( !g_bDriverIsOpen ) 
    {	
	if(!OpenI2CDriver()) 
	{
	    return FALSE;
	}			
    }	

    ioctl( g_iI2Cfd, I2C_SLAVE_G3, nAddr);	

    nRet = write( g_iI2Cfd, pBuffer, nByteCount);

    if ( nRet < 0 )
    {
	return FALSE;
    }
    
    return TRUE;
    
};

/*==========================================================================*
* FUNCTION : 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
#define nMaxBufNum						100
static	BOOL SetDev(int nAddr, int nCmdNo, int nData)
{
	BYTE Frame[nMaxBufNum]={0};    // Accept data buffer
	int nSum = 0;

	Frame[0] = 1;			//bytecount:original value is 5
	Frame[1] = nCmdNo;
	Frame[2] = nData;
	nSum = ~(Frame[0] +Frame[1] +Frame[2]) + 1;
	Frame[3] =(BYTE)nSum;
	Frame[4] =(BYTE)(nSum>>8);
	TRACE("IB control command string is %s \n",Frame);

	if( SetI2CData(nAddr, Frame, 5) != 1) 
	{
		return FALSE;
	}

	return TRUE;
}
/*==========================================================================*
* FUNCTION : 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
#define IOC_SETLVD1_BISTA              _IOW(DIDO_MAJOR, 5, unsigned long)    //˫��̬
#define IOC_SETLVD1_MONOSTA            _IOW(DIDO_MAJOR, 6, unsigned long)    //����̬
#define IOC_SETLVD2_BISTA              _IOW(DIDO_MAJOR, 7, unsigned long)    //˫��̬
#define IOC_SETLVD2_MONOSTA            _IOW(DIDO_MAJOR, 8, unsigned long)    //����̬

enum OB_CTL_CHANNEL
{ 
    MB_SET_LVD_CONTACTOR_TYPE = 5,  //OB lvd contact type
    MB_CTL_CH_LVD1 = 7,		//LVD1
    MB_CTL_CH_LVD2,		//LVD2

};

static	BOOL ExecuteControl(int nCmdNo, union PARA_CONTROL uSetVal)
{

	int nDeviceNo = 0;
	//int nExistNo = 0;
	//int nMatchFlag = 0;
	int nCmdCode = 0;
	int nDeviceAddr = 0;
	BYTE byTemp = 0;
	int iOutput;

	//OB LVD1
	if(nCmdNo == MB_CTL_CH_LVD1)
	{
	    if(g_SiteInfo.iIB01Handle < 0)
	    {
		return FALSE;	
	    }
	    iOutput = (FLOAT_EQUAL0(uSetVal.f_value)) ? 0 : 1;
	
	    if(s_OBLVDContactType == OBLVDTONTACTTYPE_BISTABLE)  //˫��
	    {
		ioctl(g_SiteInfo.iIB01Handle, IOC_SETLVD1_BISTA, iOutput);	
	    }
	    else   //����
	    {
		ioctl(g_SiteInfo.iIB01Handle, IOC_SETLVD1_MONOSTA, iOutput);	
	    }		

	    g_SiteInfo.MbRoughDataShareForI2C[OB_LVD1_STATE].iValue = iOutput;	  
	    g_LVDAlarmOrNot = TRUE;

	}
	//OB LVD2
	else if(nCmdNo == MB_CTL_CH_LVD2)
	{
	    if(g_SiteInfo.iIB01Handle < 0)
	    {
		return FALSE;	
	    }

	    iOutput = (FLOAT_EQUAL0(uSetVal.f_value)) ? 0 : 1;

	    if(s_OBLVDContactType == OBLVDTONTACTTYPE_BISTABLE)  //˫��
	    {
		ioctl(g_SiteInfo.iIB01Handle, IOC_SETLVD2_BISTA, iOutput);			
	    }
	    else   //����
	    {
		ioctl(g_SiteInfo.iIB01Handle, IOC_SETLVD2_MONOSTA, iOutput);	
	    }	      

	    g_SiteInfo.MbRoughDataShareForI2C[OB_LVD2_STATE].iValue = iOutput;
	    g_LVDAlarmOrNot = TRUE;
	}
	//OB LVD Type
	else if(nCmdNo == MB_SET_LVD_CONTACTOR_TYPE)
	{
	    s_OBLVDContactType = uSetVal.f_value;
	}
	
	/* IB */
	else if((nCmdNo >= I2C_CTRL_CH_IB_1_DO1)
		&& (nCmdNo < (I2C_CTRL_CH_IB_1_DO1 + I2C_CHANNEL_IB_SPACE * IB_NUM_SOLUTION )))
	{
		nDeviceNo = (nCmdNo - I2C_CTRL_CH_IB_1_DO1) / I2C_CHANNEL_IB_SPACE;

		nDeviceAddr = s_I2cSampleRoughData.fRoughIBData[nDeviceNo][I2C_IB_1_DEV_ADDR].iValue;
		nCmdCode = 0x82;

		byTemp = 0x01 << (nCmdNo - I2C_CTRL_CH_IB_1_DO1 - nDeviceNo * I2C_CHANNEL_IB_SPACE);

			if((int)uSetVal.f_value)
			{
				s_I2cControllStatus.byStatusIBRelay[nDeviceNo] |= byTemp;
			}
			else
			{
				s_I2cControllStatus.byStatusIBRelay[nDeviceNo] &= (~byTemp);
			}

			return SetDev(nDeviceAddr, nCmdCode, s_I2cControllStatus.byStatusIBRelay[nDeviceNo]);


	}

	/* EIB */
	else if((nCmdNo >= I2C_CTRL_CH_EIB_1_DO1)
		&& (nCmdNo < (I2C_CTRL_CH_EIB_1_DO1 + I2C_CHANNEL_EIB_SPACE * EIB_NUM_SOLUTION)))
	{
		nDeviceNo = (nCmdNo - I2C_CTRL_CH_EIB_1_DO1) / I2C_CHANNEL_EIB_SPACE;

		nDeviceAddr = s_I2cSampleRoughData.fRoughEIBData[nDeviceNo][I2C_EIB_1_DEV_ADDR].iValue;


		//if(nMatchFlag == TRUE)
		{
			// DO1~DO4
			if( (nCmdNo >= (I2C_CTRL_CH_EIB_1_DO1 + nDeviceNo * I2C_CHANNEL_EIB_SPACE))
				&& (nCmdNo <= (I2C_CTRL_CH_EIB_1_DO4 + nDeviceNo * I2C_CHANNEL_EIB_SPACE)) )
			{
				byTemp = 0x01 << (nCmdNo - I2C_CTRL_CH_EIB_1_DO1 - nDeviceNo * I2C_CHANNEL_EIB_SPACE);

				if((int)uSetVal.f_value)
				{
					s_I2cControllStatus.byStatusEIBRelay[nDeviceNo] |= byTemp;
				}
				else
				{
					s_I2cControllStatus.byStatusEIBRelay[nDeviceNo] &= (~byTemp);
				}

				nCmdCode = 0x82;

				return SetDev(nDeviceAddr, nCmdCode, s_I2cControllStatus.byStatusEIBRelay[nDeviceNo]);

			}
			// DO5
			else if(nCmdNo == (I2C_CTRL_CH_EIB_1_DO5 + nDeviceNo * I2C_CHANNEL_EIB_SPACE))  
			{
				byTemp = 0x01 << 4;

				if((int)uSetVal.f_value)
				{
					s_I2cControllStatus.byStatusEIBRelay[nDeviceNo] |= byTemp;
				}
				else
				{
					s_I2cControllStatus.byStatusEIBRelay[nDeviceNo] &= (~byTemp);
				}

				nCmdCode = 0x82;

				return SetDev(nDeviceAddr, nCmdCode, s_I2cControllStatus.byStatusEIBRelay[nDeviceNo]);
			}
			// set battery rated voltage and current to EIB2
			else if( (nCmdNo >= (I2C_CTRL_CH_EIB_1_SHUNT1_CURR + nDeviceNo * I2C_CHANNEL_EIB_SPACE))
				&& (nCmdNo <= (I2C_CTRL_CH_EIB_1_SHUNT3_VOLT + nDeviceNo * I2C_CHANNEL_EIB_SPACE)) )
			{
				s_I2cControllStatus.BatShuntPara[nDeviceNo][nCmdNo-I2C_CTRL_CH_EIB_1_SHUNT1_CURR - nDeviceNo * I2C_CHANNEL_EIB_SPACE].f_value 
					= uSetVal.f_value;
			}
			// set battery number to EIB2
			else if(nCmdNo == (I2C_CTRL_CH_EIB_1_BATT_NUM + nDeviceNo * I2C_CHANNEL_EIB_SPACE))  
			{
				//s_I2cControllStatus.BattNum[nDeviceNo].f_value = uSetVal.f_value;
				s_I2cControllStatus.BattNum[nDeviceNo] = uSetVal.f_value;
			}
			// set voltage type to EIB2
			else if(nCmdNo == (I2C_CTRL_CH_EIB_1_VOLT_TYPE + nDeviceNo * I2C_CHANNEL_EIB_SPACE))  
			{
				//s_I2cControllStatus.VoltageType[nDeviceNo].f_value = uSetVal.f_value;
				s_I2cControllStatus.VoltageType[nDeviceNo] = uSetVal.f_value;
			}
			// set loader number to EIB2
			else if(nCmdNo == (I2C_CTRL_CH_EIB_1_LOAD_NUM + nDeviceNo * I2C_CHANNEL_EIB_SPACE))  
			{
				//s_I2cControllStatus.LoadNum[nDeviceNo].f_value = uSetVal.f_value;
				s_I2cControllStatus.LoadNum[nDeviceNo] = uSetVal.f_value;
			}
			else
			{
				return FALSE;
			}


		} //END: if(nMatchFlag == TRUE)
	}
	// special channel for the control command of scanning board 
	else if(nCmdNo == I2C_SCAN_BOARDS_CMD_CHD)
	{
		if( (int)uSetVal.f_value) 
		{
			s_I2cControllStatus.bNeedScanBoards = TRUE;
		}		
	}
	else
	{
		return FALSE;
	}	

	return TRUE;
}
/*==========================================================================*
* FUNCTION : StrToK
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN OUT char  *S   : 
*            IN OUT char  *D   : 
*            IN char      cSep : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : LIXIDONG                 DATE: 2006-05-17 13:57
*==========================================================================*/

static int StrToK(IN OUT char *S, IN OUT char *D, IN char cSep)
{
	int i=0;

	if((!S) || (!D))
	{
		return 0;
	}

	while( S[i] && (S[i] != cSep))
	{
		D[i] = S[i];
		i++;
	}

	D[i] = 0;

	//if(S[i])   Modify bug
	if(S[i] && (i !=0))
	{
		return (i + 1);
	}

	return 0;
}
/*==========================================================================*
* FUNCTION : Control
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hComm    : 
*            int     nUnitNo  : 
*            char    *pCmdStr : 
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-13 15:21
*==========================================================================*/


DLLExport BOOL Control(HANDLE hComm, int nUnitNo, char *pCmdStr ) 
{

	char sTarget[128] = {0};
	int nPoint1 = 0;
	//int param1 = 0;
	int nPoint, nCmdNo;
	union PARA_CONTROL param1;
	//int   iOutput;

	TRACE("================enter the control of I2C device =======%s!\n", pCmdStr);
	ASSERT( hComm!=0 && pCmdStr!=NULL );    

	//Get nCmdNo
	nPoint = StrToK( pCmdStr, sTarget, ',' );
	TRACE("================1.sTarget =======%s\n", sTarget);
	nCmdNo = (int)atoi( sTarget );
	TRACE("================1.nCmdNo =======%d\n", nCmdNo);
	nPoint1 += nPoint;
	TRACE("================1.nPoint1 =======%d\n", nPoint1);

	//����1
	if(nPoint <= 0)
	{
	    return FALSE;	
	}

	//����2
	nPoint = StrToK( pCmdStr+nPoint1, sTarget, ',' );
	if( sTarget[0] == 0 )    
	{
	    return FALSE;
	}
	param1.f_value = atof( sTarget );	
	TRACE("----parameter param1= %f ----nCmdNo=%d---------\n",param1.f_value,nCmdNo);

	
	return ExecuteControl(nCmdNo, param1);

}








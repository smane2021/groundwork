/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : i2c_sampler_main.h
*  CREATOR  : Kenn Wan                DATE: 2008-06-19 17:04
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#ifndef __I2C_SAMPLER_MAIN_H
#define __I2C_SAMPLER_MAIN_H

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "public.h"
#include "../sampler/main_board/main_board.h"

#define DLLExport 
typedef BOOL ( CALLBACK *ENUMSIGNALPROC )( int, float, LPVOID );



#endif

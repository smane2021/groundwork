//////////////////////////////////////////////////////////////////////////////////////
// 适用设备描述：ACU型												//
// 文件名：		acueem.cpp														//
// 版本号：		V4.0																//		
// 程序员姓名：	NICHOLAS																//	
// 开发日期：	2007.11.21															//														//
// 开发工具：	VC++ 6.0 											//
//////////////////////////////////////////////////////////////////////////////////////
// 共用头文件的定义，适用于 VC++ 和 BC++部分
//#include "../../include/stdsys.h"
//#include "../../include/public.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>

//#define DEBUG_EEM

// 本程序是智能设备协议解析的主程序，本段宏定义主要是用于DLL、TSR、OCE
// 及协议编程调测时使用
// 使用方法：
//          驱动程序类型                                应包括的宏定义
//          动态库(支持直连、VP6000、OCI-5)             _DLL_
//          现场调试用应用程序　                        _DLL_,_DEBUG_
//          AMS-1的TSR                                  _TSR_
//          OCE的DRV                                    _OCE_,_TSR_
#ifdef _WINDOWS
#define _DLL_
#endif

#ifdef _CONSOLE
#define _DLL_
#define _DEBUG_
#endif

#define _OCE_        // OCE驱动程序用

#ifdef  _DLL_
#undef  _OCE_    // DLL驱动程序用
#endif
#ifdef  _TSR_
#undef  _OCE_    // TSR驱动程序用
#endif
#ifdef  _OCE_
#define  _TSR_   // TSR驱动程序用
#endif

// 动态库使用部分
#ifdef _DLL_
#include "local.h"
#include "snuoci5.h"

// 跟踪测试标志：TURE－跟踪测试运行，要调试信息；FALSE－普通运行。
BOOL bTestFlag = FALSE;

// 跟踪测试用的数据记录文件名，请根据具体协议更改！
char HEX_FILE[]={"acu.log"};
char ASC_FILE[]={"acu.asc"};
char ErrLog_FILE[]={"acu.Log"};

// 如果记录告警发生时的采集原始数据请将bWriteErrLog置位。
BOOL bWriteErrLog=FALSE;
//BOOL bWriteErrLog=TRUE;
#define HEXLOG_FILE  "acueemLog.HEX"    // 告警记录文件名称

#endif  //end #ifdef _DLL_

#ifndef _OCE_
#ifdef _TSR_
#include "struct.h"
#endif
#endif

#ifdef _OCE_
#include "Comset.h"
#ifndef _TSR_
#define _TSR_
#endif
#endif

#ifdef _TSR_
#define HIBYTE(w)   ((BYTE) (((WORD) (w) >> 8) & 0xFF))
#define LOBYTE(w)   ((BYTE) (w))
#endif

//{以下部分为系统所用参数(常数并大写)
#define MAXMODULENUM			15
//#define SIGNALMAX			20*15
#define nMaxRevBuf			550
#define TXSIZE				550
#define HEADLEN				16
#define POLLLEN				9
#define ACK				0x06

#define	WHOLESTEP			3   
#define	UPDATETIMES			3
#define RectValNum			20
#define SIGNALMAX                       7000

char SendHead[HEADLEN]={0x04,'0','1','0','0','0','0','F',0x01,'0','1','0','0','0','0',0x02};
char Poll[POLLLEN]={0x04,'0','1','0','0','0','0','P',0x05};

union iNum61Union
{
	float	fValue;
	int	iValue;
};

union iNum61Union iNum61;

union iNum62Union
{
	float	fValue;
	int	iValue;
};

union iNum62Union iNum62;

static float p[SIGNALMAX];
static int flage = 0;
static int DCFlage = 1;
static int BatFlage = 62;
static int iSampleTime = 10;
static int iNum61Temp = 0;
static int iNum62Temp = 0;


union unSAMPLING_VALUE
{
	float	fValue;
	int	iValue;
	UINT	uiValue;
	ULONG	ulValue;
	DWORD	dwValue;
};
typedef union unSAMPLING_VALUE SAMPLING_VALUE;
static SAMPLING_VALUE NoValid;
static SAMPLING_VALUE Digit_1;
static SAMPLING_VALUE Digit_0;





//通讯数据帧信息
struct FrameInfomation
{
	int	 FrameLen;				//接受数据包长度
	char Address[7];			//信号组地址
};
struct FrameInfomation FrameInfo[]=
{
	{400,   "0000"},				// Address: 0000		
	//{ 88,   "6100"},				// Address: 6100	

	{ 98,   "6101"},
	{ 98,   "6102"},
	{ 98,   "6103"},
	{ 98,   "6104"},
	{ 98,   "6105"},
	{ 98,   "6106"},
	{ 98,   "6107"},
	{ 98,   "6108"},
	{ 98,   "6109"},
	{ 98,   "610A"},
	{ 98,   "610B"},
	{ 98,   "610C"},
	{ 98,   "610D"},
	{ 98,   "610E"},
	{ 98,   "610F"},

	{ 98,   "6110"},
	{ 98,   "6111"},
	{ 98,   "6112"},
	{ 98,   "6113"},
	{ 98,   "6114"},	
	{ 98,   "6115"},
	{ 98,   "6116"},
	{ 98,   "6117"},
	{ 98,   "6118"},
	{ 98,   "6119"},
	{ 98,   "611A"},
	{ 98,   "611B"},
	{ 98,   "611C"},
	{ 98,   "611D"},
	{ 98,   "611E"},
	{ 98,   "611F"},

	{ 98,   "6120"},
	{ 98,   "6121"},
	{ 98,   "6122"},
	{ 98,   "6123"},
	{ 98,   "6124"},
	{ 98,   "6125"},
	{ 98,   "6126"},
	{ 98,   "6127"},
	{ 98,   "6128"},
	{ 98,   "6129"},	
	{ 98,   "612A"},
	{ 98,   "612B"},
	{ 98,   "612C"},
	{ 98,   "612D"},
	{ 98,   "612E"},
	{ 98,   "612F"},

	{ 98,   "6130"},
	{ 98,   "6131"},
	{ 98,   "6132"},
	{ 98,   "6133"},
	{ 98,   "6134"},
	{ 98,   "6135"},
	{ 98,   "6136"},
	{ 98,   "6137"},
	{ 98,   "6138"},
	{ 98,   "6139"},
	{ 98,   "613A"},
	{ 98,   "613B"},
	{ 98,   "613C"},

	/*{ 98,   "613D"},
	{ 66,   "613E"},
	{ 66,   "613F"},

	{ 98,   "6140"},
	{ 98,   "6141"},
	{ 98,   "6142"},
	{ 98,   "6143"},
	{ 98,   "6144"},
	{ 98,   "6145"},
	{ 98,   "6146"},
	{ 98,   "6147"},
	{ 98,   "6148"},
	{ 98,   "6149"},
	{ 98,   "614A"},
	{ 98,   "614B"},
	{ 98,   "614C"},
	{ 98,   "614D"},
	{ 66,   "614E"},
	{ 66,   "614F"},

	{ 98,   "6150"},
	{ 98,   "6151"},
	{ 98,   "6152"},
	{ 98,   "6153"},
	{ 98,   "6154"},
	{ 98,   "6155"},
	{ 98,   "6156"},
	{ 98,   "6157"},
	{ 98,   "6158"},
	{ 98,   "6159"},
	{ 98,   "615A"},
	{ 98,   "615B"},
	{ 98,   "615C"},
	{ 98,   "615D"},
	{ 66,   "615E"},
	{ 66,   "615F"},

	{ 98,   "6160"},
	{ 98,   "6161"},
	{ 98,   "6162"},
	{ 98,   "6163"},
	{ 98,   "6164"},
	{ 98,   "6165"},
	{ 98,   "6166"},
	{ 98,   "6167"},
	{ 98,   "6168"},
	{ 98,   "6169"},*/

	{ 78,   "6200"},

	{ 98,   "6201"},
	{ 98,   "6202"},
	{ 98,   "6203"},
	{ 98,   "6204"},
	{ 98,   "6205"},
	{ 98,   "6206"},
	{ 98,   "6207"},
	{ 98,   "6208"},
	{ 98,   "6209"},
	{ 98,   "620A"},
	{ 98,   "620B"},
	{ 98,   "620C"},
	{ 98,   "620D"},
	{ 98,   "620E"},
	{ 98,   "620F"},

	{ 98,   "6210"},
	{ 98,   "6211"},
	{ 98,   "6212"},
	{ 98,   "6213"},
	{ 98,   "6214"}

};

//信号数据转换信息
struct CSignal
{
	char	cType;				// 信号类型   S:开关量   A:模拟量
	short	iByteNo;			// 字节号
	char	nBit;				// 开关量处理bit位置
	int  nChanel;                           // 通道号,if nChanel = -1 then value = -9999,means signal not using! for 61xx & 62xx it's excursion
};

struct CSignal pWholeSignal[][100]=             //(0)0000,(1)61xx1,(2)61xx2,(3)61xx3,(4)61xx4,(5)6200,(6)62xx1,(7)62xx2,(8)62xx3,
{                                                                                                         //head +    data   + ! + date + last 
	{							// group adress:0000 Read CSU Block:0000,len = 19   + (379 + 1) + 1 +  12  +   3   = 415
		//Analogue Inputs
		//{'A',  0, 0,-1},			// 0	System voltage          
		//{'A',  8, 0,-1},			// 1	System load
		//{'A', 16, 0,-1},			// 2    Date of latest batterytest
		//{'A', 24, 0,-1},			// 3	Remaining chargetime in hours
		//{'A', 32, 0,-1},			// 4    Next cyclic charging
		//{'A', 40, 0,-1},			// 5    Cell diff alarm threshold
		//{'A', 48, 0,-1},			// 6    Cell fiff alarm threshold

		//Analogue Outputs
		//{'A', 57, 0,-1},			// 7	Undervoltage 1 threshold 
		//{'A', 65, 0,-1},			// 8	Undervoltage 2 threshold
		//{'A', 73, 0,-1},			// 9    Reserved
		//{'A', 81, 0,-1},			//10	Rect reconnect time after mainsfail
		//{'A', 89, 0,-1},			//11    Automatic charge factor
		//{'A', 97, 0,-1},			//12    Maximum allowed charge time
		//{'A',105, 0,-1},			//13    Cycle charge period
		//{'A',113, 0,-1},			//14	Cyclic charge time
		//{'A',121, 0,-1},			//15	Manual charge time
		//{'A',129, 0,-1},			//16    Overvoltage threshold
		//{'A',137, 0,-1},			//17	Mains failure delay time
		//{'A',145, 0,-1},                        //18    Nominal system voltage
		//{'A',153, 0,-1},                        //19    Endvoltage for batterytest
		//{'A',161, 0,-1},                        //20    Max duration for batterytest
		//{'A',169, 0,-1},                        //21    Automatic batterytest intervall
		//{'A',177, 0,-1},                        //22    Number of battery cells 
		//{'A',185, 0,-1},                        //23    Delay before new batterytest
		//{'A',193, 0,-1},                        //24    Rect voltage during battery test
		//{'A',201, 0,-1},                        //25    Battery temperature compensation
		//{'A',209, 0,-1},                        //26    Autocharge level U2
		//{'A',217, 0,-1},                        //27    Autocharge level U1
		//{'A',225, 0,-1},                        //28    Min time between two chargings
		//{'A',233, 0,-1},                        //29    Rect voltage during battery charge
		//{'A',241, 0,-1},                        //30    Battery current limit
		//{'A',249, 0,-1},                        //31    Battery current limit
		//{'A',257, 0,-1},                        //32    Total rectifier power limit
		//{'A',265, 0,-1},                        //33    Battery trip hysteresis
		//{'A',273, 0,-1},                        //34    Lowvoltage threshold
		//{'A',281, 0,-1},                        //35    Highvoltage threshold
		//{'A',289, 0,-1},                        //36    High distribution current threshold
		//{'A',297, 0,-1},                        //37    Current limit in slave mode
		//{'A',305, 0,-1},                        //38    Delta voltage in slave mode
		//{'A',313, 0,-1},                        //39    Charge voltage in slave mode
		//{'A',321, 0,-1},                        //40    High voltage hysterisis
		//{'A',329, 0,-1},                        //41    Overvoltage hysterisis
		//{'A',337, 0,-1},                        //42    High distribution current hysterisis
		//{'A',345, 0,-1},                        //43    Energy save battery current
		//{'A',353, 0,-1},                        //44    Alam delay after battery test

		//Digital Inputs
		//{'S',362, 3,-1},			//45    Mains failure on system
		{'S',362, 2, 1},			//46    Undervoltage 1
		{'S',362, 1, 2},			//47	Undervoltage 2 
		{'S',362, 0, 3},			//48	Overvoltage
		{'S',363, 3, 4},			//49	Communication fault
		//{'S',363, 2,-1},			//50	Automatic charge in progress
		//{'S',363, 1,-1},			//51	Manual charge in progress
		//{'S',363, 0,-1},			//52	Cyclic charge in progress
		//{'S',364, 3,-1},			//53    System voltage error
		//{'S',364, 2,-1},			//54    Battery test in progress
		//{'S',364, 1,-1},			//55	Battery failure when tested 
		//{'S',364, 0,-1},			//56	Double failure on rectifiers
		{'S',365, 3, 5},			//57	External alarm 1
		{'S',365, 2, 6},			//58	External alarm 2
		{'S',365, 1, 7},			//59	External alarm 3
		{'S',365, 0, 8},			//60	External alarm 4
		{'S',366, 3, 9},			//61    External alarm 5
		{'S',366, 2,10},			//62    External alarm 6
		{'S',366, 1,11},			//63	External alarm 7
		{'S',366, 0,12},			//64	External alarm 8
		{'S',367, 3,13},			//65	External communication fault
		{'S',367, 2,14},			//66	Battery current limit alarm 
		//{'S',367, 1,-1},			//67	Diff between wanted and actual sysvolt
		//{'S',367, 0,-1},			//68	Mains failure indication 
		//{'S',368, 3,-1},			//69    All rectifiers give overload
		//{'S',368, 2,-1},			//70    Low voltage alarm 
		//{'S',368, 1,-1},			//71	High voltage alarm
		//{'S',368, 0,-1},			//72	High current alarm 
		//{'S',369, 3,-1},			//73	Energy savings is active
		//{'S',369, 2,-1},			//74	More than one rectifier is off


		//Digital Outputs
		//{'S',371, 3,-1},			//75	Inhibit outgoing alarms
		//{'S',371, 2,-1},			//76	Reset overvoltage
		//{'S',371, 1,-1},			//77    Start manual charging
		//{'S',371, 0,-1},			//78    Reset high voltage
		//{'S',372, 3,-1},			//79	Inhibit charging
		//{'S',372, 2,-1},			//80	Save anal/digout to EEPROM
		//{'S',372, 1,-1},			//81	Restore anal/dig to factory 
		//{'S',372, 0,-1},			//82	Inhibit battery trip
		//{'S',373, 3,-1},			//83	Perform batterytest
		//{'S',373, 2,-1},			//84	Reset battery failure
		//{'S',373, 1,-1},			//85    Perform automatic batterytest
		//{'S',373, 0,-1},			//86    Legacy rectifier support
		//{'S',374, 3,-1},			//87	Activate auto charging
		//{'S',374, 2,-1},			//88	Activate cyclic charging
		//{'S',374, 1,-1},			//89	Automatic overvoltage reset
		//{'S',374, 0,-1},			//90	Automatic highvoltage reset
		//{'S',375, 3,-1},			//91	Master slave configuration 
		//{'S',375, 2,-1},			//92	Inhibit distribution trip
		//{'S',375, 1,-1},			//93    Activate energy savings

		{0,0,0,0}
	},
		// group adress:61xx1 Read DC Distribution Unit Type 1 ,len = 19 + (42+1) + 1 + 12 + 3 = 78
	{                           
		//Analogue Inputs
		{'A',  0, 0, 4},			// 0	Distribution voltage          
		{'A',  8, 0, 5},			// 1	Distribution current
		{'A', 16, 0, 6},			// 2    Distribution temperature

		//Analogue Outputs
		//{'A', 25, 0,-1},			// 3	Size of current shunt A/mV

		//Digital Inputs
		//{'S', 34, 3,-1},			// 4    Uncalibrated           
		//{'S', 34, 2,-1},			// 5    EEPROM failure
		{'S', 34, 1, 0},			// 6    Distribution fuse tripped  
		//{'S', 34, 0,-1},			// 7	Spare 1 
		//{'S', 35, 3,-1},			// 8	Spare 2
		//{'S', 35, 2,-1},			// 9    Spare 3
		//{'S', 35, 1,-1},			//10	Spare 4
		//{'S', 35, 0,-1},			//11    Spare 5

		//Digital Outputs
		//{'S', 39, 3,-1},			//12    Save to EEPROM
		//{'S', 39, 2,-1},			//13    Spare 1
		//{'S', 39, 1,-1},			//14	Spare 1
		//{'S', 39, 0,-1},			//15	Spare 1
		//{'S', 40, 2,-1},			//16    Inhibit Fuse Alarm

		{0,0,0,0}
	},
		// group adress:61xx2 Read DC Distribution Unit Type 2 ,len = 19 + (35 + 1) + 1 + 12 + 3 = 71
	{													
		//Analogue Inputs
		{'A',   0, 0, 4},			// 0	Distribution voltage          
		{'A',  16, 0, 6},			// 2	Distribution temperature

		//Analogue Outputs

		//Digital Inputs
		//{'S', 26, 3,-1},			// 0    Uncalibrated
		//{'S', 26, 2,-1},			// 1	EEPROM failure
		{'S', 26, 1, 0},			// 2    Distribution fuse tripped
		//{'S', 26, 0,-1},			// 3    Spare 1
		//{'S', 27, 3,-1},			// 4    Spare 2
		//{'S', 27, 2,-1},			// 5	Spare 3 
		//{'S', 27, 1,-1},			// 6	Spare 4
		//{'S', 27, 0,-1},			// 7    Spare 5

		//Digital Outputs
		//{'S', 31, 3,-1},			// 0	Save to EEPROM
		//{'S', 31, 2,-1},			// 1    Spare 1
		//{'S', 31, 1,-1},			// 2    Spare 1
		//{'S', 31, 0,-1},			// 3    Spare 1
		//{'S', 32, 2,-1},			// 5	Inhibit Fuse Alarm

		{0,0,0,0}
	},
		// group adress:61xx3 Read DC Distribution Unit Type 3 ,len = 19 + (46 + 1) + 1 + 12 + 3 = 82
	{                             
		//Analogue Inputs
		{'A',   8, 0, 5},			// 1	Distribution current          
		{'A',  16, 0, 6},			// 2	Ambient temperature

		//Analogue Outputs
		//{'A', 25, 0,-1},			// 0    Shunt factor (A/mV)

		//Digital Inputs
		//{'S', 34, 3,-1},			// 0	Uncalibrated
		//{'S', 34, 2,-1},			// 1    EEPROM failure
		{'S', 34, 1, 0},			// 2    Distribution fuse tripped
		//{'S', 34, 0,-1},			// 3    Invalid temperature
		//{'S', 35, 3,-1},			// 4	Contactor control state  1=open
		//{'S', 35, 2,-1},			// 5	MOCB 1 control state     1=open
		//{'S', 35, 1,-1},			// 6    MOCB 2 control state     1=open
		//{'S', 35, 0,-1},			// 7	MOCB 3 control state     1=open
		//{'S', 36, 3,-1},			// 8    MOCB 4 control state     1=open
		//{'S', 36, 2,-1},			// 9    Contactor failure
		//{'S', 36, 1,-1},			//10    MOCB 1 failure
		//{'S', 36, 0,-1},			//11	MOCB 2 failure
		//{'S', 37, 3,-1},			//12	MOCB 3 failure
		//{'S', 37, 2,-1},			//13    MOCB 5 failure
		//{'S', 37, 1,-1},			//14	Internal fuse off

		//{'S', 40, 3,-1},                        //24    Dip_switch 8 position
		//{'S', 40, 2,-1},                        //25    Dip_switch 7 position
		//{'S', 40, 1,-1},                        //26    Dip_switch 6 position
		//{'S', 40, 0,-1},                        //27    Contactor in use
		//{'S', 41, 3,-1},                        //28    MOCB 4 priority  1=high priority 
		//{'S', 41, 2,-1},                        //29    MOCB 3 priority  1=high priority
		//{'S', 41, 1,-1},                        //30    MOCB 2 priority  1=high priority
		//{'S', 41, 0,-1},                        //31    MOCB 1 priority  1=high priority

		//Digital Outputs
		//{'S', 43, 2,-1},                        //1    Inhibit control
		//{'S', 43, 1,-1},                        //2    Tripp high priority load
		//{'S', 43, 0,-1},                        //3    Tripp low priority load
		//{'S', 44, 3,-1},                        //4    0 -> TRIP_HIGH/LOW_613 NOK (not ok)

		{0,0,0,0}
	},
		// group adress:61xx4 Read DC Distribution Unit Type 4 ,len = 19 + (102 + 1) + 1 + 12 + 3 = 138
	{							

		//Analogue Inputs
		{'A',  8, 0, 5},			// 1	Distribution current 1          
		{'A', 16, 0, 6},			// 2	Ambient temperature
		{'A', 24, 0, 7},			// 3    Distribution current 2
		{'A', 32, 0, 8},			// 4	Distribution voltage fuse 2

		//Analogue Outputs
		//{'A', 41, 0,-1},			// 0    Shunt factor 1 (A/mV)
		//{'A', 49, 0,-1},			// 1    Shunt factor 2 (A/mV)
		//{'A', 57, 0,-1},			// 2    Disconnect voltage contactor 1
		//{'A', 65, 0,-1},			// 3	Reconnect voltage contactor 1
		//{'A', 73, 0,-1},			// 4	Disconnect voltage contactor 2
		//{'A', 81, 0,-1},			// 5    Reconnect voltage contactor 2

		//Digital Inputs
		//{'S', 90, 3,-1},			//0	Uncalibrated
		//{'S', 90, 2,-1},			//1    EEPROM failure
		{'S', 90, 1, 0},			//2    Distribution fuse 1 tripped
		//{'S', 90, 0,-1},			//3    Invalid temperature
		//{'S', 91, 3,-1},			//4	Contactor 1 state          1=open
		//{'S', 91, 2,-1},			//5	MOCB 1 control state       1=open
		//{'S', 91, 1,-1},			//6    MOCB 2 control state       1=open
		//{'S', 91, 0,-1},			//7	MOCB 3 control state       1=open
		//{'S', 92, 3,-1},                        //8    MOCB 4 control state       1=open
		{'S', 92, 2, 1},                        //9    Contactor 1 failure
		//{'S', 92, 1,-1},                        //10    MOCB 1 failure
		//{'S', 92, 0,-1},                        //11    MOCB 2 failure
		//{'S', 93, 3,-1},                        //12    MOCB 3 failure 
		//{'S', 93, 2,-1},                        //13    MOCB 5 failure
		//{'S', 93, 1,-1},                        //14    Internal fuse off
		//{'S', 93, 0,-1},                        //15    Not in use
		{'S', 94, 3, 2},                        //16    Distribution fuse 2 tripped
		//{'S', 94, 2,-1},                        //17    Contactor 2 state          1=open
		{'S', 94, 1, 3},                        //18    Contactor 2 failure 
		//{'S', 94, 0,-1},                        //19    Contactor 1 disconnected
		//{'S', 95, 3,-1},                        //20    Contactor 2 disconnected 
		//{'S', 95, 2,-1},                        //21    Disconnection warning
		//{'S', 95, 1,-1},                        //22    Not in use
		//{'S', 95, 0,-1},                        //23    Not in use
		//{'S', 96, 3,-1},                        //24    Dip_switch 8 position
		//{'S', 96, 2,-1},                        //25    Dip_switch 7 position  
		//{'S', 96, 1,-1},                        //26    Contactor 2 in use
		//{'S', 96, 0,-1},                        //27    Contactor 1 in use
		//{'S', 97, 3,-1},                        //28    Dip_switch 4 position
		//{'S', 97, 2,-1},                        //29    Dip_switch 3 position
		//{'S', 97, 1,-1},                        //30    Dip_switch 2 position
		//{'S', 97, 0,-1},                        //31    Dip_switch 1 position

		//Digital Outputs
		//{'S',99, 2,-1},                        //1    
		//{'S',100, 2,-1},                        //5    Inhibit Fuse Alarm 1
		//{'S',100, 1,-1},                        //6    Inhibit Fuse Alarm 2

		{0,0,0,0}
	},
		// group adress:6200 Read Battery Group  len = 19 + (62 + 1) + 1 + 12 + 3 = 98
	{							
		//Analog inputs
		{'A',  0, 0,4000},			// 0	Battery voltage
		{'A',  8, 0,4001},			// 1	Battery current 
		{'A', 16, 0,4002},			// 2	Battery temperature

		//Analog outputs
		//{'A', 25, 0,-1},			// 0	Low battery temp limit 
		//{'A', 33, 0,-1},			// 1	High battery temp limit
		//{'A', 41, 0,-1},			// 2	Sysvoltage when very high temp
		//{'A', 49, 0,-1},			// 3	Sysvolt when v high temp & disconnect

		//Digital Inputs

		//Digital Outputs
		//{'S',59, 3,-1},                        //0    Save to EEPROM
		//{'S',59, 2,-1},                        //1    
		//{'S',59, 1,-1},                        //2    


		{0,0,0,0}
	},
		// group adress:62xx1 Read Battery Unit Type 1 ,len = 19 + (82 +1) + 1 + 12 + 3 = 118
	{							
		//Analogue Inputs
		{'A',  0, 0, 2},			// 0	Battery voltage          
		{'A',  8, 0, 3},			// 1	Battery current
		{'A', 16, 0, 4},			// 2    Battery temperature
		//{'A', 24, 0,-1},			// 3	Battery rest time, from LCC
		//{'A', 32, 0,-1},			// 4    Battery status, from LCC
		//{'A', 40, 0,-1},			// 5    Nominell battery capacity, from LCC
		//{'A', 48, 0,-1},			// 6    Used capacity to end voltage (%)

		//Analogue Outputs
		//{'A', 57, 0,-1},			// 0	Size of current shunt A/mV 
		//{'A', 65, 0,-1},			// 1	Batterytrip voltage


		//Digital Inputs
		//{'S', 74, 3,-1},			//0     Uncalibrated
		//{'S', 74, 2,-1},			//1     EEPROM failure
		{'S', 74, 1, 0},			//2     Battery fuse tripped
		//{'S', 74, 0,-1},			//3     Spare 1
		//{'S', 75, 3,-1},			//4     Spare 2
		//{'S', 75, 2,-1},			//5	Spare 3
		//{'S', 75, 1,-1},			//6	Spare 4
		//{'S', 75, 0,-1},			//7     Spare 5

		//Digital Outputs
		//{'S', 79, 3,-1},			//0	Save to EEPROM
		//{'S', 79, 2,-1},                    //1     Disconnect battery 
		//{'S', 79, 1,-1},                    //2     Disable battery disconnect
		//{'S', 79, 0,-1},                    //3     Spare 1 
		//{'S', 80, 2,-1},                    //5     Inhibit battery fuse alarm


		{0,0,0,0}
	},
		// group adress:62xx2 Read Battery Unit Type 2 ,len = 19 + (50 + 1) + 1 + 12 + 3 = 86
	{							
		//Analogue Inputs
		{'A',   0, 0, 2},			// 0	Battery voltage          
		{'A',  16, 0, 4},			// 2	Battery temperature

		//Analogue Outputs
		//{'A', 33, 0,-1},			// 1    Batterytrip voltage

		//Digital Inputs
		//{'S', 42, 3,-1},			// 0	Uncalibrated
		//{'S', 42, 2,-1},			// 1    EEPROM failure
		{'S', 42, 1, 0},			// 2    Battery fuse tripped
		//{'S', 42, 0,-1},			// 3    Spare 1
		//{'S', 43, 3,-1},			// 4	Spare 1 
		//{'S', 43, 2,-1},			// 5	Spare 1
		//{'S', 43, 1,-1},			// 6    Spare 1
		//{'S', 43, 0,-1},			// 7	Spare 1

		//Digital Outputs
		//{'S', 47, 3,-1},			// 0    Save to EEPROM
		//{'S', 47, 2,-1},			// 1    Disconnect battery
		//{'S', 47, 1,-1},			// 2    Disable battery disconnect
		//{'S', 47, 0,-1},			// 3	Spare 1
		//{'S', 48, 3,-1},			// 5	Inhibit battery fuse alarm


		{0,0,0,0}
	},
		// group adress:62xx3 Read Battery Unit Type 3 ,len = 19 + (146 + 1) + 1 + 12 + 3 = 182
	{							
		//Analogue Inputs
		{'A',  0, 0, 2},			// 0	Battery 1 voltage          
		{'A',  8, 0, 3},			// 1	Battery 1 current
		{'A', 16, 0, 4},			// 2    Battery 1 temperature
		{'A', 24, 0, 5},			// 3	Battery 2 voltage
		{'A', 32, 0, 6},			// 4    Battery 2 current
		{'A', 40, 0, 7},			// 5    Battery 2 temperature
		//{'A', 48, 0,-1},			// 6    
		//{'A', 56, 0,-1},			// 7	System voltage 
		//{'A', 64, 0,-1},			// 8	Battery rest time, from CSU
		//{'A', 72, 0,-1},			// 9    Battery status, from CSU 
		//{'A', 80, 0,-1},			//10	Nominell battery capacity, from CSU
		//{'A', 88, 0,-1},			//11    Used capacity to end voltage (%)

		//Analogue Outputs
		//{'A', 97, 0,-1},			//12    Shunt factor 1 (A/mV)
		//{'A',105, 0,-1},			//13    Shunt factor 2 (A/mV)
		//{'A',113, 0,-1},			//14	Battery trip voltage
		//{'A',121, 0,-1},			//15	Battery trip hysterisis

		//Digital Inputs
		//{'S',130, 3,-1},			//0    Uncalibrated
		//{'S',130, 2,-1},			//1	EEPROM failure
		{'S',130, 1, 0},                        //2    Battery 1 fuse tripped
		//{'S',130, 0,-1},                        //3    Undervoltage battery 1 detected
		//{'S',131, 3,-1},                        //4    Contactor 1 present
		//{'S',131, 2,-1},                        //5    Battery 1 disconnected
		//{'S',131, 1,-1},                        //6    Contactor 1 failure 
		//{'S',131, 0,-1},                        //7    Over temperature battery 1
		//{'S',132, 3,-1},                        //8    Temperature sensor failure battery 1
		{'S',132, 2, 1},                        //9    Battery 2 fuse tripped
		//{'S',132, 1,-1},                        //10    Undervoltage battery 2 detected
		//{'S',132, 0,-1},                        //11    Contactor 2 present
		//{'S',133, 3,-1},                        //12    Battery 2 disconnected 
		//{'S',133, 2,-1},                        //13    Contactor 2 failure
		//{'S',133, 1,-1},                        //14    Over temperature battery 2 
		//{'S',133, 0,-1},                        //15    Temperature sensor failure battery 2
		//{'S',134, 3,-1},                        //16    Internal fuse off
		//{'S',134, 2,-1},                        //17    Manual state 1
		//{'S',134, 1,-1},                        //18    Manual state 2 

		{'S',136, 3, 8},                        //24    Battery 2 connected
		//{'S',136, 2,-1},                        //25    Dip_switch 7 position
		//{'S',136, 1,-1},                        //26    Dip_switch 6 position
		//{'S',136, 0,-1},                        //27    Dip_switch 5 position
		{'S',137, 3, 9},                        //28    Battery 1 connected 
		//{'S',137, 2,-1},                        //29    Dip_switch 3 position
		//{'S',137, 1,-1},                        //30    Dip_switch 2 position
		//{'S',137, 0,-1},                        //31    Dip_switch 1 position
		//{'S',138, 3,-1},                        //32    Low battery temperature detected
		//{'S',138, 2,-1},                        //33    High battery temperature detected 
		//{'S',138, 1,-1},			//34    Very high battery temperature detected
		//{'S',138, 0,-1},			//35    Contactor manually on 1
		//{'S',139, 3,-1},			//36	Contactor manually on 2
		//{'S',139, 2,-1},			//37	Contactor manually off 1 
		//{'S',139, 1,-1},			//38	Contactor manually off 2

		//Digital Outputs
		//{'S',143, 2,-1},			//1	Disable battery trip/disconnect
		//{'S',143, 1,-1},			//2	Disconnect battery 1
		//{'S',143, 0,-1},			//3	Disconnect battery 2
		//{'S',144, 3,-1},			//4    0 -> DISCON1/2_623 NOK (not ok)
		//{'S',144, 2,-1},			//5    Inhibit battery fuse alarm A
		//{'S',144, 1,-1},			//6	Inhibit battery fuse alarm B


		{0,0,0,0}
	}

};

//{以下部分为系统所用全局变量
char Temp_SendString[TXSIZE];
char Temp_ReceiveString[nMaxRevBuf];
//char Temp[50];					// 收发缓冲区
BOOL InitFlag = FALSE;
/////////////////////////////////////////////////////////////////////////////////////
//
char Info_chinese[] = {
	"     ACU 控制器单元                      \n"
	"        DLL接口程序	                     \n"
	"     版本号：V4.0                           \n"
	"   RS232/IP 数据传输方式：                     \n"
	"  2400，E，7，1                   \n"
	"                                            \n"
	"     地址为1（看面板）                      \n"
	"                                            \n"
	"     程序设计:PJC                        \n"
	"  创建日期：2006.12.13                      \n"
	"  适用范围:    DLL/Oci-5      	             \n"
	"                              	             \n"
	" 注意事项:                    	             \n"
	" 1、电源地址必须设置为1，方法祥见工程文档或 \n"
	"    设备操作手册。                          \n"
	" 本程序最后编译时间：\n"               // 请勿在此后增加信息!
	"                                \n"    // 请保留此行(分配内存用)
};


char Info[] = 
{
	"                                            \n"
	"   ACU Control Unit      \n"
	"                              \n"
	"   Version：V4.05                          \n"
	"										    \n"
	"   Communication mode：RS232/IP              \n"
	"      142.100.6.40:2000  \n"
	"   Communication parameter:    \n"
	"       2400,N,8,1(adjustable on the display)\n"
	"        \n"
	"        \n"
	"   Designer: PJC						    \n"
	"   Date of developing：2006.12.12          \n"
	"   Program modifier：          \n"
	"   Modifying reason：\n"
	"           \n"
	"   Notice: \n"
	"       1、the file name that traced testing:acu.ASC and acu.log\n" //注意修改！！
	"       2、\n"
	"   Last compiling time：\n"               // 请勿在此后增加信息!
	"                                \n"    // 请保留此行(分配内存用)
};

// 共用头文件的定义，适用于 VC++ 和 BC++部分
#ifdef _DLL_
// 功能：动态库版本中将信息包 Info 输出，以作版本信息等标志。
DLLExport char* DLLInfo( )
{
	int nStrLen = (int)strlen( Info );
	sprintf( Info+nStrLen-30, "%s ", __DATE__ );
	strcat( Info, __TIME__ );
	strcat( Info, " \n" );

	return Info;
}
#endif    


int ToUpper(char* Point){
	int i=0;
	while(Point[i]!=0){
		if(Point[i]>=97 && Point[i]<=122) Point[i]-=32;
		i++;
	}
	return (i==0)?0:i--;
}
//////////////////////////////////////////////////////////////////////////////////////
//
// 将浮点数格式 sStr转换成一个浮点数。
// 浮点数格式：尾数高位bit23-bit16；尾数中位bit15-bit8；尾数低位bit7-bit0；阶码bit7-bit0
// 转换方法：数值 = 尾数*2的阶码次方/2的23次方
double  pow2(double a,double  b)
{
	double  i,j; 
	j=1; 
	for(i=0;i<b;i++) 
		j*=a; 

	return j;
}
float FixFloatDat( char* sStr )
{
	// "0657EF6A" --> 43.9676
	char cWeiCode[7];
	char tempCode[9];
	strncpy( tempCode,sStr,8 );

	if(tempCode[0]=='7' && tempCode[1]=='F' && tempCode[2]=='F' && tempCode[3]=='F' && 
		tempCode[4]=='F' && tempCode[5]=='F' && tempCode[6]=='8' && tempCode[7]=='0' )
		return NoValid.fValue;

	strncpy( cWeiCode,sStr,6 );
	cWeiCode[6] = 0;
	long nWeiCode = strtol( cWeiCode,NULL,16 );
	if( nWeiCode>0x800000L )
		nWeiCode -= 0x1000000L;

	char cJieCode[3];
	strncpy( cJieCode,sStr+6,2 );
	cJieCode[2] = 0;
	long nJieCode = strtol( cJieCode,NULL,16 );
	if( nJieCode>0x80)
		nJieCode -= 0x100;
	if(nJieCode<0||nJieCode>23)
		return 0.0;
	else
		return  (float)nWeiCode/((float)pow2(2,23-nJieCode) );
}
//////////////////////////////////////////////////////////////////////////////////////
//发送通讯数据包接口函数
BOOL WriteData(HANDLE hComm,char* WriteString,DWORD WriteCount,DWORD* WrittenCount)
{
	BOOL result;


	//	if ( bContinue )
	PurgeComm(hComm,PURGE_TXCLEAR | PURGE_RXCLEAR);

	//#ifdef _DLL_
	result=WriteFile(hComm,WriteString,WriteCount,WrittenCount,NULL);
	/*#else
	int nTime =WriteCount/50;
	int nas = WriteCount%50;
	for(i=0;i<nTime;i++){
	WriteFile(hComm,WriteString+i*50,50,WrittenCount,NULL);
	bContinue = FALSE;
	//    Sleep(200);
	}
	result=WriteFile(hComm,WriteString+50*nTime,nas,WrittenCount,NULL);
	bContinue = TRUE;  
	//  Sleep(200);
	#endif
	*/
#ifdef _DLL_
	WriteAsc(ASC_FILE,"\r\nSend:\r\n");
	WriteAsc(HEX_FILE,"\r\nSend:\r\n");
	WriteAsc(ASC_FILE,"%s", WriteString);
	WriteHex(HEX_FILE,WriteString,(int)*WrittenCount);
#endif
	return result;
}

/*
//////////////////////////////////////////////////////////////////////////////////////
//接收通讯数据包接口函数
//最后一参数chEndCode与库函数ReadFile不同，为识别数据包结束码，当该值为NULL时，数据包无
//特定的结束码，否则该值指向的为特定的结束码字节。
BOOL ReadData(HANDLE hComm,char* ReadString,DWORD ReadCount,DWORD* ReadRICount,char* chEndCode)
{
BOOL result;
DWORD l=0,lRead;
#ifdef _TSR_
result=ReadFile(hComm,ReadString,ReadCount,ReadRICount,chEndCode);
#else
if(chEndCode==NULL)
result=ReadFile(hComm,ReadString,ReadCount,ReadRICount,NULL);
else{
*ReadRICount=0;
do {
ReadFile( hComm,ReadString+(*ReadRICount),1,&lRead,NULL);
(*ReadRICount)++;
}while( lRead!=0 && ReadString[*ReadRICount-1]!=*chEndCode && *ReadRICount<ReadCount );
if(lRead==0)
*ReadRICount-1;
}
#endif 

#ifdef _DLL_
WriteAsc(ASC_FILE,"\r\nRecv:\r\n");
WriteAsc(HEX_FILE,"\r\nRecv:\r\n");
WriteAsc(ASC_FILE,ReadString);
WriteHex(HEX_FILE,ReadString,(int)*ReadRICount);
#endif
return result;
}

*/

// 从设备 hComm 中向字符串 sRecStr 读入字符，参数个数请根据需要修改。
// 返回：实际接收的字符个数。
int ReadData(
	     HANDLE hComm,     // 通讯口句柄
	     BYTE* sRecStr,    // 接收字符串指针
	     int nStrLen       // 需要接收的字符个数
	     )
{
	DWORD lRead = 0;  //实际从设备读取的字符个数

	Sleep(100);

	if( nStrLen > 0 )
	{
		ReadFile( hComm, sRecStr, nStrLen, &lRead, NULL );
	}
	//2//////////////////////////////////////////////////////////
	// 对于只能按特殊结束字符的情况：如回车0x0D
	else
	{
		int i = 0;    // 计数器：接收字符个数
		do
		{
			ReadFile( hComm, (char*)sRecStr + i, 1, &lRead, NULL );
			if(lRead)
				i++;
			Sleep(10);
		}while( lRead == 1 && sRecStr[i-1] != 0x03 && i < nMaxRevBuf );
		ReadFile( hComm, (char*)sRecStr + i, 1, &lRead, NULL );
		if(lRead)
			i++;
		lRead = i;  // 为了方便后面的处理，将接收的字符个数统一保留。
		sRecStr[lRead] = 0;
	}

	/////////////////////////////////////////////////////////////////////
	//
	// 请注意 TSR.CPP文件 ReadFile函数中的超时时间 lTimeOut和结束字符 byEOI
	//
	//////////////////////////////////////////////////////////////////////

	// 跟踪调试的接收信息处理：回应信息记录。
#ifdef _DLL_
	WriteAsc( ASC_FILE, "\r\nRecv:%d char", (int)lRead );
	WriteAsc( HEX_FILE, "\r\nRecv:%d char", (int)lRead );
	WriteAsc( ASC_FILE, "%s", (char*)sRecStr );
	WriteHex( HEX_FILE, (char*)sRecStr, (int)lRead );
#endif    // end of #ifdef _DLL_

	return (int)lRead;
}
//////////////////////////////////////////////////////////////////////////////////////
//功能：将字符串S以cSep为分隔符分段。
//入口：S-源字符串，D-第一段字符串，cSep-分隔符。
//返回：第一段字符串的个数。
int StrToK( char *S, char *D, char cSep )
{
	int i = 0;
	while( S[i] && S[i] != cSep )
		D[i] = S[i++];
	D[i]=0;
	return (S[i] ? (i+1) : 0);
}
//////////////////////////////////////////////////////////////////////////////////////
// 校验和计算方法:
char BCC(char *lpFrame,int len)
{
	char     R=0;
	int      i;
	for( i=0; i<len; i++)
		R=R +lpFrame[i];
	R=R&0x7F;
	if(R<32) R=R+32;
	return R;
}
//////////////////////////////////////////////////////////////////////////////////////
//
void UpdateBCC(char* lpFrame,int len){
	lpFrame[len]=BCC(lpFrame,len);
}
//////////////////////////////////////////////////////////////////////////////////////
//
int GetModuleNumber(int nUnitNo,int k){
	int ModuleNumber;
	ModuleNumber=nUnitNo-k;
	return ModuleNumber;
}
//////////////////////////////////////////////////////////////////////////////////////
void  FixData(struct CSignal *pSignal, float *pData,int iChannelNo)
{
	char   temp;
	int    iLoop=0;
	int    iByteAddr;

	//printf("\n StartFixChannel = %d",iChannelNo);

	while(pSignal[iLoop].cType)
	{
		if(pSignal[iLoop].nChanel == -1)
		{
			switch(pSignal[iLoop].cType)
			{
			case 'A':
				pData[iChannelNo+pSignal[iLoop].nChanel] = NoValid.fValue;;
				break;
			case 'S':
				pData[iChannelNo+pSignal[iLoop].nChanel] = NoValid.fValue;;
				break;
			default:
				break;
			}

			iLoop++;
			continue;
		}

		iByteAddr=19+pSignal[iLoop].iByteNo;

		switch(pSignal[iLoop].cType)
		{
		case 'A':
			pData[iChannelNo+pSignal[iLoop].nChanel]=FixFloatDat(Temp_ReceiveString+iByteAddr);
			break;
		case 'S':
			if(Temp_ReceiveString[iByteAddr]>=0x30 && Temp_ReceiveString[iByteAddr]<=0x39)
				temp=Temp_ReceiveString[iByteAddr]-48;
			else	
				temp=Temp_ReceiveString[iByteAddr]-55;
			if( ((temp>>pSignal[iLoop].nBit) & 0x01) == 1 )
			{
				pData[iChannelNo+pSignal[iLoop].nChanel]=Digit_1.fValue;
			}
			else
			{
				pData[iChannelNo+pSignal[iLoop].nChanel]=Digit_0.fValue;
			}
			break;
		default:
			break;
		}
		iLoop++;
	}
}

/*
//////////////////////////////////////////////////////////////////////////////////////
MainComm(HANDLE hComm,int SendLen,int ReceiveLen)
{
DWORD lWritten=0,lRead=0; 

Sleep(100);
WriteData(hComm,Temp_SendString,SendLen,&lWritten);
ReadData(hComm,Temp,1,&lRead,NULL); 
if(lRead!=1)      return  FALSE;
if(Temp[0]!=ACK)  return  FALSE;		     

Sleep(100);
WriteData(hComm,Poll,9,&lWritten);
ReadData(hComm,Temp_ReceiveString,ReceiveLen,&lRead,NULL); 
if(lRead!=(DWORD)ReceiveLen) return FALSE;
if( Temp_ReceiveString[ReceiveLen-1] != BCC( Temp_ReceiveString +1,ReceiveLen-2) )
{
return FALSE;
}

Sleep(100);
Temp[0]=ACK;
WriteData(hComm,Temp,1,&lWritten);
ReadData(hComm,Temp,1,&lRead,NULL); 

return TRUE;
}
*/

//////////////////////////////////////////////////////////////////////////////////////
BOOL MainComm(HANDLE hComm,int SendLen,int ReceiveLen, int *pReceivedLen)
{
	DWORD lWritten=0;
	int lRead=0; 
	char Temp[50];					// 收发缓冲区
	int i ;

	WriteData(hComm,Temp_SendString,SendLen,&lWritten);
	lRead = ReadData(hComm,(BYTE*)Temp,1);
	//ReadData(hComm,Temp,1,&lRead,NULL); 
#ifdef DEBUG_EEM
	printf("\r\nlRead = %d\r\n",lRead);
#endif
	if(lRead!=1)      
	{
#ifdef DEBUG_EEM
		printf("\r\nError1! %d\r\n",lRead);
#endif
		return  FALSE;
	}
	if(Temp[0]!=ACK)  
	{
#ifdef DEBUG_EEM
		printf("\r\nError2!\r\n");
#endif
		return  FALSE;		     
	}
	Sleep(100);

#ifdef DEBUG_EEM
	printf("\r\nSample OK!\r\n");
#endif
#ifdef DEBUG_EEM
	for(i = 0; i< 50; i++)
		printf("%d",Temp_ReceiveString[i]);
#endif	
	WriteData(hComm,Poll,9,&lWritten);
	lRead = ReadData(hComm,(BYTE*)Temp_ReceiveString,0);
	//	lRead = ReadData(hComm,(BYTE*)Temp_ReceiveString,ReceiveLen);
	*pReceivedLen = lRead;
#ifdef DEBUG_EEM
	printf("\r\nResult lRead = %d\r\n",lRead);
#endif
#ifdef DEBUG_EEM
	//for(i = 0; i< 50; i++)
	printf("%s",&Temp_ReceiveString[0]);
#endif
	//ReadData(hComm,Temp_ReceiveString,ReceiveLen,&lRead,NULL); 
	//if(lRead!=(DWORD)ReceiveLen) return FALSE;
	//if( Temp_ReceiveString[ReceiveLen-1] != BCC( Temp_ReceiveString +1,ReceiveLen-2) )
	if(!lRead)
		return FALSE;
	if( Temp_ReceiveString[lRead-1] != BCC( Temp_ReceiveString +1,lRead-2) )
	{
#ifdef DEBUG_EEM
		printf("\r\nError in BCC!\r\n");
#endif	
		return FALSE;
	}

	Sleep(100);

	Temp[0]=ACK;
	WriteData(hComm,Temp,1,&lWritten);
	lRead = ReadData(hComm,(BYTE*)Temp,1);

	//ReadData(hComm,Temp,1,&lRead,NULL); 

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////////////////
BOOL GetModuleNum(HANDLE hComm)
{   
	BOOL result;
	int  Temp;
	memcpy(Temp_SendString,SendHead,HEADLEN);
	sprintf(Temp_SendString+HEADLEN,"RI*\x03");
	ToUpper(Temp_SendString+HEADLEN);
	UpdateBCC(Temp_SendString+9,11);

	result=MainComm(hComm,21,142,&Temp);
	return result;
}
//////////////////////////////////////////////////////////////////////////////////////
BOOL ReadWholeState(HANDLE hComm,int nEquipNumber,float* pData)
{
	//printf("\n----DCFlage = %d \n",DCFlage);
	//printf("\n----BatFlage = %d \n",BatFlage);

	int iChannelNo1=0;	
	BOOL result;
	int step;
	int iloop;
	int nQueryNum = 0;
	int TempLen = 0;
	int RecNum = 0;

	//if(nBatteryNumber < 0) //没有电池组
	//	nQueryNum = WHOLESTEP;

	//if(nBatteryNumber >= 0 && nBatteryNumber <= 3 )  //有电池组
	//	nQueryNum = WHOLESTEP + nBatteryNumber + 60 + 3 + 3;//max 3 battery , 3 DC distribute

	//if(nBatteryNumber > 3)  //有电池组
	//	nQueryNum = WHOLESTEP + 3;

	if(BatFlage == 82 || BatFlage == 62 + iNum62.iValue - 1)
	{
		BatFlage = 62;
	}

	if(DCFlage == 61 || DCFlage == 1 + iNum61.iValue - 1)
	{
		DCFlage = 1;
	}


	//nQueryNum = nEquipNumber;
	nQueryNum = 4;
	for(iloop = 0;iloop < nQueryNum; iloop++)
	{
		if(iloop == 0)
		{
			step = 0;
			if(iSampleTime != 10)
			{
				iChannelNo1+=39;
				continue;
			}
			else
			{
				iSampleTime = 0;
			}
		}
		else if(iloop == 1)
		{
			step = DCFlage;
			if(iNum61.iValue == 1)
			{
				continue;
			}
		}
		else if(iloop == 2)
		{
			step = 61;
		}
		else if(iloop == 3)
		{
			step = BatFlage;
			if(iNum62.iValue == 1)
			{
				continue;
			}
		}
		memset(Temp_ReceiveString,0 ,nMaxRevBuf);
		memcpy(Temp_SendString,SendHead,HEADLEN);
		sprintf(Temp_SendString+HEADLEN,"RB%04s*\x03",FrameInfo[step].Address);
		ToUpper(Temp_SendString+HEADLEN);
		UpdateBCC(Temp_SendString+9,15);

		//printf("---Step = %d---\n",step);

		//printf("\n----EquipAddress = %s\n",FrameInfo[step].Address);

		result= MainComm(hComm,25,FrameInfo[step].FrameLen,&TempLen);

		//printf("\n----ReceiveDataLen = %d\n",TempLen);

		if(!result)
		{
			break;
		}
		else
		{
			switch(TempLen)
			{

			case 414://0000
				//printf("\n----Fix 0000 Data \n");
				FixData(pWholeSignal[0],pData,iChannelNo1);
				iChannelNo1+=39;
				break;
			case 78://61xx1
				//printf("\n----Fix 61xx1 Data \n");
				iChannelNo1+=35*(DCFlage-1);
				FixData(pWholeSignal[1],pData,iChannelNo1);
				//iChannelNo1+=35;
				break;
			case 70://61xx2
				//printf("\n----Fix 61xx2 Data \n");
				iChannelNo1+=35*(DCFlage-1);
				FixData(pWholeSignal[2],pData,iChannelNo1);
				//iChannelNo1+=35;
				break;
			case 82://61xx3 
				//printf("\n----Fix 61xx3 Data \n");
				iChannelNo1+=35*(DCFlage-1);
				FixData(pWholeSignal[3],pData,iChannelNo1);
				//iChannelNo1+=35;
				break;
			case 138://61xx4
				//printf("\n----Fix 61xx4 Data \n");
				iChannelNo1+=35*(DCFlage-1);
				FixData(pWholeSignal[4],pData,iChannelNo1);
				//iChannelNo1+=35;
				break;
			case 98://6200
				//printf("\n----Fix 6200 Data \n");
				iChannelNo1=0;
				FixData(pWholeSignal[5],pData,iChannelNo1);
				iChannelNo1+=4005;
				break;
			case 118://62xx1
				//printf("\n----Fix 62xx1 Data \n");
				iChannelNo1+=50*(BatFlage-62);
				FixData(pWholeSignal[6],pData,iChannelNo1);
				//iChannelNo1+=50;
				break;
			case 86://62xx2
				//printf("\n----Fix 62xx2 Data \n");
				iChannelNo1+=50*(BatFlage-62);
				FixData(pWholeSignal[7],pData,iChannelNo1);
				//iChannelNo1+=50;
				break;
			case 182://62xx3
				//printf("\n----Fix 62xx3 Data \n");
				iChannelNo1+=50*(BatFlage-62);
				FixData(pWholeSignal[8],pData,iChannelNo1);
				if(pData[iChannelNo1+9] == 0)//Battery 1
				{
					pData[iChannelNo1+0] = NoValid.fValue;;
					pData[iChannelNo1+2] = NoValid.fValue;;
					pData[iChannelNo1+3] = NoValid.fValue;;
					pData[iChannelNo1+4] = NoValid.fValue;;
				}

				if(pData[iChannelNo1+8] == 0)//Battery 2
				{
					pData[iChannelNo1+1] = NoValid.fValue;;
					pData[iChannelNo1+5] = NoValid.fValue;;
					pData[iChannelNo1+6] = NoValid.fValue;;
					pData[iChannelNo1+7] = NoValid.fValue;;
				}

				//iChannelNo1+=50;
				break;
				//case 98:
				//	if(TempLen > 18)
				//	{
				//		RecNum++;
				//		FixData(pWholeSignal[3],pData, iChannelNo1);
				//		iChannelNo1+=21;
				//		break;
				//	}
				//	else if(TempLen == 18)
				//	{
				//		iChannelNo1 += 21 * (60 - RecNum);
				//		step += 60 - RecNum -1;
				//		break;
				//	}
			default:
				break;
			}
		}

		Sleep(100);
	}

	DCFlage++;
	BatFlage++;

	if(!result) 
	{
		return FALSE;
	}

	return TRUE;
}
//////////////////////////////////////////////////////////////////////////////////////
BOOL ReadModuleState(HANDLE hComm,int nModuleNu,float* pData){
	int iChannelNo2,nUpdate=0;
	BOOL result;\
		int i;
	int temp;
	for(i=0;i<nModuleNu;i++)
	{
		iChannelNo2=0;
		memcpy(Temp_SendString,SendHead,HEADLEN);
		sprintf(Temp_SendString+HEADLEN,"RB020%01x*\x03",i+1);
#ifdef	DEBUG_EEM
		printf(	Temp_SendString);	
#endif
		ToUpper(Temp_SendString+HEADLEN);
		UpdateBCC(Temp_SendString+9,15);
		result=MainComm(hComm,25,98,&temp);

		if(!result)
		{
			nUpdate++;
			if( nUpdate < 2 )
			{
				i--;
			}
			else
			{
				nUpdate = 0;
			}
			Sleep(500);
			continue;
		}
		else
		{
			nUpdate = 0;
			FixData(pWholeSignal[3],pData,iChannelNo2+i*RectValNum);
		}
		Sleep(100);
	}
	return TRUE;
}
//////////////////////////////////////////////////////////////////////////////////////
//
BOOL ReaddistrState(HANDLE hComm,int ndistrNumber,float* pData){
	BOOL result;
	int nTime,i;
	int iChannelNo3=0;
	int temp;
	for(nTime=0;nTime<UPDATETIMES;nTime++){
		for(i=0;i<ndistrNumber;i++){
			memcpy(Temp_SendString,SendHead,HEADLEN);
			sprintf(Temp_SendString+HEADLEN,"RB040%01x*\x03",i+1);
			ToUpper(Temp_SendString+HEADLEN);
			UpdateBCC(Temp_SendString+9,15);
			result=MainComm(hComm,25,78,&temp);
			if(!result)
				break;
			else{
				FixData(pWholeSignal[1],pData,iChannelNo3);
				iChannelNo3+=20;
			}
			Sleep(100);
		}
		if(!result) 
		{
			Sleep(500);
			continue;
		}
		else return TRUE;
	}
	return FALSE;
}
///////////////////////////////////////////////////////////////////////////////////////
// 供 PSMS4.0 调用
DLLExport BOOL Read( HANDLE hComm, int nUnitNo, void* pData )
{
	BOOL result;
	int nModuleNumber = 72;
	float *DataBuff=(float*)pData;

	//	GetModuleNum(hComm);

	//if(nUnitNo>200){
	//	nModuleNumber=GetModuleNumber(nUnitNo,200);
	//	if(nModuleNumber > nModuleNumber) return FALSE;
	//	result=ReadModuleState(hComm,nModuleNumber,DataBuff);
	//	Sleep(500);
	//	return result;
	//}
	//else {
	//	if(nUnitNo>100){
	//		nModuleNumber=GetModuleNumber(nUnitNo,100);
	//		result=ReaddistrState(hComm,nModuleNumber,DataBuff);
	//		Sleep(500);
	//		return result;
	//	}
	//	else{
	result=ReadWholeState(hComm,nModuleNumber,DataBuff);
	/* int i;
	for(i = 0;i<4500;i++)
	{
	printf("\n[%d]= %f\n",i,DataBuff[i]);
	}*/
	Sleep(500);
	return result;
	//	}
	//}

}
///////////////////////////////////////////////////////////////////////////////////////
#define	CTRLTYPE_ANALOG					1
#define	CTRLTYPE_DIGITAL				0
//控制信号信息，设置此信息目的是为了是避免填写Output信号时出错。
//爱立信控制数据为WB(信号组地址)!(若干模拟量)!(若干开关量)*
//这些模拟信号、开关信号包括所有的OUT值，所以如只改变其中一个控制信号
//只能改变具体位置的信号值（由设备的信号表决定，与采集相同），并且使
//其他的OUT信号不受影响，这就使得在控制之前应先采集，在与数据区内填写
//相应位置的改写值。
struct tag_CONTROLINFO
{
	char	szGroupValue[5];			//信号组地址	
	int		nResponeLen;				//采集该组信号返回数据包长度
	int		nAnalogOutStartByte;		//模拟量启始位置（对应采集位置=实际值 - 19）
	int		nAnalogOutNumber;			//模拟量个数
	int		nDigitalOutStartByte;		//开关量启始位置（对应采集位置=实际值 - 19）
	int		nDigitalOutNumber;			//开关量BYTE个数
};
typedef  struct tag_CONTROLINFO  CONTROLINFO;

CONTROLINFO controlinfo[]=
{
	{"0000" ,314, 57, 11, 164, 0},			// Address: 0000	2,5,2
	{  "02" , 98, 49,  3, 81,  1},
};
/////////////////////////////////////////////////////////////////////////////////////
//
ReadEricInfo(HANDLE hComm,char* szGroupValue,int nResopneLen)
{
	BOOL result;
	int UpdateTimes;
	int temp;

	memcpy(Temp_SendString,SendHead,HEADLEN);
	sprintf(Temp_SendString+HEADLEN,"RB%04s*\x03",szGroupValue);
	ToUpper(Temp_SendString+HEADLEN);
	UpdateBCC(Temp_SendString+9,15);

	for(UpdateTimes=0;UpdateTimes<UPDATETIMES;UpdateTimes++)
	{
		result=MainComm(hComm,25,nResopneLen,&temp);
		if(result) return TRUE;
	}
	return FALSE;
}
//////////////////////////////////////////////////////////////////////////////////////
// 将浮点数fValue转换成浮点数格式szString。
BOOL FloatToString(float fValue, char* szString)
{
	long	lWeiCode;
	int		nJieCode,nShift;
	BOOL	bMinus;

	if(fValue < 0)
		bMinus = TRUE;
	else
		bMinus = FALSE;

	if(fValue < 0)
	{
		bMinus = TRUE;
		fValue = -1.0f * fValue;
	}
	else
	{
		bMinus = FALSE;
	}

	if( fValue < 0x1000L)
	{    
		lWeiCode =  (long)(fValue * 1024)  ;
		nShift = 10;
	}
	else if( fValue < 0x800000L)
	{
		lWeiCode =  (long)fValue;
		nShift = 0;
	}
	else
		return FALSE;

	for	(nJieCode=1; nJieCode<24; nJieCode++){
		if ( (lWeiCode <<  nJieCode) & 0x400000L )
			break; 
	}
	if ( nJieCode == 24 ){
		lWeiCode = 0;
		nJieCode = 23;
	}

	lWeiCode <<=  nJieCode;
	nJieCode = 23 - nJieCode - nShift;
	if( bMinus ){
		lWeiCode = 0x1000000L - lWeiCode ;
	}

	//ltoa(lWeiCode,szString,16);
	sprintf(szString+6,"%02x",nJieCode);
	strupr(szString);


	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////////////
// 参数串pCmdStr的顺序：命令号，通道号，状态值
// 供 PSMS4.1 调用
//人口参数：hComm－通讯口句柄；nUnitNo－站号；
//			pCmdStr－命令串（含nCmdNo：命令号；Value：设置值）。
//返回：1－成功；-nUnitNo－失败。
//控制说明：命令号包括以下部分 控制命令类型(1:遥调,0:为遥控),信号组地址,命令号(为信号组地址内
//的偏移位置)，如1号泵室温设置值为Analog OutValues(6901)，内第一个位置(0),命令号为：1,6901,0
//
DLLExport BOOL Control(HANDLE hComm, int nUnitNo, char* pCmdStr)
{
	char	szTemp[80];
	int		nPosition = 0,nCmdType,nCmdNo,nBit,nTemp,nLen;
	char	szGroupValue[5],szSetValue[9];
	float	fSetValue;
	int 	nLoop;
	BYTE	bTemp;
	//获得控制类型 1：遥调 0：遥控
	nTemp = StrToK( pCmdStr, szTemp, ',' );
	if( nTemp == 0 ) return FALSE;
	nPosition += nTemp;
	nCmdType = atoi( szTemp );

	//获得控制信号组地址
	nTemp = StrToK( pCmdStr+nPosition, szTemp, ',' );
	if( nTemp == 0 ) return FALSE;
	nPosition += nTemp;
	if( strlen( szTemp ) != 4) return FALSE;
	strcpy( szGroupValue, szTemp);

	//获得命令号：组内偏移
	nTemp = StrToK( pCmdStr+nPosition, szTemp, ',' );
	if( nTemp == 0 ) return FALSE;
	nPosition += nTemp;
	nCmdNo = atoi( szTemp );
	//获得设置数据
	nTemp = StrToK( pCmdStr+nPosition, szTemp, ',' );
	fSetValue = (float)atof( szTemp );

	for(nLoop=0; nLoop<sizeof(controlinfo)/sizeof(CONTROLINFO); nLoop++ )
	{
		if( !memcmp( controlinfo[nLoop].szGroupValue, szGroupValue, 2) )
		{
			if ( ( strlen(controlinfo[nLoop].szGroupValue) == 4 )
				&& memcmp( controlinfo[nLoop].szGroupValue, szGroupValue, 4) )
			{
				continue;
			}
			if( nCmdType == CTRLTYPE_ANALOG	) {
				if( nCmdNo >= controlinfo[nLoop].nAnalogOutNumber)
					return FALSE;
			}
			else if( nCmdType == CTRLTYPE_DIGITAL ) {
				if( nCmdNo >= 4 * controlinfo[nLoop].nDigitalOutNumber)
					return FALSE;
			}
			else
				return FALSE;
			break;
		}
	}
	if ( nLoop == sizeof(controlinfo)/sizeof(CONTROLINFO) )
		return FALSE;

	//bTestFlag = TRUE;

	if( !ReadEricInfo(hComm,szGroupValue,controlinfo[nLoop].nResponeLen) )
		return FALSE;
	if( nCmdType == CTRLTYPE_ANALOG	) 
	{
		sprintf(Temp_SendString+HEADLEN,"WB%04s!",szGroupValue);

		FloatToString(fSetValue, szSetValue);
		nLen = strlen( Temp_SendString );
		strncat(Temp_SendString+nLen,Temp_ReceiveString+controlinfo[nLoop].nAnalogOutStartByte+19,controlinfo[nLoop].nAnalogOutNumber*8);

		strcat(Temp_SendString,"!$*\x03");

		memcpy(Temp_SendString+nLen+nCmdNo*8, szSetValue, 8);

	}
	else 
	{
		//sprintf(Temp_SendString+HEADLEN,"WB%04s!$!",szGroupValue);
		sprintf(Temp_SendString+HEADLEN,"WB%04s!!",szGroupValue);
		nLen = strlen( Temp_SendString );
		FloatToString(fSetValue, szSetValue);
		bTemp = Temp_ReceiveString[nCmdNo/4 + controlinfo[nLoop].nDigitalOutStartByte + 19];
		if( bTemp >= 'A' )
		{
			bTemp = bTemp -'A'+10;
		}
		else {
			bTemp = bTemp - '0' ;
		}
		nBit = nCmdNo % 4;
		//if((BYTE)fSetValue == ( (bTemp>>(3-nBit))  & 1 )  )
		//return TRUE;
		//else
		//{
		strncat(Temp_SendString,Temp_ReceiveString+controlinfo[nLoop].nDigitalOutStartByte+19,controlinfo[nLoop].nDigitalOutNumber);
		strcat(Temp_SendString,"*\x03");
		bTemp ^= ((1 << (3-nBit)));
		if( bTemp >= 10 ){
			bTemp = bTemp +'A'-10;
		}
		else{
			bTemp = bTemp +'0';
		}
		Temp_SendString[nCmdNo/4 + nLen] = bTemp;
		//}
	}
	strupr(Temp_SendString);
	nLen = strlen(Temp_SendString);
	UpdateBCC(Temp_SendString+9,nLen-9);

	Sleep(10);

	int UpdateTimes;
	int temp;

	for(UpdateTimes=0;UpdateTimes<UPDATETIMES;UpdateTimes++)
	{
		if(MainComm(hComm,nLen+1,13,&temp))
		{
			//bTestFlag = FALSE;
			Sleep(1000);
			return TRUE;
		}
	}
	return FALSE;
}
/*
//////////////////////////////////////////////////////////////////////////////////////
//
BOOL ControlModule(HANDLE hComm,int nModuleNumber,int nCmdNo){
memcpy(Temp_SendString,SendHead,HEADLEN);
sprintf(Temp_SendString+HEADLEN,"WB690%01x!$!%01x000*\x03",nModuleNumber,nCmdNo);
ToUpper(Temp_SendString+HEADLEN);
UpdateBCC(Temp_SendString+9,22);

if(MainComm(hComm,32,13))
return FALSE;
else
return TRUE;
}
//////////////////////////////////////////////////////////////////////////////////////
// 参数串pCmdStr的顺序：命令号，通道号，状态值
// 供 PSMS4.1 调用
DLLExport BOOL Control(HANDLE hComm, int nUnitNo, char *pCmdStr )
{
char	sTarget[20];
int		nCmdNo,nModuleNumber;

StrToK( pCmdStr, sTarget, ',' );
nCmdNo = atoi( sTarget );

if(!InitFlag){
return FALSE;
}

//bTestFlag = TRUE;

nModuleNumber=GetModuleNumber(nUnitNo,200);
if(nModuleNumber>0){
return ControlModule(hComm,nModuleNumber,nCmdNo);
}
else{
return FALSE;
}
}
*/
///////////////////////////////////////////////////////////////////////////////////////
// 前置机4.1版采集接口函数
#ifdef _DLL_



DLLExport BOOL Query(HANDLE hComm, int nUnitNo, ENUMSIGNALPROC EnumProc, LPVOID lpvoid)
{
	Digit_1.iValue	= 1;
	Digit_0.iValue  = 0;

	NoValid.iValue = -9999;

	int i;
	char *pRIResult;
	char *pTemp;
	char *pSam1 = "!61";
	char *pSam2 = "!62";
	int iFlage = 1;

	iNum61.iValue = 1;
	iNum62.iValue = 0;

	memset(Temp_ReceiveString,0 ,nMaxRevBuf);
	GetModuleNum(hComm);
	pRIResult = Temp_ReceiveString;

	//printf("---%s---\n",pRIResult);

	pTemp = pRIResult;

	//calculate the number of equip
	while(iFlage)
	{
		pTemp = strstr(pTemp,pSam1);
		if(pTemp != NULL)
		{
			iNum61.iValue++;	
			pTemp = pTemp + 3;	
		}
		else
		{
			iFlage = 0;	
		}

	}

	pTemp = pRIResult;
	iFlage = 1;

	while(iFlage)
	{
		pTemp = strstr(pTemp,pSam2);
		if(pTemp != NULL)
		{
			iNum62.iValue++;	
			pTemp = pTemp + 3;	
		}
		else
		{
			iFlage = 0;	
		}

	}

	//printf("--iNum61=%d---\n",iNum61.iValue);
	//printf("--iNum62=%d---\n",iNum62.iValue);

	//judge the equipment number,if it changed,clear the memory
	if(iNum61.iValue != iNum61Temp || iNum61.iValue != iNum61Temp)
	{
		iNum61Temp = iNum61.iValue;
		iNum62Temp = iNum62.iValue;
		flage = 0;
	}

	//clear the memory
	if(flage == 0)
	{
		for(i = 0;i < SIGNALMAX; i++ )
		{
			p[i] = NoValid.fValue;;
		}
		flage = 1;
	}

	p[37] = iNum61.fValue;
	p[38] = iNum62.fValue;

	for(i = 0;i< 54; i++)
	{
		if(i < iNum61.iValue - 1)
		{
			p[48 + 35*i] = Digit_0.fValue;//working
		}
		else
		{
			p[48 + 35*i] = Digit_1.fValue;//not working
		}
	}

	for(i = 0;i< 54; i++)
	{
		if(i < iNum62.iValue - 1)
		{
			p[4015 + 50*i] = Digit_0.fValue;//working
		}
		else
		{
			p[4015 + 50*i] = Digit_1.fValue;//not working
		}
	}


	if( Read(hComm,nUnitNo,(void*)p) ){
		for(  i = 0; i < SIGNALMAX; i++ )
			EnumProc( i, p[i], lpvoid );
		iSampleTime++;
		return TRUE;
	}
	else
		return FALSE;
}
#endif
//////////////////////////////////////////////////////////////////////////////////////
// 参数串pCmdStr的顺序：状态值，命令号，通道号
// 供 PSMS4.0 调用
DLLExport BOOL Write( HANDLE hComm, int nUnitNo, char* pCmdStr )
{
	const int CMDMAXLEN=50;
	int nCmdLen,i;
	char szNewCmdStr[CMDMAXLEN];
	if((nCmdLen=strlen(pCmdStr))>CMDMAXLEN-1)
		return FALSE;
	for(i=nCmdLen;(i>1) && (pCmdStr[i-1]!=',');i--);
	if(pCmdStr[i-1]!=',')
		return FALSE;
	else{
		strcpy(szNewCmdStr,pCmdStr+i);
		strcat(szNewCmdStr,",");
		pCmdStr[i-1]=0;
	}
	strcat(szNewCmdStr,pCmdStr);
	return Control(hComm,nUnitNo,szNewCmdStr );
}

/*****************************************************************/
// 函数名称：Test
// 功能描述：跟踪测试入口函数，在测试前，将全局标志 bTestFlag 置位；
//           测试后，复位标志。
// 输入参数：hComm - 通信句柄, 
//           nUnitNo - 采集器单元地址,
//           pData - 上报数据缓冲区指针
// 输出参数：
// 返    回：TRUE－成功；FALSE－失败。    
// 其    他：
/*****************************************************************/
#ifdef _DLL_
DLLExport BOOL Test(HANDLE hComm,               // 通讯口句柄
		    int nUnitNo,                // 采集器单元地址
		    ENUMSIGNALPROC EnumProc,    // 枚举函数
		    LPVOID lpvoid)              // 空类型指针
{
	// 将调试标志置位
	bTestFlag = TRUE;

	// 调用采集函数采集数据，其中会因调试标志的置位而显示调试信息。
	BOOL bFlag = Query( hComm, nUnitNo, EnumProc, lpvoid );

	WriteAsc( ASC_FILE, "\r\n本次采集结束\r\n", 16 );
	WriteAsc( HEX_FILE, "\r\n本次采集结束\r\n", 16 );

	// 将调试标志复位
	bTestFlag = FALSE;

	return bFlag;
}
#endif

BOOL SendString(
		HANDLE hComm,       // 通讯口句柄
		BYTE* sSendStr,     // 要发送的字符串指针
		int nStrLen )       // 要发送字符个数
{
}

int ReceiveString(
		  HANDLE hComm,     // 通讯口句柄
		  BYTE* sRecStr,    // 接收字符串指针
		  int nStrLen       // 需要接收的字符个数
		  )
{
}
// 校验码算法：累加和。
// 校验一般为了减少误码是必须作的，但特殊情况时，一定要说明原因。
void CheckSum( BYTE *Frame )
{
	WORD R=0;

	int i, nLen = (int)strlen( (char*)Frame );  // 计算要校验的字符串长度

	for( i=1; i<nLen; i++ )
	{
		R += *(Frame+i);
	}

	sprintf( (char *)Frame+nLen, "%04X\r", (WORD)-R );
}

//////////////////////////////////////////////////////////////////////////////////////
//	Test programs  begins
//#define _CONSOLETEST_
//#ifdef _CONSOLETEST_
//#define SETTIMEOUT				5000
//int baudrate_idu [] = { B115200, B57600, B38400, B19200, B9600, B4800, B2400, B1200, B300};
//int baudrate_num [] = { 115200,  57600,  38400,  19200,  9600,  4800,  2400,  1200,  300};
//
//HANDLE g_hEemThread;
//HANDLE m_hComm;
//int m_nUnitNo;

////////////////////////////////////////////////////////////////////////////////

//float g_fEemSignal[SIGNALMAX] = { 0.0 };
//
//void EEM_fnMainThread()
//{
//	int i ;
//	HANDLE  hPort;
//	int  iCommPort;
//	int  nErrCode = ERR_COMM_OK;
//	char temp[10];
//
//#ifdef	DEBUG_EEM
//	printf("***********************************************************\n");
//	printf("*                                                         *\n");
//	printf("*                   Testing new                      *\n");
//	printf("*                                                         *\n");
//	printf("***********************************************************\n");
//	printf("     ShengZheng HuaWei Telecommunication Co.,Ltd           \n");
//	printf("\n");
//#endif
//
//
//	while(1)
//	{
//		hPort = CommOpen("comm_std_serial.so",
//				"ttyS2",
//				"9600,n,8,1",
//				1,
//				1000,
//				&nErrCode);
//		if(hPort == NULL)
//		{
//#ifdef	DEBUG_EEM
//			printf("\r\nError code is %d\r\n");
//		 	printf("\r\nOpen port error!\r\n");
//#endif
//            break;
//		}
//#ifdef	DEBUG_EEM
//		printf("\n");
//		printf("Testing  Now, Please wait...\n\n");
//#endif	
//		
//		memset(g_fEemSignal, 0, sizeof(g_fEemSignal));
//	    	if( Read(hPort,1,(void*)g_fEemSignal) )
//	    	{
//#ifdef	DEBUG_EEM
//			printf("\n");
//			for (i=0; i<1530;i++)
//			{
//				printf("[%d]:%f.",i,g_fEemSignal[i]);
//			}
//			printf("\n  ******************** Success! ***************\n");
//			printf("                       Bye!                 \n");
//#endif		
//		}
//		else
//		{
//#ifdef	DEBUG_EEM
//			printf("   Sorry, Cannot received any data,The communication port disconnect!\n");
//	        	printf(" Press Any key Programming exited!");
//#endif
//			CommClose(hPort);	
//		}  
//	}
//
//	CommClose(hPort);
//	
//}
//
//BOOL EEM_Init(void)
//{
//	g_hEemThread = RunThread_Create("EEM_ACU_THD",
//					(RUN_THREAD_START_PROC)EEM_fnMainThread,
//					NULL,
//					NULL,
//					0);
//	if(g_hEemThread == NULL)
//	{
//#ifdef	DEBUG_EEM
//	    printf("Thread Create Fail!");
//#endif	    
//		return FALSE;
//	}
//
//	return TRUE;
//}
//
//void EEM_Exit(void)
//{
//
//	RunThread_Stop(g_hEemThread,1000, TRUE);
//}

//#endif //_CONSOLETEST_


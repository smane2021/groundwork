#include <fcntl.h> 
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>


#include "can_sampler_main.h"
#include "can_dcem.h" 

extern	CAN_SAMPLER_DATA	g_CanData;
GLB_STRU_CAN_SAMP_DCEM		g_can_DCEM;

/*********************************************************************************
*  
*  FUNCTION NAME : DCEM_InitRoughValue
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-3-2 17:08:26
*  DESCRIPTION   : Initial all the raw data to prepare for sampling
*  
*  CHANGE HISTORY:
*  
***********************************************************************************/
void DCEM_InitRoughValue(void)
{
	INT32 i,j,iIndex;

	//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	EQUIP_INFO* pEquipInfo;
	int  nBufLen;
 
	g_can_DCEM.bConfigured = TRUE;
	
	if (DxiGetData(VAR_A_EQUIP_INFO,
		DCEM_EQUIP_ID,			
		0,		
		&nBufLen,			
		&pEquipInfo,			
		0) == ERR_DXI_INVALID_EQUIP_ID)
	{
		g_can_DCEM.bConfigured = FALSE;
	
		//give WARNING msg
		AppLogOut("CAN_SAMP", APP_LOG_WARNING, 
			"DCEM equipment not configured in the solution.");
	}
	//G3_OPT end


	for (i = 0; i < CAN_DCEM_MAX_NUM;i++)
	{
		for (j = 0; j < ROUGH_DCEM_MAX_SIG_NUM;j++)
		{
			//最先初始化的时候不要给8Probes赋值-9999，不然会出现Web闪的情形
			if( (j >= ROUGH_DCEM_ENGY_CHANNEL1 && j <= ROUGH_DCEM_CURR_SHUNT4) ||
				(j >= ROUGH_DCEM_CHANNEL1_DAY1 && j <= ROUGH_DCEM_CHANNEL4_DAY50) )
			{
				g_can_DCEM.aRoughData[i][j].iValue = CAN_DCEM_INVALID_INI_VALUE;
			}
			
			
		}

		g_can_DCEM.aRoughData[i][ROUGH_DCEM_EXIST_STAT].iValue
			= CAN_DCEM_NON_EXIST;
		g_can_DCEM.aRoughData[i][ROUGH_DCEM_COMM_STAT].iValue
			= CAN_DCEM_COMM_OK;
		
		//To avoid the false report of EEM, Set it to -9999
		g_can_DCEM.aRoughData[i][ROUGH_DCEM_POWER_CHANNEL1].iValue = CAN_SAMP_INVALID_VALUE;
		g_can_DCEM.aRoughData[i][ROUGH_DCEM_POWER_CHANNEL2].iValue = CAN_SAMP_INVALID_VALUE;
		g_can_DCEM.aRoughData[i][ROUGH_DCEM_POWER_CHANNEL3].iValue = CAN_SAMP_INVALID_VALUE;
		g_can_DCEM.aRoughData[i][ROUGH_DCEM_POWER_CHANNEL4].iValue = CAN_SAMP_INVALID_VALUE;

		//这里防止错误的清除电量操作
		if (g_can_DCEM.bConfigured)    //G3_OPT [loader], condition added by Lin.Tao.Thomas, 2013-4
		{
			for (iIndex = 0; iIndex < DCEM_CHANNEL_NUM; iIndex++)
			{
				SetDwordSigValue(DCEM_EQUIP_ID + i, 
					SIG_TYPE_SAMPLING,
					DCEM_CHANNEL_CTRL_ENERGY_CLS_ID + iIndex,
					(DWORD)ECDM_CHANNEL_NOCLS_ENERGY,
					"CAN_SAMP");
			}
		}
	
	}
	g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_GROUP_STATE].iValue
		= CAN_DCEM_NON_EXIST;
	g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_NUM].iValue
		= 0;
	g_can_DCEM.aRoughData_Group[ROUGH_GROUP_TOTAL_ENERGY].iValue = CAN_SAMP_INVALID_VALUE;

}

/*==========================================================================*
* FUNCTION : PackAndSendDCEMCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UINT   uiMsgType    : 
*            UINT   uiDestAddr   : 
*            UINT   uiCmdType    : 
*            UINT   uiValueTypeH : 
*            UINT   uiValueTypeL : 
*            float  fParam       : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Copy Frank Cao                DATE: 2010-02-25 16:27
*==========================================================================*/
LOCAL void PackAndSendCmd(UINT uiMsgType,
			  UINT uiDestAddr,
			  UINT uiCmdType,
			  UINT uiValueTypeH,
			  UINT uiValueTypeL,
			  float fParam)
{
	BYTE*	pbySendBuf = g_can_DCEM.cBySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_DCEM_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);
	/////////////////////for debug only
	//int i;
	//printf("\n*****The sent package is: ");
	//for(i=0;i<13;i++)
	//{
	//	printf("%2X",g_can_DCEM.cBySendBuf[i]);
	//
	//}
	//printf(" *****end\n ");
	////////////end 

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	

	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}


/*=============================================================================*
* FUNCTION: GetValueTypeFromAFrame(BYTE *pFrame)
* PURPOSE : Get uValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     INT32   iValueType
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 GetValueTypeFromAFrame(BYTE *pFrame)
{
	BYTE * pValue;
	INT32 iValueType	= 0;
	INT32 iValueTypeH	= 0;
	INT32 iValueTypeL	= 0;

	pValue = pFrame + CAN_FRAME_OFFSET_VAL_TYPE_H;
	iValueTypeH = *(pValue);
	pValue = pFrame + CAN_FRAME_OFFSET_VAL_TYPE_L;
	iValueTypeL = *(pValue);
	iValueType = (iValueTypeH << 8) + iValueTypeL;

	return iValueType;
}

/*=============================================================================*
* FUNCTION: GetUintFromAFrame(BYTE *pFrame)
* PURPOSE : Get uValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     UINT   uiValue
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL UINT GetUintFromAFrame(BYTE *pFrame,int iSigType)
{
	BYTE * pValue;
	pValue = pFrame + CAN_FRAME_OFFSET_VALUE;
	UINT uiValue;
	if(iSigType == GET_SHUNT_MV)
	{
		uiValue = (((UINT)(pValue[1])) << 8)
			+ (UINT)(pValue[3]);
	}
	else if(iSigType == GET_SHUNT_A)
	{
		uiValue = (((UINT)(pValue[0])) << 8)
			+ (UINT)(pValue[2]);
	}
	else
	{
		uiValue = (((UINT)(pValue[0])) << 24)
			+ (((UINT)(pValue[1])) << 16)
			+ (((UINT)(pValue[2])) << 8)
			+ ((UINT)(pValue[3]));
	}
	return uiValue;
}
/*=============================================================================*
* FUNCTION: DCEMGetfValue(BYTE *pFrame)
* PURPOSE : Get fValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     UINT   uiValue
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL float GetFloatFromAFrame(BYTE *pFrame,int iSigType)
{
	BYTE*				pValue;
	int					i;	
	FLOAT_STRING		unValue;
	pValue = pFrame + CAN_FRAME_OFFSET_VALUE;
	float fTemp;
	switch(iSigType)
	{
	case GET_CHANNEL_ENERGY:
		for(i = 0; i < 4; i++)
		{
			unValue.abyValue[4 - i - 1] = pValue[i];
		}
		fTemp = unValue.ulValue/10.0;
		break;
	case GET_CHANNEL_POWER:
	case GET_STATS_DATA1:
		unValue.ulValue = (((ULONG)(pValue[0]))<<8) + ((ULONG)(pValue[1]));
		fTemp = unValue.ulValue/10.0;
		break;
	case GET_BUS_VOLT:		
	case GET_CHANNEL_CURR:
	case GET_STATS_DATA2:
	//case GET_SW_VER:
		unValue.ulValue = (((ULONG)(pValue[2]))<<8) + ((ULONG)(pValue[3]));
		fTemp = unValue.ulValue/10.0;
		//printf("\n*******now the ulvalue is %ld; and the float value is %f",unValue.ulValue,fTemp);
		//if(iSigType == GET_SW_VER)
		//{
		//	fTemp /= 10; //sw version = sw/100
		//}
		break;
	default:
		break;

	}

	//printf("float value = %f \n", unValue.fValue);
		
	return fTemp;
}
/*=============================================================================*
* FUNCTION:	ClrDCEMIntrruptTimes
* PURPOSE : 
*					
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     void	g_can_DCEM.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void ClrDCEMIntrruptTimes(INT32 iAddr)
{
	g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_COMM_STAT].iValue = CAN_DCEM_COMM_OK;
	g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_COMM_FAIL_TIMES].iValue = 0;
}
/*=============================================================================*
* FUNCTION:	IncDCEMIntrruptTimes
* PURPOSE : 
*					
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     void	g_can_DCEMlus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void IncDCEMIntrruptTimes(INT32 iAddr)
{
	if (g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_COMM_FAIL_TIMES].iValue < DCEM_B_COMMFAIL_TIME)
	{
		g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_COMM_FAIL_TIMES].iValue++;
		//printf("\n  ^^^  COMM_FAIL_TIMES = %d \n",g_can_DCEMlus.aRoughData[iTempAddr][SM_DCEM_COMM_FAIL_TIMES].iValue);
	}

	if (g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_COMM_FAIL_TIMES].iValue >= DCEM_B_COMMFAIL_TIME)
	{
		g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_COMM_STAT].iValue = CAN_DCEM_COMM_FAIL;
	}
}

/*********************************************************************************
*  
*  FUNCTION NAME : DCEM_Check_ValueBytes_ForCMD00
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-03-05 
*  DESCRIPTION   : create value bytes for cmd 00 , byte2 = 0, so ignore it, 
		   just handle byte3
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL BYTE DCEM_Check_ValueBytes_ForCMD00(int iAddr)
{
	BYTE byValType3 = 0;
	int iIndex = 0;
	for (iIndex = 0; iIndex < DCEM_CHANNEL_NUM; iIndex++)
	{
		if(ECDM_CHANNEL_CLS_ENERGY == GetDwordSigValue(DCEM_EQUIP_ID + iAddr, 
			SIG_TYPE_SAMPLING,
			DCEM_CHANNEL_CTRL_ENERGY_CLS_ID + iIndex,
			"CAN_SAMP"))
		{		
			SetDwordSigValue(DCEM_EQUIP_ID + iAddr, 
				SIG_TYPE_SAMPLING,
				DCEM_CHANNEL_CTRL_ENERGY_CLS_ID + iIndex,
				(DWORD)ECDM_CHANNEL_NOCLS_ENERGY,
				"CAN_SAMP");
			byValType3 = byValType3 | (BYTE)(0x01 << iIndex);
		}
		if(DCEM_CHANNEL_DISABLE == GetDwordSigValue(DCEM_EQUIP_ID + iAddr, 
			SIG_TYPE_SETTING,
			DCEM_CHANNEL_SET_ENBLED_ID + iIndex,
			"CAN_SAMP"))
		{
			byValType3 = byValType3 | (BYTE)(0x01 << (iIndex+4));
		}
	}
	return byValType3;

}
/*********************************************************************************
*  
*  FUNCTION NAME : DCEMUpackCmd00
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-03-05
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL BOOL DCEMUpackCmd00(int iAddr,
			    int iReadLen,
			    BYTE* pbyRcvBuf
			    )
{
	INT32		i,j;
	BYTE*		pFrameofPackOffset;
	BOOL		bAllFrameSucceedFlag;
	float		fTempData;

	bAllFrameSucceedFlag = TRUE;

	//printf("\n****&&&&&&&&&& %d &&&&&&&&&\n",iCMDhasFrameNum);
	int iReal_ValueType = DCEM_VAL_TYPE_R_BUS_VOLT; //0x01~0x09
	//changed by Frank Wu,1/3,20140327, for DC Meter can't reply all the 9 frames at one time sometimes
	//for (i = 0; i < DCEM_CMD00_RCV_FRAMES_NUM; i++)
	int iFrameNum = iReadLen/CAN_FRAME_LEN;
	for (i = 0; i < iFrameNum; i++)
	{
		pFrameofPackOffset = pbyRcvBuf + CAN_FRAME_LEN * i;		
		//changed by Frank Wu,2/3,20140327, for DC Meter can't reply all the 9 frames at one time sometimes
		//iReal_ValueType = DCEM_VAL_TYPE_R_BUS_VOLT + i;
		iReal_ValueType = GetValueTypeFromAFrame(pFrameofPackOffset);
		//if (iReal_ValueType != GetValueTypeFromAFrame(pFrameofPackOffset))
		if(0)
		{
			bAllFrameSucceedFlag = FALSE;
			//printf(" \n\n\n*********************************The Frame receiving is fail ValueType = %d  \n",iValueType);
			continue;
		}
		else
		{
			if (iReal_ValueType == DCEM_VAL_TYPE_R_BUS_VOLT)
			{
				g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_BUS_VOLT].fValue
					= GetFloatFromAFrame(pFrameofPackOffset,GET_BUS_VOLT);
				//printf("\n*****the bus voltage is %f",GetFloatFromAFrame(pFrameofPackOffset,GET_BUS_VOLT));
			}
			else if (iReal_ValueType >= DCEM_VAL_TYPE_R_ENERGY_CHANNEL1 && iReal_ValueType <= DCEM_VAL_TYPE_R_ENERGY_CHANNEL4)
			{
				fTempData = GetFloatFromAFrame(pFrameofPackOffset,GET_CHANNEL_ENERGY);
	
				g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL1 + iReal_ValueType - DCEM_VAL_TYPE_R_ENERGY_CHANNEL1].fValue
						= fTempData;	

			}
			else if (iReal_ValueType >= DCEM_VAL_TYPE_R_POWER_CHANNEL1 && iReal_ValueType <= DCEM_VAL_TYPE_R_POWER_CHANNEL4)
			{
				fTempData = GetFloatFromAFrame(pFrameofPackOffset,GET_CHANNEL_POWER);

				g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_POWER_CHANNEL1 + iReal_ValueType - DCEM_VAL_TYPE_R_POWER_CHANNEL1].fValue
					= fTempData;	

				fTempData = GetFloatFromAFrame(pFrameofPackOffset,GET_CHANNEL_CURR);

				g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_CURR_CHANNEL1 + iReal_ValueType - DCEM_VAL_TYPE_R_POWER_CHANNEL1].fValue
					= fTempData;	
			}
			else
			{
				//return FALSE;
			}
		}
	}
	//printf(" ############ DCEM UpackCmd00   FLAG =%d  \n",bAllFrameSucceedFlag);
	return bAllFrameSucceedFlag;

	//if (!bAllFrameSucceedFlag)
	//{
	//	return FALSE;
	//}
	//else
	//{
	//	printf(" ############ DCEM UpackCmdXX 20   okokok\n");
	//	return TRUE;
	//}
}


/*=============================================================================*
* FUNCTION: DCEM UnpackCmd01
* PURPOSE : Analyse the MSG 03 command
*			According to  VALUE_TYPE 0x0132
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_DCEMlus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/

/*=============================================================================*
* FUNCTION: DCEM SampCmd01
* PURPOSE : Sampling  data by MSG 0x03 to get Volt signals
*			It return 0x0132 Frame,But bit setting
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 DCEMSampCmd00(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	//printf("\n******Oh Yes, DCEM %d run here for get cls flag! ****\n",iAddr+1);
	PackAndSendCmd(MSG_TYPE_RQST_DATA00,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,
		DCEM_Check_ValueBytes_ForCMD00(iAddr),
		0.0);		

	Sleep(DCEM_CMD00_RCV_FRAMES_NUM * DCEM_FRAME_FRAME_INTTERVAL);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_can_DCEM.cByRecvBuf, 
		&iReadLen1st);
	//printf("\nAddr: %d; Len: %d",iAddr,iReadLen1st);
	if(iReadLen1st < (DCEM_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		Sleep(DCEM_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 01 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_can_DCEM.cByRecvBuf + iReadLen1st,
			&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}
	//changed by Frank Wu,3/3,20140327, for DC Meter can't reply all the 9 frames at one time sometimes
	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		//>= DCEM_CMD00_RCV_FRAMES_NUM)
		>= 1)
	{
		//printf("\n^_^OK,Run here to unpack cmd 00!\n");
		if (DCEMUpackCmd00(iAddr,
			iReadLen1st + iReadLen2nd,
			g_can_DCEM.cByRecvBuf))
		{
			ClrDCEMIntrruptTimes(iAddr);
		}
		else
		{
			IncDCEMIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		IncDCEMIntrruptTimes(iAddr);
		//int iRST = ((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN);
		//printf("\n********** 00 CMD LENGH = %d **********\n",iRST);
		return CAN_SAMPLE_FAIL;
	}

	//printf("#######  DCEM SampCmd01  ok \n");

	return CAN_SAMPLE_OK;
}


/*********************************************************************************
*  
*  FUNCTION NAME : DCEMUpackCmd30
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-03-05
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL BOOL DCEMUpackCmd30(int iAddr,
			  int iReadLen,
			  BYTE* pbyRcvBuf,
			  int iPackIdx
			  )
{
	INT32		i;
	BYTE*		pFrameofPackOffset;
	BOOL		bAllFrameSucceedFlag;
	float		fTempData;

	bAllFrameSucceedFlag = TRUE;

	//printf("\n****&&&&&&&&&& %d &&&&&&&&&\n",iCMDhasFrameNum);
	int iReal_ValueType = DCEM_VAL_TYPE_R_DAY1_CHANNEL_12; 
	int iIdx = 0; //to convert for the sig ID
	int iChannel_Type = 0; //0 =chanel12; 1 = chanel34

	for (i = 0; i < DCEM_CMD30_RCV_FRAMES_NUM_EACH_PART; i++)
	{
		iChannel_Type = i % 2;	//12，34，12，34。。。。12，34
		pFrameofPackOffset = pbyRcvBuf + CAN_FRAME_LEN * i;		
		iReal_ValueType = DCEM_VAL_TYPE_R_DAY1_CHANNEL_12 + i + iPackIdx * DCEM_CMD30_RCV_FRAMES_NUM_EACH_PART;
		if (iReal_ValueType != GetValueTypeFromAFrame(pFrameofPackOffset))
		{
			bAllFrameSucceedFlag = FALSE;
			//printf(" \n\n\n*********************************The Frame receiving is fail ValueType = %d  \n",iValueType);
			continue;
		}
		else
		{
			if(CHANNEL_TYPE_12 == iChannel_Type)
			{
				fTempData = GetFloatFromAFrame(pFrameofPackOffset,GET_STATS_DATA1); // chanel 1
				iIdx = ROUGH_DCEM_CHANNEL1_DAY1 + (int)(i/2)*4 + iPackIdx * MAX_DATA30_PACK_NUM*2;
				g_can_DCEM.aRoughData[iAddr][iIdx].fValue = fTempData;	

				fTempData = GetFloatFromAFrame(pFrameofPackOffset,GET_STATS_DATA2);//channel 2
				iIdx = ROUGH_DCEM_CHANNEL1_DAY2 + (int)(i/2)*4 + iPackIdx * MAX_DATA30_PACK_NUM*2;
				g_can_DCEM.aRoughData[iAddr][iIdx].fValue = fTempData;	
			}
			else
			{
				fTempData = GetFloatFromAFrame(pFrameofPackOffset,GET_STATS_DATA1); // chanel 3
				iIdx = ROUGH_DCEM_CHANNEL1_DAY3 + (int)(i/2)*4 + iPackIdx * MAX_DATA30_PACK_NUM*2;
				g_can_DCEM.aRoughData[iAddr][iIdx].fValue = fTempData;

				fTempData = GetFloatFromAFrame(pFrameofPackOffset,GET_STATS_DATA2);//channel 4
				iIdx = ROUGH_DCEM_CHANNEL1_DAY4 + (int)(i/2)*4 + iPackIdx * MAX_DATA30_PACK_NUM*2;
				g_can_DCEM.aRoughData[iAddr][iIdx].fValue = fTempData;
			}
		}
	}
	//printf(" ############ DCEM UpackCmdXX   FLAG =%d  \n",bAllFrameSucceedFlag);
	return bAllFrameSucceedFlag;

	//if (!bAllFrameSucceedFlag)
	//{
	//	return FALSE;
	//}
	//else
	//{
	//	printf(" ############ DCEM UpackCmdXX 20   okokok\n");
	//	return TRUE;
	//}
}


/*=============================================================================*
* FUNCTION: DCEM UnpackCmd01
* PURPOSE : Analyse the MSG 03 command
*			According to  VALUE_TYPE 0x0132
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_DCEMlus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/

/*=============================================================================*
* FUNCTION: DCEM SampCmd01
* PURPOSE : Sampling  data by MSG 0x03 to get Volt signals
*			It return 0x0132 Frame,But bit setting
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 DCEMSampCmd30(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	int iPackIdx;
	for(iPackIdx = 0; iPackIdx < MAX_DATA30_PACK_NUM; iPackIdx++)
	{
		//printf("\n******Oh Yes, DCEM %d run here for get cls flag! ****\n",iAddr+1);
		PackAndSendCmd(MSG_TYPE_RQST_DATA30,
			(UINT)iAddr,
			CAN_CMD_TYPE_P2P,
			0,
			DCEM_VAL_TYPE_S_DAY1_DAY5 + iPackIdx,
			0.0);		

		Sleep(DCEM_CMD30_RCV_FRAMES_NUM_EACH_PART * DCEM_FRAME_FRAME_INTTERVAL);

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_can_DCEM.cByRecvBuf, 
			&iReadLen1st);
		//printf("\nAddr: %d; Len: %d",iAddr,iReadLen1st);
		if(iReadLen1st < (DCEM_CMD30_RCV_FRAMES_NUM_EACH_PART * CAN_FRAME_LEN))
		{
			Sleep(DCEM_FRAME_ENOUGH_LONG_WAIT_TIME);

			//printf(" MSG 01 Sampling again \n");

			CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
				g_can_DCEM.cByRecvBuf + iReadLen1st,
				&iReadLen2nd);
		}
		else
		{
			//Received the Frame number is full
		}

		if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
			>= DCEM_CMD30_RCV_FRAMES_NUM_EACH_PART)
		{
			//printf("\n^_^OK,Run here to unpack cmd 00!\n");
			if (DCEMUpackCmd30(iAddr,
				iReadLen1st + iReadLen2nd,
				g_can_DCEM.cByRecvBuf,iPackIdx))
			{
				ClrDCEMIntrruptTimes(iAddr);
			}
			else
			{
				IncDCEMIntrruptTimes(iAddr);
				return CAN_SAMPLE_FAIL;	
			}

		}
		else
		{
			IncDCEMIntrruptTimes(iAddr);
			//int iRST = ((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN);
			//printf("\n********** 00 CMD LENGH = %d **********\n",iRST);
			return CAN_SAMPLE_FAIL;
		}

		//printf("#######  DCEM SampCmd01  ok \n");
	}


	return CAN_SAMPLE_OK;
}


/*********************************************************************************
*  
*  FUNCTION NAME : DCEMUnpackBarCode
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-19 11:22:44
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static BOOL DCEMUnpackBarCode(int iAddr,
				int iReadLen,
				BYTE* pbyRcvBuf
				)
{
	INT32		i;
	BYTE*		pFrameofPackOffset;
	BOOL		bAllFrameSucceedFlag;

	bAllFrameSucceedFlag = TRUE;

	//printf("\n****&&&&&&&&&& %d &&&&&&&&&\n",iCMDhasFrameNum);
	int iReal_ValueType = DCEM_VAL_TYPE_R_BARCODE1; //0x45~0x4C
	for (i = 0; i < DCEM_CMD20_RCV_FRAMES_NUM; i++)
	{
		pFrameofPackOffset = pbyRcvBuf + CAN_FRAME_LEN * i;		
		iReal_ValueType = DCEM_VAL_TYPE_R_BARCODE1 + i;
		if (iReal_ValueType != GetValueTypeFromAFrame(pFrameofPackOffset))
		{
			bAllFrameSucceedFlag = FALSE;
			//printf(" \n\n\n*********************************The Frame receiving is fail ValueType = %d  \n",iValueType);
			continue;
		}
		else
		{
			//if (iReal_ValueType >= DCEM_VAL_TYPE_R_BARCODE1 && iReal_ValueType <= DCEM_VAL_TYPE_R_BARCODE7)
			{
	
				g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_BARCODE1 + iReal_ValueType - DCEM_VAL_TYPE_R_BARCODE1].iValue
					= GetUintFromAFrame(pFrameofPackOffset,GET_BARCODE);	

			}
			//else if (iReal_ValueType >= DCEM_VAL_TYPE_R_SW_VERSION)
			//{
			//	g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_VERSION_NO].fValue
			//		= GetFloatFromAFrame(pFrameofPackOffset,GET_SW_VER);
			//}
			//else
			//{
			//	//return FALSE;
			//}
		}
	}
	//printf(" ############ DCEM UpackCmdXX   FLAG =%d  \n",bAllFrameSucceedFlag);
	return bAllFrameSucceedFlag;

	//if (!bAllFrameSucceedFlag)
	//{
	//	return FALSE;
	//}
	//else
	//{
	//	printf(" ############ DCEM UpackCmdXX 20   okokok\n");
	//	return TRUE;
	//}
}

/*********************************************************************************
*  
*  FUNCTION NAME : DCEMSampBarCode_20
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-03-05 10:33:41
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL INT32 DCEMSampBarCode_20(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	//printf("\n******Oh Yes, DCEM %d run here for get cls flag! ****\n",iAddr+1);
	PackAndSendCmd(MSG_TYPE_RQST_DATA20,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,
		0,
		0.0);		

	Sleep(DCEM_CMD20_RCV_FRAMES_NUM * DCEM_FRAME_FRAME_INTTERVAL);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_can_DCEM.cByRecvBuf, 
		&iReadLen1st);
	//printf("\nAddr: %d; Len: %d",iAddr,iReadLen1st);
	if(iReadLen1st < (DCEM_CMD20_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		Sleep(DCEM_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 01 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_can_DCEM.cByRecvBuf + iReadLen1st,
			&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= DCEM_CMD20_RCV_FRAMES_NUM)
	{
		//printf("\n^_^OK,Run here to unpack cmd 00!\n");
		if (DCEMUnpackBarCode(iAddr,
			iReadLen1st + iReadLen2nd,
			g_can_DCEM.cByRecvBuf))
		{
			//printf("\n^_^OK,Run here to unpack cmd 00! and the string is %s\n",g_can_DCEM.cByRecvBuf);
			//ClrDCEMIntrruptTimes(iAddr);
		}
		else
		{
			//IncDCEMIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		//IncDCEMIntrruptTimes(iAddr);
		//int iRST = ((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN);
		//printf("\n********** 00 CMD LENGH = %d **********\n",iRST);
		return CAN_SAMPLE_FAIL;
	}

	//printf("#######  DCEM SampCmd01  ok \n");

	return CAN_SAMPLE_OK;
}
/*=============================================================================*
* FUNCTION: DCEM _Reconfig(void)
* PURPOSE : Scan the DCEM equipment
*			
* INPUT:	void
*     
*
* RETURN:
*			void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void DCEM_Reconfig(void)
{
	INT32		i;
	INT32		iAddr;
	BOOL		iRst;
	INT32		iSeqNum;
	//int		iNoExist_No;
	iSeqNum		= 0;

	//Max address CAN_DCEM_MAX_NUM
	for (iAddr = 0; iAddr < CAN_DCEM_MAX_NUM; iAddr++)	
	{

		iRst		= CAN_SAMPLE_FAIL;

		for (i = 0; i < DCEM_SCAN_TRY_TIME; i++)
		{
			iRst = DCEMSampBarCode_20(iAddr);

			if (CAN_SAMPLE_OK == iRst)
			{
				break;
			}
			else
			{
				Sleep(30);
				//CAN_ClearReadBuf();
			}
		}

		//iRst		= DCEMSampCmd20(iActualAddr);
		//printf("  reconfig 20   iActualAddr = %d \n",iActualAddr);
		if (CAN_SAMPLE_OK == iRst)
		{
			//printf("  SCAN 20 ok  EXIST ActualADDR =%d  iAddr =%d\n",iActualAddr,iAddr);

			g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_EXIST_STAT].iValue = CAN_DCEM_EXIST;
			g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_SEQ_NUM].iValue = iSeqNum;
			g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ADDR].iValue = iAddr;
			iSeqNum++;
		}
		else
		{
			g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_EXIST_STAT].iValue = CAN_DCEM_NON_EXIST;
			g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_SEQ_NUM].iValue = -1;
			g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ADDR].iValue = iAddr;
			//Continue!! Next address!!
		}

		Sleep(60);
	}

	//for (iAddr = 0; iAddr < CAN_DCEM_MAX_NUM; iAddr++)
	//{
	//	if(-1 == g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_SEQ_NUM].iValue)
	//	{
	//		g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_SEQ_NUM].iValue = iNoExist_No;
	//		iNoExist_No++;
	//	}
	//}

	//iSeqNum = 8; //for debug use only
	if(iSeqNum > 0)
	{
		g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_NUM].iValue = iSeqNum;
		g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_GROUP_STATE].iValue = CAN_DCEM_EXIST;
	}
	else
	{
		g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_NUM].iValue = 0;
		g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_GROUP_STATE].iValue = CAN_DCEM_NON_EXIST;
	}
}


/*LOCAL BOOL BarCodeDataIsValid(BYTE byData)
{
	if ((0x20 != byData) && (0xff != byData))
	{
		return TRUE;
	}

	return FALSE;
}*/

/*********************************************************************************
*  
*  FUNCTION NAME : DCEM_GetProdctInfo
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-19 11:52:03
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
void DCEM_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	INT32	iTemp;
	INT32	iAddr, k;
	BYTE	byTempBuf[28];//总共28个字节
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *pInfo;
	pInfo = (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST		 6//##
#define		BARCODE_VV_OFST			 1//##

	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");

	for(iAddr = 0; iAddr < CAN_DCEM_MAX_NUM; iAddr++)
	{
		if ((CAN_DCEM_INVALID_INI_VALUE == g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_VERSION_NO].iValue)
			&&(CAN_DCEM_INVALID_INI_VALUE == g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_BARCODE1].iValue))
		{
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			//printf("\n	This is ADDR=%d continue	\n",iAddr);
			continue;
		}
		else
		{
			//printf("\n	Can SM Temp Samp: This is ADDR=%d  and bSigModeUsed=TRUE	\n",iAddr);
			pInfo->bSigModelUsed = TRUE;
		}
		for(k = 0; k < 7; k++) //ROUGH_DCEM_BARCODE1 -- ROUGH_DCEM_BARCODE7
		{
			byTempBuf[3 + 4*k] = (BYTE)(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_BARCODE1 + k].uiValue);
			byTempBuf[2 + 4*k] = (BYTE)(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_BARCODE1 + k].uiValue >> 8);
			byTempBuf[1 + 4*k] = (BYTE)(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_BARCODE1 + k].uiValue >> 16);
			byTempBuf[0 + 4*k] = (BYTE)(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_BARCODE1 + k].uiValue >> 24);
			//printf("\n[Barcode %d]: %c:%c:%c:%c;",k+1,byTempBuf[0+ 4*k],byTempBuf[1+ 4*k],byTempBuf[2+ 4*k],byTempBuf[3+ 4*k]);
		}
		//生产地点
		snprintf(pInfo->szSerialNumber + BARCODE_FF_OFST, 3, "%d%d",byTempBuf[0]%10,byTempBuf[1]%10);		
		//年
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",byTempBuf[2]%10,byTempBuf[3]%10);
		//月
		//iTemp = (INT32)(byTempBuf[4] * 10) + (INT32)byTempBuf[5];
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",byTempBuf[4]%10,byTempBuf[5]%10);	
		//#### 代表当月生产数量
		uiTemp = (INT32)(byTempBuf[6] * 10000) + (INT32)(byTempBuf[7] * 1000) + (INT32)(byTempBuf[8] * 100)
			+ (INT32)(byTempBuf[9] * 10) + (INT32)byTempBuf[10];
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",(LONG)uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		
		//printf("\n**Serial Number: %s",pInfo->szSerialNumber);
		//模块版本号
		//H		A00形式，高字节按A=0 B=1类推，所以A=65
		byTempBuf[0] = 0x3f & byTempBuf[24];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = (BYTE)uiTemp;

		//VV 00按照实际数值存入低字节
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[25]%10, byTempBuf[26]%10);
		pInfo->szHWVersion[4] = '\0';//end
		//printf("\n**HW Version: %s",pInfo->szHWVersion);
		//软件版本:如若软件版本为1.30,保存内容为两两字节整型数130
		uiTemp = (g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_VERSION_NO].uiValue) >> 8; //can抓包发现 为67 00，而不是00 67，故移位
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end
		//printf("\n**SW Version: %s",pInfo->szSWVersion);
		//产品类型
		pInfo->szPartNumber[0] = byTempBuf[11];
		//模块类型12--23
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13
		for(k = 12; k < 23;k++)
		{
			if (BarCodeDataIsValid(byTempBuf[k]))
			{
				pInfo->szPartNumber[k-11] = byTempBuf[k];
			}
			else
			{
				pInfo->szPartNumber[k-11] = '\0';
			}	
		}
	
		pInfo->szPartNumber[13] = '\0';//CODE25
		//printf("\n**Part Number: %s",pInfo->szPartNumber);
		//printf("\nDCEM bSigModelUsed:%d\n", pInfo->bSigModelUsed);
		//printf("\nDCEM PartNumber:%s\n", pInfo->szPartNumber);//1DCEM+
		//printf("\nDCEM	version:%s\n", pInfo->szSWVersion); //1.20
		//printf("\nDCEM version:%s\n", pInfo->szHWVersion); //A00
		//printf("\nDCEM Serial Number:%s\n", pInfo->szSerialNumber);//03100500006

		pInfo++;
	}
	//printf("\n	########		END		Convert_	GetProdctInfo	########	\n");
}



static BOOL DCEM_AllIsNoRespone()
{
	INT32	iAddr;

	for (iAddr = 0; iAddr < CAN_DCEM_MAX_NUM; iAddr++)
	{
		if(CAN_DCEM_EXIST == g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_EXIST_STAT].iValue)
		{
			//只要有一个通信正常，就将返回FALSE
			if (CAN_DCEM_COMM_OK == g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_COMM_STAT].iValue)
			{
				return FALSE;
			}
		}
	}

	return TRUE;
}
static void DCEM_Cal_TotalEnergy()
{
	int iAddr = 0;
	float fTotal = 0.0; //总电量
	int iExistNum =0;
	int iCommFailNum = 0;

	for (iAddr = 0; iAddr < CAN_DCEM_MAX_NUM; iAddr++)	
	{
		if(CAN_DCEM_EXIST == g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_EXIST_STAT].iValue)
		{	
			//20131017 lkf 应天柱要求:
		        //   1)所有DCEM若通讯中断则需要把总能量置为无效值
		        //   2)只累加通讯正常的能量
			iExistNum++;
			if(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_COMM_STAT].iValue == CAN_DCEM_COMM_FAIL)
			{
			    iCommFailNum++;
			}
			else
			{
			    if(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL1].fValue > 0.0)
			    {
				    fTotal += g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL1].fValue;
			    }
			    if(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL2].fValue > 0.0)
			    {
				    fTotal += g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL2].fValue;
			    }
			    if(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL3].fValue > 0.0)
			    {
				    fTotal += g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL3].fValue;
			    }
			    if(g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL4].fValue > 0.0)
			    {
				    fTotal += g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_ENGY_CHANNEL4].fValue;
			    }
			}
		}
	}

	//不存在DCEM，或者无通讯正常的DCEM，置总能量为无效值
	if((iExistNum == 0) || (iExistNum == iCommFailNum))
	{
	    g_can_DCEM.aRoughData_Group[ROUGH_GROUP_TOTAL_ENERGY].iValue = CAN_SAMP_INVALID_VALUE;
	}
	else
	{
	    g_can_DCEM.aRoughData_Group[ROUGH_GROUP_TOTAL_ENERGY].fValue = fTotal;
	}

}
/*=============================================================================*
* FUNCTION: DCEM _Reconfig(void)
* PURPOSE : Scan the DCEMlus equipment
*			
* INPUT:	void
*     
*
* RETURN:
*			void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void DCEM_Sample(void)
{
	INT32			iAddr;

	//G3_OPT [loader], condition add by Lin.Tao.Thomas, 2013-4
	if (!g_can_DCEM.bConfigured)    
	{
		return;
	}
	//G3_OPT end

	//if(g_can_DCEMlus.bNeedReconfig || ReceiveReconfigCmd())
	if(g_can_DCEM.bNeedReconfig)
	{
		RT_UrgencySendCurrLimit();

		DCEM_InitRoughValue();

		//First running set TRUE in the function CAN_QueryInit()
		g_can_DCEM.bNeedReconfig = FALSE;

		//printf("1The SM Temp Num is %d\n",g_can_dcem.aRoughData_Group[ROUGH_GROUP_DCEM_NUM].iValue);

		DCEM_Reconfig();

		//printf("2The SM Temp Num is %d\n",g_can_dcem.aRoughData_Group[ROUGH_GROUP_DCEM_NUM].iValue);
		return;
	}
	//sample stats data, every 3600*24 sec
	//static int nCircle = 0;
	//if(nCircle < 3600*10)
	//{
	//	nCircle++;
	//	//return;
	//}
	//else
	//{
	//	nCircle = 0;
	//}
	//if(nCircle == 1) //第一遍也采集
	//{
	//	//printf("\n!!!!Run here to ssamp sm temp!\n");
	//	for (iAddr = 0; iAddr < CAN_DCEM_MAX_NUM; iAddr++)	
	//	{
	//		if(CAN_DCEM_EXIST == g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_EXIST_STAT].iValue)
	//		{
	//			DCEMSampCmd30(iAddr);
	//		}
	//		else
	//		{
	//			//Jump to Next Query,Now Don't Sampling Voltage
	//		}
	//	}
	//}
	//printf("\n!!!!Run here to ssamp sm temp!\n");
	static int iCount = 0;
	if (iCount < 3)
	{
		iCount++;
		return;
	}
	else
	{
		iCount = 0;
	}
	for (iAddr = 0; iAddr < CAN_DCEM_MAX_NUM; iAddr++)	
	{
		if(CAN_DCEM_EXIST == g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_EXIST_STAT].iValue)
		{
			DCEMSampCmd00(iAddr);
			//DCEMSampBarCode_20(iAddr);
		}
		else
		{
			//Jump to Next Query,Now Don't Sampling Voltage
		}
	}

	//TRACE(" !!!!   IN  THE	DCEM _Sample    END \n");

	//计算总电量,供EEM使用
	DCEM_Cal_TotalEnergy();

	//不上填充，内部使用
	if (DCEM_AllIsNoRespone())
	{
		g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_G_COMM_STAT].iValue = CAN_DCEM_COMM_FAIL;
	}
	else
	{
		g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_G_COMM_STAT].iValue = CAN_DCEM_COMM_OK;
	}
}
/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-26 15:00:18
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static int GetFirst_Invalid_DCEM_Addr(void)
{
	int		i;

	for(i = 0; i < CAN_DCEM_MAX_NUM; i++)
	{
		if(g_can_DCEM.aRoughData[i][ROUGH_DCEM_EXIST_STAT].iValue 
			== CAN_DCEM_NON_EXIST)
		{
			return i;
		}
	}
	return -1;
}

/*********************************************************************************
*  
*  FUNCTION NAME : Calc_ST_Data
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-3-5 16:36:18
*  DESCRIPTION   : calculate signals from stats data
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL void Calc_ST_Data(int iAddr,ENUMSIGNALPROC EnumProc,LPVOID lpvoid, BOOL bExisted)
{
	int i,k,n;
	float fMaxValue,fAvgValue,fTotal;
	float fTempVal;
	int iCount_NoSupply;
	int iCh_SigIdx[] = {DCEM_CH_PEAK_ENGY_CHANNEL1,
			 DCEM_CH_AVG_ENGY_CHANNEL1,
			 DCEM_CH_TOTAL_ENGY_CHANNEL1,
			 DCEM_CH_NO_SUPPLY_CHANNEL1,
			 
			 DCEM_CH_PEAK_ENGY_CHANNEL2,	
			 DCEM_CH_AVG_ENGY_CHANNEL2,	
			 DCEM_CH_TOTAL_ENGY_CHANNEL2,	
			 DCEM_CH_NO_SUPPLY_CHANNEL2,	
			 
			 DCEM_CH_PEAK_ENGY_CHANNEL3,	
			 DCEM_CH_AVG_ENGY_CHANNEL3,	
			 DCEM_CH_TOTAL_ENGY_CHANNEL3,	
			 DCEM_CH_NO_SUPPLY_CHANNEL3,	
			 
			 DCEM_CH_PEAK_ENGY_CHANNEL4,	
			 DCEM_CH_AVG_ENGY_CHANNEL4,	
			 DCEM_CH_TOTAL_ENGY_CHANNEL4,	
			 DCEM_CH_NO_SUPPLY_CHANNEL4};
	if(!bExisted)
	{
		for( k = 0 ; k < ITEM_OF(iCh_SigIdx); k ++)
		{
			EnumProc(iAddr * DCEM_CH_PER_CH + iCh_SigIdx[k], 
				CAN_DCEM_INVALID_VALUE, 
				lpvoid );
		}
	}
	else
	{
		for(n = 0; n < 4; n++) //n=chanel num =4
		{
			fMaxValue = 0.0;
			fTotal = 0.0;
			fAvgValue = 0.0;
			iCount_NoSupply = 0;
			for (i = 0;i < MAX_DAYS_NEED_SAMPLE;i++)
			{
				fTempVal=  g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_CHANNEL1_DAY1 + n + i*4].fValue;

				if(fTempVal == 0.0)
				{
					iCount_NoSupply ++;
				}
				else
				{
					fTotal += fTempVal;
					if(fTempVal > fMaxValue)
					{
						fMaxValue = fTempVal;
					}
				}
			}
			fAvgValue = fTotal / MAX_DAYS_NEED_SAMPLE;
			EnumProc(iAddr * DCEM_CH_PER_CH + DCEM_CH_PEAK_ENGY_CHANNEL1 + 4*n, //4表示每个channel有四个参数
				fMaxValue, 
				lpvoid );
			EnumProc(iAddr * DCEM_CH_PER_CH + DCEM_CH_AVG_ENGY_CHANNEL1 + 4*n, //4表示每个channel有四个参数
				fAvgValue, 
				lpvoid );
			EnumProc(iAddr * DCEM_CH_PER_CH + DCEM_CH_TOTAL_ENGY_CHANNEL1 + 4*n, //4表示每个channel有四个参数
				fTotal, 
				lpvoid );
			EnumProc(iAddr * DCEM_CH_PER_CH + DCEM_CH_NO_SUPPLY_CHANNEL1 + 4*n, //4表示每个channel有四个参数
				(float)iCount_NoSupply, 
				lpvoid );
		}

	}

}
/*=============================================================================*
* FUNCTION: DCEM _StuffChannel(ENUMSIGNALPROC EnumProc,
*											LPVOID lpvoid)	
* PURPOSE : 
*			
* INPUT:	void
*     
* RETURN:
*			void
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void DCEM_StuffChannel(ENUMSIGNALPROC EnumProc,
			 LPVOID lpvoid)	
{
	INT32				i,j;
	INT32				iVirtualAddr;


	CHANNEL_TO_ROUGH_DATA		DCEM_Group_Sig[] =
	{
		{DCEM_CH_GROUP_EXISTE_STATE,		ROUGH_GROUP_DCEM_GROUP_STATE	},
		{DCEM_CH_NUM,				ROUGH_GROUP_DCEM_NUM		},
		{DCEM_CH_GROUP_TOTAL_ENERGY,		ROUGH_GROUP_TOTAL_ENERGY	}, //计算总电量
		{DCEM_CH_END_FLAG,			DCEM_CH_END_FLAG		}//end
	};

	CHANNEL_TO_ROUGH_DATA		DCEM_Sig[] =
	{
		{DCEM_CH_BUS_VOLT,			ROUGH_DCEM_BUS_VOLT	},
		{DCEM_CH_CHANNEL1_ENGY,			ROUGH_DCEM_ENGY_CHANNEL1},
		{DCEM_CH_CHANNEL2_ENGY,			ROUGH_DCEM_ENGY_CHANNEL2},
		{DCEM_CH_CHANNEL3_ENGY,			ROUGH_DCEM_ENGY_CHANNEL3},
		{DCEM_CH_CHANNEL4_ENGY,			ROUGH_DCEM_ENGY_CHANNEL4},
		{DCEM_CH_CHANNEL1_CURR,			ROUGH_DCEM_CURR_CHANNEL1},
		{DCEM_CH_CHANNEL2_CURR,			ROUGH_DCEM_CURR_CHANNEL2},
		{DCEM_CH_CHANNEL3_CURR,			ROUGH_DCEM_CURR_CHANNEL3},
		{DCEM_CH_CHANNEL4_CURR,			ROUGH_DCEM_CURR_CHANNEL4},
	
		{DCEM_CH_COMM_STAT,			ROUGH_DCEM_COMM_STAT	},
		{DCEM_CH_EXIST_STAT,			ROUGH_DCEM_EXIST_STAT	},
		{DCEM_CH_END_FLAG,			DCEM_CH_END_FLAG	}//end
	};

	////////////////////////////////////////////////////////////////
	/***************************************************************/
	//Stuff Group signals
	i = 0;
	while(DCEM_Group_Sig[i].iChannel != DCEM_CH_END_FLAG)
	{
		EnumProc(DCEM_Group_Sig[i].iChannel, 
			g_can_DCEM.aRoughData_Group[DCEM_Group_Sig[i].iRoughData].fValue, 
			lpvoid );

		//printf(" !!! @@ GGGGG sig seq i = %d  iValue = %d fValue = %f\n",DCEM_Group_Sig[i].iChannel,
		//	g_can_DCEM.aRoughData_Group[DCEM_Group_Sig[i].iRoughData].iValue,
		//	g_can_DCEM.aRoughData_Group[DCEM_Group_Sig[i].iRoughData].fValue);

		i++;
	}

	int iFirst_NoExist_DCEM = GetFirst_Invalid_DCEM_Addr();

	//printf(" \n  @@@  iExistDCEMNum = %d \n",iExistDCEMNum);

	//Stuff exist DCEM signals
	for (i = 0; i < CAN_DCEM_MAX_NUM; i++)
	{
		//Do not use this way, just keep the addr and data the same index, 2011.11.22 Jimmy Wu
		//iVirtualAddr = DCEM_Swap_Addr_From_Seq(i);
		iVirtualAddr = (g_can_DCEM.aRoughData[i][ROUGH_DCEM_EXIST_STAT].iValue == CAN_DCEM_EXIST) ? i : -1;
		//printf("\n---------BEGIN FOR DCEM%d---------\n",i);
		//printf("index \t RoughChan No \t RealChan No \t Value\n",i);
		//j = 0;
		//while(DCEM_Sig[j].iChannel != DCEM_CH_END_FLAG)
		//{
		//	printf("%d \t %d \t %d \t\t %d\n",
		//				j+1,
		//				DCEM_Sig[j].iRoughData,
		//				DCEM_Sig[j].iChannel + i * DCEM_CH_PER_CH ,
		//				g_can_DCEM.aRoughData[i][DCEM_Sig[j].iRoughData].iValue);

		//	j++;
		//}
		//printf("\n---------END---------\n",i);
		
		//Get Virtual address error!!
		j = 0;
		if (-1 == iVirtualAddr)
		{
			if(iFirst_NoExist_DCEM < 0)
			{
				break;
			}
			while(DCEM_Sig[j].iChannel != DCEM_CH_END_FLAG)
			{
				EnumProc(i * DCEM_CH_PER_CH + DCEM_Sig[j].iChannel, 
					g_can_DCEM.aRoughData[iFirst_NoExist_DCEM][DCEM_Sig[j].iRoughData].fValue, 
					lpvoid );

				//printf("Addr=%d SIG NUM= %d iValue= %d fValue= %f\n",
				//			iVirtualAddr,
				//			i * DCEM_CH_PER_CH + DCEMSig[j].iChannel,
				//			g_can_DCEMlus.aRoughData[iVirtualAddr][DCEMSig[j].iRoughData].iValue,
				//			g_can_DCEMlus.aRoughData[iVirtualAddr][DCEMSig[j].iRoughData].fValue);

				j++;
			}
			//Calc_ST_Data(i,EnumProc,lpvoid,FALSE);
		}
		else
		{
			////according to CR, NO display temp value where not installed
			//int k;
			//for(k = 0;k < 8;k++)
			//{
			//	if(g_can_DCEM.aRoughData[iVirtualAddr][ROUGH_DCEM_TEMP1_STATUS + k].iValue == (int)DCEM_PROBE_NOT_INSTALL)
			//	{
			//		g_can_DCEM.aRoughData[iVirtualAddr][ROUGH_DCEM_TEMP1 + k].iValue = CAN_DCEM_INVALID_VALUE;
			//	}
			//}
			////end editing
			while(DCEM_Sig[j].iChannel != DCEM_CH_END_FLAG)
			{
				//printf("%d):The rough value is %f\n",j+1,g_can_dcem.aRoughData[iVirtualAddr][DCEM_Sig[j].iRoughData].fValue);
				EnumProc(i * DCEM_CH_PER_CH + DCEM_Sig[j].iChannel, 
					g_can_DCEM.aRoughData[iVirtualAddr][DCEM_Sig[j].iRoughData].fValue, 
					lpvoid );

				j++;
			}
			//Calc_ST_Data(i,EnumProc,lpvoid,TRUE);
		}
	}
}
/*==========================================================================*
* FUNCTION : GetSmSeqNoByAddr
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:28
*==========================================================================*/
static int GetSeqNoByAddr(int iAddr)
{
	return g_can_DCEM.aRoughData[iAddr][ROUGH_DCEM_SEQ_NUM].iValue;
}

/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-3-6 16:18:38
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL BYTE* Combine2Uint(ULONG uiVal1,ULONG uiVal2)
{
	BYTE byTemp[4];

	//byTemp[0] = (BYTE)(uiVal2 & 0x00FF);
	//byTemp[1] = (BYTE)(uiVal1 & 0x00FF);
	//byTemp[2] = (BYTE)((uiVal2 & 0xFF00)>>8);
	//byTemp[3] = (BYTE)((uiVal1 & 0xFF00)>>8);

	//byTemp[1] = (BYTE)((uiVal1 & 0xFF00)>>8);
	//byTemp[0] = (BYTE)(uiVal1 & 0x00FF);
	//byTemp[3] = (BYTE)((uiVal2 & 0xFF00)>>8);
	//byTemp[2] = (BYTE)(uiVal2 & 0x00FF);
 //Jimmy 20120607 更改：传送的字节顺序反了
	byTemp[0] = (BYTE)((uiVal1 & 0xFF00)>>8);
	byTemp[1] = (BYTE)(uiVal1 & 0x00FF);
	byTemp[2] = (BYTE)((uiVal2 & 0xFF00)>>8);
	byTemp[3] = (BYTE)(uiVal2 & 0x00FF);


	return byTemp;
}
/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-3-6 16:18:38
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL void PackAndSendCmdForShuntSize(UINT uiMsgType,
			  UINT uiDestAddr,
			  UINT uiCmdType,
			  UINT uiValueTypeH,
			  UINT uiValueTypeL,
			  ULONG ulVar1,ULONG ulVar2)
{
	BYTE*	pbySendBuf = g_can_DCEM.cBySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_DCEM_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);
	/////////////////////for debug only
	//int i;
	//printf("\n*****The sent package is: ");
	//for(i=0;i<13;i++)
	//{
	//	printf("%2X",g_can_DCEM.cBySendBuf[i]);
	//
	//}
	//printf(" *****end\n ");
	////////////end 

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//CAN_FloatToString(ulParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);
	int	i;	
	FLOAT_STRING	unValue;
	BYTE* strOutput = pbySendBuf + CAN_FRAME_OFFSET_VALUE;
	BYTE* pbyTempVar = Combine2Uint(ulVar1,ulVar2);

	for(i = 0; i < 4; i++)
	{		
		strOutput[i] = pbyTempVar[i];
	}
//printf("**********Original values are %d:%d:%d:%d;\n", strOutput[0],strOutput[1],strOutput[2],strOutput[3]);
	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}
/*==========================================================================*
* FUNCTION : DCEM_Param_Unify
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Jimmy Wu                DATE: 2012-03-06
*==========================================================================*/
void DCEM_Param_Unify(void)
{		
	//static iCount = 0;
	//if (iCount < 3)
	//{
	//	iCount++;
	//	return;
	//}
	//else
	//{
	//	iCount = 0;
	//}
	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"DCEM Get SysVolt Level");

	PARAM_UNIFY_IDX		ParamIdx[] =
	{
		{DCEM_EQUIP_ID,1,DCEM_SETTING_SHUNT1_MV,DCEM_SETTING_SHUNT1_MV,ROUGH_DCEM_VOLT_SHUNT1,DCEM_VAL_TYPE_W_SHUNT1_SIZE,},
		//{DCEM_EQUIP_ID,1,DCEM_SETTING_SHUNT1_A,DCEM_SETTING_SHUNT1_A,ROUGH_DCEM_CURR_CHANNEL1,DCEM_VAL_TYPE_W_SHUNT1_SIZE,},

		{DCEM_EQUIP_ID,1,DCEM_SETTING_SHUNT2_MV,DCEM_SETTING_SHUNT2_MV,ROUGH_DCEM_VOLT_SHUNT2,DCEM_VAL_TYPE_W_SHUNT2_SIZE,},
		//{DCEM_EQUIP_ID,1,DCEM_SETTING_SHUNT2_A,DCEM_SETTING_SHUNT2_A,ROUGH_DCEM_CURR_CHANNEL2,DCEM_VAL_TYPE_W_SHUNT2_SIZE,},

		{DCEM_EQUIP_ID,1,DCEM_SETTING_SHUNT3_MV,DCEM_SETTING_SHUNT3_MV,ROUGH_DCEM_VOLT_SHUNT3,DCEM_VAL_TYPE_W_SHUNT3_SIZE,},
		//{DCEM_EQUIP_ID,1,DCEM_SETTING_SHUNT3_A,DCEM_SETTING_SHUNT3_A,ROUGH_DCEM_CURR_CHANNEL3,DCEM_VAL_TYPE_W_SHUNT3_SIZE,},

		{DCEM_EQUIP_ID,1,DCEM_SETTING_SHUNT4_MV,DCEM_SETTING_SHUNT4_MV,ROUGH_DCEM_VOLT_SHUNT4,DCEM_VAL_TYPE_W_SHUNT4_SIZE,},
		//{DCEM_EQUIP_ID,1,DCEM_SETTING_SHUNT4_A,DCEM_SETTING_SHUNT4_A,ROUGH_DCEM_CURR_CHANNEL4,DCEM_VAL_TYPE_W_SHUNT4_SIZE,},

		{0, 0, DCEM_CH_END_FLAG, DCEM_CH_END_FLAG, 0, 0,},
	};

	int i, j;
	int iSigId;
	DWORD uiParam_mV,uiParam_A;
	DWORD uiSampleValue_mV,uiSampleValue_A;
	for(i = 0; i < CAN_DCEM_MAX_NUM; i++)
	{
		//int iSeqNo = GetSeqNoByAddr(i);
		//if(iSeqNo >= 0)
		{
			if(CAN_DCEM_EXIST == g_can_DCEM.aRoughData[i][ROUGH_DCEM_EXIST_STAT].iValue)
			{
				j = 0;
				while(ParamIdx[j].iSigId != DCEM_CH_END_FLAG)
				{
					//if(g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue > 35.0)
					if(stVoltLevelState == 0)//48 V
					{
						iSigId = ParamIdx[j].iSigId;
					}
					else
					{
						iSigId = ParamIdx[j].iSigId24V;
					}

					uiSampleValue_mV = g_can_DCEM.aRoughData[i][ParamIdx[j].iRoughData].dwValue; //mV
					uiSampleValue_A = g_can_DCEM.aRoughData[i][ParamIdx[j].iRoughData + 1].dwValue; //A
					//printf("\n*******Now the Channel%d shunt size is: %d,%d\n",j+1,uiSampleValue_mV,uiSampleValue_A);

					uiParam_mV = GetDwordSigValue(ParamIdx[j].iEquipId 
						+ i * ParamIdx[j].iEquipIdDifference, 
						SIG_TYPE_SETTING,
						iSigId,
						"CAN_SAMP");
					uiParam_A = GetDwordSigValue(ParamIdx[j].iEquipId 
						+ i * ParamIdx[j].iEquipIdDifference, 
						SIG_TYPE_SETTING,
						iSigId + 1,
						"CAN_SAMP");
					//printf("\n*******Now the Channel%d shunt size is: %ld,%ld\n",j+1,uiParam_mV,uiParam_A);
					//只发一次不可靠，所以还是每圈都发
					//if((uiParam_A != uiSampleValue_A) || (uiParam_mV != uiSampleValue_mV))
					{
						g_can_DCEM.aRoughData[i][ParamIdx[j].iRoughData].dwValue
							= uiParam_mV; //mV
						g_can_DCEM.aRoughData[i][ParamIdx[j].iRoughData + 1].dwValue
							= uiParam_A; //A
						//printf("\n*******Now the Channel%d shunt size is: %d,%d\n",j+1,uiSampleValue_mV,uiSampleValue_A);

						PackAndSendCmdForShuntSize(MSG_TYPE_RQST_SETTINGS,
							(UINT)i,
							CAN_CMD_TYPE_P2P,
							0,
							ParamIdx[j].uiValueTypeL,
							uiParam_mV,uiParam_A);
						Sleep(100);
					}

					j++;
				}
			}
		}

	}
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : can_converter.c
 *  CREATOR  : Frank Cao                DATE: 2008-07-01 11:33
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "can_sampler_main.h"
#include "can_converter.h"

extern CAN_SAMPLER_DATA	g_CanData;



#define CT_CONFIRM_POS_SIGID		21
static BOOL	CtAnalyseSerialNo(int iAddr);
static BOOL CtAnalyseFeature(int iAddr);
static void CtSetGoupFeatureSig(void);
static void SetConvRunTime(int iAddr, float fParam);
static void RefreshConvRunTime(void);

#define	INVALID_CONV_TYPE	(0xffffffff)


//added by Jimmy Wu for clearing rectifier IDs
static BOOL g_bNeedClearConvIDs = FALSE;
static void ClrConvFalshInfo(void);//���λ�ú���Ϣ
static void ClrConvFalshInfo()
{
	int i = 0;
	for(i = 0; i < MAX_NUM_CONVERT; i++)
	{
		g_CanData.CanFlashData.aConverterInfo[i].dwSerialNo = 0;
		g_CanData.CanFlashData.aConverterInfo[i].iPositionNo = -1;
	}
	CAN_WriteFlashData(&(g_CanData.CanFlashData));
	g_bNeedClearConvIDs = TRUE;
}


/*==========================================================================*
 * FUNCTION : NeedHandleCtPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:43
 *==========================================================================*/
static BOOL NeedHandleCtPos(void)
{

	if(GetDwordSigValue(CT_GROUP_EQUIPID, 
						SIG_TYPE_SETTING,
						CT_CONFIRM_POS_SIGID,
						"CAN_SAMP") > 0)
	{
		SetDwordSigValue(CT_GROUP_EQUIPID,
						SIG_TYPE_SETTING,
						CT_CONFIRM_POS_SIGID,
						0,
						"CAN_SAMP");
		return TRUE;
	}

	return FALSE;
}



#define CT_MAX_POS_NO		999

/*==========================================================================*
 * FUNCTION : GetOneRestConvertPosNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:42
 *==========================================================================*/
static int GetOneRestConvertPosNo(void)
{
	int		i , j;
	int		iConvertNum = g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum;


	for(i = 1; i < CT_MAX_POS_NO; i++)
	{
		BOOL	bUsed = FALSE;
		for(j = 0; j < iConvertNum; j++)
		{
			if(i == g_CanData.aRoughDataConvert[j][CONVERT_POSITION_NO].iValue)
			{
				bUsed = TRUE;
				break;
			}
		}

		if(!bUsed)
		{
			return i;
		}
	}
	return i;
}




/*==========================================================================*
 * FUNCTION : CtPosUniquenessHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-07 14:42
 *==========================================================================*/
static void CtPosUniquenessHandle(void)
{
	int	i, j;
	int	iConvertNum = g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum;

	if(!iConvertNum)
	{
		return;
	}

	for(i = 1; i < iConvertNum; i++)
	{
		int iPos = g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue;
		for(j = 0; j < i; j++)
		{	
			if(iPos == g_CanData.aRoughDataConvert[j][CONVERT_POSITION_NO].iValue)
			{
				g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue
					= GetOneRestConvertPosNo();
				g_CanData.CanFlashData.aConverterInfo[i].iPositionNo 
					= g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue;
			}
		}
	}

	return;
}



/*==========================================================================*
 * FUNCTION : HandleCtPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:43
 *==========================================================================*/
static void HandleCtPos(BOOL bReadConfirm)
{
	int			i, j;
	int			iConvNum = g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum;
	if(iConvNum <= 0)
	{
		return;
	}
	//Ranged by sequence No.
	CONVERT_POS_ADDR	Flash_Map[MAX_NUM_CONVERT]; //�洢��Flash��Map
	CONVERT_POS_ADDR	sFinal_Map[MAX_NUM_CONVERT]; //������õ�Map
	//��ʼ��
	static BOOL bFirstTime = TRUE;
	int iFlashMapedNo = 0;
	int iSeqNo = 0;
	//printf("\n_____________BEGIN__________");
	for(i = 0; i < MAX_NUM_CONVERT; i++)
	{
		if((g_CanData.CanFlashData.aConverterInfo[i].dwSerialNo > 0) && 
			(g_CanData.CanFlashData.aConverterInfo[i].dwSerialNo != -1) &&
			(g_CanData.CanFlashData.aConverterInfo[i].iPositionNo > 0) &&
			(g_CanData.CanFlashData.aConverterInfo[i].iPositionNo <= 999))
		{
			Flash_Map[i].dwSerialNo = g_CanData.CanFlashData.aConverterInfo[i].dwSerialNo;
			//Flash_Map[i].dwHiSN = g_CanData.CanFlashData.aConverterInfo[i].dwHighSn;
			Flash_Map[i].iPositionNo = g_CanData.CanFlashData.aConverterInfo[i].iPositionNo;
			iFlashMapedNo++;
			//printf("\n i=%d; dwSerialNo=%d; iPos=%d",i,
			//	Flash_Map[i].dwSerialNo,Flash_Map[i].iPositionNo);
		}
		sFinal_Map[i].iPositionNo = -1;
		sFinal_Map[i].bIsNewInserted = TRUE;
		sFinal_Map[i].iSeqNo = -1;
		sFinal_Map[i].dwSerialNo = 0;
	}
	//printf("_____________END,and the FlashMappedNo=%d__________\n",iFlashMapedNo);
	//bFirstTime = FALSE;
	//���´�Flash��λ�úŲ�ƥ����֪��ģ��λ�ú�
	BOOL bIsNeedWriteFlash = TRUE; //�Ƿ���Ҫд��Flash
	if(iFlashMapedNo > iConvNum && (!bReadConfirm)) //˵����ģ�鶪ʧ��
	{
		bIsNeedWriteFlash = FALSE;
	}
	BOOL bHasInvalidConv= FALSE;
	BOOL bHasRepeastedRect = FALSE;//�Ƿ������кų�ͻ
	//������кų�ͻ
	for(i = 0; i < iConvNum; i++)
	{
		for(j = i + 1; j < iConvNum; j++)
		{
			if(g_CanData.aRoughDataConvert[i][CONVERT_SERIAL_NO_LOW].dwValue == g_CanData.aRoughDataConvert[j][CONVERT_SERIAL_NO_LOW].dwValue)
			{
				bHasRepeastedRect = TRUE;
				break;
			}
		}
	}
	if(!bReadConfirm)
	{
		for(i = 0; i < iConvNum; i++)
		{
			//sFinal_Map[i].dwHiSN = g_CanData.aRoughDataConvert[i][RECT_HIGH_SN_ROUGH].dwValue;
			sFinal_Map[i].dwSerialNo = g_CanData.aRoughDataConvert[i][CONVERT_SERIAL_NO_LOW].dwValue;
			sFinal_Map[i].iSeqNo = i;
			//��ͨ���жϣ��򲻱�д��Flash���ڶ��ν����»ָ�ԭ��Flash���ֵ
			//bIsNeedWriteFlash = (sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE) ? FALSE : TRUE;
			if(sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE)
			{
				bHasInvalidConv = TRUE;
			}
			for(j = 0; j < iFlashMapedNo; j++)
			{
				if(sFinal_Map[i].dwSerialNo == Flash_Map[j].dwSerialNo /*&& 
					(sFinal_Map[i].dwHiSN == Flash_Map[j].dwHiSN)*/)
				{
					sFinal_Map[i].bIsNewInserted = FALSE;
					sFinal_Map[i].iPositionNo = Flash_Map[j].iPositionNo;
					break;
				}
			}
		}
	}
	else //��ȡ����ֵ
	{
		for(i = 0; i < iConvNum; i++)
		{
			if(i < MAX_NUM1_CONV)
			{
				//sFinal_Map[i].dwHiSN = GetDwordSigValue(RT_START_EQUIP_ID + i,
				//					SIG_TYPE_SAMPLING,
				//					26,
				//					"CAN_SAMP");
				sFinal_Map[i].dwSerialNo = GetDwordSigValue(CT_START_EQUIP_ID + i,
									SIG_TYPE_SAMPLING,
									42,
									"CAN_SAMP");
				sFinal_Map[i].iPositionNo = GetDwordSigValue(CT_START_EQUIP_ID + i,
												SIG_TYPE_SETTING,
												CT_POS_SET_SIG_ID,
												"CAN_SAMP");
			}
			else
			{
				//sFinal_Map[i].dwHiSN = GetDwordSigValue(RT_START_EQUIP_ID + i,
				//					SIG_TYPE_SAMPLING,
				//					26,
				//					"CAN_SAMP");
				sFinal_Map[i].dwSerialNo = GetDwordSigValue(CT_START_EQUIP_ID_EXTEND + i - MAX_NUM1_CONV,
									SIG_TYPE_SAMPLING,
									42,
									"CAN_SAMP");
				sFinal_Map[i].iPositionNo = GetDwordSigValue(CT_START_EQUIP_ID_EXTEND + i - MAX_NUM1_CONV,
												SIG_TYPE_SETTING,
												CT_POS_SET_SIG_ID,
												"CAN_SAMP");
			}
			//��ͨ���жϣ��򲻱�д��Flash���ڶ��ν����»ָ�ԭ��Flash���ֵ
			//bIsNeedWriteFlash = (sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE) ? FALSE : TRUE;

			if(sFinal_Map[i].dwSerialNo == 0)
			{
				bHasInvalidConv = TRUE;
			}
			sFinal_Map[i].bIsNewInserted = FALSE;

			for(j = 0; j < iConvNum; j++)
			{
				if(g_CanData.aRoughDataConvert[j][CONVERT_SERIAL_NO_LOW].dwValue == sFinal_Map[i].dwSerialNo)
				{
					sFinal_Map[i].iSeqNo = j;
					break;
				}
			}
		}
	}
	//������Ҫ����λ�úų�ͻ�����
	//���������
	int k,s,iFoundPos;
	if(bReadConfirm || bFirstTime || bHasRepeastedRect) //�û������ˣ���Ҫ��������ͻ
	{
		for(i = 0; i < iConvNum; i++)
		{
			if(sFinal_Map[i].iPositionNo < 1 || sFinal_Map[i].iPositionNo > 999)
			{
				iFoundPos = -1;
				for(k = 1; k <= MAX_NUM_CONVERT; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iConvNum; s++)
					{
						if(sFinal_Map[s].iPositionNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[i].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);
				
			}
			else if(sFinal_Map[i].iSeqNo < 0 || sFinal_Map[i].iSeqNo > iConvNum)
			{
				iFoundPos = -1;
				for(k = 0; k < iConvNum; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iConvNum; s++)
					{
						if(sFinal_Map[s].iSeqNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[i].iSeqNo = (iFoundPos > 0 ? iFoundPos : 0);
			}
			else
			{
				for(j = i+1;j < iConvNum; j++)
				{
					if(sFinal_Map[i].iPositionNo == sFinal_Map[j].iPositionNo)
					{
						iFoundPos = -1;
						for(k = 1; k <= MAX_NUM_CONVERT; k++)
						{
							iFoundPos = k;
							for(s = 0; s < iConvNum; s++)
							{
								if(sFinal_Map[s].iPositionNo == k)
								{
									iFoundPos = -1;
									break;
								}
							}
							if(iFoundPos != -1)
							{
								break;
							}
						}
						sFinal_Map[i].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);
						break;
					}

				}
				for(j = i+1;j < iConvNum; j++)
				{
					
					if(sFinal_Map[i].iSeqNo == sFinal_Map[j].iSeqNo)
					{
						iFoundPos = -1;
						for(k = 0; k < iConvNum; k++)
						{
							iFoundPos = k;
							for(s = 0; s < iConvNum; s++)
							{
								if(sFinal_Map[s].iSeqNo == k)
								{
									iFoundPos = -1;
									break;
								}
							}
							if(iFoundPos != -1)
							{
								break;
							}
						}
						sFinal_Map[i].iSeqNo = (iFoundPos > 0 ? iFoundPos : 0);
						break;
					}
				}
			}
		}
	}
	else //���ŵ��µ�
	{
		for(j = 0;j < iConvNum; j++)
		{
			if(sFinal_Map[j].iPositionNo < 1 ||
				sFinal_Map[j].bIsNewInserted)//����������һ���ط��ţ�ѡ����С�Ŀ���λ�ú�
			{
				if(!bHasInvalidConv)//���-9999��д������ͨ���жϺ��ģ���ǲ�ס��
				{
					bIsNeedWriteFlash = TRUE; //����ģ����£�˵����Ҫ����дFlash
				}

				iFoundPos = -1;
				for(k = 1; k <= MAX_NUM_CONVERT; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iConvNum; s++)
					{
						if(sFinal_Map[s].iPositionNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[j].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);	
			}
		}
	}

	//���¿�ʼ����Postion���ã��Լ�Flash
	//printf("\n__________Begin Test Data____________");
	for(i = 0; i < MAX_NUM_CONVERT; i++)
	{
		if(i < iConvNum)
		{
			j = sFinal_Map[i].iSeqNo;
			//������ע�� �� i ���� j��Ҫ����,����Ĵ���Ƚϻ�ɬ�Ѷ�����Ҫ���׸Ķ�
			g_CanData.aRoughDataConvert[i][CONVERT_SEQ_NO].iValue = j;
			g_CanData.aRoughDataConvert[j][CONVERT_POSITION_NO].iValue = sFinal_Map[i].iPositionNo;			
			
			if((bIsNeedWriteFlash && (!bHasInvalidConv)) || bFirstTime)
			{
				g_CanData.CanFlashData.aConverterInfo[i].iPositionNo = sFinal_Map[i].iPositionNo;
				g_CanData.CanFlashData.aConverterInfo[i].dwSerialNo = sFinal_Map[i].dwSerialNo;
				//g_CanData.CanFlashData.aConverterInfo[i].dwHighSn = sFinal_Map[i].dwHiSN;
				//g_CanData.CanFlashData.aConverterInfo[i].iSeqNo = sFinal_Map[i].iSeqNo;
			}
			//printf("\ni=%d,HiSN=%d,BarC1=%d;   Pos=%d;  SeqNo=%d",i,sFinal_Map[i].dwHiSN,
			//	sFinal_Map[i].dwSerialNo,sFinal_Map[i].iPositionNo,sFinal_Map[i].iSeqNo);
		}
		else
		{
			if((bIsNeedWriteFlash && (!bHasInvalidConv)) || bFirstTime)
			{
				g_CanData.CanFlashData.aConverterInfo[i].dwSerialNo = 0;
				g_CanData.CanFlashData.aConverterInfo[i].iPositionNo = -1;
			}
			
		}
	}
	//printf("\n__________End Test Data____________\n");
	
	//-----------���»�Ҫ���򣬰�#1��#2��#3.��������
	CONVERT_POS_ADDR Sorted_Map[MAX_NUM_CONVERT];//��������
	for (i = 0; i < iConvNum; i++)
	{
		Sorted_Map[i].iPositionNo = g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue;
		Sorted_Map[i].iSeqNo = i;
	}
	int iPos;
	for (i = 0; i < iConvNum - 1; i++)
	{
		for (j = 0; j < (iConvNum - 1 - i); j++)
		{
			if(Sorted_Map[j].iPositionNo > Sorted_Map[j + 1].iPositionNo)
			{
				iPos = Sorted_Map[j].iPositionNo;
				Sorted_Map[j].iPositionNo = Sorted_Map[j + 1].iPositionNo;
				Sorted_Map[j + 1].iPositionNo = iPos;
			}
		}
	}
	for (i = 0; i < iConvNum; i++)
	{
		for (j = 0; j < iConvNum; j++)
		{
			if(Sorted_Map[i].iPositionNo == g_CanData.aRoughDataConvert[j][CONVERT_POSITION_NO].iValue)
			{
				g_CanData.aRoughDataConvert[j][CONVERT_SEQ_NO].iValue = Sorted_Map[i].iSeqNo;
				break;
			}
		}
	}
	//-----------�������
	//�������ֵ
	for (i = 0; i < iConvNum; i++)
	{
		j = g_CanData.aRoughDataConvert[i][CONVERT_SEQ_NO].iValue;
		if(j < MAX_NUM1_CONV)
		{
			SetDwordSigValue(CT_START_EQUIP_ID + j,
						SIG_TYPE_SETTING,
						CT_POS_SET_SIG_ID,
						sFinal_Map[i].iPositionNo,
						"CAN_SAMP");
		}
		else
		{
			SetDwordSigValue(CT_START_EQUIP_ID_EXTEND + j - MAX_NUM1_CONV,
						SIG_TYPE_SETTING,
						CT_POS_SET_SIG_ID,
						sFinal_Map[i].iPositionNo,
						"CAN_SAMP");
		}
	}

	if((bIsNeedWriteFlash && (!bHasInvalidConv)) || bFirstTime)//�д���rectʱ��дFalsh
	{
		CAN_WriteFlashData(&(g_CanData.CanFlashData));
	}

	bFirstTime = FALSE;
	return;

}





/*==========================================================================*
 * FUNCTION : CT_InitRoughValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:42
 *==========================================================================*/
void CT_InitRoughValue(void)
{
	int			i, j;
	SIG_ENUM		enumValue = GetEnumSigValue(CT_GROUP_EQUIPID, 
						SIG_TYPE_SETTING,
						CG_G_SIG_SET_WALKIN_ENB,
						"CAN_SAMP");
	g_CanData.CanCommInfo.ConvertCommInfo.bNeedRefreshRuntime = TRUE;

	//if(g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue 
	//	== CAN_SAMP_INVALID_VALUE)
	//{
	g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue = 0;
//	}
	g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum = 0;

	for(i = 0; i < MAX_NUM_CONVERT; i++)
	{
		CONVERT_SET_SIG* pConvertSetSig 
			= g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig + i;

		for(j = 0; j < CONVERT_MAX_SIGNALS_NUM; j++)
		{
			g_CanData.aRoughDataConvert[i][j].iValue = CAN_SAMP_INVALID_VALUE;
		}

		g_CanData.CanFlashData.aConverterInfo[i].bExistence = FALSE;

		pConvertSetSig->dwDcOnOff = CT_DC_ON;
		pConvertSetSig->dwResetOnDcOV = CT_RESET_NORMAL;
		pConvertSetSig->dwWalkinEnabled = (0 == enumValue) ? CT_WALKIN_NORMAL : CT_WALKIN_ENB;
		pConvertSetSig->dwFanFullSpeed = CT_FAN_AUTO;		
		pConvertSetSig->dwLedBlink = CT_LED_NORMAL;			

	}

	return;
}

/*==========================================================================*
 * FUNCTION : CT_InitPosition
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:42
 *==========================================================================*/
void CT_InitPosition(void)
{
	int				i;

	for(i = 0; i < MAX_NUM_CONVERT; i++)
	{
		g_CanData.CanFlashData.aConverterInfo[i].iPositionNo
			= CONVERT_POSITION_INVAILID;
	}
	return;
}


/*==========================================================================*
 * FUNCTION : CT_TriggerAllocation
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:42
 *==========================================================================*/
void CT_TriggerAllocation(void)
{
	int		i;
	//Converter re-allocation cmd require below data, 
	//please refer to the protocol
	BYTE	abyDataOfReallcate[CAN_FRAME_DATA_LEN] 
				= {0x04, 0xf0, 0x5a, 0x00, 0x00, 0x00, 0x00, 0x00};

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_CONVERT_CONVERT,
				CAN_ADDR_FOR_BROADCAST, 
				CAN_SELF_ADDR,
				CAN_ERR_CODE,
				CAN_FRAME_DATA_LEN,
				CAN_CMD_TYPE_BROADCAST,
				g_CanData.CanCommInfo.abySendBuf);
	
	//Stuff the rest bits
	for(i = 0; i < CAN_FRAME_DATA_LEN; i++)
	{
		g_CanData.CanCommInfo.abySendBuf[CAN_FRAME_OFFSET_MSG + i] 
			= abyDataOfReallcate[i];
	}
 
	write(g_CanData.CanCommInfo.iCanHandle, 
						(void *)g_CanData.CanCommInfo.abySendBuf,
						CAN_FRAME_LEN);
	
	//TRACE("************CAN DEBUG: Writed a Converter reallocting command!\n");

	g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig = TRUE;
	g_CanData.CanCommInfo.ConvertCommInfo.iConvterTimes = 0;
	//Sleep(12000);			//Waiting 12s
	//CAN_ClearReadBuf();

	return;

}


/*==========================================================================*
 * FUNCTION : CT_RefreshFlashInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:42
 *==========================================================================*/
//static void CT_RefreshFlashInfo(void)
//{
//	int			i, j;
//	int			iConvertNum 
//					= g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum;
//	DWORD		dwSnHigh;
//	DWORD		dwSnLow;
//	int			iPosNo;
//
//	CONVERT_POS_ADDR	ConvertPosAddr[MAX_NUM_CONVERT];
//
//	//Assign position No. and phase No. to converters by flash data
//	for(i = 0; i < iConvertNum; i++)
//	{
//		dwSnHigh 
//			= g_CanData.aRoughDataConvert[i][CONVERT_SERIAL_NO_HIGH].dwValue;
//		dwSnLow 
//			= g_CanData.aRoughDataConvert[i][CONVERT_SERIAL_NO_LOW].dwValue;
//
//		for(j = 0; j < MAX_NUM_CONVERT; j ++)
//		{
//			if((dwSnHigh == g_CanData.CanFlashData.aConverterInfo[j].dwHighSn)
//				&& (dwSnLow == g_CanData.CanFlashData.aConverterInfo[j].dwSerialNo))
//			{
//				g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue
//					= g_CanData.CanFlashData.aConverterInfo[j].iPositionNo;
//				break;
//			}
//		}
//	}
//
//	//Assign position No. to rest converters
//	for(i = 0; i < iConvertNum; i++)
//	{
//		iPosNo = g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue;
//
//		if(iPosNo == CAN_SAMP_INVALID_VALUE)
//		{
//			g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue
//				= GetOneRestConvertPosNo();
//		}
//		
//		ConvertPosAddr[i].iAddr = i;
//		ConvertPosAddr[i].iPositionNo 
//			= g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue;
//
//		g_CanData.aRoughDataConvert[i][CONVERT_SEQ_NO].iValue
//			= CAN_SAMP_INVALID_VALUE;	//be evaluated later
//
//		g_CanData.CanFlashData.aConverterInfo[i].bExistence = TRUE;
//		g_CanData.CanFlashData.aConverterInfo[i].dwAddress = i;
//		g_CanData.CanFlashData.aConverterInfo[i].dwHighSn 
//			= g_CanData.aRoughDataConvert[i][CONVERT_SERIAL_NO_HIGH].dwValue;
//		g_CanData.CanFlashData.aConverterInfo[i].dwSerialNo 
//			= g_CanData.aRoughDataConvert[i][CONVERT_SERIAL_NO_LOW].dwValue;
//		g_CanData.CanFlashData.aConverterInfo[i].iPositionNo
//			= g_CanData.aRoughDataConvert[i][CONVERT_POSITION_NO].iValue;
//		g_CanData.CanFlashData.aConverterInfo[i].iSeqNo 
//			= CAN_SAMP_INVALID_VALUE;	//be evaluated later
//	}
//
//    //effervescing sort
//	if(iConvertNum)
//	{
//		for (i = 0; i < (iConvertNum - 1); i++)
//		{
//			for (j = 0; j < (iConvertNum - 1 - i); j++)
//			{
//				if(ConvertPosAddr[j].iPositionNo > ConvertPosAddr[j + 1].iPositionNo)
//				{
//					CONVERT_POS_ADDR		PosAddr;
//
//					PosAddr.iAddr = ConvertPosAddr[j + 1].iAddr;
//					PosAddr.iPositionNo = ConvertPosAddr[j + 1].iPositionNo;
//
//					ConvertPosAddr[j + 1].iPositionNo = ConvertPosAddr[j].iPositionNo;
//					ConvertPosAddr[j + 1].iAddr = ConvertPosAddr[j].iAddr;
//
//					ConvertPosAddr[j].iPositionNo = PosAddr.iPositionNo;
//					ConvertPosAddr[j].iAddr = PosAddr.iAddr;
//				}
//			}
//		}
//	}
//
//	//Assign sequence No. to every converter
//	for(i = 0; i < iConvertNum; i++)
//	{
//		int iAddr = ConvertPosAddr[i].iAddr;
//
//		g_CanData.aRoughDataConvert[iAddr][CONVERT_SEQ_NO].iValue = i;
//		g_CanData.CanFlashData.aConverterInfo[iAddr].iSeqNo = i;
//		if(i < 48)
//		{
//		
//			SetDwordSigValue(CT_START_EQUIP_ID + i,
//							SIG_TYPE_SETTING,
//							CT_POS_SET_SIG_ID,
//							g_CanData.aRoughDataConvert[iAddr][CONVERT_POSITION_NO].iValue,
//							"CAN_SAMP");
//		}
//		else
//		{
//			SetDwordSigValue(CT_START_EQUIP_ID_EXTEND + i - 48,
//						SIG_TYPE_SETTING,
//						CT_POS_SET_SIG_ID,
//						g_CanData.aRoughDataConvert[iAddr][CONVERT_POSITION_NO].iValue,
//						"CAN_SAMP");
//		}
//
//	}
//
//
//	CAN_WriteFlashData(&(g_CanData.CanFlashData));
//
//	return;
//}



/*==========================================================================*
 * FUNCTION : PackAndSendConvertCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: UINT   uiMsgType    : 
 *            UINT   uiDestAddr   : 
 *            UINT   uiCmdType    : 
 *            UINT   uiValueTypeH : 
 *            UINT   uiValueTypeL : 
 *            float  fParam       : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
 *==========================================================================*/
static void PackAndSendConvertCmd(UINT uiMsgType,
								  UINT uiDestAddr,
								  UINT uiCmdType,
								  UINT uiValueTypeH,
								  UINT uiValueTypeL,
								  BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	int		i;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_CONVERT_CONTROLLER,
				uiDestAddr, 
				CAN_SELF_ADDR,
				CAN_ERR_CODE,
				CAN_FRAME_DATA_LEN,
				uiCmdType,
				pbySendBuf);
	
	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	/*CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);*/	
	for(i = 0; i < 4; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_VALUE + i] = *(pbyValue + i);
	}
 
	write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);

	return;
}



#define CT_DC_ON_OFF_MASK				(0x80)
#define	CT_OVER_VOLT_RESET_MASK			(0x40)
#define	CT_WALK_IN_MASK					(0x20)
#define	CT_FAN_FULL_SPEED_MASK			(0x10)
#define	CT_LED_BLINK_MASK				(0x08)

/*==========================================================================*
 * FUNCTION : GetCtSetByteCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static BYTE : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:42
 *==========================================================================*/
static BYTE GetCtSetByteCmd00(int iAddr)
{
	BYTE			byOutput = 0;

	CONVERT_SET_SIG*	pSetSig 
				= &(g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[iAddr]);

	if(pSetSig->dwDcOnOff != CT_DC_ON)
	{		
		byOutput |= CT_DC_ON_OFF_MASK;	
	}
	else
	{
		byOutput &= ~CT_DC_ON_OFF_MASK;	
	}

	if(pSetSig->dwResetOnDcOV != CT_RESET_NORMAL)  //Over Voltage Reset
	{
		byOutput |= CT_OVER_VOLT_RESET_MASK;	
	}	
	else
	{
		byOutput &= ~CT_OVER_VOLT_RESET_MASK;
	}

	if(pSetSig->dwWalkinEnabled != CT_WALKIN_NORMAL)    //Converter Walkin Enable
	{
		byOutput |= CT_WALK_IN_MASK;
	}
	else
	{
		byOutput &= ~CT_WALK_IN_MASK;
	}

	if(pSetSig->dwFanFullSpeed != CT_FAN_AUTO) //Fan is full speed or not
	{
		byOutput |= CT_FAN_FULL_SPEED_MASK;
	}
	else
	{
		byOutput &= ~CT_FAN_FULL_SPEED_MASK;
	}
	
	if(pSetSig->dwLedBlink != CT_LED_NORMAL)
	{
		byOutput |= CT_LED_BLINK_MASK;
	}
	else
	{
		byOutput &= ~CT_LED_BLINK_MASK;
	}

	return byOutput;	
}


#define CONVERT_NOUSED				-1

/*==========================================================================*
 * FUNCTION : UnpackCtAlarm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr      : 
 *            BYTE*  pbyAlmBuff : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
 *==========================================================================*/
static void UnpackCtAlarm(int iAddr, BYTE* pbyAlmBuff)
{
	//The order is based on the protocol, do not change it.
	int		aiSigIdx[BYTES_OF_ALM_INFO * BITS_PER_BYTE] = 
	{
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_ID_OVERLAP_ST,
		CONVERT_IDENTIFYING_ST,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_FAN_FULL_SPEED_ST,

		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_EEPROM_FAULT_ST,
		CONVERT_THERMAL_SHUTDOWN_ST,
		CONVERT_INPUT_LOW_VOLTAGE_ST,
		CONVERT_HI_AMBIENT_TEMP_ST,

		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_WALK_IN_ENB_ST,
		CONVERT_ON_OFF_ST,

		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_NOUSED,
		CONVERT_STOP_ST,
		CONVERT_POWER_LMT_BY_TEMP_ST,
		CONVERT_OVER_VOLTAGE_ST,
		CONVERT_FAN_FAILURE_ST,
		CONVERT_CONVERT_FAILURE_ST,

	};

	int		i, j;

	for(i = 0; i < BYTES_OF_ALM_INFO; i++)
	{
		BYTE	byMask = 0x80;
		BYTE	byRough = *(pbyAlmBuff + i);

		for(j = 0; j < BITS_PER_BYTE; j++)
		{
			if(aiSigIdx[i * BITS_PER_BYTE + j] != CONVERT_NOUSED)
			{
				int		iIdx = aiSigIdx[i * BITS_PER_BYTE + j];
				g_CanData.aRoughDataConvert[iAddr][iIdx].iValue
					= (byRough & byMask) ? 1 : 0;	
			}
			byMask >>= 1;
		}
	}
	
	return;
}


static void CtUnpackBarCode(int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf,
						int iBarcodeNo)
{
	int			i;
	UINT		uiBarcode;
	
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_CONVERT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
				!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
				!= 0))
		{
			continue;
		}
		
		if(*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L) 
			== CT_VAL_TYPE_R_BARCODE1 + iBarcodeNo)
		{
			uiBarcode = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE1 + iBarcodeNo].uiValue 
					= uiBarcode;
			break;
		}
	}
	return;
}

static void CtUnpackBarCode_Sp(int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf,
						int iBarcodeNo)
{
	int			i;
	UINT		uiBarcode;	
	
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_CONVERT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
				!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
				!= 0))
		{
			continue;
		}
		
		if(*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L) 
			== CG_VAL_TYPE_R_NEWFEATURE)
		{
			uiBarcode = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataConvert[iAddr][CONVERT_NEWFEATURE].uiValue 
					= uiBarcode;			
			break;
		}
	}
	return;
}

enum	CT_CMD00_FRAME_ORDER
{
	CT_CMD00_ORDER_DC_VOLT = 0,
	CT_CMD00_ORDER_REAL_CURR,
	CT_CMD00_ORDER_TEMPERATURE,
	CT_CMD00_ORDER_ALARM_BITS,
	CT_CMD00_ORDER_HIGH_SN,
	CT_CMD00_ORDER_LOW_SN,
	CT_CMD00_ORDER_RUN_TIME,

	CT_CMD00_ORDER_FRAME_NUM,
};
/*==========================================================================*
 * FUNCTION : CtUnpackCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr     : 
 *            int    iReadLen  : 
 *            BYTE*  pbyRcvBuf : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
 *==========================================================================*/
static BOOL CtUnpackCmd00(int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i;
	UINT		uiSerialNo, uiSerialNoLow;
	int			iFrameOrder = CT_CMD00_ORDER_DC_VOLT;
	
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_CONVERT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
				!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
				!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case CT_VAL_TYPE_R_DC_VOLT:
			g_CanData.aRoughDataConvert[iAddr][CONVERT_DC_VOLTAGE].fValue 
					= CAN_StringToFloat(pValueBuf);
			if(CT_CMD00_ORDER_DC_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		case CT_VAL_TYPE_R_REAL_CURR:

			
			g_CanData.aRoughDataConvert[iAddr][CONVERT_CURRENT].fValue 
					= CAN_StringToFloat(pValueBuf);
//Convter send the (current/rated currnt) to controller, so ACU+ need calculate it.
			g_CanData.aRoughDataConvert[iAddr][CONVERT_CURRENT].fValue 
					= g_CanData.aRoughDataConvert[iAddr][CONVERT_CURRENT].fValue 
						* g_CanData.aRoughDataConvert[iAddr][CONVERT_OUTPUT_RATED_CURRENT].fValue;

			if(CT_CMD00_ORDER_REAL_CURR == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case CT_VAL_TYPE_R_TEMPERATURE:
			g_CanData.aRoughDataConvert[iAddr][CONVERT_TEMPERATURE].fValue 
					= CAN_StringToFloat(pValueBuf);
			if(CT_CMD00_ORDER_TEMPERATURE == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case CT_VAL_TYPE_R_ALARM_BITS:
			UnpackCtAlarm(iAddr, pValueBuf);
			if(CT_CMD00_ORDER_ALARM_BITS == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case CT_VAL_TYPE_R_HIGH_SN:
			uiSerialNo = CAN_StringToUint(pValueBuf);
			uiSerialNoLow = g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_LOW].uiValue;
			g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_HIGH].uiValue 
					= uiSerialNo;
			
			g_CanData.aRoughDataConvert[iAddr][CONV_HIGH_SN_REPORT].uiValue
				= ((uiSerialNo & 0x0000000f) << 2) + (uiSerialNoLow >> 30);
		
			/*printf("CT_SAMPLER: uiSerialNo = %d, CONV_HIGH_SN_REPORT = %d,\n",
				uiSerialNo,
				g_CanData.aRoughDataConvert[iAddr][CONV_HIGH_SN_REPORT].uiValue);*/

			if(CT_CMD00_ORDER_HIGH_SN == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		case CT_VAL_TYPE_R_LOW_SN:
			uiSerialNo = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_LOW].uiValue 
					= uiSerialNo;
			TRACE("CT_SAMPLER: uiSerialNo = %d, CONVERT_SERIAL_NO_LOW = %d,\n",
				uiSerialNo,
				g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_LOW].uiValue);
			
			if(CT_CMD00_ORDER_LOW_SN == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case CT_VAL_TYPE_R_RUN_TIME:
			g_CanData.aRoughDataConvert[iAddr][CONVERT_TOTAL_RUN_TIME].uiValue 
					= (UINT)(CAN_StringToFloat(pValueBuf));
			if(CT_CMD00_ORDER_RUN_TIME == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		default:
			break;
		}
	}

	if(CT_CMD00_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


/*==========================================================================*
 * FUNCTION : ClrCtIntrruptTimes
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
 *==========================================================================*/
static void ClrCtIntrruptTimes(int iAddr)
{
	g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_TIMES].iValue = 0;
	g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_ST].iValue 
			= CT_COMM_NORMAL_ST;

	return;
}



/*==========================================================================*
 * FUNCTION : IncCtIntrruptTimes
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
 *==========================================================================*/
static void IncCtIntrruptTimes(int iAddr)
{
	if(g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_TIMES].iValue 
		< (CT_MAX_INTERRUPT_TIMES + 2))
	{
		g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_TIMES].iValue++;
	}

	if(g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_TIMES].iValue
		>= (CT_MAX_INTERRUPT_TIMES + 1))
	{
		if(g_CanData.aRoughDataGroup[GROUP_CT_ALL_NO_RESPONSE].iValue)
		{
			g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_ST].iValue 
				= CT_COMM_ALL_INTERRUPT_ST;
		}
		else
		{
			g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_ST].iValue 
				= CT_COMM_INTERRUPT_ST;
		}
	}
	else
	{
		g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_ST].iValue 
			= CT_COMM_NORMAL_ST;
	}

	return;
}

/*==========================================================================*
* FUNCTION : GetCtAddrBySeqNo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iSeqNo : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-20 17:40
*==========================================================================*/
static int GetCtAddrBySeqNo(int iSeqNo)
{
	int		i;
	int		iNum = g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum;

	for(i = 0; i < iNum; i++)
	{
		if(g_CanData.aRoughDataConvert[i][CONVERT_SEQ_NO].iValue == iSeqNo)
		{
			return i;
		}
	}

	return 0;
}


/*==========================================================================*
 * FUNCTION : GetConvertAddrByChnNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iChannelNo : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
 *==========================================================================*/
static int GetConvertAddrByChnNo(int iChannelNo)
{
	int i;
	
	int iSeqNo = (iChannelNo - CNMR_MAX_CT_BROADCAST_CMD_CHN - 1) 
						/ MAX_CHN_NUM_PER_CT;
	//ASSERT(iSeqNo >= 0 && iSeqNo < MAX_NUM_CONVERT);

	for(i = 0; i < g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum; i++)
	{
		if(iSeqNo == g_CanData.aRoughDataConvert[i][CONVERT_SEQ_NO].iValue)
		{
			return i;
		}
	}

	return 0;
}


float CtGetCommOKCtNum()
{
	int		i;
	int		iAddr;
	float	fCommOkCtNum = 0.1;//��Ϊ���ص���ֵ��Ҫ�����������Բ��ܸ��0

	if (g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue 
			!= CAN_SAMP_INVALID_VALUE)
	{
		for (i = 0; i < g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue; i++)
		{
			iAddr = GetCtAddrBySeqNo(i);
			if (CT_COMM_NORMAL_ST == g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_ST].iValue)
			{
				fCommOkCtNum++;
			}
			else
			{
				//jump it!
			}
		}
	}
	else
	{
		fCommOkCtNum = 0.1;
	}
	
	return fCommOkCtNum;
}
/*==========================================================================*
 * FUNCTION : CT_SendCtlCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iChannelNo : 
 *            float  fParam     : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
 *==========================================================================*/
void CT_SendCtlCmd(int iChannelNo, float fParam)
{
	int			i;
	BYTE			abyVal[4];
	int			iCommOKCtNum;
	float			fCurrLimit = 0.0;
	float			fRatedCurrent = 0.0;
	//ASSERT((iChannelNo > CNRG_MAX_CTL_CHN) && (iChannelNo <= CNCG_MAX_CTL_CHN));
	if(iChannelNo < CNMR_MAX_CT_BROADCAST_CMD_CHN)
	{
		switch(iChannelNo)
		{
		case CNCG_CURRENT_LMT:

			fCurrLimit = fParam;
			//if(g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue 
			//							!= CAN_SAMP_INVALID_VALUE)
			//{

			//	/*
			//		MSmith (24May2011): NA customers require current limit adjustment for DC outputs. 
			//		The customer desires to set current limit in amps, 
			//		so that is what should be shown on display and web page for adjustment setting. 
			//		The signal going to the converter must be in %, 
			//		so the ACU+ should convert the customer setting from amps to % to send signal to converters in %, 
			//		where 100% equals 27 amps. Example, 
			//		system has three converters installed so 100% is 27A x 3 = 81A. 
			//		Customer sets current limit to 60 amps, 
			//		so controller send signal to converters to current limit at 60 / 81 = 74.1% (or 20 amps each). 
			//	*/
			//	iCommOKCtNum = (int)CtGetCommOKCtNum();
			//	if (0 == iCommOKCtNum)
			//	{
			//		fCurrLimit = 100;
			//	}
			//	else
			//	{
			//		fCurrLimit = (float)fParam / (float)(27 * iCommOKCtNum);
			//	}
			//}
			//else
			//{
			//	fCurrLimit = 100;//�ݴ���������������·�100
			//}
			//27ʱ��100%!  ���÷�Χ��13.5---31.3
			//fRatedCurrent = g_CanData.aRoughDataGroup[GROUP_CT_OUTPUT_RATED_CURRENT].fValue;
			//if(fRatedCurrent > 1.0)
			//{
				//CAN_FloatToString(fParam / fRatedCurrent, abyVal);
				CAN_FloatToString(fCurrLimit/100, abyVal);
			//}
			//else
			//{
			//	CAN_FloatToString(fParam / 27, abyVal);
			//}

			PackAndSendConvertCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							CT_VAL_TYPE_W_CURR_LMT,
							abyVal);
			break;
		
		case CNCG_OUTPUT_VOLT:
			CAN_FloatToString(fParam, abyVal);
			PackAndSendConvertCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							CT_VAL_TYPE_W_DC_VOLT,
							abyVal);
			break;

		case CNCG_OVER_VOLT_POINT://HVSD!!
			CAN_FloatToString(fParam, abyVal);
			PackAndSendConvertCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							CT_VAL_TYPE_W_HIGH_VOLT,
							abyVal);
			break;

		case CNCG_ESTOP_FUNCTION:
			if(FLOAT_EQUAL0(fParam))
			{
				abyVal[0] = 0xff;
				abyVal[1] = 0xff;
				abyVal[2] = 0;
				abyVal[3] = 0;
			}
			else
			{
				abyVal[0] = 0;
				abyVal[1] = 0;
				abyVal[2] = 0x5a;
				abyVal[3] = 0x5a;
			}

			PackAndSendConvertCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							CT_VAL_TYPE_W_ESTOP_ENB,
							abyVal);
			break;

		case CNCG_WALKIN_ENABLED:
			for(i = 0; i < MAX_NUM_CONVERT; i++)
			{
				g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[i].dwWalkinEnabled
					= FLOAT_EQUAL0(fParam)? CT_WALKIN_NORMAL : CT_WALKIN_ENB;
			}
			break;

		case CNCG_FAN_FULL_SPEED:
			for(i = 0; i < MAX_NUM_CONVERT; i++)
			{
				g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[i].dwFanFullSpeed
					= FLOAT_EQUAL0(fParam)? CT_FAN_AUTO : CT_FAN_FULL_SPEED;
			}
			break;

		case CNCG_ALL_LED_BLINK:
			for(i = 0; i < MAX_NUM_CONVERT; i++)
			{
				g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[i].dwLedBlink
					= FLOAT_EQUAL0(fParam)? CT_LED_NORMAL : CT_LED_BLINK;
			}
			break;

		case CNCG_DC_ON_OFF:
			for(i = 0; i < MAX_NUM_CONVERT; i++)
			{
				g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[i].dwDcOnOff
					= FLOAT_EQUAL0(fParam)? CT_DC_ON : CT_DC_OFF;
			}
			break;

		case CNCG_CONV_REALLOCATE:
			CT_TriggerAllocation();
			break;
		case CNCG_CONV_CONFIRMALLCOMMFAILURE:
			if(g_CanData.aRoughDataGroup[GROUP_CT_ALL_NO_RESPONSE].iValue)
			{		
				CT_InitRoughValue();	
				g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue = 0;
				g_CanData.aRoughDataGroup[GROUP_CT_EXIST_STAT].iValue = 1;
				g_CanData.aRoughDataGroup[GROUP_CT_ALL_NO_RESPONSE].iValue = FALSE;
			}
			break;
		case CNCG_CONV_CLEAR_IDS:
			ClrConvFalshInfo();
			break;
		default:

			CAN_LOG_OUT(APP_LOG_WARNING, "Invalid control channel!\n");
			break;
		}
		return;
	}
	else
	{
		int		iConvertAddr = GetConvertAddrByChnNo(iChannelNo);
		int		iSubChnNo = ((iChannelNo - CNMR_MAX_CT_BROADCAST_CMD_CHN - 1)
							% MAX_CHN_NUM_PER_CT) 
							+ CNMR_MAX_CT_BROADCAST_CMD_CHN 
							+ 1;
		switch(iSubChnNo)
		{
		case CNCS_ON_OFF:
			g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[iConvertAddr].dwDcOnOff
					= FLOAT_EQUAL0(fParam)? CT_DC_ON : CT_DC_OFF;
			break;

		case CNCS_OVER_VOLT_RESET:
			g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[iConvertAddr].dwResetOnDcOV 
					= FLOAT_EQUAL0(fParam)? CT_RESET_NORMAL : CT_RESET_ENB;
			break;

		case CNCS_LED_BLINK:
			g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[iConvertAddr].dwLedBlink 
					= FLOAT_EQUAL0(fParam)? CT_LED_NORMAL : CT_LED_BLINK;
			break;

		/*case CNCS_WALK_IN:
			g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[iConvertAddr].dwWalkinEnabled 
					= FLOAT_EQUAL0(fParam)? CT_WALKIN_NORMAL : CT_WALKIN_ENB;
			break;

		case CNCS_FAN_FULL_SPEED:
			g_CanData.CanCommInfo.ConvertCommInfo.aConvertSetSig[iConvertAddr].dwFanFullSpeed
					= FLOAT_EQUAL0(fParam)? CT_FAN_AUTO : CT_FAN_FULL_SPEED;
			break;*/
		
		default:

			CAN_LOG_OUT(APP_LOG_WARNING, "Invalid control channel!\n");
			break;
		}
	}
	return;

}






#define CT_CH_END_FLAG			-1
/*==========================================================================*
 * FUNCTION : CT_StuffChannel
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ENUMSIGNALPROC  EnumProc : 
 *            LPVOID          lpvoid   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:40
 *==========================================================================*/
void CT_StuffChannel(ENUMSIGNALPROC EnumProc,
					 LPVOID lpvoid)	
{
	int		i, j;
	int		iConvertNum = g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue;
	SAMPLING_VALUE strValue;

	CHANNEL_TO_ROUGH_DATA		CtGroupSig[] =
	{
		{CT_CH_CONVERT_NUM,		GROUP_CT_ACTUAL_NUM,		},
		{CT_CH_CONVERT_EXIST,	GROUP_CT_EXIST_STAT,		},//2010/1/16

		{CT_CH_CONVERT_COMM_STATUS,	GROUP_CT_ALL_NO_RESPONSE},

		{CT_CH_G_INPUT_RATED_VOLTAGE,   GROUP_CT_INPUT_RATED_VOLTAGE	},
		{CT_CH_G_OUTPUT_RATED_VOLTAGE,  GROUP_CT_OUTPUT_RATED_VOLTAGE	},
		{CT_CH_G_OUTPUT_RATED_CURRENT,	GROUP_CT_OUTPUT_RATED_CURRENT	},
		{CT_CH_G_CONVERTER_TYPE,	GROUP_CT_TYPE	},
		

		{CT_CH_END_FLAG,		CT_CH_END_FLAG,				},
	};

	CHANNEL_TO_ROUGH_DATA		CtSig[] =
	{
		{CT_CH_DC_VOLT,				CONVERT_DC_VOLTAGE,				},
		{CT_CH_ACTUAL_CURR,			CONVERT_CURRENT,				},	
		{CT_CH_TEMPERATURE,			CONVERT_TEMPERATURE,			},
		{CT_CH_HIGH_SERIAL_NO,		CONV_HIGH_SN_REPORT,			},
		{CT_CH_LOW_SERIAL_NO,		CONVERT_SERIAL_NO_LOW,			},
		{CT_CH_RUN_TIME,			CONVERT_TOTAL_RUN_TIME,			},
		{CT_CH_ID_OVERLAP_ST,		CONVERT_ID_OVERLAP_ST,			},
		{CT_CH_IDENTIFYING_ST,		CONVERT_IDENTIFYING_ST,			},
		{CT_CH_FAN_FULL_SPEED_ST,	CONVERT_FAN_FULL_SPEED_ST,		},
		{CT_CH_EEPROM_FAULT_ST,		CONVERT_EEPROM_FAULT_ST,		},
		{CT_CH_THERMAL_SHUTDOWN_ST,	CONVERT_THERMAL_SHUTDOWN_ST,		},
		{CT_CH_INPUT_LOW_VOLT_ST,	CONVERT_INPUT_LOW_VOLTAGE_ST,		},
		{CT_CH_HI_AMB_TEMP_ST,		CONVERT_HI_AMBIENT_TEMP_ST,		},
		{CT_CH_WALKIN_ENB_ST,		CONVERT_WALK_IN_ENB_ST,			},
		{CT_CH_ON_OFF_ST,		CONVERT_ON_OFF_ST,			},
		{CT_CH_STOP_ST,			CONVERT_STOP_ST,			},
		{CT_CH_POWER_LMT_TEMP_ST,	CONVERT_POWER_LMT_BY_TEMP_ST,		},
		{CT_CH_OVER_VOLT_ST,		CONVERT_OVER_VOLTAGE_ST,		},
		{CT_CH_FAN_FAIL_ST,		CONVERT_FAN_FAILURE_ST,			},
		{CT_CH_CONVERT_FAIL_ST,		CONVERT_CONVERT_FAILURE_ST,		},
		{CT_CH_BARCODE1,		CONVERT_BARCODE1,			},
		{CT_CH_BARCODE2,		CONVERT_BARCODE2,			},
		{CT_CH_BARCODE3,		CONVERT_BARCODE3,			},
		{CT_CH_BARCODE4,		CONVERT_BARCODE4,			},
		{CT_CH_INTERRUPT_STATE,		CONVERT_INTERRUPT_ST,			},
		{CT_CH_POSITION_NO,		CONVERT_POSITION_NO,			},

		{CT_CH_INPUT_RATED_VOLTAGE,	CONVERT_INPUT_RATED_VOLTAGE,		},
		{CT_CH_OUTPUT_RATED_VOLTAGE,	CONVERT_OUTPUT_RATED_VOLTAGE,		},
		{CT_CH_OUTPUT_RATED_CURRENT,	CONVERT_OUTPUT_RATED_CURRENT,		},
		{CT_CH_EFFECIEY,		CONVERT_EFFECIEY,			},	
											
		{CT_CH_END_FLAG,		CT_CH_END_FLAG,				},
	};

	i = 0;
	while(CtGroupSig[i].iChannel != CT_CH_END_FLAG)
	{
		EnumProc(CtGroupSig[i].iChannel, 
				g_CanData.aRoughDataGroup[CtGroupSig[i].iRoughData].fValue, 
				lpvoid );
		i++;
	}

	strValue.iValue = CT_SIG_EXISTANCE;
	//Stuff the channels by the order of position NO.
	for(i = 0; i < iConvertNum; i++)
	{
		int		iAddr = GetCtAddrBySeqNo(i);

		j = 0;
		while(CtSig[j].iChannel != CT_CH_END_FLAG)
		{
			EnumProc(i * MAX_CHN_NUM_PER_CT + CtSig[j].iChannel, 
				g_CanData.aRoughDataConvert[iAddr][CtSig[j].iRoughData].fValue, 
				lpvoid );
			j++;
		}	

		EnumProc(i * MAX_CHN_NUM_PER_CT + CT_CH_EXISTENCE, 
							strValue.fValue, 
							lpvoid );
		
	}

	strValue.iValue = CT_SIG_INEXISTANCE;
	for (i = iConvertNum ; i < MAX_NUM_CONVERT; i++)
	{

		EnumProc(i * MAX_CHN_NUM_PER_CT + CT_CH_EXISTENCE, 
				strValue.fValue,
				lpvoid );

	}

	return;
}



/*==========================================================================*
 * FUNCTION : CtIsAllNoResponse
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:40
 *==========================================================================*/
static BOOL CtIsAllNoResponse(void)
{
	int		i;
	int		iConvertNum = g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum;
	BOOL	bRst = TRUE;

	if(!iConvertNum)
	{
		return TRUE;
	}
	
	for(i = 0; i < iConvertNum; i++)
	{
		if(g_CanData.aRoughDataConvert[i][CONVERT_INTERRUPT_TIMES].iValue 
			< CT_MAX_INTERRUPT_TIMES)
		{
			bRst = FALSE;
			break;
		}
	}

	if(bRst)
	{
		for(i = 0; i < iConvertNum; i++)
		{
			g_CanData.aRoughDataConvert[i][CONVERT_INTERRUPT_ST].iValue 
				 = CT_COMM_ALL_INTERRUPT_ST;
		}	
	}	
	

	return bRst;
}
/*static BOOL BarCodeDataIsValid(BYTE byData)
{
	if ((0x20 != byData) && (0xff != byData))
	{
		return TRUE;
	}

	return FALSE;
}*/

VOID Convert_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	INT32	iTemp;
	INT32	i,iAddr;
	BYTE	byTempBuf[4];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *				pInfo;
	pInfo						= (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");
	int kk;
	for(iAddr = 0; iAddr < MAX_NUM_CONVERT; iAddr++)
	{
		if ((CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_LOW].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataConvert[iAddr][CONVERT_VERSION].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE1].iValue))
		{
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			
			continue;
		}
		else
		{
			pInfo->bSigModelUsed = TRUE;
		}

		//printf("\n	######## IN Convert addr=%d SERIAL_NO_LOW=%2x VERSION=%2x CODE1=%2x CODE2=%2x CODE3=%2x CODE4=%2x \n",
		//	iAddr,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_LOW].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_VERSION].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE1].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE2].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE3].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE4].uiValue);

		//1.������ ��ʹ��
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		kk = GetCtAddrBySeqNo(iAddr);
		//2.���ŵ�λ	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_SERIAL_NO_LOW].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_SERIAL_NO_LOW].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_SERIAL_NO_LOW].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_SERIAL_NO_LOW].uiValue);

		//��
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//��
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//���ŵ�λ�ĵ�16�ֽ� ��Ӧ	##### ��������������
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		
		
		//3.�汾��VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_VERSION].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_VERSION].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_VERSION].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_VERSION].uiValue);

		//ģ��汾��
		//H		A00��ʽ�����ֽڰ�A=0 B=1���ƣ�����A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00����ʵ����ֵ������ֽ�
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//����汾		��������汾Ϊ1.30,��������Ϊ�����ֽ�������130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end
		
		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE1].uiValue);

		//FF ��ASCII���ʾ,ռ��CODE1 �� CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T ��ʾ��Ʒ����,ASCIIռ��CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13
		
		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		
		
		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}
		
		
		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataConvert[kk][CONVERT_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nCovert%d bSigModelUsed:%d\n",iAddr, pInfo->bSigModelUsed);
		//printf("\nCovert PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
		//printf("\nCnCovert	version:%s\n", pInfo->szSWVersion); //1.20
		//printf("\nnCovert version:%s\n", pInfo->szHWVersion); //A00
		//printf("\nnCovert Serial Number:%s\n", pInfo->szSerialNumber);//03100500006

		pInfo++;
	}
	//printf("\n	########		END		Convert_	GetProdctInfo	########	\n");
	
}
/*==========================================================================*
 * FUNCTION : CtSampleBarcode
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-03-04 15:57
 *==========================================================================*/
static SIG_ENUM CtSampleBarcode(int iAddr)
{
	int				i;
	int				iReadLen1st = 0;
	int				iReadLen2nd = 0;
	FLOAT_STRING	unVal;

	unVal.ulValue = 0;

	//2011/2/18
	for(i = 0; i < 6; i++)	//4 barcode + Feature + version
	{
		PackAndSendConvertCmd(MSG_TYPE_RQST_BYTE,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						0,
						CT_VAL_TYPE_R_BARCODE1 + i,
						unVal.abyValue);
		//In most situations, I guess converter responds in 15s
		Sleep(15);
		if(CAN_CONVERT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
									g_CanData.CanCommInfo.abyRcvBuf,
									&iReadLen1st))
		{
			return CAN_CONVERT_REALLOCATE;
		}
		else
		{
			if(iReadLen1st < (CT_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
			{
				//After 15s+55s, if no respond, interrup times increase.
				Sleep(50);
				if(CAN_CONVERT_REALLOCATE 
					== CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
									g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
									&iReadLen2nd))

				{
					return CAN_CONVERT_REALLOCATE;
				}
			}
		}

		//if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		//	>= CT_CMD00_RCV_FRAMES_NUM)
		if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
			>= 1)
		{
			CtUnpackBarCode(iAddr,
					iReadLen1st + iReadLen2nd,
					g_CanData.CanCommInfo.abyRcvBuf,
					i);
		}
	}


	PackAndSendConvertCmd(MSG_TYPE_RQST_BYTE,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					0,
					CG_VAL_TYPE_R_NEWFEATURE,
					unVal.abyValue);
	//In most situations, I guess converter responds in 15s
	Sleep(15);
	if(CAN_CONVERT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
								g_CanData.CanCommInfo.abyRcvBuf,
								&iReadLen1st))
	{
		return CAN_CONVERT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < CAN_FRAME_LEN)
		{
			//After 15s+55s, if no respond, interrup times increase.
			Sleep(85);
			if(CAN_CONVERT_REALLOCATE 
				== CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
								g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
								&iReadLen2nd))

			{
				return CAN_CONVERT_REALLOCATE;
			}
		}
	}

	//if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
	//	>= CT_CMD00_RCV_FRAMES_NUM)
	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= 1)
	{
		CtUnpackBarCode_Sp(iAddr,
				iReadLen1st + iReadLen2nd,
				g_CanData.CanCommInfo.abyRcvBuf,
				7);
	}

	return CAN_SAMPLE_OK;
}



/*==========================================================================*
 * FUNCTION : CtSampleCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:42
 *==========================================================================*/
static SIG_ENUM CtSampleCmd00(int iAddr)
{
	int				iReadLen1st = 0;
	int				iReadLen2nd = 0;
	FLOAT_STRING	unVal;

	unVal.ulValue = 0;

	PackAndSendConvertCmd(MSG_TYPE_RQST_DATA00,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					(UINT)GetCtSetByteCmd00(iAddr),
					CT_VAL_TYPE_NO_TRIM_VOLT,
					unVal.abyValue);
	//In most situations, I guess converter responds in 15s
	Sleep(CT_CMD00_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);
	if(CAN_CONVERT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
								g_CanData.CanCommInfo.abyRcvBuf,
								&iReadLen1st))
	{
		return CAN_CONVERT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (CT_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			//After 15s+55s, if no respond, interrup times increase.
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if(CAN_CONVERT_REALLOCATE 
				== CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
								g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
								&iReadLen2nd))

			{
				return CAN_CONVERT_REALLOCATE;
			}
		}
	}

    if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= CT_CMD00_RCV_FRAMES_NUM)
	{
		if(CtUnpackCmd00(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrCtIntrruptTimes(iAddr);
		}
		else
		{
			IncCtIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		IncCtIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}


/**************************************************************
2010/12/02���޸�
����������ǣ����Ƚ���RCT����CONVERT���У��ٽ���CONVERT�����
�£������CONVERTɨ�費��������
ԭ��Ϊ������һ�ϵ��ɨ�����У����ǵ�ɨ��
**************************************************************/
BOOL	CtCheckConvertExist()
{
	int			iAddr = 0;
	SIG_ENUM	emRst = 10;//Avoid the c++ test issue, to init it to 10, nonsense.

	emRst = CtSampleCmd00(iAddr);

	if(CAN_CONVERT_REALLOCATE == emRst)
	{
		//printf("\n	CtCheckConvertExist	if(CAN_CONVERT_REALLOCATE == emRst)	\n");
		return FALSE;
	}
	else if(CAN_SAMPLE_FAIL == emRst)
	{
		//printf("\n	CtCheckConvertExist if(CAN_SAMPLE_FAIL == emRst)	\n");
		return FALSE;
	}
	else if (CAN_SAMPLE_OK == emRst)
	{
		//printf("\n	CtCheckConvertExist	if (CAN_SAMPLE_OK == emRst)	\n");
		return TRUE;
	}
	else
	{
		//printf("\n	CtCheckConvertExist	else	\n");
		return FALSE;
	}
}





/*==========================================================================*
 * FUNCTION : CT_Reconfig
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:42
 *==========================================================================*/
void CT_Reconfig(void)
{
	int			iAddr;
	SIG_ENUM	emRst;
	int		iAddrAgain;

	CT_InitRoughValue();

	for(iAddr = 0; iAddr < MAX_NUM_CONVERT; iAddr++)
	{
		emRst = CtSampleCmd00(iAddr);
		//printf("Conv%d send!\n",iAddr);

		if(CAN_CONVERT_REALLOCATE == emRst)
		{
			return;
		}
		else if(CAN_SAMPLE_FAIL == emRst)
		{
			break;
		}
		g_CanData.CanFlashData.aConverterInfo[iAddr].bExistence = TRUE;

		emRst = CtSampleBarcode(iAddr);

		if(CAN_CONVERT_REALLOCATE == emRst)
		{
			return;
		}
		else if(CAN_SAMPLE_OK == emRst)
		{
			if(!CtAnalyseSerialNo(iAddr))
			{
				CtAnalyseFeature(iAddr);
			}			
		}		
	}

	//������Ź����У��м�ģ�鱻�ε������
	for (iAddrAgain = iAddr + 1; iAddrAgain <= iAddr + 3; iAddrAgain++)
	{
		emRst = CtSampleCmd00(iAddr);

		if(CAN_SAMPLE_OK == emRst)
		{
			CT_TriggerAllocation();
			return;
		}

		if(CAN_RECT_REALLOCATE == emRst)
		{
			//��������������ǲ���Ҫ�ģ���������˵�������⡣
			//Delete it , will go into the dead cycle.by YangGuoxin 2014/3/14
			return;
		}	
	}


	g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum = iAddr;

	if(iAddr)
	{
		g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue = iAddr;
		g_CanData.aRoughDataGroup[GROUP_CT_EXIST_STAT].iValue = FALSE;//exist
		//CT_RefreshFlashInfo();
	}
	
	//If just clear converter communicate failure, just keep the converter group
	//if (0 == g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue)
	//{
	//	g_CanData.aRoughDataGroup[GROUP_CT_EXIST_STAT].iValue = TRUE;//inexistence
	//}
		

	return;
}

//֮ǰ�Ĳ������ϵ��һ�����кʹ��ڵ�CONVERT��ֻҪ��һ����DXIGET�Ĳ�һ�¾͹㲥����һ��
//������ģ�鶼ͨ��ʧ�ܵ�ʱ�򣬱�����48��CONVERT����ÿһ������ȡ��ɣ����ķѴ���ʱ�䣬
//����QUETYʱ�����������������������������Query������һ�µ��ˡ�
void Ct_BroadcastUnify(BOOL bForceBroadCast)
{
	int				i;
	int				iAddr;
	int				iReadLen1st,iReadLen2nd;
	BYTE			abyVal[4];
	BOOL			beNeedBroadCast = FALSE;
	float			fValue,fReadLimitValue,fReadVolt,fSettingVolt,fSettingLimit,fSettingHvsdPst,fReadHvsdPst;
	FLOAT_STRING		unVal;
	static int		iFreCount = 0;
	SIG_ENUM		stCTVoltLevelState = 0;

	if ( !(g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue > 0) )
	{
		return;
	}

	int iHVSD_Ctl_Sig_ID = CT_G_SIG_CTRL_HVSD_ID;//Ĭ��48V��ϵͳ��

	unVal.ulValue = 0;

	//210 Convert Group,2:Setting sig 22:Default Current Limit 23:Default Voltage
	fSettingLimit = (float)GetFloatSigValue(CT_GROUP_EQUIPID, 
						SIG_TYPE_SETTING, 
						CT_G_SIG_DEFAULT_LIMIT_ID,
						"Get Default Limit");//Default value is 100.
	stCTVoltLevelState = GetEnumSigValue(1,2,225,"CT Get SysVolt Level");//0  48Voltage Output
	//if(g_CanData.aRoughDataGroup[GROUP_CT_OUTPUT_RATED_VOLTAGE].fValue > 35.0)
	if(stCTVoltLevelState == 0)
	{
		fSettingVolt	= (float)GetFloatSigValue(CT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING, 
							CT_G_SIG_DEFAULT_VOLT_ID, 
							"Get Default Voltage");//Default value is 52.

		fSettingHvsdPst = (float)GetFloatSigValue(CT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING, 
							CT_G_SIG_DEFAULT_HVSD_ID, 
							"Get Default HVSD Voltage");//Default HVSD  value is 59.
		iHVSD_Ctl_Sig_ID = CT_G_SIG_CTRL_HVSD_ID;
	}
	else
	{
		fSettingVolt	= (float)GetFloatSigValue(CT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING, 
							CT_G_SIG_DEFAULT_VOLT_ID_24V, 
							"Get Default Voltage");//Default value is 27.

		fSettingHvsdPst = (float)GetFloatSigValue(CT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING, 
							CT_G_SIG_DEFAULT_HVSD_ID_24V, 
							"Get Default HVSD Voltage");//Default HVSD  value is 29.

		iHVSD_Ctl_Sig_ID = CT_G_SIG_CTRL_HVSD_ID_24V;
	}

	if (iFreCount < 4)
	{
		iFreCount++;
	}
	else
	{
		iFreCount = 0;
		beNeedBroadCast = TRUE;//4Ȧһ�㲥
	}


	if (beNeedBroadCast || bForceBroadCast)
	{
		//Ĭ��������
		CAN_FloatToString(fSettingLimit/100.0, abyVal);

		PackAndSendConvertCmd(MSG_TYPE_RQST_SETTINGS,
			CAN_ADDR_FOR_BROADCAST,
			CAN_CMD_TYPE_BROADCAST,
			0,
			CT_VAL_TYPE_W_DEFAULT_LIMT,
			abyVal);

		Sleep(10);

		////�������źŷ����ı��ˣ�������ź�ҲҪ�ı䡣
		//SetFloatSigValue(CT_GROUP_EQUIPID, 
		//	SIG_TYPE_CONTROL, 
		//	CT_G_SIG_CTRL_LIMIT_ID, 
		//	fSettingLimit, 
		//	"Converter Unify Send Limit");

		//Sleep(10);

		//Ĭ�ϸ����ѹ
		CAN_FloatToString(fSettingVolt, abyVal);

		PackAndSendConvertCmd(MSG_TYPE_RQST_SETTINGS,
			CAN_ADDR_FOR_BROADCAST,
			CAN_CMD_TYPE_BROADCAST,
			0,
			CT_VAL_TYPE_W_DEFAULT_VOLT,
			abyVal);

		Sleep(10);
		////�������źŷ����ı��ˣ�������ź�ҲҪ�ı䡣
		//SetFloatSigValue(CT_GROUP_EQUIPID, 
		//	SIG_TYPE_CONTROL, 
		//	CT_G_SIG_CTRL_OUTPUT_VLT_ID, 
		//	fSettingVolt, 
		//	"Converter Unify Send Out Volt");
		//Sleep(10);

		//Ĭ�ϵ�HVSD��ѹ
		CAN_FloatToString(fSettingHvsdPst, abyVal);

		PackAndSendConvertCmd(MSG_TYPE_RQST_SETTINGS,
					CAN_ADDR_FOR_BROADCAST,
					CAN_CMD_TYPE_BROADCAST,
					0,
					CT_VAL_TYPE_W_DEFAULT_HVSD,
					abyVal);
		Sleep(10);

		SetFloatSigValue(CT_GROUP_EQUIPID, 
			SIG_TYPE_CONTROL, 
			iHVSD_Ctl_Sig_ID,	//CT_G_SIG_CTRL_HVSD_ID, 
			fSettingHvsdPst, 
			"Converter Unify Send HVSD");

	}
	else
	{
		//not process!!
	}

	return;
}

void CTNotSampSigCalc()
{
	int		iAddr;
	int		iCtNum,iCtActualNum;
	int		iValidCtNum = 0;
	float	fAverageCurr = 0;
	float	fAverageVolt = 0;

	//����CT��û������CT����Ӧ���Ž����ж�
	if((g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue 
		!= CAN_SAMP_INVALID_VALUE)
		&& (!g_CanData.aRoughDataGroup[GROUP_CT_ALL_NO_RESPONSE].iValue)
		&& (0 != g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue)
		)
	{
		iCtActualNum = g_CanData.aRoughDataGroup[GROUP_CT_ACTUAL_NUM].iValue;

		for (iCtNum= 0; iCtNum< iCtActualNum; iCtNum++)
		{
			iAddr = GetCtAddrBySeqNo(iCtNum);

			if (CT_COMM_NORMAL_ST == g_CanData.aRoughDataConvert[iAddr][CONVERT_INTERRUPT_ST].iValue )
			{
				iValidCtNum++;
				fAverageCurr += g_CanData.aRoughDataConvert[iAddr][CONVERT_CURRENT].fValue;
				fAverageVolt += g_CanData.aRoughDataConvert[iAddr][CONVERT_DC_VOLTAGE].fValue;
			}
			else
			{
				//Not include interrupt modules!!
			}
		}//end for
		if(iValidCtNum)
		{
			fAverageCurr = (float)(fAverageCurr / iValidCtNum);
			fAverageVolt = (float)(fAverageVolt / iValidCtNum);
		}
		else
		{
			fAverageCurr = 0;
			fAverageVolt = 0;
		}
		
	}
	else
	{
		fAverageCurr = 0;
		fAverageVolt = 0;
	}
	
	//SetFloatSigValue(CT_GROUP_EQUIPID,
	//				SIG_TYPE_SAMPLING,
	//				CT_G_SIG_AVERAGE_VOLT_ID,
	//				fAverageVolt,
	//				"Set CtGroup Average Volt sig");

	SetFloatSigValue(CT_GROUP_EQUIPID,
					SIG_TYPE_SAMPLING,
					CT_G_SIG_AVERAGE_CURR_ID,
					fAverageCurr,
					"Set CtGroup Average Current sig");
	CtSetGoupFeatureSig();
	
}

INT32 WaitCtAlocation()
{
	INT32 iTimeOut = 0;
	INT32 iResetingAddr = 0;

	while (CAN_CONVERT_REALLOCATE == CtSampleCmd00(iResetingAddr))
	{
		iTimeOut++;
		Sleep(500);

		if (iTimeOut > 40)//500MS * 40 = 20S
		{
			return -1;
		}
	}
	return 0;
}

/*==========================================================================*
 * FUNCTION : CT_Sample
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-08-20 17:43
 *==========================================================================*/
void CT_Sample(void)
{
	int		iAddr;
	static		int iTwice = 0;
	static		BOOL bReport = FALSE;
	
	RefreshConvRunTime();
	if(g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig)
	{
		RT_UrgencySendCurrLimit();

		//When trigger allocation, then wait 3 times to reconfig.
		if(g_CanData.CanCommInfo.ConvertCommInfo.iConvterTimes < MAX_CONVERTER_TRY_TIMES)
		{
			g_CanData.CanCommInfo.ConvertCommInfo.iConvterTimes++;
			//printf("g_CanData.CanCommInfo.ConvertCommInfo.iConvterTimes = %d\n",g_CanData.CanCommInfo.ConvertCommInfo.iConvterTimes);
			return;
		}
		Sleep(300);

		/**********************************************************************
		***********************************************************************
			�����ã�
			1��������ûģ�飬������CT��CAN_Sample�л��⵽����λ������
			2������CT֮�󣬼�⵽ģ�����ţ�
			3, ��һ�ϵ磬��֪������������CT
			1����������CT����CT�����Եȴ�û����
			2���������������ˣ��ȴ�Ҳû���⡣
			3������ֻ��3������֪���������Ƿ���CT����Ҳֻ����һ�Σ����ԣ���20��
			û����
		***********************************************************************
		***********************************************************************/
		WaitCtAlocation();
		//This sentence must be in front of CT_Reconfig();
		g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig = FALSE;

		CT_Reconfig();
		iTwice = 0;
		//Here will have fault all converter failure alarm when clear converter comm failure
		//g_CanData.aRoughDataGroup[GROUP_CT_ALL_NO_RESPONSE].iValue 
		//	= CtIsAllNoResponse();
		
		Ct_BroadcastUnify(TRUE);
		bReport = TRUE;

		return;
	}
	if(bReport)
	{
		NotificationToAll(MSG_RECT_ALLOCATION);
		bReport = FALSE;
	}
	BOOL bNeedConfirmPos = NeedHandleCtPos();
	if(bNeedConfirmPos || iTwice < 3 || g_bNeedClearConvIDs)
	{
		if(g_bNeedClearConvIDs)
		{
			g_bNeedClearConvIDs = FALSE;
		}
		HandleCtPos(bNeedConfirmPos);
		//Don't delete:Here is for fixing the reposition problem ,to handle twice!
		//HandleCtPos(bNeedConfirmPos);
		//printf("1----RunThread_PostMessage(-1, &msgFindEquip, FALSE);\n");
		if(iTwice == 3)
		{
			iTwice = 0;
		}
		else if(iTwice == 2)
		{
			RUN_THREAD_MSG msgFindEquip;
			RUN_THREAD_MAKE_MSG(&msgFindEquip, NULL, MSG_CONIFIRM_ID, 0, 0);
			RunThread_PostMessage(-1, &msgFindEquip, FALSE);
			//printf("RunThread_PostMessage(-1, &msgFindEquip, FALSE);nResult = %d\n", nResult);
		}
		iTwice++;
		//Don't delete
	}

	//Ct_BroadcastUnify(FALSE);

	for(iAddr = 0; 
		iAddr < g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum; 
		iAddr++)
	{
		if(g_CanData.CanFlashData.aConverterInfo[iAddr].bExistence)
		{
			if(CAN_CONVERT_REALLOCATE == CtSampleCmd00(iAddr))
			{
				break;
			}
		}
	}

	g_CanData.aRoughDataGroup[GROUP_CT_ALL_NO_RESPONSE].iValue 
		= CtIsAllNoResponse();


	CTNotSampSigCalc();

	return;
}


static BOOL	CtAnalyseSerialNo(int iAddr)
{
	int					i;
	UINT				uiType;

	static CONV_TYPE_INFO		s_aRectType[] =
	{
		{0,		48.0,		27.0,	DC_TYPE_RETURN_DOUBLE,		24.0,	1500.0,	CONV_EFFICIENCY_LT93,},
		//{1,		24.0,		27.0,	DC_TYPE_RETURN_DOUBLE,		24.0,	1500.0,	CONV_EFFICIENCY_LT93,},
		//{2,		48.0,		63.0,	DC_TYPE_RETURN_DOUBLE,		400.0,	1500.0,	CONV_EFFICIENCY_LT93,},
		
		{INVALID_CONV_TYPE, 0.0, 0.0, 0, 0, 0.0, 0.0, 	CONV_EFFICIENCY_LT93,},
	};

	int iHiSn = g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_HIGH].iValue;
	int iLowSn = g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_LOW].iValue;
	UINT uiHiSn = g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_HIGH].uiValue;
	UINT uiLowSn = g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_LOW].uiValue;

	if((CAN_SAMP_INVALID_VALUE != iHiSn)
		&& (CAN_SAMP_INVALID_VALUE != iLowSn))
	{
		uiType = ((uiHiSn >> 4) & 0x000f) * 32 
			+ ((uiLowSn >> 29) & 0x0001) * 16 
			+ ((uiLowSn >> 20) & 0x000f);
		
		g_CanData.aRoughDataConvert[iAddr][CONVERT_TYPE_NO].iValue = uiType;

		i = 0;
		while(s_aRectType[i].uiTypeNo != INVALID_CONV_TYPE)
		{
			if(uiType == s_aRectType[i].uiTypeNo)
			{
				g_CanData.aRoughDataConvert[iAddr][CONVERT_OUTPUT_RATED_VOLTAGE].fValue
					= s_aRectType[i].fRatedVolt;
				g_CanData.aRoughDataConvert[iAddr][CONVERT_OUTPUT_RATED_CURRENT].fValue
					= s_aRectType[i].fRatedCurr;
				g_CanData.aRoughDataConvert[iAddr][CONVERT_INPUT_TYPE].iValue
					= s_aRectType[i].iDcDcType;
				g_CanData.aRoughDataConvert[iAddr][CONVERT_INPUT_RATED_VOLTAGE].fValue
					= s_aRectType[i].fAcRatedVolt;
				g_CanData.aRoughDataConvert[iAddr][CONVERT_EFFECIEY].iValue
					= s_aRectType[i].iEfficiencyNo;
				
				return TRUE;
			}
			i++;
		}
	}

	return FALSE;
}


/*==========================================================================*
 * FUNCTION : CtAnalyseFeature
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE*  pbyBuf : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
static BOOL CtAnalyseFeature(int iAddr)
{
	UINT		uiFeature;
	int			iDcTypeSamp;
	int			iDcTypeReturn;
	int			iAcPhaseNum;
	int			iAcRatedVoltNo;
	int			iRatedCurrNo;
	int			iRatedVoltNo;
	int			iEfficiencyNo;

	float		fMinRatedCurr = 1000.0;

	if(g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].iValue 
		== CAN_SAMP_INVALID_VALUE 
		|| g_CanData.aRoughDataConvert[iAddr][CONVERT_NEWFEATURE].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		//������ѹ�������ѹ������������������ͣ�Ч�ʡ�
		g_CanData.aRoughDataConvert[iAddr][CONVERT_INPUT_RATED_VOLTAGE].fValue
			= 24.0;
		g_CanData.aRoughDataConvert[iAddr][CONVERT_OUTPUT_RATED_VOLTAGE].fValue
			= 48.0;
		g_CanData.aRoughDataConvert[iAddr][CONVERT_OUTPUT_RATED_CURRENT].fValue
			= 27.0;
		g_CanData.aRoughDataConvert[iAddr][CONVERT_INPUT_TYPE].iValue
			= DC_TYPE_RETURN_SINGLE;
		g_CanData.aRoughDataConvert[iAddr][CONVERT_EFFECIEY].iValue
			= CONV_EFFICIENCY_LT93;

		return FALSE;
	}


	uiFeature = g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue;

	//DC input type of rectifier
	iDcTypeSamp = ((uiFeature >> 30) & 0x00000003);
	g_CanData.aRoughDataConvert[iAddr][CONVERT_INPUT_TYPE].iValue = iDcTypeSamp;
	
	//Efficiency
	iEfficiencyNo = ((uiFeature >> 27) & 0x00000007);
	if(iEfficiencyNo == 0x0007)
	{
		iEfficiencyNo = 0;
	}
	g_CanData.aRoughDataConvert[iAddr][CONVERT_EFFECIEY].iValue = iEfficiencyNo;
	
	//Rated Voltage
	iRatedVoltNo = (uiFeature & 0x0000FFFF);	
	g_CanData.aRoughDataConvert[iAddr][CONVERT_INPUT_RATED_VOLTAGE].fValue
			= 0.1 * (float)iRatedVoltNo;

	uiFeature = g_CanData.aRoughDataConvert[iAddr][CONVERT_NEWFEATURE].uiValue;
	//Output rated voltage
	int iOutputVoltage = (uiFeature & 0x0000FFFF);	
	g_CanData.aRoughDataConvert[iAddr][CONVERT_OUTPUT_RATED_VOLTAGE].fValue = (float)iOutputVoltage * 0.1;
	
	///Output rated current
	int  iOutputRatedCurrent= ((uiFeature >> 16)& 0x0000FFFF);
	if(iOutputRatedCurrent == 0xFFFF)
	{
		iRatedCurrNo = 54;	//means 50A
	}
	g_CanData.aRoughDataConvert[iAddr][CONVERT_OUTPUT_RATED_CURRENT].fValue
			= (float)iOutputRatedCurrent * 0.01;
	

	return TRUE;

}


/*==========================================================================*
* FUNCTION : CtSetGoupFeatureSig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : by IlockTeng                DATE: 2009-10-21 16:25
*==========================================================================*/
static void CtSetGoupFeatureSig()
{
	if(g_CanData.aRoughDataConvert[0][CONVERT_INPUT_RATED_VOLTAGE].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataGroup[GROUP_CT_INPUT_RATED_VOLTAGE].fValue = 24.0;
		g_CanData.aRoughDataGroup[GROUP_CT_OUTPUT_RATED_VOLTAGE].fValue = 48.0;
		g_CanData.aRoughDataGroup[GROUP_CT_OUTPUT_RATED_CURRENT].fValue = 27.0;
		
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_CT_INPUT_RATED_VOLTAGE].fValue 
				= g_CanData.aRoughDataConvert[0][CONVERT_INPUT_RATED_VOLTAGE].fValue;
		g_CanData.aRoughDataGroup[GROUP_CT_OUTPUT_RATED_VOLTAGE].fValue 
				= g_CanData.aRoughDataConvert[0][CONVERT_OUTPUT_RATED_VOLTAGE].fValue;
		g_CanData.aRoughDataGroup[GROUP_CT_OUTPUT_RATED_CURRENT].fValue 
				= g_CanData.aRoughDataConvert[0][CONVERT_OUTPUT_RATED_CURRENT].fValue;		
		g_CanData.aRoughDataGroup[GROUP_CT_TYPE].iValue 
				= g_CanData.aRoughDataConvert[0][CONVERT_TYPE_NO].iValue;		
		
	}
}
/*==========================================================================*
 * FUNCTION : SetRectRunTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr  : 
 *            float  fParam : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:30
 *==========================================================================*/
static void SetConvRunTime(int iAddr, float fParam)
{
	BYTE			abyVal[4];

	CAN_FloatToString(fParam , abyVal);
	PackAndSendConvertCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iAddr,
				CAN_CMD_TYPE_P2P,
				0,
				CT_VAL_TYPE_W_RUN_TIME,
				abyVal);
	return;
}


/*==========================================================================*
 * FUNCTION : RefreshRectRunTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:30
 *==========================================================================*/
static void RefreshConvRunTime(void)
{
	int					i;
	time_t				tNow;   
	struct tm			tmNow; 

   
	tNow = time(NULL);

	gmtime_r(&tNow, &tmNow);

	//printf("RefreshConvRunTime %d [%d]\n", tmNow.tm_min, g_CanData.CanCommInfo.ConvertCommInfo.bNeedRefreshRuntime);
	if(g_CanData.CanCommInfo.ConvertCommInfo.bNeedRefreshRuntime)
	{
		if(tmNow.tm_min == 59)
		//if(tmNow.tm_min % 3 == 0)
		{
			for(i = 0; i < MAX_NUM_CONVERT; i++)
			{
				/*printf("Conv%d [%d][%d][%d]\n",i,g_CanData.aRoughDataConvert[i][CONVERT_TOTAL_RUN_TIME].iValue,
							g_CanData.aRoughDataConvert[i][CONVERT_INTERRUPT_TIMES].iValue,
							 g_CanData.aRoughDataConvert[i][CONVERT_STOP_ST].uiValue);*/
				if(g_CanData.aRoughDataConvert[i][CONVERT_TOTAL_RUN_TIME].iValue
					== CAN_SAMP_INVALID_VALUE)
				{
					//Do nothing until a sampling
					continue;
				}
				
				if(g_CanData.aRoughDataConvert[i][CONVERT_INTERRUPT_TIMES].iValue
						< CT_MAX_INTERRUPT_TIMES)
				{
					if(1 == g_CanData.aRoughDataConvert[i][CONVERT_ON_OFF_ST].uiValue)//When control CT, 0 is on, 1 is off, when sample , 0 is off, 1 is on. from protocol.
					{
						/*printf("Into SetConvRunTime\n");*/
						g_CanData.aRoughDataConvert[i][CONVERT_TOTAL_RUN_TIME].uiValue++;
						SetConvRunTime(i, 
							(float)(g_CanData.aRoughDataConvert[i][CONVERT_TOTAL_RUN_TIME].uiValue));
						Sleep(10);
					}
				 }
			 }
			 g_CanData.CanCommInfo.ConvertCommInfo.bNeedRefreshRuntime = FALSE;
		 }
	}
	else
	{
		if(tmNow.tm_min  == 1)
		{
			/*printf("tmNow.tm_min % 3 \n");*/
			g_CanData.CanCommInfo.ConvertCommInfo.bNeedRefreshRuntime = TRUE;
		}
	}

	return;
}

/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : can_rectifier.c
*  CREATOR  :                 DATE: 2012-02-21 09:38
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>
#include "basetypes.h"
#include "NA_Battery.h"
#include "can_smdu.h" //Jimmy:���ڹ���SMDU��web�����ı���Ϣ��Ӧ�ź�


extern	CAN_SAMPLER_DATA		g_CanData;
G_CAN_NA_BATT_SAMP			g_CanNABattSamp;
/*********************************************************************************
*  
*  FUNCTION NAME : Set_LiBatt_WorkMode
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-1 13:10:18
*  DESCRIPTION   : Set the Li Batt work mode so as to differ 
from traditional battery
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Set_LiBatt_WorkMode()
{	
	SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SAMPLING, SIGID_LI_LOST_CLEAR_STATE, SIG_VALUE_NO_LOSTCLEAR, "CanLiBatt Reset Clear Lost");
	//����Ϊ�������Comm Interrup���澯��Falg��־
	SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SAMPLING, SIGID_LI_COMMINTERR_CLEAR_STATE, SIG_VALUE_NO_LOSTCLEAR, "CanLiBatt Clear CommFail");

	//�������۾���ֻ�����Ƿ����������ļ�����������״̬
	//here we set the work mode of Li Batt, this signla will be used in GC
	//if(g_CanData.bFlagLiBattCfgExisted)
	//{
	//	if(g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_INSTALL_NUM].iValue > 0)
	//	{
	//		//g_CanData.bFlagLiBattCfgExisted = TRUE;
	//		SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SETTING, SIGID_BATT_TYPE_LI, TYPE_IS_LIBATT, "CanLiBatt Set Batt Mode");
	//	}
	//	else
	//	{
	//		//g_CanData.bFlagLiBattCfgExisted = FALSE;
	//		SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SETTING, SIGID_BATT_TYPE_LI, TYPE_NONE_LIBATT, "CanLiBatt Set Batt Mode");
	//	}		
	//}
	//else
	//{
	//	//g_CanData.bFlagLiBattCfgExisted = FALSE;
	//	SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SETTING, SIGID_BATT_TYPE_LI, TYPE_NONE_LIBATT, "CanLiBatt Set Batt Mode");
	//}
}

/*=============================================================================*
* FUNCTION: NABatt_InitRoughValue
* PURPOSE : Initialization NABatt RoughData
* INPUT:	 	
*   
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void NABatt_InitRoughValue(void)
{
	INT32 i,j,k;

	g_CanNABattSamp.iCommCount = 0; //��ʼ��ͨ�ż���

	for (i = 0; i < CAN_NA_BATT_MAX_NUM;i++)
	{
		for (j = 0; j < ROUGH_MAX_SIG_END;j++)
		{
			g_CanNABattSamp.aRoughData[i][j].iValue = CAN_SAMP_INVALID_VALUE;

//#ifdef TEST_DEBUG_NA_BATT
//			
//			
//			
//			
//			g_CanNABattSamp.aRoughData[i][ROUGH_BATT_VOLT_0X0001].fValue = 41.0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_TERMINAL_VOLT_0X0001].fValue = 42.0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_TERMINAL_VOLT_0X0001].fValue = 42.0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_BATT_CURR_0X0002].fValue = 43.0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_CELL_TEMP_0X0003].fValue = 44.0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_SWITCH_TEMP_0X0003].fValue = 45.0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_STATUS_OF_CHARGE_0X0004].fValue = 46.0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_STAT4_CHAR_EN_0X0010].iValue = 0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_STAT5_DISCHAR_EN_0X0010].iValue = 1;
//
//			g_CanNABattSamp.aRoughData[i][ROUGH_STAT6_CHARGING_0X0010].iValue = 0;
//			g_CanNABattSamp.aRoughData[i][ROUGH_STAT7_DISCHARGING_0X0010].iValue = 1;
//
//			g_CanNABattSamp.aRoughData[i][ROUGH_STAT9_DISCHARGING_THAN_5A_0X0010].iValue = 1;
//			g_CanNABattSamp.aRoughData[i][ROUGH_STAT11_CHARGING_THAN_5A_0X0010].iValue = 1;
//			g_CanNABattSamp.aRoughData[i][ROUGH_STAT14_DISCHARGING_ENABLE_0X0010].iValue = 0;
//			g_CanNABattSamp.aRoughData[i][CHNNEL_STAT15_CHARGING_ENABLE_0X0010].iValue = 1;
//
//			g_CanNABattSamp.aRoughData[i][ROUGH_FAULT2_TEMP_FAULT_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT3_CURRENT_FAULT_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT5_HARDWARE_FAULT_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT6_OVER_VOLTAGE_0X0011].iValue = 1;
//
//
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT7_LOW_VOLTAGE_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT8_CELL_VOLT_DEVIATION_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT11_LOW_CELL_VOLTAGE_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT12_HIGH_CELL_VOLTAGE_0X0011].iValue = 1;
//
//
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT13_HIGH_CELL_TEMP_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT16_HIGH_SWITCH_TEMP_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT17_CHARGE_SHORT_CIRUIT_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT18_DISCHARGE_SHORT_CIRUIT_0X0011].iValue = 1;
//
//
//
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT19_HIGH_SWITCH_TEMP_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT20_HARDWARE_FAILURE_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][CHNNEL_FAULT21_HARDWARE_FAILURE_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT22_HIGH_CHARGE_CURRENT_0X0011].iValue = 1;
//			//g_CanNABattSamp.aRoughData[i][ROUGH_FAULT23_HIGH_DISCHARGE_CURRENT_0X0011].iValue = 1;
//
//#endif

		}

		g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_EXIST_STAT].iValue
			= CAN_NA_BATT_NOT_EXIT;
		g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_COMM_STAT].iValue
			= CAN_NA_BATT_COMM_FAILURE;
		g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_DISCONNECTED_STAT].iValue
			= CAN_NA_BATT_CONNECTED;

//#ifdef TEST_DEBUG_NA_BATT
//		g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_EXIST_STAT].iValue
//			= CAN_NA_BATT_EXIT;
//		g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_COMM_STAT].iValue
//			= CAN_NA_BATT_COMM_OK;
//#endif

	}

	g_CanNABattSamp.bNeedReconfig = TRUE;
	g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_EXIST_STAT].iValue= CAN_NA_BATT_NOT_EXIT;
	g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_INSTALL_NUM].iValue = 0;
	//Set_LiBatt_WorkMode();//��ʼ����workmodeΪnon-LIģʽ,�������۾���ֻ�����Ƿ����������ļ�����������״̬
	g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_DISCONNCTED_NUM].iValue = 0;
	g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_NOREPLY_NUM].iValue = 0;
	//��ʼ���븳ֵΪ����ͨ���жϣ���Ϊδ����Li��ص������ҲҪ���ͨ���ж�
	g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_COMM_STAT].iValue = CAN_NA_BATT_COMM_FAILURE;

	//g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_UPDATE_INVENTORY_PROCESS].iValue = CAN_NA_BATT_NORMAL_SAMP;

	g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_FIRMVERSION_0X0056].iValue = CAN_SAMP_INVALID_VALUE;
	g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART1_0X005A].iValue = CAN_SAMP_INVALID_VALUE;
	g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART2_0X005B].iValue = CAN_SAMP_INVALID_VALUE;
	g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART3_0X005C].iValue = CAN_SAMP_INVALID_VALUE;
	g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART4_0X005D].iValue = CAN_SAMP_INVALID_VALUE;

//#ifdef TEST_DEBUG_NA_BATT
//	g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_EXIST_STAT].iValue
//		= 0;
//#endif

}


/*==========================================================================*
* FUNCTION : PackAndSendSmduCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UINT   uiMsgType    : 
*            UINT   uiDestAddr   : 
*            UINT   uiCmdType    : 
*            UINT   uiValueTypeH : 
*            UINT   uiValueTypeL : 
*            float  fParam       : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Copy Frank Cao                DATE: 2010-02-25 16:27
*==========================================================================*/
static void PackAndSendNABattCmd(UINT uiMsgType,
			       UINT uiDestAddr,
			       UINT uiCmdType,
			       UINT uiValueTypeH,
			       UINT uiValueTypeL,
			       float fParam)
{
	BYTE*	pbySendBuf = g_CanNABattSamp.cBySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_NABATT_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	

	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}

/*=============================================================================*
* FUNCTION: NABatt_Getu16BitValue(BYTE *pFrame)
* PURPOSE : Get uValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     INT32   iValueType
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static UINT16 NABatt_Get_UInt16BitValue(BYTE *pFrame)
{
	BYTE * pValue;
	UINT16 uiValueType	= 0;
	UINT16 uiValueTypeH	= 0;
	UINT16 uiValueTypeL	= 0;

	pValue = pFrame;
	uiValueTypeH = (UINT16)(*pValue);
	pValue = pFrame + 1;
	uiValueTypeL = (UINT16)(*pValue);
	uiValueType = ((uiValueTypeH << 8) + uiValueTypeL);

	return uiValueType;
}

static UINT32 SNABatt_Getu32BitValue(BYTE *pFrame)
{
	BYTE * pValue;
	pValue = pFrame;

	UINT32 uiValue = (((UINT32)(pValue[0])) << 24)
		+ (((UINT32)(pValue[1])) << 16)
		+ (((UINT32)(pValue[2])) << 8)
		+ ((UINT32)(pValue[3]));
	return uiValue;
}


void UnpackValueType_10(int iAddr, UINT uValue)
{
	/*************************************************************************
	Format of data frame:
	ERR	MSGTYPE	ErrType	VALUETYPE	Status0	����	Status31
	1 bit	7 bits	1 byte	2 bytes		1 bit	����	1 bit
	Byte0	Byte1	Byte2	Byte3		Byte4��Byte 7
	**************************************************************************/
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT4_CHAR_EN_0X0010].iValue = (uValue >> (31 -4)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT5_DISCHAR_EN_0X0010].iValue = (uValue >> (31 - 5)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT6_CHARGING_0X0010].iValue = (uValue >> (31 - 6)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT7_DISCHARGING_0X0010].iValue = (uValue >> (31 - 7)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT9_DISCHARGING_THAN_5A_0X0010].iValue = (uValue >> (31 - 9)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT11_CHARGING_THAN_5A_0X0010].iValue = (uValue >> (31 - 11)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT14_DISCHARGING_ENABLE_0X0010].iValue = (uValue >> (31 - 14)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT15_CHARGING_ENABLE_0X0010].iValue = (uValue >> (31 - 15)) & 0x01;
	return;
}


void UnpackValueType_11(int iAddr, UINT uValue)
{

	/*************************************************************************
	Format of data frame:
	ERR	MSGTYPE	ErrType	VALUETYPE	Fault0	����	Fault31
	1 bit	7 bits	1 byte	2 bytes		1 bit	����	1 bit
	Byte0	Byte1	Byte2	Byte3		Byte4��Byte 7
	**************************************************************************/

	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT2_TEMP_FAULT_0X0011].iValue = (uValue >> (31 -2)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT3_CURRENT_FAULT_0X0011].iValue = (uValue >> (31 - 3)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT5_HARDWARE_FAULT_0X0011].iValue = (uValue >> (31 - 5)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT6_OVER_VOLTAGE_0X0011].iValue = (uValue >> (31 - 6)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT7_LOW_VOLTAGE_0X0011].iValue = (uValue >> (31 - 7)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT8_CELL_VOLT_DEVIATION_0X0011].iValue = (uValue >> (31 - 8)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT11_LOW_CELL_VOLTAGE_0X0011].iValue = (uValue >> (31 - 11)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT12_HIGH_CELL_VOLTAGE_0X0011].iValue = (uValue >> (31 - 12)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT13_HIGH_CELL_TEMP_0X0011].iValue = (uValue >> (31 - 13)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT16_HIGH_SWITCH_TEMP_0X0011].iValue = (uValue >> (31 - 16)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT17_CHARGE_SHORT_CIRUIT_0X0011].iValue = (uValue >> (31 - 17)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT18_DISCHARGE_SHORT_CIRUIT_0X0011].iValue = (uValue >> (31 - 18)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT19_HIGH_SWITCH_TEMP_0X0011].iValue = (uValue >> (31 - 19)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT20_HARDWARE_FAILURE_0X0011].iValue = (uValue >> (31 - 20)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT21_HARDWARE_FAILURE_0X0011].iValue = (uValue >> (31 - 21)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT22_HIGH_CHARGE_CURRENT_0X0011].iValue = (uValue >> (31 - 22)) & 0x01;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT23_HIGH_DISCHARGE_CURRENT_0X0011].iValue = (uValue >> (31 - 23)) & 0x01;
	return;
}

/*==========================================================================*
* FUNCTION : NABatt_ UnpackCmd00
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  :              DATE: 2008-07-19 17:29
*==========================================================================*/
static BOOL NABatt_UnpackCmd00(int iAddr,
			  int iReadLen,
			  BYTE* pbyRcvBuf)
{
	int			i;
	int			iFrameOrder = 0;
	INT16			i16TempValue;
	UINT			uiTempValue;
	int			iTempValue;
	BYTE			byTempValue;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
						+ i * CAN_FRAME_LEN
						+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_NABATT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
			!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case NA_BATT_VALUE_TYPE_01:
			//Get Battery/Terminal Voltage
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_BATT_VOLT_0X0001].fValue 
					= (float)NABatt_Get_UInt16BitValue(pValueBuf)/100.0;

			g_CanNABattSamp.aRoughData[iAddr][ROUGH_TERMINAL_VOLT_0X0001].fValue 
				= (float)NABatt_Get_UInt16BitValue(pValueBuf + 2)/100.0;
			
			if (0 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_02:
			//Get Battery Current 
			i16TempValue = (INT16)NABatt_Get_UInt16BitValue(pValueBuf);

			//iTempValue = (((uiTempValue >> 15) & 0x01) ? (-1):(1)) * (uiTempValue & 0x7FFFFFFF);
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_BATT_CURR_0X0002].fValue 
				= (float)i16TempValue/ 100.00;

			if (1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_03:
			//Get Cell/ Switch Temperature
			//byTempValue = (*pValueBuf);
			////
			//ע�⣬ByteΪ�޷����ͣ�������Ĭ��charΪ�޷����ͣ�����Ҫ��signedǿ�Ʒ��ţ���Ȼ��������Ŵ���Jimmyע��
			////
			iTempValue = (int)(signed char)(*pValueBuf);
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_CELL_TEMP_0X0003].fValue 
				= (float)(iTempValue); //(float)((((byTempValue >> 7) & 0x01) ? (-1):(1)) * (byTempValue & 0x7F));
			
			//byTempValue = (*(pValueBuf + 1));
			iTempValue = (int)(signed char)(*(pValueBuf + 1));
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_SWITCH_TEMP_0X0003].fValue 
				= (float)(iTempValue); //(float)((((byTempValue >> 7) & 0x01) ? (-1):(1)) * (byTempValue & 0x7F));

			if (2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_04:
			//Get State Of Charge 0-255 = 0%---100%
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_STATUS_OF_CHARGE_0X0004].fValue = (100.0/255.0 * (*pValueBuf));
			if (3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_06:
			//Get LED/Relay Status
			byTempValue = (*pValueBuf);
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_LED_VALUES_0X0006].uiValue 
				= (UINT) (byTempValue % 10); //0 1 2 3 4 5 6����LED״̬

			byTempValue = (*(pValueBuf + 1));
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_RELAY_VALUES_0X0006].uiValue 
				= (UINT) (byTempValue % 2);// 0 1 ����

			if (4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_10:
			uiTempValue = SNABatt_Getu32BitValue(pValueBuf);
			//Status0 ---- Status31
			UnpackValueType_10(iAddr, uiTempValue);
			if (5 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_11:
			uiTempValue = SNABatt_Getu32BitValue(pValueBuf);
			//Fault0 ---- Fault31
			UnpackValueType_11(iAddr, uiTempValue);
			if (6 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
		default:
			break;
		}
	}

	if(NA_BATT_CMD00_RCV_FRAMES_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static INT32  NABatt_SampleCmd00(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendNABattCmd(MSG_TYPE_RQST_DATA00,
				(UINT)iAddr,
				CAN_CMD_TYPE_P2P,
				0,				//Half load on AC phase failure
				0,
				0.0);	

	Sleep((NA_BATT_CMD00_RCV_FRAMES_NUM * NA_BATT_FRAME_FRAME_INTTERVAL) 
		+ NA_BATT_FRAME_ENOUGH_WAIT_TIME);

	if(CAN_NABATT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
							g_CanData.CanCommInfo.abyRcvBuf, 
							&iReadLen1st))
	{
		return CAN_NABATT_REALLOCATE;
	}
	else if(iReadLen1st < (NA_BATT_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		if(CAN_NABATT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd))

		{
			return CAN_NABATT_REALLOCATE;
		}
	}
	//û���յ�F4����Ϊ���������ɼ���Jimmyע
	//g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_UPDATE_INVENTORY_PROCESS].iValue = CAN_NA_BATT_NORMAL_SAMP;
	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= NA_BATT_CMD00_RCV_FRAMES_NUM)
	{
		if (NABatt_UnpackCmd00(iAddr, 
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{			
			return CAN_SAMPLE_OK;
		}
		else
		{
			return CAN_SAMPLE_FAIL;
		}
		
	}
	else
	{
		return CAN_SAMPLE_FAIL;	
	}
	
	//return CAN_SAMPLE_OK;
}

static BOOL NABatt_UnpackCmd10(int iAddr,
			       int iReadLen,
			       BYTE* pbyRcvBuf)
{
	int			i;
	int			iFrameOrder = 0;
	UINT			uiTempValue;
	int			iTempValue;
	BYTE			byTempValue;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_NABATT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case NA_BATT_VALUE_TYPE_07:
			//Get Manufacture Date
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_MANUFACTURE_DATE_0X0007].uiValue 
				= SNABatt_Getu32BitValue(pValueBuf);

			if (0 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_09:
			//Get ControlBoard Firmware Num/cfg/revision
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_CONTRL_FIRMWARE_NUMBER_0X0009].uiValue 
				= SNABatt_Getu32BitValue(pValueBuf);

			if (1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_20:
			//Get Serial No Hi
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART12_0X0020].uiValue 
				= SNABatt_Getu32BitValue(pValueBuf);

			if (2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_21:
			//Get ControlBoard Firmware Num/cfg/revision
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART34_0X0021].uiValue 
				= SNABatt_Getu32BitValue(pValueBuf);
			
			if (3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}

	if(NA_BATT_CMD10_RCV_FRAMES_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static INT32  NABatt_SampleCmd10(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendNABattCmd(MSG_TYPE_RQST_DATA10,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,				//Half load on AC phase failure
		0,
		0.0);	

	Sleep((NA_BATT_CMD10_RCV_FRAMES_NUM * NA_BATT_FRAME_FRAME_INTTERVAL) 
		+ NA_BATT_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (NA_BATT_CMD10_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}
	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= NA_BATT_CMD10_RCV_FRAMES_NUM)
	{
		if (NABatt_UnpackCmd10(iAddr, 
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			return CAN_SAMPLE_OK;
		}
		else
		{
			return CAN_SAMPLE_FAIL;
		}
	}
	else
	{
		return CAN_SAMPLE_FAIL;	
	}

	//return CAN_SAMPLE_OK;
}

static BOOL NABatt_UnpackCmd20(int iAddr,
			       int iReadLen,
			       BYTE* pbyRcvBuf,
			       int iFrameNum)
{
	int			i;
	int			iFrameOrder = 0;
	UINT			uiTempValue;
	int			iTempValue;
	BYTE			byTempValue;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*	pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_NABATT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}
		if(iFrameNum == NA_BATT_CMD20_RCV_FRAMES_NUM_BRIDGE)
		{
			switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
			{
			case NA_BATT_VALUE_TYPE_56:
				//Get Bridge Card Firmware Ver
				g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_FIRMVERSION_0X0056].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (0 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_5A:
				//Get Bridge Card Barcode 1
				g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART1_0X005A].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (1 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_5B:
				//Get Bridge Card Barcode 2
				g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART2_0X005B].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (2 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_5C:
				//Get Bridge Card Barcode 3
				g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART3_0X005C].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);
				
				if (3 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_5D:
				//Get Bridge Card Barcode 4
				g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART4_0X005D].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (4 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_54:
				//Get Bridge Card Serial No Hi
				g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_LOW_0X0054].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (5 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_55:
				//Get Bridge Card Serial No Low
				g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_HI_0X0055].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (6 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;


			default:
				break;
			}
		}
		else
		{
			switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
			{
			case NA_BATT_VALUE_TYPE_5A:
				//Get Li Battery Barcode 1
				g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART1_0X005A].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (0 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_5B:
				//Get Li Battery Barcode 2
				g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART2_0X005B].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (1 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_5C:
				//Get Li Battery Barcode 3
				g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART3_0X005C].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);

				if (2 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case NA_BATT_VALUE_TYPE_5D:
				//Get Li Battery Barcode 4
				g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART4_0X005D].uiValue 
					= SNABatt_Getu32BitValue(pValueBuf);
				
				if (3 == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			default:
				break;
			}

		}

	}

	if(iFrameNum == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static INT32  NABatt_SampleCmd20(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	FLOAT_STRING	unVal;
	int iFrameNum = (iAddr == BRIDGE_CARD_ADDR) ? (int)NA_BATT_CMD20_RCV_FRAMES_NUM_BRIDGE : (int)NA_BATT_CMD20_RCV_FRAMES_NUM_LI;
	unVal.ulValue = 0;
	PackAndSendNABattCmd(MSG_TYPE_RQST_DATA20,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,				//Half load on AC phase failure
		0,
		0.0);	

	Sleep((iFrameNum * NA_BATT_FRAME_FRAME_INTTERVAL) 
		+ NA_BATT_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);
	if(iReadLen1st < (iFrameNum * CAN_FRAME_LEN))
	{
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) >= iFrameNum)
	{
		if (NABatt_UnpackCmd20(iAddr, 
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf,
			iFrameNum))
		{
			return CAN_SAMPLE_OK;
		}
		else
		{
			//printf("Sample Brigde Card Fail due to Unpack Error!!");
			return CAN_SAMPLE_FAIL;
		}

	}
	else
	{
		//printf("Sample Brigde Card Fail due to No data!!");
		return CAN_SAMPLE_FAIL;	
	}

	//return CAN_SAMPLE_OK;
}

static BOOL NABatt_UnpackCmd21(int iAddr,
			       int iReadLen,
			       BYTE* pbyRcvBuf)
{
	int			i;
	int			iFrameOrder = 0;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_NABATT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case NA_BATT_VALUE_TYPE_22:
			//Get Li Battery Name Part1
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_NAME_PART1_0X0022].uiValue 
				= SNABatt_Getu32BitValue(pValueBuf);

			if (0 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_23:
			//Get Li Battery Name Part2
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_NAME_PART2_0X0023].uiValue 
				= SNABatt_Getu32BitValue(pValueBuf);

			if (1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_24:
			//Get Li Battery Name Part3
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_NAME_PART3_0X0024].uiValue 
				= SNABatt_Getu32BitValue(pValueBuf);

			if (2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case NA_BATT_VALUE_TYPE_25:
			//Get Li Battery Name Part1
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_NAME_PART4_0X0025].uiValue 
				= SNABatt_Getu32BitValue(pValueBuf);

			if (3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}

	if(NA_BATT_CMD21_RCV_FRAMES_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static INT32  NABatt_SampleCmd21(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendNABattCmd(MSG_TYPE_RQST_DATA21,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,				//Half load on AC phase failure
		0,
		0.0);	

	Sleep((NA_BATT_CMD21_RCV_FRAMES_NUM * NA_BATT_FRAME_FRAME_INTTERVAL) 
		+ NA_BATT_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (NA_BATT_CMD21_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= NA_BATT_CMD21_RCV_FRAMES_NUM)
	{
		if (NABatt_UnpackCmd21(iAddr, 
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			return CAN_SAMPLE_OK;
		}
		else
		{
			return CAN_SAMPLE_FAIL;
		}

	}
	else
	{
		return CAN_SAMPLE_FAIL;	
	}

	//return CAN_SAMPLE_OK;
}



/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-9-12 14:21:27
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void NABatt_TriggleAllocation(void)
{
	//���ʹ���BridgeCard���ŵ�ָ���־λ�����ùܻظ���ֱ���ֶ�����Reconfig
	PackAndSendNABattCmd(MSG_TYPE_RQST_DATA20,
		(UINT)BRIDGE_CARD_ADDR,
		CAN_CMD_TYPE_P2P,
		0x80,				//Bit 7 = 1 ,trigger rellocate for Bridge card
		0,
		0.0);
	g_CanNABattSamp.bNeedReconfig = TRUE;
	g_CanNABattSamp.iCommCount = 1; //�����������ɨ��һ��product Info
}
/*********************************************************************************
*  
*  FUNCTION NAME : Set_Inventory_Updata_Flag
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-1 13:13:04
*  DESCRIPTION   : Set the flag for re-allocate process of Li Battery
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Set_Inventory_Updata_Flag(BOOL bUpdate)
{
	SetEnumSigValue(LI_BATT_GROUP_EQUIPID, SIG_TYPE_SAMPLING, SIGID_LI_BATT_INVENTORY_UPDATE, 
		(bUpdate) ? CAN_NA_BATT_INVENTORY_UPDATING : CAN_NA_BATT_NORMAL_SAMP, 
		"CAN Set LiBatt Inventory Updating Flag");
}
void NABatt_Reconfig(void)
{
	
	int i =0;
	int iAddr = 0;
	int iNABattNum = 0;
	int iRepeat00CmdTimes = 0;
	int iStatus = -1;
	int iLastOnlineAddr = -1; //ɨ�赽�����һ�����ߵĵ�ص�ַ
	int iNotFindAddrNum = 0;//����ûɨ���ĸ���
	static iNonContinousState = 0;//�������ξ����ֵ�ַ������
	//���Է��ֵ�autoreconfig֮ǰ�ε�CAN��֮�󣬴�ʱLi����жϵ�Li��ػ�����ڡ�������Ҫ������ȥ��Existence״̬
	for(i = 0; i < CAN_NA_BATT_MAX_NUM; i++)
	{
		g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_EXIST_STAT].iValue = CAN_NA_BATT_NOT_EXIT;
	}
	//Jimmy Add 2012/08/31: �������޵�أ����ɼ�Bridge Card����Ȼ��ʼʱ���е�ض��Ͽ��ˣ��Ͳɼ�����Bridge Card�ˡ�
	static int iSampleCount = 0;
	if(g_CanNABattSamp.iCommCount == 0) //�״����У�ɨ��һ��Bridge Card
	{
		iStatus = NABatt_SampleCmd20(BRIDGE_CARD_ADDR); //�е�ص�����£��ɼ�һ��Bridge Card�ĵ�ַ
		if(CAN_SAMPLE_OK == iStatus)
		{
			//����
		}
		else
		{
			iSampleCount++;
			if(iSampleCount <= 2) //����2�ζ�û�ɼ�����8s�����Ͳ��ٲɼ�
			{
				//printf("\n*******!!!Sample Bridge Card Failure!!!**********\n");
				g_CanNABattSamp.bNeedReconfig = TRUE;
				return;
			}
		}
	}
	else
	{
		iSampleCount = 0;
	}
	for (iAddr =  0; iAddr < CAN_NA_BATT_MAX_NUM; iAddr++)
	{
		iRepeat00CmdTimes = 0;
		while(iRepeat00CmdTimes < NABATT_MAX_SCAN_TIMES)
		{
			iStatus = NABatt_SampleCmd00(iAddr);

//#ifdef TEST_DEBUG_NA_BATT
//			static int iTestTimes = 0;
//			if (iTestTimes < 60)
//			{
//				
//				iStatus = CAN_SAMPLE_OK;
//			}
//			else
//			{
//				iTestTimes = 0;
//				iStatus = CAN_SAMPLE_FAIL;
//			}
//			iTestTimes++;
//#endif

			if(CAN_SAMPLE_OK == iStatus)
			{
				//printf("\n*******Oh God I found an Li Batt**********\n");
				NABatt_SampleCmd10(iAddr);
				NABatt_SampleCmd20(iAddr);
				//NABatt_SampleCmd21(iAddr);�ݲ�ʹ�ã�Ϊ�Ժ��� Jimmy2012/04/01
				break;
			}
			else if(CAN_NABATT_REALLOCATE == iStatus)
			{
				//printf("\n*******!!!IN Relocating!!!**********\n");
				g_CanNABattSamp.bNeedReconfig = TRUE;
				Set_Inventory_Updata_Flag(TRUE); //������֮ǰ��������飬֪ͨGC����Ӧ����
				return;
			}
			else if(CAN_SAMPLE_FAIL == iStatus)
			{
				iRepeat00CmdTimes++;
				Sleep(150);
			}
			else
			{
				iRepeat00CmdTimes++;
				TRACE("		The app running  as  err	");
			}
		}//end while

		//if(iRepeat00CmdTimes >= NABATT_MAX_SCAN_TIMES)
		if(CAN_SAMPLE_OK == iStatus)
		{
			iLastOnlineAddr = iAddr;
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_SEQUENCE_NO].iValue = iNABattNum++;
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_COMM_STAT].iValue = CAN_NA_BATT_COMM_OK;
			g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_EXIST_STAT].iValue = CAN_NA_BATT_EXIT;
			//--------------�����е�ַ���������������Ӳ�������ַ�ж�
			if(iLastOnlineAddr + 1 > iNABattNum)
			{
				iNonContinousState++;
				g_CanNABattSamp.bNeedReconfig = TRUE;
				if(iNonContinousState > 3 || g_CanNABattSamp.iCommCount == 0)//��������reconfig��Ȼ�����������ߵ�һ�����е�ʱ����
				{
					iNonContinousState = 0;
					//printf("\n*******!!!Trigle Relocation!!!**********\n");
					NABatt_TriggleAllocation();
				}
				break;
			}
			//--------------end
		}
		else
		{
			iNotFindAddrNum++; //û�ҵ���������1
			if(iNotFindAddrNum >= 5) //����5��û�ҵ�������ɨ��
			{
				break; //Jimmyע�ͣ���һ�ΰ����ɨ�裬�����ֵ�ַ���������򴥷�����
			}
			//g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_EXIST_STAT].iValue = CAN_NA_BATT_NOT_EXIT;
		}

	}//end for
	//printf("\n*******!!!Begin counting Online Number!!!**********\n");
	//��������ˢ��Web��LCD����comm inte�澯ʱ���á�
	if(g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_INSTALL_NUM].iValue != iNABattNum)
	{
		//For LCD
		SetDwordSigValue(SMDU_GROUP_EQUIPID, 
			SIG_TYPE_SAMPLING,
			CFG_CHANGED_SIG_ID,
			1,
			"CAN_SAMP");

		//For Web
		static	DWORD		s_dwChangeForWeb = 0;
		s_dwChangeForWeb++;
		if(s_dwChangeForWeb >= 255)
		{
			s_dwChangeForWeb = 0;
		}
		SetDwordSigValue(SMDU_GROUP_EQUIPID, 
			SIG_TYPE_SAMPLING,
			CFG_CHANGED_SIG_ID + 1,
			s_dwChangeForWeb,
			"CAN_SAMP");

	}
	if(iNABattNum > 0)
	{
		g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_INSTALL_NUM].iValue = iNABattNum;
		g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_COMM_STAT].iValue = CAN_NA_BATT_COMM_OK;
		g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_EXIST_STAT].iValue = CAN_NA_BATT_NOT_EXIT; //��Զ������
	}
	else
	{
		g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_INSTALL_NUM].iValue = 0;
		g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_COMM_STAT].iValue = CAN_NA_BATT_COMM_FAILURE;
		g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_EXIST_STAT].iValue = CAN_NA_BATT_NOT_EXIT;
	}
	return;
}

INT32 WaitNABattAllocation()
{
	INT32 iAddr;
	INT32 iResetingAddr = 0;
	INT32 iTimeOut = 0;
	INT32 iRst;

	for (iAddr = 0; iAddr <= 1; iAddr++)
	{
		iRst = NABatt_SampleCmd00(iAddr);
		if (CAN_NABATT_REALLOCATE == iRst)
		{	
			Set_Inventory_Updata_Flag(TRUE); //������֮ǰ��������飬֪ͨGC����Ӧ����
			iResetingAddr = iAddr;
			break;
		}
	}

	while (CAN_NABATT_REALLOCATE == NABatt_SampleCmd00(iResetingAddr))
	{
		iTimeOut++;
		Sleep(500);

		if (iTimeOut > 40)//500MS * 40 = 20S
		{
			return -1;
		}
	}

	return 0;
}
static void ClrNABattIntrruptTimes(int iAddr)
{
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_COMM_TIME].iValue = 0;
	g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_COMM_STAT].iValue 
		= CAN_NA_BATT_COMM_OK;
	return;
}

static void IncNABattIntrruptTimes(int iAddr)
{
	if (g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_COMM_TIME].iValue < NABATT_CHECK_INTERRUPT_TIMES)
	{
		g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_COMM_TIME].iValue++;
	}

	if (g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_COMM_TIME].iValue >= NABATT_CHECK_INTERRUPT_TIMES)
	{
		g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_COMM_STAT].iValue = CAN_NA_BATT_COMM_FAILURE;	
	}

	return;
}

/*********************************************************************************
*  
*  FUNCTION NAME : Is_AllNABattNoResponse
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-28 16:10:45
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
BOOL Is_AllNABattNoResponse()
{
	INT32	iAddr;

	for (iAddr = 0; iAddr < CAN_NA_BATT_MAX_NUM; iAddr++)
	{
		if(CAN_NA_BATT_EXIT == g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_EXIST_STAT].iValue)
		{
			//ֻҪ��һ��ͨ���������ͽ�����FALSE
			if (CAN_SAMPLE_OK == g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_COMM_STAT].iValue)
			{
				return FALSE;
			}
		}
	}

	return TRUE;
}

/*********************************************************************************
*  
*  FUNCTION NAME : Handle_LiBattCommFail_Clear
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-6-26 10:11:57
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Handle_LiBattCommFail_OrLost_Clear(void)
{
	DWORD dw_CommInterr = GetDwordSigValue(BATT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING, SIGID_LI_COMMINTERR_CLEAR_STATE,
		"Get Li CommFailCls Flag");
	DWORD dw_Lost = GetDwordSigValue(BATT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING, SIGID_LI_LOST_CLEAR_STATE,
		"Get Li Lost Flag");
	//ֻҪ��һ���澯��������򴥷�����
	if (dw_CommInterr == (DWORD)SIG_VALUE_CLEAR ||
		dw_Lost == (DWORD)SIG_VALUE_CLEAR)
	{
		NABatt_TriggleAllocation();
		if(dw_CommInterr == (DWORD)SIG_VALUE_CLEAR)
		{
			SetDwordSigValue(LI_BATT_GROUP_EQUIPID,
				SIG_TYPE_SETTING,SIGID_LI_CFG_NUM,101,
				"Set cfg Li num to 101");//����������ε���˲�����Lost��澯
		}
		Sleep(50);
	}
}
void  NABatt_Sample(void)
{
	int iAddr = 0;
	int iRst = -1;
	//ע�⣺�ɼ��ź�Ĭ��ֵ��0���������Handle_LiBattCommFail_Clear�Ļ��ὫLost�澯�����������Ӧ���ں������
	//Handle_LiBattCommFail_Clear(); //����û��Ƿ��ֶ����CommFail�澯�����ǣ������Reconfig
	if(g_CanNABattSamp.bNeedReconfig)
	{
		Set_LiBatt_WorkMode();

		RT_UrgencySendCurrLimit();
		//printf("\nHere runs the reconfig!!!!!!!");
		g_CanNABattSamp.bNeedReconfig = FALSE;		

		//WaitNABattAllocation();
		//ע��:������������������ʱ����ܻ�Ƚϳ�����˲���Ҫ�ȴ����ţ����Rectifierͨ��ʱ�����

		NABatt_Reconfig();

		NABatt_NoSampSigProc(); //������Ϸ�ֹreconfig�������߼�����
	
		return;
	}
	//����������ò�����Ĭ��ֵ����
	Handle_LiBattCommFail_OrLost_Clear(); //����û��Ƿ��ֶ����CommFail��Lost�澯�����ǣ������Reconfig
	if(g_CanNABattSamp.iCommCount < (UINT)(2*DELAY_COUNT_FOR_SAMPLE_PI))
	{
		g_CanNABattSamp.iCommCount++;
	}
	else
	{
		//
	}
	if(g_CanNABattSamp.iCommCount == DELAY_COUNT_FOR_SAMPLE_PI) //�ٲɼ�һ��0x20,0x10������Bridge Card 35s�Ųɼ�һ��Li���PI����������Ҫ����ϲɼ�һ��
	{
		for (iAddr = 0; iAddr < CAN_NA_BATT_MAX_NUM; iAddr++)
		{
			if (CAN_NA_BATT_EXIT == g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_EXIST_STAT].iValue)
			{
				//printf("\n*******Run here for Re-Sample Barcode NABatt!!!*******\n");
				NABatt_SampleCmd10(iAddr);
				NABatt_SampleCmd20(iAddr);
			}
		}
		NABatt_SampleCmd20(BRIDGE_CARD_ADDR);//�ٶ�ɼ�һ��Bridge Card
	}

	Set_Inventory_Updata_Flag(FALSE); //���������ɼ�״̬(No need)
	//printf("Update Flag is %d",
	//	GetEnumSigValue(LI_BATT_GROUP_EQUIPID, SIG_TYPE_SAMPLING, SIGID_LI_BATT_INVENTORY_UPDATE,"Get Li Upt Flag"));
	for (iAddr = 0; iAddr < CAN_NA_BATT_MAX_NUM; iAddr++)
	{
		if (CAN_NA_BATT_EXIT == g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_EXIST_STAT].iValue)
		{
			iRst = NABatt_SampleCmd00(iAddr);

			if (CAN_NABATT_REALLOCATE == iRst)
			{
				g_CanNABattSamp.bNeedReconfig = TRUE;
				g_CanNABattSamp.iCommCount = 1; //�����������ɨ��һ��product Info
				return;
			}
			else if (CAN_SAMPLE_OK == iRst)
			{
				ClrNABattIntrruptTimes(iAddr);
			}
			else
			{
				IncNABattIntrruptTimes(iAddr);
			}
		}
		else
		{
			//Continue!!!
		}
	}
	if (Is_AllNABattNoResponse())
	{
		g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_COMM_STAT].iValue = CAN_NA_BATT_COMM_FAILURE;
	}
	else
	{
		g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_COMM_STAT].iValue = CAN_NA_BATT_COMM_OK;
	}
	NABatt_NoSampSigProc();
}

/*********************************************************************************
*  
*  FUNCTION NAME : IS_LiBatt_Disconncted
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-1 13:48:28
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static BOOL IS_LiBatt_Disconncted(int iAddr)
{
	//3������£������Ϊ��disconnceted: A���ܳ�磻BӲ�����ϣ�C�߳������澯����
	return ( (g_CanNABattSamp.aRoughData[iAddr][ROUGH_STAT4_CHAR_EN_0X0010].iValue == FALSE)
		|| (g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT5_HARDWARE_FAULT_0X0011].iValue == TRUE)
		|| (g_CanNABattSamp.aRoughData[iAddr][ROUGH_FAULT22_HIGH_CHARGE_CURRENT_0X0011].iValue == TRUE)
		);
}
void NABatt_NoSampSigProc()
{
	int i = 0;
	
	float fTotalCurr = 0;
	float fTempTempValue =0;
	float fTempVoltValue =0;

	int NumOfComm = 0;
	int CommFailCount = 0;
	int iDisconnected_BattNum = 0;
	
	for (i = 0; i < CAN_NA_BATT_MAX_NUM; i++)
	{
		if (CAN_NA_BATT_EXIT == g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_EXIST_STAT].iValue)
		{
			//�����з��磬��ΪӦ����ͨ���жϻ��ǻ�����ܵ���
			fTotalCurr += g_CanNABattSamp.aRoughData[i][ROUGH_BATT_CURR_0X0002].fValue;
			//printf("%d) Curr: %f",i+1, fTotalCurr);
			fTempTempValue += g_CanNABattSamp.aRoughData[i][ROUGH_CELL_TEMP_0X0003].fValue;
			fTempVoltValue += g_CanNABattSamp.aRoughData[i][ROUGH_BATT_VOLT_0X0001].fValue;
			NumOfComm++;
			//��Ϊֻ����ͨ��������ʱ��Ÿ��disconn�澯���������ָ澯����
			if (CAN_NA_BATT_COMM_OK != g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_COMM_STAT].iValue)
			{
				CommFailCount++;
				g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_DISCONNECTED_STAT].iValue = CAN_NA_BATT_CONNECTED;
			}
			else
			{
				//fTotalCurr += g_CanNABattSamp.aRoughData[i][ROUGH_BATT_CURR_0X0002].fValue;
				////printf("%d) Curr: %f",i+1, fTotalCurr);
				//fTempTempValue += g_CanNABattSamp.aRoughData[i][ROUGH_CELL_TEMP_0X0003].fValue;
				//fTempVoltValue += g_CanNABattSamp.aRoughData[i][ROUGH_BATT_VOLT_0X0001].fValue;
				//NumOfComm++;
				if(IS_LiBatt_Disconncted(i))
				{
					g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_DISCONNECTED_STAT].iValue = CAN_NA_BATT_DISCONNECTED;
					iDisconnected_BattNum++;
				}
				else
				{
					g_CanNABattSamp.aRoughData[i][ROUGH_UNIT_DISCONNECTED_STAT].iValue = CAN_NA_BATT_CONNECTED;
				}
			}
		}
		
	}

	g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_DISCONNCTED_NUM].iValue = iDisconnected_BattNum;
	g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_NOREPLY_NUM].iValue = CommFailCount;

	//�ܵ�ѹ��	ƽ���¶�	ƽ����ѹ
	if (0 == NumOfComm)
	{
		g_CanNABattSamp.aRoughGroupData[ROUGH_G_TOTAL_CURRENT].fValue = 0.0;
		g_CanNABattSamp.aRoughGroupData[ROUGH_G_AVERAGE_TEMPERATURE].fValue = 0.0;
		g_CanNABattSamp.aRoughGroupData[ROUGH_G_AVERAGE_VOLT].fValue = 0.0;
	}
	else
	{
		g_CanNABattSamp.aRoughGroupData[ROUGH_G_TOTAL_CURRENT].fValue = fTotalCurr;
		g_CanNABattSamp.aRoughGroupData[ROUGH_G_AVERAGE_TEMPERATURE].fValue = fTempTempValue/NumOfComm;
		g_CanNABattSamp.aRoughGroupData[ROUGH_G_AVERAGE_VOLT].fValue = fTempVoltValue/NumOfComm;
	}
	//printf("\nTotal Curr: %f; Avg Temp: %f; Avg Voltage: %f",
	//	g_CanNABattSamp.aRoughGroupData[ROUGH_G_TOTAL_CURRENT].fValue,
	//	g_CanNABattSamp.aRoughGroupData[ROUGH_G_AVERAGE_TEMPERATURE].fValue,
	//	g_CanNABattSamp.aRoughGroupData[ROUGH_G_AVERAGE_VOLT].fValue);
	
}

void NABatt_StuffChannel(ENUMSIGNALPROC EnumProc,		//Callback function for stuffing channels
		     LPVOID lpvoid)				//Parameter of the callback function
{
	int	i,j;
	int	iAddr;

	CHANNEL_TO_ROUGH_DATA NABattGroupSig[] =
	{
		{CHNNEL_G_TOTAL_CURRENT,			ROUGH_G_TOTAL_CURRENT,			},
		{CHNNEL_G_AVERAGE_TEMPERATURE,			ROUGH_G_AVERAGE_TEMPERATURE,		},
		{CHNNEL_G_AVERAGE_VOLT,				ROUGH_G_AVERAGE_VOLT,			},
		//{CHNNEL_G_ALL_BATTERY_NO_RESPONSE,		ROUGH_G_ALL_BATTERY_NO_RESPONSE,	},
		//{CHNNEL_G_BATT_LOST,				ROUGH_G_BATT_LOST,			},
		{CHNNEL_G_BATT_INSTALL_NUM,			ROUGH_G_BATT_INSTALL_NUM,		},
		{CHNNEL_G_BATT_DISCONNCTED_NUM,			ROUGH_G_BATT_DISCONNCTED_NUM,		},
		{CHNNEL_G_BATT_NOREPLY_NUM,			ROUGH_G_BATT_NOREPLY_NUM,		},

		//{CHNNEL_GROUP_UPDATE_INVENTORY_PROCESS,		ROUGH_GROUP_UPDATE_INVENTORY_PROCESS,	},

		//{CHNNEL_BRIDGECARD_FIRMVERSION_0X0056,		ROUGH_BRIDGECARD_FIRMVERSION_0X0056,	},
		//{CHNNEL_BRIDGECARD_BARCODE_PART1_0X005A,	ROUGH_BRIDGECARD_BARCODE_PART1_0X005A,	},
		//{CHNNEL_BRIDGECARD_BARCODE_PART2_0X005B,	ROUGH_BRIDGECARD_BARCODE_PART2_0X005B,	},
		//{CHNNEL_BRIDGECARD_BARCODE_PART3_0X005C,	ROUGH_BRIDGECARD_BARCODE_PART3_0X005C,	},
		//{CHNNEL_BRIDGECARD_BARCODE_PART4_0X005D,	ROUGH_BRIDGECARD_BARCODE_PART4_0X005D,	},
		//{CHNNEL_BRIDGECARD_SN_LOW_0X0054,		ROUGH_BRIDGECARD_SN_LOW_0X0054,		},
		//{CHNNEL_BRIDGECARD_SN_HI_0X0055,		ROUGH_BRIDGECARD_SN_HI_0X0055,		},

		{CHNNEL_GROUP_COMM_STAT,			ROUGH_GROUP_COMM_STAT,			},
		{CHNNEL_GROUP_EXIST_STAT,			ROUGH_GROUP_EXIST_STAT,			},

		{CHNNEL_G_MAX_SIG_END,				CHNNEL_G_MAX_SIG_END,			},
	};	
	
	


	CHANNEL_TO_ROUGH_DATA NABattUnitSig[] =
	{
		{CHNNEL_BATT_VOLT_0X0001,				ROUGH_BATT_VOLT_0X0001,				},
		{CHNNEL_TERMINAL_VOLT_0X0001,				ROUGH_TERMINAL_VOLT_0X0001,			},

		{CHNNEL_BATT_CURR_0X0002,				ROUGH_BATT_CURR_0X0002,				},

		{CHNNEL_CELL_TEMP_0X0003,				ROUGH_CELL_TEMP_0X0003,				},
		{CHNNEL_SWITCH_TEMP_0X0003,				ROUGH_SWITCH_TEMP_0X0003,			},

		{CHNNEL_STATUS_OF_CHARGE_0X0004,			ROUGH_STATUS_OF_CHARGE_0X0004,			},

		{CHNNEL_LED_VALUES_0X0006,				ROUGH_LED_VALUES_0X0006,			},
		{CHNNEL_RELAY_VALUES_0X0006,				ROUGH_RELAY_VALUES_0X0006,			},

		//{CHNNEL_SERIAL_NO_L_OF_PART3_0X0021,			ROUGH_SERIAL_NO_L_OF_PART3_0X0021,		},
		//{CHNNEL_SERIAL_NO_L_OF_PART4_0X0021,			ROUGH_SERIAL_NO_L_OF_PART4_0X0021,		},

		{CHNNEL_STAT4_CHAR_EN_0X0010,				ROUGH_STAT4_CHAR_EN_0X0010,			},
		{CHNNEL_STAT5_DISCHAR_EN_0X0010,			ROUGH_STAT5_DISCHAR_EN_0X0010,			},
		{CHNNEL_STAT6_CHARGING_0X0010,				ROUGH_STAT6_CHARGING_0X0010,			},
		{CHNNEL_STAT7_DISCHARGING_0X0010,			ROUGH_STAT7_DISCHARGING_0X0010,			},
		{CHNNEL_STAT9_DISCHARGING_THAN_5A_0X0010,		ROUGH_STAT9_DISCHARGING_THAN_5A_0X0010,		},
		{CHNNEL_STAT11_CHARGING_THAN_5A_0X0010,			ROUGH_STAT11_CHARGING_THAN_5A_0X0010,		},
		{CHNNEL_STAT14_DISCHARGING_ENABLE_0X0010,		ROUGH_STAT14_DISCHARGING_ENABLE_0X0010,		},
		{CHNNEL_STAT15_CHARGING_ENABLE_0X0010,			ROUGH_STAT15_CHARGING_ENABLE_0X0010,		},

		{CHNNEL_FAULT2_TEMP_FAULT_0X0011,			ROUGH_FAULT2_TEMP_FAULT_0X0011,			},
		{CHNNEL_FAULT3_CURRENT_FAULT_0X0011,			ROUGH_FAULT3_CURRENT_FAULT_0X0011,		},
		{CHNNEL_FAULT5_HARDWARE_FAULT_0X0011,			ROUGH_FAULT5_HARDWARE_FAULT_0X0011,		},
		{CHNNEL_FAULT6_OVER_VOLTAGE_0X0011,			ROUGH_FAULT6_OVER_VOLTAGE_0X0011,		},
		{CHNNEL_FAULT7_LOW_VOLTAGE_0X0011,			ROUGH_FAULT7_LOW_VOLTAGE_0X0011,		},
		{CHNNEL_FAULT8_CELL_VOLT_DEVIATION_0X0011,		ROUGH_FAULT8_CELL_VOLT_DEVIATION_0X0011,	},
		{CHNNEL_FAULT11_LOW_CELL_VOLTAGE_0X0011,		ROUGH_FAULT11_LOW_CELL_VOLTAGE_0X0011,		},
		{CHNNEL_FAULT12_HIGH_CELL_VOLTAGE_0X0011,		ROUGH_FAULT12_HIGH_CELL_VOLTAGE_0X0011,		},
		{CHNNEL_FAULT13_HIGH_CELL_TEMP_0X0011,			ROUGH_FAULT13_HIGH_CELL_TEMP_0X0011,		},
		{CHNNEL_FAULT16_HIGH_SWITCH_TEMP_0X0011,		ROUGH_FAULT16_HIGH_SWITCH_TEMP_0X0011,		},
		{CHNNEL_FAULT17_CHARGE_SHORT_CIRUIT_0X0011,		ROUGH_FAULT17_CHARGE_SHORT_CIRUIT_0X0011,	},
		{CHNNEL_FAULT18_DISCHARGE_SHORT_CIRUIT_0X0011,		ROUGH_FAULT18_DISCHARGE_SHORT_CIRUIT_0X0011,	},
		{CHNNEL_FAULT19_HIGH_SWITCH_TEMP_0X0011,		ROUGH_FAULT19_HIGH_SWITCH_TEMP_0X0011,		},
		{CHNNEL_FAULT20_HARDWARE_FAILURE_0X0011,		ROUGH_FAULT20_HARDWARE_FAILURE_0X0011,		},
		{CHNNEL_FAULT21_HARDWARE_FAILURE_0X0011,		ROUGH_FAULT21_HARDWARE_FAILURE_0X0011,		},
		{CHNNEL_FAULT22_HIGH_CHARGE_CURRENT_0X0011,		ROUGH_FAULT22_HIGH_CHARGE_CURRENT_0X0011,	},
		{CHNNEL_FAULT23_HIGH_DISCHARGE_CURRENT_0X0011,		ROUGH_FAULT23_HIGH_DISCHARGE_CURRENT_0X0011,	},
		
		{CHNNEL_UNIT_DISCONNECTED_STAT,				ROUGH_UNIT_DISCONNECTED_STAT,			},

		{CHNNEL_UNIT_ADDR,					ROUGH_UNIT_ADDR,				},
		{CHNNEL_UNIT_COMM_STAT,					ROUGH_UNIT_COMM_STAT,				},
		{CHNNEL_UNIT_EXIST_STAT,				ROUGH_UNIT_EXIST_STAT,				},

		{CHNNEL_UNIT_MAX_SIG_END,				CHNNEL_UNIT_MAX_SIG_END,			},


	};	


	i = 0;
	j = 0;
	while(NABattGroupSig[i].iChannel != CHNNEL_G_MAX_SIG_END)
	{
		EnumProc(NABattGroupSig[i].iChannel + NA_BATT_G_SIG_START_CHANNEL, 
			g_CanNABattSamp.aRoughGroupData[NABattGroupSig[i].iRoughData].fValue, 
			lpvoid );
		i++;
	}

	//int iSequenceNo = 0;
	//��Ϊ�о������Զ����ţ����Ե�ַһ����������������ǰ��ֻ�����ݴ�
	//Jimmy�޸����㷨�����ﲻ��ǰ��
	for (iAddr = 0; iAddr < CAN_NA_BATT_MAX_NUM; iAddr++)
	{
		if (CAN_NA_BATT_EXIT == g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_EXIST_STAT].iValue)
		{
			i = 0;
			while(NABattUnitSig[i].iChannel != CHNNEL_UNIT_MAX_SIG_END)
			{
				EnumProc(NA_BATT_UNIT_SIG_START_CHANNEL + NABattUnitSig[i].iChannel + NA_BATT_DISTANCE * iAddr, 
					g_CanNABattSamp.aRoughData[iAddr][NABattUnitSig[i].iRoughData].fValue, 
					lpvoid );
				i++;
			}
		}
		else
		{
			i = 0;
			SAMPLING_VALUE stValue;
			stValue.iValue = CAN_SAMP_INVALID_VALUE;
			while(NABattUnitSig[i].iChannel != CHNNEL_UNIT_MAX_SIG_END)
			{
				EnumProc(NA_BATT_UNIT_SIG_START_CHANNEL + NABattUnitSig[i].iChannel + NA_BATT_DISTANCE * iAddr, 
					stValue.fValue, 
					lpvoid );
				i++;
			}
			stValue.iValue = CAN_NA_BATT_NOT_EXIT;
			EnumProc(NA_BATT_UNIT_SIG_START_CHANNEL + ROUGH_UNIT_EXIST_STAT + NA_BATT_DISTANCE * iAddr, 
				stValue.fValue, 
				lpvoid );

		}
	}

	//SAMPLING_VALUE stValue;
	//stValue.iValue = CAN_SAMP_INVALID_VALUE;

	//for (iAddr = iSequenceNo;  iAddr < CAN_NA_BATT_MAX_NUM; iAddr ++)
	//{
	//	stValue.iValue = CAN_SAMP_INVALID_VALUE;
	//	i = 0;
	//	while(NABattUnitSig[i].iChannel != CHNNEL_UNIT_MAX_SIG_END)
	//	{
	//		EnumProc(NA_BATT_UNIT_SIG_START_CHANNEL + NABattUnitSig[i].iChannel + NA_BATT_DISTANCE * iAddr, 
	//			stValue.fValue, 
	//			lpvoid );
	//		i++;
	//	}
	//	stValue.iValue = CAN_NA_BATT_NOT_EXIT;
	//	EnumProc(NA_BATT_UNIT_SIG_START_CHANNEL + ROUGH_UNIT_EXIST_STAT + NA_BATT_DISTANCE * iAddr, 
	//		stValue.fValue, 
	//		lpvoid );
	//}

}

/*********************************************************************************
*  
*  FUNCTION NAME : NABatt_GetProdctInfo
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-4-1 14:51:25
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
/*static BOOL BarCodeDataIsValid(BYTE byData)
{
	if ((0x20 != byData) && (0xff != byData))
	{
		return TRUE;
	}

	return FALSE;
}*/

/*********************************************************************************
*  
*  FUNCTION NAME : Get_BridgeCard_ProdctInfo
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-6-26 13:11:00
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void Get_BridgeCard_ProdctInfo(PRODUCT_INFO *pPI)
{
	INT32	iTemp;
	INT32	iAddr;
	BYTE	byTempBuf[8];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *pInfo;
	pInfo = (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST		 6//##
#define		BARCODE_VV_OFST			 1//##

	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");
	//����Ҫ��ʹ�޵��ҲҪ��ʾbridge card��Ϣ
	if(CAN_SAMP_INVALID_VALUE == g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART1_0X005A].iValue )
		//|| g_CanNABattSamp.aRoughGroupData[ROUGH_G_BATT_INSTALL_NUM].iValue < 1) //û�а�װ��ص�����²���ʾBridge Card��Ϣ
	{
		pInfo->bSigModelUsed = FALSE;
	}
	else
	{
		//printf("\n	Can SM Temp Samp: This is ADDR=%d  and bSigModeUsed=TRUE	\n",iAddr);
		pInfo->bSigModelUsed = TRUE;
	}

	//------------1 Serial No 

	byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_HI_0X0055].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_HI_0X0055].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_HI_0X0055].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_HI_0X0055].uiValue);

	byTempBuf[4] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_LOW_0X0054].uiValue >> 24);
	byTempBuf[5] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_LOW_0X0054].uiValue >> 16);
	byTempBuf[6] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_LOW_0X0054].uiValue >> 8);
	byTempBuf[7] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_SN_LOW_0X0054].uiValue);

	//Field,Jimmy20121015����barcode ��code1��code2������������ʹ��Field
	//iTemp = (byTempBuf[3] & 0x0f) * 4 + (byTempBuf[4] & 0xC0);
	//snprintf(pInfo->szSerialNumber + BARCODE_FF_OFST, 3, "%d%d",iTemp/10,iTemp%10);
	//��
	iTemp = (byTempBuf[2] & 0x0f) * 16 + (byTempBuf[4] & 0x0f);//year
	snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
	//��
	iTemp = byTempBuf[5] & 0x0f;
	snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/12,iTemp%12);	
	//���ŵ�λ�ĵ�16�ֽ� ��Ӧ	##### ��������������
	uiTemp = (((UINT)(byTempBuf[6])) << 8) + ((UINT)(byTempBuf[7]));
	snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,6,"%05ld",uiTemp);
	pInfo->szSerialNumber[11] = '\0';//end

	//3.�汾��VERSION=ffffd8f1
	byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_FIRMVERSION_0X0056].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_FIRMVERSION_0X0056].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_FIRMVERSION_0X0056].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_FIRMVERSION_0X0056].uiValue);

	//ģ��汾��
	//H		A00��ʽ�����ֽڰ�A=0 B=1���ƣ�����A=65
	byTempBuf[0] = 0x3f & byTempBuf[0];
	uiTemp = byTempBuf[0] + 65;
	pInfo->szHWVersion[0] = uiTemp;

	//VV 00����ʵ����ֵ������ֽ�
	snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
		"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
	pInfo->szHWVersion[4] = '\0';//end

	//����汾		��������汾Ϊ1.30,��������Ϊ�����ֽ�������130
	uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
	uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
	uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
	snprintf(pInfo->szSWVersion, 5,
		"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
	pInfo->szSWVersion[5] = '\0';//end

	//------------3 Product Model = Barcode
	byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART1_0X005A].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART1_0X005A].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART1_0X005A].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART1_0X005A].uiValue);

	//Code 1\2
	//FF ��ASCII���ʾ,ռ��CODE1 �� CODE2
	pInfo->szSerialNumber[0] = byTempBuf[0];
	pInfo->szSerialNumber[1] = byTempBuf[1];
	//ǰ�����ֽڲ���
	//pInfo->szPartNumber[0] = byTempBuf[0];//M CODE1
	//pInfo->szPartNumber[1] = byTempBuf[1];//D CODE2
	//pInfo->szPartNumber[2] = byTempBuf[2];//U CODE3
	pInfo->szPartNumber[0] = byTempBuf[3];//S CODE4


	byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART2_0X005B].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART2_0X005B].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART2_0X005B].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART2_0X005B].uiValue);

	pInfo->szPartNumber[1] = byTempBuf[0];//M CODE1
	pInfo->szPartNumber[2] = byTempBuf[1];//D CODE2
	pInfo->szPartNumber[3] = byTempBuf[2];//U CODE3
	pInfo->szPartNumber[4] = byTempBuf[3];//+ CODE17


	byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART3_0X005C].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART3_0X005C].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART3_0X005C].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART3_0X005C].uiValue);

	//NN.....NN			CODE13--CODE24
	if (BarCodeDataIsValid(byTempBuf[0]))
	{
		pInfo->szPartNumber[5] = byTempBuf[0];//CODE18
	}
	else
	{
		pInfo->szPartNumber[5] = '\0';//CODE18
	}

	if (BarCodeDataIsValid(byTempBuf[1]))
	{
		pInfo->szPartNumber[6] = byTempBuf[1];//CODE19
	}
	else
	{
		pInfo->szPartNumber[6] = '\0';//CODE19
	}

	if (BarCodeDataIsValid(byTempBuf[2]))
	{
		pInfo->szPartNumber[7] = byTempBuf[2];//CODE20
	}
	else
	{
		pInfo->szPartNumber[7] = '\0';//CODE20
	}

	if (BarCodeDataIsValid(byTempBuf[3]))
	{
		pInfo->szPartNumber[8] = byTempBuf[3];//CODE21
	}
	else
	{
		pInfo->szPartNumber[8] = '\0';//CODE21
	}


	byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART4_0X005D].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART4_0X005D].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART4_0X005D].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughGroupData[ROUGH_BRIDGECARD_BARCODE_PART4_0X005D].uiValue);


	if (BarCodeDataIsValid(byTempBuf[0]))
	{
		pInfo->szPartNumber[9] = byTempBuf[0];//CODE22
	}
	else
	{
		pInfo->szPartNumber[9] = '\0';
	}

	if (BarCodeDataIsValid(byTempBuf[1]))
	{
		pInfo->szPartNumber[10] = byTempBuf[1];//CODE23
	}
	else
	{
		pInfo->szPartNumber[10] = '\0';
	}

	if (BarCodeDataIsValid(byTempBuf[2]))
	{
		pInfo->szPartNumber[11] = byTempBuf[2];//CODE24
	}
	else
	{
		pInfo->szPartNumber[11] = '\0';
	}

	pInfo->szPartNumber[12] = '\0';//CODE25

	//printf("\nSMTEMP bSigModelUsed:%d\n", pInfo->bSigModelUsed);
	//printf("\nSMTEMP PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
	//printf("\nSMTEMP	version:%s\n", pInfo->szSWVersion); //1.20
	//printf("\nSMTEMP version:%s\n", pInfo->szHWVersion); //A00
	//printf("\nSMTEMP Serial Number:%s\n", pInfo->szSerialNumber);//03100500006



}
void NABatt_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	INT32	iTemp;
	INT32	iAddr;
	BYTE	byTempBuf[8];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *pInfo;
	pInfo = (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST		 6//##
#define		BARCODE_VV_OFST			 1//##

	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");
	//�Ȳɼ�﮵�أ��ٲɼ�Bridge Card����61����ַ
	for(iAddr = 0; iAddr < CAN_NA_BATT_MAX_NUM; iAddr++)
	{
		if(CAN_SAMP_INVALID_VALUE == g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART1_0X005A].iValue 
		  || g_CanNABattSamp.aRoughData[iAddr][ROUGH_UNIT_EXIST_STAT].iValue == CAN_NA_BATT_NOT_EXIT)
		{
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			//printf("\n	This is ADDR=%d continue	\n",iAddr);
			continue;
		}
		else
		{
			//printf("\n	Can SM Temp Samp: This is ADDR=%d  and bSigModeUsed=TRUE	\n",iAddr);
			pInfo->bSigModelUsed = TRUE;
		}

		//------------1 Serial No 

		byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART12_0X0020].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART12_0X0020].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART12_0X0020].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART12_0X0020].uiValue);

		byTempBuf[4] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART34_0X0021].uiValue >> 24);
		byTempBuf[5] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART34_0X0021].uiValue >> 16);
		byTempBuf[6] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART34_0X0021].uiValue >> 8);
		byTempBuf[7] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_SERIAL_NO_PART34_0X0021].uiValue);

		//ע�⣺����﮵���õĸ�ʽ�Ƚ����⣬��14(16)λ����ͬ��4����Uint16������
		//���ӣ�0001-2674-8075-9685,����Brian��ͨ������ȷ������15λ��
		//��һ���֣�3λ
		iTemp = (byTempBuf[0]) * 256 + (byTempBuf[1]);
		snprintf(pInfo->szSerialNumber + 0, 4, "%03d",iTemp%10000);
		//�ڶ����֣�4λ
		iTemp = (byTempBuf[2] ) * 256 + (byTempBuf[3]);
		snprintf(pInfo->szSerialNumber + 3, 5, "%04d",iTemp%10000);
		//�������֣�4λ
		iTemp = (byTempBuf[4] ) * 256 + (byTempBuf[5]);
		snprintf(pInfo->szSerialNumber + 7, 5, "%04d",iTemp%10000);
		//���Ĳ��֣�4λ
		iTemp = (byTempBuf[6] ) * 256 + (byTempBuf[7]);
		snprintf(pInfo->szSerialNumber + 11, 5, "%04d",iTemp%10000);
		pInfo->szSerialNumber[15] = '\0';//end

		//------------2 Product Revision = Manufature Date 
		byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_MANUFACTURE_DATE_0X0007].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_MANUFACTURE_DATE_0X0007].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_MANUFACTURE_DATE_0X0007].uiValue >> 8);
		
		byTempBuf[0] = byTempBuf[0] % 32;//Day
		byTempBuf[1] = byTempBuf[1] % 13;//Month
		byTempBuf[2] = byTempBuf[2] % 100;//Year

		snprintf(pInfo->szHWVersion,15,"20%02d/%02d/%02d",byTempBuf[2],byTempBuf[1],byTempBuf[0]);

		//------------3 Software Revision = Firware Info
		byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_CONTRL_FIRMWARE_NUMBER_0X0009].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_CONTRL_FIRMWARE_NUMBER_0X0009].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_CONTRL_FIRMWARE_NUMBER_0X0009].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_CONTRL_FIRMWARE_NUMBER_0X0009].uiValue);

		uiTemp = (((UINT)(byTempBuf[0])) << 8) + ((UINT)(byTempBuf[1]));//Firware Num
		uiTemp11 = (UINT)byTempBuf[2]; // Firmware Cfg
		//uiTemp12 = (UINT)byTempBuf[3]; // Firmware Rev
		char cFirmwareRev = (char)(byTempBuf[3]);
		if(cFirmwareRev > 25)
		{
			//cFirmwareRev = cFirmwareRev; //������
		}
		else
		{
			cFirmwareRev = 'A' + cFirmwareRev;
		}
		snprintf(pInfo->szSWVersion, 15,"%dZ%02d%c",uiTemp, uiTemp11,cFirmwareRev);

		//------------3 Product Model = Barcode
		byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART1_0X005A].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART1_0X005A].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART1_0X005A].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART1_0X005A].uiValue);

		//Code 1\2
		//FF ��ASCII���ʾ,ռ��CODE1 �� CODE2
		//pInfo->szSerialNumber[0] = byTempBuf[0];
		//pInfo->szSerialNumber[1] = byTempBuf[1];
		pInfo->szPartNumber[0] = byTempBuf[0];//M CODE1
		pInfo->szPartNumber[1] = byTempBuf[1];//D CODE2
		pInfo->szPartNumber[2] = byTempBuf[2];//U CODE3
		pInfo->szPartNumber[3] = byTempBuf[3];//S CODE4

	
		byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART2_0X005B].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART2_0X005B].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART2_0X005B].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART2_0X005B].uiValue);
		
		pInfo->szPartNumber[4] = byTempBuf[0];//M CODE1
		pInfo->szPartNumber[5] = byTempBuf[1];//D CODE2
		pInfo->szPartNumber[6] = byTempBuf[2];//U CODE3

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[7] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';
		}		

		byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART3_0X005C].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART3_0X005C].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART3_0X005C].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART3_0X005C].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[8] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[9] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[10] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[11] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';//CODE21
		}


		byTempBuf[0] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART4_0X005D].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART4_0X005D].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART4_0X005D].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanNABattSamp.aRoughData[iAddr][ROUGH_BARCODE_PART4_0X005D].uiValue);

	
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[12] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[13] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[13] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[14] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[14] = '\0';
		}

		pInfo->szPartNumber[15] = '\0';//CODE25

		//printf("\nSMTEMP bSigModelUsed:%d\n", pInfo->bSigModelUsed);
		//printf("\nSMTEMP PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
		//printf("\nSMTEMP	version:%s\n", pInfo->szSWVersion); //1.20
		//printf("\nSMTEMP version:%s\n", pInfo->szHWVersion); //A00
		//printf("\nSMTEMP Serial Number:%s\n", pInfo->szSerialNumber);//03100500006

		pInfo++;
	}
	//printf("\n	########		END		Convert_	GetProdctInfo	########	\n");
	Get_BridgeCard_ProdctInfo(pInfo);
}



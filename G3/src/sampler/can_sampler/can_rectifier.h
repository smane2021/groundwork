/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : can_rectifier.h
*  CREATOR  : Frank Cao                DATE: 2008-06-24 16:47
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/


#ifndef __CAN_SAMPLER_RECTIFIER_H
#define __CAN_SAMPLER_RECTIFIER_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "can_sampler_main.h"

#define CFGGOING	0
#define CFGOVER		1

#define DC_TYPE_SAMP_SINGLE				0
#define DC_TYPE_SAMP_SMALL				1
#define DC_TYPE_SAMP_DOUBLE				2

#define DC_TYPE_RETURN_SINGLE			1
#define DC_TYPE_RETURN_SMALL			2
#define DC_TYPE_RETURN_DOUBLE			0

#define	AC_INPUT_1_PHASE				0
#define	AC_INPUT_3_PHASE				1


#define	RT_COMM_NORMAL_ST				0
#define	RT_COMM_ALL_INTERRUPT_ST		1
#define	RT_COMM_INTERRUPT_ST			2


#define RT_EQUIP_EXISTENT				0
#define RT_EQUIP_NOT_EXISTENT			1

#define RT_MAX_INTERRUPT_TIMES			2

#define RT_CMD00_RCV_FRAMES_NUM			9
#define RT_CMD10_RCV_FRAMES_NUM			4
#define RT_CMD20_RCV_FRAMES_NUM			11
#define RT_CMD30_RCV_FRAMES_NUM			9
#define RT_CMD30_RCV_FRAMES_NUM_SPECIAL	 3

//Old rectidiers return 6 or 7 frames
#define RT_CMD20_RCV_FRAMES_NUM_SPECIAL	6


#define RT_SINGLE_SUPPLY				0
#define RT_SMALL_SUPPLY					1
#define RT_DOUBLE_SUPPLY				2

#define RT_SINGLE_PHASE					0
#define RT_THREE_PHASE					1

#define	BT_GROUP_EQUIPID				115
#define	BT_BATT_DISCH_SIGID				32
#define	RT_GROUP_EQUIPID				2
#define RT_START_EQUIP_ID				3
#define RT_START_EQUIP_ID2				1601
#define RT_POS_SET_SIG_ID				1
#define RT_PHASE_SET_SIG_ID				2
#define RT_CONFIRM_POS_PHASE_SIGID		21
#define RT_FLOAT_VOLT_SIGID				16
#define RT_OVER_VOLTAGE_SIGID			3

//2012��6��12�գ����ڲ���MPPT��ʱ���ڵ������޷��������ͣ�磬����YGXINҪ������MPPTģʽ���жϡ�
#define  RT_SYSTEM_EQUIP_ID				1
#define  RT_MPPT_MOD_SIG_ID				446
#define  RT_MMPPT_MODE_DISABLE				0

//2018-11-22
#define RT_OLD_FIRMWARE_SAM_SIGID			42


#define RT_SEQ_START_INTERVAL_SIGID		31
#define RT_OV_RESTART_ENB_SIGID			30
#define RT_OV_RESTART_INTERVAL_SIGID	6
#define RT_WALKIN_TIME_SIGID			7
#define RT_WALKIN_ENB_SIGID				8
#define RT_AC_OV_RESTART_ENB_SIGID		19
#define RT_FULL_POWER_ENB_SIGID			38
#define RT_AB_POWER_LIMIT_MODE_SIGID			51
//changed by Frank Wu,13/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
#define RT_HVDC_UPDATE				54
#define RT_HVDC_FORCEUPDATE			55

#define RT_SAMP_UpLoadOK_Number			39
#define RT_SAMP_UpLoadOK_State			40
#endif//GC_SUPPORT_RECT_DLOAD

#define	RT_DC_ON						0
#define	RT_DC_OFF						1
#define	RT_RESET_NORMAL					0
#define	RT_RESET_ENB					1
#define	RT_WALKIN_NORMAL				0
#define	RT_WALKIN_ENB					1
#define	RT_FAN_AUTO						0
#define	RT_FAN_FULL_SPEED				1
#define	RT_LED_NORMAL					0
#define	RT_LED_BLINK					1
#define	RT_AC_ON						0
#define	RT_AC_OFF						1
#define	RT_CAN_NORMAL					0
#define	RT_CAN_INIT						1
#define	RT_OV_RESET_DISB				0
#define	RT_OV_RESET_ENB					1
#define	RT_LED_NOT_BLINK				0
#define	RT_LED_BLINK					1
#define	RT_AC_OV_PROTECT_ENB			0
#define	RT_AC_OV_PROTECT_DISB			1
#define	RT_DEGRADATIVE					0
#define	RT_FULL_POWER_CAP_ENB			1
#define	RT_POWER_LIMIT_A			0
#define	RT_POWER_LIMIT_B			1


#define	RECT_POSITION_INVAILID	(-1)
#define	AC_PHASE_INVAILID		(-1)

#define	RT_AC_NORMAL		0
#define	RT_AC_LOW_VOLT		1
#define	RT_AC_FAILURE		2
#define	RT_AC_FAIL_POINT	(80.0)

#define CAN2_OFFSET		18000
#define SYS_CONVERTER_ONLY_ID		451



SAMPLING_VALUE		RectCAN2[MAX_NUM_RECT/2][RECT_MAX_SIGNALS_NUM];
int	iLastTime;

enum RECT_EFFICIENCY_NO_DEF
{
	RT_EFFICIENCY_LT93 = 0,
	RT_EFFICIENCY_GT93,
	RT_EFFICIENCY_GT95,
	RT_EFFICIENCY_GT96,
	RT_EFFICIENCY_GT97,
	RT_EFFICIENCY_GT98,
	RT_EFFICIENCY_GT99,

	RT_EFFICIENCY_MAX_NUM,
};


enum AC_PHASE_DEF
{
	RT_AC_PHASE_A = 0,
	RT_AC_PHASE_B,
	RT_AC_PHASE_C,

	RT_AC_PHASE_NUM,
};

//The channel No. is defined in "Design for NGMC-HF Software �C CAN Port Sampler Module"
enum RECT_SAMP_CHANNEL
{
	SYS_CH_CAN_INTERRUPT = 20,	//CAN all no response
	SYS_CH_CAN_CIRTIME,

	RT_CH_RECT_NUM = 50,		//Total Rectifier Number
	RT_CH_RATED_VOLT,			//Rated Voltage
	RT_CH_RATED_CURR,			//Rated Current
	RT_CH_AC_P_A_VOLT,			//AC Phase A Voltage
	RT_CH_AC_P_B_VOLT,			//AC Phase B Voltage
	RT_CH_AC_P_C_VOLT,			//AC Phase C Voltage
	RT_CH_AC_P_AB_VOLT,			//AC Phase AB Voltage
	RT_CH_AC_P_BC_VOLT,			//AC Phase BC Voltage
	RT_CH_AC_P_CA_VOLT,			//AC Phase CA Voltage
	RT_CH_DC_DC_TYPE,			//Monitoring Board Power Supply Type
	RT_CH_AC_PHASE_TYPE,		//AC 1 or 3 Phase
	RT_CH_ALL_AC_FAIL,			//All rectifers AC failure
	RT_CH_ALL_NO_RESPONSE,		//All rectifers no response
	RT_CH_GROUP_RESERVE,				//No used
	RT_CH_SNMP_A_AB_VOLT,		//AC Phase A/AB Voltage for SNMP
	RT_CH_SNMP_B_BC_VOLT,		//AC Phase B/BC Voltage for SNMP
	RT_CH_SNMP_C_CA_VOLT,		//AC Phase C/CA Voltage for SNMP
	RT_CH_AC_RATED_VOLT,		//AC input Rated Voltage

	RT_CH_OLD_SW_REC_CAN1=70,	//2018-11-23,Old firmware on CAN1 is Exist
	RT_CH_OLD_SW_REC_CAN2,
	RT_CH_COMM_REC_ON_CAN1,			
	RT_CH_COMM_REC_ON_CAN2,				

	RT_CH_INPUT_PHASEA_CURR,
	RT_CH_INPUT_PHASEB_CURR,
	RT_CH_INPUT_PHASEC_CURR,

	RT_CH_DC_VOLT = 101,		//Output Voltage
	RT_CH_ACTUAL_CURR,			//Actual Current
	RT_CH_CURR_LMT,				//Current Limitation
	RT_CH_TEMPERATURE,			//Temperature
	RT_CH_AC_VOLT,				//AC Input Voltage
	RT_CH_HIGH_VOLT,			//Over Voltage Limit
	RT_CH_DISP_CURR,			//Current for display
	RT_CH_SERIAL_NO,			//Serial No.
	RT_CH_HIGH_SERIAL_NO,		//High Serial No.
	RT_CH_INTERRUPT_ST,			//Interrupt State
	RT_CH_AC_FAIL_ST,			//AC Failure 
	RT_CH_OVER_VOLT_ST,			//DC Over Voltage Status
	RT_CH_FAULT_ST,				//Fault Status
	RT_CH_PROTECTION_ST,		//Protection Status
	RT_CH_FAN_FAULT_ST,			//Fan Fault Status
	RT_CH_EEPROM_FAULT_ST,		//EEprom Fault Status
	RT_CH_VALID_RATED_CURR,		//Valid rated current, before it is "Power limited for AC"
	RT_CH_EFFICIENCY_NO_ST,		//Rectifer Efficiency, before it is "Power limited for temperature"
	RT_CH_POWER_LMT_ST,			//Power limited
	RT_CH_DC_ON_OFF_ST,			//DC On/off status
	RT_CH_FAN_FULL_SPEED_ST,	//Fan full speed status
	RT_CH_WALKIN_ST,			//Walk-in status
	RT_CH_AC_ON_OFF_ST,			//AC on/off status
	RT_CH_SHARE_CURR_ST,		//Share current status
	RT_CH_OVER_TEMP_ST,			//Over temperature Status
	RT_CH_VER_NO,				//Version
	RT_CH_RUN_TIME,				//Total Run Time
	RT_CH_SEVERE_CURR_IMB,		//Severe current imbalance
	RT_CH_PHASE_AB_A_VOLT,		//AB/A Phase voltage
	RT_CH_PHASE_BC_B_VOLT,		//BC/B Phase voltage
	RT_CH_PHASE_CA_C_VOLT,		//CA/C Phase voltage
	RT_CH_POS_NO,				//Position No.
	RT_CH_PHASE_NO,				//Phase No.
	RT_CH_BARCODE1,				//Barcode1
	RT_CH_BARCODE2,				//Barcode2
	RT_CH_BARCODE3,				//Barcode3
	RT_CH_BARCODE4,				//Barcode4
	RT_CH_RESERVE,				//Before it was E-Stop State
	RT_CH_EXIST_STATE,
	RT_CH_CURR_IMB,
	RT_CH_PHA_CURR = RECT_ENLARGE_SIG_START_CHANNEL,
	RT_CH_PHB_CURR,
	RT_CH_PHC_CURR,

};


//Please refer to the document of CAN protocol
//Following is the data to be read from rectifier
/////////////////////////////////////////////
#define RT_VAL_TYPE_R_DC_VOLT			0x01
#define RT_VAL_TYPE_R_REAL_CURR			0x02
#define RT_VAL_TYPE_R_CURR_LMT			0x03
#define RT_VAL_TYPE_R_TEMPERATURE		0x04
#define RT_VAL_TYPE_R_AC_VOLT			0x05
#define RT_VAL_TYPE_R_HIGH_VOLT			0x06
#define RT_VAL_TYPE_R_DISP_CURR			0x07
#define RT_VAL_TYPE_R_PFC0_VOLT			0x08
#define RT_VAL_TYPE_R_HIGH_TEMP			0x09
#define RT_VAL_TYPE_R_PFC1_VOLT			0x0A
#define RT_VAL_TYPE_R_U1BOARD_TEMP		0x0B
#define RT_VAL_TYPE_R_P_A_VOLT			0x0C
#define RT_VAL_TYPE_R_P_B_VOLT			0x0D
#define RT_VAL_TYPE_R_P_C_VOLT			0x0E
#define RT_VAL_TYPE_R_RATED_VOLT		0x0F
#define RT_VAL_TYPE_R_PFC_TEMP			0x10
#define RT_VAL_TYPE_R_RATED_POWER		0x11
#define RT_VAL_TYPE_R_RATED_CURR		0x12
#define RT_VAL_TYPE_R_RATED_INPUT_VOLT	0x13
#define RT_VAL_TYPE_R_POWER_LMT_POINT	0x14

#define RT_VAL_TYPE_R_ALARM_BITS		0x40
#define RT_VAL_TYPE_R_ANALOG_ALARM		0x41
#define RT_VAL_TYPE_R_OTHER_ALARM		0x42

#define RT_VAL_TYPE_R_FEATURE			0x51

#define RT_VAL_TYPE_R_SN_LOW			0x54
#define RT_VAL_TYPE_R_SN_HIGH			0x55
#define RT_VAL_TYPE_R_VER_NO			0x56

#define RT_VAL_TYPE_R_RUN_TIME			0x58

#define RT_VAL_TYPE_R_BARCODE1			0x5A
#define RT_VAL_TYPE_R_BARCODE2			0x5B
#define RT_VAL_TYPE_R_BARCODE3			0x5C
#define RT_VAL_TYPE_R_BARCODE4			0x5D
////////////////////////////////////////////

//Following is the data to be wrote to rectifier
#define RT_VAL_TYPE_W_ESTOP_ENB			0x16
#define RT_VAL_TYPE_W_CURR_LMT			0x22
#define RT_VAL_TYPE_W_HIGH_VOLT			0x23
#define RT_VAL_TYPE_W_DC_VOLT			0x21
#define RT_VAL_TYPE_W_DEFAULT_VOLT		0x24
//#define	RT_VAL_TYPE_W_FULL_POWER_ENB	0x27		//Error!! It should be 0x2F
#define RT_VAL_TYPE_W_RESTART_TIME		0x28	//restart time on over voltage
#define RT_VAL_TYPE_W_WALKIN_TIME		0x29	//the time of soft time with load
#define RT_VAL_TYPE_W_SEQ_START_TIME	0x2A	//Sequence start interval
#define	RT_VAL_TYPE_W_FULL_POWER_ENB	0x2F
#define RT_VAL_TYPE_W_DC_ON_OFF			0x30
#define RT_VAL_TYPE_W_RESTART_ON_OV		0x31
#define RT_VAL_TYPE_W_WALKIN_ENB		0x32
#define RT_VAL_TYPE_W_FAN_FULL_SPEED	0x33
#define RT_VAL_TYPE_W_LED_BLINK			0x34
#define RT_VAL_TYPE_W_AC_ON_OFF			0x35
#define RT_VAL_TYPE_W_RUN_TIME			0xA4
#define RT_VAL_TYPE_W_AC_CURR_LIMIT		0x1a	//Input current limit

#define RT_VAL_TYPE_R_AC_PHA_CURR		0xD0	//Input current 
#define RT_VAL_TYPE_R_AC_PHB_CURR		0xD1	//Input current 
#define RT_VAL_TYPE_R_AC_PHC_CURR		0xD2	//Input current 
#define RT_VAL_TYPE_NO_TRIM_VOLT		0x80	//used while MsgType = 0x00, 0x10, 0x20


struct	tagRECT_TYPE_INFO
{
	UINT		uiTypeNo;
	float		fRatedVolt;
	float		fRatedCurr;
	int			iDcDcType;
	int			iAcInputType;
	float		fAcRatedVolt;
	float		fPower;
	int			iEfficiencyNo;
};
typedef struct tagRECT_TYPE_INFO RECT_TYPE_INFO;

struct	tagRECT_POS_ADDR
{
	DWORD			dwSerialNo;//��Barcode1����
	DWORD			dwHiSN; 
	int			iPositionNo;
	int			iPhaseNo;
	BOOL			bIsNewInserted; //�Ƿ��²����
	int			iSeqNo; //���ź��λ��
};
typedef struct tagRECT_POS_ADDR RECT_POS_ADDR;

void RT_Reconfig(void);
void RT_TriggerAllocation(void);
void RT_InitPosAndPhase(void);
void RT_InitRoughValue(void);
void RT_SendCtlCmd(int iChannelNo, float fParam);
void RT_Sample(void);
void For80RecAlarmHandle();
void RT_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
		     LPVOID lpvoid);			//Parameter of the callback function
BOOL RectIsNeedReConfig();
BOOL RectIsNeedReConfigWhenACFail();
INT32 WaitAllocation();
VOID Rect_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI);
#endif

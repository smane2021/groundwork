

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "can_sampler_main.h"
#include "can_smdu.h"
static int GetSmSeqNoByAddr(int iAddr);

extern CAN_SAMPLER_DATA	g_CanData;

#define SM_LOAD_SHUNT	1
#define SM_BATTERY_SHUNT	0
#define SM_NO_SHUNT	2
int	iShunt5Type[MAX_NUM_SMDU] = {SM_BATTERY_SHUNT};//1 for load

BYTE	bySwitchRough;


#define	SM_LVD_CTL_END_MASK				(0x08)
#define	SM_COM_LED_BLINK_MASK			(0x04)
#define SM_LVD2_MASK					(0x02)
#define SM_LVD1_MASK					(0x01)

BOOL	bSMDUSwitchStatus[8][5];
//added by Jimmy for SMDU-EIB Shunt Setting CR 2013.07.06
static BOOL g_bIsSMDUEIBMode = FALSE;
static BOOL Is_SMDU_EIBMode(void);
static float g_fSMDUShuntReadings[8][5];
static void ReDispatchShuntReadings(void);
static void ClrCurrWhenNoSamp(int iAddr);//added by Jimmy 2013.11.20
static void UnpackSmduAlarm43(int iAddr, BYTE* pbyAlmBuff);

#define	EQUIP_ID_SYS_GROUP		1
#define	EQUIP_ID_DC_UNIT		176
#define	EQUIP_ID_BATT_GROUP		115
#define	EQUIP_ID_SMDU_UNIT		107
#define	EQUIP_ID_SMDU_BATT1		126
#define	EQUIP_ID_SMDU_BATT2		127
#define	EQUIP_ID_SMDU_BATT3		128
#define	EQUIP_ID_SMDU_BATT4		129
#define	EQUIP_ID_SMDU_BATT5		659	//added by Jimmy 20110624
#define	EQUIP_ID_SMDU_LVD		188

#define SIGNAL_ID_SMDU_SHUNT_TYPE	16

static void ClrCurrWhenNoSamp(int iAddr)
{
	typedef struct stClrCurr
	{
		int iCurrRoughID;
		int iShuntCurrID;
		int iShuntVoltID;
	}CLR_CURR;
	CLR_CURR ListCurr[] = {
		{SMDU_LOAD_CURR1,SMDU_SHUNT1_A,SMDU_SHUNT1_MV,},
		{SMDU_LOAD_CURR2,SMDU_SHUNT2_A,SMDU_SHUNT2_MV,},
		{SMDU_LOAD_CURR3,SMDU_SHUNT3_A,SMDU_SHUNT3_MV,},
		{SMDU_LOAD_CURR4,SMDU_SHUNT4_A,SMDU_SHUNT4_MV,},
		{SMDU_LOAD_CURR5,SMDU_SHUNT5_A,SMDU_SHUNT5_MV,},

		{SMDU_BATT_CURR1,SMDU_SHUNT1_A,SMDU_SHUNT1_MV,},
		{SMDU_BATT_CURR2,SMDU_SHUNT2_A,SMDU_SHUNT2_MV,},
		{SMDU_BATT_CURR3,SMDU_SHUNT3_A,SMDU_SHUNT3_MV,},
		{SMDU_BATT_CURR4,SMDU_SHUNT4_A,SMDU_SHUNT4_MV,},
		{SMDU_BATT_CURR5,SMDU_SHUNT5_A,SMDU_SHUNT5_MV,},
		{-1,-1,-1,},
	};
	int i = 0;
	while(ListCurr[i].iCurrRoughID != -1)
	{
		if(g_CanData.aRoughDataSmdu[iAddr][ListCurr[i].iCurrRoughID].iValue != CAN_SAMP_INVALID_VALUE)
		{
			if(fabs(g_CanData.aRoughDataSmdu[iAddr][ListCurr[i].iCurrRoughID].fValue) < 0.0015
				* g_CanData.aRoughDataSmdu[iAddr][ListCurr[i].iShuntCurrID].fValue)
			{
				g_CanData.aRoughDataSmdu[iAddr][ListCurr[i].iCurrRoughID].fValue = 0.0;
			}
		}
		i++;
	}
}
/*==========================================================================*
* FUNCTION : GetSmSetByteCmd00
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static BYTE : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:32
*==========================================================================*/
static BYTE GetSmSetByteCmd00(int iAddr)
{
	BYTE			byOutput = 0;

	SMDU_SET_SIG*	pSetSig 
		= &(g_CanData.CanCommInfo.SmduCommInfo.aSmduSetSig[iAddr]);

	if(pSetSig->dwLvdCtlEnabled)
	{		
		byOutput |= SM_LVD_CTL_END_MASK;	
	}
	else
	{
		byOutput &= ~SM_LVD_CTL_END_MASK;	
	}

	if(pSetSig->dwComLedBlink)
	{
		byOutput |= SM_COM_LED_BLINK_MASK;	
	}	
	else
	{
		byOutput &= ~SM_COM_LED_BLINK_MASK;
	}

	if(pSetSig->dwLvd1Ctl)
	{
		byOutput |= SM_LVD1_MASK;
	}
	else
	{
		byOutput &= ~SM_LVD1_MASK;
	}

	if(pSetSig->dwLvd2Ctl)
	{
		byOutput |= SM_LVD2_MASK;
	}
	else
	{
		byOutput &= ~SM_LVD2_MASK;
	}

	return byOutput;	
}


#define SMDU_NOUSED				-1

/*==========================================================================*
* FUNCTION : UnpackSmduAlarm1
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr      : 
*            BYTE*  pbyAlmBuff : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
*==========================================================================*/
static void UnpackSmduAlarm1(int iAddr, BYTE* pbyAlmBuff)
{
	//The order is based on the protocol, do not change it.
	int		aiSigIdx[BYTES_OF_ALM_INFO * BITS_PER_BYTE] = 
	{
		SMDU_LOAD_FUSE16,
		SMDU_LOAD_FUSE15,
		SMDU_LOAD_FUSE14,
		SMDU_LOAD_FUSE13,
		SMDU_LOAD_FUSE12,
		SMDU_LOAD_FUSE11,
		SMDU_LOAD_FUSE10,
		SMDU_LOAD_FUSE9,

		SMDU_LOAD_FUSE8,
		SMDU_LOAD_FUSE7,
		SMDU_LOAD_FUSE6,
		SMDU_LOAD_FUSE5,
		SMDU_LOAD_FUSE4,
		SMDU_LOAD_FUSE3,
		SMDU_LOAD_FUSE2,
		SMDU_LOAD_FUSE1,

		SMDU_BATT_FUSE2,
		SMDU_BATT_FUSE1,
		SMDU_BUS_VOLT_ALM,
		SMDU_LVD2_CMD_STATUS,
		SMDU_LVD1_CMD_STATUS,		
		SMDU_NOUSED,
		SMDU_LVD2_FAULT_STATUS,
		SMDU_LVD1_FAULT_STATUS,


		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_BATT4_CURR_ALM,
		SMDU_BATT3_CURR_ALM,
		SMDU_BATT2_CURR_ALM,
		SMDU_BATT1_CURR_ALM,
		SMDU_BATT_FUSE4,
		SMDU_BATT_FUSE3,
	};

	int		i, j;

	for(i = 0; i < BYTES_OF_ALM_INFO; i++)
	{
		BYTE	byMask = 0x80;
		BYTE	byRough = *(pbyAlmBuff + i);

		for(j = 0; j < BITS_PER_BYTE; j++)
		{
			if(aiSigIdx[i * BITS_PER_BYTE + j] != SMDU_NOUSED)
			{
				int		iIdx = aiSigIdx[i * BITS_PER_BYTE + j];
				g_CanData.aRoughDataSmdu[iAddr][iIdx].iValue
					= (byRough & byMask) ? 1 : 0;	
			}
			byMask >>= 1;
		}
	}

	//Sigle 
	/*if(g_CanData.aRoughDataSmdu[iAddr][SMDU_CONTACTOR_TYPE].iValue
	== CONTACTOR_TYPE_SINGLE)
	{
	g_CanData.aRoughDataSmdu[iAddr][SMDU_LVD2_FAULT_STATUS].iValue
	= g_CanData.aRoughDataSmdu[iAddr][SMDU_LVD2_CMD_STATUS].iValue;
	g_CanData.aRoughDataSmdu[iAddr][SMDU_LVD1_FAULT_STATUS].iValue
	= g_CanData.aRoughDataSmdu[iAddr][SMDU_LVD1_CMD_STATUS].iValue;

	}*/

	return;
}


/*==========================================================================*
* FUNCTION : UnpackSmduAlarm2
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr      : 
*            BYTE*  pbyAlmBuff : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:31
*==========================================================================*/
static void UnpackSmduAlarm2(int iAddr, BYTE* pbyAlmBuff)
{
	//The order is based on the protocol, do not change it.
	int		aiStSigIdx[BYTES_OF_ALM_INFO] = 
	{
		SMDU_BATT1_CURR_ST,
		SMDU_BATT2_CURR_ST,
		SMDU_BATT3_CURR_ST,
		SMDU_BATT4_CURR_ST,	
	};

	int		aiAlmSigIdx[BYTES_OF_ALM_INFO] = 
	{
		SMDU_BATT1_CURR_ALM,
		SMDU_BATT2_CURR_ALM,
		SMDU_BATT3_CURR_ALM,
		SMDU_BATT4_CURR_ALM,	
	};

	int		i;

	for(i = 0; i < BYTES_OF_ALM_INFO; i++)
	{
		BYTE	byRough = *(pbyAlmBuff + i);
		int		iStIdx = aiStSigIdx[i];
		int		iAlmIdx = aiAlmSigIdx[i];

		if(g_CanData.aRoughDataSmdu[iAddr][iAlmIdx].iValue)
		{
			g_CanData.aRoughDataSmdu[iAddr][iStIdx].iValue = (int)byRough;
		}
		else
		{
			g_CanData.aRoughDataSmdu[iAddr][iStIdx].iValue = 0;		
		}
	}

	return;
}

#define CONTACTOR_TYPE_MASK			0x02

#define BATT1_EXIST_MASK			0x02
#define BATT2_EXIST_MASK			0x08
#define BATT3_EXIST_MASK			0x20
#define BATT4_EXIST_MASK			0x80
#define LOAD1_EXIST_MASK			0x01
#define LOAD2_EXIST_MASK			0x04
#define LOAD3_EXIST_MASK			0x10
#define LOAD4_EXIST_MASK			0x40


enum	SM_42VAL_BYTE_ORDER
{
	SM_42VAL_BYTE1 = 0,
	SM_42VAL_BYTE2,
	SM_42VAL_BYTE3,
	SM_42VAL_BYTE4,

	SM_42VAL_BYTE_NUM,
};
/*==========================================================================*
* FUNCTION : UnpackSmduAlarm3
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr      : 
*            BYTE*  pbyAlmBuff : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:31
*==========================================================================*/
static void UnpackSmduAlarm3(int iAddr, BYTE* pbyAlmBuff)
{
	int		i, j;
	//struct	tagMASK_SIGNAL_MAP
	//{
	//	UINT	uiMask;
	//	int		iRoughDataIdxExistSig;
	//	int		iRoughDataIdxCurrSig;
	//};

	//typedef struct tagMASK_SIGNAL_MAP MASK_SIGNAL_MAP;
	static	BOOL		s_bDetectDIPChange	= FALSE;
	static	DWORD		s_dwChangeForWeb = 0;

	if (s_bDetectDIPChange && (!g_bIsSMDUEIBMode))
	{
		s_bDetectDIPChange = FALSE; 
		
		//for LCD
		SetDwordSigValue(SMDU_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				CFG_CHANGED_SIG_ID,
				1,
				"CAN_SAMP");

		//for Web
		s_dwChangeForWeb++;
		if(s_dwChangeForWeb >= 255)
		{
			s_dwChangeForWeb = 0;
		}
		else
		{
			//
		}

		SetDwordSigValue(SMDU_GROUP_EQUIPID, 
			SIG_TYPE_SAMPLING,
			CFG_CHANGED_SIG_ID + 1,
			s_dwChangeForWeb,
			"CAN_SAMP");

	}
	else
	{
		//
	}

	MASK_SIGNAL_MAP	aBattMaskSignal[4] = 
	{
		{BATT1_EXIST_MASK, SMDU_BATT1_EXIST_ST, SMDU_BATT_CURR1},
		{BATT2_EXIST_MASK, SMDU_BATT2_EXIST_ST, SMDU_BATT_CURR2},
		{BATT3_EXIST_MASK, SMDU_BATT3_EXIST_ST, SMDU_BATT_CURR3},
		{BATT4_EXIST_MASK, SMDU_BATT4_EXIST_ST, SMDU_BATT_CURR4},	
	};

	MASK_SIGNAL_MAP	aLoadMaskSignal[4] = 
	{
		{LOAD1_EXIST_MASK, 0, SMDU_LOAD_CURR1},
		{LOAD2_EXIST_MASK, 0, SMDU_LOAD_CURR2},
		{LOAD3_EXIST_MASK, 0, SMDU_LOAD_CURR3},
		{LOAD4_EXIST_MASK, 0, SMDU_LOAD_CURR4},	
	};

	static BYTE	s_abyToggleSwitchState[MAX_NUM_SMDU * 3] = {0};
	for(i = 0; i < SM_42VAL_BYTE_NUM; i++)
	{
		BYTE	byRough = *(pbyAlmBuff + i);

		if(i == SM_42VAL_BYTE1)
		{
			if(g_CanData.aRoughDataSmdu[iAddr][SMDU_BUS_VOLT_ALM].iValue)
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BUS_VOLT_ST].iValue
					= (int)byRough;
			}
			else
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BUS_VOLT_ST].iValue = 0;

			}
		}
		else if(i == SM_42VAL_BYTE2)
		{
			//No use here
		}
		else if(i == SM_42VAL_BYTE3)
		{
			for(j = 0; j < 4; j++)
			{
				int	iIdxExsitSig = aBattMaskSignal[j].iRoughDataIdxExistSig;
				int	iIdxCurrSig = aBattMaskSignal[j].iRoughDataIdxCurrSig;

				if( ((byRough>>(2*j)) & 0x02)==0 && ((byRough>>(2*j)) & 0x01)==1)
				{
					bSMDUSwitchStatus[iAddr][j] = FALSE;
					g_CanData.aRoughDataSmdu[iAddr][iIdxExsitSig].iValue 
						= SMDU_EQUIP_EXISTENT;
				}
				else
				{
					g_CanData.aRoughDataSmdu[iAddr][iIdxExsitSig].iValue 
						= SMDU_EQUIP_NOT_EXISTENT;
					g_CanData.aRoughDataSmdu[iAddr][iIdxCurrSig].iValue = CAN_SAMP_INVALID_VALUE;

					if( ((byRough>>(2*j)) & 0x02)==0 && ((byRough>>(2*j)) & 0x01)==0)
					{
						bSMDUSwitchStatus[iAddr][j] = TRUE;
					}
					else
					{
						bSMDUSwitchStatus[iAddr][j] = FALSE;
					}


				}
			}

			for(j = 0; j < 4; j++)
			{
				int	iIdxCurrSig = aLoadMaskSignal[j].iRoughDataIdxCurrSig;

				if(((byRough>>(2*j)) & 0x01)!=0 && ((byRough>>(2*j)) & 0x02)!=1)
				{
					g_CanData.aRoughDataSmdu[iAddr][iIdxCurrSig].iValue = CAN_SAMP_INVALID_VALUE;
				}
			}
		}
		else if(i == SM_42VAL_BYTE4)
		{
			g_CanData.aRoughDataSmdu[iAddr][SMDU_CONTACTOR_TYPE].iValue 
				= (byRough & CONTACTOR_TYPE_MASK) 
				? CONTACTOR_TYPE_DOUBLE : CONTACTOR_TYPE_SINGLE;	


			//2010/06/09	分流器5由拨码开关3的   5~6分流器5设置，见总体技术方案
			if ((0x03 & (byRough >> 4)) == 0x03)//分流器不用
			{
				bSMDUSwitchStatus[iAddr][4] = FALSE;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
					= SMDU_EQUIP_NOT_EXISTENT;
				//g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_MV].iValue 
				//	= CAN_SAMP_INVALID_VALUE;
				//g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_A].iValue 
				//	= CAN_SAMP_INVALID_VALUE;
				//g_CanData.aRoughDataSmdu[iAddr][SMDU_OVER_CURR5].iValue 
				//	= CAN_SAMP_INVALID_VALUE;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].iValue 
					= CAN_SAMP_INVALID_VALUE;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
					= SMDU_EQUIP_NOT_EXISTENT;

				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR5].iValue 
					= CAN_SAMP_INVALID_VALUE;
				iShunt5Type[iAddr] = SM_NO_SHUNT;

				//printf("\n $$$$$$$$$$$$$$  11111111111\n");
			}
			else if ((0x03 & (byRough >> 4)) == 0x01)//分流器5用于电池
			{
				bSMDUSwitchStatus[iAddr][4] = FALSE;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
					= SMDU_EQUIP_EXISTENT;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].iValue 
					= CAN_SAMP_INVALID_VALUE;
				iShunt5Type[iAddr] = SM_BATTERY_SHUNT;
				//printf("\n $$$$$$$$$$$$$$  2222222222222  \n");
			}
			else if ((0x03 & (byRough >> 4)) == 0x02)//分流器5用于负载
			{
				bSMDUSwitchStatus[iAddr][4] = FALSE;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
					= SMDU_EQUIP_NOT_EXISTENT;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR5].iValue 
					= CAN_SAMP_INVALID_VALUE;
				iShunt5Type[iAddr] = SM_LOAD_SHUNT;
				//printf("\n $$$$$$$$$$$$$$  33333333333333  \n");
			}
			else
			{
				if(VersionIsNewest123(iAddr))
				{
					
				}
				else
				{
					bSMDUSwitchStatus[iAddr][4] = TRUE;
					g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].iValue 
						= CAN_SAMP_INVALID_VALUE;

					g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
						= SMDU_EQUIP_NOT_EXISTENT;

					g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR5].iValue 
						= CAN_SAMP_INVALID_VALUE;
					iShunt5Type[iAddr] = SM_NO_SHUNT;
					//软件设定不处理
					//printf("\n $$$$$$$$$$$$$$  33333333333333  \n");
				}
			}

			//added by Jimmy for CR that software Flag for changing Shunt para mV and A
			// S3 bit1
			if(g_CanData.CanCommInfo.SmduCommInfo.iNewestSmdu) //2011/08/17 add 'cause old SMDU has problem with CAN set Shunt size
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_DIP_SWITCH3_1].iValue 
					= (0x01 & byRough);
			}
			else
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_DIP_SWITCH3_1].iValue = 2; 
			}

		}

	}


	for(i = 0; i < 3; i++)
	{
		if(s_abyToggleSwitchState[i + iAddr * 3] != *(pbyAlmBuff + i + 1))
		{
			s_bDetectDIPChange = TRUE;
			break;
		}
	}
	
	for(i = 0; i < 3; i++)
	{
		s_abyToggleSwitchState[i + iAddr * 3] = *(pbyAlmBuff + i + 1);
	}

	return;
}


/*==========================================================================*
* FUNCTION : GetSmTotalLoadCurr
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : float : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-17 15:27
*==========================================================================*/
float GetSmTotalLoadCurr(int iAddr)
{
	float	fTotal = 0.0;
	int iLoadCount = 0;
	if(g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR1].iValue != CAN_SAMP_INVALID_VALUE
		&& g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT1_SETTING_STATE].uiValue == 2)
	{
		fTotal += g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR1].fValue;
		iLoadCount++;
	}

	if(g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR2].iValue != CAN_SAMP_INVALID_VALUE
		&& g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT2_SETTING_STATE].uiValue == 2)
	{
		fTotal += g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR2].fValue;
		iLoadCount++;
	}

	if(g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR3].iValue != CAN_SAMP_INVALID_VALUE
		&& g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT3_SETTING_STATE].uiValue == 2)
	{
		fTotal += g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR3].fValue;
		iLoadCount++;
	}

	if(g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR4].iValue != CAN_SAMP_INVALID_VALUE
		&& g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT4_SETTING_STATE].uiValue == 2)
	{
		fTotal += g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR4].fValue;
		iLoadCount++;
	}

	if(g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].iValue != CAN_SAMP_INVALID_VALUE
		&& g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_SETTING_STATE].uiValue == 2)
	{
		fTotal += g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].fValue;
		iLoadCount++;
	}
	if(iLoadCount <= 1)
	{
		fTotal = CAN_SAMP_INVALID_VALUE;
	}
	//printf("load %d %d %f[%d %d %d %d %d]\n",iAddr, iLoadCount, fTotal,
	//	g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT1_SETTING_STATE].uiValue,
	//	g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT2_SETTING_STATE].uiValue,
	//	g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT3_SETTING_STATE].uiValue,
	//	g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT4_SETTING_STATE].uiValue,
	//	g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_SETTING_STATE].uiValue);
	return fTotal;
}

enum	SM_CMD00_FRAME_ORDER
{
	SM_CMD00_ORDER_CURR1 = 0,
	SM_CMD00_ORDER_CURR2,
	SM_CMD00_ORDER_CURR3,
	SM_CMD00_ORDER_CURR4,
	SM_CMD00_ORDER_BUS_VOLT,
	SM_CMD00_ORDER_ALM_BITS1,
	SM_CMD00_ORDER_ALM_BITS2,
	SM_CMD00_ORDER_ALM_BITS3,

	SM_CMD00_ORDER_FRAME_NUM,
};


/*==========================================================================*
* FUNCTION : SmUnpackCmd00
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
*==========================================================================*/
static BOOL SmUnpackCmd00(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int			i;
	//UINT		uiSerialNo;
	int			iFrameOrder = SM_CMD00_ORDER_CURR1;
	//printf("----length %d [%X %X %X %X][%X %X %X %X][%X %X %X %X][%X %X %X %X][%X %X %X %X] "
	//	,iReadLen, pbyRcvBuf[0], pbyRcvBuf[1], pbyRcvBuf[2], pbyRcvBuf[3], pbyRcvBuf[4], pbyRcvBuf[5], pbyRcvBuf[6], pbyRcvBuf[7], pbyRcvBuf[8], pbyRcvBuf[9], pbyRcvBuf[10], pbyRcvBuf[11], pbyRcvBuf[12], pbyRcvBuf[13], pbyRcvBuf[14], pbyRcvBuf[15], pbyRcvBuf[16], pbyRcvBuf[17], pbyRcvBuf[18], pbyRcvBuf[19]);
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMDU_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case SM_VAL_TYPE_R_LOAD_CURR1:
			if(*(pbyRcvBuf + i * CAN_FRAME_LEN) != 0x88)
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR1].fValue 
					= CAN_SAMP_INVALID_VALUE;
				g_fSMDUShuntReadings[iAddr][0] 
					= 0;
			}
			else
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR1].fValue 
					= CAN_StringToFloat(pValueBuf);
				g_fSMDUShuntReadings[iAddr][0] 
					= CAN_StringToFloat(pValueBuf);
			}
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR1].iValue
				= CAN_SAMP_INVALID_VALUE;

			if(SM_CMD00_ORDER_CURR1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_LOAD_CURR2:
			if(*(pbyRcvBuf + i * CAN_FRAME_LEN) != 0x88)
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR2].fValue 
					= CAN_SAMP_INVALID_VALUE;
				g_fSMDUShuntReadings[iAddr][1]
					= 0;
			}
			else
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR2].fValue 
					= CAN_StringToFloat(pValueBuf);
				g_fSMDUShuntReadings[iAddr][1]
					= CAN_StringToFloat(pValueBuf);
			}
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR2].iValue
				= CAN_SAMP_INVALID_VALUE;
			if(SM_CMD00_ORDER_CURR2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_LOAD_CURR3:
			if(*(pbyRcvBuf + i * CAN_FRAME_LEN) != 0x88)
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR3].fValue 
					= CAN_SAMP_INVALID_VALUE;
				g_fSMDUShuntReadings[iAddr][2] 
					= 0;
			}
			else
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR3].fValue 
					= CAN_StringToFloat(pValueBuf);
				g_fSMDUShuntReadings[iAddr][2] 
					= CAN_StringToFloat(pValueBuf);
			}
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR3].iValue 
				= CAN_SAMP_INVALID_VALUE;
			if(SM_CMD00_ORDER_CURR3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;		

		case SM_VAL_TYPE_R_LOAD_CURR4:
			if(*(pbyRcvBuf + i * CAN_FRAME_LEN) != 0x88)
			{

				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR4].fValue 
					= CAN_SAMP_INVALID_VALUE;
				g_fSMDUShuntReadings[iAddr][3] 
					= 0;
			}
			else
			{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR4].fValue 
					= CAN_StringToFloat(pValueBuf);
				g_fSMDUShuntReadings[iAddr][3] 
					= CAN_StringToFloat(pValueBuf);
			}
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR4].iValue 
				= CAN_SAMP_INVALID_VALUE;
			if(SM_CMD00_ORDER_CURR4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_BATT_CURR1:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR1].fValue 
				= CAN_StringToFloat(pValueBuf);
			g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR1].iValue 
				= CAN_SAMP_INVALID_VALUE;
			g_fSMDUShuntReadings[iAddr][0] = CAN_StringToFloat(pValueBuf);
			if(SM_CMD00_ORDER_CURR1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_BATT_CURR2:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR2].fValue 
				= CAN_StringToFloat(pValueBuf);

			g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR2].iValue 
				= CAN_SAMP_INVALID_VALUE;
			g_fSMDUShuntReadings[iAddr][1] = CAN_StringToFloat(pValueBuf);
			if(SM_CMD00_ORDER_CURR2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_BATT_CURR3:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR3].fValue 
				= CAN_StringToFloat(pValueBuf);

			g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR3].iValue 
				= CAN_SAMP_INVALID_VALUE;
			g_fSMDUShuntReadings[iAddr][2] = CAN_StringToFloat(pValueBuf);
			if(SM_CMD00_ORDER_CURR3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;		

		case SM_VAL_TYPE_R_BATT_CURR4:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR4].fValue 
				= CAN_StringToFloat(pValueBuf);

			g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR4].iValue 
				= CAN_SAMP_INVALID_VALUE;
			g_fSMDUShuntReadings[iAddr][3] = CAN_StringToFloat(pValueBuf);
			if(SM_CMD00_ORDER_CURR4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_BUS_VOLT:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BUS_VOLTAGE].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD00_ORDER_BUS_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_ALARM1:
			UnpackSmduAlarm1(iAddr, pValueBuf);
			if(SM_CMD00_ORDER_ALM_BITS1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_ALARM2:
			UnpackSmduAlarm2(iAddr, pValueBuf);
			if(SM_CMD00_ORDER_ALM_BITS2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_ALARM3:
			UnpackSmduAlarm3(iAddr, pValueBuf);
			if(SM_CMD00_ORDER_ALM_BITS3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}

	if(SM_CMD00_ORDER_FRAME_NUM == iFrameOrder)
	{
		g_CanData.aRoughDataSmdu[iAddr][SMDU_TOTAL_LOAD_CURR].fValue 
			= GetSmTotalLoadCurr(iAddr);
		if(!FLOAT_NOT_EQUAL(g_CanData.aRoughDataSmdu[iAddr][SMDU_TOTAL_LOAD_CURR].fValue, CAN_SAMP_INVALID_VALUE))
		{
			g_CanData.aRoughDataSmdu[iAddr][SMDU_TOTAL_LOAD_CURR].iValue = CAN_SAMP_INVALID_VALUE;
		}
		g_CanData.aRoughDataSmdu[iAddr][SMDU_ADDRESS].iValue = iAddr;

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}



enum	SM_CMD20_FRAME_ORDER
{
	SM_CMD20_ORDER_FEATURE = 0,
	SM_CMD20_ORDER_LOW_SN,
	SM_CMD20_ORDER_VER_NO,
	SM_CMD20_ORDER_BARCODE1,
	SM_CMD20_ORDER_BARCODE2,
	SM_CMD20_ORDER_BARCODE3,
	SM_CMD20_ORDER_BARCODE4,

	SM_CMD20_ORDER_FRAME_NUM,
};
/*==========================================================================*
* FUNCTION : SmUnpackCmd20
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
*==========================================================================*/
static BOOL SmUnpackCmd20(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int			i;
	UINT		uiSerialNo;
	int			iFrameOrder = SM_CMD20_ORDER_FEATURE;
//printf("back %X %X %X %X %X %X %X %X %X %X %X %X %X \n",pbyRcvBuf[0],pbyRcvBuf[1],pbyRcvBuf[2],pbyRcvBuf[3],pbyRcvBuf[4],pbyRcvBuf[5],pbyRcvBuf[6],pbyRcvBuf[7],pbyRcvBuf[8],pbyRcvBuf[9],pbyRcvBuf[10],pbyRcvBuf[11],pbyRcvBuf[12]);
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMDU_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case SM_VAL_TYPE_R_FEATURE:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_FEATURE].uiValue 
				= CAN_StringToUint(pValueBuf);
			//TRACE("SM_VAL_TYPE_R_FEATURE:%4x\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_FEATURE].uiValue);
			if(SM_CMD20_ORDER_FEATURE == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_SN_LOW:
			uiSerialNo = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_LOW].uiValue 
				= uiSerialNo;
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_HIGH].uiValue 
				= uiSerialNo >> 30;

			//TRACE("SMDU_SERIAL_NO_LOW:%4x\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_LOW].uiValue );

			//TRACE("SSMDU_SERIAL_NO_HIGH:%4x\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_HIGH].uiValue );

			//printf(" CAN  smdu all LOW = %u  HI = %u \n",
			//	g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_LOW].uiValue,
			//	g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_HIGH].uiValue);

			if(SM_CMD20_ORDER_LOW_SN == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_VER_NO:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_VERSION_NO].uiValue 
				= CAN_StringToUint(pValueBuf);
//printf("56:%X %X %X %X %X %X %X %X  \n",
//	   *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L),
//	   *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L+1),
//	   *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L+2),
//	   *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L+3),
//	   *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L+4),
//	   *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L+5),
//	   *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L+6),
//	   *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L+7));

			//TRACE("SMDU_VERSION_NO:%4x\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_VERSION_NO].uiValue );

			if(SM_CMD20_ORDER_VER_NO == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;		

		case SM_VAL_TYPE_R_BARCODE1:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE1].uiValue
				= CAN_StringToUint(pValueBuf);

			//TRACE("SMDU_BARCODE1:%4x\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE1].uiValue );

			if(SM_CMD20_ORDER_BARCODE1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_BARCODE2:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE2].uiValue
				= CAN_StringToUint(pValueBuf);

			//TRACE("SMDU_BARCODE2:%4x\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE2].uiValue );

			if(SM_CMD20_ORDER_BARCODE2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_BARCODE3:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE3].uiValue
				= CAN_StringToUint(pValueBuf);

			//TRACE("SMDU_BARCODE3:%4x\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE3].uiValue );

			if(SM_CMD20_ORDER_BARCODE3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_BARCODE4:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE4].uiValue
				= CAN_StringToUint(pValueBuf);

			//TRACE("SMDU_BARCODE4:%4x\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE4].uiValue );

			if(SM_CMD20_ORDER_BARCODE4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;		

		default:
			break;
		}
	}

	if(SM_CMD20_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


enum	SM_CMD21_FRAME_ORDER
{
	SM_CMD21_ORDER_SHUNT1_V = 0,
	SM_CMD21_ORDER_SHUNT1_A,
	SM_CMD21_ORDER_SHUNT2_V,
	SM_CMD21_ORDER_SHUNT2_A,
	SM_CMD21_ORDER_SHUNT3_V,
	SM_CMD21_ORDER_SHUNT3_A,
	SM_CMD21_ORDER_SHUNT4_V,
	SM_CMD21_ORDER_SHUNT4_A,

	SM_CMD21_ORDER_FRAME_NUM,
};

/*==========================================================================*
* FUNCTION : SmUnpackCmd21
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
*==========================================================================*/
static BOOL SmUnpackCmd21(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int			i;
	//UINT		uiSerialNo;
	int			iFrameOrder = SM_CMD21_ORDER_SHUNT1_V;
	//?????????????????????
	char szSendStr[2000] = {0};
	for(i=0;i<iReadLen;i++)
	{
		sprintf(szSendStr+2*i,"%02X", *(pbyRcvBuf+i));
	}
	//printf("\n\n\n========The Sample Data for cmd21 are %s=-========\n\n\n",szSendStr);
	//??????????????????????
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMDU_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{			
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case SM_VAL_TYPE_R_CURR1_MV:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT1_MV].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("\n\nRead Shunt 1 mV is %f\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT1_MV].fValue);
			if(SM_CMD21_ORDER_SHUNT1_V == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_CURR1_A:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT1_A].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("\n\nRead Shunt 1 A is %f\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT1_A].fValue);
			if(SM_CMD21_ORDER_SHUNT1_A == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_CURR2_MV:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT2_MV].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("\n\nRead Shunt 2 mV is %f\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT2_MV].fValue);
			if(SM_CMD21_ORDER_SHUNT2_V == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_CURR2_A:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT2_A].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("\n\nRead Shunt 2 A is %f\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT2_A].fValue);
			if(SM_CMD21_ORDER_SHUNT2_A == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_CURR3_MV:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT3_MV].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("\n\nRead Shunt 3 mV is %f\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT3_MV].fValue);
			if(SM_CMD21_ORDER_SHUNT3_V == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_CURR3_A:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT3_A].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("\n\nRead Shunt 3 A is %f\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT3_A].fValue);
			if(SM_CMD21_ORDER_SHUNT3_A == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_CURR4_MV:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT4_MV].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("\n\nRead Shunt 4 mV is %f\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT4_MV].fValue);
			if(SM_CMD21_ORDER_SHUNT4_V == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_CURR4_A:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT4_A].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("\n\nRead Shunt 4 A is %f\n",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT4_A].fValue);
			if(SM_CMD21_ORDER_SHUNT4_A == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}

	if(SM_CMD21_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


enum	SM_CMD22_FRAME_ORDER
{
	SM_CMD22_ORDER_RATED_CAP = 0,
	SM_CMD22_ORDER_OVER_VOLT,
	SM_CMD22_ORDER_UNDER_VOLT,
	SM_CMD22_ORDER_OVER_CURR1,
	SM_CMD22_ORDER_OVER_CURR2,
	SM_CMD22_ORDER_OVER_CURR3,
	SM_CMD22_ORDER_OVER_CURR4,

	SM_CMD22_ORDER_FRAME_NUM,
};


/*==========================================================================*
* FUNCTION : SmUnpackCmd22
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
*==========================================================================*/
static BOOL SmUnpackCmd22(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int			i;
	//UINT		uiSerialNo;
	int			iFrameOrder = SM_CMD22_ORDER_OVER_VOLT;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMDU_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case SM_VAL_TYPE_R_RATED_CAP:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_RATED_CAP].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD22_ORDER_RATED_CAP == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_OVER_VOLT:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_OVER_VOLT].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD22_ORDER_OVER_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_UNDER_VOLT:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_UNDER_VOLT].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD22_ORDER_UNDER_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_OVER_CURR1:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_OVER_CURR1].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD22_ORDER_OVER_CURR1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_OVER_CURR2:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_OVER_CURR2].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD22_ORDER_OVER_CURR2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_OVER_CURR3:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_OVER_CURR3].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD22_ORDER_OVER_CURR3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_OVER_CURR4:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_OVER_CURR4].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD22_ORDER_OVER_CURR4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		default:
			break;
		}
	}

	if(SM_CMD22_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}



/*==========================================================================*
* FUNCTION : ClrSmIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:30
*==========================================================================*/
static void ClrSmIntrruptTimes(int iAddr)
{
	g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_TIMES].iValue = 0;
	g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_ST].iValue 
		= SM_COMM_NORMAL_ST;

	return;
}



/*==========================================================================*
* FUNCTION : IncSmIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
*==========================================================================*/
static void IncSmIntrruptTimes(int iAddr)
{
	if(g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_TIMES].iValue 
		< SM_MAX_INTERRUPT_TIMES + 2)
	{
		g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_TIMES].iValue++;
	}

	if(g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_TIMES].iValue
		>= (SM_MAX_INTERRUPT_TIMES + 1))
	{
		if(g_CanData.aRoughDataGroup[GROUP_SM_ALL_NO_RESPONSE].iValue)
		{
			g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_ST].iValue 
				= SM_COMM_ALL_INTERRUPT_ST;
		}
		else
		{
			g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_ST].iValue 
				= SM_COMM_INTERRUPT_ST;
		}
	}
	else
	{
		g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_ST].iValue 
			= SM_COMM_NORMAL_ST;
	}

	return;
}


/*==========================================================================*
* FUNCTION : PackAndSendSmduCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UINT   uiMsgType    : 
*            UINT   uiDestAddr   : 
*            UINT   uiCmdType    : 
*            UINT   uiValueTypeH : 
*            UINT   uiValueTypeL : 
*            float  fParam       : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:27
*==========================================================================*/
static void PackAndSendSmduCmd(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   float fParam)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDU_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//
	if(uiMsgType == MSG_TYPE_RQST_SETTINGS && uiValueTypeL >= SM_VAL_TYPE_R_SHUNT1_STATE && uiValueTypeL<= SM_VAL_TYPE_R_SHUNT5_STATE)
	{
		//shunt type is int in protocol 
		CAN_UintToString((UINT)fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
	}
	else
	{
		CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
	}
//printf("%X %X %X %X %X %X %X %X %X \n",pbySendBuf[0],pbySendBuf[1],pbySendBuf[2],pbySendBuf[3],pbySendBuf[4],pbySendBuf[5],pbySendBuf[6],pbySendBuf[7],pbySendBuf[8]);
/*
	int		i;
	char	strOut[30];

	for(i = 0; i < 13; i++)
	{
	sprintf(strOut + i * 2, "%02X", *(pbySendBuf + i));
	}
	strOut[26] = 0;

	if(uiMsgType == MSG_TYPE_RQST_DATA31 ||uiMsgType == MSG_TYPE_RQST_DATA21)
	{
	printf("******uiDestAddr = %d, Send string: %s \n", uiDestAddr, strOut);
	}
*/
	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}

/*==========================================================================*
 * FUNCTION : MB_IsAllLvdDisconnect
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-01-16 14:15
 *==========================================================================*/
#define	SMDU_MAX_NUM_LVD_EQUIP		9
#define LVD_EQUIP_START_ID		187
#define SMDU_LVD1_STATE_ID		1
#define SMDU_LVD2_STATE_ID		2
#define	 EQUIP_EXIST_ID			100
#define	 SMDU_LVD_EXIST			0
#define	 SMDU_LVD_NOT_EXIST		1
#define	 SMDU_LVD1_ENABLE_ID		3
#define	 SMDU_LVD2_ENABLE_ID		8
#define	 SMDU_LVD_DISABLE		0
#define	 SMDU_LVD_ENABLE		1
#define	 SMDU_SIG_ALARM			1
#define	 SMDU_SIG_NORMAL		0

static BOOL	SMDU_IsLvdSigValid(int iIdxLvdEquip, int iIdxLvdSig)
{
	char				strOut[256];
	
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	int			iEquipId;
	int			iSetLvdEnableId;

	//Equip ID
	iEquipId = LVD_EQUIP_START_ID + iIdxLvdEquip;	

	//Check if the device is exist
	//If not exist, return FALSE
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipId,
	    DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, EQUIP_EXIST_ID),
	    &iBufLen,
	    &pSigValue,
	    0);

	if(iRst != ERR_OK)
	{
	    return FALSE;
	}

	if(pSigValue->varValue.enumValue == SMDU_LVD_NOT_EXIST)
	{
	    return FALSE;
	}
			  

	//LVD Enable Setting ID
	if(iIdxLvdSig == 1)
	{
	    iSetLvdEnableId = SMDU_LVD2_ENABLE_ID;
	}
	else
	{
	    iSetLvdEnableId = SMDU_LVD1_ENABLE_ID;
	}	
	//Check if the lvd is enabled
	//If disabled, return FALSE
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipId,
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, iSetLvdEnableId),
		&iBufLen,
		&pSigValue,
		0);	

	if(pSigValue->varValue.enumValue == SMDU_LVD_DISABLE)
	{
		return FALSE;
	}

	return TRUE;
}
static SIG_ENUM SMDU_GetEnumSigValue(int nEquipId, int nSignalType, int nSignalId)
{
	SIG_BASIC_VALUE*	pSigValue;
	int					iRst;
	int					iBufLen;
	
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
					nEquipId,
					DXI_MERGE_SIG_ID(nSignalType, nSignalId),
					&iBufLen,
					&pSigValue,
					0);
	
	if(iRst != ERR_OK)
	{
		AppLogOut("SMDU SAMP", 
			APP_LOG_ERROR, 
			"SMDU_GetEnumSigValue error, EquipId = %d, SigType = %d, SigId = %d!\n", 
			nEquipId,
			nSignalType,
			nSignalId); 
	}	
	
	return pSigValue->varValue.enumValue;

}
static BOOL SMDU_IsLvd3Valid(void)
{
	char			strOut[256];
	
	SIG_BASIC_VALUE*	pSigValue;
	int			iRst;
	int			iBufLen;
	int			iEquipId;
	int			iSetLvdEnableId;

	iEquipId = 3510;//LVD 3 equipment ID

	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
		iEquipId,
		DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, EQUIP_EXIST_ID),
		&iBufLen,
		&pSigValue,
		0);

	if(iRst != ERR_OK)
	{
		return FALSE;
	}

	if(pSigValue->varValue.enumValue == SMDU_LVD_NOT_EXIST)
	{
		return FALSE;
	}

	//Check if the lvd is enabled
	//If disabled, return FALSE
	iRst = DxiGetData(VAR_A_SIGNAL_VALUE,
	    iEquipId,
	    DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, SMDU_LVD1_ENABLE_ID),
	    &iBufLen,
	    &pSigValue,
	    0);	

	if(iRst != ERR_OK || pSigValue->varValue.enumValue == SMDU_LVD_DISABLE)
	{
		return FALSE;
	}	
	
	return TRUE;
}
static BOOL SMDU_IsAllLvdDisconnect(void)
{
	BOOL	bAllLvdDisconnect = TRUE;
	BOOL	bLvdValid = FALSE;
	int		i;

	for(i = 0; i < SMDU_MAX_NUM_LVD_EQUIP; i++)
	{
		if(SMDU_IsLvdSigValid(i, 0))
		{
			bLvdValid = TRUE;
			if(SMDU_SIG_NORMAL
				== SMDU_GetEnumSigValue(LVD_EQUIP_START_ID + i, 
									SIG_TYPE_SAMPLING,
									SMDU_LVD1_STATE_ID))
			{
				bAllLvdDisconnect = FALSE;
				break;
			}
		}

		if(SMDU_IsLvdSigValid(i, 1))
		{
			bLvdValid = TRUE;
			if(SMDU_SIG_NORMAL
				== SMDU_GetEnumSigValue(LVD_EQUIP_START_ID + i, 
									SIG_TYPE_SAMPLING,
									SMDU_LVD2_STATE_ID))
			{
				bAllLvdDisconnect = FALSE;
				break;
			}
		}
	}

	//Add LVD3 disconnect information, because LVD3 is a standalone LVD3, just do it as below.
	if(SMDU_IsLvd3Valid())
	{
		bLvdValid = TRUE;
		if(SMDU_SIG_NORMAL
				== SMDU_GetEnumSigValue(3510, 
						SIG_TYPE_SAMPLING,
						SMDU_LVD1_STATE_ID))
		{
			bAllLvdDisconnect = FALSE;			
		}
	}

	if(!bLvdValid)
	{
		bAllLvdDisconnect = FALSE;
	}

	return bAllLvdDisconnect;
}



void SM_ClearCurrentVoltage(void)
{
	int i;
	if(SMDU_IsAllLvdDisconnect())
	{
		for(i = 0; i < MAX_NUM_SMDU; i++)
		{
			if(SMDU_EQUIP_EXISTENT == g_CanData.aRoughDataSmdu[i][SMDU_EXISTENCE].iValue)
			{
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR1].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR2].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR3].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR4].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR5].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_VOLT1].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_VOLT2].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_VOLT3].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_VOLT4].fValue = 0.0;
				g_CanData.aRoughDataSmdu[i][SMDU_BATT_VOLT5].fValue = 0.0;
			}
		}
	}
}
/*==========================================================================*
* FUNCTION : SmSampleCmd00
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:29
*==========================================================================*/
static SIG_ENUM SmSampleCmd00(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(MSG_TYPE_RQST_DATA00,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		(UINT)GetSmSetByteCmd00(iAddr),
		SM_VAL_TYPE_NO_TRIM_VOLT,
		0.0);
	//In most situations, I guess SM-DU responds in 15ms
	Sleep(SM_CMD00_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);	
	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);
	//printf("\nSMDU    Addr: %d; Len: %d",iAddr,iReadLen1st);
	if(iReadLen1st < (SM_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		//After 15s+55s, if no respond, interrup times increase.
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SM_CMD00_RCV_FRAMES_NUM)
	{
		if(SmUnpackCmd00(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmIntrruptTimes(iAddr);
		}
		else
		{
			IncSmIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		IncSmIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}


//2010/06/09 SmUnpackCmd10
static BOOL SmUnpackCmd10(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int			i;
	//UINT		uiSerialNo;
	int			iFrameOrder = SM_CMD00_ORDER_CURR1;

#define SM_VAL_TYPE_R_LOAD_CURR5 0x0D
#define SM_VAL_TYPE_R_BATT_CURR5 0x0E
#define SM_VAL_TYPE_R_SWITCH3	 0x43

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMDU_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case SM_VAL_TYPE_R_LOAD_CURR1:
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR1].fValue 
			//	= CAN_StringToFloat(pValueBuf);
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR1].iValue
			//	= CAN_SAMP_INVALID_VALUE;

			if(SM_CMD00_ORDER_CURR1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_LOAD_CURR2:
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR2].fValue 
			//	= CAN_StringToFloat(pValueBuf);
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR2].iValue
			//	= CAN_SAMP_INVALID_VALUE;

			if(SM_CMD00_ORDER_CURR2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_LOAD_CURR3:
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR3].fValue 
			//	= CAN_StringToFloat(pValueBuf);
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR3].iValue 
			//	= CAN_SAMP_INVALID_VALUE;

			if(SM_CMD00_ORDER_CURR3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;		

		case SM_VAL_TYPE_R_LOAD_CURR4:

			//g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR4].fValue 
			//	= CAN_StringToFloat(pValueBuf);
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR4].iValue 
			//	= CAN_SAMP_INVALID_VALUE;
			if(SM_CMD00_ORDER_CURR4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_BATT_CURR1:
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR1].fValue 
			//	= CAN_StringToFloat(pValueBuf);
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR1].iValue 
			//	= CAN_SAMP_INVALID_VALUE;

			if(SM_CMD00_ORDER_CURR1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_BATT_CURR2:
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR2].fValue 
			//	= CAN_StringToFloat(pValueBuf);
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR2].iValue 
			//	= CAN_SAMP_INVALID_VALUE;

			if(SM_CMD00_ORDER_CURR2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_BATT_CURR3:
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR3].fValue 
			//	= CAN_StringToFloat(pValueBuf);
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR3].iValue 
			//	= CAN_SAMP_INVALID_VALUE;

			if(SM_CMD00_ORDER_CURR3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;		

		case SM_VAL_TYPE_R_BATT_CURR4:
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR4].fValue 
			//	= CAN_StringToFloat(pValueBuf);
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR4].iValue 
			//	= CAN_SAMP_INVALID_VALUE;

			if(SM_CMD00_ORDER_CURR4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_BUS_VOLT:
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BUS_VOLTAGE].fValue 
			//	= CAN_StringToFloat(pValueBuf);

			if((SM_CMD00_ORDER_BUS_VOLT + 1) == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_ALARM1:
			//UnpackSmduAlarm1(iAddr, pValueBuf);

			if((SM_CMD00_ORDER_ALM_BITS1 + 1) == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_ALARM2:
			//UnpackSmduAlarm2(iAddr, pValueBuf);

			if((SM_CMD00_ORDER_ALM_BITS2 + 1) == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_ALARM3:
			//UnpackSmduAlarm3(iAddr, pValueBuf);

			if((SM_CMD00_ORDER_ALM_BITS3 + 1) == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_LOAD_CURR5:

			if(VersionIsNewest123(iAddr))
			{
				if(*(pbyRcvBuf + i * CAN_FRAME_LEN) != 0x88)
				{
					g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].fValue 
						= CAN_SAMP_INVALID_VALUE;
					g_fSMDUShuntReadings[iAddr][4] 
						= 0;
				}
				else
				{
					g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].fValue 
						= CAN_StringToFloat(pValueBuf);
					g_fSMDUShuntReadings[iAddr][4] 
						= CAN_StringToFloat(pValueBuf);
				}

				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR5].iValue
					= CAN_SAMP_INVALID_VALUE;
				
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
					= SMDU_EQUIP_NOT_EXISTENT;
			}
			else
			{
				if(iShunt5Type[iAddr] == SM_LOAD_SHUNT)
				{
	//printf("iShunt5Type[iAddr] == SM_LOAD_SHUNT\n");
					g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].fValue 
						= CAN_StringToFloat(pValueBuf);

				}
				else
				{
					g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].iValue 
						= CAN_SAMP_INVALID_VALUE;
				}
				g_fSMDUShuntReadings[iAddr][4] = CAN_StringToFloat(pValueBuf);
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
					= SMDU_EQUIP_NOT_EXISTENT;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR5].iValue 
					= CAN_SAMP_INVALID_VALUE;
			}
			
			if (4 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		case SM_VAL_TYPE_R_BATT_CURR5:

			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR5].fValue 
				= CAN_StringToFloat(pValueBuf);

			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
				= SMDU_EQUIP_EXISTENT;
			g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].iValue 
				= CAN_SAMP_INVALID_VALUE;
			g_fSMDUShuntReadings[iAddr][4] = CAN_StringToFloat(pValueBuf);
			if (4 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;
		case SM_VAL_TYPE_R_SWITCH3://BYTE 4 见总体技术方案，电池电流5状态
			//g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_CURR_ST].fValue 
			//	= (*pValueBuf);//CAN_StringToFloat(pValueBuf);
			UnpackSmduAlarm43(iAddr, pValueBuf);
			if (9 == iFrameOrder)
			{
				iFrameOrder++;
			}

		default:
			break;
		}
	}

#define SM_CMD10_ORDER_FRAME_NUM 10
	if(SM_CMD10_ORDER_FRAME_NUM == iFrameOrder)
	{
		//g_CanData.aRoughDataSmdu[iAddr][SMDU_TOTAL_LOAD_CURR].fValue 
		//	= GetSmTotalLoadCurr(iAddr);

		//g_CanData.aRoughDataSmdu[iAddr][SMDU_ADDRESS].iValue = iAddr;

		return TRUE;
	}
	else
	{
		return FALSE;
	}

}

//2010/06/09  SmSampleCmd10
static SIG_ENUM SmSampleCmd10(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;


	PackAndSendSmduCmd(MSG_TYPE_RQST_DATA10,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		(UINT)GetSmSetByteCmd00(iAddr),//因为王工新加的与00协议完全相同所以一致
		SM_VAL_TYPE_NO_TRIM_VOLT,
		0.0);

	//In most situations, I guess SM-DU responds in 15s
	Sleep(SM_CMD00_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (SM_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		//After 15s+55s, if no respond, interrup times increase.
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SM_CMD00_RCV_FRAMES_NUM)
	{
		if(SmUnpackCmd10(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			return CAN_SAMPLE_OK;
		}
		else
		{
			//printf("\n	This is  SMDU   CMD   10  1\n");
			return CAN_SAMPLE_FAIL;
		}
	}
	else
	{
		//printf("\n	This is  SMDU   CMD   10  2\n");
		return CAN_SAMPLE_FAIL;	
	}

	return CAN_SAMPLE_OK;

}

enum	SM_CMD30_FRAME_ORDER
{
	SM_CMD30_ORDER_SHUNT5_MV = 0,
	SM_CMD30_ORDER_SHUNT5_A,
	SM_CMD30_ORDER_OVER_CURR5_P,
	SM_CMD30_ORDER_CURR5_OVER_OVER_CURR5,


	SM_CMD30_ORDER_FRAME_NUM,
};

//2010/06/09 SmUnpackCmd30
static BOOL SmUnpackCmd30(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int			i;

	//SMDU_OVER_CURR5
	int			iFrameOrder = SM_CMD30_ORDER_SHUNT5_MV;

	//printf(" $$$$$$$$$$$$$$$$  D =%d ",iReadLen / CAN_FRAME_LEN);

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
								+ i * CAN_FRAME_LEN
								+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMDU_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
			!= 0))
		{
			continue;
		}

		//printf("\n @#$@$$#### i =%d  value =%d \n",i,*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L));

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case SM_VAL_TYPE_R_SHUNT5_MV:			
			//added by Jimmy 20110621
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_MV].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("&&&&&&&& Shunt 5 mV is %f",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_MV].fValue );
			if(SM_CMD30_ORDER_SHUNT5_MV == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_SHUNT5_A:			
			//added by Jimmy 20110621
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_A].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("&&&&&&&& Shunt 5 A is %f",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_A].fValue );
			if(SM_CMD30_ORDER_SHUNT5_A == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_W_OVER_CURR5:
			//printf("\33333333\n");
				
			//该参数是要参数同步用的。  过流点
			g_CanData.aRoughDataSmdu[iAddr][SMDU_OVER_CURR5].fValue 
				= CAN_StringToFloat(pValueBuf);
			//printf("&&&&&&&& Shunt 5 Over curr is %f",g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_A].fValue );
			if(SM_CMD30_ORDER_OVER_CURR5_P == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_W_OOVER_CURR5:
			//printf("\44444444\n");

			//过过流点不需要
			if(SM_CMD30_ORDER_CURR5_OVER_OVER_CURR5 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}

	}

	if(SM_CMD30_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

enum	SM_CMD31_FRAME_ORDER
{
	SM_CMD31_ORDER_SHUNT1_STATE = 0,
	SM_CMD31_ORDER_SHUNT2_STATE,
	SM_CMD31_ORDER_SHUNT3_STATE,
	SM_CMD31_ORDER_SHUNT4_STATE,
	SM_CMD31_ORDER_SHUNT5_STATE,

	SM_CMD31_ORDER_FRAME_NUM,
};



//2017/02/09 SmUnpackCmd31
static BOOL SmUnpackCmd31(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int			i;

	int			iFrameOrder = SM_CMD31_ORDER_SHUNT1_STATE;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMDU_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
			!= 0))
		{
			continue;
		}

		BYTE*		pValueBuf = pbyRcvBuf 
								+ i * CAN_FRAME_LEN
								+ CAN_FRAME_OFFSET_VALUE;

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case SM_VAL_TYPE_R_SHUNT1_STATE:			
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT1_STATE].uiValue 
				= CAN_StringToUintBack(pValueBuf);
			if(SM_CMD31_ORDER_SHUNT1_STATE == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_SHUNT2_STATE:	
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT2_STATE].uiValue 
				= CAN_StringToUintBack(pValueBuf);
			if(SM_CMD31_ORDER_SHUNT2_STATE == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_SHUNT3_STATE:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT3_STATE].uiValue 
				= CAN_StringToUintBack(pValueBuf);
			if(SM_CMD31_ORDER_SHUNT3_STATE == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		case SM_VAL_TYPE_R_SHUNT4_STATE:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT4_STATE].uiValue 
				= CAN_StringToUintBack(pValueBuf);
			if(SM_CMD31_ORDER_SHUNT4_STATE == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_SHUNT5_STATE:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_STATE].uiValue 
				= CAN_StringToUintBack(pValueBuf);
			if(SM_CMD31_ORDER_SHUNT5_STATE == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;


		default:
			break;
		}

	}

	if(SM_CMD31_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


//2017/02/09  SmSampleCmd31 
static SIG_ENUM SmSampleCmd31(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;


	PackAndSendSmduCmd(MSG_TYPE_RQST_DATA31,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						0x00,
						SM_VAL_TYPE_NO_TRIM_VOLT,
						0.0);

	//In most situations, I guess SM-DU responds in 15s
	Sleep(SM_CMD00_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (SM_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		//After 15s+55s, if no respond, interrup times increase.
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SM_CMD31_RCV_FRAMES_NUM)
	{
		if(SmUnpackCmd31(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			return CAN_SAMPLE_OK;
		}
		else
		{
			return CAN_SAMPLE_FAIL;
		}
	}
	else
	{
		return CAN_SAMPLE_FAIL;	
	}

	return CAN_SAMPLE_OK;

}

//2010/06/09  SmSampleCmd30
static SIG_ENUM SmSampleCmd30(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;


	PackAndSendSmduCmd(MSG_TYPE_RQST_DATA30,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						0x00,
						SM_VAL_TYPE_NO_TRIM_VOLT,
						0.0);

	//In most situations, I guess SM-DU responds in 15s
	Sleep(SM_CMD00_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (SM_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		//After 15s+55s, if no respond, interrup times increase.
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SM_CMD30_RCV_FRAMES_NUM)
	{
		if(SmUnpackCmd30(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			return CAN_SAMPLE_OK;
		}
		else
		{
			return CAN_SAMPLE_FAIL;
		}
	}
	else
	{
		//printf("\n >= SM_CMD30_RCV_FRAMES_NUM !!!!!\n");
		return CAN_SAMPLE_FAIL;	
	}

	return CAN_SAMPLE_OK;

}
/*==========================================================================*
* FUNCTION : SmSampleCmd20
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:29
*==========================================================================*/
static SIG_ENUM SmSampleCmd20(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(MSG_TYPE_RQST_DATA20,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0x00,
		SM_VAL_TYPE_NO_TRIM_VOLT,
		0.0);

	//In most situations, I guess SM-DU responds in 15s
	Sleep(SM_CMD20_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);
	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (SM_CMD20_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		//After 15s+55s, if no respond, interrup times increase.
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SM_CMD20_RCV_FRAMES_NUM)
	{
		SmUnpackCmd20(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
	}
	else
	{
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}


/*==========================================================================*
* FUNCTION : SmSampleCmd21
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:29
*==========================================================================*/
static SIG_ENUM SmSampleCmd21(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(MSG_TYPE_RQST_DATA21,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0x00,
		SM_VAL_TYPE_NO_TRIM_VOLT,
		0.0);

	//In most situations, I guess SM-DU responds in 15s
	Sleep(15);
	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);
	if(iReadLen1st < (SM_CMD21_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		//After 15s+55s, if no respond, interrup times increase.
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SM_CMD21_RCV_FRAMES_NUM)
	{
		SmUnpackCmd21(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
	}
	else
	{
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}


/*==========================================================================*
* FUNCTION : SmSampleCmd22
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:29
*==========================================================================*/
static SIG_ENUM SmSampleCmd22(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(MSG_TYPE_RQST_DATA22,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0x00,
		SM_VAL_TYPE_NO_TRIM_VOLT,
		0.0);

	//In most situations, I guess SM-DU responds in 15s
	Sleep(15);
	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);
	if(iReadLen1st < (SM_CMD22_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		//After 15s+55s, if no respond, interrup times increase.
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SM_CMD22_RCV_FRAMES_NUM)
	{
		SmUnpackCmd22(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
	}
	else
	{
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}


enum	SM_CMD23_FRAME_ORDER
{
	SM_CMD23_ORDER_LVD1_DISCONNECT_VOLT = 0,
	SM_CMD23_ORDER_LVD1_RECONNECT_VOLT,
	SM_CMD23_ORDER_LVD2_DISCONNECT_VOLT,
	SM_CMD23_ORDER_LVD2_RECONNECT_VOLT,

	SM_CMD23_ORDER_FRAME_NUM,
};



static BOOL SmUnpackCmd23(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int			i;
	//UINT		uiSerialNo;
	int			iFrameOrder = SM_CMD23_ORDER_LVD1_DISCONNECT_VOLT;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;
		//float		fRunTime;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMDU_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case SM_VAL_TYPE_R_LVD1_VOLT:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_LVD1_DISCON_VOLT].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD23_ORDER_LVD1_DISCONNECT_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_LVR1_VOLT:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_LVD1_RECON_VOLT].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD23_ORDER_LVD1_RECONNECT_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_LVD2_VOLT:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_LVD2_DISCON_VOLT].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD23_ORDER_LVD2_DISCONNECT_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case SM_VAL_TYPE_R_LVR2_VOLT:
			g_CanData.aRoughDataSmdu[iAddr][SMDU_LVD2_RECON_VOLT].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(SM_CMD23_ORDER_LVD2_RECONNECT_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}

	if(SM_CMD23_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static SIG_ENUM SmSampleCmd23(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(MSG_TYPE_RQST_DATA23,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0x00,
		SM_VAL_TYPE_NO_TRIM_VOLT,
		0.0);

	//In most situations, I guess SM-DU responds in 15s
	Sleep(15);
	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st);
	if(iReadLen1st < (SM_CMD23_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		//After 15s+55s, if no respond, interrup times increase.
		Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
			&iReadLen2nd);
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SM_CMD23_RCV_FRAMES_NUM)
	{
		SmUnpackCmd23(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
	}
	else
	{
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}



/*==========================================================================*
* FUNCTION : SM_RefreshFlashInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:29
*==========================================================================*/
static void SM_RefreshFlashInfo(void)
{
	int			i;

	for(i = 0; i < MAX_NUM_SMDU; i++)
	{
		g_CanData.CanFlashData.aSmduInfo[i].dwHighSn 
			= g_CanData.aRoughDataSmdu[i][SMDU_SERIAL_NO_HIGH].dwValue;
		g_CanData.CanFlashData.aSmduInfo[i].dwSerialNo 
			= g_CanData.aRoughDataSmdu[i][SMDU_SERIAL_NO_LOW].dwValue;
		g_CanData.CanFlashData.aSmduInfo[i].iSeqNo 
			= g_CanData.aRoughDataSmdu[i][SMDU_SEQ_NO].iValue;
		g_CanData.CanFlashData.aSmduInfo[i].iAddress = i; 

		if(g_CanData.CanFlashData.aSmduInfo[i].iSeqNo == CAN_SAMP_INVALID_VALUE)
		{
			g_CanData.CanFlashData.aSmduInfo[i].bExistence = FALSE;
		}
		else
		{
			g_CanData.CanFlashData.aSmduInfo[i].bExistence = TRUE;		
		}
	}

	CAN_WriteFlashData(&(g_CanData.CanFlashData));

	return;
}

BOOL VersionIsNewest(int iAddr)
{
	int iVersionL;

	iVersionL = g_CanData.aRoughDataSmdu[iAddr][SMDU_VERSION_NO].uiValue & 0xff;
	//printf("************SMDU Version is %d", iVersionL);
	//规定大于等于1.21版本的就是新版本
	if (iVersionL >= 121)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

BOOL VersionIsNewest123(int iAddr)
{
	int iVersionL;

	iVersionL = g_CanData.aRoughDataSmdu[iAddr][SMDU_VERSION_NO].uiValue & 0xff;
	//printf("************SMDU Version is %d", iVersionL);
	//判断是否大于等于1.23版本
	if (iVersionL >= 123)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*==========================================================================*
* FUNCTION : SM_Reconfig
* PURPOSE  : Judge if SM-DUs need to re-config, if need, scan SM-DUs and 
*            save SM-DU connecting information.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-06-24 15:46
*==========================================================================*/
void SM_Reconfig(void)
{

	int			iAddr;
	SIG_ENUM	emRst;
	int			iSmduNum;
	SIG_ENUM	enumTemp;

	SM_InitRoughValue();

	iSmduNum = 0;
	for(iAddr = 0; iAddr < MAX_NUM_SMDU; iAddr++)
	{
		emRst = SmSampleCmd00(iAddr);

		if(CAN_SAMPLE_OK == emRst)
		{
			g_CanData.aRoughDataSmdu[iAddr][SMDU_SEQ_NO].iValue = iSmduNum;
			iSmduNum++;

			SmSampleCmd20(iAddr);

			//我们不支持新老SMDU混接！！所以只要有一个新的我们就认为新的！！
			if(VersionIsNewest(iAddr))
			{
				//2010/06/09
				g_CanData.CanCommInfo.SmduCommInfo.iNewestSmdu = TRUE;
			}
		}
	}

	if(iSmduNum)
	{
		if(g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue != iSmduNum)
		{
			g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue = iSmduNum;
			SetDwordSigValue(SMDU_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				CFG_CHANGED_SIG_ID,
				1,
				"CAN_SAMP");
		}

		g_CanData.aRoughDataGroup[GROUP_SM_GROUP_STATE].iValue 
			= SMDU_EQUIP_EXISTENT;
		
		/*
			2010/11/11
			按照王靖需求，采集到CAN上的SMDU 就将系统的183信号置为CAN 采集
			如果采集到 485上的SMDU 就将 系统的183信号置为 485 采集
			但是，如果用户在485上接了SMDU和在CAN上也接了SMDU，属于不满足我们
			的设计需求。无法兼容
		*/
		enumTemp = 0;//The system running as Can Mode!!
		SetEnumSigValue(SYS_GROUP_EQUIPID, SIG_TYPE_SETTING, 183, enumTemp, "CanSMP Setting Bus Mode");
		
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_SM_GROUP_STATE].iValue 
			= SMDU_EQUIP_NOT_EXISTENT;
	}

	SM_RefreshFlashInfo();//move here by Jimmy 2011/10/24

	return;
}


/*==========================================================================*
* FUNCTION : ReceiveReconfigCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-10 16:55
*==========================================================================*/
static BOOL ReceiveReconfigCmd(void)
{
	if(GetDwordSigValue(SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		SYS_RECONFIG_SIG_ID,
		"CAN_SAMP") > 0)
	{
		SetDwordSigValue(SYS_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			SYS_RECONFIG_SIG_ID,
			0,
			"CAN_SAMP");
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
* FUNCTION : GetSmAddrBySeqNo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iSeqNo : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-03-04 16:10
*==========================================================================*/
//static int GetSmAddrBySeqNo(int iSeqNo)
//{
//	int		i;
//
//	for(i = 0; i < MAX_NUM_SMDU; i++)
//	{
//		if(g_CanData.aRoughDataSmdu[i][SMDU_SEQ_NO].iValue == iSeqNo)
//		{
//			return i;
//		}
//	}
//
//	return 0;
//}
//
//

/*==========================================================================*
* FUNCTION : SmIsAllNoResponse
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-03-04 09:26
*==========================================================================*/
static BOOL SmIsAllNoResponse(void)
{
	int		i;
	//int		iSmduNum = g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue;
	//BOOL	bRst = TRUE;

	//for(i = 0; i < iSmduNum; i++)
	//{
	//	int		iAddr = GetSmAddrBySeqNo(i);
	//	if(g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_TIMES].iValue 
	//		< SM_MAX_INTERRUPT_TIMES)
	//	{
	//		bRst = FALSE;
	//		break;
	//	}
	//}

	//if(bRst)
	//{
	//	for(i = 0; i < iSmduNum; i++)
	//	{
	//		int		iAddr = GetSmAddrBySeqNo(i);
	//		g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_ST].iValue 
	//			= SM_COMM_ALL_INTERRUPT_ST;
	//	}	
	//}
	
	for(i = 0; i < MAX_NUM_SMDU; i++)
	{
		if(g_CanData.aRoughDataSmdu[i][SMDU_EXISTENCE].iValue == SMDU_EQUIP_EXISTENT
			&& g_CanData.aRoughDataSmdu[i][SMDU_INTERRUPT_TIMES].iValue < SM_MAX_INTERRUPT_TIMES)
		{
			return FALSE;
		}
	}
	return TRUE;
}

int SM_GetFuseEquipID(int iSequence)
{
	int iFuseEquipID = 0;
	int iTemp = iSequence; 

	if (iTemp > 2)
	{
		iFuseEquipID = (SMDU1_BATT_FUSE_EQUIPID + 11) + (iTemp - 3);
	}
	else
	{
		iFuseEquipID = SMDU1_BATT_FUSE_EQUIPID + iTemp;
	}
	return iFuseEquipID;
}

void SM_FuseEquipExistProc()
{
	int		i;
	VAR_VALUE_EX	sigValue;
	int		iFuseEquipID = 0;

	sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
	sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
	sigValue.pszSenderName = "CAN_SAMP";

	for(i = 0; i < MAX_NUM_SMDU; i++)
	{
		iFuseEquipID = SM_GetFuseEquipID(i);
		if (SMDU_EQUIP_EXISTENT == g_CanData.aRoughDataSmdu[i][SMDU_EXISTENCE].iValue)
		{
			
			//Brian's information 2016-03-07:LVDs work now when no Battery shunt, but now I have discovered that if 
			//I don’t have a “Load” shunt on the SMDU, I do not get “SMDU DC Fuse” alarms.  
			//This needs fixed.  Robert said you were going to fix this.  Please have this fixed for next test release.
			
			g_CanData.aRoughDataSmdu[i][SMDU_DC_FUSE_EQUIP_EXIST].iValue 
				= SMDU_EQUIP_EXISTENT;				
			
			sigValue.varValue.enumValue = SMDU_EQUIP_EXISTENT;
			DxiSetData(VAR_A_SIGNAL_VALUE,
					iFuseEquipID,
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, WORK_STATUS_SIG_ID),
					sizeof(VAR_VALUE_EX),
					&sigValue,
					0);
			
			//When SMDU exist, then LVD exist,CR#:0612-15-NSC 3.0;
			sigValue.varValue.enumValue = SMDU_EQUIP_EXISTENT;
			DxiSetData(VAR_A_SIGNAL_VALUE,
					SMDU1_LVD_EQUIPID + i,
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, WORK_STATUS_SIG_ID),
					sizeof(VAR_VALUE_EX),
					&sigValue,
					0);
		}
		else
		{	
			g_CanData.aRoughDataSmdu[i][SMDU_DC_FUSE_EQUIP_EXIST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;

			sigValue.varValue.enumValue = SMDU_EQUIP_NOT_EXISTENT;
			DxiSetData(VAR_A_SIGNAL_VALUE,
					iFuseEquipID,
					DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, WORK_STATUS_SIG_ID),
					sizeof(VAR_VALUE_EX),
					&sigValue,
					0);		
				
		}	
	}

	return ;
}


#define MAX_TIMES_SM_CMD00		3
/*==========================================================================*
* FUNCTION : SM_Sample
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:29
*==========================================================================*/
void SM_Sample(void)
{
	int		iAddr;
	int i = 0;
	g_bIsSMDUEIBMode = Is_SMDU_EIBMode();
	//if((g_CanData.CanCommInfo.SmduCommInfo.bNeedReconfig)
	//	|| ReceiveReconfigCmd())

	//3月22日
	//将重配置的判断放到main.c中了，因为SMDUP也需要这个信号
	if(g_CanData.CanCommInfo.SmduCommInfo.bNeedReconfig)
	{
		RT_UrgencySendCurrLimit();
		//printf("\n\n\n\n!!!!!!!!!!!!!!!Enter Reconfig Mode for SMDU !!!!!!!!!!!!!!\n\n\n\n");
		g_CanData.CanCommInfo.SmduCommInfo.bNeedReconfig = FALSE;
		SM_Reconfig();

		g_CanData.aRoughDataGroup[GROUP_SM_ALL_NO_RESPONSE].iValue 
			= SmIsAllNoResponse();
		return;
	}
	//printf("!!!!!!!!!Sampling for SMDU canceled!!!!");
	//return;

	for(iAddr = 0; iAddr < MAX_NUM_SMDU; iAddr++)
	{
		if(g_CanData.CanFlashData.aSmduInfo[iAddr].bExistence)
		{
			//printf("Current Sample SMDU Addr is %d",iAddr);
			SmSampleCmd00(iAddr);
			//SmSampleCmd21(iAddr);
			g_CanData.CanCommInfo.SmduCommInfo.iCmd00Counter[iAddr]++;

			if(g_CanData.CanCommInfo.SmduCommInfo.iCmd00Counter[iAddr] 
			>= MAX_TIMES_SM_CMD00) 
			{
				SmSampleCmd21(iAddr);
				SmSampleCmd22(iAddr);
				SmSampleCmd23(iAddr);

				g_CanData.CanCommInfo.SmduCommInfo.iCmd00Counter[iAddr] = 0; 
			}

			g_CanData.aRoughDataSmdu[iAddr][SMDU_EXISTENCE].iValue
				= SMDU_EQUIP_EXISTENT;


			//2010/06/09   是新版本的SMDU则需要采集 10命令和 采集 30命令
			if (g_CanData.CanCommInfo.SmduCommInfo.iNewestSmdu)
			{
				SmSampleCmd10(iAddr);
				SmSampleCmd30(iAddr);
				//added for shunt type setting
				SmSampleCmd31(iAddr);
			}
			else
			{
				//2010/06/09  旧版本，所有跟分流器5有关的全部-9999
				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
					= SMDU_EQUIP_NOT_EXISTENT;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_MV].iValue 
					= CAN_SAMP_INVALID_VALUE;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT5_A].iValue 
					= CAN_SAMP_INVALID_VALUE;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_OVER_CURR5].iValue 
					= CAN_SAMP_INVALID_VALUE;
				g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].iValue 
					= CAN_SAMP_INVALID_VALUE;

				g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT_CURR5].iValue 
					= CAN_SAMP_INVALID_VALUE;
			}
			ClrCurrWhenNoSamp(iAddr);
		}
		else
		{
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT1_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT2_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT3_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT4_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;

			//2010/06/09 设备不存在
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			//2010/06/09
			g_CanData.aRoughDataSmdu[iAddr][SMDU_LOAD_CURR5].iValue 
				= CAN_SAMP_INVALID_VALUE;
			g_CanData.aRoughDataSmdu[iAddr][SMDU_EXISTENCE].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			g_CanData.aRoughDataSmdu[iAddr][SMDU_INTERRUPT_ST].iValue 
				= SM_COMM_NORMAL_ST;

		}
		//if(VersionIsNewest123(iAddr))
		//{
		for(i = 0; i < 5; i++)
		{
				g_CanData.aRoughDataSmdu[iAddr][SMDU_SHUNT1_SETTING_STATE + i].iValue = 
					GetEnumSigValue(EQUIP_ID_SMDU_UNIT + iAddr, 
					SIG_TYPE_SETTING,
					SIGNAL_ID_SMDU_SHUNT_TYPE + i,
					"CAN_SAMP");
		}
		//}
	}

	g_CanData.aRoughDataGroup[GROUP_SM_ALL_NO_RESPONSE].iValue 
		= SmIsAllNoResponse();	

	ReDispatchShuntReadings();

	SM_FuseEquipExistProc();

	return;
}


/*==========================================================================*
* FUNCTION : SM_InitRoughValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:28
*==========================================================================*/
void SM_InitRoughValue(void)
{
	int			i, j;
	int			iRst;

	VAR_VALUE_EX	sigValue;

	if(g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue = 0;	
	}
	//g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue = 0;

	for(i = 0; i < MAX_NUM_SMDU; i++)
	{
		SMDU_SET_SIG* pSmduSetSig 
			= g_CanData.CanCommInfo.SmduCommInfo.aSmduSetSig + i;

		for(j = 0; j < SMDU_MAX_SIGNALS_NUM; j++)
		{
			g_CanData.aRoughDataSmdu[i][j].iValue = CAN_SAMP_INVALID_VALUE;
		}
		//The following two are added by Jimmy 20110618 for CR
		g_CanData.aRoughDataSmdu[i][SMDU_DIP_SWITCH3_1].iValue = 1; //在这里初始化拨码状态为1，不显示		
		//g_CanData.aRoughDataSmdu[i][SMDU_CURR5_ENABLE].iValue = 1; //在这里初始化确定CAN显示第5路分流器系数设置	

		pSmduSetSig->dwLvdCtlEnabled = SM_LVD_CTL_DISABLE;
		pSmduSetSig->dwComLedBlink = SM_COM_LED_BLIND;

		g_CanData.CanCommInfo.SmduCommInfo.iCmd00Counter[i] 
		= MAX_TIMES_SM_CMD00;

		/****************************************************************************
				做特殊处理的原因是，在Solution文件中，
					160
					161
					162			地址未连续。
					171
					172
					173
					174
					175									
		****************************************************************************/
		if (i > 2)
		{

			//原因不想记日志
			//SetEnumSigValue((SMDU1_BATT_FUSE_EQUIPID + 11) + (i - 2), 
			//	SIG_TYPE_SAMPLING,
			//	WORK_STATUS_SIG_ID,
			//	SMDU_EQUIP_NOT_EXISTENT,
			//	"CAN_SAMP");	

			//sigValue.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
			sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
			sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
			sigValue.pszSenderName = "can SMDU";
			sigValue.varValue.enumValue = SMDU_EQUIP_NOT_EXISTENT;

			iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
								(SMDU1_BATT_FUSE_EQUIPID + 11) + (i - 2),
								DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, WORK_STATUS_SIG_ID),
								sizeof(VAR_VALUE_EX),
								&sigValue,
								0);
		}
		else
		{
			//原因不想记日志
			//SetEnumSigValue(SMDU1_BATT_FUSE_EQUIPID + i, 
			//	SIG_TYPE_SAMPLING,
			//	WORK_STATUS_SIG_ID,
			//	SMDU_EQUIP_NOT_EXISTENT,
			//	"CAN_SAMP");

			//sigValue.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
			sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
			sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
			sigValue.pszSenderName = "can SMDU";
			sigValue.varValue.enumValue = SMDU_EQUIP_NOT_EXISTENT;

			iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
						SMDU1_BATT_FUSE_EQUIPID + i,
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, WORK_STATUS_SIG_ID),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);

		}
		//原因不想记日志
		//SetEnumSigValue(SMDU1_LVD_EQUIPID + i, 
		//			SIG_TYPE_SAMPLING,
		//			WORK_STATUS_SIG_ID,
		//			SMDU_EQUIP_NOT_EXISTENT,
		//			"CAN_SAMP");

		//sigValue.nSendDirectly = EQUIP_CTRL_SEND_CHECK_VALUE;
		sigValue.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
		sigValue.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		sigValue.pszSenderName = "can SMDU";
		sigValue.varValue.enumValue = SMDU_EQUIP_NOT_EXISTENT;

		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
						SMDU1_LVD_EQUIPID + i,
						DXI_MERGE_SIG_ID(SIG_TYPE_SAMPLING, WORK_STATUS_SIG_ID),
						sizeof(VAR_VALUE_EX),
						&sigValue,
						0);
	}

	//2010/06/09  初始化
	g_CanData.CanCommInfo.SmduCommInfo.iNewestSmdu = FALSE;

	return;
}


/*==========================================================================*
* FUNCTION : GetSmduAddrByChnNo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iChannelNo : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:28
*==========================================================================*/
static int GetSmduAddrByChnNo(int iChannelNo)
{
	int i;

	int iSeqNo = (iChannelNo - CNMR_MAX_SM_BROADCAST_CMD_CHN - 1) 
		/ MAX_CHN_NUM_PER_SM;
	//ASSERT(iSeqNo >= 0 && iSeqNo < MAX_NUM_CONVERT);

	for(i = 0; i < MAX_NUM_SMDU; i++)
	{
		if(iSeqNo == i)//g_CanData.aRoughDataSmdu[i][SMDU_SEQ_NO].iValue)
		{
			return i;
		}
	}

	return 0;
}




/*==========================================================================*
* FUNCTION : SM_SendCtlCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iChannelNo : 
*            float  fParam     : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:28
*==========================================================================*/
void SM_SendCtlCmd(int iChannelNo, float fParam)
{
	FLOAT_STRING		unValue;
	
	if(iChannelNo < CNMR_MAX_SM_BROADCAST_CMD_CHN)
	{
		switch(iChannelNo)
		{
			/*case CNSG_ESTOP_FUNCTION:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
			CAN_ADDR_FOR_BROADCAST,
			CAN_CMD_TYPE_BROADCAST,
			0,
			SM_VAL_TYPE_W_ESTOP_ENB,
			fParam);*/
			break;

		default:

			CAN_LOG_OUT(APP_LOG_WARNING, "Invalid control channel!\n");
			break;
		}
		//printf("The current CMD__SMDUaddr is invalid");
		return;
	}
	else
	{
		int		iSmduAddr = GetSmduAddrByChnNo(iChannelNo);
		int		iSubChnNo = ((iChannelNo - CNMR_MAX_SM_BROADCAST_CMD_CHN - 1)
			% MAX_CHN_NUM_PER_SM) 
			+ CNMR_MAX_SM_BROADCAST_CMD_CHN 
			+ 1;
		int iSeqNo = (iChannelNo - CNMR_MAX_SM_BROADCAST_CMD_CHN - 1) 
			/ MAX_CHN_NUM_PER_SM;
		//printf("The current CMD__SMDUaddr is %d &&& CMD_Chanel is %d",iSmduAddr,iSubChnNo);
		switch(iSubChnNo)
		{
		case CNSS_LVD1:

			if(FLOAT_EQUAL(fParam, 1.0))
			{
				unValue.abyValue[3] = 1;
			}
			else
			{
				unValue.abyValue[3] = 0;
			}
			unValue.abyValue[0] = 0;
			unValue.abyValue[1] = 0;
			unValue.abyValue[2] = 0;

			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_LVD1_CTL,
				unValue.fValue);

			Sleep(10);

			break;

		case CNSS_LVD2:

			if(FLOAT_EQUAL(fParam, 1.0))
			{
				unValue.abyValue[3] = 1;
			}
			else
			{
				unValue.abyValue[3] = 0;
			}
			unValue.abyValue[0] = 0;
			unValue.abyValue[1] = 0;
			unValue.abyValue[2] = 0;

			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_LVD2_CTL,
				unValue.fValue);

			Sleep(10);

			break;

		case CNSS_SHUNT1_VOLT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT1_MV,
				fParam);
			break;

		case CNSS_SHUNT2_VOLT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT2_MV,
				fParam);
			break;

		case CNSS_SHUNT3_VOLT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT3_MV,
				fParam);
			break;

		case CNSS_SHUNT4_VOLT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT4_MV,
				fParam);
			break;

		case CNSS_SHUNT1_CURR:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT1_A,
				fParam);
			break;

		case CNSS_SHUNT2_CURR:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT2_A,
				fParam);
			break;

		case CNSS_SHUNT3_CURR:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT3_A,
				fParam);
			break;

		case CNSS_SHUNT4_CURR:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT4_A,
				fParam);
			break;
			
		case CNSS_SHUNT5_VOLT:		//added by Jimmy Wu 20110621
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT5_MV,
				fParam);
			break;

		case CNSS_SHUNT5_CURR:		//added by Jimmy Wu 20110621
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_SHUNT5_A,
				fParam);
			break;

		case CNSS_BUS_OVER_VOLT_POINT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_OVER_VOLT,
				fParam);
			break;

		case CNSS_BUS_UNDER_VOLT_POINT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_UNDER_VOLT,
				fParam);
			break;

		case CNSS_SHUNT1_OVER_CURR_POINT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_OVER_CURR1,
				fParam);
			break;

		case CNSS_SHUNT2_OVER_CURR_POINT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_OVER_CURR2,
				fParam);
			break;

		case CNSS_SHUNT3_OVER_CURR_POINT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_OVER_CURR3,
				fParam);
			break;

		case CNSS_SHUNT4_OVER_CURR_POINT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_OVER_CURR4,
				fParam);
			break;

		case CNSS_LVD1_DISCONNECT_VOLT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_LVD1_VOLT,
				fParam);
			break;

		case CNSS_LVD1_RECONNECT_VOLT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_LVR1_VOLT,
				fParam);
			break;

		case CNSS_LVD2_DISCONNECT_VOLT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_LVD2_VOLT,
				fParam);
			break;

		case CNSS_LVD2_RECONNECT_VOLT:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_LVR2_VOLT,
				fParam);
			break;
		case CNSS_SET_UNIT_RATED_CAP:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
				(UINT)iSmduAddr,
				CAN_CMD_TYPE_P2P,
				0,
				SM_VAL_TYPE_W_RATED_CAP,
				fParam);

			//SM_Set_UnitCap(iSeqNo);
			break;
		default:

			CAN_LOG_OUT(APP_LOG_WARNING, "Invalid control channel!\n");
			break;
		}
	}
	return;

}


/*==========================================================================*
* FUNCTION : GetSmSeqNoByAddr
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:28
*==========================================================================*/
//static int GetSmSeqNoByAddr(int iAddr)
//{
//	return g_CanData.aRoughDataSmdu[iAddr][SMDU_SEQ_NO].iValue;
//}





enum	SM_PARAM_SIG_ID
{
	SM_PARAM_SIG_ID_RATED_CAP = 31,
	SM_PARAM_SIG_ID_OVER_VOLT = 5,
	SM_PARAM_SIG_ID_UNDER_VOLT = 6,
	SM_PARAM_SIG_ID_OVER_CURR = 45,
	SM_PARAM_SIG_ID_LVD1_VOLT = 1,
	SM_PARAM_SIG_ID_LVR1_VOLT = 2,
	SM_PARAM_SIG_ID_LVD2_VOLT = 6,
	SM_PARAM_SIG_ID_LVR2_VOLT = 7,
	SM_PARAM_SIG_ID_LVD1_VOLT24 = 51,
	SM_PARAM_SIG_ID_LVR1_VOLT24 = 52,
	SM_PARAM_SIG_ID_LVD2_VOLT24 = 56,
	SM_PARAM_SIG_ID_LVR2_VOLT24 = 57,

	SM_PARAM_SIG_ID_OVER_VOLT24 = 9,
	SM_PARAM_SIG_ID_UNDER_VOLT24 = 10,
	SM_PARAM_SIG_ID_UNIT_RATED_CAP = 10,  
	
	//added by Jimmy 20110616 for CR,setting Shunt size 1~5
	SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT = 21,
	SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR = 22,
	SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT = 23,
	SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR = 24,
	SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT = 25,
	SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR = 26,
	SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT = 27,
	SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR = 28,
	SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT = 29,
	SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR = 30,

	SM_PARAM_SIG_ID_UNIT_SHUNT1_STATE = 16,
	SM_PARAM_SIG_ID_UNIT_SHUNT2_STATE = 17,
	SM_PARAM_SIG_ID_UNIT_SHUNT3_STATE = 18,
	SM_PARAM_SIG_ID_UNIT_SHUNT4_STATE = 19,
	SM_PARAM_SIG_ID_UNIT_SHUNT5_STATE = 20,
	SM_PARAM_SIG_ID_END_FLAG = -1,
};

void SM_JudgeAndUnifyShuntType(int iAddr,PARAM_UNIFY_IDX SmParamIdxTemp,int iSettingRoughData)
{
	int uiParam = 0 , uiSampleValue = 0 , uiParamTemp = 0 ;
	uiSampleValue = g_CanData.aRoughDataSmdu[iAddr][SmParamIdxTemp.iRoughData].uiValue;
	uiParamTemp = g_CanData.aRoughDataSmdu[iAddr][iSettingRoughData].uiValue;
	//uiParamTemp = GetEnumSigValue(SmParamIdxTemp.iEquipId 
	//			+ iAddr * SmParamIdxTemp.iEquipIdDifference, 
	//			SIG_TYPE_SETTING,
	//			iSigId,
	//			"CAN_SAMP");
	
	switch (uiParamTemp )
	{
	case 0:
		uiParam = 0;
		break;
	//General、Load、Source as load in SMDU 
	case 1:
	case 2:
	case 4:
		uiParam = 2;
		break;
	//Battery as battery in SMDU
	case 3:
		uiParam = 1;
		break;

	default:
		uiParam = 0;
			break;

	}
	//set shunt type when the value is different
//printf("%d %d %d\n",iAddr, uiSampleValue , uiParam);
	if(uiSampleValue != uiParam)
	{
//printf("modify %X %d %d\n",SmParamIdxTemp.uiValueTypeL,uiParam,uiSampleValue);
		PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
			(UINT)iAddr,
			CAN_CMD_TYPE_P2P,
			0,
			SmParamIdxTemp.uiValueTypeL,
			(float)uiParam);
	}
}
/*==========================================================================*
* FUNCTION : SM_Param_Unify
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:27
*==========================================================================*/
void SM_Param_Unify(void)
{
	PARAM_UNIFY_IDX		SmParamIdx[] =
	{
		//{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_RATED_CAP,	SM_PARAM_SIG_ID_RATED_CAP,		SMDU_RATED_CAP,			SM_VAL_TYPE_W_RATED_CAP,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_RATED_CAP,	SM_PARAM_SIG_ID_UNIT_RATED_CAP,		SMDU_RATED_CAP,			SM_VAL_TYPE_W_RATED_CAP,	},
		
		//added by Jimmy Wu 20110616 for CR 		
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT,	SMDU_SHUNT1_MV,			SM_VAL_TYPE_W_SHUNT1_MV,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR,	SMDU_SHUNT1_A,			SM_VAL_TYPE_W_SHUNT1_A,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT,	SMDU_SHUNT2_MV,			SM_VAL_TYPE_W_SHUNT2_MV,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR,	SMDU_SHUNT2_A,			SM_VAL_TYPE_W_SHUNT2_A,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT,	SMDU_SHUNT3_MV,			SM_VAL_TYPE_W_SHUNT3_MV,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR,	SMDU_SHUNT3_A,			SM_VAL_TYPE_W_SHUNT3_A,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT,	SMDU_SHUNT4_MV,			SM_VAL_TYPE_W_SHUNT4_MV,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR,	SMDU_SHUNT4_A,			SM_VAL_TYPE_W_SHUNT4_A,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT,	SMDU_SHUNT5_MV,			SM_VAL_TYPE_W_SHUNT5_MV,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR,	SMDU_SHUNT5_A,			SM_VAL_TYPE_W_SHUNT5_A,		},
		//added by Jimmy
		//{EQUIP_ID_SMDU_BATT1,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT,	SMDU_SHUNT1_MV,			SM_VAL_TYPE_W_SHUNT1_MV,	},
		//{EQUIP_ID_SMDU_BATT1,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR,	SMDU_SHUNT1_A,			SM_VAL_TYPE_W_SHUNT1_A,		},
		//{EQUIP_ID_SMDU_BATT2,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT,	SMDU_SHUNT2_MV,			SM_VAL_TYPE_W_SHUNT2_MV,	},
		//{EQUIP_ID_SMDU_BATT2,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR,	SMDU_SHUNT2_A,			SM_VAL_TYPE_W_SHUNT2_A,		},
		//{EQUIP_ID_SMDU_BATT3,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT,	SMDU_SHUNT3_MV,			SM_VAL_TYPE_W_SHUNT3_MV,	},
		//{EQUIP_ID_SMDU_BATT3,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR,	SMDU_SHUNT3_A,			SM_VAL_TYPE_W_SHUNT3_A,		},
		//{EQUIP_ID_SMDU_BATT4,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT,	SMDU_SHUNT4_MV,			SM_VAL_TYPE_W_SHUNT4_MV,	},
		//{EQUIP_ID_SMDU_BATT4,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR,	SMDU_SHUNT4_A,			SM_VAL_TYPE_W_SHUNT4_A,		},
		//{EQUIP_ID_SMDU_BATT5,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT,	SMDU_SHUNT5_MV,			SM_VAL_TYPE_W_SHUNT5_MV,	},
		//{EQUIP_ID_SMDU_BATT5,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR,	SMDU_SHUNT5_A,			SM_VAL_TYPE_W_SHUNT5_A,		},

		{EQUIP_ID_DC_UNIT,	0,	SM_PARAM_SIG_ID_OVER_VOLT,	SM_PARAM_SIG_ID_OVER_VOLT24,		SMDU_OVER_VOLT,			SM_VAL_TYPE_W_OVER_VOLT,	},
		{EQUIP_ID_DC_UNIT,	0,	SM_PARAM_SIG_ID_UNDER_VOLT,	SM_PARAM_SIG_ID_UNDER_VOLT24,		SMDU_UNDER_VOLT,		SM_VAL_TYPE_W_UNDER_VOLT,	},	
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SMDU_OVER_CURR1,		SM_VAL_TYPE_W_OVER_CURR1,	},
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SMDU_OVER_CURR2,		SM_VAL_TYPE_W_OVER_CURR2,	},
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SMDU_OVER_CURR3,		SM_VAL_TYPE_W_OVER_CURR3,	},
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SMDU_OVER_CURR4,		SM_VAL_TYPE_W_OVER_CURR4,	},

		//2010/06/09
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SMDU_OVER_CURR5,		SM_VAL_TYPE_W_OVER_CURR5,	},

		{EQUIP_ID_SMDU_LVD,	1,	SM_PARAM_SIG_ID_LVD1_VOLT,	SM_PARAM_SIG_ID_LVD1_VOLT24,	SMDU_LVD1_DISCON_VOLT,			SM_VAL_TYPE_W_LVD1_VOLT,	},
		{EQUIP_ID_SMDU_LVD,	1,	SM_PARAM_SIG_ID_LVR1_VOLT,	SM_PARAM_SIG_ID_LVR1_VOLT24,	SMDU_LVD1_RECON_VOLT,			SM_VAL_TYPE_W_LVR1_VOLT,	},
		{EQUIP_ID_SMDU_LVD,	1,	SM_PARAM_SIG_ID_LVD2_VOLT,	SM_PARAM_SIG_ID_LVD2_VOLT24,	SMDU_LVD2_DISCON_VOLT,			SM_VAL_TYPE_W_LVD2_VOLT,	},
		{EQUIP_ID_SMDU_LVD,	1,	SM_PARAM_SIG_ID_LVR2_VOLT,	SM_PARAM_SIG_ID_LVR2_VOLT24,	SMDU_LVD2_RECON_VOLT,			SM_VAL_TYPE_W_LVR2_VOLT,	},

		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT1_STATE,SM_PARAM_SIG_ID_UNIT_SHUNT1_STATE,	SMDU_SHUNT1_STATE,			SM_VAL_TYPE_R_SHUNT1_STATE,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT2_STATE,SM_PARAM_SIG_ID_UNIT_SHUNT2_STATE,	SMDU_SHUNT2_STATE,			SM_VAL_TYPE_R_SHUNT2_STATE,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT3_STATE,SM_PARAM_SIG_ID_UNIT_SHUNT3_STATE,	SMDU_SHUNT3_STATE,			SM_VAL_TYPE_R_SHUNT3_STATE,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT4_STATE,SM_PARAM_SIG_ID_UNIT_SHUNT4_STATE,	SMDU_SHUNT4_STATE,			SM_VAL_TYPE_R_SHUNT4_STATE,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT5_STATE,SM_PARAM_SIG_ID_UNIT_SHUNT5_STATE,	SMDU_SHUNT5_STATE,			SM_VAL_TYPE_R_SHUNT5_STATE,	},
		{0, 0, SM_PARAM_SIG_ID_END_FLAG, SM_PARAM_SIG_ID_END_FLAG, 0, 0,},

	};

	int			i, j;	
	int		iSigId;
	int		uiParam = 0, uiSampleValue = 0,uiParamTemp;
	float	fParam;
	float	fSampleValue;
	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"SMDU Get SysVolt Level");
	for(i = 0; i < MAX_NUM_SMDU; i++)
	{
		//int		iSeqNo = GetSmSeqNoByAddr(i);
		
		//if(iSeqNo != CAN_SAMP_INVALID_VALUE)
		{
			if(g_CanData.CanFlashData.aSmduInfo[i].bExistence)
			{
				//added by Jimmy default value set to 0 (0 for normal,1 for alarm)
				g_CanData.aRoughDataSmdu[i][SMDU_SHUNT1_VALUE_CHANGED].iValue = 0;
				g_CanData.aRoughDataSmdu[i][SMDU_SHUNT2_VALUE_CHANGED].iValue = 0;
				g_CanData.aRoughDataSmdu[i][SMDU_SHUNT3_VALUE_CHANGED].iValue = 0;
				g_CanData.aRoughDataSmdu[i][SMDU_SHUNT4_VALUE_CHANGED].iValue = 0;
				g_CanData.aRoughDataSmdu[i][SMDU_SHUNT5_VALUE_CHANGED].iValue = 0;
				j = 0;
				while(SmParamIdx[j].iSigId != SM_PARAM_SIG_ID_END_FLAG)
				{

					//if(g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue > 35.0)
					if(0 == stVoltLevelState)//48 V
					{
						iSigId = SmParamIdx[j].iSigId;
					}
					else
					{
						iSigId = SmParamIdx[j].iSigId24V;

					}

					if(SmParamIdx[j].iEquipId >= EQUIP_ID_SMDU_UNIT && 
						SmParamIdx[j].iEquipId < (EQUIP_ID_SMDU_UNIT + 8)&& 
						SmParamIdx[j].iSigId >= SM_PARAM_SIG_ID_UNIT_SHUNT1_STATE && 
						SmParamIdx[j].iSigId <= SM_PARAM_SIG_ID_UNIT_SHUNT5_STATE)				
					{
						SM_JudgeAndUnifyShuntType(i,
							SmParamIdx[j],
							SMDU_SHUNT1_SETTING_STATE + (iSigId - SM_PARAM_SIG_ID_UNIT_SHUNT1_STATE));
					}
					else
					{
						fSampleValue = g_CanData.aRoughDataSmdu[i][SmParamIdx[j].iRoughData].fValue;
						/*
							因为老外新需求，SMDU单元容量需要GC运算，所以我在这里赋最大数值，不让其告警即可
							然后GC自己算再给其赋值
							这里注意，在众多配置文件中只有该信号的信号ID为10，所以用信号ID判断
						*/
						if ((SM_PARAM_SIG_ID_UNIT_RATED_CAP == SmParamIdx[j].iSigId))
						{
							fParam = 9998;//与WZP最大9999
						}

						else
						{
							fParam = GetFloatSigValue(SmParamIdx[j].iEquipId 
										+ i * SmParamIdx[j].iEquipIdDifference, 
										SIG_TYPE_SETTING,
										iSigId,
										"CAN_SAMP");
						}	
						//以下根据拨码开关状态决定是否执行参数同步,added by Jimmy 2011/06/19	
						if(SmParamIdx[j].iEquipId == EQUIP_ID_SMDU_UNIT && SmParamIdx[j].iSigId >= SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT && SmParamIdx[j].iSigId <= SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR)				
						{
							if(!g_CanData.CanCommInfo.SmduCommInfo.iNewestSmdu /*&& SmParamIdx[j].iSigId >= SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT*/) //if old version, do not set support set shunt size
							{
								j++;
								continue;
							}

							if(g_CanData.aRoughDataSmdu[i][SMDU_DIP_SWITCH3_1].iValue > 0) //0=on,1=off,2=old SMDU
							{
								j++;
								continue;
							}
							else
							{
								//printf("\n********************Current CMD for Unify is %03x  and set value is %f; address is %d; Samle value is %f\n",SmParamIdx[j].uiValueTypeL,fParam,i,fSampleValue);

								PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
									(UINT)i,
									CAN_CMD_TYPE_P2P,
									0,
									SmParamIdx[j].uiValueTypeL,
									fParam);
								j++;
								continue;
							}
						}
						//

						if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
						{
							PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
								(UINT)i,
								CAN_CMD_TYPE_P2P,
								0,
								SmParamIdx[j].uiValueTypeL,
								fParam);
						}
					}
					j++;
				}
			}
			//SM_Set_UnitCap(iSeqNo);
		}

	}
}

void SM_Set_UnitCap(int iSeq)
{
	int		j; 
	float		fParam;

	//
	if (iSeq < 0 || iSeq > 8 )
	{
		return;//Sequence Error SMDU inexistent
	}

	fParam = GetFloatSigValue(EQUIP_ID_SMDU_UNIT + iSeq, 
		SIG_TYPE_SETTING,
		SM_PARAM_SIG_ID_UNIT_RATED_CAP,
		"CAN_SAMP");

	for(j = 0; j < 4; j++)
	{
		SetFloatSigValue(EQUIP_ID_SMDU_BATT1 + j + iSeq*4, 
			SIG_TYPE_SETTING,
			SM_PARAM_SIG_ID_UNIT_RATED_CAP,
			fParam,
			"CAN_SAMP");

	}
#define	EQUIP_ID_SMDU_BATT5		659
	SetFloatSigValue(EQUIP_ID_SMDU_BATT5 + iSeq, 
						SIG_TYPE_SETTING,
						SM_PARAM_SIG_ID_UNIT_RATED_CAP,
						fParam,
						"CAN_SAMP");


}

/*==========================================================================*
* FUNCTION : GetInvalidSmAddr
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-03-04 09:27
*==========================================================================*/
static int GetInvalidSmAddr(void)
{
	int		i;

	for(i = 0; i < MAX_NUM_SMDU; i++)
	{
		if(g_CanData.aRoughDataSmdu[i][SMDU_EXISTENCE].iValue 
			== SMDU_EQUIP_NOT_EXISTENT)
		{
			return i;
		}
	}
	return -1;
}


#define SM_CH_END_FLAG			(-1)
#define	SMDU_NO_DATA			(-9999)


void Smdu_SetRunAsCanMode()
{
	SIG_ENUM	eRunningmode;
	if (g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue > 0)
	{
		eRunningmode = RUN_MODE_IS_CAN;
		SetEnumSigValue(SYS_EQUIP_ID, SIG_TYPE_SAMPLING,RUN_MODE_SIG_ID, eRunningmode,"Can mode");		
	}
	else
	{
		//not process!!
	}

	return;
}

/*==========================================================================*
* FUNCTION : SM_StuffChannel
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: ENUMSIGNALPROC  EnumProc : 
*            LPVOID          lpvoid   : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-15 09:10
*==========================================================================*/
void SM_StuffChannel(ENUMSIGNALPROC EnumProc,
					 LPVOID lpvoid)	
{
	int		i, j;
	int		iSmduNum = g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue;

	//if (s_gCANiGetCommBusMode != 0)//the running mode isn't can bus mode
	//{
	//	//TRACE(" SM_StuffChannel isn't can bus \n");
	//	return;
	//}

	CHANNEL_TO_ROUGH_DATA		SmGroupSig[] =
	{
		{SM_CH_SMDU_GROUP_STATE,	GROUP_SM_GROUP_STATE,	},
		{SM_CH_SMDU_NUM,			GROUP_SM_ACTUAL_NUM,	},

		{SM_CH_END_FLAG,			SM_CH_END_FLAG,			},
	};

	CHANNEL_TO_ROUGH_DATA		SmSig[] =
	{
		{SM_CH_LOAD_CURR1,			SMDU_LOAD_CURR1,		},
		{SM_CH_LOAD_CURR2,			SMDU_LOAD_CURR2,		},	
		{SM_CH_LOAD_CURR3,			SMDU_LOAD_CURR3,		},
		{SM_CH_LOAD_CURR4,			SMDU_LOAD_CURR4,		},
		{SM_CH_BUS_VOLT,			SMDU_BUS_VOLTAGE,		},


		//2010/06/09{,SMDU_LOAD_CURR5},
		//2010/06/09因为电流限流点在配置文件中未找到相应的配置。所以将改通道改为负载电流5
		{SM_CH_CURR_LIMIT,			SMDU_CURRENT_LIMIT,		},//2010/06/09 电流限流点，以前根本未配置
		{SM_CH_LOAD_CURR5,			SMDU_LOAD_CURR5,		},//2010/06/09

		{SM_CH_BATT_CURR1,			SMDU_BATT_CURR1,		},
		{SM_CH_BATT_CURR2,			SMDU_BATT_CURR2,		},
		{SM_CH_BATT_CURR3,			SMDU_BATT_CURR3,		},
		{SM_CH_BATT_CURR4,			SMDU_BATT_CURR4,		},
		{SM_CH_TEMPERATURE1,		SMDU_TEMPURATURE1,		},
		{SM_CH_TEMPERATURE2,		SMDU_TEMPURATURE2,		},
		{SM_CH_BATT_VOLT1,			SMDU_BATT_VOLT1,		},
		{SM_CH_BATT_VOLT2,			SMDU_BATT_VOLT2,		},
		{SM_CH_BATT_VOLT3,			SMDU_BATT_VOLT3,		},
		{SM_CH_BATT_VOLT4,			SMDU_BATT_VOLT4,		},
		{SM_CH_TOTAL_RUN_TIME,		SMDU_TOTAL_RUN_TIME,	},
		//{SM_CH_SERIAL_NO,			SMDU_SERIAL_NO,			},
		{SM_CH_SERIAL_NO_HIGH,		SMDU_SERIAL_NO_HIGH,	},
		{SM_CH_SERIAL_NO_LOW,		SMDU_SERIAL_NO_LOW,		},
		{SM_CH_BARCODE1,			SMDU_BARCODE1,			},
		{SM_CH_BARCODE2,			SMDU_BARCODE2,			},
		{SM_CH_BARCODE3,			SMDU_BARCODE3,			},
		{SM_CH_BARCODE4,			SMDU_BARCODE4,			},
		{SM_CH_VERSION_NO,			SMDU_VERSION_NO,		},
		{SM_CH_LOAD_FUSE16,			SMDU_LOAD_FUSE16,		},
		{SM_CH_LOAD_FUSE15,			SMDU_LOAD_FUSE15,		},
		{SM_CH_LOAD_FUSE14,			SMDU_LOAD_FUSE14,		},		
		{SM_CH_LOAD_FUSE13,			SMDU_LOAD_FUSE13,		},		
		{SM_CH_LOAD_FUSE12,			SMDU_LOAD_FUSE12,		},		
		{SM_CH_LOAD_FUSE11,			SMDU_LOAD_FUSE11,		},		
		{SM_CH_LOAD_FUSE10,			SMDU_LOAD_FUSE10,		},		
		{SM_CH_LOAD_FUSE9,			SMDU_LOAD_FUSE9,		},		
		{SM_CH_LOAD_FUSE8,			SMDU_LOAD_FUSE8,		},		
		{SM_CH_LOAD_FUSE7,			SMDU_LOAD_FUSE7,		},		
		{SM_CH_LOAD_FUSE6,			SMDU_LOAD_FUSE6,		},		
		{SM_CH_LOAD_FUSE5,			SMDU_LOAD_FUSE5,		},		
		{SM_CH_LOAD_FUSE4,			SMDU_LOAD_FUSE4,		},	
		{SM_CH_LOAD_FUSE3,			SMDU_LOAD_FUSE3,		},	
		{SM_CH_LOAD_FUSE2,			SMDU_LOAD_FUSE2,		},	
		{SM_CH_LOAD_FUSE1,			SMDU_LOAD_FUSE1,		},	
		{SM_CH_BATT_FUSE2,			SMDU_BATT_FUSE2,		},	
		{SM_CH_BATT_FUSE1,			SMDU_BATT_FUSE1,		},	
		{SM_CH_DC_VOLT_ST,			SMDU_BUS_VOLT_ST,		},	
		{SM_CH_LVD2_FAULT_ST,		SMDU_LVD2_FAULT_STATUS,	},	
		{SM_CH_LVD1_FAULT_ST,		SMDU_LVD1_FAULT_STATUS,	},	
		{SM_CH_LVD2_CMD_ST,			SMDU_LVD2_CMD_STATUS,	},
		{SM_CH_LVD1_CMD_ST,			SMDU_LVD1_CMD_STATUS,	},
		{SM_CH_BATT1_EXIST_ST,		SMDU_BATT1_EXIST_ST,	},
		{SM_CH_BATT2_EXIST_ST,		SMDU_BATT2_EXIST_ST,	},
		{SM_CH_BATT3_EXIST_ST,		SMDU_BATT3_EXIST_ST,	},
		{SM_CH_BATT4_EXIST_ST,		SMDU_BATT4_EXIST_ST,	},		
		{SM_CH_BATT_FUSE4,			SMDU_BATT_FUSE4,		},	
		{SM_CH_BATT_FUSE3,			SMDU_BATT_FUSE3,		},	
		{SM_CH_EXIST_ST,			SMDU_EXISTENCE,			},	
		{SM_CH_INTTERUPT_STATE,		SMDU_INTERRUPT_ST,	},
		{SM_CH_BATT1_CURR_ST,		SMDU_BATT1_CURR_ST,		},
		{SM_CH_BATT2_CURR_ST,		SMDU_BATT2_CURR_ST,		},
		{SM_CH_BATT3_CURR_ST,		SMDU_BATT3_CURR_ST,		},
		{SM_CH_BATT4_CURR_ST,		SMDU_BATT4_CURR_ST,		},
		{SM_CH_TOTAL_LOAD_CURR,		SMDU_TOTAL_LOAD_CURR,	},
		{SM_CH_COMM_ADDR,			SMDU_ADDRESS,			},
		
		/*
			因为老外新需求，SMDU单元容量需要GC运算，所以我在这里赋最大数值，不让其告警即可
			然后GC自己算再给其赋值
			这里注意，在众多配置文件中只有该信号的信号ID为10，所以用信号ID判断
			不填充，GC来DXISET
		*/

		//{SM_CH_RATED_CAP,			SMDU_RATED_CAP,			},
		{SM_CH_OVER_VOLT,			SMDU_OVER_VOLT,			},
		{SM_CH_UNDER_VOLT,			SMDU_UNDER_VOLT,		},
		//{SM_CH_BATT1_OVER_CURR,		SMDU_OVER_CURR1,		},
		//{SM_CH_BATT2_OVER_CURR,		SMDU_OVER_CURR2,		},
		//{SM_CH_BATT3_OVER_CURR,		SMDU_OVER_CURR3,		},
		//{SM_CH_BATT4_OVER_CURR,		SMDU_OVER_CURR4,		},
		{SM_CH_ABS_BATTCURR1,		SMDU_ABS_BATT_CURR1,		},
		{SM_CH_ABS_BATTCURR2,		SMDU_ABS_BATT_CURR2,		},
		{SM_CH_ABS_BATTCURR3,		SMDU_ABS_BATT_CURR3,		},
		{SM_CH_ABS_BATTCURR4,		SMDU_ABS_BATT_CURR4,		},
		{SM_CH_ABS_BATTCURR5,		SMDU_ABS_BATT_CURR5,		},

		//2010/06/09
		{SM_CH_BATT5_EXIST_ST,		SMDU_BATT5_EXIST_ST,	},//2010/06/09
		{SM_CH_BATT5_CURR_ST,		SMDU_BATT5_CURR_ST,		},//2010/06/09
		{SM_CH_BATT_CURR5,			SMDU_BATT_CURR5,		},//2010/06/09
		//不加进去，第5个分流器的电池电压会显示出来
		{SM_CH_BATT_VOLT5,			SMDU_BATT_VOLT5,		},//2010/06/09

		//added by Jimmy for CR (changing shunt para through ACU+ web)
		{SM_CH_SOFT_SWITCH,			SMDU_DIP_SWITCH3_1,		},
		//{SM_CH_CURR5_ENABLE,			SMDU_CURR5_ENABLE,		},
		
		//{SM_CH_SHUNT1_MV,			SMDU_SHUNT1_MV,			},
		//{SM_CH_SHUNT1_A,			SMDU_SHUNT1_A,			},
		//{SM_CH_SHUNT2_MV,			SMDU_SHUNT2_MV,			},
		//{SM_CH_SHUNT2_A,			SMDU_SHUNT2_A,			},
		//{SM_CH_SHUNT3_MV,			SMDU_SHUNT3_MV,			},
		//{SM_CH_SHUNT3_A,			SMDU_SHUNT3_A,			},
		//{SM_CH_SHUNT4_MV,			SMDU_SHUNT4_MV,			},
		//{SM_CH_SHUNT4_A,			SMDU_SHUNT4_A,			},
		//{SM_CH_SHUNT5_MV,			SMDU_SHUNT5_MV,			},
		//{SM_CH_SHUNT5_A,			SMDU_SHUNT5_A,			},
		//2011/08/02,因为CAN回读命令有误，故这里在CAN模式下不回读和告警，取消以下几个信号
		{SM_CH_SHUNT1_VALUE_CHANGED,		SMDU_SHUNT1_VALUE_CHANGED,	},
		{SM_CH_SHUNT2_VALUE_CHANGED,		SMDU_SHUNT2_VALUE_CHANGED,	},
		{SM_CH_SHUNT3_VALUE_CHANGED,		SMDU_SHUNT3_VALUE_CHANGED,	},
		{SM_CH_SHUNT4_VALUE_CHANGED,		SMDU_SHUNT4_VALUE_CHANGED,	},
		{SM_CH_SHUNT5_VALUE_CHANGED,		SMDU_SHUNT5_VALUE_CHANGED,	},

		{SM_CH_BATT_FUSE6,			SMDU_BATT_FUSE6,		},//EXU SMDU Battery Fuse6	
		{SM_CH_BATT_FUSE5,			SMDU_BATT_FUSE5,		},//EXU SMDU Battery Fuse5	


		{SMDU_CH_DC_FUSE_EQUIP_EXIST,		SMDU_DC_FUSE_EQUIP_EXIST,	},

		{SM_CH_END_FLAG,			SM_CH_END_FLAG,			},

	};							

	i = 0;
	while(SmGroupSig[i].iChannel != SM_CH_END_FLAG)
	{
		EnumProc(SmGroupSig[i].iChannel + 100, 
			g_CanData.aRoughDataGroup[SmGroupSig[i].iRoughData].fValue, 
			lpvoid );
		i++;
	}


	////Stuff the channels for SMDU unit signals
	//for(i = 0; i < iSmduNum; i++)
	//{
	//	int		iAddr = GetSmAddrBySeqNo(i);

	//	j = 0;
	//	while(SmSig[j].iChannel != SM_CH_END_FLAG)
	//	{
	//		EnumProc(i * MAX_CHN_NUM_PER_SM + SmSig[j].iChannel, 
	//			g_CanData.aRoughDataSmdu[iAddr][SmSig[j].iRoughData].fValue, 
	//			lpvoid );
	//	
	//		j++;
	//	}
	//}

	//for(i = iSmduNum; i < MAX_NUM_SMDU; i++)
	//{
	//	int		iAddr = GetInvalidSmAddr();

	//	if(iAddr < 0)
	//	{
	//		break;	//no invalid SM-DU
	//	}

	//	j = 0;
	//	while(SmSig[j].iChannel != SM_CH_END_FLAG)
	//	{
	//		EnumProc(i * MAX_CHN_NUM_PER_SM + SmSig[j].iChannel, 
	//			g_CanData.aRoughDataSmdu[iAddr][SmSig[j].iRoughData].fValue, 
	//			lpvoid );
	//		j++;
	//	}
	//}

	//2012.5.30 Jimmy 修改：根据老外CR要求，地址不再往前堆，而是采用显示地址=实际地址的做法，
	//故这里做相应的修改
	for(i = 0; i < MAX_NUM_SMDU; i++)
	{
		if(SMDU_EQUIP_EXISTENT == g_CanData.aRoughDataSmdu[i][SMDU_EXISTENCE].iValue)
		{
			j = 0;
			while(SmSig[j].iChannel != SM_CH_END_FLAG)
			{
				EnumProc(i * MAX_CHN_NUM_PER_SM + SmSig[j].iChannel  + 100, 
					g_CanData.aRoughDataSmdu[i][SmSig[j].iRoughData].fValue, 
					lpvoid );

				j++;
			}
		}
		else
		{
			j = 0;
			while(SmSig[j].iChannel != SM_CH_END_FLAG)
			{
				EnumProc(i * MAX_CHN_NUM_PER_SM + SmSig[j].iChannel  + 100, 
					g_CanData.aRoughDataSmdu[i][SmSig[j].iRoughData].fValue, 
					lpvoid );
				j++;
			}
		}

	}
	/*
	1)如果不是全部CAN设备不存在，则SM_Sample函数将会将UNIT存在状态赋值
	此时，上述for语句的GetInvalidSmAddr将不会返回-1，正常！！

	2)全部设备不存在：因为所有设备不存在，将会自动配置然后就RETURN；
	所以没机会给UNIT存在状态赋值，见SM_Sample函数.此时GetInvalidSmAddr返回-1
	而上边代码的for语句还都break，所以SMDU-UNIT的填充函数均未调用。
	导致上层那个一维数组pChannelData中的数值都为零，即设备存在，有问题
	所以下列代码将设备不存在状态填充上去
	2010/3/25
	*/
	SAMPLING_VALUE	stTempEquipExist;
	stTempEquipExist.iValue = SMDU_EQUIP_NOT_EXISTENT;
	if (0 == iSmduNum)
	{
		for (i = 0; i < MAX_NUM_SMDU; i++)
		{
			EnumProc(i * MAX_CHN_NUM_PER_SM + SM_CH_EXIST_ST  + 100, 
				stTempEquipExist.fValue, 
				lpvoid );

			EnumProc(i * MAX_CHN_NUM_PER_SM + SMDU_CH_DC_FUSE_EQUIP_EXIST  + 100, //熔丝设备的存在状态
				stTempEquipExist.fValue, 
				lpvoid );

			EnumProc(i * MAX_CHN_NUM_PER_SM + SM_CH_BATT1_EXIST_ST  + 100, 
				stTempEquipExist.fValue, 
				lpvoid );
			EnumProc(i * MAX_CHN_NUM_PER_SM + SM_CH_BATT2_EXIST_ST  + 100, 
				stTempEquipExist.fValue, 
				lpvoid );
			EnumProc(i * MAX_CHN_NUM_PER_SM + SM_CH_BATT3_EXIST_ST  + 100, 
				stTempEquipExist.fValue, 
				lpvoid );
			EnumProc(i * MAX_CHN_NUM_PER_SM + SM_CH_BATT4_EXIST_ST  + 100, 
				stTempEquipExist.fValue, 
				lpvoid );

			EnumProc(i * MAX_CHN_NUM_PER_SM + SM_CH_BATT5_EXIST_ST + 100, 
				stTempEquipExist.fValue, 
				lpvoid );
		}
	}

	Smdu_SetRunAsCanMode();
	return;
}
static BOOL Is_SMDU_EIBMode()
{
	if(GetEnumSigValue(SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		452,
		"CAN_SAMP") > 0)
	{
		return TRUE;
	}
	return FALSE;
}
static void ReDispatchShuntReadings()
{
	//printf("\n---------Entering SMDU-CAN ReDispatching--------------\n");
	static SIG_ENUM iLastShuntSetting[8][5]= {0};
	int i = 0;
	int j = 0;
	SIG_ENUM		enumValue = 0;
	BOOL bNeedRefreshPage = FALSE;
	//计算总负载电流
	float fTotalSMDUShuntCurr = 0.0;
#define SIG_ID_SHUNT1_SET_AS		16
#define EQUP_ID_SMDU1					107
#define SIG_ID_SHUNT1_READING		26

	int iBattExChann[5] =
	{SMDU_BATT1_EXIST_ST,
	SMDU_BATT2_EXIST_ST,
	SMDU_BATT3_EXIST_ST,
	SMDU_BATT4_EXIST_ST,
	SMDU_BATT5_EXIST_ST};

	for (i = 0; i < MAX_NUM_SMDU; i++)
	{
		if(SMDU_EQUIP_EXISTENT == g_CanData.aRoughDataSmdu[i][SMDU_EXISTENCE].iValue)
		{
			for(j = 0; j < 5; j++)
			{
				enumValue = GetEnumSigValue(EQUP_ID_SMDU1 + i, 
								SIG_TYPE_SETTING,
								SIG_ID_SHUNT1_SET_AS + j,
								"CAN_SAMP");
				if((enumValue !=iLastShuntSetting[i][j]) && 
					(enumValue==3||iLastShuntSetting[i][j]==3))//说明有电池发生变动，需要刷新页面
				{
					bNeedRefreshPage = TRUE;
				}
				iLastShuntSetting[i][j] = enumValue;
				g_CanData.aRoughDataSmdu[i][iBattExChann[j]].iValue 
						= SMDU_EQUIP_NOT_EXISTENT;
				//以下对ShuntReadings进行赋值
				if(enumValue == 0)//NotUsed
				{
					//SetFloatSigValue(EQUP_ID_SMDU1 + i, 
					//			SIG_TYPE_SAMPLING,
					//			SIG_ID_SHUNT1_READING + j,
					//			0.0,
					//			"CAN_SAMP");
					g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					g_CanData.aRoughDataSmdu[i][SMDU_LOAD_CURR1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
				}
				else if(enumValue == 1)//General
				{
					SetFloatSigValue(EQUP_ID_SMDU1 + i, 
								SIG_TYPE_SAMPLING,
								SIG_ID_SHUNT1_READING + j,
								g_fSMDUShuntReadings[i][j],
								"CAN_SAMP");
					g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					g_CanData.aRoughDataSmdu[i][SMDU_LOAD_CURR1+j].iValue
						= g_fSMDUShuntReadings[i][j];//CAN_SAMP_INVALID_VALUE;
				}
				else if(enumValue == 2)//Load
				{
					SetFloatSigValue(EQUP_ID_SMDU1 + i, 
								SIG_TYPE_SAMPLING,
								SIG_ID_SHUNT1_READING + j,
								g_fSMDUShuntReadings[i][j],
								"CAN_SAMP");
					g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					g_CanData.aRoughDataSmdu[i][SMDU_LOAD_CURR1+j].fValue
						= g_fSMDUShuntReadings[i][j];
					fTotalSMDUShuntCurr += g_fSMDUShuntReadings[i][j];
				}
				else if(enumValue == 3)//Battery
				{
					SetFloatSigValue(EQUP_ID_SMDU1 + i, 
								SIG_TYPE_SAMPLING,
								SIG_ID_SHUNT1_READING + j,
								g_fSMDUShuntReadings[i][j],
								"CAN_SAMP");
					g_CanData.aRoughDataSmdu[i][SMDU_BATT_CURR1+j].fValue
						= g_fSMDUShuntReadings[i][j];
					g_CanData.aRoughDataSmdu[i][SMDU_LOAD_CURR1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					g_CanData.aRoughDataSmdu[i][iBattExChann[j]].iValue 
						= SMDU_EQUIP_EXISTENT;
				}
				g_CanData.aRoughDataSmdu[i][SMDU_ABS_BATT_CURR1+j].fValue = ABS(g_fSMDUShuntReadings[i][j]);


			}
		}
	}
	if(g_bIsSMDUEIBMode)
	{
		//总负载电流计算
		SetFloatSigValue(SMDU_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				4,
				fTotalSMDUShuntCurr,
				"CAN_SAMP");
		static int s_dwChangeForWeb = 0;
		if(bNeedRefreshPage)
		{
		
			//for LCD
			SetDwordSigValue(SMDU_GROUP_EQUIPID, 
					SIG_TYPE_SAMPLING,
					CFG_CHANGED_SIG_ID,
					1,
					"CAN_SAMP");

			//for Web
			s_dwChangeForWeb++;
			if(s_dwChangeForWeb >= 255)
			{
				s_dwChangeForWeb = 0;
			}
			else
			{
				//
			}

			SetDwordSigValue(SMDU_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				CFG_CHANGED_SIG_ID + 1,
				s_dwChangeForWeb,
				"CAN_SAMP");

		}
	}

}

void SMDU_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	//以下为不往前堆时的算法：吴政武修改201205
	UNUSED(hComm);
	UNUSED(nUnitNo);
	int iAddr;
	PRODUCT_INFO *				pInfo;
	pInfo					= (PRODUCT_INFO*)pPI;
	for(iAddr = 0; iAddr < MAX_NUM_SMDU; iAddr++)
	{
		if(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE1].iValue 
			|| CAN_SMDU_NOT_EXIT == g_CanData.aRoughDataSmdu[iAddr][SMDU_EXISTENCE].iValue)
		{
			pInfo->bSigModelUsed = FALSE;			
			//printf("\n	This is ADDR=%d continue	\n",iAddr);
			pInfo++;
			continue;
		}
		else
		{
			//printf("\n	Can SM DU Samp: This is ADDR=%d  and bSigModeUsed=TRUE	\n",iAddr);
			pInfo->bSigModelUsed = TRUE;
			//算法太土了，不用了，下面重新解析
			//SMDUPUnpackProdctInfo(pPI);
		}

		BYTE	byTempBuf[4];
		UINT	uiTemp;
		INT32	iTemp;
		UINT	uiTemp11;
		UINT	uiTemp12;

#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

		pInfo->bSigModelUsed = TRUE;

		//1.特征字 不使用
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.串号低位	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_LOW].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_LOW].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_LOW].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_LOW].uiValue);

		//年
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//月
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//串号低位的低16字节 对应	##### 代表当月生产数量
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end	
		//printf("SMDU Serial:%s;%d\n", pInfo->szSerialNumber,g_CanData.aRoughDataSmdu[iAddr][SMDU_SERIAL_NO_LOW].uiValue);

		//3.版本号VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_VERSION_NO].uiValue);

		//模块版本号
		//H		A00形式，高字节按A=0 B=1类推，所以A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00按照实际数值存入低字节
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//软件版本		如若软件版本为1.30,保存内容为两两字节整型数130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end

		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE1].uiValue);

		//FF 用ASCII码表示,占用CODE1 和 CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T 表示产品类型,ASCII占用CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdu[iAddr][SMDU_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nSMTEMP bSigModelUsed:%d\n", pInfo->bSigModelUsed);

		pInfo++;

	}
}
/*==========================================================================*
* FUNCTION : UnpackSmduAlarm43
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr      : 
*            BYTE*  pbyAlmBuff : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:31
*==========================================================================*/
static void UnpackSmduAlarm43(int iAddr, BYTE* pbyAlmBuff)
{
	//The order is based on the protocol, do not change it.
	int		aiSigIdx[BYTES_OF_43 * BITS_PER_BYTE] = 
	{
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,

		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,

		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_BATT_FUSE6,
		SMDU_BATT_FUSE5,


		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
		SMDU_NOUSED,
	};

	int		i, j;
	
	/*for(i = 0; i < 4; i++)
	{
		BYTE	byMask = 0x80;
		BYTE	byRough = *(pbyAlmBuff + i);
		printf("%2X", byRough);
		for(j = 0; j < BITS_PER_BYTE; j++)
		{
			printf("[%d]",(byRough & byMask) ? 1 : 0);
			byMask >>= 1;
		}
		printf("\n");
	}
	printf("\n");*/

	for(i = 0; i < BYTES_OF_43; i++)
	{
		BYTE	byMask = 0x80;
		BYTE	byRough = *(pbyAlmBuff + i);
		if( i == 0)
		{
			g_CanData.aRoughDataSmdu[iAddr][SMDU_BATT5_CURR_ST].fValue 
				= byRough;
			continue;
		}
		for(j = 0; j < BITS_PER_BYTE; j++)
		{
			if(aiSigIdx[i * BITS_PER_BYTE + j] != SMDU_NOUSED)
			{
				int		iIdx = aiSigIdx[i * BITS_PER_BYTE + j];
				g_CanData.aRoughDataSmdu[iAddr][iIdx].iValue
					= (byRough & byMask) ? 1 : 0;
			}
			byMask >>= 1;
		}
	}	

	return ;
}



/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : can_smdu.h
 *  CREATOR  : Frank Cao                DATE: 2008-06-24 16:25
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __CAN_SAMPLER_SMDU_H
#define __CAN_SAMPLER_SMDU_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"

#define SM_MAX_INTERRUPT_TIMES	8   //2
#define CAN_SMDU_NOT_EXIT	1

#define	SYS_GROUP_EQUIPID		1
#define	SMDU_GROUP_EQUIPID		106

#define	SMDU1_BATT_FUSE_EQUIPID		160

#define	SMDU1_LVD_EQUIPID		188

#define SYS_RECONFIG_SIG_ID		71
//#define CFG_CHANGED_SIG_ID		2

#define WORK_STATUS_SIG_ID		100

#define SM_COMM_NORMAL_ST		0
#define SM_COMM_ALL_INTERRUPT_ST	1
#define SM_COMM_INTERRUPT_ST		2

#define SMDU_EQUIP_EXISTENT		0
#define SMDU_EQUIP_NOT_EXISTENT	1


#define CONTACTOR_TYPE_SINGLE	0
#define CONTACTOR_TYPE_DOUBLE	1

#define SM_CMD30_RCV_FRAMES_NUM		4
#define SM_CMD00_RCV_FRAMES_NUM		8
#define SM_CMD20_RCV_FRAMES_NUM		7
#define SM_CMD21_RCV_FRAMES_NUM		8
#define SM_CMD22_RCV_FRAMES_NUM		6
#define SM_CMD23_RCV_FRAMES_NUM		4
#define SM_CMD31_RCV_FRAMES_NUM		5


//Please refer to the document of CAN protocol
//Reading data command 00
#define SM_VAL_TYPE_R_LOAD_CURR1		0x01
#define SM_VAL_TYPE_R_LOAD_CURR2		0x02
#define SM_VAL_TYPE_R_LOAD_CURR3		0x03
#define SM_VAL_TYPE_R_LOAD_CURR4		0x04
#define SM_VAL_TYPE_R_BUS_VOLT			0x05
#define SM_VAL_TYPE_R_BATT_CURR1		0x07
#define SM_VAL_TYPE_R_BATT_CURR2		0x08
#define SM_VAL_TYPE_R_BATT_CURR3		0x09
#define SM_VAL_TYPE_R_BATT_CURR4		0x0A
#define SM_VAL_TYPE_R_ALARM1			0x40
#define SM_VAL_TYPE_R_ALARM2			0x41
#define SM_VAL_TYPE_R_ALARM3			0x42

//2010/06/09
#define SM_VAL_TYPE_R_SHUNT5_MV			0x31
#define SM_VAL_TYPE_R_SHUNT5_A			0x32
#define SM_VAL_TYPE_W_OVER_CURR5		0x33
#define SM_VAL_TYPE_W_OOVER_CURR5		0x3f

//2017/02/09
#define SM_VAL_TYPE_R_SHUNT1_STATE			0x70
#define SM_VAL_TYPE_R_SHUNT2_STATE			0x71
#define SM_VAL_TYPE_R_SHUNT3_STATE			0x72
#define SM_VAL_TYPE_R_SHUNT4_STATE			0x73
#define SM_VAL_TYPE_R_SHUNT5_STATE			0x74

//Reading data command 20
#define SM_VAL_TYPE_R_FEATURE			0x51
#define SM_VAL_TYPE_R_SN_LOW			0x54
#define SM_VAL_TYPE_R_VER_NO			0x56
#define SM_VAL_TYPE_R_BARCODE1			0x5A
#define SM_VAL_TYPE_R_BARCODE2			0x5B
#define SM_VAL_TYPE_R_BARCODE3			0x5C
#define SM_VAL_TYPE_R_BARCODE4			0x5D

//Reading data command 21, Currents Coefficient
#define SM_VAL_TYPE_R_CURR1_MV			0x23
#define SM_VAL_TYPE_R_CURR1_A			0x24
#define SM_VAL_TYPE_R_CURR2_MV			0x25
#define SM_VAL_TYPE_R_CURR2_A			0x26
#define SM_VAL_TYPE_R_CURR3_MV			0x27
#define SM_VAL_TYPE_R_CURR3_A			0x28
#define SM_VAL_TYPE_R_CURR4_MV			0x29
#define SM_VAL_TYPE_R_CURR4_A			0x2A

//Reading data command 22
#define SM_VAL_TYPE_R_RATED_CAP			0x22
#define SM_VAL_TYPE_R_OVER_VOLT			0x2B
#define SM_VAL_TYPE_R_UNDER_VOLT		0x2C
#define SM_VAL_TYPE_R_OVER_CURR1		0x2D
#define SM_VAL_TYPE_R_OVER_CURR2		0x2E
#define SM_VAL_TYPE_R_OVER_CURR3		0x2F
#define SM_VAL_TYPE_R_OVER_CURR4		0x30

//Reading data command 23
#define SM_VAL_TYPE_R_LVD1_VOLT			0x17
#define SM_VAL_TYPE_R_LVR1_VOLT			0x18
#define SM_VAL_TYPE_R_LVD2_VOLT			0x19
#define SM_VAL_TYPE_R_LVR2_VOLT			0x1A


//Control Command Value Type
#define SM_VAL_TYPE_W_ESTOP_ENB			0x16

#define SM_VAL_TYPE_W_LVD1_VOLT			0x17
#define SM_VAL_TYPE_W_LVR1_VOLT			0x18
#define SM_VAL_TYPE_W_LVD2_VOLT			0x19
#define SM_VAL_TYPE_W_LVR2_VOLT			0x1A

#define SM_VAL_TYPE_W_SHUNT1_MV			0x23
#define SM_VAL_TYPE_W_SHUNT1_A			0x24
#define SM_VAL_TYPE_W_SHUNT2_MV			0x25
#define SM_VAL_TYPE_W_SHUNT2_A			0x26
#define SM_VAL_TYPE_W_SHUNT3_MV			0x27
#define SM_VAL_TYPE_W_SHUNT3_A			0x28
#define SM_VAL_TYPE_W_SHUNT4_MV			0x29
#define SM_VAL_TYPE_W_SHUNT4_A			0x2A
//added 20110616 by Jimmy Wu
#define SM_VAL_TYPE_W_SHUNT5_MV			0x31
#define SM_VAL_TYPE_W_SHUNT5_A			0x32

#define SM_VAL_TYPE_W_RATED_CAP			0x22
#define SM_VAL_TYPE_W_OVER_VOLT			0x2B
#define SM_VAL_TYPE_W_UNDER_VOLT		0x2C
#define SM_VAL_TYPE_W_OVER_CURR1		0x2D
#define SM_VAL_TYPE_W_OVER_CURR2		0x2E
#define SM_VAL_TYPE_W_OVER_CURR3		0x2F
#define SM_VAL_TYPE_W_OVER_CURR4		0x30
//2010/06/09
#define SM_VAL_TYPE_W_OVER_CURR5		0x33

#define SM_VAL_TYPE_W_LVD1_CTL			0x3D
#define SM_VAL_TYPE_W_LVD2_CTL			0x3E

#define SM_VAL_TYPE_NO_TRIM_VOLT		0x80	//used while MsgType = 0x00

#define	SM_LVD_CTL_ENB					0
#define	SM_LVD_CTL_DISABLE				1
#define	SM_COM_LED_BLIND				0
#define	SM_COM_LED_BLINK				1
//Below definitions are used for telling ACU+ works on CAN or 485 ,same as rs485.h
#define		SYS_EQUIP_ID		1
#define		SIG_TYPE_SAMPLING	0
#define		RUN_MODE_SIG_ID		97	
#define		RUN_MODE_IS_485		1
#define		RUN_MODE_IS_CAN		2
//#define		RUN_MODE_NOT_USED	0

enum SMDU_SAMP_CHANNEL
{
	SM_CH_SMDU_GROUP_STATE = 6450,	//SM_DU Group State
	SM_CH_SMDU_NUM,					//SM_DU Board Number

	SM_CH_LOAD_CURR1 = 6501,		//Load 1 Current
	SM_CH_LOAD_CURR2,				//Load 2 Current
	SM_CH_LOAD_CURR3,				//Load 3 Current
	SM_CH_LOAD_CURR4,				//Load 4 Current

	SM_CH_BUS_VOLT,					//Bus Voltage
	SM_CH_CURR_LIMIT,				//Current Limitation
	SM_CH_BATT_CURR1,				//Battery 1 Current
	SM_CH_BATT_CURR2,				//Battery 2 Current
	SM_CH_BATT_CURR3,				//Battery 3 Current
	SM_CH_BATT_CURR4,				//Battery 4 Current

	SM_CH_TEMPERATURE1,				//Temperature 1
	SM_CH_TEMPERATURE2,				//Temperature 2
	SM_CH_BATT_VOLT1,				//Battery 1 Voltage
	SM_CH_BATT_VOLT2,				//Battery 2 Voltage
	SM_CH_BATT_VOLT3,				//Battery 3 Voltage
	SM_CH_BATT_VOLT4,				//Battery 4 Voltage
	SM_CH_TOTAL_RUN_TIME,			//Total Run Time in hour
	SM_CH_RESERVE,					//Reserve
	SM_CH_SERIAL_NO_HIGH,			//Serial No. High
	SM_CH_SERIAL_NO_LOW,			//Serial No. Low
	SM_CH_BARCODE1,					//Bar Code 1
	SM_CH_BARCODE2,					//Bar Code 2
	SM_CH_BARCODE3,					//Bar Code 3
	SM_CH_BARCODE4,					//Bar Code 4
	SM_CH_VERSION_NO,				//Version
	SM_CH_LOAD_FUSE16,				//Load Fuse 16
	SM_CH_LOAD_FUSE15,				//Load Fuse 15
	SM_CH_LOAD_FUSE14,				//Load Fuse 14
	SM_CH_LOAD_FUSE13,				//Load Fuse 13
	SM_CH_LOAD_FUSE12,				//Load Fuse 12
	SM_CH_LOAD_FUSE11,				//Load Fuse 11
	SM_CH_LOAD_FUSE10,				//Load Fuse 10
	SM_CH_LOAD_FUSE9,				//Load Fuse 9
	SM_CH_LOAD_FUSE8,				//Load Fuse 8
	SM_CH_LOAD_FUSE7,				//Load Fuse 7
	SM_CH_LOAD_FUSE6,				//Load Fuse 6
	SM_CH_LOAD_FUSE5,				//Load Fuse 5
	SM_CH_LOAD_FUSE4,				//Load Fuse 4
	SM_CH_LOAD_FUSE3,				//Load Fuse 3
	SM_CH_LOAD_FUSE2,				//Load Fuse 2
	SM_CH_LOAD_FUSE1,				//Load Fuse 1
	SM_CH_BATT_FUSE2,				//Battery Fuse 2
	SM_CH_BATT_FUSE1,				//Battery Fuse 1
	SM_CH_DC_VOLT_ST,				//DC Voltage Status
	SM_CH_LVD2_FAULT_ST,			//Lvd 2 FAULT Status
	SM_CH_LVD1_FAULT_ST,			//Lvd 1 FAULT Status
	SM_CH_LVD2_CMD_ST,				//Lvd 2 Command Status
	SM_CH_LVD1_CMD_ST,				//Lvd 1 Command Status
	SM_CH_BATT1_EXIST_ST,			//Is Battery 1 Existent
	SM_CH_BATT2_EXIST_ST,			//Is Battery 2 Existent
	SM_CH_BATT3_EXIST_ST,			//Is Battery 3 Existent
	SM_CH_BATT4_EXIST_ST,			//Is Battery 4 Existent
	SM_CH_BATT_FUSE4,				//Battery Fuse 4
	SM_CH_BATT_FUSE3,				//Battery Fuse 3
	SM_CH_EXIST_ST,					//Is Existent
	SM_CH_INTTERUPT_STATE,			//Communication Interrupt Times
	SM_CH_BATT1_CURR_ST,			//Battery 1 work state
	SM_CH_BATT2_CURR_ST,			//Battery 2 work state
	SM_CH_BATT3_CURR_ST,			//Battery 3 work state
	SM_CH_BATT4_CURR_ST,			//Battery 4 work state
	SM_CH_TOTAL_LOAD_CURR,			//All Load Current
	SM_CH_COMM_ADDR,				//Communication Address
	SM_CH_RATED_CAP,				//Battery Rated Capacity 
	SM_CH_OVER_VOLT,				//DC Over Voltage 
	SM_CH_UNDER_VOLT,				//DC Under Voltage
	SM_CH_ABS_BATTCURR1,//SM_CH_BATT1_OVER_CURR,			//Battery 1 Over Current 
	SM_CH_ABS_BATTCURR2,//SM_CH_BATT2_OVER_CURR,			//Battery 2 Over Current 
	SM_CH_ABS_BATTCURR3,//SM_CH_BATT3_OVER_CURR,			//Battery 3 Over Current 
	SM_CH_ABS_BATTCURR4,//SM_CH_BATT4_OVER_CURR,			//Battery 4 Over Current 
	
	SM_CH_ABS_BATTCURR5,//SM_CH_BATT5_OVER_CURR,				//Battery 5 Over Current  2010/06/09����Ҫ��ģ�ֻ������ͬ����
	SM_CH_BATT_FUSE6,				//Battery Fuse 6
	SM_CH_BATT_FUSE5,				//Battery Fuse 5

	SM_CH_BATT_CURR5  = 10500,			//Battery 5 Current 2010/06/09
	SM_CH_LOAD_CURR5  = 6506,			//Load 5 Current 2010/06/09
	SM_CH_BATT5_CURR_ST = 10550,
	SM_CH_BATT5_EXIST_ST = 10542,
	SM_CH_BATT_VOLT5  = 10506,			//Battery 5 VOLT 2010/06/09

	SM_CH_SOFT_SWITCH = 6574,			//added by Jimmy for DipSwitch status of setting Shunt size
	//SM_CH_CURR5_ENABLE = 6581,			//added by Jimmy for enable curr5 setting Shunt size

	//SM_CH_SHUNT1_MV = 6570,				//added by Jimmy for samp 1~5 Shunt size
	//SM_CH_SHUNT1_A,
	//SM_CH_SHUNT2_MV,
	//SM_CH_SHUNT2_A,
	//SM_CH_SHUNT3_MV,
	//SM_CH_SHUNT3_A,
	//SM_CH_SHUNT4_MV,
	//SM_CH_SHUNT4_A,
	//SM_CH_SHUNT5_MV,
	//SM_CH_SHUNT5_A,
	
	SM_CH_SHUNT1_VALUE_CHANGED = 6575,
	SM_CH_SHUNT2_VALUE_CHANGED,
	SM_CH_SHUNT3_VALUE_CHANGED,
	SM_CH_SHUNT4_VALUE_CHANGED,
	SM_CH_SHUNT5_VALUE_CHANGED,
	
	SMDU_CH_DC_FUSE_EQUIP_EXIST = 6580,
	//SM_CH_BATT_SHUNT5_MV = 10563,		//ע�ͣ����5·��صĵ�ַ�Ƚ����⣬���ﵥ������ By Jimmy 201107   6570+3993
	//SM_CH_BATT_SHUNT5_A,	
};



void SM_SendCtlCmd(int iChannelNo, float fParam);
void SM_InitRoughValue(void);
void SM_Param_Unify(void);
void SM_Sample(void);
void SM_StuffChannel(ENUMSIGNALPROC EnumProc,
					 LPVOID lpvoid);
void SMDUP_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI);
static INT32 s_gCANiGetCommBusMode;
#endif

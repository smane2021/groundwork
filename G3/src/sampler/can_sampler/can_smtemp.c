#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>


#include "can_sampler_main.h"
#include "can_smtemp.h"

extern	CAN_SAMPLER_DATA		g_CanData;
GLB_STRU_CAN_SAMP_SMTEMP		g_can_smtemp;

//Please refer to the protocol document
const FRAME_DATA_STRUCT st_SmpMSG00Data[]=
{
	//iValueType/iSignalType/iRoughDataIdx
	{0x0040, SMTEMP_SIG_TYPE_BITS, -1},
	{0x0001, SMTEMP_SIG_TYPE_FLOAT, ROUGH_SMTEMP_TEMP1},
	{0x0002, SMTEMP_SIG_TYPE_FLOAT, ROUGH_SMTEMP_TEMP2},	
	{0x0003, SMTEMP_SIG_TYPE_FLOAT, ROUGH_SMTEMP_TEMP3},
	{0x0004, SMTEMP_SIG_TYPE_FLOAT, ROUGH_SMTEMP_TEMP4},	
	{0x0005, SMTEMP_SIG_TYPE_FLOAT, ROUGH_SMTEMP_TEMP5},		
	{0x0006, SMTEMP_SIG_TYPE_FLOAT, ROUGH_SMTEMP_TEMP6},		
	{0x0007, SMTEMP_SIG_TYPE_FLOAT, ROUGH_SMTEMP_TEMP7},
	{0x0008, SMTEMP_SIG_TYPE_FLOAT, ROUGH_SMTEMP_TEMP8},		
	{0x0054, SMTEMP_SIG_TYPE_ULONG, ROUGH_SMTEMP_SERIAL_NO_LOW},		
	{0x0058, SMTEMP_SIG_TYPE_ULONG, ROUGH_SMTEMP_RUNTIME},	
};
const FRAME_DATA_STRUCT st_MSGProductInfo[]=
{
	//iValueType/iSignalType/iRoughDataIdx
	{0x005A, SMTEMP_SIG_TYPE_ULONG, ROUGH_SMTEMP_BARCODE1},
	{0x005B, SMTEMP_SIG_TYPE_ULONG, ROUGH_SMTEMP_BARCODE2},
	{0x005C, SMTEMP_SIG_TYPE_ULONG, ROUGH_SMTEMP_BARCODE3},
	{0x005D, SMTEMP_SIG_TYPE_ULONG, ROUGH_SMTEMP_BARCODE4},
	{0x0051, SMTEMP_SIG_TYPE_ULONG, ROUGH_SMTEMP_FEATURE},
	{0x0056, SMTEMP_SIG_TYPE_ULONG, ROUGH_SMTEMP_VERSION_NO},
};

/*********************************************************************************
*  
*  FUNCTION NAME : InitiRoughData
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-18 10:25:26
*  DESCRIPTION   : Initial all the raw data to prepare for sampling
*  
*  CHANGE HISTORY:
*  
***********************************************************************************/
void SMTemp_InitRoughValue(void)
{
	INT32 i,j;

	//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	EQUIP_INFO* pEquipInfo;
	int  nBufLen;
 
	g_can_smtemp.bConfigured = TRUE;
	
	if (DxiGetData(VAR_A_EQUIP_INFO,
		(SMTEMP_EQUIP_ID)-1,			//using the Group SMTEMP equip
		0,		
		&nBufLen,			
		&pEquipInfo,			
		0) == ERR_DXI_INVALID_EQUIP_ID)
	{
		g_can_smtemp.bConfigured = FALSE;
	
		//give WARNING msg
		AppLogOut("CAN_SAMP", APP_LOG_WARNING, 
			"SMTEMP equipment not configured in the solution.");
	}
	//G3_OPT end

	for (i = 0; i < CAN_SMTEMP_MAX_NUM;i++)
	{
		for (j = 0; j < ROUGH_SMTEMP_MAX_SIG_NUM;j++)
		{
			//最先初始化的时候不要给8Probes赋值-9999，不然会出现Web闪的情形
			if(j >= ROUGH_SMTEMP_TEMP1 && j <= ROUGH_SMTEMP_TEMP8)
			{
				g_can_smtemp.aRoughData[i][j].fValue = CAN_INVALID_TEMP_VALUE;
			}
			else if(j >= ROUGH_SMTEMP_TEMP1_STATUS && j <= ROUGH_SMTEMP_TEMP8_STATUS)
			{
				g_can_smtemp.aRoughData[i][j].iValue = SMTEMP_PROBE_NOT_INSTALL;
			}
			else
			{
				g_can_smtemp.aRoughData[i][j].iValue = CAN_SMTEMP_INVALID_VALUE;
			}
		}

		g_can_smtemp.aRoughData[i][ROUGH_SMTEMP_EXIST_STAT].iValue
			= CAN_SMTEMP_NON_EXIST;
		g_can_smtemp.aRoughData[i][ROUGH_SMTEMP_COMM_STAT].iValue
			= CAN_SMTEMP_COMM_OK;

		if (g_can_smtemp.bConfigured)   //condition added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
		{
			//initial the clr-alarm flag 
			SetDwordSigValue(SMTEMP_EQUIP_ID + i, 
				SIG_TYPE_SAMPLING,
				SIG_ID_CLS_ALARM,
				(DWORD)FLAG_NO_CLS_ALARM,
				"CAN_SAMP");
		}
	}
	g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_GROUP_STATE].iValue
		= CAN_SMTEMP_NON_EXIST;
	g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_NUM].iValue
		= 0;
}

/*==========================================================================*
* FUNCTION : PackAndSendSmduCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UINT   uiMsgType    : 
*            UINT   uiDestAddr   : 
*            UINT   uiCmdType    : 
*            UINT   uiValueTypeH : 
*            UINT   uiValueTypeL : 
*            float  fParam       : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Copy Frank Cao                DATE: 2010-02-25 16:27
*==========================================================================*/
LOCAL void PackAndSendCmd(UINT uiMsgType,
			       UINT uiDestAddr,
			       UINT uiCmdType,
			       UINT uiValueTypeH,
			       UINT uiValueTypeL,
			       float fParam)
{
	BYTE*	pbySendBuf = g_can_smtemp.cBySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMTEMP_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);
	/////////////////////for debug only
	//int i;
	//printf("\n*****The sent package is: ");
	//for(i=0;i<13;i++)
	//{
	//	printf("%2X",g_can_smtemp.cBySendBuf[i]);
	//
	//}
	//printf(" *****end\n ");
	////////////end 

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	

	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}


/*=============================================================================*
* FUNCTION: GetValueTypeFromAFrame(BYTE *pFrame)
* PURPOSE : Get uValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     INT32   iValueType
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 GetValueTypeFromAFrame(BYTE *pFrame)
{
	BYTE * pValue;
	INT32 iValueType	= 0;
	INT32 iValueTypeH	= 0;
	INT32 iValueTypeL	= 0;

	pValue = pFrame + CAN_FRAME_OFFSET_VAL_TYPE_H;
	iValueTypeH = *(pValue);
	pValue = pFrame + CAN_FRAME_OFFSET_VAL_TYPE_L;
	iValueTypeL = *(pValue);
	iValueType = (iValueTypeH << 8) + iValueTypeL;

	return iValueType;
}

/*=============================================================================*
* FUNCTION: GetUintFromAFrame(BYTE *pFrame)
* PURPOSE : Get uValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     UINT   uiValue
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL UINT GetUintFromAFrame(BYTE *pFrame)
{
	BYTE * pValue;
	pValue = pFrame + CAN_FRAME_OFFSET_VALUE;

	UINT uiValue = (((UINT)(pValue[0])) << 24)
		+ (((UINT)(pValue[1])) << 16)
		+ (((UINT)(pValue[2])) << 8)
		+ ((UINT)(pValue[3]));
	return uiValue;
}
/*=============================================================================*
* FUNCTION: SMTEMPGetfValue(BYTE *pFrame)
* PURPOSE : Get fValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     UINT   uiValue
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL float GetFloatFromAFrame(BYTE *pFrame)
{
	BYTE*				pValue;
	int					i;	
	FLOAT_STRING		unValue;
	pValue = pFrame + CAN_FRAME_OFFSET_VALUE;

	for(i = 0; i < 4; i++)
	{
		//printf(" %02X", pValue[i]);
		unValue.abyValue[4 - i - 1] = pValue[i];
	}
	//printf("float value = %f \n", unValue.fValue);

	return unValue.fValue;
}
/*=============================================================================*
* FUNCTION:	ClrSMTEMPIntrruptTimes
* PURPOSE : 
*					
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     void	g_can_SMTEMPlus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void ClrSMTEMPIntrruptTimes(INT32 iAddr)
{
	g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_COMM_STAT].iValue = CAN_SMTEMP_COMM_OK;
	g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_COMM_FAIL_TIMES].iValue = 0;
}
/*=============================================================================*
* FUNCTION:	IncSMTEMPIntrruptTimes
* PURPOSE : 
*					
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     void	g_can_SMTEMPlus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void IncSMTEMPIntrruptTimes(INT32 iAddr)
{
	if (g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_COMM_FAIL_TIMES].iValue < SMTEMP_B_COMMFAIL_TIME)
	{
		g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_COMM_FAIL_TIMES].iValue++;
		//printf("\n  ^^^  COMM_FAIL_TIMES = %d \n",g_can_SMTEMPlus.aRoughData[iTempAddr][SM_SMTEMP_COMM_FAIL_TIMES].iValue);
	}

	if (g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_COMM_FAIL_TIMES].iValue >= SMTEMP_B_COMMFAIL_TIME)
	{
		g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_COMM_STAT].iValue = CAN_SMTEMP_COMM_FAIL;
	}
}

/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-18 18:17:12
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL void SMTemp_GetAllAlam_From40(int iAddr, BYTE *pFrame)
{
	BYTE* pValue;
	int i,j;
	BYTE byValue[4] = {0};	
	pValue = pFrame + CAN_FRAME_OFFSET_VALUE;
	int iProbeStatus[8];
	for(j = 0; j < 8; j++)
	{
		iProbeStatus[j] = (int)SMTEMP_PROBE_NOT_INSTALL;//default state is: not install
	}
	for(i = 0; i < 4; i++)
	{
		//printf(" %02X", pValue[i]);
		byValue[i] = pValue[i];
	}
	g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_ID_OVERLAP].iValue = byValue[0] & 0x20;	
	g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_AD_FAIL].iValue = byValue[0] & 0x08;
	g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_EEPROM_FAIL].iValue = byValue[0] & 0x04;
	for(j = 0; j < 8; j++)
	{
		for(i = 1; i < 4; i++)
		{
			if(((byValue[i]) & (0x01 << j))!=0)
			{
				switch (i)
				{
				case 1:
					iProbeStatus[j] = (int)SMTEMP_PROBE_SHORTED;
					break;
				case 2:
					iProbeStatus[j] = (int)SMTEMP_PROBE_OPEN;
					break;
				case 3:
					iProbeStatus[j] = (int)SMTEMP_PROBE_NORMAL; //note that bit =1 means installed, bit =0 means not.
					break;
				default:
					//iProbeStatus[j] = (int)SMTEMP_PROBE_NORMAL;
					break;
				}
				break; //this continue means when short.open, will not tell if installed or not.
			}			
		}
	}
	for(i = 0; i < 8 ; i ++)
	{
		g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_TEMP1_STATUS + i].iValue = iProbeStatus[i];
		//printf("\nSMTemp%d : The value of Probe%d status is %d    %d\n",iAddr,i,iProbeStatus[i],g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_TEMP1_STATUS + i].iValue);
	}
};

/*********************************************************************************
*  
*  FUNCTION NAME : Proc_InvalidProbeValues
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-1-13 16:55:02
*  DESCRIPTION   : deal with abnormal probes
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL void Proc_InvalidProbeValues(int iAddr)
{
	int i;
	for(i = 0; i < 8 ; i ++)
	{	
		//here add the logic for de-valid the abnormal probe values
		if(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_TEMP1_STATUS + i].iValue!= SMTEMP_PROBE_NORMAL)
		{
			//printf("\n*******%d, %d",iAddr+1,i+1);
			g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_TEMP1 + i].iValue = CAN_SMTEMP_INVALID_DISP; //just reverse the disp white-back
		}		
	}

	return;
}
/*********************************************************************************
*  
*  FUNCTION NAME : Is_SMTemp_NeedCls_Alarm
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-11-7 10:55:52
*  DESCRIPTION   : Read the clsalarm flag to decide the 'update inventory' of 00cmd
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL BOOL Is_SMTemp_NeedCls_Alarm(int iAddr)
{
	if(FLAG_CLS_ALARM == GetDwordSigValue(SMTEMP_EQUIP_ID + iAddr, 
		SIG_TYPE_SAMPLING,
		SIG_ID_CLS_ALARM,
		"CAN_SAMP"))
	{		
		SetDwordSigValue(SMTEMP_EQUIP_ID + iAddr, 
			SIG_TYPE_SAMPLING,
			SIG_ID_CLS_ALARM,
			(DWORD)FLAG_NO_CLS_ALARM,
			"CAN_SAMP");
		return TRUE;
	}

	return FALSE;
}
/*********************************************************************************
*  
*  FUNCTION NAME : SMTEMPUpackCmd00
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-18 14:16:37
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL BOOL SMTEMPUpackCmd00(int iAddr,
			   int iReadLen,
			   BYTE* pbyRcvBuf
			   )
{
	INT32		i,j;
	BYTE*		pFrameofPackOffset;
	BOOL		bAllFrameSucceedFlag;
	float		fTempData;

	bAllFrameSucceedFlag = TRUE;
	
	UINT iCMDhasFrameNum; //the num of frames sent back by SM Temp
	iCMDhasFrameNum = SMTEMP_CMD00_RCV_FRAMES_NUM;//ITEM_OF(st_SmpMSG00Data);
	//printf("\n****&&&&&&&&&& %d &&&&&&&&&\n",iCMDhasFrameNum);
	INT32 iValueType ;
	//The MSG20 response package  is too short
	//if ((iReadLen/CAN_ONE_FRAME_LEN) < (sizeof(stAnalyseData)/sizeof(stAnalyseData[0]) - 1))
	if ((iReadLen/CAN_FRAME_LEN) < iCMDhasFrameNum)
	{
		//printf("	 The MSGXX response package  is too short \n");
		return -1;
	}

	for (i = 0; i < iCMDhasFrameNum; i++)
	{
		pFrameofPackOffset = pbyRcvBuf + CAN_FRAME_LEN * i;
		iValueType = GetValueTypeFromAFrame(pFrameofPackOffset);

		if (st_SmpMSG00Data[i].iValueType != iValueType)
		{
			bAllFrameSucceedFlag = FALSE;
			//printf(" \n\n\n*********************************The Frame receiving is fail ValueType = %d  \n",iValueType);
			continue;
		}
		else
		{
			if (st_SmpMSG00Data[i].iSignalType == SMTEMP_SIG_TYPE_ULONG)
			{
				g_can_smtemp.aRoughData[iAddr][st_SmpMSG00Data[i].iRoughDataIdx].uiValue
					= GetUintFromAFrame(pFrameofPackOffset);
			}
			else if (st_SmpMSG00Data[i].iSignalType == SMTEMP_SIG_TYPE_FLOAT)
			{
				fTempData = GetFloatFromAFrame(pFrameofPackOffset);
				//added for TR Jimmy 2012-02, SM Temp 的温度范围保证-127~127范围。
				if(fTempData >= 127 || fTempData <= -127)
				{				
					//printf("\n The current Temp is %f", fTempData);
					g_can_smtemp.aRoughData[iAddr][st_SmpMSG00Data[i].iRoughDataIdx].iValue
						= CAN_SMTEMP_INVALID_DISP;
				}
				else
				{
					// -1 A < fTempData < 1 A,voltage no problem!!				
					g_can_smtemp.aRoughData[iAddr][st_SmpMSG00Data[i].iRoughDataIdx].fValue
						= fTempData;	
					//printf(" \n!!!!!!%d:::: !!!! %f  \n",iAddr,fTempData);
				}


			}
			else if (st_SmpMSG00Data[i].iSignalType == SMTEMP_SIG_TYPE_BYTE)
			{
				//SMTemp_GetData_ViaBytes(pFrameofPackOffset);
			}
			else if (st_SmpMSG00Data[i].iSignalType == SMTEMP_SIG_TYPE_BITS)
			{
				SMTemp_GetAllAlam_From40(iAddr,pFrameofPackOffset);
			}
			else
			{
				//return FALSE;
			}
		}
	}
	Proc_InvalidProbeValues(iAddr);
	//printf(" ############ SMTEMP UpackCmdXX   FLAG =%d  \n",bAllFrameSucceedFlag);
	return bAllFrameSucceedFlag;

	//if (!bAllFrameSucceedFlag)
	//{
	//	return FALSE;
	//}
	//else
	//{
	//	printf(" ############ SMTEMP UpackCmdXX 20   okokok\n");
	//	return TRUE;
	//}
}


/*=============================================================================*
* FUNCTION: SMTEMP UnpackCmd01
* PURPOSE : Analyse the MSG 03 command
*			According to  VALUE_TYPE 0x0132
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_SMTEMPlus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/

/*=============================================================================*
* FUNCTION: SMTEMP SampCmd01
* PURPOSE : Sampling  data by MSG 0x03 to get Volt signals
*			It return 0x0132 Frame,But bit setting
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SMTEMPSampCmd00(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;


	////////////////////////
	if(Is_SMTemp_NeedCls_Alarm(iAddr))
	{
		//printf("\n******Oh Yes, SMTemp %d run here for get cls flag! ****\n",iAddr+1);
		PackAndSendCmd(MSG_TYPE_RQST_DATA00,
			(UINT)iAddr,
			CAN_CMD_TYPE_P2P,
			SMTEMP_FRAME_BYTE2_UPDATE,
			SMTEMP_FRAME_BYTE3,
			0.0);		
	}
	else
	{
		PackAndSendCmd(MSG_TYPE_RQST_DATA00,
			(UINT)iAddr,
			CAN_CMD_TYPE_P2P,
			SMTEMP_FRAME_BYTE2_NORMAL,
			SMTEMP_FRAME_BYTE3,
			0.0);
	}

	//PackAndSendCmd(MSG_TYPE_RQST_DATA00,
	//	(UINT)0xFF,
	//	CAN_CMD_TYPE_BROADCAST,
	//	SMTEMP_FRAME_BYTE2,
	//	SMTEMP_FRAME_BYTE3,
	//	0.0);
	////////////////////////
	Sleep(SMTEMP_CMD00_RCV_FRAMES_NUM * SMTEMP_FRAME_FRAME_INTTERVAL);
	
	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_can_smtemp.cByRecvBuf, 
		&iReadLen1st);
	//printf("\nAddr: %d; Len: %d",iAddr,iReadLen1st);
	if(iReadLen1st < (SMTEMP_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
	{
		Sleep(SMTEMP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 01 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
			g_can_smtemp.cByRecvBuf + iReadLen1st,
			&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= SMTEMP_CMD00_RCV_FRAMES_NUM)
	{
		//printf("\n^_^OK,Run here to unpack cmd 00!\n");
		if (SMTEMPUpackCmd00(iAddr,
			iReadLen1st + iReadLen2nd,
			g_can_smtemp.cByRecvBuf))
		{
			ClrSMTEMPIntrruptTimes(iAddr);
		}
		else
		{
			IncSMTEMPIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		IncSMTEMPIntrruptTimes(iAddr);
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN);
		//printf("\n********** 00 CMD LENGH = %d **********\n",iRST);
		return CAN_SAMPLE_FAIL;
	}

	//printf("#######  SMTEMP SampCmd01  ok \n");

	return CAN_SAMPLE_OK;
}


/*********************************************************************************
*  
*  FUNCTION NAME : SMTempUnpackBarCode
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-19 11:22:44
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void SMTempUnpackBarCode(int iAddr,
			    int iReadLen,
			    BYTE* pbyRcvBuf,
			    INT32 iSeq)
{
	int			i;
	UINT		uiBarcode;
	BYTE*		pValueBuf;
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		pValueBuf = pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_SMTEMP_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			//printf("/n!!!!Sample of Bar Code has beed canceled!!!!! and %s/n",*pValueBuf);
			continue;
		}

		if(*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L) 
			== st_MSGProductInfo[iSeq].iValueType)
		{
			uiBarcode = CAN_StringToUint(pValueBuf);	
			(g_can_smtemp.aRoughData[iAddr][st_MSGProductInfo[iSeq].iRoughDataIdx]).uiValue = uiBarcode;
			break;
		}
	}
}



/*********************************************************************************
*  
*  FUNCTION NAME : SMTempSampBarCode
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-19 10:33:41
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL INT32 SMTempSampBarCode(INT32 iAddr)
{
	int i;
	int iReadLen1st = 0;
	int iReadLen2nd = 0;
#define PRODUCT_INFO_ITEMS 6
	for(i = 0; i < PRODUCT_INFO_ITEMS; i++)	//4 barcode + Feature + version
	{
		PackAndSendCmd(MSG_TYPE_RQST_BYTE,
			(UINT)iAddr,
			CAN_CMD_TYPE_P2P,
			0,
			st_MSGProductInfo[i].iValueType,
			0.0);
		//In most situations, I guess converter responds in 15s
		Sleep(15);
		if(CAN_SAMPLE_OK == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_can_smtemp.cByRecvBuf,
			&iReadLen1st))
		{
			if(iReadLen1st < CAN_FRAME_LEN)
			{
				//After 15s+55s, if no respond, interrup times increase.
				Sleep(85);
				CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
					g_can_smtemp.cByRecvBuf + iReadLen1st,
					&iReadLen2nd);
			}
		}

		if((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN >= 1)
		{
			SMTempUnpackBarCode(iAddr,
				iReadLen1st + iReadLen2nd,
				g_can_smtemp.cByRecvBuf,
				i);
		}
		else
		{
			return CAN_SAMPLE_FAIL;
		}
	}

	return CAN_SAMPLE_OK;
}
/*=============================================================================*
* FUNCTION: SMTEMP _Reconfig(void)
* PURPOSE : Scan the SMTEM equipment
*			
* INPUT:	void
*     
*
* RETURN:
*			void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void SMTEMP_Reconfig(void)
{
	INT32		i;
	INT32		iAddr;
	BOOL		iRst;
	INT32		iSeqNum;
	//int		iNoExist_No;
	iSeqNum		= 0;

	//Max address CAN_SMTEMP_MAX_NUM
	for (iAddr = 0; iAddr < CAN_SMTEMP_MAX_NUM; iAddr++)	
	{

		iRst		= CAN_SAMPLE_FAIL;

		for (i = 0; i < SMTEMP_SCAN_TRY_TIME; i++)
		{
			iRst = SMTempSampBarCode(iAddr);

			if (CAN_SAMPLE_OK == iRst)
			{
				break;
			}
			else
			{
				Sleep(30);
				//CAN_ClearReadBuf();
			}
		}

		//iRst		= SMTEMPSampCmd20(iActualAddr);
		//printf("  reconfig 20   iActualAddr = %d \n",iActualAddr);
		if (CAN_SAMPLE_OK == iRst)
		{
			//printf("  SCAN 20 ok  EXIST ActualADDR =%d  iAddr =%d\n",iActualAddr,iAddr);

			g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_EXIST_STAT].iValue = CAN_SMTEMP_EXIST;
			g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SEQ_NUM].iValue = iSeqNum;
			g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_ADDR].iValue = iAddr;
			iSeqNum++;
		}
		else
		{
			g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_EXIST_STAT].iValue = CAN_SMTEMP_NON_EXIST;
			g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SEQ_NUM].iValue = -1;
			g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_ADDR].iValue = iAddr;
			//Continue!! Next address!!
		}

		Sleep(60);
	}


	//for (iAddr = 0; iAddr < CAN_SMTEMP_MAX_NUM; iAddr++)
	//{
	//	if(-1 == g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SEQ_NUM].iValue)
	//	{
	//		g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SEQ_NUM].iValue = iNoExist_No;
	//		iNoExist_No++;
	//	}
	//}
	//iSeqNum = 8; //for debug use only
	if(iSeqNum > 0)
	{
		g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_NUM].iValue = iSeqNum;
		g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_GROUP_STATE].iValue = CAN_SMTEMP_EXIST;
	}
	else
	{
		g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_NUM].iValue = 0;
		g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_GROUP_STATE].iValue = CAN_SMTEMP_NON_EXIST;
	}
}


/*LOCAL BOOL BarCodeDataIsValid(BYTE byData)
{
	if ((0x20 != byData) && (0xff != byData))
	{
		return TRUE;
	}

	return FALSE;
}*/

/*********************************************************************************
*  
*  FUNCTION NAME : SMTEMP_GetProdctInfo
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-19 11:52:03
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
void SMTemp_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	INT32	iTemp;
	INT32	iAddr;
	BYTE	byTempBuf[4];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *pInfo;
	pInfo = (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST		 6//##
#define		BARCODE_VV_OFST			 1//##

	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");

	for(iAddr = 0; iAddr < CAN_SMTEMP_MAX_NUM; iAddr++)
	{
		if ((CAN_SMTEMP_INVALID_VALUE == g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SERIAL_NO_LOW].iValue)
			&&(CAN_SMTEMP_INVALID_VALUE == g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_VERSION_NO].iValue)
			&&(CAN_SMTEMP_INVALID_VALUE == g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE1].iValue))
		{
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			//printf("\n	This is ADDR=%d continue	\n",iAddr);
			continue;
		}
		else
		{
			//printf("\n	Can SM Temp Samp: This is ADDR=%d  and bSigModeUsed=TRUE	\n",iAddr);
			pInfo->bSigModelUsed = TRUE;
		}

		//printf("\n	######## IN Convert addr=%d SERIAL_NO_LOW=%2x VERSION=%2x CODE1=%2x CODE2=%2x CODE3=%2x CODE4=%2x \n",
		//	iAddr,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_SERIAL_NO_LOW].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_VERSION].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE1].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE2].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE3].uiValue,
		//	g_CanData.aRoughDataConvert[iAddr][CONVERT_BARCODE4].uiValue);

		//1.特征字 不使用
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.串号低位	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SERIAL_NO_LOW].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SERIAL_NO_LOW].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SERIAL_NO_LOW].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SERIAL_NO_LOW].uiValue);

		//年
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//月
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//串号低位的低16字节 对应	##### 代表当月生产数量
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		

		//3.版本号VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_VERSION_NO].uiValue);

		//模块版本号
		//H		A00形式，高字节按A=0 B=1类推，所以A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00按照实际数值存入低字节
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//软件版本		如若软件版本为1.30,保存内容为两两字节整型数130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end

		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE1].uiValue);

		//FF 用ASCII码表示,占用CODE1 和 CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T 表示产品类型,ASCII占用CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nSMTEMP bSigModelUsed:%d\n", pInfo->bSigModelUsed);
		//printf("\nSMTEMP PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
		//printf("\nSMTEMP	version:%s\n", pInfo->szSWVersion); //1.20
		//printf("\nSMTEMP version:%s\n", pInfo->szHWVersion); //A00
		//printf("\nSMTEMP Serial Number:%s\n", pInfo->szSerialNumber);//03100500006

		pInfo++;
	}
	//printf("\n	########		END		Convert_	GetProdctInfo	########	\n");
}



static BOOL SMTEMP_AllIsNoRespone()
{
	INT32	iAddr;

	for (iAddr = 0; iAddr < CAN_SMTEMP_MAX_NUM; iAddr++)
	{
		if(CAN_SMTEMP_EXIST == g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_EXIST_STAT].iValue)
		{
			//只要有一个通信正常，就将返回FALSE
			if (CAN_SMTEMP_COMM_OK == g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_COMM_STAT].iValue)
			{
				return FALSE;
			}
		}
	}

	return TRUE;
}
/*=============================================================================*
* FUNCTION: SMTEMP _Reconfig(void)
* PURPOSE : Scan the SMTEMPlus equipment
*			
* INPUT:	void
*     
*
* RETURN:
*			void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMTemp_Sample(void)
{
	INT32			iAddr;

	//condition added for G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	if (!g_can_smtemp.bConfigured)   
	{
		return;
	}

	//if(g_can_SMTEMPlus.bNeedReconfig || ReceiveReconfigCmd())
	if(g_can_smtemp.bNeedReconfig)
	{
		RT_UrgencySendCurrLimit();
		SMTemp_InitRoughValue();

		//First running set TRUE in the function CAN_QueryInit()
		g_can_smtemp.bNeedReconfig = FALSE;
	
		//printf("1The SM Temp Num is %d\n",g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_NUM].iValue);

		SMTEMP_Reconfig();
	
		//printf("2The SM Temp Num is %d\n",g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_NUM].iValue);
		return;
	}
	//here I reduce the frequncy to improve CAN performance
	static int nCircle = 0;
	if(nCircle < 3) // Sample in every 3 periods
	{
		nCircle++;
		return;
	}
	else
	{
		nCircle = 0;
	}
	//printf("\n!!!!Run here to ssamp sm temp!\n");
	for (iAddr = 0; iAddr < CAN_SMTEMP_MAX_NUM; iAddr++)	
	{
		if(CAN_SMTEMP_EXIST == g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_EXIST_STAT].iValue)
		{
			SMTEMPSampCmd00(iAddr);
		}
		else
		{
			//Jump to Next Query,Now Don't Sampling Voltage
		}
	}

	//TRACE(" !!!!   IN  THE	SMTEMP _Sample    END \n");


	//不上填充，内部使用
	if (SMTEMP_AllIsNoRespone())
	{
		g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_G_COMM_STAT].iValue = CAN_SMTEMP_COMM_FAIL;
	}
	else
	{
		g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_G_COMM_STAT].iValue = CAN_SMTEMP_COMM_OK;
	}
}

/*********************************************************************************
*  
*  FUNCTION NAME : SMTemp_Swap_Addr_From_Seq
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-20 14:42:41
*  DESCRIPTION   : Fetch real addr from the 8 seq no and fill the values 
*		   due to the real values from each unit
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL INT32 SMTemp_Swap_Addr_From_Seq(int iSeq)
{
	int iAddr = 0;
	for(iAddr = 0; iAddr < CAN_SMTEMP_MAX_NUM; iAddr++)
	{
		if(g_can_smtemp.aRoughData[iAddr][ROUGH_SMTEMP_SEQ_NUM].iValue == iSeq)
		{
			return iAddr;
		}
	}
	return -1;
}

/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2011-10-26 15:00:18
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static int GetFirst_Invalid_SMTemp_Addr(void)
{
	int		i;

	for(i = 0; i < CAN_SMTEMP_MAX_NUM; i++)
	{
		if(g_can_smtemp.aRoughData[i][ROUGH_SMTEMP_EXIST_STAT].iValue 
			== CAN_SMTEMP_NON_EXIST)
		{
			return i;
		}
	}
	return -1;
}
/*=============================================================================*
* FUNCTION: SMTEMP _StuffChannel(ENUMSIGNALPROC EnumProc,
*											LPVOID lpvoid)	
* PURPOSE : 
*			
* INPUT:	void
*     
* RETURN:
*			void
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMTemp_StuffChannel(ENUMSIGNALPROC EnumProc,
			LPVOID lpvoid)	
{
	INT32				i,j,k;
	INT32				iVirtualAddr;
	//INT32				iExistSMTEMPNum;
	//SAMPLING_VALUE		stTempEquipExist;
	//iExistSMTEMPNum = g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_NUM].iValue;
	//TRACE(" !!!!   IN  THE SMTEMP_StuffChannel    START \n");

	CHANNEL_TO_ROUGH_DATA		SMTemp_Group_Sig[] =
	{
		{SM_CH_SMTEMP_GROUP_EXISTE_STATE,		ROUGH_GROUP_SMTEMP_GROUP_STATE,	},
		{SM_CH_SMTEMP_NUM,				ROUGH_GROUP_SMTEMP_NUM,			},
		//{SM_CH_SMTEMP_GROUP_COMM_STATE,	SM_SMTEMP_G_COMM_STAT	},//其实是不需要的	保留

		{SMTEMP_CH_END_FLAG,				SMTEMP_CH_END_FLAG,		},//end
	};

	CHANNEL_TO_ROUGH_DATA		SMTemp_Sig[] =
	{
		{SM_CH_SMTEMP_TEMP1,			ROUGH_SMTEMP_TEMP1,},
		{SM_CH_SMTEMP_TEMP2,			ROUGH_SMTEMP_TEMP2,},
		{SM_CH_SMTEMP_TEMP3,			ROUGH_SMTEMP_TEMP3,},
		{SM_CH_SMTEMP_TEMP4,			ROUGH_SMTEMP_TEMP4,},
		{SM_CH_SMTEMP_TEMP5,			ROUGH_SMTEMP_TEMP5,},
		{SM_CH_SMTEMP_TEMP6,			ROUGH_SMTEMP_TEMP6,},
		{SM_CH_SMTEMP_TEMP7,			ROUGH_SMTEMP_TEMP7,},
		{SM_CH_SMTEMP_TEMP8,			ROUGH_SMTEMP_TEMP8,},

		{SM_CH_SMTEMP_TEMP1_PROBE_STATUS,	ROUGH_SMTEMP_TEMP1_STATUS,},
		{SM_CH_SMTEMP_TEMP2_PROBE_STATUS,	ROUGH_SMTEMP_TEMP2_STATUS,},
		{SM_CH_SMTEMP_TEMP3_PROBE_STATUS,	ROUGH_SMTEMP_TEMP3_STATUS,},
		{SM_CH_SMTEMP_TEMP4_PROBE_STATUS,	ROUGH_SMTEMP_TEMP4_STATUS,},
		{SM_CH_SMTEMP_TEMP5_PROBE_STATUS,	ROUGH_SMTEMP_TEMP5_STATUS,},
		{SM_CH_SMTEMP_TEMP6_PROBE_STATUS,	ROUGH_SMTEMP_TEMP6_STATUS,},
		{SM_CH_SMTEMP_TEMP7_PROBE_STATUS,	ROUGH_SMTEMP_TEMP7_STATUS,},
		{SM_CH_SMTEMP_TEMP8_PROBE_STATUS,	ROUGH_SMTEMP_TEMP8_STATUS,},

		{SM_CH_SMTEMP_MODULE_ID_OVERLAP,	ROUGH_SMTEMP_ID_OVERLAP,},
		{SM_CH_SMTEMP_AD_CONV_FAILURE,		ROUGH_SMTEMP_AD_FAIL,},
		{SM_CH_SMTEMP_EEPROM_FAILURE,		ROUGH_SMTEMP_EEPROM_FAIL,},

		{SM_CH_SMTEMP_COMM_STAT,		ROUGH_SMTEMP_COMM_STAT,},
		{SM_CH_SMTEMP_EXIST_STAT,		ROUGH_SMTEMP_EXIST_STAT,},

		{SMTEMP_CH_END_FLAG,			SMTEMP_CH_END_FLAG}		//end
	};

	////////////////////////////////////////////////////////////////
	/***************************************************************/
	//Stuff Group signals
	i = 0;
	while(SMTemp_Group_Sig[i].iChannel != SMTEMP_CH_END_FLAG)
	{
		EnumProc(SMTemp_Group_Sig[i].iChannel, 
			g_can_smtemp.aRoughData_Group[SMTemp_Group_Sig[i].iRoughData].fValue, 
			lpvoid );

		//printf(" !!! @@ GGGGG sig seq i = %d  iValue = %d fValue = %f\n",SMTEMP_G_Sig[i].iChannel,
		//	g_can_SMTEMPlus.aRoughData[0][SMTEMP_G_Sig[i].iRoughData].iValue,
		//	g_can_SMTEMPlus.aRoughData[0][SMTEMP_G_Sig[i].iRoughData].fValue);

		i++;
	}

	int iFirst_NoExist_SMTemp = GetFirst_Invalid_SMTemp_Addr();

	//printf(" \n  @@@  iExistSMTEMPNum = %d \n",iExistSMTEMPNum);

	//Stuff exist SMTEMP signals
	for (i = 0; i < CAN_SMTEMP_MAX_NUM; i++)
	{
		//Do not use this way, just keep the addr and data the same index, 2011.11.22 Jimmy Wu
		//iVirtualAddr = SMTemp_Swap_Addr_From_Seq(i);
		iVirtualAddr = (g_can_smtemp.aRoughData[i][ROUGH_SMTEMP_EXIST_STAT].iValue == CAN_SMTEMP_EXIST) ? i : -1;
	///////////////
		//j = 0;
		//printf("\n---------BEGIN FOR SMTEMP%d---------\n",i);
		//printf("index \t RoughChan No \t RealChan No \t Value\n",i);
		//while(SMTemp_Sig[j].iChannel != SMTEMP_CH_END_FLAG)
		//{
		//	printf("%d \t %d \t %d \t\t %d\n",
		//				j+1,
		//				SMTemp_Sig[j].iRoughData,
		//				SMTemp_Sig[j].iChannel + i * SMTEMP_CH_PER_CH ,
		//				g_can_smtemp.aRoughData[i][SMTemp_Sig[j].iRoughData].iValue);

		//	j++;
		//}
		//printf("\n---------END---------\n",i);
	//////////////

		//Get Virtual address error!!
		j = 0;
		if (-1 == iVirtualAddr)
		{
			if(iFirst_NoExist_SMTemp < 0)
			{
				break;
			}
			while(SMTemp_Sig[j].iChannel != SMTEMP_CH_END_FLAG)
			{
				EnumProc(i * SMTEMP_CH_PER_CH + SMTemp_Sig[j].iChannel, 
					g_can_smtemp.aRoughData[iFirst_NoExist_SMTemp][SMTemp_Sig[j].iRoughData].fValue, 
					lpvoid );

				/*printf("Addr=%d SIG NUM= %d iValue= %d fValue= %f\n",
							iVirtualAddr,
							i * SMTEMP_CH_PER_CH + SMTemp_Sig[j].iChannel,
							g_can_smtemp.aRoughData[iVirtualAddr][SMTemp_Sig[j].iRoughData].iValue,
							g_can_smtemp.aRoughData[iVirtualAddr][SMTemp_Sig[j].iRoughData].fValue);*/

				j++;
			}
		}
		else
		{
			////according to CR, NO display temp value where not installed
			//int k;
			//for(k = 0;k < 8;k++)
			//{
			//	if(g_can_smtemp.aRoughData[iVirtualAddr][ROUGH_SMTEMP_TEMP1_STATUS + k].iValue == (int)SMTEMP_PROBE_NOT_INSTALL)
			//	{
			//		g_can_smtemp.aRoughData[iVirtualAddr][ROUGH_SMTEMP_TEMP1 + k].iValue = CAN_SMTEMP_INVALID_VALUE;
			//	}
			//}
			////end editing
			while(SMTemp_Sig[j].iChannel != SMTEMP_CH_END_FLAG)
			{
				//printf("%d):The rough value is %f\n",j+1,g_can_smtemp.aRoughData[iVirtualAddr][SMTemp_Sig[j].iRoughData].fValue);
				EnumProc(i * SMTEMP_CH_PER_CH + SMTemp_Sig[j].iChannel, 
					g_can_smtemp.aRoughData[iVirtualAddr][SMTemp_Sig[j].iRoughData].fValue, 
					lpvoid );

				j++;
			}
		}


	}

}
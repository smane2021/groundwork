/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : can_rectifier.c
 *  CREATOR  : Frank Cao                DATE: 2008-07-25 09:38
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "can_sampler_main.h"
#include "can_rectifier.h"

//changed by Frank Wu,2/30,20140217, for upgrading software of rectifier---start---
#if (GC_SUPPORT_RECT_DLOAD == 1)

#include <ctype.h>

#define BARCODE_LEN		12
BYTE CAN_SELF_ADDR = 0xf0;
char Barcode_S[MAX_NUM_RECT][BARCODE_LEN];
char Rect_Sn[MAX_NUM_RECT][11];
int RTSN_HIGH[MAX_NUM_RECT];

static int gs_iReadBinLen=0, gs_iReadBinStart=0;

static int ConvertCanHandleToId(IN int iCanHandle);
static int ConvertCanIdToHandle(IN int iCanNum);
static void PackAndSendDLoad(IN INT32 iCanHandle,
							IN UINT uiProtNo,
							IN UINT uiDestAddr,
							IN UINT uiLen,
							IN BYTE* pbyValue);
static void InitCanToNormal(void);
static void InitCanToDLoad(void);
static void InitCanToForceDLoad(void);
static BOOL IsRespondseOfCmd00(IN int iCanHandle, IN SIG_ENUM eStatus);
static BOOL ReadDLoadReplyFrames(IN int iCanHandle, 
								IN int iTimeout,
								IN UINT iReadInterval);
static BOOL IsRespondseOfDLoad(IN int iTimeout, IN int iCanHandle, IN int iAddr);
static BOOL IsRespondseOfForceDLoad(IN int iCanHandle, IN BYTE* bSend);
static BOOL	 RequestRectForceDLoad(IN int iTimeout,
									IN BYTE *pbAbyVal,
									OUT int *piCanhandle);
static BOOL ForceDLoad(OUT int *piErr);
static void RequestRectDLoad(IN int iCanHandle, IN int uiAddr);
static BOOL DLoad(IN int iCanHandle, IN int iAddr, IN int iInfoIndex, OUT int *piErr);
extern int RT_ForceDLoad(void);
extern int RT_DLoad(void);

#endif//GC_SUPPORT_RECT_DLOAD
//changed by Frank Wu,2/30,20140217, for upgrading software of rectifier---end---

extern float fRectVoltCAN1,fDeltaRectV;
extern CAN_SAMPLER_DATA	g_CanData;
static void Param24VDispByConvProc(void);
#define	INVALID_RECT_TYPE	(0xffffffff)
#ifdef GC_SUPPORT_MPPT
/*==========================================================================*
* FUNCTION : RtGetMaxNumber
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2012-04-17 11:12
*==========================================================================*/
static int RtGetMaxNumber(void)
{
	int iEnumState =GetEnumSigValue(MT_SYSTEM_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_MPPT_SETTING_MODE,
		"CAN_SAMP");
	/*if(iEnumState == 0)// Disable
	{
		return MAX_NUM_RECT;
	}
	else if(iEnumState == 2)//Mppt
	{
		return 0;
	}
	else //MPPT-RECT
	{
		return MAX_NUM_RECT - MAX_NUM_MPPT - 40;
	}*/
	if(iEnumState == 2)//Mppt
	{
		return 0;
	}
	else
	{
		return MAX_RECTNUM_CAN;
	}
	

	return;
}
#endif

//added by Jimmy Wu for clearing rectifier IDs
static BOOL g_bNeedClearRectIDs = FALSE;
static void ClrRectFalshInfo(void);//���λ�ú����?
static void ClrRectFalshInfo()
{
	int i = 0;
	for(i = 0; i < MAX_NUM_RECT; i++)
	{
		g_CanData.CanFlashData.aRectifierInfo[i].dwSerialNo = 0;
		g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo = -1;
		g_CanData.CanFlashData.aRectifierInfo[i].iAcPhase = 0;
	}
	CAN_WriteFlashData(&(g_CanData.CanFlashData));
	g_bNeedClearRectIDs = TRUE;
}
/*==========================================================================*
 * FUNCTION : PackAndSendRectCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: UINT   uiMsgType    : 
 *            UINT   uiDestAddr   : 
 *            UINT   uiCmdType    : 
 *            UINT   uiValueTypeH : 
 *            UINT   uiValueTypeL : 
 *            float  fParam       : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-08 13:57
 *==========================================================================*/
static void PackAndSendRectCmd(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	int		i;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_RECT_CONTROLLER,
				uiDestAddr, 
				CAN_SELF_ADDR,
				CAN_ERR_CODE,
				CAN_FRAME_DATA_LEN,
				uiCmdType,
				pbySendBuf);
	
	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
 
	for(i = 0; i < 4; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_VALUE + i] = *(pbyValue + i);
	}
	
	/*int		i;
	char	strOut[30];

	for(i = 0; i < 13; i++)
	{
		sprintf(strOut + i * 2, "%02X", *(pbySendBuf + i));
	}
	strOut[26] = 0;

	if(uiDestAddr == 0xff)
	{
		TRACE("******uiDestAddr = %d, Send string: %s \n", uiDestAddr, strOut);
	}*/

	write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);

	return;
}


#define RECT_NOUSED				-1
/*==========================================================================*
 * FUNCTION : UnpackRtAlarm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr      : 
 *            BYTE*  pbyAlmBuff : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:29
 *==========================================================================*/
static void UnpackRtAlarm(int iAddr,
						  BYTE* pbyAlmBuff)
{
	//The order is based on the protocol, do not change it.
	static int		s_aiSigIdx[BYTES_OF_ALM_INFO * BITS_PER_BYTE] = 
	{
		RECT_DC_OVER_VOLTAGE,
		RECT_OVER_TEMP_STATUS,
		RECT_RECTFIER_FAULT,
		RECT_PROTECTION,
		RECT_FAN_FAULT,
		RECT_EEPROM_FAULT,
		RECT_NOUSED,		//RECT_POWER_LIMITED_FOR_AC,
		RECT_NOUSED,		//RECT_POWER_LIMITED_FOR_TEMP,

		RECT_POWER_LIMITED,
		RECT_DC_ON_OFF_STATUS,
		RECT_FAN_FULL_SPEED,
		RECT_WALKIN_STATUS,
		RECT_AC_ON_OFF_STATUS,
		RECT_NOUSED,
		RECT_SHARE_CURR_STATUS,
		RECT_NOUSED,

		RECT_SEQ_START_STATUS,
		RECT_AC_UNDER_VOLT_STATUS,
		RECT_NOUSED,
		RECT_NOUSED,
		RECT_SEVERE_CURR_IMBALANCE,
		RECT_NOUSED,
		RECT_NOUSED,
		RECT_NOUSED,

		RECT_CURR_IMBALANCE,
		RECT_NOUSED,
		RECT_NOUSED,
		RECT_NOUSED,
		RECT_NOUSED,
		RECT_NOUSED,
		RECT_NOUSED,	//RECT_ESTOP_STATUS,
		RECT_NOUSED,
	};

	int		i, j;

	for(i = 0; i < BYTES_OF_ALM_INFO; i++)
	{
		BYTE	byMask = 0x80;
		BYTE	byRough = *(pbyAlmBuff + i);

		for(j = 0; j < BITS_PER_BYTE; j++)
		{
			if(s_aiSigIdx[i * BITS_PER_BYTE + j] != RECT_NOUSED)
			{
				int		iIdx = s_aiSigIdx[i * BITS_PER_BYTE + j];
				g_CanData.aRoughDataRect[iAddr][iIdx].iValue
					= (byRough & byMask) ? 1 : 0;	
			}
			byMask >>= 1;
		}
	}
	
	return;
}

enum	RT_CMD00_FRAME_ORDER
{
	RT_CMD00_ORDER_DC_VOLT = 0,
	RT_CMD00_ORDER_REAL_CURR,
	RT_CMD00_ORDER_CURR_LMT,
	RT_CMD00_ORDER_TEMPERATURE,
	RT_CMD00_ORDER_AC_VOLT,
	RT_CMD00_ORDER_DISP_CURR,
	RT_CMD00_ORDER_ALARM_BITS,
	RT_CMD00_ORDER_SN_LOW,
	RT_CMD00_ORDER_RUN_TIME,

	RT_CMD00_ORDER_FRAME_NUM,
};

/*==========================================================================*
 * FUNCTION : RtUnpackCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr     : 
 *            int    iReadLen  : 
 *            BYTE*  pbyRcvBuf : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Catherine Wang                DATE: 2013-6-13 
 *==========================================================================*/
static BOOL RtUnpackCfg(int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i, iFrameOrder=0;
	
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;
		float		fRunTime;
		BYTE*		pbyFrame = pbyRcvBuf + i * CAN_FRAME_LEN;
		
		if((CAN_GetProtNo(pbyFrame) != PROTNO_RECT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyFrame) != (UINT)iAddr)
			|| (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			continue;
		}
		iFrameOrder++;

		switch (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_L])
		{
		case RT_VAL_TYPE_R_DC_VOLT:
			RectCAN2[iAddr][RECT_DC_VOLTAGE].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameOrder++;
			break;
	
		case RT_VAL_TYPE_R_REAL_CURR:
			RectCAN2[iAddr][RECT_ACTUAL_CURRENT].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameOrder++;
			break;

		case RT_VAL_TYPE_R_CURR_LMT:
			RectCAN2[iAddr][RECT_CURRENT_LIMIT].fValue 
					= 100.0 * CAN_StringToFloat(pValueBuf);
			iFrameOrder++;
			break;

		case RT_VAL_TYPE_R_TEMPERATURE:
			RectCAN2[iAddr][RECT_TEMPERATURE].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameOrder++;
			break;
	
		case RT_VAL_TYPE_R_AC_VOLT:
			RectCAN2[iAddr][RECT_AC_VOLTAGE].fValue 
					= CAN_StringToFloat(pValueBuf);
			break;

		case RT_VAL_TYPE_R_DISP_CURR:
			RectCAN2[iAddr][RECT_CURRENT_FOR_DISP].fValue 
					= CAN_StringToFloat(pValueBuf);
			break;
	
		case RT_VAL_TYPE_R_ALARM_BITS:
			//UnpackRtAlarm(iAddr, pValueBuf);
			break;

		case RT_VAL_TYPE_R_SN_LOW:		
			RectCAN2[iAddr][RECT_SERIEL_NO].uiValue 
					= CAN_StringToUint(pValueBuf);
			break;

		case RT_VAL_TYPE_R_RUN_TIME:
			fRunTime = CAN_StringToFloat(pValueBuf);
			RectCAN2[iAddr][RECT_TOTAL_RUN_TIME].iValue = CAN_SAMP_INVALID_VALUE;
			break;
		default:
			break;
		}
	}

	if(iFrameOrder == 0)
		return FALSE;
	else
		return TRUE;
}
/*==========================================================================*
 * FUNCTION : RtUnpackCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr     : 
 *            int    iReadLen  : 
 *            BYTE*  pbyRcvBuf : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:29
 *==========================================================================*/
static BOOL RtUnpackCmd00(int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i, iSAddr;
	//UINT		uiSerialNo;
	int			iFrameOrder = RT_CMD00_ORDER_DC_VOLT;
//changed by Frank Wu,3/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
	UINT			uiTemp;
#endif//GC_SUPPORT_RECT_DLOAD
	char cLogCAN[80];
	int nLogLen = 0;
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;
		float		fRunTime;

		BYTE*		pbyFrame = pbyRcvBuf + i * CAN_FRAME_LEN;

		/*TRACE("**********i = %d  VAL_TYPE_H = %02X  VAL_TYPE_L = %02X  ",
			i,
			pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H],
			pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L]);*/
		if(iAddr >= g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1)
		{
			iSAddr = iAddr - g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;
		}
		else
		{
			iSAddr = iAddr;
		}
		
		if((CAN_GetProtNo(pbyFrame) != PROTNO_RECT_CONTROLLER)
			//|| (CAN_GetSrcAddr(pbyFrame) != (UINT)iAddr)
			|| (CAN_GetSrcAddr(pbyFrame) != (UINT)iSAddr)
			|| (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			/*TRACE(" It is NOT frame of mine!!!!\n");*/
			continue;
		}

		switch (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_L])
		{
		case RT_VAL_TYPE_R_DC_VOLT:
			g_CanData.aRoughDataRect[iAddr][RECT_DC_VOLTAGE].fValue 
					= CAN_StringToFloat(pValueBuf);
			//if(RT_CMD00_ORDER_DC_VOLT == iFrameOrder)
			//{
				iFrameOrder++;
			//}
			/*TRACE(" iAddr = %d, SigIdx = %d, RECT_DC_VOLTAGE = %.02f ok!!!\n", 
				iAddr,
				RECT_DC_VOLTAGE,
				g_CanData.aRoughDataRect[iAddr][RECT_DC_VOLTAGE].fValue);*/
			break;
	
		case RT_VAL_TYPE_R_REAL_CURR:
			g_CanData.aRoughDataRect[iAddr][RECT_ACTUAL_CURRENT].fValue 
					= CAN_StringToFloat(pValueBuf);
			//if(RT_CMD00_ORDER_REAL_CURR == iFrameOrder)
			//{
				iFrameOrder++;
			//}
			/*TRACE(" iAddr = %d, SigIdx = %d, RECT_ACTUAL_CURRENT = %.02f ok!!!\n", 
				iAddr,
				RECT_DC_VOLTAGE,
				g_CanData.aRoughDataRect[iAddr][RECT_ACTUAL_CURRENT].fValue);*/
			break;

		case RT_VAL_TYPE_R_CURR_LMT:
			g_CanData.aRoughDataRect[iAddr][RECT_CURRENT_LIMIT].fValue 
					= 100.0 * CAN_StringToFloat(pValueBuf);
			//if(RT_CMD00_ORDER_CURR_LMT == iFrameOrder)
			//{
				iFrameOrder++;
			//}
			/*TRACE(" iAddr = %d, SigIdx = %d, RECT_CURRENT_LIMIT = %.02f ok!!!\n", 
				iAddr,
				RECT_DC_VOLTAGE,
				g_CanData.aRoughDataRect[iAddr][RECT_CURRENT_LIMIT].fValue);*/

			break;

		case RT_VAL_TYPE_R_TEMPERATURE:
			g_CanData.aRoughDataRect[iAddr][RECT_TEMPERATURE].fValue 
					= CAN_StringToFloat(pValueBuf);
			//if(RT_CMD00_ORDER_TEMPERATURE == iFrameOrder)
			//{
				iFrameOrder++;
			//}
			/*TRACE(" iAddr = %d, SigIdx = %d, RECT_TEMPERATURE = %.02f ok!!!\n", 
				iAddr,
				RECT_DC_VOLTAGE,
				g_CanData.aRoughDataRect[iAddr][RECT_TEMPERATURE].fValue);*/

			break;
	
		case RT_VAL_TYPE_R_AC_VOLT:
			g_CanData.aRoughDataRect[iAddr][RECT_AC_VOLTAGE].fValue 
					= CAN_StringToFloat(pValueBuf);
			
			if(g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue
					== RT_SINGLE_PHASE)
			{
				int iAcPhase = g_CanData.CanFlashData.aRectifierInfo[iAddr].iAcPhase;

				if(iAcPhase < RT_AC_PHASE_NUM)
				{
					g_CanData.aRoughDataGroup[GROUP_RT_AC_P_A_VOLT + iAcPhase].fValue
						= CAN_StringToFloat(pValueBuf);
				}
			}
			else
			{
				g_CanData.aRoughDataRect[iAddr][RECT_AC_VOLTAGE].iValue 
					= CAN_SAMP_INVALID_VALUE;
			}

			//if(RT_CMD00_ORDER_AC_VOLT == iFrameOrder)
			//{
				iFrameOrder++;
			//}
			
			/*TRACE(" iAddr = %d, SigIdx = %d, RECT_AC_VOLTAGE = %.02f ok!!!\n", 
				iAddr,
				RECT_DC_VOLTAGE,
				g_CanData.aRoughDataRect[iAddr][RECT_AC_VOLTAGE].fValue);*/

			break;

		case RT_VAL_TYPE_R_DISP_CURR:
			g_CanData.aRoughDataRect[iAddr][RECT_CURRENT_FOR_DISP].fValue 
					= CAN_StringToFloat(pValueBuf);
			//if(RT_CMD00_ORDER_DISP_CURR == iFrameOrder)
			//{
				iFrameOrder++;
			//}

			/*TRACE(" iAddr = %d, SigIdx = %d, RECT_CURRENT_FOR_DISP = %.02f ok!!!\n", 
				iAddr,
				RECT_DC_VOLTAGE,
				g_CanData.aRoughDataRect[iAddr][RECT_CURRENT_FOR_DISP].fValue);*/

			break;
	
		case RT_VAL_TYPE_R_ALARM_BITS:
			UnpackRtAlarm(iAddr, pValueBuf);
			//if(RT_CMD00_ORDER_ALARM_BITS == iFrameOrder)
			//{
				iFrameOrder++;
			//}
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_ALARM_BITS ok!!!\n", iFrameOrder);


			break;

		case RT_VAL_TYPE_R_SN_LOW:		
			g_CanData.aRoughDataRect[iAddr][RECT_SERIEL_NO].uiValue 
					= CAN_StringToUint(pValueBuf);
//changed by Frank Wu,4/30,20140217, for upgrading software of rectifier		
#if (GC_SUPPORT_RECT_DLOAD == 1)			
			Rect_Sn[iAddr][2] = (*pValueBuf & 0x1f) / 10 + 0x30;
			Rect_Sn[iAddr][3] = (*pValueBuf & 0x1f) % 10 + 0x30;
			Rect_Sn[iAddr][4] = (*(pValueBuf + 1) & 0x0f) / 10 + 0x30;
			Rect_Sn[iAddr][5] = (*(pValueBuf + 1) & 0x0f) % 10 + 0x30;
			uiTemp =  g_CanData.aRoughDataRect[iAddr][RECT_SERIEL_NO].uiValue & 0xffff;
			Rect_Sn[iAddr][6] = uiTemp / 10000 + 0x30;
			uiTemp = uiTemp % 10000;
			Rect_Sn[iAddr][7] = uiTemp / 1000 + 0x30;
			uiTemp = uiTemp % 1000;
			Rect_Sn[iAddr][8] = uiTemp / 100 + 0x30;
			uiTemp = uiTemp % 100;
			Rect_Sn[iAddr][9] = uiTemp / 10 + 0x30;
			Rect_Sn[iAddr][10] = uiTemp % 10 + 0x30;
#endif//GC_SUPPORT_RECT_DLOAD

			//if(RT_CMD00_ORDER_SN_LOW == iFrameOrder)
			//{
				iFrameOrder++;
			//}
			/*TRACE(" iAddr = %d, SigIdx = %d, RECT_SERIEL_NO = %d--%d--%d--%d--%d ok!!!\n", 
				iAddr,
				RECT_SERIEL_NO,
				g_CanData.aRoughDataRect[iAddr][RECT_SERIEL_NO].uiValue,
				(((UINT)(pValueBuf[0])) << 8) + (((UINT)(pValueBuf[1])) << 0),
				(((UINT)(pValueBuf[2])) << 8) + ((UINT)(pValueBuf[3])),
				(((UINT)(pValueBuf[3])) << 8) + (((UINT)(pValueBuf[2])) << 0),
				(((UINT)(pValueBuf[1])) << 8) + ((UINT)(pValueBuf[0])));*/

			break;


		case RT_VAL_TYPE_R_RUN_TIME:
			fRunTime = CAN_StringToFloat(pValueBuf);
			if(g_CanData.aRoughDataRect[iAddr][RECT_TOTAL_RUN_TIME].iValue
					== CAN_SAMP_INVALID_VALUE)
			{
				g_CanData.aRoughDataRect[iAddr][RECT_TOTAL_RUN_TIME].uiValue
						= (uint)fRunTime;
			}
			//if(RT_CMD00_ORDER_RUN_TIME == iFrameOrder)
			//{
				iFrameOrder++;
			//}
			/*TRACE(" iAddr = %d, SigIdx = %d, RECT_TOTAL_RUN_TIME = %d ok!!!\n", 
				iAddr,
				RT_VAL_TYPE_R_RUN_TIME,
				g_CanData.aRoughDataRect[iAddr][RECT_TOTAL_RUN_TIME].uiValue);*/

			break;
		default:
			break;
		}
	}

	if(iFrameOrder != RT_CMD00_ORDER_FRAME_NUM)
	{
		nLogLen = snprintf(cLogCAN, sizeof(cLogCAN), "L%d,F%d",
			iReadLen, iFrameOrder);
		if(iReadLen >= CAN_FRAME_LEN)
		{
			nLogLen += snprintf(cLogCAN + nLogLen, sizeof(cLogCAN) - nLogLen, "[%2X%2X%2X%2X,%2X%2X%2X%2X,%2X%2X%2X%2X,%X]",
				pbyRcvBuf[0],pbyRcvBuf[1],pbyRcvBuf[2],pbyRcvBuf[3],pbyRcvBuf[4],pbyRcvBuf[5],pbyRcvBuf[6],
				pbyRcvBuf[7],pbyRcvBuf[8],pbyRcvBuf[9],pbyRcvBuf[10],pbyRcvBuf[11],pbyRcvBuf[12]
			);
		}
		if(iReadLen >= CAN_FRAME_LEN * 2)
		{
			nLogLen += snprintf(cLogCAN + nLogLen, sizeof(cLogCAN) - nLogLen, "[%2X%2X%2X%2X,%2X%2X%2X%2X,%2X%2X%2X%2X,%X]",
				pbyRcvBuf[0+13],pbyRcvBuf[1+13],pbyRcvBuf[2+13],pbyRcvBuf[3+13],pbyRcvBuf[4+13],pbyRcvBuf[5+13],pbyRcvBuf[6+13],
				pbyRcvBuf[7+13],pbyRcvBuf[8+13],pbyRcvBuf[9+13],pbyRcvBuf[10+13],pbyRcvBuf[11+13],pbyRcvBuf[12+13]
			);
		}
		AppLogOut("SAMP", 
				APP_LOG_INFO, 
				"RT SAMP %s\n",
				cLogCAN);		
	}

	if(iFrameOrder > 0)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


enum	RT_CMD10_FRAME_ORDER
{
	RT_CMD10_ORDER_P_A_VOLT = 0,
	RT_CMD10_ORDER_P_B_VOLT,
	RT_CMD10_ORDER_P_C_VOLT,
	RT_CMD10_ORDER_ALARM_BITS,

	RT_CMD10_ORDER_FRAME_NUM,
};
/*==========================================================================*
 * FUNCTION : RtUnpackCmd10
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr     : 
 *            int    iReadLen  : 
 *            BYTE*  pbyRcvBuf : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:29
 *==========================================================================*/
static BOOL RtUnpackCmd10(int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i;
	int			iFrameOrder = RT_CMD10_ORDER_P_A_VOLT;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;
		
		/*TRACE("**********i = %d  VAL_TYPE_H = %02X  VAL_TYPE_L = %02X  ",
			i,
			pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H],
			pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L]);*/

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_RECT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
				!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
				!= 0))
		{
			/*TRACE(" It is NOT frame of mine!!!!\n");*/
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case RT_VAL_TYPE_R_P_A_VOLT:
			g_CanData.aRoughDataRect[iAddr][RECT_PH_A_VOLTAGE].fValue 
					= CAN_StringToFloat(pValueBuf);
			/*if(iAddr == 0)
			{
				g_CanData.aRoughDataGroup[GROUP_RT_AC_P_AB_VOLT].fValue
					= g_CanData.aRoughDataRect[0][RECT_PH_A_VOLTAGE].fValue;
			}*/
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_P_A_VOLT ok!!!\n", iFrameOrder);
			if(RT_CMD10_ORDER_P_A_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		case RT_VAL_TYPE_R_P_B_VOLT:
			g_CanData.aRoughDataRect[iAddr][RECT_PH_B_VOLTAGE].fValue 
					= CAN_StringToFloat(pValueBuf);
			/*if(iAddr == 0)
			{
				g_CanData.aRoughDataGroup[GROUP_RT_AC_P_BC_VOLT].fValue
					= g_CanData.aRoughDataRect[0][RECT_PH_B_VOLTAGE].fValue;
			}*/
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_P_B_VOLT ok!!!\n", iFrameOrder);
			if(RT_CMD10_ORDER_P_B_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case RT_VAL_TYPE_R_P_C_VOLT:
			g_CanData.aRoughDataRect[iAddr][RECT_PH_C_VOLTAGE].fValue 
					= CAN_StringToFloat(pValueBuf);
			/*if(iAddr == 0)
			{
				g_CanData.aRoughDataGroup[GROUP_RT_AC_P_CA_VOLT].fValue
					= g_CanData.aRoughDataRect[0][RECT_PH_C_VOLTAGE].fValue;
			}*/
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_P_C_VOLT ok!!!\n", iFrameOrder);
			if(RT_CMD10_ORDER_P_B_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		case RT_VAL_TYPE_R_ALARM_BITS:
			UnpackRtAlarm(iAddr, pValueBuf);
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_ALARM_BITS ok!!!\n", iFrameOrder);
			if(RT_CMD10_ORDER_ALARM_BITS == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}

	if(RT_CMD10_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}



enum	CMD20_FRAME_ORDER
{
	CMD20_ORDER_RATED_VOLT = 0,
	CMD20_ORDER_RATED_POWER,
	CMD20_ORDER_RATED_CURR,
	CMD20_ORDER_RATED_INPUT_VOLT,
	CMD20_ORDER_FEATURE,
	CMD20_ORDER_SN_HIGH,
	CMD20_ORDER_VER_NO,
	CMD20_ORDER_BARCODE1,
	CMD20_ORDER_BARCODE2,
	CMD20_ORDER_BARCODE3,
	CMD20_ORDER_BARCODE4,

	CMD20_ORDER_FRAME_NUM,
};
/*==========================================================================*
 * FUNCTION : RtUnpackCmd20
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr     : 
 *            int    iReadLen  : 
 *            BYTE*  pbyRcvBuf : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:29
 *==========================================================================*/
static BOOL RtUnpackCmd20(int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i, iSAddr;
	UINT		uiSerialNoHi, uiSerialNoLow, uiVerNo, uiBarcode;
	int			iFrameOrder = CMD20_ORDER_RATED_VOLT;
//changed by Frank Wu,5/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)			
	int			j;
	int			iInfoIndex = 0;
#endif//GC_SUPPORT_RECT_DLOAD

	if(iAddr >= g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1)
		iSAddr = iAddr - g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;
	else
		iSAddr = iAddr;
	
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;
		
		/*TRACE("**********i = %d  VAL_TYPE_H = %02X  VAL_TYPE_L = %02X  ",
			i,
			pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H],
			pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L]);*/

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) != PROTNO_RECT_CONTROLLER)
			//|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != (UINT)iAddr)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != (UINT)iSAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
				!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case RT_VAL_TYPE_R_RATED_VOLT:
			if(iAddr == 0)
			{
				g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue
					= CAN_StringToFloat(pValueBuf);
			}

			g_CanData.aRoughDataRect[iAddr][RECT_RATED_VOLT].fValue
					= CAN_StringToFloat(pValueBuf);
			
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_RATED_VOLT ok!!!\n", iFrameOrder);

			if(CMD20_ORDER_RATED_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		case RT_VAL_TYPE_R_RATED_POWER:
			//No use
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_RATED_POWER ok!!!\n", iFrameOrder);

			if(CMD20_ORDER_RATED_POWER == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		case RT_VAL_TYPE_R_RATED_CURR:

			g_CanData.aRoughDataRect[iAddr][RECT_RATED_CURR].fValue 
					= CAN_StringToFloat(pValueBuf);
			
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_RATED_CURR ok!!!\n", iFrameOrder);

			if(CMD20_ORDER_RATED_CURR == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case RT_VAL_TYPE_R_RATED_INPUT_VOLT:
			//No use
			if(CMD20_ORDER_RATED_INPUT_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_RATED_INPUT_VOLT ok!!!\n", iFrameOrder);

			break;
	
		case RT_VAL_TYPE_R_FEATURE:
			/*if(iAddr == 0)
			{
				RtAnalyseFeature(pValueBuf);
			}*/	

			g_CanData.aRoughDataRect[iAddr][RECT_FEATURE].uiValue 
					= CAN_StringToUint(pValueBuf);
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_FEATURE ok!!!\n", iFrameOrder);

			if(CMD20_ORDER_FEATURE == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		case RT_VAL_TYPE_R_SN_HIGH:
			uiSerialNoHi = CAN_StringToUint(pValueBuf);
			uiSerialNoLow = g_CanData.aRoughDataRect[iAddr][RECT_SERIEL_NO].uiValue;
			g_CanData.aRoughDataRect[iAddr][RECT_HIGH_SN_ROUGH].uiValue 
				= uiSerialNoHi;
			
			g_CanData.aRoughDataRect[iAddr][RECT_HIGH_SN_REPORT].uiValue
				= ((uiSerialNoHi & 0x0000000f) << 2) + (uiSerialNoLow >> 30);
//changed by Frank Wu,6/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
			iInfoIndex = iAddr;
			//RTSN_HIGH[iAddr] = *(pValueBuf+1);
			RTSN_HIGH[iInfoIndex] = uiSerialNoHi;
#endif//GC_SUPPORT_RECT_DLOAD

			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_SN_HIGH ok!!!\n", iFrameOrder);

			if(CMD20_ORDER_SN_HIGH == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		case RT_VAL_TYPE_R_VER_NO:
			uiVerNo =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataRect[iAddr][RECT_VERSION_NO].uiValue 
					= uiVerNo;
			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_VER_NO ok!!!\n", iFrameOrder);


			if(CMD20_ORDER_VER_NO == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case RT_VAL_TYPE_R_BARCODE1:		
			uiBarcode =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataRect[iAddr][RECT_BARCODE1].uiValue 
					= uiBarcode;
//changed by Frank Wu,7/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
			iInfoIndex = iAddr;
			Barcode_S[iInfoIndex][0] = *(pValueBuf + 3);
			Rect_Sn[iInfoIndex][0] = *pValueBuf;
			Rect_Sn[iInfoIndex][1] = *(pValueBuf + 1);
#endif//GC_SUPPORT_RECT_DLOAD

			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_BARCODE1 ok!!!\n", iFrameOrder);
			
			if(CMD20_ORDER_BARCODE1 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;


		case RT_VAL_TYPE_R_BARCODE2:		
			uiBarcode = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataRect[iAddr][RECT_BARCODE2].uiValue 
					= uiBarcode;
			//printf("Barcode%d:%d\n", iAddr,g_CanData.aRoughDataRect[iAddr][RECT_BARCODE2].uiValue );
//changed by Frank Wu,8/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
			iInfoIndex = iAddr;
			for(j = 0;j < 4;j++)
			{
				Barcode_S[iInfoIndex][1 + j] = *(pValueBuf + j);
			}
#endif//GC_SUPPORT_RECT_DLOAD

			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_BARCODE2 ok!!!\n", iFrameOrder);
			
			if(CMD20_ORDER_BARCODE2 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		case RT_VAL_TYPE_R_BARCODE3:		
			uiBarcode = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataRect[iAddr][RECT_BARCODE3].uiValue
					= uiBarcode;
//changed by Frank Wu,9/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
			iInfoIndex = iAddr;
			for(j = 0;j < 4;j++)
			{
				Barcode_S[iInfoIndex][5 + j] = *(pValueBuf + j);
			}
#endif//GC_SUPPORT_RECT_DLOAD

			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_BARCODE3 ok!!!\n", iFrameOrder);
			
			if(CMD20_ORDER_BARCODE3 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		case RT_VAL_TYPE_R_BARCODE4:		
			uiBarcode = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataRect[iAddr][RECT_BARCODE4].uiValue 
					= uiBarcode;
//changed by Frank Wu,10/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
			iInfoIndex = iAddr;
			for(j = 0;j < 3;j++)
			{
				Barcode_S[iInfoIndex][9 + j] = *(pValueBuf + j);
			}
#endif//GC_SUPPORT_RECT_DLOAD

			//TRACE(" iFrameOrder = %d, RT_VAL_TYPE_R_BARCODE4 ok!!!\n", iFrameOrder);
			
			if(CMD20_ORDER_BARCODE4 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		default:
			break;
		}
	}


	//if(CMD20_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	/*else
	{
		return FALSE;
	}*/
}

static BOOL RtUnpackCmd30(int iAddr,
	int iReadLen,
	BYTE* pbyRcvBuf)
{
	int			i, iSAddr;
	UINT		uiSerialNoHi, uiSerialNoLow, uiVerNo, uiBarcode;
	int			iFrameOrder = CMD20_ORDER_RATED_VOLT;
	//changed by Frank Wu,5/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)			
	int			j;
	int			iInfoIndex = 0;
#endif//GC_SUPPORT_RECT_DLOAD

	if (iAddr >= g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1)
		iSAddr = iAddr - g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;
	else
		iSAddr = iAddr;

	for (i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		/*TRACE("**********i = %d  VAL_TYPE_H = %02X  VAL_TYPE_L = %02X  ",
			i,
			pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H],
			pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L]);*/

		if ((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) != PROTNO_RECT_CONTROLLER)
			//|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != (UINT)iAddr)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != (UINT)iSAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
				!= 0))
		{
			continue;
		}

		//printf("Unpack 30  Cmd [%d] value [%f]  \n",*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L), CAN_StringToFloat(pValueBuf));
		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case RT_VAL_TYPE_R_AC_PHA_CURR:

			g_CanData.aRoughDataRect[iAddr][RECT_PHA_CURR].fValue
				= CAN_StringToFloat(pValueBuf);
			//printf("Rect [%d] inPut A  Curr [%f]  \n",iAddr,g_CanData.aRoughDataRect[iAddr][RECT_PHA_CURR].fValue);
			break;

		case RT_VAL_TYPE_R_AC_PHB_CURR:
			g_CanData.aRoughDataRect[iAddr][RECT_PHB_CURR].fValue
				= CAN_StringToFloat(pValueBuf);
			break;

		case RT_VAL_TYPE_R_AC_PHC_CURR:
			g_CanData.aRoughDataRect[iAddr][RECT_PHC_CURR].fValue
				= CAN_StringToFloat(pValueBuf);

		//case RT_VAL_TYPE_R_AC_INPUT_POWER:

		//	g_CanData.aRoughDataRect[iAddr][RECT_INPUT_POWER].fValue
		//		= CAN_StringToFloat(pValueBuf);
		//	break;

		//case RT_VAL_TYPE_R_AC_INPUT_POWERTOTAL:
		//	g_CanData.aRoughDataRect[iAddr][RECT_INTPUT_POWER_TOTAL].fValue
		//		= CAN_StringToFloat(pValueBuf);
		//	//printf("energy %f ",g_CanData.aRoughDataRect[iAddr][RECT_INTPUT_POWER_TOTAL].fValue);
		//	break;

		//case RT_VAL_TYPE_R_AC_INPUT_FREQ:
		//	g_CanData.aRoughDataRect[iAddr][RECT_INTPUT_FREQ].fValue
		//		= CAN_StringToFloat(pValueBuf);
		//	break;

		default:
			break;
		}
	}

	return TRUE;

}



#define RT_DC_ON_OFF_MASK				(0x80)
#define	RT_OVER_VOLT_RESET_MASK			(0x40)
#define	RT_WALK_IN_MASK					(0x20)
#define	RT_FAN_FULL_SPEED_MASK			(0x10)
#define	RT_LED_BLINK_MASK				(0x08)
#define	RT_OVERVOLT_RELAY_MASK			(0x04)
#define	RT_RESET_CAN_PORT_MASK			(0x02)
#define	RT_AC_OVERVOLT_PRORECT_MASK		(0x01)
/*==========================================================================*
 * FUNCTION : GetRtSetByteCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static BYTE : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
static BYTE GetRtSetByteCmd00(int iAddr)
{
	BYTE			byOutput = 0;

	RECT_SET_SIG*	pSetSig 
				= &(g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[iAddr]);

	if(pSetSig->dwDcOnOff != RT_DC_ON)
	{		
		byOutput |= RT_DC_ON_OFF_MASK;	
	}
	else
	{
		byOutput &= ~RT_DC_ON_OFF_MASK;	
	}

	if(pSetSig->dwResetOnDcOV != RT_RESET_NORMAL)  //Rectifier Over Voltage Reset
	{
		//2010/12/27  
		//������ĵ�Ŀ����������ĳ��ģ�����RESET����ִֻ��һ�飡��
		pSetSig->dwResetOnDcOV = RT_RESET_NORMAL;
		byOutput |= RT_OVER_VOLT_RESET_MASK;
		
	}	
	else
	{
		byOutput &= ~RT_OVER_VOLT_RESET_MASK;
	}

	if(pSetSig->dwWalkinEnabled != RT_WALKIN_NORMAL)    //Rectifier Walkin Enable
	{
		byOutput |= RT_WALK_IN_MASK;
	}
	else
	{
		byOutput &= ~RT_WALK_IN_MASK;
	}

	if(pSetSig->dwFanFullSpeed != RT_FAN_AUTO) //Fan is full speed or not
	{
		byOutput |= RT_FAN_FULL_SPEED_MASK;
	}
	else
	{
		byOutput &= ~RT_FAN_FULL_SPEED_MASK;
	}
	
	if(pSetSig->dwLedBlink != RT_LED_NORMAL)
	{
		byOutput |= RT_LED_BLINK_MASK;
	}
	else
	{
		byOutput &= ~RT_LED_BLINK_MASK;
	}

	if(pSetSig->dwAcOnOff != RT_AC_ON)
	{
		byOutput |= RT_OVERVOLT_RELAY_MASK;
	}
	else
	{
		byOutput &= ~RT_OVERVOLT_RELAY_MASK;
	}

	if(pSetSig->dwCanInit != RT_CAN_NORMAL)
	{
		byOutput |= RT_RESET_CAN_PORT_MASK;
	}
	else
	{
		byOutput &= ~RT_RESET_CAN_PORT_MASK;
	}


	if(pSetSig->dwResetOnAcOV != RT_AC_OV_PROTECT_ENB)
	{
		byOutput |= RT_AC_OVERVOLT_PRORECT_MASK;
	}
	else
	{
		byOutput &= ~RT_AC_OVERVOLT_PRORECT_MASK;
	}

	/*if(iAddr == 1)
		printf("LED R%d: %d", iAddr, pSetSig->dwLedBlink);*/
	return byOutput;	
}



/*==========================================================================*
 * FUNCTION : ClrRtIntrruptTimes
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:29
 *==========================================================================*/
static void ClrRtIntrruptTimes(int iAddr)
{
	g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_TIMES].iValue = 0;
	g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_ST].iValue 
			= RT_COMM_NORMAL_ST;

	return;
}

/*==========================================================================*
 * FUNCTION : IntrruptTimesCfg
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Catherine Wang                DATE: 2013-6-13
 *==========================================================================*/
static void IntrruptTimesCfg(int iAddr)
{
	if(RectCAN2[iAddr][RECT_INTERRUPT_TIMES].iValue 
		< RT_MAX_INTERRUPT_TIMES + 2)
	{
		RectCAN2[iAddr][RECT_INTERRUPT_TIMES].iValue++;
	}
	
	if(RectCAN2[iAddr][RECT_INTERRUPT_TIMES].iValue
		>= (RT_MAX_INTERRUPT_TIMES + 1))
	{
		if(g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue)
		{
			RectCAN2[iAddr][RECT_INTERRUPT_ST].iValue 
				= RT_COMM_ALL_INTERRUPT_ST;
		}
		else
		{
			RectCAN2[iAddr][RECT_INTERRUPT_ST].iValue 
				= RT_COMM_INTERRUPT_ST;
		}
	}
	else
	{
		RectCAN2[iAddr][RECT_INTERRUPT_ST].iValue 
			= RT_COMM_NORMAL_ST;
	}

	return;
}

/*==========================================================================*
 * FUNCTION : IncRtIntrruptTimes
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:29
 *==========================================================================*/
static void IncRtIntrruptTimes(int iAddr)
{
	if(g_CanData.aRoughDataRect[iAddr][RECT_EXIST_ST].iValue == RT_EQUIP_EXISTENT)
	{
		if(g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_TIMES].iValue 
			< RT_MAX_INTERRUPT_TIMES + 2)
		{
			g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_TIMES].iValue++;
		}
		
		if(g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_TIMES].iValue
			>= (RT_MAX_INTERRUPT_TIMES + 1))
		{
			if(g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue)
			{
				g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_ST].iValue 
					= RT_COMM_ALL_INTERRUPT_ST;
			}
			else
			{
				g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_ST].iValue 
					= RT_COMM_INTERRUPT_ST;
			}
		}
		else
		{
			g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_ST].iValue 
				= RT_COMM_NORMAL_ST;
		}
	}
	else
		g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_TIMES].iValue = 0;

	return;
}



/*==========================================================================*
 * FUNCTION : RtSampleCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:30 
 *==========================================================================*/
static SIG_ENUM RtSampleCmd00(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	int		iSAddr, iState = FALSE;
	SIG_ENUM	sState;

	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendRtCAN1(MSG_TYPE_RQST_DATA00,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					(UINT)GetRtSetByteCmd00(iAddr),
					RT_VAL_TYPE_NO_TRIM_VOLT,
					unVal.abyValue);

	PackAndSendRtCAN2(MSG_TYPE_RQST_DATA00,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					(UINT)GetRtSetByteCmd00(iAddr + g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1),
					RT_VAL_TYPE_NO_TRIM_VOLT,
					unVal.abyValue);
	//In most situations, rectifer responds in 15s
	Sleep(30);
	if(CAN_RECT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
							g_CanData.CanCommInfo.abyRcvBuf, 
							&iReadLen1st))
	{
		return CAN_RECT_REALLOCATE;
	}
	else
	{
		if((iReadLen1st < (RT_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
			&& (iAddr < g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1))
		{
			//After 15s+85s, if no respond, interrup times increase.
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			iState = TRUE;
			if(CAN_RECT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
							g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
							&iReadLen2nd))

			{
				return CAN_RECT_REALLOCATE;
			}
		}
	}

    if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= RT_CMD00_RCV_FRAMES_NUM)
	{
		//TRACE("****iReadLen1st = %d, iReadLen2nd= %d\n", iReadLen1st, iReadLen2nd);
		if(RtUnpackCmd00(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrRtIntrruptTimes(iAddr);
			sState = CAN_SAMPLE_OK;
		}
		else
		{
			IncRtIntrruptTimes(iAddr);
			sState = CAN1_RECT_FAIL;
			//return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		//IncRtIntrruptTimes(iAddr);
		if(iAddr < g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1)
		{
			IncRtIntrruptTimes(iAddr);
		}
		sState = CAN1_RECT_FAIL;
		//return CAN_SAMPLE_FAIL;		
	}

    /*CAN2�ϵ�Rect*/
	iReadLen1st = 0;
	iReadLen2nd = 0;
	if(CAN_RECT_REALLOCATE == CAN2_ReadFrames(MAX_FRAMES_IN_BUFF, 
							g_CanData.CanCommInfo.abyRcvBuf, 
							&iReadLen1st))
	{
		return CAN_RECT_REALLOCATE;
	}
	else
	{
		if((iReadLen1st < (RT_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
			&& (iAddr < g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2)
			&& (iState == FALSE))
		{
			//After 15s+85s, if no respond, interrup times increase.
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if(CAN_RECT_REALLOCATE == CAN2_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
							g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
							&iReadLen2nd))

			{
				return CAN_RECT_REALLOCATE;
			}
		}
	}

	iSAddr = iAddr + g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= RT_CMD00_RCV_FRAMES_NUM)
	{
		//TRACE("****iReadLen1st = %d, iReadLen2nd= %d\n", iReadLen1st, iReadLen2nd);
		if((g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig)
			|| (g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue == 0))
		{
			if(RtUnpackCfg(iAddr,
				iReadLen1st + iReadLen2nd,
				g_CanData.CanCommInfo.abyRcvBuf))
			{
				//ClrRtIntrruptTimes(iSAddr);
				RectCAN2[iAddr][RECT_INTERRUPT_TIMES].iValue = 0;
				RectCAN2[iAddr][RECT_INTERRUPT_ST].iValue = RT_COMM_NORMAL_ST;
			}
			else
			{
				//IncRtIntrruptTimes(iSAddr);
				IntrruptTimesCfg(iAddr);
				if(sState == CAN1_RECT_FAIL)
					return CAN_BOTH_RECT_FAIL;
				else
					return CAN2_RECT_FAIL;
			}
		}
		else
		{
			if(RtUnpackCmd00(iSAddr,
				iReadLen1st + iReadLen2nd,
				g_CanData.CanCommInfo.abyRcvBuf))
			{
				ClrRtIntrruptTimes(iSAddr);
			}
			else
			{
				IncRtIntrruptTimes(iSAddr);
				if(sState == CAN1_RECT_FAIL)
					return CAN_BOTH_RECT_FAIL;
				else
					return CAN2_RECT_FAIL;
			}
		}
	}
	else
	{
		if((g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig)
			|| (g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue == 0))
		{
			IntrruptTimesCfg(iAddr);
			//IncRtIntrruptTimes(iSAddr);
		}
		else
			IncRtIntrruptTimes(iSAddr);

		if(sState == CAN1_RECT_FAIL)
			return CAN_BOTH_RECT_FAIL;
		else
			return CAN2_RECT_FAIL;
		//return CAN_SAMPLE_FAIL;		
	}
	return sState;
}


/*==========================================================================*
 * FUNCTION : RtSampleCmd10
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:30
 *==========================================================================*/
static SIG_ENUM RtSampleCmd10(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	int		iSAddr, iState = FALSE;
	FLOAT_STRING	unVal;
	SIG_ENUM	sState;

	unVal.ulValue = 0;
	PackAndSendRtCAN1(MSG_TYPE_RQST_DATA10,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					0,				//Half load on AC phase failure
					RT_VAL_TYPE_NO_TRIM_VOLT,
					unVal.abyValue);
	PackAndSendRtCAN2(MSG_TYPE_RQST_DATA10,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					0,
					RT_VAL_TYPE_NO_TRIM_VOLT,
					unVal.abyValue);
	Sleep(30);
	if(CAN_RECT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
								g_CanData.CanCommInfo.abyRcvBuf, 
								&iReadLen1st))
	{
		return CAN_RECT_REALLOCATE;
	}
	else
	{
		if((iReadLen1st < (RT_CMD10_RCV_FRAMES_NUM * CAN_FRAME_LEN))
			&& (iAddr < g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			iState = TRUE;
			if(CAN_RECT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
										g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
										&iReadLen2nd))

			{
				return CAN_RECT_REALLOCATE;
			}
		}
	}

    if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= RT_CMD10_RCV_FRAMES_NUM)
	{
		RtUnpackCmd10(iAddr, 
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
		sState = CAN_SAMPLE_OK;
	}
	else
	{
		sState = CAN1_RECT_FAIL;
		//return CAN_SAMPLE_FAIL;	
	}

	/*CAN2*/
	iReadLen1st = 0;
	iReadLen2nd = 0;
	iSAddr = iAddr + g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;

	if(CAN_RECT_REALLOCATE == CAN2_ReadFrames(MAX_FRAMES_IN_BUFF, 
								g_CanData.CanCommInfo.abyRcvBuf, 
								&iReadLen1st))
	{
		return CAN_RECT_REALLOCATE;
	}
	else
	{
		if((iReadLen1st < (RT_CMD10_RCV_FRAMES_NUM * CAN_FRAME_LEN))
			&& (iAddr < g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2)
			&& (iState == FALSE))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if(CAN_RECT_REALLOCATE == CAN2_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
										g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
										&iReadLen2nd))

			{
				return CAN_RECT_REALLOCATE;
			}
		}
	}

	 if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= RT_CMD10_RCV_FRAMES_NUM)
	{
		RtUnpackCmd10(iSAddr, 
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
		return sState;
	}
	else
	{
		if(sState == CAN1_RECT_FAIL)
			return CAN2_RECT_FAIL;
		else
			return CAN_BOTH_RECT_FAIL;
		//return CAN_SAMPLE_FAIL;	
	}

}



/*==========================================================================*
 * FUNCTION : RtSampleCmd20
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:30
 *==========================================================================*/
static SIG_ENUM RtSampleCmd20(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0, iSAddr;
	FLOAT_STRING	unVal;
	SIG_ENUM	siState;

	unVal.ulValue = 0;
	PackAndSendRtCAN1(MSG_TYPE_RQST_DATA20,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					0,
					RT_VAL_TYPE_NO_TRIM_VOLT,
					unVal.abyValue);
	PackAndSendRtCAN2(MSG_TYPE_RQST_DATA20,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					0,
					RT_VAL_TYPE_NO_TRIM_VOLT,
					unVal.abyValue);
	Sleep(RT_CMD20_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);

	if(CAN_RECT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
								g_CanData.CanCommInfo.abyRcvBuf, 
								&iReadLen1st))
	{
		return CAN_RECT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (RT_CMD20_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if(CAN_RECT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
										g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
										&iReadLen2nd))

			{
				return CAN_RECT_REALLOCATE;
			}
		}
	}

    if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= RT_CMD20_RCV_FRAMES_NUM_SPECIAL)
	{
		RtUnpackCmd20(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
		siState = CAN_SAMPLE_OK;
	}
	else
	{
		siState = CAN1_RECT_FAIL;
		//return CAN_SAMPLE_FAIL;	
	}

	/*CAN2*/
	if(CAN_RECT_REALLOCATE == CAN2_ReadFrames(MAX_FRAMES_IN_BUFF, 
								g_CanData.CanCommInfo.abyRcvBuf, 
								&iReadLen1st))
	{
		return CAN_RECT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (RT_CMD20_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if(CAN_RECT_REALLOCATE == CAN2_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
										g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
										&iReadLen2nd))

			{
				return CAN_RECT_REALLOCATE;
			}
		}
	}

	iSAddr = iAddr + g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;

    if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= RT_CMD20_RCV_FRAMES_NUM_SPECIAL)
	{
		RtUnpackCmd20(iSAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
	}
	else
	{
		if(siState == CAN1_RECT_FAIL)
			siState = CAN_BOTH_RECT_FAIL;
		else
			siState = CAN2_RECT_FAIL;
		//return CAN_SAMPLE_FAIL;	
	}
	//TRACE("iAddr = %d, OK!!!\n", iAddr);

	return siState;
	//return CAN_SAMPLE_OK;
}

/*==========================================================================*
* FUNCTION : RtSampleCmd30
* PURPOSE  :
* CALLS    :
* CALLED BY:
* ARGUMENTS: int  iAddr :
* RETURN   : static SIG_ENUM :
* COMMENTS :
* CREATOR  : Frank Cao                DATE: 2008-07-19 17:30
*==========================================================================*/
static SIG_ENUM RtSampleCmd30(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0, iSAddr;
	FLOAT_STRING	unVal;
	SIG_ENUM	siState;

	unVal.ulValue = 0;
	PackAndSendRtCAN1(MSG_TYPE_RQST_DATA30,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,
		RT_VAL_TYPE_NO_TRIM_VOLT,
		unVal.abyValue);
	PackAndSendRtCAN2(MSG_TYPE_RQST_DATA30,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,
		RT_VAL_TYPE_NO_TRIM_VOLT,
		unVal.abyValue);
	Sleep(RT_CMD30_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);


	if (CAN_RECT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF,
		g_CanData.CanCommInfo.abyRcvBuf,
		&iReadLen1st))
	{

		return CAN_RECT_REALLOCATE;
	}
	else
	{
		if (iReadLen1st < (RT_CMD30_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if (CAN_RECT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN,
				g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
				&iReadLen2nd))
			{
				return CAN_RECT_REALLOCATE;
			}
		}
	}

	if (((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN)
		>= RT_CMD30_RCV_FRAMES_NUM_SPECIAL)
	{
		RtUnpackCmd30(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
		siState = CAN_SAMPLE_OK;
	}
	else
	{
		siState = CAN1_RECT_FAIL;
		//return CAN_SAMPLE_FAIL;	
	}

	/*CAN2*/
	if (CAN_RECT_REALLOCATE == CAN2_ReadFrames(MAX_FRAMES_IN_BUFF,
		g_CanData.CanCommInfo.abyRcvBuf,
		&iReadLen1st))
	{
		return CAN_RECT_REALLOCATE;
	}
	else
	{
		if (iReadLen1st < (RT_CMD30_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if (CAN_RECT_REALLOCATE == CAN2_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN,
				g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
				&iReadLen2nd))

			{
				return CAN_RECT_REALLOCATE;
			}
		}
	}

	iSAddr = iAddr + g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;

	if (((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN)
		>= RT_CMD30_RCV_FRAMES_NUM_SPECIAL)
	{
		RtUnpackCmd30(iSAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
	}
	else
	{
		if (siState == CAN1_RECT_FAIL)
			siState = CAN_BOTH_RECT_FAIL;
		else
			siState = CAN2_RECT_FAIL;
		//return CAN_SAMPLE_FAIL;	
	}
	//TRACE("iAddr = %d, OK!!!\n", iAddr);

	return siState;
}


#define RT_MAX_POS_NO		999
/*==========================================================================*
 * FUNCTION : GetOneRestRectPosNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
static int GetOneRestRectPosNo(void)
{
	int		i , j;
	int		iRectNum = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;


	for(i = 1; i < RT_MAX_POS_NO; i++)
	{
		BOOL	bUsed = FALSE;
		for(j = 0; j < iRectNum; j++)
		{
			if(i == g_CanData.aRoughDataRect[j][RECT_POSITION_NO].iValue)
			{
				bUsed = TRUE;
				break;
			}
		}

		if(!bUsed)
		{
			return i;
		}
	}
	return i;
}


/*==========================================================================*
 * FUNCTION : GetPhaseNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iSeqNo : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
static int GetPhaseNo(int iSeqNo)
{
	return iSeqNo % RT_AC_PHASE_NUM;
}




/*==========================================================================*
 * FUNCTION : RT_RefreshFlashInfo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
//static void RT_RefreshFlashInfo(void)
//{
//	int			i, j;
//	int			iRectNum = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;
//	DWORD		dwSnHigh;
//	DWORD		dwSnLow;
//	int			iPosNo;
//	int			iAcPhase;
//	//HANDLE		hCanFlashData;
//
//	/*RECT_POS_ADDR	RectPosAddr[MAX_NUM_RECT];*/
//
//	//Assign position No. and phase No. to rectifers by flash data
//	for(i = 0; i < iRectNum; i++)
//	{
//		dwSnHigh = g_CanData.aRoughDataRect[i][RECT_HIGH_SN_ROUGH].dwValue;
//		dwSnLow = g_CanData.aRoughDataRect[i][RECT_SERIEL_NO].dwValue;
//
//		//printf("R%d=%d\n",i,g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue);
//		for(j = 0; j < MAX_NUM_RECT; j ++)
//		{
//			if((dwSnHigh == g_CanData.CanFlashData.aRectifierInfo[j].dwHighSn)
//				&& (dwSnLow == g_CanData.CanFlashData.aRectifierInfo[j].dwSerialNo))
//			{
//				g_CanData.aRoughDataRect[i][RECT_PHASE_NO].iValue
//					= g_CanData.CanFlashData.aRectifierInfo[j].iAcPhase;
//				g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue
//					= g_CanData.CanFlashData.aRectifierInfo[j].iPositionNo;
//				//printf("R%d=%d\n",i,g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue);
//				break;
//			}
//		}
//	}
//
//	//Assign position No. and phase No. to rest rectifers
//	for(i = 0; i < iRectNum; i++)
//	{
//		//iPosNo = g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue;
//		//iAcPhase = g_CanData.aRoughDataRect[i][RECT_PHASE_NO].iValue;
//
//		//if(iPosNo == CAN_SAMP_INVALID_VALUE)
//		//{
//		//	g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue
//		//		= GetOneRestRectPosNo();
//		//	g_CanData.aRoughDataRect[i][RECT_PHASE_NO].iValue
//		//		= GetPhaseNo(i);
//		//}
//		//
//		//RectPosAddr[i].iAddr = i;
//		//RectPosAddr[i].iPositionNo = g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue;
//		//RectPosAddr[i].iPhaseNo = g_CanData.aRoughDataRect[i][RECT_PHASE_NO].iValue;
//
//		//g_CanData.aRoughDataRect[i][RECT_SEQ_NO].iValue
//		//	= CAN_SAMP_INVALID_VALUE;		//be evaluated later
//
//		g_CanData.CanFlashData.aRectifierInfo[i].bExistence = TRUE;
//		g_CanData.CanFlashData.aRectifierInfo[i].dwAddress = i;
//		g_CanData.CanFlashData.aRectifierInfo[i].dwHighSn 
//			= g_CanData.aRoughDataRect[i][RECT_HIGH_SN_ROUGH].dwValue;
//		g_CanData.CanFlashData.aRectifierInfo[i].dwSerialNo 
//			= g_CanData.aRoughDataRect[i][RECT_SERIEL_NO].dwValue;
//		g_CanData.CanFlashData.aRectifierInfo[i].iAcPhase 
//			= g_CanData.aRoughDataRect[i][RECT_PHASE_NO].iValue;
//		g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo
//			= g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue;
//		//g_CanData.CanFlashData.aRectifierInfo[i].iSeqNo 
//		//	= CAN_SAMP_INVALID_VALUE; //be evaluated later
//	}
//
//    //effervescing sort
//	//if(iRectNum)
//	//{
//	//	for (i = 0; i < (iRectNum - 1); i++)
//	//	{
//	//		for (j = 0; j < (iRectNum - 1 - i); j++)
//	//		{
//	//			if(RectPosAddr[j].iPositionNo > RectPosAddr[j + 1].iPositionNo)
//	//			{
//	//				RECT_POS_ADDR		PosAddr;
//
//	//				PosAddr.iAddr = RectPosAddr[j + 1].iAddr;
//	//				PosAddr.iPositionNo = RectPosAddr[j + 1].iPositionNo;
//	//				PosAddr.iPhaseNo = RectPosAddr[j + 1].iPhaseNo;
//
//	//				RectPosAddr[j + 1].iPositionNo = RectPosAddr[j].iPositionNo;
//	//				RectPosAddr[j + 1].iPhaseNo = RectPosAddr[j].iPhaseNo;
//	//				RectPosAddr[j + 1].iAddr = RectPosAddr[j].iAddr;
//
//	//				RectPosAddr[j].iPositionNo = PosAddr.iPositionNo;
//	//				RectPosAddr[j].iPhaseNo = PosAddr.iPhaseNo;
//	//				RectPosAddr[j].iAddr = PosAddr.iAddr;
//	//			}
//	//		}
//	//	}
//	//}
//
//	//Assign sequence No. to every rectifier
//	//for(i = 0; i < iRectNum; i++)
//	//{
//	//	int iAddr = RectPosAddr[i].iAddr;
//
//	//	if(i < MAX_NUM1_RECT)
//	//	{
//	//		g_CanData.aRoughDataRect[iAddr][RECT_SEQ_NO].iValue = i;
//	//		g_CanData.CanFlashData.aRectifierInfo[iAddr].iSeqNo = i;
//	//		SetDwordSigValue(RT_START_EQUIP_ID + i,
//	//					SIG_TYPE_SETTING,
//	//					RT_POS_SET_SIG_ID,
//	//					g_CanData.aRoughDataRect[iAddr][RECT_POSITION_NO].iValue,
//	//					"CAN_SAMP");
//	//		SetDwordSigValue(RT_START_EQUIP_ID + i,
//	//					SIG_TYPE_SETTING,
//	//					RT_PHASE_SET_SIG_ID,
//	//					g_CanData.aRoughDataRect[iAddr][RECT_PHASE_NO].iValue,
//	//					"CAN_SAMP");
//	//	}
//	//	else
//	//	{
//	//		g_CanData.aRoughDataRect[iAddr][RECT_SEQ_NO].iValue = i;
//	//		g_CanData.CanFlashData.aRectifierInfo[iAddr].iSeqNo = i;
//	//		SetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
//	//					SIG_TYPE_SETTING,
//	//					RT_POS_SET_SIG_ID,
//	//					g_CanData.aRoughDataRect[iAddr][RECT_POSITION_NO].iValue,
//	//					"CAN_SAMP");
//	//		SetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
//	//					SIG_TYPE_SETTING,
//	//					RT_PHASE_SET_SIG_ID,
//	//					g_CanData.aRoughDataRect[iAddr][RECT_PHASE_NO].iValue,
//	//					"CAN_SAMP");
//	//	}
//	//}
//
//	/*for(i = 0;i < iRectNum;i++)
//	{
//		printf("R%d = %d;\n",i,g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue);
//	}*/
//
//	CAN_WriteFlashData(&(g_CanData.CanFlashData));
//
//	return;
//}



/*==========================================================================*
 * FUNCTION : RtUnifyParam
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-02-02 15:08
 *==========================================================================*/
static void RtUnifyParam(void)
{
	int				i;
	DWORD			dwValue;
	SIG_ENUM		enumValue, enumABValue;
	BYTE			abyVal[4];

	//Interval of sequential start
	dwValue = GetDwordSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_SEQ_START_INTERVAL_SIGID,
							"CAN_SAMP");
	
	CAN_FloatToString((float)dwValue, abyVal);

	PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						0,
						RT_VAL_TYPE_W_SEQ_START_TIME,
						abyVal);
	
	Sleep(100);
	//Interval of restart on over voltage
	dwValue = GetDwordSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_OV_RESTART_INTERVAL_SIGID,
							"CAN_SAMP");
	
	CAN_FloatToString((float)dwValue, abyVal);

	PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						0,
						RT_VAL_TYPE_W_RESTART_TIME,
						abyVal);

	Sleep(100);

	//Walk-in time	
	dwValue = GetDwordSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_WALKIN_TIME_SIGID,
							"CAN_SAMP");
	
	CAN_FloatToString((float)dwValue * 100/*The unit is 10ms*/, abyVal);

	PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						0,
						RT_VAL_TYPE_W_WALKIN_TIME,
						abyVal);

	Sleep(100);

	//Walk-in Enabled	
	enumValue = GetEnumSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_WALKIN_ENB_SIGID,
							"CAN_SAMP");
	
	abyVal[0] = 0;
	abyVal[1] = (BYTE)enumValue;
	abyVal[2] = 0;
	abyVal[3] = 0;

	PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						0,
						RT_VAL_TYPE_W_WALKIN_ENB,
						abyVal);

	for(i = 0; i < MAX_NUM_RECT; i++)
	{
		g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwWalkinEnabled 
			= (0 == enumValue) ? RT_WALKIN_NORMAL : RT_WALKIN_ENB;
	}
	Sleep(100);

	//Full Power Capacity Enabled	
	enumValue = GetEnumSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_FULL_POWER_ENB_SIGID,
							"CAN_SAMP");
	enumABValue = GetEnumSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_AB_POWER_LIMIT_MODE_SIGID,
							"CAN_SAMP");
	
	abyVal[0] = 0;
	abyVal[1] = (BYTE)enumValue + 2 * enumABValue;
	abyVal[2] = 0;
	abyVal[3] = 0;

	PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						0,
						RT_VAL_TYPE_W_FULL_POWER_ENB,
						abyVal);
	
	//for(i = 0; i < MAX_NUM_RECT; i++)
	//{
	//	g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwWalkinEnabled 
	//		= (0 == enumValue) ? RT_WALKIN_NORMAL : RT_WALKIN_ENB;
	//}
	//Sleep(100);

	//Restart on over voltage Enabled	
	enumValue = GetEnumSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_OV_RESTART_ENB_SIGID,
							"CAN_SAMP");
	
	abyVal[0] = 0;
	abyVal[1] = (BYTE)enumValue;
	abyVal[2] = 0;
	abyVal[3] = 0;

	//2010/12/27
	//���Ķ�Э�飬��ѹ����������ʹ��	��VALUETYPEL 0X39 txy ����Ҫ�úò���һ��
	PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						0,
						0x39,
						abyVal);
	

	//PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
	//					CAN_ADDR_FOR_BROADCAST,
	//					CAN_CMD_TYPE_BROADCAST,
	//					0,
	//					RT_VAL_TYPE_W_RESTART_ON_OV,
	//					abyVal);	
	//

	/*	
	for(i = 0; i < MAX_NUM_RECT; i++)
	{
		g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwResetOnDcOV
			= (0 == enumValue) ? RT_OV_RESET_DISB : RT_OV_RESET_ENB;
	}	
	*/	

	Sleep(100);

	//Restart on AC over voltage Enabled	
	enumValue = GetEnumSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_AC_OV_RESTART_ENB_SIGID,
							"CAN_SAMP");
	

	if(enumValue == 0)
	{
		for(i = 0; i < MAX_NUM_RECT; i++)
		{
			g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwResetOnAcOV 
						=  RT_AC_OV_PROTECT_DISB;
		}
		//printf("\n	This is if(enumValue == 0) for all DISB		\n");
	}
	else
	{
		BOOL	bSet = FALSE;
		for(i = 0; i < MAX_NUM_RECT; i++)
		{
			//Only one rectifier needs to be controlled to enabled
			if((!bSet) 
				&& (g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue
					< RT_MAX_INTERRUPT_TIMES))
			{
				g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwResetOnAcOV 
						=  RT_AC_OV_PROTECT_ENB;
				bSet = TRUE;
			}
			else
			{
				g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwResetOnAcOV 
						=  RT_AC_OV_PROTECT_DISB;
			
			}
		}
		//printf("\n	This is ELSE 	if(enumValue == 0)	\n");
	}		
			
	return;
}


/*==========================================================================*
 * FUNCTION : RtDefaultVoltParam
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-01-08 14:41
 *==========================================================================*/
static void RtDefaultVoltParam(BOOL bDirectSendCmd)
{
	float			fFloatVolt;
	float			fOverVolt;
	BYTE			abyVal[4];
	static	float	s_fLastDefaultVolt = 0.0;
	static	float	s_fLastOverVolt = 0.0;
	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"RT Get SysVolt Level");

	//if(g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue > 35.0)
	if(0 == stVoltLevelState)
	{
		fFloatVolt = GetFloatSigValue(BT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_FLOAT_VOLT_SIGID,
							"CAN_SAMP");
		fOverVolt = GetFloatSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_OVER_VOLTAGE_SIGID,
							"CAN_SAMP");

	}
	else
	{
		fFloatVolt = GetFloatSigValue(BT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_FLOAT_VOLT_SIGID + 100,
							"CAN_SAMP");
		fOverVolt = GetFloatSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_OVER_VOLTAGE_SIGID + 100,
							"CAN_SAMP");
	}

	if(bDirectSendCmd 
		|| (FLOAT_NOT_EQUAL(s_fLastDefaultVolt, fFloatVolt)))
	{
		s_fLastDefaultVolt = fFloatVolt;

		CAN_FloatToString(fFloatVolt, abyVal);
		PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_DEFAULT_VOLT,
							abyVal);
		
		Sleep(100);
	}

	if(bDirectSendCmd
		|| (FLOAT_NOT_EQUAL(s_fLastOverVolt, fOverVolt)))
	{
		s_fLastOverVolt = fOverVolt;

		CAN_FloatToString(fOverVolt, abyVal);
		PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_HIGH_VOLT,
							abyVal);
		
		Sleep(100);
	}

	return;
}




/*==========================================================================*
 * FUNCTION : RT_TriggerAllocation
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:31
 *==========================================================================*/
void RT_TriggerAllocation(void)
{
	int		i;
	//Rectifier re-allocation cmd require below data, 
	//please refer to the protocol
	BYTE	abyDataOfReallcate[CAN_FRAME_DATA_LEN] 
				= {0x04, 0xf0, 0x01, 0x5a, 0x00, 0x00, 0x00, 0x00};

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_RECT_RECT,
				CAN_ADDR_FOR_BROADCAST, 
				CAN_SELF_ADDR,
				CAN_ERR_CODE,
				CAN_FRAME_DATA_LEN,
				CAN_CMD_TYPE_BROADCAST,
				g_CanData.CanCommInfo.abySendBuf);
	
	//Stuff the rest bits
	for(i = 0; i < CAN_FRAME_DATA_LEN; i++)
	{
		g_CanData.CanCommInfo.abySendBuf[CAN_FRAME_OFFSET_MSG + i] 
			= abyDataOfReallcate[i];
	}
 
	write(g_CanData.CanCommInfo.iCanHandle, 
						(void *)g_CanData.CanCommInfo.abySendBuf,
						CAN_FRAME_LEN);
	write(g_CanData.CanCommInfo.iCan2Handle, 
						(void *)g_CanData.CanCommInfo.abySendBuf,
						CAN_FRAME_LEN);
	
	//TRACE("************CAN DEBUG: Writed a reallocting command!\n");

	//����HVDC��ʱ���ֵ�
	//g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = TRUE;

	//Sleep(12000);			//Waiting 12s
	//CAN_ClearReadBuf();

	return;

}



/*==========================================================================*
 * FUNCTION : RT_InitPosAndPhase
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT CAN_FLASH_DATA*  pFlashData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:31
 *==========================================================================*/
void RT_InitPosAndPhase(void)
{
	int				i;

	for(i = 0; i < MAX_NUM_RECT; i++)
	{
		g_CanData.CanFlashData.aRectifierInfo[i].iAcPhase = AC_PHASE_INVAILID;
		g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo
			= RECT_POSITION_INVAILID;
	}
	return;
}


/*==========================================================================*
 * FUNCTION : RT_InitRoughValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:31
 *==========================================================================*/
void RT_InitRoughValue(void)
{
	int			i, j;
	SIG_ENUM		enumValue = GetEnumSigValue(RT_GROUP_EQUIPID, 
						SIG_TYPE_SETTING,
						RT_WALKIN_ENB_SIGID,
						"CAN_SAMP");

	g_CanData.CanCommInfo.RectCommInfo.bNeedRefreshRuntime = TRUE;

	g_CanData.CanCommInfo.RectCommInfo.iCommRectNum = 0;
	if(g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue = 0;	
	}

	//Ranged by rectifier address order
	for(i = 0; i < MAX_RECTNUM_CAN; i++)
	{
		for(j = 0; j < RECT_MAX_SIGNALS_NUM; j++)
			RectCAN2[i][j].iValue = CAN_SAMP_INVALID_VALUE;
	}

	for(i = 0; i < MAX_NUM_RECT; i++)
	{
		//Assign invalid value to sampling signals first
		for(j = 0; j < RECT_MAX_SIGNALS_NUM; j++)
		{
			g_CanData.aRoughDataRect[i][j].iValue = CAN_SAMP_INVALID_VALUE;
		}

		g_CanData.aRoughDataRect[i][RECT_SEQ_NO].iValue = i;
		g_CanData.CanFlashData.aRectifierInfo[i].bExistence = FALSE;
		
		RECT_SET_SIG* pRectSetSig 
			= g_CanData.CanCommInfo.RectCommInfo.aRectSetSig + i;

		//g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].fRunTime = 0
		pRectSetSig->dwDcOnOff = RT_DC_ON;

		//2011/2/21�գ����ģ���������ú�ģ��HVSD����δ���ֵĸ��ġ�
		//pRectSetSig->dwResetOnDcOV = RT_OV_RESET_ENB;
		pRectSetSig->dwWalkinEnabled = (0 == enumValue) ? RT_WALKIN_NORMAL : RT_WALKIN_ENB;//RT_WALKIN_NORMAL;
		pRectSetSig->dwFanFullSpeed = RT_FAN_AUTO;
		pRectSetSig->dwLedBlink = RT_LED_NORMAL;
		pRectSetSig->dwAcOnOff = RT_AC_ON;
		pRectSetSig->dwCanInit = RT_CAN_NORMAL;
		pRectSetSig->dwResetOnAcOV = RT_AC_OV_PROTECT_ENB;

	}

	return;
}


/*==========================================================================*
 * FUNCTION : GetRectAddrByChnNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   iChannelNo : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:31
 *==========================================================================*/
static int GetRectAddrByChnNo(int iChannelNo)
{
	int i;
	
	/*int iSeqNo = (iChannelNo - CNMR_MAX_RT_BROADCAST_CMD_CHN - 1) 
						/ MAX_CHN_NUM_PER_RT;*/
	int iSeqNo;

	if(iChannelNo >= CNRS2_DC_ON_OFF)
		iSeqNo = (iChannelNo - CNRS2_DC_ON_OFF) / MAX_CHN_NUM_PER_RT + MAX_NUM1_RECT;
	else
		iSeqNo = (iChannelNo - CNMR_MAX_RT_BROADCAST_CMD_CHN - 1) / MAX_CHN_NUM_PER_RT;
	//ASSERT(iSeqNo >= 0 && iSeqNo < MAX_NUM_RECT);

	for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
	{
		//aRoughDataGroup is ranged by address order, so "i" is address 
		if(iSeqNo == g_CanData.aRoughDataRect[i][RECT_SEQ_NO].iValue)
		{
			return i;
		}
	}

	return 0;
}


void RT_LoadShare(void)
{
    // static float  fDelta=0;
     float         fCAN1Curr=0, fCAN2Curr=0, fCAN1Rated=0, fCAN2Rated=0;
	 float		fCAN1CAvg = 0,fCAN2CAvg = 0;
     int      i, iCAN1Rect=0, iCAN2Rect=0;
     BYTE     abyVal[4];
     RECT_COMM_INFO*    pRectCommInfo;

     pRectCommInfo = &g_CanData.CanCommInfo.RectCommInfo;

     for(i = 0;i < pRectCommInfo->iRectNumCAN1;i++)
     {
         if((g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue == RT_COMM_NORMAL_ST)
              && (g_CanData.aRoughDataRect[i][RECT_DC_ON_OFF_STATUS].iValue == 0)
              && (g_CanData.aRoughDataRect[i][RECT_AC_ON_OFF_STATUS].iValue == 0))
         {
              fCAN1Curr += g_CanData.aRoughDataRect[i][RECT_ACTUAL_CURRENT].fValue;
              fCAN1Rated += g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue;
              iCAN1Rect++;
			 // printf("CAN1_%d Curr/Rated = %f/%f; ",i, g_CanData.aRoughDataRect[i][RECT_ACTUAL_CURRENT].fValue,  g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue);
         }
     }
	
	// printf("\n");
     //if(iCAN1Rect == 0)
     if(fCAN1Rated < 1)
         return;
	  fCAN1CAvg = fCAN1Curr / iCAN1Rect;

     //fCAN1Curr /= iCAN1Rect;
     fCAN1Curr /= fCAN1Rated;

     for(i = pRectCommInfo->iRectNumCAN1;i < pRectCommInfo->iCommRectNum;i++)
     {
         if((g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue == RT_COMM_NORMAL_ST)
              && (g_CanData.aRoughDataRect[i][RECT_DC_ON_OFF_STATUS].iValue == 0)
              && (g_CanData.aRoughDataRect[i][RECT_AC_ON_OFF_STATUS].iValue == 0))
         {
              fCAN2Curr += g_CanData.aRoughDataRect[i][RECT_ACTUAL_CURRENT].fValue;
              fCAN2Rated += g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue;
			   //printf("CAN2_%d Curr/Rated = %f/%f; ",i, g_CanData.aRoughDataRect[i][RECT_ACTUAL_CURRENT].fValue,  g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue);
              iCAN2Rect++;
         }
     }
	 //printf("CAN1Num=%d; CAN2Num=%d\n", pRectCommInfo->iRectNumCAN1, pRectCommInfo->iRectNumCAN2);
	//  printf("\n");
     //if(iCAN2Rect == 0)
     if(fCAN2Rated < 1)
         return;
	  fCAN2CAvg = fCAN2Curr / iCAN2Rect;

     //fCAN2Curr /= iCAN2Rect;
     fCAN2Curr /= fCAN2Rated;
	// printf("fCAN1Curr=%f; fCAN2Curr=%f\n",fCAN1Curr,fCAN2Curr);

     fCAN1Curr -= fCAN2Curr;

    /* if(fCAN1Curr > 0.2)
	 {
		  fDeltaRectV += 0.006;
	 }
	 else if(fCAN1Curr > 0.15)
     {
         fDeltaRectV += 0.004;
     }
	 else if(fCAN1Curr > 0.04)
     {
         fDeltaRectV += 0.002;
     }
	 else if(fCAN1Curr < -0.2)
     {
         fDeltaRectV -= 0.06;    
     }
	 else if(fCAN1Curr < -0.15)
     {
         fDeltaRectV -= 0.004;    
     }
     else if(fCAN1Curr < -0.04)
     {
         fDeltaRectV -= 0.002;    
     }*/
	 if((fCAN1Curr > 0.04) || (fCAN1Curr < -0.04))
	 {
		 fDeltaRectV += (fCAN1CAvg - fCAN2CAvg) / 2000;
	 }

	 //printf("CAN1Volt=%f; CAN2Volt=%f; fDelta=%f\n", fRectVoltCAN1, fRectVoltCAN1 + fDeltaRectV, fDeltaRectV);
     if(fDeltaRectV > 0.4)
         fDeltaRectV = 0.4;
     else if(fDeltaRectV < -0.4)
         fDeltaRectV = -0.4;

     //printf("CAN2Volt=%f\n", (fRectVoltCAN1 + fDeltaRectV));
     CAN_FloatToString((fRectVoltCAN1 + fDeltaRectV), abyVal);
     PackAndSendRtCAN2(MSG_TYPE_RQST_SETTINGS,
              CAN_ADDR_FOR_BROADCAST,
              CAN_CMD_TYPE_BROADCAST,
              0,
              RT_VAL_TYPE_W_DC_VOLT,
              abyVal);
}


/*==========================================================================*
 * FUNCTION : RT_SendCtlCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iChannelNo : 
 *            float  fParam     : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:31
 *==========================================================================*/
void RT_SendCtlCmd(int iChannelNo, float fParam)
{
	int				i;
	BYTE			abyVal[4];
	SIG_ENUM enumABValue;
	//float			fCAN2Volt;
	/*TRACE("CAN DEBUG: ************ Channel No.: %d, Parameter: %.02f!\n",
				iChannelNo,
				fParam);*/
	
	if(iChannelNo < CNMR_MAX_RT_BROADCAST_CMD_CHN)
	{
		switch(iChannelNo)
		{
		case CNRG_CURR_LIMT:
			CAN_FloatToString(fParam / 100, abyVal);
			//printf("CURR_LIMT 1:%f; Time:%d\n", fParam,time(NULL));
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_CURR_LMT,
							abyVal);
			break;
		
		case CNRG_DC_VOLT:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendRtCAN1(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_DC_VOLT,
							abyVal);

			 CAN_FloatToString((fParam + fDeltaRectV), abyVal);
			 PackAndSendRtCAN2(MSG_TYPE_RQST_SETTINGS,
              CAN_ADDR_FOR_BROADCAST,
              CAN_CMD_TYPE_BROADCAST,
              0,
              RT_VAL_TYPE_W_DC_VOLT,
              abyVal);
			/*PackAndSendRtCAN1(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_DC_VOLT,
							abyVal);*/
			fRectVoltCAN1 = fParam;
			//printf("fRectVoltCAN1=%f\n", fRectVoltCAN1);
			//fCAN2Volt = BalanceRect();
			break;

		case CNRG_DC_ON_OFF:

			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam)? RT_DC_ON : RT_DC_OFF;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_DC_ON_OFF,
							abyVal);

			for(i = 0; i < MAX_NUM_RECT; i++)
			{
				g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwDcOnOff 
					= FLOAT_EQUAL0(fParam)? RT_DC_ON : RT_DC_OFF;
			}
			break;

		case CNRG_AC_ON_OFF:
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? RT_AC_ON : RT_AC_OFF;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_AC_ON_OFF,
							abyVal);
			for(i = 0; i < MAX_NUM_RECT; i++)
			{
				g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwAcOnOff 
					= FLOAT_EQUAL0(fParam)? RT_AC_ON : RT_AC_OFF;
			}
			break;

		case CNRG_HI_VOLT_LMT:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_HIGH_VOLT,
							abyVal);
			break;

		case CNRG_OVER_V_RESTART_TIME:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_RESTART_TIME,
							abyVal);
			break;

		case CNRG_WALKIN_TIME:
			CAN_FloatToString(fParam * 100/*The unit is 10ms*/ , abyVal);
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_WALKIN_TIME,
							abyVal);
			break;

		case CNRG_WALKIN_ENB:
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? RT_WALKIN_NORMAL : RT_WALKIN_ENB;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_WALKIN_ENB,
							abyVal);
			for(i = 0; i < MAX_NUM_RECT; i++)
			{
				g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwWalkinEnabled 
					= FLOAT_EQUAL0(fParam)? RT_WALKIN_NORMAL : RT_WALKIN_ENB;
			}
			break;

		case CNRG_SEQUENCE_START_INTEVAL:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_SEQ_START_TIME,
							abyVal);
			break;

		case CNRG_RESTART_FOR_AC_OV:	
			if(FLOAT_EQUAL0(fParam))
			{
				for(i = 0; i < MAX_NUM_RECT; i++)
				{
					g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwResetOnAcOV 
								=  RT_AC_OV_PROTECT_DISB;
				}
			}
			else
			{
				BOOL	bSet = FALSE;
				for(i = 0; i < MAX_NUM_RECT; i++)
				{
					//Only one rectifier needs to be controlled to enabled
					if((!bSet) 
						&& (g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue
							< RT_MAX_INTERRUPT_TIMES))
					{
						g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwResetOnAcOV 
								=  RT_AC_OV_PROTECT_ENB;
						bSet = TRUE;
					}
					else
					{
						g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwResetOnAcOV 
								=  RT_AC_OV_PROTECT_DISB;
					
					}
				}
			}
			break;

		case CNRG_FAN_FULL_SPEED:
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? RT_FAN_AUTO : RT_FAN_FULL_SPEED;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_FAN_FULL_SPEED,
							abyVal);
			for(i = 0; i < MAX_NUM_RECT; i++)
			{
				g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwFanFullSpeed 
					= FLOAT_EQUAL0(fParam) ? RT_FAN_AUTO : RT_FAN_FULL_SPEED;
			}
			break;

		case CNRG_ESTOP_FUNCTION:

			if(FLOAT_EQUAL0(fParam))
			{
				abyVal[0] = 0xff;
				abyVal[1] = 0xff;
				abyVal[2] = 0;
				abyVal[3] = 0;
			}
			else
			{
				abyVal[0] = 0;
				abyVal[1] = 0;
				abyVal[2] = 0x5a;
				abyVal[3] = 0x5a;
			}

			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_ESTOP_ENB,
							abyVal);
			break;

		case CNRG_RESTART_FOR_DC_OV:
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? RT_OV_RESET_DISB : RT_OV_RESET_ENB;
			abyVal[2] = 0;
			abyVal[3] = 0;

			//2010/12/27
			//��Э��VALUETYPEL  0x39,�������������ʹ�ܵ�����?
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							0x39,
							abyVal);

			//PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
			//				CAN_ADDR_FOR_BROADCAST,
			//				CAN_CMD_TYPE_BROADCAST,
			//				0,
			//				RT_VAL_TYPE_W_RESTART_ON_OV,
			//				abyVal);

			//for(i = 0; i < MAX_NUM_RECT; i++)
			//{
			//	g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwResetOnDcOV
			//		= FLOAT_EQUAL0(fParam)? RT_OV_RESET_DISB : RT_OV_RESET_ENB;
			//}

			break;

		case CNRG_ALL_LEDS_BLINK:
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? RT_LED_NOT_BLINK : RT_LED_BLINK;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_LED_BLINK,
							abyVal);
			for(i = 0; i < MAX_NUM_RECT; i++)
			{
				g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[i].dwLedBlink
					= FLOAT_EQUAL0(fParam)? RT_LED_NOT_BLINK : RT_LED_BLINK;
			}
			break;

		case CNRG_FULL_POWER_CAP_ENB:
			enumABValue = GetEnumSigValue(RT_GROUP_EQUIPID, 
				SIG_TYPE_SETTING,
				RT_AB_POWER_LIMIT_MODE_SIGID,
				"CAN_SAMP");

			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? (RT_DEGRADATIVE + enumABValue * 2) : (RT_FULL_POWER_CAP_ENB + enumABValue * 2);
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_FULL_POWER_ENB,
							abyVal);
			break;

		case CNRG_AC_CURRENT_LIMIT:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				RT_VAL_TYPE_W_AC_CURR_LIMIT,
				abyVal);
			TRACE("\n************ !!!!!!!!  CNRG_AC_CURRENT_LIMIT   *******\n");
			break;

		case CNRG_RECT_REALLOCATE:
			RT_TriggerAllocation();
			break;
		case CNRG_RECT_INPUT_POWER_LIMIT_MODE:
			enumABValue = GetEnumSigValue(RT_GROUP_EQUIPID, 
				SIG_TYPE_SETTING,
				RT_FULL_POWER_ENB_SIGID,
				"CAN_SAMP");

			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? (RT_POWER_LIMIT_A * 2 + enumABValue ) : (RT_POWER_LIMIT_B * 2 + enumABValue);
			abyVal[2] = 0;
			abyVal[3] = 0;

			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				RT_VAL_TYPE_W_FULL_POWER_ENB,
				abyVal);
			break;

		case CNRG_RECT_CLEAR_IDS:
			ClrRectFalshInfo();
			break;
//changed by Frank Wu,11/30,20140217, for upgrading software of rectifier---start---
//#if (GC_SUPPORT_RECT_DLOAD == 1)
#if 0
		case CNRG_RECT_UPDATE:
			if(FLOAT_EQUAL0(fParam) == 0)
			{
				SetFloatSigValue(RT_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				RT_SAMP_UpLoadOK_Number,
				//DLOAD_STATUS_START_DLOAD,
				0,
				"CAN_SAMP");

				SetEnumSigValue(RT_GROUP_EQUIPID, 
					SIG_TYPE_SAMPLING,
					RT_SAMP_UpLoadOK_State,
					DLOAD_STATUS_START_DLOAD,
					"CAN_SAMP");

				system(RECT_DLOAD_MOUNT_USB_SHELL);
				g_CanData.CanCommInfo.iUploadFlag = CAN_FLAG_UPDATE;
				//printf("f=255");

				/*fTemp = RT_DLoad();
				SetFloatSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SAMPLING,
							RT_SAMP_UpLoadOK_Number,
							fTemp,
							"CAN_SAMP");

				//printf("Rect Dload=%d",i);
				enumTemp = 0;
				SetEnumSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_HVDC_UPDATE,
							enumTemp,
							"CAN_SAMP");*/
			}
			break;
		case CNRG_RECT_FORCEUPDATE:
			if(FLOAT_EQUAL0(fParam) == 0)
			{
				SetFloatSigValue(RT_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				RT_SAMP_UpLoadOK_Number,
				//DLOAD_STATUS_START_DLOAD,
				0,
				"CAN_SAMP");

				SetEnumSigValue(RT_GROUP_EQUIPID, 
					SIG_TYPE_SAMPLING,
					RT_SAMP_UpLoadOK_State,
					DLOAD_STATUS_START_DLOAD,
					"CAN_SAMP");

				system(RECT_DLOAD_MOUNT_USB_SHELL);
				g_CanData.CanCommInfo.iUploadFlag = CAN_FLAG_FORCEUPDATE;
				/*enumTemp = 0;
				SetEnumSigValue(RT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							RT_HVDC_FORCEUPDATE,
							enumTemp,
							"CAN_SAMP");
				RT_ForceDLoad();*/
			}
			break;
#endif//GC_SUPPORT_RECT_DLOAD
//changed by Frank Wu,11/30,20140217, for upgrading software of rectifier---end---
		default:
			TRACE("Invalid RT Group control channel %d!\n",
					iChannelNo);
			AppLogOut("SAMP", 
					APP_LOG_WARNING, 
					"Invalid RT Group control channel %d!\n",
					iChannelNo);
			break;
		}
		return;
	}
	else
	{
		int		iRectAddr = GetRectAddrByChnNo(iChannelNo);
		/*int		iSubChnNo = ((iChannelNo - CNMR_MAX_RT_BROADCAST_CMD_CHN - 1) 
							% MAX_CHN_NUM_PER_RT) 
							+ CNMR_MAX_RT_BROADCAST_CMD_CHN 
							+ 1;*/
		int		iSubChnNo;

		
		//printf("RT_SendCtlCmd():iChannelNo=%d; Addr=%d;\n", iChannelNo,iRectAddr);

		if(iChannelNo >= CNRS2_DC_ON_OFF)
		{
			iSubChnNo = (iChannelNo - CNRS2_DC_ON_OFF) 
							% MAX_CHN_NUM_PER_RT 
							+CNRS_DC_ON_OFF;
							//+ CNRS2_DC_ON_OFF;
			//printf("!!iChannelNo=%d;\n", iChannelNo);
		}
		else
		{
			iSubChnNo = ((iChannelNo - CNMR_MAX_RT_BROADCAST_CMD_CHN - 1) 
							% MAX_CHN_NUM_PER_RT) 
							+ CNMR_MAX_RT_BROADCAST_CMD_CHN 
							+ 1;
		}

		switch(iSubChnNo)
		{
		case CNRS_DC_ON_OFF:
			g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[iRectAddr].dwDcOnOff
					= FLOAT_EQUAL0(fParam)? RT_DC_ON : RT_DC_OFF;
			break;

		case CNRS_AC_ON_OFF:
			g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[iRectAddr].dwAcOnOff 
					= FLOAT_EQUAL0(fParam)? RT_AC_ON : RT_AC_OFF;
			break;

		case CNRS_LED:
		case CNRS2_LED:
			g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[iRectAddr].dwLedBlink 
					= FLOAT_EQUAL0(fParam)? RT_LED_NORMAL : RT_LED_BLINK;
			break;

		case CNRS_RECT_RESET_FOR_DC_OV:
			g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[iRectAddr].dwResetOnDcOV
					= FLOAT_EQUAL0(fParam)? RT_RESET_NORMAL : RT_RESET_ENB;
			break;

		default:
			TRACE("Invalid RT Unit control channel %d, SubChn = %d!\n",
					iChannelNo,
					iSubChnNo);
			AppLogOut("SAMP", 
					APP_LOG_WARNING, 
					"Invalid RT Unit control channel %d, SubChn = %d!\n",
					iChannelNo,
					iSubChnNo);
			break;
		}
		//printf("RT_SendCtlCmd():g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[%d].dwDcOnOff=%d,fParam=%f~~~~\n",iRectAddr,g_CanData.CanCommInfo.RectCommInfo.aRectSetSig[iRectAddr].dwDcOnOff,fParam);
	}
	return;

}
//changed by Frank Wu,12/30,20140217, for upgrading software of rectifier---start---
#if (GC_SUPPORT_RECT_DLOAD == 1)

#define DLOAD_CAN_ID_NONE	0
#define DLOAD_CAN_ID_CAN1	1
#define DLOAD_CAN_ID_CAN2	2

static int ConvertCanHandleToId(IN int iCanHandle)
{
	int iCanNum = DLOAD_CAN_ID_NONE;

	if(iCanHandle == g_CanData.CanCommInfo.iCanHandle)
	{
		iCanNum = DLOAD_CAN_ID_CAN1;
	}
	else if(iCanHandle == g_CanData.CanCommInfo.iCan2Handle)
	{
		iCanNum = DLOAD_CAN_ID_CAN2;
	}

	return iCanNum;
}

static int ConvertCanIdToHandle(IN int iCanNum)
{
	int iCanHandle = -1;
 
	switch(iCanNum)
	{
		case DLOAD_CAN_ID_CAN1:
		{
			iCanHandle = g_CanData.CanCommInfo.iCanHandle;
			break;
		}
		case DLOAD_CAN_ID_CAN2:
		{
			iCanHandle = g_CanData.CanCommInfo.iCan2Handle;
			break;
		}
		case DLOAD_CAN_ID_NONE:
		default:
		{
			break;
		}
	}

	return iCanHandle;
}

static void PackAndSendDLoad(IN INT32 iCanHandle,
							IN UINT uiProtNo,
							IN UINT uiDestAddr,
							IN UINT uiLen,
							IN BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	UINT		i;

	CAN_StuffHead(uiProtNo,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		uiLen,
		CAN_CMD_TYPE_P2P,
		pbySendBuf);

	for(i = 0;i < uiLen; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_MSG +i] = *(pbyValue + i);
	}

#ifdef GC_DEBUG_RECT_DLOAD
	printf("PackAndSendDLoad::Time:%u, SendTo->RECT:", (UINT)time(NULL));
	for(i = 0;i < CAN_FRAME_OFFSET_MSG + uiLen; i++)
	{
		printf("%2x ", pbySendBuf[i]);
	}
	printf("\n");
#endif//GC_DEBUG_RECT_DLOAD

	write(iCanHandle, (void *)pbySendBuf, CAN_FRAME_LEN);

}

/*==========================================================================*
 * FUNCTION : InitCanToNormal
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void 
 *            
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : wzp                DATE: 2012-11-7 
 *==========================================================================*/
static void InitCanToNormal(void)
{
	int iCanHandle;
	UINT uiMid = CAN_MID1;
	UINT uiMask = CAN_MAM1;
	//CAN1
	iCanHandle = g_CanData.CanCommInfo.iCanHandle;
	ioctl(iCanHandle, SET_RECV1_MID, uiMid);
	ioctl(iCanHandle, SET_RECV1_MASK, uiMask);

	//CAN2
	iCanHandle = g_CanData.CanCommInfo.iCan2Handle;
	ioctl(iCanHandle, SET_RECV2_MID, uiMid);
	ioctl(iCanHandle, SET_RECV2_MASK, uiMask);

	CAN_SELF_ADDR = 0xf0;
	g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = TRUE;
}

static void InitCanToForceDLoad(void)
{
	int iCanHandle;
	UINT uiMid = 0x1cA78003;
	UINT uiMask = 0x1f000000;//0x01f007ff
	//filter commands,just receiving commands which belong to the protocol number 0x1CX
	//CAN1
	iCanHandle = g_CanData.CanCommInfo.iCanHandle;
	ioctl(iCanHandle, SET_RECV1_MID, uiMid);
	ioctl(iCanHandle, SET_RECV1_MASK, uiMask);//0x01f007ff);
	//CAN2
	iCanHandle = g_CanData.CanCommInfo.iCan2Handle;
	ioctl(iCanHandle, SET_RECV2_MID, uiMid);
	ioctl(iCanHandle, SET_RECV2_MASK, uiMask);//0x01f007ff);

	CAN_SELF_ADDR = 0xe0; //According to protocol, the address has to be set to 0xE0
}

static void InitCanToDLoad(void)
{
	int iCanHandle;
	UINT uiMid = 0x1c878003;
	UINT uiMask = 0x1fe00000;//0x01f007ff
	//filter commands,just receiving commands which belong to the protocol number 0x1C8 or 0x1C9
	//CAN1
	iCanHandle = g_CanData.CanCommInfo.iCanHandle;
	ioctl(iCanHandle, SET_RECV1_MID, uiMid);
	ioctl(iCanHandle, SET_RECV1_MASK, uiMask);//0x01f007ff);
	//CAN2
	iCanHandle = g_CanData.CanCommInfo.iCan2Handle;
	ioctl(iCanHandle, SET_RECV2_MID, uiMid);
	ioctl(iCanHandle, SET_RECV2_MASK, uiMask);//0x01f007ff);

	CAN_SELF_ADDR = 0xe0; //According to protocol, the address has to be set to 0xE0
}

static BOOL IsRespondseOfCmd00(IN int iCanHandle, IN SIG_ENUM eStatus)
{
	BOOL bIsRespondse = FALSE;
	int iCanNum;

	//judge which channel of CAN need to respond  
	iCanNum = ConvertCanHandleToId(iCanHandle);

	//check status
	switch(iCanNum)
	{
		case DLOAD_CAN_ID_CAN1://CAN1
		{
			if((eStatus == CAN_SAMPLE_OK)
				||(eStatus == CAN2_RECT_FAIL)
				||(eStatus == CAN_RECT_REALLOCATE))
			{
				bIsRespondse =  TRUE;
			}
			else
			{
				bIsRespondse =  FALSE;
			}
			break;
		}
		case DLOAD_CAN_ID_CAN2://CAN2
		{
			if((eStatus == CAN_SAMPLE_OK)
				||(eStatus == CAN1_RECT_FAIL)
				||(eStatus == CAN_RECT_REALLOCATE))
			{
				bIsRespondse =  TRUE;
			}
			else
			{
				bIsRespondse =  FALSE;
			}
			break;
		}
		case DLOAD_CAN_ID_NONE:
		default:
		{
			break;
		}
	}

	return bIsRespondse;

}

static BOOL ReadDLoadReplyFrames(IN int iCanHandle, 
								IN int iTimeout,
								IN UINT iReadInterval)
{
	int iDataL = 0;
	int iCanNum;
	int i;
	int iTimeoutCount = iTimeout/iReadInterval;

	//judge which channel of CAN need to respond  
	iCanNum = ConvertCanHandleToId(iCanHandle);

	//read data frames
	iDataL = 0;
	for(i = 0;i < iTimeoutCount;i++)
	{
		switch (iCanNum)
		{
			case DLOAD_CAN_ID_CAN1://CAN1
			{
				CAN_ReadFrames(MAX_FRAMES_IN_BUFF,
					g_CanData.CanCommInfo.abyRcvBuf,
					&iDataL);
				break;
			}
			case DLOAD_CAN_ID_CAN2://CAN2
			{
				CAN2_ReadFrames(MAX_FRAMES_IN_BUFF,
					g_CanData.CanCommInfo.abyRcvBuf,
					&iDataL);
				break;
			}
			case DLOAD_CAN_ID_NONE:
			default://unknown, do nothing
			{
				break;
			}
		}

		if(iDataL >= CAN_FRAME_LEN)
		{
			//printf("*****recieve! iDataL=%d\n", iDataL);
			//return FALSE;
			break;
		}

		Sleep(iReadInterval);
	}

	//printf("ReadDLoadReplyFrames::i= %d\n",i);
	if(i == iTimeoutCount)
	{
		//printf("*****Long time!\n");
		return FALSE;
	}

	return TRUE;
}

static BOOL IsRespondseOfDLoad(IN int iTimeout, IN int iCanHandle, IN int iAddr)
{
	UINT Prtocl;
	UINT iReadInterval = 30;//30ms, the interval of scanning frames 

	if( ! ReadDLoadReplyFrames(iCanHandle, iTimeout, iReadInterval))
	{
		//printf("read frames failed\n");
		return FALSE;
	}

	Prtocl = CAN_GetProtNo(g_CanData.CanCommInfo.abyRcvBuf);
	if(((Prtocl != PROTNO_RECT_DLOADFst) && (Prtocl != PROTNO_RECT_DLOADNxt))
		|| (CAN_GetSrcAddr(g_CanData.CanCommInfo.abyRcvBuf) != (UINT)iAddr))
	{
		//printf("*****Prot or Addr Wrong! Prot =%d, Addr=%d/%d\n",
		//		Prtocl,
		//		CAN_GetSrcAddr(g_CanData.CanCommInfo.abyRcvBuf),iAddr);
		return FALSE;
	}

	gs_iReadBinLen = (((int)g_CanData.CanCommInfo.abyRcvBuf[11])<<8) 
						+ (int)g_CanData.CanCommInfo.abyRcvBuf[12];
	gs_iReadBinStart = (((int)g_CanData.CanCommInfo.abyRcvBuf[9])<<8) 
						+ (int)g_CanData.CanCommInfo.abyRcvBuf[10];

#ifdef GC_DEBUG_RECT_DLOAD
	int i;
	printf("IsRespondseOfDLoad::Time:%u, RecvFrom<-RECT:", (UINT)time(NULL));
	for(i = 0;i < CAN_FRAME_LEN; i++)
	{
		printf("%2x ", g_CanData.CanCommInfo.abyRcvBuf[i]);
	}
	printf("\n");
	printf("IsRespondseOfDLoad::gs_iReadBinStart=%d, gs_iReadBinLen=%d\n",
		gs_iReadBinStart, gs_iReadBinLen);
#endif//GC_DEBUG_RECT_DLOAD

	return TRUE;
}

static BOOL IsRespondseOfForceDLoad(IN int iCanHandle, OUT BYTE* bSend)
{
	int i;
	UINT Prtocl;
	UINT iReadInterval = 10;//10ms, the interval of scanning frames 
	int iTimeout = 30;//30ms, the timeout of reading frames

	if( ! ReadDLoadReplyFrames(iCanHandle, iTimeout, iReadInterval))
	{
		//printf("read frames failed\n");
		return FALSE;
	}

	Prtocl = CAN_GetProtNo(g_CanData.CanCommInfo.abyRcvBuf);
	if(Prtocl != PROTNO_RECT_FORCE)
	{
		//printf("*****Prot or Addr Wrong! Prot =%d\n",Prtocl);
		return FALSE;
	}

	for(i = 0;i < 8; i++)
		*(bSend+i) = g_CanData.CanCommInfo.abyRcvBuf[5+i];

#ifdef GC_DEBUG_RECT_DLOAD
	printf("Force DLoad::Time:%u, RecvFrom<-RECT:", (UINT)time(NULL));
	for(i = 0;i < CAN_FRAME_LEN; i++)
	{
		printf("%2x ", g_CanData.CanCommInfo.abyRcvBuf[i]);
	}
	printf("\n");
#endif//GC_DEBUG_RECT_DLOAD	

	return TRUE;
}

static BOOL	 RequestRectForceDLoad(IN int iTimeout,
									IN BYTE *pbAbyVal,
									OUT int *piCanhandle)
{
	int iTimeoutCount = iTimeout/60;//CAN1 timeout 30ms, CAN2 timeout 30ms
	int iCanHandle;
	int i;

	for(i = 0; i < iTimeoutCount; i++)
	{
		//CAN1
		iCanHandle = g_CanData.CanCommInfo.iCanHandle;
		PackAndSendDLoad(iCanHandle,
						PROTNO_RECT_FORCE,
						0,
						0,
						pbAbyVal);
#ifdef GC_DEBUG_RECT_DLOAD
		printf("Force DLoad::CAN1 SendTo->Rect PROTNO_RECT_FORCE=%4x, i=%d\n", 
			PROTNO_RECT_FORCE,
			i);
#endif//GC_DEBUG_RECT_DLOAD
		if(IsRespondseOfForceDLoad(iCanHandle, pbAbyVal))//30ms, wait for rectifier responding
		{
#ifdef GC_DEBUG_RECT_DLOAD
			printf("Force DLoad::CAN1 RecvFrom<-Rect ACK PROTNO_RECT_FORCE=%4x\n", PROTNO_RECT_FORCE);
#endif//GC_DEBUG_RECT_DLOAD	
			break;
		}
		//CAN2
		iCanHandle = g_CanData.CanCommInfo.iCan2Handle;
		PackAndSendDLoad(iCanHandle,
						PROTNO_RECT_FORCE,
						0,
						0,
						pbAbyVal);
#ifdef GC_DEBUG_RECT_DLOAD
		printf("Force DLoad::CAN2 SendTo->Rect PROTNO_RECT_FORCE=%4x, i=%d\n", 
			PROTNO_RECT_FORCE,
			i);
#endif//GC_DEBUG_RECT_DLOAD
		if(IsRespondseOfForceDLoad(iCanHandle, pbAbyVal))//30ms, wait for rectifier responding
		{
#ifdef GC_DEBUG_RECT_DLOAD
			printf("Force DLoad::CAN2 RecvFrom<-Rect ACK PROTNO_RECT_FORCE=%4x\n", PROTNO_RECT_FORCE);
#endif//GC_DEBUG_RECT_DLOAD	
			break;
		}
	}

	if(i == iTimeoutCount)
	{
		return FALSE;
	}

	//reply the ACK of PROTNO_RECT_FORCE to rectifier
	pbAbyVal[0] = 0x80;
	PackAndSendDLoad(iCanHandle,
					PROTNO_RECT_FORCE,
					0,
					8,
					pbAbyVal);

#ifdef GC_DEBUG_RECT_DLOAD
	printf("Force DLoad::SendTo->Rect ACK PROTNO_RECT_FORCE=%4x\n", PROTNO_RECT_FORCE);
#endif//GC_DEBUG_RECT_DLOAD

	*piCanhandle = iCanHandle;

	return TRUE;
}

static BOOL ForceDLoad(OUT int *piErr)
{
	BYTE abyVal[8];
	int i;
	BYTE bLen,bReadBuf[RECT_DLOAD_BIN_SIZE];
	SIG_ENUM	emRst;
	char s[] = RECT_F_DLOAD_FILE;
	int iCanHandle = -1;
	int iResend = 0;
	int iResendCount = 2;
	int iTimeoutCount = 0;

	if(piErr != NULL)
	{
		*piErr = 0;
	}

	//printf("filename=%s\n", s);
	USBInfor.iUSBHandle = open(s,O_RDWR);
	if(USBInfor.iUSBHandle < 0)
	{
		//printf("open bin error, stat = %d\n",USBInfor.iUSBHandle);
		if(piErr != NULL)
		{
			*piErr = 1;
		}
		return FALSE;//�ļ�������
	}
	else
	{
		memset(bReadBuf, 0, sizeof(bReadBuf));
		read(USBInfor.iUSBHandle, bReadBuf, RECT_DLOAD_BIN_SIZE);
		close(USBInfor.iUSBHandle);
		USBInfor.iUSBHandle = 0;
	}
	//printf("open bin ok\n");

	//find out the rectifier which is in the mode of bootloader
	if(! RequestRectForceDLoad(RECT_F_DLOAD_SCAN_TIME, abyVal, &iCanHandle))
	{
		if(piErr != NULL)
		{
			*piErr = 3;
		}
		return FALSE;
	}
	RunThread_Heartbeat(RunThread_GetId(NULL));
	//start to force download
	while(IsRespondseOfDLoad(RECT_DLOAD_COMM_TIMEOUT, iCanHandle, 0))
	{
		if(iTimeoutCount%30 == 0)
		{
			RunThread_Heartbeat(RunThread_GetId(NULL));
		}

		i = 0;
		//communication except, is in the circle of death
		iTimeoutCount++;
		if(iTimeoutCount > RECT_DLOAD_BIN_SIZE)
		{
			if(piErr != NULL)
			{
				*piErr = 2;
			}
			return FALSE;
		}

		//data except
		//if(gs_iReadBinLen > RECT_DLOAD_BIN_SIZE)			//��ȡ���ݳ�������ֹ���أ�����
		if(gs_iReadBinStart + gs_iReadBinLen > RECT_DLOAD_BIN_SIZE)	//��ȡ���ݳ�������ֹ���أ�����
		{
			//printf("gs_iReadBinLen=%d > RECT_DLOAD_BIN_SIZE=%d\n", gs_iReadBinLen, RECT_DLOAD_BIN_SIZE);
			if(piErr != NULL)
			{
				*piErr = 2;
			}
			break;
		}
		//finish downloading
		if(gs_iReadBinLen == 0)			//���ؽ��� OK
		{
			InitCanToNormal();//before calling the function of RtSampleCmd00
			//printf("finished,Time=%u\n", (UINT)time(NULL));
			Sleep(RECT_DLOAD_WAIT_RESTART_TIME);
			emRst =RtSampleCmd00(0);
			//printf("RtSampleCmd00, Time=%u, emRst=%d\n", (UINT)time(NULL), (int)emRst);
			if(IsRespondseOfCmd00(iCanHandle, emRst))
			{
				if(piErr != NULL)
				{
					*piErr = 0;
				}
				return TRUE;
			}

			break;
		}
		//send data block to rectifier, only send 8 bytes per times 
		while(gs_iReadBinLen > 0)
		{
			if(gs_iReadBinLen > 8)
			{
				bLen = 8;
				iResendCount = 2;//every packet resends 2 times
			}
			else
			{
				bLen = gs_iReadBinLen;
				iResendCount = 1;//the last packet of every block can't be resend, otherwise no reply
			}

			for(iResend = 0; iResend < iResendCount; iResend++)
			{				
				//CMD_NxtBoot(&USBInfor.bReadBuf[i], bLen,iAddr);
				PackAndSendDLoad(iCanHandle,
								(UINT)(PROTNO_RECT_DLOADSendBase + i/8),
								0,
								bLen,
								&bReadBuf[gs_iReadBinStart + i]);
				Sleep(5);
			}
			gs_iReadBinLen -= bLen;
			i += bLen;
			//printf("go on!!! i=%d\n",i);
		}
		//Sleep(50);
	}

	if(piErr != NULL)
	{
		*piErr = 3;
	}
	return FALSE;
}

int RT_ForceDLoad(void)
{
	int iState;
	int iErrNo = 0;
	SIG_ENUM enumTemp = DLOAD_STATUS_FORCEUPDATE_OK;
	const char sErrStr[][50] = { 
		"successful",
		"open file failed",
		"rectifier reply error",
		"rectifier reply time-out"
	};
	
	InitCanToForceDLoad();
	iState = ForceDLoad(&iErrNo);
	InitCanToNormal();

	if(iState)
	{
		//there are some matter with the format of log file, so adding the char "\n" 
		//AppLogOut("RUPD", APP_LOG_INFO, "\n");
		AppLogOut("FRUP", 
			APP_LOG_INFO, 
			"Rectifier force update successful!\n");

		SetFloatSigValue(RT_GROUP_EQUIPID, 
			SIG_TYPE_SAMPLING,
			RT_SAMP_UpLoadOK_Number,
			1,
			"CAN_SAMP");
	}
	else
	{
		//there are some matter with the format of log file, so adding the char "\n" 
		//AppLogOut("RUPD", APP_LOG_INFO, "\n");
		AppLogOut("FRUP", 
			APP_LOG_INFO, 
			"Rectifier force update failed! Reason: %s\n",
			sErrStr[iErrNo]);

		SetFloatSigValue(RT_GROUP_EQUIPID, 
			SIG_TYPE_SAMPLING,
			RT_SAMP_UpLoadOK_Number,
			0,
			"CAN_SAMP");
	}

	enumTemp = DLOAD_STATUS_FORCEUPDATE_OK;
	if(iErrNo == 1)
	{
		enumTemp = DLOAD_STATUS_OPEN_FILE_FAIL;
	}
	else if((iErrNo == 2) || (iErrNo == 3))
	{
		enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
	}
	SetEnumSigValue(RT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		RT_SAMP_UpLoadOK_State,
		enumTemp,
		"CAN_SAMP");


#ifdef GC_DEBUG_RECT_DLOAD
	if(iState)
	{
		printf("Force DLoad::Successful\n");
	}
	else
	{
		printf("Force DLoad::Failed\n");
	}
#endif//GC_DEBUG_RECT_DLOAD

	return iState;
}

static void RequestRectDLoad(IN int iCanHandle, IN int uiAddr)
{
	BYTE abyVal[8];
	//build command of PROTNO 0x60, MSGTYPE 0x03, VALUETYPE 0xFE 
	abyVal[0] = 3;//MSGTYPE
	abyVal[1] = 0xf0;
	abyVal[2] = 0;
	abyVal[3] = 0xfe;//VALUETYPE
	abyVal[4] = 0;
	abyVal[5] = 1; //0, stop updating; 1,start updating the first DSP; 2, Start updating the second DSP
	abyVal[6] = 0;
	abyVal[7] = 0;
	PackAndSendDLoad(iCanHandle,
					PROTNO_RECT_CONTROLLER,
					(UINT)uiAddr,
					8,
					abyVal);

	/*PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
						uiAddr,
						CAN_CMD_TYPE_P2P,
						0,
						RT_CMD_DLoad,
						abyVal);*/
}

/*==========================================================================*
 * FUNCTION : DLoad
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: 
 * RETURN   : DLOAD_END 
 * COMMENTS : 
 * CREATOR  : wzp                DATE: 2012-11-7 
 *==========================================================================*/

static BOOL DLoad(IN int iCanHandle, IN int iAddr, IN int iInfoIndex, OUT int *piErr)
{
	int i;
	//int k;
	BYTE bLen,bReadBuf[RECT_DLOAD_BIN_SIZE];
	//char bVersion;
	//char s[21] = "/usb/";  //"/usb/H1R12U211.bin";/usr/R482000E3_A02.bin
	char s[40] = "/usb/";  //"/usb/H1R12U211.bin";/usr/R482000E3_A02.bin
	SIG_ENUM	emRst;
	int iResend = 0;
	int iResendCount = 2;
	char cChar = 0;
	int iTimeoutCount = 0;
	int iNodeType = 0;
	UINT uiVerNo = 0;
	char cVerM = 'A';
	UINT uiVerS = 0;
	UINT uiSWVer = 0;

	if(piErr != NULL)
	{
		*piErr = 0;
	}

	/*Barcode_S[0][0] = 'R';
	Barcode_S[0][1] = '2';
	Barcode_S[0][2] = '9';
	Barcode_S[0][3] = '0';
	Barcode_S[0][4] = '0';
	Barcode_S[0][5] = 'U';
	Barcode_S[0][6] = '1';
	Barcode_S[0][7] = 0x20;
	RTSN_HIGH[iAddr] = 0x10;//16, not be used now

	g_CanData.aRoughDataRect[0][RECT_VERSION_NO].uiValue = 0;*/
	for(i = 0;i < BARCODE_LEN;i++)
	{
		cChar = Barcode_S[iInfoIndex][i];
		if(cChar == 0x20)
		{
			break;
		}
		else
		{
			//changed,1/N,20140227
			//if( islower(cChar) )
			//{
			//	cChar -= 32;//to upper
			//}
			sprintf((s+5+i), "%c",cChar);
		}
	}

	uiVerNo = g_CanData.aRoughDataRect[iInfoIndex][RECT_VERSION_NO].uiValue;
	cVerM = ((uiVerNo >> 24) & 0xff) + 'A';//0->'A', 1->'B',2->'C'...etc.
	uiVerS = (uiVerNo >> 16) & 0xff;
	//changed by Frank Wu,1/1/N,20150120, for TR version range too small
	//uiSWVer = (uiVerNo & 0xff);
	uiSWVer = (uiVerNo & 0xffff);
	//sprintf((s+5+i), "_%c%02u", cVerM, uiVerS);//_A01,_B08
	//_A01_Vxxx.bin, version number at least 3 digit, eg:v0.03 is _A01_V003.bin, v21.33 is _A01_V2133.bin
	sprintf((s+5+i), "_%c%02u_V%03u%s", cVerM, uiVerS, uiSWVer, ".bin");//_A01_V111.bin,_B08_V123.bin
#ifdef GC_DEBUG_RECT_DLOAD
	printf("DLoad::Rectifier Address=%d, Ver=%8x, Hw=%c%02u Sw=V%d\n", 
					iAddr, uiVerNo, cVerM, uiVerS, (uiVerNo & 0xff));
#endif//GC_DEBUG_RECT_DLOAD

	//printf("s=%s %d\n",s,i);
	//OCT3, Node Type of CAN, be used for bootloader
	//0 = Controller	1 = Rectifier 	2 = Converter	3 = LC	4 = Inverter 5 = HVDC	6 = MPPT
	//printf("RTSN_HIGH[%d]=%8x\n", iInfoIndex, RTSN_HIGH[iInfoIndex]);
	//iNodeType = (RTSN_HIGH[iInfoIndex] >> 8) & 0xff;
	//sprintf((s+5+i), "_%03d", iNodeType);
	//sprintf((s+5+i), "_%03d",RTSN_HIGH[iAddr]);
	/*if(RTSN_HIGH[iAddr] < 10)
	{
		sprintf((s+5+i), "%s%1d","_", RTSN_HIGH[iAddr]);
	}
	else
	{
		sprintf((s+5+i), "%s","_9");
	}*/
	//printf("s=%s %d %d\n",s,bVersion,i);
	/*bVersion = 0x41 + ((char)(g_CanData.aRoughDataRect[iAddr][RECT_VERSION_NO].uiValue >> 24) & 0x3f);
	sprintf((s+9+i), "%1s",&bVersion);
	k = (BYTE)(g_CanData.aRoughDataRect[iAddr][RECT_VERSION_NO].uiValue >> 16);
	sprintf((s+10+i), "%02d",k);*/

	//sprintf((s+9+i), "%s",".bin");

	//printf("iAddr=%d, filename=%s\n", iAddr, s);
	USBInfor.iUSBHandle = open(s,O_RDWR);
	if(USBInfor.iUSBHandle < 0)		
	{
		//printf("open bin failed, stat = %d\n",USBInfor.iUSBHandle);
		if(piErr != NULL)
		{
			*piErr = 1;
		}
		
		AppLogOut("RUPD", APP_LOG_INFO, "RectAddr=%d, open file=%s failed\n", iAddr, s);
		
		return FALSE;//�ļ�������
	}
	else
	{
		memset(bReadBuf, 0, sizeof(bReadBuf));
		read(USBInfor.iUSBHandle, bReadBuf, RECT_DLOAD_BIN_SIZE);
		close(USBInfor.iUSBHandle);
		USBInfor.iUSBHandle = 0;
	}
	//printf("open bin OK!\n");

	//request the rectifier to start the function of downloading
	RequestRectDLoad(iCanHandle, iAddr);

	RunThread_Heartbeat(RunThread_GetId(NULL));
	while(IsRespondseOfDLoad(RECT_DLOAD_COMM_TIMEOUT, iCanHandle, iAddr))
	{
		if(iTimeoutCount%30 == 0)
		{
			RunThread_Heartbeat(RunThread_GetId(NULL));
		}

		i = 0;
		//communication except, is in the circle of death
		iTimeoutCount++;
		if(iTimeoutCount > RECT_DLOAD_BIN_SIZE)
		{
			if(piErr != NULL)
			{
				*piErr = 2;
			}
			return FALSE;
		}
		//data except
		//if(gs_iReadBinLen > RECT_DLOAD_BIN_SIZE)	//��ȡ���ݳ�������ֹ���أ�����
		if(gs_iReadBinStart + gs_iReadBinLen > RECT_DLOAD_BIN_SIZE)	//��ȡ���ݳ�������ֹ���أ�����
		{
			//printf("gs_iReadBinLen=%d > RECT_DLOAD_BIN_SIZE=%d\n", gs_iReadBinLen, RECT_DLOAD_BIN_SIZE);
			if(piErr != NULL)
			{
				*piErr = 2;
			}
			return FALSE;
		}
		//finish downloading
		if(gs_iReadBinLen == 0)//���ؽ��� OK
		{
			InitCanToNormal();//before calling the function of RtSampleCmd00
			//printf("finished,Time=%u\n", (UINT)time(NULL));
			Sleep(RECT_DLOAD_WAIT_RESTART_TIME);
			emRst =RtSampleCmd00(iAddr);
			//printf("RtSampleCmd00 Time=%u, emRst=%d\n", (UINT)time(NULL), (int)emRst);
			if(IsRespondseOfCmd00(iCanHandle, emRst))
			{
				if(piErr != NULL)
				{
					*piErr = 0;
				}
				return TRUE;
			}
			break;
		}
		//send data block to rectifier, only send 8 bytes per times 
		while(gs_iReadBinLen > 0)
		{
			if(gs_iReadBinLen > 8)
			{
				bLen = 8;
				iResendCount = 2;//every packet resends 2 times
			}
			else
			{
				bLen = gs_iReadBinLen;
				iResendCount = 1;//the last packet of every block can't be resend, otherwise no reply
			}
			
			for(iResend = 0; iResend < iResendCount; iResend++)
			{				
				//CMD_NxtBoot(&USBInfor.bReadBuf[i], bLen,iAddr);
				PackAndSendDLoad(iCanHandle,
					(UINT)(PROTNO_RECT_DLOADSendBase + i/8),
					(UINT)iAddr,
					bLen,
					&bReadBuf[gs_iReadBinStart + i]);
				Sleep(5);
			}
			gs_iReadBinLen -= bLen;
			i += bLen;
			//printf("go on! i=%d\n",i);
		}
		//Sleep(50);
	}

	if(piErr != NULL)
	{
		*piErr = 3;
	}
	return FALSE;
}


int RT_DLoad(void)
{
	int iDLoadNumCan1 = 0;
	int iDLoadNumCan2 = 0;
	int iAddr = 0;
	int iRectCount = 0;
	int iCanHandle = -1;
	int iIndexOffsetBase = 0;
	int iInfoIndex = 0;
	BOOL bIsExistError = FALSE;
	int iErrNo = 0;
	SIG_ENUM enumTemp = DLOAD_STATUS_NORMALUPDATE_OK;
	const char sErrStr[][50] = { 
		"successful",
		"open file failed",
		"rectifier reply error",
		"rectifier reply time-out"
	};

	//CAN1
	iRectCount = g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;
	iCanHandle = g_CanData.CanCommInfo.iCanHandle;
	iIndexOffsetBase = 0;
	iInfoIndex = 0;
	iDLoadNumCan1 = 0;

	//printf("RT_DLoad::scanning CAN1\n");
	for(iAddr = 0; iAddr < iRectCount; iAddr++)
	{
		if(bIsExistError)
		{
			break;
		}

		iInfoIndex = iIndexOffsetBase + iAddr;

		InitCanToDLoad();
		iErrNo = 0;
		if(DLoad(iCanHandle, iAddr, iInfoIndex, &iErrNo))
		{
			//there are some matter with the format of log file, so adding the char "\n" 
			//AppLogOut("RUPD", APP_LOG_INFO, "\n");
			AppLogOut("RUPD", 
				APP_LOG_INFO, 
				"%11.11s: Normal update successful!\n",
				Rect_Sn[iInfoIndex]);

			iDLoadNumCan1++;

			SetFloatSigValue(RT_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				RT_SAMP_UpLoadOK_Number,
				iDLoadNumCan1 + iDLoadNumCan2,
				"CAN_SAMP");
		}
		else
		{
			//there are some matter with the format of log file, so adding the char "\n" 
			//AppLogOut("RUPD", APP_LOG_INFO, "\n");
			AppLogOut("RUPD", 
					APP_LOG_INFO, 
					"%11.11s: Normal update failed! Reason: %s\n",
					Rect_Sn[iInfoIndex],
					sErrStr[iErrNo]);

			bIsExistError = TRUE;

			if(1 == iErrNo)//open file failed, continue to upgrade
			{
				bIsExistError = FALSE;
			}
		}

		InitCanToNormal();
		RunThread_Heartbeat(RunThread_GetId(NULL));// feed the watchdog

#ifdef GC_DEBUG_RECT_DLOAD
		if(bIsExistError)
		{
			printf("DLoad::Failed\n");
		}
		else
		{
			printf("DLoad::Successful\n");
		}
#endif//GC_DEBUG_RECT_DLOAD
	}

	//CAN2
	iRectCount = g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2;
	iCanHandle = g_CanData.CanCommInfo.iCan2Handle;
	iIndexOffsetBase = g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;
	iInfoIndex = 0;
	iDLoadNumCan2 = 0;

	//printf("RT_DLoad::scanning CAN2\n");
	for(iAddr = 0; iAddr < iRectCount; iAddr++)
	{
		if(bIsExistError)
		{
			break;
		}

		iInfoIndex = iIndexOffsetBase + iAddr;

		InitCanToDLoad();
		iErrNo = 0;
		if(DLoad(iCanHandle, iAddr, iInfoIndex, &iErrNo))
		{
			//there are some matter with the format of log file, so adding the char "\n" 
			//AppLogOut("RUPD", APP_LOG_INFO, "\n");
			AppLogOut("RUPD", 
					APP_LOG_INFO, 
					"%11.11s: Normal update successful!\n",
					Rect_Sn[iInfoIndex]);

			iDLoadNumCan2++;

			SetFloatSigValue(RT_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				RT_SAMP_UpLoadOK_Number,
				iDLoadNumCan1 + iDLoadNumCan2,
				"CAN_SAMP");
		}
		else
		{
			//there are some matter with the format of log file, so adding the char "\n" 
			//AppLogOut("RUPD", APP_LOG_INFO, "\n");
			AppLogOut("RUPD", 
				APP_LOG_INFO, 
				"%11.11s: Normal update failed! Reason: %s\n",
				Rect_Sn[iInfoIndex],
				sErrStr[iErrNo]);
			bIsExistError = TRUE;

			if(1 == iErrNo)//open file failed, continue to upgrade
			{
				bIsExistError = FALSE;
			}
		}

		InitCanToNormal();
		RunThread_Heartbeat(RunThread_GetId(NULL));// feed the watchdog

#ifdef GC_DEBUG_RECT_DLOAD
		if(bIsExistError)
		{
			printf("DLoad::Failed\n");
		}
		else
		{
			printf("DLoad::Successful\n");
		}
#endif//GC_DEBUG_RECT_DLOAD
	}

	enumTemp = DLOAD_STATUS_NORMALUPDATE_OK;
	if(iDLoadNumCan1 + iDLoadNumCan2 > 0)
	{
		enumTemp = DLOAD_STATUS_NORMALUPDATE_OK;
	}
	else if(iErrNo == 1)
	{
		enumTemp = DLOAD_STATUS_OPEN_FILE_FAIL;
	}
	else if((iErrNo == 2) || (iErrNo == 3))
	{
		enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
	}
	else
	{
		enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
	}
	SetEnumSigValue(RT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		RT_SAMP_UpLoadOK_State,
		enumTemp,
		"CAN_SAMP");



#ifdef GC_DEBUG_RECT_DLOAD
	printf("RT_DLoad::CAN1 Total Num=%d,Success Num=%d\n",
			g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1,
			iDLoadNumCan1);
	printf("RT_DLoad::CAN2 Total Num=%d,Success Num=%d\n",
			g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2,
			iDLoadNumCan2);
#endif//GC_DEBUG_RECT_DLOAD

	return iDLoadNumCan1 + iDLoadNumCan2;
}

#endif//GC_SUPPORT_RECT_DLOAD
//changed by Frank Wu,12/30,20140217, for upgrading software of rectifier---end---


#define	RT_NO_DISCHAGE_ST		0
#define	RT_DISCHAGE_ST			1
/*==========================================================================*
 * FUNCTION : RtJudgeAcFail
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-07 17:21
 *==========================================================================*/
static BOOL RtJudgeAcFail(void)
{
	static	BOOL	s_bLastAcFail = FALSE;
	static INT32	s_iDelayRCTAcFailTimes = 0;
	int				i;
	BOOL			bAcFail = TRUE;
	BOOL			bAllCommInterrupt = TRUE;

#define  RCT_AC_FAIL_DELAY  4

	if (s_iDelayRCTAcFailTimes < RCT_AC_FAIL_DELAY)
	{
		s_iDelayRCTAcFailTimes++;// Reconfig Query  Query  Query. Enough!
	}

	if(RT_THREE_PHASE 
		== g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue)
	{
		for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
		{
			if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
			{
				continue;
			}

			bAllCommInterrupt = FALSE;

			if(g_CanData.aRoughDataRect[i][RECT_AC_UNDER_VOLT_STATUS].iValue)
			{
				if(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE].iValue)
				{
					g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_FAILURE;
					
					if (s_iDelayRCTAcFailTimes < RCT_AC_FAIL_DELAY)
					{
						g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_NORMAL;
					}
				}
				else if((g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE].fValue < RT_AC_FAIL_POINT)
					&& (g_CanData.aRoughDataRect[i][RECT_PH_B_VOLTAGE].fValue < RT_AC_FAIL_POINT)
					&& (g_CanData.aRoughDataRect[i][RECT_PH_C_VOLTAGE].fValue < RT_AC_FAIL_POINT))
				{
					g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_FAILURE;

					if (s_iDelayRCTAcFailTimes < RCT_AC_FAIL_DELAY)
					{
						g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_NORMAL;
					}
				}
				else
				{
					g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_LOW_VOLT;	

					if (s_iDelayRCTAcFailTimes < RCT_AC_FAIL_DELAY)
					{
						g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_NORMAL;
					}
				}
			}
			else
			{
				g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_NORMAL;
			}
		}
	}
	else
	{
		for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
		{

			//printf("\n	This is g_CanData.aRoughDataRect[%d][RECT_AC_VOLTAGE].fValue =%f	\n",i,
			//		g_CanData.aRoughDataRect[i][RECT_AC_VOLTAGE].fValue);

			//printf("	g_CanData.aRoughDataRect[%d][RECT_INTERRUPT_TIMES].iValue=%d	\n",i,
			//	g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue);

			if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
			{
				continue;
			}

			bAllCommInterrupt = FALSE;

			if(g_CanData.aRoughDataRect[i][RECT_AC_VOLTAGE].fValue < RT_AC_FAIL_POINT)
			{
				g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_FAILURE;
				 
				if (s_iDelayRCTAcFailTimes < RCT_AC_FAIL_DELAY)
				{
					//printf("\n	1111	\n");
					g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_NORMAL;
				}
			}
			else
			{
				//printf("\n	222	\n");
				g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue = RT_AC_NORMAL;
			}
		}
	}


	for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
	{
		if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
		{
			continue;
		}

		if(g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue != RT_AC_FAILURE)
		{
			bAcFail = FALSE;
			break;
		}
	}

	if(bAllCommInterrupt)
	{
		if(DC_TYPE_RETURN_DOUBLE 
			!= g_CanData.aRoughDataGroup[GROUP_RT_DC_DC_TYPE].iValue)
		{
			if( (RT_NO_DISCHAGE_ST != GetEnumSigValue(BT_GROUP_EQUIPID, 
											SIG_TYPE_SAMPLING,
											BT_BATT_DISCH_SIGID,
											"CAN_SAMP"))//�ŵ�
				&& (RT_MMPPT_MODE_DISABLE == GetEnumSigValue(RT_SYSTEM_EQUIP_ID, 
							SIG_TYPE_SETTING,
							RT_MPPT_MOD_SIG_ID,
								"Get MPPT Running Mode")) )//�� ����MPPTģʽ��
			{
				bAcFail = TRUE;			
			}
			else
			{
				//��������磬����ģ�鶼ͨ���жϵĻ�����ǰ��δ�ж��齻��ͣ��?
				//���ڸ���Ϊ�����жϡ�
				for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
				{
					if(g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue != RT_AC_FAILURE)
					{
						bAcFail = FALSE;
						break;
					}
				}
			}
		}
		else
		{
			bAcFail = s_bLastAcFail;
		}
	}

	s_bLastAcFail = bAcFail;

	return bAcFail;
}


/*==========================================================================*
 * FUNCTION : RtCalcSinglePhaseVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-08 17:49
 *==========================================================================*/
static void RtCalcSinglePhaseVolt(void)
{
	int				i, iPhaseNo;
	SAMPLING_VALUE	MaxVolt[RT_AC_PHASE_NUM];
	BOOL			bCalculated = FALSE;

	if(RT_THREE_PHASE 
		== g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue)
	{
		for(i = 0; i < RT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_RT_AC_P_A_VOLT + i].iValue 
				= CAN_SAMP_INVALID_VALUE;
		}
		return;
	}
	
	for(i = 0; i < RT_AC_PHASE_NUM; i++)
	{
		MaxVolt[i].iValue = CAN_SAMP_INVALID_VALUE;
	}

	for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
	{
		iPhaseNo = g_CanData.aRoughDataRect[i][RECT_PHASE_NO].iValue;

		if((iPhaseNo < 0) 
			|| (iPhaseNo >= RT_AC_PHASE_NUM))
		{
			continue;
		}
				
		if( DC_TYPE_RETURN_DOUBLE 
			!= g_CanData.aRoughDataGroup[GROUP_RT_DC_DC_TYPE].iValue && g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue == RT_AC_FAILURE)
		{
			if(MaxVolt[iPhaseNo].iValue == CAN_SAMP_INVALID_VALUE)
			{
				MaxVolt[iPhaseNo].fValue = 0.0;
			}
			bCalculated = TRUE;
			//printf("1       Rect %d value iPhaseNo = %d is %f\n", i,iPhaseNo, MaxVolt[iPhaseNo].fValue);
			continue;
		}
		else if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
		{
			//printf("2    Rect %d value continue\n", i);
			continue;
		}

		if((MaxVolt[iPhaseNo].iValue == CAN_SAMP_INVALID_VALUE)
			|| (MaxVolt[iPhaseNo].fValue 
				< g_CanData.aRoughDataRect[i][RECT_AC_VOLTAGE].fValue))
		{
			MaxVolt[iPhaseNo].fValue 
				= g_CanData.aRoughDataRect[i][RECT_AC_VOLTAGE].fValue;
			//printf("3    Rect %d value phase %d continue%f\n", i,iPhaseNo, MaxVolt[iPhaseNo].fValue);
			bCalculated = TRUE;
		}
	}

	if(bCalculated)
	{	
		for(i = 0; i < RT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_RT_AC_P_A_VOLT + i].iValue
				= MaxVolt[i].iValue;
			//printf("MaxVolt[%d].fValue =%f\n ",i, MaxVolt[i].fValue);
		}
	}

	return;
}


/*==========================================================================*
 * FUNCTION : RtCalcThreePhaseVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-08 17:49
 *==========================================================================*/
static void RtCalcThreePhaseVolt(void)
{
	int				i, j;
	SAMPLING_VALUE	AcVolt[RT_AC_PHASE_NUM];
	BOOL			bCalculated = FALSE;

	if(RT_SINGLE_PHASE 
		== g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue)
	{
		for(i = 0; i < RT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_RT_AC_P_AB_VOLT + i].iValue 
				= CAN_SAMP_INVALID_VALUE;
		}
		return;
	}
	
	for(i = 0; i < RT_AC_PHASE_NUM; i++)
	{
		AcVolt[i].iValue = CAN_SAMP_INVALID_VALUE;
	}

	for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
	{
		if((g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
			|| (g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE].iValue == CAN_SAMP_INVALID_VALUE))
		{
			continue;
		}

		if((g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE + 0].fValue < 80) 
			|| (g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE + 1].fValue < 80) 
			|| (g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE + 2].fValue < 80) )
		{
			for(j = 0; j < RT_AC_PHASE_NUM; j++)
			{
				AcVolt[j].fValue 
					= g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE + j].fValue;
			}
			bCalculated = TRUE;
			continue;
		}
		
		for(j = 0; j < RT_AC_PHASE_NUM; j++)
		{
			AcVolt[j].fValue 
				= g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE + j].fValue;
		}
		bCalculated = TRUE;

		break;
	}

	if(bCalculated)
	{	
		for(i = 0; i < RT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_RT_AC_P_AB_VOLT + i].iValue
				= AcVolt[i].iValue;
		}
	}

	return;
}


/*==========================================================================*
 * FUNCTION : RtCalcAcVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-08 17:49
 *==========================================================================*/
static void RtCalcAcVolt(void)
{
	int			i;

	RtCalcSinglePhaseVolt();
	RtCalcThreePhaseVolt();
	if(RT_SINGLE_PHASE 
		== g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue)
	{
		for(i = 0; i < RT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_RT_AC_SNMP_VOLT1 + i].iValue
				= g_CanData.aRoughDataGroup[GROUP_RT_AC_P_A_VOLT + i].iValue;
		}
	}
	else
	{
		for(i = 0; i < RT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_RT_AC_SNMP_VOLT1 + i].iValue
				= g_CanData.aRoughDataGroup[GROUP_RT_AC_P_AB_VOLT + i].iValue;
		}	
	}

	if(GetEnumSigValue(1, 
			SIG_TYPE_SETTING,
			SYS_CONVERTER_ONLY_ID,
			"CAN_SAMP")) //Converter only enable, set all AC signal to invalid.
	{
		for(i = 0; i < RT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_RT_AC_P_A_VOLT + i].iValue 
				= CAN_SAMP_INVALID_VALUE;
			g_CanData.aRoughDataGroup[GROUP_RT_AC_P_AB_VOLT + i].iValue 
				= CAN_SAMP_INVALID_VALUE;
			g_CanData.aRoughDataGroup[GROUP_RT_AC_SNMP_VOLT1 + i].iValue
				= CAN_SAMP_INVALID_VALUE;
		}		
	}
	return;
}

//static int RtCalcThreePhaseVolt(int iPhaseNo)
//{
//	int				i;
//	SAMPLING_VALUE	AcVolt;
//	
//	AcVolt.iValue = CAN_SAMP_INVALID_VALUE;
//
//	for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
//	{
//		if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
//		{
//			continue;
//		}
//
//		if(CAN_SAMP_INVALID_VALUE 
//			!= g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE + iPhaseNo].iValue)
//
//		{
//			AcVolt.fValue = g_CanData.aRoughDataRect[i][RECT_PH_A_VOLTAGE + iPhaseNo].fValue;
//			break;
//		}
//
//	}
//
//	return AcVolt.iValue;
//
//}


/*==========================================================================*
 * FUNCTION : RtCalcRatedVolt
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-07 17:31
 *==========================================================================*/
//static int RtCalcRatedVolt(void)
//{
//	int				i;
//	SAMPLING_VALUE	RatedVolt;
//
//	RatedVolt.fValue = 48.0;
//
//	for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
//	{
//		if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
//		{
//			continue;
//		}
//
//		if(CAN_SAMP_INVALID_VALUE != g_CanData.aRoughDataRect[i][RECT_RATED_VOLT].iValue)
//		{
//			RatedVolt.fValue = g_CanData.aRoughDataRect[i][RECT_RATED_VOLT].fValue;
//			break;
//		}
//	}
//
//	return RatedVolt.iValue;
//}

//����HVDC��ʱ���ֵ�
//1.���ֶ����ģ���ʱ�򣬻ᵼ��CAN���ϣ�
//���Ǵ�ʱ�����ϻ�������ģ�飬������Ҫ����ɨ��
//2.����LVD����ģ�鶼ͣ���ˣ����ʱ��ɨ��һ��?0-9��ģ��
//������ɹ����������Ϊ����Ϊͣ���ˣ�����Ҫ����ԭ����״̬
//�ȴ������ˣ������ģ��ָ������ˡ�
BOOL RectIsNeedReConfig()
{
	int			iRepeat00CmdTimes;
	SIG_ENUM	emRst;
	int			iAddr;

	for(iAddr = 0; iAddr < 10; iAddr++)
	{
		iRepeat00CmdTimes = 0;

		while(iRepeat00CmdTimes < 2)
		{
			iRepeat00CmdTimes++;

			emRst = RtSampleCmd00(iAddr);

			/*if(CAN_SAMPLE_OK == emRst
				|| CAN_RECT_REALLOCATE == emRst)*/
			if(CAN_BOTH_RECT_FAIL != emRst)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}


//The 3000E3 rectifier will keep more than 30 seconds after AC Failure.
//It will lead to the controller send out the reconfig command after AC Failure, and clear rough data to 0.
//Add the function to avoid sending out reconfig command after AC Failure. Only send out the reconfig command when the bus has data.
BOOL RectIsNeedReConfigWhenACFail()
{
       int                 iRepeat00CmdTimes;
       SIG_ENUM    emRst;
       int                 iAddr;
       if(g_CanData.aRoughDataGroup[GROUP_RT_AC_FAIL].iValue == FALSE)
       {
       		//printf("RectIsNeedReConfigWhenACFail():g_CanData.aRoughDataGroup[GROUP_RT_AC_FAIL].iValue is FALSE~~~~~~~~~~~~~~~\n");
              return TRUE;
       }
       
       for(iAddr = 0; iAddr < 2; iAddr++)
       {
              iRepeat00CmdTimes = 0;

              while(iRepeat00CmdTimes < 2)
              {
                     iRepeat00CmdTimes++;

                     emRst = RtSampleCmd00(iAddr);

                     /*if(CAN_SAMPLE_OK == emRst
                            || CAN_RECT_REALLOCATE == emRst)*/
                     if(CAN_BOTH_RECT_FAIL != emRst)
                     {
                      	//printf("RectIsNeedReConfigWhenACFail():RtSampleCmd00 has a response~~~~~~~~~~~~~~~\n");
      
                            return TRUE;
                     }
              }
       }
       
       //printf("RectIsNeedReConfigWhenACFail():Return FALSE !!!!!!!!!!!!!!!!!~~~~~~~~~~~~~~~\n");

       return FALSE;
}



/*==========================================================================*
 * FUNCTION : RtCalcRatedCurr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-07 17:31
 *==========================================================================*/
static int RtCalcRatedCurr(void)
{
	int				i;
	SAMPLING_VALUE	RatedCurr;

	RatedCurr.iValue = CAN_SAMP_INVALID_VALUE;

	for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
	{
		if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
		{
			continue;
		}

		if(CAN_SAMP_INVALID_VALUE != g_CanData.aRoughDataRect[i][RECT_RATED_CURR].iValue)
		{
			RatedCurr.fValue = g_CanData.aRoughDataRect[i][RECT_RATED_CURR].fValue;
			break;
		}
	}

	return RatedCurr.iValue;
}


/*==========================================================================*
 * FUNCTION : RtIsAllNoResponse
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-25 10:55
 *==========================================================================*/
static BOOL RtIsAllNoResponse(void)
{
	int		i;
	int		iRectNum = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;
	BOOL	bRst = TRUE;

	for(i = 0; i < iRectNum; i++)
	{
		if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue 
			< RT_MAX_INTERRUPT_TIMES)
		{
			bRst = FALSE;
			break;
		}
	}

	if(bRst)
	{
		for(i = 0; i < iRectNum; i++)
		{
			g_CanData.aRoughDataRect[i][RECT_INTERRUPT_ST].iValue 
				 = RT_COMM_ALL_INTERRUPT_ST;
		}	
	}

	return bRst;
}

#define		RT_TYPE_NO_G1_55_1		3
#define		RT_TYPE_NO_G1_55_2		4
#define		RT_TYPE_NO_G1_55_3		5
#define		RT_TYPE_NO_G2_55		35
#define		RT_TYPE_NO_G2_60		39

/*==========================================================================*
 * FUNCTION : RtGetValidRatedCurr
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2009-07-29 11:12
 *==========================================================================*/
static void RtGetValidRatedCurr(void)
{
	int			i;
	int			iRectNum = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;
	SIG_ENUM	enumFullCap = GetEnumSigValue(RT_GROUP_EQUIPID, 
									SIG_TYPE_SETTING,
									RT_FULL_POWER_ENB_SIGID,
									"CAN_SAMP");
	float		fMinRatedCurr = 500.0;

	for(i = 0; i < iRectNum; i++)
	{
		if(CAN_SAMP_INVALID_VALUE 
			!= g_CanData.aRoughDataRect[i][RECT_RATED_CURR].iValue)
		{
			if(fMinRatedCurr 
				> g_CanData.aRoughDataRect[i][RECT_RATED_CURR].fValue)
			{
				fMinRatedCurr 
					= g_CanData.aRoughDataRect[i][RECT_RATED_CURR].fValue;
			}
		}
	}
	
	for(i = 0; i < iRectNum; i++)
	{
		int		iTypeNo = g_CanData.aRoughDataRect[i][RECT_TYPE_NO].iValue;
		float	fRatedCurr = g_CanData.aRoughDataRect[i][RECT_RATED_CURR].fValue;
		//In order to fix the MixHE issue, Changed by Marco Yang, 2011-6-1
		/*switch(iTypeNo)
		{
		case RT_TYPE_NO_G1_55_1:
		case RT_TYPE_NO_G1_55_2:
		case RT_TYPE_NO_G1_55_3:
			if(fMinRatedCurr < 55.0)
			{
				g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue
					= fMinRatedCurr;
			}
			else
			{
				g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue
					= fRatedCurr;
			
			}
			break;

		case RT_TYPE_NO_G2_55:
		case RT_TYPE_NO_G2_60:
			if((fMinRatedCurr < fRatedCurr) && (enumFullCap == RT_DEGRADATIVE))
			{
				g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue
					= fMinRatedCurr;
			}
			else
			{
				g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue
					= fRatedCurr;
			
			}
			break;

		default:
			g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue
				= fRatedCurr;

			break;
		}*/
		switch(enumFullCap)
		{
		case RT_DEGRADATIVE:
			g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue
				= fMinRatedCurr;
			break;
		default:
			g_CanData.aRoughDataRect[i][RECT_VALID_RATED_CURR].fValue
				= fRatedCurr;
			break;
		}
		//End:In order to fix the MixHE issue, Changed by Marco Yang, 2011-6-1
	}

	return;
}

/*==========================================================================*
* FUNCTION : RtSetGoupFeatureSig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : by IlockTeng                DATE: 2009-10-21 16:25
*==========================================================================*/
static void RtSetGoupFeatureSig()
{
	/******************************************************************
		����ģ������?0��������Ŀ���ǣ�
		�������ģ��ͨ���жϵ�ʱ����ֻ����ģ�飬�ᱨCAN���ϣ�
		��ô�ͻ������ã��������õ�ʱ������INITRoughData.
		�������ͻᵼ��RECT_RATED_VOLT�ź�Ϊ��Ч��ֵ�����¶����
		��Ϊ50A���ⲻ����Ҫ�Ľ����?
		����˼ά��ֻ�е�һ�θ�����ϵ磬��û�ɼ���ģ�飬�Ÿ�?
		���µĶ������Ĭ����ֵ50A��
	******************************************************************/
	if((g_CanData.aRoughDataRect[0][RECT_RATED_VOLT].iValue 
		== CAN_SAMP_INVALID_VALUE)
		&& (0 == g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue))
	{
		g_CanData.aRoughDataGroup[GROUP_RT_AC_RATED_VOLT].fValue = 220.0;
		g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue = 48.0;
		g_CanData.aRoughDataGroup[GROUP_RT_RATED_CURR].fValue = 50.0;
		g_CanData.aRoughDataGroup[GROUP_RT_DC_DC_TYPE].iValue = DC_TYPE_RETURN_DOUBLE;
		g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].uiValue = 0;
	}
	else
	{
		//ת�Ƶ�RECONFIG��ʱ�� 20ָ��ɼ��ɹ��Ž��и����?
		//���ͨ���жϣ���?20ָ��ɼ����ɹ����Ͳ��ḳֵ��������ԭ����ֵ��
		//g_CanData.aRoughDataGroup[GROUP_RT_AC_RATED_VOLT].fValue 
		//	= g_CanData.aRoughDataRect[0][RECT_AC_RATED_VOLT].fValue;
		//g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue 
		//	= g_CanData.aRoughDataRect[0][RECT_RATED_VOLT].fValue;
		//g_CanData.aRoughDataGroup[GROUP_RT_RATED_CURR].fValue 
		//	= g_CanData.aRoughDataRect[0][RECT_RATED_CURR].fValue;
		//g_CanData.aRoughDataGroup[GROUP_RT_DC_DC_TYPE].iValue 
		//	= g_CanData.aRoughDataRect[0][RECT_DC_DC_TYPE].iValue;
		//g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue 
		//	= g_CanData.aRoughDataRect[0][RECT_1_OR_3_PHASE].iValue;
	}
}

#define	PHASE_AB_NO		0
#define	PHASE_BC_NO		1
#define	PHASE_CA_NO		2

#define	AC_RESTORE_HYSTERESIS	4
/*==========================================================================*
 * FUNCTION : RtNonSampSigHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-07 17:23
 *==========================================================================*/
static void RtNonSampSigHandle(void)
{
	static	int		s_iAcRestoreCounter = 0;
	static  int		s_iDelayAcFailTimes = 0;//Need a little hysteresis first running
	
	RtSetGoupFeatureSig();
#define  RT_AC_FAIL_DELAY  5
	
	if (s_iDelayAcFailTimes < RT_AC_FAIL_DELAY)
	{
		s_iDelayAcFailTimes++;// Reconfig Query  Query  Query. Enough!
	}

	g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue 
		= RtIsAllNoResponse();

	RtCalcAcVolt();

	//need a little hysteresis when AC restored
	if(RtJudgeAcFail())
	{
		//
		g_CanData.aRoughDataGroup[GROUP_RT_AC_FAIL].iValue = FALSE;
		s_iAcRestoreCounter = 0;

		if (s_iDelayAcFailTimes >= RT_AC_FAIL_DELAY)
		{
			//ԭ���룬���ڼӸ��������ϵ��Ҫ��?120��QUERY��ʱ��Ż�澯
			g_CanData.aRoughDataGroup[GROUP_RT_AC_FAIL].iValue = TRUE;
		}
	}
	else
	{
		if(s_iAcRestoreCounter < AC_RESTORE_HYSTERESIS)
		{
			s_iAcRestoreCounter++;
			g_CanData.aRoughDataGroup[GROUP_RT_AC_FAIL].iValue = TRUE;
			
			TRACE("\n  if(RtJudgeAcFail()  else (s_iAcRestoreCounter < AC_RESTORE_HYSTERESIS) \n");
				
			if (s_iDelayAcFailTimes < RT_AC_FAIL_DELAY)
			{
				//���ﲻ����������ǼӸ��ж��ݴ�?
				g_CanData.aRoughDataGroup[GROUP_RT_AC_FAIL].iValue = FALSE;
			}
		}
		else
		{
			g_CanData.aRoughDataGroup[GROUP_RT_AC_FAIL].iValue = FALSE;		
		}
	}

	RtDefaultVoltParam(FALSE);

	RtGetValidRatedCurr();

	RtSetGoupFeatureSig();
	return;
}

static BOOL	RtAnalyseSerialNo(int iAddr)
{
	int					i;
	UINT				uiType;

	//struct	tagRECT_TYPE_INFO
	//{
	//	UINT		uiTypeNo;
	//	float		fRatedVolt;
	//	float		fRatedCurr;
	//	int			iDcDcType;
	//	int			iAcInputType;
	//	float		fAcRatedVolt;
	//	float		fPower;
	//	int			iEfficiencyNo;
	//};
	//typedef struct tagRECT_TYPE_INFO RECT_TYPE_INFO;

	static RECT_TYPE_INFO		s_aRectType[] =
	{
	//	Type	Rated_V		Rated_C	DCDC Type				AC_TYPE				Rated_AC_V	Rated_P	
		{0,		48.0,		50.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	2900.0,	RT_EFFICIENCY_LT93,},
		{1,		48.0,		50.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	2900.0,	RT_EFFICIENCY_LT93,},
		{2,		48.0,		50.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	2900.0,	RT_EFFICIENCY_LT93,},
		{3,		48.0,		55.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	3200.0,	RT_EFFICIENCY_LT93,},
		{4,		48.0,		55.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	3200.0,	RT_EFFICIENCY_LT93,},
		{5,		48.0,		55.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	3200.0,	RT_EFFICIENCY_LT93,},
		{6,		48.0,		35.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	1900.0,	RT_EFFICIENCY_LT93,},
		{7,		48.0,		45.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	2000.0,	RT_EFFICIENCY_LT93,},
		{8,		48.0,		50.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	2900.0,	RT_EFFICIENCY_LT93,},	//tec
		{9,		24.0,		75.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	2200.0,	RT_EFFICIENCY_LT93,},
		{10,	48.0,		15.0,	DC_TYPE_RETURN_SINGLE,	AC_INPUT_1_PHASE,	220.0,	800.0,	RT_EFFICIENCY_LT93,},
		{11,	48.0,		7.5,	DC_TYPE_RETURN_SINGLE,	AC_INPUT_1_PHASE,	220.0,	400.0,	RT_EFFICIENCY_LT93,},
		{12,	24.0,		88.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	2500.0,	RT_EFFICIENCY_LT93,},
		{13,	48.0,		44.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_1_PHASE,	220.0,	2500.0,	RT_EFFICIENCY_LT93,},
									
		{15,	48.0,		30.0,	DC_TYPE_RETURN_SMALL,	AC_INPUT_1_PHASE,	220.0,	1740.0,	RT_EFFICIENCY_LT93,},
		{16,	48.0,		30.0,	DC_TYPE_RETURN_SMALL,	AC_INPUT_1_PHASE,	220.0,	1700.0,	RT_EFFICIENCY_LT93,},
		{17,	48.0,		35.0,	DC_TYPE_RETURN_SMALL,	AC_INPUT_1_PHASE,	220.0,	1700.0,	RT_EFFICIENCY_LT93,},
									
		{20,	48.0,		100.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_3_PHASE,	380.0,	5800.0,	RT_EFFICIENCY_LT93,},
		{21,	48.0,		100.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_3_PHASE,	380.0,	5800.0,	RT_EFFICIENCY_LT93,},								
		{22,	48.0,		200.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_3_PHASE,	380.0,	11600.0,RT_EFFICIENCY_LT93,},
		
		//Support R48-6000N, which only change the type number, others are same with R48-5800									
		{24,	48.0,		100.0,	DC_TYPE_RETURN_DOUBLE,	AC_INPUT_3_PHASE,	380.0,	5800.0,	RT_EFFICIENCY_LT93,},

		{INVALID_RECT_TYPE, 0.0, 0.0, 0, 0, 0.0, 0.0, 	RT_EFFICIENCY_LT93,},
	};

	int iHiSn = g_CanData.aRoughDataRect[iAddr][RECT_HIGH_SN_ROUGH].iValue;
	int iLowSn = g_CanData.aRoughDataRect[iAddr][RECT_SERIEL_NO].iValue;
	UINT uiHiSn = g_CanData.aRoughDataRect[iAddr][RECT_HIGH_SN_ROUGH].uiValue;
	UINT uiLowSn = g_CanData.aRoughDataRect[iAddr][RECT_SERIEL_NO].uiValue;

	if((CAN_SAMP_INVALID_VALUE != iHiSn)
		&& (CAN_SAMP_INVALID_VALUE != iLowSn))
	{
		uiType = ((uiHiSn >> 4) & 0x000f) * 32 
			+ ((uiLowSn >> 29) & 0x0001) * 16 
			+ ((uiLowSn >> 20) & 0x000f);

		printf("Address %d RtAnalyseSerialNo %d\n", iAddr, uiType);
		g_CanData.aRoughDataRect[iAddr][RECT_TYPE_NO].iValue = uiType;

		i = 0;
		while(s_aRectType[i].uiTypeNo != INVALID_RECT_TYPE)
		{
			if(uiType == s_aRectType[i].uiTypeNo)
			{
				g_CanData.aRoughDataRect[iAddr][RECT_RATED_VOLT].fValue
					= s_aRectType[i].fRatedVolt;
				g_CanData.aRoughDataRect[iAddr][RECT_RATED_CURR].fValue
					= s_aRectType[i].fRatedCurr;
				g_CanData.aRoughDataRect[iAddr][RECT_DC_DC_TYPE].iValue
					= s_aRectType[i].iDcDcType;
				g_CanData.aRoughDataRect[iAddr][RECT_1_OR_3_PHASE].iValue
					= s_aRectType[i].iAcInputType;
				g_CanData.aRoughDataRect[iAddr][RECT_AC_RATED_VOLT].fValue
					= s_aRectType[i].fAcRatedVolt;
				g_CanData.aRoughDataRect[iAddr][RECT_EFFICIENCY_NO].iValue
					= s_aRectType[i].iEfficiencyNo;
				
				return TRUE;
			}
			i++;
		}
	}

	return FALSE;
}



/*==========================================================================*
 * FUNCTION : RtAnalyseFeature
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE*  pbyBuf : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
static void RtAnalyseFeature(int iAddr)
{
	UINT		uiFeature;
	int			iDcTypeSamp;
	int			iDcTypeReturn;
	int			iAcPhaseNum;
	int			iAcRatedVoltNo;
	int			iRatedCurrNo;
	int			iRatedVoltNo;
	int			iEfficiencyNo;

	float		fMinRatedCurr = 1000.0;


	if(g_CanData.aRoughDataRect[iAddr][RECT_FEATURE].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataRect[iAddr][RECT_RATED_VOLT].fValue
			= 48.0;
		g_CanData.aRoughDataRect[iAddr][RECT_RATED_CURR].fValue
			= 50.0;
		g_CanData.aRoughDataRect[iAddr][RECT_DC_DC_TYPE].iValue
			= DC_TYPE_RETURN_DOUBLE;
		g_CanData.aRoughDataRect[iAddr][RECT_1_OR_3_PHASE].iValue
			= AC_INPUT_1_PHASE;
		g_CanData.aRoughDataRect[iAddr][RECT_AC_RATED_VOLT].fValue
			= 220.0;
		g_CanData.aRoughDataRect[iAddr][RECT_EFFICIENCY_NO].iValue
			= RT_EFFICIENCY_LT93;

		return;
	}

	uiFeature = g_CanData.aRoughDataRect[iAddr][RECT_FEATURE].uiValue;

	//DC input type of rectifier
	iDcTypeSamp = uiFeature >> 30;

	if(iDcTypeSamp == DC_TYPE_SAMP_SINGLE)
	{
		iDcTypeReturn = DC_TYPE_RETURN_SINGLE;
	}
	else if(iDcTypeSamp == DC_TYPE_SAMP_DOUBLE)
	{
		iDcTypeReturn = DC_TYPE_RETURN_DOUBLE;
	}
	else if(iDcTypeSamp == DC_TYPE_SAMP_SMALL)
	{
		iDcTypeReturn = DC_TYPE_RETURN_SMALL;
	}
	g_CanData.aRoughDataRect[iAddr][RECT_DC_DC_TYPE].iValue = iDcTypeReturn;

	//AC Input sigle or three phase
	iAcPhaseNum = (uiFeature & 0x30000000) >> 28;
	printf("Address %d RtAnalyseFeature = %d\n",iAddr, iAcPhaseNum);
	if(iAcPhaseNum == 0x03) //not written
	{
		iAcPhaseNum = 0;
	}
	g_CanData.aRoughDataRect[iAddr][RECT_1_OR_3_PHASE].uiValue = iAcPhaseNum;

	//AC input rated voltage
	iAcRatedVoltNo = (uiFeature & 0x0c000000) >> 26;
	if(iAcRatedVoltNo == 0x03) //not written
	{
		iAcRatedVoltNo = 0;
	}

	if(iAcRatedVoltNo == 0)
	{
		g_CanData.aRoughDataRect[iAddr][RECT_AC_RATED_VOLT].fValue = 220.0;
	}
	else if(iAcRatedVoltNo == 1)
	{
		g_CanData.aRoughDataRect[iAddr][RECT_AC_RATED_VOLT].fValue = 110.0;
	}
	else
	{
		g_CanData.aRoughDataRect[iAddr][RECT_AC_RATED_VOLT].fValue = 220.0;			
	}

	//Rated Voltage
	iRatedVoltNo = (uiFeature & 0x0000f000) >> 12;
	if(iRatedVoltNo == 0x0f)
	{
		iRatedVoltNo = 0;	//means -48V
	}
	if(iRatedVoltNo == 0)
	{
		g_CanData.aRoughDataRect[iAddr][RECT_RATED_VOLT].fValue = 48.0;
	}
	else if(iRatedVoltNo == 1)
	{
		g_CanData.aRoughDataRect[iAddr][RECT_RATED_VOLT].fValue = 24.0;
	}
	else
	{
		g_CanData.aRoughDataRect[iAddr][RECT_RATED_VOLT].fValue = 48.0;			
	}

	//Rated Current = iRatedCurrNo * 0.5A
	iRatedCurrNo = (uiFeature & 0x03ff0000) >> 16;
	if(iRatedCurrNo == 0x03ff)
	{
		iRatedCurrNo = 100;	//means 50A
	}

	if (fMinRatedCurr > (0.5 * (float)iRatedCurrNo))
	{
		fMinRatedCurr = 0.5 * (float)iRatedCurrNo;
	}
	g_CanData.aRoughDataRect[iAddr][RECT_RATED_CURR].fValue = fMinRatedCurr;

	iEfficiencyNo = (uiFeature & 0x00000E00) >> 9;
	if(iEfficiencyNo == 0x0007)
	{
		iEfficiencyNo = 0;
	}
	g_CanData.aRoughDataRect[iAddr][RECT_EFFICIENCY_NO].iValue = iEfficiencyNo;

	return;

}


//static void RtGetFeature(void)
//{
//	int			i;
//	
//	for(i = 0; i < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; i++)
//	{
//		if(!RtAnalyseSerialNo(i))
//		{
//			RtAnalyseFeature(i);
//		}
//	}
//
//	if(g_CanData.aRoughDataRect[0][RECT_RATED_VOLT].iValue 
//		== CAN_SAMP_INVALID_VALUE)
//	{
//		g_CanData.aRoughDataGroup[GROUP_RT_AC_RATED_VOLT].fValue = 220.0;
//		g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue = 48.0;
//		g_CanData.aRoughDataGroup[GROUP_RT_RATED_CURR].fValue = 50.0;
//		g_CanData.aRoughDataGroup[GROUP_RT_DC_DC_TYPE].iValue = DC_TYPE_RETURN_DOUBLE;
//		g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].uiValue = 0;
//	}
//	else
//	{
//		g_CanData.aRoughDataGroup[GROUP_RT_AC_RATED_VOLT].fValue 
//			= g_CanData.aRoughDataRect[0][RECT_AC_RATED_VOLT].fValue;
//		g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue 
//			= g_CanData.aRoughDataRect[0][RECT_RATED_VOLT].fValue;
//		g_CanData.aRoughDataGroup[GROUP_RT_RATED_CURR].fValue 
//			= g_CanData.aRoughDataRect[0][RECT_RATED_CURR].fValue;
//		g_CanData.aRoughDataGroup[GROUP_RT_DC_DC_TYPE].iValue 
//			= g_CanData.aRoughDataRect[0][RECT_DC_DC_TYPE].fValue;
//		g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].uiValue 
//			= g_CanData.aRoughDataRect[0][RECT_1_OR_3_PHASE].fValue;
//	}
//
//	return;
//}


#define	MAX_SAMPLE_TIMES	5
/*==========================================================================*
 * FUNCTION : RT_Reconfig
 * PURPOSE  : scan all rectifiers, get rectifiers connecting information and 
 *            save the information into the flash
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-06-24 15:59
 *==========================================================================*/
void RT_Reconfig(void)
{
	int			iAddr,iAddrAgain,iSAddr, iCAN1Num=0, iCAN2Num=0;
	int			iCAN1St, iCAN2St;
	int		iRepeatT1 = 0, iRepeatT2 = 0;
	SIG_ENUM	emRst;
	static	int	siMaxReallocTimes = 0;

	g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1 = MAX_RECTNUM_CAN;
	g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2 = MAX_RECTNUM_CAN;

	fDeltaRectV = 0;
	iCAN1St = CFGGOING;
	iCAN2St = CFGGOING;
	//TRACE("************CAN DEBUG: Rectifiers is reconfiging!\n");

#ifdef GC_SUPPORT_MPPT
	int	iMaxRect = RtGetMaxNumber();
#endif
	RT_InitRoughValue();
//#ifdef GC_SUPPORT_MPPT
//	for(iAddr = 0; iAddr < iMaxRect; iAddr++)
//#else
	for(iAddr = 0; iAddr < MAX_RECTNUM_CAN; iAddr++)
//#endif
	{
		int		iRepeat00CmdTimes = 0;

		if(iRepeatT1 < MAX_SAMPLE_TIMES)
			iRepeatT1 = 0;

		if(iRepeatT2 < MAX_SAMPLE_TIMES)
			iRepeatT2 = 0;


		while(iRepeat00CmdTimes < MAX_SAMPLE_TIMES)
		{
			emRst = RtSampleCmd00(iAddr);

			/*TRACE("************CAN DEBUG: Sampled %d rectifier, result is %d!\n",
				iAddr,
				emRst);*/

			/*if((CAN_SAMPLE_OK == emRst)
				|| ((iRepeatT1 == MAX_SAMPLE_TIMES) && (emRst == CAN1_RECT_FAIL))
				|| ((iRepeatT2 == MAX_SAMPLE_TIMES) && (emRst == CAN2_RECT_FAIL)))*/
			if(emRst == CAN_SAMPLE_OK)
			{
				break;
			}
			else if((iRepeatT1 == MAX_SAMPLE_TIMES) && (emRst == CAN1_RECT_FAIL))
			{
				iCAN1St = CFGOVER;
				break;
			}
			else if((iRepeatT2 == MAX_SAMPLE_TIMES) && (emRst == CAN2_RECT_FAIL))
			{
				iCAN2St = CFGOVER;
				break;
			}

			if(CAN_RECT_REALLOCATE == emRst)
			{
				return;
			}

			//if(CAN_SAMPLE_FAIL == emRst)
			if((emRst == CAN1_RECT_FAIL)
				|| (emRst == CAN2_RECT_FAIL)
				|| (emRst == CAN_BOTH_RECT_FAIL))
			{
				iRepeat00CmdTimes++;
				Sleep(150);
				if(emRst == CAN1_RECT_FAIL)
					iRepeatT1++;
				else if(emRst == CAN2_RECT_FAIL)
					iRepeatT2++;
			}
		}

		
		if(iAddr == 0 && !isnan(g_CanData.aRoughDataRect[iAddr][RECT_DC_VOLTAGE].fValue))//Special for AP
		{
			SetFloatSigValue(1, 
					SIG_TYPE_SAMPLING,
					1,
					g_CanData.aRoughDataRect[iAddr][RECT_DC_VOLTAGE].fValue,
					"CAN_SAMP");
		}
		//if(iRepeat00CmdTimes >= MAX_SAMPLE_TIMES)
		if(emRst == CAN_BOTH_RECT_FAIL)
		{
			//printf("Rst %d=CAN_BOTH_RECT_FAIL\n", iAddr);
			break;
		}
		else if(emRst == CAN1_RECT_FAIL)
		{
			if(iCAN2St == CFGGOING)
			{
				iCAN2Num++;
				RectCAN2[iAddr][RECT_EXIST_ST].iValue = RT_EQUIP_EXISTENT;
				//printf("Rst=CAN1_RECT_FAIL; CAN2Num=%d\n",iCAN2Num);
			}
			else
			{
				RT_TriggerAllocation();
				return;
			}
		}
		else if(emRst == CAN2_RECT_FAIL)
		{
			if(iCAN1St == CFGGOING)
			{
				iCAN1Num++;
				g_CanData.CanFlashData.aRectifierInfo[iAddr].bExistence = TRUE;
				g_CanData.aRoughDataRect[iAddr][RECT_EXIST_ST].iValue = RT_EQUIP_EXISTENT;
				//printf("Rst=CAN2_RECT_FAIL\n");
			}
			else
			{
				RT_TriggerAllocation();
				return;
			}
		}
		else if(emRst == CAN_SAMPLE_OK)
		{
			if(iCAN1St == CFGGOING)
			{
				iCAN1Num++;
				g_CanData.CanFlashData.aRectifierInfo[iAddr].bExistence = TRUE;
				g_CanData.aRoughDataRect[iAddr][RECT_EXIST_ST].iValue = RT_EQUIP_EXISTENT;
			}
			else
			{
				RT_TriggerAllocation();
				return;
			}

			if(iCAN2St == CFGGOING)
			{
				iCAN2Num++;
				RectCAN2[iAddr][RECT_EXIST_ST].iValue = RT_EQUIP_EXISTENT;
				//printf("Rst %d=CAN_SAMPLE_OK\n",iAddr);
			}
			else
			{
				RT_TriggerAllocation();
				return;
			}
		}

		//g_CanData.CanFlashData.aRectifierInfo[iAddr].bExistence = TRUE;

		//g_CanData.aRoughDataRect[iAddr][RECT_EXIST_ST].iValue = RT_EQUIP_EXISTENT;


		/*while(iRepeat20CmdTimes < MAX_SAMPLE_TIMES)
		{
			emRst = RtSampleCmd20(iAddr);

			if(CAN_SAMPLE_OK == emRst)
			{
				if(!RtAnalyseSerialNo(iAddr))
				{
					RtAnalyseFeature(iAddr);
					//RtGetFeature();
				}
				break;
			}

			if(CAN_RECT_REALLOCATE == emRst)
			{
				return;
			}

			if(CAN_SAMPLE_FAIL == emRst)
			{
				iRepeat20CmdTimes++;
				Sleep(150);
			}
		}*/
	}


	if(iCAN1Num > iCAN2Num)
		iAddr = iCAN1Num;
	else
		iAddr = iCAN2Num;

	//������Ź����У��м�ģ�鱻�ε������
	for (iAddrAgain = iAddr; iAddrAgain < iAddr + 3; iAddrAgain++)
	{
		emRst = RtSampleCmd00(iAddrAgain);

		if((CAN_SAMPLE_OK == emRst)
			|| (emRst == CAN1_RECT_FAIL)
			|| (emRst == CAN2_RECT_FAIL))
		{
			siMaxReallocTimes++;

			if (siMaxReallocTimes < 3)
			{			
				RT_TriggerAllocation();
				return;
			}
			else
			{
				break;
			}

		}

		if(CAN_RECT_REALLOCATE == emRst)
		{
			//��������������ǲ���Ҫ�ģ���������˵��������?
			//Delete it , will go into the dead cycle.by YangGuoxin 2014/3/14
			return;
		}

		//if(CAN_SAMPLE_FAIL == emRst)
		if(emRst == CAN_BOTH_RECT_FAIL)
		{
			Sleep(20);
		}
	}


	siMaxReallocTimes = 0;
	g_CanData.CanCommInfo.RectCommInfo.iCommRectNum = iCAN1Num + iCAN2Num;
	g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1 = iCAN1Num;
	g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2 = iCAN2Num;

	for(iAddrAgain = 0; iAddrAgain < iCAN2Num; iAddrAgain++)
	{
		if(RectCAN2[iAddrAgain][RECT_EXIST_ST].iValue == RT_EQUIP_EXISTENT)
			g_CanData.CanFlashData.aRectifierInfo[iAddrAgain + iCAN1Num].bExistence = TRUE;
	}

	memcpy(g_CanData.aRoughDataRect[iCAN1Num], RectCAN2[0], sizeof(RectCAN2));
	//printf("Addr=%d;CAN1Num = %d; CAN2Num = %d\n", iAddr, iCAN1Num, iCAN2Num);

	for(iAddrAgain = 0; iAddrAgain < iAddr; iAddrAgain++)
	{
		int		iRepeat20CmdTimes = 0;
		iSAddr = iAddrAgain + iCAN1Num;
		while(iRepeat20CmdTimes < MAX_SAMPLE_TIMES)
		{
			emRst = RtSampleCmd20(iAddrAgain);

			if(emRst == CAN_SAMPLE_OK)
			{
				if(!RtAnalyseSerialNo(iAddrAgain))
				{
					RtAnalyseFeature(iAddrAgain);
					//RtGetFeature();
				}
				if(!RtAnalyseSerialNo(iSAddr))
				{
					RtAnalyseFeature(iSAddr);
				}
				break;
			}
			if(emRst == CAN2_RECT_FAIL)
			{
				if(!RtAnalyseSerialNo(iAddrAgain))
				{
					RtAnalyseFeature(iAddrAgain);
				}
				if(iAddrAgain < iCAN2Num)
					iRepeat20CmdTimes++;
				else
					break;
			}
			else if(emRst == CAN1_RECT_FAIL)
			{
				if(!RtAnalyseSerialNo(iSAddr))
				{
					RtAnalyseFeature(iSAddr);
				}
				if(iAddrAgain < iCAN1Num)
					iRepeat20CmdTimes++;
				else
					break;
			}
			else if(emRst == CAN_BOTH_RECT_FAIL)
			{
				Sleep(150);
				iRepeat20CmdTimes++;
			}

			if(CAN_RECT_REALLOCATE == emRst)
			{
				return;
			}

			/*if(CAN_SAMPLE_FAIL == emRst)
			{
				iRepeat20CmdTimes++;
				Sleep(150);
			}*/
		}
	}
	//printf("RectNum=%d", g_CanData.CanCommInfo.RectCommInfo.iCommRectNum);
	if(iAddr)
	{
		g_CanData.aRoughDataGroup[GROUP_RT_AC_RATED_VOLT].fValue 
			= g_CanData.aRoughDataRect[0][RECT_AC_RATED_VOLT].fValue;
		g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue 
			= g_CanData.aRoughDataRect[0][RECT_RATED_VOLT].fValue;
		g_CanData.aRoughDataGroup[GROUP_RT_RATED_CURR].fValue 
			= g_CanData.aRoughDataRect[0][RECT_RATED_CURR].fValue;
		g_CanData.aRoughDataGroup[GROUP_RT_DC_DC_TYPE].iValue 
			= g_CanData.aRoughDataRect[0][RECT_DC_DC_TYPE].iValue;
		g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue 
			= g_CanData.aRoughDataRect[0][RECT_1_OR_3_PHASE].iValue;
	}

	//printf("Rect_Reconfig:g_CanData.CanCommInfo.RectCommInfo.iCommRectNum =%d, iCAN1Num=%d,iCAN2Num=%d !!!!!!!!!!!!!\n ",g_CanData.CanCommInfo.RectCommInfo.iCommRectNum,iCAN1Num,iCAN2Num);	
	if(g_CanData.CanCommInfo.RectCommInfo.iCommRectNum)
	{
		//g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue = iAddr;
		g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;

		//RT_RefreshFlashInfo();
	}

	for(iAddr = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; iAddr < MAX_NUM_RECT; iAddr++)
	{
		g_CanData.aRoughDataRect[iAddr][RECT_EXIST_ST].iValue = RT_EQUIP_NOT_EXISTENT;
	}

	//RtCalcEnergyInit();
	int iMaxAddr = (iCAN1Num > iCAN2Num) ? iCAN1Num : iCAN2Num;
	printf("0x30 reconfig   %d\n", iMaxAddr);
	for (iAddr = 0; iAddr < iMaxAddr; iAddr++)
	{
		if (CAN_RECT_REALLOCATE == RtSampleCmd30(iAddr))
		{
			printf("0x30 reconfig CAN_RECT_REALLOCATE iAddr %d \n", iAddr);
			break;
		}
		//RtCalcEnergyReconfig(iAddr);
	}

	RtNonSampSigHandle();

	RtUnifyParam();
	Sleep(100);

	RtDefaultVoltParam(TRUE);
	Sleep(100);

	return;
}

/*==========================================================================*
 * FUNCTION : SetRectRunTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr  : 
 *            float  fParam : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:30
 *==========================================================================*/
static void SetRectRunTime(int iAddr, float fParam)
{
	BYTE			abyVal[4];

	CAN_FloatToString(fParam , abyVal);
	if(iAddr < g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1)
	{
		PackAndSendRtCAN1(MSG_TYPE_RQST_SETTINGS,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					0,
					RT_VAL_TYPE_W_RUN_TIME,
					abyVal);
	}
	else
	{
		PackAndSendRtCAN2(MSG_TYPE_RQST_SETTINGS,
					((UINT)iAddr - g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1),
					CAN_CMD_TYPE_P2P,
					0,
					RT_VAL_TYPE_W_RUN_TIME,
					abyVal);
	}
	return;
}



/*==========================================================================*
 * FUNCTION : RefreshRectRunTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:30
 *==========================================================================*/
static void RefreshRectRunTime(void)
{
	int					i;
	time_t				tNow;   
	struct tm			tmNow; 

   
	tNow = time(NULL);

	gmtime_r(&tNow, &tmNow);

	if(g_CanData.CanCommInfo.RectCommInfo.bNeedRefreshRuntime)
	{
		if(tmNow.tm_min == 59)
		{
			for(i = 0; i < MAX_NUM_RECT; i++)
			{
				if(g_CanData.aRoughDataRect[i][RECT_TOTAL_RUN_TIME].iValue
					== CAN_SAMP_INVALID_VALUE)
				{
					//Do nothing until a sampling
					continue;
				}
				
				if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue
						< RT_MAX_INTERRUPT_TIMES)
				{
					if(RT_DC_ON 
						== g_CanData.aRoughDataRect[i][RECT_DC_ON_OFF_STATUS].uiValue)
					{
						g_CanData.aRoughDataRect[i][RECT_TOTAL_RUN_TIME].uiValue++;
						SetRectRunTime(i, 
							(float)(g_CanData.aRoughDataRect[i][RECT_TOTAL_RUN_TIME].uiValue));
						Sleep(10);
					}
				 }
			 }
			 g_CanData.CanCommInfo.RectCommInfo.bNeedRefreshRuntime = FALSE;
		 }
	}
	else
	{
		if(tmNow.tm_min == 1)
		{
			g_CanData.CanCommInfo.RectCommInfo.bNeedRefreshRuntime = TRUE;
		}
	}

	return;
}


/*==========================================================================*
 * FUNCTION : NeedHandleRtPosPhase
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
static BOOL NeedHandleRtPosPhase(void)
{
	if(GetDwordSigValue(RT_GROUP_EQUIPID, 
						SIG_TYPE_SETTING,
						RT_CONFIRM_POS_PHASE_SIGID,
						"CAN_SAMP") > 0)
	{
		SetDwordSigValue(RT_GROUP_EQUIPID,
						SIG_TYPE_SETTING,
						RT_CONFIRM_POS_PHASE_SIGID,
						0,
						"CAN_SAMP");
		return TRUE;
	}

	return FALSE;
}



/*==========================================================================*
 * FUNCTION : RtPosUniquenessHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-10-07 14:42
 *==========================================================================*/
static void RtPosUniquenessHandle(void)
{
	int	i, j;
	int	iRectNum = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;

	if(!iRectNum)
	{
		return;
	}

	for(i = 1; i < iRectNum; i++)
	{
		int iPos = g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue;
		for(j = 0; j < i; j++)
		{	
			if(iPos == g_CanData.aRoughDataRect[j][RECT_POSITION_NO].iValue)
			{
				g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue
					= GetOneRestRectPosNo();
				g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo 
					= g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue;
			}
		}
	}

	return;
}




/*==========================================================================*
 * FUNCTION : HandleRtPosAndPhase
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
//static void HandleRtPosAndPhase(void)
//{
//	int			i, j;
//	int			iRectNum = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;
//
//	int			iSeqNo;
//
//	//Ranged by sequence No.
//	RECT_POS_ADDR	RectPosAddr[MAX_NUM_RECT];
//
//	for(i = 0; i < iRectNum; i++)	//by sequence No.
//	{
//		if(i < MAX_NUM1_RECT)
//		{
//			RectPosAddr[i].iPositionNo = GetDwordSigValue(RT_START_EQUIP_ID + i,
//											SIG_TYPE_SETTING,
//											RT_POS_SET_SIG_ID,
//											"CAN_SAMP");
//			RectPosAddr[i].iPhaseNo = GetDwordSigValue(RT_START_EQUIP_ID + i,
//											SIG_TYPE_SETTING,
//											RT_PHASE_SET_SIG_ID,
//											"CAN_SAMP");
//		}
//		else
//		{
//			RectPosAddr[i].iPositionNo = GetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
//											SIG_TYPE_SETTING,
//											RT_POS_SET_SIG_ID,
//											"CAN_SAMP");
//			RectPosAddr[i].iPhaseNo = GetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
//											SIG_TYPE_SETTING,
//											RT_PHASE_SET_SIG_ID,
//											"CAN_SAMP");
//		}
//	}
//
//	//Ranged by address
//	for(i = 0; i < iRectNum; i++)
//	{
//		iSeqNo = g_CanData.aRoughDataRect[i][RECT_SEQ_NO].iValue;
//
//		RectPosAddr[iSeqNo].iAddr = i;
//
//		//Assign position No.
//		g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue
//				= RectPosAddr[iSeqNo].iPositionNo;
//		g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo
//				= RectPosAddr[iSeqNo].iPositionNo;
//	
//		//Assign AC phase No.
//		g_CanData.aRoughDataRect[i][RECT_PHASE_NO].iValue
//				= RectPosAddr[iSeqNo].iPhaseNo;
//		g_CanData.CanFlashData.aRectifierInfo[i].iAcPhase 
//				= RectPosAddr[iSeqNo].iPhaseNo;
//	
//		//Erase sequence No.
//		g_CanData.aRoughDataRect[i][RECT_SEQ_NO].iValue 
//				= CAN_SAMP_INVALID_VALUE;
//		g_CanData.CanFlashData.aRectifierInfo[i].iSeqNo 
//				= CAN_SAMP_INVALID_VALUE;
//
//	}
//	
//	RtPosUniquenessHandle();
//
//    //Re-range by position No.
//	if(iRectNum)
//	{
//		//effervescing sort
//		for (i = 0; i < (iRectNum - 1); i++)
//		{
//			for (j = 0; j < (iRectNum - 1 - i); j++)
//			{
//				if(RectPosAddr[j].iPositionNo > RectPosAddr[j + 1].iPositionNo)
//				{
//					RECT_POS_ADDR		PosAddr;
//
//					PosAddr.iAddr = RectPosAddr[j + 1].iAddr;
//					PosAddr.iPositionNo = RectPosAddr[j + 1].iPositionNo;
//					PosAddr.iPhaseNo = RectPosAddr[j + 1].iPhaseNo;
//
//					RectPosAddr[j + 1].iPositionNo = RectPosAddr[j].iPositionNo;
//					RectPosAddr[j + 1].iPhaseNo = RectPosAddr[j].iPhaseNo;
//					RectPosAddr[j + 1].iAddr = RectPosAddr[j].iAddr;
//
//					RectPosAddr[j].iPositionNo = PosAddr.iPositionNo;
//					RectPosAddr[j].iPhaseNo = PosAddr.iPhaseNo;
//					RectPosAddr[j].iAddr = PosAddr.iAddr;
//				}
//			}
//		}
//	}
//
//
//	//Assign sequence No. to every rectifier
//	for(i = 0; i < iRectNum; i++)
//	{
//		int iAddr = RectPosAddr[i].iAddr;
//
//		if(i < MAX_NUM1_RECT)
//		{
//			g_CanData.aRoughDataRect[iAddr][RECT_SEQ_NO].iValue = i;
//			g_CanData.CanFlashData.aRectifierInfo[iAddr].iSeqNo = i;
//
//			SetDwordSigValue(RT_START_EQUIP_ID + i,
//						SIG_TYPE_SETTING,
//						RT_POS_SET_SIG_ID,
//						g_CanData.aRoughDataRect[iAddr][RECT_POSITION_NO].iValue,
//						"CAN_SAMP");
//			SetDwordSigValue(RT_START_EQUIP_ID + i,
//						SIG_TYPE_SETTING,
//						RT_PHASE_SET_SIG_ID,
//						g_CanData.aRoughDataRect[iAddr][RECT_PHASE_NO].iValue,
//						"CAN_SAMP");
//		}
//		else
//		{
//			g_CanData.aRoughDataRect[iAddr][RECT_SEQ_NO].iValue = i;
//			g_CanData.CanFlashData.aRectifierInfo[iAddr].iSeqNo = i;
//
//			SetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
//						SIG_TYPE_SETTING,
//						RT_POS_SET_SIG_ID,
//						g_CanData.aRoughDataRect[iAddr][RECT_POSITION_NO].iValue,
//						"CAN_SAMP");
//			SetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
//						SIG_TYPE_SETTING,
//						RT_PHASE_SET_SIG_ID,
//						g_CanData.aRoughDataRect[iAddr][RECT_PHASE_NO].iValue,
//						"CAN_SAMP");
//		}
//	}
////
//	CAN_WriteFlashData(&(g_CanData.CanFlashData));
//
//	return;
//}


//added by Jimmy Wu 2013.10.27 for reason that the old arthemetis is not quite right
static void HandleRtPosAndPhase_New(BOOL bReadConfirm)
{
	int			i, j;
	int			iRectNum = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;
	if(iRectNum <= 0)
	{
		return;
	}
	//Ranged by sequence No.
	RECT_POS_ADDR	Flash_Map[MAX_NUM_RECT]; //�洢��Flash��Map
	RECT_POS_ADDR	sFinal_Map[MAX_NUM_RECT]; //������õ�Map
	//��ʼ��
	static BOOL bFirstTime = TRUE;
	int iFlashMapedNo = 0;
	int iSeqNo = 0;
	//printf("\n_____________BEGIN__________");
	for(i = 0; i < MAX_NUM_RECT; i++)
	{
		if((g_CanData.CanFlashData.aRectifierInfo[i].dwSerialNo > 0) && 
			(g_CanData.CanFlashData.aRectifierInfo[i].dwSerialNo != -1) &&
			(g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo > 0) &&
			(g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo <= 999))
		{
			Flash_Map[i].dwSerialNo = g_CanData.CanFlashData.aRectifierInfo[i].dwSerialNo;
			Flash_Map[i].dwHiSN = g_CanData.CanFlashData.aRectifierInfo[i].dwHighSn;
			Flash_Map[i].iPhaseNo = g_CanData.CanFlashData.aRectifierInfo[i].iAcPhase;
			Flash_Map[i].iPositionNo = g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo;
			iFlashMapedNo++;
			//printf("\n i=%d; dwSerialNo=%d; iPos=%d",i,
			//	Flash_Map[i].dwSerialNo,Flash_Map[i].iPositionNo);
		}
		sFinal_Map[i].iPositionNo = -1;
		sFinal_Map[i].bIsNewInserted = TRUE;
		sFinal_Map[i].iSeqNo = -1;
		sFinal_Map[i].dwSerialNo = 0;
		sFinal_Map[i].iPhaseNo = GetPhaseNo(i);//��ABCABC�ķ�ʽ����
		//if(bFirstTime)
		//{
		//	iFlashMapedNo = 0;
		//	Flash_Map[i].dwSerialNo = -1;
		//	sFinal_Map[i].iSeqNo = i;
		//}
		//else
		//{
		//	sFinal_Map[i].iPositionNo = -1;
		//	sFinal_Map[i].dwSerialNo = -1;
		//}
		
	}
	//printf("_____________END,and the FlashMappedNo=%d__________\n",iFlashMapedNo);
	//bFirstTime = FALSE;
	//���´�Flash��λ�úŲ�ƥ����֪��ģ��λ�ú�
	BOOL bIsNeedWriteFlash = TRUE; //�Ƿ���Ҫд��Flash
	if(iFlashMapedNo > iRectNum && (!bReadConfirm)) //˵����ģ�鶪ʧ��
	{
		bIsNeedWriteFlash = FALSE;
	}
	BOOL bHasInvalidRect = FALSE;
	BOOL bHasRepeastedRect = FALSE;//�Ƿ������кų�ͻ
	//������кų��?,ʵ��Ӧ�û����������ظ��������Ե�ʱ����Բ��Ե�?
	for(i = 0; i < iRectNum; i++)
	{
		for(j = i + 1; j < iRectNum; j++)
		{
			if(g_CanData.aRoughDataRect[i][RECT_SERIEL_NO].dwValue == g_CanData.aRoughDataRect[j][RECT_SERIEL_NO].dwValue)
			{
				bHasRepeastedRect = TRUE;
				break;
			}
		}
	}
	if(!bReadConfirm)
	{
		for(i = 0; i < iRectNum; i++)
		{
			//sFinal_Map[i].dwHiSN = g_CanData.aRoughDataRect[i][RECT_HIGH_SN_ROUGH].dwValue;
			sFinal_Map[i].dwSerialNo = g_CanData.aRoughDataRect[i][RECT_SERIEL_NO].dwValue;
			sFinal_Map[i].iSeqNo = i;
			//��ͨ���жϣ��򲻱�д��Flash���ڶ��ν����»ָ�ԭ��Flash����?
			//bIsNeedWriteFlash = (sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE) ? FALSE : TRUE;
			if(sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE)
			{
				bHasInvalidRect = TRUE;
			}
			for(j = 0; j < iFlashMapedNo; j++)
			{
				if(sFinal_Map[i].dwSerialNo == Flash_Map[j].dwSerialNo)
				{
					sFinal_Map[i].bIsNewInserted = FALSE;
					sFinal_Map[i].iPositionNo = Flash_Map[j].iPositionNo;
					sFinal_Map[i].iPhaseNo = Flash_Map[j].iPhaseNo;
					break;
				}
			}
		}
	}
	else //��ȡ����ֵ
	{
		for(i = 0; i < iRectNum; i++)
		{
			if(i < MAX_NUM1_RECT)
			{
				//sFinal_Map[i].dwHiSN = GetDwordSigValue(RT_START_EQUIP_ID + i,
				//					SIG_TYPE_SAMPLING,
				//					26,
				//					"CAN_SAMP");
				sFinal_Map[i].dwSerialNo = GetDwordSigValue(RT_START_EQUIP_ID + i,
									SIG_TYPE_SAMPLING,
									42,
									"CAN_SAMP");
				sFinal_Map[i].iPhaseNo = GetDwordSigValue(RT_START_EQUIP_ID + i,
									SIG_TYPE_SETTING,
									RT_PHASE_SET_SIG_ID,
									"CAN_SAMP");
				sFinal_Map[i].iPositionNo = GetDwordSigValue(RT_START_EQUIP_ID + i,
												SIG_TYPE_SETTING,
												RT_POS_SET_SIG_ID,
												"CAN_SAMP");
			}
			else
			{
				//sFinal_Map[i].dwHiSN = GetDwordSigValue(RT_START_EQUIP_ID + i,
				//					SIG_TYPE_SAMPLING,
				//					26,
				//					"CAN_SAMP");
				sFinal_Map[i].dwSerialNo = GetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
									SIG_TYPE_SAMPLING,
									42,
									"CAN_SAMP");
				sFinal_Map[i].iPhaseNo = GetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
									SIG_TYPE_SETTING,
									RT_PHASE_SET_SIG_ID,
									"CAN_SAMP");
				sFinal_Map[i].iPositionNo = GetDwordSigValue(RT_START_EQUIP_ID2 + i - MAX_NUM1_RECT,
												SIG_TYPE_SETTING,
												RT_POS_SET_SIG_ID,
												"CAN_SAMP");
			}
			//��ͨ���жϣ��򲻱�д��Flash���ڶ��ν����»ָ�ԭ��Flash����?
			//bIsNeedWriteFlash = (sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE) ? FALSE : TRUE;

			if(sFinal_Map[i].dwSerialNo == 0)
			{
				bHasInvalidRect = TRUE;
			}
			sFinal_Map[i].bIsNewInserted = FALSE;

			for(j = 0; j < iRectNum; j++)
			{
				if(g_CanData.aRoughDataRect[j][RECT_SERIEL_NO].dwValue == sFinal_Map[i].dwSerialNo)
				{
					sFinal_Map[i].iSeqNo = j;
					break;
				}
			}
		}
	}
	//������Ҫ����λ�úų�ͻ�����?
	//���������?
	int k,s,iFoundPos;
	if(bReadConfirm || bFirstTime || bHasRepeastedRect) //�û������ˣ���Ҫ��������ͻ
	{
		for(i = 0; i < iRectNum; i++)
		{
			if(sFinal_Map[i].iPositionNo < 1 || sFinal_Map[i].iPositionNo > 999)
			{
				iFoundPos = -1;
				for(k = 1; k <= MAX_NUM_RECT; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iRectNum; s++)
					{
						if(sFinal_Map[s].iPositionNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[i].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);
				
			}
			else if(sFinal_Map[i].iSeqNo < 0 || sFinal_Map[i].iSeqNo > iRectNum)
			{
				iFoundPos = -1;
				for(k = 0; k < iRectNum; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iRectNum; s++)
					{
						if(sFinal_Map[s].iSeqNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[i].iSeqNo = (iFoundPos > 0 ? iFoundPos : 0);
			}
			else
			{
				for(j = i+1;j < iRectNum; j++)
				{
					if(sFinal_Map[i].iPositionNo == sFinal_Map[j].iPositionNo)
					{
						iFoundPos = -1;
						for(k = 1; k <= MAX_NUM_RECT; k++)
						{
							iFoundPos = k;
							for(s = 0; s < iRectNum; s++)
							{
								if(sFinal_Map[s].iPositionNo == k)
								{
									iFoundPos = -1;
									break;
								}
							}
							if(iFoundPos != -1)
							{
								break;
							}
						}
						sFinal_Map[i].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);
						break;
					}

				}
				for(j = i+1;j < iRectNum; j++)
				{
					
					if(sFinal_Map[i].iSeqNo == sFinal_Map[j].iSeqNo)
					{
						iFoundPos = -1;
						for(k = 0; k < iRectNum; k++)
						{
							iFoundPos = k;
							for(s = 0; s < iRectNum; s++)
							{
								if(sFinal_Map[s].iSeqNo == k)
								{
									iFoundPos = -1;
									break;
								}
							}
							if(iFoundPos != -1)
							{
								break;
							}
						}
						sFinal_Map[i].iSeqNo = (iFoundPos > 0 ? iFoundPos : 0);
						break;
					}
				}
			}
		}
	}
	else //���ŵ��µ�
	{
		for(j = 0;j < iRectNum; j++)
		{
			if(sFinal_Map[j].iPositionNo < 1 ||
				sFinal_Map[j].bIsNewInserted)//����������һ���ط��ţ�ѡ����С�Ŀ���λ�ú�
			{
				if(!bHasInvalidRect)//���?-9999��?�������ͨ���жϺ��ģ���ǲ�ס��
				{
					bIsNeedWriteFlash = TRUE; //����ģ����£�˵����Ҫ����дFlash
				}
				iFoundPos = -1;
				for(k = 1; k <= MAX_NUM_RECT; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iRectNum; s++)
					{
						if(sFinal_Map[s].iPositionNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[j].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);	
			}
		}
	}

	//���¿�ʼ����Postion���ã��Լ�Flash
	//printf("\n__________Begin Test Data____________");
	for(i = 0; i < MAX_NUM_RECT; i++)
	{
		if(i < iRectNum)
		{
			j = sFinal_Map[i].iSeqNo;
			//������ע�� �� i ���� j��Ҫ����,����Ĵ���Ƚϻ�ɬ�Ѷ�����Ҫ���׸Ķ�
			g_CanData.aRoughDataRect[i][RECT_SEQ_NO].iValue = j;

			g_CanData.aRoughDataRect[j][RECT_POSITION_NO].iValue = sFinal_Map[i].iPositionNo;			
			g_CanData.aRoughDataRect[j][RECT_PHASE_NO].iValue = sFinal_Map[i].iPhaseNo;
			
			if((bIsNeedWriteFlash && (!bHasInvalidRect)) || bFirstTime)
			{
				g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo = sFinal_Map[i].iPositionNo;
				g_CanData.CanFlashData.aRectifierInfo[i].iAcPhase = sFinal_Map[i].iPhaseNo;
				g_CanData.CanFlashData.aRectifierInfo[i].dwSerialNo = sFinal_Map[i].dwSerialNo;
				g_CanData.CanFlashData.aRectifierInfo[i].dwHighSn = sFinal_Map[i].dwHiSN;
				g_CanData.CanFlashData.aRectifierInfo[i].iSeqNo = sFinal_Map[i].iSeqNo;
			}
			//printf("\ni=%d,HiSN=%d,BarC1=%d;   Pos=%d;  SeqNo=%d",i,sFinal_Map[i].dwHiSN,
			//	sFinal_Map[i].dwSerialNo,sFinal_Map[i].iPositionNo,sFinal_Map[i].iSeqNo);
		}
		else
		{
			if((bIsNeedWriteFlash && (!bHasInvalidRect)) || bFirstTime)
			{
				g_CanData.CanFlashData.aRectifierInfo[i].dwSerialNo = 0;
				g_CanData.CanFlashData.aRectifierInfo[i].iPositionNo = -1;
			}
			
		}
	}
	//printf("\n__________End Test Data____________\n");
	
	//-----------���»�Ҫ���򣬰�#1��#2��#3.��������
	RECT_POS_ADDR	Sorted_Map[MAX_NUM_RECT];//��������
	for (i = 0; i < iRectNum; i++)
	{
		Sorted_Map[i].iPositionNo = g_CanData.aRoughDataRect[i][RECT_POSITION_NO].iValue;
		Sorted_Map[i].iSeqNo = i;
	}
	int iPos;
	for (i = 0; i < iRectNum - 1; i++)
	{
		for (j = 0; j < (iRectNum - 1 - i); j++)
		{
			if(Sorted_Map[j].iPositionNo > Sorted_Map[j + 1].iPositionNo)
			{
				iPos = Sorted_Map[j].iPositionNo;
				Sorted_Map[j].iPositionNo = Sorted_Map[j + 1].iPositionNo;
				Sorted_Map[j + 1].iPositionNo = iPos;
			}
		}
	}
	for (i = 0; i < iRectNum; i++)
	{
		for (j = 0; j < iRectNum; j++)
		{
			if(Sorted_Map[i].iPositionNo == g_CanData.aRoughDataRect[j][RECT_POSITION_NO].iValue)
			{
				g_CanData.aRoughDataRect[j][RECT_SEQ_NO].iValue = Sorted_Map[i].iSeqNo;
				break;
			}
		}
	}
	//-----------�������?
	//��������?
	for (i = 0; i < iRectNum; i++)
	{
		j = (g_CanData.aRoughDataRect[i][RECT_SEQ_NO].iValue);
		if(j < MAX_NUM1_RECT)
		{
			SetDwordSigValue(RT_START_EQUIP_ID + j,
						SIG_TYPE_SETTING,
						RT_POS_SET_SIG_ID,
						sFinal_Map[i].iPositionNo,
						"CAN_SAMP");
			SetDwordSigValue(RT_START_EQUIP_ID + j,
						SIG_TYPE_SETTING,
						RT_PHASE_SET_SIG_ID,
						sFinal_Map[i].iPhaseNo,
						"CAN_SAMP");
		}
		else
		{
			SetDwordSigValue(RT_START_EQUIP_ID2 + j - MAX_NUM1_RECT,
						SIG_TYPE_SETTING,
						RT_POS_SET_SIG_ID,
						sFinal_Map[i].iPositionNo,
						"CAN_SAMP");
			SetDwordSigValue(RT_START_EQUIP_ID2 + j - MAX_NUM1_RECT,
						SIG_TYPE_SETTING,
						RT_PHASE_SET_SIG_ID,
						sFinal_Map[i].iPhaseNo,
						"CAN_SAMP");
		}
	}

	if((bIsNeedWriteFlash && (!bHasInvalidRect)) || bFirstTime)//�д���rectʱ��дFalsh
	{
		CAN_WriteFlashData(&(g_CanData.CanFlashData));
	}

	bFirstTime = FALSE;
	return;
}

/*==========================================================================*
* FUNCTION : WaitAllocation
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  :                 DATE: 2010-02-05 08:31
*==========================================================================*/
INT32 WaitAllocation()
{
	//INT32 i;
	INT32 iResetingAddr = 0;
	INT32 iTimeOut = 0;
	INT32 iRst;

	//�󾭹��������Ī��ƽȷ�ϣ����ŵ�ʱ��?	ֻ��	��ַ1	
	//�Ϳ���ȷ���������������Ź����еĽ���ַ��0��100����Ϊ1
	//MAX_NUM_RECT
	/*for (i = 0; i <= 1; i++)
	{
		iRst = RtSampleCmd20(i);
		//TRACE(" Cmd20 Address %d  Rst %d \n",i,iRst);

		if (CAN_RECT_REALLOCATE == iRst)
		{
			iResetingAddr = i;
			break;
		}
	}*/

	while (CAN_RECT_REALLOCATE == RtSampleCmd20(iResetingAddr))
	{
		iTimeOut++;
		Sleep(500);
		RT_UrgencySendCurrLimit();
		TRACE("\n The Rectifer Reseting \n");

		if (iTimeOut > 40)//500MS * 40 = 20S
		{
			TRACE("\n The Rectifer have Bug that Allocation timeout \n");
			return -1;
		}
	}

	return 0;
}

static void SetRectInfoChangedFlag()
{
#define		RECT_GROUP_EQUP_ID2				2
#define		MODULE_INFO_CHANGE_SIG_TYPE		2
#define		MODULE_INFO_CHANGE_SIG_ID		37

	SetDwordSigValue(RECT_GROUP_EQUP_ID2, 
		MODULE_INFO_CHANGE_SIG_TYPE, 
		MODULE_INFO_CHANGE_SIG_ID,
		TRUE,
		"FOR_RS485");
	//TRACE("\n @@@@@@@    SLAVE MODULES WAS CHANGE NOTIFY TO MASTER \n");

}


#define MAX_TIMES_RT_CMD00	3
#define INTERVAL_TIMES_RT_CMD30		3

/*==========================================================================*
 * FUNCTION : RT_Sample
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:31
 *==========================================================================*/
#define GC_SUPPORT_CONVERTER_ONLY
void RT_Sample(void)
{
	int		iAddr,iMAXAddr, iSAddr;
	static		int iTwice = 0;	
	static		BOOL bReport = FALSE;
	SIG_ENUM	emRst;
	static int s_Times = 0;
	//int j;
	if(s_Times > 100)
	{
		s_Times = 0;
	}
	else
	{
		s_Times++;
	}

	RefreshRectRunTime();
	Param24VDispByConvProc();
//	printf("T105\n");

	//Modified by Samson Fan, 2009-11-17
	//���⣺������ͣ���ҵ���µ������ģ��ͨѶ�жϣ���ʱCAN�ֻ������ã�����ģ����ϢȫΪδ���ã�
	//�޷��鿴ͨѶ�ж�ǰ��ģ����Ϣ��Ӧ���Ǳ���֮ǰ����Ϣ��������ʾ
	//�����Ϊֻ���ڴ�û�ҵ���ģ�飬���߲���ģ�����ŵ�ַʱ�������á�?
	if((g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig)
	//	|| (g_CanData.CanCommInfo.RectCommInfo.iCommRectNum == 0))
		|| (g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue == 0))

	{
		RT_UrgencySendCurrLimit();
		WaitAllocation();
		RtDefaultVoltParam(TRUE);
		//This sentence must be in front of RT_Reconfig();
		g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = FALSE;
		RT_Reconfig();


		if(GetEnumSigValue(1, 
				SIG_TYPE_SETTING,
				SYS_CONVERTER_ONLY_ID,
				"CAN_SAMP"))
		{
			//Do nothing;
		}
		else
		{
			SetRectInfoChangedFlag();
		}
		iTwice = 0;
		RT_UrgencySendCurrLimit();
		if(GetEnumSigValue(1, 
			SIG_TYPE_SETTING,
			SYS_CONVERTER_ONLY_ID,
			"CAN_SAMP"))
		{
			SIG_ENUM stState = GetEnumSigValue(210,
					SIG_TYPE_SAMPLING,
					35,
					"CAN_SAMP");
			//if alarm is active, end the alarm
			if(stState != 0)
			{
				SetEnumSigValue(210,
					SIG_TYPE_SAMPLING,
					35,
					0,
					"CAN_SAMP");
			}
		}
		bReport = TRUE;
		
		return;
	}

	if(bReport)
	{
		NotificationToAll(MSG_RECT_ALLOCATION);
		bReport = FALSE;
	}
	//���´���HandlePos
	BOOL bNeedHandlPosPhase = NeedHandleRtPosPhase();
	//Jimmyע�ͣ����ģ������֮�󣬴����Ҫ���������ڲ����ź�λ�úţ�����������3.
	if(bNeedHandlPosPhase || iTwice < 3 || g_bNeedClearRectIDs)
	{
		if(g_bNeedClearRectIDs)
		{
			g_bNeedClearRectIDs = FALSE;
		}
		HandleRtPosAndPhase_New(bNeedHandlPosPhase);
		//Don't delete:Here is for fixing the reposition problem ,to handle twice!
		//HandleRtPosAndPhase_New(bNeedHandlPosPhase);
		//Don't delete
		if(iTwice == 3)
		{
			iTwice = 0;
		}
		if(iTwice == 2)
		{
			NotificationToAll(MSG_CONIFIRM_ID);
			if(GetEnumSigValue(1, 
				SIG_TYPE_SETTING,
				180,
				"CAN_SAMP") == 2)
			{
				SetEnumSigValue(2,
					2,
					37,
					1,
					"CAN_SAMP");

			}
			
		}
		iTwice++;
	}
	/*for(j = 0; j < MAX_NUM_RECT; j ++)
	{
		printf("***R%d=%d\n",j,g_CanData.aRoughDataRect[j][RECT_POSITION_NO].iValue);
		printf("!!!R%d=%d\n",j,g_CanData.CanFlashData.aRectifierInfo[j].iPositionNo);
		printf("!!!Est%d=%d\n",j,g_CanData.CanFlashData.aRectifierInfo[j].bExistence);
		printf("!!!Int%d=%d\n",j,g_CanData.aRoughDataRect[j][RECT_INTERRUPT_ST].iValue);
	}*/


	/*for(j = 0; j < MAX_NUM_RECT; j ++)
	{
		printf("***R%d=%d\n",j,g_CanData.aRoughDataRect[j][RECT_POSITION_NO].iValue);
	}*/

	if(g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1 > g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2)
		iMAXAddr = g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;
	else
		iMAXAddr = g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2;
		
	//for(iAddr = 0; iAddr < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; iAddr++)
	for(iAddr = 0; iAddr < iMAXAddr; iAddr++)
	{
		iSAddr = iAddr + g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;
		//if((g_CanData.CanFlashData.aRectifierInfo[iAddr].bExistence)
		//	|| (g_CanData.CanFlashData.aRectifierInfo[iSAddr].bExistence)
		//{
			emRst = RtSampleCmd00(iAddr);
			//printf("V%d=%f\n",iAddr,g_CanData.aRoughDataRect[iAddr][RECT_DC_VOLTAGE].fValue);
			//printf("emRst %d = %d! CAN1Ext=%d; CAN2Ext=%d\n", iAddr, emRst);

			if(emRst == CAN_RECT_REALLOCATE)
			{
				break;
			}
			/*if(((emRst == CAN1_RECT_FAIL) || (emRst == CAN_BOTH_RECT_FAIL))
				&& (g_CanData.CanFlashData.aRectifierInfo[iAddr].bExistence != 0))
			{
				g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iAddr]++;
			}
			if(((emRst == CAN2_RECT_FAIL) || (emRst == CAN_BOTH_RECT_FAIL))
				&& (g_CanData.CanFlashData.aRectifierInfo[iSAddr].bExistence != 0))
			{
				g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iSAddr]++;
			}*/

			if(((emRst == CAN_SAMPLE_OK) || (emRst == CAN2_RECT_FAIL))
				&& (iAddr >= g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1))
			{
				RT_TriggerAllocation();
				return;
			}

			if(((emRst == CAN_SAMPLE_OK) || (emRst == CAN1_RECT_FAIL))
				&& (iAddr >= g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2))
			{
				RT_TriggerAllocation();
				return;
			}

			if(g_CanData.CanFlashData.aRectifierInfo[iAddr].bExistence)
				g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iAddr]++;
			else if((emRst == CAN_SAMPLE_OK) || (emRst == CAN2_RECT_FAIL))
			{
				RT_TriggerAllocation();
				return;
			}

			if(g_CanData.CanFlashData.aRectifierInfo[iSAddr].bExistence)
				g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iSAddr]++;
			else if((emRst == CAN_SAMPLE_OK) || (emRst == CAN1_RECT_FAIL))
			{
				RT_TriggerAllocation();
				return;
			}

			if(s_Times%INTERVAL_TIMES_RT_CMD30 == 0)
			{
				RtSampleCmd30(iAddr);
				//emRstCmd30 = RtSampleCmd30(iAddr);
				//if (emRstCmd30 == CAN_SAMPLE_OK || emRstCmd30 == CAN1_RECT_FAIL || emRstCmd30 == CAN2_RECT_FAIL)
				//{
					//RtCalcEnergy(iAddr);
				//}
			}

			if((g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iAddr] >= MAX_TIMES_RT_CMD00) 
				&& (g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue == RT_THREE_PHASE))
			{
				if(CAN_RECT_REALLOCATE == RtSampleCmd10(iAddr))
				{
					break;
				}
				g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iAddr] = 0; 
			}

			if((g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iSAddr] >= MAX_TIMES_RT_CMD00)
				&& (g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue == RT_THREE_PHASE))
			{
				if(CAN_RECT_REALLOCATE == RtSampleCmd10(iSAddr))
				{
					break;
				}
				g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iSAddr] = 0;
			}

			if(iAddr % 10 == 0)
			{
				RT_UrgencySendCurrLimit();
			}
		//}
	}

	//printf("Load fRectVoltCAN1=%f; Num1=%d; Num2=%d\n", fRectVoltCAN1, g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1, g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2);
	if((g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1 != 0)
		&& (g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2 != 0))
	{
		//printf("Begin Loadshare!!!\n");
		RT_LoadShare();
	}

	if(GetEnumSigValue(1, 
			SIG_TYPE_SETTING,
			SYS_CONVERTER_ONLY_ID,
			"CAN_SAMP"))
	{
		BYTE			abyVal[4];
		int iCommRectNumber = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;
		for(iAddr = 0; iAddr < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; iAddr++)
		{
			/*if(g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iAddr] 
						>= MAX_TIMES_RT_CMD00)*/
			if(g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue || 
				g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_ST].iValue == RT_COMM_INTERRUPT_ST) 
			{
				//printf("iAddr= %d, g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iAddr]= %d, g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue= %d,g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_ST].iValue = %d\n", iAddr, g_CanData.CanCommInfo.RectCommInfo.iCmd00Counter[iAddr], g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue,g_CanData.aRoughDataRect[iAddr][RECT_INTERRUPT_ST].iValue );
				iCommRectNumber--;
			}
		}
		//printf("g_CanData.CanCommInfo.RectCommInfo.iCommRectNum = %d\n, iCommRectNumber = %d\n", g_CanData.CanCommInfo.RectCommInfo.iCommRectNum, iCommRectNumber );
		if(iCommRectNumber)
		{
			//Raise alarm
			SetEnumSigValue(210,
				SIG_TYPE_SAMPLING,
				35,
				1,
				"CAN_SAMP");
			//Broadcast the off command.
			abyVal[0] = 0;
			abyVal[1] = RT_AC_OFF;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							RT_VAL_TYPE_W_AC_ON_OFF,
							abyVal);
			
		}
		else
		{
			//if alarm is active, end the alarm
			SetEnumSigValue(210,
				SIG_TYPE_SAMPLING,
				35,
				0,
				"CAN_SAMP");
		}
	}
	RtNonSampSigHandle();

	return;
}



/*==========================================================================*
 * FUNCTION : GetRtAddrBySeqNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iSeqNo : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:28
 *==========================================================================*/
static int GetRtAddrBySeqNo(int iSeqNo)
{
	int		i;
	int		iRectNum = g_CanData.CanCommInfo.RectCommInfo.iCommRectNum;
	
	for(i = 0; i < iRectNum; i++)
	{
		if(g_CanData.aRoughDataRect[i][RECT_SEQ_NO].iValue == iSeqNo)
		{
			return i;
		}
	}
	
	return 0;
}

//��������豸ͨ���жϣ���ģ�����µ�һЩ�����źŸ�ֵ�?-9998
void RT_SetSomeSigInvalid()
{
	//����жϳ������豸����Ӧ�����ߵ�ѹ�ȵȼ����źŸ��?-9998 ����Ҫ��12��2��17�ո��ġ�
	if (TRUE == g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue)
	{
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_A_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_B_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_C_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_AB_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_BC_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_CA_VOLT].iValue = (-9998);
		
		g_CanData.aRoughDataGroup[GROUP_RT_AC_SNMP_VOLT1].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_RT_AC_SNMP_VOLT2].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_RT_AC_SNMP_VOLT3].iValue = (-9998);
	}

}


/*==========================================================================*
 * FUNCTION : Rec_GetBarCode
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: 
 * LPVOID  lpvoid   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : 		                DATE: 2018-11-28 17:27
 *==========================================================================*/
void Rec_GetBarCode(int iRecNum,char * buf)
{
		BYTE	byTempBuf[4];
		char 	* pPartNumber;

		pPartNumber = buf;
	
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE1].uiValue);
		pPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pPartNumber[1] = byTempBuf[3];//S CODE13
		
		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pPartNumber[2] = byTempBuf[0];//M CODE14
		pPartNumber[3] = byTempBuf[1];//D CODE15
		pPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pPartNumber[5] = '\0';
		}
		
		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pPartNumber[9] = '\0';//CODE21
		}

		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[iRecNum][RECT_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pPartNumber[12] = '\0';
		}

		pPartNumber[13] = '\0';//CODE25
}



/*==========================================================================*
 * FUNCTION : For80RecAlarmHandle
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: 
 * LPVOID  lpvoid   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : 		                DATE: 2018-11-28 17:27
 *==========================================================================*/

 #define REC_3500E3_SW_VER  104 //3500e3 ֧��80��ģ�������汾Ϊ1.04
 #define REC_4000E_SW_VER  102 //4000e ֧�ִ���60��ģ�������汾Ϊ1.02
void For80RecAlarmHandle()
{
	int i=0;
	int iRet1=0;
	int iRet2=0;
	unsigned int uiRecVersion = 0;
	int iRecSwVersion = 0;
	int iRec4000eVer =REC_4000E_SW_VER;
	int iRec3500e3Ver =REC_3500E3_SW_VER;
	BOOL bOldFirmwareOnCan1 = FALSE;
	BOOL bOldFirmwareOnCan2 = FALSE;
	int iNum4000eOnCan1=0;
	int iNum4000eOnCan2=0;
	int iNum3500e3OnCan1=0;
	int iNum3500e3OnCan2=0;
	char szRecPartNumber[16]={0};
	char szTemp[16]={0};
	
	const char * pRec4000eBar  ="1R484000e";
	const char * pRec4000EBar  ="1R484000E";
	const char * pRec3500e3Bar="1R483500e3";
	const char * pRec3500E3Bar="1R483500E3";
	const char * pRec29001oBar="1R4829001o";//use for test
	
	int iActualRecNum = g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue;//����g_CanData.CanCommInfo.RectCommInfo.iCommRectNum

	g_CanData.aRoughDataGroup[GROUP_RT_OLD_SW_REC_CAN1].iValue = 0;
	g_CanData.aRoughDataGroup[GROUP_RT_OLD_SW_REC_CAN2].iValue = 0;
	g_CanData.aRoughDataGroup[GROUP_RT_COMM_REC_ON_CAN1].iValue = g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1;
	g_CanData.aRoughDataGroup[GROUP_RT_COMM_REC_ON_CAN2].iValue= g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN2;
	
	for(i=0; i<iActualRecNum; i++)
	{
		uiRecVersion = g_CanData.aRoughDataRect[i][RECT_VERSION_NO].uiValue;
		iRecSwVersion = uiRecVersion & 0xFFFF;

		Rec_GetBarCode(i,szRecPartNumber);

		if( szRecPartNumber[0] )
		{
			iRet1=snprintf(szTemp,sizeof(szTemp),"%s",szRecPartNumber);
			//printf("iRecSwVersion=%d,szTemp=%s,iRet1=%d~~~\n",iRecSwVersion,szTemp,iRet1);
			if( iRet1 > 0 )
			{
				iRet1= strcmp(szTemp,pRec3500e3Bar);
				iRet2= strcmp(szTemp,pRec3500E3Bar);
				if( (0==iRet1) ||(0==iRet2) )
				{
					if( i < g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1 )
					{
						//iNum3500e3OnCan1++;
						if( iRecSwVersion < iRec3500e3Ver )
						{
							bOldFirmwareOnCan1 = TRUE;
							break;
						}
					}
					else
					{
						//iNum3500e3OnCan2++;
						if( iRecSwVersion < iRec3500e3Ver )
						{
							bOldFirmwareOnCan2 = TRUE;
							break;
						}
					}	
				}

				iRet1= strcmp(szTemp,pRec4000eBar);
				iRet2= strcmp(szTemp,pRec4000EBar);
				//iRet2= strcmp(szTemp,pRec29001oBar);

				if( (0==iRet1) || (0==iRet2) )
				{
					if( i < g_CanData.CanCommInfo.RectCommInfo.iRectNumCAN1 )
					{
						//iNum4000eOnCan1++;
						if( iRecSwVersion < iRec4000eVer )
						{
							bOldFirmwareOnCan1 = TRUE;
							break;
						}
					}
					else
					{
						//iNum4000eOnCan2++;
						if( iRecSwVersion < iRec4000eVer )
						{
							bOldFirmwareOnCan2 = TRUE;
							break;
						}
					}	
				}
			}
		}	
	}

//printf("iNum3500e3OnCan1=%d,iNum3500e3OnCan2=%d,iNum4000eOnCan1=%d,iNum4000eOnCan2=%d~~\n",iNum3500e3OnCan1,iNum3500e3OnCan2,iNum4000eOnCan1,iNum4000eOnCan2);
//printf("bOldFirmwareOnCan1=%d,bOldFirmwareOnCan2=%d~~\n",bOldFirmwareOnCan1,bOldFirmwareOnCan2);


	if( TRUE == bOldFirmwareOnCan1 )
	{
		g_CanData.aRoughDataGroup[GROUP_RT_OLD_SW_REC_CAN1].iValue =1;
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_RT_OLD_SW_REC_CAN1].iValue =0;
	}

	if( TRUE == bOldFirmwareOnCan2 )
	{
		g_CanData.aRoughDataGroup[GROUP_RT_OLD_SW_REC_CAN2].iValue =1;
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_RT_OLD_SW_REC_CAN2].iValue =0;
	}

	return;
}

enum PHASE_CURR_STATES
{
  PHASE_CURR_STATE_UNINIT = 0,
  PHASE_CURR_STATE_VALID,
  PHASE_CURR_STATE_INVALID
};

#define RT_CH_END_FLAG			-1
/*==========================================================================*
 * FUNCTION : RT_StuffChannel
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ENUMSIGNALPROC EnumProc : //Callback function for stuffing channels 
 *            LPVOID  lpvoid   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Frank Cao                DATE: 2008-07-19 17:27
 *==========================================================================*/
void RT_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
					 LPVOID lpvoid)				//Parameter of the callback function
{
	int		i, j, iRectPhase;

	int		iRectNum = g_CanData.aRoughDataGroup[GROUP_RT_ACTUAL_NUM].iValue;

	float	fPhaseCurr[3] = { 0.0 };
	int iPhaseCurrState[3] = {0};	// This is to be closely coupled with fPhaseCurr[]



	For80RecAlarmHandle();

	CHANNEL_TO_ROUGH_DATA		RtGroupSig[] =
	{
		{SYS_CH_CAN_INTERRUPT,	GROUP_SYS_CAN_INTERRUPT,	},
		{SYS_CH_CAN_CIRTIME,	GROUP_RT_CIRTIME,},

		{RT_CH_RECT_NUM,		GROUP_RT_ACTUAL_NUM,		},
		{RT_CH_RATED_VOLT,		GROUP_RT_RATED_VOLT,		},	
		{RT_CH_RATED_CURR,		GROUP_RT_RATED_CURR,		},
		{RT_CH_AC_P_A_VOLT,		GROUP_RT_AC_P_A_VOLT,		},
		{RT_CH_AC_P_B_VOLT,		GROUP_RT_AC_P_B_VOLT,		},
		{RT_CH_AC_P_C_VOLT,		GROUP_RT_AC_P_C_VOLT,		},
		{RT_CH_AC_P_AB_VOLT,	GROUP_RT_AC_P_AB_VOLT,		},
		{RT_CH_AC_P_BC_VOLT,	GROUP_RT_AC_P_BC_VOLT,		},
		{RT_CH_AC_P_CA_VOLT,	GROUP_RT_AC_P_CA_VOLT,		},
		{RT_CH_SNMP_A_AB_VOLT,	GROUP_RT_AC_SNMP_VOLT1,		},
		{RT_CH_SNMP_B_BC_VOLT,	GROUP_RT_AC_SNMP_VOLT2,		},
		{RT_CH_SNMP_C_CA_VOLT,	GROUP_RT_AC_SNMP_VOLT3,		},
		{RT_CH_ALL_AC_FAIL,		GROUP_RT_AC_FAIL,			},
		{RT_CH_ALL_NO_RESPONSE,	GROUP_RT_ALL_NO_RESPONSE,	},
		{RT_CH_AC_RATED_VOLT,	GROUP_RT_AC_RATED_VOLT,		},

		{RT_CH_DC_DC_TYPE,		GROUP_RT_DC_DC_TYPE,		},
		{RT_CH_AC_PHASE_TYPE,	GROUP_RT_1_OR_3_PHASE,},
		{RT_CH_OLD_SW_REC_CAN1,	GROUP_RT_OLD_SW_REC_CAN1},
		{RT_CH_OLD_SW_REC_CAN2,	GROUP_RT_OLD_SW_REC_CAN2},
		{RT_CH_COMM_REC_ON_CAN1,	GROUP_RT_COMM_REC_ON_CAN1},
		{RT_CH_COMM_REC_ON_CAN2,	GROUP_RT_COMM_REC_ON_CAN2},

		{RT_CH_END_FLAG,		RT_CH_END_FLAG,				},
	};

	CHANNEL_TO_ROUGH_DATA		RtSig[] =
	{
		{RT_CH_DC_VOLT,				RECT_DC_VOLTAGE,			},
		{RT_CH_ACTUAL_CURR,			RECT_ACTUAL_CURRENT,		},	
		{RT_CH_CURR_LMT,			RECT_CURRENT_LIMIT,			},
		{RT_CH_TEMPERATURE,			RECT_TEMPERATURE,			},
		{RT_CH_AC_VOLT,				RECT_AC_VOLTAGE,			},
		{RT_CH_HIGH_VOLT,			RECT_OVER_VOLTAGE_LIMIT,	},
		{RT_CH_DISP_CURR,			RECT_CURRENT_FOR_DISP,		},
		{RT_CH_SERIAL_NO,			RECT_SERIEL_NO,				},
		{RT_CH_HIGH_SERIAL_NO,		RECT_HIGH_SN_REPORT,		},
		{RT_CH_INTERRUPT_ST,		RECT_INTERRUPT_ST,			},
		{RT_CH_AC_FAIL_ST,			RECT_AC_FAILURE,			},
		{RT_CH_OVER_VOLT_ST,		RECT_DC_OVER_VOLTAGE,		},
		{RT_CH_FAULT_ST,			RECT_RECTFIER_FAULT,		},
		{RT_CH_PROTECTION_ST,		RECT_PROTECTION,			},
		{RT_CH_FAN_FAULT_ST,		RECT_FAN_FAULT,				},
		{RT_CH_EEPROM_FAULT_ST,		RECT_EEPROM_FAULT,			},
		{RT_CH_VALID_RATED_CURR,	RECT_VALID_RATED_CURR,		},
		{RT_CH_EFFICIENCY_NO_ST,	RECT_EFFICIENCY_NO,			},
		{RT_CH_POWER_LMT_ST,		RECT_POWER_LIMITED,			},
		{RT_CH_DC_ON_OFF_ST,		RECT_DC_ON_OFF_STATUS,		},
		{RT_CH_FAN_FULL_SPEED_ST,	RECT_FAN_FULL_SPEED,		},
		{RT_CH_WALKIN_ST,			RECT_WALKIN_STATUS,			},
		{RT_CH_AC_ON_OFF_ST,		RECT_AC_ON_OFF_STATUS,		},
		{RT_CH_SHARE_CURR_ST,		RECT_SHARE_CURR_STATUS,		},
		{RT_CH_OVER_TEMP_ST,		RECT_OVER_TEMP_STATUS,		},
		{RT_CH_VER_NO,				RECT_VERSION_NO,			},
		{RT_CH_RUN_TIME,			RECT_TOTAL_RUN_TIME,		},
		//{RT_CH_PART_NO,			RECT_PART_NO,				},
		{RT_CH_PHASE_AB_A_VOLT,		RECT_PH_A_VOLTAGE,			},
		{RT_CH_PHASE_BC_B_VOLT,		RECT_PH_B_VOLTAGE,			},
		{RT_CH_PHASE_CA_C_VOLT,		RECT_PH_C_VOLTAGE,			},
		{RT_CH_POS_NO,				RECT_POSITION_NO,			},
		{RT_CH_PHASE_NO,			RECT_PHASE_NO,				},
		{RT_CH_BARCODE1,			RECT_BARCODE1,				},
		{RT_CH_BARCODE2,			RECT_BARCODE2,				},
		{RT_CH_BARCODE3,			RECT_BARCODE3,				},
		{RT_CH_BARCODE4,			RECT_BARCODE4,				},
		//{RT_CH_ESTOP_STATE,			RECT_ESTOP_STATUS,			},
		{RT_CH_EXIST_STATE,			RECT_EXIST_ST,				},
		{RT_CH_SEVERE_CURR_IMB,		RECT_SEVERE_CURR_IMBALANCE,	},
		{RT_CH_CURR_IMB,			RECT_CURR_IMBALANCE,		},

		{RT_CH_END_FLAG,			RT_CH_END_FLAG,				},
	};

	CHANNEL_TO_ROUGH_DATA		Rt_Greater40Sig[] =
	{
		{RT_CH_PHA_CURR,			RECT_PHA_CURR,		},
		{RT_CH_PHB_CURR,			RECT_PHB_CURR,		},
		{RT_CH_PHC_CURR,			RECT_PHC_CURR,		},
		{RT_CH_END_FLAG,			RT_CH_END_FLAG,		},
	};


	i = 0;

	if(g_CanData.aRoughDataGroup[GROUP_RT_AC_P_A_VOLT].fValue < RT_AC_FAIL_POINT)
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_A_VOLT].fValue = 0;

	if(g_CanData.aRoughDataGroup[GROUP_RT_AC_P_B_VOLT].fValue < RT_AC_FAIL_POINT)
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_B_VOLT].fValue = 0;

	if(g_CanData.aRoughDataGroup[GROUP_RT_AC_P_C_VOLT].fValue < RT_AC_FAIL_POINT)
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_C_VOLT].fValue = 0;

	if(g_CanData.aRoughDataGroup[GROUP_RT_AC_P_AB_VOLT].fValue < RT_AC_FAIL_POINT)
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_AB_VOLT].fValue = 0;

	if(g_CanData.aRoughDataGroup[GROUP_RT_AC_P_BC_VOLT].fValue < RT_AC_FAIL_POINT)
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_BC_VOLT].fValue = 0;

	if(g_CanData.aRoughDataGroup[GROUP_RT_AC_P_CA_VOLT].fValue < RT_AC_FAIL_POINT)
		g_CanData.aRoughDataGroup[GROUP_RT_AC_P_CA_VOLT].fValue = 0;

	while(RtGroupSig[i].iChannel != RT_CH_END_FLAG)
	{
		if (GROUP_RT_ALL_NO_RESPONSE == RtGroupSig[i].iRoughData)
		{
			TRACE("Stuff Group iChannel = %d, lValue = %d, fValue = %.02f...\n",
				RtGroupSig[i].iChannel,
				g_CanData.aRoughDataGroup[RtGroupSig[i].iRoughData].iValue,
				g_CanData.aRoughDataGroup[RtGroupSig[i].iRoughData].fValue);

			//����жϳ������豸����Ӧ�����ߵ�ѹ�ȵȼ����źŸ��?-9998 ����Ҫ��12��2��17�ո��ġ�
			if (TRUE == g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue)
			{
				RT_SetSomeSigInvalid();
			}

		}
		if(GetEnumSigValue(1, 
				SIG_TYPE_SETTING,
				SYS_CONVERTER_ONLY_ID,
				"CAN_SAMP"))
		{	
			SAMPLING_VALUE		stInvalidValue;
			stInvalidValue.iValue = CAN_SAMP_INVALID_VALUE;
			//make the rectifier number be zero.
			EnumProc(RtGroupSig[i].iChannel, 
					stInvalidValue.fValue, 
					lpvoid );
			
		}
		else
		{
			EnumProc(RtGroupSig[i].iChannel, 
					g_CanData.aRoughDataGroup[RtGroupSig[i].iRoughData].fValue, 
					lpvoid );
		}
		
		i++;
	}
	
	
	//Stuff the channels by the order of position NO.
	if(GetEnumSigValue(1, 
				SIG_TYPE_SETTING,
				SYS_CONVERTER_ONLY_ID,
				"CAN_SAMP") )
	{	
		//make the rectifier number be zero.
		for(i = 0; i < MAX_NUM_RECT; i++)
		{
			if(i < MAX_NUM1_RECT)
				EnumProc(i * MAX_CHN_NUM_PER_RT + RT_CH_EXIST_STATE, 
					RT_EQUIP_NOT_EXISTENT, 
					lpvoid );
			else
				EnumProc((i - MAX_NUM1_RECT) * MAX_CHN_NUM_PER_RT + RT_CH_EXIST_STATE + CAN2_OFFSET, 
					RT_EQUIP_NOT_EXISTENT, 
					lpvoid );
		}
		
	}
	else
	{
		for(i = 0; i < iRectNum; i++)
		{
			int		iAddr = GetRtAddrBySeqNo(i);

			j = 0;
			while(RtSig[j].iChannel != RT_CH_END_FLAG)
			{
				/*if(RtSig[j].iChannel == RT_CH_SERIAL_NO)
				{
					printf("Stuff unit: iAddr = %d, SigIdx = %d, iChannel = %d, lValue = %d, fValue = %.02f...\n",
					iAddr,
					RtSig[j].iRoughData,
					i * MAX_CHN_NUM_PER_RT + RtSig[j].iChannel,
					g_CanData.aRoughDataRect[iAddr][RtSig[j].iRoughData].iValue,
					g_CanData.aRoughDataRect[iAddr][RtSig[j].iRoughData].fValue);
					g_CanData.aRoughDataRect[iAddr][RtSig[j].iRoughData].uiValue = 12345567+i;
				}*/

				if(i < MAX_NUM1_RECT)
					EnumProc(i * MAX_CHN_NUM_PER_RT + RtSig[j].iChannel, 
						g_CanData.aRoughDataRect[iAddr][RtSig[j].iRoughData].fValue, 
						lpvoid );
				else
					EnumProc((i - MAX_NUM1_RECT) * MAX_CHN_NUM_PER_RT + RtSig[j].iChannel + CAN2_OFFSET, 
						g_CanData.aRoughDataRect[iAddr][RtSig[j].iRoughData].fValue, 
						lpvoid );

				/*if (RECT_AC_VOLTAGE == RtSig[j].iRoughData)
				{
					//printf("	g_CanData.aRoughDataRect[%d][RECT_AC_VOLTAGE].fValue=%f \n",iAddr,g_CanData.aRoughDataRect[iAddr][RtSig[j].iRoughData].fValue);

				}
				if (RECT_AC_FAILURE == RtSig[j].iRoughData)
				{
					//printf("	g_CanData.aRoughDataRect[%d][RECT_AC_FAILURE].iValue=%d \n",iAddr,g_CanData.aRoughDataRect[iAddr][RtSig[j].iRoughData].iValue);

				}*/
				j++;
			}	

			j = 0;
			while (Rt_Greater40Sig[j].iChannel != RT_CH_END_FLAG)
			{
				if (i < MAX_NUM1_RECT)
				{
					EnumProc(i * MAX_CHN_NUM_PER_RT + Rt_Greater40Sig[j].iChannel,
						g_CanData.aRoughDataRect[iAddr][Rt_Greater40Sig[j].iRoughData].fValue,
						lpvoid);
				}
				else
				{
					EnumProc((i - MAX_NUM1_RECT)* MAX_CHN_NUM_PER_RT + Rt_Greater40Sig[j].iChannel + CAN2_OFFSET,
						g_CanData.aRoughDataRect[iAddr][Rt_Greater40Sig[j].iRoughData].fValue,
						lpvoid);
				}
				// Add by raoliang  2019 02 14  
				if (g_CanData.aRoughDataRect[iAddr][GROUP_RT_1_OR_3_PHASE].iValue == 0)
				{
					if (RECT_PHA_CURR == Rt_Greater40Sig[j].iRoughData)
					{

						iRectPhase = GetEnumSigValue(3 + iAddr, SIG_TYPE_SETTING, 2, "CAN_Rect");
						if (iRectPhase >= 0 && iRectPhase <= 2)
						{
							if (g_CanData.aRoughDataRect[iAddr][RECT_PHA_CURR].iValue != CAN_SAMP_INVALID_VALUE)
							{
							if(iPhaseCurrState[iRectPhase] != PHASE_CURR_STATE_INVALID)
							{
								fPhaseCurr[iRectPhase] += g_CanData.aRoughDataRect[iAddr][Rt_Greater40Sig[j].iRoughData].fValue;
								iPhaseCurrState[iRectPhase] = PHASE_CURR_STATE_VALID;
							}
							}
							else
							{
							iPhaseCurrState[iRectPhase] = PHASE_CURR_STATE_INVALID;
							}
						}
					}
				}
				else
				{
					if (RECT_PHA_CURR == Rt_Greater40Sig[j].iRoughData
						|| RECT_PHB_CURR == Rt_Greater40Sig[j].iRoughData
						|| RECT_PHC_CURR == Rt_Greater40Sig[j].iRoughData)
					{
						iRectPhase = Rt_Greater40Sig[j].iRoughData - RECT_PHA_CURR;
						if (g_CanData.aRoughDataRect[iAddr][Rt_Greater40Sig[j].iRoughData].iValue != CAN_SAMP_INVALID_VALUE)
						{
							if(iPhaseCurrState[iRectPhase] != PHASE_CURR_STATE_INVALID)
							{
							fPhaseCurr[iRectPhase] += g_CanData.aRoughDataRect[iAddr][Rt_Greater40Sig[j].iRoughData].fValue;
							iPhaseCurrState[iRectPhase] = PHASE_CURR_STATE_VALID;
							}
						}
						else
						{
							iPhaseCurrState[iRectPhase] = PHASE_CURR_STATE_INVALID;
						}
					}
				}

				j++;

			}
		}

		for(i = iRectNum; i < MAX_NUM_RECT; i++)
		{
			if(i < MAX_NUM1_RECT)
				EnumProc(i * MAX_CHN_NUM_PER_RT + RT_CH_EXIST_STATE, 
					g_CanData.aRoughDataRect[iRectNum][RECT_EXIST_ST].fValue, 
					lpvoid );
			else
				EnumProc((i - MAX_NUM1_RECT) * MAX_CHN_NUM_PER_RT + RT_CH_EXIST_STATE + CAN2_OFFSET, 
					g_CanData.aRoughDataRect[iRectNum][RECT_EXIST_ST].fValue, 
					lpvoid );
		}

		if(PHASE_CURR_STATE_VALID == iPhaseCurrState[0])
		{
		    g_CanData.aRoughDataGroup[GROUP_RT_PHASEA_CURR].fValue = fPhaseCurr[0];
		}
		if(PHASE_CURR_STATE_VALID == iPhaseCurrState[1])
		{
		    g_CanData.aRoughDataGroup[GROUP_RT_PHASEB_CURR].fValue = fPhaseCurr[1];
		}
		if(PHASE_CURR_STATE_VALID == iPhaseCurrState[2])
		{
		    g_CanData.aRoughDataGroup[GROUP_RT_PHASEC_CURR].fValue = fPhaseCurr[2];
		}
		EnumProc(RT_CH_INPUT_PHASEA_CURR,
			g_CanData.aRoughDataGroup[GROUP_RT_PHASEA_CURR].fValue,
			lpvoid);
		EnumProc(RT_CH_INPUT_PHASEB_CURR,
			g_CanData.aRoughDataGroup[GROUP_RT_PHASEB_CURR].fValue,
			lpvoid);
		EnumProc(RT_CH_INPUT_PHASEC_CURR,
			g_CanData.aRoughDataGroup[GROUP_RT_PHASEC_CURR].fValue,
			lpvoid);
	}


	return;
}


//�����ṩ�����������Ϊ��?802��Ŀ
VOID Rect_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	INT32	iTemp;
	INT32	i,iAddr,kk;
	BYTE	byTempBuf[4];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;
	UINT	uiTemp13;
	BOOL	legacy_NS802;

	PRODUCT_INFO *				pInfo;
	pInfo						= (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");

	
	//for(iAddr = 0; iAddr < MAX_NUM_RECT; iAddr++)
	for(iAddr = 0; iAddr < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; iAddr++)
	{	
		legacy_NS802 = FALSE;
	
		kk = GetRtAddrBySeqNo(iAddr);

		if ((CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataRect[kk][RECT_BARCODE1].iValue))
		{
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			continue;
		}
		else
		{
			pInfo->bSigModelUsed = TRUE;
		}

		//1.������ ��ʹ��
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.���ŵ�λ	SERIAL_NO_LOW=580005dc
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue);
		
		// added by George Plaza
		if ( byTempBuf[1] == 0xFF )
		{
			legacy_NS802 = TRUE;
				// year and month encoded in CODE23 and CODE24 of barcode 4 for legacy_NS802
			byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue >> 16);
			byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue >> 8);
		}
		else
		{
			byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue >> 24) & 0x1f;
			byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue >> 16) & 0x0f;
		}
		
		//��
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",byTempBuf[0]/10,byTempBuf[0]%10);
		//��
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",byTempBuf[1]/10,byTempBuf[1]%10);	

		//���ŵ�λ�ĵ�16�ֽ� ��Ӧ	##### ��������������
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		

		//3.�汾��VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].uiValue);

		//ģ��汾��?
		// added by George Plaza
		if ( (0xFF == byTempBuf[0]) && (0xFF == byTempBuf[1]) )
		{
			strcpy(pInfo->szHWVersion, "---");
		}
		else
		{
			//H		A00��ʽ�����ֽڰ�A=0 B=1���ƣ�����A=65
			byTempBuf[0] = 0x3f & byTempBuf[0];
			uiTemp = byTempBuf[0] + 65;
			pInfo->szHWVersion[0] = uiTemp;

			//VV 00����ʵ����ֵ������ֽ�?
			snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
				"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
			pInfo->szHWVersion[4] = '\0';//end
		}
		
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		
		// added by George Plaza
		if ( legacy_NS802 )
		{
			uiTemp11 = (uiTemp % 1000);
			uiTemp12 = (uiTemp11 % 100);
			uiTemp13 = (uiTemp12 % 10);
			
			
			snprintf(pInfo->szSWVersion, 8,
				"%d.%d.%d.%d",uiTemp/1000, uiTemp11/100,uiTemp12/10, uiTemp13);
			pInfo->szSWVersion[8] = '\0';//end
		}
		else
		{
			//�����?		��������汾�?1.30,��������Ϊ�����ֽ�������130
			uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
			uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
			snprintf(pInfo->szSWVersion, 5,
				"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
			pInfo->szSWVersion[5] = '\0';//end
		}
		
		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE1].uiValue);

		//FF ��ASCII����?,ռ��CODE1 �� CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T ��ʾ��Ʒ����,ASCIIռ��CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		// added by George Plaza
		if ( legacy_NS802 )
		{
			pInfo->szPartNumber[11] = 0x20;
			pInfo->szPartNumber[12] = 0x20;
		}
		else
		{
			if (BarCodeDataIsValid(byTempBuf[1]))
			{
				pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
			}
			else
			{
				pInfo->szPartNumber[11] = '\0';
			}
		
			if (BarCodeDataIsValid(byTempBuf[2]))
			{
				pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
			}
			else
			{
				pInfo->szPartNumber[12] = '\0';
			}
		}
		
		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nCovert bSigModelUsed:%d\n", pInfo->bSigModelUsed);
		//printf("\nCovert PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
		//printf("\nCnCovert	version:%s\n", pInfo->szSWVersion); //1.20
		//printf("\nnCovert version:%s\n", pInfo->szHWVersion); //A00
		//printf("\nnCovert Serial Number:%s\n", pInfo->szSerialNumber);//03100500006

		pInfo++;
	}
	//printf("\n	########		END		Convert_	GetProdctInfo	########	\n");

}



/*
VOID Rect_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	INT32	iTemp;
	INT32	i,iAddr,kk;
	BYTE	byTempBuf[4];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *				pInfo;
	pInfo						= (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");

	//for(iAddr = 0; iAddr < MAX_NUM_RECT; iAddr++)
	for(iAddr = 0; iAddr < g_CanData.CanCommInfo.RectCommInfo.iCommRectNum; iAddr++)
	{
		kk = GetRtAddrBySeqNo(iAddr);

		if ((CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataRect[kk][RECT_BARCODE1].iValue))
		{
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			continue;
		}
		else
		{
			pInfo->bSigModelUsed = TRUE;
		}

		//1.������ ��ʹ��
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.���ŵ�λ	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_SERIEL_NO].uiValue);

		//��
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//��
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//���ŵ�λ�ĵ�16�ֽ� ��Ӧ	##### ��������������
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		

		//3.�汾��VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_VERSION_NO].uiValue);

		//ģ��汾��?
		//H		A00��ʽ�����ֽڰ�A=0 B=1���ƣ�����A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00����ʵ����ֵ������ֽ�?
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//�����?		��������汾�?1.30,��������Ϊ�����ֽ�������130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end

		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE1].uiValue);

		//FF ��ASCII����?,ռ��CODE1 �� CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T ��ʾ��Ʒ����,ASCIIռ��CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataRect[kk][RECT_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nCovert bSigModelUsed:%d\n", pInfo->bSigModelUsed);
		//printf("\nCovert PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
		//printf("\nCnCovert	version:%s\n", pInfo->szSWVersion); //1.20
		//printf("\nnCovert version:%s\n", pInfo->szHWVersion); //A00
		//printf("\nnCovert Serial Number:%s\n", pInfo->szSerialNumber);//03100500006

		pInfo++;
	}
	//printf("\n	########		END		Convert_	GetProdctInfo	########	\n");

}
*/

static void Param24VDispByConvProc(void)
{
	SIG_ENUM stVoltLevelState = 0;
	stVoltLevelState = GetEnumSigValue(1,2,224,"CT Get SysVolt Level");
	SIG_ENUM st24VState =GetEnumSigValue(1,
					SIG_TYPE_SAMPLING,
					184,
					"CAN_SAMP");
	if(GetEnumSigValue(1, 
		SIG_TYPE_SETTING,
		SYS_CONVERTER_ONLY_ID,
		"CAN_SAMP"))
	{
		//Do not display 24V parameter.
		if(st24VState != 0)
		{
			SetEnumSigValue(1,
				SIG_TYPE_SAMPLING,
				184,
				0,
				"CAN_SAMP");
		}
	}
	else
	{
		//if(g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue < 35.0)
		if(stVoltLevelState)//24 V
		{
			//Do not display 24V parameter.
			if(st24VState != 1)
			{
				SetEnumSigValue(1,
					SIG_TYPE_SAMPLING,
					184,
					1,
					"CAN_SAMP");
			}
		}
		else
		{
			//Do not display 24V parameter.
			if(st24VState != 0 )
			{
				SetEnumSigValue(1,
					SIG_TYPE_SAMPLING,
					184,
					0,
					"CAN_SAMP");
			}
		}
	}
	return;
}


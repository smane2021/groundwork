#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "can_sampler_main.h"
#include "can_smduh.h"

//changed by Frank Wu,20140123,39/57, for SMDUH TR129 and Hall Calibrate
#include "cfg_model.h"

extern CAN_SAMPLER_DATA	g_CanData;
/*==========================================================================*
* FUNCTION : PackAndSendSmduhCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UINT   uiMsgType    : 
*            UINT   uiDestAddr   : 
*            UINT   uiCmdType    : 
*            UINT   uiValueTypeH : 
*            UINT   uiValueTypeL : 
*            float  fParam       : 
*	     UINT uiCANPort	 :
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static void PackAndSendSmduhCmd(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   float fParam,
							   UINT uiCANPort)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDU_CONTROLLER,
		(uiDestAddr + SMDUH_ADDR_OFFSET), 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	

	//changed by Frank Wu,20140123,40/57, for SMDUH TR129 and Hall Calibrate
	//int i;
	//printf("\nPackAndSendSmduhCmd SEND>>>>\n");
	//if(uiCANPort == SMDUHonCAN1)
	//{
	//	printf("SMDUHonCAN1\n");
	//}
	//else if(uiCANPort == SMDUHonCAN2)
	//{
	//	printf("SMDUHonCAN2\n");
	//}
	//for(i = 0; i < CAN_FRAME_LEN; i++)
	//{
	//	printf("  %0x", pbySendBuf[i]);
	//}
	//printf("\n--------------------------------------\n");

	if(uiCANPort == SMDUHonCAN1)
	{
		write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}
	else if(uiCANPort == SMDUHonCAN2)
	{
		write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}
	/*else
	{
		write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
		write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}*/

	return;
}

/*==========================================================================*
* FUNCTION : SmduhUnpackBarcode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static BOOL SmduhUnpackBarcode(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	BYTE	bValueType;
	UINT		uiSerialNo;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDU_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)!= ((UINT)iAddr + SMDUH_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			continue;

		}

		bValueType = (int)*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);

		if((bValueType >= SMDUH_BARCODE_1) && (bValueType <= SMDUH_BARCODE_4))
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE1 + bValueType - SMDUH_BARCODE_1].uiValue 
					= CAN_StringToUint(pValueBuf);
			iFrameNum++;
		}
		else if(bValueType == SMDUH_SERIALNO)
		{
			uiSerialNo = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_SERIAL_NO_LOW].uiValue 
				= uiSerialNo;
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_SERIAL_NO_HIGH].uiValue 
				= uiSerialNo >> 30;
			iFrameNum++;
		}
		else if(bValueType == SMDUH_VERSIOM)
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_VERSION_NO].uiValue 
				= CAN_StringToUint(pValueBuf);
			iFrameNum++;
		}
		else //if(bValueType == SMDUH_FEATURE)
		{
			iFrameNum++;
		}
	}
	//printf("FrameNum=%d",iFrameNum);
	//if(iFrameNum == SMDUH_CMDBARCODE_FRAMES_NUM)
	if(iFrameNum)
		return TRUE;
	else
		return FALSE;
	//else
	//	return FALSE;
}

/*==========================================================================*
* FUNCTION : SmduhUnpackAlm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static BOOL SmduhUnpackAlm(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	int	iValueType, iValue;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDU_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)!= ((UINT)iAddr + SMDUH_ADDR_OFFSET)))
		{
			continue;
		}

		iValueType = (((int)*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H)) << 8)
				+ (int)*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);

		if(iValueType == SMDUH_VOLT)
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_BUS_VOLT].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if(iValueType == SMDUH_ALARM)
		{
			iValue = CAN_StringToFloat(pValueBuf);
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_VOLT_ALM].fValue 
					= (iValue >> 13) & 0x01;
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_FAULT_ALM].fValue 
					= (iValue >> 10) & 0x01;
			iFrameNum++;
		}
		else
			iFrameNum++;
	}

	if(iFrameNum == SMDUH_CMDALM_FRAMES_NUM)
	//if(iFrameNum)
		return TRUE;
	else
		return FALSE;
}

/*==========================================================================*
* FUNCTION : SmduhUnpackGetData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static BOOL SmduhUnpackGetData(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	BYTE	bValueType;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDU_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iAddr + SMDUH_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 1))
		{
			continue;
		}

		bValueType = *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);

		if((bValueType <= SMDUH_CURR20))
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_LOAD_CURR1 + bValueType - SMDUH_CURR1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType >= SMDUH_KW1) && (bValueType <= SMDUH_KW20))
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_KW_1 + bValueType - SMDUH_KW1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType >= SMDUH_DAYKWH1) && (bValueType <= SMDUH_DAYKWH20))
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_DAY_KWH1 + bValueType - SMDUH_DAYKWH1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType >= SMDUH_TOTALKWH1) && (bValueType <= SMDUH_TOTALKWH20))
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_TOTAL_KWH1 + bValueType - SMDUH_TOTALKWH1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		//changed by Frank Wu,20140123,41/57, for SMDUH TR129 and Hall Calibrate
		else if((bValueType >= SMDUH_HALLCOEFF1) && (bValueType <= SMDUH_HALLCOEFF20))
		{
			UINT iValMA = ((UINT)pValueBuf[0] << 8) + (UINT)pValueBuf[1];
			UINT iValA = ((UINT)pValueBuf[2] << 8) + (UINT)pValueBuf[3];
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_HALL_COEFF1 + bValueType - SMDUH_HALLCOEFF1].fValue 
				= iValA + iValMA/10000.0;
			iFrameNum++;
		}
		else
			iFrameNum++;
	}

	if(iFrameNum == SMDUH_CMDGETDATA_FRAMES_NUM)
	//if(iFrameNum)
		return TRUE;
	else
		return FALSE;
}

/*==========================================================================*
* FUNCTION : ClrSmIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static void ClrSmduhIntrruptTimes(int iAddr)
{
	g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_TIMES].iValue = 0;
	g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_ST].iValue 
		= SMDUH_COMM_NORMAL_ST;

	return;
}

/*==========================================================================*
* FUNCTION : IncSmduhIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static void IncSmduhIntrruptTimes(int iAddr)
{
	if(g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_TIMES].iValue 
		< SMDUH_MAX_INTERRUPT_TIMES + 2)
	{
		g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_TIMES].iValue++;
	}

	if(g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_TIMES].iValue
		>= (SMDUH_MAX_INTERRUPT_TIMES + 1))
	{
		if(g_CanData.aRoughDataGroup[GROUP_SMDUH_ALL_NORESPONSE].iValue)
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_ST].iValue 
				= SMDUH_COMM_ALL_INTERRUPT_ST;
		}
		else
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_ST].iValue 
				= SMDUH_COMM_INTERRUPT_ST;
		}
	}
	else
	{
		g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_ST].iValue 
			= SMDUH_COMM_INTERRUPT_ST;
	}

	return;
}

/*==========================================================================*
* FUNCTION : SmduhSampleBarcode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static SIG_ENUM SmduhSampleBarcode(int iAddr, UINT uiCANPort)
{
	int		iReadLen1st = 0;

	//Sleep(20);

	PackAndSendSmduhCmd(MSG_TYPE_RQST_DATA2D,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		//(UINT)GetSmSetByteCmd00(iAddr),
		0,
		SMDUH_VAL_TYPE_NO_TRIM_VOLT,
		0.0,
		uiCANPort);

	Sleep(SMDUH_CMDBARCODE_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);

	if(uiCANPort == SMDUHonCAN1)
	{
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_CanData.CanCommInfo.abyRcvBuf, 
			&iReadLen1st);
	}
	else
	{
		CAN2_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_CanData.CanCommInfo.abyRcvBuf, 
			&iReadLen1st);
	}
	//printf("iReadLen1st=%d;\n", iReadLen1st);

	if(iReadLen1st >= (SMDUH_CMDBARCODE_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmduhUnpackBarcode(iAddr,
			iReadLen1st,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmduhIntrruptTimes(iAddr);
			//printf("***Barcode %d OK!", iAddr);
		}
		else
		{
			IncSmduhIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		IncSmduhIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}

/*==========================================================================*
* FUNCTION : SmduhSampleAlm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static SIG_ENUM SmduhSampleAlm(int iAddr, UINT uiCANPort)
{
	int		iReadLen1st = 0;

	PackAndSendSmduhCmd(MSG_TYPE_RQST_DATA38,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		//(UINT)GetSmSetByteCmd00(iAddr),
		0,
		SMDUH_VAL_TYPE_NO_TRIM_VOLT,
		0.0,
		uiCANPort);

	Sleep(30);
	if(uiCANPort == SMDUHonCAN1)
	{
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_CanData.CanCommInfo.abyRcvBuf, 
			&iReadLen1st);
	}
	else
	{
		CAN2_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_CanData.CanCommInfo.abyRcvBuf, 
			&iReadLen1st);
	}

	if(iReadLen1st >= (SMDUH_CMDALM_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmduhUnpackAlm(iAddr,
			iReadLen1st,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmduhIntrruptTimes(iAddr);
		}
		else
		{
			IncSmduhIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		IncSmduhIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}
/*==========================================================================*
* FUNCTION : SmduhSampleCmd00
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static SIG_ENUM SmduhSampleGetData(int iAddr, UINT uiMsgType, UINT uiCANPort)
{
	int		iReadLen1st = 0;

	PackAndSendSmduhCmd(uiMsgType,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		//(UINT)GetSmSetByteCmd00(iAddr),
		0,
		SMDUH_VAL_TYPE_NO_TRIM_VOLT,
		0.0,
		uiCANPort);

	Sleep(SMDUH_CMDGETDATA_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);
	if(uiCANPort == SMDUHonCAN1)
	{
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_CanData.CanCommInfo.abyRcvBuf, 
			&iReadLen1st);
	}
	else
	{
		CAN2_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_CanData.CanCommInfo.abyRcvBuf, 
			&iReadLen1st);
	}
	//printf("\nSMDU    Addr: %d; Len: %d",iAddr,iReadLen1st);
	if(iReadLen1st >= (SMDUH_CMDGETDATA_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmduhUnpackGetData(iAddr,
			iReadLen1st,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmduhIntrruptTimes(iAddr);
		}
		else
		{
			IncSmduhIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		IncSmduhIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}

/*==========================================================================*
* FUNCTION : SM_RefreshFlashInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 16:29
*==========================================================================*/
static void SMDUH_RefreshFlashInfo(void)
{
	int			i;

	for(i = 0; i < MAX_NUM_SMDUH; i++)
	{
		g_CanData.CanFlashData.aSmduhInfo[i].dwHighSn 
			= g_CanData.aRoughDataSmduh[i][SMDU_SERIAL_NO_HIGH].dwValue;
		g_CanData.CanFlashData.aSmduhInfo[i].dwSerialNo 
			= g_CanData.aRoughDataSmduh[i][SMDU_SERIAL_NO_LOW].dwValue;
		g_CanData.CanFlashData.aSmduhInfo[i].iSeqNo 
			= g_CanData.aRoughDataSmduh[i][SMDU_SEQ_NO].iValue;
		g_CanData.CanFlashData.aSmduhInfo[i].iAddress = i; 

		if(g_CanData.CanFlashData.aSmduhInfo[i].iSeqNo == CAN_SAMP_INVALID_VALUE)
		{
			g_CanData.CanFlashData.aSmduhInfo[i].bExistence = FALSE;
		}
		else
		{
			g_CanData.CanFlashData.aSmduhInfo[i].bExistence = TRUE;		
		}
	}

	CAN_WriteFlashData(&(g_CanData.CanFlashData));

	return;
}

void SMDUH_InitRoughValue(void)
{
	int i,j;
	if(g_CanData.aRoughDataGroup[GROUP_SMDUH_ACTUAL_NUM].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataGroup[GROUP_SMDUH_ACTUAL_NUM].iValue = 0;	
	}

	for(i = 0; i < MAX_NUM_SMDUH; i++)
	{
		for(j = 0; j < SMDUH_MAX_SIGNALS_NUM; j++)
		{
			g_CanData.aRoughDataSmduh[i][j].iValue = CAN_SAMP_INVALID_VALUE;
		}
	}

	g_CanData.CanCommInfo.SmduHCommInfo.bNeedFreshKWH = TRUE;
}

/*==========================================================================*
* FUNCTION : SMDUH_Reconfig
* PURPOSE  : Judge if SM-DUs need to re-config, if need, scan SM-DUs and 
*            save SM-DU connecting information.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
void SMDUH_Reconfig(void)
{
	int	iSmduNum, iAddr,iTemp;
	SIG_ENUM	emRst;
	UINT	bCANPort;

	SMDUH_InitRoughValue();

	iTemp = MAX_NUM_SMDUH;
	iSmduNum = 0;
	bCANPort = SMDUHonCAN1;
	//printf("Begin SMDUH cfg!!!\n");
	Sleep(20);

	for(iAddr = 0; iAddr < MAX_NUM_SMDUH; iAddr++)
	{
		//emRst = SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA2E, SMDUHonCAN1);
		emRst = SmduhSampleBarcode(iAddr, SMDUHonCAN1);
		//printf("Addr = %d; emRst = %d\n",iAddr, emRst);

		if(CAN_SAMPLE_OK == emRst)
		{
			g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue = SMDUHonCAN1;
			bCANPort = SMDUHonCAN1;
			iTemp = iAddr;
			break;
		}
		else
		{
			emRst = SmduhSampleBarcode(iAddr, SMDUHonCAN2);

			if(CAN_SAMPLE_OK == emRst)
			{
				g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue = SMDUHonCAN2;
				bCANPort = SMDUHonCAN2;
				iTemp = iAddr;
				//printf("SMDUH Addr=%d", iAddr);
				break;
			}
		}
		
	}

	for(iAddr = iTemp; iAddr < MAX_NUM_SMDUH; iAddr++)
	{
		emRst = SmduhSampleBarcode(iAddr, bCANPort);
		//printf("SMDUH Port:%d", bCANPort);
		if(CAN_SAMPLE_OK == emRst)
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDU_SEQ_NO].iValue = iSmduNum;
			iSmduNum++;
			//printf("Num=%d", iSmduNum);
		}
	}

	if(iSmduNum)
	{
		if(g_CanData.aRoughDataGroup[GROUP_SMDUH_ACTUAL_NUM].iValue != iSmduNum)
		{
			g_CanData.aRoughDataGroup[GROUP_SMDUH_ACTUAL_NUM].iValue = iSmduNum;
			SetDwordSigValue(SMDUH_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				CFG_CHANGED_SIG_ID,
				1,
				"CAN_SAMP");
		}

		g_CanData.aRoughDataGroup[GROUP_SMDUH_GROUP_STATE].iValue 
			= SMDUH_EQUIP_EXISTENT;
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_SMDUH_GROUP_STATE].iValue 
			= SMDUH_EQUIP_NOT_EXISTENT;
	}
	SMDUH_RefreshFlashInfo();
	//printf("SMDUH Port:%d\n",g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
}

/*==========================================================================*
* FUNCTION : SmduhAllNoResp
* PURPOSE  : Judge if SM-DUs need to re-config, if need, scan SM-DUs and 
*            save SM-DU connecting information.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static BOOL SmduhAllNoResp(void)
{
	int i;

	for(i = 0; i < MAX_NUM_SMDUH; i++)
	{
		//if(g_CanData.CanFlashData.aSmduhInfo[i].bExistence)
		if((g_CanData.aRoughDataSmduh[i][SMDUH_EXISTENCE].iValue == SMDUH_EQUIP_EXISTENT)
			 && (g_CanData.aRoughDataSmdu[i][SMDUH_INTERRUPT_TIMES].iValue < SMDUH_MAX_INTERRUPT_TIMES))
		{
			return FALSE;
		}
	}
	return TRUE;
}

/*==========================================================================*
* FUNCTION : SMDUH_Reconfig
* PURPOSE  : Judge if SM-DUs need to re-config, if need, scan SM-DUs and 
*            save SM-DU connecting information.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
static void FreshTimeforSMDUH(void)
{
	time_t				tNow;   
	struct tm			tmNow; 

   
	tNow = time(NULL);
	gmtime_r(&tNow, &tmNow);

	if(tmNow.tm_hour == 0)
	{
		g_CanData.CanCommInfo.SmduHCommInfo.bNeedFreshKWH = TRUE;
	}
}

/*==========================================================================*
* FUNCTION : SMDUH_Reconfig
* PURPOSE  : Judge if SM-DUs need to re-config, if need, scan SM-DUs and 
*            save SM-DU connecting information.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-19 
*==========================================================================*/
void SMDUH_Sample(void)
{
	int		iAddr;

	//printf("SMDUH Rcfg=%d\n",g_CanData.CanCommInfo.SmduHCommInfo.bNeedReconfig);

	if(g_CanData.CanCommInfo.SmduHCommInfo.bNeedReconfig)
	{
		RT_UrgencySendCurrLimit();
		g_CanData.CanCommInfo.SmduHCommInfo.bNeedReconfig = FALSE;
		SMDUH_Reconfig();
		g_CanData.aRoughDataGroup[GROUP_SMDUH_ALL_NORESPONSE].iValue 
			= SmduhAllNoResp();
		return;
	}
	FreshTimeforSMDUH();
	//printf("SMDUH NUM = %d;%d\n",g_CanData.aRoughDataGroup[GROUP_SMDUH_ACTUAL_NUM].iValue,g_CanData.CanFlashData.aSmduhInfo[0].bExistence);

	for(iAddr = 0; iAddr < MAX_NUM_SMDUH; iAddr++)
	{
//		printf("SMDUH Sample!!!\n");
		//printf("Exist %d=%d;\n",iAddr,g_CanData.CanFlashData.aSmduhInfo[iAddr].bExistence);
		if(g_CanData.CanFlashData.aSmduhInfo[iAddr].bExistence)
		{
			/*Curr*/
			SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA2E, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA2F, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			/*kw*/
			SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA30, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA31, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			/*kwh*/
			SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA34, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA35, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			//changed by Frank Wu,20140123,42/57, for SMDUH TR129 and Hall Calibrate
			/*Hall Coeff, Not used*/
			//SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA36, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			//SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA37, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			/*volt & alarm*/
			SmduhSampleAlm(iAddr, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);

			if(g_CanData.CanCommInfo.SmduHCommInfo.bNeedFreshKWH)
			{/*最近一天电量数据*/
				SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA32, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
				SmduhSampleGetData(iAddr, MSG_TYPE_RQST_DATA33, g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue);
			}

			g_CanData.aRoughDataSmduh[iAddr][SMDUH_EXISTENCE].iValue = SMDUH_EQUIP_EXISTENT;
			//printf("SMDUH = %d\n", g_CanData.aRoughDataGroup[GROUP_SMDUH_ACTUAL_NUM].iValue);
		}
		else
		{
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_EXISTENCE].iValue 
				= SMDUH_EQUIP_NOT_EXISTENT;
			g_CanData.aRoughDataSmduh[iAddr][SMDUH_INTERRUPT_ST].iValue 
				= SMDUH_COMM_NORMAL_ST;
		}
	}
	g_CanData.CanCommInfo.SmduHCommInfo.bNeedFreshKWH = FALSE;
	g_CanData.aRoughDataGroup[GROUP_SMDUH_ALL_NORESPONSE].iValue 
		= SmduhAllNoResp();

	
}

/*==========================================================================*
* FUNCTION : SM_StuffChannel
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: ENUMSIGNALPROC  EnumProc : 
*            LPVOID          lpvoid   : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-20 
*==========================================================================*/
void SMDUH_StuffChannel(ENUMSIGNALPROC EnumProc,
					 LPVOID lpvoid)	
{
	int		i, j;
	int		iSmduNum = g_CanData.aRoughDataGroup[GROUP_SMDUH_ACTUAL_NUM].iValue;

	CHANNEL_TO_ROUGH_DATA		SmduhGroupSig[] =
	{
		{SM_CH_SMDUH_GROUP_STATE,	GROUP_SMDUH_GROUP_STATE,	},
		{SM_CH_SMDUH_NUM,			GROUP_SMDUH_ACTUAL_NUM,	},

		{SMDUH_CH_END_FLAG,			SMDUH_CH_END_FLAG,	},
	};

	CHANNEL_TO_ROUGH_DATA		SmduhSig[] =
	{
		{SMDUH_CH_BUS_VOLT,	SMDUH_BUS_VOLT,},
		{SMDUH_CH_LOAD_CURR1,	SMDUH_LOAD_CURR1,},
		{SMDUH_CH_LOAD_CURR2,	SMDUH_LOAD_CURR2,},
		{SMDUH_CH_LOAD_CURR3,	SMDUH_LOAD_CURR3,},
		{SMDUH_CH_LOAD_CURR4,	SMDUH_LOAD_CURR4,},
		{SMDUH_CH_LOAD_CURR5,	SMDUH_LOAD_CURR5,},
		{SMDUH_CH_LOAD_CURR6,	SMDUH_LOAD_CURR6,},
		{SMDUH_CH_LOAD_CURR7,	SMDUH_LOAD_CURR7,},
		{SMDUH_CH_LOAD_CURR8,	SMDUH_LOAD_CURR8,},
		{SMDUH_CH_LOAD_CURR9,	SMDUH_LOAD_CURR9,},
		{SMDUH_CH_LOAD_CURR10,	SMDUH_LOAD_CURR10,},
		{SMDUH_CH_LOAD_CURR11,	SMDUH_LOAD_CURR11,},
		{SMDUH_CH_LOAD_CURR12,	SMDUH_LOAD_CURR12,},
		{SMDUH_CH_LOAD_CURR13,	SMDUH_LOAD_CURR13,},
		{SMDUH_CH_LOAD_CURR14,	SMDUH_LOAD_CURR14,},
		{SMDUH_CH_LOAD_CURR15,	SMDUH_LOAD_CURR15,},
		{SMDUH_CH_LOAD_CURR16,	SMDUH_LOAD_CURR16,},
		{SMDUH_CH_LOAD_CURR17,	SMDUH_LOAD_CURR17,},
		{SMDUH_CH_LOAD_CURR18,	SMDUH_LOAD_CURR18,},
		{SMDUH_CH_LOAD_CURR19,	SMDUH_LOAD_CURR19,},
		{SMDUH_CH_LOAD_CURR20,	SMDUH_LOAD_CURR20,},
		{SMDUH_CH_KW_1,		SMDUH_KW_1,},
		{SMDUH_CH_KW_2,		SMDUH_KW_2,},
		{SMDUH_CH_KW_3,		SMDUH_KW_3,},
		{SMDUH_CH_KW_4,		SMDUH_KW_4,},
		{SMDUH_CH_KW_5,		SMDUH_KW_5,},
		{SMDUH_CH_KW_6,		SMDUH_KW_6,},
		{SMDUH_CH_KW_7,		SMDUH_KW_7,},
		{SMDUH_CH_KW_8,		SMDUH_KW_8,},
		{SMDUH_CH_KW_9,		SMDUH_KW_9,},
		{SMDUH_CH_KW_10,	SMDUH_KW_10,},
		{SMDUH_CH_KW_11,	SMDUH_KW_11,},
		{SMDUH_CH_KW_12,	SMDUH_KW_12,},
		{SMDUH_CH_KW_13,	SMDUH_KW_13,},
		{SMDUH_CH_KW_14,	SMDUH_KW_14,},
		{SMDUH_CH_KW_15,	SMDUH_KW_15,},
		{SMDUH_CH_KW_16,	SMDUH_KW_16,},
		{SMDUH_CH_KW_17,	SMDUH_KW_17,},
		{SMDUH_CH_KW_18,	SMDUH_KW_18,},
		{SMDUH_CH_KW_19,	SMDUH_KW_19,},
		{SMDUH_CH_KW_20,	SMDUH_KW_20,},
		{SMDUH_CH_DAYW_KWH1,	SMDUH_DAY_KWH1,},
		{SMDUH_CH_DAYW_KWH2,	SMDUH_DAY_KWH2,},
		{SMDUH_CH_DAYW_KWH3,	SMDUH_DAY_KWH3,},
		{SMDUH_CH_DAYW_KWH4,	SMDUH_DAY_KWH4,},
		{SMDUH_CH_DAYW_KWH5,	SMDUH_DAY_KWH5,},
		{SMDUH_CH_DAYW_KWH6,	SMDUH_DAY_KWH6,},
		{SMDUH_CH_DAYW_KWH7,	SMDUH_DAY_KWH7,},
		{SMDUH_CH_DAYW_KWH8,	SMDUH_DAY_KWH8,},
		{SMDUH_CH_DAYW_KWH9,	SMDUH_DAY_KWH9,},
		{SMDUH_CH_DAYW_KWH10,	SMDUH_DAY_KWH10,},
		{SMDUH_CH_DAYW_KWH11,	SMDUH_DAY_KWH11,},
		{SMDUH_CH_DAYW_KWH12,	SMDUH_DAY_KWH12,},
		{SMDUH_CH_DAYW_KWH13,	SMDUH_DAY_KWH13,},
		{SMDUH_CH_DAYW_KWH14,	SMDUH_DAY_KWH14,},
		{SMDUH_CH_DAYW_KWH15,	SMDUH_DAY_KWH15,},
		{SMDUH_CH_DAYW_KWH16,	SMDUH_DAY_KWH16,},
		{SMDUH_CH_DAYW_KWH17,	SMDUH_DAY_KWH17,},
		{SMDUH_CH_DAYW_KWH18,	SMDUH_DAY_KWH18,},
		{SMDUH_CH_DAYW_KWH19,	SMDUH_DAY_KWH19,},
		{SMDUH_CH_DAYW_KWH20,	SMDUH_DAY_KWH20,},
		{SMDUH_CH_TOTAL_KWH1,	SMDUH_TOTAL_KWH1,},
		{SMDUH_CH_TOTAL_KWH2,	SMDUH_TOTAL_KWH2,},
		{SMDUH_CH_TOTAL_KWH3,	SMDUH_TOTAL_KWH3,},
		{SMDUH_CH_TOTAL_KWH4,	SMDUH_TOTAL_KWH4,},
		{SMDUH_CH_TOTAL_KWH5,	SMDUH_TOTAL_KWH5,},
		{SMDUH_CH_TOTAL_KWH6,	SMDUH_TOTAL_KWH6,},
		{SMDUH_CH_TOTAL_KWH7,	SMDUH_TOTAL_KWH7,},
		{SMDUH_CH_TOTAL_KWH8,	SMDUH_TOTAL_KWH8,},
		{SMDUH_CH_TOTAL_KWH9,	SMDUH_TOTAL_KWH9,},
		{SMDUH_CH_TOTAL_KWH10,	SMDUH_TOTAL_KWH10,},
		{SMDUH_CH_TOTAL_KWH11,	SMDUH_TOTAL_KWH11,},
		{SMDUH_CH_TOTAL_KWH12,	SMDUH_TOTAL_KWH12,},
		{SMDUH_CH_TOTAL_KWH13,	SMDUH_TOTAL_KWH13,},
		{SMDUH_CH_TOTAL_KWH14,	SMDUH_TOTAL_KWH14,},
		{SMDUH_CH_TOTAL_KWH15,	SMDUH_TOTAL_KWH15,},
		{SMDUH_CH_TOTAL_KWH16,	SMDUH_TOTAL_KWH16,},
		{SMDUH_CH_TOTAL_KWH17,	SMDUH_TOTAL_KWH17,},
		{SMDUH_CH_TOTAL_KWH18,	SMDUH_TOTAL_KWH18,},
		{SMDUH_CH_TOTAL_KWH19,	SMDUH_TOTAL_KWH19,},
		{SMDUH_CH_TOTAL_KWH20,	SMDUH_TOTAL_KWH20,},
		{SMDUH_CH_DC_VOLT_ST,	SMDUH_VOLT_ALM},
		{SMDUH_CH_FAULT_ST,	SMDUH_FAULT_ALM,		},
		{SMDUH_CH_BARCODE1,	SMDUH_BARCODE1,		},
		{SMDUH_CH_BARCODE2,	SMDUH_BARCODE2,		},
		{SMDUH_CH_BARCODE3,	SMDUH_BARCODE3,		},
		{SMDUH_CH_BARCODE4,	SMDUH_BARCODE4,},
		{SMDUH_CH_INTTERUPT_ST,	SMDUH_INTERRUPT_ST},
		{SMDUH_CH_EXIST_ST,	SMDUH_EXISTENCE},
		{SMDUH_CH_ADDRESS,	SMDUH_ADDRESS,	},
		//changed by Frank Wu,20140123,43/57, for SMDUH TR129 and Hall Calibrate
		{SMDUH_CH_HALL_COEFF1,	SMDUH_HALL_COEFF1,},
		{SMDUH_CH_HALL_COEFF2,	SMDUH_HALL_COEFF2,},
		{SMDUH_CH_HALL_COEFF3,	SMDUH_HALL_COEFF3,},
		{SMDUH_CH_HALL_COEFF4,	SMDUH_HALL_COEFF4,},
		{SMDUH_CH_HALL_COEFF5,	SMDUH_HALL_COEFF5,},
		{SMDUH_CH_HALL_COEFF6,	SMDUH_HALL_COEFF6,},
		{SMDUH_CH_HALL_COEFF7,	SMDUH_HALL_COEFF7,},
		{SMDUH_CH_HALL_COEFF8,	SMDUH_HALL_COEFF8,},
		{SMDUH_CH_HALL_COEFF9,	SMDUH_HALL_COEFF9,},
		{SMDUH_CH_HALL_COEFF10,	SMDUH_HALL_COEFF10,},
		{SMDUH_CH_HALL_COEFF11,	SMDUH_HALL_COEFF11,},
		{SMDUH_CH_HALL_COEFF12,	SMDUH_HALL_COEFF12,},
		{SMDUH_CH_HALL_COEFF13,	SMDUH_HALL_COEFF13,},
		{SMDUH_CH_HALL_COEFF14,	SMDUH_HALL_COEFF14,},
		{SMDUH_CH_HALL_COEFF15,	SMDUH_HALL_COEFF15,},
		{SMDUH_CH_HALL_COEFF16,	SMDUH_HALL_COEFF16,},
		{SMDUH_CH_HALL_COEFF17,	SMDUH_HALL_COEFF17,},
		{SMDUH_CH_HALL_COEFF18,	SMDUH_HALL_COEFF18,},
		{SMDUH_CH_HALL_COEFF19,	SMDUH_HALL_COEFF19,},
		{SMDUH_CH_HALL_COEFF20,	SMDUH_HALL_COEFF20,},

		{SMDUH_CH_END_FLAG,	SMDUH_CH_END_FLAG,	},
	};	

	i = 0;
	while(SmduhGroupSig[i].iChannel != SMDUH_CH_END_FLAG)
	{
		EnumProc(SmduhGroupSig[i].iChannel, 
			g_CanData.aRoughDataGroup[SmduhGroupSig[i].iRoughData].fValue, 
			lpvoid );
		i++;
	}

	for(i = 0; i < MAX_NUM_SMDUH; i++)
	{
		
		j = 0;
		while(SmduhSig[j].iChannel != SMDUH_CH_END_FLAG)
		{
			EnumProc(i * MAX_CHN_NUM_PER_SMDUH + SmduhSig[j].iChannel, 
				g_CanData.aRoughDataSmduh[i][SmduhSig[j].iRoughData].fValue, 
				lpvoid );

			j++;
		}
		

	}
}

void SMDUH_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	//以下为不往前堆时的算法：吴政武修改201205
	UNUSED(hComm);
	UNUSED(nUnitNo);
	int iAddr;
	PRODUCT_INFO *				pInfo;
	pInfo					= (PRODUCT_INFO*)pPI;
	for(iAddr = 0; iAddr < MAX_NUM_SMDU; iAddr++)
	{
		if(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE1].iValue 
			|| CAN_SMDUH_NOT_EXIT == g_CanData.aRoughDataSmduh[iAddr][SMDUH_EXISTENCE].iValue)
		{
			pInfo->bSigModelUsed = FALSE;			
			//printf("\n	This is ADDR=%d continue	\n",iAddr);
			pInfo++;
			continue;
		}
		else
		{
			//printf("\n	Can SM Temp Samp: This is ADDR=%d  and bSigModeUsed=TRUE	\n",iAddr);
			pInfo->bSigModelUsed = TRUE;
			//算法太土了，不用了，下面重新解析
			//SMDUPUnpackProdctInfo(pPI);
		}

		BYTE	byTempBuf[4];
		UINT	uiTemp;
		INT32	iTemp;
		UINT	uiTemp11;
		UINT	uiTemp12;

#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

		pInfo->bSigModelUsed = TRUE;

		//1.特征字 不使用
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.串号低位	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_SERIAL_NO_LOW].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_SERIAL_NO_LOW].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_SERIAL_NO_LOW].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_SERIAL_NO_LOW].uiValue);

		//printf("SMDUH SERIAL_LOW:%d;%d;%d;%d;\n", byTempBuf[0], byTempBuf[1],byTempBuf[2],byTempBuf[3]);
		//年
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//月
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//串号低位的低16字节 对应	##### 代表当月生产数量
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end	

		//3.版本号VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_VERSION_NO].uiValue);

		//模块版本号
		//H		A00形式，高字节按A=0 B=1类推，所以A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00按照实际数值存入低字节
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//软件版本		如若软件版本为1.30,保存内容为两两字节整型数130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end

		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE1].uiValue);

		//FF 用ASCII码表示,占用CODE1 和 CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//printf("SMDUH Serial:%d/ %s\n", byTempBuf[0],pInfo->szSerialNumber);
		//T 表示产品类型,ASCII占用CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduh[iAddr][SMDUH_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nSMTEMP bSigModelUsed:%d\n", pInfo->bSigModelUsed);

		pInfo++;

	}
}



//changed by Frank Wu,20140123,44/57, for SMDUH TR129 and Hall Calibrate
/*==========================================================================*
* FUNCTION : GetSmduhAddrByChnNo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iChannelNo : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Wu                DATE: 2014-01-21
*==========================================================================*/
static int GetSmduhAddrByChnNo(IN int iChannelNo)
{
	int i;

	int iSeqNo = (iChannelNo - CNMR_MAX_SMDUH_BROADCAST_CMD_CHN - 1) 
		/ MAX_CHN_NUM_PER_SMDUH;
	//ASSERT(iSeqNo >= 0 && iSeqNo < MAX_NUM_CONVERT);

	for(i = 0; i < MAX_NUM_SMDUH; i++)
	{
		if(iSeqNo == i)//g_CanData.aRoughDataSmduh[i][SMDU_SEQ_NO].iValue)
		{
			return i;
		}
	}

	return 0;
}

static int GetHallDemarcateChannel(IN int iSeqNo)
{
#define SMDUH_SIGNAL_ID_HALL_CALIBRATE_CHAN		2

	SIG_BASIC_VALUE *pSigVal = NULL;
	int iBufSize = 0;
	int iErr = -1;
	int iEquipID = SMDUH_GROUP_EQUIPID + 1 + iSeqNo;
	int iSigID = SMDUH_SIGNAL_ID_HALL_CALIBRATE_CHAN;
	int iDemarcateChan = 0;

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipID,
						DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,iSigID),
						&iBufSize,
						(void *)&pSigVal,
						0);
	if(iErr == ERR_DXI_OK)
	{
		iDemarcateChan = pSigVal->varValue.enumValue;
	}
	//printf("GetHallDemarcateChannel iDemarcateChan=%d\n", iDemarcateChan);
	if((iDemarcateChan < 0) || (iDemarcateChan > SMDUH_MAX_VALID_CHN_NUM))
	{
		iDemarcateChan = 0;
	}

	return iDemarcateChan;
}

static void DemarcateHall(IN float fSetValue,
						IN UINT uiCurrentPoint,
						IN UINT uiCANPort,
						IN UINT uiDestAddr,
						IN int iSeqNo)
{
	UINT i = 0, j = 0;
	//changed by Frank Wu,20140422, for the bug which a offset of about 2A
	//should be resend only once during 500ms
	//UINT iResendCount = 2;
	UINT iResendCount = 1;
	UINT uiChannel = 0;
	int iDemarcateChannelEnum = 0;

	iDemarcateChannelEnum = GetHallDemarcateChannel(iSeqNo);
	//printf("DemarcateHall iDemarcateChannelEnum=%d\n", iDemarcateChannelEnum);
	if(iDemarcateChannelEnum == 0)//demarcate all channels
	{
		for(i = 0; i < SMDUH_MAX_VALID_CHN_NUM; i++)
		{
			uiChannel = i;
			for(j = 0; j < iResendCount; j++)
			{
				PackAndSendSmduhCmd(MSG_TYPE_RQST_DATA3A,
					uiDestAddr,
					CAN_CMD_TYPE_P2P,
					uiChannel,
					uiCurrentPoint,
					fSetValue,
					uiCANPort);
				Sleep(50);
			}
			//printf("DemarcateHall uiChannel=%d\n", uiChannel);
		}
	}
	else
	{
		uiChannel = iDemarcateChannelEnum - 1;
		for(j = 0; j < iResendCount; j++)
		{
			PackAndSendSmduhCmd(MSG_TYPE_RQST_DATA3A,
				uiDestAddr,
				CAN_CMD_TYPE_P2P,
				uiChannel,
				uiCurrentPoint,
				fSetValue,
				uiCANPort);
			Sleep(50);
		}
		//printf("DemarcateHall uiChannel=%d\n", uiChannel);
	}
}

static void ClearEnergy(IN int iSetValue,
						IN UINT uiCANPort,
						IN UINT uiDestAddr,
						IN int iSeqNo)
{
	UNUSED(iSeqNo);
	UINT uiDay = 0;
	UINT j = 0;
	UINT iResendCount = 2;

	if((iSetValue >= 0) && (iSetValue <= SMDUH_MAX_RECORD_DAY_NUM))
	{
		//uiDay = iSetValue - 1;//just clear one day data
		//if(iSetValue == 0)//clear all data
		{
			uiDay = 255;
		}

		for(j = 0; j < iResendCount; j++)
		{
			PackAndSendSmduhCmd(MSG_TYPE_RQST_DATA39,
				uiDestAddr,
				CAN_CMD_TYPE_P2P,
				uiDay,
				0,
				0.0,
				uiCANPort);
			Sleep(10);
		}
	}
}
//changed by Frank Wu,18/19,20140408, for the function which clearing total energy of single channel
static void ClearChanEnergy(IN int iSetValue,
	IN UINT uiCANPort,
	IN UINT uiDestAddr,
	IN int iSeqNo)
{
#define CLEAR_TOTAL_ENERGY_VALUETYPE_CHAN_ALL		255
#define CLEAR_TOTAL_ENERGY_VALUETYPE_CHAN_1			30

	UNUSED(iSeqNo);
	UINT uiValueTypeH = CLEAR_TOTAL_ENERGY_VALUETYPE_CHAN_ALL;
	UINT j = 0;
	UINT iResendCount = 2;

	if((iSetValue >= 0) && (iSetValue <= SMDUH_MAX_VALID_CHN_NUM))
	{
		//uiDay = iSetValue - 1;//just clear one day data
		if(0 == iSetValue)//clear all channels
		{
			uiValueTypeH = CLEAR_TOTAL_ENERGY_VALUETYPE_CHAN_ALL;
		}
		else//clear some channel's total energy
		{
			uiValueTypeH = (iSetValue - 1) + CLEAR_TOTAL_ENERGY_VALUETYPE_CHAN_1;
		}

		for(j = 0; j < iResendCount; j++)
		{
			PackAndSendSmduhCmd(MSG_TYPE_RQST_DATA39,
				uiDestAddr,
				CAN_CMD_TYPE_P2P,
				uiValueTypeH,
				0,
				0.0,
				uiCANPort);
			Sleep(10);
		}
	}
}

/*==========================================================================*
* FUNCTION : SMDUH_SendCtlCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iChannelNo : 
*            float  fParam     : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Wu                DATE: 2014-01-21
*==========================================================================*/
void SMDUH_SendCtlCmd(IN int iChannelNo, IN float fParam)
{

	//FLOAT_STRING		unValue;
	
	if(iChannelNo < CNMR_MAX_SMDUH_BROADCAST_CMD_CHN)
	{
		switch(iChannelNo)
		{
			/*case CNSG_ESTOP_FUNCTION:
			PackAndSendSmduCmd(MSG_TYPE_RQST_SETTINGS,
			CAN_ADDR_FOR_BROADCAST,
			CAN_CMD_TYPE_BROADCAST,
			0,
			SM_VAL_TYPE_W_ESTOP_ENB,
			fParam);*/
			break;

		default:

			CAN_LOG_OUT(APP_LOG_WARNING, "Invalid control channel!\n");
			break;
		}
		//printf("The current CMD__SMDUHaddr is invalid");
		return;
	}
	else
	{
		int		iSmduhAddr = GetSmduhAddrByChnNo(iChannelNo);
		int		iSubChnNo = ((iChannelNo - CNMR_MAX_SMDUH_BROADCAST_CMD_CHN - 1)
			% MAX_CHN_NUM_PER_SMDUH) 
			+ CNMR_MAX_SMDUH_BROADCAST_CMD_CHN 
			+ 1;
		int iSeqNo = (iChannelNo - CNMR_MAX_SMDUH_BROADCAST_CMD_CHN - 1) 
			/ MAX_CHN_NUM_PER_SMDUH;
		UINT uiCANPort = g_CanData.aRoughDataGroup[GROUP_SMDUH_CANPORT].iValue;

		//printf("SMDUH_SendCtlCmd  iSmduhAddr=%d, iSubChnNo=%d, iSeqNo=%d\n",iSmduhAddr, iSubChnNo, iSeqNo);
		switch(iSubChnNo)
		{
			case CNSS_SMDUH_HALL_CALIBRATE_1:
			{
				float fSetValue = fParam;
				UINT uiCurrentPoint = 0;//0, the first current point; 1, the second current point
				//printf("SMDUH_SendCtlCmd fSetValue=%f, uiCurrentPoint=%d\n", fSetValue, uiCurrentPoint);
				DemarcateHall(fSetValue, uiCurrentPoint, uiCANPort, (UINT)iSmduhAddr, iSeqNo);

				break;
			}
			case CNSS_SMDUH_HALL_CALIBRATE_2:
			{
				float fSetValue = fParam;
				UINT uiCurrentPoint = 1;//0, the first current point; 1, the second current point
				//printf("SMDUH_SendCtlCmd fSetValue=%f, uiCurrentPoint=%d\n", fSetValue, uiCurrentPoint);
				DemarcateHall(fSetValue, uiCurrentPoint, uiCANPort, (UINT)iSmduhAddr, iSeqNo);

				break;
			}
			case CNSS_SMDUH_ENERGY_CLEAR:
			{
				int iSetValue = (int)(fParam + 0.0001);//[1]
				//printf("SMDUH_SendCtlCmd iSetValue=%d\n", iSetValue);
				ClearEnergy(iSetValue, uiCANPort, (UINT)iSmduhAddr, iSeqNo);

				break;
			}
			//changed by Frank Wu,19/19,20140408, for the function which clearing total energy of single channel
			case CNSS_SMDUH_CHAN_ENERGY_CLEAR:
			{
				int iSetValue = (int)(fParam + 0.0001);//[1]
				//printf("SMDUH_SendCtlCmd iSetValue=%d\n", iSetValue);
				ClearChanEnergy(iSetValue, uiCANPort, (UINT)iSmduhAddr, iSeqNo);

				break;
			}
		default:

			CAN_LOG_OUT(APP_LOG_WARNING, "Invalid control channel!\n");
			break;
		}
	}
	
	return;
}


